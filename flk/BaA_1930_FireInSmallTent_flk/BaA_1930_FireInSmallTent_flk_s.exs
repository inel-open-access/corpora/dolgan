<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID666DD352-8F78-ACF9-C83F-780541ED0F95">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>BaA_1930_FireInSmallTent_flk</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\flk\BaA_1930_FireInSmallTent_flk\BaA_1930_FireInSmallTent_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">393</ud-information>
            <ud-information attribute-name="# HIAT:w">298</ud-information>
            <ud-information attribute-name="# e">298</ud-information>
            <ud-information attribute-name="# HIAT:u">46</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="BaA">
            <abbreviation>BaA</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="4.0" type="appl" />
         <tli id="T1" time="4.5" type="appl" />
         <tli id="T2" time="5.0" type="appl" />
         <tli id="T3" time="5.5" type="appl" />
         <tli id="T4" time="6.0" type="appl" />
         <tli id="T5" time="6.5" type="appl" />
         <tli id="T6" time="7.0" type="appl" />
         <tli id="T7" time="7.5" type="appl" />
         <tli id="T8" time="8.0" type="appl" />
         <tli id="T9" time="8.5" type="appl" />
         <tli id="T10" time="9.0" type="appl" />
         <tli id="T11" time="9.5" type="appl" />
         <tli id="T12" time="10.0" type="appl" />
         <tli id="T13" time="10.5" type="appl" />
         <tli id="T14" time="11.0" type="appl" />
         <tli id="T15" time="11.5" type="appl" />
         <tli id="T16" time="12.0" type="appl" />
         <tli id="T17" time="12.5" type="appl" />
         <tli id="T18" time="13.0" type="appl" />
         <tli id="T19" time="13.5" type="appl" />
         <tli id="T20" time="14.0" type="appl" />
         <tli id="T21" time="14.5" type="appl" />
         <tli id="T22" time="15.0" type="appl" />
         <tli id="T23" time="15.5" type="appl" />
         <tli id="T24" time="16.0" type="appl" />
         <tli id="T25" time="16.5" type="appl" />
         <tli id="T26" time="17.0" type="appl" />
         <tli id="T27" time="17.5" type="appl" />
         <tli id="T28" time="18.0" type="appl" />
         <tli id="T29" time="18.5" type="appl" />
         <tli id="T30" time="19.0" type="appl" />
         <tli id="T31" time="19.5" type="appl" />
         <tli id="T32" time="20.0" type="appl" />
         <tli id="T33" time="20.5" type="appl" />
         <tli id="T34" time="21.0" type="appl" />
         <tli id="T35" time="21.5" type="appl" />
         <tli id="T36" time="22.0" type="appl" />
         <tli id="T37" time="22.5" type="appl" />
         <tli id="T38" time="23.0" type="appl" />
         <tli id="T39" time="23.5" type="appl" />
         <tli id="T40" time="24.0" type="appl" />
         <tli id="T41" time="24.5" type="appl" />
         <tli id="T42" time="25.0" type="appl" />
         <tli id="T43" time="25.5" type="appl" />
         <tli id="T44" time="26.0" type="appl" />
         <tli id="T45" time="26.5" type="appl" />
         <tli id="T46" time="27.0" type="appl" />
         <tli id="T47" time="27.5" type="appl" />
         <tli id="T48" time="28.0" type="appl" />
         <tli id="T49" time="28.5" type="appl" />
         <tli id="T50" time="29.0" type="appl" />
         <tli id="T51" time="29.5" type="appl" />
         <tli id="T52" time="30.0" type="appl" />
         <tli id="T53" time="30.5" type="appl" />
         <tli id="T54" time="31.0" type="appl" />
         <tli id="T55" time="31.5" type="appl" />
         <tli id="T56" time="32.0" type="appl" />
         <tli id="T57" time="32.5" type="appl" />
         <tli id="T58" time="33.0" type="appl" />
         <tli id="T59" time="33.5" type="appl" />
         <tli id="T60" time="34.0" type="appl" />
         <tli id="T61" time="34.5" type="appl" />
         <tli id="T62" time="35.0" type="appl" />
         <tli id="T63" time="35.5" type="appl" />
         <tli id="T64" time="36.0" type="appl" />
         <tli id="T65" time="36.5" type="appl" />
         <tli id="T66" time="37.0" type="appl" />
         <tli id="T67" time="37.5" type="appl" />
         <tli id="T68" time="38.0" type="appl" />
         <tli id="T69" time="38.5" type="appl" />
         <tli id="T70" time="39.0" type="appl" />
         <tli id="T71" time="39.5" type="appl" />
         <tli id="T72" time="40.0" type="appl" />
         <tli id="T73" time="40.5" type="appl" />
         <tli id="T74" time="41.0" type="appl" />
         <tli id="T75" time="41.5" type="appl" />
         <tli id="T76" time="42.0" type="appl" />
         <tli id="T77" time="42.5" type="appl" />
         <tli id="T78" time="43.0" type="appl" />
         <tli id="T79" time="43.5" type="appl" />
         <tli id="T80" time="44.0" type="appl" />
         <tli id="T81" time="44.5" type="appl" />
         <tli id="T82" time="45.0" type="appl" />
         <tli id="T83" time="45.5" type="appl" />
         <tli id="T84" time="46.0" type="appl" />
         <tli id="T85" time="46.5" type="appl" />
         <tli id="T86" time="47.0" type="appl" />
         <tli id="T87" time="47.5" type="appl" />
         <tli id="T88" time="48.0" type="appl" />
         <tli id="T89" time="48.5" type="appl" />
         <tli id="T90" time="49.0" type="appl" />
         <tli id="T91" time="49.5" type="appl" />
         <tli id="T92" time="50.0" type="appl" />
         <tli id="T93" time="50.5" type="appl" />
         <tli id="T94" time="51.0" type="appl" />
         <tli id="T95" time="51.5" type="appl" />
         <tli id="T96" time="52.0" type="appl" />
         <tli id="T97" time="52.5" type="appl" />
         <tli id="T98" time="53.0" type="appl" />
         <tli id="T99" time="53.5" type="appl" />
         <tli id="T100" time="54.0" type="appl" />
         <tli id="T101" time="54.5" type="appl" />
         <tli id="T102" time="55.0" type="appl" />
         <tli id="T103" time="55.5" type="appl" />
         <tli id="T104" time="56.0" type="appl" />
         <tli id="T105" time="56.5" type="appl" />
         <tli id="T106" time="57.0" type="appl" />
         <tli id="T107" time="57.5" type="appl" />
         <tli id="T108" time="58.0" type="appl" />
         <tli id="T109" time="58.5" type="appl" />
         <tli id="T110" time="59.0" type="appl" />
         <tli id="T111" time="59.5" type="appl" />
         <tli id="T112" time="60.0" type="appl" />
         <tli id="T113" time="60.5" type="appl" />
         <tli id="T114" time="61.0" type="appl" />
         <tli id="T115" time="61.5" type="appl" />
         <tli id="T116" time="62.0" type="appl" />
         <tli id="T117" time="62.5" type="appl" />
         <tli id="T118" time="63.0" type="appl" />
         <tli id="T119" time="63.5" type="appl" />
         <tli id="T120" time="64.0" type="appl" />
         <tli id="T121" time="64.5" type="appl" />
         <tli id="T122" time="65.0" type="appl" />
         <tli id="T123" time="65.5" type="appl" />
         <tli id="T124" time="66.0" type="appl" />
         <tli id="T125" time="66.5" type="appl" />
         <tli id="T126" time="67.0" type="appl" />
         <tli id="T127" time="67.5" type="appl" />
         <tli id="T128" time="68.0" type="appl" />
         <tli id="T129" time="68.5" type="appl" />
         <tli id="T130" time="69.0" type="appl" />
         <tli id="T131" time="69.5" type="appl" />
         <tli id="T132" time="70.0" type="appl" />
         <tli id="T133" time="70.5" type="appl" />
         <tli id="T134" time="71.0" type="appl" />
         <tli id="T135" time="71.5" type="appl" />
         <tli id="T136" time="72.0" type="appl" />
         <tli id="T137" time="72.5" type="appl" />
         <tli id="T138" time="73.0" type="appl" />
         <tli id="T139" time="73.5" type="appl" />
         <tli id="T140" time="74.0" type="appl" />
         <tli id="T141" time="74.5" type="appl" />
         <tli id="T142" time="75.0" type="appl" />
         <tli id="T143" time="75.5" type="appl" />
         <tli id="T144" time="76.0" type="appl" />
         <tli id="T145" time="76.5" type="appl" />
         <tli id="T146" time="77.0" type="appl" />
         <tli id="T147" time="77.5" type="appl" />
         <tli id="T148" time="78.0" type="appl" />
         <tli id="T149" time="78.5" type="appl" />
         <tli id="T150" time="79.0" type="appl" />
         <tli id="T151" time="79.5" type="appl" />
         <tli id="T152" time="80.0" type="appl" />
         <tli id="T153" time="80.5" type="appl" />
         <tli id="T154" time="81.0" type="appl" />
         <tli id="T155" time="81.5" type="appl" />
         <tli id="T156" time="82.0" type="appl" />
         <tli id="T157" time="82.5" type="appl" />
         <tli id="T158" time="83.0" type="appl" />
         <tli id="T159" time="83.5" type="appl" />
         <tli id="T160" time="84.0" type="appl" />
         <tli id="T161" time="84.5" type="appl" />
         <tli id="T162" time="85.0" type="appl" />
         <tli id="T163" time="85.5" type="appl" />
         <tli id="T164" time="86.0" type="appl" />
         <tli id="T165" time="86.5" type="appl" />
         <tli id="T166" time="87.0" type="appl" />
         <tli id="T167" time="87.5" type="appl" />
         <tli id="T168" time="88.0" type="appl" />
         <tli id="T169" time="88.5" type="appl" />
         <tli id="T170" time="89.0" type="appl" />
         <tli id="T171" time="89.5" type="appl" />
         <tli id="T172" time="90.0" type="appl" />
         <tli id="T173" time="90.5" type="appl" />
         <tli id="T174" time="91.0" type="appl" />
         <tli id="T175" time="91.5" type="appl" />
         <tli id="T176" time="92.0" type="appl" />
         <tli id="T177" time="92.5" type="appl" />
         <tli id="T178" time="93.0" type="appl" />
         <tli id="T179" time="93.5" type="appl" />
         <tli id="T180" time="94.0" type="appl" />
         <tli id="T181" time="94.5" type="appl" />
         <tli id="T182" time="95.0" type="appl" />
         <tli id="T183" time="95.5" type="appl" />
         <tli id="T184" time="96.0" type="appl" />
         <tli id="T185" time="96.5" type="appl" />
         <tli id="T186" time="97.0" type="appl" />
         <tli id="T187" time="97.5" type="appl" />
         <tli id="T188" time="98.0" type="appl" />
         <tli id="T189" time="98.5" type="appl" />
         <tli id="T190" time="99.0" type="appl" />
         <tli id="T191" time="99.5" type="appl" />
         <tli id="T192" time="100.0" type="appl" />
         <tli id="T193" time="100.5" type="appl" />
         <tli id="T194" time="101.0" type="appl" />
         <tli id="T195" time="101.5" type="appl" />
         <tli id="T196" time="102.0" type="appl" />
         <tli id="T197" time="102.5" type="appl" />
         <tli id="T198" time="103.0" type="appl" />
         <tli id="T199" time="103.5" type="appl" />
         <tli id="T200" time="104.0" type="appl" />
         <tli id="T201" time="104.5" type="appl" />
         <tli id="T202" time="105.0" type="appl" />
         <tli id="T203" time="105.5" type="appl" />
         <tli id="T204" time="106.0" type="appl" />
         <tli id="T205" time="106.5" type="appl" />
         <tli id="T206" time="107.0" type="appl" />
         <tli id="T207" time="107.5" type="appl" />
         <tli id="T208" time="108.0" type="appl" />
         <tli id="T209" time="108.5" type="appl" />
         <tli id="T210" time="109.0" type="appl" />
         <tli id="T211" time="109.5" type="appl" />
         <tli id="T212" time="110.0" type="appl" />
         <tli id="T213" time="110.5" type="appl" />
         <tli id="T214" time="111.0" type="appl" />
         <tli id="T215" time="111.5" type="appl" />
         <tli id="T216" time="112.0" type="appl" />
         <tli id="T217" time="112.5" type="appl" />
         <tli id="T218" time="113.0" type="appl" />
         <tli id="T219" time="113.5" type="appl" />
         <tli id="T220" time="114.0" type="appl" />
         <tli id="T221" time="114.5" type="appl" />
         <tli id="T222" time="115.0" type="appl" />
         <tli id="T223" time="115.5" type="appl" />
         <tli id="T224" time="116.0" type="appl" />
         <tli id="T225" time="116.5" type="appl" />
         <tli id="T226" time="117.0" type="appl" />
         <tli id="T227" time="117.5" type="appl" />
         <tli id="T228" time="118.0" type="appl" />
         <tli id="T229" time="118.5" type="appl" />
         <tli id="T230" time="119.0" type="appl" />
         <tli id="T231" time="119.5" type="appl" />
         <tli id="T232" time="120.0" type="appl" />
         <tli id="T233" time="120.5" type="appl" />
         <tli id="T234" time="121.0" type="appl" />
         <tli id="T235" time="121.5" type="appl" />
         <tli id="T236" time="122.0" type="appl" />
         <tli id="T237" time="122.5" type="appl" />
         <tli id="T238" time="123.0" type="appl" />
         <tli id="T239" time="123.5" type="appl" />
         <tli id="T240" time="124.0" type="appl" />
         <tli id="T241" time="124.5" type="appl" />
         <tli id="T242" time="125.0" type="appl" />
         <tli id="T243" time="125.5" type="appl" />
         <tli id="T244" time="126.0" type="appl" />
         <tli id="T245" time="126.5" type="appl" />
         <tli id="T246" time="127.0" type="appl" />
         <tli id="T247" time="127.5" type="appl" />
         <tli id="T248" time="128.0" type="appl" />
         <tli id="T249" time="128.5" type="appl" />
         <tli id="T250" time="129.0" type="appl" />
         <tli id="T251" time="129.5" type="appl" />
         <tli id="T252" time="130.0" type="appl" />
         <tli id="T253" time="130.5" type="appl" />
         <tli id="T254" time="131.0" type="appl" />
         <tli id="T255" time="131.5" type="appl" />
         <tli id="T256" time="132.0" type="appl" />
         <tli id="T257" time="132.5" type="appl" />
         <tli id="T258" time="133.0" type="appl" />
         <tli id="T259" time="133.5" type="appl" />
         <tli id="T260" time="134.0" type="appl" />
         <tli id="T261" time="134.5" type="appl" />
         <tli id="T262" time="135.0" type="appl" />
         <tli id="T263" time="135.5" type="appl" />
         <tli id="T264" time="136.0" type="appl" />
         <tli id="T265" time="136.5" type="appl" />
         <tli id="T266" time="137.0" type="appl" />
         <tli id="T267" time="137.5" type="appl" />
         <tli id="T268" time="138.0" type="appl" />
         <tli id="T269" time="138.5" type="appl" />
         <tli id="T270" time="139.0" type="appl" />
         <tli id="T271" time="139.5" type="appl" />
         <tli id="T272" time="140.0" type="appl" />
         <tli id="T273" time="140.5" type="appl" />
         <tli id="T274" time="141.0" type="appl" />
         <tli id="T275" time="141.5" type="appl" />
         <tli id="T276" time="142.0" type="appl" />
         <tli id="T277" time="142.5" type="appl" />
         <tli id="T278" time="143.0" type="appl" />
         <tli id="T279" time="143.5" type="appl" />
         <tli id="T280" time="144.0" type="appl" />
         <tli id="T281" time="144.5" type="appl" />
         <tli id="T282" time="145.0" type="appl" />
         <tli id="T283" time="145.5" type="appl" />
         <tli id="T284" time="146.0" type="appl" />
         <tli id="T285" time="146.5" type="appl" />
         <tli id="T286" time="147.0" type="appl" />
         <tli id="T287" time="147.5" type="appl" />
         <tli id="T288" time="148.0" type="appl" />
         <tli id="T289" time="148.5" type="appl" />
         <tli id="T290" time="149.0" type="appl" />
         <tli id="T291" time="149.5" type="appl" />
         <tli id="T292" time="150.0" type="appl" />
         <tli id="T293" time="150.5" type="appl" />
         <tli id="T294" time="151.0" type="appl" />
         <tli id="T295" time="151.5" type="appl" />
         <tli id="T296" time="152.0" type="appl" />
         <tli id="T297" time="152.5" type="appl" />
         <tli id="T298" time="153.0" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="BaA"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T298" id="Seg_0" n="sc" s="T0">
               <ts e="T10" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Tɨ͡a</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">hiriger</ts>
                  <nts id="Seg_8" n="HIAT:ip">,</nts>
                  <nts id="Seg_9" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_11" n="HIAT:w" s="T2">kömükteːk</ts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_14" n="HIAT:w" s="T3">bagajɨ</ts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_17" n="HIAT:w" s="T4">hirge</ts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_20" n="HIAT:w" s="T5">bɨlɨrgɨ</ts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_23" n="HIAT:w" s="T6">kaːlbɨt</ts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_26" n="HIAT:w" s="T7">očiːhakaːn</ts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_29" n="HIAT:w" s="T8">baːra</ts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_32" n="HIAT:w" s="T9">ebit</ts>
                  <nts id="Seg_33" n="HIAT:ip">.</nts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_36" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_38" n="HIAT:w" s="T10">Ol</ts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_41" n="HIAT:w" s="T11">očihakaːŋŋa</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_44" n="HIAT:w" s="T12">ki͡ehe</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_47" n="HIAT:w" s="T13">aːjɨ</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_50" n="HIAT:w" s="T14">u͡ot</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_53" n="HIAT:w" s="T15">ubajar</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_56" n="HIAT:w" s="T16">bu͡olbut</ts>
                  <nts id="Seg_57" n="HIAT:ip">.</nts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T23" id="Seg_60" n="HIAT:u" s="T17">
                  <ts e="T18" id="Seg_62" n="HIAT:w" s="T17">Manna</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_65" n="HIAT:w" s="T18">baːr</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_68" n="HIAT:w" s="T19">ebit</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_71" n="HIAT:w" s="T20">biːr</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_74" n="HIAT:w" s="T21">kordʼon</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_77" n="HIAT:w" s="T22">kihi</ts>
                  <nts id="Seg_78" n="HIAT:ip">.</nts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T27" id="Seg_81" n="HIAT:u" s="T23">
                  <ts e="T24" id="Seg_83" n="HIAT:w" s="T23">Kupi͡eska</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_86" n="HIAT:w" s="T24">orobotnik</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_89" n="HIAT:w" s="T25">bu͡ola</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_92" n="HIAT:w" s="T26">hɨldʼar</ts>
                  <nts id="Seg_93" n="HIAT:ip">.</nts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T28" id="Seg_96" n="HIAT:u" s="T27">
                  <ts e="T28" id="Seg_98" n="HIAT:w" s="T27">Dʼonnor</ts>
                  <nts id="Seg_99" n="HIAT:ip">:</nts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T35" id="Seg_102" n="HIAT:u" s="T28">
                  <nts id="Seg_103" n="HIAT:ip">"</nts>
                  <ts e="T29" id="Seg_105" n="HIAT:w" s="T28">Tu͡ok</ts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_108" n="HIAT:w" s="T29">u͡ota</ts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_111" n="HIAT:w" s="T30">ubajara</ts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_114" n="HIAT:w" s="T31">bu͡olla</ts>
                  <nts id="Seg_115" n="HIAT:ip">"</nts>
                  <nts id="Seg_116" n="HIAT:ip">,</nts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_119" n="HIAT:w" s="T32">di͡en</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_122" n="HIAT:w" s="T33">högön</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_125" n="HIAT:w" s="T34">kepseteller</ts>
                  <nts id="Seg_126" n="HIAT:ip">.</nts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T41" id="Seg_129" n="HIAT:u" s="T35">
                  <ts e="T36" id="Seg_131" n="HIAT:w" s="T35">Manɨ͡aga</ts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_134" n="HIAT:w" s="T36">maː</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_137" n="HIAT:w" s="T37">börköj</ts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_140" n="HIAT:w" s="T38">kihiŋ</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_143" n="HIAT:w" s="T39">eter</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_146" n="HIAT:w" s="T40">kupi͡ehɨgar</ts>
                  <nts id="Seg_147" n="HIAT:ip">:</nts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T44" id="Seg_150" n="HIAT:u" s="T41">
                  <nts id="Seg_151" n="HIAT:ip">"</nts>
                  <ts e="T42" id="Seg_153" n="HIAT:w" s="T41">Dʼe</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_156" n="HIAT:w" s="T42">zaklaːttahɨ͡ak</ts>
                  <nts id="Seg_157" n="HIAT:ip">"</nts>
                  <nts id="Seg_158" n="HIAT:ip">,</nts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_161" n="HIAT:w" s="T43">diːr</ts>
                  <nts id="Seg_162" n="HIAT:ip">.</nts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T48" id="Seg_165" n="HIAT:u" s="T44">
                  <nts id="Seg_166" n="HIAT:ip">"</nts>
                  <ts e="T45" id="Seg_168" n="HIAT:w" s="T44">Min</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_171" n="HIAT:w" s="T45">baran</ts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_174" n="HIAT:w" s="T46">bilen</ts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_177" n="HIAT:w" s="T47">keli͡em</ts>
                  <nts id="Seg_178" n="HIAT:ip">.</nts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T57" id="Seg_181" n="HIAT:u" s="T48">
                  <ts e="T49" id="Seg_183" n="HIAT:w" s="T48">Min</ts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_186" n="HIAT:w" s="T49">kottordokpuna</ts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_189" n="HIAT:w" s="T50">bi͡ek</ts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_192" n="HIAT:w" s="T51">bosko</ts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_195" n="HIAT:w" s="T52">üleli͡em</ts>
                  <nts id="Seg_196" n="HIAT:ip">,</nts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_199" n="HIAT:w" s="T53">en</ts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_202" n="HIAT:w" s="T54">bu͡ollagɨna</ts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_205" n="HIAT:w" s="T55">karčɨnɨ</ts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_208" n="HIAT:w" s="T56">bi͡eri͡eŋ</ts>
                  <nts id="Seg_209" n="HIAT:ip">.</nts>
                  <nts id="Seg_210" n="HIAT:ip">"</nts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T60" id="Seg_213" n="HIAT:u" s="T57">
                  <ts e="T58" id="Seg_215" n="HIAT:w" s="T57">Onon</ts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_218" n="HIAT:w" s="T58">höbülehen</ts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_221" n="HIAT:w" s="T59">kaːlsallar</ts>
                  <nts id="Seg_222" n="HIAT:ip">.</nts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T65" id="Seg_225" n="HIAT:u" s="T60">
                  <ts e="T61" id="Seg_227" n="HIAT:w" s="T60">Maː</ts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_230" n="HIAT:w" s="T61">kihiŋ</ts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_233" n="HIAT:w" s="T62">ki͡ehe</ts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_236" n="HIAT:w" s="T63">ɨtɨnan</ts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_239" n="HIAT:w" s="T64">barar</ts>
                  <nts id="Seg_240" n="HIAT:ip">.</nts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T69" id="Seg_243" n="HIAT:u" s="T65">
                  <ts e="T66" id="Seg_245" n="HIAT:w" s="T65">Bu</ts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_248" n="HIAT:w" s="T66">baran</ts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_251" n="HIAT:w" s="T67">očiːhaga</ts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_254" n="HIAT:w" s="T68">kiːrer</ts>
                  <nts id="Seg_255" n="HIAT:ip">.</nts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T75" id="Seg_258" n="HIAT:u" s="T69">
                  <ts e="T70" id="Seg_260" n="HIAT:w" s="T69">Bu</ts>
                  <nts id="Seg_261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_263" n="HIAT:w" s="T70">kiːrbite</ts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_266" n="HIAT:w" s="T71">tüːleːk</ts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_269" n="HIAT:w" s="T72">hɨrajdaːk</ts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_272" n="HIAT:w" s="T73">kihi</ts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_275" n="HIAT:w" s="T74">oloror</ts>
                  <nts id="Seg_276" n="HIAT:ip">.</nts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T80" id="Seg_279" n="HIAT:u" s="T75">
                  <ts e="T76" id="Seg_281" n="HIAT:w" s="T75">Manna</ts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_284" n="HIAT:w" s="T76">kɨbɨsta-kɨbɨsta</ts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_287" n="HIAT:w" s="T77">börköjüŋ</ts>
                  <nts id="Seg_288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_290" n="HIAT:w" s="T78">oloror</ts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_293" n="HIAT:w" s="T79">ɨskaːmɨjaga</ts>
                  <nts id="Seg_294" n="HIAT:ip">.</nts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T86" id="Seg_297" n="HIAT:u" s="T80">
                  <ts e="T81" id="Seg_299" n="HIAT:w" s="T80">Tüːleːk</ts>
                  <nts id="Seg_300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_302" n="HIAT:w" s="T81">kihite</ts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_305" n="HIAT:w" s="T82">kü͡östenen</ts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_308" n="HIAT:w" s="T83">ahaːrɨ</ts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_311" n="HIAT:w" s="T84">oloror</ts>
                  <nts id="Seg_312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_314" n="HIAT:w" s="T85">ebit</ts>
                  <nts id="Seg_315" n="HIAT:ip">.</nts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T89" id="Seg_318" n="HIAT:u" s="T86">
                  <ts e="T87" id="Seg_320" n="HIAT:w" s="T86">Dʼe</ts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_323" n="HIAT:w" s="T87">kü͡öhün</ts>
                  <nts id="Seg_324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_326" n="HIAT:w" s="T88">kotorunan</ts>
                  <nts id="Seg_327" n="HIAT:ip">:</nts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T91" id="Seg_330" n="HIAT:u" s="T89">
                  <nts id="Seg_331" n="HIAT:ip">"</nts>
                  <ts e="T90" id="Seg_333" n="HIAT:w" s="T89">Ahaː</ts>
                  <nts id="Seg_334" n="HIAT:ip">"</nts>
                  <nts id="Seg_335" n="HIAT:ip">,</nts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_338" n="HIAT:w" s="T90">di͡ebit</ts>
                  <nts id="Seg_339" n="HIAT:ip">.</nts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T96" id="Seg_342" n="HIAT:u" s="T91">
                  <ts e="T92" id="Seg_344" n="HIAT:w" s="T91">Manɨ</ts>
                  <nts id="Seg_345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_347" n="HIAT:w" s="T92">ahaːn</ts>
                  <nts id="Seg_348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_350" n="HIAT:w" s="T93">baran</ts>
                  <nts id="Seg_351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_353" n="HIAT:w" s="T94">utujaːrɨ</ts>
                  <nts id="Seg_354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_356" n="HIAT:w" s="T95">hɨppɨttar</ts>
                  <nts id="Seg_357" n="HIAT:ip">.</nts>
                  <nts id="Seg_358" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T104" id="Seg_360" n="HIAT:u" s="T96">
                  <ts e="T97" id="Seg_362" n="HIAT:w" s="T96">Bu</ts>
                  <nts id="Seg_363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_365" n="HIAT:w" s="T97">hɨttagɨna</ts>
                  <nts id="Seg_366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_368" n="HIAT:w" s="T98">harsɨ͡arda</ts>
                  <nts id="Seg_369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_371" n="HIAT:w" s="T99">erde</ts>
                  <nts id="Seg_372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_374" n="HIAT:w" s="T100">kihite</ts>
                  <nts id="Seg_375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_377" n="HIAT:w" s="T101">kiŋilenen</ts>
                  <nts id="Seg_378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_380" n="HIAT:w" s="T102">baran</ts>
                  <nts id="Seg_381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_383" n="HIAT:w" s="T103">kaːlar</ts>
                  <nts id="Seg_384" n="HIAT:ip">.</nts>
                  <nts id="Seg_385" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T110" id="Seg_387" n="HIAT:u" s="T104">
                  <ts e="T105" id="Seg_389" n="HIAT:w" s="T104">Kordʼon</ts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_392" n="HIAT:w" s="T105">kihibit</ts>
                  <nts id="Seg_393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_395" n="HIAT:w" s="T106">turar</ts>
                  <nts id="Seg_396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_398" n="HIAT:w" s="T107">daː</ts>
                  <nts id="Seg_399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_401" n="HIAT:w" s="T108">hanɨː</ts>
                  <nts id="Seg_402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_404" n="HIAT:w" s="T109">bi͡erer</ts>
                  <nts id="Seg_405" n="HIAT:ip">:</nts>
                  <nts id="Seg_406" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T116" id="Seg_408" n="HIAT:u" s="T110">
                  <nts id="Seg_409" n="HIAT:ip">"</nts>
                  <ts e="T111" id="Seg_411" n="HIAT:w" s="T110">Kajdak</ts>
                  <nts id="Seg_412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_414" n="HIAT:w" s="T111">min</ts>
                  <nts id="Seg_415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_417" n="HIAT:w" s="T112">manna</ts>
                  <nts id="Seg_418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_420" n="HIAT:w" s="T113">hɨldʼɨbɨppɨn</ts>
                  <nts id="Seg_421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_423" n="HIAT:w" s="T114">billeri͡emij</ts>
                  <nts id="Seg_424" n="HIAT:ip">"</nts>
                  <nts id="Seg_425" n="HIAT:ip">,</nts>
                  <nts id="Seg_426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_428" n="HIAT:w" s="T115">diːr</ts>
                  <nts id="Seg_429" n="HIAT:ip">.</nts>
                  <nts id="Seg_430" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T132" id="Seg_432" n="HIAT:u" s="T116">
                  <nts id="Seg_433" n="HIAT:ip">"</nts>
                  <ts e="T117" id="Seg_435" n="HIAT:w" s="T116">Bu</ts>
                  <nts id="Seg_436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_438" n="HIAT:w" s="T117">kihi</ts>
                  <nts id="Seg_439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_441" n="HIAT:w" s="T118">kanna</ts>
                  <nts id="Seg_442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_444" n="HIAT:w" s="T119">barbɨtɨn</ts>
                  <nts id="Seg_445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_447" n="HIAT:w" s="T120">bilen</ts>
                  <nts id="Seg_448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_450" n="HIAT:w" s="T121">beli͡e</ts>
                  <nts id="Seg_451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_453" n="HIAT:w" s="T122">egeli͡ekteːkpin</ts>
                  <nts id="Seg_454" n="HIAT:ip">"</nts>
                  <nts id="Seg_455" n="HIAT:ip">,</nts>
                  <nts id="Seg_456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_458" n="HIAT:w" s="T123">di͡en</ts>
                  <nts id="Seg_459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_461" n="HIAT:w" s="T124">orogun</ts>
                  <nts id="Seg_462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_464" n="HIAT:w" s="T125">batan</ts>
                  <nts id="Seg_465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_467" n="HIAT:w" s="T126">baran</ts>
                  <nts id="Seg_468" n="HIAT:ip">,</nts>
                  <nts id="Seg_469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_471" n="HIAT:w" s="T127">bu</ts>
                  <nts id="Seg_472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_474" n="HIAT:w" s="T128">baran</ts>
                  <nts id="Seg_475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_477" n="HIAT:w" s="T129">taːs</ts>
                  <nts id="Seg_478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_480" n="HIAT:w" s="T130">kajaga</ts>
                  <nts id="Seg_481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_483" n="HIAT:w" s="T131">tijer</ts>
                  <nts id="Seg_484" n="HIAT:ip">.</nts>
                  <nts id="Seg_485" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T140" id="Seg_487" n="HIAT:u" s="T132">
                  <ts e="T133" id="Seg_489" n="HIAT:w" s="T132">Manna</ts>
                  <nts id="Seg_490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_492" n="HIAT:w" s="T133">kistenen</ts>
                  <nts id="Seg_493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_495" n="HIAT:w" s="T134">kelbite</ts>
                  <nts id="Seg_496" n="HIAT:ip">,</nts>
                  <nts id="Seg_497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_499" n="HIAT:w" s="T135">bu</ts>
                  <nts id="Seg_500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_502" n="HIAT:w" s="T136">taːs</ts>
                  <nts id="Seg_503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_505" n="HIAT:w" s="T137">kaja</ts>
                  <nts id="Seg_506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_508" n="HIAT:w" s="T138">barɨta</ts>
                  <nts id="Seg_509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_511" n="HIAT:w" s="T139">kahɨllɨbɨt</ts>
                  <nts id="Seg_512" n="HIAT:ip">.</nts>
                  <nts id="Seg_513" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T152" id="Seg_515" n="HIAT:u" s="T140">
                  <ts e="T141" id="Seg_517" n="HIAT:w" s="T140">Dʼon</ts>
                  <nts id="Seg_518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_520" n="HIAT:w" s="T141">köːküne</ts>
                  <nts id="Seg_521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_523" n="HIAT:w" s="T142">di͡en</ts>
                  <nts id="Seg_524" n="HIAT:ip">,</nts>
                  <nts id="Seg_525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_527" n="HIAT:w" s="T143">haŋalara</ts>
                  <nts id="Seg_528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_530" n="HIAT:w" s="T144">di͡en</ts>
                  <nts id="Seg_531" n="HIAT:ip">,</nts>
                  <nts id="Seg_532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_534" n="HIAT:w" s="T145">öŋöjön</ts>
                  <nts id="Seg_535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_537" n="HIAT:w" s="T146">körbüte</ts>
                  <nts id="Seg_538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_540" n="HIAT:w" s="T147">kajagahɨnan</ts>
                  <nts id="Seg_541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_543" n="HIAT:w" s="T148">kihilere</ts>
                  <nts id="Seg_544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_546" n="HIAT:w" s="T149">barɨta</ts>
                  <nts id="Seg_547" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_549" n="HIAT:w" s="T150">tüːleːk</ts>
                  <nts id="Seg_550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_552" n="HIAT:w" s="T151">hirejdeːk</ts>
                  <nts id="Seg_553" n="HIAT:ip">.</nts>
                  <nts id="Seg_554" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T155" id="Seg_556" n="HIAT:u" s="T152">
                  <ts e="T153" id="Seg_558" n="HIAT:w" s="T152">Manna</ts>
                  <nts id="Seg_559" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_561" n="HIAT:w" s="T153">ü͡ömen</ts>
                  <nts id="Seg_562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_564" n="HIAT:w" s="T154">kiːrbit</ts>
                  <nts id="Seg_565" n="HIAT:ip">.</nts>
                  <nts id="Seg_566" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T162" id="Seg_568" n="HIAT:u" s="T155">
                  <ts e="T156" id="Seg_570" n="HIAT:w" s="T155">Bu</ts>
                  <nts id="Seg_571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_573" n="HIAT:w" s="T156">kiːren</ts>
                  <nts id="Seg_574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_576" n="HIAT:w" s="T157">bu</ts>
                  <nts id="Seg_577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_579" n="HIAT:w" s="T158">dʼi͡ege</ts>
                  <nts id="Seg_580" n="HIAT:ip">,</nts>
                  <nts id="Seg_581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_583" n="HIAT:w" s="T159">balagaŋŋa</ts>
                  <nts id="Seg_584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_586" n="HIAT:w" s="T160">kiːren</ts>
                  <nts id="Seg_587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_589" n="HIAT:w" s="T161">oloror</ts>
                  <nts id="Seg_590" n="HIAT:ip">.</nts>
                  <nts id="Seg_591" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T168" id="Seg_593" n="HIAT:u" s="T162">
                  <ts e="T163" id="Seg_595" n="HIAT:w" s="T162">Bu</ts>
                  <nts id="Seg_596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_598" n="HIAT:w" s="T163">olorbuta</ts>
                  <nts id="Seg_599" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_601" n="HIAT:w" s="T164">biːr</ts>
                  <nts id="Seg_602" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_604" n="HIAT:w" s="T165">dʼaktar</ts>
                  <nts id="Seg_605" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_607" n="HIAT:w" s="T166">baːr</ts>
                  <nts id="Seg_608" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_610" n="HIAT:w" s="T167">ebit</ts>
                  <nts id="Seg_611" n="HIAT:ip">.</nts>
                  <nts id="Seg_612" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T171" id="Seg_614" n="HIAT:u" s="T168">
                  <ts e="T169" id="Seg_616" n="HIAT:w" s="T168">Ol</ts>
                  <nts id="Seg_617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_619" n="HIAT:w" s="T169">orgujkaːn</ts>
                  <nts id="Seg_620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_622" n="HIAT:w" s="T170">ɨjɨtar</ts>
                  <nts id="Seg_623" n="HIAT:ip">:</nts>
                  <nts id="Seg_624" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T176" id="Seg_626" n="HIAT:u" s="T171">
                  <nts id="Seg_627" n="HIAT:ip">"</nts>
                  <ts e="T172" id="Seg_629" n="HIAT:w" s="T171">Kaja</ts>
                  <nts id="Seg_630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_632" n="HIAT:w" s="T172">tugunan</ts>
                  <nts id="Seg_633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_635" n="HIAT:w" s="T173">naːdɨjan</ts>
                  <nts id="Seg_636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_638" n="HIAT:w" s="T174">kelliŋ</ts>
                  <nts id="Seg_639" n="HIAT:ip">"</nts>
                  <nts id="Seg_640" n="HIAT:ip">,</nts>
                  <nts id="Seg_641" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_643" n="HIAT:w" s="T175">diːr</ts>
                  <nts id="Seg_644" n="HIAT:ip">.</nts>
                  <nts id="Seg_645" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T178" id="Seg_647" n="HIAT:u" s="T176">
                  <ts e="T177" id="Seg_649" n="HIAT:w" s="T176">Manɨ</ts>
                  <nts id="Seg_650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_652" n="HIAT:w" s="T177">eter</ts>
                  <nts id="Seg_653" n="HIAT:ip">:</nts>
                  <nts id="Seg_654" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T181" id="Seg_656" n="HIAT:u" s="T178">
                  <nts id="Seg_657" n="HIAT:ip">"</nts>
                  <ts e="T179" id="Seg_659" n="HIAT:w" s="T178">Hinnʼiges</ts>
                  <nts id="Seg_660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_662" n="HIAT:w" s="T179">hirbinen</ts>
                  <nts id="Seg_663" n="HIAT:ip">"</nts>
                  <nts id="Seg_664" n="HIAT:ip">,</nts>
                  <nts id="Seg_665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_667" n="HIAT:w" s="T180">diːr</ts>
                  <nts id="Seg_668" n="HIAT:ip">.</nts>
                  <nts id="Seg_669" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T190" id="Seg_671" n="HIAT:u" s="T181">
                  <ts e="T182" id="Seg_673" n="HIAT:w" s="T181">Onu͡oka</ts>
                  <nts id="Seg_674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_676" n="HIAT:w" s="T182">maː</ts>
                  <nts id="Seg_677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_679" n="HIAT:w" s="T183">dʼaktara</ts>
                  <nts id="Seg_680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_682" n="HIAT:w" s="T184">tugu</ts>
                  <nts id="Seg_683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_685" n="HIAT:w" s="T185">ere</ts>
                  <nts id="Seg_686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_688" n="HIAT:w" s="T186">hɨmnagas</ts>
                  <nts id="Seg_689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_691" n="HIAT:w" s="T187">bagajɨnɨ</ts>
                  <nts id="Seg_692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_694" n="HIAT:w" s="T188">tuttaran</ts>
                  <nts id="Seg_695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_697" n="HIAT:w" s="T189">keːher</ts>
                  <nts id="Seg_698" n="HIAT:ip">.</nts>
                  <nts id="Seg_699" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T198" id="Seg_701" n="HIAT:u" s="T190">
                  <ts e="T191" id="Seg_703" n="HIAT:w" s="T190">Onu</ts>
                  <nts id="Seg_704" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_706" n="HIAT:w" s="T191">körümne</ts>
                  <nts id="Seg_707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_709" n="HIAT:w" s="T192">ere</ts>
                  <nts id="Seg_710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_712" n="HIAT:w" s="T193">hi͡ebiger</ts>
                  <nts id="Seg_713" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_715" n="HIAT:w" s="T194">uktan</ts>
                  <nts id="Seg_716" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_718" n="HIAT:w" s="T195">keːher</ts>
                  <nts id="Seg_719" n="HIAT:ip">,</nts>
                  <nts id="Seg_720" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_722" n="HIAT:w" s="T196">dʼaktara</ts>
                  <nts id="Seg_723" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_725" n="HIAT:w" s="T197">eter</ts>
                  <nts id="Seg_726" n="HIAT:ip">:</nts>
                  <nts id="Seg_727" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T208" id="Seg_729" n="HIAT:u" s="T198">
                  <nts id="Seg_730" n="HIAT:ip">"</nts>
                  <ts e="T199" id="Seg_732" n="HIAT:w" s="T198">Dʼe</ts>
                  <nts id="Seg_733" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_735" n="HIAT:w" s="T199">kɨtaːt</ts>
                  <nts id="Seg_736" n="HIAT:ip">,</nts>
                  <nts id="Seg_737" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_739" n="HIAT:w" s="T200">baran</ts>
                  <nts id="Seg_740" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_742" n="HIAT:w" s="T201">kör</ts>
                  <nts id="Seg_743" n="HIAT:ip">,</nts>
                  <nts id="Seg_744" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_746" n="HIAT:w" s="T202">manna</ts>
                  <nts id="Seg_747" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_749" n="HIAT:w" s="T203">dʼonnorum</ts>
                  <nts id="Seg_750" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_752" n="HIAT:w" s="T204">kördöktörüne</ts>
                  <nts id="Seg_753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_755" n="HIAT:w" s="T205">kihili͡ektere</ts>
                  <nts id="Seg_756" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_758" n="HIAT:w" s="T206">hu͡oga</ts>
                  <nts id="Seg_759" n="HIAT:ip">"</nts>
                  <nts id="Seg_760" n="HIAT:ip">,</nts>
                  <nts id="Seg_761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_763" n="HIAT:w" s="T207">di͡ebit</ts>
                  <nts id="Seg_764" n="HIAT:ip">.</nts>
                  <nts id="Seg_765" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T218" id="Seg_767" n="HIAT:u" s="T208">
                  <ts e="T209" id="Seg_769" n="HIAT:w" s="T208">Onu</ts>
                  <nts id="Seg_770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_772" n="HIAT:w" s="T209">isteːt</ts>
                  <nts id="Seg_773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_775" n="HIAT:w" s="T210">kordʼon</ts>
                  <nts id="Seg_776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_778" n="HIAT:w" s="T211">kihi</ts>
                  <nts id="Seg_779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_781" n="HIAT:w" s="T212">taksɨbɨt</ts>
                  <nts id="Seg_782" n="HIAT:ip">,</nts>
                  <nts id="Seg_783" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_785" n="HIAT:w" s="T213">ɨtɨgar</ts>
                  <nts id="Seg_786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_788" n="HIAT:w" s="T214">olorbut</ts>
                  <nts id="Seg_789" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_791" n="HIAT:w" s="T215">daː</ts>
                  <nts id="Seg_792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_794" n="HIAT:w" s="T216">bararga</ts>
                  <nts id="Seg_795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_797" n="HIAT:w" s="T217">bu͡olbut</ts>
                  <nts id="Seg_798" n="HIAT:ip">.</nts>
                  <nts id="Seg_799" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T227" id="Seg_801" n="HIAT:u" s="T218">
                  <ts e="T219" id="Seg_803" n="HIAT:w" s="T218">Bu</ts>
                  <nts id="Seg_804" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_806" n="HIAT:w" s="T219">baran</ts>
                  <nts id="Seg_807" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_809" n="HIAT:w" s="T220">ihen</ts>
                  <nts id="Seg_810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_812" n="HIAT:w" s="T221">kennin</ts>
                  <nts id="Seg_813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_815" n="HIAT:w" s="T222">körbüte</ts>
                  <nts id="Seg_816" n="HIAT:ip">,</nts>
                  <nts id="Seg_817" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_819" n="HIAT:w" s="T223">biːr</ts>
                  <nts id="Seg_820" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_822" n="HIAT:w" s="T224">kihi</ts>
                  <nts id="Seg_823" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_825" n="HIAT:w" s="T225">batɨhan</ts>
                  <nts id="Seg_826" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_828" n="HIAT:w" s="T226">iher</ts>
                  <nts id="Seg_829" n="HIAT:ip">.</nts>
                  <nts id="Seg_830" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T234" id="Seg_832" n="HIAT:u" s="T227">
                  <ts e="T228" id="Seg_834" n="HIAT:w" s="T227">Ontuta</ts>
                  <nts id="Seg_835" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_837" n="HIAT:w" s="T228">hiten</ts>
                  <nts id="Seg_838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_840" n="HIAT:w" s="T229">iherin</ts>
                  <nts id="Seg_841" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_843" n="HIAT:w" s="T230">körön</ts>
                  <nts id="Seg_844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_846" n="HIAT:w" s="T231">maː</ts>
                  <nts id="Seg_847" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_849" n="HIAT:w" s="T232">kihi</ts>
                  <nts id="Seg_850" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_852" n="HIAT:w" s="T233">kötüppüt</ts>
                  <nts id="Seg_853" n="HIAT:ip">.</nts>
                  <nts id="Seg_854" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T244" id="Seg_856" n="HIAT:u" s="T234">
                  <ts e="T235" id="Seg_858" n="HIAT:w" s="T234">Tüːleːk</ts>
                  <nts id="Seg_859" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_861" n="HIAT:w" s="T235">hɨrajdaːk</ts>
                  <nts id="Seg_862" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_864" n="HIAT:w" s="T236">kihite</ts>
                  <nts id="Seg_865" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_867" n="HIAT:w" s="T237">hɨrgatɨn</ts>
                  <nts id="Seg_868" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_870" n="HIAT:w" s="T238">kelin</ts>
                  <nts id="Seg_871" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_873" n="HIAT:w" s="T239">atagɨttan</ts>
                  <nts id="Seg_874" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_876" n="HIAT:w" s="T240">ɨlan</ts>
                  <nts id="Seg_877" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_879" n="HIAT:w" s="T241">ikki</ts>
                  <nts id="Seg_880" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_882" n="HIAT:w" s="T242">atagɨn</ts>
                  <nts id="Seg_883" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_885" n="HIAT:w" s="T243">kaːllarbɨt</ts>
                  <nts id="Seg_886" n="HIAT:ip">.</nts>
                  <nts id="Seg_887" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T250" id="Seg_889" n="HIAT:u" s="T244">
                  <nts id="Seg_890" n="HIAT:ip">"</nts>
                  <ts e="T245" id="Seg_892" n="HIAT:w" s="T244">Tübestergin</ts>
                  <nts id="Seg_893" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_895" n="HIAT:w" s="T245">kördörü͡ök</ts>
                  <nts id="Seg_896" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_898" n="HIAT:w" s="T246">etim</ts>
                  <nts id="Seg_899" n="HIAT:ip">"</nts>
                  <nts id="Seg_900" n="HIAT:ip">,</nts>
                  <nts id="Seg_901" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_903" n="HIAT:w" s="T247">di͡en</ts>
                  <nts id="Seg_904" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_906" n="HIAT:w" s="T248">kahɨːtaːn</ts>
                  <nts id="Seg_907" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_909" n="HIAT:w" s="T249">kaːlbɨt</ts>
                  <nts id="Seg_910" n="HIAT:ip">.</nts>
                  <nts id="Seg_911" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T263" id="Seg_913" n="HIAT:u" s="T250">
                  <ts e="T251" id="Seg_915" n="HIAT:w" s="T250">Maː</ts>
                  <nts id="Seg_916" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_918" n="HIAT:w" s="T251">u͡ol</ts>
                  <nts id="Seg_919" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_921" n="HIAT:w" s="T252">dʼi͡etiger</ts>
                  <nts id="Seg_922" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_924" n="HIAT:w" s="T253">kelen</ts>
                  <nts id="Seg_925" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_926" n="HIAT:ip">"</nts>
                  <ts e="T255" id="Seg_928" n="HIAT:w" s="T254">beli͡e</ts>
                  <nts id="Seg_929" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_931" n="HIAT:w" s="T255">egellim</ts>
                  <nts id="Seg_932" n="HIAT:ip">"</nts>
                  <nts id="Seg_933" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_935" n="HIAT:w" s="T256">di͡en</ts>
                  <nts id="Seg_936" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_938" n="HIAT:w" s="T257">karmaːnɨttan</ts>
                  <nts id="Seg_939" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_941" n="HIAT:w" s="T258">hɨmnagahɨn</ts>
                  <nts id="Seg_942" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_944" n="HIAT:w" s="T259">tahaːrbɨta</ts>
                  <nts id="Seg_945" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_946" n="HIAT:ip">—</nts>
                  <nts id="Seg_947" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_949" n="HIAT:w" s="T260">kara</ts>
                  <nts id="Seg_950" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_952" n="HIAT:w" s="T261">hahɨl</ts>
                  <nts id="Seg_953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_955" n="HIAT:w" s="T262">bu͡olbut</ts>
                  <nts id="Seg_956" n="HIAT:ip">.</nts>
                  <nts id="Seg_957" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T268" id="Seg_959" n="HIAT:u" s="T263">
                  <ts e="T264" id="Seg_961" n="HIAT:w" s="T263">Kupi͡eha</ts>
                  <nts id="Seg_962" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_964" n="HIAT:w" s="T264">bu͡ollagɨna</ts>
                  <nts id="Seg_965" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_967" n="HIAT:w" s="T265">karčɨ</ts>
                  <nts id="Seg_968" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_970" n="HIAT:w" s="T266">bögö</ts>
                  <nts id="Seg_971" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_973" n="HIAT:w" s="T267">bi͡erbit</ts>
                  <nts id="Seg_974" n="HIAT:ip">.</nts>
                  <nts id="Seg_975" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T272" id="Seg_977" n="HIAT:u" s="T268">
                  <ts e="T269" id="Seg_979" n="HIAT:w" s="T268">Onon</ts>
                  <nts id="Seg_980" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_982" n="HIAT:w" s="T269">baːj</ts>
                  <nts id="Seg_983" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_985" n="HIAT:w" s="T270">kihi</ts>
                  <nts id="Seg_986" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_988" n="HIAT:w" s="T271">bu͡olbut</ts>
                  <nts id="Seg_989" n="HIAT:ip">.</nts>
                  <nts id="Seg_990" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T275" id="Seg_992" n="HIAT:u" s="T272">
                  <ts e="T273" id="Seg_994" n="HIAT:w" s="T272">Manɨ</ts>
                  <nts id="Seg_995" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_997" n="HIAT:w" s="T273">kɨrdʼagastar</ts>
                  <nts id="Seg_998" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_1000" n="HIAT:w" s="T274">eppitter</ts>
                  <nts id="Seg_1001" n="HIAT:ip">:</nts>
                  <nts id="Seg_1002" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T289" id="Seg_1004" n="HIAT:u" s="T275">
                  <nts id="Seg_1005" n="HIAT:ip">"</nts>
                  <ts e="T276" id="Seg_1007" n="HIAT:w" s="T275">En</ts>
                  <nts id="Seg_1008" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_1010" n="HIAT:w" s="T276">tije</ts>
                  <nts id="Seg_1011" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_1013" n="HIAT:w" s="T277">hɨldʼɨbɨkkɨn</ts>
                  <nts id="Seg_1014" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_1016" n="HIAT:w" s="T278">bɨlɨr</ts>
                  <nts id="Seg_1017" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_1019" n="HIAT:w" s="T279">ɨraːktaːgɨ</ts>
                  <nts id="Seg_1020" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_1022" n="HIAT:w" s="T280">ölbügetitten</ts>
                  <nts id="Seg_1023" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_1025" n="HIAT:w" s="T281">kürenen</ts>
                  <nts id="Seg_1026" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_1028" n="HIAT:w" s="T282">kelbit</ts>
                  <nts id="Seg_1029" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_1031" n="HIAT:w" s="T283">dʼonnorgo</ts>
                  <nts id="Seg_1032" n="HIAT:ip">,</nts>
                  <nts id="Seg_1033" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_1035" n="HIAT:w" s="T284">oloruŋ</ts>
                  <nts id="Seg_1036" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_1038" n="HIAT:w" s="T285">hahan</ts>
                  <nts id="Seg_1039" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_1041" n="HIAT:w" s="T286">kaja</ts>
                  <nts id="Seg_1042" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_1044" n="HIAT:w" s="T287">ihiger</ts>
                  <nts id="Seg_1045" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_1047" n="HIAT:w" s="T288">dʼi͡elenniler</ts>
                  <nts id="Seg_1048" n="HIAT:ip">.</nts>
                  <nts id="Seg_1049" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T298" id="Seg_1051" n="HIAT:u" s="T289">
                  <ts e="T290" id="Seg_1053" n="HIAT:w" s="T289">Maŋnaj</ts>
                  <nts id="Seg_1054" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_1056" n="HIAT:w" s="T290">en</ts>
                  <nts id="Seg_1057" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_1059" n="HIAT:w" s="T291">očiːhaga</ts>
                  <nts id="Seg_1060" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_1062" n="HIAT:w" s="T292">köröːččü</ts>
                  <nts id="Seg_1063" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_1065" n="HIAT:w" s="T293">kihiŋ</ts>
                  <nts id="Seg_1066" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1068" n="HIAT:w" s="T294">ol</ts>
                  <nts id="Seg_1069" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1071" n="HIAT:w" s="T295">kiniler</ts>
                  <nts id="Seg_1072" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_1074" n="HIAT:w" s="T296">bulčuttara</ts>
                  <nts id="Seg_1075" n="HIAT:ip">,</nts>
                  <nts id="Seg_1076" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_1078" n="HIAT:w" s="T297">kɨːldʼɨttara</ts>
                  <nts id="Seg_1079" n="HIAT:ip">.</nts>
                  <nts id="Seg_1080" n="HIAT:ip">"</nts>
                  <nts id="Seg_1081" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T298" id="Seg_1082" n="sc" s="T0">
               <ts e="T1" id="Seg_1084" n="e" s="T0">Tɨ͡a </ts>
               <ts e="T2" id="Seg_1086" n="e" s="T1">hiriger, </ts>
               <ts e="T3" id="Seg_1088" n="e" s="T2">kömükteːk </ts>
               <ts e="T4" id="Seg_1090" n="e" s="T3">bagajɨ </ts>
               <ts e="T5" id="Seg_1092" n="e" s="T4">hirge </ts>
               <ts e="T6" id="Seg_1094" n="e" s="T5">bɨlɨrgɨ </ts>
               <ts e="T7" id="Seg_1096" n="e" s="T6">kaːlbɨt </ts>
               <ts e="T8" id="Seg_1098" n="e" s="T7">očiːhakaːn </ts>
               <ts e="T9" id="Seg_1100" n="e" s="T8">baːra </ts>
               <ts e="T10" id="Seg_1102" n="e" s="T9">ebit. </ts>
               <ts e="T11" id="Seg_1104" n="e" s="T10">Ol </ts>
               <ts e="T12" id="Seg_1106" n="e" s="T11">očihakaːŋŋa </ts>
               <ts e="T13" id="Seg_1108" n="e" s="T12">ki͡ehe </ts>
               <ts e="T14" id="Seg_1110" n="e" s="T13">aːjɨ </ts>
               <ts e="T15" id="Seg_1112" n="e" s="T14">u͡ot </ts>
               <ts e="T16" id="Seg_1114" n="e" s="T15">ubajar </ts>
               <ts e="T17" id="Seg_1116" n="e" s="T16">bu͡olbut. </ts>
               <ts e="T18" id="Seg_1118" n="e" s="T17">Manna </ts>
               <ts e="T19" id="Seg_1120" n="e" s="T18">baːr </ts>
               <ts e="T20" id="Seg_1122" n="e" s="T19">ebit </ts>
               <ts e="T21" id="Seg_1124" n="e" s="T20">biːr </ts>
               <ts e="T22" id="Seg_1126" n="e" s="T21">kordʼon </ts>
               <ts e="T23" id="Seg_1128" n="e" s="T22">kihi. </ts>
               <ts e="T24" id="Seg_1130" n="e" s="T23">Kupi͡eska </ts>
               <ts e="T25" id="Seg_1132" n="e" s="T24">orobotnik </ts>
               <ts e="T26" id="Seg_1134" n="e" s="T25">bu͡ola </ts>
               <ts e="T27" id="Seg_1136" n="e" s="T26">hɨldʼar. </ts>
               <ts e="T28" id="Seg_1138" n="e" s="T27">Dʼonnor: </ts>
               <ts e="T29" id="Seg_1140" n="e" s="T28">"Tu͡ok </ts>
               <ts e="T30" id="Seg_1142" n="e" s="T29">u͡ota </ts>
               <ts e="T31" id="Seg_1144" n="e" s="T30">ubajara </ts>
               <ts e="T32" id="Seg_1146" n="e" s="T31">bu͡olla", </ts>
               <ts e="T33" id="Seg_1148" n="e" s="T32">di͡en </ts>
               <ts e="T34" id="Seg_1150" n="e" s="T33">högön </ts>
               <ts e="T35" id="Seg_1152" n="e" s="T34">kepseteller. </ts>
               <ts e="T36" id="Seg_1154" n="e" s="T35">Manɨ͡aga </ts>
               <ts e="T37" id="Seg_1156" n="e" s="T36">maː </ts>
               <ts e="T38" id="Seg_1158" n="e" s="T37">börköj </ts>
               <ts e="T39" id="Seg_1160" n="e" s="T38">kihiŋ </ts>
               <ts e="T40" id="Seg_1162" n="e" s="T39">eter </ts>
               <ts e="T41" id="Seg_1164" n="e" s="T40">kupi͡ehɨgar: </ts>
               <ts e="T42" id="Seg_1166" n="e" s="T41">"Dʼe </ts>
               <ts e="T43" id="Seg_1168" n="e" s="T42">zaklaːttahɨ͡ak", </ts>
               <ts e="T44" id="Seg_1170" n="e" s="T43">diːr. </ts>
               <ts e="T45" id="Seg_1172" n="e" s="T44">"Min </ts>
               <ts e="T46" id="Seg_1174" n="e" s="T45">baran </ts>
               <ts e="T47" id="Seg_1176" n="e" s="T46">bilen </ts>
               <ts e="T48" id="Seg_1178" n="e" s="T47">keli͡em. </ts>
               <ts e="T49" id="Seg_1180" n="e" s="T48">Min </ts>
               <ts e="T50" id="Seg_1182" n="e" s="T49">kottordokpuna </ts>
               <ts e="T51" id="Seg_1184" n="e" s="T50">bi͡ek </ts>
               <ts e="T52" id="Seg_1186" n="e" s="T51">bosko </ts>
               <ts e="T53" id="Seg_1188" n="e" s="T52">üleli͡em, </ts>
               <ts e="T54" id="Seg_1190" n="e" s="T53">en </ts>
               <ts e="T55" id="Seg_1192" n="e" s="T54">bu͡ollagɨna </ts>
               <ts e="T56" id="Seg_1194" n="e" s="T55">karčɨnɨ </ts>
               <ts e="T57" id="Seg_1196" n="e" s="T56">bi͡eri͡eŋ." </ts>
               <ts e="T58" id="Seg_1198" n="e" s="T57">Onon </ts>
               <ts e="T59" id="Seg_1200" n="e" s="T58">höbülehen </ts>
               <ts e="T60" id="Seg_1202" n="e" s="T59">kaːlsallar. </ts>
               <ts e="T61" id="Seg_1204" n="e" s="T60">Maː </ts>
               <ts e="T62" id="Seg_1206" n="e" s="T61">kihiŋ </ts>
               <ts e="T63" id="Seg_1208" n="e" s="T62">ki͡ehe </ts>
               <ts e="T64" id="Seg_1210" n="e" s="T63">ɨtɨnan </ts>
               <ts e="T65" id="Seg_1212" n="e" s="T64">barar. </ts>
               <ts e="T66" id="Seg_1214" n="e" s="T65">Bu </ts>
               <ts e="T67" id="Seg_1216" n="e" s="T66">baran </ts>
               <ts e="T68" id="Seg_1218" n="e" s="T67">očiːhaga </ts>
               <ts e="T69" id="Seg_1220" n="e" s="T68">kiːrer. </ts>
               <ts e="T70" id="Seg_1222" n="e" s="T69">Bu </ts>
               <ts e="T71" id="Seg_1224" n="e" s="T70">kiːrbite </ts>
               <ts e="T72" id="Seg_1226" n="e" s="T71">tüːleːk </ts>
               <ts e="T73" id="Seg_1228" n="e" s="T72">hɨrajdaːk </ts>
               <ts e="T74" id="Seg_1230" n="e" s="T73">kihi </ts>
               <ts e="T75" id="Seg_1232" n="e" s="T74">oloror. </ts>
               <ts e="T76" id="Seg_1234" n="e" s="T75">Manna </ts>
               <ts e="T77" id="Seg_1236" n="e" s="T76">kɨbɨsta-kɨbɨsta </ts>
               <ts e="T78" id="Seg_1238" n="e" s="T77">börköjüŋ </ts>
               <ts e="T79" id="Seg_1240" n="e" s="T78">oloror </ts>
               <ts e="T80" id="Seg_1242" n="e" s="T79">ɨskaːmɨjaga. </ts>
               <ts e="T81" id="Seg_1244" n="e" s="T80">Tüːleːk </ts>
               <ts e="T82" id="Seg_1246" n="e" s="T81">kihite </ts>
               <ts e="T83" id="Seg_1248" n="e" s="T82">kü͡östenen </ts>
               <ts e="T84" id="Seg_1250" n="e" s="T83">ahaːrɨ </ts>
               <ts e="T85" id="Seg_1252" n="e" s="T84">oloror </ts>
               <ts e="T86" id="Seg_1254" n="e" s="T85">ebit. </ts>
               <ts e="T87" id="Seg_1256" n="e" s="T86">Dʼe </ts>
               <ts e="T88" id="Seg_1258" n="e" s="T87">kü͡öhün </ts>
               <ts e="T89" id="Seg_1260" n="e" s="T88">kotorunan: </ts>
               <ts e="T90" id="Seg_1262" n="e" s="T89">"Ahaː", </ts>
               <ts e="T91" id="Seg_1264" n="e" s="T90">di͡ebit. </ts>
               <ts e="T92" id="Seg_1266" n="e" s="T91">Manɨ </ts>
               <ts e="T93" id="Seg_1268" n="e" s="T92">ahaːn </ts>
               <ts e="T94" id="Seg_1270" n="e" s="T93">baran </ts>
               <ts e="T95" id="Seg_1272" n="e" s="T94">utujaːrɨ </ts>
               <ts e="T96" id="Seg_1274" n="e" s="T95">hɨppɨttar. </ts>
               <ts e="T97" id="Seg_1276" n="e" s="T96">Bu </ts>
               <ts e="T98" id="Seg_1278" n="e" s="T97">hɨttagɨna </ts>
               <ts e="T99" id="Seg_1280" n="e" s="T98">harsɨ͡arda </ts>
               <ts e="T100" id="Seg_1282" n="e" s="T99">erde </ts>
               <ts e="T101" id="Seg_1284" n="e" s="T100">kihite </ts>
               <ts e="T102" id="Seg_1286" n="e" s="T101">kiŋilenen </ts>
               <ts e="T103" id="Seg_1288" n="e" s="T102">baran </ts>
               <ts e="T104" id="Seg_1290" n="e" s="T103">kaːlar. </ts>
               <ts e="T105" id="Seg_1292" n="e" s="T104">Kordʼon </ts>
               <ts e="T106" id="Seg_1294" n="e" s="T105">kihibit </ts>
               <ts e="T107" id="Seg_1296" n="e" s="T106">turar </ts>
               <ts e="T108" id="Seg_1298" n="e" s="T107">daː </ts>
               <ts e="T109" id="Seg_1300" n="e" s="T108">hanɨː </ts>
               <ts e="T110" id="Seg_1302" n="e" s="T109">bi͡erer: </ts>
               <ts e="T111" id="Seg_1304" n="e" s="T110">"Kajdak </ts>
               <ts e="T112" id="Seg_1306" n="e" s="T111">min </ts>
               <ts e="T113" id="Seg_1308" n="e" s="T112">manna </ts>
               <ts e="T114" id="Seg_1310" n="e" s="T113">hɨldʼɨbɨppɨn </ts>
               <ts e="T115" id="Seg_1312" n="e" s="T114">billeri͡emij", </ts>
               <ts e="T116" id="Seg_1314" n="e" s="T115">diːr. </ts>
               <ts e="T117" id="Seg_1316" n="e" s="T116">"Bu </ts>
               <ts e="T118" id="Seg_1318" n="e" s="T117">kihi </ts>
               <ts e="T119" id="Seg_1320" n="e" s="T118">kanna </ts>
               <ts e="T120" id="Seg_1322" n="e" s="T119">barbɨtɨn </ts>
               <ts e="T121" id="Seg_1324" n="e" s="T120">bilen </ts>
               <ts e="T122" id="Seg_1326" n="e" s="T121">beli͡e </ts>
               <ts e="T123" id="Seg_1328" n="e" s="T122">egeli͡ekteːkpin", </ts>
               <ts e="T124" id="Seg_1330" n="e" s="T123">di͡en </ts>
               <ts e="T125" id="Seg_1332" n="e" s="T124">orogun </ts>
               <ts e="T126" id="Seg_1334" n="e" s="T125">batan </ts>
               <ts e="T127" id="Seg_1336" n="e" s="T126">baran, </ts>
               <ts e="T128" id="Seg_1338" n="e" s="T127">bu </ts>
               <ts e="T129" id="Seg_1340" n="e" s="T128">baran </ts>
               <ts e="T130" id="Seg_1342" n="e" s="T129">taːs </ts>
               <ts e="T131" id="Seg_1344" n="e" s="T130">kajaga </ts>
               <ts e="T132" id="Seg_1346" n="e" s="T131">tijer. </ts>
               <ts e="T133" id="Seg_1348" n="e" s="T132">Manna </ts>
               <ts e="T134" id="Seg_1350" n="e" s="T133">kistenen </ts>
               <ts e="T135" id="Seg_1352" n="e" s="T134">kelbite, </ts>
               <ts e="T136" id="Seg_1354" n="e" s="T135">bu </ts>
               <ts e="T137" id="Seg_1356" n="e" s="T136">taːs </ts>
               <ts e="T138" id="Seg_1358" n="e" s="T137">kaja </ts>
               <ts e="T139" id="Seg_1360" n="e" s="T138">barɨta </ts>
               <ts e="T140" id="Seg_1362" n="e" s="T139">kahɨllɨbɨt. </ts>
               <ts e="T141" id="Seg_1364" n="e" s="T140">Dʼon </ts>
               <ts e="T142" id="Seg_1366" n="e" s="T141">köːküne </ts>
               <ts e="T143" id="Seg_1368" n="e" s="T142">di͡en, </ts>
               <ts e="T144" id="Seg_1370" n="e" s="T143">haŋalara </ts>
               <ts e="T145" id="Seg_1372" n="e" s="T144">di͡en, </ts>
               <ts e="T146" id="Seg_1374" n="e" s="T145">öŋöjön </ts>
               <ts e="T147" id="Seg_1376" n="e" s="T146">körbüte </ts>
               <ts e="T148" id="Seg_1378" n="e" s="T147">kajagahɨnan </ts>
               <ts e="T149" id="Seg_1380" n="e" s="T148">kihilere </ts>
               <ts e="T150" id="Seg_1382" n="e" s="T149">barɨta </ts>
               <ts e="T151" id="Seg_1384" n="e" s="T150">tüːleːk </ts>
               <ts e="T152" id="Seg_1386" n="e" s="T151">hirejdeːk. </ts>
               <ts e="T153" id="Seg_1388" n="e" s="T152">Manna </ts>
               <ts e="T154" id="Seg_1390" n="e" s="T153">ü͡ömen </ts>
               <ts e="T155" id="Seg_1392" n="e" s="T154">kiːrbit. </ts>
               <ts e="T156" id="Seg_1394" n="e" s="T155">Bu </ts>
               <ts e="T157" id="Seg_1396" n="e" s="T156">kiːren </ts>
               <ts e="T158" id="Seg_1398" n="e" s="T157">bu </ts>
               <ts e="T159" id="Seg_1400" n="e" s="T158">dʼi͡ege, </ts>
               <ts e="T160" id="Seg_1402" n="e" s="T159">balagaŋŋa </ts>
               <ts e="T161" id="Seg_1404" n="e" s="T160">kiːren </ts>
               <ts e="T162" id="Seg_1406" n="e" s="T161">oloror. </ts>
               <ts e="T163" id="Seg_1408" n="e" s="T162">Bu </ts>
               <ts e="T164" id="Seg_1410" n="e" s="T163">olorbuta </ts>
               <ts e="T165" id="Seg_1412" n="e" s="T164">biːr </ts>
               <ts e="T166" id="Seg_1414" n="e" s="T165">dʼaktar </ts>
               <ts e="T167" id="Seg_1416" n="e" s="T166">baːr </ts>
               <ts e="T168" id="Seg_1418" n="e" s="T167">ebit. </ts>
               <ts e="T169" id="Seg_1420" n="e" s="T168">Ol </ts>
               <ts e="T170" id="Seg_1422" n="e" s="T169">orgujkaːn </ts>
               <ts e="T171" id="Seg_1424" n="e" s="T170">ɨjɨtar: </ts>
               <ts e="T172" id="Seg_1426" n="e" s="T171">"Kaja </ts>
               <ts e="T173" id="Seg_1428" n="e" s="T172">tugunan </ts>
               <ts e="T174" id="Seg_1430" n="e" s="T173">naːdɨjan </ts>
               <ts e="T175" id="Seg_1432" n="e" s="T174">kelliŋ", </ts>
               <ts e="T176" id="Seg_1434" n="e" s="T175">diːr. </ts>
               <ts e="T177" id="Seg_1436" n="e" s="T176">Manɨ </ts>
               <ts e="T178" id="Seg_1438" n="e" s="T177">eter: </ts>
               <ts e="T179" id="Seg_1440" n="e" s="T178">"Hinnʼiges </ts>
               <ts e="T180" id="Seg_1442" n="e" s="T179">hirbinen", </ts>
               <ts e="T181" id="Seg_1444" n="e" s="T180">diːr. </ts>
               <ts e="T182" id="Seg_1446" n="e" s="T181">Onu͡oka </ts>
               <ts e="T183" id="Seg_1448" n="e" s="T182">maː </ts>
               <ts e="T184" id="Seg_1450" n="e" s="T183">dʼaktara </ts>
               <ts e="T185" id="Seg_1452" n="e" s="T184">tugu </ts>
               <ts e="T186" id="Seg_1454" n="e" s="T185">ere </ts>
               <ts e="T187" id="Seg_1456" n="e" s="T186">hɨmnagas </ts>
               <ts e="T188" id="Seg_1458" n="e" s="T187">bagajɨnɨ </ts>
               <ts e="T189" id="Seg_1460" n="e" s="T188">tuttaran </ts>
               <ts e="T190" id="Seg_1462" n="e" s="T189">keːher. </ts>
               <ts e="T191" id="Seg_1464" n="e" s="T190">Onu </ts>
               <ts e="T192" id="Seg_1466" n="e" s="T191">körümne </ts>
               <ts e="T193" id="Seg_1468" n="e" s="T192">ere </ts>
               <ts e="T194" id="Seg_1470" n="e" s="T193">hi͡ebiger </ts>
               <ts e="T195" id="Seg_1472" n="e" s="T194">uktan </ts>
               <ts e="T196" id="Seg_1474" n="e" s="T195">keːher, </ts>
               <ts e="T197" id="Seg_1476" n="e" s="T196">dʼaktara </ts>
               <ts e="T198" id="Seg_1478" n="e" s="T197">eter: </ts>
               <ts e="T199" id="Seg_1480" n="e" s="T198">"Dʼe </ts>
               <ts e="T200" id="Seg_1482" n="e" s="T199">kɨtaːt, </ts>
               <ts e="T201" id="Seg_1484" n="e" s="T200">baran </ts>
               <ts e="T202" id="Seg_1486" n="e" s="T201">kör, </ts>
               <ts e="T203" id="Seg_1488" n="e" s="T202">manna </ts>
               <ts e="T204" id="Seg_1490" n="e" s="T203">dʼonnorum </ts>
               <ts e="T205" id="Seg_1492" n="e" s="T204">kördöktörüne </ts>
               <ts e="T206" id="Seg_1494" n="e" s="T205">kihili͡ektere </ts>
               <ts e="T207" id="Seg_1496" n="e" s="T206">hu͡oga", </ts>
               <ts e="T208" id="Seg_1498" n="e" s="T207">di͡ebit. </ts>
               <ts e="T209" id="Seg_1500" n="e" s="T208">Onu </ts>
               <ts e="T210" id="Seg_1502" n="e" s="T209">isteːt </ts>
               <ts e="T211" id="Seg_1504" n="e" s="T210">kordʼon </ts>
               <ts e="T212" id="Seg_1506" n="e" s="T211">kihi </ts>
               <ts e="T213" id="Seg_1508" n="e" s="T212">taksɨbɨt, </ts>
               <ts e="T214" id="Seg_1510" n="e" s="T213">ɨtɨgar </ts>
               <ts e="T215" id="Seg_1512" n="e" s="T214">olorbut </ts>
               <ts e="T216" id="Seg_1514" n="e" s="T215">daː </ts>
               <ts e="T217" id="Seg_1516" n="e" s="T216">bararga </ts>
               <ts e="T218" id="Seg_1518" n="e" s="T217">bu͡olbut. </ts>
               <ts e="T219" id="Seg_1520" n="e" s="T218">Bu </ts>
               <ts e="T220" id="Seg_1522" n="e" s="T219">baran </ts>
               <ts e="T221" id="Seg_1524" n="e" s="T220">ihen </ts>
               <ts e="T222" id="Seg_1526" n="e" s="T221">kennin </ts>
               <ts e="T223" id="Seg_1528" n="e" s="T222">körbüte, </ts>
               <ts e="T224" id="Seg_1530" n="e" s="T223">biːr </ts>
               <ts e="T225" id="Seg_1532" n="e" s="T224">kihi </ts>
               <ts e="T226" id="Seg_1534" n="e" s="T225">batɨhan </ts>
               <ts e="T227" id="Seg_1536" n="e" s="T226">iher. </ts>
               <ts e="T228" id="Seg_1538" n="e" s="T227">Ontuta </ts>
               <ts e="T229" id="Seg_1540" n="e" s="T228">hiten </ts>
               <ts e="T230" id="Seg_1542" n="e" s="T229">iherin </ts>
               <ts e="T231" id="Seg_1544" n="e" s="T230">körön </ts>
               <ts e="T232" id="Seg_1546" n="e" s="T231">maː </ts>
               <ts e="T233" id="Seg_1548" n="e" s="T232">kihi </ts>
               <ts e="T234" id="Seg_1550" n="e" s="T233">kötüppüt. </ts>
               <ts e="T235" id="Seg_1552" n="e" s="T234">Tüːleːk </ts>
               <ts e="T236" id="Seg_1554" n="e" s="T235">hɨrajdaːk </ts>
               <ts e="T237" id="Seg_1556" n="e" s="T236">kihite </ts>
               <ts e="T238" id="Seg_1558" n="e" s="T237">hɨrgatɨn </ts>
               <ts e="T239" id="Seg_1560" n="e" s="T238">kelin </ts>
               <ts e="T240" id="Seg_1562" n="e" s="T239">atagɨttan </ts>
               <ts e="T241" id="Seg_1564" n="e" s="T240">ɨlan </ts>
               <ts e="T242" id="Seg_1566" n="e" s="T241">ikki </ts>
               <ts e="T243" id="Seg_1568" n="e" s="T242">atagɨn </ts>
               <ts e="T244" id="Seg_1570" n="e" s="T243">kaːllarbɨt. </ts>
               <ts e="T245" id="Seg_1572" n="e" s="T244">"Tübestergin </ts>
               <ts e="T246" id="Seg_1574" n="e" s="T245">kördörü͡ök </ts>
               <ts e="T247" id="Seg_1576" n="e" s="T246">etim", </ts>
               <ts e="T248" id="Seg_1578" n="e" s="T247">di͡en </ts>
               <ts e="T249" id="Seg_1580" n="e" s="T248">kahɨːtaːn </ts>
               <ts e="T250" id="Seg_1582" n="e" s="T249">kaːlbɨt. </ts>
               <ts e="T251" id="Seg_1584" n="e" s="T250">Maː </ts>
               <ts e="T252" id="Seg_1586" n="e" s="T251">u͡ol </ts>
               <ts e="T253" id="Seg_1588" n="e" s="T252">dʼi͡etiger </ts>
               <ts e="T254" id="Seg_1590" n="e" s="T253">kelen </ts>
               <ts e="T255" id="Seg_1592" n="e" s="T254">"beli͡e </ts>
               <ts e="T256" id="Seg_1594" n="e" s="T255">egellim" </ts>
               <ts e="T257" id="Seg_1596" n="e" s="T256">di͡en </ts>
               <ts e="T258" id="Seg_1598" n="e" s="T257">karmaːnɨttan </ts>
               <ts e="T259" id="Seg_1600" n="e" s="T258">hɨmnagahɨn </ts>
               <ts e="T260" id="Seg_1602" n="e" s="T259">tahaːrbɨta — </ts>
               <ts e="T261" id="Seg_1604" n="e" s="T260">kara </ts>
               <ts e="T262" id="Seg_1606" n="e" s="T261">hahɨl </ts>
               <ts e="T263" id="Seg_1608" n="e" s="T262">bu͡olbut. </ts>
               <ts e="T264" id="Seg_1610" n="e" s="T263">Kupi͡eha </ts>
               <ts e="T265" id="Seg_1612" n="e" s="T264">bu͡ollagɨna </ts>
               <ts e="T266" id="Seg_1614" n="e" s="T265">karčɨ </ts>
               <ts e="T267" id="Seg_1616" n="e" s="T266">bögö </ts>
               <ts e="T268" id="Seg_1618" n="e" s="T267">bi͡erbit. </ts>
               <ts e="T269" id="Seg_1620" n="e" s="T268">Onon </ts>
               <ts e="T270" id="Seg_1622" n="e" s="T269">baːj </ts>
               <ts e="T271" id="Seg_1624" n="e" s="T270">kihi </ts>
               <ts e="T272" id="Seg_1626" n="e" s="T271">bu͡olbut. </ts>
               <ts e="T273" id="Seg_1628" n="e" s="T272">Manɨ </ts>
               <ts e="T274" id="Seg_1630" n="e" s="T273">kɨrdʼagastar </ts>
               <ts e="T275" id="Seg_1632" n="e" s="T274">eppitter: </ts>
               <ts e="T276" id="Seg_1634" n="e" s="T275">"En </ts>
               <ts e="T277" id="Seg_1636" n="e" s="T276">tije </ts>
               <ts e="T278" id="Seg_1638" n="e" s="T277">hɨldʼɨbɨkkɨn </ts>
               <ts e="T279" id="Seg_1640" n="e" s="T278">bɨlɨr </ts>
               <ts e="T280" id="Seg_1642" n="e" s="T279">ɨraːktaːgɨ </ts>
               <ts e="T281" id="Seg_1644" n="e" s="T280">ölbügetitten </ts>
               <ts e="T282" id="Seg_1646" n="e" s="T281">kürenen </ts>
               <ts e="T283" id="Seg_1648" n="e" s="T282">kelbit </ts>
               <ts e="T284" id="Seg_1650" n="e" s="T283">dʼonnorgo, </ts>
               <ts e="T285" id="Seg_1652" n="e" s="T284">oloruŋ </ts>
               <ts e="T286" id="Seg_1654" n="e" s="T285">hahan </ts>
               <ts e="T287" id="Seg_1656" n="e" s="T286">kaja </ts>
               <ts e="T288" id="Seg_1658" n="e" s="T287">ihiger </ts>
               <ts e="T289" id="Seg_1660" n="e" s="T288">dʼi͡elenniler. </ts>
               <ts e="T290" id="Seg_1662" n="e" s="T289">Maŋnaj </ts>
               <ts e="T291" id="Seg_1664" n="e" s="T290">en </ts>
               <ts e="T292" id="Seg_1666" n="e" s="T291">očiːhaga </ts>
               <ts e="T293" id="Seg_1668" n="e" s="T292">köröːččü </ts>
               <ts e="T294" id="Seg_1670" n="e" s="T293">kihiŋ </ts>
               <ts e="T295" id="Seg_1672" n="e" s="T294">ol </ts>
               <ts e="T296" id="Seg_1674" n="e" s="T295">kiniler </ts>
               <ts e="T297" id="Seg_1676" n="e" s="T296">bulčuttara, </ts>
               <ts e="T298" id="Seg_1678" n="e" s="T297">kɨːldʼɨttara." </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T10" id="Seg_1679" s="T0">BaA_1930_FireInSmallTent_flk.001 (001.001)</ta>
            <ta e="T17" id="Seg_1680" s="T10">BaA_1930_FireInSmallTent_flk.002 (001.002)</ta>
            <ta e="T23" id="Seg_1681" s="T17">BaA_1930_FireInSmallTent_flk.003 (001.003)</ta>
            <ta e="T27" id="Seg_1682" s="T23">BaA_1930_FireInSmallTent_flk.004 (001.004)</ta>
            <ta e="T28" id="Seg_1683" s="T27">BaA_1930_FireInSmallTent_flk.005 (001.005)</ta>
            <ta e="T35" id="Seg_1684" s="T28">BaA_1930_FireInSmallTent_flk.006 (001.006)</ta>
            <ta e="T41" id="Seg_1685" s="T35">BaA_1930_FireInSmallTent_flk.007 (001.007)</ta>
            <ta e="T44" id="Seg_1686" s="T41">BaA_1930_FireInSmallTent_flk.008 (001.008)</ta>
            <ta e="T48" id="Seg_1687" s="T44">BaA_1930_FireInSmallTent_flk.009 (001.009)</ta>
            <ta e="T57" id="Seg_1688" s="T48">BaA_1930_FireInSmallTent_flk.010 (001.010)</ta>
            <ta e="T60" id="Seg_1689" s="T57">BaA_1930_FireInSmallTent_flk.011 (001.011)</ta>
            <ta e="T65" id="Seg_1690" s="T60">BaA_1930_FireInSmallTent_flk.012 (001.012)</ta>
            <ta e="T69" id="Seg_1691" s="T65">BaA_1930_FireInSmallTent_flk.013 (001.013)</ta>
            <ta e="T75" id="Seg_1692" s="T69">BaA_1930_FireInSmallTent_flk.014 (001.014)</ta>
            <ta e="T80" id="Seg_1693" s="T75">BaA_1930_FireInSmallTent_flk.015 (001.015)</ta>
            <ta e="T86" id="Seg_1694" s="T80">BaA_1930_FireInSmallTent_flk.016 (001.016)</ta>
            <ta e="T89" id="Seg_1695" s="T86">BaA_1930_FireInSmallTent_flk.017 (001.017)</ta>
            <ta e="T91" id="Seg_1696" s="T89">BaA_1930_FireInSmallTent_flk.018 (001.018)</ta>
            <ta e="T96" id="Seg_1697" s="T91">BaA_1930_FireInSmallTent_flk.019 (001.019)</ta>
            <ta e="T104" id="Seg_1698" s="T96">BaA_1930_FireInSmallTent_flk.020 (001.020)</ta>
            <ta e="T110" id="Seg_1699" s="T104">BaA_1930_FireInSmallTent_flk.021 (001.021)</ta>
            <ta e="T116" id="Seg_1700" s="T110">BaA_1930_FireInSmallTent_flk.022 (001.022)</ta>
            <ta e="T132" id="Seg_1701" s="T116">BaA_1930_FireInSmallTent_flk.023 (001.024)</ta>
            <ta e="T140" id="Seg_1702" s="T132">BaA_1930_FireInSmallTent_flk.024 (001.025)</ta>
            <ta e="T152" id="Seg_1703" s="T140">BaA_1930_FireInSmallTent_flk.025 (001.026)</ta>
            <ta e="T155" id="Seg_1704" s="T152">BaA_1930_FireInSmallTent_flk.026 (001.027)</ta>
            <ta e="T162" id="Seg_1705" s="T155">BaA_1930_FireInSmallTent_flk.027 (001.028)</ta>
            <ta e="T168" id="Seg_1706" s="T162">BaA_1930_FireInSmallTent_flk.028 (001.029)</ta>
            <ta e="T171" id="Seg_1707" s="T168">BaA_1930_FireInSmallTent_flk.029 (001.030)</ta>
            <ta e="T176" id="Seg_1708" s="T171">BaA_1930_FireInSmallTent_flk.030 (001.031)</ta>
            <ta e="T178" id="Seg_1709" s="T176">BaA_1930_FireInSmallTent_flk.031 (001.033)</ta>
            <ta e="T181" id="Seg_1710" s="T178">BaA_1930_FireInSmallTent_flk.032 (001.034)</ta>
            <ta e="T190" id="Seg_1711" s="T181">BaA_1930_FireInSmallTent_flk.033 (001.035)</ta>
            <ta e="T198" id="Seg_1712" s="T190">BaA_1930_FireInSmallTent_flk.034 (001.036)</ta>
            <ta e="T208" id="Seg_1713" s="T198">BaA_1930_FireInSmallTent_flk.035 (001.037)</ta>
            <ta e="T218" id="Seg_1714" s="T208">BaA_1930_FireInSmallTent_flk.036 (001.038)</ta>
            <ta e="T227" id="Seg_1715" s="T218">BaA_1930_FireInSmallTent_flk.037 (001.039)</ta>
            <ta e="T234" id="Seg_1716" s="T227">BaA_1930_FireInSmallTent_flk.038 (001.040)</ta>
            <ta e="T244" id="Seg_1717" s="T234">BaA_1930_FireInSmallTent_flk.039 (001.041)</ta>
            <ta e="T250" id="Seg_1718" s="T244">BaA_1930_FireInSmallTent_flk.040 (001.042)</ta>
            <ta e="T263" id="Seg_1719" s="T250">BaA_1930_FireInSmallTent_flk.041 (001.043)</ta>
            <ta e="T268" id="Seg_1720" s="T263">BaA_1930_FireInSmallTent_flk.042 (001.044)</ta>
            <ta e="T272" id="Seg_1721" s="T268">BaA_1930_FireInSmallTent_flk.043 (001.045)</ta>
            <ta e="T275" id="Seg_1722" s="T272">BaA_1930_FireInSmallTent_flk.044 (001.046)</ta>
            <ta e="T289" id="Seg_1723" s="T275">BaA_1930_FireInSmallTent_flk.045 (001.047)</ta>
            <ta e="T298" id="Seg_1724" s="T289">BaA_1930_FireInSmallTent_flk.046 (001.048)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T10" id="Seg_1725" s="T0">Тыа һиригэр, көмүктээх багайы һиргэ былыргы каалбыт очииһакаан баара эбит.</ta>
            <ta e="T17" id="Seg_1726" s="T10">Ол очиһакааҥҥа киэһэ аайы уот убайар буолбут.</ta>
            <ta e="T23" id="Seg_1727" s="T17">Манна баар эбит биир кордьон киһи.</ta>
            <ta e="T27" id="Seg_1728" s="T23">Купиэска ороботник буола һылдьар.</ta>
            <ta e="T28" id="Seg_1729" s="T27">Дьоннор:</ta>
            <ta e="T35" id="Seg_1730" s="T28">— Туок уота убайара буолла, — диэн һөгөн кэпсэтэллэр.</ta>
            <ta e="T41" id="Seg_1731" s="T35">Маныага маа бөркөй киһиҥ этэр купиэһыгар:</ta>
            <ta e="T44" id="Seg_1732" s="T41">— Дьэ заклааттаһыак, — диир.</ta>
            <ta e="T48" id="Seg_1733" s="T44">— Мин баран билэн кэлиэм.</ta>
            <ta e="T57" id="Seg_1734" s="T48">Мин коттордокпуна биэк боско үлэлиэм, эн буоллагына карчыны биэриэҥ.</ta>
            <ta e="T60" id="Seg_1735" s="T57">Онон һөбүлэһэн каалсаллар.</ta>
            <ta e="T65" id="Seg_1736" s="T60">Маа киһиҥ киэһэ ытынан барар.</ta>
            <ta e="T69" id="Seg_1737" s="T65">Бу баран очииһага киирэр.</ta>
            <ta e="T75" id="Seg_1738" s="T69">Бу киирбитэ түүлээк һырайдаак киһи олорор.</ta>
            <ta e="T80" id="Seg_1739" s="T75">Манна кыбыста-кыбыста бөркөйүҥ олорор ыскаамыйага.</ta>
            <ta e="T86" id="Seg_1740" s="T80">Түүлээк киһитэ күөстэнэн аһаары олорор эбит.</ta>
            <ta e="T89" id="Seg_1741" s="T86">Дьэ күөһүн которунан:</ta>
            <ta e="T91" id="Seg_1742" s="T89">— Аһаа, — диэбит.</ta>
            <ta e="T96" id="Seg_1743" s="T91">Маны аһаан баран утуйаары һыппыттар.</ta>
            <ta e="T104" id="Seg_1744" s="T96">Бу һыттагына һарсыарда эрдэ киһитэ киҥилэнэн баран каалар.</ta>
            <ta e="T110" id="Seg_1745" s="T104">Кордьон киһибит турар даа һаныы биэрэр:</ta>
            <ta e="T116" id="Seg_1746" s="T110">— Кайдак мин манна һылдьыбыппын биллэриэмий? — диир.</ta>
            <ta e="T132" id="Seg_1747" s="T116">— Бу киһи канна барбытын билэн бэлиэ эгэлиэктээкпин, — диэн орогун батан баран, бу баран таас кайага тийэр.</ta>
            <ta e="T140" id="Seg_1748" s="T132">Манна кистэнэн кэлбитэ, бу таас кайа барыта каһыллыбыт.</ta>
            <ta e="T152" id="Seg_1749" s="T140">Дьон көөкүнэ диэн, һаҥалара диэн, өҥөйөн көрбүтэ кайагаһынан киһилэрэ барыта түүлээк һирэйдээк.</ta>
            <ta e="T155" id="Seg_1750" s="T152">Манна үөмэн киирбит.</ta>
            <ta e="T162" id="Seg_1751" s="T155">Бу киирэн бу дьиэгэ, балагаҥҥа киирэн олорор.</ta>
            <ta e="T168" id="Seg_1752" s="T162">Бу олорбута биир дьактар баар эбит.</ta>
            <ta e="T171" id="Seg_1753" s="T168">Ол оргуйкаан ыйытар:</ta>
            <ta e="T176" id="Seg_1754" s="T171">— Кайа тугунан наадыйан кэллиҥ? — диир.</ta>
            <ta e="T178" id="Seg_1755" s="T176">Маны этэр:</ta>
            <ta e="T181" id="Seg_1756" s="T178">— Һинньигэс һирбинэн, — диир.</ta>
            <ta e="T190" id="Seg_1757" s="T181">Онуока маа дьактара тугу эрэ һымнагас багайыны туттаран кээһэр.</ta>
            <ta e="T198" id="Seg_1758" s="T190">Ону көрүмнэ эрэ һиэбигэр уктан кээһэр, дьактара этэр:</ta>
            <ta e="T208" id="Seg_1759" s="T198">— Дьэ кытаат, баран көр, манна дьоннорум көрдөктөрүнэ киһилиэктэрэ һуога, — диэбит.</ta>
            <ta e="T218" id="Seg_1760" s="T208">Ону истээт кордьон киһи таксыбыт, ытыгар олорбут даа барарга буолбут.</ta>
            <ta e="T227" id="Seg_1761" s="T218">Бу баран иһэн кэннин көрбүтэ: биир киһи батыһан иһэр.</ta>
            <ta e="T234" id="Seg_1762" s="T227">Онтута һитэн иһэрин көрөн маа киһи көтүппүт.</ta>
            <ta e="T244" id="Seg_1763" s="T234">Түүлээк һырайдаак киһитэ һыргатын кэлин атагыттан ылан икки атагын каалларбыт.</ta>
            <ta e="T250" id="Seg_1764" s="T244">— Түбэстэргин көрдөрүөх этим, — диэн каһыытаан каалбыт.</ta>
            <ta e="T263" id="Seg_1765" s="T250">Маа уол дьиэтигэр кэлэн бэлиэ эгэллим диэн кармааныттан һымнагаһын таһаарбыта — кара һаһыл буолбут.</ta>
            <ta e="T268" id="Seg_1766" s="T263">Купиэһа буоллагына карчы бөгө биэрбит.</ta>
            <ta e="T272" id="Seg_1767" s="T268">Онон баай киһи буолбут.</ta>
            <ta e="T275" id="Seg_1768" s="T272">Маны кырдьагастар эппиттэр:</ta>
            <ta e="T289" id="Seg_1769" s="T275">— Эн тийэ һылдьыбыккын былыр ыраактаагы өлбүгэтиттэн күрэнэн кэлбит дьоннорго, олоруҥ һаһан кайа иһигэр дьиэлэннилэр.</ta>
            <ta e="T298" id="Seg_1770" s="T289">Маҥнай эн очииһага көрөөччү киһиҥ ол кинилэр булчуттара, кыылдьыттара.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T10" id="Seg_1771" s="T0">Tɨ͡a hiriger, kömükteːk bagajɨ hirge bɨlɨrgɨ kaːlbɨt očiːhakaːn baːra ebit. </ta>
            <ta e="T17" id="Seg_1772" s="T10">Ol očihakaːŋŋa ki͡ehe aːjɨ u͡ot ubajar bu͡olbut. </ta>
            <ta e="T23" id="Seg_1773" s="T17">Manna baːr ebit biːr kordʼon kihi. </ta>
            <ta e="T27" id="Seg_1774" s="T23">Kupi͡eska orobotnik bu͡ola hɨldʼar. </ta>
            <ta e="T28" id="Seg_1775" s="T27">Dʼonnor:</ta>
            <ta e="T35" id="Seg_1776" s="T28">"Tu͡ok u͡ota ubajara bu͡olla", di͡en högön kepseteller. </ta>
            <ta e="T41" id="Seg_1777" s="T35">Manɨ͡aga maː börköj kihiŋ eter kupi͡ehɨgar:</ta>
            <ta e="T44" id="Seg_1778" s="T41">"Dʼe zaklaːttahɨ͡ak", diːr. </ta>
            <ta e="T48" id="Seg_1779" s="T44">"Min baran bilen keli͡em. </ta>
            <ta e="T57" id="Seg_1780" s="T48">Min kottordokpuna bi͡ek bosko üleli͡em, en bu͡ollagɨna karčɨnɨ bi͡eri͡eŋ." </ta>
            <ta e="T60" id="Seg_1781" s="T57">Onon höbülehen kaːlsallar. </ta>
            <ta e="T65" id="Seg_1782" s="T60">Maː kihiŋ ki͡ehe ɨtɨnan barar. </ta>
            <ta e="T69" id="Seg_1783" s="T65">Bu baran očiːhaga kiːrer. </ta>
            <ta e="T75" id="Seg_1784" s="T69">Bu kiːrbite tüːleːk hɨrajdaːk kihi oloror. </ta>
            <ta e="T80" id="Seg_1785" s="T75">Manna kɨbɨsta-kɨbɨsta börköjüŋ oloror ɨskaːmɨjaga. </ta>
            <ta e="T86" id="Seg_1786" s="T80">Tüːleːk kihite kü͡östenen ahaːrɨ oloror ebit. </ta>
            <ta e="T89" id="Seg_1787" s="T86">Dʼe kü͡öhün kotorunan:</ta>
            <ta e="T91" id="Seg_1788" s="T89">"Ahaː", di͡ebit. </ta>
            <ta e="T96" id="Seg_1789" s="T91">Manɨ ahaːn baran utujaːrɨ hɨppɨttar. </ta>
            <ta e="T104" id="Seg_1790" s="T96">Bu hɨttagɨna harsɨ͡arda erde kihite kiŋilenen baran kaːlar. </ta>
            <ta e="T110" id="Seg_1791" s="T104">Kordʼon kihibit turar daː hanɨː bi͡erer:</ta>
            <ta e="T116" id="Seg_1792" s="T110">"Kajdak min manna hɨldʼɨbɨppɨn billeri͡emij", diːr. </ta>
            <ta e="T132" id="Seg_1793" s="T116">"Bu kihi kanna barbɨtɨn bilen beli͡e egeli͡ekteːkpin", di͡en orogun batan baran, bu baran taːs kajaga tijer. </ta>
            <ta e="T140" id="Seg_1794" s="T132">Manna kistenen kelbite, bu taːs kaja barɨta kahɨllɨbɨt. </ta>
            <ta e="T152" id="Seg_1795" s="T140">Dʼon köːküne di͡en, haŋalara di͡en, öŋöjön körbüte kajagahɨnan kihilere barɨta tüːleːk hirejdeːk. </ta>
            <ta e="T155" id="Seg_1796" s="T152">Manna ü͡ömen kiːrbit. </ta>
            <ta e="T162" id="Seg_1797" s="T155">Bu kiːren bu dʼi͡ege, balagaŋŋa kiːren oloror. </ta>
            <ta e="T168" id="Seg_1798" s="T162">Bu olorbuta biːr dʼaktar baːr ebit. </ta>
            <ta e="T171" id="Seg_1799" s="T168">Ol orgujkaːn ɨjɨtar:</ta>
            <ta e="T176" id="Seg_1800" s="T171">"Kaja tugunan naːdɨjan kelliŋ", diːr. </ta>
            <ta e="T178" id="Seg_1801" s="T176">Manɨ eter:</ta>
            <ta e="T181" id="Seg_1802" s="T178">"Hinnʼiges hirbinen", diːr. </ta>
            <ta e="T190" id="Seg_1803" s="T181">Onu͡oka maː dʼaktara tugu ere hɨmnagas bagajɨnɨ tuttaran keːher. </ta>
            <ta e="T198" id="Seg_1804" s="T190">Onu körümne ere hi͡ebiger uktan keːher, dʼaktara eter:</ta>
            <ta e="T208" id="Seg_1805" s="T198">"Dʼe kɨtaːt, baran kör, manna dʼonnorum kördöktörüne kihili͡ektere hu͡oga", di͡ebit. </ta>
            <ta e="T218" id="Seg_1806" s="T208">Onu isteːt kordʼon kihi taksɨbɨt, ɨtɨgar olorbut daː bararga bu͡olbut. </ta>
            <ta e="T227" id="Seg_1807" s="T218">Bu baran ihen kennin körbüte, biːr kihi batɨhan iher. </ta>
            <ta e="T234" id="Seg_1808" s="T227">Ontuta hiten iherin körön maː kihi kötüppüt. </ta>
            <ta e="T244" id="Seg_1809" s="T234">Tüːleːk hɨrajdaːk kihite hɨrgatɨn kelin atagɨttan ɨlan ikki atagɨn kaːllarbɨt. </ta>
            <ta e="T250" id="Seg_1810" s="T244">"Tübestergin kördörü͡ök etim", di͡en kahɨːtaːn kaːlbɨt. </ta>
            <ta e="T263" id="Seg_1811" s="T250">Maː u͡ol dʼi͡etiger kelen "beli͡e egellim" di͡en karmaːnɨttan hɨmnagahɨn tahaːrbɨta — kara hahɨl bu͡olbut. </ta>
            <ta e="T268" id="Seg_1812" s="T263">Kupi͡eha bu͡ollagɨna karčɨ bögö bi͡erbit. </ta>
            <ta e="T272" id="Seg_1813" s="T268">Onon baːj kihi bu͡olbut. </ta>
            <ta e="T275" id="Seg_1814" s="T272">Manɨ kɨrdʼagastar eppitter:</ta>
            <ta e="T289" id="Seg_1815" s="T275">"En tije hɨldʼɨbɨkkɨn bɨlɨr ɨraːktaːgɨ ölbügetitten kürenen kelbit dʼonnorgo, oloruŋ hahan kaja ihiger dʼi͡elenniler. </ta>
            <ta e="T298" id="Seg_1816" s="T289">Maŋnaj en očiːhaga köröːččü kihiŋ ol kiniler bulčuttara, kɨːldʼɨttara." </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_1817" s="T0">tɨ͡a</ta>
            <ta e="T2" id="Seg_1818" s="T1">hir-i-ger</ta>
            <ta e="T3" id="Seg_1819" s="T2">kömük-teːk</ta>
            <ta e="T4" id="Seg_1820" s="T3">bagajɨ</ta>
            <ta e="T5" id="Seg_1821" s="T4">hir-ge</ta>
            <ta e="T6" id="Seg_1822" s="T5">bɨlɨr-gɨ</ta>
            <ta e="T7" id="Seg_1823" s="T6">kaːl-bɨt</ta>
            <ta e="T8" id="Seg_1824" s="T7">očiːha-kaːn</ta>
            <ta e="T9" id="Seg_1825" s="T8">baːr-a</ta>
            <ta e="T10" id="Seg_1826" s="T9">e-bit</ta>
            <ta e="T11" id="Seg_1827" s="T10">ol</ta>
            <ta e="T12" id="Seg_1828" s="T11">očiha-kaːŋ-ŋa</ta>
            <ta e="T13" id="Seg_1829" s="T12">ki͡ehe</ta>
            <ta e="T14" id="Seg_1830" s="T13">aːjɨ</ta>
            <ta e="T15" id="Seg_1831" s="T14">u͡ot</ta>
            <ta e="T16" id="Seg_1832" s="T15">ubaj-ar</ta>
            <ta e="T17" id="Seg_1833" s="T16">bu͡ol-but</ta>
            <ta e="T18" id="Seg_1834" s="T17">manna</ta>
            <ta e="T19" id="Seg_1835" s="T18">baːr</ta>
            <ta e="T20" id="Seg_1836" s="T19">e-bit</ta>
            <ta e="T21" id="Seg_1837" s="T20">biːr</ta>
            <ta e="T22" id="Seg_1838" s="T21">kordʼon</ta>
            <ta e="T23" id="Seg_1839" s="T22">kihi</ta>
            <ta e="T24" id="Seg_1840" s="T23">kupi͡es-ka</ta>
            <ta e="T25" id="Seg_1841" s="T24">orobotnik</ta>
            <ta e="T26" id="Seg_1842" s="T25">bu͡ol-a</ta>
            <ta e="T27" id="Seg_1843" s="T26">hɨldʼ-ar</ta>
            <ta e="T28" id="Seg_1844" s="T27">dʼon-nor</ta>
            <ta e="T29" id="Seg_1845" s="T28">tu͡ok</ta>
            <ta e="T30" id="Seg_1846" s="T29">u͡ot-a</ta>
            <ta e="T31" id="Seg_1847" s="T30">ubaj-ar-a</ta>
            <ta e="T32" id="Seg_1848" s="T31">bu͡ol-l-a</ta>
            <ta e="T33" id="Seg_1849" s="T32">di͡e-n</ta>
            <ta e="T34" id="Seg_1850" s="T33">hög-ön</ta>
            <ta e="T35" id="Seg_1851" s="T34">kepset-el-ler</ta>
            <ta e="T36" id="Seg_1852" s="T35">manɨ͡a-ga</ta>
            <ta e="T37" id="Seg_1853" s="T36">maː</ta>
            <ta e="T38" id="Seg_1854" s="T37">börköj</ta>
            <ta e="T39" id="Seg_1855" s="T38">kihi-ŋ</ta>
            <ta e="T40" id="Seg_1856" s="T39">et-er</ta>
            <ta e="T41" id="Seg_1857" s="T40">kupi͡eh-ɨ-gar</ta>
            <ta e="T42" id="Seg_1858" s="T41">dʼe</ta>
            <ta e="T43" id="Seg_1859" s="T42">zaklaːt-t-a-h-ɨ͡ak</ta>
            <ta e="T44" id="Seg_1860" s="T43">diː-r</ta>
            <ta e="T45" id="Seg_1861" s="T44">min</ta>
            <ta e="T46" id="Seg_1862" s="T45">bar-an</ta>
            <ta e="T47" id="Seg_1863" s="T46">bil-en</ta>
            <ta e="T48" id="Seg_1864" s="T47">kel-i͡e-m</ta>
            <ta e="T49" id="Seg_1865" s="T48">min</ta>
            <ta e="T50" id="Seg_1866" s="T49">kottor-dok-puna</ta>
            <ta e="T51" id="Seg_1867" s="T50">bi͡ek</ta>
            <ta e="T52" id="Seg_1868" s="T51">bosko</ta>
            <ta e="T53" id="Seg_1869" s="T52">ülel-i͡e-m</ta>
            <ta e="T54" id="Seg_1870" s="T53">en</ta>
            <ta e="T55" id="Seg_1871" s="T54">bu͡ollagɨna</ta>
            <ta e="T56" id="Seg_1872" s="T55">karčɨ-nɨ</ta>
            <ta e="T57" id="Seg_1873" s="T56">bi͡er-i͡e-ŋ</ta>
            <ta e="T58" id="Seg_1874" s="T57">o-non</ta>
            <ta e="T59" id="Seg_1875" s="T58">höbül-e-h-en</ta>
            <ta e="T60" id="Seg_1876" s="T59">kaːl-s-al-lar</ta>
            <ta e="T61" id="Seg_1877" s="T60">maː</ta>
            <ta e="T62" id="Seg_1878" s="T61">kihi-ŋ</ta>
            <ta e="T63" id="Seg_1879" s="T62">ki͡ehe</ta>
            <ta e="T64" id="Seg_1880" s="T63">ɨt-ɨ-nan</ta>
            <ta e="T65" id="Seg_1881" s="T64">bar-ar</ta>
            <ta e="T66" id="Seg_1882" s="T65">bu</ta>
            <ta e="T67" id="Seg_1883" s="T66">bar-an</ta>
            <ta e="T68" id="Seg_1884" s="T67">očiːha-ga</ta>
            <ta e="T69" id="Seg_1885" s="T68">kiːr-er</ta>
            <ta e="T70" id="Seg_1886" s="T69">bu</ta>
            <ta e="T71" id="Seg_1887" s="T70">kiːr-bit-e</ta>
            <ta e="T72" id="Seg_1888" s="T71">tüː-leːk</ta>
            <ta e="T73" id="Seg_1889" s="T72">hɨraj-daːk</ta>
            <ta e="T74" id="Seg_1890" s="T73">kihi</ta>
            <ta e="T75" id="Seg_1891" s="T74">olor-or</ta>
            <ta e="T76" id="Seg_1892" s="T75">manna</ta>
            <ta e="T77" id="Seg_1893" s="T76">kɨbɨst-a-kɨbɨst-a</ta>
            <ta e="T78" id="Seg_1894" s="T77">börköj-ü-ŋ</ta>
            <ta e="T79" id="Seg_1895" s="T78">olor-or</ta>
            <ta e="T80" id="Seg_1896" s="T79">ɨskaːmɨja-ga</ta>
            <ta e="T81" id="Seg_1897" s="T80">tüː-leːk</ta>
            <ta e="T82" id="Seg_1898" s="T81">kihi-te</ta>
            <ta e="T83" id="Seg_1899" s="T82">kü͡ös-ten-en</ta>
            <ta e="T84" id="Seg_1900" s="T83">ah-aːrɨ</ta>
            <ta e="T85" id="Seg_1901" s="T84">olor-or</ta>
            <ta e="T86" id="Seg_1902" s="T85">e-bit</ta>
            <ta e="T87" id="Seg_1903" s="T86">dʼe</ta>
            <ta e="T88" id="Seg_1904" s="T87">kü͡öh-ü-n</ta>
            <ta e="T89" id="Seg_1905" s="T88">kotor-u-n-an</ta>
            <ta e="T90" id="Seg_1906" s="T89">ahaː</ta>
            <ta e="T91" id="Seg_1907" s="T90">di͡e-bit</ta>
            <ta e="T92" id="Seg_1908" s="T91">ma-nɨ</ta>
            <ta e="T93" id="Seg_1909" s="T92">ahaː-n</ta>
            <ta e="T94" id="Seg_1910" s="T93">baran</ta>
            <ta e="T95" id="Seg_1911" s="T94">utuj-aːrɨ</ta>
            <ta e="T96" id="Seg_1912" s="T95">hɨp-pɨt-tar</ta>
            <ta e="T97" id="Seg_1913" s="T96">bu</ta>
            <ta e="T98" id="Seg_1914" s="T97">hɨt-tag-ɨna</ta>
            <ta e="T99" id="Seg_1915" s="T98">harsɨ͡arda</ta>
            <ta e="T100" id="Seg_1916" s="T99">erde</ta>
            <ta e="T101" id="Seg_1917" s="T100">kihi-te</ta>
            <ta e="T102" id="Seg_1918" s="T101">kiŋile-nen</ta>
            <ta e="T103" id="Seg_1919" s="T102">bar-an</ta>
            <ta e="T104" id="Seg_1920" s="T103">kaːl-ar</ta>
            <ta e="T105" id="Seg_1921" s="T104">kordʼon</ta>
            <ta e="T106" id="Seg_1922" s="T105">kihi-bit</ta>
            <ta e="T107" id="Seg_1923" s="T106">tur-ar</ta>
            <ta e="T108" id="Seg_1924" s="T107">daː</ta>
            <ta e="T109" id="Seg_1925" s="T108">han-ɨː</ta>
            <ta e="T110" id="Seg_1926" s="T109">bi͡er-er</ta>
            <ta e="T111" id="Seg_1927" s="T110">kajdak</ta>
            <ta e="T112" id="Seg_1928" s="T111">min</ta>
            <ta e="T113" id="Seg_1929" s="T112">manna</ta>
            <ta e="T114" id="Seg_1930" s="T113">hɨldʼ-ɨ-bɨp-pɨ-n</ta>
            <ta e="T115" id="Seg_1931" s="T114">bil-ler-i͡e-m=ij</ta>
            <ta e="T116" id="Seg_1932" s="T115">diː-r</ta>
            <ta e="T117" id="Seg_1933" s="T116">bu</ta>
            <ta e="T118" id="Seg_1934" s="T117">kihi</ta>
            <ta e="T119" id="Seg_1935" s="T118">kanna</ta>
            <ta e="T120" id="Seg_1936" s="T119">bar-bɨt-ɨ-n</ta>
            <ta e="T121" id="Seg_1937" s="T120">bil-en</ta>
            <ta e="T122" id="Seg_1938" s="T121">beli͡e</ta>
            <ta e="T123" id="Seg_1939" s="T122">egel-i͡ek-teːk-pin</ta>
            <ta e="T124" id="Seg_1940" s="T123">di͡e-n</ta>
            <ta e="T125" id="Seg_1941" s="T124">orog-u-n</ta>
            <ta e="T126" id="Seg_1942" s="T125">bat-an</ta>
            <ta e="T127" id="Seg_1943" s="T126">bar-an</ta>
            <ta e="T128" id="Seg_1944" s="T127">bu</ta>
            <ta e="T129" id="Seg_1945" s="T128">bar-an</ta>
            <ta e="T130" id="Seg_1946" s="T129">taːs</ta>
            <ta e="T131" id="Seg_1947" s="T130">kaja-ga</ta>
            <ta e="T132" id="Seg_1948" s="T131">tij-er</ta>
            <ta e="T133" id="Seg_1949" s="T132">manna</ta>
            <ta e="T134" id="Seg_1950" s="T133">kisten-en</ta>
            <ta e="T135" id="Seg_1951" s="T134">kel-bit-e</ta>
            <ta e="T136" id="Seg_1952" s="T135">bu</ta>
            <ta e="T137" id="Seg_1953" s="T136">taːs</ta>
            <ta e="T138" id="Seg_1954" s="T137">kaja</ta>
            <ta e="T139" id="Seg_1955" s="T138">barɨta</ta>
            <ta e="T140" id="Seg_1956" s="T139">kah-ɨ-ll-ɨ-bɨt</ta>
            <ta e="T141" id="Seg_1957" s="T140">dʼon</ta>
            <ta e="T142" id="Seg_1958" s="T141">köːkün-e</ta>
            <ta e="T143" id="Seg_1959" s="T142">di͡e-n</ta>
            <ta e="T144" id="Seg_1960" s="T143">haŋa-lara</ta>
            <ta e="T145" id="Seg_1961" s="T144">di͡e-n</ta>
            <ta e="T146" id="Seg_1962" s="T145">öŋöj-ön</ta>
            <ta e="T147" id="Seg_1963" s="T146">kör-büt-e</ta>
            <ta e="T148" id="Seg_1964" s="T147">kajagah-ɨ-nan</ta>
            <ta e="T149" id="Seg_1965" s="T148">kihi-lere</ta>
            <ta e="T150" id="Seg_1966" s="T149">barɨta</ta>
            <ta e="T151" id="Seg_1967" s="T150">tüː-leːk</ta>
            <ta e="T152" id="Seg_1968" s="T151">hirej-deːk</ta>
            <ta e="T153" id="Seg_1969" s="T152">manna</ta>
            <ta e="T154" id="Seg_1970" s="T153">ü͡öm-en</ta>
            <ta e="T155" id="Seg_1971" s="T154">kiːr-bit</ta>
            <ta e="T156" id="Seg_1972" s="T155">bu</ta>
            <ta e="T157" id="Seg_1973" s="T156">kiːr-en</ta>
            <ta e="T158" id="Seg_1974" s="T157">bu</ta>
            <ta e="T159" id="Seg_1975" s="T158">dʼi͡e-ge</ta>
            <ta e="T160" id="Seg_1976" s="T159">balagaŋ-ŋa</ta>
            <ta e="T161" id="Seg_1977" s="T160">kiːr-en</ta>
            <ta e="T162" id="Seg_1978" s="T161">olor-or</ta>
            <ta e="T163" id="Seg_1979" s="T162">bu</ta>
            <ta e="T164" id="Seg_1980" s="T163">olor-but-a</ta>
            <ta e="T165" id="Seg_1981" s="T164">biːr</ta>
            <ta e="T166" id="Seg_1982" s="T165">dʼaktar</ta>
            <ta e="T167" id="Seg_1983" s="T166">baːr</ta>
            <ta e="T168" id="Seg_1984" s="T167">e-bit</ta>
            <ta e="T169" id="Seg_1985" s="T168">ol</ta>
            <ta e="T170" id="Seg_1986" s="T169">orgujkaːn</ta>
            <ta e="T171" id="Seg_1987" s="T170">ɨjɨt-ar</ta>
            <ta e="T172" id="Seg_1988" s="T171">kaja</ta>
            <ta e="T173" id="Seg_1989" s="T172">tug-u-nan</ta>
            <ta e="T174" id="Seg_1990" s="T173">naːdɨj-an</ta>
            <ta e="T175" id="Seg_1991" s="T174">kel-li-ŋ</ta>
            <ta e="T176" id="Seg_1992" s="T175">diː-r</ta>
            <ta e="T177" id="Seg_1993" s="T176">ma-nɨ</ta>
            <ta e="T178" id="Seg_1994" s="T177">et-er</ta>
            <ta e="T179" id="Seg_1995" s="T178">hinnʼiges</ta>
            <ta e="T180" id="Seg_1996" s="T179">hir-bi-nen</ta>
            <ta e="T181" id="Seg_1997" s="T180">diː-r</ta>
            <ta e="T182" id="Seg_1998" s="T181">onu͡o-ka</ta>
            <ta e="T183" id="Seg_1999" s="T182">maː</ta>
            <ta e="T184" id="Seg_2000" s="T183">dʼaktar-a</ta>
            <ta e="T185" id="Seg_2001" s="T184">tug-u</ta>
            <ta e="T186" id="Seg_2002" s="T185">ere</ta>
            <ta e="T187" id="Seg_2003" s="T186">hɨmnagas</ta>
            <ta e="T188" id="Seg_2004" s="T187">bagajɨ-nɨ</ta>
            <ta e="T189" id="Seg_2005" s="T188">tuttar-an</ta>
            <ta e="T190" id="Seg_2006" s="T189">keːh-er</ta>
            <ta e="T191" id="Seg_2007" s="T190">o-nu</ta>
            <ta e="T192" id="Seg_2008" s="T191">kör-ü-mne</ta>
            <ta e="T193" id="Seg_2009" s="T192">ere</ta>
            <ta e="T194" id="Seg_2010" s="T193">hi͡eb-i-ger</ta>
            <ta e="T195" id="Seg_2011" s="T194">uk-t-an</ta>
            <ta e="T196" id="Seg_2012" s="T195">keːh-er</ta>
            <ta e="T197" id="Seg_2013" s="T196">dʼaktar-a</ta>
            <ta e="T198" id="Seg_2014" s="T197">et-er</ta>
            <ta e="T199" id="Seg_2015" s="T198">dʼe</ta>
            <ta e="T200" id="Seg_2016" s="T199">kɨtaːt</ta>
            <ta e="T201" id="Seg_2017" s="T200">bar-an</ta>
            <ta e="T202" id="Seg_2018" s="T201">kör</ta>
            <ta e="T203" id="Seg_2019" s="T202">manna</ta>
            <ta e="T204" id="Seg_2020" s="T203">dʼon-nor-u-m</ta>
            <ta e="T205" id="Seg_2021" s="T204">kör-dök-törüne</ta>
            <ta e="T206" id="Seg_2022" s="T205">kihi-l-i͡ek-tere</ta>
            <ta e="T207" id="Seg_2023" s="T206">hu͡og-a</ta>
            <ta e="T208" id="Seg_2024" s="T207">di͡e-bit</ta>
            <ta e="T209" id="Seg_2025" s="T208">o-nu</ta>
            <ta e="T210" id="Seg_2026" s="T209">ist-eːt</ta>
            <ta e="T211" id="Seg_2027" s="T210">kordʼon</ta>
            <ta e="T212" id="Seg_2028" s="T211">kihi</ta>
            <ta e="T213" id="Seg_2029" s="T212">taks-ɨ-bɨt</ta>
            <ta e="T214" id="Seg_2030" s="T213">ɨt-ɨ-gar</ta>
            <ta e="T215" id="Seg_2031" s="T214">olor-but</ta>
            <ta e="T216" id="Seg_2032" s="T215">daː</ta>
            <ta e="T217" id="Seg_2033" s="T216">bar-ar-ga</ta>
            <ta e="T218" id="Seg_2034" s="T217">bu͡ol-but</ta>
            <ta e="T219" id="Seg_2035" s="T218">bu</ta>
            <ta e="T220" id="Seg_2036" s="T219">bar-an</ta>
            <ta e="T221" id="Seg_2037" s="T220">ih-en</ta>
            <ta e="T222" id="Seg_2038" s="T221">kenn-i-n</ta>
            <ta e="T223" id="Seg_2039" s="T222">kör-büt-e</ta>
            <ta e="T224" id="Seg_2040" s="T223">biːr</ta>
            <ta e="T225" id="Seg_2041" s="T224">kihi</ta>
            <ta e="T226" id="Seg_2042" s="T225">batɨh-an</ta>
            <ta e="T227" id="Seg_2043" s="T226">ih-er</ta>
            <ta e="T228" id="Seg_2044" s="T227">on-tu-ta</ta>
            <ta e="T229" id="Seg_2045" s="T228">hit-en</ta>
            <ta e="T230" id="Seg_2046" s="T229">ih-er-i-n</ta>
            <ta e="T231" id="Seg_2047" s="T230">kör-ön</ta>
            <ta e="T232" id="Seg_2048" s="T231">maː</ta>
            <ta e="T233" id="Seg_2049" s="T232">kihi</ta>
            <ta e="T234" id="Seg_2050" s="T233">köt-ü-p-püt</ta>
            <ta e="T235" id="Seg_2051" s="T234">tüː-leːk</ta>
            <ta e="T236" id="Seg_2052" s="T235">hɨraj-daːk</ta>
            <ta e="T237" id="Seg_2053" s="T236">kihi-te</ta>
            <ta e="T238" id="Seg_2054" s="T237">hɨrga-tɨ-n</ta>
            <ta e="T239" id="Seg_2055" s="T238">kelin</ta>
            <ta e="T240" id="Seg_2056" s="T239">atag-ɨ-ttan</ta>
            <ta e="T241" id="Seg_2057" s="T240">ɨl-an</ta>
            <ta e="T242" id="Seg_2058" s="T241">ikki</ta>
            <ta e="T243" id="Seg_2059" s="T242">atag-ɨ-n</ta>
            <ta e="T244" id="Seg_2060" s="T243">kaːl-lar-bɨt</ta>
            <ta e="T245" id="Seg_2061" s="T244">tübes-t-er-gi-n</ta>
            <ta e="T246" id="Seg_2062" s="T245">kör-dör-ü͡ök</ta>
            <ta e="T247" id="Seg_2063" s="T246">e-ti-m</ta>
            <ta e="T248" id="Seg_2064" s="T247">di͡e-n</ta>
            <ta e="T249" id="Seg_2065" s="T248">kahɨːtaː-n</ta>
            <ta e="T250" id="Seg_2066" s="T249">kaːl-bɨt</ta>
            <ta e="T251" id="Seg_2067" s="T250">maː</ta>
            <ta e="T252" id="Seg_2068" s="T251">u͡ol</ta>
            <ta e="T253" id="Seg_2069" s="T252">dʼi͡e-ti-ger</ta>
            <ta e="T254" id="Seg_2070" s="T253">kel-en</ta>
            <ta e="T255" id="Seg_2071" s="T254">beli͡e</ta>
            <ta e="T256" id="Seg_2072" s="T255">egel-li-m</ta>
            <ta e="T257" id="Seg_2073" s="T256">di͡e-n</ta>
            <ta e="T258" id="Seg_2074" s="T257">karmaːn-ɨ-ttan</ta>
            <ta e="T259" id="Seg_2075" s="T258">hɨmnagah-ɨ-n</ta>
            <ta e="T260" id="Seg_2076" s="T259">tahaːr-bɨt-a</ta>
            <ta e="T261" id="Seg_2077" s="T260">kara</ta>
            <ta e="T262" id="Seg_2078" s="T261">hahɨl</ta>
            <ta e="T263" id="Seg_2079" s="T262">bu͡ol-but</ta>
            <ta e="T264" id="Seg_2080" s="T263">kupi͡eh-a</ta>
            <ta e="T265" id="Seg_2081" s="T264">bu͡ollagɨna</ta>
            <ta e="T266" id="Seg_2082" s="T265">karčɨ</ta>
            <ta e="T267" id="Seg_2083" s="T266">bögö</ta>
            <ta e="T268" id="Seg_2084" s="T267">bi͡er-bit</ta>
            <ta e="T269" id="Seg_2085" s="T268">onon</ta>
            <ta e="T270" id="Seg_2086" s="T269">baːj</ta>
            <ta e="T271" id="Seg_2087" s="T270">kihi</ta>
            <ta e="T272" id="Seg_2088" s="T271">bu͡ol-but</ta>
            <ta e="T273" id="Seg_2089" s="T272">ma-nɨ</ta>
            <ta e="T274" id="Seg_2090" s="T273">kɨrdʼagas-tar</ta>
            <ta e="T275" id="Seg_2091" s="T274">ep-pit-ter</ta>
            <ta e="T276" id="Seg_2092" s="T275">en</ta>
            <ta e="T277" id="Seg_2093" s="T276">tij-e</ta>
            <ta e="T278" id="Seg_2094" s="T277">hɨldʼ-ɨ-bɨk-kɨn</ta>
            <ta e="T279" id="Seg_2095" s="T278">bɨlɨr</ta>
            <ta e="T280" id="Seg_2096" s="T279">ɨraːktaːgɨ</ta>
            <ta e="T281" id="Seg_2097" s="T280">ölbüge-ti-tten</ta>
            <ta e="T282" id="Seg_2098" s="T281">küren-en</ta>
            <ta e="T283" id="Seg_2099" s="T282">kel-bit</ta>
            <ta e="T284" id="Seg_2100" s="T283">dʼon-nor-go</ta>
            <ta e="T285" id="Seg_2101" s="T284">o-lor-u-ŋ</ta>
            <ta e="T286" id="Seg_2102" s="T285">hah-an</ta>
            <ta e="T287" id="Seg_2103" s="T286">kaja</ta>
            <ta e="T288" id="Seg_2104" s="T287">ih-i-ger</ta>
            <ta e="T289" id="Seg_2105" s="T288">dʼi͡e-len-ni-ler</ta>
            <ta e="T290" id="Seg_2106" s="T289">maŋnaj</ta>
            <ta e="T291" id="Seg_2107" s="T290">en</ta>
            <ta e="T292" id="Seg_2108" s="T291">očiːha-ga</ta>
            <ta e="T293" id="Seg_2109" s="T292">kör-öːččü</ta>
            <ta e="T294" id="Seg_2110" s="T293">kihi-ŋ</ta>
            <ta e="T295" id="Seg_2111" s="T294">ol</ta>
            <ta e="T296" id="Seg_2112" s="T295">kiniler</ta>
            <ta e="T297" id="Seg_2113" s="T296">bulčut-tara</ta>
            <ta e="T298" id="Seg_2114" s="T297">kɨːl-dʼɨt-tara</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_2115" s="T0">tɨ͡a</ta>
            <ta e="T2" id="Seg_2116" s="T1">hir-tI-GAr</ta>
            <ta e="T3" id="Seg_2117" s="T2">kömük-LAːK</ta>
            <ta e="T4" id="Seg_2118" s="T3">bagajɨ</ta>
            <ta e="T5" id="Seg_2119" s="T4">hir-GA</ta>
            <ta e="T6" id="Seg_2120" s="T5">bɨlɨr-GI</ta>
            <ta e="T7" id="Seg_2121" s="T6">kaːl-BIT</ta>
            <ta e="T8" id="Seg_2122" s="T7">očiha-kAːN</ta>
            <ta e="T9" id="Seg_2123" s="T8">baːr-tA</ta>
            <ta e="T10" id="Seg_2124" s="T9">e-BIT</ta>
            <ta e="T11" id="Seg_2125" s="T10">ol</ta>
            <ta e="T12" id="Seg_2126" s="T11">očiha-kAːN-GA</ta>
            <ta e="T13" id="Seg_2127" s="T12">ki͡ehe</ta>
            <ta e="T14" id="Seg_2128" s="T13">aːjɨ</ta>
            <ta e="T15" id="Seg_2129" s="T14">u͡ot</ta>
            <ta e="T16" id="Seg_2130" s="T15">ubaj-Ar</ta>
            <ta e="T17" id="Seg_2131" s="T16">bu͡ol-BIT</ta>
            <ta e="T18" id="Seg_2132" s="T17">manna</ta>
            <ta e="T19" id="Seg_2133" s="T18">baːr</ta>
            <ta e="T20" id="Seg_2134" s="T19">e-BIT</ta>
            <ta e="T21" id="Seg_2135" s="T20">biːr</ta>
            <ta e="T22" id="Seg_2136" s="T21">kordʼon</ta>
            <ta e="T23" id="Seg_2137" s="T22">kihi</ta>
            <ta e="T24" id="Seg_2138" s="T23">kupi͡es-GA</ta>
            <ta e="T25" id="Seg_2139" s="T24">orobu͡otnik</ta>
            <ta e="T26" id="Seg_2140" s="T25">bu͡ol-A</ta>
            <ta e="T27" id="Seg_2141" s="T26">hɨrɨt-Ar</ta>
            <ta e="T28" id="Seg_2142" s="T27">dʼon-LAr</ta>
            <ta e="T29" id="Seg_2143" s="T28">tu͡ok</ta>
            <ta e="T30" id="Seg_2144" s="T29">u͡ot-tA</ta>
            <ta e="T31" id="Seg_2145" s="T30">ubaj-Ar-tA</ta>
            <ta e="T32" id="Seg_2146" s="T31">bu͡ol-TI-tA</ta>
            <ta e="T33" id="Seg_2147" s="T32">di͡e-An</ta>
            <ta e="T34" id="Seg_2148" s="T33">hök-An</ta>
            <ta e="T35" id="Seg_2149" s="T34">kepset-Ar-LAr</ta>
            <ta e="T36" id="Seg_2150" s="T35">bu-GA</ta>
            <ta e="T37" id="Seg_2151" s="T36">bu</ta>
            <ta e="T38" id="Seg_2152" s="T37">börköj</ta>
            <ta e="T39" id="Seg_2153" s="T38">kihi-ŋ</ta>
            <ta e="T40" id="Seg_2154" s="T39">et-Ar</ta>
            <ta e="T41" id="Seg_2155" s="T40">kupi͡es-tI-GAr</ta>
            <ta e="T42" id="Seg_2156" s="T41">dʼe</ta>
            <ta e="T43" id="Seg_2157" s="T42">zaklaːt-LAː-A-s-IAk</ta>
            <ta e="T44" id="Seg_2158" s="T43">di͡e-Ar</ta>
            <ta e="T45" id="Seg_2159" s="T44">min</ta>
            <ta e="T46" id="Seg_2160" s="T45">bar-An</ta>
            <ta e="T47" id="Seg_2161" s="T46">bil-An</ta>
            <ta e="T48" id="Seg_2162" s="T47">kel-IAK-m</ta>
            <ta e="T49" id="Seg_2163" s="T48">min</ta>
            <ta e="T50" id="Seg_2164" s="T49">kottor-TAK-BInA</ta>
            <ta e="T51" id="Seg_2165" s="T50">bi͡ek</ta>
            <ta e="T52" id="Seg_2166" s="T51">bosko</ta>
            <ta e="T53" id="Seg_2167" s="T52">üleleː-IAK-m</ta>
            <ta e="T54" id="Seg_2168" s="T53">en</ta>
            <ta e="T55" id="Seg_2169" s="T54">bu͡ollagɨna</ta>
            <ta e="T56" id="Seg_2170" s="T55">karčɨ-nI</ta>
            <ta e="T57" id="Seg_2171" s="T56">bi͡er-IAK-ŋ</ta>
            <ta e="T58" id="Seg_2172" s="T57">ol-nAn</ta>
            <ta e="T59" id="Seg_2173" s="T58">höbüleː-A-s-An</ta>
            <ta e="T60" id="Seg_2174" s="T59">kaːl-s-Ar-LAr</ta>
            <ta e="T61" id="Seg_2175" s="T60">bu</ta>
            <ta e="T62" id="Seg_2176" s="T61">kihi-ŋ</ta>
            <ta e="T63" id="Seg_2177" s="T62">ki͡ehe</ta>
            <ta e="T64" id="Seg_2178" s="T63">ɨt-tI-nAn</ta>
            <ta e="T65" id="Seg_2179" s="T64">bar-Ar</ta>
            <ta e="T66" id="Seg_2180" s="T65">bu</ta>
            <ta e="T67" id="Seg_2181" s="T66">bar-An</ta>
            <ta e="T68" id="Seg_2182" s="T67">očiha-GA</ta>
            <ta e="T69" id="Seg_2183" s="T68">kiːr-Ar</ta>
            <ta e="T70" id="Seg_2184" s="T69">bu</ta>
            <ta e="T71" id="Seg_2185" s="T70">kiːr-BIT-tA</ta>
            <ta e="T72" id="Seg_2186" s="T71">tüː-LAːK</ta>
            <ta e="T73" id="Seg_2187" s="T72">hɨraj-LAːK</ta>
            <ta e="T74" id="Seg_2188" s="T73">kihi</ta>
            <ta e="T75" id="Seg_2189" s="T74">olor-Ar</ta>
            <ta e="T76" id="Seg_2190" s="T75">manna</ta>
            <ta e="T77" id="Seg_2191" s="T76">kɨbɨhɨn-A-kɨbɨhɨn-A</ta>
            <ta e="T78" id="Seg_2192" s="T77">börköj-I-ŋ</ta>
            <ta e="T79" id="Seg_2193" s="T78">olor-Ar</ta>
            <ta e="T80" id="Seg_2194" s="T79">ɨskaːmɨja-GA</ta>
            <ta e="T81" id="Seg_2195" s="T80">tüː-LAːK</ta>
            <ta e="T82" id="Seg_2196" s="T81">kihi-tA</ta>
            <ta e="T83" id="Seg_2197" s="T82">kü͡ös-LAN-An</ta>
            <ta e="T84" id="Seg_2198" s="T83">ahaː-AːrI</ta>
            <ta e="T85" id="Seg_2199" s="T84">olor-Ar</ta>
            <ta e="T86" id="Seg_2200" s="T85">e-BIT</ta>
            <ta e="T87" id="Seg_2201" s="T86">dʼe</ta>
            <ta e="T88" id="Seg_2202" s="T87">kü͡ös-tI-n</ta>
            <ta e="T89" id="Seg_2203" s="T88">kotor-I-n-An</ta>
            <ta e="T90" id="Seg_2204" s="T89">ahaː</ta>
            <ta e="T91" id="Seg_2205" s="T90">di͡e-BIT</ta>
            <ta e="T92" id="Seg_2206" s="T91">bu-nI</ta>
            <ta e="T93" id="Seg_2207" s="T92">ahaː-An</ta>
            <ta e="T94" id="Seg_2208" s="T93">baran</ta>
            <ta e="T95" id="Seg_2209" s="T94">utuj-AːrI</ta>
            <ta e="T96" id="Seg_2210" s="T95">hɨt-BIT-LAr</ta>
            <ta e="T97" id="Seg_2211" s="T96">bu</ta>
            <ta e="T98" id="Seg_2212" s="T97">hɨt-TAK-InA</ta>
            <ta e="T99" id="Seg_2213" s="T98">harsi͡erda</ta>
            <ta e="T100" id="Seg_2214" s="T99">erde</ta>
            <ta e="T101" id="Seg_2215" s="T100">kihi-tA</ta>
            <ta e="T102" id="Seg_2216" s="T101">kiŋile-nAn</ta>
            <ta e="T103" id="Seg_2217" s="T102">bar-An</ta>
            <ta e="T104" id="Seg_2218" s="T103">kaːl-Ar</ta>
            <ta e="T105" id="Seg_2219" s="T104">kordʼon</ta>
            <ta e="T106" id="Seg_2220" s="T105">kihi-BIt</ta>
            <ta e="T107" id="Seg_2221" s="T106">tur-Ar</ta>
            <ta e="T108" id="Seg_2222" s="T107">da</ta>
            <ta e="T109" id="Seg_2223" s="T108">hanaː-A</ta>
            <ta e="T110" id="Seg_2224" s="T109">bi͡er-Ar</ta>
            <ta e="T111" id="Seg_2225" s="T110">kajdak</ta>
            <ta e="T112" id="Seg_2226" s="T111">min</ta>
            <ta e="T113" id="Seg_2227" s="T112">manna</ta>
            <ta e="T114" id="Seg_2228" s="T113">hɨrɨt-I-BIT-BI-n</ta>
            <ta e="T115" id="Seg_2229" s="T114">bil-TAr-IAK-m=Ij</ta>
            <ta e="T116" id="Seg_2230" s="T115">di͡e-Ar</ta>
            <ta e="T117" id="Seg_2231" s="T116">bu</ta>
            <ta e="T118" id="Seg_2232" s="T117">kihi</ta>
            <ta e="T119" id="Seg_2233" s="T118">kanna</ta>
            <ta e="T120" id="Seg_2234" s="T119">bar-BIT-tI-n</ta>
            <ta e="T121" id="Seg_2235" s="T120">bil-An</ta>
            <ta e="T122" id="Seg_2236" s="T121">beli͡e</ta>
            <ta e="T123" id="Seg_2237" s="T122">egel-IAK-LAːK-BIn</ta>
            <ta e="T124" id="Seg_2238" s="T123">di͡e-An</ta>
            <ta e="T125" id="Seg_2239" s="T124">orok-tI-n</ta>
            <ta e="T126" id="Seg_2240" s="T125">bat-An</ta>
            <ta e="T127" id="Seg_2241" s="T126">bar-An</ta>
            <ta e="T128" id="Seg_2242" s="T127">bu</ta>
            <ta e="T129" id="Seg_2243" s="T128">bar-An</ta>
            <ta e="T130" id="Seg_2244" s="T129">taːs</ta>
            <ta e="T131" id="Seg_2245" s="T130">kaja-GA</ta>
            <ta e="T132" id="Seg_2246" s="T131">tij-Ar</ta>
            <ta e="T133" id="Seg_2247" s="T132">manna</ta>
            <ta e="T134" id="Seg_2248" s="T133">kisten-An</ta>
            <ta e="T135" id="Seg_2249" s="T134">kel-BIT-tA</ta>
            <ta e="T136" id="Seg_2250" s="T135">bu</ta>
            <ta e="T137" id="Seg_2251" s="T136">taːs</ta>
            <ta e="T138" id="Seg_2252" s="T137">kaja</ta>
            <ta e="T139" id="Seg_2253" s="T138">barɨta</ta>
            <ta e="T140" id="Seg_2254" s="T139">kas-I-LIN-I-BIT</ta>
            <ta e="T141" id="Seg_2255" s="T140">dʼon</ta>
            <ta e="T142" id="Seg_2256" s="T141">köːkün-tA</ta>
            <ta e="T143" id="Seg_2257" s="T142">di͡e-An</ta>
            <ta e="T144" id="Seg_2258" s="T143">haŋa-LArA</ta>
            <ta e="T145" id="Seg_2259" s="T144">di͡e-An</ta>
            <ta e="T146" id="Seg_2260" s="T145">öŋöj-An</ta>
            <ta e="T147" id="Seg_2261" s="T146">kör-BIT-tA</ta>
            <ta e="T148" id="Seg_2262" s="T147">kajagas-I-nAn</ta>
            <ta e="T149" id="Seg_2263" s="T148">kihi-LArA</ta>
            <ta e="T150" id="Seg_2264" s="T149">barɨta</ta>
            <ta e="T151" id="Seg_2265" s="T150">tüː-LAːK</ta>
            <ta e="T152" id="Seg_2266" s="T151">hɨraj-LAːK</ta>
            <ta e="T153" id="Seg_2267" s="T152">manna</ta>
            <ta e="T154" id="Seg_2268" s="T153">ü͡öŋ-An</ta>
            <ta e="T155" id="Seg_2269" s="T154">kiːr-BIT</ta>
            <ta e="T156" id="Seg_2270" s="T155">bu</ta>
            <ta e="T157" id="Seg_2271" s="T156">kiːr-An</ta>
            <ta e="T158" id="Seg_2272" s="T157">bu</ta>
            <ta e="T159" id="Seg_2273" s="T158">dʼi͡e-GA</ta>
            <ta e="T160" id="Seg_2274" s="T159">balagan-GA</ta>
            <ta e="T161" id="Seg_2275" s="T160">kiːr-An</ta>
            <ta e="T162" id="Seg_2276" s="T161">olor-Ar</ta>
            <ta e="T163" id="Seg_2277" s="T162">bu</ta>
            <ta e="T164" id="Seg_2278" s="T163">olor-BIT-tA</ta>
            <ta e="T165" id="Seg_2279" s="T164">biːr</ta>
            <ta e="T166" id="Seg_2280" s="T165">dʼaktar</ta>
            <ta e="T167" id="Seg_2281" s="T166">baːr</ta>
            <ta e="T168" id="Seg_2282" s="T167">e-BIT</ta>
            <ta e="T169" id="Seg_2283" s="T168">ol</ta>
            <ta e="T170" id="Seg_2284" s="T169">orgujakaːn</ta>
            <ta e="T171" id="Seg_2285" s="T170">ɨjɨt-Ar</ta>
            <ta e="T172" id="Seg_2286" s="T171">kaja</ta>
            <ta e="T173" id="Seg_2287" s="T172">tu͡ok-I-nAn</ta>
            <ta e="T174" id="Seg_2288" s="T173">naːdɨj-An</ta>
            <ta e="T175" id="Seg_2289" s="T174">kel-TI-ŋ</ta>
            <ta e="T176" id="Seg_2290" s="T175">di͡e-Ar</ta>
            <ta e="T177" id="Seg_2291" s="T176">bu-nI</ta>
            <ta e="T178" id="Seg_2292" s="T177">et-Ar</ta>
            <ta e="T179" id="Seg_2293" s="T178">hinnʼiges</ta>
            <ta e="T180" id="Seg_2294" s="T179">hir-BI-nAn</ta>
            <ta e="T181" id="Seg_2295" s="T180">di͡e-Ar</ta>
            <ta e="T182" id="Seg_2296" s="T181">ol-GA</ta>
            <ta e="T183" id="Seg_2297" s="T182">bu</ta>
            <ta e="T184" id="Seg_2298" s="T183">dʼaktar-tA</ta>
            <ta e="T185" id="Seg_2299" s="T184">tu͡ok-nI</ta>
            <ta e="T186" id="Seg_2300" s="T185">ere</ta>
            <ta e="T187" id="Seg_2301" s="T186">hɨmnagas</ta>
            <ta e="T188" id="Seg_2302" s="T187">bagajɨ-nI</ta>
            <ta e="T189" id="Seg_2303" s="T188">tuttar-An</ta>
            <ta e="T190" id="Seg_2304" s="T189">keːs-Ar</ta>
            <ta e="T191" id="Seg_2305" s="T190">ol-nI</ta>
            <ta e="T192" id="Seg_2306" s="T191">kör-I-mInA</ta>
            <ta e="T193" id="Seg_2307" s="T192">ere</ta>
            <ta e="T194" id="Seg_2308" s="T193">hi͡ep-tI-GAr</ta>
            <ta e="T195" id="Seg_2309" s="T194">uk-t-An</ta>
            <ta e="T196" id="Seg_2310" s="T195">keːs-Ar</ta>
            <ta e="T197" id="Seg_2311" s="T196">dʼaktar-tA</ta>
            <ta e="T198" id="Seg_2312" s="T197">et-Ar</ta>
            <ta e="T199" id="Seg_2313" s="T198">dʼe</ta>
            <ta e="T200" id="Seg_2314" s="T199">kɨtaːt</ta>
            <ta e="T201" id="Seg_2315" s="T200">bar-An</ta>
            <ta e="T202" id="Seg_2316" s="T201">kör</ta>
            <ta e="T203" id="Seg_2317" s="T202">manna</ta>
            <ta e="T204" id="Seg_2318" s="T203">dʼon-LAr-I-m</ta>
            <ta e="T205" id="Seg_2319" s="T204">kör-TAK-TArInA</ta>
            <ta e="T206" id="Seg_2320" s="T205">kihi-LAː-IAK-LArA</ta>
            <ta e="T207" id="Seg_2321" s="T206">hu͡ok-tA</ta>
            <ta e="T208" id="Seg_2322" s="T207">di͡e-BIT</ta>
            <ta e="T209" id="Seg_2323" s="T208">ol-nI</ta>
            <ta e="T210" id="Seg_2324" s="T209">ihit-AːT</ta>
            <ta e="T211" id="Seg_2325" s="T210">kordʼon</ta>
            <ta e="T212" id="Seg_2326" s="T211">kihi</ta>
            <ta e="T213" id="Seg_2327" s="T212">tagɨs-I-BIT</ta>
            <ta e="T214" id="Seg_2328" s="T213">ɨt-tI-GAr</ta>
            <ta e="T215" id="Seg_2329" s="T214">olor-BIT</ta>
            <ta e="T216" id="Seg_2330" s="T215">da</ta>
            <ta e="T217" id="Seg_2331" s="T216">bar-Ar-GA</ta>
            <ta e="T218" id="Seg_2332" s="T217">bu͡ol-BIT</ta>
            <ta e="T219" id="Seg_2333" s="T218">bu</ta>
            <ta e="T220" id="Seg_2334" s="T219">bar-An</ta>
            <ta e="T221" id="Seg_2335" s="T220">is-An</ta>
            <ta e="T222" id="Seg_2336" s="T221">kelin-tI-n</ta>
            <ta e="T223" id="Seg_2337" s="T222">kör-BIT-tA</ta>
            <ta e="T224" id="Seg_2338" s="T223">biːr</ta>
            <ta e="T225" id="Seg_2339" s="T224">kihi</ta>
            <ta e="T226" id="Seg_2340" s="T225">batɨs-An</ta>
            <ta e="T227" id="Seg_2341" s="T226">is-Ar</ta>
            <ta e="T228" id="Seg_2342" s="T227">ol-tI-tA</ta>
            <ta e="T229" id="Seg_2343" s="T228">hit-An</ta>
            <ta e="T230" id="Seg_2344" s="T229">is-Ar-tI-n</ta>
            <ta e="T231" id="Seg_2345" s="T230">kör-An</ta>
            <ta e="T232" id="Seg_2346" s="T231">bu</ta>
            <ta e="T233" id="Seg_2347" s="T232">kihi</ta>
            <ta e="T234" id="Seg_2348" s="T233">köt-I-t-BIT</ta>
            <ta e="T235" id="Seg_2349" s="T234">tüː-LAːK</ta>
            <ta e="T236" id="Seg_2350" s="T235">hɨraj-LAːK</ta>
            <ta e="T237" id="Seg_2351" s="T236">kihi-tA</ta>
            <ta e="T238" id="Seg_2352" s="T237">hɨrga-tI-n</ta>
            <ta e="T239" id="Seg_2353" s="T238">kelin</ta>
            <ta e="T240" id="Seg_2354" s="T239">atak-tI-ttAn</ta>
            <ta e="T241" id="Seg_2355" s="T240">ɨl-An</ta>
            <ta e="T242" id="Seg_2356" s="T241">ikki</ta>
            <ta e="T243" id="Seg_2357" s="T242">atak-tI-n</ta>
            <ta e="T244" id="Seg_2358" s="T243">kaːl-TAr-BIT</ta>
            <ta e="T245" id="Seg_2359" s="T244">tübes-n-Ar-GI-n</ta>
            <ta e="T246" id="Seg_2360" s="T245">kör-TAr-IAK</ta>
            <ta e="T247" id="Seg_2361" s="T246">e-TI-m</ta>
            <ta e="T248" id="Seg_2362" s="T247">di͡e-An</ta>
            <ta e="T249" id="Seg_2363" s="T248">kahɨːtaː-An</ta>
            <ta e="T250" id="Seg_2364" s="T249">kaːl-BIT</ta>
            <ta e="T251" id="Seg_2365" s="T250">bu</ta>
            <ta e="T252" id="Seg_2366" s="T251">u͡ol</ta>
            <ta e="T253" id="Seg_2367" s="T252">dʼi͡e-tI-GAr</ta>
            <ta e="T254" id="Seg_2368" s="T253">kel-An</ta>
            <ta e="T255" id="Seg_2369" s="T254">beli͡e</ta>
            <ta e="T256" id="Seg_2370" s="T255">egel-TI-m</ta>
            <ta e="T257" id="Seg_2371" s="T256">di͡e-An</ta>
            <ta e="T258" id="Seg_2372" s="T257">karmaːn-tI-ttAn</ta>
            <ta e="T259" id="Seg_2373" s="T258">hɨmnagas-tI-n</ta>
            <ta e="T260" id="Seg_2374" s="T259">tahaːr-BIT-tA</ta>
            <ta e="T261" id="Seg_2375" s="T260">kara</ta>
            <ta e="T262" id="Seg_2376" s="T261">hahɨl</ta>
            <ta e="T263" id="Seg_2377" s="T262">bu͡ol-BIT</ta>
            <ta e="T264" id="Seg_2378" s="T263">kupi͡es-tA</ta>
            <ta e="T265" id="Seg_2379" s="T264">bu͡ollagɨna</ta>
            <ta e="T266" id="Seg_2380" s="T265">karčɨ</ta>
            <ta e="T267" id="Seg_2381" s="T266">bögö</ta>
            <ta e="T268" id="Seg_2382" s="T267">bi͡er-BIT</ta>
            <ta e="T269" id="Seg_2383" s="T268">onon</ta>
            <ta e="T270" id="Seg_2384" s="T269">baːj</ta>
            <ta e="T271" id="Seg_2385" s="T270">kihi</ta>
            <ta e="T272" id="Seg_2386" s="T271">bu͡ol-BIT</ta>
            <ta e="T273" id="Seg_2387" s="T272">bu-nI</ta>
            <ta e="T274" id="Seg_2388" s="T273">kɨrdʼagas-LAr</ta>
            <ta e="T275" id="Seg_2389" s="T274">et-BIT-LAr</ta>
            <ta e="T276" id="Seg_2390" s="T275">en</ta>
            <ta e="T277" id="Seg_2391" s="T276">tij-A</ta>
            <ta e="T278" id="Seg_2392" s="T277">hɨrɨt-I-BIT-GIn</ta>
            <ta e="T279" id="Seg_2393" s="T278">bɨlɨr</ta>
            <ta e="T280" id="Seg_2394" s="T279">ɨraːktaːgɨ</ta>
            <ta e="T281" id="Seg_2395" s="T280">ölbüge-tI-ttAn</ta>
            <ta e="T282" id="Seg_2396" s="T281">küren-An</ta>
            <ta e="T283" id="Seg_2397" s="T282">kel-BIT</ta>
            <ta e="T284" id="Seg_2398" s="T283">dʼon-LAr-GA</ta>
            <ta e="T285" id="Seg_2399" s="T284">ol-LAr-I-ŋ</ta>
            <ta e="T286" id="Seg_2400" s="T285">has-An</ta>
            <ta e="T287" id="Seg_2401" s="T286">kaja</ta>
            <ta e="T288" id="Seg_2402" s="T287">is-tI-GAr</ta>
            <ta e="T289" id="Seg_2403" s="T288">dʼi͡e-LAN-TI-LAr</ta>
            <ta e="T290" id="Seg_2404" s="T289">maŋnaj</ta>
            <ta e="T291" id="Seg_2405" s="T290">en</ta>
            <ta e="T292" id="Seg_2406" s="T291">očiha-GA</ta>
            <ta e="T293" id="Seg_2407" s="T292">kör-AːččI</ta>
            <ta e="T294" id="Seg_2408" s="T293">kihi-ŋ</ta>
            <ta e="T295" id="Seg_2409" s="T294">ol</ta>
            <ta e="T296" id="Seg_2410" s="T295">giniler</ta>
            <ta e="T297" id="Seg_2411" s="T296">bulčut-LArA</ta>
            <ta e="T298" id="Seg_2412" s="T297">kɨːl-ČIt-LArA</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_2413" s="T0">tundra.[NOM]</ta>
            <ta e="T2" id="Seg_2414" s="T1">place-3SG-DAT/LOC</ta>
            <ta e="T3" id="Seg_2415" s="T2">deep.snow-PROPR</ta>
            <ta e="T4" id="Seg_2416" s="T3">very</ta>
            <ta e="T5" id="Seg_2417" s="T4">place-DAT/LOC</ta>
            <ta e="T6" id="Seg_2418" s="T5">long.ago-ADJZ</ta>
            <ta e="T7" id="Seg_2419" s="T6">stay-PTCP.PST</ta>
            <ta e="T8" id="Seg_2420" s="T7">tent-DIM.[NOM]</ta>
            <ta e="T9" id="Seg_2421" s="T8">there.is-3SG</ta>
            <ta e="T10" id="Seg_2422" s="T9">be-PST2.[3SG]</ta>
            <ta e="T11" id="Seg_2423" s="T10">that</ta>
            <ta e="T12" id="Seg_2424" s="T11">tent-DIM-DAT/LOC</ta>
            <ta e="T13" id="Seg_2425" s="T12">in.the.evening</ta>
            <ta e="T14" id="Seg_2426" s="T13">every</ta>
            <ta e="T15" id="Seg_2427" s="T14">light.[NOM]</ta>
            <ta e="T16" id="Seg_2428" s="T15">flame.up-PTCP.PRS</ta>
            <ta e="T17" id="Seg_2429" s="T16">become-PST2.[3SG]</ta>
            <ta e="T18" id="Seg_2430" s="T17">here</ta>
            <ta e="T19" id="Seg_2431" s="T18">there.is</ta>
            <ta e="T20" id="Seg_2432" s="T19">be-PST2.[3SG]</ta>
            <ta e="T21" id="Seg_2433" s="T20">one</ta>
            <ta e="T22" id="Seg_2434" s="T21">small</ta>
            <ta e="T23" id="Seg_2435" s="T22">human.being.[NOM]</ta>
            <ta e="T24" id="Seg_2436" s="T23">merchant-DAT/LOC</ta>
            <ta e="T25" id="Seg_2437" s="T24">worker.[NOM]</ta>
            <ta e="T26" id="Seg_2438" s="T25">be-CVB.SIM</ta>
            <ta e="T27" id="Seg_2439" s="T26">go-PRS.[3SG]</ta>
            <ta e="T28" id="Seg_2440" s="T27">people-PL.[NOM]</ta>
            <ta e="T29" id="Seg_2441" s="T28">what</ta>
            <ta e="T30" id="Seg_2442" s="T29">light-3SG.[NOM]</ta>
            <ta e="T31" id="Seg_2443" s="T30">flame.up-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T32" id="Seg_2444" s="T31">be-PST1-3SG</ta>
            <ta e="T33" id="Seg_2445" s="T32">say-CVB.SEQ</ta>
            <ta e="T34" id="Seg_2446" s="T33">wonder-CVB.SEQ</ta>
            <ta e="T35" id="Seg_2447" s="T34">chat-PRS-3PL</ta>
            <ta e="T36" id="Seg_2448" s="T35">this-DAT/LOC</ta>
            <ta e="T37" id="Seg_2449" s="T36">this</ta>
            <ta e="T38" id="Seg_2450" s="T37">brave.man.[NOM]</ta>
            <ta e="T39" id="Seg_2451" s="T38">human.being-2SG.[NOM]</ta>
            <ta e="T40" id="Seg_2452" s="T39">speak-PRS.[3SG]</ta>
            <ta e="T41" id="Seg_2453" s="T40">merchant-3SG-DAT/LOC</ta>
            <ta e="T42" id="Seg_2454" s="T41">well</ta>
            <ta e="T43" id="Seg_2455" s="T42">pledge-VBZ-EP-RECP/COLL-IMP.1DU</ta>
            <ta e="T44" id="Seg_2456" s="T43">say-PRS.[3SG]</ta>
            <ta e="T45" id="Seg_2457" s="T44">1SG.[NOM]</ta>
            <ta e="T46" id="Seg_2458" s="T45">go-CVB.SEQ</ta>
            <ta e="T47" id="Seg_2459" s="T46">know-CVB.SEQ</ta>
            <ta e="T48" id="Seg_2460" s="T47">come-FUT-1SG</ta>
            <ta e="T49" id="Seg_2461" s="T48">1SG.[NOM]</ta>
            <ta e="T50" id="Seg_2462" s="T49">lose-TEMP-1SG</ta>
            <ta e="T51" id="Seg_2463" s="T50">always</ta>
            <ta e="T52" id="Seg_2464" s="T51">free</ta>
            <ta e="T53" id="Seg_2465" s="T52">work-FUT-1SG</ta>
            <ta e="T54" id="Seg_2466" s="T53">2SG.[NOM]</ta>
            <ta e="T55" id="Seg_2467" s="T54">though</ta>
            <ta e="T56" id="Seg_2468" s="T55">money-ACC</ta>
            <ta e="T57" id="Seg_2469" s="T56">give-FUT-2SG</ta>
            <ta e="T58" id="Seg_2470" s="T57">that-INSTR</ta>
            <ta e="T59" id="Seg_2471" s="T58">agree-EP-RECP/COLL-CVB.SEQ</ta>
            <ta e="T60" id="Seg_2472" s="T59">stay-RECP/COLL-PRS-3PL</ta>
            <ta e="T61" id="Seg_2473" s="T60">this</ta>
            <ta e="T62" id="Seg_2474" s="T61">human.being-2SG.[NOM]</ta>
            <ta e="T63" id="Seg_2475" s="T62">in.the.evening</ta>
            <ta e="T64" id="Seg_2476" s="T63">dog-3SG-INSTR</ta>
            <ta e="T65" id="Seg_2477" s="T64">go-PRS.[3SG]</ta>
            <ta e="T66" id="Seg_2478" s="T65">this</ta>
            <ta e="T67" id="Seg_2479" s="T66">go-CVB.SEQ</ta>
            <ta e="T68" id="Seg_2480" s="T67">tent-DAT/LOC</ta>
            <ta e="T69" id="Seg_2481" s="T68">go.in-PRS.[3SG]</ta>
            <ta e="T70" id="Seg_2482" s="T69">this</ta>
            <ta e="T71" id="Seg_2483" s="T70">go.in-PST2-3SG</ta>
            <ta e="T72" id="Seg_2484" s="T71">animal.hair-PROPR</ta>
            <ta e="T73" id="Seg_2485" s="T72">face-PROPR</ta>
            <ta e="T74" id="Seg_2486" s="T73">human.being.[NOM]</ta>
            <ta e="T75" id="Seg_2487" s="T74">sit-PRS.[3SG]</ta>
            <ta e="T76" id="Seg_2488" s="T75">here</ta>
            <ta e="T77" id="Seg_2489" s="T76">be.afraid-CVB.SIM-be.afraid-CVB.SIM</ta>
            <ta e="T78" id="Seg_2490" s="T77">brave.man-EP-2SG.[NOM]</ta>
            <ta e="T79" id="Seg_2491" s="T78">sit.down-PRS.[3SG]</ta>
            <ta e="T80" id="Seg_2492" s="T79">bench-DAT/LOC</ta>
            <ta e="T81" id="Seg_2493" s="T80">animal.hair-PROPR</ta>
            <ta e="T82" id="Seg_2494" s="T81">human.being-3SG.[NOM]</ta>
            <ta e="T83" id="Seg_2495" s="T82">meal-VBZ-CVB.SEQ</ta>
            <ta e="T84" id="Seg_2496" s="T83">eat-CVB.PURP</ta>
            <ta e="T85" id="Seg_2497" s="T84">sit.down-PTCP.PRS</ta>
            <ta e="T86" id="Seg_2498" s="T85">be-PST2.[3SG]</ta>
            <ta e="T87" id="Seg_2499" s="T86">well</ta>
            <ta e="T88" id="Seg_2500" s="T87">meal-3SG-ACC</ta>
            <ta e="T89" id="Seg_2501" s="T88">take.out-EP-MED-CVB.SEQ</ta>
            <ta e="T90" id="Seg_2502" s="T89">eat.[IMP.2SG]</ta>
            <ta e="T91" id="Seg_2503" s="T90">say-PST2.[3SG]</ta>
            <ta e="T92" id="Seg_2504" s="T91">this-ACC</ta>
            <ta e="T93" id="Seg_2505" s="T92">eat-CVB.SEQ</ta>
            <ta e="T94" id="Seg_2506" s="T93">after</ta>
            <ta e="T95" id="Seg_2507" s="T94">sleep-CVB.PURP</ta>
            <ta e="T96" id="Seg_2508" s="T95">lie.down-PST2-3PL</ta>
            <ta e="T97" id="Seg_2509" s="T96">this</ta>
            <ta e="T98" id="Seg_2510" s="T97">lie-TEMP-3SG</ta>
            <ta e="T99" id="Seg_2511" s="T98">in.the.morning</ta>
            <ta e="T100" id="Seg_2512" s="T99">early</ta>
            <ta e="T101" id="Seg_2513" s="T100">human.being-3SG.[NOM]</ta>
            <ta e="T102" id="Seg_2514" s="T101">ski-INSTR</ta>
            <ta e="T103" id="Seg_2515" s="T102">go-CVB.SEQ</ta>
            <ta e="T104" id="Seg_2516" s="T103">stay-PRS.[3SG]</ta>
            <ta e="T105" id="Seg_2517" s="T104">small</ta>
            <ta e="T106" id="Seg_2518" s="T105">human.being-1PL.[NOM]</ta>
            <ta e="T107" id="Seg_2519" s="T106">stand.up-PRS.[3SG]</ta>
            <ta e="T108" id="Seg_2520" s="T107">and</ta>
            <ta e="T109" id="Seg_2521" s="T108">think-CVB.SIM</ta>
            <ta e="T110" id="Seg_2522" s="T109">give-PRS.[3SG]</ta>
            <ta e="T111" id="Seg_2523" s="T110">how</ta>
            <ta e="T112" id="Seg_2524" s="T111">1SG.[NOM]</ta>
            <ta e="T113" id="Seg_2525" s="T112">here</ta>
            <ta e="T114" id="Seg_2526" s="T113">go-EP-PTCP.PST-1SG-ACC</ta>
            <ta e="T115" id="Seg_2527" s="T114">know-CAUS-FUT-1SG=Q</ta>
            <ta e="T116" id="Seg_2528" s="T115">think-PRS.[3SG]</ta>
            <ta e="T117" id="Seg_2529" s="T116">this</ta>
            <ta e="T118" id="Seg_2530" s="T117">human.being.[NOM]</ta>
            <ta e="T119" id="Seg_2531" s="T118">whereto</ta>
            <ta e="T120" id="Seg_2532" s="T119">go-PTCP.PST-3SG-ACC</ta>
            <ta e="T121" id="Seg_2533" s="T120">get.to.know-CVB.SEQ</ta>
            <ta e="T122" id="Seg_2534" s="T121">mark.[NOM]</ta>
            <ta e="T123" id="Seg_2535" s="T122">bring-FUT-NEC-1SG</ta>
            <ta e="T124" id="Seg_2536" s="T123">think-CVB.SEQ</ta>
            <ta e="T125" id="Seg_2537" s="T124">path-3SG-ACC</ta>
            <ta e="T126" id="Seg_2538" s="T125">follow-CVB.SEQ</ta>
            <ta e="T127" id="Seg_2539" s="T126">go-CVB.SEQ</ta>
            <ta e="T128" id="Seg_2540" s="T127">this</ta>
            <ta e="T129" id="Seg_2541" s="T128">go-CVB.SEQ</ta>
            <ta e="T130" id="Seg_2542" s="T129">stone.[NOM]</ta>
            <ta e="T131" id="Seg_2543" s="T130">mountain-DAT/LOC</ta>
            <ta e="T132" id="Seg_2544" s="T131">reach-PRS.[3SG]</ta>
            <ta e="T133" id="Seg_2545" s="T132">here</ta>
            <ta e="T134" id="Seg_2546" s="T133">hide-CVB.SEQ</ta>
            <ta e="T135" id="Seg_2547" s="T134">come-PST2-3SG</ta>
            <ta e="T136" id="Seg_2548" s="T135">this</ta>
            <ta e="T137" id="Seg_2549" s="T136">stone.[NOM]</ta>
            <ta e="T138" id="Seg_2550" s="T137">mountain.[NOM]</ta>
            <ta e="T139" id="Seg_2551" s="T138">completely</ta>
            <ta e="T140" id="Seg_2552" s="T139">dig-EP-PASS/REFL-EP-PTCP.PST.[NOM]</ta>
            <ta e="T141" id="Seg_2553" s="T140">people.[NOM]</ta>
            <ta e="T142" id="Seg_2554" s="T141">mumbling-3SG.[NOM]</ta>
            <ta e="T143" id="Seg_2555" s="T142">say-CVB.SEQ</ta>
            <ta e="T144" id="Seg_2556" s="T143">voice-3PL.[NOM]</ta>
            <ta e="T145" id="Seg_2557" s="T144">say-CVB.SEQ</ta>
            <ta e="T146" id="Seg_2558" s="T145">look.into-CVB.SEQ</ta>
            <ta e="T147" id="Seg_2559" s="T146">see-PST2-3SG</ta>
            <ta e="T148" id="Seg_2560" s="T147">hole-EP-INSTR</ta>
            <ta e="T149" id="Seg_2561" s="T148">human.being-3PL.[NOM]</ta>
            <ta e="T150" id="Seg_2562" s="T149">completely</ta>
            <ta e="T151" id="Seg_2563" s="T150">animal.hair-PROPR.[NOM]</ta>
            <ta e="T152" id="Seg_2564" s="T151">face-PROPR.[NOM]</ta>
            <ta e="T153" id="Seg_2565" s="T152">here</ta>
            <ta e="T154" id="Seg_2566" s="T153">stalk-CVB.SEQ</ta>
            <ta e="T155" id="Seg_2567" s="T154">go.in-PST2.[3SG]</ta>
            <ta e="T156" id="Seg_2568" s="T155">this</ta>
            <ta e="T157" id="Seg_2569" s="T156">go.in-CVB.SEQ</ta>
            <ta e="T158" id="Seg_2570" s="T157">this</ta>
            <ta e="T159" id="Seg_2571" s="T158">house-DAT/LOC</ta>
            <ta e="T160" id="Seg_2572" s="T159">yurt-DAT/LOC</ta>
            <ta e="T161" id="Seg_2573" s="T160">go.in-CVB.SEQ</ta>
            <ta e="T162" id="Seg_2574" s="T161">sit.down-PRS.[3SG]</ta>
            <ta e="T163" id="Seg_2575" s="T162">this</ta>
            <ta e="T164" id="Seg_2576" s="T163">sit.down-PST2-3SG</ta>
            <ta e="T165" id="Seg_2577" s="T164">one</ta>
            <ta e="T166" id="Seg_2578" s="T165">woman.[NOM]</ta>
            <ta e="T167" id="Seg_2579" s="T166">there.is</ta>
            <ta e="T168" id="Seg_2580" s="T167">be-PST2.[3SG]</ta>
            <ta e="T169" id="Seg_2581" s="T168">that.[NOM]</ta>
            <ta e="T170" id="Seg_2582" s="T169">silently</ta>
            <ta e="T171" id="Seg_2583" s="T170">ask-PRS.[3SG]</ta>
            <ta e="T172" id="Seg_2584" s="T171">well</ta>
            <ta e="T173" id="Seg_2585" s="T172">what-EP-INSTR</ta>
            <ta e="T174" id="Seg_2586" s="T173">need-CVB.SEQ</ta>
            <ta e="T175" id="Seg_2587" s="T174">come-PST1-2SG</ta>
            <ta e="T176" id="Seg_2588" s="T175">say-PRS.[3SG]</ta>
            <ta e="T177" id="Seg_2589" s="T176">this-ACC</ta>
            <ta e="T178" id="Seg_2590" s="T177">speak-PRS.[3SG]</ta>
            <ta e="T179" id="Seg_2591" s="T178">not.big</ta>
            <ta e="T180" id="Seg_2592" s="T179">matter-1SG-INSTR</ta>
            <ta e="T181" id="Seg_2593" s="T180">say-PRS.[3SG]</ta>
            <ta e="T182" id="Seg_2594" s="T181">that-DAT/LOC</ta>
            <ta e="T183" id="Seg_2595" s="T182">this</ta>
            <ta e="T184" id="Seg_2596" s="T183">woman-3SG.[NOM]</ta>
            <ta e="T185" id="Seg_2597" s="T184">what-ACC</ta>
            <ta e="T186" id="Seg_2598" s="T185">INDEF</ta>
            <ta e="T187" id="Seg_2599" s="T186">soft</ta>
            <ta e="T188" id="Seg_2600" s="T187">very-ACC</ta>
            <ta e="T189" id="Seg_2601" s="T188">hand-CVB.SEQ</ta>
            <ta e="T190" id="Seg_2602" s="T189">throw-PRS.[3SG]</ta>
            <ta e="T191" id="Seg_2603" s="T190">that-ACC</ta>
            <ta e="T192" id="Seg_2604" s="T191">see-EP-NEG.CVB</ta>
            <ta e="T193" id="Seg_2605" s="T192">just</ta>
            <ta e="T194" id="Seg_2606" s="T193">pocket-3SG-DAT/LOC</ta>
            <ta e="T195" id="Seg_2607" s="T194">stick-MED-CVB.SEQ</ta>
            <ta e="T196" id="Seg_2608" s="T195">throw-PRS.[3SG]</ta>
            <ta e="T197" id="Seg_2609" s="T196">woman-3SG.[NOM]</ta>
            <ta e="T198" id="Seg_2610" s="T197">speak-PRS.[3SG]</ta>
            <ta e="T199" id="Seg_2611" s="T198">well</ta>
            <ta e="T200" id="Seg_2612" s="T199">attempt.[IMP.2SG]</ta>
            <ta e="T201" id="Seg_2613" s="T200">go-CVB.SEQ</ta>
            <ta e="T202" id="Seg_2614" s="T201">see.[IMP.2SG]</ta>
            <ta e="T203" id="Seg_2615" s="T202">here</ta>
            <ta e="T204" id="Seg_2616" s="T203">people-PL-EP-1SG.[NOM]</ta>
            <ta e="T205" id="Seg_2617" s="T204">see-TEMP-3PL</ta>
            <ta e="T206" id="Seg_2618" s="T205">human.being-VBZ-FUT-3PL</ta>
            <ta e="T207" id="Seg_2619" s="T206">NEG-3SG</ta>
            <ta e="T208" id="Seg_2620" s="T207">say-PST2.[3SG]</ta>
            <ta e="T209" id="Seg_2621" s="T208">that-ACC</ta>
            <ta e="T210" id="Seg_2622" s="T209">hear-CVB.ANT</ta>
            <ta e="T211" id="Seg_2623" s="T210">small</ta>
            <ta e="T212" id="Seg_2624" s="T211">human.being.[NOM]</ta>
            <ta e="T213" id="Seg_2625" s="T212">go.out-EP-PST2.[3SG]</ta>
            <ta e="T214" id="Seg_2626" s="T213">dog-3SG-DAT/LOC</ta>
            <ta e="T215" id="Seg_2627" s="T214">sit.down-PST2.[3SG]</ta>
            <ta e="T216" id="Seg_2628" s="T215">and</ta>
            <ta e="T217" id="Seg_2629" s="T216">go-PTCP.PRS-DAT/LOC</ta>
            <ta e="T218" id="Seg_2630" s="T217">become-PST2.[3SG]</ta>
            <ta e="T219" id="Seg_2631" s="T218">this</ta>
            <ta e="T220" id="Seg_2632" s="T219">go-CVB.SEQ</ta>
            <ta e="T221" id="Seg_2633" s="T220">drive-CVB.SEQ</ta>
            <ta e="T222" id="Seg_2634" s="T221">back-3SG-ACC</ta>
            <ta e="T223" id="Seg_2635" s="T222">see-PST2-3SG</ta>
            <ta e="T224" id="Seg_2636" s="T223">one</ta>
            <ta e="T225" id="Seg_2637" s="T224">human.being.[NOM]</ta>
            <ta e="T226" id="Seg_2638" s="T225">follow-CVB.SEQ</ta>
            <ta e="T227" id="Seg_2639" s="T226">go-PRS.[3SG]</ta>
            <ta e="T228" id="Seg_2640" s="T227">that-3SG-3SG.[NOM]</ta>
            <ta e="T229" id="Seg_2641" s="T228">chase-CVB.SEQ</ta>
            <ta e="T230" id="Seg_2642" s="T229">go-PTCP.PRS-3SG-ACC</ta>
            <ta e="T231" id="Seg_2643" s="T230">see-CVB.SEQ</ta>
            <ta e="T232" id="Seg_2644" s="T231">this</ta>
            <ta e="T233" id="Seg_2645" s="T232">human.being.[NOM]</ta>
            <ta e="T234" id="Seg_2646" s="T233">run-EP-CAUS-PST2.[3SG]</ta>
            <ta e="T235" id="Seg_2647" s="T234">animal.hair-PROPR</ta>
            <ta e="T236" id="Seg_2648" s="T235">face-PROPR</ta>
            <ta e="T237" id="Seg_2649" s="T236">human.being-3SG.[NOM]</ta>
            <ta e="T238" id="Seg_2650" s="T237">sledge-3SG-GEN</ta>
            <ta e="T239" id="Seg_2651" s="T238">back</ta>
            <ta e="T240" id="Seg_2652" s="T239">leg-3SG-ABL</ta>
            <ta e="T241" id="Seg_2653" s="T240">take-CVB.SEQ</ta>
            <ta e="T242" id="Seg_2654" s="T241">two</ta>
            <ta e="T243" id="Seg_2655" s="T242">leg-3SG-ACC</ta>
            <ta e="T244" id="Seg_2656" s="T243">stay-CAUS-PST2.[3SG]</ta>
            <ta e="T245" id="Seg_2657" s="T244">get.into-REFL-PTCP.PRS-2SG-ACC</ta>
            <ta e="T246" id="Seg_2658" s="T245">see-CAUS-PTCP.FUT</ta>
            <ta e="T247" id="Seg_2659" s="T246">be-PST1-1SG</ta>
            <ta e="T248" id="Seg_2660" s="T247">say-CVB.SEQ</ta>
            <ta e="T249" id="Seg_2661" s="T248">shout-CVB.SEQ</ta>
            <ta e="T250" id="Seg_2662" s="T249">stay-PST2.[3SG]</ta>
            <ta e="T251" id="Seg_2663" s="T250">this</ta>
            <ta e="T252" id="Seg_2664" s="T251">boy.[NOM]</ta>
            <ta e="T253" id="Seg_2665" s="T252">house-3SG-DAT/LOC</ta>
            <ta e="T254" id="Seg_2666" s="T253">come-CVB.SEQ</ta>
            <ta e="T255" id="Seg_2667" s="T254">mark.[NOM]</ta>
            <ta e="T256" id="Seg_2668" s="T255">bring-PST1-1SG</ta>
            <ta e="T257" id="Seg_2669" s="T256">say-CVB.SEQ</ta>
            <ta e="T258" id="Seg_2670" s="T257">pocket-3SG-ABL</ta>
            <ta e="T259" id="Seg_2671" s="T258">soft-3SG-ACC</ta>
            <ta e="T260" id="Seg_2672" s="T259">take.out-PST2-3SG</ta>
            <ta e="T261" id="Seg_2673" s="T260">black</ta>
            <ta e="T262" id="Seg_2674" s="T261">fox.[NOM]</ta>
            <ta e="T263" id="Seg_2675" s="T262">be-PST2.[3SG]</ta>
            <ta e="T264" id="Seg_2676" s="T263">merchant-3SG.[NOM]</ta>
            <ta e="T265" id="Seg_2677" s="T264">though</ta>
            <ta e="T266" id="Seg_2678" s="T265">money.[NOM]</ta>
            <ta e="T267" id="Seg_2679" s="T266">a.lot</ta>
            <ta e="T268" id="Seg_2680" s="T267">give-PST2.[3SG]</ta>
            <ta e="T269" id="Seg_2681" s="T268">so</ta>
            <ta e="T270" id="Seg_2682" s="T269">rich</ta>
            <ta e="T271" id="Seg_2683" s="T270">human.being.[NOM]</ta>
            <ta e="T272" id="Seg_2684" s="T271">become-PST2.[3SG]</ta>
            <ta e="T273" id="Seg_2685" s="T272">this-ACC</ta>
            <ta e="T274" id="Seg_2686" s="T273">old-PL.[NOM]</ta>
            <ta e="T275" id="Seg_2687" s="T274">speak-PST2-3PL</ta>
            <ta e="T276" id="Seg_2688" s="T275">2SG.[NOM]</ta>
            <ta e="T277" id="Seg_2689" s="T276">reach-CVB.SIM</ta>
            <ta e="T278" id="Seg_2690" s="T277">go-EP-PST2-2SG</ta>
            <ta e="T279" id="Seg_2691" s="T278">long.ago</ta>
            <ta e="T280" id="Seg_2692" s="T279">czar.[NOM]</ta>
            <ta e="T281" id="Seg_2693" s="T280">tax-3SG-ABL</ta>
            <ta e="T282" id="Seg_2694" s="T281">run.away-CVB.SEQ</ta>
            <ta e="T283" id="Seg_2695" s="T282">come-PTCP.PST</ta>
            <ta e="T284" id="Seg_2696" s="T283">people-PL-DAT/LOC</ta>
            <ta e="T285" id="Seg_2697" s="T284">that-PL-EP-2SG.[NOM]</ta>
            <ta e="T286" id="Seg_2698" s="T285">hide-CVB.SEQ</ta>
            <ta e="T287" id="Seg_2699" s="T286">mountain.[NOM]</ta>
            <ta e="T288" id="Seg_2700" s="T287">inside-3SG-DAT/LOC</ta>
            <ta e="T289" id="Seg_2701" s="T288">house-VBZ-PST1-3PL</ta>
            <ta e="T290" id="Seg_2702" s="T289">at.first</ta>
            <ta e="T291" id="Seg_2703" s="T290">2SG.[NOM]</ta>
            <ta e="T292" id="Seg_2704" s="T291">tent-DAT/LOC</ta>
            <ta e="T293" id="Seg_2705" s="T292">see-PTCP.HAB</ta>
            <ta e="T294" id="Seg_2706" s="T293">human.being-2SG.[NOM]</ta>
            <ta e="T295" id="Seg_2707" s="T294">that.[NOM]</ta>
            <ta e="T296" id="Seg_2708" s="T295">3PL.[NOM]</ta>
            <ta e="T297" id="Seg_2709" s="T296">hunter-3PL.[NOM]</ta>
            <ta e="T298" id="Seg_2710" s="T297">animal-AG-3PL.[NOM]</ta>
         </annotation>
         <annotation name="gg" tierref="gg">
            <ta e="T1" id="Seg_2711" s="T0">Tundra.[NOM]</ta>
            <ta e="T2" id="Seg_2712" s="T1">Ort-3SG-DAT/LOC</ta>
            <ta e="T3" id="Seg_2713" s="T2">tiefer.Schnee-PROPR</ta>
            <ta e="T4" id="Seg_2714" s="T3">sehr</ta>
            <ta e="T5" id="Seg_2715" s="T4">Ort-DAT/LOC</ta>
            <ta e="T6" id="Seg_2716" s="T5">vor.langer.Zeit-ADJZ</ta>
            <ta e="T7" id="Seg_2717" s="T6">bleiben-PTCP.PST</ta>
            <ta e="T8" id="Seg_2718" s="T7">Zelt-DIM.[NOM]</ta>
            <ta e="T9" id="Seg_2719" s="T8">es.gibt-3SG</ta>
            <ta e="T10" id="Seg_2720" s="T9">sein-PST2.[3SG]</ta>
            <ta e="T11" id="Seg_2721" s="T10">jenes</ta>
            <ta e="T12" id="Seg_2722" s="T11">Zelt-DIM-DAT/LOC</ta>
            <ta e="T13" id="Seg_2723" s="T12">am.Abend</ta>
            <ta e="T14" id="Seg_2724" s="T13">jeder</ta>
            <ta e="T15" id="Seg_2725" s="T14">Licht.[NOM]</ta>
            <ta e="T16" id="Seg_2726" s="T15">aufflammen-PTCP.PRS</ta>
            <ta e="T17" id="Seg_2727" s="T16">werden-PST2.[3SG]</ta>
            <ta e="T18" id="Seg_2728" s="T17">hier</ta>
            <ta e="T19" id="Seg_2729" s="T18">es.gibt</ta>
            <ta e="T20" id="Seg_2730" s="T19">sein-PST2.[3SG]</ta>
            <ta e="T21" id="Seg_2731" s="T20">eins</ta>
            <ta e="T22" id="Seg_2732" s="T21">klein</ta>
            <ta e="T23" id="Seg_2733" s="T22">Mensch.[NOM]</ta>
            <ta e="T24" id="Seg_2734" s="T23">Kaufmann-DAT/LOC</ta>
            <ta e="T25" id="Seg_2735" s="T24">Arbeiter.[NOM]</ta>
            <ta e="T26" id="Seg_2736" s="T25">sein-CVB.SIM</ta>
            <ta e="T27" id="Seg_2737" s="T26">gehen-PRS.[3SG]</ta>
            <ta e="T28" id="Seg_2738" s="T27">Leute-PL.[NOM]</ta>
            <ta e="T29" id="Seg_2739" s="T28">was</ta>
            <ta e="T30" id="Seg_2740" s="T29">Licht-3SG.[NOM]</ta>
            <ta e="T31" id="Seg_2741" s="T30">aufflammen-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T32" id="Seg_2742" s="T31">sein-PST1-3SG</ta>
            <ta e="T33" id="Seg_2743" s="T32">sagen-CVB.SEQ</ta>
            <ta e="T34" id="Seg_2744" s="T33">sich.wundern-CVB.SEQ</ta>
            <ta e="T35" id="Seg_2745" s="T34">sich.unterhalten-PRS-3PL</ta>
            <ta e="T36" id="Seg_2746" s="T35">dieses-DAT/LOC</ta>
            <ta e="T37" id="Seg_2747" s="T36">dieses</ta>
            <ta e="T38" id="Seg_2748" s="T37">Prachtkerl.[NOM]</ta>
            <ta e="T39" id="Seg_2749" s="T38">Mensch-2SG.[NOM]</ta>
            <ta e="T40" id="Seg_2750" s="T39">sprechen-PRS.[3SG]</ta>
            <ta e="T41" id="Seg_2751" s="T40">Kaufmann-3SG-DAT/LOC</ta>
            <ta e="T42" id="Seg_2752" s="T41">doch</ta>
            <ta e="T43" id="Seg_2753" s="T42">Pfand-VBZ-EP-RECP/COLL-IMP.1DU</ta>
            <ta e="T44" id="Seg_2754" s="T43">sagen-PRS.[3SG]</ta>
            <ta e="T45" id="Seg_2755" s="T44">1SG.[NOM]</ta>
            <ta e="T46" id="Seg_2756" s="T45">gehen-CVB.SEQ</ta>
            <ta e="T47" id="Seg_2757" s="T46">wissen-CVB.SEQ</ta>
            <ta e="T48" id="Seg_2758" s="T47">kommen-FUT-1SG</ta>
            <ta e="T49" id="Seg_2759" s="T48">1SG.[NOM]</ta>
            <ta e="T50" id="Seg_2760" s="T49">verlieren-TEMP-1SG</ta>
            <ta e="T51" id="Seg_2761" s="T50">immer</ta>
            <ta e="T52" id="Seg_2762" s="T51">umsonst</ta>
            <ta e="T53" id="Seg_2763" s="T52">arbeiten-FUT-1SG</ta>
            <ta e="T54" id="Seg_2764" s="T53">2SG.[NOM]</ta>
            <ta e="T55" id="Seg_2765" s="T54">aber</ta>
            <ta e="T56" id="Seg_2766" s="T55">Geld-ACC</ta>
            <ta e="T57" id="Seg_2767" s="T56">geben-FUT-2SG</ta>
            <ta e="T58" id="Seg_2768" s="T57">jenes-INSTR</ta>
            <ta e="T59" id="Seg_2769" s="T58">einverstanden.sein-EP-RECP/COLL-CVB.SEQ</ta>
            <ta e="T60" id="Seg_2770" s="T59">bleiben-RECP/COLL-PRS-3PL</ta>
            <ta e="T61" id="Seg_2771" s="T60">dieses</ta>
            <ta e="T62" id="Seg_2772" s="T61">Mensch-2SG.[NOM]</ta>
            <ta e="T63" id="Seg_2773" s="T62">am.Abend</ta>
            <ta e="T64" id="Seg_2774" s="T63">Hund-3SG-INSTR</ta>
            <ta e="T65" id="Seg_2775" s="T64">gehen-PRS.[3SG]</ta>
            <ta e="T66" id="Seg_2776" s="T65">dieses</ta>
            <ta e="T67" id="Seg_2777" s="T66">gehen-CVB.SEQ</ta>
            <ta e="T68" id="Seg_2778" s="T67">Zelt-DAT/LOC</ta>
            <ta e="T69" id="Seg_2779" s="T68">hineingehen-PRS.[3SG]</ta>
            <ta e="T70" id="Seg_2780" s="T69">dieses</ta>
            <ta e="T71" id="Seg_2781" s="T70">hineingehen-PST2-3SG</ta>
            <ta e="T72" id="Seg_2782" s="T71">Tierhaar-PROPR</ta>
            <ta e="T73" id="Seg_2783" s="T72">Gesicht-PROPR</ta>
            <ta e="T74" id="Seg_2784" s="T73">Mensch.[NOM]</ta>
            <ta e="T75" id="Seg_2785" s="T74">sitzen-PRS.[3SG]</ta>
            <ta e="T76" id="Seg_2786" s="T75">hier</ta>
            <ta e="T77" id="Seg_2787" s="T76">Angst.haben-CVB.SIM-Angst.haben-CVB.SIM</ta>
            <ta e="T78" id="Seg_2788" s="T77">Prachtkerl-EP-2SG.[NOM]</ta>
            <ta e="T79" id="Seg_2789" s="T78">sich.setzen-PRS.[3SG]</ta>
            <ta e="T80" id="Seg_2790" s="T79">Bank-DAT/LOC</ta>
            <ta e="T81" id="Seg_2791" s="T80">Tierhaar-PROPR</ta>
            <ta e="T82" id="Seg_2792" s="T81">Mensch-3SG.[NOM]</ta>
            <ta e="T83" id="Seg_2793" s="T82">Speise-VBZ-CVB.SEQ</ta>
            <ta e="T84" id="Seg_2794" s="T83">essen-CVB.PURP</ta>
            <ta e="T85" id="Seg_2795" s="T84">sich.setzen-PTCP.PRS</ta>
            <ta e="T86" id="Seg_2796" s="T85">sein-PST2.[3SG]</ta>
            <ta e="T87" id="Seg_2797" s="T86">doch</ta>
            <ta e="T88" id="Seg_2798" s="T87">Speise-3SG-ACC</ta>
            <ta e="T89" id="Seg_2799" s="T88">herausnehmen-EP-MED-CVB.SEQ</ta>
            <ta e="T90" id="Seg_2800" s="T89">essen.[IMP.2SG]</ta>
            <ta e="T91" id="Seg_2801" s="T90">sagen-PST2.[3SG]</ta>
            <ta e="T92" id="Seg_2802" s="T91">dieses-ACC</ta>
            <ta e="T93" id="Seg_2803" s="T92">essen-CVB.SEQ</ta>
            <ta e="T94" id="Seg_2804" s="T93">nachdem</ta>
            <ta e="T95" id="Seg_2805" s="T94">schlafen-CVB.PURP</ta>
            <ta e="T96" id="Seg_2806" s="T95">sich.hinlegen-PST2-3PL</ta>
            <ta e="T97" id="Seg_2807" s="T96">dieses</ta>
            <ta e="T98" id="Seg_2808" s="T97">liegen-TEMP-3SG</ta>
            <ta e="T99" id="Seg_2809" s="T98">am.Morgen</ta>
            <ta e="T100" id="Seg_2810" s="T99">früh</ta>
            <ta e="T101" id="Seg_2811" s="T100">Mensch-3SG.[NOM]</ta>
            <ta e="T102" id="Seg_2812" s="T101">Ski-INSTR</ta>
            <ta e="T103" id="Seg_2813" s="T102">gehen-CVB.SEQ</ta>
            <ta e="T104" id="Seg_2814" s="T103">bleiben-PRS.[3SG]</ta>
            <ta e="T105" id="Seg_2815" s="T104">klein</ta>
            <ta e="T106" id="Seg_2816" s="T105">Mensch-1PL.[NOM]</ta>
            <ta e="T107" id="Seg_2817" s="T106">aufstehen-PRS.[3SG]</ta>
            <ta e="T108" id="Seg_2818" s="T107">und</ta>
            <ta e="T109" id="Seg_2819" s="T108">denken-CVB.SIM</ta>
            <ta e="T110" id="Seg_2820" s="T109">geben-PRS.[3SG]</ta>
            <ta e="T111" id="Seg_2821" s="T110">wie</ta>
            <ta e="T112" id="Seg_2822" s="T111">1SG.[NOM]</ta>
            <ta e="T113" id="Seg_2823" s="T112">hier</ta>
            <ta e="T114" id="Seg_2824" s="T113">gehen-EP-PTCP.PST-1SG-ACC</ta>
            <ta e="T115" id="Seg_2825" s="T114">wissen-CAUS-FUT-1SG=Q</ta>
            <ta e="T116" id="Seg_2826" s="T115">denken-PRS.[3SG]</ta>
            <ta e="T117" id="Seg_2827" s="T116">dieses</ta>
            <ta e="T118" id="Seg_2828" s="T117">Mensch.[NOM]</ta>
            <ta e="T119" id="Seg_2829" s="T118">wohin</ta>
            <ta e="T120" id="Seg_2830" s="T119">gehen-PTCP.PST-3SG-ACC</ta>
            <ta e="T121" id="Seg_2831" s="T120">erfahren-CVB.SEQ</ta>
            <ta e="T122" id="Seg_2832" s="T121">Kennzeichen.[NOM]</ta>
            <ta e="T123" id="Seg_2833" s="T122">bringen-FUT-NEC-1SG</ta>
            <ta e="T124" id="Seg_2834" s="T123">denken-CVB.SEQ</ta>
            <ta e="T125" id="Seg_2835" s="T124">Pfad-3SG-ACC</ta>
            <ta e="T126" id="Seg_2836" s="T125">folgen-CVB.SEQ</ta>
            <ta e="T127" id="Seg_2837" s="T126">gehen-CVB.SEQ</ta>
            <ta e="T128" id="Seg_2838" s="T127">dieses</ta>
            <ta e="T129" id="Seg_2839" s="T128">gehen-CVB.SEQ</ta>
            <ta e="T130" id="Seg_2840" s="T129">Stein.[NOM]</ta>
            <ta e="T131" id="Seg_2841" s="T130">Berg-DAT/LOC</ta>
            <ta e="T132" id="Seg_2842" s="T131">ankommen-PRS.[3SG]</ta>
            <ta e="T133" id="Seg_2843" s="T132">hier</ta>
            <ta e="T134" id="Seg_2844" s="T133">sich.verstecken-CVB.SEQ</ta>
            <ta e="T135" id="Seg_2845" s="T134">kommen-PST2-3SG</ta>
            <ta e="T136" id="Seg_2846" s="T135">dieses</ta>
            <ta e="T137" id="Seg_2847" s="T136">Stein.[NOM]</ta>
            <ta e="T138" id="Seg_2848" s="T137">Berg.[NOM]</ta>
            <ta e="T139" id="Seg_2849" s="T138">ganz</ta>
            <ta e="T140" id="Seg_2850" s="T139">graben-EP-PASS/REFL-EP-PTCP.PST.[NOM]</ta>
            <ta e="T141" id="Seg_2851" s="T140">Leute.[NOM]</ta>
            <ta e="T142" id="Seg_2852" s="T141">Gemurmel-3SG.[NOM]</ta>
            <ta e="T143" id="Seg_2853" s="T142">sagen-CVB.SEQ</ta>
            <ta e="T144" id="Seg_2854" s="T143">Stimme-3PL.[NOM]</ta>
            <ta e="T145" id="Seg_2855" s="T144">sagen-CVB.SEQ</ta>
            <ta e="T146" id="Seg_2856" s="T145">hineinschauen-CVB.SEQ</ta>
            <ta e="T147" id="Seg_2857" s="T146">sehen-PST2-3SG</ta>
            <ta e="T148" id="Seg_2858" s="T147">Loch-EP-INSTR</ta>
            <ta e="T149" id="Seg_2859" s="T148">Mensch-3PL.[NOM]</ta>
            <ta e="T150" id="Seg_2860" s="T149">ganz</ta>
            <ta e="T151" id="Seg_2861" s="T150">Tierhaar-PROPR.[NOM]</ta>
            <ta e="T152" id="Seg_2862" s="T151">Gesicht-PROPR.[NOM]</ta>
            <ta e="T153" id="Seg_2863" s="T152">hier</ta>
            <ta e="T154" id="Seg_2864" s="T153">sich.anschleichen-CVB.SEQ</ta>
            <ta e="T155" id="Seg_2865" s="T154">hineingehen-PST2.[3SG]</ta>
            <ta e="T156" id="Seg_2866" s="T155">dieses</ta>
            <ta e="T157" id="Seg_2867" s="T156">hineingehen-CVB.SEQ</ta>
            <ta e="T158" id="Seg_2868" s="T157">dieses</ta>
            <ta e="T159" id="Seg_2869" s="T158">Haus-DAT/LOC</ta>
            <ta e="T160" id="Seg_2870" s="T159">Jurte-DAT/LOC</ta>
            <ta e="T161" id="Seg_2871" s="T160">hineingehen-CVB.SEQ</ta>
            <ta e="T162" id="Seg_2872" s="T161">sich.setzen-PRS.[3SG]</ta>
            <ta e="T163" id="Seg_2873" s="T162">dieses</ta>
            <ta e="T164" id="Seg_2874" s="T163">sich.setzen-PST2-3SG</ta>
            <ta e="T165" id="Seg_2875" s="T164">eins</ta>
            <ta e="T166" id="Seg_2876" s="T165">Frau.[NOM]</ta>
            <ta e="T167" id="Seg_2877" s="T166">es.gibt</ta>
            <ta e="T168" id="Seg_2878" s="T167">sein-PST2.[3SG]</ta>
            <ta e="T169" id="Seg_2879" s="T168">jenes.[NOM]</ta>
            <ta e="T170" id="Seg_2880" s="T169">ganz.leise</ta>
            <ta e="T171" id="Seg_2881" s="T170">fragen-PRS.[3SG]</ta>
            <ta e="T172" id="Seg_2882" s="T171">na</ta>
            <ta e="T173" id="Seg_2883" s="T172">was-EP-INSTR</ta>
            <ta e="T174" id="Seg_2884" s="T173">brauchen-CVB.SEQ</ta>
            <ta e="T175" id="Seg_2885" s="T174">kommen-PST1-2SG</ta>
            <ta e="T176" id="Seg_2886" s="T175">sagen-PRS.[3SG]</ta>
            <ta e="T177" id="Seg_2887" s="T176">dieses-ACC</ta>
            <ta e="T178" id="Seg_2888" s="T177">sprechen-PRS.[3SG]</ta>
            <ta e="T179" id="Seg_2889" s="T178">nicht.groß</ta>
            <ta e="T180" id="Seg_2890" s="T179">Sache-1SG-INSTR</ta>
            <ta e="T181" id="Seg_2891" s="T180">sagen-PRS.[3SG]</ta>
            <ta e="T182" id="Seg_2892" s="T181">jenes-DAT/LOC</ta>
            <ta e="T183" id="Seg_2893" s="T182">dieses</ta>
            <ta e="T184" id="Seg_2894" s="T183">Frau-3SG.[NOM]</ta>
            <ta e="T185" id="Seg_2895" s="T184">was-ACC</ta>
            <ta e="T186" id="Seg_2896" s="T185">INDEF</ta>
            <ta e="T187" id="Seg_2897" s="T186">weich</ta>
            <ta e="T188" id="Seg_2898" s="T187">sehr-ACC</ta>
            <ta e="T189" id="Seg_2899" s="T188">überreichen-CVB.SEQ</ta>
            <ta e="T190" id="Seg_2900" s="T189">werfen-PRS.[3SG]</ta>
            <ta e="T191" id="Seg_2901" s="T190">jenes-ACC</ta>
            <ta e="T192" id="Seg_2902" s="T191">sehen-EP-NEG.CVB</ta>
            <ta e="T193" id="Seg_2903" s="T192">nur</ta>
            <ta e="T194" id="Seg_2904" s="T193">Tasche-3SG-DAT/LOC</ta>
            <ta e="T195" id="Seg_2905" s="T194">stecken-MED-CVB.SEQ</ta>
            <ta e="T196" id="Seg_2906" s="T195">werfen-PRS.[3SG]</ta>
            <ta e="T197" id="Seg_2907" s="T196">Frau-3SG.[NOM]</ta>
            <ta e="T198" id="Seg_2908" s="T197">sprechen-PRS.[3SG]</ta>
            <ta e="T199" id="Seg_2909" s="T198">doch</ta>
            <ta e="T200" id="Seg_2910" s="T199">sich.bemühen.[IMP.2SG]</ta>
            <ta e="T201" id="Seg_2911" s="T200">gehen-CVB.SEQ</ta>
            <ta e="T202" id="Seg_2912" s="T201">sehen.[IMP.2SG]</ta>
            <ta e="T203" id="Seg_2913" s="T202">hier</ta>
            <ta e="T204" id="Seg_2914" s="T203">Volk-PL-EP-1SG.[NOM]</ta>
            <ta e="T205" id="Seg_2915" s="T204">sehen-TEMP-3PL</ta>
            <ta e="T206" id="Seg_2916" s="T205">Mensch-VBZ-FUT-3PL</ta>
            <ta e="T207" id="Seg_2917" s="T206">NEG-3SG</ta>
            <ta e="T208" id="Seg_2918" s="T207">sagen-PST2.[3SG]</ta>
            <ta e="T209" id="Seg_2919" s="T208">jenes-ACC</ta>
            <ta e="T210" id="Seg_2920" s="T209">hören-CVB.ANT</ta>
            <ta e="T211" id="Seg_2921" s="T210">klein</ta>
            <ta e="T212" id="Seg_2922" s="T211">Mensch.[NOM]</ta>
            <ta e="T213" id="Seg_2923" s="T212">hinausgehen-EP-PST2.[3SG]</ta>
            <ta e="T214" id="Seg_2924" s="T213">Hund-3SG-DAT/LOC</ta>
            <ta e="T215" id="Seg_2925" s="T214">sich.setzen-PST2.[3SG]</ta>
            <ta e="T216" id="Seg_2926" s="T215">und</ta>
            <ta e="T217" id="Seg_2927" s="T216">gehen-PTCP.PRS-DAT/LOC</ta>
            <ta e="T218" id="Seg_2928" s="T217">werden-PST2.[3SG]</ta>
            <ta e="T219" id="Seg_2929" s="T218">dieses</ta>
            <ta e="T220" id="Seg_2930" s="T219">gehen-CVB.SEQ</ta>
            <ta e="T221" id="Seg_2931" s="T220">fahren-CVB.SEQ</ta>
            <ta e="T222" id="Seg_2932" s="T221">Hinterteil-3SG-ACC</ta>
            <ta e="T223" id="Seg_2933" s="T222">sehen-PST2-3SG</ta>
            <ta e="T224" id="Seg_2934" s="T223">eins</ta>
            <ta e="T225" id="Seg_2935" s="T224">Mensch.[NOM]</ta>
            <ta e="T226" id="Seg_2936" s="T225">folgen-CVB.SEQ</ta>
            <ta e="T227" id="Seg_2937" s="T226">gehen-PRS.[3SG]</ta>
            <ta e="T228" id="Seg_2938" s="T227">jenes-3SG-3SG.[NOM]</ta>
            <ta e="T229" id="Seg_2939" s="T228">verfolgen-CVB.SEQ</ta>
            <ta e="T230" id="Seg_2940" s="T229">gehen-PTCP.PRS-3SG-ACC</ta>
            <ta e="T231" id="Seg_2941" s="T230">sehen-CVB.SEQ</ta>
            <ta e="T232" id="Seg_2942" s="T231">dieses</ta>
            <ta e="T233" id="Seg_2943" s="T232">Mensch.[NOM]</ta>
            <ta e="T234" id="Seg_2944" s="T233">rennen-EP-CAUS-PST2.[3SG]</ta>
            <ta e="T235" id="Seg_2945" s="T234">Tierhaar-PROPR</ta>
            <ta e="T236" id="Seg_2946" s="T235">Gesicht-PROPR</ta>
            <ta e="T237" id="Seg_2947" s="T236">Mensch-3SG.[NOM]</ta>
            <ta e="T238" id="Seg_2948" s="T237">Schlitten-3SG-GEN</ta>
            <ta e="T239" id="Seg_2949" s="T238">hinterer</ta>
            <ta e="T240" id="Seg_2950" s="T239">Bein-3SG-ABL</ta>
            <ta e="T241" id="Seg_2951" s="T240">nehmen-CVB.SEQ</ta>
            <ta e="T242" id="Seg_2952" s="T241">zwei</ta>
            <ta e="T243" id="Seg_2953" s="T242">Bein-3SG-ACC</ta>
            <ta e="T244" id="Seg_2954" s="T243">bleiben-CAUS-PST2.[3SG]</ta>
            <ta e="T245" id="Seg_2955" s="T244">geraten-REFL-PTCP.PRS-2SG-ACC</ta>
            <ta e="T246" id="Seg_2956" s="T245">sehen-CAUS-PTCP.FUT</ta>
            <ta e="T247" id="Seg_2957" s="T246">sein-PST1-1SG</ta>
            <ta e="T248" id="Seg_2958" s="T247">sagen-CVB.SEQ</ta>
            <ta e="T249" id="Seg_2959" s="T248">schreien-CVB.SEQ</ta>
            <ta e="T250" id="Seg_2960" s="T249">bleiben-PST2.[3SG]</ta>
            <ta e="T251" id="Seg_2961" s="T250">dieses</ta>
            <ta e="T252" id="Seg_2962" s="T251">Junge.[NOM]</ta>
            <ta e="T253" id="Seg_2963" s="T252">Haus-3SG-DAT/LOC</ta>
            <ta e="T254" id="Seg_2964" s="T253">kommen-CVB.SEQ</ta>
            <ta e="T255" id="Seg_2965" s="T254">Kennzeichen.[NOM]</ta>
            <ta e="T256" id="Seg_2966" s="T255">bringen-PST1-1SG</ta>
            <ta e="T257" id="Seg_2967" s="T256">sagen-CVB.SEQ</ta>
            <ta e="T258" id="Seg_2968" s="T257">Tasche-3SG-ABL</ta>
            <ta e="T259" id="Seg_2969" s="T258">weich-3SG-ACC</ta>
            <ta e="T260" id="Seg_2970" s="T259">herausnehmen-PST2-3SG</ta>
            <ta e="T261" id="Seg_2971" s="T260">schwarz</ta>
            <ta e="T262" id="Seg_2972" s="T261">Fuchs.[NOM]</ta>
            <ta e="T263" id="Seg_2973" s="T262">sein-PST2.[3SG]</ta>
            <ta e="T264" id="Seg_2974" s="T263">Kaufmann-3SG.[NOM]</ta>
            <ta e="T265" id="Seg_2975" s="T264">aber</ta>
            <ta e="T266" id="Seg_2976" s="T265">Geld.[NOM]</ta>
            <ta e="T267" id="Seg_2977" s="T266">sehr.viel</ta>
            <ta e="T268" id="Seg_2978" s="T267">geben-PST2.[3SG]</ta>
            <ta e="T269" id="Seg_2979" s="T268">so</ta>
            <ta e="T270" id="Seg_2980" s="T269">reich</ta>
            <ta e="T271" id="Seg_2981" s="T270">Mensch.[NOM]</ta>
            <ta e="T272" id="Seg_2982" s="T271">werden-PST2.[3SG]</ta>
            <ta e="T273" id="Seg_2983" s="T272">dieses-ACC</ta>
            <ta e="T274" id="Seg_2984" s="T273">alt-PL.[NOM]</ta>
            <ta e="T275" id="Seg_2985" s="T274">sprechen-PST2-3PL</ta>
            <ta e="T276" id="Seg_2986" s="T275">2SG.[NOM]</ta>
            <ta e="T277" id="Seg_2987" s="T276">ankommen-CVB.SIM</ta>
            <ta e="T278" id="Seg_2988" s="T277">gehen-EP-PST2-2SG</ta>
            <ta e="T279" id="Seg_2989" s="T278">vor.langer.Zeit</ta>
            <ta e="T280" id="Seg_2990" s="T279">Zar.[NOM]</ta>
            <ta e="T281" id="Seg_2991" s="T280">Steuer-3SG-ABL</ta>
            <ta e="T282" id="Seg_2992" s="T281">weglaufen-CVB.SEQ</ta>
            <ta e="T283" id="Seg_2993" s="T282">kommen-PTCP.PST</ta>
            <ta e="T284" id="Seg_2994" s="T283">Volk-PL-DAT/LOC</ta>
            <ta e="T285" id="Seg_2995" s="T284">jenes-PL-EP-2SG.[NOM]</ta>
            <ta e="T286" id="Seg_2996" s="T285">sich.verstecken-CVB.SEQ</ta>
            <ta e="T287" id="Seg_2997" s="T286">Berg.[NOM]</ta>
            <ta e="T288" id="Seg_2998" s="T287">Inneres-3SG-DAT/LOC</ta>
            <ta e="T289" id="Seg_2999" s="T288">Haus-VBZ-PST1-3PL</ta>
            <ta e="T290" id="Seg_3000" s="T289">zuerst</ta>
            <ta e="T291" id="Seg_3001" s="T290">2SG.[NOM]</ta>
            <ta e="T292" id="Seg_3002" s="T291">Zelt-DAT/LOC</ta>
            <ta e="T293" id="Seg_3003" s="T292">sehen-PTCP.HAB</ta>
            <ta e="T294" id="Seg_3004" s="T293">Mensch-2SG.[NOM]</ta>
            <ta e="T295" id="Seg_3005" s="T294">jenes.[NOM]</ta>
            <ta e="T296" id="Seg_3006" s="T295">3PL.[NOM]</ta>
            <ta e="T297" id="Seg_3007" s="T296">Jäger-3PL.[NOM]</ta>
            <ta e="T298" id="Seg_3008" s="T297">Tier-AG-3PL.[NOM]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_3009" s="T0">тундра.[NOM]</ta>
            <ta e="T2" id="Seg_3010" s="T1">место-3SG-DAT/LOC</ta>
            <ta e="T3" id="Seg_3011" s="T2">глубокий.снег-PROPR</ta>
            <ta e="T4" id="Seg_3012" s="T3">очень</ta>
            <ta e="T5" id="Seg_3013" s="T4">место-DAT/LOC</ta>
            <ta e="T6" id="Seg_3014" s="T5">давно-ADJZ</ta>
            <ta e="T7" id="Seg_3015" s="T6">оставаться-PTCP.PST</ta>
            <ta e="T8" id="Seg_3016" s="T7">чум-DIM.[NOM]</ta>
            <ta e="T9" id="Seg_3017" s="T8">есть-3SG</ta>
            <ta e="T10" id="Seg_3018" s="T9">быть-PST2.[3SG]</ta>
            <ta e="T11" id="Seg_3019" s="T10">тот</ta>
            <ta e="T12" id="Seg_3020" s="T11">чум-DIM-DAT/LOC</ta>
            <ta e="T13" id="Seg_3021" s="T12">вечером</ta>
            <ta e="T14" id="Seg_3022" s="T13">каждый</ta>
            <ta e="T15" id="Seg_3023" s="T14">свет.[NOM]</ta>
            <ta e="T16" id="Seg_3024" s="T15">загораться-PTCP.PRS</ta>
            <ta e="T17" id="Seg_3025" s="T16">становиться-PST2.[3SG]</ta>
            <ta e="T18" id="Seg_3026" s="T17">здесь</ta>
            <ta e="T19" id="Seg_3027" s="T18">есть</ta>
            <ta e="T20" id="Seg_3028" s="T19">быть-PST2.[3SG]</ta>
            <ta e="T21" id="Seg_3029" s="T20">один</ta>
            <ta e="T22" id="Seg_3030" s="T21">мелкий</ta>
            <ta e="T23" id="Seg_3031" s="T22">человек.[NOM]</ta>
            <ta e="T24" id="Seg_3032" s="T23">купец-DAT/LOC</ta>
            <ta e="T25" id="Seg_3033" s="T24">работник.[NOM]</ta>
            <ta e="T26" id="Seg_3034" s="T25">быть-CVB.SIM</ta>
            <ta e="T27" id="Seg_3035" s="T26">идти-PRS.[3SG]</ta>
            <ta e="T28" id="Seg_3036" s="T27">люди-PL.[NOM]</ta>
            <ta e="T29" id="Seg_3037" s="T28">что</ta>
            <ta e="T30" id="Seg_3038" s="T29">свет-3SG.[NOM]</ta>
            <ta e="T31" id="Seg_3039" s="T30">загораться-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T32" id="Seg_3040" s="T31">быть-PST1-3SG</ta>
            <ta e="T33" id="Seg_3041" s="T32">говорить-CVB.SEQ</ta>
            <ta e="T34" id="Seg_3042" s="T33">удивляться-CVB.SEQ</ta>
            <ta e="T35" id="Seg_3043" s="T34">разговаривать-PRS-3PL</ta>
            <ta e="T36" id="Seg_3044" s="T35">этот-DAT/LOC</ta>
            <ta e="T37" id="Seg_3045" s="T36">этот</ta>
            <ta e="T38" id="Seg_3046" s="T37">молодец.[NOM]</ta>
            <ta e="T39" id="Seg_3047" s="T38">человек-2SG.[NOM]</ta>
            <ta e="T40" id="Seg_3048" s="T39">говорить-PRS.[3SG]</ta>
            <ta e="T41" id="Seg_3049" s="T40">купец-3SG-DAT/LOC</ta>
            <ta e="T42" id="Seg_3050" s="T41">вот</ta>
            <ta e="T43" id="Seg_3051" s="T42">заклад-VBZ-EP-RECP/COLL-IMP.1DU</ta>
            <ta e="T44" id="Seg_3052" s="T43">говорить-PRS.[3SG]</ta>
            <ta e="T45" id="Seg_3053" s="T44">1SG.[NOM]</ta>
            <ta e="T46" id="Seg_3054" s="T45">идти-CVB.SEQ</ta>
            <ta e="T47" id="Seg_3055" s="T46">знать-CVB.SEQ</ta>
            <ta e="T48" id="Seg_3056" s="T47">приходить-FUT-1SG</ta>
            <ta e="T49" id="Seg_3057" s="T48">1SG.[NOM]</ta>
            <ta e="T50" id="Seg_3058" s="T49">проиграть-TEMP-1SG</ta>
            <ta e="T51" id="Seg_3059" s="T50">всегда</ta>
            <ta e="T52" id="Seg_3060" s="T51">даром</ta>
            <ta e="T53" id="Seg_3061" s="T52">работать-FUT-1SG</ta>
            <ta e="T54" id="Seg_3062" s="T53">2SG.[NOM]</ta>
            <ta e="T55" id="Seg_3063" s="T54">однако</ta>
            <ta e="T56" id="Seg_3064" s="T55">деньги-ACC</ta>
            <ta e="T57" id="Seg_3065" s="T56">давать-FUT-2SG</ta>
            <ta e="T58" id="Seg_3066" s="T57">тот-INSTR</ta>
            <ta e="T59" id="Seg_3067" s="T58">согласить-EP-RECP/COLL-CVB.SEQ</ta>
            <ta e="T60" id="Seg_3068" s="T59">оставаться-RECP/COLL-PRS-3PL</ta>
            <ta e="T61" id="Seg_3069" s="T60">этот</ta>
            <ta e="T62" id="Seg_3070" s="T61">человек-2SG.[NOM]</ta>
            <ta e="T63" id="Seg_3071" s="T62">вечером</ta>
            <ta e="T64" id="Seg_3072" s="T63">собака-3SG-INSTR</ta>
            <ta e="T65" id="Seg_3073" s="T64">идти-PRS.[3SG]</ta>
            <ta e="T66" id="Seg_3074" s="T65">этот</ta>
            <ta e="T67" id="Seg_3075" s="T66">идти-CVB.SEQ</ta>
            <ta e="T68" id="Seg_3076" s="T67">чум-DAT/LOC</ta>
            <ta e="T69" id="Seg_3077" s="T68">входить-PRS.[3SG]</ta>
            <ta e="T70" id="Seg_3078" s="T69">этот</ta>
            <ta e="T71" id="Seg_3079" s="T70">входить-PST2-3SG</ta>
            <ta e="T72" id="Seg_3080" s="T71">шерсть-PROPR</ta>
            <ta e="T73" id="Seg_3081" s="T72">лицо-PROPR</ta>
            <ta e="T74" id="Seg_3082" s="T73">человек.[NOM]</ta>
            <ta e="T75" id="Seg_3083" s="T74">сидеть-PRS.[3SG]</ta>
            <ta e="T76" id="Seg_3084" s="T75">здесь</ta>
            <ta e="T77" id="Seg_3085" s="T76">боиться-CVB.SIM-боиться-CVB.SIM</ta>
            <ta e="T78" id="Seg_3086" s="T77">молодец-EP-2SG.[NOM]</ta>
            <ta e="T79" id="Seg_3087" s="T78">сесть-PRS.[3SG]</ta>
            <ta e="T80" id="Seg_3088" s="T79">скамья-DAT/LOC</ta>
            <ta e="T81" id="Seg_3089" s="T80">шерсть-PROPR</ta>
            <ta e="T82" id="Seg_3090" s="T81">человек-3SG.[NOM]</ta>
            <ta e="T83" id="Seg_3091" s="T82">пища-VBZ-CVB.SEQ</ta>
            <ta e="T84" id="Seg_3092" s="T83">есть-CVB.PURP</ta>
            <ta e="T85" id="Seg_3093" s="T84">сесть-PTCP.PRS</ta>
            <ta e="T86" id="Seg_3094" s="T85">быть-PST2.[3SG]</ta>
            <ta e="T87" id="Seg_3095" s="T86">вот</ta>
            <ta e="T88" id="Seg_3096" s="T87">пища-3SG-ACC</ta>
            <ta e="T89" id="Seg_3097" s="T88">выкладывать-EP-MED-CVB.SEQ</ta>
            <ta e="T90" id="Seg_3098" s="T89">есть.[IMP.2SG]</ta>
            <ta e="T91" id="Seg_3099" s="T90">говорить-PST2.[3SG]</ta>
            <ta e="T92" id="Seg_3100" s="T91">этот-ACC</ta>
            <ta e="T93" id="Seg_3101" s="T92">есть-CVB.SEQ</ta>
            <ta e="T94" id="Seg_3102" s="T93">после</ta>
            <ta e="T95" id="Seg_3103" s="T94">спать-CVB.PURP</ta>
            <ta e="T96" id="Seg_3104" s="T95">ложиться-PST2-3PL</ta>
            <ta e="T97" id="Seg_3105" s="T96">этот</ta>
            <ta e="T98" id="Seg_3106" s="T97">лежать-TEMP-3SG</ta>
            <ta e="T99" id="Seg_3107" s="T98">утром</ta>
            <ta e="T100" id="Seg_3108" s="T99">рано</ta>
            <ta e="T101" id="Seg_3109" s="T100">человек-3SG.[NOM]</ta>
            <ta e="T102" id="Seg_3110" s="T101">лыжа-INSTR</ta>
            <ta e="T103" id="Seg_3111" s="T102">идти-CVB.SEQ</ta>
            <ta e="T104" id="Seg_3112" s="T103">оставаться-PRS.[3SG]</ta>
            <ta e="T105" id="Seg_3113" s="T104">мелкий</ta>
            <ta e="T106" id="Seg_3114" s="T105">человек-1PL.[NOM]</ta>
            <ta e="T107" id="Seg_3115" s="T106">вставать-PRS.[3SG]</ta>
            <ta e="T108" id="Seg_3116" s="T107">да</ta>
            <ta e="T109" id="Seg_3117" s="T108">думать-CVB.SIM</ta>
            <ta e="T110" id="Seg_3118" s="T109">давать-PRS.[3SG]</ta>
            <ta e="T111" id="Seg_3119" s="T110">как</ta>
            <ta e="T112" id="Seg_3120" s="T111">1SG.[NOM]</ta>
            <ta e="T113" id="Seg_3121" s="T112">здесь</ta>
            <ta e="T114" id="Seg_3122" s="T113">идти-EP-PTCP.PST-1SG-ACC</ta>
            <ta e="T115" id="Seg_3123" s="T114">знать-CAUS-FUT-1SG=Q</ta>
            <ta e="T116" id="Seg_3124" s="T115">думать-PRS.[3SG]</ta>
            <ta e="T117" id="Seg_3125" s="T116">этот</ta>
            <ta e="T118" id="Seg_3126" s="T117">человек.[NOM]</ta>
            <ta e="T119" id="Seg_3127" s="T118">куда</ta>
            <ta e="T120" id="Seg_3128" s="T119">идти-PTCP.PST-3SG-ACC</ta>
            <ta e="T121" id="Seg_3129" s="T120">узнавать-CVB.SEQ</ta>
            <ta e="T122" id="Seg_3130" s="T121">метка.[NOM]</ta>
            <ta e="T123" id="Seg_3131" s="T122">принести-FUT-NEC-1SG</ta>
            <ta e="T124" id="Seg_3132" s="T123">думать-CVB.SEQ</ta>
            <ta e="T125" id="Seg_3133" s="T124">тропка-3SG-ACC</ta>
            <ta e="T126" id="Seg_3134" s="T125">следовать-CVB.SEQ</ta>
            <ta e="T127" id="Seg_3135" s="T126">идти-CVB.SEQ</ta>
            <ta e="T128" id="Seg_3136" s="T127">этот</ta>
            <ta e="T129" id="Seg_3137" s="T128">идти-CVB.SEQ</ta>
            <ta e="T130" id="Seg_3138" s="T129">камень.[NOM]</ta>
            <ta e="T131" id="Seg_3139" s="T130">гора-DAT/LOC</ta>
            <ta e="T132" id="Seg_3140" s="T131">доезжать-PRS.[3SG]</ta>
            <ta e="T133" id="Seg_3141" s="T132">здесь</ta>
            <ta e="T134" id="Seg_3142" s="T133">прятаться-CVB.SEQ</ta>
            <ta e="T135" id="Seg_3143" s="T134">приходить-PST2-3SG</ta>
            <ta e="T136" id="Seg_3144" s="T135">этот</ta>
            <ta e="T137" id="Seg_3145" s="T136">камень.[NOM]</ta>
            <ta e="T138" id="Seg_3146" s="T137">гора.[NOM]</ta>
            <ta e="T139" id="Seg_3147" s="T138">полностью</ta>
            <ta e="T140" id="Seg_3148" s="T139">копать-EP-PASS/REFL-EP-PTCP.PST.[NOM]</ta>
            <ta e="T141" id="Seg_3149" s="T140">люди.[NOM]</ta>
            <ta e="T142" id="Seg_3150" s="T141">бормотанье-3SG.[NOM]</ta>
            <ta e="T143" id="Seg_3151" s="T142">говорить-CVB.SEQ</ta>
            <ta e="T144" id="Seg_3152" s="T143">голос-3PL.[NOM]</ta>
            <ta e="T145" id="Seg_3153" s="T144">говорить-CVB.SEQ</ta>
            <ta e="T146" id="Seg_3154" s="T145">загладывать-CVB.SEQ</ta>
            <ta e="T147" id="Seg_3155" s="T146">видеть-PST2-3SG</ta>
            <ta e="T148" id="Seg_3156" s="T147">отверстие-EP-INSTR</ta>
            <ta e="T149" id="Seg_3157" s="T148">человек-3PL.[NOM]</ta>
            <ta e="T150" id="Seg_3158" s="T149">полностью</ta>
            <ta e="T151" id="Seg_3159" s="T150">шерсть-PROPR.[NOM]</ta>
            <ta e="T152" id="Seg_3160" s="T151">лицо-PROPR.[NOM]</ta>
            <ta e="T153" id="Seg_3161" s="T152">здесь</ta>
            <ta e="T154" id="Seg_3162" s="T153">подкрадываться-CVB.SEQ</ta>
            <ta e="T155" id="Seg_3163" s="T154">входить-PST2.[3SG]</ta>
            <ta e="T156" id="Seg_3164" s="T155">этот</ta>
            <ta e="T157" id="Seg_3165" s="T156">входить-CVB.SEQ</ta>
            <ta e="T158" id="Seg_3166" s="T157">этот</ta>
            <ta e="T159" id="Seg_3167" s="T158">дом-DAT/LOC</ta>
            <ta e="T160" id="Seg_3168" s="T159">юрта-DAT/LOC</ta>
            <ta e="T161" id="Seg_3169" s="T160">входить-CVB.SEQ</ta>
            <ta e="T162" id="Seg_3170" s="T161">сесть-PRS.[3SG]</ta>
            <ta e="T163" id="Seg_3171" s="T162">этот</ta>
            <ta e="T164" id="Seg_3172" s="T163">сесть-PST2-3SG</ta>
            <ta e="T165" id="Seg_3173" s="T164">один</ta>
            <ta e="T166" id="Seg_3174" s="T165">жена.[NOM]</ta>
            <ta e="T167" id="Seg_3175" s="T166">есть</ta>
            <ta e="T168" id="Seg_3176" s="T167">быть-PST2.[3SG]</ta>
            <ta e="T169" id="Seg_3177" s="T168">тот.[NOM]</ta>
            <ta e="T170" id="Seg_3178" s="T169">потихоньку</ta>
            <ta e="T171" id="Seg_3179" s="T170">спрашивать-PRS.[3SG]</ta>
            <ta e="T172" id="Seg_3180" s="T171">эй</ta>
            <ta e="T173" id="Seg_3181" s="T172">что-EP-INSTR</ta>
            <ta e="T174" id="Seg_3182" s="T173">нуждаться-CVB.SEQ</ta>
            <ta e="T175" id="Seg_3183" s="T174">приходить-PST1-2SG</ta>
            <ta e="T176" id="Seg_3184" s="T175">говорить-PRS.[3SG]</ta>
            <ta e="T177" id="Seg_3185" s="T176">этот-ACC</ta>
            <ta e="T178" id="Seg_3186" s="T177">говорить-PRS.[3SG]</ta>
            <ta e="T179" id="Seg_3187" s="T178">небольшой</ta>
            <ta e="T180" id="Seg_3188" s="T179">дело-1SG-INSTR</ta>
            <ta e="T181" id="Seg_3189" s="T180">говорить-PRS.[3SG]</ta>
            <ta e="T182" id="Seg_3190" s="T181">тот-DAT/LOC</ta>
            <ta e="T183" id="Seg_3191" s="T182">этот</ta>
            <ta e="T184" id="Seg_3192" s="T183">жена-3SG.[NOM]</ta>
            <ta e="T185" id="Seg_3193" s="T184">что-ACC</ta>
            <ta e="T186" id="Seg_3194" s="T185">INDEF</ta>
            <ta e="T187" id="Seg_3195" s="T186">мягкий</ta>
            <ta e="T188" id="Seg_3196" s="T187">очень-ACC</ta>
            <ta e="T189" id="Seg_3197" s="T188">подать-CVB.SEQ</ta>
            <ta e="T190" id="Seg_3198" s="T189">бросать-PRS.[3SG]</ta>
            <ta e="T191" id="Seg_3199" s="T190">тот-ACC</ta>
            <ta e="T192" id="Seg_3200" s="T191">видеть-EP-NEG.CVB</ta>
            <ta e="T193" id="Seg_3201" s="T192">только</ta>
            <ta e="T194" id="Seg_3202" s="T193">карман-3SG-DAT/LOC</ta>
            <ta e="T195" id="Seg_3203" s="T194">вставлять-MED-CVB.SEQ</ta>
            <ta e="T196" id="Seg_3204" s="T195">бросать-PRS.[3SG]</ta>
            <ta e="T197" id="Seg_3205" s="T196">жена-3SG.[NOM]</ta>
            <ta e="T198" id="Seg_3206" s="T197">говорить-PRS.[3SG]</ta>
            <ta e="T199" id="Seg_3207" s="T198">вот</ta>
            <ta e="T200" id="Seg_3208" s="T199">стараться.[IMP.2SG]</ta>
            <ta e="T201" id="Seg_3209" s="T200">идти-CVB.SEQ</ta>
            <ta e="T202" id="Seg_3210" s="T201">видеть.[IMP.2SG]</ta>
            <ta e="T203" id="Seg_3211" s="T202">здесь</ta>
            <ta e="T204" id="Seg_3212" s="T203">народ-PL-EP-1SG.[NOM]</ta>
            <ta e="T205" id="Seg_3213" s="T204">видеть-TEMP-3PL</ta>
            <ta e="T206" id="Seg_3214" s="T205">человек-VBZ-FUT-3PL</ta>
            <ta e="T207" id="Seg_3215" s="T206">NEG-3SG</ta>
            <ta e="T208" id="Seg_3216" s="T207">говорить-PST2.[3SG]</ta>
            <ta e="T209" id="Seg_3217" s="T208">тот-ACC</ta>
            <ta e="T210" id="Seg_3218" s="T209">слышать-CVB.ANT</ta>
            <ta e="T211" id="Seg_3219" s="T210">мелкий</ta>
            <ta e="T212" id="Seg_3220" s="T211">человек.[NOM]</ta>
            <ta e="T213" id="Seg_3221" s="T212">выйти-EP-PST2.[3SG]</ta>
            <ta e="T214" id="Seg_3222" s="T213">собака-3SG-DAT/LOC</ta>
            <ta e="T215" id="Seg_3223" s="T214">сесть-PST2.[3SG]</ta>
            <ta e="T216" id="Seg_3224" s="T215">да</ta>
            <ta e="T217" id="Seg_3225" s="T216">идти-PTCP.PRS-DAT/LOC</ta>
            <ta e="T218" id="Seg_3226" s="T217">становиться-PST2.[3SG]</ta>
            <ta e="T219" id="Seg_3227" s="T218">этот</ta>
            <ta e="T220" id="Seg_3228" s="T219">идти-CVB.SEQ</ta>
            <ta e="T221" id="Seg_3229" s="T220">ехать-CVB.SEQ</ta>
            <ta e="T222" id="Seg_3230" s="T221">задняя.часть-3SG-ACC</ta>
            <ta e="T223" id="Seg_3231" s="T222">видеть-PST2-3SG</ta>
            <ta e="T224" id="Seg_3232" s="T223">один</ta>
            <ta e="T225" id="Seg_3233" s="T224">человек.[NOM]</ta>
            <ta e="T226" id="Seg_3234" s="T225">следовать-CVB.SEQ</ta>
            <ta e="T227" id="Seg_3235" s="T226">идти-PRS.[3SG]</ta>
            <ta e="T228" id="Seg_3236" s="T227">тот-3SG-3SG.[NOM]</ta>
            <ta e="T229" id="Seg_3237" s="T228">догонять-CVB.SEQ</ta>
            <ta e="T230" id="Seg_3238" s="T229">идти-PTCP.PRS-3SG-ACC</ta>
            <ta e="T231" id="Seg_3239" s="T230">видеть-CVB.SEQ</ta>
            <ta e="T232" id="Seg_3240" s="T231">этот</ta>
            <ta e="T233" id="Seg_3241" s="T232">человек.[NOM]</ta>
            <ta e="T234" id="Seg_3242" s="T233">бежать-EP-CAUS-PST2.[3SG]</ta>
            <ta e="T235" id="Seg_3243" s="T234">шерсть-PROPR</ta>
            <ta e="T236" id="Seg_3244" s="T235">лицо-PROPR</ta>
            <ta e="T237" id="Seg_3245" s="T236">человек-3SG.[NOM]</ta>
            <ta e="T238" id="Seg_3246" s="T237">сани-3SG-GEN</ta>
            <ta e="T239" id="Seg_3247" s="T238">задний</ta>
            <ta e="T240" id="Seg_3248" s="T239">нога-3SG-ABL</ta>
            <ta e="T241" id="Seg_3249" s="T240">взять-CVB.SEQ</ta>
            <ta e="T242" id="Seg_3250" s="T241">два</ta>
            <ta e="T243" id="Seg_3251" s="T242">нога-3SG-ACC</ta>
            <ta e="T244" id="Seg_3252" s="T243">оставаться-CAUS-PST2.[3SG]</ta>
            <ta e="T245" id="Seg_3253" s="T244">попадать-REFL-PTCP.PRS-2SG-ACC</ta>
            <ta e="T246" id="Seg_3254" s="T245">видеть-CAUS-PTCP.FUT</ta>
            <ta e="T247" id="Seg_3255" s="T246">быть-PST1-1SG</ta>
            <ta e="T248" id="Seg_3256" s="T247">говорить-CVB.SEQ</ta>
            <ta e="T249" id="Seg_3257" s="T248">кричать-CVB.SEQ</ta>
            <ta e="T250" id="Seg_3258" s="T249">оставаться-PST2.[3SG]</ta>
            <ta e="T251" id="Seg_3259" s="T250">этот</ta>
            <ta e="T252" id="Seg_3260" s="T251">мальчик.[NOM]</ta>
            <ta e="T253" id="Seg_3261" s="T252">дом-3SG-DAT/LOC</ta>
            <ta e="T254" id="Seg_3262" s="T253">приходить-CVB.SEQ</ta>
            <ta e="T255" id="Seg_3263" s="T254">метка.[NOM]</ta>
            <ta e="T256" id="Seg_3264" s="T255">принести-PST1-1SG</ta>
            <ta e="T257" id="Seg_3265" s="T256">говорить-CVB.SEQ</ta>
            <ta e="T258" id="Seg_3266" s="T257">карман-3SG-ABL</ta>
            <ta e="T259" id="Seg_3267" s="T258">мягкий-3SG-ACC</ta>
            <ta e="T260" id="Seg_3268" s="T259">вынимать-PST2-3SG</ta>
            <ta e="T261" id="Seg_3269" s="T260">черный</ta>
            <ta e="T262" id="Seg_3270" s="T261">лиса.[NOM]</ta>
            <ta e="T263" id="Seg_3271" s="T262">быть-PST2.[3SG]</ta>
            <ta e="T264" id="Seg_3272" s="T263">купец-3SG.[NOM]</ta>
            <ta e="T265" id="Seg_3273" s="T264">однако</ta>
            <ta e="T266" id="Seg_3274" s="T265">деньги.[NOM]</ta>
            <ta e="T267" id="Seg_3275" s="T266">очень.много</ta>
            <ta e="T268" id="Seg_3276" s="T267">давать-PST2.[3SG]</ta>
            <ta e="T269" id="Seg_3277" s="T268">так</ta>
            <ta e="T270" id="Seg_3278" s="T269">богатый</ta>
            <ta e="T271" id="Seg_3279" s="T270">человек.[NOM]</ta>
            <ta e="T272" id="Seg_3280" s="T271">становиться-PST2.[3SG]</ta>
            <ta e="T273" id="Seg_3281" s="T272">этот-ACC</ta>
            <ta e="T274" id="Seg_3282" s="T273">старый-PL.[NOM]</ta>
            <ta e="T275" id="Seg_3283" s="T274">говорить-PST2-3PL</ta>
            <ta e="T276" id="Seg_3284" s="T275">2SG.[NOM]</ta>
            <ta e="T277" id="Seg_3285" s="T276">доезжать-CVB.SIM</ta>
            <ta e="T278" id="Seg_3286" s="T277">идти-EP-PST2-2SG</ta>
            <ta e="T279" id="Seg_3287" s="T278">давно</ta>
            <ta e="T280" id="Seg_3288" s="T279">царь.[NOM]</ta>
            <ta e="T281" id="Seg_3289" s="T280">подать-3SG-ABL</ta>
            <ta e="T282" id="Seg_3290" s="T281">убегать-CVB.SEQ</ta>
            <ta e="T283" id="Seg_3291" s="T282">приходить-PTCP.PST</ta>
            <ta e="T284" id="Seg_3292" s="T283">народ-PL-DAT/LOC</ta>
            <ta e="T285" id="Seg_3293" s="T284">тот-PL-EP-2SG.[NOM]</ta>
            <ta e="T286" id="Seg_3294" s="T285">прятаться-CVB.SEQ</ta>
            <ta e="T287" id="Seg_3295" s="T286">гора.[NOM]</ta>
            <ta e="T288" id="Seg_3296" s="T287">нутро-3SG-DAT/LOC</ta>
            <ta e="T289" id="Seg_3297" s="T288">дом-VBZ-PST1-3PL</ta>
            <ta e="T290" id="Seg_3298" s="T289">сначала</ta>
            <ta e="T291" id="Seg_3299" s="T290">2SG.[NOM]</ta>
            <ta e="T292" id="Seg_3300" s="T291">чум-DAT/LOC</ta>
            <ta e="T293" id="Seg_3301" s="T292">видеть-PTCP.HAB</ta>
            <ta e="T294" id="Seg_3302" s="T293">человек-2SG.[NOM]</ta>
            <ta e="T295" id="Seg_3303" s="T294">тот.[NOM]</ta>
            <ta e="T296" id="Seg_3304" s="T295">3PL.[NOM]</ta>
            <ta e="T297" id="Seg_3305" s="T296">охотник-3PL.[NOM]</ta>
            <ta e="T298" id="Seg_3306" s="T297">зверь-AG-3PL.[NOM]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_3307" s="T0">n-n:case</ta>
            <ta e="T2" id="Seg_3308" s="T1">n-n:poss-n:case</ta>
            <ta e="T3" id="Seg_3309" s="T2">n-n&gt;adj</ta>
            <ta e="T4" id="Seg_3310" s="T3">ptcl</ta>
            <ta e="T5" id="Seg_3311" s="T4">n-n:case</ta>
            <ta e="T6" id="Seg_3312" s="T5">adv-adv&gt;adj</ta>
            <ta e="T7" id="Seg_3313" s="T6">v-v:ptcp</ta>
            <ta e="T8" id="Seg_3314" s="T7">n-n&gt;n-n:case</ta>
            <ta e="T9" id="Seg_3315" s="T8">ptcl-ptcl:(poss.pn)</ta>
            <ta e="T10" id="Seg_3316" s="T9">v-v:tense-v:pred.pn</ta>
            <ta e="T11" id="Seg_3317" s="T10">dempro</ta>
            <ta e="T12" id="Seg_3318" s="T11">n-n&gt;n-n:case</ta>
            <ta e="T13" id="Seg_3319" s="T12">adv</ta>
            <ta e="T14" id="Seg_3320" s="T13">adj</ta>
            <ta e="T15" id="Seg_3321" s="T14">n-n:case</ta>
            <ta e="T16" id="Seg_3322" s="T15">v-v:ptcp</ta>
            <ta e="T17" id="Seg_3323" s="T16">v-v:tense-v:pred.pn</ta>
            <ta e="T18" id="Seg_3324" s="T17">adv</ta>
            <ta e="T19" id="Seg_3325" s="T18">ptcl</ta>
            <ta e="T20" id="Seg_3326" s="T19">v-v:tense-v:pred.pn</ta>
            <ta e="T21" id="Seg_3327" s="T20">cardnum</ta>
            <ta e="T22" id="Seg_3328" s="T21">adj</ta>
            <ta e="T23" id="Seg_3329" s="T22">n-n:case</ta>
            <ta e="T24" id="Seg_3330" s="T23">n-n:case</ta>
            <ta e="T25" id="Seg_3331" s="T24">n-n:case</ta>
            <ta e="T26" id="Seg_3332" s="T25">v-v:cvb</ta>
            <ta e="T27" id="Seg_3333" s="T26">v-v:tense-v:pred.pn</ta>
            <ta e="T28" id="Seg_3334" s="T27">n-n:(num)-n:case</ta>
            <ta e="T29" id="Seg_3335" s="T28">que</ta>
            <ta e="T30" id="Seg_3336" s="T29">n-n:(poss)-n:case</ta>
            <ta e="T31" id="Seg_3337" s="T30">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T32" id="Seg_3338" s="T31">v-v:tense-v:poss.pn</ta>
            <ta e="T33" id="Seg_3339" s="T32">v-v:cvb</ta>
            <ta e="T34" id="Seg_3340" s="T33">v-v:cvb</ta>
            <ta e="T35" id="Seg_3341" s="T34">v-v:tense-v:pred.pn</ta>
            <ta e="T36" id="Seg_3342" s="T35">dempro-pro:case</ta>
            <ta e="T37" id="Seg_3343" s="T36">dempro</ta>
            <ta e="T38" id="Seg_3344" s="T37">n-n:case</ta>
            <ta e="T39" id="Seg_3345" s="T38">n-n:(poss)-n:case</ta>
            <ta e="T40" id="Seg_3346" s="T39">v-v:tense-v:pred.pn</ta>
            <ta e="T41" id="Seg_3347" s="T40">n-n:poss-n:case</ta>
            <ta e="T42" id="Seg_3348" s="T41">ptcl</ta>
            <ta e="T43" id="Seg_3349" s="T42">n-n&gt;v-v:(ins)-v&gt;v-v:mood.pn</ta>
            <ta e="T44" id="Seg_3350" s="T43">v-v:tense-v:pred.pn</ta>
            <ta e="T45" id="Seg_3351" s="T44">pers-pro:case</ta>
            <ta e="T46" id="Seg_3352" s="T45">v-v:cvb</ta>
            <ta e="T47" id="Seg_3353" s="T46">v-v:cvb</ta>
            <ta e="T48" id="Seg_3354" s="T47">v-v:tense-v:poss.pn</ta>
            <ta e="T49" id="Seg_3355" s="T48">pers-pro:case</ta>
            <ta e="T50" id="Seg_3356" s="T49">v-v:mood-v:temp.pn</ta>
            <ta e="T51" id="Seg_3357" s="T50">adv</ta>
            <ta e="T52" id="Seg_3358" s="T51">adv</ta>
            <ta e="T53" id="Seg_3359" s="T52">v-v:tense-v:poss.pn</ta>
            <ta e="T54" id="Seg_3360" s="T53">pers-pro:case</ta>
            <ta e="T55" id="Seg_3361" s="T54">ptcl</ta>
            <ta e="T56" id="Seg_3362" s="T55">n-n:case</ta>
            <ta e="T57" id="Seg_3363" s="T56">v-v:tense-v:poss.pn</ta>
            <ta e="T58" id="Seg_3364" s="T57">dempro-pro:case</ta>
            <ta e="T59" id="Seg_3365" s="T58">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T60" id="Seg_3366" s="T59">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T61" id="Seg_3367" s="T60">dempro</ta>
            <ta e="T62" id="Seg_3368" s="T61">n-n:(poss)-n:case</ta>
            <ta e="T63" id="Seg_3369" s="T62">adv</ta>
            <ta e="T64" id="Seg_3370" s="T63">n-n:poss-n:case</ta>
            <ta e="T65" id="Seg_3371" s="T64">v-v:tense-v:pred.pn</ta>
            <ta e="T66" id="Seg_3372" s="T65">dempro</ta>
            <ta e="T67" id="Seg_3373" s="T66">v-v:cvb</ta>
            <ta e="T68" id="Seg_3374" s="T67">n-n:case</ta>
            <ta e="T69" id="Seg_3375" s="T68">v-v:tense-v:pred.pn</ta>
            <ta e="T70" id="Seg_3376" s="T69">dempro</ta>
            <ta e="T71" id="Seg_3377" s="T70">v-v:tense-v:poss.pn</ta>
            <ta e="T72" id="Seg_3378" s="T71">n-n&gt;adj</ta>
            <ta e="T73" id="Seg_3379" s="T72">n-n&gt;adj</ta>
            <ta e="T74" id="Seg_3380" s="T73">n-n:case</ta>
            <ta e="T75" id="Seg_3381" s="T74">v-v:tense-v:pred.pn</ta>
            <ta e="T76" id="Seg_3382" s="T75">adv</ta>
            <ta e="T77" id="Seg_3383" s="T76">v-v:cvb-v-v:cvb</ta>
            <ta e="T78" id="Seg_3384" s="T77">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T79" id="Seg_3385" s="T78">v-v:tense-v:pred.pn</ta>
            <ta e="T80" id="Seg_3386" s="T79">n-n:case</ta>
            <ta e="T81" id="Seg_3387" s="T80">n-n&gt;adj</ta>
            <ta e="T82" id="Seg_3388" s="T81">n-n:(poss)-n:case</ta>
            <ta e="T83" id="Seg_3389" s="T82">n-n&gt;v-v:cvb</ta>
            <ta e="T84" id="Seg_3390" s="T83">v-v:cvb</ta>
            <ta e="T85" id="Seg_3391" s="T84">v-v:ptcp</ta>
            <ta e="T86" id="Seg_3392" s="T85">v-v:tense-v:pred.pn</ta>
            <ta e="T87" id="Seg_3393" s="T86">ptcl</ta>
            <ta e="T88" id="Seg_3394" s="T87">n-n:poss-n:case</ta>
            <ta e="T89" id="Seg_3395" s="T88">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T90" id="Seg_3396" s="T89">v-v:mood.pn</ta>
            <ta e="T91" id="Seg_3397" s="T90">v-v:tense-v:pred.pn</ta>
            <ta e="T92" id="Seg_3398" s="T91">dempro-pro:case</ta>
            <ta e="T93" id="Seg_3399" s="T92">v-v:cvb</ta>
            <ta e="T94" id="Seg_3400" s="T93">post</ta>
            <ta e="T95" id="Seg_3401" s="T94">v-v:cvb</ta>
            <ta e="T96" id="Seg_3402" s="T95">v-v:tense-v:pred.pn</ta>
            <ta e="T97" id="Seg_3403" s="T96">dempro</ta>
            <ta e="T98" id="Seg_3404" s="T97">v-v:mood-v:temp.pn</ta>
            <ta e="T99" id="Seg_3405" s="T98">adv</ta>
            <ta e="T100" id="Seg_3406" s="T99">adv</ta>
            <ta e="T101" id="Seg_3407" s="T100">n-n:(poss)-n:case</ta>
            <ta e="T102" id="Seg_3408" s="T101">n-n:case</ta>
            <ta e="T103" id="Seg_3409" s="T102">v-v:cvb</ta>
            <ta e="T104" id="Seg_3410" s="T103">v-v:tense-v:pred.pn</ta>
            <ta e="T105" id="Seg_3411" s="T104">adj</ta>
            <ta e="T106" id="Seg_3412" s="T105">n-n:(poss)-n:case</ta>
            <ta e="T107" id="Seg_3413" s="T106">v-v:tense-v:pred.pn</ta>
            <ta e="T108" id="Seg_3414" s="T107">conj</ta>
            <ta e="T109" id="Seg_3415" s="T108">v-v:cvb</ta>
            <ta e="T110" id="Seg_3416" s="T109">v-v:tense-v:pred.pn</ta>
            <ta e="T111" id="Seg_3417" s="T110">que</ta>
            <ta e="T112" id="Seg_3418" s="T111">pers-pro:case</ta>
            <ta e="T113" id="Seg_3419" s="T112">adv</ta>
            <ta e="T114" id="Seg_3420" s="T113">v-v:(ins)-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T115" id="Seg_3421" s="T114">v-v&gt;v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T116" id="Seg_3422" s="T115">v-v:tense-v:pred.pn</ta>
            <ta e="T117" id="Seg_3423" s="T116">dempro</ta>
            <ta e="T118" id="Seg_3424" s="T117">n-n:case</ta>
            <ta e="T119" id="Seg_3425" s="T118">que</ta>
            <ta e="T120" id="Seg_3426" s="T119">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T121" id="Seg_3427" s="T120">v-v:cvb</ta>
            <ta e="T122" id="Seg_3428" s="T121">n-n:case</ta>
            <ta e="T123" id="Seg_3429" s="T122">v-v:tense-v:mood-v:pred.pn</ta>
            <ta e="T124" id="Seg_3430" s="T123">v-v:cvb</ta>
            <ta e="T125" id="Seg_3431" s="T124">n-n:poss-n:case</ta>
            <ta e="T126" id="Seg_3432" s="T125">v-v:cvb</ta>
            <ta e="T127" id="Seg_3433" s="T126">v-v:cvb</ta>
            <ta e="T128" id="Seg_3434" s="T127">dempro</ta>
            <ta e="T129" id="Seg_3435" s="T128">v-v:cvb</ta>
            <ta e="T130" id="Seg_3436" s="T129">n-n:case</ta>
            <ta e="T131" id="Seg_3437" s="T130">n-n:case</ta>
            <ta e="T132" id="Seg_3438" s="T131">v-v:tense-v:pred.pn</ta>
            <ta e="T133" id="Seg_3439" s="T132">adv</ta>
            <ta e="T134" id="Seg_3440" s="T133">v-v:cvb</ta>
            <ta e="T135" id="Seg_3441" s="T134">v-v:tense-v:poss.pn</ta>
            <ta e="T136" id="Seg_3442" s="T135">dempro</ta>
            <ta e="T137" id="Seg_3443" s="T136">n-n:case</ta>
            <ta e="T138" id="Seg_3444" s="T137">n-n:case</ta>
            <ta e="T139" id="Seg_3445" s="T138">adv</ta>
            <ta e="T140" id="Seg_3446" s="T139">v-v:(ins)-v&gt;v-v:(ins)-v:ptcp-v:(case)</ta>
            <ta e="T141" id="Seg_3447" s="T140">n-n:case</ta>
            <ta e="T142" id="Seg_3448" s="T141">n-n:(poss)-n:case</ta>
            <ta e="T143" id="Seg_3449" s="T142">v-v:cvb</ta>
            <ta e="T144" id="Seg_3450" s="T143">n-n:(poss)-n:case</ta>
            <ta e="T145" id="Seg_3451" s="T144">v-v:cvb</ta>
            <ta e="T146" id="Seg_3452" s="T145">v-v:cvb</ta>
            <ta e="T147" id="Seg_3453" s="T146">v-v:tense-v:poss.pn</ta>
            <ta e="T148" id="Seg_3454" s="T147">n-n:(ins)-n:case</ta>
            <ta e="T149" id="Seg_3455" s="T148">n-n:(poss)-n:case</ta>
            <ta e="T150" id="Seg_3456" s="T149">adv</ta>
            <ta e="T151" id="Seg_3457" s="T150">n-n&gt;adj-n:case</ta>
            <ta e="T152" id="Seg_3458" s="T151">n-n&gt;adj-n:case</ta>
            <ta e="T153" id="Seg_3459" s="T152">adv</ta>
            <ta e="T154" id="Seg_3460" s="T153">v-v:cvb</ta>
            <ta e="T155" id="Seg_3461" s="T154">v-v:tense-v:pred.pn</ta>
            <ta e="T156" id="Seg_3462" s="T155">dempro</ta>
            <ta e="T157" id="Seg_3463" s="T156">v-v:cvb</ta>
            <ta e="T158" id="Seg_3464" s="T157">dempro</ta>
            <ta e="T159" id="Seg_3465" s="T158">n-n:case</ta>
            <ta e="T160" id="Seg_3466" s="T159">n-n:case</ta>
            <ta e="T161" id="Seg_3467" s="T160">v-v:cvb</ta>
            <ta e="T162" id="Seg_3468" s="T161">v-v:tense-v:pred.pn</ta>
            <ta e="T163" id="Seg_3469" s="T162">dempro</ta>
            <ta e="T164" id="Seg_3470" s="T163">v-v:tense-v:poss.pn</ta>
            <ta e="T165" id="Seg_3471" s="T164">cardnum</ta>
            <ta e="T166" id="Seg_3472" s="T165">n-n:case</ta>
            <ta e="T167" id="Seg_3473" s="T166">ptcl</ta>
            <ta e="T168" id="Seg_3474" s="T167">v-v:tense-v:pred.pn</ta>
            <ta e="T169" id="Seg_3475" s="T168">dempro-pro:case</ta>
            <ta e="T170" id="Seg_3476" s="T169">adv</ta>
            <ta e="T171" id="Seg_3477" s="T170">v-v:tense-v:pred.pn</ta>
            <ta e="T172" id="Seg_3478" s="T171">interj</ta>
            <ta e="T173" id="Seg_3479" s="T172">que-pro:(ins)-pro:case</ta>
            <ta e="T174" id="Seg_3480" s="T173">v-v:cvb</ta>
            <ta e="T175" id="Seg_3481" s="T174">v-v:tense-v:poss.pn</ta>
            <ta e="T176" id="Seg_3482" s="T175">v-v:tense-v:pred.pn</ta>
            <ta e="T177" id="Seg_3483" s="T176">dempro-pro:case</ta>
            <ta e="T178" id="Seg_3484" s="T177">v-v:tense-v:pred.pn</ta>
            <ta e="T179" id="Seg_3485" s="T178">adj</ta>
            <ta e="T180" id="Seg_3486" s="T179">n-n:poss-n:case</ta>
            <ta e="T181" id="Seg_3487" s="T180">v-v:tense-v:pred.pn</ta>
            <ta e="T182" id="Seg_3488" s="T181">dempro-pro:case</ta>
            <ta e="T183" id="Seg_3489" s="T182">dempro</ta>
            <ta e="T184" id="Seg_3490" s="T183">n-n:(poss)-n:case</ta>
            <ta e="T185" id="Seg_3491" s="T184">que-pro:case</ta>
            <ta e="T186" id="Seg_3492" s="T185">ptcl</ta>
            <ta e="T187" id="Seg_3493" s="T186">adj</ta>
            <ta e="T188" id="Seg_3494" s="T187">ptcl-n:case</ta>
            <ta e="T189" id="Seg_3495" s="T188">v-v:cvb</ta>
            <ta e="T190" id="Seg_3496" s="T189">v-v:tense-v:pred.pn</ta>
            <ta e="T191" id="Seg_3497" s="T190">dempro-pro:case</ta>
            <ta e="T192" id="Seg_3498" s="T191">v-v:(ins)-v:cvb</ta>
            <ta e="T193" id="Seg_3499" s="T192">ptcl</ta>
            <ta e="T194" id="Seg_3500" s="T193">n-n:poss-n:case</ta>
            <ta e="T195" id="Seg_3501" s="T194">v-v&gt;v-v:cvb</ta>
            <ta e="T196" id="Seg_3502" s="T195">v-v:tense-v:pred.pn</ta>
            <ta e="T197" id="Seg_3503" s="T196">n-n:(poss)-n:case</ta>
            <ta e="T198" id="Seg_3504" s="T197">v-v:tense-v:pred.pn</ta>
            <ta e="T199" id="Seg_3505" s="T198">ptcl</ta>
            <ta e="T200" id="Seg_3506" s="T199">v-v:mood.pn</ta>
            <ta e="T201" id="Seg_3507" s="T200">v-v:cvb</ta>
            <ta e="T202" id="Seg_3508" s="T201">v-v:mood.pn</ta>
            <ta e="T203" id="Seg_3509" s="T202">adv</ta>
            <ta e="T204" id="Seg_3510" s="T203">n-n:(num)-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T205" id="Seg_3511" s="T204">v-v:mood-v:temp.pn</ta>
            <ta e="T206" id="Seg_3512" s="T205">n-n&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T207" id="Seg_3513" s="T206">ptcl-ptcl:(poss.pn)</ta>
            <ta e="T208" id="Seg_3514" s="T207">v-v:tense-v:pred.pn</ta>
            <ta e="T209" id="Seg_3515" s="T208">dempro-pro:case</ta>
            <ta e="T210" id="Seg_3516" s="T209">v-v:cvb</ta>
            <ta e="T211" id="Seg_3517" s="T210">adj</ta>
            <ta e="T212" id="Seg_3518" s="T211">n-n:case</ta>
            <ta e="T213" id="Seg_3519" s="T212">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T214" id="Seg_3520" s="T213">n-n:poss-n:case</ta>
            <ta e="T215" id="Seg_3521" s="T214">v-v:tense-v:pred.pn</ta>
            <ta e="T216" id="Seg_3522" s="T215">conj</ta>
            <ta e="T217" id="Seg_3523" s="T216">v-v:ptcp-v:(case)</ta>
            <ta e="T218" id="Seg_3524" s="T217">v-v:tense-v:pred.pn</ta>
            <ta e="T219" id="Seg_3525" s="T218">dempro</ta>
            <ta e="T220" id="Seg_3526" s="T219">v-v:cvb</ta>
            <ta e="T221" id="Seg_3527" s="T220">v-v:cvb</ta>
            <ta e="T222" id="Seg_3528" s="T221">n-n:poss-n:case</ta>
            <ta e="T223" id="Seg_3529" s="T222">v-v:tense-v:poss.pn</ta>
            <ta e="T224" id="Seg_3530" s="T223">cardnum</ta>
            <ta e="T225" id="Seg_3531" s="T224">n-n:case</ta>
            <ta e="T226" id="Seg_3532" s="T225">v-v:cvb</ta>
            <ta e="T227" id="Seg_3533" s="T226">v-v:tense-v:pred.pn</ta>
            <ta e="T228" id="Seg_3534" s="T227">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T229" id="Seg_3535" s="T228">v-v:cvb</ta>
            <ta e="T230" id="Seg_3536" s="T229">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T231" id="Seg_3537" s="T230">v-v:cvb</ta>
            <ta e="T232" id="Seg_3538" s="T231">dempro</ta>
            <ta e="T233" id="Seg_3539" s="T232">n-n:case</ta>
            <ta e="T234" id="Seg_3540" s="T233">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T235" id="Seg_3541" s="T234">n-n&gt;adj</ta>
            <ta e="T236" id="Seg_3542" s="T235">n-n&gt;adj</ta>
            <ta e="T237" id="Seg_3543" s="T236">n-n:(poss)-n:case</ta>
            <ta e="T238" id="Seg_3544" s="T237">n-n:poss-n:case</ta>
            <ta e="T239" id="Seg_3545" s="T238">adj</ta>
            <ta e="T240" id="Seg_3546" s="T239">n-n:poss-n:case</ta>
            <ta e="T241" id="Seg_3547" s="T240">v-v:cvb</ta>
            <ta e="T242" id="Seg_3548" s="T241">cardnum</ta>
            <ta e="T243" id="Seg_3549" s="T242">n-n:poss-n:case</ta>
            <ta e="T244" id="Seg_3550" s="T243">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T245" id="Seg_3551" s="T244">v-v&gt;v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T246" id="Seg_3552" s="T245">v-v&gt;v-v:ptcp</ta>
            <ta e="T247" id="Seg_3553" s="T246">v-v:tense-v:poss.pn</ta>
            <ta e="T248" id="Seg_3554" s="T247">v-v:cvb</ta>
            <ta e="T249" id="Seg_3555" s="T248">v-v:cvb</ta>
            <ta e="T250" id="Seg_3556" s="T249">v-v:tense-v:pred.pn</ta>
            <ta e="T251" id="Seg_3557" s="T250">dempro</ta>
            <ta e="T252" id="Seg_3558" s="T251">n-n:case</ta>
            <ta e="T253" id="Seg_3559" s="T252">n-n:poss-n:case</ta>
            <ta e="T254" id="Seg_3560" s="T253">v-v:cvb</ta>
            <ta e="T255" id="Seg_3561" s="T254">n-n:case</ta>
            <ta e="T256" id="Seg_3562" s="T255">v-v:tense-v:poss.pn</ta>
            <ta e="T257" id="Seg_3563" s="T256">v-v:cvb</ta>
            <ta e="T258" id="Seg_3564" s="T257">n-n:poss-n:case</ta>
            <ta e="T259" id="Seg_3565" s="T258">adj-n:poss-n:case</ta>
            <ta e="T260" id="Seg_3566" s="T259">v-v:tense-v:poss.pn</ta>
            <ta e="T261" id="Seg_3567" s="T260">adj</ta>
            <ta e="T262" id="Seg_3568" s="T261">n-n:case</ta>
            <ta e="T263" id="Seg_3569" s="T262">v-v:tense-v:pred.pn</ta>
            <ta e="T264" id="Seg_3570" s="T263">n-n:(poss)-n:case</ta>
            <ta e="T265" id="Seg_3571" s="T264">ptcl</ta>
            <ta e="T266" id="Seg_3572" s="T265">n-n:case</ta>
            <ta e="T267" id="Seg_3573" s="T266">ptcl</ta>
            <ta e="T268" id="Seg_3574" s="T267">v-v:tense-v:pred.pn</ta>
            <ta e="T269" id="Seg_3575" s="T268">adv</ta>
            <ta e="T270" id="Seg_3576" s="T269">adj</ta>
            <ta e="T271" id="Seg_3577" s="T270">n-n:case</ta>
            <ta e="T272" id="Seg_3578" s="T271">v-v:tense-v:pred.pn</ta>
            <ta e="T273" id="Seg_3579" s="T272">dempro-pro:case</ta>
            <ta e="T274" id="Seg_3580" s="T273">adj-n:(num)-n:case</ta>
            <ta e="T275" id="Seg_3581" s="T274">v-v:tense-v:pred.pn</ta>
            <ta e="T276" id="Seg_3582" s="T275">pers-pro:case</ta>
            <ta e="T277" id="Seg_3583" s="T276">v-v:cvb</ta>
            <ta e="T278" id="Seg_3584" s="T277">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T279" id="Seg_3585" s="T278">adv</ta>
            <ta e="T280" id="Seg_3586" s="T279">n-n:case</ta>
            <ta e="T281" id="Seg_3587" s="T280">n-n:poss-n:case</ta>
            <ta e="T282" id="Seg_3588" s="T281">v-v:cvb</ta>
            <ta e="T283" id="Seg_3589" s="T282">v-v:ptcp</ta>
            <ta e="T284" id="Seg_3590" s="T283">n-n:(num)-n:case</ta>
            <ta e="T285" id="Seg_3591" s="T284">dempro-pro:(num)-pro:(ins)-pro:(poss)-pro:case</ta>
            <ta e="T286" id="Seg_3592" s="T285">v-v:cvb</ta>
            <ta e="T287" id="Seg_3593" s="T286">n-n:case</ta>
            <ta e="T288" id="Seg_3594" s="T287">n-n:poss-n:case</ta>
            <ta e="T289" id="Seg_3595" s="T288">n-n&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T290" id="Seg_3596" s="T289">adv</ta>
            <ta e="T291" id="Seg_3597" s="T290">pers-pro:case</ta>
            <ta e="T292" id="Seg_3598" s="T291">n-n:case</ta>
            <ta e="T293" id="Seg_3599" s="T292">v-v:ptcp</ta>
            <ta e="T294" id="Seg_3600" s="T293">n-n:(poss)-n:case</ta>
            <ta e="T295" id="Seg_3601" s="T294">dempro-pro:case</ta>
            <ta e="T296" id="Seg_3602" s="T295">pers-pro:case</ta>
            <ta e="T297" id="Seg_3603" s="T296">n-n:(poss)-n:case</ta>
            <ta e="T298" id="Seg_3604" s="T297">n-n&gt;n-n:(poss)-n:case</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_3605" s="T0">n</ta>
            <ta e="T2" id="Seg_3606" s="T1">n</ta>
            <ta e="T3" id="Seg_3607" s="T2">adj</ta>
            <ta e="T4" id="Seg_3608" s="T3">ptcl</ta>
            <ta e="T5" id="Seg_3609" s="T4">n</ta>
            <ta e="T6" id="Seg_3610" s="T5">adj</ta>
            <ta e="T7" id="Seg_3611" s="T6">v</ta>
            <ta e="T8" id="Seg_3612" s="T7">n</ta>
            <ta e="T9" id="Seg_3613" s="T8">ptcl</ta>
            <ta e="T10" id="Seg_3614" s="T9">aux</ta>
            <ta e="T11" id="Seg_3615" s="T10">dempro</ta>
            <ta e="T12" id="Seg_3616" s="T11">n</ta>
            <ta e="T13" id="Seg_3617" s="T12">adv</ta>
            <ta e="T14" id="Seg_3618" s="T13">adj</ta>
            <ta e="T15" id="Seg_3619" s="T14">n</ta>
            <ta e="T16" id="Seg_3620" s="T15">v</ta>
            <ta e="T17" id="Seg_3621" s="T16">aux</ta>
            <ta e="T18" id="Seg_3622" s="T17">adv</ta>
            <ta e="T19" id="Seg_3623" s="T18">ptcl</ta>
            <ta e="T20" id="Seg_3624" s="T19">cop</ta>
            <ta e="T21" id="Seg_3625" s="T20">cardnum</ta>
            <ta e="T22" id="Seg_3626" s="T21">adj</ta>
            <ta e="T23" id="Seg_3627" s="T22">n</ta>
            <ta e="T24" id="Seg_3628" s="T23">n</ta>
            <ta e="T25" id="Seg_3629" s="T24">n</ta>
            <ta e="T26" id="Seg_3630" s="T25">cop</ta>
            <ta e="T27" id="Seg_3631" s="T26">aux</ta>
            <ta e="T28" id="Seg_3632" s="T27">n</ta>
            <ta e="T29" id="Seg_3633" s="T28">que</ta>
            <ta e="T30" id="Seg_3634" s="T29">n</ta>
            <ta e="T31" id="Seg_3635" s="T30">v</ta>
            <ta e="T32" id="Seg_3636" s="T31">cop</ta>
            <ta e="T33" id="Seg_3637" s="T32">v</ta>
            <ta e="T34" id="Seg_3638" s="T33">v</ta>
            <ta e="T35" id="Seg_3639" s="T34">v</ta>
            <ta e="T36" id="Seg_3640" s="T35">dempro</ta>
            <ta e="T37" id="Seg_3641" s="T36">dempro</ta>
            <ta e="T38" id="Seg_3642" s="T37">n</ta>
            <ta e="T39" id="Seg_3643" s="T38">n</ta>
            <ta e="T40" id="Seg_3644" s="T39">v</ta>
            <ta e="T41" id="Seg_3645" s="T40">n</ta>
            <ta e="T42" id="Seg_3646" s="T41">ptcl</ta>
            <ta e="T43" id="Seg_3647" s="T42">v</ta>
            <ta e="T44" id="Seg_3648" s="T43">v</ta>
            <ta e="T45" id="Seg_3649" s="T44">pers</ta>
            <ta e="T46" id="Seg_3650" s="T45">v</ta>
            <ta e="T47" id="Seg_3651" s="T46">v</ta>
            <ta e="T48" id="Seg_3652" s="T47">v</ta>
            <ta e="T49" id="Seg_3653" s="T48">pers</ta>
            <ta e="T50" id="Seg_3654" s="T49">v</ta>
            <ta e="T51" id="Seg_3655" s="T50">adv</ta>
            <ta e="T52" id="Seg_3656" s="T51">adv</ta>
            <ta e="T53" id="Seg_3657" s="T52">v</ta>
            <ta e="T54" id="Seg_3658" s="T53">pers</ta>
            <ta e="T55" id="Seg_3659" s="T54">ptcl</ta>
            <ta e="T56" id="Seg_3660" s="T55">n</ta>
            <ta e="T57" id="Seg_3661" s="T56">v</ta>
            <ta e="T58" id="Seg_3662" s="T57">dempro</ta>
            <ta e="T59" id="Seg_3663" s="T58">v</ta>
            <ta e="T60" id="Seg_3664" s="T59">aux</ta>
            <ta e="T61" id="Seg_3665" s="T60">dempro</ta>
            <ta e="T62" id="Seg_3666" s="T61">n</ta>
            <ta e="T63" id="Seg_3667" s="T62">adv</ta>
            <ta e="T64" id="Seg_3668" s="T63">n</ta>
            <ta e="T65" id="Seg_3669" s="T64">v</ta>
            <ta e="T66" id="Seg_3670" s="T65">dempro</ta>
            <ta e="T67" id="Seg_3671" s="T66">v</ta>
            <ta e="T68" id="Seg_3672" s="T67">n</ta>
            <ta e="T69" id="Seg_3673" s="T68">v</ta>
            <ta e="T70" id="Seg_3674" s="T69">dempro</ta>
            <ta e="T71" id="Seg_3675" s="T70">v</ta>
            <ta e="T72" id="Seg_3676" s="T71">adj</ta>
            <ta e="T73" id="Seg_3677" s="T72">adj</ta>
            <ta e="T74" id="Seg_3678" s="T73">n</ta>
            <ta e="T75" id="Seg_3679" s="T74">v</ta>
            <ta e="T76" id="Seg_3680" s="T75">adv</ta>
            <ta e="T77" id="Seg_3681" s="T76">v</ta>
            <ta e="T78" id="Seg_3682" s="T77">n</ta>
            <ta e="T79" id="Seg_3683" s="T78">v</ta>
            <ta e="T80" id="Seg_3684" s="T79">n</ta>
            <ta e="T81" id="Seg_3685" s="T80">adj</ta>
            <ta e="T82" id="Seg_3686" s="T81">n</ta>
            <ta e="T83" id="Seg_3687" s="T82">v</ta>
            <ta e="T84" id="Seg_3688" s="T83">v</ta>
            <ta e="T85" id="Seg_3689" s="T84">v</ta>
            <ta e="T86" id="Seg_3690" s="T85">aux</ta>
            <ta e="T87" id="Seg_3691" s="T86">ptcl</ta>
            <ta e="T88" id="Seg_3692" s="T87">n</ta>
            <ta e="T89" id="Seg_3693" s="T88">v</ta>
            <ta e="T90" id="Seg_3694" s="T89">v</ta>
            <ta e="T91" id="Seg_3695" s="T90">v</ta>
            <ta e="T92" id="Seg_3696" s="T91">dempro</ta>
            <ta e="T93" id="Seg_3697" s="T92">v</ta>
            <ta e="T94" id="Seg_3698" s="T93">post</ta>
            <ta e="T95" id="Seg_3699" s="T94">v</ta>
            <ta e="T96" id="Seg_3700" s="T95">v</ta>
            <ta e="T97" id="Seg_3701" s="T96">dempro</ta>
            <ta e="T98" id="Seg_3702" s="T97">v</ta>
            <ta e="T99" id="Seg_3703" s="T98">adv</ta>
            <ta e="T100" id="Seg_3704" s="T99">adv</ta>
            <ta e="T101" id="Seg_3705" s="T100">n</ta>
            <ta e="T102" id="Seg_3706" s="T101">n</ta>
            <ta e="T103" id="Seg_3707" s="T102">v</ta>
            <ta e="T104" id="Seg_3708" s="T103">aux</ta>
            <ta e="T105" id="Seg_3709" s="T104">adj</ta>
            <ta e="T106" id="Seg_3710" s="T105">n</ta>
            <ta e="T107" id="Seg_3711" s="T106">v</ta>
            <ta e="T108" id="Seg_3712" s="T107">conj</ta>
            <ta e="T109" id="Seg_3713" s="T108">v</ta>
            <ta e="T110" id="Seg_3714" s="T109">aux</ta>
            <ta e="T111" id="Seg_3715" s="T110">que</ta>
            <ta e="T112" id="Seg_3716" s="T111">pers</ta>
            <ta e="T113" id="Seg_3717" s="T112">adv</ta>
            <ta e="T114" id="Seg_3718" s="T113">v</ta>
            <ta e="T115" id="Seg_3719" s="T114">v</ta>
            <ta e="T116" id="Seg_3720" s="T115">v</ta>
            <ta e="T117" id="Seg_3721" s="T116">dempro</ta>
            <ta e="T118" id="Seg_3722" s="T117">n</ta>
            <ta e="T119" id="Seg_3723" s="T118">que</ta>
            <ta e="T120" id="Seg_3724" s="T119">v</ta>
            <ta e="T121" id="Seg_3725" s="T120">v</ta>
            <ta e="T122" id="Seg_3726" s="T121">n</ta>
            <ta e="T123" id="Seg_3727" s="T122">v</ta>
            <ta e="T124" id="Seg_3728" s="T123">v</ta>
            <ta e="T125" id="Seg_3729" s="T124">n</ta>
            <ta e="T126" id="Seg_3730" s="T125">v</ta>
            <ta e="T127" id="Seg_3731" s="T126">v</ta>
            <ta e="T128" id="Seg_3732" s="T127">dempro</ta>
            <ta e="T129" id="Seg_3733" s="T128">v</ta>
            <ta e="T130" id="Seg_3734" s="T129">n</ta>
            <ta e="T131" id="Seg_3735" s="T130">n</ta>
            <ta e="T132" id="Seg_3736" s="T131">v</ta>
            <ta e="T133" id="Seg_3737" s="T132">adv</ta>
            <ta e="T134" id="Seg_3738" s="T133">v</ta>
            <ta e="T135" id="Seg_3739" s="T134">v</ta>
            <ta e="T136" id="Seg_3740" s="T135">dempro</ta>
            <ta e="T137" id="Seg_3741" s="T136">n</ta>
            <ta e="T138" id="Seg_3742" s="T137">n</ta>
            <ta e="T139" id="Seg_3743" s="T138">adv</ta>
            <ta e="T140" id="Seg_3744" s="T139">v</ta>
            <ta e="T141" id="Seg_3745" s="T140">n</ta>
            <ta e="T142" id="Seg_3746" s="T141">n</ta>
            <ta e="T143" id="Seg_3747" s="T142">v</ta>
            <ta e="T144" id="Seg_3748" s="T143">n</ta>
            <ta e="T145" id="Seg_3749" s="T144">v</ta>
            <ta e="T146" id="Seg_3750" s="T145">v</ta>
            <ta e="T147" id="Seg_3751" s="T146">v</ta>
            <ta e="T148" id="Seg_3752" s="T147">n</ta>
            <ta e="T149" id="Seg_3753" s="T148">n</ta>
            <ta e="T150" id="Seg_3754" s="T149">adv</ta>
            <ta e="T151" id="Seg_3755" s="T150">adj</ta>
            <ta e="T152" id="Seg_3756" s="T151">adj</ta>
            <ta e="T153" id="Seg_3757" s="T152">adv</ta>
            <ta e="T154" id="Seg_3758" s="T153">v</ta>
            <ta e="T155" id="Seg_3759" s="T154">v</ta>
            <ta e="T156" id="Seg_3760" s="T155">dempro</ta>
            <ta e="T157" id="Seg_3761" s="T156">v</ta>
            <ta e="T158" id="Seg_3762" s="T157">dempro</ta>
            <ta e="T159" id="Seg_3763" s="T158">n</ta>
            <ta e="T160" id="Seg_3764" s="T159">n</ta>
            <ta e="T161" id="Seg_3765" s="T160">v</ta>
            <ta e="T162" id="Seg_3766" s="T161">v</ta>
            <ta e="T163" id="Seg_3767" s="T162">dempro</ta>
            <ta e="T164" id="Seg_3768" s="T163">v</ta>
            <ta e="T165" id="Seg_3769" s="T164">cardnum</ta>
            <ta e="T166" id="Seg_3770" s="T165">n</ta>
            <ta e="T167" id="Seg_3771" s="T166">ptcl</ta>
            <ta e="T168" id="Seg_3772" s="T167">cop</ta>
            <ta e="T169" id="Seg_3773" s="T168">dempro</ta>
            <ta e="T170" id="Seg_3774" s="T169">adv</ta>
            <ta e="T171" id="Seg_3775" s="T170">v</ta>
            <ta e="T172" id="Seg_3776" s="T171">interj</ta>
            <ta e="T173" id="Seg_3777" s="T172">que</ta>
            <ta e="T174" id="Seg_3778" s="T173">v</ta>
            <ta e="T175" id="Seg_3779" s="T174">v</ta>
            <ta e="T176" id="Seg_3780" s="T175">v</ta>
            <ta e="T177" id="Seg_3781" s="T176">dempro</ta>
            <ta e="T178" id="Seg_3782" s="T177">v</ta>
            <ta e="T179" id="Seg_3783" s="T178">adj</ta>
            <ta e="T180" id="Seg_3784" s="T179">n</ta>
            <ta e="T181" id="Seg_3785" s="T180">v</ta>
            <ta e="T182" id="Seg_3786" s="T181">dempro</ta>
            <ta e="T183" id="Seg_3787" s="T182">dempro</ta>
            <ta e="T184" id="Seg_3788" s="T183">n</ta>
            <ta e="T185" id="Seg_3789" s="T184">que</ta>
            <ta e="T186" id="Seg_3790" s="T185">ptcl</ta>
            <ta e="T187" id="Seg_3791" s="T186">adj</ta>
            <ta e="T188" id="Seg_3792" s="T187">ptcl</ta>
            <ta e="T189" id="Seg_3793" s="T188">v</ta>
            <ta e="T190" id="Seg_3794" s="T189">aux</ta>
            <ta e="T191" id="Seg_3795" s="T190">dempro</ta>
            <ta e="T192" id="Seg_3796" s="T191">v</ta>
            <ta e="T193" id="Seg_3797" s="T192">ptcl</ta>
            <ta e="T194" id="Seg_3798" s="T193">n</ta>
            <ta e="T195" id="Seg_3799" s="T194">v</ta>
            <ta e="T196" id="Seg_3800" s="T195">aux</ta>
            <ta e="T197" id="Seg_3801" s="T196">n</ta>
            <ta e="T198" id="Seg_3802" s="T197">v</ta>
            <ta e="T199" id="Seg_3803" s="T198">ptcl</ta>
            <ta e="T200" id="Seg_3804" s="T199">v</ta>
            <ta e="T201" id="Seg_3805" s="T200">v</ta>
            <ta e="T202" id="Seg_3806" s="T201">aux</ta>
            <ta e="T203" id="Seg_3807" s="T202">adv</ta>
            <ta e="T204" id="Seg_3808" s="T203">n</ta>
            <ta e="T205" id="Seg_3809" s="T204">v</ta>
            <ta e="T206" id="Seg_3810" s="T205">v</ta>
            <ta e="T207" id="Seg_3811" s="T206">ptcl</ta>
            <ta e="T208" id="Seg_3812" s="T207">v</ta>
            <ta e="T209" id="Seg_3813" s="T208">dempro</ta>
            <ta e="T210" id="Seg_3814" s="T209">v</ta>
            <ta e="T211" id="Seg_3815" s="T210">adj</ta>
            <ta e="T212" id="Seg_3816" s="T211">n</ta>
            <ta e="T213" id="Seg_3817" s="T212">v</ta>
            <ta e="T214" id="Seg_3818" s="T213">n</ta>
            <ta e="T215" id="Seg_3819" s="T214">v</ta>
            <ta e="T216" id="Seg_3820" s="T215">conj</ta>
            <ta e="T217" id="Seg_3821" s="T216">v</ta>
            <ta e="T218" id="Seg_3822" s="T217">aux</ta>
            <ta e="T219" id="Seg_3823" s="T218">dempro</ta>
            <ta e="T220" id="Seg_3824" s="T219">v</ta>
            <ta e="T221" id="Seg_3825" s="T220">v</ta>
            <ta e="T222" id="Seg_3826" s="T221">n</ta>
            <ta e="T223" id="Seg_3827" s="T222">v</ta>
            <ta e="T224" id="Seg_3828" s="T223">cardnum</ta>
            <ta e="T225" id="Seg_3829" s="T224">n</ta>
            <ta e="T226" id="Seg_3830" s="T225">v</ta>
            <ta e="T227" id="Seg_3831" s="T226">aux</ta>
            <ta e="T228" id="Seg_3832" s="T227">dempro</ta>
            <ta e="T229" id="Seg_3833" s="T228">v</ta>
            <ta e="T230" id="Seg_3834" s="T229">aux</ta>
            <ta e="T231" id="Seg_3835" s="T230">v</ta>
            <ta e="T232" id="Seg_3836" s="T231">dempro</ta>
            <ta e="T233" id="Seg_3837" s="T232">n</ta>
            <ta e="T234" id="Seg_3838" s="T233">v</ta>
            <ta e="T235" id="Seg_3839" s="T234">adj</ta>
            <ta e="T236" id="Seg_3840" s="T235">adj</ta>
            <ta e="T237" id="Seg_3841" s="T236">n</ta>
            <ta e="T238" id="Seg_3842" s="T237">n</ta>
            <ta e="T239" id="Seg_3843" s="T238">adj</ta>
            <ta e="T240" id="Seg_3844" s="T239">n</ta>
            <ta e="T241" id="Seg_3845" s="T240">v</ta>
            <ta e="T242" id="Seg_3846" s="T241">cardnum</ta>
            <ta e="T243" id="Seg_3847" s="T242">n</ta>
            <ta e="T244" id="Seg_3848" s="T243">v</ta>
            <ta e="T245" id="Seg_3849" s="T244">v</ta>
            <ta e="T246" id="Seg_3850" s="T245">v</ta>
            <ta e="T247" id="Seg_3851" s="T246">aux</ta>
            <ta e="T248" id="Seg_3852" s="T247">v</ta>
            <ta e="T249" id="Seg_3853" s="T248">v</ta>
            <ta e="T250" id="Seg_3854" s="T249">aux</ta>
            <ta e="T251" id="Seg_3855" s="T250">dempro</ta>
            <ta e="T252" id="Seg_3856" s="T251">n</ta>
            <ta e="T253" id="Seg_3857" s="T252">n</ta>
            <ta e="T254" id="Seg_3858" s="T253">v</ta>
            <ta e="T255" id="Seg_3859" s="T254">n</ta>
            <ta e="T256" id="Seg_3860" s="T255">v</ta>
            <ta e="T257" id="Seg_3861" s="T256">v</ta>
            <ta e="T258" id="Seg_3862" s="T257">n</ta>
            <ta e="T259" id="Seg_3863" s="T258">n</ta>
            <ta e="T260" id="Seg_3864" s="T259">v</ta>
            <ta e="T261" id="Seg_3865" s="T260">adj</ta>
            <ta e="T262" id="Seg_3866" s="T261">n</ta>
            <ta e="T263" id="Seg_3867" s="T262">cop</ta>
            <ta e="T264" id="Seg_3868" s="T263">n</ta>
            <ta e="T265" id="Seg_3869" s="T264">ptcl</ta>
            <ta e="T266" id="Seg_3870" s="T265">n</ta>
            <ta e="T267" id="Seg_3871" s="T266">ptcl</ta>
            <ta e="T268" id="Seg_3872" s="T267">v</ta>
            <ta e="T269" id="Seg_3873" s="T268">adv</ta>
            <ta e="T270" id="Seg_3874" s="T269">adj</ta>
            <ta e="T271" id="Seg_3875" s="T270">n</ta>
            <ta e="T272" id="Seg_3876" s="T271">cop</ta>
            <ta e="T273" id="Seg_3877" s="T272">dempro</ta>
            <ta e="T274" id="Seg_3878" s="T273">n</ta>
            <ta e="T275" id="Seg_3879" s="T274">v</ta>
            <ta e="T276" id="Seg_3880" s="T275">pers</ta>
            <ta e="T277" id="Seg_3881" s="T276">v</ta>
            <ta e="T278" id="Seg_3882" s="T277">aux</ta>
            <ta e="T279" id="Seg_3883" s="T278">adv</ta>
            <ta e="T280" id="Seg_3884" s="T279">n</ta>
            <ta e="T281" id="Seg_3885" s="T280">n</ta>
            <ta e="T282" id="Seg_3886" s="T281">v</ta>
            <ta e="T283" id="Seg_3887" s="T282">aux</ta>
            <ta e="T284" id="Seg_3888" s="T283">n</ta>
            <ta e="T285" id="Seg_3889" s="T284">dempro</ta>
            <ta e="T286" id="Seg_3890" s="T285">v</ta>
            <ta e="T287" id="Seg_3891" s="T286">n</ta>
            <ta e="T288" id="Seg_3892" s="T287">n</ta>
            <ta e="T289" id="Seg_3893" s="T288">v</ta>
            <ta e="T290" id="Seg_3894" s="T289">adv</ta>
            <ta e="T291" id="Seg_3895" s="T290">pers</ta>
            <ta e="T292" id="Seg_3896" s="T291">n</ta>
            <ta e="T293" id="Seg_3897" s="T292">v</ta>
            <ta e="T294" id="Seg_3898" s="T293">n</ta>
            <ta e="T295" id="Seg_3899" s="T294">dempro</ta>
            <ta e="T296" id="Seg_3900" s="T295">pers</ta>
            <ta e="T297" id="Seg_3901" s="T296">n</ta>
            <ta e="T298" id="Seg_3902" s="T297">n</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_3903" s="T1">np:L</ta>
            <ta e="T5" id="Seg_3904" s="T4">np:L</ta>
            <ta e="T8" id="Seg_3905" s="T7">np:Th</ta>
            <ta e="T12" id="Seg_3906" s="T11">np:L</ta>
            <ta e="T13" id="Seg_3907" s="T12">adv:Time</ta>
            <ta e="T15" id="Seg_3908" s="T14">np:Th</ta>
            <ta e="T18" id="Seg_3909" s="T17">adv:L</ta>
            <ta e="T23" id="Seg_3910" s="T22">np.h:Th</ta>
            <ta e="T24" id="Seg_3911" s="T23">np:L</ta>
            <ta e="T27" id="Seg_3912" s="T26">0.3.h:Th</ta>
            <ta e="T30" id="Seg_3913" s="T29">np:Th</ta>
            <ta e="T34" id="Seg_3914" s="T33">0.3.h:E</ta>
            <ta e="T35" id="Seg_3915" s="T34">0.3.h:A</ta>
            <ta e="T39" id="Seg_3916" s="T38">np.h:A</ta>
            <ta e="T41" id="Seg_3917" s="T40">np.h:R</ta>
            <ta e="T43" id="Seg_3918" s="T42">0.1.h:A</ta>
            <ta e="T44" id="Seg_3919" s="T43">0.3.h:A</ta>
            <ta e="T45" id="Seg_3920" s="T44">pro.h:A</ta>
            <ta e="T46" id="Seg_3921" s="T45">0.1.h:A</ta>
            <ta e="T47" id="Seg_3922" s="T46">0.1.h:A</ta>
            <ta e="T49" id="Seg_3923" s="T48">pro.h:Th</ta>
            <ta e="T53" id="Seg_3924" s="T52">0.1.h:A</ta>
            <ta e="T54" id="Seg_3925" s="T53">pro.h:Th</ta>
            <ta e="T56" id="Seg_3926" s="T55">np:Th</ta>
            <ta e="T57" id="Seg_3927" s="T56">0.2.h:A</ta>
            <ta e="T60" id="Seg_3928" s="T59">0.3.h:A</ta>
            <ta e="T62" id="Seg_3929" s="T61">np.h:A</ta>
            <ta e="T63" id="Seg_3930" s="T62">adv:Time</ta>
            <ta e="T67" id="Seg_3931" s="T66">0.3.h:A</ta>
            <ta e="T68" id="Seg_3932" s="T67">np:G</ta>
            <ta e="T69" id="Seg_3933" s="T68">0.3.h:A</ta>
            <ta e="T71" id="Seg_3934" s="T70">0.3.h:A</ta>
            <ta e="T74" id="Seg_3935" s="T73">np.h:Th</ta>
            <ta e="T77" id="Seg_3936" s="T76">0.3.h:E</ta>
            <ta e="T78" id="Seg_3937" s="T77">np.h:A</ta>
            <ta e="T80" id="Seg_3938" s="T79">np:G</ta>
            <ta e="T82" id="Seg_3939" s="T81">np.h:A</ta>
            <ta e="T83" id="Seg_3940" s="T82">0.3.h:A</ta>
            <ta e="T88" id="Seg_3941" s="T87">np:Th</ta>
            <ta e="T89" id="Seg_3942" s="T88">0.3.h:A</ta>
            <ta e="T90" id="Seg_3943" s="T89">0.2.h:A</ta>
            <ta e="T91" id="Seg_3944" s="T90">0.3.h:A</ta>
            <ta e="T93" id="Seg_3945" s="T92">0.3.h:A</ta>
            <ta e="T96" id="Seg_3946" s="T95">0.3.h:A</ta>
            <ta e="T98" id="Seg_3947" s="T97">0.3.h:Th</ta>
            <ta e="T99" id="Seg_3948" s="T98">adv:Time</ta>
            <ta e="T101" id="Seg_3949" s="T100">np.h:A</ta>
            <ta e="T102" id="Seg_3950" s="T101">np:Ins</ta>
            <ta e="T106" id="Seg_3951" s="T105">np.h:A</ta>
            <ta e="T110" id="Seg_3952" s="T109">0.3.h:E</ta>
            <ta e="T112" id="Seg_3953" s="T111">pro.h:A</ta>
            <ta e="T114" id="Seg_3954" s="T113">0.1.h:Th</ta>
            <ta e="T116" id="Seg_3955" s="T115">0.3.h:E</ta>
            <ta e="T118" id="Seg_3956" s="T117">np.h:A</ta>
            <ta e="T121" id="Seg_3957" s="T120">0.1.h:A</ta>
            <ta e="T122" id="Seg_3958" s="T121">np:Th</ta>
            <ta e="T123" id="Seg_3959" s="T122">0.1.h:A</ta>
            <ta e="T124" id="Seg_3960" s="T123">0.3.h:E</ta>
            <ta e="T125" id="Seg_3961" s="T124">0.3.h:Poss np:Th</ta>
            <ta e="T127" id="Seg_3962" s="T126">0.3.h:A</ta>
            <ta e="T129" id="Seg_3963" s="T128">0.3.h:A</ta>
            <ta e="T131" id="Seg_3964" s="T130">np:G</ta>
            <ta e="T132" id="Seg_3965" s="T131">0.3.h:Th</ta>
            <ta e="T133" id="Seg_3966" s="T132">adv:G</ta>
            <ta e="T134" id="Seg_3967" s="T133">0.3.h:A</ta>
            <ta e="T135" id="Seg_3968" s="T134">0.3.h:Th</ta>
            <ta e="T138" id="Seg_3969" s="T137">np:Th</ta>
            <ta e="T146" id="Seg_3970" s="T145">0.3.h:A</ta>
            <ta e="T147" id="Seg_3971" s="T146">0.3.h:E</ta>
            <ta e="T148" id="Seg_3972" s="T147">np:Path</ta>
            <ta e="T149" id="Seg_3973" s="T148">np.h:Th</ta>
            <ta e="T154" id="Seg_3974" s="T153">0.3.h:A</ta>
            <ta e="T155" id="Seg_3975" s="T154">0.3.h:A</ta>
            <ta e="T157" id="Seg_3976" s="T156">0.3.h:A</ta>
            <ta e="T159" id="Seg_3977" s="T158">np:G</ta>
            <ta e="T160" id="Seg_3978" s="T159">np:G</ta>
            <ta e="T161" id="Seg_3979" s="T160">0.3.h:A</ta>
            <ta e="T162" id="Seg_3980" s="T161">0.3.h:A</ta>
            <ta e="T164" id="Seg_3981" s="T163">0.3.h:A</ta>
            <ta e="T166" id="Seg_3982" s="T165">np.h:Th</ta>
            <ta e="T169" id="Seg_3983" s="T168">pro.h:A</ta>
            <ta e="T175" id="Seg_3984" s="T174">0.2.h:A</ta>
            <ta e="T176" id="Seg_3985" s="T175">0.3.h:A</ta>
            <ta e="T178" id="Seg_3986" s="T177">0.3.h:A</ta>
            <ta e="T181" id="Seg_3987" s="T180">0.3.h:A</ta>
            <ta e="T184" id="Seg_3988" s="T183">np.h:A</ta>
            <ta e="T185" id="Seg_3989" s="T184">pro:Th</ta>
            <ta e="T191" id="Seg_3990" s="T190">pro:Th</ta>
            <ta e="T192" id="Seg_3991" s="T191">0.3.h:A</ta>
            <ta e="T194" id="Seg_3992" s="T193">0.3.h:Poss np:G</ta>
            <ta e="T196" id="Seg_3993" s="T195">0.3.h:A</ta>
            <ta e="T197" id="Seg_3994" s="T196">np.h:A</ta>
            <ta e="T200" id="Seg_3995" s="T199">0.2.h:A</ta>
            <ta e="T202" id="Seg_3996" s="T201">0.2.h:A</ta>
            <ta e="T203" id="Seg_3997" s="T202">adv:L</ta>
            <ta e="T204" id="Seg_3998" s="T203">0.1.h:Poss np.h:E</ta>
            <ta e="T206" id="Seg_3999" s="T205">0.3.h:A</ta>
            <ta e="T208" id="Seg_4000" s="T207">0.3.h:A</ta>
            <ta e="T209" id="Seg_4001" s="T208">pro:St</ta>
            <ta e="T210" id="Seg_4002" s="T209">0.3.h:E</ta>
            <ta e="T212" id="Seg_4003" s="T211">np.h:A</ta>
            <ta e="T214" id="Seg_4004" s="T213">np:G</ta>
            <ta e="T215" id="Seg_4005" s="T214">0.3.h:A</ta>
            <ta e="T218" id="Seg_4006" s="T217">0.3.h:A</ta>
            <ta e="T221" id="Seg_4007" s="T220">0.3.h:A</ta>
            <ta e="T222" id="Seg_4008" s="T221">np:Th</ta>
            <ta e="T223" id="Seg_4009" s="T222">0.3.h:A</ta>
            <ta e="T225" id="Seg_4010" s="T224">np.h:A</ta>
            <ta e="T228" id="Seg_4011" s="T227">pro.h:A</ta>
            <ta e="T231" id="Seg_4012" s="T230">0.3.h:E</ta>
            <ta e="T233" id="Seg_4013" s="T232">np.h:A</ta>
            <ta e="T237" id="Seg_4014" s="T236">np.h:A</ta>
            <ta e="T238" id="Seg_4015" s="T237">np:Poss</ta>
            <ta e="T240" id="Seg_4016" s="T239">np:Th</ta>
            <ta e="T241" id="Seg_4017" s="T240">0.3.h:A</ta>
            <ta e="T243" id="Seg_4018" s="T242">np:Th</ta>
            <ta e="T245" id="Seg_4019" s="T244">0.2.h:Th</ta>
            <ta e="T247" id="Seg_4020" s="T246">0.1.h:A</ta>
            <ta e="T250" id="Seg_4021" s="T249">0.3.h:A</ta>
            <ta e="T252" id="Seg_4022" s="T251">np.h:A</ta>
            <ta e="T253" id="Seg_4023" s="T252">np:G</ta>
            <ta e="T254" id="Seg_4024" s="T253">0.3.h:Th</ta>
            <ta e="T255" id="Seg_4025" s="T254">np:Th</ta>
            <ta e="T256" id="Seg_4026" s="T255">0.1.h:A</ta>
            <ta e="T257" id="Seg_4027" s="T256">0.3.h:A</ta>
            <ta e="T258" id="Seg_4028" s="T257">0.3.h:Poss np:So</ta>
            <ta e="T259" id="Seg_4029" s="T258">np:Th</ta>
            <ta e="T263" id="Seg_4030" s="T262">0.3:Th</ta>
            <ta e="T264" id="Seg_4031" s="T263">np.h:A</ta>
            <ta e="T266" id="Seg_4032" s="T265">np:Th</ta>
            <ta e="T272" id="Seg_4033" s="T271">0.3.h:Th</ta>
            <ta e="T274" id="Seg_4034" s="T273">np.h:A</ta>
            <ta e="T276" id="Seg_4035" s="T275">pro.h:Th</ta>
            <ta e="T279" id="Seg_4036" s="T278">adv:Time</ta>
            <ta e="T281" id="Seg_4037" s="T280">np:So</ta>
            <ta e="T284" id="Seg_4038" s="T283">np:G</ta>
            <ta e="T285" id="Seg_4039" s="T284">pro.h:A</ta>
            <ta e="T286" id="Seg_4040" s="T285">0.3.h:A</ta>
            <ta e="T288" id="Seg_4041" s="T287">np:L</ta>
            <ta e="T291" id="Seg_4042" s="T290">pro.h:E</ta>
            <ta e="T292" id="Seg_4043" s="T291">np:L</ta>
            <ta e="T294" id="Seg_4044" s="T293">np.h:St</ta>
            <ta e="T295" id="Seg_4045" s="T294">pro.h:Th</ta>
            <ta e="T296" id="Seg_4046" s="T295">pro.h:Poss</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T8" id="Seg_4047" s="T7">np:S</ta>
            <ta e="T9" id="Seg_4048" s="T8">ptcl:pred</ta>
            <ta e="T15" id="Seg_4049" s="T14">np:S</ta>
            <ta e="T17" id="Seg_4050" s="T16">v:pred</ta>
            <ta e="T19" id="Seg_4051" s="T18">ptcl:pred</ta>
            <ta e="T23" id="Seg_4052" s="T22">np.h:S</ta>
            <ta e="T25" id="Seg_4053" s="T24">n:pred</ta>
            <ta e="T27" id="Seg_4054" s="T26">0.3.h:S v:pred</ta>
            <ta e="T30" id="Seg_4055" s="T29">np:S</ta>
            <ta e="T32" id="Seg_4056" s="T31">v:pred</ta>
            <ta e="T33" id="Seg_4057" s="T32">s:adv</ta>
            <ta e="T34" id="Seg_4058" s="T33">s:adv</ta>
            <ta e="T35" id="Seg_4059" s="T34">0.3.h:S v:pred</ta>
            <ta e="T39" id="Seg_4060" s="T38">np.h:S</ta>
            <ta e="T40" id="Seg_4061" s="T39">v:pred</ta>
            <ta e="T43" id="Seg_4062" s="T42">0.1.h:S v:pred</ta>
            <ta e="T44" id="Seg_4063" s="T43">0.3.h:S v:pred</ta>
            <ta e="T45" id="Seg_4064" s="T44">pro.h:S</ta>
            <ta e="T46" id="Seg_4065" s="T45">s:adv</ta>
            <ta e="T47" id="Seg_4066" s="T46">s:adv</ta>
            <ta e="T48" id="Seg_4067" s="T47">v:pred</ta>
            <ta e="T50" id="Seg_4068" s="T48">s:cond</ta>
            <ta e="T53" id="Seg_4069" s="T52">0.1.h:S v:pred</ta>
            <ta e="T56" id="Seg_4070" s="T55">np:O</ta>
            <ta e="T57" id="Seg_4071" s="T56">0.2.h:S v:pred</ta>
            <ta e="T60" id="Seg_4072" s="T59">0.3.h:S v:pred</ta>
            <ta e="T62" id="Seg_4073" s="T61">np.h:S</ta>
            <ta e="T65" id="Seg_4074" s="T64">v:pred</ta>
            <ta e="T67" id="Seg_4075" s="T65">s:adv</ta>
            <ta e="T69" id="Seg_4076" s="T68">0.3.h:S v:pred</ta>
            <ta e="T71" id="Seg_4077" s="T70">0.3.h:S v:pred</ta>
            <ta e="T74" id="Seg_4078" s="T73">np.h:S</ta>
            <ta e="T75" id="Seg_4079" s="T74">v:pred</ta>
            <ta e="T77" id="Seg_4080" s="T76">s:adv</ta>
            <ta e="T78" id="Seg_4081" s="T77">np.h:S</ta>
            <ta e="T79" id="Seg_4082" s="T78">v:pred</ta>
            <ta e="T82" id="Seg_4083" s="T81">np.h:S</ta>
            <ta e="T83" id="Seg_4084" s="T82">s:adv</ta>
            <ta e="T86" id="Seg_4085" s="T85">v:pred</ta>
            <ta e="T89" id="Seg_4086" s="T86">s:adv</ta>
            <ta e="T90" id="Seg_4087" s="T89">0.2.h:S v:pred</ta>
            <ta e="T91" id="Seg_4088" s="T90">0.3.h:S v:pred</ta>
            <ta e="T94" id="Seg_4089" s="T91">s:temp</ta>
            <ta e="T96" id="Seg_4090" s="T95">0.3.h:S v:pred</ta>
            <ta e="T98" id="Seg_4091" s="T96">s:temp</ta>
            <ta e="T101" id="Seg_4092" s="T100">np.h:S</ta>
            <ta e="T104" id="Seg_4093" s="T103">v:pred</ta>
            <ta e="T106" id="Seg_4094" s="T105">np.h:S</ta>
            <ta e="T107" id="Seg_4095" s="T106">v:pred</ta>
            <ta e="T110" id="Seg_4096" s="T109">0.3.h:S v:pred</ta>
            <ta e="T112" id="Seg_4097" s="T111">pro.h:S</ta>
            <ta e="T114" id="Seg_4098" s="T112">s:comp</ta>
            <ta e="T115" id="Seg_4099" s="T114">v:pred</ta>
            <ta e="T116" id="Seg_4100" s="T115">0.3.h:S v:pred</ta>
            <ta e="T121" id="Seg_4101" s="T116">s:adv</ta>
            <ta e="T122" id="Seg_4102" s="T121">np:O</ta>
            <ta e="T123" id="Seg_4103" s="T122">0.1.h:S v:pred</ta>
            <ta e="T124" id="Seg_4104" s="T123">s:adv</ta>
            <ta e="T127" id="Seg_4105" s="T124">s:adv</ta>
            <ta e="T129" id="Seg_4106" s="T127">s:adv</ta>
            <ta e="T132" id="Seg_4107" s="T131">0.3.h:S v:pred</ta>
            <ta e="T134" id="Seg_4108" s="T133">s:adv</ta>
            <ta e="T135" id="Seg_4109" s="T134">0.3.h:S v:pred</ta>
            <ta e="T138" id="Seg_4110" s="T137">np:S</ta>
            <ta e="T140" id="Seg_4111" s="T139">adj:pred</ta>
            <ta e="T146" id="Seg_4112" s="T145">s:adv</ta>
            <ta e="T147" id="Seg_4113" s="T146">0.3.h:S v:pred</ta>
            <ta e="T149" id="Seg_4114" s="T148">np.h:S</ta>
            <ta e="T151" id="Seg_4115" s="T150">adj:pred</ta>
            <ta e="T152" id="Seg_4116" s="T151">adj:pred</ta>
            <ta e="T154" id="Seg_4117" s="T153">s:adv</ta>
            <ta e="T155" id="Seg_4118" s="T154">0.3.h:S v:pred</ta>
            <ta e="T159" id="Seg_4119" s="T155">s:adv</ta>
            <ta e="T161" id="Seg_4120" s="T159">s:adv</ta>
            <ta e="T162" id="Seg_4121" s="T161">0.3.h:S v:pred</ta>
            <ta e="T164" id="Seg_4122" s="T163">0.3.h:S v:pred</ta>
            <ta e="T166" id="Seg_4123" s="T165">np.h:S</ta>
            <ta e="T167" id="Seg_4124" s="T166">ptcl:pred</ta>
            <ta e="T169" id="Seg_4125" s="T168">pro.h:S</ta>
            <ta e="T171" id="Seg_4126" s="T170">v:pred</ta>
            <ta e="T174" id="Seg_4127" s="T172">s:adv</ta>
            <ta e="T175" id="Seg_4128" s="T174">0.2.h:S v:pred</ta>
            <ta e="T176" id="Seg_4129" s="T175">0.3.h:S v:pred</ta>
            <ta e="T178" id="Seg_4130" s="T177">0.3.h:S v:pred</ta>
            <ta e="T181" id="Seg_4131" s="T180">0.3.h:S v:pred</ta>
            <ta e="T184" id="Seg_4132" s="T183">np.h:S</ta>
            <ta e="T185" id="Seg_4133" s="T184">pro:O</ta>
            <ta e="T190" id="Seg_4134" s="T189">v:pred</ta>
            <ta e="T191" id="Seg_4135" s="T190">pro:O</ta>
            <ta e="T192" id="Seg_4136" s="T191">s:adv</ta>
            <ta e="T196" id="Seg_4137" s="T195">0.3.h:S v:pred</ta>
            <ta e="T197" id="Seg_4138" s="T196">np.h:S</ta>
            <ta e="T198" id="Seg_4139" s="T197">v:pred</ta>
            <ta e="T200" id="Seg_4140" s="T199">0.2.h:S v:pred</ta>
            <ta e="T201" id="Seg_4141" s="T200">s:adv</ta>
            <ta e="T202" id="Seg_4142" s="T201">0.2.h:S v:pred</ta>
            <ta e="T205" id="Seg_4143" s="T202">s:cond</ta>
            <ta e="T206" id="Seg_4144" s="T205">0.3.h:S v:pred</ta>
            <ta e="T208" id="Seg_4145" s="T207">0.3.h:S v:pred</ta>
            <ta e="T210" id="Seg_4146" s="T208">s:temp</ta>
            <ta e="T212" id="Seg_4147" s="T211">np.h:S</ta>
            <ta e="T213" id="Seg_4148" s="T212">v:pred</ta>
            <ta e="T215" id="Seg_4149" s="T214">0.3.h:S v:pred</ta>
            <ta e="T218" id="Seg_4150" s="T217">0.3.h:S v:pred</ta>
            <ta e="T221" id="Seg_4151" s="T218">s:adv</ta>
            <ta e="T222" id="Seg_4152" s="T221">np:O</ta>
            <ta e="T223" id="Seg_4153" s="T222">0.3.h:S v:pred</ta>
            <ta e="T225" id="Seg_4154" s="T224">np.h:S</ta>
            <ta e="T227" id="Seg_4155" s="T226">v:pred</ta>
            <ta e="T231" id="Seg_4156" s="T227">s:temp</ta>
            <ta e="T233" id="Seg_4157" s="T232">np.h:S</ta>
            <ta e="T234" id="Seg_4158" s="T233">v:pred</ta>
            <ta e="T237" id="Seg_4159" s="T236">np.h:S</ta>
            <ta e="T241" id="Seg_4160" s="T237">s:adv</ta>
            <ta e="T243" id="Seg_4161" s="T242">np:O</ta>
            <ta e="T244" id="Seg_4162" s="T243">v:pred</ta>
            <ta e="T245" id="Seg_4163" s="T244">s:temp</ta>
            <ta e="T247" id="Seg_4164" s="T246">0.1.h:S v:pred</ta>
            <ta e="T248" id="Seg_4165" s="T247">s:adv</ta>
            <ta e="T250" id="Seg_4166" s="T249">0.3.h:S v:pred</ta>
            <ta e="T252" id="Seg_4167" s="T251">np.h:S</ta>
            <ta e="T254" id="Seg_4168" s="T252">s:adv</ta>
            <ta e="T257" id="Seg_4169" s="T254">s:adv</ta>
            <ta e="T259" id="Seg_4170" s="T258">np:O</ta>
            <ta e="T260" id="Seg_4171" s="T259">v:pred</ta>
            <ta e="T262" id="Seg_4172" s="T261">n:pred</ta>
            <ta e="T263" id="Seg_4173" s="T262">0.3:S cop</ta>
            <ta e="T264" id="Seg_4174" s="T263">np.h:S</ta>
            <ta e="T266" id="Seg_4175" s="T265">np:O</ta>
            <ta e="T268" id="Seg_4176" s="T267">v:pred</ta>
            <ta e="T271" id="Seg_4177" s="T270">n:pred</ta>
            <ta e="T272" id="Seg_4178" s="T271">0.3.h:S cop</ta>
            <ta e="T274" id="Seg_4179" s="T273">np.h:S</ta>
            <ta e="T275" id="Seg_4180" s="T274">v:pred</ta>
            <ta e="T276" id="Seg_4181" s="T275">pro.h:S</ta>
            <ta e="T278" id="Seg_4182" s="T277">v:pred</ta>
            <ta e="T283" id="Seg_4183" s="T278">s:rel</ta>
            <ta e="T285" id="Seg_4184" s="T284">pro.h:S</ta>
            <ta e="T286" id="Seg_4185" s="T285">s:adv</ta>
            <ta e="T289" id="Seg_4186" s="T288">v:pred</ta>
            <ta e="T293" id="Seg_4187" s="T289">s:rel</ta>
            <ta e="T295" id="Seg_4188" s="T294">pro.h:S</ta>
            <ta e="T297" id="Seg_4189" s="T296">n:pred</ta>
            <ta e="T298" id="Seg_4190" s="T297">n:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST">
            <ta e="T1" id="Seg_4191" s="T0">accs-gen</ta>
            <ta e="T2" id="Seg_4192" s="T1">accs-inf</ta>
            <ta e="T5" id="Seg_4193" s="T4">accs-inf</ta>
            <ta e="T8" id="Seg_4194" s="T7">new</ta>
            <ta e="T12" id="Seg_4195" s="T11">giv-active</ta>
            <ta e="T15" id="Seg_4196" s="T14">new</ta>
            <ta e="T23" id="Seg_4197" s="T22">new</ta>
            <ta e="T24" id="Seg_4198" s="T23">new</ta>
            <ta e="T27" id="Seg_4199" s="T26">0.giv-active</ta>
            <ta e="T28" id="Seg_4200" s="T27">accs-gen</ta>
            <ta e="T30" id="Seg_4201" s="T29">0.giv-inactive-Q</ta>
            <ta e="T33" id="Seg_4202" s="T32">0.quot-sp</ta>
            <ta e="T35" id="Seg_4203" s="T34">0.giv-active</ta>
            <ta e="T39" id="Seg_4204" s="T38">giv-inactive</ta>
            <ta e="T40" id="Seg_4205" s="T39">quot-sp</ta>
            <ta e="T41" id="Seg_4206" s="T40">giv-inactive</ta>
            <ta e="T43" id="Seg_4207" s="T42">0.accs-aggr-Q</ta>
            <ta e="T44" id="Seg_4208" s="T43">0.giv-active 0.quot-sp</ta>
            <ta e="T45" id="Seg_4209" s="T44">giv-active-Q</ta>
            <ta e="T49" id="Seg_4210" s="T48">giv-active-Q</ta>
            <ta e="T53" id="Seg_4211" s="T52">0.giv-active-Q</ta>
            <ta e="T54" id="Seg_4212" s="T53">giv-inactive-Q</ta>
            <ta e="T56" id="Seg_4213" s="T55">new-Q</ta>
            <ta e="T57" id="Seg_4214" s="T56">0.giv-active-Q</ta>
            <ta e="T60" id="Seg_4215" s="T59">0.giv-active</ta>
            <ta e="T62" id="Seg_4216" s="T61">giv-active</ta>
            <ta e="T64" id="Seg_4217" s="T63">new</ta>
            <ta e="T67" id="Seg_4218" s="T66">0.giv-active</ta>
            <ta e="T68" id="Seg_4219" s="T67">giv-inactive</ta>
            <ta e="T69" id="Seg_4220" s="T68">0.giv-active</ta>
            <ta e="T71" id="Seg_4221" s="T70">0.giv-active</ta>
            <ta e="T74" id="Seg_4222" s="T73">new</ta>
            <ta e="T77" id="Seg_4223" s="T76">0.giv-active</ta>
            <ta e="T78" id="Seg_4224" s="T77">giv-active</ta>
            <ta e="T80" id="Seg_4225" s="T79">new</ta>
            <ta e="T82" id="Seg_4226" s="T81">giv-inactive</ta>
            <ta e="T83" id="Seg_4227" s="T82">0.giv-active</ta>
            <ta e="T88" id="Seg_4228" s="T87">accs-sit</ta>
            <ta e="T89" id="Seg_4229" s="T88">0.giv-active</ta>
            <ta e="T90" id="Seg_4230" s="T89">0.giv-inactive-Q</ta>
            <ta e="T91" id="Seg_4231" s="T90">0.giv-active 0.quot-sp</ta>
            <ta e="T93" id="Seg_4232" s="T92">0.accs-aggr</ta>
            <ta e="T96" id="Seg_4233" s="T95">0.giv-active</ta>
            <ta e="T98" id="Seg_4234" s="T97">0.giv-active</ta>
            <ta e="T101" id="Seg_4235" s="T100">giv-active</ta>
            <ta e="T102" id="Seg_4236" s="T101">new</ta>
            <ta e="T106" id="Seg_4237" s="T105">giv-active</ta>
            <ta e="T110" id="Seg_4238" s="T109">0.giv-active 0.quot-th</ta>
            <ta e="T112" id="Seg_4239" s="T111">giv-active-Q</ta>
            <ta e="T116" id="Seg_4240" s="T115">0.giv-active 0.quot-th</ta>
            <ta e="T118" id="Seg_4241" s="T117">giv-inactive-Q</ta>
            <ta e="T121" id="Seg_4242" s="T120">0.giv-active-Q</ta>
            <ta e="T122" id="Seg_4243" s="T121">new-Q</ta>
            <ta e="T123" id="Seg_4244" s="T122">0.giv-active-Q</ta>
            <ta e="T124" id="Seg_4245" s="T123">0.giv-active 0.quot-th</ta>
            <ta e="T125" id="Seg_4246" s="T124">accs-inf</ta>
            <ta e="T127" id="Seg_4247" s="T126">0.giv-active</ta>
            <ta e="T129" id="Seg_4248" s="T128">0.giv-active</ta>
            <ta e="T131" id="Seg_4249" s="T130">new</ta>
            <ta e="T132" id="Seg_4250" s="T131">0.giv-active</ta>
            <ta e="T134" id="Seg_4251" s="T133">0.giv-active</ta>
            <ta e="T135" id="Seg_4252" s="T134">0.giv-active</ta>
            <ta e="T138" id="Seg_4253" s="T137">giv-active</ta>
            <ta e="T141" id="Seg_4254" s="T140">new</ta>
            <ta e="T142" id="Seg_4255" s="T141">new</ta>
            <ta e="T144" id="Seg_4256" s="T143">new</ta>
            <ta e="T146" id="Seg_4257" s="T145">0.giv-active</ta>
            <ta e="T147" id="Seg_4258" s="T146">0.giv-active</ta>
            <ta e="T148" id="Seg_4259" s="T147">new</ta>
            <ta e="T149" id="Seg_4260" s="T148">giv-active</ta>
            <ta e="T154" id="Seg_4261" s="T153">0.giv-active</ta>
            <ta e="T155" id="Seg_4262" s="T154">0.giv-active</ta>
            <ta e="T157" id="Seg_4263" s="T156">0.giv-active</ta>
            <ta e="T159" id="Seg_4264" s="T158">accs-sit</ta>
            <ta e="T160" id="Seg_4265" s="T159">giv-active</ta>
            <ta e="T161" id="Seg_4266" s="T160">0.giv-active</ta>
            <ta e="T162" id="Seg_4267" s="T161">0.giv-active</ta>
            <ta e="T164" id="Seg_4268" s="T163">0.giv-active</ta>
            <ta e="T166" id="Seg_4269" s="T165">new</ta>
            <ta e="T169" id="Seg_4270" s="T168">giv-active</ta>
            <ta e="T171" id="Seg_4271" s="T170">quot-sp</ta>
            <ta e="T175" id="Seg_4272" s="T174">0.giv-inactive-Q</ta>
            <ta e="T176" id="Seg_4273" s="T175">0.giv-active 0.quot-sp</ta>
            <ta e="T178" id="Seg_4274" s="T177">0.giv-inactive 0.quot-sp</ta>
            <ta e="T181" id="Seg_4275" s="T180">0.giv-active 0.quot-sp</ta>
            <ta e="T184" id="Seg_4276" s="T183">giv-inactive</ta>
            <ta e="T185" id="Seg_4277" s="T184">new</ta>
            <ta e="T191" id="Seg_4278" s="T190">giv-active</ta>
            <ta e="T192" id="Seg_4279" s="T191">0.giv-inactive</ta>
            <ta e="T194" id="Seg_4280" s="T193">accs-inf</ta>
            <ta e="T196" id="Seg_4281" s="T195">0.giv-active</ta>
            <ta e="T197" id="Seg_4282" s="T196">giv-active</ta>
            <ta e="T198" id="Seg_4283" s="T197">quot-sp</ta>
            <ta e="T200" id="Seg_4284" s="T199">0.giv-active-Q</ta>
            <ta e="T202" id="Seg_4285" s="T201">0.giv-active-Q</ta>
            <ta e="T204" id="Seg_4286" s="T203">accs-inf-Q</ta>
            <ta e="T206" id="Seg_4287" s="T205">0.giv-active-Q</ta>
            <ta e="T208" id="Seg_4288" s="T207">0.giv-active 0.quot-sp</ta>
            <ta e="T210" id="Seg_4289" s="T209">0.giv-active</ta>
            <ta e="T212" id="Seg_4290" s="T211">giv-active</ta>
            <ta e="T214" id="Seg_4291" s="T213">giv-inactive</ta>
            <ta e="T215" id="Seg_4292" s="T214">0.giv-active</ta>
            <ta e="T218" id="Seg_4293" s="T217">0.giv-active</ta>
            <ta e="T221" id="Seg_4294" s="T220">0.giv-active</ta>
            <ta e="T222" id="Seg_4295" s="T221">accs-inf</ta>
            <ta e="T223" id="Seg_4296" s="T222">0.giv-active</ta>
            <ta e="T225" id="Seg_4297" s="T224">new</ta>
            <ta e="T228" id="Seg_4298" s="T227">giv-active</ta>
            <ta e="T231" id="Seg_4299" s="T230">0.giv-active</ta>
            <ta e="T233" id="Seg_4300" s="T232">giv-active</ta>
            <ta e="T237" id="Seg_4301" s="T236">giv-active</ta>
            <ta e="T238" id="Seg_4302" s="T237">giv-inactive</ta>
            <ta e="T240" id="Seg_4303" s="T239">accs-inf</ta>
            <ta e="T243" id="Seg_4304" s="T242">accs-inf</ta>
            <ta e="T245" id="Seg_4305" s="T244">0.giv-inactive-Q</ta>
            <ta e="T247" id="Seg_4306" s="T246">0.giv-active-Q</ta>
            <ta e="T248" id="Seg_4307" s="T247">0.giv-active 0.quot-sp</ta>
            <ta e="T250" id="Seg_4308" s="T249">0.giv-active</ta>
            <ta e="T252" id="Seg_4309" s="T251">giv-active</ta>
            <ta e="T254" id="Seg_4310" s="T253">0.giv-active</ta>
            <ta e="T255" id="Seg_4311" s="T254">giv-inactive-Q</ta>
            <ta e="T256" id="Seg_4312" s="T255">0.giv-active-Q</ta>
            <ta e="T257" id="Seg_4313" s="T256">0.giv-active 0.quot-sp</ta>
            <ta e="T258" id="Seg_4314" s="T257">giv-inactive</ta>
            <ta e="T259" id="Seg_4315" s="T258">giv-active</ta>
            <ta e="T263" id="Seg_4316" s="T262">0.giv-active</ta>
            <ta e="T264" id="Seg_4317" s="T263">giv-inactive</ta>
            <ta e="T266" id="Seg_4318" s="T265">new</ta>
            <ta e="T272" id="Seg_4319" s="T271">0.giv-inactive</ta>
            <ta e="T274" id="Seg_4320" s="T273">accs-gen</ta>
            <ta e="T275" id="Seg_4321" s="T274">quot-sp</ta>
            <ta e="T276" id="Seg_4322" s="T275">giv-inactive-Q</ta>
            <ta e="T280" id="Seg_4323" s="T279">accs-gen-Q</ta>
            <ta e="T281" id="Seg_4324" s="T280">accs-inf-Q</ta>
            <ta e="T284" id="Seg_4325" s="T283">giv-inactive-Q</ta>
            <ta e="T285" id="Seg_4326" s="T284">giv-active-Q</ta>
            <ta e="T291" id="Seg_4327" s="T290">giv-active-Q</ta>
            <ta e="T292" id="Seg_4328" s="T291">giv-inactive-Q</ta>
            <ta e="T294" id="Seg_4329" s="T293">giv-inactive-Q</ta>
            <ta e="T295" id="Seg_4330" s="T294">giv-active-Q</ta>
            <ta e="T296" id="Seg_4331" s="T295">giv-active-Q</ta>
         </annotation>
         <annotation name="Top" tierref="Top">
            <ta e="T2" id="Seg_4332" s="T0">top.int.concr</ta>
            <ta e="T5" id="Seg_4333" s="T2">top.int.concr</ta>
            <ta e="T12" id="Seg_4334" s="T10">top.int.concr</ta>
            <ta e="T18" id="Seg_4335" s="T17">top.int.concr</ta>
            <ta e="T27" id="Seg_4336" s="T26">0.top.int.concr</ta>
            <ta e="T40" id="Seg_4337" s="T39">0.top.int.abstr.</ta>
            <ta e="T45" id="Seg_4338" s="T44">top.int.concr</ta>
            <ta e="T50" id="Seg_4339" s="T48">top.int.concr</ta>
            <ta e="T55" id="Seg_4340" s="T53">top.int.concr.contr</ta>
            <ta e="T60" id="Seg_4341" s="T59">0.top.int.concr</ta>
            <ta e="T62" id="Seg_4342" s="T60">top.int.concr</ta>
            <ta e="T69" id="Seg_4343" s="T68">0.top.int.concr</ta>
            <ta e="T71" id="Seg_4344" s="T70">0.top.int.concr</ta>
            <ta e="T75" id="Seg_4345" s="T74">0.top.int.abstr</ta>
            <ta e="T78" id="Seg_4346" s="T77">top.int.concr</ta>
            <ta e="T82" id="Seg_4347" s="T80">top.int.concr</ta>
            <ta e="T94" id="Seg_4348" s="T91">top.int.concr</ta>
            <ta e="T98" id="Seg_4349" s="T96">top.int.concr</ta>
            <ta e="T100" id="Seg_4350" s="T98">top.int.concr</ta>
            <ta e="T106" id="Seg_4351" s="T104">top.int.concr</ta>
            <ta e="T110" id="Seg_4352" s="T109">0.top.int.concr</ta>
            <ta e="T123" id="Seg_4353" s="T122">0.top.int.concr</ta>
            <ta e="T127" id="Seg_4354" s="T126">0.top.int.concr</ta>
            <ta e="T132" id="Seg_4355" s="T131">0.top.int.concr</ta>
            <ta e="T135" id="Seg_4356" s="T134">0.top.int.concr</ta>
            <ta e="T138" id="Seg_4357" s="T135">top.int.concr</ta>
            <ta e="T149" id="Seg_4358" s="T148">top.int.concr</ta>
            <ta e="T155" id="Seg_4359" s="T154">0.top.int.concr</ta>
            <ta e="T162" id="Seg_4360" s="T161">0.top.int.concr</ta>
            <ta e="T164" id="Seg_4361" s="T163">0.top.int.concr</ta>
            <ta e="T169" id="Seg_4362" s="T168">top.int.concr</ta>
            <ta e="T178" id="Seg_4363" s="T177">0.top.int.concr</ta>
            <ta e="T190" id="Seg_4364" s="T189">0.top.int.abstr.</ta>
            <ta e="T191" id="Seg_4365" s="T190">top.int.concr</ta>
            <ta e="T197" id="Seg_4366" s="T196">top.int.concr</ta>
            <ta e="T205" id="Seg_4367" s="T202">top.int.concr</ta>
            <ta e="T210" id="Seg_4368" s="T208">top.int.concr</ta>
            <ta e="T215" id="Seg_4369" s="T214">0.top.int.concr</ta>
            <ta e="T218" id="Seg_4370" s="T217">0.top.int.concr</ta>
            <ta e="T221" id="Seg_4371" s="T218">top.int.concr</ta>
            <ta e="T231" id="Seg_4372" s="T227">top.int.concr</ta>
            <ta e="T237" id="Seg_4373" s="T234">top.int.concr</ta>
            <ta e="T245" id="Seg_4374" s="T244">top.int.concr</ta>
            <ta e="T252" id="Seg_4375" s="T250">top.int.concr</ta>
            <ta e="T263" id="Seg_4376" s="T262">0.top.int.concr</ta>
            <ta e="T265" id="Seg_4377" s="T263">top.int.concr</ta>
            <ta e="T272" id="Seg_4378" s="T271">0.top.int.concr</ta>
            <ta e="T275" id="Seg_4379" s="T274">0.top.int.abstr.</ta>
            <ta e="T276" id="Seg_4380" s="T275">top.int.concr</ta>
            <ta e="T285" id="Seg_4381" s="T284">top.int.concr</ta>
            <ta e="T294" id="Seg_4382" s="T289">top.ext</ta>
         </annotation>
         <annotation name="Foc" tierref="Foc">
            <ta e="T10" id="Seg_4383" s="T0">foc.wid</ta>
            <ta e="T17" id="Seg_4384" s="T12">foc.wid</ta>
            <ta e="T23" id="Seg_4385" s="T20">foc.nar</ta>
            <ta e="T24" id="Seg_4386" s="T23">foc.nar</ta>
            <ta e="T30" id="Seg_4387" s="T28">foc.nar</ta>
            <ta e="T41" id="Seg_4388" s="T39">foc.int</ta>
            <ta e="T43" id="Seg_4389" s="T42">foc.int</ta>
            <ta e="T48" id="Seg_4390" s="T45">foc.int</ta>
            <ta e="T53" id="Seg_4391" s="T50">foc.int</ta>
            <ta e="T57" id="Seg_4392" s="T55">foc.contr</ta>
            <ta e="T60" id="Seg_4393" s="T57">foc.int</ta>
            <ta e="T65" id="Seg_4394" s="T62">foc.int</ta>
            <ta e="T69" id="Seg_4395" s="T65">foc.int</ta>
            <ta e="T71" id="Seg_4396" s="T69">foc.int</ta>
            <ta e="T75" id="Seg_4397" s="T71">foc.wid</ta>
            <ta e="T80" id="Seg_4398" s="T78">foc.int</ta>
            <ta e="T86" id="Seg_4399" s="T82">foc.int</ta>
            <ta e="T90" id="Seg_4400" s="T89">foc.int</ta>
            <ta e="T96" id="Seg_4401" s="T94">foc.int</ta>
            <ta e="T104" id="Seg_4402" s="T100">foc.wid</ta>
            <ta e="T107" id="Seg_4403" s="T106">foc.int</ta>
            <ta e="T110" id="Seg_4404" s="T108">foc.int</ta>
            <ta e="T111" id="Seg_4405" s="T110">foc.nar</ta>
            <ta e="T122" id="Seg_4406" s="T121">foc.nar</ta>
            <ta e="T127" id="Seg_4407" s="T124">foc.int</ta>
            <ta e="T132" id="Seg_4408" s="T127">foc.int</ta>
            <ta e="T134" id="Seg_4409" s="T133">foc.nar</ta>
            <ta e="T140" id="Seg_4410" s="T139">foc.nar</ta>
            <ta e="T152" id="Seg_4411" s="T150">foc.nar</ta>
            <ta e="T155" id="Seg_4412" s="T152">foc.int</ta>
            <ta e="T162" id="Seg_4413" s="T155">foc.int</ta>
            <ta e="T164" id="Seg_4414" s="T162">foc.int</ta>
            <ta e="T168" id="Seg_4415" s="T164">foc.wid</ta>
            <ta e="T171" id="Seg_4416" s="T169">foc.int</ta>
            <ta e="T173" id="Seg_4417" s="T172">foc.nar</ta>
            <ta e="T178" id="Seg_4418" s="T176">foc.int</ta>
            <ta e="T180" id="Seg_4419" s="T178">foc.nar</ta>
            <ta e="T188" id="Seg_4420" s="T184">foc.nar</ta>
            <ta e="T196" id="Seg_4421" s="T193">foc.int</ta>
            <ta e="T198" id="Seg_4422" s="T197">foc.int</ta>
            <ta e="T200" id="Seg_4423" s="T199">foc.int</ta>
            <ta e="T202" id="Seg_4424" s="T200">foc.int</ta>
            <ta e="T207" id="Seg_4425" s="T205">foc.nar</ta>
            <ta e="T213" id="Seg_4426" s="T212">foc.int</ta>
            <ta e="T215" id="Seg_4427" s="T213">foc.int</ta>
            <ta e="T218" id="Seg_4428" s="T216">foc.int</ta>
            <ta e="T223" id="Seg_4429" s="T221">foc.int</ta>
            <ta e="T227" id="Seg_4430" s="T223">foc.wid</ta>
            <ta e="T234" id="Seg_4431" s="T233">foc.nar</ta>
            <ta e="T244" id="Seg_4432" s="T237">foc.int</ta>
            <ta e="T247" id="Seg_4433" s="T245">foc.int</ta>
            <ta e="T260" id="Seg_4434" s="T252">foc.int</ta>
            <ta e="T262" id="Seg_4435" s="T260">foc.nar</ta>
            <ta e="T267" id="Seg_4436" s="T265">foc.nar</ta>
            <ta e="T271" id="Seg_4437" s="T269">foc.nar</ta>
            <ta e="T275" id="Seg_4438" s="T272">foc.wid</ta>
            <ta e="T284" id="Seg_4439" s="T278">foc.nar</ta>
            <ta e="T289" id="Seg_4440" s="T285">foc.int</ta>
            <ta e="T297" id="Seg_4441" s="T296">foc.nar</ta>
            <ta e="T298" id="Seg_4442" s="T297">foc.nar</ta>
         </annotation>
         <annotation name="BOR" tierref="BOR">
            <ta e="T8" id="Seg_4443" s="T7">EV:gram (DIM)</ta>
            <ta e="T12" id="Seg_4444" s="T11">EV:gram (DIM)</ta>
            <ta e="T24" id="Seg_4445" s="T23">RUS:cult</ta>
            <ta e="T25" id="Seg_4446" s="T24">RUS:cult</ta>
            <ta e="T41" id="Seg_4447" s="T40">RUS:cult</ta>
            <ta e="T43" id="Seg_4448" s="T42">RUS:cult</ta>
            <ta e="T51" id="Seg_4449" s="T50">RUS:core</ta>
            <ta e="T108" id="Seg_4450" s="T107">RUS:gram</ta>
            <ta e="T174" id="Seg_4451" s="T173">RUS:mod</ta>
            <ta e="T216" id="Seg_4452" s="T215">RUS:gram</ta>
            <ta e="T258" id="Seg_4453" s="T257">RUS:cult</ta>
            <ta e="T264" id="Seg_4454" s="T263">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon">
            <ta e="T24" id="Seg_4455" s="T23">Vsub</ta>
            <ta e="T25" id="Seg_4456" s="T24">inVins Vsub</ta>
            <ta e="T41" id="Seg_4457" s="T40">Vsub</ta>
            <ta e="T43" id="Seg_4458" s="T42">Vsub</ta>
            <ta e="T51" id="Seg_4459" s="T50">fortition Vsub</ta>
            <ta e="T108" id="Seg_4460" s="T107">Vsub</ta>
            <ta e="T174" id="Seg_4461" s="T173">Vsub</ta>
            <ta e="T216" id="Seg_4462" s="T215">Vsub</ta>
            <ta e="T258" id="Seg_4463" s="T257">Vsub</ta>
            <ta e="T264" id="Seg_4464" s="T263">Vsub</ta>
         </annotation>
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T24" id="Seg_4465" s="T23">dir:infl</ta>
            <ta e="T25" id="Seg_4466" s="T24">dir:bare</ta>
            <ta e="T41" id="Seg_4467" s="T40">dir:infl</ta>
            <ta e="T43" id="Seg_4468" s="T42">dir:infl</ta>
            <ta e="T51" id="Seg_4469" s="T50">dir:bare</ta>
            <ta e="T108" id="Seg_4470" s="T107">dir:bare</ta>
            <ta e="T174" id="Seg_4471" s="T173">indir:infl</ta>
            <ta e="T216" id="Seg_4472" s="T215">dir:bare</ta>
            <ta e="T258" id="Seg_4473" s="T257">dir:infl</ta>
            <ta e="T264" id="Seg_4474" s="T263">dir:infl</ta>
         </annotation>
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T10" id="Seg_4475" s="T0">In one place in tundra, in a place with very deep snow there was a small tent, they say.</ta>
            <ta e="T17" id="Seg_4476" s="T10">In that tent light became to flame up every evening.</ta>
            <ta e="T23" id="Seg_4477" s="T17">There was one small human, apparently.</ta>
            <ta e="T27" id="Seg_4478" s="T23">He was one merchant's worker.</ta>
            <ta e="T28" id="Seg_4479" s="T27">People:</ta>
            <ta e="T35" id="Seg_4480" s="T28">"What is this flaming up light?", they say wondering.</ta>
            <ta e="T41" id="Seg_4481" s="T35">There that brave man says to his merchant:</ta>
            <ta e="T44" id="Seg_4482" s="T41">"Let us bet", he says.</ta>
            <ta e="T48" id="Seg_4483" s="T44">"I will go, find out and come back.</ta>
            <ta e="T57" id="Seg_4484" s="T48">If I lose, then I will work forever for free, if you lose, then you will give me money."</ta>
            <ta e="T60" id="Seg_4485" s="T57">On this they agree.</ta>
            <ta e="T65" id="Seg_4486" s="T60">That human goes off with his dogs in the evening.</ta>
            <ta e="T69" id="Seg_4487" s="T65">He drives and goes into the tent.</ta>
            <ta e="T75" id="Seg_4488" s="T69">He entered it, a human with a hairy face is sitting there.</ta>
            <ta e="T80" id="Seg_4489" s="T75">Being a bit afraid the brave man sits down on a bench.</ta>
            <ta e="T86" id="Seg_4490" s="T80">The hairy human had prepared food and sat down to eat, apparently.</ta>
            <ta e="T89" id="Seg_4491" s="T86">He took out food:</ta>
            <ta e="T91" id="Seg_4492" s="T89">"Eat", he said.</ta>
            <ta e="T96" id="Seg_4493" s="T91">After having eaten they lay down to sleep.</ta>
            <ta e="T104" id="Seg_4494" s="T96">Early in the morning, as [the young man] is still lying, the [hairy] human goes off on skis.</ta>
            <ta e="T110" id="Seg_4495" s="T104">Our small human stands up and thinks:</ta>
            <ta e="T116" id="Seg_4496" s="T110">"How shall I prove that I've been here?", he thinks.</ta>
            <ta e="T132" id="Seg_4497" s="T116">"I will find out, where this human went, and I will bring this as evidence", he thinks, and following his path he reaches a stony mountain.</ta>
            <ta e="T140" id="Seg_4498" s="T132">Hiding he came closer, the whole mountain was dug up.</ta>
            <ta e="T152" id="Seg_4499" s="T140">The voices of many people were heard, as he looked into a hole, he saw that all people had hairy faces.</ta>
            <ta e="T155" id="Seg_4500" s="T152">He stalked inside.</ta>
            <ta e="T162" id="Seg_4501" s="T155">He goes into that yurt and sits down.</ta>
            <ta e="T168" id="Seg_4502" s="T162">He sat down and there was a woman, apparently. </ta>
            <ta e="T171" id="Seg_4503" s="T168">She asks silently:</ta>
            <ta e="T176" id="Seg_4504" s="T171">"What has brought you here?", she says.</ta>
            <ta e="T178" id="Seg_4505" s="T176">He says on that:</ta>
            <ta e="T181" id="Seg_4506" s="T178">"I need something small", he says.</ta>
            <ta e="T190" id="Seg_4507" s="T181">Then this woman gives him something very soft.</ta>
            <ta e="T198" id="Seg_4508" s="T190">Without taking a look on it, he puts it into his pocket, the woman says:</ta>
            <ta e="T208" id="Seg_4509" s="T198">"Well, now go away quickly, if my people see you here, they won't treat you well", she said.</ta>
            <ta e="T218" id="Seg_4510" s="T208">As he had heard this, the small human went out, sat down on his dog sled and hurried away.</ta>
            <ta e="T227" id="Seg_4511" s="T218">As he was driving, he looked back and saw a human following him.</ta>
            <ta e="T234" id="Seg_4512" s="T227">As he saw that one chasing him, he ran [his dogs] even more.</ta>
            <ta e="T244" id="Seg_4513" s="T234">The human with the hairy face grabbed the back legs of the sled and pulled two of them out.</ta>
            <ta e="T250" id="Seg_4514" s="T244">"If I had gotten you, I would have shown you what is what!", he shouted.</ta>
            <ta e="T263" id="Seg_4515" s="T250">The boy came back home and said "I have brought evidence", he took the soft thing out of his pocket; it turned out to be a black fox fur.</ta>
            <ta e="T268" id="Seg_4516" s="T263">Then the merchant gave him a lot of money.</ta>
            <ta e="T272" id="Seg_4517" s="T268">So he became a rich human.</ta>
            <ta e="T275" id="Seg_4518" s="T272">There the old people said:</ta>
            <ta e="T289" id="Seg_4519" s="T275">"You've reached those poeple, who had run away from the taxes of the czar a long time ago, they hid themselves and built their dwelling inside of a mountain.</ta>
            <ta e="T298" id="Seg_4520" s="T289">The human you saw first in the tent is their hunter."</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T10" id="Seg_4521" s="T0">An einem Ort in der Tundra, an einem Ort mit sehr tiefem Schnee, stand früher ein kleines Zelt, sagt man.</ta>
            <ta e="T17" id="Seg_4522" s="T10">In diesem Zelt leuchtete jeden Abend Licht auf.</ta>
            <ta e="T23" id="Seg_4523" s="T17">Dort war offenbar ein kleiner Mensch.</ta>
            <ta e="T27" id="Seg_4524" s="T23">Er arbeitete bei einem Kaufmann.</ta>
            <ta e="T28" id="Seg_4525" s="T27">Die Leute:</ta>
            <ta e="T35" id="Seg_4526" s="T28">"Was für ein Licht brennt dort?", sagen sie und wundern sich.</ta>
            <ta e="T41" id="Seg_4527" s="T35">Da sagt dieser Prachtkerl zu seinem Kaufmann:</ta>
            <ta e="T44" id="Seg_4528" s="T41">"Lass uns wetten", sagt er.</ta>
            <ta e="T48" id="Seg_4529" s="T44">"Ich gehe, erfahre es und komme.</ta>
            <ta e="T57" id="Seg_4530" s="T48">Wenn ich verliere, dann arbeite ich für immer umsonst, wenn du aber [verlierst], dann gibst du Geld."</ta>
            <ta e="T60" id="Seg_4531" s="T57">Darauf einigen sie sich.</ta>
            <ta e="T65" id="Seg_4532" s="T60">Dieser Mensch fährt am Abend mit seinen Hunden los.</ta>
            <ta e="T69" id="Seg_4533" s="T65">Er fährt und geht in das Zelt hinein.</ta>
            <ta e="T75" id="Seg_4534" s="T69">Er ging hinein, dort sitzt ein Mensch mit behaartem Gesicht.</ta>
            <ta e="T80" id="Seg_4535" s="T75">Etwas Angst habend setzt sich der Prachtkerl auf die Bank.</ta>
            <ta e="T86" id="Seg_4536" s="T80">Der behaarte Mensch hatte offenbar Essen gekocht und setzte sich, um zu essen.</ta>
            <ta e="T89" id="Seg_4537" s="T86">Er nahm Essen heraus:</ta>
            <ta e="T91" id="Seg_4538" s="T89">"Iss", sagte er.</ta>
            <ta e="T96" id="Seg_4539" s="T91">Nachdem sie gegessen hatten, legen sie sich hin, um zu schlafen.</ta>
            <ta e="T104" id="Seg_4540" s="T96">Früh am Morgen, als er noch liegt, fährt der [haarige] Mensch auf Skiern los.</ta>
            <ta e="T110" id="Seg_4541" s="T104">Unser kleiner Mensch steht auf und denkt nach:</ta>
            <ta e="T116" id="Seg_4542" s="T110">"Wie soll ich beweisen, dass ich hier war?", denkt er.</ta>
            <ta e="T132" id="Seg_4543" s="T116">"Ich finde heraus, wohin dieser Mensch gegangen ist, und bringe es als Beweis", denkt er, folgt seinem Pfad und kommt an einen steinigen Berg.</ta>
            <ta e="T140" id="Seg_4544" s="T132">Er kam versteckt dorthin, der ganze Berg war ausgehöhlt.</ta>
            <ta e="T152" id="Seg_4545" s="T140">Es waren die Stimmen vieler Leute zu hören, als er durch ein Loch hineinschaute, sah er, dass alle Leute behaarte Gesichter hatten.</ta>
            <ta e="T155" id="Seg_4546" s="T152">Er schlich sich hinein.</ta>
            <ta e="T162" id="Seg_4547" s="T155">Er geht in diese Behausung, diese Jurte hinein und setzt sich.</ta>
            <ta e="T168" id="Seg_4548" s="T162">Er setzte sich und dort war offenbar eine Frau.</ta>
            <ta e="T171" id="Seg_4549" s="T168">Sie fragt ganz leise:</ta>
            <ta e="T176" id="Seg_4550" s="T171">"Mit welchem Bedürfnis bist du gekommen?", sagt sie.</ta>
            <ta e="T178" id="Seg_4551" s="T176">Darauf sagt er:</ta>
            <ta e="T181" id="Seg_4552" s="T178">"Nach einer Kleinigkeit", sagt er.</ta>
            <ta e="T190" id="Seg_4553" s="T181">Da gibt diese Frau ihm etwas sehr Weiches.</ta>
            <ta e="T198" id="Seg_4554" s="T190">Ohne es anzuschauen, steckt er es in die Tasche, die Frau sagt:</ta>
            <ta e="T208" id="Seg_4555" s="T198">"Nun, fahr schnell weg, wenn meine Leute dich hier sehen, dann werden sie dich nicht gut behandeln", sagte sie.</ta>
            <ta e="T218" id="Seg_4556" s="T208">Als er das gehört hatte, ging der kleine Mensch hinaus, setzte sich auf seinen Hundeschlitten und machte sich davon.</ta>
            <ta e="T227" id="Seg_4557" s="T218">Als er fuhr, sah er sich um und sah, dass ihn ein Mensch verfolgte.</ta>
            <ta e="T234" id="Seg_4558" s="T227">Als er sah, dass er verfolgt wurde, jagte er [seine Hunde] noch mehr.</ta>
            <ta e="T244" id="Seg_4559" s="T234">Der Mensch mit dem behaarten Gesicht griff die hinteren Beine des Schlittens und zog zwei heraus.</ta>
            <ta e="T250" id="Seg_4560" s="T244">"Wenn ich dich bekommen hätte, hätte ich's dir gezeigt", schrie er.</ta>
            <ta e="T263" id="Seg_4561" s="T250">Der Junge kam nach Hause, sagte "Ich habe einen Beweis gebracht" und zog das Weiche aus seiner Tasche heraus; es war das Fell eines schwarzen Fuchses.</ta>
            <ta e="T268" id="Seg_4562" s="T263">Da gab ihm der Kaufmann sehr viel Geld.</ta>
            <ta e="T272" id="Seg_4563" s="T268">So wurde er ein reicher Mensch.</ta>
            <ta e="T275" id="Seg_4564" s="T272">Darauf sagten die Alten:</ta>
            <ta e="T289" id="Seg_4565" s="T275">"Du bist zu Leuten gekommen, die vor langer Zeit vor den Steuern des Zaren geflohen sind, sie versteckten sich und haben sich in einem Berg eine Wohnung gebaut.</ta>
            <ta e="T298" id="Seg_4566" s="T289">Der Mensch, den du zuerst im Zelt gesehen hast, das ist ihr Jäger, ihr Versorger."</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T10" id="Seg_4567" s="T0">В дальней местности, где лежит глубокий снег, стоял старинный маленький чум, говорят.</ta>
            <ta e="T17" id="Seg_4568" s="T10">В этом чуме каждый вечер стал загораться свет.</ta>
            <ta e="T23" id="Seg_4569" s="T17">В тех местах жил парень, оказывается.</ta>
            <ta e="T27" id="Seg_4570" s="T23">Был он работником у купца.</ta>
            <ta e="T28" id="Seg_4571" s="T27">Люди:</ta>
            <ta e="T35" id="Seg_4572" s="T28">"Что за огонь там горит?", говорят, удивляясь.</ta>
            <ta e="T41" id="Seg_4573" s="T35">Тогда парень-молодец говорит своему купцу:</ta>
            <ta e="T44" id="Seg_4574" s="T41">"Давай биться об заклад", говорит.</ta>
            <ta e="T48" id="Seg_4575" s="T44">"Пойду, разузнаю и приду.</ta>
            <ta e="T57" id="Seg_4576" s="T48">Если я проиграю, век даром на тебя работать буду, а ты, [если я выиграю], денег дашь."</ta>
            <ta e="T60" id="Seg_4577" s="T57">На том и соглашаются.</ta>
            <ta e="T65" id="Seg_4578" s="T60">Тот человек вечером на собаках отправляется.</ta>
            <ta e="T69" id="Seg_4579" s="T65">Вот приехал и входит в чум.</ta>
            <ta e="T75" id="Seg_4580" s="T69">Вошел, а там с волосатым лицом человек сидит.</ta>
            <ta e="T80" id="Seg_4581" s="T75">Тут осторожно, с оглядкой работник садится на скамью.</ta>
            <ta e="T86" id="Seg_4582" s="T80">Волосатый человек, сварив еду, собирался есть, оказывается.</ta>
            <ta e="T89" id="Seg_4583" s="T86">Ну, выложил он еду.</ta>
            <ta e="T91" id="Seg_4584" s="T89">"Ешь", говорит.</ta>
            <ta e="T96" id="Seg_4585" s="T91">Поев, улеглись спать.</ta>
            <ta e="T104" id="Seg_4586" s="T96">Утром, когда [работник] еще не поднялся, [волосатый] человек ушел на лыжах.</ta>
            <ta e="T110" id="Seg_4587" s="T104">Парень встает и размышляет:</ta>
            <ta e="T116" id="Seg_4588" s="T110">"Как же мне доказать, что я был здесь?", подумал.</ta>
            <ta e="T132" id="Seg_4589" s="T116">"Узнаю, куда поехал этот человек и привезу доказательства", подумав, последовал по его тропе и доехал до каменной горы.</ta>
            <ta e="T140" id="Seg_4590" s="T132">Когда подкрался туда, [видит:] вся гора изрыта.</ta>
            <ta e="T152" id="Seg_4591" s="T140">Изнутри раздается шум многих голосов, когда заглянул через щель — у всех людей волосатые лица.</ta>
            <ta e="T155" id="Seg_4592" s="T152">Крадучись, входит.</ta>
            <ta e="T162" id="Seg_4593" s="T155">Войдя в это жилище-балаган, усаживается.</ta>
            <ta e="T168" id="Seg_4594" s="T162">Он сел, там была одна женщина, оказывается.</ta>
            <ta e="T171" id="Seg_4595" s="T168">Она потихоньку спрашивает:</ta>
            <ta e="T176" id="Seg_4596" s="T171">"Какая нужда привела?", говорит.</ta>
            <ta e="T178" id="Seg_4597" s="T176">На это отвечает.</ta>
            <ta e="T181" id="Seg_4598" s="T178">"Невеликая нужда", говорит.</ta>
            <ta e="T190" id="Seg_4599" s="T181">Тогда эта женщина что-то мягкое ему сует.</ta>
            <ta e="T198" id="Seg_4600" s="T190">Не глядя, [человек] кладет это в карман. Женщина сказала:</ta>
            <ta e="T208" id="Seg_4601" s="T198">"Ну, поскорее уезжай, если увидят тебя здесь мои люди, не сдобровать", сказала.</ta>
            <ta e="T218" id="Seg_4602" s="T208">Услышав это, парень вышел, сел на нарту да и уехал поспешно.</ta>
            <ta e="T227" id="Seg_4603" s="T218">По пути оглянулся — видит, что за ним гонится один человек.</ta>
            <ta e="T234" id="Seg_4604" s="T227">Увидев, что тот догоняет, погнал [собак] вовсю.</ta>
            <ta e="T244" id="Seg_4605" s="T234">Человек с волосатым лицом, схватившись руками за задние копылья нарты, выдернул две из них.</ta>
            <ta e="T250" id="Seg_4606" s="T244">Отстав, прокричал: "Если бы мне попался, показал бы тебе!"</ta>
            <ta e="T263" id="Seg_4607" s="T250">Тот парень, приехав домой, говоря: "Примету привез" — из кармана вынул то мягкое, а это мех черной лисы, оказывается.</ta>
            <ta e="T268" id="Seg_4608" s="T263">Купец тогда много денег дал.</ta>
            <ta e="T272" id="Seg_4609" s="T268">Вот так богатым человеком стал [работник].</ta>
            <ta e="T275" id="Seg_4610" s="T272">Это старики так объяснили:</ta>
            <ta e="T289" id="Seg_4611" s="T275">— Ты добрался до людей, которые туда в старину убежали, спасаясь от царских податей, они, скрываясь, устраивают себе жилища внутри горы.</ta>
            <ta e="T298" id="Seg_4612" s="T289">Тот человек, которого ты встретил в лесном чуме, — это их охотник-промысловик."</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T10" id="Seg_4613" s="T0">В дальней местности, где лежит глубокий снег, стоял старинный маленький чум, говорят.</ta>
            <ta e="T17" id="Seg_4614" s="T10">В этом чуме каждый вечер стал загораться свет.</ta>
            <ta e="T23" id="Seg_4615" s="T17">В тех местах жил парень, оказывается.</ta>
            <ta e="T27" id="Seg_4616" s="T23">Был он работником у купца.</ta>
            <ta e="T28" id="Seg_4617" s="T27">Люди, удивляясь, спрашивали:</ta>
            <ta e="T35" id="Seg_4618" s="T28">— Что за огонь там горит?</ta>
            <ta e="T41" id="Seg_4619" s="T35">Тогда парень-молодец сказал своему купцу:</ta>
            <ta e="T44" id="Seg_4620" s="T41">— Давай биться об заклад, — говорит.</ta>
            <ta e="T48" id="Seg_4621" s="T44">— Пойду, разузнаю и приду.</ta>
            <ta e="T57" id="Seg_4622" s="T48">Если я проиграю, век даром на тебя работать буду, а ты, [если я выиграю], денег дашь.</ta>
            <ta e="T60" id="Seg_4623" s="T57">На том и согласились.</ta>
            <ta e="T65" id="Seg_4624" s="T60">Тот человек вечером на собаках отправился.</ta>
            <ta e="T69" id="Seg_4625" s="T65">Вот приехал и входит в чум.</ta>
            <ta e="T75" id="Seg_4626" s="T69">Вошел, а там с волосатым лицом человек сидит.</ta>
            <ta e="T80" id="Seg_4627" s="T75">Тут осторожно, с оглядкой работник сел на скамью.</ta>
            <ta e="T86" id="Seg_4628" s="T80">Волосатый человек, сварив еду, собирался есть, оказывается.</ta>
            <ta e="T89" id="Seg_4629" s="T86">Ну, выложил он еду.</ta>
            <ta e="T91" id="Seg_4630" s="T89">— Ешь, — говорит.</ta>
            <ta e="T96" id="Seg_4631" s="T91">Поев, улеглись спать.</ta>
            <ta e="T104" id="Seg_4632" s="T96">Утром, когда [работник] еще не поднялся, [волосатый] человек ушел на лыжах.</ta>
            <ta e="T110" id="Seg_4633" s="T104">Парень встает и размышляет:</ta>
            <ta e="T116" id="Seg_4634" s="T110">— Как же мне доказать, что я был здесь?</ta>
            <ta e="T132" id="Seg_4635" s="T116">Узнаю, куда поехал этот человек и привезу доказательства, — подумав, последовал по его тропе и доехал до каменной горы.</ta>
            <ta e="T140" id="Seg_4636" s="T132">Когда подкрался туда, [видит:] вся гора изрыта.</ta>
            <ta e="T152" id="Seg_4637" s="T140">Изнутри раздается шум многих голосов, когда заглянул через щель — у всех людей волосатые лица.</ta>
            <ta e="T155" id="Seg_4638" s="T152">Крадучись, входит.</ta>
            <ta e="T162" id="Seg_4639" s="T155">Войдя в это жилище-балаган, усаживается.</ta>
            <ta e="T168" id="Seg_4640" s="T162">А там была одна женщина, оказывается.</ta>
            <ta e="T171" id="Seg_4641" s="T168">Она потихоньку спрашивает:</ta>
            <ta e="T176" id="Seg_4642" s="T171">— Какая нужда привела?</ta>
            <ta e="T178" id="Seg_4643" s="T176">На это отвечает.</ta>
            <ta e="T181" id="Seg_4644" s="T178">— Невеликая нужда.</ta>
            <ta e="T190" id="Seg_4645" s="T181">Тогда эта женщина что-то мягкое ему сует.</ta>
            <ta e="T198" id="Seg_4646" s="T190">Не глядя, [человек] кладет это в карман. Женщина говорит</ta>
            <ta e="T208" id="Seg_4647" s="T198">— Ну, поскорее уезжай, если увидят тебя здесь мои люди, не сдобровать.</ta>
            <ta e="T218" id="Seg_4648" s="T208">Услышав это, парень вышел, сел на нарту да и уехал поспешно.</ta>
            <ta e="T227" id="Seg_4649" s="T218">По пути оглянулся — видит, что за ним гонится один человек.</ta>
            <ta e="T234" id="Seg_4650" s="T227">Увидев, что тот догоняет, погнал [собак] вовсю.</ta>
            <ta e="T244" id="Seg_4651" s="T234">Человек с волосатым лицом, схватившись руками за задние копылья нарты, выдернул две из них.</ta>
            <ta e="T250" id="Seg_4652" s="T244">Отстав, прокричал: — Если бы мне попался, показал бы тебе!</ta>
            <ta e="T263" id="Seg_4653" s="T250">Тот парень, приехав домой, говоря: "Примету привез", — из кармана вынул то мягкое, а это мех черной лисы, оказывается.</ta>
            <ta e="T268" id="Seg_4654" s="T263">Купец тогда много денег дал.</ta>
            <ta e="T272" id="Seg_4655" s="T268">Вот так богатым человеком стал [работник].</ta>
            <ta e="T275" id="Seg_4656" s="T272">Это старики так объяснили:</ta>
            <ta e="T289" id="Seg_4657" s="T275">— Ты добрался до людей, которые туда в старину убежали, спасаясь от царских податей, они, скрываясь, устраивают себе жилища внутри горы.</ta>
            <ta e="T298" id="Seg_4658" s="T289">Тот человек, которого ты встретил в лесном чуме, — это их охотник-промысловик.</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg"
                          name="gg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top"
                          name="Top"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc"
                          name="Foc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
