<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID72991696-EAD3-5FF8-4008-6D8AD8D1ED62">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>AnZA_19XX_WarPartridgesPikesFragment_flk</transcription-name>
         <referenced-file url="AnZA_19XX_PartridgesPikesFragment_flk.wav" />
         <referenced-file url="AnZA_19XX_PartridgesPikesFragment_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\flk\AnZA_19XX_PartridgesPikesFragment_flk\AnZA_19XX_PartridgesPikesFragment_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">50</ud-information>
            <ud-information attribute-name="# HIAT:w">33</ud-information>
            <ud-information attribute-name="# e">32</ud-information>
            <ud-information attribute-name="# HIAT:u">5</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="AnZA">
            <abbreviation>AnZA</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="1.001" type="appl" />
         <tli id="T1" time="1.617" type="appl" />
         <tli id="T2" time="2.234" type="appl" />
         <tli id="T3" time="2.85" type="appl" />
         <tli id="T4" time="3.467" type="appl" />
         <tli id="T5" time="4.083" type="appl" />
         <tli id="T6" time="4.7" type="appl" />
         <tli id="T7" time="5.316" type="appl" />
         <tli id="T8" time="5.933" type="appl" />
         <tli id="T9" time="6.549" type="appl" />
         <tli id="T10" time="7.156" type="appl" />
         <tli id="T11" time="7.763" type="appl" />
         <tli id="T12" time="8.369" type="appl" />
         <tli id="T13" time="8.976" type="appl" />
         <tli id="T14" time="9.583" type="appl" />
         <tli id="T15" time="10.19" type="appl" />
         <tli id="T16" time="10.796" type="appl" />
         <tli id="T17" time="11.403" type="appl" />
         <tli id="T18" time="12.01" type="appl" />
         <tli id="T19" time="12.501" type="appl" />
         <tli id="T20" time="12.991" type="appl" />
         <tli id="T21" time="13.482" type="appl" />
         <tli id="T22" time="13.972" type="appl" />
         <tli id="T23" time="14.463" type="appl" />
         <tli id="T24" time="14.954" type="appl" />
         <tli id="T25" time="15.444" type="appl" />
         <tli id="T26" time="15.935" type="appl" />
         <tli id="T27" time="16.424" type="appl" />
         <tli id="T28" time="16.913" type="appl" />
         <tli id="T29" time="17.402" type="appl" />
         <tli id="T30" time="17.892" type="appl" />
         <tli id="T31" time="18.381" type="appl" />
         <tli id="T32" time="18.87" type="appl" />
         <tli id="T33" time="19.37233107301587" />
         <tli id="T34" time="20.558" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="AnZA"
                      type="t">
         <timeline-fork end="T28" start="T26">
            <tli id="T26.tx.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T34" id="Seg_0" n="sc" s="T0">
               <ts e="T9" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Eː</ts>
                  <nts id="Seg_5" n="HIAT:ip">,</nts>
                  <nts id="Seg_6" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_8" n="HIAT:w" s="T1">heriːlehen</ts>
                  <nts id="Seg_9" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_11" n="HIAT:w" s="T2">biːrgehe</ts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_14" n="HIAT:w" s="T3">bu͡ollagɨna</ts>
                  <nts id="Seg_15" n="HIAT:ip">,</nts>
                  <nts id="Seg_16" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_18" n="HIAT:w" s="T4">ku͡okaːta</ts>
                  <nts id="Seg_19" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_21" n="HIAT:w" s="T5">bu͡olla</ts>
                  <nts id="Seg_22" n="HIAT:ip">,</nts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">bɨlčɨŋŋa</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">agaj</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">ɨppɨt</ts>
                  <nts id="Seg_32" n="HIAT:ip">.</nts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T18" id="Seg_35" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_37" n="HIAT:w" s="T9">Eː</ts>
                  <nts id="Seg_38" n="HIAT:ip">,</nts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_40" n="HIAT:ip">(</nts>
                  <ts e="T12" id="Seg_42" n="HIAT:w" s="T10">ku͡okaːt-</ts>
                  <nts id="Seg_43" n="HIAT:ip">)</nts>
                  <nts id="Seg_44" n="HIAT:ip">,</nts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_47" n="HIAT:w" s="T12">eː</ts>
                  <nts id="Seg_48" n="HIAT:ip">,</nts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_51" n="HIAT:w" s="T13">kurpaːskɨta</ts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_54" n="HIAT:w" s="T14">bu͡olla</ts>
                  <nts id="Seg_55" n="HIAT:ip">,</nts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_58" n="HIAT:w" s="T15">sigdetiger</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_61" n="HIAT:w" s="T16">agaj</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_64" n="HIAT:w" s="T17">ɨppɨt</ts>
                  <nts id="Seg_65" n="HIAT:ip">.</nts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_68" n="HIAT:u" s="T18">
                  <ts e="T19" id="Seg_70" n="HIAT:w" s="T18">Ol</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_73" n="HIAT:w" s="T19">ihin</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_76" n="HIAT:w" s="T20">bu͡ollagɨna</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_79" n="HIAT:w" s="T21">kurpaːskɨ</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_82" n="HIAT:w" s="T22">gi͡ene</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_85" n="HIAT:w" s="T23">bɨlčɨŋa</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_88" n="HIAT:w" s="T24">oŋu͡ok</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_91" n="HIAT:w" s="T25">agaj</ts>
                  <nts id="Seg_92" n="HIAT:ip">.</nts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T33" id="Seg_95" n="HIAT:u" s="T26">
                  <nts id="Seg_96" n="HIAT:ip">(</nts>
                  <ts e="T26.tx.1" id="Seg_98" n="HIAT:w" s="T26">Kurpa</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_101" n="HIAT:w" s="T26.tx.1">-</ts>
                  <nts id="Seg_102" n="HIAT:ip">)</nts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_105" n="HIAT:w" s="T28">e</ts>
                  <nts id="Seg_106" n="HIAT:ip">,</nts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_109" n="HIAT:w" s="T29">ku͡oka</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_112" n="HIAT:w" s="T30">gi͡ene</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_115" n="HIAT:w" s="T31">oŋu͡ok</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_118" n="HIAT:w" s="T32">agaj</ts>
                  <nts id="Seg_119" n="HIAT:ip">.</nts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T34" id="Seg_122" n="HIAT:u" s="T33">
                  <ts e="T34" id="Seg_124" n="HIAT:w" s="T33">Elete</ts>
                  <nts id="Seg_125" n="HIAT:ip">.</nts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T34" id="Seg_127" n="sc" s="T0">
               <ts e="T1" id="Seg_129" n="e" s="T0">Eː, </ts>
               <ts e="T2" id="Seg_131" n="e" s="T1">heriːlehen </ts>
               <ts e="T3" id="Seg_133" n="e" s="T2">biːrgehe </ts>
               <ts e="T4" id="Seg_135" n="e" s="T3">bu͡ollagɨna, </ts>
               <ts e="T5" id="Seg_137" n="e" s="T4">ku͡okaːta </ts>
               <ts e="T6" id="Seg_139" n="e" s="T5">bu͡olla, </ts>
               <ts e="T7" id="Seg_141" n="e" s="T6">bɨlčɨŋŋa </ts>
               <ts e="T8" id="Seg_143" n="e" s="T7">agaj </ts>
               <ts e="T9" id="Seg_145" n="e" s="T8">ɨppɨt. </ts>
               <ts e="T10" id="Seg_147" n="e" s="T9">Eː, </ts>
               <ts e="T12" id="Seg_149" n="e" s="T10">(ku͡okaːt-), </ts>
               <ts e="T13" id="Seg_151" n="e" s="T12">eː, </ts>
               <ts e="T14" id="Seg_153" n="e" s="T13">kurpaːskɨta </ts>
               <ts e="T15" id="Seg_155" n="e" s="T14">bu͡olla, </ts>
               <ts e="T16" id="Seg_157" n="e" s="T15">sigdetiger </ts>
               <ts e="T17" id="Seg_159" n="e" s="T16">agaj </ts>
               <ts e="T18" id="Seg_161" n="e" s="T17">ɨppɨt. </ts>
               <ts e="T19" id="Seg_163" n="e" s="T18">Ol </ts>
               <ts e="T20" id="Seg_165" n="e" s="T19">ihin </ts>
               <ts e="T21" id="Seg_167" n="e" s="T20">bu͡ollagɨna </ts>
               <ts e="T22" id="Seg_169" n="e" s="T21">kurpaːskɨ </ts>
               <ts e="T23" id="Seg_171" n="e" s="T22">gi͡ene </ts>
               <ts e="T24" id="Seg_173" n="e" s="T23">bɨlčɨŋa </ts>
               <ts e="T25" id="Seg_175" n="e" s="T24">oŋu͡ok </ts>
               <ts e="T26" id="Seg_177" n="e" s="T25">agaj. </ts>
               <ts e="T28" id="Seg_179" n="e" s="T26">(Kurpa -) </ts>
               <ts e="T29" id="Seg_181" n="e" s="T28">e, </ts>
               <ts e="T30" id="Seg_183" n="e" s="T29">ku͡oka </ts>
               <ts e="T31" id="Seg_185" n="e" s="T30">gi͡ene </ts>
               <ts e="T32" id="Seg_187" n="e" s="T31">oŋu͡ok </ts>
               <ts e="T33" id="Seg_189" n="e" s="T32">agaj. </ts>
               <ts e="T34" id="Seg_191" n="e" s="T33">Elete. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T9" id="Seg_192" s="T0">AnZA_19XX_PartridgesPikesFragment_flk.001 (001.001)</ta>
            <ta e="T18" id="Seg_193" s="T9">AnZA_19XX_PartridgesPikesFragment_flk.002 (001.002)</ta>
            <ta e="T26" id="Seg_194" s="T18">AnZA_19XX_PartridgesPikesFragment_flk.003 (001.003)</ta>
            <ta e="T33" id="Seg_195" s="T26">AnZA_19XX_PartridgesPikesFragment_flk.004 (001.004)</ta>
            <ta e="T34" id="Seg_196" s="T33">AnZA_19XX_PartridgesPikesFragment_flk.005 (001.005)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T9" id="Seg_197" s="T0">Э-э, һэриилэһэн бииргэһэ буоллагына, куокаата буолла былчыӈӈа агай ыппыт.</ta>
            <ta e="T18" id="Seg_198" s="T9">Э-э, куокаат, э, курпааскыта буолла сигдэтигэр агай ыппыт.</ta>
            <ta e="T26" id="Seg_199" s="T18">Ол иһин буоллагына курпааскы гиэнэ былчыӈа оӈуок агай.</ta>
            <ta e="T33" id="Seg_200" s="T26">(Курпа э,) куока гиэнэ оӈуок агай. </ta>
            <ta e="T34" id="Seg_201" s="T33">Элэтэ.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T9" id="Seg_202" s="T0">Eː, heriːlehen biːrgehe bu͡ollagɨna, ku͡okaːta bu͡olla, bɨlčɨŋŋa agaj ɨppɨt. </ta>
            <ta e="T18" id="Seg_203" s="T9">Eː, (ku͡okaːt-), eː, kurpaːskɨta bu͡olla, sigdetiger agaj ɨppɨt. </ta>
            <ta e="T26" id="Seg_204" s="T18">Ol ihin bu͡ollagɨna kurpaːskɨ gi͡ene bɨlčɨŋa oŋu͡ok agaj. </ta>
            <ta e="T33" id="Seg_205" s="T26">(Kurpa-) e, ku͡oka gi͡ene oŋu͡ok agaj. </ta>
            <ta e="T34" id="Seg_206" s="T33">Elete. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_207" s="T0">eː</ta>
            <ta e="T2" id="Seg_208" s="T1">heriːleh-en</ta>
            <ta e="T3" id="Seg_209" s="T2">biːrgeh-e</ta>
            <ta e="T4" id="Seg_210" s="T3">bu͡ollagɨna</ta>
            <ta e="T5" id="Seg_211" s="T4">ku͡okaː-ta</ta>
            <ta e="T6" id="Seg_212" s="T5">bu͡ol-l-a</ta>
            <ta e="T7" id="Seg_213" s="T6">bɨlčɨŋ-ŋa</ta>
            <ta e="T8" id="Seg_214" s="T7">agaj</ta>
            <ta e="T9" id="Seg_215" s="T8">ɨp-pɨt</ta>
            <ta e="T10" id="Seg_216" s="T9">eː</ta>
            <ta e="T13" id="Seg_217" s="T12">eː</ta>
            <ta e="T14" id="Seg_218" s="T13">kurpaːskɨ-ta</ta>
            <ta e="T15" id="Seg_219" s="T14">bu͡ol-l-a</ta>
            <ta e="T16" id="Seg_220" s="T15">sigde-ti-ger</ta>
            <ta e="T17" id="Seg_221" s="T16">agaj</ta>
            <ta e="T18" id="Seg_222" s="T17">ɨp-pɨt</ta>
            <ta e="T19" id="Seg_223" s="T18">ol</ta>
            <ta e="T20" id="Seg_224" s="T19">ihin</ta>
            <ta e="T21" id="Seg_225" s="T20">bu͡ollagɨna</ta>
            <ta e="T22" id="Seg_226" s="T21">kurpaːskɨ</ta>
            <ta e="T23" id="Seg_227" s="T22">gi͡en-e</ta>
            <ta e="T24" id="Seg_228" s="T23">bɨlčɨŋ-a</ta>
            <ta e="T25" id="Seg_229" s="T24">oŋu͡ok</ta>
            <ta e="T26" id="Seg_230" s="T25">agaj</ta>
            <ta e="T29" id="Seg_231" s="T28">e</ta>
            <ta e="T30" id="Seg_232" s="T29">ku͡oka</ta>
            <ta e="T31" id="Seg_233" s="T30">gi͡en-e</ta>
            <ta e="T32" id="Seg_234" s="T31">oŋu͡ok</ta>
            <ta e="T33" id="Seg_235" s="T32">agaj</ta>
            <ta e="T34" id="Seg_236" s="T33">ele-te</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_237" s="T0">eː</ta>
            <ta e="T2" id="Seg_238" s="T1">heriːles-An</ta>
            <ta e="T3" id="Seg_239" s="T2">biːrges-tA</ta>
            <ta e="T4" id="Seg_240" s="T3">bu͡ollagɨna</ta>
            <ta e="T5" id="Seg_241" s="T4">ku͡okaː-tA</ta>
            <ta e="T6" id="Seg_242" s="T5">bu͡ol-TI-tA</ta>
            <ta e="T7" id="Seg_243" s="T6">bɨlčɨŋ-GA</ta>
            <ta e="T8" id="Seg_244" s="T7">agaj</ta>
            <ta e="T9" id="Seg_245" s="T8">ɨt-BIT</ta>
            <ta e="T10" id="Seg_246" s="T9">eː</ta>
            <ta e="T13" id="Seg_247" s="T12">eː</ta>
            <ta e="T14" id="Seg_248" s="T13">kurpaːskɨ-tA</ta>
            <ta e="T15" id="Seg_249" s="T14">bu͡ol-TI-tA</ta>
            <ta e="T16" id="Seg_250" s="T15">higde-tI-GAr</ta>
            <ta e="T17" id="Seg_251" s="T16">agaj</ta>
            <ta e="T18" id="Seg_252" s="T17">ɨt-BIT</ta>
            <ta e="T19" id="Seg_253" s="T18">ol</ta>
            <ta e="T20" id="Seg_254" s="T19">ihin</ta>
            <ta e="T21" id="Seg_255" s="T20">bu͡ollagɨna</ta>
            <ta e="T22" id="Seg_256" s="T21">kurpaːskɨ</ta>
            <ta e="T23" id="Seg_257" s="T22">gi͡en-tA</ta>
            <ta e="T24" id="Seg_258" s="T23">bɨlčɨŋ-tA</ta>
            <ta e="T25" id="Seg_259" s="T24">oŋu͡ok</ta>
            <ta e="T26" id="Seg_260" s="T25">agaj</ta>
            <ta e="T29" id="Seg_261" s="T28">eː</ta>
            <ta e="T30" id="Seg_262" s="T29">ku͡okaː</ta>
            <ta e="T31" id="Seg_263" s="T30">gi͡en-tA</ta>
            <ta e="T32" id="Seg_264" s="T31">oŋu͡ok</ta>
            <ta e="T33" id="Seg_265" s="T32">agaj</ta>
            <ta e="T34" id="Seg_266" s="T33">ele-tA</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_267" s="T0">hey</ta>
            <ta e="T2" id="Seg_268" s="T1">fight-CVB.SEQ</ta>
            <ta e="T3" id="Seg_269" s="T2">one.out.of.two-3SG.[NOM]</ta>
            <ta e="T4" id="Seg_270" s="T3">though</ta>
            <ta e="T5" id="Seg_271" s="T4">pike-3SG.[NOM]</ta>
            <ta e="T6" id="Seg_272" s="T5">be-PST1-3SG</ta>
            <ta e="T7" id="Seg_273" s="T6">muscle-DAT/LOC</ta>
            <ta e="T8" id="Seg_274" s="T7">only</ta>
            <ta e="T9" id="Seg_275" s="T8">shoot-PST2.[3SG]</ta>
            <ta e="T10" id="Seg_276" s="T9">hey</ta>
            <ta e="T13" id="Seg_277" s="T12">hey</ta>
            <ta e="T14" id="Seg_278" s="T13">partridge-3SG.[NOM]</ta>
            <ta e="T15" id="Seg_279" s="T14">be-PST1-3SG</ta>
            <ta e="T16" id="Seg_280" s="T15">back.part.of.a.fish-3SG-DAT/LOC</ta>
            <ta e="T17" id="Seg_281" s="T16">only</ta>
            <ta e="T18" id="Seg_282" s="T17">shoot-PST2.[3SG]</ta>
            <ta e="T19" id="Seg_283" s="T18">that.[NOM]</ta>
            <ta e="T20" id="Seg_284" s="T19">because.of</ta>
            <ta e="T21" id="Seg_285" s="T20">though</ta>
            <ta e="T22" id="Seg_286" s="T21">partridge.[NOM]</ta>
            <ta e="T23" id="Seg_287" s="T22">own-3SG</ta>
            <ta e="T24" id="Seg_288" s="T23">muscle-3SG.[NOM]</ta>
            <ta e="T25" id="Seg_289" s="T24">bone.[NOM]</ta>
            <ta e="T26" id="Seg_290" s="T25">only</ta>
            <ta e="T29" id="Seg_291" s="T28">hey</ta>
            <ta e="T30" id="Seg_292" s="T29">pike.[NOM]</ta>
            <ta e="T31" id="Seg_293" s="T30">own-3SG.[NOM]</ta>
            <ta e="T32" id="Seg_294" s="T31">bone.[NOM]</ta>
            <ta e="T33" id="Seg_295" s="T32">only</ta>
            <ta e="T34" id="Seg_296" s="T33">last-3SG.[NOM]</ta>
         </annotation>
         <annotation name="gg" tierref="gg">
            <ta e="T1" id="Seg_297" s="T0">hey</ta>
            <ta e="T2" id="Seg_298" s="T1">kämpfen-CVB.SEQ</ta>
            <ta e="T3" id="Seg_299" s="T2">einer.von.zwei-3SG.[NOM]</ta>
            <ta e="T4" id="Seg_300" s="T3">aber</ta>
            <ta e="T5" id="Seg_301" s="T4">Hecht-3SG.[NOM]</ta>
            <ta e="T6" id="Seg_302" s="T5">sein-PST1-3SG</ta>
            <ta e="T7" id="Seg_303" s="T6">Muskel-DAT/LOC</ta>
            <ta e="T8" id="Seg_304" s="T7">nur</ta>
            <ta e="T9" id="Seg_305" s="T8">schießen-PST2.[3SG]</ta>
            <ta e="T10" id="Seg_306" s="T9">hey</ta>
            <ta e="T13" id="Seg_307" s="T12">hey</ta>
            <ta e="T14" id="Seg_308" s="T13">Rebhuhn-3SG.[NOM]</ta>
            <ta e="T15" id="Seg_309" s="T14">sein-PST1-3SG</ta>
            <ta e="T16" id="Seg_310" s="T15">Rückenteil.eines.Fisches-3SG-DAT/LOC</ta>
            <ta e="T17" id="Seg_311" s="T16">nur</ta>
            <ta e="T18" id="Seg_312" s="T17">schießen-PST2.[3SG]</ta>
            <ta e="T19" id="Seg_313" s="T18">jenes.[NOM]</ta>
            <ta e="T20" id="Seg_314" s="T19">wegen</ta>
            <ta e="T21" id="Seg_315" s="T20">aber</ta>
            <ta e="T22" id="Seg_316" s="T21">Rebhuhn.[NOM]</ta>
            <ta e="T23" id="Seg_317" s="T22">eigen-3SG</ta>
            <ta e="T24" id="Seg_318" s="T23">Muskel-3SG.[NOM]</ta>
            <ta e="T25" id="Seg_319" s="T24">Knochen.[NOM]</ta>
            <ta e="T26" id="Seg_320" s="T25">nur</ta>
            <ta e="T29" id="Seg_321" s="T28">hey</ta>
            <ta e="T30" id="Seg_322" s="T29">Hecht.[NOM]</ta>
            <ta e="T31" id="Seg_323" s="T30">eigen-3SG.[NOM]</ta>
            <ta e="T32" id="Seg_324" s="T31">Knochen.[NOM]</ta>
            <ta e="T33" id="Seg_325" s="T32">nur</ta>
            <ta e="T34" id="Seg_326" s="T33">letzter-3SG.[NOM]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_327" s="T0">ээ</ta>
            <ta e="T2" id="Seg_328" s="T1">воевать-CVB.SEQ</ta>
            <ta e="T3" id="Seg_329" s="T2">один.из.двух-3SG.[NOM]</ta>
            <ta e="T4" id="Seg_330" s="T3">однако</ta>
            <ta e="T5" id="Seg_331" s="T4">щука-3SG.[NOM]</ta>
            <ta e="T6" id="Seg_332" s="T5">быть-PST1-3SG</ta>
            <ta e="T7" id="Seg_333" s="T6">мышца-DAT/LOC</ta>
            <ta e="T8" id="Seg_334" s="T7">только</ta>
            <ta e="T9" id="Seg_335" s="T8">стрелять-PST2.[3SG]</ta>
            <ta e="T10" id="Seg_336" s="T9">ээ</ta>
            <ta e="T13" id="Seg_337" s="T12">ээ</ta>
            <ta e="T14" id="Seg_338" s="T13">куропатка-3SG.[NOM]</ta>
            <ta e="T15" id="Seg_339" s="T14">быть-PST1-3SG</ta>
            <ta e="T16" id="Seg_340" s="T15">спина.рыбы-3SG-DAT/LOC</ta>
            <ta e="T17" id="Seg_341" s="T16">только</ta>
            <ta e="T18" id="Seg_342" s="T17">стрелять-PST2.[3SG]</ta>
            <ta e="T19" id="Seg_343" s="T18">тот.[NOM]</ta>
            <ta e="T20" id="Seg_344" s="T19">из_за</ta>
            <ta e="T21" id="Seg_345" s="T20">однако</ta>
            <ta e="T22" id="Seg_346" s="T21">куропатка.[NOM]</ta>
            <ta e="T23" id="Seg_347" s="T22">собственный-3SG</ta>
            <ta e="T24" id="Seg_348" s="T23">мышца-3SG.[NOM]</ta>
            <ta e="T25" id="Seg_349" s="T24">кость.[NOM]</ta>
            <ta e="T26" id="Seg_350" s="T25">только</ta>
            <ta e="T29" id="Seg_351" s="T28">ээ</ta>
            <ta e="T30" id="Seg_352" s="T29">щука.[NOM]</ta>
            <ta e="T31" id="Seg_353" s="T30">собственный-3SG.[NOM]</ta>
            <ta e="T32" id="Seg_354" s="T31">кость.[NOM]</ta>
            <ta e="T33" id="Seg_355" s="T32">только</ta>
            <ta e="T34" id="Seg_356" s="T33">последний-3SG.[NOM]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_357" s="T0">interj</ta>
            <ta e="T2" id="Seg_358" s="T1">v-v:cvb</ta>
            <ta e="T3" id="Seg_359" s="T2">adj-n:(poss)-n:case</ta>
            <ta e="T4" id="Seg_360" s="T3">ptcl</ta>
            <ta e="T5" id="Seg_361" s="T4">n-n:(poss)-n:case</ta>
            <ta e="T6" id="Seg_362" s="T5">v-v:tense-v:poss.pn</ta>
            <ta e="T7" id="Seg_363" s="T6">n-n:case</ta>
            <ta e="T8" id="Seg_364" s="T7">ptcl</ta>
            <ta e="T9" id="Seg_365" s="T8">v-v:tense-v:pred.pn</ta>
            <ta e="T10" id="Seg_366" s="T9">interj</ta>
            <ta e="T13" id="Seg_367" s="T12">interj</ta>
            <ta e="T14" id="Seg_368" s="T13">n-n:(poss)-n:case</ta>
            <ta e="T15" id="Seg_369" s="T14">v-v:tense-v:poss.pn</ta>
            <ta e="T16" id="Seg_370" s="T15">n-n:poss-n:case</ta>
            <ta e="T17" id="Seg_371" s="T16">ptcl</ta>
            <ta e="T18" id="Seg_372" s="T17">v-v:tense-v:pred.pn</ta>
            <ta e="T19" id="Seg_373" s="T18">dempro-pro:case</ta>
            <ta e="T20" id="Seg_374" s="T19">post</ta>
            <ta e="T21" id="Seg_375" s="T20">ptcl</ta>
            <ta e="T22" id="Seg_376" s="T21">n-n:case</ta>
            <ta e="T23" id="Seg_377" s="T22">adj-n:(poss)</ta>
            <ta e="T24" id="Seg_378" s="T23">n-n:(poss)-n:case</ta>
            <ta e="T25" id="Seg_379" s="T24">n-n:case</ta>
            <ta e="T26" id="Seg_380" s="T25">ptcl</ta>
            <ta e="T29" id="Seg_381" s="T28">interj</ta>
            <ta e="T30" id="Seg_382" s="T29">n-n:case</ta>
            <ta e="T31" id="Seg_383" s="T30">adj-n:(poss)-n:case</ta>
            <ta e="T32" id="Seg_384" s="T31">n-n:case</ta>
            <ta e="T33" id="Seg_385" s="T32">ptcl</ta>
            <ta e="T34" id="Seg_386" s="T33">adj-n:(poss)-n:case</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_387" s="T0">interj</ta>
            <ta e="T2" id="Seg_388" s="T1">v</ta>
            <ta e="T3" id="Seg_389" s="T2">adj</ta>
            <ta e="T4" id="Seg_390" s="T3">ptcl</ta>
            <ta e="T5" id="Seg_391" s="T4">n</ta>
            <ta e="T6" id="Seg_392" s="T5">cop</ta>
            <ta e="T7" id="Seg_393" s="T6">n</ta>
            <ta e="T8" id="Seg_394" s="T7">ptcl</ta>
            <ta e="T9" id="Seg_395" s="T8">v</ta>
            <ta e="T10" id="Seg_396" s="T9">interj</ta>
            <ta e="T13" id="Seg_397" s="T12">interj</ta>
            <ta e="T14" id="Seg_398" s="T13">n</ta>
            <ta e="T15" id="Seg_399" s="T14">cop</ta>
            <ta e="T16" id="Seg_400" s="T15">n</ta>
            <ta e="T17" id="Seg_401" s="T16">ptcl</ta>
            <ta e="T18" id="Seg_402" s="T17">v</ta>
            <ta e="T19" id="Seg_403" s="T18">dempro</ta>
            <ta e="T20" id="Seg_404" s="T19">post</ta>
            <ta e="T21" id="Seg_405" s="T20">ptcl</ta>
            <ta e="T22" id="Seg_406" s="T21">n</ta>
            <ta e="T23" id="Seg_407" s="T22">adj</ta>
            <ta e="T24" id="Seg_408" s="T23">n</ta>
            <ta e="T25" id="Seg_409" s="T24">n</ta>
            <ta e="T26" id="Seg_410" s="T25">ptcl</ta>
            <ta e="T29" id="Seg_411" s="T28">interj</ta>
            <ta e="T30" id="Seg_412" s="T29">n</ta>
            <ta e="T31" id="Seg_413" s="T30">adj</ta>
            <ta e="T32" id="Seg_414" s="T31">n</ta>
            <ta e="T33" id="Seg_415" s="T32">ptcl</ta>
            <ta e="T34" id="Seg_416" s="T33">adj</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T3" id="Seg_417" s="T2">np.h:A</ta>
            <ta e="T7" id="Seg_418" s="T6">np:G</ta>
            <ta e="T16" id="Seg_419" s="T15">0.3.h:Poss np:G</ta>
            <ta e="T18" id="Seg_420" s="T17">0.3.h:A</ta>
            <ta e="T20" id="Seg_421" s="T18">pp:Cau</ta>
            <ta e="T22" id="Seg_422" s="T21">np.h:Poss</ta>
            <ta e="T24" id="Seg_423" s="T23">np:Th</ta>
            <ta e="T30" id="Seg_424" s="T29">np.h:Poss</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_425" s="T1">s:adv</ta>
            <ta e="T3" id="Seg_426" s="T2">np.h:S</ta>
            <ta e="T5" id="Seg_427" s="T4">np.h:S</ta>
            <ta e="T6" id="Seg_428" s="T5">cop</ta>
            <ta e="T9" id="Seg_429" s="T8">v:pred</ta>
            <ta e="T14" id="Seg_430" s="T13">np.h:S</ta>
            <ta e="T15" id="Seg_431" s="T14">cop</ta>
            <ta e="T18" id="Seg_432" s="T17">0.3.h:S v:pred</ta>
            <ta e="T24" id="Seg_433" s="T23">np:S</ta>
            <ta e="T25" id="Seg_434" s="T24">n:pred</ta>
            <ta e="T32" id="Seg_435" s="T31">n:pred</ta>
            <ta e="T34" id="Seg_436" s="T33">n:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST">
            <ta e="T3" id="Seg_437" s="T2">accs-inf</ta>
            <ta e="T5" id="Seg_438" s="T4">giv</ta>
            <ta e="T7" id="Seg_439" s="T6">accs-inf</ta>
            <ta e="T14" id="Seg_440" s="T13">giv-inactive</ta>
            <ta e="T16" id="Seg_441" s="T15">accs-inf</ta>
            <ta e="T18" id="Seg_442" s="T17">0.giv-active</ta>
            <ta e="T22" id="Seg_443" s="T21">giv-active</ta>
            <ta e="T24" id="Seg_444" s="T23">giv-inactive</ta>
            <ta e="T30" id="Seg_445" s="T29">giv-inactive</ta>
         </annotation>
         <annotation name="Top" tierref="Top">
            <ta e="T4" id="Seg_446" s="T2">top.int.concr</ta>
            <ta e="T6" id="Seg_447" s="T4">top.int.concr</ta>
            <ta e="T15" id="Seg_448" s="T13">top.ext</ta>
            <ta e="T24" id="Seg_449" s="T21">top.int.concr</ta>
            <ta e="T31" id="Seg_450" s="T29">top.int.concr</ta>
         </annotation>
         <annotation name="Foc" tierref="Foc">
            <ta e="T8" id="Seg_451" s="T6">foc.nar</ta>
            <ta e="T17" id="Seg_452" s="T15">foc.nar</ta>
            <ta e="T26" id="Seg_453" s="T24">foc.nar</ta>
            <ta e="T33" id="Seg_454" s="T31">foc.nar</ta>
         </annotation>
         <annotation name="BOR" tierref="BOR">
            <ta e="T14" id="Seg_455" s="T13">RUS:cult</ta>
            <ta e="T16" id="Seg_456" s="T15">EV:cult</ta>
            <ta e="T22" id="Seg_457" s="T21">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T14" id="Seg_458" s="T13">dir:infl</ta>
            <ta e="T16" id="Seg_459" s="T15">dir:infl</ta>
            <ta e="T22" id="Seg_460" s="T21">dir:bare</ta>
         </annotation>
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T9" id="Seg_461" s="T0">Eh, fighting, one of the two, it was the pike, it just shot in the (leg) muscle.</ta>
            <ta e="T18" id="Seg_462" s="T9">The pike, eh, it was the partridge, it just shot in the back of the fish.</ta>
            <ta e="T26" id="Seg_463" s="T18">Therefore the leg muscles of partridges are bony.</ta>
            <ta e="T33" id="Seg_464" s="T26">The [back] of pikes is just a bone.</ta>
            <ta e="T34" id="Seg_465" s="T33">That's all.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T9" id="Seg_466" s="T0">Äh, beim Kämpfen einer von beiden, der Hecht war es, schoss nur in den (Bein)Muskel.</ta>
            <ta e="T18" id="Seg_467" s="T9">Der Hecht, äh, das Rebhuhn war es, es schoss nur in den Fischrücken.</ta>
            <ta e="T26" id="Seg_468" s="T18">Deshalb sind die Beinmuskeln der Rebhühner knochig.</ta>
            <ta e="T33" id="Seg_469" s="T26">Der [Rücken] der Hechte ist nur Knochen.</ta>
            <ta e="T34" id="Seg_470" s="T33">Ende.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T9" id="Seg_471" s="T0">Одна из воюющих, это была щука, стрельнула [куропатке] в ногу.</ta>
            <ta e="T18" id="Seg_472" s="T9">Щука, то есть куропатка, стрельнула [щуке] в хребет.</ta>
            <ta e="T26" id="Seg_473" s="T18">Поэтому у куропатки нога костлявая.</ta>
            <ta e="T33" id="Seg_474" s="T26">[А] у щуки [в спине] одни кости. </ta>
            <ta e="T34" id="Seg_475" s="T33">Всё.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T9" id="Seg_476" s="T0">Воевая, одна от воевающих, этo была щука, стрельнула только в (ножную) мышцу.</ta>
            <ta e="T18" id="Seg_477" s="T9">Щука, то есть куропатка стрельнула (щуке) в хребет.</ta>
            <ta e="T26" id="Seg_478" s="T18">Поэтому у куропатки нога костлявая.</ta>
            <ta e="T33" id="Seg_479" s="T26">У щуки (спина) полна костей. </ta>
            <ta e="T34" id="Seg_480" s="T33">Всё.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T9" id="Seg_481" s="T0">[DCh]: As this text is only a fragment, it can be assumed that the pike and the partridge are aforementioned, hence, they are considered as given entities.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg"
                          name="gg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top"
                          name="Top"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc"
                          name="Foc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
