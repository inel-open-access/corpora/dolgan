<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID3A2CDBE2-2709-6D7F-69F6-06852CFB8FD7">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>PoMA_1964_YoungCzarPeasantsDaughter_flk</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\flk\PoMA_1964_YoungCzar_flk\PoMA_1964_YoungCzar_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">1152</ud-information>
            <ud-information attribute-name="# HIAT:w">798</ud-information>
            <ud-information attribute-name="# e">798</ud-information>
            <ud-information attribute-name="# HIAT:u">139</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PoMA">
            <abbreviation>PoMA</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="4.0" type="appl" />
         <tli id="T1" time="4.5" type="appl" />
         <tli id="T2" time="5.0" type="appl" />
         <tli id="T3" time="5.5" type="appl" />
         <tli id="T4" time="6.0" type="appl" />
         <tli id="T5" time="6.5" type="appl" />
         <tli id="T6" time="7.0" type="appl" />
         <tli id="T7" time="7.5" type="appl" />
         <tli id="T8" time="8.0" type="appl" />
         <tli id="T9" time="8.5" type="appl" />
         <tli id="T10" time="9.0" type="appl" />
         <tli id="T11" time="9.5" type="appl" />
         <tli id="T12" time="10.0" type="appl" />
         <tli id="T13" time="10.5" type="appl" />
         <tli id="T14" time="11.0" type="appl" />
         <tli id="T15" time="11.5" type="appl" />
         <tli id="T16" time="12.0" type="appl" />
         <tli id="T17" time="12.5" type="appl" />
         <tli id="T18" time="13.0" type="appl" />
         <tli id="T19" time="13.5" type="appl" />
         <tli id="T20" time="14.0" type="appl" />
         <tli id="T21" time="14.5" type="appl" />
         <tli id="T22" time="15.0" type="appl" />
         <tli id="T23" time="15.5" type="appl" />
         <tli id="T24" time="16.0" type="appl" />
         <tli id="T25" time="16.5" type="appl" />
         <tli id="T26" time="17.0" type="appl" />
         <tli id="T27" time="17.5" type="appl" />
         <tli id="T28" time="18.0" type="appl" />
         <tli id="T29" time="18.5" type="appl" />
         <tli id="T30" time="19.0" type="appl" />
         <tli id="T31" time="19.5" type="appl" />
         <tli id="T32" time="20.0" type="appl" />
         <tli id="T33" time="20.5" type="appl" />
         <tli id="T34" time="21.0" type="appl" />
         <tli id="T35" time="21.5" type="appl" />
         <tli id="T36" time="22.0" type="appl" />
         <tli id="T37" time="22.5" type="appl" />
         <tli id="T38" time="23.0" type="appl" />
         <tli id="T39" time="23.5" type="appl" />
         <tli id="T40" time="24.0" type="appl" />
         <tli id="T41" time="24.5" type="appl" />
         <tli id="T42" time="25.0" type="appl" />
         <tli id="T43" time="25.5" type="appl" />
         <tli id="T44" time="26.0" type="appl" />
         <tli id="T45" time="26.5" type="appl" />
         <tli id="T46" time="27.0" type="appl" />
         <tli id="T47" time="27.5" type="appl" />
         <tli id="T48" time="28.0" type="appl" />
         <tli id="T49" time="28.5" type="appl" />
         <tli id="T50" time="29.0" type="appl" />
         <tli id="T51" time="29.5" type="appl" />
         <tli id="T52" time="30.0" type="appl" />
         <tli id="T53" time="30.5" type="appl" />
         <tli id="T54" time="31.0" type="appl" />
         <tli id="T55" time="31.5" type="appl" />
         <tli id="T56" time="32.0" type="appl" />
         <tli id="T57" time="32.5" type="appl" />
         <tli id="T58" time="33.0" type="appl" />
         <tli id="T59" time="33.5" type="appl" />
         <tli id="T60" time="34.0" type="appl" />
         <tli id="T61" time="34.5" type="appl" />
         <tli id="T62" time="35.0" type="appl" />
         <tli id="T63" time="35.5" type="appl" />
         <tli id="T64" time="36.0" type="appl" />
         <tli id="T65" time="36.5" type="appl" />
         <tli id="T66" time="37.0" type="appl" />
         <tli id="T67" time="37.5" type="appl" />
         <tli id="T68" time="38.0" type="appl" />
         <tli id="T69" time="38.5" type="appl" />
         <tli id="T70" time="39.0" type="appl" />
         <tli id="T71" time="39.5" type="appl" />
         <tli id="T72" time="40.0" type="appl" />
         <tli id="T73" time="40.5" type="appl" />
         <tli id="T74" time="41.0" type="appl" />
         <tli id="T75" time="41.5" type="appl" />
         <tli id="T76" time="42.0" type="appl" />
         <tli id="T77" time="42.5" type="appl" />
         <tli id="T78" time="43.0" type="appl" />
         <tli id="T79" time="43.5" type="appl" />
         <tli id="T80" time="44.0" type="appl" />
         <tli id="T81" time="44.5" type="appl" />
         <tli id="T82" time="45.0" type="appl" />
         <tli id="T83" time="45.5" type="appl" />
         <tli id="T84" time="46.0" type="appl" />
         <tli id="T85" time="46.5" type="appl" />
         <tli id="T86" time="47.0" type="appl" />
         <tli id="T87" time="47.5" type="appl" />
         <tli id="T88" time="48.0" type="appl" />
         <tli id="T89" time="48.5" type="appl" />
         <tli id="T90" time="49.0" type="appl" />
         <tli id="T91" time="49.5" type="appl" />
         <tli id="T92" time="50.0" type="appl" />
         <tli id="T93" time="50.5" type="appl" />
         <tli id="T94" time="51.0" type="appl" />
         <tli id="T95" time="51.5" type="appl" />
         <tli id="T96" time="52.0" type="appl" />
         <tli id="T97" time="52.5" type="appl" />
         <tli id="T98" time="53.0" type="appl" />
         <tli id="T99" time="53.5" type="appl" />
         <tli id="T100" time="54.0" type="appl" />
         <tli id="T101" time="54.5" type="appl" />
         <tli id="T102" time="55.0" type="appl" />
         <tli id="T103" time="55.5" type="appl" />
         <tli id="T104" time="56.0" type="appl" />
         <tli id="T105" time="56.5" type="appl" />
         <tli id="T106" time="57.0" type="appl" />
         <tli id="T107" time="57.5" type="appl" />
         <tli id="T108" time="58.0" type="appl" />
         <tli id="T109" time="58.5" type="appl" />
         <tli id="T110" time="59.0" type="appl" />
         <tli id="T111" time="59.5" type="appl" />
         <tli id="T112" time="60.0" type="appl" />
         <tli id="T113" time="60.5" type="appl" />
         <tli id="T114" time="61.0" type="appl" />
         <tli id="T115" time="61.5" type="appl" />
         <tli id="T116" time="62.0" type="appl" />
         <tli id="T117" time="62.5" type="appl" />
         <tli id="T118" time="63.0" type="appl" />
         <tli id="T119" time="63.5" type="appl" />
         <tli id="T120" time="64.0" type="appl" />
         <tli id="T121" time="64.5" type="appl" />
         <tli id="T122" time="65.0" type="appl" />
         <tli id="T123" time="65.5" type="appl" />
         <tli id="T124" time="66.0" type="appl" />
         <tli id="T125" time="66.5" type="appl" />
         <tli id="T126" time="67.0" type="appl" />
         <tli id="T127" time="67.5" type="appl" />
         <tli id="T128" time="68.0" type="appl" />
         <tli id="T129" time="68.5" type="appl" />
         <tli id="T130" time="69.0" type="appl" />
         <tli id="T131" time="69.5" type="appl" />
         <tli id="T132" time="70.0" type="appl" />
         <tli id="T133" time="70.5" type="appl" />
         <tli id="T134" time="71.0" type="appl" />
         <tli id="T135" time="71.5" type="appl" />
         <tli id="T136" time="72.0" type="appl" />
         <tli id="T137" time="72.5" type="appl" />
         <tli id="T138" time="73.0" type="appl" />
         <tli id="T139" time="73.5" type="appl" />
         <tli id="T140" time="74.0" type="appl" />
         <tli id="T141" time="74.5" type="appl" />
         <tli id="T142" time="75.0" type="appl" />
         <tli id="T143" time="75.5" type="appl" />
         <tli id="T144" time="76.0" type="appl" />
         <tli id="T145" time="76.5" type="appl" />
         <tli id="T146" time="77.0" type="appl" />
         <tli id="T147" time="77.5" type="appl" />
         <tli id="T148" time="78.0" type="appl" />
         <tli id="T149" time="78.5" type="appl" />
         <tli id="T150" time="79.0" type="appl" />
         <tli id="T151" time="79.5" type="appl" />
         <tli id="T152" time="80.0" type="appl" />
         <tli id="T153" time="80.5" type="appl" />
         <tli id="T154" time="81.0" type="appl" />
         <tli id="T155" time="81.5" type="appl" />
         <tli id="T156" time="82.0" type="appl" />
         <tli id="T157" time="82.5" type="appl" />
         <tli id="T158" time="83.0" type="appl" />
         <tli id="T159" time="83.5" type="appl" />
         <tli id="T160" time="84.0" type="appl" />
         <tli id="T161" time="84.5" type="appl" />
         <tli id="T162" time="85.0" type="appl" />
         <tli id="T163" time="85.5" type="appl" />
         <tli id="T164" time="86.0" type="appl" />
         <tli id="T165" time="86.5" type="appl" />
         <tli id="T166" time="87.0" type="appl" />
         <tli id="T167" time="87.5" type="appl" />
         <tli id="T168" time="88.0" type="appl" />
         <tli id="T169" time="88.5" type="appl" />
         <tli id="T170" time="89.0" type="appl" />
         <tli id="T171" time="89.5" type="appl" />
         <tli id="T172" time="90.0" type="appl" />
         <tli id="T173" time="90.5" type="appl" />
         <tli id="T174" time="91.0" type="appl" />
         <tli id="T175" time="91.5" type="appl" />
         <tli id="T176" time="92.0" type="appl" />
         <tli id="T177" time="92.5" type="appl" />
         <tli id="T178" time="93.0" type="appl" />
         <tli id="T179" time="93.5" type="appl" />
         <tli id="T180" time="94.0" type="appl" />
         <tli id="T181" time="94.5" type="appl" />
         <tli id="T182" time="95.0" type="appl" />
         <tli id="T183" time="95.5" type="appl" />
         <tli id="T184" time="96.0" type="appl" />
         <tli id="T185" time="96.5" type="appl" />
         <tli id="T186" time="97.0" type="appl" />
         <tli id="T187" time="97.5" type="appl" />
         <tli id="T188" time="98.0" type="appl" />
         <tli id="T189" time="98.5" type="appl" />
         <tli id="T190" time="99.0" type="appl" />
         <tli id="T191" time="99.5" type="appl" />
         <tli id="T192" time="100.0" type="appl" />
         <tli id="T193" time="100.5" type="appl" />
         <tli id="T194" time="101.0" type="appl" />
         <tli id="T195" time="101.5" type="appl" />
         <tli id="T196" time="102.0" type="appl" />
         <tli id="T197" time="102.5" type="appl" />
         <tli id="T198" time="103.0" type="appl" />
         <tli id="T199" time="103.5" type="appl" />
         <tli id="T200" time="104.0" type="appl" />
         <tli id="T201" time="104.5" type="appl" />
         <tli id="T202" time="105.0" type="appl" />
         <tli id="T203" time="105.5" type="appl" />
         <tli id="T204" time="106.0" type="appl" />
         <tli id="T205" time="106.5" type="appl" />
         <tli id="T206" time="107.0" type="appl" />
         <tli id="T207" time="107.5" type="appl" />
         <tli id="T208" time="108.0" type="appl" />
         <tli id="T209" time="108.5" type="appl" />
         <tli id="T210" time="109.0" type="appl" />
         <tli id="T211" time="109.5" type="appl" />
         <tli id="T212" time="110.0" type="appl" />
         <tli id="T213" time="110.5" type="appl" />
         <tli id="T214" time="111.0" type="appl" />
         <tli id="T215" time="111.5" type="appl" />
         <tli id="T216" time="112.0" type="appl" />
         <tli id="T217" time="112.5" type="appl" />
         <tli id="T218" time="113.0" type="appl" />
         <tli id="T219" time="113.5" type="appl" />
         <tli id="T220" time="114.0" type="appl" />
         <tli id="T221" time="114.5" type="appl" />
         <tli id="T222" time="115.0" type="appl" />
         <tli id="T223" time="115.5" type="appl" />
         <tli id="T224" time="116.0" type="appl" />
         <tli id="T225" time="116.5" type="appl" />
         <tli id="T226" time="117.0" type="appl" />
         <tli id="T227" time="117.5" type="appl" />
         <tli id="T228" time="118.0" type="appl" />
         <tli id="T229" time="118.5" type="appl" />
         <tli id="T230" time="119.0" type="appl" />
         <tli id="T231" time="119.5" type="appl" />
         <tli id="T232" time="120.0" type="appl" />
         <tli id="T233" time="120.5" type="appl" />
         <tli id="T234" time="121.0" type="appl" />
         <tli id="T235" time="121.5" type="appl" />
         <tli id="T236" time="122.0" type="appl" />
         <tli id="T237" time="122.5" type="appl" />
         <tli id="T238" time="123.0" type="appl" />
         <tli id="T239" time="123.5" type="appl" />
         <tli id="T240" time="124.0" type="appl" />
         <tli id="T241" time="124.5" type="appl" />
         <tli id="T242" time="125.0" type="appl" />
         <tli id="T243" time="125.5" type="appl" />
         <tli id="T244" time="126.0" type="appl" />
         <tli id="T245" time="126.5" type="appl" />
         <tli id="T246" time="127.0" type="appl" />
         <tli id="T247" time="127.5" type="appl" />
         <tli id="T248" time="128.0" type="appl" />
         <tli id="T249" time="128.5" type="appl" />
         <tli id="T250" time="129.0" type="appl" />
         <tli id="T251" time="129.5" type="appl" />
         <tli id="T252" time="130.0" type="appl" />
         <tli id="T253" time="130.5" type="appl" />
         <tli id="T254" time="131.0" type="appl" />
         <tli id="T255" time="131.5" type="appl" />
         <tli id="T256" time="132.0" type="appl" />
         <tli id="T257" time="132.5" type="appl" />
         <tli id="T258" time="133.0" type="appl" />
         <tli id="T259" time="133.5" type="appl" />
         <tli id="T260" time="134.0" type="appl" />
         <tli id="T261" time="134.5" type="appl" />
         <tli id="T262" time="135.0" type="appl" />
         <tli id="T263" time="135.5" type="appl" />
         <tli id="T264" time="136.0" type="appl" />
         <tli id="T265" time="136.5" type="appl" />
         <tli id="T266" time="137.0" type="appl" />
         <tli id="T267" time="137.5" type="appl" />
         <tli id="T268" time="138.0" type="appl" />
         <tli id="T269" time="138.5" type="appl" />
         <tli id="T270" time="139.0" type="appl" />
         <tli id="T271" time="139.5" type="appl" />
         <tli id="T272" time="140.0" type="appl" />
         <tli id="T273" time="140.5" type="appl" />
         <tli id="T274" time="141.0" type="appl" />
         <tli id="T275" time="141.5" type="appl" />
         <tli id="T276" time="142.0" type="appl" />
         <tli id="T277" time="142.5" type="appl" />
         <tli id="T278" time="143.0" type="appl" />
         <tli id="T279" time="143.5" type="appl" />
         <tli id="T280" time="144.0" type="appl" />
         <tli id="T281" time="144.5" type="appl" />
         <tli id="T282" time="145.0" type="appl" />
         <tli id="T283" time="145.5" type="appl" />
         <tli id="T284" time="146.0" type="appl" />
         <tli id="T285" time="146.5" type="appl" />
         <tli id="T286" time="147.0" type="appl" />
         <tli id="T287" time="147.5" type="appl" />
         <tli id="T288" time="148.0" type="appl" />
         <tli id="T289" time="148.5" type="appl" />
         <tli id="T290" time="149.0" type="appl" />
         <tli id="T291" time="149.5" type="appl" />
         <tli id="T292" time="150.0" type="appl" />
         <tli id="T293" time="150.5" type="appl" />
         <tli id="T294" time="151.0" type="appl" />
         <tli id="T295" time="151.5" type="appl" />
         <tli id="T296" time="152.0" type="appl" />
         <tli id="T297" time="152.5" type="appl" />
         <tli id="T298" time="153.0" type="appl" />
         <tli id="T299" time="153.5" type="appl" />
         <tli id="T300" time="154.0" type="appl" />
         <tli id="T301" time="154.5" type="appl" />
         <tli id="T302" time="155.0" type="appl" />
         <tli id="T303" time="155.5" type="appl" />
         <tli id="T304" time="156.0" type="appl" />
         <tli id="T305" time="156.5" type="appl" />
         <tli id="T306" time="157.0" type="appl" />
         <tli id="T307" time="157.5" type="appl" />
         <tli id="T308" time="158.0" type="appl" />
         <tli id="T309" time="158.5" type="appl" />
         <tli id="T310" time="159.0" type="appl" />
         <tli id="T311" time="159.5" type="appl" />
         <tli id="T312" time="160.0" type="appl" />
         <tli id="T313" time="160.5" type="appl" />
         <tli id="T314" time="161.0" type="appl" />
         <tli id="T315" time="161.5" type="appl" />
         <tli id="T316" time="162.0" type="appl" />
         <tli id="T317" time="162.5" type="appl" />
         <tli id="T318" time="163.0" type="appl" />
         <tli id="T319" time="163.5" type="appl" />
         <tli id="T320" time="164.0" type="appl" />
         <tli id="T321" time="164.5" type="appl" />
         <tli id="T322" time="165.0" type="appl" />
         <tli id="T323" time="165.5" type="appl" />
         <tli id="T324" time="166.0" type="appl" />
         <tli id="T325" time="166.5" type="appl" />
         <tli id="T326" time="167.0" type="appl" />
         <tli id="T327" time="167.5" type="appl" />
         <tli id="T328" time="168.0" type="appl" />
         <tli id="T329" time="168.5" type="appl" />
         <tli id="T330" time="169.0" type="appl" />
         <tli id="T331" time="169.5" type="appl" />
         <tli id="T332" time="170.0" type="appl" />
         <tli id="T333" time="170.5" type="appl" />
         <tli id="T334" time="171.0" type="appl" />
         <tli id="T335" time="171.5" type="appl" />
         <tli id="T336" time="172.0" type="appl" />
         <tli id="T337" time="172.5" type="appl" />
         <tli id="T338" time="173.0" type="appl" />
         <tli id="T339" time="173.5" type="appl" />
         <tli id="T340" time="174.0" type="appl" />
         <tli id="T341" time="174.5" type="appl" />
         <tli id="T342" time="175.0" type="appl" />
         <tli id="T343" time="175.5" type="appl" />
         <tli id="T344" time="176.0" type="appl" />
         <tli id="T345" time="176.5" type="appl" />
         <tli id="T346" time="177.0" type="appl" />
         <tli id="T347" time="177.5" type="appl" />
         <tli id="T348" time="178.0" type="appl" />
         <tli id="T349" time="178.5" type="appl" />
         <tli id="T350" time="179.0" type="appl" />
         <tli id="T351" time="179.5" type="appl" />
         <tli id="T352" time="180.0" type="appl" />
         <tli id="T353" time="180.5" type="appl" />
         <tli id="T354" time="181.0" type="appl" />
         <tli id="T355" time="181.5" type="appl" />
         <tli id="T356" time="182.0" type="appl" />
         <tli id="T357" time="182.5" type="appl" />
         <tli id="T358" time="183.0" type="appl" />
         <tli id="T359" time="183.5" type="appl" />
         <tli id="T360" time="184.0" type="appl" />
         <tli id="T361" time="184.5" type="appl" />
         <tli id="T362" time="185.0" type="appl" />
         <tli id="T363" time="185.5" type="appl" />
         <tli id="T364" time="186.0" type="appl" />
         <tli id="T365" time="186.5" type="appl" />
         <tli id="T366" time="187.0" type="appl" />
         <tli id="T367" time="187.5" type="appl" />
         <tli id="T368" time="188.0" type="appl" />
         <tli id="T369" time="188.5" type="appl" />
         <tli id="T370" time="189.0" type="appl" />
         <tli id="T371" time="189.5" type="appl" />
         <tli id="T372" time="190.0" type="appl" />
         <tli id="T373" time="190.5" type="appl" />
         <tli id="T374" time="191.0" type="appl" />
         <tli id="T375" time="191.5" type="appl" />
         <tli id="T376" time="192.0" type="appl" />
         <tli id="T377" time="192.5" type="appl" />
         <tli id="T378" time="193.0" type="appl" />
         <tli id="T379" time="193.5" type="appl" />
         <tli id="T380" time="194.0" type="appl" />
         <tli id="T381" time="194.5" type="appl" />
         <tli id="T382" time="195.0" type="appl" />
         <tli id="T383" time="195.5" type="appl" />
         <tli id="T384" time="196.0" type="appl" />
         <tli id="T385" time="196.5" type="appl" />
         <tli id="T386" time="197.0" type="appl" />
         <tli id="T387" time="197.5" type="appl" />
         <tli id="T388" time="198.0" type="appl" />
         <tli id="T389" time="198.5" type="appl" />
         <tli id="T390" time="199.0" type="appl" />
         <tli id="T391" time="199.5" type="appl" />
         <tli id="T392" time="200.0" type="appl" />
         <tli id="T393" time="200.5" type="appl" />
         <tli id="T394" time="201.0" type="appl" />
         <tli id="T395" time="201.5" type="appl" />
         <tli id="T396" time="202.0" type="appl" />
         <tli id="T397" time="202.5" type="appl" />
         <tli id="T398" time="203.0" type="appl" />
         <tli id="T399" time="203.5" type="appl" />
         <tli id="T400" time="204.0" type="appl" />
         <tli id="T401" time="204.5" type="appl" />
         <tli id="T402" time="205.0" type="appl" />
         <tli id="T403" time="205.5" type="appl" />
         <tli id="T404" time="206.0" type="appl" />
         <tli id="T405" time="206.5" type="appl" />
         <tli id="T406" time="207.0" type="appl" />
         <tli id="T407" time="207.5" type="appl" />
         <tli id="T408" time="208.0" type="appl" />
         <tli id="T409" time="208.5" type="appl" />
         <tli id="T410" time="209.0" type="appl" />
         <tli id="T411" time="209.5" type="appl" />
         <tli id="T412" time="210.0" type="appl" />
         <tli id="T413" time="210.5" type="appl" />
         <tli id="T414" time="211.0" type="appl" />
         <tli id="T415" time="211.5" type="appl" />
         <tli id="T416" time="212.0" type="appl" />
         <tli id="T417" time="212.5" type="appl" />
         <tli id="T418" time="213.0" type="appl" />
         <tli id="T419" time="213.5" type="appl" />
         <tli id="T420" time="214.0" type="appl" />
         <tli id="T421" time="214.5" type="appl" />
         <tli id="T422" time="215.0" type="appl" />
         <tli id="T423" time="215.5" type="appl" />
         <tli id="T424" time="216.0" type="appl" />
         <tli id="T425" time="216.5" type="appl" />
         <tli id="T426" time="217.0" type="appl" />
         <tli id="T427" time="217.5" type="appl" />
         <tli id="T428" time="218.0" type="appl" />
         <tli id="T429" time="218.5" type="appl" />
         <tli id="T430" time="219.0" type="appl" />
         <tli id="T431" time="219.5" type="appl" />
         <tli id="T432" time="220.0" type="appl" />
         <tli id="T433" time="220.5" type="appl" />
         <tli id="T434" time="221.0" type="appl" />
         <tli id="T435" time="221.5" type="appl" />
         <tli id="T436" time="222.0" type="appl" />
         <tli id="T437" time="222.5" type="appl" />
         <tli id="T438" time="223.0" type="appl" />
         <tli id="T439" time="223.5" type="appl" />
         <tli id="T440" time="224.0" type="appl" />
         <tli id="T441" time="224.5" type="appl" />
         <tli id="T442" time="225.0" type="appl" />
         <tli id="T443" time="225.5" type="appl" />
         <tli id="T444" time="226.0" type="appl" />
         <tli id="T445" time="226.5" type="appl" />
         <tli id="T446" time="227.0" type="appl" />
         <tli id="T447" time="227.5" type="appl" />
         <tli id="T448" time="228.0" type="appl" />
         <tli id="T449" time="228.5" type="appl" />
         <tli id="T450" time="229.0" type="appl" />
         <tli id="T451" time="229.5" type="appl" />
         <tli id="T452" time="230.0" type="appl" />
         <tli id="T453" time="230.5" type="appl" />
         <tli id="T454" time="231.0" type="appl" />
         <tli id="T455" time="231.5" type="appl" />
         <tli id="T456" time="232.0" type="appl" />
         <tli id="T457" time="232.5" type="appl" />
         <tli id="T458" time="233.0" type="appl" />
         <tli id="T459" time="233.5" type="appl" />
         <tli id="T460" time="234.0" type="appl" />
         <tli id="T461" time="234.5" type="appl" />
         <tli id="T462" time="235.0" type="appl" />
         <tli id="T463" time="235.5" type="appl" />
         <tli id="T464" time="236.0" type="appl" />
         <tli id="T465" time="236.5" type="appl" />
         <tli id="T466" time="237.0" type="appl" />
         <tli id="T467" time="237.5" type="appl" />
         <tli id="T468" time="238.0" type="appl" />
         <tli id="T469" time="238.5" type="appl" />
         <tli id="T470" time="239.0" type="appl" />
         <tli id="T471" time="239.5" type="appl" />
         <tli id="T472" time="240.0" type="appl" />
         <tli id="T473" time="240.5" type="appl" />
         <tli id="T474" time="241.0" type="appl" />
         <tli id="T475" time="241.5" type="appl" />
         <tli id="T476" time="242.0" type="appl" />
         <tli id="T477" time="242.5" type="appl" />
         <tli id="T478" time="243.0" type="appl" />
         <tli id="T479" time="243.5" type="appl" />
         <tli id="T480" time="244.0" type="appl" />
         <tli id="T481" time="244.5" type="appl" />
         <tli id="T482" time="245.0" type="appl" />
         <tli id="T483" time="245.5" type="appl" />
         <tli id="T484" time="246.0" type="appl" />
         <tli id="T485" time="246.5" type="appl" />
         <tli id="T486" time="247.0" type="appl" />
         <tli id="T487" time="247.5" type="appl" />
         <tli id="T488" time="248.0" type="appl" />
         <tli id="T489" time="248.5" type="appl" />
         <tli id="T490" time="249.0" type="appl" />
         <tli id="T491" time="249.5" type="appl" />
         <tli id="T492" time="250.0" type="appl" />
         <tli id="T493" time="250.5" type="appl" />
         <tli id="T494" time="251.0" type="appl" />
         <tli id="T495" time="251.5" type="appl" />
         <tli id="T496" time="252.0" type="appl" />
         <tli id="T497" time="252.5" type="appl" />
         <tli id="T498" time="253.0" type="appl" />
         <tli id="T499" time="253.5" type="appl" />
         <tli id="T500" time="254.0" type="appl" />
         <tli id="T501" time="254.5" type="appl" />
         <tli id="T502" time="255.0" type="appl" />
         <tli id="T503" time="255.5" type="appl" />
         <tli id="T504" time="256.0" type="appl" />
         <tli id="T505" time="256.5" type="appl" />
         <tli id="T506" time="257.0" type="appl" />
         <tli id="T507" time="257.5" type="appl" />
         <tli id="T508" time="258.0" type="appl" />
         <tli id="T509" time="258.5" type="appl" />
         <tli id="T510" time="259.0" type="appl" />
         <tli id="T511" time="259.5" type="appl" />
         <tli id="T512" time="260.0" type="appl" />
         <tli id="T513" time="260.5" type="appl" />
         <tli id="T514" time="261.0" type="appl" />
         <tli id="T515" time="261.5" type="appl" />
         <tli id="T516" time="262.0" type="appl" />
         <tli id="T517" time="262.5" type="appl" />
         <tli id="T518" time="263.0" type="appl" />
         <tli id="T519" time="263.5" type="appl" />
         <tli id="T520" time="264.0" type="appl" />
         <tli id="T521" time="264.5" type="appl" />
         <tli id="T522" time="265.0" type="appl" />
         <tli id="T523" time="265.5" type="appl" />
         <tli id="T524" time="266.0" type="appl" />
         <tli id="T525" time="266.5" type="appl" />
         <tli id="T526" time="267.0" type="appl" />
         <tli id="T527" time="267.5" type="appl" />
         <tli id="T528" time="268.0" type="appl" />
         <tli id="T529" time="268.5" type="appl" />
         <tli id="T530" time="269.0" type="appl" />
         <tli id="T531" time="269.5" type="appl" />
         <tli id="T532" time="270.0" type="appl" />
         <tli id="T533" time="270.5" type="appl" />
         <tli id="T534" time="271.0" type="appl" />
         <tli id="T535" time="271.5" type="appl" />
         <tli id="T536" time="272.0" type="appl" />
         <tli id="T537" time="272.5" type="appl" />
         <tli id="T538" time="273.0" type="appl" />
         <tli id="T539" time="273.5" type="appl" />
         <tli id="T540" time="274.0" type="appl" />
         <tli id="T541" time="274.5" type="appl" />
         <tli id="T542" time="275.0" type="appl" />
         <tli id="T543" time="275.5" type="appl" />
         <tli id="T544" time="276.0" type="appl" />
         <tli id="T545" time="276.5" type="appl" />
         <tli id="T546" time="277.0" type="appl" />
         <tli id="T547" time="277.5" type="appl" />
         <tli id="T548" time="278.0" type="appl" />
         <tli id="T549" time="278.5" type="appl" />
         <tli id="T550" time="279.0" type="appl" />
         <tli id="T551" time="279.5" type="appl" />
         <tli id="T552" time="280.0" type="appl" />
         <tli id="T553" time="280.5" type="appl" />
         <tli id="T554" time="281.0" type="appl" />
         <tli id="T555" time="281.5" type="appl" />
         <tli id="T556" time="282.0" type="appl" />
         <tli id="T557" time="282.5" type="appl" />
         <tli id="T558" time="283.0" type="appl" />
         <tli id="T559" time="283.5" type="appl" />
         <tli id="T560" time="284.0" type="appl" />
         <tli id="T561" time="284.5" type="appl" />
         <tli id="T562" time="285.0" type="appl" />
         <tli id="T563" time="285.5" type="appl" />
         <tli id="T564" time="286.0" type="appl" />
         <tli id="T565" time="286.5" type="appl" />
         <tli id="T566" time="287.0" type="appl" />
         <tli id="T567" time="287.5" type="appl" />
         <tli id="T568" time="288.0" type="appl" />
         <tli id="T569" time="288.5" type="appl" />
         <tli id="T570" time="289.0" type="appl" />
         <tli id="T571" time="289.5" type="appl" />
         <tli id="T572" time="290.0" type="appl" />
         <tli id="T573" time="290.5" type="appl" />
         <tli id="T574" time="291.0" type="appl" />
         <tli id="T575" time="291.5" type="appl" />
         <tli id="T576" time="292.0" type="appl" />
         <tli id="T577" time="292.5" type="appl" />
         <tli id="T578" time="293.0" type="appl" />
         <tli id="T579" time="293.5" type="appl" />
         <tli id="T580" time="294.0" type="appl" />
         <tli id="T581" time="294.5" type="appl" />
         <tli id="T582" time="295.0" type="appl" />
         <tli id="T583" time="295.5" type="appl" />
         <tli id="T584" time="296.0" type="appl" />
         <tli id="T585" time="296.5" type="appl" />
         <tli id="T586" time="297.0" type="appl" />
         <tli id="T587" time="297.5" type="appl" />
         <tli id="T588" time="298.0" type="appl" />
         <tli id="T589" time="298.5" type="appl" />
         <tli id="T590" time="299.0" type="appl" />
         <tli id="T591" time="299.5" type="appl" />
         <tli id="T592" time="300.0" type="appl" />
         <tli id="T593" time="300.5" type="appl" />
         <tli id="T594" time="301.0" type="appl" />
         <tli id="T595" time="301.5" type="appl" />
         <tli id="T596" time="302.0" type="appl" />
         <tli id="T597" time="302.5" type="appl" />
         <tli id="T598" time="303.0" type="appl" />
         <tli id="T599" time="303.5" type="appl" />
         <tli id="T600" time="304.0" type="appl" />
         <tli id="T601" time="304.5" type="appl" />
         <tli id="T602" time="305.0" type="appl" />
         <tli id="T603" time="305.5" type="appl" />
         <tli id="T604" time="306.0" type="appl" />
         <tli id="T605" time="306.5" type="appl" />
         <tli id="T606" time="307.0" type="appl" />
         <tli id="T607" time="307.5" type="appl" />
         <tli id="T608" time="308.0" type="appl" />
         <tli id="T609" time="308.5" type="appl" />
         <tli id="T610" time="309.0" type="appl" />
         <tli id="T611" time="309.5" type="appl" />
         <tli id="T612" time="310.0" type="appl" />
         <tli id="T613" time="310.5" type="appl" />
         <tli id="T614" time="311.0" type="appl" />
         <tli id="T615" time="311.5" type="appl" />
         <tli id="T616" time="312.0" type="appl" />
         <tli id="T617" time="312.5" type="appl" />
         <tli id="T618" time="313.0" type="appl" />
         <tli id="T619" time="313.5" type="appl" />
         <tli id="T620" time="314.0" type="appl" />
         <tli id="T621" time="314.5" type="appl" />
         <tli id="T622" time="315.0" type="appl" />
         <tli id="T623" time="315.5" type="appl" />
         <tli id="T624" time="316.0" type="appl" />
         <tli id="T625" time="316.5" type="appl" />
         <tli id="T626" time="317.0" type="appl" />
         <tli id="T627" time="317.5" type="appl" />
         <tli id="T628" time="318.0" type="appl" />
         <tli id="T629" time="318.5" type="appl" />
         <tli id="T630" time="319.0" type="appl" />
         <tli id="T631" time="319.5" type="appl" />
         <tli id="T632" time="320.0" type="appl" />
         <tli id="T633" time="320.5" type="appl" />
         <tli id="T634" time="321.0" type="appl" />
         <tli id="T635" time="321.5" type="appl" />
         <tli id="T636" time="322.0" type="appl" />
         <tli id="T637" time="322.5" type="appl" />
         <tli id="T638" time="323.0" type="appl" />
         <tli id="T639" time="323.5" type="appl" />
         <tli id="T640" time="324.0" type="appl" />
         <tli id="T641" time="324.5" type="appl" />
         <tli id="T642" time="325.0" type="appl" />
         <tli id="T643" time="325.5" type="appl" />
         <tli id="T644" time="326.0" type="appl" />
         <tli id="T645" time="326.5" type="appl" />
         <tli id="T646" time="327.0" type="appl" />
         <tli id="T647" time="327.5" type="appl" />
         <tli id="T648" time="328.0" type="appl" />
         <tli id="T649" time="328.5" type="appl" />
         <tli id="T650" time="329.0" type="appl" />
         <tli id="T651" time="329.5" type="appl" />
         <tli id="T652" time="330.0" type="appl" />
         <tli id="T653" time="330.5" type="appl" />
         <tli id="T654" time="331.0" type="appl" />
         <tli id="T655" time="331.5" type="appl" />
         <tli id="T656" time="332.0" type="appl" />
         <tli id="T657" time="332.5" type="appl" />
         <tli id="T658" time="333.0" type="appl" />
         <tli id="T659" time="333.5" type="appl" />
         <tli id="T660" time="334.0" type="appl" />
         <tli id="T661" time="334.5" type="appl" />
         <tli id="T662" time="335.0" type="appl" />
         <tli id="T663" time="335.5" type="appl" />
         <tli id="T664" time="336.0" type="appl" />
         <tli id="T665" time="336.5" type="appl" />
         <tli id="T666" time="337.0" type="appl" />
         <tli id="T667" time="337.5" type="appl" />
         <tli id="T668" time="338.0" type="appl" />
         <tli id="T669" time="338.5" type="appl" />
         <tli id="T670" time="339.0" type="appl" />
         <tli id="T671" time="339.5" type="appl" />
         <tli id="T672" time="340.0" type="appl" />
         <tli id="T673" time="340.5" type="appl" />
         <tli id="T674" time="341.0" type="appl" />
         <tli id="T675" time="341.5" type="appl" />
         <tli id="T676" time="342.0" type="appl" />
         <tli id="T677" time="342.5" type="appl" />
         <tli id="T678" time="343.0" type="appl" />
         <tli id="T679" time="343.5" type="appl" />
         <tli id="T680" time="344.0" type="appl" />
         <tli id="T681" time="344.5" type="appl" />
         <tli id="T682" time="345.0" type="appl" />
         <tli id="T683" time="345.5" type="appl" />
         <tli id="T684" time="346.0" type="appl" />
         <tli id="T685" time="346.5" type="appl" />
         <tli id="T686" time="347.0" type="appl" />
         <tli id="T687" time="347.5" type="appl" />
         <tli id="T688" time="348.0" type="appl" />
         <tli id="T689" time="348.5" type="appl" />
         <tli id="T690" time="349.0" type="appl" />
         <tli id="T691" time="349.5" type="appl" />
         <tli id="T692" time="350.0" type="appl" />
         <tli id="T693" time="350.5" type="appl" />
         <tli id="T694" time="351.0" type="appl" />
         <tli id="T695" time="351.5" type="appl" />
         <tli id="T696" time="352.0" type="appl" />
         <tli id="T697" time="352.5" type="appl" />
         <tli id="T698" time="353.0" type="appl" />
         <tli id="T699" time="353.5" type="appl" />
         <tli id="T700" time="354.0" type="appl" />
         <tli id="T701" time="354.5" type="appl" />
         <tli id="T702" time="355.0" type="appl" />
         <tli id="T703" time="355.5" type="appl" />
         <tli id="T704" time="356.0" type="appl" />
         <tli id="T705" time="356.5" type="appl" />
         <tli id="T706" time="357.0" type="appl" />
         <tli id="T707" time="357.5" type="appl" />
         <tli id="T708" time="358.0" type="appl" />
         <tli id="T709" time="358.5" type="appl" />
         <tli id="T710" time="359.0" type="appl" />
         <tli id="T711" time="359.5" type="appl" />
         <tli id="T712" time="360.0" type="appl" />
         <tli id="T713" time="360.5" type="appl" />
         <tli id="T714" time="361.0" type="appl" />
         <tli id="T715" time="361.5" type="appl" />
         <tli id="T716" time="362.0" type="appl" />
         <tli id="T717" time="362.5" type="appl" />
         <tli id="T718" time="363.0" type="appl" />
         <tli id="T719" time="363.5" type="appl" />
         <tli id="T720" time="364.0" type="appl" />
         <tli id="T721" time="364.5" type="appl" />
         <tli id="T722" time="365.0" type="appl" />
         <tli id="T723" time="365.5" type="appl" />
         <tli id="T724" time="366.0" type="appl" />
         <tli id="T725" time="366.5" type="appl" />
         <tli id="T726" time="367.0" type="appl" />
         <tli id="T727" time="367.5" type="appl" />
         <tli id="T728" time="368.0" type="appl" />
         <tli id="T729" time="368.5" type="appl" />
         <tli id="T730" time="369.0" type="appl" />
         <tli id="T731" time="369.5" type="appl" />
         <tli id="T732" time="370.0" type="appl" />
         <tli id="T733" time="370.5" type="appl" />
         <tli id="T734" time="371.0" type="appl" />
         <tli id="T735" time="371.5" type="appl" />
         <tli id="T736" time="372.0" type="appl" />
         <tli id="T737" time="372.5" type="appl" />
         <tli id="T738" time="373.0" type="appl" />
         <tli id="T739" time="373.5" type="appl" />
         <tli id="T740" time="374.0" type="appl" />
         <tli id="T741" time="374.5" type="appl" />
         <tli id="T742" time="375.0" type="appl" />
         <tli id="T743" time="375.5" type="appl" />
         <tli id="T744" time="376.0" type="appl" />
         <tli id="T745" time="376.5" type="appl" />
         <tli id="T746" time="377.0" type="appl" />
         <tli id="T747" time="377.5" type="appl" />
         <tli id="T748" time="378.0" type="appl" />
         <tli id="T749" time="378.5" type="appl" />
         <tli id="T750" time="379.0" type="appl" />
         <tli id="T751" time="379.5" type="appl" />
         <tli id="T752" time="380.0" type="appl" />
         <tli id="T753" time="380.5" type="appl" />
         <tli id="T754" time="381.0" type="appl" />
         <tli id="T755" time="381.5" type="appl" />
         <tli id="T756" time="382.0" type="appl" />
         <tli id="T757" time="382.5" type="appl" />
         <tli id="T758" time="383.0" type="appl" />
         <tli id="T759" time="383.5" type="appl" />
         <tli id="T760" time="384.0" type="appl" />
         <tli id="T761" time="384.5" type="appl" />
         <tli id="T762" time="385.0" type="appl" />
         <tli id="T763" time="385.5" type="appl" />
         <tli id="T764" time="386.0" type="appl" />
         <tli id="T765" time="386.5" type="appl" />
         <tli id="T766" time="387.0" type="appl" />
         <tli id="T767" time="387.5" type="appl" />
         <tli id="T768" time="388.0" type="appl" />
         <tli id="T769" time="388.5" type="appl" />
         <tli id="T770" time="389.0" type="appl" />
         <tli id="T771" time="389.5" type="appl" />
         <tli id="T772" time="390.0" type="appl" />
         <tli id="T773" time="390.5" type="appl" />
         <tli id="T774" time="391.0" type="appl" />
         <tli id="T775" time="391.5" type="appl" />
         <tli id="T776" time="392.0" type="appl" />
         <tli id="T777" time="392.5" type="appl" />
         <tli id="T778" time="393.0" type="appl" />
         <tli id="T779" time="393.5" type="appl" />
         <tli id="T780" time="394.0" type="appl" />
         <tli id="T781" time="394.5" type="appl" />
         <tli id="T782" time="395.0" type="appl" />
         <tli id="T783" time="395.5" type="appl" />
         <tli id="T784" time="396.0" type="appl" />
         <tli id="T785" time="396.5" type="appl" />
         <tli id="T786" time="397.0" type="appl" />
         <tli id="T787" time="397.5" type="appl" />
         <tli id="T788" time="398.0" type="appl" />
         <tli id="T789" time="398.5" type="appl" />
         <tli id="T790" time="399.0" type="appl" />
         <tli id="T791" time="399.5" type="appl" />
         <tli id="T792" time="400.0" type="appl" />
         <tli id="T793" time="400.5" type="appl" />
         <tli id="T794" time="401.0" type="appl" />
         <tli id="T795" time="401.5" type="appl" />
         <tli id="T796" time="402.0" type="appl" />
         <tli id="T797" time="402.5" type="appl" />
         <tli id="T798" time="403.0" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PoMA"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T798" id="Seg_0" n="sc" s="T0">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Dʼe</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">U͡ol</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">ɨraːktaːgɨ</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">olorbuta</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T7" id="Seg_17" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">Ulkatɨgar</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">Umnahɨt</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">paːsɨnajdaːk</ts>
                  <nts id="Seg_26" n="HIAT:ip">.</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T23" id="Seg_29" n="HIAT:u" s="T7">
                  <ts e="T8" id="Seg_31" n="HIAT:w" s="T7">Umnahɨt</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_34" n="HIAT:w" s="T8">paːsɨnaj</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_37" n="HIAT:w" s="T9">bu</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_40" n="HIAT:w" s="T10">ɨraːktaːgɨga</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_43" n="HIAT:w" s="T11">üleleːn</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_46" n="HIAT:w" s="T12">harsi͡erdaːŋŋɨ</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_49" n="HIAT:w" s="T13">karaŋattan</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_52" n="HIAT:w" s="T14">ki͡ehe</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_55" n="HIAT:w" s="T15">karaŋaga</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_58" n="HIAT:w" s="T16">di͡eri</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_61" n="HIAT:w" s="T17">biːr</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_64" n="HIAT:w" s="T18">lu͡osku</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_67" n="HIAT:w" s="T19">burduk</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_70" n="HIAT:w" s="T20">ihin</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_73" n="HIAT:w" s="T21">üleliːr</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_76" n="HIAT:w" s="T22">ebit</ts>
                  <nts id="Seg_77" n="HIAT:ip">.</nts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_80" n="HIAT:u" s="T23">
                  <ts e="T24" id="Seg_82" n="HIAT:w" s="T23">Manan</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_85" n="HIAT:w" s="T24">iːtten</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_88" n="HIAT:w" s="T25">olorollor</ts>
                  <nts id="Seg_89" n="HIAT:ip">.</nts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T31" id="Seg_92" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_94" n="HIAT:w" s="T26">Umnahɨt</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_97" n="HIAT:w" s="T27">paːsɨnaj</ts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_100" n="HIAT:w" s="T28">biːr</ts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_103" n="HIAT:w" s="T29">kɨːs</ts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_106" n="HIAT:w" s="T30">ogoloːk</ts>
                  <nts id="Seg_107" n="HIAT:ip">.</nts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T36" id="Seg_110" n="HIAT:u" s="T31">
                  <ts e="T32" id="Seg_112" n="HIAT:w" s="T31">Dʼe</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_115" n="HIAT:w" s="T32">biːrde</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_118" n="HIAT:w" s="T33">U͡ol</ts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_121" n="HIAT:w" s="T34">ɨraːktaːgɨ</ts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_124" n="HIAT:w" s="T35">diːr</ts>
                  <nts id="Seg_125" n="HIAT:ip">:</nts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T45" id="Seg_128" n="HIAT:u" s="T36">
                  <nts id="Seg_129" n="HIAT:ip">"</nts>
                  <ts e="T37" id="Seg_131" n="HIAT:w" s="T36">Kaja</ts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_134" n="HIAT:w" s="T37">dʼe</ts>
                  <nts id="Seg_135" n="HIAT:ip">,</nts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_138" n="HIAT:w" s="T38">Umnahɨt</ts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_141" n="HIAT:w" s="T39">paːsɨnaj</ts>
                  <nts id="Seg_142" n="HIAT:ip">,</nts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_145" n="HIAT:w" s="T40">töhö</ts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_148" n="HIAT:w" s="T41">öjdöːk</ts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_151" n="HIAT:w" s="T42">kihi</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_154" n="HIAT:w" s="T43">töröːn</ts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_157" n="HIAT:w" s="T44">tönünnüŋ</ts>
                  <nts id="Seg_158" n="HIAT:ip">?</nts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T50" id="Seg_161" n="HIAT:u" s="T45">
                  <ts e="T46" id="Seg_163" n="HIAT:w" s="T45">Taːburunna</ts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_166" n="HIAT:w" s="T46">taːj</ts>
                  <nts id="Seg_167" n="HIAT:ip">,</nts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_170" n="HIAT:w" s="T47">minnʼigesten</ts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_173" n="HIAT:w" s="T48">minnʼiges</ts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_176" n="HIAT:w" s="T49">tu͡oguj</ts>
                  <nts id="Seg_177" n="HIAT:ip">?</nts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T55" id="Seg_180" n="HIAT:u" s="T50">
                  <ts e="T51" id="Seg_182" n="HIAT:w" s="T50">Harsi͡erda</ts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_185" n="HIAT:w" s="T51">kelen</ts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_188" n="HIAT:w" s="T52">togus</ts>
                  <nts id="Seg_189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_191" n="HIAT:w" s="T53">čaːska</ts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_194" n="HIAT:w" s="T54">haŋaraːr</ts>
                  <nts id="Seg_195" n="HIAT:ip">.</nts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T62" id="Seg_198" n="HIAT:u" s="T55">
                  <ts e="T56" id="Seg_200" n="HIAT:w" s="T55">Onu</ts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_203" n="HIAT:w" s="T56">taːjbatakkɨna</ts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_206" n="HIAT:w" s="T57">ɨjɨːr</ts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_209" n="HIAT:w" s="T58">maspar</ts>
                  <nts id="Seg_210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_212" n="HIAT:w" s="T59">moːjdoːk</ts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_215" n="HIAT:w" s="T60">baskɨn</ts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_218" n="HIAT:w" s="T61">bɨhɨ͡am</ts>
                  <nts id="Seg_219" n="HIAT:ip">.</nts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T77" id="Seg_222" n="HIAT:u" s="T62">
                  <ts e="T63" id="Seg_224" n="HIAT:w" s="T62">Umnahɨt</ts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_227" n="HIAT:w" s="T63">paːsɨnaj</ts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_230" n="HIAT:w" s="T64">togo</ts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_233" n="HIAT:w" s="T65">da</ts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_236" n="HIAT:w" s="T66">öjö</ts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_239" n="HIAT:w" s="T67">kɨ͡ajbat</ts>
                  <nts id="Seg_240" n="HIAT:ip">,</nts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_243" n="HIAT:w" s="T68">minnʼiges</ts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_246" n="HIAT:w" s="T69">ahɨ</ts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_249" n="HIAT:w" s="T70">bileːkteːbet</ts>
                  <nts id="Seg_250" n="HIAT:ip">,</nts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_253" n="HIAT:w" s="T71">ɨraːktaːgɨ</ts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_256" n="HIAT:w" s="T72">aha</ts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_259" n="HIAT:w" s="T73">barɨta</ts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_262" n="HIAT:w" s="T74">minnʼiges</ts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_265" n="HIAT:w" s="T75">kurduk</ts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_268" n="HIAT:w" s="T76">gini͡eke</ts>
                  <nts id="Seg_269" n="HIAT:ip">.</nts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T81" id="Seg_272" n="HIAT:u" s="T77">
                  <ts e="T78" id="Seg_274" n="HIAT:w" s="T77">Dʼi͡etiger</ts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_277" n="HIAT:w" s="T78">kelen</ts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_280" n="HIAT:w" s="T79">ɨtana</ts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_283" n="HIAT:w" s="T80">olordo</ts>
                  <nts id="Seg_284" n="HIAT:ip">.</nts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T83" id="Seg_287" n="HIAT:u" s="T81">
                  <ts e="T82" id="Seg_289" n="HIAT:w" s="T81">Kɨːha</ts>
                  <nts id="Seg_290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_292" n="HIAT:w" s="T82">ɨjɨtar</ts>
                  <nts id="Seg_293" n="HIAT:ip">:</nts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T87" id="Seg_296" n="HIAT:u" s="T83">
                  <nts id="Seg_297" n="HIAT:ip">"</nts>
                  <ts e="T84" id="Seg_299" n="HIAT:w" s="T83">Kaja</ts>
                  <nts id="Seg_300" n="HIAT:ip">,</nts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_303" n="HIAT:w" s="T84">agaː</ts>
                  <nts id="Seg_304" n="HIAT:ip">,</nts>
                  <nts id="Seg_305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_307" n="HIAT:w" s="T85">togo</ts>
                  <nts id="Seg_308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_310" n="HIAT:w" s="T86">ɨtaːtɨn</ts>
                  <nts id="Seg_311" n="HIAT:ip">?</nts>
                  <nts id="Seg_312" n="HIAT:ip">"</nts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T93" id="Seg_315" n="HIAT:u" s="T87">
                  <nts id="Seg_316" n="HIAT:ip">"</nts>
                  <ts e="T88" id="Seg_318" n="HIAT:w" s="T87">Dʼe</ts>
                  <nts id="Seg_319" n="HIAT:ip">,</nts>
                  <nts id="Seg_320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_322" n="HIAT:w" s="T88">togojum</ts>
                  <nts id="Seg_323" n="HIAT:ip">,</nts>
                  <nts id="Seg_324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_326" n="HIAT:w" s="T89">hɨldʼɨbɨtɨm</ts>
                  <nts id="Seg_327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_329" n="HIAT:w" s="T90">hɨččak</ts>
                  <nts id="Seg_330" n="HIAT:ip">,</nts>
                  <nts id="Seg_331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_333" n="HIAT:w" s="T91">olorbutum</ts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_336" n="HIAT:w" s="T92">uhuga</ts>
                  <nts id="Seg_337" n="HIAT:ip">.</nts>
                  <nts id="Seg_338" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T97" id="Seg_340" n="HIAT:u" s="T93">
                  <ts e="T94" id="Seg_342" n="HIAT:w" s="T93">U͡ol</ts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_345" n="HIAT:w" s="T94">ɨraːktaːgɨ</ts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_348" n="HIAT:w" s="T95">taːburuːn</ts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_351" n="HIAT:w" s="T96">bi͡erde</ts>
                  <nts id="Seg_352" n="HIAT:ip">.</nts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T102" id="Seg_355" n="HIAT:u" s="T97">
                  <ts e="T98" id="Seg_357" n="HIAT:w" s="T97">Taːjbatakpɨna</ts>
                  <nts id="Seg_358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_360" n="HIAT:w" s="T98">moːjdoːk</ts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_363" n="HIAT:w" s="T99">baspɨn</ts>
                  <nts id="Seg_364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_366" n="HIAT:w" s="T100">bɨhɨ͡ak</ts>
                  <nts id="Seg_367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_369" n="HIAT:w" s="T101">bu͡olla</ts>
                  <nts id="Seg_370" n="HIAT:ip">.</nts>
                  <nts id="Seg_371" n="HIAT:ip">"</nts>
                  <nts id="Seg_372" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T105" id="Seg_374" n="HIAT:u" s="T102">
                  <nts id="Seg_375" n="HIAT:ip">"</nts>
                  <ts e="T103" id="Seg_377" n="HIAT:w" s="T102">Tugu</ts>
                  <nts id="Seg_378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_380" n="HIAT:w" s="T103">taːttarar</ts>
                  <nts id="Seg_381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_383" n="HIAT:w" s="T104">ol</ts>
                  <nts id="Seg_384" n="HIAT:ip">?</nts>
                  <nts id="Seg_385" n="HIAT:ip">"</nts>
                  <nts id="Seg_386" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T110" id="Seg_388" n="HIAT:u" s="T105">
                  <nts id="Seg_389" n="HIAT:ip">"</nts>
                  <nts id="Seg_390" n="HIAT:ip">"</nts>
                  <ts e="T106" id="Seg_392" n="HIAT:w" s="T105">Minnʼigesten</ts>
                  <nts id="Seg_393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_395" n="HIAT:w" s="T106">minnʼiges</ts>
                  <nts id="Seg_396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_398" n="HIAT:w" s="T107">tu͡oguj</ts>
                  <nts id="Seg_399" n="HIAT:ip">"</nts>
                  <nts id="Seg_400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_402" n="HIAT:w" s="T108">diːr</ts>
                  <nts id="Seg_403" n="HIAT:ip">"</nts>
                  <nts id="Seg_404" n="HIAT:ip">,</nts>
                  <nts id="Seg_405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_407" n="HIAT:w" s="T109">di͡ebit</ts>
                  <nts id="Seg_408" n="HIAT:ip">.</nts>
                  <nts id="Seg_409" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T116" id="Seg_411" n="HIAT:u" s="T110">
                  <nts id="Seg_412" n="HIAT:ip">"</nts>
                  <ts e="T111" id="Seg_414" n="HIAT:w" s="T110">Eː</ts>
                  <nts id="Seg_415" n="HIAT:ip">,</nts>
                  <nts id="Seg_416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_418" n="HIAT:w" s="T111">agaː</ts>
                  <nts id="Seg_419" n="HIAT:ip">,</nts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_422" n="HIAT:w" s="T112">tuhata</ts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_425" n="HIAT:w" s="T113">hu͡okka</ts>
                  <nts id="Seg_426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_428" n="HIAT:w" s="T114">togo</ts>
                  <nts id="Seg_429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_431" n="HIAT:w" s="T115">ɨtɨːgɨn</ts>
                  <nts id="Seg_432" n="HIAT:ip">.</nts>
                  <nts id="Seg_433" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T118" id="Seg_435" n="HIAT:u" s="T116">
                  <ts e="T117" id="Seg_437" n="HIAT:w" s="T116">Utujaːktaː</ts>
                  <nts id="Seg_438" n="HIAT:ip">,</nts>
                  <nts id="Seg_439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_441" n="HIAT:w" s="T117">hɨnnʼan</ts>
                  <nts id="Seg_442" n="HIAT:ip">.</nts>
                  <nts id="Seg_443" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T124" id="Seg_445" n="HIAT:u" s="T118">
                  <nts id="Seg_446" n="HIAT:ip">"</nts>
                  <ts e="T119" id="Seg_448" n="HIAT:w" s="T118">Minnʼigesten</ts>
                  <nts id="Seg_449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_451" n="HIAT:w" s="T119">minnʼigehi</ts>
                  <nts id="Seg_452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_454" n="HIAT:w" s="T120">biler</ts>
                  <nts id="Seg_455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_457" n="HIAT:w" s="T121">hirim</ts>
                  <nts id="Seg_458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_460" n="HIAT:w" s="T122">biːr</ts>
                  <nts id="Seg_461" n="HIAT:ip">"</nts>
                  <nts id="Seg_462" n="HIAT:ip">,</nts>
                  <nts id="Seg_463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_465" n="HIAT:w" s="T123">di͡er</ts>
                  <nts id="Seg_466" n="HIAT:ip">.</nts>
                  <nts id="Seg_467" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T144" id="Seg_469" n="HIAT:u" s="T124">
                  <nts id="Seg_470" n="HIAT:ip">"</nts>
                  <ts e="T125" id="Seg_472" n="HIAT:w" s="T124">Harsi͡erdaːŋŋɨ</ts>
                  <nts id="Seg_473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_475" n="HIAT:w" s="T125">karaŋattan</ts>
                  <nts id="Seg_476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_478" n="HIAT:w" s="T126">ki͡eheːŋŋi</ts>
                  <nts id="Seg_479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_481" n="HIAT:w" s="T127">karaŋaga</ts>
                  <nts id="Seg_482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_484" n="HIAT:w" s="T128">di͡eri</ts>
                  <nts id="Seg_485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_487" n="HIAT:w" s="T129">üleleːn</ts>
                  <nts id="Seg_488" n="HIAT:ip">,</nts>
                  <nts id="Seg_489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_491" n="HIAT:w" s="T130">en</ts>
                  <nts id="Seg_492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_494" n="HIAT:w" s="T131">haːkkɨn-kirgin</ts>
                  <nts id="Seg_495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_497" n="HIAT:w" s="T132">ɨraːstaːn</ts>
                  <nts id="Seg_498" n="HIAT:ip">,</nts>
                  <nts id="Seg_499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_501" n="HIAT:w" s="T133">biːr</ts>
                  <nts id="Seg_502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_504" n="HIAT:w" s="T134">lu͡osku</ts>
                  <nts id="Seg_505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_507" n="HIAT:w" s="T135">burdugu</ts>
                  <nts id="Seg_508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_510" n="HIAT:w" s="T136">iltekpine</ts>
                  <nts id="Seg_511" n="HIAT:ip">,</nts>
                  <nts id="Seg_512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_514" n="HIAT:w" s="T137">emeːksinim</ts>
                  <nts id="Seg_515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_517" n="HIAT:w" s="T138">bu͡or</ts>
                  <nts id="Seg_518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_520" n="HIAT:w" s="T139">golomo</ts>
                  <nts id="Seg_521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_523" n="HIAT:w" s="T140">dʼi͡etin</ts>
                  <nts id="Seg_524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_526" n="HIAT:w" s="T141">otunnagɨna</ts>
                  <nts id="Seg_527" n="HIAT:ip">,</nts>
                  <nts id="Seg_528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_530" n="HIAT:w" s="T142">munnum</ts>
                  <nts id="Seg_531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_533" n="HIAT:w" s="T143">hɨlɨjar</ts>
                  <nts id="Seg_534" n="HIAT:ip">.</nts>
                  <nts id="Seg_535" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T151" id="Seg_537" n="HIAT:u" s="T144">
                  <ts e="T145" id="Seg_539" n="HIAT:w" s="T144">Ol</ts>
                  <nts id="Seg_540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_542" n="HIAT:w" s="T145">burdukkaːnɨnan</ts>
                  <nts id="Seg_543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_545" n="HIAT:w" s="T146">toloŋku͡o</ts>
                  <nts id="Seg_546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_548" n="HIAT:w" s="T147">oŋordoguna</ts>
                  <nts id="Seg_549" n="HIAT:ip">,</nts>
                  <nts id="Seg_550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_552" n="HIAT:w" s="T148">ahaːbakka</ts>
                  <nts id="Seg_553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_555" n="HIAT:w" s="T149">da</ts>
                  <nts id="Seg_556" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_558" n="HIAT:w" s="T150">utujabɨn</ts>
                  <nts id="Seg_559" n="HIAT:ip">.</nts>
                  <nts id="Seg_560" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T158" id="Seg_562" n="HIAT:u" s="T151">
                  <ts e="T152" id="Seg_564" n="HIAT:w" s="T151">Onon</ts>
                  <nts id="Seg_565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_567" n="HIAT:w" s="T152">minnʼigesten</ts>
                  <nts id="Seg_568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_570" n="HIAT:w" s="T153">minnʼiges</ts>
                  <nts id="Seg_571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_573" n="HIAT:w" s="T154">utujar</ts>
                  <nts id="Seg_574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_576" n="HIAT:w" s="T155">uː</ts>
                  <nts id="Seg_577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_579" n="HIAT:w" s="T156">bu͡olu͡oktaːk</ts>
                  <nts id="Seg_580" n="HIAT:ip">"</nts>
                  <nts id="Seg_581" n="HIAT:ip">,</nts>
                  <nts id="Seg_582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_584" n="HIAT:w" s="T157">di͡er</ts>
                  <nts id="Seg_585" n="HIAT:ip">.</nts>
                  <nts id="Seg_586" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T167" id="Seg_588" n="HIAT:u" s="T158">
                  <ts e="T159" id="Seg_590" n="HIAT:w" s="T158">Kini</ts>
                  <nts id="Seg_591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_593" n="HIAT:w" s="T159">onton</ts>
                  <nts id="Seg_594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_596" n="HIAT:w" s="T160">da</ts>
                  <nts id="Seg_597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_599" n="HIAT:w" s="T161">ordugu</ts>
                  <nts id="Seg_600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_602" n="HIAT:w" s="T162">bilere</ts>
                  <nts id="Seg_603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_605" n="HIAT:w" s="T163">bu͡olu͡o</ts>
                  <nts id="Seg_606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_608" n="HIAT:w" s="T164">du͡o</ts>
                  <nts id="Seg_609" n="HIAT:ip">"</nts>
                  <nts id="Seg_610" n="HIAT:ip">,</nts>
                  <nts id="Seg_611" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_613" n="HIAT:w" s="T165">di͡ebit</ts>
                  <nts id="Seg_614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_616" n="HIAT:w" s="T166">kɨːha</ts>
                  <nts id="Seg_617" n="HIAT:ip">.</nts>
                  <nts id="Seg_618" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T170" id="Seg_620" n="HIAT:u" s="T167">
                  <ts e="T168" id="Seg_622" n="HIAT:w" s="T167">Ogonnʼor</ts>
                  <nts id="Seg_623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_625" n="HIAT:w" s="T168">utujan</ts>
                  <nts id="Seg_626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_628" n="HIAT:w" s="T169">kaːlla</ts>
                  <nts id="Seg_629" n="HIAT:ip">.</nts>
                  <nts id="Seg_630" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T177" id="Seg_632" n="HIAT:u" s="T170">
                  <ts e="T171" id="Seg_634" n="HIAT:w" s="T170">Harsi͡erda</ts>
                  <nts id="Seg_635" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_637" n="HIAT:w" s="T171">erde</ts>
                  <nts id="Seg_638" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_640" n="HIAT:w" s="T172">turan</ts>
                  <nts id="Seg_641" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_643" n="HIAT:w" s="T173">togus</ts>
                  <nts id="Seg_644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_646" n="HIAT:w" s="T174">čaːska</ts>
                  <nts id="Seg_647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_649" n="HIAT:w" s="T175">ɨraːktɨːgɨga</ts>
                  <nts id="Seg_650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_652" n="HIAT:w" s="T176">tijer</ts>
                  <nts id="Seg_653" n="HIAT:ip">.</nts>
                  <nts id="Seg_654" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T180" id="Seg_656" n="HIAT:u" s="T177">
                  <ts e="T178" id="Seg_658" n="HIAT:w" s="T177">U͡ol</ts>
                  <nts id="Seg_659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_661" n="HIAT:w" s="T178">ɨraːktaːgɨ</ts>
                  <nts id="Seg_662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_664" n="HIAT:w" s="T179">ɨjɨtar</ts>
                  <nts id="Seg_665" n="HIAT:ip">:</nts>
                  <nts id="Seg_666" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T183" id="Seg_668" n="HIAT:u" s="T180">
                  <nts id="Seg_669" n="HIAT:ip">"</nts>
                  <ts e="T181" id="Seg_671" n="HIAT:w" s="T180">Kaja</ts>
                  <nts id="Seg_672" n="HIAT:ip">,</nts>
                  <nts id="Seg_673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_675" n="HIAT:w" s="T181">taːjdɨŋ</ts>
                  <nts id="Seg_676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_678" n="HIAT:w" s="T182">du͡o</ts>
                  <nts id="Seg_679" n="HIAT:ip">?</nts>
                  <nts id="Seg_680" n="HIAT:ip">"</nts>
                  <nts id="Seg_681" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T187" id="Seg_683" n="HIAT:u" s="T183">
                  <nts id="Seg_684" n="HIAT:ip">"</nts>
                  <ts e="T184" id="Seg_686" n="HIAT:w" s="T183">Eː</ts>
                  <nts id="Seg_687" n="HIAT:ip">,</nts>
                  <nts id="Seg_688" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_690" n="HIAT:w" s="T184">taːjarga</ts>
                  <nts id="Seg_691" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_693" n="HIAT:w" s="T185">talɨ</ts>
                  <nts id="Seg_694" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_696" n="HIAT:w" s="T186">gɨnnɨm</ts>
                  <nts id="Seg_697" n="HIAT:ip">.</nts>
                  <nts id="Seg_698" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T192" id="Seg_700" n="HIAT:u" s="T187">
                  <ts e="T188" id="Seg_702" n="HIAT:w" s="T187">Höbö</ts>
                  <nts id="Seg_703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_705" n="HIAT:w" s="T188">duː</ts>
                  <nts id="Seg_706" n="HIAT:ip">,</nts>
                  <nts id="Seg_707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_709" n="HIAT:w" s="T189">hɨːhata</ts>
                  <nts id="Seg_710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_712" n="HIAT:w" s="T190">duː</ts>
                  <nts id="Seg_713" n="HIAT:ip">"</nts>
                  <nts id="Seg_714" n="HIAT:ip">,</nts>
                  <nts id="Seg_715" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_717" n="HIAT:w" s="T191">diːr</ts>
                  <nts id="Seg_718" n="HIAT:ip">.</nts>
                  <nts id="Seg_719" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T194" id="Seg_721" n="HIAT:u" s="T192">
                  <nts id="Seg_722" n="HIAT:ip">"</nts>
                  <ts e="T193" id="Seg_724" n="HIAT:w" s="T192">Dʼe</ts>
                  <nts id="Seg_725" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_727" n="HIAT:w" s="T193">tuguj</ts>
                  <nts id="Seg_728" n="HIAT:ip">?</nts>
                  <nts id="Seg_729" n="HIAT:ip">"</nts>
                  <nts id="Seg_730" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T214" id="Seg_732" n="HIAT:u" s="T194">
                  <nts id="Seg_733" n="HIAT:ip">"</nts>
                  <ts e="T195" id="Seg_735" n="HIAT:w" s="T194">Harsi͡erdaːŋŋɨ</ts>
                  <nts id="Seg_736" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_738" n="HIAT:w" s="T195">karaŋattan</ts>
                  <nts id="Seg_739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_741" n="HIAT:w" s="T196">ki͡eheːŋŋi</ts>
                  <nts id="Seg_742" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_744" n="HIAT:w" s="T197">karaŋaga</ts>
                  <nts id="Seg_745" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_747" n="HIAT:w" s="T198">di͡eri</ts>
                  <nts id="Seg_748" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_750" n="HIAT:w" s="T199">üleleːn</ts>
                  <nts id="Seg_751" n="HIAT:ip">,</nts>
                  <nts id="Seg_752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_754" n="HIAT:w" s="T200">en</ts>
                  <nts id="Seg_755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_757" n="HIAT:w" s="T201">haːkkɨn-kirgin</ts>
                  <nts id="Seg_758" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_760" n="HIAT:w" s="T202">ɨraːstaːn</ts>
                  <nts id="Seg_761" n="HIAT:ip">,</nts>
                  <nts id="Seg_762" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_764" n="HIAT:w" s="T203">biːr</ts>
                  <nts id="Seg_765" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_767" n="HIAT:w" s="T204">lu͡osku</ts>
                  <nts id="Seg_768" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_770" n="HIAT:w" s="T205">burdugu</ts>
                  <nts id="Seg_771" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_773" n="HIAT:w" s="T206">iltekpine</ts>
                  <nts id="Seg_774" n="HIAT:ip">,</nts>
                  <nts id="Seg_775" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_777" n="HIAT:w" s="T207">emeːksinim</ts>
                  <nts id="Seg_778" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_780" n="HIAT:w" s="T208">bu͡or</ts>
                  <nts id="Seg_781" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_783" n="HIAT:w" s="T209">golomo</ts>
                  <nts id="Seg_784" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_786" n="HIAT:w" s="T210">dʼi͡etin</ts>
                  <nts id="Seg_787" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_789" n="HIAT:w" s="T211">otunnagɨna</ts>
                  <nts id="Seg_790" n="HIAT:ip">,</nts>
                  <nts id="Seg_791" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_793" n="HIAT:w" s="T212">munnum</ts>
                  <nts id="Seg_794" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_796" n="HIAT:w" s="T213">hɨlɨjar</ts>
                  <nts id="Seg_797" n="HIAT:ip">.</nts>
                  <nts id="Seg_798" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T221" id="Seg_800" n="HIAT:u" s="T214">
                  <ts e="T215" id="Seg_802" n="HIAT:w" s="T214">Ol</ts>
                  <nts id="Seg_803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_805" n="HIAT:w" s="T215">burdukkaːnɨnan</ts>
                  <nts id="Seg_806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_808" n="HIAT:w" s="T216">toloŋku͡o</ts>
                  <nts id="Seg_809" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_811" n="HIAT:w" s="T217">oŋordoguna</ts>
                  <nts id="Seg_812" n="HIAT:ip">,</nts>
                  <nts id="Seg_813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_815" n="HIAT:w" s="T218">ahaːbakka</ts>
                  <nts id="Seg_816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_818" n="HIAT:w" s="T219">da</ts>
                  <nts id="Seg_819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_821" n="HIAT:w" s="T220">utujabɨn</ts>
                  <nts id="Seg_822" n="HIAT:ip">.</nts>
                  <nts id="Seg_823" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T227" id="Seg_825" n="HIAT:u" s="T221">
                  <ts e="T222" id="Seg_827" n="HIAT:w" s="T221">Onon</ts>
                  <nts id="Seg_828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_830" n="HIAT:w" s="T222">minnʼigesten</ts>
                  <nts id="Seg_831" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_833" n="HIAT:w" s="T223">minnʼiges</ts>
                  <nts id="Seg_834" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_836" n="HIAT:w" s="T224">utujar</ts>
                  <nts id="Seg_837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_839" n="HIAT:w" s="T225">uː</ts>
                  <nts id="Seg_840" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_842" n="HIAT:w" s="T226">bu͡olu͡oktaːk</ts>
                  <nts id="Seg_843" n="HIAT:ip">.</nts>
                  <nts id="Seg_844" n="HIAT:ip">"</nts>
                  <nts id="Seg_845" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T230" id="Seg_847" n="HIAT:u" s="T227">
                  <ts e="T228" id="Seg_849" n="HIAT:w" s="T227">U͡ol</ts>
                  <nts id="Seg_850" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_852" n="HIAT:w" s="T228">ɨraːktaːgɨ</ts>
                  <nts id="Seg_853" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_855" n="HIAT:w" s="T229">diːr</ts>
                  <nts id="Seg_856" n="HIAT:ip">:</nts>
                  <nts id="Seg_857" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T235" id="Seg_859" n="HIAT:u" s="T230">
                  <nts id="Seg_860" n="HIAT:ip">"</nts>
                  <ts e="T231" id="Seg_862" n="HIAT:w" s="T230">Oː</ts>
                  <nts id="Seg_863" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_865" n="HIAT:w" s="T231">dʼe</ts>
                  <nts id="Seg_866" n="HIAT:ip">,</nts>
                  <nts id="Seg_867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_869" n="HIAT:w" s="T232">öjdöːk</ts>
                  <nts id="Seg_870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_872" n="HIAT:w" s="T233">kihigin</ts>
                  <nts id="Seg_873" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_875" n="HIAT:w" s="T234">ebit</ts>
                  <nts id="Seg_876" n="HIAT:ip">.</nts>
                  <nts id="Seg_877" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T238" id="Seg_879" n="HIAT:u" s="T235">
                  <ts e="T236" id="Seg_881" n="HIAT:w" s="T235">Kajdak</ts>
                  <nts id="Seg_882" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_884" n="HIAT:w" s="T236">inen-batan</ts>
                  <nts id="Seg_885" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_887" n="HIAT:w" s="T237">hɨtagɨn</ts>
                  <nts id="Seg_888" n="HIAT:ip">?</nts>
                  <nts id="Seg_889" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T246" id="Seg_891" n="HIAT:u" s="T238">
                  <ts e="T239" id="Seg_893" n="HIAT:w" s="T238">Össü͡ö</ts>
                  <nts id="Seg_894" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_896" n="HIAT:w" s="T239">taːj</ts>
                  <nts id="Seg_897" n="HIAT:ip">,</nts>
                  <nts id="Seg_898" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_900" n="HIAT:w" s="T240">orto</ts>
                  <nts id="Seg_901" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_903" n="HIAT:w" s="T241">dojduga</ts>
                  <nts id="Seg_904" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_906" n="HIAT:w" s="T242">türgenten</ts>
                  <nts id="Seg_907" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_909" n="HIAT:w" s="T243">türgen</ts>
                  <nts id="Seg_910" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_912" n="HIAT:w" s="T244">tu͡ok</ts>
                  <nts id="Seg_913" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_915" n="HIAT:w" s="T245">baːrɨj</ts>
                  <nts id="Seg_916" n="HIAT:ip">?</nts>
                  <nts id="Seg_917" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T251" id="Seg_919" n="HIAT:u" s="T246">
                  <ts e="T247" id="Seg_921" n="HIAT:w" s="T246">Harsi͡erda</ts>
                  <nts id="Seg_922" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_924" n="HIAT:w" s="T247">taːjbatakkɨna</ts>
                  <nts id="Seg_925" n="HIAT:ip">,</nts>
                  <nts id="Seg_926" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_928" n="HIAT:w" s="T248">moːjdoːk</ts>
                  <nts id="Seg_929" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_931" n="HIAT:w" s="T249">baskɨn</ts>
                  <nts id="Seg_932" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_934" n="HIAT:w" s="T250">bɨhabɨn</ts>
                  <nts id="Seg_935" n="HIAT:ip">.</nts>
                  <nts id="Seg_936" n="HIAT:ip">"</nts>
                  <nts id="Seg_937" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T255" id="Seg_939" n="HIAT:u" s="T251">
                  <ts e="T252" id="Seg_941" n="HIAT:w" s="T251">Ogonnʼor</ts>
                  <nts id="Seg_942" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_944" n="HIAT:w" s="T252">tugu</ts>
                  <nts id="Seg_945" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_947" n="HIAT:w" s="T253">da</ts>
                  <nts id="Seg_948" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_950" n="HIAT:w" s="T254">tobulbat</ts>
                  <nts id="Seg_951" n="HIAT:ip">.</nts>
                  <nts id="Seg_952" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T260" id="Seg_954" n="HIAT:u" s="T255">
                  <ts e="T256" id="Seg_956" n="HIAT:w" s="T255">Tijen</ts>
                  <nts id="Seg_957" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_959" n="HIAT:w" s="T256">ɨtana</ts>
                  <nts id="Seg_960" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_962" n="HIAT:w" s="T257">olordoguna</ts>
                  <nts id="Seg_963" n="HIAT:ip">,</nts>
                  <nts id="Seg_964" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_966" n="HIAT:w" s="T258">kɨːha</ts>
                  <nts id="Seg_967" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_969" n="HIAT:w" s="T259">ɨjɨtar</ts>
                  <nts id="Seg_970" n="HIAT:ip">.</nts>
                  <nts id="Seg_971" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T263" id="Seg_973" n="HIAT:u" s="T260">
                  <ts e="T261" id="Seg_975" n="HIAT:w" s="T260">Ogonnʼor</ts>
                  <nts id="Seg_976" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_978" n="HIAT:w" s="T261">innʼe-innʼe</ts>
                  <nts id="Seg_979" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_981" n="HIAT:w" s="T262">diːr</ts>
                  <nts id="Seg_982" n="HIAT:ip">.</nts>
                  <nts id="Seg_983" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T268" id="Seg_985" n="HIAT:u" s="T263">
                  <nts id="Seg_986" n="HIAT:ip">"</nts>
                  <ts e="T264" id="Seg_988" n="HIAT:w" s="T263">Tu͡ok</ts>
                  <nts id="Seg_989" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_991" n="HIAT:w" s="T264">tuhata</ts>
                  <nts id="Seg_992" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_994" n="HIAT:w" s="T265">hu͡okka</ts>
                  <nts id="Seg_995" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_997" n="HIAT:w" s="T266">ɨtɨːgɨn</ts>
                  <nts id="Seg_998" n="HIAT:ip">,</nts>
                  <nts id="Seg_999" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_1001" n="HIAT:w" s="T267">utuj</ts>
                  <nts id="Seg_1002" n="HIAT:ip">.</nts>
                  <nts id="Seg_1003" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T299" id="Seg_1005" n="HIAT:u" s="T268">
                  <ts e="T269" id="Seg_1007" n="HIAT:w" s="T268">Harsi͡erda</ts>
                  <nts id="Seg_1008" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_1010" n="HIAT:w" s="T269">eteːr</ts>
                  <nts id="Seg_1011" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1012" n="HIAT:ip">"</nts>
                  <ts e="T271" id="Seg_1014" n="HIAT:w" s="T270">türgenten</ts>
                  <nts id="Seg_1015" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_1017" n="HIAT:w" s="T271">türgen</ts>
                  <nts id="Seg_1018" n="HIAT:ip">"</nts>
                  <nts id="Seg_1019" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_1021" n="HIAT:w" s="T272">di͡er</ts>
                  <nts id="Seg_1022" n="HIAT:ip">,</nts>
                  <nts id="Seg_1023" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1024" n="HIAT:ip">"</nts>
                  <ts e="T274" id="Seg_1026" n="HIAT:w" s="T273">kuhagan</ts>
                  <nts id="Seg_1027" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_1029" n="HIAT:w" s="T274">buru͡olaːk</ts>
                  <nts id="Seg_1030" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_1032" n="HIAT:w" s="T275">bu͡or</ts>
                  <nts id="Seg_1033" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_1035" n="HIAT:w" s="T276">golomobuttan</ts>
                  <nts id="Seg_1036" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_1038" n="HIAT:w" s="T277">taksa</ts>
                  <nts id="Seg_1039" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_1041" n="HIAT:w" s="T278">kötön</ts>
                  <nts id="Seg_1042" n="HIAT:ip">,</nts>
                  <nts id="Seg_1043" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_1045" n="HIAT:w" s="T279">karakpɨn</ts>
                  <nts id="Seg_1046" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_1048" n="HIAT:w" s="T280">kastɨrɨta</ts>
                  <nts id="Seg_1049" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_1051" n="HIAT:w" s="T281">hoton</ts>
                  <nts id="Seg_1052" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_1054" n="HIAT:w" s="T282">oduːlaːn</ts>
                  <nts id="Seg_1055" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_1057" n="HIAT:w" s="T283">kördökpüne</ts>
                  <nts id="Seg_1058" n="HIAT:ip">,</nts>
                  <nts id="Seg_1059" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_1061" n="HIAT:w" s="T284">kallaːn</ts>
                  <nts id="Seg_1062" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_1064" n="HIAT:w" s="T285">di͡ek</ts>
                  <nts id="Seg_1065" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_1067" n="HIAT:w" s="T286">kantajdakpɨna</ts>
                  <nts id="Seg_1068" n="HIAT:ip">—</nts>
                  <nts id="Seg_1069" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_1071" n="HIAT:w" s="T287">kallaːn</ts>
                  <nts id="Seg_1072" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_1074" n="HIAT:w" s="T288">huluhun</ts>
                  <nts id="Seg_1075" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_1077" n="HIAT:w" s="T289">karagɨm</ts>
                  <nts id="Seg_1078" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_1080" n="HIAT:w" s="T290">aːga</ts>
                  <nts id="Seg_1081" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_1083" n="HIAT:w" s="T291">körör</ts>
                  <nts id="Seg_1084" n="HIAT:ip">,</nts>
                  <nts id="Seg_1085" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_1087" n="HIAT:w" s="T292">hir</ts>
                  <nts id="Seg_1088" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_1090" n="HIAT:w" s="T293">di͡ek</ts>
                  <nts id="Seg_1091" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1093" n="HIAT:w" s="T294">kördökpüne</ts>
                  <nts id="Seg_1094" n="HIAT:ip">—</nts>
                  <nts id="Seg_1095" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1097" n="HIAT:w" s="T295">hir</ts>
                  <nts id="Seg_1098" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_1100" n="HIAT:w" s="T296">otun-mahɨn</ts>
                  <nts id="Seg_1101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_1103" n="HIAT:w" s="T297">aːga</ts>
                  <nts id="Seg_1104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_1106" n="HIAT:w" s="T298">köröbün</ts>
                  <nts id="Seg_1107" n="HIAT:ip">.</nts>
                  <nts id="Seg_1108" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T304" id="Seg_1110" n="HIAT:u" s="T299">
                  <ts e="T300" id="Seg_1112" n="HIAT:w" s="T299">Karagɨm</ts>
                  <nts id="Seg_1113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_1115" n="HIAT:w" s="T300">ɨlarɨgar</ts>
                  <nts id="Seg_1116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_1118" n="HIAT:w" s="T301">kahan</ts>
                  <nts id="Seg_1119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1121" n="HIAT:w" s="T302">da</ts>
                  <nts id="Seg_1122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_1124" n="HIAT:w" s="T303">tiːjbeppin</ts>
                  <nts id="Seg_1125" n="HIAT:ip">.</nts>
                  <nts id="Seg_1126" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T310" id="Seg_1128" n="HIAT:u" s="T304">
                  <ts e="T305" id="Seg_1130" n="HIAT:w" s="T304">Onon</ts>
                  <nts id="Seg_1131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_1133" n="HIAT:w" s="T305">türgenten</ts>
                  <nts id="Seg_1134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_1136" n="HIAT:w" s="T306">türgen</ts>
                  <nts id="Seg_1137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_1139" n="HIAT:w" s="T307">kihi</ts>
                  <nts id="Seg_1140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1142" n="HIAT:w" s="T308">körüːte</ts>
                  <nts id="Seg_1143" n="HIAT:ip">"</nts>
                  <nts id="Seg_1144" n="HIAT:ip">,</nts>
                  <nts id="Seg_1145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_1147" n="HIAT:w" s="T309">di͡er</ts>
                  <nts id="Seg_1148" n="HIAT:ip">.</nts>
                  <nts id="Seg_1149" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T318" id="Seg_1151" n="HIAT:u" s="T310">
                  <ts e="T311" id="Seg_1153" n="HIAT:w" s="T310">Onton</ts>
                  <nts id="Seg_1154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_1156" n="HIAT:w" s="T311">ordugu</ts>
                  <nts id="Seg_1157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_1159" n="HIAT:w" s="T312">kini</ts>
                  <nts id="Seg_1160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_1162" n="HIAT:w" s="T313">bilere</ts>
                  <nts id="Seg_1163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_1165" n="HIAT:w" s="T314">bu͡olu͡o</ts>
                  <nts id="Seg_1166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1168" n="HIAT:w" s="T315">du͡o</ts>
                  <nts id="Seg_1169" n="HIAT:ip">"</nts>
                  <nts id="Seg_1170" n="HIAT:ip">,</nts>
                  <nts id="Seg_1171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_1173" n="HIAT:w" s="T316">diːr</ts>
                  <nts id="Seg_1174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_1176" n="HIAT:w" s="T317">kɨːs</ts>
                  <nts id="Seg_1177" n="HIAT:ip">.</nts>
                  <nts id="Seg_1178" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T321" id="Seg_1180" n="HIAT:u" s="T318">
                  <ts e="T319" id="Seg_1182" n="HIAT:w" s="T318">Ogonnʼor</ts>
                  <nts id="Seg_1183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_1185" n="HIAT:w" s="T319">utujan</ts>
                  <nts id="Seg_1186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1188" n="HIAT:w" s="T320">kaːlla</ts>
                  <nts id="Seg_1189" n="HIAT:ip">.</nts>
                  <nts id="Seg_1190" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T325" id="Seg_1192" n="HIAT:u" s="T321">
                  <ts e="T322" id="Seg_1194" n="HIAT:w" s="T321">Harsi͡erde</ts>
                  <nts id="Seg_1195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1197" n="HIAT:w" s="T322">turan</ts>
                  <nts id="Seg_1198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_1200" n="HIAT:w" s="T323">ɨraːktaːgɨtɨgar</ts>
                  <nts id="Seg_1201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1203" n="HIAT:w" s="T324">bardaga</ts>
                  <nts id="Seg_1204" n="HIAT:ip">.</nts>
                  <nts id="Seg_1205" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T328" id="Seg_1207" n="HIAT:u" s="T325">
                  <ts e="T326" id="Seg_1209" n="HIAT:w" s="T325">U͡ol</ts>
                  <nts id="Seg_1210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1212" n="HIAT:w" s="T326">ɨraːktaːgɨ</ts>
                  <nts id="Seg_1213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_1215" n="HIAT:w" s="T327">ɨjɨtar</ts>
                  <nts id="Seg_1216" n="HIAT:ip">:</nts>
                  <nts id="Seg_1217" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T331" id="Seg_1219" n="HIAT:u" s="T328">
                  <nts id="Seg_1220" n="HIAT:ip">"</nts>
                  <ts e="T329" id="Seg_1222" n="HIAT:w" s="T328">Kaja</ts>
                  <nts id="Seg_1223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_1225" n="HIAT:w" s="T329">taːjdɨŋ</ts>
                  <nts id="Seg_1226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_1228" n="HIAT:w" s="T330">du͡o</ts>
                  <nts id="Seg_1229" n="HIAT:ip">?</nts>
                  <nts id="Seg_1230" n="HIAT:ip">"</nts>
                  <nts id="Seg_1231" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T335" id="Seg_1233" n="HIAT:u" s="T331">
                  <nts id="Seg_1234" n="HIAT:ip">"</nts>
                  <ts e="T332" id="Seg_1236" n="HIAT:w" s="T331">Eː</ts>
                  <nts id="Seg_1237" n="HIAT:ip">,</nts>
                  <nts id="Seg_1238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_1240" n="HIAT:w" s="T332">taːjarga</ts>
                  <nts id="Seg_1241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_1243" n="HIAT:w" s="T333">talɨ</ts>
                  <nts id="Seg_1244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_1246" n="HIAT:w" s="T334">gɨnnɨm</ts>
                  <nts id="Seg_1247" n="HIAT:ip">.</nts>
                  <nts id="Seg_1248" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T340" id="Seg_1250" n="HIAT:u" s="T335">
                  <ts e="T336" id="Seg_1252" n="HIAT:w" s="T335">Höbö</ts>
                  <nts id="Seg_1253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_1255" n="HIAT:w" s="T336">duː</ts>
                  <nts id="Seg_1256" n="HIAT:ip">,</nts>
                  <nts id="Seg_1257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_1259" n="HIAT:w" s="T337">hɨːhata</ts>
                  <nts id="Seg_1260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_1262" n="HIAT:w" s="T338">duː</ts>
                  <nts id="Seg_1263" n="HIAT:ip">"</nts>
                  <nts id="Seg_1264" n="HIAT:ip">,</nts>
                  <nts id="Seg_1265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_1267" n="HIAT:w" s="T339">diːr</ts>
                  <nts id="Seg_1268" n="HIAT:ip">.</nts>
                  <nts id="Seg_1269" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T366" id="Seg_1271" n="HIAT:u" s="T340">
                  <nts id="Seg_1272" n="HIAT:ip">"</nts>
                  <ts e="T341" id="Seg_1274" n="HIAT:w" s="T340">Kuhagan</ts>
                  <nts id="Seg_1275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_1277" n="HIAT:w" s="T341">buru͡olaːk</ts>
                  <nts id="Seg_1278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1280" n="HIAT:w" s="T342">bu͡or</ts>
                  <nts id="Seg_1281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_1283" n="HIAT:w" s="T343">golomobuttan</ts>
                  <nts id="Seg_1284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_1286" n="HIAT:w" s="T344">taksa</ts>
                  <nts id="Seg_1287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_1289" n="HIAT:w" s="T345">kötön</ts>
                  <nts id="Seg_1290" n="HIAT:ip">,</nts>
                  <nts id="Seg_1291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_1293" n="HIAT:w" s="T346">karakpɨn</ts>
                  <nts id="Seg_1294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1296" n="HIAT:w" s="T347">kastɨrɨta</ts>
                  <nts id="Seg_1297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_1299" n="HIAT:w" s="T348">hoton</ts>
                  <nts id="Seg_1300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_1302" n="HIAT:w" s="T349">oduːlanan</ts>
                  <nts id="Seg_1303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_1305" n="HIAT:w" s="T350">kördökpüne</ts>
                  <nts id="Seg_1306" n="HIAT:ip">,</nts>
                  <nts id="Seg_1307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_1309" n="HIAT:w" s="T351">kallaːn</ts>
                  <nts id="Seg_1310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_1312" n="HIAT:w" s="T352">di͡ek</ts>
                  <nts id="Seg_1313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_1315" n="HIAT:w" s="T353">kantajdakpɨna</ts>
                  <nts id="Seg_1316" n="HIAT:ip">—</nts>
                  <nts id="Seg_1317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_1319" n="HIAT:w" s="T354">kallaːn</ts>
                  <nts id="Seg_1320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_1322" n="HIAT:w" s="T355">huluhun</ts>
                  <nts id="Seg_1323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_1325" n="HIAT:w" s="T356">karagɨm</ts>
                  <nts id="Seg_1326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_1328" n="HIAT:w" s="T357">aːga</ts>
                  <nts id="Seg_1329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_1331" n="HIAT:w" s="T358">körör</ts>
                  <nts id="Seg_1332" n="HIAT:ip">,</nts>
                  <nts id="Seg_1333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_1335" n="HIAT:w" s="T359">hir</ts>
                  <nts id="Seg_1336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_1338" n="HIAT:w" s="T360">di͡ek</ts>
                  <nts id="Seg_1339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_1341" n="HIAT:w" s="T361">kördökpüne</ts>
                  <nts id="Seg_1342" n="HIAT:ip">—</nts>
                  <nts id="Seg_1343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_1345" n="HIAT:w" s="T362">hir</ts>
                  <nts id="Seg_1346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_1348" n="HIAT:w" s="T363">otun-mahɨn</ts>
                  <nts id="Seg_1349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_1351" n="HIAT:w" s="T364">aːga</ts>
                  <nts id="Seg_1352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_1354" n="HIAT:w" s="T365">köröbün</ts>
                  <nts id="Seg_1355" n="HIAT:ip">.</nts>
                  <nts id="Seg_1356" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T371" id="Seg_1358" n="HIAT:u" s="T366">
                  <ts e="T367" id="Seg_1360" n="HIAT:w" s="T366">Karagɨm</ts>
                  <nts id="Seg_1361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_1363" n="HIAT:w" s="T367">ɨlarɨgar</ts>
                  <nts id="Seg_1364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_1366" n="HIAT:w" s="T368">kahan</ts>
                  <nts id="Seg_1367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_1369" n="HIAT:w" s="T369">da</ts>
                  <nts id="Seg_1370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_1372" n="HIAT:w" s="T370">tijbeppin</ts>
                  <nts id="Seg_1373" n="HIAT:ip">.</nts>
                  <nts id="Seg_1374" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T378" id="Seg_1376" n="HIAT:u" s="T371">
                  <ts e="T372" id="Seg_1378" n="HIAT:w" s="T371">Onon</ts>
                  <nts id="Seg_1379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_1381" n="HIAT:w" s="T372">türgenten</ts>
                  <nts id="Seg_1382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_1384" n="HIAT:w" s="T373">türgen</ts>
                  <nts id="Seg_1385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_1387" n="HIAT:w" s="T374">kihi</ts>
                  <nts id="Seg_1388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_1390" n="HIAT:w" s="T375">körüːte</ts>
                  <nts id="Seg_1391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_1393" n="HIAT:w" s="T376">ebit</ts>
                  <nts id="Seg_1394" n="HIAT:ip">"</nts>
                  <nts id="Seg_1395" n="HIAT:ip">,</nts>
                  <nts id="Seg_1396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378" id="Seg_1398" n="HIAT:w" s="T377">diːr</ts>
                  <nts id="Seg_1399" n="HIAT:ip">.</nts>
                  <nts id="Seg_1400" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T381" id="Seg_1402" n="HIAT:u" s="T378">
                  <ts e="T379" id="Seg_1404" n="HIAT:w" s="T378">U͡ol</ts>
                  <nts id="Seg_1405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_1407" n="HIAT:w" s="T379">ɨraːktaːgɨ</ts>
                  <nts id="Seg_1408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_1410" n="HIAT:w" s="T380">högör</ts>
                  <nts id="Seg_1411" n="HIAT:ip">:</nts>
                  <nts id="Seg_1412" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T387" id="Seg_1414" n="HIAT:u" s="T381">
                  <nts id="Seg_1415" n="HIAT:ip">"</nts>
                  <ts e="T382" id="Seg_1417" n="HIAT:w" s="T381">Oː</ts>
                  <nts id="Seg_1418" n="HIAT:ip">,</nts>
                  <nts id="Seg_1419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_1421" n="HIAT:w" s="T382">ogonnʼor</ts>
                  <nts id="Seg_1422" n="HIAT:ip">,</nts>
                  <nts id="Seg_1423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_1425" n="HIAT:w" s="T383">dʼe</ts>
                  <nts id="Seg_1426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_1428" n="HIAT:w" s="T384">öjdöːk</ts>
                  <nts id="Seg_1429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T386" id="Seg_1431" n="HIAT:w" s="T385">kihigin</ts>
                  <nts id="Seg_1432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_1434" n="HIAT:w" s="T386">ebit</ts>
                  <nts id="Seg_1435" n="HIAT:ip">.</nts>
                  <nts id="Seg_1436" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T397" id="Seg_1438" n="HIAT:u" s="T387">
                  <ts e="T388" id="Seg_1440" n="HIAT:w" s="T387">Dʼe</ts>
                  <nts id="Seg_1441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_1443" n="HIAT:w" s="T388">tu͡ok</ts>
                  <nts id="Seg_1444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_1446" n="HIAT:w" s="T389">da</ts>
                  <nts id="Seg_1447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T391" id="Seg_1449" n="HIAT:w" s="T390">üs</ts>
                  <nts id="Seg_1450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_1452" n="HIAT:w" s="T391">tögülleːk</ts>
                  <nts id="Seg_1453" n="HIAT:ip">"</nts>
                  <nts id="Seg_1454" n="HIAT:ip">,</nts>
                  <nts id="Seg_1455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T393" id="Seg_1457" n="HIAT:w" s="T392">taksan</ts>
                  <nts id="Seg_1458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_1460" n="HIAT:w" s="T393">irgek</ts>
                  <nts id="Seg_1461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_1463" n="HIAT:w" s="T394">koru͡obanɨ</ts>
                  <nts id="Seg_1464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T396" id="Seg_1466" n="HIAT:w" s="T395">tutan</ts>
                  <nts id="Seg_1467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_1469" n="HIAT:w" s="T396">bi͡erer</ts>
                  <nts id="Seg_1470" n="HIAT:ip">.</nts>
                  <nts id="Seg_1471" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T403" id="Seg_1473" n="HIAT:u" s="T397">
                  <nts id="Seg_1474" n="HIAT:ip">"</nts>
                  <ts e="T398" id="Seg_1476" n="HIAT:w" s="T397">Manɨ</ts>
                  <nts id="Seg_1477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_1479" n="HIAT:w" s="T398">harsi͡erda</ts>
                  <nts id="Seg_1480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T400" id="Seg_1482" n="HIAT:w" s="T399">törötön</ts>
                  <nts id="Seg_1483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_1485" n="HIAT:w" s="T400">tugutun</ts>
                  <nts id="Seg_1486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T402" id="Seg_1488" n="HIAT:w" s="T401">batɨhɨnnaran</ts>
                  <nts id="Seg_1489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_1491" n="HIAT:w" s="T402">egeleːr</ts>
                  <nts id="Seg_1492" n="HIAT:ip">.</nts>
                  <nts id="Seg_1493" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T410" id="Seg_1495" n="HIAT:u" s="T403">
                  <ts e="T404" id="Seg_1497" n="HIAT:w" s="T403">Kaja</ts>
                  <nts id="Seg_1498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T405" id="Seg_1500" n="HIAT:w" s="T404">u͡oruktatɨn</ts>
                  <nts id="Seg_1501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_1503" n="HIAT:w" s="T405">komujan</ts>
                  <nts id="Seg_1504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T407" id="Seg_1506" n="HIAT:w" s="T406">egeleːjegin</ts>
                  <nts id="Seg_1507" n="HIAT:ip">"</nts>
                  <nts id="Seg_1508" n="HIAT:ip">,</nts>
                  <nts id="Seg_1509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T408" id="Seg_1511" n="HIAT:w" s="T407">di͡ebit</ts>
                  <nts id="Seg_1512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T409" id="Seg_1514" n="HIAT:w" s="T408">U͡ol</ts>
                  <nts id="Seg_1515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_1517" n="HIAT:w" s="T409">ɨraːktaːgɨ</ts>
                  <nts id="Seg_1518" n="HIAT:ip">.</nts>
                  <nts id="Seg_1519" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T418" id="Seg_1521" n="HIAT:u" s="T410">
                  <ts e="T411" id="Seg_1523" n="HIAT:w" s="T410">Ogonnʼor</ts>
                  <nts id="Seg_1524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T412" id="Seg_1526" n="HIAT:w" s="T411">erejdeːk</ts>
                  <nts id="Seg_1527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T413" id="Seg_1529" n="HIAT:w" s="T412">irgek</ts>
                  <nts id="Seg_1530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414" id="Seg_1532" n="HIAT:w" s="T413">koru͡obatɨn</ts>
                  <nts id="Seg_1533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T415" id="Seg_1535" n="HIAT:w" s="T414">hi͡eten</ts>
                  <nts id="Seg_1536" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T416" id="Seg_1538" n="HIAT:w" s="T415">tijen</ts>
                  <nts id="Seg_1539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T417" id="Seg_1541" n="HIAT:w" s="T416">ɨtɨː</ts>
                  <nts id="Seg_1542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T418" id="Seg_1544" n="HIAT:w" s="T417">olordo</ts>
                  <nts id="Seg_1545" n="HIAT:ip">.</nts>
                  <nts id="Seg_1546" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T420" id="Seg_1548" n="HIAT:u" s="T418">
                  <ts e="T419" id="Seg_1550" n="HIAT:w" s="T418">Kɨːha</ts>
                  <nts id="Seg_1551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T420" id="Seg_1553" n="HIAT:w" s="T419">ɨjɨtta</ts>
                  <nts id="Seg_1554" n="HIAT:ip">:</nts>
                  <nts id="Seg_1555" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T424" id="Seg_1557" n="HIAT:u" s="T420">
                  <nts id="Seg_1558" n="HIAT:ip">"</nts>
                  <ts e="T421" id="Seg_1560" n="HIAT:w" s="T420">Kaja</ts>
                  <nts id="Seg_1561" n="HIAT:ip">,</nts>
                  <nts id="Seg_1562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T422" id="Seg_1564" n="HIAT:w" s="T421">agaː</ts>
                  <nts id="Seg_1565" n="HIAT:ip">,</nts>
                  <nts id="Seg_1566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T423" id="Seg_1568" n="HIAT:w" s="T422">togo</ts>
                  <nts id="Seg_1569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T424" id="Seg_1571" n="HIAT:w" s="T423">ɨtaːtɨŋ</ts>
                  <nts id="Seg_1572" n="HIAT:ip">?</nts>
                  <nts id="Seg_1573" n="HIAT:ip">"</nts>
                  <nts id="Seg_1574" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T436" id="Seg_1576" n="HIAT:u" s="T424">
                  <nts id="Seg_1577" n="HIAT:ip">"</nts>
                  <ts e="T425" id="Seg_1579" n="HIAT:w" s="T424">Kaja</ts>
                  <nts id="Seg_1580" n="HIAT:ip">,</nts>
                  <nts id="Seg_1581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T426" id="Seg_1583" n="HIAT:w" s="T425">irgek</ts>
                  <nts id="Seg_1584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T427" id="Seg_1586" n="HIAT:w" s="T426">koru͡obanɨ</ts>
                  <nts id="Seg_1587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T428" id="Seg_1589" n="HIAT:w" s="T427">bi͡erde</ts>
                  <nts id="Seg_1590" n="HIAT:ip">,</nts>
                  <nts id="Seg_1591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T429" id="Seg_1593" n="HIAT:w" s="T428">törötön</ts>
                  <nts id="Seg_1594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_1596" n="HIAT:w" s="T429">egel</ts>
                  <nts id="Seg_1597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T431" id="Seg_1599" n="HIAT:w" s="T430">diːr</ts>
                  <nts id="Seg_1600" n="HIAT:ip">,</nts>
                  <nts id="Seg_1601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_1603" n="HIAT:w" s="T431">irgek</ts>
                  <nts id="Seg_1604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T433" id="Seg_1606" n="HIAT:w" s="T432">koru͡oba</ts>
                  <nts id="Seg_1607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T434" id="Seg_1609" n="HIAT:w" s="T433">kantɨnan</ts>
                  <nts id="Seg_1610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T435" id="Seg_1612" n="HIAT:w" s="T434">töröːččünüj</ts>
                  <nts id="Seg_1613" n="HIAT:ip">,</nts>
                  <nts id="Seg_1614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_1616" n="HIAT:w" s="T435">kuːgu</ts>
                  <nts id="Seg_1617" n="HIAT:ip">?</nts>
                  <nts id="Seg_1618" n="HIAT:ip">"</nts>
                  <nts id="Seg_1619" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T441" id="Seg_1621" n="HIAT:u" s="T436">
                  <nts id="Seg_1622" n="HIAT:ip">"</nts>
                  <ts e="T437" id="Seg_1624" n="HIAT:w" s="T436">Eː</ts>
                  <nts id="Seg_1625" n="HIAT:ip">,</nts>
                  <nts id="Seg_1626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T438" id="Seg_1628" n="HIAT:w" s="T437">irgek</ts>
                  <nts id="Seg_1629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T439" id="Seg_1631" n="HIAT:w" s="T438">koru͡oba</ts>
                  <nts id="Seg_1632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440" id="Seg_1634" n="HIAT:w" s="T439">kantan</ts>
                  <nts id="Seg_1635" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_1637" n="HIAT:w" s="T440">törü͡öj</ts>
                  <nts id="Seg_1638" n="HIAT:ip">?</nts>
                  <nts id="Seg_1639" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T443" id="Seg_1641" n="HIAT:u" s="T441">
                  <ts e="T442" id="Seg_1643" n="HIAT:w" s="T441">Kata</ts>
                  <nts id="Seg_1644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T443" id="Seg_1646" n="HIAT:w" s="T442">astammɨppɨt</ts>
                  <nts id="Seg_1647" n="HIAT:ip">.</nts>
                  <nts id="Seg_1648" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T449" id="Seg_1650" n="HIAT:u" s="T443">
                  <ts e="T444" id="Seg_1652" n="HIAT:w" s="T443">Taksaŋŋɨn</ts>
                  <nts id="Seg_1653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T445" id="Seg_1655" n="HIAT:w" s="T444">ölörböktöː</ts>
                  <nts id="Seg_1656" n="HIAT:ip">,</nts>
                  <nts id="Seg_1657" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T446" id="Seg_1659" n="HIAT:w" s="T445">etin</ts>
                  <nts id="Seg_1660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T447" id="Seg_1662" n="HIAT:w" s="T446">hi͡ekpit</ts>
                  <nts id="Seg_1663" n="HIAT:ip">"</nts>
                  <nts id="Seg_1664" n="HIAT:ip">,</nts>
                  <nts id="Seg_1665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T448" id="Seg_1667" n="HIAT:w" s="T447">diːr</ts>
                  <nts id="Seg_1668" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T449" id="Seg_1670" n="HIAT:w" s="T448">kɨːs</ts>
                  <nts id="Seg_1671" n="HIAT:ip">.</nts>
                  <nts id="Seg_1672" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T457" id="Seg_1674" n="HIAT:u" s="T449">
                  <ts e="T450" id="Seg_1676" n="HIAT:w" s="T449">Ogonnʼor</ts>
                  <nts id="Seg_1677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T451" id="Seg_1679" n="HIAT:w" s="T450">irgek</ts>
                  <nts id="Seg_1680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T452" id="Seg_1682" n="HIAT:w" s="T451">koru͡obatɨn</ts>
                  <nts id="Seg_1683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T453" id="Seg_1685" n="HIAT:w" s="T452">ölörön</ts>
                  <nts id="Seg_1686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T454" id="Seg_1688" n="HIAT:w" s="T453">keːher</ts>
                  <nts id="Seg_1689" n="HIAT:ip">,</nts>
                  <nts id="Seg_1690" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T455" id="Seg_1692" n="HIAT:w" s="T454">dʼe</ts>
                  <nts id="Seg_1693" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T456" id="Seg_1695" n="HIAT:w" s="T455">manɨ</ts>
                  <nts id="Seg_1696" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T457" id="Seg_1698" n="HIAT:w" s="T456">hi͡ektere</ts>
                  <nts id="Seg_1699" n="HIAT:ip">.</nts>
                  <nts id="Seg_1700" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T465" id="Seg_1702" n="HIAT:u" s="T457">
                  <ts e="T458" id="Seg_1704" n="HIAT:w" s="T457">Kɨːha</ts>
                  <nts id="Seg_1705" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T459" id="Seg_1707" n="HIAT:w" s="T458">utakaːga</ts>
                  <nts id="Seg_1708" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T460" id="Seg_1710" n="HIAT:w" s="T459">koru͡oba</ts>
                  <nts id="Seg_1711" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T461" id="Seg_1713" n="HIAT:w" s="T460">tunnʼagɨn</ts>
                  <nts id="Seg_1714" n="HIAT:ip">,</nts>
                  <nts id="Seg_1715" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T462" id="Seg_1717" n="HIAT:w" s="T461">tugulun</ts>
                  <nts id="Seg_1718" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T463" id="Seg_1720" n="HIAT:w" s="T462">komujan</ts>
                  <nts id="Seg_1721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T464" id="Seg_1723" n="HIAT:w" s="T463">bi͡erde</ts>
                  <nts id="Seg_1724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T465" id="Seg_1726" n="HIAT:w" s="T464">agatɨgar</ts>
                  <nts id="Seg_1727" n="HIAT:ip">:</nts>
                  <nts id="Seg_1728" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T481" id="Seg_1730" n="HIAT:u" s="T465">
                  <nts id="Seg_1731" n="HIAT:ip">"</nts>
                  <nts id="Seg_1732" n="HIAT:ip">"</nts>
                  <ts e="T466" id="Seg_1734" n="HIAT:w" s="T465">Dʼe</ts>
                  <nts id="Seg_1735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T467" id="Seg_1737" n="HIAT:w" s="T466">iti</ts>
                  <nts id="Seg_1738" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T468" id="Seg_1740" n="HIAT:w" s="T467">kara</ts>
                  <nts id="Seg_1741" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T469" id="Seg_1743" n="HIAT:w" s="T468">tunʼaga</ts>
                  <nts id="Seg_1744" n="HIAT:ip">—</nts>
                  <nts id="Seg_1745" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T470" id="Seg_1747" n="HIAT:w" s="T469">inʼete</ts>
                  <nts id="Seg_1748" n="HIAT:ip">,</nts>
                  <nts id="Seg_1749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T471" id="Seg_1751" n="HIAT:w" s="T470">tugula</ts>
                  <nts id="Seg_1752" n="HIAT:ip">—</nts>
                  <nts id="Seg_1753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T472" id="Seg_1755" n="HIAT:w" s="T471">tubuta</ts>
                  <nts id="Seg_1756" n="HIAT:ip">"</nts>
                  <nts id="Seg_1757" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T473" id="Seg_1759" n="HIAT:w" s="T472">di͡en</ts>
                  <nts id="Seg_1760" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T474" id="Seg_1762" n="HIAT:w" s="T473">baraːn</ts>
                  <nts id="Seg_1763" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T475" id="Seg_1765" n="HIAT:w" s="T474">ɨraːktaːgɨn</ts>
                  <nts id="Seg_1766" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T476" id="Seg_1768" n="HIAT:w" s="T475">ostu͡olugar</ts>
                  <nts id="Seg_1769" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T477" id="Seg_1771" n="HIAT:w" s="T476">ogo</ts>
                  <nts id="Seg_1772" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T478" id="Seg_1774" n="HIAT:w" s="T477">oːnnʼuːrun</ts>
                  <nts id="Seg_1775" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T479" id="Seg_1777" n="HIAT:w" s="T478">kurduk</ts>
                  <nts id="Seg_1778" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T480" id="Seg_1780" n="HIAT:w" s="T479">huburutalaːn</ts>
                  <nts id="Seg_1781" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T481" id="Seg_1783" n="HIAT:w" s="T480">keːheːr</ts>
                  <nts id="Seg_1784" n="HIAT:ip">.</nts>
                  <nts id="Seg_1785" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T489" id="Seg_1787" n="HIAT:u" s="T481">
                  <nts id="Seg_1788" n="HIAT:ip">"</nts>
                  <ts e="T482" id="Seg_1790" n="HIAT:w" s="T481">Irgek</ts>
                  <nts id="Seg_1791" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T483" id="Seg_1793" n="HIAT:w" s="T482">koru͡obattan</ts>
                  <nts id="Seg_1794" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T484" id="Seg_1796" n="HIAT:w" s="T483">törüːr</ts>
                  <nts id="Seg_1797" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T485" id="Seg_1799" n="HIAT:w" s="T484">agaj</ts>
                  <nts id="Seg_1800" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T486" id="Seg_1802" n="HIAT:w" s="T485">hirin</ts>
                  <nts id="Seg_1803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T487" id="Seg_1805" n="HIAT:w" s="T486">bu</ts>
                  <nts id="Seg_1806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T488" id="Seg_1808" n="HIAT:w" s="T487">bullum</ts>
                  <nts id="Seg_1809" n="HIAT:ip">"</nts>
                  <nts id="Seg_1810" n="HIAT:ip">,</nts>
                  <nts id="Seg_1811" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T489" id="Seg_1813" n="HIAT:w" s="T488">di͡er</ts>
                  <nts id="Seg_1814" n="HIAT:ip">.</nts>
                  <nts id="Seg_1815" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T496" id="Seg_1817" n="HIAT:u" s="T489">
                  <ts e="T490" id="Seg_1819" n="HIAT:w" s="T489">Onton</ts>
                  <nts id="Seg_1820" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T491" id="Seg_1822" n="HIAT:w" s="T490">ordugu</ts>
                  <nts id="Seg_1823" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T492" id="Seg_1825" n="HIAT:w" s="T491">da</ts>
                  <nts id="Seg_1826" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T493" id="Seg_1828" n="HIAT:w" s="T492">kini</ts>
                  <nts id="Seg_1829" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T494" id="Seg_1831" n="HIAT:w" s="T493">bilere</ts>
                  <nts id="Seg_1832" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T495" id="Seg_1834" n="HIAT:w" s="T494">bu͡olu͡o</ts>
                  <nts id="Seg_1835" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T496" id="Seg_1837" n="HIAT:w" s="T495">du͡o</ts>
                  <nts id="Seg_1838" n="HIAT:ip">?</nts>
                  <nts id="Seg_1839" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T500" id="Seg_1841" n="HIAT:u" s="T496">
                  <ts e="T497" id="Seg_1843" n="HIAT:w" s="T496">Dʼe</ts>
                  <nts id="Seg_1844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T498" id="Seg_1846" n="HIAT:w" s="T497">oččogo</ts>
                  <nts id="Seg_1847" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T499" id="Seg_1849" n="HIAT:w" s="T498">mohujan</ts>
                  <nts id="Seg_1850" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T500" id="Seg_1852" n="HIAT:w" s="T499">ɨjɨtɨ͡a</ts>
                  <nts id="Seg_1853" n="HIAT:ip">:</nts>
                  <nts id="Seg_1854" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T504" id="Seg_1856" n="HIAT:u" s="T500">
                  <nts id="Seg_1857" n="HIAT:ip">"</nts>
                  <ts e="T501" id="Seg_1859" n="HIAT:w" s="T500">Bejeŋ</ts>
                  <nts id="Seg_1860" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T502" id="Seg_1862" n="HIAT:w" s="T501">öjgünen</ts>
                  <nts id="Seg_1863" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T503" id="Seg_1865" n="HIAT:w" s="T502">gɨmmatɨŋ</ts>
                  <nts id="Seg_1866" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T504" id="Seg_1868" n="HIAT:w" s="T503">bu͡olu͡o</ts>
                  <nts id="Seg_1869" n="HIAT:ip">.</nts>
                  <nts id="Seg_1870" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T514" id="Seg_1872" n="HIAT:u" s="T504">
                  <ts e="T505" id="Seg_1874" n="HIAT:w" s="T504">Itičče</ts>
                  <nts id="Seg_1875" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T506" id="Seg_1877" n="HIAT:w" s="T505">öjdöːgüŋ</ts>
                  <nts id="Seg_1878" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T507" id="Seg_1880" n="HIAT:w" s="T506">bu͡ollar</ts>
                  <nts id="Seg_1881" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T508" id="Seg_1883" n="HIAT:w" s="T507">min</ts>
                  <nts id="Seg_1884" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T509" id="Seg_1886" n="HIAT:w" s="T508">iːkpin-haːkpɨn</ts>
                  <nts id="Seg_1887" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T510" id="Seg_1889" n="HIAT:w" s="T509">ɨraːstɨː</ts>
                  <nts id="Seg_1890" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T511" id="Seg_1892" n="HIAT:w" s="T510">iŋen-batan</ts>
                  <nts id="Seg_1893" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T512" id="Seg_1895" n="HIAT:w" s="T511">hɨtɨ͡a</ts>
                  <nts id="Seg_1896" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T513" id="Seg_1898" n="HIAT:w" s="T512">hu͡ok</ts>
                  <nts id="Seg_1899" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T514" id="Seg_1901" n="HIAT:w" s="T513">etiŋ</ts>
                  <nts id="Seg_1902" n="HIAT:ip">.</nts>
                  <nts id="Seg_1903" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T517" id="Seg_1905" n="HIAT:u" s="T514">
                  <ts e="T515" id="Seg_1907" n="HIAT:w" s="T514">Kim</ts>
                  <nts id="Seg_1908" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T516" id="Seg_1910" n="HIAT:w" s="T515">ü͡öreter</ts>
                  <nts id="Seg_1911" n="HIAT:ip">"</nts>
                  <nts id="Seg_1912" n="HIAT:ip">,</nts>
                  <nts id="Seg_1913" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T517" id="Seg_1915" n="HIAT:w" s="T516">di͡ege</ts>
                  <nts id="Seg_1916" n="HIAT:ip">.</nts>
                  <nts id="Seg_1917" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T521" id="Seg_1919" n="HIAT:u" s="T517">
                  <ts e="T518" id="Seg_1921" n="HIAT:w" s="T517">Onno</ts>
                  <nts id="Seg_1922" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T519" id="Seg_1924" n="HIAT:w" s="T518">kisteːjegin</ts>
                  <nts id="Seg_1925" n="HIAT:ip">"</nts>
                  <nts id="Seg_1926" n="HIAT:ip">,</nts>
                  <nts id="Seg_1927" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T520" id="Seg_1929" n="HIAT:w" s="T519">diːr</ts>
                  <nts id="Seg_1930" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T521" id="Seg_1932" n="HIAT:w" s="T520">kɨːha</ts>
                  <nts id="Seg_1933" n="HIAT:ip">.</nts>
                  <nts id="Seg_1934" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T527" id="Seg_1936" n="HIAT:u" s="T521">
                  <nts id="Seg_1937" n="HIAT:ip">"</nts>
                  <nts id="Seg_1938" n="HIAT:ip">"</nts>
                  <ts e="T522" id="Seg_1940" n="HIAT:w" s="T521">Kuhagan</ts>
                  <nts id="Seg_1941" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T523" id="Seg_1943" n="HIAT:w" s="T522">kɨːs</ts>
                  <nts id="Seg_1944" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T524" id="Seg_1946" n="HIAT:w" s="T523">ogoloːkpun</ts>
                  <nts id="Seg_1947" n="HIAT:ip">,</nts>
                  <nts id="Seg_1948" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T525" id="Seg_1950" n="HIAT:w" s="T524">ol</ts>
                  <nts id="Seg_1951" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T526" id="Seg_1953" n="HIAT:w" s="T525">ü͡öreteːktiːr</ts>
                  <nts id="Seg_1954" n="HIAT:ip">"</nts>
                  <nts id="Seg_1955" n="HIAT:ip">,</nts>
                  <nts id="Seg_1956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T527" id="Seg_1958" n="HIAT:w" s="T526">di͡er</ts>
                  <nts id="Seg_1959" n="HIAT:ip">.</nts>
                  <nts id="Seg_1960" n="HIAT:ip">"</nts>
                  <nts id="Seg_1961" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T530" id="Seg_1963" n="HIAT:u" s="T527">
                  <ts e="T528" id="Seg_1965" n="HIAT:w" s="T527">Ogonnʼor</ts>
                  <nts id="Seg_1966" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T529" id="Seg_1968" n="HIAT:w" s="T528">harsi͡erda</ts>
                  <nts id="Seg_1969" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T530" id="Seg_1971" n="HIAT:w" s="T529">barar</ts>
                  <nts id="Seg_1972" n="HIAT:ip">.</nts>
                  <nts id="Seg_1973" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T533" id="Seg_1975" n="HIAT:u" s="T530">
                  <ts e="T531" id="Seg_1977" n="HIAT:w" s="T530">U͡ol</ts>
                  <nts id="Seg_1978" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T532" id="Seg_1980" n="HIAT:w" s="T531">ɨraːktaːgɨ</ts>
                  <nts id="Seg_1981" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T533" id="Seg_1983" n="HIAT:w" s="T532">ɨjɨtar</ts>
                  <nts id="Seg_1984" n="HIAT:ip">:</nts>
                  <nts id="Seg_1985" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T536" id="Seg_1987" n="HIAT:u" s="T533">
                  <nts id="Seg_1988" n="HIAT:ip">"</nts>
                  <ts e="T534" id="Seg_1990" n="HIAT:w" s="T533">Kaja</ts>
                  <nts id="Seg_1991" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T535" id="Seg_1993" n="HIAT:w" s="T534">taːjdɨŋ</ts>
                  <nts id="Seg_1994" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T536" id="Seg_1996" n="HIAT:w" s="T535">du͡o</ts>
                  <nts id="Seg_1997" n="HIAT:ip">?</nts>
                  <nts id="Seg_1998" n="HIAT:ip">"</nts>
                  <nts id="Seg_1999" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T546" id="Seg_2001" n="HIAT:u" s="T536">
                  <ts e="T537" id="Seg_2003" n="HIAT:w" s="T536">Ogonnʼor</ts>
                  <nts id="Seg_2004" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T538" id="Seg_2006" n="HIAT:w" s="T537">koru͡oba</ts>
                  <nts id="Seg_2007" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T539" id="Seg_2009" n="HIAT:w" s="T538">tunʼagɨn</ts>
                  <nts id="Seg_2010" n="HIAT:ip">,</nts>
                  <nts id="Seg_2011" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T540" id="Seg_2013" n="HIAT:w" s="T539">tugulun</ts>
                  <nts id="Seg_2014" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T541" id="Seg_2016" n="HIAT:w" s="T540">ostu͡olga</ts>
                  <nts id="Seg_2017" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T542" id="Seg_2019" n="HIAT:w" s="T541">ogo</ts>
                  <nts id="Seg_2020" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T543" id="Seg_2022" n="HIAT:w" s="T542">oːnnʼuːrun</ts>
                  <nts id="Seg_2023" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T544" id="Seg_2025" n="HIAT:w" s="T543">kurduk</ts>
                  <nts id="Seg_2026" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T545" id="Seg_2028" n="HIAT:w" s="T544">huburutalaːn</ts>
                  <nts id="Seg_2029" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T546" id="Seg_2031" n="HIAT:w" s="T545">keːher</ts>
                  <nts id="Seg_2032" n="HIAT:ip">.</nts>
                  <nts id="Seg_2033" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T554" id="Seg_2035" n="HIAT:u" s="T546">
                  <nts id="Seg_2036" n="HIAT:ip">"</nts>
                  <ts e="T547" id="Seg_2038" n="HIAT:w" s="T546">Irgek</ts>
                  <nts id="Seg_2039" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T548" id="Seg_2041" n="HIAT:w" s="T547">koru͡obattan</ts>
                  <nts id="Seg_2042" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T549" id="Seg_2044" n="HIAT:w" s="T548">törüːr</ts>
                  <nts id="Seg_2045" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T550" id="Seg_2047" n="HIAT:w" s="T549">agaj</ts>
                  <nts id="Seg_2048" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T551" id="Seg_2050" n="HIAT:w" s="T550">hirin</ts>
                  <nts id="Seg_2051" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T552" id="Seg_2053" n="HIAT:w" s="T551">bu</ts>
                  <nts id="Seg_2054" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T553" id="Seg_2056" n="HIAT:w" s="T552">bullum</ts>
                  <nts id="Seg_2057" n="HIAT:ip">"</nts>
                  <nts id="Seg_2058" n="HIAT:ip">,</nts>
                  <nts id="Seg_2059" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T554" id="Seg_2061" n="HIAT:w" s="T553">diːr</ts>
                  <nts id="Seg_2062" n="HIAT:ip">.</nts>
                  <nts id="Seg_2063" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T555" id="Seg_2065" n="HIAT:u" s="T554">
                  <ts e="T555" id="Seg_2067" n="HIAT:w" s="T554">ɨraːktaːgɨ</ts>
                  <nts id="Seg_2068" n="HIAT:ip">:</nts>
                  <nts id="Seg_2069" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T559" id="Seg_2071" n="HIAT:u" s="T555">
                  <nts id="Seg_2072" n="HIAT:ip">"</nts>
                  <ts e="T556" id="Seg_2074" n="HIAT:w" s="T555">Bejeŋ</ts>
                  <nts id="Seg_2075" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T557" id="Seg_2077" n="HIAT:w" s="T556">öjgünen</ts>
                  <nts id="Seg_2078" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T558" id="Seg_2080" n="HIAT:w" s="T557">gɨmmatɨŋ</ts>
                  <nts id="Seg_2081" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T559" id="Seg_2083" n="HIAT:w" s="T558">bu͡olu͡o</ts>
                  <nts id="Seg_2084" n="HIAT:ip">.</nts>
                  <nts id="Seg_2085" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T569" id="Seg_2087" n="HIAT:u" s="T559">
                  <ts e="T560" id="Seg_2089" n="HIAT:w" s="T559">Itičče</ts>
                  <nts id="Seg_2090" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T561" id="Seg_2092" n="HIAT:w" s="T560">öjdöːguŋ</ts>
                  <nts id="Seg_2093" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T562" id="Seg_2095" n="HIAT:w" s="T561">bu͡ollar</ts>
                  <nts id="Seg_2096" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T563" id="Seg_2098" n="HIAT:w" s="T562">min</ts>
                  <nts id="Seg_2099" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T564" id="Seg_2101" n="HIAT:w" s="T563">iːkpin-haːkpɨn</ts>
                  <nts id="Seg_2102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T565" id="Seg_2104" n="HIAT:w" s="T564">ɨraːstɨː</ts>
                  <nts id="Seg_2105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T566" id="Seg_2107" n="HIAT:w" s="T565">iŋen-batan</ts>
                  <nts id="Seg_2108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T567" id="Seg_2110" n="HIAT:w" s="T566">hɨtɨ͡a</ts>
                  <nts id="Seg_2111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T568" id="Seg_2113" n="HIAT:w" s="T567">hu͡ok</ts>
                  <nts id="Seg_2114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T569" id="Seg_2116" n="HIAT:w" s="T568">etiŋ</ts>
                  <nts id="Seg_2117" n="HIAT:ip">.</nts>
                  <nts id="Seg_2118" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T572" id="Seg_2120" n="HIAT:u" s="T569">
                  <ts e="T570" id="Seg_2122" n="HIAT:w" s="T569">Kim</ts>
                  <nts id="Seg_2123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T571" id="Seg_2125" n="HIAT:w" s="T570">ü͡öreter</ts>
                  <nts id="Seg_2126" n="HIAT:ip">"</nts>
                  <nts id="Seg_2127" n="HIAT:ip">,</nts>
                  <nts id="Seg_2128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T572" id="Seg_2130" n="HIAT:w" s="T571">diːr</ts>
                  <nts id="Seg_2131" n="HIAT:ip">.</nts>
                  <nts id="Seg_2132" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T579" id="Seg_2134" n="HIAT:u" s="T572">
                  <nts id="Seg_2135" n="HIAT:ip">"</nts>
                  <ts e="T573" id="Seg_2137" n="HIAT:w" s="T572">Kuhagan</ts>
                  <nts id="Seg_2138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T574" id="Seg_2140" n="HIAT:w" s="T573">kɨːs</ts>
                  <nts id="Seg_2141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T575" id="Seg_2143" n="HIAT:w" s="T574">ogoloːkpun</ts>
                  <nts id="Seg_2144" n="HIAT:ip">,</nts>
                  <nts id="Seg_2145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T576" id="Seg_2147" n="HIAT:w" s="T575">ol</ts>
                  <nts id="Seg_2148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T577" id="Seg_2150" n="HIAT:w" s="T576">ü͡öreteːktiːr</ts>
                  <nts id="Seg_2151" n="HIAT:ip">"</nts>
                  <nts id="Seg_2152" n="HIAT:ip">,</nts>
                  <nts id="Seg_2153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T578" id="Seg_2155" n="HIAT:w" s="T577">diːr</ts>
                  <nts id="Seg_2156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T579" id="Seg_2158" n="HIAT:w" s="T578">ogonnʼor</ts>
                  <nts id="Seg_2159" n="HIAT:ip">.</nts>
                  <nts id="Seg_2160" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T581" id="Seg_2162" n="HIAT:u" s="T579">
                  <ts e="T580" id="Seg_2164" n="HIAT:w" s="T579">U͡ol</ts>
                  <nts id="Seg_2165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T581" id="Seg_2167" n="HIAT:w" s="T580">ɨraːktaːgɨ</ts>
                  <nts id="Seg_2168" n="HIAT:ip">:</nts>
                  <nts id="Seg_2169" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T586" id="Seg_2171" n="HIAT:u" s="T581">
                  <nts id="Seg_2172" n="HIAT:ip">"</nts>
                  <ts e="T582" id="Seg_2174" n="HIAT:w" s="T581">Kɨːhɨŋ</ts>
                  <nts id="Seg_2175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T583" id="Seg_2177" n="HIAT:w" s="T582">öjdöːk</ts>
                  <nts id="Seg_2178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T584" id="Seg_2180" n="HIAT:w" s="T583">kɨːs</ts>
                  <nts id="Seg_2181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T585" id="Seg_2183" n="HIAT:w" s="T584">töröːbüt</ts>
                  <nts id="Seg_2184" n="HIAT:ip">"</nts>
                  <nts id="Seg_2185" n="HIAT:ip">,</nts>
                  <nts id="Seg_2186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T586" id="Seg_2188" n="HIAT:w" s="T585">diːr</ts>
                  <nts id="Seg_2189" n="HIAT:ip">.</nts>
                  <nts id="Seg_2190" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T591" id="Seg_2192" n="HIAT:u" s="T586">
                  <ts e="T587" id="Seg_2194" n="HIAT:w" s="T586">Timir</ts>
                  <nts id="Seg_2195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T588" id="Seg_2197" n="HIAT:w" s="T587">kokoroːgu</ts>
                  <nts id="Seg_2198" n="HIAT:ip">,</nts>
                  <nts id="Seg_2199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T589" id="Seg_2201" n="HIAT:w" s="T588">tügege</ts>
                  <nts id="Seg_2202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T590" id="Seg_2204" n="HIAT:w" s="T589">kajagastaːgɨ</ts>
                  <nts id="Seg_2205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T591" id="Seg_2207" n="HIAT:w" s="T590">bi͡erer</ts>
                  <nts id="Seg_2208" n="HIAT:ip">.</nts>
                  <nts id="Seg_2209" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T595" id="Seg_2211" n="HIAT:u" s="T591">
                  <nts id="Seg_2212" n="HIAT:ip">"</nts>
                  <ts e="T592" id="Seg_2214" n="HIAT:w" s="T591">Manɨ</ts>
                  <nts id="Seg_2215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T593" id="Seg_2217" n="HIAT:w" s="T592">hamaːn</ts>
                  <nts id="Seg_2218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T594" id="Seg_2220" n="HIAT:w" s="T593">ɨːttɨn</ts>
                  <nts id="Seg_2221" n="HIAT:ip">"</nts>
                  <nts id="Seg_2222" n="HIAT:ip">,</nts>
                  <nts id="Seg_2223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T595" id="Seg_2225" n="HIAT:w" s="T594">diːr</ts>
                  <nts id="Seg_2226" n="HIAT:ip">.</nts>
                  <nts id="Seg_2227" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T601" id="Seg_2229" n="HIAT:u" s="T595">
                  <nts id="Seg_2230" n="HIAT:ip">"</nts>
                  <ts e="T596" id="Seg_2232" n="HIAT:w" s="T595">Onu</ts>
                  <nts id="Seg_2233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T597" id="Seg_2235" n="HIAT:w" s="T596">hamaːbatagɨna</ts>
                  <nts id="Seg_2236" n="HIAT:ip">,</nts>
                  <nts id="Seg_2237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T598" id="Seg_2239" n="HIAT:w" s="T597">ikki͡enŋitin</ts>
                  <nts id="Seg_2240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T599" id="Seg_2242" n="HIAT:w" s="T598">töbögütün</ts>
                  <nts id="Seg_2243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T600" id="Seg_2245" n="HIAT:w" s="T599">bɨhɨ͡am</ts>
                  <nts id="Seg_2246" n="HIAT:ip">"</nts>
                  <nts id="Seg_2247" n="HIAT:ip">,</nts>
                  <nts id="Seg_2248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T601" id="Seg_2250" n="HIAT:w" s="T600">diːr</ts>
                  <nts id="Seg_2251" n="HIAT:ip">.</nts>
                  <nts id="Seg_2252" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T606" id="Seg_2254" n="HIAT:u" s="T601">
                  <ts e="T602" id="Seg_2256" n="HIAT:w" s="T601">Ogonnʼor</ts>
                  <nts id="Seg_2257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T603" id="Seg_2259" n="HIAT:w" s="T602">kokoroːgu</ts>
                  <nts id="Seg_2260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T604" id="Seg_2262" n="HIAT:w" s="T603">ildʼe</ts>
                  <nts id="Seg_2263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T605" id="Seg_2265" n="HIAT:w" s="T604">barbɨta</ts>
                  <nts id="Seg_2266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T606" id="Seg_2268" n="HIAT:w" s="T605">ɨtɨː-ɨtɨː</ts>
                  <nts id="Seg_2269" n="HIAT:ip">.</nts>
                  <nts id="Seg_2270" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T607" id="Seg_2272" n="HIAT:u" s="T606">
                  <ts e="T607" id="Seg_2274" n="HIAT:w" s="T606">Kɨːs</ts>
                  <nts id="Seg_2275" n="HIAT:ip">:</nts>
                  <nts id="Seg_2276" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T612" id="Seg_2278" n="HIAT:u" s="T607">
                  <nts id="Seg_2279" n="HIAT:ip">"</nts>
                  <ts e="T608" id="Seg_2281" n="HIAT:w" s="T607">Kaja</ts>
                  <nts id="Seg_2282" n="HIAT:ip">,</nts>
                  <nts id="Seg_2283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T609" id="Seg_2285" n="HIAT:w" s="T608">agaː</ts>
                  <nts id="Seg_2286" n="HIAT:ip">,</nts>
                  <nts id="Seg_2287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T610" id="Seg_2289" n="HIAT:w" s="T609">tu͡oguŋ</ts>
                  <nts id="Seg_2290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T611" id="Seg_2292" n="HIAT:w" s="T610">kokoroːgoj</ts>
                  <nts id="Seg_2293" n="HIAT:ip">"</nts>
                  <nts id="Seg_2294" n="HIAT:ip">,</nts>
                  <nts id="Seg_2295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T612" id="Seg_2297" n="HIAT:w" s="T611">diːr</ts>
                  <nts id="Seg_2298" n="HIAT:ip">.</nts>
                  <nts id="Seg_2299" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T617" id="Seg_2301" n="HIAT:u" s="T612">
                  <nts id="Seg_2302" n="HIAT:ip">"</nts>
                  <ts e="T613" id="Seg_2304" n="HIAT:w" s="T612">Kaja-kuː</ts>
                  <nts id="Seg_2305" n="HIAT:ip">,</nts>
                  <nts id="Seg_2306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T614" id="Seg_2308" n="HIAT:w" s="T613">dʼe</ts>
                  <nts id="Seg_2309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T615" id="Seg_2311" n="HIAT:w" s="T614">ölörör</ts>
                  <nts id="Seg_2312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T616" id="Seg_2314" n="HIAT:w" s="T615">küne</ts>
                  <nts id="Seg_2315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T617" id="Seg_2317" n="HIAT:w" s="T616">kelle</ts>
                  <nts id="Seg_2318" n="HIAT:ip">.</nts>
                  <nts id="Seg_2319" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T622" id="Seg_2321" n="HIAT:u" s="T617">
                  <nts id="Seg_2322" n="HIAT:ip">"</nts>
                  <ts e="T618" id="Seg_2324" n="HIAT:w" s="T617">Bu</ts>
                  <nts id="Seg_2325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T619" id="Seg_2327" n="HIAT:w" s="T618">kokoroːgu</ts>
                  <nts id="Seg_2328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T620" id="Seg_2330" n="HIAT:w" s="T619">hamaːn</ts>
                  <nts id="Seg_2331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T621" id="Seg_2333" n="HIAT:w" s="T620">ɨːttɨn</ts>
                  <nts id="Seg_2334" n="HIAT:ip">"</nts>
                  <nts id="Seg_2335" n="HIAT:ip">,</nts>
                  <nts id="Seg_2336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T622" id="Seg_2338" n="HIAT:w" s="T621">diːr</ts>
                  <nts id="Seg_2339" n="HIAT:ip">.</nts>
                  <nts id="Seg_2340" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T628" id="Seg_2342" n="HIAT:u" s="T622">
                  <nts id="Seg_2343" n="HIAT:ip">"</nts>
                  <ts e="T623" id="Seg_2345" n="HIAT:w" s="T622">Bunu</ts>
                  <nts id="Seg_2346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T624" id="Seg_2348" n="HIAT:w" s="T623">hamaːbatagɨna</ts>
                  <nts id="Seg_2349" n="HIAT:ip">,</nts>
                  <nts id="Seg_2350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T625" id="Seg_2352" n="HIAT:w" s="T624">ikki͡enŋitin</ts>
                  <nts id="Seg_2353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T626" id="Seg_2355" n="HIAT:w" s="T625">töbögütün</ts>
                  <nts id="Seg_2356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T627" id="Seg_2358" n="HIAT:w" s="T626">bɨhɨ͡am</ts>
                  <nts id="Seg_2359" n="HIAT:ip">"</nts>
                  <nts id="Seg_2360" n="HIAT:ip">,</nts>
                  <nts id="Seg_2361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T628" id="Seg_2363" n="HIAT:w" s="T627">diːr</ts>
                  <nts id="Seg_2364" n="HIAT:ip">.</nts>
                  <nts id="Seg_2365" n="HIAT:ip">"</nts>
                  <nts id="Seg_2366" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T630" id="Seg_2368" n="HIAT:u" s="T628">
                  <ts e="T629" id="Seg_2370" n="HIAT:w" s="T628">Kɨːs</ts>
                  <nts id="Seg_2371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T630" id="Seg_2373" n="HIAT:w" s="T629">diːr</ts>
                  <nts id="Seg_2374" n="HIAT:ip">:</nts>
                  <nts id="Seg_2375" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T642" id="Seg_2377" n="HIAT:u" s="T630">
                  <nts id="Seg_2378" n="HIAT:ip">"</nts>
                  <ts e="T631" id="Seg_2380" n="HIAT:w" s="T630">Ilt</ts>
                  <nts id="Seg_2381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T632" id="Seg_2383" n="HIAT:w" s="T631">tötterü</ts>
                  <nts id="Seg_2384" n="HIAT:ip">,</nts>
                  <nts id="Seg_2385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T633" id="Seg_2387" n="HIAT:w" s="T632">dʼaktar</ts>
                  <nts id="Seg_2388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T634" id="Seg_2390" n="HIAT:w" s="T633">kihi</ts>
                  <nts id="Seg_2391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T635" id="Seg_2393" n="HIAT:w" s="T634">hamɨ͡am</ts>
                  <nts id="Seg_2394" n="HIAT:ip">,</nts>
                  <nts id="Seg_2395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T636" id="Seg_2397" n="HIAT:w" s="T635">er</ts>
                  <nts id="Seg_2398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T637" id="Seg_2400" n="HIAT:w" s="T636">kihi</ts>
                  <nts id="Seg_2401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T638" id="Seg_2403" n="HIAT:w" s="T637">kokoroːgu</ts>
                  <nts id="Seg_2404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T639" id="Seg_2406" n="HIAT:w" s="T638">čorčokoːt</ts>
                  <nts id="Seg_2407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T640" id="Seg_2409" n="HIAT:w" s="T639">kördük</ts>
                  <nts id="Seg_2410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T641" id="Seg_2412" n="HIAT:w" s="T640">ti͡eren</ts>
                  <nts id="Seg_2413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T642" id="Seg_2415" n="HIAT:w" s="T641">ɨːttɨn</ts>
                  <nts id="Seg_2416" n="HIAT:ip">.</nts>
                  <nts id="Seg_2417" n="HIAT:ip">"</nts>
                  <nts id="Seg_2418" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T645" id="Seg_2420" n="HIAT:u" s="T642">
                  <ts e="T643" id="Seg_2422" n="HIAT:w" s="T642">Ogonnʼor</ts>
                  <nts id="Seg_2423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T644" id="Seg_2425" n="HIAT:w" s="T643">ildʼen</ts>
                  <nts id="Seg_2426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T645" id="Seg_2428" n="HIAT:w" s="T644">bi͡erer</ts>
                  <nts id="Seg_2429" n="HIAT:ip">.</nts>
                  <nts id="Seg_2430" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T648" id="Seg_2432" n="HIAT:u" s="T645">
                  <ts e="T646" id="Seg_2434" n="HIAT:w" s="T645">Uːol</ts>
                  <nts id="Seg_2435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T647" id="Seg_2437" n="HIAT:w" s="T646">ɨraːktaːgɨ</ts>
                  <nts id="Seg_2438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T648" id="Seg_2440" n="HIAT:w" s="T647">högör</ts>
                  <nts id="Seg_2441" n="HIAT:ip">.</nts>
                  <nts id="Seg_2442" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T650" id="Seg_2444" n="HIAT:u" s="T648">
                  <ts e="T649" id="Seg_2446" n="HIAT:w" s="T648">Ogonnʼoru</ts>
                  <nts id="Seg_2447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T650" id="Seg_2449" n="HIAT:w" s="T649">ɨːtar</ts>
                  <nts id="Seg_2450" n="HIAT:ip">.</nts>
                  <nts id="Seg_2451" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T655" id="Seg_2453" n="HIAT:u" s="T650">
                  <nts id="Seg_2454" n="HIAT:ip">"</nts>
                  <ts e="T651" id="Seg_2456" n="HIAT:w" s="T650">Bejem</ts>
                  <nts id="Seg_2457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T652" id="Seg_2459" n="HIAT:w" s="T651">kɨːskɨn</ts>
                  <nts id="Seg_2460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T653" id="Seg_2462" n="HIAT:w" s="T652">körö</ts>
                  <nts id="Seg_2463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T654" id="Seg_2465" n="HIAT:w" s="T653">barɨ͡am</ts>
                  <nts id="Seg_2466" n="HIAT:ip">"</nts>
                  <nts id="Seg_2467" n="HIAT:ip">,</nts>
                  <nts id="Seg_2468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T655" id="Seg_2470" n="HIAT:w" s="T654">diːr</ts>
                  <nts id="Seg_2471" n="HIAT:ip">.</nts>
                  <nts id="Seg_2472" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T662" id="Seg_2474" n="HIAT:u" s="T655">
                  <ts e="T656" id="Seg_2476" n="HIAT:w" s="T655">U͡ol</ts>
                  <nts id="Seg_2477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T657" id="Seg_2479" n="HIAT:w" s="T656">ɨraːktaːgɨ</ts>
                  <nts id="Seg_2480" n="HIAT:ip">,</nts>
                  <nts id="Seg_2481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T658" id="Seg_2483" n="HIAT:w" s="T657">kɨːhɨ</ts>
                  <nts id="Seg_2484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T659" id="Seg_2486" n="HIAT:w" s="T658">köröːt</ts>
                  <nts id="Seg_2487" n="HIAT:ip">,</nts>
                  <nts id="Seg_2488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T660" id="Seg_2490" n="HIAT:w" s="T659">bagarar</ts>
                  <nts id="Seg_2491" n="HIAT:ip">,</nts>
                  <nts id="Seg_2492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T661" id="Seg_2494" n="HIAT:w" s="T660">dʼaktar</ts>
                  <nts id="Seg_2495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T662" id="Seg_2497" n="HIAT:w" s="T661">ɨlar</ts>
                  <nts id="Seg_2498" n="HIAT:ip">.</nts>
                  <nts id="Seg_2499" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T665" id="Seg_2501" n="HIAT:u" s="T662">
                  <ts e="T663" id="Seg_2503" n="HIAT:w" s="T662">Töhönü-kaččanɨ</ts>
                  <nts id="Seg_2504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T664" id="Seg_2506" n="HIAT:w" s="T663">olorbuttara</ts>
                  <nts id="Seg_2507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T665" id="Seg_2509" n="HIAT:w" s="T664">bu͡olla</ts>
                  <nts id="Seg_2510" n="HIAT:ip">.</nts>
                  <nts id="Seg_2511" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T676" id="Seg_2513" n="HIAT:u" s="T665">
                  <ts e="T666" id="Seg_2515" n="HIAT:w" s="T665">Dʼaktardammɨtɨttan</ts>
                  <nts id="Seg_2516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T667" id="Seg_2518" n="HIAT:w" s="T666">U͡ol</ts>
                  <nts id="Seg_2519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T668" id="Seg_2521" n="HIAT:w" s="T667">ɨraːktaːgɨ</ts>
                  <nts id="Seg_2522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T669" id="Seg_2524" n="HIAT:w" s="T668">kuːkula</ts>
                  <nts id="Seg_2525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T670" id="Seg_2527" n="HIAT:w" s="T669">kurduk</ts>
                  <nts id="Seg_2528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T671" id="Seg_2530" n="HIAT:w" s="T670">olorčok</ts>
                  <nts id="Seg_2531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T672" id="Seg_2533" n="HIAT:w" s="T671">bu͡olla</ts>
                  <nts id="Seg_2534" n="HIAT:ip">,</nts>
                  <nts id="Seg_2535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T673" id="Seg_2537" n="HIAT:w" s="T672">barɨtɨn</ts>
                  <nts id="Seg_2538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T674" id="Seg_2540" n="HIAT:w" s="T673">dʼaktara</ts>
                  <nts id="Seg_2541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T675" id="Seg_2543" n="HIAT:w" s="T674">hubeliːr</ts>
                  <nts id="Seg_2544" n="HIAT:ip">,</nts>
                  <nts id="Seg_2545" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T676" id="Seg_2547" n="HIAT:w" s="T675">bɨhaːrar</ts>
                  <nts id="Seg_2548" n="HIAT:ip">.</nts>
                  <nts id="Seg_2549" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T679" id="Seg_2551" n="HIAT:u" s="T676">
                  <ts e="T677" id="Seg_2553" n="HIAT:w" s="T676">U͡ol</ts>
                  <nts id="Seg_2554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T678" id="Seg_2556" n="HIAT:w" s="T677">ɨraːktaːgɨ</ts>
                  <nts id="Seg_2557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T679" id="Seg_2559" n="HIAT:w" s="T678">tɨllanar</ts>
                  <nts id="Seg_2560" n="HIAT:ip">:</nts>
                  <nts id="Seg_2561" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T687" id="Seg_2563" n="HIAT:u" s="T679">
                  <nts id="Seg_2564" n="HIAT:ip">"</nts>
                  <ts e="T680" id="Seg_2566" n="HIAT:w" s="T679">Dogoː</ts>
                  <nts id="Seg_2567" n="HIAT:ip">,</nts>
                  <nts id="Seg_2568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T681" id="Seg_2570" n="HIAT:w" s="T680">bu</ts>
                  <nts id="Seg_2571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T682" id="Seg_2573" n="HIAT:w" s="T681">biːr</ts>
                  <nts id="Seg_2574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T683" id="Seg_2576" n="HIAT:w" s="T682">dojduga</ts>
                  <nts id="Seg_2577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T684" id="Seg_2579" n="HIAT:w" s="T683">ikki</ts>
                  <nts id="Seg_2580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T685" id="Seg_2582" n="HIAT:w" s="T684">ɨraːktaːgɨ</ts>
                  <nts id="Seg_2583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T686" id="Seg_2585" n="HIAT:w" s="T685">bu͡olarbɨt</ts>
                  <nts id="Seg_2586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T687" id="Seg_2588" n="HIAT:w" s="T686">tabɨllɨbat</ts>
                  <nts id="Seg_2589" n="HIAT:ip">.</nts>
                  <nts id="Seg_2590" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T689" id="Seg_2592" n="HIAT:u" s="T687">
                  <ts e="T688" id="Seg_2594" n="HIAT:w" s="T687">Bihigi</ts>
                  <nts id="Seg_2595" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T689" id="Seg_2597" n="HIAT:w" s="T688">araksɨ͡ak</ts>
                  <nts id="Seg_2598" n="HIAT:ip">.</nts>
                  <nts id="Seg_2599" n="HIAT:ip">"</nts>
                  <nts id="Seg_2600" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T690" id="Seg_2602" n="HIAT:u" s="T689">
                  <ts e="T690" id="Seg_2604" n="HIAT:w" s="T689">Dʼaktara</ts>
                  <nts id="Seg_2605" n="HIAT:ip">:</nts>
                  <nts id="Seg_2606" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T694" id="Seg_2608" n="HIAT:u" s="T690">
                  <nts id="Seg_2609" n="HIAT:ip">"</nts>
                  <ts e="T691" id="Seg_2611" n="HIAT:w" s="T690">Dʼe</ts>
                  <nts id="Seg_2612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T692" id="Seg_2614" n="HIAT:w" s="T691">araksɨ͡ak</ts>
                  <nts id="Seg_2615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T693" id="Seg_2617" n="HIAT:w" s="T692">duː</ts>
                  <nts id="Seg_2618" n="HIAT:ip">"</nts>
                  <nts id="Seg_2619" n="HIAT:ip">,</nts>
                  <nts id="Seg_2620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T694" id="Seg_2622" n="HIAT:w" s="T693">diːr</ts>
                  <nts id="Seg_2623" n="HIAT:ip">.</nts>
                  <nts id="Seg_2624" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T696" id="Seg_2626" n="HIAT:u" s="T694">
                  <ts e="T695" id="Seg_2628" n="HIAT:w" s="T694">U͡ol</ts>
                  <nts id="Seg_2629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T696" id="Seg_2631" n="HIAT:w" s="T695">ɨraːktaːgɨ</ts>
                  <nts id="Seg_2632" n="HIAT:ip">:</nts>
                  <nts id="Seg_2633" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T702" id="Seg_2635" n="HIAT:u" s="T696">
                  <nts id="Seg_2636" n="HIAT:ip">"</nts>
                  <ts e="T697" id="Seg_2638" n="HIAT:w" s="T696">Tu͡ok</ts>
                  <nts id="Seg_2639" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T698" id="Seg_2641" n="HIAT:w" s="T697">bagalaːk</ts>
                  <nts id="Seg_2642" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T699" id="Seg_2644" n="HIAT:w" s="T698">hirgin</ts>
                  <nts id="Seg_2645" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T700" id="Seg_2647" n="HIAT:w" s="T699">barɨtɨn</ts>
                  <nts id="Seg_2648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T701" id="Seg_2650" n="HIAT:w" s="T700">ɨl</ts>
                  <nts id="Seg_2651" n="HIAT:ip">"</nts>
                  <nts id="Seg_2652" n="HIAT:ip">,</nts>
                  <nts id="Seg_2653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T702" id="Seg_2655" n="HIAT:w" s="T701">diːr</ts>
                  <nts id="Seg_2656" n="HIAT:ip">.</nts>
                  <nts id="Seg_2657" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T704" id="Seg_2659" n="HIAT:u" s="T702">
                  <ts e="T703" id="Seg_2661" n="HIAT:w" s="T702">Dʼaktar</ts>
                  <nts id="Seg_2662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T704" id="Seg_2664" n="HIAT:w" s="T703">höbüleher</ts>
                  <nts id="Seg_2665" n="HIAT:ip">.</nts>
                  <nts id="Seg_2666" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T707" id="Seg_2668" n="HIAT:u" s="T704">
                  <ts e="T705" id="Seg_2670" n="HIAT:w" s="T704">Araksar</ts>
                  <nts id="Seg_2671" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T706" id="Seg_2673" n="HIAT:w" s="T705">tüːnnere-keː</ts>
                  <nts id="Seg_2674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T707" id="Seg_2676" n="HIAT:w" s="T706">bu͡olar</ts>
                  <nts id="Seg_2677" n="HIAT:ip">.</nts>
                  <nts id="Seg_2678" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T710" id="Seg_2680" n="HIAT:u" s="T707">
                  <ts e="T708" id="Seg_2682" n="HIAT:w" s="T707">Ere</ts>
                  <nts id="Seg_2683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T709" id="Seg_2685" n="HIAT:w" s="T708">utujan</ts>
                  <nts id="Seg_2686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T710" id="Seg_2688" n="HIAT:w" s="T709">kaːlar</ts>
                  <nts id="Seg_2689" n="HIAT:ip">.</nts>
                  <nts id="Seg_2690" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T720" id="Seg_2692" n="HIAT:u" s="T710">
                  <ts e="T711" id="Seg_2694" n="HIAT:w" s="T710">Dʼaktar</ts>
                  <nts id="Seg_2695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T712" id="Seg_2697" n="HIAT:w" s="T711">erin</ts>
                  <nts id="Seg_2698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T713" id="Seg_2700" n="HIAT:w" s="T712">hu͡organnarɨ</ts>
                  <nts id="Seg_2701" n="HIAT:ip">,</nts>
                  <nts id="Seg_2702" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T714" id="Seg_2704" n="HIAT:w" s="T713">tellekteri</ts>
                  <nts id="Seg_2705" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T715" id="Seg_2707" n="HIAT:w" s="T714">agatɨn</ts>
                  <nts id="Seg_2708" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T716" id="Seg_2710" n="HIAT:w" s="T715">bu͡or</ts>
                  <nts id="Seg_2711" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T717" id="Seg_2713" n="HIAT:w" s="T716">golomo</ts>
                  <nts id="Seg_2714" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T718" id="Seg_2716" n="HIAT:w" s="T717">dʼi͡etiger</ts>
                  <nts id="Seg_2717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T719" id="Seg_2719" n="HIAT:w" s="T718">kötögön</ts>
                  <nts id="Seg_2720" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T720" id="Seg_2722" n="HIAT:w" s="T719">ildʼer</ts>
                  <nts id="Seg_2723" n="HIAT:ip">.</nts>
                  <nts id="Seg_2724" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T731" id="Seg_2726" n="HIAT:u" s="T720">
                  <ts e="T721" id="Seg_2728" n="HIAT:w" s="T720">U͡ol</ts>
                  <nts id="Seg_2729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T722" id="Seg_2731" n="HIAT:w" s="T721">ɨraːktaːgɨ</ts>
                  <nts id="Seg_2732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T723" id="Seg_2734" n="HIAT:w" s="T722">harsi͡erda</ts>
                  <nts id="Seg_2735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T724" id="Seg_2737" n="HIAT:w" s="T723">uhuktan</ts>
                  <nts id="Seg_2738" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T725" id="Seg_2740" n="HIAT:w" s="T724">hohujar</ts>
                  <nts id="Seg_2741" n="HIAT:ip">,</nts>
                  <nts id="Seg_2742" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T726" id="Seg_2744" n="HIAT:w" s="T725">tɨːllan</ts>
                  <nts id="Seg_2745" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T727" id="Seg_2747" n="HIAT:w" s="T726">körbüte</ts>
                  <nts id="Seg_2748" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T728" id="Seg_2750" n="HIAT:w" s="T727">baha-ataga</ts>
                  <nts id="Seg_2751" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T729" id="Seg_2753" n="HIAT:w" s="T728">golomo</ts>
                  <nts id="Seg_2754" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T730" id="Seg_2756" n="HIAT:w" s="T729">mahɨgar</ts>
                  <nts id="Seg_2757" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T731" id="Seg_2759" n="HIAT:w" s="T730">kebiller</ts>
                  <nts id="Seg_2760" n="HIAT:ip">.</nts>
                  <nts id="Seg_2761" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T735" id="Seg_2763" n="HIAT:u" s="T731">
                  <nts id="Seg_2764" n="HIAT:ip">"</nts>
                  <ts e="T732" id="Seg_2766" n="HIAT:w" s="T731">Kaja-keː</ts>
                  <nts id="Seg_2767" n="HIAT:ip">,</nts>
                  <nts id="Seg_2768" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T733" id="Seg_2770" n="HIAT:w" s="T732">bu</ts>
                  <nts id="Seg_2771" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T734" id="Seg_2773" n="HIAT:w" s="T733">kanna</ts>
                  <nts id="Seg_2774" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T735" id="Seg_2776" n="HIAT:w" s="T734">baːrbɨtɨj</ts>
                  <nts id="Seg_2777" n="HIAT:ip">?</nts>
                  <nts id="Seg_2778" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T737" id="Seg_2780" n="HIAT:u" s="T735">
                  <ts e="T736" id="Seg_2782" n="HIAT:w" s="T735">Togo</ts>
                  <nts id="Seg_2783" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T737" id="Seg_2785" n="HIAT:w" s="T736">kelbitimij</ts>
                  <nts id="Seg_2786" n="HIAT:ip">?</nts>
                  <nts id="Seg_2787" n="HIAT:ip">"</nts>
                  <nts id="Seg_2788" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T740" id="Seg_2790" n="HIAT:u" s="T737">
                  <ts e="T738" id="Seg_2792" n="HIAT:w" s="T737">Dʼaktarɨn</ts>
                  <nts id="Seg_2793" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T739" id="Seg_2795" n="HIAT:w" s="T738">kebi͡eleːn</ts>
                  <nts id="Seg_2796" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T740" id="Seg_2798" n="HIAT:w" s="T739">uhugunnarar</ts>
                  <nts id="Seg_2799" n="HIAT:ip">.</nts>
                  <nts id="Seg_2800" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T741" id="Seg_2802" n="HIAT:u" s="T740">
                  <ts e="T741" id="Seg_2804" n="HIAT:w" s="T740">Dʼaktara</ts>
                  <nts id="Seg_2805" n="HIAT:ip">:</nts>
                  <nts id="Seg_2806" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T746" id="Seg_2808" n="HIAT:u" s="T741">
                  <nts id="Seg_2809" n="HIAT:ip">"</nts>
                  <ts e="T742" id="Seg_2811" n="HIAT:w" s="T741">Kaja</ts>
                  <nts id="Seg_2812" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T743" id="Seg_2814" n="HIAT:w" s="T742">dogoː</ts>
                  <nts id="Seg_2815" n="HIAT:ip">,</nts>
                  <nts id="Seg_2816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T744" id="Seg_2818" n="HIAT:w" s="T743">min</ts>
                  <nts id="Seg_2819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T745" id="Seg_2821" n="HIAT:w" s="T744">egelbitim</ts>
                  <nts id="Seg_2822" n="HIAT:ip">"</nts>
                  <nts id="Seg_2823" n="HIAT:ip">,</nts>
                  <nts id="Seg_2824" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T746" id="Seg_2826" n="HIAT:w" s="T745">diːr</ts>
                  <nts id="Seg_2827" n="HIAT:ip">.</nts>
                  <nts id="Seg_2828" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T748" id="Seg_2830" n="HIAT:u" s="T746">
                  <nts id="Seg_2831" n="HIAT:ip">"</nts>
                  <ts e="T747" id="Seg_2833" n="HIAT:w" s="T746">Togo</ts>
                  <nts id="Seg_2834" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T748" id="Seg_2836" n="HIAT:w" s="T747">egelbikkinij</ts>
                  <nts id="Seg_2837" n="HIAT:ip">?</nts>
                  <nts id="Seg_2838" n="HIAT:ip">"</nts>
                  <nts id="Seg_2839" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T754" id="Seg_2841" n="HIAT:u" s="T748">
                  <nts id="Seg_2842" n="HIAT:ip">"</nts>
                  <ts e="T749" id="Seg_2844" n="HIAT:w" s="T748">Tɨːj</ts>
                  <nts id="Seg_2845" n="HIAT:ip">,</nts>
                  <nts id="Seg_2846" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T750" id="Seg_2848" n="HIAT:w" s="T749">bejegin</ts>
                  <nts id="Seg_2849" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T751" id="Seg_2851" n="HIAT:w" s="T750">bagardɨm</ts>
                  <nts id="Seg_2852" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T752" id="Seg_2854" n="HIAT:w" s="T751">da</ts>
                  <nts id="Seg_2855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T753" id="Seg_2857" n="HIAT:w" s="T752">bejegin</ts>
                  <nts id="Seg_2858" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T754" id="Seg_2860" n="HIAT:w" s="T753">egellim</ts>
                  <nts id="Seg_2861" n="HIAT:ip">.</nts>
                  <nts id="Seg_2862" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T761" id="Seg_2864" n="HIAT:u" s="T754">
                  <ts e="T755" id="Seg_2866" n="HIAT:w" s="T754">Tu͡ogu</ts>
                  <nts id="Seg_2867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T756" id="Seg_2869" n="HIAT:w" s="T755">bagarargɨn</ts>
                  <nts id="Seg_2870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T757" id="Seg_2872" n="HIAT:w" s="T756">ɨlaːr</ts>
                  <nts id="Seg_2873" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T758" id="Seg_2875" n="HIAT:w" s="T757">di͡ebitiŋ</ts>
                  <nts id="Seg_2876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T759" id="Seg_2878" n="HIAT:w" s="T758">diː</ts>
                  <nts id="Seg_2879" n="HIAT:ip">"</nts>
                  <nts id="Seg_2880" n="HIAT:ip">,</nts>
                  <nts id="Seg_2881" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T760" id="Seg_2883" n="HIAT:w" s="T759">diːr</ts>
                  <nts id="Seg_2884" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T761" id="Seg_2886" n="HIAT:w" s="T760">dʼaktar</ts>
                  <nts id="Seg_2887" n="HIAT:ip">.</nts>
                  <nts id="Seg_2888" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T765" id="Seg_2890" n="HIAT:u" s="T761">
                  <ts e="T762" id="Seg_2892" n="HIAT:w" s="T761">U͡ol</ts>
                  <nts id="Seg_2893" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T763" id="Seg_2895" n="HIAT:w" s="T762">ɨraːktaːgɨ</ts>
                  <nts id="Seg_2896" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T764" id="Seg_2898" n="HIAT:w" s="T763">kɨ͡ajan</ts>
                  <nts id="Seg_2899" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T765" id="Seg_2901" n="HIAT:w" s="T764">arakpata</ts>
                  <nts id="Seg_2902" n="HIAT:ip">.</nts>
                  <nts id="Seg_2903" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T768" id="Seg_2905" n="HIAT:u" s="T765">
                  <ts e="T766" id="Seg_2907" n="HIAT:w" s="T765">Dʼe</ts>
                  <nts id="Seg_2908" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T767" id="Seg_2910" n="HIAT:w" s="T766">dʼaktara</ts>
                  <nts id="Seg_2911" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T768" id="Seg_2913" n="HIAT:w" s="T767">diːr</ts>
                  <nts id="Seg_2914" n="HIAT:ip">:</nts>
                  <nts id="Seg_2915" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T773" id="Seg_2917" n="HIAT:u" s="T768">
                  <nts id="Seg_2918" n="HIAT:ip">"</nts>
                  <ts e="T769" id="Seg_2920" n="HIAT:w" s="T768">Oččo</ts>
                  <nts id="Seg_2921" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T770" id="Seg_2923" n="HIAT:w" s="T769">öjdöːk</ts>
                  <nts id="Seg_2924" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T771" id="Seg_2926" n="HIAT:w" s="T770">kihi</ts>
                  <nts id="Seg_2927" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T772" id="Seg_2929" n="HIAT:w" s="T771">bu͡olbatak</ts>
                  <nts id="Seg_2930" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T773" id="Seg_2932" n="HIAT:w" s="T772">ebikkin</ts>
                  <nts id="Seg_2933" n="HIAT:ip">.</nts>
                  <nts id="Seg_2934" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T777" id="Seg_2936" n="HIAT:u" s="T773">
                  <ts e="T774" id="Seg_2938" n="HIAT:w" s="T773">Dʼoŋŋun</ts>
                  <nts id="Seg_2939" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T775" id="Seg_2941" n="HIAT:w" s="T774">hübeleːn</ts>
                  <nts id="Seg_2942" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T776" id="Seg_2944" n="HIAT:w" s="T775">bejeŋ</ts>
                  <nts id="Seg_2945" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T777" id="Seg_2947" n="HIAT:w" s="T776">olor</ts>
                  <nts id="Seg_2948" n="HIAT:ip">.</nts>
                  <nts id="Seg_2949" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T782" id="Seg_2951" n="HIAT:u" s="T777">
                  <ts e="T778" id="Seg_2953" n="HIAT:w" s="T777">Min</ts>
                  <nts id="Seg_2954" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T779" id="Seg_2956" n="HIAT:w" s="T778">könnörü</ts>
                  <nts id="Seg_2957" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T780" id="Seg_2959" n="HIAT:w" s="T779">küseːjke</ts>
                  <nts id="Seg_2960" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T781" id="Seg_2962" n="HIAT:w" s="T780">bu͡olan</ts>
                  <nts id="Seg_2963" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T782" id="Seg_2965" n="HIAT:w" s="T781">oloru͡om</ts>
                  <nts id="Seg_2966" n="HIAT:ip">.</nts>
                  <nts id="Seg_2967" n="HIAT:ip">"</nts>
                  <nts id="Seg_2968" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T794" id="Seg_2970" n="HIAT:u" s="T782">
                  <ts e="T783" id="Seg_2972" n="HIAT:w" s="T782">Umnahɨt</ts>
                  <nts id="Seg_2973" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T784" id="Seg_2975" n="HIAT:w" s="T783">paːsɨnaj</ts>
                  <nts id="Seg_2976" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T785" id="Seg_2978" n="HIAT:w" s="T784">kɨːhɨn</ts>
                  <nts id="Seg_2979" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T786" id="Seg_2981" n="HIAT:w" s="T785">ütü͡ötünen</ts>
                  <nts id="Seg_2982" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T787" id="Seg_2984" n="HIAT:w" s="T786">hi͡erkile</ts>
                  <nts id="Seg_2985" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T788" id="Seg_2987" n="HIAT:w" s="T787">taːs</ts>
                  <nts id="Seg_2988" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T789" id="Seg_2990" n="HIAT:w" s="T788">dʼi͡ege</ts>
                  <nts id="Seg_2991" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T790" id="Seg_2993" n="HIAT:w" s="T789">olordogo</ts>
                  <nts id="Seg_2994" n="HIAT:ip">,</nts>
                  <nts id="Seg_2995" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T791" id="Seg_2997" n="HIAT:w" s="T790">bu͡or</ts>
                  <nts id="Seg_2998" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T792" id="Seg_3000" n="HIAT:w" s="T791">golomotton</ts>
                  <nts id="Seg_3001" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T793" id="Seg_3003" n="HIAT:w" s="T792">araksan</ts>
                  <nts id="Seg_3004" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T794" id="Seg_3006" n="HIAT:w" s="T793">uhuguttan</ts>
                  <nts id="Seg_3007" n="HIAT:ip">.</nts>
                  <nts id="Seg_3008" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T797" id="Seg_3010" n="HIAT:u" s="T794">
                  <ts e="T795" id="Seg_3012" n="HIAT:w" s="T794">Bajan-tajan</ts>
                  <nts id="Seg_3013" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T796" id="Seg_3015" n="HIAT:w" s="T795">olordoktoro</ts>
                  <nts id="Seg_3016" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T797" id="Seg_3018" n="HIAT:w" s="T796">dʼe</ts>
                  <nts id="Seg_3019" n="HIAT:ip">.</nts>
                  <nts id="Seg_3020" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T798" id="Seg_3022" n="HIAT:u" s="T797">
                  <ts e="T798" id="Seg_3024" n="HIAT:w" s="T797">Elete</ts>
                  <nts id="Seg_3025" n="HIAT:ip">.</nts>
                  <nts id="Seg_3026" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T798" id="Seg_3027" n="sc" s="T0">
               <ts e="T1" id="Seg_3029" n="e" s="T0">Dʼe </ts>
               <ts e="T2" id="Seg_3031" n="e" s="T1">U͡ol </ts>
               <ts e="T3" id="Seg_3033" n="e" s="T2">ɨraːktaːgɨ </ts>
               <ts e="T4" id="Seg_3035" n="e" s="T3">olorbuta. </ts>
               <ts e="T5" id="Seg_3037" n="e" s="T4">Ulkatɨgar </ts>
               <ts e="T6" id="Seg_3039" n="e" s="T5">Umnahɨt </ts>
               <ts e="T7" id="Seg_3041" n="e" s="T6">paːsɨnajdaːk. </ts>
               <ts e="T8" id="Seg_3043" n="e" s="T7">Umnahɨt </ts>
               <ts e="T9" id="Seg_3045" n="e" s="T8">paːsɨnaj </ts>
               <ts e="T10" id="Seg_3047" n="e" s="T9">bu </ts>
               <ts e="T11" id="Seg_3049" n="e" s="T10">ɨraːktaːgɨga </ts>
               <ts e="T12" id="Seg_3051" n="e" s="T11">üleleːn </ts>
               <ts e="T13" id="Seg_3053" n="e" s="T12">harsi͡erdaːŋŋɨ </ts>
               <ts e="T14" id="Seg_3055" n="e" s="T13">karaŋattan </ts>
               <ts e="T15" id="Seg_3057" n="e" s="T14">ki͡ehe </ts>
               <ts e="T16" id="Seg_3059" n="e" s="T15">karaŋaga </ts>
               <ts e="T17" id="Seg_3061" n="e" s="T16">di͡eri </ts>
               <ts e="T18" id="Seg_3063" n="e" s="T17">biːr </ts>
               <ts e="T19" id="Seg_3065" n="e" s="T18">lu͡osku </ts>
               <ts e="T20" id="Seg_3067" n="e" s="T19">burduk </ts>
               <ts e="T21" id="Seg_3069" n="e" s="T20">ihin </ts>
               <ts e="T22" id="Seg_3071" n="e" s="T21">üleliːr </ts>
               <ts e="T23" id="Seg_3073" n="e" s="T22">ebit. </ts>
               <ts e="T24" id="Seg_3075" n="e" s="T23">Manan </ts>
               <ts e="T25" id="Seg_3077" n="e" s="T24">iːtten </ts>
               <ts e="T26" id="Seg_3079" n="e" s="T25">olorollor. </ts>
               <ts e="T27" id="Seg_3081" n="e" s="T26">Umnahɨt </ts>
               <ts e="T28" id="Seg_3083" n="e" s="T27">paːsɨnaj </ts>
               <ts e="T29" id="Seg_3085" n="e" s="T28">biːr </ts>
               <ts e="T30" id="Seg_3087" n="e" s="T29">kɨːs </ts>
               <ts e="T31" id="Seg_3089" n="e" s="T30">ogoloːk. </ts>
               <ts e="T32" id="Seg_3091" n="e" s="T31">Dʼe </ts>
               <ts e="T33" id="Seg_3093" n="e" s="T32">biːrde </ts>
               <ts e="T34" id="Seg_3095" n="e" s="T33">U͡ol </ts>
               <ts e="T35" id="Seg_3097" n="e" s="T34">ɨraːktaːgɨ </ts>
               <ts e="T36" id="Seg_3099" n="e" s="T35">diːr: </ts>
               <ts e="T37" id="Seg_3101" n="e" s="T36">"Kaja </ts>
               <ts e="T38" id="Seg_3103" n="e" s="T37">dʼe, </ts>
               <ts e="T39" id="Seg_3105" n="e" s="T38">Umnahɨt </ts>
               <ts e="T40" id="Seg_3107" n="e" s="T39">paːsɨnaj, </ts>
               <ts e="T41" id="Seg_3109" n="e" s="T40">töhö </ts>
               <ts e="T42" id="Seg_3111" n="e" s="T41">öjdöːk </ts>
               <ts e="T43" id="Seg_3113" n="e" s="T42">kihi </ts>
               <ts e="T44" id="Seg_3115" n="e" s="T43">töröːn </ts>
               <ts e="T45" id="Seg_3117" n="e" s="T44">tönünnüŋ? </ts>
               <ts e="T46" id="Seg_3119" n="e" s="T45">Taːburunna </ts>
               <ts e="T47" id="Seg_3121" n="e" s="T46">taːj, </ts>
               <ts e="T48" id="Seg_3123" n="e" s="T47">minnʼigesten </ts>
               <ts e="T49" id="Seg_3125" n="e" s="T48">minnʼiges </ts>
               <ts e="T50" id="Seg_3127" n="e" s="T49">tu͡oguj? </ts>
               <ts e="T51" id="Seg_3129" n="e" s="T50">Harsi͡erda </ts>
               <ts e="T52" id="Seg_3131" n="e" s="T51">kelen </ts>
               <ts e="T53" id="Seg_3133" n="e" s="T52">togus </ts>
               <ts e="T54" id="Seg_3135" n="e" s="T53">čaːska </ts>
               <ts e="T55" id="Seg_3137" n="e" s="T54">haŋaraːr. </ts>
               <ts e="T56" id="Seg_3139" n="e" s="T55">Onu </ts>
               <ts e="T57" id="Seg_3141" n="e" s="T56">taːjbatakkɨna </ts>
               <ts e="T58" id="Seg_3143" n="e" s="T57">ɨjɨːr </ts>
               <ts e="T59" id="Seg_3145" n="e" s="T58">maspar </ts>
               <ts e="T60" id="Seg_3147" n="e" s="T59">moːjdoːk </ts>
               <ts e="T61" id="Seg_3149" n="e" s="T60">baskɨn </ts>
               <ts e="T62" id="Seg_3151" n="e" s="T61">bɨhɨ͡am. </ts>
               <ts e="T63" id="Seg_3153" n="e" s="T62">Umnahɨt </ts>
               <ts e="T64" id="Seg_3155" n="e" s="T63">paːsɨnaj </ts>
               <ts e="T65" id="Seg_3157" n="e" s="T64">togo </ts>
               <ts e="T66" id="Seg_3159" n="e" s="T65">da </ts>
               <ts e="T67" id="Seg_3161" n="e" s="T66">öjö </ts>
               <ts e="T68" id="Seg_3163" n="e" s="T67">kɨ͡ajbat, </ts>
               <ts e="T69" id="Seg_3165" n="e" s="T68">minnʼiges </ts>
               <ts e="T70" id="Seg_3167" n="e" s="T69">ahɨ </ts>
               <ts e="T71" id="Seg_3169" n="e" s="T70">bileːkteːbet, </ts>
               <ts e="T72" id="Seg_3171" n="e" s="T71">ɨraːktaːgɨ </ts>
               <ts e="T73" id="Seg_3173" n="e" s="T72">aha </ts>
               <ts e="T74" id="Seg_3175" n="e" s="T73">barɨta </ts>
               <ts e="T75" id="Seg_3177" n="e" s="T74">minnʼiges </ts>
               <ts e="T76" id="Seg_3179" n="e" s="T75">kurduk </ts>
               <ts e="T77" id="Seg_3181" n="e" s="T76">gini͡eke. </ts>
               <ts e="T78" id="Seg_3183" n="e" s="T77">Dʼi͡etiger </ts>
               <ts e="T79" id="Seg_3185" n="e" s="T78">kelen </ts>
               <ts e="T80" id="Seg_3187" n="e" s="T79">ɨtana </ts>
               <ts e="T81" id="Seg_3189" n="e" s="T80">olordo. </ts>
               <ts e="T82" id="Seg_3191" n="e" s="T81">Kɨːha </ts>
               <ts e="T83" id="Seg_3193" n="e" s="T82">ɨjɨtar: </ts>
               <ts e="T84" id="Seg_3195" n="e" s="T83">"Kaja, </ts>
               <ts e="T85" id="Seg_3197" n="e" s="T84">agaː, </ts>
               <ts e="T86" id="Seg_3199" n="e" s="T85">togo </ts>
               <ts e="T87" id="Seg_3201" n="e" s="T86">ɨtaːtɨn?" </ts>
               <ts e="T88" id="Seg_3203" n="e" s="T87">"Dʼe, </ts>
               <ts e="T89" id="Seg_3205" n="e" s="T88">togojum, </ts>
               <ts e="T90" id="Seg_3207" n="e" s="T89">hɨldʼɨbɨtɨm </ts>
               <ts e="T91" id="Seg_3209" n="e" s="T90">hɨččak, </ts>
               <ts e="T92" id="Seg_3211" n="e" s="T91">olorbutum </ts>
               <ts e="T93" id="Seg_3213" n="e" s="T92">uhuga. </ts>
               <ts e="T94" id="Seg_3215" n="e" s="T93">U͡ol </ts>
               <ts e="T95" id="Seg_3217" n="e" s="T94">ɨraːktaːgɨ </ts>
               <ts e="T96" id="Seg_3219" n="e" s="T95">taːburuːn </ts>
               <ts e="T97" id="Seg_3221" n="e" s="T96">bi͡erde. </ts>
               <ts e="T98" id="Seg_3223" n="e" s="T97">Taːjbatakpɨna </ts>
               <ts e="T99" id="Seg_3225" n="e" s="T98">moːjdoːk </ts>
               <ts e="T100" id="Seg_3227" n="e" s="T99">baspɨn </ts>
               <ts e="T101" id="Seg_3229" n="e" s="T100">bɨhɨ͡ak </ts>
               <ts e="T102" id="Seg_3231" n="e" s="T101">bu͡olla." </ts>
               <ts e="T103" id="Seg_3233" n="e" s="T102">"Tugu </ts>
               <ts e="T104" id="Seg_3235" n="e" s="T103">taːttarar </ts>
               <ts e="T105" id="Seg_3237" n="e" s="T104">ol?" </ts>
               <ts e="T106" id="Seg_3239" n="e" s="T105">""Minnʼigesten </ts>
               <ts e="T107" id="Seg_3241" n="e" s="T106">minnʼiges </ts>
               <ts e="T108" id="Seg_3243" n="e" s="T107">tu͡oguj" </ts>
               <ts e="T109" id="Seg_3245" n="e" s="T108">diːr", </ts>
               <ts e="T110" id="Seg_3247" n="e" s="T109">di͡ebit. </ts>
               <ts e="T111" id="Seg_3249" n="e" s="T110">"Eː, </ts>
               <ts e="T112" id="Seg_3251" n="e" s="T111">agaː, </ts>
               <ts e="T113" id="Seg_3253" n="e" s="T112">tuhata </ts>
               <ts e="T114" id="Seg_3255" n="e" s="T113">hu͡okka </ts>
               <ts e="T115" id="Seg_3257" n="e" s="T114">togo </ts>
               <ts e="T116" id="Seg_3259" n="e" s="T115">ɨtɨːgɨn. </ts>
               <ts e="T117" id="Seg_3261" n="e" s="T116">Utujaːktaː, </ts>
               <ts e="T118" id="Seg_3263" n="e" s="T117">hɨnnʼan. </ts>
               <ts e="T119" id="Seg_3265" n="e" s="T118">"Minnʼigesten </ts>
               <ts e="T120" id="Seg_3267" n="e" s="T119">minnʼigehi </ts>
               <ts e="T121" id="Seg_3269" n="e" s="T120">biler </ts>
               <ts e="T122" id="Seg_3271" n="e" s="T121">hirim </ts>
               <ts e="T123" id="Seg_3273" n="e" s="T122">biːr", </ts>
               <ts e="T124" id="Seg_3275" n="e" s="T123">di͡er. </ts>
               <ts e="T125" id="Seg_3277" n="e" s="T124">"Harsi͡erdaːŋŋɨ </ts>
               <ts e="T126" id="Seg_3279" n="e" s="T125">karaŋattan </ts>
               <ts e="T127" id="Seg_3281" n="e" s="T126">ki͡eheːŋŋi </ts>
               <ts e="T128" id="Seg_3283" n="e" s="T127">karaŋaga </ts>
               <ts e="T129" id="Seg_3285" n="e" s="T128">di͡eri </ts>
               <ts e="T130" id="Seg_3287" n="e" s="T129">üleleːn, </ts>
               <ts e="T131" id="Seg_3289" n="e" s="T130">en </ts>
               <ts e="T132" id="Seg_3291" n="e" s="T131">haːkkɨn-kirgin </ts>
               <ts e="T133" id="Seg_3293" n="e" s="T132">ɨraːstaːn, </ts>
               <ts e="T134" id="Seg_3295" n="e" s="T133">biːr </ts>
               <ts e="T135" id="Seg_3297" n="e" s="T134">lu͡osku </ts>
               <ts e="T136" id="Seg_3299" n="e" s="T135">burdugu </ts>
               <ts e="T137" id="Seg_3301" n="e" s="T136">iltekpine, </ts>
               <ts e="T138" id="Seg_3303" n="e" s="T137">emeːksinim </ts>
               <ts e="T139" id="Seg_3305" n="e" s="T138">bu͡or </ts>
               <ts e="T140" id="Seg_3307" n="e" s="T139">golomo </ts>
               <ts e="T141" id="Seg_3309" n="e" s="T140">dʼi͡etin </ts>
               <ts e="T142" id="Seg_3311" n="e" s="T141">otunnagɨna, </ts>
               <ts e="T143" id="Seg_3313" n="e" s="T142">munnum </ts>
               <ts e="T144" id="Seg_3315" n="e" s="T143">hɨlɨjar. </ts>
               <ts e="T145" id="Seg_3317" n="e" s="T144">Ol </ts>
               <ts e="T146" id="Seg_3319" n="e" s="T145">burdukkaːnɨnan </ts>
               <ts e="T147" id="Seg_3321" n="e" s="T146">toloŋku͡o </ts>
               <ts e="T148" id="Seg_3323" n="e" s="T147">oŋordoguna, </ts>
               <ts e="T149" id="Seg_3325" n="e" s="T148">ahaːbakka </ts>
               <ts e="T150" id="Seg_3327" n="e" s="T149">da </ts>
               <ts e="T151" id="Seg_3329" n="e" s="T150">utujabɨn. </ts>
               <ts e="T152" id="Seg_3331" n="e" s="T151">Onon </ts>
               <ts e="T153" id="Seg_3333" n="e" s="T152">minnʼigesten </ts>
               <ts e="T154" id="Seg_3335" n="e" s="T153">minnʼiges </ts>
               <ts e="T155" id="Seg_3337" n="e" s="T154">utujar </ts>
               <ts e="T156" id="Seg_3339" n="e" s="T155">uː </ts>
               <ts e="T157" id="Seg_3341" n="e" s="T156">bu͡olu͡oktaːk", </ts>
               <ts e="T158" id="Seg_3343" n="e" s="T157">di͡er. </ts>
               <ts e="T159" id="Seg_3345" n="e" s="T158">Kini </ts>
               <ts e="T160" id="Seg_3347" n="e" s="T159">onton </ts>
               <ts e="T161" id="Seg_3349" n="e" s="T160">da </ts>
               <ts e="T162" id="Seg_3351" n="e" s="T161">ordugu </ts>
               <ts e="T163" id="Seg_3353" n="e" s="T162">bilere </ts>
               <ts e="T164" id="Seg_3355" n="e" s="T163">bu͡olu͡o </ts>
               <ts e="T165" id="Seg_3357" n="e" s="T164">du͡o", </ts>
               <ts e="T166" id="Seg_3359" n="e" s="T165">di͡ebit </ts>
               <ts e="T167" id="Seg_3361" n="e" s="T166">kɨːha. </ts>
               <ts e="T168" id="Seg_3363" n="e" s="T167">Ogonnʼor </ts>
               <ts e="T169" id="Seg_3365" n="e" s="T168">utujan </ts>
               <ts e="T170" id="Seg_3367" n="e" s="T169">kaːlla. </ts>
               <ts e="T171" id="Seg_3369" n="e" s="T170">Harsi͡erda </ts>
               <ts e="T172" id="Seg_3371" n="e" s="T171">erde </ts>
               <ts e="T173" id="Seg_3373" n="e" s="T172">turan </ts>
               <ts e="T174" id="Seg_3375" n="e" s="T173">togus </ts>
               <ts e="T175" id="Seg_3377" n="e" s="T174">čaːska </ts>
               <ts e="T176" id="Seg_3379" n="e" s="T175">ɨraːktɨːgɨga </ts>
               <ts e="T177" id="Seg_3381" n="e" s="T176">tijer. </ts>
               <ts e="T178" id="Seg_3383" n="e" s="T177">U͡ol </ts>
               <ts e="T179" id="Seg_3385" n="e" s="T178">ɨraːktaːgɨ </ts>
               <ts e="T180" id="Seg_3387" n="e" s="T179">ɨjɨtar: </ts>
               <ts e="T181" id="Seg_3389" n="e" s="T180">"Kaja, </ts>
               <ts e="T182" id="Seg_3391" n="e" s="T181">taːjdɨŋ </ts>
               <ts e="T183" id="Seg_3393" n="e" s="T182">du͡o?" </ts>
               <ts e="T184" id="Seg_3395" n="e" s="T183">"Eː, </ts>
               <ts e="T185" id="Seg_3397" n="e" s="T184">taːjarga </ts>
               <ts e="T186" id="Seg_3399" n="e" s="T185">talɨ </ts>
               <ts e="T187" id="Seg_3401" n="e" s="T186">gɨnnɨm. </ts>
               <ts e="T188" id="Seg_3403" n="e" s="T187">Höbö </ts>
               <ts e="T189" id="Seg_3405" n="e" s="T188">duː, </ts>
               <ts e="T190" id="Seg_3407" n="e" s="T189">hɨːhata </ts>
               <ts e="T191" id="Seg_3409" n="e" s="T190">duː", </ts>
               <ts e="T192" id="Seg_3411" n="e" s="T191">diːr. </ts>
               <ts e="T193" id="Seg_3413" n="e" s="T192">"Dʼe </ts>
               <ts e="T194" id="Seg_3415" n="e" s="T193">tuguj?" </ts>
               <ts e="T195" id="Seg_3417" n="e" s="T194">"Harsi͡erdaːŋŋɨ </ts>
               <ts e="T196" id="Seg_3419" n="e" s="T195">karaŋattan </ts>
               <ts e="T197" id="Seg_3421" n="e" s="T196">ki͡eheːŋŋi </ts>
               <ts e="T198" id="Seg_3423" n="e" s="T197">karaŋaga </ts>
               <ts e="T199" id="Seg_3425" n="e" s="T198">di͡eri </ts>
               <ts e="T200" id="Seg_3427" n="e" s="T199">üleleːn, </ts>
               <ts e="T201" id="Seg_3429" n="e" s="T200">en </ts>
               <ts e="T202" id="Seg_3431" n="e" s="T201">haːkkɨn-kirgin </ts>
               <ts e="T203" id="Seg_3433" n="e" s="T202">ɨraːstaːn, </ts>
               <ts e="T204" id="Seg_3435" n="e" s="T203">biːr </ts>
               <ts e="T205" id="Seg_3437" n="e" s="T204">lu͡osku </ts>
               <ts e="T206" id="Seg_3439" n="e" s="T205">burdugu </ts>
               <ts e="T207" id="Seg_3441" n="e" s="T206">iltekpine, </ts>
               <ts e="T208" id="Seg_3443" n="e" s="T207">emeːksinim </ts>
               <ts e="T209" id="Seg_3445" n="e" s="T208">bu͡or </ts>
               <ts e="T210" id="Seg_3447" n="e" s="T209">golomo </ts>
               <ts e="T211" id="Seg_3449" n="e" s="T210">dʼi͡etin </ts>
               <ts e="T212" id="Seg_3451" n="e" s="T211">otunnagɨna, </ts>
               <ts e="T213" id="Seg_3453" n="e" s="T212">munnum </ts>
               <ts e="T214" id="Seg_3455" n="e" s="T213">hɨlɨjar. </ts>
               <ts e="T215" id="Seg_3457" n="e" s="T214">Ol </ts>
               <ts e="T216" id="Seg_3459" n="e" s="T215">burdukkaːnɨnan </ts>
               <ts e="T217" id="Seg_3461" n="e" s="T216">toloŋku͡o </ts>
               <ts e="T218" id="Seg_3463" n="e" s="T217">oŋordoguna, </ts>
               <ts e="T219" id="Seg_3465" n="e" s="T218">ahaːbakka </ts>
               <ts e="T220" id="Seg_3467" n="e" s="T219">da </ts>
               <ts e="T221" id="Seg_3469" n="e" s="T220">utujabɨn. </ts>
               <ts e="T222" id="Seg_3471" n="e" s="T221">Onon </ts>
               <ts e="T223" id="Seg_3473" n="e" s="T222">minnʼigesten </ts>
               <ts e="T224" id="Seg_3475" n="e" s="T223">minnʼiges </ts>
               <ts e="T225" id="Seg_3477" n="e" s="T224">utujar </ts>
               <ts e="T226" id="Seg_3479" n="e" s="T225">uː </ts>
               <ts e="T227" id="Seg_3481" n="e" s="T226">bu͡olu͡oktaːk." </ts>
               <ts e="T228" id="Seg_3483" n="e" s="T227">U͡ol </ts>
               <ts e="T229" id="Seg_3485" n="e" s="T228">ɨraːktaːgɨ </ts>
               <ts e="T230" id="Seg_3487" n="e" s="T229">diːr: </ts>
               <ts e="T231" id="Seg_3489" n="e" s="T230">"Oː </ts>
               <ts e="T232" id="Seg_3491" n="e" s="T231">dʼe, </ts>
               <ts e="T233" id="Seg_3493" n="e" s="T232">öjdöːk </ts>
               <ts e="T234" id="Seg_3495" n="e" s="T233">kihigin </ts>
               <ts e="T235" id="Seg_3497" n="e" s="T234">ebit. </ts>
               <ts e="T236" id="Seg_3499" n="e" s="T235">Kajdak </ts>
               <ts e="T237" id="Seg_3501" n="e" s="T236">inen-batan </ts>
               <ts e="T238" id="Seg_3503" n="e" s="T237">hɨtagɨn? </ts>
               <ts e="T239" id="Seg_3505" n="e" s="T238">Össü͡ö </ts>
               <ts e="T240" id="Seg_3507" n="e" s="T239">taːj, </ts>
               <ts e="T241" id="Seg_3509" n="e" s="T240">orto </ts>
               <ts e="T242" id="Seg_3511" n="e" s="T241">dojduga </ts>
               <ts e="T243" id="Seg_3513" n="e" s="T242">türgenten </ts>
               <ts e="T244" id="Seg_3515" n="e" s="T243">türgen </ts>
               <ts e="T245" id="Seg_3517" n="e" s="T244">tu͡ok </ts>
               <ts e="T246" id="Seg_3519" n="e" s="T245">baːrɨj? </ts>
               <ts e="T247" id="Seg_3521" n="e" s="T246">Harsi͡erda </ts>
               <ts e="T248" id="Seg_3523" n="e" s="T247">taːjbatakkɨna, </ts>
               <ts e="T249" id="Seg_3525" n="e" s="T248">moːjdoːk </ts>
               <ts e="T250" id="Seg_3527" n="e" s="T249">baskɨn </ts>
               <ts e="T251" id="Seg_3529" n="e" s="T250">bɨhabɨn." </ts>
               <ts e="T252" id="Seg_3531" n="e" s="T251">Ogonnʼor </ts>
               <ts e="T253" id="Seg_3533" n="e" s="T252">tugu </ts>
               <ts e="T254" id="Seg_3535" n="e" s="T253">da </ts>
               <ts e="T255" id="Seg_3537" n="e" s="T254">tobulbat. </ts>
               <ts e="T256" id="Seg_3539" n="e" s="T255">Tijen </ts>
               <ts e="T257" id="Seg_3541" n="e" s="T256">ɨtana </ts>
               <ts e="T258" id="Seg_3543" n="e" s="T257">olordoguna, </ts>
               <ts e="T259" id="Seg_3545" n="e" s="T258">kɨːha </ts>
               <ts e="T260" id="Seg_3547" n="e" s="T259">ɨjɨtar. </ts>
               <ts e="T261" id="Seg_3549" n="e" s="T260">Ogonnʼor </ts>
               <ts e="T262" id="Seg_3551" n="e" s="T261">innʼe-innʼe </ts>
               <ts e="T263" id="Seg_3553" n="e" s="T262">diːr. </ts>
               <ts e="T264" id="Seg_3555" n="e" s="T263">"Tu͡ok </ts>
               <ts e="T265" id="Seg_3557" n="e" s="T264">tuhata </ts>
               <ts e="T266" id="Seg_3559" n="e" s="T265">hu͡okka </ts>
               <ts e="T267" id="Seg_3561" n="e" s="T266">ɨtɨːgɨn, </ts>
               <ts e="T268" id="Seg_3563" n="e" s="T267">utuj. </ts>
               <ts e="T269" id="Seg_3565" n="e" s="T268">Harsi͡erda </ts>
               <ts e="T270" id="Seg_3567" n="e" s="T269">eteːr </ts>
               <ts e="T271" id="Seg_3569" n="e" s="T270">"türgenten </ts>
               <ts e="T272" id="Seg_3571" n="e" s="T271">türgen" </ts>
               <ts e="T273" id="Seg_3573" n="e" s="T272">di͡er, </ts>
               <ts e="T274" id="Seg_3575" n="e" s="T273">"kuhagan </ts>
               <ts e="T275" id="Seg_3577" n="e" s="T274">buru͡olaːk </ts>
               <ts e="T276" id="Seg_3579" n="e" s="T275">bu͡or </ts>
               <ts e="T277" id="Seg_3581" n="e" s="T276">golomobuttan </ts>
               <ts e="T278" id="Seg_3583" n="e" s="T277">taksa </ts>
               <ts e="T279" id="Seg_3585" n="e" s="T278">kötön, </ts>
               <ts e="T280" id="Seg_3587" n="e" s="T279">karakpɨn </ts>
               <ts e="T281" id="Seg_3589" n="e" s="T280">kastɨrɨta </ts>
               <ts e="T282" id="Seg_3591" n="e" s="T281">hoton </ts>
               <ts e="T283" id="Seg_3593" n="e" s="T282">oduːlaːn </ts>
               <ts e="T284" id="Seg_3595" n="e" s="T283">kördökpüne, </ts>
               <ts e="T285" id="Seg_3597" n="e" s="T284">kallaːn </ts>
               <ts e="T286" id="Seg_3599" n="e" s="T285">di͡ek </ts>
               <ts e="T287" id="Seg_3601" n="e" s="T286">kantajdakpɨna— </ts>
               <ts e="T288" id="Seg_3603" n="e" s="T287">kallaːn </ts>
               <ts e="T289" id="Seg_3605" n="e" s="T288">huluhun </ts>
               <ts e="T290" id="Seg_3607" n="e" s="T289">karagɨm </ts>
               <ts e="T291" id="Seg_3609" n="e" s="T290">aːga </ts>
               <ts e="T292" id="Seg_3611" n="e" s="T291">körör, </ts>
               <ts e="T293" id="Seg_3613" n="e" s="T292">hir </ts>
               <ts e="T294" id="Seg_3615" n="e" s="T293">di͡ek </ts>
               <ts e="T295" id="Seg_3617" n="e" s="T294">kördökpüne— </ts>
               <ts e="T296" id="Seg_3619" n="e" s="T295">hir </ts>
               <ts e="T297" id="Seg_3621" n="e" s="T296">otun-mahɨn </ts>
               <ts e="T298" id="Seg_3623" n="e" s="T297">aːga </ts>
               <ts e="T299" id="Seg_3625" n="e" s="T298">köröbün. </ts>
               <ts e="T300" id="Seg_3627" n="e" s="T299">Karagɨm </ts>
               <ts e="T301" id="Seg_3629" n="e" s="T300">ɨlarɨgar </ts>
               <ts e="T302" id="Seg_3631" n="e" s="T301">kahan </ts>
               <ts e="T303" id="Seg_3633" n="e" s="T302">da </ts>
               <ts e="T304" id="Seg_3635" n="e" s="T303">tiːjbeppin. </ts>
               <ts e="T305" id="Seg_3637" n="e" s="T304">Onon </ts>
               <ts e="T306" id="Seg_3639" n="e" s="T305">türgenten </ts>
               <ts e="T307" id="Seg_3641" n="e" s="T306">türgen </ts>
               <ts e="T308" id="Seg_3643" n="e" s="T307">kihi </ts>
               <ts e="T309" id="Seg_3645" n="e" s="T308">körüːte", </ts>
               <ts e="T310" id="Seg_3647" n="e" s="T309">di͡er. </ts>
               <ts e="T311" id="Seg_3649" n="e" s="T310">Onton </ts>
               <ts e="T312" id="Seg_3651" n="e" s="T311">ordugu </ts>
               <ts e="T313" id="Seg_3653" n="e" s="T312">kini </ts>
               <ts e="T314" id="Seg_3655" n="e" s="T313">bilere </ts>
               <ts e="T315" id="Seg_3657" n="e" s="T314">bu͡olu͡o </ts>
               <ts e="T316" id="Seg_3659" n="e" s="T315">du͡o", </ts>
               <ts e="T317" id="Seg_3661" n="e" s="T316">diːr </ts>
               <ts e="T318" id="Seg_3663" n="e" s="T317">kɨːs. </ts>
               <ts e="T319" id="Seg_3665" n="e" s="T318">Ogonnʼor </ts>
               <ts e="T320" id="Seg_3667" n="e" s="T319">utujan </ts>
               <ts e="T321" id="Seg_3669" n="e" s="T320">kaːlla. </ts>
               <ts e="T322" id="Seg_3671" n="e" s="T321">Harsi͡erde </ts>
               <ts e="T323" id="Seg_3673" n="e" s="T322">turan </ts>
               <ts e="T324" id="Seg_3675" n="e" s="T323">ɨraːktaːgɨtɨgar </ts>
               <ts e="T325" id="Seg_3677" n="e" s="T324">bardaga. </ts>
               <ts e="T326" id="Seg_3679" n="e" s="T325">U͡ol </ts>
               <ts e="T327" id="Seg_3681" n="e" s="T326">ɨraːktaːgɨ </ts>
               <ts e="T328" id="Seg_3683" n="e" s="T327">ɨjɨtar: </ts>
               <ts e="T329" id="Seg_3685" n="e" s="T328">"Kaja </ts>
               <ts e="T330" id="Seg_3687" n="e" s="T329">taːjdɨŋ </ts>
               <ts e="T331" id="Seg_3689" n="e" s="T330">du͡o?" </ts>
               <ts e="T332" id="Seg_3691" n="e" s="T331">"Eː, </ts>
               <ts e="T333" id="Seg_3693" n="e" s="T332">taːjarga </ts>
               <ts e="T334" id="Seg_3695" n="e" s="T333">talɨ </ts>
               <ts e="T335" id="Seg_3697" n="e" s="T334">gɨnnɨm. </ts>
               <ts e="T336" id="Seg_3699" n="e" s="T335">Höbö </ts>
               <ts e="T337" id="Seg_3701" n="e" s="T336">duː, </ts>
               <ts e="T338" id="Seg_3703" n="e" s="T337">hɨːhata </ts>
               <ts e="T339" id="Seg_3705" n="e" s="T338">duː", </ts>
               <ts e="T340" id="Seg_3707" n="e" s="T339">diːr. </ts>
               <ts e="T341" id="Seg_3709" n="e" s="T340">"Kuhagan </ts>
               <ts e="T342" id="Seg_3711" n="e" s="T341">buru͡olaːk </ts>
               <ts e="T343" id="Seg_3713" n="e" s="T342">bu͡or </ts>
               <ts e="T344" id="Seg_3715" n="e" s="T343">golomobuttan </ts>
               <ts e="T345" id="Seg_3717" n="e" s="T344">taksa </ts>
               <ts e="T346" id="Seg_3719" n="e" s="T345">kötön, </ts>
               <ts e="T347" id="Seg_3721" n="e" s="T346">karakpɨn </ts>
               <ts e="T348" id="Seg_3723" n="e" s="T347">kastɨrɨta </ts>
               <ts e="T349" id="Seg_3725" n="e" s="T348">hoton </ts>
               <ts e="T350" id="Seg_3727" n="e" s="T349">oduːlanan </ts>
               <ts e="T351" id="Seg_3729" n="e" s="T350">kördökpüne, </ts>
               <ts e="T352" id="Seg_3731" n="e" s="T351">kallaːn </ts>
               <ts e="T353" id="Seg_3733" n="e" s="T352">di͡ek </ts>
               <ts e="T354" id="Seg_3735" n="e" s="T353">kantajdakpɨna— </ts>
               <ts e="T355" id="Seg_3737" n="e" s="T354">kallaːn </ts>
               <ts e="T356" id="Seg_3739" n="e" s="T355">huluhun </ts>
               <ts e="T357" id="Seg_3741" n="e" s="T356">karagɨm </ts>
               <ts e="T358" id="Seg_3743" n="e" s="T357">aːga </ts>
               <ts e="T359" id="Seg_3745" n="e" s="T358">körör, </ts>
               <ts e="T360" id="Seg_3747" n="e" s="T359">hir </ts>
               <ts e="T361" id="Seg_3749" n="e" s="T360">di͡ek </ts>
               <ts e="T362" id="Seg_3751" n="e" s="T361">kördökpüne— </ts>
               <ts e="T363" id="Seg_3753" n="e" s="T362">hir </ts>
               <ts e="T364" id="Seg_3755" n="e" s="T363">otun-mahɨn </ts>
               <ts e="T365" id="Seg_3757" n="e" s="T364">aːga </ts>
               <ts e="T366" id="Seg_3759" n="e" s="T365">köröbün. </ts>
               <ts e="T367" id="Seg_3761" n="e" s="T366">Karagɨm </ts>
               <ts e="T368" id="Seg_3763" n="e" s="T367">ɨlarɨgar </ts>
               <ts e="T369" id="Seg_3765" n="e" s="T368">kahan </ts>
               <ts e="T370" id="Seg_3767" n="e" s="T369">da </ts>
               <ts e="T371" id="Seg_3769" n="e" s="T370">tijbeppin. </ts>
               <ts e="T372" id="Seg_3771" n="e" s="T371">Onon </ts>
               <ts e="T373" id="Seg_3773" n="e" s="T372">türgenten </ts>
               <ts e="T374" id="Seg_3775" n="e" s="T373">türgen </ts>
               <ts e="T375" id="Seg_3777" n="e" s="T374">kihi </ts>
               <ts e="T376" id="Seg_3779" n="e" s="T375">körüːte </ts>
               <ts e="T377" id="Seg_3781" n="e" s="T376">ebit", </ts>
               <ts e="T378" id="Seg_3783" n="e" s="T377">diːr. </ts>
               <ts e="T379" id="Seg_3785" n="e" s="T378">U͡ol </ts>
               <ts e="T380" id="Seg_3787" n="e" s="T379">ɨraːktaːgɨ </ts>
               <ts e="T381" id="Seg_3789" n="e" s="T380">högör: </ts>
               <ts e="T382" id="Seg_3791" n="e" s="T381">"Oː, </ts>
               <ts e="T383" id="Seg_3793" n="e" s="T382">ogonnʼor, </ts>
               <ts e="T384" id="Seg_3795" n="e" s="T383">dʼe </ts>
               <ts e="T385" id="Seg_3797" n="e" s="T384">öjdöːk </ts>
               <ts e="T386" id="Seg_3799" n="e" s="T385">kihigin </ts>
               <ts e="T387" id="Seg_3801" n="e" s="T386">ebit. </ts>
               <ts e="T388" id="Seg_3803" n="e" s="T387">Dʼe </ts>
               <ts e="T389" id="Seg_3805" n="e" s="T388">tu͡ok </ts>
               <ts e="T390" id="Seg_3807" n="e" s="T389">da </ts>
               <ts e="T391" id="Seg_3809" n="e" s="T390">üs </ts>
               <ts e="T392" id="Seg_3811" n="e" s="T391">tögülleːk", </ts>
               <ts e="T393" id="Seg_3813" n="e" s="T392">taksan </ts>
               <ts e="T394" id="Seg_3815" n="e" s="T393">irgek </ts>
               <ts e="T395" id="Seg_3817" n="e" s="T394">koru͡obanɨ </ts>
               <ts e="T396" id="Seg_3819" n="e" s="T395">tutan </ts>
               <ts e="T397" id="Seg_3821" n="e" s="T396">bi͡erer. </ts>
               <ts e="T398" id="Seg_3823" n="e" s="T397">"Manɨ </ts>
               <ts e="T399" id="Seg_3825" n="e" s="T398">harsi͡erda </ts>
               <ts e="T400" id="Seg_3827" n="e" s="T399">törötön </ts>
               <ts e="T401" id="Seg_3829" n="e" s="T400">tugutun </ts>
               <ts e="T402" id="Seg_3831" n="e" s="T401">batɨhɨnnaran </ts>
               <ts e="T403" id="Seg_3833" n="e" s="T402">egeleːr. </ts>
               <ts e="T404" id="Seg_3835" n="e" s="T403">Kaja </ts>
               <ts e="T405" id="Seg_3837" n="e" s="T404">u͡oruktatɨn </ts>
               <ts e="T406" id="Seg_3839" n="e" s="T405">komujan </ts>
               <ts e="T407" id="Seg_3841" n="e" s="T406">egeleːjegin", </ts>
               <ts e="T408" id="Seg_3843" n="e" s="T407">di͡ebit </ts>
               <ts e="T409" id="Seg_3845" n="e" s="T408">U͡ol </ts>
               <ts e="T410" id="Seg_3847" n="e" s="T409">ɨraːktaːgɨ. </ts>
               <ts e="T411" id="Seg_3849" n="e" s="T410">Ogonnʼor </ts>
               <ts e="T412" id="Seg_3851" n="e" s="T411">erejdeːk </ts>
               <ts e="T413" id="Seg_3853" n="e" s="T412">irgek </ts>
               <ts e="T414" id="Seg_3855" n="e" s="T413">koru͡obatɨn </ts>
               <ts e="T415" id="Seg_3857" n="e" s="T414">hi͡eten </ts>
               <ts e="T416" id="Seg_3859" n="e" s="T415">tijen </ts>
               <ts e="T417" id="Seg_3861" n="e" s="T416">ɨtɨː </ts>
               <ts e="T418" id="Seg_3863" n="e" s="T417">olordo. </ts>
               <ts e="T419" id="Seg_3865" n="e" s="T418">Kɨːha </ts>
               <ts e="T420" id="Seg_3867" n="e" s="T419">ɨjɨtta: </ts>
               <ts e="T421" id="Seg_3869" n="e" s="T420">"Kaja, </ts>
               <ts e="T422" id="Seg_3871" n="e" s="T421">agaː, </ts>
               <ts e="T423" id="Seg_3873" n="e" s="T422">togo </ts>
               <ts e="T424" id="Seg_3875" n="e" s="T423">ɨtaːtɨŋ?" </ts>
               <ts e="T425" id="Seg_3877" n="e" s="T424">"Kaja, </ts>
               <ts e="T426" id="Seg_3879" n="e" s="T425">irgek </ts>
               <ts e="T427" id="Seg_3881" n="e" s="T426">koru͡obanɨ </ts>
               <ts e="T428" id="Seg_3883" n="e" s="T427">bi͡erde, </ts>
               <ts e="T429" id="Seg_3885" n="e" s="T428">törötön </ts>
               <ts e="T430" id="Seg_3887" n="e" s="T429">egel </ts>
               <ts e="T431" id="Seg_3889" n="e" s="T430">diːr, </ts>
               <ts e="T432" id="Seg_3891" n="e" s="T431">irgek </ts>
               <ts e="T433" id="Seg_3893" n="e" s="T432">koru͡oba </ts>
               <ts e="T434" id="Seg_3895" n="e" s="T433">kantɨnan </ts>
               <ts e="T435" id="Seg_3897" n="e" s="T434">töröːččünüj, </ts>
               <ts e="T436" id="Seg_3899" n="e" s="T435">kuːgu?" </ts>
               <ts e="T437" id="Seg_3901" n="e" s="T436">"Eː, </ts>
               <ts e="T438" id="Seg_3903" n="e" s="T437">irgek </ts>
               <ts e="T439" id="Seg_3905" n="e" s="T438">koru͡oba </ts>
               <ts e="T440" id="Seg_3907" n="e" s="T439">kantan </ts>
               <ts e="T441" id="Seg_3909" n="e" s="T440">törü͡öj? </ts>
               <ts e="T442" id="Seg_3911" n="e" s="T441">Kata </ts>
               <ts e="T443" id="Seg_3913" n="e" s="T442">astammɨppɨt. </ts>
               <ts e="T444" id="Seg_3915" n="e" s="T443">Taksaŋŋɨn </ts>
               <ts e="T445" id="Seg_3917" n="e" s="T444">ölörböktöː, </ts>
               <ts e="T446" id="Seg_3919" n="e" s="T445">etin </ts>
               <ts e="T447" id="Seg_3921" n="e" s="T446">hi͡ekpit", </ts>
               <ts e="T448" id="Seg_3923" n="e" s="T447">diːr </ts>
               <ts e="T449" id="Seg_3925" n="e" s="T448">kɨːs. </ts>
               <ts e="T450" id="Seg_3927" n="e" s="T449">Ogonnʼor </ts>
               <ts e="T451" id="Seg_3929" n="e" s="T450">irgek </ts>
               <ts e="T452" id="Seg_3931" n="e" s="T451">koru͡obatɨn </ts>
               <ts e="T453" id="Seg_3933" n="e" s="T452">ölörön </ts>
               <ts e="T454" id="Seg_3935" n="e" s="T453">keːher, </ts>
               <ts e="T455" id="Seg_3937" n="e" s="T454">dʼe </ts>
               <ts e="T456" id="Seg_3939" n="e" s="T455">manɨ </ts>
               <ts e="T457" id="Seg_3941" n="e" s="T456">hi͡ektere. </ts>
               <ts e="T458" id="Seg_3943" n="e" s="T457">Kɨːha </ts>
               <ts e="T459" id="Seg_3945" n="e" s="T458">utakaːga </ts>
               <ts e="T460" id="Seg_3947" n="e" s="T459">koru͡oba </ts>
               <ts e="T461" id="Seg_3949" n="e" s="T460">tunnʼagɨn, </ts>
               <ts e="T462" id="Seg_3951" n="e" s="T461">tugulun </ts>
               <ts e="T463" id="Seg_3953" n="e" s="T462">komujan </ts>
               <ts e="T464" id="Seg_3955" n="e" s="T463">bi͡erde </ts>
               <ts e="T465" id="Seg_3957" n="e" s="T464">agatɨgar: </ts>
               <ts e="T466" id="Seg_3959" n="e" s="T465">""Dʼe </ts>
               <ts e="T467" id="Seg_3961" n="e" s="T466">iti </ts>
               <ts e="T468" id="Seg_3963" n="e" s="T467">kara </ts>
               <ts e="T469" id="Seg_3965" n="e" s="T468">tunʼaga— </ts>
               <ts e="T470" id="Seg_3967" n="e" s="T469">inʼete, </ts>
               <ts e="T471" id="Seg_3969" n="e" s="T470">tugula— </ts>
               <ts e="T472" id="Seg_3971" n="e" s="T471">tubuta" </ts>
               <ts e="T473" id="Seg_3973" n="e" s="T472">di͡en </ts>
               <ts e="T474" id="Seg_3975" n="e" s="T473">baraːn </ts>
               <ts e="T475" id="Seg_3977" n="e" s="T474">ɨraːktaːgɨn </ts>
               <ts e="T476" id="Seg_3979" n="e" s="T475">ostu͡olugar </ts>
               <ts e="T477" id="Seg_3981" n="e" s="T476">ogo </ts>
               <ts e="T478" id="Seg_3983" n="e" s="T477">oːnnʼuːrun </ts>
               <ts e="T479" id="Seg_3985" n="e" s="T478">kurduk </ts>
               <ts e="T480" id="Seg_3987" n="e" s="T479">huburutalaːn </ts>
               <ts e="T481" id="Seg_3989" n="e" s="T480">keːheːr. </ts>
               <ts e="T482" id="Seg_3991" n="e" s="T481">"Irgek </ts>
               <ts e="T483" id="Seg_3993" n="e" s="T482">koru͡obattan </ts>
               <ts e="T484" id="Seg_3995" n="e" s="T483">törüːr </ts>
               <ts e="T485" id="Seg_3997" n="e" s="T484">agaj </ts>
               <ts e="T486" id="Seg_3999" n="e" s="T485">hirin </ts>
               <ts e="T487" id="Seg_4001" n="e" s="T486">bu </ts>
               <ts e="T488" id="Seg_4003" n="e" s="T487">bullum", </ts>
               <ts e="T489" id="Seg_4005" n="e" s="T488">di͡er. </ts>
               <ts e="T490" id="Seg_4007" n="e" s="T489">Onton </ts>
               <ts e="T491" id="Seg_4009" n="e" s="T490">ordugu </ts>
               <ts e="T492" id="Seg_4011" n="e" s="T491">da </ts>
               <ts e="T493" id="Seg_4013" n="e" s="T492">kini </ts>
               <ts e="T494" id="Seg_4015" n="e" s="T493">bilere </ts>
               <ts e="T495" id="Seg_4017" n="e" s="T494">bu͡olu͡o </ts>
               <ts e="T496" id="Seg_4019" n="e" s="T495">du͡o? </ts>
               <ts e="T497" id="Seg_4021" n="e" s="T496">Dʼe </ts>
               <ts e="T498" id="Seg_4023" n="e" s="T497">oččogo </ts>
               <ts e="T499" id="Seg_4025" n="e" s="T498">mohujan </ts>
               <ts e="T500" id="Seg_4027" n="e" s="T499">ɨjɨtɨ͡a: </ts>
               <ts e="T501" id="Seg_4029" n="e" s="T500">"Bejeŋ </ts>
               <ts e="T502" id="Seg_4031" n="e" s="T501">öjgünen </ts>
               <ts e="T503" id="Seg_4033" n="e" s="T502">gɨmmatɨŋ </ts>
               <ts e="T504" id="Seg_4035" n="e" s="T503">bu͡olu͡o. </ts>
               <ts e="T505" id="Seg_4037" n="e" s="T504">Itičče </ts>
               <ts e="T506" id="Seg_4039" n="e" s="T505">öjdöːgüŋ </ts>
               <ts e="T507" id="Seg_4041" n="e" s="T506">bu͡ollar </ts>
               <ts e="T508" id="Seg_4043" n="e" s="T507">min </ts>
               <ts e="T509" id="Seg_4045" n="e" s="T508">iːkpin-haːkpɨn </ts>
               <ts e="T510" id="Seg_4047" n="e" s="T509">ɨraːstɨː </ts>
               <ts e="T511" id="Seg_4049" n="e" s="T510">iŋen-batan </ts>
               <ts e="T512" id="Seg_4051" n="e" s="T511">hɨtɨ͡a </ts>
               <ts e="T513" id="Seg_4053" n="e" s="T512">hu͡ok </ts>
               <ts e="T514" id="Seg_4055" n="e" s="T513">etiŋ. </ts>
               <ts e="T515" id="Seg_4057" n="e" s="T514">Kim </ts>
               <ts e="T516" id="Seg_4059" n="e" s="T515">ü͡öreter", </ts>
               <ts e="T517" id="Seg_4061" n="e" s="T516">di͡ege. </ts>
               <ts e="T518" id="Seg_4063" n="e" s="T517">Onno </ts>
               <ts e="T519" id="Seg_4065" n="e" s="T518">kisteːjegin", </ts>
               <ts e="T520" id="Seg_4067" n="e" s="T519">diːr </ts>
               <ts e="T521" id="Seg_4069" n="e" s="T520">kɨːha. </ts>
               <ts e="T522" id="Seg_4071" n="e" s="T521">""Kuhagan </ts>
               <ts e="T523" id="Seg_4073" n="e" s="T522">kɨːs </ts>
               <ts e="T524" id="Seg_4075" n="e" s="T523">ogoloːkpun, </ts>
               <ts e="T525" id="Seg_4077" n="e" s="T524">ol </ts>
               <ts e="T526" id="Seg_4079" n="e" s="T525">ü͡öreteːktiːr", </ts>
               <ts e="T527" id="Seg_4081" n="e" s="T526">di͡er." </ts>
               <ts e="T528" id="Seg_4083" n="e" s="T527">Ogonnʼor </ts>
               <ts e="T529" id="Seg_4085" n="e" s="T528">harsi͡erda </ts>
               <ts e="T530" id="Seg_4087" n="e" s="T529">barar. </ts>
               <ts e="T531" id="Seg_4089" n="e" s="T530">U͡ol </ts>
               <ts e="T532" id="Seg_4091" n="e" s="T531">ɨraːktaːgɨ </ts>
               <ts e="T533" id="Seg_4093" n="e" s="T532">ɨjɨtar: </ts>
               <ts e="T534" id="Seg_4095" n="e" s="T533">"Kaja </ts>
               <ts e="T535" id="Seg_4097" n="e" s="T534">taːjdɨŋ </ts>
               <ts e="T536" id="Seg_4099" n="e" s="T535">du͡o?" </ts>
               <ts e="T537" id="Seg_4101" n="e" s="T536">Ogonnʼor </ts>
               <ts e="T538" id="Seg_4103" n="e" s="T537">koru͡oba </ts>
               <ts e="T539" id="Seg_4105" n="e" s="T538">tunʼagɨn, </ts>
               <ts e="T540" id="Seg_4107" n="e" s="T539">tugulun </ts>
               <ts e="T541" id="Seg_4109" n="e" s="T540">ostu͡olga </ts>
               <ts e="T542" id="Seg_4111" n="e" s="T541">ogo </ts>
               <ts e="T543" id="Seg_4113" n="e" s="T542">oːnnʼuːrun </ts>
               <ts e="T544" id="Seg_4115" n="e" s="T543">kurduk </ts>
               <ts e="T545" id="Seg_4117" n="e" s="T544">huburutalaːn </ts>
               <ts e="T546" id="Seg_4119" n="e" s="T545">keːher. </ts>
               <ts e="T547" id="Seg_4121" n="e" s="T546">"Irgek </ts>
               <ts e="T548" id="Seg_4123" n="e" s="T547">koru͡obattan </ts>
               <ts e="T549" id="Seg_4125" n="e" s="T548">törüːr </ts>
               <ts e="T550" id="Seg_4127" n="e" s="T549">agaj </ts>
               <ts e="T551" id="Seg_4129" n="e" s="T550">hirin </ts>
               <ts e="T552" id="Seg_4131" n="e" s="T551">bu </ts>
               <ts e="T553" id="Seg_4133" n="e" s="T552">bullum", </ts>
               <ts e="T554" id="Seg_4135" n="e" s="T553">diːr. </ts>
               <ts e="T555" id="Seg_4137" n="e" s="T554">ɨraːktaːgɨ: </ts>
               <ts e="T556" id="Seg_4139" n="e" s="T555">"Bejeŋ </ts>
               <ts e="T557" id="Seg_4141" n="e" s="T556">öjgünen </ts>
               <ts e="T558" id="Seg_4143" n="e" s="T557">gɨmmatɨŋ </ts>
               <ts e="T559" id="Seg_4145" n="e" s="T558">bu͡olu͡o. </ts>
               <ts e="T560" id="Seg_4147" n="e" s="T559">Itičče </ts>
               <ts e="T561" id="Seg_4149" n="e" s="T560">öjdöːguŋ </ts>
               <ts e="T562" id="Seg_4151" n="e" s="T561">bu͡ollar </ts>
               <ts e="T563" id="Seg_4153" n="e" s="T562">min </ts>
               <ts e="T564" id="Seg_4155" n="e" s="T563">iːkpin-haːkpɨn </ts>
               <ts e="T565" id="Seg_4157" n="e" s="T564">ɨraːstɨː </ts>
               <ts e="T566" id="Seg_4159" n="e" s="T565">iŋen-batan </ts>
               <ts e="T567" id="Seg_4161" n="e" s="T566">hɨtɨ͡a </ts>
               <ts e="T568" id="Seg_4163" n="e" s="T567">hu͡ok </ts>
               <ts e="T569" id="Seg_4165" n="e" s="T568">etiŋ. </ts>
               <ts e="T570" id="Seg_4167" n="e" s="T569">Kim </ts>
               <ts e="T571" id="Seg_4169" n="e" s="T570">ü͡öreter", </ts>
               <ts e="T572" id="Seg_4171" n="e" s="T571">diːr. </ts>
               <ts e="T573" id="Seg_4173" n="e" s="T572">"Kuhagan </ts>
               <ts e="T574" id="Seg_4175" n="e" s="T573">kɨːs </ts>
               <ts e="T575" id="Seg_4177" n="e" s="T574">ogoloːkpun, </ts>
               <ts e="T576" id="Seg_4179" n="e" s="T575">ol </ts>
               <ts e="T577" id="Seg_4181" n="e" s="T576">ü͡öreteːktiːr", </ts>
               <ts e="T578" id="Seg_4183" n="e" s="T577">diːr </ts>
               <ts e="T579" id="Seg_4185" n="e" s="T578">ogonnʼor. </ts>
               <ts e="T580" id="Seg_4187" n="e" s="T579">U͡ol </ts>
               <ts e="T581" id="Seg_4189" n="e" s="T580">ɨraːktaːgɨ: </ts>
               <ts e="T582" id="Seg_4191" n="e" s="T581">"Kɨːhɨŋ </ts>
               <ts e="T583" id="Seg_4193" n="e" s="T582">öjdöːk </ts>
               <ts e="T584" id="Seg_4195" n="e" s="T583">kɨːs </ts>
               <ts e="T585" id="Seg_4197" n="e" s="T584">töröːbüt", </ts>
               <ts e="T586" id="Seg_4199" n="e" s="T585">diːr. </ts>
               <ts e="T587" id="Seg_4201" n="e" s="T586">Timir </ts>
               <ts e="T588" id="Seg_4203" n="e" s="T587">kokoroːgu, </ts>
               <ts e="T589" id="Seg_4205" n="e" s="T588">tügege </ts>
               <ts e="T590" id="Seg_4207" n="e" s="T589">kajagastaːgɨ </ts>
               <ts e="T591" id="Seg_4209" n="e" s="T590">bi͡erer. </ts>
               <ts e="T592" id="Seg_4211" n="e" s="T591">"Manɨ </ts>
               <ts e="T593" id="Seg_4213" n="e" s="T592">hamaːn </ts>
               <ts e="T594" id="Seg_4215" n="e" s="T593">ɨːttɨn", </ts>
               <ts e="T595" id="Seg_4217" n="e" s="T594">diːr. </ts>
               <ts e="T596" id="Seg_4219" n="e" s="T595">"Onu </ts>
               <ts e="T597" id="Seg_4221" n="e" s="T596">hamaːbatagɨna, </ts>
               <ts e="T598" id="Seg_4223" n="e" s="T597">ikki͡enŋitin </ts>
               <ts e="T599" id="Seg_4225" n="e" s="T598">töbögütün </ts>
               <ts e="T600" id="Seg_4227" n="e" s="T599">bɨhɨ͡am", </ts>
               <ts e="T601" id="Seg_4229" n="e" s="T600">diːr. </ts>
               <ts e="T602" id="Seg_4231" n="e" s="T601">Ogonnʼor </ts>
               <ts e="T603" id="Seg_4233" n="e" s="T602">kokoroːgu </ts>
               <ts e="T604" id="Seg_4235" n="e" s="T603">ildʼe </ts>
               <ts e="T605" id="Seg_4237" n="e" s="T604">barbɨta </ts>
               <ts e="T606" id="Seg_4239" n="e" s="T605">ɨtɨː-ɨtɨː. </ts>
               <ts e="T607" id="Seg_4241" n="e" s="T606">Kɨːs: </ts>
               <ts e="T608" id="Seg_4243" n="e" s="T607">"Kaja, </ts>
               <ts e="T609" id="Seg_4245" n="e" s="T608">agaː, </ts>
               <ts e="T610" id="Seg_4247" n="e" s="T609">tu͡oguŋ </ts>
               <ts e="T611" id="Seg_4249" n="e" s="T610">kokoroːgoj", </ts>
               <ts e="T612" id="Seg_4251" n="e" s="T611">diːr. </ts>
               <ts e="T613" id="Seg_4253" n="e" s="T612">"Kaja-kuː, </ts>
               <ts e="T614" id="Seg_4255" n="e" s="T613">dʼe </ts>
               <ts e="T615" id="Seg_4257" n="e" s="T614">ölörör </ts>
               <ts e="T616" id="Seg_4259" n="e" s="T615">küne </ts>
               <ts e="T617" id="Seg_4261" n="e" s="T616">kelle. </ts>
               <ts e="T618" id="Seg_4263" n="e" s="T617">"Bu </ts>
               <ts e="T619" id="Seg_4265" n="e" s="T618">kokoroːgu </ts>
               <ts e="T620" id="Seg_4267" n="e" s="T619">hamaːn </ts>
               <ts e="T621" id="Seg_4269" n="e" s="T620">ɨːttɨn", </ts>
               <ts e="T622" id="Seg_4271" n="e" s="T621">diːr. </ts>
               <ts e="T623" id="Seg_4273" n="e" s="T622">"Bunu </ts>
               <ts e="T624" id="Seg_4275" n="e" s="T623">hamaːbatagɨna, </ts>
               <ts e="T625" id="Seg_4277" n="e" s="T624">ikki͡enŋitin </ts>
               <ts e="T626" id="Seg_4279" n="e" s="T625">töbögütün </ts>
               <ts e="T627" id="Seg_4281" n="e" s="T626">bɨhɨ͡am", </ts>
               <ts e="T628" id="Seg_4283" n="e" s="T627">diːr." </ts>
               <ts e="T629" id="Seg_4285" n="e" s="T628">Kɨːs </ts>
               <ts e="T630" id="Seg_4287" n="e" s="T629">diːr: </ts>
               <ts e="T631" id="Seg_4289" n="e" s="T630">"Ilt </ts>
               <ts e="T632" id="Seg_4291" n="e" s="T631">tötterü, </ts>
               <ts e="T633" id="Seg_4293" n="e" s="T632">dʼaktar </ts>
               <ts e="T634" id="Seg_4295" n="e" s="T633">kihi </ts>
               <ts e="T635" id="Seg_4297" n="e" s="T634">hamɨ͡am, </ts>
               <ts e="T636" id="Seg_4299" n="e" s="T635">er </ts>
               <ts e="T637" id="Seg_4301" n="e" s="T636">kihi </ts>
               <ts e="T638" id="Seg_4303" n="e" s="T637">kokoroːgu </ts>
               <ts e="T639" id="Seg_4305" n="e" s="T638">čorčokoːt </ts>
               <ts e="T640" id="Seg_4307" n="e" s="T639">kördük </ts>
               <ts e="T641" id="Seg_4309" n="e" s="T640">ti͡eren </ts>
               <ts e="T642" id="Seg_4311" n="e" s="T641">ɨːttɨn." </ts>
               <ts e="T643" id="Seg_4313" n="e" s="T642">Ogonnʼor </ts>
               <ts e="T644" id="Seg_4315" n="e" s="T643">ildʼen </ts>
               <ts e="T645" id="Seg_4317" n="e" s="T644">bi͡erer. </ts>
               <ts e="T646" id="Seg_4319" n="e" s="T645">Uːol </ts>
               <ts e="T647" id="Seg_4321" n="e" s="T646">ɨraːktaːgɨ </ts>
               <ts e="T648" id="Seg_4323" n="e" s="T647">högör. </ts>
               <ts e="T649" id="Seg_4325" n="e" s="T648">Ogonnʼoru </ts>
               <ts e="T650" id="Seg_4327" n="e" s="T649">ɨːtar. </ts>
               <ts e="T651" id="Seg_4329" n="e" s="T650">"Bejem </ts>
               <ts e="T652" id="Seg_4331" n="e" s="T651">kɨːskɨn </ts>
               <ts e="T653" id="Seg_4333" n="e" s="T652">körö </ts>
               <ts e="T654" id="Seg_4335" n="e" s="T653">barɨ͡am", </ts>
               <ts e="T655" id="Seg_4337" n="e" s="T654">diːr. </ts>
               <ts e="T656" id="Seg_4339" n="e" s="T655">U͡ol </ts>
               <ts e="T657" id="Seg_4341" n="e" s="T656">ɨraːktaːgɨ, </ts>
               <ts e="T658" id="Seg_4343" n="e" s="T657">kɨːhɨ </ts>
               <ts e="T659" id="Seg_4345" n="e" s="T658">köröːt, </ts>
               <ts e="T660" id="Seg_4347" n="e" s="T659">bagarar, </ts>
               <ts e="T661" id="Seg_4349" n="e" s="T660">dʼaktar </ts>
               <ts e="T662" id="Seg_4351" n="e" s="T661">ɨlar. </ts>
               <ts e="T663" id="Seg_4353" n="e" s="T662">Töhönü-kaččanɨ </ts>
               <ts e="T664" id="Seg_4355" n="e" s="T663">olorbuttara </ts>
               <ts e="T665" id="Seg_4357" n="e" s="T664">bu͡olla. </ts>
               <ts e="T666" id="Seg_4359" n="e" s="T665">Dʼaktardammɨtɨttan </ts>
               <ts e="T667" id="Seg_4361" n="e" s="T666">U͡ol </ts>
               <ts e="T668" id="Seg_4363" n="e" s="T667">ɨraːktaːgɨ </ts>
               <ts e="T669" id="Seg_4365" n="e" s="T668">kuːkula </ts>
               <ts e="T670" id="Seg_4367" n="e" s="T669">kurduk </ts>
               <ts e="T671" id="Seg_4369" n="e" s="T670">olorčok </ts>
               <ts e="T672" id="Seg_4371" n="e" s="T671">bu͡olla, </ts>
               <ts e="T673" id="Seg_4373" n="e" s="T672">barɨtɨn </ts>
               <ts e="T674" id="Seg_4375" n="e" s="T673">dʼaktara </ts>
               <ts e="T675" id="Seg_4377" n="e" s="T674">hubeliːr, </ts>
               <ts e="T676" id="Seg_4379" n="e" s="T675">bɨhaːrar. </ts>
               <ts e="T677" id="Seg_4381" n="e" s="T676">U͡ol </ts>
               <ts e="T678" id="Seg_4383" n="e" s="T677">ɨraːktaːgɨ </ts>
               <ts e="T679" id="Seg_4385" n="e" s="T678">tɨllanar: </ts>
               <ts e="T680" id="Seg_4387" n="e" s="T679">"Dogoː, </ts>
               <ts e="T681" id="Seg_4389" n="e" s="T680">bu </ts>
               <ts e="T682" id="Seg_4391" n="e" s="T681">biːr </ts>
               <ts e="T683" id="Seg_4393" n="e" s="T682">dojduga </ts>
               <ts e="T684" id="Seg_4395" n="e" s="T683">ikki </ts>
               <ts e="T685" id="Seg_4397" n="e" s="T684">ɨraːktaːgɨ </ts>
               <ts e="T686" id="Seg_4399" n="e" s="T685">bu͡olarbɨt </ts>
               <ts e="T687" id="Seg_4401" n="e" s="T686">tabɨllɨbat. </ts>
               <ts e="T688" id="Seg_4403" n="e" s="T687">Bihigi </ts>
               <ts e="T689" id="Seg_4405" n="e" s="T688">araksɨ͡ak." </ts>
               <ts e="T690" id="Seg_4407" n="e" s="T689">Dʼaktara: </ts>
               <ts e="T691" id="Seg_4409" n="e" s="T690">"Dʼe </ts>
               <ts e="T692" id="Seg_4411" n="e" s="T691">araksɨ͡ak </ts>
               <ts e="T693" id="Seg_4413" n="e" s="T692">duː", </ts>
               <ts e="T694" id="Seg_4415" n="e" s="T693">diːr. </ts>
               <ts e="T695" id="Seg_4417" n="e" s="T694">U͡ol </ts>
               <ts e="T696" id="Seg_4419" n="e" s="T695">ɨraːktaːgɨ: </ts>
               <ts e="T697" id="Seg_4421" n="e" s="T696">"Tu͡ok </ts>
               <ts e="T698" id="Seg_4423" n="e" s="T697">bagalaːk </ts>
               <ts e="T699" id="Seg_4425" n="e" s="T698">hirgin </ts>
               <ts e="T700" id="Seg_4427" n="e" s="T699">barɨtɨn </ts>
               <ts e="T701" id="Seg_4429" n="e" s="T700">ɨl", </ts>
               <ts e="T702" id="Seg_4431" n="e" s="T701">diːr. </ts>
               <ts e="T703" id="Seg_4433" n="e" s="T702">Dʼaktar </ts>
               <ts e="T704" id="Seg_4435" n="e" s="T703">höbüleher. </ts>
               <ts e="T705" id="Seg_4437" n="e" s="T704">Araksar </ts>
               <ts e="T706" id="Seg_4439" n="e" s="T705">tüːnnere-keː </ts>
               <ts e="T707" id="Seg_4441" n="e" s="T706">bu͡olar. </ts>
               <ts e="T708" id="Seg_4443" n="e" s="T707">Ere </ts>
               <ts e="T709" id="Seg_4445" n="e" s="T708">utujan </ts>
               <ts e="T710" id="Seg_4447" n="e" s="T709">kaːlar. </ts>
               <ts e="T711" id="Seg_4449" n="e" s="T710">Dʼaktar </ts>
               <ts e="T712" id="Seg_4451" n="e" s="T711">erin </ts>
               <ts e="T713" id="Seg_4453" n="e" s="T712">hu͡organnarɨ, </ts>
               <ts e="T714" id="Seg_4455" n="e" s="T713">tellekteri </ts>
               <ts e="T715" id="Seg_4457" n="e" s="T714">agatɨn </ts>
               <ts e="T716" id="Seg_4459" n="e" s="T715">bu͡or </ts>
               <ts e="T717" id="Seg_4461" n="e" s="T716">golomo </ts>
               <ts e="T718" id="Seg_4463" n="e" s="T717">dʼi͡etiger </ts>
               <ts e="T719" id="Seg_4465" n="e" s="T718">kötögön </ts>
               <ts e="T720" id="Seg_4467" n="e" s="T719">ildʼer. </ts>
               <ts e="T721" id="Seg_4469" n="e" s="T720">U͡ol </ts>
               <ts e="T722" id="Seg_4471" n="e" s="T721">ɨraːktaːgɨ </ts>
               <ts e="T723" id="Seg_4473" n="e" s="T722">harsi͡erda </ts>
               <ts e="T724" id="Seg_4475" n="e" s="T723">uhuktan </ts>
               <ts e="T725" id="Seg_4477" n="e" s="T724">hohujar, </ts>
               <ts e="T726" id="Seg_4479" n="e" s="T725">tɨːllan </ts>
               <ts e="T727" id="Seg_4481" n="e" s="T726">körbüte </ts>
               <ts e="T728" id="Seg_4483" n="e" s="T727">baha-ataga </ts>
               <ts e="T729" id="Seg_4485" n="e" s="T728">golomo </ts>
               <ts e="T730" id="Seg_4487" n="e" s="T729">mahɨgar </ts>
               <ts e="T731" id="Seg_4489" n="e" s="T730">kebiller. </ts>
               <ts e="T732" id="Seg_4491" n="e" s="T731">"Kaja-keː, </ts>
               <ts e="T733" id="Seg_4493" n="e" s="T732">bu </ts>
               <ts e="T734" id="Seg_4495" n="e" s="T733">kanna </ts>
               <ts e="T735" id="Seg_4497" n="e" s="T734">baːrbɨtɨj? </ts>
               <ts e="T736" id="Seg_4499" n="e" s="T735">Togo </ts>
               <ts e="T737" id="Seg_4501" n="e" s="T736">kelbitimij?" </ts>
               <ts e="T738" id="Seg_4503" n="e" s="T737">Dʼaktarɨn </ts>
               <ts e="T739" id="Seg_4505" n="e" s="T738">kebi͡eleːn </ts>
               <ts e="T740" id="Seg_4507" n="e" s="T739">uhugunnarar. </ts>
               <ts e="T741" id="Seg_4509" n="e" s="T740">Dʼaktara: </ts>
               <ts e="T742" id="Seg_4511" n="e" s="T741">"Kaja </ts>
               <ts e="T743" id="Seg_4513" n="e" s="T742">dogoː, </ts>
               <ts e="T744" id="Seg_4515" n="e" s="T743">min </ts>
               <ts e="T745" id="Seg_4517" n="e" s="T744">egelbitim", </ts>
               <ts e="T746" id="Seg_4519" n="e" s="T745">diːr. </ts>
               <ts e="T747" id="Seg_4521" n="e" s="T746">"Togo </ts>
               <ts e="T748" id="Seg_4523" n="e" s="T747">egelbikkinij?" </ts>
               <ts e="T749" id="Seg_4525" n="e" s="T748">"Tɨːj, </ts>
               <ts e="T750" id="Seg_4527" n="e" s="T749">bejegin </ts>
               <ts e="T751" id="Seg_4529" n="e" s="T750">bagardɨm </ts>
               <ts e="T752" id="Seg_4531" n="e" s="T751">da </ts>
               <ts e="T753" id="Seg_4533" n="e" s="T752">bejegin </ts>
               <ts e="T754" id="Seg_4535" n="e" s="T753">egellim. </ts>
               <ts e="T755" id="Seg_4537" n="e" s="T754">Tu͡ogu </ts>
               <ts e="T756" id="Seg_4539" n="e" s="T755">bagarargɨn </ts>
               <ts e="T757" id="Seg_4541" n="e" s="T756">ɨlaːr </ts>
               <ts e="T758" id="Seg_4543" n="e" s="T757">di͡ebitiŋ </ts>
               <ts e="T759" id="Seg_4545" n="e" s="T758">diː", </ts>
               <ts e="T760" id="Seg_4547" n="e" s="T759">diːr </ts>
               <ts e="T761" id="Seg_4549" n="e" s="T760">dʼaktar. </ts>
               <ts e="T762" id="Seg_4551" n="e" s="T761">U͡ol </ts>
               <ts e="T763" id="Seg_4553" n="e" s="T762">ɨraːktaːgɨ </ts>
               <ts e="T764" id="Seg_4555" n="e" s="T763">kɨ͡ajan </ts>
               <ts e="T765" id="Seg_4557" n="e" s="T764">arakpata. </ts>
               <ts e="T766" id="Seg_4559" n="e" s="T765">Dʼe </ts>
               <ts e="T767" id="Seg_4561" n="e" s="T766">dʼaktara </ts>
               <ts e="T768" id="Seg_4563" n="e" s="T767">diːr: </ts>
               <ts e="T769" id="Seg_4565" n="e" s="T768">"Oččo </ts>
               <ts e="T770" id="Seg_4567" n="e" s="T769">öjdöːk </ts>
               <ts e="T771" id="Seg_4569" n="e" s="T770">kihi </ts>
               <ts e="T772" id="Seg_4571" n="e" s="T771">bu͡olbatak </ts>
               <ts e="T773" id="Seg_4573" n="e" s="T772">ebikkin. </ts>
               <ts e="T774" id="Seg_4575" n="e" s="T773">Dʼoŋŋun </ts>
               <ts e="T775" id="Seg_4577" n="e" s="T774">hübeleːn </ts>
               <ts e="T776" id="Seg_4579" n="e" s="T775">bejeŋ </ts>
               <ts e="T777" id="Seg_4581" n="e" s="T776">olor. </ts>
               <ts e="T778" id="Seg_4583" n="e" s="T777">Min </ts>
               <ts e="T779" id="Seg_4585" n="e" s="T778">könnörü </ts>
               <ts e="T780" id="Seg_4587" n="e" s="T779">küseːjke </ts>
               <ts e="T781" id="Seg_4589" n="e" s="T780">bu͡olan </ts>
               <ts e="T782" id="Seg_4591" n="e" s="T781">oloru͡om." </ts>
               <ts e="T783" id="Seg_4593" n="e" s="T782">Umnahɨt </ts>
               <ts e="T784" id="Seg_4595" n="e" s="T783">paːsɨnaj </ts>
               <ts e="T785" id="Seg_4597" n="e" s="T784">kɨːhɨn </ts>
               <ts e="T786" id="Seg_4599" n="e" s="T785">ütü͡ötünen </ts>
               <ts e="T787" id="Seg_4601" n="e" s="T786">hi͡erkile </ts>
               <ts e="T788" id="Seg_4603" n="e" s="T787">taːs </ts>
               <ts e="T789" id="Seg_4605" n="e" s="T788">dʼi͡ege </ts>
               <ts e="T790" id="Seg_4607" n="e" s="T789">olordogo, </ts>
               <ts e="T791" id="Seg_4609" n="e" s="T790">bu͡or </ts>
               <ts e="T792" id="Seg_4611" n="e" s="T791">golomotton </ts>
               <ts e="T793" id="Seg_4613" n="e" s="T792">araksan </ts>
               <ts e="T794" id="Seg_4615" n="e" s="T793">uhuguttan. </ts>
               <ts e="T795" id="Seg_4617" n="e" s="T794">Bajan-tajan </ts>
               <ts e="T796" id="Seg_4619" n="e" s="T795">olordoktoro </ts>
               <ts e="T797" id="Seg_4621" n="e" s="T796">dʼe. </ts>
               <ts e="T798" id="Seg_4623" n="e" s="T797">Elete. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_4624" s="T0">PoMA_1964_YoungCzar_flk.001 (001.001)</ta>
            <ta e="T7" id="Seg_4625" s="T4">PoMA_1964_YoungCzar_flk.002 (001.002)</ta>
            <ta e="T23" id="Seg_4626" s="T7">PoMA_1964_YoungCzar_flk.003 (001.003)</ta>
            <ta e="T26" id="Seg_4627" s="T23">PoMA_1964_YoungCzar_flk.004 (001.004)</ta>
            <ta e="T31" id="Seg_4628" s="T26">PoMA_1964_YoungCzar_flk.005 (001.005)</ta>
            <ta e="T36" id="Seg_4629" s="T31">PoMA_1964_YoungCzar_flk.006 (001.006)</ta>
            <ta e="T45" id="Seg_4630" s="T36">PoMA_1964_YoungCzar_flk.007 (001.007)</ta>
            <ta e="T50" id="Seg_4631" s="T45">PoMA_1964_YoungCzar_flk.008 (001.008)</ta>
            <ta e="T55" id="Seg_4632" s="T50">PoMA_1964_YoungCzar_flk.009 (001.009)</ta>
            <ta e="T62" id="Seg_4633" s="T55">PoMA_1964_YoungCzar_flk.010 (001.010)</ta>
            <ta e="T77" id="Seg_4634" s="T62">PoMA_1964_YoungCzar_flk.011 (001.011)</ta>
            <ta e="T81" id="Seg_4635" s="T77">PoMA_1964_YoungCzar_flk.012 (001.012)</ta>
            <ta e="T83" id="Seg_4636" s="T81">PoMA_1964_YoungCzar_flk.013 (001.013)</ta>
            <ta e="T87" id="Seg_4637" s="T83">PoMA_1964_YoungCzar_flk.014 (001.014)</ta>
            <ta e="T93" id="Seg_4638" s="T87">PoMA_1964_YoungCzar_flk.015 (001.015)</ta>
            <ta e="T97" id="Seg_4639" s="T93">PoMA_1964_YoungCzar_flk.016 (001.016)</ta>
            <ta e="T102" id="Seg_4640" s="T97">PoMA_1964_YoungCzar_flk.017 (001.017)</ta>
            <ta e="T105" id="Seg_4641" s="T102">PoMA_1964_YoungCzar_flk.018 (001.018)</ta>
            <ta e="T110" id="Seg_4642" s="T105">PoMA_1964_YoungCzar_flk.019 (001.019)</ta>
            <ta e="T116" id="Seg_4643" s="T110">PoMA_1964_YoungCzar_flk.020 (001.021)</ta>
            <ta e="T118" id="Seg_4644" s="T116">PoMA_1964_YoungCzar_flk.021 (001.022)</ta>
            <ta e="T124" id="Seg_4645" s="T118">PoMA_1964_YoungCzar_flk.022 (001.023)</ta>
            <ta e="T144" id="Seg_4646" s="T124">PoMA_1964_YoungCzar_flk.023 (001.024)</ta>
            <ta e="T151" id="Seg_4647" s="T144">PoMA_1964_YoungCzar_flk.024 (001.025)</ta>
            <ta e="T158" id="Seg_4648" s="T151">PoMA_1964_YoungCzar_flk.025 (001.026)</ta>
            <ta e="T167" id="Seg_4649" s="T158">PoMA_1964_YoungCzar_flk.026 (001.027)</ta>
            <ta e="T170" id="Seg_4650" s="T167">PoMA_1964_YoungCzar_flk.027 (001.029)</ta>
            <ta e="T177" id="Seg_4651" s="T170">PoMA_1964_YoungCzar_flk.028 (001.030)</ta>
            <ta e="T180" id="Seg_4652" s="T177">PoMA_1964_YoungCzar_flk.029 (001.031)</ta>
            <ta e="T183" id="Seg_4653" s="T180">PoMA_1964_YoungCzar_flk.030 (001.032)</ta>
            <ta e="T187" id="Seg_4654" s="T183">PoMA_1964_YoungCzar_flk.031 (001.033)</ta>
            <ta e="T192" id="Seg_4655" s="T187">PoMA_1964_YoungCzar_flk.032 (001.034)</ta>
            <ta e="T194" id="Seg_4656" s="T192">PoMA_1964_YoungCzar_flk.033 (001.036)</ta>
            <ta e="T214" id="Seg_4657" s="T194">PoMA_1964_YoungCzar_flk.034 (001.037)</ta>
            <ta e="T221" id="Seg_4658" s="T214">PoMA_1964_YoungCzar_flk.035 (001.038)</ta>
            <ta e="T227" id="Seg_4659" s="T221">PoMA_1964_YoungCzar_flk.036 (001.039)</ta>
            <ta e="T230" id="Seg_4660" s="T227">PoMA_1964_YoungCzar_flk.037 (001.040)</ta>
            <ta e="T235" id="Seg_4661" s="T230">PoMA_1964_YoungCzar_flk.038 (001.041)</ta>
            <ta e="T238" id="Seg_4662" s="T235">PoMA_1964_YoungCzar_flk.039 (001.042)</ta>
            <ta e="T246" id="Seg_4663" s="T238">PoMA_1964_YoungCzar_flk.040 (001.043)</ta>
            <ta e="T251" id="Seg_4664" s="T246">PoMA_1964_YoungCzar_flk.041 (001.044)</ta>
            <ta e="T255" id="Seg_4665" s="T251">PoMA_1964_YoungCzar_flk.042 (001.045)</ta>
            <ta e="T260" id="Seg_4666" s="T255">PoMA_1964_YoungCzar_flk.043 (001.046)</ta>
            <ta e="T263" id="Seg_4667" s="T260">PoMA_1964_YoungCzar_flk.044 (001.047)</ta>
            <ta e="T268" id="Seg_4668" s="T263">PoMA_1964_YoungCzar_flk.045 (001.048)</ta>
            <ta e="T299" id="Seg_4669" s="T268">PoMA_1964_YoungCzar_flk.046 (001.049)</ta>
            <ta e="T304" id="Seg_4670" s="T299">PoMA_1964_YoungCzar_flk.047 (001.050)</ta>
            <ta e="T310" id="Seg_4671" s="T304">PoMA_1964_YoungCzar_flk.048 (001.051)</ta>
            <ta e="T318" id="Seg_4672" s="T310">PoMA_1964_YoungCzar_flk.049 (001.052)</ta>
            <ta e="T321" id="Seg_4673" s="T318">PoMA_1964_YoungCzar_flk.050 (001.054)</ta>
            <ta e="T325" id="Seg_4674" s="T321">PoMA_1964_YoungCzar_flk.051 (001.055)</ta>
            <ta e="T328" id="Seg_4675" s="T325">PoMA_1964_YoungCzar_flk.052 (001.056)</ta>
            <ta e="T331" id="Seg_4676" s="T328">PoMA_1964_YoungCzar_flk.053 (001.057)</ta>
            <ta e="T335" id="Seg_4677" s="T331">PoMA_1964_YoungCzar_flk.054 (001.058)</ta>
            <ta e="T340" id="Seg_4678" s="T335">PoMA_1964_YoungCzar_flk.055 (001.059)</ta>
            <ta e="T366" id="Seg_4679" s="T340">PoMA_1964_YoungCzar_flk.056 (001.061)</ta>
            <ta e="T371" id="Seg_4680" s="T366">PoMA_1964_YoungCzar_flk.057 (001.062)</ta>
            <ta e="T378" id="Seg_4681" s="T371">PoMA_1964_YoungCzar_flk.058 (001.063)</ta>
            <ta e="T381" id="Seg_4682" s="T378">PoMA_1964_YoungCzar_flk.059 (001.064)</ta>
            <ta e="T387" id="Seg_4683" s="T381">PoMA_1964_YoungCzar_flk.060 (001.065)</ta>
            <ta e="T397" id="Seg_4684" s="T387">PoMA_1964_YoungCzar_flk.061 (001.066)</ta>
            <ta e="T403" id="Seg_4685" s="T397">PoMA_1964_YoungCzar_flk.062 (001.068)</ta>
            <ta e="T410" id="Seg_4686" s="T403">PoMA_1964_YoungCzar_flk.063 (001.069)</ta>
            <ta e="T418" id="Seg_4687" s="T410">PoMA_1964_YoungCzar_flk.064 (001.071)</ta>
            <ta e="T420" id="Seg_4688" s="T418">PoMA_1964_YoungCzar_flk.065 (001.072)</ta>
            <ta e="T424" id="Seg_4689" s="T420">PoMA_1964_YoungCzar_flk.066 (001.073)</ta>
            <ta e="T436" id="Seg_4690" s="T424">PoMA_1964_YoungCzar_flk.067 (001.074)</ta>
            <ta e="T441" id="Seg_4691" s="T436">PoMA_1964_YoungCzar_flk.068 (001.075)</ta>
            <ta e="T443" id="Seg_4692" s="T441">PoMA_1964_YoungCzar_flk.069 (001.076)</ta>
            <ta e="T449" id="Seg_4693" s="T443">PoMA_1964_YoungCzar_flk.070 (001.077)</ta>
            <ta e="T457" id="Seg_4694" s="T449">PoMA_1964_YoungCzar_flk.071 (001.079)</ta>
            <ta e="T465" id="Seg_4695" s="T457">PoMA_1964_YoungCzar_flk.072 (001.080)</ta>
            <ta e="T481" id="Seg_4696" s="T465">PoMA_1964_YoungCzar_flk.073 (001.081)</ta>
            <ta e="T489" id="Seg_4697" s="T481">PoMA_1964_YoungCzar_flk.074 (001.082)</ta>
            <ta e="T496" id="Seg_4698" s="T489">PoMA_1964_YoungCzar_flk.075 (001.083)</ta>
            <ta e="T500" id="Seg_4699" s="T496">PoMA_1964_YoungCzar_flk.076 (001.084)</ta>
            <ta e="T504" id="Seg_4700" s="T500">PoMA_1964_YoungCzar_flk.077 (001.084)</ta>
            <ta e="T514" id="Seg_4701" s="T504">PoMA_1964_YoungCzar_flk.078 (001.085)</ta>
            <ta e="T517" id="Seg_4702" s="T514">PoMA_1964_YoungCzar_flk.079 (001.086)</ta>
            <ta e="T521" id="Seg_4703" s="T517">PoMA_1964_YoungCzar_flk.080 (001.088)</ta>
            <ta e="T527" id="Seg_4704" s="T521">PoMA_1964_YoungCzar_flk.081 (001.089)</ta>
            <ta e="T530" id="Seg_4705" s="T527">PoMA_1964_YoungCzar_flk.082 (001.090)</ta>
            <ta e="T533" id="Seg_4706" s="T530">PoMA_1964_YoungCzar_flk.083 (001.091)</ta>
            <ta e="T536" id="Seg_4707" s="T533">PoMA_1964_YoungCzar_flk.084 (001.092)</ta>
            <ta e="T546" id="Seg_4708" s="T536">PoMA_1964_YoungCzar_flk.085 (001.093)</ta>
            <ta e="T554" id="Seg_4709" s="T546">PoMA_1964_YoungCzar_flk.086 (001.094)</ta>
            <ta e="T555" id="Seg_4710" s="T554">PoMA_1964_YoungCzar_flk.087 (001.095)</ta>
            <ta e="T559" id="Seg_4711" s="T555">PoMA_1964_YoungCzar_flk.088 (001.096)</ta>
            <ta e="T569" id="Seg_4712" s="T559">PoMA_1964_YoungCzar_flk.089 (001.097)</ta>
            <ta e="T572" id="Seg_4713" s="T569">PoMA_1964_YoungCzar_flk.090 (001.098)</ta>
            <ta e="T579" id="Seg_4714" s="T572">PoMA_1964_YoungCzar_flk.091 (001.100)</ta>
            <ta e="T581" id="Seg_4715" s="T579">PoMA_1964_YoungCzar_flk.092 (001.101)</ta>
            <ta e="T586" id="Seg_4716" s="T581">PoMA_1964_YoungCzar_flk.093 (001.102)</ta>
            <ta e="T591" id="Seg_4717" s="T586">PoMA_1964_YoungCzar_flk.094 (001.103)</ta>
            <ta e="T595" id="Seg_4718" s="T591">PoMA_1964_YoungCzar_flk.095 (001.104)</ta>
            <ta e="T601" id="Seg_4719" s="T595">PoMA_1964_YoungCzar_flk.096 (001.105)</ta>
            <ta e="T606" id="Seg_4720" s="T601">PoMA_1964_YoungCzar_flk.097 (001.106)</ta>
            <ta e="T607" id="Seg_4721" s="T606">PoMA_1964_YoungCzar_flk.098 (001.107)</ta>
            <ta e="T612" id="Seg_4722" s="T607">PoMA_1964_YoungCzar_flk.099 (001.108)</ta>
            <ta e="T617" id="Seg_4723" s="T612">PoMA_1964_YoungCzar_flk.100 (001.110)</ta>
            <ta e="T622" id="Seg_4724" s="T617">PoMA_1964_YoungCzar_flk.101 (001.111)</ta>
            <ta e="T628" id="Seg_4725" s="T622">PoMA_1964_YoungCzar_flk.102 (001.112)</ta>
            <ta e="T630" id="Seg_4726" s="T628">PoMA_1964_YoungCzar_flk.103 (001.113)</ta>
            <ta e="T642" id="Seg_4727" s="T630">PoMA_1964_YoungCzar_flk.104 (001.114)</ta>
            <ta e="T645" id="Seg_4728" s="T642">PoMA_1964_YoungCzar_flk.105 (001.115)</ta>
            <ta e="T648" id="Seg_4729" s="T645">PoMA_1964_YoungCzar_flk.106 (001.116)</ta>
            <ta e="T650" id="Seg_4730" s="T648">PoMA_1964_YoungCzar_flk.107 (001.117)</ta>
            <ta e="T655" id="Seg_4731" s="T650">PoMA_1964_YoungCzar_flk.108 (001.118)</ta>
            <ta e="T662" id="Seg_4732" s="T655">PoMA_1964_YoungCzar_flk.109 (001.119)</ta>
            <ta e="T665" id="Seg_4733" s="T662">PoMA_1964_YoungCzar_flk.110 (001.120)</ta>
            <ta e="T676" id="Seg_4734" s="T665">PoMA_1964_YoungCzar_flk.111 (001.121)</ta>
            <ta e="T679" id="Seg_4735" s="T676">PoMA_1964_YoungCzar_flk.112 (001.122)</ta>
            <ta e="T687" id="Seg_4736" s="T679">PoMA_1964_YoungCzar_flk.113 (001.123)</ta>
            <ta e="T689" id="Seg_4737" s="T687">PoMA_1964_YoungCzar_flk.114 (001.124)</ta>
            <ta e="T690" id="Seg_4738" s="T689">PoMA_1964_YoungCzar_flk.115 (001.125)</ta>
            <ta e="T694" id="Seg_4739" s="T690">PoMA_1964_YoungCzar_flk.116 (001.126)</ta>
            <ta e="T696" id="Seg_4740" s="T694">PoMA_1964_YoungCzar_flk.117 (001.128)</ta>
            <ta e="T702" id="Seg_4741" s="T696">PoMA_1964_YoungCzar_flk.118 (001.129)</ta>
            <ta e="T704" id="Seg_4742" s="T702">PoMA_1964_YoungCzar_flk.119 (001.130)</ta>
            <ta e="T707" id="Seg_4743" s="T704">PoMA_1964_YoungCzar_flk.120 (001.131)</ta>
            <ta e="T710" id="Seg_4744" s="T707">PoMA_1964_YoungCzar_flk.121 (001.132)</ta>
            <ta e="T720" id="Seg_4745" s="T710">PoMA_1964_YoungCzar_flk.122 (001.133)</ta>
            <ta e="T731" id="Seg_4746" s="T720">PoMA_1964_YoungCzar_flk.123 (001.134)</ta>
            <ta e="T735" id="Seg_4747" s="T731">PoMA_1964_YoungCzar_flk.124 (001.135)</ta>
            <ta e="T737" id="Seg_4748" s="T735">PoMA_1964_YoungCzar_flk.125 (001.136)</ta>
            <ta e="T740" id="Seg_4749" s="T737">PoMA_1964_YoungCzar_flk.126 (001.137)</ta>
            <ta e="T741" id="Seg_4750" s="T740">PoMA_1964_YoungCzar_flk.127 (001.138)</ta>
            <ta e="T746" id="Seg_4751" s="T741">PoMA_1964_YoungCzar_flk.128 (001.139)</ta>
            <ta e="T748" id="Seg_4752" s="T746">PoMA_1964_YoungCzar_flk.129 (001.140)</ta>
            <ta e="T754" id="Seg_4753" s="T748">PoMA_1964_YoungCzar_flk.130 (001.141)</ta>
            <ta e="T761" id="Seg_4754" s="T754">PoMA_1964_YoungCzar_flk.131 (001.142)</ta>
            <ta e="T765" id="Seg_4755" s="T761">PoMA_1964_YoungCzar_flk.132 (001.144)</ta>
            <ta e="T768" id="Seg_4756" s="T765">PoMA_1964_YoungCzar_flk.133 (001.145)</ta>
            <ta e="T773" id="Seg_4757" s="T768">PoMA_1964_YoungCzar_flk.134 (001.146)</ta>
            <ta e="T777" id="Seg_4758" s="T773">PoMA_1964_YoungCzar_flk.135 (001.147)</ta>
            <ta e="T782" id="Seg_4759" s="T777">PoMA_1964_YoungCzar_flk.136 (001.148)</ta>
            <ta e="T794" id="Seg_4760" s="T782">PoMA_1964_YoungCzar_flk.137 (001.149)</ta>
            <ta e="T797" id="Seg_4761" s="T794">PoMA_1964_YoungCzar_flk.138 (001.150)</ta>
            <ta e="T798" id="Seg_4762" s="T797">PoMA_1964_YoungCzar_flk.139 (001.151)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T4" id="Seg_4763" s="T0">Дьэ Уол ыраактаагы олорбута.</ta>
            <ta e="T7" id="Seg_4764" s="T4">Улкатыгар Умнаһыт паасынайдаак.</ta>
            <ta e="T23" id="Seg_4765" s="T7">Умнаһыт паасынай бу ыраактаагыга үлэлээн һарсиэрдааҥҥы каранаттан киэһэ каранага диэри биир луоску бурдук иһин үлэлиир эбит.</ta>
            <ta e="T26" id="Seg_4766" s="T23">Манан ииттэн олороллор.</ta>
            <ta e="T31" id="Seg_4767" s="T26">Умнаһыт паасынай биир кыыс оголоок.</ta>
            <ta e="T36" id="Seg_4768" s="T31">Дьэ биирдэ Уол ыраактаагы диир:</ta>
            <ta e="T45" id="Seg_4769" s="T36">— Кайа дьэ, Умнаһыт паасынай, төһө өйдөөк киһи төрөөн төнүннүҥ?</ta>
            <ta e="T50" id="Seg_4770" s="T45">Таабурунна таай: "Минньигэстэн минньигэс туогуй?" </ta>
            <ta e="T55" id="Seg_4771" s="T50">һарсиэрда кэлэн тогус чааска һаҥараар.</ta>
            <ta e="T62" id="Seg_4772" s="T55">Ону таайбатаккына ыйыыр маспар моойдоок баскын быһыам.</ta>
            <ta e="T77" id="Seg_4773" s="T62">Умнаһыт паасынай того да өйө кыайбат, минньигэс аһы билээктээбэт, ыраактаагы аһа барыта минньигэс курдук гиниэкэ.</ta>
            <ta e="T81" id="Seg_4774" s="T77">Дьиэтигэр кэлэн ытана олордо.</ta>
            <ta e="T83" id="Seg_4775" s="T81">Кыыһа ыйытар:</ta>
            <ta e="T87" id="Seg_4776" s="T83">— Кайа, агаа, того ытаатын?</ta>
            <ta e="T93" id="Seg_4777" s="T87">— Дьэ, тогойум, һылдьыбытым һыччак, олорбутум уһуга.</ta>
            <ta e="T97" id="Seg_4778" s="T93">Уол ыраактаагы таабуруун биэрдэ.</ta>
            <ta e="T102" id="Seg_4779" s="T97">Таайбатакпына моойдоок баспын быһыак буолла.</ta>
            <ta e="T105" id="Seg_4780" s="T102">— Тугу тааттарар ол?</ta>
            <ta e="T110" id="Seg_4781" s="T105">— "Минньигэстэн минньигэс туогуй?" — диир, — диэбит.</ta>
            <ta e="T116" id="Seg_4782" s="T110">— Ээ, агаа, туһата һуокка того ытыыгын.</ta>
            <ta e="T118" id="Seg_4783" s="T116">Утуйаактаа, һынньан.</ta>
            <ta e="T124" id="Seg_4784" s="T118">"Минньигэстэн минньигэһи билэр һирим биир, — диэр.</ta>
            <ta e="T144" id="Seg_4785" s="T124">— Һарсиэрдааҥҥы караҥаттан киэһээҥҥи караҥага диэри үлэлээн, эн һааккын-киргин ыраастаан, биир луоску бурдугу илтэкпинэ, эмээксиним буор голомо дьиэтин отуннагына, муннум һылыйар.</ta>
            <ta e="T151" id="Seg_4786" s="T144">Ол бурдуккаанынан толоҥкуо оҥордогуна, аһаабакка да утуйабын.</ta>
            <ta e="T158" id="Seg_4787" s="T151">Онон минньигэстэн минньигэс утуйар уу буолуоктаак", — диэр.</ta>
            <ta e="T167" id="Seg_4788" s="T158">Кини онтон да ордугу билэрэ буолуо дуо?— диэбит кыыһа.</ta>
            <ta e="T170" id="Seg_4789" s="T167">Огонньор утуйан каалла.</ta>
            <ta e="T177" id="Seg_4790" s="T170">һарсиэрда эрдэ туран тогус чааска ыраактыыгыга тийэр.</ta>
            <ta e="T180" id="Seg_4791" s="T177">Уол ыраактаагы ыйытар:</ta>
            <ta e="T183" id="Seg_4792" s="T180">— Кайа, таайдыҥ дуо?</ta>
            <ta e="T187" id="Seg_4793" s="T183">— Ээ, таайарга талы гынным.</ta>
            <ta e="T192" id="Seg_4794" s="T187">һөбө дуу, һыыһата дуу? — диир.</ta>
            <ta e="T194" id="Seg_4795" s="T192">— Дьэ тугуй?</ta>
            <ta e="T214" id="Seg_4796" s="T194">— Һарсиэрдааҥҥы караҥаттан киэһээҥҥи караҥага диэри үлэлээн, эн һааккын-киргин ыраастаан, биир луоску бурдугу илтэкпинэ, эмээксиним буор голомо дьиэтин отуннагына, муннум һылыйар.</ta>
            <ta e="T221" id="Seg_4797" s="T214">Ол бурдуккаанынан толоҥкуо оҥордогуна, аһаабакка да утуйабын.</ta>
            <ta e="T227" id="Seg_4798" s="T221">Онон минньигэстэн минньигэс утуйар уу буолуоктаак.</ta>
            <ta e="T230" id="Seg_4799" s="T227">Уол ыраактаагы диир:</ta>
            <ta e="T235" id="Seg_4800" s="T230">— Оо дьэ, өйдөөк киһигин эбит.</ta>
            <ta e="T238" id="Seg_4801" s="T235">Кайдак инэн-батан һытагын?</ta>
            <ta e="T246" id="Seg_4802" s="T238">Өссүө таай: "Орто дойдуга түргэнтэн түргэн туок баарый?" </ta>
            <ta e="T251" id="Seg_4803" s="T246">һарсиэрда таайбатаккына, моойдоок баскын быһабын.</ta>
            <ta e="T255" id="Seg_4804" s="T251">Огонньор тугу да тобулбат.</ta>
            <ta e="T260" id="Seg_4805" s="T255">Тийэн ытана олордогуна, кыыһа ыйытар.</ta>
            <ta e="T263" id="Seg_4806" s="T260">Огонньор инньэ-инньэ диир.</ta>
            <ta e="T268" id="Seg_4807" s="T263">— Туок туһата һуокка ытыыгын, утуй.</ta>
            <ta e="T299" id="Seg_4808" s="T268">һарсиэрда этээр: "Түргэнтэн түргэн, диэр, куһаган буруолаак буор голомобуттан такса көтөн, каракпын кастырыта һотон одуулаан көрдөкпүнэ: каллаан диэк кантайдакпына — каллаан һулуһун карагым аага көрөр, һир диэк көрдөкпүнэ — һир отун-маһын аага көрөбүн.</ta>
            <ta e="T304" id="Seg_4809" s="T299">Карагым ыларыгар каһан да тиийбэппин.</ta>
            <ta e="T310" id="Seg_4810" s="T304">Онон түргэнтэн түргэн киһи көрүүтэ", — диэр.</ta>
            <ta e="T318" id="Seg_4811" s="T310">Онтон ордугу кини билэрэ буолуо дуо?! — диир кыыс.</ta>
            <ta e="T321" id="Seg_4812" s="T318">Огонньор утуйан каалла.</ta>
            <ta e="T325" id="Seg_4813" s="T321">Һарсиэрдэ туран ыраактаагытыгар бардага.</ta>
            <ta e="T328" id="Seg_4814" s="T325">Уол ыраактаагы ыйытар:</ta>
            <ta e="T331" id="Seg_4815" s="T328">— Кайа таайдыҥ дуо?</ta>
            <ta e="T335" id="Seg_4816" s="T331">— Ээ, таайарга талы гынным.</ta>
            <ta e="T340" id="Seg_4817" s="T335">Һөбө дуу, һыыһата дуу? — диир.</ta>
            <ta e="T366" id="Seg_4818" s="T340">— Куһаган буруолаак буор голомобуттан такса көтөн, каракпын кастырыта һотон одууланан көрдөкпүнэ: каллаан диэк кантайдакпына — каллаан һулуһун карагым аага көрөр, һир диэк көрдөкпүнэ — һир отун-маһын аага көрөбүн.</ta>
            <ta e="T371" id="Seg_4819" s="T366">Карагым ыларыгар каһан да тийбэппин.</ta>
            <ta e="T378" id="Seg_4820" s="T371">Онон түргэнтэн түргэн киһи көрүүтэ эбит, — диир.</ta>
            <ta e="T381" id="Seg_4821" s="T378">Уол ыраактаагы һөгөр:</ta>
            <ta e="T387" id="Seg_4822" s="T381">— Оо, огонньор, дьэ өйдөөк киһигин эбит.</ta>
            <ta e="T397" id="Seg_4823" s="T387">Дьэ туок да үс төгүллээк, — таксан иргэк коруобаны тутан биэрэр,</ta>
            <ta e="T403" id="Seg_4824" s="T397">— Маны һарсиэрда төрөтөн тугутун батыһыннаран эгэлээр.</ta>
            <ta e="T410" id="Seg_4825" s="T403">Кайа уоруктатын комуйан эгэлээйэгин?! — диэбит Уол ыраактаагы.</ta>
            <ta e="T418" id="Seg_4826" s="T410">Огонньор эрэйдээк иргэк коруобатын һиэтэн тийэн ытыы олордо.</ta>
            <ta e="T420" id="Seg_4827" s="T418">Кыыһа ыйытта:</ta>
            <ta e="T424" id="Seg_4828" s="T420">— Кайа, агаа, того ытаатыҥ?</ta>
            <ta e="T436" id="Seg_4829" s="T424">— Кайа, иргэк коруобаны биэрдэ, төрөтөн эгэл диир, иргэк коруоба кантынан төрөөччүнүй, куугу?</ta>
            <ta e="T441" id="Seg_4830" s="T436">— Ээ, иргэк коруоба кантан төрүөй?!</ta>
            <ta e="T443" id="Seg_4831" s="T441">Ката астаммыппыт.</ta>
            <ta e="T449" id="Seg_4832" s="T443">Таксаҥҥын өлөрбөктөө, этин һиэкпит! — диир кыыс.</ta>
            <ta e="T457" id="Seg_4833" s="T449">Огонньор иргэк коруобатын өлөрөн кээһэр, дьэ маны һиэктэрэ.</ta>
            <ta e="T465" id="Seg_4834" s="T457">Кыыһа утакаага коруоба тунньагын, тугулун комуйан биэрдэ агатыгар:</ta>
            <ta e="T481" id="Seg_4835" s="T465">— "Дьэ ити кара туньага — иньэтэ, тугула — тубута" диэн бараан ыраактаагын остуолугар ого оонньуурун курдук һубуруталаан кээһээр.</ta>
            <ta e="T489" id="Seg_4836" s="T481">"Иргэк коруобаттан төрүүр агай һирин бу буллум" диэр.</ta>
            <ta e="T496" id="Seg_4837" s="T489">Онтон ордугу да кини билэрэ буолуо дуо?!</ta>
            <ta e="T500" id="Seg_4838" s="T496">Дьэ оччого моһуйан ыйытыа: </ta>
            <ta e="T504" id="Seg_4839" s="T500">"Бэйэҥ өйгүнэн гымматыҥ буолуо.</ta>
            <ta e="T514" id="Seg_4840" s="T504">Итиччэ өйдөөгүҥ буоллар мин иикпин-һаакпын ыраастыы иҥэн-батан һытыа һуок этиҥ.</ta>
            <ta e="T517" id="Seg_4841" s="T514">Ким үөрэтэр?" </ta>
            <ta e="T521" id="Seg_4842" s="T517">Онно кистээйэгин, — диир кыыһа.</ta>
            <ta e="T527" id="Seg_4843" s="T521">— "Куһаган кыыс оголоокпун, ол үөрэтээктиир", — диэр.</ta>
            <ta e="T530" id="Seg_4844" s="T527">Огонньор һарсиэрда барар.</ta>
            <ta e="T533" id="Seg_4845" s="T530">Уол ыраактаагы ыйытар:</ta>
            <ta e="T536" id="Seg_4846" s="T533">— Кайа таайдыҥ дуо?</ta>
            <ta e="T546" id="Seg_4847" s="T536">Огонньор коруоба туньагын, тугулун остуолга ого оонньуурун курдук һубуруталаан кээһэр.</ta>
            <ta e="T554" id="Seg_4848" s="T546">— Иргэк коруобаттан төрүүр агай һирин бу буллум, — диир.</ta>
            <ta e="T555" id="Seg_4849" s="T554">Ыраактаагы:</ta>
            <ta e="T559" id="Seg_4850" s="T555">— Бэйэҥ өйгүнэн гымматыҥ буолуо.</ta>
            <ta e="T569" id="Seg_4851" s="T559">Итиччэ өйдөөгуҥ буоллар мин иикпин-һаакпын ыраастыы иҥэн-батан һытыа һуок этиҥ.</ta>
            <ta e="T572" id="Seg_4852" s="T569">Ким үөрэтэр? — диир.</ta>
            <ta e="T579" id="Seg_4853" s="T572">— Куһаган кыыс оголоокпун, ол үөрэтээктиир, — диир огонньор.</ta>
            <ta e="T581" id="Seg_4854" s="T579">Уол ыраактаагы:</ta>
            <ta e="T586" id="Seg_4855" s="T581">— Кыыһыҥ өйдөөк кыыс төрөөбүт, — диир.</ta>
            <ta e="T591" id="Seg_4856" s="T586">Тимир кокороогу, түгэгэ кайагастаагы биэрэр.</ta>
            <ta e="T595" id="Seg_4857" s="T591">— Маны һамаан ыыттын, — диир.</ta>
            <ta e="T601" id="Seg_4858" s="T595">— Ону һамаабатагына, иккиэнҥитин төбөгүтүн быһыам, — диир.</ta>
            <ta e="T606" id="Seg_4859" s="T601">Огонньор кокороогу илдьэ барбыта ытыы-ытыы.</ta>
            <ta e="T607" id="Seg_4860" s="T606">Кыыс:</ta>
            <ta e="T612" id="Seg_4861" s="T607">— Кайа, агаа, туогуҥ кокороогой? — диир.</ta>
            <ta e="T617" id="Seg_4862" s="T612">— Кайа-куу, дьэ өлөрөр күнэ кэллэ.</ta>
            <ta e="T622" id="Seg_4863" s="T617">Бу кокороогу һамаан ыыттын — диир.</ta>
            <ta e="T628" id="Seg_4864" s="T622">— Буну һамаабатагына, иккиэнҥитин төбөгүтүн быһыам, — диир.</ta>
            <ta e="T630" id="Seg_4865" s="T628">Кыыс диир:</ta>
            <ta e="T642" id="Seg_4866" s="T630">— Илт төттерү: дьактар киһи һамыам, эр киһи кокороогу чорчокоот көрдүк тиэрэн ыыттын.</ta>
            <ta e="T645" id="Seg_4867" s="T642">Огонньор илдьэн биэрэр.</ta>
            <ta e="T648" id="Seg_4868" s="T645">Ууол ыраактаагы һөгөр.</ta>
            <ta e="T650" id="Seg_4869" s="T648">Огонньору ыытар.</ta>
            <ta e="T655" id="Seg_4870" s="T650">— Бэйэм кыыскын көрө барыам, — диир.</ta>
            <ta e="T662" id="Seg_4871" s="T655">Уол ыраактаагы, кыыһы көрөөт, багарар, дьактар ылар.</ta>
            <ta e="T665" id="Seg_4872" s="T662">Төһөнү-каччаны олорбуттара буолла.</ta>
            <ta e="T676" id="Seg_4873" s="T665">Дьактардаммытыттан Уол ыраактаагы куукула курдук олорчок буолла, барытын дьактара һубэлиир, быһаарар.</ta>
            <ta e="T679" id="Seg_4874" s="T676">Уол ыраактаагы тылланар:</ta>
            <ta e="T687" id="Seg_4875" s="T679">— Догоо, бу биир дойдуга икки ыраактаагы буоларбыт табыллыбат.</ta>
            <ta e="T689" id="Seg_4876" s="T687">Биһиги араксыак.</ta>
            <ta e="T690" id="Seg_4877" s="T689">Дьактара:</ta>
            <ta e="T694" id="Seg_4878" s="T690">— Дьэ араксыак дуу? — диир.</ta>
            <ta e="T696" id="Seg_4879" s="T694">Уол ыраактаагы:</ta>
            <ta e="T702" id="Seg_4880" s="T696">— Туок багалаак һиргин барытын ыл, — диир.</ta>
            <ta e="T704" id="Seg_4881" s="T702">Дьактар һөбүлэһэр.</ta>
            <ta e="T707" id="Seg_4882" s="T704">Араксар түүннэрэ-кээ буолар.</ta>
            <ta e="T710" id="Seg_4883" s="T707">Эрэ утуйан каалар.</ta>
            <ta e="T720" id="Seg_4884" s="T710">Дьактар эрин һуорганнары, тэллэктэри агатын буор голомо дьиэтигэр көтөгөн илдьэр.</ta>
            <ta e="T731" id="Seg_4885" s="T720">Уол ыраактаагы һарсиэрда уһуктан һоһуйар, тыыллан көрбүтэ баһа-атага голомо маһыгар кэбиллэр.</ta>
            <ta e="T735" id="Seg_4886" s="T731">— Кайа-кээ, бу канна баарбытый?</ta>
            <ta e="T737" id="Seg_4887" s="T735">Того кэлбитимий?</ta>
            <ta e="T740" id="Seg_4888" s="T737">— дьактарын кэбиэлээн уһугуннарар.</ta>
            <ta e="T741" id="Seg_4889" s="T740">Дьактара:</ta>
            <ta e="T746" id="Seg_4890" s="T741">— Кайа догоо, мин эгэлбитим, — диир.</ta>
            <ta e="T748" id="Seg_4891" s="T746">— Того эгэлбиккиний?</ta>
            <ta e="T754" id="Seg_4892" s="T748">— Тыый, бэйэгин багардым да бэйэгин эгэллим.</ta>
            <ta e="T761" id="Seg_4893" s="T754">Туогу багараргын ылаар диэбитиҥ дии! — диир дьактар.</ta>
            <ta e="T765" id="Seg_4894" s="T761">Уол ыраактаагы кыайан аракпата.</ta>
            <ta e="T768" id="Seg_4895" s="T765">Дьэ дьактара диир:</ta>
            <ta e="T773" id="Seg_4896" s="T768">— Оччо өйдөөк киһи буолбатак эбиккин.</ta>
            <ta e="T777" id="Seg_4897" s="T773">Дьоҥҥун һүбэлээн бэйэҥ олор.</ta>
            <ta e="T782" id="Seg_4898" s="T777">Мин көннөрү күсээйкэ буолан олоруом.</ta>
            <ta e="T794" id="Seg_4899" s="T782">Умнаһыт паасынай кыыһын үтүөтүнэн һиэркилэ таас дьиэгэ олордого, буор голомоттон араксан уһугуттан.</ta>
            <ta e="T797" id="Seg_4900" s="T794">Байан-тайан олордокторо дьэ.</ta>
            <ta e="T798" id="Seg_4901" s="T797">Элэтэ.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_4902" s="T0">Dʼe U͡ol ɨraːktaːgɨ olorbuta. </ta>
            <ta e="T7" id="Seg_4903" s="T4">Ulkatɨgar Umnahɨt paːsɨnajdaːk. </ta>
            <ta e="T23" id="Seg_4904" s="T7">Umnahɨt paːsɨnaj bu ɨraːktaːgɨga üleleːn harsi͡erdaːŋŋɨ karaŋattan ki͡ehe karaŋaga di͡eri biːr lu͡osku burduk ihin üleliːr ebit. </ta>
            <ta e="T26" id="Seg_4905" s="T23">Manan iːtten olorollor. </ta>
            <ta e="T31" id="Seg_4906" s="T26">Umnahɨt paːsɨnaj biːr kɨːs ogoloːk. </ta>
            <ta e="T36" id="Seg_4907" s="T31">Dʼe biːrde U͡ol ɨraːktaːgɨ diːr: </ta>
            <ta e="T45" id="Seg_4908" s="T36">"Kaja dʼe, Umnahɨt paːsɨnaj, töhö öjdöːk kihi töröːn tönünnüŋ? </ta>
            <ta e="T50" id="Seg_4909" s="T45">Taːburunna taːj, minnʼigesten minnʼiges tu͡oguj? </ta>
            <ta e="T55" id="Seg_4910" s="T50">Harsi͡erda kelen togus čaːska haŋaraːr. </ta>
            <ta e="T62" id="Seg_4911" s="T55">Onu taːjbatakkɨna ɨjɨːr maspar moːjdoːk baskɨn bɨhɨ͡am. </ta>
            <ta e="T77" id="Seg_4912" s="T62">Umnahɨt paːsɨnaj togo da öjö kɨ͡ajbat, minnʼiges ahɨ bileːkteːbet, ɨraːktaːgɨ aha barɨta minnʼiges kurduk gini͡eke. </ta>
            <ta e="T81" id="Seg_4913" s="T77">Dʼi͡etiger kelen ɨtana olordo. </ta>
            <ta e="T83" id="Seg_4914" s="T81">Kɨːha ɨjɨtar: </ta>
            <ta e="T87" id="Seg_4915" s="T83">"Kaja, agaː, togo ɨtaːtɨn?" </ta>
            <ta e="T93" id="Seg_4916" s="T87">"Dʼe, togojum, hɨldʼɨbɨtɨm hɨččak, olorbutum uhuga. </ta>
            <ta e="T97" id="Seg_4917" s="T93">U͡ol ɨraːktaːgɨ taːburuːn bi͡erde. </ta>
            <ta e="T102" id="Seg_4918" s="T97">Taːjbatakpɨna moːjdoːk baspɨn bɨhɨ͡ak bu͡olla." </ta>
            <ta e="T105" id="Seg_4919" s="T102">"Tugu taːttarar ol?" </ta>
            <ta e="T110" id="Seg_4920" s="T105">""Minnʼigesten minnʼiges tu͡oguj" diːr", di͡ebit. </ta>
            <ta e="T116" id="Seg_4921" s="T110">"Eː, agaː, tuhata hu͡okka togo ɨtɨːgɨn. </ta>
            <ta e="T118" id="Seg_4922" s="T116">Utujaːktaː, hɨnnʼan. </ta>
            <ta e="T124" id="Seg_4923" s="T118">"Minnʼigesten minnʼigehi biler hirim biːr", di͡er. </ta>
            <ta e="T144" id="Seg_4924" s="T124">"Harsi͡erdaːŋŋɨ karaŋattan ki͡eheːŋŋi karaŋaga di͡eri üleleːn, en haːkkɨn-kirgin ɨraːstaːn, biːr lu͡osku burdugu iltekpine, emeːksinim bu͡or golomo dʼi͡etin otunnagɨna, munnum hɨlɨjar. </ta>
            <ta e="T151" id="Seg_4925" s="T144">Ol burdukkaːnɨnan toloŋku͡o oŋordoguna, ahaːbakka da utujabɨn. </ta>
            <ta e="T158" id="Seg_4926" s="T151">Onon minnʼigesten minnʼiges utujar uː bu͡olu͡oktaːk", di͡er. </ta>
            <ta e="T167" id="Seg_4927" s="T158">Kini onton da ordugu bilere bu͡olu͡o du͡o", di͡ebit kɨːha. </ta>
            <ta e="T170" id="Seg_4928" s="T167">Ogonnʼor utujan kaːlla. </ta>
            <ta e="T177" id="Seg_4929" s="T170">Harsi͡erda erde turan togus čaːska ɨraːktɨːgɨga tijer. </ta>
            <ta e="T180" id="Seg_4930" s="T177">U͡ol ɨraːktaːgɨ ɨjɨtar: </ta>
            <ta e="T183" id="Seg_4931" s="T180">"Kaja, taːjdɨŋ du͡o?" </ta>
            <ta e="T187" id="Seg_4932" s="T183">"Eː, taːjarga talɨ gɨnnɨm. </ta>
            <ta e="T192" id="Seg_4933" s="T187">Höbö duː, hɨːhata duː", diːr. </ta>
            <ta e="T194" id="Seg_4934" s="T192">"Dʼe tuguj?" </ta>
            <ta e="T214" id="Seg_4935" s="T194">"Harsi͡erdaːŋŋɨ karaŋattan ki͡eheːŋŋi karaŋaga di͡eri üleleːn, en haːkkɨn-kirgin ɨraːstaːn, biːr lu͡osku burdugu iltekpine, emeːksinim bu͡or golomo dʼi͡etin otunnagɨna, munnum hɨlɨjar. </ta>
            <ta e="T221" id="Seg_4936" s="T214">Ol burdukkaːnɨnan toloŋku͡o oŋordoguna, ahaːbakka da utujabɨn. </ta>
            <ta e="T227" id="Seg_4937" s="T221">Onon minnʼigesten minnʼiges utujar uː bu͡olu͡oktaːk." </ta>
            <ta e="T230" id="Seg_4938" s="T227">U͡ol ɨraːktaːgɨ diːr: </ta>
            <ta e="T235" id="Seg_4939" s="T230">"Oː dʼe, öjdöːk kihigin ebit. </ta>
            <ta e="T238" id="Seg_4940" s="T235">Kajdak inen-batan hɨtagɨn? </ta>
            <ta e="T246" id="Seg_4941" s="T238">Össü͡ö taːj, orto dojduga türgenten türgen tu͡ok baːrɨj? </ta>
            <ta e="T251" id="Seg_4942" s="T246">Harsi͡erda taːjbatakkɨna, moːjdoːk baskɨn bɨhabɨn." </ta>
            <ta e="T255" id="Seg_4943" s="T251">Ogonnʼor tugu da tobulbat. </ta>
            <ta e="T260" id="Seg_4944" s="T255">Tijen ɨtana olordoguna, kɨːha ɨjɨtar. </ta>
            <ta e="T263" id="Seg_4945" s="T260">Ogonnʼor innʼe-innʼe diːr. </ta>
            <ta e="T268" id="Seg_4946" s="T263">"Tu͡ok tuhata hu͡okka ɨtɨːgɨn, utuj. </ta>
            <ta e="T299" id="Seg_4947" s="T268">Harsi͡erda eteːr "türgenten türgen" di͡er, "kuhagan buru͡olaːk bu͡or golomobuttan taksa kötön, karakpɨn kastɨrɨta hoton oduːlaːn kördökpüne, kallaːn di͡ek kantajdakpɨna— kallaːn huluhun karagɨm aːga körör, hir di͡ek kördökpüne— hir otun-mahɨn aːga köröbün. </ta>
            <ta e="T304" id="Seg_4948" s="T299">Karagɨm ɨlarɨgar kahan da tiːjbeppin. </ta>
            <ta e="T310" id="Seg_4949" s="T304">Onon türgenten türgen kihi körüːte", di͡er. </ta>
            <ta e="T318" id="Seg_4950" s="T310">Onton ordugu kini bilere bu͡olu͡o du͡o", diːr kɨːs. </ta>
            <ta e="T321" id="Seg_4951" s="T318">Ogonnʼor utujan kaːlla. </ta>
            <ta e="T325" id="Seg_4952" s="T321">Harsi͡erde turan ɨraːktaːgɨtɨgar bardaga. </ta>
            <ta e="T328" id="Seg_4953" s="T325">U͡ol ɨraːktaːgɨ ɨjɨtar: </ta>
            <ta e="T331" id="Seg_4954" s="T328">"Kaja taːjdɨŋ du͡o?" </ta>
            <ta e="T335" id="Seg_4955" s="T331">"Eː, taːjarga talɨ gɨnnɨm. </ta>
            <ta e="T340" id="Seg_4956" s="T335">Höbö duː, hɨːhata duː", diːr. </ta>
            <ta e="T366" id="Seg_4957" s="T340">"Kuhagan buru͡olaːk bu͡or golomobuttan taksa kötön, karakpɨn kastɨrɨta hoton oduːlanan kördökpüne, kallaːn di͡ek kantajdakpɨna— kallaːn huluhun karagɨm aːga körör, hir di͡ek kördökpüne— hir otun-mahɨn aːga köröbün. </ta>
            <ta e="T371" id="Seg_4958" s="T366">Karagɨm ɨlarɨgar kahan da tijbeppin. </ta>
            <ta e="T378" id="Seg_4959" s="T371">Onon türgenten türgen kihi körüːte ebit", diːr. </ta>
            <ta e="T381" id="Seg_4960" s="T378">U͡ol ɨraːktaːgɨ högör: </ta>
            <ta e="T387" id="Seg_4961" s="T381">"Oː, ogonnʼor, dʼe öjdöːk kihigin ebit. </ta>
            <ta e="T397" id="Seg_4962" s="T387">Dʼe tu͡ok da üs tögülleːk", taksan irgek koru͡obanɨ tutan bi͡erer. </ta>
            <ta e="T403" id="Seg_4963" s="T397">"Manɨ harsi͡erda törötön tugutun batɨhɨnnaran egeleːr. </ta>
            <ta e="T410" id="Seg_4964" s="T403">Kaja u͡oruktatɨn komujan egeleːjegin", di͡ebit U͡ol ɨraːktaːgɨ. </ta>
            <ta e="T418" id="Seg_4965" s="T410">Ogonnʼor erejdeːk irgek koru͡obatɨn hi͡eten tijen ɨtɨː olordo. </ta>
            <ta e="T420" id="Seg_4966" s="T418">Kɨːha ɨjɨtta: </ta>
            <ta e="T424" id="Seg_4967" s="T420">"Kaja, agaː, togo ɨtaːtɨŋ?" </ta>
            <ta e="T436" id="Seg_4968" s="T424">"Kaja, irgek koru͡obanɨ bi͡erde, törötön egel diːr, irgek koru͡oba kantɨnan töröːččünüj, kuːgu?" </ta>
            <ta e="T441" id="Seg_4969" s="T436">"Eː, irgek koru͡oba kantan törü͡öj? </ta>
            <ta e="T443" id="Seg_4970" s="T441">Kata astammɨppɨt. </ta>
            <ta e="T449" id="Seg_4971" s="T443">Taksaŋŋɨn ölörböktöː, etin hi͡ekpit", diːr kɨːs. </ta>
            <ta e="T457" id="Seg_4972" s="T449">Ogonnʼor irgek koru͡obatɨn ölörön keːher, dʼe manɨ hi͡ektere. </ta>
            <ta e="T465" id="Seg_4973" s="T457">Kɨːha utakaːga koru͡oba tunnʼagɨn, tugulun komujan bi͡erde agatɨgar: </ta>
            <ta e="T481" id="Seg_4974" s="T465">""Dʼe iti kara tunʼaga— inʼete, tugula— tubuta", di͡en baraːn ɨraːktaːgɨn ostu͡olugar ogo oːnnʼuːrun kurduk huburutalaːn keːheːr. </ta>
            <ta e="T489" id="Seg_4975" s="T481">"Irgek koru͡obattan törüːr agaj hirin bu bullum", di͡er. </ta>
            <ta e="T496" id="Seg_4976" s="T489">Onton ordugu da kini bilere bu͡olu͡o du͡o? </ta>
            <ta e="T500" id="Seg_4977" s="T496">Dʼe oččogo mohujan ɨjɨtɨ͡a: </ta>
            <ta e="T504" id="Seg_4978" s="T500">"Bejeŋ öjgünen gɨmmatɨŋ bu͡olu͡o. </ta>
            <ta e="T514" id="Seg_4979" s="T504">Itičče öjdöːgüŋ bu͡ollar min iːkpin-haːkpɨn ɨraːstɨː iŋen-batan hɨtɨ͡a hu͡ok etiŋ. </ta>
            <ta e="T517" id="Seg_4980" s="T514">Kim ü͡öreter", di͡ege. </ta>
            <ta e="T521" id="Seg_4981" s="T517">Onno kisteːjegin", diːr kɨːha. </ta>
            <ta e="T527" id="Seg_4982" s="T521">""Kuhagan kɨːs ogoloːkpun, ol ü͡öreteːktiːr", di͡er." </ta>
            <ta e="T530" id="Seg_4983" s="T527">Ogonnʼor harsi͡erda barar. </ta>
            <ta e="T533" id="Seg_4984" s="T530">U͡ol ɨraːktaːgɨ ɨjɨtar: </ta>
            <ta e="T536" id="Seg_4985" s="T533">"Kaja taːjdɨŋ du͡o?" </ta>
            <ta e="T546" id="Seg_4986" s="T536">Ogonnʼor koru͡oba tunʼagɨn, tugulun ostu͡olga ogo oːnnʼuːrun kurduk huburutalaːn keːher. </ta>
            <ta e="T554" id="Seg_4987" s="T546">"Irgek koru͡obattan törüːr agaj hirin bu bullum", diːr. </ta>
            <ta e="T555" id="Seg_4988" s="T554">ɨraːktaːgɨ: </ta>
            <ta e="T559" id="Seg_4989" s="T555">"Bejeŋ öjgünen gɨmmatɨŋ bu͡olu͡o. </ta>
            <ta e="T569" id="Seg_4990" s="T559">Itičče öjdöːguŋ bu͡ollar min iːkpin-haːkpɨn ɨraːstɨː iŋen-batan hɨtɨ͡a hu͡ok etiŋ. </ta>
            <ta e="T572" id="Seg_4991" s="T569">Kim ü͡öreter", diːr. </ta>
            <ta e="T579" id="Seg_4992" s="T572">"Kuhagan kɨːs ogoloːkpun, ol ü͡öreteːktiːr", diːr ogonnʼor. </ta>
            <ta e="T581" id="Seg_4993" s="T579">U͡ol ɨraːktaːgɨ: </ta>
            <ta e="T586" id="Seg_4994" s="T581">"Kɨːhɨŋ öjdöːk kɨːs töröːbüt", diːr. </ta>
            <ta e="T591" id="Seg_4995" s="T586">Timir kokoroːgu, tügege kajagastaːgɨ bi͡erer. </ta>
            <ta e="T595" id="Seg_4996" s="T591">"Manɨ hamaːn ɨːttɨn", diːr. </ta>
            <ta e="T601" id="Seg_4997" s="T595">"Onu hamaːbatagɨna, ikki͡enŋitin töbögütün bɨhɨ͡am", diːr. </ta>
            <ta e="T606" id="Seg_4998" s="T601">Ogonnʼor kokoroːgu ildʼe barbɨta ɨtɨː-ɨtɨː. </ta>
            <ta e="T607" id="Seg_4999" s="T606">Kɨːs: </ta>
            <ta e="T612" id="Seg_5000" s="T607">"Kaja, agaː, tu͡oguŋ kokoroːgoj", diːr. </ta>
            <ta e="T617" id="Seg_5001" s="T612">"Kaja-kuː, dʼe ölörör küne kelle. </ta>
            <ta e="T622" id="Seg_5002" s="T617">"Bu kokoroːgu hamaːn ɨːttɨn", diːr. </ta>
            <ta e="T628" id="Seg_5003" s="T622">"Bunu hamaːbatagɨna, ikki͡enŋitin töbögütün bɨhɨ͡am", diːr." </ta>
            <ta e="T630" id="Seg_5004" s="T628">Kɨːs diːr: </ta>
            <ta e="T642" id="Seg_5005" s="T630">"Ilt tötterü, dʼaktar kihi hamɨ͡am, er kihi kokoroːgu čorčokoːt kördük ti͡eren ɨːttɨn." </ta>
            <ta e="T645" id="Seg_5006" s="T642">Ogonnʼor ildʼen bi͡erer. </ta>
            <ta e="T648" id="Seg_5007" s="T645">Uːol ɨraːktaːgɨ högör. </ta>
            <ta e="T650" id="Seg_5008" s="T648">Ogonnʼoru ɨːtar. </ta>
            <ta e="T655" id="Seg_5009" s="T650">"Bejem kɨːskɨn körö barɨ͡am", diːr. </ta>
            <ta e="T662" id="Seg_5010" s="T655">U͡ol ɨraːktaːgɨ, kɨːhɨ köröːt, bagarar, dʼaktar ɨlar. </ta>
            <ta e="T665" id="Seg_5011" s="T662">Töhönü-kaččanɨ olorbuttara bu͡olla. </ta>
            <ta e="T676" id="Seg_5012" s="T665">Dʼaktardammɨtɨttan U͡ol ɨraːktaːgɨ kuːkula kurduk olorčok bu͡olla, barɨtɨn dʼaktara hubeliːr, bɨhaːrar. </ta>
            <ta e="T679" id="Seg_5013" s="T676">U͡ol ɨraːktaːgɨ tɨllanar: </ta>
            <ta e="T687" id="Seg_5014" s="T679">"Dogoː, bu biːr dojduga ikki ɨraːktaːgɨ bu͡olarbɨt tabɨllɨbat. </ta>
            <ta e="T689" id="Seg_5015" s="T687">Bihigi araksɨ͡ak." </ta>
            <ta e="T690" id="Seg_5016" s="T689">Dʼaktara: </ta>
            <ta e="T694" id="Seg_5017" s="T690">"Dʼe araksɨ͡ak duː", diːr. </ta>
            <ta e="T696" id="Seg_5018" s="T694">U͡ol ɨraːktaːgɨ: </ta>
            <ta e="T702" id="Seg_5019" s="T696">"Tu͡ok bagalaːk hirgin barɨtɨn ɨl", diːr. </ta>
            <ta e="T704" id="Seg_5020" s="T702">Dʼaktar höbüleher. </ta>
            <ta e="T707" id="Seg_5021" s="T704">Araksar tüːnnere-keː bu͡olar. </ta>
            <ta e="T710" id="Seg_5022" s="T707">Ere utujan kaːlar. </ta>
            <ta e="T720" id="Seg_5023" s="T710">Dʼaktar erin hu͡organnarɨ, tellekteri agatɨn bu͡or golomo dʼi͡etiger kötögön ildʼer. </ta>
            <ta e="T731" id="Seg_5024" s="T720">U͡ol ɨraːktaːgɨ harsi͡erda uhuktan hohujar, tɨːllan körbüte baha-ataga golomo mahɨgar kebiller. </ta>
            <ta e="T735" id="Seg_5025" s="T731">"Kaja-keː, bu kanna baːrbɨtɨj? </ta>
            <ta e="T737" id="Seg_5026" s="T735">Togo kelbitimij?" </ta>
            <ta e="T740" id="Seg_5027" s="T737">Dʼaktarɨn kebi͡eleːn uhugunnarar. </ta>
            <ta e="T741" id="Seg_5028" s="T740">Dʼaktara: </ta>
            <ta e="T746" id="Seg_5029" s="T741">"Kaja dogoː, min egelbitim", diːr. </ta>
            <ta e="T748" id="Seg_5030" s="T746">"Togo egelbikkinij?" </ta>
            <ta e="T754" id="Seg_5031" s="T748">"Tɨːj, bejegin bagardɨm da bejegin egellim. </ta>
            <ta e="T761" id="Seg_5032" s="T754">Tu͡ogu bagarargɨn ɨlaːr di͡ebitiŋ diː", diːr dʼaktar. </ta>
            <ta e="T765" id="Seg_5033" s="T761">U͡ol ɨraːktaːgɨ kɨ͡ajan arakpata. </ta>
            <ta e="T768" id="Seg_5034" s="T765">Dʼe dʼaktara diːr: </ta>
            <ta e="T773" id="Seg_5035" s="T768">"Oččo öjdöːk kihi bu͡olbatak ebikkin. </ta>
            <ta e="T777" id="Seg_5036" s="T773">Dʼoŋŋun hübeleːn bejeŋ olor. </ta>
            <ta e="T782" id="Seg_5037" s="T777">Min könnörü küseːjke bu͡olan oloru͡om." </ta>
            <ta e="T794" id="Seg_5038" s="T782">Umnahɨt paːsɨnaj kɨːhɨn ütü͡ötünen hi͡erkile taːs dʼi͡ege olordogo, bu͡or golomotton araksan uhuguttan. </ta>
            <ta e="T797" id="Seg_5039" s="T794">Bajan-tajan olordoktoro dʼe. </ta>
            <ta e="T798" id="Seg_5040" s="T797">Elete. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_5041" s="T0">dʼe</ta>
            <ta e="T2" id="Seg_5042" s="T1">u͡ol</ta>
            <ta e="T3" id="Seg_5043" s="T2">ɨraːktaːgɨ</ta>
            <ta e="T4" id="Seg_5044" s="T3">olor-but-a</ta>
            <ta e="T5" id="Seg_5045" s="T4">ulka-tɨ-gar</ta>
            <ta e="T6" id="Seg_5046" s="T5">umnahɨt</ta>
            <ta e="T7" id="Seg_5047" s="T6">paːsɨnaj-daːk</ta>
            <ta e="T8" id="Seg_5048" s="T7">umnahɨt</ta>
            <ta e="T9" id="Seg_5049" s="T8">paːsɨnaj</ta>
            <ta e="T10" id="Seg_5050" s="T9">bu</ta>
            <ta e="T11" id="Seg_5051" s="T10">ɨraːktaːgɨ-ga</ta>
            <ta e="T12" id="Seg_5052" s="T11">üleleː-n</ta>
            <ta e="T13" id="Seg_5053" s="T12">harsi͡erdaːŋŋɨ</ta>
            <ta e="T14" id="Seg_5054" s="T13">karaŋa-ttan</ta>
            <ta e="T15" id="Seg_5055" s="T14">ki͡ehe</ta>
            <ta e="T16" id="Seg_5056" s="T15">karaŋa-ga</ta>
            <ta e="T17" id="Seg_5057" s="T16">di͡eri</ta>
            <ta e="T18" id="Seg_5058" s="T17">biːr</ta>
            <ta e="T19" id="Seg_5059" s="T18">lu͡osku</ta>
            <ta e="T20" id="Seg_5060" s="T19">burduk</ta>
            <ta e="T21" id="Seg_5061" s="T20">ihin</ta>
            <ta e="T22" id="Seg_5062" s="T21">üleliː-r</ta>
            <ta e="T23" id="Seg_5063" s="T22">e-bit</ta>
            <ta e="T24" id="Seg_5064" s="T23">ma-nan</ta>
            <ta e="T25" id="Seg_5065" s="T24">iːt-t-en</ta>
            <ta e="T26" id="Seg_5066" s="T25">olor-ol-lor</ta>
            <ta e="T27" id="Seg_5067" s="T26">umnahɨt</ta>
            <ta e="T28" id="Seg_5068" s="T27">paːsɨnaj</ta>
            <ta e="T29" id="Seg_5069" s="T28">biːr</ta>
            <ta e="T30" id="Seg_5070" s="T29">kɨːs</ta>
            <ta e="T31" id="Seg_5071" s="T30">ogo-loːk</ta>
            <ta e="T32" id="Seg_5072" s="T31">dʼe</ta>
            <ta e="T33" id="Seg_5073" s="T32">biːrde</ta>
            <ta e="T34" id="Seg_5074" s="T33">u͡ol</ta>
            <ta e="T35" id="Seg_5075" s="T34">ɨraːktaːgɨ</ta>
            <ta e="T36" id="Seg_5076" s="T35">diː-r</ta>
            <ta e="T37" id="Seg_5077" s="T36">kaja</ta>
            <ta e="T38" id="Seg_5078" s="T37">dʼe</ta>
            <ta e="T39" id="Seg_5079" s="T38">umnahɨt</ta>
            <ta e="T40" id="Seg_5080" s="T39">paːsɨnaj</ta>
            <ta e="T41" id="Seg_5081" s="T40">töhö</ta>
            <ta e="T42" id="Seg_5082" s="T41">öj-döːk</ta>
            <ta e="T43" id="Seg_5083" s="T42">kihi</ta>
            <ta e="T44" id="Seg_5084" s="T43">töröː-n</ta>
            <ta e="T45" id="Seg_5085" s="T44">tönün-nü-ŋ</ta>
            <ta e="T46" id="Seg_5086" s="T45">taːburun-na</ta>
            <ta e="T47" id="Seg_5087" s="T46">taːj</ta>
            <ta e="T48" id="Seg_5088" s="T47">minnʼiges-ten</ta>
            <ta e="T49" id="Seg_5089" s="T48">minnʼiges</ta>
            <ta e="T50" id="Seg_5090" s="T49">tu͡og=uj</ta>
            <ta e="T51" id="Seg_5091" s="T50">harsi͡erda</ta>
            <ta e="T52" id="Seg_5092" s="T51">kel-en</ta>
            <ta e="T53" id="Seg_5093" s="T52">togus</ta>
            <ta e="T54" id="Seg_5094" s="T53">čaːs-ka</ta>
            <ta e="T55" id="Seg_5095" s="T54">haŋar-aːr</ta>
            <ta e="T56" id="Seg_5096" s="T55">o-nu</ta>
            <ta e="T57" id="Seg_5097" s="T56">taːj-ba-tak-kɨna</ta>
            <ta e="T58" id="Seg_5098" s="T57">ɨjɨː-r</ta>
            <ta e="T59" id="Seg_5099" s="T58">mas-pa-r</ta>
            <ta e="T60" id="Seg_5100" s="T59">moːj-doːk</ta>
            <ta e="T61" id="Seg_5101" s="T60">bas-kɨ-n</ta>
            <ta e="T62" id="Seg_5102" s="T61">bɨh-ɨ͡a-m</ta>
            <ta e="T63" id="Seg_5103" s="T62">umnahɨt</ta>
            <ta e="T64" id="Seg_5104" s="T63">paːsɨnaj</ta>
            <ta e="T65" id="Seg_5105" s="T64">togo</ta>
            <ta e="T66" id="Seg_5106" s="T65">da</ta>
            <ta e="T67" id="Seg_5107" s="T66">öj-ö</ta>
            <ta e="T68" id="Seg_5108" s="T67">kɨ͡aj-bat</ta>
            <ta e="T69" id="Seg_5109" s="T68">minnʼiges</ta>
            <ta e="T70" id="Seg_5110" s="T69">ah-ɨ</ta>
            <ta e="T71" id="Seg_5111" s="T70">bil-eːkteː-bet</ta>
            <ta e="T72" id="Seg_5112" s="T71">ɨraːktaːgɨ</ta>
            <ta e="T73" id="Seg_5113" s="T72">ah-a</ta>
            <ta e="T74" id="Seg_5114" s="T73">barɨ-ta</ta>
            <ta e="T75" id="Seg_5115" s="T74">minnʼiges</ta>
            <ta e="T76" id="Seg_5116" s="T75">kurduk</ta>
            <ta e="T77" id="Seg_5117" s="T76">gini͡e-ke</ta>
            <ta e="T78" id="Seg_5118" s="T77">dʼi͡e-ti-ger</ta>
            <ta e="T79" id="Seg_5119" s="T78">kel-en</ta>
            <ta e="T80" id="Seg_5120" s="T79">ɨt-a-n-a</ta>
            <ta e="T81" id="Seg_5121" s="T80">olor-d-o</ta>
            <ta e="T82" id="Seg_5122" s="T81">kɨːh-a</ta>
            <ta e="T83" id="Seg_5123" s="T82">ɨjɨt-ar</ta>
            <ta e="T84" id="Seg_5124" s="T83">kaja</ta>
            <ta e="T85" id="Seg_5125" s="T84">agaː</ta>
            <ta e="T86" id="Seg_5126" s="T85">togo</ta>
            <ta e="T87" id="Seg_5127" s="T86">ɨtaː-tɨ-n</ta>
            <ta e="T88" id="Seg_5128" s="T87">dʼe</ta>
            <ta e="T89" id="Seg_5129" s="T88">togoj-u-m</ta>
            <ta e="T90" id="Seg_5130" s="T89">hɨldʼ-ɨ-bɨt-ɨ-m</ta>
            <ta e="T91" id="Seg_5131" s="T90">hɨččak</ta>
            <ta e="T92" id="Seg_5132" s="T91">olor-but-u-m</ta>
            <ta e="T93" id="Seg_5133" s="T92">uhug-a</ta>
            <ta e="T94" id="Seg_5134" s="T93">u͡ol</ta>
            <ta e="T95" id="Seg_5135" s="T94">ɨraːktaːgɨ</ta>
            <ta e="T96" id="Seg_5136" s="T95">taːburuːn</ta>
            <ta e="T97" id="Seg_5137" s="T96">bi͡er-d-e</ta>
            <ta e="T98" id="Seg_5138" s="T97">taːj-ba-tak-pɨna</ta>
            <ta e="T99" id="Seg_5139" s="T98">moːj-doːk</ta>
            <ta e="T100" id="Seg_5140" s="T99">bas-pɨ-n</ta>
            <ta e="T101" id="Seg_5141" s="T100">bɨh-ɨ͡ak</ta>
            <ta e="T102" id="Seg_5142" s="T101">bu͡ol-l-a</ta>
            <ta e="T103" id="Seg_5143" s="T102">tug-u</ta>
            <ta e="T104" id="Seg_5144" s="T103">taː-t-tar-ar</ta>
            <ta e="T105" id="Seg_5145" s="T104">ol</ta>
            <ta e="T106" id="Seg_5146" s="T105">minnʼiges-ten</ta>
            <ta e="T107" id="Seg_5147" s="T106">minnʼiges</ta>
            <ta e="T108" id="Seg_5148" s="T107">tu͡og=uj</ta>
            <ta e="T109" id="Seg_5149" s="T108">diː-r</ta>
            <ta e="T110" id="Seg_5150" s="T109">di͡e-bit</ta>
            <ta e="T111" id="Seg_5151" s="T110">eː</ta>
            <ta e="T112" id="Seg_5152" s="T111">agaː</ta>
            <ta e="T113" id="Seg_5153" s="T112">tuha-ta</ta>
            <ta e="T114" id="Seg_5154" s="T113">hu͡ok-ka</ta>
            <ta e="T115" id="Seg_5155" s="T114">togo</ta>
            <ta e="T116" id="Seg_5156" s="T115">ɨt-ɨː-gɨn</ta>
            <ta e="T117" id="Seg_5157" s="T116">utuj-aːktaː</ta>
            <ta e="T118" id="Seg_5158" s="T117">hɨnnʼan</ta>
            <ta e="T119" id="Seg_5159" s="T118">minnʼiges-ten</ta>
            <ta e="T120" id="Seg_5160" s="T119">minnʼigeh-i</ta>
            <ta e="T121" id="Seg_5161" s="T120">bil-er</ta>
            <ta e="T122" id="Seg_5162" s="T121">hir-i-m</ta>
            <ta e="T123" id="Seg_5163" s="T122">biːr</ta>
            <ta e="T124" id="Seg_5164" s="T123">di͡e-r</ta>
            <ta e="T125" id="Seg_5165" s="T124">harsi͡erdaːŋŋɨ</ta>
            <ta e="T126" id="Seg_5166" s="T125">karaŋa-ttan</ta>
            <ta e="T127" id="Seg_5167" s="T126">ki͡eheːŋŋi</ta>
            <ta e="T128" id="Seg_5168" s="T127">karaŋa-ga</ta>
            <ta e="T129" id="Seg_5169" s="T128">di͡eri</ta>
            <ta e="T130" id="Seg_5170" s="T129">üleleː-n</ta>
            <ta e="T131" id="Seg_5171" s="T130">en</ta>
            <ta e="T132" id="Seg_5172" s="T131">haːk-kɨ-n-kir-gi-n</ta>
            <ta e="T133" id="Seg_5173" s="T132">ɨraːstaː-n</ta>
            <ta e="T134" id="Seg_5174" s="T133">biːr</ta>
            <ta e="T135" id="Seg_5175" s="T134">lu͡osku</ta>
            <ta e="T136" id="Seg_5176" s="T135">burdug-u</ta>
            <ta e="T137" id="Seg_5177" s="T136">il-tek-pine</ta>
            <ta e="T138" id="Seg_5178" s="T137">emeːksin-i-m</ta>
            <ta e="T139" id="Seg_5179" s="T138">bu͡or</ta>
            <ta e="T140" id="Seg_5180" s="T139">golomo</ta>
            <ta e="T141" id="Seg_5181" s="T140">dʼi͡e-ti-n</ta>
            <ta e="T142" id="Seg_5182" s="T141">otun-nag-ɨna</ta>
            <ta e="T143" id="Seg_5183" s="T142">munnu-m</ta>
            <ta e="T144" id="Seg_5184" s="T143">hɨlɨj-ar</ta>
            <ta e="T145" id="Seg_5185" s="T144">ol</ta>
            <ta e="T146" id="Seg_5186" s="T145">burduk-kaːn-ɨ-nan</ta>
            <ta e="T147" id="Seg_5187" s="T146">toloŋku͡o</ta>
            <ta e="T148" id="Seg_5188" s="T147">oŋor-dog-una</ta>
            <ta e="T149" id="Seg_5189" s="T148">ahaː-bakka</ta>
            <ta e="T150" id="Seg_5190" s="T149">da</ta>
            <ta e="T151" id="Seg_5191" s="T150">utuj-a-bɨn</ta>
            <ta e="T152" id="Seg_5192" s="T151">onon</ta>
            <ta e="T153" id="Seg_5193" s="T152">minnʼiges-ten</ta>
            <ta e="T154" id="Seg_5194" s="T153">minnʼiges</ta>
            <ta e="T155" id="Seg_5195" s="T154">utuj-ar</ta>
            <ta e="T156" id="Seg_5196" s="T155">uː</ta>
            <ta e="T157" id="Seg_5197" s="T156">bu͡ol-u͡ok-taːk</ta>
            <ta e="T158" id="Seg_5198" s="T157">di͡e-r</ta>
            <ta e="T159" id="Seg_5199" s="T158">kini</ta>
            <ta e="T160" id="Seg_5200" s="T159">on-ton</ta>
            <ta e="T161" id="Seg_5201" s="T160">da</ta>
            <ta e="T162" id="Seg_5202" s="T161">ordug-u</ta>
            <ta e="T163" id="Seg_5203" s="T162">bil-er-e</ta>
            <ta e="T164" id="Seg_5204" s="T163">bu͡ol-u͡o</ta>
            <ta e="T165" id="Seg_5205" s="T164">du͡o</ta>
            <ta e="T166" id="Seg_5206" s="T165">di͡e-bit</ta>
            <ta e="T167" id="Seg_5207" s="T166">kɨːh-a</ta>
            <ta e="T168" id="Seg_5208" s="T167">ogonnʼor</ta>
            <ta e="T169" id="Seg_5209" s="T168">utuj-an</ta>
            <ta e="T170" id="Seg_5210" s="T169">kaːl-l-a</ta>
            <ta e="T171" id="Seg_5211" s="T170">harsi͡erda</ta>
            <ta e="T172" id="Seg_5212" s="T171">erde</ta>
            <ta e="T173" id="Seg_5213" s="T172">tur-an</ta>
            <ta e="T174" id="Seg_5214" s="T173">togus</ta>
            <ta e="T175" id="Seg_5215" s="T174">čaːs-ka</ta>
            <ta e="T176" id="Seg_5216" s="T175">ɨraːktɨːgɨ-ga</ta>
            <ta e="T177" id="Seg_5217" s="T176">tij-er</ta>
            <ta e="T178" id="Seg_5218" s="T177">u͡ol</ta>
            <ta e="T179" id="Seg_5219" s="T178">ɨraːktaːgɨ</ta>
            <ta e="T180" id="Seg_5220" s="T179">ɨjɨt-ar</ta>
            <ta e="T181" id="Seg_5221" s="T180">kaja</ta>
            <ta e="T182" id="Seg_5222" s="T181">taːj-dɨ-ŋ</ta>
            <ta e="T183" id="Seg_5223" s="T182">du͡o</ta>
            <ta e="T184" id="Seg_5224" s="T183">eː</ta>
            <ta e="T185" id="Seg_5225" s="T184">taːj-ar-ga</ta>
            <ta e="T186" id="Seg_5226" s="T185">talɨ</ta>
            <ta e="T187" id="Seg_5227" s="T186">gɨn-nɨ-m</ta>
            <ta e="T188" id="Seg_5228" s="T187">höbö</ta>
            <ta e="T189" id="Seg_5229" s="T188">duː</ta>
            <ta e="T190" id="Seg_5230" s="T189">hɨːha-ta</ta>
            <ta e="T191" id="Seg_5231" s="T190">duː</ta>
            <ta e="T192" id="Seg_5232" s="T191">diː-r</ta>
            <ta e="T193" id="Seg_5233" s="T192">dʼe</ta>
            <ta e="T194" id="Seg_5234" s="T193">tug-u=j</ta>
            <ta e="T195" id="Seg_5235" s="T194">harsi͡erdaːŋŋɨ</ta>
            <ta e="T196" id="Seg_5236" s="T195">karaŋa-ttan</ta>
            <ta e="T197" id="Seg_5237" s="T196">ki͡eheːŋŋi</ta>
            <ta e="T198" id="Seg_5238" s="T197">karaŋa-ga</ta>
            <ta e="T199" id="Seg_5239" s="T198">di͡eri</ta>
            <ta e="T200" id="Seg_5240" s="T199">üleleː-n</ta>
            <ta e="T201" id="Seg_5241" s="T200">en</ta>
            <ta e="T202" id="Seg_5242" s="T201">haːk-kɨ-n-kir-gi-n</ta>
            <ta e="T203" id="Seg_5243" s="T202">ɨraːstaː-n</ta>
            <ta e="T204" id="Seg_5244" s="T203">biːr</ta>
            <ta e="T205" id="Seg_5245" s="T204">lu͡osku</ta>
            <ta e="T206" id="Seg_5246" s="T205">burdug-u</ta>
            <ta e="T207" id="Seg_5247" s="T206">il-tek-pine</ta>
            <ta e="T208" id="Seg_5248" s="T207">emeːksin-i-m</ta>
            <ta e="T209" id="Seg_5249" s="T208">bu͡or</ta>
            <ta e="T210" id="Seg_5250" s="T209">golomo</ta>
            <ta e="T211" id="Seg_5251" s="T210">dʼi͡e-ti-n</ta>
            <ta e="T212" id="Seg_5252" s="T211">otun-nag-ɨna</ta>
            <ta e="T213" id="Seg_5253" s="T212">munnu-m</ta>
            <ta e="T214" id="Seg_5254" s="T213">hɨlɨj-ar</ta>
            <ta e="T215" id="Seg_5255" s="T214">ol</ta>
            <ta e="T216" id="Seg_5256" s="T215">burduk-kaːn-ɨ-nan</ta>
            <ta e="T217" id="Seg_5257" s="T216">toloŋku͡o</ta>
            <ta e="T218" id="Seg_5258" s="T217">oŋor-dog-una</ta>
            <ta e="T219" id="Seg_5259" s="T218">ahaː-bakka</ta>
            <ta e="T220" id="Seg_5260" s="T219">da</ta>
            <ta e="T221" id="Seg_5261" s="T220">utuj-a-bɨn</ta>
            <ta e="T222" id="Seg_5262" s="T221">onon</ta>
            <ta e="T223" id="Seg_5263" s="T222">minnʼiges-ten</ta>
            <ta e="T224" id="Seg_5264" s="T223">minnʼiges</ta>
            <ta e="T225" id="Seg_5265" s="T224">utuj-ar</ta>
            <ta e="T226" id="Seg_5266" s="T225">uː</ta>
            <ta e="T227" id="Seg_5267" s="T226">bu͡ol-u͡ok-taːk</ta>
            <ta e="T228" id="Seg_5268" s="T227">u͡ol</ta>
            <ta e="T229" id="Seg_5269" s="T228">ɨraːktaːgɨ</ta>
            <ta e="T230" id="Seg_5270" s="T229">diː-r</ta>
            <ta e="T231" id="Seg_5271" s="T230">oː</ta>
            <ta e="T232" id="Seg_5272" s="T231">dʼe</ta>
            <ta e="T233" id="Seg_5273" s="T232">öj-döːk</ta>
            <ta e="T234" id="Seg_5274" s="T233">kihi-gin</ta>
            <ta e="T235" id="Seg_5275" s="T234">e-bit</ta>
            <ta e="T236" id="Seg_5276" s="T235">kajdak</ta>
            <ta e="T237" id="Seg_5277" s="T236">in-en-bat-an</ta>
            <ta e="T238" id="Seg_5278" s="T237">hɨt-a-gɨn</ta>
            <ta e="T239" id="Seg_5279" s="T238">össü͡ö</ta>
            <ta e="T240" id="Seg_5280" s="T239">taːj</ta>
            <ta e="T241" id="Seg_5281" s="T240">orto</ta>
            <ta e="T242" id="Seg_5282" s="T241">dojdu-ga</ta>
            <ta e="T243" id="Seg_5283" s="T242">türgen-ten</ta>
            <ta e="T244" id="Seg_5284" s="T243">türgen</ta>
            <ta e="T245" id="Seg_5285" s="T244">tu͡ok</ta>
            <ta e="T246" id="Seg_5286" s="T245">baːr=ɨj</ta>
            <ta e="T247" id="Seg_5287" s="T246">harsi͡erda</ta>
            <ta e="T248" id="Seg_5288" s="T247">taːj-ba-tak-kɨna</ta>
            <ta e="T249" id="Seg_5289" s="T248">moːj-doːk</ta>
            <ta e="T250" id="Seg_5290" s="T249">bas-kɨ-n</ta>
            <ta e="T251" id="Seg_5291" s="T250">bɨh-a-bɨn</ta>
            <ta e="T252" id="Seg_5292" s="T251">ogonnʼor</ta>
            <ta e="T253" id="Seg_5293" s="T252">tug-u</ta>
            <ta e="T254" id="Seg_5294" s="T253">da</ta>
            <ta e="T255" id="Seg_5295" s="T254">tobul-bat</ta>
            <ta e="T256" id="Seg_5296" s="T255">tij-en</ta>
            <ta e="T257" id="Seg_5297" s="T256">ɨt-a-n-a</ta>
            <ta e="T258" id="Seg_5298" s="T257">olor-dog-una</ta>
            <ta e="T259" id="Seg_5299" s="T258">kɨːh-a</ta>
            <ta e="T260" id="Seg_5300" s="T259">ɨjɨt-ar</ta>
            <ta e="T261" id="Seg_5301" s="T260">ogonnʼor</ta>
            <ta e="T262" id="Seg_5302" s="T261">innʼe-innʼe</ta>
            <ta e="T263" id="Seg_5303" s="T262">diː-r</ta>
            <ta e="T264" id="Seg_5304" s="T263">tu͡ok</ta>
            <ta e="T265" id="Seg_5305" s="T264">tuha-ta</ta>
            <ta e="T266" id="Seg_5306" s="T265">hu͡ok-ka</ta>
            <ta e="T267" id="Seg_5307" s="T266">ɨt-ɨː-gɨn</ta>
            <ta e="T268" id="Seg_5308" s="T267">utuj</ta>
            <ta e="T269" id="Seg_5309" s="T268">harsi͡erda</ta>
            <ta e="T270" id="Seg_5310" s="T269">et-eːr</ta>
            <ta e="T271" id="Seg_5311" s="T270">türgen-ten</ta>
            <ta e="T272" id="Seg_5312" s="T271">türgen</ta>
            <ta e="T273" id="Seg_5313" s="T272">di͡e-r</ta>
            <ta e="T274" id="Seg_5314" s="T273">kuhagan</ta>
            <ta e="T275" id="Seg_5315" s="T274">buru͡o-laːk</ta>
            <ta e="T276" id="Seg_5316" s="T275">bu͡or</ta>
            <ta e="T277" id="Seg_5317" s="T276">golomo-bu-ttan</ta>
            <ta e="T278" id="Seg_5318" s="T277">taks-a</ta>
            <ta e="T279" id="Seg_5319" s="T278">köt-ön</ta>
            <ta e="T280" id="Seg_5320" s="T279">karak-pɨ-n</ta>
            <ta e="T281" id="Seg_5321" s="T280">kas-t-ɨ-r-ɨ-t-a</ta>
            <ta e="T282" id="Seg_5322" s="T281">hot-on</ta>
            <ta e="T283" id="Seg_5323" s="T282">oduːlaː-n</ta>
            <ta e="T284" id="Seg_5324" s="T283">kör-dök-püne</ta>
            <ta e="T285" id="Seg_5325" s="T284">kallaːn</ta>
            <ta e="T286" id="Seg_5326" s="T285">di͡ek</ta>
            <ta e="T287" id="Seg_5327" s="T286">kantaj-dak-pɨna</ta>
            <ta e="T288" id="Seg_5328" s="T287">kallaːn</ta>
            <ta e="T289" id="Seg_5329" s="T288">huluh-u-n</ta>
            <ta e="T290" id="Seg_5330" s="T289">karag-ɨ-m</ta>
            <ta e="T291" id="Seg_5331" s="T290">aːga</ta>
            <ta e="T292" id="Seg_5332" s="T291">kör-ör</ta>
            <ta e="T293" id="Seg_5333" s="T292">hir</ta>
            <ta e="T294" id="Seg_5334" s="T293">di͡ek</ta>
            <ta e="T295" id="Seg_5335" s="T294">kör-dök-püne</ta>
            <ta e="T296" id="Seg_5336" s="T295">hir</ta>
            <ta e="T297" id="Seg_5337" s="T296">ot-u-n-mah-ɨ-n</ta>
            <ta e="T298" id="Seg_5338" s="T297">aːga</ta>
            <ta e="T299" id="Seg_5339" s="T298">kör-ö-bün</ta>
            <ta e="T300" id="Seg_5340" s="T299">karag-ɨ-m</ta>
            <ta e="T301" id="Seg_5341" s="T300">ɨl-ar-ɨ-gar</ta>
            <ta e="T302" id="Seg_5342" s="T301">kahan</ta>
            <ta e="T303" id="Seg_5343" s="T302">da</ta>
            <ta e="T304" id="Seg_5344" s="T303">tiːj-bep-pin</ta>
            <ta e="T305" id="Seg_5345" s="T304">onon</ta>
            <ta e="T306" id="Seg_5346" s="T305">türgen-ten</ta>
            <ta e="T307" id="Seg_5347" s="T306">türgen</ta>
            <ta e="T308" id="Seg_5348" s="T307">kihi</ta>
            <ta e="T309" id="Seg_5349" s="T308">kör-üː-te</ta>
            <ta e="T310" id="Seg_5350" s="T309">di͡e-r</ta>
            <ta e="T311" id="Seg_5351" s="T310">on-ton</ta>
            <ta e="T312" id="Seg_5352" s="T311">ordug-u</ta>
            <ta e="T313" id="Seg_5353" s="T312">kini</ta>
            <ta e="T314" id="Seg_5354" s="T313">bil-er-e</ta>
            <ta e="T315" id="Seg_5355" s="T314">bu͡ol-u͡o</ta>
            <ta e="T316" id="Seg_5356" s="T315">du͡o</ta>
            <ta e="T317" id="Seg_5357" s="T316">diː-r</ta>
            <ta e="T318" id="Seg_5358" s="T317">kɨːs</ta>
            <ta e="T319" id="Seg_5359" s="T318">ogonnʼor</ta>
            <ta e="T320" id="Seg_5360" s="T319">utuj-an</ta>
            <ta e="T321" id="Seg_5361" s="T320">kaːl-l-a</ta>
            <ta e="T322" id="Seg_5362" s="T321">harsi͡erde</ta>
            <ta e="T323" id="Seg_5363" s="T322">tur-an</ta>
            <ta e="T324" id="Seg_5364" s="T323">ɨraːktaːgɨ-tɨ-gar</ta>
            <ta e="T325" id="Seg_5365" s="T324">bar-dag-a</ta>
            <ta e="T326" id="Seg_5366" s="T325">u͡ol</ta>
            <ta e="T327" id="Seg_5367" s="T326">ɨraːktaːgɨ</ta>
            <ta e="T328" id="Seg_5368" s="T327">ɨjɨt-ar</ta>
            <ta e="T329" id="Seg_5369" s="T328">kaja</ta>
            <ta e="T330" id="Seg_5370" s="T329">taːj-dɨ-ŋ</ta>
            <ta e="T331" id="Seg_5371" s="T330">du͡o</ta>
            <ta e="T332" id="Seg_5372" s="T331">eː</ta>
            <ta e="T333" id="Seg_5373" s="T332">taːj-ar-ga</ta>
            <ta e="T334" id="Seg_5374" s="T333">talɨ</ta>
            <ta e="T335" id="Seg_5375" s="T334">gɨn-nɨ-m</ta>
            <ta e="T336" id="Seg_5376" s="T335">höbö</ta>
            <ta e="T337" id="Seg_5377" s="T336">duː</ta>
            <ta e="T338" id="Seg_5378" s="T337">hɨːha-ta</ta>
            <ta e="T339" id="Seg_5379" s="T338">duː</ta>
            <ta e="T340" id="Seg_5380" s="T339">diː-r</ta>
            <ta e="T341" id="Seg_5381" s="T340">kuhagan</ta>
            <ta e="T342" id="Seg_5382" s="T341">buru͡o-laːk</ta>
            <ta e="T343" id="Seg_5383" s="T342">bu͡or</ta>
            <ta e="T344" id="Seg_5384" s="T343">golomo-bu-ttan</ta>
            <ta e="T345" id="Seg_5385" s="T344">taks-a</ta>
            <ta e="T346" id="Seg_5386" s="T345">köt-ön</ta>
            <ta e="T347" id="Seg_5387" s="T346">karak-pɨ-n</ta>
            <ta e="T348" id="Seg_5388" s="T347">kas-t-ɨ-r-ɨ-t-a</ta>
            <ta e="T349" id="Seg_5389" s="T348">hot-on</ta>
            <ta e="T350" id="Seg_5390" s="T349">oduː-lan-an</ta>
            <ta e="T351" id="Seg_5391" s="T350">kör-dök-püne</ta>
            <ta e="T352" id="Seg_5392" s="T351">kallaːn</ta>
            <ta e="T353" id="Seg_5393" s="T352">di͡ek</ta>
            <ta e="T354" id="Seg_5394" s="T353">kantaj-dak-pɨna</ta>
            <ta e="T355" id="Seg_5395" s="T354">kallaːn</ta>
            <ta e="T356" id="Seg_5396" s="T355">huluh-u-n</ta>
            <ta e="T357" id="Seg_5397" s="T356">karag-ɨ-m</ta>
            <ta e="T358" id="Seg_5398" s="T357">aːga</ta>
            <ta e="T359" id="Seg_5399" s="T358">kör-ör</ta>
            <ta e="T360" id="Seg_5400" s="T359">hir</ta>
            <ta e="T361" id="Seg_5401" s="T360">di͡ek</ta>
            <ta e="T362" id="Seg_5402" s="T361">kör-dök-püne</ta>
            <ta e="T363" id="Seg_5403" s="T362">hir</ta>
            <ta e="T364" id="Seg_5404" s="T363">ot-u-n-mah-ɨ-n</ta>
            <ta e="T365" id="Seg_5405" s="T364">aːga</ta>
            <ta e="T366" id="Seg_5406" s="T365">kör-ö-bün</ta>
            <ta e="T367" id="Seg_5407" s="T366">karag-ɨ-m</ta>
            <ta e="T368" id="Seg_5408" s="T367">ɨl-ar-ɨ-gar</ta>
            <ta e="T369" id="Seg_5409" s="T368">kahan</ta>
            <ta e="T370" id="Seg_5410" s="T369">da</ta>
            <ta e="T371" id="Seg_5411" s="T370">tij-bep-pin</ta>
            <ta e="T372" id="Seg_5412" s="T371">onon</ta>
            <ta e="T373" id="Seg_5413" s="T372">türgen-ten</ta>
            <ta e="T374" id="Seg_5414" s="T373">türgen</ta>
            <ta e="T375" id="Seg_5415" s="T374">kihi</ta>
            <ta e="T376" id="Seg_5416" s="T375">kör-üː-te</ta>
            <ta e="T377" id="Seg_5417" s="T376">e-bit</ta>
            <ta e="T378" id="Seg_5418" s="T377">diː-r</ta>
            <ta e="T379" id="Seg_5419" s="T378">u͡ol</ta>
            <ta e="T380" id="Seg_5420" s="T379">ɨraːktaːgɨ</ta>
            <ta e="T381" id="Seg_5421" s="T380">hög-ör</ta>
            <ta e="T382" id="Seg_5422" s="T381">oː</ta>
            <ta e="T383" id="Seg_5423" s="T382">ogonnʼor</ta>
            <ta e="T384" id="Seg_5424" s="T383">dʼe</ta>
            <ta e="T385" id="Seg_5425" s="T384">öj-döːk</ta>
            <ta e="T386" id="Seg_5426" s="T385">kihi-gin</ta>
            <ta e="T387" id="Seg_5427" s="T386">e-bit</ta>
            <ta e="T388" id="Seg_5428" s="T387">dʼe</ta>
            <ta e="T389" id="Seg_5429" s="T388">tu͡ok</ta>
            <ta e="T390" id="Seg_5430" s="T389">da</ta>
            <ta e="T391" id="Seg_5431" s="T390">üs</ta>
            <ta e="T392" id="Seg_5432" s="T391">tögül-leːk</ta>
            <ta e="T393" id="Seg_5433" s="T392">taks-an</ta>
            <ta e="T394" id="Seg_5434" s="T393">irgek</ta>
            <ta e="T395" id="Seg_5435" s="T394">koru͡oba-nɨ</ta>
            <ta e="T396" id="Seg_5436" s="T395">tut-an</ta>
            <ta e="T397" id="Seg_5437" s="T396">bi͡er-er</ta>
            <ta e="T398" id="Seg_5438" s="T397">ma-nɨ</ta>
            <ta e="T399" id="Seg_5439" s="T398">harsi͡erda</ta>
            <ta e="T400" id="Seg_5440" s="T399">tör-ö-t-ön</ta>
            <ta e="T401" id="Seg_5441" s="T400">tugut-u-n</ta>
            <ta e="T402" id="Seg_5442" s="T401">batɨh-ɨ-n-nar-an</ta>
            <ta e="T403" id="Seg_5443" s="T402">egel-eːr</ta>
            <ta e="T404" id="Seg_5444" s="T403">kaja</ta>
            <ta e="T405" id="Seg_5445" s="T404">u͡orukta-tɨ-n</ta>
            <ta e="T406" id="Seg_5446" s="T405">komuj-an</ta>
            <ta e="T407" id="Seg_5447" s="T406">egel-eːje-gin</ta>
            <ta e="T408" id="Seg_5448" s="T407">di͡e-bit</ta>
            <ta e="T409" id="Seg_5449" s="T408">u͡ol</ta>
            <ta e="T410" id="Seg_5450" s="T409">ɨraːktaːgɨ</ta>
            <ta e="T411" id="Seg_5451" s="T410">ogonnʼor</ta>
            <ta e="T412" id="Seg_5452" s="T411">erej-deːk</ta>
            <ta e="T413" id="Seg_5453" s="T412">irgek</ta>
            <ta e="T414" id="Seg_5454" s="T413">koru͡oba-tɨ-n</ta>
            <ta e="T415" id="Seg_5455" s="T414">hi͡et-en</ta>
            <ta e="T416" id="Seg_5456" s="T415">tij-en</ta>
            <ta e="T417" id="Seg_5457" s="T416">ɨt-ɨː</ta>
            <ta e="T418" id="Seg_5458" s="T417">olor-d-o</ta>
            <ta e="T419" id="Seg_5459" s="T418">kɨːh-a</ta>
            <ta e="T420" id="Seg_5460" s="T419">ɨjɨt-t-a</ta>
            <ta e="T421" id="Seg_5461" s="T420">kaja</ta>
            <ta e="T422" id="Seg_5462" s="T421">agaː</ta>
            <ta e="T423" id="Seg_5463" s="T422">togo</ta>
            <ta e="T424" id="Seg_5464" s="T423">ɨtaː-tɨ-ŋ</ta>
            <ta e="T425" id="Seg_5465" s="T424">kaja</ta>
            <ta e="T426" id="Seg_5466" s="T425">irgek</ta>
            <ta e="T427" id="Seg_5467" s="T426">koru͡oba-nɨ</ta>
            <ta e="T428" id="Seg_5468" s="T427">bi͡er-d-e</ta>
            <ta e="T429" id="Seg_5469" s="T428">tör-ö-t-ön</ta>
            <ta e="T430" id="Seg_5470" s="T429">egel</ta>
            <ta e="T431" id="Seg_5471" s="T430">diː-r</ta>
            <ta e="T432" id="Seg_5472" s="T431">irgek</ta>
            <ta e="T433" id="Seg_5473" s="T432">koru͡oba</ta>
            <ta e="T434" id="Seg_5474" s="T433">kantɨnan</ta>
            <ta e="T435" id="Seg_5475" s="T434">töröː-ččü-nü=j</ta>
            <ta e="T436" id="Seg_5476" s="T435">kuːgu</ta>
            <ta e="T437" id="Seg_5477" s="T436">eː</ta>
            <ta e="T438" id="Seg_5478" s="T437">irgek</ta>
            <ta e="T439" id="Seg_5479" s="T438">koru͡oba</ta>
            <ta e="T440" id="Seg_5480" s="T439">kantan</ta>
            <ta e="T441" id="Seg_5481" s="T440">tör-ü͡ö=j</ta>
            <ta e="T442" id="Seg_5482" s="T441">kata</ta>
            <ta e="T443" id="Seg_5483" s="T442">as-tam-mɨp-pɨt</ta>
            <ta e="T444" id="Seg_5484" s="T443">taks-aŋ-ŋɨn</ta>
            <ta e="T445" id="Seg_5485" s="T444">ölör-böktöː</ta>
            <ta e="T446" id="Seg_5486" s="T445">et-i-n</ta>
            <ta e="T447" id="Seg_5487" s="T446">h-i͡ek-pit</ta>
            <ta e="T448" id="Seg_5488" s="T447">diː-r</ta>
            <ta e="T449" id="Seg_5489" s="T448">kɨːs</ta>
            <ta e="T450" id="Seg_5490" s="T449">ogonnʼor</ta>
            <ta e="T451" id="Seg_5491" s="T450">irgek</ta>
            <ta e="T452" id="Seg_5492" s="T451">koru͡oba-tɨ-n</ta>
            <ta e="T453" id="Seg_5493" s="T452">ölör-ön</ta>
            <ta e="T454" id="Seg_5494" s="T453">keːh-er</ta>
            <ta e="T455" id="Seg_5495" s="T454">dʼe</ta>
            <ta e="T456" id="Seg_5496" s="T455">ma-nɨ</ta>
            <ta e="T457" id="Seg_5497" s="T456">h-i͡ek-tere</ta>
            <ta e="T458" id="Seg_5498" s="T457">kɨːh-a</ta>
            <ta e="T459" id="Seg_5499" s="T458">utakaː-ga</ta>
            <ta e="T460" id="Seg_5500" s="T459">koru͡oba</ta>
            <ta e="T461" id="Seg_5501" s="T460">tunnʼag-ɨ-n</ta>
            <ta e="T462" id="Seg_5502" s="T461">tugul-u-n</ta>
            <ta e="T463" id="Seg_5503" s="T462">komuj-an</ta>
            <ta e="T464" id="Seg_5504" s="T463">bi͡er-d-e</ta>
            <ta e="T465" id="Seg_5505" s="T464">aga-tɨ-gar</ta>
            <ta e="T466" id="Seg_5506" s="T465">dʼe</ta>
            <ta e="T467" id="Seg_5507" s="T466">iti</ta>
            <ta e="T468" id="Seg_5508" s="T467">kara</ta>
            <ta e="T469" id="Seg_5509" s="T468">tunʼag-a</ta>
            <ta e="T470" id="Seg_5510" s="T469">inʼe-te</ta>
            <ta e="T471" id="Seg_5511" s="T470">tugul-a</ta>
            <ta e="T472" id="Seg_5512" s="T471">tubut-a</ta>
            <ta e="T473" id="Seg_5513" s="T472">di͡e-n</ta>
            <ta e="T474" id="Seg_5514" s="T473">baraːn</ta>
            <ta e="T475" id="Seg_5515" s="T474">ɨraːktaːg-ɨ-n</ta>
            <ta e="T476" id="Seg_5516" s="T475">ostu͡ol-u-gar</ta>
            <ta e="T477" id="Seg_5517" s="T476">ogo</ta>
            <ta e="T478" id="Seg_5518" s="T477">oːnnʼuːr-u-n</ta>
            <ta e="T479" id="Seg_5519" s="T478">kurduk</ta>
            <ta e="T480" id="Seg_5520" s="T479">huburut-alaː-n</ta>
            <ta e="T481" id="Seg_5521" s="T480">keːh-eːr</ta>
            <ta e="T482" id="Seg_5522" s="T481">irgek</ta>
            <ta e="T483" id="Seg_5523" s="T482">koru͡oba-ttan</ta>
            <ta e="T484" id="Seg_5524" s="T483">törüː-r</ta>
            <ta e="T485" id="Seg_5525" s="T484">agaj</ta>
            <ta e="T486" id="Seg_5526" s="T485">hir-i-n</ta>
            <ta e="T487" id="Seg_5527" s="T486">bu</ta>
            <ta e="T488" id="Seg_5528" s="T487">bul-lu-m</ta>
            <ta e="T489" id="Seg_5529" s="T488">di͡e-r</ta>
            <ta e="T490" id="Seg_5530" s="T489">on-ton</ta>
            <ta e="T491" id="Seg_5531" s="T490">ordug-u</ta>
            <ta e="T492" id="Seg_5532" s="T491">da</ta>
            <ta e="T493" id="Seg_5533" s="T492">kini</ta>
            <ta e="T494" id="Seg_5534" s="T493">bil-er-e</ta>
            <ta e="T495" id="Seg_5535" s="T494">bu͡ol-u͡o</ta>
            <ta e="T496" id="Seg_5536" s="T495">du͡o</ta>
            <ta e="T497" id="Seg_5537" s="T496">dʼe</ta>
            <ta e="T498" id="Seg_5538" s="T497">oččogo</ta>
            <ta e="T499" id="Seg_5539" s="T498">mohuj-an</ta>
            <ta e="T500" id="Seg_5540" s="T499">ɨjɨt-ɨ͡a</ta>
            <ta e="T501" id="Seg_5541" s="T500">beje-ŋ</ta>
            <ta e="T502" id="Seg_5542" s="T501">öj-gü-nen</ta>
            <ta e="T503" id="Seg_5543" s="T502">gɨm-ma-tɨ-ŋ</ta>
            <ta e="T504" id="Seg_5544" s="T503">bu͡olu͡o</ta>
            <ta e="T505" id="Seg_5545" s="T504">iti-ččA</ta>
            <ta e="T506" id="Seg_5546" s="T505">öj-döːg-ü-ŋ</ta>
            <ta e="T507" id="Seg_5547" s="T506">bu͡ol-lar</ta>
            <ta e="T508" id="Seg_5548" s="T507">min</ta>
            <ta e="T509" id="Seg_5549" s="T508">iːk-pi-n-haːk-pɨ-n</ta>
            <ta e="T510" id="Seg_5550" s="T509">ɨraːst-ɨː</ta>
            <ta e="T511" id="Seg_5551" s="T510">iŋ-en-bat-an</ta>
            <ta e="T512" id="Seg_5552" s="T511">hɨt-ɨ͡a</ta>
            <ta e="T513" id="Seg_5553" s="T512">hu͡ok</ta>
            <ta e="T514" id="Seg_5554" s="T513">e-ti-ŋ</ta>
            <ta e="T515" id="Seg_5555" s="T514">kim</ta>
            <ta e="T516" id="Seg_5556" s="T515">ü͡öret-er</ta>
            <ta e="T517" id="Seg_5557" s="T516">d-i͡eg-e</ta>
            <ta e="T518" id="Seg_5558" s="T517">onno</ta>
            <ta e="T519" id="Seg_5559" s="T518">kist-eːje-gin</ta>
            <ta e="T520" id="Seg_5560" s="T519">diː-r</ta>
            <ta e="T521" id="Seg_5561" s="T520">kɨːh-a</ta>
            <ta e="T522" id="Seg_5562" s="T521">kuhagan</ta>
            <ta e="T523" id="Seg_5563" s="T522">kɨːs</ta>
            <ta e="T524" id="Seg_5564" s="T523">ogo-loːk-pun</ta>
            <ta e="T525" id="Seg_5565" s="T524">ol</ta>
            <ta e="T526" id="Seg_5566" s="T525">ü͡öret-eːktiː-r</ta>
            <ta e="T527" id="Seg_5567" s="T526">di͡e-r</ta>
            <ta e="T528" id="Seg_5568" s="T527">ogonnʼor</ta>
            <ta e="T529" id="Seg_5569" s="T528">harsi͡erda</ta>
            <ta e="T530" id="Seg_5570" s="T529">bar-ar</ta>
            <ta e="T531" id="Seg_5571" s="T530">u͡ol</ta>
            <ta e="T532" id="Seg_5572" s="T531">ɨraːktaːgɨ</ta>
            <ta e="T533" id="Seg_5573" s="T532">ɨjɨt-ar</ta>
            <ta e="T534" id="Seg_5574" s="T533">kaja</ta>
            <ta e="T535" id="Seg_5575" s="T534">taːj-dɨ-ŋ</ta>
            <ta e="T536" id="Seg_5576" s="T535">du͡o</ta>
            <ta e="T537" id="Seg_5577" s="T536">ogonnʼor</ta>
            <ta e="T538" id="Seg_5578" s="T537">koru͡oba</ta>
            <ta e="T539" id="Seg_5579" s="T538">tunʼag-ɨ-n</ta>
            <ta e="T540" id="Seg_5580" s="T539">tugul-u-n</ta>
            <ta e="T541" id="Seg_5581" s="T540">ostu͡ol-ga</ta>
            <ta e="T542" id="Seg_5582" s="T541">ogo</ta>
            <ta e="T543" id="Seg_5583" s="T542">oːnnʼuːr-u-n</ta>
            <ta e="T544" id="Seg_5584" s="T543">kurduk</ta>
            <ta e="T545" id="Seg_5585" s="T544">huburut-alaː-n</ta>
            <ta e="T546" id="Seg_5586" s="T545">keːh-er</ta>
            <ta e="T547" id="Seg_5587" s="T546">irgek</ta>
            <ta e="T548" id="Seg_5588" s="T547">koru͡oba-ttan</ta>
            <ta e="T549" id="Seg_5589" s="T548">törüː-r</ta>
            <ta e="T550" id="Seg_5590" s="T549">agaj</ta>
            <ta e="T551" id="Seg_5591" s="T550">hir-i-n</ta>
            <ta e="T552" id="Seg_5592" s="T551">bu</ta>
            <ta e="T553" id="Seg_5593" s="T552">bul-lu-m</ta>
            <ta e="T554" id="Seg_5594" s="T553">diː-r</ta>
            <ta e="T555" id="Seg_5595" s="T554">ɨraːktaːgɨ</ta>
            <ta e="T556" id="Seg_5596" s="T555">beje-ŋ</ta>
            <ta e="T557" id="Seg_5597" s="T556">öj-gü-nen</ta>
            <ta e="T558" id="Seg_5598" s="T557">gɨm-ma-tɨ-ŋ</ta>
            <ta e="T559" id="Seg_5599" s="T558">bu͡olu͡o</ta>
            <ta e="T560" id="Seg_5600" s="T559">iti-ččA</ta>
            <ta e="T561" id="Seg_5601" s="T560">öj-döːg-u-ŋ</ta>
            <ta e="T562" id="Seg_5602" s="T561">bu͡ol-lar</ta>
            <ta e="T563" id="Seg_5603" s="T562">min</ta>
            <ta e="T564" id="Seg_5604" s="T563">iːk-pi-n-haːk-pɨ-n</ta>
            <ta e="T565" id="Seg_5605" s="T564">ɨraːst-ɨː</ta>
            <ta e="T566" id="Seg_5606" s="T565">iŋ-en-bat-an</ta>
            <ta e="T567" id="Seg_5607" s="T566">hɨt-ɨ͡a</ta>
            <ta e="T568" id="Seg_5608" s="T567">hu͡ok</ta>
            <ta e="T569" id="Seg_5609" s="T568">e-ti-ŋ</ta>
            <ta e="T570" id="Seg_5610" s="T569">kim</ta>
            <ta e="T571" id="Seg_5611" s="T570">ü͡öret-er</ta>
            <ta e="T572" id="Seg_5612" s="T571">diː-r</ta>
            <ta e="T573" id="Seg_5613" s="T572">kuhagan</ta>
            <ta e="T574" id="Seg_5614" s="T573">kɨːs</ta>
            <ta e="T575" id="Seg_5615" s="T574">ogo-loːk-pun</ta>
            <ta e="T576" id="Seg_5616" s="T575">ol</ta>
            <ta e="T577" id="Seg_5617" s="T576">ü͡öret-eːktiː-r</ta>
            <ta e="T578" id="Seg_5618" s="T577">diː-r</ta>
            <ta e="T579" id="Seg_5619" s="T578">ogonnʼor</ta>
            <ta e="T580" id="Seg_5620" s="T579">u͡ol</ta>
            <ta e="T581" id="Seg_5621" s="T580">ɨraːktaːgɨ</ta>
            <ta e="T582" id="Seg_5622" s="T581">kɨːh-ɨ-ŋ</ta>
            <ta e="T583" id="Seg_5623" s="T582">öj-döːk</ta>
            <ta e="T584" id="Seg_5624" s="T583">kɨːs</ta>
            <ta e="T585" id="Seg_5625" s="T584">töröː-büt</ta>
            <ta e="T586" id="Seg_5626" s="T585">diː-r</ta>
            <ta e="T587" id="Seg_5627" s="T586">timir</ta>
            <ta e="T588" id="Seg_5628" s="T587">kokoroːg-u</ta>
            <ta e="T589" id="Seg_5629" s="T588">tügeg-e</ta>
            <ta e="T590" id="Seg_5630" s="T589">kajagas-taːg-ɨ</ta>
            <ta e="T591" id="Seg_5631" s="T590">bi͡er-er</ta>
            <ta e="T592" id="Seg_5632" s="T591">ma-nɨ</ta>
            <ta e="T593" id="Seg_5633" s="T592">hamaː-n</ta>
            <ta e="T594" id="Seg_5634" s="T593">ɨːt-tɨn</ta>
            <ta e="T595" id="Seg_5635" s="T594">diː-r</ta>
            <ta e="T596" id="Seg_5636" s="T595">on-u</ta>
            <ta e="T597" id="Seg_5637" s="T596">hamaː-ba-tag-ɨna</ta>
            <ta e="T598" id="Seg_5638" s="T597">ikk-i͡en-ŋiti-n</ta>
            <ta e="T599" id="Seg_5639" s="T598">töbö-gütü-n</ta>
            <ta e="T600" id="Seg_5640" s="T599">bɨh-ɨ͡a-m</ta>
            <ta e="T601" id="Seg_5641" s="T600">diː-r</ta>
            <ta e="T602" id="Seg_5642" s="T601">ogonnʼor</ta>
            <ta e="T603" id="Seg_5643" s="T602">kokoroːg-u</ta>
            <ta e="T604" id="Seg_5644" s="T603">ildʼ-e</ta>
            <ta e="T605" id="Seg_5645" s="T604">bar-bɨt-a</ta>
            <ta e="T606" id="Seg_5646" s="T605">ɨt-ɨː-ɨt-ɨː</ta>
            <ta e="T607" id="Seg_5647" s="T606">kɨːs</ta>
            <ta e="T608" id="Seg_5648" s="T607">kaja</ta>
            <ta e="T609" id="Seg_5649" s="T608">agaː</ta>
            <ta e="T610" id="Seg_5650" s="T609">tu͡og-u-ŋ</ta>
            <ta e="T611" id="Seg_5651" s="T610">kokoroːg-o=j</ta>
            <ta e="T612" id="Seg_5652" s="T611">diː-r</ta>
            <ta e="T613" id="Seg_5653" s="T612">kaja-kuː</ta>
            <ta e="T614" id="Seg_5654" s="T613">dʼe</ta>
            <ta e="T615" id="Seg_5655" s="T614">ölör-ör</ta>
            <ta e="T616" id="Seg_5656" s="T615">kün-e</ta>
            <ta e="T617" id="Seg_5657" s="T616">kel-l-e</ta>
            <ta e="T618" id="Seg_5658" s="T617">bu</ta>
            <ta e="T619" id="Seg_5659" s="T618">kokoroːg-u</ta>
            <ta e="T620" id="Seg_5660" s="T619">hamaː-n</ta>
            <ta e="T621" id="Seg_5661" s="T620">ɨːt-tɨn</ta>
            <ta e="T622" id="Seg_5662" s="T621">diː-r</ta>
            <ta e="T623" id="Seg_5663" s="T622">bu-nu</ta>
            <ta e="T624" id="Seg_5664" s="T623">hamaː-ba-tag-ɨna</ta>
            <ta e="T625" id="Seg_5665" s="T624">ikk-i͡en-ŋiti-n</ta>
            <ta e="T626" id="Seg_5666" s="T625">töbö-gütü-n</ta>
            <ta e="T627" id="Seg_5667" s="T626">bɨh-ɨ͡a-m</ta>
            <ta e="T628" id="Seg_5668" s="T627">diː-r</ta>
            <ta e="T629" id="Seg_5669" s="T628">kɨːs</ta>
            <ta e="T630" id="Seg_5670" s="T629">diː-r</ta>
            <ta e="T631" id="Seg_5671" s="T630">ilt</ta>
            <ta e="T632" id="Seg_5672" s="T631">tötterü</ta>
            <ta e="T633" id="Seg_5673" s="T632">dʼaktar</ta>
            <ta e="T634" id="Seg_5674" s="T633">kihi</ta>
            <ta e="T635" id="Seg_5675" s="T634">ham-ɨ͡a-m</ta>
            <ta e="T636" id="Seg_5676" s="T635">er</ta>
            <ta e="T637" id="Seg_5677" s="T636">kihi</ta>
            <ta e="T638" id="Seg_5678" s="T637">kokoroːg-u</ta>
            <ta e="T639" id="Seg_5679" s="T638">čorčokoːt</ta>
            <ta e="T640" id="Seg_5680" s="T639">kördük</ta>
            <ta e="T641" id="Seg_5681" s="T640">ti͡er-en</ta>
            <ta e="T642" id="Seg_5682" s="T641">ɨːt-tɨn</ta>
            <ta e="T643" id="Seg_5683" s="T642">ogonnʼor</ta>
            <ta e="T644" id="Seg_5684" s="T643">ildʼ-en</ta>
            <ta e="T645" id="Seg_5685" s="T644">bi͡er-er</ta>
            <ta e="T646" id="Seg_5686" s="T645">u͡ol</ta>
            <ta e="T647" id="Seg_5687" s="T646">ɨraːktaːgɨ</ta>
            <ta e="T648" id="Seg_5688" s="T647">hög-ör</ta>
            <ta e="T649" id="Seg_5689" s="T648">ogonnʼor-u</ta>
            <ta e="T650" id="Seg_5690" s="T649">ɨːt-ar</ta>
            <ta e="T651" id="Seg_5691" s="T650">beje-m</ta>
            <ta e="T652" id="Seg_5692" s="T651">kɨːs-kɨ-n</ta>
            <ta e="T653" id="Seg_5693" s="T652">kör-ö</ta>
            <ta e="T654" id="Seg_5694" s="T653">bar-ɨ͡a-m</ta>
            <ta e="T655" id="Seg_5695" s="T654">diː-r</ta>
            <ta e="T656" id="Seg_5696" s="T655">u͡ol</ta>
            <ta e="T657" id="Seg_5697" s="T656">ɨraːktaːgɨ</ta>
            <ta e="T658" id="Seg_5698" s="T657">kɨːh-ɨ</ta>
            <ta e="T659" id="Seg_5699" s="T658">kör-öːt</ta>
            <ta e="T660" id="Seg_5700" s="T659">bagar-ar</ta>
            <ta e="T661" id="Seg_5701" s="T660">dʼaktar</ta>
            <ta e="T662" id="Seg_5702" s="T661">ɨl-ar</ta>
            <ta e="T663" id="Seg_5703" s="T662">töhö-nü-kačča-nɨ</ta>
            <ta e="T664" id="Seg_5704" s="T663">olor-but-tara</ta>
            <ta e="T665" id="Seg_5705" s="T664">bu͡ol-l-a</ta>
            <ta e="T666" id="Seg_5706" s="T665">dʼaktar-dam-mɨt-ɨ-ttan</ta>
            <ta e="T667" id="Seg_5707" s="T666">u͡ol</ta>
            <ta e="T668" id="Seg_5708" s="T667">ɨraːktaːgɨ</ta>
            <ta e="T669" id="Seg_5709" s="T668">kuːkula</ta>
            <ta e="T670" id="Seg_5710" s="T669">kurduk</ta>
            <ta e="T671" id="Seg_5711" s="T670">olor-čok</ta>
            <ta e="T672" id="Seg_5712" s="T671">bu͡ol-l-a</ta>
            <ta e="T673" id="Seg_5713" s="T672">barɨ-tɨ-n</ta>
            <ta e="T674" id="Seg_5714" s="T673">dʼaktar-a</ta>
            <ta e="T675" id="Seg_5715" s="T674">hubeliː-r</ta>
            <ta e="T676" id="Seg_5716" s="T675">bɨhaːr-ar</ta>
            <ta e="T677" id="Seg_5717" s="T676">u͡ol</ta>
            <ta e="T678" id="Seg_5718" s="T677">ɨraːktaːgɨ</ta>
            <ta e="T679" id="Seg_5719" s="T678">tɨl-lan-ar</ta>
            <ta e="T680" id="Seg_5720" s="T679">dogoː</ta>
            <ta e="T681" id="Seg_5721" s="T680">bu</ta>
            <ta e="T682" id="Seg_5722" s="T681">biːr</ta>
            <ta e="T683" id="Seg_5723" s="T682">dojdu-ga</ta>
            <ta e="T684" id="Seg_5724" s="T683">ikki</ta>
            <ta e="T685" id="Seg_5725" s="T684">ɨraːktaːgɨ</ta>
            <ta e="T686" id="Seg_5726" s="T685">bu͡ol-ar-bɨt</ta>
            <ta e="T687" id="Seg_5727" s="T686">tabɨll-ɨ-bat</ta>
            <ta e="T688" id="Seg_5728" s="T687">bihigi</ta>
            <ta e="T689" id="Seg_5729" s="T688">araks-ɨ͡ak</ta>
            <ta e="T690" id="Seg_5730" s="T689">dʼaktar-a</ta>
            <ta e="T691" id="Seg_5731" s="T690">dʼe</ta>
            <ta e="T692" id="Seg_5732" s="T691">araks-ɨ͡ak</ta>
            <ta e="T693" id="Seg_5733" s="T692">duː</ta>
            <ta e="T694" id="Seg_5734" s="T693">diː-r</ta>
            <ta e="T695" id="Seg_5735" s="T694">u͡ol</ta>
            <ta e="T696" id="Seg_5736" s="T695">ɨraːktaːgɨ</ta>
            <ta e="T697" id="Seg_5737" s="T696">tu͡ok</ta>
            <ta e="T698" id="Seg_5738" s="T697">baga-laːk</ta>
            <ta e="T699" id="Seg_5739" s="T698">hir-gi-n</ta>
            <ta e="T700" id="Seg_5740" s="T699">barɨ-tɨ-n</ta>
            <ta e="T701" id="Seg_5741" s="T700">ɨl</ta>
            <ta e="T702" id="Seg_5742" s="T701">diː-r</ta>
            <ta e="T703" id="Seg_5743" s="T702">dʼaktar</ta>
            <ta e="T704" id="Seg_5744" s="T703">höbül-e-h-er</ta>
            <ta e="T705" id="Seg_5745" s="T704">araks-ar</ta>
            <ta e="T706" id="Seg_5746" s="T705">tüːn-nere=keː</ta>
            <ta e="T707" id="Seg_5747" s="T706">bu͡ol-ar</ta>
            <ta e="T708" id="Seg_5748" s="T707">er-e</ta>
            <ta e="T709" id="Seg_5749" s="T708">utuj-an</ta>
            <ta e="T710" id="Seg_5750" s="T709">kaːl-ar</ta>
            <ta e="T711" id="Seg_5751" s="T710">dʼaktar</ta>
            <ta e="T712" id="Seg_5752" s="T711">er-i-n</ta>
            <ta e="T713" id="Seg_5753" s="T712">hu͡organ-nar-ɨ</ta>
            <ta e="T714" id="Seg_5754" s="T713">tellek-ter-i</ta>
            <ta e="T715" id="Seg_5755" s="T714">aga-tɨ-n</ta>
            <ta e="T716" id="Seg_5756" s="T715">bu͡or</ta>
            <ta e="T717" id="Seg_5757" s="T716">golomo</ta>
            <ta e="T718" id="Seg_5758" s="T717">dʼi͡e-ti-ger</ta>
            <ta e="T719" id="Seg_5759" s="T718">kötög-ön</ta>
            <ta e="T720" id="Seg_5760" s="T719">ildʼ-er</ta>
            <ta e="T721" id="Seg_5761" s="T720">u͡ol</ta>
            <ta e="T722" id="Seg_5762" s="T721">ɨraːktaːgɨ</ta>
            <ta e="T723" id="Seg_5763" s="T722">harsi͡erda</ta>
            <ta e="T724" id="Seg_5764" s="T723">uhukt-an</ta>
            <ta e="T725" id="Seg_5765" s="T724">hohuj-ar</ta>
            <ta e="T726" id="Seg_5766" s="T725">tɨːll-an</ta>
            <ta e="T727" id="Seg_5767" s="T726">kör-büt-e</ta>
            <ta e="T728" id="Seg_5768" s="T727">bah-a-atag-a</ta>
            <ta e="T729" id="Seg_5769" s="T728">golomo</ta>
            <ta e="T730" id="Seg_5770" s="T729">mah-ɨ-gar</ta>
            <ta e="T731" id="Seg_5771" s="T730">keb-il-ler</ta>
            <ta e="T732" id="Seg_5772" s="T731">kaja-keː</ta>
            <ta e="T733" id="Seg_5773" s="T732">bu</ta>
            <ta e="T734" id="Seg_5774" s="T733">kanna</ta>
            <ta e="T735" id="Seg_5775" s="T734">baːr-bɨt=ɨj</ta>
            <ta e="T736" id="Seg_5776" s="T735">togo</ta>
            <ta e="T737" id="Seg_5777" s="T736">kel-bit-i-m=ij</ta>
            <ta e="T738" id="Seg_5778" s="T737">dʼaktar-ɨ-n</ta>
            <ta e="T739" id="Seg_5779" s="T738">keb-i͡eleː-n</ta>
            <ta e="T740" id="Seg_5780" s="T739">uhugun-nar-ar</ta>
            <ta e="T741" id="Seg_5781" s="T740">dʼaktar-a</ta>
            <ta e="T742" id="Seg_5782" s="T741">kaja</ta>
            <ta e="T743" id="Seg_5783" s="T742">dogoː</ta>
            <ta e="T744" id="Seg_5784" s="T743">min</ta>
            <ta e="T745" id="Seg_5785" s="T744">egel-bit-i-m</ta>
            <ta e="T746" id="Seg_5786" s="T745">diː-r</ta>
            <ta e="T747" id="Seg_5787" s="T746">togo</ta>
            <ta e="T748" id="Seg_5788" s="T747">egel-bik-kin=ij</ta>
            <ta e="T749" id="Seg_5789" s="T748">tɨːj</ta>
            <ta e="T750" id="Seg_5790" s="T749">beje-gi-n</ta>
            <ta e="T751" id="Seg_5791" s="T750">bagar-dɨ-m</ta>
            <ta e="T752" id="Seg_5792" s="T751">da</ta>
            <ta e="T753" id="Seg_5793" s="T752">beje-gi-n</ta>
            <ta e="T754" id="Seg_5794" s="T753">egel-li-m</ta>
            <ta e="T755" id="Seg_5795" s="T754">tu͡og-u</ta>
            <ta e="T756" id="Seg_5796" s="T755">bagar-ar-gɨ-n</ta>
            <ta e="T757" id="Seg_5797" s="T756">ɨl-aːr</ta>
            <ta e="T758" id="Seg_5798" s="T757">di͡e-bit-i-ŋ</ta>
            <ta e="T759" id="Seg_5799" s="T758">diː</ta>
            <ta e="T760" id="Seg_5800" s="T759">diː-r</ta>
            <ta e="T761" id="Seg_5801" s="T760">dʼaktar</ta>
            <ta e="T762" id="Seg_5802" s="T761">u͡ol</ta>
            <ta e="T763" id="Seg_5803" s="T762">ɨraːktaːgɨ</ta>
            <ta e="T764" id="Seg_5804" s="T763">kɨ͡aj-an</ta>
            <ta e="T765" id="Seg_5805" s="T764">arak-pa-t-a</ta>
            <ta e="T766" id="Seg_5806" s="T765">dʼe</ta>
            <ta e="T767" id="Seg_5807" s="T766">dʼaktar-a</ta>
            <ta e="T768" id="Seg_5808" s="T767">diː-r</ta>
            <ta e="T769" id="Seg_5809" s="T768">oččo</ta>
            <ta e="T770" id="Seg_5810" s="T769">öj-döːk</ta>
            <ta e="T771" id="Seg_5811" s="T770">kihi</ta>
            <ta e="T772" id="Seg_5812" s="T771">bu͡ol-batak</ta>
            <ta e="T773" id="Seg_5813" s="T772">e-bik-kin</ta>
            <ta e="T774" id="Seg_5814" s="T773">dʼoŋ-ŋu-n</ta>
            <ta e="T775" id="Seg_5815" s="T774">hübeleː-n</ta>
            <ta e="T776" id="Seg_5816" s="T775">beje-ŋ</ta>
            <ta e="T777" id="Seg_5817" s="T776">olor</ta>
            <ta e="T778" id="Seg_5818" s="T777">min</ta>
            <ta e="T779" id="Seg_5819" s="T778">könnörü</ta>
            <ta e="T780" id="Seg_5820" s="T779">küseːjke</ta>
            <ta e="T781" id="Seg_5821" s="T780">bu͡ol-an</ta>
            <ta e="T782" id="Seg_5822" s="T781">olor-u͡o-m</ta>
            <ta e="T783" id="Seg_5823" s="T782">umnahɨt</ta>
            <ta e="T784" id="Seg_5824" s="T783">paːsɨnaj</ta>
            <ta e="T785" id="Seg_5825" s="T784">kɨːh-ɨ-n</ta>
            <ta e="T786" id="Seg_5826" s="T785">ütü͡ö-tü-nen</ta>
            <ta e="T787" id="Seg_5827" s="T786">hi͡erkile</ta>
            <ta e="T788" id="Seg_5828" s="T787">taːs</ta>
            <ta e="T789" id="Seg_5829" s="T788">dʼi͡e-ge</ta>
            <ta e="T790" id="Seg_5830" s="T789">olor-dog-o</ta>
            <ta e="T791" id="Seg_5831" s="T790">bu͡or</ta>
            <ta e="T792" id="Seg_5832" s="T791">golomo-tton</ta>
            <ta e="T793" id="Seg_5833" s="T792">araks-an</ta>
            <ta e="T794" id="Seg_5834" s="T793">uhug-u-ttan</ta>
            <ta e="T795" id="Seg_5835" s="T794">baj-an-taj-an</ta>
            <ta e="T796" id="Seg_5836" s="T795">olor-dok-toro</ta>
            <ta e="T797" id="Seg_5837" s="T796">dʼe</ta>
            <ta e="T798" id="Seg_5838" s="T797">ele-te</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_5839" s="T0">dʼe</ta>
            <ta e="T2" id="Seg_5840" s="T1">u͡ol</ta>
            <ta e="T3" id="Seg_5841" s="T2">ɨraːktaːgɨ</ta>
            <ta e="T4" id="Seg_5842" s="T3">olor-BIT-tA</ta>
            <ta e="T5" id="Seg_5843" s="T4">ulka-tI-GAr</ta>
            <ta e="T6" id="Seg_5844" s="T5">umnahɨt</ta>
            <ta e="T7" id="Seg_5845" s="T6">paːsɨnaj-LAːK</ta>
            <ta e="T8" id="Seg_5846" s="T7">umnahɨt</ta>
            <ta e="T9" id="Seg_5847" s="T8">paːsɨnaj</ta>
            <ta e="T10" id="Seg_5848" s="T9">bu</ta>
            <ta e="T11" id="Seg_5849" s="T10">ɨraːktaːgɨ-GA</ta>
            <ta e="T12" id="Seg_5850" s="T11">üleleː-An</ta>
            <ta e="T13" id="Seg_5851" s="T12">harsi͡erdaːŋŋɨ</ta>
            <ta e="T14" id="Seg_5852" s="T13">karaŋa-ttAn</ta>
            <ta e="T15" id="Seg_5853" s="T14">ki͡ehe</ta>
            <ta e="T16" id="Seg_5854" s="T15">karaŋa-GA</ta>
            <ta e="T17" id="Seg_5855" s="T16">di͡eri</ta>
            <ta e="T18" id="Seg_5856" s="T17">biːr</ta>
            <ta e="T19" id="Seg_5857" s="T18">lu͡osku</ta>
            <ta e="T20" id="Seg_5858" s="T19">burduk</ta>
            <ta e="T21" id="Seg_5859" s="T20">ihin</ta>
            <ta e="T22" id="Seg_5860" s="T21">üleleː-Ar</ta>
            <ta e="T23" id="Seg_5861" s="T22">e-BIT</ta>
            <ta e="T24" id="Seg_5862" s="T23">bu-nAn</ta>
            <ta e="T25" id="Seg_5863" s="T24">iːt-n-An</ta>
            <ta e="T26" id="Seg_5864" s="T25">olor-Ar-LAr</ta>
            <ta e="T27" id="Seg_5865" s="T26">umnahɨt</ta>
            <ta e="T28" id="Seg_5866" s="T27">paːsɨnaj</ta>
            <ta e="T29" id="Seg_5867" s="T28">biːr</ta>
            <ta e="T30" id="Seg_5868" s="T29">kɨːs</ta>
            <ta e="T31" id="Seg_5869" s="T30">ogo-LAːK</ta>
            <ta e="T32" id="Seg_5870" s="T31">dʼe</ta>
            <ta e="T33" id="Seg_5871" s="T32">biːrde</ta>
            <ta e="T34" id="Seg_5872" s="T33">u͡ol</ta>
            <ta e="T35" id="Seg_5873" s="T34">ɨraːktaːgɨ</ta>
            <ta e="T36" id="Seg_5874" s="T35">di͡e-Ar</ta>
            <ta e="T37" id="Seg_5875" s="T36">kaja</ta>
            <ta e="T38" id="Seg_5876" s="T37">dʼe</ta>
            <ta e="T39" id="Seg_5877" s="T38">umnahɨt</ta>
            <ta e="T40" id="Seg_5878" s="T39">paːsɨnaj</ta>
            <ta e="T41" id="Seg_5879" s="T40">töhö</ta>
            <ta e="T42" id="Seg_5880" s="T41">öj-LAːK</ta>
            <ta e="T43" id="Seg_5881" s="T42">kihi</ta>
            <ta e="T44" id="Seg_5882" s="T43">töröː-An</ta>
            <ta e="T45" id="Seg_5883" s="T44">tönün-TI-ŋ</ta>
            <ta e="T46" id="Seg_5884" s="T45">taːburun-nA</ta>
            <ta e="T47" id="Seg_5885" s="T46">taːj</ta>
            <ta e="T48" id="Seg_5886" s="T47">minnʼiges-ttAn</ta>
            <ta e="T49" id="Seg_5887" s="T48">minnʼiges</ta>
            <ta e="T50" id="Seg_5888" s="T49">tu͡ok=Ij</ta>
            <ta e="T51" id="Seg_5889" s="T50">harsi͡erda</ta>
            <ta e="T52" id="Seg_5890" s="T51">kel-An</ta>
            <ta e="T53" id="Seg_5891" s="T52">togus</ta>
            <ta e="T54" id="Seg_5892" s="T53">čaːs-GA</ta>
            <ta e="T55" id="Seg_5893" s="T54">haŋar-Aːr</ta>
            <ta e="T56" id="Seg_5894" s="T55">ol-nI</ta>
            <ta e="T57" id="Seg_5895" s="T56">taːj-BA-TAK-GInA</ta>
            <ta e="T58" id="Seg_5896" s="T57">ɨjaː-Ar</ta>
            <ta e="T59" id="Seg_5897" s="T58">mas-BA-r</ta>
            <ta e="T60" id="Seg_5898" s="T59">moːj-LAːK</ta>
            <ta e="T61" id="Seg_5899" s="T60">bas-GI-n</ta>
            <ta e="T62" id="Seg_5900" s="T61">bɨs-IAK-m</ta>
            <ta e="T63" id="Seg_5901" s="T62">umnahɨt</ta>
            <ta e="T64" id="Seg_5902" s="T63">paːsɨnaj</ta>
            <ta e="T65" id="Seg_5903" s="T64">togo</ta>
            <ta e="T66" id="Seg_5904" s="T65">da</ta>
            <ta e="T67" id="Seg_5905" s="T66">öj-A</ta>
            <ta e="T68" id="Seg_5906" s="T67">kɨ͡aj-BAT</ta>
            <ta e="T69" id="Seg_5907" s="T68">minnʼiges</ta>
            <ta e="T70" id="Seg_5908" s="T69">as-nI</ta>
            <ta e="T71" id="Seg_5909" s="T70">bil-AːktAː-BAT</ta>
            <ta e="T72" id="Seg_5910" s="T71">ɨraːktaːgɨ</ta>
            <ta e="T73" id="Seg_5911" s="T72">as-tA</ta>
            <ta e="T74" id="Seg_5912" s="T73">barɨ-tA</ta>
            <ta e="T75" id="Seg_5913" s="T74">minnʼiges</ta>
            <ta e="T76" id="Seg_5914" s="T75">kurduk</ta>
            <ta e="T77" id="Seg_5915" s="T76">gini-GA</ta>
            <ta e="T78" id="Seg_5916" s="T77">dʼi͡e-tI-GAr</ta>
            <ta e="T79" id="Seg_5917" s="T78">kel-An</ta>
            <ta e="T80" id="Seg_5918" s="T79">ɨtaː-A-n-A</ta>
            <ta e="T81" id="Seg_5919" s="T80">olor-TI-tA</ta>
            <ta e="T82" id="Seg_5920" s="T81">kɨːs-tA</ta>
            <ta e="T83" id="Seg_5921" s="T82">ɨjɨt-Ar</ta>
            <ta e="T84" id="Seg_5922" s="T83">kaja</ta>
            <ta e="T85" id="Seg_5923" s="T84">aga</ta>
            <ta e="T86" id="Seg_5924" s="T85">togo</ta>
            <ta e="T87" id="Seg_5925" s="T86">ɨtaː-TI-ŋ</ta>
            <ta e="T88" id="Seg_5926" s="T87">dʼe</ta>
            <ta e="T89" id="Seg_5927" s="T88">dogor-I-m</ta>
            <ta e="T90" id="Seg_5928" s="T89">hɨrɨt-I-BIT-I-m</ta>
            <ta e="T91" id="Seg_5929" s="T90">hɨččak</ta>
            <ta e="T92" id="Seg_5930" s="T91">olor-BIT-I-m</ta>
            <ta e="T93" id="Seg_5931" s="T92">uhuk-tA</ta>
            <ta e="T94" id="Seg_5932" s="T93">u͡ol</ta>
            <ta e="T95" id="Seg_5933" s="T94">ɨraːktaːgɨ</ta>
            <ta e="T96" id="Seg_5934" s="T95">taːburun</ta>
            <ta e="T97" id="Seg_5935" s="T96">bi͡er-TI-tA</ta>
            <ta e="T98" id="Seg_5936" s="T97">taːj-BA-TAK-BInA</ta>
            <ta e="T99" id="Seg_5937" s="T98">moːj-LAːK</ta>
            <ta e="T100" id="Seg_5938" s="T99">bas-BI-n</ta>
            <ta e="T101" id="Seg_5939" s="T100">bɨs-IAK</ta>
            <ta e="T102" id="Seg_5940" s="T101">bu͡ol-TI-tA</ta>
            <ta e="T103" id="Seg_5941" s="T102">tu͡ok-nI</ta>
            <ta e="T104" id="Seg_5942" s="T103">taːj-t-TAr-Ar</ta>
            <ta e="T105" id="Seg_5943" s="T104">ol</ta>
            <ta e="T106" id="Seg_5944" s="T105">minnʼiges-ttAn</ta>
            <ta e="T107" id="Seg_5945" s="T106">minnʼiges</ta>
            <ta e="T108" id="Seg_5946" s="T107">tu͡ok=Ij</ta>
            <ta e="T109" id="Seg_5947" s="T108">di͡e-Ar</ta>
            <ta e="T110" id="Seg_5948" s="T109">di͡e-BIT</ta>
            <ta e="T111" id="Seg_5949" s="T110">eː</ta>
            <ta e="T112" id="Seg_5950" s="T111">aga</ta>
            <ta e="T113" id="Seg_5951" s="T112">tuha-tA</ta>
            <ta e="T114" id="Seg_5952" s="T113">hu͡ok-GA</ta>
            <ta e="T115" id="Seg_5953" s="T114">togo</ta>
            <ta e="T116" id="Seg_5954" s="T115">ɨtaː-A-GIn</ta>
            <ta e="T117" id="Seg_5955" s="T116">utuj-AːktAː</ta>
            <ta e="T118" id="Seg_5956" s="T117">hɨnnʼan</ta>
            <ta e="T119" id="Seg_5957" s="T118">minnʼiges-ttAn</ta>
            <ta e="T120" id="Seg_5958" s="T119">minnʼiges-nI</ta>
            <ta e="T121" id="Seg_5959" s="T120">bil-Ar</ta>
            <ta e="T122" id="Seg_5960" s="T121">hir-I-m</ta>
            <ta e="T123" id="Seg_5961" s="T122">biːr</ta>
            <ta e="T124" id="Seg_5962" s="T123">di͡e-Aːr</ta>
            <ta e="T125" id="Seg_5963" s="T124">harsi͡erdaːŋŋɨ</ta>
            <ta e="T126" id="Seg_5964" s="T125">karaŋa-ttAn</ta>
            <ta e="T127" id="Seg_5965" s="T126">ki͡eheːŋŋi</ta>
            <ta e="T128" id="Seg_5966" s="T127">karaŋa-GA</ta>
            <ta e="T129" id="Seg_5967" s="T128">di͡eri</ta>
            <ta e="T130" id="Seg_5968" s="T129">üleleː-An</ta>
            <ta e="T131" id="Seg_5969" s="T130">en</ta>
            <ta e="T132" id="Seg_5970" s="T131">haːk-GI-n-kir-GI-n</ta>
            <ta e="T133" id="Seg_5971" s="T132">ɨraːstaː-An</ta>
            <ta e="T134" id="Seg_5972" s="T133">biːr</ta>
            <ta e="T135" id="Seg_5973" s="T134">lu͡osku</ta>
            <ta e="T136" id="Seg_5974" s="T135">burduk-nI</ta>
            <ta e="T137" id="Seg_5975" s="T136">ilt-TAK-BInA</ta>
            <ta e="T138" id="Seg_5976" s="T137">emeːksin-I-m</ta>
            <ta e="T139" id="Seg_5977" s="T138">bu͡or</ta>
            <ta e="T140" id="Seg_5978" s="T139">golomo</ta>
            <ta e="T141" id="Seg_5979" s="T140">dʼi͡e-tI-n</ta>
            <ta e="T142" id="Seg_5980" s="T141">otun-TAK-InA</ta>
            <ta e="T143" id="Seg_5981" s="T142">munnu-m</ta>
            <ta e="T144" id="Seg_5982" s="T143">hɨlɨj-Ar</ta>
            <ta e="T145" id="Seg_5983" s="T144">ol</ta>
            <ta e="T146" id="Seg_5984" s="T145">burduk-kAːN-tI-nAn</ta>
            <ta e="T147" id="Seg_5985" s="T146">toloŋku͡o</ta>
            <ta e="T148" id="Seg_5986" s="T147">oŋor-TAK-InA</ta>
            <ta e="T149" id="Seg_5987" s="T148">ahaː-BAkkA</ta>
            <ta e="T150" id="Seg_5988" s="T149">da</ta>
            <ta e="T151" id="Seg_5989" s="T150">utuj-A-BIn</ta>
            <ta e="T152" id="Seg_5990" s="T151">onon</ta>
            <ta e="T153" id="Seg_5991" s="T152">minnʼiges-ttAn</ta>
            <ta e="T154" id="Seg_5992" s="T153">minnʼiges</ta>
            <ta e="T155" id="Seg_5993" s="T154">utuj-Ar</ta>
            <ta e="T156" id="Seg_5994" s="T155">uː</ta>
            <ta e="T157" id="Seg_5995" s="T156">bu͡ol-IAK-LAːK</ta>
            <ta e="T158" id="Seg_5996" s="T157">di͡e-Aːr</ta>
            <ta e="T159" id="Seg_5997" s="T158">gini</ta>
            <ta e="T160" id="Seg_5998" s="T159">ol-ttAn</ta>
            <ta e="T161" id="Seg_5999" s="T160">da</ta>
            <ta e="T162" id="Seg_6000" s="T161">orduk-nI</ta>
            <ta e="T163" id="Seg_6001" s="T162">bil-Ar-tA</ta>
            <ta e="T164" id="Seg_6002" s="T163">bu͡ol-IAK.[tA]</ta>
            <ta e="T165" id="Seg_6003" s="T164">du͡o</ta>
            <ta e="T166" id="Seg_6004" s="T165">di͡e-BIT</ta>
            <ta e="T167" id="Seg_6005" s="T166">kɨːs-tA</ta>
            <ta e="T168" id="Seg_6006" s="T167">ogonnʼor</ta>
            <ta e="T169" id="Seg_6007" s="T168">utuj-An</ta>
            <ta e="T170" id="Seg_6008" s="T169">kaːl-TI-tA</ta>
            <ta e="T171" id="Seg_6009" s="T170">harsi͡erda</ta>
            <ta e="T172" id="Seg_6010" s="T171">erde</ta>
            <ta e="T173" id="Seg_6011" s="T172">tur-An</ta>
            <ta e="T174" id="Seg_6012" s="T173">togus</ta>
            <ta e="T175" id="Seg_6013" s="T174">čaːs-GA</ta>
            <ta e="T176" id="Seg_6014" s="T175">ɨraːktaːgɨ-GA</ta>
            <ta e="T177" id="Seg_6015" s="T176">tij-Ar</ta>
            <ta e="T178" id="Seg_6016" s="T177">u͡ol</ta>
            <ta e="T179" id="Seg_6017" s="T178">ɨraːktaːgɨ</ta>
            <ta e="T180" id="Seg_6018" s="T179">ɨjɨt-Ar</ta>
            <ta e="T181" id="Seg_6019" s="T180">kaja</ta>
            <ta e="T182" id="Seg_6020" s="T181">taːj-TI-ŋ</ta>
            <ta e="T183" id="Seg_6021" s="T182">du͡o</ta>
            <ta e="T184" id="Seg_6022" s="T183">eː</ta>
            <ta e="T185" id="Seg_6023" s="T184">taːj-Ar-GA</ta>
            <ta e="T186" id="Seg_6024" s="T185">talɨ</ta>
            <ta e="T187" id="Seg_6025" s="T186">gɨn-TI-m</ta>
            <ta e="T188" id="Seg_6026" s="T187">höbö</ta>
            <ta e="T189" id="Seg_6027" s="T188">du͡o</ta>
            <ta e="T190" id="Seg_6028" s="T189">hɨːha-tA</ta>
            <ta e="T191" id="Seg_6029" s="T190">du͡o</ta>
            <ta e="T192" id="Seg_6030" s="T191">di͡e-Ar</ta>
            <ta e="T193" id="Seg_6031" s="T192">dʼe</ta>
            <ta e="T194" id="Seg_6032" s="T193">tu͡ok-nI=Ij</ta>
            <ta e="T195" id="Seg_6033" s="T194">harsi͡erdaːŋŋɨ</ta>
            <ta e="T196" id="Seg_6034" s="T195">karaŋa-ttAn</ta>
            <ta e="T197" id="Seg_6035" s="T196">ki͡eheːŋŋi</ta>
            <ta e="T198" id="Seg_6036" s="T197">karaŋa-GA</ta>
            <ta e="T199" id="Seg_6037" s="T198">di͡eri</ta>
            <ta e="T200" id="Seg_6038" s="T199">üleleː-An</ta>
            <ta e="T201" id="Seg_6039" s="T200">en</ta>
            <ta e="T202" id="Seg_6040" s="T201">haːk-GI-n-kir-GI-n</ta>
            <ta e="T203" id="Seg_6041" s="T202">ɨraːstaː-An</ta>
            <ta e="T204" id="Seg_6042" s="T203">biːr</ta>
            <ta e="T205" id="Seg_6043" s="T204">lu͡osku</ta>
            <ta e="T206" id="Seg_6044" s="T205">burduk-nI</ta>
            <ta e="T207" id="Seg_6045" s="T206">ilt-TAK-BInA</ta>
            <ta e="T208" id="Seg_6046" s="T207">emeːksin-I-m</ta>
            <ta e="T209" id="Seg_6047" s="T208">bu͡or</ta>
            <ta e="T210" id="Seg_6048" s="T209">golomo</ta>
            <ta e="T211" id="Seg_6049" s="T210">dʼi͡e-tI-n</ta>
            <ta e="T212" id="Seg_6050" s="T211">otun-TAK-InA</ta>
            <ta e="T213" id="Seg_6051" s="T212">munnu-m</ta>
            <ta e="T214" id="Seg_6052" s="T213">hɨlɨj-Ar</ta>
            <ta e="T215" id="Seg_6053" s="T214">ol</ta>
            <ta e="T216" id="Seg_6054" s="T215">burduk-kAːN-tI-nAn</ta>
            <ta e="T217" id="Seg_6055" s="T216">toloŋku͡o</ta>
            <ta e="T218" id="Seg_6056" s="T217">oŋor-TAK-InA</ta>
            <ta e="T219" id="Seg_6057" s="T218">ahaː-BAkkA</ta>
            <ta e="T220" id="Seg_6058" s="T219">da</ta>
            <ta e="T221" id="Seg_6059" s="T220">utuj-A-BIn</ta>
            <ta e="T222" id="Seg_6060" s="T221">onon</ta>
            <ta e="T223" id="Seg_6061" s="T222">minnʼiges-ttAn</ta>
            <ta e="T224" id="Seg_6062" s="T223">minnʼiges</ta>
            <ta e="T225" id="Seg_6063" s="T224">utuj-Ar</ta>
            <ta e="T226" id="Seg_6064" s="T225">uː</ta>
            <ta e="T227" id="Seg_6065" s="T226">bu͡ol-IAK-LAːK</ta>
            <ta e="T228" id="Seg_6066" s="T227">u͡ol</ta>
            <ta e="T229" id="Seg_6067" s="T228">ɨraːktaːgɨ</ta>
            <ta e="T230" id="Seg_6068" s="T229">di͡e-Ar</ta>
            <ta e="T231" id="Seg_6069" s="T230">oː</ta>
            <ta e="T232" id="Seg_6070" s="T231">dʼe</ta>
            <ta e="T233" id="Seg_6071" s="T232">öj-LAːK</ta>
            <ta e="T234" id="Seg_6072" s="T233">kihi-GIn</ta>
            <ta e="T235" id="Seg_6073" s="T234">e-BIT</ta>
            <ta e="T236" id="Seg_6074" s="T235">kajdak</ta>
            <ta e="T237" id="Seg_6075" s="T236">iŋ-An-bat-An</ta>
            <ta e="T238" id="Seg_6076" s="T237">hɨt-A-GIn</ta>
            <ta e="T239" id="Seg_6077" s="T238">össü͡ö</ta>
            <ta e="T240" id="Seg_6078" s="T239">taːj</ta>
            <ta e="T241" id="Seg_6079" s="T240">orto</ta>
            <ta e="T242" id="Seg_6080" s="T241">dojdu-GA</ta>
            <ta e="T243" id="Seg_6081" s="T242">türgen-ttAn</ta>
            <ta e="T244" id="Seg_6082" s="T243">türgen</ta>
            <ta e="T245" id="Seg_6083" s="T244">tu͡ok</ta>
            <ta e="T246" id="Seg_6084" s="T245">baːr=Ij</ta>
            <ta e="T247" id="Seg_6085" s="T246">harsi͡erda</ta>
            <ta e="T248" id="Seg_6086" s="T247">taːj-BA-TAK-GInA</ta>
            <ta e="T249" id="Seg_6087" s="T248">moːj-LAːK</ta>
            <ta e="T250" id="Seg_6088" s="T249">bas-GI-n</ta>
            <ta e="T251" id="Seg_6089" s="T250">bɨs-A-BIn</ta>
            <ta e="T252" id="Seg_6090" s="T251">ogonnʼor</ta>
            <ta e="T253" id="Seg_6091" s="T252">tu͡ok-nI</ta>
            <ta e="T254" id="Seg_6092" s="T253">da</ta>
            <ta e="T255" id="Seg_6093" s="T254">tobul-BAT</ta>
            <ta e="T256" id="Seg_6094" s="T255">tij-An</ta>
            <ta e="T257" id="Seg_6095" s="T256">ɨtaː-A-n-A</ta>
            <ta e="T258" id="Seg_6096" s="T257">olor-TAK-InA</ta>
            <ta e="T259" id="Seg_6097" s="T258">kɨːs-tA</ta>
            <ta e="T260" id="Seg_6098" s="T259">ɨjɨt-Ar</ta>
            <ta e="T261" id="Seg_6099" s="T260">ogonnʼor</ta>
            <ta e="T262" id="Seg_6100" s="T261">innʼe-innʼe</ta>
            <ta e="T263" id="Seg_6101" s="T262">di͡e-Ar</ta>
            <ta e="T264" id="Seg_6102" s="T263">tu͡ok</ta>
            <ta e="T265" id="Seg_6103" s="T264">tuha-tA</ta>
            <ta e="T266" id="Seg_6104" s="T265">hu͡ok-GA</ta>
            <ta e="T267" id="Seg_6105" s="T266">ɨtaː-A-GIn</ta>
            <ta e="T268" id="Seg_6106" s="T267">utuj</ta>
            <ta e="T269" id="Seg_6107" s="T268">harsi͡erda</ta>
            <ta e="T270" id="Seg_6108" s="T269">et-Aːr</ta>
            <ta e="T271" id="Seg_6109" s="T270">türgen-ttAn</ta>
            <ta e="T272" id="Seg_6110" s="T271">türgen</ta>
            <ta e="T273" id="Seg_6111" s="T272">di͡e-Aːr</ta>
            <ta e="T274" id="Seg_6112" s="T273">kuhagan</ta>
            <ta e="T275" id="Seg_6113" s="T274">buru͡o-LAːK</ta>
            <ta e="T276" id="Seg_6114" s="T275">bu͡or</ta>
            <ta e="T277" id="Seg_6115" s="T276">golomo-BI-ttAn</ta>
            <ta e="T278" id="Seg_6116" s="T277">tagɨs-A</ta>
            <ta e="T279" id="Seg_6117" s="T278">köt-An</ta>
            <ta e="T280" id="Seg_6118" s="T279">karak-BI-n</ta>
            <ta e="T281" id="Seg_6119" s="T280">kas-t-I-r-I-t-A</ta>
            <ta e="T282" id="Seg_6120" s="T281">hot-An</ta>
            <ta e="T283" id="Seg_6121" s="T282">oduːlaː-An</ta>
            <ta e="T284" id="Seg_6122" s="T283">kör-TAK-BInA</ta>
            <ta e="T285" id="Seg_6123" s="T284">kallaːn</ta>
            <ta e="T286" id="Seg_6124" s="T285">dek</ta>
            <ta e="T287" id="Seg_6125" s="T286">kantaj-TAK-BInA</ta>
            <ta e="T288" id="Seg_6126" s="T287">kallaːn</ta>
            <ta e="T289" id="Seg_6127" s="T288">hulus-tI-n</ta>
            <ta e="T290" id="Seg_6128" s="T289">karak-I-m</ta>
            <ta e="T291" id="Seg_6129" s="T290">aːga</ta>
            <ta e="T292" id="Seg_6130" s="T291">kör-Ar</ta>
            <ta e="T293" id="Seg_6131" s="T292">hir</ta>
            <ta e="T294" id="Seg_6132" s="T293">dek</ta>
            <ta e="T295" id="Seg_6133" s="T294">kör-TAK-BInA</ta>
            <ta e="T296" id="Seg_6134" s="T295">hir</ta>
            <ta e="T297" id="Seg_6135" s="T296">ot-tI-n-mas-tI-n</ta>
            <ta e="T298" id="Seg_6136" s="T297">aːga</ta>
            <ta e="T299" id="Seg_6137" s="T298">kör-A-BIn</ta>
            <ta e="T300" id="Seg_6138" s="T299">karak-I-m</ta>
            <ta e="T301" id="Seg_6139" s="T300">ɨl-Ar-I-GAr</ta>
            <ta e="T302" id="Seg_6140" s="T301">kahan</ta>
            <ta e="T303" id="Seg_6141" s="T302">da</ta>
            <ta e="T304" id="Seg_6142" s="T303">tij-BAT-BIn</ta>
            <ta e="T305" id="Seg_6143" s="T304">onon</ta>
            <ta e="T306" id="Seg_6144" s="T305">türgen-ttAn</ta>
            <ta e="T307" id="Seg_6145" s="T306">türgen</ta>
            <ta e="T308" id="Seg_6146" s="T307">kihi</ta>
            <ta e="T309" id="Seg_6147" s="T308">kör-Iː-tA</ta>
            <ta e="T310" id="Seg_6148" s="T309">di͡e-Aːr</ta>
            <ta e="T311" id="Seg_6149" s="T310">ol-ttAn</ta>
            <ta e="T312" id="Seg_6150" s="T311">orduk-nI</ta>
            <ta e="T313" id="Seg_6151" s="T312">gini</ta>
            <ta e="T314" id="Seg_6152" s="T313">bil-Ar-tA</ta>
            <ta e="T315" id="Seg_6153" s="T314">bu͡ol-IAK.[tA]</ta>
            <ta e="T316" id="Seg_6154" s="T315">du͡o</ta>
            <ta e="T317" id="Seg_6155" s="T316">di͡e-Ar</ta>
            <ta e="T318" id="Seg_6156" s="T317">kɨːs</ta>
            <ta e="T319" id="Seg_6157" s="T318">ogonnʼor</ta>
            <ta e="T320" id="Seg_6158" s="T319">utuj-An</ta>
            <ta e="T321" id="Seg_6159" s="T320">kaːl-TI-tA</ta>
            <ta e="T322" id="Seg_6160" s="T321">harsi͡erda</ta>
            <ta e="T323" id="Seg_6161" s="T322">tur-An</ta>
            <ta e="T324" id="Seg_6162" s="T323">ɨraːktaːgɨ-tI-GAr</ta>
            <ta e="T325" id="Seg_6163" s="T324">bar-TAK-tA</ta>
            <ta e="T326" id="Seg_6164" s="T325">u͡ol</ta>
            <ta e="T327" id="Seg_6165" s="T326">ɨraːktaːgɨ</ta>
            <ta e="T328" id="Seg_6166" s="T327">ɨjɨt-Ar</ta>
            <ta e="T329" id="Seg_6167" s="T328">kaja</ta>
            <ta e="T330" id="Seg_6168" s="T329">taːj-TI-ŋ</ta>
            <ta e="T331" id="Seg_6169" s="T330">du͡o</ta>
            <ta e="T332" id="Seg_6170" s="T331">eː</ta>
            <ta e="T333" id="Seg_6171" s="T332">taːj-Ar-GA</ta>
            <ta e="T334" id="Seg_6172" s="T333">talɨ</ta>
            <ta e="T335" id="Seg_6173" s="T334">gɨn-TI-m</ta>
            <ta e="T336" id="Seg_6174" s="T335">höbö</ta>
            <ta e="T337" id="Seg_6175" s="T336">du͡o</ta>
            <ta e="T338" id="Seg_6176" s="T337">hɨːha-tA</ta>
            <ta e="T339" id="Seg_6177" s="T338">du͡o</ta>
            <ta e="T340" id="Seg_6178" s="T339">di͡e-Ar</ta>
            <ta e="T341" id="Seg_6179" s="T340">kuhagan</ta>
            <ta e="T342" id="Seg_6180" s="T341">buru͡o-LAːK</ta>
            <ta e="T343" id="Seg_6181" s="T342">bu͡or</ta>
            <ta e="T344" id="Seg_6182" s="T343">golomo-BI-ttAn</ta>
            <ta e="T345" id="Seg_6183" s="T344">tagɨs-A</ta>
            <ta e="T346" id="Seg_6184" s="T345">köt-An</ta>
            <ta e="T347" id="Seg_6185" s="T346">karak-BI-n</ta>
            <ta e="T348" id="Seg_6186" s="T347">kas-t-I-r-I-t-A</ta>
            <ta e="T349" id="Seg_6187" s="T348">hot-An</ta>
            <ta e="T350" id="Seg_6188" s="T349">oduː-LAN-An</ta>
            <ta e="T351" id="Seg_6189" s="T350">kör-TAK-BInA</ta>
            <ta e="T352" id="Seg_6190" s="T351">kallaːn</ta>
            <ta e="T353" id="Seg_6191" s="T352">dek</ta>
            <ta e="T354" id="Seg_6192" s="T353">kantaj-TAK-BInA</ta>
            <ta e="T355" id="Seg_6193" s="T354">kallaːn</ta>
            <ta e="T356" id="Seg_6194" s="T355">hulus-tI-n</ta>
            <ta e="T357" id="Seg_6195" s="T356">karak-I-m</ta>
            <ta e="T358" id="Seg_6196" s="T357">aːga</ta>
            <ta e="T359" id="Seg_6197" s="T358">kör-Ar</ta>
            <ta e="T360" id="Seg_6198" s="T359">hir</ta>
            <ta e="T361" id="Seg_6199" s="T360">dek</ta>
            <ta e="T362" id="Seg_6200" s="T361">kör-TAK-BInA</ta>
            <ta e="T363" id="Seg_6201" s="T362">hir</ta>
            <ta e="T364" id="Seg_6202" s="T363">ot-tI-n-mas-tI-n</ta>
            <ta e="T365" id="Seg_6203" s="T364">aːga</ta>
            <ta e="T366" id="Seg_6204" s="T365">kör-A-BIn</ta>
            <ta e="T367" id="Seg_6205" s="T366">karak-I-m</ta>
            <ta e="T368" id="Seg_6206" s="T367">ɨl-Ar-tI-GAr</ta>
            <ta e="T369" id="Seg_6207" s="T368">kahan</ta>
            <ta e="T370" id="Seg_6208" s="T369">da</ta>
            <ta e="T371" id="Seg_6209" s="T370">tij-BAT-BIn</ta>
            <ta e="T372" id="Seg_6210" s="T371">onon</ta>
            <ta e="T373" id="Seg_6211" s="T372">türgen-ttAn</ta>
            <ta e="T374" id="Seg_6212" s="T373">türgen</ta>
            <ta e="T375" id="Seg_6213" s="T374">kihi</ta>
            <ta e="T376" id="Seg_6214" s="T375">kör-Iː-tA</ta>
            <ta e="T377" id="Seg_6215" s="T376">e-BIT</ta>
            <ta e="T378" id="Seg_6216" s="T377">di͡e-Ar</ta>
            <ta e="T379" id="Seg_6217" s="T378">u͡ol</ta>
            <ta e="T380" id="Seg_6218" s="T379">ɨraːktaːgɨ</ta>
            <ta e="T381" id="Seg_6219" s="T380">hök-Ar</ta>
            <ta e="T382" id="Seg_6220" s="T381">oː</ta>
            <ta e="T383" id="Seg_6221" s="T382">ogonnʼor</ta>
            <ta e="T384" id="Seg_6222" s="T383">dʼe</ta>
            <ta e="T385" id="Seg_6223" s="T384">öj-LAːK</ta>
            <ta e="T386" id="Seg_6224" s="T385">kihi-GIn</ta>
            <ta e="T387" id="Seg_6225" s="T386">e-BIT</ta>
            <ta e="T388" id="Seg_6226" s="T387">dʼe</ta>
            <ta e="T389" id="Seg_6227" s="T388">tu͡ok</ta>
            <ta e="T390" id="Seg_6228" s="T389">da</ta>
            <ta e="T391" id="Seg_6229" s="T390">üs</ta>
            <ta e="T392" id="Seg_6230" s="T391">tögül-LAːK</ta>
            <ta e="T393" id="Seg_6231" s="T392">tagɨs-An</ta>
            <ta e="T394" id="Seg_6232" s="T393">irgek</ta>
            <ta e="T395" id="Seg_6233" s="T394">koru͡oba-nI</ta>
            <ta e="T396" id="Seg_6234" s="T395">tut-An</ta>
            <ta e="T397" id="Seg_6235" s="T396">bi͡er-Ar</ta>
            <ta e="T398" id="Seg_6236" s="T397">bu-nI</ta>
            <ta e="T399" id="Seg_6237" s="T398">harsi͡erda</ta>
            <ta e="T400" id="Seg_6238" s="T399">töröː-A-t-An</ta>
            <ta e="T401" id="Seg_6239" s="T400">tugut-tI-n</ta>
            <ta e="T402" id="Seg_6240" s="T401">batɨs-I-n-TAr-An</ta>
            <ta e="T403" id="Seg_6241" s="T402">egel-Aːr</ta>
            <ta e="T404" id="Seg_6242" s="T403">kaja</ta>
            <ta e="T405" id="Seg_6243" s="T404">u͡orukta-tI-n</ta>
            <ta e="T406" id="Seg_6244" s="T405">komuj-An</ta>
            <ta e="T407" id="Seg_6245" s="T406">egel-AːjA-GIn</ta>
            <ta e="T408" id="Seg_6246" s="T407">di͡e-BIT</ta>
            <ta e="T409" id="Seg_6247" s="T408">u͡ol</ta>
            <ta e="T410" id="Seg_6248" s="T409">ɨraːktaːgɨ</ta>
            <ta e="T411" id="Seg_6249" s="T410">ogonnʼor</ta>
            <ta e="T412" id="Seg_6250" s="T411">erej-LAːK</ta>
            <ta e="T413" id="Seg_6251" s="T412">irgek</ta>
            <ta e="T414" id="Seg_6252" s="T413">koru͡oba-tI-n</ta>
            <ta e="T415" id="Seg_6253" s="T414">hi͡et-An</ta>
            <ta e="T416" id="Seg_6254" s="T415">tij-An</ta>
            <ta e="T417" id="Seg_6255" s="T416">ɨtaː-A</ta>
            <ta e="T418" id="Seg_6256" s="T417">olor-TI-tA</ta>
            <ta e="T419" id="Seg_6257" s="T418">kɨːs-tA</ta>
            <ta e="T420" id="Seg_6258" s="T419">ɨjɨt-TI-tA</ta>
            <ta e="T421" id="Seg_6259" s="T420">kaja</ta>
            <ta e="T422" id="Seg_6260" s="T421">aga</ta>
            <ta e="T423" id="Seg_6261" s="T422">togo</ta>
            <ta e="T424" id="Seg_6262" s="T423">ɨtaː-TI-ŋ</ta>
            <ta e="T425" id="Seg_6263" s="T424">kaja</ta>
            <ta e="T426" id="Seg_6264" s="T425">irgek</ta>
            <ta e="T427" id="Seg_6265" s="T426">koru͡oba-nI</ta>
            <ta e="T428" id="Seg_6266" s="T427">bi͡er-TI-tA</ta>
            <ta e="T429" id="Seg_6267" s="T428">töröː-A-t-An</ta>
            <ta e="T430" id="Seg_6268" s="T429">egel</ta>
            <ta e="T431" id="Seg_6269" s="T430">di͡e-Ar</ta>
            <ta e="T432" id="Seg_6270" s="T431">irgek</ta>
            <ta e="T433" id="Seg_6271" s="T432">koru͡oba</ta>
            <ta e="T434" id="Seg_6272" s="T433">kantɨnan</ta>
            <ta e="T435" id="Seg_6273" s="T434">töröː-AːččI-nI=Ij</ta>
            <ta e="T436" id="Seg_6274" s="T435">kuːgu</ta>
            <ta e="T437" id="Seg_6275" s="T436">eː</ta>
            <ta e="T438" id="Seg_6276" s="T437">irgek</ta>
            <ta e="T439" id="Seg_6277" s="T438">koru͡oba</ta>
            <ta e="T440" id="Seg_6278" s="T439">kantan</ta>
            <ta e="T441" id="Seg_6279" s="T440">töröː-IAK.[tA]=Ij</ta>
            <ta e="T442" id="Seg_6280" s="T441">kata</ta>
            <ta e="T443" id="Seg_6281" s="T442">as-LAN-BIT-BIt</ta>
            <ta e="T444" id="Seg_6282" s="T443">tagɨs-An-GIn</ta>
            <ta e="T445" id="Seg_6283" s="T444">ölör-BAktAː</ta>
            <ta e="T446" id="Seg_6284" s="T445">et-tI-n</ta>
            <ta e="T447" id="Seg_6285" s="T446">hi͡e-IAK-BIt</ta>
            <ta e="T448" id="Seg_6286" s="T447">di͡e-Ar</ta>
            <ta e="T449" id="Seg_6287" s="T448">kɨːs</ta>
            <ta e="T450" id="Seg_6288" s="T449">ogonnʼor</ta>
            <ta e="T451" id="Seg_6289" s="T450">irgek</ta>
            <ta e="T452" id="Seg_6290" s="T451">koru͡oba-tI-n</ta>
            <ta e="T453" id="Seg_6291" s="T452">ölör-An</ta>
            <ta e="T454" id="Seg_6292" s="T453">keːs-Ar</ta>
            <ta e="T455" id="Seg_6293" s="T454">dʼe</ta>
            <ta e="T456" id="Seg_6294" s="T455">bu-nI</ta>
            <ta e="T457" id="Seg_6295" s="T456">hi͡e-IAK-LArA</ta>
            <ta e="T458" id="Seg_6296" s="T457">kɨːs-tA</ta>
            <ta e="T459" id="Seg_6297" s="T458">utakaː-GA</ta>
            <ta e="T460" id="Seg_6298" s="T459">koru͡oba</ta>
            <ta e="T461" id="Seg_6299" s="T460">tunʼak-tI-n</ta>
            <ta e="T462" id="Seg_6300" s="T461">tugul-tI-n</ta>
            <ta e="T463" id="Seg_6301" s="T462">komuj-An</ta>
            <ta e="T464" id="Seg_6302" s="T463">bi͡er-TI-tA</ta>
            <ta e="T465" id="Seg_6303" s="T464">aga-tI-GAr</ta>
            <ta e="T466" id="Seg_6304" s="T465">dʼe</ta>
            <ta e="T467" id="Seg_6305" s="T466">iti</ta>
            <ta e="T468" id="Seg_6306" s="T467">kara</ta>
            <ta e="T469" id="Seg_6307" s="T468">tunʼak-tA</ta>
            <ta e="T470" id="Seg_6308" s="T469">inʼe-tA</ta>
            <ta e="T471" id="Seg_6309" s="T470">tugul-tA</ta>
            <ta e="T472" id="Seg_6310" s="T471">tugut-tA</ta>
            <ta e="T473" id="Seg_6311" s="T472">di͡e-An</ta>
            <ta e="T474" id="Seg_6312" s="T473">baran</ta>
            <ta e="T475" id="Seg_6313" s="T474">ɨraːktaːgɨ-tI-n</ta>
            <ta e="T476" id="Seg_6314" s="T475">ostu͡ol-tI-GAr</ta>
            <ta e="T477" id="Seg_6315" s="T476">ogo</ta>
            <ta e="T478" id="Seg_6316" s="T477">oːnnʼuːr-tI-n</ta>
            <ta e="T479" id="Seg_6317" s="T478">kurduk</ta>
            <ta e="T480" id="Seg_6318" s="T479">huburut-AlAː-An</ta>
            <ta e="T481" id="Seg_6319" s="T480">keːs-Aːr</ta>
            <ta e="T482" id="Seg_6320" s="T481">irgek</ta>
            <ta e="T483" id="Seg_6321" s="T482">koru͡oba-ttAn</ta>
            <ta e="T484" id="Seg_6322" s="T483">töröː-Ar</ta>
            <ta e="T485" id="Seg_6323" s="T484">agaj</ta>
            <ta e="T486" id="Seg_6324" s="T485">hir-tI-n</ta>
            <ta e="T487" id="Seg_6325" s="T486">bu</ta>
            <ta e="T488" id="Seg_6326" s="T487">bul-TI-m</ta>
            <ta e="T489" id="Seg_6327" s="T488">di͡e-Aːr</ta>
            <ta e="T490" id="Seg_6328" s="T489">ol-ttAn</ta>
            <ta e="T491" id="Seg_6329" s="T490">orduk-nI</ta>
            <ta e="T492" id="Seg_6330" s="T491">da</ta>
            <ta e="T493" id="Seg_6331" s="T492">gini</ta>
            <ta e="T494" id="Seg_6332" s="T493">bil-Ar-tA</ta>
            <ta e="T495" id="Seg_6333" s="T494">bu͡ol-IAK.[tA]</ta>
            <ta e="T496" id="Seg_6334" s="T495">du͡o</ta>
            <ta e="T497" id="Seg_6335" s="T496">dʼe</ta>
            <ta e="T498" id="Seg_6336" s="T497">oččogo</ta>
            <ta e="T499" id="Seg_6337" s="T498">mohuj-An</ta>
            <ta e="T500" id="Seg_6338" s="T499">ɨjɨt-IAK.[tA]</ta>
            <ta e="T501" id="Seg_6339" s="T500">beje-ŋ</ta>
            <ta e="T502" id="Seg_6340" s="T501">öj-GI-nAn</ta>
            <ta e="T503" id="Seg_6341" s="T502">gɨn-BA-TI-ŋ</ta>
            <ta e="T504" id="Seg_6342" s="T503">bu͡olu͡o</ta>
            <ta e="T505" id="Seg_6343" s="T504">iti-ččA</ta>
            <ta e="T506" id="Seg_6344" s="T505">öj-LAːK-I-ŋ</ta>
            <ta e="T507" id="Seg_6345" s="T506">bu͡ol-TAR</ta>
            <ta e="T508" id="Seg_6346" s="T507">min</ta>
            <ta e="T509" id="Seg_6347" s="T508">iːk-BI-n-haːk-BI-n</ta>
            <ta e="T510" id="Seg_6348" s="T509">ɨraːstaː-A</ta>
            <ta e="T511" id="Seg_6349" s="T510">iŋ-An-bat-An</ta>
            <ta e="T512" id="Seg_6350" s="T511">hɨt-IAK.[tA]</ta>
            <ta e="T513" id="Seg_6351" s="T512">hu͡ok</ta>
            <ta e="T514" id="Seg_6352" s="T513">e-TI-ŋ</ta>
            <ta e="T515" id="Seg_6353" s="T514">kim</ta>
            <ta e="T516" id="Seg_6354" s="T515">ü͡öret-Ar</ta>
            <ta e="T517" id="Seg_6355" s="T516">di͡e-IAK-tA</ta>
            <ta e="T518" id="Seg_6356" s="T517">onno</ta>
            <ta e="T519" id="Seg_6357" s="T518">kisteː-AːjA-GIn</ta>
            <ta e="T520" id="Seg_6358" s="T519">di͡e-Ar</ta>
            <ta e="T521" id="Seg_6359" s="T520">kɨːs-tA</ta>
            <ta e="T522" id="Seg_6360" s="T521">kuhagan</ta>
            <ta e="T523" id="Seg_6361" s="T522">kɨːs</ta>
            <ta e="T524" id="Seg_6362" s="T523">ogo-LAːK-BIn</ta>
            <ta e="T525" id="Seg_6363" s="T524">ol</ta>
            <ta e="T526" id="Seg_6364" s="T525">ü͡öret-AːktAː-Ar</ta>
            <ta e="T527" id="Seg_6365" s="T526">di͡e-Aːr</ta>
            <ta e="T528" id="Seg_6366" s="T527">ogonnʼor</ta>
            <ta e="T529" id="Seg_6367" s="T528">harsi͡erda</ta>
            <ta e="T530" id="Seg_6368" s="T529">bar-Ar</ta>
            <ta e="T531" id="Seg_6369" s="T530">u͡ol</ta>
            <ta e="T532" id="Seg_6370" s="T531">ɨraːktaːgɨ</ta>
            <ta e="T533" id="Seg_6371" s="T532">ɨjɨt-Ar</ta>
            <ta e="T534" id="Seg_6372" s="T533">kaja</ta>
            <ta e="T535" id="Seg_6373" s="T534">taːj-TI-ŋ</ta>
            <ta e="T536" id="Seg_6374" s="T535">du͡o</ta>
            <ta e="T537" id="Seg_6375" s="T536">ogonnʼor</ta>
            <ta e="T538" id="Seg_6376" s="T537">koru͡oba</ta>
            <ta e="T539" id="Seg_6377" s="T538">tunʼak-tI-n</ta>
            <ta e="T540" id="Seg_6378" s="T539">tugul-tI-n</ta>
            <ta e="T541" id="Seg_6379" s="T540">ostu͡ol-GA</ta>
            <ta e="T542" id="Seg_6380" s="T541">ogo</ta>
            <ta e="T543" id="Seg_6381" s="T542">oːnnʼuːr-tI-n</ta>
            <ta e="T544" id="Seg_6382" s="T543">kurduk</ta>
            <ta e="T545" id="Seg_6383" s="T544">huburut-AlAː-An</ta>
            <ta e="T546" id="Seg_6384" s="T545">keːs-Ar</ta>
            <ta e="T547" id="Seg_6385" s="T546">irgek</ta>
            <ta e="T548" id="Seg_6386" s="T547">koru͡oba-ttAn</ta>
            <ta e="T549" id="Seg_6387" s="T548">töröː-Ar</ta>
            <ta e="T550" id="Seg_6388" s="T549">agaj</ta>
            <ta e="T551" id="Seg_6389" s="T550">hir-tI-n</ta>
            <ta e="T552" id="Seg_6390" s="T551">bu</ta>
            <ta e="T553" id="Seg_6391" s="T552">bul-TI-m</ta>
            <ta e="T554" id="Seg_6392" s="T553">di͡e-Ar</ta>
            <ta e="T555" id="Seg_6393" s="T554">ɨraːktaːgɨ</ta>
            <ta e="T556" id="Seg_6394" s="T555">beje-ŋ</ta>
            <ta e="T557" id="Seg_6395" s="T556">öj-GI-nAn</ta>
            <ta e="T558" id="Seg_6396" s="T557">gɨn-BA-TI-ŋ</ta>
            <ta e="T559" id="Seg_6397" s="T558">bu͡olu͡o</ta>
            <ta e="T560" id="Seg_6398" s="T559">iti-ččA</ta>
            <ta e="T561" id="Seg_6399" s="T560">öj-LAːK-I-ŋ</ta>
            <ta e="T562" id="Seg_6400" s="T561">bu͡ol-TAR</ta>
            <ta e="T563" id="Seg_6401" s="T562">min</ta>
            <ta e="T564" id="Seg_6402" s="T563">iːk-BI-n-haːk-BI-n</ta>
            <ta e="T565" id="Seg_6403" s="T564">ɨraːstaː-A</ta>
            <ta e="T566" id="Seg_6404" s="T565">iŋ-An-bat-An</ta>
            <ta e="T567" id="Seg_6405" s="T566">hɨt-IAK.[tA]</ta>
            <ta e="T568" id="Seg_6406" s="T567">hu͡ok</ta>
            <ta e="T569" id="Seg_6407" s="T568">e-TI-ŋ</ta>
            <ta e="T570" id="Seg_6408" s="T569">kim</ta>
            <ta e="T571" id="Seg_6409" s="T570">ü͡öret-Ar</ta>
            <ta e="T572" id="Seg_6410" s="T571">di͡e-Ar</ta>
            <ta e="T573" id="Seg_6411" s="T572">kuhagan</ta>
            <ta e="T574" id="Seg_6412" s="T573">kɨːs</ta>
            <ta e="T575" id="Seg_6413" s="T574">ogo-LAːK-BIn</ta>
            <ta e="T576" id="Seg_6414" s="T575">ol</ta>
            <ta e="T577" id="Seg_6415" s="T576">ü͡öret-AːktAː-Ar</ta>
            <ta e="T578" id="Seg_6416" s="T577">di͡e-Ar</ta>
            <ta e="T579" id="Seg_6417" s="T578">ogonnʼor</ta>
            <ta e="T580" id="Seg_6418" s="T579">u͡ol</ta>
            <ta e="T581" id="Seg_6419" s="T580">ɨraːktaːgɨ</ta>
            <ta e="T582" id="Seg_6420" s="T581">kɨːs-I-ŋ</ta>
            <ta e="T583" id="Seg_6421" s="T582">öj-LAːK</ta>
            <ta e="T584" id="Seg_6422" s="T583">kɨːs</ta>
            <ta e="T585" id="Seg_6423" s="T584">töröː-BIT</ta>
            <ta e="T586" id="Seg_6424" s="T585">di͡e-Ar</ta>
            <ta e="T587" id="Seg_6425" s="T586">timir</ta>
            <ta e="T588" id="Seg_6426" s="T587">kokoroːk-nI</ta>
            <ta e="T589" id="Seg_6427" s="T588">tügek-tA</ta>
            <ta e="T590" id="Seg_6428" s="T589">kajagas-LAːK-nI</ta>
            <ta e="T591" id="Seg_6429" s="T590">bi͡er-Ar</ta>
            <ta e="T592" id="Seg_6430" s="T591">bu-nI</ta>
            <ta e="T593" id="Seg_6431" s="T592">hamaː-An</ta>
            <ta e="T594" id="Seg_6432" s="T593">ɨːt-TIn</ta>
            <ta e="T595" id="Seg_6433" s="T594">di͡e-Ar</ta>
            <ta e="T596" id="Seg_6434" s="T595">ol-nI</ta>
            <ta e="T597" id="Seg_6435" s="T596">hamaː-BA-TAK-InA</ta>
            <ta e="T598" id="Seg_6436" s="T597">ikki-IAn-GItI-n</ta>
            <ta e="T599" id="Seg_6437" s="T598">töbö-GItI-n</ta>
            <ta e="T600" id="Seg_6438" s="T599">bɨs-IAK-m</ta>
            <ta e="T601" id="Seg_6439" s="T600">di͡e-Ar</ta>
            <ta e="T602" id="Seg_6440" s="T601">ogonnʼor</ta>
            <ta e="T603" id="Seg_6441" s="T602">kokoroːk-nI</ta>
            <ta e="T604" id="Seg_6442" s="T603">ilt-A</ta>
            <ta e="T605" id="Seg_6443" s="T604">bar-BIT-tA</ta>
            <ta e="T606" id="Seg_6444" s="T605">ɨtaː-A-ɨtaː-A</ta>
            <ta e="T607" id="Seg_6445" s="T606">kɨːs</ta>
            <ta e="T608" id="Seg_6446" s="T607">kaja</ta>
            <ta e="T609" id="Seg_6447" s="T608">aga</ta>
            <ta e="T610" id="Seg_6448" s="T609">tu͡ok-I-ŋ</ta>
            <ta e="T611" id="Seg_6449" s="T610">kokoroːk-tA=Ij</ta>
            <ta e="T612" id="Seg_6450" s="T611">di͡e-Ar</ta>
            <ta e="T613" id="Seg_6451" s="T612">kaja-kuː</ta>
            <ta e="T614" id="Seg_6452" s="T613">dʼe</ta>
            <ta e="T615" id="Seg_6453" s="T614">ölör-Ar</ta>
            <ta e="T616" id="Seg_6454" s="T615">kün-tA</ta>
            <ta e="T617" id="Seg_6455" s="T616">kel-TI-tA</ta>
            <ta e="T618" id="Seg_6456" s="T617">bu</ta>
            <ta e="T619" id="Seg_6457" s="T618">kokoroːk-nI</ta>
            <ta e="T620" id="Seg_6458" s="T619">hamaː-An</ta>
            <ta e="T621" id="Seg_6459" s="T620">ɨːt-TIn</ta>
            <ta e="T622" id="Seg_6460" s="T621">di͡e-Ar</ta>
            <ta e="T623" id="Seg_6461" s="T622">bu-nI</ta>
            <ta e="T624" id="Seg_6462" s="T623">hamaː-BA-TAK-InA</ta>
            <ta e="T625" id="Seg_6463" s="T624">ikki-IAn-GItI-n</ta>
            <ta e="T626" id="Seg_6464" s="T625">töbö-GItI-n</ta>
            <ta e="T627" id="Seg_6465" s="T626">bɨs-IAK-m</ta>
            <ta e="T628" id="Seg_6466" s="T627">di͡e-Ar</ta>
            <ta e="T629" id="Seg_6467" s="T628">kɨːs</ta>
            <ta e="T630" id="Seg_6468" s="T629">di͡e-Ar</ta>
            <ta e="T631" id="Seg_6469" s="T630">ilt</ta>
            <ta e="T632" id="Seg_6470" s="T631">töttörü</ta>
            <ta e="T633" id="Seg_6471" s="T632">dʼaktar</ta>
            <ta e="T634" id="Seg_6472" s="T633">kihi</ta>
            <ta e="T635" id="Seg_6473" s="T634">hamaː-IAK-m</ta>
            <ta e="T636" id="Seg_6474" s="T635">er</ta>
            <ta e="T637" id="Seg_6475" s="T636">kihi</ta>
            <ta e="T638" id="Seg_6476" s="T637">kokoroːk-nI</ta>
            <ta e="T639" id="Seg_6477" s="T638">čorčokoːt</ta>
            <ta e="T640" id="Seg_6478" s="T639">kördük</ta>
            <ta e="T641" id="Seg_6479" s="T640">ti͡er-An</ta>
            <ta e="T642" id="Seg_6480" s="T641">ɨːt-TIn</ta>
            <ta e="T643" id="Seg_6481" s="T642">ogonnʼor</ta>
            <ta e="T644" id="Seg_6482" s="T643">ilt-An</ta>
            <ta e="T645" id="Seg_6483" s="T644">bi͡er-Ar</ta>
            <ta e="T646" id="Seg_6484" s="T645">u͡ol</ta>
            <ta e="T647" id="Seg_6485" s="T646">ɨraːktaːgɨ</ta>
            <ta e="T648" id="Seg_6486" s="T647">hök-Ar</ta>
            <ta e="T649" id="Seg_6487" s="T648">ogonnʼor-nI</ta>
            <ta e="T650" id="Seg_6488" s="T649">ɨːt-Ar</ta>
            <ta e="T651" id="Seg_6489" s="T650">beje-m</ta>
            <ta e="T652" id="Seg_6490" s="T651">kɨːs-GI-n</ta>
            <ta e="T653" id="Seg_6491" s="T652">kör-A</ta>
            <ta e="T654" id="Seg_6492" s="T653">bar-IAK-m</ta>
            <ta e="T655" id="Seg_6493" s="T654">di͡e-Ar</ta>
            <ta e="T656" id="Seg_6494" s="T655">u͡ol</ta>
            <ta e="T657" id="Seg_6495" s="T656">ɨraːktaːgɨ</ta>
            <ta e="T658" id="Seg_6496" s="T657">kɨːs-nI</ta>
            <ta e="T659" id="Seg_6497" s="T658">kör-AːT</ta>
            <ta e="T660" id="Seg_6498" s="T659">bagar-Ar</ta>
            <ta e="T661" id="Seg_6499" s="T660">dʼaktar</ta>
            <ta e="T662" id="Seg_6500" s="T661">ɨl-Ar</ta>
            <ta e="T663" id="Seg_6501" s="T662">töhö-nI-kačča-nI</ta>
            <ta e="T664" id="Seg_6502" s="T663">olor-BIT-LArA</ta>
            <ta e="T665" id="Seg_6503" s="T664">bu͡ol-TI-tA</ta>
            <ta e="T666" id="Seg_6504" s="T665">dʼaktar-LAN-BIT-tI-ttAn</ta>
            <ta e="T667" id="Seg_6505" s="T666">u͡ol</ta>
            <ta e="T668" id="Seg_6506" s="T667">ɨraːktaːgɨ</ta>
            <ta e="T669" id="Seg_6507" s="T668">kukla</ta>
            <ta e="T670" id="Seg_6508" s="T669">kurduk</ta>
            <ta e="T671" id="Seg_6509" s="T670">olor-čAk</ta>
            <ta e="T672" id="Seg_6510" s="T671">bu͡ol-TI-tA</ta>
            <ta e="T673" id="Seg_6511" s="T672">barɨ-tI-n</ta>
            <ta e="T674" id="Seg_6512" s="T673">dʼaktar-tA</ta>
            <ta e="T675" id="Seg_6513" s="T674">hübeleː-Ar</ta>
            <ta e="T676" id="Seg_6514" s="T675">bɨhaːr-Ar</ta>
            <ta e="T677" id="Seg_6515" s="T676">u͡ol</ta>
            <ta e="T678" id="Seg_6516" s="T677">ɨraːktaːgɨ</ta>
            <ta e="T679" id="Seg_6517" s="T678">tɨl-LAN-Ar</ta>
            <ta e="T680" id="Seg_6518" s="T679">dogor</ta>
            <ta e="T681" id="Seg_6519" s="T680">bu</ta>
            <ta e="T682" id="Seg_6520" s="T681">biːr</ta>
            <ta e="T683" id="Seg_6521" s="T682">dojdu-GA</ta>
            <ta e="T684" id="Seg_6522" s="T683">ikki</ta>
            <ta e="T685" id="Seg_6523" s="T684">ɨraːktaːgɨ</ta>
            <ta e="T686" id="Seg_6524" s="T685">bu͡ol-Ar-BIt</ta>
            <ta e="T687" id="Seg_6525" s="T686">tabɨlɨn-I-BAT</ta>
            <ta e="T688" id="Seg_6526" s="T687">bihigi</ta>
            <ta e="T689" id="Seg_6527" s="T688">aragɨs-IAk</ta>
            <ta e="T690" id="Seg_6528" s="T689">dʼaktar-tA</ta>
            <ta e="T691" id="Seg_6529" s="T690">dʼe</ta>
            <ta e="T692" id="Seg_6530" s="T691">aragɨs-IAk</ta>
            <ta e="T693" id="Seg_6531" s="T692">du͡o</ta>
            <ta e="T694" id="Seg_6532" s="T693">di͡e-Ar</ta>
            <ta e="T695" id="Seg_6533" s="T694">u͡ol</ta>
            <ta e="T696" id="Seg_6534" s="T695">ɨraːktaːgɨ</ta>
            <ta e="T697" id="Seg_6535" s="T696">tu͡ok</ta>
            <ta e="T698" id="Seg_6536" s="T697">baga-LAːK</ta>
            <ta e="T699" id="Seg_6537" s="T698">hir-GI-n</ta>
            <ta e="T700" id="Seg_6538" s="T699">barɨ-tI-n</ta>
            <ta e="T701" id="Seg_6539" s="T700">ɨl</ta>
            <ta e="T702" id="Seg_6540" s="T701">di͡e-Ar</ta>
            <ta e="T703" id="Seg_6541" s="T702">dʼaktar</ta>
            <ta e="T704" id="Seg_6542" s="T703">höbüleː-A-s-Ar</ta>
            <ta e="T705" id="Seg_6543" s="T704">aragɨs-Ar</ta>
            <ta e="T706" id="Seg_6544" s="T705">tüːn-LArA=keː</ta>
            <ta e="T707" id="Seg_6545" s="T706">bu͡ol-Ar</ta>
            <ta e="T708" id="Seg_6546" s="T707">er-tA</ta>
            <ta e="T709" id="Seg_6547" s="T708">utuj-An</ta>
            <ta e="T710" id="Seg_6548" s="T709">kaːl-Ar</ta>
            <ta e="T711" id="Seg_6549" s="T710">dʼaktar</ta>
            <ta e="T712" id="Seg_6550" s="T711">er-tI-n</ta>
            <ta e="T713" id="Seg_6551" s="T712">hu͡organ-LAr-nI</ta>
            <ta e="T714" id="Seg_6552" s="T713">tellek-LAr-nI</ta>
            <ta e="T715" id="Seg_6553" s="T714">aga-tI-n</ta>
            <ta e="T716" id="Seg_6554" s="T715">bu͡or</ta>
            <ta e="T717" id="Seg_6555" s="T716">golomo</ta>
            <ta e="T718" id="Seg_6556" s="T717">dʼi͡e-tI-GAr</ta>
            <ta e="T719" id="Seg_6557" s="T718">kötök-An</ta>
            <ta e="T720" id="Seg_6558" s="T719">ilt-Ar</ta>
            <ta e="T721" id="Seg_6559" s="T720">u͡ol</ta>
            <ta e="T722" id="Seg_6560" s="T721">ɨraːktaːgɨ</ta>
            <ta e="T723" id="Seg_6561" s="T722">harsi͡erda</ta>
            <ta e="T724" id="Seg_6562" s="T723">uhugun-An</ta>
            <ta e="T725" id="Seg_6563" s="T724">hohuj-Ar</ta>
            <ta e="T726" id="Seg_6564" s="T725">tɨːlɨn-An</ta>
            <ta e="T727" id="Seg_6565" s="T726">kör-BIT-tA</ta>
            <ta e="T728" id="Seg_6566" s="T727">bas-tA-atak-tA</ta>
            <ta e="T729" id="Seg_6567" s="T728">golomo</ta>
            <ta e="T730" id="Seg_6568" s="T729">mas-tI-GAr</ta>
            <ta e="T731" id="Seg_6569" s="T730">kep-Ar-LAr</ta>
            <ta e="T732" id="Seg_6570" s="T731">kaja-keː</ta>
            <ta e="T733" id="Seg_6571" s="T732">bu</ta>
            <ta e="T734" id="Seg_6572" s="T733">kanna</ta>
            <ta e="T735" id="Seg_6573" s="T734">baːr-BIt=Ij</ta>
            <ta e="T736" id="Seg_6574" s="T735">togo</ta>
            <ta e="T737" id="Seg_6575" s="T736">kel-BIT-I-m=Ij</ta>
            <ta e="T738" id="Seg_6576" s="T737">dʼaktar-tI-n</ta>
            <ta e="T739" id="Seg_6577" s="T738">kep-IAlAː-An</ta>
            <ta e="T740" id="Seg_6578" s="T739">uhugun-TAr-Ar</ta>
            <ta e="T741" id="Seg_6579" s="T740">dʼaktar-tA</ta>
            <ta e="T742" id="Seg_6580" s="T741">kaja</ta>
            <ta e="T743" id="Seg_6581" s="T742">dogor</ta>
            <ta e="T744" id="Seg_6582" s="T743">min</ta>
            <ta e="T745" id="Seg_6583" s="T744">egel-BIT-I-m</ta>
            <ta e="T746" id="Seg_6584" s="T745">di͡e-Ar</ta>
            <ta e="T747" id="Seg_6585" s="T746">togo</ta>
            <ta e="T748" id="Seg_6586" s="T747">egel-BIT-GIn=Ij</ta>
            <ta e="T749" id="Seg_6587" s="T748">tɨːj</ta>
            <ta e="T750" id="Seg_6588" s="T749">beje-GI-n</ta>
            <ta e="T751" id="Seg_6589" s="T750">bagar-TI-m</ta>
            <ta e="T752" id="Seg_6590" s="T751">da</ta>
            <ta e="T753" id="Seg_6591" s="T752">beje-GI-n</ta>
            <ta e="T754" id="Seg_6592" s="T753">egel-TI-m</ta>
            <ta e="T755" id="Seg_6593" s="T754">tu͡ok-nI</ta>
            <ta e="T756" id="Seg_6594" s="T755">bagar-Ar-GI-n</ta>
            <ta e="T757" id="Seg_6595" s="T756">ɨl-Aːr</ta>
            <ta e="T758" id="Seg_6596" s="T757">di͡e-BIT-I-ŋ</ta>
            <ta e="T759" id="Seg_6597" s="T758">diː</ta>
            <ta e="T760" id="Seg_6598" s="T759">di͡e-Ar</ta>
            <ta e="T761" id="Seg_6599" s="T760">dʼaktar</ta>
            <ta e="T762" id="Seg_6600" s="T761">u͡ol</ta>
            <ta e="T763" id="Seg_6601" s="T762">ɨraːktaːgɨ</ta>
            <ta e="T764" id="Seg_6602" s="T763">kɨ͡aj-An</ta>
            <ta e="T765" id="Seg_6603" s="T764">araːk-BA-tI-tA</ta>
            <ta e="T766" id="Seg_6604" s="T765">dʼe</ta>
            <ta e="T767" id="Seg_6605" s="T766">dʼaktar-tA</ta>
            <ta e="T768" id="Seg_6606" s="T767">di͡e-Ar</ta>
            <ta e="T769" id="Seg_6607" s="T768">oččo</ta>
            <ta e="T770" id="Seg_6608" s="T769">öj-LAːK</ta>
            <ta e="T771" id="Seg_6609" s="T770">kihi</ta>
            <ta e="T772" id="Seg_6610" s="T771">bu͡ol-BAtAK</ta>
            <ta e="T773" id="Seg_6611" s="T772">e-BIT-GIn</ta>
            <ta e="T774" id="Seg_6612" s="T773">dʼon-GI-n</ta>
            <ta e="T775" id="Seg_6613" s="T774">hübeleː-An</ta>
            <ta e="T776" id="Seg_6614" s="T775">beje-ŋ</ta>
            <ta e="T777" id="Seg_6615" s="T776">olor</ta>
            <ta e="T778" id="Seg_6616" s="T777">min</ta>
            <ta e="T779" id="Seg_6617" s="T778">könnörü</ta>
            <ta e="T780" id="Seg_6618" s="T779">küheːjke</ta>
            <ta e="T781" id="Seg_6619" s="T780">bu͡ol-An</ta>
            <ta e="T782" id="Seg_6620" s="T781">olor-IAK-m</ta>
            <ta e="T783" id="Seg_6621" s="T782">umnahɨt</ta>
            <ta e="T784" id="Seg_6622" s="T783">paːsɨnaj</ta>
            <ta e="T785" id="Seg_6623" s="T784">kɨːs-tI-n</ta>
            <ta e="T786" id="Seg_6624" s="T785">ötü͡ö-tI-nAn</ta>
            <ta e="T787" id="Seg_6625" s="T786">hi͡erkile</ta>
            <ta e="T788" id="Seg_6626" s="T787">taːs</ta>
            <ta e="T789" id="Seg_6627" s="T788">dʼi͡e-GA</ta>
            <ta e="T790" id="Seg_6628" s="T789">olor-TAK-tA</ta>
            <ta e="T791" id="Seg_6629" s="T790">bu͡or</ta>
            <ta e="T792" id="Seg_6630" s="T791">golomo-ttAn</ta>
            <ta e="T793" id="Seg_6631" s="T792">aragɨs-An</ta>
            <ta e="T794" id="Seg_6632" s="T793">uhuk-I-ttAn</ta>
            <ta e="T795" id="Seg_6633" s="T794">baːj-An-baːj-An</ta>
            <ta e="T796" id="Seg_6634" s="T795">olor-TAK-LArA</ta>
            <ta e="T797" id="Seg_6635" s="T796">dʼe</ta>
            <ta e="T798" id="Seg_6636" s="T797">ele-tA</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_6637" s="T0">well</ta>
            <ta e="T2" id="Seg_6638" s="T1">boy.[NOM]</ta>
            <ta e="T3" id="Seg_6639" s="T2">czar.[NOM]</ta>
            <ta e="T4" id="Seg_6640" s="T3">live-PST2-3SG</ta>
            <ta e="T5" id="Seg_6641" s="T4">neighbourhood-3SG-DAT/LOC</ta>
            <ta e="T6" id="Seg_6642" s="T5">poor</ta>
            <ta e="T7" id="Seg_6643" s="T6">farmer-PROPR.[NOM]</ta>
            <ta e="T8" id="Seg_6644" s="T7">poor</ta>
            <ta e="T9" id="Seg_6645" s="T8">farmer.[NOM]</ta>
            <ta e="T10" id="Seg_6646" s="T9">this</ta>
            <ta e="T11" id="Seg_6647" s="T10">czar-DAT/LOC</ta>
            <ta e="T12" id="Seg_6648" s="T11">work-CVB.SEQ</ta>
            <ta e="T13" id="Seg_6649" s="T12">morning</ta>
            <ta e="T14" id="Seg_6650" s="T13">dawn-ABL</ta>
            <ta e="T15" id="Seg_6651" s="T14">evening.[NOM]</ta>
            <ta e="T16" id="Seg_6652" s="T15">dawn-DAT/LOC</ta>
            <ta e="T17" id="Seg_6653" s="T16">until</ta>
            <ta e="T18" id="Seg_6654" s="T17">one</ta>
            <ta e="T19" id="Seg_6655" s="T18">spoon.[NOM]</ta>
            <ta e="T20" id="Seg_6656" s="T19">flour.[NOM]</ta>
            <ta e="T21" id="Seg_6657" s="T20">for</ta>
            <ta e="T22" id="Seg_6658" s="T21">work-PTCP.PRS</ta>
            <ta e="T23" id="Seg_6659" s="T22">be-PST2.[3SG]</ta>
            <ta e="T24" id="Seg_6660" s="T23">this-INSTR</ta>
            <ta e="T25" id="Seg_6661" s="T24">feed-REFL-CVB.SEQ</ta>
            <ta e="T26" id="Seg_6662" s="T25">live-PRS-3PL</ta>
            <ta e="T27" id="Seg_6663" s="T26">poor</ta>
            <ta e="T28" id="Seg_6664" s="T27">farmer.[NOM]</ta>
            <ta e="T29" id="Seg_6665" s="T28">one</ta>
            <ta e="T30" id="Seg_6666" s="T29">girl.[NOM]</ta>
            <ta e="T31" id="Seg_6667" s="T30">child-PROPR.[NOM]</ta>
            <ta e="T32" id="Seg_6668" s="T31">well</ta>
            <ta e="T33" id="Seg_6669" s="T32">once</ta>
            <ta e="T34" id="Seg_6670" s="T33">boy.[NOM]</ta>
            <ta e="T35" id="Seg_6671" s="T34">czar.[NOM]</ta>
            <ta e="T36" id="Seg_6672" s="T35">say-PRS.[3SG]</ta>
            <ta e="T37" id="Seg_6673" s="T36">well</ta>
            <ta e="T38" id="Seg_6674" s="T37">well</ta>
            <ta e="T39" id="Seg_6675" s="T38">poor</ta>
            <ta e="T40" id="Seg_6676" s="T39">farmer.[NOM]</ta>
            <ta e="T41" id="Seg_6677" s="T40">how.much</ta>
            <ta e="T42" id="Seg_6678" s="T41">mind-PROPR</ta>
            <ta e="T43" id="Seg_6679" s="T42">human.being.[NOM]</ta>
            <ta e="T44" id="Seg_6680" s="T43">give.birth-CVB.SEQ</ta>
            <ta e="T45" id="Seg_6681" s="T44">come.back-PST1-2SG</ta>
            <ta e="T46" id="Seg_6682" s="T45">riddle-PART</ta>
            <ta e="T47" id="Seg_6683" s="T46">guess.[IMP.2SG]</ta>
            <ta e="T48" id="Seg_6684" s="T47">sweet-ABL</ta>
            <ta e="T49" id="Seg_6685" s="T48">sweet.[NOM]</ta>
            <ta e="T50" id="Seg_6686" s="T49">what.[NOM]=Q</ta>
            <ta e="T51" id="Seg_6687" s="T50">in.the.morning</ta>
            <ta e="T52" id="Seg_6688" s="T51">come-CVB.SEQ</ta>
            <ta e="T53" id="Seg_6689" s="T52">nine</ta>
            <ta e="T54" id="Seg_6690" s="T53">hour-DAT/LOC</ta>
            <ta e="T55" id="Seg_6691" s="T54">speak-FUT.[IMP.2SG]</ta>
            <ta e="T56" id="Seg_6692" s="T55">that-ACC</ta>
            <ta e="T57" id="Seg_6693" s="T56">guess-NEG-TEMP-2SG</ta>
            <ta e="T58" id="Seg_6694" s="T57">hang.up-PTCP.PRS</ta>
            <ta e="T59" id="Seg_6695" s="T58">tree-1SG-DAT/LOC</ta>
            <ta e="T60" id="Seg_6696" s="T59">neck-PROPR</ta>
            <ta e="T61" id="Seg_6697" s="T60">head-2SG-ACC</ta>
            <ta e="T62" id="Seg_6698" s="T61">cut-FUT-1SG</ta>
            <ta e="T63" id="Seg_6699" s="T62">poor</ta>
            <ta e="T64" id="Seg_6700" s="T63">farmer.[NOM]</ta>
            <ta e="T65" id="Seg_6701" s="T64">why</ta>
            <ta e="T66" id="Seg_6702" s="T65">NEG</ta>
            <ta e="T67" id="Seg_6703" s="T66">think.out-CVB.SIM</ta>
            <ta e="T68" id="Seg_6704" s="T67">can-NEG.[3SG]</ta>
            <ta e="T69" id="Seg_6705" s="T68">sweet</ta>
            <ta e="T70" id="Seg_6706" s="T69">food-ACC</ta>
            <ta e="T71" id="Seg_6707" s="T70">know-EMOT-NEG.[3SG]</ta>
            <ta e="T72" id="Seg_6708" s="T71">czar.[NOM]</ta>
            <ta e="T73" id="Seg_6709" s="T72">food-3SG.[NOM]</ta>
            <ta e="T74" id="Seg_6710" s="T73">every-3SG.[NOM]</ta>
            <ta e="T75" id="Seg_6711" s="T74">sweet.[NOM]</ta>
            <ta e="T76" id="Seg_6712" s="T75">like</ta>
            <ta e="T77" id="Seg_6713" s="T76">3SG-DAT/LOC</ta>
            <ta e="T78" id="Seg_6714" s="T77">house-3SG-DAT/LOC</ta>
            <ta e="T79" id="Seg_6715" s="T78">come-CVB.SEQ</ta>
            <ta e="T80" id="Seg_6716" s="T79">cry-EP-MED-CVB.SIM</ta>
            <ta e="T81" id="Seg_6717" s="T80">sit.down-PST1-3SG</ta>
            <ta e="T82" id="Seg_6718" s="T81">daughter-3SG.[NOM]</ta>
            <ta e="T83" id="Seg_6719" s="T82">ask-PRS.[3SG]</ta>
            <ta e="T84" id="Seg_6720" s="T83">well</ta>
            <ta e="T85" id="Seg_6721" s="T84">father</ta>
            <ta e="T86" id="Seg_6722" s="T85">why</ta>
            <ta e="T87" id="Seg_6723" s="T86">cry-PST1-2SG</ta>
            <ta e="T88" id="Seg_6724" s="T87">well</ta>
            <ta e="T89" id="Seg_6725" s="T88">friend-EP-1SG.[NOM]</ta>
            <ta e="T90" id="Seg_6726" s="T89">go-EP-PST2-EP-1SG</ta>
            <ta e="T91" id="Seg_6727" s="T90">just</ta>
            <ta e="T92" id="Seg_6728" s="T91">live-PTCP.PST-EP-1SG.[NOM]</ta>
            <ta e="T93" id="Seg_6729" s="T92">end-3SG.[NOM]</ta>
            <ta e="T94" id="Seg_6730" s="T93">boy.[NOM]</ta>
            <ta e="T95" id="Seg_6731" s="T94">czar.[NOM]</ta>
            <ta e="T96" id="Seg_6732" s="T95">riddle.[NOM]</ta>
            <ta e="T97" id="Seg_6733" s="T96">give-PST1-3SG</ta>
            <ta e="T98" id="Seg_6734" s="T97">realize-NEG-TEMP-1SG</ta>
            <ta e="T99" id="Seg_6735" s="T98">neck-PROPR</ta>
            <ta e="T100" id="Seg_6736" s="T99">head-1SG-ACC</ta>
            <ta e="T101" id="Seg_6737" s="T100">cut-PTCP.FUT</ta>
            <ta e="T102" id="Seg_6738" s="T101">be-PST1-3SG</ta>
            <ta e="T103" id="Seg_6739" s="T102">what-ACC</ta>
            <ta e="T104" id="Seg_6740" s="T103">realize-CAUS-CAUS-PRS.[3SG]</ta>
            <ta e="T105" id="Seg_6741" s="T104">that.[NOM]</ta>
            <ta e="T106" id="Seg_6742" s="T105">sweet-ABL</ta>
            <ta e="T107" id="Seg_6743" s="T106">sweet.[NOM]</ta>
            <ta e="T108" id="Seg_6744" s="T107">what.[NOM]=Q</ta>
            <ta e="T109" id="Seg_6745" s="T108">say-PRS.[3SG]</ta>
            <ta e="T110" id="Seg_6746" s="T109">say-PST2.[3SG]</ta>
            <ta e="T111" id="Seg_6747" s="T110">hey</ta>
            <ta e="T112" id="Seg_6748" s="T111">father</ta>
            <ta e="T113" id="Seg_6749" s="T112">use-POSS</ta>
            <ta e="T114" id="Seg_6750" s="T113">NEG-DAT/LOC</ta>
            <ta e="T115" id="Seg_6751" s="T114">then</ta>
            <ta e="T116" id="Seg_6752" s="T115">cry-PRS-2SG</ta>
            <ta e="T117" id="Seg_6753" s="T116">sleep-EMOT.[IMP.2SG]</ta>
            <ta e="T118" id="Seg_6754" s="T117">relax.[IMP.2SG]</ta>
            <ta e="T119" id="Seg_6755" s="T118">sweet-ABL</ta>
            <ta e="T120" id="Seg_6756" s="T119">sweet-ACC</ta>
            <ta e="T121" id="Seg_6757" s="T120">know-PTCP.PRS</ta>
            <ta e="T122" id="Seg_6758" s="T121">matter-EP-1SG.[NOM]</ta>
            <ta e="T123" id="Seg_6759" s="T122">one</ta>
            <ta e="T124" id="Seg_6760" s="T123">say-FUT.[IMP.2SG]</ta>
            <ta e="T125" id="Seg_6761" s="T124">morning</ta>
            <ta e="T126" id="Seg_6762" s="T125">dawn-ABL</ta>
            <ta e="T127" id="Seg_6763" s="T126">evening</ta>
            <ta e="T128" id="Seg_6764" s="T127">dawn-DAT/LOC</ta>
            <ta e="T129" id="Seg_6765" s="T128">until</ta>
            <ta e="T130" id="Seg_6766" s="T129">work-CVB.SEQ</ta>
            <ta e="T131" id="Seg_6767" s="T130">2SG.[NOM]</ta>
            <ta e="T132" id="Seg_6768" s="T131">faeces-2SG-ACC-dirt-2SG-ACC</ta>
            <ta e="T133" id="Seg_6769" s="T132">clean-CVB.SEQ</ta>
            <ta e="T134" id="Seg_6770" s="T133">one</ta>
            <ta e="T135" id="Seg_6771" s="T134">spoon.[NOM]</ta>
            <ta e="T136" id="Seg_6772" s="T135">flour-ACC</ta>
            <ta e="T137" id="Seg_6773" s="T136">bring-TEMP-1SG</ta>
            <ta e="T138" id="Seg_6774" s="T137">old.woman-EP-1SG.[NOM]</ta>
            <ta e="T139" id="Seg_6775" s="T138">earth.[NOM]</ta>
            <ta e="T140" id="Seg_6776" s="T139">pit_dwelling.[NOM]</ta>
            <ta e="T141" id="Seg_6777" s="T140">house-3SG-ACC</ta>
            <ta e="T142" id="Seg_6778" s="T141">heat-TEMP-3SG</ta>
            <ta e="T143" id="Seg_6779" s="T142">nose-1SG.[NOM]</ta>
            <ta e="T144" id="Seg_6780" s="T143">warm.oneself-PRS.[3SG]</ta>
            <ta e="T145" id="Seg_6781" s="T144">that</ta>
            <ta e="T146" id="Seg_6782" s="T145">flour-DIM-3SG-INSTR</ta>
            <ta e="T147" id="Seg_6783" s="T146">roux.[NOM]</ta>
            <ta e="T148" id="Seg_6784" s="T147">make-TEMP-3SG</ta>
            <ta e="T149" id="Seg_6785" s="T148">eat-NEG.CVB.SIM</ta>
            <ta e="T150" id="Seg_6786" s="T149">and</ta>
            <ta e="T151" id="Seg_6787" s="T150">fall.asleep-PRS-1SG</ta>
            <ta e="T152" id="Seg_6788" s="T151">so</ta>
            <ta e="T153" id="Seg_6789" s="T152">sweet-ABL</ta>
            <ta e="T154" id="Seg_6790" s="T153">sweet.[NOM]</ta>
            <ta e="T155" id="Seg_6791" s="T154">sleep-PTCP.PRS</ta>
            <ta e="T156" id="Seg_6792" s="T155">sleep.[NOM]</ta>
            <ta e="T157" id="Seg_6793" s="T156">be-FUT-NEC.[3SG]</ta>
            <ta e="T158" id="Seg_6794" s="T157">say-FUT.[IMP.2SG]</ta>
            <ta e="T159" id="Seg_6795" s="T158">3SG.[NOM]</ta>
            <ta e="T160" id="Seg_6796" s="T159">that-ABL</ta>
            <ta e="T161" id="Seg_6797" s="T160">and</ta>
            <ta e="T162" id="Seg_6798" s="T161">rest-ACC</ta>
            <ta e="T163" id="Seg_6799" s="T162">know-PTCP.PRS-3SG</ta>
            <ta e="T164" id="Seg_6800" s="T163">be-FUT.[3SG]</ta>
            <ta e="T165" id="Seg_6801" s="T164">Q</ta>
            <ta e="T166" id="Seg_6802" s="T165">say-PST2.[3SG]</ta>
            <ta e="T167" id="Seg_6803" s="T166">daughter-3SG.[NOM]</ta>
            <ta e="T168" id="Seg_6804" s="T167">old.man.[NOM]</ta>
            <ta e="T169" id="Seg_6805" s="T168">fall.asleep-CVB.SEQ</ta>
            <ta e="T170" id="Seg_6806" s="T169">stay-PST1-3SG</ta>
            <ta e="T171" id="Seg_6807" s="T170">in.the.morning</ta>
            <ta e="T172" id="Seg_6808" s="T171">early</ta>
            <ta e="T173" id="Seg_6809" s="T172">stand.up-CVB.SEQ</ta>
            <ta e="T174" id="Seg_6810" s="T173">nine</ta>
            <ta e="T175" id="Seg_6811" s="T174">hour-DAT/LOC</ta>
            <ta e="T176" id="Seg_6812" s="T175">czar-DAT/LOC</ta>
            <ta e="T177" id="Seg_6813" s="T176">reach-PRS.[3SG]</ta>
            <ta e="T178" id="Seg_6814" s="T177">boy.[NOM]</ta>
            <ta e="T179" id="Seg_6815" s="T178">czar.[NOM]</ta>
            <ta e="T180" id="Seg_6816" s="T179">ask-PRS.[3SG]</ta>
            <ta e="T181" id="Seg_6817" s="T180">well</ta>
            <ta e="T182" id="Seg_6818" s="T181">guess-PST1-2SG</ta>
            <ta e="T183" id="Seg_6819" s="T182">Q</ta>
            <ta e="T184" id="Seg_6820" s="T183">hey</ta>
            <ta e="T185" id="Seg_6821" s="T184">realize-PTCP.PRS-DAT/LOC</ta>
            <ta e="T186" id="Seg_6822" s="T185">similar</ta>
            <ta e="T187" id="Seg_6823" s="T186">make-PST1-1SG</ta>
            <ta e="T188" id="Seg_6824" s="T187">right</ta>
            <ta e="T189" id="Seg_6825" s="T188">Q</ta>
            <ta e="T190" id="Seg_6826" s="T189">mistake-3SG.[NOM]</ta>
            <ta e="T191" id="Seg_6827" s="T190">Q</ta>
            <ta e="T192" id="Seg_6828" s="T191">say-PRS.[3SG]</ta>
            <ta e="T193" id="Seg_6829" s="T192">well</ta>
            <ta e="T194" id="Seg_6830" s="T193">what-ACC=Q</ta>
            <ta e="T195" id="Seg_6831" s="T194">morning</ta>
            <ta e="T196" id="Seg_6832" s="T195">dawn-ABL</ta>
            <ta e="T197" id="Seg_6833" s="T196">evening</ta>
            <ta e="T198" id="Seg_6834" s="T197">dawn-DAT/LOC</ta>
            <ta e="T199" id="Seg_6835" s="T198">until</ta>
            <ta e="T200" id="Seg_6836" s="T199">work-CVB.SEQ</ta>
            <ta e="T201" id="Seg_6837" s="T200">2SG.[NOM]</ta>
            <ta e="T202" id="Seg_6838" s="T201">faeces-2SG-ACC-dirt-2SG-ACC</ta>
            <ta e="T203" id="Seg_6839" s="T202">clean-CVB.SEQ</ta>
            <ta e="T204" id="Seg_6840" s="T203">one</ta>
            <ta e="T205" id="Seg_6841" s="T204">spoon.[NOM]</ta>
            <ta e="T206" id="Seg_6842" s="T205">flour-ACC</ta>
            <ta e="T207" id="Seg_6843" s="T206">bring-TEMP-1SG</ta>
            <ta e="T208" id="Seg_6844" s="T207">old.woman-EP-1SG.[NOM]</ta>
            <ta e="T209" id="Seg_6845" s="T208">earth.[NOM]</ta>
            <ta e="T210" id="Seg_6846" s="T209">pit_dwelling.[NOM]</ta>
            <ta e="T211" id="Seg_6847" s="T210">house-3SG-ACC</ta>
            <ta e="T212" id="Seg_6848" s="T211">heat-TEMP-3SG</ta>
            <ta e="T213" id="Seg_6849" s="T212">nose-1SG.[NOM]</ta>
            <ta e="T214" id="Seg_6850" s="T213">warm.oneself-PRS.[3SG]</ta>
            <ta e="T215" id="Seg_6851" s="T214">that</ta>
            <ta e="T216" id="Seg_6852" s="T215">flour-DIM-3SG-INSTR</ta>
            <ta e="T217" id="Seg_6853" s="T216">roux.[NOM]</ta>
            <ta e="T218" id="Seg_6854" s="T217">make-TEMP-3SG</ta>
            <ta e="T219" id="Seg_6855" s="T218">eat-NEG.CVB.SIM</ta>
            <ta e="T220" id="Seg_6856" s="T219">and</ta>
            <ta e="T221" id="Seg_6857" s="T220">fall.asleep-PRS-1SG</ta>
            <ta e="T222" id="Seg_6858" s="T221">so</ta>
            <ta e="T223" id="Seg_6859" s="T222">sweet-ABL</ta>
            <ta e="T224" id="Seg_6860" s="T223">sweet.[NOM]</ta>
            <ta e="T225" id="Seg_6861" s="T224">sleep-PTCP.PRS</ta>
            <ta e="T226" id="Seg_6862" s="T225">sleep.[NOM]</ta>
            <ta e="T227" id="Seg_6863" s="T226">be-FUT-NEC.[3SG]</ta>
            <ta e="T228" id="Seg_6864" s="T227">boy.[NOM]</ta>
            <ta e="T229" id="Seg_6865" s="T228">czar.[NOM]</ta>
            <ta e="T230" id="Seg_6866" s="T229">say-PRS.[3SG]</ta>
            <ta e="T231" id="Seg_6867" s="T230">oh</ta>
            <ta e="T232" id="Seg_6868" s="T231">well</ta>
            <ta e="T233" id="Seg_6869" s="T232">mind-PROPR</ta>
            <ta e="T234" id="Seg_6870" s="T233">human.being-2SG</ta>
            <ta e="T235" id="Seg_6871" s="T234">be-PST2.[3SG]</ta>
            <ta e="T236" id="Seg_6872" s="T235">how</ta>
            <ta e="T237" id="Seg_6873" s="T236">get.saturated-CVB.SEQ-get.along-CVB.SEQ</ta>
            <ta e="T238" id="Seg_6874" s="T237">lie-PRS-2SG</ta>
            <ta e="T239" id="Seg_6875" s="T238">still</ta>
            <ta e="T240" id="Seg_6876" s="T239">guess.[IMP.2SG]</ta>
            <ta e="T241" id="Seg_6877" s="T240">middle</ta>
            <ta e="T242" id="Seg_6878" s="T241">world-DAT/LOC</ta>
            <ta e="T243" id="Seg_6879" s="T242">fast-ABL</ta>
            <ta e="T244" id="Seg_6880" s="T243">fast.[NOM]</ta>
            <ta e="T245" id="Seg_6881" s="T244">what.[NOM]</ta>
            <ta e="T246" id="Seg_6882" s="T245">there.is=Q</ta>
            <ta e="T247" id="Seg_6883" s="T246">in.the.morning</ta>
            <ta e="T248" id="Seg_6884" s="T247">guess-NEG-TEMP-2SG</ta>
            <ta e="T249" id="Seg_6885" s="T248">neck-PROPR</ta>
            <ta e="T250" id="Seg_6886" s="T249">head-2SG-ACC</ta>
            <ta e="T251" id="Seg_6887" s="T250">cut-PRS-1SG</ta>
            <ta e="T252" id="Seg_6888" s="T251">old.man.[NOM]</ta>
            <ta e="T253" id="Seg_6889" s="T252">what-ACC</ta>
            <ta e="T254" id="Seg_6890" s="T253">NEG</ta>
            <ta e="T255" id="Seg_6891" s="T254">solve-NEG.[3SG]</ta>
            <ta e="T256" id="Seg_6892" s="T255">reach-CVB.SEQ</ta>
            <ta e="T257" id="Seg_6893" s="T256">cry-EP-MED-CVB.SIM</ta>
            <ta e="T258" id="Seg_6894" s="T257">sit.down-TEMP-3SG</ta>
            <ta e="T259" id="Seg_6895" s="T258">daughter-3SG.[NOM]</ta>
            <ta e="T260" id="Seg_6896" s="T259">ask-PRS.[3SG]</ta>
            <ta e="T261" id="Seg_6897" s="T260">old.man.[NOM]</ta>
            <ta e="T262" id="Seg_6898" s="T261">so-so</ta>
            <ta e="T263" id="Seg_6899" s="T262">say-PRS.[3SG]</ta>
            <ta e="T264" id="Seg_6900" s="T263">what</ta>
            <ta e="T265" id="Seg_6901" s="T264">use-POSS</ta>
            <ta e="T266" id="Seg_6902" s="T265">NEG-DAT/LOC</ta>
            <ta e="T267" id="Seg_6903" s="T266">cry-PRS-2SG</ta>
            <ta e="T268" id="Seg_6904" s="T267">sleep.[IMP.2SG]</ta>
            <ta e="T269" id="Seg_6905" s="T268">in.the.morning</ta>
            <ta e="T270" id="Seg_6906" s="T269">speak-FUT.[IMP.2SG]</ta>
            <ta e="T271" id="Seg_6907" s="T270">fast-ABL</ta>
            <ta e="T272" id="Seg_6908" s="T271">fast.[NOM]</ta>
            <ta e="T273" id="Seg_6909" s="T272">say-FUT.[IMP.2SG]</ta>
            <ta e="T274" id="Seg_6910" s="T273">bad</ta>
            <ta e="T275" id="Seg_6911" s="T274">smoke-PROPR</ta>
            <ta e="T276" id="Seg_6912" s="T275">earth.[NOM]</ta>
            <ta e="T277" id="Seg_6913" s="T276">pit_dwelling-1SG-ABL</ta>
            <ta e="T278" id="Seg_6914" s="T277">go.out-CVB.SIM</ta>
            <ta e="T279" id="Seg_6915" s="T278">run-CVB.SEQ</ta>
            <ta e="T280" id="Seg_6916" s="T279">eye-1SG-ACC</ta>
            <ta e="T281" id="Seg_6917" s="T280">poke-CAUS-EP-INTNS-EP-INTNS-CVB.SIM</ta>
            <ta e="T282" id="Seg_6918" s="T281">wipe-CVB.SEQ</ta>
            <ta e="T283" id="Seg_6919" s="T282">watch-CVB.SEQ</ta>
            <ta e="T284" id="Seg_6920" s="T283">see-TEMP-1SG</ta>
            <ta e="T285" id="Seg_6921" s="T284">sky.[NOM]</ta>
            <ta e="T286" id="Seg_6922" s="T285">to</ta>
            <ta e="T287" id="Seg_6923" s="T286">raise.the.head-TEMP-1SG</ta>
            <ta e="T288" id="Seg_6924" s="T287">sky.[NOM]</ta>
            <ta e="T289" id="Seg_6925" s="T288">star-3SG-ACC</ta>
            <ta e="T290" id="Seg_6926" s="T289">eye-EP-1SG.[NOM]</ta>
            <ta e="T291" id="Seg_6927" s="T290">all.without.exception</ta>
            <ta e="T292" id="Seg_6928" s="T291">see-PRS.[3SG]</ta>
            <ta e="T293" id="Seg_6929" s="T292">earth.[NOM]</ta>
            <ta e="T294" id="Seg_6930" s="T293">to</ta>
            <ta e="T295" id="Seg_6931" s="T294">see-TEMP-1SG</ta>
            <ta e="T296" id="Seg_6932" s="T295">earth.[NOM]</ta>
            <ta e="T297" id="Seg_6933" s="T296">grass-3SG-ACC-tree-3SG-ACC</ta>
            <ta e="T298" id="Seg_6934" s="T297">all.without.exception</ta>
            <ta e="T299" id="Seg_6935" s="T298">see-PRS-1SG</ta>
            <ta e="T300" id="Seg_6936" s="T299">eye-EP-1SG.[NOM]</ta>
            <ta e="T301" id="Seg_6937" s="T300">take-PTCP.PRS-EP-DAT/LOC</ta>
            <ta e="T302" id="Seg_6938" s="T301">when</ta>
            <ta e="T303" id="Seg_6939" s="T302">NEG</ta>
            <ta e="T304" id="Seg_6940" s="T303">reach-NEG-1SG</ta>
            <ta e="T305" id="Seg_6941" s="T304">so</ta>
            <ta e="T306" id="Seg_6942" s="T305">fast-ABL</ta>
            <ta e="T307" id="Seg_6943" s="T306">fast.[NOM]</ta>
            <ta e="T308" id="Seg_6944" s="T307">human.being.[NOM]</ta>
            <ta e="T309" id="Seg_6945" s="T308">see-NMNZ-3SG.[NOM]</ta>
            <ta e="T310" id="Seg_6946" s="T309">say-FUT.[IMP.2SG]</ta>
            <ta e="T311" id="Seg_6947" s="T310">that-ABL</ta>
            <ta e="T312" id="Seg_6948" s="T311">rest-ACC</ta>
            <ta e="T313" id="Seg_6949" s="T312">3SG.[NOM]</ta>
            <ta e="T314" id="Seg_6950" s="T313">know-PTCP.PRS-3SG</ta>
            <ta e="T315" id="Seg_6951" s="T314">be-FUT.[3SG]</ta>
            <ta e="T316" id="Seg_6952" s="T315">Q</ta>
            <ta e="T317" id="Seg_6953" s="T316">say-PRS.[3SG]</ta>
            <ta e="T318" id="Seg_6954" s="T317">daughter.[NOM]</ta>
            <ta e="T319" id="Seg_6955" s="T318">old.man.[NOM]</ta>
            <ta e="T320" id="Seg_6956" s="T319">fall.asleep-CVB.SEQ</ta>
            <ta e="T321" id="Seg_6957" s="T320">stay-PST1-3SG</ta>
            <ta e="T322" id="Seg_6958" s="T321">morning</ta>
            <ta e="T323" id="Seg_6959" s="T322">stand.up-CVB.SEQ</ta>
            <ta e="T324" id="Seg_6960" s="T323">czar-3SG-DAT/LOC</ta>
            <ta e="T325" id="Seg_6961" s="T324">go-INFER-3SG</ta>
            <ta e="T326" id="Seg_6962" s="T325">boy.[NOM]</ta>
            <ta e="T327" id="Seg_6963" s="T326">czar.[NOM]</ta>
            <ta e="T328" id="Seg_6964" s="T327">ask-PRS.[3SG]</ta>
            <ta e="T329" id="Seg_6965" s="T328">well</ta>
            <ta e="T330" id="Seg_6966" s="T329">realize-PST1-2SG</ta>
            <ta e="T331" id="Seg_6967" s="T330">Q</ta>
            <ta e="T332" id="Seg_6968" s="T331">hey</ta>
            <ta e="T333" id="Seg_6969" s="T332">realize-PTCP.PRS-DAT/LOC</ta>
            <ta e="T334" id="Seg_6970" s="T333">similar</ta>
            <ta e="T335" id="Seg_6971" s="T334">make-PST1-1SG</ta>
            <ta e="T336" id="Seg_6972" s="T335">right</ta>
            <ta e="T337" id="Seg_6973" s="T336">Q</ta>
            <ta e="T338" id="Seg_6974" s="T337">mistake-3SG.[NOM]</ta>
            <ta e="T339" id="Seg_6975" s="T338">Q</ta>
            <ta e="T340" id="Seg_6976" s="T339">say-PRS.[3SG]</ta>
            <ta e="T341" id="Seg_6977" s="T340">bad</ta>
            <ta e="T342" id="Seg_6978" s="T341">smoke-PROPR</ta>
            <ta e="T343" id="Seg_6979" s="T342">earth.[NOM]</ta>
            <ta e="T344" id="Seg_6980" s="T343">pit_dwelling-1SG-ABL</ta>
            <ta e="T345" id="Seg_6981" s="T344">go.out-CVB.SIM</ta>
            <ta e="T346" id="Seg_6982" s="T345">run-CVB.SEQ</ta>
            <ta e="T347" id="Seg_6983" s="T346">eye-1SG-ACC</ta>
            <ta e="T348" id="Seg_6984" s="T347">poke-CAUS-EP-INTNS-EP-INTNS-CVB.SIM</ta>
            <ta e="T349" id="Seg_6985" s="T348">wipe-CVB.SEQ</ta>
            <ta e="T350" id="Seg_6986" s="T349">seeing-VBZ-CVB.SEQ</ta>
            <ta e="T351" id="Seg_6987" s="T350">see-TEMP-1SG</ta>
            <ta e="T352" id="Seg_6988" s="T351">sky.[NOM]</ta>
            <ta e="T353" id="Seg_6989" s="T352">to</ta>
            <ta e="T354" id="Seg_6990" s="T353">raise.the.head-TEMP-1SG</ta>
            <ta e="T355" id="Seg_6991" s="T354">sky.[NOM]</ta>
            <ta e="T356" id="Seg_6992" s="T355">star-3SG-ACC</ta>
            <ta e="T357" id="Seg_6993" s="T356">eye-EP-1SG.[NOM]</ta>
            <ta e="T358" id="Seg_6994" s="T357">all.without.exception</ta>
            <ta e="T359" id="Seg_6995" s="T358">see-PRS.[3SG]</ta>
            <ta e="T360" id="Seg_6996" s="T359">earth.[NOM]</ta>
            <ta e="T361" id="Seg_6997" s="T360">to</ta>
            <ta e="T362" id="Seg_6998" s="T361">see-TEMP-1SG</ta>
            <ta e="T363" id="Seg_6999" s="T362">earth.[NOM]</ta>
            <ta e="T364" id="Seg_7000" s="T363">grass-3SG-ACC-tree-3SG-ACC</ta>
            <ta e="T365" id="Seg_7001" s="T364">all.without.exception</ta>
            <ta e="T366" id="Seg_7002" s="T365">see-PRS-1SG</ta>
            <ta e="T367" id="Seg_7003" s="T366">eye-EP-1SG.[NOM]</ta>
            <ta e="T368" id="Seg_7004" s="T367">take-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T369" id="Seg_7005" s="T368">when</ta>
            <ta e="T370" id="Seg_7006" s="T369">NEG</ta>
            <ta e="T371" id="Seg_7007" s="T370">reach-NEG-1SG</ta>
            <ta e="T372" id="Seg_7008" s="T371">so</ta>
            <ta e="T373" id="Seg_7009" s="T372">fast-ABL</ta>
            <ta e="T374" id="Seg_7010" s="T373">fast.[NOM]</ta>
            <ta e="T375" id="Seg_7011" s="T374">human.being.[NOM]</ta>
            <ta e="T376" id="Seg_7012" s="T375">see-NMNZ-3SG.[NOM]</ta>
            <ta e="T377" id="Seg_7013" s="T376">be-PST2.[3SG]</ta>
            <ta e="T378" id="Seg_7014" s="T377">say-PRS.[3SG]</ta>
            <ta e="T379" id="Seg_7015" s="T378">boy.[NOM]</ta>
            <ta e="T380" id="Seg_7016" s="T379">czar.[NOM]</ta>
            <ta e="T381" id="Seg_7017" s="T380">wonder-PRS.[3SG]</ta>
            <ta e="T382" id="Seg_7018" s="T381">oh</ta>
            <ta e="T383" id="Seg_7019" s="T382">old.man.[NOM]</ta>
            <ta e="T384" id="Seg_7020" s="T383">well</ta>
            <ta e="T385" id="Seg_7021" s="T384">mind-PROPR</ta>
            <ta e="T386" id="Seg_7022" s="T385">human.being-2SG</ta>
            <ta e="T387" id="Seg_7023" s="T386">be-PST2.[3SG]</ta>
            <ta e="T388" id="Seg_7024" s="T387">well</ta>
            <ta e="T389" id="Seg_7025" s="T388">what.[NOM]</ta>
            <ta e="T390" id="Seg_7026" s="T389">and</ta>
            <ta e="T391" id="Seg_7027" s="T390">three</ta>
            <ta e="T392" id="Seg_7028" s="T391">time-PROPR.[NOM]</ta>
            <ta e="T393" id="Seg_7029" s="T392">go.out-CVB.SEQ</ta>
            <ta e="T394" id="Seg_7030" s="T393">male.[NOM]</ta>
            <ta e="T395" id="Seg_7031" s="T394">cow-ACC</ta>
            <ta e="T396" id="Seg_7032" s="T395">grab-CVB.SEQ</ta>
            <ta e="T397" id="Seg_7033" s="T396">give-PRS.[3SG]</ta>
            <ta e="T398" id="Seg_7034" s="T397">this-ACC</ta>
            <ta e="T399" id="Seg_7035" s="T398">in.the.morning</ta>
            <ta e="T400" id="Seg_7036" s="T399">give.birth-EP-CAUS-CVB.SEQ</ta>
            <ta e="T401" id="Seg_7037" s="T400">calf-3SG-ACC</ta>
            <ta e="T402" id="Seg_7038" s="T401">follow-EP-MED-CAUS-CVB.SEQ</ta>
            <ta e="T403" id="Seg_7039" s="T402">bring-FUT.[IMP.2SG]</ta>
            <ta e="T404" id="Seg_7040" s="T403">what.kind.of</ta>
            <ta e="T405" id="Seg_7041" s="T404">dung-3SG-ACC</ta>
            <ta e="T406" id="Seg_7042" s="T405">gather-CVB.SEQ</ta>
            <ta e="T407" id="Seg_7043" s="T406">bring-APPR-2SG</ta>
            <ta e="T408" id="Seg_7044" s="T407">say-PST2.[3SG]</ta>
            <ta e="T409" id="Seg_7045" s="T408">boy.[NOM]</ta>
            <ta e="T410" id="Seg_7046" s="T409">czar.[NOM]</ta>
            <ta e="T411" id="Seg_7047" s="T410">old.man.[NOM]</ta>
            <ta e="T412" id="Seg_7048" s="T411">pain-PROPR</ta>
            <ta e="T413" id="Seg_7049" s="T412">male.[NOM]</ta>
            <ta e="T414" id="Seg_7050" s="T413">cow-3SG-ACC</ta>
            <ta e="T415" id="Seg_7051" s="T414">lead-CVB.SEQ</ta>
            <ta e="T416" id="Seg_7052" s="T415">reach-CVB.SEQ</ta>
            <ta e="T417" id="Seg_7053" s="T416">cry-CVB.SIM</ta>
            <ta e="T418" id="Seg_7054" s="T417">sit.down-PST1-3SG</ta>
            <ta e="T419" id="Seg_7055" s="T418">daughter-3SG.[NOM]</ta>
            <ta e="T420" id="Seg_7056" s="T419">ask-PST1-3SG</ta>
            <ta e="T421" id="Seg_7057" s="T420">well</ta>
            <ta e="T422" id="Seg_7058" s="T421">father</ta>
            <ta e="T423" id="Seg_7059" s="T422">why</ta>
            <ta e="T424" id="Seg_7060" s="T423">cry-PST1-2SG</ta>
            <ta e="T425" id="Seg_7061" s="T424">well</ta>
            <ta e="T426" id="Seg_7062" s="T425">male.[NOM]</ta>
            <ta e="T427" id="Seg_7063" s="T426">cow-ACC</ta>
            <ta e="T428" id="Seg_7064" s="T427">give-PST1-3SG</ta>
            <ta e="T429" id="Seg_7065" s="T428">give.birth-EP-CAUS-CVB.SEQ</ta>
            <ta e="T430" id="Seg_7066" s="T429">bring.[IMP.2SG]</ta>
            <ta e="T431" id="Seg_7067" s="T430">say-PRS.[3SG]</ta>
            <ta e="T432" id="Seg_7068" s="T431">male.[NOM]</ta>
            <ta e="T433" id="Seg_7069" s="T432">cow.[NOM]</ta>
            <ta e="T434" id="Seg_7070" s="T433">how</ta>
            <ta e="T435" id="Seg_7071" s="T434">give.birth-PTCP.HAB-ACC=Q</ta>
            <ta e="T436" id="Seg_7072" s="T435">small.child.[NOM]</ta>
            <ta e="T437" id="Seg_7073" s="T436">AFFIRM</ta>
            <ta e="T438" id="Seg_7074" s="T437">male.[NOM]</ta>
            <ta e="T439" id="Seg_7075" s="T438">cow.[NOM]</ta>
            <ta e="T440" id="Seg_7076" s="T439">where.from</ta>
            <ta e="T441" id="Seg_7077" s="T440">give.birth-FUT.[3SG]=Q</ta>
            <ta e="T442" id="Seg_7078" s="T441">instead</ta>
            <ta e="T443" id="Seg_7079" s="T442">food-VBZ-PST2-1PL</ta>
            <ta e="T444" id="Seg_7080" s="T443">go.out-CVB.SEQ-2SG</ta>
            <ta e="T445" id="Seg_7081" s="T444">kill-INCH.[IMP.2SG]</ta>
            <ta e="T446" id="Seg_7082" s="T445">meat-3SG-ACC</ta>
            <ta e="T447" id="Seg_7083" s="T446">eat-FUT-1PL</ta>
            <ta e="T448" id="Seg_7084" s="T447">say-PRS.[3SG]</ta>
            <ta e="T449" id="Seg_7085" s="T448">daughter.[NOM]</ta>
            <ta e="T450" id="Seg_7086" s="T449">old.man.[NOM]</ta>
            <ta e="T451" id="Seg_7087" s="T450">male.[NOM]</ta>
            <ta e="T452" id="Seg_7088" s="T451">cow-3SG-ACC</ta>
            <ta e="T453" id="Seg_7089" s="T452">kill-CVB.SEQ</ta>
            <ta e="T454" id="Seg_7090" s="T453">throw-PRS.[3SG]</ta>
            <ta e="T455" id="Seg_7091" s="T454">well</ta>
            <ta e="T456" id="Seg_7092" s="T455">this-ACC</ta>
            <ta e="T457" id="Seg_7093" s="T456">eat-FUT-3PL</ta>
            <ta e="T458" id="Seg_7094" s="T457">daughter-3SG.[NOM]</ta>
            <ta e="T459" id="Seg_7095" s="T458">bag-DAT/LOC</ta>
            <ta e="T460" id="Seg_7096" s="T459">cow.[NOM]</ta>
            <ta e="T461" id="Seg_7097" s="T460">hoof-3SG-ACC</ta>
            <ta e="T462" id="Seg_7098" s="T461">knucklebone-3SG-ACC</ta>
            <ta e="T463" id="Seg_7099" s="T462">gather-CVB.SEQ</ta>
            <ta e="T464" id="Seg_7100" s="T463">give-PST1-3SG</ta>
            <ta e="T465" id="Seg_7101" s="T464">father-3SG-DAT/LOC</ta>
            <ta e="T466" id="Seg_7102" s="T465">well</ta>
            <ta e="T467" id="Seg_7103" s="T466">that</ta>
            <ta e="T468" id="Seg_7104" s="T467">black</ta>
            <ta e="T469" id="Seg_7105" s="T468">hoof-3SG.[NOM]</ta>
            <ta e="T470" id="Seg_7106" s="T469">mother-3SG.[NOM]</ta>
            <ta e="T471" id="Seg_7107" s="T470">knucklebone-3SG.[NOM]</ta>
            <ta e="T472" id="Seg_7108" s="T471">calf-3SG.[NOM]</ta>
            <ta e="T473" id="Seg_7109" s="T472">say-CVB.SEQ</ta>
            <ta e="T474" id="Seg_7110" s="T473">after</ta>
            <ta e="T475" id="Seg_7111" s="T474">czar-3SG-GEN</ta>
            <ta e="T476" id="Seg_7112" s="T475">table-3SG-DAT/LOC</ta>
            <ta e="T477" id="Seg_7113" s="T476">child.[NOM]</ta>
            <ta e="T478" id="Seg_7114" s="T477">toys-3SG-ACC</ta>
            <ta e="T479" id="Seg_7115" s="T478">like</ta>
            <ta e="T480" id="Seg_7116" s="T479">spread.out-FREQ-CVB.SEQ</ta>
            <ta e="T481" id="Seg_7117" s="T480">throw-FUT.[IMP.2SG]</ta>
            <ta e="T482" id="Seg_7118" s="T481">male.[NOM]</ta>
            <ta e="T483" id="Seg_7119" s="T482">cow-ABL</ta>
            <ta e="T484" id="Seg_7120" s="T483">give.birth-PTCP.PRS</ta>
            <ta e="T485" id="Seg_7121" s="T484">only</ta>
            <ta e="T486" id="Seg_7122" s="T485">matter-3SG-ACC</ta>
            <ta e="T487" id="Seg_7123" s="T486">this</ta>
            <ta e="T488" id="Seg_7124" s="T487">find-PST1-1SG</ta>
            <ta e="T489" id="Seg_7125" s="T488">say-FUT.[IMP.2SG]</ta>
            <ta e="T490" id="Seg_7126" s="T489">that-ABL</ta>
            <ta e="T491" id="Seg_7127" s="T490">rest-ACC</ta>
            <ta e="T492" id="Seg_7128" s="T491">and</ta>
            <ta e="T493" id="Seg_7129" s="T492">3SG.[NOM]</ta>
            <ta e="T494" id="Seg_7130" s="T493">know-PTCP.PRS-3SG</ta>
            <ta e="T495" id="Seg_7131" s="T494">be-FUT.[3SG]</ta>
            <ta e="T496" id="Seg_7132" s="T495">Q</ta>
            <ta e="T497" id="Seg_7133" s="T496">well</ta>
            <ta e="T498" id="Seg_7134" s="T497">then</ta>
            <ta e="T499" id="Seg_7135" s="T498">demand-CVB.SEQ</ta>
            <ta e="T500" id="Seg_7136" s="T499">ask-FUT.[3SG]</ta>
            <ta e="T501" id="Seg_7137" s="T500">self-2SG.[NOM]</ta>
            <ta e="T502" id="Seg_7138" s="T501">mind-2SG-INSTR</ta>
            <ta e="T503" id="Seg_7139" s="T502">make-NEG-PST1-2SG</ta>
            <ta e="T504" id="Seg_7140" s="T503">probably</ta>
            <ta e="T505" id="Seg_7141" s="T504">that-APRX</ta>
            <ta e="T506" id="Seg_7142" s="T505">mind-PROPR-EP-2SG.[NOM]</ta>
            <ta e="T507" id="Seg_7143" s="T506">be-COND.[3SG]</ta>
            <ta e="T508" id="Seg_7144" s="T507">1SG.[NOM]</ta>
            <ta e="T509" id="Seg_7145" s="T508">urine-1SG-ACC-faeces-1SG-ACC</ta>
            <ta e="T510" id="Seg_7146" s="T509">clean-CVB.SIM</ta>
            <ta e="T511" id="Seg_7147" s="T510">get.saturated-CVB.SEQ-get.along-CVB.SEQ</ta>
            <ta e="T512" id="Seg_7148" s="T511">lie-FUT.[3SG]</ta>
            <ta e="T513" id="Seg_7149" s="T512">NEG</ta>
            <ta e="T514" id="Seg_7150" s="T513">be-PST1-2SG</ta>
            <ta e="T515" id="Seg_7151" s="T514">who.[NOM]</ta>
            <ta e="T516" id="Seg_7152" s="T515">teach-PRS.[3SG]</ta>
            <ta e="T517" id="Seg_7153" s="T516">say-FUT-3SG</ta>
            <ta e="T518" id="Seg_7154" s="T517">there</ta>
            <ta e="T519" id="Seg_7155" s="T518">hide-APPR-2SG</ta>
            <ta e="T520" id="Seg_7156" s="T519">say-PRS.[3SG]</ta>
            <ta e="T521" id="Seg_7157" s="T520">daughter-3SG.[NOM]</ta>
            <ta e="T522" id="Seg_7158" s="T521">bad</ta>
            <ta e="T523" id="Seg_7159" s="T522">girl.[NOM]</ta>
            <ta e="T524" id="Seg_7160" s="T523">child-PROPR-1SG</ta>
            <ta e="T525" id="Seg_7161" s="T524">that.[NOM]</ta>
            <ta e="T526" id="Seg_7162" s="T525">teach-FREQ-PRS.[3SG]</ta>
            <ta e="T527" id="Seg_7163" s="T526">say-FUT.[IMP.2SG]</ta>
            <ta e="T528" id="Seg_7164" s="T527">old.man.[NOM]</ta>
            <ta e="T529" id="Seg_7165" s="T528">in.the.morning</ta>
            <ta e="T530" id="Seg_7166" s="T529">go-PRS.[3SG]</ta>
            <ta e="T531" id="Seg_7167" s="T530">boy.[NOM]</ta>
            <ta e="T532" id="Seg_7168" s="T531">czar.[NOM]</ta>
            <ta e="T533" id="Seg_7169" s="T532">ask-PRS.[3SG]</ta>
            <ta e="T534" id="Seg_7170" s="T533">well</ta>
            <ta e="T535" id="Seg_7171" s="T534">guess-PST1-2SG</ta>
            <ta e="T536" id="Seg_7172" s="T535">Q</ta>
            <ta e="T537" id="Seg_7173" s="T536">old.man.[NOM]</ta>
            <ta e="T538" id="Seg_7174" s="T537">cow.[NOM]</ta>
            <ta e="T539" id="Seg_7175" s="T538">hoof-3SG-ACC</ta>
            <ta e="T540" id="Seg_7176" s="T539">knucklebone-3SG-ACC</ta>
            <ta e="T541" id="Seg_7177" s="T540">table-DAT/LOC</ta>
            <ta e="T542" id="Seg_7178" s="T541">child.[NOM]</ta>
            <ta e="T543" id="Seg_7179" s="T542">toys-3SG-ACC</ta>
            <ta e="T544" id="Seg_7180" s="T543">like</ta>
            <ta e="T545" id="Seg_7181" s="T544">spread.out-FREQ-CVB.SEQ</ta>
            <ta e="T546" id="Seg_7182" s="T545">throw-PRS.[3SG]</ta>
            <ta e="T547" id="Seg_7183" s="T546">male.[NOM]</ta>
            <ta e="T548" id="Seg_7184" s="T547">cow-ABL</ta>
            <ta e="T549" id="Seg_7185" s="T548">give.birth-PTCP.PRS</ta>
            <ta e="T550" id="Seg_7186" s="T549">only</ta>
            <ta e="T551" id="Seg_7187" s="T550">matter-3SG-ACC</ta>
            <ta e="T552" id="Seg_7188" s="T551">this</ta>
            <ta e="T553" id="Seg_7189" s="T552">find-PST1-1SG</ta>
            <ta e="T554" id="Seg_7190" s="T553">say-PRS.[3SG]</ta>
            <ta e="T555" id="Seg_7191" s="T554">czar.[NOM]</ta>
            <ta e="T556" id="Seg_7192" s="T555">self-2SG.[NOM]</ta>
            <ta e="T557" id="Seg_7193" s="T556">mind-2SG-INSTR</ta>
            <ta e="T558" id="Seg_7194" s="T557">make-NEG-PST1-2SG</ta>
            <ta e="T559" id="Seg_7195" s="T558">probably</ta>
            <ta e="T560" id="Seg_7196" s="T559">that-APRX</ta>
            <ta e="T561" id="Seg_7197" s="T560">mind-PROPR-EP-2SG.[NOM]</ta>
            <ta e="T562" id="Seg_7198" s="T561">be-COND.[3SG]</ta>
            <ta e="T563" id="Seg_7199" s="T562">1SG.[NOM]</ta>
            <ta e="T564" id="Seg_7200" s="T563">urine-1SG-ACC-faeces-1SG-ACC</ta>
            <ta e="T565" id="Seg_7201" s="T564">clean-CVB.SIM</ta>
            <ta e="T566" id="Seg_7202" s="T565">get.saturated-CVB.SEQ-get.along-CVB.SEQ</ta>
            <ta e="T567" id="Seg_7203" s="T566">lie-FUT.[3SG]</ta>
            <ta e="T568" id="Seg_7204" s="T567">NEG</ta>
            <ta e="T569" id="Seg_7205" s="T568">be-PST1-2SG</ta>
            <ta e="T570" id="Seg_7206" s="T569">who.[NOM]</ta>
            <ta e="T571" id="Seg_7207" s="T570">teach-PRS.[3SG]</ta>
            <ta e="T572" id="Seg_7208" s="T571">say-PRS.[3SG]</ta>
            <ta e="T573" id="Seg_7209" s="T572">bad</ta>
            <ta e="T574" id="Seg_7210" s="T573">girl.[NOM]</ta>
            <ta e="T575" id="Seg_7211" s="T574">child-PROPR-1SG</ta>
            <ta e="T576" id="Seg_7212" s="T575">that.[NOM]</ta>
            <ta e="T577" id="Seg_7213" s="T576">teach-FREQ-PRS.[3SG]</ta>
            <ta e="T578" id="Seg_7214" s="T577">say-PRS.[3SG]</ta>
            <ta e="T579" id="Seg_7215" s="T578">old.man.[NOM]</ta>
            <ta e="T580" id="Seg_7216" s="T579">boy.[NOM]</ta>
            <ta e="T581" id="Seg_7217" s="T580">czar.[NOM]</ta>
            <ta e="T582" id="Seg_7218" s="T581">girl-EP-2SG.[NOM]</ta>
            <ta e="T583" id="Seg_7219" s="T582">mind-PROPR</ta>
            <ta e="T584" id="Seg_7220" s="T583">girl.[NOM]</ta>
            <ta e="T585" id="Seg_7221" s="T584">give.birth-PST2.[3SG]</ta>
            <ta e="T586" id="Seg_7222" s="T585">say-PRS.[3SG]</ta>
            <ta e="T587" id="Seg_7223" s="T586">iron</ta>
            <ta e="T588" id="Seg_7224" s="T587">bucket-ACC</ta>
            <ta e="T589" id="Seg_7225" s="T588">ground-3SG.[NOM]</ta>
            <ta e="T590" id="Seg_7226" s="T589">hole-PROPR-ACC</ta>
            <ta e="T591" id="Seg_7227" s="T590">give-PRS.[3SG]</ta>
            <ta e="T592" id="Seg_7228" s="T591">this-ACC</ta>
            <ta e="T593" id="Seg_7229" s="T592">patch-CVB.SEQ</ta>
            <ta e="T594" id="Seg_7230" s="T593">send-IMP.3SG</ta>
            <ta e="T595" id="Seg_7231" s="T594">say-PRS.[3SG]</ta>
            <ta e="T596" id="Seg_7232" s="T595">that-ACC</ta>
            <ta e="T597" id="Seg_7233" s="T596">patch-NEG-TEMP-3SG</ta>
            <ta e="T598" id="Seg_7234" s="T597">two-COLL-2PL-GEN</ta>
            <ta e="T599" id="Seg_7235" s="T598">head-2PL-ACC</ta>
            <ta e="T600" id="Seg_7236" s="T599">cut-FUT-1SG</ta>
            <ta e="T601" id="Seg_7237" s="T600">say-PRS.[3SG]</ta>
            <ta e="T602" id="Seg_7238" s="T601">old.man.[NOM]</ta>
            <ta e="T603" id="Seg_7239" s="T602">bucket-ACC</ta>
            <ta e="T604" id="Seg_7240" s="T603">carry-CVB.SIM</ta>
            <ta e="T605" id="Seg_7241" s="T604">go-PST2-3SG</ta>
            <ta e="T606" id="Seg_7242" s="T605">cry-CVB.SIM-cry-CVB.SIM</ta>
            <ta e="T607" id="Seg_7243" s="T606">girl.[NOM]</ta>
            <ta e="T608" id="Seg_7244" s="T607">well</ta>
            <ta e="T609" id="Seg_7245" s="T608">father</ta>
            <ta e="T610" id="Seg_7246" s="T609">what-EP-2SG.[NOM]</ta>
            <ta e="T611" id="Seg_7247" s="T610">bucket-3SG.[NOM]=Q</ta>
            <ta e="T612" id="Seg_7248" s="T611">say-PRS.[3SG]</ta>
            <ta e="T613" id="Seg_7249" s="T612">well-EMPH</ta>
            <ta e="T614" id="Seg_7250" s="T613">well</ta>
            <ta e="T615" id="Seg_7251" s="T614">kill-PTCP.PRS</ta>
            <ta e="T616" id="Seg_7252" s="T615">day-3SG.[NOM]</ta>
            <ta e="T617" id="Seg_7253" s="T616">come-PST1-3SG</ta>
            <ta e="T618" id="Seg_7254" s="T617">this</ta>
            <ta e="T619" id="Seg_7255" s="T618">bucket-ACC</ta>
            <ta e="T620" id="Seg_7256" s="T619">patch-CVB.SEQ</ta>
            <ta e="T621" id="Seg_7257" s="T620">send-IMP.3SG</ta>
            <ta e="T622" id="Seg_7258" s="T621">say-PRS.[3SG]</ta>
            <ta e="T623" id="Seg_7259" s="T622">this-ACC</ta>
            <ta e="T624" id="Seg_7260" s="T623">patch-NEG-TEMP-3SG</ta>
            <ta e="T625" id="Seg_7261" s="T624">two-COLL-2PL-GEN</ta>
            <ta e="T626" id="Seg_7262" s="T625">head-2PL-ACC</ta>
            <ta e="T627" id="Seg_7263" s="T626">cut-FUT-1SG</ta>
            <ta e="T628" id="Seg_7264" s="T627">say-PRS.[3SG]</ta>
            <ta e="T629" id="Seg_7265" s="T628">daughter.[NOM]</ta>
            <ta e="T630" id="Seg_7266" s="T629">say-PRS.[3SG]</ta>
            <ta e="T631" id="Seg_7267" s="T630">carry.[IMP.2SG]</ta>
            <ta e="T632" id="Seg_7268" s="T631">back</ta>
            <ta e="T633" id="Seg_7269" s="T632">woman.[NOM]</ta>
            <ta e="T634" id="Seg_7270" s="T633">human.being.[NOM]</ta>
            <ta e="T635" id="Seg_7271" s="T634">patch-FUT-1SG</ta>
            <ta e="T636" id="Seg_7272" s="T635">man.[NOM]</ta>
            <ta e="T637" id="Seg_7273" s="T636">human.being.[NOM]</ta>
            <ta e="T638" id="Seg_7274" s="T637">bucket-ACC</ta>
            <ta e="T639" id="Seg_7275" s="T638">light.boots.of.reindeer.fur.[NOM]</ta>
            <ta e="T640" id="Seg_7276" s="T639">similar</ta>
            <ta e="T641" id="Seg_7277" s="T640">turn.inside.out-CVB.SEQ</ta>
            <ta e="T642" id="Seg_7278" s="T641">send-IMP.3SG</ta>
            <ta e="T643" id="Seg_7279" s="T642">old.man.[NOM]</ta>
            <ta e="T644" id="Seg_7280" s="T643">carry-CVB.SEQ</ta>
            <ta e="T645" id="Seg_7281" s="T644">give-PRS.[3SG]</ta>
            <ta e="T646" id="Seg_7282" s="T645">boy.[NOM]</ta>
            <ta e="T647" id="Seg_7283" s="T646">czar.[NOM]</ta>
            <ta e="T648" id="Seg_7284" s="T647">wonder-PRS.[3SG]</ta>
            <ta e="T649" id="Seg_7285" s="T648">old.man-ACC</ta>
            <ta e="T650" id="Seg_7286" s="T649">send-PRS.[3SG]</ta>
            <ta e="T651" id="Seg_7287" s="T650">self-1SG.[NOM]</ta>
            <ta e="T652" id="Seg_7288" s="T651">daughter-2SG-ACC</ta>
            <ta e="T653" id="Seg_7289" s="T652">see-CVB.SIM</ta>
            <ta e="T654" id="Seg_7290" s="T653">go-FUT-1SG</ta>
            <ta e="T655" id="Seg_7291" s="T654">say-PRS.[3SG]</ta>
            <ta e="T656" id="Seg_7292" s="T655">boy.[NOM]</ta>
            <ta e="T657" id="Seg_7293" s="T656">czar.[NOM]</ta>
            <ta e="T658" id="Seg_7294" s="T657">daughter-ACC</ta>
            <ta e="T659" id="Seg_7295" s="T658">see-CVB.ANT</ta>
            <ta e="T660" id="Seg_7296" s="T659">love-PRS.[3SG]</ta>
            <ta e="T661" id="Seg_7297" s="T660">woman.[NOM]</ta>
            <ta e="T662" id="Seg_7298" s="T661">take-PRS.[3SG]</ta>
            <ta e="T663" id="Seg_7299" s="T662">how.much-ACC-how.much-ACC</ta>
            <ta e="T664" id="Seg_7300" s="T663">live-PST2-3PL</ta>
            <ta e="T665" id="Seg_7301" s="T664">be-PST1-3SG</ta>
            <ta e="T666" id="Seg_7302" s="T665">woman-VBZ-PTCP.PST-3SG-ABL</ta>
            <ta e="T667" id="Seg_7303" s="T666">boy.[NOM]</ta>
            <ta e="T668" id="Seg_7304" s="T667">czar.[NOM]</ta>
            <ta e="T669" id="Seg_7305" s="T668">doll.[NOM]</ta>
            <ta e="T670" id="Seg_7306" s="T669">like</ta>
            <ta e="T671" id="Seg_7307" s="T670">sit.down-ADVZ</ta>
            <ta e="T672" id="Seg_7308" s="T671">be-PST1-3SG</ta>
            <ta e="T673" id="Seg_7309" s="T672">every-3SG-ACC</ta>
            <ta e="T674" id="Seg_7310" s="T673">woman-3SG.[NOM]</ta>
            <ta e="T675" id="Seg_7311" s="T674">advise-PRS.[3SG]</ta>
            <ta e="T676" id="Seg_7312" s="T675">decide-PRS.[3SG]</ta>
            <ta e="T677" id="Seg_7313" s="T676">boy.[NOM]</ta>
            <ta e="T678" id="Seg_7314" s="T677">czar.[NOM]</ta>
            <ta e="T679" id="Seg_7315" s="T678">tongue-VBZ-PRS.[3SG]</ta>
            <ta e="T680" id="Seg_7316" s="T679">friend</ta>
            <ta e="T681" id="Seg_7317" s="T680">this</ta>
            <ta e="T682" id="Seg_7318" s="T681">one</ta>
            <ta e="T683" id="Seg_7319" s="T682">country-DAT/LOC</ta>
            <ta e="T684" id="Seg_7320" s="T683">two</ta>
            <ta e="T685" id="Seg_7321" s="T684">czar.[NOM]</ta>
            <ta e="T686" id="Seg_7322" s="T685">be-PTCP.PRS-1PL.[NOM]</ta>
            <ta e="T687" id="Seg_7323" s="T686">occur-EP-NEG.[3SG]</ta>
            <ta e="T688" id="Seg_7324" s="T687">1PL.[NOM]</ta>
            <ta e="T689" id="Seg_7325" s="T688">separate-IMP.1DU</ta>
            <ta e="T690" id="Seg_7326" s="T689">woman-3SG.[NOM]</ta>
            <ta e="T691" id="Seg_7327" s="T690">well</ta>
            <ta e="T692" id="Seg_7328" s="T691">separate-IMP.1DU</ta>
            <ta e="T693" id="Seg_7329" s="T692">Q</ta>
            <ta e="T694" id="Seg_7330" s="T693">say-PRS.[3SG]</ta>
            <ta e="T695" id="Seg_7331" s="T694">boy.[NOM]</ta>
            <ta e="T696" id="Seg_7332" s="T695">czar.[NOM]</ta>
            <ta e="T697" id="Seg_7333" s="T696">what</ta>
            <ta e="T698" id="Seg_7334" s="T697">wish-PROPR</ta>
            <ta e="T699" id="Seg_7335" s="T698">matter-2SG-ACC</ta>
            <ta e="T700" id="Seg_7336" s="T699">every-3SG-ACC</ta>
            <ta e="T701" id="Seg_7337" s="T700">take.[IMP.2SG]</ta>
            <ta e="T702" id="Seg_7338" s="T701">say-PRS.[3SG]</ta>
            <ta e="T703" id="Seg_7339" s="T702">woman.[NOM]</ta>
            <ta e="T704" id="Seg_7340" s="T703">agree-EP-RECP/COLL-PRS.[3SG]</ta>
            <ta e="T705" id="Seg_7341" s="T704">separate-PTCP.PRS</ta>
            <ta e="T706" id="Seg_7342" s="T705">night-3PL.[NOM]=EMPH</ta>
            <ta e="T707" id="Seg_7343" s="T706">become-PRS.[3SG]</ta>
            <ta e="T708" id="Seg_7344" s="T707">man-3SG.[NOM]</ta>
            <ta e="T709" id="Seg_7345" s="T708">fall.asleep-CVB.SEQ</ta>
            <ta e="T710" id="Seg_7346" s="T709">stay-PRS.[3SG]</ta>
            <ta e="T711" id="Seg_7347" s="T710">woman.[NOM]</ta>
            <ta e="T712" id="Seg_7348" s="T711">husband-3SG-ACC</ta>
            <ta e="T713" id="Seg_7349" s="T712">blanket-PL-ACC</ta>
            <ta e="T714" id="Seg_7350" s="T713">bedding-PL-ACC</ta>
            <ta e="T715" id="Seg_7351" s="T714">father-3SG-GEN</ta>
            <ta e="T716" id="Seg_7352" s="T715">earth.[NOM]</ta>
            <ta e="T717" id="Seg_7353" s="T716">pit_dwelling.[NOM]</ta>
            <ta e="T718" id="Seg_7354" s="T717">house-3SG-DAT/LOC</ta>
            <ta e="T719" id="Seg_7355" s="T718">raise-CVB.SEQ</ta>
            <ta e="T720" id="Seg_7356" s="T719">carry-PRS.[3SG]</ta>
            <ta e="T721" id="Seg_7357" s="T720">boy.[NOM]</ta>
            <ta e="T722" id="Seg_7358" s="T721">czar.[NOM]</ta>
            <ta e="T723" id="Seg_7359" s="T722">in.the.morning</ta>
            <ta e="T724" id="Seg_7360" s="T723">wake.up-CVB.SEQ</ta>
            <ta e="T725" id="Seg_7361" s="T724">startle-PRS.[3SG]</ta>
            <ta e="T726" id="Seg_7362" s="T725">stretch-CVB.SEQ</ta>
            <ta e="T727" id="Seg_7363" s="T726">see-PST2-3SG</ta>
            <ta e="T728" id="Seg_7364" s="T727">head-3SG.[NOM]-leg-3SG.[NOM]</ta>
            <ta e="T729" id="Seg_7365" s="T728">pit_dwelling.[NOM]</ta>
            <ta e="T730" id="Seg_7366" s="T729">wood-3SG-DAT/LOC</ta>
            <ta e="T731" id="Seg_7367" s="T730">push-PRS-3PL</ta>
            <ta e="T732" id="Seg_7368" s="T731">well-EMPH</ta>
            <ta e="T733" id="Seg_7369" s="T732">this</ta>
            <ta e="T734" id="Seg_7370" s="T733">where</ta>
            <ta e="T735" id="Seg_7371" s="T734">there.is-1PL=Q</ta>
            <ta e="T736" id="Seg_7372" s="T735">why</ta>
            <ta e="T737" id="Seg_7373" s="T736">come-PST2-EP-1SG=Q</ta>
            <ta e="T738" id="Seg_7374" s="T737">woman-3SG-ACC</ta>
            <ta e="T739" id="Seg_7375" s="T738">push-FREQ-CVB.SEQ</ta>
            <ta e="T740" id="Seg_7376" s="T739">wake.up-CAUS-PRS.[3SG]</ta>
            <ta e="T741" id="Seg_7377" s="T740">woman-3SG.[NOM]</ta>
            <ta e="T742" id="Seg_7378" s="T741">well</ta>
            <ta e="T743" id="Seg_7379" s="T742">friend</ta>
            <ta e="T744" id="Seg_7380" s="T743">1SG.[NOM]</ta>
            <ta e="T745" id="Seg_7381" s="T744">bring-PST2-EP-1SG</ta>
            <ta e="T746" id="Seg_7382" s="T745">say-PRS.[3SG]</ta>
            <ta e="T747" id="Seg_7383" s="T746">why</ta>
            <ta e="T748" id="Seg_7384" s="T747">bring-PST2-2SG=Q</ta>
            <ta e="T749" id="Seg_7385" s="T748">ah</ta>
            <ta e="T750" id="Seg_7386" s="T749">self-2SG-ACC</ta>
            <ta e="T751" id="Seg_7387" s="T750">want-PST1-1SG</ta>
            <ta e="T752" id="Seg_7388" s="T751">and</ta>
            <ta e="T753" id="Seg_7389" s="T752">self-2SG-ACC</ta>
            <ta e="T754" id="Seg_7390" s="T753">bring-PST1-1SG</ta>
            <ta e="T755" id="Seg_7391" s="T754">what-ACC</ta>
            <ta e="T756" id="Seg_7392" s="T755">want-PTCP.PRS-2SG-ACC</ta>
            <ta e="T757" id="Seg_7393" s="T756">take-FUT.[IMP.2SG]</ta>
            <ta e="T758" id="Seg_7394" s="T757">say-PST2-EP-2SG</ta>
            <ta e="T759" id="Seg_7395" s="T758">EMPH</ta>
            <ta e="T760" id="Seg_7396" s="T759">say-PRS.[3SG]</ta>
            <ta e="T761" id="Seg_7397" s="T760">woman.[NOM]</ta>
            <ta e="T762" id="Seg_7398" s="T761">boy.[NOM]</ta>
            <ta e="T763" id="Seg_7399" s="T762">czar.[NOM]</ta>
            <ta e="T764" id="Seg_7400" s="T763">can-CVB.SEQ</ta>
            <ta e="T765" id="Seg_7401" s="T764">go.away-NEG-PST1-3SG</ta>
            <ta e="T766" id="Seg_7402" s="T765">well</ta>
            <ta e="T767" id="Seg_7403" s="T766">woman-3SG.[NOM]</ta>
            <ta e="T768" id="Seg_7404" s="T767">say-PRS.[3SG]</ta>
            <ta e="T769" id="Seg_7405" s="T768">so.much</ta>
            <ta e="T770" id="Seg_7406" s="T769">mind-PROPR</ta>
            <ta e="T771" id="Seg_7407" s="T770">human.being.[NOM]</ta>
            <ta e="T772" id="Seg_7408" s="T771">be-NEG.PTCP.PST</ta>
            <ta e="T773" id="Seg_7409" s="T772">be-PST2-2SG</ta>
            <ta e="T774" id="Seg_7410" s="T773">people-2SG-ACC</ta>
            <ta e="T775" id="Seg_7411" s="T774">advise-CVB.SEQ</ta>
            <ta e="T776" id="Seg_7412" s="T775">self-2SG.[NOM]</ta>
            <ta e="T777" id="Seg_7413" s="T776">live.[IMP.2SG]</ta>
            <ta e="T778" id="Seg_7414" s="T777">1SG.[NOM]</ta>
            <ta e="T779" id="Seg_7415" s="T778">simple</ta>
            <ta e="T780" id="Seg_7416" s="T779">housewife.[NOM]</ta>
            <ta e="T781" id="Seg_7417" s="T780">be-CVB.SEQ</ta>
            <ta e="T782" id="Seg_7418" s="T781">sit-FUT-1SG</ta>
            <ta e="T783" id="Seg_7419" s="T782">poor</ta>
            <ta e="T784" id="Seg_7420" s="T783">farmer.[NOM]</ta>
            <ta e="T785" id="Seg_7421" s="T784">girl-3SG-ACC</ta>
            <ta e="T786" id="Seg_7422" s="T785">good-3SG-INSTR</ta>
            <ta e="T787" id="Seg_7423" s="T786">mirror.[NOM]</ta>
            <ta e="T788" id="Seg_7424" s="T787">stone.[NOM]</ta>
            <ta e="T789" id="Seg_7425" s="T788">house-DAT/LOC</ta>
            <ta e="T790" id="Seg_7426" s="T789">live-INFER-3SG</ta>
            <ta e="T791" id="Seg_7427" s="T790">earth.[NOM]</ta>
            <ta e="T792" id="Seg_7428" s="T791">pit_dwelling-ABL</ta>
            <ta e="T793" id="Seg_7429" s="T792">separate-CVB.SEQ</ta>
            <ta e="T794" id="Seg_7430" s="T793">end-EP-ABL</ta>
            <ta e="T795" id="Seg_7431" s="T794">be.rich-CVB.SEQ-be.rich-CVB.SEQ</ta>
            <ta e="T796" id="Seg_7432" s="T795">live-INFER-3PL</ta>
            <ta e="T797" id="Seg_7433" s="T796">well</ta>
            <ta e="T798" id="Seg_7434" s="T797">last-3SG.[NOM]</ta>
         </annotation>
         <annotation name="gg" tierref="gg">
            <ta e="T1" id="Seg_7435" s="T0">doch</ta>
            <ta e="T2" id="Seg_7436" s="T1">Junge.[NOM]</ta>
            <ta e="T3" id="Seg_7437" s="T2">Zar.[NOM]</ta>
            <ta e="T4" id="Seg_7438" s="T3">leben-PST2-3SG</ta>
            <ta e="T5" id="Seg_7439" s="T4">Nachbarschaft-3SG-DAT/LOC</ta>
            <ta e="T6" id="Seg_7440" s="T5">arm</ta>
            <ta e="T7" id="Seg_7441" s="T6">Bauer-PROPR.[NOM]</ta>
            <ta e="T8" id="Seg_7442" s="T7">arm</ta>
            <ta e="T9" id="Seg_7443" s="T8">Bauer.[NOM]</ta>
            <ta e="T10" id="Seg_7444" s="T9">dieses</ta>
            <ta e="T11" id="Seg_7445" s="T10">Zar-DAT/LOC</ta>
            <ta e="T12" id="Seg_7446" s="T11">arbeiten-CVB.SEQ</ta>
            <ta e="T13" id="Seg_7447" s="T12">morgendlich</ta>
            <ta e="T14" id="Seg_7448" s="T13">Dämmerung-ABL</ta>
            <ta e="T15" id="Seg_7449" s="T14">Abend.[NOM]</ta>
            <ta e="T16" id="Seg_7450" s="T15">Dämmerung-DAT/LOC</ta>
            <ta e="T17" id="Seg_7451" s="T16">bis.zu</ta>
            <ta e="T18" id="Seg_7452" s="T17">eins</ta>
            <ta e="T19" id="Seg_7453" s="T18">Löffel.[NOM]</ta>
            <ta e="T20" id="Seg_7454" s="T19">Mehl.[NOM]</ta>
            <ta e="T21" id="Seg_7455" s="T20">für</ta>
            <ta e="T22" id="Seg_7456" s="T21">arbeiten-PTCP.PRS</ta>
            <ta e="T23" id="Seg_7457" s="T22">sein-PST2.[3SG]</ta>
            <ta e="T24" id="Seg_7458" s="T23">dieses-INSTR</ta>
            <ta e="T25" id="Seg_7459" s="T24">füttern-REFL-CVB.SEQ</ta>
            <ta e="T26" id="Seg_7460" s="T25">leben-PRS-3PL</ta>
            <ta e="T27" id="Seg_7461" s="T26">arm</ta>
            <ta e="T28" id="Seg_7462" s="T27">Bauer.[NOM]</ta>
            <ta e="T29" id="Seg_7463" s="T28">eins</ta>
            <ta e="T30" id="Seg_7464" s="T29">Mädchen.[NOM]</ta>
            <ta e="T31" id="Seg_7465" s="T30">Kind-PROPR.[NOM]</ta>
            <ta e="T32" id="Seg_7466" s="T31">doch</ta>
            <ta e="T33" id="Seg_7467" s="T32">einmal</ta>
            <ta e="T34" id="Seg_7468" s="T33">Junge.[NOM]</ta>
            <ta e="T35" id="Seg_7469" s="T34">Zar.[NOM]</ta>
            <ta e="T36" id="Seg_7470" s="T35">sagen-PRS.[3SG]</ta>
            <ta e="T37" id="Seg_7471" s="T36">na</ta>
            <ta e="T38" id="Seg_7472" s="T37">doch</ta>
            <ta e="T39" id="Seg_7473" s="T38">arm</ta>
            <ta e="T40" id="Seg_7474" s="T39">Bauer.[NOM]</ta>
            <ta e="T41" id="Seg_7475" s="T40">wie.viel</ta>
            <ta e="T42" id="Seg_7476" s="T41">Verstand-PROPR</ta>
            <ta e="T43" id="Seg_7477" s="T42">Mensch.[NOM]</ta>
            <ta e="T44" id="Seg_7478" s="T43">gebären-CVB.SEQ</ta>
            <ta e="T45" id="Seg_7479" s="T44">zurückkommen-PST1-2SG</ta>
            <ta e="T46" id="Seg_7480" s="T45">Rätsel-PART</ta>
            <ta e="T47" id="Seg_7481" s="T46">erraten.[IMP.2SG]</ta>
            <ta e="T48" id="Seg_7482" s="T47">süß-ABL</ta>
            <ta e="T49" id="Seg_7483" s="T48">süß.[NOM]</ta>
            <ta e="T50" id="Seg_7484" s="T49">was.[NOM]=Q</ta>
            <ta e="T51" id="Seg_7485" s="T50">am.Morgen</ta>
            <ta e="T52" id="Seg_7486" s="T51">kommen-CVB.SEQ</ta>
            <ta e="T53" id="Seg_7487" s="T52">neun</ta>
            <ta e="T54" id="Seg_7488" s="T53">Stunde-DAT/LOC</ta>
            <ta e="T55" id="Seg_7489" s="T54">sprechen-FUT.[IMP.2SG]</ta>
            <ta e="T56" id="Seg_7490" s="T55">jenes-ACC</ta>
            <ta e="T57" id="Seg_7491" s="T56">erraten-NEG-TEMP-2SG</ta>
            <ta e="T58" id="Seg_7492" s="T57">aufhängen-PTCP.PRS</ta>
            <ta e="T59" id="Seg_7493" s="T58">Baum-1SG-DAT/LOC</ta>
            <ta e="T60" id="Seg_7494" s="T59">Hals-PROPR</ta>
            <ta e="T61" id="Seg_7495" s="T60">Kopf-2SG-ACC</ta>
            <ta e="T62" id="Seg_7496" s="T61">schneiden-FUT-1SG</ta>
            <ta e="T63" id="Seg_7497" s="T62">arm</ta>
            <ta e="T64" id="Seg_7498" s="T63">Bauer.[NOM]</ta>
            <ta e="T65" id="Seg_7499" s="T64">warum</ta>
            <ta e="T66" id="Seg_7500" s="T65">NEG</ta>
            <ta e="T67" id="Seg_7501" s="T66">ausdenken-CVB.SIM</ta>
            <ta e="T68" id="Seg_7502" s="T67">können-NEG.[3SG]</ta>
            <ta e="T69" id="Seg_7503" s="T68">süß</ta>
            <ta e="T70" id="Seg_7504" s="T69">Nahrung-ACC</ta>
            <ta e="T71" id="Seg_7505" s="T70">wissen-EMOT-NEG.[3SG]</ta>
            <ta e="T72" id="Seg_7506" s="T71">Zar.[NOM]</ta>
            <ta e="T73" id="Seg_7507" s="T72">Nahrung-3SG.[NOM]</ta>
            <ta e="T74" id="Seg_7508" s="T73">jeder-3SG.[NOM]</ta>
            <ta e="T75" id="Seg_7509" s="T74">süß.[NOM]</ta>
            <ta e="T76" id="Seg_7510" s="T75">wie</ta>
            <ta e="T77" id="Seg_7511" s="T76">3SG-DAT/LOC</ta>
            <ta e="T78" id="Seg_7512" s="T77">Haus-3SG-DAT/LOC</ta>
            <ta e="T79" id="Seg_7513" s="T78">kommen-CVB.SEQ</ta>
            <ta e="T80" id="Seg_7514" s="T79">weinen-EP-MED-CVB.SIM</ta>
            <ta e="T81" id="Seg_7515" s="T80">sich.setzen-PST1-3SG</ta>
            <ta e="T82" id="Seg_7516" s="T81">Tochter-3SG.[NOM]</ta>
            <ta e="T83" id="Seg_7517" s="T82">fragen-PRS.[3SG]</ta>
            <ta e="T84" id="Seg_7518" s="T83">na</ta>
            <ta e="T85" id="Seg_7519" s="T84">Vater</ta>
            <ta e="T86" id="Seg_7520" s="T85">warum</ta>
            <ta e="T87" id="Seg_7521" s="T86">weinen-PST1-2SG</ta>
            <ta e="T88" id="Seg_7522" s="T87">doch</ta>
            <ta e="T89" id="Seg_7523" s="T88">Freund-EP-1SG.[NOM]</ta>
            <ta e="T90" id="Seg_7524" s="T89">gehen-EP-PST2-EP-1SG</ta>
            <ta e="T91" id="Seg_7525" s="T90">lediglich</ta>
            <ta e="T92" id="Seg_7526" s="T91">leben-PTCP.PST-EP-1SG.[NOM]</ta>
            <ta e="T93" id="Seg_7527" s="T92">Ende-3SG.[NOM]</ta>
            <ta e="T94" id="Seg_7528" s="T93">Junge.[NOM]</ta>
            <ta e="T95" id="Seg_7529" s="T94">Zar.[NOM]</ta>
            <ta e="T96" id="Seg_7530" s="T95">Rätsel.[NOM]</ta>
            <ta e="T97" id="Seg_7531" s="T96">geben-PST1-3SG</ta>
            <ta e="T98" id="Seg_7532" s="T97">erkennen-NEG-TEMP-1SG</ta>
            <ta e="T99" id="Seg_7533" s="T98">Hals-PROPR</ta>
            <ta e="T100" id="Seg_7534" s="T99">Kopf-1SG-ACC</ta>
            <ta e="T101" id="Seg_7535" s="T100">schneiden-PTCP.FUT</ta>
            <ta e="T102" id="Seg_7536" s="T101">sein-PST1-3SG</ta>
            <ta e="T103" id="Seg_7537" s="T102">was-ACC</ta>
            <ta e="T104" id="Seg_7538" s="T103">erkennen-CAUS-CAUS-PRS.[3SG]</ta>
            <ta e="T105" id="Seg_7539" s="T104">jenes.[NOM]</ta>
            <ta e="T106" id="Seg_7540" s="T105">süß-ABL</ta>
            <ta e="T107" id="Seg_7541" s="T106">süß.[NOM]</ta>
            <ta e="T108" id="Seg_7542" s="T107">was.[NOM]=Q</ta>
            <ta e="T109" id="Seg_7543" s="T108">sagen-PRS.[3SG]</ta>
            <ta e="T110" id="Seg_7544" s="T109">sagen-PST2.[3SG]</ta>
            <ta e="T111" id="Seg_7545" s="T110">hey</ta>
            <ta e="T112" id="Seg_7546" s="T111">Vater</ta>
            <ta e="T113" id="Seg_7547" s="T112">Nutzen-POSS</ta>
            <ta e="T114" id="Seg_7548" s="T113">NEG-DAT/LOC</ta>
            <ta e="T115" id="Seg_7549" s="T114">denn</ta>
            <ta e="T116" id="Seg_7550" s="T115">weinen-PRS-2SG</ta>
            <ta e="T117" id="Seg_7551" s="T116">schlafen-EMOT.[IMP.2SG]</ta>
            <ta e="T118" id="Seg_7552" s="T117">sich.ausruhen.[IMP.2SG]</ta>
            <ta e="T119" id="Seg_7553" s="T118">süß-ABL</ta>
            <ta e="T120" id="Seg_7554" s="T119">süß-ACC</ta>
            <ta e="T121" id="Seg_7555" s="T120">wissen-PTCP.PRS</ta>
            <ta e="T122" id="Seg_7556" s="T121">Sache-EP-1SG.[NOM]</ta>
            <ta e="T123" id="Seg_7557" s="T122">eins</ta>
            <ta e="T124" id="Seg_7558" s="T123">sagen-FUT.[IMP.2SG]</ta>
            <ta e="T125" id="Seg_7559" s="T124">morgendlich</ta>
            <ta e="T126" id="Seg_7560" s="T125">Dämmerung-ABL</ta>
            <ta e="T127" id="Seg_7561" s="T126">abendlich</ta>
            <ta e="T128" id="Seg_7562" s="T127">Dämmerung-DAT/LOC</ta>
            <ta e="T129" id="Seg_7563" s="T128">bis.zu</ta>
            <ta e="T130" id="Seg_7564" s="T129">arbeiten-CVB.SEQ</ta>
            <ta e="T131" id="Seg_7565" s="T130">2SG.[NOM]</ta>
            <ta e="T132" id="Seg_7566" s="T131">Kot-2SG-ACC-Dreck-2SG-ACC</ta>
            <ta e="T133" id="Seg_7567" s="T132">säubern-CVB.SEQ</ta>
            <ta e="T134" id="Seg_7568" s="T133">eins</ta>
            <ta e="T135" id="Seg_7569" s="T134">Löffel.[NOM]</ta>
            <ta e="T136" id="Seg_7570" s="T135">Mehl-ACC</ta>
            <ta e="T137" id="Seg_7571" s="T136">bringen-TEMP-1SG</ta>
            <ta e="T138" id="Seg_7572" s="T137">Alte-EP-1SG.[NOM]</ta>
            <ta e="T139" id="Seg_7573" s="T138">Boden.[NOM]</ta>
            <ta e="T140" id="Seg_7574" s="T139">Erdhütte.[NOM]</ta>
            <ta e="T141" id="Seg_7575" s="T140">Haus-3SG-ACC</ta>
            <ta e="T142" id="Seg_7576" s="T141">heizen-TEMP-3SG</ta>
            <ta e="T143" id="Seg_7577" s="T142">Nase-1SG.[NOM]</ta>
            <ta e="T144" id="Seg_7578" s="T143">sich.wärmen-PRS.[3SG]</ta>
            <ta e="T145" id="Seg_7579" s="T144">jenes</ta>
            <ta e="T146" id="Seg_7580" s="T145">Mehl-DIM-3SG-INSTR</ta>
            <ta e="T147" id="Seg_7581" s="T146">Mehlschwitze.[NOM]</ta>
            <ta e="T148" id="Seg_7582" s="T147">machen-TEMP-3SG</ta>
            <ta e="T149" id="Seg_7583" s="T148">essen-NEG.CVB.SIM</ta>
            <ta e="T150" id="Seg_7584" s="T149">und</ta>
            <ta e="T151" id="Seg_7585" s="T150">einschlafen-PRS-1SG</ta>
            <ta e="T152" id="Seg_7586" s="T151">so</ta>
            <ta e="T153" id="Seg_7587" s="T152">süß-ABL</ta>
            <ta e="T154" id="Seg_7588" s="T153">süß.[NOM]</ta>
            <ta e="T155" id="Seg_7589" s="T154">schlafen-PTCP.PRS</ta>
            <ta e="T156" id="Seg_7590" s="T155">Schlaf.[NOM]</ta>
            <ta e="T157" id="Seg_7591" s="T156">sein-FUT-NEC.[3SG]</ta>
            <ta e="T158" id="Seg_7592" s="T157">sagen-FUT.[IMP.2SG]</ta>
            <ta e="T159" id="Seg_7593" s="T158">3SG.[NOM]</ta>
            <ta e="T160" id="Seg_7594" s="T159">jenes-ABL</ta>
            <ta e="T161" id="Seg_7595" s="T160">und</ta>
            <ta e="T162" id="Seg_7596" s="T161">Rest-ACC</ta>
            <ta e="T163" id="Seg_7597" s="T162">wissen-PTCP.PRS-3SG</ta>
            <ta e="T164" id="Seg_7598" s="T163">sein-FUT.[3SG]</ta>
            <ta e="T165" id="Seg_7599" s="T164">Q</ta>
            <ta e="T166" id="Seg_7600" s="T165">sagen-PST2.[3SG]</ta>
            <ta e="T167" id="Seg_7601" s="T166">Tochter-3SG.[NOM]</ta>
            <ta e="T168" id="Seg_7602" s="T167">alter.Mann.[NOM]</ta>
            <ta e="T169" id="Seg_7603" s="T168">einschlafen-CVB.SEQ</ta>
            <ta e="T170" id="Seg_7604" s="T169">bleiben-PST1-3SG</ta>
            <ta e="T171" id="Seg_7605" s="T170">am.Morgen</ta>
            <ta e="T172" id="Seg_7606" s="T171">früh</ta>
            <ta e="T173" id="Seg_7607" s="T172">aufstehen-CVB.SEQ</ta>
            <ta e="T174" id="Seg_7608" s="T173">neun</ta>
            <ta e="T175" id="Seg_7609" s="T174">Stunde-DAT/LOC</ta>
            <ta e="T176" id="Seg_7610" s="T175">Zar-DAT/LOC</ta>
            <ta e="T177" id="Seg_7611" s="T176">ankommen-PRS.[3SG]</ta>
            <ta e="T178" id="Seg_7612" s="T177">Junge.[NOM]</ta>
            <ta e="T179" id="Seg_7613" s="T178">Zar.[NOM]</ta>
            <ta e="T180" id="Seg_7614" s="T179">fragen-PRS.[3SG]</ta>
            <ta e="T181" id="Seg_7615" s="T180">na</ta>
            <ta e="T182" id="Seg_7616" s="T181">erraten-PST1-2SG</ta>
            <ta e="T183" id="Seg_7617" s="T182">Q</ta>
            <ta e="T184" id="Seg_7618" s="T183">hey</ta>
            <ta e="T185" id="Seg_7619" s="T184">erkennen-PTCP.PRS-DAT/LOC</ta>
            <ta e="T186" id="Seg_7620" s="T185">ähnlich</ta>
            <ta e="T187" id="Seg_7621" s="T186">machen-PST1-1SG</ta>
            <ta e="T188" id="Seg_7622" s="T187">richtig</ta>
            <ta e="T189" id="Seg_7623" s="T188">Q</ta>
            <ta e="T190" id="Seg_7624" s="T189">Fehler-3SG.[NOM]</ta>
            <ta e="T191" id="Seg_7625" s="T190">Q</ta>
            <ta e="T192" id="Seg_7626" s="T191">sagen-PRS.[3SG]</ta>
            <ta e="T193" id="Seg_7627" s="T192">doch</ta>
            <ta e="T194" id="Seg_7628" s="T193">was-ACC=Q</ta>
            <ta e="T195" id="Seg_7629" s="T194">morgendlich</ta>
            <ta e="T196" id="Seg_7630" s="T195">Dämmerung-ABL</ta>
            <ta e="T197" id="Seg_7631" s="T196">abendlich</ta>
            <ta e="T198" id="Seg_7632" s="T197">Dämmerung-DAT/LOC</ta>
            <ta e="T199" id="Seg_7633" s="T198">bis.zu</ta>
            <ta e="T200" id="Seg_7634" s="T199">arbeiten-CVB.SEQ</ta>
            <ta e="T201" id="Seg_7635" s="T200">2SG.[NOM]</ta>
            <ta e="T202" id="Seg_7636" s="T201">Kot-2SG-ACC-Dreck-2SG-ACC</ta>
            <ta e="T203" id="Seg_7637" s="T202">säubern-CVB.SEQ</ta>
            <ta e="T204" id="Seg_7638" s="T203">eins</ta>
            <ta e="T205" id="Seg_7639" s="T204">Löffel.[NOM]</ta>
            <ta e="T206" id="Seg_7640" s="T205">Mehl-ACC</ta>
            <ta e="T207" id="Seg_7641" s="T206">bringen-TEMP-1SG</ta>
            <ta e="T208" id="Seg_7642" s="T207">Alte-EP-1SG.[NOM]</ta>
            <ta e="T209" id="Seg_7643" s="T208">Boden.[NOM]</ta>
            <ta e="T210" id="Seg_7644" s="T209">Erdhütte.[NOM]</ta>
            <ta e="T211" id="Seg_7645" s="T210">Haus-3SG-ACC</ta>
            <ta e="T212" id="Seg_7646" s="T211">heizen-TEMP-3SG</ta>
            <ta e="T213" id="Seg_7647" s="T212">Nase-1SG.[NOM]</ta>
            <ta e="T214" id="Seg_7648" s="T213">sich.wärmen-PRS.[3SG]</ta>
            <ta e="T215" id="Seg_7649" s="T214">jenes</ta>
            <ta e="T216" id="Seg_7650" s="T215">Mehl-DIM-3SG-INSTR</ta>
            <ta e="T217" id="Seg_7651" s="T216">Mehlschwitze.[NOM]</ta>
            <ta e="T218" id="Seg_7652" s="T217">machen-TEMP-3SG</ta>
            <ta e="T219" id="Seg_7653" s="T218">essen-NEG.CVB.SIM</ta>
            <ta e="T220" id="Seg_7654" s="T219">und</ta>
            <ta e="T221" id="Seg_7655" s="T220">einschlafen-PRS-1SG</ta>
            <ta e="T222" id="Seg_7656" s="T221">so</ta>
            <ta e="T223" id="Seg_7657" s="T222">süß-ABL</ta>
            <ta e="T224" id="Seg_7658" s="T223">süß.[NOM]</ta>
            <ta e="T225" id="Seg_7659" s="T224">schlafen-PTCP.PRS</ta>
            <ta e="T226" id="Seg_7660" s="T225">Schlaf.[NOM]</ta>
            <ta e="T227" id="Seg_7661" s="T226">sein-FUT-NEC.[3SG]</ta>
            <ta e="T228" id="Seg_7662" s="T227">Junge.[NOM]</ta>
            <ta e="T229" id="Seg_7663" s="T228">Zar.[NOM]</ta>
            <ta e="T230" id="Seg_7664" s="T229">sagen-PRS.[3SG]</ta>
            <ta e="T231" id="Seg_7665" s="T230">oh</ta>
            <ta e="T232" id="Seg_7666" s="T231">doch</ta>
            <ta e="T233" id="Seg_7667" s="T232">Verstand-PROPR</ta>
            <ta e="T234" id="Seg_7668" s="T233">Mensch-2SG</ta>
            <ta e="T235" id="Seg_7669" s="T234">sein-PST2.[3SG]</ta>
            <ta e="T236" id="Seg_7670" s="T235">wie</ta>
            <ta e="T237" id="Seg_7671" s="T236">sich.sättigen-CVB.SEQ-zurecht.kommen-CVB.SEQ</ta>
            <ta e="T238" id="Seg_7672" s="T237">liegen-PRS-2SG</ta>
            <ta e="T239" id="Seg_7673" s="T238">noch</ta>
            <ta e="T240" id="Seg_7674" s="T239">erraten.[IMP.2SG]</ta>
            <ta e="T241" id="Seg_7675" s="T240">mittlerer</ta>
            <ta e="T242" id="Seg_7676" s="T241">Welt-DAT/LOC</ta>
            <ta e="T243" id="Seg_7677" s="T242">schnell-ABL</ta>
            <ta e="T244" id="Seg_7678" s="T243">schnell.[NOM]</ta>
            <ta e="T245" id="Seg_7679" s="T244">was.[NOM]</ta>
            <ta e="T246" id="Seg_7680" s="T245">es.gibt=Q</ta>
            <ta e="T247" id="Seg_7681" s="T246">am.Morgen</ta>
            <ta e="T248" id="Seg_7682" s="T247">erraten-NEG-TEMP-2SG</ta>
            <ta e="T249" id="Seg_7683" s="T248">Hals-PROPR</ta>
            <ta e="T250" id="Seg_7684" s="T249">Kopf-2SG-ACC</ta>
            <ta e="T251" id="Seg_7685" s="T250">schneiden-PRS-1SG</ta>
            <ta e="T252" id="Seg_7686" s="T251">alter.Mann.[NOM]</ta>
            <ta e="T253" id="Seg_7687" s="T252">was-ACC</ta>
            <ta e="T254" id="Seg_7688" s="T253">NEG</ta>
            <ta e="T255" id="Seg_7689" s="T254">lösen-NEG.[3SG]</ta>
            <ta e="T256" id="Seg_7690" s="T255">ankommen-CVB.SEQ</ta>
            <ta e="T257" id="Seg_7691" s="T256">weinen-EP-MED-CVB.SIM</ta>
            <ta e="T258" id="Seg_7692" s="T257">sich.hinsetzen-TEMP-3SG</ta>
            <ta e="T259" id="Seg_7693" s="T258">Tochter-3SG.[NOM]</ta>
            <ta e="T260" id="Seg_7694" s="T259">fragen-PRS.[3SG]</ta>
            <ta e="T261" id="Seg_7695" s="T260">alter.Mann.[NOM]</ta>
            <ta e="T262" id="Seg_7696" s="T261">so-so</ta>
            <ta e="T263" id="Seg_7697" s="T262">sagen-PRS.[3SG]</ta>
            <ta e="T264" id="Seg_7698" s="T263">was</ta>
            <ta e="T265" id="Seg_7699" s="T264">Nutzen-POSS</ta>
            <ta e="T266" id="Seg_7700" s="T265">NEG-DAT/LOC</ta>
            <ta e="T267" id="Seg_7701" s="T266">weinen-PRS-2SG</ta>
            <ta e="T268" id="Seg_7702" s="T267">schlafen.[IMP.2SG]</ta>
            <ta e="T269" id="Seg_7703" s="T268">am.Morgen</ta>
            <ta e="T270" id="Seg_7704" s="T269">sprechen-FUT.[IMP.2SG]</ta>
            <ta e="T271" id="Seg_7705" s="T270">schnell-ABL</ta>
            <ta e="T272" id="Seg_7706" s="T271">schnell.[NOM]</ta>
            <ta e="T273" id="Seg_7707" s="T272">sagen-FUT.[IMP.2SG]</ta>
            <ta e="T274" id="Seg_7708" s="T273">schlecht</ta>
            <ta e="T275" id="Seg_7709" s="T274">Rauch-PROPR</ta>
            <ta e="T276" id="Seg_7710" s="T275">Boden.[NOM]</ta>
            <ta e="T277" id="Seg_7711" s="T276">Erdhütte-1SG-ABL</ta>
            <ta e="T278" id="Seg_7712" s="T277">hinausgehen-CVB.SIM</ta>
            <ta e="T279" id="Seg_7713" s="T278">rennen-CVB.SEQ</ta>
            <ta e="T280" id="Seg_7714" s="T279">Auge-1SG-ACC</ta>
            <ta e="T281" id="Seg_7715" s="T280">stochern-CAUS-EP-INTNS-EP-INTNS-CVB.SIM</ta>
            <ta e="T282" id="Seg_7716" s="T281">wischen-CVB.SEQ</ta>
            <ta e="T283" id="Seg_7717" s="T282">betrachten-CVB.SEQ</ta>
            <ta e="T284" id="Seg_7718" s="T283">sehen-TEMP-1SG</ta>
            <ta e="T285" id="Seg_7719" s="T284">Himmel.[NOM]</ta>
            <ta e="T286" id="Seg_7720" s="T285">zu</ta>
            <ta e="T287" id="Seg_7721" s="T286">den.Kopf.heben-TEMP-1SG</ta>
            <ta e="T288" id="Seg_7722" s="T287">Himmel.[NOM]</ta>
            <ta e="T289" id="Seg_7723" s="T288">Stern-3SG-ACC</ta>
            <ta e="T290" id="Seg_7724" s="T289">Auge-EP-1SG.[NOM]</ta>
            <ta e="T291" id="Seg_7725" s="T290">alles.ohne.Ausnahme</ta>
            <ta e="T292" id="Seg_7726" s="T291">sehen-PRS.[3SG]</ta>
            <ta e="T293" id="Seg_7727" s="T292">Erde.[NOM]</ta>
            <ta e="T294" id="Seg_7728" s="T293">zu</ta>
            <ta e="T295" id="Seg_7729" s="T294">sehen-TEMP-1SG</ta>
            <ta e="T296" id="Seg_7730" s="T295">Erde.[NOM]</ta>
            <ta e="T297" id="Seg_7731" s="T296">Gras-3SG-ACC-Baum-3SG-ACC</ta>
            <ta e="T298" id="Seg_7732" s="T297">alles.ohne.Ausnahme</ta>
            <ta e="T299" id="Seg_7733" s="T298">sehen-PRS-1SG</ta>
            <ta e="T300" id="Seg_7734" s="T299">Auge-EP-1SG.[NOM]</ta>
            <ta e="T301" id="Seg_7735" s="T300">nehmen-PTCP.PRS-EP-DAT/LOC</ta>
            <ta e="T302" id="Seg_7736" s="T301">wann</ta>
            <ta e="T303" id="Seg_7737" s="T302">NEG</ta>
            <ta e="T304" id="Seg_7738" s="T303">ankommen-NEG-1SG</ta>
            <ta e="T305" id="Seg_7739" s="T304">so</ta>
            <ta e="T306" id="Seg_7740" s="T305">schnell-ABL</ta>
            <ta e="T307" id="Seg_7741" s="T306">schnell.[NOM]</ta>
            <ta e="T308" id="Seg_7742" s="T307">Mensch.[NOM]</ta>
            <ta e="T309" id="Seg_7743" s="T308">sehen-NMNZ-3SG.[NOM]</ta>
            <ta e="T310" id="Seg_7744" s="T309">sagen-FUT.[IMP.2SG]</ta>
            <ta e="T311" id="Seg_7745" s="T310">jenes-ABL</ta>
            <ta e="T312" id="Seg_7746" s="T311">Rest-ACC</ta>
            <ta e="T313" id="Seg_7747" s="T312">3SG.[NOM]</ta>
            <ta e="T314" id="Seg_7748" s="T313">wissen-PTCP.PRS-3SG</ta>
            <ta e="T315" id="Seg_7749" s="T314">sein-FUT.[3SG]</ta>
            <ta e="T316" id="Seg_7750" s="T315">Q</ta>
            <ta e="T317" id="Seg_7751" s="T316">sagen-PRS.[3SG]</ta>
            <ta e="T318" id="Seg_7752" s="T317">Tochter.[NOM]</ta>
            <ta e="T319" id="Seg_7753" s="T318">alter.Mann.[NOM]</ta>
            <ta e="T320" id="Seg_7754" s="T319">einschlafen-CVB.SEQ</ta>
            <ta e="T321" id="Seg_7755" s="T320">bleiben-PST1-3SG</ta>
            <ta e="T322" id="Seg_7756" s="T321">Morgen</ta>
            <ta e="T323" id="Seg_7757" s="T322">aufstehen-CVB.SEQ</ta>
            <ta e="T324" id="Seg_7758" s="T323">Zar-3SG-DAT/LOC</ta>
            <ta e="T325" id="Seg_7759" s="T324">gehen-INFER-3SG</ta>
            <ta e="T326" id="Seg_7760" s="T325">Junge.[NOM]</ta>
            <ta e="T327" id="Seg_7761" s="T326">Zar.[NOM]</ta>
            <ta e="T328" id="Seg_7762" s="T327">fragen-PRS.[3SG]</ta>
            <ta e="T329" id="Seg_7763" s="T328">na</ta>
            <ta e="T330" id="Seg_7764" s="T329">erkennen-PST1-2SG</ta>
            <ta e="T331" id="Seg_7765" s="T330">Q</ta>
            <ta e="T332" id="Seg_7766" s="T331">hey</ta>
            <ta e="T333" id="Seg_7767" s="T332">erkennen-PTCP.PRS-DAT/LOC</ta>
            <ta e="T334" id="Seg_7768" s="T333">ähnlich</ta>
            <ta e="T335" id="Seg_7769" s="T334">machen-PST1-1SG</ta>
            <ta e="T336" id="Seg_7770" s="T335">richtig</ta>
            <ta e="T337" id="Seg_7771" s="T336">Q</ta>
            <ta e="T338" id="Seg_7772" s="T337">Fehler-3SG.[NOM]</ta>
            <ta e="T339" id="Seg_7773" s="T338">Q</ta>
            <ta e="T340" id="Seg_7774" s="T339">sagen-PRS.[3SG]</ta>
            <ta e="T341" id="Seg_7775" s="T340">schlecht</ta>
            <ta e="T342" id="Seg_7776" s="T341">Rauch-PROPR</ta>
            <ta e="T343" id="Seg_7777" s="T342">Boden.[NOM]</ta>
            <ta e="T344" id="Seg_7778" s="T343">Erdhütte-1SG-ABL</ta>
            <ta e="T345" id="Seg_7779" s="T344">hinausgehen-CVB.SIM</ta>
            <ta e="T346" id="Seg_7780" s="T345">rennen-CVB.SEQ</ta>
            <ta e="T347" id="Seg_7781" s="T346">Auge-1SG-ACC</ta>
            <ta e="T348" id="Seg_7782" s="T347">stochern-CAUS-EP-INTNS-EP-INTNS-CVB.SIM</ta>
            <ta e="T349" id="Seg_7783" s="T348">wischen-CVB.SEQ</ta>
            <ta e="T350" id="Seg_7784" s="T349">Sehen-VBZ-CVB.SEQ</ta>
            <ta e="T351" id="Seg_7785" s="T350">sehen-TEMP-1SG</ta>
            <ta e="T352" id="Seg_7786" s="T351">Himmel.[NOM]</ta>
            <ta e="T353" id="Seg_7787" s="T352">zu</ta>
            <ta e="T354" id="Seg_7788" s="T353">den.Kopf.heben-TEMP-1SG</ta>
            <ta e="T355" id="Seg_7789" s="T354">Himmel.[NOM]</ta>
            <ta e="T356" id="Seg_7790" s="T355">Stern-3SG-ACC</ta>
            <ta e="T357" id="Seg_7791" s="T356">Auge-EP-1SG.[NOM]</ta>
            <ta e="T358" id="Seg_7792" s="T357">alles.ohne.Ausnahme</ta>
            <ta e="T359" id="Seg_7793" s="T358">sehen-PRS.[3SG]</ta>
            <ta e="T360" id="Seg_7794" s="T359">Erde.[NOM]</ta>
            <ta e="T361" id="Seg_7795" s="T360">zu</ta>
            <ta e="T362" id="Seg_7796" s="T361">sehen-TEMP-1SG</ta>
            <ta e="T363" id="Seg_7797" s="T362">Erde.[NOM]</ta>
            <ta e="T364" id="Seg_7798" s="T363">Gras-3SG-ACC-Baum-3SG-ACC</ta>
            <ta e="T365" id="Seg_7799" s="T364">alles.ohne.Ausnahme</ta>
            <ta e="T366" id="Seg_7800" s="T365">sehen-PRS-1SG</ta>
            <ta e="T367" id="Seg_7801" s="T366">Auge-EP-1SG.[NOM]</ta>
            <ta e="T368" id="Seg_7802" s="T367">nehmen-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T369" id="Seg_7803" s="T368">wann</ta>
            <ta e="T370" id="Seg_7804" s="T369">NEG</ta>
            <ta e="T371" id="Seg_7805" s="T370">ankommen-NEG-1SG</ta>
            <ta e="T372" id="Seg_7806" s="T371">so</ta>
            <ta e="T373" id="Seg_7807" s="T372">schnell-ABL</ta>
            <ta e="T374" id="Seg_7808" s="T373">schnell.[NOM]</ta>
            <ta e="T375" id="Seg_7809" s="T374">Mensch.[NOM]</ta>
            <ta e="T376" id="Seg_7810" s="T375">sehen-NMNZ-3SG.[NOM]</ta>
            <ta e="T377" id="Seg_7811" s="T376">sein-PST2.[3SG]</ta>
            <ta e="T378" id="Seg_7812" s="T377">sagen-PRS.[3SG]</ta>
            <ta e="T379" id="Seg_7813" s="T378">Junge.[NOM]</ta>
            <ta e="T380" id="Seg_7814" s="T379">Zar.[NOM]</ta>
            <ta e="T381" id="Seg_7815" s="T380">sich.wundern-PRS.[3SG]</ta>
            <ta e="T382" id="Seg_7816" s="T381">oh</ta>
            <ta e="T383" id="Seg_7817" s="T382">alter.Mann.[NOM]</ta>
            <ta e="T384" id="Seg_7818" s="T383">doch</ta>
            <ta e="T385" id="Seg_7819" s="T384">Verstand-PROPR</ta>
            <ta e="T386" id="Seg_7820" s="T385">Mensch-2SG</ta>
            <ta e="T387" id="Seg_7821" s="T386">sein-PST2.[3SG]</ta>
            <ta e="T388" id="Seg_7822" s="T387">doch</ta>
            <ta e="T389" id="Seg_7823" s="T388">was.[NOM]</ta>
            <ta e="T390" id="Seg_7824" s="T389">und</ta>
            <ta e="T391" id="Seg_7825" s="T390">drei</ta>
            <ta e="T392" id="Seg_7826" s="T391">Mal-PROPR.[NOM]</ta>
            <ta e="T393" id="Seg_7827" s="T392">hinausgehen-CVB.SEQ</ta>
            <ta e="T394" id="Seg_7828" s="T393">Männchen.[NOM]</ta>
            <ta e="T395" id="Seg_7829" s="T394">Kuh-ACC</ta>
            <ta e="T396" id="Seg_7830" s="T395">greifen-CVB.SEQ</ta>
            <ta e="T397" id="Seg_7831" s="T396">geben-PRS.[3SG]</ta>
            <ta e="T398" id="Seg_7832" s="T397">dieses-ACC</ta>
            <ta e="T399" id="Seg_7833" s="T398">am.Morgen</ta>
            <ta e="T400" id="Seg_7834" s="T399">gebären-EP-CAUS-CVB.SEQ</ta>
            <ta e="T401" id="Seg_7835" s="T400">Kalb-3SG-ACC</ta>
            <ta e="T402" id="Seg_7836" s="T401">folgen-EP-MED-CAUS-CVB.SEQ</ta>
            <ta e="T403" id="Seg_7837" s="T402">bringen-FUT.[IMP.2SG]</ta>
            <ta e="T404" id="Seg_7838" s="T403">was.für.ein</ta>
            <ta e="T405" id="Seg_7839" s="T404">Mist-3SG-ACC</ta>
            <ta e="T406" id="Seg_7840" s="T405">sammeln-CVB.SEQ</ta>
            <ta e="T407" id="Seg_7841" s="T406">bringen-APPR-2SG</ta>
            <ta e="T408" id="Seg_7842" s="T407">sagen-PST2.[3SG]</ta>
            <ta e="T409" id="Seg_7843" s="T408">Junge.[NOM]</ta>
            <ta e="T410" id="Seg_7844" s="T409">Zar.[NOM]</ta>
            <ta e="T411" id="Seg_7845" s="T410">alter.Mann.[NOM]</ta>
            <ta e="T412" id="Seg_7846" s="T411">Qual-PROPR</ta>
            <ta e="T413" id="Seg_7847" s="T412">Männchen.[NOM]</ta>
            <ta e="T414" id="Seg_7848" s="T413">Kuh-3SG-ACC</ta>
            <ta e="T415" id="Seg_7849" s="T414">führen-CVB.SEQ</ta>
            <ta e="T416" id="Seg_7850" s="T415">ankommen-CVB.SEQ</ta>
            <ta e="T417" id="Seg_7851" s="T416">weinen-CVB.SIM</ta>
            <ta e="T418" id="Seg_7852" s="T417">sich.setzen-PST1-3SG</ta>
            <ta e="T419" id="Seg_7853" s="T418">Tochter-3SG.[NOM]</ta>
            <ta e="T420" id="Seg_7854" s="T419">fragen-PST1-3SG</ta>
            <ta e="T421" id="Seg_7855" s="T420">na</ta>
            <ta e="T422" id="Seg_7856" s="T421">Vater</ta>
            <ta e="T423" id="Seg_7857" s="T422">warum</ta>
            <ta e="T424" id="Seg_7858" s="T423">weinen-PST1-2SG</ta>
            <ta e="T425" id="Seg_7859" s="T424">na</ta>
            <ta e="T426" id="Seg_7860" s="T425">Männchen.[NOM]</ta>
            <ta e="T427" id="Seg_7861" s="T426">Kuh-ACC</ta>
            <ta e="T428" id="Seg_7862" s="T427">geben-PST1-3SG</ta>
            <ta e="T429" id="Seg_7863" s="T428">gebären-EP-CAUS-CVB.SEQ</ta>
            <ta e="T430" id="Seg_7864" s="T429">bringen.[IMP.2SG]</ta>
            <ta e="T431" id="Seg_7865" s="T430">sagen-PRS.[3SG]</ta>
            <ta e="T432" id="Seg_7866" s="T431">Männchen.[NOM]</ta>
            <ta e="T433" id="Seg_7867" s="T432">Kuh.[NOM]</ta>
            <ta e="T434" id="Seg_7868" s="T433">wie</ta>
            <ta e="T435" id="Seg_7869" s="T434">gebären-PTCP.HAB-ACC=Q</ta>
            <ta e="T436" id="Seg_7870" s="T435">Kindchen.[NOM]</ta>
            <ta e="T437" id="Seg_7871" s="T436">AFFIRM</ta>
            <ta e="T438" id="Seg_7872" s="T437">Männchen.[NOM]</ta>
            <ta e="T439" id="Seg_7873" s="T438">Kuh.[NOM]</ta>
            <ta e="T440" id="Seg_7874" s="T439">woher</ta>
            <ta e="T441" id="Seg_7875" s="T440">gebären-FUT.[3SG]=Q</ta>
            <ta e="T442" id="Seg_7876" s="T441">dafür</ta>
            <ta e="T443" id="Seg_7877" s="T442">Nahrung-VBZ-PST2-1PL</ta>
            <ta e="T444" id="Seg_7878" s="T443">hinausgehen-CVB.SEQ-2SG</ta>
            <ta e="T445" id="Seg_7879" s="T444">töten-INCH.[IMP.2SG]</ta>
            <ta e="T446" id="Seg_7880" s="T445">Fleisch-3SG-ACC</ta>
            <ta e="T447" id="Seg_7881" s="T446">essen-FUT-1PL</ta>
            <ta e="T448" id="Seg_7882" s="T447">sagen-PRS.[3SG]</ta>
            <ta e="T449" id="Seg_7883" s="T448">Tochter.[NOM]</ta>
            <ta e="T450" id="Seg_7884" s="T449">alter.Mann.[NOM]</ta>
            <ta e="T451" id="Seg_7885" s="T450">Männchen.[NOM]</ta>
            <ta e="T452" id="Seg_7886" s="T451">Kuh-3SG-ACC</ta>
            <ta e="T453" id="Seg_7887" s="T452">töten-CVB.SEQ</ta>
            <ta e="T454" id="Seg_7888" s="T453">werfen-PRS.[3SG]</ta>
            <ta e="T455" id="Seg_7889" s="T454">doch</ta>
            <ta e="T456" id="Seg_7890" s="T455">dieses-ACC</ta>
            <ta e="T457" id="Seg_7891" s="T456">essen-FUT-3PL</ta>
            <ta e="T458" id="Seg_7892" s="T457">Tochter-3SG.[NOM]</ta>
            <ta e="T459" id="Seg_7893" s="T458">Tasche-DAT/LOC</ta>
            <ta e="T460" id="Seg_7894" s="T459">Kuh.[NOM]</ta>
            <ta e="T461" id="Seg_7895" s="T460">Huf-3SG-ACC</ta>
            <ta e="T462" id="Seg_7896" s="T461">Knöchelbein-3SG-ACC</ta>
            <ta e="T463" id="Seg_7897" s="T462">sammeln-CVB.SEQ</ta>
            <ta e="T464" id="Seg_7898" s="T463">geben-PST1-3SG</ta>
            <ta e="T465" id="Seg_7899" s="T464">Vater-3SG-DAT/LOC</ta>
            <ta e="T466" id="Seg_7900" s="T465">doch</ta>
            <ta e="T467" id="Seg_7901" s="T466">dieses</ta>
            <ta e="T468" id="Seg_7902" s="T467">schwarz</ta>
            <ta e="T469" id="Seg_7903" s="T468">Huf-3SG.[NOM]</ta>
            <ta e="T470" id="Seg_7904" s="T469">Mutter-3SG.[NOM]</ta>
            <ta e="T471" id="Seg_7905" s="T470">Knöchelbein-3SG.[NOM]</ta>
            <ta e="T472" id="Seg_7906" s="T471">Kalb-3SG.[NOM]</ta>
            <ta e="T473" id="Seg_7907" s="T472">sagen-CVB.SEQ</ta>
            <ta e="T474" id="Seg_7908" s="T473">nachdem</ta>
            <ta e="T475" id="Seg_7909" s="T474">Zar-3SG-GEN</ta>
            <ta e="T476" id="Seg_7910" s="T475">Tisch-3SG-DAT/LOC</ta>
            <ta e="T477" id="Seg_7911" s="T476">Kind.[NOM]</ta>
            <ta e="T478" id="Seg_7912" s="T477">Spielzeug-3SG-ACC</ta>
            <ta e="T479" id="Seg_7913" s="T478">wie</ta>
            <ta e="T480" id="Seg_7914" s="T479">ausbreiten-FREQ-CVB.SEQ</ta>
            <ta e="T481" id="Seg_7915" s="T480">werfen-FUT.[IMP.2SG]</ta>
            <ta e="T482" id="Seg_7916" s="T481">Männchen.[NOM]</ta>
            <ta e="T483" id="Seg_7917" s="T482">Kuh-ABL</ta>
            <ta e="T484" id="Seg_7918" s="T483">gebären-PTCP.PRS</ta>
            <ta e="T485" id="Seg_7919" s="T484">nur</ta>
            <ta e="T486" id="Seg_7920" s="T485">Sache-3SG-ACC</ta>
            <ta e="T487" id="Seg_7921" s="T486">dieses</ta>
            <ta e="T488" id="Seg_7922" s="T487">finden-PST1-1SG</ta>
            <ta e="T489" id="Seg_7923" s="T488">sagen-FUT.[IMP.2SG]</ta>
            <ta e="T490" id="Seg_7924" s="T489">jenes-ABL</ta>
            <ta e="T491" id="Seg_7925" s="T490">Rest-ACC</ta>
            <ta e="T492" id="Seg_7926" s="T491">und</ta>
            <ta e="T493" id="Seg_7927" s="T492">3SG.[NOM]</ta>
            <ta e="T494" id="Seg_7928" s="T493">wissen-PTCP.PRS-3SG</ta>
            <ta e="T495" id="Seg_7929" s="T494">sein-FUT.[3SG]</ta>
            <ta e="T496" id="Seg_7930" s="T495">Q</ta>
            <ta e="T497" id="Seg_7931" s="T496">doch</ta>
            <ta e="T498" id="Seg_7932" s="T497">dann</ta>
            <ta e="T499" id="Seg_7933" s="T498">fordern-CVB.SEQ</ta>
            <ta e="T500" id="Seg_7934" s="T499">fragen-FUT.[3SG]</ta>
            <ta e="T501" id="Seg_7935" s="T500">selbst-2SG.[NOM]</ta>
            <ta e="T502" id="Seg_7936" s="T501">Verstand-2SG-INSTR</ta>
            <ta e="T503" id="Seg_7937" s="T502">machen-NEG-PST1-2SG</ta>
            <ta e="T504" id="Seg_7938" s="T503">wahrscheinlich</ta>
            <ta e="T505" id="Seg_7939" s="T504">dieses-APRX</ta>
            <ta e="T506" id="Seg_7940" s="T505">Verstand-PROPR-EP-2SG.[NOM]</ta>
            <ta e="T507" id="Seg_7941" s="T506">sein-COND.[3SG]</ta>
            <ta e="T508" id="Seg_7942" s="T507">1SG.[NOM]</ta>
            <ta e="T509" id="Seg_7943" s="T508">Harn-1SG-ACC-Kot-1SG-ACC</ta>
            <ta e="T510" id="Seg_7944" s="T509">säubern-CVB.SIM</ta>
            <ta e="T511" id="Seg_7945" s="T510">sich.sättigen-CVB.SEQ-zurecht.kommen-CVB.SEQ</ta>
            <ta e="T512" id="Seg_7946" s="T511">liegen-FUT.[3SG]</ta>
            <ta e="T513" id="Seg_7947" s="T512">NEG</ta>
            <ta e="T514" id="Seg_7948" s="T513">sein-PST1-2SG</ta>
            <ta e="T515" id="Seg_7949" s="T514">wer.[NOM]</ta>
            <ta e="T516" id="Seg_7950" s="T515">lehren-PRS.[3SG]</ta>
            <ta e="T517" id="Seg_7951" s="T516">sagen-FUT-3SG</ta>
            <ta e="T518" id="Seg_7952" s="T517">dort</ta>
            <ta e="T519" id="Seg_7953" s="T518">verstecken-APPR-2SG</ta>
            <ta e="T520" id="Seg_7954" s="T519">sagen-PRS.[3SG]</ta>
            <ta e="T521" id="Seg_7955" s="T520">Tochter-3SG.[NOM]</ta>
            <ta e="T522" id="Seg_7956" s="T521">schlecht</ta>
            <ta e="T523" id="Seg_7957" s="T522">Mädchen.[NOM]</ta>
            <ta e="T524" id="Seg_7958" s="T523">Kind-PROPR-1SG</ta>
            <ta e="T525" id="Seg_7959" s="T524">jenes.[NOM]</ta>
            <ta e="T526" id="Seg_7960" s="T525">lehren-FREQ-PRS.[3SG]</ta>
            <ta e="T527" id="Seg_7961" s="T526">sagen-FUT.[IMP.2SG]</ta>
            <ta e="T528" id="Seg_7962" s="T527">alter.Mann.[NOM]</ta>
            <ta e="T529" id="Seg_7963" s="T528">am.Morgen</ta>
            <ta e="T530" id="Seg_7964" s="T529">gehen-PRS.[3SG]</ta>
            <ta e="T531" id="Seg_7965" s="T530">Junge.[NOM]</ta>
            <ta e="T532" id="Seg_7966" s="T531">Zar.[NOM]</ta>
            <ta e="T533" id="Seg_7967" s="T532">fragen-PRS.[3SG]</ta>
            <ta e="T534" id="Seg_7968" s="T533">na</ta>
            <ta e="T535" id="Seg_7969" s="T534">erraten-PST1-2SG</ta>
            <ta e="T536" id="Seg_7970" s="T535">Q</ta>
            <ta e="T537" id="Seg_7971" s="T536">alter.Mann.[NOM]</ta>
            <ta e="T538" id="Seg_7972" s="T537">Kuh.[NOM]</ta>
            <ta e="T539" id="Seg_7973" s="T538">Huf-3SG-ACC</ta>
            <ta e="T540" id="Seg_7974" s="T539">Knöchelbein-3SG-ACC</ta>
            <ta e="T541" id="Seg_7975" s="T540">Tisch-DAT/LOC</ta>
            <ta e="T542" id="Seg_7976" s="T541">Kind.[NOM]</ta>
            <ta e="T543" id="Seg_7977" s="T542">Spielzeug-3SG-ACC</ta>
            <ta e="T544" id="Seg_7978" s="T543">wie</ta>
            <ta e="T545" id="Seg_7979" s="T544">ausbreiten-FREQ-CVB.SEQ</ta>
            <ta e="T546" id="Seg_7980" s="T545">werfen-PRS.[3SG]</ta>
            <ta e="T547" id="Seg_7981" s="T546">Männchen.[NOM]</ta>
            <ta e="T548" id="Seg_7982" s="T547">Kuh-ABL</ta>
            <ta e="T549" id="Seg_7983" s="T548">gebären-PTCP.PRS</ta>
            <ta e="T550" id="Seg_7984" s="T549">nur</ta>
            <ta e="T551" id="Seg_7985" s="T550">Sache-3SG-ACC</ta>
            <ta e="T552" id="Seg_7986" s="T551">dieses</ta>
            <ta e="T553" id="Seg_7987" s="T552">finden-PST1-1SG</ta>
            <ta e="T554" id="Seg_7988" s="T553">sagen-PRS.[3SG]</ta>
            <ta e="T555" id="Seg_7989" s="T554">Zar.[NOM]</ta>
            <ta e="T556" id="Seg_7990" s="T555">selbst-2SG.[NOM]</ta>
            <ta e="T557" id="Seg_7991" s="T556">Verstand-2SG-INSTR</ta>
            <ta e="T558" id="Seg_7992" s="T557">machen-NEG-PST1-2SG</ta>
            <ta e="T559" id="Seg_7993" s="T558">wahrscheinlich</ta>
            <ta e="T560" id="Seg_7994" s="T559">dieses-APRX</ta>
            <ta e="T561" id="Seg_7995" s="T560">Verstand-PROPR-EP-2SG.[NOM]</ta>
            <ta e="T562" id="Seg_7996" s="T561">sein-COND.[3SG]</ta>
            <ta e="T563" id="Seg_7997" s="T562">1SG.[NOM]</ta>
            <ta e="T564" id="Seg_7998" s="T563">Harn-1SG-ACC-Kot-1SG-ACC</ta>
            <ta e="T565" id="Seg_7999" s="T564">säubern-CVB.SIM</ta>
            <ta e="T566" id="Seg_8000" s="T565">sich.sättigen-CVB.SEQ-zurecht.kommen-CVB.SEQ</ta>
            <ta e="T567" id="Seg_8001" s="T566">liegen-FUT.[3SG]</ta>
            <ta e="T568" id="Seg_8002" s="T567">NEG</ta>
            <ta e="T569" id="Seg_8003" s="T568">sein-PST1-2SG</ta>
            <ta e="T570" id="Seg_8004" s="T569">wer.[NOM]</ta>
            <ta e="T571" id="Seg_8005" s="T570">lehren-PRS.[3SG]</ta>
            <ta e="T572" id="Seg_8006" s="T571">sagen-PRS.[3SG]</ta>
            <ta e="T573" id="Seg_8007" s="T572">schlecht</ta>
            <ta e="T574" id="Seg_8008" s="T573">Mädchen.[NOM]</ta>
            <ta e="T575" id="Seg_8009" s="T574">Kind-PROPR-1SG</ta>
            <ta e="T576" id="Seg_8010" s="T575">jenes.[NOM]</ta>
            <ta e="T577" id="Seg_8011" s="T576">lehren-FREQ-PRS.[3SG]</ta>
            <ta e="T578" id="Seg_8012" s="T577">sagen-PRS.[3SG]</ta>
            <ta e="T579" id="Seg_8013" s="T578">alter.Mann.[NOM]</ta>
            <ta e="T580" id="Seg_8014" s="T579">Junge.[NOM]</ta>
            <ta e="T581" id="Seg_8015" s="T580">Zar.[NOM]</ta>
            <ta e="T582" id="Seg_8016" s="T581">Mädchen-EP-2SG.[NOM]</ta>
            <ta e="T583" id="Seg_8017" s="T582">Verstand-PROPR</ta>
            <ta e="T584" id="Seg_8018" s="T583">Mädchen.[NOM]</ta>
            <ta e="T585" id="Seg_8019" s="T584">gebären-PST2.[3SG]</ta>
            <ta e="T586" id="Seg_8020" s="T585">sagen-PRS.[3SG]</ta>
            <ta e="T587" id="Seg_8021" s="T586">eisern</ta>
            <ta e="T588" id="Seg_8022" s="T587">Eimer-ACC</ta>
            <ta e="T589" id="Seg_8023" s="T588">Boden-3SG.[NOM]</ta>
            <ta e="T590" id="Seg_8024" s="T589">Loch-PROPR-ACC</ta>
            <ta e="T591" id="Seg_8025" s="T590">geben-PRS.[3SG]</ta>
            <ta e="T592" id="Seg_8026" s="T591">dieses-ACC</ta>
            <ta e="T593" id="Seg_8027" s="T592">flicken-CVB.SEQ</ta>
            <ta e="T594" id="Seg_8028" s="T593">schicken-IMP.3SG</ta>
            <ta e="T595" id="Seg_8029" s="T594">sagen-PRS.[3SG]</ta>
            <ta e="T596" id="Seg_8030" s="T595">jenes-ACC</ta>
            <ta e="T597" id="Seg_8031" s="T596">flicken-NEG-TEMP-3SG</ta>
            <ta e="T598" id="Seg_8032" s="T597">zwei-COLL-2PL-GEN</ta>
            <ta e="T599" id="Seg_8033" s="T598">Kopf-2PL-ACC</ta>
            <ta e="T600" id="Seg_8034" s="T599">schneiden-FUT-1SG</ta>
            <ta e="T601" id="Seg_8035" s="T600">sagen-PRS.[3SG]</ta>
            <ta e="T602" id="Seg_8036" s="T601">alter.Mann.[NOM]</ta>
            <ta e="T603" id="Seg_8037" s="T602">Eimer-ACC</ta>
            <ta e="T604" id="Seg_8038" s="T603">tragen-CVB.SIM</ta>
            <ta e="T605" id="Seg_8039" s="T604">gehen-PST2-3SG</ta>
            <ta e="T606" id="Seg_8040" s="T605">weinen-CVB.SIM-weinen-CVB.SIM</ta>
            <ta e="T607" id="Seg_8041" s="T606">Mädchen.[NOM]</ta>
            <ta e="T608" id="Seg_8042" s="T607">na</ta>
            <ta e="T609" id="Seg_8043" s="T608">Vater</ta>
            <ta e="T610" id="Seg_8044" s="T609">was-EP-2SG.[NOM]</ta>
            <ta e="T611" id="Seg_8045" s="T610">Eimer-3SG.[NOM]=Q</ta>
            <ta e="T612" id="Seg_8046" s="T611">sagen-PRS.[3SG]</ta>
            <ta e="T613" id="Seg_8047" s="T612">na-EMPH</ta>
            <ta e="T614" id="Seg_8048" s="T613">doch</ta>
            <ta e="T615" id="Seg_8049" s="T614">töten-PTCP.PRS</ta>
            <ta e="T616" id="Seg_8050" s="T615">Tag-3SG.[NOM]</ta>
            <ta e="T617" id="Seg_8051" s="T616">kommen-PST1-3SG</ta>
            <ta e="T618" id="Seg_8052" s="T617">dieses</ta>
            <ta e="T619" id="Seg_8053" s="T618">Eimer-ACC</ta>
            <ta e="T620" id="Seg_8054" s="T619">flicken-CVB.SEQ</ta>
            <ta e="T621" id="Seg_8055" s="T620">schicken-IMP.3SG</ta>
            <ta e="T622" id="Seg_8056" s="T621">sagen-PRS.[3SG]</ta>
            <ta e="T623" id="Seg_8057" s="T622">dieses-ACC</ta>
            <ta e="T624" id="Seg_8058" s="T623">flicken-NEG-TEMP-3SG</ta>
            <ta e="T625" id="Seg_8059" s="T624">zwei-COLL-2PL-GEN</ta>
            <ta e="T626" id="Seg_8060" s="T625">Kopf-2PL-ACC</ta>
            <ta e="T627" id="Seg_8061" s="T626">schneiden-FUT-1SG</ta>
            <ta e="T628" id="Seg_8062" s="T627">sagen-PRS.[3SG]</ta>
            <ta e="T629" id="Seg_8063" s="T628">Tochter.[NOM]</ta>
            <ta e="T630" id="Seg_8064" s="T629">sagen-PRS.[3SG]</ta>
            <ta e="T631" id="Seg_8065" s="T630">tragen.[IMP.2SG]</ta>
            <ta e="T632" id="Seg_8066" s="T631">zurück</ta>
            <ta e="T633" id="Seg_8067" s="T632">Frau.[NOM]</ta>
            <ta e="T634" id="Seg_8068" s="T633">Mensch.[NOM]</ta>
            <ta e="T635" id="Seg_8069" s="T634">flicken-FUT-1SG</ta>
            <ta e="T636" id="Seg_8070" s="T635">Mann.[NOM]</ta>
            <ta e="T637" id="Seg_8071" s="T636">Mensch.[NOM]</ta>
            <ta e="T638" id="Seg_8072" s="T637">Eimer-ACC</ta>
            <ta e="T639" id="Seg_8073" s="T638">leichte.Stiefel.aus.Rentierfell.[NOM]</ta>
            <ta e="T640" id="Seg_8074" s="T639">ähnlich</ta>
            <ta e="T641" id="Seg_8075" s="T640">auf.links.drehen-CVB.SEQ</ta>
            <ta e="T642" id="Seg_8076" s="T641">schicken-IMP.3SG</ta>
            <ta e="T643" id="Seg_8077" s="T642">alter.Mann.[NOM]</ta>
            <ta e="T644" id="Seg_8078" s="T643">tragen-CVB.SEQ</ta>
            <ta e="T645" id="Seg_8079" s="T644">geben-PRS.[3SG]</ta>
            <ta e="T646" id="Seg_8080" s="T645">Junge.[NOM]</ta>
            <ta e="T647" id="Seg_8081" s="T646">Zar.[NOM]</ta>
            <ta e="T648" id="Seg_8082" s="T647">sich.wundern-PRS.[3SG]</ta>
            <ta e="T649" id="Seg_8083" s="T648">alter.Mann-ACC</ta>
            <ta e="T650" id="Seg_8084" s="T649">schicken-PRS.[3SG]</ta>
            <ta e="T651" id="Seg_8085" s="T650">selbst-1SG.[NOM]</ta>
            <ta e="T652" id="Seg_8086" s="T651">Tochter-2SG-ACC</ta>
            <ta e="T653" id="Seg_8087" s="T652">sehen-CVB.SIM</ta>
            <ta e="T654" id="Seg_8088" s="T653">gehen-FUT-1SG</ta>
            <ta e="T655" id="Seg_8089" s="T654">sagen-PRS.[3SG]</ta>
            <ta e="T656" id="Seg_8090" s="T655">Junge.[NOM]</ta>
            <ta e="T657" id="Seg_8091" s="T656">Zar.[NOM]</ta>
            <ta e="T658" id="Seg_8092" s="T657">Tochter-ACC</ta>
            <ta e="T659" id="Seg_8093" s="T658">sehen-CVB.ANT</ta>
            <ta e="T660" id="Seg_8094" s="T659">lieben-PRS.[3SG]</ta>
            <ta e="T661" id="Seg_8095" s="T660">Frau.[NOM]</ta>
            <ta e="T662" id="Seg_8096" s="T661">nehmen-PRS.[3SG]</ta>
            <ta e="T663" id="Seg_8097" s="T662">wie.viel-ACC-wie.viel-ACC</ta>
            <ta e="T664" id="Seg_8098" s="T663">leben-PST2-3PL</ta>
            <ta e="T665" id="Seg_8099" s="T664">sein-PST1-3SG</ta>
            <ta e="T666" id="Seg_8100" s="T665">Frau-VBZ-PTCP.PST-3SG-ABL</ta>
            <ta e="T667" id="Seg_8101" s="T666">Junge.[NOM]</ta>
            <ta e="T668" id="Seg_8102" s="T667">Zar.[NOM]</ta>
            <ta e="T669" id="Seg_8103" s="T668">Puppe.[NOM]</ta>
            <ta e="T670" id="Seg_8104" s="T669">wie</ta>
            <ta e="T671" id="Seg_8105" s="T670">sich.setzen-ADVZ</ta>
            <ta e="T672" id="Seg_8106" s="T671">sein-PST1-3SG</ta>
            <ta e="T673" id="Seg_8107" s="T672">jeder-3SG-ACC</ta>
            <ta e="T674" id="Seg_8108" s="T673">Frau-3SG.[NOM]</ta>
            <ta e="T675" id="Seg_8109" s="T674">raten-PRS.[3SG]</ta>
            <ta e="T676" id="Seg_8110" s="T675">entscheiden-PRS.[3SG]</ta>
            <ta e="T677" id="Seg_8111" s="T676">Junge.[NOM]</ta>
            <ta e="T678" id="Seg_8112" s="T677">Zar.[NOM]</ta>
            <ta e="T679" id="Seg_8113" s="T678">Zunge-VBZ-PRS.[3SG]</ta>
            <ta e="T680" id="Seg_8114" s="T679">Freund</ta>
            <ta e="T681" id="Seg_8115" s="T680">dieses</ta>
            <ta e="T682" id="Seg_8116" s="T681">eins</ta>
            <ta e="T683" id="Seg_8117" s="T682">Land-DAT/LOC</ta>
            <ta e="T684" id="Seg_8118" s="T683">zwei</ta>
            <ta e="T685" id="Seg_8119" s="T684">Zar.[NOM]</ta>
            <ta e="T686" id="Seg_8120" s="T685">sein-PTCP.PRS-1PL.[NOM]</ta>
            <ta e="T687" id="Seg_8121" s="T686">vorkommen-EP-NEG.[3SG]</ta>
            <ta e="T688" id="Seg_8122" s="T687">1PL.[NOM]</ta>
            <ta e="T689" id="Seg_8123" s="T688">sich.trennen-IMP.1DU</ta>
            <ta e="T690" id="Seg_8124" s="T689">Frau-3SG.[NOM]</ta>
            <ta e="T691" id="Seg_8125" s="T690">doch</ta>
            <ta e="T692" id="Seg_8126" s="T691">sich.trennen-IMP.1DU</ta>
            <ta e="T693" id="Seg_8127" s="T692">Q</ta>
            <ta e="T694" id="Seg_8128" s="T693">sagen-PRS.[3SG]</ta>
            <ta e="T695" id="Seg_8129" s="T694">Junge.[NOM]</ta>
            <ta e="T696" id="Seg_8130" s="T695">Zar.[NOM]</ta>
            <ta e="T697" id="Seg_8131" s="T696">was</ta>
            <ta e="T698" id="Seg_8132" s="T697">Wunsch-PROPR</ta>
            <ta e="T699" id="Seg_8133" s="T698">Sache-2SG-ACC</ta>
            <ta e="T700" id="Seg_8134" s="T699">jeder-3SG-ACC</ta>
            <ta e="T701" id="Seg_8135" s="T700">nehmen.[IMP.2SG]</ta>
            <ta e="T702" id="Seg_8136" s="T701">sagen-PRS.[3SG]</ta>
            <ta e="T703" id="Seg_8137" s="T702">Frau.[NOM]</ta>
            <ta e="T704" id="Seg_8138" s="T703">einverstanden.sein-EP-RECP/COLL-PRS.[3SG]</ta>
            <ta e="T705" id="Seg_8139" s="T704">sich.trennen-PTCP.PRS</ta>
            <ta e="T706" id="Seg_8140" s="T705">Nacht-3PL.[NOM]=EMPH</ta>
            <ta e="T707" id="Seg_8141" s="T706">werden-PRS.[3SG]</ta>
            <ta e="T708" id="Seg_8142" s="T707">Mann-3SG.[NOM]</ta>
            <ta e="T709" id="Seg_8143" s="T708">einschlafen-CVB.SEQ</ta>
            <ta e="T710" id="Seg_8144" s="T709">bleiben-PRS.[3SG]</ta>
            <ta e="T711" id="Seg_8145" s="T710">Frau.[NOM]</ta>
            <ta e="T712" id="Seg_8146" s="T711">Ehemann-3SG-ACC</ta>
            <ta e="T713" id="Seg_8147" s="T712">Bettdecke-PL-ACC</ta>
            <ta e="T714" id="Seg_8148" s="T713">Bettwäsche-PL-ACC</ta>
            <ta e="T715" id="Seg_8149" s="T714">Vater-3SG-GEN</ta>
            <ta e="T716" id="Seg_8150" s="T715">Boden.[NOM]</ta>
            <ta e="T717" id="Seg_8151" s="T716">Erdhütte.[NOM]</ta>
            <ta e="T718" id="Seg_8152" s="T717">Haus-3SG-DAT/LOC</ta>
            <ta e="T719" id="Seg_8153" s="T718">heben-CVB.SEQ</ta>
            <ta e="T720" id="Seg_8154" s="T719">tragen-PRS.[3SG]</ta>
            <ta e="T721" id="Seg_8155" s="T720">Junge.[NOM]</ta>
            <ta e="T722" id="Seg_8156" s="T721">Zar.[NOM]</ta>
            <ta e="T723" id="Seg_8157" s="T722">am.Morgen</ta>
            <ta e="T724" id="Seg_8158" s="T723">aufwachen-CVB.SEQ</ta>
            <ta e="T725" id="Seg_8159" s="T724">erschrecken-PRS.[3SG]</ta>
            <ta e="T726" id="Seg_8160" s="T725">sich.strecken-CVB.SEQ</ta>
            <ta e="T727" id="Seg_8161" s="T726">sehen-PST2-3SG</ta>
            <ta e="T728" id="Seg_8162" s="T727">Kopf-3SG.[NOM]-Bein-3SG.[NOM]</ta>
            <ta e="T729" id="Seg_8163" s="T728">Erdhütte.[NOM]</ta>
            <ta e="T730" id="Seg_8164" s="T729">Holz-3SG-DAT/LOC</ta>
            <ta e="T731" id="Seg_8165" s="T730">stoßen-PRS-3PL</ta>
            <ta e="T732" id="Seg_8166" s="T731">na-EMPH</ta>
            <ta e="T733" id="Seg_8167" s="T732">dieses</ta>
            <ta e="T734" id="Seg_8168" s="T733">wo</ta>
            <ta e="T735" id="Seg_8169" s="T734">es.gibt-1PL=Q</ta>
            <ta e="T736" id="Seg_8170" s="T735">warum</ta>
            <ta e="T737" id="Seg_8171" s="T736">kommen-PST2-EP-1SG=Q</ta>
            <ta e="T738" id="Seg_8172" s="T737">Frau-3SG-ACC</ta>
            <ta e="T739" id="Seg_8173" s="T738">stoßen-FREQ-CVB.SEQ</ta>
            <ta e="T740" id="Seg_8174" s="T739">aufwachen-CAUS-PRS.[3SG]</ta>
            <ta e="T741" id="Seg_8175" s="T740">Frau-3SG.[NOM]</ta>
            <ta e="T742" id="Seg_8176" s="T741">na</ta>
            <ta e="T743" id="Seg_8177" s="T742">Freund</ta>
            <ta e="T744" id="Seg_8178" s="T743">1SG.[NOM]</ta>
            <ta e="T745" id="Seg_8179" s="T744">bringen-PST2-EP-1SG</ta>
            <ta e="T746" id="Seg_8180" s="T745">sagen-PRS.[3SG]</ta>
            <ta e="T747" id="Seg_8181" s="T746">warum</ta>
            <ta e="T748" id="Seg_8182" s="T747">bringen-PST2-2SG=Q</ta>
            <ta e="T749" id="Seg_8183" s="T748">ach</ta>
            <ta e="T750" id="Seg_8184" s="T749">selbst-2SG-ACC</ta>
            <ta e="T751" id="Seg_8185" s="T750">wollen-PST1-1SG</ta>
            <ta e="T752" id="Seg_8186" s="T751">und</ta>
            <ta e="T753" id="Seg_8187" s="T752">selbst-2SG-ACC</ta>
            <ta e="T754" id="Seg_8188" s="T753">bringen-PST1-1SG</ta>
            <ta e="T755" id="Seg_8189" s="T754">was-ACC</ta>
            <ta e="T756" id="Seg_8190" s="T755">wollen-PTCP.PRS-2SG-ACC</ta>
            <ta e="T757" id="Seg_8191" s="T756">nehmen-FUT.[IMP.2SG]</ta>
            <ta e="T758" id="Seg_8192" s="T757">sagen-PST2-EP-2SG</ta>
            <ta e="T759" id="Seg_8193" s="T758">EMPH</ta>
            <ta e="T760" id="Seg_8194" s="T759">sagen-PRS.[3SG]</ta>
            <ta e="T761" id="Seg_8195" s="T760">Frau.[NOM]</ta>
            <ta e="T762" id="Seg_8196" s="T761">Junge.[NOM]</ta>
            <ta e="T763" id="Seg_8197" s="T762">Zar.[NOM]</ta>
            <ta e="T764" id="Seg_8198" s="T763">können-CVB.SEQ</ta>
            <ta e="T765" id="Seg_8199" s="T764">weggehen-NEG-PST1-3SG</ta>
            <ta e="T766" id="Seg_8200" s="T765">doch</ta>
            <ta e="T767" id="Seg_8201" s="T766">Frau-3SG.[NOM]</ta>
            <ta e="T768" id="Seg_8202" s="T767">sagen-PRS.[3SG]</ta>
            <ta e="T769" id="Seg_8203" s="T768">so.viel</ta>
            <ta e="T770" id="Seg_8204" s="T769">Verstand-PROPR</ta>
            <ta e="T771" id="Seg_8205" s="T770">Mensch.[NOM]</ta>
            <ta e="T772" id="Seg_8206" s="T771">sein-NEG.PTCP.PST</ta>
            <ta e="T773" id="Seg_8207" s="T772">sein-PST2-2SG</ta>
            <ta e="T774" id="Seg_8208" s="T773">Volk-2SG-ACC</ta>
            <ta e="T775" id="Seg_8209" s="T774">raten-CVB.SEQ</ta>
            <ta e="T776" id="Seg_8210" s="T775">selbst-2SG.[NOM]</ta>
            <ta e="T777" id="Seg_8211" s="T776">leben.[IMP.2SG]</ta>
            <ta e="T778" id="Seg_8212" s="T777">1SG.[NOM]</ta>
            <ta e="T779" id="Seg_8213" s="T778">einfach</ta>
            <ta e="T780" id="Seg_8214" s="T779">Hausfrau.[NOM]</ta>
            <ta e="T781" id="Seg_8215" s="T780">sein-CVB.SEQ</ta>
            <ta e="T782" id="Seg_8216" s="T781">sitzen-FUT-1SG</ta>
            <ta e="T783" id="Seg_8217" s="T782">arm</ta>
            <ta e="T784" id="Seg_8218" s="T783">Bauer.[NOM]</ta>
            <ta e="T785" id="Seg_8219" s="T784">Mädchen-3SG-ACC</ta>
            <ta e="T786" id="Seg_8220" s="T785">gut-3SG-INSTR</ta>
            <ta e="T787" id="Seg_8221" s="T786">Spiegel.[NOM]</ta>
            <ta e="T788" id="Seg_8222" s="T787">Stein.[NOM]</ta>
            <ta e="T789" id="Seg_8223" s="T788">Haus-DAT/LOC</ta>
            <ta e="T790" id="Seg_8224" s="T789">leben-INFER-3SG</ta>
            <ta e="T791" id="Seg_8225" s="T790">Boden.[NOM]</ta>
            <ta e="T792" id="Seg_8226" s="T791">Erdhütte-ABL</ta>
            <ta e="T793" id="Seg_8227" s="T792">sich.trennen-CVB.SEQ</ta>
            <ta e="T794" id="Seg_8228" s="T793">Ende-EP-ABL</ta>
            <ta e="T795" id="Seg_8229" s="T794">reich.sein-CVB.SEQ-reich.sein-CVB.SEQ</ta>
            <ta e="T796" id="Seg_8230" s="T795">leben-INFER-3PL</ta>
            <ta e="T797" id="Seg_8231" s="T796">doch</ta>
            <ta e="T798" id="Seg_8232" s="T797">letzter-3SG.[NOM]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_8233" s="T0">вот</ta>
            <ta e="T2" id="Seg_8234" s="T1">мальчик.[NOM]</ta>
            <ta e="T3" id="Seg_8235" s="T2">царь.[NOM]</ta>
            <ta e="T4" id="Seg_8236" s="T3">жить-PST2-3SG</ta>
            <ta e="T5" id="Seg_8237" s="T4">соседтсво-3SG-DAT/LOC</ta>
            <ta e="T6" id="Seg_8238" s="T5">нищий</ta>
            <ta e="T7" id="Seg_8239" s="T6">крестьянин-PROPR.[NOM]</ta>
            <ta e="T8" id="Seg_8240" s="T7">нищий</ta>
            <ta e="T9" id="Seg_8241" s="T8">крестьянин.[NOM]</ta>
            <ta e="T10" id="Seg_8242" s="T9">этот</ta>
            <ta e="T11" id="Seg_8243" s="T10">царь-DAT/LOC</ta>
            <ta e="T12" id="Seg_8244" s="T11">работать-CVB.SEQ</ta>
            <ta e="T13" id="Seg_8245" s="T12">утренний</ta>
            <ta e="T14" id="Seg_8246" s="T13">рассвет-ABL</ta>
            <ta e="T15" id="Seg_8247" s="T14">вечер.[NOM]</ta>
            <ta e="T16" id="Seg_8248" s="T15">рассвет-DAT/LOC</ta>
            <ta e="T17" id="Seg_8249" s="T16">пока</ta>
            <ta e="T18" id="Seg_8250" s="T17">один</ta>
            <ta e="T19" id="Seg_8251" s="T18">ложка.[NOM]</ta>
            <ta e="T20" id="Seg_8252" s="T19">мука.[NOM]</ta>
            <ta e="T21" id="Seg_8253" s="T20">для</ta>
            <ta e="T22" id="Seg_8254" s="T21">работать-PTCP.PRS</ta>
            <ta e="T23" id="Seg_8255" s="T22">быть-PST2.[3SG]</ta>
            <ta e="T24" id="Seg_8256" s="T23">этот-INSTR</ta>
            <ta e="T25" id="Seg_8257" s="T24">кормить-REFL-CVB.SEQ</ta>
            <ta e="T26" id="Seg_8258" s="T25">жить-PRS-3PL</ta>
            <ta e="T27" id="Seg_8259" s="T26">нищий</ta>
            <ta e="T28" id="Seg_8260" s="T27">крестьянин.[NOM]</ta>
            <ta e="T29" id="Seg_8261" s="T28">один</ta>
            <ta e="T30" id="Seg_8262" s="T29">девушка.[NOM]</ta>
            <ta e="T31" id="Seg_8263" s="T30">ребенок-PROPR.[NOM]</ta>
            <ta e="T32" id="Seg_8264" s="T31">вот</ta>
            <ta e="T33" id="Seg_8265" s="T32">однажды</ta>
            <ta e="T34" id="Seg_8266" s="T33">мальчик.[NOM]</ta>
            <ta e="T35" id="Seg_8267" s="T34">царь.[NOM]</ta>
            <ta e="T36" id="Seg_8268" s="T35">говорить-PRS.[3SG]</ta>
            <ta e="T37" id="Seg_8269" s="T36">эй</ta>
            <ta e="T38" id="Seg_8270" s="T37">вот</ta>
            <ta e="T39" id="Seg_8271" s="T38">нищий</ta>
            <ta e="T40" id="Seg_8272" s="T39">крестьянин.[NOM]</ta>
            <ta e="T41" id="Seg_8273" s="T40">сколько</ta>
            <ta e="T42" id="Seg_8274" s="T41">ум-PROPR</ta>
            <ta e="T43" id="Seg_8275" s="T42">человек.[NOM]</ta>
            <ta e="T44" id="Seg_8276" s="T43">родить-CVB.SEQ</ta>
            <ta e="T45" id="Seg_8277" s="T44">возвращаться-PST1-2SG</ta>
            <ta e="T46" id="Seg_8278" s="T45">загадка-PART</ta>
            <ta e="T47" id="Seg_8279" s="T46">догадываться.[IMP.2SG]</ta>
            <ta e="T48" id="Seg_8280" s="T47">сладкий-ABL</ta>
            <ta e="T49" id="Seg_8281" s="T48">сладкий.[NOM]</ta>
            <ta e="T50" id="Seg_8282" s="T49">что.[NOM]=Q</ta>
            <ta e="T51" id="Seg_8283" s="T50">утром</ta>
            <ta e="T52" id="Seg_8284" s="T51">приходить-CVB.SEQ</ta>
            <ta e="T53" id="Seg_8285" s="T52">девять</ta>
            <ta e="T54" id="Seg_8286" s="T53">час-DAT/LOC</ta>
            <ta e="T55" id="Seg_8287" s="T54">говорить-FUT.[IMP.2SG]</ta>
            <ta e="T56" id="Seg_8288" s="T55">тот-ACC</ta>
            <ta e="T57" id="Seg_8289" s="T56">догадываться-NEG-TEMP-2SG</ta>
            <ta e="T58" id="Seg_8290" s="T57">повесить-PTCP.PRS</ta>
            <ta e="T59" id="Seg_8291" s="T58">дерево-1SG-DAT/LOC</ta>
            <ta e="T60" id="Seg_8292" s="T59">шея-PROPR</ta>
            <ta e="T61" id="Seg_8293" s="T60">голова-2SG-ACC</ta>
            <ta e="T62" id="Seg_8294" s="T61">резать-FUT-1SG</ta>
            <ta e="T63" id="Seg_8295" s="T62">нищий</ta>
            <ta e="T64" id="Seg_8296" s="T63">крестьянин.[NOM]</ta>
            <ta e="T65" id="Seg_8297" s="T64">почему</ta>
            <ta e="T66" id="Seg_8298" s="T65">NEG</ta>
            <ta e="T67" id="Seg_8299" s="T66">придумать-CVB.SIM</ta>
            <ta e="T68" id="Seg_8300" s="T67">мочь-NEG.[3SG]</ta>
            <ta e="T69" id="Seg_8301" s="T68">сладкий</ta>
            <ta e="T70" id="Seg_8302" s="T69">пища-ACC</ta>
            <ta e="T71" id="Seg_8303" s="T70">знать-EMOT-NEG.[3SG]</ta>
            <ta e="T72" id="Seg_8304" s="T71">царь.[NOM]</ta>
            <ta e="T73" id="Seg_8305" s="T72">пища-3SG.[NOM]</ta>
            <ta e="T74" id="Seg_8306" s="T73">каждый-3SG.[NOM]</ta>
            <ta e="T75" id="Seg_8307" s="T74">сладкий.[NOM]</ta>
            <ta e="T76" id="Seg_8308" s="T75">как</ta>
            <ta e="T77" id="Seg_8309" s="T76">3SG-DAT/LOC</ta>
            <ta e="T78" id="Seg_8310" s="T77">дом-3SG-DAT/LOC</ta>
            <ta e="T79" id="Seg_8311" s="T78">приходить-CVB.SEQ</ta>
            <ta e="T80" id="Seg_8312" s="T79">плакать-EP-MED-CVB.SIM</ta>
            <ta e="T81" id="Seg_8313" s="T80">сесть-PST1-3SG</ta>
            <ta e="T82" id="Seg_8314" s="T81">дочь-3SG.[NOM]</ta>
            <ta e="T83" id="Seg_8315" s="T82">спрашивать-PRS.[3SG]</ta>
            <ta e="T84" id="Seg_8316" s="T83">эй</ta>
            <ta e="T85" id="Seg_8317" s="T84">отец</ta>
            <ta e="T86" id="Seg_8318" s="T85">почему</ta>
            <ta e="T87" id="Seg_8319" s="T86">плакать-PST1-2SG</ta>
            <ta e="T88" id="Seg_8320" s="T87">вот</ta>
            <ta e="T89" id="Seg_8321" s="T88">друг-EP-1SG.[NOM]</ta>
            <ta e="T90" id="Seg_8322" s="T89">идти-EP-PST2-EP-1SG</ta>
            <ta e="T91" id="Seg_8323" s="T90">только</ta>
            <ta e="T92" id="Seg_8324" s="T91">жить-PTCP.PST-EP-1SG.[NOM]</ta>
            <ta e="T93" id="Seg_8325" s="T92">конец-3SG.[NOM]</ta>
            <ta e="T94" id="Seg_8326" s="T93">мальчик.[NOM]</ta>
            <ta e="T95" id="Seg_8327" s="T94">царь.[NOM]</ta>
            <ta e="T96" id="Seg_8328" s="T95">загадка.[NOM]</ta>
            <ta e="T97" id="Seg_8329" s="T96">давать-PST1-3SG</ta>
            <ta e="T98" id="Seg_8330" s="T97">узнавать-NEG-TEMP-1SG</ta>
            <ta e="T99" id="Seg_8331" s="T98">шея-PROPR</ta>
            <ta e="T100" id="Seg_8332" s="T99">голова-1SG-ACC</ta>
            <ta e="T101" id="Seg_8333" s="T100">резать-PTCP.FUT</ta>
            <ta e="T102" id="Seg_8334" s="T101">быть-PST1-3SG</ta>
            <ta e="T103" id="Seg_8335" s="T102">что-ACC</ta>
            <ta e="T104" id="Seg_8336" s="T103">узнавать-CAUS-CAUS-PRS.[3SG]</ta>
            <ta e="T105" id="Seg_8337" s="T104">тот.[NOM]</ta>
            <ta e="T106" id="Seg_8338" s="T105">сладкий-ABL</ta>
            <ta e="T107" id="Seg_8339" s="T106">сладкий.[NOM]</ta>
            <ta e="T108" id="Seg_8340" s="T107">что.[NOM]=Q</ta>
            <ta e="T109" id="Seg_8341" s="T108">говорить-PRS.[3SG]</ta>
            <ta e="T110" id="Seg_8342" s="T109">говорить-PST2.[3SG]</ta>
            <ta e="T111" id="Seg_8343" s="T110">ээ</ta>
            <ta e="T112" id="Seg_8344" s="T111">отец</ta>
            <ta e="T113" id="Seg_8345" s="T112">польза-POSS</ta>
            <ta e="T114" id="Seg_8346" s="T113">NEG-DAT/LOC</ta>
            <ta e="T115" id="Seg_8347" s="T114">разве</ta>
            <ta e="T116" id="Seg_8348" s="T115">плакать-PRS-2SG</ta>
            <ta e="T117" id="Seg_8349" s="T116">спать-EMOT.[IMP.2SG]</ta>
            <ta e="T118" id="Seg_8350" s="T117">отдыхать.[IMP.2SG]</ta>
            <ta e="T119" id="Seg_8351" s="T118">сладкий-ABL</ta>
            <ta e="T120" id="Seg_8352" s="T119">сладкий-ACC</ta>
            <ta e="T121" id="Seg_8353" s="T120">знать-PTCP.PRS</ta>
            <ta e="T122" id="Seg_8354" s="T121">дело-EP-1SG.[NOM]</ta>
            <ta e="T123" id="Seg_8355" s="T122">один</ta>
            <ta e="T124" id="Seg_8356" s="T123">говорить-FUT.[IMP.2SG]</ta>
            <ta e="T125" id="Seg_8357" s="T124">утренний</ta>
            <ta e="T126" id="Seg_8358" s="T125">рассвет-ABL</ta>
            <ta e="T127" id="Seg_8359" s="T126">вечерний</ta>
            <ta e="T128" id="Seg_8360" s="T127">рассвет-DAT/LOC</ta>
            <ta e="T129" id="Seg_8361" s="T128">пока</ta>
            <ta e="T130" id="Seg_8362" s="T129">работать-CVB.SEQ</ta>
            <ta e="T131" id="Seg_8363" s="T130">2SG.[NOM]</ta>
            <ta e="T132" id="Seg_8364" s="T131">кал-2SG-ACC-грязь-2SG-ACC</ta>
            <ta e="T133" id="Seg_8365" s="T132">очистить-CVB.SEQ</ta>
            <ta e="T134" id="Seg_8366" s="T133">один</ta>
            <ta e="T135" id="Seg_8367" s="T134">ложка.[NOM]</ta>
            <ta e="T136" id="Seg_8368" s="T135">мука-ACC</ta>
            <ta e="T137" id="Seg_8369" s="T136">приносить-TEMP-1SG</ta>
            <ta e="T138" id="Seg_8370" s="T137">старуха-EP-1SG.[NOM]</ta>
            <ta e="T139" id="Seg_8371" s="T138">земля.[NOM]</ta>
            <ta e="T140" id="Seg_8372" s="T139">землянка.[NOM]</ta>
            <ta e="T141" id="Seg_8373" s="T140">дом-3SG-ACC</ta>
            <ta e="T142" id="Seg_8374" s="T141">топить-TEMP-3SG</ta>
            <ta e="T143" id="Seg_8375" s="T142">нос-1SG.[NOM]</ta>
            <ta e="T144" id="Seg_8376" s="T143">греться-PRS.[3SG]</ta>
            <ta e="T145" id="Seg_8377" s="T144">тот</ta>
            <ta e="T146" id="Seg_8378" s="T145">мука-DIM-3SG-INSTR</ta>
            <ta e="T147" id="Seg_8379" s="T146">толокно.[NOM]</ta>
            <ta e="T148" id="Seg_8380" s="T147">делать-TEMP-3SG</ta>
            <ta e="T149" id="Seg_8381" s="T148">есть-NEG.CVB.SIM</ta>
            <ta e="T150" id="Seg_8382" s="T149">да</ta>
            <ta e="T151" id="Seg_8383" s="T150">уснуть-PRS-1SG</ta>
            <ta e="T152" id="Seg_8384" s="T151">так</ta>
            <ta e="T153" id="Seg_8385" s="T152">сладкий-ABL</ta>
            <ta e="T154" id="Seg_8386" s="T153">сладкий.[NOM]</ta>
            <ta e="T155" id="Seg_8387" s="T154">спать-PTCP.PRS</ta>
            <ta e="T156" id="Seg_8388" s="T155">сон.[NOM]</ta>
            <ta e="T157" id="Seg_8389" s="T156">быть-FUT-NEC.[3SG]</ta>
            <ta e="T158" id="Seg_8390" s="T157">говорить-FUT.[IMP.2SG]</ta>
            <ta e="T159" id="Seg_8391" s="T158">3SG.[NOM]</ta>
            <ta e="T160" id="Seg_8392" s="T159">тот-ABL</ta>
            <ta e="T161" id="Seg_8393" s="T160">да</ta>
            <ta e="T162" id="Seg_8394" s="T161">остаток-ACC</ta>
            <ta e="T163" id="Seg_8395" s="T162">знать-PTCP.PRS-3SG</ta>
            <ta e="T164" id="Seg_8396" s="T163">быть-FUT.[3SG]</ta>
            <ta e="T165" id="Seg_8397" s="T164">Q</ta>
            <ta e="T166" id="Seg_8398" s="T165">говорить-PST2.[3SG]</ta>
            <ta e="T167" id="Seg_8399" s="T166">дочь-3SG.[NOM]</ta>
            <ta e="T168" id="Seg_8400" s="T167">старик.[NOM]</ta>
            <ta e="T169" id="Seg_8401" s="T168">уснуть-CVB.SEQ</ta>
            <ta e="T170" id="Seg_8402" s="T169">оставаться-PST1-3SG</ta>
            <ta e="T171" id="Seg_8403" s="T170">утром</ta>
            <ta e="T172" id="Seg_8404" s="T171">рано</ta>
            <ta e="T173" id="Seg_8405" s="T172">вставать-CVB.SEQ</ta>
            <ta e="T174" id="Seg_8406" s="T173">девять</ta>
            <ta e="T175" id="Seg_8407" s="T174">час-DAT/LOC</ta>
            <ta e="T176" id="Seg_8408" s="T175">царь-DAT/LOC</ta>
            <ta e="T177" id="Seg_8409" s="T176">доезжать-PRS.[3SG]</ta>
            <ta e="T178" id="Seg_8410" s="T177">мальчик.[NOM]</ta>
            <ta e="T179" id="Seg_8411" s="T178">царь.[NOM]</ta>
            <ta e="T180" id="Seg_8412" s="T179">спрашивать-PRS.[3SG]</ta>
            <ta e="T181" id="Seg_8413" s="T180">эй</ta>
            <ta e="T182" id="Seg_8414" s="T181">догадываться-PST1-2SG</ta>
            <ta e="T183" id="Seg_8415" s="T182">Q</ta>
            <ta e="T184" id="Seg_8416" s="T183">ээ</ta>
            <ta e="T185" id="Seg_8417" s="T184">узнавать-PTCP.PRS-DAT/LOC</ta>
            <ta e="T186" id="Seg_8418" s="T185">подобно</ta>
            <ta e="T187" id="Seg_8419" s="T186">делать-PST1-1SG</ta>
            <ta e="T188" id="Seg_8420" s="T187">правильно</ta>
            <ta e="T189" id="Seg_8421" s="T188">Q</ta>
            <ta e="T190" id="Seg_8422" s="T189">ошибка-3SG.[NOM]</ta>
            <ta e="T191" id="Seg_8423" s="T190">Q</ta>
            <ta e="T192" id="Seg_8424" s="T191">говорить-PRS.[3SG]</ta>
            <ta e="T193" id="Seg_8425" s="T192">вот</ta>
            <ta e="T194" id="Seg_8426" s="T193">что-ACC=Q</ta>
            <ta e="T195" id="Seg_8427" s="T194">утренний</ta>
            <ta e="T196" id="Seg_8428" s="T195">рассвет-ABL</ta>
            <ta e="T197" id="Seg_8429" s="T196">вечерний</ta>
            <ta e="T198" id="Seg_8430" s="T197">рассвет-DAT/LOC</ta>
            <ta e="T199" id="Seg_8431" s="T198">пока</ta>
            <ta e="T200" id="Seg_8432" s="T199">работать-CVB.SEQ</ta>
            <ta e="T201" id="Seg_8433" s="T200">2SG.[NOM]</ta>
            <ta e="T202" id="Seg_8434" s="T201">кал-2SG-ACC-грязь-2SG-ACC</ta>
            <ta e="T203" id="Seg_8435" s="T202">очистить-CVB.SEQ</ta>
            <ta e="T204" id="Seg_8436" s="T203">один</ta>
            <ta e="T205" id="Seg_8437" s="T204">ложка.[NOM]</ta>
            <ta e="T206" id="Seg_8438" s="T205">мука-ACC</ta>
            <ta e="T207" id="Seg_8439" s="T206">приносить-TEMP-1SG</ta>
            <ta e="T208" id="Seg_8440" s="T207">старуха-EP-1SG.[NOM]</ta>
            <ta e="T209" id="Seg_8441" s="T208">земля.[NOM]</ta>
            <ta e="T210" id="Seg_8442" s="T209">землянка.[NOM]</ta>
            <ta e="T211" id="Seg_8443" s="T210">дом-3SG-ACC</ta>
            <ta e="T212" id="Seg_8444" s="T211">топить-TEMP-3SG</ta>
            <ta e="T213" id="Seg_8445" s="T212">нос-1SG.[NOM]</ta>
            <ta e="T214" id="Seg_8446" s="T213">греться-PRS.[3SG]</ta>
            <ta e="T215" id="Seg_8447" s="T214">тот</ta>
            <ta e="T216" id="Seg_8448" s="T215">мука-DIM-3SG-INSTR</ta>
            <ta e="T217" id="Seg_8449" s="T216">толокно.[NOM]</ta>
            <ta e="T218" id="Seg_8450" s="T217">делать-TEMP-3SG</ta>
            <ta e="T219" id="Seg_8451" s="T218">есть-NEG.CVB.SIM</ta>
            <ta e="T220" id="Seg_8452" s="T219">да</ta>
            <ta e="T221" id="Seg_8453" s="T220">уснуть-PRS-1SG</ta>
            <ta e="T222" id="Seg_8454" s="T221">так</ta>
            <ta e="T223" id="Seg_8455" s="T222">сладкий-ABL</ta>
            <ta e="T224" id="Seg_8456" s="T223">сладкий.[NOM]</ta>
            <ta e="T225" id="Seg_8457" s="T224">спать-PTCP.PRS</ta>
            <ta e="T226" id="Seg_8458" s="T225">сон.[NOM]</ta>
            <ta e="T227" id="Seg_8459" s="T226">быть-FUT-NEC.[3SG]</ta>
            <ta e="T228" id="Seg_8460" s="T227">мальчик.[NOM]</ta>
            <ta e="T229" id="Seg_8461" s="T228">царь.[NOM]</ta>
            <ta e="T230" id="Seg_8462" s="T229">говорить-PRS.[3SG]</ta>
            <ta e="T231" id="Seg_8463" s="T230">о</ta>
            <ta e="T232" id="Seg_8464" s="T231">вот</ta>
            <ta e="T233" id="Seg_8465" s="T232">ум-PROPR</ta>
            <ta e="T234" id="Seg_8466" s="T233">человек-2SG</ta>
            <ta e="T235" id="Seg_8467" s="T234">быть-PST2.[3SG]</ta>
            <ta e="T236" id="Seg_8468" s="T235">как</ta>
            <ta e="T237" id="Seg_8469" s="T236">пропитываться-CVB.SEQ-уживаться-CVB.SEQ</ta>
            <ta e="T238" id="Seg_8470" s="T237">лежать-PRS-2SG</ta>
            <ta e="T239" id="Seg_8471" s="T238">еще</ta>
            <ta e="T240" id="Seg_8472" s="T239">догадываться.[IMP.2SG]</ta>
            <ta e="T241" id="Seg_8473" s="T240">средний</ta>
            <ta e="T242" id="Seg_8474" s="T241">мир-DAT/LOC</ta>
            <ta e="T243" id="Seg_8475" s="T242">быстрый-ABL</ta>
            <ta e="T244" id="Seg_8476" s="T243">быстрый.[NOM]</ta>
            <ta e="T245" id="Seg_8477" s="T244">что.[NOM]</ta>
            <ta e="T246" id="Seg_8478" s="T245">есть=Q</ta>
            <ta e="T247" id="Seg_8479" s="T246">утром</ta>
            <ta e="T248" id="Seg_8480" s="T247">догадываться-NEG-TEMP-2SG</ta>
            <ta e="T249" id="Seg_8481" s="T248">шея-PROPR</ta>
            <ta e="T250" id="Seg_8482" s="T249">голова-2SG-ACC</ta>
            <ta e="T251" id="Seg_8483" s="T250">резать-PRS-1SG</ta>
            <ta e="T252" id="Seg_8484" s="T251">старик.[NOM]</ta>
            <ta e="T253" id="Seg_8485" s="T252">что-ACC</ta>
            <ta e="T254" id="Seg_8486" s="T253">NEG</ta>
            <ta e="T255" id="Seg_8487" s="T254">решать-NEG.[3SG]</ta>
            <ta e="T256" id="Seg_8488" s="T255">доезжать-CVB.SEQ</ta>
            <ta e="T257" id="Seg_8489" s="T256">плакать-EP-MED-CVB.SIM</ta>
            <ta e="T258" id="Seg_8490" s="T257">сесть-TEMP-3SG</ta>
            <ta e="T259" id="Seg_8491" s="T258">дочь-3SG.[NOM]</ta>
            <ta e="T260" id="Seg_8492" s="T259">спрашивать-PRS.[3SG]</ta>
            <ta e="T261" id="Seg_8493" s="T260">старик.[NOM]</ta>
            <ta e="T262" id="Seg_8494" s="T261">так-так</ta>
            <ta e="T263" id="Seg_8495" s="T262">говорить-PRS.[3SG]</ta>
            <ta e="T264" id="Seg_8496" s="T263">что</ta>
            <ta e="T265" id="Seg_8497" s="T264">польза-POSS</ta>
            <ta e="T266" id="Seg_8498" s="T265">NEG-DAT/LOC</ta>
            <ta e="T267" id="Seg_8499" s="T266">плакать-PRS-2SG</ta>
            <ta e="T268" id="Seg_8500" s="T267">спать.[IMP.2SG]</ta>
            <ta e="T269" id="Seg_8501" s="T268">утром</ta>
            <ta e="T270" id="Seg_8502" s="T269">говорить-FUT.[IMP.2SG]</ta>
            <ta e="T271" id="Seg_8503" s="T270">быстрый-ABL</ta>
            <ta e="T272" id="Seg_8504" s="T271">быстрый.[NOM]</ta>
            <ta e="T273" id="Seg_8505" s="T272">говорить-FUT.[IMP.2SG]</ta>
            <ta e="T274" id="Seg_8506" s="T273">плохой</ta>
            <ta e="T275" id="Seg_8507" s="T274">дым-PROPR</ta>
            <ta e="T276" id="Seg_8508" s="T275">земля.[NOM]</ta>
            <ta e="T277" id="Seg_8509" s="T276">землянка-1SG-ABL</ta>
            <ta e="T278" id="Seg_8510" s="T277">выйти-CVB.SIM</ta>
            <ta e="T279" id="Seg_8511" s="T278">бежать-CVB.SEQ</ta>
            <ta e="T280" id="Seg_8512" s="T279">глаз-1SG-ACC</ta>
            <ta e="T281" id="Seg_8513" s="T280">ковырять-CAUS-EP-INTNS-EP-INTNS-CVB.SIM</ta>
            <ta e="T282" id="Seg_8514" s="T281">вытирать-CVB.SEQ</ta>
            <ta e="T283" id="Seg_8515" s="T282">смотреть-CVB.SEQ</ta>
            <ta e="T284" id="Seg_8516" s="T283">видеть-TEMP-1SG</ta>
            <ta e="T285" id="Seg_8517" s="T284">небо.[NOM]</ta>
            <ta e="T286" id="Seg_8518" s="T285">к</ta>
            <ta e="T287" id="Seg_8519" s="T286">поднимать.голову-TEMP-1SG</ta>
            <ta e="T288" id="Seg_8520" s="T287">небо.[NOM]</ta>
            <ta e="T289" id="Seg_8521" s="T288">звезда-3SG-ACC</ta>
            <ta e="T290" id="Seg_8522" s="T289">глаз-EP-1SG.[NOM]</ta>
            <ta e="T291" id="Seg_8523" s="T290">все.до.единого</ta>
            <ta e="T292" id="Seg_8524" s="T291">видеть-PRS.[3SG]</ta>
            <ta e="T293" id="Seg_8525" s="T292">земля.[NOM]</ta>
            <ta e="T294" id="Seg_8526" s="T293">к</ta>
            <ta e="T295" id="Seg_8527" s="T294">видеть-TEMP-1SG</ta>
            <ta e="T296" id="Seg_8528" s="T295">земля.[NOM]</ta>
            <ta e="T297" id="Seg_8529" s="T296">трава-3SG-ACC-дерево-3SG-ACC</ta>
            <ta e="T298" id="Seg_8530" s="T297">все.до.единого</ta>
            <ta e="T299" id="Seg_8531" s="T298">видеть-PRS-1SG</ta>
            <ta e="T300" id="Seg_8532" s="T299">глаз-EP-1SG.[NOM]</ta>
            <ta e="T301" id="Seg_8533" s="T300">взять-PTCP.PRS-EP-DAT/LOC</ta>
            <ta e="T302" id="Seg_8534" s="T301">когда</ta>
            <ta e="T303" id="Seg_8535" s="T302">NEG</ta>
            <ta e="T304" id="Seg_8536" s="T303">доезжать-NEG-1SG</ta>
            <ta e="T305" id="Seg_8537" s="T304">так</ta>
            <ta e="T306" id="Seg_8538" s="T305">быстрый-ABL</ta>
            <ta e="T307" id="Seg_8539" s="T306">быстрый.[NOM]</ta>
            <ta e="T308" id="Seg_8540" s="T307">человек.[NOM]</ta>
            <ta e="T309" id="Seg_8541" s="T308">видеть-NMNZ-3SG.[NOM]</ta>
            <ta e="T310" id="Seg_8542" s="T309">говорить-FUT.[IMP.2SG]</ta>
            <ta e="T311" id="Seg_8543" s="T310">тот-ABL</ta>
            <ta e="T312" id="Seg_8544" s="T311">остаток-ACC</ta>
            <ta e="T313" id="Seg_8545" s="T312">3SG.[NOM]</ta>
            <ta e="T314" id="Seg_8546" s="T313">знать-PTCP.PRS-3SG</ta>
            <ta e="T315" id="Seg_8547" s="T314">быть-FUT.[3SG]</ta>
            <ta e="T316" id="Seg_8548" s="T315">Q</ta>
            <ta e="T317" id="Seg_8549" s="T316">говорить-PRS.[3SG]</ta>
            <ta e="T318" id="Seg_8550" s="T317">дочь.[NOM]</ta>
            <ta e="T319" id="Seg_8551" s="T318">старик.[NOM]</ta>
            <ta e="T320" id="Seg_8552" s="T319">уснуть-CVB.SEQ</ta>
            <ta e="T321" id="Seg_8553" s="T320">оставаться-PST1-3SG</ta>
            <ta e="T322" id="Seg_8554" s="T321">утро</ta>
            <ta e="T323" id="Seg_8555" s="T322">вставать-CVB.SEQ</ta>
            <ta e="T324" id="Seg_8556" s="T323">царь-3SG-DAT/LOC</ta>
            <ta e="T325" id="Seg_8557" s="T324">идти-INFER-3SG</ta>
            <ta e="T326" id="Seg_8558" s="T325">мальчик.[NOM]</ta>
            <ta e="T327" id="Seg_8559" s="T326">царь.[NOM]</ta>
            <ta e="T328" id="Seg_8560" s="T327">спрашивать-PRS.[3SG]</ta>
            <ta e="T329" id="Seg_8561" s="T328">эй</ta>
            <ta e="T330" id="Seg_8562" s="T329">узнавать-PST1-2SG</ta>
            <ta e="T331" id="Seg_8563" s="T330">Q</ta>
            <ta e="T332" id="Seg_8564" s="T331">ээ</ta>
            <ta e="T333" id="Seg_8565" s="T332">узнавать-PTCP.PRS-DAT/LOC</ta>
            <ta e="T334" id="Seg_8566" s="T333">подобно</ta>
            <ta e="T335" id="Seg_8567" s="T334">делать-PST1-1SG</ta>
            <ta e="T336" id="Seg_8568" s="T335">правильно</ta>
            <ta e="T337" id="Seg_8569" s="T336">Q</ta>
            <ta e="T338" id="Seg_8570" s="T337">ошибка-3SG.[NOM]</ta>
            <ta e="T339" id="Seg_8571" s="T338">Q</ta>
            <ta e="T340" id="Seg_8572" s="T339">говорить-PRS.[3SG]</ta>
            <ta e="T341" id="Seg_8573" s="T340">плохой</ta>
            <ta e="T342" id="Seg_8574" s="T341">дым-PROPR</ta>
            <ta e="T343" id="Seg_8575" s="T342">земля.[NOM]</ta>
            <ta e="T344" id="Seg_8576" s="T343">землянка-1SG-ABL</ta>
            <ta e="T345" id="Seg_8577" s="T344">выйти-CVB.SIM</ta>
            <ta e="T346" id="Seg_8578" s="T345">бежать-CVB.SEQ</ta>
            <ta e="T347" id="Seg_8579" s="T346">глаз-1SG-ACC</ta>
            <ta e="T348" id="Seg_8580" s="T347">ковырять-CAUS-EP-INTNS-EP-INTNS-CVB.SIM</ta>
            <ta e="T349" id="Seg_8581" s="T348">вытирать-CVB.SEQ</ta>
            <ta e="T350" id="Seg_8582" s="T349">видение-VBZ-CVB.SEQ</ta>
            <ta e="T351" id="Seg_8583" s="T350">видеть-TEMP-1SG</ta>
            <ta e="T352" id="Seg_8584" s="T351">небо.[NOM]</ta>
            <ta e="T353" id="Seg_8585" s="T352">к</ta>
            <ta e="T354" id="Seg_8586" s="T353">поднимать.голову-TEMP-1SG</ta>
            <ta e="T355" id="Seg_8587" s="T354">небо.[NOM]</ta>
            <ta e="T356" id="Seg_8588" s="T355">звезда-3SG-ACC</ta>
            <ta e="T357" id="Seg_8589" s="T356">глаз-EP-1SG.[NOM]</ta>
            <ta e="T358" id="Seg_8590" s="T357">все.до.единого</ta>
            <ta e="T359" id="Seg_8591" s="T358">видеть-PRS.[3SG]</ta>
            <ta e="T360" id="Seg_8592" s="T359">земля.[NOM]</ta>
            <ta e="T361" id="Seg_8593" s="T360">к</ta>
            <ta e="T362" id="Seg_8594" s="T361">видеть-TEMP-1SG</ta>
            <ta e="T363" id="Seg_8595" s="T362">земля.[NOM]</ta>
            <ta e="T364" id="Seg_8596" s="T363">трава-3SG-ACC-дерево-3SG-ACC</ta>
            <ta e="T365" id="Seg_8597" s="T364">все.до.единого</ta>
            <ta e="T366" id="Seg_8598" s="T365">видеть-PRS-1SG</ta>
            <ta e="T367" id="Seg_8599" s="T366">глаз-EP-1SG.[NOM]</ta>
            <ta e="T368" id="Seg_8600" s="T367">взять-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T369" id="Seg_8601" s="T368">когда</ta>
            <ta e="T370" id="Seg_8602" s="T369">NEG</ta>
            <ta e="T371" id="Seg_8603" s="T370">доезжать-NEG-1SG</ta>
            <ta e="T372" id="Seg_8604" s="T371">так</ta>
            <ta e="T373" id="Seg_8605" s="T372">быстрый-ABL</ta>
            <ta e="T374" id="Seg_8606" s="T373">быстрый.[NOM]</ta>
            <ta e="T375" id="Seg_8607" s="T374">человек.[NOM]</ta>
            <ta e="T376" id="Seg_8608" s="T375">видеть-NMNZ-3SG.[NOM]</ta>
            <ta e="T377" id="Seg_8609" s="T376">быть-PST2.[3SG]</ta>
            <ta e="T378" id="Seg_8610" s="T377">говорить-PRS.[3SG]</ta>
            <ta e="T379" id="Seg_8611" s="T378">мальчик.[NOM]</ta>
            <ta e="T380" id="Seg_8612" s="T379">царь.[NOM]</ta>
            <ta e="T381" id="Seg_8613" s="T380">удивляться-PRS.[3SG]</ta>
            <ta e="T382" id="Seg_8614" s="T381">о</ta>
            <ta e="T383" id="Seg_8615" s="T382">старик.[NOM]</ta>
            <ta e="T384" id="Seg_8616" s="T383">вот</ta>
            <ta e="T385" id="Seg_8617" s="T384">ум-PROPR</ta>
            <ta e="T386" id="Seg_8618" s="T385">человек-2SG</ta>
            <ta e="T387" id="Seg_8619" s="T386">быть-PST2.[3SG]</ta>
            <ta e="T388" id="Seg_8620" s="T387">вот</ta>
            <ta e="T389" id="Seg_8621" s="T388">что.[NOM]</ta>
            <ta e="T390" id="Seg_8622" s="T389">да</ta>
            <ta e="T391" id="Seg_8623" s="T390">три</ta>
            <ta e="T392" id="Seg_8624" s="T391">раз-PROPR.[NOM]</ta>
            <ta e="T393" id="Seg_8625" s="T392">выйти-CVB.SEQ</ta>
            <ta e="T394" id="Seg_8626" s="T393">самец.[NOM]</ta>
            <ta e="T395" id="Seg_8627" s="T394">корова-ACC</ta>
            <ta e="T396" id="Seg_8628" s="T395">хватать-CVB.SEQ</ta>
            <ta e="T397" id="Seg_8629" s="T396">давать-PRS.[3SG]</ta>
            <ta e="T398" id="Seg_8630" s="T397">этот-ACC</ta>
            <ta e="T399" id="Seg_8631" s="T398">утром</ta>
            <ta e="T400" id="Seg_8632" s="T399">родить-EP-CAUS-CVB.SEQ</ta>
            <ta e="T401" id="Seg_8633" s="T400">теленок-3SG-ACC</ta>
            <ta e="T402" id="Seg_8634" s="T401">следовать-EP-MED-CAUS-CVB.SEQ</ta>
            <ta e="T403" id="Seg_8635" s="T402">принести-FUT.[IMP.2SG]</ta>
            <ta e="T404" id="Seg_8636" s="T403">какой</ta>
            <ta e="T405" id="Seg_8637" s="T404">помет-3SG-ACC</ta>
            <ta e="T406" id="Seg_8638" s="T405">собирать-CVB.SEQ</ta>
            <ta e="T407" id="Seg_8639" s="T406">принести-APPR-2SG</ta>
            <ta e="T408" id="Seg_8640" s="T407">говорить-PST2.[3SG]</ta>
            <ta e="T409" id="Seg_8641" s="T408">мальчик.[NOM]</ta>
            <ta e="T410" id="Seg_8642" s="T409">царь.[NOM]</ta>
            <ta e="T411" id="Seg_8643" s="T410">старик.[NOM]</ta>
            <ta e="T412" id="Seg_8644" s="T411">мука-PROPR</ta>
            <ta e="T413" id="Seg_8645" s="T412">самец.[NOM]</ta>
            <ta e="T414" id="Seg_8646" s="T413">корова-3SG-ACC</ta>
            <ta e="T415" id="Seg_8647" s="T414">водить-CVB.SEQ</ta>
            <ta e="T416" id="Seg_8648" s="T415">доезжать-CVB.SEQ</ta>
            <ta e="T417" id="Seg_8649" s="T416">плакать-CVB.SIM</ta>
            <ta e="T418" id="Seg_8650" s="T417">сесть-PST1-3SG</ta>
            <ta e="T419" id="Seg_8651" s="T418">дочь-3SG.[NOM]</ta>
            <ta e="T420" id="Seg_8652" s="T419">спрашивать-PST1-3SG</ta>
            <ta e="T421" id="Seg_8653" s="T420">эй</ta>
            <ta e="T422" id="Seg_8654" s="T421">отец</ta>
            <ta e="T423" id="Seg_8655" s="T422">почему</ta>
            <ta e="T424" id="Seg_8656" s="T423">плакать-PST1-2SG</ta>
            <ta e="T425" id="Seg_8657" s="T424">эй</ta>
            <ta e="T426" id="Seg_8658" s="T425">самец.[NOM]</ta>
            <ta e="T427" id="Seg_8659" s="T426">корова-ACC</ta>
            <ta e="T428" id="Seg_8660" s="T427">давать-PST1-3SG</ta>
            <ta e="T429" id="Seg_8661" s="T428">родить-EP-CAUS-CVB.SEQ</ta>
            <ta e="T430" id="Seg_8662" s="T429">принести.[IMP.2SG]</ta>
            <ta e="T431" id="Seg_8663" s="T430">говорить-PRS.[3SG]</ta>
            <ta e="T432" id="Seg_8664" s="T431">самец.[NOM]</ta>
            <ta e="T433" id="Seg_8665" s="T432">корова.[NOM]</ta>
            <ta e="T434" id="Seg_8666" s="T433">как</ta>
            <ta e="T435" id="Seg_8667" s="T434">родить-PTCP.HAB-ACC=Q</ta>
            <ta e="T436" id="Seg_8668" s="T435">детенок.[NOM]</ta>
            <ta e="T437" id="Seg_8669" s="T436">AFFIRM</ta>
            <ta e="T438" id="Seg_8670" s="T437">самец.[NOM]</ta>
            <ta e="T439" id="Seg_8671" s="T438">корова.[NOM]</ta>
            <ta e="T440" id="Seg_8672" s="T439">откуда</ta>
            <ta e="T441" id="Seg_8673" s="T440">родить-FUT.[3SG]=Q</ta>
            <ta e="T442" id="Seg_8674" s="T441">зато</ta>
            <ta e="T443" id="Seg_8675" s="T442">пища-VBZ-PST2-1PL</ta>
            <ta e="T444" id="Seg_8676" s="T443">выйти-CVB.SEQ-2SG</ta>
            <ta e="T445" id="Seg_8677" s="T444">убить-INCH.[IMP.2SG]</ta>
            <ta e="T446" id="Seg_8678" s="T445">мясо-3SG-ACC</ta>
            <ta e="T447" id="Seg_8679" s="T446">есть-FUT-1PL</ta>
            <ta e="T448" id="Seg_8680" s="T447">говорить-PRS.[3SG]</ta>
            <ta e="T449" id="Seg_8681" s="T448">дочь.[NOM]</ta>
            <ta e="T450" id="Seg_8682" s="T449">старик.[NOM]</ta>
            <ta e="T451" id="Seg_8683" s="T450">самец.[NOM]</ta>
            <ta e="T452" id="Seg_8684" s="T451">корова-3SG-ACC</ta>
            <ta e="T453" id="Seg_8685" s="T452">убить-CVB.SEQ</ta>
            <ta e="T454" id="Seg_8686" s="T453">бросать-PRS.[3SG]</ta>
            <ta e="T455" id="Seg_8687" s="T454">вот</ta>
            <ta e="T456" id="Seg_8688" s="T455">этот-ACC</ta>
            <ta e="T457" id="Seg_8689" s="T456">есть-FUT-3PL</ta>
            <ta e="T458" id="Seg_8690" s="T457">дочь-3SG.[NOM]</ta>
            <ta e="T459" id="Seg_8691" s="T458">сума-DAT/LOC</ta>
            <ta e="T460" id="Seg_8692" s="T459">корова.[NOM]</ta>
            <ta e="T461" id="Seg_8693" s="T460">копыто-3SG-ACC</ta>
            <ta e="T462" id="Seg_8694" s="T461">бабка-3SG-ACC</ta>
            <ta e="T463" id="Seg_8695" s="T462">собирать-CVB.SEQ</ta>
            <ta e="T464" id="Seg_8696" s="T463">давать-PST1-3SG</ta>
            <ta e="T465" id="Seg_8697" s="T464">отец-3SG-DAT/LOC</ta>
            <ta e="T466" id="Seg_8698" s="T465">вот</ta>
            <ta e="T467" id="Seg_8699" s="T466">тот</ta>
            <ta e="T468" id="Seg_8700" s="T467">черный</ta>
            <ta e="T469" id="Seg_8701" s="T468">копыто-3SG.[NOM]</ta>
            <ta e="T470" id="Seg_8702" s="T469">мать-3SG.[NOM]</ta>
            <ta e="T471" id="Seg_8703" s="T470">бабка-3SG.[NOM]</ta>
            <ta e="T472" id="Seg_8704" s="T471">теленок-3SG.[NOM]</ta>
            <ta e="T473" id="Seg_8705" s="T472">говорить-CVB.SEQ</ta>
            <ta e="T474" id="Seg_8706" s="T473">после</ta>
            <ta e="T475" id="Seg_8707" s="T474">царь-3SG-GEN</ta>
            <ta e="T476" id="Seg_8708" s="T475">стол-3SG-DAT/LOC</ta>
            <ta e="T477" id="Seg_8709" s="T476">ребенок.[NOM]</ta>
            <ta e="T478" id="Seg_8710" s="T477">игрушки-3SG-ACC</ta>
            <ta e="T479" id="Seg_8711" s="T478">как</ta>
            <ta e="T480" id="Seg_8712" s="T479">размотать-FREQ-CVB.SEQ</ta>
            <ta e="T481" id="Seg_8713" s="T480">бросать-FUT.[IMP.2SG]</ta>
            <ta e="T482" id="Seg_8714" s="T481">самец.[NOM]</ta>
            <ta e="T483" id="Seg_8715" s="T482">корова-ABL</ta>
            <ta e="T484" id="Seg_8716" s="T483">родить-PTCP.PRS</ta>
            <ta e="T485" id="Seg_8717" s="T484">только</ta>
            <ta e="T486" id="Seg_8718" s="T485">дело-3SG-ACC</ta>
            <ta e="T487" id="Seg_8719" s="T486">этот</ta>
            <ta e="T488" id="Seg_8720" s="T487">найти-PST1-1SG</ta>
            <ta e="T489" id="Seg_8721" s="T488">говорить-FUT.[IMP.2SG]</ta>
            <ta e="T490" id="Seg_8722" s="T489">тот-ABL</ta>
            <ta e="T491" id="Seg_8723" s="T490">остаток-ACC</ta>
            <ta e="T492" id="Seg_8724" s="T491">да</ta>
            <ta e="T493" id="Seg_8725" s="T492">3SG.[NOM]</ta>
            <ta e="T494" id="Seg_8726" s="T493">знать-PTCP.PRS-3SG</ta>
            <ta e="T495" id="Seg_8727" s="T494">быть-FUT.[3SG]</ta>
            <ta e="T496" id="Seg_8728" s="T495">Q</ta>
            <ta e="T497" id="Seg_8729" s="T496">вот</ta>
            <ta e="T498" id="Seg_8730" s="T497">тогда</ta>
            <ta e="T499" id="Seg_8731" s="T498">требовать-CVB.SEQ</ta>
            <ta e="T500" id="Seg_8732" s="T499">спрашивать-FUT.[3SG]</ta>
            <ta e="T501" id="Seg_8733" s="T500">сам-2SG.[NOM]</ta>
            <ta e="T502" id="Seg_8734" s="T501">ум-2SG-INSTR</ta>
            <ta e="T503" id="Seg_8735" s="T502">делать-NEG-PST1-2SG</ta>
            <ta e="T504" id="Seg_8736" s="T503">наверное</ta>
            <ta e="T505" id="Seg_8737" s="T504">тот-APRX</ta>
            <ta e="T506" id="Seg_8738" s="T505">ум-PROPR-EP-2SG.[NOM]</ta>
            <ta e="T507" id="Seg_8739" s="T506">быть-COND.[3SG]</ta>
            <ta e="T508" id="Seg_8740" s="T507">1SG.[NOM]</ta>
            <ta e="T509" id="Seg_8741" s="T508">моча-1SG-ACC-кал-1SG-ACC</ta>
            <ta e="T510" id="Seg_8742" s="T509">очистить-CVB.SIM</ta>
            <ta e="T511" id="Seg_8743" s="T510">пропитываться-CVB.SEQ-уживаться-CVB.SEQ</ta>
            <ta e="T512" id="Seg_8744" s="T511">лежать-FUT.[3SG]</ta>
            <ta e="T513" id="Seg_8745" s="T512">NEG</ta>
            <ta e="T514" id="Seg_8746" s="T513">быть-PST1-2SG</ta>
            <ta e="T515" id="Seg_8747" s="T514">кто.[NOM]</ta>
            <ta e="T516" id="Seg_8748" s="T515">учить-PRS.[3SG]</ta>
            <ta e="T517" id="Seg_8749" s="T516">говорить-FUT-3SG</ta>
            <ta e="T518" id="Seg_8750" s="T517">там</ta>
            <ta e="T519" id="Seg_8751" s="T518">скрывать-APPR-2SG</ta>
            <ta e="T520" id="Seg_8752" s="T519">говорить-PRS.[3SG]</ta>
            <ta e="T521" id="Seg_8753" s="T520">дочь-3SG.[NOM]</ta>
            <ta e="T522" id="Seg_8754" s="T521">плохой</ta>
            <ta e="T523" id="Seg_8755" s="T522">девушка.[NOM]</ta>
            <ta e="T524" id="Seg_8756" s="T523">ребенок-PROPR-1SG</ta>
            <ta e="T525" id="Seg_8757" s="T524">тот.[NOM]</ta>
            <ta e="T526" id="Seg_8758" s="T525">учить-FREQ-PRS.[3SG]</ta>
            <ta e="T527" id="Seg_8759" s="T526">говорить-FUT.[IMP.2SG]</ta>
            <ta e="T528" id="Seg_8760" s="T527">старик.[NOM]</ta>
            <ta e="T529" id="Seg_8761" s="T528">утром</ta>
            <ta e="T530" id="Seg_8762" s="T529">идти-PRS.[3SG]</ta>
            <ta e="T531" id="Seg_8763" s="T530">мальчик.[NOM]</ta>
            <ta e="T532" id="Seg_8764" s="T531">царь.[NOM]</ta>
            <ta e="T533" id="Seg_8765" s="T532">спрашивать-PRS.[3SG]</ta>
            <ta e="T534" id="Seg_8766" s="T533">эй</ta>
            <ta e="T535" id="Seg_8767" s="T534">догадываться-PST1-2SG</ta>
            <ta e="T536" id="Seg_8768" s="T535">Q</ta>
            <ta e="T537" id="Seg_8769" s="T536">старик.[NOM]</ta>
            <ta e="T538" id="Seg_8770" s="T537">корова.[NOM]</ta>
            <ta e="T539" id="Seg_8771" s="T538">копыто-3SG-ACC</ta>
            <ta e="T540" id="Seg_8772" s="T539">бабка-3SG-ACC</ta>
            <ta e="T541" id="Seg_8773" s="T540">стол-DAT/LOC</ta>
            <ta e="T542" id="Seg_8774" s="T541">ребенок.[NOM]</ta>
            <ta e="T543" id="Seg_8775" s="T542">игрушки-3SG-ACC</ta>
            <ta e="T544" id="Seg_8776" s="T543">как</ta>
            <ta e="T545" id="Seg_8777" s="T544">размотать-FREQ-CVB.SEQ</ta>
            <ta e="T546" id="Seg_8778" s="T545">бросать-PRS.[3SG]</ta>
            <ta e="T547" id="Seg_8779" s="T546">самец.[NOM]</ta>
            <ta e="T548" id="Seg_8780" s="T547">корова-ABL</ta>
            <ta e="T549" id="Seg_8781" s="T548">родить-PTCP.PRS</ta>
            <ta e="T550" id="Seg_8782" s="T549">только</ta>
            <ta e="T551" id="Seg_8783" s="T550">дело-3SG-ACC</ta>
            <ta e="T552" id="Seg_8784" s="T551">этот</ta>
            <ta e="T553" id="Seg_8785" s="T552">найти-PST1-1SG</ta>
            <ta e="T554" id="Seg_8786" s="T553">говорить-PRS.[3SG]</ta>
            <ta e="T555" id="Seg_8787" s="T554">царь.[NOM]</ta>
            <ta e="T556" id="Seg_8788" s="T555">сам-2SG.[NOM]</ta>
            <ta e="T557" id="Seg_8789" s="T556">ум-2SG-INSTR</ta>
            <ta e="T558" id="Seg_8790" s="T557">делать-NEG-PST1-2SG</ta>
            <ta e="T559" id="Seg_8791" s="T558">наверное</ta>
            <ta e="T560" id="Seg_8792" s="T559">тот-APRX</ta>
            <ta e="T561" id="Seg_8793" s="T560">ум-PROPR-EP-2SG.[NOM]</ta>
            <ta e="T562" id="Seg_8794" s="T561">быть-COND.[3SG]</ta>
            <ta e="T563" id="Seg_8795" s="T562">1SG.[NOM]</ta>
            <ta e="T564" id="Seg_8796" s="T563">моча-1SG-ACC-кал-1SG-ACC</ta>
            <ta e="T565" id="Seg_8797" s="T564">очистить-CVB.SIM</ta>
            <ta e="T566" id="Seg_8798" s="T565">пропитываться-CVB.SEQ-уживаться-CVB.SEQ</ta>
            <ta e="T567" id="Seg_8799" s="T566">лежать-FUT.[3SG]</ta>
            <ta e="T568" id="Seg_8800" s="T567">NEG</ta>
            <ta e="T569" id="Seg_8801" s="T568">быть-PST1-2SG</ta>
            <ta e="T570" id="Seg_8802" s="T569">кто.[NOM]</ta>
            <ta e="T571" id="Seg_8803" s="T570">учить-PRS.[3SG]</ta>
            <ta e="T572" id="Seg_8804" s="T571">говорить-PRS.[3SG]</ta>
            <ta e="T573" id="Seg_8805" s="T572">плохой</ta>
            <ta e="T574" id="Seg_8806" s="T573">девушка.[NOM]</ta>
            <ta e="T575" id="Seg_8807" s="T574">ребенок-PROPR-1SG</ta>
            <ta e="T576" id="Seg_8808" s="T575">тот.[NOM]</ta>
            <ta e="T577" id="Seg_8809" s="T576">учить-FREQ-PRS.[3SG]</ta>
            <ta e="T578" id="Seg_8810" s="T577">говорить-PRS.[3SG]</ta>
            <ta e="T579" id="Seg_8811" s="T578">старик.[NOM]</ta>
            <ta e="T580" id="Seg_8812" s="T579">мальчик.[NOM]</ta>
            <ta e="T581" id="Seg_8813" s="T580">царь.[NOM]</ta>
            <ta e="T582" id="Seg_8814" s="T581">девушка-EP-2SG.[NOM]</ta>
            <ta e="T583" id="Seg_8815" s="T582">ум-PROPR</ta>
            <ta e="T584" id="Seg_8816" s="T583">девушка.[NOM]</ta>
            <ta e="T585" id="Seg_8817" s="T584">родить-PST2.[3SG]</ta>
            <ta e="T586" id="Seg_8818" s="T585">говорить-PRS.[3SG]</ta>
            <ta e="T587" id="Seg_8819" s="T586">железный</ta>
            <ta e="T588" id="Seg_8820" s="T587">ведро-ACC</ta>
            <ta e="T589" id="Seg_8821" s="T588">дно-3SG.[NOM]</ta>
            <ta e="T590" id="Seg_8822" s="T589">отверстие-PROPR-ACC</ta>
            <ta e="T591" id="Seg_8823" s="T590">давать-PRS.[3SG]</ta>
            <ta e="T592" id="Seg_8824" s="T591">этот-ACC</ta>
            <ta e="T593" id="Seg_8825" s="T592">залатать-CVB.SEQ</ta>
            <ta e="T594" id="Seg_8826" s="T593">послать-IMP.3SG</ta>
            <ta e="T595" id="Seg_8827" s="T594">говорить-PRS.[3SG]</ta>
            <ta e="T596" id="Seg_8828" s="T595">тот-ACC</ta>
            <ta e="T597" id="Seg_8829" s="T596">залатать-NEG-TEMP-3SG</ta>
            <ta e="T598" id="Seg_8830" s="T597">два-COLL-2PL-GEN</ta>
            <ta e="T599" id="Seg_8831" s="T598">голова-2PL-ACC</ta>
            <ta e="T600" id="Seg_8832" s="T599">резать-FUT-1SG</ta>
            <ta e="T601" id="Seg_8833" s="T600">говорить-PRS.[3SG]</ta>
            <ta e="T602" id="Seg_8834" s="T601">старик.[NOM]</ta>
            <ta e="T603" id="Seg_8835" s="T602">ведро-ACC</ta>
            <ta e="T604" id="Seg_8836" s="T603">носить-CVB.SIM</ta>
            <ta e="T605" id="Seg_8837" s="T604">идти-PST2-3SG</ta>
            <ta e="T606" id="Seg_8838" s="T605">плакать-CVB.SIM-плакать-CVB.SIM</ta>
            <ta e="T607" id="Seg_8839" s="T606">девушка.[NOM]</ta>
            <ta e="T608" id="Seg_8840" s="T607">эй</ta>
            <ta e="T609" id="Seg_8841" s="T608">отец</ta>
            <ta e="T610" id="Seg_8842" s="T609">что-EP-2SG.[NOM]</ta>
            <ta e="T611" id="Seg_8843" s="T610">ведро-3SG.[NOM]=Q</ta>
            <ta e="T612" id="Seg_8844" s="T611">говорить-PRS.[3SG]</ta>
            <ta e="T613" id="Seg_8845" s="T612">эй-EMPH</ta>
            <ta e="T614" id="Seg_8846" s="T613">вот</ta>
            <ta e="T615" id="Seg_8847" s="T614">убить-PTCP.PRS</ta>
            <ta e="T616" id="Seg_8848" s="T615">день-3SG.[NOM]</ta>
            <ta e="T617" id="Seg_8849" s="T616">приходить-PST1-3SG</ta>
            <ta e="T618" id="Seg_8850" s="T617">этот</ta>
            <ta e="T619" id="Seg_8851" s="T618">ведро-ACC</ta>
            <ta e="T620" id="Seg_8852" s="T619">залатать-CVB.SEQ</ta>
            <ta e="T621" id="Seg_8853" s="T620">послать-IMP.3SG</ta>
            <ta e="T622" id="Seg_8854" s="T621">говорить-PRS.[3SG]</ta>
            <ta e="T623" id="Seg_8855" s="T622">этот-ACC</ta>
            <ta e="T624" id="Seg_8856" s="T623">залатать-NEG-TEMP-3SG</ta>
            <ta e="T625" id="Seg_8857" s="T624">два-COLL-2PL-GEN</ta>
            <ta e="T626" id="Seg_8858" s="T625">голова-2PL-ACC</ta>
            <ta e="T627" id="Seg_8859" s="T626">резать-FUT-1SG</ta>
            <ta e="T628" id="Seg_8860" s="T627">говорить-PRS.[3SG]</ta>
            <ta e="T629" id="Seg_8861" s="T628">дочь.[NOM]</ta>
            <ta e="T630" id="Seg_8862" s="T629">говорить-PRS.[3SG]</ta>
            <ta e="T631" id="Seg_8863" s="T630">носить.[IMP.2SG]</ta>
            <ta e="T632" id="Seg_8864" s="T631">назад</ta>
            <ta e="T633" id="Seg_8865" s="T632">жена.[NOM]</ta>
            <ta e="T634" id="Seg_8866" s="T633">человек.[NOM]</ta>
            <ta e="T635" id="Seg_8867" s="T634">залатать-FUT-1SG</ta>
            <ta e="T636" id="Seg_8868" s="T635">мужчина.[NOM]</ta>
            <ta e="T637" id="Seg_8869" s="T636">человек.[NOM]</ta>
            <ta e="T638" id="Seg_8870" s="T637">ведро-ACC</ta>
            <ta e="T639" id="Seg_8871" s="T638">торбас.[NOM]</ta>
            <ta e="T640" id="Seg_8872" s="T639">подобно</ta>
            <ta e="T641" id="Seg_8873" s="T640">вертеть.наизнанку-CVB.SEQ</ta>
            <ta e="T642" id="Seg_8874" s="T641">послать-IMP.3SG</ta>
            <ta e="T643" id="Seg_8875" s="T642">старик.[NOM]</ta>
            <ta e="T644" id="Seg_8876" s="T643">носить-CVB.SEQ</ta>
            <ta e="T645" id="Seg_8877" s="T644">давать-PRS.[3SG]</ta>
            <ta e="T646" id="Seg_8878" s="T645">мальчик.[NOM]</ta>
            <ta e="T647" id="Seg_8879" s="T646">царь.[NOM]</ta>
            <ta e="T648" id="Seg_8880" s="T647">удивляться-PRS.[3SG]</ta>
            <ta e="T649" id="Seg_8881" s="T648">старик-ACC</ta>
            <ta e="T650" id="Seg_8882" s="T649">послать-PRS.[3SG]</ta>
            <ta e="T651" id="Seg_8883" s="T650">сам-1SG.[NOM]</ta>
            <ta e="T652" id="Seg_8884" s="T651">дочь-2SG-ACC</ta>
            <ta e="T653" id="Seg_8885" s="T652">видеть-CVB.SIM</ta>
            <ta e="T654" id="Seg_8886" s="T653">идти-FUT-1SG</ta>
            <ta e="T655" id="Seg_8887" s="T654">говорить-PRS.[3SG]</ta>
            <ta e="T656" id="Seg_8888" s="T655">мальчик.[NOM]</ta>
            <ta e="T657" id="Seg_8889" s="T656">царь.[NOM]</ta>
            <ta e="T658" id="Seg_8890" s="T657">дочь-ACC</ta>
            <ta e="T659" id="Seg_8891" s="T658">видеть-CVB.ANT</ta>
            <ta e="T660" id="Seg_8892" s="T659">любить-PRS.[3SG]</ta>
            <ta e="T661" id="Seg_8893" s="T660">жена.[NOM]</ta>
            <ta e="T662" id="Seg_8894" s="T661">взять-PRS.[3SG]</ta>
            <ta e="T663" id="Seg_8895" s="T662">сколько-ACC-сколько-ACC</ta>
            <ta e="T664" id="Seg_8896" s="T663">жить-PST2-3PL</ta>
            <ta e="T665" id="Seg_8897" s="T664">быть-PST1-3SG</ta>
            <ta e="T666" id="Seg_8898" s="T665">жена-VBZ-PTCP.PST-3SG-ABL</ta>
            <ta e="T667" id="Seg_8899" s="T666">мальчик.[NOM]</ta>
            <ta e="T668" id="Seg_8900" s="T667">царь.[NOM]</ta>
            <ta e="T669" id="Seg_8901" s="T668">кукла.[NOM]</ta>
            <ta e="T670" id="Seg_8902" s="T669">как</ta>
            <ta e="T671" id="Seg_8903" s="T670">сесть-ADVZ</ta>
            <ta e="T672" id="Seg_8904" s="T671">быть-PST1-3SG</ta>
            <ta e="T673" id="Seg_8905" s="T672">каждый-3SG-ACC</ta>
            <ta e="T674" id="Seg_8906" s="T673">жена-3SG.[NOM]</ta>
            <ta e="T675" id="Seg_8907" s="T674">советовать-PRS.[3SG]</ta>
            <ta e="T676" id="Seg_8908" s="T675">решать-PRS.[3SG]</ta>
            <ta e="T677" id="Seg_8909" s="T676">мальчик.[NOM]</ta>
            <ta e="T678" id="Seg_8910" s="T677">царь.[NOM]</ta>
            <ta e="T679" id="Seg_8911" s="T678">язык-VBZ-PRS.[3SG]</ta>
            <ta e="T680" id="Seg_8912" s="T679">друг</ta>
            <ta e="T681" id="Seg_8913" s="T680">этот</ta>
            <ta e="T682" id="Seg_8914" s="T681">один</ta>
            <ta e="T683" id="Seg_8915" s="T682">страна-DAT/LOC</ta>
            <ta e="T684" id="Seg_8916" s="T683">два</ta>
            <ta e="T685" id="Seg_8917" s="T684">царь.[NOM]</ta>
            <ta e="T686" id="Seg_8918" s="T685">быть-PTCP.PRS-1PL.[NOM]</ta>
            <ta e="T687" id="Seg_8919" s="T686">бывать-EP-NEG.[3SG]</ta>
            <ta e="T688" id="Seg_8920" s="T687">1PL.[NOM]</ta>
            <ta e="T689" id="Seg_8921" s="T688">расходиться-IMP.1DU</ta>
            <ta e="T690" id="Seg_8922" s="T689">жена-3SG.[NOM]</ta>
            <ta e="T691" id="Seg_8923" s="T690">вот</ta>
            <ta e="T692" id="Seg_8924" s="T691">расходиться-IMP.1DU</ta>
            <ta e="T693" id="Seg_8925" s="T692">Q</ta>
            <ta e="T694" id="Seg_8926" s="T693">говорить-PRS.[3SG]</ta>
            <ta e="T695" id="Seg_8927" s="T694">мальчик.[NOM]</ta>
            <ta e="T696" id="Seg_8928" s="T695">царь.[NOM]</ta>
            <ta e="T697" id="Seg_8929" s="T696">что</ta>
            <ta e="T698" id="Seg_8930" s="T697">желание-PROPR</ta>
            <ta e="T699" id="Seg_8931" s="T698">дело-2SG-ACC</ta>
            <ta e="T700" id="Seg_8932" s="T699">каждый-3SG-ACC</ta>
            <ta e="T701" id="Seg_8933" s="T700">взять.[IMP.2SG]</ta>
            <ta e="T702" id="Seg_8934" s="T701">говорить-PRS.[3SG]</ta>
            <ta e="T703" id="Seg_8935" s="T702">жена.[NOM]</ta>
            <ta e="T704" id="Seg_8936" s="T703">согласить-EP-RECP/COLL-PRS.[3SG]</ta>
            <ta e="T705" id="Seg_8937" s="T704">расходиться-PTCP.PRS</ta>
            <ta e="T706" id="Seg_8938" s="T705">ночь-3PL.[NOM]=EMPH</ta>
            <ta e="T707" id="Seg_8939" s="T706">становиться-PRS.[3SG]</ta>
            <ta e="T708" id="Seg_8940" s="T707">мужчина-3SG.[NOM]</ta>
            <ta e="T709" id="Seg_8941" s="T708">уснуть-CVB.SEQ</ta>
            <ta e="T710" id="Seg_8942" s="T709">оставаться-PRS.[3SG]</ta>
            <ta e="T711" id="Seg_8943" s="T710">жена.[NOM]</ta>
            <ta e="T712" id="Seg_8944" s="T711">муж-3SG-ACC</ta>
            <ta e="T713" id="Seg_8945" s="T712">одеяло-PL-ACC</ta>
            <ta e="T714" id="Seg_8946" s="T713">постельное.белье-PL-ACC</ta>
            <ta e="T715" id="Seg_8947" s="T714">отец-3SG-GEN</ta>
            <ta e="T716" id="Seg_8948" s="T715">земля.[NOM]</ta>
            <ta e="T717" id="Seg_8949" s="T716">землянка.[NOM]</ta>
            <ta e="T718" id="Seg_8950" s="T717">дом-3SG-DAT/LOC</ta>
            <ta e="T719" id="Seg_8951" s="T718">поднимать-CVB.SEQ</ta>
            <ta e="T720" id="Seg_8952" s="T719">носить-PRS.[3SG]</ta>
            <ta e="T721" id="Seg_8953" s="T720">мальчик.[NOM]</ta>
            <ta e="T722" id="Seg_8954" s="T721">царь.[NOM]</ta>
            <ta e="T723" id="Seg_8955" s="T722">утром</ta>
            <ta e="T724" id="Seg_8956" s="T723">просыпаться-CVB.SEQ</ta>
            <ta e="T725" id="Seg_8957" s="T724">испугаться-PRS.[3SG]</ta>
            <ta e="T726" id="Seg_8958" s="T725">потягиваться-CVB.SEQ</ta>
            <ta e="T727" id="Seg_8959" s="T726">видеть-PST2-3SG</ta>
            <ta e="T728" id="Seg_8960" s="T727">голова-3SG.[NOM]-нога-3SG.[NOM]</ta>
            <ta e="T729" id="Seg_8961" s="T728">землянка.[NOM]</ta>
            <ta e="T730" id="Seg_8962" s="T729">дерево-3SG-DAT/LOC</ta>
            <ta e="T731" id="Seg_8963" s="T730">толкать-PRS-3PL</ta>
            <ta e="T732" id="Seg_8964" s="T731">эй-EMPH</ta>
            <ta e="T733" id="Seg_8965" s="T732">этот</ta>
            <ta e="T734" id="Seg_8966" s="T733">где</ta>
            <ta e="T735" id="Seg_8967" s="T734">есть-1PL=Q</ta>
            <ta e="T736" id="Seg_8968" s="T735">почему</ta>
            <ta e="T737" id="Seg_8969" s="T736">приходить-PST2-EP-1SG=Q</ta>
            <ta e="T738" id="Seg_8970" s="T737">жена-3SG-ACC</ta>
            <ta e="T739" id="Seg_8971" s="T738">толкать-FREQ-CVB.SEQ</ta>
            <ta e="T740" id="Seg_8972" s="T739">просыпаться-CAUS-PRS.[3SG]</ta>
            <ta e="T741" id="Seg_8973" s="T740">жена-3SG.[NOM]</ta>
            <ta e="T742" id="Seg_8974" s="T741">эй</ta>
            <ta e="T743" id="Seg_8975" s="T742">друг</ta>
            <ta e="T744" id="Seg_8976" s="T743">1SG.[NOM]</ta>
            <ta e="T745" id="Seg_8977" s="T744">принести-PST2-EP-1SG</ta>
            <ta e="T746" id="Seg_8978" s="T745">говорить-PRS.[3SG]</ta>
            <ta e="T747" id="Seg_8979" s="T746">почему</ta>
            <ta e="T748" id="Seg_8980" s="T747">принести-PST2-2SG=Q</ta>
            <ta e="T749" id="Seg_8981" s="T748">ах</ta>
            <ta e="T750" id="Seg_8982" s="T749">сам-2SG-ACC</ta>
            <ta e="T751" id="Seg_8983" s="T750">хотеть-PST1-1SG</ta>
            <ta e="T752" id="Seg_8984" s="T751">да</ta>
            <ta e="T753" id="Seg_8985" s="T752">сам-2SG-ACC</ta>
            <ta e="T754" id="Seg_8986" s="T753">принести-PST1-1SG</ta>
            <ta e="T755" id="Seg_8987" s="T754">что-ACC</ta>
            <ta e="T756" id="Seg_8988" s="T755">хотеть-PTCP.PRS-2SG-ACC</ta>
            <ta e="T757" id="Seg_8989" s="T756">взять-FUT.[IMP.2SG]</ta>
            <ta e="T758" id="Seg_8990" s="T757">говорить-PST2-EP-2SG</ta>
            <ta e="T759" id="Seg_8991" s="T758">EMPH</ta>
            <ta e="T760" id="Seg_8992" s="T759">говорить-PRS.[3SG]</ta>
            <ta e="T761" id="Seg_8993" s="T760">жена.[NOM]</ta>
            <ta e="T762" id="Seg_8994" s="T761">мальчик.[NOM]</ta>
            <ta e="T763" id="Seg_8995" s="T762">царь.[NOM]</ta>
            <ta e="T764" id="Seg_8996" s="T763">мочь-CVB.SEQ</ta>
            <ta e="T765" id="Seg_8997" s="T764">уйти-NEG-PST1-3SG</ta>
            <ta e="T766" id="Seg_8998" s="T765">вот</ta>
            <ta e="T767" id="Seg_8999" s="T766">жена-3SG.[NOM]</ta>
            <ta e="T768" id="Seg_9000" s="T767">говорить-PRS.[3SG]</ta>
            <ta e="T769" id="Seg_9001" s="T768">столько</ta>
            <ta e="T770" id="Seg_9002" s="T769">ум-PROPR</ta>
            <ta e="T771" id="Seg_9003" s="T770">человек.[NOM]</ta>
            <ta e="T772" id="Seg_9004" s="T771">быть-NEG.PTCP.PST</ta>
            <ta e="T773" id="Seg_9005" s="T772">быть-PST2-2SG</ta>
            <ta e="T774" id="Seg_9006" s="T773">народ-2SG-ACC</ta>
            <ta e="T775" id="Seg_9007" s="T774">советовать-CVB.SEQ</ta>
            <ta e="T776" id="Seg_9008" s="T775">сам-2SG.[NOM]</ta>
            <ta e="T777" id="Seg_9009" s="T776">жить.[IMP.2SG]</ta>
            <ta e="T778" id="Seg_9010" s="T777">1SG.[NOM]</ta>
            <ta e="T779" id="Seg_9011" s="T778">простой</ta>
            <ta e="T780" id="Seg_9012" s="T779">хозяйка.[NOM]</ta>
            <ta e="T781" id="Seg_9013" s="T780">быть-CVB.SEQ</ta>
            <ta e="T782" id="Seg_9014" s="T781">сидеть-FUT-1SG</ta>
            <ta e="T783" id="Seg_9015" s="T782">нищий</ta>
            <ta e="T784" id="Seg_9016" s="T783">крестьянин.[NOM]</ta>
            <ta e="T785" id="Seg_9017" s="T784">девушка-3SG-ACC</ta>
            <ta e="T786" id="Seg_9018" s="T785">добрый-3SG-INSTR</ta>
            <ta e="T787" id="Seg_9019" s="T786">зеркало.[NOM]</ta>
            <ta e="T788" id="Seg_9020" s="T787">камень.[NOM]</ta>
            <ta e="T789" id="Seg_9021" s="T788">дом-DAT/LOC</ta>
            <ta e="T790" id="Seg_9022" s="T789">жить-INFER-3SG</ta>
            <ta e="T791" id="Seg_9023" s="T790">земля.[NOM]</ta>
            <ta e="T792" id="Seg_9024" s="T791">землянка-ABL</ta>
            <ta e="T793" id="Seg_9025" s="T792">расходиться-CVB.SEQ</ta>
            <ta e="T794" id="Seg_9026" s="T793">конец-EP-ABL</ta>
            <ta e="T795" id="Seg_9027" s="T794">быть.богатым-CVB.SEQ-быть.богатым-CVB.SEQ</ta>
            <ta e="T796" id="Seg_9028" s="T795">жить-INFER-3PL</ta>
            <ta e="T797" id="Seg_9029" s="T796">вот</ta>
            <ta e="T798" id="Seg_9030" s="T797">последний-3SG.[NOM]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_9031" s="T0">ptcl</ta>
            <ta e="T2" id="Seg_9032" s="T1">n-n:case</ta>
            <ta e="T3" id="Seg_9033" s="T2">n-n:case</ta>
            <ta e="T4" id="Seg_9034" s="T3">v-v:tense-v:poss.pn</ta>
            <ta e="T5" id="Seg_9035" s="T4">n-n:poss-n:case</ta>
            <ta e="T6" id="Seg_9036" s="T5">adj</ta>
            <ta e="T7" id="Seg_9037" s="T6">n-n&gt;adj-n:case</ta>
            <ta e="T8" id="Seg_9038" s="T7">adj</ta>
            <ta e="T9" id="Seg_9039" s="T8">n-n:case</ta>
            <ta e="T10" id="Seg_9040" s="T9">dempro</ta>
            <ta e="T11" id="Seg_9041" s="T10">n-n:case</ta>
            <ta e="T12" id="Seg_9042" s="T11">v-v:cvb</ta>
            <ta e="T13" id="Seg_9043" s="T12">adj</ta>
            <ta e="T14" id="Seg_9044" s="T13">n-n:case</ta>
            <ta e="T15" id="Seg_9045" s="T14">n-n:case</ta>
            <ta e="T16" id="Seg_9046" s="T15">n-n:case</ta>
            <ta e="T17" id="Seg_9047" s="T16">post</ta>
            <ta e="T18" id="Seg_9048" s="T17">cardnum</ta>
            <ta e="T19" id="Seg_9049" s="T18">n-n:case</ta>
            <ta e="T20" id="Seg_9050" s="T19">n-n:case</ta>
            <ta e="T21" id="Seg_9051" s="T20">post</ta>
            <ta e="T22" id="Seg_9052" s="T21">v-v:ptcp</ta>
            <ta e="T23" id="Seg_9053" s="T22">v-v:tense-v:pred.pn</ta>
            <ta e="T24" id="Seg_9054" s="T23">dempro-pro:case</ta>
            <ta e="T25" id="Seg_9055" s="T24">v-v&gt;v-v:cvb</ta>
            <ta e="T26" id="Seg_9056" s="T25">v-v:tense-v:pred.pn</ta>
            <ta e="T27" id="Seg_9057" s="T26">adj</ta>
            <ta e="T28" id="Seg_9058" s="T27">n-n:case</ta>
            <ta e="T29" id="Seg_9059" s="T28">cardnum</ta>
            <ta e="T30" id="Seg_9060" s="T29">n-n:case</ta>
            <ta e="T31" id="Seg_9061" s="T30">n-n&gt;adj-n:case</ta>
            <ta e="T32" id="Seg_9062" s="T31">ptcl</ta>
            <ta e="T33" id="Seg_9063" s="T32">adv</ta>
            <ta e="T34" id="Seg_9064" s="T33">n-n:case</ta>
            <ta e="T35" id="Seg_9065" s="T34">n-n:case</ta>
            <ta e="T36" id="Seg_9066" s="T35">v-v:tense-v:pred.pn</ta>
            <ta e="T37" id="Seg_9067" s="T36">interj</ta>
            <ta e="T38" id="Seg_9068" s="T37">ptcl</ta>
            <ta e="T39" id="Seg_9069" s="T38">adj</ta>
            <ta e="T40" id="Seg_9070" s="T39">n-n:case</ta>
            <ta e="T41" id="Seg_9071" s="T40">que</ta>
            <ta e="T42" id="Seg_9072" s="T41">n-n&gt;adj</ta>
            <ta e="T43" id="Seg_9073" s="T42">n-n:case</ta>
            <ta e="T44" id="Seg_9074" s="T43">v-v:cvb</ta>
            <ta e="T45" id="Seg_9075" s="T44">v-v:tense-v:poss.pn</ta>
            <ta e="T46" id="Seg_9076" s="T45">n-n:case</ta>
            <ta e="T47" id="Seg_9077" s="T46">v-v:mood.pn</ta>
            <ta e="T48" id="Seg_9078" s="T47">adj-n:case</ta>
            <ta e="T49" id="Seg_9079" s="T48">adj-n:case</ta>
            <ta e="T50" id="Seg_9080" s="T49">que-pro:case-ptcl</ta>
            <ta e="T51" id="Seg_9081" s="T50">adv</ta>
            <ta e="T52" id="Seg_9082" s="T51">v-v:cvb</ta>
            <ta e="T53" id="Seg_9083" s="T52">cardnum</ta>
            <ta e="T54" id="Seg_9084" s="T53">n-n:case</ta>
            <ta e="T55" id="Seg_9085" s="T54">v-v:(tense)-v:mood.pn</ta>
            <ta e="T56" id="Seg_9086" s="T55">dempro-pro:case</ta>
            <ta e="T57" id="Seg_9087" s="T56">v-v:(neg)-v:mood-v:temp.pn</ta>
            <ta e="T58" id="Seg_9088" s="T57">v-v:ptcp</ta>
            <ta e="T59" id="Seg_9089" s="T58">n-n:poss-n:case</ta>
            <ta e="T60" id="Seg_9090" s="T59">n-n&gt;adj</ta>
            <ta e="T61" id="Seg_9091" s="T60">n-n:poss-n:case</ta>
            <ta e="T62" id="Seg_9092" s="T61">v-v:tense-v:poss.pn</ta>
            <ta e="T63" id="Seg_9093" s="T62">adj</ta>
            <ta e="T64" id="Seg_9094" s="T63">n-n:case</ta>
            <ta e="T65" id="Seg_9095" s="T64">que</ta>
            <ta e="T66" id="Seg_9096" s="T65">ptcl</ta>
            <ta e="T67" id="Seg_9097" s="T66">v-v:cvb</ta>
            <ta e="T68" id="Seg_9098" s="T67">v-v:(neg)-v:pred.pn</ta>
            <ta e="T69" id="Seg_9099" s="T68">adj</ta>
            <ta e="T70" id="Seg_9100" s="T69">n-n:case</ta>
            <ta e="T71" id="Seg_9101" s="T70">v-v&gt;v-v:(neg)-v:pred.pn</ta>
            <ta e="T72" id="Seg_9102" s="T71">n-n:case</ta>
            <ta e="T73" id="Seg_9103" s="T72">n-n:(poss)-n:case</ta>
            <ta e="T74" id="Seg_9104" s="T73">adj-n:(poss)-n:case</ta>
            <ta e="T75" id="Seg_9105" s="T74">adj-n:case</ta>
            <ta e="T76" id="Seg_9106" s="T75">post</ta>
            <ta e="T77" id="Seg_9107" s="T76">pers-pro:case</ta>
            <ta e="T78" id="Seg_9108" s="T77">n-n:poss-n:case</ta>
            <ta e="T79" id="Seg_9109" s="T78">v-v:cvb</ta>
            <ta e="T80" id="Seg_9110" s="T79">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T81" id="Seg_9111" s="T80">v-v:tense-v:poss.pn</ta>
            <ta e="T82" id="Seg_9112" s="T81">n-n:(poss)-n:case</ta>
            <ta e="T83" id="Seg_9113" s="T82">v-v:tense-v:pred.pn</ta>
            <ta e="T84" id="Seg_9114" s="T83">interj</ta>
            <ta e="T85" id="Seg_9115" s="T84">n</ta>
            <ta e="T86" id="Seg_9116" s="T85">que</ta>
            <ta e="T87" id="Seg_9117" s="T86">v-v:tense-v:poss.pn</ta>
            <ta e="T88" id="Seg_9118" s="T87">ptcl</ta>
            <ta e="T89" id="Seg_9119" s="T88">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T90" id="Seg_9120" s="T89">v-v:(ins)-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T91" id="Seg_9121" s="T90">adv</ta>
            <ta e="T92" id="Seg_9122" s="T91">v-v:ptcp-v:(ins)-v:(poss)-v:(case)</ta>
            <ta e="T93" id="Seg_9123" s="T92">n-n:(poss)-n:case</ta>
            <ta e="T94" id="Seg_9124" s="T93">n-n:case</ta>
            <ta e="T95" id="Seg_9125" s="T94">n-n:case</ta>
            <ta e="T96" id="Seg_9126" s="T95">n-n:case</ta>
            <ta e="T97" id="Seg_9127" s="T96">v-v:tense-v:poss.pn</ta>
            <ta e="T98" id="Seg_9128" s="T97">v-v:(neg)-v:mood-v:temp.pn</ta>
            <ta e="T99" id="Seg_9129" s="T98">n-n&gt;adj</ta>
            <ta e="T100" id="Seg_9130" s="T99">n-n:poss-n:case</ta>
            <ta e="T101" id="Seg_9131" s="T100">v-v:ptcp</ta>
            <ta e="T102" id="Seg_9132" s="T101">v-v:tense-v:poss.pn</ta>
            <ta e="T103" id="Seg_9133" s="T102">que-pro:case</ta>
            <ta e="T104" id="Seg_9134" s="T103">v-v&gt;v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T105" id="Seg_9135" s="T104">dempro-pro:case</ta>
            <ta e="T106" id="Seg_9136" s="T105">adj-n:case</ta>
            <ta e="T107" id="Seg_9137" s="T106">adj-n:case</ta>
            <ta e="T108" id="Seg_9138" s="T107">que-pro:case-ptcl</ta>
            <ta e="T109" id="Seg_9139" s="T108">v-v:tense-v:pred.pn</ta>
            <ta e="T110" id="Seg_9140" s="T109">v-v:tense-v:pred.pn</ta>
            <ta e="T111" id="Seg_9141" s="T110">interj</ta>
            <ta e="T112" id="Seg_9142" s="T111">n</ta>
            <ta e="T113" id="Seg_9143" s="T112">n-n:(poss)</ta>
            <ta e="T114" id="Seg_9144" s="T113">ptcl-ptcl:case</ta>
            <ta e="T115" id="Seg_9145" s="T114">ptcl</ta>
            <ta e="T116" id="Seg_9146" s="T115">v-v:tense-v:pred.pn</ta>
            <ta e="T117" id="Seg_9147" s="T116">v-v&gt;v-v:mood.pn</ta>
            <ta e="T118" id="Seg_9148" s="T117">v-v:mood.pn</ta>
            <ta e="T119" id="Seg_9149" s="T118">adj-n:case</ta>
            <ta e="T120" id="Seg_9150" s="T119">adj-n:case</ta>
            <ta e="T121" id="Seg_9151" s="T120">v-v:ptcp</ta>
            <ta e="T122" id="Seg_9152" s="T121">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T123" id="Seg_9153" s="T122">cardnum</ta>
            <ta e="T124" id="Seg_9154" s="T123">v-v:(tense)-v:mood.pn</ta>
            <ta e="T125" id="Seg_9155" s="T124">adj</ta>
            <ta e="T126" id="Seg_9156" s="T125">n-n:case</ta>
            <ta e="T127" id="Seg_9157" s="T126">adj</ta>
            <ta e="T128" id="Seg_9158" s="T127">n-n:case</ta>
            <ta e="T129" id="Seg_9159" s="T128">post</ta>
            <ta e="T130" id="Seg_9160" s="T129">v-v:cvb</ta>
            <ta e="T131" id="Seg_9161" s="T130">pers-pro:case</ta>
            <ta e="T132" id="Seg_9162" s="T131">n-n:poss-n:case-n-n:poss-n:case</ta>
            <ta e="T133" id="Seg_9163" s="T132">v-v:cvb</ta>
            <ta e="T134" id="Seg_9164" s="T133">cardnum</ta>
            <ta e="T135" id="Seg_9165" s="T134">n-n:case</ta>
            <ta e="T136" id="Seg_9166" s="T135">n-n:case</ta>
            <ta e="T137" id="Seg_9167" s="T136">v-v:mood-v:temp.pn</ta>
            <ta e="T138" id="Seg_9168" s="T137">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T139" id="Seg_9169" s="T138">n-n:case</ta>
            <ta e="T140" id="Seg_9170" s="T139">n-n:case</ta>
            <ta e="T141" id="Seg_9171" s="T140">n-n:poss-n:case</ta>
            <ta e="T142" id="Seg_9172" s="T141">v-v:mood-v:temp.pn</ta>
            <ta e="T143" id="Seg_9173" s="T142">n-n:(poss)-n:case</ta>
            <ta e="T144" id="Seg_9174" s="T143">v-v:tense-v:pred.pn</ta>
            <ta e="T145" id="Seg_9175" s="T144">dempro</ta>
            <ta e="T146" id="Seg_9176" s="T145">n-n&gt;n-n:poss-n:case</ta>
            <ta e="T147" id="Seg_9177" s="T146">n-n:case</ta>
            <ta e="T148" id="Seg_9178" s="T147">v-v:mood-v:temp.pn</ta>
            <ta e="T149" id="Seg_9179" s="T148">v-v:cvb</ta>
            <ta e="T150" id="Seg_9180" s="T149">conj</ta>
            <ta e="T151" id="Seg_9181" s="T150">v-v:tense-v:pred.pn</ta>
            <ta e="T152" id="Seg_9182" s="T151">adv</ta>
            <ta e="T153" id="Seg_9183" s="T152">adj-n:case</ta>
            <ta e="T154" id="Seg_9184" s="T153">adj-n:case</ta>
            <ta e="T155" id="Seg_9185" s="T154">v-v:ptcp</ta>
            <ta e="T156" id="Seg_9186" s="T155">n-n:case</ta>
            <ta e="T157" id="Seg_9187" s="T156">v-v:tense-v:mood-v:pred.pn</ta>
            <ta e="T158" id="Seg_9188" s="T157">v-v:(tense)-v:mood.pn</ta>
            <ta e="T159" id="Seg_9189" s="T158">pers-pro:case</ta>
            <ta e="T160" id="Seg_9190" s="T159">dempro-pro:case</ta>
            <ta e="T161" id="Seg_9191" s="T160">conj</ta>
            <ta e="T162" id="Seg_9192" s="T161">n-n:case</ta>
            <ta e="T163" id="Seg_9193" s="T162">v-v:ptcp-v:(poss)</ta>
            <ta e="T164" id="Seg_9194" s="T163">v-v:tense-v:poss.pn</ta>
            <ta e="T165" id="Seg_9195" s="T164">ptcl</ta>
            <ta e="T166" id="Seg_9196" s="T165">v-v:tense-v:pred.pn</ta>
            <ta e="T167" id="Seg_9197" s="T166">n-n:(poss)-n:case</ta>
            <ta e="T168" id="Seg_9198" s="T167">n-n:case</ta>
            <ta e="T169" id="Seg_9199" s="T168">v-v:cvb</ta>
            <ta e="T170" id="Seg_9200" s="T169">v-v:tense-v:poss.pn</ta>
            <ta e="T171" id="Seg_9201" s="T170">adv</ta>
            <ta e="T172" id="Seg_9202" s="T171">adv</ta>
            <ta e="T173" id="Seg_9203" s="T172">v-v:cvb</ta>
            <ta e="T174" id="Seg_9204" s="T173">cardnum</ta>
            <ta e="T175" id="Seg_9205" s="T174">n-n:case</ta>
            <ta e="T176" id="Seg_9206" s="T175">n-n:case</ta>
            <ta e="T177" id="Seg_9207" s="T176">v-v:tense-v:pred.pn</ta>
            <ta e="T178" id="Seg_9208" s="T177">n-n:case</ta>
            <ta e="T179" id="Seg_9209" s="T178">n-n:case</ta>
            <ta e="T180" id="Seg_9210" s="T179">v-v:tense-v:pred.pn</ta>
            <ta e="T181" id="Seg_9211" s="T180">interj</ta>
            <ta e="T182" id="Seg_9212" s="T181">v-v:tense-v:poss.pn</ta>
            <ta e="T183" id="Seg_9213" s="T182">ptcl</ta>
            <ta e="T184" id="Seg_9214" s="T183">interj</ta>
            <ta e="T185" id="Seg_9215" s="T184">v-v:ptcp-v:(case)</ta>
            <ta e="T186" id="Seg_9216" s="T185">post</ta>
            <ta e="T187" id="Seg_9217" s="T186">v-v:tense-v:poss.pn</ta>
            <ta e="T188" id="Seg_9218" s="T187">adv</ta>
            <ta e="T189" id="Seg_9219" s="T188">ptcl</ta>
            <ta e="T190" id="Seg_9220" s="T189">n-n:(poss)-n:case</ta>
            <ta e="T191" id="Seg_9221" s="T190">ptcl</ta>
            <ta e="T192" id="Seg_9222" s="T191">v-v:tense-v:pred.pn</ta>
            <ta e="T193" id="Seg_9223" s="T192">ptcl</ta>
            <ta e="T194" id="Seg_9224" s="T193">que-pro:case-ptcl</ta>
            <ta e="T195" id="Seg_9225" s="T194">adj</ta>
            <ta e="T196" id="Seg_9226" s="T195">n-n:case</ta>
            <ta e="T197" id="Seg_9227" s="T196">adj</ta>
            <ta e="T198" id="Seg_9228" s="T197">n-n:case</ta>
            <ta e="T199" id="Seg_9229" s="T198">post</ta>
            <ta e="T200" id="Seg_9230" s="T199">v-v:cvb</ta>
            <ta e="T201" id="Seg_9231" s="T200">pers-pro:case</ta>
            <ta e="T202" id="Seg_9232" s="T201">n-n:poss-n:case-n-n:poss-n:case</ta>
            <ta e="T203" id="Seg_9233" s="T202">v-v:cvb</ta>
            <ta e="T204" id="Seg_9234" s="T203">cardnum</ta>
            <ta e="T205" id="Seg_9235" s="T204">n-n:case</ta>
            <ta e="T206" id="Seg_9236" s="T205">n-n:case</ta>
            <ta e="T207" id="Seg_9237" s="T206">v-v:mood-v:temp.pn</ta>
            <ta e="T208" id="Seg_9238" s="T207">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T209" id="Seg_9239" s="T208">n-n:case</ta>
            <ta e="T210" id="Seg_9240" s="T209">n-n:case</ta>
            <ta e="T211" id="Seg_9241" s="T210">n-n:poss-n:case</ta>
            <ta e="T212" id="Seg_9242" s="T211">v-v:mood-v:temp.pn</ta>
            <ta e="T213" id="Seg_9243" s="T212">n-n:(poss)-n:case</ta>
            <ta e="T214" id="Seg_9244" s="T213">v-v:tense-v:pred.pn</ta>
            <ta e="T215" id="Seg_9245" s="T214">dempro</ta>
            <ta e="T216" id="Seg_9246" s="T215">n-n&gt;n-n:poss-n:case</ta>
            <ta e="T217" id="Seg_9247" s="T216">n-n:case</ta>
            <ta e="T218" id="Seg_9248" s="T217">v-v:mood-v:temp.pn</ta>
            <ta e="T219" id="Seg_9249" s="T218">v-v:cvb</ta>
            <ta e="T220" id="Seg_9250" s="T219">conj</ta>
            <ta e="T221" id="Seg_9251" s="T220">v-v:tense-v:pred.pn</ta>
            <ta e="T222" id="Seg_9252" s="T221">adv</ta>
            <ta e="T223" id="Seg_9253" s="T222">adj-n:case</ta>
            <ta e="T224" id="Seg_9254" s="T223">adj-n:case</ta>
            <ta e="T225" id="Seg_9255" s="T224">v-v:ptcp</ta>
            <ta e="T226" id="Seg_9256" s="T225">n-n:case</ta>
            <ta e="T227" id="Seg_9257" s="T226">v-v:tense-v:mood-v:pred.pn</ta>
            <ta e="T228" id="Seg_9258" s="T227">n-n:case</ta>
            <ta e="T229" id="Seg_9259" s="T228">n-n:case</ta>
            <ta e="T230" id="Seg_9260" s="T229">v-v:tense-v:pred.pn</ta>
            <ta e="T231" id="Seg_9261" s="T230">interj</ta>
            <ta e="T232" id="Seg_9262" s="T231">ptcl</ta>
            <ta e="T233" id="Seg_9263" s="T232">n-n&gt;adj</ta>
            <ta e="T234" id="Seg_9264" s="T233">n-n:(pred.pn)</ta>
            <ta e="T235" id="Seg_9265" s="T234">v-v:tense-v:pred.pn</ta>
            <ta e="T236" id="Seg_9266" s="T235">que</ta>
            <ta e="T237" id="Seg_9267" s="T236">v-v:cvb-v-v:cvb</ta>
            <ta e="T238" id="Seg_9268" s="T237">v-v:tense-v:pred.pn</ta>
            <ta e="T239" id="Seg_9269" s="T238">adv</ta>
            <ta e="T240" id="Seg_9270" s="T239">v-v:mood.pn</ta>
            <ta e="T241" id="Seg_9271" s="T240">adj</ta>
            <ta e="T242" id="Seg_9272" s="T241">n-n:case</ta>
            <ta e="T243" id="Seg_9273" s="T242">adj-n:case</ta>
            <ta e="T244" id="Seg_9274" s="T243">adj-n:case</ta>
            <ta e="T245" id="Seg_9275" s="T244">que-pro:case</ta>
            <ta e="T246" id="Seg_9276" s="T245">ptcl-ptcl</ta>
            <ta e="T247" id="Seg_9277" s="T246">adv</ta>
            <ta e="T248" id="Seg_9278" s="T247">v-v:(neg)-v:mood-v:temp.pn</ta>
            <ta e="T249" id="Seg_9279" s="T248">n-n&gt;adj</ta>
            <ta e="T250" id="Seg_9280" s="T249">n-n:poss-n:case</ta>
            <ta e="T251" id="Seg_9281" s="T250">v-v:tense-v:pred.pn</ta>
            <ta e="T252" id="Seg_9282" s="T251">n-n:case</ta>
            <ta e="T253" id="Seg_9283" s="T252">que-pro:case</ta>
            <ta e="T254" id="Seg_9284" s="T253">ptcl</ta>
            <ta e="T255" id="Seg_9285" s="T254">v-v:(neg)-v:pred.pn</ta>
            <ta e="T256" id="Seg_9286" s="T255">v-v:cvb</ta>
            <ta e="T257" id="Seg_9287" s="T256">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T258" id="Seg_9288" s="T257">v-v:mood-v:temp.pn</ta>
            <ta e="T259" id="Seg_9289" s="T258">n-n:(poss)-n:case</ta>
            <ta e="T260" id="Seg_9290" s="T259">v-v:tense-v:pred.pn</ta>
            <ta e="T261" id="Seg_9291" s="T260">n-n:case</ta>
            <ta e="T262" id="Seg_9292" s="T261">adv-adv</ta>
            <ta e="T263" id="Seg_9293" s="T262">v-v:tense-v:pred.pn</ta>
            <ta e="T264" id="Seg_9294" s="T263">que</ta>
            <ta e="T265" id="Seg_9295" s="T264">n-n:(poss)</ta>
            <ta e="T266" id="Seg_9296" s="T265">ptcl-ptcl:case</ta>
            <ta e="T267" id="Seg_9297" s="T266">v-v:tense-v:pred.pn</ta>
            <ta e="T268" id="Seg_9298" s="T267">v-v:mood.pn</ta>
            <ta e="T269" id="Seg_9299" s="T268">adv</ta>
            <ta e="T270" id="Seg_9300" s="T269">v-v:(tense)-v:mood.pn</ta>
            <ta e="T271" id="Seg_9301" s="T270">adj-n:case</ta>
            <ta e="T272" id="Seg_9302" s="T271">adj-n:case</ta>
            <ta e="T273" id="Seg_9303" s="T272">v-v:(tense)-v:mood.pn</ta>
            <ta e="T274" id="Seg_9304" s="T273">adj</ta>
            <ta e="T275" id="Seg_9305" s="T274">n-n&gt;adj</ta>
            <ta e="T276" id="Seg_9306" s="T275">n-n:case</ta>
            <ta e="T277" id="Seg_9307" s="T276">n-n:poss-n:case</ta>
            <ta e="T278" id="Seg_9308" s="T277">v-v:cvb</ta>
            <ta e="T279" id="Seg_9309" s="T278">v-v:cvb</ta>
            <ta e="T280" id="Seg_9310" s="T279">n-n:poss-n:case</ta>
            <ta e="T281" id="Seg_9311" s="T280">v-v&gt;v-v:(ins)-v&gt;v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T282" id="Seg_9312" s="T281">v-v:cvb</ta>
            <ta e="T283" id="Seg_9313" s="T282">v-v:cvb</ta>
            <ta e="T284" id="Seg_9314" s="T283">v-v:mood-v:temp.pn</ta>
            <ta e="T285" id="Seg_9315" s="T284">n-n:case</ta>
            <ta e="T286" id="Seg_9316" s="T285">post</ta>
            <ta e="T287" id="Seg_9317" s="T286">v-v:mood-v:temp.pn</ta>
            <ta e="T288" id="Seg_9318" s="T287">n-n:case</ta>
            <ta e="T289" id="Seg_9319" s="T288">n-n:poss-n:case</ta>
            <ta e="T290" id="Seg_9320" s="T289">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T291" id="Seg_9321" s="T290">adv</ta>
            <ta e="T292" id="Seg_9322" s="T291">v-v:tense-v:pred.pn</ta>
            <ta e="T293" id="Seg_9323" s="T292">n-n:case</ta>
            <ta e="T294" id="Seg_9324" s="T293">post</ta>
            <ta e="T295" id="Seg_9325" s="T294">v-v:mood-v:temp.pn</ta>
            <ta e="T296" id="Seg_9326" s="T295">n-n:case</ta>
            <ta e="T297" id="Seg_9327" s="T296">n-n:poss-n:case-n-n:poss-n:case</ta>
            <ta e="T298" id="Seg_9328" s="T297">adv</ta>
            <ta e="T299" id="Seg_9329" s="T298">v-v:tense-v:pred.pn</ta>
            <ta e="T300" id="Seg_9330" s="T299">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T301" id="Seg_9331" s="T300">v-v:ptcp-v:(ins)-v:(case)</ta>
            <ta e="T302" id="Seg_9332" s="T301">que</ta>
            <ta e="T303" id="Seg_9333" s="T302">ptcl</ta>
            <ta e="T304" id="Seg_9334" s="T303">v-v:(neg)-v:pred.pn</ta>
            <ta e="T305" id="Seg_9335" s="T304">adv</ta>
            <ta e="T306" id="Seg_9336" s="T305">adj-n:case</ta>
            <ta e="T307" id="Seg_9337" s="T306">adj-n:case</ta>
            <ta e="T308" id="Seg_9338" s="T307">n-n:case</ta>
            <ta e="T309" id="Seg_9339" s="T308">v-v&gt;n-n:(poss)-n:case</ta>
            <ta e="T310" id="Seg_9340" s="T309">v-v:(tense)-v:mood.pn</ta>
            <ta e="T311" id="Seg_9341" s="T310">dempro-pro:case</ta>
            <ta e="T312" id="Seg_9342" s="T311">n-n:case</ta>
            <ta e="T313" id="Seg_9343" s="T312">pers-pro:case</ta>
            <ta e="T314" id="Seg_9344" s="T313">v-v:ptcp-v:(poss)</ta>
            <ta e="T315" id="Seg_9345" s="T314">v-v:tense-v:poss.pn</ta>
            <ta e="T316" id="Seg_9346" s="T315">ptcl</ta>
            <ta e="T317" id="Seg_9347" s="T316">v-v:tense-v:pred.pn</ta>
            <ta e="T318" id="Seg_9348" s="T317">n-n:case</ta>
            <ta e="T319" id="Seg_9349" s="T318">n-n:case</ta>
            <ta e="T320" id="Seg_9350" s="T319">v-v:cvb</ta>
            <ta e="T321" id="Seg_9351" s="T320">v-v:tense-v:poss.pn</ta>
            <ta e="T322" id="Seg_9352" s="T321">n</ta>
            <ta e="T323" id="Seg_9353" s="T322">v-v:cvb</ta>
            <ta e="T324" id="Seg_9354" s="T323">n-n:poss-n:case</ta>
            <ta e="T325" id="Seg_9355" s="T324">v-v:mood-v:poss.pn</ta>
            <ta e="T326" id="Seg_9356" s="T325">n-n:case</ta>
            <ta e="T327" id="Seg_9357" s="T326">n-n:case</ta>
            <ta e="T328" id="Seg_9358" s="T327">v-v:tense-v:pred.pn</ta>
            <ta e="T329" id="Seg_9359" s="T328">interj</ta>
            <ta e="T330" id="Seg_9360" s="T329">v-v:tense-v:poss.pn</ta>
            <ta e="T331" id="Seg_9361" s="T330">ptcl</ta>
            <ta e="T332" id="Seg_9362" s="T331">interj</ta>
            <ta e="T333" id="Seg_9363" s="T332">v-v:ptcp-v:(case)</ta>
            <ta e="T334" id="Seg_9364" s="T333">post</ta>
            <ta e="T335" id="Seg_9365" s="T334">v-v:tense-v:poss.pn</ta>
            <ta e="T336" id="Seg_9366" s="T335">adv</ta>
            <ta e="T337" id="Seg_9367" s="T336">ptcl</ta>
            <ta e="T338" id="Seg_9368" s="T337">n-n:(poss)-n:case</ta>
            <ta e="T339" id="Seg_9369" s="T338">ptcl</ta>
            <ta e="T340" id="Seg_9370" s="T339">v-v:tense-v:pred.pn</ta>
            <ta e="T341" id="Seg_9371" s="T340">adj</ta>
            <ta e="T342" id="Seg_9372" s="T341">n-n&gt;adj</ta>
            <ta e="T343" id="Seg_9373" s="T342">n-n:case</ta>
            <ta e="T344" id="Seg_9374" s="T343">n-n:poss-n:case</ta>
            <ta e="T345" id="Seg_9375" s="T344">v-v:cvb</ta>
            <ta e="T346" id="Seg_9376" s="T345">v-v:cvb</ta>
            <ta e="T347" id="Seg_9377" s="T346">n-n:poss-n:case</ta>
            <ta e="T348" id="Seg_9378" s="T347">v-v&gt;v-v:(ins)-v&gt;v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T349" id="Seg_9379" s="T348">v-v:cvb</ta>
            <ta e="T350" id="Seg_9380" s="T349">n-n&gt;v-v:cvb</ta>
            <ta e="T351" id="Seg_9381" s="T350">v-v:mood-v:temp.pn</ta>
            <ta e="T352" id="Seg_9382" s="T351">n-n:case</ta>
            <ta e="T353" id="Seg_9383" s="T352">post</ta>
            <ta e="T354" id="Seg_9384" s="T353">v-v:mood-v:temp.pn</ta>
            <ta e="T355" id="Seg_9385" s="T354">n-n:case</ta>
            <ta e="T356" id="Seg_9386" s="T355">n-n:poss-n:case</ta>
            <ta e="T357" id="Seg_9387" s="T356">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T358" id="Seg_9388" s="T357">adv</ta>
            <ta e="T359" id="Seg_9389" s="T358">v-v:tense-v:pred.pn</ta>
            <ta e="T360" id="Seg_9390" s="T359">n-n:case</ta>
            <ta e="T361" id="Seg_9391" s="T360">post</ta>
            <ta e="T362" id="Seg_9392" s="T361">v-v:mood-v:temp.pn</ta>
            <ta e="T363" id="Seg_9393" s="T362">n-n:case</ta>
            <ta e="T364" id="Seg_9394" s="T363">n-n:poss-n:case-n-n:poss-n:case</ta>
            <ta e="T365" id="Seg_9395" s="T364">adv</ta>
            <ta e="T366" id="Seg_9396" s="T365">v-v:tense-v:pred.pn</ta>
            <ta e="T367" id="Seg_9397" s="T366">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T368" id="Seg_9398" s="T367">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T369" id="Seg_9399" s="T368">que</ta>
            <ta e="T370" id="Seg_9400" s="T369">ptcl</ta>
            <ta e="T371" id="Seg_9401" s="T370">v-v:(neg)-v:pred.pn</ta>
            <ta e="T372" id="Seg_9402" s="T371">adv</ta>
            <ta e="T373" id="Seg_9403" s="T372">adj-n:case</ta>
            <ta e="T374" id="Seg_9404" s="T373">adj-n:case</ta>
            <ta e="T375" id="Seg_9405" s="T374">n-n:case</ta>
            <ta e="T376" id="Seg_9406" s="T375">v-v&gt;n-n:(poss)-n:case</ta>
            <ta e="T377" id="Seg_9407" s="T376">v-v:tense-v:pred.pn</ta>
            <ta e="T378" id="Seg_9408" s="T377">v-v:tense-v:pred.pn</ta>
            <ta e="T379" id="Seg_9409" s="T378">n-n:case</ta>
            <ta e="T380" id="Seg_9410" s="T379">n-n:case</ta>
            <ta e="T381" id="Seg_9411" s="T380">v-v:tense-v:pred.pn</ta>
            <ta e="T382" id="Seg_9412" s="T381">interj</ta>
            <ta e="T383" id="Seg_9413" s="T382">n-n:case</ta>
            <ta e="T384" id="Seg_9414" s="T383">ptcl</ta>
            <ta e="T385" id="Seg_9415" s="T384">n-n&gt;adj</ta>
            <ta e="T386" id="Seg_9416" s="T385">n-n:(pred.pn)</ta>
            <ta e="T387" id="Seg_9417" s="T386">v-v:tense-v:pred.pn</ta>
            <ta e="T388" id="Seg_9418" s="T387">ptcl</ta>
            <ta e="T389" id="Seg_9419" s="T388">que-pro:case</ta>
            <ta e="T390" id="Seg_9420" s="T389">conj</ta>
            <ta e="T391" id="Seg_9421" s="T390">cardnum</ta>
            <ta e="T392" id="Seg_9422" s="T391">n-n&gt;adj-n:case</ta>
            <ta e="T393" id="Seg_9423" s="T392">v-v:cvb</ta>
            <ta e="T394" id="Seg_9424" s="T393">n-n:case</ta>
            <ta e="T395" id="Seg_9425" s="T394">n-n:case</ta>
            <ta e="T396" id="Seg_9426" s="T395">v-v:cvb</ta>
            <ta e="T397" id="Seg_9427" s="T396">v-v:tense-v:pred.pn</ta>
            <ta e="T398" id="Seg_9428" s="T397">dempro-pro:case</ta>
            <ta e="T399" id="Seg_9429" s="T398">adv</ta>
            <ta e="T400" id="Seg_9430" s="T399">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T401" id="Seg_9431" s="T400">n-n:poss-n:case</ta>
            <ta e="T402" id="Seg_9432" s="T401">v-v:(ins)-v&gt;v-v&gt;v-v:cvb</ta>
            <ta e="T403" id="Seg_9433" s="T402">v-v:(tense)-v:mood.pn</ta>
            <ta e="T404" id="Seg_9434" s="T403">que</ta>
            <ta e="T405" id="Seg_9435" s="T404">n-n:poss-n:case</ta>
            <ta e="T406" id="Seg_9436" s="T405">v-v:cvb</ta>
            <ta e="T407" id="Seg_9437" s="T406">v-v:mood-v:pred.pn</ta>
            <ta e="T408" id="Seg_9438" s="T407">v-v:tense-v:pred.pn</ta>
            <ta e="T409" id="Seg_9439" s="T408">n-n:case</ta>
            <ta e="T410" id="Seg_9440" s="T409">n-n:case</ta>
            <ta e="T411" id="Seg_9441" s="T410">n-n:case</ta>
            <ta e="T412" id="Seg_9442" s="T411">n-n&gt;adj</ta>
            <ta e="T413" id="Seg_9443" s="T412">n-n:case</ta>
            <ta e="T414" id="Seg_9444" s="T413">n-n:poss-n:case</ta>
            <ta e="T415" id="Seg_9445" s="T414">v-v:cvb</ta>
            <ta e="T416" id="Seg_9446" s="T415">v-v:cvb</ta>
            <ta e="T417" id="Seg_9447" s="T416">v-v:cvb</ta>
            <ta e="T418" id="Seg_9448" s="T417">v-v:tense-v:poss.pn</ta>
            <ta e="T419" id="Seg_9449" s="T418">n-n:(poss)-n:case</ta>
            <ta e="T420" id="Seg_9450" s="T419">v-v:tense-v:poss.pn</ta>
            <ta e="T421" id="Seg_9451" s="T420">interj</ta>
            <ta e="T422" id="Seg_9452" s="T421">n</ta>
            <ta e="T423" id="Seg_9453" s="T422">que</ta>
            <ta e="T424" id="Seg_9454" s="T423">v-v:tense-v:poss.pn</ta>
            <ta e="T425" id="Seg_9455" s="T424">interj</ta>
            <ta e="T426" id="Seg_9456" s="T425">n-n:case</ta>
            <ta e="T427" id="Seg_9457" s="T426">n-n:case</ta>
            <ta e="T428" id="Seg_9458" s="T427">v-v:tense-v:poss.pn</ta>
            <ta e="T429" id="Seg_9459" s="T428">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T430" id="Seg_9460" s="T429">v-v:mood.pn</ta>
            <ta e="T431" id="Seg_9461" s="T430">v-v:tense-v:pred.pn</ta>
            <ta e="T432" id="Seg_9462" s="T431">n-n:case</ta>
            <ta e="T433" id="Seg_9463" s="T432">n-n:case</ta>
            <ta e="T434" id="Seg_9464" s="T433">que</ta>
            <ta e="T435" id="Seg_9465" s="T434">v-v:ptcp-v:(case)-ptcl</ta>
            <ta e="T436" id="Seg_9466" s="T435">n-n:case</ta>
            <ta e="T437" id="Seg_9467" s="T436">ptcl</ta>
            <ta e="T438" id="Seg_9468" s="T437">n-n:case</ta>
            <ta e="T439" id="Seg_9469" s="T438">n-n:case</ta>
            <ta e="T440" id="Seg_9470" s="T439">que</ta>
            <ta e="T441" id="Seg_9471" s="T440">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T442" id="Seg_9472" s="T441">adv</ta>
            <ta e="T443" id="Seg_9473" s="T442">n-n&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T444" id="Seg_9474" s="T443">v-v:cvb-v:pred.pn</ta>
            <ta e="T445" id="Seg_9475" s="T444">v-v&gt;v-v:mood.pn</ta>
            <ta e="T446" id="Seg_9476" s="T445">n-n:poss-n:case</ta>
            <ta e="T447" id="Seg_9477" s="T446">v-v:tense-v:poss.pn</ta>
            <ta e="T448" id="Seg_9478" s="T447">v-v:tense-v:pred.pn</ta>
            <ta e="T449" id="Seg_9479" s="T448">n-n:case</ta>
            <ta e="T450" id="Seg_9480" s="T449">n-n:case</ta>
            <ta e="T451" id="Seg_9481" s="T450">n-n:case</ta>
            <ta e="T452" id="Seg_9482" s="T451">n-n:poss-n:case</ta>
            <ta e="T453" id="Seg_9483" s="T452">v-v:cvb</ta>
            <ta e="T454" id="Seg_9484" s="T453">v-v:tense-v:pred.pn</ta>
            <ta e="T455" id="Seg_9485" s="T454">ptcl</ta>
            <ta e="T456" id="Seg_9486" s="T455">dempro-pro:case</ta>
            <ta e="T457" id="Seg_9487" s="T456">v-v:tense-v:poss.pn</ta>
            <ta e="T458" id="Seg_9488" s="T457">n-n:(poss)-n:case</ta>
            <ta e="T459" id="Seg_9489" s="T458">n-n:case</ta>
            <ta e="T460" id="Seg_9490" s="T459">n-n:case</ta>
            <ta e="T461" id="Seg_9491" s="T460">n-n:poss-n:case</ta>
            <ta e="T462" id="Seg_9492" s="T461">n-n:poss-n:case</ta>
            <ta e="T463" id="Seg_9493" s="T462">v-v:cvb</ta>
            <ta e="T464" id="Seg_9494" s="T463">v-v:tense-v:poss.pn</ta>
            <ta e="T465" id="Seg_9495" s="T464">n-n:poss-n:case</ta>
            <ta e="T466" id="Seg_9496" s="T465">ptcl</ta>
            <ta e="T467" id="Seg_9497" s="T466">dempro</ta>
            <ta e="T468" id="Seg_9498" s="T467">adj</ta>
            <ta e="T469" id="Seg_9499" s="T468">n-n:(poss)-n:case</ta>
            <ta e="T470" id="Seg_9500" s="T469">n-n:(poss)-n:case</ta>
            <ta e="T471" id="Seg_9501" s="T470">n-n:(poss)-n:case</ta>
            <ta e="T472" id="Seg_9502" s="T471">n-n:(poss)-n:case</ta>
            <ta e="T473" id="Seg_9503" s="T472">v-v:cvb</ta>
            <ta e="T474" id="Seg_9504" s="T473">post</ta>
            <ta e="T475" id="Seg_9505" s="T474">n-n:poss-n:case</ta>
            <ta e="T476" id="Seg_9506" s="T475">n-n:poss-n:case</ta>
            <ta e="T477" id="Seg_9507" s="T476">n-n:case</ta>
            <ta e="T478" id="Seg_9508" s="T477">n-n:poss-n:case</ta>
            <ta e="T479" id="Seg_9509" s="T478">post</ta>
            <ta e="T480" id="Seg_9510" s="T479">v-v&gt;v-v:cvb</ta>
            <ta e="T481" id="Seg_9511" s="T480">v-v:(tense)-v:mood.pn</ta>
            <ta e="T482" id="Seg_9512" s="T481">n-n:case</ta>
            <ta e="T483" id="Seg_9513" s="T482">n-n:case</ta>
            <ta e="T484" id="Seg_9514" s="T483">v-v:ptcp</ta>
            <ta e="T485" id="Seg_9515" s="T484">ptcl</ta>
            <ta e="T486" id="Seg_9516" s="T485">n-n:poss-n:case</ta>
            <ta e="T487" id="Seg_9517" s="T486">dempro</ta>
            <ta e="T488" id="Seg_9518" s="T487">v-v:tense-v:poss.pn</ta>
            <ta e="T489" id="Seg_9519" s="T488">v-v:(tense)-v:mood.pn</ta>
            <ta e="T490" id="Seg_9520" s="T489">dempro-pro:case</ta>
            <ta e="T491" id="Seg_9521" s="T490">n-n:case</ta>
            <ta e="T492" id="Seg_9522" s="T491">conj</ta>
            <ta e="T493" id="Seg_9523" s="T492">pers-pro:case</ta>
            <ta e="T494" id="Seg_9524" s="T493">v-v:ptcp-v:(poss)</ta>
            <ta e="T495" id="Seg_9525" s="T494">v-v:tense-v:poss.pn</ta>
            <ta e="T496" id="Seg_9526" s="T495">ptcl</ta>
            <ta e="T497" id="Seg_9527" s="T496">ptcl</ta>
            <ta e="T498" id="Seg_9528" s="T497">adv</ta>
            <ta e="T499" id="Seg_9529" s="T498">v-v:cvb</ta>
            <ta e="T500" id="Seg_9530" s="T499">v-v:tense-v:poss.pn</ta>
            <ta e="T501" id="Seg_9531" s="T500">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T502" id="Seg_9532" s="T501">n-n:poss-n:case</ta>
            <ta e="T503" id="Seg_9533" s="T502">v-v:(neg)-v:tense-v:poss.pn</ta>
            <ta e="T504" id="Seg_9534" s="T503">adv</ta>
            <ta e="T505" id="Seg_9535" s="T504">dempro-pro&gt;adv</ta>
            <ta e="T506" id="Seg_9536" s="T505">n-n&gt;adj-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T507" id="Seg_9537" s="T506">v-v:mood-v:pred.pn</ta>
            <ta e="T508" id="Seg_9538" s="T507">pers-pro:case</ta>
            <ta e="T509" id="Seg_9539" s="T508">n-n:poss-n:case-n-n:poss-n:case</ta>
            <ta e="T510" id="Seg_9540" s="T509">v-v:cvb</ta>
            <ta e="T511" id="Seg_9541" s="T510">v-v:cvb-v-v:cvb</ta>
            <ta e="T512" id="Seg_9542" s="T511">v-v:tense-v:poss.pn</ta>
            <ta e="T513" id="Seg_9543" s="T512">ptcl</ta>
            <ta e="T514" id="Seg_9544" s="T513">v-v:tense-v:poss.pn</ta>
            <ta e="T515" id="Seg_9545" s="T514">que-pro:case</ta>
            <ta e="T516" id="Seg_9546" s="T515">v-v:tense-v:pred.pn</ta>
            <ta e="T517" id="Seg_9547" s="T516">v-v:tense-v:poss.pn</ta>
            <ta e="T518" id="Seg_9548" s="T517">adv</ta>
            <ta e="T519" id="Seg_9549" s="T518">v-v:mood-v:pred.pn</ta>
            <ta e="T520" id="Seg_9550" s="T519">v-v:tense-v:pred.pn</ta>
            <ta e="T521" id="Seg_9551" s="T520">n-n:(poss)-n:case</ta>
            <ta e="T522" id="Seg_9552" s="T521">adj</ta>
            <ta e="T523" id="Seg_9553" s="T522">n-n:case</ta>
            <ta e="T524" id="Seg_9554" s="T523">n-n&gt;adj-n:(pred.pn)</ta>
            <ta e="T525" id="Seg_9555" s="T524">dempro-pro:case</ta>
            <ta e="T526" id="Seg_9556" s="T525">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T527" id="Seg_9557" s="T526">v-v:(tense)-v:mood.pn</ta>
            <ta e="T528" id="Seg_9558" s="T527">n-n:case</ta>
            <ta e="T529" id="Seg_9559" s="T528">adv</ta>
            <ta e="T530" id="Seg_9560" s="T529">v-v:tense-v:pred.pn</ta>
            <ta e="T531" id="Seg_9561" s="T530">n-n:case</ta>
            <ta e="T532" id="Seg_9562" s="T531">n-n:case</ta>
            <ta e="T533" id="Seg_9563" s="T532">v-v:tense-v:pred.pn</ta>
            <ta e="T534" id="Seg_9564" s="T533">interj</ta>
            <ta e="T535" id="Seg_9565" s="T534">v-v:tense-v:poss.pn</ta>
            <ta e="T536" id="Seg_9566" s="T535">ptcl</ta>
            <ta e="T537" id="Seg_9567" s="T536">n-n:case</ta>
            <ta e="T538" id="Seg_9568" s="T537">n-n:case</ta>
            <ta e="T539" id="Seg_9569" s="T538">n-n:poss-n:case</ta>
            <ta e="T540" id="Seg_9570" s="T539">n-n:poss-n:case</ta>
            <ta e="T541" id="Seg_9571" s="T540">n-n:case</ta>
            <ta e="T542" id="Seg_9572" s="T541">n-n:case</ta>
            <ta e="T543" id="Seg_9573" s="T542">n-n:poss-n:case</ta>
            <ta e="T544" id="Seg_9574" s="T543">post</ta>
            <ta e="T545" id="Seg_9575" s="T544">v-v&gt;v-v:cvb</ta>
            <ta e="T546" id="Seg_9576" s="T545">v-v:tense-v:pred.pn</ta>
            <ta e="T547" id="Seg_9577" s="T546">n-n:case</ta>
            <ta e="T548" id="Seg_9578" s="T547">n-n:case</ta>
            <ta e="T549" id="Seg_9579" s="T548">v-v:ptcp</ta>
            <ta e="T550" id="Seg_9580" s="T549">ptcl</ta>
            <ta e="T551" id="Seg_9581" s="T550">n-n:poss-n:case</ta>
            <ta e="T552" id="Seg_9582" s="T551">dempro</ta>
            <ta e="T553" id="Seg_9583" s="T552">v-v:tense-v:poss.pn</ta>
            <ta e="T554" id="Seg_9584" s="T553">v-v:tense-v:pred.pn</ta>
            <ta e="T555" id="Seg_9585" s="T554">n-n:case</ta>
            <ta e="T556" id="Seg_9586" s="T555">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T557" id="Seg_9587" s="T556">n-n:poss-n:case</ta>
            <ta e="T558" id="Seg_9588" s="T557">v-v:(neg)-v:tense-v:poss.pn</ta>
            <ta e="T559" id="Seg_9589" s="T558">adv</ta>
            <ta e="T560" id="Seg_9590" s="T559">dempro-pro&gt;adv</ta>
            <ta e="T561" id="Seg_9591" s="T560">n-n&gt;adj-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T562" id="Seg_9592" s="T561">v-v:mood-v:pred.pn</ta>
            <ta e="T563" id="Seg_9593" s="T562">pers-pro:case</ta>
            <ta e="T564" id="Seg_9594" s="T563">n-n:poss-n:case-n-n:poss-n:case</ta>
            <ta e="T565" id="Seg_9595" s="T564">v-v:cvb</ta>
            <ta e="T566" id="Seg_9596" s="T565">v-v:cvb-v-v:cvb</ta>
            <ta e="T567" id="Seg_9597" s="T566">v-v:tense-v:poss.pn</ta>
            <ta e="T568" id="Seg_9598" s="T567">ptcl</ta>
            <ta e="T569" id="Seg_9599" s="T568">v-v:tense-v:poss.pn</ta>
            <ta e="T570" id="Seg_9600" s="T569">que-pro:case</ta>
            <ta e="T571" id="Seg_9601" s="T570">v-v:tense-v:pred.pn</ta>
            <ta e="T572" id="Seg_9602" s="T571">v-v:tense-v:pred.pn</ta>
            <ta e="T573" id="Seg_9603" s="T572">adj</ta>
            <ta e="T574" id="Seg_9604" s="T573">n-n:case</ta>
            <ta e="T575" id="Seg_9605" s="T574">n-n&gt;adj-n:(pred.pn)</ta>
            <ta e="T576" id="Seg_9606" s="T575">dempro-pro:case</ta>
            <ta e="T577" id="Seg_9607" s="T576">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T578" id="Seg_9608" s="T577">v-v:tense-v:pred.pn</ta>
            <ta e="T579" id="Seg_9609" s="T578">n-n:case</ta>
            <ta e="T580" id="Seg_9610" s="T579">n-n:case</ta>
            <ta e="T581" id="Seg_9611" s="T580">n-n:case</ta>
            <ta e="T582" id="Seg_9612" s="T581">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T583" id="Seg_9613" s="T582">n-n&gt;adj</ta>
            <ta e="T584" id="Seg_9614" s="T583">n-n:case</ta>
            <ta e="T585" id="Seg_9615" s="T584">v-v:tense-v:pred.pn</ta>
            <ta e="T586" id="Seg_9616" s="T585">v-v:tense-v:pred.pn</ta>
            <ta e="T587" id="Seg_9617" s="T586">adj</ta>
            <ta e="T588" id="Seg_9618" s="T587">n-n:case</ta>
            <ta e="T589" id="Seg_9619" s="T588">n-n:(poss)-n:case</ta>
            <ta e="T590" id="Seg_9620" s="T589">n-n&gt;adj-n:case</ta>
            <ta e="T591" id="Seg_9621" s="T590">v-v:tense-v:pred.pn</ta>
            <ta e="T592" id="Seg_9622" s="T591">dempro-pro:case</ta>
            <ta e="T593" id="Seg_9623" s="T592">v-v:cvb</ta>
            <ta e="T594" id="Seg_9624" s="T593">v-v:mood.pn</ta>
            <ta e="T595" id="Seg_9625" s="T594">v-v:tense-v:pred.pn</ta>
            <ta e="T596" id="Seg_9626" s="T595">dempro-pro:case</ta>
            <ta e="T597" id="Seg_9627" s="T596">v-v:(neg)-v:mood-v:temp.pn</ta>
            <ta e="T598" id="Seg_9628" s="T597">cardnum-cardnum&gt;collnum-n:poss-n:case</ta>
            <ta e="T599" id="Seg_9629" s="T598">n-n:poss-n:case</ta>
            <ta e="T600" id="Seg_9630" s="T599">v-v:tense-v:poss.pn</ta>
            <ta e="T601" id="Seg_9631" s="T600">v-v:tense-v:pred.pn</ta>
            <ta e="T602" id="Seg_9632" s="T601">n-n:case</ta>
            <ta e="T603" id="Seg_9633" s="T602">n-n:case</ta>
            <ta e="T604" id="Seg_9634" s="T603">v-v:cvb</ta>
            <ta e="T605" id="Seg_9635" s="T604">v-v:tense-v:poss.pn</ta>
            <ta e="T606" id="Seg_9636" s="T605">v-v:cvb-v-v:cvb</ta>
            <ta e="T607" id="Seg_9637" s="T606">n-n:case</ta>
            <ta e="T608" id="Seg_9638" s="T607">interj</ta>
            <ta e="T609" id="Seg_9639" s="T608">n</ta>
            <ta e="T610" id="Seg_9640" s="T609">que-pro:(ins)-pro:(poss)-pro:case</ta>
            <ta e="T611" id="Seg_9641" s="T610">n-n:(poss)-n:case-ptcl</ta>
            <ta e="T612" id="Seg_9642" s="T611">v-v:tense-v:pred.pn</ta>
            <ta e="T613" id="Seg_9643" s="T612">interj-ptcl</ta>
            <ta e="T614" id="Seg_9644" s="T613">ptcl</ta>
            <ta e="T615" id="Seg_9645" s="T614">v-v:ptcp</ta>
            <ta e="T616" id="Seg_9646" s="T615">n-n:(poss)-n:case</ta>
            <ta e="T617" id="Seg_9647" s="T616">v-v:tense-v:poss.pn</ta>
            <ta e="T618" id="Seg_9648" s="T617">dempro</ta>
            <ta e="T619" id="Seg_9649" s="T618">n-n:case</ta>
            <ta e="T620" id="Seg_9650" s="T619">v-v:cvb</ta>
            <ta e="T621" id="Seg_9651" s="T620">v-v:mood.pn</ta>
            <ta e="T622" id="Seg_9652" s="T621">v-v:tense-v:pred.pn</ta>
            <ta e="T623" id="Seg_9653" s="T622">dempro-pro:case</ta>
            <ta e="T624" id="Seg_9654" s="T623">v-v:(neg)-v:mood-v:temp.pn</ta>
            <ta e="T625" id="Seg_9655" s="T624">cardnum-cardnum&gt;collnum-n:poss-n:case</ta>
            <ta e="T626" id="Seg_9656" s="T625">n-n:poss-n:case</ta>
            <ta e="T627" id="Seg_9657" s="T626">v-v:tense-v:poss.pn</ta>
            <ta e="T628" id="Seg_9658" s="T627">v-v:tense-v:pred.pn</ta>
            <ta e="T629" id="Seg_9659" s="T628">n-n:case</ta>
            <ta e="T630" id="Seg_9660" s="T629">v-v:tense-v:pred.pn</ta>
            <ta e="T631" id="Seg_9661" s="T630">v-v:mood.pn</ta>
            <ta e="T632" id="Seg_9662" s="T631">adv</ta>
            <ta e="T633" id="Seg_9663" s="T632">n-n:case</ta>
            <ta e="T634" id="Seg_9664" s="T633">n-n:case</ta>
            <ta e="T635" id="Seg_9665" s="T634">v-v:tense-v:poss.pn</ta>
            <ta e="T636" id="Seg_9666" s="T635">n-n:case</ta>
            <ta e="T637" id="Seg_9667" s="T636">n-n:case</ta>
            <ta e="T638" id="Seg_9668" s="T637">n-n:case</ta>
            <ta e="T639" id="Seg_9669" s="T638">n-n:case</ta>
            <ta e="T640" id="Seg_9670" s="T639">post</ta>
            <ta e="T641" id="Seg_9671" s="T640">v-v:cvb</ta>
            <ta e="T642" id="Seg_9672" s="T641">v-v:mood.pn</ta>
            <ta e="T643" id="Seg_9673" s="T642">n-n:case</ta>
            <ta e="T644" id="Seg_9674" s="T643">v-v:cvb</ta>
            <ta e="T645" id="Seg_9675" s="T644">v-v:tense-v:pred.pn</ta>
            <ta e="T646" id="Seg_9676" s="T645">n-n:case</ta>
            <ta e="T647" id="Seg_9677" s="T646">n-n:case</ta>
            <ta e="T648" id="Seg_9678" s="T647">v-v:tense-v:pred.pn</ta>
            <ta e="T649" id="Seg_9679" s="T648">n-n:case</ta>
            <ta e="T650" id="Seg_9680" s="T649">v-v:tense-v:pred.pn</ta>
            <ta e="T651" id="Seg_9681" s="T650">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T652" id="Seg_9682" s="T651">n-n:poss-n:case</ta>
            <ta e="T653" id="Seg_9683" s="T652">v-v:cvb</ta>
            <ta e="T654" id="Seg_9684" s="T653">v-v:tense-v:poss.pn</ta>
            <ta e="T655" id="Seg_9685" s="T654">v-v:tense-v:pred.pn</ta>
            <ta e="T656" id="Seg_9686" s="T655">n-n:case</ta>
            <ta e="T657" id="Seg_9687" s="T656">n-n:case</ta>
            <ta e="T658" id="Seg_9688" s="T657">n-n:case</ta>
            <ta e="T659" id="Seg_9689" s="T658">v-v:cvb</ta>
            <ta e="T660" id="Seg_9690" s="T659">v-v:tense-v:pred.pn</ta>
            <ta e="T661" id="Seg_9691" s="T660">n-n:case</ta>
            <ta e="T662" id="Seg_9692" s="T661">v-v:tense-v:pred.pn</ta>
            <ta e="T663" id="Seg_9693" s="T662">que-pro:case-que-pro:case</ta>
            <ta e="T664" id="Seg_9694" s="T663">v-v:tense-v:poss.pn</ta>
            <ta e="T665" id="Seg_9695" s="T664">v-v:tense-v:poss.pn</ta>
            <ta e="T666" id="Seg_9696" s="T665">n-n&gt;v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T667" id="Seg_9697" s="T666">n-n:case</ta>
            <ta e="T668" id="Seg_9698" s="T667">n-n:case</ta>
            <ta e="T669" id="Seg_9699" s="T668">n-n:case</ta>
            <ta e="T670" id="Seg_9700" s="T669">post</ta>
            <ta e="T671" id="Seg_9701" s="T670">v-v&gt;adv</ta>
            <ta e="T672" id="Seg_9702" s="T671">v-v:tense-v:poss.pn</ta>
            <ta e="T673" id="Seg_9703" s="T672">adj-n:poss-n:case</ta>
            <ta e="T674" id="Seg_9704" s="T673">n-n:(poss)-n:case</ta>
            <ta e="T675" id="Seg_9705" s="T674">v-v:tense-v:pred.pn</ta>
            <ta e="T676" id="Seg_9706" s="T675">v-v:tense-v:pred.pn</ta>
            <ta e="T677" id="Seg_9707" s="T676">n-n:case</ta>
            <ta e="T678" id="Seg_9708" s="T677">n-n:case</ta>
            <ta e="T679" id="Seg_9709" s="T678">n-n&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T680" id="Seg_9710" s="T679">n</ta>
            <ta e="T681" id="Seg_9711" s="T680">dempro</ta>
            <ta e="T682" id="Seg_9712" s="T681">cardnum</ta>
            <ta e="T683" id="Seg_9713" s="T682">n-n:case</ta>
            <ta e="T684" id="Seg_9714" s="T683">cardnum</ta>
            <ta e="T685" id="Seg_9715" s="T684">n-n:case</ta>
            <ta e="T686" id="Seg_9716" s="T685">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T687" id="Seg_9717" s="T686">v-v:(ins)-v:(neg)-v:pred.pn</ta>
            <ta e="T688" id="Seg_9718" s="T687">pers-pro:case</ta>
            <ta e="T689" id="Seg_9719" s="T688">v-v:mood.pn</ta>
            <ta e="T690" id="Seg_9720" s="T689">n-n:(poss)-n:case</ta>
            <ta e="T691" id="Seg_9721" s="T690">ptcl</ta>
            <ta e="T692" id="Seg_9722" s="T691">v-v:mood.pn</ta>
            <ta e="T693" id="Seg_9723" s="T692">ptcl</ta>
            <ta e="T694" id="Seg_9724" s="T693">v-v:tense-v:pred.pn</ta>
            <ta e="T695" id="Seg_9725" s="T694">n-n:case</ta>
            <ta e="T696" id="Seg_9726" s="T695">n-n:case</ta>
            <ta e="T697" id="Seg_9727" s="T696">que</ta>
            <ta e="T698" id="Seg_9728" s="T697">n-n&gt;adj</ta>
            <ta e="T699" id="Seg_9729" s="T698">n-n:poss-n:case</ta>
            <ta e="T700" id="Seg_9730" s="T699">adj-n:poss-n:case</ta>
            <ta e="T701" id="Seg_9731" s="T700">v-v:mood.pn</ta>
            <ta e="T702" id="Seg_9732" s="T701">v-v:tense-v:pred.pn</ta>
            <ta e="T703" id="Seg_9733" s="T702">n-n:case</ta>
            <ta e="T704" id="Seg_9734" s="T703">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T705" id="Seg_9735" s="T704">v-v:ptcp</ta>
            <ta e="T706" id="Seg_9736" s="T705">n-n:(poss)-n:case-ptcl</ta>
            <ta e="T707" id="Seg_9737" s="T706">v-v:tense-v:pred.pn</ta>
            <ta e="T708" id="Seg_9738" s="T707">n-n:(poss)-n:case</ta>
            <ta e="T709" id="Seg_9739" s="T708">v-v:cvb</ta>
            <ta e="T710" id="Seg_9740" s="T709">v-v:tense-v:pred.pn</ta>
            <ta e="T711" id="Seg_9741" s="T710">n-n:case</ta>
            <ta e="T712" id="Seg_9742" s="T711">n-n:poss-n:case</ta>
            <ta e="T713" id="Seg_9743" s="T712">n-n:(num)-n:case</ta>
            <ta e="T714" id="Seg_9744" s="T713">n-n:(num)-n:case</ta>
            <ta e="T715" id="Seg_9745" s="T714">n-n:poss-n:case</ta>
            <ta e="T716" id="Seg_9746" s="T715">n-n:case</ta>
            <ta e="T717" id="Seg_9747" s="T716">n-n:case</ta>
            <ta e="T718" id="Seg_9748" s="T717">n-n:poss-n:case</ta>
            <ta e="T719" id="Seg_9749" s="T718">v-v:cvb</ta>
            <ta e="T720" id="Seg_9750" s="T719">v-v:tense-v:pred.pn</ta>
            <ta e="T721" id="Seg_9751" s="T720">n-n:case</ta>
            <ta e="T722" id="Seg_9752" s="T721">n-n:case</ta>
            <ta e="T723" id="Seg_9753" s="T722">adv</ta>
            <ta e="T724" id="Seg_9754" s="T723">v-v:cvb</ta>
            <ta e="T725" id="Seg_9755" s="T724">v-v:tense-v:pred.pn</ta>
            <ta e="T726" id="Seg_9756" s="T725">v-v:cvb</ta>
            <ta e="T727" id="Seg_9757" s="T726">v-v:tense-v:poss.pn</ta>
            <ta e="T728" id="Seg_9758" s="T727">n-n:(poss)-n:case-n-n:(poss)-n:case</ta>
            <ta e="T729" id="Seg_9759" s="T728">n-n:case</ta>
            <ta e="T730" id="Seg_9760" s="T729">n-n:poss-n:case</ta>
            <ta e="T731" id="Seg_9761" s="T730">v-v:tense-v:pred.pn</ta>
            <ta e="T732" id="Seg_9762" s="T731">interj-ptcl</ta>
            <ta e="T733" id="Seg_9763" s="T732">dempro</ta>
            <ta e="T734" id="Seg_9764" s="T733">que</ta>
            <ta e="T735" id="Seg_9765" s="T734">ptcl-ptcl:(pred.pn)-ptcl</ta>
            <ta e="T736" id="Seg_9766" s="T735">que</ta>
            <ta e="T737" id="Seg_9767" s="T736">v-v:tense-v:(ins)-v:poss.pn-ptcl</ta>
            <ta e="T738" id="Seg_9768" s="T737">n-n:poss-n:case</ta>
            <ta e="T739" id="Seg_9769" s="T738">v-v&gt;v-v:cvb</ta>
            <ta e="T740" id="Seg_9770" s="T739">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T741" id="Seg_9771" s="T740">n-n:(poss)-n:case</ta>
            <ta e="T742" id="Seg_9772" s="T741">interj</ta>
            <ta e="T743" id="Seg_9773" s="T742">n</ta>
            <ta e="T744" id="Seg_9774" s="T743">pers-pro:case</ta>
            <ta e="T745" id="Seg_9775" s="T744">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T746" id="Seg_9776" s="T745">v-v:tense-v:pred.pn</ta>
            <ta e="T747" id="Seg_9777" s="T746">que</ta>
            <ta e="T748" id="Seg_9778" s="T747">v-v:tense-v:pred.pn-ptcl</ta>
            <ta e="T749" id="Seg_9779" s="T748">interj</ta>
            <ta e="T750" id="Seg_9780" s="T749">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T751" id="Seg_9781" s="T750">v-v:tense-v:poss.pn</ta>
            <ta e="T752" id="Seg_9782" s="T751">conj</ta>
            <ta e="T753" id="Seg_9783" s="T752">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T754" id="Seg_9784" s="T753">v-v:tense-v:poss.pn</ta>
            <ta e="T755" id="Seg_9785" s="T754">que-pro:case</ta>
            <ta e="T756" id="Seg_9786" s="T755">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T757" id="Seg_9787" s="T756">v-v:(tense)-v:mood.pn</ta>
            <ta e="T758" id="Seg_9788" s="T757">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T759" id="Seg_9789" s="T758">ptcl</ta>
            <ta e="T760" id="Seg_9790" s="T759">v-v:tense-v:pred.pn</ta>
            <ta e="T761" id="Seg_9791" s="T760">n-n:case</ta>
            <ta e="T762" id="Seg_9792" s="T761">n-n:case</ta>
            <ta e="T763" id="Seg_9793" s="T762">n-n:case</ta>
            <ta e="T764" id="Seg_9794" s="T763">v-v:cvb</ta>
            <ta e="T765" id="Seg_9795" s="T764">v-v:(neg)-v:tense-v:poss.pn</ta>
            <ta e="T766" id="Seg_9796" s="T765">ptcl</ta>
            <ta e="T767" id="Seg_9797" s="T766">n-n:(poss)-n:case</ta>
            <ta e="T768" id="Seg_9798" s="T767">v-v:tense-v:pred.pn</ta>
            <ta e="T769" id="Seg_9799" s="T768">adv</ta>
            <ta e="T770" id="Seg_9800" s="T769">n-n&gt;adj</ta>
            <ta e="T771" id="Seg_9801" s="T770">n-n:case</ta>
            <ta e="T772" id="Seg_9802" s="T771">v-v:ptcp</ta>
            <ta e="T773" id="Seg_9803" s="T772">v-v:tense-v:pred.pn</ta>
            <ta e="T774" id="Seg_9804" s="T773">n-n:poss-n:case</ta>
            <ta e="T775" id="Seg_9805" s="T774">v-v:cvb</ta>
            <ta e="T776" id="Seg_9806" s="T775">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T777" id="Seg_9807" s="T776">v-v:mood.pn</ta>
            <ta e="T778" id="Seg_9808" s="T777">pers-pro:case</ta>
            <ta e="T779" id="Seg_9809" s="T778">adj</ta>
            <ta e="T780" id="Seg_9810" s="T779">n-n:case</ta>
            <ta e="T781" id="Seg_9811" s="T780">v-v:cvb</ta>
            <ta e="T782" id="Seg_9812" s="T781">v-v:tense-v:poss.pn</ta>
            <ta e="T783" id="Seg_9813" s="T782">adj</ta>
            <ta e="T784" id="Seg_9814" s="T783">n-n:case</ta>
            <ta e="T785" id="Seg_9815" s="T784">n-n:poss-n:case</ta>
            <ta e="T786" id="Seg_9816" s="T785">adj-n:poss-n:case</ta>
            <ta e="T787" id="Seg_9817" s="T786">n-n:case</ta>
            <ta e="T788" id="Seg_9818" s="T787">n-n:case</ta>
            <ta e="T789" id="Seg_9819" s="T788">n-n:case</ta>
            <ta e="T790" id="Seg_9820" s="T789">v-v:mood-v:poss.pn</ta>
            <ta e="T791" id="Seg_9821" s="T790">n-n:case</ta>
            <ta e="T792" id="Seg_9822" s="T791">n-n:case</ta>
            <ta e="T793" id="Seg_9823" s="T792">v-v:cvb</ta>
            <ta e="T794" id="Seg_9824" s="T793">n-n:(ins)-n:case</ta>
            <ta e="T795" id="Seg_9825" s="T794">v-v:cvb-v-v:cvb</ta>
            <ta e="T796" id="Seg_9826" s="T795">v-v:mood-v:poss.pn</ta>
            <ta e="T797" id="Seg_9827" s="T796">ptcl</ta>
            <ta e="T798" id="Seg_9828" s="T797">adj-n:(poss)-n:case</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_9829" s="T0">ptcl</ta>
            <ta e="T2" id="Seg_9830" s="T1">n</ta>
            <ta e="T3" id="Seg_9831" s="T2">n</ta>
            <ta e="T4" id="Seg_9832" s="T3">v</ta>
            <ta e="T5" id="Seg_9833" s="T4">n</ta>
            <ta e="T6" id="Seg_9834" s="T5">adj</ta>
            <ta e="T7" id="Seg_9835" s="T6">adj</ta>
            <ta e="T8" id="Seg_9836" s="T7">adj</ta>
            <ta e="T9" id="Seg_9837" s="T8">n</ta>
            <ta e="T10" id="Seg_9838" s="T9">dempro</ta>
            <ta e="T11" id="Seg_9839" s="T10">n</ta>
            <ta e="T12" id="Seg_9840" s="T11">v</ta>
            <ta e="T13" id="Seg_9841" s="T12">adj</ta>
            <ta e="T14" id="Seg_9842" s="T13">n</ta>
            <ta e="T15" id="Seg_9843" s="T14">n</ta>
            <ta e="T16" id="Seg_9844" s="T15">n</ta>
            <ta e="T17" id="Seg_9845" s="T16">post</ta>
            <ta e="T18" id="Seg_9846" s="T17">cardnum</ta>
            <ta e="T19" id="Seg_9847" s="T18">n</ta>
            <ta e="T20" id="Seg_9848" s="T19">n</ta>
            <ta e="T21" id="Seg_9849" s="T20">post</ta>
            <ta e="T22" id="Seg_9850" s="T21">v</ta>
            <ta e="T23" id="Seg_9851" s="T22">aux</ta>
            <ta e="T24" id="Seg_9852" s="T23">dempro</ta>
            <ta e="T25" id="Seg_9853" s="T24">v</ta>
            <ta e="T26" id="Seg_9854" s="T25">v</ta>
            <ta e="T27" id="Seg_9855" s="T26">adj</ta>
            <ta e="T28" id="Seg_9856" s="T27">n</ta>
            <ta e="T29" id="Seg_9857" s="T28">cardnum</ta>
            <ta e="T30" id="Seg_9858" s="T29">n</ta>
            <ta e="T31" id="Seg_9859" s="T30">adj</ta>
            <ta e="T32" id="Seg_9860" s="T31">ptcl</ta>
            <ta e="T33" id="Seg_9861" s="T32">adv</ta>
            <ta e="T34" id="Seg_9862" s="T33">n</ta>
            <ta e="T35" id="Seg_9863" s="T34">n</ta>
            <ta e="T36" id="Seg_9864" s="T35">v</ta>
            <ta e="T37" id="Seg_9865" s="T36">interj</ta>
            <ta e="T38" id="Seg_9866" s="T37">ptcl</ta>
            <ta e="T39" id="Seg_9867" s="T38">adj</ta>
            <ta e="T40" id="Seg_9868" s="T39">n</ta>
            <ta e="T41" id="Seg_9869" s="T40">que</ta>
            <ta e="T42" id="Seg_9870" s="T41">adj</ta>
            <ta e="T43" id="Seg_9871" s="T42">n</ta>
            <ta e="T44" id="Seg_9872" s="T43">v</ta>
            <ta e="T45" id="Seg_9873" s="T44">v</ta>
            <ta e="T46" id="Seg_9874" s="T45">n</ta>
            <ta e="T47" id="Seg_9875" s="T46">v</ta>
            <ta e="T48" id="Seg_9876" s="T47">adj</ta>
            <ta e="T49" id="Seg_9877" s="T48">adj</ta>
            <ta e="T50" id="Seg_9878" s="T49">que</ta>
            <ta e="T51" id="Seg_9879" s="T50">adv</ta>
            <ta e="T52" id="Seg_9880" s="T51">v</ta>
            <ta e="T53" id="Seg_9881" s="T52">cardnum</ta>
            <ta e="T54" id="Seg_9882" s="T53">n</ta>
            <ta e="T55" id="Seg_9883" s="T54">v</ta>
            <ta e="T56" id="Seg_9884" s="T55">dempro</ta>
            <ta e="T57" id="Seg_9885" s="T56">v</ta>
            <ta e="T58" id="Seg_9886" s="T57">v</ta>
            <ta e="T59" id="Seg_9887" s="T58">n</ta>
            <ta e="T60" id="Seg_9888" s="T59">adj</ta>
            <ta e="T61" id="Seg_9889" s="T60">n</ta>
            <ta e="T62" id="Seg_9890" s="T61">v</ta>
            <ta e="T63" id="Seg_9891" s="T62">adj</ta>
            <ta e="T64" id="Seg_9892" s="T63">n</ta>
            <ta e="T65" id="Seg_9893" s="T64">que</ta>
            <ta e="T66" id="Seg_9894" s="T65">ptcl</ta>
            <ta e="T67" id="Seg_9895" s="T66">v</ta>
            <ta e="T68" id="Seg_9896" s="T67">v</ta>
            <ta e="T69" id="Seg_9897" s="T68">adj</ta>
            <ta e="T70" id="Seg_9898" s="T69">n</ta>
            <ta e="T71" id="Seg_9899" s="T70">v</ta>
            <ta e="T72" id="Seg_9900" s="T71">n</ta>
            <ta e="T73" id="Seg_9901" s="T72">n</ta>
            <ta e="T74" id="Seg_9902" s="T73">adj</ta>
            <ta e="T75" id="Seg_9903" s="T74">adj</ta>
            <ta e="T76" id="Seg_9904" s="T75">post</ta>
            <ta e="T77" id="Seg_9905" s="T76">pers</ta>
            <ta e="T78" id="Seg_9906" s="T77">n</ta>
            <ta e="T79" id="Seg_9907" s="T78">v</ta>
            <ta e="T80" id="Seg_9908" s="T79">v</ta>
            <ta e="T81" id="Seg_9909" s="T80">v</ta>
            <ta e="T82" id="Seg_9910" s="T81">n</ta>
            <ta e="T83" id="Seg_9911" s="T82">v</ta>
            <ta e="T84" id="Seg_9912" s="T83">interj</ta>
            <ta e="T85" id="Seg_9913" s="T84">n</ta>
            <ta e="T86" id="Seg_9914" s="T85">que</ta>
            <ta e="T87" id="Seg_9915" s="T86">v</ta>
            <ta e="T88" id="Seg_9916" s="T87">ptcl</ta>
            <ta e="T89" id="Seg_9917" s="T88">n</ta>
            <ta e="T90" id="Seg_9918" s="T89">v</ta>
            <ta e="T91" id="Seg_9919" s="T90">adv</ta>
            <ta e="T92" id="Seg_9920" s="T91">v</ta>
            <ta e="T93" id="Seg_9921" s="T92">n</ta>
            <ta e="T94" id="Seg_9922" s="T93">n</ta>
            <ta e="T95" id="Seg_9923" s="T94">n</ta>
            <ta e="T96" id="Seg_9924" s="T95">n</ta>
            <ta e="T97" id="Seg_9925" s="T96">v</ta>
            <ta e="T98" id="Seg_9926" s="T97">v</ta>
            <ta e="T99" id="Seg_9927" s="T98">adj</ta>
            <ta e="T100" id="Seg_9928" s="T99">n</ta>
            <ta e="T101" id="Seg_9929" s="T100">v</ta>
            <ta e="T102" id="Seg_9930" s="T101">aux</ta>
            <ta e="T103" id="Seg_9931" s="T102">que</ta>
            <ta e="T104" id="Seg_9932" s="T103">v</ta>
            <ta e="T105" id="Seg_9933" s="T104">dempro</ta>
            <ta e="T106" id="Seg_9934" s="T105">adj</ta>
            <ta e="T107" id="Seg_9935" s="T106">adj</ta>
            <ta e="T108" id="Seg_9936" s="T107">que</ta>
            <ta e="T109" id="Seg_9937" s="T108">v</ta>
            <ta e="T110" id="Seg_9938" s="T109">v</ta>
            <ta e="T111" id="Seg_9939" s="T110">interj</ta>
            <ta e="T112" id="Seg_9940" s="T111">n</ta>
            <ta e="T113" id="Seg_9941" s="T112">n</ta>
            <ta e="T114" id="Seg_9942" s="T113">ptcl</ta>
            <ta e="T115" id="Seg_9943" s="T114">ptcl</ta>
            <ta e="T116" id="Seg_9944" s="T115">v</ta>
            <ta e="T117" id="Seg_9945" s="T116">v</ta>
            <ta e="T118" id="Seg_9946" s="T117">v</ta>
            <ta e="T119" id="Seg_9947" s="T118">adj</ta>
            <ta e="T120" id="Seg_9948" s="T119">adj</ta>
            <ta e="T121" id="Seg_9949" s="T120">v</ta>
            <ta e="T122" id="Seg_9950" s="T121">n</ta>
            <ta e="T123" id="Seg_9951" s="T122">cardnum</ta>
            <ta e="T124" id="Seg_9952" s="T123">v</ta>
            <ta e="T125" id="Seg_9953" s="T124">adj</ta>
            <ta e="T126" id="Seg_9954" s="T125">n</ta>
            <ta e="T127" id="Seg_9955" s="T126">adj</ta>
            <ta e="T128" id="Seg_9956" s="T127">n</ta>
            <ta e="T129" id="Seg_9957" s="T128">post</ta>
            <ta e="T130" id="Seg_9958" s="T129">v</ta>
            <ta e="T131" id="Seg_9959" s="T130">pers</ta>
            <ta e="T132" id="Seg_9960" s="T131">n</ta>
            <ta e="T133" id="Seg_9961" s="T132">v</ta>
            <ta e="T134" id="Seg_9962" s="T133">cardnum</ta>
            <ta e="T135" id="Seg_9963" s="T134">n</ta>
            <ta e="T136" id="Seg_9964" s="T135">n</ta>
            <ta e="T137" id="Seg_9965" s="T136">v</ta>
            <ta e="T138" id="Seg_9966" s="T137">n</ta>
            <ta e="T139" id="Seg_9967" s="T138">n</ta>
            <ta e="T140" id="Seg_9968" s="T139">n</ta>
            <ta e="T141" id="Seg_9969" s="T140">n</ta>
            <ta e="T142" id="Seg_9970" s="T141">v</ta>
            <ta e="T143" id="Seg_9971" s="T142">n</ta>
            <ta e="T144" id="Seg_9972" s="T143">v</ta>
            <ta e="T145" id="Seg_9973" s="T144">dempro</ta>
            <ta e="T146" id="Seg_9974" s="T145">n</ta>
            <ta e="T147" id="Seg_9975" s="T146">n</ta>
            <ta e="T148" id="Seg_9976" s="T147">v</ta>
            <ta e="T149" id="Seg_9977" s="T148">v</ta>
            <ta e="T150" id="Seg_9978" s="T149">conj</ta>
            <ta e="T151" id="Seg_9979" s="T150">v</ta>
            <ta e="T152" id="Seg_9980" s="T151">adv</ta>
            <ta e="T153" id="Seg_9981" s="T152">adj</ta>
            <ta e="T154" id="Seg_9982" s="T153">adj</ta>
            <ta e="T155" id="Seg_9983" s="T154">v</ta>
            <ta e="T156" id="Seg_9984" s="T155">n</ta>
            <ta e="T157" id="Seg_9985" s="T156">cop</ta>
            <ta e="T158" id="Seg_9986" s="T157">v</ta>
            <ta e="T159" id="Seg_9987" s="T158">pers</ta>
            <ta e="T160" id="Seg_9988" s="T159">dempro</ta>
            <ta e="T161" id="Seg_9989" s="T160">conj</ta>
            <ta e="T162" id="Seg_9990" s="T161">n</ta>
            <ta e="T163" id="Seg_9991" s="T162">v</ta>
            <ta e="T164" id="Seg_9992" s="T163">aux</ta>
            <ta e="T165" id="Seg_9993" s="T164">ptcl</ta>
            <ta e="T166" id="Seg_9994" s="T165">v</ta>
            <ta e="T167" id="Seg_9995" s="T166">n</ta>
            <ta e="T168" id="Seg_9996" s="T167">n</ta>
            <ta e="T169" id="Seg_9997" s="T168">v</ta>
            <ta e="T170" id="Seg_9998" s="T169">aux</ta>
            <ta e="T171" id="Seg_9999" s="T170">adv</ta>
            <ta e="T172" id="Seg_10000" s="T171">adv</ta>
            <ta e="T173" id="Seg_10001" s="T172">v</ta>
            <ta e="T174" id="Seg_10002" s="T173">cardnum</ta>
            <ta e="T175" id="Seg_10003" s="T174">n</ta>
            <ta e="T176" id="Seg_10004" s="T175">n</ta>
            <ta e="T177" id="Seg_10005" s="T176">v</ta>
            <ta e="T178" id="Seg_10006" s="T177">n</ta>
            <ta e="T179" id="Seg_10007" s="T178">n</ta>
            <ta e="T180" id="Seg_10008" s="T179">v</ta>
            <ta e="T181" id="Seg_10009" s="T180">interj</ta>
            <ta e="T182" id="Seg_10010" s="T181">v</ta>
            <ta e="T183" id="Seg_10011" s="T182">ptcl</ta>
            <ta e="T184" id="Seg_10012" s="T183">interj</ta>
            <ta e="T185" id="Seg_10013" s="T184">v</ta>
            <ta e="T186" id="Seg_10014" s="T185">post</ta>
            <ta e="T187" id="Seg_10015" s="T186">v</ta>
            <ta e="T188" id="Seg_10016" s="T187">adv</ta>
            <ta e="T189" id="Seg_10017" s="T188">ptcl</ta>
            <ta e="T190" id="Seg_10018" s="T189">n</ta>
            <ta e="T191" id="Seg_10019" s="T190">ptcl</ta>
            <ta e="T192" id="Seg_10020" s="T191">v</ta>
            <ta e="T193" id="Seg_10021" s="T192">ptcl</ta>
            <ta e="T194" id="Seg_10022" s="T193">que</ta>
            <ta e="T195" id="Seg_10023" s="T194">adj</ta>
            <ta e="T196" id="Seg_10024" s="T195">n</ta>
            <ta e="T197" id="Seg_10025" s="T196">adj</ta>
            <ta e="T198" id="Seg_10026" s="T197">n</ta>
            <ta e="T199" id="Seg_10027" s="T198">post</ta>
            <ta e="T200" id="Seg_10028" s="T199">v</ta>
            <ta e="T201" id="Seg_10029" s="T200">pers</ta>
            <ta e="T202" id="Seg_10030" s="T201">n</ta>
            <ta e="T203" id="Seg_10031" s="T202">v</ta>
            <ta e="T204" id="Seg_10032" s="T203">cardnum</ta>
            <ta e="T205" id="Seg_10033" s="T204">n</ta>
            <ta e="T206" id="Seg_10034" s="T205">n</ta>
            <ta e="T207" id="Seg_10035" s="T206">v</ta>
            <ta e="T208" id="Seg_10036" s="T207">n</ta>
            <ta e="T209" id="Seg_10037" s="T208">n</ta>
            <ta e="T210" id="Seg_10038" s="T209">n</ta>
            <ta e="T211" id="Seg_10039" s="T210">n</ta>
            <ta e="T212" id="Seg_10040" s="T211">v</ta>
            <ta e="T213" id="Seg_10041" s="T212">n</ta>
            <ta e="T214" id="Seg_10042" s="T213">v</ta>
            <ta e="T215" id="Seg_10043" s="T214">dempro</ta>
            <ta e="T216" id="Seg_10044" s="T215">n</ta>
            <ta e="T217" id="Seg_10045" s="T216">n</ta>
            <ta e="T218" id="Seg_10046" s="T217">v</ta>
            <ta e="T219" id="Seg_10047" s="T218">v</ta>
            <ta e="T220" id="Seg_10048" s="T219">conj</ta>
            <ta e="T221" id="Seg_10049" s="T220">v</ta>
            <ta e="T222" id="Seg_10050" s="T221">adv</ta>
            <ta e="T223" id="Seg_10051" s="T222">adj</ta>
            <ta e="T224" id="Seg_10052" s="T223">adj</ta>
            <ta e="T225" id="Seg_10053" s="T224">v</ta>
            <ta e="T226" id="Seg_10054" s="T225">n</ta>
            <ta e="T227" id="Seg_10055" s="T226">cop</ta>
            <ta e="T228" id="Seg_10056" s="T227">n</ta>
            <ta e="T229" id="Seg_10057" s="T228">n</ta>
            <ta e="T230" id="Seg_10058" s="T229">v</ta>
            <ta e="T231" id="Seg_10059" s="T230">interj</ta>
            <ta e="T232" id="Seg_10060" s="T231">ptcl</ta>
            <ta e="T233" id="Seg_10061" s="T232">adj</ta>
            <ta e="T234" id="Seg_10062" s="T233">n</ta>
            <ta e="T235" id="Seg_10063" s="T234">aux</ta>
            <ta e="T236" id="Seg_10064" s="T235">que</ta>
            <ta e="T237" id="Seg_10065" s="T236">v</ta>
            <ta e="T238" id="Seg_10066" s="T237">aux</ta>
            <ta e="T239" id="Seg_10067" s="T238">adv</ta>
            <ta e="T240" id="Seg_10068" s="T239">v</ta>
            <ta e="T241" id="Seg_10069" s="T240">adj</ta>
            <ta e="T242" id="Seg_10070" s="T241">n</ta>
            <ta e="T243" id="Seg_10071" s="T242">adj</ta>
            <ta e="T244" id="Seg_10072" s="T243">adj</ta>
            <ta e="T245" id="Seg_10073" s="T244">que</ta>
            <ta e="T246" id="Seg_10074" s="T245">ptcl</ta>
            <ta e="T247" id="Seg_10075" s="T246">adv</ta>
            <ta e="T248" id="Seg_10076" s="T247">v</ta>
            <ta e="T249" id="Seg_10077" s="T248">adj</ta>
            <ta e="T250" id="Seg_10078" s="T249">n</ta>
            <ta e="T251" id="Seg_10079" s="T250">v</ta>
            <ta e="T252" id="Seg_10080" s="T251">n</ta>
            <ta e="T253" id="Seg_10081" s="T252">que</ta>
            <ta e="T254" id="Seg_10082" s="T253">ptcl</ta>
            <ta e="T255" id="Seg_10083" s="T254">v</ta>
            <ta e="T256" id="Seg_10084" s="T255">v</ta>
            <ta e="T257" id="Seg_10085" s="T256">v</ta>
            <ta e="T258" id="Seg_10086" s="T257">v</ta>
            <ta e="T259" id="Seg_10087" s="T258">n</ta>
            <ta e="T260" id="Seg_10088" s="T259">v</ta>
            <ta e="T261" id="Seg_10089" s="T260">n</ta>
            <ta e="T262" id="Seg_10090" s="T261">adv</ta>
            <ta e="T263" id="Seg_10091" s="T262">v</ta>
            <ta e="T264" id="Seg_10092" s="T263">que</ta>
            <ta e="T265" id="Seg_10093" s="T264">n</ta>
            <ta e="T266" id="Seg_10094" s="T265">ptcl</ta>
            <ta e="T267" id="Seg_10095" s="T266">v</ta>
            <ta e="T268" id="Seg_10096" s="T267">v</ta>
            <ta e="T269" id="Seg_10097" s="T268">adv</ta>
            <ta e="T270" id="Seg_10098" s="T269">v</ta>
            <ta e="T271" id="Seg_10099" s="T270">adj</ta>
            <ta e="T272" id="Seg_10100" s="T271">adj</ta>
            <ta e="T273" id="Seg_10101" s="T272">v</ta>
            <ta e="T274" id="Seg_10102" s="T273">adj</ta>
            <ta e="T275" id="Seg_10103" s="T274">adj</ta>
            <ta e="T276" id="Seg_10104" s="T275">n</ta>
            <ta e="T277" id="Seg_10105" s="T276">n</ta>
            <ta e="T278" id="Seg_10106" s="T277">v</ta>
            <ta e="T279" id="Seg_10107" s="T278">v</ta>
            <ta e="T280" id="Seg_10108" s="T279">n</ta>
            <ta e="T281" id="Seg_10109" s="T280">v</ta>
            <ta e="T282" id="Seg_10110" s="T281">v</ta>
            <ta e="T283" id="Seg_10111" s="T282">v</ta>
            <ta e="T284" id="Seg_10112" s="T283">v</ta>
            <ta e="T285" id="Seg_10113" s="T284">n</ta>
            <ta e="T286" id="Seg_10114" s="T285">post</ta>
            <ta e="T287" id="Seg_10115" s="T286">v</ta>
            <ta e="T288" id="Seg_10116" s="T287">n</ta>
            <ta e="T289" id="Seg_10117" s="T288">n</ta>
            <ta e="T290" id="Seg_10118" s="T289">n</ta>
            <ta e="T291" id="Seg_10119" s="T290">adv</ta>
            <ta e="T292" id="Seg_10120" s="T291">v</ta>
            <ta e="T293" id="Seg_10121" s="T292">n</ta>
            <ta e="T294" id="Seg_10122" s="T293">post</ta>
            <ta e="T295" id="Seg_10123" s="T294">v</ta>
            <ta e="T296" id="Seg_10124" s="T295">n</ta>
            <ta e="T297" id="Seg_10125" s="T296">n</ta>
            <ta e="T298" id="Seg_10126" s="T297">adv</ta>
            <ta e="T299" id="Seg_10127" s="T298">v</ta>
            <ta e="T300" id="Seg_10128" s="T299">n</ta>
            <ta e="T301" id="Seg_10129" s="T300">v</ta>
            <ta e="T302" id="Seg_10130" s="T301">que</ta>
            <ta e="T303" id="Seg_10131" s="T302">ptcl</ta>
            <ta e="T304" id="Seg_10132" s="T303">v</ta>
            <ta e="T305" id="Seg_10133" s="T304">adv</ta>
            <ta e="T306" id="Seg_10134" s="T305">adj</ta>
            <ta e="T307" id="Seg_10135" s="T306">adj</ta>
            <ta e="T308" id="Seg_10136" s="T307">n</ta>
            <ta e="T309" id="Seg_10137" s="T308">n</ta>
            <ta e="T310" id="Seg_10138" s="T309">v</ta>
            <ta e="T311" id="Seg_10139" s="T310">dempro</ta>
            <ta e="T312" id="Seg_10140" s="T311">n</ta>
            <ta e="T313" id="Seg_10141" s="T312">pers</ta>
            <ta e="T314" id="Seg_10142" s="T313">v</ta>
            <ta e="T315" id="Seg_10143" s="T314">aux</ta>
            <ta e="T316" id="Seg_10144" s="T315">ptcl</ta>
            <ta e="T317" id="Seg_10145" s="T316">v</ta>
            <ta e="T318" id="Seg_10146" s="T317">n</ta>
            <ta e="T319" id="Seg_10147" s="T318">n</ta>
            <ta e="T320" id="Seg_10148" s="T319">v</ta>
            <ta e="T321" id="Seg_10149" s="T320">aux</ta>
            <ta e="T322" id="Seg_10150" s="T321">n</ta>
            <ta e="T323" id="Seg_10151" s="T322">v</ta>
            <ta e="T324" id="Seg_10152" s="T323">n</ta>
            <ta e="T325" id="Seg_10153" s="T324">v</ta>
            <ta e="T326" id="Seg_10154" s="T325">n</ta>
            <ta e="T327" id="Seg_10155" s="T326">n</ta>
            <ta e="T328" id="Seg_10156" s="T327">v</ta>
            <ta e="T329" id="Seg_10157" s="T328">interj</ta>
            <ta e="T330" id="Seg_10158" s="T329">v</ta>
            <ta e="T331" id="Seg_10159" s="T330">ptcl</ta>
            <ta e="T332" id="Seg_10160" s="T331">interj</ta>
            <ta e="T333" id="Seg_10161" s="T332">v</ta>
            <ta e="T334" id="Seg_10162" s="T333">post</ta>
            <ta e="T335" id="Seg_10163" s="T334">v</ta>
            <ta e="T336" id="Seg_10164" s="T335">adv</ta>
            <ta e="T337" id="Seg_10165" s="T336">ptcl</ta>
            <ta e="T338" id="Seg_10166" s="T337">n</ta>
            <ta e="T339" id="Seg_10167" s="T338">ptcl</ta>
            <ta e="T340" id="Seg_10168" s="T339">v</ta>
            <ta e="T341" id="Seg_10169" s="T340">adj</ta>
            <ta e="T342" id="Seg_10170" s="T341">adj</ta>
            <ta e="T343" id="Seg_10171" s="T342">n</ta>
            <ta e="T344" id="Seg_10172" s="T343">n</ta>
            <ta e="T345" id="Seg_10173" s="T344">v</ta>
            <ta e="T346" id="Seg_10174" s="T345">v</ta>
            <ta e="T347" id="Seg_10175" s="T346">n</ta>
            <ta e="T348" id="Seg_10176" s="T347">v</ta>
            <ta e="T349" id="Seg_10177" s="T348">v</ta>
            <ta e="T350" id="Seg_10178" s="T349">v</ta>
            <ta e="T351" id="Seg_10179" s="T350">v</ta>
            <ta e="T352" id="Seg_10180" s="T351">n</ta>
            <ta e="T353" id="Seg_10181" s="T352">post</ta>
            <ta e="T354" id="Seg_10182" s="T353">v</ta>
            <ta e="T355" id="Seg_10183" s="T354">n</ta>
            <ta e="T356" id="Seg_10184" s="T355">n</ta>
            <ta e="T357" id="Seg_10185" s="T356">n</ta>
            <ta e="T358" id="Seg_10186" s="T357">adv</ta>
            <ta e="T359" id="Seg_10187" s="T358">v</ta>
            <ta e="T360" id="Seg_10188" s="T359">n</ta>
            <ta e="T361" id="Seg_10189" s="T360">post</ta>
            <ta e="T362" id="Seg_10190" s="T361">v</ta>
            <ta e="T363" id="Seg_10191" s="T362">n</ta>
            <ta e="T364" id="Seg_10192" s="T363">n</ta>
            <ta e="T365" id="Seg_10193" s="T364">adv</ta>
            <ta e="T366" id="Seg_10194" s="T365">v</ta>
            <ta e="T367" id="Seg_10195" s="T366">n</ta>
            <ta e="T368" id="Seg_10196" s="T367">v</ta>
            <ta e="T369" id="Seg_10197" s="T368">que</ta>
            <ta e="T370" id="Seg_10198" s="T369">ptcl</ta>
            <ta e="T371" id="Seg_10199" s="T370">v</ta>
            <ta e="T372" id="Seg_10200" s="T371">adv</ta>
            <ta e="T373" id="Seg_10201" s="T372">adj</ta>
            <ta e="T374" id="Seg_10202" s="T373">adj</ta>
            <ta e="T375" id="Seg_10203" s="T374">n</ta>
            <ta e="T376" id="Seg_10204" s="T375">n</ta>
            <ta e="T377" id="Seg_10205" s="T376">cop</ta>
            <ta e="T378" id="Seg_10206" s="T377">v</ta>
            <ta e="T379" id="Seg_10207" s="T378">n</ta>
            <ta e="T380" id="Seg_10208" s="T379">n</ta>
            <ta e="T381" id="Seg_10209" s="T380">v</ta>
            <ta e="T382" id="Seg_10210" s="T381">interj</ta>
            <ta e="T383" id="Seg_10211" s="T382">n</ta>
            <ta e="T384" id="Seg_10212" s="T383">ptcl</ta>
            <ta e="T385" id="Seg_10213" s="T384">adj</ta>
            <ta e="T386" id="Seg_10214" s="T385">n</ta>
            <ta e="T387" id="Seg_10215" s="T386">aux</ta>
            <ta e="T388" id="Seg_10216" s="T387">ptcl</ta>
            <ta e="T389" id="Seg_10217" s="T388">que</ta>
            <ta e="T390" id="Seg_10218" s="T389">conj</ta>
            <ta e="T391" id="Seg_10219" s="T390">cardnum</ta>
            <ta e="T392" id="Seg_10220" s="T391">adj</ta>
            <ta e="T393" id="Seg_10221" s="T392">v</ta>
            <ta e="T394" id="Seg_10222" s="T393">n</ta>
            <ta e="T395" id="Seg_10223" s="T394">n</ta>
            <ta e="T396" id="Seg_10224" s="T395">v</ta>
            <ta e="T397" id="Seg_10225" s="T396">v</ta>
            <ta e="T398" id="Seg_10226" s="T397">dempro</ta>
            <ta e="T399" id="Seg_10227" s="T398">adv</ta>
            <ta e="T400" id="Seg_10228" s="T399">v</ta>
            <ta e="T401" id="Seg_10229" s="T400">n</ta>
            <ta e="T402" id="Seg_10230" s="T401">v</ta>
            <ta e="T403" id="Seg_10231" s="T402">v</ta>
            <ta e="T404" id="Seg_10232" s="T403">que</ta>
            <ta e="T405" id="Seg_10233" s="T404">n</ta>
            <ta e="T406" id="Seg_10234" s="T405">v</ta>
            <ta e="T407" id="Seg_10235" s="T406">v</ta>
            <ta e="T408" id="Seg_10236" s="T407">v</ta>
            <ta e="T409" id="Seg_10237" s="T408">n</ta>
            <ta e="T410" id="Seg_10238" s="T409">n</ta>
            <ta e="T411" id="Seg_10239" s="T410">n</ta>
            <ta e="T412" id="Seg_10240" s="T411">adj</ta>
            <ta e="T413" id="Seg_10241" s="T412">n</ta>
            <ta e="T414" id="Seg_10242" s="T413">n</ta>
            <ta e="T415" id="Seg_10243" s="T414">v</ta>
            <ta e="T416" id="Seg_10244" s="T415">v</ta>
            <ta e="T417" id="Seg_10245" s="T416">v</ta>
            <ta e="T418" id="Seg_10246" s="T417">v</ta>
            <ta e="T419" id="Seg_10247" s="T418">n</ta>
            <ta e="T420" id="Seg_10248" s="T419">v</ta>
            <ta e="T421" id="Seg_10249" s="T420">interj</ta>
            <ta e="T422" id="Seg_10250" s="T421">n</ta>
            <ta e="T423" id="Seg_10251" s="T422">que</ta>
            <ta e="T424" id="Seg_10252" s="T423">v</ta>
            <ta e="T425" id="Seg_10253" s="T424">interj</ta>
            <ta e="T426" id="Seg_10254" s="T425">n</ta>
            <ta e="T427" id="Seg_10255" s="T426">n</ta>
            <ta e="T428" id="Seg_10256" s="T427">v</ta>
            <ta e="T429" id="Seg_10257" s="T428">v</ta>
            <ta e="T430" id="Seg_10258" s="T429">aux</ta>
            <ta e="T431" id="Seg_10259" s="T430">v</ta>
            <ta e="T432" id="Seg_10260" s="T431">n</ta>
            <ta e="T433" id="Seg_10261" s="T432">n</ta>
            <ta e="T434" id="Seg_10262" s="T433">que</ta>
            <ta e="T435" id="Seg_10263" s="T434">v</ta>
            <ta e="T436" id="Seg_10264" s="T435">n</ta>
            <ta e="T437" id="Seg_10265" s="T436">ptcl</ta>
            <ta e="T438" id="Seg_10266" s="T437">n</ta>
            <ta e="T439" id="Seg_10267" s="T438">n</ta>
            <ta e="T440" id="Seg_10268" s="T439">que</ta>
            <ta e="T441" id="Seg_10269" s="T440">v</ta>
            <ta e="T442" id="Seg_10270" s="T441">adv</ta>
            <ta e="T443" id="Seg_10271" s="T442">v</ta>
            <ta e="T444" id="Seg_10272" s="T443">v</ta>
            <ta e="T445" id="Seg_10273" s="T444">v</ta>
            <ta e="T446" id="Seg_10274" s="T445">n</ta>
            <ta e="T447" id="Seg_10275" s="T446">v</ta>
            <ta e="T448" id="Seg_10276" s="T447">v</ta>
            <ta e="T449" id="Seg_10277" s="T448">n</ta>
            <ta e="T450" id="Seg_10278" s="T449">n</ta>
            <ta e="T451" id="Seg_10279" s="T450">n</ta>
            <ta e="T452" id="Seg_10280" s="T451">n</ta>
            <ta e="T453" id="Seg_10281" s="T452">v</ta>
            <ta e="T454" id="Seg_10282" s="T453">aux</ta>
            <ta e="T455" id="Seg_10283" s="T454">ptcl</ta>
            <ta e="T456" id="Seg_10284" s="T455">dempro</ta>
            <ta e="T457" id="Seg_10285" s="T456">v</ta>
            <ta e="T458" id="Seg_10286" s="T457">n</ta>
            <ta e="T459" id="Seg_10287" s="T458">n</ta>
            <ta e="T460" id="Seg_10288" s="T459">n</ta>
            <ta e="T461" id="Seg_10289" s="T460">n</ta>
            <ta e="T462" id="Seg_10290" s="T461">n</ta>
            <ta e="T463" id="Seg_10291" s="T462">v</ta>
            <ta e="T464" id="Seg_10292" s="T463">v</ta>
            <ta e="T465" id="Seg_10293" s="T464">n</ta>
            <ta e="T466" id="Seg_10294" s="T465">ptcl</ta>
            <ta e="T467" id="Seg_10295" s="T466">dempro</ta>
            <ta e="T468" id="Seg_10296" s="T467">adj</ta>
            <ta e="T469" id="Seg_10297" s="T468">n</ta>
            <ta e="T470" id="Seg_10298" s="T469">n</ta>
            <ta e="T471" id="Seg_10299" s="T470">n</ta>
            <ta e="T472" id="Seg_10300" s="T471">n</ta>
            <ta e="T473" id="Seg_10301" s="T472">v</ta>
            <ta e="T474" id="Seg_10302" s="T473">post</ta>
            <ta e="T475" id="Seg_10303" s="T474">n</ta>
            <ta e="T476" id="Seg_10304" s="T475">n</ta>
            <ta e="T477" id="Seg_10305" s="T476">n</ta>
            <ta e="T478" id="Seg_10306" s="T477">n</ta>
            <ta e="T479" id="Seg_10307" s="T478">post</ta>
            <ta e="T480" id="Seg_10308" s="T479">v</ta>
            <ta e="T481" id="Seg_10309" s="T480">aux</ta>
            <ta e="T482" id="Seg_10310" s="T481">n</ta>
            <ta e="T483" id="Seg_10311" s="T482">n</ta>
            <ta e="T484" id="Seg_10312" s="T483">v</ta>
            <ta e="T485" id="Seg_10313" s="T484">ptcl</ta>
            <ta e="T486" id="Seg_10314" s="T485">n</ta>
            <ta e="T487" id="Seg_10315" s="T486">dempro</ta>
            <ta e="T488" id="Seg_10316" s="T487">v</ta>
            <ta e="T489" id="Seg_10317" s="T488">v</ta>
            <ta e="T490" id="Seg_10318" s="T489">dempro</ta>
            <ta e="T491" id="Seg_10319" s="T490">n</ta>
            <ta e="T492" id="Seg_10320" s="T491">conj</ta>
            <ta e="T493" id="Seg_10321" s="T492">pers</ta>
            <ta e="T494" id="Seg_10322" s="T493">v</ta>
            <ta e="T495" id="Seg_10323" s="T494">aux</ta>
            <ta e="T496" id="Seg_10324" s="T495">ptcl</ta>
            <ta e="T497" id="Seg_10325" s="T496">ptcl</ta>
            <ta e="T498" id="Seg_10326" s="T497">adv</ta>
            <ta e="T499" id="Seg_10327" s="T498">v</ta>
            <ta e="T500" id="Seg_10328" s="T499">v</ta>
            <ta e="T501" id="Seg_10329" s="T500">emphpro</ta>
            <ta e="T502" id="Seg_10330" s="T501">n</ta>
            <ta e="T503" id="Seg_10331" s="T502">v</ta>
            <ta e="T504" id="Seg_10332" s="T503">adv</ta>
            <ta e="T505" id="Seg_10333" s="T504">adv</ta>
            <ta e="T506" id="Seg_10334" s="T505">adj</ta>
            <ta e="T507" id="Seg_10335" s="T506">cop</ta>
            <ta e="T508" id="Seg_10336" s="T507">pers</ta>
            <ta e="T509" id="Seg_10337" s="T508">n</ta>
            <ta e="T510" id="Seg_10338" s="T509">v</ta>
            <ta e="T511" id="Seg_10339" s="T510">v</ta>
            <ta e="T512" id="Seg_10340" s="T511">aux</ta>
            <ta e="T513" id="Seg_10341" s="T512">ptcl</ta>
            <ta e="T514" id="Seg_10342" s="T513">aux</ta>
            <ta e="T515" id="Seg_10343" s="T514">que</ta>
            <ta e="T516" id="Seg_10344" s="T515">v</ta>
            <ta e="T517" id="Seg_10345" s="T516">v</ta>
            <ta e="T518" id="Seg_10346" s="T517">adv</ta>
            <ta e="T519" id="Seg_10347" s="T518">v</ta>
            <ta e="T520" id="Seg_10348" s="T519">v</ta>
            <ta e="T521" id="Seg_10349" s="T520">n</ta>
            <ta e="T522" id="Seg_10350" s="T521">adj</ta>
            <ta e="T523" id="Seg_10351" s="T522">n</ta>
            <ta e="T524" id="Seg_10352" s="T523">adj</ta>
            <ta e="T525" id="Seg_10353" s="T524">dempro</ta>
            <ta e="T526" id="Seg_10354" s="T525">v</ta>
            <ta e="T527" id="Seg_10355" s="T526">v</ta>
            <ta e="T528" id="Seg_10356" s="T527">n</ta>
            <ta e="T529" id="Seg_10357" s="T528">adv</ta>
            <ta e="T530" id="Seg_10358" s="T529">v</ta>
            <ta e="T531" id="Seg_10359" s="T530">n</ta>
            <ta e="T532" id="Seg_10360" s="T531">n</ta>
            <ta e="T533" id="Seg_10361" s="T532">v</ta>
            <ta e="T534" id="Seg_10362" s="T533">interj</ta>
            <ta e="T535" id="Seg_10363" s="T534">v</ta>
            <ta e="T536" id="Seg_10364" s="T535">ptcl</ta>
            <ta e="T537" id="Seg_10365" s="T536">n</ta>
            <ta e="T538" id="Seg_10366" s="T537">n</ta>
            <ta e="T539" id="Seg_10367" s="T538">n</ta>
            <ta e="T540" id="Seg_10368" s="T539">n</ta>
            <ta e="T541" id="Seg_10369" s="T540">n</ta>
            <ta e="T542" id="Seg_10370" s="T541">n</ta>
            <ta e="T543" id="Seg_10371" s="T542">n</ta>
            <ta e="T544" id="Seg_10372" s="T543">post</ta>
            <ta e="T545" id="Seg_10373" s="T544">v</ta>
            <ta e="T546" id="Seg_10374" s="T545">aux</ta>
            <ta e="T547" id="Seg_10375" s="T546">n</ta>
            <ta e="T548" id="Seg_10376" s="T547">n</ta>
            <ta e="T549" id="Seg_10377" s="T548">v</ta>
            <ta e="T550" id="Seg_10378" s="T549">ptcl</ta>
            <ta e="T551" id="Seg_10379" s="T550">n</ta>
            <ta e="T552" id="Seg_10380" s="T551">dempro</ta>
            <ta e="T553" id="Seg_10381" s="T552">v</ta>
            <ta e="T554" id="Seg_10382" s="T553">v</ta>
            <ta e="T555" id="Seg_10383" s="T554">n</ta>
            <ta e="T556" id="Seg_10384" s="T555">emphpro</ta>
            <ta e="T557" id="Seg_10385" s="T556">n</ta>
            <ta e="T558" id="Seg_10386" s="T557">v</ta>
            <ta e="T559" id="Seg_10387" s="T558">adv</ta>
            <ta e="T560" id="Seg_10388" s="T559">adv</ta>
            <ta e="T561" id="Seg_10389" s="T560">adj</ta>
            <ta e="T562" id="Seg_10390" s="T561">cop</ta>
            <ta e="T563" id="Seg_10391" s="T562">pers</ta>
            <ta e="T564" id="Seg_10392" s="T563">n</ta>
            <ta e="T565" id="Seg_10393" s="T564">v</ta>
            <ta e="T566" id="Seg_10394" s="T565">v</ta>
            <ta e="T567" id="Seg_10395" s="T566">aux</ta>
            <ta e="T568" id="Seg_10396" s="T567">ptcl</ta>
            <ta e="T569" id="Seg_10397" s="T568">aux</ta>
            <ta e="T570" id="Seg_10398" s="T569">que</ta>
            <ta e="T571" id="Seg_10399" s="T570">v</ta>
            <ta e="T572" id="Seg_10400" s="T571">v</ta>
            <ta e="T573" id="Seg_10401" s="T572">adj</ta>
            <ta e="T574" id="Seg_10402" s="T573">n</ta>
            <ta e="T575" id="Seg_10403" s="T574">adj</ta>
            <ta e="T576" id="Seg_10404" s="T575">dempro</ta>
            <ta e="T577" id="Seg_10405" s="T576">v</ta>
            <ta e="T578" id="Seg_10406" s="T577">v</ta>
            <ta e="T579" id="Seg_10407" s="T578">n</ta>
            <ta e="T580" id="Seg_10408" s="T579">n</ta>
            <ta e="T581" id="Seg_10409" s="T580">n</ta>
            <ta e="T582" id="Seg_10410" s="T581">n</ta>
            <ta e="T583" id="Seg_10411" s="T582">adj</ta>
            <ta e="T584" id="Seg_10412" s="T583">n</ta>
            <ta e="T585" id="Seg_10413" s="T584">v</ta>
            <ta e="T586" id="Seg_10414" s="T585">v</ta>
            <ta e="T587" id="Seg_10415" s="T586">adj</ta>
            <ta e="T588" id="Seg_10416" s="T587">n</ta>
            <ta e="T589" id="Seg_10417" s="T588">n</ta>
            <ta e="T590" id="Seg_10418" s="T589">adj</ta>
            <ta e="T591" id="Seg_10419" s="T590">v</ta>
            <ta e="T592" id="Seg_10420" s="T591">dempro</ta>
            <ta e="T593" id="Seg_10421" s="T592">v</ta>
            <ta e="T594" id="Seg_10422" s="T593">aux</ta>
            <ta e="T595" id="Seg_10423" s="T594">v</ta>
            <ta e="T596" id="Seg_10424" s="T595">dempro</ta>
            <ta e="T597" id="Seg_10425" s="T596">v</ta>
            <ta e="T598" id="Seg_10426" s="T597">collnum</ta>
            <ta e="T599" id="Seg_10427" s="T598">n</ta>
            <ta e="T600" id="Seg_10428" s="T599">v</ta>
            <ta e="T601" id="Seg_10429" s="T600">v</ta>
            <ta e="T602" id="Seg_10430" s="T601">n</ta>
            <ta e="T603" id="Seg_10431" s="T602">n</ta>
            <ta e="T604" id="Seg_10432" s="T603">v</ta>
            <ta e="T605" id="Seg_10433" s="T604">aux</ta>
            <ta e="T606" id="Seg_10434" s="T605">v</ta>
            <ta e="T607" id="Seg_10435" s="T606">n</ta>
            <ta e="T608" id="Seg_10436" s="T607">interj</ta>
            <ta e="T609" id="Seg_10437" s="T608">n</ta>
            <ta e="T610" id="Seg_10438" s="T609">que</ta>
            <ta e="T611" id="Seg_10439" s="T610">n</ta>
            <ta e="T612" id="Seg_10440" s="T611">v</ta>
            <ta e="T613" id="Seg_10441" s="T612">interj</ta>
            <ta e="T614" id="Seg_10442" s="T613">ptcl</ta>
            <ta e="T615" id="Seg_10443" s="T614">v</ta>
            <ta e="T616" id="Seg_10444" s="T615">n</ta>
            <ta e="T617" id="Seg_10445" s="T616">v</ta>
            <ta e="T618" id="Seg_10446" s="T617">dempro</ta>
            <ta e="T619" id="Seg_10447" s="T618">n</ta>
            <ta e="T620" id="Seg_10448" s="T619">v</ta>
            <ta e="T621" id="Seg_10449" s="T620">aux</ta>
            <ta e="T622" id="Seg_10450" s="T621">v</ta>
            <ta e="T623" id="Seg_10451" s="T622">dempro</ta>
            <ta e="T624" id="Seg_10452" s="T623">v</ta>
            <ta e="T625" id="Seg_10453" s="T624">collnum</ta>
            <ta e="T626" id="Seg_10454" s="T625">n</ta>
            <ta e="T627" id="Seg_10455" s="T626">v</ta>
            <ta e="T628" id="Seg_10456" s="T627">v</ta>
            <ta e="T629" id="Seg_10457" s="T628">n</ta>
            <ta e="T630" id="Seg_10458" s="T629">v</ta>
            <ta e="T631" id="Seg_10459" s="T630">v</ta>
            <ta e="T632" id="Seg_10460" s="T631">adv</ta>
            <ta e="T633" id="Seg_10461" s="T632">n</ta>
            <ta e="T634" id="Seg_10462" s="T633">n</ta>
            <ta e="T635" id="Seg_10463" s="T634">v</ta>
            <ta e="T636" id="Seg_10464" s="T635">n</ta>
            <ta e="T637" id="Seg_10465" s="T636">n</ta>
            <ta e="T638" id="Seg_10466" s="T637">n</ta>
            <ta e="T639" id="Seg_10467" s="T638">n</ta>
            <ta e="T640" id="Seg_10468" s="T639">post</ta>
            <ta e="T641" id="Seg_10469" s="T640">v</ta>
            <ta e="T642" id="Seg_10470" s="T641">aux</ta>
            <ta e="T643" id="Seg_10471" s="T642">n</ta>
            <ta e="T644" id="Seg_10472" s="T643">v</ta>
            <ta e="T645" id="Seg_10473" s="T644">v</ta>
            <ta e="T646" id="Seg_10474" s="T645">n</ta>
            <ta e="T647" id="Seg_10475" s="T646">n</ta>
            <ta e="T648" id="Seg_10476" s="T647">v</ta>
            <ta e="T649" id="Seg_10477" s="T648">n</ta>
            <ta e="T650" id="Seg_10478" s="T649">v</ta>
            <ta e="T651" id="Seg_10479" s="T650">emphpro</ta>
            <ta e="T652" id="Seg_10480" s="T651">n</ta>
            <ta e="T653" id="Seg_10481" s="T652">v</ta>
            <ta e="T654" id="Seg_10482" s="T653">v</ta>
            <ta e="T655" id="Seg_10483" s="T654">v</ta>
            <ta e="T656" id="Seg_10484" s="T655">n</ta>
            <ta e="T657" id="Seg_10485" s="T656">n</ta>
            <ta e="T658" id="Seg_10486" s="T657">n</ta>
            <ta e="T659" id="Seg_10487" s="T658">v</ta>
            <ta e="T660" id="Seg_10488" s="T659">v</ta>
            <ta e="T661" id="Seg_10489" s="T660">n</ta>
            <ta e="T662" id="Seg_10490" s="T661">v</ta>
            <ta e="T663" id="Seg_10491" s="T662">que</ta>
            <ta e="T664" id="Seg_10492" s="T663">v</ta>
            <ta e="T665" id="Seg_10493" s="T664">aux</ta>
            <ta e="T666" id="Seg_10494" s="T665">v</ta>
            <ta e="T667" id="Seg_10495" s="T666">n</ta>
            <ta e="T668" id="Seg_10496" s="T667">n</ta>
            <ta e="T669" id="Seg_10497" s="T668">n</ta>
            <ta e="T670" id="Seg_10498" s="T669">post</ta>
            <ta e="T671" id="Seg_10499" s="T670">adv</ta>
            <ta e="T672" id="Seg_10500" s="T671">cop</ta>
            <ta e="T673" id="Seg_10501" s="T672">adj</ta>
            <ta e="T674" id="Seg_10502" s="T673">n</ta>
            <ta e="T675" id="Seg_10503" s="T674">v</ta>
            <ta e="T676" id="Seg_10504" s="T675">v</ta>
            <ta e="T677" id="Seg_10505" s="T676">n</ta>
            <ta e="T678" id="Seg_10506" s="T677">n</ta>
            <ta e="T679" id="Seg_10507" s="T678">v</ta>
            <ta e="T680" id="Seg_10508" s="T679">n</ta>
            <ta e="T681" id="Seg_10509" s="T680">dempro</ta>
            <ta e="T682" id="Seg_10510" s="T681">cardnum</ta>
            <ta e="T683" id="Seg_10511" s="T682">n</ta>
            <ta e="T684" id="Seg_10512" s="T683">cardnum</ta>
            <ta e="T685" id="Seg_10513" s="T684">n</ta>
            <ta e="T686" id="Seg_10514" s="T685">v</ta>
            <ta e="T687" id="Seg_10515" s="T686">v</ta>
            <ta e="T688" id="Seg_10516" s="T687">pers</ta>
            <ta e="T689" id="Seg_10517" s="T688">v</ta>
            <ta e="T690" id="Seg_10518" s="T689">n</ta>
            <ta e="T691" id="Seg_10519" s="T690">ptcl</ta>
            <ta e="T692" id="Seg_10520" s="T691">v</ta>
            <ta e="T693" id="Seg_10521" s="T692">ptcl</ta>
            <ta e="T694" id="Seg_10522" s="T693">v</ta>
            <ta e="T695" id="Seg_10523" s="T694">n</ta>
            <ta e="T696" id="Seg_10524" s="T695">n</ta>
            <ta e="T697" id="Seg_10525" s="T696">que</ta>
            <ta e="T698" id="Seg_10526" s="T697">adj</ta>
            <ta e="T699" id="Seg_10527" s="T698">n</ta>
            <ta e="T700" id="Seg_10528" s="T699">adj</ta>
            <ta e="T701" id="Seg_10529" s="T700">v</ta>
            <ta e="T702" id="Seg_10530" s="T701">v</ta>
            <ta e="T703" id="Seg_10531" s="T702">n</ta>
            <ta e="T704" id="Seg_10532" s="T703">v</ta>
            <ta e="T705" id="Seg_10533" s="T704">v</ta>
            <ta e="T706" id="Seg_10534" s="T705">n</ta>
            <ta e="T707" id="Seg_10535" s="T706">cop</ta>
            <ta e="T708" id="Seg_10536" s="T707">n</ta>
            <ta e="T709" id="Seg_10537" s="T708">v</ta>
            <ta e="T710" id="Seg_10538" s="T709">aux</ta>
            <ta e="T711" id="Seg_10539" s="T710">n</ta>
            <ta e="T712" id="Seg_10540" s="T711">n</ta>
            <ta e="T713" id="Seg_10541" s="T712">n</ta>
            <ta e="T714" id="Seg_10542" s="T713">n</ta>
            <ta e="T715" id="Seg_10543" s="T714">n</ta>
            <ta e="T716" id="Seg_10544" s="T715">n</ta>
            <ta e="T717" id="Seg_10545" s="T716">n</ta>
            <ta e="T718" id="Seg_10546" s="T717">n</ta>
            <ta e="T719" id="Seg_10547" s="T718">v</ta>
            <ta e="T720" id="Seg_10548" s="T719">v</ta>
            <ta e="T721" id="Seg_10549" s="T720">n</ta>
            <ta e="T722" id="Seg_10550" s="T721">n</ta>
            <ta e="T723" id="Seg_10551" s="T722">adv</ta>
            <ta e="T724" id="Seg_10552" s="T723">v</ta>
            <ta e="T725" id="Seg_10553" s="T724">v</ta>
            <ta e="T726" id="Seg_10554" s="T725">v</ta>
            <ta e="T727" id="Seg_10555" s="T726">v</ta>
            <ta e="T728" id="Seg_10556" s="T727">n</ta>
            <ta e="T729" id="Seg_10557" s="T728">n</ta>
            <ta e="T730" id="Seg_10558" s="T729">n</ta>
            <ta e="T731" id="Seg_10559" s="T730">v</ta>
            <ta e="T732" id="Seg_10560" s="T731">interj</ta>
            <ta e="T733" id="Seg_10561" s="T732">dempro</ta>
            <ta e="T734" id="Seg_10562" s="T733">que</ta>
            <ta e="T735" id="Seg_10563" s="T734">ptcl</ta>
            <ta e="T736" id="Seg_10564" s="T735">que</ta>
            <ta e="T737" id="Seg_10565" s="T736">v</ta>
            <ta e="T738" id="Seg_10566" s="T737">n</ta>
            <ta e="T739" id="Seg_10567" s="T738">v</ta>
            <ta e="T740" id="Seg_10568" s="T739">v</ta>
            <ta e="T741" id="Seg_10569" s="T740">n</ta>
            <ta e="T742" id="Seg_10570" s="T741">interj</ta>
            <ta e="T743" id="Seg_10571" s="T742">n</ta>
            <ta e="T744" id="Seg_10572" s="T743">pers</ta>
            <ta e="T745" id="Seg_10573" s="T744">v</ta>
            <ta e="T746" id="Seg_10574" s="T745">v</ta>
            <ta e="T747" id="Seg_10575" s="T746">que</ta>
            <ta e="T748" id="Seg_10576" s="T747">v</ta>
            <ta e="T749" id="Seg_10577" s="T748">interj</ta>
            <ta e="T750" id="Seg_10578" s="T749">emphpro</ta>
            <ta e="T751" id="Seg_10579" s="T750">v</ta>
            <ta e="T752" id="Seg_10580" s="T751">conj</ta>
            <ta e="T753" id="Seg_10581" s="T752">emphpro</ta>
            <ta e="T754" id="Seg_10582" s="T753">v</ta>
            <ta e="T755" id="Seg_10583" s="T754">que</ta>
            <ta e="T756" id="Seg_10584" s="T755">v</ta>
            <ta e="T757" id="Seg_10585" s="T756">v</ta>
            <ta e="T758" id="Seg_10586" s="T757">v</ta>
            <ta e="T759" id="Seg_10587" s="T758">ptcl</ta>
            <ta e="T760" id="Seg_10588" s="T759">v</ta>
            <ta e="T761" id="Seg_10589" s="T760">n</ta>
            <ta e="T762" id="Seg_10590" s="T761">n</ta>
            <ta e="T763" id="Seg_10591" s="T762">n</ta>
            <ta e="T764" id="Seg_10592" s="T763">v</ta>
            <ta e="T765" id="Seg_10593" s="T764">v</ta>
            <ta e="T766" id="Seg_10594" s="T765">ptcl</ta>
            <ta e="T767" id="Seg_10595" s="T766">n</ta>
            <ta e="T768" id="Seg_10596" s="T767">v</ta>
            <ta e="T769" id="Seg_10597" s="T768">adv</ta>
            <ta e="T770" id="Seg_10598" s="T769">adj</ta>
            <ta e="T771" id="Seg_10599" s="T770">n</ta>
            <ta e="T772" id="Seg_10600" s="T771">cop</ta>
            <ta e="T773" id="Seg_10601" s="T772">aux</ta>
            <ta e="T774" id="Seg_10602" s="T773">n</ta>
            <ta e="T775" id="Seg_10603" s="T774">v</ta>
            <ta e="T776" id="Seg_10604" s="T775">emphpro</ta>
            <ta e="T777" id="Seg_10605" s="T776">v</ta>
            <ta e="T778" id="Seg_10606" s="T777">pers</ta>
            <ta e="T779" id="Seg_10607" s="T778">adj</ta>
            <ta e="T780" id="Seg_10608" s="T779">n</ta>
            <ta e="T781" id="Seg_10609" s="T780">cop</ta>
            <ta e="T782" id="Seg_10610" s="T781">aux</ta>
            <ta e="T783" id="Seg_10611" s="T782">adj</ta>
            <ta e="T784" id="Seg_10612" s="T783">n</ta>
            <ta e="T785" id="Seg_10613" s="T784">n</ta>
            <ta e="T786" id="Seg_10614" s="T785">adj</ta>
            <ta e="T787" id="Seg_10615" s="T786">n</ta>
            <ta e="T788" id="Seg_10616" s="T787">n</ta>
            <ta e="T789" id="Seg_10617" s="T788">n</ta>
            <ta e="T790" id="Seg_10618" s="T789">v</ta>
            <ta e="T791" id="Seg_10619" s="T790">n</ta>
            <ta e="T792" id="Seg_10620" s="T791">n</ta>
            <ta e="T793" id="Seg_10621" s="T792">v</ta>
            <ta e="T794" id="Seg_10622" s="T793">n</ta>
            <ta e="T795" id="Seg_10623" s="T794">v</ta>
            <ta e="T796" id="Seg_10624" s="T795">v</ta>
            <ta e="T797" id="Seg_10625" s="T796">ptcl</ta>
            <ta e="T798" id="Seg_10626" s="T797">adj</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR" />
         <annotation name="SyF" tierref="SyF" />
         <annotation name="IST" tierref="IST" />
         <annotation name="Top" tierref="Top" />
         <annotation name="Foc" tierref="Foc" />
         <annotation name="BOR" tierref="BOR" />
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_10627" s="T0">Well, there lived a young czar.</ta>
            <ta e="T7" id="Seg_10628" s="T4">In his neighbourhood there lived a poor farmer.</ta>
            <ta e="T23" id="Seg_10629" s="T7">This poor farmer worked from dawn until sunset for the czar just for one spoon of flour.</ta>
            <ta e="T26" id="Seg_10630" s="T23">They lived on it.</ta>
            <ta e="T31" id="Seg_10631" s="T26">The poor farmer had a daughter. </ta>
            <ta e="T36" id="Seg_10632" s="T31">Once the young czar said: </ta>
            <ta e="T45" id="Seg_10633" s="T36">"Well, poor farmer, how much did you smarten up since you've been born?</ta>
            <ta e="T50" id="Seg_10634" s="T45">Guess the riddle: What is sweeter than sweet?</ta>
            <ta e="T55" id="Seg_10635" s="T50">Come in the morning at nine and say it.</ta>
            <ta e="T62" id="Seg_10636" s="T55">If you don't guess, then I will cut your head off on the gallows.</ta>
            <ta e="T77" id="Seg_10637" s="T62">The poor farmer couldn't think out anything: he didn't knew any sweet food, all of the czar's food seemed sweet to him.</ta>
            <ta e="T81" id="Seg_10638" s="T77">He came home, sat down and began to cry.</ta>
            <ta e="T83" id="Seg_10639" s="T81">His daughter asked:</ta>
            <ta e="T87" id="Seg_10640" s="T83">"Father, why are you crying?"</ta>
            <ta e="T93" id="Seg_10641" s="T87">"Oh, my darling, I just went, my life is over. </ta>
            <ta e="T97" id="Seg_10642" s="T93">The young czar gave me a riddle.</ta>
            <ta e="T102" id="Seg_10643" s="T97">If I don't guess it, then he will cut off my head."</ta>
            <ta e="T105" id="Seg_10644" s="T102">"And what does he make you guess?"</ta>
            <ta e="T110" id="Seg_10645" s="T105">""What is sweeter then sweet?", he asked", he said.</ta>
            <ta e="T116" id="Seg_10646" s="T110">"Oh, father, there is no need to cry.</ta>
            <ta e="T118" id="Seg_10647" s="T116">Sleep, relax.</ta>
            <ta e="T124" id="Seg_10648" s="T118">"I know one thing that is sweeter then sweet", you will say.</ta>
            <ta e="T144" id="Seg_10649" s="T124">"After working from dawn to sunset, cleaning your dirt, I come home with one spoon of flour, my old woman heats the pit-dwelling and my nose begins to warm up.</ta>
            <ta e="T151" id="Seg_10650" s="T144">When she makes roux with that little bit of flour, I fall asleep without having eaten.</ta>
            <ta e="T158" id="Seg_10651" s="T151">Therefore the sleep is sweeter than sweet, so it will be probably sleep", you will say.</ta>
            <ta e="T167" id="Seg_10652" s="T158">Could he know more then this?!", the daughter said.</ta>
            <ta e="T170" id="Seg_10653" s="T167">The old man fell asleep.</ta>
            <ta e="T177" id="Seg_10654" s="T170">In the morning he stood up early and came at nine to the czar.</ta>
            <ta e="T180" id="Seg_10655" s="T177">The young czar asked:</ta>
            <ta e="T183" id="Seg_10656" s="T180">"Well, did you guess it?"</ta>
            <ta e="T187" id="Seg_10657" s="T183">"Hey, I guessed something, though.</ta>
            <ta e="T192" id="Seg_10658" s="T187">Whether it is right or wrong?", he said.</ta>
            <ta e="T194" id="Seg_10659" s="T192">"Well, what?"</ta>
            <ta e="T214" id="Seg_10660" s="T194">"After working from dawn to sunset, cleaning your dirt, I come home with one spoon of flour, my old woman heats the pit-dwelling and my nose begins to warm up. </ta>
            <ta e="T221" id="Seg_10661" s="T214">When she makes roux with that little bit of flour, I fall asleep without having eaten. </ta>
            <ta e="T227" id="Seg_10662" s="T221">That sleep is sweeter then sweet, so it will be probably sleep".</ta>
            <ta e="T230" id="Seg_10663" s="T227">The young czar said:</ta>
            <ta e="T235" id="Seg_10664" s="T230">"Oh, you are apparently a smart man.</ta>
            <ta e="T238" id="Seg_10665" s="T235">How do you feed on like this?</ta>
            <ta e="T246" id="Seg_10666" s="T238">Guess more: What is there in the middle world that is faster than fast?</ta>
            <ta e="T251" id="Seg_10667" s="T246">If you don't guess it until tomorrow morning, I will cut off your head".</ta>
            <ta e="T255" id="Seg_10668" s="T251">The old man couldn't solve it.</ta>
            <ta e="T260" id="Seg_10669" s="T255">He came home, sat down and began to cry, the daughter asked.</ta>
            <ta e="T263" id="Seg_10670" s="T260">The old man said: "So and so".</ta>
            <ta e="T268" id="Seg_10671" s="T263">"There is no need to cry, sleep.</ta>
            <ta e="T299" id="Seg_10672" s="T268">Say then in the morning: "Faster than fast", say," is when I run out of my smoky pit-dwelling, clean my eyes, throw back my head and look - my eyes see all stars in the sky, when I look around, I see all the trees and the grass. </ta>
            <ta e="T304" id="Seg_10673" s="T299">I cannot reach [the end of] what I see.</ta>
            <ta e="T310" id="Seg_10674" s="T304">That's why, faster then fast is the human sight", say then.</ta>
            <ta e="T318" id="Seg_10675" s="T310">Could he know more then this?!", the daughter said.</ta>
            <ta e="T321" id="Seg_10676" s="T318">The old man fell asleep.</ta>
            <ta e="T325" id="Seg_10677" s="T321">In the morning he stood up and went to the czar. </ta>
            <ta e="T328" id="Seg_10678" s="T325">The young czar asked: </ta>
            <ta e="T331" id="Seg_10679" s="T328">"Well, did you guess it?" </ta>
            <ta e="T335" id="Seg_10680" s="T331">"Hey, I guessed something.</ta>
            <ta e="T340" id="Seg_10681" s="T335">Whether it is right or wrong?", he said.</ta>
            <ta e="T366" id="Seg_10682" s="T340">"When I run out of my smoky pit-dwelling, clean my eyes, throw back my head and look - my eyes see all stars in the sky, when I look around, I see all the trees and the grass. </ta>
            <ta e="T371" id="Seg_10683" s="T366">I cannot reach [the end of] what I see.</ta>
            <ta e="T378" id="Seg_10684" s="T371">That's why, faster than fast is, apparently, the human sight", he said.</ta>
            <ta e="T381" id="Seg_10685" s="T378">The young czar wondered: </ta>
            <ta e="T387" id="Seg_10686" s="T381">"Oh, old man, you are obviously a smart man.</ta>
            <ta e="T397" id="Seg_10687" s="T387">Well, all good things come in threes", he went out, grabbed a bull and gave it to the old man.</ta>
            <ta e="T403" id="Seg_10688" s="T397">"Let it calve until tomorrow morning and bring the calf with you.</ta>
            <ta e="T410" id="Seg_10689" s="T403">Don't even think of bringing me its dung!", the young czar said.</ta>
            <ta e="T418" id="Seg_10690" s="T410">The poor old man brought the bull home, sat down and began to cry. </ta>
            <ta e="T420" id="Seg_10691" s="T418">His daughter asked:</ta>
            <ta e="T424" id="Seg_10692" s="T420">"Oh, father, why are you crying?"</ta>
            <ta e="T436" id="Seg_10693" s="T424">"Well, he gave me a bull to calve, he said, but how can a bull calve, my darling?"</ta>
            <ta e="T441" id="Seg_10694" s="T436">"Yes, how can a bull calve?!</ta>
            <ta e="T443" id="Seg_10695" s="T441">On the other hand, we have food now.</ta>
            <ta e="T449" id="Seg_10696" s="T443">Go outside and kill it, we'll eat its meat!", the daughter said.</ta>
            <ta e="T457" id="Seg_10697" s="T449">The old man killed the bull, well, they ate.</ta>
            <ta e="T465" id="Seg_10698" s="T457">The daughter gathered the bull's hooves and knucklebones in a bag and gave them to her father:</ta>
            <ta e="T481" id="Seg_10699" s="T465">"Say "These black hooves are the mother and the knucklebones are the calf" and spread them out like children's toys on the czar's table.</ta>
            <ta e="T489" id="Seg_10700" s="T481">Say "I found out, this is the only way for a bull to calve."</ta>
            <ta e="T496" id="Seg_10701" s="T489">Could he know more then this?!</ta>
            <ta e="T500" id="Seg_10702" s="T496">Then he will question you: </ta>
            <ta e="T504" id="Seg_10703" s="T500">"You couldn't make it probably with your own mind.</ta>
            <ta e="T514" id="Seg_10704" s="T504">If you were so smart, you would not earn your keep by cleaning my dirt.</ta>
            <ta e="T517" id="Seg_10705" s="T514">Who taught you?", he will say.</ta>
            <ta e="T521" id="Seg_10706" s="T517">Then don't hide it", the daughter said.</ta>
            <ta e="T527" id="Seg_10707" s="T521">"Say: "I have a bad daughter, she teaches me.""</ta>
            <ta e="T530" id="Seg_10708" s="T527">In the morning the old man went off.</ta>
            <ta e="T533" id="Seg_10709" s="T530">The young czar asked: </ta>
            <ta e="T536" id="Seg_10710" s="T533">"Well, did you guess?"</ta>
            <ta e="T546" id="Seg_10711" s="T536">The old man spread out the bull's hooves and knucklebones like children's toys on the table.</ta>
            <ta e="T554" id="Seg_10712" s="T546">"I found out, this is the only way for a bull to calve", he said.</ta>
            <ta e="T555" id="Seg_10713" s="T554">The czar: </ta>
            <ta e="T559" id="Seg_10714" s="T555">"You probably couldn't make it with your own mind.</ta>
            <ta e="T569" id="Seg_10715" s="T559">If you were so smart, you would not earn your keep by cleaning my dirt.</ta>
            <ta e="T572" id="Seg_10716" s="T569">Who teaches you?", he said.</ta>
            <ta e="T579" id="Seg_10717" s="T572">"I have a bad daughter, she teaches me", the old man said.</ta>
            <ta e="T581" id="Seg_10718" s="T579">The young czar: </ta>
            <ta e="T586" id="Seg_10719" s="T581">"Your daughter was born as a smart girl", he said.</ta>
            <ta e="T591" id="Seg_10720" s="T586">He gave him a bucket with a holey ground.</ta>
            <ta e="T595" id="Seg_10721" s="T591">"May she patch it", he said.</ta>
            <ta e="T601" id="Seg_10722" s="T595">"If she doesn't patch it, I will cut off the heads of both of you", he said.</ta>
            <ta e="T606" id="Seg_10723" s="T601">The old man carried away the bucket and cried.</ta>
            <ta e="T607" id="Seg_10724" s="T606">The daughter: </ta>
            <ta e="T612" id="Seg_10725" s="T607">"What kind of bucket do you have there?", she said.</ta>
            <ta e="T617" id="Seg_10726" s="T612">"Oh, our last day has come.</ta>
            <ta e="T622" id="Seg_10727" s="T617">"She shall patch this bucket", he [the czar] said.</ta>
            <ta e="T628" id="Seg_10728" s="T622">"If she doesn't patch it, I will cut off the heads of both of you", he said."</ta>
            <ta e="T630" id="Seg_10729" s="T628">The daughter said:</ta>
            <ta e="T642" id="Seg_10730" s="T630">"Bring it back: I, woman, will patch it, and he, man, should turn the bucket inside out, like a boot."</ta>
            <ta e="T645" id="Seg_10731" s="T642">The old man carried the bucket [there] and gave it back.</ta>
            <ta e="T648" id="Seg_10732" s="T645">The young czar wondered.</ta>
            <ta e="T650" id="Seg_10733" s="T648">He sent the old man away.</ta>
            <ta e="T655" id="Seg_10734" s="T650">"I will go and see your daughter by myself", he said.</ta>
            <ta e="T662" id="Seg_10735" s="T655">As the young czar saw the daughter, he fell in love and married her.</ta>
            <ta e="T665" id="Seg_10736" s="T662">They lived so for a while.</ta>
            <ta e="T676" id="Seg_10737" s="T665">As they married, the young czar sat there like a doll, his wife advised and decided everything.</ta>
            <ta e="T679" id="Seg_10738" s="T676">The young czar said:</ta>
            <ta e="T687" id="Seg_10739" s="T679">"My darling, it is not possible to have two czars in one country.</ta>
            <ta e="T689" id="Seg_10740" s="T687">Let us separate."</ta>
            <ta e="T690" id="Seg_10741" s="T689">His woman:</ta>
            <ta e="T694" id="Seg_10742" s="T690">"Well, then let us separate, why not?", she said.</ta>
            <ta e="T696" id="Seg_10743" s="T694">The young czar:</ta>
            <ta e="T702" id="Seg_10744" s="T696">"Take everything you want ", he said.</ta>
            <ta e="T704" id="Seg_10745" s="T702">His wife agreed.</ta>
            <ta e="T707" id="Seg_10746" s="T704">And so the night of their separation came.</ta>
            <ta e="T710" id="Seg_10747" s="T707">Her husband fell asleep.</ta>
            <ta e="T720" id="Seg_10748" s="T710">The wife took her husband and the bedding and brought them to her father's pit-dwelling.</ta>
            <ta e="T731" id="Seg_10749" s="T720">The young czar woke up in the morning and got scared; strechting himself he saw that his head and his feet pushed into the poles of a pit-dwelling.</ta>
            <ta e="T735" id="Seg_10750" s="T731">"Hey, where are we?</ta>
            <ta e="T737" id="Seg_10751" s="T735">Why did I come here?"</ta>
            <ta e="T740" id="Seg_10752" s="T737">He shoke his wife and woke her up.</ta>
            <ta e="T741" id="Seg_10753" s="T740">His wife: </ta>
            <ta e="T746" id="Seg_10754" s="T741">"Well, my darling, I brought you here", she said.</ta>
            <ta e="T748" id="Seg_10755" s="T746">"Why did you bring me here?"</ta>
            <ta e="T754" id="Seg_10756" s="T748">"Ah, I wanted just you, and I brought you.</ta>
            <ta e="T761" id="Seg_10757" s="T754">You told me to take everything I want!", the wife said.</ta>
            <ta e="T765" id="Seg_10758" s="T761">The young czar could not go away.</ta>
            <ta e="T768" id="Seg_10759" s="T765">His wife said then:</ta>
            <ta e="T773" id="Seg_10760" s="T768">"You are apparently not that smart.</ta>
            <ta e="T777" id="Seg_10761" s="T773">Be a czar yourself, reign over your people.</ta>
            <ta e="T782" id="Seg_10762" s="T777">And I will be a simple housewife."</ta>
            <ta e="T794" id="Seg_10763" s="T782">Thanks to his daughter the poor farmer started to live in a stone house full of mirrors, he left his pit-dwelling.</ta>
            <ta e="T797" id="Seg_10764" s="T794">So they lived in wealth.</ta>
            <ta e="T798" id="Seg_10765" s="T797">The end.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_10766" s="T0">Es war einmal ein junger Zar.</ta>
            <ta e="T7" id="Seg_10767" s="T4">In seiner Nähe lebte ein armer Bauer.</ta>
            <ta e="T23" id="Seg_10768" s="T7">Dieser arme Bauer arbeitete von der Morgen- bis zur Abenddämmerung für den Zaren, er arbeitete für einen Löffel Mehl.</ta>
            <ta e="T26" id="Seg_10769" s="T23">Davon lebten sie auch.</ta>
            <ta e="T31" id="Seg_10770" s="T26">Der arme Bauer hatte eine Tochter.</ta>
            <ta e="T36" id="Seg_10771" s="T31">Da sagte einmal der junge Zar:</ta>
            <ta e="T45" id="Seg_10772" s="T36">"Nun, armer Bauer, wieviel Verstand hast du in deinem Leben bekommen?</ta>
            <ta e="T50" id="Seg_10773" s="T45">Löse das Rätsel: Was ist süßer als süß?</ta>
            <ta e="T55" id="Seg_10774" s="T50">Komm am Morgen um neun Uhr und sag es.</ta>
            <ta e="T62" id="Seg_10775" s="T55">Wenn du es nicht errätst, dann trenne ich dir am Galgen den Kopf ab.</ta>
            <ta e="T77" id="Seg_10776" s="T62">Der arme Bauer konnte sich nichts denken: er kannte keine süße Speise, alle Speisen des Zaren waren für ihn süß.</ta>
            <ta e="T81" id="Seg_10777" s="T77">Er kam nach Hause, setzte sich hin und weinte.</ta>
            <ta e="T83" id="Seg_10778" s="T81">Seine Tochter fragte:</ta>
            <ta e="T87" id="Seg_10779" s="T83">"Vater, warum weinst du?"</ta>
            <ta e="T93" id="Seg_10780" s="T87">"Ach, meine Liebe, ich ging nur, mein Leben ist zu Ende.</ta>
            <ta e="T97" id="Seg_10781" s="T93">Der junge Zar gab mir ein Rätsel.</ta>
            <ta e="T102" id="Seg_10782" s="T97">Wenn ich es nicht errate, dann schneidet er mir den Kopf ab."</ta>
            <ta e="T105" id="Seg_10783" s="T102">"Und was lässt er dich raten?"</ta>
            <ta e="T110" id="Seg_10784" s="T105">""Was ist süßer als süß?", fragte er", sagte er.</ta>
            <ta e="T116" id="Seg_10785" s="T110">"Ach, Vater, du weinst umsonst.</ta>
            <ta e="T118" id="Seg_10786" s="T116">Schlaf, ruh dich aus.</ta>
            <ta e="T124" id="Seg_10787" s="T118">"Ich weiß eine Sache, die süßer ist als süß", wirst du sagen.</ta>
            <ta e="T144" id="Seg_10788" s="T124">"Wenn ich von der Morgendämmerung bis zur Abenddämmerung gearbeitet habe, deinen Dreck weggemacht habe und einen Löffel Mehl nach Hause bringe, wenn meine Alte die Erdhütte heizt, dann wird meine Nase warm.</ta>
            <ta e="T151" id="Seg_10789" s="T144">Wenn sie dann aus dem Bisschen Mehl Mehlschwitze macht, dann schlafe ich ein ohne zu essen.</ta>
            <ta e="T158" id="Seg_10790" s="T151">Daher ist der Schlaf wohl süßer als süß", wirst du sagen.</ta>
            <ta e="T167" id="Seg_10791" s="T158">Kann er selbst davon mehr wissen?", sagte die Tochter.</ta>
            <ta e="T170" id="Seg_10792" s="T167">Der alte Mann schlief ein.</ta>
            <ta e="T177" id="Seg_10793" s="T170">Am Morgen stand er früh auf und kam um neun Uhr zum Zaren.</ta>
            <ta e="T180" id="Seg_10794" s="T177">Der junge Zar fragte:</ta>
            <ta e="T183" id="Seg_10795" s="T180">"Na, hast du es erraten?"</ta>
            <ta e="T187" id="Seg_10796" s="T183">"Hey, ich habe wohl etwas erraten.</ta>
            <ta e="T192" id="Seg_10797" s="T187">Ob es richtig ist, oder nicht", sagte er.</ta>
            <ta e="T194" id="Seg_10798" s="T192">"Nun, was?"</ta>
            <ta e="T214" id="Seg_10799" s="T194">"Wenn ich von der Morgendämmerung bis zur Abenddämmerung gearbeitet habe und deinen Dreck weggemacht habe, wenn ich einen Löffel Mehl nach Hause bringe, wenn meine Alte die Erdhütte geheizt hat, dann wird meine Nase warm.</ta>
            <ta e="T221" id="Seg_10800" s="T214">Wenn sie dann aus dem Bisschen Mehl Mehlschwitze macht, dann schlafe ich ohne zu essen ein.</ta>
            <ta e="T227" id="Seg_10801" s="T221">Daher ist das, was süßer ist süß, wohl der Schlaf."</ta>
            <ta e="T230" id="Seg_10802" s="T227">Der junge Zar sagte:</ta>
            <ta e="T235" id="Seg_10803" s="T230">"Oh, du bist offenbar ein kluger Mensch.</ta>
            <ta e="T238" id="Seg_10804" s="T235">Wie kommst du so zurecht?</ta>
            <ta e="T246" id="Seg_10805" s="T238">Errate noch: Was gibt es in der mittleren Welt, das schneller ist als schnell? </ta>
            <ta e="T251" id="Seg_10806" s="T246">Wenn du es bis zum Morgen nicht errätst, dann schneide ich dir den Kopf ab."</ta>
            <ta e="T255" id="Seg_10807" s="T251">Der alte Mann konnte es nicht lösen.</ta>
            <ta e="T260" id="Seg_10808" s="T255">Als er angekommen war, setzte er sich und weinte, die Tochter fragte.</ta>
            <ta e="T263" id="Seg_10809" s="T260">Der alte Mann sagte: "So und so".</ta>
            <ta e="T268" id="Seg_10810" s="T263">"Du weinst umsonst, schlaf.</ta>
            <ta e="T299" id="Seg_10811" s="T268">Sag dann am Morgen: "Schneller als schnell", sag, "ist, wenn ich aus meiner schlechten rauchigen Erdhütte springe, meine Augen wische, meinen Kopf zum Himmel hebe und schaue - meine Augen sehen sämtliche Sterne am Himmel, wenn ich auf die Erde sehe, sehe ich sämtliche Gräser und Bäume.</ta>
            <ta e="T304" id="Seg_10812" s="T299">[Die Grenzen] dessen, was ich sehe, erreiche ich nie.</ta>
            <ta e="T310" id="Seg_10813" s="T304">Daher ist das, was schneller ist als schnell, der menschliche Blick", sag dann.</ta>
            <ta e="T318" id="Seg_10814" s="T310">Weiß er selber davon mehr?!", sagte die Tochter.</ta>
            <ta e="T321" id="Seg_10815" s="T318">Der Mann schlief ein.</ta>
            <ta e="T325" id="Seg_10816" s="T321">Am Morgen stand er auf und ging zum Zaren. </ta>
            <ta e="T328" id="Seg_10817" s="T325">Der Junge Zar fragte:</ta>
            <ta e="T331" id="Seg_10818" s="T328">"Nun, hast du es erraten?"</ta>
            <ta e="T335" id="Seg_10819" s="T331">"Hey, ich habe wohl etwas erraten.</ta>
            <ta e="T340" id="Seg_10820" s="T335">Ob es richtig ist oder falsch", sagte er.</ta>
            <ta e="T366" id="Seg_10821" s="T340">"Wenn ich aus meiner schlechten rauchigen Erdhütte springe, meine Augen wische, meinen Kopf zum Himmel hebe und schaue - meine Augen sehen sämtliche Sterne am Himmel, wenn ich auf die Erde sehe, sehe ich sämtliche Gräser und Bäume.</ta>
            <ta e="T371" id="Seg_10822" s="T366">[Die Grenzen] dessen, was ich sehe, erreiche ich nie.</ta>
            <ta e="T378" id="Seg_10823" s="T371">Daher ist das, was schneller ist als schnell, wohl der menschliche Blick", sagte er.</ta>
            <ta e="T381" id="Seg_10824" s="T378">Der junge Zar wunderte sich:</ta>
            <ta e="T387" id="Seg_10825" s="T381">"Oh, alter Mann, du bist doch wohl ein kluger Mensch.</ta>
            <ta e="T397" id="Seg_10826" s="T387">Nun, aller guten Dinge sind drei", er ging hinaus, fing einen Stier und gab ihn dem alten Mann.</ta>
            <ta e="T403" id="Seg_10827" s="T397">"Lass ihn bis zum Morgen kalben und bring das Kalb mit.</ta>
            <ta e="T410" id="Seg_10828" s="T403">Komm nicht auf die Idee seinen Mist zu bringen!", sagte der junge Zar.</ta>
            <ta e="T418" id="Seg_10829" s="T410">Der arme alte Mann brachte den Stier nach Hause, setzte sich hin und weinte.</ta>
            <ta e="T420" id="Seg_10830" s="T418">Seine Tochter fragte:</ta>
            <ta e="T424" id="Seg_10831" s="T420">"Ach, Vater, warum weinst du?"</ta>
            <ta e="T436" id="Seg_10832" s="T424">"Nun, er gab mir einen Stier, damit er kalbt, sagte er, aber wie soll denn ein Stier kalben, mein Kindchen?"</ta>
            <ta e="T441" id="Seg_10833" s="T436">"Ja, wie soll ein Stier kalben?!</ta>
            <ta e="T443" id="Seg_10834" s="T441">Dafür haben wir jetzt zu essen.</ta>
            <ta e="T449" id="Seg_10835" s="T443">Geh hinaus und schlachte ihn, sein Fleisch werden wir essen!", sagte die Tochter.</ta>
            <ta e="T457" id="Seg_10836" s="T449">Der alte Mann schlachtete den Stier, da konnten sie essen.</ta>
            <ta e="T465" id="Seg_10837" s="T457">Die Tochter sammelte die Hufe und Knöchelbeine der Kuh in einer Tasche und gab sie ihrem Vater:</ta>
            <ta e="T481" id="Seg_10838" s="T465">"Sage "Diese schwarzen Hufe sind die Mutter und die Knöchelbeine sind das Kalb" und stelle sie wie Spielzeug auf den Tisch des Zaren.</ta>
            <ta e="T489" id="Seg_10839" s="T481">Sage "Nur so habe ich es für möglich gehalten, dass der Stier kalbt."</ta>
            <ta e="T496" id="Seg_10840" s="T489">Weiß er selber mehr als das?!</ta>
            <ta e="T500" id="Seg_10841" s="T496">Dann wird er dich ausfragen: </ta>
            <ta e="T504" id="Seg_10842" s="T500">"Mit deinem eigenen Verstand hast du das wohl nicht gemacht.</ta>
            <ta e="T514" id="Seg_10843" s="T504">Wenn du so klug wärst, dann hättest du nicht angefangen zurechtzukommen, indem du meinen Dreck wegmachst.</ta>
            <ta e="T517" id="Seg_10844" s="T514">Wer hat es dir beigebracht?", wird er sagen.</ta>
            <ta e="T521" id="Seg_10845" s="T517">Dann verheimliche es nicht", sagte die Tochter.</ta>
            <ta e="T527" id="Seg_10846" s="T521">"Sage: "Ich habe eine böse Tochter, die bringt es mir bei.""</ta>
            <ta e="T530" id="Seg_10847" s="T527">Am Morgen ging der alte Mann.</ta>
            <ta e="T533" id="Seg_10848" s="T530">Der junge Zar fragte:</ta>
            <ta e="T536" id="Seg_10849" s="T533">"Nun, hast du es erraten?"</ta>
            <ta e="T546" id="Seg_10850" s="T536">Der alte Mann stellte die Hufe und Knöchelbeine des Stiers wie Kinderspielzeug auf den Tisch.</ta>
            <ta e="T554" id="Seg_10851" s="T546">"Ich bin nur auf diese Möglichkeit gekommen, wie ein Stier kalben kann", sagte er.</ta>
            <ta e="T555" id="Seg_10852" s="T554">Der Zar:</ta>
            <ta e="T559" id="Seg_10853" s="T555">"Mit deinem eigenen Verstand hast du das wohl nicht gemacht.</ta>
            <ta e="T569" id="Seg_10854" s="T559">Wenn du so klug wärst, dann hättest du nicht angefangen meinen Dreck wegzumachen.</ta>
            <ta e="T572" id="Seg_10855" s="T569">Wer lehrt dich?", sagte er.</ta>
            <ta e="T579" id="Seg_10856" s="T572">"Ich habe eine böse Tochter, die lehrt mich", sagte der alte Mann.</ta>
            <ta e="T581" id="Seg_10857" s="T579">Der junge Zar:</ta>
            <ta e="T586" id="Seg_10858" s="T581">"Als Tochter wurde dir ein kluges Mädchen geboren", sagte er.</ta>
            <ta e="T591" id="Seg_10859" s="T586">Er gab ihm einen Eimer mit löchrigem Boden.</ta>
            <ta e="T595" id="Seg_10860" s="T591">"Sie soll ihn flicken", sagte er.</ta>
            <ta e="T601" id="Seg_10861" s="T595">"Wenn sie ihn nicht flickt, schneide ich euch beiden den Kopf ab", sagte er.</ta>
            <ta e="T606" id="Seg_10862" s="T601">Der alte Mann trug den Eimer davon und weinte.</ta>
            <ta e="T607" id="Seg_10863" s="T606">Die Tochter:</ta>
            <ta e="T612" id="Seg_10864" s="T607">"Was für einen Eimer hast du da?", sagte sie.</ta>
            <ta e="T617" id="Seg_10865" s="T612">"Ach nun, unser Todestag ist gekommen.</ta>
            <ta e="T622" id="Seg_10866" s="T617">"Sie soll diesen Eimer flicken", sagte er [der Zar].</ta>
            <ta e="T628" id="Seg_10867" s="T622">"Wenn sie ihn nicht flickt, dann schneide ich euch beiden den Kopf ab", sagte er."</ta>
            <ta e="T630" id="Seg_10868" s="T628">Die Tochter sagte:</ta>
            <ta e="T642" id="Seg_10869" s="T630">"Bring ihn zurück: Ich flicke [als] Frau, er [als] Mann soll den Eimer wie einen Stiefel auf links drehen."</ta>
            <ta e="T645" id="Seg_10870" s="T642">Der alte Mann trug den Eimer hin und übergab ihn.</ta>
            <ta e="T648" id="Seg_10871" s="T645">Der junge Zar wunderte sich.</ta>
            <ta e="T650" id="Seg_10872" s="T648">Er schickte den alten Mann weg.</ta>
            <ta e="T655" id="Seg_10873" s="T650">"Ich gehe selber, deine Tochter anzuschauen", sagte er.</ta>
            <ta e="T662" id="Seg_10874" s="T655">Als der junge Zar die Tochter sah, verliebte er sich und nahm sie zur Frau.</ta>
            <ta e="T665" id="Seg_10875" s="T662">So lebten sie einige Zeit.</ta>
            <ta e="T676" id="Seg_10876" s="T665">Als er sie geheiratet hatte, saß der junge Zar wie eine Puppe da, alles entschied und riet seine Frau.</ta>
            <ta e="T679" id="Seg_10877" s="T676">Der junge Zar sagte:</ta>
            <ta e="T687" id="Seg_10878" s="T679">"Meine Liebe, in einem Land können wir nicht zwei Zaren sein.</ta>
            <ta e="T689" id="Seg_10879" s="T687">Lass uns uns trennen."</ta>
            <ta e="T690" id="Seg_10880" s="T689">Seine Frau:</ta>
            <ta e="T694" id="Seg_10881" s="T690">"Nun, dann trennen wir uns, warum nicht?", sagte sie.</ta>
            <ta e="T696" id="Seg_10882" s="T694">Der junge Zar:</ta>
            <ta e="T702" id="Seg_10883" s="T696">"Nimm alles mit, was du möchtest", sagte er.</ta>
            <ta e="T704" id="Seg_10884" s="T702">Die Frau war einverstanden.</ta>
            <ta e="T707" id="Seg_10885" s="T704">Und so kam die Nacht ihrer Trennung.</ta>
            <ta e="T710" id="Seg_10886" s="T707">Der Mann schlief ein.</ta>
            <ta e="T720" id="Seg_10887" s="T710">Die Frau nahm ihren Mann und die Bettwäsche und brachte sie in die Erdhütte ihres Vaters.</ta>
            <ta e="T731" id="Seg_10888" s="T720">Der junge Zar wachte am Morgen auf und erschrak; als er sich streckte sah er, dass sein Kopf und seine Beine an die Stangen der Hütte stießen.</ta>
            <ta e="T735" id="Seg_10889" s="T731">"Hey, wo sind wir denn?</ta>
            <ta e="T737" id="Seg_10890" s="T735">Warum bin ich hierher gekommen?"</ta>
            <ta e="T740" id="Seg_10891" s="T737">Er schüttelte und weckte seine Frau.</ta>
            <ta e="T741" id="Seg_10892" s="T740">Seine Frau:</ta>
            <ta e="T746" id="Seg_10893" s="T741">"Nun, meine Liebe, ich habe dich gebracht", sagte sie.</ta>
            <ta e="T748" id="Seg_10894" s="T746">"Warum hast du mich gebracht?"</ta>
            <ta e="T754" id="Seg_10895" s="T748">"Ach, ich wollte dich und habe dich mitgebracht.</ta>
            <ta e="T761" id="Seg_10896" s="T754">Du hast selbst gesagt, dass ich mitnehmen soll, was ich möchte!", sagte die Frau.</ta>
            <ta e="T765" id="Seg_10897" s="T761">Der junge Zar konnte nicht weggehen.</ta>
            <ta e="T768" id="Seg_10898" s="T765">Da sagte die Frau:</ta>
            <ta e="T773" id="Seg_10899" s="T768">"Du bist wohl doch nicht so ein kluger Mensch.</ta>
            <ta e="T777" id="Seg_10900" s="T773">Sei selber Zar und regiere dein Volk.</ta>
            <ta e="T782" id="Seg_10901" s="T777">Und ich bin eine einfache Hausfrau."</ta>
            <ta e="T794" id="Seg_10902" s="T782">Der arme Bauer fing dank seiner Tochter an, im mit Spiegeln versehenen Steinhaus zu leben, seine Erdhütte ließ er zurück.</ta>
            <ta e="T797" id="Seg_10903" s="T794">Und so lebten sie reich und zufrieden.</ta>
            <ta e="T798" id="Seg_10904" s="T797">Ende.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_10905" s="T0">Вот жил-был Юноша-царь.</ta>
            <ta e="T7" id="Seg_10906" s="T4">По соседству с ним жил Нищий крестьянин.</ta>
            <ta e="T23" id="Seg_10907" s="T7">Этот Нищий крестьянин, с утренней тьмы до вечерней тьмы работая у того царя, зарабатывал одну ложку муки.</ta>
            <ta e="T26" id="Seg_10908" s="T23">Этим и жили.</ta>
            <ta e="T31" id="Seg_10909" s="T26">У Нищего крестьянина была одна дочь.</ta>
            <ta e="T36" id="Seg_10910" s="T31">Вот однажды Юноша-царь говорит:</ta>
            <ta e="T45" id="Seg_10911" s="T36">— Ну, Нищий крестьянин, какой ум нажил ты за свой век?</ta>
            <ta e="T50" id="Seg_10912" s="T45">Отгадай загадку: Что слаще сладкого?</ta>
            <ta e="T55" id="Seg_10913" s="T50">Утром в девять часов придешь и скажешь.</ta>
            <ta e="T62" id="Seg_10914" s="T55">Если не отгадаешь этого, отрублю тебе голову.</ta>
            <ta e="T77" id="Seg_10915" s="T62">Нищий крестьянин ничего придумать не мог: сладкой пищи не пробовал, что ни дает царь — все кажется сладким.</ta>
            <ta e="T81" id="Seg_10916" s="T77">Пришел домой, сидит и плачет.​</ta>
            <ta e="T83" id="Seg_10917" s="T81">Дочь спросила:</ta>
            <ta e="T87" id="Seg_10918" s="T83">— Что ты, отец, почему плачешь?</ta>
            <ta e="T93" id="Seg_10919" s="T87">— Ну, дочка, походил — и кончено, пожил — и хватит.</ta>
            <ta e="T97" id="Seg_10920" s="T93">Юноша-царь загадал загадку.</ta>
            <ta e="T102" id="Seg_10921" s="T97">Если не отгадаю, обещал отрубить голову.</ta>
            <ta e="T105" id="Seg_10922" s="T102">— А что загадал?</ta>
            <ta e="T110" id="Seg_10923" s="T105">"Что слаще сладкого?" — спросил — сказал [старик].</ta>
            <ta e="T116" id="Seg_10924" s="T110">— Да ну, отец, зря плачешь.</ta>
            <ta e="T118" id="Seg_10925" s="T116">Усни, отдохни.</ta>
            <ta e="T124" id="Seg_10926" s="T118">"Что слаще сладкого? Знаю одно, — скажешь.</ta>
            <ta e="T144" id="Seg_10927" s="T124">— Когда приношу домой одну ложку муки, с утренней тьмы до вечерней тьмы убирая у тебя грязь, старуха моя топит печь в земляном чуме-голомо, и тогда тепло ударяет мне в нос.</ta>
            <ta e="T151" id="Seg_10928" s="T144">Пока она готовит еду — жарит муку на жиру, я уже засыпаю без ужина.</ta>
            <ta e="T158" id="Seg_10929" s="T151">Так что слаще сладкого, наверно, сон", — скажешь.</ta>
            <ta e="T167" id="Seg_10930" s="T158">Больше этого знает ли он сам? — сказала дочь.</ta>
            <ta e="T170" id="Seg_10931" s="T167">Старик уснул.</ta>
            <ta e="T177" id="Seg_10932" s="T170">Утром встал рано и в девять часов пришел к царю.</ta>
            <ta e="T180" id="Seg_10933" s="T177">Юноша-царь спросил:</ta>
            <ta e="T183" id="Seg_10934" s="T180">— Ну, отгадал?</ta>
            <ta e="T187" id="Seg_10935" s="T183">— Э-э, отгадал как будто.</ta>
            <ta e="T192" id="Seg_10936" s="T187">Правильно ли, нет ли? — сказал.</ta>
            <ta e="T194" id="Seg_10937" s="T192">— Ну что?</ta>
            <ta e="T214" id="Seg_10938" s="T194">— Когда приношу домой одну ложку муки, с утренней тьмы до вечерней тьмы убирая у тебя грязь, старуха моя топит печь в земляном чуме-голомо, и тогда тепло ударяет [мне] в нос.</ta>
            <ta e="T221" id="Seg_10939" s="T214">Пока из муки она готовит кашу, я уже засыпаю без ужина.</ta>
            <ta e="T227" id="Seg_10940" s="T221">Так что слаще сладкого, наверно, сон.</ta>
            <ta e="T230" id="Seg_10941" s="T227">Юноша-царь сказал:</ta>
            <ta e="T235" id="Seg_10942" s="T230">— Да ну, оказывается, ты умный человек.</ta>
            <ta e="T238" id="Seg_10943" s="T235">Почему же так просто живешь?</ta>
            <ta e="T246" id="Seg_10944" s="T238">Еще отгадай: "В Среднем мире что быстрее быстрого?"</ta>
            <ta e="T251" id="Seg_10945" s="T246">Если к утру не отгадаешь, отрублю голову.</ta>
            <ta e="T255" id="Seg_10946" s="T251">Старик ничего придумать не смог.</ta>
            <ta e="T260" id="Seg_10947" s="T255">Когда пришел, сел и плакал, дочь спросила.</ta>
            <ta e="T263" id="Seg_10948" s="T260">Старик рассказывал: так-то и так-то.</ta>
            <ta e="T268" id="Seg_10949" s="T263">— Зачем зря плакать, усни.</ta>
            <ta e="T299" id="Seg_10950" s="T268">Утром скажешь: "[Что] быстрее быстрого? Когда выскакиваю из своей дымной землянки и, протерев глаза, запрокидываю голову вверх — все небесные звезды до единой вижу, опущу взгляд на землю — все деревья, все травки до единой вижу.</ta>
            <ta e="T304" id="Seg_10951" s="T299">Но достичь [предела] видимого не могу.</ta>
            <ta e="T310" id="Seg_10952" s="T304">Так что быстрее быстрого человеческий взор", — скажешь.</ta>
            <ta e="T318" id="Seg_10953" s="T310">Больше этого знает ли он сам?! — сказала дочь.</ta>
            <ta e="T321" id="Seg_10954" s="T318">Старик уснул.</ta>
            <ta e="T325" id="Seg_10955" s="T321">Утром пошел к своему царю. </ta>
            <ta e="T328" id="Seg_10956" s="T325">Юноша-царь спросил:</ta>
            <ta e="T331" id="Seg_10957" s="T328">— Ну, отгадал?</ta>
            <ta e="T335" id="Seg_10958" s="T331">— Э-э, отгадал как будто.</ta>
            <ta e="T340" id="Seg_10959" s="T335">Правильно ли, нет ли?</ta>
            <ta e="T366" id="Seg_10960" s="T340">— Когда выскакиваю из своей дымной землянки и, протерев глаза, запрокидываю голову вверх — все небесные звезды до единой вижу, опущу взгляд на землю — все деревья, все травки до единой вижу.</ta>
            <ta e="T371" id="Seg_10961" s="T366">Но достичь [предела] видимого не могу.</ta>
            <ta e="T378" id="Seg_10962" s="T371">Так что быстрее быстрого — человеческий взор, — сказал.</ta>
            <ta e="T381" id="Seg_10963" s="T378">Юноша-царь удивился:</ta>
            <ta e="T387" id="Seg_10964" s="T381">— Да ты, старик, оказывается, умный человек.</ta>
            <ta e="T397" id="Seg_10965" s="T387">Все проходит три круга!", Выйдя, поймал быка, дал старику.</ta>
            <ta e="T403" id="Seg_10966" s="T397">— Чтоб у тебя к утру бык отелился, приведешь с теленком.</ta>
            <ta e="T410" id="Seg_10967" s="T403">Только не вздумай помет его принести! — сказал Юноша-царь.</ta>
            <ta e="T418" id="Seg_10968" s="T410">Бедный старик привел быка домой, сел и плакал.</ta>
            <ta e="T420" id="Seg_10969" s="T418">Дочь спросила:</ta>
            <ta e="T424" id="Seg_10970" s="T420">— Что ты, отец, почему плачешь?</ta>
            <ta e="T436" id="Seg_10971" s="T424">— Вот дал быка, чтоб тот отелился, — говорит, — а как может телиться бык, милая?</ta>
            <ta e="T441" id="Seg_10972" s="T436">— Э-э, как отелится бык?!</ta>
            <ta e="T443" id="Seg_10973" s="T441">Все к лучшему, теперь мы с пищей.</ta>
            <ta e="T449" id="Seg_10974" s="T443">Выйди и заколи, мясо съедим! — сказала дочь.</ta>
            <ta e="T457" id="Seg_10975" s="T449">Старик заколол быка, вот съели.</ta>
            <ta e="T465" id="Seg_10976" s="T457">Дочь собрала в суму копыта и бабки и передала отцу.</ta>
            <ta e="T481" id="Seg_10977" s="T465">— "Вот черные роговые копыта — мать, а бабки — телята" — так сказав, расставишь их на царском столе подряд, как детские игрушки.</ta>
            <ta e="T489" id="Seg_10978" s="T481">"Я постиг, что бык только так телится", — скажешь.</ta>
            <ta e="T496" id="Seg_10979" s="T489">Больше этого знает ли он сам?!</ta>
            <ta e="T500" id="Seg_10980" s="T496">Тогда он станет пытать: </ta>
            <ta e="T504" id="Seg_10981" s="T500">"Своим умом ты этого постигнуть не можешь.</ta>
            <ta e="T514" id="Seg_10982" s="T504">Если б ты был таким умным, не стал бы чистить у меня грязь-нечистоты.</ta>
            <ta e="T517" id="Seg_10983" s="T514">Кто научает?" — спросит.</ta>
            <ta e="T521" id="Seg_10984" s="T517">Тогда не скрывай, — говорит дочь.</ta>
            <ta e="T527" id="Seg_10985" s="T521">— "Есть у меня дочь-простушка, она научает", — скажешь.</ta>
            <ta e="T530" id="Seg_10986" s="T527">Утром старик пошел.</ta>
            <ta e="T533" id="Seg_10987" s="T530">Юноша-царь спросил:</ta>
            <ta e="T536" id="Seg_10988" s="T533">— Ну, отгадал?</ta>
            <ta e="T546" id="Seg_10989" s="T536">Старик копыта и бабки расставляет подряд на столе, словно детские игрушки.</ta>
            <ta e="T554" id="Seg_10990" s="T546">— Я постиг, что бык только так телится, — сказал.</ta>
            <ta e="T555" id="Seg_10991" s="T554">Царь:</ta>
            <ta e="T559" id="Seg_10992" s="T555">— Своим умом ты этого постигнуть не мог.</ta>
            <ta e="T569" id="Seg_10993" s="T559">Если бы ты был таким умным, не стал бы чистить у меня грязь-нечистоты.</ta>
            <ta e="T572" id="Seg_10994" s="T569">Кто научает? — сказал.</ta>
            <ta e="T579" id="Seg_10995" s="T572">— Есть у меня дочь-простушка, она научает, — сказал старик.</ta>
            <ta e="T581" id="Seg_10996" s="T579">Юноша-царь:</ta>
            <ta e="T586" id="Seg_10997" s="T581">— Дочь у тебя умницей родилась, — сказал.</ta>
            <ta e="T591" id="Seg_10998" s="T586">Дает ему ведро с пробитым дном.</ta>
            <ta e="T595" id="Seg_10999" s="T591">— Пусть залатает, — сказал.</ta>
            <ta e="T601" id="Seg_11000" s="T595">— Если не залатает, обоим отрублю головы, — говорит.</ta>
            <ta e="T606" id="Seg_11001" s="T601">Старик с плачем унес ведро.</ta>
            <ta e="T607" id="Seg_11002" s="T606">Дочь:</ta>
            <ta e="T612" id="Seg_11003" s="T607">— Что за ведро у тебя, отец? — сказала.</ta>
            <ta e="T617" id="Seg_11004" s="T612">— Ну, милая, настал день неминуемой гибели.</ta>
            <ta e="T622" id="Seg_11005" s="T617">Велел тебе залатать ведро, — сказал.</ta>
            <ta e="T628" id="Seg_11006" s="T622">— "Если не залатает, обоим отрублю головы", — сказал.</ta>
            <ta e="T630" id="Seg_11007" s="T628">Дочь сказала:</ta>
            <ta e="T642" id="Seg_11008" s="T630">— Унеси обратно и передай: залатать, конечно, женское дело, но пусть он как мужчина, вывернет ведро, словно торбаса, наизнанку.</ta>
            <ta e="T645" id="Seg_11009" s="T642">Старик унес и передал.</ta>
            <ta e="T648" id="Seg_11010" s="T645">Юноша-царь удивился.</ta>
            <ta e="T650" id="Seg_11011" s="T648">Отпустил старика.</ta>
            <ta e="T655" id="Seg_11012" s="T650">— Сам пойду посмотреть на твою дочь, — сказал.</ta>
            <ta e="T662" id="Seg_11013" s="T655">Юноша-царь, как только увидел девушку, сразу влюбился и женился на ней.</ta>
            <ta e="T665" id="Seg_11014" s="T662">Так прожили несколько времени.</ta>
            <ta e="T676" id="Seg_11015" s="T665">Как поженились, Юноша-царь, словно кукла, сидит себе, — все решает, советует жена.</ta>
            <ta e="T679" id="Seg_11016" s="T676">Юноша-царь сказал:</ta>
            <ta e="T687" id="Seg_11017" s="T679">— Милая, в одной стране двум царям не бывать.</ta>
            <ta e="T689" id="Seg_11018" s="T687">Разведемся лучше.</ta>
            <ta e="T690" id="Seg_11019" s="T689">Жена:</ta>
            <ta e="T694" id="Seg_11020" s="T690">— Давай разведемся, что ли? — сказала.</ta>
            <ta e="T696" id="Seg_11021" s="T694">Юноша-царь:</ta>
            <ta e="T702" id="Seg_11022" s="T696">— Бери с собой все, что пожелаешь. — сказал.</ta>
            <ta e="T704" id="Seg_11023" s="T702">Жена согласилась.</ta>
            <ta e="T707" id="Seg_11024" s="T704">Вот настала последняя ночь.</ta>
            <ta e="T710" id="Seg_11025" s="T707">Муж засыпал.</ta>
            <ta e="T720" id="Seg_11026" s="T710">Жена взяла в охапку мужа с постелью и унесла в землянку отца.</ta>
            <ta e="T731" id="Seg_11027" s="T720">Юноша-царь утром проснулся — удивился, потянулся было — голова и ноги уперлись в шесты землянки.</ta>
            <ta e="T735" id="Seg_11028" s="T731">— Да где же мы?</ta>
            <ta e="T737" id="Seg_11029" s="T735">Как я здесь очутился?</ta>
            <ta e="T740" id="Seg_11030" s="T737">Толкая, будил жену.</ta>
            <ta e="T741" id="Seg_11031" s="T740">Жена:</ta>
            <ta e="T746" id="Seg_11032" s="T741">— Милый, это я тебя принесла [сюда]. — сказал.</ta>
            <ta e="T748" id="Seg_11033" s="T746">— А почему принесла?</ta>
            <ta e="T754" id="Seg_11034" s="T748">— А как же, захотела тебя, вот и принесла.</ta>
            <ta e="T761" id="Seg_11035" s="T754">Сам же сказал взять с собой все, что захочу! — сказала жена.</ta>
            <ta e="T765" id="Seg_11036" s="T761">Юноша-царь так и не смог развестись.</ta>
            <ta e="T768" id="Seg_11037" s="T765">Вот жена сказала:</ta>
            <ta e="T773" id="Seg_11038" s="T768">— Не такой уж ты умный человек, оказывается.</ta>
            <ta e="T777" id="Seg_11039" s="T773">Будь сам царем, правь своим народом.</ta>
            <ta e="T782" id="Seg_11040" s="T777">А я буду [у тебя] простой хозяйкой.</ta>
            <ta e="T794" id="Seg_11041" s="T782">Нищий крестьянин благодаря дочери стал вот жить в зеркальном каменном доме, покинув совсем свою землянку.</ta>
            <ta e="T797" id="Seg_11042" s="T794">Вот так и живут богато.</ta>
            <ta e="T798" id="Seg_11043" s="T797">Конец.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T4" id="Seg_11044" s="T0">Вот жил-был Юноша-царь.</ta>
            <ta e="T7" id="Seg_11045" s="T4">По соседству с ним жил Нищий крестьянин.</ta>
            <ta e="T23" id="Seg_11046" s="T7">Этот Нищий крестьянин, с утренней тьмы до вечерней тьмы работая у того царя, зарабатывал одну ложку муки.</ta>
            <ta e="T26" id="Seg_11047" s="T23">Этим и жили.</ta>
            <ta e="T31" id="Seg_11048" s="T26">У Нищего крестьянина была единственная дочь.</ta>
            <ta e="T36" id="Seg_11049" s="T31">Вот однажды Юноша-царь говорит:</ta>
            <ta e="T45" id="Seg_11050" s="T36">— Ну, Нищий крестьянин, какой ум нажил ты за свой век?</ta>
            <ta e="T50" id="Seg_11051" s="T45">Отгадай загадку: "Что слаще сладкого?" </ta>
            <ta e="T55" id="Seg_11052" s="T50">Утром в девять часов придешь и скажешь.</ta>
            <ta e="T62" id="Seg_11053" s="T55">Если не отгадаешь этого, отрублю тебе голову.</ta>
            <ta e="T77" id="Seg_11054" s="T62">Нищий крестьянин ничего придумать не мог: сладкой пищи не пробовал, что ни дает царь — все кажется сладким.</ta>
            <ta e="T81" id="Seg_11055" s="T77">Пришел домой, сидит и плачет.</ta>
            <ta e="T83" id="Seg_11056" s="T81">Дочь спрашивает:</ta>
            <ta e="T87" id="Seg_11057" s="T83">— Что ты, отец, почему плачешь?</ta>
            <ta e="T93" id="Seg_11058" s="T87">— Ну, дочка, походил — и кончено, пожил — и хватит.</ta>
            <ta e="T97" id="Seg_11059" s="T93">Юноша-царь загадал загадку.</ta>
            <ta e="T102" id="Seg_11060" s="T97">Если не отгадаю, обещал отрубить голову.</ta>
            <ta e="T105" id="Seg_11061" s="T102">— А что загадал?</ta>
            <ta e="T110" id="Seg_11062" s="T105">— Спросил: "Что слаще сладкого?" — говорит [старик].</ta>
            <ta e="T116" id="Seg_11063" s="T110">— Да ну, отец, зря плачешь.</ta>
            <ta e="T118" id="Seg_11064" s="T116">Усни, отдохни.</ta>
            <ta e="T124" id="Seg_11065" s="T118">"Что слаще сладкого? Знаю одно, — скажешь.</ta>
            <ta e="T144" id="Seg_11066" s="T124">— Когда приношу домой одну ложку муки, с утренней тьмы до вечерней тьмы убирая у тебя грязь, старуха моя топит печь в земляном чуме-голомо, и тогда тепло ударяет мне в нос.</ta>
            <ta e="T151" id="Seg_11067" s="T144">Пока она готовит еду — жарит муку на жиру, я уже засыпаю без ужина.</ta>
            <ta e="T158" id="Seg_11068" s="T151">Так что слаще сладкого, наверно, сон", — скажешь.</ta>
            <ta e="T167" id="Seg_11069" s="T158">Больше этого знает ли он сам? — сказала дочь.</ta>
            <ta e="T170" id="Seg_11070" s="T167">Старик уснул.</ta>
            <ta e="T177" id="Seg_11071" s="T170">Утром встал рано и в девять часов пришел к царю.</ta>
            <ta e="T180" id="Seg_11072" s="T177">Юноша-царь спрашивает:</ta>
            <ta e="T183" id="Seg_11073" s="T180">— Ну, отгадал?</ta>
            <ta e="T187" id="Seg_11074" s="T183">— Э-э, отгадал как будто.</ta>
            <ta e="T192" id="Seg_11075" s="T187">Правильно ли, нет ли? — говорит.</ta>
            <ta e="T194" id="Seg_11076" s="T192">— Ну что?</ta>
            <ta e="T214" id="Seg_11077" s="T194">— Когда приношу домой одну ложку муки, с утренней тьмы до вечерней тьмы убирая у тебя грязь, старуха моя топит печь в земляном чуме-голомо, и тогда тепло ударяет [мне] в нос.</ta>
            <ta e="T221" id="Seg_11078" s="T214">Пока из муки она готовит кашу, я уже засыпаю без ужина.</ta>
            <ta e="T227" id="Seg_11079" s="T221">Так что слаще сладкого, наверно, сон.</ta>
            <ta e="T230" id="Seg_11080" s="T227">Юноша-царь говорит:</ta>
            <ta e="T235" id="Seg_11081" s="T230">— Да ну, оказывается, ты умный человек.</ta>
            <ta e="T238" id="Seg_11082" s="T235">Почему же так просто живешь?</ta>
            <ta e="T246" id="Seg_11083" s="T238">Еще отгадай: "В Среднем мире что быстрее быстрого?" </ta>
            <ta e="T251" id="Seg_11084" s="T246">Если к утру не отгадаешь, отрублю голову.</ta>
            <ta e="T255" id="Seg_11085" s="T251">Старик ничего придумать не смог.</ta>
            <ta e="T260" id="Seg_11086" s="T255">Когда сидел и плакал, дочь спросила.</ta>
            <ta e="T263" id="Seg_11087" s="T260">Старик рассказывал: так-то и так-то.</ta>
            <ta e="T268" id="Seg_11088" s="T263">— Зачем зря плакать, усни.</ta>
            <ta e="T299" id="Seg_11089" s="T268">Утром скажешь: "[Что] быстрее быстрого? Когда выскакиваю из своего дымного земляного чума-голомо и, протерев глаза, запрокидываю голову вверх — все небесные звезды до единой вижу, опущу взгляд на землю — все деревья, все травки до единой вижу.</ta>
            <ta e="T304" id="Seg_11090" s="T299">Но достичь [предела] видимого не могу.</ta>
            <ta e="T310" id="Seg_11091" s="T304">Так что быстрее быстрого человеческий взор", — скажешь.</ta>
            <ta e="T318" id="Seg_11092" s="T310">Больше этого знает ли он сам?! — сказала дочь.</ta>
            <ta e="T321" id="Seg_11093" s="T318">Старик уснул.</ta>
            <ta e="T325" id="Seg_11094" s="T321">Утром пошел к своему царю. </ta>
            <ta e="T328" id="Seg_11095" s="T325">Юноша-царь спрашивает:</ta>
            <ta e="T331" id="Seg_11096" s="T328">— Ну, отгадал?</ta>
            <ta e="T335" id="Seg_11097" s="T331">— Э-э, отгадал как будто.</ta>
            <ta e="T340" id="Seg_11098" s="T335">Правильно ли, нет ли? — говорит.</ta>
            <ta e="T366" id="Seg_11099" s="T340">— Когда выскакиваю из своего дымного земляного чунл-голомо и, протерев глаза, запрокидываю голову вверх — все небесные звезды до единой вижу, опущу взгляд на землю — все деревья, все травки до единой вижу,</ta>
            <ta e="T371" id="Seg_11100" s="T366">но достичь [предела] видимого не могу.</ta>
            <ta e="T378" id="Seg_11101" s="T371">Так что быстрее быстрого — человеческий взор, — сказал.</ta>
            <ta e="T381" id="Seg_11102" s="T378">Юноша-царь удивляется:</ta>
            <ta e="T387" id="Seg_11103" s="T381">— Да ты, старик, оказывается, умный человек.</ta>
            <ta e="T397" id="Seg_11104" s="T387">Все проходит три круга! — Выйдя, велит поймать быка, дает старику.</ta>
            <ta e="T403" id="Seg_11105" s="T397">— Чтоб у тебя к утру бык отелился, приведешь с теленком.</ta>
            <ta e="T410" id="Seg_11106" s="T403">Только не вздумай помет его принести! — сказал Юноша-царь.</ta>
            <ta e="T418" id="Seg_11107" s="T410">Бедный старик привел быка домой, сидит и плачет.</ta>
            <ta e="T420" id="Seg_11108" s="T418">Дочь спрашивает:</ta>
            <ta e="T424" id="Seg_11109" s="T420">— Что ты, отец, почему плачешь?</ta>
            <ta e="T436" id="Seg_11110" s="T424">— Вот дал быка, чтоб тот отелился, — говорит, — а как может телиться бык, милая?</ta>
            <ta e="T441" id="Seg_11111" s="T436">— Э-э, как отелится бык?!</ta>
            <ta e="T443" id="Seg_11112" s="T441">Все к лучшему, теперь мы с пищей.</ta>
            <ta e="T449" id="Seg_11113" s="T443">Выйди и заколи, мясо съедим! — говорит дочь.</ta>
            <ta e="T457" id="Seg_11114" s="T449">Старик заколол быка, вот съели.</ta>
            <ta e="T465" id="Seg_11115" s="T457">Дочь собрала в суму копыта и бабки и передала отцу.</ta>
            <ta e="T481" id="Seg_11116" s="T465">— "Вот черные роговые копыта — мать, а бабки — телята" — так сказав, расставишь их на царском столе подряд, как детские игрушки.</ta>
            <ta e="T489" id="Seg_11117" s="T481">"Я постиг, что бык только так телится", — скажешь.</ta>
            <ta e="T496" id="Seg_11118" s="T489">Больше этого знает ли он сам?!</ta>
            <ta e="T500" id="Seg_11119" s="T496">Тогда он станет пытать: </ta>
            <ta e="T504" id="Seg_11120" s="T500">"Своим умом ты этого постигнуть не можешь.</ta>
            <ta e="T514" id="Seg_11121" s="T504">Если б ты был таким умным, не стал бы чистить у меня грязь-нечистоты.</ta>
            <ta e="T517" id="Seg_11122" s="T514">Кто научает?" — спросит.</ta>
            <ta e="T521" id="Seg_11123" s="T517">Тогда не скрывай, — говорит дочь.</ta>
            <ta e="T527" id="Seg_11124" s="T521">— "Есть у меня дочь-простушка, она научает", — скажешь.</ta>
            <ta e="T530" id="Seg_11125" s="T527">Утром старик пошел.</ta>
            <ta e="T533" id="Seg_11126" s="T530">Юноша-царь спрашивает:</ta>
            <ta e="T536" id="Seg_11127" s="T533">— Ну, отгадал?</ta>
            <ta e="T546" id="Seg_11128" s="T536">Старик копыта и бабки расставляет подряд на столе, словно детские игрушки.</ta>
            <ta e="T554" id="Seg_11129" s="T546">— Я постиг, что бык только так телится, — сказал.</ta>
            <ta e="T555" id="Seg_11130" s="T554">Царь говорит:</ta>
            <ta e="T559" id="Seg_11131" s="T555">— Своим умом ты этого постигнуть не мог.</ta>
            <ta e="T569" id="Seg_11132" s="T559">Если бы ты был таким умным, не стал бы чистить у меня грязь-нечистоты.</ta>
            <ta e="T572" id="Seg_11133" s="T569">Кто научает? — говорит.</ta>
            <ta e="T579" id="Seg_11134" s="T572">— Есть у меня дочь-простушка, она научает, — говорит старик.</ta>
            <ta e="T581" id="Seg_11135" s="T579">Юноша-царь:</ta>
            <ta e="T586" id="Seg_11136" s="T581">— Дочь у тебя умницей родилась, — говорит.</ta>
            <ta e="T591" id="Seg_11137" s="T586">Дает ему ведро с пробитым дном.</ta>
            <ta e="T595" id="Seg_11138" s="T591">— Пусть залатает, — говорит.</ta>
            <ta e="T601" id="Seg_11139" s="T595">— Если не залатает, обоим отрублю головы, — говорит.</ta>
            <ta e="T606" id="Seg_11140" s="T601">Старик с плачем унес ведро.</ta>
            <ta e="T607" id="Seg_11141" s="T606">Дочь спрашивает:</ta>
            <ta e="T612" id="Seg_11142" s="T607">— Что за ведро у тебя, отец? — говорит.</ta>
            <ta e="T617" id="Seg_11143" s="T612">— Ну, милая, настал день неминуемой гибели.</ta>
            <ta e="T622" id="Seg_11144" s="T617">Велел тебе залатать ведро, — говорит.</ta>
            <ta e="T628" id="Seg_11145" s="T622">— "Если не залатает, обоим отрублю головы", — сказал.</ta>
            <ta e="T630" id="Seg_11146" s="T628">Дочь говорит:</ta>
            <ta e="T642" id="Seg_11147" s="T630">— Унеси обратно и передай: залатать, конечно, женское дело, но пусть он как мужчина, вывернет ведро, словно торбаса, наизнанку.</ta>
            <ta e="T645" id="Seg_11148" s="T642">Старик унес и передал.</ta>
            <ta e="T648" id="Seg_11149" s="T645">Юноша-царь удивляется.</ta>
            <ta e="T650" id="Seg_11150" s="T648">Отпускает старика.</ta>
            <ta e="T655" id="Seg_11151" s="T650">— Сам пойду посмотреть на твою дочь, — говорит.</ta>
            <ta e="T662" id="Seg_11152" s="T655">Юноша-царь, как только увидел девушку, сразу влюбился и женился на ней.</ta>
            <ta e="T665" id="Seg_11153" s="T662">Сколько прожили — неизвестно.</ta>
            <ta e="T676" id="Seg_11154" s="T665">Как поженились, Юноша-царь, словно кукла, сидит себе, — все решает, советует жена.</ta>
            <ta e="T679" id="Seg_11155" s="T676">Юноша-царь говорит:</ta>
            <ta e="T687" id="Seg_11156" s="T679">— Милая, в одной стране двум царям не бывать.</ta>
            <ta e="T689" id="Seg_11157" s="T687">Разведемся лучше.</ta>
            <ta e="T690" id="Seg_11158" s="T689">Жена говорит:</ta>
            <ta e="T694" id="Seg_11159" s="T690">— Давай разведемся, что ли? — говорит.</ta>
            <ta e="T696" id="Seg_11160" s="T694">Юноша-царь говорит:</ta>
            <ta e="T702" id="Seg_11161" s="T696">— Бери с собой все, что пожелаешь.</ta>
            <ta e="T704" id="Seg_11162" s="T702">Жена согласилась.</ta>
            <ta e="T707" id="Seg_11163" s="T704">Вот настает последняя ночь.</ta>
            <ta e="T710" id="Seg_11164" s="T707">Муж засыпает.</ta>
            <ta e="T720" id="Seg_11165" s="T710">Жена берет в охапку мужа с постелью и уносит в земляной чум-голомо отца.</ta>
            <ta e="T731" id="Seg_11166" s="T720">Юноша-царь утром проснулся — удивляется, потянулся было — голова и ноги уперлись в шесты голомо.</ta>
            <ta e="T735" id="Seg_11167" s="T731">— Да где же мы?</ta>
            <ta e="T737" id="Seg_11168" s="T735">Как я здесь очутился?</ta>
            <ta e="T740" id="Seg_11169" s="T737">— толкая, будит жену.</ta>
            <ta e="T741" id="Seg_11170" s="T740">Жена говорит:</ta>
            <ta e="T746" id="Seg_11171" s="T741">— Милый, это я тебя принесла [сюда].</ta>
            <ta e="T748" id="Seg_11172" s="T746">— А почему принесла?</ta>
            <ta e="T754" id="Seg_11173" s="T748">— А как же, захотела тебя, вот и принесла.</ta>
            <ta e="T761" id="Seg_11174" s="T754">Сам же сказал взять с собой все, что захочу! — говорит жена.</ta>
            <ta e="T765" id="Seg_11175" s="T761">Юноша-царь так и не смог развестись.</ta>
            <ta e="T768" id="Seg_11176" s="T765">Вот жена говорит:</ta>
            <ta e="T773" id="Seg_11177" s="T768">— Не такой уж ты умный человек, оказывается.</ta>
            <ta e="T777" id="Seg_11178" s="T773">Будь сам царем, правь своим народом.</ta>
            <ta e="T782" id="Seg_11179" s="T777">А я буду [у тебя] простой хозяйкой.</ta>
            <ta e="T794" id="Seg_11180" s="T782">Нищий крестьянин благодаря дочери стал вот жить в зеркальном каменном доме, покинув совсем свой земляной чум-голомо.</ta>
            <ta e="T797" id="Seg_11181" s="T794">Вот так и живут богато.</ta>
            <ta e="T798" id="Seg_11182" s="T797">Конец.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T62" id="Seg_11183" s="T55">[DCh]: "ɨjɨːr mas" means 'gallows'</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T559" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
            <conversion-tli id="T571" />
            <conversion-tli id="T572" />
            <conversion-tli id="T573" />
            <conversion-tli id="T574" />
            <conversion-tli id="T575" />
            <conversion-tli id="T576" />
            <conversion-tli id="T577" />
            <conversion-tli id="T578" />
            <conversion-tli id="T579" />
            <conversion-tli id="T580" />
            <conversion-tli id="T581" />
            <conversion-tli id="T582" />
            <conversion-tli id="T583" />
            <conversion-tli id="T584" />
            <conversion-tli id="T585" />
            <conversion-tli id="T586" />
            <conversion-tli id="T587" />
            <conversion-tli id="T588" />
            <conversion-tli id="T589" />
            <conversion-tli id="T590" />
            <conversion-tli id="T591" />
            <conversion-tli id="T592" />
            <conversion-tli id="T593" />
            <conversion-tli id="T594" />
            <conversion-tli id="T595" />
            <conversion-tli id="T596" />
            <conversion-tli id="T597" />
            <conversion-tli id="T598" />
            <conversion-tli id="T599" />
            <conversion-tli id="T600" />
            <conversion-tli id="T601" />
            <conversion-tli id="T602" />
            <conversion-tli id="T603" />
            <conversion-tli id="T604" />
            <conversion-tli id="T605" />
            <conversion-tli id="T606" />
            <conversion-tli id="T607" />
            <conversion-tli id="T608" />
            <conversion-tli id="T609" />
            <conversion-tli id="T610" />
            <conversion-tli id="T611" />
            <conversion-tli id="T612" />
            <conversion-tli id="T613" />
            <conversion-tli id="T614" />
            <conversion-tli id="T615" />
            <conversion-tli id="T616" />
            <conversion-tli id="T617" />
            <conversion-tli id="T618" />
            <conversion-tli id="T619" />
            <conversion-tli id="T620" />
            <conversion-tli id="T621" />
            <conversion-tli id="T622" />
            <conversion-tli id="T623" />
            <conversion-tli id="T624" />
            <conversion-tli id="T625" />
            <conversion-tli id="T626" />
            <conversion-tli id="T627" />
            <conversion-tli id="T628" />
            <conversion-tli id="T629" />
            <conversion-tli id="T630" />
            <conversion-tli id="T631" />
            <conversion-tli id="T632" />
            <conversion-tli id="T633" />
            <conversion-tli id="T634" />
            <conversion-tli id="T635" />
            <conversion-tli id="T636" />
            <conversion-tli id="T637" />
            <conversion-tli id="T638" />
            <conversion-tli id="T639" />
            <conversion-tli id="T640" />
            <conversion-tli id="T641" />
            <conversion-tli id="T642" />
            <conversion-tli id="T643" />
            <conversion-tli id="T644" />
            <conversion-tli id="T645" />
            <conversion-tli id="T646" />
            <conversion-tli id="T647" />
            <conversion-tli id="T648" />
            <conversion-tli id="T649" />
            <conversion-tli id="T650" />
            <conversion-tli id="T651" />
            <conversion-tli id="T652" />
            <conversion-tli id="T653" />
            <conversion-tli id="T654" />
            <conversion-tli id="T655" />
            <conversion-tli id="T656" />
            <conversion-tli id="T657" />
            <conversion-tli id="T658" />
            <conversion-tli id="T659" />
            <conversion-tli id="T660" />
            <conversion-tli id="T661" />
            <conversion-tli id="T662" />
            <conversion-tli id="T663" />
            <conversion-tli id="T664" />
            <conversion-tli id="T665" />
            <conversion-tli id="T666" />
            <conversion-tli id="T667" />
            <conversion-tli id="T668" />
            <conversion-tli id="T669" />
            <conversion-tli id="T670" />
            <conversion-tli id="T671" />
            <conversion-tli id="T672" />
            <conversion-tli id="T673" />
            <conversion-tli id="T674" />
            <conversion-tli id="T675" />
            <conversion-tli id="T676" />
            <conversion-tli id="T677" />
            <conversion-tli id="T678" />
            <conversion-tli id="T679" />
            <conversion-tli id="T680" />
            <conversion-tli id="T681" />
            <conversion-tli id="T682" />
            <conversion-tli id="T683" />
            <conversion-tli id="T684" />
            <conversion-tli id="T685" />
            <conversion-tli id="T686" />
            <conversion-tli id="T687" />
            <conversion-tli id="T688" />
            <conversion-tli id="T689" />
            <conversion-tli id="T690" />
            <conversion-tli id="T691" />
            <conversion-tli id="T692" />
            <conversion-tli id="T693" />
            <conversion-tli id="T694" />
            <conversion-tli id="T695" />
            <conversion-tli id="T696" />
            <conversion-tli id="T697" />
            <conversion-tli id="T698" />
            <conversion-tli id="T699" />
            <conversion-tli id="T700" />
            <conversion-tli id="T701" />
            <conversion-tli id="T702" />
            <conversion-tli id="T703" />
            <conversion-tli id="T704" />
            <conversion-tli id="T705" />
            <conversion-tli id="T706" />
            <conversion-tli id="T707" />
            <conversion-tli id="T708" />
            <conversion-tli id="T709" />
            <conversion-tli id="T710" />
            <conversion-tli id="T711" />
            <conversion-tli id="T712" />
            <conversion-tli id="T713" />
            <conversion-tli id="T714" />
            <conversion-tli id="T715" />
            <conversion-tli id="T716" />
            <conversion-tli id="T717" />
            <conversion-tli id="T718" />
            <conversion-tli id="T719" />
            <conversion-tli id="T720" />
            <conversion-tli id="T721" />
            <conversion-tli id="T722" />
            <conversion-tli id="T723" />
            <conversion-tli id="T724" />
            <conversion-tli id="T725" />
            <conversion-tli id="T726" />
            <conversion-tli id="T727" />
            <conversion-tli id="T728" />
            <conversion-tli id="T729" />
            <conversion-tli id="T730" />
            <conversion-tli id="T731" />
            <conversion-tli id="T732" />
            <conversion-tli id="T733" />
            <conversion-tli id="T734" />
            <conversion-tli id="T735" />
            <conversion-tli id="T736" />
            <conversion-tli id="T737" />
            <conversion-tli id="T738" />
            <conversion-tli id="T739" />
            <conversion-tli id="T740" />
            <conversion-tli id="T741" />
            <conversion-tli id="T742" />
            <conversion-tli id="T743" />
            <conversion-tli id="T744" />
            <conversion-tli id="T745" />
            <conversion-tli id="T746" />
            <conversion-tli id="T747" />
            <conversion-tli id="T748" />
            <conversion-tli id="T749" />
            <conversion-tli id="T750" />
            <conversion-tli id="T751" />
            <conversion-tli id="T752" />
            <conversion-tli id="T753" />
            <conversion-tli id="T754" />
            <conversion-tli id="T755" />
            <conversion-tli id="T756" />
            <conversion-tli id="T757" />
            <conversion-tli id="T758" />
            <conversion-tli id="T759" />
            <conversion-tli id="T760" />
            <conversion-tli id="T761" />
            <conversion-tli id="T762" />
            <conversion-tli id="T763" />
            <conversion-tli id="T764" />
            <conversion-tli id="T765" />
            <conversion-tli id="T766" />
            <conversion-tli id="T767" />
            <conversion-tli id="T768" />
            <conversion-tli id="T769" />
            <conversion-tli id="T770" />
            <conversion-tli id="T771" />
            <conversion-tli id="T772" />
            <conversion-tli id="T773" />
            <conversion-tli id="T774" />
            <conversion-tli id="T775" />
            <conversion-tli id="T776" />
            <conversion-tli id="T777" />
            <conversion-tli id="T778" />
            <conversion-tli id="T779" />
            <conversion-tli id="T780" />
            <conversion-tli id="T781" />
            <conversion-tli id="T782" />
            <conversion-tli id="T783" />
            <conversion-tli id="T784" />
            <conversion-tli id="T785" />
            <conversion-tli id="T786" />
            <conversion-tli id="T787" />
            <conversion-tli id="T788" />
            <conversion-tli id="T789" />
            <conversion-tli id="T790" />
            <conversion-tli id="T791" />
            <conversion-tli id="T792" />
            <conversion-tli id="T793" />
            <conversion-tli id="T794" />
            <conversion-tli id="T795" />
            <conversion-tli id="T796" />
            <conversion-tli id="T797" />
            <conversion-tli id="T798" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg"
                          name="gg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top"
                          name="Top"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc"
                          name="Foc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
