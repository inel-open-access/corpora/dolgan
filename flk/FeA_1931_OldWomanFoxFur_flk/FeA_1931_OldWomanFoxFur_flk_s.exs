<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID61624967-CC19-87C2-5A39-F728FBF0DAC3">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>FeA_1931_OldWomanFoxFur_flk</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\flk\FeA_1931_OldWomanFoxFur_flk\FeA_1931_OldWomanFoxFur_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">388</ud-information>
            <ud-information attribute-name="# HIAT:w">278</ud-information>
            <ud-information attribute-name="# e">277</ud-information>
            <ud-information attribute-name="# HIAT:u">43</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="FeA">
            <abbreviation>FeA</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="4.0" type="appl" />
         <tli id="T1" time="4.5" type="appl" />
         <tli id="T2" time="5.0" type="appl" />
         <tli id="T3" time="5.5" type="appl" />
         <tli id="T4" time="6.0" type="appl" />
         <tli id="T5" time="6.5" type="appl" />
         <tli id="T6" time="7.0" type="appl" />
         <tli id="T7" time="7.5" type="appl" />
         <tli id="T8" time="8.0" type="appl" />
         <tli id="T9" time="8.5" type="appl" />
         <tli id="T10" time="9.0" type="appl" />
         <tli id="T11" time="9.5" type="appl" />
         <tli id="T12" time="10.0" type="appl" />
         <tli id="T13" time="10.5" type="appl" />
         <tli id="T14" time="11.0" type="appl" />
         <tli id="T15" time="11.5" type="appl" />
         <tli id="T16" time="12.0" type="appl" />
         <tli id="T17" time="12.5" type="appl" />
         <tli id="T18" time="13.0" type="appl" />
         <tli id="T19" time="13.5" type="appl" />
         <tli id="T20" time="14.0" type="appl" />
         <tli id="T21" time="14.5" type="appl" />
         <tli id="T22" time="15.0" type="appl" />
         <tli id="T23" time="15.5" type="appl" />
         <tli id="T24" time="16.0" type="appl" />
         <tli id="T25" time="16.5" type="appl" />
         <tli id="T26" time="17.0" type="appl" />
         <tli id="T27" time="17.5" type="appl" />
         <tli id="T28" time="18.0" type="appl" />
         <tli id="T29" time="18.5" type="appl" />
         <tli id="T30" time="19.0" type="appl" />
         <tli id="T31" time="19.5" type="appl" />
         <tli id="T32" time="20.0" type="appl" />
         <tli id="T33" time="20.5" type="appl" />
         <tli id="T34" time="21.0" type="appl" />
         <tli id="T35" time="21.5" type="appl" />
         <tli id="T36" time="22.0" type="appl" />
         <tli id="T37" time="22.5" type="appl" />
         <tli id="T38" time="23.0" type="appl" />
         <tli id="T39" time="23.5" type="appl" />
         <tli id="T40" time="24.0" type="appl" />
         <tli id="T41" time="24.5" type="appl" />
         <tli id="T42" time="25.0" type="appl" />
         <tli id="T43" time="25.5" type="appl" />
         <tli id="T44" time="26.0" type="appl" />
         <tli id="T45" time="26.5" type="appl" />
         <tli id="T46" time="27.0" type="appl" />
         <tli id="T47" time="27.5" type="appl" />
         <tli id="T48" time="28.0" type="appl" />
         <tli id="T49" time="28.5" type="appl" />
         <tli id="T50" time="29.0" type="appl" />
         <tli id="T51" time="29.5" type="appl" />
         <tli id="T52" time="30.0" type="appl" />
         <tli id="T53" time="30.5" type="appl" />
         <tli id="T54" time="31.0" type="appl" />
         <tli id="T55" time="31.5" type="appl" />
         <tli id="T56" time="32.0" type="appl" />
         <tli id="T57" time="32.5" type="appl" />
         <tli id="T58" time="33.0" type="appl" />
         <tli id="T59" time="33.5" type="appl" />
         <tli id="T60" time="34.0" type="appl" />
         <tli id="T61" time="34.5" type="appl" />
         <tli id="T62" time="35.0" type="appl" />
         <tli id="T63" time="35.5" type="appl" />
         <tli id="T64" time="36.0" type="appl" />
         <tli id="T65" time="36.5" type="appl" />
         <tli id="T66" time="37.0" type="appl" />
         <tli id="T67" time="37.5" type="appl" />
         <tli id="T68" time="38.0" type="appl" />
         <tli id="T69" time="38.5" type="appl" />
         <tli id="T70" time="39.0" type="appl" />
         <tli id="T71" time="39.5" type="appl" />
         <tli id="T72" time="40.0" type="appl" />
         <tli id="T73" time="40.5" type="appl" />
         <tli id="T74" time="41.0" type="appl" />
         <tli id="T75" time="41.5" type="appl" />
         <tli id="T76" time="42.0" type="appl" />
         <tli id="T77" time="42.5" type="appl" />
         <tli id="T78" time="43.0" type="appl" />
         <tli id="T79" time="43.5" type="appl" />
         <tli id="T80" time="44.0" type="appl" />
         <tli id="T81" time="44.5" type="appl" />
         <tli id="T82" time="45.0" type="appl" />
         <tli id="T83" time="45.5" type="appl" />
         <tli id="T84" time="46.0" type="appl" />
         <tli id="T85" time="46.5" type="appl" />
         <tli id="T86" time="47.0" type="appl" />
         <tli id="T87" time="47.5" type="appl" />
         <tli id="T88" time="48.0" type="appl" />
         <tli id="T89" time="48.5" type="appl" />
         <tli id="T90" time="49.0" type="appl" />
         <tli id="T91" time="49.5" type="appl" />
         <tli id="T92" time="50.0" type="appl" />
         <tli id="T93" time="50.5" type="appl" />
         <tli id="T94" time="51.0" type="appl" />
         <tli id="T95" time="51.5" type="appl" />
         <tli id="T96" time="52.0" type="appl" />
         <tli id="T97" time="52.5" type="appl" />
         <tli id="T98" time="53.0" type="appl" />
         <tli id="T99" time="53.5" type="appl" />
         <tli id="T100" time="54.0" type="appl" />
         <tli id="T101" time="54.5" type="appl" />
         <tli id="T102" time="55.0" type="appl" />
         <tli id="T103" time="55.5" type="appl" />
         <tli id="T104" time="56.0" type="appl" />
         <tli id="T105" time="56.5" type="appl" />
         <tli id="T106" time="57.0" type="appl" />
         <tli id="T107" time="57.5" type="appl" />
         <tli id="T108" time="58.0" type="appl" />
         <tli id="T109" time="58.5" type="appl" />
         <tli id="T110" time="59.0" type="appl" />
         <tli id="T111" time="59.5" type="appl" />
         <tli id="T112" time="60.0" type="appl" />
         <tli id="T113" time="60.5" type="appl" />
         <tli id="T114" time="61.0" type="appl" />
         <tli id="T115" time="61.5" type="appl" />
         <tli id="T116" time="62.0" type="appl" />
         <tli id="T117" time="62.5" type="appl" />
         <tli id="T118" time="63.0" type="appl" />
         <tli id="T119" time="63.5" type="appl" />
         <tli id="T120" time="64.0" type="appl" />
         <tli id="T121" time="64.5" type="appl" />
         <tli id="T122" time="65.0" type="appl" />
         <tli id="T123" time="65.5" type="appl" />
         <tli id="T124" time="66.0" type="appl" />
         <tli id="T125" time="66.5" type="appl" />
         <tli id="T126" time="67.0" type="appl" />
         <tli id="T127" time="67.5" type="appl" />
         <tli id="T128" time="68.0" type="appl" />
         <tli id="T129" time="68.5" type="appl" />
         <tli id="T130" time="69.0" type="appl" />
         <tli id="T131" time="69.5" type="appl" />
         <tli id="T132" time="70.0" type="appl" />
         <tli id="T133" time="70.5" type="appl" />
         <tli id="T134" time="71.0" type="appl" />
         <tli id="T135" time="71.5" type="appl" />
         <tli id="T136" time="72.0" type="appl" />
         <tli id="T137" time="72.5" type="appl" />
         <tli id="T138" time="73.0" type="appl" />
         <tli id="T139" time="73.5" type="appl" />
         <tli id="T140" time="74.0" type="appl" />
         <tli id="T141" time="74.5" type="appl" />
         <tli id="T142" time="75.0" type="appl" />
         <tli id="T143" time="75.5" type="appl" />
         <tli id="T144" time="76.0" type="appl" />
         <tli id="T145" time="76.5" type="appl" />
         <tli id="T146" time="77.0" type="appl" />
         <tli id="T147" time="77.5" type="appl" />
         <tli id="T148" time="78.0" type="appl" />
         <tli id="T149" time="78.5" type="appl" />
         <tli id="T150" time="79.0" type="appl" />
         <tli id="T151" time="79.5" type="appl" />
         <tli id="T152" time="80.0" type="appl" />
         <tli id="T153" time="80.5" type="appl" />
         <tli id="T154" time="81.0" type="appl" />
         <tli id="T155" time="81.5" type="appl" />
         <tli id="T156" time="82.0" type="appl" />
         <tli id="T157" time="82.5" type="appl" />
         <tli id="T158" time="83.0" type="appl" />
         <tli id="T159" time="83.5" type="appl" />
         <tli id="T160" time="84.0" type="appl" />
         <tli id="T161" time="84.5" type="appl" />
         <tli id="T162" time="85.0" type="appl" />
         <tli id="T163" time="85.5" type="appl" />
         <tli id="T164" time="86.0" type="appl" />
         <tli id="T165" time="86.5" type="appl" />
         <tli id="T166" time="87.0" type="appl" />
         <tli id="T167" time="87.5" type="appl" />
         <tli id="T168" time="88.0" type="appl" />
         <tli id="T169" time="88.5" type="appl" />
         <tli id="T170" time="89.0" type="appl" />
         <tli id="T171" time="89.5" type="appl" />
         <tli id="T172" time="90.0" type="appl" />
         <tli id="T173" time="90.5" type="appl" />
         <tli id="T174" time="91.0" type="appl" />
         <tli id="T175" time="91.5" type="appl" />
         <tli id="T176" time="92.0" type="appl" />
         <tli id="T177" time="92.5" type="appl" />
         <tli id="T178" time="93.0" type="appl" />
         <tli id="T179" time="93.5" type="appl" />
         <tli id="T180" time="94.0" type="appl" />
         <tli id="T181" time="94.5" type="appl" />
         <tli id="T182" time="95.0" type="appl" />
         <tli id="T183" time="95.5" type="appl" />
         <tli id="T184" time="96.0" type="appl" />
         <tli id="T185" time="96.5" type="appl" />
         <tli id="T186" time="97.0" type="appl" />
         <tli id="T187" time="97.5" type="appl" />
         <tli id="T188" time="98.0" type="appl" />
         <tli id="T189" time="98.5" type="appl" />
         <tli id="T190" time="99.0" type="appl" />
         <tli id="T191" time="99.5" type="appl" />
         <tli id="T192" time="100.0" type="appl" />
         <tli id="T193" time="100.5" type="appl" />
         <tli id="T194" time="101.0" type="appl" />
         <tli id="T195" time="101.5" type="appl" />
         <tli id="T196" time="102.0" type="appl" />
         <tli id="T197" time="102.5" type="appl" />
         <tli id="T198" time="103.0" type="appl" />
         <tli id="T199" time="103.5" type="appl" />
         <tli id="T200" time="104.0" type="appl" />
         <tli id="T201" time="104.5" type="appl" />
         <tli id="T202" time="105.0" type="appl" />
         <tli id="T203" time="105.5" type="appl" />
         <tli id="T204" time="106.0" type="appl" />
         <tli id="T205" time="106.5" type="appl" />
         <tli id="T206" time="107.0" type="appl" />
         <tli id="T207" time="107.5" type="appl" />
         <tli id="T208" time="108.0" type="appl" />
         <tli id="T209" time="108.5" type="appl" />
         <tli id="T210" time="109.0" type="appl" />
         <tli id="T211" time="109.5" type="appl" />
         <tli id="T212" time="110.0" type="appl" />
         <tli id="T213" time="110.5" type="appl" />
         <tli id="T214" time="111.0" type="appl" />
         <tli id="T215" time="111.5" type="appl" />
         <tli id="T216" time="112.0" type="appl" />
         <tli id="T217" time="112.5" type="appl" />
         <tli id="T218" time="113.0" type="appl" />
         <tli id="T219" time="113.5" type="appl" />
         <tli id="T220" time="114.0" type="appl" />
         <tli id="T221" time="114.5" type="appl" />
         <tli id="T222" time="115.0" type="appl" />
         <tli id="T223" time="115.5" type="appl" />
         <tli id="T224" time="116.0" type="appl" />
         <tli id="T225" time="116.5" type="appl" />
         <tli id="T226" time="117.0" type="appl" />
         <tli id="T227" time="117.5" type="appl" />
         <tli id="T228" time="118.0" type="appl" />
         <tli id="T229" time="118.5" type="appl" />
         <tli id="T230" time="119.0" type="appl" />
         <tli id="T231" time="119.5" type="appl" />
         <tli id="T232" time="120.0" type="appl" />
         <tli id="T233" time="120.5" type="appl" />
         <tli id="T234" time="121.0" type="appl" />
         <tli id="T235" time="121.5" type="appl" />
         <tli id="T236" time="122.0" type="appl" />
         <tli id="T237" time="122.5" type="appl" />
         <tli id="T238" time="123.0" type="appl" />
         <tli id="T239" time="123.5" type="appl" />
         <tli id="T240" time="124.0" type="appl" />
         <tli id="T241" time="124.5" type="appl" />
         <tli id="T242" time="125.0" type="appl" />
         <tli id="T243" time="125.5" type="appl" />
         <tli id="T244" time="126.0" type="appl" />
         <tli id="T245" time="126.5" type="appl" />
         <tli id="T246" time="127.0" type="appl" />
         <tli id="T247" time="127.5" type="appl" />
         <tli id="T248" time="128.0" type="appl" />
         <tli id="T249" time="128.5" type="appl" />
         <tli id="T250" time="129.0" type="appl" />
         <tli id="T251" time="129.5" type="appl" />
         <tli id="T252" time="130.0" type="appl" />
         <tli id="T253" time="130.5" type="appl" />
         <tli id="T254" time="131.0" type="appl" />
         <tli id="T255" time="131.5" type="appl" />
         <tli id="T256" time="132.0" type="appl" />
         <tli id="T257" time="132.5" type="appl" />
         <tli id="T258" time="133.0" type="appl" />
         <tli id="T259" time="133.5" type="appl" />
         <tli id="T260" time="134.0" type="appl" />
         <tli id="T261" time="134.5" type="appl" />
         <tli id="T262" time="135.0" type="appl" />
         <tli id="T263" time="135.5" type="appl" />
         <tli id="T264" time="136.0" type="appl" />
         <tli id="T265" time="136.5" type="appl" />
         <tli id="T266" time="137.0" type="appl" />
         <tli id="T267" time="137.5" type="appl" />
         <tli id="T268" time="138.0" type="appl" />
         <tli id="T269" time="138.5" type="appl" />
         <tli id="T270" time="139.0" type="appl" />
         <tli id="T271" time="139.5" type="appl" />
         <tli id="T272" time="140.0" type="appl" />
         <tli id="T273" time="140.5" type="appl" />
         <tli id="T274" time="141.0" type="appl" />
         <tli id="T275" time="141.5" type="appl" />
         <tli id="T276" time="142.0" type="appl" />
         <tli id="T277" time="142.5" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="FeA"
                      type="t">
         <timeline-fork end="T183" start="T182">
            <tli id="T182.tx.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T277" id="Seg_0" n="sc" s="T0">
               <ts e="T9" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Biːr</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">hahɨl</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">arɨːga</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">kaːttarbɨt</ts>
                  <nts id="Seg_14" n="HIAT:ip">,</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_17" n="HIAT:w" s="T4">kanan</ts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_20" n="HIAT:w" s="T5">da</ts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_23" n="HIAT:w" s="T6">karbaːn</ts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_26" n="HIAT:w" s="T7">taksɨ͡agɨn</ts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_29" n="HIAT:w" s="T8">hu͡ok</ts>
                  <nts id="Seg_30" n="HIAT:ip">.</nts>
                  <nts id="Seg_31" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T12" id="Seg_33" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_35" n="HIAT:w" s="T9">Bu</ts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_38" n="HIAT:w" s="T10">hahɨlɨŋ</ts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_41" n="HIAT:w" s="T11">ɨtaːta</ts>
                  <nts id="Seg_42" n="HIAT:ip">:</nts>
                  <nts id="Seg_43" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T27" id="Seg_45" n="HIAT:u" s="T12">
                  <nts id="Seg_46" n="HIAT:ip">"</nts>
                  <ts e="T13" id="Seg_48" n="HIAT:w" s="T12">Bu</ts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_51" n="HIAT:w" s="T13">daganɨ</ts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_54" n="HIAT:w" s="T14">arɨːga</ts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_57" n="HIAT:w" s="T15">kaːttardakpɨn</ts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_60" n="HIAT:w" s="T16">abatɨn</ts>
                  <nts id="Seg_61" n="HIAT:ip">,</nts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_64" n="HIAT:w" s="T17">bu</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_67" n="HIAT:w" s="T18">arɨːga</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_70" n="HIAT:w" s="T19">kaːttarbatɨm</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_73" n="HIAT:w" s="T20">bu͡ollar</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_76" n="HIAT:w" s="T21">bu</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_79" n="HIAT:w" s="T22">dulgaːnnar</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_82" n="HIAT:w" s="T23">ajalarɨn</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_85" n="HIAT:w" s="T24">kirsilerin</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_88" n="HIAT:w" s="T25">hi͡eg</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_91" n="HIAT:w" s="T26">etim</ts>
                  <nts id="Seg_92" n="HIAT:ip">.</nts>
                  <nts id="Seg_93" n="HIAT:ip">"</nts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T35" id="Seg_96" n="HIAT:u" s="T27">
                  <ts e="T28" id="Seg_98" n="HIAT:w" s="T27">Manɨga</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_101" n="HIAT:w" s="T28">uː</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_104" n="HIAT:w" s="T29">ihiger</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_107" n="HIAT:w" s="T30">haŋa</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_110" n="HIAT:w" s="T31">ihillibit</ts>
                  <nts id="Seg_111" n="HIAT:ip">,</nts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_114" n="HIAT:w" s="T32">araj</ts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_117" n="HIAT:w" s="T33">hɨ͡alɨhardar</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_120" n="HIAT:w" s="T34">haŋarbɨttar</ts>
                  <nts id="Seg_121" n="HIAT:ip">:</nts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T41" id="Seg_124" n="HIAT:u" s="T35">
                  <nts id="Seg_125" n="HIAT:ip">"</nts>
                  <ts e="T36" id="Seg_127" n="HIAT:w" s="T35">Aː</ts>
                  <nts id="Seg_128" n="HIAT:ip">,</nts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_131" n="HIAT:w" s="T36">ogolor</ts>
                  <nts id="Seg_132" n="HIAT:ip">,</nts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_135" n="HIAT:w" s="T37">hahɨlbɨt</ts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_138" n="HIAT:w" s="T38">ɨtɨːr</ts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_141" n="HIAT:w" s="T39">ebit</ts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_144" n="HIAT:w" s="T40">duː</ts>
                  <nts id="Seg_145" n="HIAT:ip">?</nts>
                  <nts id="Seg_146" n="HIAT:ip">"</nts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T46" id="Seg_149" n="HIAT:u" s="T41">
                  <ts e="T42" id="Seg_151" n="HIAT:w" s="T41">Bu</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_154" n="HIAT:w" s="T42">manɨ</ts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_157" n="HIAT:w" s="T43">isten</ts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_160" n="HIAT:w" s="T44">hahɨl</ts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_163" n="HIAT:w" s="T45">di͡ete</ts>
                  <nts id="Seg_164" n="HIAT:ip">:</nts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T49" id="Seg_167" n="HIAT:u" s="T46">
                  <nts id="Seg_168" n="HIAT:ip">"</nts>
                  <ts e="T47" id="Seg_170" n="HIAT:w" s="T46">Hɨ͡alɨhar</ts>
                  <nts id="Seg_171" n="HIAT:ip">"</nts>
                  <nts id="Seg_172" n="HIAT:ip">,</nts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_175" n="HIAT:w" s="T47">diːr</ts>
                  <nts id="Seg_176" n="HIAT:ip">,</nts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_178" n="HIAT:ip">"</nts>
                  <ts e="T49" id="Seg_180" n="HIAT:w" s="T48">hɨrsɨ͡ak</ts>
                  <nts id="Seg_181" n="HIAT:ip">!</nts>
                  <nts id="Seg_182" n="HIAT:ip">"</nts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T54" id="Seg_185" n="HIAT:u" s="T49">
                  <ts e="T50" id="Seg_187" n="HIAT:w" s="T49">Hɨ͡alɨhardar</ts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_190" n="HIAT:w" s="T50">emi͡e</ts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_192" n="HIAT:ip">"</nts>
                  <ts e="T52" id="Seg_194" n="HIAT:w" s="T51">dʼe</ts>
                  <nts id="Seg_195" n="HIAT:ip">,</nts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_198" n="HIAT:w" s="T52">hɨrsɨ͡ak</ts>
                  <nts id="Seg_199" n="HIAT:ip">"</nts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_202" n="HIAT:w" s="T53">di͡ebitter</ts>
                  <nts id="Seg_203" n="HIAT:ip">.</nts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T56" id="Seg_206" n="HIAT:u" s="T54">
                  <ts e="T55" id="Seg_208" n="HIAT:w" s="T54">Hahɨlɨŋ</ts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_211" n="HIAT:w" s="T55">di͡ete</ts>
                  <nts id="Seg_212" n="HIAT:ip">:</nts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T59" id="Seg_215" n="HIAT:u" s="T56">
                  <nts id="Seg_216" n="HIAT:ip">"</nts>
                  <ts e="T57" id="Seg_218" n="HIAT:w" s="T56">Čej</ts>
                  <nts id="Seg_219" n="HIAT:ip">,</nts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_222" n="HIAT:w" s="T57">kɨtɨlga</ts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_225" n="HIAT:w" s="T58">tiksiŋ</ts>
                  <nts id="Seg_226" n="HIAT:ip">!</nts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T63" id="Seg_229" n="HIAT:u" s="T59">
                  <ts e="T60" id="Seg_231" n="HIAT:w" s="T59">Kaskɨtɨj</ts>
                  <nts id="Seg_232" n="HIAT:ip">,</nts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_235" n="HIAT:w" s="T60">min</ts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_238" n="HIAT:w" s="T61">aːgɨ͡am</ts>
                  <nts id="Seg_239" n="HIAT:ip">"</nts>
                  <nts id="Seg_240" n="HIAT:ip">,</nts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_243" n="HIAT:w" s="T62">di͡ete</ts>
                  <nts id="Seg_244" n="HIAT:ip">.</nts>
                  <nts id="Seg_245" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T70" id="Seg_247" n="HIAT:u" s="T63">
                  <ts e="T64" id="Seg_249" n="HIAT:w" s="T63">Hahɨl</ts>
                  <nts id="Seg_250" n="HIAT:ip">,</nts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_253" n="HIAT:w" s="T64">kɨtɨlga</ts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_256" n="HIAT:w" s="T65">kelen</ts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_259" n="HIAT:w" s="T66">turbuttarɨgar</ts>
                  <nts id="Seg_260" n="HIAT:ip">,</nts>
                  <nts id="Seg_261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_263" n="HIAT:w" s="T67">ürdüleriger</ts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_266" n="HIAT:w" s="T68">ojo</ts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_269" n="HIAT:w" s="T69">hɨrɨtta</ts>
                  <nts id="Seg_270" n="HIAT:ip">.</nts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T79" id="Seg_273" n="HIAT:u" s="T70">
                  <ts e="T71" id="Seg_275" n="HIAT:w" s="T70">Hahɨl</ts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_278" n="HIAT:w" s="T71">aːgan</ts>
                  <nts id="Seg_279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_280" n="HIAT:ip">"</nts>
                  <ts e="T73" id="Seg_282" n="HIAT:w" s="T72">ugun</ts>
                  <nts id="Seg_283" n="HIAT:ip">,</nts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_286" n="HIAT:w" s="T73">ugun</ts>
                  <nts id="Seg_287" n="HIAT:ip">,</nts>
                  <nts id="Seg_288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_290" n="HIAT:w" s="T74">dʼanus</ts>
                  <nts id="Seg_291" n="HIAT:ip">"</nts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_294" n="HIAT:w" s="T75">di͡en</ts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_297" n="HIAT:w" s="T76">aksɨːtɨn</ts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_300" n="HIAT:w" s="T77">tɨla</ts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_303" n="HIAT:w" s="T78">iti</ts>
                  <nts id="Seg_304" n="HIAT:ip">.</nts>
                  <nts id="Seg_305" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T81" id="Seg_307" n="HIAT:u" s="T79">
                  <ts e="T80" id="Seg_309" n="HIAT:w" s="T79">Dʼe</ts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_312" n="HIAT:w" s="T80">barallar</ts>
                  <nts id="Seg_313" n="HIAT:ip">.</nts>
                  <nts id="Seg_314" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T88" id="Seg_316" n="HIAT:u" s="T81">
                  <ts e="T82" id="Seg_318" n="HIAT:w" s="T81">Hahɨlɨŋ</ts>
                  <nts id="Seg_319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_321" n="HIAT:w" s="T82">kɨtɨl</ts>
                  <nts id="Seg_322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_324" n="HIAT:w" s="T83">ustun</ts>
                  <nts id="Seg_325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_327" n="HIAT:w" s="T84">barbɨt</ts>
                  <nts id="Seg_328" n="HIAT:ip">,</nts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_331" n="HIAT:w" s="T85">balɨgɨŋ</ts>
                  <nts id="Seg_332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_334" n="HIAT:w" s="T86">uː</ts>
                  <nts id="Seg_335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_337" n="HIAT:w" s="T87">ustun</ts>
                  <nts id="Seg_338" n="HIAT:ip">.</nts>
                  <nts id="Seg_339" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T95" id="Seg_341" n="HIAT:u" s="T88">
                  <ts e="T89" id="Seg_343" n="HIAT:w" s="T88">Bu</ts>
                  <nts id="Seg_344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_346" n="HIAT:w" s="T89">hahɨlɨŋ</ts>
                  <nts id="Seg_347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_349" n="HIAT:w" s="T90">kötön</ts>
                  <nts id="Seg_350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_352" n="HIAT:w" s="T91">ihen</ts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_355" n="HIAT:w" s="T92">baran</ts>
                  <nts id="Seg_356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_357" n="HIAT:ip">"</nts>
                  <ts e="T94" id="Seg_359" n="HIAT:w" s="T93">hɨ͡alɨhardar</ts>
                  <nts id="Seg_360" n="HIAT:ip">"</nts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_363" n="HIAT:w" s="T94">di͡ebit</ts>
                  <nts id="Seg_364" n="HIAT:ip">.</nts>
                  <nts id="Seg_365" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T99" id="Seg_367" n="HIAT:u" s="T95">
                  <ts e="T96" id="Seg_369" n="HIAT:w" s="T95">Innitiger</ts>
                  <nts id="Seg_370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_371" n="HIAT:ip">"</nts>
                  <ts e="T97" id="Seg_373" n="HIAT:w" s="T96">u͡op</ts>
                  <nts id="Seg_374" n="HIAT:ip">"</nts>
                  <nts id="Seg_375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_377" n="HIAT:w" s="T97">di͡ebitter</ts>
                  <nts id="Seg_378" n="HIAT:ip">,</nts>
                  <nts id="Seg_379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_381" n="HIAT:w" s="T98">ku͡opputtar</ts>
                  <nts id="Seg_382" n="HIAT:ip">.</nts>
                  <nts id="Seg_383" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T108" id="Seg_385" n="HIAT:u" s="T99">
                  <nts id="Seg_386" n="HIAT:ip">"</nts>
                  <ts e="T100" id="Seg_388" n="HIAT:w" s="T99">Uː</ts>
                  <nts id="Seg_389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_391" n="HIAT:w" s="T100">daː</ts>
                  <nts id="Seg_392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_394" n="HIAT:w" s="T101">balɨga</ts>
                  <nts id="Seg_395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_397" n="HIAT:w" s="T102">bɨhɨja</ts>
                  <nts id="Seg_398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_400" n="HIAT:w" s="T103">daː</ts>
                  <nts id="Seg_401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_403" n="HIAT:w" s="T104">bert</ts>
                  <nts id="Seg_404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_406" n="HIAT:w" s="T105">bu͡olar</ts>
                  <nts id="Seg_407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_409" n="HIAT:w" s="T106">ebit</ts>
                  <nts id="Seg_410" n="HIAT:ip">"</nts>
                  <nts id="Seg_411" n="HIAT:ip">,</nts>
                  <nts id="Seg_412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_414" n="HIAT:w" s="T107">di͡ete</ts>
                  <nts id="Seg_415" n="HIAT:ip">.</nts>
                  <nts id="Seg_416" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T113" id="Seg_418" n="HIAT:u" s="T108">
                  <ts e="T109" id="Seg_420" n="HIAT:w" s="T108">Innʼe</ts>
                  <nts id="Seg_421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_423" n="HIAT:w" s="T109">gɨnan</ts>
                  <nts id="Seg_424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_426" n="HIAT:w" s="T110">baran</ts>
                  <nts id="Seg_427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_429" n="HIAT:w" s="T111">emi͡e</ts>
                  <nts id="Seg_430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_432" n="HIAT:w" s="T112">köttö</ts>
                  <nts id="Seg_433" n="HIAT:ip">.</nts>
                  <nts id="Seg_434" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T126" id="Seg_436" n="HIAT:u" s="T113">
                  <ts e="T114" id="Seg_438" n="HIAT:w" s="T113">Bu</ts>
                  <nts id="Seg_439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_441" n="HIAT:w" s="T114">ihen</ts>
                  <nts id="Seg_442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_444" n="HIAT:w" s="T115">biːr</ts>
                  <nts id="Seg_445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_447" n="HIAT:w" s="T116">hirge</ts>
                  <nts id="Seg_448" n="HIAT:ip">,</nts>
                  <nts id="Seg_449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_451" n="HIAT:w" s="T117">uː</ts>
                  <nts id="Seg_452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_454" n="HIAT:w" s="T118">iste</ts>
                  <nts id="Seg_455" n="HIAT:ip">,</nts>
                  <nts id="Seg_456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_458" n="HIAT:w" s="T119">bu</ts>
                  <nts id="Seg_459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_461" n="HIAT:w" s="T120">ihebin</ts>
                  <nts id="Seg_462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_464" n="HIAT:w" s="T121">di͡en</ts>
                  <nts id="Seg_465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_467" n="HIAT:w" s="T122">hahɨlɨŋ</ts>
                  <nts id="Seg_468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_470" n="HIAT:w" s="T123">tumnastan</ts>
                  <nts id="Seg_471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_473" n="HIAT:w" s="T124">ölön</ts>
                  <nts id="Seg_474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_476" n="HIAT:w" s="T125">kaːlla</ts>
                  <nts id="Seg_477" n="HIAT:ip">.</nts>
                  <nts id="Seg_478" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T140" id="Seg_480" n="HIAT:u" s="T126">
                  <ts e="T127" id="Seg_482" n="HIAT:w" s="T126">Biːr</ts>
                  <nts id="Seg_483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_485" n="HIAT:w" s="T127">emeːksin</ts>
                  <nts id="Seg_486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_488" n="HIAT:w" s="T128">uː</ts>
                  <nts id="Seg_489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_491" n="HIAT:w" s="T129">bahɨna</ts>
                  <nts id="Seg_492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_494" n="HIAT:w" s="T130">kiːrbit</ts>
                  <nts id="Seg_495" n="HIAT:ip">,</nts>
                  <nts id="Seg_496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_498" n="HIAT:w" s="T131">araj</ts>
                  <nts id="Seg_499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_501" n="HIAT:w" s="T132">bu</ts>
                  <nts id="Seg_502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_504" n="HIAT:w" s="T133">uː</ts>
                  <nts id="Seg_505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_507" n="HIAT:w" s="T134">baha</ts>
                  <nts id="Seg_508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_510" n="HIAT:w" s="T135">kiːrbite</ts>
                  <nts id="Seg_511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_513" n="HIAT:w" s="T136">hahɨl</ts>
                  <nts id="Seg_514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_516" n="HIAT:w" s="T137">ölö</ts>
                  <nts id="Seg_517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_519" n="HIAT:w" s="T138">hɨtarɨn</ts>
                  <nts id="Seg_520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_522" n="HIAT:w" s="T139">bulbut</ts>
                  <nts id="Seg_523" n="HIAT:ip">.</nts>
                  <nts id="Seg_524" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T150" id="Seg_526" n="HIAT:u" s="T140">
                  <ts e="T141" id="Seg_528" n="HIAT:w" s="T140">Bu</ts>
                  <nts id="Seg_529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_531" n="HIAT:w" s="T141">emeːksiniŋ</ts>
                  <nts id="Seg_532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_534" n="HIAT:w" s="T142">bu</ts>
                  <nts id="Seg_535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_537" n="HIAT:w" s="T143">hahɨlɨ</ts>
                  <nts id="Seg_538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_540" n="HIAT:w" s="T144">ildʼen</ts>
                  <nts id="Seg_541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_543" n="HIAT:w" s="T145">hülünne</ts>
                  <nts id="Seg_544" n="HIAT:ip">,</nts>
                  <nts id="Seg_545" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_547" n="HIAT:w" s="T146">bu</ts>
                  <nts id="Seg_548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_550" n="HIAT:w" s="T147">hülen</ts>
                  <nts id="Seg_551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_553" n="HIAT:w" s="T148">baran</ts>
                  <nts id="Seg_554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_556" n="HIAT:w" s="T149">kuːrta</ts>
                  <nts id="Seg_557" n="HIAT:ip">.</nts>
                  <nts id="Seg_558" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T155" id="Seg_560" n="HIAT:u" s="T150">
                  <ts e="T151" id="Seg_562" n="HIAT:w" s="T150">Bu</ts>
                  <nts id="Seg_563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_565" n="HIAT:w" s="T151">gɨnan</ts>
                  <nts id="Seg_566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_568" n="HIAT:w" s="T152">baran</ts>
                  <nts id="Seg_569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_571" n="HIAT:w" s="T153">araŋaska</ts>
                  <nts id="Seg_572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_574" n="HIAT:w" s="T154">ɨjaːta</ts>
                  <nts id="Seg_575" n="HIAT:ip">.</nts>
                  <nts id="Seg_576" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T161" id="Seg_578" n="HIAT:u" s="T155">
                  <ts e="T156" id="Seg_580" n="HIAT:w" s="T155">Bu</ts>
                  <nts id="Seg_581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_583" n="HIAT:w" s="T156">ɨjɨː</ts>
                  <nts id="Seg_584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_586" n="HIAT:w" s="T157">turan</ts>
                  <nts id="Seg_587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_589" n="HIAT:w" s="T158">haŋarda</ts>
                  <nts id="Seg_590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_592" n="HIAT:w" s="T159">bu</ts>
                  <nts id="Seg_593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_595" n="HIAT:w" s="T160">emmeksin</ts>
                  <nts id="Seg_596" n="HIAT:ip">:</nts>
                  <nts id="Seg_597" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T165" id="Seg_599" n="HIAT:u" s="T161">
                  <nts id="Seg_600" n="HIAT:ip">"</nts>
                  <ts e="T162" id="Seg_602" n="HIAT:w" s="T161">Hoguruːttan</ts>
                  <nts id="Seg_603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_605" n="HIAT:w" s="T162">tɨ͡al</ts>
                  <nts id="Seg_606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_608" n="HIAT:w" s="T163">bu͡ol</ts>
                  <nts id="Seg_609" n="HIAT:ip">"</nts>
                  <nts id="Seg_610" n="HIAT:ip">,</nts>
                  <nts id="Seg_611" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_613" n="HIAT:w" s="T164">di͡ete</ts>
                  <nts id="Seg_614" n="HIAT:ip">.</nts>
                  <nts id="Seg_615" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T169" id="Seg_617" n="HIAT:u" s="T165">
                  <nts id="Seg_618" n="HIAT:ip">"</nts>
                  <ts e="T166" id="Seg_620" n="HIAT:w" s="T165">Haːpattan</ts>
                  <nts id="Seg_621" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_623" n="HIAT:w" s="T166">tɨ͡al</ts>
                  <nts id="Seg_624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_626" n="HIAT:w" s="T167">bu͡ol</ts>
                  <nts id="Seg_627" n="HIAT:ip">"</nts>
                  <nts id="Seg_628" n="HIAT:ip">,</nts>
                  <nts id="Seg_629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_631" n="HIAT:w" s="T168">di͡ete</ts>
                  <nts id="Seg_632" n="HIAT:ip">.</nts>
                  <nts id="Seg_633" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T172" id="Seg_635" n="HIAT:u" s="T169">
                  <ts e="T170" id="Seg_637" n="HIAT:w" s="T169">Muntuta</ts>
                  <nts id="Seg_638" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_640" n="HIAT:w" s="T170">dʼe</ts>
                  <nts id="Seg_641" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_643" n="HIAT:w" s="T171">möŋtö</ts>
                  <nts id="Seg_644" n="HIAT:ip">.</nts>
                  <nts id="Seg_645" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T184" id="Seg_647" n="HIAT:u" s="T172">
                  <ts e="T173" id="Seg_649" n="HIAT:w" s="T172">Tɨ͡al</ts>
                  <nts id="Seg_650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_652" n="HIAT:w" s="T173">kelen</ts>
                  <nts id="Seg_653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_655" n="HIAT:w" s="T174">dʼe</ts>
                  <nts id="Seg_656" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_658" n="HIAT:w" s="T175">bu</ts>
                  <nts id="Seg_659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_661" n="HIAT:w" s="T176">hahɨlɨ</ts>
                  <nts id="Seg_662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_664" n="HIAT:w" s="T177">möktögüne</ts>
                  <nts id="Seg_665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_667" n="HIAT:w" s="T178">emeːksin</ts>
                  <nts id="Seg_668" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_670" n="HIAT:w" s="T179">hahɨlɨn</ts>
                  <nts id="Seg_671" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_673" n="HIAT:w" s="T180">kɨtta</ts>
                  <nts id="Seg_674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_676" n="HIAT:w" s="T181">üŋküːleːte</ts>
                  <nts id="Seg_677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_678" n="HIAT:ip">"</nts>
                  <ts e="T182.tx.1" id="Seg_680" n="HIAT:w" s="T182">huːraŋki</ts>
                  <nts id="Seg_681" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_683" n="HIAT:w" s="T182.tx.1">haːraŋki</ts>
                  <nts id="Seg_684" n="HIAT:ip">"</nts>
                  <nts id="Seg_685" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_687" n="HIAT:w" s="T183">diː-diː</ts>
                  <nts id="Seg_688" n="HIAT:ip">.</nts>
                  <nts id="Seg_689" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T193" id="Seg_691" n="HIAT:u" s="T184">
                  <ts e="T185" id="Seg_693" n="HIAT:w" s="T184">Bu</ts>
                  <nts id="Seg_694" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_696" n="HIAT:w" s="T185">gɨna</ts>
                  <nts id="Seg_697" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_699" n="HIAT:w" s="T186">turdagɨna</ts>
                  <nts id="Seg_700" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_702" n="HIAT:w" s="T187">hahɨlɨn</ts>
                  <nts id="Seg_703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_705" n="HIAT:w" s="T188">tɨ͡ala</ts>
                  <nts id="Seg_706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_708" n="HIAT:w" s="T189">bɨha</ts>
                  <nts id="Seg_709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_711" n="HIAT:w" s="T190">okson</ts>
                  <nts id="Seg_712" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_714" n="HIAT:w" s="T191">ildʼen</ts>
                  <nts id="Seg_715" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_717" n="HIAT:w" s="T192">keːste</ts>
                  <nts id="Seg_718" n="HIAT:ip">.</nts>
                  <nts id="Seg_719" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T199" id="Seg_721" n="HIAT:u" s="T193">
                  <ts e="T194" id="Seg_723" n="HIAT:w" s="T193">Bu</ts>
                  <nts id="Seg_724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_726" n="HIAT:w" s="T194">emeːksin</ts>
                  <nts id="Seg_727" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_729" n="HIAT:w" s="T195">bu</ts>
                  <nts id="Seg_730" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_732" n="HIAT:w" s="T196">hahɨlɨn</ts>
                  <nts id="Seg_733" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_735" n="HIAT:w" s="T197">dʼe</ts>
                  <nts id="Seg_736" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_738" n="HIAT:w" s="T198">batta</ts>
                  <nts id="Seg_739" n="HIAT:ip">.</nts>
                  <nts id="Seg_740" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T205" id="Seg_742" n="HIAT:u" s="T199">
                  <ts e="T200" id="Seg_744" n="HIAT:w" s="T199">Bu</ts>
                  <nts id="Seg_745" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_747" n="HIAT:w" s="T200">batan</ts>
                  <nts id="Seg_748" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_750" n="HIAT:w" s="T201">ihen</ts>
                  <nts id="Seg_751" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_753" n="HIAT:w" s="T202">bu</ts>
                  <nts id="Seg_754" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_756" n="HIAT:w" s="T203">emeːksin</ts>
                  <nts id="Seg_757" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_759" n="HIAT:w" s="T204">di͡ete</ts>
                  <nts id="Seg_760" n="HIAT:ip">:</nts>
                  <nts id="Seg_761" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T215" id="Seg_763" n="HIAT:u" s="T205">
                  <nts id="Seg_764" n="HIAT:ip">"</nts>
                  <ts e="T206" id="Seg_766" n="HIAT:w" s="T205">Bu</ts>
                  <nts id="Seg_767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_769" n="HIAT:w" s="T206">emijim</ts>
                  <nts id="Seg_770" n="HIAT:ip">,</nts>
                  <nts id="Seg_771" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_773" n="HIAT:w" s="T207">bɨhɨːta</ts>
                  <nts id="Seg_774" n="HIAT:ip">,</nts>
                  <nts id="Seg_775" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_777" n="HIAT:w" s="T208">ɨ͡arakan</ts>
                  <nts id="Seg_778" n="HIAT:ip">,</nts>
                  <nts id="Seg_779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_781" n="HIAT:w" s="T209">battɨːr</ts>
                  <nts id="Seg_782" n="HIAT:ip">,</nts>
                  <nts id="Seg_783" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_785" n="HIAT:w" s="T210">bɨhɨːta</ts>
                  <nts id="Seg_786" n="HIAT:ip">"</nts>
                  <nts id="Seg_787" n="HIAT:ip">,</nts>
                  <nts id="Seg_788" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_790" n="HIAT:w" s="T211">di͡en</ts>
                  <nts id="Seg_791" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_793" n="HIAT:w" s="T212">emijin</ts>
                  <nts id="Seg_794" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_796" n="HIAT:w" s="T213">bɨhan</ts>
                  <nts id="Seg_797" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_799" n="HIAT:w" s="T214">keːspite</ts>
                  <nts id="Seg_800" n="HIAT:ip">.</nts>
                  <nts id="Seg_801" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T220" id="Seg_803" n="HIAT:u" s="T215">
                  <ts e="T216" id="Seg_805" n="HIAT:w" s="T215">Bu</ts>
                  <nts id="Seg_806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_808" n="HIAT:w" s="T216">gɨnan</ts>
                  <nts id="Seg_809" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_811" n="HIAT:w" s="T217">baran</ts>
                  <nts id="Seg_812" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_814" n="HIAT:w" s="T218">emi͡e</ts>
                  <nts id="Seg_815" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_817" n="HIAT:w" s="T219">barbɨta</ts>
                  <nts id="Seg_818" n="HIAT:ip">.</nts>
                  <nts id="Seg_819" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T232" id="Seg_821" n="HIAT:u" s="T220">
                  <nts id="Seg_822" n="HIAT:ip">"</nts>
                  <ts e="T221" id="Seg_824" n="HIAT:w" s="T220">Aŋaːr</ts>
                  <nts id="Seg_825" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_827" n="HIAT:w" s="T221">di͡egi</ts>
                  <nts id="Seg_828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_830" n="HIAT:w" s="T222">emijim</ts>
                  <nts id="Seg_831" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_833" n="HIAT:w" s="T223">emi͡e</ts>
                  <nts id="Seg_834" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_836" n="HIAT:w" s="T224">ɨ͡arakan</ts>
                  <nts id="Seg_837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_839" n="HIAT:w" s="T225">duː</ts>
                  <nts id="Seg_840" n="HIAT:ip">"</nts>
                  <nts id="Seg_841" n="HIAT:ip">,</nts>
                  <nts id="Seg_842" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_844" n="HIAT:w" s="T226">di͡et</ts>
                  <nts id="Seg_845" n="HIAT:ip">,</nts>
                  <nts id="Seg_846" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_848" n="HIAT:w" s="T227">bu</ts>
                  <nts id="Seg_849" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_851" n="HIAT:w" s="T228">emiji</ts>
                  <nts id="Seg_852" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_854" n="HIAT:w" s="T229">emi͡e</ts>
                  <nts id="Seg_855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_857" n="HIAT:w" s="T230">bɨhan</ts>
                  <nts id="Seg_858" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_860" n="HIAT:w" s="T231">keːspite</ts>
                  <nts id="Seg_861" n="HIAT:ip">.</nts>
                  <nts id="Seg_862" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T237" id="Seg_864" n="HIAT:u" s="T232">
                  <ts e="T233" id="Seg_866" n="HIAT:w" s="T232">Bu</ts>
                  <nts id="Seg_867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_869" n="HIAT:w" s="T233">gɨnan</ts>
                  <nts id="Seg_870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_872" n="HIAT:w" s="T234">baran</ts>
                  <nts id="Seg_873" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_875" n="HIAT:w" s="T235">emi͡e</ts>
                  <nts id="Seg_876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_878" n="HIAT:w" s="T236">barbɨta</ts>
                  <nts id="Seg_879" n="HIAT:ip">:</nts>
                  <nts id="Seg_880" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T248" id="Seg_882" n="HIAT:u" s="T237">
                  <nts id="Seg_883" n="HIAT:ip">"</nts>
                  <ts e="T238" id="Seg_885" n="HIAT:w" s="T237">Doː</ts>
                  <nts id="Seg_886" n="HIAT:ip">,</nts>
                  <nts id="Seg_887" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_889" n="HIAT:w" s="T238">bu</ts>
                  <nts id="Seg_890" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_892" n="HIAT:w" s="T239">aŋaːr</ts>
                  <nts id="Seg_893" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_895" n="HIAT:w" s="T240">iliːm</ts>
                  <nts id="Seg_896" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_898" n="HIAT:w" s="T241">mi͡ene</ts>
                  <nts id="Seg_899" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_901" n="HIAT:w" s="T242">ɨ͡arakan</ts>
                  <nts id="Seg_902" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_904" n="HIAT:w" s="T243">duː</ts>
                  <nts id="Seg_905" n="HIAT:ip">"</nts>
                  <nts id="Seg_906" n="HIAT:ip">,</nts>
                  <nts id="Seg_907" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_909" n="HIAT:w" s="T244">di͡en</ts>
                  <nts id="Seg_910" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_912" n="HIAT:w" s="T245">aŋaːr</ts>
                  <nts id="Seg_913" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_915" n="HIAT:w" s="T246">iliːtin</ts>
                  <nts id="Seg_916" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_918" n="HIAT:w" s="T247">bɨsta</ts>
                  <nts id="Seg_919" n="HIAT:ip">.</nts>
                  <nts id="Seg_920" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T251" id="Seg_922" n="HIAT:u" s="T248">
                  <ts e="T249" id="Seg_924" n="HIAT:w" s="T248">Dʼe</ts>
                  <nts id="Seg_925" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_927" n="HIAT:w" s="T249">emi͡e</ts>
                  <nts id="Seg_928" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_930" n="HIAT:w" s="T250">barda</ts>
                  <nts id="Seg_931" n="HIAT:ip">.</nts>
                  <nts id="Seg_932" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T256" id="Seg_934" n="HIAT:u" s="T251">
                  <ts e="T252" id="Seg_936" n="HIAT:w" s="T251">Bu</ts>
                  <nts id="Seg_937" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_939" n="HIAT:w" s="T252">emi͡e</ts>
                  <nts id="Seg_940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_942" n="HIAT:w" s="T253">aŋaːr</ts>
                  <nts id="Seg_943" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_945" n="HIAT:w" s="T254">iliːtin</ts>
                  <nts id="Seg_946" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_948" n="HIAT:w" s="T255">bɨsta</ts>
                  <nts id="Seg_949" n="HIAT:ip">.</nts>
                  <nts id="Seg_950" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T260" id="Seg_952" n="HIAT:u" s="T256">
                  <ts e="T257" id="Seg_954" n="HIAT:w" s="T256">Ikki</ts>
                  <nts id="Seg_955" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_957" n="HIAT:w" s="T257">iliːte</ts>
                  <nts id="Seg_958" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_960" n="HIAT:w" s="T258">hu͡ok</ts>
                  <nts id="Seg_961" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_963" n="HIAT:w" s="T259">bu͡olla</ts>
                  <nts id="Seg_964" n="HIAT:ip">.</nts>
                  <nts id="Seg_965" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T263" id="Seg_967" n="HIAT:u" s="T260">
                  <ts e="T261" id="Seg_969" n="HIAT:w" s="T260">Itigenne</ts>
                  <nts id="Seg_970" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_972" n="HIAT:w" s="T261">emeːksin</ts>
                  <nts id="Seg_973" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_975" n="HIAT:w" s="T262">eppite</ts>
                  <nts id="Seg_976" n="HIAT:ip">:</nts>
                  <nts id="Seg_977" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T272" id="Seg_979" n="HIAT:u" s="T263">
                  <nts id="Seg_980" n="HIAT:ip">"</nts>
                  <ts e="T264" id="Seg_982" n="HIAT:w" s="T263">Bu</ts>
                  <nts id="Seg_983" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_985" n="HIAT:w" s="T264">taŋara</ts>
                  <nts id="Seg_986" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_988" n="HIAT:w" s="T265">u͡olun</ts>
                  <nts id="Seg_989" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_991" n="HIAT:w" s="T266">gɨtta</ts>
                  <nts id="Seg_992" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_994" n="HIAT:w" s="T267">ugurahɨ͡akpɨn</ts>
                  <nts id="Seg_995" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_997" n="HIAT:w" s="T268">u͡ohum</ts>
                  <nts id="Seg_998" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_1000" n="HIAT:w" s="T269">ere</ts>
                  <nts id="Seg_1001" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_1003" n="HIAT:w" s="T270">bütün</ts>
                  <nts id="Seg_1004" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_1006" n="HIAT:w" s="T271">bu͡ollun</ts>
                  <nts id="Seg_1007" n="HIAT:ip">.</nts>
                  <nts id="Seg_1008" n="HIAT:ip">"</nts>
                  <nts id="Seg_1009" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T274" id="Seg_1011" n="HIAT:u" s="T272">
                  <ts e="T273" id="Seg_1013" n="HIAT:w" s="T272">Onton</ts>
                  <nts id="Seg_1014" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_1016" n="HIAT:w" s="T273">barbɨta</ts>
                  <nts id="Seg_1017" n="HIAT:ip">.</nts>
                  <nts id="Seg_1018" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T277" id="Seg_1020" n="HIAT:u" s="T274">
                  <ts e="T275" id="Seg_1022" n="HIAT:w" s="T274">Elete</ts>
                  <nts id="Seg_1023" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_1025" n="HIAT:w" s="T275">bu͡olan</ts>
                  <nts id="Seg_1026" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_1028" n="HIAT:w" s="T276">ölbüte</ts>
                  <nts id="Seg_1029" n="HIAT:ip">.</nts>
                  <nts id="Seg_1030" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T277" id="Seg_1031" n="sc" s="T0">
               <ts e="T1" id="Seg_1033" n="e" s="T0">Biːr </ts>
               <ts e="T2" id="Seg_1035" n="e" s="T1">hahɨl </ts>
               <ts e="T3" id="Seg_1037" n="e" s="T2">arɨːga </ts>
               <ts e="T4" id="Seg_1039" n="e" s="T3">kaːttarbɨt, </ts>
               <ts e="T5" id="Seg_1041" n="e" s="T4">kanan </ts>
               <ts e="T6" id="Seg_1043" n="e" s="T5">da </ts>
               <ts e="T7" id="Seg_1045" n="e" s="T6">karbaːn </ts>
               <ts e="T8" id="Seg_1047" n="e" s="T7">taksɨ͡agɨn </ts>
               <ts e="T9" id="Seg_1049" n="e" s="T8">hu͡ok. </ts>
               <ts e="T10" id="Seg_1051" n="e" s="T9">Bu </ts>
               <ts e="T11" id="Seg_1053" n="e" s="T10">hahɨlɨŋ </ts>
               <ts e="T12" id="Seg_1055" n="e" s="T11">ɨtaːta: </ts>
               <ts e="T13" id="Seg_1057" n="e" s="T12">"Bu </ts>
               <ts e="T14" id="Seg_1059" n="e" s="T13">daganɨ </ts>
               <ts e="T15" id="Seg_1061" n="e" s="T14">arɨːga </ts>
               <ts e="T16" id="Seg_1063" n="e" s="T15">kaːttardakpɨn </ts>
               <ts e="T17" id="Seg_1065" n="e" s="T16">abatɨn, </ts>
               <ts e="T18" id="Seg_1067" n="e" s="T17">bu </ts>
               <ts e="T19" id="Seg_1069" n="e" s="T18">arɨːga </ts>
               <ts e="T20" id="Seg_1071" n="e" s="T19">kaːttarbatɨm </ts>
               <ts e="T21" id="Seg_1073" n="e" s="T20">bu͡ollar </ts>
               <ts e="T22" id="Seg_1075" n="e" s="T21">bu </ts>
               <ts e="T23" id="Seg_1077" n="e" s="T22">dulgaːnnar </ts>
               <ts e="T24" id="Seg_1079" n="e" s="T23">ajalarɨn </ts>
               <ts e="T25" id="Seg_1081" n="e" s="T24">kirsilerin </ts>
               <ts e="T26" id="Seg_1083" n="e" s="T25">hi͡eg </ts>
               <ts e="T27" id="Seg_1085" n="e" s="T26">etim." </ts>
               <ts e="T28" id="Seg_1087" n="e" s="T27">Manɨga </ts>
               <ts e="T29" id="Seg_1089" n="e" s="T28">uː </ts>
               <ts e="T30" id="Seg_1091" n="e" s="T29">ihiger </ts>
               <ts e="T31" id="Seg_1093" n="e" s="T30">haŋa </ts>
               <ts e="T32" id="Seg_1095" n="e" s="T31">ihillibit, </ts>
               <ts e="T33" id="Seg_1097" n="e" s="T32">araj </ts>
               <ts e="T34" id="Seg_1099" n="e" s="T33">hɨ͡alɨhardar </ts>
               <ts e="T35" id="Seg_1101" n="e" s="T34">haŋarbɨttar: </ts>
               <ts e="T36" id="Seg_1103" n="e" s="T35">"Aː, </ts>
               <ts e="T37" id="Seg_1105" n="e" s="T36">ogolor, </ts>
               <ts e="T38" id="Seg_1107" n="e" s="T37">hahɨlbɨt </ts>
               <ts e="T39" id="Seg_1109" n="e" s="T38">ɨtɨːr </ts>
               <ts e="T40" id="Seg_1111" n="e" s="T39">ebit </ts>
               <ts e="T41" id="Seg_1113" n="e" s="T40">duː?" </ts>
               <ts e="T42" id="Seg_1115" n="e" s="T41">Bu </ts>
               <ts e="T43" id="Seg_1117" n="e" s="T42">manɨ </ts>
               <ts e="T44" id="Seg_1119" n="e" s="T43">isten </ts>
               <ts e="T45" id="Seg_1121" n="e" s="T44">hahɨl </ts>
               <ts e="T46" id="Seg_1123" n="e" s="T45">di͡ete: </ts>
               <ts e="T47" id="Seg_1125" n="e" s="T46">"Hɨ͡alɨhar", </ts>
               <ts e="T48" id="Seg_1127" n="e" s="T47">diːr, </ts>
               <ts e="T49" id="Seg_1129" n="e" s="T48">"hɨrsɨ͡ak!" </ts>
               <ts e="T50" id="Seg_1131" n="e" s="T49">Hɨ͡alɨhardar </ts>
               <ts e="T51" id="Seg_1133" n="e" s="T50">emi͡e </ts>
               <ts e="T52" id="Seg_1135" n="e" s="T51">"dʼe, </ts>
               <ts e="T53" id="Seg_1137" n="e" s="T52">hɨrsɨ͡ak" </ts>
               <ts e="T54" id="Seg_1139" n="e" s="T53">di͡ebitter. </ts>
               <ts e="T55" id="Seg_1141" n="e" s="T54">Hahɨlɨŋ </ts>
               <ts e="T56" id="Seg_1143" n="e" s="T55">di͡ete: </ts>
               <ts e="T57" id="Seg_1145" n="e" s="T56">"Čej, </ts>
               <ts e="T58" id="Seg_1147" n="e" s="T57">kɨtɨlga </ts>
               <ts e="T59" id="Seg_1149" n="e" s="T58">tiksiŋ! </ts>
               <ts e="T60" id="Seg_1151" n="e" s="T59">Kaskɨtɨj, </ts>
               <ts e="T61" id="Seg_1153" n="e" s="T60">min </ts>
               <ts e="T62" id="Seg_1155" n="e" s="T61">aːgɨ͡am", </ts>
               <ts e="T63" id="Seg_1157" n="e" s="T62">di͡ete. </ts>
               <ts e="T64" id="Seg_1159" n="e" s="T63">Hahɨl, </ts>
               <ts e="T65" id="Seg_1161" n="e" s="T64">kɨtɨlga </ts>
               <ts e="T66" id="Seg_1163" n="e" s="T65">kelen </ts>
               <ts e="T67" id="Seg_1165" n="e" s="T66">turbuttarɨgar, </ts>
               <ts e="T68" id="Seg_1167" n="e" s="T67">ürdüleriger </ts>
               <ts e="T69" id="Seg_1169" n="e" s="T68">ojo </ts>
               <ts e="T70" id="Seg_1171" n="e" s="T69">hɨrɨtta. </ts>
               <ts e="T71" id="Seg_1173" n="e" s="T70">Hahɨl </ts>
               <ts e="T72" id="Seg_1175" n="e" s="T71">aːgan </ts>
               <ts e="T73" id="Seg_1177" n="e" s="T72">"ugun, </ts>
               <ts e="T74" id="Seg_1179" n="e" s="T73">ugun, </ts>
               <ts e="T75" id="Seg_1181" n="e" s="T74">dʼanus" </ts>
               <ts e="T76" id="Seg_1183" n="e" s="T75">di͡en </ts>
               <ts e="T77" id="Seg_1185" n="e" s="T76">aksɨːtɨn </ts>
               <ts e="T78" id="Seg_1187" n="e" s="T77">tɨla </ts>
               <ts e="T79" id="Seg_1189" n="e" s="T78">iti. </ts>
               <ts e="T80" id="Seg_1191" n="e" s="T79">Dʼe </ts>
               <ts e="T81" id="Seg_1193" n="e" s="T80">barallar. </ts>
               <ts e="T82" id="Seg_1195" n="e" s="T81">Hahɨlɨŋ </ts>
               <ts e="T83" id="Seg_1197" n="e" s="T82">kɨtɨl </ts>
               <ts e="T84" id="Seg_1199" n="e" s="T83">ustun </ts>
               <ts e="T85" id="Seg_1201" n="e" s="T84">barbɨt, </ts>
               <ts e="T86" id="Seg_1203" n="e" s="T85">balɨgɨŋ </ts>
               <ts e="T87" id="Seg_1205" n="e" s="T86">uː </ts>
               <ts e="T88" id="Seg_1207" n="e" s="T87">ustun. </ts>
               <ts e="T89" id="Seg_1209" n="e" s="T88">Bu </ts>
               <ts e="T90" id="Seg_1211" n="e" s="T89">hahɨlɨŋ </ts>
               <ts e="T91" id="Seg_1213" n="e" s="T90">kötön </ts>
               <ts e="T92" id="Seg_1215" n="e" s="T91">ihen </ts>
               <ts e="T93" id="Seg_1217" n="e" s="T92">baran </ts>
               <ts e="T94" id="Seg_1219" n="e" s="T93">"hɨ͡alɨhardar" </ts>
               <ts e="T95" id="Seg_1221" n="e" s="T94">di͡ebit. </ts>
               <ts e="T96" id="Seg_1223" n="e" s="T95">Innitiger </ts>
               <ts e="T97" id="Seg_1225" n="e" s="T96">"u͡op" </ts>
               <ts e="T98" id="Seg_1227" n="e" s="T97">di͡ebitter, </ts>
               <ts e="T99" id="Seg_1229" n="e" s="T98">ku͡opputtar. </ts>
               <ts e="T100" id="Seg_1231" n="e" s="T99">"Uː </ts>
               <ts e="T101" id="Seg_1233" n="e" s="T100">daː </ts>
               <ts e="T102" id="Seg_1235" n="e" s="T101">balɨga </ts>
               <ts e="T103" id="Seg_1237" n="e" s="T102">bɨhɨja </ts>
               <ts e="T104" id="Seg_1239" n="e" s="T103">daː </ts>
               <ts e="T105" id="Seg_1241" n="e" s="T104">bert </ts>
               <ts e="T106" id="Seg_1243" n="e" s="T105">bu͡olar </ts>
               <ts e="T107" id="Seg_1245" n="e" s="T106">ebit", </ts>
               <ts e="T108" id="Seg_1247" n="e" s="T107">di͡ete. </ts>
               <ts e="T109" id="Seg_1249" n="e" s="T108">Innʼe </ts>
               <ts e="T110" id="Seg_1251" n="e" s="T109">gɨnan </ts>
               <ts e="T111" id="Seg_1253" n="e" s="T110">baran </ts>
               <ts e="T112" id="Seg_1255" n="e" s="T111">emi͡e </ts>
               <ts e="T113" id="Seg_1257" n="e" s="T112">köttö. </ts>
               <ts e="T114" id="Seg_1259" n="e" s="T113">Bu </ts>
               <ts e="T115" id="Seg_1261" n="e" s="T114">ihen </ts>
               <ts e="T116" id="Seg_1263" n="e" s="T115">biːr </ts>
               <ts e="T117" id="Seg_1265" n="e" s="T116">hirge, </ts>
               <ts e="T118" id="Seg_1267" n="e" s="T117">uː </ts>
               <ts e="T119" id="Seg_1269" n="e" s="T118">iste, </ts>
               <ts e="T120" id="Seg_1271" n="e" s="T119">bu </ts>
               <ts e="T121" id="Seg_1273" n="e" s="T120">ihebin </ts>
               <ts e="T122" id="Seg_1275" n="e" s="T121">di͡en </ts>
               <ts e="T123" id="Seg_1277" n="e" s="T122">hahɨlɨŋ </ts>
               <ts e="T124" id="Seg_1279" n="e" s="T123">tumnastan </ts>
               <ts e="T125" id="Seg_1281" n="e" s="T124">ölön </ts>
               <ts e="T126" id="Seg_1283" n="e" s="T125">kaːlla. </ts>
               <ts e="T127" id="Seg_1285" n="e" s="T126">Biːr </ts>
               <ts e="T128" id="Seg_1287" n="e" s="T127">emeːksin </ts>
               <ts e="T129" id="Seg_1289" n="e" s="T128">uː </ts>
               <ts e="T130" id="Seg_1291" n="e" s="T129">bahɨna </ts>
               <ts e="T131" id="Seg_1293" n="e" s="T130">kiːrbit, </ts>
               <ts e="T132" id="Seg_1295" n="e" s="T131">araj </ts>
               <ts e="T133" id="Seg_1297" n="e" s="T132">bu </ts>
               <ts e="T134" id="Seg_1299" n="e" s="T133">uː </ts>
               <ts e="T135" id="Seg_1301" n="e" s="T134">baha </ts>
               <ts e="T136" id="Seg_1303" n="e" s="T135">kiːrbite </ts>
               <ts e="T137" id="Seg_1305" n="e" s="T136">hahɨl </ts>
               <ts e="T138" id="Seg_1307" n="e" s="T137">ölö </ts>
               <ts e="T139" id="Seg_1309" n="e" s="T138">hɨtarɨn </ts>
               <ts e="T140" id="Seg_1311" n="e" s="T139">bulbut. </ts>
               <ts e="T141" id="Seg_1313" n="e" s="T140">Bu </ts>
               <ts e="T142" id="Seg_1315" n="e" s="T141">emeːksiniŋ </ts>
               <ts e="T143" id="Seg_1317" n="e" s="T142">bu </ts>
               <ts e="T144" id="Seg_1319" n="e" s="T143">hahɨlɨ </ts>
               <ts e="T145" id="Seg_1321" n="e" s="T144">ildʼen </ts>
               <ts e="T146" id="Seg_1323" n="e" s="T145">hülünne, </ts>
               <ts e="T147" id="Seg_1325" n="e" s="T146">bu </ts>
               <ts e="T148" id="Seg_1327" n="e" s="T147">hülen </ts>
               <ts e="T149" id="Seg_1329" n="e" s="T148">baran </ts>
               <ts e="T150" id="Seg_1331" n="e" s="T149">kuːrta. </ts>
               <ts e="T151" id="Seg_1333" n="e" s="T150">Bu </ts>
               <ts e="T152" id="Seg_1335" n="e" s="T151">gɨnan </ts>
               <ts e="T153" id="Seg_1337" n="e" s="T152">baran </ts>
               <ts e="T154" id="Seg_1339" n="e" s="T153">araŋaska </ts>
               <ts e="T155" id="Seg_1341" n="e" s="T154">ɨjaːta. </ts>
               <ts e="T156" id="Seg_1343" n="e" s="T155">Bu </ts>
               <ts e="T157" id="Seg_1345" n="e" s="T156">ɨjɨː </ts>
               <ts e="T158" id="Seg_1347" n="e" s="T157">turan </ts>
               <ts e="T159" id="Seg_1349" n="e" s="T158">haŋarda </ts>
               <ts e="T160" id="Seg_1351" n="e" s="T159">bu </ts>
               <ts e="T161" id="Seg_1353" n="e" s="T160">emmeksin: </ts>
               <ts e="T162" id="Seg_1355" n="e" s="T161">"Hoguruːttan </ts>
               <ts e="T163" id="Seg_1357" n="e" s="T162">tɨ͡al </ts>
               <ts e="T164" id="Seg_1359" n="e" s="T163">bu͡ol", </ts>
               <ts e="T165" id="Seg_1361" n="e" s="T164">di͡ete. </ts>
               <ts e="T166" id="Seg_1363" n="e" s="T165">"Haːpattan </ts>
               <ts e="T167" id="Seg_1365" n="e" s="T166">tɨ͡al </ts>
               <ts e="T168" id="Seg_1367" n="e" s="T167">bu͡ol", </ts>
               <ts e="T169" id="Seg_1369" n="e" s="T168">di͡ete. </ts>
               <ts e="T170" id="Seg_1371" n="e" s="T169">Muntuta </ts>
               <ts e="T171" id="Seg_1373" n="e" s="T170">dʼe </ts>
               <ts e="T172" id="Seg_1375" n="e" s="T171">möŋtö. </ts>
               <ts e="T173" id="Seg_1377" n="e" s="T172">Tɨ͡al </ts>
               <ts e="T174" id="Seg_1379" n="e" s="T173">kelen </ts>
               <ts e="T175" id="Seg_1381" n="e" s="T174">dʼe </ts>
               <ts e="T176" id="Seg_1383" n="e" s="T175">bu </ts>
               <ts e="T177" id="Seg_1385" n="e" s="T176">hahɨlɨ </ts>
               <ts e="T178" id="Seg_1387" n="e" s="T177">möktögüne </ts>
               <ts e="T179" id="Seg_1389" n="e" s="T178">emeːksin </ts>
               <ts e="T180" id="Seg_1391" n="e" s="T179">hahɨlɨn </ts>
               <ts e="T181" id="Seg_1393" n="e" s="T180">kɨtta </ts>
               <ts e="T182" id="Seg_1395" n="e" s="T181">üŋküːleːte </ts>
               <ts e="T183" id="Seg_1397" n="e" s="T182">"huːraŋki haːraŋki" </ts>
               <ts e="T184" id="Seg_1399" n="e" s="T183">diː-diː. </ts>
               <ts e="T185" id="Seg_1401" n="e" s="T184">Bu </ts>
               <ts e="T186" id="Seg_1403" n="e" s="T185">gɨna </ts>
               <ts e="T187" id="Seg_1405" n="e" s="T186">turdagɨna </ts>
               <ts e="T188" id="Seg_1407" n="e" s="T187">hahɨlɨn </ts>
               <ts e="T189" id="Seg_1409" n="e" s="T188">tɨ͡ala </ts>
               <ts e="T190" id="Seg_1411" n="e" s="T189">bɨha </ts>
               <ts e="T191" id="Seg_1413" n="e" s="T190">okson </ts>
               <ts e="T192" id="Seg_1415" n="e" s="T191">ildʼen </ts>
               <ts e="T193" id="Seg_1417" n="e" s="T192">keːste. </ts>
               <ts e="T194" id="Seg_1419" n="e" s="T193">Bu </ts>
               <ts e="T195" id="Seg_1421" n="e" s="T194">emeːksin </ts>
               <ts e="T196" id="Seg_1423" n="e" s="T195">bu </ts>
               <ts e="T197" id="Seg_1425" n="e" s="T196">hahɨlɨn </ts>
               <ts e="T198" id="Seg_1427" n="e" s="T197">dʼe </ts>
               <ts e="T199" id="Seg_1429" n="e" s="T198">batta. </ts>
               <ts e="T200" id="Seg_1431" n="e" s="T199">Bu </ts>
               <ts e="T201" id="Seg_1433" n="e" s="T200">batan </ts>
               <ts e="T202" id="Seg_1435" n="e" s="T201">ihen </ts>
               <ts e="T203" id="Seg_1437" n="e" s="T202">bu </ts>
               <ts e="T204" id="Seg_1439" n="e" s="T203">emeːksin </ts>
               <ts e="T205" id="Seg_1441" n="e" s="T204">di͡ete: </ts>
               <ts e="T206" id="Seg_1443" n="e" s="T205">"Bu </ts>
               <ts e="T207" id="Seg_1445" n="e" s="T206">emijim, </ts>
               <ts e="T208" id="Seg_1447" n="e" s="T207">bɨhɨːta, </ts>
               <ts e="T209" id="Seg_1449" n="e" s="T208">ɨ͡arakan, </ts>
               <ts e="T210" id="Seg_1451" n="e" s="T209">battɨːr, </ts>
               <ts e="T211" id="Seg_1453" n="e" s="T210">bɨhɨːta", </ts>
               <ts e="T212" id="Seg_1455" n="e" s="T211">di͡en </ts>
               <ts e="T213" id="Seg_1457" n="e" s="T212">emijin </ts>
               <ts e="T214" id="Seg_1459" n="e" s="T213">bɨhan </ts>
               <ts e="T215" id="Seg_1461" n="e" s="T214">keːspite. </ts>
               <ts e="T216" id="Seg_1463" n="e" s="T215">Bu </ts>
               <ts e="T217" id="Seg_1465" n="e" s="T216">gɨnan </ts>
               <ts e="T218" id="Seg_1467" n="e" s="T217">baran </ts>
               <ts e="T219" id="Seg_1469" n="e" s="T218">emi͡e </ts>
               <ts e="T220" id="Seg_1471" n="e" s="T219">barbɨta. </ts>
               <ts e="T221" id="Seg_1473" n="e" s="T220">"Aŋaːr </ts>
               <ts e="T222" id="Seg_1475" n="e" s="T221">di͡egi </ts>
               <ts e="T223" id="Seg_1477" n="e" s="T222">emijim </ts>
               <ts e="T224" id="Seg_1479" n="e" s="T223">emi͡e </ts>
               <ts e="T225" id="Seg_1481" n="e" s="T224">ɨ͡arakan </ts>
               <ts e="T226" id="Seg_1483" n="e" s="T225">duː", </ts>
               <ts e="T227" id="Seg_1485" n="e" s="T226">di͡et, </ts>
               <ts e="T228" id="Seg_1487" n="e" s="T227">bu </ts>
               <ts e="T229" id="Seg_1489" n="e" s="T228">emiji </ts>
               <ts e="T230" id="Seg_1491" n="e" s="T229">emi͡e </ts>
               <ts e="T231" id="Seg_1493" n="e" s="T230">bɨhan </ts>
               <ts e="T232" id="Seg_1495" n="e" s="T231">keːspite. </ts>
               <ts e="T233" id="Seg_1497" n="e" s="T232">Bu </ts>
               <ts e="T234" id="Seg_1499" n="e" s="T233">gɨnan </ts>
               <ts e="T235" id="Seg_1501" n="e" s="T234">baran </ts>
               <ts e="T236" id="Seg_1503" n="e" s="T235">emi͡e </ts>
               <ts e="T237" id="Seg_1505" n="e" s="T236">barbɨta: </ts>
               <ts e="T238" id="Seg_1507" n="e" s="T237">"Doː, </ts>
               <ts e="T239" id="Seg_1509" n="e" s="T238">bu </ts>
               <ts e="T240" id="Seg_1511" n="e" s="T239">aŋaːr </ts>
               <ts e="T241" id="Seg_1513" n="e" s="T240">iliːm </ts>
               <ts e="T242" id="Seg_1515" n="e" s="T241">mi͡ene </ts>
               <ts e="T243" id="Seg_1517" n="e" s="T242">ɨ͡arakan </ts>
               <ts e="T244" id="Seg_1519" n="e" s="T243">duː", </ts>
               <ts e="T245" id="Seg_1521" n="e" s="T244">di͡en </ts>
               <ts e="T246" id="Seg_1523" n="e" s="T245">aŋaːr </ts>
               <ts e="T247" id="Seg_1525" n="e" s="T246">iliːtin </ts>
               <ts e="T248" id="Seg_1527" n="e" s="T247">bɨsta. </ts>
               <ts e="T249" id="Seg_1529" n="e" s="T248">Dʼe </ts>
               <ts e="T250" id="Seg_1531" n="e" s="T249">emi͡e </ts>
               <ts e="T251" id="Seg_1533" n="e" s="T250">barda. </ts>
               <ts e="T252" id="Seg_1535" n="e" s="T251">Bu </ts>
               <ts e="T253" id="Seg_1537" n="e" s="T252">emi͡e </ts>
               <ts e="T254" id="Seg_1539" n="e" s="T253">aŋaːr </ts>
               <ts e="T255" id="Seg_1541" n="e" s="T254">iliːtin </ts>
               <ts e="T256" id="Seg_1543" n="e" s="T255">bɨsta. </ts>
               <ts e="T257" id="Seg_1545" n="e" s="T256">Ikki </ts>
               <ts e="T258" id="Seg_1547" n="e" s="T257">iliːte </ts>
               <ts e="T259" id="Seg_1549" n="e" s="T258">hu͡ok </ts>
               <ts e="T260" id="Seg_1551" n="e" s="T259">bu͡olla. </ts>
               <ts e="T261" id="Seg_1553" n="e" s="T260">Itigenne </ts>
               <ts e="T262" id="Seg_1555" n="e" s="T261">emeːksin </ts>
               <ts e="T263" id="Seg_1557" n="e" s="T262">eppite: </ts>
               <ts e="T264" id="Seg_1559" n="e" s="T263">"Bu </ts>
               <ts e="T265" id="Seg_1561" n="e" s="T264">taŋara </ts>
               <ts e="T266" id="Seg_1563" n="e" s="T265">u͡olun </ts>
               <ts e="T267" id="Seg_1565" n="e" s="T266">gɨtta </ts>
               <ts e="T268" id="Seg_1567" n="e" s="T267">ugurahɨ͡akpɨn </ts>
               <ts e="T269" id="Seg_1569" n="e" s="T268">u͡ohum </ts>
               <ts e="T270" id="Seg_1571" n="e" s="T269">ere </ts>
               <ts e="T271" id="Seg_1573" n="e" s="T270">bütün </ts>
               <ts e="T272" id="Seg_1575" n="e" s="T271">bu͡ollun." </ts>
               <ts e="T273" id="Seg_1577" n="e" s="T272">Onton </ts>
               <ts e="T274" id="Seg_1579" n="e" s="T273">barbɨta. </ts>
               <ts e="T275" id="Seg_1581" n="e" s="T274">Elete </ts>
               <ts e="T276" id="Seg_1583" n="e" s="T275">bu͡olan </ts>
               <ts e="T277" id="Seg_1585" n="e" s="T276">ölbüte. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T9" id="Seg_1586" s="T0">FeA_1931_OldWomanFoxFur_flk.001</ta>
            <ta e="T12" id="Seg_1587" s="T9">FeA_1931_OldWomanFoxFur_flk.002</ta>
            <ta e="T27" id="Seg_1588" s="T12">FeA_1931_OldWomanFoxFur_flk.003</ta>
            <ta e="T35" id="Seg_1589" s="T27">FeA_1931_OldWomanFoxFur_flk.004</ta>
            <ta e="T41" id="Seg_1590" s="T35">FeA_1931_OldWomanFoxFur_flk.005</ta>
            <ta e="T46" id="Seg_1591" s="T41">FeA_1931_OldWomanFoxFur_flk.006</ta>
            <ta e="T49" id="Seg_1592" s="T46">FeA_1931_OldWomanFoxFur_flk.007</ta>
            <ta e="T54" id="Seg_1593" s="T49">FeA_1931_OldWomanFoxFur_flk.008</ta>
            <ta e="T56" id="Seg_1594" s="T54">FeA_1931_OldWomanFoxFur_flk.009</ta>
            <ta e="T59" id="Seg_1595" s="T56">FeA_1931_OldWomanFoxFur_flk.010</ta>
            <ta e="T63" id="Seg_1596" s="T59">FeA_1931_OldWomanFoxFur_flk.011</ta>
            <ta e="T70" id="Seg_1597" s="T63">FeA_1931_OldWomanFoxFur_flk.012</ta>
            <ta e="T79" id="Seg_1598" s="T70">FeA_1931_OldWomanFoxFur_flk.013</ta>
            <ta e="T81" id="Seg_1599" s="T79">FeA_1931_OldWomanFoxFur_flk.014</ta>
            <ta e="T88" id="Seg_1600" s="T81">FeA_1931_OldWomanFoxFur_flk.015</ta>
            <ta e="T95" id="Seg_1601" s="T88">FeA_1931_OldWomanFoxFur_flk.016</ta>
            <ta e="T99" id="Seg_1602" s="T95">FeA_1931_OldWomanFoxFur_flk.017</ta>
            <ta e="T108" id="Seg_1603" s="T99">FeA_1931_OldWomanFoxFur_flk.018</ta>
            <ta e="T113" id="Seg_1604" s="T108">FeA_1931_OldWomanFoxFur_flk.019</ta>
            <ta e="T126" id="Seg_1605" s="T113">FeA_1931_OldWomanFoxFur_flk.020</ta>
            <ta e="T140" id="Seg_1606" s="T126">FeA_1931_OldWomanFoxFur_flk.021</ta>
            <ta e="T150" id="Seg_1607" s="T140">FeA_1931_OldWomanFoxFur_flk.022</ta>
            <ta e="T155" id="Seg_1608" s="T150">FeA_1931_OldWomanFoxFur_flk.023</ta>
            <ta e="T161" id="Seg_1609" s="T155">FeA_1931_OldWomanFoxFur_flk.024</ta>
            <ta e="T165" id="Seg_1610" s="T161">FeA_1931_OldWomanFoxFur_flk.025</ta>
            <ta e="T169" id="Seg_1611" s="T165">FeA_1931_OldWomanFoxFur_flk.026</ta>
            <ta e="T172" id="Seg_1612" s="T169">FeA_1931_OldWomanFoxFur_flk.027</ta>
            <ta e="T184" id="Seg_1613" s="T172">FeA_1931_OldWomanFoxFur_flk.028</ta>
            <ta e="T193" id="Seg_1614" s="T184">FeA_1931_OldWomanFoxFur_flk.029</ta>
            <ta e="T199" id="Seg_1615" s="T193">FeA_1931_OldWomanFoxFur_flk.030</ta>
            <ta e="T205" id="Seg_1616" s="T199">FeA_1931_OldWomanFoxFur_flk.031</ta>
            <ta e="T215" id="Seg_1617" s="T205">FeA_1931_OldWomanFoxFur_flk.032</ta>
            <ta e="T220" id="Seg_1618" s="T215">FeA_1931_OldWomanFoxFur_flk.033</ta>
            <ta e="T232" id="Seg_1619" s="T220">FeA_1931_OldWomanFoxFur_flk.034</ta>
            <ta e="T237" id="Seg_1620" s="T232">FeA_1931_OldWomanFoxFur_flk.035</ta>
            <ta e="T248" id="Seg_1621" s="T237">FeA_1931_OldWomanFoxFur_flk.036</ta>
            <ta e="T251" id="Seg_1622" s="T248">FeA_1931_OldWomanFoxFur_flk.037</ta>
            <ta e="T256" id="Seg_1623" s="T251">FeA_1931_OldWomanFoxFur_flk.038</ta>
            <ta e="T260" id="Seg_1624" s="T256">FeA_1931_OldWomanFoxFur_flk.039</ta>
            <ta e="T263" id="Seg_1625" s="T260">FeA_1931_OldWomanFoxFur_flk.040</ta>
            <ta e="T272" id="Seg_1626" s="T263">FeA_1931_OldWomanFoxFur_flk.041</ta>
            <ta e="T274" id="Seg_1627" s="T272">FeA_1931_OldWomanFoxFur_flk.042</ta>
            <ta e="T277" id="Seg_1628" s="T274">FeA_1931_OldWomanFoxFur_flk.043</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T9" id="Seg_1629" s="T0">Биир һаһыл арыыга кааттарбыт, канан да карбаан таксыагын һуок.</ta>
            <ta e="T12" id="Seg_1630" s="T9">Бу һаһылыҥ ытаата: </ta>
            <ta e="T27" id="Seg_1631" s="T12">"Бу даганы арыыга кааттардакпын абатын, бу арыыга кааттарбатым буоллар бу дулгааннар айаларын кирсилэрин һиэг этим".</ta>
            <ta e="T35" id="Seg_1632" s="T27">Маныга уу иһигэр һаҥа иһиллибит, арай һыалыһардар һаҥарбыттар: </ta>
            <ta e="T41" id="Seg_1633" s="T35">— Аа, оголор, һаһылбыт ытыыр эбит дуу?</ta>
            <ta e="T46" id="Seg_1634" s="T41">Бу маны истэн һаһыл диэтэ: </ta>
            <ta e="T49" id="Seg_1635" s="T46">— һыалыһар, — диир, — һырсыак!</ta>
            <ta e="T54" id="Seg_1636" s="T49">Һыалыһардар эмиэ: — Дьэ, һырсыак! — диэбиттэр.</ta>
            <ta e="T56" id="Seg_1637" s="T54">һаһылыҥ диэтэ: </ta>
            <ta e="T59" id="Seg_1638" s="T56">— Чэй, кытылга тиксиҥ!</ta>
            <ta e="T63" id="Seg_1639" s="T59">Каскытый, мин аагыам, — диэтэ.</ta>
            <ta e="T70" id="Seg_1640" s="T63">Һаһыл, кытылга кэлэн турбуттарыгар, үрдүлэригэр ойо һырытта.</ta>
            <ta e="T79" id="Seg_1641" s="T70">һаһыл ааган: "Угун, угун, дьанус!" — диэн аксыытын тыла ити.</ta>
            <ta e="T81" id="Seg_1642" s="T79">Дьэ бараллар.</ta>
            <ta e="T88" id="Seg_1643" s="T81">Һаһылыҥ кытыл устун барбыт, балыгыҥ уу устун.</ta>
            <ta e="T95" id="Seg_1644" s="T88">Бу һаһылыҥ көтөн иһэн баран: ‎‎— һыалыһардар! — диэбит.</ta>
            <ta e="T99" id="Seg_1645" s="T95">Иннитигэр: — Уоп! — диэбиттэр, куоппуттар.</ta>
            <ta e="T108" id="Seg_1646" s="T99">— Уу даа балыга быһыйа даа бэрт буолар эбит, — диэтэ.</ta>
            <ta e="T113" id="Seg_1647" s="T108">Инньэ гынан баран эмиэ көттө.</ta>
            <ta e="T126" id="Seg_1648" s="T113">Бу иһэн биир һиргэ, уу истэ, бу иһэбин диэн һаһылыҥ тумнастан өлөн каалла.</ta>
            <ta e="T140" id="Seg_1649" s="T126">Биир эмээксин уу баһына киирбит, арай бу уу баһа киирбитэ һаһыл өлө һытарын булбут.</ta>
            <ta e="T150" id="Seg_1650" s="T140">Бу эмээксиниҥ бу һаһылы илдьэн һүлүннэ, бу һүлэн баран куурта.</ta>
            <ta e="T155" id="Seg_1651" s="T150">Бу гынан баран араҥаска ыйаата.</ta>
            <ta e="T161" id="Seg_1652" s="T155">Бу ыйыы туран һаҥарда бу эммэксин: </ta>
            <ta e="T165" id="Seg_1653" s="T161">— һогурууттан тыал буол! — диэтэ.</ta>
            <ta e="T169" id="Seg_1654" s="T165">— һаапаттан тыал буол! — диэтэ.</ta>
            <ta e="T172" id="Seg_1655" s="T169">Мунтута дьэ мөҥтө.</ta>
            <ta e="T184" id="Seg_1656" s="T172">Тыал кэлэн дьэ бу һаһылы мөктөгөгүнэ эмээксин һаһылын кытта үҥкүүлээтэ: ‎‎— һуураҥки-һаараҥки! — дии-дии.</ta>
            <ta e="T193" id="Seg_1657" s="T184">Бу гына турдагына һаһылын тыала быһа оксон илдьэн кээстэ.</ta>
            <ta e="T199" id="Seg_1658" s="T193">Бу эмээксин бу һаһылын дьэ батта.</ta>
            <ta e="T205" id="Seg_1659" s="T199">Бу батан иһэн бу эмээксин диэтэ: </ta>
            <ta e="T215" id="Seg_1660" s="T205">"Бу эмийим, быһыыта, ыаракан, баттыыр, быһыыта", — диэн эмийин быһан кээспитэ.</ta>
            <ta e="T220" id="Seg_1661" s="T215">Бу гынан баран эмиэ барбыта.</ta>
            <ta e="T232" id="Seg_1662" s="T220">"Аҥаар диэги эмийим эмиэ ыаракан дуу", — диэт, бу эмийи эмиэ быһан кээспитэ.</ta>
            <ta e="T237" id="Seg_1663" s="T232">Бу гынан баран эмиэ барбыта:</ta>
            <ta e="T248" id="Seg_1664" s="T237">"Доо, бу аҥаар илиим миэнэ ыаракан дуу?" — диэн аҥаар илиитин быста.</ta>
            <ta e="T251" id="Seg_1665" s="T248">Дьэ эмиэ барда.</ta>
            <ta e="T256" id="Seg_1666" s="T251">Бу эмиэ аҥаар илиитин быста.</ta>
            <ta e="T260" id="Seg_1667" s="T256">Икки илиитэ һуок буолла.</ta>
            <ta e="T263" id="Seg_1668" s="T260">Итигэннэ эмээксин эппитэ: </ta>
            <ta e="T272" id="Seg_1669" s="T263">"Бу таҥара уолун гытта угураһыакпын уоһум эрэ бүтүн буоллун".</ta>
            <ta e="T274" id="Seg_1670" s="T272">Онтон барбыта.</ta>
            <ta e="T277" id="Seg_1671" s="T274">Элэтэ буолан өлбүтэ.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T9" id="Seg_1672" s="T0">Biːr hahɨl arɨːga kaːttarbɨt, kanan da karbaːn taksɨ͡agɨn hu͡ok. </ta>
            <ta e="T12" id="Seg_1673" s="T9">Bu hahɨlɨŋ ɨtaːta: </ta>
            <ta e="T27" id="Seg_1674" s="T12">"Bu daganɨ arɨːga kaːttardakpɨn abatɨn, bu arɨːga kaːttarbatɨm bu͡ollar bu dulgaːnnar ajalarɨn kirsilerin hi͡eg etim." </ta>
            <ta e="T35" id="Seg_1675" s="T27">Manɨga uː ihiger haŋa ihillibit, araj hɨ͡alɨhardar haŋarbɨttar: </ta>
            <ta e="T41" id="Seg_1676" s="T35">"Aː, ogolor, hahɨlbɨt ɨtɨːr ebit duː?" </ta>
            <ta e="T46" id="Seg_1677" s="T41">Bu manɨ isten hahɨl di͡ete: </ta>
            <ta e="T49" id="Seg_1678" s="T46">"Hɨ͡alɨhar", diːr, "hɨrsɨ͡ak!" </ta>
            <ta e="T54" id="Seg_1679" s="T49">Hɨ͡alɨhardar emi͡e "dʼe, hɨrsɨ͡ak" di͡ebitter. </ta>
            <ta e="T56" id="Seg_1680" s="T54">Hahɨlɨŋ di͡ete: </ta>
            <ta e="T59" id="Seg_1681" s="T56">"Čej, kɨtɨlga tiksiŋ! </ta>
            <ta e="T63" id="Seg_1682" s="T59">Kaskɨtɨj, min aːgɨ͡am", di͡ete. </ta>
            <ta e="T70" id="Seg_1683" s="T63">Hahɨl, kɨtɨlga kelen turbuttarɨgar, ürdüleriger ojo hɨrɨtta. </ta>
            <ta e="T79" id="Seg_1684" s="T70">Hahɨl aːgan "ugun, ugun, dʼanus" di͡en aksɨːtɨn tɨla iti. </ta>
            <ta e="T81" id="Seg_1685" s="T79">Dʼe barallar. </ta>
            <ta e="T88" id="Seg_1686" s="T81">Hahɨlɨŋ kɨtɨl ustun barbɨt, balɨgɨŋ uː ustun. </ta>
            <ta e="T95" id="Seg_1687" s="T88">Bu hahɨlɨŋ kötön ihen baran "hɨ͡alɨhardar" di͡ebit. </ta>
            <ta e="T99" id="Seg_1688" s="T95">Innitiger "u͡op" di͡ebitter, ku͡opputtar. </ta>
            <ta e="T108" id="Seg_1689" s="T99">"Uː daː balɨga bɨhɨja daː bert bu͡olar ebit", di͡ete. </ta>
            <ta e="T113" id="Seg_1690" s="T108">Innʼe gɨnan baran emi͡e köttö. </ta>
            <ta e="T126" id="Seg_1691" s="T113">Bu ihen biːr hirge, uː iste, bu ihebin di͡en hahɨlɨŋ tumnastan ölön kaːlla. </ta>
            <ta e="T140" id="Seg_1692" s="T126">Biːr emeːksin uː bahɨna kiːrbit, araj bu uː baha kiːrbite hahɨl ölö hɨtarɨn bulbut. </ta>
            <ta e="T150" id="Seg_1693" s="T140">Bu emeːksiniŋ bu hahɨlɨ ildʼen hülünne, bu hülen baran kuːrta. </ta>
            <ta e="T155" id="Seg_1694" s="T150">Bu gɨnan baran araŋaska ɨjaːta. </ta>
            <ta e="T161" id="Seg_1695" s="T155">Bu ɨjɨː turan haŋarda bu emmeksin: </ta>
            <ta e="T165" id="Seg_1696" s="T161">"Hoguruːttan tɨ͡al bu͡ol", di͡ete. </ta>
            <ta e="T169" id="Seg_1697" s="T165">"Haːpattan tɨ͡al bu͡ol", di͡ete. </ta>
            <ta e="T172" id="Seg_1698" s="T169">Muntuta dʼe möŋtö. </ta>
            <ta e="T184" id="Seg_1699" s="T172">Tɨ͡al kelen dʼe bu hahɨlɨ möktögüne emeːksin hahɨlɨn kɨtta üŋküːleːte "huːraŋki haːraŋki" diː-diː. </ta>
            <ta e="T193" id="Seg_1700" s="T184">Bu gɨna turdagɨna hahɨlɨn tɨ͡ala bɨha okson ildʼen keːste. </ta>
            <ta e="T199" id="Seg_1701" s="T193">Bu emeːksin bu hahɨlɨn dʼe batta. </ta>
            <ta e="T205" id="Seg_1702" s="T199">Bu batan ihen bu emeːksin di͡ete: </ta>
            <ta e="T215" id="Seg_1703" s="T205">"Bu emijim, bɨhɨːta, ɨ͡arakan, battɨːr, bɨhɨːta", di͡en emijin bɨhan keːspite. </ta>
            <ta e="T220" id="Seg_1704" s="T215">Bu gɨnan baran emi͡e barbɨta. </ta>
            <ta e="T232" id="Seg_1705" s="T220">"Aŋaːr di͡egi emijim emi͡e ɨ͡arakan duː", di͡et, bu emiji emi͡e bɨhan keːspite. </ta>
            <ta e="T237" id="Seg_1706" s="T232">Bu gɨnan baran emi͡e barbɨta:</ta>
            <ta e="T248" id="Seg_1707" s="T237">"Doː, bu aŋaːr iliːm mi͡ene ɨ͡arakan duː", di͡en aŋaːr iliːtin bɨsta. </ta>
            <ta e="T251" id="Seg_1708" s="T248">Dʼe emi͡e barda. </ta>
            <ta e="T256" id="Seg_1709" s="T251">Bu emi͡e aŋaːr iliːtin bɨsta. </ta>
            <ta e="T260" id="Seg_1710" s="T256">Ikki iliːte hu͡ok bu͡olla. </ta>
            <ta e="T263" id="Seg_1711" s="T260">Itigenne emeːksin eppite: </ta>
            <ta e="T272" id="Seg_1712" s="T263">"Bu taŋara u͡olun gɨtta ugurahɨ͡akpɨn u͡ohum ere bütün bu͡ollun." </ta>
            <ta e="T274" id="Seg_1713" s="T272">Onton barbɨta. </ta>
            <ta e="T277" id="Seg_1714" s="T274">Elete bu͡olan ölbüte. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_1715" s="T0">biːr</ta>
            <ta e="T2" id="Seg_1716" s="T1">hahɨl</ta>
            <ta e="T3" id="Seg_1717" s="T2">arɨː-ga</ta>
            <ta e="T4" id="Seg_1718" s="T3">kaːttar-bɨt</ta>
            <ta e="T5" id="Seg_1719" s="T4">kanan</ta>
            <ta e="T6" id="Seg_1720" s="T5">da</ta>
            <ta e="T7" id="Seg_1721" s="T6">karbaː-n</ta>
            <ta e="T8" id="Seg_1722" s="T7">taks-ɨ͡ag-ɨ-n</ta>
            <ta e="T9" id="Seg_1723" s="T8">hu͡ok</ta>
            <ta e="T10" id="Seg_1724" s="T9">bu</ta>
            <ta e="T11" id="Seg_1725" s="T10">hahɨl-ɨ-ŋ</ta>
            <ta e="T12" id="Seg_1726" s="T11">ɨtaː-t-a</ta>
            <ta e="T13" id="Seg_1727" s="T12">bu</ta>
            <ta e="T14" id="Seg_1728" s="T13">daganɨ</ta>
            <ta e="T15" id="Seg_1729" s="T14">arɨː-ga</ta>
            <ta e="T16" id="Seg_1730" s="T15">kaːttar-dak-pɨ-n</ta>
            <ta e="T17" id="Seg_1731" s="T16">aba-tɨ-n</ta>
            <ta e="T18" id="Seg_1732" s="T17">bu</ta>
            <ta e="T19" id="Seg_1733" s="T18">arɨː-ga</ta>
            <ta e="T20" id="Seg_1734" s="T19">kaːttar-bat-ɨ-m</ta>
            <ta e="T21" id="Seg_1735" s="T20">bu͡ol-lar</ta>
            <ta e="T22" id="Seg_1736" s="T21">bu</ta>
            <ta e="T23" id="Seg_1737" s="T22">dulgaːn-nar</ta>
            <ta e="T24" id="Seg_1738" s="T23">aja-larɨ-n</ta>
            <ta e="T25" id="Seg_1739" s="T24">kirs-i-leri-n</ta>
            <ta e="T26" id="Seg_1740" s="T25">h-i͡eg</ta>
            <ta e="T27" id="Seg_1741" s="T26">e-ti-m</ta>
            <ta e="T28" id="Seg_1742" s="T27">manɨ-ga</ta>
            <ta e="T29" id="Seg_1743" s="T28">uː</ta>
            <ta e="T30" id="Seg_1744" s="T29">ih-i-ger</ta>
            <ta e="T31" id="Seg_1745" s="T30">haŋa</ta>
            <ta e="T32" id="Seg_1746" s="T31">ihill-i-bit</ta>
            <ta e="T33" id="Seg_1747" s="T32">araj</ta>
            <ta e="T34" id="Seg_1748" s="T33">hɨ͡alɨhar-dar</ta>
            <ta e="T35" id="Seg_1749" s="T34">haŋar-bɨt-tar</ta>
            <ta e="T36" id="Seg_1750" s="T35">aː</ta>
            <ta e="T37" id="Seg_1751" s="T36">ogo-lor</ta>
            <ta e="T38" id="Seg_1752" s="T37">hahɨl-bɨt</ta>
            <ta e="T39" id="Seg_1753" s="T38">ɨtɨː-r</ta>
            <ta e="T40" id="Seg_1754" s="T39">e-bit</ta>
            <ta e="T41" id="Seg_1755" s="T40">duː</ta>
            <ta e="T42" id="Seg_1756" s="T41">bu</ta>
            <ta e="T43" id="Seg_1757" s="T42">ma-nɨ</ta>
            <ta e="T44" id="Seg_1758" s="T43">ist-en</ta>
            <ta e="T45" id="Seg_1759" s="T44">hahɨl</ta>
            <ta e="T46" id="Seg_1760" s="T45">di͡e-t-e</ta>
            <ta e="T47" id="Seg_1761" s="T46">hɨ͡alɨhar</ta>
            <ta e="T48" id="Seg_1762" s="T47">diː-r</ta>
            <ta e="T49" id="Seg_1763" s="T48">hɨrs-ɨ͡ak</ta>
            <ta e="T50" id="Seg_1764" s="T49">hɨ͡alɨhar-dar</ta>
            <ta e="T51" id="Seg_1765" s="T50">emi͡e</ta>
            <ta e="T52" id="Seg_1766" s="T51">dʼe</ta>
            <ta e="T53" id="Seg_1767" s="T52">hɨrs-ɨ͡ak</ta>
            <ta e="T54" id="Seg_1768" s="T53">di͡e-bit-ter</ta>
            <ta e="T55" id="Seg_1769" s="T54">hahɨl-ɨ-ŋ</ta>
            <ta e="T56" id="Seg_1770" s="T55">di͡e-t-e</ta>
            <ta e="T57" id="Seg_1771" s="T56">čej</ta>
            <ta e="T58" id="Seg_1772" s="T57">kɨtɨl-ga</ta>
            <ta e="T59" id="Seg_1773" s="T58">tiks-i-ŋ</ta>
            <ta e="T60" id="Seg_1774" s="T59">kas-kɨt=ɨj</ta>
            <ta e="T61" id="Seg_1775" s="T60">min</ta>
            <ta e="T62" id="Seg_1776" s="T61">aːg-ɨ͡a-m</ta>
            <ta e="T63" id="Seg_1777" s="T62">di͡e-t-e</ta>
            <ta e="T64" id="Seg_1778" s="T63">hahɨl</ta>
            <ta e="T65" id="Seg_1779" s="T64">kɨtɨl-ga</ta>
            <ta e="T66" id="Seg_1780" s="T65">kel-en</ta>
            <ta e="T67" id="Seg_1781" s="T66">tur-but-tarɨ-gar</ta>
            <ta e="T68" id="Seg_1782" s="T67">ürdü-leri-ger</ta>
            <ta e="T69" id="Seg_1783" s="T68">oj-o</ta>
            <ta e="T70" id="Seg_1784" s="T69">hɨrɨt-t-a</ta>
            <ta e="T71" id="Seg_1785" s="T70">hahɨl</ta>
            <ta e="T72" id="Seg_1786" s="T71">aːg-an</ta>
            <ta e="T76" id="Seg_1787" s="T75">di͡e-n</ta>
            <ta e="T77" id="Seg_1788" s="T76">aksɨː-tɨ-n</ta>
            <ta e="T78" id="Seg_1789" s="T77">tɨl-a</ta>
            <ta e="T79" id="Seg_1790" s="T78">iti</ta>
            <ta e="T80" id="Seg_1791" s="T79">dʼe</ta>
            <ta e="T81" id="Seg_1792" s="T80">bar-al-lar</ta>
            <ta e="T82" id="Seg_1793" s="T81">hahɨl-ɨ-ŋ</ta>
            <ta e="T83" id="Seg_1794" s="T82">kɨtɨl</ta>
            <ta e="T84" id="Seg_1795" s="T83">ustun</ta>
            <ta e="T85" id="Seg_1796" s="T84">bar-bɨt</ta>
            <ta e="T86" id="Seg_1797" s="T85">balɨg-ɨ-ŋ</ta>
            <ta e="T87" id="Seg_1798" s="T86">uː</ta>
            <ta e="T88" id="Seg_1799" s="T87">ustun</ta>
            <ta e="T89" id="Seg_1800" s="T88">bu</ta>
            <ta e="T90" id="Seg_1801" s="T89">hahɨl-ɨ-ŋ</ta>
            <ta e="T91" id="Seg_1802" s="T90">köt-ön</ta>
            <ta e="T92" id="Seg_1803" s="T91">ih-en</ta>
            <ta e="T93" id="Seg_1804" s="T92">bar-an</ta>
            <ta e="T94" id="Seg_1805" s="T93">hɨ͡alɨhar-dar</ta>
            <ta e="T95" id="Seg_1806" s="T94">di͡e-bit</ta>
            <ta e="T96" id="Seg_1807" s="T95">inni-ti-ger</ta>
            <ta e="T97" id="Seg_1808" s="T96">U͡op</ta>
            <ta e="T98" id="Seg_1809" s="T97">di͡e-bit-ter</ta>
            <ta e="T99" id="Seg_1810" s="T98">ku͡op-put-tar</ta>
            <ta e="T100" id="Seg_1811" s="T99">uː</ta>
            <ta e="T101" id="Seg_1812" s="T100">daː</ta>
            <ta e="T102" id="Seg_1813" s="T101">balɨg-a</ta>
            <ta e="T103" id="Seg_1814" s="T102">bɨhɨj-a</ta>
            <ta e="T104" id="Seg_1815" s="T103">daː</ta>
            <ta e="T105" id="Seg_1816" s="T104">bert</ta>
            <ta e="T106" id="Seg_1817" s="T105">bu͡ol-ar</ta>
            <ta e="T107" id="Seg_1818" s="T106">e-bit</ta>
            <ta e="T108" id="Seg_1819" s="T107">di͡e-t-e</ta>
            <ta e="T109" id="Seg_1820" s="T108">innʼe</ta>
            <ta e="T110" id="Seg_1821" s="T109">gɨn-an</ta>
            <ta e="T111" id="Seg_1822" s="T110">baran</ta>
            <ta e="T112" id="Seg_1823" s="T111">emi͡e</ta>
            <ta e="T113" id="Seg_1824" s="T112">köt-t-ö</ta>
            <ta e="T114" id="Seg_1825" s="T113">bu</ta>
            <ta e="T115" id="Seg_1826" s="T114">ih-en</ta>
            <ta e="T116" id="Seg_1827" s="T115">biːr</ta>
            <ta e="T117" id="Seg_1828" s="T116">hir-ge</ta>
            <ta e="T118" id="Seg_1829" s="T117">uː</ta>
            <ta e="T119" id="Seg_1830" s="T118">is-t-e</ta>
            <ta e="T120" id="Seg_1831" s="T119">bu</ta>
            <ta e="T121" id="Seg_1832" s="T120">ih-e-bin</ta>
            <ta e="T122" id="Seg_1833" s="T121">di͡e-n</ta>
            <ta e="T123" id="Seg_1834" s="T122">hahɨl-ɨ-ŋ</ta>
            <ta e="T124" id="Seg_1835" s="T123">tumnast-an</ta>
            <ta e="T125" id="Seg_1836" s="T124">öl-ön</ta>
            <ta e="T126" id="Seg_1837" s="T125">kaːl-l-a</ta>
            <ta e="T127" id="Seg_1838" s="T126">biːr</ta>
            <ta e="T128" id="Seg_1839" s="T127">emeːksin</ta>
            <ta e="T129" id="Seg_1840" s="T128">uː</ta>
            <ta e="T130" id="Seg_1841" s="T129">bahɨn-a</ta>
            <ta e="T131" id="Seg_1842" s="T130">kiːr-bit</ta>
            <ta e="T132" id="Seg_1843" s="T131">araj</ta>
            <ta e="T133" id="Seg_1844" s="T132">bu</ta>
            <ta e="T134" id="Seg_1845" s="T133">uː</ta>
            <ta e="T135" id="Seg_1846" s="T134">bah-a</ta>
            <ta e="T136" id="Seg_1847" s="T135">kiːr-bit-e</ta>
            <ta e="T137" id="Seg_1848" s="T136">hahɨl</ta>
            <ta e="T138" id="Seg_1849" s="T137">öl-ö</ta>
            <ta e="T139" id="Seg_1850" s="T138">hɨt-ar-ɨ-n</ta>
            <ta e="T140" id="Seg_1851" s="T139">bul-but</ta>
            <ta e="T141" id="Seg_1852" s="T140">bu</ta>
            <ta e="T142" id="Seg_1853" s="T141">emeːksin-i-ŋ</ta>
            <ta e="T143" id="Seg_1854" s="T142">bu</ta>
            <ta e="T144" id="Seg_1855" s="T143">hahɨl-ɨ</ta>
            <ta e="T145" id="Seg_1856" s="T144">ildʼ-en</ta>
            <ta e="T146" id="Seg_1857" s="T145">hül-ü-n-n-e</ta>
            <ta e="T147" id="Seg_1858" s="T146">bu</ta>
            <ta e="T148" id="Seg_1859" s="T147">hül-en</ta>
            <ta e="T149" id="Seg_1860" s="T148">baran</ta>
            <ta e="T150" id="Seg_1861" s="T149">kuːr-t-a</ta>
            <ta e="T151" id="Seg_1862" s="T150">bu</ta>
            <ta e="T152" id="Seg_1863" s="T151">gɨn-an</ta>
            <ta e="T153" id="Seg_1864" s="T152">baran</ta>
            <ta e="T154" id="Seg_1865" s="T153">araŋas-ka</ta>
            <ta e="T155" id="Seg_1866" s="T154">ɨjaː-t-a</ta>
            <ta e="T156" id="Seg_1867" s="T155">bu</ta>
            <ta e="T157" id="Seg_1868" s="T156">ɨj-ɨː</ta>
            <ta e="T158" id="Seg_1869" s="T157">tur-an</ta>
            <ta e="T159" id="Seg_1870" s="T158">haŋar-d-a</ta>
            <ta e="T160" id="Seg_1871" s="T159">bu</ta>
            <ta e="T161" id="Seg_1872" s="T160">emmeksin</ta>
            <ta e="T162" id="Seg_1873" s="T161">hoguruː-ttan</ta>
            <ta e="T163" id="Seg_1874" s="T162">tɨ͡al</ta>
            <ta e="T164" id="Seg_1875" s="T163">bu͡ol</ta>
            <ta e="T165" id="Seg_1876" s="T164">di͡e-t-e</ta>
            <ta e="T166" id="Seg_1877" s="T165">haːpat-tan</ta>
            <ta e="T167" id="Seg_1878" s="T166">tɨ͡al</ta>
            <ta e="T168" id="Seg_1879" s="T167">bu͡ol</ta>
            <ta e="T169" id="Seg_1880" s="T168">di͡e-t-e</ta>
            <ta e="T170" id="Seg_1881" s="T169">mun-tu-ta</ta>
            <ta e="T171" id="Seg_1882" s="T170">dʼe</ta>
            <ta e="T172" id="Seg_1883" s="T171">möŋ-t-ö</ta>
            <ta e="T173" id="Seg_1884" s="T172">tɨ͡al</ta>
            <ta e="T174" id="Seg_1885" s="T173">kel-en</ta>
            <ta e="T175" id="Seg_1886" s="T174">dʼe</ta>
            <ta e="T176" id="Seg_1887" s="T175">bu</ta>
            <ta e="T177" id="Seg_1888" s="T176">hahɨl-ɨ</ta>
            <ta e="T178" id="Seg_1889" s="T177">mök-tög-üne</ta>
            <ta e="T179" id="Seg_1890" s="T178">emeːksin</ta>
            <ta e="T180" id="Seg_1891" s="T179">hahɨl-ɨ-n</ta>
            <ta e="T181" id="Seg_1892" s="T180">kɨtta</ta>
            <ta e="T182" id="Seg_1893" s="T181">üŋküːleː-t-e</ta>
            <ta e="T183" id="Seg_1894" s="T182">huːraŋki haːraŋki</ta>
            <ta e="T184" id="Seg_1895" s="T183">d-iː-d-iː</ta>
            <ta e="T185" id="Seg_1896" s="T184">bu</ta>
            <ta e="T186" id="Seg_1897" s="T185">gɨn-a</ta>
            <ta e="T187" id="Seg_1898" s="T186">tur-dag-ɨna</ta>
            <ta e="T188" id="Seg_1899" s="T187">hahɨl-ɨ-n</ta>
            <ta e="T189" id="Seg_1900" s="T188">tɨ͡al-a</ta>
            <ta e="T190" id="Seg_1901" s="T189">bɨh-a</ta>
            <ta e="T191" id="Seg_1902" s="T190">oks-on</ta>
            <ta e="T192" id="Seg_1903" s="T191">ildʼ-en</ta>
            <ta e="T193" id="Seg_1904" s="T192">keːs-t-e</ta>
            <ta e="T194" id="Seg_1905" s="T193">bu</ta>
            <ta e="T195" id="Seg_1906" s="T194">emeːksin</ta>
            <ta e="T196" id="Seg_1907" s="T195">bu</ta>
            <ta e="T197" id="Seg_1908" s="T196">hahɨl-ɨ-n</ta>
            <ta e="T198" id="Seg_1909" s="T197">dʼe</ta>
            <ta e="T199" id="Seg_1910" s="T198">bat-t-a</ta>
            <ta e="T200" id="Seg_1911" s="T199">bu</ta>
            <ta e="T201" id="Seg_1912" s="T200">bat-an</ta>
            <ta e="T202" id="Seg_1913" s="T201">ih-en</ta>
            <ta e="T203" id="Seg_1914" s="T202">bu</ta>
            <ta e="T204" id="Seg_1915" s="T203">emeːksin</ta>
            <ta e="T205" id="Seg_1916" s="T204">di͡e-t-e</ta>
            <ta e="T206" id="Seg_1917" s="T205">bu</ta>
            <ta e="T207" id="Seg_1918" s="T206">emij-i-m</ta>
            <ta e="T208" id="Seg_1919" s="T207">bɨhɨːta</ta>
            <ta e="T209" id="Seg_1920" s="T208">ɨ͡arakan</ta>
            <ta e="T210" id="Seg_1921" s="T209">battɨː-r</ta>
            <ta e="T211" id="Seg_1922" s="T210">bɨhɨːta</ta>
            <ta e="T212" id="Seg_1923" s="T211">di͡e-n</ta>
            <ta e="T213" id="Seg_1924" s="T212">emij-i-n</ta>
            <ta e="T214" id="Seg_1925" s="T213">bɨh-an</ta>
            <ta e="T215" id="Seg_1926" s="T214">keːs-pit-e</ta>
            <ta e="T216" id="Seg_1927" s="T215">bu</ta>
            <ta e="T217" id="Seg_1928" s="T216">gɨn-an</ta>
            <ta e="T218" id="Seg_1929" s="T217">baran</ta>
            <ta e="T219" id="Seg_1930" s="T218">emi͡e</ta>
            <ta e="T220" id="Seg_1931" s="T219">bar-bɨt-a</ta>
            <ta e="T221" id="Seg_1932" s="T220">aŋaːr</ta>
            <ta e="T222" id="Seg_1933" s="T221">di͡egi</ta>
            <ta e="T223" id="Seg_1934" s="T222">emij-i-m</ta>
            <ta e="T224" id="Seg_1935" s="T223">emi͡e</ta>
            <ta e="T225" id="Seg_1936" s="T224">ɨ͡arakan</ta>
            <ta e="T226" id="Seg_1937" s="T225">duː</ta>
            <ta e="T227" id="Seg_1938" s="T226">di͡e-t</ta>
            <ta e="T228" id="Seg_1939" s="T227">bu</ta>
            <ta e="T229" id="Seg_1940" s="T228">emij-i</ta>
            <ta e="T230" id="Seg_1941" s="T229">emi͡e</ta>
            <ta e="T231" id="Seg_1942" s="T230">bɨh-an</ta>
            <ta e="T232" id="Seg_1943" s="T231">keːs-pit-e</ta>
            <ta e="T233" id="Seg_1944" s="T232">bu</ta>
            <ta e="T234" id="Seg_1945" s="T233">gɨn-an</ta>
            <ta e="T235" id="Seg_1946" s="T234">baran</ta>
            <ta e="T236" id="Seg_1947" s="T235">emi͡e</ta>
            <ta e="T237" id="Seg_1948" s="T236">bar-bɨt-a</ta>
            <ta e="T238" id="Seg_1949" s="T237">doː</ta>
            <ta e="T239" id="Seg_1950" s="T238">bu</ta>
            <ta e="T240" id="Seg_1951" s="T239">aŋaːr</ta>
            <ta e="T241" id="Seg_1952" s="T240">iliː-m</ta>
            <ta e="T242" id="Seg_1953" s="T241">mi͡ene</ta>
            <ta e="T243" id="Seg_1954" s="T242">ɨ͡arakan</ta>
            <ta e="T244" id="Seg_1955" s="T243">duː</ta>
            <ta e="T245" id="Seg_1956" s="T244">di͡e-n</ta>
            <ta e="T246" id="Seg_1957" s="T245">aŋaːr</ta>
            <ta e="T247" id="Seg_1958" s="T246">iliː-ti-n</ta>
            <ta e="T248" id="Seg_1959" s="T247">bɨs-t-a</ta>
            <ta e="T249" id="Seg_1960" s="T248">dʼe</ta>
            <ta e="T250" id="Seg_1961" s="T249">emi͡e</ta>
            <ta e="T251" id="Seg_1962" s="T250">bar-d-a</ta>
            <ta e="T252" id="Seg_1963" s="T251">bu</ta>
            <ta e="T253" id="Seg_1964" s="T252">emi͡e</ta>
            <ta e="T254" id="Seg_1965" s="T253">aŋaːr</ta>
            <ta e="T255" id="Seg_1966" s="T254">iliː-ti-n</ta>
            <ta e="T256" id="Seg_1967" s="T255">bɨs-t-a</ta>
            <ta e="T257" id="Seg_1968" s="T256">ikki</ta>
            <ta e="T258" id="Seg_1969" s="T257">iliː-te</ta>
            <ta e="T259" id="Seg_1970" s="T258">hu͡ok</ta>
            <ta e="T260" id="Seg_1971" s="T259">bu͡ol-l-a</ta>
            <ta e="T261" id="Seg_1972" s="T260">itigenne</ta>
            <ta e="T262" id="Seg_1973" s="T261">emeːksin</ta>
            <ta e="T263" id="Seg_1974" s="T262">ep-pit-e</ta>
            <ta e="T264" id="Seg_1975" s="T263">bu</ta>
            <ta e="T265" id="Seg_1976" s="T264">taŋara</ta>
            <ta e="T266" id="Seg_1977" s="T265">u͡ol-u-n</ta>
            <ta e="T267" id="Seg_1978" s="T266">gɨtta</ta>
            <ta e="T268" id="Seg_1979" s="T267">ugurah-ɨ͡ak-pɨ-n</ta>
            <ta e="T269" id="Seg_1980" s="T268">u͡oh-u-m</ta>
            <ta e="T270" id="Seg_1981" s="T269">ere</ta>
            <ta e="T271" id="Seg_1982" s="T270">bütün</ta>
            <ta e="T272" id="Seg_1983" s="T271">bu͡ol-lun</ta>
            <ta e="T273" id="Seg_1984" s="T272">onton</ta>
            <ta e="T274" id="Seg_1985" s="T273">bar-bɨt-a</ta>
            <ta e="T275" id="Seg_1986" s="T274">ele-te</ta>
            <ta e="T276" id="Seg_1987" s="T275">bu͡ol-an</ta>
            <ta e="T277" id="Seg_1988" s="T276">öl-büt-e</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_1989" s="T0">biːr</ta>
            <ta e="T2" id="Seg_1990" s="T1">hahɨl</ta>
            <ta e="T3" id="Seg_1991" s="T2">arɨː-GA</ta>
            <ta e="T4" id="Seg_1992" s="T3">kaːttar-BIT</ta>
            <ta e="T5" id="Seg_1993" s="T4">kanan</ta>
            <ta e="T6" id="Seg_1994" s="T5">da</ta>
            <ta e="T7" id="Seg_1995" s="T6">karbaː-An</ta>
            <ta e="T8" id="Seg_1996" s="T7">tagɨs-IAK-tI-n</ta>
            <ta e="T9" id="Seg_1997" s="T8">hu͡ok</ta>
            <ta e="T10" id="Seg_1998" s="T9">bu</ta>
            <ta e="T11" id="Seg_1999" s="T10">hahɨl-I-ŋ</ta>
            <ta e="T12" id="Seg_2000" s="T11">ɨtaː-TI-tA</ta>
            <ta e="T13" id="Seg_2001" s="T12">bu</ta>
            <ta e="T14" id="Seg_2002" s="T13">daːganɨ</ta>
            <ta e="T15" id="Seg_2003" s="T14">arɨː-GA</ta>
            <ta e="T16" id="Seg_2004" s="T15">kaːttar-TAK-BI-n</ta>
            <ta e="T17" id="Seg_2005" s="T16">aba-tI-n</ta>
            <ta e="T18" id="Seg_2006" s="T17">bu</ta>
            <ta e="T19" id="Seg_2007" s="T18">arɨː-GA</ta>
            <ta e="T20" id="Seg_2008" s="T19">kaːttar-BAT-I-m</ta>
            <ta e="T21" id="Seg_2009" s="T20">bu͡ol-TAR</ta>
            <ta e="T22" id="Seg_2010" s="T21">bu</ta>
            <ta e="T23" id="Seg_2011" s="T22">dulgaːn-LAr</ta>
            <ta e="T24" id="Seg_2012" s="T23">aja-LArI-n</ta>
            <ta e="T25" id="Seg_2013" s="T24">kiris-I-LArI-n</ta>
            <ta e="T26" id="Seg_2014" s="T25">hi͡e-IAK</ta>
            <ta e="T27" id="Seg_2015" s="T26">e-TI-m</ta>
            <ta e="T28" id="Seg_2016" s="T27">bu-GA</ta>
            <ta e="T29" id="Seg_2017" s="T28">uː</ta>
            <ta e="T30" id="Seg_2018" s="T29">is-tI-GAr</ta>
            <ta e="T31" id="Seg_2019" s="T30">haŋa</ta>
            <ta e="T32" id="Seg_2020" s="T31">ihilin-I-BIT</ta>
            <ta e="T33" id="Seg_2021" s="T32">agaj</ta>
            <ta e="T34" id="Seg_2022" s="T33">hɨ͡alɨhar-LAr</ta>
            <ta e="T35" id="Seg_2023" s="T34">haŋar-BIT-LAr</ta>
            <ta e="T36" id="Seg_2024" s="T35">aː</ta>
            <ta e="T37" id="Seg_2025" s="T36">ogo-LAr</ta>
            <ta e="T38" id="Seg_2026" s="T37">hahɨl-BIt</ta>
            <ta e="T39" id="Seg_2027" s="T38">ɨtaː-Ar</ta>
            <ta e="T40" id="Seg_2028" s="T39">e-BIT</ta>
            <ta e="T41" id="Seg_2029" s="T40">du͡o</ta>
            <ta e="T42" id="Seg_2030" s="T41">bu</ta>
            <ta e="T43" id="Seg_2031" s="T42">bu-nI</ta>
            <ta e="T44" id="Seg_2032" s="T43">ihit-An</ta>
            <ta e="T45" id="Seg_2033" s="T44">hahɨl</ta>
            <ta e="T46" id="Seg_2034" s="T45">di͡e-TI-tA</ta>
            <ta e="T47" id="Seg_2035" s="T46">hɨ͡alɨhar</ta>
            <ta e="T48" id="Seg_2036" s="T47">di͡e-Ar</ta>
            <ta e="T49" id="Seg_2037" s="T48">hɨrɨs-IAk</ta>
            <ta e="T50" id="Seg_2038" s="T49">hɨ͡alɨhar-LAr</ta>
            <ta e="T51" id="Seg_2039" s="T50">emi͡e</ta>
            <ta e="T52" id="Seg_2040" s="T51">dʼe</ta>
            <ta e="T53" id="Seg_2041" s="T52">hɨrɨs-IAk</ta>
            <ta e="T54" id="Seg_2042" s="T53">di͡e-BIT-LAr</ta>
            <ta e="T55" id="Seg_2043" s="T54">hahɨl-I-ŋ</ta>
            <ta e="T56" id="Seg_2044" s="T55">di͡e-TI-tA</ta>
            <ta e="T57" id="Seg_2045" s="T56">dʼe</ta>
            <ta e="T58" id="Seg_2046" s="T57">kɨtɨl-GA</ta>
            <ta e="T59" id="Seg_2047" s="T58">tigis-I-ŋ</ta>
            <ta e="T60" id="Seg_2048" s="T59">kas-GIt=Ij</ta>
            <ta e="T61" id="Seg_2049" s="T60">min</ta>
            <ta e="T62" id="Seg_2050" s="T61">aːk-IAK-m</ta>
            <ta e="T63" id="Seg_2051" s="T62">di͡e-TI-tA</ta>
            <ta e="T64" id="Seg_2052" s="T63">hahɨl</ta>
            <ta e="T65" id="Seg_2053" s="T64">kɨtɨl-GA</ta>
            <ta e="T66" id="Seg_2054" s="T65">kel-An</ta>
            <ta e="T67" id="Seg_2055" s="T66">tur-BIT-LArI-GAr</ta>
            <ta e="T68" id="Seg_2056" s="T67">ürüt-LArI-GAr</ta>
            <ta e="T69" id="Seg_2057" s="T68">oj-A</ta>
            <ta e="T70" id="Seg_2058" s="T69">hɨrɨt-TI-tA</ta>
            <ta e="T71" id="Seg_2059" s="T70">hahɨl</ta>
            <ta e="T72" id="Seg_2060" s="T71">aːk-An</ta>
            <ta e="T76" id="Seg_2061" s="T75">di͡e-An</ta>
            <ta e="T77" id="Seg_2062" s="T76">aksɨː-tI-n</ta>
            <ta e="T78" id="Seg_2063" s="T77">tɨl-tA</ta>
            <ta e="T79" id="Seg_2064" s="T78">iti</ta>
            <ta e="T80" id="Seg_2065" s="T79">dʼe</ta>
            <ta e="T81" id="Seg_2066" s="T80">bar-Ar-LAr</ta>
            <ta e="T82" id="Seg_2067" s="T81">hahɨl-I-ŋ</ta>
            <ta e="T83" id="Seg_2068" s="T82">kɨtɨl</ta>
            <ta e="T84" id="Seg_2069" s="T83">üstün</ta>
            <ta e="T85" id="Seg_2070" s="T84">bar-BIT</ta>
            <ta e="T86" id="Seg_2071" s="T85">balɨk-I-ŋ</ta>
            <ta e="T87" id="Seg_2072" s="T86">uː</ta>
            <ta e="T88" id="Seg_2073" s="T87">üstün</ta>
            <ta e="T89" id="Seg_2074" s="T88">bu</ta>
            <ta e="T90" id="Seg_2075" s="T89">hahɨl-I-ŋ</ta>
            <ta e="T91" id="Seg_2076" s="T90">köt-An</ta>
            <ta e="T92" id="Seg_2077" s="T91">is-An</ta>
            <ta e="T93" id="Seg_2078" s="T92">bar-An</ta>
            <ta e="T94" id="Seg_2079" s="T93">hɨ͡alɨhar-LAr</ta>
            <ta e="T95" id="Seg_2080" s="T94">di͡e-BIT</ta>
            <ta e="T96" id="Seg_2081" s="T95">ilin-tI-GAr</ta>
            <ta e="T97" id="Seg_2082" s="T96">u͡op</ta>
            <ta e="T98" id="Seg_2083" s="T97">di͡e-BIT-LAr</ta>
            <ta e="T99" id="Seg_2084" s="T98">ku͡ot-BIT-LAr</ta>
            <ta e="T100" id="Seg_2085" s="T99">uː</ta>
            <ta e="T101" id="Seg_2086" s="T100">da</ta>
            <ta e="T102" id="Seg_2087" s="T101">balɨk-tA</ta>
            <ta e="T103" id="Seg_2088" s="T102">bɨhɨj-tA</ta>
            <ta e="T104" id="Seg_2089" s="T103">da</ta>
            <ta e="T105" id="Seg_2090" s="T104">bert</ta>
            <ta e="T106" id="Seg_2091" s="T105">bu͡ol-Ar</ta>
            <ta e="T107" id="Seg_2092" s="T106">e-BIT</ta>
            <ta e="T108" id="Seg_2093" s="T107">di͡e-TI-tA</ta>
            <ta e="T109" id="Seg_2094" s="T108">innʼe</ta>
            <ta e="T110" id="Seg_2095" s="T109">gɨn-An</ta>
            <ta e="T111" id="Seg_2096" s="T110">baran</ta>
            <ta e="T112" id="Seg_2097" s="T111">emi͡e</ta>
            <ta e="T113" id="Seg_2098" s="T112">köt-TI-tA</ta>
            <ta e="T114" id="Seg_2099" s="T113">bu</ta>
            <ta e="T115" id="Seg_2100" s="T114">is-An</ta>
            <ta e="T116" id="Seg_2101" s="T115">biːr</ta>
            <ta e="T117" id="Seg_2102" s="T116">hir-GA</ta>
            <ta e="T118" id="Seg_2103" s="T117">uː</ta>
            <ta e="T119" id="Seg_2104" s="T118">is-TI-tA</ta>
            <ta e="T120" id="Seg_2105" s="T119">bu</ta>
            <ta e="T121" id="Seg_2106" s="T120">is-A-BIn</ta>
            <ta e="T122" id="Seg_2107" s="T121">di͡e-An</ta>
            <ta e="T123" id="Seg_2108" s="T122">hahɨl-I-ŋ</ta>
            <ta e="T124" id="Seg_2109" s="T123">tumnahɨn-An</ta>
            <ta e="T125" id="Seg_2110" s="T124">öl-An</ta>
            <ta e="T126" id="Seg_2111" s="T125">kaːl-TI-tA</ta>
            <ta e="T127" id="Seg_2112" s="T126">biːr</ta>
            <ta e="T128" id="Seg_2113" s="T127">emeːksin</ta>
            <ta e="T129" id="Seg_2114" s="T128">uː</ta>
            <ta e="T130" id="Seg_2115" s="T129">bahɨn-A</ta>
            <ta e="T131" id="Seg_2116" s="T130">kiːr-BIT</ta>
            <ta e="T132" id="Seg_2117" s="T131">agaj</ta>
            <ta e="T133" id="Seg_2118" s="T132">bu</ta>
            <ta e="T134" id="Seg_2119" s="T133">uː</ta>
            <ta e="T135" id="Seg_2120" s="T134">bas-A</ta>
            <ta e="T136" id="Seg_2121" s="T135">kiːr-BIT-tA</ta>
            <ta e="T137" id="Seg_2122" s="T136">hahɨl</ta>
            <ta e="T138" id="Seg_2123" s="T137">öl-A</ta>
            <ta e="T139" id="Seg_2124" s="T138">hɨt-Ar-tI-n</ta>
            <ta e="T140" id="Seg_2125" s="T139">bul-BIT</ta>
            <ta e="T141" id="Seg_2126" s="T140">bu</ta>
            <ta e="T142" id="Seg_2127" s="T141">emeːksin-I-ŋ</ta>
            <ta e="T143" id="Seg_2128" s="T142">bu</ta>
            <ta e="T144" id="Seg_2129" s="T143">hahɨl-nI</ta>
            <ta e="T145" id="Seg_2130" s="T144">ilt-An</ta>
            <ta e="T146" id="Seg_2131" s="T145">hül-I-n-TI-tA</ta>
            <ta e="T147" id="Seg_2132" s="T146">bu</ta>
            <ta e="T148" id="Seg_2133" s="T147">hül-An</ta>
            <ta e="T149" id="Seg_2134" s="T148">baran</ta>
            <ta e="T150" id="Seg_2135" s="T149">kuːrt-TI-tA</ta>
            <ta e="T151" id="Seg_2136" s="T150">bu</ta>
            <ta e="T152" id="Seg_2137" s="T151">gɨn-An</ta>
            <ta e="T153" id="Seg_2138" s="T152">baran</ta>
            <ta e="T154" id="Seg_2139" s="T153">araŋas-GA</ta>
            <ta e="T155" id="Seg_2140" s="T154">ɨjaː-TI-tA</ta>
            <ta e="T156" id="Seg_2141" s="T155">bu</ta>
            <ta e="T157" id="Seg_2142" s="T156">ɨjaː-A</ta>
            <ta e="T158" id="Seg_2143" s="T157">tur-An</ta>
            <ta e="T159" id="Seg_2144" s="T158">haŋar-TI-tA</ta>
            <ta e="T160" id="Seg_2145" s="T159">bu</ta>
            <ta e="T161" id="Seg_2146" s="T160">emeːksin</ta>
            <ta e="T162" id="Seg_2147" s="T161">hoguruː-ttAn</ta>
            <ta e="T163" id="Seg_2148" s="T162">tɨ͡al</ta>
            <ta e="T164" id="Seg_2149" s="T163">bu͡ol</ta>
            <ta e="T165" id="Seg_2150" s="T164">di͡e-TI-tA</ta>
            <ta e="T166" id="Seg_2151" s="T165">haːpat-ttAn</ta>
            <ta e="T167" id="Seg_2152" s="T166">tɨ͡al</ta>
            <ta e="T168" id="Seg_2153" s="T167">bu͡ol</ta>
            <ta e="T169" id="Seg_2154" s="T168">di͡e-TI-tA</ta>
            <ta e="T170" id="Seg_2155" s="T169">bu-tI-tA</ta>
            <ta e="T171" id="Seg_2156" s="T170">dʼe</ta>
            <ta e="T172" id="Seg_2157" s="T171">möŋ-TI-tA</ta>
            <ta e="T173" id="Seg_2158" s="T172">tɨ͡al</ta>
            <ta e="T174" id="Seg_2159" s="T173">kel-An</ta>
            <ta e="T175" id="Seg_2160" s="T174">dʼe</ta>
            <ta e="T176" id="Seg_2161" s="T175">bu</ta>
            <ta e="T177" id="Seg_2162" s="T176">hahɨl-nI</ta>
            <ta e="T178" id="Seg_2163" s="T177">mök-TAK-InA</ta>
            <ta e="T179" id="Seg_2164" s="T178">emeːksin</ta>
            <ta e="T180" id="Seg_2165" s="T179">hahɨl-tI-n</ta>
            <ta e="T181" id="Seg_2166" s="T180">kɨtta</ta>
            <ta e="T182" id="Seg_2167" s="T181">üŋküːleː-TI-tA</ta>
            <ta e="T183" id="Seg_2168" s="T182">huːraŋki haːraŋki</ta>
            <ta e="T184" id="Seg_2169" s="T183">di͡e-A-di͡e-A</ta>
            <ta e="T185" id="Seg_2170" s="T184">bu</ta>
            <ta e="T186" id="Seg_2171" s="T185">gɨn-A</ta>
            <ta e="T187" id="Seg_2172" s="T186">tur-TAK-InA</ta>
            <ta e="T188" id="Seg_2173" s="T187">hahɨl-tI-n</ta>
            <ta e="T189" id="Seg_2174" s="T188">tɨ͡al-tA</ta>
            <ta e="T190" id="Seg_2175" s="T189">bɨs-A</ta>
            <ta e="T191" id="Seg_2176" s="T190">ogus-An</ta>
            <ta e="T192" id="Seg_2177" s="T191">ilt-An</ta>
            <ta e="T193" id="Seg_2178" s="T192">keːs-TI-tA</ta>
            <ta e="T194" id="Seg_2179" s="T193">bu</ta>
            <ta e="T195" id="Seg_2180" s="T194">emeːksin</ta>
            <ta e="T196" id="Seg_2181" s="T195">bu</ta>
            <ta e="T197" id="Seg_2182" s="T196">hahɨl-tI-n</ta>
            <ta e="T198" id="Seg_2183" s="T197">dʼe</ta>
            <ta e="T199" id="Seg_2184" s="T198">bat-TI-tA</ta>
            <ta e="T200" id="Seg_2185" s="T199">bu</ta>
            <ta e="T201" id="Seg_2186" s="T200">bat-An</ta>
            <ta e="T202" id="Seg_2187" s="T201">is-An</ta>
            <ta e="T203" id="Seg_2188" s="T202">bu</ta>
            <ta e="T204" id="Seg_2189" s="T203">emeːksin</ta>
            <ta e="T205" id="Seg_2190" s="T204">di͡e-TI-tA</ta>
            <ta e="T206" id="Seg_2191" s="T205">bu</ta>
            <ta e="T207" id="Seg_2192" s="T206">emij-I-m</ta>
            <ta e="T208" id="Seg_2193" s="T207">bɨhɨːta</ta>
            <ta e="T209" id="Seg_2194" s="T208">ɨ͡arakan</ta>
            <ta e="T210" id="Seg_2195" s="T209">battaː-Ar</ta>
            <ta e="T211" id="Seg_2196" s="T210">bɨhɨːta</ta>
            <ta e="T212" id="Seg_2197" s="T211">di͡e-An</ta>
            <ta e="T213" id="Seg_2198" s="T212">emij-tI-n</ta>
            <ta e="T214" id="Seg_2199" s="T213">bɨs-An</ta>
            <ta e="T215" id="Seg_2200" s="T214">keːs-BIT-tA</ta>
            <ta e="T216" id="Seg_2201" s="T215">bu</ta>
            <ta e="T217" id="Seg_2202" s="T216">gɨn-An</ta>
            <ta e="T218" id="Seg_2203" s="T217">baran</ta>
            <ta e="T219" id="Seg_2204" s="T218">emi͡e</ta>
            <ta e="T220" id="Seg_2205" s="T219">bar-BIT-tA</ta>
            <ta e="T221" id="Seg_2206" s="T220">aŋar</ta>
            <ta e="T222" id="Seg_2207" s="T221">di͡egi</ta>
            <ta e="T223" id="Seg_2208" s="T222">emij-I-m</ta>
            <ta e="T224" id="Seg_2209" s="T223">emi͡e</ta>
            <ta e="T225" id="Seg_2210" s="T224">ɨ͡arakan</ta>
            <ta e="T226" id="Seg_2211" s="T225">du͡o</ta>
            <ta e="T227" id="Seg_2212" s="T226">di͡e-AːT</ta>
            <ta e="T228" id="Seg_2213" s="T227">bu</ta>
            <ta e="T229" id="Seg_2214" s="T228">emij-nI</ta>
            <ta e="T230" id="Seg_2215" s="T229">emi͡e</ta>
            <ta e="T231" id="Seg_2216" s="T230">bɨs-An</ta>
            <ta e="T232" id="Seg_2217" s="T231">keːs-BIT-tA</ta>
            <ta e="T233" id="Seg_2218" s="T232">bu</ta>
            <ta e="T234" id="Seg_2219" s="T233">gɨn-An</ta>
            <ta e="T235" id="Seg_2220" s="T234">baran</ta>
            <ta e="T236" id="Seg_2221" s="T235">emi͡e</ta>
            <ta e="T237" id="Seg_2222" s="T236">bar-BIT-tA</ta>
            <ta e="T238" id="Seg_2223" s="T237">doː</ta>
            <ta e="T239" id="Seg_2224" s="T238">bu</ta>
            <ta e="T240" id="Seg_2225" s="T239">aŋar</ta>
            <ta e="T241" id="Seg_2226" s="T240">iliː-m</ta>
            <ta e="T242" id="Seg_2227" s="T241">mini͡ene</ta>
            <ta e="T243" id="Seg_2228" s="T242">ɨ͡arakan</ta>
            <ta e="T244" id="Seg_2229" s="T243">du͡o</ta>
            <ta e="T245" id="Seg_2230" s="T244">di͡e-An</ta>
            <ta e="T246" id="Seg_2231" s="T245">aŋar</ta>
            <ta e="T247" id="Seg_2232" s="T246">iliː-tI-n</ta>
            <ta e="T248" id="Seg_2233" s="T247">bɨs-TI-tA</ta>
            <ta e="T249" id="Seg_2234" s="T248">dʼe</ta>
            <ta e="T250" id="Seg_2235" s="T249">emi͡e</ta>
            <ta e="T251" id="Seg_2236" s="T250">bar-TI-tA</ta>
            <ta e="T252" id="Seg_2237" s="T251">bu</ta>
            <ta e="T253" id="Seg_2238" s="T252">emi͡e</ta>
            <ta e="T254" id="Seg_2239" s="T253">aŋar</ta>
            <ta e="T255" id="Seg_2240" s="T254">iliː-tI-n</ta>
            <ta e="T256" id="Seg_2241" s="T255">bɨs-TI-tA</ta>
            <ta e="T257" id="Seg_2242" s="T256">ikki</ta>
            <ta e="T258" id="Seg_2243" s="T257">iliː-tA</ta>
            <ta e="T259" id="Seg_2244" s="T258">hu͡ok</ta>
            <ta e="T260" id="Seg_2245" s="T259">bu͡ol-TI-tA</ta>
            <ta e="T261" id="Seg_2246" s="T260">itigenne</ta>
            <ta e="T262" id="Seg_2247" s="T261">emeːksin</ta>
            <ta e="T263" id="Seg_2248" s="T262">et-BIT-tA</ta>
            <ta e="T264" id="Seg_2249" s="T263">bu</ta>
            <ta e="T265" id="Seg_2250" s="T264">taŋara</ta>
            <ta e="T266" id="Seg_2251" s="T265">u͡ol-tI-n</ta>
            <ta e="T267" id="Seg_2252" s="T266">kɨtta</ta>
            <ta e="T268" id="Seg_2253" s="T267">uguras-IAK-BI-n</ta>
            <ta e="T269" id="Seg_2254" s="T268">u͡os-I-m</ta>
            <ta e="T270" id="Seg_2255" s="T269">ere</ta>
            <ta e="T271" id="Seg_2256" s="T270">bütün</ta>
            <ta e="T272" id="Seg_2257" s="T271">bu͡ol-TIn</ta>
            <ta e="T273" id="Seg_2258" s="T272">onton</ta>
            <ta e="T274" id="Seg_2259" s="T273">bar-BIT-tA</ta>
            <ta e="T275" id="Seg_2260" s="T274">ele-tA</ta>
            <ta e="T276" id="Seg_2261" s="T275">bu͡ol-An</ta>
            <ta e="T277" id="Seg_2262" s="T276">öl-BIT-tA</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_2263" s="T0">one</ta>
            <ta e="T2" id="Seg_2264" s="T1">fox.[NOM]</ta>
            <ta e="T3" id="Seg_2265" s="T2">island-DAT/LOC</ta>
            <ta e="T4" id="Seg_2266" s="T3">get.in-PST2.[3SG]</ta>
            <ta e="T5" id="Seg_2267" s="T4">which.way</ta>
            <ta e="T6" id="Seg_2268" s="T5">NEG</ta>
            <ta e="T7" id="Seg_2269" s="T6">swim-CVB.SEQ</ta>
            <ta e="T8" id="Seg_2270" s="T7">go.out-PTCP.FUT-3SG-ACC</ta>
            <ta e="T9" id="Seg_2271" s="T8">NEG</ta>
            <ta e="T10" id="Seg_2272" s="T9">this</ta>
            <ta e="T11" id="Seg_2273" s="T10">fox-EP-2SG.[NOM]</ta>
            <ta e="T12" id="Seg_2274" s="T11">cry-PST1-3SG</ta>
            <ta e="T13" id="Seg_2275" s="T12">this</ta>
            <ta e="T14" id="Seg_2276" s="T13">even.if</ta>
            <ta e="T15" id="Seg_2277" s="T14">island-DAT/LOC</ta>
            <ta e="T16" id="Seg_2278" s="T15">get.in-PTCP.COND-1SG-GEN</ta>
            <ta e="T17" id="Seg_2279" s="T16">annoyance-3SG-ACC</ta>
            <ta e="T18" id="Seg_2280" s="T17">this</ta>
            <ta e="T19" id="Seg_2281" s="T18">island-DAT/LOC</ta>
            <ta e="T20" id="Seg_2282" s="T19">get.in-NEG.PTCP-EP-1SG.[NOM]</ta>
            <ta e="T21" id="Seg_2283" s="T20">be-COND.[3SG]</ta>
            <ta e="T22" id="Seg_2284" s="T21">this</ta>
            <ta e="T23" id="Seg_2285" s="T22">dolgan-PL.[NOM]</ta>
            <ta e="T24" id="Seg_2286" s="T23">self.shooting.bow-3PL-GEN</ta>
            <ta e="T25" id="Seg_2287" s="T24">bowstring-EP-3PL-ACC</ta>
            <ta e="T26" id="Seg_2288" s="T25">eat-PTCP.FUT</ta>
            <ta e="T27" id="Seg_2289" s="T26">be-PST1-1SG</ta>
            <ta e="T28" id="Seg_2290" s="T27">this-DAT/LOC</ta>
            <ta e="T29" id="Seg_2291" s="T28">water.[NOM]</ta>
            <ta e="T30" id="Seg_2292" s="T29">inside-3SG-DAT/LOC</ta>
            <ta e="T31" id="Seg_2293" s="T30">voice.[NOM]</ta>
            <ta e="T32" id="Seg_2294" s="T31">be.heard-EP-PST2.[3SG]</ta>
            <ta e="T33" id="Seg_2295" s="T32">only</ta>
            <ta e="T34" id="Seg_2296" s="T33">eelpout-PL.[NOM]</ta>
            <ta e="T35" id="Seg_2297" s="T34">speak-PST2-3PL</ta>
            <ta e="T36" id="Seg_2298" s="T35">ah</ta>
            <ta e="T37" id="Seg_2299" s="T36">child-PL.[NOM]</ta>
            <ta e="T38" id="Seg_2300" s="T37">fox-1PL.[NOM]</ta>
            <ta e="T39" id="Seg_2301" s="T38">cry-PTCP.PRS</ta>
            <ta e="T40" id="Seg_2302" s="T39">be-PST2.[3SG]</ta>
            <ta e="T41" id="Seg_2303" s="T40">Q</ta>
            <ta e="T42" id="Seg_2304" s="T41">this</ta>
            <ta e="T43" id="Seg_2305" s="T42">this-ACC</ta>
            <ta e="T44" id="Seg_2306" s="T43">hear-CVB.SEQ</ta>
            <ta e="T45" id="Seg_2307" s="T44">fox.[NOM]</ta>
            <ta e="T46" id="Seg_2308" s="T45">say-PST1-3SG</ta>
            <ta e="T47" id="Seg_2309" s="T46">eelpout.[NOM]</ta>
            <ta e="T48" id="Seg_2310" s="T47">say-PRS.[3SG]</ta>
            <ta e="T49" id="Seg_2311" s="T48">run.a.race-IMP.1DU</ta>
            <ta e="T50" id="Seg_2312" s="T49">eelpout-PL.[NOM]</ta>
            <ta e="T51" id="Seg_2313" s="T50">also</ta>
            <ta e="T52" id="Seg_2314" s="T51">well</ta>
            <ta e="T53" id="Seg_2315" s="T52">run.a.race-IMP.1DU</ta>
            <ta e="T54" id="Seg_2316" s="T53">say-PST2-3PL</ta>
            <ta e="T55" id="Seg_2317" s="T54">fox-EP-2SG.[NOM]</ta>
            <ta e="T56" id="Seg_2318" s="T55">say-PST1-3SG</ta>
            <ta e="T57" id="Seg_2319" s="T56">hey</ta>
            <ta e="T58" id="Seg_2320" s="T57">shore-DAT/LOC</ta>
            <ta e="T59" id="Seg_2321" s="T58">come.close-EP-IMP.2PL</ta>
            <ta e="T60" id="Seg_2322" s="T59">how.much-2PL=Q</ta>
            <ta e="T61" id="Seg_2323" s="T60">1SG.[NOM]</ta>
            <ta e="T62" id="Seg_2324" s="T61">count-FUT-1SG</ta>
            <ta e="T63" id="Seg_2325" s="T62">say-PST1-3SG</ta>
            <ta e="T64" id="Seg_2326" s="T63">fox.[NOM]</ta>
            <ta e="T65" id="Seg_2327" s="T64">shore-DAT/LOC</ta>
            <ta e="T66" id="Seg_2328" s="T65">come-CVB.SEQ</ta>
            <ta e="T67" id="Seg_2329" s="T66">stand-PTCP.PST-3PL-DAT/LOC</ta>
            <ta e="T68" id="Seg_2330" s="T67">upper.part-3PL-DAT/LOC</ta>
            <ta e="T69" id="Seg_2331" s="T68">jump-CVB.SIM</ta>
            <ta e="T70" id="Seg_2332" s="T69">go-PST1-3SG</ta>
            <ta e="T71" id="Seg_2333" s="T70">fox.[NOM]</ta>
            <ta e="T72" id="Seg_2334" s="T71">count-CVB.SEQ</ta>
            <ta e="T76" id="Seg_2335" s="T75">say-CVB.SEQ</ta>
            <ta e="T77" id="Seg_2336" s="T76">calculation-3SG-GEN</ta>
            <ta e="T78" id="Seg_2337" s="T77">word-3SG.[NOM]</ta>
            <ta e="T79" id="Seg_2338" s="T78">that.[NOM]</ta>
            <ta e="T80" id="Seg_2339" s="T79">well</ta>
            <ta e="T81" id="Seg_2340" s="T80">go-PRS-3PL</ta>
            <ta e="T82" id="Seg_2341" s="T81">fox-EP-2SG.[NOM]</ta>
            <ta e="T83" id="Seg_2342" s="T82">shore.[NOM]</ta>
            <ta e="T84" id="Seg_2343" s="T83">along</ta>
            <ta e="T85" id="Seg_2344" s="T84">go-PST2.[3SG]</ta>
            <ta e="T86" id="Seg_2345" s="T85">fish-EP-2SG.[NOM]</ta>
            <ta e="T87" id="Seg_2346" s="T86">water.[NOM]</ta>
            <ta e="T88" id="Seg_2347" s="T87">through</ta>
            <ta e="T89" id="Seg_2348" s="T88">this</ta>
            <ta e="T90" id="Seg_2349" s="T89">fox-EP-2SG.[NOM]</ta>
            <ta e="T91" id="Seg_2350" s="T90">run-CVB.SEQ</ta>
            <ta e="T92" id="Seg_2351" s="T91">go-CVB.SEQ</ta>
            <ta e="T93" id="Seg_2352" s="T92">go-CVB.SEQ</ta>
            <ta e="T94" id="Seg_2353" s="T93">eelpout-PL.[NOM]</ta>
            <ta e="T95" id="Seg_2354" s="T94">say-PST2.[3SG]</ta>
            <ta e="T96" id="Seg_2355" s="T95">front-3SG-DAT/LOC</ta>
            <ta e="T97" id="Seg_2356" s="T96">oops</ta>
            <ta e="T98" id="Seg_2357" s="T97">say-PST2-3PL</ta>
            <ta e="T99" id="Seg_2358" s="T98">overtake-PST2-3PL</ta>
            <ta e="T100" id="Seg_2359" s="T99">water.[NOM]</ta>
            <ta e="T101" id="Seg_2360" s="T100">and</ta>
            <ta e="T102" id="Seg_2361" s="T101">fish-3SG.[NOM]</ta>
            <ta e="T103" id="Seg_2362" s="T102">agile-3SG.[NOM]</ta>
            <ta e="T104" id="Seg_2363" s="T103">and</ta>
            <ta e="T105" id="Seg_2364" s="T104">very</ta>
            <ta e="T106" id="Seg_2365" s="T105">be-PTCP.PRS</ta>
            <ta e="T107" id="Seg_2366" s="T106">be-PST2.[3SG]</ta>
            <ta e="T108" id="Seg_2367" s="T107">say-PST1-3SG</ta>
            <ta e="T109" id="Seg_2368" s="T108">so</ta>
            <ta e="T110" id="Seg_2369" s="T109">make-CVB.SEQ</ta>
            <ta e="T111" id="Seg_2370" s="T110">after</ta>
            <ta e="T112" id="Seg_2371" s="T111">again</ta>
            <ta e="T113" id="Seg_2372" s="T112">run-PST1-3SG</ta>
            <ta e="T114" id="Seg_2373" s="T113">this</ta>
            <ta e="T115" id="Seg_2374" s="T114">go-CVB.SEQ</ta>
            <ta e="T116" id="Seg_2375" s="T115">one</ta>
            <ta e="T117" id="Seg_2376" s="T116">place-DAT/LOC</ta>
            <ta e="T118" id="Seg_2377" s="T117">water.[NOM]</ta>
            <ta e="T119" id="Seg_2378" s="T118">drink-PST1-3SG</ta>
            <ta e="T120" id="Seg_2379" s="T119">this</ta>
            <ta e="T121" id="Seg_2380" s="T120">drink-PRS-1SG</ta>
            <ta e="T122" id="Seg_2381" s="T121">say-CVB.SEQ</ta>
            <ta e="T123" id="Seg_2382" s="T122">fox-EP-2SG.[NOM]</ta>
            <ta e="T124" id="Seg_2383" s="T123">choke-CVB.SEQ</ta>
            <ta e="T125" id="Seg_2384" s="T124">die-CVB.SEQ</ta>
            <ta e="T126" id="Seg_2385" s="T125">stay-PST1-3SG</ta>
            <ta e="T127" id="Seg_2386" s="T126">one</ta>
            <ta e="T128" id="Seg_2387" s="T127">old.woman.[NOM]</ta>
            <ta e="T129" id="Seg_2388" s="T128">water.[NOM]</ta>
            <ta e="T130" id="Seg_2389" s="T129">scoop-CVB.SIM</ta>
            <ta e="T131" id="Seg_2390" s="T130">go.in-PST2.[3SG]</ta>
            <ta e="T132" id="Seg_2391" s="T131">only</ta>
            <ta e="T133" id="Seg_2392" s="T132">this</ta>
            <ta e="T134" id="Seg_2393" s="T133">water.[NOM]</ta>
            <ta e="T135" id="Seg_2394" s="T134">scoop-CVB.SIM</ta>
            <ta e="T136" id="Seg_2395" s="T135">go.in-PST2-3SG</ta>
            <ta e="T137" id="Seg_2396" s="T136">fox.[NOM]</ta>
            <ta e="T138" id="Seg_2397" s="T137">die-CVB.SIM</ta>
            <ta e="T139" id="Seg_2398" s="T138">lie-PTCP.PRS-3SG-ACC</ta>
            <ta e="T140" id="Seg_2399" s="T139">find-PST2.[3SG]</ta>
            <ta e="T141" id="Seg_2400" s="T140">this</ta>
            <ta e="T142" id="Seg_2401" s="T141">old.woman-EP-2SG.[NOM]</ta>
            <ta e="T143" id="Seg_2402" s="T142">this</ta>
            <ta e="T144" id="Seg_2403" s="T143">fox-ACC</ta>
            <ta e="T145" id="Seg_2404" s="T144">carry-CVB.SEQ</ta>
            <ta e="T146" id="Seg_2405" s="T145">skin-EP-MED-PST1-3SG</ta>
            <ta e="T147" id="Seg_2406" s="T146">this</ta>
            <ta e="T148" id="Seg_2407" s="T147">skin-CVB.SEQ</ta>
            <ta e="T149" id="Seg_2408" s="T148">after</ta>
            <ta e="T150" id="Seg_2409" s="T149">dry-PST1-3SG</ta>
            <ta e="T151" id="Seg_2410" s="T150">this</ta>
            <ta e="T152" id="Seg_2411" s="T151">make-CVB.SEQ</ta>
            <ta e="T153" id="Seg_2412" s="T152">after</ta>
            <ta e="T154" id="Seg_2413" s="T153">storage-DAT/LOC</ta>
            <ta e="T155" id="Seg_2414" s="T154">hang.up-PST1-3SG</ta>
            <ta e="T156" id="Seg_2415" s="T155">this</ta>
            <ta e="T157" id="Seg_2416" s="T156">hang.up-CVB.SIM</ta>
            <ta e="T158" id="Seg_2417" s="T157">stand-CVB.SEQ</ta>
            <ta e="T159" id="Seg_2418" s="T158">speak-PST1-3SG</ta>
            <ta e="T160" id="Seg_2419" s="T159">this</ta>
            <ta e="T161" id="Seg_2420" s="T160">old.woman.[NOM]</ta>
            <ta e="T162" id="Seg_2421" s="T161">south-ABL</ta>
            <ta e="T163" id="Seg_2422" s="T162">wind.[NOM]</ta>
            <ta e="T164" id="Seg_2423" s="T163">be.[IMP.2SG]</ta>
            <ta e="T165" id="Seg_2424" s="T164">say-PST1-3SG</ta>
            <ta e="T166" id="Seg_2425" s="T165">west-ABL</ta>
            <ta e="T167" id="Seg_2426" s="T166">wind.[NOM]</ta>
            <ta e="T168" id="Seg_2427" s="T167">be.[IMP.2SG]</ta>
            <ta e="T169" id="Seg_2428" s="T168">say-PST1-3SG</ta>
            <ta e="T170" id="Seg_2429" s="T169">this-3SG-3SG.[NOM]</ta>
            <ta e="T171" id="Seg_2430" s="T170">well</ta>
            <ta e="T172" id="Seg_2431" s="T171">move-PST1-3SG</ta>
            <ta e="T173" id="Seg_2432" s="T172">wind.[NOM]</ta>
            <ta e="T174" id="Seg_2433" s="T173">come-CVB.SEQ</ta>
            <ta e="T175" id="Seg_2434" s="T174">well</ta>
            <ta e="T176" id="Seg_2435" s="T175">this</ta>
            <ta e="T177" id="Seg_2436" s="T176">fox-ACC</ta>
            <ta e="T178" id="Seg_2437" s="T177">throw.oneself-TEMP-3SG</ta>
            <ta e="T179" id="Seg_2438" s="T178">old.woman.[NOM]</ta>
            <ta e="T180" id="Seg_2439" s="T179">fox-3SG-ACC</ta>
            <ta e="T181" id="Seg_2440" s="T180">with</ta>
            <ta e="T182" id="Seg_2441" s="T181">dance-PST1-3SG</ta>
            <ta e="T183" id="Seg_2442" s="T182">huuranki_haaranki</ta>
            <ta e="T184" id="Seg_2443" s="T183">say-CVB.SIM-say-CVB.SIM</ta>
            <ta e="T185" id="Seg_2444" s="T184">this</ta>
            <ta e="T186" id="Seg_2445" s="T185">make-CVB.SIM</ta>
            <ta e="T187" id="Seg_2446" s="T186">stand-TEMP-3SG</ta>
            <ta e="T188" id="Seg_2447" s="T187">fox-3SG-ACC</ta>
            <ta e="T189" id="Seg_2448" s="T188">wind-3SG.[NOM]</ta>
            <ta e="T190" id="Seg_2449" s="T189">cut-CVB.SIM</ta>
            <ta e="T191" id="Seg_2450" s="T190">beat-CVB.SEQ</ta>
            <ta e="T192" id="Seg_2451" s="T191">carry-CVB.SEQ</ta>
            <ta e="T193" id="Seg_2452" s="T192">throw-PST1-3SG</ta>
            <ta e="T194" id="Seg_2453" s="T193">this</ta>
            <ta e="T195" id="Seg_2454" s="T194">old.woman.[NOM]</ta>
            <ta e="T196" id="Seg_2455" s="T195">this</ta>
            <ta e="T197" id="Seg_2456" s="T196">fox-3SG-ACC</ta>
            <ta e="T198" id="Seg_2457" s="T197">well</ta>
            <ta e="T199" id="Seg_2458" s="T198">follow-PST1-3SG</ta>
            <ta e="T200" id="Seg_2459" s="T199">this</ta>
            <ta e="T201" id="Seg_2460" s="T200">follow-CVB.SEQ</ta>
            <ta e="T202" id="Seg_2461" s="T201">go-CVB.SEQ</ta>
            <ta e="T203" id="Seg_2462" s="T202">this</ta>
            <ta e="T204" id="Seg_2463" s="T203">old.woman.[NOM]</ta>
            <ta e="T205" id="Seg_2464" s="T204">say-PST1-3SG</ta>
            <ta e="T206" id="Seg_2465" s="T205">this</ta>
            <ta e="T207" id="Seg_2466" s="T206">bosoms-EP-1SG.[NOM]</ta>
            <ta e="T208" id="Seg_2467" s="T207">it.seems</ta>
            <ta e="T209" id="Seg_2468" s="T208">heavy.[NOM]</ta>
            <ta e="T210" id="Seg_2469" s="T209">pull.down-PRS.[3SG]</ta>
            <ta e="T211" id="Seg_2470" s="T210">it.seems</ta>
            <ta e="T212" id="Seg_2471" s="T211">say-CVB.SEQ</ta>
            <ta e="T213" id="Seg_2472" s="T212">bosoms-3SG-ACC</ta>
            <ta e="T214" id="Seg_2473" s="T213">cut-CVB.SEQ</ta>
            <ta e="T215" id="Seg_2474" s="T214">throw-PST2-3SG</ta>
            <ta e="T216" id="Seg_2475" s="T215">this</ta>
            <ta e="T217" id="Seg_2476" s="T216">make-CVB.SEQ</ta>
            <ta e="T218" id="Seg_2477" s="T217">after</ta>
            <ta e="T219" id="Seg_2478" s="T218">again</ta>
            <ta e="T220" id="Seg_2479" s="T219">go-PST2-3SG</ta>
            <ta e="T221" id="Seg_2480" s="T220">other.of.two</ta>
            <ta e="T222" id="Seg_2481" s="T221">side.[NOM]</ta>
            <ta e="T223" id="Seg_2482" s="T222">bosoms-EP-1SG.[NOM]</ta>
            <ta e="T224" id="Seg_2483" s="T223">also</ta>
            <ta e="T225" id="Seg_2484" s="T224">heavy.[NOM]</ta>
            <ta e="T226" id="Seg_2485" s="T225">Q</ta>
            <ta e="T227" id="Seg_2486" s="T226">say-CVB.ANT</ta>
            <ta e="T228" id="Seg_2487" s="T227">this</ta>
            <ta e="T229" id="Seg_2488" s="T228">bosoms-ACC</ta>
            <ta e="T230" id="Seg_2489" s="T229">also</ta>
            <ta e="T231" id="Seg_2490" s="T230">cut-CVB.SEQ</ta>
            <ta e="T232" id="Seg_2491" s="T231">throw-PST2-3SG</ta>
            <ta e="T233" id="Seg_2492" s="T232">this</ta>
            <ta e="T234" id="Seg_2493" s="T233">make-CVB.SEQ</ta>
            <ta e="T235" id="Seg_2494" s="T234">after</ta>
            <ta e="T236" id="Seg_2495" s="T235">again</ta>
            <ta e="T237" id="Seg_2496" s="T236">go-PST2-3SG</ta>
            <ta e="T238" id="Seg_2497" s="T237">well</ta>
            <ta e="T239" id="Seg_2498" s="T238">this</ta>
            <ta e="T240" id="Seg_2499" s="T239">half.[NOM]</ta>
            <ta e="T241" id="Seg_2500" s="T240">hand-1SG.[NOM]</ta>
            <ta e="T242" id="Seg_2501" s="T241">my</ta>
            <ta e="T243" id="Seg_2502" s="T242">heavy.[NOM]</ta>
            <ta e="T244" id="Seg_2503" s="T243">Q</ta>
            <ta e="T245" id="Seg_2504" s="T244">say-CVB.SEQ</ta>
            <ta e="T246" id="Seg_2505" s="T245">half.[NOM]</ta>
            <ta e="T247" id="Seg_2506" s="T246">hand-3SG-ACC</ta>
            <ta e="T248" id="Seg_2507" s="T247">cut-PST1-3SG</ta>
            <ta e="T249" id="Seg_2508" s="T248">well</ta>
            <ta e="T250" id="Seg_2509" s="T249">again</ta>
            <ta e="T251" id="Seg_2510" s="T250">go-PST1-3SG</ta>
            <ta e="T252" id="Seg_2511" s="T251">this</ta>
            <ta e="T253" id="Seg_2512" s="T252">also</ta>
            <ta e="T254" id="Seg_2513" s="T253">other.of.two</ta>
            <ta e="T255" id="Seg_2514" s="T254">hand-3SG-ACC</ta>
            <ta e="T256" id="Seg_2515" s="T255">cut-PST1-3SG</ta>
            <ta e="T257" id="Seg_2516" s="T256">two</ta>
            <ta e="T258" id="Seg_2517" s="T257">hand-POSS</ta>
            <ta e="T259" id="Seg_2518" s="T258">NEG</ta>
            <ta e="T260" id="Seg_2519" s="T259">be-PST1-3SG</ta>
            <ta e="T261" id="Seg_2520" s="T260">after.that</ta>
            <ta e="T262" id="Seg_2521" s="T261">old.woman.[NOM]</ta>
            <ta e="T263" id="Seg_2522" s="T262">speak-PST2-3SG</ta>
            <ta e="T264" id="Seg_2523" s="T263">this</ta>
            <ta e="T265" id="Seg_2524" s="T264">sky.[NOM]</ta>
            <ta e="T266" id="Seg_2525" s="T265">son-3SG-ACC</ta>
            <ta e="T267" id="Seg_2526" s="T266">with</ta>
            <ta e="T268" id="Seg_2527" s="T267">kiss-PTCP.FUT-1SG-ACC</ta>
            <ta e="T269" id="Seg_2528" s="T268">lip-EP-1SG.[NOM]</ta>
            <ta e="T270" id="Seg_2529" s="T269">just</ta>
            <ta e="T271" id="Seg_2530" s="T270">intact.[NOM]</ta>
            <ta e="T272" id="Seg_2531" s="T271">be-IMP.3SG</ta>
            <ta e="T273" id="Seg_2532" s="T272">then</ta>
            <ta e="T274" id="Seg_2533" s="T273">go-PST2-3SG</ta>
            <ta e="T275" id="Seg_2534" s="T274">last-3SG.[NOM]</ta>
            <ta e="T276" id="Seg_2535" s="T275">be-CVB.SEQ</ta>
            <ta e="T277" id="Seg_2536" s="T276">die-PST2-3SG</ta>
         </annotation>
         <annotation name="gg" tierref="gd">
            <ta e="T1" id="Seg_2537" s="T0">eins</ta>
            <ta e="T2" id="Seg_2538" s="T1">Fuchs.[NOM]</ta>
            <ta e="T3" id="Seg_2539" s="T2">Insel-DAT/LOC</ta>
            <ta e="T4" id="Seg_2540" s="T3">geraten-PST2.[3SG]</ta>
            <ta e="T5" id="Seg_2541" s="T4">wo.entlang</ta>
            <ta e="T6" id="Seg_2542" s="T5">NEG</ta>
            <ta e="T7" id="Seg_2543" s="T6">schwimmen-CVB.SEQ</ta>
            <ta e="T8" id="Seg_2544" s="T7">hinausgehen-PTCP.FUT-3SG-ACC</ta>
            <ta e="T9" id="Seg_2545" s="T8">NEG</ta>
            <ta e="T10" id="Seg_2546" s="T9">dieses</ta>
            <ta e="T11" id="Seg_2547" s="T10">Fuchs-EP-2SG.[NOM]</ta>
            <ta e="T12" id="Seg_2548" s="T11">weinen-PST1-3SG</ta>
            <ta e="T13" id="Seg_2549" s="T12">dieses</ta>
            <ta e="T14" id="Seg_2550" s="T13">wenn.auch</ta>
            <ta e="T15" id="Seg_2551" s="T14">Insel-DAT/LOC</ta>
            <ta e="T16" id="Seg_2552" s="T15">geraten-PTCP.COND-1SG-GEN</ta>
            <ta e="T17" id="Seg_2553" s="T16">Ärger-3SG-ACC</ta>
            <ta e="T18" id="Seg_2554" s="T17">dieses</ta>
            <ta e="T19" id="Seg_2555" s="T18">Insel-DAT/LOC</ta>
            <ta e="T20" id="Seg_2556" s="T19">geraten-NEG.PTCP-EP-1SG.[NOM]</ta>
            <ta e="T21" id="Seg_2557" s="T20">sein-COND.[3SG]</ta>
            <ta e="T22" id="Seg_2558" s="T21">dieses</ta>
            <ta e="T23" id="Seg_2559" s="T22">Dolgane-PL.[NOM]</ta>
            <ta e="T24" id="Seg_2560" s="T23">Selbstschussbogen-3PL-GEN</ta>
            <ta e="T25" id="Seg_2561" s="T24">Bogensehne-EP-3PL-ACC</ta>
            <ta e="T26" id="Seg_2562" s="T25">essen-PTCP.FUT</ta>
            <ta e="T27" id="Seg_2563" s="T26">sein-PST1-1SG</ta>
            <ta e="T28" id="Seg_2564" s="T27">dieses-DAT/LOC</ta>
            <ta e="T29" id="Seg_2565" s="T28">Wasser.[NOM]</ta>
            <ta e="T30" id="Seg_2566" s="T29">Inneres-3SG-DAT/LOC</ta>
            <ta e="T31" id="Seg_2567" s="T30">Stimme.[NOM]</ta>
            <ta e="T32" id="Seg_2568" s="T31">gehört.werden-EP-PST2.[3SG]</ta>
            <ta e="T33" id="Seg_2569" s="T32">nur</ta>
            <ta e="T34" id="Seg_2570" s="T33">Aalquappe-PL.[NOM]</ta>
            <ta e="T35" id="Seg_2571" s="T34">sprechen-PST2-3PL</ta>
            <ta e="T36" id="Seg_2572" s="T35">ah</ta>
            <ta e="T37" id="Seg_2573" s="T36">Kind-PL.[NOM]</ta>
            <ta e="T38" id="Seg_2574" s="T37">Fuchs-1PL.[NOM]</ta>
            <ta e="T39" id="Seg_2575" s="T38">weinen-PTCP.PRS</ta>
            <ta e="T40" id="Seg_2576" s="T39">sein-PST2.[3SG]</ta>
            <ta e="T41" id="Seg_2577" s="T40">Q</ta>
            <ta e="T42" id="Seg_2578" s="T41">dieses</ta>
            <ta e="T43" id="Seg_2579" s="T42">dieses-ACC</ta>
            <ta e="T44" id="Seg_2580" s="T43">hören-CVB.SEQ</ta>
            <ta e="T45" id="Seg_2581" s="T44">Fuchs.[NOM]</ta>
            <ta e="T46" id="Seg_2582" s="T45">sagen-PST1-3SG</ta>
            <ta e="T47" id="Seg_2583" s="T46">Aalquappe.[NOM]</ta>
            <ta e="T48" id="Seg_2584" s="T47">sagen-PRS.[3SG]</ta>
            <ta e="T49" id="Seg_2585" s="T48">um.die.Wette.laufen-IMP.1DU</ta>
            <ta e="T50" id="Seg_2586" s="T49">Aalquappe-PL.[NOM]</ta>
            <ta e="T51" id="Seg_2587" s="T50">auch</ta>
            <ta e="T52" id="Seg_2588" s="T51">doch</ta>
            <ta e="T53" id="Seg_2589" s="T52">um.die.Wette.laufen-IMP.1DU</ta>
            <ta e="T54" id="Seg_2590" s="T53">sagen-PST2-3PL</ta>
            <ta e="T55" id="Seg_2591" s="T54">Fuchs-EP-2SG.[NOM]</ta>
            <ta e="T56" id="Seg_2592" s="T55">sagen-PST1-3SG</ta>
            <ta e="T57" id="Seg_2593" s="T56">nun</ta>
            <ta e="T58" id="Seg_2594" s="T57">Ufer-DAT/LOC</ta>
            <ta e="T59" id="Seg_2595" s="T58">herankommen-EP-IMP.2PL</ta>
            <ta e="T60" id="Seg_2596" s="T59">wie.viel-2PL=Q</ta>
            <ta e="T61" id="Seg_2597" s="T60">1SG.[NOM]</ta>
            <ta e="T62" id="Seg_2598" s="T61">zählen-FUT-1SG</ta>
            <ta e="T63" id="Seg_2599" s="T62">sagen-PST1-3SG</ta>
            <ta e="T64" id="Seg_2600" s="T63">Fuchs.[NOM]</ta>
            <ta e="T65" id="Seg_2601" s="T64">Ufer-DAT/LOC</ta>
            <ta e="T66" id="Seg_2602" s="T65">kommen-CVB.SEQ</ta>
            <ta e="T67" id="Seg_2603" s="T66">stehen-PTCP.PST-3PL-DAT/LOC</ta>
            <ta e="T68" id="Seg_2604" s="T67">oberer.Teil-3PL-DAT/LOC</ta>
            <ta e="T69" id="Seg_2605" s="T68">springen-CVB.SIM</ta>
            <ta e="T70" id="Seg_2606" s="T69">gehen-PST1-3SG</ta>
            <ta e="T71" id="Seg_2607" s="T70">Fuchs.[NOM]</ta>
            <ta e="T72" id="Seg_2608" s="T71">zählen-CVB.SEQ</ta>
            <ta e="T76" id="Seg_2609" s="T75">sagen-CVB.SEQ</ta>
            <ta e="T77" id="Seg_2610" s="T76">Rechnung-3SG-GEN</ta>
            <ta e="T78" id="Seg_2611" s="T77">Wort-3SG.[NOM]</ta>
            <ta e="T79" id="Seg_2612" s="T78">dieses.[NOM]</ta>
            <ta e="T80" id="Seg_2613" s="T79">doch</ta>
            <ta e="T81" id="Seg_2614" s="T80">gehen-PRS-3PL</ta>
            <ta e="T82" id="Seg_2615" s="T81">Fuchs-EP-2SG.[NOM]</ta>
            <ta e="T83" id="Seg_2616" s="T82">Ufer.[NOM]</ta>
            <ta e="T84" id="Seg_2617" s="T83">entlang</ta>
            <ta e="T85" id="Seg_2618" s="T84">gehen-PST2.[3SG]</ta>
            <ta e="T86" id="Seg_2619" s="T85">Fisch-EP-2SG.[NOM]</ta>
            <ta e="T87" id="Seg_2620" s="T86">Wasser.[NOM]</ta>
            <ta e="T88" id="Seg_2621" s="T87">durch</ta>
            <ta e="T89" id="Seg_2622" s="T88">dieses</ta>
            <ta e="T90" id="Seg_2623" s="T89">Fuchs-EP-2SG.[NOM]</ta>
            <ta e="T91" id="Seg_2624" s="T90">rennen-CVB.SEQ</ta>
            <ta e="T92" id="Seg_2625" s="T91">gehen-CVB.SEQ</ta>
            <ta e="T93" id="Seg_2626" s="T92">anfangen-CVB.SEQ</ta>
            <ta e="T94" id="Seg_2627" s="T93">Aalquappe-PL.[NOM]</ta>
            <ta e="T95" id="Seg_2628" s="T94">sagen-PST2.[3SG]</ta>
            <ta e="T96" id="Seg_2629" s="T95">Vorderseite-3SG-DAT/LOC</ta>
            <ta e="T97" id="Seg_2630" s="T96">ups</ta>
            <ta e="T98" id="Seg_2631" s="T97">sagen-PST2-3PL</ta>
            <ta e="T99" id="Seg_2632" s="T98">überholen-PST2-3PL</ta>
            <ta e="T100" id="Seg_2633" s="T99">Wasser.[NOM]</ta>
            <ta e="T101" id="Seg_2634" s="T100">und</ta>
            <ta e="T102" id="Seg_2635" s="T101">Fisch-3SG.[NOM]</ta>
            <ta e="T103" id="Seg_2636" s="T102">flink-3SG.[NOM]</ta>
            <ta e="T104" id="Seg_2637" s="T103">und</ta>
            <ta e="T105" id="Seg_2638" s="T104">sehr</ta>
            <ta e="T106" id="Seg_2639" s="T105">sein-PTCP.PRS</ta>
            <ta e="T107" id="Seg_2640" s="T106">be-PST2.[3SG]</ta>
            <ta e="T108" id="Seg_2641" s="T107">sagen-PST1-3SG</ta>
            <ta e="T109" id="Seg_2642" s="T108">so</ta>
            <ta e="T110" id="Seg_2643" s="T109">machen-CVB.SEQ</ta>
            <ta e="T111" id="Seg_2644" s="T110">nachdem</ta>
            <ta e="T112" id="Seg_2645" s="T111">wieder</ta>
            <ta e="T113" id="Seg_2646" s="T112">rennen-PST1-3SG</ta>
            <ta e="T114" id="Seg_2647" s="T113">dieses</ta>
            <ta e="T115" id="Seg_2648" s="T114">gehen-CVB.SEQ</ta>
            <ta e="T116" id="Seg_2649" s="T115">eins</ta>
            <ta e="T117" id="Seg_2650" s="T116">Ort-DAT/LOC</ta>
            <ta e="T118" id="Seg_2651" s="T117">Wasser.[NOM]</ta>
            <ta e="T119" id="Seg_2652" s="T118">trinken-PST1-3SG</ta>
            <ta e="T120" id="Seg_2653" s="T119">dieses</ta>
            <ta e="T121" id="Seg_2654" s="T120">trinken-PRS-1SG</ta>
            <ta e="T122" id="Seg_2655" s="T121">sagen-CVB.SEQ</ta>
            <ta e="T123" id="Seg_2656" s="T122">Fuchs-EP-2SG.[NOM]</ta>
            <ta e="T124" id="Seg_2657" s="T123">ersticken-CVB.SEQ</ta>
            <ta e="T125" id="Seg_2658" s="T124">sterben-CVB.SEQ</ta>
            <ta e="T126" id="Seg_2659" s="T125">bleiben-PST1-3SG</ta>
            <ta e="T127" id="Seg_2660" s="T126">eins</ta>
            <ta e="T128" id="Seg_2661" s="T127">Alte.[NOM]</ta>
            <ta e="T129" id="Seg_2662" s="T128">Wasser.[NOM]</ta>
            <ta e="T130" id="Seg_2663" s="T129">schöpfen-CVB.SIM</ta>
            <ta e="T131" id="Seg_2664" s="T130">hineingehen-PST2.[3SG]</ta>
            <ta e="T132" id="Seg_2665" s="T131">nur</ta>
            <ta e="T133" id="Seg_2666" s="T132">dieses</ta>
            <ta e="T134" id="Seg_2667" s="T133">Wasser.[NOM]</ta>
            <ta e="T135" id="Seg_2668" s="T134">schöpfen-CVB.SIM</ta>
            <ta e="T136" id="Seg_2669" s="T135">hineingehen-PST2-3SG</ta>
            <ta e="T137" id="Seg_2670" s="T136">Fuchs.[NOM]</ta>
            <ta e="T138" id="Seg_2671" s="T137">sterben-CVB.SIM</ta>
            <ta e="T139" id="Seg_2672" s="T138">liegen-PTCP.PRS-3SG-ACC</ta>
            <ta e="T140" id="Seg_2673" s="T139">finden-PST2.[3SG]</ta>
            <ta e="T141" id="Seg_2674" s="T140">dieses</ta>
            <ta e="T142" id="Seg_2675" s="T141">Alte-EP-2SG.[NOM]</ta>
            <ta e="T143" id="Seg_2676" s="T142">dieses</ta>
            <ta e="T144" id="Seg_2677" s="T143">Fuchs-ACC</ta>
            <ta e="T145" id="Seg_2678" s="T144">tragen-CVB.SEQ</ta>
            <ta e="T146" id="Seg_2679" s="T145">Haut.abziehen-EP-MED-PST1-3SG</ta>
            <ta e="T147" id="Seg_2680" s="T146">dieses</ta>
            <ta e="T148" id="Seg_2681" s="T147">Haut.abziehen-CVB.SEQ</ta>
            <ta e="T149" id="Seg_2682" s="T148">nachdem</ta>
            <ta e="T150" id="Seg_2683" s="T149">trocken.werden-PST1-3SG</ta>
            <ta e="T151" id="Seg_2684" s="T150">dieses</ta>
            <ta e="T152" id="Seg_2685" s="T151">machen-CVB.SEQ</ta>
            <ta e="T153" id="Seg_2686" s="T152">nachdem</ta>
            <ta e="T154" id="Seg_2687" s="T153">Speicher-DAT/LOC</ta>
            <ta e="T155" id="Seg_2688" s="T154">aufhängen-PST1-3SG</ta>
            <ta e="T156" id="Seg_2689" s="T155">dieses</ta>
            <ta e="T157" id="Seg_2690" s="T156">aufhängen-CVB.SIM</ta>
            <ta e="T158" id="Seg_2691" s="T157">stehen-CVB.SEQ</ta>
            <ta e="T159" id="Seg_2692" s="T158">sprechen-PST1-3SG</ta>
            <ta e="T160" id="Seg_2693" s="T159">dieses</ta>
            <ta e="T161" id="Seg_2694" s="T160">Alte.[NOM]</ta>
            <ta e="T162" id="Seg_2695" s="T161">Süden-ABL</ta>
            <ta e="T163" id="Seg_2696" s="T162">Wind.[NOM]</ta>
            <ta e="T164" id="Seg_2697" s="T163">sein.[IMP.2SG]</ta>
            <ta e="T165" id="Seg_2698" s="T164">sagen-PST1-3SG</ta>
            <ta e="T166" id="Seg_2699" s="T165">Westen-ABL</ta>
            <ta e="T167" id="Seg_2700" s="T166">Wind.[NOM]</ta>
            <ta e="T168" id="Seg_2701" s="T167">sein.[IMP.2SG]</ta>
            <ta e="T169" id="Seg_2702" s="T168">sagen-PST1-3SG</ta>
            <ta e="T170" id="Seg_2703" s="T169">dieses-3SG-3SG.[NOM]</ta>
            <ta e="T171" id="Seg_2704" s="T170">doch</ta>
            <ta e="T172" id="Seg_2705" s="T171">sich.bewegen-PST1-3SG</ta>
            <ta e="T173" id="Seg_2706" s="T172">Wind.[NOM]</ta>
            <ta e="T174" id="Seg_2707" s="T173">kommen-CVB.SEQ</ta>
            <ta e="T175" id="Seg_2708" s="T174">doch</ta>
            <ta e="T176" id="Seg_2709" s="T175">dieses</ta>
            <ta e="T177" id="Seg_2710" s="T176">Fuchs-ACC</ta>
            <ta e="T178" id="Seg_2711" s="T177">sich.werfen-TEMP-3SG</ta>
            <ta e="T179" id="Seg_2712" s="T178">Alte.[NOM]</ta>
            <ta e="T180" id="Seg_2713" s="T179">Fuchs-3SG-ACC</ta>
            <ta e="T181" id="Seg_2714" s="T180">mit</ta>
            <ta e="T182" id="Seg_2715" s="T181">tanzen-PST1-3SG</ta>
            <ta e="T183" id="Seg_2716" s="T182">huuranki_haaranki</ta>
            <ta e="T184" id="Seg_2717" s="T183">sagen-CVB.SIM-sagen-CVB.SIM</ta>
            <ta e="T185" id="Seg_2718" s="T184">dieses</ta>
            <ta e="T186" id="Seg_2719" s="T185">machen-CVB.SIM</ta>
            <ta e="T187" id="Seg_2720" s="T186">stehen-TEMP-3SG</ta>
            <ta e="T188" id="Seg_2721" s="T187">Fuchs-3SG-ACC</ta>
            <ta e="T189" id="Seg_2722" s="T188">Wind-3SG.[NOM]</ta>
            <ta e="T190" id="Seg_2723" s="T189">schneiden-CVB.SIM</ta>
            <ta e="T191" id="Seg_2724" s="T190">schlagen-CVB.SEQ</ta>
            <ta e="T192" id="Seg_2725" s="T191">tragen-CVB.SEQ</ta>
            <ta e="T193" id="Seg_2726" s="T192">werfen-PST1-3SG</ta>
            <ta e="T194" id="Seg_2727" s="T193">dieses</ta>
            <ta e="T195" id="Seg_2728" s="T194">Alte.[NOM]</ta>
            <ta e="T196" id="Seg_2729" s="T195">dieses</ta>
            <ta e="T197" id="Seg_2730" s="T196">Fuchs-3SG-ACC</ta>
            <ta e="T198" id="Seg_2731" s="T197">doch</ta>
            <ta e="T199" id="Seg_2732" s="T198">folgen-PST1-3SG</ta>
            <ta e="T200" id="Seg_2733" s="T199">dieses</ta>
            <ta e="T201" id="Seg_2734" s="T200">folgen-CVB.SEQ</ta>
            <ta e="T202" id="Seg_2735" s="T201">gehen-CVB.SEQ</ta>
            <ta e="T203" id="Seg_2736" s="T202">dieses</ta>
            <ta e="T204" id="Seg_2737" s="T203">Alte.[NOM]</ta>
            <ta e="T205" id="Seg_2738" s="T204">sagen-PST1-3SG</ta>
            <ta e="T206" id="Seg_2739" s="T205">dieses</ta>
            <ta e="T207" id="Seg_2740" s="T206">Busen-EP-1SG.[NOM]</ta>
            <ta e="T208" id="Seg_2741" s="T207">offenbar</ta>
            <ta e="T209" id="Seg_2742" s="T208">schwer.[NOM]</ta>
            <ta e="T210" id="Seg_2743" s="T209">herunterziehen-PRS.[3SG]</ta>
            <ta e="T211" id="Seg_2744" s="T210">offenbar</ta>
            <ta e="T212" id="Seg_2745" s="T211">sagen-CVB.SEQ</ta>
            <ta e="T213" id="Seg_2746" s="T212">Busen-3SG-ACC</ta>
            <ta e="T214" id="Seg_2747" s="T213">schneiden-CVB.SEQ</ta>
            <ta e="T215" id="Seg_2748" s="T214">werfen-PST2-3SG</ta>
            <ta e="T216" id="Seg_2749" s="T215">dieses</ta>
            <ta e="T217" id="Seg_2750" s="T216">machen-CVB.SEQ</ta>
            <ta e="T218" id="Seg_2751" s="T217">nachdem</ta>
            <ta e="T219" id="Seg_2752" s="T218">wieder</ta>
            <ta e="T220" id="Seg_2753" s="T219">gehen-PST2-3SG</ta>
            <ta e="T221" id="Seg_2754" s="T220">anderer.von.zwei</ta>
            <ta e="T222" id="Seg_2755" s="T221">Seite.[NOM]</ta>
            <ta e="T223" id="Seg_2756" s="T222">Busen-EP-1SG.[NOM]</ta>
            <ta e="T224" id="Seg_2757" s="T223">auch</ta>
            <ta e="T225" id="Seg_2758" s="T224">schwer.[NOM]</ta>
            <ta e="T226" id="Seg_2759" s="T225">Q</ta>
            <ta e="T227" id="Seg_2760" s="T226">sagen-CVB.ANT</ta>
            <ta e="T228" id="Seg_2761" s="T227">dieses</ta>
            <ta e="T229" id="Seg_2762" s="T228">Busen-ACC</ta>
            <ta e="T230" id="Seg_2763" s="T229">auch</ta>
            <ta e="T231" id="Seg_2764" s="T230">schneiden-CVB.SEQ</ta>
            <ta e="T232" id="Seg_2765" s="T231">werfen-PST2-3SG</ta>
            <ta e="T233" id="Seg_2766" s="T232">dieses</ta>
            <ta e="T234" id="Seg_2767" s="T233">machen-CVB.SEQ</ta>
            <ta e="T235" id="Seg_2768" s="T234">nachdem</ta>
            <ta e="T236" id="Seg_2769" s="T235">wieder</ta>
            <ta e="T237" id="Seg_2770" s="T236">gehen-PST2-3SG</ta>
            <ta e="T238" id="Seg_2771" s="T237">nun</ta>
            <ta e="T239" id="Seg_2772" s="T238">dieses</ta>
            <ta e="T240" id="Seg_2773" s="T239">Hälfte.[NOM]</ta>
            <ta e="T241" id="Seg_2774" s="T240">Hand-1SG.[NOM]</ta>
            <ta e="T242" id="Seg_2775" s="T241">mein</ta>
            <ta e="T243" id="Seg_2776" s="T242">schwer.[NOM]</ta>
            <ta e="T244" id="Seg_2777" s="T243">Q</ta>
            <ta e="T245" id="Seg_2778" s="T244">sagen-CVB.SEQ</ta>
            <ta e="T246" id="Seg_2779" s="T245">Hälfte.[NOM]</ta>
            <ta e="T247" id="Seg_2780" s="T246">Hand-3SG-ACC</ta>
            <ta e="T248" id="Seg_2781" s="T247">schneiden-PST1-3SG</ta>
            <ta e="T249" id="Seg_2782" s="T248">doch</ta>
            <ta e="T250" id="Seg_2783" s="T249">wieder</ta>
            <ta e="T251" id="Seg_2784" s="T250">gehen-PST1-3SG</ta>
            <ta e="T252" id="Seg_2785" s="T251">dieses</ta>
            <ta e="T253" id="Seg_2786" s="T252">auch</ta>
            <ta e="T254" id="Seg_2787" s="T253">anderer.von.zwei</ta>
            <ta e="T255" id="Seg_2788" s="T254">Hand-3SG-ACC</ta>
            <ta e="T256" id="Seg_2789" s="T255">schneiden-PST1-3SG</ta>
            <ta e="T257" id="Seg_2790" s="T256">zwei</ta>
            <ta e="T258" id="Seg_2791" s="T257">Hand-POSS</ta>
            <ta e="T259" id="Seg_2792" s="T258">NEG</ta>
            <ta e="T260" id="Seg_2793" s="T259">sein-PST1-3SG</ta>
            <ta e="T261" id="Seg_2794" s="T260">danach</ta>
            <ta e="T262" id="Seg_2795" s="T261">Alte.[NOM]</ta>
            <ta e="T263" id="Seg_2796" s="T262">sprechen-PST2-3SG</ta>
            <ta e="T264" id="Seg_2797" s="T263">dieses</ta>
            <ta e="T265" id="Seg_2798" s="T264">Himmel.[NOM]</ta>
            <ta e="T266" id="Seg_2799" s="T265">Sohn-3SG-ACC</ta>
            <ta e="T267" id="Seg_2800" s="T266">mit</ta>
            <ta e="T268" id="Seg_2801" s="T267">sich.küssen-PTCP.FUT-1SG-ACC</ta>
            <ta e="T269" id="Seg_2802" s="T268">Lippe-EP-1SG.[NOM]</ta>
            <ta e="T270" id="Seg_2803" s="T269">nur</ta>
            <ta e="T271" id="Seg_2804" s="T270">ganz.[NOM]</ta>
            <ta e="T272" id="Seg_2805" s="T271">sein-IMP.3SG</ta>
            <ta e="T273" id="Seg_2806" s="T272">dann</ta>
            <ta e="T274" id="Seg_2807" s="T273">gehen-PST2-3SG</ta>
            <ta e="T275" id="Seg_2808" s="T274">letzter-3SG.[NOM]</ta>
            <ta e="T276" id="Seg_2809" s="T275">sein-CVB.SEQ</ta>
            <ta e="T277" id="Seg_2810" s="T276">sterben-PST2-3SG</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_2811" s="T0">один</ta>
            <ta e="T2" id="Seg_2812" s="T1">лиса.[NOM]</ta>
            <ta e="T3" id="Seg_2813" s="T2">остров-DAT/LOC</ta>
            <ta e="T4" id="Seg_2814" s="T3">попасть-PST2.[3SG]</ta>
            <ta e="T5" id="Seg_2815" s="T4">каким.местом</ta>
            <ta e="T6" id="Seg_2816" s="T5">NEG</ta>
            <ta e="T7" id="Seg_2817" s="T6">плавать-CVB.SEQ</ta>
            <ta e="T8" id="Seg_2818" s="T7">выйти-PTCP.FUT-3SG-ACC</ta>
            <ta e="T9" id="Seg_2819" s="T8">NEG</ta>
            <ta e="T10" id="Seg_2820" s="T9">этот</ta>
            <ta e="T11" id="Seg_2821" s="T10">лиса-EP-2SG.[NOM]</ta>
            <ta e="T12" id="Seg_2822" s="T11">плакать-PST1-3SG</ta>
            <ta e="T13" id="Seg_2823" s="T12">этот</ta>
            <ta e="T14" id="Seg_2824" s="T13">хоть</ta>
            <ta e="T15" id="Seg_2825" s="T14">остров-DAT/LOC</ta>
            <ta e="T16" id="Seg_2826" s="T15">попасть-PTCP.COND-1SG-GEN</ta>
            <ta e="T17" id="Seg_2827" s="T16">досада-3SG-ACC</ta>
            <ta e="T18" id="Seg_2828" s="T17">этот</ta>
            <ta e="T19" id="Seg_2829" s="T18">остров-DAT/LOC</ta>
            <ta e="T20" id="Seg_2830" s="T19">попасть-NEG.PTCP-EP-1SG.[NOM]</ta>
            <ta e="T21" id="Seg_2831" s="T20">быть-COND.[3SG]</ta>
            <ta e="T22" id="Seg_2832" s="T21">этот</ta>
            <ta e="T23" id="Seg_2833" s="T22">долган-PL.[NOM]</ta>
            <ta e="T24" id="Seg_2834" s="T23">лук.самострел-3PL-GEN</ta>
            <ta e="T25" id="Seg_2835" s="T24">тетива-EP-3PL-ACC</ta>
            <ta e="T26" id="Seg_2836" s="T25">есть-PTCP.FUT</ta>
            <ta e="T27" id="Seg_2837" s="T26">быть-PST1-1SG</ta>
            <ta e="T28" id="Seg_2838" s="T27">этот-DAT/LOC</ta>
            <ta e="T29" id="Seg_2839" s="T28">вода.[NOM]</ta>
            <ta e="T30" id="Seg_2840" s="T29">нутро-3SG-DAT/LOC</ta>
            <ta e="T31" id="Seg_2841" s="T30">голос.[NOM]</ta>
            <ta e="T32" id="Seg_2842" s="T31">слышаться-EP-PST2.[3SG]</ta>
            <ta e="T33" id="Seg_2843" s="T32">только</ta>
            <ta e="T34" id="Seg_2844" s="T33">налим-PL.[NOM]</ta>
            <ta e="T35" id="Seg_2845" s="T34">говорить-PST2-3PL</ta>
            <ta e="T36" id="Seg_2846" s="T35">ах</ta>
            <ta e="T37" id="Seg_2847" s="T36">ребенок-PL.[NOM]</ta>
            <ta e="T38" id="Seg_2848" s="T37">лиса-1PL.[NOM]</ta>
            <ta e="T39" id="Seg_2849" s="T38">плакать-PTCP.PRS</ta>
            <ta e="T40" id="Seg_2850" s="T39">быть-PST2.[3SG]</ta>
            <ta e="T41" id="Seg_2851" s="T40">Q</ta>
            <ta e="T42" id="Seg_2852" s="T41">этот</ta>
            <ta e="T43" id="Seg_2853" s="T42">этот-ACC</ta>
            <ta e="T44" id="Seg_2854" s="T43">слышать-CVB.SEQ</ta>
            <ta e="T45" id="Seg_2855" s="T44">лиса.[NOM]</ta>
            <ta e="T46" id="Seg_2856" s="T45">говорить-PST1-3SG</ta>
            <ta e="T47" id="Seg_2857" s="T46">налим.[NOM]</ta>
            <ta e="T48" id="Seg_2858" s="T47">говорить-PRS.[3SG]</ta>
            <ta e="T49" id="Seg_2859" s="T48">бежить.наперегонки-IMP.1DU</ta>
            <ta e="T50" id="Seg_2860" s="T49">налим-PL.[NOM]</ta>
            <ta e="T51" id="Seg_2861" s="T50">тоже</ta>
            <ta e="T52" id="Seg_2862" s="T51">вот</ta>
            <ta e="T53" id="Seg_2863" s="T52">бежить.наперегонки-IMP.1DU</ta>
            <ta e="T54" id="Seg_2864" s="T53">говорить-PST2-3PL</ta>
            <ta e="T55" id="Seg_2865" s="T54">лиса-EP-2SG.[NOM]</ta>
            <ta e="T56" id="Seg_2866" s="T55">говорить-PST1-3SG</ta>
            <ta e="T57" id="Seg_2867" s="T56">ну</ta>
            <ta e="T58" id="Seg_2868" s="T57">берег-DAT/LOC</ta>
            <ta e="T59" id="Seg_2869" s="T58">приставать-EP-IMP.2PL</ta>
            <ta e="T60" id="Seg_2870" s="T59">сколько-2PL=Q</ta>
            <ta e="T61" id="Seg_2871" s="T60">1SG.[NOM]</ta>
            <ta e="T62" id="Seg_2872" s="T61">считать-FUT-1SG</ta>
            <ta e="T63" id="Seg_2873" s="T62">говорить-PST1-3SG</ta>
            <ta e="T64" id="Seg_2874" s="T63">лиса.[NOM]</ta>
            <ta e="T65" id="Seg_2875" s="T64">берег-DAT/LOC</ta>
            <ta e="T66" id="Seg_2876" s="T65">приходить-CVB.SEQ</ta>
            <ta e="T67" id="Seg_2877" s="T66">стоять-PTCP.PST-3PL-DAT/LOC</ta>
            <ta e="T68" id="Seg_2878" s="T67">верхняя.часть-3PL-DAT/LOC</ta>
            <ta e="T69" id="Seg_2879" s="T68">прыгать-CVB.SIM</ta>
            <ta e="T70" id="Seg_2880" s="T69">идти-PST1-3SG</ta>
            <ta e="T71" id="Seg_2881" s="T70">лиса.[NOM]</ta>
            <ta e="T72" id="Seg_2882" s="T71">считать-CVB.SEQ</ta>
            <ta e="T76" id="Seg_2883" s="T75">говорить-CVB.SEQ</ta>
            <ta e="T77" id="Seg_2884" s="T76">счет-3SG-GEN</ta>
            <ta e="T78" id="Seg_2885" s="T77">слово-3SG.[NOM]</ta>
            <ta e="T79" id="Seg_2886" s="T78">тот.[NOM]</ta>
            <ta e="T80" id="Seg_2887" s="T79">вот</ta>
            <ta e="T81" id="Seg_2888" s="T80">идти-PRS-3PL</ta>
            <ta e="T82" id="Seg_2889" s="T81">лиса-EP-2SG.[NOM]</ta>
            <ta e="T83" id="Seg_2890" s="T82">берег.[NOM]</ta>
            <ta e="T84" id="Seg_2891" s="T83">вдоль</ta>
            <ta e="T85" id="Seg_2892" s="T84">идти-PST2.[3SG]</ta>
            <ta e="T86" id="Seg_2893" s="T85">рыба-EP-2SG.[NOM]</ta>
            <ta e="T87" id="Seg_2894" s="T86">вода.[NOM]</ta>
            <ta e="T88" id="Seg_2895" s="T87">через</ta>
            <ta e="T89" id="Seg_2896" s="T88">этот</ta>
            <ta e="T90" id="Seg_2897" s="T89">лиса-EP-2SG.[NOM]</ta>
            <ta e="T91" id="Seg_2898" s="T90">бежать-CVB.SEQ</ta>
            <ta e="T92" id="Seg_2899" s="T91">идти-CVB.SEQ</ta>
            <ta e="T93" id="Seg_2900" s="T92">идти-CVB.SEQ</ta>
            <ta e="T94" id="Seg_2901" s="T93">налим-PL.[NOM]</ta>
            <ta e="T95" id="Seg_2902" s="T94">говорить-PST2.[3SG]</ta>
            <ta e="T96" id="Seg_2903" s="T95">передняя.часть-3SG-DAT/LOC</ta>
            <ta e="T97" id="Seg_2904" s="T96">оп</ta>
            <ta e="T98" id="Seg_2905" s="T97">говорить-PST2-3PL</ta>
            <ta e="T99" id="Seg_2906" s="T98">опередить-PST2-3PL</ta>
            <ta e="T100" id="Seg_2907" s="T99">вода.[NOM]</ta>
            <ta e="T101" id="Seg_2908" s="T100">да</ta>
            <ta e="T102" id="Seg_2909" s="T101">рыба-3SG.[NOM]</ta>
            <ta e="T103" id="Seg_2910" s="T102">резвый-3SG.[NOM]</ta>
            <ta e="T104" id="Seg_2911" s="T103">да</ta>
            <ta e="T105" id="Seg_2912" s="T104">очень</ta>
            <ta e="T106" id="Seg_2913" s="T105">быть-PTCP.PRS</ta>
            <ta e="T107" id="Seg_2914" s="T106">быть-PST2.[3SG]</ta>
            <ta e="T108" id="Seg_2915" s="T107">говорить-PST1-3SG</ta>
            <ta e="T109" id="Seg_2916" s="T108">так</ta>
            <ta e="T110" id="Seg_2917" s="T109">делать-CVB.SEQ</ta>
            <ta e="T111" id="Seg_2918" s="T110">после</ta>
            <ta e="T112" id="Seg_2919" s="T111">опять</ta>
            <ta e="T113" id="Seg_2920" s="T112">бежать-PST1-3SG</ta>
            <ta e="T114" id="Seg_2921" s="T113">этот</ta>
            <ta e="T115" id="Seg_2922" s="T114">идти-CVB.SEQ</ta>
            <ta e="T116" id="Seg_2923" s="T115">один</ta>
            <ta e="T117" id="Seg_2924" s="T116">место-DAT/LOC</ta>
            <ta e="T118" id="Seg_2925" s="T117">вода.[NOM]</ta>
            <ta e="T119" id="Seg_2926" s="T118">пить-PST1-3SG</ta>
            <ta e="T120" id="Seg_2927" s="T119">этот</ta>
            <ta e="T121" id="Seg_2928" s="T120">пить-PRS-1SG</ta>
            <ta e="T122" id="Seg_2929" s="T121">говорить-CVB.SEQ</ta>
            <ta e="T123" id="Seg_2930" s="T122">лиса-EP-2SG.[NOM]</ta>
            <ta e="T124" id="Seg_2931" s="T123">задыхаться-CVB.SEQ</ta>
            <ta e="T125" id="Seg_2932" s="T124">умирать-CVB.SEQ</ta>
            <ta e="T126" id="Seg_2933" s="T125">оставаться-PST1-3SG</ta>
            <ta e="T127" id="Seg_2934" s="T126">один</ta>
            <ta e="T128" id="Seg_2935" s="T127">старуха.[NOM]</ta>
            <ta e="T129" id="Seg_2936" s="T128">вода.[NOM]</ta>
            <ta e="T130" id="Seg_2937" s="T129">черпать-CVB.SIM</ta>
            <ta e="T131" id="Seg_2938" s="T130">входить-PST2.[3SG]</ta>
            <ta e="T132" id="Seg_2939" s="T131">только</ta>
            <ta e="T133" id="Seg_2940" s="T132">этот</ta>
            <ta e="T134" id="Seg_2941" s="T133">вода.[NOM]</ta>
            <ta e="T135" id="Seg_2942" s="T134">черпать-CVB.SIM</ta>
            <ta e="T136" id="Seg_2943" s="T135">входить-PST2-3SG</ta>
            <ta e="T137" id="Seg_2944" s="T136">лиса.[NOM]</ta>
            <ta e="T138" id="Seg_2945" s="T137">умирать-CVB.SIM</ta>
            <ta e="T139" id="Seg_2946" s="T138">лежать-PTCP.PRS-3SG-ACC</ta>
            <ta e="T140" id="Seg_2947" s="T139">найти-PST2.[3SG]</ta>
            <ta e="T141" id="Seg_2948" s="T140">этот</ta>
            <ta e="T142" id="Seg_2949" s="T141">старуха-EP-2SG.[NOM]</ta>
            <ta e="T143" id="Seg_2950" s="T142">этот</ta>
            <ta e="T144" id="Seg_2951" s="T143">лиса-ACC</ta>
            <ta e="T145" id="Seg_2952" s="T144">носить-CVB.SEQ</ta>
            <ta e="T146" id="Seg_2953" s="T145">сдирать.кожу-EP-MED-PST1-3SG</ta>
            <ta e="T147" id="Seg_2954" s="T146">этот</ta>
            <ta e="T148" id="Seg_2955" s="T147">сдирать.кожу-CVB.SEQ</ta>
            <ta e="T149" id="Seg_2956" s="T148">после</ta>
            <ta e="T150" id="Seg_2957" s="T149">сушить-PST1-3SG</ta>
            <ta e="T151" id="Seg_2958" s="T150">этот</ta>
            <ta e="T152" id="Seg_2959" s="T151">делать-CVB.SEQ</ta>
            <ta e="T153" id="Seg_2960" s="T152">после</ta>
            <ta e="T154" id="Seg_2961" s="T153">лабаз-DAT/LOC</ta>
            <ta e="T155" id="Seg_2962" s="T154">повесить-PST1-3SG</ta>
            <ta e="T156" id="Seg_2963" s="T155">этот</ta>
            <ta e="T157" id="Seg_2964" s="T156">повесить-CVB.SIM</ta>
            <ta e="T158" id="Seg_2965" s="T157">стоять-CVB.SEQ</ta>
            <ta e="T159" id="Seg_2966" s="T158">говорить-PST1-3SG</ta>
            <ta e="T160" id="Seg_2967" s="T159">этот</ta>
            <ta e="T161" id="Seg_2968" s="T160">старуха.[NOM]</ta>
            <ta e="T162" id="Seg_2969" s="T161">юг-ABL</ta>
            <ta e="T163" id="Seg_2970" s="T162">ветер.[NOM]</ta>
            <ta e="T164" id="Seg_2971" s="T163">быть.[IMP.2SG]</ta>
            <ta e="T165" id="Seg_2972" s="T164">говорить-PST1-3SG</ta>
            <ta e="T166" id="Seg_2973" s="T165">запад-ABL</ta>
            <ta e="T167" id="Seg_2974" s="T166">ветер.[NOM]</ta>
            <ta e="T168" id="Seg_2975" s="T167">быть.[IMP.2SG]</ta>
            <ta e="T169" id="Seg_2976" s="T168">говорить-PST1-3SG</ta>
            <ta e="T170" id="Seg_2977" s="T169">этот-3SG-3SG.[NOM]</ta>
            <ta e="T171" id="Seg_2978" s="T170">вот</ta>
            <ta e="T172" id="Seg_2979" s="T171">двигаться-PST1-3SG</ta>
            <ta e="T173" id="Seg_2980" s="T172">ветер.[NOM]</ta>
            <ta e="T174" id="Seg_2981" s="T173">приходить-CVB.SEQ</ta>
            <ta e="T175" id="Seg_2982" s="T174">вот</ta>
            <ta e="T176" id="Seg_2983" s="T175">этот</ta>
            <ta e="T177" id="Seg_2984" s="T176">лиса-ACC</ta>
            <ta e="T178" id="Seg_2985" s="T177">кидаться-TEMP-3SG</ta>
            <ta e="T179" id="Seg_2986" s="T178">старуха.[NOM]</ta>
            <ta e="T180" id="Seg_2987" s="T179">лиса-3SG-ACC</ta>
            <ta e="T181" id="Seg_2988" s="T180">с</ta>
            <ta e="T182" id="Seg_2989" s="T181">плясать-PST1-3SG</ta>
            <ta e="T183" id="Seg_2990" s="T182">суурангки_саарангки</ta>
            <ta e="T184" id="Seg_2991" s="T183">говорить-CVB.SIM-говорить-CVB.SIM</ta>
            <ta e="T185" id="Seg_2992" s="T184">этот</ta>
            <ta e="T186" id="Seg_2993" s="T185">делать-CVB.SIM</ta>
            <ta e="T187" id="Seg_2994" s="T186">стоять-TEMP-3SG</ta>
            <ta e="T188" id="Seg_2995" s="T187">лиса-3SG-ACC</ta>
            <ta e="T189" id="Seg_2996" s="T188">ветер-3SG.[NOM]</ta>
            <ta e="T190" id="Seg_2997" s="T189">резать-CVB.SIM</ta>
            <ta e="T191" id="Seg_2998" s="T190">бить-CVB.SEQ</ta>
            <ta e="T192" id="Seg_2999" s="T191">носить-CVB.SEQ</ta>
            <ta e="T193" id="Seg_3000" s="T192">бросать-PST1-3SG</ta>
            <ta e="T194" id="Seg_3001" s="T193">этот</ta>
            <ta e="T195" id="Seg_3002" s="T194">старуха.[NOM]</ta>
            <ta e="T196" id="Seg_3003" s="T195">этот</ta>
            <ta e="T197" id="Seg_3004" s="T196">лиса-3SG-ACC</ta>
            <ta e="T198" id="Seg_3005" s="T197">вот</ta>
            <ta e="T199" id="Seg_3006" s="T198">следовать-PST1-3SG</ta>
            <ta e="T200" id="Seg_3007" s="T199">этот</ta>
            <ta e="T201" id="Seg_3008" s="T200">следовать-CVB.SEQ</ta>
            <ta e="T202" id="Seg_3009" s="T201">идти-CVB.SEQ</ta>
            <ta e="T203" id="Seg_3010" s="T202">этот</ta>
            <ta e="T204" id="Seg_3011" s="T203">старуха.[NOM]</ta>
            <ta e="T205" id="Seg_3012" s="T204">говорить-PST1-3SG</ta>
            <ta e="T206" id="Seg_3013" s="T205">этот</ta>
            <ta e="T207" id="Seg_3014" s="T206">женская.грудь-EP-1SG.[NOM]</ta>
            <ta e="T208" id="Seg_3015" s="T207">видать</ta>
            <ta e="T209" id="Seg_3016" s="T208">тяжелый.[NOM]</ta>
            <ta e="T210" id="Seg_3017" s="T209">перевешивать-PRS.[3SG]</ta>
            <ta e="T211" id="Seg_3018" s="T210">видать</ta>
            <ta e="T212" id="Seg_3019" s="T211">говорить-CVB.SEQ</ta>
            <ta e="T213" id="Seg_3020" s="T212">женская.грудь-3SG-ACC</ta>
            <ta e="T214" id="Seg_3021" s="T213">резать-CVB.SEQ</ta>
            <ta e="T215" id="Seg_3022" s="T214">бросать-PST2-3SG</ta>
            <ta e="T216" id="Seg_3023" s="T215">этот</ta>
            <ta e="T217" id="Seg_3024" s="T216">делать-CVB.SEQ</ta>
            <ta e="T218" id="Seg_3025" s="T217">после</ta>
            <ta e="T219" id="Seg_3026" s="T218">опять</ta>
            <ta e="T220" id="Seg_3027" s="T219">идти-PST2-3SG</ta>
            <ta e="T221" id="Seg_3028" s="T220">другой.из.двух</ta>
            <ta e="T222" id="Seg_3029" s="T221">сторона.[NOM]</ta>
            <ta e="T223" id="Seg_3030" s="T222">женская.грудь-EP-1SG.[NOM]</ta>
            <ta e="T224" id="Seg_3031" s="T223">тоже</ta>
            <ta e="T225" id="Seg_3032" s="T224">тяжелый.[NOM]</ta>
            <ta e="T226" id="Seg_3033" s="T225">Q</ta>
            <ta e="T227" id="Seg_3034" s="T226">говорить-CVB.ANT</ta>
            <ta e="T228" id="Seg_3035" s="T227">этот</ta>
            <ta e="T229" id="Seg_3036" s="T228">женская.грудь-ACC</ta>
            <ta e="T230" id="Seg_3037" s="T229">тоже</ta>
            <ta e="T231" id="Seg_3038" s="T230">резать-CVB.SEQ</ta>
            <ta e="T232" id="Seg_3039" s="T231">бросать-PST2-3SG</ta>
            <ta e="T233" id="Seg_3040" s="T232">этот</ta>
            <ta e="T234" id="Seg_3041" s="T233">делать-CVB.SEQ</ta>
            <ta e="T235" id="Seg_3042" s="T234">после</ta>
            <ta e="T236" id="Seg_3043" s="T235">опять</ta>
            <ta e="T237" id="Seg_3044" s="T236">идти-PST2-3SG</ta>
            <ta e="T238" id="Seg_3045" s="T237">ну</ta>
            <ta e="T239" id="Seg_3046" s="T238">этот</ta>
            <ta e="T240" id="Seg_3047" s="T239">половина.[NOM]</ta>
            <ta e="T241" id="Seg_3048" s="T240">рука-1SG.[NOM]</ta>
            <ta e="T242" id="Seg_3049" s="T241">мой</ta>
            <ta e="T243" id="Seg_3050" s="T242">тяжелый.[NOM]</ta>
            <ta e="T244" id="Seg_3051" s="T243">Q</ta>
            <ta e="T245" id="Seg_3052" s="T244">говорить-CVB.SEQ</ta>
            <ta e="T246" id="Seg_3053" s="T245">половина.[NOM]</ta>
            <ta e="T247" id="Seg_3054" s="T246">рука-3SG-ACC</ta>
            <ta e="T248" id="Seg_3055" s="T247">резать-PST1-3SG</ta>
            <ta e="T249" id="Seg_3056" s="T248">вот</ta>
            <ta e="T250" id="Seg_3057" s="T249">опять</ta>
            <ta e="T251" id="Seg_3058" s="T250">идти-PST1-3SG</ta>
            <ta e="T252" id="Seg_3059" s="T251">этот</ta>
            <ta e="T253" id="Seg_3060" s="T252">тоже</ta>
            <ta e="T254" id="Seg_3061" s="T253">другой.из.двух</ta>
            <ta e="T255" id="Seg_3062" s="T254">рука-3SG-ACC</ta>
            <ta e="T256" id="Seg_3063" s="T255">резать-PST1-3SG</ta>
            <ta e="T257" id="Seg_3064" s="T256">два</ta>
            <ta e="T258" id="Seg_3065" s="T257">рука-POSS</ta>
            <ta e="T259" id="Seg_3066" s="T258">NEG</ta>
            <ta e="T260" id="Seg_3067" s="T259">быть-PST1-3SG</ta>
            <ta e="T261" id="Seg_3068" s="T260">после.этого</ta>
            <ta e="T262" id="Seg_3069" s="T261">старуха.[NOM]</ta>
            <ta e="T263" id="Seg_3070" s="T262">говорить-PST2-3SG</ta>
            <ta e="T264" id="Seg_3071" s="T263">этот</ta>
            <ta e="T265" id="Seg_3072" s="T264">небо.[NOM]</ta>
            <ta e="T266" id="Seg_3073" s="T265">сын-3SG-ACC</ta>
            <ta e="T267" id="Seg_3074" s="T266">с</ta>
            <ta e="T268" id="Seg_3075" s="T267">целоваться-PTCP.FUT-1SG-ACC</ta>
            <ta e="T269" id="Seg_3076" s="T268">губа-EP-1SG.[NOM]</ta>
            <ta e="T270" id="Seg_3077" s="T269">только</ta>
            <ta e="T271" id="Seg_3078" s="T270">целый.[NOM]</ta>
            <ta e="T272" id="Seg_3079" s="T271">быть-IMP.3SG</ta>
            <ta e="T273" id="Seg_3080" s="T272">потом</ta>
            <ta e="T274" id="Seg_3081" s="T273">идти-PST2-3SG</ta>
            <ta e="T275" id="Seg_3082" s="T274">последний-3SG.[NOM]</ta>
            <ta e="T276" id="Seg_3083" s="T275">быть-CVB.SEQ</ta>
            <ta e="T277" id="Seg_3084" s="T276">умирать-PST2-3SG</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_3085" s="T0">cardnum</ta>
            <ta e="T2" id="Seg_3086" s="T1">n-n:case</ta>
            <ta e="T3" id="Seg_3087" s="T2">n-n:case</ta>
            <ta e="T4" id="Seg_3088" s="T3">v-v:tense-v:pred.pn</ta>
            <ta e="T5" id="Seg_3089" s="T4">que</ta>
            <ta e="T6" id="Seg_3090" s="T5">ptcl</ta>
            <ta e="T7" id="Seg_3091" s="T6">v-v:cvb</ta>
            <ta e="T8" id="Seg_3092" s="T7">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T9" id="Seg_3093" s="T8">ptcl</ta>
            <ta e="T10" id="Seg_3094" s="T9">dempro</ta>
            <ta e="T11" id="Seg_3095" s="T10">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T12" id="Seg_3096" s="T11">v-v:tense-v:poss.pn</ta>
            <ta e="T13" id="Seg_3097" s="T12">dempro</ta>
            <ta e="T14" id="Seg_3098" s="T13">ptcl</ta>
            <ta e="T15" id="Seg_3099" s="T14">n-n:case</ta>
            <ta e="T16" id="Seg_3100" s="T15">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T17" id="Seg_3101" s="T16">n-n:poss-n:case</ta>
            <ta e="T18" id="Seg_3102" s="T17">dempro</ta>
            <ta e="T19" id="Seg_3103" s="T18">n-n:case</ta>
            <ta e="T20" id="Seg_3104" s="T19">v-v:ptcp-v:(ins)-v:(poss)-v:(case)</ta>
            <ta e="T21" id="Seg_3105" s="T20">v-v:mood-v:pred.pn</ta>
            <ta e="T22" id="Seg_3106" s="T21">dempro</ta>
            <ta e="T23" id="Seg_3107" s="T22">n-n:(num)-n:case</ta>
            <ta e="T24" id="Seg_3108" s="T23">n-n:poss-n:case</ta>
            <ta e="T25" id="Seg_3109" s="T24">n-n:(ins)-n:poss-n:case</ta>
            <ta e="T26" id="Seg_3110" s="T25">v-v:ptcp</ta>
            <ta e="T27" id="Seg_3111" s="T26">v-v:tense-v:poss.pn</ta>
            <ta e="T28" id="Seg_3112" s="T27">dempro-pro:case</ta>
            <ta e="T29" id="Seg_3113" s="T28">n-n:case</ta>
            <ta e="T30" id="Seg_3114" s="T29">n-n:poss-n:case</ta>
            <ta e="T31" id="Seg_3115" s="T30">n-n:case</ta>
            <ta e="T32" id="Seg_3116" s="T31">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T33" id="Seg_3117" s="T32">ptcl</ta>
            <ta e="T34" id="Seg_3118" s="T33">n-n:(num)-n:case</ta>
            <ta e="T35" id="Seg_3119" s="T34">v-v:tense-v:pred.pn</ta>
            <ta e="T36" id="Seg_3120" s="T35">interj</ta>
            <ta e="T37" id="Seg_3121" s="T36">n-n:(num)-n:case</ta>
            <ta e="T38" id="Seg_3122" s="T37">n-n:(poss)-n:case</ta>
            <ta e="T39" id="Seg_3123" s="T38">v-v:ptcp</ta>
            <ta e="T40" id="Seg_3124" s="T39">v-v:tense-v:pred.pn</ta>
            <ta e="T41" id="Seg_3125" s="T40">ptcl</ta>
            <ta e="T42" id="Seg_3126" s="T41">dempro</ta>
            <ta e="T43" id="Seg_3127" s="T42">dempro-pro:case</ta>
            <ta e="T44" id="Seg_3128" s="T43">v-v:cvb</ta>
            <ta e="T45" id="Seg_3129" s="T44">n-n:case</ta>
            <ta e="T46" id="Seg_3130" s="T45">v-v:tense-v:poss.pn</ta>
            <ta e="T47" id="Seg_3131" s="T46">n-n:case</ta>
            <ta e="T48" id="Seg_3132" s="T47">v-v:tense-v:pred.pn</ta>
            <ta e="T49" id="Seg_3133" s="T48">v-v:mood.pn</ta>
            <ta e="T50" id="Seg_3134" s="T49">n-n:(num)-n:case</ta>
            <ta e="T51" id="Seg_3135" s="T50">ptcl</ta>
            <ta e="T52" id="Seg_3136" s="T51">ptcl</ta>
            <ta e="T53" id="Seg_3137" s="T52">v-v:mood.pn</ta>
            <ta e="T54" id="Seg_3138" s="T53">v-v:tense-v:pred.pn</ta>
            <ta e="T55" id="Seg_3139" s="T54">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T56" id="Seg_3140" s="T55">v-v:tense-v:poss.pn</ta>
            <ta e="T57" id="Seg_3141" s="T56">interj</ta>
            <ta e="T58" id="Seg_3142" s="T57">n-n:case</ta>
            <ta e="T59" id="Seg_3143" s="T58">v-v:(ins)-v:mood.pn</ta>
            <ta e="T60" id="Seg_3144" s="T59">que-pro:(pred.pn)-ptcl</ta>
            <ta e="T61" id="Seg_3145" s="T60">pers-pro:case</ta>
            <ta e="T62" id="Seg_3146" s="T61">v-v:tense-v:poss.pn</ta>
            <ta e="T63" id="Seg_3147" s="T62">v-v:tense-v:poss.pn</ta>
            <ta e="T64" id="Seg_3148" s="T63">n-n:case</ta>
            <ta e="T65" id="Seg_3149" s="T64">n-n:case</ta>
            <ta e="T66" id="Seg_3150" s="T65">v-v:cvb</ta>
            <ta e="T67" id="Seg_3151" s="T66">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T68" id="Seg_3152" s="T67">n-n:poss-n:case</ta>
            <ta e="T69" id="Seg_3153" s="T68">v-v:cvb</ta>
            <ta e="T70" id="Seg_3154" s="T69">v-v:tense-v:poss.pn</ta>
            <ta e="T71" id="Seg_3155" s="T70">n-n:case</ta>
            <ta e="T72" id="Seg_3156" s="T71">v-v:cvb</ta>
            <ta e="T76" id="Seg_3157" s="T75">v-v:cvb</ta>
            <ta e="T77" id="Seg_3158" s="T76">n-n:poss-n:case</ta>
            <ta e="T78" id="Seg_3159" s="T77">n-n:(poss)-n:case</ta>
            <ta e="T79" id="Seg_3160" s="T78">dempro-pro:case</ta>
            <ta e="T80" id="Seg_3161" s="T79">ptcl</ta>
            <ta e="T81" id="Seg_3162" s="T80">v-v:tense-v:pred.pn</ta>
            <ta e="T82" id="Seg_3163" s="T81">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T83" id="Seg_3164" s="T82">n-n:case</ta>
            <ta e="T84" id="Seg_3165" s="T83">post</ta>
            <ta e="T85" id="Seg_3166" s="T84">v-v:tense-v:pred.pn</ta>
            <ta e="T86" id="Seg_3167" s="T85">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T87" id="Seg_3168" s="T86">n-n:case</ta>
            <ta e="T88" id="Seg_3169" s="T87">post</ta>
            <ta e="T89" id="Seg_3170" s="T88">dempro</ta>
            <ta e="T90" id="Seg_3171" s="T89">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T91" id="Seg_3172" s="T90">v-v:cvb</ta>
            <ta e="T92" id="Seg_3173" s="T91">v-v:cvb</ta>
            <ta e="T93" id="Seg_3174" s="T92">v-v:cvb</ta>
            <ta e="T94" id="Seg_3175" s="T93">n-n:(num)-n:case</ta>
            <ta e="T95" id="Seg_3176" s="T94">v-v:tense-v:pred.pn</ta>
            <ta e="T96" id="Seg_3177" s="T95">n-n:poss-n:case</ta>
            <ta e="T97" id="Seg_3178" s="T96">interj</ta>
            <ta e="T98" id="Seg_3179" s="T97">v-v:tense-v:pred.pn</ta>
            <ta e="T99" id="Seg_3180" s="T98">v-v:tense-v:pred.pn</ta>
            <ta e="T100" id="Seg_3181" s="T99">n-n:case</ta>
            <ta e="T101" id="Seg_3182" s="T100">conj</ta>
            <ta e="T102" id="Seg_3183" s="T101">n-n:(poss)-n:case</ta>
            <ta e="T103" id="Seg_3184" s="T102">adj-n:(poss)-n:case</ta>
            <ta e="T104" id="Seg_3185" s="T103">conj</ta>
            <ta e="T105" id="Seg_3186" s="T104">adv</ta>
            <ta e="T106" id="Seg_3187" s="T105">v-v:ptcp</ta>
            <ta e="T107" id="Seg_3188" s="T106">v-v:tense-v:pred.pn</ta>
            <ta e="T108" id="Seg_3189" s="T107">v-v:tense-v:poss.pn</ta>
            <ta e="T109" id="Seg_3190" s="T108">adv</ta>
            <ta e="T110" id="Seg_3191" s="T109">v-v:cvb</ta>
            <ta e="T111" id="Seg_3192" s="T110">post</ta>
            <ta e="T112" id="Seg_3193" s="T111">ptcl</ta>
            <ta e="T113" id="Seg_3194" s="T112">v-v:tense-v:poss.pn</ta>
            <ta e="T114" id="Seg_3195" s="T113">dempro</ta>
            <ta e="T115" id="Seg_3196" s="T114">v-v:cvb</ta>
            <ta e="T116" id="Seg_3197" s="T115">cardnum</ta>
            <ta e="T117" id="Seg_3198" s="T116">n-n:case</ta>
            <ta e="T118" id="Seg_3199" s="T117">n-n:case</ta>
            <ta e="T119" id="Seg_3200" s="T118">v-v:tense-v:poss.pn</ta>
            <ta e="T120" id="Seg_3201" s="T119">dempro</ta>
            <ta e="T121" id="Seg_3202" s="T120">v-v:tense-v:pred.pn</ta>
            <ta e="T122" id="Seg_3203" s="T121">v-v:cvb</ta>
            <ta e="T123" id="Seg_3204" s="T122">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T124" id="Seg_3205" s="T123">v-v:cvb</ta>
            <ta e="T125" id="Seg_3206" s="T124">v-v:cvb</ta>
            <ta e="T126" id="Seg_3207" s="T125">v-v:tense-v:poss.pn</ta>
            <ta e="T127" id="Seg_3208" s="T126">cardnum</ta>
            <ta e="T128" id="Seg_3209" s="T127">n-n:case</ta>
            <ta e="T129" id="Seg_3210" s="T128">n-n:case</ta>
            <ta e="T130" id="Seg_3211" s="T129">v-v:cvb</ta>
            <ta e="T131" id="Seg_3212" s="T130">v-v:tense-v:pred.pn</ta>
            <ta e="T132" id="Seg_3213" s="T131">ptcl</ta>
            <ta e="T133" id="Seg_3214" s="T132">dempro</ta>
            <ta e="T134" id="Seg_3215" s="T133">n-n:case</ta>
            <ta e="T135" id="Seg_3216" s="T134">v-v:cvb</ta>
            <ta e="T136" id="Seg_3217" s="T135">v-v:tense-v:poss.pn</ta>
            <ta e="T137" id="Seg_3218" s="T136">n-n:case</ta>
            <ta e="T138" id="Seg_3219" s="T137">v-v:cvb</ta>
            <ta e="T139" id="Seg_3220" s="T138">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T140" id="Seg_3221" s="T139">v-v:tense-v:pred.pn</ta>
            <ta e="T141" id="Seg_3222" s="T140">dempro</ta>
            <ta e="T142" id="Seg_3223" s="T141">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T143" id="Seg_3224" s="T142">dempro</ta>
            <ta e="T144" id="Seg_3225" s="T143">n-n:case</ta>
            <ta e="T145" id="Seg_3226" s="T144">v-v:cvb</ta>
            <ta e="T146" id="Seg_3227" s="T145">v-v:(ins)-v&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T147" id="Seg_3228" s="T146">dempro</ta>
            <ta e="T148" id="Seg_3229" s="T147">v-v:cvb</ta>
            <ta e="T149" id="Seg_3230" s="T148">post</ta>
            <ta e="T150" id="Seg_3231" s="T149">v-v:tense-v:poss.pn</ta>
            <ta e="T151" id="Seg_3232" s="T150">dempro</ta>
            <ta e="T152" id="Seg_3233" s="T151">v-v:cvb</ta>
            <ta e="T153" id="Seg_3234" s="T152">post</ta>
            <ta e="T154" id="Seg_3235" s="T153">n-n:case</ta>
            <ta e="T155" id="Seg_3236" s="T154">v-v:tense-v:poss.pn</ta>
            <ta e="T156" id="Seg_3237" s="T155">dempro</ta>
            <ta e="T157" id="Seg_3238" s="T156">v-v:cvb</ta>
            <ta e="T158" id="Seg_3239" s="T157">v-v:cvb</ta>
            <ta e="T159" id="Seg_3240" s="T158">v-v:tense-v:poss.pn</ta>
            <ta e="T160" id="Seg_3241" s="T159">dempro</ta>
            <ta e="T161" id="Seg_3242" s="T160">n-n:case</ta>
            <ta e="T162" id="Seg_3243" s="T161">n-n:case</ta>
            <ta e="T163" id="Seg_3244" s="T162">n-n:case</ta>
            <ta e="T164" id="Seg_3245" s="T163">v-v:mood.pn</ta>
            <ta e="T165" id="Seg_3246" s="T164">v-v:tense-v:poss.pn</ta>
            <ta e="T166" id="Seg_3247" s="T165">n-n:case</ta>
            <ta e="T167" id="Seg_3248" s="T166">n-n:case</ta>
            <ta e="T168" id="Seg_3249" s="T167">v-v:mood.pn</ta>
            <ta e="T169" id="Seg_3250" s="T168">v-v:tense-v:poss.pn</ta>
            <ta e="T170" id="Seg_3251" s="T169">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T171" id="Seg_3252" s="T170">ptcl</ta>
            <ta e="T172" id="Seg_3253" s="T171">v-v:tense-v:poss.pn</ta>
            <ta e="T173" id="Seg_3254" s="T172">n-n:case</ta>
            <ta e="T174" id="Seg_3255" s="T173">v-v:cvb</ta>
            <ta e="T175" id="Seg_3256" s="T174">ptcl</ta>
            <ta e="T176" id="Seg_3257" s="T175">dempro</ta>
            <ta e="T177" id="Seg_3258" s="T176">n-n:case</ta>
            <ta e="T178" id="Seg_3259" s="T177">v-v:mood-v:temp.pn</ta>
            <ta e="T179" id="Seg_3260" s="T178">n-n:case</ta>
            <ta e="T180" id="Seg_3261" s="T179">n-n:poss-n:case</ta>
            <ta e="T181" id="Seg_3262" s="T180">post</ta>
            <ta e="T182" id="Seg_3263" s="T181">v-v:tense-v:poss.pn</ta>
            <ta e="T183" id="Seg_3264" s="T182">interj</ta>
            <ta e="T184" id="Seg_3265" s="T183">v-v:cvb-v-v:cvb</ta>
            <ta e="T185" id="Seg_3266" s="T184">dempro</ta>
            <ta e="T186" id="Seg_3267" s="T185">v-v:cvb</ta>
            <ta e="T187" id="Seg_3268" s="T186">v-v:mood-v:temp.pn</ta>
            <ta e="T188" id="Seg_3269" s="T187">n-n:poss-n:case</ta>
            <ta e="T189" id="Seg_3270" s="T188">n-n:(poss)-n:case</ta>
            <ta e="T190" id="Seg_3271" s="T189">v-v:cvb</ta>
            <ta e="T191" id="Seg_3272" s="T190">v-v:cvb</ta>
            <ta e="T192" id="Seg_3273" s="T191">v-v:cvb</ta>
            <ta e="T193" id="Seg_3274" s="T192">v-v:tense-v:poss.pn</ta>
            <ta e="T194" id="Seg_3275" s="T193">dempro</ta>
            <ta e="T195" id="Seg_3276" s="T194">n-n:case</ta>
            <ta e="T196" id="Seg_3277" s="T195">dempro</ta>
            <ta e="T197" id="Seg_3278" s="T196">n-n:poss-n:case</ta>
            <ta e="T198" id="Seg_3279" s="T197">ptcl</ta>
            <ta e="T199" id="Seg_3280" s="T198">v-v:tense-v:poss.pn</ta>
            <ta e="T200" id="Seg_3281" s="T199">dempro</ta>
            <ta e="T201" id="Seg_3282" s="T200">v-v:cvb</ta>
            <ta e="T202" id="Seg_3283" s="T201">v-v:cvb</ta>
            <ta e="T203" id="Seg_3284" s="T202">dempro</ta>
            <ta e="T204" id="Seg_3285" s="T203">n-n:case</ta>
            <ta e="T205" id="Seg_3286" s="T204">v-v:tense-v:poss.pn</ta>
            <ta e="T206" id="Seg_3287" s="T205">dempro</ta>
            <ta e="T207" id="Seg_3288" s="T206">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T208" id="Seg_3289" s="T207">ptcl</ta>
            <ta e="T209" id="Seg_3290" s="T208">adj-n:case</ta>
            <ta e="T210" id="Seg_3291" s="T209">v-v:tense-v:pred.pn</ta>
            <ta e="T211" id="Seg_3292" s="T210">ptcl</ta>
            <ta e="T212" id="Seg_3293" s="T211">v-v:cvb</ta>
            <ta e="T213" id="Seg_3294" s="T212">n-n:poss-n:case</ta>
            <ta e="T214" id="Seg_3295" s="T213">v-v:cvb</ta>
            <ta e="T215" id="Seg_3296" s="T214">v-v:tense-v:poss.pn</ta>
            <ta e="T216" id="Seg_3297" s="T215">dempro</ta>
            <ta e="T217" id="Seg_3298" s="T216">v-v:cvb</ta>
            <ta e="T218" id="Seg_3299" s="T217">post</ta>
            <ta e="T219" id="Seg_3300" s="T218">ptcl</ta>
            <ta e="T220" id="Seg_3301" s="T219">v-v:tense-v:poss.pn</ta>
            <ta e="T221" id="Seg_3302" s="T220">adj</ta>
            <ta e="T222" id="Seg_3303" s="T221">n-n:case</ta>
            <ta e="T223" id="Seg_3304" s="T222">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T224" id="Seg_3305" s="T223">ptcl</ta>
            <ta e="T225" id="Seg_3306" s="T224">adj-n:case</ta>
            <ta e="T226" id="Seg_3307" s="T225">ptcl</ta>
            <ta e="T227" id="Seg_3308" s="T226">v-v:cvb</ta>
            <ta e="T228" id="Seg_3309" s="T227">dempro</ta>
            <ta e="T229" id="Seg_3310" s="T228">n-n:case</ta>
            <ta e="T230" id="Seg_3311" s="T229">ptcl</ta>
            <ta e="T231" id="Seg_3312" s="T230">v-v:cvb</ta>
            <ta e="T232" id="Seg_3313" s="T231">v-v:tense-v:poss.pn</ta>
            <ta e="T233" id="Seg_3314" s="T232">dempro</ta>
            <ta e="T234" id="Seg_3315" s="T233">v-v:cvb</ta>
            <ta e="T235" id="Seg_3316" s="T234">post</ta>
            <ta e="T236" id="Seg_3317" s="T235">ptcl</ta>
            <ta e="T237" id="Seg_3318" s="T236">v-v:tense-v:poss.pn</ta>
            <ta e="T238" id="Seg_3319" s="T237">ptcl</ta>
            <ta e="T239" id="Seg_3320" s="T238">dempro</ta>
            <ta e="T240" id="Seg_3321" s="T239">n-n:case</ta>
            <ta e="T241" id="Seg_3322" s="T240">n-n:(poss)-n:case</ta>
            <ta e="T242" id="Seg_3323" s="T241">posspr</ta>
            <ta e="T243" id="Seg_3324" s="T242">adj-n:case</ta>
            <ta e="T244" id="Seg_3325" s="T243">ptcl</ta>
            <ta e="T245" id="Seg_3326" s="T244">v-v:cvb</ta>
            <ta e="T246" id="Seg_3327" s="T245">n-n:case</ta>
            <ta e="T247" id="Seg_3328" s="T246">n-n:poss-n:case</ta>
            <ta e="T248" id="Seg_3329" s="T247">v-v:tense-v:poss.pn</ta>
            <ta e="T249" id="Seg_3330" s="T248">ptcl</ta>
            <ta e="T250" id="Seg_3331" s="T249">ptcl</ta>
            <ta e="T251" id="Seg_3332" s="T250">v-v:tense-v:poss.pn</ta>
            <ta e="T252" id="Seg_3333" s="T251">dempro</ta>
            <ta e="T253" id="Seg_3334" s="T252">ptcl</ta>
            <ta e="T254" id="Seg_3335" s="T253">adj</ta>
            <ta e="T255" id="Seg_3336" s="T254">n-n:poss-n:case</ta>
            <ta e="T256" id="Seg_3337" s="T255">v-v:tense-v:poss.pn</ta>
            <ta e="T257" id="Seg_3338" s="T256">cardnum</ta>
            <ta e="T258" id="Seg_3339" s="T257">n-n:(poss)</ta>
            <ta e="T259" id="Seg_3340" s="T258">ptcl</ta>
            <ta e="T260" id="Seg_3341" s="T259">v-v:tense-v:poss.pn</ta>
            <ta e="T261" id="Seg_3342" s="T260">adv</ta>
            <ta e="T262" id="Seg_3343" s="T261">n-n:case</ta>
            <ta e="T263" id="Seg_3344" s="T262">v-v:tense-v:poss.pn</ta>
            <ta e="T264" id="Seg_3345" s="T263">dempro</ta>
            <ta e="T265" id="Seg_3346" s="T264">n-n:case</ta>
            <ta e="T266" id="Seg_3347" s="T265">n-n:poss-n:case</ta>
            <ta e="T267" id="Seg_3348" s="T266">post</ta>
            <ta e="T268" id="Seg_3349" s="T267">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T269" id="Seg_3350" s="T268">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T270" id="Seg_3351" s="T269">ptcl</ta>
            <ta e="T271" id="Seg_3352" s="T270">adj-n:case</ta>
            <ta e="T272" id="Seg_3353" s="T271">v-v:mood.pn</ta>
            <ta e="T273" id="Seg_3354" s="T272">adv</ta>
            <ta e="T274" id="Seg_3355" s="T273">v-v:tense-v:poss.pn</ta>
            <ta e="T275" id="Seg_3356" s="T274">adj-n:(poss)-n:case</ta>
            <ta e="T276" id="Seg_3357" s="T275">v-v:cvb</ta>
            <ta e="T277" id="Seg_3358" s="T276">v-v:tense-v:poss.pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_3359" s="T0">cardnum</ta>
            <ta e="T2" id="Seg_3360" s="T1">n</ta>
            <ta e="T3" id="Seg_3361" s="T2">n</ta>
            <ta e="T4" id="Seg_3362" s="T3">v</ta>
            <ta e="T5" id="Seg_3363" s="T4">que</ta>
            <ta e="T6" id="Seg_3364" s="T5">ptcl</ta>
            <ta e="T7" id="Seg_3365" s="T6">v</ta>
            <ta e="T8" id="Seg_3366" s="T7">v</ta>
            <ta e="T9" id="Seg_3367" s="T8">ptcl</ta>
            <ta e="T10" id="Seg_3368" s="T9">dempro</ta>
            <ta e="T11" id="Seg_3369" s="T10">n</ta>
            <ta e="T12" id="Seg_3370" s="T11">v</ta>
            <ta e="T13" id="Seg_3371" s="T12">dempro</ta>
            <ta e="T14" id="Seg_3372" s="T13">ptcl</ta>
            <ta e="T15" id="Seg_3373" s="T14">n</ta>
            <ta e="T16" id="Seg_3374" s="T15">v</ta>
            <ta e="T17" id="Seg_3375" s="T16">n</ta>
            <ta e="T18" id="Seg_3376" s="T17">dempro</ta>
            <ta e="T19" id="Seg_3377" s="T18">n</ta>
            <ta e="T20" id="Seg_3378" s="T19">v</ta>
            <ta e="T21" id="Seg_3379" s="T20">aux</ta>
            <ta e="T22" id="Seg_3380" s="T21">dempro</ta>
            <ta e="T23" id="Seg_3381" s="T22">n</ta>
            <ta e="T24" id="Seg_3382" s="T23">n</ta>
            <ta e="T25" id="Seg_3383" s="T24">n</ta>
            <ta e="T26" id="Seg_3384" s="T25">v</ta>
            <ta e="T27" id="Seg_3385" s="T26">aux</ta>
            <ta e="T28" id="Seg_3386" s="T27">dempro</ta>
            <ta e="T29" id="Seg_3387" s="T28">n</ta>
            <ta e="T30" id="Seg_3388" s="T29">n</ta>
            <ta e="T31" id="Seg_3389" s="T30">n</ta>
            <ta e="T32" id="Seg_3390" s="T31">v</ta>
            <ta e="T33" id="Seg_3391" s="T32">ptcl</ta>
            <ta e="T34" id="Seg_3392" s="T33">n</ta>
            <ta e="T35" id="Seg_3393" s="T34">v</ta>
            <ta e="T36" id="Seg_3394" s="T35">interj</ta>
            <ta e="T37" id="Seg_3395" s="T36">n</ta>
            <ta e="T38" id="Seg_3396" s="T37">n</ta>
            <ta e="T39" id="Seg_3397" s="T38">v</ta>
            <ta e="T40" id="Seg_3398" s="T39">aux</ta>
            <ta e="T41" id="Seg_3399" s="T40">ptcl</ta>
            <ta e="T42" id="Seg_3400" s="T41">dempro</ta>
            <ta e="T43" id="Seg_3401" s="T42">dempro</ta>
            <ta e="T44" id="Seg_3402" s="T43">v</ta>
            <ta e="T45" id="Seg_3403" s="T44">n</ta>
            <ta e="T46" id="Seg_3404" s="T45">v</ta>
            <ta e="T47" id="Seg_3405" s="T46">n</ta>
            <ta e="T48" id="Seg_3406" s="T47">v</ta>
            <ta e="T49" id="Seg_3407" s="T48">v</ta>
            <ta e="T50" id="Seg_3408" s="T49">n</ta>
            <ta e="T51" id="Seg_3409" s="T50">ptcl</ta>
            <ta e="T52" id="Seg_3410" s="T51">ptcl</ta>
            <ta e="T53" id="Seg_3411" s="T52">v</ta>
            <ta e="T54" id="Seg_3412" s="T53">v</ta>
            <ta e="T55" id="Seg_3413" s="T54">n</ta>
            <ta e="T56" id="Seg_3414" s="T55">v</ta>
            <ta e="T57" id="Seg_3415" s="T56">interj</ta>
            <ta e="T58" id="Seg_3416" s="T57">n</ta>
            <ta e="T59" id="Seg_3417" s="T58">v</ta>
            <ta e="T60" id="Seg_3418" s="T59">que</ta>
            <ta e="T61" id="Seg_3419" s="T60">pers</ta>
            <ta e="T62" id="Seg_3420" s="T61">v</ta>
            <ta e="T63" id="Seg_3421" s="T62">v</ta>
            <ta e="T64" id="Seg_3422" s="T63">n</ta>
            <ta e="T65" id="Seg_3423" s="T64">n</ta>
            <ta e="T66" id="Seg_3424" s="T65">v</ta>
            <ta e="T67" id="Seg_3425" s="T66">aux</ta>
            <ta e="T68" id="Seg_3426" s="T67">n</ta>
            <ta e="T69" id="Seg_3427" s="T68">v</ta>
            <ta e="T70" id="Seg_3428" s="T69">aux</ta>
            <ta e="T71" id="Seg_3429" s="T70">n</ta>
            <ta e="T72" id="Seg_3430" s="T71">v</ta>
            <ta e="T76" id="Seg_3431" s="T75">v</ta>
            <ta e="T77" id="Seg_3432" s="T76">n</ta>
            <ta e="T78" id="Seg_3433" s="T77">n</ta>
            <ta e="T79" id="Seg_3434" s="T78">dempro</ta>
            <ta e="T80" id="Seg_3435" s="T79">ptcl</ta>
            <ta e="T81" id="Seg_3436" s="T80">v</ta>
            <ta e="T82" id="Seg_3437" s="T81">n</ta>
            <ta e="T83" id="Seg_3438" s="T82">n</ta>
            <ta e="T84" id="Seg_3439" s="T83">post</ta>
            <ta e="T85" id="Seg_3440" s="T84">v</ta>
            <ta e="T86" id="Seg_3441" s="T85">n</ta>
            <ta e="T87" id="Seg_3442" s="T86">n</ta>
            <ta e="T88" id="Seg_3443" s="T87">post</ta>
            <ta e="T89" id="Seg_3444" s="T88">dempro</ta>
            <ta e="T90" id="Seg_3445" s="T89">n</ta>
            <ta e="T91" id="Seg_3446" s="T90">v</ta>
            <ta e="T92" id="Seg_3447" s="T91">v</ta>
            <ta e="T93" id="Seg_3448" s="T92">aux</ta>
            <ta e="T94" id="Seg_3449" s="T93">n</ta>
            <ta e="T95" id="Seg_3450" s="T94">v</ta>
            <ta e="T96" id="Seg_3451" s="T95">n</ta>
            <ta e="T97" id="Seg_3452" s="T96">interj</ta>
            <ta e="T98" id="Seg_3453" s="T97">v</ta>
            <ta e="T99" id="Seg_3454" s="T98">v</ta>
            <ta e="T100" id="Seg_3455" s="T99">n</ta>
            <ta e="T101" id="Seg_3456" s="T100">conj</ta>
            <ta e="T102" id="Seg_3457" s="T101">n</ta>
            <ta e="T103" id="Seg_3458" s="T102">adj</ta>
            <ta e="T104" id="Seg_3459" s="T103">conj</ta>
            <ta e="T105" id="Seg_3460" s="T104">adv</ta>
            <ta e="T106" id="Seg_3461" s="T105">cop</ta>
            <ta e="T107" id="Seg_3462" s="T106">aux</ta>
            <ta e="T108" id="Seg_3463" s="T107">v</ta>
            <ta e="T109" id="Seg_3464" s="T108">adv</ta>
            <ta e="T110" id="Seg_3465" s="T109">v</ta>
            <ta e="T111" id="Seg_3466" s="T110">post</ta>
            <ta e="T112" id="Seg_3467" s="T111">ptcl</ta>
            <ta e="T113" id="Seg_3468" s="T112">v</ta>
            <ta e="T114" id="Seg_3469" s="T113">dempro</ta>
            <ta e="T115" id="Seg_3470" s="T114">v</ta>
            <ta e="T116" id="Seg_3471" s="T115">cardnum</ta>
            <ta e="T117" id="Seg_3472" s="T116">n</ta>
            <ta e="T118" id="Seg_3473" s="T117">n</ta>
            <ta e="T119" id="Seg_3474" s="T118">v</ta>
            <ta e="T120" id="Seg_3475" s="T119">dempro</ta>
            <ta e="T121" id="Seg_3476" s="T120">v</ta>
            <ta e="T122" id="Seg_3477" s="T121">v</ta>
            <ta e="T123" id="Seg_3478" s="T122">n</ta>
            <ta e="T124" id="Seg_3479" s="T123">v</ta>
            <ta e="T125" id="Seg_3480" s="T124">v</ta>
            <ta e="T126" id="Seg_3481" s="T125">aux</ta>
            <ta e="T127" id="Seg_3482" s="T126">cardnum</ta>
            <ta e="T128" id="Seg_3483" s="T127">n</ta>
            <ta e="T129" id="Seg_3484" s="T128">n</ta>
            <ta e="T130" id="Seg_3485" s="T129">v</ta>
            <ta e="T131" id="Seg_3486" s="T130">v</ta>
            <ta e="T132" id="Seg_3487" s="T131">ptcl</ta>
            <ta e="T133" id="Seg_3488" s="T132">dempro</ta>
            <ta e="T134" id="Seg_3489" s="T133">n</ta>
            <ta e="T135" id="Seg_3490" s="T134">v</ta>
            <ta e="T136" id="Seg_3491" s="T135">v</ta>
            <ta e="T137" id="Seg_3492" s="T136">n</ta>
            <ta e="T138" id="Seg_3493" s="T137">v</ta>
            <ta e="T139" id="Seg_3494" s="T138">aux</ta>
            <ta e="T140" id="Seg_3495" s="T139">v</ta>
            <ta e="T141" id="Seg_3496" s="T140">dempro</ta>
            <ta e="T142" id="Seg_3497" s="T141">n</ta>
            <ta e="T143" id="Seg_3498" s="T142">dempro</ta>
            <ta e="T144" id="Seg_3499" s="T143">n</ta>
            <ta e="T145" id="Seg_3500" s="T144">v</ta>
            <ta e="T146" id="Seg_3501" s="T145">v</ta>
            <ta e="T147" id="Seg_3502" s="T146">dempro</ta>
            <ta e="T148" id="Seg_3503" s="T147">v</ta>
            <ta e="T149" id="Seg_3504" s="T148">post</ta>
            <ta e="T150" id="Seg_3505" s="T149">v</ta>
            <ta e="T151" id="Seg_3506" s="T150">dempro</ta>
            <ta e="T152" id="Seg_3507" s="T151">v</ta>
            <ta e="T153" id="Seg_3508" s="T152">post</ta>
            <ta e="T154" id="Seg_3509" s="T153">n</ta>
            <ta e="T155" id="Seg_3510" s="T154">v</ta>
            <ta e="T156" id="Seg_3511" s="T155">dempro</ta>
            <ta e="T157" id="Seg_3512" s="T156">v</ta>
            <ta e="T158" id="Seg_3513" s="T157">aux</ta>
            <ta e="T159" id="Seg_3514" s="T158">v</ta>
            <ta e="T160" id="Seg_3515" s="T159">dempro</ta>
            <ta e="T161" id="Seg_3516" s="T160">n</ta>
            <ta e="T162" id="Seg_3517" s="T161">n</ta>
            <ta e="T163" id="Seg_3518" s="T162">n</ta>
            <ta e="T164" id="Seg_3519" s="T163">v</ta>
            <ta e="T165" id="Seg_3520" s="T164">v</ta>
            <ta e="T166" id="Seg_3521" s="T165">n</ta>
            <ta e="T167" id="Seg_3522" s="T166">n</ta>
            <ta e="T168" id="Seg_3523" s="T167">v</ta>
            <ta e="T169" id="Seg_3524" s="T168">v</ta>
            <ta e="T170" id="Seg_3525" s="T169">dempro</ta>
            <ta e="T171" id="Seg_3526" s="T170">ptcl</ta>
            <ta e="T172" id="Seg_3527" s="T171">v</ta>
            <ta e="T173" id="Seg_3528" s="T172">n</ta>
            <ta e="T174" id="Seg_3529" s="T173">v</ta>
            <ta e="T175" id="Seg_3530" s="T174">ptcl</ta>
            <ta e="T176" id="Seg_3531" s="T175">dempro</ta>
            <ta e="T177" id="Seg_3532" s="T176">n</ta>
            <ta e="T178" id="Seg_3533" s="T177">v</ta>
            <ta e="T179" id="Seg_3534" s="T178">n</ta>
            <ta e="T180" id="Seg_3535" s="T179">n</ta>
            <ta e="T181" id="Seg_3536" s="T180">post</ta>
            <ta e="T182" id="Seg_3537" s="T181">v</ta>
            <ta e="T183" id="Seg_3538" s="T182">interj</ta>
            <ta e="T184" id="Seg_3539" s="T183">v</ta>
            <ta e="T185" id="Seg_3540" s="T184">dempro</ta>
            <ta e="T186" id="Seg_3541" s="T185">v</ta>
            <ta e="T187" id="Seg_3542" s="T186">aux</ta>
            <ta e="T188" id="Seg_3543" s="T187">n</ta>
            <ta e="T189" id="Seg_3544" s="T188">n</ta>
            <ta e="T190" id="Seg_3545" s="T189">v</ta>
            <ta e="T191" id="Seg_3546" s="T190">v</ta>
            <ta e="T192" id="Seg_3547" s="T191">v</ta>
            <ta e="T193" id="Seg_3548" s="T192">aux</ta>
            <ta e="T194" id="Seg_3549" s="T193">dempro</ta>
            <ta e="T195" id="Seg_3550" s="T194">n</ta>
            <ta e="T196" id="Seg_3551" s="T195">dempro</ta>
            <ta e="T197" id="Seg_3552" s="T196">n</ta>
            <ta e="T198" id="Seg_3553" s="T197">ptcl</ta>
            <ta e="T199" id="Seg_3554" s="T198">v</ta>
            <ta e="T200" id="Seg_3555" s="T199">dempro</ta>
            <ta e="T201" id="Seg_3556" s="T200">v</ta>
            <ta e="T202" id="Seg_3557" s="T201">v</ta>
            <ta e="T203" id="Seg_3558" s="T202">dempro</ta>
            <ta e="T204" id="Seg_3559" s="T203">n</ta>
            <ta e="T205" id="Seg_3560" s="T204">v</ta>
            <ta e="T206" id="Seg_3561" s="T205">dempro</ta>
            <ta e="T207" id="Seg_3562" s="T206">n</ta>
            <ta e="T208" id="Seg_3563" s="T207">ptcl</ta>
            <ta e="T209" id="Seg_3564" s="T208">adj</ta>
            <ta e="T210" id="Seg_3565" s="T209">v</ta>
            <ta e="T211" id="Seg_3566" s="T210">ptcl</ta>
            <ta e="T212" id="Seg_3567" s="T211">v</ta>
            <ta e="T213" id="Seg_3568" s="T212">n</ta>
            <ta e="T214" id="Seg_3569" s="T213">v</ta>
            <ta e="T215" id="Seg_3570" s="T214">aux</ta>
            <ta e="T216" id="Seg_3571" s="T215">dempro</ta>
            <ta e="T217" id="Seg_3572" s="T216">v</ta>
            <ta e="T218" id="Seg_3573" s="T217">post</ta>
            <ta e="T219" id="Seg_3574" s="T218">ptcl</ta>
            <ta e="T220" id="Seg_3575" s="T219">v</ta>
            <ta e="T221" id="Seg_3576" s="T220">adj</ta>
            <ta e="T222" id="Seg_3577" s="T221">n</ta>
            <ta e="T223" id="Seg_3578" s="T222">n</ta>
            <ta e="T224" id="Seg_3579" s="T223">ptcl</ta>
            <ta e="T225" id="Seg_3580" s="T224">adj</ta>
            <ta e="T226" id="Seg_3581" s="T225">ptcl</ta>
            <ta e="T227" id="Seg_3582" s="T226">v</ta>
            <ta e="T228" id="Seg_3583" s="T227">dempro</ta>
            <ta e="T229" id="Seg_3584" s="T228">n</ta>
            <ta e="T230" id="Seg_3585" s="T229">ptcl</ta>
            <ta e="T231" id="Seg_3586" s="T230">v</ta>
            <ta e="T232" id="Seg_3587" s="T231">aux</ta>
            <ta e="T233" id="Seg_3588" s="T232">dempro</ta>
            <ta e="T234" id="Seg_3589" s="T233">v</ta>
            <ta e="T235" id="Seg_3590" s="T234">post</ta>
            <ta e="T236" id="Seg_3591" s="T235">ptcl</ta>
            <ta e="T237" id="Seg_3592" s="T236">v</ta>
            <ta e="T238" id="Seg_3593" s="T237">ptcl</ta>
            <ta e="T239" id="Seg_3594" s="T238">dempro</ta>
            <ta e="T240" id="Seg_3595" s="T239">n</ta>
            <ta e="T241" id="Seg_3596" s="T240">n</ta>
            <ta e="T242" id="Seg_3597" s="T241">posspr</ta>
            <ta e="T243" id="Seg_3598" s="T242">adj</ta>
            <ta e="T244" id="Seg_3599" s="T243">ptcl</ta>
            <ta e="T245" id="Seg_3600" s="T244">v</ta>
            <ta e="T246" id="Seg_3601" s="T245">n</ta>
            <ta e="T247" id="Seg_3602" s="T246">n</ta>
            <ta e="T248" id="Seg_3603" s="T247">v</ta>
            <ta e="T249" id="Seg_3604" s="T248">ptcl</ta>
            <ta e="T250" id="Seg_3605" s="T249">ptcl</ta>
            <ta e="T251" id="Seg_3606" s="T250">v</ta>
            <ta e="T252" id="Seg_3607" s="T251">dempro</ta>
            <ta e="T253" id="Seg_3608" s="T252">ptcl</ta>
            <ta e="T254" id="Seg_3609" s="T253">adj</ta>
            <ta e="T255" id="Seg_3610" s="T254">n</ta>
            <ta e="T256" id="Seg_3611" s="T255">v</ta>
            <ta e="T257" id="Seg_3612" s="T256">cardnum</ta>
            <ta e="T258" id="Seg_3613" s="T257">n</ta>
            <ta e="T259" id="Seg_3614" s="T258">ptcl</ta>
            <ta e="T260" id="Seg_3615" s="T259">cop</ta>
            <ta e="T261" id="Seg_3616" s="T260">adv</ta>
            <ta e="T262" id="Seg_3617" s="T261">n</ta>
            <ta e="T263" id="Seg_3618" s="T262">v</ta>
            <ta e="T264" id="Seg_3619" s="T263">dempro</ta>
            <ta e="T265" id="Seg_3620" s="T264">n</ta>
            <ta e="T266" id="Seg_3621" s="T265">n</ta>
            <ta e="T267" id="Seg_3622" s="T266">post</ta>
            <ta e="T268" id="Seg_3623" s="T267">v</ta>
            <ta e="T269" id="Seg_3624" s="T268">n</ta>
            <ta e="T270" id="Seg_3625" s="T269">ptcl</ta>
            <ta e="T271" id="Seg_3626" s="T270">adj</ta>
            <ta e="T272" id="Seg_3627" s="T271">cop</ta>
            <ta e="T273" id="Seg_3628" s="T272">adv</ta>
            <ta e="T274" id="Seg_3629" s="T273">v</ta>
            <ta e="T275" id="Seg_3630" s="T274">adj</ta>
            <ta e="T276" id="Seg_3631" s="T275">cop</ta>
            <ta e="T277" id="Seg_3632" s="T276">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_3633" s="T1">np.h:Th</ta>
            <ta e="T3" id="Seg_3634" s="T2">np:G</ta>
            <ta e="T6" id="Seg_3635" s="T4">np:Path</ta>
            <ta e="T9" id="Seg_3636" s="T7">0.3.h:Th</ta>
            <ta e="T11" id="Seg_3637" s="T10">np.h:A</ta>
            <ta e="T15" id="Seg_3638" s="T14">np:G</ta>
            <ta e="T19" id="Seg_3639" s="T18">np:G</ta>
            <ta e="T23" id="Seg_3640" s="T22">np.h:Poss</ta>
            <ta e="T24" id="Seg_3641" s="T23">np:Poss</ta>
            <ta e="T25" id="Seg_3642" s="T24">np:P</ta>
            <ta e="T27" id="Seg_3643" s="T25">0.1.h:A</ta>
            <ta e="T28" id="Seg_3644" s="T27">pro:Time</ta>
            <ta e="T29" id="Seg_3645" s="T28">np:Poss</ta>
            <ta e="T30" id="Seg_3646" s="T29">np:L</ta>
            <ta e="T31" id="Seg_3647" s="T30">np:Th</ta>
            <ta e="T34" id="Seg_3648" s="T33">np.h:A</ta>
            <ta e="T38" id="Seg_3649" s="T37">np.h:A</ta>
            <ta e="T43" id="Seg_3650" s="T42">pro:P</ta>
            <ta e="T45" id="Seg_3651" s="T44">np.h:E</ta>
            <ta e="T48" id="Seg_3652" s="T47">0.3.h:A</ta>
            <ta e="T49" id="Seg_3653" s="T48">0.1.h:A</ta>
            <ta e="T50" id="Seg_3654" s="T49">0.3.h:A</ta>
            <ta e="T53" id="Seg_3655" s="T52">0.1.h:A</ta>
            <ta e="T54" id="Seg_3656" s="T53">0.3.h:A</ta>
            <ta e="T55" id="Seg_3657" s="T54">np.h:A</ta>
            <ta e="T58" id="Seg_3658" s="T57">np:G</ta>
            <ta e="T59" id="Seg_3659" s="T58">0.2.h:A</ta>
            <ta e="T60" id="Seg_3660" s="T59">0.2.h:Th</ta>
            <ta e="T61" id="Seg_3661" s="T60">pro.h:A</ta>
            <ta e="T63" id="Seg_3662" s="T62">0.3.h:A</ta>
            <ta e="T64" id="Seg_3663" s="T63">np.h:A</ta>
            <ta e="T65" id="Seg_3664" s="T64">np:G</ta>
            <ta e="T67" id="Seg_3665" s="T65">0.3.h:A</ta>
            <ta e="T68" id="Seg_3666" s="T67">0.3.h:Poss np:G</ta>
            <ta e="T71" id="Seg_3667" s="T70">np.h:A</ta>
            <ta e="T77" id="Seg_3668" s="T76">np:Poss</ta>
            <ta e="T79" id="Seg_3669" s="T78">pro:Th</ta>
            <ta e="T81" id="Seg_3670" s="T80">0.3.h:A</ta>
            <ta e="T82" id="Seg_3671" s="T81">np.h:A</ta>
            <ta e="T84" id="Seg_3672" s="T82">pp:Path</ta>
            <ta e="T86" id="Seg_3673" s="T85">np.h:A</ta>
            <ta e="T88" id="Seg_3674" s="T86">pp:Path</ta>
            <ta e="T90" id="Seg_3675" s="T89">np.h:A</ta>
            <ta e="T95" id="Seg_3676" s="T94">0.3.h:A</ta>
            <ta e="T96" id="Seg_3677" s="T95">np:L</ta>
            <ta e="T98" id="Seg_3678" s="T97">0.3.h:A</ta>
            <ta e="T99" id="Seg_3679" s="T98">0.3.h:A</ta>
            <ta e="T102" id="Seg_3680" s="T101">np.h:Th</ta>
            <ta e="T108" id="Seg_3681" s="T107">0.3.h:A</ta>
            <ta e="T113" id="Seg_3682" s="T112">0.3.h:A</ta>
            <ta e="T117" id="Seg_3683" s="T116">np:L</ta>
            <ta e="T118" id="Seg_3684" s="T117">np:P</ta>
            <ta e="T119" id="Seg_3685" s="T118">0.3.h:A</ta>
            <ta e="T121" id="Seg_3686" s="T120">0.1.h:A</ta>
            <ta e="T123" id="Seg_3687" s="T122">np.h:A</ta>
            <ta e="T128" id="Seg_3688" s="T127">0.3.h:A</ta>
            <ta e="T129" id="Seg_3689" s="T128">np:P</ta>
            <ta e="T134" id="Seg_3690" s="T133">np:P</ta>
            <ta e="T136" id="Seg_3691" s="T135">0.3.h:A</ta>
            <ta e="T137" id="Seg_3692" s="T136">np:P</ta>
            <ta e="T140" id="Seg_3693" s="T139">0.3.h:E</ta>
            <ta e="T142" id="Seg_3694" s="T141">np.h:A</ta>
            <ta e="T144" id="Seg_3695" s="T143">np:P</ta>
            <ta e="T150" id="Seg_3696" s="T149">0.3:Th</ta>
            <ta e="T154" id="Seg_3697" s="T153">np:G</ta>
            <ta e="T155" id="Seg_3698" s="T154">0.3.h:A</ta>
            <ta e="T161" id="Seg_3699" s="T160">np.h:A</ta>
            <ta e="T162" id="Seg_3700" s="T161">np:So</ta>
            <ta e="T163" id="Seg_3701" s="T162">np:Th</ta>
            <ta e="T165" id="Seg_3702" s="T164">0.3.h:A</ta>
            <ta e="T166" id="Seg_3703" s="T165">np:So</ta>
            <ta e="T167" id="Seg_3704" s="T166">np:Th</ta>
            <ta e="T169" id="Seg_3705" s="T168">0.3.h:A</ta>
            <ta e="T170" id="Seg_3706" s="T169">pro.h:Poss</ta>
            <ta e="T172" id="Seg_3707" s="T171">0.3:Th</ta>
            <ta e="T173" id="Seg_3708" s="T172">np:Th</ta>
            <ta e="T177" id="Seg_3709" s="T176">np:P</ta>
            <ta e="T179" id="Seg_3710" s="T178">0.3.h:A</ta>
            <ta e="T181" id="Seg_3711" s="T179">pp:Com</ta>
            <ta e="T184" id="Seg_3712" s="T183">0.3.h:A</ta>
            <ta e="T188" id="Seg_3713" s="T187">np:P</ta>
            <ta e="T189" id="Seg_3714" s="T188">np:A</ta>
            <ta e="T195" id="Seg_3715" s="T194">np.h:A</ta>
            <ta e="T197" id="Seg_3716" s="T196">np:P</ta>
            <ta e="T204" id="Seg_3717" s="T203">np.h:A</ta>
            <ta e="T207" id="Seg_3718" s="T206">0.1.h:Poss np:Th</ta>
            <ta e="T210" id="Seg_3719" s="T209">0.3:Th</ta>
            <ta e="T213" id="Seg_3720" s="T212">0.3.h:Poss np:P</ta>
            <ta e="T215" id="Seg_3721" s="T213">0.3.h:A</ta>
            <ta e="T220" id="Seg_3722" s="T219">0.3.h:A</ta>
            <ta e="T223" id="Seg_3723" s="T222">0.1.h:Poss np:Th</ta>
            <ta e="T229" id="Seg_3724" s="T228">np:P</ta>
            <ta e="T232" id="Seg_3725" s="T230">0.3.h:A</ta>
            <ta e="T237" id="Seg_3726" s="T236">0.3.h:A</ta>
            <ta e="T241" id="Seg_3727" s="T240">np:Th</ta>
            <ta e="T242" id="Seg_3728" s="T241">pro.h:Poss</ta>
            <ta e="T247" id="Seg_3729" s="T246">0.3.h:Poss np:P</ta>
            <ta e="T248" id="Seg_3730" s="T247">0.3.h:A</ta>
            <ta e="T251" id="Seg_3731" s="T250">0.3.h:A</ta>
            <ta e="T255" id="Seg_3732" s="T254">0.3.h:Poss np:P</ta>
            <ta e="T256" id="Seg_3733" s="T255">0.3.h:A</ta>
            <ta e="T258" id="Seg_3734" s="T257">0.3.h:Poss np:Th</ta>
            <ta e="T261" id="Seg_3735" s="T260">adv:Time</ta>
            <ta e="T262" id="Seg_3736" s="T261">0.3.h:A</ta>
            <ta e="T265" id="Seg_3737" s="T264">np:Poss</ta>
            <ta e="T267" id="Seg_3738" s="T265">pp:Com</ta>
            <ta e="T268" id="Seg_3739" s="T267">0.1.h:A</ta>
            <ta e="T269" id="Seg_3740" s="T268">0.1.h:Poss np:Th</ta>
            <ta e="T273" id="Seg_3741" s="T272">pro:Time</ta>
            <ta e="T274" id="Seg_3742" s="T273">0.3.h:A</ta>
            <ta e="T277" id="Seg_3743" s="T276">0.3.h:P</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_3744" s="T1">np.h:S</ta>
            <ta e="T4" id="Seg_3745" s="T3">v:pred</ta>
            <ta e="T7" id="Seg_3746" s="T6">s:adv</ta>
            <ta e="T9" id="Seg_3747" s="T7">0.3.h:S v:pred</ta>
            <ta e="T11" id="Seg_3748" s="T10">np.h:S</ta>
            <ta e="T12" id="Seg_3749" s="T11">v:pred</ta>
            <ta e="T16" id="Seg_3750" s="T14">s:comp</ta>
            <ta e="T17" id="Seg_3751" s="T16">n:pred</ta>
            <ta e="T20" id="Seg_3752" s="T17">s:comp</ta>
            <ta e="T21" id="Seg_3753" s="T20">cop</ta>
            <ta e="T25" id="Seg_3754" s="T24">np:O</ta>
            <ta e="T27" id="Seg_3755" s="T25">0.1.h:S v:pred</ta>
            <ta e="T31" id="Seg_3756" s="T30">np:S</ta>
            <ta e="T32" id="Seg_3757" s="T31">v:pred</ta>
            <ta e="T34" id="Seg_3758" s="T33">np.h:S</ta>
            <ta e="T35" id="Seg_3759" s="T34">v:pred</ta>
            <ta e="T38" id="Seg_3760" s="T37">np.h:S</ta>
            <ta e="T39" id="Seg_3761" s="T38">adj:pred</ta>
            <ta e="T40" id="Seg_3762" s="T39">cop</ta>
            <ta e="T44" id="Seg_3763" s="T42">s:temp</ta>
            <ta e="T45" id="Seg_3764" s="T44">np.h:S</ta>
            <ta e="T46" id="Seg_3765" s="T45">v:pred</ta>
            <ta e="T48" id="Seg_3766" s="T47">0.3.h:S v:pred</ta>
            <ta e="T49" id="Seg_3767" s="T48">0.1.h:S v:pred</ta>
            <ta e="T50" id="Seg_3768" s="T49">np.h:S</ta>
            <ta e="T53" id="Seg_3769" s="T52">0.1.h:S v:pred</ta>
            <ta e="T54" id="Seg_3770" s="T53">0.3.h:S v:pred</ta>
            <ta e="T55" id="Seg_3771" s="T54">np.h:S</ta>
            <ta e="T56" id="Seg_3772" s="T55">v:pred</ta>
            <ta e="T59" id="Seg_3773" s="T58">0.2.h:S v:pred</ta>
            <ta e="T60" id="Seg_3774" s="T59">0.2.h:S pro:pred</ta>
            <ta e="T61" id="Seg_3775" s="T60">pro.h:S</ta>
            <ta e="T62" id="Seg_3776" s="T61">v:pred</ta>
            <ta e="T63" id="Seg_3777" s="T62">0.3.h:S v:pred</ta>
            <ta e="T64" id="Seg_3778" s="T63">np.h:S</ta>
            <ta e="T67" id="Seg_3779" s="T64">s:temp</ta>
            <ta e="T70" id="Seg_3780" s="T68">v:pred</ta>
            <ta e="T71" id="Seg_3781" s="T70">np.h:S</ta>
            <ta e="T72" id="Seg_3782" s="T71">v:pred</ta>
            <ta e="T78" id="Seg_3783" s="T77">n:pred</ta>
            <ta e="T79" id="Seg_3784" s="T78">pro:S</ta>
            <ta e="T81" id="Seg_3785" s="T80">0.3.h:S v:pred</ta>
            <ta e="T82" id="Seg_3786" s="T81">np.h:S</ta>
            <ta e="T85" id="Seg_3787" s="T84">v:pred</ta>
            <ta e="T86" id="Seg_3788" s="T85">np.h:S</ta>
            <ta e="T90" id="Seg_3789" s="T89">np.h:S</ta>
            <ta e="T93" id="Seg_3790" s="T90">v:pred</ta>
            <ta e="T95" id="Seg_3791" s="T94">0.3.h:S v:pred</ta>
            <ta e="T98" id="Seg_3792" s="T97">0.3.h:S v:pred</ta>
            <ta e="T99" id="Seg_3793" s="T98">0.3.h:S v:pred</ta>
            <ta e="T102" id="Seg_3794" s="T101">np.h:S</ta>
            <ta e="T106" id="Seg_3795" s="T102">s:comp</ta>
            <ta e="T107" id="Seg_3796" s="T106">ptcl:pred</ta>
            <ta e="T108" id="Seg_3797" s="T107">0.3.h:S v:pred</ta>
            <ta e="T111" id="Seg_3798" s="T108">s:temp</ta>
            <ta e="T113" id="Seg_3799" s="T112">0.3.h:S v:pred</ta>
            <ta e="T115" id="Seg_3800" s="T113">s:adv</ta>
            <ta e="T119" id="Seg_3801" s="T118">0.3.h:S v:pred</ta>
            <ta e="T122" id="Seg_3802" s="T120">s:adv</ta>
            <ta e="T123" id="Seg_3803" s="T122">np.h:S</ta>
            <ta e="T124" id="Seg_3804" s="T123">s:adv</ta>
            <ta e="T126" id="Seg_3805" s="T124">v:pred</ta>
            <ta e="T128" id="Seg_3806" s="T127">np.h:S</ta>
            <ta e="T130" id="Seg_3807" s="T128">s:purp</ta>
            <ta e="T131" id="Seg_3808" s="T130">v:pred</ta>
            <ta e="T135" id="Seg_3809" s="T133">s:purp</ta>
            <ta e="T136" id="Seg_3810" s="T135">v:pred</ta>
            <ta e="T137" id="Seg_3811" s="T136">np.h:O</ta>
            <ta e="T139" id="Seg_3812" s="T137">s:rel</ta>
            <ta e="T140" id="Seg_3813" s="T139">v:pred</ta>
            <ta e="T142" id="Seg_3814" s="T141">np.h:S</ta>
            <ta e="T144" id="Seg_3815" s="T143">np.h:O</ta>
            <ta e="T145" id="Seg_3816" s="T144">s:adv</ta>
            <ta e="T146" id="Seg_3817" s="T145">v:pred</ta>
            <ta e="T149" id="Seg_3818" s="T146">s:temp</ta>
            <ta e="T150" id="Seg_3819" s="T149">0.3:S v:pred</ta>
            <ta e="T153" id="Seg_3820" s="T150">s:temp</ta>
            <ta e="T155" id="Seg_3821" s="T154">0.3.h:S v:pred</ta>
            <ta e="T158" id="Seg_3822" s="T155">s:adv</ta>
            <ta e="T159" id="Seg_3823" s="T158">v:pred</ta>
            <ta e="T161" id="Seg_3824" s="T160">np.h:S</ta>
            <ta e="T163" id="Seg_3825" s="T162">n:pred</ta>
            <ta e="T164" id="Seg_3826" s="T163">0.2.h:S cop</ta>
            <ta e="T165" id="Seg_3827" s="T164">0.3.h:S v:pred</ta>
            <ta e="T167" id="Seg_3828" s="T166">n:pred</ta>
            <ta e="T168" id="Seg_3829" s="T167">0.2.h:S cop</ta>
            <ta e="T169" id="Seg_3830" s="T168">0.3.h:S v:pred</ta>
            <ta e="T170" id="Seg_3831" s="T169">pro.h:S</ta>
            <ta e="T172" id="Seg_3832" s="T171">v:pred</ta>
            <ta e="T178" id="Seg_3833" s="T172">s:temp</ta>
            <ta e="T179" id="Seg_3834" s="T178">np.h:S</ta>
            <ta e="T182" id="Seg_3835" s="T181">v:pred</ta>
            <ta e="T184" id="Seg_3836" s="T183">s:adv</ta>
            <ta e="T187" id="Seg_3837" s="T184">s:temp</ta>
            <ta e="T188" id="Seg_3838" s="T187">np:O</ta>
            <ta e="T189" id="Seg_3839" s="T188">np:S</ta>
            <ta e="T191" id="Seg_3840" s="T189">s:adv</ta>
            <ta e="T193" id="Seg_3841" s="T191">v:pred</ta>
            <ta e="T195" id="Seg_3842" s="T194">np.h:S</ta>
            <ta e="T197" id="Seg_3843" s="T196">np:O</ta>
            <ta e="T199" id="Seg_3844" s="T198">v:pred</ta>
            <ta e="T202" id="Seg_3845" s="T199">s:adv</ta>
            <ta e="T204" id="Seg_3846" s="T203">np.h:S</ta>
            <ta e="T205" id="Seg_3847" s="T204">v:pred</ta>
            <ta e="T207" id="Seg_3848" s="T206">np:S</ta>
            <ta e="T209" id="Seg_3849" s="T208">adj:pred</ta>
            <ta e="T210" id="Seg_3850" s="T209">0.3:S v:pred</ta>
            <ta e="T212" id="Seg_3851" s="T211">s:adv</ta>
            <ta e="T213" id="Seg_3852" s="T212">np:O</ta>
            <ta e="T215" id="Seg_3853" s="T213">0.3.h:S v:pred</ta>
            <ta e="T218" id="Seg_3854" s="T215">s:temp</ta>
            <ta e="T220" id="Seg_3855" s="T219">0.3.h:S v:pred</ta>
            <ta e="T223" id="Seg_3856" s="T222">np:S</ta>
            <ta e="T225" id="Seg_3857" s="T224">adj:pred</ta>
            <ta e="T227" id="Seg_3858" s="T226">s:temp</ta>
            <ta e="T229" id="Seg_3859" s="T228">np:O</ta>
            <ta e="T232" id="Seg_3860" s="T230">0.3.h:S v:pred</ta>
            <ta e="T235" id="Seg_3861" s="T232">s:temp</ta>
            <ta e="T237" id="Seg_3862" s="T236">0.3.h:S v:pred</ta>
            <ta e="T241" id="Seg_3863" s="T240">np:S</ta>
            <ta e="T243" id="Seg_3864" s="T242">adj:pred</ta>
            <ta e="T245" id="Seg_3865" s="T244">s:adv</ta>
            <ta e="T247" id="Seg_3866" s="T246">np:O</ta>
            <ta e="T248" id="Seg_3867" s="T247">0.3.h:S v:pred</ta>
            <ta e="T251" id="Seg_3868" s="T250">0.3.h:S v:pred</ta>
            <ta e="T255" id="Seg_3869" s="T254">np:O</ta>
            <ta e="T256" id="Seg_3870" s="T255">0.3.h:S v:pred</ta>
            <ta e="T258" id="Seg_3871" s="T257">np:S</ta>
            <ta e="T259" id="Seg_3872" s="T258">ptcl:pred</ta>
            <ta e="T260" id="Seg_3873" s="T259">cop</ta>
            <ta e="T262" id="Seg_3874" s="T261">np.h:S</ta>
            <ta e="T263" id="Seg_3875" s="T262">v:pred</ta>
            <ta e="T268" id="Seg_3876" s="T263">s:purp</ta>
            <ta e="T269" id="Seg_3877" s="T268">np:S</ta>
            <ta e="T271" id="Seg_3878" s="T270">adj:pred</ta>
            <ta e="T272" id="Seg_3879" s="T271">cop</ta>
            <ta e="T274" id="Seg_3880" s="T273">0.3.h:S v:pred</ta>
            <ta e="T276" id="Seg_3881" s="T274">s:adv</ta>
            <ta e="T277" id="Seg_3882" s="T276">0.3.h:S v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST">
            <ta e="T2" id="Seg_3883" s="T1">new</ta>
            <ta e="T3" id="Seg_3884" s="T2">new</ta>
            <ta e="T9" id="Seg_3885" s="T7">0.giv-active</ta>
            <ta e="T11" id="Seg_3886" s="T10">giv-active</ta>
            <ta e="T12" id="Seg_3887" s="T11">quot-sp</ta>
            <ta e="T15" id="Seg_3888" s="T14">giv-active-Q</ta>
            <ta e="T16" id="Seg_3889" s="T15">0.giv-active-Q</ta>
            <ta e="T19" id="Seg_3890" s="T18">giv-active-Q</ta>
            <ta e="T20" id="Seg_3891" s="T19">0.giv-active-Q</ta>
            <ta e="T23" id="Seg_3892" s="T22">accs-gen-Q</ta>
            <ta e="T24" id="Seg_3893" s="T23">accs-gen-Q</ta>
            <ta e="T25" id="Seg_3894" s="T24">accs-inf-Q</ta>
            <ta e="T27" id="Seg_3895" s="T25">0.giv-active-Q</ta>
            <ta e="T29" id="Seg_3896" s="T28">accs-sit</ta>
            <ta e="T30" id="Seg_3897" s="T29">accs-inf</ta>
            <ta e="T31" id="Seg_3898" s="T30">new</ta>
            <ta e="T34" id="Seg_3899" s="T33">new</ta>
            <ta e="T35" id="Seg_3900" s="T34">quot-sp</ta>
            <ta e="T37" id="Seg_3901" s="T36">new-Q</ta>
            <ta e="T38" id="Seg_3902" s="T37">giv-inactive-Q</ta>
            <ta e="T43" id="Seg_3903" s="T42">accs-sit</ta>
            <ta e="T45" id="Seg_3904" s="T44">giv-active</ta>
            <ta e="T46" id="Seg_3905" s="T45">quot-sp</ta>
            <ta e="T47" id="Seg_3906" s="T46">giv-active-Q</ta>
            <ta e="T48" id="Seg_3907" s="T47">0.quot-sp</ta>
            <ta e="T49" id="Seg_3908" s="T48">0.accs-aggr-Q</ta>
            <ta e="T50" id="Seg_3909" s="T49">giv-active</ta>
            <ta e="T53" id="Seg_3910" s="T52">0.giv-active-Q</ta>
            <ta e="T54" id="Seg_3911" s="T53">0.quot-sp</ta>
            <ta e="T55" id="Seg_3912" s="T54">giv-inactive</ta>
            <ta e="T56" id="Seg_3913" s="T55">quot-sp</ta>
            <ta e="T58" id="Seg_3914" s="T57">accs-sit-Q</ta>
            <ta e="T59" id="Seg_3915" s="T58">0.giv-active-Q</ta>
            <ta e="T60" id="Seg_3916" s="T59">0.giv-active-Q</ta>
            <ta e="T61" id="Seg_3917" s="T60">giv-active-Q</ta>
            <ta e="T63" id="Seg_3918" s="T62">0.quot-sp</ta>
            <ta e="T64" id="Seg_3919" s="T63">giv-active</ta>
            <ta e="T65" id="Seg_3920" s="T64">giv-inactive</ta>
            <ta e="T67" id="Seg_3921" s="T66">0.giv-active</ta>
            <ta e="T68" id="Seg_3922" s="T67">accs-inf</ta>
            <ta e="T71" id="Seg_3923" s="T70">giv-active</ta>
            <ta e="T72" id="Seg_3924" s="T71">quot-sp</ta>
            <ta e="T76" id="Seg_3925" s="T75">0.quot-sp</ta>
            <ta e="T77" id="Seg_3926" s="T76">accs-aggr</ta>
            <ta e="T78" id="Seg_3927" s="T77">accs-inf</ta>
            <ta e="T81" id="Seg_3928" s="T80">0.accs-aggr</ta>
            <ta e="T82" id="Seg_3929" s="T81">giv-inactive</ta>
            <ta e="T83" id="Seg_3930" s="T82">giv-inactive</ta>
            <ta e="T86" id="Seg_3931" s="T85">giv-inactive</ta>
            <ta e="T87" id="Seg_3932" s="T86">giv-inactive</ta>
            <ta e="T90" id="Seg_3933" s="T89">giv-active</ta>
            <ta e="T94" id="Seg_3934" s="T93">giv-inactive-Q</ta>
            <ta e="T95" id="Seg_3935" s="T94">0.quot-sp</ta>
            <ta e="T96" id="Seg_3936" s="T95">accs-sit</ta>
            <ta e="T98" id="Seg_3937" s="T97">0.quot-sp</ta>
            <ta e="T99" id="Seg_3938" s="T98">0.giv-inactive</ta>
            <ta e="T102" id="Seg_3939" s="T101">giv-active-Q</ta>
            <ta e="T108" id="Seg_3940" s="T107">0.quot-sp</ta>
            <ta e="T113" id="Seg_3941" s="T112">0.giv-inactive</ta>
            <ta e="T117" id="Seg_3942" s="T116">new</ta>
            <ta e="T118" id="Seg_3943" s="T117">new</ta>
            <ta e="T119" id="Seg_3944" s="T118">0.giv-active</ta>
            <ta e="T121" id="Seg_3945" s="T120">0.giv-active-Q</ta>
            <ta e="T122" id="Seg_3946" s="T121">0.quot-sp</ta>
            <ta e="T123" id="Seg_3947" s="T122">giv-active</ta>
            <ta e="T128" id="Seg_3948" s="T127">new</ta>
            <ta e="T129" id="Seg_3949" s="T128">accs-gen</ta>
            <ta e="T134" id="Seg_3950" s="T133">giv-active</ta>
            <ta e="T136" id="Seg_3951" s="T135">0.giv-active</ta>
            <ta e="T137" id="Seg_3952" s="T136">giv-inactive</ta>
            <ta e="T140" id="Seg_3953" s="T139">0.giv-active</ta>
            <ta e="T142" id="Seg_3954" s="T141">giv-active</ta>
            <ta e="T144" id="Seg_3955" s="T143">giv-active</ta>
            <ta e="T150" id="Seg_3956" s="T149">0.giv-active</ta>
            <ta e="T154" id="Seg_3957" s="T153">new</ta>
            <ta e="T155" id="Seg_3958" s="T154">0.giv-active</ta>
            <ta e="T159" id="Seg_3959" s="T158">quot-sp</ta>
            <ta e="T161" id="Seg_3960" s="T160">giv-active</ta>
            <ta e="T162" id="Seg_3961" s="T161">accs-gen-Q</ta>
            <ta e="T163" id="Seg_3962" s="T162">new-Q</ta>
            <ta e="T165" id="Seg_3963" s="T164">0.quot-sp</ta>
            <ta e="T166" id="Seg_3964" s="T165">accs-gen-Q</ta>
            <ta e="T167" id="Seg_3965" s="T166">new-Q</ta>
            <ta e="T169" id="Seg_3966" s="T168">0.quot-sp</ta>
            <ta e="T170" id="Seg_3967" s="T169">giv-active</ta>
            <ta e="T173" id="Seg_3968" s="T172">giv-active</ta>
            <ta e="T177" id="Seg_3969" s="T176">giv-inactive</ta>
            <ta e="T179" id="Seg_3970" s="T178">giv-inactive</ta>
            <ta e="T180" id="Seg_3971" s="T179">giv-active</ta>
            <ta e="T184" id="Seg_3972" s="T183">0.quot-sp</ta>
            <ta e="T187" id="Seg_3973" s="T186">0.giv-inactive</ta>
            <ta e="T188" id="Seg_3974" s="T187">giv-inactive</ta>
            <ta e="T189" id="Seg_3975" s="T188">giv-inactive</ta>
            <ta e="T195" id="Seg_3976" s="T194">giv-active</ta>
            <ta e="T197" id="Seg_3977" s="T196">giv-active</ta>
            <ta e="T204" id="Seg_3978" s="T203">giv-active</ta>
            <ta e="T205" id="Seg_3979" s="T204">quot-sp</ta>
            <ta e="T207" id="Seg_3980" s="T206">accs-inf-Q</ta>
            <ta e="T210" id="Seg_3981" s="T209">0.giv-active-Q</ta>
            <ta e="T212" id="Seg_3982" s="T211">0.quot-sp</ta>
            <ta e="T213" id="Seg_3983" s="T212">accs-inf</ta>
            <ta e="T215" id="Seg_3984" s="T213">0.giv-active</ta>
            <ta e="T220" id="Seg_3985" s="T219">0.giv-active</ta>
            <ta e="T223" id="Seg_3986" s="T222">accs-inf-Q</ta>
            <ta e="T227" id="Seg_3987" s="T226">0.quot-sp</ta>
            <ta e="T229" id="Seg_3988" s="T228">giv-active</ta>
            <ta e="T232" id="Seg_3989" s="T230">0.giv-active</ta>
            <ta e="T237" id="Seg_3990" s="T236">0.giv-active</ta>
            <ta e="T241" id="Seg_3991" s="T240">accs-inf-Q</ta>
            <ta e="T242" id="Seg_3992" s="T241">giv-active-Q</ta>
            <ta e="T245" id="Seg_3993" s="T244">0.quot-sp</ta>
            <ta e="T247" id="Seg_3994" s="T246">accs-inf</ta>
            <ta e="T248" id="Seg_3995" s="T247">0.giv-active-Q</ta>
            <ta e="T251" id="Seg_3996" s="T250">0.giv-active-Q</ta>
            <ta e="T255" id="Seg_3997" s="T254">accs-inf</ta>
            <ta e="T256" id="Seg_3998" s="T255">0.giv-active-Q</ta>
            <ta e="T258" id="Seg_3999" s="T257">giv-inactive</ta>
            <ta e="T262" id="Seg_4000" s="T261">giv-inactive</ta>
            <ta e="T263" id="Seg_4001" s="T262">quot-sp</ta>
            <ta e="T265" id="Seg_4002" s="T264">accs-gen-Q</ta>
            <ta e="T266" id="Seg_4003" s="T265">new-Q</ta>
            <ta e="T268" id="Seg_4004" s="T267">0.giv-active-Q</ta>
            <ta e="T269" id="Seg_4005" s="T268">accs-inf-Q</ta>
            <ta e="T274" id="Seg_4006" s="T273">0.giv-active-Q</ta>
            <ta e="T277" id="Seg_4007" s="T276">0.giv-active-Q</ta>
         </annotation>
         <annotation name="Top" tierref="Top">
            <ta e="T2" id="Seg_4008" s="T1">top.int.concr</ta>
            <ta e="T11" id="Seg_4009" s="T10">top.int.concr</ta>
            <ta e="T30" id="Seg_4010" s="T28">top.int.concr</ta>
            <ta e="T34" id="Seg_4011" s="T33">top.int.concr</ta>
            <ta e="T38" id="Seg_4012" s="T37">top.int.concr</ta>
            <ta e="T45" id="Seg_4013" s="T44">top.int.concr</ta>
            <ta e="T49" id="Seg_4014" s="T48">0.top.int.concr</ta>
            <ta e="T53" id="Seg_4015" s="T52">0.top.int.concr</ta>
            <ta e="T55" id="Seg_4016" s="T54">top.int.concr</ta>
            <ta e="T59" id="Seg_4017" s="T58">0.top.int.concr</ta>
            <ta e="T60" id="Seg_4018" s="T59">0.top.int.concr</ta>
            <ta e="T61" id="Seg_4019" s="T60">top.int.concr</ta>
            <ta e="T64" id="Seg_4020" s="T63">top.int.concr</ta>
            <ta e="T81" id="Seg_4021" s="T80">0.top.int.concr</ta>
            <ta e="T82" id="Seg_4022" s="T81">top.int.concr</ta>
            <ta e="T86" id="Seg_4023" s="T85">top.int.concr</ta>
            <ta e="T90" id="Seg_4024" s="T89">top.int.concr</ta>
            <ta e="T99" id="Seg_4025" s="T98">0.top.int.concr</ta>
            <ta e="T107" id="Seg_4026" s="T106">0.top.int.abstr.</ta>
            <ta e="T111" id="Seg_4027" s="T108">top.int.concr</ta>
            <ta e="T117" id="Seg_4028" s="T113">top.int.concr</ta>
            <ta e="T121" id="Seg_4029" s="T120">0.top.int.concr</ta>
            <ta e="T123" id="Seg_4030" s="T122">top.int.concr</ta>
            <ta e="T128" id="Seg_4031" s="T127">top.int.concr</ta>
            <ta e="T136" id="Seg_4032" s="T135">0.top.int.concr</ta>
            <ta e="T140" id="Seg_4033" s="T139">0.top.int.concr</ta>
            <ta e="T142" id="Seg_4034" s="T141">top.int.concr</ta>
            <ta e="T149" id="Seg_4035" s="T146">top.int.concr</ta>
            <ta e="T153" id="Seg_4036" s="T150">top.int.concr</ta>
            <ta e="T158" id="Seg_4037" s="T155">top.int.concr</ta>
            <ta e="T170" id="Seg_4038" s="T169">top.int.concr</ta>
            <ta e="T178" id="Seg_4039" s="T172">top.int.concr</ta>
            <ta e="T187" id="Seg_4040" s="T184">top.int.concr</ta>
            <ta e="T195" id="Seg_4041" s="T194">top.int.concr</ta>
            <ta e="T202" id="Seg_4042" s="T199">top.int.concr</ta>
            <ta e="T207" id="Seg_4043" s="T206">top.int.concr</ta>
            <ta e="T210" id="Seg_4044" s="T209">0.top.int.concr</ta>
            <ta e="T215" id="Seg_4045" s="T213">0.top.int.concr</ta>
            <ta e="T218" id="Seg_4046" s="T215">top.int.concr</ta>
            <ta e="T223" id="Seg_4047" s="T220">top.int.concr</ta>
            <ta e="T232" id="Seg_4048" s="T230">0.top.int.concr</ta>
            <ta e="T235" id="Seg_4049" s="T232">top.int.concr</ta>
            <ta e="T242" id="Seg_4050" s="T238">top.int.concr</ta>
            <ta e="T248" id="Seg_4051" s="T247">0.top.int.concr</ta>
            <ta e="T251" id="Seg_4052" s="T250">0.top.int.concr</ta>
            <ta e="T256" id="Seg_4053" s="T255">0.top.int.concr</ta>
            <ta e="T260" id="Seg_4054" s="T259">0.top.int.concr</ta>
            <ta e="T261" id="Seg_4055" s="T260">top.int.concr</ta>
            <ta e="T268" id="Seg_4056" s="T267">0.top.int.concr</ta>
            <ta e="T273" id="Seg_4057" s="T272">top.int.concr</ta>
            <ta e="T277" id="Seg_4058" s="T276">0.top.int.concr</ta>
         </annotation>
         <annotation name="Foc" tierref="Foc">
            <ta e="T4" id="Seg_4059" s="T0">foc.wid</ta>
            <ta e="T12" id="Seg_4060" s="T11">foc.int</ta>
            <ta e="T17" id="Seg_4061" s="T12">foc.wid</ta>
            <ta e="T27" id="Seg_4062" s="T17">foc.wid</ta>
            <ta e="T32" id="Seg_4063" s="T28">foc.wid</ta>
            <ta e="T34" id="Seg_4064" s="T32">foc.nar</ta>
            <ta e="T41" id="Seg_4065" s="T38">foc.int</ta>
            <ta e="T46" id="Seg_4066" s="T45">foc.int</ta>
            <ta e="T49" id="Seg_4067" s="T48">foc.nar</ta>
            <ta e="T53" id="Seg_4068" s="T52">foc.ver</ta>
            <ta e="T56" id="Seg_4069" s="T55">foc.int</ta>
            <ta e="T59" id="Seg_4070" s="T57">foc.int</ta>
            <ta e="T60" id="Seg_4071" s="T59">foc.nar</ta>
            <ta e="T62" id="Seg_4072" s="T61">foc.int</ta>
            <ta e="T70" id="Seg_4073" s="T67">foc.int</ta>
            <ta e="T78" id="Seg_4074" s="T76">foc.nar</ta>
            <ta e="T81" id="Seg_4075" s="T79">foc.int</ta>
            <ta e="T84" id="Seg_4076" s="T82">foc.contr</ta>
            <ta e="T88" id="Seg_4077" s="T86">foc.contr</ta>
            <ta e="T93" id="Seg_4078" s="T90">foc.int</ta>
            <ta e="T99" id="Seg_4079" s="T98">foc.int</ta>
            <ta e="T101" id="Seg_4080" s="T100">foc.nar</ta>
            <ta e="T105" id="Seg_4081" s="T102">foc.nar</ta>
            <ta e="T113" id="Seg_4082" s="T111">foc.int</ta>
            <ta e="T119" id="Seg_4083" s="T117">foc.int</ta>
            <ta e="T121" id="Seg_4084" s="T120">foc.int</ta>
            <ta e="T126" id="Seg_4085" s="T123">foc.int</ta>
            <ta e="T131" id="Seg_4086" s="T128">foc.int</ta>
            <ta e="T136" id="Seg_4087" s="T133">foc.int</ta>
            <ta e="T140" id="Seg_4088" s="T136">foc.int</ta>
            <ta e="T146" id="Seg_4089" s="T142">foc.int</ta>
            <ta e="T150" id="Seg_4090" s="T149">foc.int</ta>
            <ta e="T155" id="Seg_4091" s="T153">foc.int</ta>
            <ta e="T161" id="Seg_4092" s="T158">foc.wid</ta>
            <ta e="T164" id="Seg_4093" s="T161">foc.int</ta>
            <ta e="T168" id="Seg_4094" s="T165">foc.int</ta>
            <ta e="T172" id="Seg_4095" s="T170">foc.int</ta>
            <ta e="T182" id="Seg_4096" s="T178">foc.wid</ta>
            <ta e="T193" id="Seg_4097" s="T187">foc.wid</ta>
            <ta e="T199" id="Seg_4098" s="T195">foc.int</ta>
            <ta e="T205" id="Seg_4099" s="T202">foc.wid</ta>
            <ta e="T209" id="Seg_4100" s="T208">foc.nar</ta>
            <ta e="T210" id="Seg_4101" s="T209">foc.int</ta>
            <ta e="T215" id="Seg_4102" s="T212">foc.int</ta>
            <ta e="T220" id="Seg_4103" s="T218">foc.int</ta>
            <ta e="T225" id="Seg_4104" s="T223">foc.int</ta>
            <ta e="T230" id="Seg_4105" s="T227">foc.nar</ta>
            <ta e="T237" id="Seg_4106" s="T235">foc.int</ta>
            <ta e="T243" id="Seg_4107" s="T242">foc.nar</ta>
            <ta e="T248" id="Seg_4108" s="T245">foc.int</ta>
            <ta e="T251" id="Seg_4109" s="T248">foc.int</ta>
            <ta e="T255" id="Seg_4110" s="T251">foc.nar</ta>
            <ta e="T259" id="Seg_4111" s="T256">foc.nar</ta>
            <ta e="T263" id="Seg_4112" s="T261">foc.wid</ta>
            <ta e="T268" id="Seg_4113" s="T264">foc.int</ta>
            <ta e="T270" id="Seg_4114" s="T268">foc.nar</ta>
            <ta e="T274" id="Seg_4115" s="T273">foc.int</ta>
            <ta e="T277" id="Seg_4116" s="T274">foc.int</ta>
         </annotation>
         <annotation name="BOR" tierref="TIE0" />
         <annotation name="BOR-Phon" tierref="TIE2" />
         <annotation name="BOR-Morph" tierref="TIE3" />
         <annotation name="CS" tierref="TIE4" />
         <annotation name="fe" tierref="fe">
            <ta e="T9" id="Seg_4117" s="T0">One fox got to an island and by swimming in any direction it could not get out.</ta>
            <ta e="T12" id="Seg_4118" s="T9">The fox cried: </ta>
            <ta e="T27" id="Seg_4119" s="T12">"It's annoying that I got to that island, if I weren't caught on this island, I would eat the bowstrings of the Dolgans' self-shooting bows."</ta>
            <ta e="T35" id="Seg_4120" s="T27">In the water some voices were heard, the eelpouts spoke: </ta>
            <ta e="T41" id="Seg_4121" s="T35">"Oh, children, is it a fox crying?"</ta>
            <ta e="T46" id="Seg_4122" s="T41">The fox heard this and said: </ta>
            <ta e="T49" id="Seg_4123" s="T46">"Eelpounts, let us run a race!"</ta>
            <ta e="T54" id="Seg_4124" s="T49">The eelpouts also: "Well, let's run a race!", they said.</ta>
            <ta e="T56" id="Seg_4125" s="T54">The fox said: </ta>
            <ta e="T59" id="Seg_4126" s="T56">"Hey, come up close to the shore!</ta>
            <ta e="T63" id="Seg_4127" s="T59">How many are you? I will count", it said.</ta>
            <ta e="T70" id="Seg_4128" s="T63">As they came up to the shore, the fox jumped on their backs.</ta>
            <ta e="T79" id="Seg_4129" s="T70">The fox counted: "One, one, ten!", that was his calculation.</ta>
            <ta e="T81" id="Seg_4130" s="T79">They went on.</ta>
            <ta e="T88" id="Seg_4131" s="T81">The fox along the shore, the fishes in the water.</ta>
            <ta e="T95" id="Seg_4132" s="T88">The fox began to run: "Eelpouts!", it said.</ta>
            <ta e="T99" id="Seg_4133" s="T95">In front of it they said "oops" and overtook [it].</ta>
            <ta e="T108" id="Seg_4134" s="T99">"There is no water [to drink] and the fishes are very agile", it said.</ta>
            <ta e="T113" id="Seg_4135" s="T108">After this it ran on again.</ta>
            <ta e="T126" id="Seg_4136" s="T113">On its way it drank water: "I will drink this water", it said, choked and died.</ta>
            <ta e="T140" id="Seg_4137" s="T126">One old woman came to scoope some water. She came into the water to scoop and found the dead fox lying there. </ta>
            <ta e="T150" id="Seg_4138" s="T140">This old woman carried the fox, skined it and then dried the fur.</ta>
            <ta e="T155" id="Seg_4139" s="T150">After this she hung it up on the storage. </ta>
            <ta e="T161" id="Seg_4140" s="T155">The old woman hung it up and said: </ta>
            <ta e="T165" id="Seg_4141" s="T161">"Get up, south wind!", she said. </ta>
            <ta e="T169" id="Seg_4142" s="T165">"Get up, west wind!", she said.</ta>
            <ta e="T172" id="Seg_4143" s="T169">The wind began to blow.</ta>
            <ta e="T184" id="Seg_4144" s="T172">The fox fur started to swing in the wind, the old woman danced with it saying "huuranki-haaranki".</ta>
            <ta e="T193" id="Seg_4145" s="T184">As she was dancing so, the wind got so strong that it threw the fur down and carried it away.</ta>
            <ta e="T199" id="Seg_4146" s="T193">The old woman followed the fur.</ta>
            <ta e="T205" id="Seg_4147" s="T199">While following it the old woman said: </ta>
            <ta e="T215" id="Seg_4148" s="T205">"My breast seems to pull down", saying this she cut off one breast.</ta>
            <ta e="T220" id="Seg_4149" s="T215">After that she went on.</ta>
            <ta e="T232" id="Seg_4150" s="T220">"The other breast also seems to be heavy", she said and cut off this breast too.</ta>
            <ta e="T237" id="Seg_4151" s="T232">After that she went on.</ta>
            <ta e="T248" id="Seg_4152" s="T237">"Well, is my hand also heavy?", she said and cut off one hand.</ta>
            <ta e="T251" id="Seg_4153" s="T248">She went on again.</ta>
            <ta e="T256" id="Seg_4154" s="T251">The other one she cut off either.</ta>
            <ta e="T260" id="Seg_4155" s="T256">She was without hands.</ta>
            <ta e="T263" id="Seg_4156" s="T260">After that the old woman said: </ta>
            <ta e="T272" id="Seg_4157" s="T263">"To kiss the Son of the Sky just my lips should be intact!"</ta>
            <ta e="T274" id="Seg_4158" s="T272">She went on again.</ta>
            <ta e="T277" id="Seg_4159" s="T274">Her strength was gone and she died.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T9" id="Seg_4160" s="T0">Ein Fuchs geriet auf eine Insel, schwimmend konnte er nicht von der Insel wegkommen.</ta>
            <ta e="T12" id="Seg_4161" s="T9">Da weinte der Fuchs: </ta>
            <ta e="T27" id="Seg_4162" s="T12">"Was für ein Ärger, dass ich auf diese Insel geraten bin, wenn ich nicht auf dieser Insel festsäße, dann würde ich die Sehnen der Selbstschussbögen der Dolganen fressen."</ta>
            <ta e="T35" id="Seg_4163" s="T27">Da war im Wasser eine Stimme zu hören, keine anderen als die Aalquappen sprachen: </ta>
            <ta e="T41" id="Seg_4164" s="T35">"Ach, Kinder, weint der Fuchs etwa?"</ta>
            <ta e="T46" id="Seg_4165" s="T41">Der Fuchs hörte das und sagte: </ta>
            <ta e="T49" id="Seg_4166" s="T46">"Aalquappen", sagt er, "lasst uns um die Wette laufen!"</ta>
            <ta e="T54" id="Seg_4167" s="T49">Die Aalquappen darauf: "Ja, laufen wir um die Wette!", sagten sie.</ta>
            <ta e="T56" id="Seg_4168" s="T54">Der Fuchs sagte: </ta>
            <ta e="T59" id="Seg_4169" s="T56">"Hey, kommt ans Ufer heran!</ta>
            <ta e="T63" id="Seg_4170" s="T59">Wie viele seid ihr, ich zähle", sagte er.</ta>
            <ta e="T70" id="Seg_4171" s="T63">Als sie am Ufer angekommen waren, sprang der Fuchs auf ihre Rücken.</ta>
            <ta e="T79" id="Seg_4172" s="T70">Der Fuchs zählt: "Eins, eins, zehn!", sagte er seine Rechnung.</ta>
            <ta e="T81" id="Seg_4173" s="T79">Und sie laufen.</ta>
            <ta e="T88" id="Seg_4174" s="T81">Der Fuchs lief das Ufer entlang, die Fische schwammen durchs Wasser.</ta>
            <ta e="T95" id="Seg_4175" s="T88">Der Fuchs fängt an zu rennen: "Aalquappen!", sagte er.</ta>
            <ta e="T99" id="Seg_4176" s="T95">Vor ihm sagten sie "ups" und überholten [ihn].</ta>
            <ta e="T108" id="Seg_4177" s="T99">"Es gibt kein Wasser und die Fische sind ganz schön flink, zeigt sich", sagte er.</ta>
            <ta e="T113" id="Seg_4178" s="T108">Danach rannte er wieder.</ta>
            <ta e="T126" id="Seg_4179" s="T113">An einem Ort während des Laufes trank er Wasser, "ich trinke" sagend erstickte der Fuchs.</ta>
            <ta e="T140" id="Seg_4180" s="T126">Eine alte Frau ging Wasser schöpfen, sie ging Wasser schöpfen und fand den tot daliegenden Fuchs.</ta>
            <ta e="T150" id="Seg_4181" s="T140">Diese alte Frau trug den Fuchs, zog ihm das Fell ab, danach trocknete das Fell.</ta>
            <ta e="T155" id="Seg_4182" s="T150">Danach hängte sie es am Speicher auf.</ta>
            <ta e="T161" id="Seg_4183" s="T155">Sie hängte es auf und da sagte die Alte: </ta>
            <ta e="T165" id="Seg_4184" s="T161">"Erhebe dich von Süden, Wind!", sagte sie.</ta>
            <ta e="T169" id="Seg_4185" s="T165">"Erhebe dich von Westen, Wind!", sagte sie.</ta>
            <ta e="T172" id="Seg_4186" s="T169">Da kam dieser ihr Wind auf.</ta>
            <ta e="T184" id="Seg_4187" s="T172">Als das Fell im Wind herumgeworfen wurde, tanzte die alte Frau mit ihm und sagte "huuranki-haaranki".</ta>
            <ta e="T193" id="Seg_4188" s="T184">Als sie so tanzte, riss der Wind das Fuchsfell ab und trug es davon.</ta>
            <ta e="T199" id="Seg_4189" s="T193">Die alte Frau verfolgte das Fuchsfell.</ta>
            <ta e="T205" id="Seg_4190" s="T199">Die alte Frau verfolgte es und sagte: </ta>
            <ta e="T215" id="Seg_4191" s="T205">"Mein Busen ist wohl schwer, er zieht mich offenbar herunter", sagte sie und schnitt sich eine Brust ab.</ta>
            <ta e="T220" id="Seg_4192" s="T215">Danach ging sie wieder [los].</ta>
            <ta e="T232" id="Seg_4193" s="T220">"Meine andere Brust ist wohl auch schwer", sagte sie und schnitt auch diese Brust ab.</ta>
            <ta e="T237" id="Seg_4194" s="T232">Danach ging sie wieder [los].</ta>
            <ta e="T248" id="Seg_4195" s="T237">"Nun, ist auch meine eine Hand schwer?", sagte sie und schnitt eine Hand ab.</ta>
            <ta e="T251" id="Seg_4196" s="T248">Und sie ging wieder.</ta>
            <ta e="T256" id="Seg_4197" s="T251">Sie schnitt sich auch die andere Hand ab.</ta>
            <ta e="T260" id="Seg_4198" s="T256">Sie hatte keine Hände mehr.</ta>
            <ta e="T263" id="Seg_4199" s="T260">Danach sagte die alte Frau: </ta>
            <ta e="T272" id="Seg_4200" s="T263">"Um mit dem Sohn des Himmels zu küssen, müssen nur meine Lippen ganz sein!"</ta>
            <ta e="T274" id="Seg_4201" s="T272">Dann ging sie weiter.</ta>
            <ta e="T277" id="Seg_4202" s="T274">Sie war am Ende und starb.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T9" id="Seg_4203" s="T0">Одна лиса попала на остров и вплавь выбраться не может.</ta>
            <ta e="T12" id="Seg_4204" s="T9">Вот лисица запричитала: </ta>
            <ta e="T27" id="Seg_4205" s="T12">"Какая досада, что я попала на этот остров, если б не попала сюда, перегрызла бы тетивы самострелов у долган!"</ta>
            <ta e="T35" id="Seg_4206" s="T27">Тут в воде послышались голоса, оказывается, самые налимы говорили: </ta>
            <ta e="T41" id="Seg_4207" s="T35">"А-а, ребята, неужели лиса может плакать?"</ta>
            <ta e="T46" id="Seg_4208" s="T41">Услышав это, лиса сказала: </ta>
            <ta e="T49" id="Seg_4209" s="T46">"Налимы, давайте побежим наперегонки!"</ta>
            <ta e="T54" id="Seg_4210" s="T49">Налимы на это: "Давай побежим", сказали.</ta>
            <ta e="T56" id="Seg_4211" s="T54">Лиса говорит: </ta>
            <ta e="T59" id="Seg_4212" s="T56">"А ну, приставайте к берегу!</ta>
            <ta e="T63" id="Seg_4213" s="T59">Сколько вас, я сосчитаю", сказала.</ta>
            <ta e="T70" id="Seg_4214" s="T63">Когда [те] пристали к берегу, лиса стала прыгать по ним.</ta>
            <ta e="T79" id="Seg_4215" s="T70">Лиса считает: "Один, один, десять!", такие слова счета.</ta>
            <ta e="T81" id="Seg_4216" s="T79">Вот начали [бег].</ta>
            <ta e="T88" id="Seg_4217" s="T81">Лиса берегом пошла, а рыбы по воде.</ta>
            <ta e="T95" id="Seg_4218" s="T88">Лиса мчится и зовет: ‎‎"Налимы!", сказала.</ta>
            <ta e="T99" id="Seg_4219" s="T95">Впереди: "Оп!", кричат, опередили.</ta>
            <ta e="T108" id="Seg_4220" s="T99">"Воды нет, а рыбы-то резвые какие, оказывается", сказала [лиса].</ta>
            <ta e="T113" id="Seg_4221" s="T108">Затем опять помчалась.</ta>
            <ta e="T126" id="Seg_4222" s="T113">На бегу в одном месте напился, "я напьюсь" говоря он задыхался.</ta>
            <ta e="T140" id="Seg_4223" s="T126">Одна старуха пошла набрать воды. Когда шла по воду, вдруг нашла мертвую лису.</ta>
            <ta e="T150" id="Seg_4224" s="T140">Та старуха принесла лису, ободрала и снятую [шкуру] стала сушить.</ta>
            <ta e="T155" id="Seg_4225" s="T150">Повесила на лабаз.</ta>
            <ta e="T161" id="Seg_4226" s="T155">Так развешивая, говорила: </ta>
            <ta e="T165" id="Seg_4227" s="T161">"С юга поднимись, ветер!", сказала.</ta>
            <ta e="T169" id="Seg_4228" s="T165">"С запада поднимись, ветер!", сказала.</ta>
            <ta e="T172" id="Seg_4229" s="T169">Вот задули [ветры].</ta>
            <ta e="T184" id="Seg_4230" s="T172">Когда забилась на ветру лисья шкура, старуха заплясала вместе с ней, приговаривая: "Суурангки-саарангки!", говоря-говоря.</ta>
            <ta e="T193" id="Seg_4231" s="T184">Когда так плясала, ветер сорвал лисью шкуру и унес.</ta>
            <ta e="T199" id="Seg_4232" s="T193">Старуха пустилась вслед за лисой.</ta>
            <ta e="T205" id="Seg_4233" s="T199">Бежит за ней и говорит: </ta>
            <ta e="T215" id="Seg_4234" s="T205">"Грудь моя, видать, тяжелая, перевешивает, видать",так говоря, отрезала одну грудь.</ta>
            <ta e="T220" id="Seg_4235" s="T215">После этого опять [бежать] пустилась.</ta>
            <ta e="T232" id="Seg_4236" s="T220">"И другая моя грудь тоже, видать, тяжелая", так сказав, другую грудь тоже отрезала.</ta>
            <ta e="T237" id="Seg_4237" s="T232">Снова побежала.</ta>
            <ta e="T248" id="Seg_4238" s="T237">"Ну, это моя рука тяжелая, что ли?", сказав, одну руку отрезала.</ta>
            <ta e="T251" id="Seg_4239" s="T248">Опять пошла.</ta>
            <ta e="T256" id="Seg_4240" s="T251">Другую руку тоже отрезала.</ta>
            <ta e="T260" id="Seg_4241" s="T256">Осталась без рук.</ta>
            <ta e="T263" id="Seg_4242" s="T260">После этого старуха сказала: </ta>
            <ta e="T272" id="Seg_4243" s="T263">"Чтоб с божьим сыном целоваться лишь бы губы целыми остались!"</ta>
            <ta e="T274" id="Seg_4244" s="T272">Дальше побежала.</ta>
            <ta e="T277" id="Seg_4245" s="T274">Иссякли ее силы, померла.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T9" id="Seg_4246" s="T0">Одна лиса попала на остров и вплавь выбраться не может.</ta>
            <ta e="T12" id="Seg_4247" s="T9">Вот лисица запричитала: </ta>
            <ta e="T27" id="Seg_4248" s="T12">"Какая досада, что не могу выбраться с этого острова, если б смогла выбраться отсюда, перегрызла бы тетивы самострелов у долган!"</ta>
            <ta e="T35" id="Seg_4249" s="T27">Тут в воде послышались голоса, оказывается, налимы говорили. </ta>
            <ta e="T41" id="Seg_4250" s="T35">— А-а, ребята, неужели лиса может плакать?</ta>
            <ta e="T46" id="Seg_4251" s="T41">Услышав это, лиса сказала: </ta>
            <ta e="T49" id="Seg_4252" s="T46">— Налимы, давайте побежим наперегонки!</ta>
            <ta e="T54" id="Seg_4253" s="T49">Налимы на это: — Давай побежим, — сказали.</ta>
            <ta e="T56" id="Seg_4254" s="T54">Лиса говорит: </ta>
            <ta e="T59" id="Seg_4255" s="T56">— А ну, приставайте к берегу!</ta>
            <ta e="T63" id="Seg_4256" s="T59">Сколько вас, я сосчитаю.</ta>
            <ta e="T70" id="Seg_4257" s="T63">Когда [те] пристали к берегу, лиса стала прыгать по ним.</ta>
            <ta e="T79" id="Seg_4258" s="T70">Лиса считает: "Один, один, десять!" — такие слова счета.</ta>
            <ta e="T81" id="Seg_4259" s="T79">Вот начали [бег].</ta>
            <ta e="T88" id="Seg_4260" s="T81">Лиса берегом пошла, а рыбы по воде.</ta>
            <ta e="T95" id="Seg_4261" s="T88">Лиса мчится и зовет: ‎‎— Налимы! — сказала.</ta>
            <ta e="T99" id="Seg_4262" s="T95">Впереди: — Оп! — кричат, опередили.</ta>
            <ta e="T108" id="Seg_4263" s="T99">— Рыбы-то резвые какие, оказывается, — сказала [лиса].</ta>
            <ta e="T113" id="Seg_4264" s="T108">Затем опять помчалась.</ta>
            <ta e="T126" id="Seg_4265" s="T113">На бегу в одном месте напиться захотела, стала пить и, захлебнувшись, померла.</ta>
            <ta e="T140" id="Seg_4266" s="T126">Одна старуха пошла набрать воды. Когда шла по воду, вдруг нашла мертвую лису.</ta>
            <ta e="T150" id="Seg_4267" s="T140">Та старуха принесла лису, ободрала и снятую [шкуру] стала сушить.</ta>
            <ta e="T155" id="Seg_4268" s="T150">Повесила на лабаз.</ta>
            <ta e="T161" id="Seg_4269" s="T155">Так развешивая, говорила: </ta>
            <ta e="T165" id="Seg_4270" s="T161">— С юга поднимись, ветер! — сказала.</ta>
            <ta e="T169" id="Seg_4271" s="T165">— С запада подуй ветер? — сказала.</ta>
            <ta e="T172" id="Seg_4272" s="T169">Вот задули [ветры].</ta>
            <ta e="T184" id="Seg_4273" s="T172">Когда забилась на ветру лисья шкура, старуха заплясала вместе с ней, приговаривая: — Суурангки-саарангки! — говоря-говоря.</ta>
            <ta e="T193" id="Seg_4274" s="T184">Когда так плясала, ветер сорвал лисью шкуру и унес.</ta>
            <ta e="T199" id="Seg_4275" s="T193">Старуха пустилась вслед за лисой.</ta>
            <ta e="T205" id="Seg_4276" s="T199">Бежит за ней и говорит: </ta>
            <ta e="T215" id="Seg_4277" s="T205">"Грудь моя, видать, тяжелая, перевешивает, видать".Так говоря, отрезала одну грудь.</ta>
            <ta e="T220" id="Seg_4278" s="T215">После этого опять [бежать] пустилась.</ta>
            <ta e="T232" id="Seg_4279" s="T220">"И другая моя грудь тоже, видать, тяжелая", — так сказав, другую грудь тоже отрезала.</ta>
            <ta e="T237" id="Seg_4280" s="T232">Снова побежала.</ta>
            <ta e="T248" id="Seg_4281" s="T237">"Ну, это моя рука тяжелая, что ли?" — сказав, одну руку отрезала.</ta>
            <ta e="T251" id="Seg_4282" s="T248">Опять бежит.</ta>
            <ta e="T256" id="Seg_4283" s="T251">Другую руку тоже отрезала.</ta>
            <ta e="T260" id="Seg_4284" s="T256">Осталась без рук.</ta>
            <ta e="T263" id="Seg_4285" s="T260">После этого старуха сказала: </ta>
            <ta e="T272" id="Seg_4286" s="T263">"Чтоб с божьим сыном целоваться лишь бы губы целыми остались!"</ta>
            <ta e="T274" id="Seg_4287" s="T272">Дальше побежала.</ta>
            <ta e="T277" id="Seg_4288" s="T274">Иссякли ее силы, померла.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T79" id="Seg_4289" s="T70">[DCh]: "Ugun, ugun, dʼanus" is Evenki and means 'One, one, ten!'.</ta>
            <ta e="T184" id="Seg_4290" s="T172">[DCh]: "möktögögüne" seems to be some kind of mistake, the correct form "möktögüne" would perfectly fit here</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg"
                          name="gg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top"
                          name="Top"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc"
                          name="Foc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
