<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID8089A4E2-E533-DFB9-6167-DD24A5CB06FE">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>PoMA_1964_IronCapBonyBelt_flk</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\flk\PoMA_1964_IronCapBonyBelt_flk\PoMA_1964_IronCapBonyBelt_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">1124</ud-information>
            <ud-information attribute-name="# HIAT:w">862</ud-information>
            <ud-information attribute-name="# e">826</ud-information>
            <ud-information attribute-name="# HIAT:u">122</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PoMA">
            <abbreviation>PoMA</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="4.0" type="appl" />
         <tli id="T1" time="4.5" type="appl" />
         <tli id="T2" time="5.0" type="appl" />
         <tli id="T3" time="5.5" type="appl" />
         <tli id="T4" time="6.0" type="appl" />
         <tli id="T5" time="6.5" type="appl" />
         <tli id="T6" time="7.0" type="appl" />
         <tli id="T7" time="7.5" type="appl" />
         <tli id="T8" time="8.0" type="appl" />
         <tli id="T9" time="8.5" type="appl" />
         <tli id="T10" time="9.0" type="appl" />
         <tli id="T11" time="9.5" type="appl" />
         <tli id="T12" time="10.0" type="appl" />
         <tli id="T13" time="10.5" type="appl" />
         <tli id="T14" time="11.0" type="appl" />
         <tli id="T15" time="11.5" type="appl" />
         <tli id="T16" time="12.0" type="appl" />
         <tli id="T17" time="12.5" type="appl" />
         <tli id="T18" time="13.0" type="appl" />
         <tli id="T19" time="13.5" type="appl" />
         <tli id="T20" time="14.0" type="appl" />
         <tli id="T21" time="14.5" type="appl" />
         <tli id="T22" time="15.0" type="appl" />
         <tli id="T23" time="15.5" type="appl" />
         <tli id="T24" time="16.0" type="appl" />
         <tli id="T25" time="16.5" type="appl" />
         <tli id="T26" time="17.0" type="appl" />
         <tli id="T27" time="17.5" type="appl" />
         <tli id="T28" time="18.0" type="appl" />
         <tli id="T29" time="18.5" type="appl" />
         <tli id="T30" time="19.0" type="appl" />
         <tli id="T31" time="19.5" type="appl" />
         <tli id="T32" time="20.0" type="appl" />
         <tli id="T33" time="20.5" type="appl" />
         <tli id="T34" time="21.0" type="appl" />
         <tli id="T35" time="21.5" type="appl" />
         <tli id="T36" time="22.0" type="appl" />
         <tli id="T37" time="22.5" type="appl" />
         <tli id="T38" time="23.0" type="appl" />
         <tli id="T39" time="23.5" type="appl" />
         <tli id="T40" time="24.0" type="appl" />
         <tli id="T41" time="24.5" type="appl" />
         <tli id="T42" time="25.0" type="appl" />
         <tli id="T43" time="25.5" type="appl" />
         <tli id="T44" time="26.0" type="appl" />
         <tli id="T45" time="26.5" type="appl" />
         <tli id="T46" time="27.0" type="appl" />
         <tli id="T47" time="27.5" type="appl" />
         <tli id="T48" time="28.0" type="appl" />
         <tli id="T49" time="28.5" type="appl" />
         <tli id="T50" time="29.0" type="appl" />
         <tli id="T51" time="29.5" type="appl" />
         <tli id="T52" time="30.0" type="appl" />
         <tli id="T53" time="30.5" type="appl" />
         <tli id="T54" time="31.0" type="appl" />
         <tli id="T55" time="31.5" type="appl" />
         <tli id="T56" time="32.0" type="appl" />
         <tli id="T57" time="32.5" type="appl" />
         <tli id="T58" time="33.0" type="appl" />
         <tli id="T59" time="33.5" type="appl" />
         <tli id="T60" time="34.0" type="appl" />
         <tli id="T61" time="34.5" type="appl" />
         <tli id="T62" time="35.0" type="appl" />
         <tli id="T63" time="35.5" type="appl" />
         <tli id="T64" time="36.0" type="appl" />
         <tli id="T65" time="36.5" type="appl" />
         <tli id="T66" time="37.0" type="appl" />
         <tli id="T67" time="37.5" type="appl" />
         <tli id="T68" time="38.0" type="appl" />
         <tli id="T69" time="38.5" type="appl" />
         <tli id="T70" time="39.0" type="appl" />
         <tli id="T71" time="39.5" type="appl" />
         <tli id="T72" time="40.0" type="appl" />
         <tli id="T73" time="40.5" type="appl" />
         <tli id="T74" time="41.0" type="appl" />
         <tli id="T75" time="41.5" type="appl" />
         <tli id="T76" time="42.0" type="appl" />
         <tli id="T77" time="42.5" type="appl" />
         <tli id="T78" time="43.0" type="appl" />
         <tli id="T79" time="43.5" type="appl" />
         <tli id="T80" time="44.0" type="appl" />
         <tli id="T81" time="44.5" type="appl" />
         <tli id="T82" time="45.0" type="appl" />
         <tli id="T83" time="45.5" type="appl" />
         <tli id="T84" time="46.0" type="appl" />
         <tli id="T85" time="46.5" type="appl" />
         <tli id="T86" time="47.0" type="appl" />
         <tli id="T87" time="47.5" type="appl" />
         <tli id="T88" time="48.0" type="appl" />
         <tli id="T89" time="48.5" type="appl" />
         <tli id="T90" time="49.0" type="appl" />
         <tli id="T91" time="49.5" type="appl" />
         <tli id="T92" time="50.0" type="appl" />
         <tli id="T93" time="50.5" type="appl" />
         <tli id="T94" time="51.0" type="appl" />
         <tli id="T95" time="51.5" type="appl" />
         <tli id="T96" time="52.0" type="appl" />
         <tli id="T97" time="52.5" type="appl" />
         <tli id="T98" time="53.0" type="appl" />
         <tli id="T99" time="53.5" type="appl" />
         <tli id="T100" time="54.0" type="appl" />
         <tli id="T101" time="54.5" type="appl" />
         <tli id="T102" time="55.0" type="appl" />
         <tli id="T103" time="55.5" type="appl" />
         <tli id="T104" time="56.0" type="appl" />
         <tli id="T105" time="56.5" type="appl" />
         <tli id="T106" time="57.0" type="appl" />
         <tli id="T107" time="57.5" type="appl" />
         <tli id="T108" time="58.0" type="appl" />
         <tli id="T109" time="58.5" type="appl" />
         <tli id="T110" time="59.0" type="appl" />
         <tli id="T111" time="59.5" type="appl" />
         <tli id="T112" time="60.0" type="appl" />
         <tli id="T113" time="60.5" type="appl" />
         <tli id="T114" time="61.0" type="appl" />
         <tli id="T115" time="61.5" type="appl" />
         <tli id="T116" time="62.0" type="appl" />
         <tli id="T117" time="62.5" type="appl" />
         <tli id="T118" time="63.0" type="appl" />
         <tli id="T119" time="63.5" type="appl" />
         <tli id="T120" time="64.0" type="appl" />
         <tli id="T121" time="64.5" type="appl" />
         <tli id="T122" time="65.0" type="appl" />
         <tli id="T123" time="65.5" type="appl" />
         <tli id="T124" time="66.0" type="appl" />
         <tli id="T125" time="66.5" type="appl" />
         <tli id="T126" time="67.0" type="appl" />
         <tli id="T127" time="67.5" type="appl" />
         <tli id="T128" time="68.0" type="appl" />
         <tli id="T129" time="68.5" type="appl" />
         <tli id="T130" time="69.0" type="appl" />
         <tli id="T131" time="69.5" type="appl" />
         <tli id="T132" time="70.0" type="appl" />
         <tli id="T133" time="70.5" type="appl" />
         <tli id="T134" time="71.0" type="appl" />
         <tli id="T135" time="71.5" type="appl" />
         <tli id="T136" time="72.0" type="appl" />
         <tli id="T137" time="72.5" type="appl" />
         <tli id="T138" time="73.0" type="appl" />
         <tli id="T139" time="73.5" type="appl" />
         <tli id="T140" time="74.0" type="appl" />
         <tli id="T141" time="74.5" type="appl" />
         <tli id="T142" time="75.0" type="appl" />
         <tli id="T143" time="75.5" type="appl" />
         <tli id="T144" time="76.0" type="appl" />
         <tli id="T145" time="76.5" type="appl" />
         <tli id="T146" time="77.0" type="appl" />
         <tli id="T147" time="77.5" type="appl" />
         <tli id="T148" time="78.0" type="appl" />
         <tli id="T149" time="78.5" type="appl" />
         <tli id="T150" time="79.0" type="appl" />
         <tli id="T151" time="79.5" type="appl" />
         <tli id="T152" time="80.0" type="appl" />
         <tli id="T153" time="80.5" type="appl" />
         <tli id="T154" time="81.0" type="appl" />
         <tli id="T155" time="81.5" type="appl" />
         <tli id="T156" time="82.0" type="appl" />
         <tli id="T157" time="82.5" type="appl" />
         <tli id="T158" time="83.0" type="appl" />
         <tli id="T159" time="83.5" type="appl" />
         <tli id="T160" time="84.0" type="appl" />
         <tli id="T161" time="84.5" type="appl" />
         <tli id="T162" time="85.0" type="appl" />
         <tli id="T163" time="85.5" type="appl" />
         <tli id="T164" time="86.0" type="appl" />
         <tli id="T165" time="86.5" type="appl" />
         <tli id="T166" time="87.0" type="appl" />
         <tli id="T167" time="87.5" type="appl" />
         <tli id="T168" time="88.0" type="appl" />
         <tli id="T169" time="88.5" type="appl" />
         <tli id="T170" time="89.0" type="appl" />
         <tli id="T171" time="89.5" type="appl" />
         <tli id="T172" time="90.0" type="appl" />
         <tli id="T173" time="90.5" type="appl" />
         <tli id="T174" time="91.0" type="appl" />
         <tli id="T175" time="91.5" type="appl" />
         <tli id="T176" time="92.0" type="appl" />
         <tli id="T177" time="92.5" type="appl" />
         <tli id="T178" time="93.0" type="appl" />
         <tli id="T179" time="93.5" type="appl" />
         <tli id="T180" time="94.0" type="appl" />
         <tli id="T181" time="94.5" type="appl" />
         <tli id="T182" time="95.0" type="appl" />
         <tli id="T183" time="95.5" type="appl" />
         <tli id="T184" time="96.0" type="appl" />
         <tli id="T185" time="96.5" type="appl" />
         <tli id="T186" time="97.0" type="appl" />
         <tli id="T187" time="97.5" type="appl" />
         <tli id="T188" time="98.0" type="appl" />
         <tli id="T189" time="98.5" type="appl" />
         <tli id="T190" time="99.0" type="appl" />
         <tli id="T191" time="99.5" type="appl" />
         <tli id="T192" time="100.0" type="appl" />
         <tli id="T193" time="100.5" type="appl" />
         <tli id="T194" time="101.0" type="appl" />
         <tli id="T195" time="101.5" type="appl" />
         <tli id="T196" time="102.0" type="appl" />
         <tli id="T197" time="102.5" type="appl" />
         <tli id="T198" time="103.0" type="appl" />
         <tli id="T199" time="103.5" type="appl" />
         <tli id="T200" time="104.0" type="appl" />
         <tli id="T201" time="104.5" type="appl" />
         <tli id="T202" time="105.0" type="appl" />
         <tli id="T203" time="105.5" type="appl" />
         <tli id="T204" time="106.0" type="appl" />
         <tli id="T205" time="106.5" type="appl" />
         <tli id="T206" time="107.0" type="appl" />
         <tli id="T207" time="107.5" type="appl" />
         <tli id="T208" time="108.0" type="appl" />
         <tli id="T209" time="108.5" type="appl" />
         <tli id="T210" time="109.0" type="appl" />
         <tli id="T211" time="109.5" type="appl" />
         <tli id="T212" time="110.0" type="appl" />
         <tli id="T213" time="110.5" type="appl" />
         <tli id="T214" time="111.0" type="appl" />
         <tli id="T215" time="111.5" type="appl" />
         <tli id="T216" time="112.0" type="appl" />
         <tli id="T217" time="112.5" type="appl" />
         <tli id="T218" time="113.0" type="appl" />
         <tli id="T219" time="113.5" type="appl" />
         <tli id="T220" time="114.0" type="appl" />
         <tli id="T221" time="114.5" type="appl" />
         <tli id="T222" time="115.0" type="appl" />
         <tli id="T223" time="115.5" type="appl" />
         <tli id="T224" time="116.0" type="appl" />
         <tli id="T225" time="116.5" type="appl" />
         <tli id="T226" time="117.0" type="appl" />
         <tli id="T227" time="117.5" type="appl" />
         <tli id="T228" time="118.0" type="appl" />
         <tli id="T229" time="118.5" type="appl" />
         <tli id="T230" time="119.0" type="appl" />
         <tli id="T231" time="119.5" type="appl" />
         <tli id="T232" time="120.0" type="appl" />
         <tli id="T233" time="120.5" type="appl" />
         <tli id="T234" time="121.0" type="appl" />
         <tli id="T235" time="121.5" type="appl" />
         <tli id="T236" time="122.0" type="appl" />
         <tli id="T237" time="122.5" type="appl" />
         <tli id="T238" time="123.0" type="appl" />
         <tli id="T239" time="123.5" type="appl" />
         <tli id="T240" time="124.0" type="appl" />
         <tli id="T241" time="124.5" type="appl" />
         <tli id="T242" time="125.0" type="appl" />
         <tli id="T243" time="125.5" type="appl" />
         <tli id="T244" time="126.0" type="appl" />
         <tli id="T245" time="126.5" type="appl" />
         <tli id="T246" time="127.0" type="appl" />
         <tli id="T247" time="127.5" type="appl" />
         <tli id="T248" time="128.0" type="appl" />
         <tli id="T249" time="128.5" type="appl" />
         <tli id="T250" time="129.0" type="appl" />
         <tli id="T251" time="129.5" type="appl" />
         <tli id="T252" time="130.0" type="appl" />
         <tli id="T253" time="130.5" type="appl" />
         <tli id="T254" time="131.0" type="appl" />
         <tli id="T255" time="131.5" type="appl" />
         <tli id="T256" time="132.0" type="appl" />
         <tli id="T257" time="132.5" type="appl" />
         <tli id="T258" time="133.0" type="appl" />
         <tli id="T259" time="133.5" type="appl" />
         <tli id="T260" time="134.0" type="appl" />
         <tli id="T261" time="134.5" type="appl" />
         <tli id="T262" time="135.0" type="appl" />
         <tli id="T263" time="135.5" type="appl" />
         <tli id="T264" time="136.0" type="appl" />
         <tli id="T265" time="136.5" type="appl" />
         <tli id="T266" time="137.0" type="appl" />
         <tli id="T267" time="137.5" type="appl" />
         <tli id="T268" time="138.0" type="appl" />
         <tli id="T269" time="138.5" type="appl" />
         <tli id="T270" time="139.0" type="appl" />
         <tli id="T271" time="139.5" type="appl" />
         <tli id="T272" time="140.0" type="appl" />
         <tli id="T273" time="140.5" type="appl" />
         <tli id="T274" time="141.0" type="appl" />
         <tli id="T275" time="141.5" type="appl" />
         <tli id="T276" time="142.0" type="appl" />
         <tli id="T277" time="142.5" type="appl" />
         <tli id="T278" time="143.0" type="appl" />
         <tli id="T279" time="143.5" type="appl" />
         <tli id="T280" time="144.0" type="appl" />
         <tli id="T281" time="144.5" type="appl" />
         <tli id="T282" time="145.0" type="appl" />
         <tli id="T283" time="145.5" type="appl" />
         <tli id="T284" time="146.0" type="appl" />
         <tli id="T285" time="146.5" type="appl" />
         <tli id="T286" time="147.0" type="appl" />
         <tli id="T287" time="147.5" type="appl" />
         <tli id="T288" time="148.0" type="appl" />
         <tli id="T289" time="148.5" type="appl" />
         <tli id="T290" time="149.0" type="appl" />
         <tli id="T291" time="149.5" type="appl" />
         <tli id="T292" time="150.0" type="appl" />
         <tli id="T293" time="150.5" type="appl" />
         <tli id="T294" time="151.0" type="appl" />
         <tli id="T295" time="151.5" type="appl" />
         <tli id="T296" time="152.0" type="appl" />
         <tli id="T297" time="152.5" type="appl" />
         <tli id="T298" time="153.0" type="appl" />
         <tli id="T299" time="153.5" type="appl" />
         <tli id="T300" time="154.0" type="appl" />
         <tli id="T301" time="154.5" type="appl" />
         <tli id="T302" time="155.0" type="appl" />
         <tli id="T303" time="155.5" type="appl" />
         <tli id="T304" time="156.0" type="appl" />
         <tli id="T305" time="156.5" type="appl" />
         <tli id="T306" time="157.0" type="appl" />
         <tli id="T307" time="157.5" type="appl" />
         <tli id="T308" time="158.0" type="appl" />
         <tli id="T309" time="158.5" type="appl" />
         <tli id="T310" time="159.0" type="appl" />
         <tli id="T311" time="159.5" type="appl" />
         <tli id="T312" time="160.0" type="appl" />
         <tli id="T313" time="160.5" type="appl" />
         <tli id="T314" time="161.0" type="appl" />
         <tli id="T315" time="161.5" type="appl" />
         <tli id="T316" time="162.0" type="appl" />
         <tli id="T317" time="162.5" type="appl" />
         <tli id="T318" time="163.0" type="appl" />
         <tli id="T319" time="163.5" type="appl" />
         <tli id="T320" time="164.0" type="appl" />
         <tli id="T321" time="164.5" type="appl" />
         <tli id="T322" time="165.0" type="appl" />
         <tli id="T323" time="165.5" type="appl" />
         <tli id="T324" time="166.0" type="appl" />
         <tli id="T325" time="166.5" type="appl" />
         <tli id="T326" time="167.0" type="appl" />
         <tli id="T327" time="167.5" type="appl" />
         <tli id="T328" time="168.0" type="appl" />
         <tli id="T329" time="168.5" type="appl" />
         <tli id="T330" time="169.0" type="appl" />
         <tli id="T331" time="169.5" type="appl" />
         <tli id="T332" time="170.0" type="appl" />
         <tli id="T333" time="170.5" type="appl" />
         <tli id="T334" time="171.0" type="appl" />
         <tli id="T335" time="171.5" type="appl" />
         <tli id="T336" time="172.0" type="appl" />
         <tli id="T337" time="172.5" type="appl" />
         <tli id="T338" time="173.0" type="appl" />
         <tli id="T339" time="173.5" type="appl" />
         <tli id="T340" time="174.0" type="appl" />
         <tli id="T341" time="174.5" type="appl" />
         <tli id="T342" time="175.0" type="appl" />
         <tli id="T343" time="175.5" type="appl" />
         <tli id="T344" time="176.0" type="appl" />
         <tli id="T345" time="176.5" type="appl" />
         <tli id="T346" time="177.0" type="appl" />
         <tli id="T347" time="177.5" type="appl" />
         <tli id="T348" time="178.0" type="appl" />
         <tli id="T349" time="178.5" type="appl" />
         <tli id="T350" time="179.0" type="appl" />
         <tli id="T351" time="179.5" type="appl" />
         <tli id="T352" time="180.0" type="appl" />
         <tli id="T353" time="180.5" type="appl" />
         <tli id="T354" time="181.0" type="appl" />
         <tli id="T355" time="181.5" type="appl" />
         <tli id="T356" time="182.0" type="appl" />
         <tli id="T357" time="182.5" type="appl" />
         <tli id="T358" time="183.0" type="appl" />
         <tli id="T359" time="183.5" type="appl" />
         <tli id="T360" time="184.0" type="appl" />
         <tli id="T361" time="184.5" type="appl" />
         <tli id="T362" time="185.0" type="appl" />
         <tli id="T363" time="185.5" type="appl" />
         <tli id="T364" time="186.0" type="appl" />
         <tli id="T365" time="186.5" type="appl" />
         <tli id="T366" time="187.0" type="appl" />
         <tli id="T367" time="187.5" type="appl" />
         <tli id="T368" time="188.0" type="appl" />
         <tli id="T369" time="188.5" type="appl" />
         <tli id="T370" time="189.0" type="appl" />
         <tli id="T371" time="189.5" type="appl" />
         <tli id="T372" time="190.0" type="appl" />
         <tli id="T373" time="190.5" type="appl" />
         <tli id="T374" time="191.0" type="appl" />
         <tli id="T375" time="191.5" type="appl" />
         <tli id="T376" time="192.0" type="appl" />
         <tli id="T377" time="192.5" type="appl" />
         <tli id="T378" time="193.0" type="appl" />
         <tli id="T379" time="193.5" type="appl" />
         <tli id="T380" time="194.0" type="appl" />
         <tli id="T381" time="194.5" type="appl" />
         <tli id="T382" time="195.0" type="appl" />
         <tli id="T383" time="195.5" type="appl" />
         <tli id="T384" time="196.0" type="appl" />
         <tli id="T385" time="196.5" type="appl" />
         <tli id="T386" time="197.0" type="appl" />
         <tli id="T387" time="197.5" type="appl" />
         <tli id="T388" time="198.0" type="appl" />
         <tli id="T389" time="198.5" type="appl" />
         <tli id="T390" time="199.0" type="appl" />
         <tli id="T391" time="199.5" type="appl" />
         <tli id="T392" time="200.0" type="appl" />
         <tli id="T393" time="200.5" type="appl" />
         <tli id="T394" time="201.0" type="appl" />
         <tli id="T395" time="201.5" type="appl" />
         <tli id="T396" time="202.0" type="appl" />
         <tli id="T397" time="202.5" type="appl" />
         <tli id="T398" time="203.0" type="appl" />
         <tli id="T399" time="203.5" type="appl" />
         <tli id="T400" time="204.0" type="appl" />
         <tli id="T401" time="204.5" type="appl" />
         <tli id="T402" time="205.0" type="appl" />
         <tli id="T403" time="205.5" type="appl" />
         <tli id="T404" time="206.0" type="appl" />
         <tli id="T405" time="206.5" type="appl" />
         <tli id="T406" time="207.0" type="appl" />
         <tli id="T407" time="207.5" type="appl" />
         <tli id="T408" time="208.0" type="appl" />
         <tli id="T409" time="208.5" type="appl" />
         <tli id="T410" time="209.0" type="appl" />
         <tli id="T411" time="209.5" type="appl" />
         <tli id="T412" time="210.0" type="appl" />
         <tli id="T413" time="210.5" type="appl" />
         <tli id="T414" time="211.0" type="appl" />
         <tli id="T415" time="211.5" type="appl" />
         <tli id="T416" time="212.0" type="appl" />
         <tli id="T417" time="212.5" type="appl" />
         <tli id="T418" time="213.0" type="appl" />
         <tli id="T419" time="213.5" type="appl" />
         <tli id="T420" time="214.0" type="appl" />
         <tli id="T421" time="214.5" type="appl" />
         <tli id="T422" time="215.0" type="appl" />
         <tli id="T423" time="215.5" type="appl" />
         <tli id="T424" time="216.0" type="appl" />
         <tli id="T425" time="216.5" type="appl" />
         <tli id="T426" time="217.0" type="appl" />
         <tli id="T427" time="217.5" type="appl" />
         <tli id="T428" time="218.0" type="appl" />
         <tli id="T429" time="218.5" type="appl" />
         <tli id="T430" time="219.0" type="appl" />
         <tli id="T431" time="219.5" type="appl" />
         <tli id="T432" time="220.0" type="appl" />
         <tli id="T433" time="220.5" type="appl" />
         <tli id="T434" time="221.0" type="appl" />
         <tli id="T435" time="221.5" type="appl" />
         <tli id="T436" time="222.0" type="appl" />
         <tli id="T437" time="222.5" type="appl" />
         <tli id="T438" time="223.0" type="appl" />
         <tli id="T439" time="223.5" type="appl" />
         <tli id="T440" time="224.0" type="appl" />
         <tli id="T441" time="224.5" type="appl" />
         <tli id="T442" time="225.0" type="appl" />
         <tli id="T443" time="225.5" type="appl" />
         <tli id="T444" time="226.0" type="appl" />
         <tli id="T445" time="226.5" type="appl" />
         <tli id="T446" time="227.0" type="appl" />
         <tli id="T447" time="227.5" type="appl" />
         <tli id="T448" time="228.0" type="appl" />
         <tli id="T449" time="228.5" type="appl" />
         <tli id="T450" time="229.0" type="appl" />
         <tli id="T451" time="229.5" type="appl" />
         <tli id="T452" time="230.0" type="appl" />
         <tli id="T453" time="230.5" type="appl" />
         <tli id="T454" time="231.0" type="appl" />
         <tli id="T455" time="231.5" type="appl" />
         <tli id="T456" time="232.0" type="appl" />
         <tli id="T457" time="232.5" type="appl" />
         <tli id="T458" time="233.0" type="appl" />
         <tli id="T459" time="233.5" type="appl" />
         <tli id="T460" time="234.0" type="appl" />
         <tli id="T461" time="234.5" type="appl" />
         <tli id="T462" time="235.0" type="appl" />
         <tli id="T463" time="235.5" type="appl" />
         <tli id="T464" time="236.0" type="appl" />
         <tli id="T465" time="236.5" type="appl" />
         <tli id="T466" time="237.0" type="appl" />
         <tli id="T467" time="237.5" type="appl" />
         <tli id="T468" time="238.0" type="appl" />
         <tli id="T469" time="238.5" type="appl" />
         <tli id="T470" time="239.0" type="appl" />
         <tli id="T471" time="239.5" type="appl" />
         <tli id="T472" time="240.0" type="appl" />
         <tli id="T473" time="240.5" type="appl" />
         <tli id="T474" time="241.0" type="appl" />
         <tli id="T475" time="241.5" type="appl" />
         <tli id="T476" time="242.0" type="appl" />
         <tli id="T477" time="242.5" type="appl" />
         <tli id="T478" time="243.0" type="appl" />
         <tli id="T479" time="243.5" type="appl" />
         <tli id="T480" time="244.0" type="appl" />
         <tli id="T481" time="244.5" type="appl" />
         <tli id="T482" time="245.0" type="appl" />
         <tli id="T483" time="245.5" type="appl" />
         <tli id="T484" time="246.0" type="appl" />
         <tli id="T485" time="246.5" type="appl" />
         <tli id="T486" time="247.0" type="appl" />
         <tli id="T487" time="247.5" type="appl" />
         <tli id="T488" time="248.0" type="appl" />
         <tli id="T489" time="248.5" type="appl" />
         <tli id="T490" time="249.0" type="appl" />
         <tli id="T491" time="249.5" type="appl" />
         <tli id="T492" time="250.0" type="appl" />
         <tli id="T493" time="250.5" type="appl" />
         <tli id="T494" time="251.0" type="appl" />
         <tli id="T495" time="251.5" type="appl" />
         <tli id="T496" time="252.0" type="appl" />
         <tli id="T497" time="252.5" type="appl" />
         <tli id="T498" time="253.0" type="appl" />
         <tli id="T499" time="253.5" type="appl" />
         <tli id="T500" time="254.0" type="appl" />
         <tli id="T501" time="254.5" type="appl" />
         <tli id="T502" time="255.0" type="appl" />
         <tli id="T503" time="255.5" type="appl" />
         <tli id="T504" time="256.0" type="appl" />
         <tli id="T505" time="256.5" type="appl" />
         <tli id="T506" time="257.0" type="appl" />
         <tli id="T507" time="257.5" type="appl" />
         <tli id="T508" time="258.0" type="appl" />
         <tli id="T509" time="258.5" type="appl" />
         <tli id="T510" time="259.0" type="appl" />
         <tli id="T511" time="259.5" type="appl" />
         <tli id="T512" time="260.0" type="appl" />
         <tli id="T513" time="260.5" type="appl" />
         <tli id="T514" time="261.0" type="appl" />
         <tli id="T515" time="261.5" type="appl" />
         <tli id="T516" time="262.0" type="appl" />
         <tli id="T517" time="262.5" type="appl" />
         <tli id="T518" time="263.0" type="appl" />
         <tli id="T519" time="263.5" type="appl" />
         <tli id="T520" time="264.0" type="appl" />
         <tli id="T521" time="264.5" type="appl" />
         <tli id="T522" time="265.0" type="appl" />
         <tli id="T523" time="265.5" type="appl" />
         <tli id="T524" time="266.0" type="appl" />
         <tli id="T525" time="266.5" type="appl" />
         <tli id="T526" time="267.0" type="appl" />
         <tli id="T527" time="267.5" type="appl" />
         <tli id="T528" time="268.0" type="appl" />
         <tli id="T529" time="268.5" type="appl" />
         <tli id="T530" time="269.0" type="appl" />
         <tli id="T531" time="269.5" type="appl" />
         <tli id="T532" time="270.0" type="appl" />
         <tli id="T533" time="270.5" type="appl" />
         <tli id="T534" time="271.0" type="appl" />
         <tli id="T535" time="271.5" type="appl" />
         <tli id="T536" time="272.0" type="appl" />
         <tli id="T537" time="272.5" type="appl" />
         <tli id="T538" time="273.0" type="appl" />
         <tli id="T539" time="273.5" type="appl" />
         <tli id="T540" time="274.0" type="appl" />
         <tli id="T541" time="274.5" type="appl" />
         <tli id="T542" time="275.0" type="appl" />
         <tli id="T543" time="275.5" type="appl" />
         <tli id="T544" time="276.0" type="appl" />
         <tli id="T545" time="276.5" type="appl" />
         <tli id="T546" time="277.0" type="appl" />
         <tli id="T547" time="277.5" type="appl" />
         <tli id="T548" time="278.0" type="appl" />
         <tli id="T549" time="278.5" type="appl" />
         <tli id="T550" time="279.0" type="appl" />
         <tli id="T551" time="279.5" type="appl" />
         <tli id="T552" time="280.0" type="appl" />
         <tli id="T553" time="280.5" type="appl" />
         <tli id="T554" time="281.0" type="appl" />
         <tli id="T555" time="281.5" type="appl" />
         <tli id="T556" time="282.0" type="appl" />
         <tli id="T557" time="282.5" type="appl" />
         <tli id="T558" time="283.0" type="appl" />
         <tli id="T559" time="283.5" type="appl" />
         <tli id="T560" time="284.0" type="appl" />
         <tli id="T561" time="284.5" type="appl" />
         <tli id="T562" time="285.0" type="appl" />
         <tli id="T563" time="285.5" type="appl" />
         <tli id="T564" time="286.0" type="appl" />
         <tli id="T565" time="286.5" type="appl" />
         <tli id="T566" time="287.0" type="appl" />
         <tli id="T567" time="287.5" type="appl" />
         <tli id="T568" time="288.0" type="appl" />
         <tli id="T569" time="288.5" type="appl" />
         <tli id="T570" time="289.0" type="appl" />
         <tli id="T571" time="289.5" type="appl" />
         <tli id="T572" time="290.0" type="appl" />
         <tli id="T573" time="290.5" type="appl" />
         <tli id="T574" time="291.0" type="appl" />
         <tli id="T575" time="291.5" type="appl" />
         <tli id="T576" time="292.0" type="appl" />
         <tli id="T577" time="292.5" type="appl" />
         <tli id="T578" time="293.0" type="appl" />
         <tli id="T579" time="293.5" type="appl" />
         <tli id="T580" time="294.0" type="appl" />
         <tli id="T581" time="294.5" type="appl" />
         <tli id="T582" time="295.0" type="appl" />
         <tli id="T583" time="295.5" type="appl" />
         <tli id="T584" time="296.0" type="appl" />
         <tli id="T585" time="296.5" type="appl" />
         <tli id="T586" time="297.0" type="appl" />
         <tli id="T587" time="297.5" type="appl" />
         <tli id="T588" time="298.0" type="appl" />
         <tli id="T589" time="298.5" type="appl" />
         <tli id="T590" time="299.0" type="appl" />
         <tli id="T591" time="299.5" type="appl" />
         <tli id="T592" time="300.0" type="appl" />
         <tli id="T593" time="300.5" type="appl" />
         <tli id="T594" time="301.0" type="appl" />
         <tli id="T595" time="301.5" type="appl" />
         <tli id="T596" time="302.0" type="appl" />
         <tli id="T597" time="302.5" type="appl" />
         <tli id="T598" time="303.0" type="appl" />
         <tli id="T599" time="303.5" type="appl" />
         <tli id="T600" time="304.0" type="appl" />
         <tli id="T601" time="304.5" type="appl" />
         <tli id="T602" time="305.0" type="appl" />
         <tli id="T603" time="305.5" type="appl" />
         <tli id="T604" time="306.0" type="appl" />
         <tli id="T605" time="306.5" type="appl" />
         <tli id="T606" time="307.0" type="appl" />
         <tli id="T607" time="307.5" type="appl" />
         <tli id="T608" time="308.0" type="appl" />
         <tli id="T609" time="308.5" type="appl" />
         <tli id="T610" time="309.0" type="appl" />
         <tli id="T611" time="309.5" type="appl" />
         <tli id="T612" time="310.0" type="appl" />
         <tli id="T613" time="310.5" type="appl" />
         <tli id="T614" time="311.0" type="appl" />
         <tli id="T615" time="311.5" type="appl" />
         <tli id="T616" time="312.0" type="appl" />
         <tli id="T617" time="312.5" type="appl" />
         <tli id="T618" time="313.0" type="appl" />
         <tli id="T619" time="313.5" type="appl" />
         <tli id="T620" time="314.0" type="appl" />
         <tli id="T621" time="314.5" type="appl" />
         <tli id="T622" time="315.0" type="appl" />
         <tli id="T623" time="315.5" type="appl" />
         <tli id="T624" time="316.0" type="appl" />
         <tli id="T625" time="316.5" type="appl" />
         <tli id="T626" time="317.0" type="appl" />
         <tli id="T627" time="317.5" type="appl" />
         <tli id="T628" time="318.0" type="appl" />
         <tli id="T629" time="318.5" type="appl" />
         <tli id="T630" time="319.0" type="appl" />
         <tli id="T631" time="319.5" type="appl" />
         <tli id="T632" time="320.0" type="appl" />
         <tli id="T633" time="320.5" type="appl" />
         <tli id="T634" time="321.0" type="appl" />
         <tli id="T635" time="321.5" type="appl" />
         <tli id="T636" time="322.0" type="appl" />
         <tli id="T637" time="322.5" type="appl" />
         <tli id="T638" time="323.0" type="appl" />
         <tli id="T639" time="323.5" type="appl" />
         <tli id="T640" time="324.0" type="appl" />
         <tli id="T641" time="324.5" type="appl" />
         <tli id="T642" time="325.0" type="appl" />
         <tli id="T643" time="325.5" type="appl" />
         <tli id="T644" time="326.0" type="appl" />
         <tli id="T645" time="326.5" type="appl" />
         <tli id="T646" time="327.0" type="appl" />
         <tli id="T647" time="327.5" type="appl" />
         <tli id="T648" time="328.0" type="appl" />
         <tli id="T649" time="328.5" type="appl" />
         <tli id="T650" time="329.0" type="appl" />
         <tli id="T651" time="329.5" type="appl" />
         <tli id="T652" time="330.0" type="appl" />
         <tli id="T653" time="330.5" type="appl" />
         <tli id="T654" time="331.0" type="appl" />
         <tli id="T655" time="331.5" type="appl" />
         <tli id="T656" time="332.0" type="appl" />
         <tli id="T657" time="332.5" type="appl" />
         <tli id="T658" time="333.0" type="appl" />
         <tli id="T659" time="333.5" type="appl" />
         <tli id="T660" time="334.0" type="appl" />
         <tli id="T661" time="334.5" type="appl" />
         <tli id="T662" time="335.0" type="appl" />
         <tli id="T663" time="335.5" type="appl" />
         <tli id="T664" time="336.0" type="appl" />
         <tli id="T665" time="336.5" type="appl" />
         <tli id="T666" time="337.0" type="appl" />
         <tli id="T667" time="337.5" type="appl" />
         <tli id="T668" time="338.0" type="appl" />
         <tli id="T669" time="338.5" type="appl" />
         <tli id="T670" time="339.0" type="appl" />
         <tli id="T671" time="339.5" type="appl" />
         <tli id="T672" time="340.0" type="appl" />
         <tli id="T673" time="340.5" type="appl" />
         <tli id="T674" time="341.0" type="appl" />
         <tli id="T675" time="341.5" type="appl" />
         <tli id="T676" time="342.0" type="appl" />
         <tli id="T677" time="342.5" type="appl" />
         <tli id="T678" time="343.0" type="appl" />
         <tli id="T679" time="343.5" type="appl" />
         <tli id="T680" time="344.0" type="appl" />
         <tli id="T681" time="344.5" type="appl" />
         <tli id="T682" time="345.0" type="appl" />
         <tli id="T683" time="345.5" type="appl" />
         <tli id="T684" time="346.0" type="appl" />
         <tli id="T685" time="346.5" type="appl" />
         <tli id="T686" time="347.0" type="appl" />
         <tli id="T687" time="347.5" type="appl" />
         <tli id="T688" time="348.0" type="appl" />
         <tli id="T689" time="348.5" type="appl" />
         <tli id="T690" time="349.0" type="appl" />
         <tli id="T691" time="349.5" type="appl" />
         <tli id="T692" time="350.0" type="appl" />
         <tli id="T693" time="350.5" type="appl" />
         <tli id="T694" time="351.0" type="appl" />
         <tli id="T695" time="351.5" type="appl" />
         <tli id="T696" time="352.0" type="appl" />
         <tli id="T697" time="352.5" type="appl" />
         <tli id="T698" time="353.0" type="appl" />
         <tli id="T699" time="353.5" type="appl" />
         <tli id="T700" time="354.0" type="appl" />
         <tli id="T701" time="354.5" type="appl" />
         <tli id="T702" time="355.0" type="appl" />
         <tli id="T703" time="355.5" type="appl" />
         <tli id="T704" time="356.0" type="appl" />
         <tli id="T705" time="356.5" type="appl" />
         <tli id="T706" time="357.0" type="appl" />
         <tli id="T707" time="357.5" type="appl" />
         <tli id="T708" time="358.0" type="appl" />
         <tli id="T709" time="358.5" type="appl" />
         <tli id="T710" time="359.0" type="appl" />
         <tli id="T711" time="359.5" type="appl" />
         <tli id="T712" time="360.0" type="appl" />
         <tli id="T713" time="360.5" type="appl" />
         <tli id="T714" time="361.0" type="appl" />
         <tli id="T715" time="361.5" type="appl" />
         <tli id="T716" time="362.0" type="appl" />
         <tli id="T717" time="362.5" type="appl" />
         <tli id="T718" time="363.0" type="appl" />
         <tli id="T719" time="363.5" type="appl" />
         <tli id="T720" time="364.0" type="appl" />
         <tli id="T721" time="364.5" type="appl" />
         <tli id="T722" time="365.0" type="appl" />
         <tli id="T723" time="365.5" type="appl" />
         <tli id="T724" time="366.0" type="appl" />
         <tli id="T725" time="366.5" type="appl" />
         <tli id="T726" time="367.0" type="appl" />
         <tli id="T727" time="367.5" type="appl" />
         <tli id="T728" time="368.0" type="appl" />
         <tli id="T729" time="368.5" type="appl" />
         <tli id="T730" time="369.0" type="appl" />
         <tli id="T731" time="369.5" type="appl" />
         <tli id="T732" time="370.0" type="appl" />
         <tli id="T733" time="370.5" type="appl" />
         <tli id="T734" time="371.0" type="appl" />
         <tli id="T735" time="371.5" type="appl" />
         <tli id="T736" time="372.0" type="appl" />
         <tli id="T737" time="372.5" type="appl" />
         <tli id="T738" time="373.0" type="appl" />
         <tli id="T739" time="373.5" type="appl" />
         <tli id="T740" time="374.0" type="appl" />
         <tli id="T741" time="374.5" type="appl" />
         <tli id="T742" time="375.0" type="appl" />
         <tli id="T743" time="375.5" type="appl" />
         <tli id="T744" time="376.0" type="appl" />
         <tli id="T745" time="376.5" type="appl" />
         <tli id="T746" time="377.0" type="appl" />
         <tli id="T747" time="377.5" type="appl" />
         <tli id="T748" time="378.0" type="appl" />
         <tli id="T749" time="378.5" type="appl" />
         <tli id="T750" time="379.0" type="appl" />
         <tli id="T751" time="379.5" type="appl" />
         <tli id="T752" time="380.0" type="appl" />
         <tli id="T753" time="380.5" type="appl" />
         <tli id="T754" time="381.0" type="appl" />
         <tli id="T755" time="381.5" type="appl" />
         <tli id="T756" time="382.0" type="appl" />
         <tli id="T757" time="382.5" type="appl" />
         <tli id="T758" time="383.0" type="appl" />
         <tli id="T759" time="383.5" type="appl" />
         <tli id="T760" time="384.0" type="appl" />
         <tli id="T761" time="384.5" type="appl" />
         <tli id="T762" time="385.0" type="appl" />
         <tli id="T763" time="385.5" type="appl" />
         <tli id="T764" time="386.0" type="appl" />
         <tli id="T765" time="386.5" type="appl" />
         <tli id="T766" time="387.0" type="appl" />
         <tli id="T767" time="387.5" type="appl" />
         <tli id="T768" time="388.0" type="appl" />
         <tli id="T769" time="388.5" type="appl" />
         <tli id="T770" time="389.0" type="appl" />
         <tli id="T771" time="389.5" type="appl" />
         <tli id="T772" time="390.0" type="appl" />
         <tli id="T773" time="390.5" type="appl" />
         <tli id="T774" time="391.0" type="appl" />
         <tli id="T775" time="391.5" type="appl" />
         <tli id="T776" time="392.0" type="appl" />
         <tli id="T777" time="392.5" type="appl" />
         <tli id="T778" time="393.0" type="appl" />
         <tli id="T779" time="393.5" type="appl" />
         <tli id="T780" time="394.0" type="appl" />
         <tli id="T781" time="394.5" type="appl" />
         <tli id="T782" time="395.0" type="appl" />
         <tli id="T783" time="395.5" type="appl" />
         <tli id="T784" time="396.0" type="appl" />
         <tli id="T785" time="396.5" type="appl" />
         <tli id="T786" time="397.0" type="appl" />
         <tli id="T787" time="397.5" type="appl" />
         <tli id="T788" time="398.0" type="appl" />
         <tli id="T789" time="398.5" type="appl" />
         <tli id="T790" time="399.0" type="appl" />
         <tli id="T791" time="399.5" type="appl" />
         <tli id="T792" time="400.0" type="appl" />
         <tli id="T793" time="400.5" type="appl" />
         <tli id="T794" time="401.0" type="appl" />
         <tli id="T795" time="401.5" type="appl" />
         <tli id="T796" time="402.0" type="appl" />
         <tli id="T797" time="402.5" type="appl" />
         <tli id="T798" time="403.0" type="appl" />
         <tli id="T799" time="403.5" type="appl" />
         <tli id="T800" time="404.0" type="appl" />
         <tli id="T801" time="404.5" type="appl" />
         <tli id="T802" time="405.0" type="appl" />
         <tli id="T803" time="405.5" type="appl" />
         <tli id="T804" time="406.0" type="appl" />
         <tli id="T805" time="406.5" type="appl" />
         <tli id="T806" time="407.0" type="appl" />
         <tli id="T807" time="407.5" type="appl" />
         <tli id="T808" time="408.0" type="appl" />
         <tli id="T809" time="408.5" type="appl" />
         <tli id="T810" time="409.0" type="appl" />
         <tli id="T811" time="409.5" type="appl" />
         <tli id="T812" time="410.0" type="appl" />
         <tli id="T813" time="410.5" type="appl" />
         <tli id="T814" time="411.0" type="appl" />
         <tli id="T815" time="411.5" type="appl" />
         <tli id="T816" time="412.0" type="appl" />
         <tli id="T817" time="412.5" type="appl" />
         <tli id="T818" time="413.0" type="appl" />
         <tli id="T819" time="413.5" type="appl" />
         <tli id="T820" time="414.0" type="appl" />
         <tli id="T821" time="414.5" type="appl" />
         <tli id="T822" time="415.0" type="appl" />
         <tli id="T823" time="415.5" type="appl" />
         <tli id="T824" time="416.0" type="appl" />
         <tli id="T825" time="416.5" type="appl" />
         <tli id="T826" time="417.0" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PoMA"
                      type="t">
         <timeline-fork end="T1" start="T0">
            <tli id="T0.tx.1" />
         </timeline-fork>
         <timeline-fork end="T3" start="T2">
            <tli id="T2.tx.1" />
         </timeline-fork>
         <timeline-fork end="T15" start="T14">
            <tli id="T14.tx.1" />
         </timeline-fork>
         <timeline-fork end="T17" start="T16">
            <tli id="T16.tx.1" />
         </timeline-fork>
         <timeline-fork end="T51" start="T50">
            <tli id="T50.tx.1" />
         </timeline-fork>
         <timeline-fork end="T77" start="T76">
            <tli id="T76.tx.1" />
         </timeline-fork>
         <timeline-fork end="T83" start="T82">
            <tli id="T82.tx.1" />
         </timeline-fork>
         <timeline-fork end="T94" start="T93">
            <tli id="T93.tx.1" />
         </timeline-fork>
         <timeline-fork end="T100" start="T99">
            <tli id="T99.tx.1" />
         </timeline-fork>
         <timeline-fork end="T110" start="T109">
            <tli id="T109.tx.1" />
         </timeline-fork>
         <timeline-fork end="T121" start="T120">
            <tli id="T120.tx.1" />
         </timeline-fork>
         <timeline-fork end="T134" start="T133">
            <tli id="T133.tx.1" />
         </timeline-fork>
         <timeline-fork end="T157" start="T156">
            <tli id="T156.tx.1" />
         </timeline-fork>
         <timeline-fork end="T167" start="T166">
            <tli id="T166.tx.1" />
         </timeline-fork>
         <timeline-fork end="T175" start="T174">
            <tli id="T174.tx.1" />
         </timeline-fork>
         <timeline-fork end="T207" start="T206">
            <tli id="T206.tx.1" />
         </timeline-fork>
         <timeline-fork end="T230" start="T229">
            <tli id="T229.tx.1" />
         </timeline-fork>
         <timeline-fork end="T251" start="T250">
            <tli id="T250.tx.1" />
         </timeline-fork>
         <timeline-fork end="T285" start="T284">
            <tli id="T284.tx.1" />
         </timeline-fork>
         <timeline-fork end="T416" start="T415">
            <tli id="T415.tx.1" />
         </timeline-fork>
         <timeline-fork end="T487" start="T486">
            <tli id="T486.tx.1" />
         </timeline-fork>
         <timeline-fork end="T499" start="T498">
            <tli id="T498.tx.1" />
         </timeline-fork>
         <timeline-fork end="T504" start="T503">
            <tli id="T503.tx.1" />
         </timeline-fork>
         <timeline-fork end="T532" start="T531">
            <tli id="T531.tx.1" />
         </timeline-fork>
         <timeline-fork end="T647" start="T646">
            <tli id="T646.tx.1" />
         </timeline-fork>
         <timeline-fork end="T653" start="T652">
            <tli id="T652.tx.1" />
         </timeline-fork>
         <timeline-fork end="T663" start="T662">
            <tli id="T662.tx.1" />
         </timeline-fork>
         <timeline-fork end="T665" start="T664">
            <tli id="T664.tx.1" />
         </timeline-fork>
         <timeline-fork end="T699" start="T698">
            <tli id="T698.tx.1" />
         </timeline-fork>
         <timeline-fork end="T734" start="T733">
            <tli id="T733.tx.1" />
         </timeline-fork>
         <timeline-fork end="T743" start="T742">
            <tli id="T742.tx.1" />
         </timeline-fork>
         <timeline-fork end="T755" start="T754">
            <tli id="T754.tx.1" />
         </timeline-fork>
         <timeline-fork end="T756" start="T755">
            <tli id="T755.tx.1" />
         </timeline-fork>
         <timeline-fork end="T758" start="T757">
            <tli id="T757.tx.1" />
         </timeline-fork>
         <timeline-fork end="T811" start="T810">
            <tli id="T810.tx.1" />
         </timeline-fork>
         <timeline-fork end="T817" start="T816">
            <tli id="T816.tx.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T826" id="Seg_0" n="sc" s="T0">
               <ts e="T8" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T0.tx.1" id="Seg_4" n="HIAT:w" s="T0">Timir</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1" id="Seg_7" n="HIAT:w" s="T0.tx.1">Haːpka</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_10" n="HIAT:w" s="T1">onuga</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2.tx.1" id="Seg_13" n="HIAT:w" s="T2">Heliː</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_16" n="HIAT:w" s="T2.tx.1">Kur</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_19" n="HIAT:w" s="T3">di͡en</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_22" n="HIAT:w" s="T4">ikki</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_25" n="HIAT:w" s="T5">u͡ol</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_28" n="HIAT:w" s="T6">kihiler</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_31" n="HIAT:w" s="T7">olorbuttar</ts>
                  <nts id="Seg_32" n="HIAT:ip">.</nts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T11" id="Seg_35" n="HIAT:u" s="T8">
                  <ts e="T9" id="Seg_37" n="HIAT:w" s="T8">Ikki͡en</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_40" n="HIAT:w" s="T9">agalaːktar</ts>
                  <nts id="Seg_41" n="HIAT:ip">,</nts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_44" n="HIAT:w" s="T10">ijeleːkter</ts>
                  <nts id="Seg_45" n="HIAT:ip">.</nts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_48" n="HIAT:u" s="T11">
                  <ts e="T12" id="Seg_50" n="HIAT:w" s="T11">Dʼe</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_53" n="HIAT:w" s="T12">bu</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_56" n="HIAT:w" s="T13">oloronnor</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14.tx.1" id="Seg_59" n="HIAT:w" s="T14">Heliː</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_62" n="HIAT:w" s="T14.tx.1">Kura</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_65" n="HIAT:w" s="T15">diːr</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16.tx.1" id="Seg_68" n="HIAT:w" s="T16">Timir</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_71" n="HIAT:w" s="T16.tx.1">Haːpkatɨn</ts>
                  <nts id="Seg_72" n="HIAT:ip">:</nts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T23" id="Seg_75" n="HIAT:u" s="T17">
                  <nts id="Seg_76" n="HIAT:ip">"</nts>
                  <ts e="T18" id="Seg_78" n="HIAT:w" s="T17">Dogoːr</ts>
                  <nts id="Seg_79" n="HIAT:ip">,</nts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_82" n="HIAT:w" s="T18">bu</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_85" n="HIAT:w" s="T19">bihigi</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_88" n="HIAT:w" s="T20">tu͡ok</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_91" n="HIAT:w" s="T21">ideleːk</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_94" n="HIAT:w" s="T22">bu͡olabɨt</ts>
                  <nts id="Seg_95" n="HIAT:ip">?</nts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T27" id="Seg_98" n="HIAT:u" s="T23">
                  <ts e="T24" id="Seg_100" n="HIAT:w" s="T23">Togo</ts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_103" n="HIAT:w" s="T24">ide</ts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_106" n="HIAT:w" s="T25">idebitin</ts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_109" n="HIAT:w" s="T26">bilsibeppit</ts>
                  <nts id="Seg_110" n="HIAT:ip">?</nts>
                  <nts id="Seg_111" n="HIAT:ip">"</nts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T31" id="Seg_114" n="HIAT:u" s="T27">
                  <ts e="T28" id="Seg_116" n="HIAT:w" s="T27">Mekti͡e</ts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_119" n="HIAT:w" s="T28">bɨldʼahaːrɨ</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_122" n="HIAT:w" s="T29">gɨnar</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_125" n="HIAT:w" s="T30">bɨhɨːlaːk</ts>
                  <nts id="Seg_126" n="HIAT:ip">.</nts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T40" id="Seg_129" n="HIAT:u" s="T31">
                  <ts e="T32" id="Seg_131" n="HIAT:w" s="T31">Dogoro</ts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_133" n="HIAT:ip">"</nts>
                  <ts e="T33" id="Seg_135" n="HIAT:w" s="T32">eː</ts>
                  <nts id="Seg_136" n="HIAT:ip">,</nts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_139" n="HIAT:w" s="T33">dogor</ts>
                  <nts id="Seg_140" n="HIAT:ip">,</nts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_143" n="HIAT:w" s="T34">tu͡okputun</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_146" n="HIAT:w" s="T35">bɨldʼahan</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_149" n="HIAT:w" s="T36">mekti͡e</ts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_152" n="HIAT:w" s="T37">bɨldʼahɨ͡akpɨtɨj</ts>
                  <nts id="Seg_153" n="HIAT:ip">"</nts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_156" n="HIAT:w" s="T38">di͡en</ts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_159" n="HIAT:w" s="T39">bu͡olummat</ts>
                  <nts id="Seg_160" n="HIAT:ip">.</nts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T42" id="Seg_163" n="HIAT:u" s="T40">
                  <ts e="T41" id="Seg_165" n="HIAT:w" s="T40">Dogoro</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_168" n="HIAT:w" s="T41">tehippet</ts>
                  <nts id="Seg_169" n="HIAT:ip">:</nts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T44" id="Seg_172" n="HIAT:u" s="T42">
                  <nts id="Seg_173" n="HIAT:ip">"</nts>
                  <ts e="T43" id="Seg_175" n="HIAT:w" s="T42">Togo</ts>
                  <nts id="Seg_176" n="HIAT:ip">,</nts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_179" n="HIAT:w" s="T43">dogor</ts>
                  <nts id="Seg_180" n="HIAT:ip">!</nts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T48" id="Seg_183" n="HIAT:u" s="T44">
                  <ts e="T45" id="Seg_185" n="HIAT:w" s="T44">En</ts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_188" n="HIAT:w" s="T45">idegin</ts>
                  <nts id="Seg_189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_191" n="HIAT:w" s="T46">bili͡ekpin</ts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_194" n="HIAT:w" s="T47">bagarabɨn</ts>
                  <nts id="Seg_195" n="HIAT:ip">!</nts>
                  <nts id="Seg_196" n="HIAT:ip">"</nts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T50" id="Seg_199" n="HIAT:u" s="T48">
                  <ts e="T49" id="Seg_201" n="HIAT:w" s="T48">Dogoro</ts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_204" n="HIAT:w" s="T49">bu͡olummat</ts>
                  <nts id="Seg_205" n="HIAT:ip">.</nts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T64" id="Seg_208" n="HIAT:u" s="T50">
                  <ts e="T50.tx.1" id="Seg_210" n="HIAT:w" s="T50">Heliː</ts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_213" n="HIAT:w" s="T50.tx.1">Kur</ts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_215" n="HIAT:ip">"</nts>
                  <ts e="T52" id="Seg_217" n="HIAT:w" s="T51">min</ts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_220" n="HIAT:w" s="T52">kürü͡öm</ts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_223" n="HIAT:w" s="T53">enʼigitten</ts>
                  <nts id="Seg_224" n="HIAT:ip">,</nts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_227" n="HIAT:w" s="T54">bat</ts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_230" n="HIAT:w" s="T55">minʼiːgin</ts>
                  <nts id="Seg_231" n="HIAT:ip">,</nts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_234" n="HIAT:w" s="T56">min</ts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_237" n="HIAT:w" s="T57">kajdak</ts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_240" n="HIAT:w" s="T58">bu͡olabɨn</ts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_243" n="HIAT:w" s="T59">da</ts>
                  <nts id="Seg_244" n="HIAT:ip">,</nts>
                  <nts id="Seg_245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_247" n="HIAT:w" s="T60">hogordukkaːn</ts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_250" n="HIAT:w" s="T61">bu͡olan</ts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_253" n="HIAT:w" s="T62">is</ts>
                  <nts id="Seg_254" n="HIAT:ip">"</nts>
                  <nts id="Seg_255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_257" n="HIAT:w" s="T63">diːr</ts>
                  <nts id="Seg_258" n="HIAT:ip">.</nts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T65" id="Seg_261" n="HIAT:u" s="T64">
                  <ts e="T65" id="Seg_263" n="HIAT:w" s="T64">Dogoro</ts>
                  <nts id="Seg_264" n="HIAT:ip">:</nts>
                  <nts id="Seg_265" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T68" id="Seg_267" n="HIAT:u" s="T65">
                  <nts id="Seg_268" n="HIAT:ip">"</nts>
                  <ts e="T66" id="Seg_270" n="HIAT:w" s="T65">Batɨ͡aktɨbɨn</ts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_273" n="HIAT:w" s="T66">duː</ts>
                  <nts id="Seg_274" n="HIAT:ip">,</nts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_277" n="HIAT:w" s="T67">ajɨlaːgɨn</ts>
                  <nts id="Seg_278" n="HIAT:ip">.</nts>
                  <nts id="Seg_279" n="HIAT:ip">"</nts>
                  <nts id="Seg_280" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T77" id="Seg_282" n="HIAT:u" s="T68">
                  <nts id="Seg_283" n="HIAT:ip">"</nts>
                  <ts e="T69" id="Seg_285" n="HIAT:w" s="T68">Harsi͡erda</ts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_288" n="HIAT:w" s="T69">erdehit</ts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_291" n="HIAT:w" s="T70">kihi</ts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_294" n="HIAT:w" s="T71">turar</ts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_297" n="HIAT:w" s="T72">kemiger</ts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_300" n="HIAT:w" s="T73">bataːr</ts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_303" n="HIAT:w" s="T74">minʼigin</ts>
                  <nts id="Seg_304" n="HIAT:ip">"</nts>
                  <nts id="Seg_305" n="HIAT:ip">,</nts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_308" n="HIAT:w" s="T75">diːr</ts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76.tx.1" id="Seg_311" n="HIAT:w" s="T76">Heliː</ts>
                  <nts id="Seg_312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_314" n="HIAT:w" s="T76.tx.1">Kur</ts>
                  <nts id="Seg_315" n="HIAT:ip">.</nts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T82" id="Seg_318" n="HIAT:u" s="T77">
                  <ts e="T78" id="Seg_320" n="HIAT:w" s="T77">Innʼe</ts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_323" n="HIAT:w" s="T78">dehen</ts>
                  <nts id="Seg_324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_326" n="HIAT:w" s="T79">araksallar</ts>
                  <nts id="Seg_327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_329" n="HIAT:w" s="T80">dʼi͡e</ts>
                  <nts id="Seg_330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_332" n="HIAT:w" s="T81">dʼi͡eleriger</ts>
                  <nts id="Seg_333" n="HIAT:ip">.</nts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T86" id="Seg_336" n="HIAT:u" s="T82">
                  <ts e="T82.tx.1" id="Seg_338" n="HIAT:w" s="T82">Timir</ts>
                  <nts id="Seg_339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_341" n="HIAT:w" s="T82.tx.1">Haːpka</ts>
                  <nts id="Seg_342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_344" n="HIAT:w" s="T83">taksan</ts>
                  <nts id="Seg_345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_347" n="HIAT:w" s="T84">utuja</ts>
                  <nts id="Seg_348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_350" n="HIAT:w" s="T85">hɨtar</ts>
                  <nts id="Seg_351" n="HIAT:ip">.</nts>
                  <nts id="Seg_352" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T88" id="Seg_354" n="HIAT:u" s="T86">
                  <ts e="T87" id="Seg_356" n="HIAT:w" s="T86">Uhuktan</ts>
                  <nts id="Seg_357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_359" n="HIAT:w" s="T87">keler</ts>
                  <nts id="Seg_360" n="HIAT:ip">:</nts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T91" id="Seg_363" n="HIAT:u" s="T88">
                  <nts id="Seg_364" n="HIAT:ip">"</nts>
                  <ts e="T89" id="Seg_366" n="HIAT:w" s="T88">Dogorum</ts>
                  <nts id="Seg_367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_369" n="HIAT:w" s="T89">baːra</ts>
                  <nts id="Seg_370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_372" n="HIAT:w" s="T90">dʼürü</ts>
                  <nts id="Seg_373" n="HIAT:ip">?</nts>
                  <nts id="Seg_374" n="HIAT:ip">"</nts>
                  <nts id="Seg_375" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T99" id="Seg_377" n="HIAT:u" s="T91">
                  <ts e="T92" id="Seg_379" n="HIAT:w" s="T91">Dogorugar</ts>
                  <nts id="Seg_380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_382" n="HIAT:w" s="T92">kelbite</ts>
                  <nts id="Seg_383" n="HIAT:ip">,</nts>
                  <nts id="Seg_384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93.tx.1" id="Seg_386" n="HIAT:w" s="T93">Heliː</ts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_389" n="HIAT:w" s="T93.tx.1">Kur</ts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_392" n="HIAT:w" s="T94">orono</ts>
                  <nts id="Seg_393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_395" n="HIAT:w" s="T95">čoŋkojo</ts>
                  <nts id="Seg_396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_398" n="HIAT:w" s="T96">hɨtar</ts>
                  <nts id="Seg_399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_400" n="HIAT:ip">—</nts>
                  <nts id="Seg_401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_403" n="HIAT:w" s="T97">baran</ts>
                  <nts id="Seg_404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_406" n="HIAT:w" s="T98">kaːlbɨt</ts>
                  <nts id="Seg_407" n="HIAT:ip">.</nts>
                  <nts id="Seg_408" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T106" id="Seg_410" n="HIAT:u" s="T99">
                  <ts e="T99.tx.1" id="Seg_412" n="HIAT:w" s="T99">Timir</ts>
                  <nts id="Seg_413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_415" n="HIAT:w" s="T99.tx.1">Haːpka</ts>
                  <nts id="Seg_416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_418" n="HIAT:w" s="T100">hu͡ol</ts>
                  <nts id="Seg_419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_421" n="HIAT:w" s="T101">kajar</ts>
                  <nts id="Seg_422" n="HIAT:ip">,</nts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_425" n="HIAT:w" s="T102">hu͡olu</ts>
                  <nts id="Seg_426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_428" n="HIAT:w" s="T103">bulbat</ts>
                  <nts id="Seg_429" n="HIAT:ip">,</nts>
                  <nts id="Seg_430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_432" n="HIAT:w" s="T104">elete</ts>
                  <nts id="Seg_433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_435" n="HIAT:w" s="T105">bu͡olar</ts>
                  <nts id="Seg_436" n="HIAT:ip">.</nts>
                  <nts id="Seg_437" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T109" id="Seg_439" n="HIAT:u" s="T106">
                  <ts e="T107" id="Seg_441" n="HIAT:w" s="T106">Kiːren</ts>
                  <nts id="Seg_442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_444" n="HIAT:w" s="T107">oronun</ts>
                  <nts id="Seg_445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_447" n="HIAT:w" s="T108">kuptujar</ts>
                  <nts id="Seg_448" n="HIAT:ip">.</nts>
                  <nts id="Seg_449" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T120" id="Seg_451" n="HIAT:u" s="T109">
                  <ts e="T109.tx.1" id="Seg_453" n="HIAT:w" s="T109">Heliː</ts>
                  <nts id="Seg_454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_456" n="HIAT:w" s="T109.tx.1">Kur</ts>
                  <nts id="Seg_457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_459" n="HIAT:w" s="T110">oronun</ts>
                  <nts id="Seg_460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_462" n="HIAT:w" s="T111">annɨttan</ts>
                  <nts id="Seg_463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_465" n="HIAT:w" s="T112">üːteːn</ts>
                  <nts id="Seg_466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_468" n="HIAT:w" s="T113">kutujagɨn</ts>
                  <nts id="Seg_469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_471" n="HIAT:w" s="T114">koroːno</ts>
                  <nts id="Seg_472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_474" n="HIAT:w" s="T115">čoŋojo</ts>
                  <nts id="Seg_475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_477" n="HIAT:w" s="T116">hɨtar</ts>
                  <nts id="Seg_478" n="HIAT:ip">,</nts>
                  <nts id="Seg_479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_481" n="HIAT:w" s="T117">atɨn</ts>
                  <nts id="Seg_482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_484" n="HIAT:w" s="T118">hu͡ol</ts>
                  <nts id="Seg_485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_487" n="HIAT:w" s="T119">hu͡ok</ts>
                  <nts id="Seg_488" n="HIAT:ip">.</nts>
                  <nts id="Seg_489" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T126" id="Seg_491" n="HIAT:u" s="T120">
                  <ts e="T120.tx.1" id="Seg_493" n="HIAT:w" s="T120">Timir</ts>
                  <nts id="Seg_494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_496" n="HIAT:w" s="T120.tx.1">Haːpka</ts>
                  <nts id="Seg_497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_499" n="HIAT:w" s="T121">kutujak</ts>
                  <nts id="Seg_500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_502" n="HIAT:w" s="T122">bu͡olla</ts>
                  <nts id="Seg_503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_505" n="HIAT:w" s="T123">da</ts>
                  <nts id="Seg_506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_508" n="HIAT:w" s="T124">koroːnunan</ts>
                  <nts id="Seg_509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_511" n="HIAT:w" s="T125">tüste</ts>
                  <nts id="Seg_512" n="HIAT:ip">.</nts>
                  <nts id="Seg_513" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T133" id="Seg_515" n="HIAT:u" s="T126">
                  <ts e="T127" id="Seg_517" n="HIAT:w" s="T126">ɨnaraːŋŋɨta</ts>
                  <nts id="Seg_518" n="HIAT:ip">,</nts>
                  <nts id="Seg_519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_521" n="HIAT:w" s="T127">koroːnton</ts>
                  <nts id="Seg_522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_524" n="HIAT:w" s="T128">taksaːt</ts>
                  <nts id="Seg_525" n="HIAT:ip">,</nts>
                  <nts id="Seg_526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_528" n="HIAT:w" s="T129">goronu͡ok</ts>
                  <nts id="Seg_529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_531" n="HIAT:w" s="T130">bu͡olaːt</ts>
                  <nts id="Seg_532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_534" n="HIAT:w" s="T131">ɨstanan</ts>
                  <nts id="Seg_535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_537" n="HIAT:w" s="T132">kaːlar</ts>
                  <nts id="Seg_538" n="HIAT:ip">.</nts>
                  <nts id="Seg_539" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T138" id="Seg_541" n="HIAT:u" s="T133">
                  <ts e="T133.tx.1" id="Seg_543" n="HIAT:w" s="T133">Timir</ts>
                  <nts id="Seg_544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_546" n="HIAT:w" s="T133.tx.1">Haːpka</ts>
                  <nts id="Seg_547" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_549" n="HIAT:w" s="T134">goronu͡ok</ts>
                  <nts id="Seg_550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_552" n="HIAT:w" s="T135">bu͡olaːt</ts>
                  <nts id="Seg_553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_555" n="HIAT:w" s="T136">batan</ts>
                  <nts id="Seg_556" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_558" n="HIAT:w" s="T137">iher</ts>
                  <nts id="Seg_559" n="HIAT:ip">.</nts>
                  <nts id="Seg_560" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T142" id="Seg_562" n="HIAT:u" s="T138">
                  <ts e="T139" id="Seg_564" n="HIAT:w" s="T138">Huk</ts>
                  <nts id="Seg_565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_567" n="HIAT:w" s="T139">kötüː</ts>
                  <nts id="Seg_568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_570" n="HIAT:w" s="T140">baran</ts>
                  <nts id="Seg_571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_573" n="HIAT:w" s="T141">iste</ts>
                  <nts id="Seg_574" n="HIAT:ip">.</nts>
                  <nts id="Seg_575" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T153" id="Seg_577" n="HIAT:u" s="T142">
                  <ts e="T143" id="Seg_579" n="HIAT:w" s="T142">Bu</ts>
                  <nts id="Seg_580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_582" n="HIAT:w" s="T143">goronu͡ok</ts>
                  <nts id="Seg_583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_585" n="HIAT:w" s="T144">hu͡ola</ts>
                  <nts id="Seg_586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_588" n="HIAT:w" s="T145">büter</ts>
                  <nts id="Seg_589" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_591" n="HIAT:w" s="T146">da</ts>
                  <nts id="Seg_592" n="HIAT:ip">,</nts>
                  <nts id="Seg_593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_595" n="HIAT:w" s="T147">orto</ts>
                  <nts id="Seg_596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_598" n="HIAT:w" s="T148">dojdu</ts>
                  <nts id="Seg_599" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_601" n="HIAT:w" s="T149">kɨrsa</ts>
                  <nts id="Seg_602" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_604" n="HIAT:w" s="T150">hu͡ola</ts>
                  <nts id="Seg_605" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_607" n="HIAT:w" s="T151">bara</ts>
                  <nts id="Seg_608" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_610" n="HIAT:w" s="T152">turda</ts>
                  <nts id="Seg_611" n="HIAT:ip">.</nts>
                  <nts id="Seg_612" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T156" id="Seg_614" n="HIAT:u" s="T153">
                  <ts e="T154" id="Seg_616" n="HIAT:w" s="T153">Dogoro</ts>
                  <nts id="Seg_617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_619" n="HIAT:w" s="T154">halla</ts>
                  <nts id="Seg_620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_622" n="HIAT:w" s="T155">iste</ts>
                  <nts id="Seg_623" n="HIAT:ip">.</nts>
                  <nts id="Seg_624" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T162" id="Seg_626" n="HIAT:u" s="T156">
                  <ts e="T156.tx.1" id="Seg_628" n="HIAT:w" s="T156">Timir</ts>
                  <nts id="Seg_629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_631" n="HIAT:w" s="T156.tx.1">Haːpka</ts>
                  <nts id="Seg_632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_634" n="HIAT:w" s="T157">kɨrsa</ts>
                  <nts id="Seg_635" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_637" n="HIAT:w" s="T158">bu͡olar</ts>
                  <nts id="Seg_638" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_640" n="HIAT:w" s="T159">da</ts>
                  <nts id="Seg_641" n="HIAT:ip">,</nts>
                  <nts id="Seg_642" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_644" n="HIAT:w" s="T160">ojon</ts>
                  <nts id="Seg_645" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_647" n="HIAT:w" s="T161">iste</ts>
                  <nts id="Seg_648" n="HIAT:ip">.</nts>
                  <nts id="Seg_649" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T170" id="Seg_651" n="HIAT:u" s="T162">
                  <ts e="T163" id="Seg_653" n="HIAT:w" s="T162">Biːr</ts>
                  <nts id="Seg_654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_656" n="HIAT:w" s="T163">hirge</ts>
                  <nts id="Seg_657" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_659" n="HIAT:w" s="T164">tüspüt</ts>
                  <nts id="Seg_660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_662" n="HIAT:w" s="T165">da</ts>
                  <nts id="Seg_663" n="HIAT:ip">,</nts>
                  <nts id="Seg_664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166.tx.1" id="Seg_666" n="HIAT:w" s="T166">Heliː</ts>
                  <nts id="Seg_667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_669" n="HIAT:w" s="T166.tx.1">Kura</ts>
                  <nts id="Seg_670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_672" n="HIAT:w" s="T167">uskaːn</ts>
                  <nts id="Seg_673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_675" n="HIAT:w" s="T168">bu͡olan</ts>
                  <nts id="Seg_676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_678" n="HIAT:w" s="T169">ojbut</ts>
                  <nts id="Seg_679" n="HIAT:ip">.</nts>
                  <nts id="Seg_680" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T174" id="Seg_682" n="HIAT:u" s="T170">
                  <ts e="T171" id="Seg_684" n="HIAT:w" s="T170">Kennikite</ts>
                  <nts id="Seg_685" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_687" n="HIAT:w" s="T171">uskaːn</ts>
                  <nts id="Seg_688" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_690" n="HIAT:w" s="T172">bu͡olan</ts>
                  <nts id="Seg_691" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_693" n="HIAT:w" s="T173">batta</ts>
                  <nts id="Seg_694" n="HIAT:ip">.</nts>
                  <nts id="Seg_695" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T178" id="Seg_697" n="HIAT:u" s="T174">
                  <ts e="T174.tx.1" id="Seg_699" n="HIAT:w" s="T174">Heliː</ts>
                  <nts id="Seg_700" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_702" n="HIAT:w" s="T174.tx.1">Kur</ts>
                  <nts id="Seg_703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_705" n="HIAT:w" s="T175">hahɨl</ts>
                  <nts id="Seg_706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_708" n="HIAT:w" s="T176">bu͡olla</ts>
                  <nts id="Seg_709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_711" n="HIAT:w" s="T177">onton</ts>
                  <nts id="Seg_712" n="HIAT:ip">.</nts>
                  <nts id="Seg_713" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T186" id="Seg_715" n="HIAT:u" s="T178">
                  <ts e="T179" id="Seg_717" n="HIAT:w" s="T178">Kennikite</ts>
                  <nts id="Seg_718" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_720" n="HIAT:w" s="T179">emi͡e</ts>
                  <nts id="Seg_721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_723" n="HIAT:w" s="T180">hahɨl</ts>
                  <nts id="Seg_724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_726" n="HIAT:w" s="T181">bu͡olan</ts>
                  <nts id="Seg_727" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_729" n="HIAT:w" s="T182">hu͡olun</ts>
                  <nts id="Seg_730" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_732" n="HIAT:w" s="T183">agaj</ts>
                  <nts id="Seg_733" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_735" n="HIAT:w" s="T184">ütükten</ts>
                  <nts id="Seg_736" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_738" n="HIAT:w" s="T185">iste</ts>
                  <nts id="Seg_739" n="HIAT:ip">.</nts>
                  <nts id="Seg_740" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T191" id="Seg_742" n="HIAT:u" s="T186">
                  <ts e="T187" id="Seg_744" n="HIAT:w" s="T186">Maːnɨta</ts>
                  <nts id="Seg_745" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_747" n="HIAT:w" s="T187">börö</ts>
                  <nts id="Seg_748" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_750" n="HIAT:w" s="T188">bu͡olan</ts>
                  <nts id="Seg_751" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_753" n="HIAT:w" s="T189">kötön</ts>
                  <nts id="Seg_754" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_756" n="HIAT:w" s="T190">nʼulujbut</ts>
                  <nts id="Seg_757" n="HIAT:ip">.</nts>
                  <nts id="Seg_758" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T194" id="Seg_760" n="HIAT:u" s="T191">
                  <ts e="T192" id="Seg_762" n="HIAT:w" s="T191">Kennikite</ts>
                  <nts id="Seg_763" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_765" n="HIAT:w" s="T192">högö</ts>
                  <nts id="Seg_766" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_768" n="HIAT:w" s="T193">iste</ts>
                  <nts id="Seg_769" n="HIAT:ip">:</nts>
                  <nts id="Seg_770" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T199" id="Seg_772" n="HIAT:u" s="T194">
                  <nts id="Seg_773" n="HIAT:ip">"</nts>
                  <ts e="T195" id="Seg_775" n="HIAT:w" s="T194">Mantɨkaja</ts>
                  <nts id="Seg_776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_778" n="HIAT:w" s="T195">batɨmɨna</ts>
                  <nts id="Seg_779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_781" n="HIAT:w" s="T196">kördömmüt</ts>
                  <nts id="Seg_782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_784" n="HIAT:w" s="T197">bu͡ollaga</ts>
                  <nts id="Seg_785" n="HIAT:ip">"</nts>
                  <nts id="Seg_786" n="HIAT:ip">,</nts>
                  <nts id="Seg_787" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_789" n="HIAT:w" s="T198">diːr</ts>
                  <nts id="Seg_790" n="HIAT:ip">.</nts>
                  <nts id="Seg_791" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T203" id="Seg_793" n="HIAT:u" s="T199">
                  <ts e="T200" id="Seg_795" n="HIAT:w" s="T199">Börö</ts>
                  <nts id="Seg_796" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_798" n="HIAT:w" s="T200">bu͡olan</ts>
                  <nts id="Seg_799" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_801" n="HIAT:w" s="T201">batan</ts>
                  <nts id="Seg_802" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_804" n="HIAT:w" s="T202">iste</ts>
                  <nts id="Seg_805" n="HIAT:ip">.</nts>
                  <nts id="Seg_806" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T206" id="Seg_808" n="HIAT:u" s="T203">
                  <ts e="T204" id="Seg_810" n="HIAT:w" s="T203">Töhönü-kaččanɨ</ts>
                  <nts id="Seg_811" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_813" n="HIAT:w" s="T204">barbɨta</ts>
                  <nts id="Seg_814" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_816" n="HIAT:w" s="T205">billibet</ts>
                  <nts id="Seg_817" n="HIAT:ip">.</nts>
                  <nts id="Seg_818" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T211" id="Seg_820" n="HIAT:u" s="T206">
                  <ts e="T206.tx.1" id="Seg_822" n="HIAT:w" s="T206">Heliː</ts>
                  <nts id="Seg_823" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_825" n="HIAT:w" s="T206.tx.1">Kur</ts>
                  <nts id="Seg_826" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_828" n="HIAT:w" s="T207">hi͡egen</ts>
                  <nts id="Seg_829" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_831" n="HIAT:w" s="T208">bu͡olan</ts>
                  <nts id="Seg_832" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_834" n="HIAT:w" s="T209">leppeje</ts>
                  <nts id="Seg_835" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_837" n="HIAT:w" s="T210">turbut</ts>
                  <nts id="Seg_838" n="HIAT:ip">.</nts>
                  <nts id="Seg_839" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T219" id="Seg_841" n="HIAT:u" s="T211">
                  <ts e="T212" id="Seg_843" n="HIAT:w" s="T211">Kennikite</ts>
                  <nts id="Seg_844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_846" n="HIAT:w" s="T212">emi͡e</ts>
                  <nts id="Seg_847" n="HIAT:ip">,</nts>
                  <nts id="Seg_848" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_850" n="HIAT:w" s="T213">bi͡ek</ts>
                  <nts id="Seg_851" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_853" n="HIAT:w" s="T214">högö-högö</ts>
                  <nts id="Seg_854" n="HIAT:ip">,</nts>
                  <nts id="Seg_855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_857" n="HIAT:w" s="T215">hi͡egen</ts>
                  <nts id="Seg_858" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_860" n="HIAT:w" s="T216">bu͡olan</ts>
                  <nts id="Seg_861" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_863" n="HIAT:w" s="T217">batan</ts>
                  <nts id="Seg_864" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_866" n="HIAT:w" s="T218">iste</ts>
                  <nts id="Seg_867" n="HIAT:ip">.</nts>
                  <nts id="Seg_868" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T225" id="Seg_870" n="HIAT:u" s="T219">
                  <ts e="T220" id="Seg_872" n="HIAT:w" s="T219">Hi͡egenin</ts>
                  <nts id="Seg_873" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_875" n="HIAT:w" s="T220">hu͡ola</ts>
                  <nts id="Seg_876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_878" n="HIAT:w" s="T221">büteːtin</ts>
                  <nts id="Seg_879" n="HIAT:ip">,</nts>
                  <nts id="Seg_880" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_882" n="HIAT:w" s="T222">ebekeː</ts>
                  <nts id="Seg_883" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_885" n="HIAT:w" s="T223">bu͡olan</ts>
                  <nts id="Seg_886" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_888" n="HIAT:w" s="T224">lippejbit</ts>
                  <nts id="Seg_889" n="HIAT:ip">.</nts>
                  <nts id="Seg_890" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T230" id="Seg_892" n="HIAT:u" s="T225">
                  <ts e="T226" id="Seg_894" n="HIAT:w" s="T225">Ehe</ts>
                  <nts id="Seg_895" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_897" n="HIAT:w" s="T226">bu͡olan</ts>
                  <nts id="Seg_898" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_900" n="HIAT:w" s="T227">batan</ts>
                  <nts id="Seg_901" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_903" n="HIAT:w" s="T228">iste</ts>
                  <nts id="Seg_904" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229.tx.1" id="Seg_906" n="HIAT:w" s="T229">Timir</ts>
                  <nts id="Seg_907" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_909" n="HIAT:w" s="T229.tx.1">Haːpka</ts>
                  <nts id="Seg_910" n="HIAT:ip">.</nts>
                  <nts id="Seg_911" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T235" id="Seg_913" n="HIAT:u" s="T230">
                  <ts e="T231" id="Seg_915" n="HIAT:w" s="T230">Bu</ts>
                  <nts id="Seg_916" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_918" n="HIAT:w" s="T231">tukkarɨ</ts>
                  <nts id="Seg_919" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_921" n="HIAT:w" s="T232">huk</ts>
                  <nts id="Seg_922" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_924" n="HIAT:w" s="T233">kötüː</ts>
                  <nts id="Seg_925" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_927" n="HIAT:w" s="T234">ajan</ts>
                  <nts id="Seg_928" n="HIAT:ip">.</nts>
                  <nts id="Seg_929" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T244" id="Seg_931" n="HIAT:u" s="T235">
                  <ts e="T236" id="Seg_933" n="HIAT:w" s="T235">Ehe</ts>
                  <nts id="Seg_934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_936" n="HIAT:w" s="T236">hu͡ola</ts>
                  <nts id="Seg_937" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_939" n="HIAT:w" s="T237">baranar</ts>
                  <nts id="Seg_940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_942" n="HIAT:w" s="T238">da</ts>
                  <nts id="Seg_943" n="HIAT:ip">,</nts>
                  <nts id="Seg_944" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_946" n="HIAT:w" s="T239">kɨːl</ts>
                  <nts id="Seg_947" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_949" n="HIAT:w" s="T240">atɨːra</ts>
                  <nts id="Seg_950" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_952" n="HIAT:w" s="T241">bu͡olan</ts>
                  <nts id="Seg_953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_955" n="HIAT:w" s="T242">ojon</ts>
                  <nts id="Seg_956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_958" n="HIAT:w" s="T243">kaːlaːktaːbɨt</ts>
                  <nts id="Seg_959" n="HIAT:ip">.</nts>
                  <nts id="Seg_960" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T250" id="Seg_962" n="HIAT:u" s="T244">
                  <ts e="T245" id="Seg_964" n="HIAT:w" s="T244">Högö</ts>
                  <nts id="Seg_965" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_967" n="HIAT:w" s="T245">agaj</ts>
                  <nts id="Seg_968" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_970" n="HIAT:w" s="T246">iher</ts>
                  <nts id="Seg_971" n="HIAT:ip">,</nts>
                  <nts id="Seg_972" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_974" n="HIAT:w" s="T247">bi͡ek</ts>
                  <nts id="Seg_975" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_977" n="HIAT:w" s="T248">batar</ts>
                  <nts id="Seg_978" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_980" n="HIAT:w" s="T249">dogoro</ts>
                  <nts id="Seg_981" n="HIAT:ip">.</nts>
                  <nts id="Seg_982" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T254" id="Seg_984" n="HIAT:u" s="T250">
                  <ts e="T250.tx.1" id="Seg_986" n="HIAT:w" s="T250">Timir</ts>
                  <nts id="Seg_987" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_989" n="HIAT:w" s="T250.tx.1">Haːpka</ts>
                  <nts id="Seg_990" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_992" n="HIAT:w" s="T251">kɨːl</ts>
                  <nts id="Seg_993" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_995" n="HIAT:w" s="T252">atɨːra</ts>
                  <nts id="Seg_996" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_998" n="HIAT:w" s="T253">bu͡olla</ts>
                  <nts id="Seg_999" n="HIAT:ip">.</nts>
                  <nts id="Seg_1000" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T263" id="Seg_1002" n="HIAT:u" s="T254">
                  <nts id="Seg_1003" n="HIAT:ip">"</nts>
                  <ts e="T255" id="Seg_1005" n="HIAT:w" s="T254">Ideŋ</ts>
                  <nts id="Seg_1006" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_1008" n="HIAT:w" s="T255">da</ts>
                  <nts id="Seg_1009" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_1011" n="HIAT:w" s="T256">elete</ts>
                  <nts id="Seg_1012" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_1014" n="HIAT:w" s="T257">ini</ts>
                  <nts id="Seg_1015" n="HIAT:ip">,</nts>
                  <nts id="Seg_1016" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_1018" n="HIAT:w" s="T258">hiteː</ts>
                  <nts id="Seg_1019" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_1021" n="HIAT:w" s="T259">inibin</ts>
                  <nts id="Seg_1022" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_1024" n="HIAT:w" s="T260">hubukaːn</ts>
                  <nts id="Seg_1025" n="HIAT:ip">"</nts>
                  <nts id="Seg_1026" n="HIAT:ip">,</nts>
                  <nts id="Seg_1027" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_1029" n="HIAT:w" s="T261">diː</ts>
                  <nts id="Seg_1030" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_1032" n="HIAT:w" s="T262">hanɨːr</ts>
                  <nts id="Seg_1033" n="HIAT:ip">.</nts>
                  <nts id="Seg_1034" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T269" id="Seg_1036" n="HIAT:u" s="T263">
                  <ts e="T264" id="Seg_1038" n="HIAT:w" s="T263">Körbüte</ts>
                  <nts id="Seg_1039" n="HIAT:ip">,</nts>
                  <nts id="Seg_1040" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_1042" n="HIAT:w" s="T264">biːr</ts>
                  <nts id="Seg_1043" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_1045" n="HIAT:w" s="T265">hirge</ts>
                  <nts id="Seg_1046" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_1048" n="HIAT:w" s="T266">dogoro</ts>
                  <nts id="Seg_1049" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_1051" n="HIAT:w" s="T267">balčajan</ts>
                  <nts id="Seg_1052" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_1054" n="HIAT:w" s="T268">turar</ts>
                  <nts id="Seg_1055" n="HIAT:ip">.</nts>
                  <nts id="Seg_1056" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T282" id="Seg_1058" n="HIAT:u" s="T269">
                  <ts e="T270" id="Seg_1060" n="HIAT:w" s="T269">Čugahaːn</ts>
                  <nts id="Seg_1061" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_1063" n="HIAT:w" s="T270">körbüte</ts>
                  <nts id="Seg_1064" n="HIAT:ip">,</nts>
                  <nts id="Seg_1065" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_1067" n="HIAT:w" s="T271">ikki</ts>
                  <nts id="Seg_1068" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_1070" n="HIAT:w" s="T272">agaj</ts>
                  <nts id="Seg_1071" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_1073" n="HIAT:w" s="T273">ataga</ts>
                  <nts id="Seg_1074" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_1076" n="HIAT:w" s="T274">čočojon</ts>
                  <nts id="Seg_1077" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_1079" n="HIAT:w" s="T275">kaːlbɨt</ts>
                  <nts id="Seg_1080" n="HIAT:ip">,</nts>
                  <nts id="Seg_1081" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_1083" n="HIAT:w" s="T276">hühü͡ökterinen</ts>
                  <nts id="Seg_1084" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_1086" n="HIAT:w" s="T277">bɨhan</ts>
                  <nts id="Seg_1087" n="HIAT:ip">,</nts>
                  <nts id="Seg_1088" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_1090" n="HIAT:w" s="T278">tuːsa</ts>
                  <nts id="Seg_1091" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_1093" n="HIAT:w" s="T279">ataktarɨn</ts>
                  <nts id="Seg_1094" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_1096" n="HIAT:w" s="T280">čekeliten</ts>
                  <nts id="Seg_1097" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_1099" n="HIAT:w" s="T281">keːspit</ts>
                  <nts id="Seg_1100" n="HIAT:ip">.</nts>
                  <nts id="Seg_1101" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T291" id="Seg_1103" n="HIAT:u" s="T282">
                  <ts e="T283" id="Seg_1105" n="HIAT:w" s="T282">Hol</ts>
                  <nts id="Seg_1106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_1108" n="HIAT:w" s="T283">kordukkaːn</ts>
                  <nts id="Seg_1109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284.tx.1" id="Seg_1111" n="HIAT:w" s="T284">Timir</ts>
                  <nts id="Seg_1112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_1114" n="HIAT:w" s="T284.tx.1">Haːpka</ts>
                  <nts id="Seg_1115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_1117" n="HIAT:w" s="T285">atagɨn</ts>
                  <nts id="Seg_1118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_1120" n="HIAT:w" s="T286">bɨhaːt</ts>
                  <nts id="Seg_1121" n="HIAT:ip">,</nts>
                  <nts id="Seg_1122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_1124" n="HIAT:w" s="T287">tuːsa</ts>
                  <nts id="Seg_1125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_1127" n="HIAT:w" s="T288">bu͡olan</ts>
                  <nts id="Seg_1128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_1130" n="HIAT:w" s="T289">čekelijen</ts>
                  <nts id="Seg_1131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_1133" n="HIAT:w" s="T290">iste</ts>
                  <nts id="Seg_1134" n="HIAT:ip">.</nts>
                  <nts id="Seg_1135" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T297" id="Seg_1137" n="HIAT:u" s="T291">
                  <ts e="T292" id="Seg_1139" n="HIAT:w" s="T291">Töhö</ts>
                  <nts id="Seg_1140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_1142" n="HIAT:w" s="T292">da</ts>
                  <nts id="Seg_1143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_1145" n="HIAT:w" s="T293">bu͡olumuja</ts>
                  <nts id="Seg_1146" n="HIAT:ip">,</nts>
                  <nts id="Seg_1147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1149" n="HIAT:w" s="T294">tuːsa</ts>
                  <nts id="Seg_1150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1152" n="HIAT:w" s="T295">köppöjö</ts>
                  <nts id="Seg_1153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_1155" n="HIAT:w" s="T296">hɨtar</ts>
                  <nts id="Seg_1156" n="HIAT:ip">.</nts>
                  <nts id="Seg_1157" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T301" id="Seg_1159" n="HIAT:u" s="T297">
                  <ts e="T298" id="Seg_1161" n="HIAT:w" s="T297">Ü͡örer</ts>
                  <nts id="Seg_1162" n="HIAT:ip">,</nts>
                  <nts id="Seg_1163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1164" n="HIAT:ip">"</nts>
                  <ts e="T299" id="Seg_1166" n="HIAT:w" s="T298">hittim</ts>
                  <nts id="Seg_1167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_1169" n="HIAT:w" s="T299">eː</ts>
                  <nts id="Seg_1170" n="HIAT:ip">"</nts>
                  <nts id="Seg_1171" n="HIAT:ip">,</nts>
                  <nts id="Seg_1172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_1174" n="HIAT:w" s="T300">diːr</ts>
                  <nts id="Seg_1175" n="HIAT:ip">.</nts>
                  <nts id="Seg_1176" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T312" id="Seg_1178" n="HIAT:u" s="T301">
                  <ts e="T302" id="Seg_1180" n="HIAT:w" s="T301">Bu</ts>
                  <nts id="Seg_1181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1183" n="HIAT:w" s="T302">tuːsaga</ts>
                  <nts id="Seg_1184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_1186" n="HIAT:w" s="T303">tiːjbite</ts>
                  <nts id="Seg_1187" n="HIAT:ip">,</nts>
                  <nts id="Seg_1188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_1190" n="HIAT:w" s="T304">tuːsa</ts>
                  <nts id="Seg_1191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_1193" n="HIAT:w" s="T305">postu͡oj</ts>
                  <nts id="Seg_1194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_1196" n="HIAT:w" s="T306">agaj</ts>
                  <nts id="Seg_1197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_1199" n="HIAT:w" s="T307">hɨtar</ts>
                  <nts id="Seg_1200" n="HIAT:ip">,</nts>
                  <nts id="Seg_1201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1203" n="HIAT:w" s="T308">mejiːte</ts>
                  <nts id="Seg_1204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_1206" n="HIAT:w" s="T309">agaj</ts>
                  <nts id="Seg_1207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_1209" n="HIAT:w" s="T310">čekelijen</ts>
                  <nts id="Seg_1210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_1212" n="HIAT:w" s="T311">kaːlbɨt</ts>
                  <nts id="Seg_1213" n="HIAT:ip">.</nts>
                  <nts id="Seg_1214" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T320" id="Seg_1216" n="HIAT:u" s="T312">
                  <nts id="Seg_1217" n="HIAT:ip">"</nts>
                  <ts e="T313" id="Seg_1219" n="HIAT:w" s="T312">Kaja</ts>
                  <nts id="Seg_1220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_1222" n="HIAT:w" s="T313">doː</ts>
                  <nts id="Seg_1223" n="HIAT:ip">,</nts>
                  <nts id="Seg_1224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_1226" n="HIAT:w" s="T314">bu</ts>
                  <nts id="Seg_1227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1229" n="HIAT:w" s="T315">togo</ts>
                  <nts id="Seg_1230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_1232" n="HIAT:w" s="T316">kerdiner</ts>
                  <nts id="Seg_1233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_1235" n="HIAT:w" s="T317">bejetin</ts>
                  <nts id="Seg_1236" n="HIAT:ip">"</nts>
                  <nts id="Seg_1237" n="HIAT:ip">,</nts>
                  <nts id="Seg_1238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1240" n="HIAT:w" s="T318">di͡en</ts>
                  <nts id="Seg_1241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_1243" n="HIAT:w" s="T319">hanɨːr</ts>
                  <nts id="Seg_1244" n="HIAT:ip">.</nts>
                  <nts id="Seg_1245" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T326" id="Seg_1247" n="HIAT:u" s="T320">
                  <nts id="Seg_1248" n="HIAT:ip">"</nts>
                  <ts e="T321" id="Seg_1250" n="HIAT:w" s="T320">Bugurdukkaːn</ts>
                  <nts id="Seg_1251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_1253" n="HIAT:w" s="T321">tu͡ok</ts>
                  <nts id="Seg_1254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1256" n="HIAT:w" s="T322">mökkü͡önej</ts>
                  <nts id="Seg_1257" n="HIAT:ip">"</nts>
                  <nts id="Seg_1258" n="HIAT:ip">,</nts>
                  <nts id="Seg_1259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_1261" n="HIAT:w" s="T323">di͡en</ts>
                  <nts id="Seg_1262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1264" n="HIAT:w" s="T324">oduːrguː</ts>
                  <nts id="Seg_1265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_1267" n="HIAT:w" s="T325">iste</ts>
                  <nts id="Seg_1268" n="HIAT:ip">.</nts>
                  <nts id="Seg_1269" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T330" id="Seg_1271" n="HIAT:u" s="T326">
                  <ts e="T327" id="Seg_1273" n="HIAT:w" s="T326">Mejiː</ts>
                  <nts id="Seg_1274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_1276" n="HIAT:w" s="T327">bu͡olan</ts>
                  <nts id="Seg_1277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_1279" n="HIAT:w" s="T328">čekelijen</ts>
                  <nts id="Seg_1280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_1282" n="HIAT:w" s="T329">iste</ts>
                  <nts id="Seg_1283" n="HIAT:ip">.</nts>
                  <nts id="Seg_1284" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T334" id="Seg_1286" n="HIAT:u" s="T330">
                  <ts e="T331" id="Seg_1288" n="HIAT:w" s="T330">Araj</ts>
                  <nts id="Seg_1289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_1291" n="HIAT:w" s="T331">mejiː</ts>
                  <nts id="Seg_1292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_1294" n="HIAT:w" s="T332">čankaja</ts>
                  <nts id="Seg_1295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_1297" n="HIAT:w" s="T333">hɨtar</ts>
                  <nts id="Seg_1298" n="HIAT:ip">.</nts>
                  <nts id="Seg_1299" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T337" id="Seg_1301" n="HIAT:u" s="T334">
                  <nts id="Seg_1302" n="HIAT:ip">"</nts>
                  <ts e="T335" id="Seg_1304" n="HIAT:w" s="T334">Dʼe</ts>
                  <nts id="Seg_1305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_1307" n="HIAT:w" s="T335">hittim</ts>
                  <nts id="Seg_1308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_1310" n="HIAT:w" s="T336">ini</ts>
                  <nts id="Seg_1311" n="HIAT:ip">!</nts>
                  <nts id="Seg_1312" n="HIAT:ip">"</nts>
                  <nts id="Seg_1313" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T342" id="Seg_1315" n="HIAT:u" s="T337">
                  <ts e="T338" id="Seg_1317" n="HIAT:w" s="T337">Araj</ts>
                  <nts id="Seg_1318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_1320" n="HIAT:w" s="T338">ikki</ts>
                  <nts id="Seg_1321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_1323" n="HIAT:w" s="T339">karaga</ts>
                  <nts id="Seg_1324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_1326" n="HIAT:w" s="T340">uhullan</ts>
                  <nts id="Seg_1327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_1329" n="HIAT:w" s="T341">barbɨt</ts>
                  <nts id="Seg_1330" n="HIAT:ip">.</nts>
                  <nts id="Seg_1331" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T346" id="Seg_1333" n="HIAT:u" s="T342">
                  <nts id="Seg_1334" n="HIAT:ip">"</nts>
                  <ts e="T343" id="Seg_1336" n="HIAT:w" s="T342">Oroː</ts>
                  <nts id="Seg_1337" n="HIAT:ip">,</nts>
                  <nts id="Seg_1338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_1340" n="HIAT:w" s="T343">bu</ts>
                  <nts id="Seg_1341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_1343" n="HIAT:w" s="T344">tu͡ok</ts>
                  <nts id="Seg_1344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_1346" n="HIAT:w" s="T345">kihitej</ts>
                  <nts id="Seg_1347" n="HIAT:ip">?</nts>
                  <nts id="Seg_1348" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T352" id="Seg_1350" n="HIAT:u" s="T346">
                  <ts e="T347" id="Seg_1352" n="HIAT:w" s="T346">Ajɨːnɨ</ts>
                  <nts id="Seg_1353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1355" n="HIAT:w" s="T347">batabɨn</ts>
                  <nts id="Seg_1356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_1358" n="HIAT:w" s="T348">duː</ts>
                  <nts id="Seg_1359" n="HIAT:ip">,</nts>
                  <nts id="Seg_1360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_1362" n="HIAT:w" s="T349">abaːhɨnɨ</ts>
                  <nts id="Seg_1363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_1365" n="HIAT:w" s="T350">duː</ts>
                  <nts id="Seg_1366" n="HIAT:ip">"</nts>
                  <nts id="Seg_1367" n="HIAT:ip">,</nts>
                  <nts id="Seg_1368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_1370" n="HIAT:w" s="T351">diːr</ts>
                  <nts id="Seg_1371" n="HIAT:ip">.</nts>
                  <nts id="Seg_1372" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T358" id="Seg_1374" n="HIAT:u" s="T352">
                  <ts e="T353" id="Seg_1376" n="HIAT:w" s="T352">Ikki</ts>
                  <nts id="Seg_1377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_1379" n="HIAT:w" s="T353">karaga</ts>
                  <nts id="Seg_1380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_1382" n="HIAT:w" s="T354">čekerijen</ts>
                  <nts id="Seg_1383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_1385" n="HIAT:w" s="T355">iste</ts>
                  <nts id="Seg_1386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_1388" n="HIAT:w" s="T356">karaktar</ts>
                  <nts id="Seg_1389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_1391" n="HIAT:w" s="T357">hu͡ollarɨnan</ts>
                  <nts id="Seg_1392" n="HIAT:ip">.</nts>
                  <nts id="Seg_1393" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T367" id="Seg_1395" n="HIAT:u" s="T358">
                  <ts e="T359" id="Seg_1397" n="HIAT:w" s="T358">Kahan</ts>
                  <nts id="Seg_1398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_1400" n="HIAT:w" s="T359">ere</ts>
                  <nts id="Seg_1401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_1403" n="HIAT:w" s="T360">bu</ts>
                  <nts id="Seg_1404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_1406" n="HIAT:w" s="T361">ikki</ts>
                  <nts id="Seg_1407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_1409" n="HIAT:w" s="T362">karak</ts>
                  <nts id="Seg_1410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_1412" n="HIAT:w" s="T363">hu͡ola</ts>
                  <nts id="Seg_1413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_1415" n="HIAT:w" s="T364">biːr</ts>
                  <nts id="Seg_1416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_1418" n="HIAT:w" s="T365">dʼi͡ekeːŋŋe</ts>
                  <nts id="Seg_1419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_1421" n="HIAT:w" s="T366">tijer</ts>
                  <nts id="Seg_1422" n="HIAT:ip">.</nts>
                  <nts id="Seg_1423" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T374" id="Seg_1425" n="HIAT:u" s="T367">
                  <ts e="T368" id="Seg_1427" n="HIAT:w" s="T367">Bu</ts>
                  <nts id="Seg_1428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_1430" n="HIAT:w" s="T368">tijen</ts>
                  <nts id="Seg_1431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_1433" n="HIAT:w" s="T369">ikki</ts>
                  <nts id="Seg_1434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_1436" n="HIAT:w" s="T370">karak</ts>
                  <nts id="Seg_1437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_1439" n="HIAT:w" s="T371">hu͡ola</ts>
                  <nts id="Seg_1440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_1442" n="HIAT:w" s="T372">dʼi͡ege</ts>
                  <nts id="Seg_1443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_1445" n="HIAT:w" s="T373">kiːrbit</ts>
                  <nts id="Seg_1446" n="HIAT:ip">.</nts>
                  <nts id="Seg_1447" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T377" id="Seg_1449" n="HIAT:u" s="T374">
                  <nts id="Seg_1450" n="HIAT:ip">"</nts>
                  <ts e="T375" id="Seg_1452" n="HIAT:w" s="T374">Dʼe</ts>
                  <nts id="Seg_1453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_1455" n="HIAT:w" s="T375">eleŋ</ts>
                  <nts id="Seg_1456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_1458" n="HIAT:w" s="T376">ini</ts>
                  <nts id="Seg_1459" n="HIAT:ip">!</nts>
                  <nts id="Seg_1460" n="HIAT:ip">"</nts>
                  <nts id="Seg_1461" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T382" id="Seg_1463" n="HIAT:u" s="T377">
                  <ts e="T378" id="Seg_1465" n="HIAT:w" s="T377">Ikki</ts>
                  <nts id="Seg_1466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_1468" n="HIAT:w" s="T378">kelin</ts>
                  <nts id="Seg_1469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_1471" n="HIAT:w" s="T379">karak</ts>
                  <nts id="Seg_1472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_1474" n="HIAT:w" s="T380">dʼi͡ege</ts>
                  <nts id="Seg_1475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T382" id="Seg_1477" n="HIAT:w" s="T381">kiːrde</ts>
                  <nts id="Seg_1478" n="HIAT:ip">.</nts>
                  <nts id="Seg_1479" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T392" id="Seg_1481" n="HIAT:u" s="T382">
                  <ts e="T383" id="Seg_1483" n="HIAT:w" s="T382">Kiːrbite</ts>
                  <nts id="Seg_1484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_1486" n="HIAT:w" s="T383">uŋa</ts>
                  <nts id="Seg_1487" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_1489" n="HIAT:w" s="T384">di͡ekki</ts>
                  <nts id="Seg_1490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T386" id="Seg_1492" n="HIAT:w" s="T385">oroŋŋo</ts>
                  <nts id="Seg_1493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_1495" n="HIAT:w" s="T386">emeːksin</ts>
                  <nts id="Seg_1496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_1498" n="HIAT:w" s="T387">oloror</ts>
                  <nts id="Seg_1499" n="HIAT:ip">,</nts>
                  <nts id="Seg_1500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_1502" n="HIAT:w" s="T388">karaktara</ts>
                  <nts id="Seg_1503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_1505" n="HIAT:w" s="T389">ketegeriːn</ts>
                  <nts id="Seg_1506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T391" id="Seg_1508" n="HIAT:w" s="T390">čagɨlɨha</ts>
                  <nts id="Seg_1509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_1511" n="HIAT:w" s="T391">hɨtallar</ts>
                  <nts id="Seg_1512" n="HIAT:ip">.</nts>
                  <nts id="Seg_1513" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T400" id="Seg_1515" n="HIAT:u" s="T392">
                  <ts e="T393" id="Seg_1517" n="HIAT:w" s="T392">Bili</ts>
                  <nts id="Seg_1518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_1520" n="HIAT:w" s="T393">ikki</ts>
                  <nts id="Seg_1521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_1523" n="HIAT:w" s="T394">karak</ts>
                  <nts id="Seg_1524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T396" id="Seg_1526" n="HIAT:w" s="T395">kiːrbitiger</ts>
                  <nts id="Seg_1527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_1529" n="HIAT:w" s="T396">inniki</ts>
                  <nts id="Seg_1530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_1532" n="HIAT:w" s="T397">ikki</ts>
                  <nts id="Seg_1533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_1535" n="HIAT:w" s="T398">karak</ts>
                  <nts id="Seg_1536" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T400" id="Seg_1538" n="HIAT:w" s="T399">hanalaːk</ts>
                  <nts id="Seg_1539" n="HIAT:ip">:</nts>
                  <nts id="Seg_1540" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T406" id="Seg_1542" n="HIAT:u" s="T400">
                  <nts id="Seg_1543" n="HIAT:ip">"</nts>
                  <ts e="T401" id="Seg_1545" n="HIAT:w" s="T400">O</ts>
                  <nts id="Seg_1546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T402" id="Seg_1548" n="HIAT:w" s="T401">da</ts>
                  <nts id="Seg_1549" n="HIAT:ip">,</nts>
                  <nts id="Seg_1550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_1552" n="HIAT:w" s="T402">kihini</ts>
                  <nts id="Seg_1553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T404" id="Seg_1555" n="HIAT:w" s="T403">batar</ts>
                  <nts id="Seg_1556" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T405" id="Seg_1558" n="HIAT:w" s="T404">kihi</ts>
                  <nts id="Seg_1559" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_1561" n="HIAT:w" s="T405">hɨlajaːččɨ</ts>
                  <nts id="Seg_1562" n="HIAT:ip">.</nts>
                  <nts id="Seg_1563" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T408" id="Seg_1565" n="HIAT:u" s="T406">
                  <ts e="T407" id="Seg_1567" n="HIAT:w" s="T406">En</ts>
                  <nts id="Seg_1568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T408" id="Seg_1570" n="HIAT:w" s="T407">hɨnnʼan</ts>
                  <nts id="Seg_1571" n="HIAT:ip">.</nts>
                  <nts id="Seg_1572" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T414" id="Seg_1574" n="HIAT:u" s="T408">
                  <ts e="T409" id="Seg_1576" n="HIAT:w" s="T408">Min</ts>
                  <nts id="Seg_1577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_1579" n="HIAT:w" s="T409">tuːsalarbɨtɨn</ts>
                  <nts id="Seg_1580" n="HIAT:ip">,</nts>
                  <nts id="Seg_1581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T411" id="Seg_1583" n="HIAT:w" s="T410">mejiːlerbitin</ts>
                  <nts id="Seg_1584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T412" id="Seg_1586" n="HIAT:w" s="T411">komujtaːn</ts>
                  <nts id="Seg_1587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T413" id="Seg_1589" n="HIAT:w" s="T412">egeli͡em</ts>
                  <nts id="Seg_1590" n="HIAT:ip">"</nts>
                  <nts id="Seg_1591" n="HIAT:ip">,</nts>
                  <nts id="Seg_1592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414" id="Seg_1594" n="HIAT:w" s="T413">diːr</ts>
                  <nts id="Seg_1595" n="HIAT:ip">.</nts>
                  <nts id="Seg_1596" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T417" id="Seg_1598" n="HIAT:u" s="T414">
                  <ts e="T415" id="Seg_1600" n="HIAT:w" s="T414">Dogoro</ts>
                  <nts id="Seg_1601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T415.tx.1" id="Seg_1603" n="HIAT:w" s="T415">Timir</ts>
                  <nts id="Seg_1604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T416" id="Seg_1606" n="HIAT:w" s="T415.tx.1">Haːpka</ts>
                  <nts id="Seg_1607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T417" id="Seg_1609" n="HIAT:w" s="T416">onuga</ts>
                  <nts id="Seg_1610" n="HIAT:ip">:</nts>
                  <nts id="Seg_1611" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T425" id="Seg_1613" n="HIAT:u" s="T417">
                  <nts id="Seg_1614" n="HIAT:ip">"</nts>
                  <ts e="T418" id="Seg_1616" n="HIAT:w" s="T417">Bu</ts>
                  <nts id="Seg_1617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T419" id="Seg_1619" n="HIAT:w" s="T418">ile</ts>
                  <nts id="Seg_1620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T420" id="Seg_1622" n="HIAT:w" s="T419">öjünen</ts>
                  <nts id="Seg_1623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_1625" n="HIAT:w" s="T420">hɨldʼar</ts>
                  <nts id="Seg_1626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T422" id="Seg_1628" n="HIAT:w" s="T421">kihigin</ts>
                  <nts id="Seg_1629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T423" id="Seg_1631" n="HIAT:w" s="T422">duː</ts>
                  <nts id="Seg_1632" n="HIAT:ip">,</nts>
                  <nts id="Seg_1633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T424" id="Seg_1635" n="HIAT:w" s="T423">hu͡ok</ts>
                  <nts id="Seg_1636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T425" id="Seg_1638" n="HIAT:w" s="T424">duː</ts>
                  <nts id="Seg_1639" n="HIAT:ip">?</nts>
                  <nts id="Seg_1640" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T432" id="Seg_1642" n="HIAT:u" s="T425">
                  <ts e="T426" id="Seg_1644" n="HIAT:w" s="T425">Bu</ts>
                  <nts id="Seg_1645" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T427" id="Seg_1647" n="HIAT:w" s="T426">bihigi</ts>
                  <nts id="Seg_1648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T428" id="Seg_1650" n="HIAT:w" s="T427">kihi</ts>
                  <nts id="Seg_1651" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T429" id="Seg_1653" n="HIAT:w" s="T428">kɨ͡ajan</ts>
                  <nts id="Seg_1654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_1656" n="HIAT:w" s="T429">tönnübet</ts>
                  <nts id="Seg_1657" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T431" id="Seg_1659" n="HIAT:w" s="T430">hiriger</ts>
                  <nts id="Seg_1660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_1662" n="HIAT:w" s="T431">kellibit</ts>
                  <nts id="Seg_1663" n="HIAT:ip">!</nts>
                  <nts id="Seg_1664" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T440" id="Seg_1666" n="HIAT:u" s="T432">
                  <ts e="T433" id="Seg_1668" n="HIAT:w" s="T432">Dʼe</ts>
                  <nts id="Seg_1669" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T434" id="Seg_1671" n="HIAT:w" s="T433">anɨ</ts>
                  <nts id="Seg_1672" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T435" id="Seg_1674" n="HIAT:w" s="T434">en</ts>
                  <nts id="Seg_1675" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_1677" n="HIAT:w" s="T435">bataːr</ts>
                  <nts id="Seg_1678" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T437" id="Seg_1680" n="HIAT:w" s="T436">minʼigin</ts>
                  <nts id="Seg_1681" n="HIAT:ip">,</nts>
                  <nts id="Seg_1682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T438" id="Seg_1684" n="HIAT:w" s="T437">hiteːr</ts>
                  <nts id="Seg_1685" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T439" id="Seg_1687" n="HIAT:w" s="T438">min</ts>
                  <nts id="Seg_1688" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440" id="Seg_1690" n="HIAT:w" s="T439">idebin</ts>
                  <nts id="Seg_1691" n="HIAT:ip">.</nts>
                  <nts id="Seg_1692" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T445" id="Seg_1694" n="HIAT:u" s="T440">
                  <ts e="T441" id="Seg_1696" n="HIAT:w" s="T440">Bilegi͡en</ts>
                  <nts id="Seg_1697" n="HIAT:ip">,</nts>
                  <nts id="Seg_1698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T442" id="Seg_1700" n="HIAT:w" s="T441">bu</ts>
                  <nts id="Seg_1701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T443" id="Seg_1703" n="HIAT:w" s="T442">Purgaː</ts>
                  <nts id="Seg_1704" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T444" id="Seg_1706" n="HIAT:w" s="T443">iččitiger</ts>
                  <nts id="Seg_1707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T445" id="Seg_1709" n="HIAT:w" s="T444">kellibit</ts>
                  <nts id="Seg_1710" n="HIAT:ip">.</nts>
                  <nts id="Seg_1711" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T452" id="Seg_1713" n="HIAT:u" s="T445">
                  <ts e="T446" id="Seg_1715" n="HIAT:w" s="T445">Harsi͡erda</ts>
                  <nts id="Seg_1716" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T447" id="Seg_1718" n="HIAT:w" s="T446">erdehit</ts>
                  <nts id="Seg_1719" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T448" id="Seg_1721" n="HIAT:w" s="T447">kihi</ts>
                  <nts id="Seg_1722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T449" id="Seg_1724" n="HIAT:w" s="T448">turar</ts>
                  <nts id="Seg_1725" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T450" id="Seg_1727" n="HIAT:w" s="T449">kemiger</ts>
                  <nts id="Seg_1728" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T451" id="Seg_1730" n="HIAT:w" s="T450">turan</ts>
                  <nts id="Seg_1731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T452" id="Seg_1733" n="HIAT:w" s="T451">taksɨ͡am</ts>
                  <nts id="Seg_1734" n="HIAT:ip">.</nts>
                  <nts id="Seg_1735" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T459" id="Seg_1737" n="HIAT:u" s="T452">
                  <ts e="T453" id="Seg_1739" n="HIAT:w" s="T452">Manɨga</ts>
                  <nts id="Seg_1740" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T454" id="Seg_1742" n="HIAT:w" s="T453">hüːstüː</ts>
                  <nts id="Seg_1743" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T455" id="Seg_1745" n="HIAT:w" s="T454">ölgöbüːnneːk</ts>
                  <nts id="Seg_1746" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T456" id="Seg_1748" n="HIAT:w" s="T455">hetiːleːk</ts>
                  <nts id="Seg_1749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T457" id="Seg_1751" n="HIAT:w" s="T456">ikki</ts>
                  <nts id="Seg_1752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T458" id="Seg_1754" n="HIAT:w" s="T457">kɨrgɨttar</ts>
                  <nts id="Seg_1755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T459" id="Seg_1757" n="HIAT:w" s="T458">keli͡ektere</ts>
                  <nts id="Seg_1758" n="HIAT:ip">.</nts>
                  <nts id="Seg_1759" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T478" id="Seg_1761" n="HIAT:u" s="T459">
                  <ts e="T460" id="Seg_1763" n="HIAT:w" s="T459">Manɨga</ts>
                  <nts id="Seg_1764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T461" id="Seg_1766" n="HIAT:w" s="T460">min</ts>
                  <nts id="Seg_1767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T462" id="Seg_1769" n="HIAT:w" s="T461">taksa</ts>
                  <nts id="Seg_1770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T463" id="Seg_1772" n="HIAT:w" s="T462">kötömmün</ts>
                  <nts id="Seg_1773" n="HIAT:ip">,</nts>
                  <nts id="Seg_1774" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T464" id="Seg_1776" n="HIAT:w" s="T463">bastakɨ</ts>
                  <nts id="Seg_1777" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T465" id="Seg_1779" n="HIAT:w" s="T464">kɨːhɨ</ts>
                  <nts id="Seg_1780" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T466" id="Seg_1782" n="HIAT:w" s="T465">nʼu͡oguhutun</ts>
                  <nts id="Seg_1783" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T467" id="Seg_1785" n="HIAT:w" s="T466">u͡oluguttan</ts>
                  <nts id="Seg_1786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T468" id="Seg_1788" n="HIAT:w" s="T467">tutu͡om</ts>
                  <nts id="Seg_1789" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T469" id="Seg_1791" n="HIAT:w" s="T468">da</ts>
                  <nts id="Seg_1792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T470" id="Seg_1794" n="HIAT:w" s="T469">kötütü͡öm</ts>
                  <nts id="Seg_1795" n="HIAT:ip">,</nts>
                  <nts id="Seg_1796" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T471" id="Seg_1798" n="HIAT:w" s="T470">bu</ts>
                  <nts id="Seg_1799" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T472" id="Seg_1801" n="HIAT:w" s="T471">hüːs</ts>
                  <nts id="Seg_1802" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T473" id="Seg_1804" n="HIAT:w" s="T472">hetiːtten</ts>
                  <nts id="Seg_1805" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T474" id="Seg_1807" n="HIAT:w" s="T473">biːriger</ts>
                  <nts id="Seg_1808" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T475" id="Seg_1810" n="HIAT:w" s="T474">eme</ts>
                  <nts id="Seg_1811" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T476" id="Seg_1813" n="HIAT:w" s="T475">katannakkɨna</ts>
                  <nts id="Seg_1814" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T477" id="Seg_1816" n="HIAT:w" s="T476">tijsi͡eŋ</ts>
                  <nts id="Seg_1817" n="HIAT:ip">,</nts>
                  <nts id="Seg_1818" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T478" id="Seg_1820" n="HIAT:w" s="T477">dojdugar</ts>
                  <nts id="Seg_1821" n="HIAT:ip">.</nts>
                  <nts id="Seg_1822" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T484" id="Seg_1824" n="HIAT:u" s="T478">
                  <ts e="T479" id="Seg_1826" n="HIAT:w" s="T478">Otton</ts>
                  <nts id="Seg_1827" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T480" id="Seg_1829" n="HIAT:w" s="T479">katammatakkɨna</ts>
                  <nts id="Seg_1830" n="HIAT:ip">,</nts>
                  <nts id="Seg_1831" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T481" id="Seg_1833" n="HIAT:w" s="T480">bu</ts>
                  <nts id="Seg_1834" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T482" id="Seg_1836" n="HIAT:w" s="T481">dojduga</ts>
                  <nts id="Seg_1837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T483" id="Seg_1839" n="HIAT:w" s="T482">ölü͡öŋ</ts>
                  <nts id="Seg_1840" n="HIAT:ip">"</nts>
                  <nts id="Seg_1841" n="HIAT:ip">,</nts>
                  <nts id="Seg_1842" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T484" id="Seg_1844" n="HIAT:w" s="T483">diːr</ts>
                  <nts id="Seg_1845" n="HIAT:ip">.</nts>
                  <nts id="Seg_1846" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T491" id="Seg_1848" n="HIAT:u" s="T484">
                  <ts e="T485" id="Seg_1850" n="HIAT:w" s="T484">Dʼe</ts>
                  <nts id="Seg_1851" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T486" id="Seg_1853" n="HIAT:w" s="T485">dogoro</ts>
                  <nts id="Seg_1854" n="HIAT:ip">,</nts>
                  <nts id="Seg_1855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T486.tx.1" id="Seg_1857" n="HIAT:w" s="T486">Heliː</ts>
                  <nts id="Seg_1858" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T487" id="Seg_1860" n="HIAT:w" s="T486.tx.1">Kur</ts>
                  <nts id="Seg_1861" n="HIAT:ip">,</nts>
                  <nts id="Seg_1862" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T488" id="Seg_1864" n="HIAT:w" s="T487">tuːsanɨ</ts>
                  <nts id="Seg_1865" n="HIAT:ip">,</nts>
                  <nts id="Seg_1866" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T489" id="Seg_1868" n="HIAT:w" s="T488">ataktarɨ</ts>
                  <nts id="Seg_1869" n="HIAT:ip">,</nts>
                  <nts id="Seg_1870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T490" id="Seg_1872" n="HIAT:w" s="T489">mejiːleri</ts>
                  <nts id="Seg_1873" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T491" id="Seg_1875" n="HIAT:w" s="T490">egelle</ts>
                  <nts id="Seg_1876" n="HIAT:ip">.</nts>
                  <nts id="Seg_1877" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T496" id="Seg_1879" n="HIAT:u" s="T491">
                  <ts e="T492" id="Seg_1881" n="HIAT:w" s="T491">Bejelere</ts>
                  <nts id="Seg_1882" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T493" id="Seg_1884" n="HIAT:w" s="T492">bejelerinen</ts>
                  <nts id="Seg_1885" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T494" id="Seg_1887" n="HIAT:w" s="T493">kihi</ts>
                  <nts id="Seg_1888" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T495" id="Seg_1890" n="HIAT:w" s="T494">bu͡olattaːn</ts>
                  <nts id="Seg_1891" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T496" id="Seg_1893" n="HIAT:w" s="T495">kaːllɨlar</ts>
                  <nts id="Seg_1894" n="HIAT:ip">.</nts>
                  <nts id="Seg_1895" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T503" id="Seg_1897" n="HIAT:u" s="T496">
                  <ts e="T497" id="Seg_1899" n="HIAT:w" s="T496">Utujtaːn</ts>
                  <nts id="Seg_1900" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T498" id="Seg_1902" n="HIAT:w" s="T497">baran</ts>
                  <nts id="Seg_1903" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T498.tx.1" id="Seg_1905" n="HIAT:w" s="T498">Heliː</ts>
                  <nts id="Seg_1906" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T499" id="Seg_1908" n="HIAT:w" s="T498.tx.1">Kur</ts>
                  <nts id="Seg_1909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T500" id="Seg_1911" n="HIAT:w" s="T499">uhuktar</ts>
                  <nts id="Seg_1912" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T501" id="Seg_1914" n="HIAT:w" s="T500">dogoro</ts>
                  <nts id="Seg_1915" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T502" id="Seg_1917" n="HIAT:w" s="T501">taksɨbɨt</ts>
                  <nts id="Seg_1918" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T503" id="Seg_1920" n="HIAT:w" s="T502">tɨ͡ahɨgar</ts>
                  <nts id="Seg_1921" n="HIAT:ip">.</nts>
                  <nts id="Seg_1922" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T513" id="Seg_1924" n="HIAT:u" s="T503">
                  <ts e="T503.tx.1" id="Seg_1926" n="HIAT:w" s="T503">Heliː</ts>
                  <nts id="Seg_1927" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T504" id="Seg_1929" n="HIAT:w" s="T503.tx.1">Kur</ts>
                  <nts id="Seg_1930" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T505" id="Seg_1932" n="HIAT:w" s="T504">taksa</ts>
                  <nts id="Seg_1933" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T506" id="Seg_1935" n="HIAT:w" s="T505">köppüte</ts>
                  <nts id="Seg_1936" n="HIAT:ip">,</nts>
                  <nts id="Seg_1937" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T507" id="Seg_1939" n="HIAT:w" s="T506">hüːs</ts>
                  <nts id="Seg_1940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T508" id="Seg_1942" n="HIAT:w" s="T507">hetiː</ts>
                  <nts id="Seg_1943" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T509" id="Seg_1945" n="HIAT:w" s="T508">eleːnnenen</ts>
                  <nts id="Seg_1946" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T510" id="Seg_1948" n="HIAT:w" s="T509">erdegine</ts>
                  <nts id="Seg_1949" n="HIAT:ip">,</nts>
                  <nts id="Seg_1950" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T511" id="Seg_1952" n="HIAT:w" s="T510">bütehik</ts>
                  <nts id="Seg_1953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T512" id="Seg_1955" n="HIAT:w" s="T511">hetiːge</ts>
                  <nts id="Seg_1956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T513" id="Seg_1958" n="HIAT:w" s="T512">iːlihinne</ts>
                  <nts id="Seg_1959" n="HIAT:ip">.</nts>
                  <nts id="Seg_1960" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T519" id="Seg_1962" n="HIAT:u" s="T513">
                  <ts e="T514" id="Seg_1964" n="HIAT:w" s="T513">Kallaːnɨnan</ts>
                  <nts id="Seg_1965" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T515" id="Seg_1967" n="HIAT:w" s="T514">da</ts>
                  <nts id="Seg_1968" n="HIAT:ip">,</nts>
                  <nts id="Seg_1969" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T516" id="Seg_1971" n="HIAT:w" s="T515">hirinen</ts>
                  <nts id="Seg_1972" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T517" id="Seg_1974" n="HIAT:w" s="T516">da</ts>
                  <nts id="Seg_1975" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T518" id="Seg_1977" n="HIAT:w" s="T517">bararɨn</ts>
                  <nts id="Seg_1978" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T519" id="Seg_1980" n="HIAT:w" s="T518">dʼüːlleːbetege</ts>
                  <nts id="Seg_1981" n="HIAT:ip">.</nts>
                  <nts id="Seg_1982" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T530" id="Seg_1984" n="HIAT:u" s="T519">
                  <ts e="T520" id="Seg_1986" n="HIAT:w" s="T519">Kobu͡o</ts>
                  <nts id="Seg_1987" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T521" id="Seg_1989" n="HIAT:w" s="T520">kojut</ts>
                  <nts id="Seg_1990" n="HIAT:ip">,</nts>
                  <nts id="Seg_1991" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T522" id="Seg_1993" n="HIAT:w" s="T521">kebi͡e</ts>
                  <nts id="Seg_1994" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T523" id="Seg_1996" n="HIAT:w" s="T522">keneges</ts>
                  <nts id="Seg_1997" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T524" id="Seg_1999" n="HIAT:w" s="T523">hüːs</ts>
                  <nts id="Seg_2000" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T525" id="Seg_2002" n="HIAT:w" s="T524">hetiː</ts>
                  <nts id="Seg_2003" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T526" id="Seg_2005" n="HIAT:w" s="T525">toktoːbutugar</ts>
                  <nts id="Seg_2006" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T527" id="Seg_2008" n="HIAT:w" s="T526">körbüte</ts>
                  <nts id="Seg_2009" n="HIAT:ip">,</nts>
                  <nts id="Seg_2010" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T528" id="Seg_2012" n="HIAT:w" s="T527">dogoro</ts>
                  <nts id="Seg_2013" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T529" id="Seg_2015" n="HIAT:w" s="T528">dojdutugar</ts>
                  <nts id="Seg_2016" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T530" id="Seg_2018" n="HIAT:w" s="T529">tiri͡erbit</ts>
                  <nts id="Seg_2019" n="HIAT:ip">.</nts>
                  <nts id="Seg_2020" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T534" id="Seg_2022" n="HIAT:u" s="T530">
                  <ts e="T531" id="Seg_2024" n="HIAT:w" s="T530">Dʼe</ts>
                  <nts id="Seg_2025" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T531.tx.1" id="Seg_2027" n="HIAT:w" s="T531">Timir</ts>
                  <nts id="Seg_2028" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T532" id="Seg_2030" n="HIAT:w" s="T531.tx.1">Haːpka</ts>
                  <nts id="Seg_2031" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T533" id="Seg_2033" n="HIAT:w" s="T532">diːr</ts>
                  <nts id="Seg_2034" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T534" id="Seg_2036" n="HIAT:w" s="T533">dogorun</ts>
                  <nts id="Seg_2037" n="HIAT:ip">:</nts>
                  <nts id="Seg_2038" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T551" id="Seg_2040" n="HIAT:u" s="T534">
                  <nts id="Seg_2041" n="HIAT:ip">"</nts>
                  <ts e="T535" id="Seg_2043" n="HIAT:w" s="T534">Dʼe</ts>
                  <nts id="Seg_2044" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T536" id="Seg_2046" n="HIAT:w" s="T535">iti</ts>
                  <nts id="Seg_2047" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T537" id="Seg_2049" n="HIAT:w" s="T536">hüːs</ts>
                  <nts id="Seg_2050" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T538" id="Seg_2052" n="HIAT:w" s="T537">hetiː</ts>
                  <nts id="Seg_2053" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T539" id="Seg_2055" n="HIAT:w" s="T538">aŋar</ts>
                  <nts id="Seg_2056" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T540" id="Seg_2058" n="HIAT:w" s="T539">ojogohugar</ts>
                  <nts id="Seg_2059" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T541" id="Seg_2061" n="HIAT:w" s="T540">üs</ts>
                  <nts id="Seg_2062" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T542" id="Seg_2064" n="HIAT:w" s="T541">čeːlkeː</ts>
                  <nts id="Seg_2065" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T543" id="Seg_2067" n="HIAT:w" s="T542">atɨːr</ts>
                  <nts id="Seg_2068" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T544" id="Seg_2070" n="HIAT:w" s="T543">kölüllen</ts>
                  <nts id="Seg_2071" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T545" id="Seg_2073" n="HIAT:w" s="T544">turar</ts>
                  <nts id="Seg_2074" n="HIAT:ip">,</nts>
                  <nts id="Seg_2075" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T546" id="Seg_2077" n="HIAT:w" s="T545">aŋar</ts>
                  <nts id="Seg_2078" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T547" id="Seg_2080" n="HIAT:w" s="T546">ojogohugar</ts>
                  <nts id="Seg_2081" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T548" id="Seg_2083" n="HIAT:w" s="T547">üs</ts>
                  <nts id="Seg_2084" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T549" id="Seg_2086" n="HIAT:w" s="T548">kara</ts>
                  <nts id="Seg_2087" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T550" id="Seg_2089" n="HIAT:w" s="T549">atɨːr</ts>
                  <nts id="Seg_2090" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T551" id="Seg_2092" n="HIAT:w" s="T550">turar</ts>
                  <nts id="Seg_2093" n="HIAT:ip">.</nts>
                  <nts id="Seg_2094" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T566" id="Seg_2096" n="HIAT:u" s="T551">
                  <ts e="T552" id="Seg_2098" n="HIAT:w" s="T551">Dʼe</ts>
                  <nts id="Seg_2099" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T553" id="Seg_2101" n="HIAT:w" s="T552">min</ts>
                  <nts id="Seg_2102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T554" id="Seg_2104" n="HIAT:w" s="T553">iti</ts>
                  <nts id="Seg_2105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T555" id="Seg_2107" n="HIAT:w" s="T554">üs</ts>
                  <nts id="Seg_2108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T556" id="Seg_2110" n="HIAT:w" s="T555">čeːlkeː</ts>
                  <nts id="Seg_2111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T557" id="Seg_2113" n="HIAT:w" s="T556">atɨːrɨ</ts>
                  <nts id="Seg_2114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T558" id="Seg_2116" n="HIAT:w" s="T557">ölörü͡öm</ts>
                  <nts id="Seg_2117" n="HIAT:ip">,</nts>
                  <nts id="Seg_2118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T559" id="Seg_2120" n="HIAT:w" s="T558">tiriːlerin</ts>
                  <nts id="Seg_2121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T560" id="Seg_2123" n="HIAT:w" s="T559">kastɨ͡am</ts>
                  <nts id="Seg_2124" n="HIAT:ip">,</nts>
                  <nts id="Seg_2125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T561" id="Seg_2127" n="HIAT:w" s="T560">en</ts>
                  <nts id="Seg_2128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T562" id="Seg_2130" n="HIAT:w" s="T561">üs</ts>
                  <nts id="Seg_2131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T563" id="Seg_2133" n="HIAT:w" s="T562">karanɨ</ts>
                  <nts id="Seg_2134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T564" id="Seg_2136" n="HIAT:w" s="T563">ölörü͡öŋ</ts>
                  <nts id="Seg_2137" n="HIAT:ip">,</nts>
                  <nts id="Seg_2138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T565" id="Seg_2140" n="HIAT:w" s="T564">kastɨ͡aŋ</ts>
                  <nts id="Seg_2141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T566" id="Seg_2143" n="HIAT:w" s="T565">tiriːlerin</ts>
                  <nts id="Seg_2144" n="HIAT:ip">.</nts>
                  <nts id="Seg_2145" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T574" id="Seg_2147" n="HIAT:u" s="T566">
                  <ts e="T567" id="Seg_2149" n="HIAT:w" s="T566">Bilegi͡en</ts>
                  <nts id="Seg_2150" n="HIAT:ip">,</nts>
                  <nts id="Seg_2151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T568" id="Seg_2153" n="HIAT:w" s="T567">bu</ts>
                  <nts id="Seg_2154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T569" id="Seg_2156" n="HIAT:w" s="T568">Purgaː</ts>
                  <nts id="Seg_2157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T570" id="Seg_2159" n="HIAT:w" s="T569">iččitin</ts>
                  <nts id="Seg_2160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T571" id="Seg_2162" n="HIAT:w" s="T570">ɨlgɨn</ts>
                  <nts id="Seg_2163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T572" id="Seg_2165" n="HIAT:w" s="T571">kɨːstarɨn</ts>
                  <nts id="Seg_2166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T573" id="Seg_2168" n="HIAT:w" s="T572">egellim</ts>
                  <nts id="Seg_2169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T574" id="Seg_2171" n="HIAT:w" s="T573">albaspɨnan</ts>
                  <nts id="Seg_2172" n="HIAT:ip">.</nts>
                  <nts id="Seg_2173" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T582" id="Seg_2175" n="HIAT:u" s="T574">
                  <ts e="T575" id="Seg_2177" n="HIAT:w" s="T574">Anɨ</ts>
                  <nts id="Seg_2178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T576" id="Seg_2180" n="HIAT:w" s="T575">itintilerin</ts>
                  <nts id="Seg_2181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T577" id="Seg_2183" n="HIAT:w" s="T576">batan</ts>
                  <nts id="Seg_2184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T578" id="Seg_2186" n="HIAT:w" s="T577">üs</ts>
                  <nts id="Seg_2187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T579" id="Seg_2189" n="HIAT:w" s="T578">ubaja</ts>
                  <nts id="Seg_2190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T580" id="Seg_2192" n="HIAT:w" s="T579">Purgaː</ts>
                  <nts id="Seg_2193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T581" id="Seg_2195" n="HIAT:w" s="T580">iččilere</ts>
                  <nts id="Seg_2196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T582" id="Seg_2198" n="HIAT:w" s="T581">keli͡ektere</ts>
                  <nts id="Seg_2199" n="HIAT:ip">.</nts>
                  <nts id="Seg_2200" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T596" id="Seg_2202" n="HIAT:u" s="T582">
                  <ts e="T583" id="Seg_2204" n="HIAT:w" s="T582">Ol</ts>
                  <nts id="Seg_2205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T584" id="Seg_2207" n="HIAT:w" s="T583">kihiler</ts>
                  <nts id="Seg_2208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T585" id="Seg_2210" n="HIAT:w" s="T584">kellekterine</ts>
                  <nts id="Seg_2211" n="HIAT:ip">,</nts>
                  <nts id="Seg_2212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T586" id="Seg_2214" n="HIAT:w" s="T585">ketegeriːn</ts>
                  <nts id="Seg_2215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T587" id="Seg_2217" n="HIAT:w" s="T586">üs</ts>
                  <nts id="Seg_2218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T588" id="Seg_2220" n="HIAT:w" s="T587">čeːlkeː</ts>
                  <nts id="Seg_2221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T589" id="Seg_2223" n="HIAT:w" s="T588">tiriːni</ts>
                  <nts id="Seg_2224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T590" id="Seg_2226" n="HIAT:w" s="T589">telgi͡em</ts>
                  <nts id="Seg_2227" n="HIAT:ip">,</nts>
                  <nts id="Seg_2228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T591" id="Seg_2230" n="HIAT:w" s="T590">en</ts>
                  <nts id="Seg_2231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T592" id="Seg_2233" n="HIAT:w" s="T591">iti</ts>
                  <nts id="Seg_2234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T593" id="Seg_2236" n="HIAT:w" s="T592">üs</ts>
                  <nts id="Seg_2237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T594" id="Seg_2239" n="HIAT:w" s="T593">koŋnomu͡oj</ts>
                  <nts id="Seg_2240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T595" id="Seg_2242" n="HIAT:w" s="T594">tiriːgin</ts>
                  <nts id="Seg_2243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T596" id="Seg_2245" n="HIAT:w" s="T595">telgi͡er</ts>
                  <nts id="Seg_2246" n="HIAT:ip">.</nts>
                  <nts id="Seg_2247" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T605" id="Seg_2249" n="HIAT:u" s="T596">
                  <ts e="T597" id="Seg_2251" n="HIAT:w" s="T596">Dʼe</ts>
                  <nts id="Seg_2252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T598" id="Seg_2254" n="HIAT:w" s="T597">ulakan</ts>
                  <nts id="Seg_2255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T599" id="Seg_2257" n="HIAT:w" s="T598">čaːŋŋar</ts>
                  <nts id="Seg_2258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T600" id="Seg_2260" n="HIAT:w" s="T599">toloru</ts>
                  <nts id="Seg_2261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T601" id="Seg_2263" n="HIAT:w" s="T600">uːta</ts>
                  <nts id="Seg_2264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T602" id="Seg_2266" n="HIAT:w" s="T601">kɨːjnar</ts>
                  <nts id="Seg_2267" n="HIAT:ip">,</nts>
                  <nts id="Seg_2268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T603" id="Seg_2270" n="HIAT:w" s="T602">min</ts>
                  <nts id="Seg_2271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T604" id="Seg_2273" n="HIAT:w" s="T603">emi͡e</ts>
                  <nts id="Seg_2274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T605" id="Seg_2276" n="HIAT:w" s="T604">kɨːjnarɨ͡am</ts>
                  <nts id="Seg_2277" n="HIAT:ip">.</nts>
                  <nts id="Seg_2278" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T609" id="Seg_2280" n="HIAT:u" s="T605">
                  <ts e="T606" id="Seg_2282" n="HIAT:w" s="T605">Buka</ts>
                  <nts id="Seg_2283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T607" id="Seg_2285" n="HIAT:w" s="T606">mannaj</ts>
                  <nts id="Seg_2286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T608" id="Seg_2288" n="HIAT:w" s="T607">mi͡eke</ts>
                  <nts id="Seg_2289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T609" id="Seg_2291" n="HIAT:w" s="T608">ɨ͡aldʼɨttɨ͡aktara</ts>
                  <nts id="Seg_2292" n="HIAT:ip">.</nts>
                  <nts id="Seg_2293" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T615" id="Seg_2295" n="HIAT:u" s="T609">
                  <ts e="T610" id="Seg_2297" n="HIAT:w" s="T609">Miːgitten</ts>
                  <nts id="Seg_2298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T611" id="Seg_2300" n="HIAT:w" s="T610">toton</ts>
                  <nts id="Seg_2301" n="HIAT:ip">,</nts>
                  <nts id="Seg_2302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T612" id="Seg_2304" n="HIAT:w" s="T611">höbüleːn</ts>
                  <nts id="Seg_2305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T613" id="Seg_2307" n="HIAT:w" s="T612">tagɨstaktarɨna</ts>
                  <nts id="Seg_2308" n="HIAT:ip">,</nts>
                  <nts id="Seg_2309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T614" id="Seg_2311" n="HIAT:w" s="T613">eji͡eke</ts>
                  <nts id="Seg_2312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T615" id="Seg_2314" n="HIAT:w" s="T614">ɨ͡allanɨ͡aktara</ts>
                  <nts id="Seg_2315" n="HIAT:ip">.</nts>
                  <nts id="Seg_2316" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T621" id="Seg_2318" n="HIAT:u" s="T615">
                  <ts e="T616" id="Seg_2320" n="HIAT:w" s="T615">Min</ts>
                  <nts id="Seg_2321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T617" id="Seg_2323" n="HIAT:w" s="T616">kinilerge</ts>
                  <nts id="Seg_2324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T618" id="Seg_2326" n="HIAT:w" s="T617">tugu</ts>
                  <nts id="Seg_2327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T619" id="Seg_2329" n="HIAT:w" s="T618">eme</ts>
                  <nts id="Seg_2330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T620" id="Seg_2332" n="HIAT:w" s="T619">köllörü͡öm</ts>
                  <nts id="Seg_2333" n="HIAT:ip">,</nts>
                  <nts id="Seg_2334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T621" id="Seg_2336" n="HIAT:w" s="T620">kiːrseːr</ts>
                  <nts id="Seg_2337" n="HIAT:ip">.</nts>
                  <nts id="Seg_2338" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T631" id="Seg_2340" n="HIAT:u" s="T621">
                  <ts e="T622" id="Seg_2342" n="HIAT:w" s="T621">Min</ts>
                  <nts id="Seg_2343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T623" id="Seg_2345" n="HIAT:w" s="T622">tugu</ts>
                  <nts id="Seg_2346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T624" id="Seg_2348" n="HIAT:w" s="T623">kördöröbün</ts>
                  <nts id="Seg_2349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T625" id="Seg_2351" n="HIAT:w" s="T624">da</ts>
                  <nts id="Seg_2352" n="HIAT:ip">,</nts>
                  <nts id="Seg_2353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T626" id="Seg_2355" n="HIAT:w" s="T625">ütükten</ts>
                  <nts id="Seg_2356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T627" id="Seg_2358" n="HIAT:w" s="T626">kinilerge</ts>
                  <nts id="Seg_2359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T628" id="Seg_2361" n="HIAT:w" s="T627">kördöröːr</ts>
                  <nts id="Seg_2362" n="HIAT:ip">"</nts>
                  <nts id="Seg_2363" n="HIAT:ip">,</nts>
                  <nts id="Seg_2364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T629" id="Seg_2366" n="HIAT:w" s="T628">di͡ete</ts>
                  <nts id="Seg_2367" n="HIAT:ip">,</nts>
                  <nts id="Seg_2368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T630" id="Seg_2370" n="HIAT:w" s="T629">ü͡örette</ts>
                  <nts id="Seg_2371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T631" id="Seg_2373" n="HIAT:w" s="T630">dogorun</ts>
                  <nts id="Seg_2374" n="HIAT:ip">.</nts>
                  <nts id="Seg_2375" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T641" id="Seg_2377" n="HIAT:u" s="T631">
                  <ts e="T632" id="Seg_2379" n="HIAT:w" s="T631">Töhö</ts>
                  <nts id="Seg_2380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T633" id="Seg_2382" n="HIAT:w" s="T632">da</ts>
                  <nts id="Seg_2383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T634" id="Seg_2385" n="HIAT:w" s="T633">bu͡olumuja</ts>
                  <nts id="Seg_2386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T635" id="Seg_2388" n="HIAT:w" s="T634">Purgaː</ts>
                  <nts id="Seg_2389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T636" id="Seg_2391" n="HIAT:w" s="T635">iččilere</ts>
                  <nts id="Seg_2392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T637" id="Seg_2394" n="HIAT:w" s="T636">bolohonnon</ts>
                  <nts id="Seg_2395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T638" id="Seg_2397" n="HIAT:w" s="T637">kelliler</ts>
                  <nts id="Seg_2398" n="HIAT:ip">,</nts>
                  <nts id="Seg_2399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T639" id="Seg_2401" n="HIAT:w" s="T638">kam</ts>
                  <nts id="Seg_2402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T640" id="Seg_2404" n="HIAT:w" s="T639">muːs</ts>
                  <nts id="Seg_2405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T641" id="Seg_2407" n="HIAT:w" s="T640">dʼonnor</ts>
                  <nts id="Seg_2408" n="HIAT:ip">.</nts>
                  <nts id="Seg_2409" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T646" id="Seg_2411" n="HIAT:u" s="T641">
                  <ts e="T642" id="Seg_2413" n="HIAT:w" s="T641">Kiːrenner</ts>
                  <nts id="Seg_2414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T643" id="Seg_2416" n="HIAT:w" s="T642">ötörü-hötörü</ts>
                  <nts id="Seg_2417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2418" n="HIAT:ip">"</nts>
                  <ts e="T644" id="Seg_2420" n="HIAT:w" s="T643">doroːbo</ts>
                  <nts id="Seg_2421" n="HIAT:ip">"</nts>
                  <nts id="Seg_2422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T645" id="Seg_2424" n="HIAT:w" s="T644">da</ts>
                  <nts id="Seg_2425" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T646" id="Seg_2427" n="HIAT:w" s="T645">di͡ebetter</ts>
                  <nts id="Seg_2428" n="HIAT:ip">.</nts>
                  <nts id="Seg_2429" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T652" id="Seg_2431" n="HIAT:u" s="T646">
                  <ts e="T646.tx.1" id="Seg_2433" n="HIAT:w" s="T646">Timir</ts>
                  <nts id="Seg_2434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T647" id="Seg_2436" n="HIAT:w" s="T646.tx.1">Haːpka</ts>
                  <nts id="Seg_2437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T648" id="Seg_2439" n="HIAT:w" s="T647">dʼi͡etiger</ts>
                  <nts id="Seg_2440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T649" id="Seg_2442" n="HIAT:w" s="T648">üs</ts>
                  <nts id="Seg_2443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T650" id="Seg_2445" n="HIAT:w" s="T649">čeːlkeː</ts>
                  <nts id="Seg_2446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T651" id="Seg_2448" n="HIAT:w" s="T650">tiriːge</ts>
                  <nts id="Seg_2449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T652" id="Seg_2451" n="HIAT:w" s="T651">olorollor</ts>
                  <nts id="Seg_2452" n="HIAT:ip">.</nts>
                  <nts id="Seg_2453" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T656" id="Seg_2455" n="HIAT:u" s="T652">
                  <ts e="T652.tx.1" id="Seg_2457" n="HIAT:w" s="T652">Timir</ts>
                  <nts id="Seg_2458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T653" id="Seg_2460" n="HIAT:w" s="T652.tx.1">Haːpka</ts>
                  <nts id="Seg_2461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T654" id="Seg_2463" n="HIAT:w" s="T653">ideleniːhi</ts>
                  <nts id="Seg_2464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T655" id="Seg_2466" n="HIAT:w" s="T654">bu</ts>
                  <nts id="Seg_2467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T656" id="Seg_2469" n="HIAT:w" s="T655">dʼoŋŋo</ts>
                  <nts id="Seg_2470" n="HIAT:ip">.</nts>
                  <nts id="Seg_2471" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T681" id="Seg_2473" n="HIAT:u" s="T656">
                  <ts e="T657" id="Seg_2475" n="HIAT:w" s="T656">Kɨːjna</ts>
                  <nts id="Seg_2476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T658" id="Seg_2478" n="HIAT:w" s="T657">turar</ts>
                  <nts id="Seg_2479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T659" id="Seg_2481" n="HIAT:w" s="T658">ulakan</ts>
                  <nts id="Seg_2482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T660" id="Seg_2484" n="HIAT:w" s="T659">čaːŋŋa</ts>
                  <nts id="Seg_2485" n="HIAT:ip">,</nts>
                  <nts id="Seg_2486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T661" id="Seg_2488" n="HIAT:w" s="T660">heliː</ts>
                  <nts id="Seg_2489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T662" id="Seg_2491" n="HIAT:w" s="T661">munnulaːk</ts>
                  <nts id="Seg_2492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T662.tx.1" id="Seg_2494" n="HIAT:w" s="T662">tallan</ts>
                  <nts id="Seg_2495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T663" id="Seg_2497" n="HIAT:w" s="T662.tx.1">ku͡ogas</ts>
                  <nts id="Seg_2498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T664" id="Seg_2500" n="HIAT:w" s="T663">bu͡olaːt</ts>
                  <nts id="Seg_2501" n="HIAT:ip">,</nts>
                  <nts id="Seg_2502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2503" n="HIAT:ip">"</nts>
                  <ts e="T664.tx.1" id="Seg_2505" n="HIAT:w" s="T664">Aː</ts>
                  <nts id="Seg_2506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T665" id="Seg_2508" n="HIAT:w" s="T664.tx.1">uː</ts>
                  <nts id="Seg_2509" n="HIAT:ip">"</nts>
                  <nts id="Seg_2510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T666" id="Seg_2512" n="HIAT:w" s="T665">di͡et</ts>
                  <nts id="Seg_2513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T667" id="Seg_2515" n="HIAT:w" s="T666">umsan</ts>
                  <nts id="Seg_2516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T668" id="Seg_2518" n="HIAT:w" s="T667">kaːlar</ts>
                  <nts id="Seg_2519" n="HIAT:ip">,</nts>
                  <nts id="Seg_2520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T669" id="Seg_2522" n="HIAT:w" s="T668">dajda-dajbɨtɨnan</ts>
                  <nts id="Seg_2523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T670" id="Seg_2525" n="HIAT:w" s="T669">kötön</ts>
                  <nts id="Seg_2526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T671" id="Seg_2528" n="HIAT:w" s="T670">taksan</ts>
                  <nts id="Seg_2529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T672" id="Seg_2531" n="HIAT:w" s="T671">ijetin</ts>
                  <nts id="Seg_2532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T673" id="Seg_2534" n="HIAT:w" s="T672">bɨ͡arɨnan</ts>
                  <nts id="Seg_2535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T674" id="Seg_2537" n="HIAT:w" s="T673">helip</ts>
                  <nts id="Seg_2538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T675" id="Seg_2540" n="HIAT:w" s="T674">gɨnar</ts>
                  <nts id="Seg_2541" n="HIAT:ip">,</nts>
                  <nts id="Seg_2542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T676" id="Seg_2544" n="HIAT:w" s="T675">onton</ts>
                  <nts id="Seg_2545" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T677" id="Seg_2547" n="HIAT:w" s="T676">ijetin</ts>
                  <nts id="Seg_2548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T678" id="Seg_2550" n="HIAT:w" s="T677">ajagɨn</ts>
                  <nts id="Seg_2551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T679" id="Seg_2553" n="HIAT:w" s="T678">ustun</ts>
                  <nts id="Seg_2554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T680" id="Seg_2556" n="HIAT:w" s="T679">taksa</ts>
                  <nts id="Seg_2557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T681" id="Seg_2559" n="HIAT:w" s="T680">köppüt</ts>
                  <nts id="Seg_2560" n="HIAT:ip">.</nts>
                  <nts id="Seg_2561" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T696" id="Seg_2563" n="HIAT:u" s="T681">
                  <ts e="T682" id="Seg_2565" n="HIAT:w" s="T681">Dʼe</ts>
                  <nts id="Seg_2566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T683" id="Seg_2568" n="HIAT:w" s="T682">onton</ts>
                  <nts id="Seg_2569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T684" id="Seg_2571" n="HIAT:w" s="T683">agatɨn</ts>
                  <nts id="Seg_2572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T685" id="Seg_2574" n="HIAT:w" s="T684">bɨ͡arɨnan</ts>
                  <nts id="Seg_2575" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T686" id="Seg_2577" n="HIAT:w" s="T685">helip</ts>
                  <nts id="Seg_2578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T687" id="Seg_2580" n="HIAT:w" s="T686">gɨmmɨt</ts>
                  <nts id="Seg_2581" n="HIAT:ip">,</nts>
                  <nts id="Seg_2582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T688" id="Seg_2584" n="HIAT:w" s="T687">agatɨn</ts>
                  <nts id="Seg_2585" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T689" id="Seg_2587" n="HIAT:w" s="T688">ajagɨnan</ts>
                  <nts id="Seg_2588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T690" id="Seg_2590" n="HIAT:w" s="T689">hanardɨː</ts>
                  <nts id="Seg_2591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T691" id="Seg_2593" n="HIAT:w" s="T690">mu͡ohun</ts>
                  <nts id="Seg_2594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T692" id="Seg_2596" n="HIAT:w" s="T691">hullaːbɨt</ts>
                  <nts id="Seg_2597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T693" id="Seg_2599" n="HIAT:w" s="T692">kɨːl</ts>
                  <nts id="Seg_2600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T694" id="Seg_2602" n="HIAT:w" s="T693">atɨːra</ts>
                  <nts id="Seg_2603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T695" id="Seg_2605" n="HIAT:w" s="T694">bu͡olan</ts>
                  <nts id="Seg_2606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T696" id="Seg_2608" n="HIAT:w" s="T695">taksar</ts>
                  <nts id="Seg_2609" n="HIAT:ip">.</nts>
                  <nts id="Seg_2610" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T709" id="Seg_2612" n="HIAT:u" s="T696">
                  <ts e="T697" id="Seg_2614" n="HIAT:w" s="T696">Bu</ts>
                  <nts id="Seg_2615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T698" id="Seg_2617" n="HIAT:w" s="T697">kennitten</ts>
                  <nts id="Seg_2618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T698.tx.1" id="Seg_2620" n="HIAT:w" s="T698">Timir</ts>
                  <nts id="Seg_2621" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T699" id="Seg_2623" n="HIAT:w" s="T698.tx.1">Haːpka</ts>
                  <nts id="Seg_2624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T700" id="Seg_2626" n="HIAT:w" s="T699">kihi</ts>
                  <nts id="Seg_2627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T701" id="Seg_2629" n="HIAT:w" s="T700">bu͡olar</ts>
                  <nts id="Seg_2630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T702" id="Seg_2632" n="HIAT:w" s="T701">da</ts>
                  <nts id="Seg_2633" n="HIAT:ip">,</nts>
                  <nts id="Seg_2634" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T703" id="Seg_2636" n="HIAT:w" s="T702">alanaːnnan</ts>
                  <nts id="Seg_2637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T704" id="Seg_2639" n="HIAT:w" s="T703">bu</ts>
                  <nts id="Seg_2640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T705" id="Seg_2642" n="HIAT:w" s="T704">kɨːl</ts>
                  <nts id="Seg_2643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T706" id="Seg_2645" n="HIAT:w" s="T705">atɨːrɨn</ts>
                  <nts id="Seg_2646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T707" id="Seg_2648" n="HIAT:w" s="T706">huptu</ts>
                  <nts id="Seg_2649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T708" id="Seg_2651" n="HIAT:w" s="T707">ɨtan</ts>
                  <nts id="Seg_2652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T709" id="Seg_2654" n="HIAT:w" s="T708">tüherer</ts>
                  <nts id="Seg_2655" n="HIAT:ip">.</nts>
                  <nts id="Seg_2656" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T721" id="Seg_2658" n="HIAT:u" s="T709">
                  <ts e="T710" id="Seg_2660" n="HIAT:w" s="T709">Ölörör</ts>
                  <nts id="Seg_2661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T711" id="Seg_2663" n="HIAT:w" s="T710">da</ts>
                  <nts id="Seg_2664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T712" id="Seg_2666" n="HIAT:w" s="T711">manɨga</ts>
                  <nts id="Seg_2667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T713" id="Seg_2669" n="HIAT:w" s="T712">kastɨrɨta</ts>
                  <nts id="Seg_2670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T714" id="Seg_2672" n="HIAT:w" s="T713">hülen</ts>
                  <nts id="Seg_2673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T715" id="Seg_2675" n="HIAT:w" s="T714">mas</ts>
                  <nts id="Seg_2676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T716" id="Seg_2678" n="HIAT:w" s="T715">atɨjakka</ts>
                  <nts id="Seg_2679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T717" id="Seg_2681" n="HIAT:w" s="T716">hiːkejdiː-buhuːluː</ts>
                  <nts id="Seg_2682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T718" id="Seg_2684" n="HIAT:w" s="T717">tardan</ts>
                  <nts id="Seg_2685" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T719" id="Seg_2687" n="HIAT:w" s="T718">keːher</ts>
                  <nts id="Seg_2688" n="HIAT:ip">,</nts>
                  <nts id="Seg_2689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T720" id="Seg_2691" n="HIAT:w" s="T719">ɨ͡aldʼɨttarga</ts>
                  <nts id="Seg_2692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T721" id="Seg_2694" n="HIAT:w" s="T720">hogudaːjdatan</ts>
                  <nts id="Seg_2695" n="HIAT:ip">.</nts>
                  <nts id="Seg_2696" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T726" id="Seg_2698" n="HIAT:u" s="T721">
                  <ts e="T722" id="Seg_2700" n="HIAT:w" s="T721">ɨ͡aldʼɨttara</ts>
                  <nts id="Seg_2701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T723" id="Seg_2703" n="HIAT:w" s="T722">haŋa-iŋe</ts>
                  <nts id="Seg_2704" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T724" id="Seg_2706" n="HIAT:w" s="T723">hu͡ok</ts>
                  <nts id="Seg_2707" n="HIAT:ip">,</nts>
                  <nts id="Seg_2708" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T725" id="Seg_2710" n="HIAT:w" s="T724">ahɨːrga</ts>
                  <nts id="Seg_2711" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T726" id="Seg_2713" n="HIAT:w" s="T725">barbɨttar</ts>
                  <nts id="Seg_2714" n="HIAT:ip">.</nts>
                  <nts id="Seg_2715" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T733" id="Seg_2717" n="HIAT:u" s="T726">
                  <ts e="T727" id="Seg_2719" n="HIAT:w" s="T726">Kühüŋŋü</ts>
                  <nts id="Seg_2720" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T728" id="Seg_2722" n="HIAT:w" s="T727">atɨːrtan</ts>
                  <nts id="Seg_2723" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T729" id="Seg_2725" n="HIAT:w" s="T728">amattan</ts>
                  <nts id="Seg_2726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T730" id="Seg_2728" n="HIAT:w" s="T729">mu͡ostaːk</ts>
                  <nts id="Seg_2729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T731" id="Seg_2731" n="HIAT:w" s="T730">tujagɨn</ts>
                  <nts id="Seg_2732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T732" id="Seg_2734" n="HIAT:w" s="T731">ordoron</ts>
                  <nts id="Seg_2735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T733" id="Seg_2737" n="HIAT:w" s="T732">kebistiler</ts>
                  <nts id="Seg_2738" n="HIAT:ip">.</nts>
                  <nts id="Seg_2739" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T736" id="Seg_2741" n="HIAT:u" s="T733">
                  <ts e="T733.tx.1" id="Seg_2743" n="HIAT:w" s="T733">Heliː</ts>
                  <nts id="Seg_2744" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T734" id="Seg_2746" n="HIAT:w" s="T733.tx.1">Kur</ts>
                  <nts id="Seg_2747" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T735" id="Seg_2749" n="HIAT:w" s="T734">körön</ts>
                  <nts id="Seg_2750" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T736" id="Seg_2752" n="HIAT:w" s="T735">olorunaːktaːta</ts>
                  <nts id="Seg_2753" n="HIAT:ip">.</nts>
                  <nts id="Seg_2754" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T742" id="Seg_2756" n="HIAT:u" s="T736">
                  <ts e="T737" id="Seg_2758" n="HIAT:w" s="T736">Ahaːn</ts>
                  <nts id="Seg_2759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T738" id="Seg_2761" n="HIAT:w" s="T737">büteller</ts>
                  <nts id="Seg_2762" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T739" id="Seg_2764" n="HIAT:w" s="T738">da</ts>
                  <nts id="Seg_2765" n="HIAT:ip">,</nts>
                  <nts id="Seg_2766" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T740" id="Seg_2768" n="HIAT:w" s="T739">u͡ostarɨn</ts>
                  <nts id="Seg_2769" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T741" id="Seg_2771" n="HIAT:w" s="T740">hottoːn</ts>
                  <nts id="Seg_2772" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T742" id="Seg_2774" n="HIAT:w" s="T741">taksallar</ts>
                  <nts id="Seg_2775" n="HIAT:ip">.</nts>
                  <nts id="Seg_2776" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T747" id="Seg_2778" n="HIAT:u" s="T742">
                  <ts e="T742.tx.1" id="Seg_2780" n="HIAT:w" s="T742">Heliː</ts>
                  <nts id="Seg_2781" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T743" id="Seg_2783" n="HIAT:w" s="T742.tx.1">Kur</ts>
                  <nts id="Seg_2784" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T744" id="Seg_2786" n="HIAT:w" s="T743">dʼi͡etiger</ts>
                  <nts id="Seg_2787" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T745" id="Seg_2789" n="HIAT:w" s="T744">ɨstanar</ts>
                  <nts id="Seg_2790" n="HIAT:ip">,</nts>
                  <nts id="Seg_2791" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T746" id="Seg_2793" n="HIAT:w" s="T745">ɨ͡aldʼɨttarɨ</ts>
                  <nts id="Seg_2794" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T747" id="Seg_2796" n="HIAT:w" s="T746">tohujuna</ts>
                  <nts id="Seg_2797" n="HIAT:ip">.</nts>
                  <nts id="Seg_2798" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T754" id="Seg_2800" n="HIAT:u" s="T747">
                  <ts e="T748" id="Seg_2802" n="HIAT:w" s="T747">ɨ͡aldʼɨttara</ts>
                  <nts id="Seg_2803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T749" id="Seg_2805" n="HIAT:w" s="T748">kiːren</ts>
                  <nts id="Seg_2806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T750" id="Seg_2808" n="HIAT:w" s="T749">üs</ts>
                  <nts id="Seg_2809" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T751" id="Seg_2811" n="HIAT:w" s="T750">koŋnomu͡oj</ts>
                  <nts id="Seg_2812" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T752" id="Seg_2814" n="HIAT:w" s="T751">tiriːge</ts>
                  <nts id="Seg_2815" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T753" id="Seg_2817" n="HIAT:w" s="T752">oloro</ts>
                  <nts id="Seg_2818" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T754" id="Seg_2820" n="HIAT:w" s="T753">bi͡ereller</ts>
                  <nts id="Seg_2821" n="HIAT:ip">.</nts>
                  <nts id="Seg_2822" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T772" id="Seg_2824" n="HIAT:u" s="T754">
                  <ts e="T754.tx.1" id="Seg_2826" n="HIAT:w" s="T754">Heliː</ts>
                  <nts id="Seg_2827" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T755" id="Seg_2829" n="HIAT:w" s="T754.tx.1">Kur</ts>
                  <nts id="Seg_2830" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2831" n="HIAT:ip">"</nts>
                  <ts e="T755.tx.1" id="Seg_2833" n="HIAT:w" s="T755">A</ts>
                  <nts id="Seg_2834" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T756" id="Seg_2836" n="HIAT:w" s="T755.tx.1">uːk</ts>
                  <nts id="Seg_2837" n="HIAT:ip">"</nts>
                  <nts id="Seg_2838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T757" id="Seg_2840" n="HIAT:w" s="T756">di͡et</ts>
                  <nts id="Seg_2841" n="HIAT:ip">,</nts>
                  <nts id="Seg_2842" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T757.tx.1" id="Seg_2844" n="HIAT:w" s="T757">bɨːtta</ts>
                  <nts id="Seg_2845" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T758" id="Seg_2847" n="HIAT:w" s="T757.tx.1">ku͡ogaskaːn</ts>
                  <nts id="Seg_2848" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T759" id="Seg_2850" n="HIAT:w" s="T758">bu͡olaːt</ts>
                  <nts id="Seg_2851" n="HIAT:ip">,</nts>
                  <nts id="Seg_2852" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T760" id="Seg_2854" n="HIAT:w" s="T759">čaːŋŋa</ts>
                  <nts id="Seg_2855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T761" id="Seg_2857" n="HIAT:w" s="T760">umsar</ts>
                  <nts id="Seg_2858" n="HIAT:ip">,</nts>
                  <nts id="Seg_2859" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T762" id="Seg_2861" n="HIAT:w" s="T761">ijetin</ts>
                  <nts id="Seg_2862" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T763" id="Seg_2864" n="HIAT:w" s="T762">bɨ͡arɨgar</ts>
                  <nts id="Seg_2865" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T764" id="Seg_2867" n="HIAT:w" s="T763">helip</ts>
                  <nts id="Seg_2868" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T765" id="Seg_2870" n="HIAT:w" s="T764">gɨnar</ts>
                  <nts id="Seg_2871" n="HIAT:ip">,</nts>
                  <nts id="Seg_2872" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T766" id="Seg_2874" n="HIAT:w" s="T765">ajagɨn</ts>
                  <nts id="Seg_2875" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T767" id="Seg_2877" n="HIAT:w" s="T766">ustun</ts>
                  <nts id="Seg_2878" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T768" id="Seg_2880" n="HIAT:w" s="T767">taksar</ts>
                  <nts id="Seg_2881" n="HIAT:ip">,</nts>
                  <nts id="Seg_2882" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T769" id="Seg_2884" n="HIAT:w" s="T768">agatɨn</ts>
                  <nts id="Seg_2885" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T770" id="Seg_2887" n="HIAT:w" s="T769">bɨ͡arɨgar</ts>
                  <nts id="Seg_2888" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T771" id="Seg_2890" n="HIAT:w" s="T770">helip</ts>
                  <nts id="Seg_2891" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T772" id="Seg_2893" n="HIAT:w" s="T771">gɨnar</ts>
                  <nts id="Seg_2894" n="HIAT:ip">.</nts>
                  <nts id="Seg_2895" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T783" id="Seg_2897" n="HIAT:u" s="T772">
                  <ts e="T773" id="Seg_2899" n="HIAT:w" s="T772">Agatɨn</ts>
                  <nts id="Seg_2900" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T774" id="Seg_2902" n="HIAT:w" s="T773">ajagɨn</ts>
                  <nts id="Seg_2903" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T775" id="Seg_2905" n="HIAT:w" s="T774">ustun</ts>
                  <nts id="Seg_2906" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T776" id="Seg_2908" n="HIAT:w" s="T775">tuguttan</ts>
                  <nts id="Seg_2909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T777" id="Seg_2911" n="HIAT:w" s="T776">ere</ts>
                  <nts id="Seg_2912" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T778" id="Seg_2914" n="HIAT:w" s="T777">kuhagan</ts>
                  <nts id="Seg_2915" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T779" id="Seg_2917" n="HIAT:w" s="T778">tulaːjakiː</ts>
                  <nts id="Seg_2918" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T780" id="Seg_2920" n="HIAT:w" s="T779">tugukkaːn</ts>
                  <nts id="Seg_2921" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T781" id="Seg_2923" n="HIAT:w" s="T780">bu͡olan</ts>
                  <nts id="Seg_2924" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T782" id="Seg_2926" n="HIAT:w" s="T781">taksa</ts>
                  <nts id="Seg_2927" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T783" id="Seg_2929" n="HIAT:w" s="T782">köppüt</ts>
                  <nts id="Seg_2930" n="HIAT:ip">.</nts>
                  <nts id="Seg_2931" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T794" id="Seg_2933" n="HIAT:u" s="T783">
                  <ts e="T784" id="Seg_2935" n="HIAT:w" s="T783">Kennitten</ts>
                  <nts id="Seg_2936" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T785" id="Seg_2938" n="HIAT:w" s="T784">haːlaːk</ts>
                  <nts id="Seg_2939" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T786" id="Seg_2941" n="HIAT:w" s="T785">kihi</ts>
                  <nts id="Seg_2942" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T787" id="Seg_2944" n="HIAT:w" s="T786">taksar</ts>
                  <nts id="Seg_2945" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T788" id="Seg_2947" n="HIAT:w" s="T787">da</ts>
                  <nts id="Seg_2948" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T789" id="Seg_2950" n="HIAT:w" s="T788">ɨ͡aldʼɨttar</ts>
                  <nts id="Seg_2951" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T790" id="Seg_2953" n="HIAT:w" s="T789">innileriger</ts>
                  <nts id="Seg_2954" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T791" id="Seg_2956" n="HIAT:w" s="T790">epseri</ts>
                  <nts id="Seg_2957" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T792" id="Seg_2959" n="HIAT:w" s="T791">ɨtar</ts>
                  <nts id="Seg_2960" n="HIAT:ip">,</nts>
                  <nts id="Seg_2961" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T793" id="Seg_2963" n="HIAT:w" s="T792">astaːn</ts>
                  <nts id="Seg_2964" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T794" id="Seg_2966" n="HIAT:w" s="T793">kebiher</ts>
                  <nts id="Seg_2967" n="HIAT:ip">.</nts>
                  <nts id="Seg_2968" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T797" id="Seg_2970" n="HIAT:u" s="T794">
                  <ts e="T795" id="Seg_2972" n="HIAT:w" s="T794">ɨ͡aldʼɨttarga</ts>
                  <nts id="Seg_2973" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T796" id="Seg_2975" n="HIAT:w" s="T795">tardan</ts>
                  <nts id="Seg_2976" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T797" id="Seg_2978" n="HIAT:w" s="T796">keːher</ts>
                  <nts id="Seg_2979" n="HIAT:ip">.</nts>
                  <nts id="Seg_2980" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T800" id="Seg_2982" n="HIAT:u" s="T797">
                  <ts e="T798" id="Seg_2984" n="HIAT:w" s="T797">Tardan</ts>
                  <nts id="Seg_2985" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T799" id="Seg_2987" n="HIAT:w" s="T798">agaj</ts>
                  <nts id="Seg_2988" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T800" id="Seg_2990" n="HIAT:w" s="T799">keːspite</ts>
                  <nts id="Seg_2991" n="HIAT:ip">.</nts>
                  <nts id="Seg_2992" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T805" id="Seg_2994" n="HIAT:u" s="T800">
                  <ts e="T801" id="Seg_2996" n="HIAT:w" s="T800">Dʼi͡ete-u͡ota</ts>
                  <nts id="Seg_2997" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T802" id="Seg_2999" n="HIAT:w" s="T801">kanna</ts>
                  <nts id="Seg_3000" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T803" id="Seg_3002" n="HIAT:w" s="T802">da</ts>
                  <nts id="Seg_3003" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T804" id="Seg_3005" n="HIAT:w" s="T803">barbɨttarɨn</ts>
                  <nts id="Seg_3006" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T805" id="Seg_3008" n="HIAT:w" s="T804">bilbetiler</ts>
                  <nts id="Seg_3009" n="HIAT:ip">.</nts>
                  <nts id="Seg_3010" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T815" id="Seg_3012" n="HIAT:u" s="T805">
                  <ts e="T806" id="Seg_3014" n="HIAT:w" s="T805">Purgaː</ts>
                  <nts id="Seg_3015" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T807" id="Seg_3017" n="HIAT:w" s="T806">iččilere</ts>
                  <nts id="Seg_3018" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T808" id="Seg_3020" n="HIAT:w" s="T807">öhürgenen</ts>
                  <nts id="Seg_3021" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T809" id="Seg_3023" n="HIAT:w" s="T808">bu</ts>
                  <nts id="Seg_3024" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T810" id="Seg_3026" n="HIAT:w" s="T809">dʼi͡eni</ts>
                  <nts id="Seg_3027" n="HIAT:ip">,</nts>
                  <nts id="Seg_3028" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T810.tx.1" id="Seg_3030" n="HIAT:w" s="T810">Heliː</ts>
                  <nts id="Seg_3031" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T811" id="Seg_3033" n="HIAT:w" s="T810.tx.1">Kuru</ts>
                  <nts id="Seg_3034" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T812" id="Seg_3036" n="HIAT:w" s="T811">ijeliːni-agalɨːnɨ</ts>
                  <nts id="Seg_3037" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T813" id="Seg_3039" n="HIAT:w" s="T812">kötüten</ts>
                  <nts id="Seg_3040" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T814" id="Seg_3042" n="HIAT:w" s="T813">bolohonnon</ts>
                  <nts id="Seg_3043" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T815" id="Seg_3045" n="HIAT:w" s="T814">ispittere</ts>
                  <nts id="Seg_3046" n="HIAT:ip">.</nts>
                  <nts id="Seg_3047" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T819" id="Seg_3049" n="HIAT:u" s="T815">
                  <ts e="T816" id="Seg_3051" n="HIAT:w" s="T815">Araj</ts>
                  <nts id="Seg_3052" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T816.tx.1" id="Seg_3054" n="HIAT:w" s="T816">Timir</ts>
                  <nts id="Seg_3055" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T817" id="Seg_3057" n="HIAT:w" s="T816.tx.1">Haːpka</ts>
                  <nts id="Seg_3058" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T818" id="Seg_3060" n="HIAT:w" s="T817">ijeliːn-agalɨːn</ts>
                  <nts id="Seg_3061" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T819" id="Seg_3063" n="HIAT:w" s="T818">kaːlbɨt</ts>
                  <nts id="Seg_3064" n="HIAT:ip">.</nts>
                  <nts id="Seg_3065" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T825" id="Seg_3067" n="HIAT:u" s="T819">
                  <ts e="T820" id="Seg_3069" n="HIAT:w" s="T819">Purgaː</ts>
                  <nts id="Seg_3070" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T821" id="Seg_3072" n="HIAT:w" s="T820">kɨːhɨn</ts>
                  <nts id="Seg_3073" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T822" id="Seg_3075" n="HIAT:w" s="T821">dʼaktardanan</ts>
                  <nts id="Seg_3076" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T823" id="Seg_3078" n="HIAT:w" s="T822">bajan-tajan</ts>
                  <nts id="Seg_3079" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T824" id="Seg_3081" n="HIAT:w" s="T823">olorbuta</ts>
                  <nts id="Seg_3082" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T825" id="Seg_3084" n="HIAT:w" s="T824">ühü</ts>
                  <nts id="Seg_3085" n="HIAT:ip">.</nts>
                  <nts id="Seg_3086" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T826" id="Seg_3088" n="HIAT:u" s="T825">
                  <ts e="T826" id="Seg_3090" n="HIAT:w" s="T825">Elete</ts>
                  <nts id="Seg_3091" n="HIAT:ip">.</nts>
                  <nts id="Seg_3092" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T826" id="Seg_3093" n="sc" s="T0">
               <ts e="T1" id="Seg_3095" n="e" s="T0">Timir Haːpka </ts>
               <ts e="T2" id="Seg_3097" n="e" s="T1">onuga </ts>
               <ts e="T3" id="Seg_3099" n="e" s="T2">Heliː Kur </ts>
               <ts e="T4" id="Seg_3101" n="e" s="T3">di͡en </ts>
               <ts e="T5" id="Seg_3103" n="e" s="T4">ikki </ts>
               <ts e="T6" id="Seg_3105" n="e" s="T5">u͡ol </ts>
               <ts e="T7" id="Seg_3107" n="e" s="T6">kihiler </ts>
               <ts e="T8" id="Seg_3109" n="e" s="T7">olorbuttar. </ts>
               <ts e="T9" id="Seg_3111" n="e" s="T8">Ikki͡en </ts>
               <ts e="T10" id="Seg_3113" n="e" s="T9">agalaːktar, </ts>
               <ts e="T11" id="Seg_3115" n="e" s="T10">ijeleːkter. </ts>
               <ts e="T12" id="Seg_3117" n="e" s="T11">Dʼe </ts>
               <ts e="T13" id="Seg_3119" n="e" s="T12">bu </ts>
               <ts e="T14" id="Seg_3121" n="e" s="T13">oloronnor </ts>
               <ts e="T15" id="Seg_3123" n="e" s="T14">Heliː Kura </ts>
               <ts e="T16" id="Seg_3125" n="e" s="T15">diːr </ts>
               <ts e="T17" id="Seg_3127" n="e" s="T16">Timir Haːpkatɨn: </ts>
               <ts e="T18" id="Seg_3129" n="e" s="T17">"Dogoːr, </ts>
               <ts e="T19" id="Seg_3131" n="e" s="T18">bu </ts>
               <ts e="T20" id="Seg_3133" n="e" s="T19">bihigi </ts>
               <ts e="T21" id="Seg_3135" n="e" s="T20">tu͡ok </ts>
               <ts e="T22" id="Seg_3137" n="e" s="T21">ideleːk </ts>
               <ts e="T23" id="Seg_3139" n="e" s="T22">bu͡olabɨt? </ts>
               <ts e="T24" id="Seg_3141" n="e" s="T23">Togo </ts>
               <ts e="T25" id="Seg_3143" n="e" s="T24">ide </ts>
               <ts e="T26" id="Seg_3145" n="e" s="T25">idebitin </ts>
               <ts e="T27" id="Seg_3147" n="e" s="T26">bilsibeppit?" </ts>
               <ts e="T28" id="Seg_3149" n="e" s="T27">Mekti͡e </ts>
               <ts e="T29" id="Seg_3151" n="e" s="T28">bɨldʼahaːrɨ </ts>
               <ts e="T30" id="Seg_3153" n="e" s="T29">gɨnar </ts>
               <ts e="T31" id="Seg_3155" n="e" s="T30">bɨhɨːlaːk. </ts>
               <ts e="T32" id="Seg_3157" n="e" s="T31">Dogoro </ts>
               <ts e="T33" id="Seg_3159" n="e" s="T32">"eː, </ts>
               <ts e="T34" id="Seg_3161" n="e" s="T33">dogor, </ts>
               <ts e="T35" id="Seg_3163" n="e" s="T34">tu͡okputun </ts>
               <ts e="T36" id="Seg_3165" n="e" s="T35">bɨldʼahan </ts>
               <ts e="T37" id="Seg_3167" n="e" s="T36">mekti͡e </ts>
               <ts e="T38" id="Seg_3169" n="e" s="T37">bɨldʼahɨ͡akpɨtɨj" </ts>
               <ts e="T39" id="Seg_3171" n="e" s="T38">di͡en </ts>
               <ts e="T40" id="Seg_3173" n="e" s="T39">bu͡olummat. </ts>
               <ts e="T41" id="Seg_3175" n="e" s="T40">Dogoro </ts>
               <ts e="T42" id="Seg_3177" n="e" s="T41">tehippet: </ts>
               <ts e="T43" id="Seg_3179" n="e" s="T42">"Togo, </ts>
               <ts e="T44" id="Seg_3181" n="e" s="T43">dogor! </ts>
               <ts e="T45" id="Seg_3183" n="e" s="T44">En </ts>
               <ts e="T46" id="Seg_3185" n="e" s="T45">idegin </ts>
               <ts e="T47" id="Seg_3187" n="e" s="T46">bili͡ekpin </ts>
               <ts e="T48" id="Seg_3189" n="e" s="T47">bagarabɨn!" </ts>
               <ts e="T49" id="Seg_3191" n="e" s="T48">Dogoro </ts>
               <ts e="T50" id="Seg_3193" n="e" s="T49">bu͡olummat. </ts>
               <ts e="T51" id="Seg_3195" n="e" s="T50">Heliː Kur </ts>
               <ts e="T52" id="Seg_3197" n="e" s="T51">"min </ts>
               <ts e="T53" id="Seg_3199" n="e" s="T52">kürü͡öm </ts>
               <ts e="T54" id="Seg_3201" n="e" s="T53">enʼigitten, </ts>
               <ts e="T55" id="Seg_3203" n="e" s="T54">bat </ts>
               <ts e="T56" id="Seg_3205" n="e" s="T55">minʼiːgin, </ts>
               <ts e="T57" id="Seg_3207" n="e" s="T56">min </ts>
               <ts e="T58" id="Seg_3209" n="e" s="T57">kajdak </ts>
               <ts e="T59" id="Seg_3211" n="e" s="T58">bu͡olabɨn </ts>
               <ts e="T60" id="Seg_3213" n="e" s="T59">da, </ts>
               <ts e="T61" id="Seg_3215" n="e" s="T60">hogordukkaːn </ts>
               <ts e="T62" id="Seg_3217" n="e" s="T61">bu͡olan </ts>
               <ts e="T63" id="Seg_3219" n="e" s="T62">is" </ts>
               <ts e="T64" id="Seg_3221" n="e" s="T63">diːr. </ts>
               <ts e="T65" id="Seg_3223" n="e" s="T64">Dogoro: </ts>
               <ts e="T66" id="Seg_3225" n="e" s="T65">"Batɨ͡aktɨbɨn </ts>
               <ts e="T67" id="Seg_3227" n="e" s="T66">duː, </ts>
               <ts e="T68" id="Seg_3229" n="e" s="T67">ajɨlaːgɨn." </ts>
               <ts e="T69" id="Seg_3231" n="e" s="T68">"Harsi͡erda </ts>
               <ts e="T70" id="Seg_3233" n="e" s="T69">erdehit </ts>
               <ts e="T71" id="Seg_3235" n="e" s="T70">kihi </ts>
               <ts e="T72" id="Seg_3237" n="e" s="T71">turar </ts>
               <ts e="T73" id="Seg_3239" n="e" s="T72">kemiger </ts>
               <ts e="T74" id="Seg_3241" n="e" s="T73">bataːr </ts>
               <ts e="T75" id="Seg_3243" n="e" s="T74">minʼigin", </ts>
               <ts e="T76" id="Seg_3245" n="e" s="T75">diːr </ts>
               <ts e="T77" id="Seg_3247" n="e" s="T76">Heliː Kur. </ts>
               <ts e="T78" id="Seg_3249" n="e" s="T77">Innʼe </ts>
               <ts e="T79" id="Seg_3251" n="e" s="T78">dehen </ts>
               <ts e="T80" id="Seg_3253" n="e" s="T79">araksallar </ts>
               <ts e="T81" id="Seg_3255" n="e" s="T80">dʼi͡e </ts>
               <ts e="T82" id="Seg_3257" n="e" s="T81">dʼi͡eleriger. </ts>
               <ts e="T83" id="Seg_3259" n="e" s="T82">Timir Haːpka </ts>
               <ts e="T84" id="Seg_3261" n="e" s="T83">taksan </ts>
               <ts e="T85" id="Seg_3263" n="e" s="T84">utuja </ts>
               <ts e="T86" id="Seg_3265" n="e" s="T85">hɨtar. </ts>
               <ts e="T87" id="Seg_3267" n="e" s="T86">Uhuktan </ts>
               <ts e="T88" id="Seg_3269" n="e" s="T87">keler: </ts>
               <ts e="T89" id="Seg_3271" n="e" s="T88">"Dogorum </ts>
               <ts e="T90" id="Seg_3273" n="e" s="T89">baːra </ts>
               <ts e="T91" id="Seg_3275" n="e" s="T90">dʼürü?" </ts>
               <ts e="T92" id="Seg_3277" n="e" s="T91">Dogorugar </ts>
               <ts e="T93" id="Seg_3279" n="e" s="T92">kelbite, </ts>
               <ts e="T94" id="Seg_3281" n="e" s="T93">Heliː Kur </ts>
               <ts e="T95" id="Seg_3283" n="e" s="T94">orono </ts>
               <ts e="T96" id="Seg_3285" n="e" s="T95">čoŋkojo </ts>
               <ts e="T97" id="Seg_3287" n="e" s="T96">hɨtar — </ts>
               <ts e="T98" id="Seg_3289" n="e" s="T97">baran </ts>
               <ts e="T99" id="Seg_3291" n="e" s="T98">kaːlbɨt. </ts>
               <ts e="T100" id="Seg_3293" n="e" s="T99">Timir Haːpka </ts>
               <ts e="T101" id="Seg_3295" n="e" s="T100">hu͡ol </ts>
               <ts e="T102" id="Seg_3297" n="e" s="T101">kajar, </ts>
               <ts e="T103" id="Seg_3299" n="e" s="T102">hu͡olu </ts>
               <ts e="T104" id="Seg_3301" n="e" s="T103">bulbat, </ts>
               <ts e="T105" id="Seg_3303" n="e" s="T104">elete </ts>
               <ts e="T106" id="Seg_3305" n="e" s="T105">bu͡olar. </ts>
               <ts e="T107" id="Seg_3307" n="e" s="T106">Kiːren </ts>
               <ts e="T108" id="Seg_3309" n="e" s="T107">oronun </ts>
               <ts e="T109" id="Seg_3311" n="e" s="T108">kuptujar. </ts>
               <ts e="T110" id="Seg_3313" n="e" s="T109">Heliː Kur </ts>
               <ts e="T111" id="Seg_3315" n="e" s="T110">oronun </ts>
               <ts e="T112" id="Seg_3317" n="e" s="T111">annɨttan </ts>
               <ts e="T113" id="Seg_3319" n="e" s="T112">üːteːn </ts>
               <ts e="T114" id="Seg_3321" n="e" s="T113">kutujagɨn </ts>
               <ts e="T115" id="Seg_3323" n="e" s="T114">koroːno </ts>
               <ts e="T116" id="Seg_3325" n="e" s="T115">čoŋojo </ts>
               <ts e="T117" id="Seg_3327" n="e" s="T116">hɨtar, </ts>
               <ts e="T118" id="Seg_3329" n="e" s="T117">atɨn </ts>
               <ts e="T119" id="Seg_3331" n="e" s="T118">hu͡ol </ts>
               <ts e="T120" id="Seg_3333" n="e" s="T119">hu͡ok. </ts>
               <ts e="T121" id="Seg_3335" n="e" s="T120">Timir Haːpka </ts>
               <ts e="T122" id="Seg_3337" n="e" s="T121">kutujak </ts>
               <ts e="T123" id="Seg_3339" n="e" s="T122">bu͡olla </ts>
               <ts e="T124" id="Seg_3341" n="e" s="T123">da </ts>
               <ts e="T125" id="Seg_3343" n="e" s="T124">koroːnunan </ts>
               <ts e="T126" id="Seg_3345" n="e" s="T125">tüste. </ts>
               <ts e="T127" id="Seg_3347" n="e" s="T126">ɨnaraːŋŋɨta, </ts>
               <ts e="T128" id="Seg_3349" n="e" s="T127">koroːnton </ts>
               <ts e="T129" id="Seg_3351" n="e" s="T128">taksaːt, </ts>
               <ts e="T130" id="Seg_3353" n="e" s="T129">goronu͡ok </ts>
               <ts e="T131" id="Seg_3355" n="e" s="T130">bu͡olaːt </ts>
               <ts e="T132" id="Seg_3357" n="e" s="T131">ɨstanan </ts>
               <ts e="T133" id="Seg_3359" n="e" s="T132">kaːlar. </ts>
               <ts e="T134" id="Seg_3361" n="e" s="T133">Timir Haːpka </ts>
               <ts e="T135" id="Seg_3363" n="e" s="T134">goronu͡ok </ts>
               <ts e="T136" id="Seg_3365" n="e" s="T135">bu͡olaːt </ts>
               <ts e="T137" id="Seg_3367" n="e" s="T136">batan </ts>
               <ts e="T138" id="Seg_3369" n="e" s="T137">iher. </ts>
               <ts e="T139" id="Seg_3371" n="e" s="T138">Huk </ts>
               <ts e="T140" id="Seg_3373" n="e" s="T139">kötüː </ts>
               <ts e="T141" id="Seg_3375" n="e" s="T140">baran </ts>
               <ts e="T142" id="Seg_3377" n="e" s="T141">iste. </ts>
               <ts e="T143" id="Seg_3379" n="e" s="T142">Bu </ts>
               <ts e="T144" id="Seg_3381" n="e" s="T143">goronu͡ok </ts>
               <ts e="T145" id="Seg_3383" n="e" s="T144">hu͡ola </ts>
               <ts e="T146" id="Seg_3385" n="e" s="T145">büter </ts>
               <ts e="T147" id="Seg_3387" n="e" s="T146">da, </ts>
               <ts e="T148" id="Seg_3389" n="e" s="T147">orto </ts>
               <ts e="T149" id="Seg_3391" n="e" s="T148">dojdu </ts>
               <ts e="T150" id="Seg_3393" n="e" s="T149">kɨrsa </ts>
               <ts e="T151" id="Seg_3395" n="e" s="T150">hu͡ola </ts>
               <ts e="T152" id="Seg_3397" n="e" s="T151">bara </ts>
               <ts e="T153" id="Seg_3399" n="e" s="T152">turda. </ts>
               <ts e="T154" id="Seg_3401" n="e" s="T153">Dogoro </ts>
               <ts e="T155" id="Seg_3403" n="e" s="T154">halla </ts>
               <ts e="T156" id="Seg_3405" n="e" s="T155">iste. </ts>
               <ts e="T157" id="Seg_3407" n="e" s="T156">Timir Haːpka </ts>
               <ts e="T158" id="Seg_3409" n="e" s="T157">kɨrsa </ts>
               <ts e="T159" id="Seg_3411" n="e" s="T158">bu͡olar </ts>
               <ts e="T160" id="Seg_3413" n="e" s="T159">da, </ts>
               <ts e="T161" id="Seg_3415" n="e" s="T160">ojon </ts>
               <ts e="T162" id="Seg_3417" n="e" s="T161">iste. </ts>
               <ts e="T163" id="Seg_3419" n="e" s="T162">Biːr </ts>
               <ts e="T164" id="Seg_3421" n="e" s="T163">hirge </ts>
               <ts e="T165" id="Seg_3423" n="e" s="T164">tüspüt </ts>
               <ts e="T166" id="Seg_3425" n="e" s="T165">da, </ts>
               <ts e="T167" id="Seg_3427" n="e" s="T166">Heliː Kura </ts>
               <ts e="T168" id="Seg_3429" n="e" s="T167">uskaːn </ts>
               <ts e="T169" id="Seg_3431" n="e" s="T168">bu͡olan </ts>
               <ts e="T170" id="Seg_3433" n="e" s="T169">ojbut. </ts>
               <ts e="T171" id="Seg_3435" n="e" s="T170">Kennikite </ts>
               <ts e="T172" id="Seg_3437" n="e" s="T171">uskaːn </ts>
               <ts e="T173" id="Seg_3439" n="e" s="T172">bu͡olan </ts>
               <ts e="T174" id="Seg_3441" n="e" s="T173">batta. </ts>
               <ts e="T175" id="Seg_3443" n="e" s="T174">Heliː Kur </ts>
               <ts e="T176" id="Seg_3445" n="e" s="T175">hahɨl </ts>
               <ts e="T177" id="Seg_3447" n="e" s="T176">bu͡olla </ts>
               <ts e="T178" id="Seg_3449" n="e" s="T177">onton. </ts>
               <ts e="T179" id="Seg_3451" n="e" s="T178">Kennikite </ts>
               <ts e="T180" id="Seg_3453" n="e" s="T179">emi͡e </ts>
               <ts e="T181" id="Seg_3455" n="e" s="T180">hahɨl </ts>
               <ts e="T182" id="Seg_3457" n="e" s="T181">bu͡olan </ts>
               <ts e="T183" id="Seg_3459" n="e" s="T182">hu͡olun </ts>
               <ts e="T184" id="Seg_3461" n="e" s="T183">agaj </ts>
               <ts e="T185" id="Seg_3463" n="e" s="T184">ütükten </ts>
               <ts e="T186" id="Seg_3465" n="e" s="T185">iste. </ts>
               <ts e="T187" id="Seg_3467" n="e" s="T186">Maːnɨta </ts>
               <ts e="T188" id="Seg_3469" n="e" s="T187">börö </ts>
               <ts e="T189" id="Seg_3471" n="e" s="T188">bu͡olan </ts>
               <ts e="T190" id="Seg_3473" n="e" s="T189">kötön </ts>
               <ts e="T191" id="Seg_3475" n="e" s="T190">nʼulujbut. </ts>
               <ts e="T192" id="Seg_3477" n="e" s="T191">Kennikite </ts>
               <ts e="T193" id="Seg_3479" n="e" s="T192">högö </ts>
               <ts e="T194" id="Seg_3481" n="e" s="T193">iste: </ts>
               <ts e="T195" id="Seg_3483" n="e" s="T194">"Mantɨkaja </ts>
               <ts e="T196" id="Seg_3485" n="e" s="T195">batɨmɨna </ts>
               <ts e="T197" id="Seg_3487" n="e" s="T196">kördömmüt </ts>
               <ts e="T198" id="Seg_3489" n="e" s="T197">bu͡ollaga", </ts>
               <ts e="T199" id="Seg_3491" n="e" s="T198">diːr. </ts>
               <ts e="T200" id="Seg_3493" n="e" s="T199">Börö </ts>
               <ts e="T201" id="Seg_3495" n="e" s="T200">bu͡olan </ts>
               <ts e="T202" id="Seg_3497" n="e" s="T201">batan </ts>
               <ts e="T203" id="Seg_3499" n="e" s="T202">iste. </ts>
               <ts e="T204" id="Seg_3501" n="e" s="T203">Töhönü-kaččanɨ </ts>
               <ts e="T205" id="Seg_3503" n="e" s="T204">barbɨta </ts>
               <ts e="T206" id="Seg_3505" n="e" s="T205">billibet. </ts>
               <ts e="T207" id="Seg_3507" n="e" s="T206">Heliː Kur </ts>
               <ts e="T208" id="Seg_3509" n="e" s="T207">hi͡egen </ts>
               <ts e="T209" id="Seg_3511" n="e" s="T208">bu͡olan </ts>
               <ts e="T210" id="Seg_3513" n="e" s="T209">leppeje </ts>
               <ts e="T211" id="Seg_3515" n="e" s="T210">turbut. </ts>
               <ts e="T212" id="Seg_3517" n="e" s="T211">Kennikite </ts>
               <ts e="T213" id="Seg_3519" n="e" s="T212">emi͡e, </ts>
               <ts e="T214" id="Seg_3521" n="e" s="T213">bi͡ek </ts>
               <ts e="T215" id="Seg_3523" n="e" s="T214">högö-högö, </ts>
               <ts e="T216" id="Seg_3525" n="e" s="T215">hi͡egen </ts>
               <ts e="T217" id="Seg_3527" n="e" s="T216">bu͡olan </ts>
               <ts e="T218" id="Seg_3529" n="e" s="T217">batan </ts>
               <ts e="T219" id="Seg_3531" n="e" s="T218">iste. </ts>
               <ts e="T220" id="Seg_3533" n="e" s="T219">Hi͡egenin </ts>
               <ts e="T221" id="Seg_3535" n="e" s="T220">hu͡ola </ts>
               <ts e="T222" id="Seg_3537" n="e" s="T221">büteːtin, </ts>
               <ts e="T223" id="Seg_3539" n="e" s="T222">ebekeː </ts>
               <ts e="T224" id="Seg_3541" n="e" s="T223">bu͡olan </ts>
               <ts e="T225" id="Seg_3543" n="e" s="T224">lippejbit. </ts>
               <ts e="T226" id="Seg_3545" n="e" s="T225">Ehe </ts>
               <ts e="T227" id="Seg_3547" n="e" s="T226">bu͡olan </ts>
               <ts e="T228" id="Seg_3549" n="e" s="T227">batan </ts>
               <ts e="T229" id="Seg_3551" n="e" s="T228">iste </ts>
               <ts e="T230" id="Seg_3553" n="e" s="T229">Timir Haːpka. </ts>
               <ts e="T231" id="Seg_3555" n="e" s="T230">Bu </ts>
               <ts e="T232" id="Seg_3557" n="e" s="T231">tukkarɨ </ts>
               <ts e="T233" id="Seg_3559" n="e" s="T232">huk </ts>
               <ts e="T234" id="Seg_3561" n="e" s="T233">kötüː </ts>
               <ts e="T235" id="Seg_3563" n="e" s="T234">ajan. </ts>
               <ts e="T236" id="Seg_3565" n="e" s="T235">Ehe </ts>
               <ts e="T237" id="Seg_3567" n="e" s="T236">hu͡ola </ts>
               <ts e="T238" id="Seg_3569" n="e" s="T237">baranar </ts>
               <ts e="T239" id="Seg_3571" n="e" s="T238">da, </ts>
               <ts e="T240" id="Seg_3573" n="e" s="T239">kɨːl </ts>
               <ts e="T241" id="Seg_3575" n="e" s="T240">atɨːra </ts>
               <ts e="T242" id="Seg_3577" n="e" s="T241">bu͡olan </ts>
               <ts e="T243" id="Seg_3579" n="e" s="T242">ojon </ts>
               <ts e="T244" id="Seg_3581" n="e" s="T243">kaːlaːktaːbɨt. </ts>
               <ts e="T245" id="Seg_3583" n="e" s="T244">Högö </ts>
               <ts e="T246" id="Seg_3585" n="e" s="T245">agaj </ts>
               <ts e="T247" id="Seg_3587" n="e" s="T246">iher, </ts>
               <ts e="T248" id="Seg_3589" n="e" s="T247">bi͡ek </ts>
               <ts e="T249" id="Seg_3591" n="e" s="T248">batar </ts>
               <ts e="T250" id="Seg_3593" n="e" s="T249">dogoro. </ts>
               <ts e="T251" id="Seg_3595" n="e" s="T250">Timir Haːpka </ts>
               <ts e="T252" id="Seg_3597" n="e" s="T251">kɨːl </ts>
               <ts e="T253" id="Seg_3599" n="e" s="T252">atɨːra </ts>
               <ts e="T254" id="Seg_3601" n="e" s="T253">bu͡olla. </ts>
               <ts e="T255" id="Seg_3603" n="e" s="T254">"Ideŋ </ts>
               <ts e="T256" id="Seg_3605" n="e" s="T255">da </ts>
               <ts e="T257" id="Seg_3607" n="e" s="T256">elete </ts>
               <ts e="T258" id="Seg_3609" n="e" s="T257">ini, </ts>
               <ts e="T259" id="Seg_3611" n="e" s="T258">hiteː </ts>
               <ts e="T260" id="Seg_3613" n="e" s="T259">inibin </ts>
               <ts e="T261" id="Seg_3615" n="e" s="T260">hubukaːn", </ts>
               <ts e="T262" id="Seg_3617" n="e" s="T261">diː </ts>
               <ts e="T263" id="Seg_3619" n="e" s="T262">hanɨːr. </ts>
               <ts e="T264" id="Seg_3621" n="e" s="T263">Körbüte, </ts>
               <ts e="T265" id="Seg_3623" n="e" s="T264">biːr </ts>
               <ts e="T266" id="Seg_3625" n="e" s="T265">hirge </ts>
               <ts e="T267" id="Seg_3627" n="e" s="T266">dogoro </ts>
               <ts e="T268" id="Seg_3629" n="e" s="T267">balčajan </ts>
               <ts e="T269" id="Seg_3631" n="e" s="T268">turar. </ts>
               <ts e="T270" id="Seg_3633" n="e" s="T269">Čugahaːn </ts>
               <ts e="T271" id="Seg_3635" n="e" s="T270">körbüte, </ts>
               <ts e="T272" id="Seg_3637" n="e" s="T271">ikki </ts>
               <ts e="T273" id="Seg_3639" n="e" s="T272">agaj </ts>
               <ts e="T274" id="Seg_3641" n="e" s="T273">ataga </ts>
               <ts e="T275" id="Seg_3643" n="e" s="T274">čočojon </ts>
               <ts e="T276" id="Seg_3645" n="e" s="T275">kaːlbɨt, </ts>
               <ts e="T277" id="Seg_3647" n="e" s="T276">hühü͡ökterinen </ts>
               <ts e="T278" id="Seg_3649" n="e" s="T277">bɨhan, </ts>
               <ts e="T279" id="Seg_3651" n="e" s="T278">tuːsa </ts>
               <ts e="T280" id="Seg_3653" n="e" s="T279">ataktarɨn </ts>
               <ts e="T281" id="Seg_3655" n="e" s="T280">čekeliten </ts>
               <ts e="T282" id="Seg_3657" n="e" s="T281">keːspit. </ts>
               <ts e="T283" id="Seg_3659" n="e" s="T282">Hol </ts>
               <ts e="T284" id="Seg_3661" n="e" s="T283">kordukkaːn </ts>
               <ts e="T285" id="Seg_3663" n="e" s="T284">Timir Haːpka </ts>
               <ts e="T286" id="Seg_3665" n="e" s="T285">atagɨn </ts>
               <ts e="T287" id="Seg_3667" n="e" s="T286">bɨhaːt, </ts>
               <ts e="T288" id="Seg_3669" n="e" s="T287">tuːsa </ts>
               <ts e="T289" id="Seg_3671" n="e" s="T288">bu͡olan </ts>
               <ts e="T290" id="Seg_3673" n="e" s="T289">čekelijen </ts>
               <ts e="T291" id="Seg_3675" n="e" s="T290">iste. </ts>
               <ts e="T292" id="Seg_3677" n="e" s="T291">Töhö </ts>
               <ts e="T293" id="Seg_3679" n="e" s="T292">da </ts>
               <ts e="T294" id="Seg_3681" n="e" s="T293">bu͡olumuja, </ts>
               <ts e="T295" id="Seg_3683" n="e" s="T294">tuːsa </ts>
               <ts e="T296" id="Seg_3685" n="e" s="T295">köppöjö </ts>
               <ts e="T297" id="Seg_3687" n="e" s="T296">hɨtar. </ts>
               <ts e="T298" id="Seg_3689" n="e" s="T297">Ü͡örer, </ts>
               <ts e="T299" id="Seg_3691" n="e" s="T298">"hittim </ts>
               <ts e="T300" id="Seg_3693" n="e" s="T299">eː", </ts>
               <ts e="T301" id="Seg_3695" n="e" s="T300">diːr. </ts>
               <ts e="T302" id="Seg_3697" n="e" s="T301">Bu </ts>
               <ts e="T303" id="Seg_3699" n="e" s="T302">tuːsaga </ts>
               <ts e="T304" id="Seg_3701" n="e" s="T303">tiːjbite, </ts>
               <ts e="T305" id="Seg_3703" n="e" s="T304">tuːsa </ts>
               <ts e="T306" id="Seg_3705" n="e" s="T305">postu͡oj </ts>
               <ts e="T307" id="Seg_3707" n="e" s="T306">agaj </ts>
               <ts e="T308" id="Seg_3709" n="e" s="T307">hɨtar, </ts>
               <ts e="T309" id="Seg_3711" n="e" s="T308">mejiːte </ts>
               <ts e="T310" id="Seg_3713" n="e" s="T309">agaj </ts>
               <ts e="T311" id="Seg_3715" n="e" s="T310">čekelijen </ts>
               <ts e="T312" id="Seg_3717" n="e" s="T311">kaːlbɨt. </ts>
               <ts e="T313" id="Seg_3719" n="e" s="T312">"Kaja </ts>
               <ts e="T314" id="Seg_3721" n="e" s="T313">doː, </ts>
               <ts e="T315" id="Seg_3723" n="e" s="T314">bu </ts>
               <ts e="T316" id="Seg_3725" n="e" s="T315">togo </ts>
               <ts e="T317" id="Seg_3727" n="e" s="T316">kerdiner </ts>
               <ts e="T318" id="Seg_3729" n="e" s="T317">bejetin", </ts>
               <ts e="T319" id="Seg_3731" n="e" s="T318">di͡en </ts>
               <ts e="T320" id="Seg_3733" n="e" s="T319">hanɨːr. </ts>
               <ts e="T321" id="Seg_3735" n="e" s="T320">"Bugurdukkaːn </ts>
               <ts e="T322" id="Seg_3737" n="e" s="T321">tu͡ok </ts>
               <ts e="T323" id="Seg_3739" n="e" s="T322">mökkü͡önej", </ts>
               <ts e="T324" id="Seg_3741" n="e" s="T323">di͡en </ts>
               <ts e="T325" id="Seg_3743" n="e" s="T324">oduːrguː </ts>
               <ts e="T326" id="Seg_3745" n="e" s="T325">iste. </ts>
               <ts e="T327" id="Seg_3747" n="e" s="T326">Mejiː </ts>
               <ts e="T328" id="Seg_3749" n="e" s="T327">bu͡olan </ts>
               <ts e="T329" id="Seg_3751" n="e" s="T328">čekelijen </ts>
               <ts e="T330" id="Seg_3753" n="e" s="T329">iste. </ts>
               <ts e="T331" id="Seg_3755" n="e" s="T330">Araj </ts>
               <ts e="T332" id="Seg_3757" n="e" s="T331">mejiː </ts>
               <ts e="T333" id="Seg_3759" n="e" s="T332">čankaja </ts>
               <ts e="T334" id="Seg_3761" n="e" s="T333">hɨtar. </ts>
               <ts e="T335" id="Seg_3763" n="e" s="T334">"Dʼe </ts>
               <ts e="T336" id="Seg_3765" n="e" s="T335">hittim </ts>
               <ts e="T337" id="Seg_3767" n="e" s="T336">ini!" </ts>
               <ts e="T338" id="Seg_3769" n="e" s="T337">Araj </ts>
               <ts e="T339" id="Seg_3771" n="e" s="T338">ikki </ts>
               <ts e="T340" id="Seg_3773" n="e" s="T339">karaga </ts>
               <ts e="T341" id="Seg_3775" n="e" s="T340">uhullan </ts>
               <ts e="T342" id="Seg_3777" n="e" s="T341">barbɨt. </ts>
               <ts e="T343" id="Seg_3779" n="e" s="T342">"Oroː, </ts>
               <ts e="T344" id="Seg_3781" n="e" s="T343">bu </ts>
               <ts e="T345" id="Seg_3783" n="e" s="T344">tu͡ok </ts>
               <ts e="T346" id="Seg_3785" n="e" s="T345">kihitej? </ts>
               <ts e="T347" id="Seg_3787" n="e" s="T346">Ajɨːnɨ </ts>
               <ts e="T348" id="Seg_3789" n="e" s="T347">batabɨn </ts>
               <ts e="T349" id="Seg_3791" n="e" s="T348">duː, </ts>
               <ts e="T350" id="Seg_3793" n="e" s="T349">abaːhɨnɨ </ts>
               <ts e="T351" id="Seg_3795" n="e" s="T350">duː", </ts>
               <ts e="T352" id="Seg_3797" n="e" s="T351">diːr. </ts>
               <ts e="T353" id="Seg_3799" n="e" s="T352">Ikki </ts>
               <ts e="T354" id="Seg_3801" n="e" s="T353">karaga </ts>
               <ts e="T355" id="Seg_3803" n="e" s="T354">čekerijen </ts>
               <ts e="T356" id="Seg_3805" n="e" s="T355">iste </ts>
               <ts e="T357" id="Seg_3807" n="e" s="T356">karaktar </ts>
               <ts e="T358" id="Seg_3809" n="e" s="T357">hu͡ollarɨnan. </ts>
               <ts e="T359" id="Seg_3811" n="e" s="T358">Kahan </ts>
               <ts e="T360" id="Seg_3813" n="e" s="T359">ere </ts>
               <ts e="T361" id="Seg_3815" n="e" s="T360">bu </ts>
               <ts e="T362" id="Seg_3817" n="e" s="T361">ikki </ts>
               <ts e="T363" id="Seg_3819" n="e" s="T362">karak </ts>
               <ts e="T364" id="Seg_3821" n="e" s="T363">hu͡ola </ts>
               <ts e="T365" id="Seg_3823" n="e" s="T364">biːr </ts>
               <ts e="T366" id="Seg_3825" n="e" s="T365">dʼi͡ekeːŋŋe </ts>
               <ts e="T367" id="Seg_3827" n="e" s="T366">tijer. </ts>
               <ts e="T368" id="Seg_3829" n="e" s="T367">Bu </ts>
               <ts e="T369" id="Seg_3831" n="e" s="T368">tijen </ts>
               <ts e="T370" id="Seg_3833" n="e" s="T369">ikki </ts>
               <ts e="T371" id="Seg_3835" n="e" s="T370">karak </ts>
               <ts e="T372" id="Seg_3837" n="e" s="T371">hu͡ola </ts>
               <ts e="T373" id="Seg_3839" n="e" s="T372">dʼi͡ege </ts>
               <ts e="T374" id="Seg_3841" n="e" s="T373">kiːrbit. </ts>
               <ts e="T375" id="Seg_3843" n="e" s="T374">"Dʼe </ts>
               <ts e="T376" id="Seg_3845" n="e" s="T375">eleŋ </ts>
               <ts e="T377" id="Seg_3847" n="e" s="T376">ini!" </ts>
               <ts e="T378" id="Seg_3849" n="e" s="T377">Ikki </ts>
               <ts e="T379" id="Seg_3851" n="e" s="T378">kelin </ts>
               <ts e="T380" id="Seg_3853" n="e" s="T379">karak </ts>
               <ts e="T381" id="Seg_3855" n="e" s="T380">dʼi͡ege </ts>
               <ts e="T382" id="Seg_3857" n="e" s="T381">kiːrde. </ts>
               <ts e="T383" id="Seg_3859" n="e" s="T382">Kiːrbite </ts>
               <ts e="T384" id="Seg_3861" n="e" s="T383">uŋa </ts>
               <ts e="T385" id="Seg_3863" n="e" s="T384">di͡ekki </ts>
               <ts e="T386" id="Seg_3865" n="e" s="T385">oroŋŋo </ts>
               <ts e="T387" id="Seg_3867" n="e" s="T386">emeːksin </ts>
               <ts e="T388" id="Seg_3869" n="e" s="T387">oloror, </ts>
               <ts e="T389" id="Seg_3871" n="e" s="T388">karaktara </ts>
               <ts e="T390" id="Seg_3873" n="e" s="T389">ketegeriːn </ts>
               <ts e="T391" id="Seg_3875" n="e" s="T390">čagɨlɨha </ts>
               <ts e="T392" id="Seg_3877" n="e" s="T391">hɨtallar. </ts>
               <ts e="T393" id="Seg_3879" n="e" s="T392">Bili </ts>
               <ts e="T394" id="Seg_3881" n="e" s="T393">ikki </ts>
               <ts e="T395" id="Seg_3883" n="e" s="T394">karak </ts>
               <ts e="T396" id="Seg_3885" n="e" s="T395">kiːrbitiger </ts>
               <ts e="T397" id="Seg_3887" n="e" s="T396">inniki </ts>
               <ts e="T398" id="Seg_3889" n="e" s="T397">ikki </ts>
               <ts e="T399" id="Seg_3891" n="e" s="T398">karak </ts>
               <ts e="T400" id="Seg_3893" n="e" s="T399">hanalaːk: </ts>
               <ts e="T401" id="Seg_3895" n="e" s="T400">"O </ts>
               <ts e="T402" id="Seg_3897" n="e" s="T401">da, </ts>
               <ts e="T403" id="Seg_3899" n="e" s="T402">kihini </ts>
               <ts e="T404" id="Seg_3901" n="e" s="T403">batar </ts>
               <ts e="T405" id="Seg_3903" n="e" s="T404">kihi </ts>
               <ts e="T406" id="Seg_3905" n="e" s="T405">hɨlajaːččɨ. </ts>
               <ts e="T407" id="Seg_3907" n="e" s="T406">En </ts>
               <ts e="T408" id="Seg_3909" n="e" s="T407">hɨnnʼan. </ts>
               <ts e="T409" id="Seg_3911" n="e" s="T408">Min </ts>
               <ts e="T410" id="Seg_3913" n="e" s="T409">tuːsalarbɨtɨn, </ts>
               <ts e="T411" id="Seg_3915" n="e" s="T410">mejiːlerbitin </ts>
               <ts e="T412" id="Seg_3917" n="e" s="T411">komujtaːn </ts>
               <ts e="T413" id="Seg_3919" n="e" s="T412">egeli͡em", </ts>
               <ts e="T414" id="Seg_3921" n="e" s="T413">diːr. </ts>
               <ts e="T415" id="Seg_3923" n="e" s="T414">Dogoro </ts>
               <ts e="T416" id="Seg_3925" n="e" s="T415">Timir Haːpka </ts>
               <ts e="T417" id="Seg_3927" n="e" s="T416">onuga: </ts>
               <ts e="T418" id="Seg_3929" n="e" s="T417">"Bu </ts>
               <ts e="T419" id="Seg_3931" n="e" s="T418">ile </ts>
               <ts e="T420" id="Seg_3933" n="e" s="T419">öjünen </ts>
               <ts e="T421" id="Seg_3935" n="e" s="T420">hɨldʼar </ts>
               <ts e="T422" id="Seg_3937" n="e" s="T421">kihigin </ts>
               <ts e="T423" id="Seg_3939" n="e" s="T422">duː, </ts>
               <ts e="T424" id="Seg_3941" n="e" s="T423">hu͡ok </ts>
               <ts e="T425" id="Seg_3943" n="e" s="T424">duː? </ts>
               <ts e="T426" id="Seg_3945" n="e" s="T425">Bu </ts>
               <ts e="T427" id="Seg_3947" n="e" s="T426">bihigi </ts>
               <ts e="T428" id="Seg_3949" n="e" s="T427">kihi </ts>
               <ts e="T429" id="Seg_3951" n="e" s="T428">kɨ͡ajan </ts>
               <ts e="T430" id="Seg_3953" n="e" s="T429">tönnübet </ts>
               <ts e="T431" id="Seg_3955" n="e" s="T430">hiriger </ts>
               <ts e="T432" id="Seg_3957" n="e" s="T431">kellibit! </ts>
               <ts e="T433" id="Seg_3959" n="e" s="T432">Dʼe </ts>
               <ts e="T434" id="Seg_3961" n="e" s="T433">anɨ </ts>
               <ts e="T435" id="Seg_3963" n="e" s="T434">en </ts>
               <ts e="T436" id="Seg_3965" n="e" s="T435">bataːr </ts>
               <ts e="T437" id="Seg_3967" n="e" s="T436">minʼigin, </ts>
               <ts e="T438" id="Seg_3969" n="e" s="T437">hiteːr </ts>
               <ts e="T439" id="Seg_3971" n="e" s="T438">min </ts>
               <ts e="T440" id="Seg_3973" n="e" s="T439">idebin. </ts>
               <ts e="T441" id="Seg_3975" n="e" s="T440">Bilegi͡en, </ts>
               <ts e="T442" id="Seg_3977" n="e" s="T441">bu </ts>
               <ts e="T443" id="Seg_3979" n="e" s="T442">Purgaː </ts>
               <ts e="T444" id="Seg_3981" n="e" s="T443">iččitiger </ts>
               <ts e="T445" id="Seg_3983" n="e" s="T444">kellibit. </ts>
               <ts e="T446" id="Seg_3985" n="e" s="T445">Harsi͡erda </ts>
               <ts e="T447" id="Seg_3987" n="e" s="T446">erdehit </ts>
               <ts e="T448" id="Seg_3989" n="e" s="T447">kihi </ts>
               <ts e="T449" id="Seg_3991" n="e" s="T448">turar </ts>
               <ts e="T450" id="Seg_3993" n="e" s="T449">kemiger </ts>
               <ts e="T451" id="Seg_3995" n="e" s="T450">turan </ts>
               <ts e="T452" id="Seg_3997" n="e" s="T451">taksɨ͡am. </ts>
               <ts e="T453" id="Seg_3999" n="e" s="T452">Manɨga </ts>
               <ts e="T454" id="Seg_4001" n="e" s="T453">hüːstüː </ts>
               <ts e="T455" id="Seg_4003" n="e" s="T454">ölgöbüːnneːk </ts>
               <ts e="T456" id="Seg_4005" n="e" s="T455">hetiːleːk </ts>
               <ts e="T457" id="Seg_4007" n="e" s="T456">ikki </ts>
               <ts e="T458" id="Seg_4009" n="e" s="T457">kɨrgɨttar </ts>
               <ts e="T459" id="Seg_4011" n="e" s="T458">keli͡ektere. </ts>
               <ts e="T460" id="Seg_4013" n="e" s="T459">Manɨga </ts>
               <ts e="T461" id="Seg_4015" n="e" s="T460">min </ts>
               <ts e="T462" id="Seg_4017" n="e" s="T461">taksa </ts>
               <ts e="T463" id="Seg_4019" n="e" s="T462">kötömmün, </ts>
               <ts e="T464" id="Seg_4021" n="e" s="T463">bastakɨ </ts>
               <ts e="T465" id="Seg_4023" n="e" s="T464">kɨːhɨ </ts>
               <ts e="T466" id="Seg_4025" n="e" s="T465">nʼu͡oguhutun </ts>
               <ts e="T467" id="Seg_4027" n="e" s="T466">u͡oluguttan </ts>
               <ts e="T468" id="Seg_4029" n="e" s="T467">tutu͡om </ts>
               <ts e="T469" id="Seg_4031" n="e" s="T468">da </ts>
               <ts e="T470" id="Seg_4033" n="e" s="T469">kötütü͡öm, </ts>
               <ts e="T471" id="Seg_4035" n="e" s="T470">bu </ts>
               <ts e="T472" id="Seg_4037" n="e" s="T471">hüːs </ts>
               <ts e="T473" id="Seg_4039" n="e" s="T472">hetiːtten </ts>
               <ts e="T474" id="Seg_4041" n="e" s="T473">biːriger </ts>
               <ts e="T475" id="Seg_4043" n="e" s="T474">eme </ts>
               <ts e="T476" id="Seg_4045" n="e" s="T475">katannakkɨna </ts>
               <ts e="T477" id="Seg_4047" n="e" s="T476">tijsi͡eŋ, </ts>
               <ts e="T478" id="Seg_4049" n="e" s="T477">dojdugar. </ts>
               <ts e="T479" id="Seg_4051" n="e" s="T478">Otton </ts>
               <ts e="T480" id="Seg_4053" n="e" s="T479">katammatakkɨna, </ts>
               <ts e="T481" id="Seg_4055" n="e" s="T480">bu </ts>
               <ts e="T482" id="Seg_4057" n="e" s="T481">dojduga </ts>
               <ts e="T483" id="Seg_4059" n="e" s="T482">ölü͡öŋ", </ts>
               <ts e="T484" id="Seg_4061" n="e" s="T483">diːr. </ts>
               <ts e="T485" id="Seg_4063" n="e" s="T484">Dʼe </ts>
               <ts e="T486" id="Seg_4065" n="e" s="T485">dogoro, </ts>
               <ts e="T487" id="Seg_4067" n="e" s="T486">Heliː Kur, </ts>
               <ts e="T488" id="Seg_4069" n="e" s="T487">tuːsanɨ, </ts>
               <ts e="T489" id="Seg_4071" n="e" s="T488">ataktarɨ, </ts>
               <ts e="T490" id="Seg_4073" n="e" s="T489">mejiːleri </ts>
               <ts e="T491" id="Seg_4075" n="e" s="T490">egelle. </ts>
               <ts e="T492" id="Seg_4077" n="e" s="T491">Bejelere </ts>
               <ts e="T493" id="Seg_4079" n="e" s="T492">bejelerinen </ts>
               <ts e="T494" id="Seg_4081" n="e" s="T493">kihi </ts>
               <ts e="T495" id="Seg_4083" n="e" s="T494">bu͡olattaːn </ts>
               <ts e="T496" id="Seg_4085" n="e" s="T495">kaːllɨlar. </ts>
               <ts e="T497" id="Seg_4087" n="e" s="T496">Utujtaːn </ts>
               <ts e="T498" id="Seg_4089" n="e" s="T497">baran </ts>
               <ts e="T499" id="Seg_4091" n="e" s="T498">Heliː Kur </ts>
               <ts e="T500" id="Seg_4093" n="e" s="T499">uhuktar </ts>
               <ts e="T501" id="Seg_4095" n="e" s="T500">dogoro </ts>
               <ts e="T502" id="Seg_4097" n="e" s="T501">taksɨbɨt </ts>
               <ts e="T503" id="Seg_4099" n="e" s="T502">tɨ͡ahɨgar. </ts>
               <ts e="T504" id="Seg_4101" n="e" s="T503">Heliː Kur </ts>
               <ts e="T505" id="Seg_4103" n="e" s="T504">taksa </ts>
               <ts e="T506" id="Seg_4105" n="e" s="T505">köppüte, </ts>
               <ts e="T507" id="Seg_4107" n="e" s="T506">hüːs </ts>
               <ts e="T508" id="Seg_4109" n="e" s="T507">hetiː </ts>
               <ts e="T509" id="Seg_4111" n="e" s="T508">eleːnnenen </ts>
               <ts e="T510" id="Seg_4113" n="e" s="T509">erdegine, </ts>
               <ts e="T511" id="Seg_4115" n="e" s="T510">bütehik </ts>
               <ts e="T512" id="Seg_4117" n="e" s="T511">hetiːge </ts>
               <ts e="T513" id="Seg_4119" n="e" s="T512">iːlihinne. </ts>
               <ts e="T514" id="Seg_4121" n="e" s="T513">Kallaːnɨnan </ts>
               <ts e="T515" id="Seg_4123" n="e" s="T514">da, </ts>
               <ts e="T516" id="Seg_4125" n="e" s="T515">hirinen </ts>
               <ts e="T517" id="Seg_4127" n="e" s="T516">da </ts>
               <ts e="T518" id="Seg_4129" n="e" s="T517">bararɨn </ts>
               <ts e="T519" id="Seg_4131" n="e" s="T518">dʼüːlleːbetege. </ts>
               <ts e="T520" id="Seg_4133" n="e" s="T519">Kobu͡o </ts>
               <ts e="T521" id="Seg_4135" n="e" s="T520">kojut, </ts>
               <ts e="T522" id="Seg_4137" n="e" s="T521">kebi͡e </ts>
               <ts e="T523" id="Seg_4139" n="e" s="T522">keneges </ts>
               <ts e="T524" id="Seg_4141" n="e" s="T523">hüːs </ts>
               <ts e="T525" id="Seg_4143" n="e" s="T524">hetiː </ts>
               <ts e="T526" id="Seg_4145" n="e" s="T525">toktoːbutugar </ts>
               <ts e="T527" id="Seg_4147" n="e" s="T526">körbüte, </ts>
               <ts e="T528" id="Seg_4149" n="e" s="T527">dogoro </ts>
               <ts e="T529" id="Seg_4151" n="e" s="T528">dojdutugar </ts>
               <ts e="T530" id="Seg_4153" n="e" s="T529">tiri͡erbit. </ts>
               <ts e="T531" id="Seg_4155" n="e" s="T530">Dʼe </ts>
               <ts e="T532" id="Seg_4157" n="e" s="T531">Timir Haːpka </ts>
               <ts e="T533" id="Seg_4159" n="e" s="T532">diːr </ts>
               <ts e="T534" id="Seg_4161" n="e" s="T533">dogorun: </ts>
               <ts e="T535" id="Seg_4163" n="e" s="T534">"Dʼe </ts>
               <ts e="T536" id="Seg_4165" n="e" s="T535">iti </ts>
               <ts e="T537" id="Seg_4167" n="e" s="T536">hüːs </ts>
               <ts e="T538" id="Seg_4169" n="e" s="T537">hetiː </ts>
               <ts e="T539" id="Seg_4171" n="e" s="T538">aŋar </ts>
               <ts e="T540" id="Seg_4173" n="e" s="T539">ojogohugar </ts>
               <ts e="T541" id="Seg_4175" n="e" s="T540">üs </ts>
               <ts e="T542" id="Seg_4177" n="e" s="T541">čeːlkeː </ts>
               <ts e="T543" id="Seg_4179" n="e" s="T542">atɨːr </ts>
               <ts e="T544" id="Seg_4181" n="e" s="T543">kölüllen </ts>
               <ts e="T545" id="Seg_4183" n="e" s="T544">turar, </ts>
               <ts e="T546" id="Seg_4185" n="e" s="T545">aŋar </ts>
               <ts e="T547" id="Seg_4187" n="e" s="T546">ojogohugar </ts>
               <ts e="T548" id="Seg_4189" n="e" s="T547">üs </ts>
               <ts e="T549" id="Seg_4191" n="e" s="T548">kara </ts>
               <ts e="T550" id="Seg_4193" n="e" s="T549">atɨːr </ts>
               <ts e="T551" id="Seg_4195" n="e" s="T550">turar. </ts>
               <ts e="T552" id="Seg_4197" n="e" s="T551">Dʼe </ts>
               <ts e="T553" id="Seg_4199" n="e" s="T552">min </ts>
               <ts e="T554" id="Seg_4201" n="e" s="T553">iti </ts>
               <ts e="T555" id="Seg_4203" n="e" s="T554">üs </ts>
               <ts e="T556" id="Seg_4205" n="e" s="T555">čeːlkeː </ts>
               <ts e="T557" id="Seg_4207" n="e" s="T556">atɨːrɨ </ts>
               <ts e="T558" id="Seg_4209" n="e" s="T557">ölörü͡öm, </ts>
               <ts e="T559" id="Seg_4211" n="e" s="T558">tiriːlerin </ts>
               <ts e="T560" id="Seg_4213" n="e" s="T559">kastɨ͡am, </ts>
               <ts e="T561" id="Seg_4215" n="e" s="T560">en </ts>
               <ts e="T562" id="Seg_4217" n="e" s="T561">üs </ts>
               <ts e="T563" id="Seg_4219" n="e" s="T562">karanɨ </ts>
               <ts e="T564" id="Seg_4221" n="e" s="T563">ölörü͡öŋ, </ts>
               <ts e="T565" id="Seg_4223" n="e" s="T564">kastɨ͡aŋ </ts>
               <ts e="T566" id="Seg_4225" n="e" s="T565">tiriːlerin. </ts>
               <ts e="T567" id="Seg_4227" n="e" s="T566">Bilegi͡en, </ts>
               <ts e="T568" id="Seg_4229" n="e" s="T567">bu </ts>
               <ts e="T569" id="Seg_4231" n="e" s="T568">Purgaː </ts>
               <ts e="T570" id="Seg_4233" n="e" s="T569">iččitin </ts>
               <ts e="T571" id="Seg_4235" n="e" s="T570">ɨlgɨn </ts>
               <ts e="T572" id="Seg_4237" n="e" s="T571">kɨːstarɨn </ts>
               <ts e="T573" id="Seg_4239" n="e" s="T572">egellim </ts>
               <ts e="T574" id="Seg_4241" n="e" s="T573">albaspɨnan. </ts>
               <ts e="T575" id="Seg_4243" n="e" s="T574">Anɨ </ts>
               <ts e="T576" id="Seg_4245" n="e" s="T575">itintilerin </ts>
               <ts e="T577" id="Seg_4247" n="e" s="T576">batan </ts>
               <ts e="T578" id="Seg_4249" n="e" s="T577">üs </ts>
               <ts e="T579" id="Seg_4251" n="e" s="T578">ubaja </ts>
               <ts e="T580" id="Seg_4253" n="e" s="T579">Purgaː </ts>
               <ts e="T581" id="Seg_4255" n="e" s="T580">iččilere </ts>
               <ts e="T582" id="Seg_4257" n="e" s="T581">keli͡ektere. </ts>
               <ts e="T583" id="Seg_4259" n="e" s="T582">Ol </ts>
               <ts e="T584" id="Seg_4261" n="e" s="T583">kihiler </ts>
               <ts e="T585" id="Seg_4263" n="e" s="T584">kellekterine, </ts>
               <ts e="T586" id="Seg_4265" n="e" s="T585">ketegeriːn </ts>
               <ts e="T587" id="Seg_4267" n="e" s="T586">üs </ts>
               <ts e="T588" id="Seg_4269" n="e" s="T587">čeːlkeː </ts>
               <ts e="T589" id="Seg_4271" n="e" s="T588">tiriːni </ts>
               <ts e="T590" id="Seg_4273" n="e" s="T589">telgi͡em, </ts>
               <ts e="T591" id="Seg_4275" n="e" s="T590">en </ts>
               <ts e="T592" id="Seg_4277" n="e" s="T591">iti </ts>
               <ts e="T593" id="Seg_4279" n="e" s="T592">üs </ts>
               <ts e="T594" id="Seg_4281" n="e" s="T593">koŋnomu͡oj </ts>
               <ts e="T595" id="Seg_4283" n="e" s="T594">tiriːgin </ts>
               <ts e="T596" id="Seg_4285" n="e" s="T595">telgi͡er. </ts>
               <ts e="T597" id="Seg_4287" n="e" s="T596">Dʼe </ts>
               <ts e="T598" id="Seg_4289" n="e" s="T597">ulakan </ts>
               <ts e="T599" id="Seg_4291" n="e" s="T598">čaːŋŋar </ts>
               <ts e="T600" id="Seg_4293" n="e" s="T599">toloru </ts>
               <ts e="T601" id="Seg_4295" n="e" s="T600">uːta </ts>
               <ts e="T602" id="Seg_4297" n="e" s="T601">kɨːjnar, </ts>
               <ts e="T603" id="Seg_4299" n="e" s="T602">min </ts>
               <ts e="T604" id="Seg_4301" n="e" s="T603">emi͡e </ts>
               <ts e="T605" id="Seg_4303" n="e" s="T604">kɨːjnarɨ͡am. </ts>
               <ts e="T606" id="Seg_4305" n="e" s="T605">Buka </ts>
               <ts e="T607" id="Seg_4307" n="e" s="T606">mannaj </ts>
               <ts e="T608" id="Seg_4309" n="e" s="T607">mi͡eke </ts>
               <ts e="T609" id="Seg_4311" n="e" s="T608">ɨ͡aldʼɨttɨ͡aktara. </ts>
               <ts e="T610" id="Seg_4313" n="e" s="T609">Miːgitten </ts>
               <ts e="T611" id="Seg_4315" n="e" s="T610">toton, </ts>
               <ts e="T612" id="Seg_4317" n="e" s="T611">höbüleːn </ts>
               <ts e="T613" id="Seg_4319" n="e" s="T612">tagɨstaktarɨna, </ts>
               <ts e="T614" id="Seg_4321" n="e" s="T613">eji͡eke </ts>
               <ts e="T615" id="Seg_4323" n="e" s="T614">ɨ͡allanɨ͡aktara. </ts>
               <ts e="T616" id="Seg_4325" n="e" s="T615">Min </ts>
               <ts e="T617" id="Seg_4327" n="e" s="T616">kinilerge </ts>
               <ts e="T618" id="Seg_4329" n="e" s="T617">tugu </ts>
               <ts e="T619" id="Seg_4331" n="e" s="T618">eme </ts>
               <ts e="T620" id="Seg_4333" n="e" s="T619">köllörü͡öm, </ts>
               <ts e="T621" id="Seg_4335" n="e" s="T620">kiːrseːr. </ts>
               <ts e="T622" id="Seg_4337" n="e" s="T621">Min </ts>
               <ts e="T623" id="Seg_4339" n="e" s="T622">tugu </ts>
               <ts e="T624" id="Seg_4341" n="e" s="T623">kördöröbün </ts>
               <ts e="T625" id="Seg_4343" n="e" s="T624">da, </ts>
               <ts e="T626" id="Seg_4345" n="e" s="T625">ütükten </ts>
               <ts e="T627" id="Seg_4347" n="e" s="T626">kinilerge </ts>
               <ts e="T628" id="Seg_4349" n="e" s="T627">kördöröːr", </ts>
               <ts e="T629" id="Seg_4351" n="e" s="T628">di͡ete, </ts>
               <ts e="T630" id="Seg_4353" n="e" s="T629">ü͡örette </ts>
               <ts e="T631" id="Seg_4355" n="e" s="T630">dogorun. </ts>
               <ts e="T632" id="Seg_4357" n="e" s="T631">Töhö </ts>
               <ts e="T633" id="Seg_4359" n="e" s="T632">da </ts>
               <ts e="T634" id="Seg_4361" n="e" s="T633">bu͡olumuja </ts>
               <ts e="T635" id="Seg_4363" n="e" s="T634">Purgaː </ts>
               <ts e="T636" id="Seg_4365" n="e" s="T635">iččilere </ts>
               <ts e="T637" id="Seg_4367" n="e" s="T636">bolohonnon </ts>
               <ts e="T638" id="Seg_4369" n="e" s="T637">kelliler, </ts>
               <ts e="T639" id="Seg_4371" n="e" s="T638">kam </ts>
               <ts e="T640" id="Seg_4373" n="e" s="T639">muːs </ts>
               <ts e="T641" id="Seg_4375" n="e" s="T640">dʼonnor. </ts>
               <ts e="T642" id="Seg_4377" n="e" s="T641">Kiːrenner </ts>
               <ts e="T643" id="Seg_4379" n="e" s="T642">ötörü-hötörü </ts>
               <ts e="T644" id="Seg_4381" n="e" s="T643">"doroːbo" </ts>
               <ts e="T645" id="Seg_4383" n="e" s="T644">da </ts>
               <ts e="T646" id="Seg_4385" n="e" s="T645">di͡ebetter. </ts>
               <ts e="T647" id="Seg_4387" n="e" s="T646">Timir Haːpka </ts>
               <ts e="T648" id="Seg_4389" n="e" s="T647">dʼi͡etiger </ts>
               <ts e="T649" id="Seg_4391" n="e" s="T648">üs </ts>
               <ts e="T650" id="Seg_4393" n="e" s="T649">čeːlkeː </ts>
               <ts e="T651" id="Seg_4395" n="e" s="T650">tiriːge </ts>
               <ts e="T652" id="Seg_4397" n="e" s="T651">olorollor. </ts>
               <ts e="T653" id="Seg_4399" n="e" s="T652">Timir Haːpka </ts>
               <ts e="T654" id="Seg_4401" n="e" s="T653">ideleniːhi </ts>
               <ts e="T655" id="Seg_4403" n="e" s="T654">bu </ts>
               <ts e="T656" id="Seg_4405" n="e" s="T655">dʼoŋŋo. </ts>
               <ts e="T657" id="Seg_4407" n="e" s="T656">Kɨːjna </ts>
               <ts e="T658" id="Seg_4409" n="e" s="T657">turar </ts>
               <ts e="T659" id="Seg_4411" n="e" s="T658">ulakan </ts>
               <ts e="T660" id="Seg_4413" n="e" s="T659">čaːŋŋa, </ts>
               <ts e="T661" id="Seg_4415" n="e" s="T660">heliː </ts>
               <ts e="T662" id="Seg_4417" n="e" s="T661">munnulaːk </ts>
               <ts e="T663" id="Seg_4419" n="e" s="T662">tallan ku͡ogas </ts>
               <ts e="T664" id="Seg_4421" n="e" s="T663">bu͡olaːt, </ts>
               <ts e="T665" id="Seg_4423" n="e" s="T664">"Aː uː" </ts>
               <ts e="T666" id="Seg_4425" n="e" s="T665">di͡et </ts>
               <ts e="T667" id="Seg_4427" n="e" s="T666">umsan </ts>
               <ts e="T668" id="Seg_4429" n="e" s="T667">kaːlar, </ts>
               <ts e="T669" id="Seg_4431" n="e" s="T668">dajda-dajbɨtɨnan </ts>
               <ts e="T670" id="Seg_4433" n="e" s="T669">kötön </ts>
               <ts e="T671" id="Seg_4435" n="e" s="T670">taksan </ts>
               <ts e="T672" id="Seg_4437" n="e" s="T671">ijetin </ts>
               <ts e="T673" id="Seg_4439" n="e" s="T672">bɨ͡arɨnan </ts>
               <ts e="T674" id="Seg_4441" n="e" s="T673">helip </ts>
               <ts e="T675" id="Seg_4443" n="e" s="T674">gɨnar, </ts>
               <ts e="T676" id="Seg_4445" n="e" s="T675">onton </ts>
               <ts e="T677" id="Seg_4447" n="e" s="T676">ijetin </ts>
               <ts e="T678" id="Seg_4449" n="e" s="T677">ajagɨn </ts>
               <ts e="T679" id="Seg_4451" n="e" s="T678">ustun </ts>
               <ts e="T680" id="Seg_4453" n="e" s="T679">taksa </ts>
               <ts e="T681" id="Seg_4455" n="e" s="T680">köppüt. </ts>
               <ts e="T682" id="Seg_4457" n="e" s="T681">Dʼe </ts>
               <ts e="T683" id="Seg_4459" n="e" s="T682">onton </ts>
               <ts e="T684" id="Seg_4461" n="e" s="T683">agatɨn </ts>
               <ts e="T685" id="Seg_4463" n="e" s="T684">bɨ͡arɨnan </ts>
               <ts e="T686" id="Seg_4465" n="e" s="T685">helip </ts>
               <ts e="T687" id="Seg_4467" n="e" s="T686">gɨmmɨt, </ts>
               <ts e="T688" id="Seg_4469" n="e" s="T687">agatɨn </ts>
               <ts e="T689" id="Seg_4471" n="e" s="T688">ajagɨnan </ts>
               <ts e="T690" id="Seg_4473" n="e" s="T689">hanardɨː </ts>
               <ts e="T691" id="Seg_4475" n="e" s="T690">mu͡ohun </ts>
               <ts e="T692" id="Seg_4477" n="e" s="T691">hullaːbɨt </ts>
               <ts e="T693" id="Seg_4479" n="e" s="T692">kɨːl </ts>
               <ts e="T694" id="Seg_4481" n="e" s="T693">atɨːra </ts>
               <ts e="T695" id="Seg_4483" n="e" s="T694">bu͡olan </ts>
               <ts e="T696" id="Seg_4485" n="e" s="T695">taksar. </ts>
               <ts e="T697" id="Seg_4487" n="e" s="T696">Bu </ts>
               <ts e="T698" id="Seg_4489" n="e" s="T697">kennitten </ts>
               <ts e="T699" id="Seg_4491" n="e" s="T698">Timir Haːpka </ts>
               <ts e="T700" id="Seg_4493" n="e" s="T699">kihi </ts>
               <ts e="T701" id="Seg_4495" n="e" s="T700">bu͡olar </ts>
               <ts e="T702" id="Seg_4497" n="e" s="T701">da, </ts>
               <ts e="T703" id="Seg_4499" n="e" s="T702">alanaːnnan </ts>
               <ts e="T704" id="Seg_4501" n="e" s="T703">bu </ts>
               <ts e="T705" id="Seg_4503" n="e" s="T704">kɨːl </ts>
               <ts e="T706" id="Seg_4505" n="e" s="T705">atɨːrɨn </ts>
               <ts e="T707" id="Seg_4507" n="e" s="T706">huptu </ts>
               <ts e="T708" id="Seg_4509" n="e" s="T707">ɨtan </ts>
               <ts e="T709" id="Seg_4511" n="e" s="T708">tüherer. </ts>
               <ts e="T710" id="Seg_4513" n="e" s="T709">Ölörör </ts>
               <ts e="T711" id="Seg_4515" n="e" s="T710">da </ts>
               <ts e="T712" id="Seg_4517" n="e" s="T711">manɨga </ts>
               <ts e="T713" id="Seg_4519" n="e" s="T712">kastɨrɨta </ts>
               <ts e="T714" id="Seg_4521" n="e" s="T713">hülen </ts>
               <ts e="T715" id="Seg_4523" n="e" s="T714">mas </ts>
               <ts e="T716" id="Seg_4525" n="e" s="T715">atɨjakka </ts>
               <ts e="T717" id="Seg_4527" n="e" s="T716">hiːkejdiː-buhuːluː </ts>
               <ts e="T718" id="Seg_4529" n="e" s="T717">tardan </ts>
               <ts e="T719" id="Seg_4531" n="e" s="T718">keːher, </ts>
               <ts e="T720" id="Seg_4533" n="e" s="T719">ɨ͡aldʼɨttarga </ts>
               <ts e="T721" id="Seg_4535" n="e" s="T720">hogudaːjdatan. </ts>
               <ts e="T722" id="Seg_4537" n="e" s="T721">ɨ͡aldʼɨttara </ts>
               <ts e="T723" id="Seg_4539" n="e" s="T722">haŋa-iŋe </ts>
               <ts e="T724" id="Seg_4541" n="e" s="T723">hu͡ok, </ts>
               <ts e="T725" id="Seg_4543" n="e" s="T724">ahɨːrga </ts>
               <ts e="T726" id="Seg_4545" n="e" s="T725">barbɨttar. </ts>
               <ts e="T727" id="Seg_4547" n="e" s="T726">Kühüŋŋü </ts>
               <ts e="T728" id="Seg_4549" n="e" s="T727">atɨːrtan </ts>
               <ts e="T729" id="Seg_4551" n="e" s="T728">amattan </ts>
               <ts e="T730" id="Seg_4553" n="e" s="T729">mu͡ostaːk </ts>
               <ts e="T731" id="Seg_4555" n="e" s="T730">tujagɨn </ts>
               <ts e="T732" id="Seg_4557" n="e" s="T731">ordoron </ts>
               <ts e="T733" id="Seg_4559" n="e" s="T732">kebistiler. </ts>
               <ts e="T734" id="Seg_4561" n="e" s="T733">Heliː Kur </ts>
               <ts e="T735" id="Seg_4563" n="e" s="T734">körön </ts>
               <ts e="T736" id="Seg_4565" n="e" s="T735">olorunaːktaːta. </ts>
               <ts e="T737" id="Seg_4567" n="e" s="T736">Ahaːn </ts>
               <ts e="T738" id="Seg_4569" n="e" s="T737">büteller </ts>
               <ts e="T739" id="Seg_4571" n="e" s="T738">da, </ts>
               <ts e="T740" id="Seg_4573" n="e" s="T739">u͡ostarɨn </ts>
               <ts e="T741" id="Seg_4575" n="e" s="T740">hottoːn </ts>
               <ts e="T742" id="Seg_4577" n="e" s="T741">taksallar. </ts>
               <ts e="T743" id="Seg_4579" n="e" s="T742">Heliː Kur </ts>
               <ts e="T744" id="Seg_4581" n="e" s="T743">dʼi͡etiger </ts>
               <ts e="T745" id="Seg_4583" n="e" s="T744">ɨstanar, </ts>
               <ts e="T746" id="Seg_4585" n="e" s="T745">ɨ͡aldʼɨttarɨ </ts>
               <ts e="T747" id="Seg_4587" n="e" s="T746">tohujuna. </ts>
               <ts e="T748" id="Seg_4589" n="e" s="T747">ɨ͡aldʼɨttara </ts>
               <ts e="T749" id="Seg_4591" n="e" s="T748">kiːren </ts>
               <ts e="T750" id="Seg_4593" n="e" s="T749">üs </ts>
               <ts e="T751" id="Seg_4595" n="e" s="T750">koŋnomu͡oj </ts>
               <ts e="T752" id="Seg_4597" n="e" s="T751">tiriːge </ts>
               <ts e="T753" id="Seg_4599" n="e" s="T752">oloro </ts>
               <ts e="T754" id="Seg_4601" n="e" s="T753">bi͡ereller. </ts>
               <ts e="T755" id="Seg_4603" n="e" s="T754">Heliː Kur </ts>
               <ts e="T756" id="Seg_4605" n="e" s="T755">"A uːk" </ts>
               <ts e="T757" id="Seg_4607" n="e" s="T756">di͡et, </ts>
               <ts e="T758" id="Seg_4609" n="e" s="T757">bɨːtta ku͡ogaskaːn </ts>
               <ts e="T759" id="Seg_4611" n="e" s="T758">bu͡olaːt, </ts>
               <ts e="T760" id="Seg_4613" n="e" s="T759">čaːŋŋa </ts>
               <ts e="T761" id="Seg_4615" n="e" s="T760">umsar, </ts>
               <ts e="T762" id="Seg_4617" n="e" s="T761">ijetin </ts>
               <ts e="T763" id="Seg_4619" n="e" s="T762">bɨ͡arɨgar </ts>
               <ts e="T764" id="Seg_4621" n="e" s="T763">helip </ts>
               <ts e="T765" id="Seg_4623" n="e" s="T764">gɨnar, </ts>
               <ts e="T766" id="Seg_4625" n="e" s="T765">ajagɨn </ts>
               <ts e="T767" id="Seg_4627" n="e" s="T766">ustun </ts>
               <ts e="T768" id="Seg_4629" n="e" s="T767">taksar, </ts>
               <ts e="T769" id="Seg_4631" n="e" s="T768">agatɨn </ts>
               <ts e="T770" id="Seg_4633" n="e" s="T769">bɨ͡arɨgar </ts>
               <ts e="T771" id="Seg_4635" n="e" s="T770">helip </ts>
               <ts e="T772" id="Seg_4637" n="e" s="T771">gɨnar. </ts>
               <ts e="T773" id="Seg_4639" n="e" s="T772">Agatɨn </ts>
               <ts e="T774" id="Seg_4641" n="e" s="T773">ajagɨn </ts>
               <ts e="T775" id="Seg_4643" n="e" s="T774">ustun </ts>
               <ts e="T776" id="Seg_4645" n="e" s="T775">tuguttan </ts>
               <ts e="T777" id="Seg_4647" n="e" s="T776">ere </ts>
               <ts e="T778" id="Seg_4649" n="e" s="T777">kuhagan </ts>
               <ts e="T779" id="Seg_4651" n="e" s="T778">tulaːjakiː </ts>
               <ts e="T780" id="Seg_4653" n="e" s="T779">tugukkaːn </ts>
               <ts e="T781" id="Seg_4655" n="e" s="T780">bu͡olan </ts>
               <ts e="T782" id="Seg_4657" n="e" s="T781">taksa </ts>
               <ts e="T783" id="Seg_4659" n="e" s="T782">köppüt. </ts>
               <ts e="T784" id="Seg_4661" n="e" s="T783">Kennitten </ts>
               <ts e="T785" id="Seg_4663" n="e" s="T784">haːlaːk </ts>
               <ts e="T786" id="Seg_4665" n="e" s="T785">kihi </ts>
               <ts e="T787" id="Seg_4667" n="e" s="T786">taksar </ts>
               <ts e="T788" id="Seg_4669" n="e" s="T787">da </ts>
               <ts e="T789" id="Seg_4671" n="e" s="T788">ɨ͡aldʼɨttar </ts>
               <ts e="T790" id="Seg_4673" n="e" s="T789">innileriger </ts>
               <ts e="T791" id="Seg_4675" n="e" s="T790">epseri </ts>
               <ts e="T792" id="Seg_4677" n="e" s="T791">ɨtar, </ts>
               <ts e="T793" id="Seg_4679" n="e" s="T792">astaːn </ts>
               <ts e="T794" id="Seg_4681" n="e" s="T793">kebiher. </ts>
               <ts e="T795" id="Seg_4683" n="e" s="T794">ɨ͡aldʼɨttarga </ts>
               <ts e="T796" id="Seg_4685" n="e" s="T795">tardan </ts>
               <ts e="T797" id="Seg_4687" n="e" s="T796">keːher. </ts>
               <ts e="T798" id="Seg_4689" n="e" s="T797">Tardan </ts>
               <ts e="T799" id="Seg_4691" n="e" s="T798">agaj </ts>
               <ts e="T800" id="Seg_4693" n="e" s="T799">keːspite. </ts>
               <ts e="T801" id="Seg_4695" n="e" s="T800">Dʼi͡ete-u͡ota </ts>
               <ts e="T802" id="Seg_4697" n="e" s="T801">kanna </ts>
               <ts e="T803" id="Seg_4699" n="e" s="T802">da </ts>
               <ts e="T804" id="Seg_4701" n="e" s="T803">barbɨttarɨn </ts>
               <ts e="T805" id="Seg_4703" n="e" s="T804">bilbetiler. </ts>
               <ts e="T806" id="Seg_4705" n="e" s="T805">Purgaː </ts>
               <ts e="T807" id="Seg_4707" n="e" s="T806">iččilere </ts>
               <ts e="T808" id="Seg_4709" n="e" s="T807">öhürgenen </ts>
               <ts e="T809" id="Seg_4711" n="e" s="T808">bu </ts>
               <ts e="T810" id="Seg_4713" n="e" s="T809">dʼi͡eni, </ts>
               <ts e="T811" id="Seg_4715" n="e" s="T810">Heliː Kuru </ts>
               <ts e="T812" id="Seg_4717" n="e" s="T811">ijeliːni-agalɨːnɨ </ts>
               <ts e="T813" id="Seg_4719" n="e" s="T812">kötüten </ts>
               <ts e="T814" id="Seg_4721" n="e" s="T813">bolohonnon </ts>
               <ts e="T815" id="Seg_4723" n="e" s="T814">ispittere. </ts>
               <ts e="T816" id="Seg_4725" n="e" s="T815">Araj </ts>
               <ts e="T817" id="Seg_4727" n="e" s="T816">Timir Haːpka </ts>
               <ts e="T818" id="Seg_4729" n="e" s="T817">ijeliːn-agalɨːn </ts>
               <ts e="T819" id="Seg_4731" n="e" s="T818">kaːlbɨt. </ts>
               <ts e="T820" id="Seg_4733" n="e" s="T819">Purgaː </ts>
               <ts e="T821" id="Seg_4735" n="e" s="T820">kɨːhɨn </ts>
               <ts e="T822" id="Seg_4737" n="e" s="T821">dʼaktardanan </ts>
               <ts e="T823" id="Seg_4739" n="e" s="T822">bajan-tajan </ts>
               <ts e="T824" id="Seg_4741" n="e" s="T823">olorbuta </ts>
               <ts e="T825" id="Seg_4743" n="e" s="T824">ühü. </ts>
               <ts e="T826" id="Seg_4745" n="e" s="T825">Elete. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T8" id="Seg_4746" s="T0">PoMA_1964_IronCapBonyBelt_flk.001 (001.001)</ta>
            <ta e="T11" id="Seg_4747" s="T8">PoMA_1964_IronCapBonyBelt_flk.002 (001.002)</ta>
            <ta e="T17" id="Seg_4748" s="T11">PoMA_1964_IronCapBonyBelt_flk.003 (001.003)</ta>
            <ta e="T23" id="Seg_4749" s="T17">PoMA_1964_IronCapBonyBelt_flk.004 (001.003)</ta>
            <ta e="T27" id="Seg_4750" s="T23">PoMA_1964_IronCapBonyBelt_flk.005 (001.004)</ta>
            <ta e="T31" id="Seg_4751" s="T27">PoMA_1964_IronCapBonyBelt_flk.006 (001.005)</ta>
            <ta e="T40" id="Seg_4752" s="T31">PoMA_1964_IronCapBonyBelt_flk.007 (001.006)</ta>
            <ta e="T42" id="Seg_4753" s="T40">PoMA_1964_IronCapBonyBelt_flk.008 (001.008)</ta>
            <ta e="T44" id="Seg_4754" s="T42">PoMA_1964_IronCapBonyBelt_flk.009 (001.008)</ta>
            <ta e="T48" id="Seg_4755" s="T44">PoMA_1964_IronCapBonyBelt_flk.010 (001.009)</ta>
            <ta e="T50" id="Seg_4756" s="T48">PoMA_1964_IronCapBonyBelt_flk.011 (001.010)</ta>
            <ta e="T64" id="Seg_4757" s="T50">PoMA_1964_IronCapBonyBelt_flk.012 (001.011)</ta>
            <ta e="T65" id="Seg_4758" s="T64">PoMA_1964_IronCapBonyBelt_flk.013 (001.012)</ta>
            <ta e="T68" id="Seg_4759" s="T65">PoMA_1964_IronCapBonyBelt_flk.014 (001.012)</ta>
            <ta e="T77" id="Seg_4760" s="T68">PoMA_1964_IronCapBonyBelt_flk.015 (001.013)</ta>
            <ta e="T82" id="Seg_4761" s="T77">PoMA_1964_IronCapBonyBelt_flk.016 (001.014)</ta>
            <ta e="T86" id="Seg_4762" s="T82">PoMA_1964_IronCapBonyBelt_flk.017 (001.015)</ta>
            <ta e="T88" id="Seg_4763" s="T86">PoMA_1964_IronCapBonyBelt_flk.018 (001.016)</ta>
            <ta e="T91" id="Seg_4764" s="T88">PoMA_1964_IronCapBonyBelt_flk.019 (001.016)</ta>
            <ta e="T99" id="Seg_4765" s="T91">PoMA_1964_IronCapBonyBelt_flk.020 (001.017)</ta>
            <ta e="T106" id="Seg_4766" s="T99">PoMA_1964_IronCapBonyBelt_flk.021 (001.018)</ta>
            <ta e="T109" id="Seg_4767" s="T106">PoMA_1964_IronCapBonyBelt_flk.022 (001.019)</ta>
            <ta e="T120" id="Seg_4768" s="T109">PoMA_1964_IronCapBonyBelt_flk.023 (001.020)</ta>
            <ta e="T126" id="Seg_4769" s="T120">PoMA_1964_IronCapBonyBelt_flk.024 (001.021)</ta>
            <ta e="T133" id="Seg_4770" s="T126">PoMA_1964_IronCapBonyBelt_flk.025 (001.022)</ta>
            <ta e="T138" id="Seg_4771" s="T133">PoMA_1964_IronCapBonyBelt_flk.026 (001.023)</ta>
            <ta e="T142" id="Seg_4772" s="T138">PoMA_1964_IronCapBonyBelt_flk.027 (001.024)</ta>
            <ta e="T153" id="Seg_4773" s="T142">PoMA_1964_IronCapBonyBelt_flk.028 (001.025)</ta>
            <ta e="T156" id="Seg_4774" s="T153">PoMA_1964_IronCapBonyBelt_flk.029 (001.026)</ta>
            <ta e="T162" id="Seg_4775" s="T156">PoMA_1964_IronCapBonyBelt_flk.030 (001.027)</ta>
            <ta e="T170" id="Seg_4776" s="T162">PoMA_1964_IronCapBonyBelt_flk.031 (001.028)</ta>
            <ta e="T174" id="Seg_4777" s="T170">PoMA_1964_IronCapBonyBelt_flk.032 (001.029)</ta>
            <ta e="T178" id="Seg_4778" s="T174">PoMA_1964_IronCapBonyBelt_flk.033 (001.030)</ta>
            <ta e="T186" id="Seg_4779" s="T178">PoMA_1964_IronCapBonyBelt_flk.034 (001.031)</ta>
            <ta e="T191" id="Seg_4780" s="T186">PoMA_1964_IronCapBonyBelt_flk.035 (001.032)</ta>
            <ta e="T194" id="Seg_4781" s="T191">PoMA_1964_IronCapBonyBelt_flk.036 (001.033)</ta>
            <ta e="T199" id="Seg_4782" s="T194">PoMA_1964_IronCapBonyBelt_flk.037 (001.033)</ta>
            <ta e="T203" id="Seg_4783" s="T199">PoMA_1964_IronCapBonyBelt_flk.038 (001.035)</ta>
            <ta e="T206" id="Seg_4784" s="T203">PoMA_1964_IronCapBonyBelt_flk.039 (001.036)</ta>
            <ta e="T211" id="Seg_4785" s="T206">PoMA_1964_IronCapBonyBelt_flk.040 (001.037)</ta>
            <ta e="T219" id="Seg_4786" s="T211">PoMA_1964_IronCapBonyBelt_flk.041 (001.038)</ta>
            <ta e="T225" id="Seg_4787" s="T219">PoMA_1964_IronCapBonyBelt_flk.042 (001.039)</ta>
            <ta e="T230" id="Seg_4788" s="T225">PoMA_1964_IronCapBonyBelt_flk.043 (001.040)</ta>
            <ta e="T235" id="Seg_4789" s="T230">PoMA_1964_IronCapBonyBelt_flk.044 (001.041)</ta>
            <ta e="T244" id="Seg_4790" s="T235">PoMA_1964_IronCapBonyBelt_flk.045 (001.042)</ta>
            <ta e="T250" id="Seg_4791" s="T244">PoMA_1964_IronCapBonyBelt_flk.046 (001.043)</ta>
            <ta e="T254" id="Seg_4792" s="T250">PoMA_1964_IronCapBonyBelt_flk.047 (001.044)</ta>
            <ta e="T263" id="Seg_4793" s="T254">PoMA_1964_IronCapBonyBelt_flk.048 (001.045)</ta>
            <ta e="T269" id="Seg_4794" s="T263">PoMA_1964_IronCapBonyBelt_flk.049 (001.046)</ta>
            <ta e="T282" id="Seg_4795" s="T269">PoMA_1964_IronCapBonyBelt_flk.050 (001.047)</ta>
            <ta e="T291" id="Seg_4796" s="T282">PoMA_1964_IronCapBonyBelt_flk.051 (001.048)</ta>
            <ta e="T297" id="Seg_4797" s="T291">PoMA_1964_IronCapBonyBelt_flk.052 (001.049)</ta>
            <ta e="T301" id="Seg_4798" s="T297">PoMA_1964_IronCapBonyBelt_flk.053 (001.050)</ta>
            <ta e="T312" id="Seg_4799" s="T301">PoMA_1964_IronCapBonyBelt_flk.054 (001.052)</ta>
            <ta e="T320" id="Seg_4800" s="T312">PoMA_1964_IronCapBonyBelt_flk.055 (001.053)</ta>
            <ta e="T326" id="Seg_4801" s="T320">PoMA_1964_IronCapBonyBelt_flk.056 (001.055)</ta>
            <ta e="T330" id="Seg_4802" s="T326">PoMA_1964_IronCapBonyBelt_flk.057 (001.057)</ta>
            <ta e="T334" id="Seg_4803" s="T330">PoMA_1964_IronCapBonyBelt_flk.058 (001.058)</ta>
            <ta e="T337" id="Seg_4804" s="T334">PoMA_1964_IronCapBonyBelt_flk.059 (001.059)</ta>
            <ta e="T342" id="Seg_4805" s="T337">PoMA_1964_IronCapBonyBelt_flk.060 (001.060)</ta>
            <ta e="T346" id="Seg_4806" s="T342">PoMA_1964_IronCapBonyBelt_flk.061 (001.061)</ta>
            <ta e="T352" id="Seg_4807" s="T346">PoMA_1964_IronCapBonyBelt_flk.062 (001.062)</ta>
            <ta e="T358" id="Seg_4808" s="T352">PoMA_1964_IronCapBonyBelt_flk.063 (001.064)</ta>
            <ta e="T367" id="Seg_4809" s="T358">PoMA_1964_IronCapBonyBelt_flk.064 (001.065)</ta>
            <ta e="T374" id="Seg_4810" s="T367">PoMA_1964_IronCapBonyBelt_flk.065 (001.066)</ta>
            <ta e="T377" id="Seg_4811" s="T374">PoMA_1964_IronCapBonyBelt_flk.066 (001.067)</ta>
            <ta e="T382" id="Seg_4812" s="T377">PoMA_1964_IronCapBonyBelt_flk.067 (001.068)</ta>
            <ta e="T392" id="Seg_4813" s="T382">PoMA_1964_IronCapBonyBelt_flk.068 (001.069)</ta>
            <ta e="T400" id="Seg_4814" s="T392">PoMA_1964_IronCapBonyBelt_flk.069 (001.070)</ta>
            <ta e="T406" id="Seg_4815" s="T400">PoMA_1964_IronCapBonyBelt_flk.070 (001.070)</ta>
            <ta e="T408" id="Seg_4816" s="T406">PoMA_1964_IronCapBonyBelt_flk.071 (001.071)</ta>
            <ta e="T414" id="Seg_4817" s="T408">PoMA_1964_IronCapBonyBelt_flk.072 (001.072)</ta>
            <ta e="T417" id="Seg_4818" s="T414">PoMA_1964_IronCapBonyBelt_flk.073 (001.073)</ta>
            <ta e="T425" id="Seg_4819" s="T417">PoMA_1964_IronCapBonyBelt_flk.074 (001.073)</ta>
            <ta e="T432" id="Seg_4820" s="T425">PoMA_1964_IronCapBonyBelt_flk.075 (001.074)</ta>
            <ta e="T440" id="Seg_4821" s="T432">PoMA_1964_IronCapBonyBelt_flk.076 (001.075)</ta>
            <ta e="T445" id="Seg_4822" s="T440">PoMA_1964_IronCapBonyBelt_flk.077 (001.076)</ta>
            <ta e="T452" id="Seg_4823" s="T445">PoMA_1964_IronCapBonyBelt_flk.078 (001.077)</ta>
            <ta e="T459" id="Seg_4824" s="T452">PoMA_1964_IronCapBonyBelt_flk.079 (001.078)</ta>
            <ta e="T478" id="Seg_4825" s="T459">PoMA_1964_IronCapBonyBelt_flk.080 (001.079)</ta>
            <ta e="T484" id="Seg_4826" s="T478">PoMA_1964_IronCapBonyBelt_flk.081 (001.080)</ta>
            <ta e="T491" id="Seg_4827" s="T484">PoMA_1964_IronCapBonyBelt_flk.082 (001.082)</ta>
            <ta e="T496" id="Seg_4828" s="T491">PoMA_1964_IronCapBonyBelt_flk.083 (001.083)</ta>
            <ta e="T503" id="Seg_4829" s="T496">PoMA_1964_IronCapBonyBelt_flk.084 (001.084)</ta>
            <ta e="T513" id="Seg_4830" s="T503">PoMA_1964_IronCapBonyBelt_flk.085 (001.085)</ta>
            <ta e="T519" id="Seg_4831" s="T513">PoMA_1964_IronCapBonyBelt_flk.086 (001.086)</ta>
            <ta e="T530" id="Seg_4832" s="T519">PoMA_1964_IronCapBonyBelt_flk.087 (001.087)</ta>
            <ta e="T534" id="Seg_4833" s="T530">PoMA_1964_IronCapBonyBelt_flk.088 (001.088)</ta>
            <ta e="T551" id="Seg_4834" s="T534">PoMA_1964_IronCapBonyBelt_flk.089 (001.088)</ta>
            <ta e="T566" id="Seg_4835" s="T551">PoMA_1964_IronCapBonyBelt_flk.090 (001.089)</ta>
            <ta e="T574" id="Seg_4836" s="T566">PoMA_1964_IronCapBonyBelt_flk.091 (001.090)</ta>
            <ta e="T582" id="Seg_4837" s="T574">PoMA_1964_IronCapBonyBelt_flk.092 (001.091)</ta>
            <ta e="T596" id="Seg_4838" s="T582">PoMA_1964_IronCapBonyBelt_flk.093 (001.092)</ta>
            <ta e="T605" id="Seg_4839" s="T596">PoMA_1964_IronCapBonyBelt_flk.094 (001.093)</ta>
            <ta e="T609" id="Seg_4840" s="T605">PoMA_1964_IronCapBonyBelt_flk.095 (001.094)</ta>
            <ta e="T615" id="Seg_4841" s="T609">PoMA_1964_IronCapBonyBelt_flk.096 (001.095)</ta>
            <ta e="T621" id="Seg_4842" s="T615">PoMA_1964_IronCapBonyBelt_flk.097 (001.096)</ta>
            <ta e="T631" id="Seg_4843" s="T621">PoMA_1964_IronCapBonyBelt_flk.098 (001.097)</ta>
            <ta e="T641" id="Seg_4844" s="T631">PoMA_1964_IronCapBonyBelt_flk.099 (001.099)</ta>
            <ta e="T646" id="Seg_4845" s="T641">PoMA_1964_IronCapBonyBelt_flk.100 (001.100)</ta>
            <ta e="T652" id="Seg_4846" s="T646">PoMA_1964_IronCapBonyBelt_flk.101 (001.101)</ta>
            <ta e="T656" id="Seg_4847" s="T652">PoMA_1964_IronCapBonyBelt_flk.102 (001.102)</ta>
            <ta e="T681" id="Seg_4848" s="T656">PoMA_1964_IronCapBonyBelt_flk.103 (001.103)</ta>
            <ta e="T696" id="Seg_4849" s="T681">PoMA_1964_IronCapBonyBelt_flk.104 (001.104)</ta>
            <ta e="T709" id="Seg_4850" s="T696">PoMA_1964_IronCapBonyBelt_flk.105 (001.105)</ta>
            <ta e="T721" id="Seg_4851" s="T709">PoMA_1964_IronCapBonyBelt_flk.106 (001.106)</ta>
            <ta e="T726" id="Seg_4852" s="T721">PoMA_1964_IronCapBonyBelt_flk.107 (001.107)</ta>
            <ta e="T733" id="Seg_4853" s="T726">PoMA_1964_IronCapBonyBelt_flk.108 (001.108)</ta>
            <ta e="T736" id="Seg_4854" s="T733">PoMA_1964_IronCapBonyBelt_flk.109 (001.109)</ta>
            <ta e="T742" id="Seg_4855" s="T736">PoMA_1964_IronCapBonyBelt_flk.110 (001.110)</ta>
            <ta e="T747" id="Seg_4856" s="T742">PoMA_1964_IronCapBonyBelt_flk.111 (001.111)</ta>
            <ta e="T754" id="Seg_4857" s="T747">PoMA_1964_IronCapBonyBelt_flk.112 (001.112)</ta>
            <ta e="T772" id="Seg_4858" s="T754">PoMA_1964_IronCapBonyBelt_flk.113 (001.113)</ta>
            <ta e="T783" id="Seg_4859" s="T772">PoMA_1964_IronCapBonyBelt_flk.114 (001.114)</ta>
            <ta e="T794" id="Seg_4860" s="T783">PoMA_1964_IronCapBonyBelt_flk.115 (001.115)</ta>
            <ta e="T797" id="Seg_4861" s="T794">PoMA_1964_IronCapBonyBelt_flk.116 (001.116)</ta>
            <ta e="T800" id="Seg_4862" s="T797">PoMA_1964_IronCapBonyBelt_flk.117 (001.117)</ta>
            <ta e="T805" id="Seg_4863" s="T800">PoMA_1964_IronCapBonyBelt_flk.118 (001.118)</ta>
            <ta e="T815" id="Seg_4864" s="T805">PoMA_1964_IronCapBonyBelt_flk.119 (001.119)</ta>
            <ta e="T819" id="Seg_4865" s="T815">PoMA_1964_IronCapBonyBelt_flk.120 (001.120)</ta>
            <ta e="T825" id="Seg_4866" s="T819">PoMA_1964_IronCapBonyBelt_flk.121 (001.121)</ta>
            <ta e="T826" id="Seg_4867" s="T825">PoMA_1964_IronCapBonyBelt_flk.122 (001.122)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T8" id="Seg_4868" s="T0">Тимир Һаапка онуга Һэлии Кур диэн икки уол киһилэр олорбуттар.</ta>
            <ta e="T11" id="Seg_4869" s="T8">Иккиэн агалаактар, ийэлээктэр.</ta>
            <ta e="T17" id="Seg_4870" s="T11">Дьэ бу олороннор Һэлии Кура диир Тимир Һаапкатын: </ta>
            <ta e="T23" id="Seg_4871" s="T17">— Догоор, бу биһиги туок идэлээк буолабыт?</ta>
            <ta e="T27" id="Seg_4872" s="T23">Того идэ идэбитин билсибэппит?</ta>
            <ta e="T31" id="Seg_4873" s="T27">— мэктиэ былдьаһаары гынар быһыылаак.</ta>
            <ta e="T40" id="Seg_4874" s="T31">Догоро: — Ээ, догор, туокпутун былдьаһан мэктиэ былдьаһыакпытый? — диэн буолуммат.</ta>
            <ta e="T42" id="Seg_4875" s="T40">Догоро тэһиппэт: </ta>
            <ta e="T44" id="Seg_4876" s="T42">— Того, догор!</ta>
            <ta e="T48" id="Seg_4877" s="T44">Эн идэгин билиэкпин багарабын!</ta>
            <ta e="T50" id="Seg_4878" s="T48">Догоро буолуммат.</ta>
            <ta e="T64" id="Seg_4879" s="T50">Һэлии Кур: — Мин күрүөм эньигиттэн, бат миньиигин, мин кайдак буолабын да, һогордуккаан буолан ис, — диир.</ta>
            <ta e="T65" id="Seg_4880" s="T64">Догоро: </ta>
            <ta e="T68" id="Seg_4881" s="T65">— Батыактыбын дуу, айылаагын.</ta>
            <ta e="T77" id="Seg_4882" s="T68">— Һарсиэрда эрдэһит киһи турар кэмигэр батаар миньигин, — диир Һэлии Кур.</ta>
            <ta e="T82" id="Seg_4883" s="T77">Инньэ дэһэн араксаллар дьиэ дьиэлэригэр.</ta>
            <ta e="T86" id="Seg_4884" s="T82">Тимир Һаапка таксан утуйа һытар.</ta>
            <ta e="T88" id="Seg_4885" s="T86">Уһуктан кэлэр: </ta>
            <ta e="T91" id="Seg_4886" s="T88">"Догорум баара дьүрү?"</ta>
            <ta e="T99" id="Seg_4887" s="T91">Догоругар кэлбитэ: Һэлии Кур ороно чоҥкойо һытар — баран каалбыт.</ta>
            <ta e="T106" id="Seg_4888" s="T99">Тимир Һаапка һуол кайар, һуолу булбат, элэтэ буолар.</ta>
            <ta e="T109" id="Seg_4889" s="T106">Киирэн оронун куптуйар.</ta>
            <ta e="T120" id="Seg_4890" s="T109">Һэлии Кур оронун анныттан үүтээн кутуйагын корооно чоҥойо һытар, атын һуол һуок.</ta>
            <ta e="T126" id="Seg_4891" s="T120">Тимир Һаапка кутуйак буолла да короонунан түстэ.</ta>
            <ta e="T133" id="Seg_4892" s="T126">Ынарааҥҥыта, короонтон таксаат, горонуок буолаат ыстанан каалар.</ta>
            <ta e="T138" id="Seg_4893" s="T133">Тимир Һаапка горонуок буолаат батан иһэр.</ta>
            <ta e="T142" id="Seg_4894" s="T138">Һук көтүү баран истэ.</ta>
            <ta e="T153" id="Seg_4895" s="T142">Бу горонуок һуола бүтэр да, орто дойду кырса һуола бара турда.</ta>
            <ta e="T156" id="Seg_4896" s="T153">Догоро һалла истэ.</ta>
            <ta e="T162" id="Seg_4897" s="T156">Тимир Һаапка кырса буолар да, ойон истэ.</ta>
            <ta e="T170" id="Seg_4898" s="T162">Биир һиргэ түспүт да, Һэлии Кура ускаан буолан ойбут.</ta>
            <ta e="T174" id="Seg_4899" s="T170">Кэнникитэ ускаан буолан батта.</ta>
            <ta e="T178" id="Seg_4900" s="T174">Һэлии Кур һаһыл буолла онтон.</ta>
            <ta e="T186" id="Seg_4901" s="T178">Кэнникитэ эмиэ һаһыл буолан һуолун агай үтүктэн истэ.</ta>
            <ta e="T191" id="Seg_4902" s="T186">Мааныта бөрө буолан көтөн ньулуйбут.</ta>
            <ta e="T194" id="Seg_4903" s="T191">Кэнникитэ һөгө истэ: </ta>
            <ta e="T199" id="Seg_4904" s="T194">"Мантыкайа батымына көрдөммүт буоллага!" — диир.</ta>
            <ta e="T203" id="Seg_4905" s="T199">Бөрө буолан батан истэ.</ta>
            <ta e="T206" id="Seg_4906" s="T203">Төһөнү-каччаны барбыта биллибэт.</ta>
            <ta e="T211" id="Seg_4907" s="T206">Һэлии Кур һиэгэн буолан лэппэйэ турбут.</ta>
            <ta e="T219" id="Seg_4908" s="T211">Кэнникитэ эмиэ, биэк һөгө-һөгө, һиэгэн буолан батан истэ.</ta>
            <ta e="T225" id="Seg_4909" s="T219">Һиэгэнин һуола бүтээтин, эбэкээ буолан липпэйбит.</ta>
            <ta e="T230" id="Seg_4910" s="T225">Эһэ буолан батан истэ Тимир Һаапка.</ta>
            <ta e="T235" id="Seg_4911" s="T230">Бу туккары һук көтүү айан.</ta>
            <ta e="T244" id="Seg_4912" s="T235">Эһэ һуола баранар да, кыыл атыыра буолан ойон каалаактаабыт.</ta>
            <ta e="T250" id="Seg_4913" s="T244">Һөгө агай иһэр, биэк батар догоро.</ta>
            <ta e="T254" id="Seg_4914" s="T250">Тимир Һаапка кыыл атыыра буолла.</ta>
            <ta e="T263" id="Seg_4915" s="T254">"Идэҥ да элэтэ ини, һитээ инибин һубукаан", — дии һаныыр.</ta>
            <ta e="T269" id="Seg_4916" s="T263">Көрбүтэ: биир һиргэ догоро балчайан турар.</ta>
            <ta e="T282" id="Seg_4917" s="T269">Чугаһаан көрбүтэ: икки агай атага чочойон каалбыт, һүһүөктэринэн быһан, тууса атактарын чэкэлитэн кээспит.</ta>
            <ta e="T291" id="Seg_4918" s="T282">Һол кордуккаан Тимир Һаапка атагын биһаат, тууса буолан чэкэлийэн истэ.</ta>
            <ta e="T297" id="Seg_4919" s="T291">Төһө да буолумуйа, тууса көппөйө һытар.</ta>
            <ta e="T301" id="Seg_4920" s="T297">Үөрэр: "һиттим ээ!" — диир. </ta>
            <ta e="T312" id="Seg_4921" s="T301">Бу туусага тиийбитэ, тууса постуой агай һытар, мэйиитэ агай чэкэлийэн каалбыт.</ta>
            <ta e="T320" id="Seg_4922" s="T312">"Кайа доо, бу того кэрдинэр бэйэтин?" — диэн һаныыр.</ta>
            <ta e="T326" id="Seg_4923" s="T320">"Бугурдуккаан туок мөккүөнэй?!" — диэн одуургуу истэ.</ta>
            <ta e="T330" id="Seg_4924" s="T326">Мэйии буолан чэкэлийэн истэ.</ta>
            <ta e="T334" id="Seg_4925" s="T330">Арай мэйии чанкайа һытар.</ta>
            <ta e="T337" id="Seg_4926" s="T334">"Дьэ һиттим ини!" </ta>
            <ta e="T342" id="Seg_4927" s="T337">Арай икки карага уһуллан барбыт.</ta>
            <ta e="T346" id="Seg_4928" s="T342">"Ороо, бу туок киһитэй?</ta>
            <ta e="T352" id="Seg_4929" s="T346">Айыыны батабын дуу, абааһыны дуу?" — диир.</ta>
            <ta e="T358" id="Seg_4930" s="T352">Икки карага чэкэрийэн истэ карактар һуолларынан.</ta>
            <ta e="T367" id="Seg_4931" s="T358">Каһан эрэ бу икки карак һуола биир дьиэкээҥҥэ тийэр.</ta>
            <ta e="T374" id="Seg_4932" s="T367">Бу тийэн икки карак һуола дьиэгэ киирбит.</ta>
            <ta e="T377" id="Seg_4933" s="T374">— Дьэ элэӈ ини!</ta>
            <ta e="T382" id="Seg_4934" s="T377">— икки кэлин карак дьиэгэ киирдэ.</ta>
            <ta e="T392" id="Seg_4935" s="T382">Киирбитэ уҥа диэкки ороҥҥо эмээксин олорор, карактара кэтэгэриин чагылыһа һыталлар.</ta>
            <ta e="T400" id="Seg_4936" s="T392">Били икки карак киирбитигэр инники икки карак һаналаак: </ta>
            <ta e="T406" id="Seg_4937" s="T400">— О да, киһини батар киһи һылайааччы.</ta>
            <ta e="T408" id="Seg_4938" s="T406">Эн һынньан.</ta>
            <ta e="T414" id="Seg_4939" s="T408">Мин туусаларбытын, мэйиилэрбитин комуйтаан эгэлиэм, — диир.</ta>
            <ta e="T417" id="Seg_4940" s="T414">Догоро Тимир Һаапка онуга: </ta>
            <ta e="T425" id="Seg_4941" s="T417">— Бу илэ өйүнэн һылдьар киһигин дуу, һуок дуу?!</ta>
            <ta e="T432" id="Seg_4942" s="T425">Бу биһиги киһи кыайан төннүбэт һиригэр кэллибит!</ta>
            <ta e="T440" id="Seg_4943" s="T432">Дьэ аны эн батаар миньигин, һитээр мин идэбин.</ta>
            <ta e="T445" id="Seg_4944" s="T440">Билэгиэн: бу Пургаа иччитигэр кэллибит.</ta>
            <ta e="T452" id="Seg_4945" s="T445">һарсиэрда эрдэһит киһи турар кэмигэр туран таксыам.</ta>
            <ta e="T459" id="Seg_4946" s="T452">Маныгам һүүстүү өлгөбүүннээк (һэтиилээк) икки кыргыттар кэлиэктэрэ.</ta>
            <ta e="T478" id="Seg_4947" s="T459">Маныга мин такса көтөммүн, бастакы кыыһы ньуогуһутун уолугуттан тутуом да көтүтүөм, бу һүүс һэтииттэн бииригэр эмэ катаннаккына тийсиэҥ, дойдугар.</ta>
            <ta e="T484" id="Seg_4948" s="T478">Оттон катамматаккына, бу дойдуга өлүөҥ! — диир.</ta>
            <ta e="T491" id="Seg_4949" s="T484">Дьэ догоро, Һэлии Кур, туусаны, атактары, мэйиилэри эгэллэ.</ta>
            <ta e="T496" id="Seg_4950" s="T491">Бэйэлэрэ бэйэлэринэн киһи буолаттаан кааллылар.</ta>
            <ta e="T503" id="Seg_4951" s="T496">Утуйтаан баран Һэлии Кур уһуктар догоро таксыбыт тыаһыгар.</ta>
            <ta e="T513" id="Seg_4952" s="T503">Һелии Кур такса көппүтэ, һүүс һэтии элээннэнэн эрдэгинэ, бүтэһик һэтиигэ иилиһиннэ.</ta>
            <ta e="T519" id="Seg_4953" s="T513">Каллаанынан да, һиринэн да барарын дьүүллээбэтэгэ.</ta>
            <ta e="T530" id="Seg_4954" s="T519">Кобуо койут, кэбиэ кэнэгэс һүүс һэтии токтообутугар көрбүтэ, догоро дойдутугар тириэрбит.</ta>
            <ta e="T534" id="Seg_4955" s="T530">Дьэ Тимир Һаапка диир догорун: </ta>
            <ta e="T551" id="Seg_4956" s="T534">— Дьэ ити һүүс һэтии аҥар ойогоһугар үс чээлкээ атыыр көлүллэн турар, аҥар ойогоһугар үс кара атыыр турар.</ta>
            <ta e="T566" id="Seg_4957" s="T551">Дьэ мин ити үс чээлкээ атыыры өлөрүөм, тириилэрин кастыам, эн үс караны өлөрүөҥ, кастыаҥ тириилэрин.</ta>
            <ta e="T574" id="Seg_4958" s="T566">Билэгиэн: бу Пургаа иччитин ылгын кыыстарын эгэллим албаспынан.</ta>
            <ta e="T582" id="Seg_4959" s="T574">Аны итинтилэрин батан үс убайа Пургаа иччилэрэ кэлиэктэрэ.</ta>
            <ta e="T596" id="Seg_4960" s="T582">Ол киһилэр кэллэктэринэ, кэтэгэриин үс чээлкээ тириини тэлгиэм, эн ити үс коҥномуой тириигин тэлгиэр.</ta>
            <ta e="T605" id="Seg_4961" s="T596">Дьэ улакан чааҥҥар толору уута кыыйнар, мин эмиэ кыыйнарыам.</ta>
            <ta e="T609" id="Seg_4962" s="T605">Бука маннай миэкэ ыалдьыттыактара.</ta>
            <ta e="T615" id="Seg_4963" s="T609">Миигиттэн тотон, һөбүлээн тагыстактарына, эйиэкэ ыалланыактара.</ta>
            <ta e="T621" id="Seg_4964" s="T615">Мин кинилэргэ тугу эмэ көллөрүөм, киирсээр.</ta>
            <ta e="T631" id="Seg_4965" s="T621">Мин тугу көрдөрөбүн да, үтүктэн кинилэргэ көрдөрөөр! — диэтэ, үөрэттэ догорун.</ta>
            <ta e="T641" id="Seg_4966" s="T631">Төһө да буолумуйа Пургаа иччилэрэ болоһоннон кэллилэр, кам муус дьоннор.</ta>
            <ta e="T646" id="Seg_4967" s="T641">Киирэннэр өтөрү-һөтөрү "дорообо!" да диэбэттэр.</ta>
            <ta e="T652" id="Seg_4968" s="T646">Тимир Һаапка дьиэтигэр үс чээлкээ тириигэ олороллор.</ta>
            <ta e="T656" id="Seg_4969" s="T652">Тимир Һаапка идэлэнииһи бу дьоҥҥо.</ta>
            <ta e="T681" id="Seg_4970" s="T656">Кыыйна турар улакан чааҥҥа, һэлии муннулаак таллан куогас буолаат, "Аа-уу" диэт умсан каалар, дайда-дайбытынан көтөн таксан ийэтин быарынан һэлип гынар, онтон ийэтин айагын устун такса көппүт.</ta>
            <ta e="T696" id="Seg_4971" s="T681">Дьэ онтон агатын быарынан һэлип гыммыт, агатын айагынан һанардыы муоһун һуллаабыт кыыл атыыра буолан таксар.</ta>
            <ta e="T709" id="Seg_4972" s="T696">Бу кэнниттэн Тимир Һаапка киһи буолар да, аланааннан бу кыыл атыырын һупту ытан түһэрэр.</ta>
            <ta e="T721" id="Seg_4973" s="T709">Өлөрөр да маныга кастырыта һүлэн мас атыйакка һиикэйдии-буһуулуу тардан кээһэр, ыалдьыттарга һогудаайдатан.</ta>
            <ta e="T726" id="Seg_4974" s="T721">Ыалдьыттара һаҥа-иҥэ һуок, аһыырга барбыттар.</ta>
            <ta e="T733" id="Seg_4975" s="T726">Күһүҥҥү атыыртан аматтан муостаак туйагын ордорон кэбистилэр.</ta>
            <ta e="T736" id="Seg_4976" s="T733">Һэлии Кур көрөн олорунаактаата.</ta>
            <ta e="T742" id="Seg_4977" s="T736">Аһаан бүтэллэр да, уостарын һоттоон таксаллар.</ta>
            <ta e="T747" id="Seg_4978" s="T742">Һэлии Кур дьиэтигэр ыстанар, ыалдьыттары тоһуйуна.</ta>
            <ta e="T754" id="Seg_4979" s="T747">Ыалдьыттара киирэн үс коҥномуой тириигэ олоро биэрэллэр. </ta>
            <ta e="T772" id="Seg_4980" s="T754">Һэлии Кур "А-уук" диэт, бытта куогаскаан буолаат, чааӈӈа умсар, ийэтин быарыгар һэлип гынар, айагын устун таксар, агатын быарыгар һэлип гынар.</ta>
            <ta e="T783" id="Seg_4981" s="T772">Агатын айагын устун тугуттан эрэ куһаган тулаайакии тугуккаан буолан такса көппүт.</ta>
            <ta e="T794" id="Seg_4982" s="T783">Кэнниттэн һаалаак киһи таксар да ыалдьыттар иннилэригэр эпсэри ытар, астаан кэбиһэр.</ta>
            <ta e="T797" id="Seg_4983" s="T794">Ыалдьыттарга тардан кээһэр.</ta>
            <ta e="T800" id="Seg_4984" s="T797">Тардан агай кээспитэ.</ta>
            <ta e="T805" id="Seg_4985" s="T800">Дьиэтэ-уота канна да барбыттарын билбэтилэр.</ta>
            <ta e="T815" id="Seg_4986" s="T805">Пургаа иччилэрэ өһүргэнэн бу дьиэни, Һэлии Куру ийэлиини-агалыыны көтүтэн болоһоннон испиттэрэ.</ta>
            <ta e="T819" id="Seg_4987" s="T815">Арай Тимир Һаапка ийэлиин-агалыын каалбыт. </ta>
            <ta e="T825" id="Seg_4988" s="T819">Пургаа кыыһын дьактарданан байан-тайан олорбута үһү.</ta>
            <ta e="T826" id="Seg_4989" s="T825">Элэтэ.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T8" id="Seg_4990" s="T0">Timir Haːpka onuga Heliː Kur di͡en ikki u͡ol kihiler olorbuttar. </ta>
            <ta e="T11" id="Seg_4991" s="T8">Ikki͡en agalaːktar, ijeleːkter. </ta>
            <ta e="T17" id="Seg_4992" s="T11">Dʼe bu oloronnor Heliː Kura diːr Timir Haːpkatɨn: </ta>
            <ta e="T23" id="Seg_4993" s="T17">"Dogoːr, bu bihigi tu͡ok ideleːk bu͡olabɨt? </ta>
            <ta e="T27" id="Seg_4994" s="T23">Togo ide idebitin bilsibeppit?"</ta>
            <ta e="T31" id="Seg_4995" s="T27">Mekti͡e bɨldʼahaːrɨ gɨnar bɨhɨːlaːk. </ta>
            <ta e="T40" id="Seg_4996" s="T31">Dogoro "eː, dogor, tu͡okputun bɨldʼahan mekti͡e bɨldʼahɨ͡akpɨtɨj" di͡en bu͡olummat. </ta>
            <ta e="T42" id="Seg_4997" s="T40">Dogoro tehippet: </ta>
            <ta e="T44" id="Seg_4998" s="T42">"Togo, dogor! </ta>
            <ta e="T48" id="Seg_4999" s="T44">En idegin bili͡ekpin bagarabɨn!" </ta>
            <ta e="T50" id="Seg_5000" s="T48">Dogoro bu͡olummat. </ta>
            <ta e="T64" id="Seg_5001" s="T50">Heliː Kur "min kürü͡öm enʼigitten, bat minʼiːgin, min kajdak bu͡olabɨn da, hogordukkaːn bu͡olan is" diːr. </ta>
            <ta e="T65" id="Seg_5002" s="T64">Dogoro: </ta>
            <ta e="T68" id="Seg_5003" s="T65">"Batɨ͡aktɨbɨn duː, ajɨlaːgɨn." </ta>
            <ta e="T77" id="Seg_5004" s="T68">"Harsi͡erda erdehit kihi turar kemiger bataːr minʼigin", diːr Heliː Kur. </ta>
            <ta e="T82" id="Seg_5005" s="T77">Innʼe dehen araksallar dʼi͡e dʼi͡eleriger. </ta>
            <ta e="T86" id="Seg_5006" s="T82">Timir Haːpka taksan utuja hɨtar. </ta>
            <ta e="T88" id="Seg_5007" s="T86">Uhuktan keler: </ta>
            <ta e="T91" id="Seg_5008" s="T88">"Dogorum baːra dʼürü?" </ta>
            <ta e="T99" id="Seg_5009" s="T91">Dogorugar kelbite, Heliː Kur orono čoŋkojo hɨtar — baran kaːlbɨt. </ta>
            <ta e="T106" id="Seg_5010" s="T99">Timir Haːpka hu͡ol kajar, hu͡olu bulbat, elete bu͡olar. </ta>
            <ta e="T109" id="Seg_5011" s="T106">Kiːren oronun kuptujar. </ta>
            <ta e="T120" id="Seg_5012" s="T109">Heliː Kur oronun annɨttan üːteːn kutujagɨn koroːno čoŋojo hɨtar, atɨn hu͡ol hu͡ok. </ta>
            <ta e="T126" id="Seg_5013" s="T120">Timir Haːpka kutujak bu͡olla da koroːnunan tüste. </ta>
            <ta e="T133" id="Seg_5014" s="T126">ɨnaraːŋŋɨta, koroːnton taksaːt, goronu͡ok bu͡olaːt ɨstanan kaːlar. </ta>
            <ta e="T138" id="Seg_5015" s="T133">Timir Haːpka goronu͡ok bu͡olaːt batan iher. </ta>
            <ta e="T142" id="Seg_5016" s="T138">Huk kötüː baran iste. </ta>
            <ta e="T153" id="Seg_5017" s="T142">Bu goronu͡ok hu͡ola büter da, orto dojdu kɨrsa hu͡ola bara turda. </ta>
            <ta e="T156" id="Seg_5018" s="T153">Dogoro halla iste. </ta>
            <ta e="T162" id="Seg_5019" s="T156">Timir Haːpka kɨrsa bu͡olar da, ojon iste. </ta>
            <ta e="T170" id="Seg_5020" s="T162">Biːr hirge tüspüt da, Heliː Kura uskaːn bu͡olan ojbut. </ta>
            <ta e="T174" id="Seg_5021" s="T170">Kennikite uskaːn bu͡olan batta. </ta>
            <ta e="T178" id="Seg_5022" s="T174">Heliː Kur hahɨl bu͡olla onton. </ta>
            <ta e="T186" id="Seg_5023" s="T178">Kennikite emi͡e hahɨl bu͡olan hu͡olun agaj ütükten iste. </ta>
            <ta e="T191" id="Seg_5024" s="T186">Maːnɨta börö bu͡olan kötön nʼulujbut. </ta>
            <ta e="T194" id="Seg_5025" s="T191">Kennikite högö iste: </ta>
            <ta e="T199" id="Seg_5026" s="T194">"Mantɨkaja batɨmɨna kördömmüt bu͡ollaga", diːr. </ta>
            <ta e="T203" id="Seg_5027" s="T199">Börö bu͡olan batan iste. </ta>
            <ta e="T206" id="Seg_5028" s="T203">Töhönü-kaččanɨ barbɨta billibet. </ta>
            <ta e="T211" id="Seg_5029" s="T206">Heliː Kur hi͡egen bu͡olan leppeje turbut. </ta>
            <ta e="T219" id="Seg_5030" s="T211">Kennikite emi͡e, bi͡ek högö-högö, hi͡egen bu͡olan batan iste. </ta>
            <ta e="T225" id="Seg_5031" s="T219">Hi͡egenin hu͡ola büteːtin, ebekeː bu͡olan lippejbit. </ta>
            <ta e="T230" id="Seg_5032" s="T225">Ehe bu͡olan batan iste Timir Haːpka. </ta>
            <ta e="T235" id="Seg_5033" s="T230">Bu tukkarɨ huk kötüː ajan. </ta>
            <ta e="T244" id="Seg_5034" s="T235">Ehe hu͡ola baranar da, kɨːl atɨːra bu͡olan ojon kaːlaːktaːbɨt. </ta>
            <ta e="T250" id="Seg_5035" s="T244">Högö agaj iher, bi͡ek batar dogoro. </ta>
            <ta e="T254" id="Seg_5036" s="T250">Timir Haːpka kɨːl atɨːra bu͡olla. </ta>
            <ta e="T263" id="Seg_5037" s="T254">"Ideŋ da elete ini, hiteː inibin hubukaːn", diː hanɨːr. </ta>
            <ta e="T269" id="Seg_5038" s="T263">Körbüte, biːr hirge dogoro balčajan turar. </ta>
            <ta e="T282" id="Seg_5039" s="T269">Čugahaːn körbüte, ikki agaj ataga čočojon kaːlbɨt, hühü͡ökterinen bɨhan, tuːsa ataktarɨn čekeliten keːspit. </ta>
            <ta e="T291" id="Seg_5040" s="T282">Hol kordukkaːn Timir Haːpka atagɨn bɨhaːt, tuːsa bu͡olan čekelijen iste. </ta>
            <ta e="T297" id="Seg_5041" s="T291">Töhö da bu͡olumuja, tuːsa köppöjö hɨtar. </ta>
            <ta e="T301" id="Seg_5042" s="T297">Ü͡örer, "hittim eː", diːr. </ta>
            <ta e="T312" id="Seg_5043" s="T301">Bu tuːsaga tiːjbite, tuːsa postu͡oj agaj hɨtar, mejiːte agaj čekelijen kaːlbɨt. </ta>
            <ta e="T320" id="Seg_5044" s="T312">"Kaja doː, bu togo kerdiner bejetin", di͡en hanɨːr. </ta>
            <ta e="T326" id="Seg_5045" s="T320">"Bugurdukkaːn tu͡ok mökkü͡önej", di͡en oduːrguː iste. </ta>
            <ta e="T330" id="Seg_5046" s="T326">Mejiː bu͡olan čekelijen iste. </ta>
            <ta e="T334" id="Seg_5047" s="T330">Araj mejiː čankaja hɨtar. </ta>
            <ta e="T337" id="Seg_5048" s="T334">"Dʼe hittim ini!" </ta>
            <ta e="T342" id="Seg_5049" s="T337">Araj ikki karaga uhullan barbɨt. </ta>
            <ta e="T346" id="Seg_5050" s="T342">"Oroː, bu tu͡ok kihitej? </ta>
            <ta e="T352" id="Seg_5051" s="T346">Ajɨːnɨ batabɨn duː, abaːhɨnɨ duː", diːr. </ta>
            <ta e="T358" id="Seg_5052" s="T352">Ikki karaga čekerijen iste karaktar hu͡ollarɨnan. </ta>
            <ta e="T367" id="Seg_5053" s="T358">Kahan ere bu ikki karak hu͡ola biːr dʼi͡ekeːŋŋe tijer. </ta>
            <ta e="T374" id="Seg_5054" s="T367">Bu tijen ikki karak hu͡ola dʼi͡ege kiːrbit. </ta>
            <ta e="T377" id="Seg_5055" s="T374">"Dʼe eleŋ ini!" </ta>
            <ta e="T382" id="Seg_5056" s="T377">Ikki kelin karak dʼi͡ege kiːrde. </ta>
            <ta e="T392" id="Seg_5057" s="T382">Kiːrbite uŋa di͡ekki oroŋŋo emeːksin oloror, karaktara ketegeriːn čagɨlɨha hɨtallar. </ta>
            <ta e="T400" id="Seg_5058" s="T392">Bili ikki karak kiːrbitiger inniki ikki karak hanalaːk: </ta>
            <ta e="T406" id="Seg_5059" s="T400">"O da, kihini batar kihi hɨlajaːččɨ. </ta>
            <ta e="T408" id="Seg_5060" s="T406">En hɨnnʼan. </ta>
            <ta e="T414" id="Seg_5061" s="T408">Min tuːsalarbɨtɨn, mejiːlerbitin komujtaːn egeli͡em", diːr. </ta>
            <ta e="T417" id="Seg_5062" s="T414">Dogoro Timir Haːpka onuga: </ta>
            <ta e="T425" id="Seg_5063" s="T417">"Bu ile öjünen hɨldʼar kihigin duː, hu͡ok duː? </ta>
            <ta e="T432" id="Seg_5064" s="T425">Bu bihigi kihi kɨ͡ajan tönnübet hiriger kellibit! </ta>
            <ta e="T440" id="Seg_5065" s="T432">Dʼe anɨ en bataːr minʼigin, hiteːr min idebin. </ta>
            <ta e="T445" id="Seg_5066" s="T440">Bilegi͡en, bu Purgaː iččitiger kellibit. </ta>
            <ta e="T452" id="Seg_5067" s="T445">Harsi͡erda erdehit kihi turar kemiger turan taksɨ͡am. </ta>
            <ta e="T459" id="Seg_5068" s="T452">Manɨga hüːstüː ölgöbüːnneːk hetiːleːk ikki kɨrgɨttar keli͡ektere. </ta>
            <ta e="T478" id="Seg_5069" s="T459">Manɨga min taksa kötömmün, bastakɨ kɨːhɨ nʼu͡oguhutun u͡oluguttan tutu͡om da kötütü͡öm, bu hüːs hetiːtten biːriger eme katannakkɨna tijsi͡eŋ, dojdugar. </ta>
            <ta e="T484" id="Seg_5070" s="T478">Otton katammatakkɨna, bu dojduga ölü͡öŋ", diːr. </ta>
            <ta e="T491" id="Seg_5071" s="T484">Dʼe dogoro, Heliː Kur, tuːsanɨ, ataktarɨ, mejiːleri egelle. </ta>
            <ta e="T496" id="Seg_5072" s="T491">Bejelere bejelerinen kihi bu͡olattaːn kaːllɨlar. </ta>
            <ta e="T503" id="Seg_5073" s="T496">Utujtaːn baran Heliː Kur uhuktar dogoro taksɨbɨt tɨ͡ahɨgar. </ta>
            <ta e="T513" id="Seg_5074" s="T503">Heliː Kur taksa köppüte, hüːs hetiː eleːnnenen erdegine, bütehik hetiːge iːlihinne. </ta>
            <ta e="T519" id="Seg_5075" s="T513">Kallaːnɨnan da, hirinen da bararɨn dʼüːlleːbetege. </ta>
            <ta e="T530" id="Seg_5076" s="T519">Kobu͡o kojut, kebi͡e keneges hüːs hetiː toktoːbutugar körbüte, dogoro dojdutugar tiri͡erbit. </ta>
            <ta e="T534" id="Seg_5077" s="T530">Dʼe Timir Haːpka diːr dogorun: </ta>
            <ta e="T551" id="Seg_5078" s="T534">"Dʼe iti hüːs hetiː aŋar ojogohugar üs čeːlkeː atɨːr kölüllen turar, aŋar ojogohugar üs kara atɨːr turar. </ta>
            <ta e="T566" id="Seg_5079" s="T551">Dʼe min iti üs čeːlkeː atɨːrɨ ölörü͡öm, tiriːlerin kastɨ͡am, en üs karanɨ ölörü͡öŋ, kastɨ͡aŋ tiriːlerin. </ta>
            <ta e="T574" id="Seg_5080" s="T566">Bilegi͡en, bu Purgaː iččitin ɨlgɨn kɨːstarɨn egellim albaspɨnan. </ta>
            <ta e="T582" id="Seg_5081" s="T574">Anɨ itintilerin batan üs ubaja Purgaː iččilere keli͡ektere. </ta>
            <ta e="T596" id="Seg_5082" s="T582">Ol kihiler kellekterine, ketegeriːn üs čeːlkeː tiriːni telgi͡em, en iti üs koŋnomu͡oj tiriːgin telgi͡er. </ta>
            <ta e="T605" id="Seg_5083" s="T596">Dʼe ulakan čaːŋŋar toloru uːta kɨːjnar, min emi͡e kɨːjnarɨ͡am. </ta>
            <ta e="T609" id="Seg_5084" s="T605">Buka mannaj mi͡eke ɨ͡aldʼɨttɨ͡aktara. </ta>
            <ta e="T615" id="Seg_5085" s="T609">Miːgitten toton, höbüleːn tagɨstaktarɨna, eji͡eke ɨ͡allanɨ͡aktara. </ta>
            <ta e="T621" id="Seg_5086" s="T615">Min kinilerge tugu eme köllörü͡öm, kiːrseːr. </ta>
            <ta e="T631" id="Seg_5087" s="T621">Min tugu kördöröbün da, ütükten kinilerge kördöröːr", di͡ete, ü͡örette dogorun. </ta>
            <ta e="T641" id="Seg_5088" s="T631">Töhö da bu͡olumuja Purgaː iččilere bolohonnon kelliler, kam muːs dʼonnor. </ta>
            <ta e="T646" id="Seg_5089" s="T641">Kiːrenner ötörü-hötörü "doroːbo" da di͡ebetter. </ta>
            <ta e="T652" id="Seg_5090" s="T646">Timir Haːpka dʼi͡etiger üs čeːlkeː tiriːge olorollor. </ta>
            <ta e="T656" id="Seg_5091" s="T652">Timir Haːpka ideleniːhi bu dʼoŋŋo. </ta>
            <ta e="T681" id="Seg_5092" s="T656">Kɨːjna turar ulakan čaːŋŋa, heliː munnulaːk tallan ku͡ogas bu͡olaːt, "Aː uː" di͡et umsan kaːlar, dajda-dajbɨtɨnan kötön taksan ijetin bɨ͡arɨnan helip gɨnar, onton ijetin ajagɨn ustun taksa köppüt. </ta>
            <ta e="T696" id="Seg_5093" s="T681">Dʼe onton agatɨn bɨ͡arɨnan helip gɨmmɨt, agatɨn ajagɨnan hanardɨː mu͡ohun hullaːbɨt kɨːl atɨːra bu͡olan taksar. </ta>
            <ta e="T709" id="Seg_5094" s="T696">Bu kennitten Timir Haːpka kihi bu͡olar da, alanaːnnan bu kɨːl atɨːrɨn huptu ɨtan tüherer. </ta>
            <ta e="T721" id="Seg_5095" s="T709">Ölörör da manɨga kastɨrɨta hülen mas atɨjakka hiːkejdiː-buhuːluː tardan keːher, ɨ͡aldʼɨttarga hogudaːjdatan. </ta>
            <ta e="T726" id="Seg_5096" s="T721">ɨ͡aldʼɨttara haŋa-iŋe hu͡ok, ahɨːrga barbɨttar. </ta>
            <ta e="T733" id="Seg_5097" s="T726">Kühüŋŋü atɨːrtan amattan mu͡ostaːk tujagɨn ordoron kebistiler. </ta>
            <ta e="T736" id="Seg_5098" s="T733">Heliː Kur körön olorunaːktaːta. </ta>
            <ta e="T742" id="Seg_5099" s="T736">Ahaːn büteller da, u͡ostarɨn hottoːn taksallar. </ta>
            <ta e="T747" id="Seg_5100" s="T742">Heliː Kur dʼi͡etiger ɨstanar, ɨ͡aldʼɨttarɨ tohujuna. </ta>
            <ta e="T754" id="Seg_5101" s="T747">ɨ͡aldʼɨttara kiːren üs koŋnomu͡oj tiriːge oloro bi͡ereller. </ta>
            <ta e="T772" id="Seg_5102" s="T754">Heliː Kur "A uːk" di͡et, bɨːtta ku͡ogaskaːn bu͡olaːt, čaːŋŋa umsar, ijetin bɨ͡arɨgar helip gɨnar, ajagɨn ustun taksar, agatɨn bɨ͡arɨgar helip gɨnar. </ta>
            <ta e="T783" id="Seg_5103" s="T772">Agatɨn ajagɨn ustun tuguttan ere kuhagan tulaːjakiː tugukkaːn bu͡olan taksa köppüt. </ta>
            <ta e="T794" id="Seg_5104" s="T783">Kennitten haːlaːk kihi taksar da ɨ͡aldʼɨttar innileriger epseri ɨtar, astaːn kebiher. </ta>
            <ta e="T797" id="Seg_5105" s="T794">ɨ͡aldʼɨttarga tardan keːher. </ta>
            <ta e="T800" id="Seg_5106" s="T797">Tardan agaj keːspite. </ta>
            <ta e="T805" id="Seg_5107" s="T800">Dʼi͡ete-u͡ota kanna da barbɨttarɨn bilbetiler. </ta>
            <ta e="T815" id="Seg_5108" s="T805">Purgaː iččilere öhürgenen bu dʼi͡eni, Heliː Kuru ijeliːni-agalɨːnɨ kötüten bolohonnon ispittere. </ta>
            <ta e="T819" id="Seg_5109" s="T815">Araj Timir Haːpka ijeliːn-agalɨːn kaːlbɨt. </ta>
            <ta e="T825" id="Seg_5110" s="T819">Purgaː kɨːhɨn dʼaktardanan bajan-tajan olorbuta ühü. </ta>
            <ta e="T826" id="Seg_5111" s="T825">Elete. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_5112" s="T0">Timir Haːpka</ta>
            <ta e="T2" id="Seg_5113" s="T1">onu-ga</ta>
            <ta e="T3" id="Seg_5114" s="T2">Heliː Kur</ta>
            <ta e="T4" id="Seg_5115" s="T3">di͡e-n</ta>
            <ta e="T5" id="Seg_5116" s="T4">ikki</ta>
            <ta e="T6" id="Seg_5117" s="T5">u͡ol</ta>
            <ta e="T7" id="Seg_5118" s="T6">kihi-ler</ta>
            <ta e="T8" id="Seg_5119" s="T7">olor-but-tar</ta>
            <ta e="T9" id="Seg_5120" s="T8">ikk-i͡en</ta>
            <ta e="T10" id="Seg_5121" s="T9">aga-laːk-tar</ta>
            <ta e="T11" id="Seg_5122" s="T10">ije-leːk-ter</ta>
            <ta e="T12" id="Seg_5123" s="T11">dʼe</ta>
            <ta e="T13" id="Seg_5124" s="T12">bu</ta>
            <ta e="T14" id="Seg_5125" s="T13">olor-on-nor</ta>
            <ta e="T15" id="Seg_5126" s="T14">Heliː Kur-a</ta>
            <ta e="T16" id="Seg_5127" s="T15">diː-r</ta>
            <ta e="T17" id="Seg_5128" s="T16">Timir Haːpka-tɨ-n</ta>
            <ta e="T18" id="Seg_5129" s="T17">dogoːr</ta>
            <ta e="T19" id="Seg_5130" s="T18">bu</ta>
            <ta e="T20" id="Seg_5131" s="T19">bihigi</ta>
            <ta e="T21" id="Seg_5132" s="T20">tu͡ok</ta>
            <ta e="T22" id="Seg_5133" s="T21">ide-leːk</ta>
            <ta e="T23" id="Seg_5134" s="T22">bu͡ol-a-bɨt</ta>
            <ta e="T24" id="Seg_5135" s="T23">togo</ta>
            <ta e="T25" id="Seg_5136" s="T24">ide</ta>
            <ta e="T26" id="Seg_5137" s="T25">ide-biti-n</ta>
            <ta e="T27" id="Seg_5138" s="T26">bil-s-i-bep-pit</ta>
            <ta e="T28" id="Seg_5139" s="T27">mekti͡e</ta>
            <ta e="T29" id="Seg_5140" s="T28">bɨldʼ-a-h-aːrɨ</ta>
            <ta e="T30" id="Seg_5141" s="T29">gɨn-ar</ta>
            <ta e="T31" id="Seg_5142" s="T30">bɨhɨːlaːk</ta>
            <ta e="T32" id="Seg_5143" s="T31">dogor-o</ta>
            <ta e="T33" id="Seg_5144" s="T32">eː</ta>
            <ta e="T34" id="Seg_5145" s="T33">dogor</ta>
            <ta e="T35" id="Seg_5146" s="T34">tu͡ok-putu-n</ta>
            <ta e="T36" id="Seg_5147" s="T35">bɨldʼ-a-h-an</ta>
            <ta e="T37" id="Seg_5148" s="T36">mekti͡e</ta>
            <ta e="T38" id="Seg_5149" s="T37">bɨldʼ-a-h-ɨ͡ak-pɨt=ɨj</ta>
            <ta e="T39" id="Seg_5150" s="T38">di͡e-n</ta>
            <ta e="T40" id="Seg_5151" s="T39">bu͡ol-u-m-mat</ta>
            <ta e="T41" id="Seg_5152" s="T40">dogor-o</ta>
            <ta e="T42" id="Seg_5153" s="T41">tehi-p-pet</ta>
            <ta e="T43" id="Seg_5154" s="T42">togo</ta>
            <ta e="T44" id="Seg_5155" s="T43">dogor</ta>
            <ta e="T45" id="Seg_5156" s="T44">en</ta>
            <ta e="T46" id="Seg_5157" s="T45">ide-gi-n</ta>
            <ta e="T47" id="Seg_5158" s="T46">bil-i͡ek-pi-n</ta>
            <ta e="T48" id="Seg_5159" s="T47">bagar-a-bɨn</ta>
            <ta e="T49" id="Seg_5160" s="T48">dogor-o</ta>
            <ta e="T50" id="Seg_5161" s="T49">bu͡ol-u-m-mat</ta>
            <ta e="T51" id="Seg_5162" s="T50">Heliː Kur</ta>
            <ta e="T52" id="Seg_5163" s="T51">min</ta>
            <ta e="T53" id="Seg_5164" s="T52">kür-ü͡ö-m</ta>
            <ta e="T54" id="Seg_5165" s="T53">enʼigi-tten</ta>
            <ta e="T55" id="Seg_5166" s="T54">bat</ta>
            <ta e="T56" id="Seg_5167" s="T55">minʼiːgi-n</ta>
            <ta e="T57" id="Seg_5168" s="T56">min</ta>
            <ta e="T58" id="Seg_5169" s="T57">kajdak</ta>
            <ta e="T59" id="Seg_5170" s="T58">bu͡ol-a-bɨn</ta>
            <ta e="T60" id="Seg_5171" s="T59">da</ta>
            <ta e="T61" id="Seg_5172" s="T60">hogorduk-kaːn</ta>
            <ta e="T62" id="Seg_5173" s="T61">bu͡ol-an</ta>
            <ta e="T63" id="Seg_5174" s="T62">is</ta>
            <ta e="T64" id="Seg_5175" s="T63">diː-r</ta>
            <ta e="T65" id="Seg_5176" s="T64">dogor-o</ta>
            <ta e="T66" id="Seg_5177" s="T65">bat-ɨ͡aktɨ-bɨn</ta>
            <ta e="T67" id="Seg_5178" s="T66">duː</ta>
            <ta e="T68" id="Seg_5179" s="T67">ajɨlaːgɨn</ta>
            <ta e="T69" id="Seg_5180" s="T68">harsi͡erda</ta>
            <ta e="T70" id="Seg_5181" s="T69">erde-hit</ta>
            <ta e="T71" id="Seg_5182" s="T70">kihi</ta>
            <ta e="T72" id="Seg_5183" s="T71">tur-ar</ta>
            <ta e="T73" id="Seg_5184" s="T72">kem-i-ger</ta>
            <ta e="T74" id="Seg_5185" s="T73">bat-aːr</ta>
            <ta e="T75" id="Seg_5186" s="T74">minʼigi-n</ta>
            <ta e="T76" id="Seg_5187" s="T75">diː-r</ta>
            <ta e="T77" id="Seg_5188" s="T76">Heliː Kur</ta>
            <ta e="T78" id="Seg_5189" s="T77">innʼe</ta>
            <ta e="T79" id="Seg_5190" s="T78">d-e-h-en</ta>
            <ta e="T80" id="Seg_5191" s="T79">araks-al-lar</ta>
            <ta e="T81" id="Seg_5192" s="T80">dʼi͡e</ta>
            <ta e="T82" id="Seg_5193" s="T81">dʼi͡e-leri-ger</ta>
            <ta e="T83" id="Seg_5194" s="T82">Timir Haːpka</ta>
            <ta e="T84" id="Seg_5195" s="T83">taks-an</ta>
            <ta e="T85" id="Seg_5196" s="T84">utuj-a</ta>
            <ta e="T86" id="Seg_5197" s="T85">hɨt-ar</ta>
            <ta e="T87" id="Seg_5198" s="T86">uhukt-an</ta>
            <ta e="T88" id="Seg_5199" s="T87">kel-er</ta>
            <ta e="T89" id="Seg_5200" s="T88">dogor-u-m</ta>
            <ta e="T90" id="Seg_5201" s="T89">baːr-a</ta>
            <ta e="T91" id="Seg_5202" s="T90">dʼürü</ta>
            <ta e="T92" id="Seg_5203" s="T91">dogor-u-gar</ta>
            <ta e="T93" id="Seg_5204" s="T92">kel-bit-e</ta>
            <ta e="T94" id="Seg_5205" s="T93">Heliː Kur</ta>
            <ta e="T95" id="Seg_5206" s="T94">oron-o</ta>
            <ta e="T96" id="Seg_5207" s="T95">čoŋkoj-o</ta>
            <ta e="T97" id="Seg_5208" s="T96">hɨt-ar</ta>
            <ta e="T98" id="Seg_5209" s="T97">bar-an</ta>
            <ta e="T99" id="Seg_5210" s="T98">kaːl-bɨt</ta>
            <ta e="T100" id="Seg_5211" s="T99">Timir Haːpka</ta>
            <ta e="T101" id="Seg_5212" s="T100">hu͡ol</ta>
            <ta e="T102" id="Seg_5213" s="T101">kaj-ar</ta>
            <ta e="T103" id="Seg_5214" s="T102">hu͡ol-u</ta>
            <ta e="T104" id="Seg_5215" s="T103">bul-bat</ta>
            <ta e="T105" id="Seg_5216" s="T104">ele-te</ta>
            <ta e="T106" id="Seg_5217" s="T105">bu͡ol-ar</ta>
            <ta e="T107" id="Seg_5218" s="T106">kiːr-en</ta>
            <ta e="T108" id="Seg_5219" s="T107">oron-u-n</ta>
            <ta e="T109" id="Seg_5220" s="T108">kuptu-j-ar</ta>
            <ta e="T110" id="Seg_5221" s="T109">Heliː Kur</ta>
            <ta e="T111" id="Seg_5222" s="T110">oron-u-n</ta>
            <ta e="T112" id="Seg_5223" s="T111">ann-ɨ-ttan</ta>
            <ta e="T113" id="Seg_5224" s="T112">üːteːn</ta>
            <ta e="T114" id="Seg_5225" s="T113">kutujag-ɨ-n</ta>
            <ta e="T115" id="Seg_5226" s="T114">koroːn-o</ta>
            <ta e="T116" id="Seg_5227" s="T115">čoŋoj-o</ta>
            <ta e="T117" id="Seg_5228" s="T116">hɨt-ar</ta>
            <ta e="T118" id="Seg_5229" s="T117">atɨn</ta>
            <ta e="T119" id="Seg_5230" s="T118">hu͡ol</ta>
            <ta e="T120" id="Seg_5231" s="T119">hu͡ok</ta>
            <ta e="T121" id="Seg_5232" s="T120">Timir Haːpka</ta>
            <ta e="T122" id="Seg_5233" s="T121">kutujak</ta>
            <ta e="T123" id="Seg_5234" s="T122">bu͡ol-l-a</ta>
            <ta e="T124" id="Seg_5235" s="T123">da</ta>
            <ta e="T125" id="Seg_5236" s="T124">koroːn-u-nan</ta>
            <ta e="T126" id="Seg_5237" s="T125">tüs-t-e</ta>
            <ta e="T127" id="Seg_5238" s="T126">ɨnaraː-ŋŋɨ-ta</ta>
            <ta e="T128" id="Seg_5239" s="T127">koroːn-ton</ta>
            <ta e="T129" id="Seg_5240" s="T128">taks-aːt</ta>
            <ta e="T130" id="Seg_5241" s="T129">goronu͡ok</ta>
            <ta e="T131" id="Seg_5242" s="T130">bu͡ol-aːt</ta>
            <ta e="T132" id="Seg_5243" s="T131">ɨstan-an</ta>
            <ta e="T133" id="Seg_5244" s="T132">kaːl-ar</ta>
            <ta e="T134" id="Seg_5245" s="T133">Timir Haːpka</ta>
            <ta e="T135" id="Seg_5246" s="T134">goronu͡ok</ta>
            <ta e="T136" id="Seg_5247" s="T135">bu͡ol-aːt</ta>
            <ta e="T137" id="Seg_5248" s="T136">bat-an</ta>
            <ta e="T138" id="Seg_5249" s="T137">ih-er</ta>
            <ta e="T139" id="Seg_5250" s="T138">huk</ta>
            <ta e="T140" id="Seg_5251" s="T139">köt-üː</ta>
            <ta e="T141" id="Seg_5252" s="T140">bar-an</ta>
            <ta e="T142" id="Seg_5253" s="T141">is-t-e</ta>
            <ta e="T143" id="Seg_5254" s="T142">bu</ta>
            <ta e="T144" id="Seg_5255" s="T143">goronu͡ok</ta>
            <ta e="T145" id="Seg_5256" s="T144">hu͡ol-a</ta>
            <ta e="T146" id="Seg_5257" s="T145">büt-er</ta>
            <ta e="T147" id="Seg_5258" s="T146">da</ta>
            <ta e="T148" id="Seg_5259" s="T147">orto</ta>
            <ta e="T149" id="Seg_5260" s="T148">dojdu</ta>
            <ta e="T150" id="Seg_5261" s="T149">kɨrsa</ta>
            <ta e="T151" id="Seg_5262" s="T150">hu͡ol-a</ta>
            <ta e="T152" id="Seg_5263" s="T151">bar-a</ta>
            <ta e="T153" id="Seg_5264" s="T152">tur-d-a</ta>
            <ta e="T154" id="Seg_5265" s="T153">dogor-o</ta>
            <ta e="T155" id="Seg_5266" s="T154">hall-a</ta>
            <ta e="T156" id="Seg_5267" s="T155">is-t-e</ta>
            <ta e="T157" id="Seg_5268" s="T156">Timir Haːpka</ta>
            <ta e="T158" id="Seg_5269" s="T157">kɨrsa</ta>
            <ta e="T159" id="Seg_5270" s="T158">bu͡ol-ar</ta>
            <ta e="T160" id="Seg_5271" s="T159">da</ta>
            <ta e="T161" id="Seg_5272" s="T160">oj-on</ta>
            <ta e="T162" id="Seg_5273" s="T161">is-t-e</ta>
            <ta e="T163" id="Seg_5274" s="T162">biːr</ta>
            <ta e="T164" id="Seg_5275" s="T163">hir-ge</ta>
            <ta e="T165" id="Seg_5276" s="T164">tüs-püt</ta>
            <ta e="T166" id="Seg_5277" s="T165">da</ta>
            <ta e="T167" id="Seg_5278" s="T166">Heliː Kur-a</ta>
            <ta e="T168" id="Seg_5279" s="T167">uskaːn</ta>
            <ta e="T169" id="Seg_5280" s="T168">bu͡ol-an</ta>
            <ta e="T170" id="Seg_5281" s="T169">oj-but</ta>
            <ta e="T171" id="Seg_5282" s="T170">kenni-ki-te</ta>
            <ta e="T172" id="Seg_5283" s="T171">uskaːn</ta>
            <ta e="T173" id="Seg_5284" s="T172">bu͡ol-an</ta>
            <ta e="T174" id="Seg_5285" s="T173">bat-t-a</ta>
            <ta e="T175" id="Seg_5286" s="T174">Heliː Kur</ta>
            <ta e="T176" id="Seg_5287" s="T175">hahɨl</ta>
            <ta e="T177" id="Seg_5288" s="T176">bu͡ol-l-a</ta>
            <ta e="T178" id="Seg_5289" s="T177">onton</ta>
            <ta e="T179" id="Seg_5290" s="T178">kenni-ki-te</ta>
            <ta e="T180" id="Seg_5291" s="T179">emi͡e</ta>
            <ta e="T181" id="Seg_5292" s="T180">hahɨl</ta>
            <ta e="T182" id="Seg_5293" s="T181">bu͡ol-an</ta>
            <ta e="T183" id="Seg_5294" s="T182">hu͡ol-u-n</ta>
            <ta e="T184" id="Seg_5295" s="T183">agaj</ta>
            <ta e="T185" id="Seg_5296" s="T184">ütükt-en</ta>
            <ta e="T186" id="Seg_5297" s="T185">is-t-e</ta>
            <ta e="T187" id="Seg_5298" s="T186">maːnɨ-ta</ta>
            <ta e="T188" id="Seg_5299" s="T187">börö</ta>
            <ta e="T189" id="Seg_5300" s="T188">bu͡ol-an</ta>
            <ta e="T190" id="Seg_5301" s="T189">köt-ön</ta>
            <ta e="T191" id="Seg_5302" s="T190">nʼuluj-but</ta>
            <ta e="T192" id="Seg_5303" s="T191">kenni-ki-te</ta>
            <ta e="T193" id="Seg_5304" s="T192">hög-ö</ta>
            <ta e="T194" id="Seg_5305" s="T193">is-t-e</ta>
            <ta e="T195" id="Seg_5306" s="T194">man-tɨ-kaj-a</ta>
            <ta e="T196" id="Seg_5307" s="T195">bat-ɨ-mɨna</ta>
            <ta e="T197" id="Seg_5308" s="T196">körd-ö-m-müt</ta>
            <ta e="T198" id="Seg_5309" s="T197">bu͡ollaga</ta>
            <ta e="T199" id="Seg_5310" s="T198">diː-r</ta>
            <ta e="T200" id="Seg_5311" s="T199">börö</ta>
            <ta e="T201" id="Seg_5312" s="T200">bu͡ol-an</ta>
            <ta e="T202" id="Seg_5313" s="T201">bat-an</ta>
            <ta e="T203" id="Seg_5314" s="T202">is-t-e</ta>
            <ta e="T204" id="Seg_5315" s="T203">töhö-nü-kačča-nɨ</ta>
            <ta e="T205" id="Seg_5316" s="T204">bar-bɨt-a</ta>
            <ta e="T206" id="Seg_5317" s="T205">bil-l-i-bet</ta>
            <ta e="T207" id="Seg_5318" s="T206">Heliː Kur</ta>
            <ta e="T208" id="Seg_5319" s="T207">hi͡egen</ta>
            <ta e="T209" id="Seg_5320" s="T208">bu͡ol-an</ta>
            <ta e="T210" id="Seg_5321" s="T209">leppej-e</ta>
            <ta e="T211" id="Seg_5322" s="T210">tur-but</ta>
            <ta e="T212" id="Seg_5323" s="T211">kenni-ki-te</ta>
            <ta e="T213" id="Seg_5324" s="T212">emi͡e</ta>
            <ta e="T214" id="Seg_5325" s="T213">bi͡ek</ta>
            <ta e="T215" id="Seg_5326" s="T214">hög-ö-hög-ö</ta>
            <ta e="T216" id="Seg_5327" s="T215">hi͡egen</ta>
            <ta e="T217" id="Seg_5328" s="T216">bu͡ol-an</ta>
            <ta e="T218" id="Seg_5329" s="T217">bat-an</ta>
            <ta e="T219" id="Seg_5330" s="T218">is-t-e</ta>
            <ta e="T220" id="Seg_5331" s="T219">hi͡egen-i-n</ta>
            <ta e="T221" id="Seg_5332" s="T220">hu͡ol-a</ta>
            <ta e="T222" id="Seg_5333" s="T221">büt-eːt-in</ta>
            <ta e="T223" id="Seg_5334" s="T222">ebekeː</ta>
            <ta e="T224" id="Seg_5335" s="T223">bu͡ol-an</ta>
            <ta e="T225" id="Seg_5336" s="T224">lippej-bit</ta>
            <ta e="T226" id="Seg_5337" s="T225">ehe</ta>
            <ta e="T227" id="Seg_5338" s="T226">bu͡ol-an</ta>
            <ta e="T228" id="Seg_5339" s="T227">bat-an</ta>
            <ta e="T229" id="Seg_5340" s="T228">is-t-e</ta>
            <ta e="T230" id="Seg_5341" s="T229">Timir Haːpka</ta>
            <ta e="T231" id="Seg_5342" s="T230">bu</ta>
            <ta e="T232" id="Seg_5343" s="T231">tukkarɨ</ta>
            <ta e="T233" id="Seg_5344" s="T232">huk</ta>
            <ta e="T234" id="Seg_5345" s="T233">köt-üː</ta>
            <ta e="T235" id="Seg_5346" s="T234">ajan</ta>
            <ta e="T236" id="Seg_5347" s="T235">ehe</ta>
            <ta e="T237" id="Seg_5348" s="T236">hu͡ol-a</ta>
            <ta e="T238" id="Seg_5349" s="T237">baran-ar</ta>
            <ta e="T239" id="Seg_5350" s="T238">da</ta>
            <ta e="T240" id="Seg_5351" s="T239">kɨːl</ta>
            <ta e="T241" id="Seg_5352" s="T240">atɨːr-a</ta>
            <ta e="T242" id="Seg_5353" s="T241">bu͡ol-an</ta>
            <ta e="T243" id="Seg_5354" s="T242">oj-on</ta>
            <ta e="T244" id="Seg_5355" s="T243">kaːl-aːktaː-bɨt</ta>
            <ta e="T245" id="Seg_5356" s="T244">hög-ö</ta>
            <ta e="T246" id="Seg_5357" s="T245">agaj</ta>
            <ta e="T247" id="Seg_5358" s="T246">ih-er</ta>
            <ta e="T248" id="Seg_5359" s="T247">bi͡ek</ta>
            <ta e="T249" id="Seg_5360" s="T248">bat-ar</ta>
            <ta e="T250" id="Seg_5361" s="T249">dogor-o</ta>
            <ta e="T251" id="Seg_5362" s="T250">Timir Haːpka</ta>
            <ta e="T252" id="Seg_5363" s="T251">kɨːl</ta>
            <ta e="T253" id="Seg_5364" s="T252">atɨːr-a</ta>
            <ta e="T254" id="Seg_5365" s="T253">bu͡ol-l-a</ta>
            <ta e="T255" id="Seg_5366" s="T254">ide-ŋ</ta>
            <ta e="T256" id="Seg_5367" s="T255">da</ta>
            <ta e="T257" id="Seg_5368" s="T256">ele-te</ta>
            <ta e="T258" id="Seg_5369" s="T257">ini</ta>
            <ta e="T259" id="Seg_5370" s="T258">hit-eː</ta>
            <ta e="T260" id="Seg_5371" s="T259">ini-bin</ta>
            <ta e="T261" id="Seg_5372" s="T260">hubu-kaːn</ta>
            <ta e="T262" id="Seg_5373" s="T261">d-iː</ta>
            <ta e="T263" id="Seg_5374" s="T262">hanɨː-r</ta>
            <ta e="T264" id="Seg_5375" s="T263">kör-büt-e</ta>
            <ta e="T265" id="Seg_5376" s="T264">biːr</ta>
            <ta e="T266" id="Seg_5377" s="T265">hir-ge</ta>
            <ta e="T267" id="Seg_5378" s="T266">dogor-o</ta>
            <ta e="T268" id="Seg_5379" s="T267">balčaj-an</ta>
            <ta e="T269" id="Seg_5380" s="T268">tur-ar</ta>
            <ta e="T270" id="Seg_5381" s="T269">čugahaː-n</ta>
            <ta e="T271" id="Seg_5382" s="T270">kör-büt-e</ta>
            <ta e="T272" id="Seg_5383" s="T271">ikki</ta>
            <ta e="T273" id="Seg_5384" s="T272">agaj</ta>
            <ta e="T274" id="Seg_5385" s="T273">atag-a</ta>
            <ta e="T275" id="Seg_5386" s="T274">čočoj-on</ta>
            <ta e="T276" id="Seg_5387" s="T275">kaːl-bɨt</ta>
            <ta e="T277" id="Seg_5388" s="T276">hühü͡ök-ter-i-nen</ta>
            <ta e="T278" id="Seg_5389" s="T277">bɨh-an</ta>
            <ta e="T279" id="Seg_5390" s="T278">tuːsa</ta>
            <ta e="T280" id="Seg_5391" s="T279">atak-tar-ɨ-n</ta>
            <ta e="T281" id="Seg_5392" s="T280">čekeli-t-en</ta>
            <ta e="T282" id="Seg_5393" s="T281">keːs-pit</ta>
            <ta e="T283" id="Seg_5394" s="T282">hol</ta>
            <ta e="T284" id="Seg_5395" s="T283">korduk-kaːn</ta>
            <ta e="T285" id="Seg_5396" s="T284">Timir Haːpka</ta>
            <ta e="T286" id="Seg_5397" s="T285">atag-ɨ-n</ta>
            <ta e="T287" id="Seg_5398" s="T286">bɨh-aːt</ta>
            <ta e="T288" id="Seg_5399" s="T287">tuːsa</ta>
            <ta e="T289" id="Seg_5400" s="T288">bu͡ol-an</ta>
            <ta e="T290" id="Seg_5401" s="T289">čekelij-en</ta>
            <ta e="T291" id="Seg_5402" s="T290">is-t-e</ta>
            <ta e="T292" id="Seg_5403" s="T291">töhö</ta>
            <ta e="T293" id="Seg_5404" s="T292">da</ta>
            <ta e="T294" id="Seg_5405" s="T293">bu͡ol-u-muja</ta>
            <ta e="T295" id="Seg_5406" s="T294">tuːsa</ta>
            <ta e="T296" id="Seg_5407" s="T295">köppöj-ö</ta>
            <ta e="T297" id="Seg_5408" s="T296">hɨt-ar</ta>
            <ta e="T298" id="Seg_5409" s="T297">ü͡ör-er</ta>
            <ta e="T299" id="Seg_5410" s="T298">hit-ti-m</ta>
            <ta e="T300" id="Seg_5411" s="T299">eː</ta>
            <ta e="T301" id="Seg_5412" s="T300">diː-r</ta>
            <ta e="T302" id="Seg_5413" s="T301">bu</ta>
            <ta e="T303" id="Seg_5414" s="T302">tuːsa-ga</ta>
            <ta e="T304" id="Seg_5415" s="T303">tiːj-bit-e</ta>
            <ta e="T305" id="Seg_5416" s="T304">tuːsa</ta>
            <ta e="T306" id="Seg_5417" s="T305">postu͡oj</ta>
            <ta e="T307" id="Seg_5418" s="T306">agaj</ta>
            <ta e="T308" id="Seg_5419" s="T307">hɨt-ar</ta>
            <ta e="T309" id="Seg_5420" s="T308">mejiː-te</ta>
            <ta e="T310" id="Seg_5421" s="T309">agaj</ta>
            <ta e="T311" id="Seg_5422" s="T310">čekelij-en</ta>
            <ta e="T312" id="Seg_5423" s="T311">kaːl-bɨt</ta>
            <ta e="T313" id="Seg_5424" s="T312">kaja</ta>
            <ta e="T314" id="Seg_5425" s="T313">doː</ta>
            <ta e="T315" id="Seg_5426" s="T314">bu</ta>
            <ta e="T316" id="Seg_5427" s="T315">togo</ta>
            <ta e="T317" id="Seg_5428" s="T316">kerd-i-n-er</ta>
            <ta e="T318" id="Seg_5429" s="T317">beje-ti-n</ta>
            <ta e="T319" id="Seg_5430" s="T318">di͡e-n</ta>
            <ta e="T320" id="Seg_5431" s="T319">hanɨː-r</ta>
            <ta e="T321" id="Seg_5432" s="T320">bugurduk-kaːn</ta>
            <ta e="T322" id="Seg_5433" s="T321">tu͡ok</ta>
            <ta e="T323" id="Seg_5434" s="T322">mökkü͡ön-e=j</ta>
            <ta e="T324" id="Seg_5435" s="T323">di͡e-n</ta>
            <ta e="T325" id="Seg_5436" s="T324">oduːrg-uː</ta>
            <ta e="T326" id="Seg_5437" s="T325">is-t-e</ta>
            <ta e="T327" id="Seg_5438" s="T326">mejiː</ta>
            <ta e="T328" id="Seg_5439" s="T327">bu͡ol-an</ta>
            <ta e="T329" id="Seg_5440" s="T328">čekelij-en</ta>
            <ta e="T330" id="Seg_5441" s="T329">is-t-e</ta>
            <ta e="T331" id="Seg_5442" s="T330">araj</ta>
            <ta e="T332" id="Seg_5443" s="T331">mejiː</ta>
            <ta e="T333" id="Seg_5444" s="T332">čankaj-a</ta>
            <ta e="T334" id="Seg_5445" s="T333">hɨt-ar</ta>
            <ta e="T335" id="Seg_5446" s="T334">dʼe</ta>
            <ta e="T336" id="Seg_5447" s="T335">hit-ti-m</ta>
            <ta e="T337" id="Seg_5448" s="T336">ini</ta>
            <ta e="T338" id="Seg_5449" s="T337">araj</ta>
            <ta e="T339" id="Seg_5450" s="T338">ikki</ta>
            <ta e="T340" id="Seg_5451" s="T339">karag-a</ta>
            <ta e="T341" id="Seg_5452" s="T340">uhul-l-an</ta>
            <ta e="T342" id="Seg_5453" s="T341">bar-bɨt</ta>
            <ta e="T343" id="Seg_5454" s="T342">oroː</ta>
            <ta e="T344" id="Seg_5455" s="T343">bu</ta>
            <ta e="T345" id="Seg_5456" s="T344">tu͡ok</ta>
            <ta e="T346" id="Seg_5457" s="T345">kihi-te=j</ta>
            <ta e="T347" id="Seg_5458" s="T346">ajɨː-nɨ</ta>
            <ta e="T348" id="Seg_5459" s="T347">bat-a-bɨn</ta>
            <ta e="T349" id="Seg_5460" s="T348">duː</ta>
            <ta e="T350" id="Seg_5461" s="T349">abaːhɨ-nɨ</ta>
            <ta e="T351" id="Seg_5462" s="T350">duː</ta>
            <ta e="T352" id="Seg_5463" s="T351">diː-r</ta>
            <ta e="T353" id="Seg_5464" s="T352">ikki</ta>
            <ta e="T354" id="Seg_5465" s="T353">karag-a</ta>
            <ta e="T355" id="Seg_5466" s="T354">čekerij-en</ta>
            <ta e="T356" id="Seg_5467" s="T355">is-t-e</ta>
            <ta e="T357" id="Seg_5468" s="T356">karak-tar</ta>
            <ta e="T358" id="Seg_5469" s="T357">hu͡ol-larɨ-nan</ta>
            <ta e="T359" id="Seg_5470" s="T358">kahan</ta>
            <ta e="T360" id="Seg_5471" s="T359">ere</ta>
            <ta e="T361" id="Seg_5472" s="T360">bu</ta>
            <ta e="T362" id="Seg_5473" s="T361">ikki</ta>
            <ta e="T363" id="Seg_5474" s="T362">karak</ta>
            <ta e="T364" id="Seg_5475" s="T363">hu͡ol-a</ta>
            <ta e="T365" id="Seg_5476" s="T364">biːr</ta>
            <ta e="T366" id="Seg_5477" s="T365">dʼi͡e-keːŋ-ŋe</ta>
            <ta e="T367" id="Seg_5478" s="T366">tij-er</ta>
            <ta e="T368" id="Seg_5479" s="T367">bu</ta>
            <ta e="T369" id="Seg_5480" s="T368">tij-en</ta>
            <ta e="T370" id="Seg_5481" s="T369">ikki</ta>
            <ta e="T371" id="Seg_5482" s="T370">karak</ta>
            <ta e="T372" id="Seg_5483" s="T371">hu͡ol-a</ta>
            <ta e="T373" id="Seg_5484" s="T372">dʼi͡e-ge</ta>
            <ta e="T374" id="Seg_5485" s="T373">kiːr-bit</ta>
            <ta e="T375" id="Seg_5486" s="T374">dʼe</ta>
            <ta e="T376" id="Seg_5487" s="T375">ele-ŋ</ta>
            <ta e="T377" id="Seg_5488" s="T376">ini</ta>
            <ta e="T378" id="Seg_5489" s="T377">ikki</ta>
            <ta e="T379" id="Seg_5490" s="T378">kelin</ta>
            <ta e="T380" id="Seg_5491" s="T379">karak</ta>
            <ta e="T381" id="Seg_5492" s="T380">dʼi͡e-ge</ta>
            <ta e="T382" id="Seg_5493" s="T381">kiːr-d-e</ta>
            <ta e="T383" id="Seg_5494" s="T382">kiːr-bit-e</ta>
            <ta e="T384" id="Seg_5495" s="T383">uŋa</ta>
            <ta e="T385" id="Seg_5496" s="T384">di͡ekki</ta>
            <ta e="T386" id="Seg_5497" s="T385">oroŋ-ŋo</ta>
            <ta e="T387" id="Seg_5498" s="T386">emeːksin</ta>
            <ta e="T388" id="Seg_5499" s="T387">olor-or</ta>
            <ta e="T389" id="Seg_5500" s="T388">karak-tara</ta>
            <ta e="T390" id="Seg_5501" s="T389">ketegeriːn</ta>
            <ta e="T391" id="Seg_5502" s="T390">čagɨlɨh-a</ta>
            <ta e="T392" id="Seg_5503" s="T391">hɨt-al-lar</ta>
            <ta e="T393" id="Seg_5504" s="T392">bili</ta>
            <ta e="T394" id="Seg_5505" s="T393">ikki</ta>
            <ta e="T395" id="Seg_5506" s="T394">karak</ta>
            <ta e="T396" id="Seg_5507" s="T395">kiːr-bit-i-ger</ta>
            <ta e="T397" id="Seg_5508" s="T396">inniki</ta>
            <ta e="T398" id="Seg_5509" s="T397">ikki</ta>
            <ta e="T399" id="Seg_5510" s="T398">karak</ta>
            <ta e="T400" id="Seg_5511" s="T399">hana-laːk</ta>
            <ta e="T401" id="Seg_5512" s="T400">o</ta>
            <ta e="T402" id="Seg_5513" s="T401">da</ta>
            <ta e="T403" id="Seg_5514" s="T402">kihi-ni</ta>
            <ta e="T404" id="Seg_5515" s="T403">bat-ar</ta>
            <ta e="T405" id="Seg_5516" s="T404">kihi</ta>
            <ta e="T406" id="Seg_5517" s="T405">hɨlaj-aːččɨ</ta>
            <ta e="T407" id="Seg_5518" s="T406">en</ta>
            <ta e="T408" id="Seg_5519" s="T407">hɨnnʼan</ta>
            <ta e="T409" id="Seg_5520" s="T408">min</ta>
            <ta e="T410" id="Seg_5521" s="T409">tuːsa-lar-bɨtɨ-n</ta>
            <ta e="T411" id="Seg_5522" s="T410">mejiː-ler-biti-n</ta>
            <ta e="T412" id="Seg_5523" s="T411">komuj-taː-n</ta>
            <ta e="T413" id="Seg_5524" s="T412">egel-i͡e-m</ta>
            <ta e="T414" id="Seg_5525" s="T413">diː-r</ta>
            <ta e="T415" id="Seg_5526" s="T414">dogor-o</ta>
            <ta e="T416" id="Seg_5527" s="T415">Timir Haːpka</ta>
            <ta e="T417" id="Seg_5528" s="T416">onu-ga</ta>
            <ta e="T418" id="Seg_5529" s="T417">bu</ta>
            <ta e="T419" id="Seg_5530" s="T418">ile</ta>
            <ta e="T420" id="Seg_5531" s="T419">öj-ü-nen</ta>
            <ta e="T421" id="Seg_5532" s="T420">hɨldʼ-ar</ta>
            <ta e="T422" id="Seg_5533" s="T421">kihi-gin</ta>
            <ta e="T423" id="Seg_5534" s="T422">duː</ta>
            <ta e="T424" id="Seg_5535" s="T423">hu͡ok</ta>
            <ta e="T425" id="Seg_5536" s="T424">duː</ta>
            <ta e="T426" id="Seg_5537" s="T425">bu</ta>
            <ta e="T427" id="Seg_5538" s="T426">bihigi</ta>
            <ta e="T428" id="Seg_5539" s="T427">kihi</ta>
            <ta e="T429" id="Seg_5540" s="T428">kɨ͡aj-an</ta>
            <ta e="T430" id="Seg_5541" s="T429">tönn-ü-bet</ta>
            <ta e="T431" id="Seg_5542" s="T430">hir-i-ger</ta>
            <ta e="T432" id="Seg_5543" s="T431">kel-li-bit</ta>
            <ta e="T433" id="Seg_5544" s="T432">dʼe</ta>
            <ta e="T434" id="Seg_5545" s="T433">anɨ</ta>
            <ta e="T435" id="Seg_5546" s="T434">en</ta>
            <ta e="T436" id="Seg_5547" s="T435">bat-aːr</ta>
            <ta e="T437" id="Seg_5548" s="T436">minʼigi-n</ta>
            <ta e="T438" id="Seg_5549" s="T437">hit-eːr</ta>
            <ta e="T439" id="Seg_5550" s="T438">min</ta>
            <ta e="T440" id="Seg_5551" s="T439">ide-bi-n</ta>
            <ta e="T441" id="Seg_5552" s="T440">bil-e-gi͡en</ta>
            <ta e="T442" id="Seg_5553" s="T441">bu</ta>
            <ta e="T443" id="Seg_5554" s="T442">Purgaː</ta>
            <ta e="T444" id="Seg_5555" s="T443">ičči-ti-ger</ta>
            <ta e="T445" id="Seg_5556" s="T444">kel-li-bit</ta>
            <ta e="T446" id="Seg_5557" s="T445">harsi͡erda</ta>
            <ta e="T447" id="Seg_5558" s="T446">erde-hit</ta>
            <ta e="T448" id="Seg_5559" s="T447">kihi</ta>
            <ta e="T449" id="Seg_5560" s="T448">tur-ar</ta>
            <ta e="T450" id="Seg_5561" s="T449">kem-i-ger</ta>
            <ta e="T451" id="Seg_5562" s="T450">tur-an</ta>
            <ta e="T452" id="Seg_5563" s="T451">taks-ɨ͡a-m</ta>
            <ta e="T453" id="Seg_5564" s="T452">manɨ-ga</ta>
            <ta e="T454" id="Seg_5565" s="T453">hüːs-tüː</ta>
            <ta e="T455" id="Seg_5566" s="T454">ölgöbüːn-neːk</ta>
            <ta e="T456" id="Seg_5567" s="T455">hetiː-leːk</ta>
            <ta e="T457" id="Seg_5568" s="T456">ikki</ta>
            <ta e="T458" id="Seg_5569" s="T457">kɨrgɨt-tar</ta>
            <ta e="T459" id="Seg_5570" s="T458">kel-i͡ek-tere</ta>
            <ta e="T460" id="Seg_5571" s="T459">manɨ-ga</ta>
            <ta e="T461" id="Seg_5572" s="T460">min</ta>
            <ta e="T462" id="Seg_5573" s="T461">taks-a</ta>
            <ta e="T463" id="Seg_5574" s="T462">köt-öm-mün</ta>
            <ta e="T464" id="Seg_5575" s="T463">bastakɨ</ta>
            <ta e="T465" id="Seg_5576" s="T464">kɨːh-ɨ</ta>
            <ta e="T466" id="Seg_5577" s="T465">nʼu͡oguhut-u-n</ta>
            <ta e="T467" id="Seg_5578" s="T466">u͡olug-u-ttan</ta>
            <ta e="T468" id="Seg_5579" s="T467">tut-u͡o-m</ta>
            <ta e="T469" id="Seg_5580" s="T468">da</ta>
            <ta e="T470" id="Seg_5581" s="T469">kötüt-ü͡ö-m</ta>
            <ta e="T471" id="Seg_5582" s="T470">bu</ta>
            <ta e="T472" id="Seg_5583" s="T471">hüːs</ta>
            <ta e="T473" id="Seg_5584" s="T472">hetiː-tten</ta>
            <ta e="T474" id="Seg_5585" s="T473">biːr-i-ger</ta>
            <ta e="T475" id="Seg_5586" s="T474">eme</ta>
            <ta e="T476" id="Seg_5587" s="T475">kat-a-n-nak-kɨna</ta>
            <ta e="T477" id="Seg_5588" s="T476">tij-s-i͡e-ŋ</ta>
            <ta e="T478" id="Seg_5589" s="T477">dojdu-ga-r</ta>
            <ta e="T479" id="Seg_5590" s="T478">otton</ta>
            <ta e="T480" id="Seg_5591" s="T479">kat-a-m-ma-tak-kɨna</ta>
            <ta e="T481" id="Seg_5592" s="T480">bu</ta>
            <ta e="T482" id="Seg_5593" s="T481">dojdu-ga</ta>
            <ta e="T483" id="Seg_5594" s="T482">öl-ü͡ö-ŋ</ta>
            <ta e="T484" id="Seg_5595" s="T483">diː-r</ta>
            <ta e="T485" id="Seg_5596" s="T484">dʼe</ta>
            <ta e="T486" id="Seg_5597" s="T485">dogor-o</ta>
            <ta e="T487" id="Seg_5598" s="T486">Heliː Kur</ta>
            <ta e="T488" id="Seg_5599" s="T487">tuːsa-nɨ</ta>
            <ta e="T489" id="Seg_5600" s="T488">atak-tar-ɨ</ta>
            <ta e="T490" id="Seg_5601" s="T489">mejiː-ler-i</ta>
            <ta e="T491" id="Seg_5602" s="T490">egel-l-e</ta>
            <ta e="T492" id="Seg_5603" s="T491">beje-lere</ta>
            <ta e="T493" id="Seg_5604" s="T492">beje-leri-nen</ta>
            <ta e="T494" id="Seg_5605" s="T493">kihi</ta>
            <ta e="T495" id="Seg_5606" s="T494">bu͡ol-attaː-n</ta>
            <ta e="T496" id="Seg_5607" s="T495">kaːl-lɨ-lar</ta>
            <ta e="T497" id="Seg_5608" s="T496">utuj-taː-n</ta>
            <ta e="T498" id="Seg_5609" s="T497">baran</ta>
            <ta e="T499" id="Seg_5610" s="T498">Heliː Kur</ta>
            <ta e="T500" id="Seg_5611" s="T499">uhukt-ar</ta>
            <ta e="T501" id="Seg_5612" s="T500">dogor-o</ta>
            <ta e="T502" id="Seg_5613" s="T501">taks-ɨ-bɨt</ta>
            <ta e="T503" id="Seg_5614" s="T502">tɨ͡ah-ɨ-gar</ta>
            <ta e="T504" id="Seg_5615" s="T503">Heliː Kur</ta>
            <ta e="T505" id="Seg_5616" s="T504">taks-a</ta>
            <ta e="T506" id="Seg_5617" s="T505">köp-püt-e</ta>
            <ta e="T507" id="Seg_5618" s="T506">hüːs</ta>
            <ta e="T508" id="Seg_5619" s="T507">hetiː</ta>
            <ta e="T509" id="Seg_5620" s="T508">eleːnn-e-n-en</ta>
            <ta e="T510" id="Seg_5621" s="T509">er-deg-ine</ta>
            <ta e="T511" id="Seg_5622" s="T510">bütehik</ta>
            <ta e="T512" id="Seg_5623" s="T511">hetiː-ge</ta>
            <ta e="T513" id="Seg_5624" s="T512">iːlihin-n-e</ta>
            <ta e="T514" id="Seg_5625" s="T513">kallaːn-ɨ-nan</ta>
            <ta e="T515" id="Seg_5626" s="T514">da</ta>
            <ta e="T516" id="Seg_5627" s="T515">hir-i-nen</ta>
            <ta e="T517" id="Seg_5628" s="T516">da</ta>
            <ta e="T518" id="Seg_5629" s="T517">bar-ar-ɨ-n</ta>
            <ta e="T519" id="Seg_5630" s="T518">dʼüːlleː-beteg-e</ta>
            <ta e="T520" id="Seg_5631" s="T519">kobu͡o</ta>
            <ta e="T521" id="Seg_5632" s="T520">kojut</ta>
            <ta e="T522" id="Seg_5633" s="T521">kebi͡e</ta>
            <ta e="T523" id="Seg_5634" s="T522">keneges</ta>
            <ta e="T524" id="Seg_5635" s="T523">hüːs</ta>
            <ta e="T525" id="Seg_5636" s="T524">hetiː</ta>
            <ta e="T526" id="Seg_5637" s="T525">toktoː-but-u-gar</ta>
            <ta e="T527" id="Seg_5638" s="T526">kör-büt-e</ta>
            <ta e="T528" id="Seg_5639" s="T527">dogor-o</ta>
            <ta e="T529" id="Seg_5640" s="T528">dojdu-tu-gar</ta>
            <ta e="T530" id="Seg_5641" s="T529">tiri͡er-bit</ta>
            <ta e="T531" id="Seg_5642" s="T530">dʼe</ta>
            <ta e="T532" id="Seg_5643" s="T531">Timir Haːpka</ta>
            <ta e="T533" id="Seg_5644" s="T532">diː-r</ta>
            <ta e="T534" id="Seg_5645" s="T533">dogor-u-n</ta>
            <ta e="T535" id="Seg_5646" s="T534">dʼe</ta>
            <ta e="T536" id="Seg_5647" s="T535">iti</ta>
            <ta e="T537" id="Seg_5648" s="T536">hüːs</ta>
            <ta e="T538" id="Seg_5649" s="T537">hetiː</ta>
            <ta e="T539" id="Seg_5650" s="T538">aŋar</ta>
            <ta e="T540" id="Seg_5651" s="T539">ojogoh-u-gar</ta>
            <ta e="T541" id="Seg_5652" s="T540">üs</ta>
            <ta e="T542" id="Seg_5653" s="T541">čeːlkeː</ta>
            <ta e="T543" id="Seg_5654" s="T542">atɨːr</ta>
            <ta e="T544" id="Seg_5655" s="T543">kölü-ll-en</ta>
            <ta e="T545" id="Seg_5656" s="T544">tur-ar</ta>
            <ta e="T546" id="Seg_5657" s="T545">aŋar</ta>
            <ta e="T547" id="Seg_5658" s="T546">ojogoh-u-gar</ta>
            <ta e="T548" id="Seg_5659" s="T547">üs</ta>
            <ta e="T549" id="Seg_5660" s="T548">kara</ta>
            <ta e="T550" id="Seg_5661" s="T549">atɨːr</ta>
            <ta e="T551" id="Seg_5662" s="T550">tur-ar</ta>
            <ta e="T552" id="Seg_5663" s="T551">dʼe</ta>
            <ta e="T553" id="Seg_5664" s="T552">min</ta>
            <ta e="T554" id="Seg_5665" s="T553">iti</ta>
            <ta e="T555" id="Seg_5666" s="T554">üs</ta>
            <ta e="T556" id="Seg_5667" s="T555">čeːlkeː</ta>
            <ta e="T557" id="Seg_5668" s="T556">atɨːr-ɨ</ta>
            <ta e="T558" id="Seg_5669" s="T557">ölör-ü͡ö-m</ta>
            <ta e="T559" id="Seg_5670" s="T558">tiriː-leri-n</ta>
            <ta e="T560" id="Seg_5671" s="T559">kast-ɨ͡a-m</ta>
            <ta e="T561" id="Seg_5672" s="T560">en</ta>
            <ta e="T562" id="Seg_5673" s="T561">üs</ta>
            <ta e="T563" id="Seg_5674" s="T562">kara-nɨ</ta>
            <ta e="T564" id="Seg_5675" s="T563">ölör-ü͡ö-ŋ</ta>
            <ta e="T565" id="Seg_5676" s="T564">kast-ɨ͡a-ŋ</ta>
            <ta e="T566" id="Seg_5677" s="T565">tiriː-leri-n</ta>
            <ta e="T567" id="Seg_5678" s="T566">bil-e-gi͡en</ta>
            <ta e="T568" id="Seg_5679" s="T567">bu</ta>
            <ta e="T569" id="Seg_5680" s="T568">Purgaː</ta>
            <ta e="T570" id="Seg_5681" s="T569">ičči-ti-n</ta>
            <ta e="T571" id="Seg_5682" s="T570">ɨlgɨn</ta>
            <ta e="T572" id="Seg_5683" s="T571">kɨːs-tar-ɨ-n</ta>
            <ta e="T573" id="Seg_5684" s="T572">egel-li-m</ta>
            <ta e="T574" id="Seg_5685" s="T573">albas-pɨ-nan</ta>
            <ta e="T575" id="Seg_5686" s="T574">anɨ</ta>
            <ta e="T576" id="Seg_5687" s="T575">itin-ti-leri-n</ta>
            <ta e="T577" id="Seg_5688" s="T576">bat-an</ta>
            <ta e="T578" id="Seg_5689" s="T577">üs</ta>
            <ta e="T579" id="Seg_5690" s="T578">ubaj-a</ta>
            <ta e="T580" id="Seg_5691" s="T579">Purgaː</ta>
            <ta e="T581" id="Seg_5692" s="T580">ičči-ler-e</ta>
            <ta e="T582" id="Seg_5693" s="T581">kel-i͡ek-tere</ta>
            <ta e="T583" id="Seg_5694" s="T582">ol</ta>
            <ta e="T584" id="Seg_5695" s="T583">kihi-ler</ta>
            <ta e="T585" id="Seg_5696" s="T584">kel-lek-terine</ta>
            <ta e="T586" id="Seg_5697" s="T585">ketegeriːn</ta>
            <ta e="T587" id="Seg_5698" s="T586">üs</ta>
            <ta e="T588" id="Seg_5699" s="T587">čeːlkeː</ta>
            <ta e="T589" id="Seg_5700" s="T588">tiriː-ni</ta>
            <ta e="T590" id="Seg_5701" s="T589">telg-i͡e-m</ta>
            <ta e="T591" id="Seg_5702" s="T590">en</ta>
            <ta e="T592" id="Seg_5703" s="T591">iti</ta>
            <ta e="T593" id="Seg_5704" s="T592">üs</ta>
            <ta e="T594" id="Seg_5705" s="T593">koŋnomu͡oj</ta>
            <ta e="T595" id="Seg_5706" s="T594">tiriː-gi-n</ta>
            <ta e="T596" id="Seg_5707" s="T595">telgi͡e-r</ta>
            <ta e="T597" id="Seg_5708" s="T596">dʼe</ta>
            <ta e="T598" id="Seg_5709" s="T597">ulakan</ta>
            <ta e="T599" id="Seg_5710" s="T598">čaːŋ-ŋa-r</ta>
            <ta e="T600" id="Seg_5711" s="T599">toloru</ta>
            <ta e="T601" id="Seg_5712" s="T600">uː-ta</ta>
            <ta e="T602" id="Seg_5713" s="T601">kɨːj-nar</ta>
            <ta e="T603" id="Seg_5714" s="T602">min</ta>
            <ta e="T604" id="Seg_5715" s="T603">emi͡e</ta>
            <ta e="T605" id="Seg_5716" s="T604">kɨːj-nar-ɨ͡a-m</ta>
            <ta e="T606" id="Seg_5717" s="T605">buka</ta>
            <ta e="T607" id="Seg_5718" s="T606">mannaj</ta>
            <ta e="T608" id="Seg_5719" s="T607">mi͡e-ke</ta>
            <ta e="T609" id="Seg_5720" s="T608">ɨ͡aldʼɨt-t-ɨ͡ak-tara</ta>
            <ta e="T610" id="Seg_5721" s="T609">miːgi-tten</ta>
            <ta e="T611" id="Seg_5722" s="T610">tot-on</ta>
            <ta e="T612" id="Seg_5723" s="T611">höbüleː-n</ta>
            <ta e="T613" id="Seg_5724" s="T612">tagɨs-tak-tarɨna</ta>
            <ta e="T614" id="Seg_5725" s="T613">eji͡e-ke</ta>
            <ta e="T615" id="Seg_5726" s="T614">ɨ͡al-lan-ɨ͡ak-tara</ta>
            <ta e="T616" id="Seg_5727" s="T615">min</ta>
            <ta e="T617" id="Seg_5728" s="T616">kiniler-ge</ta>
            <ta e="T618" id="Seg_5729" s="T617">tug-u</ta>
            <ta e="T619" id="Seg_5730" s="T618">eme</ta>
            <ta e="T620" id="Seg_5731" s="T619">köllör-ü͡ö-m</ta>
            <ta e="T621" id="Seg_5732" s="T620">kiːr-s-eːr</ta>
            <ta e="T622" id="Seg_5733" s="T621">min</ta>
            <ta e="T623" id="Seg_5734" s="T622">tug-u</ta>
            <ta e="T624" id="Seg_5735" s="T623">kör-dör-ö-bün</ta>
            <ta e="T625" id="Seg_5736" s="T624">da</ta>
            <ta e="T626" id="Seg_5737" s="T625">ütükt-en</ta>
            <ta e="T627" id="Seg_5738" s="T626">kiniler-ge</ta>
            <ta e="T628" id="Seg_5739" s="T627">kör-dör-öːr</ta>
            <ta e="T629" id="Seg_5740" s="T628">di͡e-t-e</ta>
            <ta e="T630" id="Seg_5741" s="T629">ü͡öret-t-e</ta>
            <ta e="T631" id="Seg_5742" s="T630">dogor-u-n</ta>
            <ta e="T632" id="Seg_5743" s="T631">töhö</ta>
            <ta e="T633" id="Seg_5744" s="T632">da</ta>
            <ta e="T634" id="Seg_5745" s="T633">bu͡ol-u-muja</ta>
            <ta e="T635" id="Seg_5746" s="T634">Purgaː</ta>
            <ta e="T636" id="Seg_5747" s="T635">ičči-ler-e</ta>
            <ta e="T637" id="Seg_5748" s="T636">boloho-nnon</ta>
            <ta e="T638" id="Seg_5749" s="T637">kel-li-ler</ta>
            <ta e="T639" id="Seg_5750" s="T638">kam</ta>
            <ta e="T640" id="Seg_5751" s="T639">muːs</ta>
            <ta e="T641" id="Seg_5752" s="T640">dʼon-nor</ta>
            <ta e="T642" id="Seg_5753" s="T641">kiːr-en-ner</ta>
            <ta e="T643" id="Seg_5754" s="T642">ötörü-h-ötörü</ta>
            <ta e="T644" id="Seg_5755" s="T643">doroːbo</ta>
            <ta e="T645" id="Seg_5756" s="T644">da</ta>
            <ta e="T646" id="Seg_5757" s="T645">di͡e-bet-ter</ta>
            <ta e="T647" id="Seg_5758" s="T646">Timir Haːpka</ta>
            <ta e="T648" id="Seg_5759" s="T647">dʼi͡e-ti-ger</ta>
            <ta e="T649" id="Seg_5760" s="T648">üs</ta>
            <ta e="T650" id="Seg_5761" s="T649">čeːlkeː</ta>
            <ta e="T651" id="Seg_5762" s="T650">tiriː-ge</ta>
            <ta e="T652" id="Seg_5763" s="T651">olor-ol-lor</ta>
            <ta e="T653" id="Seg_5764" s="T652">Timir Haːpka</ta>
            <ta e="T654" id="Seg_5765" s="T653">ide-len-iːhi</ta>
            <ta e="T655" id="Seg_5766" s="T654">bu</ta>
            <ta e="T656" id="Seg_5767" s="T655">dʼoŋ-ŋo</ta>
            <ta e="T657" id="Seg_5768" s="T656">kɨːjn-a</ta>
            <ta e="T658" id="Seg_5769" s="T657">tur-ar</ta>
            <ta e="T659" id="Seg_5770" s="T658">ulakan</ta>
            <ta e="T660" id="Seg_5771" s="T659">čaːŋ-ŋa</ta>
            <ta e="T661" id="Seg_5772" s="T660">heliː</ta>
            <ta e="T662" id="Seg_5773" s="T661">munnu-laːk</ta>
            <ta e="T663" id="Seg_5774" s="T662">tallan ku͡ogas</ta>
            <ta e="T664" id="Seg_5775" s="T663">bu͡ol-aːt</ta>
            <ta e="T665" id="Seg_5776" s="T664">Aː uː</ta>
            <ta e="T666" id="Seg_5777" s="T665">di͡e-t</ta>
            <ta e="T667" id="Seg_5778" s="T666">ums-an</ta>
            <ta e="T668" id="Seg_5779" s="T667">kaːl-ar</ta>
            <ta e="T669" id="Seg_5780" s="T668">dajd-a-daj-bɨt-ɨ-nan</ta>
            <ta e="T670" id="Seg_5781" s="T669">köt-ön</ta>
            <ta e="T671" id="Seg_5782" s="T670">taks-an</ta>
            <ta e="T672" id="Seg_5783" s="T671">ije-ti-n</ta>
            <ta e="T673" id="Seg_5784" s="T672">bɨ͡ar-ɨ-nan</ta>
            <ta e="T674" id="Seg_5785" s="T673">helip</ta>
            <ta e="T675" id="Seg_5786" s="T674">gɨn-ar</ta>
            <ta e="T676" id="Seg_5787" s="T675">onton</ta>
            <ta e="T677" id="Seg_5788" s="T676">ije-ti-n</ta>
            <ta e="T678" id="Seg_5789" s="T677">ajag-ɨ-n</ta>
            <ta e="T679" id="Seg_5790" s="T678">ustun</ta>
            <ta e="T680" id="Seg_5791" s="T679">taks-a</ta>
            <ta e="T681" id="Seg_5792" s="T680">köp-püt</ta>
            <ta e="T682" id="Seg_5793" s="T681">dʼe</ta>
            <ta e="T683" id="Seg_5794" s="T682">onton</ta>
            <ta e="T684" id="Seg_5795" s="T683">aga-tɨ-n</ta>
            <ta e="T685" id="Seg_5796" s="T684">bɨ͡ar-ɨ-nan</ta>
            <ta e="T686" id="Seg_5797" s="T685">helip</ta>
            <ta e="T687" id="Seg_5798" s="T686">gɨm-mɨt</ta>
            <ta e="T688" id="Seg_5799" s="T687">aga-tɨ-n</ta>
            <ta e="T689" id="Seg_5800" s="T688">ajag-ɨ-nan</ta>
            <ta e="T690" id="Seg_5801" s="T689">hanardɨː</ta>
            <ta e="T691" id="Seg_5802" s="T690">mu͡oh-u-n</ta>
            <ta e="T692" id="Seg_5803" s="T691">hullaː-bɨt</ta>
            <ta e="T693" id="Seg_5804" s="T692">kɨːl</ta>
            <ta e="T694" id="Seg_5805" s="T693">atɨːr-a</ta>
            <ta e="T695" id="Seg_5806" s="T694">bu͡ol-an</ta>
            <ta e="T696" id="Seg_5807" s="T695">taks-ar</ta>
            <ta e="T697" id="Seg_5808" s="T696">bu</ta>
            <ta e="T698" id="Seg_5809" s="T697">kenni-tten</ta>
            <ta e="T699" id="Seg_5810" s="T698">Timir Haːpka</ta>
            <ta e="T700" id="Seg_5811" s="T699">kihi</ta>
            <ta e="T701" id="Seg_5812" s="T700">bu͡ol-ar</ta>
            <ta e="T702" id="Seg_5813" s="T701">da</ta>
            <ta e="T703" id="Seg_5814" s="T702">alanaː-nnan</ta>
            <ta e="T704" id="Seg_5815" s="T703">bu</ta>
            <ta e="T705" id="Seg_5816" s="T704">kɨːl</ta>
            <ta e="T706" id="Seg_5817" s="T705">atɨːr-ɨ-n</ta>
            <ta e="T707" id="Seg_5818" s="T706">huptu</ta>
            <ta e="T708" id="Seg_5819" s="T707">ɨt-an</ta>
            <ta e="T709" id="Seg_5820" s="T708">tüher-er</ta>
            <ta e="T710" id="Seg_5821" s="T709">ölör-ör</ta>
            <ta e="T711" id="Seg_5822" s="T710">da</ta>
            <ta e="T712" id="Seg_5823" s="T711">manɨ-ga</ta>
            <ta e="T713" id="Seg_5824" s="T712">kast-ɨ-r-ɨ-t-a</ta>
            <ta e="T714" id="Seg_5825" s="T713">hül-en</ta>
            <ta e="T715" id="Seg_5826" s="T714">mas</ta>
            <ta e="T716" id="Seg_5827" s="T715">atɨjak-ka</ta>
            <ta e="T717" id="Seg_5828" s="T716">hiːkej-d-iː-buh-uː-l-uː</ta>
            <ta e="T718" id="Seg_5829" s="T717">tard-an</ta>
            <ta e="T719" id="Seg_5830" s="T718">keːh-er</ta>
            <ta e="T720" id="Seg_5831" s="T719">ɨ͡aldʼɨt-tar-ga</ta>
            <ta e="T721" id="Seg_5832" s="T720">hogudaːj-d-a-t-an</ta>
            <ta e="T722" id="Seg_5833" s="T721">ɨ͡aldʼɨt-tara</ta>
            <ta e="T723" id="Seg_5834" s="T722">haŋa-iŋe</ta>
            <ta e="T724" id="Seg_5835" s="T723">hu͡ok</ta>
            <ta e="T725" id="Seg_5836" s="T724">ahɨː-r-ga</ta>
            <ta e="T726" id="Seg_5837" s="T725">bar-bɨt-tar</ta>
            <ta e="T727" id="Seg_5838" s="T726">kühüŋŋü</ta>
            <ta e="T728" id="Seg_5839" s="T727">atɨːr-tan</ta>
            <ta e="T729" id="Seg_5840" s="T728">amattan</ta>
            <ta e="T730" id="Seg_5841" s="T729">mu͡os-taːk</ta>
            <ta e="T731" id="Seg_5842" s="T730">tujag-ɨ-n</ta>
            <ta e="T732" id="Seg_5843" s="T731">or-dor-on</ta>
            <ta e="T733" id="Seg_5844" s="T732">kebis-ti-ler</ta>
            <ta e="T734" id="Seg_5845" s="T733">Heliː Kur</ta>
            <ta e="T735" id="Seg_5846" s="T734">kör-ön</ta>
            <ta e="T736" id="Seg_5847" s="T735">olor-u-n-aːktaː-t-a</ta>
            <ta e="T737" id="Seg_5848" s="T736">ahaː-n</ta>
            <ta e="T738" id="Seg_5849" s="T737">büt-el-ler</ta>
            <ta e="T739" id="Seg_5850" s="T738">da</ta>
            <ta e="T740" id="Seg_5851" s="T739">u͡os-tarɨ-n</ta>
            <ta e="T741" id="Seg_5852" s="T740">hot-toː-n</ta>
            <ta e="T742" id="Seg_5853" s="T741">taks-al-lar</ta>
            <ta e="T743" id="Seg_5854" s="T742">Heliː Kur</ta>
            <ta e="T744" id="Seg_5855" s="T743">dʼi͡e-ti-ger</ta>
            <ta e="T745" id="Seg_5856" s="T744">ɨstan-ar</ta>
            <ta e="T746" id="Seg_5857" s="T745">ɨ͡aldʼɨt-tar-ɨ</ta>
            <ta e="T747" id="Seg_5858" s="T746">tohuj-u-n-a</ta>
            <ta e="T748" id="Seg_5859" s="T747">ɨ͡aldʼɨt-tara</ta>
            <ta e="T749" id="Seg_5860" s="T748">kiːr-en</ta>
            <ta e="T750" id="Seg_5861" s="T749">üs</ta>
            <ta e="T751" id="Seg_5862" s="T750">koŋnomu͡oj</ta>
            <ta e="T752" id="Seg_5863" s="T751">tiriː-ge</ta>
            <ta e="T753" id="Seg_5864" s="T752">olor-o</ta>
            <ta e="T754" id="Seg_5865" s="T753">bi͡er-el-ler</ta>
            <ta e="T755" id="Seg_5866" s="T754">Heliː Kur</ta>
            <ta e="T756" id="Seg_5867" s="T755">A uːk</ta>
            <ta e="T757" id="Seg_5868" s="T756">di͡e-t</ta>
            <ta e="T758" id="Seg_5869" s="T757">bɨːtta ku͡ogas-kaːn</ta>
            <ta e="T759" id="Seg_5870" s="T758">bu͡ol-aːt</ta>
            <ta e="T760" id="Seg_5871" s="T759">čaːŋ-ŋa</ta>
            <ta e="T761" id="Seg_5872" s="T760">ums-ar</ta>
            <ta e="T762" id="Seg_5873" s="T761">ije-ti-n</ta>
            <ta e="T763" id="Seg_5874" s="T762">bɨ͡ar-ɨ-gar</ta>
            <ta e="T764" id="Seg_5875" s="T763">helip</ta>
            <ta e="T765" id="Seg_5876" s="T764">gɨn-ar</ta>
            <ta e="T766" id="Seg_5877" s="T765">ajag-ɨ-n</ta>
            <ta e="T767" id="Seg_5878" s="T766">ustun</ta>
            <ta e="T768" id="Seg_5879" s="T767">taks-ar</ta>
            <ta e="T769" id="Seg_5880" s="T768">aga-tɨ-n</ta>
            <ta e="T770" id="Seg_5881" s="T769">bɨ͡ar-ɨ-gar</ta>
            <ta e="T771" id="Seg_5882" s="T770">helip</ta>
            <ta e="T772" id="Seg_5883" s="T771">gɨn-ar</ta>
            <ta e="T773" id="Seg_5884" s="T772">aga-tɨ-n</ta>
            <ta e="T774" id="Seg_5885" s="T773">ajag-ɨ-n</ta>
            <ta e="T775" id="Seg_5886" s="T774">ustun</ta>
            <ta e="T776" id="Seg_5887" s="T775">tugu-ttan</ta>
            <ta e="T777" id="Seg_5888" s="T776">ere</ta>
            <ta e="T778" id="Seg_5889" s="T777">kuhagan</ta>
            <ta e="T779" id="Seg_5890" s="T778">tulaːjak-iː</ta>
            <ta e="T780" id="Seg_5891" s="T779">tuguk-kaːn</ta>
            <ta e="T781" id="Seg_5892" s="T780">bu͡ol-an</ta>
            <ta e="T782" id="Seg_5893" s="T781">taks-a</ta>
            <ta e="T783" id="Seg_5894" s="T782">köp-püt</ta>
            <ta e="T784" id="Seg_5895" s="T783">kenn-i-tten</ta>
            <ta e="T785" id="Seg_5896" s="T784">haː-laːk</ta>
            <ta e="T786" id="Seg_5897" s="T785">kihi</ta>
            <ta e="T787" id="Seg_5898" s="T786">taks-ar</ta>
            <ta e="T788" id="Seg_5899" s="T787">da</ta>
            <ta e="T789" id="Seg_5900" s="T788">ɨ͡aldʼɨt-tar</ta>
            <ta e="T790" id="Seg_5901" s="T789">inni-leri-ger</ta>
            <ta e="T791" id="Seg_5902" s="T790">epseri</ta>
            <ta e="T792" id="Seg_5903" s="T791">ɨt-ar</ta>
            <ta e="T793" id="Seg_5904" s="T792">as-taː-n</ta>
            <ta e="T794" id="Seg_5905" s="T793">kebih-er</ta>
            <ta e="T795" id="Seg_5906" s="T794">ɨ͡aldʼɨt-tar-ga</ta>
            <ta e="T796" id="Seg_5907" s="T795">tard-an</ta>
            <ta e="T797" id="Seg_5908" s="T796">keːh-er</ta>
            <ta e="T798" id="Seg_5909" s="T797">tard-an</ta>
            <ta e="T799" id="Seg_5910" s="T798">agaj</ta>
            <ta e="T800" id="Seg_5911" s="T799">keːs-pit-e</ta>
            <ta e="T801" id="Seg_5912" s="T800">dʼi͡e-te-u͡ot-a</ta>
            <ta e="T802" id="Seg_5913" s="T801">kanna</ta>
            <ta e="T803" id="Seg_5914" s="T802">da</ta>
            <ta e="T804" id="Seg_5915" s="T803">bar-bɨt-tarɨ-n</ta>
            <ta e="T805" id="Seg_5916" s="T804">bil-be-ti-ler</ta>
            <ta e="T806" id="Seg_5917" s="T805">purgaː</ta>
            <ta e="T807" id="Seg_5918" s="T806">ičči-ler-e</ta>
            <ta e="T808" id="Seg_5919" s="T807">öhürgen-en</ta>
            <ta e="T809" id="Seg_5920" s="T808">bu</ta>
            <ta e="T810" id="Seg_5921" s="T809">dʼi͡e-ni</ta>
            <ta e="T811" id="Seg_5922" s="T810">Heliː Kur-u</ta>
            <ta e="T812" id="Seg_5923" s="T811">ije-liːn-i-aga-lɨːn-ɨ</ta>
            <ta e="T813" id="Seg_5924" s="T812">kötüt-en</ta>
            <ta e="T814" id="Seg_5925" s="T813">boloho-nnon</ta>
            <ta e="T815" id="Seg_5926" s="T814">is-pit-tere</ta>
            <ta e="T816" id="Seg_5927" s="T815">araj</ta>
            <ta e="T817" id="Seg_5928" s="T816">Timir Haːpka</ta>
            <ta e="T818" id="Seg_5929" s="T817">ije-liːn-aga-lɨːn</ta>
            <ta e="T819" id="Seg_5930" s="T818">kaːl-bɨt</ta>
            <ta e="T820" id="Seg_5931" s="T819">purgaː</ta>
            <ta e="T821" id="Seg_5932" s="T820">kɨːh-ɨ-n</ta>
            <ta e="T822" id="Seg_5933" s="T821">dʼaktar-dan-an</ta>
            <ta e="T823" id="Seg_5934" s="T822">baj-an-taj-an</ta>
            <ta e="T824" id="Seg_5935" s="T823">olor-but-a</ta>
            <ta e="T825" id="Seg_5936" s="T824">ühü</ta>
            <ta e="T826" id="Seg_5937" s="T825">ele-te</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_5938" s="T0">Timir Haːpka</ta>
            <ta e="T2" id="Seg_5939" s="T1">ol-GA</ta>
            <ta e="T3" id="Seg_5940" s="T2">Heliː Kur</ta>
            <ta e="T4" id="Seg_5941" s="T3">di͡e-An</ta>
            <ta e="T5" id="Seg_5942" s="T4">ikki</ta>
            <ta e="T6" id="Seg_5943" s="T5">u͡ol</ta>
            <ta e="T7" id="Seg_5944" s="T6">kihi-LAr</ta>
            <ta e="T8" id="Seg_5945" s="T7">olor-BIT-LAr</ta>
            <ta e="T9" id="Seg_5946" s="T8">ikki-IAn</ta>
            <ta e="T10" id="Seg_5947" s="T9">aga-LAːK-LAr</ta>
            <ta e="T11" id="Seg_5948" s="T10">inʼe-LAːK-LAr</ta>
            <ta e="T12" id="Seg_5949" s="T11">dʼe</ta>
            <ta e="T13" id="Seg_5950" s="T12">bu</ta>
            <ta e="T14" id="Seg_5951" s="T13">olor-An-LAr</ta>
            <ta e="T15" id="Seg_5952" s="T14">Heliː Kur-tA</ta>
            <ta e="T16" id="Seg_5953" s="T15">di͡e-Ar</ta>
            <ta e="T17" id="Seg_5954" s="T16">Timir Haːpka-tI-n</ta>
            <ta e="T18" id="Seg_5955" s="T17">dogor</ta>
            <ta e="T19" id="Seg_5956" s="T18">bu</ta>
            <ta e="T20" id="Seg_5957" s="T19">bihigi</ta>
            <ta e="T21" id="Seg_5958" s="T20">tu͡ok</ta>
            <ta e="T22" id="Seg_5959" s="T21">ide-LAːK</ta>
            <ta e="T23" id="Seg_5960" s="T22">bu͡ol-A-BIt</ta>
            <ta e="T24" id="Seg_5961" s="T23">togo</ta>
            <ta e="T25" id="Seg_5962" s="T24">ide</ta>
            <ta e="T26" id="Seg_5963" s="T25">ide-BItI-n</ta>
            <ta e="T27" id="Seg_5964" s="T26">bil-s-I-BAT-BIt</ta>
            <ta e="T28" id="Seg_5965" s="T27">mekti͡e</ta>
            <ta e="T29" id="Seg_5966" s="T28">bɨldʼaː-A-s-AːrI</ta>
            <ta e="T30" id="Seg_5967" s="T29">gɨn-Ar</ta>
            <ta e="T31" id="Seg_5968" s="T30">bɨhɨːlaːk</ta>
            <ta e="T32" id="Seg_5969" s="T31">dogor-tA</ta>
            <ta e="T33" id="Seg_5970" s="T32">eː</ta>
            <ta e="T34" id="Seg_5971" s="T33">dogor</ta>
            <ta e="T35" id="Seg_5972" s="T34">tu͡ok-BItI-n</ta>
            <ta e="T36" id="Seg_5973" s="T35">bɨldʼaː-A-s-An</ta>
            <ta e="T37" id="Seg_5974" s="T36">mekti͡e</ta>
            <ta e="T38" id="Seg_5975" s="T37">bɨldʼaː-A-s-IAK-BIt=Ij</ta>
            <ta e="T39" id="Seg_5976" s="T38">di͡e-An</ta>
            <ta e="T40" id="Seg_5977" s="T39">bu͡ol-I-n-BAT</ta>
            <ta e="T41" id="Seg_5978" s="T40">dogor-tA</ta>
            <ta e="T42" id="Seg_5979" s="T41">tehij-t-BAT</ta>
            <ta e="T43" id="Seg_5980" s="T42">togo</ta>
            <ta e="T44" id="Seg_5981" s="T43">dogor</ta>
            <ta e="T45" id="Seg_5982" s="T44">en</ta>
            <ta e="T46" id="Seg_5983" s="T45">ide-GI-n</ta>
            <ta e="T47" id="Seg_5984" s="T46">bil-IAK-BI-n</ta>
            <ta e="T48" id="Seg_5985" s="T47">bagar-A-BIn</ta>
            <ta e="T49" id="Seg_5986" s="T48">dogor-tA</ta>
            <ta e="T50" id="Seg_5987" s="T49">bu͡ol-I-n-BAT</ta>
            <ta e="T51" id="Seg_5988" s="T50">Heliː Kur</ta>
            <ta e="T52" id="Seg_5989" s="T51">min</ta>
            <ta e="T53" id="Seg_5990" s="T52">küreː-IAK-m</ta>
            <ta e="T54" id="Seg_5991" s="T53">en-ttAn</ta>
            <ta e="T55" id="Seg_5992" s="T54">bat</ta>
            <ta e="T56" id="Seg_5993" s="T55">min-n</ta>
            <ta e="T57" id="Seg_5994" s="T56">min</ta>
            <ta e="T58" id="Seg_5995" s="T57">kajdak</ta>
            <ta e="T59" id="Seg_5996" s="T58">bu͡ol-A-BIn</ta>
            <ta e="T60" id="Seg_5997" s="T59">da</ta>
            <ta e="T61" id="Seg_5998" s="T60">bugurduk-kAːN</ta>
            <ta e="T62" id="Seg_5999" s="T61">bu͡ol-An</ta>
            <ta e="T63" id="Seg_6000" s="T62">is</ta>
            <ta e="T64" id="Seg_6001" s="T63">di͡e-Ar</ta>
            <ta e="T65" id="Seg_6002" s="T64">dogor-tA</ta>
            <ta e="T66" id="Seg_6003" s="T65">bat-IAktI-BIn</ta>
            <ta e="T67" id="Seg_6004" s="T66">du͡o</ta>
            <ta e="T68" id="Seg_6005" s="T67">ajɨlaːgɨn</ta>
            <ta e="T69" id="Seg_6006" s="T68">harsi͡erda</ta>
            <ta e="T70" id="Seg_6007" s="T69">erde-ČIt</ta>
            <ta e="T71" id="Seg_6008" s="T70">kihi</ta>
            <ta e="T72" id="Seg_6009" s="T71">tur-Ar</ta>
            <ta e="T73" id="Seg_6010" s="T72">kem-tI-GAr</ta>
            <ta e="T74" id="Seg_6011" s="T73">bat-Aːr</ta>
            <ta e="T75" id="Seg_6012" s="T74">min-n</ta>
            <ta e="T76" id="Seg_6013" s="T75">di͡e-Ar</ta>
            <ta e="T77" id="Seg_6014" s="T76">Heliː Kur</ta>
            <ta e="T78" id="Seg_6015" s="T77">innʼe</ta>
            <ta e="T79" id="Seg_6016" s="T78">di͡e-A-s-An</ta>
            <ta e="T80" id="Seg_6017" s="T79">aragɨs-Ar-LAr</ta>
            <ta e="T81" id="Seg_6018" s="T80">dʼi͡e</ta>
            <ta e="T82" id="Seg_6019" s="T81">dʼi͡e-LArI-GAr</ta>
            <ta e="T83" id="Seg_6020" s="T82">Timir Haːpka</ta>
            <ta e="T84" id="Seg_6021" s="T83">tagɨs-An</ta>
            <ta e="T85" id="Seg_6022" s="T84">utuj-tA</ta>
            <ta e="T86" id="Seg_6023" s="T85">hɨt-Ar</ta>
            <ta e="T87" id="Seg_6024" s="T86">uhugun-An</ta>
            <ta e="T88" id="Seg_6025" s="T87">kel-Ar</ta>
            <ta e="T89" id="Seg_6026" s="T88">dogor-I-m</ta>
            <ta e="T90" id="Seg_6027" s="T89">baːr-tA</ta>
            <ta e="T91" id="Seg_6028" s="T90">dʼürü</ta>
            <ta e="T92" id="Seg_6029" s="T91">dogor-tI-GAr</ta>
            <ta e="T93" id="Seg_6030" s="T92">kel-BIT-tA</ta>
            <ta e="T94" id="Seg_6031" s="T93">Heliː Kur</ta>
            <ta e="T95" id="Seg_6032" s="T94">oron-tA</ta>
            <ta e="T96" id="Seg_6033" s="T95">čüŋküj-A</ta>
            <ta e="T97" id="Seg_6034" s="T96">hɨt-Ar</ta>
            <ta e="T98" id="Seg_6035" s="T97">bar-An</ta>
            <ta e="T99" id="Seg_6036" s="T98">kaːl-BIT</ta>
            <ta e="T100" id="Seg_6037" s="T99">Timir Haːpka</ta>
            <ta e="T101" id="Seg_6038" s="T100">hu͡ol</ta>
            <ta e="T102" id="Seg_6039" s="T101">kaj-Ar</ta>
            <ta e="T103" id="Seg_6040" s="T102">hu͡ol-nI</ta>
            <ta e="T104" id="Seg_6041" s="T103">bul-BAT</ta>
            <ta e="T105" id="Seg_6042" s="T104">ele-tA</ta>
            <ta e="T106" id="Seg_6043" s="T105">bu͡ol-Ar</ta>
            <ta e="T107" id="Seg_6044" s="T106">kiːr-An</ta>
            <ta e="T108" id="Seg_6045" s="T107">oron-tI-n</ta>
            <ta e="T109" id="Seg_6046" s="T108">huptu-j-Ar</ta>
            <ta e="T110" id="Seg_6047" s="T109">Heliː Kur</ta>
            <ta e="T111" id="Seg_6048" s="T110">oron-tI-n</ta>
            <ta e="T112" id="Seg_6049" s="T111">alɨn-tI-ttAn</ta>
            <ta e="T113" id="Seg_6050" s="T112">üːteːn</ta>
            <ta e="T114" id="Seg_6051" s="T113">kutujak-tI-n</ta>
            <ta e="T115" id="Seg_6052" s="T114">koroːn-tA</ta>
            <ta e="T116" id="Seg_6053" s="T115">čüŋküj-A</ta>
            <ta e="T117" id="Seg_6054" s="T116">hɨt-Ar</ta>
            <ta e="T118" id="Seg_6055" s="T117">atɨn</ta>
            <ta e="T119" id="Seg_6056" s="T118">hu͡ol</ta>
            <ta e="T120" id="Seg_6057" s="T119">hu͡ok</ta>
            <ta e="T121" id="Seg_6058" s="T120">Timir Haːpka</ta>
            <ta e="T122" id="Seg_6059" s="T121">kutujak</ta>
            <ta e="T123" id="Seg_6060" s="T122">bu͡ol-TI-tA</ta>
            <ta e="T124" id="Seg_6061" s="T123">da</ta>
            <ta e="T125" id="Seg_6062" s="T124">koroːn-tI-nAn</ta>
            <ta e="T126" id="Seg_6063" s="T125">tüs-TI-tA</ta>
            <ta e="T127" id="Seg_6064" s="T126">ɨnaraː-GI-tA</ta>
            <ta e="T128" id="Seg_6065" s="T127">koroːn-ttAn</ta>
            <ta e="T129" id="Seg_6066" s="T128">tagɨs-AːT</ta>
            <ta e="T130" id="Seg_6067" s="T129">gornu͡ok</ta>
            <ta e="T131" id="Seg_6068" s="T130">bu͡ol-AːT</ta>
            <ta e="T132" id="Seg_6069" s="T131">ɨstan-An</ta>
            <ta e="T133" id="Seg_6070" s="T132">kaːl-Ar</ta>
            <ta e="T134" id="Seg_6071" s="T133">Timir Haːpka</ta>
            <ta e="T135" id="Seg_6072" s="T134">gornu͡ok</ta>
            <ta e="T136" id="Seg_6073" s="T135">bu͡ol-AːT</ta>
            <ta e="T137" id="Seg_6074" s="T136">bat-An</ta>
            <ta e="T138" id="Seg_6075" s="T137">is-Ar</ta>
            <ta e="T139" id="Seg_6076" s="T138">huk</ta>
            <ta e="T140" id="Seg_6077" s="T139">köt-Iː</ta>
            <ta e="T141" id="Seg_6078" s="T140">bar-An</ta>
            <ta e="T142" id="Seg_6079" s="T141">is-TI-tA</ta>
            <ta e="T143" id="Seg_6080" s="T142">bu</ta>
            <ta e="T144" id="Seg_6081" s="T143">gornu͡ok</ta>
            <ta e="T145" id="Seg_6082" s="T144">hu͡ol-tA</ta>
            <ta e="T146" id="Seg_6083" s="T145">büt-Ar</ta>
            <ta e="T147" id="Seg_6084" s="T146">da</ta>
            <ta e="T148" id="Seg_6085" s="T147">orto</ta>
            <ta e="T149" id="Seg_6086" s="T148">dojdu</ta>
            <ta e="T150" id="Seg_6087" s="T149">kɨrsa</ta>
            <ta e="T151" id="Seg_6088" s="T150">hu͡ol-tA</ta>
            <ta e="T152" id="Seg_6089" s="T151">bar-A</ta>
            <ta e="T153" id="Seg_6090" s="T152">tur-TI-tA</ta>
            <ta e="T154" id="Seg_6091" s="T153">dogor-tA</ta>
            <ta e="T155" id="Seg_6092" s="T154">halɨn-A</ta>
            <ta e="T156" id="Seg_6093" s="T155">is-TI-tA</ta>
            <ta e="T157" id="Seg_6094" s="T156">Timir Haːpka</ta>
            <ta e="T158" id="Seg_6095" s="T157">kɨrsa</ta>
            <ta e="T159" id="Seg_6096" s="T158">bu͡ol-Ar</ta>
            <ta e="T160" id="Seg_6097" s="T159">da</ta>
            <ta e="T161" id="Seg_6098" s="T160">oj-An</ta>
            <ta e="T162" id="Seg_6099" s="T161">is-TI-tA</ta>
            <ta e="T163" id="Seg_6100" s="T162">biːr</ta>
            <ta e="T164" id="Seg_6101" s="T163">hir-GA</ta>
            <ta e="T165" id="Seg_6102" s="T164">tüs-BIT</ta>
            <ta e="T166" id="Seg_6103" s="T165">da</ta>
            <ta e="T167" id="Seg_6104" s="T166">Heliː Kur-tA</ta>
            <ta e="T168" id="Seg_6105" s="T167">uskaːn</ta>
            <ta e="T169" id="Seg_6106" s="T168">bu͡ol-An</ta>
            <ta e="T170" id="Seg_6107" s="T169">oj-BIT</ta>
            <ta e="T171" id="Seg_6108" s="T170">kelin-GI-tA</ta>
            <ta e="T172" id="Seg_6109" s="T171">uskaːn</ta>
            <ta e="T173" id="Seg_6110" s="T172">bu͡ol-An</ta>
            <ta e="T174" id="Seg_6111" s="T173">bat-TI-tA</ta>
            <ta e="T175" id="Seg_6112" s="T174">Heliː Kur</ta>
            <ta e="T176" id="Seg_6113" s="T175">hahɨl</ta>
            <ta e="T177" id="Seg_6114" s="T176">bu͡ol-TI-tA</ta>
            <ta e="T178" id="Seg_6115" s="T177">onton</ta>
            <ta e="T179" id="Seg_6116" s="T178">kelin-GI-tA</ta>
            <ta e="T180" id="Seg_6117" s="T179">emi͡e</ta>
            <ta e="T181" id="Seg_6118" s="T180">hahɨl</ta>
            <ta e="T182" id="Seg_6119" s="T181">bu͡ol-An</ta>
            <ta e="T183" id="Seg_6120" s="T182">hu͡ol-tI-n</ta>
            <ta e="T184" id="Seg_6121" s="T183">agaj</ta>
            <ta e="T185" id="Seg_6122" s="T184">ütügün-An</ta>
            <ta e="T186" id="Seg_6123" s="T185">is-TI-tA</ta>
            <ta e="T187" id="Seg_6124" s="T186">bu-tA</ta>
            <ta e="T188" id="Seg_6125" s="T187">börö</ta>
            <ta e="T189" id="Seg_6126" s="T188">bu͡ol-An</ta>
            <ta e="T190" id="Seg_6127" s="T189">köt-An</ta>
            <ta e="T191" id="Seg_6128" s="T190">nʼuluj-BIT</ta>
            <ta e="T192" id="Seg_6129" s="T191">kelin-GI-tA</ta>
            <ta e="T193" id="Seg_6130" s="T192">hök-A</ta>
            <ta e="T194" id="Seg_6131" s="T193">is-TI-tA</ta>
            <ta e="T195" id="Seg_6132" s="T194">bu-tI-kAj-tA</ta>
            <ta e="T196" id="Seg_6133" s="T195">bat-I-mInA</ta>
            <ta e="T197" id="Seg_6134" s="T196">kördöː-A-n-BIT</ta>
            <ta e="T198" id="Seg_6135" s="T197">bu͡ollaga</ta>
            <ta e="T199" id="Seg_6136" s="T198">di͡e-Ar</ta>
            <ta e="T200" id="Seg_6137" s="T199">börö</ta>
            <ta e="T201" id="Seg_6138" s="T200">bu͡ol-An</ta>
            <ta e="T202" id="Seg_6139" s="T201">bat-An</ta>
            <ta e="T203" id="Seg_6140" s="T202">is-TI-tA</ta>
            <ta e="T204" id="Seg_6141" s="T203">töhö-nI-kačča-nI</ta>
            <ta e="T205" id="Seg_6142" s="T204">bar-BIT-tA</ta>
            <ta e="T206" id="Seg_6143" s="T205">bil-LIN-I-BAT</ta>
            <ta e="T207" id="Seg_6144" s="T206">Heliː Kur</ta>
            <ta e="T208" id="Seg_6145" s="T207">hi͡egen</ta>
            <ta e="T209" id="Seg_6146" s="T208">bu͡ol-An</ta>
            <ta e="T210" id="Seg_6147" s="T209">leppej-A</ta>
            <ta e="T211" id="Seg_6148" s="T210">tur-BIT</ta>
            <ta e="T212" id="Seg_6149" s="T211">kelin-GI-tA</ta>
            <ta e="T213" id="Seg_6150" s="T212">emi͡e</ta>
            <ta e="T214" id="Seg_6151" s="T213">bi͡ek</ta>
            <ta e="T215" id="Seg_6152" s="T214">hök-A-hök-A</ta>
            <ta e="T216" id="Seg_6153" s="T215">hi͡egen</ta>
            <ta e="T217" id="Seg_6154" s="T216">bu͡ol-An</ta>
            <ta e="T218" id="Seg_6155" s="T217">bat-An</ta>
            <ta e="T219" id="Seg_6156" s="T218">is-TI-tA</ta>
            <ta e="T220" id="Seg_6157" s="T219">hi͡egen-tI-n</ta>
            <ta e="T221" id="Seg_6158" s="T220">hu͡ol-tA</ta>
            <ta e="T222" id="Seg_6159" s="T221">büt-AːT-In</ta>
            <ta e="T223" id="Seg_6160" s="T222">ebekeː</ta>
            <ta e="T224" id="Seg_6161" s="T223">bu͡ol-An</ta>
            <ta e="T225" id="Seg_6162" s="T224">leppej-BIT</ta>
            <ta e="T226" id="Seg_6163" s="T225">ehe</ta>
            <ta e="T227" id="Seg_6164" s="T226">bu͡ol-An</ta>
            <ta e="T228" id="Seg_6165" s="T227">bat-An</ta>
            <ta e="T229" id="Seg_6166" s="T228">is-TI-tA</ta>
            <ta e="T230" id="Seg_6167" s="T229">Timir Haːpka</ta>
            <ta e="T231" id="Seg_6168" s="T230">bu</ta>
            <ta e="T232" id="Seg_6169" s="T231">turkarɨ</ta>
            <ta e="T233" id="Seg_6170" s="T232">huk</ta>
            <ta e="T234" id="Seg_6171" s="T233">köt-Iː</ta>
            <ta e="T235" id="Seg_6172" s="T234">ajan</ta>
            <ta e="T236" id="Seg_6173" s="T235">ehe</ta>
            <ta e="T237" id="Seg_6174" s="T236">hu͡ol-tA</ta>
            <ta e="T238" id="Seg_6175" s="T237">baran-Ar</ta>
            <ta e="T239" id="Seg_6176" s="T238">da</ta>
            <ta e="T240" id="Seg_6177" s="T239">kɨːl</ta>
            <ta e="T241" id="Seg_6178" s="T240">atɨːr-tA</ta>
            <ta e="T242" id="Seg_6179" s="T241">bu͡ol-An</ta>
            <ta e="T243" id="Seg_6180" s="T242">oj-An</ta>
            <ta e="T244" id="Seg_6181" s="T243">kaːl-AːktAː-BIT</ta>
            <ta e="T245" id="Seg_6182" s="T244">hök-A</ta>
            <ta e="T246" id="Seg_6183" s="T245">agaj</ta>
            <ta e="T247" id="Seg_6184" s="T246">is-Ar</ta>
            <ta e="T248" id="Seg_6185" s="T247">bi͡ek</ta>
            <ta e="T249" id="Seg_6186" s="T248">bat-Ar</ta>
            <ta e="T250" id="Seg_6187" s="T249">dogor-tA</ta>
            <ta e="T251" id="Seg_6188" s="T250">Timir Haːpka</ta>
            <ta e="T252" id="Seg_6189" s="T251">kɨːl</ta>
            <ta e="T253" id="Seg_6190" s="T252">atɨːr-tA</ta>
            <ta e="T254" id="Seg_6191" s="T253">bu͡ol-TI-tA</ta>
            <ta e="T255" id="Seg_6192" s="T254">ide-ŋ</ta>
            <ta e="T256" id="Seg_6193" s="T255">da</ta>
            <ta e="T257" id="Seg_6194" s="T256">ele-tA</ta>
            <ta e="T258" id="Seg_6195" s="T257">ini</ta>
            <ta e="T259" id="Seg_6196" s="T258">hit-A</ta>
            <ta e="T260" id="Seg_6197" s="T259">ini-BIn</ta>
            <ta e="T261" id="Seg_6198" s="T260">hubu-kAːN</ta>
            <ta e="T262" id="Seg_6199" s="T261">di͡e-A</ta>
            <ta e="T263" id="Seg_6200" s="T262">hanaː-Ar</ta>
            <ta e="T264" id="Seg_6201" s="T263">kör-BIT-tA</ta>
            <ta e="T265" id="Seg_6202" s="T264">biːr</ta>
            <ta e="T266" id="Seg_6203" s="T265">hir-GA</ta>
            <ta e="T267" id="Seg_6204" s="T266">dogor-tA</ta>
            <ta e="T268" id="Seg_6205" s="T267">balčaj-An</ta>
            <ta e="T269" id="Seg_6206" s="T268">tur-Ar</ta>
            <ta e="T270" id="Seg_6207" s="T269">čugahaː-An</ta>
            <ta e="T271" id="Seg_6208" s="T270">kör-BIT-tA</ta>
            <ta e="T272" id="Seg_6209" s="T271">ikki</ta>
            <ta e="T273" id="Seg_6210" s="T272">agaj</ta>
            <ta e="T274" id="Seg_6211" s="T273">atak-tA</ta>
            <ta e="T275" id="Seg_6212" s="T274">čočoj-An</ta>
            <ta e="T276" id="Seg_6213" s="T275">kaːl-BIT</ta>
            <ta e="T277" id="Seg_6214" s="T276">hühü͡ök-LAr-tI-nAn</ta>
            <ta e="T278" id="Seg_6215" s="T277">bɨs-An</ta>
            <ta e="T279" id="Seg_6216" s="T278">tuːsa</ta>
            <ta e="T280" id="Seg_6217" s="T279">atak-LAr-tI-n</ta>
            <ta e="T281" id="Seg_6218" s="T280">čekenij-t-An</ta>
            <ta e="T282" id="Seg_6219" s="T281">keːs-BIT</ta>
            <ta e="T283" id="Seg_6220" s="T282">hol</ta>
            <ta e="T284" id="Seg_6221" s="T283">kördük-kAːN</ta>
            <ta e="T285" id="Seg_6222" s="T284">Timir Haːpka</ta>
            <ta e="T286" id="Seg_6223" s="T285">atak-tI-n</ta>
            <ta e="T287" id="Seg_6224" s="T286">bɨs-AːT</ta>
            <ta e="T288" id="Seg_6225" s="T287">tuːsa</ta>
            <ta e="T289" id="Seg_6226" s="T288">bu͡ol-An</ta>
            <ta e="T290" id="Seg_6227" s="T289">čekenij-An</ta>
            <ta e="T291" id="Seg_6228" s="T290">is-TI-tA</ta>
            <ta e="T292" id="Seg_6229" s="T291">töhö</ta>
            <ta e="T293" id="Seg_6230" s="T292">da</ta>
            <ta e="T294" id="Seg_6231" s="T293">bu͡ol-I-mInA</ta>
            <ta e="T295" id="Seg_6232" s="T294">tuːsa</ta>
            <ta e="T296" id="Seg_6233" s="T295">köppöj-A</ta>
            <ta e="T297" id="Seg_6234" s="T296">hɨt-Ar</ta>
            <ta e="T298" id="Seg_6235" s="T297">ü͡ör-Ar</ta>
            <ta e="T299" id="Seg_6236" s="T298">hit-TI-m</ta>
            <ta e="T300" id="Seg_6237" s="T299">eː</ta>
            <ta e="T301" id="Seg_6238" s="T300">di͡e-Ar</ta>
            <ta e="T302" id="Seg_6239" s="T301">bu</ta>
            <ta e="T303" id="Seg_6240" s="T302">tuːsa-GA</ta>
            <ta e="T304" id="Seg_6241" s="T303">tij-BIT-tA</ta>
            <ta e="T305" id="Seg_6242" s="T304">tuːsa</ta>
            <ta e="T306" id="Seg_6243" s="T305">postu͡oj</ta>
            <ta e="T307" id="Seg_6244" s="T306">agaj</ta>
            <ta e="T308" id="Seg_6245" s="T307">hɨt-Ar</ta>
            <ta e="T309" id="Seg_6246" s="T308">menʼiː-tA</ta>
            <ta e="T310" id="Seg_6247" s="T309">agaj</ta>
            <ta e="T311" id="Seg_6248" s="T310">čekenij-An</ta>
            <ta e="T312" id="Seg_6249" s="T311">kaːl-BIT</ta>
            <ta e="T313" id="Seg_6250" s="T312">kaja</ta>
            <ta e="T314" id="Seg_6251" s="T313">doː</ta>
            <ta e="T315" id="Seg_6252" s="T314">bu</ta>
            <ta e="T316" id="Seg_6253" s="T315">togo</ta>
            <ta e="T317" id="Seg_6254" s="T316">kert-I-n-Ar</ta>
            <ta e="T318" id="Seg_6255" s="T317">beje-tI-n</ta>
            <ta e="T319" id="Seg_6256" s="T318">di͡e-An</ta>
            <ta e="T320" id="Seg_6257" s="T319">hanaː-Ar</ta>
            <ta e="T321" id="Seg_6258" s="T320">bugurduk-kAːN</ta>
            <ta e="T322" id="Seg_6259" s="T321">tu͡ok</ta>
            <ta e="T323" id="Seg_6260" s="T322">mökkü͡ön-tA=Ij</ta>
            <ta e="T324" id="Seg_6261" s="T323">di͡e-An</ta>
            <ta e="T325" id="Seg_6262" s="T324">oduːrgaː-A</ta>
            <ta e="T326" id="Seg_6263" s="T325">is-TI-tA</ta>
            <ta e="T327" id="Seg_6264" s="T326">menʼiː</ta>
            <ta e="T328" id="Seg_6265" s="T327">bu͡ol-An</ta>
            <ta e="T329" id="Seg_6266" s="T328">čekenij-An</ta>
            <ta e="T330" id="Seg_6267" s="T329">is-TI-tA</ta>
            <ta e="T331" id="Seg_6268" s="T330">agaj</ta>
            <ta e="T332" id="Seg_6269" s="T331">menʼiː</ta>
            <ta e="T333" id="Seg_6270" s="T332">čaŋkaj-A</ta>
            <ta e="T334" id="Seg_6271" s="T333">hɨt-Ar</ta>
            <ta e="T335" id="Seg_6272" s="T334">dʼe</ta>
            <ta e="T336" id="Seg_6273" s="T335">hit-TI-m</ta>
            <ta e="T337" id="Seg_6274" s="T336">ini</ta>
            <ta e="T338" id="Seg_6275" s="T337">agaj</ta>
            <ta e="T339" id="Seg_6276" s="T338">ikki</ta>
            <ta e="T340" id="Seg_6277" s="T339">karak-tA</ta>
            <ta e="T341" id="Seg_6278" s="T340">ugul-n-An</ta>
            <ta e="T342" id="Seg_6279" s="T341">bar-BIT</ta>
            <ta e="T343" id="Seg_6280" s="T342">araː</ta>
            <ta e="T344" id="Seg_6281" s="T343">bu</ta>
            <ta e="T345" id="Seg_6282" s="T344">tu͡ok</ta>
            <ta e="T346" id="Seg_6283" s="T345">kihi-tA=Ij</ta>
            <ta e="T347" id="Seg_6284" s="T346">ajɨː-nI</ta>
            <ta e="T348" id="Seg_6285" s="T347">bat-A-BIn</ta>
            <ta e="T349" id="Seg_6286" s="T348">du͡o</ta>
            <ta e="T350" id="Seg_6287" s="T349">abaːhɨ-nI</ta>
            <ta e="T351" id="Seg_6288" s="T350">du͡o</ta>
            <ta e="T352" id="Seg_6289" s="T351">di͡e-Ar</ta>
            <ta e="T353" id="Seg_6290" s="T352">ikki</ta>
            <ta e="T354" id="Seg_6291" s="T353">karak-tA</ta>
            <ta e="T355" id="Seg_6292" s="T354">čekenij-An</ta>
            <ta e="T356" id="Seg_6293" s="T355">is-TI-tA</ta>
            <ta e="T357" id="Seg_6294" s="T356">karak-LAr</ta>
            <ta e="T358" id="Seg_6295" s="T357">hu͡ol-LArI-nAn</ta>
            <ta e="T359" id="Seg_6296" s="T358">kahan</ta>
            <ta e="T360" id="Seg_6297" s="T359">ere</ta>
            <ta e="T361" id="Seg_6298" s="T360">bu</ta>
            <ta e="T362" id="Seg_6299" s="T361">ikki</ta>
            <ta e="T363" id="Seg_6300" s="T362">karak</ta>
            <ta e="T364" id="Seg_6301" s="T363">hu͡ol-tA</ta>
            <ta e="T365" id="Seg_6302" s="T364">biːr</ta>
            <ta e="T366" id="Seg_6303" s="T365">dʼi͡e-kAːN-GA</ta>
            <ta e="T367" id="Seg_6304" s="T366">tij-Ar</ta>
            <ta e="T368" id="Seg_6305" s="T367">bu</ta>
            <ta e="T369" id="Seg_6306" s="T368">tij-An</ta>
            <ta e="T370" id="Seg_6307" s="T369">ikki</ta>
            <ta e="T371" id="Seg_6308" s="T370">karak</ta>
            <ta e="T372" id="Seg_6309" s="T371">hu͡ol-tA</ta>
            <ta e="T373" id="Seg_6310" s="T372">dʼi͡e-GA</ta>
            <ta e="T374" id="Seg_6311" s="T373">kiːr-BIT</ta>
            <ta e="T375" id="Seg_6312" s="T374">dʼe</ta>
            <ta e="T376" id="Seg_6313" s="T375">ele-ŋ</ta>
            <ta e="T377" id="Seg_6314" s="T376">ini</ta>
            <ta e="T378" id="Seg_6315" s="T377">ikki</ta>
            <ta e="T379" id="Seg_6316" s="T378">kelin</ta>
            <ta e="T380" id="Seg_6317" s="T379">karak</ta>
            <ta e="T381" id="Seg_6318" s="T380">dʼi͡e-GA</ta>
            <ta e="T382" id="Seg_6319" s="T381">kiːr-TI-tA</ta>
            <ta e="T383" id="Seg_6320" s="T382">kiːr-BIT-tA</ta>
            <ta e="T384" id="Seg_6321" s="T383">uŋa</ta>
            <ta e="T385" id="Seg_6322" s="T384">di͡ekki</ta>
            <ta e="T386" id="Seg_6323" s="T385">oron-GA</ta>
            <ta e="T387" id="Seg_6324" s="T386">emeːksin</ta>
            <ta e="T388" id="Seg_6325" s="T387">olor-Ar</ta>
            <ta e="T389" id="Seg_6326" s="T388">karak-LArA</ta>
            <ta e="T390" id="Seg_6327" s="T389">ketegeriːn</ta>
            <ta e="T391" id="Seg_6328" s="T390">čagɨlɨs-A</ta>
            <ta e="T392" id="Seg_6329" s="T391">hɨt-Ar-LAr</ta>
            <ta e="T393" id="Seg_6330" s="T392">bili</ta>
            <ta e="T394" id="Seg_6331" s="T393">ikki</ta>
            <ta e="T395" id="Seg_6332" s="T394">karak</ta>
            <ta e="T396" id="Seg_6333" s="T395">kiːr-BIT-tI-GAr</ta>
            <ta e="T397" id="Seg_6334" s="T396">inniki</ta>
            <ta e="T398" id="Seg_6335" s="T397">ikki</ta>
            <ta e="T399" id="Seg_6336" s="T398">karak</ta>
            <ta e="T400" id="Seg_6337" s="T399">haŋa-LAːK</ta>
            <ta e="T401" id="Seg_6338" s="T400">o</ta>
            <ta e="T402" id="Seg_6339" s="T401">da</ta>
            <ta e="T403" id="Seg_6340" s="T402">kihi-nI</ta>
            <ta e="T404" id="Seg_6341" s="T403">bat-Ar</ta>
            <ta e="T405" id="Seg_6342" s="T404">kihi</ta>
            <ta e="T406" id="Seg_6343" s="T405">hɨlaj-AːččI</ta>
            <ta e="T407" id="Seg_6344" s="T406">en</ta>
            <ta e="T408" id="Seg_6345" s="T407">hɨnnʼan</ta>
            <ta e="T409" id="Seg_6346" s="T408">min</ta>
            <ta e="T410" id="Seg_6347" s="T409">tuːsa-LAr-BItI-n</ta>
            <ta e="T411" id="Seg_6348" s="T410">menʼiː-LAr-BItI-n</ta>
            <ta e="T412" id="Seg_6349" s="T411">komuj-TAː-An</ta>
            <ta e="T413" id="Seg_6350" s="T412">egel-IAK-m</ta>
            <ta e="T414" id="Seg_6351" s="T413">di͡e-Ar</ta>
            <ta e="T415" id="Seg_6352" s="T414">dogor-tA</ta>
            <ta e="T416" id="Seg_6353" s="T415">Timir Haːpka</ta>
            <ta e="T417" id="Seg_6354" s="T416">ol-GA</ta>
            <ta e="T418" id="Seg_6355" s="T417">bu</ta>
            <ta e="T419" id="Seg_6356" s="T418">ile</ta>
            <ta e="T420" id="Seg_6357" s="T419">öj-I-nAn</ta>
            <ta e="T421" id="Seg_6358" s="T420">hɨrɨt-Ar</ta>
            <ta e="T422" id="Seg_6359" s="T421">kihi-GIn</ta>
            <ta e="T423" id="Seg_6360" s="T422">du͡o</ta>
            <ta e="T424" id="Seg_6361" s="T423">hu͡ok</ta>
            <ta e="T425" id="Seg_6362" s="T424">du͡o</ta>
            <ta e="T426" id="Seg_6363" s="T425">bu</ta>
            <ta e="T427" id="Seg_6364" s="T426">bihigi</ta>
            <ta e="T428" id="Seg_6365" s="T427">kihi</ta>
            <ta e="T429" id="Seg_6366" s="T428">kɨ͡aj-An</ta>
            <ta e="T430" id="Seg_6367" s="T429">tönün-I-BAT</ta>
            <ta e="T431" id="Seg_6368" s="T430">hir-tI-GAr</ta>
            <ta e="T432" id="Seg_6369" s="T431">kel-TI-BIt</ta>
            <ta e="T433" id="Seg_6370" s="T432">dʼe</ta>
            <ta e="T434" id="Seg_6371" s="T433">anɨ</ta>
            <ta e="T435" id="Seg_6372" s="T434">en</ta>
            <ta e="T436" id="Seg_6373" s="T435">bat-Aːr</ta>
            <ta e="T437" id="Seg_6374" s="T436">min-n</ta>
            <ta e="T438" id="Seg_6375" s="T437">hit-Aːr</ta>
            <ta e="T439" id="Seg_6376" s="T438">min</ta>
            <ta e="T440" id="Seg_6377" s="T439">ide-BI-n</ta>
            <ta e="T441" id="Seg_6378" s="T440">bil-A-GIn</ta>
            <ta e="T442" id="Seg_6379" s="T441">bu</ta>
            <ta e="T443" id="Seg_6380" s="T442">purgaː</ta>
            <ta e="T444" id="Seg_6381" s="T443">ičči-tI-GAr</ta>
            <ta e="T445" id="Seg_6382" s="T444">kel-TI-BIt</ta>
            <ta e="T446" id="Seg_6383" s="T445">harsi͡erda</ta>
            <ta e="T447" id="Seg_6384" s="T446">erde-ČIt</ta>
            <ta e="T448" id="Seg_6385" s="T447">kihi</ta>
            <ta e="T449" id="Seg_6386" s="T448">tur-Ar</ta>
            <ta e="T450" id="Seg_6387" s="T449">kem-tI-GAr</ta>
            <ta e="T451" id="Seg_6388" s="T450">tur-An</ta>
            <ta e="T452" id="Seg_6389" s="T451">tagɨs-IAK-m</ta>
            <ta e="T453" id="Seg_6390" s="T452">bu-GA</ta>
            <ta e="T454" id="Seg_6391" s="T453">hüːs-LIː</ta>
            <ta e="T455" id="Seg_6392" s="T454">ölgöbüːn-LAːK</ta>
            <ta e="T456" id="Seg_6393" s="T455">hetiː-LAːK</ta>
            <ta e="T457" id="Seg_6394" s="T456">ikki</ta>
            <ta e="T458" id="Seg_6395" s="T457">kɨːs-LAr</ta>
            <ta e="T459" id="Seg_6396" s="T458">kel-IAK-LArA</ta>
            <ta e="T460" id="Seg_6397" s="T459">bu-GA</ta>
            <ta e="T461" id="Seg_6398" s="T460">min</ta>
            <ta e="T462" id="Seg_6399" s="T461">tagɨs-A</ta>
            <ta e="T463" id="Seg_6400" s="T462">köt-An-BIn</ta>
            <ta e="T464" id="Seg_6401" s="T463">bastakɨ</ta>
            <ta e="T465" id="Seg_6402" s="T464">kɨːs-nI</ta>
            <ta e="T466" id="Seg_6403" s="T465">nʼu͡oguhut-tI-n</ta>
            <ta e="T467" id="Seg_6404" s="T466">u͡oluk-tI-ttAn</ta>
            <ta e="T468" id="Seg_6405" s="T467">tut-IAK-m</ta>
            <ta e="T469" id="Seg_6406" s="T468">da</ta>
            <ta e="T470" id="Seg_6407" s="T469">kötüt-IAK-m</ta>
            <ta e="T471" id="Seg_6408" s="T470">bu</ta>
            <ta e="T472" id="Seg_6409" s="T471">hüːs</ta>
            <ta e="T473" id="Seg_6410" s="T472">hetiː-ttAn</ta>
            <ta e="T474" id="Seg_6411" s="T473">biːr-tI-GAr</ta>
            <ta e="T475" id="Seg_6412" s="T474">eme</ta>
            <ta e="T476" id="Seg_6413" s="T475">kataː-A-n-TAK-GInA</ta>
            <ta e="T477" id="Seg_6414" s="T476">tij-s-IAK-ŋ</ta>
            <ta e="T478" id="Seg_6415" s="T477">dojdu-GA-r</ta>
            <ta e="T479" id="Seg_6416" s="T478">onton</ta>
            <ta e="T480" id="Seg_6417" s="T479">kataː-A-n-BA-TAK-GInA</ta>
            <ta e="T481" id="Seg_6418" s="T480">bu</ta>
            <ta e="T482" id="Seg_6419" s="T481">dojdu-GA</ta>
            <ta e="T483" id="Seg_6420" s="T482">öl-IAK-ŋ</ta>
            <ta e="T484" id="Seg_6421" s="T483">di͡e-Ar</ta>
            <ta e="T485" id="Seg_6422" s="T484">dʼe</ta>
            <ta e="T486" id="Seg_6423" s="T485">dogor-tA</ta>
            <ta e="T487" id="Seg_6424" s="T486">Heliː Kur</ta>
            <ta e="T488" id="Seg_6425" s="T487">tuːsa-nI</ta>
            <ta e="T489" id="Seg_6426" s="T488">atak-LAr-nI</ta>
            <ta e="T490" id="Seg_6427" s="T489">menʼiː-LAr-nI</ta>
            <ta e="T491" id="Seg_6428" s="T490">egel-TI-tA</ta>
            <ta e="T492" id="Seg_6429" s="T491">beje-LArA</ta>
            <ta e="T493" id="Seg_6430" s="T492">beje-LArI-nAn</ta>
            <ta e="T494" id="Seg_6431" s="T493">kihi</ta>
            <ta e="T495" id="Seg_6432" s="T494">bu͡ol-AːktAː-An</ta>
            <ta e="T496" id="Seg_6433" s="T495">kaːl-TI-LAr</ta>
            <ta e="T497" id="Seg_6434" s="T496">utuj-TAː-An</ta>
            <ta e="T498" id="Seg_6435" s="T497">baran</ta>
            <ta e="T499" id="Seg_6436" s="T498">Heliː Kur</ta>
            <ta e="T500" id="Seg_6437" s="T499">uhugun-Ar</ta>
            <ta e="T501" id="Seg_6438" s="T500">dogor-tA</ta>
            <ta e="T502" id="Seg_6439" s="T501">tagɨs-I-BIT</ta>
            <ta e="T503" id="Seg_6440" s="T502">tɨ͡as-tI-GAr</ta>
            <ta e="T504" id="Seg_6441" s="T503">Heliː Kur</ta>
            <ta e="T505" id="Seg_6442" s="T504">tagɨs-A</ta>
            <ta e="T506" id="Seg_6443" s="T505">köt-BIT-tA</ta>
            <ta e="T507" id="Seg_6444" s="T506">hüːs</ta>
            <ta e="T508" id="Seg_6445" s="T507">hetiː</ta>
            <ta e="T509" id="Seg_6446" s="T508">eleːŋneː-A-n-An</ta>
            <ta e="T510" id="Seg_6447" s="T509">er-TAK-InA</ta>
            <ta e="T511" id="Seg_6448" s="T510">bütehik</ta>
            <ta e="T512" id="Seg_6449" s="T511">hetiː-GA</ta>
            <ta e="T513" id="Seg_6450" s="T512">iːlihin-TI-tA</ta>
            <ta e="T514" id="Seg_6451" s="T513">kallaːn-I-nAn</ta>
            <ta e="T515" id="Seg_6452" s="T514">da</ta>
            <ta e="T516" id="Seg_6453" s="T515">hir-I-nAn</ta>
            <ta e="T517" id="Seg_6454" s="T516">da</ta>
            <ta e="T518" id="Seg_6455" s="T517">bar-Ar-tI-n</ta>
            <ta e="T519" id="Seg_6456" s="T518">dʼüːlleː-BAtAK-tA</ta>
            <ta e="T520" id="Seg_6457" s="T519">kobu͡o</ta>
            <ta e="T521" id="Seg_6458" s="T520">kojut</ta>
            <ta e="T522" id="Seg_6459" s="T521">kobu͡o</ta>
            <ta e="T523" id="Seg_6460" s="T522">keneges</ta>
            <ta e="T524" id="Seg_6461" s="T523">hüːs</ta>
            <ta e="T525" id="Seg_6462" s="T524">hetiː</ta>
            <ta e="T526" id="Seg_6463" s="T525">toktoː-BIT-tI-GAr</ta>
            <ta e="T527" id="Seg_6464" s="T526">kör-BIT-tA</ta>
            <ta e="T528" id="Seg_6465" s="T527">dogor-tA</ta>
            <ta e="T529" id="Seg_6466" s="T528">dojdu-tI-GAr</ta>
            <ta e="T530" id="Seg_6467" s="T529">tiri͡er-BIT</ta>
            <ta e="T531" id="Seg_6468" s="T530">dʼe</ta>
            <ta e="T532" id="Seg_6469" s="T531">Timir Haːpka</ta>
            <ta e="T533" id="Seg_6470" s="T532">di͡e-Ar</ta>
            <ta e="T534" id="Seg_6471" s="T533">dogor-tI-n</ta>
            <ta e="T535" id="Seg_6472" s="T534">dʼe</ta>
            <ta e="T536" id="Seg_6473" s="T535">iti</ta>
            <ta e="T537" id="Seg_6474" s="T536">hüːs</ta>
            <ta e="T538" id="Seg_6475" s="T537">hetiː</ta>
            <ta e="T539" id="Seg_6476" s="T538">aŋar</ta>
            <ta e="T540" id="Seg_6477" s="T539">ojogos-tI-GAr</ta>
            <ta e="T541" id="Seg_6478" s="T540">üs</ta>
            <ta e="T542" id="Seg_6479" s="T541">čeːlkeː</ta>
            <ta e="T543" id="Seg_6480" s="T542">atɨːr</ta>
            <ta e="T544" id="Seg_6481" s="T543">kölüj-LIN-An</ta>
            <ta e="T545" id="Seg_6482" s="T544">tur-Ar</ta>
            <ta e="T546" id="Seg_6483" s="T545">aŋar</ta>
            <ta e="T547" id="Seg_6484" s="T546">ojogos-tI-GAr</ta>
            <ta e="T548" id="Seg_6485" s="T547">üs</ta>
            <ta e="T549" id="Seg_6486" s="T548">kara</ta>
            <ta e="T550" id="Seg_6487" s="T549">atɨːr</ta>
            <ta e="T551" id="Seg_6488" s="T550">tur-Ar</ta>
            <ta e="T552" id="Seg_6489" s="T551">dʼe</ta>
            <ta e="T553" id="Seg_6490" s="T552">min</ta>
            <ta e="T554" id="Seg_6491" s="T553">iti</ta>
            <ta e="T555" id="Seg_6492" s="T554">üs</ta>
            <ta e="T556" id="Seg_6493" s="T555">čeːlkeː</ta>
            <ta e="T557" id="Seg_6494" s="T556">atɨːr-nI</ta>
            <ta e="T558" id="Seg_6495" s="T557">ölör-IAK-m</ta>
            <ta e="T559" id="Seg_6496" s="T558">tiriː-LArI-n</ta>
            <ta e="T560" id="Seg_6497" s="T559">kastaː-IAK-m</ta>
            <ta e="T561" id="Seg_6498" s="T560">en</ta>
            <ta e="T562" id="Seg_6499" s="T561">üs</ta>
            <ta e="T563" id="Seg_6500" s="T562">kara-nI</ta>
            <ta e="T564" id="Seg_6501" s="T563">ölör-IAK-ŋ</ta>
            <ta e="T565" id="Seg_6502" s="T564">kastaː-IAK-ŋ</ta>
            <ta e="T566" id="Seg_6503" s="T565">tiriː-LArI-n</ta>
            <ta e="T567" id="Seg_6504" s="T566">bil-A-GIn</ta>
            <ta e="T568" id="Seg_6505" s="T567">bu</ta>
            <ta e="T569" id="Seg_6506" s="T568">purgaː</ta>
            <ta e="T570" id="Seg_6507" s="T569">ičči-tI-n</ta>
            <ta e="T571" id="Seg_6508" s="T570">ɨlgɨn</ta>
            <ta e="T572" id="Seg_6509" s="T571">kɨːs-LAr-tI-n</ta>
            <ta e="T573" id="Seg_6510" s="T572">egel-TI-m</ta>
            <ta e="T574" id="Seg_6511" s="T573">albas-BI-nAn</ta>
            <ta e="T575" id="Seg_6512" s="T574">anɨ</ta>
            <ta e="T576" id="Seg_6513" s="T575">iti-tI-LArI-n</ta>
            <ta e="T577" id="Seg_6514" s="T576">bat-An</ta>
            <ta e="T578" id="Seg_6515" s="T577">üs</ta>
            <ta e="T579" id="Seg_6516" s="T578">ubaj-tA</ta>
            <ta e="T580" id="Seg_6517" s="T579">purgaː</ta>
            <ta e="T581" id="Seg_6518" s="T580">ičči-LAr-tA</ta>
            <ta e="T582" id="Seg_6519" s="T581">kel-IAK-LArA</ta>
            <ta e="T583" id="Seg_6520" s="T582">ol</ta>
            <ta e="T584" id="Seg_6521" s="T583">kihi-LAr</ta>
            <ta e="T585" id="Seg_6522" s="T584">kel-TAK-TArInA</ta>
            <ta e="T586" id="Seg_6523" s="T585">ketegeriːn</ta>
            <ta e="T587" id="Seg_6524" s="T586">üs</ta>
            <ta e="T588" id="Seg_6525" s="T587">čeːlkeː</ta>
            <ta e="T589" id="Seg_6526" s="T588">tiriː-nI</ta>
            <ta e="T590" id="Seg_6527" s="T589">telgeː-IAK-m</ta>
            <ta e="T591" id="Seg_6528" s="T590">en</ta>
            <ta e="T592" id="Seg_6529" s="T591">iti</ta>
            <ta e="T593" id="Seg_6530" s="T592">üs</ta>
            <ta e="T594" id="Seg_6531" s="T593">koŋnomu͡oj</ta>
            <ta e="T595" id="Seg_6532" s="T594">tiriː-GI-n</ta>
            <ta e="T596" id="Seg_6533" s="T595">telgeː-Aːr</ta>
            <ta e="T597" id="Seg_6534" s="T596">dʼe</ta>
            <ta e="T598" id="Seg_6535" s="T597">ulakan</ta>
            <ta e="T599" id="Seg_6536" s="T598">čaːn-GA-r</ta>
            <ta e="T600" id="Seg_6537" s="T599">toloru</ta>
            <ta e="T601" id="Seg_6538" s="T600">uː-tA</ta>
            <ta e="T602" id="Seg_6539" s="T601">kɨːj-TAr</ta>
            <ta e="T603" id="Seg_6540" s="T602">min</ta>
            <ta e="T604" id="Seg_6541" s="T603">emi͡e</ta>
            <ta e="T605" id="Seg_6542" s="T604">kɨːj-TAr-IAK-m</ta>
            <ta e="T606" id="Seg_6543" s="T605">buka</ta>
            <ta e="T607" id="Seg_6544" s="T606">mannaj</ta>
            <ta e="T608" id="Seg_6545" s="T607">min-GA</ta>
            <ta e="T609" id="Seg_6546" s="T608">ɨ͡aldʼɨt-LAː-IAK-LArA</ta>
            <ta e="T610" id="Seg_6547" s="T609">min-ttAn</ta>
            <ta e="T611" id="Seg_6548" s="T610">tot-An</ta>
            <ta e="T612" id="Seg_6549" s="T611">höbüleː-An</ta>
            <ta e="T613" id="Seg_6550" s="T612">tagɨs-TAK-TArInA</ta>
            <ta e="T614" id="Seg_6551" s="T613">en-GA</ta>
            <ta e="T615" id="Seg_6552" s="T614">ɨ͡al-LAN-IAK-LArA</ta>
            <ta e="T616" id="Seg_6553" s="T615">min</ta>
            <ta e="T617" id="Seg_6554" s="T616">giniler-GA</ta>
            <ta e="T618" id="Seg_6555" s="T617">tu͡ok-nI</ta>
            <ta e="T619" id="Seg_6556" s="T618">eme</ta>
            <ta e="T620" id="Seg_6557" s="T619">köllör-IAK-m</ta>
            <ta e="T621" id="Seg_6558" s="T620">kiːr-s-Aːr</ta>
            <ta e="T622" id="Seg_6559" s="T621">min</ta>
            <ta e="T623" id="Seg_6560" s="T622">tu͡ok-nI</ta>
            <ta e="T624" id="Seg_6561" s="T623">kör-TAr-A-BIn</ta>
            <ta e="T625" id="Seg_6562" s="T624">da</ta>
            <ta e="T626" id="Seg_6563" s="T625">ütügün-An</ta>
            <ta e="T627" id="Seg_6564" s="T626">giniler-GA</ta>
            <ta e="T628" id="Seg_6565" s="T627">kör-TAr-Aːr</ta>
            <ta e="T629" id="Seg_6566" s="T628">di͡e-TI-tA</ta>
            <ta e="T630" id="Seg_6567" s="T629">ü͡öret-TI-tA</ta>
            <ta e="T631" id="Seg_6568" s="T630">dogor-tI-n</ta>
            <ta e="T632" id="Seg_6569" s="T631">töhö</ta>
            <ta e="T633" id="Seg_6570" s="T632">da</ta>
            <ta e="T634" id="Seg_6571" s="T633">bu͡ol-I-mInA</ta>
            <ta e="T635" id="Seg_6572" s="T634">purgaː</ta>
            <ta e="T636" id="Seg_6573" s="T635">ičči-LAr-tA</ta>
            <ta e="T637" id="Seg_6574" s="T636">boloho-nAn</ta>
            <ta e="T638" id="Seg_6575" s="T637">kel-TI-LAr</ta>
            <ta e="T639" id="Seg_6576" s="T638">kam</ta>
            <ta e="T640" id="Seg_6577" s="T639">buːs</ta>
            <ta e="T641" id="Seg_6578" s="T640">dʼon-LAr</ta>
            <ta e="T642" id="Seg_6579" s="T641">kiːr-An-LAr</ta>
            <ta e="T643" id="Seg_6580" s="T642">ötörü-h-ötörü</ta>
            <ta e="T644" id="Seg_6581" s="T643">doroːbo</ta>
            <ta e="T645" id="Seg_6582" s="T644">da</ta>
            <ta e="T646" id="Seg_6583" s="T645">di͡e-BAT-LAr</ta>
            <ta e="T647" id="Seg_6584" s="T646">Timir Haːpka</ta>
            <ta e="T648" id="Seg_6585" s="T647">dʼi͡e-tI-GAr</ta>
            <ta e="T649" id="Seg_6586" s="T648">üs</ta>
            <ta e="T650" id="Seg_6587" s="T649">čeːlkeː</ta>
            <ta e="T651" id="Seg_6588" s="T650">tiriː-GA</ta>
            <ta e="T652" id="Seg_6589" s="T651">olor-Ar-LAr</ta>
            <ta e="T653" id="Seg_6590" s="T652">Timir Haːpka</ta>
            <ta e="T654" id="Seg_6591" s="T653">ide-LAN-IːhI</ta>
            <ta e="T655" id="Seg_6592" s="T654">bu</ta>
            <ta e="T656" id="Seg_6593" s="T655">dʼon-GA</ta>
            <ta e="T657" id="Seg_6594" s="T656">kɨːj-A</ta>
            <ta e="T658" id="Seg_6595" s="T657">tur-Ar</ta>
            <ta e="T659" id="Seg_6596" s="T658">ulakan</ta>
            <ta e="T660" id="Seg_6597" s="T659">čaːn-GA</ta>
            <ta e="T661" id="Seg_6598" s="T660">heliː</ta>
            <ta e="T662" id="Seg_6599" s="T661">munnu-LAːK</ta>
            <ta e="T663" id="Seg_6600" s="T662">tallan ku͡ogas</ta>
            <ta e="T664" id="Seg_6601" s="T663">bu͡ol-AːT</ta>
            <ta e="T665" id="Seg_6602" s="T664">Aː uː</ta>
            <ta e="T666" id="Seg_6603" s="T665">di͡e-AːT</ta>
            <ta e="T667" id="Seg_6604" s="T666">umus-An</ta>
            <ta e="T668" id="Seg_6605" s="T667">kaːl-Ar</ta>
            <ta e="T669" id="Seg_6606" s="T668">daj-A-daj-BIT-I-nAn</ta>
            <ta e="T670" id="Seg_6607" s="T669">köt-An</ta>
            <ta e="T671" id="Seg_6608" s="T670">tagɨs-An</ta>
            <ta e="T672" id="Seg_6609" s="T671">inʼe-tI-n</ta>
            <ta e="T673" id="Seg_6610" s="T672">bɨ͡ar-tI-nAn</ta>
            <ta e="T674" id="Seg_6611" s="T673">helip</ta>
            <ta e="T675" id="Seg_6612" s="T674">gɨn-Ar</ta>
            <ta e="T676" id="Seg_6613" s="T675">onton</ta>
            <ta e="T677" id="Seg_6614" s="T676">inʼe-tI-n</ta>
            <ta e="T678" id="Seg_6615" s="T677">anʼak-tI-n</ta>
            <ta e="T679" id="Seg_6616" s="T678">üstün</ta>
            <ta e="T680" id="Seg_6617" s="T679">tagɨs-A</ta>
            <ta e="T681" id="Seg_6618" s="T680">köt-BIT</ta>
            <ta e="T682" id="Seg_6619" s="T681">dʼe</ta>
            <ta e="T683" id="Seg_6620" s="T682">onton</ta>
            <ta e="T684" id="Seg_6621" s="T683">aga-tI-n</ta>
            <ta e="T685" id="Seg_6622" s="T684">bɨ͡ar-tI-nAn</ta>
            <ta e="T686" id="Seg_6623" s="T685">helip</ta>
            <ta e="T687" id="Seg_6624" s="T686">gɨn-BIT</ta>
            <ta e="T688" id="Seg_6625" s="T687">aga-tI-n</ta>
            <ta e="T689" id="Seg_6626" s="T688">anʼak-tI-nAn</ta>
            <ta e="T690" id="Seg_6627" s="T689">haŋardɨː</ta>
            <ta e="T691" id="Seg_6628" s="T690">mu͡os-tI-n</ta>
            <ta e="T692" id="Seg_6629" s="T691">hullaː-BIT</ta>
            <ta e="T693" id="Seg_6630" s="T692">kɨːl</ta>
            <ta e="T694" id="Seg_6631" s="T693">atɨːr-tA</ta>
            <ta e="T695" id="Seg_6632" s="T694">bu͡ol-An</ta>
            <ta e="T696" id="Seg_6633" s="T695">tagɨs-Ar</ta>
            <ta e="T697" id="Seg_6634" s="T696">bu</ta>
            <ta e="T698" id="Seg_6635" s="T697">kelin-ttAn</ta>
            <ta e="T699" id="Seg_6636" s="T698">Timir Haːpka</ta>
            <ta e="T700" id="Seg_6637" s="T699">kihi</ta>
            <ta e="T701" id="Seg_6638" s="T700">bu͡ol-Ar</ta>
            <ta e="T702" id="Seg_6639" s="T701">da</ta>
            <ta e="T703" id="Seg_6640" s="T702">alaŋaː-nAn</ta>
            <ta e="T704" id="Seg_6641" s="T703">bu</ta>
            <ta e="T705" id="Seg_6642" s="T704">kɨːl</ta>
            <ta e="T706" id="Seg_6643" s="T705">atɨːr-tI-n</ta>
            <ta e="T707" id="Seg_6644" s="T706">huptu</ta>
            <ta e="T708" id="Seg_6645" s="T707">ɨt-An</ta>
            <ta e="T709" id="Seg_6646" s="T708">tüher-Ar</ta>
            <ta e="T710" id="Seg_6647" s="T709">ölör-Ar</ta>
            <ta e="T711" id="Seg_6648" s="T710">da</ta>
            <ta e="T712" id="Seg_6649" s="T711">bu-GA</ta>
            <ta e="T713" id="Seg_6650" s="T712">kastaː-I-r-I-t-A</ta>
            <ta e="T714" id="Seg_6651" s="T713">hül-An</ta>
            <ta e="T715" id="Seg_6652" s="T714">mas</ta>
            <ta e="T716" id="Seg_6653" s="T715">atɨjak-GA</ta>
            <ta e="T717" id="Seg_6654" s="T716">hiːkej-LAː-A-bɨs-Iː-LAː-A</ta>
            <ta e="T718" id="Seg_6655" s="T717">tart-An</ta>
            <ta e="T719" id="Seg_6656" s="T718">keːs-Ar</ta>
            <ta e="T720" id="Seg_6657" s="T719">ɨ͡aldʼɨt-LAr-GA</ta>
            <ta e="T721" id="Seg_6658" s="T720">hogudaːj-LAː-A-t-An</ta>
            <ta e="T722" id="Seg_6659" s="T721">ɨ͡aldʼɨt-LArA</ta>
            <ta e="T723" id="Seg_6660" s="T722">haŋa-haŋa</ta>
            <ta e="T724" id="Seg_6661" s="T723">hu͡ok</ta>
            <ta e="T725" id="Seg_6662" s="T724">ahaː-Ar-GA</ta>
            <ta e="T726" id="Seg_6663" s="T725">bar-BIT-LAr</ta>
            <ta e="T727" id="Seg_6664" s="T726">kühüŋŋü</ta>
            <ta e="T728" id="Seg_6665" s="T727">atɨːr-ttAn</ta>
            <ta e="T729" id="Seg_6666" s="T728">amattan</ta>
            <ta e="T730" id="Seg_6667" s="T729">mu͡os-LAːK</ta>
            <ta e="T731" id="Seg_6668" s="T730">tunʼak-tI-n</ta>
            <ta e="T732" id="Seg_6669" s="T731">ort-TAr-An</ta>
            <ta e="T733" id="Seg_6670" s="T732">keːs-TI-LAr</ta>
            <ta e="T734" id="Seg_6671" s="T733">Heliː Kur</ta>
            <ta e="T735" id="Seg_6672" s="T734">kör-An</ta>
            <ta e="T736" id="Seg_6673" s="T735">olor-I-n-AːktAː-TI-tA</ta>
            <ta e="T737" id="Seg_6674" s="T736">ahaː-An</ta>
            <ta e="T738" id="Seg_6675" s="T737">büt-Ar-LAr</ta>
            <ta e="T739" id="Seg_6676" s="T738">da</ta>
            <ta e="T740" id="Seg_6677" s="T739">u͡os-LArI-n</ta>
            <ta e="T741" id="Seg_6678" s="T740">hot-TAː-An</ta>
            <ta e="T742" id="Seg_6679" s="T741">tagɨs-Ar-LAr</ta>
            <ta e="T743" id="Seg_6680" s="T742">Heliː Kur</ta>
            <ta e="T744" id="Seg_6681" s="T743">dʼi͡e-tI-GAr</ta>
            <ta e="T745" id="Seg_6682" s="T744">ɨstan-Ar</ta>
            <ta e="T746" id="Seg_6683" s="T745">ɨ͡aldʼɨt-LAr-nI</ta>
            <ta e="T747" id="Seg_6684" s="T746">tohuj-I-n-A</ta>
            <ta e="T748" id="Seg_6685" s="T747">ɨ͡aldʼɨt-LArA</ta>
            <ta e="T749" id="Seg_6686" s="T748">kiːr-An</ta>
            <ta e="T750" id="Seg_6687" s="T749">üs</ta>
            <ta e="T751" id="Seg_6688" s="T750">koŋnomu͡oj</ta>
            <ta e="T752" id="Seg_6689" s="T751">tiriː-GA</ta>
            <ta e="T753" id="Seg_6690" s="T752">olor-A</ta>
            <ta e="T754" id="Seg_6691" s="T753">bi͡er-Ar-LAr</ta>
            <ta e="T755" id="Seg_6692" s="T754">Heliː Kur</ta>
            <ta e="T756" id="Seg_6693" s="T755">Aː uː</ta>
            <ta e="T757" id="Seg_6694" s="T756">di͡e-AːT</ta>
            <ta e="T758" id="Seg_6695" s="T757">bɨːtta ku͡ogas-kAːN</ta>
            <ta e="T759" id="Seg_6696" s="T758">bu͡ol-AːT</ta>
            <ta e="T760" id="Seg_6697" s="T759">čaːn-GA</ta>
            <ta e="T761" id="Seg_6698" s="T760">umus-Ar</ta>
            <ta e="T762" id="Seg_6699" s="T761">inʼe-tI-n</ta>
            <ta e="T763" id="Seg_6700" s="T762">bɨ͡ar-tI-GAr</ta>
            <ta e="T764" id="Seg_6701" s="T763">helip</ta>
            <ta e="T765" id="Seg_6702" s="T764">gɨn-Ar</ta>
            <ta e="T766" id="Seg_6703" s="T765">anʼak-tI-n</ta>
            <ta e="T767" id="Seg_6704" s="T766">üstün</ta>
            <ta e="T768" id="Seg_6705" s="T767">tagɨs-Ar</ta>
            <ta e="T769" id="Seg_6706" s="T768">aga-tI-n</ta>
            <ta e="T770" id="Seg_6707" s="T769">bɨ͡ar-tI-GAr</ta>
            <ta e="T771" id="Seg_6708" s="T770">helip</ta>
            <ta e="T772" id="Seg_6709" s="T771">gɨn-Ar</ta>
            <ta e="T773" id="Seg_6710" s="T772">aga-tI-n</ta>
            <ta e="T774" id="Seg_6711" s="T773">anʼak-tI-n</ta>
            <ta e="T775" id="Seg_6712" s="T774">üstün</ta>
            <ta e="T776" id="Seg_6713" s="T775">tu͡ok-ttAn</ta>
            <ta e="T777" id="Seg_6714" s="T776">ere</ta>
            <ta e="T778" id="Seg_6715" s="T777">kuhagan</ta>
            <ta e="T779" id="Seg_6716" s="T778">tulaːjak-Iː</ta>
            <ta e="T780" id="Seg_6717" s="T779">tugut-kAːN</ta>
            <ta e="T781" id="Seg_6718" s="T780">bu͡ol-An</ta>
            <ta e="T782" id="Seg_6719" s="T781">tagɨs-A</ta>
            <ta e="T783" id="Seg_6720" s="T782">köt-BIT</ta>
            <ta e="T784" id="Seg_6721" s="T783">kelin-tI-ttAn</ta>
            <ta e="T785" id="Seg_6722" s="T784">haː-LAːK</ta>
            <ta e="T786" id="Seg_6723" s="T785">kihi</ta>
            <ta e="T787" id="Seg_6724" s="T786">tagɨs-Ar</ta>
            <ta e="T788" id="Seg_6725" s="T787">da</ta>
            <ta e="T789" id="Seg_6726" s="T788">ɨ͡aldʼɨt-LAr</ta>
            <ta e="T790" id="Seg_6727" s="T789">ilin-LArI-GAr</ta>
            <ta e="T791" id="Seg_6728" s="T790">epseri</ta>
            <ta e="T792" id="Seg_6729" s="T791">ɨt-Ar</ta>
            <ta e="T793" id="Seg_6730" s="T792">as-LAː-An</ta>
            <ta e="T794" id="Seg_6731" s="T793">keːs-Ar</ta>
            <ta e="T795" id="Seg_6732" s="T794">ɨ͡aldʼɨt-LAr-GA</ta>
            <ta e="T796" id="Seg_6733" s="T795">tart-An</ta>
            <ta e="T797" id="Seg_6734" s="T796">keːs-Ar</ta>
            <ta e="T798" id="Seg_6735" s="T797">tart-An</ta>
            <ta e="T799" id="Seg_6736" s="T798">agaj</ta>
            <ta e="T800" id="Seg_6737" s="T799">keːs-BIT-tA</ta>
            <ta e="T801" id="Seg_6738" s="T800">dʼi͡e-tA-u͡ot-tA</ta>
            <ta e="T802" id="Seg_6739" s="T801">kanna</ta>
            <ta e="T803" id="Seg_6740" s="T802">da</ta>
            <ta e="T804" id="Seg_6741" s="T803">bar-BIT-LArI-n</ta>
            <ta e="T805" id="Seg_6742" s="T804">bil-BA-TI-LAr</ta>
            <ta e="T806" id="Seg_6743" s="T805">purgaː</ta>
            <ta e="T807" id="Seg_6744" s="T806">ičči-LAr-tA</ta>
            <ta e="T808" id="Seg_6745" s="T807">öhürgen-An</ta>
            <ta e="T809" id="Seg_6746" s="T808">bu</ta>
            <ta e="T810" id="Seg_6747" s="T809">dʼi͡e-nI</ta>
            <ta e="T811" id="Seg_6748" s="T810">Heliː Kur-nI</ta>
            <ta e="T812" id="Seg_6749" s="T811">inʼe-LIːN-nI-aga-LIːN-nI</ta>
            <ta e="T813" id="Seg_6750" s="T812">kötüt-An</ta>
            <ta e="T814" id="Seg_6751" s="T813">boloho-nAn</ta>
            <ta e="T815" id="Seg_6752" s="T814">is-BIT-LArA</ta>
            <ta e="T816" id="Seg_6753" s="T815">agaj</ta>
            <ta e="T817" id="Seg_6754" s="T816">Timir Haːpka</ta>
            <ta e="T818" id="Seg_6755" s="T817">inʼe-LIːN-aga-LIːN</ta>
            <ta e="T819" id="Seg_6756" s="T818">kaːl-BIT</ta>
            <ta e="T820" id="Seg_6757" s="T819">purgaː</ta>
            <ta e="T821" id="Seg_6758" s="T820">kɨːs-tI-n</ta>
            <ta e="T822" id="Seg_6759" s="T821">dʼaktar-LAN-An</ta>
            <ta e="T823" id="Seg_6760" s="T822">baːj-An-baːj-An</ta>
            <ta e="T824" id="Seg_6761" s="T823">olor-BIT-tA</ta>
            <ta e="T825" id="Seg_6762" s="T824">ühü</ta>
            <ta e="T826" id="Seg_6763" s="T825">ele-tA</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_6764" s="T0">Iron.Cap.[NOM]</ta>
            <ta e="T2" id="Seg_6765" s="T1">that-DAT/LOC</ta>
            <ta e="T3" id="Seg_6766" s="T2">Bony.Belt.[NOM]</ta>
            <ta e="T4" id="Seg_6767" s="T3">say-CVB.SEQ</ta>
            <ta e="T5" id="Seg_6768" s="T4">two</ta>
            <ta e="T6" id="Seg_6769" s="T5">boy.[NOM]</ta>
            <ta e="T7" id="Seg_6770" s="T6">human.being-PL.[NOM]</ta>
            <ta e="T8" id="Seg_6771" s="T7">live-PST2-3PL</ta>
            <ta e="T9" id="Seg_6772" s="T8">two-COLL.[NOM]</ta>
            <ta e="T10" id="Seg_6773" s="T9">father-PROPR-3PL</ta>
            <ta e="T11" id="Seg_6774" s="T10">mother-PROPR-3PL</ta>
            <ta e="T12" id="Seg_6775" s="T11">well</ta>
            <ta e="T13" id="Seg_6776" s="T12">this</ta>
            <ta e="T14" id="Seg_6777" s="T13">live-CVB.SEQ-3PL</ta>
            <ta e="T15" id="Seg_6778" s="T14">Bony.Belt-3SG.[NOM]</ta>
            <ta e="T16" id="Seg_6779" s="T15">say-PRS.[3SG]</ta>
            <ta e="T17" id="Seg_6780" s="T16">Iron.Cap-3SG-ACC</ta>
            <ta e="T18" id="Seg_6781" s="T17">friend</ta>
            <ta e="T19" id="Seg_6782" s="T18">this</ta>
            <ta e="T20" id="Seg_6783" s="T19">1PL.[NOM]</ta>
            <ta e="T21" id="Seg_6784" s="T20">what.[NOM]</ta>
            <ta e="T22" id="Seg_6785" s="T21">ability-PROPR.[NOM]</ta>
            <ta e="T23" id="Seg_6786" s="T22">be-PRS-1PL</ta>
            <ta e="T24" id="Seg_6787" s="T23">why</ta>
            <ta e="T25" id="Seg_6788" s="T24">ability.[NOM]</ta>
            <ta e="T26" id="Seg_6789" s="T25">ability-1PL-ACC</ta>
            <ta e="T27" id="Seg_6790" s="T26">get.to.know-RECP/COLL-EP-NEG-1PL</ta>
            <ta e="T28" id="Seg_6791" s="T27">surety.[NOM]</ta>
            <ta e="T29" id="Seg_6792" s="T28">take.away-EP-RECP/COLL-CVB.PURP</ta>
            <ta e="T30" id="Seg_6793" s="T29">want-PRS.[3SG]</ta>
            <ta e="T31" id="Seg_6794" s="T30">apparently</ta>
            <ta e="T32" id="Seg_6795" s="T31">friend-3SG.[NOM]</ta>
            <ta e="T33" id="Seg_6796" s="T32">hey</ta>
            <ta e="T34" id="Seg_6797" s="T33">friend.[NOM]</ta>
            <ta e="T35" id="Seg_6798" s="T34">what-1PL-ACC</ta>
            <ta e="T36" id="Seg_6799" s="T35">take.away-EP-RECP/COLL-CVB.SEQ</ta>
            <ta e="T37" id="Seg_6800" s="T36">surety.[NOM]</ta>
            <ta e="T38" id="Seg_6801" s="T37">take.away-EP-RECP/COLL-FUT-1PL=Q</ta>
            <ta e="T39" id="Seg_6802" s="T38">say-CVB.SEQ</ta>
            <ta e="T40" id="Seg_6803" s="T39">be-EP-MED-NEG.[3SG]</ta>
            <ta e="T41" id="Seg_6804" s="T40">friend-3SG.[NOM]</ta>
            <ta e="T42" id="Seg_6805" s="T41">bear-CAUS-NEG.[3SG]</ta>
            <ta e="T43" id="Seg_6806" s="T42">why</ta>
            <ta e="T44" id="Seg_6807" s="T43">friend.[NOM]</ta>
            <ta e="T45" id="Seg_6808" s="T44">2SG.[NOM]</ta>
            <ta e="T46" id="Seg_6809" s="T45">ability-2SG-ACC</ta>
            <ta e="T47" id="Seg_6810" s="T46">get.to.know-PTCP.FUT-1SG-ACC</ta>
            <ta e="T48" id="Seg_6811" s="T47">want-PRS-1SG</ta>
            <ta e="T49" id="Seg_6812" s="T48">friend-3SG.[NOM]</ta>
            <ta e="T50" id="Seg_6813" s="T49">be-EP-MED-NEG.[3SG]</ta>
            <ta e="T51" id="Seg_6814" s="T50">Bony.Belt.[NOM]</ta>
            <ta e="T52" id="Seg_6815" s="T51">1SG.[NOM]</ta>
            <ta e="T53" id="Seg_6816" s="T52">escape-FUT-1SG</ta>
            <ta e="T54" id="Seg_6817" s="T53">2SG-ABL</ta>
            <ta e="T55" id="Seg_6818" s="T54">follow.[IMP.2SG]</ta>
            <ta e="T56" id="Seg_6819" s="T55">1SG-ACC</ta>
            <ta e="T57" id="Seg_6820" s="T56">1SG.[NOM]</ta>
            <ta e="T58" id="Seg_6821" s="T57">how</ta>
            <ta e="T59" id="Seg_6822" s="T58">be-PRS-1SG</ta>
            <ta e="T60" id="Seg_6823" s="T59">and</ta>
            <ta e="T61" id="Seg_6824" s="T60">like.this-INTNS</ta>
            <ta e="T62" id="Seg_6825" s="T61">be-CVB.SEQ</ta>
            <ta e="T63" id="Seg_6826" s="T62">go.[IMP.2SG]</ta>
            <ta e="T64" id="Seg_6827" s="T63">say-PRS.[3SG]</ta>
            <ta e="T65" id="Seg_6828" s="T64">friend-3SG.[NOM]</ta>
            <ta e="T66" id="Seg_6829" s="T65">follow-FUT2-1SG</ta>
            <ta e="T67" id="Seg_6830" s="T66">Q</ta>
            <ta e="T68" id="Seg_6831" s="T67">better</ta>
            <ta e="T69" id="Seg_6832" s="T68">in.the.morning</ta>
            <ta e="T70" id="Seg_6833" s="T69">early-AG.[NOM]</ta>
            <ta e="T71" id="Seg_6834" s="T70">human.being.[NOM]</ta>
            <ta e="T72" id="Seg_6835" s="T71">stand.up-PTCP.PRS</ta>
            <ta e="T73" id="Seg_6836" s="T72">time-3SG-DAT/LOC</ta>
            <ta e="T74" id="Seg_6837" s="T73">follow-FUT.[IMP.2SG]</ta>
            <ta e="T75" id="Seg_6838" s="T74">1SG-ACC</ta>
            <ta e="T76" id="Seg_6839" s="T75">say-PRS.[3SG]</ta>
            <ta e="T77" id="Seg_6840" s="T76">Bony.Belt.[NOM]</ta>
            <ta e="T78" id="Seg_6841" s="T77">so</ta>
            <ta e="T79" id="Seg_6842" s="T78">say-EP-RECP/COLL-CVB.SEQ</ta>
            <ta e="T80" id="Seg_6843" s="T79">separate-PRS-3PL</ta>
            <ta e="T81" id="Seg_6844" s="T80">house.[NOM]</ta>
            <ta e="T82" id="Seg_6845" s="T81">house-3PL-DAT/LOC</ta>
            <ta e="T83" id="Seg_6846" s="T82">Iron.Cap.[NOM]</ta>
            <ta e="T84" id="Seg_6847" s="T83">go.out-CVB.SEQ</ta>
            <ta e="T85" id="Seg_6848" s="T84">sleep-CVB.SIM</ta>
            <ta e="T86" id="Seg_6849" s="T85">lie.down-PRS.[3SG]</ta>
            <ta e="T87" id="Seg_6850" s="T86">wake.up-CVB.SEQ</ta>
            <ta e="T88" id="Seg_6851" s="T87">come-PRS.[3SG]</ta>
            <ta e="T89" id="Seg_6852" s="T88">friend-EP-1SG.[NOM]</ta>
            <ta e="T90" id="Seg_6853" s="T89">there.is-3SG</ta>
            <ta e="T91" id="Seg_6854" s="T90">Q</ta>
            <ta e="T92" id="Seg_6855" s="T91">friend-3SG-DAT/LOC</ta>
            <ta e="T93" id="Seg_6856" s="T92">come-PST2-3SG</ta>
            <ta e="T94" id="Seg_6857" s="T93">Bony.Belt.[NOM]</ta>
            <ta e="T95" id="Seg_6858" s="T94">bed-3SG.[NOM]</ta>
            <ta e="T96" id="Seg_6859" s="T95">be.empty-CVB.SIM</ta>
            <ta e="T97" id="Seg_6860" s="T96">lie-PRS.[3SG]</ta>
            <ta e="T98" id="Seg_6861" s="T97">go-CVB.SEQ</ta>
            <ta e="T99" id="Seg_6862" s="T98">stay-PST2.[3SG]</ta>
            <ta e="T100" id="Seg_6863" s="T99">Iron.Cap.[NOM]</ta>
            <ta e="T101" id="Seg_6864" s="T100">trace.[NOM]</ta>
            <ta e="T102" id="Seg_6865" s="T101">follow-PRS.[3SG]</ta>
            <ta e="T103" id="Seg_6866" s="T102">trace-ACC</ta>
            <ta e="T104" id="Seg_6867" s="T103">find-NEG.[3SG]</ta>
            <ta e="T105" id="Seg_6868" s="T104">last-3SG.[NOM]</ta>
            <ta e="T106" id="Seg_6869" s="T105">be-PRS.[3SG]</ta>
            <ta e="T107" id="Seg_6870" s="T106">go.in-CVB.SEQ</ta>
            <ta e="T108" id="Seg_6871" s="T107">bed-3SG-ACC</ta>
            <ta e="T109" id="Seg_6872" s="T108">straight-VBZ-PRS.[3SG]</ta>
            <ta e="T110" id="Seg_6873" s="T109">Bony.Belt.[NOM]</ta>
            <ta e="T111" id="Seg_6874" s="T110">bed-3SG-GEN</ta>
            <ta e="T112" id="Seg_6875" s="T111">lower.part-3SG-ABL</ta>
            <ta e="T113" id="Seg_6876" s="T112">narrowheaded</ta>
            <ta e="T114" id="Seg_6877" s="T113">mouse-3SG-GEN</ta>
            <ta e="T115" id="Seg_6878" s="T114">burrow-3SG.[NOM]</ta>
            <ta e="T116" id="Seg_6879" s="T115">be.empty-CVB.SIM</ta>
            <ta e="T117" id="Seg_6880" s="T116">lie-PRS.[3SG]</ta>
            <ta e="T118" id="Seg_6881" s="T117">different</ta>
            <ta e="T119" id="Seg_6882" s="T118">trace.[NOM]</ta>
            <ta e="T120" id="Seg_6883" s="T119">NEG.EX</ta>
            <ta e="T121" id="Seg_6884" s="T120">Iron.Cap.[NOM]</ta>
            <ta e="T122" id="Seg_6885" s="T121">mouse.[NOM]</ta>
            <ta e="T123" id="Seg_6886" s="T122">become-PST1-3SG</ta>
            <ta e="T124" id="Seg_6887" s="T123">and</ta>
            <ta e="T125" id="Seg_6888" s="T124">burrow-3SG-INSTR</ta>
            <ta e="T126" id="Seg_6889" s="T125">go.down-PST1-3SG</ta>
            <ta e="T127" id="Seg_6890" s="T126">remote-ADJZ-3SG.[NOM]</ta>
            <ta e="T128" id="Seg_6891" s="T127">burrow-ABL</ta>
            <ta e="T129" id="Seg_6892" s="T128">go.out-CVB.ANT</ta>
            <ta e="T130" id="Seg_6893" s="T129">ermine.[NOM]</ta>
            <ta e="T131" id="Seg_6894" s="T130">become-CVB.ANT</ta>
            <ta e="T132" id="Seg_6895" s="T131">jump-CVB.SEQ</ta>
            <ta e="T133" id="Seg_6896" s="T132">stay-PRS.[3SG]</ta>
            <ta e="T134" id="Seg_6897" s="T133">Iron.Cap.[NOM]</ta>
            <ta e="T135" id="Seg_6898" s="T134">ermine.[NOM]</ta>
            <ta e="T136" id="Seg_6899" s="T135">be-CVB.ANT</ta>
            <ta e="T137" id="Seg_6900" s="T136">follow-CVB.SEQ</ta>
            <ta e="T138" id="Seg_6901" s="T137">go-PRS.[3SG]</ta>
            <ta e="T139" id="Seg_6902" s="T138">phew</ta>
            <ta e="T140" id="Seg_6903" s="T139">run-NMNZ.[NOM]</ta>
            <ta e="T141" id="Seg_6904" s="T140">go-CVB.SEQ</ta>
            <ta e="T142" id="Seg_6905" s="T141">go-PST1-3SG</ta>
            <ta e="T143" id="Seg_6906" s="T142">this</ta>
            <ta e="T144" id="Seg_6907" s="T143">ermine.[NOM]</ta>
            <ta e="T145" id="Seg_6908" s="T144">trace-3SG.[NOM]</ta>
            <ta e="T146" id="Seg_6909" s="T145">stop-PRS.[3SG]</ta>
            <ta e="T147" id="Seg_6910" s="T146">and</ta>
            <ta e="T148" id="Seg_6911" s="T147">middle</ta>
            <ta e="T149" id="Seg_6912" s="T148">world.[NOM]</ta>
            <ta e="T150" id="Seg_6913" s="T149">polar.fox.[NOM]</ta>
            <ta e="T151" id="Seg_6914" s="T150">trace-3SG.[NOM]</ta>
            <ta e="T152" id="Seg_6915" s="T151">go-CVB.SIM</ta>
            <ta e="T153" id="Seg_6916" s="T152">stand-PST1-3SG</ta>
            <ta e="T154" id="Seg_6917" s="T153">friend-3SG.[NOM]</ta>
            <ta e="T155" id="Seg_6918" s="T154">fear-CVB.SIM</ta>
            <ta e="T156" id="Seg_6919" s="T155">go-PST1-3SG</ta>
            <ta e="T157" id="Seg_6920" s="T156">Iron.Cap.[NOM]</ta>
            <ta e="T158" id="Seg_6921" s="T157">polar.fox.[NOM]</ta>
            <ta e="T159" id="Seg_6922" s="T158">become-PRS.[3SG]</ta>
            <ta e="T160" id="Seg_6923" s="T159">and</ta>
            <ta e="T161" id="Seg_6924" s="T160">jump-CVB.SEQ</ta>
            <ta e="T162" id="Seg_6925" s="T161">go-PST1-3SG</ta>
            <ta e="T163" id="Seg_6926" s="T162">one</ta>
            <ta e="T164" id="Seg_6927" s="T163">place-DAT/LOC</ta>
            <ta e="T165" id="Seg_6928" s="T164">fall-PST2.[3SG]</ta>
            <ta e="T166" id="Seg_6929" s="T165">and</ta>
            <ta e="T167" id="Seg_6930" s="T166">Bony.Belt-3SG.[NOM]</ta>
            <ta e="T168" id="Seg_6931" s="T167">hare.[NOM]</ta>
            <ta e="T169" id="Seg_6932" s="T168">become-CVB.SEQ</ta>
            <ta e="T170" id="Seg_6933" s="T169">jump-PST2.[3SG]</ta>
            <ta e="T171" id="Seg_6934" s="T170">back-ADJZ-3SG.[NOM]</ta>
            <ta e="T172" id="Seg_6935" s="T171">hare.[NOM]</ta>
            <ta e="T173" id="Seg_6936" s="T172">become-CVB.SEQ</ta>
            <ta e="T174" id="Seg_6937" s="T173">follow-PST1-3SG</ta>
            <ta e="T175" id="Seg_6938" s="T174">Bony.Belt.[NOM]</ta>
            <ta e="T176" id="Seg_6939" s="T175">fox.[NOM]</ta>
            <ta e="T177" id="Seg_6940" s="T176">become-PST1-3SG</ta>
            <ta e="T178" id="Seg_6941" s="T177">then</ta>
            <ta e="T179" id="Seg_6942" s="T178">back-ADJZ-3SG.[NOM]</ta>
            <ta e="T180" id="Seg_6943" s="T179">also</ta>
            <ta e="T181" id="Seg_6944" s="T180">fox.[NOM]</ta>
            <ta e="T182" id="Seg_6945" s="T181">become-CVB.SEQ</ta>
            <ta e="T183" id="Seg_6946" s="T182">way-3SG-ACC</ta>
            <ta e="T184" id="Seg_6947" s="T183">only</ta>
            <ta e="T185" id="Seg_6948" s="T184">imitate-CVB.SEQ</ta>
            <ta e="T186" id="Seg_6949" s="T185">go-PST1-3SG</ta>
            <ta e="T187" id="Seg_6950" s="T186">this-3SG.[NOM]</ta>
            <ta e="T188" id="Seg_6951" s="T187">wolf.[NOM]</ta>
            <ta e="T189" id="Seg_6952" s="T188">be-CVB.SEQ</ta>
            <ta e="T190" id="Seg_6953" s="T189">run-CVB.SEQ</ta>
            <ta e="T191" id="Seg_6954" s="T190">get.bigger-PST2.[3SG]</ta>
            <ta e="T192" id="Seg_6955" s="T191">back-ADJZ-3SG.[NOM]</ta>
            <ta e="T193" id="Seg_6956" s="T192">wonder-CVB.SIM</ta>
            <ta e="T194" id="Seg_6957" s="T193">go-PST1-3SG</ta>
            <ta e="T195" id="Seg_6958" s="T194">this-3SG-ADJZ-3SG.[NOM]</ta>
            <ta e="T196" id="Seg_6959" s="T195">get.along-EP-NEG.CVB</ta>
            <ta e="T197" id="Seg_6960" s="T196">beg-EP-MED-PST2.[3SG]</ta>
            <ta e="T198" id="Seg_6961" s="T197">MOD</ta>
            <ta e="T199" id="Seg_6962" s="T198">think-PRS.[3SG]</ta>
            <ta e="T200" id="Seg_6963" s="T199">wolf.[NOM]</ta>
            <ta e="T201" id="Seg_6964" s="T200">become-CVB.SEQ</ta>
            <ta e="T202" id="Seg_6965" s="T201">follow-CVB.SEQ</ta>
            <ta e="T203" id="Seg_6966" s="T202">go-PST1-3SG</ta>
            <ta e="T204" id="Seg_6967" s="T203">how.much-ACC-how.much-ACC</ta>
            <ta e="T205" id="Seg_6968" s="T204">go-PST2-3SG</ta>
            <ta e="T206" id="Seg_6969" s="T205">know-PASS/REFL-EP-NEG.[3SG]</ta>
            <ta e="T207" id="Seg_6970" s="T206">Bony.Belt.[NOM]</ta>
            <ta e="T208" id="Seg_6971" s="T207">wolverine.[NOM]</ta>
            <ta e="T209" id="Seg_6972" s="T208">become-CVB.SEQ</ta>
            <ta e="T210" id="Seg_6973" s="T209">trot-CVB.SIM</ta>
            <ta e="T211" id="Seg_6974" s="T210">stand-PST2.[3SG]</ta>
            <ta e="T212" id="Seg_6975" s="T211">back-ADJZ-3SG.[NOM]</ta>
            <ta e="T213" id="Seg_6976" s="T212">again</ta>
            <ta e="T214" id="Seg_6977" s="T213">always</ta>
            <ta e="T215" id="Seg_6978" s="T214">wonder-CVB.SIM-wonder-CVB.SIM</ta>
            <ta e="T216" id="Seg_6979" s="T215">wolverine.[NOM]</ta>
            <ta e="T217" id="Seg_6980" s="T216">become-CVB.SEQ</ta>
            <ta e="T218" id="Seg_6981" s="T217">follow-CVB.SEQ</ta>
            <ta e="T219" id="Seg_6982" s="T218">go-PST1-3SG</ta>
            <ta e="T220" id="Seg_6983" s="T219">wolverine-3SG-GEN</ta>
            <ta e="T221" id="Seg_6984" s="T220">trace-3SG.[NOM]</ta>
            <ta e="T222" id="Seg_6985" s="T221">stop-CVB.ANT-3SG</ta>
            <ta e="T223" id="Seg_6986" s="T222">bear.[NOM]</ta>
            <ta e="T224" id="Seg_6987" s="T223">become-CVB.SEQ</ta>
            <ta e="T225" id="Seg_6988" s="T224">trot-PST2.[3SG]</ta>
            <ta e="T226" id="Seg_6989" s="T225">bear.[NOM]</ta>
            <ta e="T227" id="Seg_6990" s="T226">become-CVB.SEQ</ta>
            <ta e="T228" id="Seg_6991" s="T227">follow-CVB.SEQ</ta>
            <ta e="T229" id="Seg_6992" s="T228">go-PST1-3SG</ta>
            <ta e="T230" id="Seg_6993" s="T229">Iron.Cap.[NOM]</ta>
            <ta e="T231" id="Seg_6994" s="T230">this.[NOM]</ta>
            <ta e="T232" id="Seg_6995" s="T231">as.long.as</ta>
            <ta e="T233" id="Seg_6996" s="T232">phew</ta>
            <ta e="T234" id="Seg_6997" s="T233">run-NMNZ.[NOM]</ta>
            <ta e="T235" id="Seg_6998" s="T234">way.[NOM]</ta>
            <ta e="T236" id="Seg_6999" s="T235">bear.[NOM]</ta>
            <ta e="T237" id="Seg_7000" s="T236">trace-3SG.[NOM]</ta>
            <ta e="T238" id="Seg_7001" s="T237">end-PRS.[3SG]</ta>
            <ta e="T239" id="Seg_7002" s="T238">and</ta>
            <ta e="T240" id="Seg_7003" s="T239">wild.reindeer</ta>
            <ta e="T241" id="Seg_7004" s="T240">reindeer.bull-3SG.[NOM]</ta>
            <ta e="T242" id="Seg_7005" s="T241">become-CVB.SEQ</ta>
            <ta e="T243" id="Seg_7006" s="T242">jump-CVB.SEQ</ta>
            <ta e="T244" id="Seg_7007" s="T243">stay-FREQ-PST2.[3SG]</ta>
            <ta e="T245" id="Seg_7008" s="T244">wonder-CVB.SIM</ta>
            <ta e="T246" id="Seg_7009" s="T245">only</ta>
            <ta e="T247" id="Seg_7010" s="T246">go-PRS.[3SG]</ta>
            <ta e="T248" id="Seg_7011" s="T247">always</ta>
            <ta e="T249" id="Seg_7012" s="T248">follow-PRS.[3SG]</ta>
            <ta e="T250" id="Seg_7013" s="T249">friend-3SG.[NOM]</ta>
            <ta e="T251" id="Seg_7014" s="T250">Iron.Cap.[NOM]</ta>
            <ta e="T252" id="Seg_7015" s="T251">wild.reindeer.[NOM]</ta>
            <ta e="T253" id="Seg_7016" s="T252">reindeer.bull-3SG.[NOM]</ta>
            <ta e="T254" id="Seg_7017" s="T253">become-PST1-3SG</ta>
            <ta e="T255" id="Seg_7018" s="T254">ability-2SG.[NOM]</ta>
            <ta e="T256" id="Seg_7019" s="T255">and</ta>
            <ta e="T257" id="Seg_7020" s="T256">last-3SG.[NOM]</ta>
            <ta e="T258" id="Seg_7021" s="T257">MOD</ta>
            <ta e="T259" id="Seg_7022" s="T258">chase-CVB.SIM</ta>
            <ta e="T260" id="Seg_7023" s="T259">MOD-1SG</ta>
            <ta e="T261" id="Seg_7024" s="T260">this.EMPH-INTNS</ta>
            <ta e="T262" id="Seg_7025" s="T261">think-CVB.SIM</ta>
            <ta e="T263" id="Seg_7026" s="T262">think-PRS.[3SG]</ta>
            <ta e="T264" id="Seg_7027" s="T263">see-PST2-3SG</ta>
            <ta e="T265" id="Seg_7028" s="T264">one</ta>
            <ta e="T266" id="Seg_7029" s="T265">place-DAT/LOC</ta>
            <ta e="T267" id="Seg_7030" s="T266">friend-3SG.[NOM]</ta>
            <ta e="T268" id="Seg_7031" s="T267">toddle-CVB.SEQ</ta>
            <ta e="T269" id="Seg_7032" s="T268">stand-PRS.[3SG]</ta>
            <ta e="T270" id="Seg_7033" s="T269">come.closer-CVB.SEQ</ta>
            <ta e="T271" id="Seg_7034" s="T270">see-PST2-3SG</ta>
            <ta e="T272" id="Seg_7035" s="T271">two</ta>
            <ta e="T273" id="Seg_7036" s="T272">suddenly</ta>
            <ta e="T274" id="Seg_7037" s="T273">leg-3SG.[NOM]</ta>
            <ta e="T275" id="Seg_7038" s="T274">stick.out-CVB.SEQ</ta>
            <ta e="T276" id="Seg_7039" s="T275">stay-PST2.[3SG]</ta>
            <ta e="T277" id="Seg_7040" s="T276">joint-PL-3SG-INSTR</ta>
            <ta e="T278" id="Seg_7041" s="T277">cut-CVB.SEQ</ta>
            <ta e="T279" id="Seg_7042" s="T278">carcass.[NOM]</ta>
            <ta e="T280" id="Seg_7043" s="T279">leg-PL-3SG-ACC</ta>
            <ta e="T281" id="Seg_7044" s="T280">roll-CAUS-CVB.SEQ</ta>
            <ta e="T282" id="Seg_7045" s="T281">throw-PST2.[3SG]</ta>
            <ta e="T283" id="Seg_7046" s="T282">that.EMPH.[NOM]</ta>
            <ta e="T284" id="Seg_7047" s="T283">like-INTNS</ta>
            <ta e="T285" id="Seg_7048" s="T284">Iron.Cap.[NOM]</ta>
            <ta e="T286" id="Seg_7049" s="T285">leg-3SG-ACC</ta>
            <ta e="T287" id="Seg_7050" s="T286">cut-CVB.ANT</ta>
            <ta e="T288" id="Seg_7051" s="T287">carcass.[NOM]</ta>
            <ta e="T289" id="Seg_7052" s="T288">become-CVB.SEQ</ta>
            <ta e="T290" id="Seg_7053" s="T289">roll-CVB.SEQ</ta>
            <ta e="T291" id="Seg_7054" s="T290">go-PST1-3SG</ta>
            <ta e="T292" id="Seg_7055" s="T291">how.much</ta>
            <ta e="T293" id="Seg_7056" s="T292">NEG</ta>
            <ta e="T294" id="Seg_7057" s="T293">be-EP-NEG.CVB</ta>
            <ta e="T295" id="Seg_7058" s="T294">carcass.[NOM]</ta>
            <ta e="T296" id="Seg_7059" s="T295">tower-CVB.SIM</ta>
            <ta e="T297" id="Seg_7060" s="T296">lie-PRS.[3SG]</ta>
            <ta e="T298" id="Seg_7061" s="T297">be.happy-PRS.[3SG]</ta>
            <ta e="T299" id="Seg_7062" s="T298">reach-PST1-1SG</ta>
            <ta e="T300" id="Seg_7063" s="T299">EMPH</ta>
            <ta e="T301" id="Seg_7064" s="T300">say-PRS.[3SG]</ta>
            <ta e="T302" id="Seg_7065" s="T301">this</ta>
            <ta e="T303" id="Seg_7066" s="T302">carcass-DAT/LOC</ta>
            <ta e="T304" id="Seg_7067" s="T303">reach-PST2-3SG</ta>
            <ta e="T305" id="Seg_7068" s="T304">carcass.[NOM]</ta>
            <ta e="T306" id="Seg_7069" s="T305">empty.[NOM]</ta>
            <ta e="T307" id="Seg_7070" s="T306">only</ta>
            <ta e="T308" id="Seg_7071" s="T307">lie-PRS.[3SG]</ta>
            <ta e="T309" id="Seg_7072" s="T308">head-3SG.[NOM]</ta>
            <ta e="T310" id="Seg_7073" s="T309">only</ta>
            <ta e="T311" id="Seg_7074" s="T310">roll-CVB.SEQ</ta>
            <ta e="T312" id="Seg_7075" s="T311">stay-PST2.[3SG]</ta>
            <ta e="T313" id="Seg_7076" s="T312">well</ta>
            <ta e="T314" id="Seg_7077" s="T313">well</ta>
            <ta e="T315" id="Seg_7078" s="T314">this</ta>
            <ta e="T316" id="Seg_7079" s="T315">why</ta>
            <ta e="T317" id="Seg_7080" s="T316">cut.up-EP-REFL-PRS.[3SG]</ta>
            <ta e="T318" id="Seg_7081" s="T317">self-3SG-ACC</ta>
            <ta e="T319" id="Seg_7082" s="T318">think-CVB.SEQ</ta>
            <ta e="T320" id="Seg_7083" s="T319">think-PRS.[3SG]</ta>
            <ta e="T321" id="Seg_7084" s="T320">like.this-INTNS</ta>
            <ta e="T322" id="Seg_7085" s="T321">what.[NOM]</ta>
            <ta e="T323" id="Seg_7086" s="T322">competition-3SG.[NOM]=Q</ta>
            <ta e="T324" id="Seg_7087" s="T323">think-CVB.SEQ</ta>
            <ta e="T325" id="Seg_7088" s="T324">be.surprised-CVB.SIM</ta>
            <ta e="T326" id="Seg_7089" s="T325">go-PST1-3SG</ta>
            <ta e="T327" id="Seg_7090" s="T326">head.[NOM]</ta>
            <ta e="T328" id="Seg_7091" s="T327">become-CVB.SEQ</ta>
            <ta e="T329" id="Seg_7092" s="T328">roll-CVB.SEQ</ta>
            <ta e="T330" id="Seg_7093" s="T329">go-PST1-3SG</ta>
            <ta e="T331" id="Seg_7094" s="T330">only</ta>
            <ta e="T332" id="Seg_7095" s="T331">head.[NOM]</ta>
            <ta e="T333" id="Seg_7096" s="T332">lie.stiff-CVB.SIM</ta>
            <ta e="T334" id="Seg_7097" s="T333">lie-PRS.[3SG]</ta>
            <ta e="T335" id="Seg_7098" s="T334">well</ta>
            <ta e="T336" id="Seg_7099" s="T335">reach-PST1-1SG</ta>
            <ta e="T337" id="Seg_7100" s="T336">MOD</ta>
            <ta e="T338" id="Seg_7101" s="T337">suddenly</ta>
            <ta e="T339" id="Seg_7102" s="T338">two</ta>
            <ta e="T340" id="Seg_7103" s="T339">eye-3SG.[NOM]</ta>
            <ta e="T341" id="Seg_7104" s="T340">undress-REFL-CVB.SEQ</ta>
            <ta e="T342" id="Seg_7105" s="T341">go-PST2.[3SG]</ta>
            <ta e="T343" id="Seg_7106" s="T342">oh.dear</ta>
            <ta e="T344" id="Seg_7107" s="T343">this</ta>
            <ta e="T345" id="Seg_7108" s="T344">what.[NOM]</ta>
            <ta e="T346" id="Seg_7109" s="T345">human.being-3SG.[NOM]=Q</ta>
            <ta e="T347" id="Seg_7110" s="T346">good.spirit-ACC</ta>
            <ta e="T348" id="Seg_7111" s="T347">follow-PRS-1SG</ta>
            <ta e="T349" id="Seg_7112" s="T348">Q</ta>
            <ta e="T350" id="Seg_7113" s="T349">evil.spirit-ACC</ta>
            <ta e="T351" id="Seg_7114" s="T350">Q</ta>
            <ta e="T352" id="Seg_7115" s="T351">say-PRS.[3SG]</ta>
            <ta e="T353" id="Seg_7116" s="T352">two</ta>
            <ta e="T354" id="Seg_7117" s="T353">eye-3SG.[NOM]</ta>
            <ta e="T355" id="Seg_7118" s="T354">roll-CVB.SEQ</ta>
            <ta e="T356" id="Seg_7119" s="T355">go-PST1-3SG</ta>
            <ta e="T357" id="Seg_7120" s="T356">eye-PL.[NOM]</ta>
            <ta e="T358" id="Seg_7121" s="T357">way-3PL-INSTR</ta>
            <ta e="T359" id="Seg_7122" s="T358">when</ta>
            <ta e="T360" id="Seg_7123" s="T359">INDEF</ta>
            <ta e="T361" id="Seg_7124" s="T360">this</ta>
            <ta e="T362" id="Seg_7125" s="T361">two</ta>
            <ta e="T363" id="Seg_7126" s="T362">eye.[NOM]</ta>
            <ta e="T364" id="Seg_7127" s="T363">trace-3SG.[NOM]</ta>
            <ta e="T365" id="Seg_7128" s="T364">one</ta>
            <ta e="T366" id="Seg_7129" s="T365">house-DIM-DAT/LOC</ta>
            <ta e="T367" id="Seg_7130" s="T366">reach-PRS.[3SG]</ta>
            <ta e="T368" id="Seg_7131" s="T367">this</ta>
            <ta e="T369" id="Seg_7132" s="T368">reach-CVB.SEQ</ta>
            <ta e="T370" id="Seg_7133" s="T369">two</ta>
            <ta e="T371" id="Seg_7134" s="T370">eye.[NOM]</ta>
            <ta e="T372" id="Seg_7135" s="T371">trace-3SG.[NOM]</ta>
            <ta e="T373" id="Seg_7136" s="T372">house-DAT/LOC</ta>
            <ta e="T374" id="Seg_7137" s="T373">go.in-PST2.[3SG]</ta>
            <ta e="T375" id="Seg_7138" s="T374">well</ta>
            <ta e="T376" id="Seg_7139" s="T375">last-2SG.[NOM]</ta>
            <ta e="T377" id="Seg_7140" s="T376">MOD</ta>
            <ta e="T378" id="Seg_7141" s="T377">two</ta>
            <ta e="T379" id="Seg_7142" s="T378">back</ta>
            <ta e="T380" id="Seg_7143" s="T379">eye.[NOM]</ta>
            <ta e="T381" id="Seg_7144" s="T380">house-DAT/LOC</ta>
            <ta e="T382" id="Seg_7145" s="T381">go.in-PST1-3SG</ta>
            <ta e="T383" id="Seg_7146" s="T382">go.in-PST2-3SG</ta>
            <ta e="T384" id="Seg_7147" s="T383">right</ta>
            <ta e="T385" id="Seg_7148" s="T384">in.the.direction</ta>
            <ta e="T386" id="Seg_7149" s="T385">bed-DAT/LOC</ta>
            <ta e="T387" id="Seg_7150" s="T386">old.woman.[NOM]</ta>
            <ta e="T388" id="Seg_7151" s="T387">sit-PRS.[3SG]</ta>
            <ta e="T389" id="Seg_7152" s="T388">eye-3PL.[NOM]</ta>
            <ta e="T390" id="Seg_7153" s="T389">back.part.of.the.yurt.[NOM]</ta>
            <ta e="T391" id="Seg_7154" s="T390">twinkle-CVB.SIM</ta>
            <ta e="T392" id="Seg_7155" s="T391">lie-PRS-3PL</ta>
            <ta e="T393" id="Seg_7156" s="T392">exactly.this</ta>
            <ta e="T394" id="Seg_7157" s="T393">two</ta>
            <ta e="T395" id="Seg_7158" s="T394">eye.[NOM]</ta>
            <ta e="T396" id="Seg_7159" s="T395">go.in-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T397" id="Seg_7160" s="T396">front</ta>
            <ta e="T398" id="Seg_7161" s="T397">two</ta>
            <ta e="T399" id="Seg_7162" s="T398">eye.[NOM]</ta>
            <ta e="T400" id="Seg_7163" s="T399">language-PROPR.[NOM]</ta>
            <ta e="T401" id="Seg_7164" s="T400">oh</ta>
            <ta e="T402" id="Seg_7165" s="T401">and</ta>
            <ta e="T403" id="Seg_7166" s="T402">human.being-ACC</ta>
            <ta e="T404" id="Seg_7167" s="T403">follow-PTCP.PRS</ta>
            <ta e="T405" id="Seg_7168" s="T404">human.being.[NOM]</ta>
            <ta e="T406" id="Seg_7169" s="T405">get.tired-HAB.[3SG]</ta>
            <ta e="T407" id="Seg_7170" s="T406">2SG.[NOM]</ta>
            <ta e="T408" id="Seg_7171" s="T407">relax.[IMP.2SG]</ta>
            <ta e="T409" id="Seg_7172" s="T408">1SG.[NOM]</ta>
            <ta e="T410" id="Seg_7173" s="T409">carcass-PL-1PL-ACC</ta>
            <ta e="T411" id="Seg_7174" s="T410">head-PL-1PL-ACC</ta>
            <ta e="T412" id="Seg_7175" s="T411">gather-ITER-CVB.SEQ</ta>
            <ta e="T413" id="Seg_7176" s="T412">bring-FUT-1SG</ta>
            <ta e="T414" id="Seg_7177" s="T413">say-PRS.[3SG]</ta>
            <ta e="T415" id="Seg_7178" s="T414">friend-3SG.[NOM]</ta>
            <ta e="T416" id="Seg_7179" s="T415">Iron.Cap.[NOM]</ta>
            <ta e="T417" id="Seg_7180" s="T416">that-DAT/LOC</ta>
            <ta e="T418" id="Seg_7181" s="T417">this</ta>
            <ta e="T419" id="Seg_7182" s="T418">real</ta>
            <ta e="T420" id="Seg_7183" s="T419">mind-EP-INSTR</ta>
            <ta e="T421" id="Seg_7184" s="T420">go-PTCP.PRS</ta>
            <ta e="T422" id="Seg_7185" s="T421">human.being-2SG</ta>
            <ta e="T423" id="Seg_7186" s="T422">Q</ta>
            <ta e="T424" id="Seg_7187" s="T423">NEG</ta>
            <ta e="T425" id="Seg_7188" s="T424">Q</ta>
            <ta e="T426" id="Seg_7189" s="T425">this</ta>
            <ta e="T427" id="Seg_7190" s="T426">1PL.[NOM]</ta>
            <ta e="T428" id="Seg_7191" s="T427">human.being.[NOM]</ta>
            <ta e="T429" id="Seg_7192" s="T428">can-CVB.SEQ</ta>
            <ta e="T430" id="Seg_7193" s="T429">come.back-EP-NEG.PTCP</ta>
            <ta e="T431" id="Seg_7194" s="T430">place-3SG-DAT/LOC</ta>
            <ta e="T432" id="Seg_7195" s="T431">come-PST1-1PL</ta>
            <ta e="T433" id="Seg_7196" s="T432">well</ta>
            <ta e="T434" id="Seg_7197" s="T433">now</ta>
            <ta e="T435" id="Seg_7198" s="T434">2SG.[NOM]</ta>
            <ta e="T436" id="Seg_7199" s="T435">follow-FUT.[IMP.2SG]</ta>
            <ta e="T437" id="Seg_7200" s="T436">1SG-ACC</ta>
            <ta e="T438" id="Seg_7201" s="T437">chase-FUT.[IMP.2SG]</ta>
            <ta e="T439" id="Seg_7202" s="T438">1SG.[NOM]</ta>
            <ta e="T440" id="Seg_7203" s="T439">ability-1SG-ACC</ta>
            <ta e="T441" id="Seg_7204" s="T440">know-PRS-2SG</ta>
            <ta e="T442" id="Seg_7205" s="T441">this</ta>
            <ta e="T443" id="Seg_7206" s="T442">snowstorm</ta>
            <ta e="T444" id="Seg_7207" s="T443">master-3SG-DAT/LOC</ta>
            <ta e="T445" id="Seg_7208" s="T444">come-PST1-1PL</ta>
            <ta e="T446" id="Seg_7209" s="T445">in.the.morning</ta>
            <ta e="T447" id="Seg_7210" s="T446">early-AG.[NOM]</ta>
            <ta e="T448" id="Seg_7211" s="T447">human.being.[NOM]</ta>
            <ta e="T449" id="Seg_7212" s="T448">stand-PTCP.PRS</ta>
            <ta e="T450" id="Seg_7213" s="T449">time-3SG-DAT/LOC</ta>
            <ta e="T451" id="Seg_7214" s="T450">stand-CVB.SEQ</ta>
            <ta e="T452" id="Seg_7215" s="T451">go.out-FUT-1SG</ta>
            <ta e="T453" id="Seg_7216" s="T452">this-DAT/LOC</ta>
            <ta e="T454" id="Seg_7217" s="T453">hundred-DISTR</ta>
            <ta e="T455" id="Seg_7218" s="T454">caravan-PROPR</ta>
            <ta e="T456" id="Seg_7219" s="T455">sledge-PROPR</ta>
            <ta e="T457" id="Seg_7220" s="T456">two</ta>
            <ta e="T458" id="Seg_7221" s="T457">girl-PL.[NOM]</ta>
            <ta e="T459" id="Seg_7222" s="T458">come-FUT-3PL</ta>
            <ta e="T460" id="Seg_7223" s="T459">this-DAT/LOC</ta>
            <ta e="T461" id="Seg_7224" s="T460">1SG.[NOM]</ta>
            <ta e="T462" id="Seg_7225" s="T461">go.out-CVB.SIM</ta>
            <ta e="T463" id="Seg_7226" s="T462">run-CVB.SEQ-1SG</ta>
            <ta e="T464" id="Seg_7227" s="T463">front</ta>
            <ta e="T465" id="Seg_7228" s="T464">girl-ACC</ta>
            <ta e="T466" id="Seg_7229" s="T465">front.reindeer-3SG-ACC</ta>
            <ta e="T467" id="Seg_7230" s="T466">shoulder.strap-3SG-ABL</ta>
            <ta e="T468" id="Seg_7231" s="T467">hold-FUT-1SG</ta>
            <ta e="T469" id="Seg_7232" s="T468">and</ta>
            <ta e="T470" id="Seg_7233" s="T469">run-FUT-1SG</ta>
            <ta e="T471" id="Seg_7234" s="T470">this</ta>
            <ta e="T472" id="Seg_7235" s="T471">hundred</ta>
            <ta e="T473" id="Seg_7236" s="T472">sledge-ABL</ta>
            <ta e="T474" id="Seg_7237" s="T473">one-3SG-DAT/LOC</ta>
            <ta e="T475" id="Seg_7238" s="T474">INDEF</ta>
            <ta e="T476" id="Seg_7239" s="T475">fix-EP-REFL-TEMP-2SG</ta>
            <ta e="T477" id="Seg_7240" s="T476">reach-MED-FUT-2SG</ta>
            <ta e="T478" id="Seg_7241" s="T477">country-2SG-DAT/LOC</ta>
            <ta e="T479" id="Seg_7242" s="T478">then</ta>
            <ta e="T480" id="Seg_7243" s="T479">fix-EP-REFL-NEG-TEMP-2SG</ta>
            <ta e="T481" id="Seg_7244" s="T480">this</ta>
            <ta e="T482" id="Seg_7245" s="T481">world-DAT/LOC</ta>
            <ta e="T483" id="Seg_7246" s="T482">die-FUT-2SG</ta>
            <ta e="T484" id="Seg_7247" s="T483">say-PRS.[3SG]</ta>
            <ta e="T485" id="Seg_7248" s="T484">well</ta>
            <ta e="T486" id="Seg_7249" s="T485">friend-3SG.[NOM]</ta>
            <ta e="T487" id="Seg_7250" s="T486">Bony.Belt.[NOM]</ta>
            <ta e="T488" id="Seg_7251" s="T487">carcass-ACC</ta>
            <ta e="T489" id="Seg_7252" s="T488">leg-PL-ACC</ta>
            <ta e="T490" id="Seg_7253" s="T489">head-PL-ACC</ta>
            <ta e="T491" id="Seg_7254" s="T490">bring-PST1-3SG</ta>
            <ta e="T492" id="Seg_7255" s="T491">self-3PL.[NOM]</ta>
            <ta e="T493" id="Seg_7256" s="T492">self-3PL-INSTR</ta>
            <ta e="T494" id="Seg_7257" s="T493">human.being.[NOM]</ta>
            <ta e="T495" id="Seg_7258" s="T494">be-EMOT-CVB.SEQ</ta>
            <ta e="T496" id="Seg_7259" s="T495">stay-PST1-3PL</ta>
            <ta e="T497" id="Seg_7260" s="T496">sleep-ITER-CVB.SEQ</ta>
            <ta e="T498" id="Seg_7261" s="T497">after</ta>
            <ta e="T499" id="Seg_7262" s="T498">Bony.Belt.[NOM]</ta>
            <ta e="T500" id="Seg_7263" s="T499">wake.up-PRS.[3SG]</ta>
            <ta e="T501" id="Seg_7264" s="T500">friend-3SG.[NOM]</ta>
            <ta e="T502" id="Seg_7265" s="T501">go.out-EP-PTCP.PST</ta>
            <ta e="T503" id="Seg_7266" s="T502">noise-3SG-DAT/LOC</ta>
            <ta e="T504" id="Seg_7267" s="T503">Bony.Belt.[NOM]</ta>
            <ta e="T505" id="Seg_7268" s="T504">go.out-CVB.SIM</ta>
            <ta e="T506" id="Seg_7269" s="T505">run-PST2-3SG</ta>
            <ta e="T507" id="Seg_7270" s="T506">hundred</ta>
            <ta e="T508" id="Seg_7271" s="T507">sledge.[NOM]</ta>
            <ta e="T509" id="Seg_7272" s="T508">scurry-EP-MED-CVB.SEQ</ta>
            <ta e="T510" id="Seg_7273" s="T509">be-TEMP-3SG</ta>
            <ta e="T511" id="Seg_7274" s="T510">last</ta>
            <ta e="T512" id="Seg_7275" s="T511">sledge-DAT/LOC</ta>
            <ta e="T513" id="Seg_7276" s="T512">hang.oneself-PST1-3SG</ta>
            <ta e="T514" id="Seg_7277" s="T513">sky-EP-INSTR</ta>
            <ta e="T515" id="Seg_7278" s="T514">and</ta>
            <ta e="T516" id="Seg_7279" s="T515">earth-EP-INSTR</ta>
            <ta e="T517" id="Seg_7280" s="T516">and</ta>
            <ta e="T518" id="Seg_7281" s="T517">go-PTCP.PRS-3SG-ACC</ta>
            <ta e="T519" id="Seg_7282" s="T518">judge-PST2.NEG-3SG</ta>
            <ta e="T520" id="Seg_7283" s="T519">EMPH</ta>
            <ta e="T521" id="Seg_7284" s="T520">later</ta>
            <ta e="T522" id="Seg_7285" s="T521">EMPH</ta>
            <ta e="T523" id="Seg_7286" s="T522">then</ta>
            <ta e="T524" id="Seg_7287" s="T523">hundred</ta>
            <ta e="T525" id="Seg_7288" s="T524">sledge.[NOM]</ta>
            <ta e="T526" id="Seg_7289" s="T525">stop-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T527" id="Seg_7290" s="T526">see-PST2-3SG</ta>
            <ta e="T528" id="Seg_7291" s="T527">friend-3SG.[NOM]</ta>
            <ta e="T529" id="Seg_7292" s="T528">country-3SG-DAT/LOC</ta>
            <ta e="T530" id="Seg_7293" s="T529">bring-PST2.[3SG]</ta>
            <ta e="T531" id="Seg_7294" s="T530">well</ta>
            <ta e="T532" id="Seg_7295" s="T531">Iron.Cap.[NOM]</ta>
            <ta e="T533" id="Seg_7296" s="T532">say-PRS.[3SG]</ta>
            <ta e="T534" id="Seg_7297" s="T533">friend-3SG-ACC</ta>
            <ta e="T535" id="Seg_7298" s="T534">well</ta>
            <ta e="T536" id="Seg_7299" s="T535">that.[NOM]</ta>
            <ta e="T537" id="Seg_7300" s="T536">hundred</ta>
            <ta e="T538" id="Seg_7301" s="T537">sledge.[NOM]</ta>
            <ta e="T539" id="Seg_7302" s="T538">other.of.two</ta>
            <ta e="T540" id="Seg_7303" s="T539">side-3SG-DAT/LOC</ta>
            <ta e="T541" id="Seg_7304" s="T540">three</ta>
            <ta e="T542" id="Seg_7305" s="T541">white</ta>
            <ta e="T543" id="Seg_7306" s="T542">reindeer.bull.[NOM]</ta>
            <ta e="T544" id="Seg_7307" s="T543">harness-PASS/REFL-CVB.SEQ</ta>
            <ta e="T545" id="Seg_7308" s="T544">stand-PRS.[3SG]</ta>
            <ta e="T546" id="Seg_7309" s="T545">other.of.two</ta>
            <ta e="T547" id="Seg_7310" s="T546">side-3SG-DAT/LOC</ta>
            <ta e="T548" id="Seg_7311" s="T547">three</ta>
            <ta e="T549" id="Seg_7312" s="T548">black</ta>
            <ta e="T550" id="Seg_7313" s="T549">reindeer.bull.[NOM]</ta>
            <ta e="T551" id="Seg_7314" s="T550">stand-PRS.[3SG]</ta>
            <ta e="T552" id="Seg_7315" s="T551">well</ta>
            <ta e="T553" id="Seg_7316" s="T552">1SG.[NOM]</ta>
            <ta e="T554" id="Seg_7317" s="T553">that</ta>
            <ta e="T555" id="Seg_7318" s="T554">three</ta>
            <ta e="T556" id="Seg_7319" s="T555">white</ta>
            <ta e="T557" id="Seg_7320" s="T556">reindeer.bull-ACC</ta>
            <ta e="T558" id="Seg_7321" s="T557">kill-FUT-1SG</ta>
            <ta e="T559" id="Seg_7322" s="T558">skin-3PL-ACC</ta>
            <ta e="T560" id="Seg_7323" s="T559">remove-FUT-1SG</ta>
            <ta e="T561" id="Seg_7324" s="T560">2SG.[NOM]</ta>
            <ta e="T562" id="Seg_7325" s="T561">three</ta>
            <ta e="T563" id="Seg_7326" s="T562">black-ACC</ta>
            <ta e="T564" id="Seg_7327" s="T563">kill-FUT-2SG</ta>
            <ta e="T565" id="Seg_7328" s="T564">skin-FUT-2SG</ta>
            <ta e="T566" id="Seg_7329" s="T565">skin-3PL-ACC</ta>
            <ta e="T567" id="Seg_7330" s="T566">know-PRS-2SG</ta>
            <ta e="T568" id="Seg_7331" s="T567">this</ta>
            <ta e="T569" id="Seg_7332" s="T568">snowstorm.[NOM]</ta>
            <ta e="T570" id="Seg_7333" s="T569">master-3SG-GEN</ta>
            <ta e="T571" id="Seg_7334" s="T570">little</ta>
            <ta e="T572" id="Seg_7335" s="T571">daughter-PL-3SG-ACC</ta>
            <ta e="T573" id="Seg_7336" s="T572">bring-PST1-1SG</ta>
            <ta e="T574" id="Seg_7337" s="T573">magic-1SG-INSTR</ta>
            <ta e="T575" id="Seg_7338" s="T574">now</ta>
            <ta e="T576" id="Seg_7339" s="T575">that-3SG-3PL-ACC</ta>
            <ta e="T577" id="Seg_7340" s="T576">follow-CVB.SEQ</ta>
            <ta e="T578" id="Seg_7341" s="T577">three</ta>
            <ta e="T579" id="Seg_7342" s="T578">elder.brother-3SG.[NOM]</ta>
            <ta e="T580" id="Seg_7343" s="T579">snowstorm</ta>
            <ta e="T581" id="Seg_7344" s="T580">master-PL-3SG.[NOM]</ta>
            <ta e="T582" id="Seg_7345" s="T581">come-FUT-3PL</ta>
            <ta e="T583" id="Seg_7346" s="T582">that</ta>
            <ta e="T584" id="Seg_7347" s="T583">human.being-PL.[NOM]</ta>
            <ta e="T585" id="Seg_7348" s="T584">come-TEMP-3PL</ta>
            <ta e="T586" id="Seg_7349" s="T585">back.part.of.the.yurt.[NOM]</ta>
            <ta e="T587" id="Seg_7350" s="T586">three</ta>
            <ta e="T588" id="Seg_7351" s="T587">white</ta>
            <ta e="T589" id="Seg_7352" s="T588">fur-ACC</ta>
            <ta e="T590" id="Seg_7353" s="T589">spread-FUT-1SG</ta>
            <ta e="T591" id="Seg_7354" s="T590">2SG.[NOM]</ta>
            <ta e="T592" id="Seg_7355" s="T591">that.[NOM]</ta>
            <ta e="T593" id="Seg_7356" s="T592">three</ta>
            <ta e="T594" id="Seg_7357" s="T593">black</ta>
            <ta e="T595" id="Seg_7358" s="T594">skin-2SG-ACC</ta>
            <ta e="T596" id="Seg_7359" s="T595">spread-FUT.[IMP.2SG]</ta>
            <ta e="T597" id="Seg_7360" s="T596">well</ta>
            <ta e="T598" id="Seg_7361" s="T597">big</ta>
            <ta e="T599" id="Seg_7362" s="T598">big.kettle-2SG-DAT/LOC</ta>
            <ta e="T600" id="Seg_7363" s="T599">full</ta>
            <ta e="T601" id="Seg_7364" s="T600">water-3SG.[NOM]</ta>
            <ta e="T602" id="Seg_7365" s="T601">boil-CAUS.[IMP.2SG]</ta>
            <ta e="T603" id="Seg_7366" s="T602">1SG.[NOM]</ta>
            <ta e="T604" id="Seg_7367" s="T603">again</ta>
            <ta e="T605" id="Seg_7368" s="T604">boil-CAUS-FUT-1SG</ta>
            <ta e="T606" id="Seg_7369" s="T605">exactly</ta>
            <ta e="T607" id="Seg_7370" s="T606">first</ta>
            <ta e="T608" id="Seg_7371" s="T607">1SG-DAT/LOC</ta>
            <ta e="T609" id="Seg_7372" s="T608">guest-VBZ-FUT-3PL</ta>
            <ta e="T610" id="Seg_7373" s="T609">1SG-ABL</ta>
            <ta e="T611" id="Seg_7374" s="T610">eat.ones.fill-CVB.SEQ</ta>
            <ta e="T612" id="Seg_7375" s="T611">be.content-CVB.SEQ</ta>
            <ta e="T613" id="Seg_7376" s="T612">go.out-TEMP-3PL</ta>
            <ta e="T614" id="Seg_7377" s="T613">2SG-DAT/LOC</ta>
            <ta e="T615" id="Seg_7378" s="T614">guest-VBZ-FUT-3PL</ta>
            <ta e="T616" id="Seg_7379" s="T615">1SG.[NOM]</ta>
            <ta e="T617" id="Seg_7380" s="T616">3PL-DAT/LOC</ta>
            <ta e="T618" id="Seg_7381" s="T617">what-ACC</ta>
            <ta e="T619" id="Seg_7382" s="T618">INDEF</ta>
            <ta e="T620" id="Seg_7383" s="T619">show-FUT-1SG</ta>
            <ta e="T621" id="Seg_7384" s="T620">go.in-RECP/COLL-FUT.[IMP.2SG]</ta>
            <ta e="T622" id="Seg_7385" s="T621">1SG.[NOM]</ta>
            <ta e="T623" id="Seg_7386" s="T622">what-ACC</ta>
            <ta e="T624" id="Seg_7387" s="T623">see-CAUS-PRS-1SG</ta>
            <ta e="T625" id="Seg_7388" s="T624">and</ta>
            <ta e="T626" id="Seg_7389" s="T625">imitate-CVB.SEQ</ta>
            <ta e="T627" id="Seg_7390" s="T626">3PL-DAT/LOC</ta>
            <ta e="T628" id="Seg_7391" s="T627">see-CAUS-FUT.[IMP.2SG]</ta>
            <ta e="T629" id="Seg_7392" s="T628">say-PST1-3SG</ta>
            <ta e="T630" id="Seg_7393" s="T629">teach-PST1-3SG</ta>
            <ta e="T631" id="Seg_7394" s="T630">friend-3SG-ACC</ta>
            <ta e="T632" id="Seg_7395" s="T631">how.much</ta>
            <ta e="T633" id="Seg_7396" s="T632">NEG</ta>
            <ta e="T634" id="Seg_7397" s="T633">be-EP-NEG.CVB</ta>
            <ta e="T635" id="Seg_7398" s="T634">snowstorm.[NOM]</ta>
            <ta e="T636" id="Seg_7399" s="T635">master-PL-3SG.[NOM]</ta>
            <ta e="T637" id="Seg_7400" s="T636">whirlwind-INSTR</ta>
            <ta e="T638" id="Seg_7401" s="T637">come-PST1-3PL</ta>
            <ta e="T639" id="Seg_7402" s="T638">dense</ta>
            <ta e="T640" id="Seg_7403" s="T639">ice.[NOM]</ta>
            <ta e="T641" id="Seg_7404" s="T640">people-PL.[NOM]</ta>
            <ta e="T642" id="Seg_7405" s="T641">go.in-CVB.SEQ-3PL</ta>
            <ta e="T643" id="Seg_7406" s="T642">completely-EMPH-completely</ta>
            <ta e="T644" id="Seg_7407" s="T643">hello</ta>
            <ta e="T645" id="Seg_7408" s="T644">NEG</ta>
            <ta e="T646" id="Seg_7409" s="T645">say-NEG-3PL</ta>
            <ta e="T647" id="Seg_7410" s="T646">Iron.Cap.[NOM]</ta>
            <ta e="T648" id="Seg_7411" s="T647">house-3SG-DAT/LOC</ta>
            <ta e="T649" id="Seg_7412" s="T648">three</ta>
            <ta e="T650" id="Seg_7413" s="T649">white</ta>
            <ta e="T651" id="Seg_7414" s="T650">skin-DAT/LOC</ta>
            <ta e="T652" id="Seg_7415" s="T651">sit.down-PRS-3PL</ta>
            <ta e="T653" id="Seg_7416" s="T652">Iron.Cap.[NOM]</ta>
            <ta e="T654" id="Seg_7417" s="T653">ability-VBZ-CAP.[3SG]</ta>
            <ta e="T655" id="Seg_7418" s="T654">this</ta>
            <ta e="T656" id="Seg_7419" s="T655">people-DAT/LOC</ta>
            <ta e="T657" id="Seg_7420" s="T656">boil-CVB.SIM</ta>
            <ta e="T658" id="Seg_7421" s="T657">stand-PTCP.PRS</ta>
            <ta e="T659" id="Seg_7422" s="T658">big</ta>
            <ta e="T660" id="Seg_7423" s="T659">big.kettle-DAT/LOC</ta>
            <ta e="T661" id="Seg_7424" s="T660">bony</ta>
            <ta e="T662" id="Seg_7425" s="T661">beak-PROPR</ta>
            <ta e="T663" id="Seg_7426" s="T662">black_throated.diver.[NOM]</ta>
            <ta e="T664" id="Seg_7427" s="T663">become-CVB.ANT</ta>
            <ta e="T665" id="Seg_7428" s="T664">aa_uu</ta>
            <ta e="T666" id="Seg_7429" s="T665">say-CVB.ANT</ta>
            <ta e="T667" id="Seg_7430" s="T666">dive-CVB.SEQ</ta>
            <ta e="T668" id="Seg_7431" s="T667">stay-PRS.[3SG]</ta>
            <ta e="T669" id="Seg_7432" s="T668">beat.the.wings-CVB.SIM-beat.the.wings-PTCP.PST-EP-INSTR</ta>
            <ta e="T670" id="Seg_7433" s="T669">fly-CVB.SEQ</ta>
            <ta e="T671" id="Seg_7434" s="T670">go.out-CVB.SEQ</ta>
            <ta e="T672" id="Seg_7435" s="T671">mother-3SG-GEN</ta>
            <ta e="T673" id="Seg_7436" s="T672">stomach-3SG-INSTR</ta>
            <ta e="T674" id="Seg_7437" s="T673">ONOM</ta>
            <ta e="T675" id="Seg_7438" s="T674">make-PRS.[3SG]</ta>
            <ta e="T676" id="Seg_7439" s="T675">then</ta>
            <ta e="T677" id="Seg_7440" s="T676">mother-3SG-GEN</ta>
            <ta e="T678" id="Seg_7441" s="T677">mouth-3SG-ACC</ta>
            <ta e="T679" id="Seg_7442" s="T678">through</ta>
            <ta e="T680" id="Seg_7443" s="T679">go.out-CVB.SIM</ta>
            <ta e="T681" id="Seg_7444" s="T680">fly-PST2.[3SG]</ta>
            <ta e="T682" id="Seg_7445" s="T681">well</ta>
            <ta e="T683" id="Seg_7446" s="T682">then</ta>
            <ta e="T684" id="Seg_7447" s="T683">father-3SG-GEN</ta>
            <ta e="T685" id="Seg_7448" s="T684">belly-3SG-INSTR</ta>
            <ta e="T686" id="Seg_7449" s="T685">ONOM</ta>
            <ta e="T687" id="Seg_7450" s="T686">make-PST2.[3SG]</ta>
            <ta e="T688" id="Seg_7451" s="T687">father-3SG-GEN</ta>
            <ta e="T689" id="Seg_7452" s="T688">mouth-3SG-INSTR</ta>
            <ta e="T690" id="Seg_7453" s="T689">just</ta>
            <ta e="T691" id="Seg_7454" s="T690">horn-3SG-ACC</ta>
            <ta e="T692" id="Seg_7455" s="T691">pull.off-PTCP.PST</ta>
            <ta e="T693" id="Seg_7456" s="T692">wild</ta>
            <ta e="T694" id="Seg_7457" s="T693">reindeer.bull-3SG.[NOM]</ta>
            <ta e="T695" id="Seg_7458" s="T694">be-CVB.SEQ</ta>
            <ta e="T696" id="Seg_7459" s="T695">go.out-PRS.[3SG]</ta>
            <ta e="T697" id="Seg_7460" s="T696">this.[NOM]</ta>
            <ta e="T698" id="Seg_7461" s="T697">back-ABL</ta>
            <ta e="T699" id="Seg_7462" s="T698">Iron.Cap.[NOM]</ta>
            <ta e="T700" id="Seg_7463" s="T699">human.being.[NOM]</ta>
            <ta e="T701" id="Seg_7464" s="T700">become-PRS.[3SG]</ta>
            <ta e="T702" id="Seg_7465" s="T701">and</ta>
            <ta e="T703" id="Seg_7466" s="T702">bow-INSTR</ta>
            <ta e="T704" id="Seg_7467" s="T703">this</ta>
            <ta e="T705" id="Seg_7468" s="T704">wild</ta>
            <ta e="T706" id="Seg_7469" s="T705">reindeer.bull-3SG-ACC</ta>
            <ta e="T707" id="Seg_7470" s="T706">through</ta>
            <ta e="T708" id="Seg_7471" s="T707">shoot-CVB.SEQ</ta>
            <ta e="T709" id="Seg_7472" s="T708">drop-PRS.[3SG]</ta>
            <ta e="T710" id="Seg_7473" s="T709">kill-PRS.[3SG]</ta>
            <ta e="T711" id="Seg_7474" s="T710">and</ta>
            <ta e="T712" id="Seg_7475" s="T711">this-DAT/LOC</ta>
            <ta e="T713" id="Seg_7476" s="T712">remove-EP-CAUS-EP-CAUS-CVB.SIM</ta>
            <ta e="T714" id="Seg_7477" s="T713">skin-CVB.SEQ</ta>
            <ta e="T715" id="Seg_7478" s="T714">wood.[NOM]</ta>
            <ta e="T716" id="Seg_7479" s="T715">bowl-DAT/LOC</ta>
            <ta e="T717" id="Seg_7480" s="T716">raw-VBZ-CVB.SIM-cut-NMNZ-VBZ-CVB.SIM</ta>
            <ta e="T718" id="Seg_7481" s="T717">pull-CVB.SEQ</ta>
            <ta e="T719" id="Seg_7482" s="T718">throw-PRS.[3SG]</ta>
            <ta e="T720" id="Seg_7483" s="T719">guest-PL-DAT/LOC</ta>
            <ta e="T721" id="Seg_7484" s="T720">raw.meat-VBZ-EP-CAUS-CVB.SEQ</ta>
            <ta e="T722" id="Seg_7485" s="T721">guest-3PL.[NOM]</ta>
            <ta e="T723" id="Seg_7486" s="T722">word.[NOM]-language.[NOM]</ta>
            <ta e="T724" id="Seg_7487" s="T723">NEG.EX</ta>
            <ta e="T725" id="Seg_7488" s="T724">eat-PTCP.PRS-DAT/LOC</ta>
            <ta e="T726" id="Seg_7489" s="T725">go-PST2-3PL</ta>
            <ta e="T727" id="Seg_7490" s="T726">autumnal</ta>
            <ta e="T728" id="Seg_7491" s="T727">reindeer.bull-ABL</ta>
            <ta e="T729" id="Seg_7492" s="T728">at.all</ta>
            <ta e="T730" id="Seg_7493" s="T729">horn-PROPR</ta>
            <ta e="T731" id="Seg_7494" s="T730">hoof-3SG-ACC</ta>
            <ta e="T732" id="Seg_7495" s="T731">remain-CAUS-CVB.SEQ</ta>
            <ta e="T733" id="Seg_7496" s="T732">throw-PST1-3PL</ta>
            <ta e="T734" id="Seg_7497" s="T733">Bony.Belt.[NOM]</ta>
            <ta e="T735" id="Seg_7498" s="T734">see-CVB.SEQ</ta>
            <ta e="T736" id="Seg_7499" s="T735">sit.down-EP-MED-FREQ-PST1-3SG</ta>
            <ta e="T737" id="Seg_7500" s="T736">eat-CVB.SEQ</ta>
            <ta e="T738" id="Seg_7501" s="T737">stop-PRS-3PL</ta>
            <ta e="T739" id="Seg_7502" s="T738">and</ta>
            <ta e="T740" id="Seg_7503" s="T739">lip-3PL-ACC</ta>
            <ta e="T741" id="Seg_7504" s="T740">wipe-ITER-CVB.SEQ</ta>
            <ta e="T742" id="Seg_7505" s="T741">go.out-PRS-3PL</ta>
            <ta e="T743" id="Seg_7506" s="T742">Bony.Belt.[NOM]</ta>
            <ta e="T744" id="Seg_7507" s="T743">house-3SG-DAT/LOC</ta>
            <ta e="T745" id="Seg_7508" s="T744">jump-PRS.[3SG]</ta>
            <ta e="T746" id="Seg_7509" s="T745">guest-PL-ACC</ta>
            <ta e="T747" id="Seg_7510" s="T746">receive-EP-MED-CVB.SIM</ta>
            <ta e="T748" id="Seg_7511" s="T747">guest-3PL.[NOM]</ta>
            <ta e="T749" id="Seg_7512" s="T748">go.in-CVB.SEQ</ta>
            <ta e="T750" id="Seg_7513" s="T749">three</ta>
            <ta e="T751" id="Seg_7514" s="T750">black</ta>
            <ta e="T752" id="Seg_7515" s="T751">skin-DAT/LOC</ta>
            <ta e="T753" id="Seg_7516" s="T752">sit-CVB.SIM</ta>
            <ta e="T754" id="Seg_7517" s="T753">give-PRS-3PL</ta>
            <ta e="T755" id="Seg_7518" s="T754">Bony.Belt.[NOM]</ta>
            <ta e="T756" id="Seg_7519" s="T755">aa_uu</ta>
            <ta e="T757" id="Seg_7520" s="T756">say-CVB.ANT</ta>
            <ta e="T758" id="Seg_7521" s="T757">red_throated.diver-DIM.[NOM]</ta>
            <ta e="T759" id="Seg_7522" s="T758">become-CVB.ANT</ta>
            <ta e="T760" id="Seg_7523" s="T759">big.kettle-DAT/LOC</ta>
            <ta e="T761" id="Seg_7524" s="T760">dive-PRS.[3SG]</ta>
            <ta e="T762" id="Seg_7525" s="T761">mother-3SG-GEN</ta>
            <ta e="T763" id="Seg_7526" s="T762">belly-3SG-DAT/LOC</ta>
            <ta e="T764" id="Seg_7527" s="T763">ONOM</ta>
            <ta e="T765" id="Seg_7528" s="T764">make-PRS.[3SG]</ta>
            <ta e="T766" id="Seg_7529" s="T765">mouth-3SG-ACC</ta>
            <ta e="T767" id="Seg_7530" s="T766">through</ta>
            <ta e="T768" id="Seg_7531" s="T767">go.out-PRS.[3SG]</ta>
            <ta e="T769" id="Seg_7532" s="T768">father-3SG-GEN</ta>
            <ta e="T770" id="Seg_7533" s="T769">belly-3SG-DAT/LOC</ta>
            <ta e="T771" id="Seg_7534" s="T770">ONOM</ta>
            <ta e="T772" id="Seg_7535" s="T771">make-PRS.[3SG]</ta>
            <ta e="T773" id="Seg_7536" s="T772">father-3SG-GEN</ta>
            <ta e="T774" id="Seg_7537" s="T773">mouth-3SG-ACC</ta>
            <ta e="T775" id="Seg_7538" s="T774">through</ta>
            <ta e="T776" id="Seg_7539" s="T775">what-ABL</ta>
            <ta e="T777" id="Seg_7540" s="T776">INDEF</ta>
            <ta e="T778" id="Seg_7541" s="T777">bad</ta>
            <ta e="T779" id="Seg_7542" s="T778">widower-ADJZ</ta>
            <ta e="T780" id="Seg_7543" s="T779">calf-DIM.[NOM]</ta>
            <ta e="T781" id="Seg_7544" s="T780">become-CVB.SEQ</ta>
            <ta e="T782" id="Seg_7545" s="T781">go.out-CVB.SIM</ta>
            <ta e="T783" id="Seg_7546" s="T782">run-PST2.[3SG]</ta>
            <ta e="T784" id="Seg_7547" s="T783">back-3SG-ABL</ta>
            <ta e="T785" id="Seg_7548" s="T784">bow-PROPR</ta>
            <ta e="T786" id="Seg_7549" s="T785">human.being.[NOM]</ta>
            <ta e="T787" id="Seg_7550" s="T786">go.out-PRS.[3SG]</ta>
            <ta e="T788" id="Seg_7551" s="T787">and</ta>
            <ta e="T789" id="Seg_7552" s="T788">guest-PL.[NOM]</ta>
            <ta e="T790" id="Seg_7553" s="T789">front-3PL-DAT/LOC</ta>
            <ta e="T791" id="Seg_7554" s="T790">close</ta>
            <ta e="T792" id="Seg_7555" s="T791">shoot-PRS.[3SG]</ta>
            <ta e="T793" id="Seg_7556" s="T792">food-VBZ-CVB.SEQ</ta>
            <ta e="T794" id="Seg_7557" s="T793">throw-PRS.[3SG]</ta>
            <ta e="T795" id="Seg_7558" s="T794">guest-PL-DAT/LOC</ta>
            <ta e="T796" id="Seg_7559" s="T795">pull-CVB.SEQ</ta>
            <ta e="T797" id="Seg_7560" s="T796">throw-PRS.[3SG]</ta>
            <ta e="T798" id="Seg_7561" s="T797">pull-CVB.SEQ</ta>
            <ta e="T799" id="Seg_7562" s="T798">only</ta>
            <ta e="T800" id="Seg_7563" s="T799">throw-PST2-3SG</ta>
            <ta e="T801" id="Seg_7564" s="T800">house-3SG.[NOM]-fire-3SG.[NOM]</ta>
            <ta e="T802" id="Seg_7565" s="T801">whereto</ta>
            <ta e="T803" id="Seg_7566" s="T802">NEG</ta>
            <ta e="T804" id="Seg_7567" s="T803">go-PTCP.PST-3PL-ACC</ta>
            <ta e="T805" id="Seg_7568" s="T804">notice-NEG-PST1-3PL</ta>
            <ta e="T806" id="Seg_7569" s="T805">snowstorm.[NOM]</ta>
            <ta e="T807" id="Seg_7570" s="T806">master-PL-3SG.[NOM]</ta>
            <ta e="T808" id="Seg_7571" s="T807">be.offended-CVB.SEQ</ta>
            <ta e="T809" id="Seg_7572" s="T808">this</ta>
            <ta e="T810" id="Seg_7573" s="T809">house-ACC</ta>
            <ta e="T811" id="Seg_7574" s="T810">Bony.Belt-ACC</ta>
            <ta e="T812" id="Seg_7575" s="T811">mother-COM-ACC-father-COM-ACC</ta>
            <ta e="T813" id="Seg_7576" s="T812">run-CVB.SEQ</ta>
            <ta e="T814" id="Seg_7577" s="T813">whirlwind-INSTR</ta>
            <ta e="T815" id="Seg_7578" s="T814">go-PST2-3PL</ta>
            <ta e="T816" id="Seg_7579" s="T815">only</ta>
            <ta e="T817" id="Seg_7580" s="T816">Iron.Cap.[NOM]</ta>
            <ta e="T818" id="Seg_7581" s="T817">mother-COM-father-COM</ta>
            <ta e="T819" id="Seg_7582" s="T818">stay-PST2.[3SG]</ta>
            <ta e="T820" id="Seg_7583" s="T819">snowstorm.[NOM]</ta>
            <ta e="T821" id="Seg_7584" s="T820">daughter-3SG-ACC</ta>
            <ta e="T822" id="Seg_7585" s="T821">woman-VBZ-CVB.SEQ</ta>
            <ta e="T823" id="Seg_7586" s="T822">be.rich-CVB.SEQ-be.rich-CVB.SEQ</ta>
            <ta e="T824" id="Seg_7587" s="T823">live-PST2-3SG</ta>
            <ta e="T825" id="Seg_7588" s="T824">it.is.said</ta>
            <ta e="T826" id="Seg_7589" s="T825">last-3SG.[NOM]</ta>
         </annotation>
         <annotation name="gg" tierref="gg">
            <ta e="T1" id="Seg_7590" s="T0">Eisenmütze.[NOM]</ta>
            <ta e="T2" id="Seg_7591" s="T1">jenes-DAT/LOC</ta>
            <ta e="T3" id="Seg_7592" s="T2">Knochengürtel.[NOM]</ta>
            <ta e="T4" id="Seg_7593" s="T3">sagen-CVB.SEQ</ta>
            <ta e="T5" id="Seg_7594" s="T4">zwei</ta>
            <ta e="T6" id="Seg_7595" s="T5">Junge.[NOM]</ta>
            <ta e="T7" id="Seg_7596" s="T6">Mensch-PL.[NOM]</ta>
            <ta e="T8" id="Seg_7597" s="T7">leben-PST2-3PL</ta>
            <ta e="T9" id="Seg_7598" s="T8">zwei-COLL.[NOM]</ta>
            <ta e="T10" id="Seg_7599" s="T9">Vater-PROPR-3PL</ta>
            <ta e="T11" id="Seg_7600" s="T10">Mutter-PROPR-3PL</ta>
            <ta e="T12" id="Seg_7601" s="T11">doch</ta>
            <ta e="T13" id="Seg_7602" s="T12">dieses</ta>
            <ta e="T14" id="Seg_7603" s="T13">leben-CVB.SEQ-3PL</ta>
            <ta e="T15" id="Seg_7604" s="T14">Knochengürtel-3SG.[NOM]</ta>
            <ta e="T16" id="Seg_7605" s="T15">sagen-PRS.[3SG]</ta>
            <ta e="T17" id="Seg_7606" s="T16">Eisenmütze-3SG-ACC</ta>
            <ta e="T18" id="Seg_7607" s="T17">Freund</ta>
            <ta e="T19" id="Seg_7608" s="T18">dieses</ta>
            <ta e="T20" id="Seg_7609" s="T19">1PL.[NOM]</ta>
            <ta e="T21" id="Seg_7610" s="T20">was.[NOM]</ta>
            <ta e="T22" id="Seg_7611" s="T21">Fähigkeit-PROPR.[NOM]</ta>
            <ta e="T23" id="Seg_7612" s="T22">sein-PRS-1PL</ta>
            <ta e="T24" id="Seg_7613" s="T23">warum</ta>
            <ta e="T25" id="Seg_7614" s="T24">Fähigkeit.[NOM]</ta>
            <ta e="T26" id="Seg_7615" s="T25">Fähigkeit-1PL-ACC</ta>
            <ta e="T27" id="Seg_7616" s="T26">erfahren-RECP/COLL-EP-NEG-1PL</ta>
            <ta e="T28" id="Seg_7617" s="T27">Sicherheit.[NOM]</ta>
            <ta e="T29" id="Seg_7618" s="T28">wegnehmen-EP-RECP/COLL-CVB.PURP</ta>
            <ta e="T30" id="Seg_7619" s="T29">wollen-PRS.[3SG]</ta>
            <ta e="T31" id="Seg_7620" s="T30">offenbar</ta>
            <ta e="T32" id="Seg_7621" s="T31">Freund-3SG.[NOM]</ta>
            <ta e="T33" id="Seg_7622" s="T32">hey</ta>
            <ta e="T34" id="Seg_7623" s="T33">Freund.[NOM]</ta>
            <ta e="T35" id="Seg_7624" s="T34">was-1PL-ACC</ta>
            <ta e="T36" id="Seg_7625" s="T35">wegnehmen-EP-RECP/COLL-CVB.SEQ</ta>
            <ta e="T37" id="Seg_7626" s="T36">Sicherheit.[NOM]</ta>
            <ta e="T38" id="Seg_7627" s="T37">wegnehmen-EP-RECP/COLL-FUT-1PL=Q</ta>
            <ta e="T39" id="Seg_7628" s="T38">sagen-CVB.SEQ</ta>
            <ta e="T40" id="Seg_7629" s="T39">sein-EP-MED-NEG.[3SG]</ta>
            <ta e="T41" id="Seg_7630" s="T40">Freund-3SG.[NOM]</ta>
            <ta e="T42" id="Seg_7631" s="T41">aushalten-CAUS-NEG.[3SG]</ta>
            <ta e="T43" id="Seg_7632" s="T42">warum</ta>
            <ta e="T44" id="Seg_7633" s="T43">Freund.[NOM]</ta>
            <ta e="T45" id="Seg_7634" s="T44">2SG.[NOM]</ta>
            <ta e="T46" id="Seg_7635" s="T45">Fähigkeit-2SG-ACC</ta>
            <ta e="T47" id="Seg_7636" s="T46">erfahren-PTCP.FUT-1SG-ACC</ta>
            <ta e="T48" id="Seg_7637" s="T47">wollen-PRS-1SG</ta>
            <ta e="T49" id="Seg_7638" s="T48">Freund-3SG.[NOM]</ta>
            <ta e="T50" id="Seg_7639" s="T49">sein-EP-MED-NEG.[3SG]</ta>
            <ta e="T51" id="Seg_7640" s="T50">Knochengürtel.[NOM]</ta>
            <ta e="T52" id="Seg_7641" s="T51">1SG.[NOM]</ta>
            <ta e="T53" id="Seg_7642" s="T52">entfliehen-FUT-1SG</ta>
            <ta e="T54" id="Seg_7643" s="T53">2SG-ABL</ta>
            <ta e="T55" id="Seg_7644" s="T54">folgen.[IMP.2SG]</ta>
            <ta e="T56" id="Seg_7645" s="T55">1SG-ACC</ta>
            <ta e="T57" id="Seg_7646" s="T56">1SG.[NOM]</ta>
            <ta e="T58" id="Seg_7647" s="T57">wie</ta>
            <ta e="T59" id="Seg_7648" s="T58">sein-PRS-1SG</ta>
            <ta e="T60" id="Seg_7649" s="T59">und</ta>
            <ta e="T61" id="Seg_7650" s="T60">so-INTNS</ta>
            <ta e="T62" id="Seg_7651" s="T61">sein-CVB.SEQ</ta>
            <ta e="T63" id="Seg_7652" s="T62">gehen.[IMP.2SG]</ta>
            <ta e="T64" id="Seg_7653" s="T63">sagen-PRS.[3SG]</ta>
            <ta e="T65" id="Seg_7654" s="T64">Freund-3SG.[NOM]</ta>
            <ta e="T66" id="Seg_7655" s="T65">folgen-FUT2-1SG</ta>
            <ta e="T67" id="Seg_7656" s="T66">Q</ta>
            <ta e="T68" id="Seg_7657" s="T67">besser</ta>
            <ta e="T69" id="Seg_7658" s="T68">am.Morgen</ta>
            <ta e="T70" id="Seg_7659" s="T69">früh-AG.[NOM]</ta>
            <ta e="T71" id="Seg_7660" s="T70">Mensch.[NOM]</ta>
            <ta e="T72" id="Seg_7661" s="T71">aufstehen-PTCP.PRS</ta>
            <ta e="T73" id="Seg_7662" s="T72">Zeit-3SG-DAT/LOC</ta>
            <ta e="T74" id="Seg_7663" s="T73">folgen-FUT.[IMP.2SG]</ta>
            <ta e="T75" id="Seg_7664" s="T74">1SG-ACC</ta>
            <ta e="T76" id="Seg_7665" s="T75">sagen-PRS.[3SG]</ta>
            <ta e="T77" id="Seg_7666" s="T76">Knochengürtel.[NOM]</ta>
            <ta e="T78" id="Seg_7667" s="T77">so</ta>
            <ta e="T79" id="Seg_7668" s="T78">sagen-EP-RECP/COLL-CVB.SEQ</ta>
            <ta e="T80" id="Seg_7669" s="T79">sich.trennen-PRS-3PL</ta>
            <ta e="T81" id="Seg_7670" s="T80">Haus.[NOM]</ta>
            <ta e="T82" id="Seg_7671" s="T81">Haus-3PL-DAT/LOC</ta>
            <ta e="T83" id="Seg_7672" s="T82">Eisenmütze.[NOM]</ta>
            <ta e="T84" id="Seg_7673" s="T83">hinausgehen-CVB.SEQ</ta>
            <ta e="T85" id="Seg_7674" s="T84">schlafen-CVB.SIM</ta>
            <ta e="T86" id="Seg_7675" s="T85">sich.hinlegen-PRS.[3SG]</ta>
            <ta e="T87" id="Seg_7676" s="T86">aufwachen-CVB.SEQ</ta>
            <ta e="T88" id="Seg_7677" s="T87">kommen-PRS.[3SG]</ta>
            <ta e="T89" id="Seg_7678" s="T88">Freund-EP-1SG.[NOM]</ta>
            <ta e="T90" id="Seg_7679" s="T89">es.gibt-3SG</ta>
            <ta e="T91" id="Seg_7680" s="T90">Q</ta>
            <ta e="T92" id="Seg_7681" s="T91">Freund-3SG-DAT/LOC</ta>
            <ta e="T93" id="Seg_7682" s="T92">kommen-PST2-3SG</ta>
            <ta e="T94" id="Seg_7683" s="T93">Knochengürtel.[NOM]</ta>
            <ta e="T95" id="Seg_7684" s="T94">Bett-3SG.[NOM]</ta>
            <ta e="T96" id="Seg_7685" s="T95">leer.sein-CVB.SIM</ta>
            <ta e="T97" id="Seg_7686" s="T96">liegen-PRS.[3SG]</ta>
            <ta e="T98" id="Seg_7687" s="T97">gehen-CVB.SEQ</ta>
            <ta e="T99" id="Seg_7688" s="T98">bleiben-PST2.[3SG]</ta>
            <ta e="T100" id="Seg_7689" s="T99">Eisenmütze.[NOM]</ta>
            <ta e="T101" id="Seg_7690" s="T100">Spur.[NOM]</ta>
            <ta e="T102" id="Seg_7691" s="T101">verfolgen-PRS.[3SG]</ta>
            <ta e="T103" id="Seg_7692" s="T102">Spur-ACC</ta>
            <ta e="T104" id="Seg_7693" s="T103">finden-NEG.[3SG]</ta>
            <ta e="T105" id="Seg_7694" s="T104">letzter-3SG.[NOM]</ta>
            <ta e="T106" id="Seg_7695" s="T105">sein-PRS.[3SG]</ta>
            <ta e="T107" id="Seg_7696" s="T106">hineingehen-CVB.SEQ</ta>
            <ta e="T108" id="Seg_7697" s="T107">Bett-3SG-ACC</ta>
            <ta e="T109" id="Seg_7698" s="T108">direkt-VBZ-PRS.[3SG]</ta>
            <ta e="T110" id="Seg_7699" s="T109">Knochengürtel.[NOM]</ta>
            <ta e="T111" id="Seg_7700" s="T110">Bett-3SG-GEN</ta>
            <ta e="T112" id="Seg_7701" s="T111">Unterteil-3SG-ABL</ta>
            <ta e="T113" id="Seg_7702" s="T112">spitzmäulig</ta>
            <ta e="T114" id="Seg_7703" s="T113">Maus-3SG-GEN</ta>
            <ta e="T115" id="Seg_7704" s="T114">Höhle-3SG.[NOM]</ta>
            <ta e="T116" id="Seg_7705" s="T115">leer.sein-CVB.SIM</ta>
            <ta e="T117" id="Seg_7706" s="T116">liegen-PRS.[3SG]</ta>
            <ta e="T118" id="Seg_7707" s="T117">anders</ta>
            <ta e="T119" id="Seg_7708" s="T118">Spur.[NOM]</ta>
            <ta e="T120" id="Seg_7709" s="T119">NEG.EX</ta>
            <ta e="T121" id="Seg_7710" s="T120">Eisenmütze.[NOM]</ta>
            <ta e="T122" id="Seg_7711" s="T121">Maus.[NOM]</ta>
            <ta e="T123" id="Seg_7712" s="T122">werden-PST1-3SG</ta>
            <ta e="T124" id="Seg_7713" s="T123">und</ta>
            <ta e="T125" id="Seg_7714" s="T124">Höhle-3SG-INSTR</ta>
            <ta e="T126" id="Seg_7715" s="T125">hinuntergehen-PST1-3SG</ta>
            <ta e="T127" id="Seg_7716" s="T126">entfernt-ADJZ-3SG.[NOM]</ta>
            <ta e="T128" id="Seg_7717" s="T127">Höhle-ABL</ta>
            <ta e="T129" id="Seg_7718" s="T128">hinausgehen-CVB.ANT</ta>
            <ta e="T130" id="Seg_7719" s="T129">Hermelin.[NOM]</ta>
            <ta e="T131" id="Seg_7720" s="T130">werden-CVB.ANT</ta>
            <ta e="T132" id="Seg_7721" s="T131">springen-CVB.SEQ</ta>
            <ta e="T133" id="Seg_7722" s="T132">bleiben-PRS.[3SG]</ta>
            <ta e="T134" id="Seg_7723" s="T133">Eisenmütze.[NOM]</ta>
            <ta e="T135" id="Seg_7724" s="T134">Hermelin.[NOM]</ta>
            <ta e="T136" id="Seg_7725" s="T135">sein-CVB.ANT</ta>
            <ta e="T137" id="Seg_7726" s="T136">folgen-CVB.SEQ</ta>
            <ta e="T138" id="Seg_7727" s="T137">gehen-PRS.[3SG]</ta>
            <ta e="T139" id="Seg_7728" s="T138">uff</ta>
            <ta e="T140" id="Seg_7729" s="T139">rennen-NMNZ.[NOM]</ta>
            <ta e="T141" id="Seg_7730" s="T140">gehen-CVB.SEQ</ta>
            <ta e="T142" id="Seg_7731" s="T141">gehen-PST1-3SG</ta>
            <ta e="T143" id="Seg_7732" s="T142">dieses</ta>
            <ta e="T144" id="Seg_7733" s="T143">Hermelin.[NOM]</ta>
            <ta e="T145" id="Seg_7734" s="T144">Spur-3SG.[NOM]</ta>
            <ta e="T146" id="Seg_7735" s="T145">aufhören-PRS.[3SG]</ta>
            <ta e="T147" id="Seg_7736" s="T146">und</ta>
            <ta e="T148" id="Seg_7737" s="T147">mittlerer</ta>
            <ta e="T149" id="Seg_7738" s="T148">Welt.[NOM]</ta>
            <ta e="T150" id="Seg_7739" s="T149">Polarfuchs.[NOM]</ta>
            <ta e="T151" id="Seg_7740" s="T150">Spur-3SG.[NOM]</ta>
            <ta e="T152" id="Seg_7741" s="T151">gehen-CVB.SIM</ta>
            <ta e="T153" id="Seg_7742" s="T152">stehen-PST1-3SG</ta>
            <ta e="T154" id="Seg_7743" s="T153">Freund-3SG.[NOM]</ta>
            <ta e="T155" id="Seg_7744" s="T154">Angst.haben-CVB.SIM</ta>
            <ta e="T156" id="Seg_7745" s="T155">gehen-PST1-3SG</ta>
            <ta e="T157" id="Seg_7746" s="T156">Eisenmütze.[NOM]</ta>
            <ta e="T158" id="Seg_7747" s="T157">Polarfuchs.[NOM]</ta>
            <ta e="T159" id="Seg_7748" s="T158">werden-PRS.[3SG]</ta>
            <ta e="T160" id="Seg_7749" s="T159">und</ta>
            <ta e="T161" id="Seg_7750" s="T160">springen-CVB.SEQ</ta>
            <ta e="T162" id="Seg_7751" s="T161">gehen-PST1-3SG</ta>
            <ta e="T163" id="Seg_7752" s="T162">eins</ta>
            <ta e="T164" id="Seg_7753" s="T163">Ort-DAT/LOC</ta>
            <ta e="T165" id="Seg_7754" s="T164">fallen-PST2.[3SG]</ta>
            <ta e="T166" id="Seg_7755" s="T165">und</ta>
            <ta e="T167" id="Seg_7756" s="T166">Knochengürtel-3SG.[NOM]</ta>
            <ta e="T168" id="Seg_7757" s="T167">Hase.[NOM]</ta>
            <ta e="T169" id="Seg_7758" s="T168">werden-CVB.SEQ</ta>
            <ta e="T170" id="Seg_7759" s="T169">springen-PST2.[3SG]</ta>
            <ta e="T171" id="Seg_7760" s="T170">Hinterteil-ADJZ-3SG.[NOM]</ta>
            <ta e="T172" id="Seg_7761" s="T171">Hase.[NOM]</ta>
            <ta e="T173" id="Seg_7762" s="T172">werden-CVB.SEQ</ta>
            <ta e="T174" id="Seg_7763" s="T173">folgen-PST1-3SG</ta>
            <ta e="T175" id="Seg_7764" s="T174">Knochengürtel.[NOM]</ta>
            <ta e="T176" id="Seg_7765" s="T175">Fuchs.[NOM]</ta>
            <ta e="T177" id="Seg_7766" s="T176">werden-PST1-3SG</ta>
            <ta e="T178" id="Seg_7767" s="T177">dann</ta>
            <ta e="T179" id="Seg_7768" s="T178">Hinterteil-ADJZ-3SG.[NOM]</ta>
            <ta e="T180" id="Seg_7769" s="T179">auch</ta>
            <ta e="T181" id="Seg_7770" s="T180">Fuchs.[NOM]</ta>
            <ta e="T182" id="Seg_7771" s="T181">werden-CVB.SEQ</ta>
            <ta e="T183" id="Seg_7772" s="T182">Weg-3SG-ACC</ta>
            <ta e="T184" id="Seg_7773" s="T183">nur</ta>
            <ta e="T185" id="Seg_7774" s="T184">nachmachen-CVB.SEQ</ta>
            <ta e="T186" id="Seg_7775" s="T185">gehen-PST1-3SG</ta>
            <ta e="T187" id="Seg_7776" s="T186">dieses-3SG.[NOM]</ta>
            <ta e="T188" id="Seg_7777" s="T187">Wolf.[NOM]</ta>
            <ta e="T189" id="Seg_7778" s="T188">sein-CVB.SEQ</ta>
            <ta e="T190" id="Seg_7779" s="T189">rennen-CVB.SEQ</ta>
            <ta e="T191" id="Seg_7780" s="T190">größer.werden-PST2.[3SG]</ta>
            <ta e="T192" id="Seg_7781" s="T191">Hinterteil-ADJZ-3SG.[NOM]</ta>
            <ta e="T193" id="Seg_7782" s="T192">sich.wundern-CVB.SIM</ta>
            <ta e="T194" id="Seg_7783" s="T193">gehen-PST1-3SG</ta>
            <ta e="T195" id="Seg_7784" s="T194">dieses-3SG-ADJZ-3SG.[NOM]</ta>
            <ta e="T196" id="Seg_7785" s="T195">zurecht.kommen-EP-NEG.CVB</ta>
            <ta e="T197" id="Seg_7786" s="T196">bitten-EP-MED-PST2.[3SG]</ta>
            <ta e="T198" id="Seg_7787" s="T197">MOD</ta>
            <ta e="T199" id="Seg_7788" s="T198">denken-PRS.[3SG]</ta>
            <ta e="T200" id="Seg_7789" s="T199">Wolf.[NOM]</ta>
            <ta e="T201" id="Seg_7790" s="T200">werden-CVB.SEQ</ta>
            <ta e="T202" id="Seg_7791" s="T201">folgen-CVB.SEQ</ta>
            <ta e="T203" id="Seg_7792" s="T202">gehen-PST1-3SG</ta>
            <ta e="T204" id="Seg_7793" s="T203">wie.viel-ACC-wie.viel-ACC</ta>
            <ta e="T205" id="Seg_7794" s="T204">gehen-PST2-3SG</ta>
            <ta e="T206" id="Seg_7795" s="T205">wissen-PASS/REFL-EP-NEG.[3SG]</ta>
            <ta e="T207" id="Seg_7796" s="T206">Knochengürtel.[NOM]</ta>
            <ta e="T208" id="Seg_7797" s="T207">Vielfraß.[NOM]</ta>
            <ta e="T209" id="Seg_7798" s="T208">werden-CVB.SEQ</ta>
            <ta e="T210" id="Seg_7799" s="T209">traben-CVB.SIM</ta>
            <ta e="T211" id="Seg_7800" s="T210">stehen-PST2.[3SG]</ta>
            <ta e="T212" id="Seg_7801" s="T211">Hinterteil-ADJZ-3SG.[NOM]</ta>
            <ta e="T213" id="Seg_7802" s="T212">wieder</ta>
            <ta e="T214" id="Seg_7803" s="T213">immer</ta>
            <ta e="T215" id="Seg_7804" s="T214">sich.wundern-CVB.SIM-sich.wundern-CVB.SIM</ta>
            <ta e="T216" id="Seg_7805" s="T215">Vielfraß.[NOM]</ta>
            <ta e="T217" id="Seg_7806" s="T216">werden-CVB.SEQ</ta>
            <ta e="T218" id="Seg_7807" s="T217">folgen-CVB.SEQ</ta>
            <ta e="T219" id="Seg_7808" s="T218">gehen-PST1-3SG</ta>
            <ta e="T220" id="Seg_7809" s="T219">Vielfraß-3SG-GEN</ta>
            <ta e="T221" id="Seg_7810" s="T220">Spur-3SG.[NOM]</ta>
            <ta e="T222" id="Seg_7811" s="T221">aufhören-CVB.ANT-3SG</ta>
            <ta e="T223" id="Seg_7812" s="T222">Bär.[NOM]</ta>
            <ta e="T224" id="Seg_7813" s="T223">werden-CVB.SEQ</ta>
            <ta e="T225" id="Seg_7814" s="T224">traben-PST2.[3SG]</ta>
            <ta e="T226" id="Seg_7815" s="T225">Bär.[NOM]</ta>
            <ta e="T227" id="Seg_7816" s="T226">werden-CVB.SEQ</ta>
            <ta e="T228" id="Seg_7817" s="T227">folgen-CVB.SEQ</ta>
            <ta e="T229" id="Seg_7818" s="T228">gehen-PST1-3SG</ta>
            <ta e="T230" id="Seg_7819" s="T229">Eisenmütze.[NOM]</ta>
            <ta e="T231" id="Seg_7820" s="T230">dieses.[NOM]</ta>
            <ta e="T232" id="Seg_7821" s="T231">solange</ta>
            <ta e="T233" id="Seg_7822" s="T232">uff</ta>
            <ta e="T234" id="Seg_7823" s="T233">rennen-NMNZ.[NOM]</ta>
            <ta e="T235" id="Seg_7824" s="T234">Weg.[NOM]</ta>
            <ta e="T236" id="Seg_7825" s="T235">Bär.[NOM]</ta>
            <ta e="T237" id="Seg_7826" s="T236">Spur-3SG.[NOM]</ta>
            <ta e="T238" id="Seg_7827" s="T237">enden-PRS.[3SG]</ta>
            <ta e="T239" id="Seg_7828" s="T238">und</ta>
            <ta e="T240" id="Seg_7829" s="T239">wildes.Rentier</ta>
            <ta e="T241" id="Seg_7830" s="T240">Rentierbulle-3SG.[NOM]</ta>
            <ta e="T242" id="Seg_7831" s="T241">werden-CVB.SEQ</ta>
            <ta e="T243" id="Seg_7832" s="T242">springen-CVB.SEQ</ta>
            <ta e="T244" id="Seg_7833" s="T243">bleiben-FREQ-PST2.[3SG]</ta>
            <ta e="T245" id="Seg_7834" s="T244">sich.wundern-CVB.SIM</ta>
            <ta e="T246" id="Seg_7835" s="T245">nur</ta>
            <ta e="T247" id="Seg_7836" s="T246">gehen-PRS.[3SG]</ta>
            <ta e="T248" id="Seg_7837" s="T247">immer</ta>
            <ta e="T249" id="Seg_7838" s="T248">folgen-PRS.[3SG]</ta>
            <ta e="T250" id="Seg_7839" s="T249">Freund-3SG.[NOM]</ta>
            <ta e="T251" id="Seg_7840" s="T250">Eisenmütze.[NOM]</ta>
            <ta e="T252" id="Seg_7841" s="T251">wildes.Rentier.[NOM]</ta>
            <ta e="T253" id="Seg_7842" s="T252">Rentierbulle-3SG.[NOM]</ta>
            <ta e="T254" id="Seg_7843" s="T253">werden-PST1-3SG</ta>
            <ta e="T255" id="Seg_7844" s="T254">Fähigkeit-2SG.[NOM]</ta>
            <ta e="T256" id="Seg_7845" s="T255">und</ta>
            <ta e="T257" id="Seg_7846" s="T256">letzter-3SG.[NOM]</ta>
            <ta e="T258" id="Seg_7847" s="T257">MOD</ta>
            <ta e="T259" id="Seg_7848" s="T258">verfolgen-CVB.SIM</ta>
            <ta e="T260" id="Seg_7849" s="T259">MOD-1SG</ta>
            <ta e="T261" id="Seg_7850" s="T260">dieses.EMPH-INTNS</ta>
            <ta e="T262" id="Seg_7851" s="T261">denken-CVB.SIM</ta>
            <ta e="T263" id="Seg_7852" s="T262">denken-PRS.[3SG]</ta>
            <ta e="T264" id="Seg_7853" s="T263">sehen-PST2-3SG</ta>
            <ta e="T265" id="Seg_7854" s="T264">eins</ta>
            <ta e="T266" id="Seg_7855" s="T265">Ort-DAT/LOC</ta>
            <ta e="T267" id="Seg_7856" s="T266">Freund-3SG.[NOM]</ta>
            <ta e="T268" id="Seg_7857" s="T267">schwanken-CVB.SEQ</ta>
            <ta e="T269" id="Seg_7858" s="T268">stehen-PRS.[3SG]</ta>
            <ta e="T270" id="Seg_7859" s="T269">sich.nähern-CVB.SEQ</ta>
            <ta e="T271" id="Seg_7860" s="T270">sehen-PST2-3SG</ta>
            <ta e="T272" id="Seg_7861" s="T271">zwei</ta>
            <ta e="T273" id="Seg_7862" s="T272">plötzlich</ta>
            <ta e="T274" id="Seg_7863" s="T273">Bein-3SG.[NOM]</ta>
            <ta e="T275" id="Seg_7864" s="T274">abstehen-CVB.SEQ</ta>
            <ta e="T276" id="Seg_7865" s="T275">bleiben-PST2.[3SG]</ta>
            <ta e="T277" id="Seg_7866" s="T276">Gelenk-PL-3SG-INSTR</ta>
            <ta e="T278" id="Seg_7867" s="T277">schneiden-CVB.SEQ</ta>
            <ta e="T279" id="Seg_7868" s="T278">Gerippe.[NOM]</ta>
            <ta e="T280" id="Seg_7869" s="T279">Bein-PL-3SG-ACC</ta>
            <ta e="T281" id="Seg_7870" s="T280">rollen-CAUS-CVB.SEQ</ta>
            <ta e="T282" id="Seg_7871" s="T281">werfen-PST2.[3SG]</ta>
            <ta e="T283" id="Seg_7872" s="T282">jenes.EMPH.[NOM]</ta>
            <ta e="T284" id="Seg_7873" s="T283">wie-INTNS</ta>
            <ta e="T285" id="Seg_7874" s="T284">Eisenmütze.[NOM]</ta>
            <ta e="T286" id="Seg_7875" s="T285">Bein-3SG-ACC</ta>
            <ta e="T287" id="Seg_7876" s="T286">schneiden-CVB.ANT</ta>
            <ta e="T288" id="Seg_7877" s="T287">Gerippe.[NOM]</ta>
            <ta e="T289" id="Seg_7878" s="T288">werden-CVB.SEQ</ta>
            <ta e="T290" id="Seg_7879" s="T289">rollen-CVB.SEQ</ta>
            <ta e="T291" id="Seg_7880" s="T290">gehen-PST1-3SG</ta>
            <ta e="T292" id="Seg_7881" s="T291">wie.viel</ta>
            <ta e="T293" id="Seg_7882" s="T292">NEG</ta>
            <ta e="T294" id="Seg_7883" s="T293">sein-EP-NEG.CVB</ta>
            <ta e="T295" id="Seg_7884" s="T294">Gerippe.[NOM]</ta>
            <ta e="T296" id="Seg_7885" s="T295">sich.auftürmen-CVB.SIM</ta>
            <ta e="T297" id="Seg_7886" s="T296">liegen-PRS.[3SG]</ta>
            <ta e="T298" id="Seg_7887" s="T297">sich.freuen-PRS.[3SG]</ta>
            <ta e="T299" id="Seg_7888" s="T298">erreichen-PST1-1SG</ta>
            <ta e="T300" id="Seg_7889" s="T299">EMPH</ta>
            <ta e="T301" id="Seg_7890" s="T300">sagen-PRS.[3SG]</ta>
            <ta e="T302" id="Seg_7891" s="T301">dieses</ta>
            <ta e="T303" id="Seg_7892" s="T302">Gerippe-DAT/LOC</ta>
            <ta e="T304" id="Seg_7893" s="T303">ankommen-PST2-3SG</ta>
            <ta e="T305" id="Seg_7894" s="T304">Gerippe.[NOM]</ta>
            <ta e="T306" id="Seg_7895" s="T305">leer.[NOM]</ta>
            <ta e="T307" id="Seg_7896" s="T306">nur</ta>
            <ta e="T308" id="Seg_7897" s="T307">liegen-PRS.[3SG]</ta>
            <ta e="T309" id="Seg_7898" s="T308">Kopf-3SG.[NOM]</ta>
            <ta e="T310" id="Seg_7899" s="T309">nur</ta>
            <ta e="T311" id="Seg_7900" s="T310">rollen-CVB.SEQ</ta>
            <ta e="T312" id="Seg_7901" s="T311">bleiben-PST2.[3SG]</ta>
            <ta e="T313" id="Seg_7902" s="T312">na</ta>
            <ta e="T314" id="Seg_7903" s="T313">nun</ta>
            <ta e="T315" id="Seg_7904" s="T314">dieses</ta>
            <ta e="T316" id="Seg_7905" s="T315">warum</ta>
            <ta e="T317" id="Seg_7906" s="T316">zerschneiden-EP-REFL-PRS.[3SG]</ta>
            <ta e="T318" id="Seg_7907" s="T317">selbst-3SG-ACC</ta>
            <ta e="T319" id="Seg_7908" s="T318">denken-CVB.SEQ</ta>
            <ta e="T320" id="Seg_7909" s="T319">denken-PRS.[3SG]</ta>
            <ta e="T321" id="Seg_7910" s="T320">so-INTNS</ta>
            <ta e="T322" id="Seg_7911" s="T321">was.[NOM]</ta>
            <ta e="T323" id="Seg_7912" s="T322">Wettbewerb-3SG.[NOM]=Q</ta>
            <ta e="T324" id="Seg_7913" s="T323">denken-CVB.SEQ</ta>
            <ta e="T325" id="Seg_7914" s="T324">sich.wundern-CVB.SIM</ta>
            <ta e="T326" id="Seg_7915" s="T325">gehen-PST1-3SG</ta>
            <ta e="T327" id="Seg_7916" s="T326">Kopf.[NOM]</ta>
            <ta e="T328" id="Seg_7917" s="T327">werden-CVB.SEQ</ta>
            <ta e="T329" id="Seg_7918" s="T328">rollen-CVB.SEQ</ta>
            <ta e="T330" id="Seg_7919" s="T329">gehen-PST1-3SG</ta>
            <ta e="T331" id="Seg_7920" s="T330">nur</ta>
            <ta e="T332" id="Seg_7921" s="T331">Kopf.[NOM]</ta>
            <ta e="T333" id="Seg_7922" s="T332">starr.liegen-CVB.SIM</ta>
            <ta e="T334" id="Seg_7923" s="T333">liegen-PRS.[3SG]</ta>
            <ta e="T335" id="Seg_7924" s="T334">doch</ta>
            <ta e="T336" id="Seg_7925" s="T335">erreichen-PST1-1SG</ta>
            <ta e="T337" id="Seg_7926" s="T336">MOD</ta>
            <ta e="T338" id="Seg_7927" s="T337">plötzlich</ta>
            <ta e="T339" id="Seg_7928" s="T338">zwei</ta>
            <ta e="T340" id="Seg_7929" s="T339">Auge-3SG.[NOM]</ta>
            <ta e="T341" id="Seg_7930" s="T340">ausziehen-REFL-CVB.SEQ</ta>
            <ta e="T342" id="Seg_7931" s="T341">gehen-PST2.[3SG]</ta>
            <ta e="T343" id="Seg_7932" s="T342">oh.nein</ta>
            <ta e="T344" id="Seg_7933" s="T343">dieses</ta>
            <ta e="T345" id="Seg_7934" s="T344">was.[NOM]</ta>
            <ta e="T346" id="Seg_7935" s="T345">Mensch-3SG.[NOM]=Q</ta>
            <ta e="T347" id="Seg_7936" s="T346">guter.Geist-ACC</ta>
            <ta e="T348" id="Seg_7937" s="T347">folgen-PRS-1SG</ta>
            <ta e="T349" id="Seg_7938" s="T348">Q</ta>
            <ta e="T350" id="Seg_7939" s="T349">böser.Geist-ACC</ta>
            <ta e="T351" id="Seg_7940" s="T350">Q</ta>
            <ta e="T352" id="Seg_7941" s="T351">sagen-PRS.[3SG]</ta>
            <ta e="T353" id="Seg_7942" s="T352">zwei</ta>
            <ta e="T354" id="Seg_7943" s="T353">Auge-3SG.[NOM]</ta>
            <ta e="T355" id="Seg_7944" s="T354">rollen-CVB.SEQ</ta>
            <ta e="T356" id="Seg_7945" s="T355">gehen-PST1-3SG</ta>
            <ta e="T357" id="Seg_7946" s="T356">Auge-PL.[NOM]</ta>
            <ta e="T358" id="Seg_7947" s="T357">Weg-3PL-INSTR</ta>
            <ta e="T359" id="Seg_7948" s="T358">wann</ta>
            <ta e="T360" id="Seg_7949" s="T359">INDEF</ta>
            <ta e="T361" id="Seg_7950" s="T360">dieses</ta>
            <ta e="T362" id="Seg_7951" s="T361">zwei</ta>
            <ta e="T363" id="Seg_7952" s="T362">Auge.[NOM]</ta>
            <ta e="T364" id="Seg_7953" s="T363">Spur-3SG.[NOM]</ta>
            <ta e="T365" id="Seg_7954" s="T364">eins</ta>
            <ta e="T366" id="Seg_7955" s="T365">Haus-DIM-DAT/LOC</ta>
            <ta e="T367" id="Seg_7956" s="T366">ankommen-PRS.[3SG]</ta>
            <ta e="T368" id="Seg_7957" s="T367">dieses</ta>
            <ta e="T369" id="Seg_7958" s="T368">ankommen-CVB.SEQ</ta>
            <ta e="T370" id="Seg_7959" s="T369">zwei</ta>
            <ta e="T371" id="Seg_7960" s="T370">Auge.[NOM]</ta>
            <ta e="T372" id="Seg_7961" s="T371">Spur-3SG.[NOM]</ta>
            <ta e="T373" id="Seg_7962" s="T372">Haus-DAT/LOC</ta>
            <ta e="T374" id="Seg_7963" s="T373">hineingehen-PST2.[3SG]</ta>
            <ta e="T375" id="Seg_7964" s="T374">doch</ta>
            <ta e="T376" id="Seg_7965" s="T375">letzter-2SG.[NOM]</ta>
            <ta e="T377" id="Seg_7966" s="T376">MOD</ta>
            <ta e="T378" id="Seg_7967" s="T377">zwei</ta>
            <ta e="T379" id="Seg_7968" s="T378">hinterer</ta>
            <ta e="T380" id="Seg_7969" s="T379">Auge.[NOM]</ta>
            <ta e="T381" id="Seg_7970" s="T380">Haus-DAT/LOC</ta>
            <ta e="T382" id="Seg_7971" s="T381">hineingehen-PST1-3SG</ta>
            <ta e="T383" id="Seg_7972" s="T382">hineingehen-PST2-3SG</ta>
            <ta e="T384" id="Seg_7973" s="T383">rechter</ta>
            <ta e="T385" id="Seg_7974" s="T384">in.Richtung</ta>
            <ta e="T386" id="Seg_7975" s="T385">Bett-DAT/LOC</ta>
            <ta e="T387" id="Seg_7976" s="T386">Alte.[NOM]</ta>
            <ta e="T388" id="Seg_7977" s="T387">sitzen-PRS.[3SG]</ta>
            <ta e="T389" id="Seg_7978" s="T388">Auge-3PL.[NOM]</ta>
            <ta e="T390" id="Seg_7979" s="T389">hinterer.Teil.der.Jurte.[NOM]</ta>
            <ta e="T391" id="Seg_7980" s="T390">funkeln-CVB.SIM</ta>
            <ta e="T392" id="Seg_7981" s="T391">liegen-PRS-3PL</ta>
            <ta e="T393" id="Seg_7982" s="T392">eben.der</ta>
            <ta e="T394" id="Seg_7983" s="T393">zwei</ta>
            <ta e="T395" id="Seg_7984" s="T394">Auge.[NOM]</ta>
            <ta e="T396" id="Seg_7985" s="T395">hineingehen-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T397" id="Seg_7986" s="T396">vorderer</ta>
            <ta e="T398" id="Seg_7987" s="T397">zwei</ta>
            <ta e="T399" id="Seg_7988" s="T398">Auge.[NOM]</ta>
            <ta e="T400" id="Seg_7989" s="T399">Sprache-PROPR.[NOM]</ta>
            <ta e="T401" id="Seg_7990" s="T400">oh</ta>
            <ta e="T402" id="Seg_7991" s="T401">und</ta>
            <ta e="T403" id="Seg_7992" s="T402">Mensch-ACC</ta>
            <ta e="T404" id="Seg_7993" s="T403">folgen-PTCP.PRS</ta>
            <ta e="T405" id="Seg_7994" s="T404">Mensch.[NOM]</ta>
            <ta e="T406" id="Seg_7995" s="T405">müde.werden-HAB.[3SG]</ta>
            <ta e="T407" id="Seg_7996" s="T406">2SG.[NOM]</ta>
            <ta e="T408" id="Seg_7997" s="T407">sich.ausruhen.[IMP.2SG]</ta>
            <ta e="T409" id="Seg_7998" s="T408">1SG.[NOM]</ta>
            <ta e="T410" id="Seg_7999" s="T409">Gerippe-PL-1PL-ACC</ta>
            <ta e="T411" id="Seg_8000" s="T410">Kopf-PL-1PL-ACC</ta>
            <ta e="T412" id="Seg_8001" s="T411">sammeln-ITER-CVB.SEQ</ta>
            <ta e="T413" id="Seg_8002" s="T412">bringen-FUT-1SG</ta>
            <ta e="T414" id="Seg_8003" s="T413">sagen-PRS.[3SG]</ta>
            <ta e="T415" id="Seg_8004" s="T414">Freund-3SG.[NOM]</ta>
            <ta e="T416" id="Seg_8005" s="T415">Eisenmütze.[NOM]</ta>
            <ta e="T417" id="Seg_8006" s="T416">jenes-DAT/LOC</ta>
            <ta e="T418" id="Seg_8007" s="T417">dieses</ta>
            <ta e="T419" id="Seg_8008" s="T418">echt</ta>
            <ta e="T420" id="Seg_8009" s="T419">Verstand-EP-INSTR</ta>
            <ta e="T421" id="Seg_8010" s="T420">gehen-PTCP.PRS</ta>
            <ta e="T422" id="Seg_8011" s="T421">Mensch-2SG</ta>
            <ta e="T423" id="Seg_8012" s="T422">Q</ta>
            <ta e="T424" id="Seg_8013" s="T423">NEG</ta>
            <ta e="T425" id="Seg_8014" s="T424">Q</ta>
            <ta e="T426" id="Seg_8015" s="T425">dieses</ta>
            <ta e="T427" id="Seg_8016" s="T426">1PL.[NOM]</ta>
            <ta e="T428" id="Seg_8017" s="T427">Mensch.[NOM]</ta>
            <ta e="T429" id="Seg_8018" s="T428">können-CVB.SEQ</ta>
            <ta e="T430" id="Seg_8019" s="T429">zurückkommen-EP-NEG.PTCP</ta>
            <ta e="T431" id="Seg_8020" s="T430">Ort-3SG-DAT/LOC</ta>
            <ta e="T432" id="Seg_8021" s="T431">kommen-PST1-1PL</ta>
            <ta e="T433" id="Seg_8022" s="T432">doch</ta>
            <ta e="T434" id="Seg_8023" s="T433">jetzt</ta>
            <ta e="T435" id="Seg_8024" s="T434">2SG.[NOM]</ta>
            <ta e="T436" id="Seg_8025" s="T435">folgen-FUT.[IMP.2SG]</ta>
            <ta e="T437" id="Seg_8026" s="T436">1SG-ACC</ta>
            <ta e="T438" id="Seg_8027" s="T437">verfolgen-FUT.[IMP.2SG]</ta>
            <ta e="T439" id="Seg_8028" s="T438">1SG.[NOM]</ta>
            <ta e="T440" id="Seg_8029" s="T439">Fähigkeit-1SG-ACC</ta>
            <ta e="T441" id="Seg_8030" s="T440">wissen-PRS-2SG</ta>
            <ta e="T442" id="Seg_8031" s="T441">dieses</ta>
            <ta e="T443" id="Seg_8032" s="T442">Schneesturm</ta>
            <ta e="T444" id="Seg_8033" s="T443">Herr-3SG-DAT/LOC</ta>
            <ta e="T445" id="Seg_8034" s="T444">kommen-PST1-1PL</ta>
            <ta e="T446" id="Seg_8035" s="T445">am.Morgen</ta>
            <ta e="T447" id="Seg_8036" s="T446">früh-AG.[NOM]</ta>
            <ta e="T448" id="Seg_8037" s="T447">Mensch.[NOM]</ta>
            <ta e="T449" id="Seg_8038" s="T448">stehen-PTCP.PRS</ta>
            <ta e="T450" id="Seg_8039" s="T449">Zeit-3SG-DAT/LOC</ta>
            <ta e="T451" id="Seg_8040" s="T450">stehen-CVB.SEQ</ta>
            <ta e="T452" id="Seg_8041" s="T451">hinausgehen-FUT-1SG</ta>
            <ta e="T453" id="Seg_8042" s="T452">dieses-DAT/LOC</ta>
            <ta e="T454" id="Seg_8043" s="T453">hundert-DISTR</ta>
            <ta e="T455" id="Seg_8044" s="T454">Karawane-PROPR</ta>
            <ta e="T456" id="Seg_8045" s="T455">Schlitten-PROPR</ta>
            <ta e="T457" id="Seg_8046" s="T456">zwei</ta>
            <ta e="T458" id="Seg_8047" s="T457">Mädchen-PL.[NOM]</ta>
            <ta e="T459" id="Seg_8048" s="T458">kommen-FUT-3PL</ta>
            <ta e="T460" id="Seg_8049" s="T459">dieses-DAT/LOC</ta>
            <ta e="T461" id="Seg_8050" s="T460">1SG.[NOM]</ta>
            <ta e="T462" id="Seg_8051" s="T461">hinausgehen-CVB.SIM</ta>
            <ta e="T463" id="Seg_8052" s="T462">rennen-CVB.SEQ-1SG</ta>
            <ta e="T464" id="Seg_8053" s="T463">vorderster</ta>
            <ta e="T465" id="Seg_8054" s="T464">Mädchen-ACC</ta>
            <ta e="T466" id="Seg_8055" s="T465">vorderstes.Rentier-3SG-ACC</ta>
            <ta e="T467" id="Seg_8056" s="T466">Schulterriemen-3SG-ABL</ta>
            <ta e="T468" id="Seg_8057" s="T467">halten-FUT-1SG</ta>
            <ta e="T469" id="Seg_8058" s="T468">und</ta>
            <ta e="T470" id="Seg_8059" s="T469">rennen-FUT-1SG</ta>
            <ta e="T471" id="Seg_8060" s="T470">dieses</ta>
            <ta e="T472" id="Seg_8061" s="T471">hundert</ta>
            <ta e="T473" id="Seg_8062" s="T472">Schlitten-ABL</ta>
            <ta e="T474" id="Seg_8063" s="T473">eins-3SG-DAT/LOC</ta>
            <ta e="T475" id="Seg_8064" s="T474">INDEF</ta>
            <ta e="T476" id="Seg_8065" s="T475">befestigen-EP-REFL-TEMP-2SG</ta>
            <ta e="T477" id="Seg_8066" s="T476">ankommen-MED-FUT-2SG</ta>
            <ta e="T478" id="Seg_8067" s="T477">Land-2SG-DAT/LOC</ta>
            <ta e="T479" id="Seg_8068" s="T478">dann</ta>
            <ta e="T480" id="Seg_8069" s="T479">befestigen-EP-REFL-NEG-TEMP-2SG</ta>
            <ta e="T481" id="Seg_8070" s="T480">dieses</ta>
            <ta e="T482" id="Seg_8071" s="T481">Welt-DAT/LOC</ta>
            <ta e="T483" id="Seg_8072" s="T482">sterben-FUT-2SG</ta>
            <ta e="T484" id="Seg_8073" s="T483">sagen-PRS.[3SG]</ta>
            <ta e="T485" id="Seg_8074" s="T484">doch</ta>
            <ta e="T486" id="Seg_8075" s="T485">Freund-3SG.[NOM]</ta>
            <ta e="T487" id="Seg_8076" s="T486">Knochengürtel.[NOM]</ta>
            <ta e="T488" id="Seg_8077" s="T487">Gerippe-ACC</ta>
            <ta e="T489" id="Seg_8078" s="T488">Bein-PL-ACC</ta>
            <ta e="T490" id="Seg_8079" s="T489">Kopf-PL-ACC</ta>
            <ta e="T491" id="Seg_8080" s="T490">bringen-PST1-3SG</ta>
            <ta e="T492" id="Seg_8081" s="T491">selbst-3PL.[NOM]</ta>
            <ta e="T493" id="Seg_8082" s="T492">selbst-3PL-INSTR</ta>
            <ta e="T494" id="Seg_8083" s="T493">Mensch.[NOM]</ta>
            <ta e="T495" id="Seg_8084" s="T494">sein-EMOT-CVB.SEQ</ta>
            <ta e="T496" id="Seg_8085" s="T495">bleiben-PST1-3PL</ta>
            <ta e="T497" id="Seg_8086" s="T496">schlafen-ITER-CVB.SEQ</ta>
            <ta e="T498" id="Seg_8087" s="T497">nachdem</ta>
            <ta e="T499" id="Seg_8088" s="T498">Knochengürtel.[NOM]</ta>
            <ta e="T500" id="Seg_8089" s="T499">aufwachen-PRS.[3SG]</ta>
            <ta e="T501" id="Seg_8090" s="T500">Freund-3SG.[NOM]</ta>
            <ta e="T502" id="Seg_8091" s="T501">hinausgehen-EP-PTCP.PST</ta>
            <ta e="T503" id="Seg_8092" s="T502">Lärm-3SG-DAT/LOC</ta>
            <ta e="T504" id="Seg_8093" s="T503">Knochengürtel.[NOM]</ta>
            <ta e="T505" id="Seg_8094" s="T504">hinausgehen-CVB.SIM</ta>
            <ta e="T506" id="Seg_8095" s="T505">rennen-PST2-3SG</ta>
            <ta e="T507" id="Seg_8096" s="T506">hundert</ta>
            <ta e="T508" id="Seg_8097" s="T507">Schlitten.[NOM]</ta>
            <ta e="T509" id="Seg_8098" s="T508">huschen-EP-MED-CVB.SEQ</ta>
            <ta e="T510" id="Seg_8099" s="T509">sein-TEMP-3SG</ta>
            <ta e="T511" id="Seg_8100" s="T510">letzter</ta>
            <ta e="T512" id="Seg_8101" s="T511">Schlitten-DAT/LOC</ta>
            <ta e="T513" id="Seg_8102" s="T512">sich.hängen-PST1-3SG</ta>
            <ta e="T514" id="Seg_8103" s="T513">Himmel-EP-INSTR</ta>
            <ta e="T515" id="Seg_8104" s="T514">und</ta>
            <ta e="T516" id="Seg_8105" s="T515">Erde-EP-INSTR</ta>
            <ta e="T517" id="Seg_8106" s="T516">und</ta>
            <ta e="T518" id="Seg_8107" s="T517">gehen-PTCP.PRS-3SG-ACC</ta>
            <ta e="T519" id="Seg_8108" s="T518">urteilen-PST2.NEG-3SG</ta>
            <ta e="T520" id="Seg_8109" s="T519">EMPH</ta>
            <ta e="T521" id="Seg_8110" s="T520">später</ta>
            <ta e="T522" id="Seg_8111" s="T521">EMPH</ta>
            <ta e="T523" id="Seg_8112" s="T522">dann</ta>
            <ta e="T524" id="Seg_8113" s="T523">hundert</ta>
            <ta e="T525" id="Seg_8114" s="T524">Schlitten.[NOM]</ta>
            <ta e="T526" id="Seg_8115" s="T525">halten-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T527" id="Seg_8116" s="T526">sehen-PST2-3SG</ta>
            <ta e="T528" id="Seg_8117" s="T527">Freund-3SG.[NOM]</ta>
            <ta e="T529" id="Seg_8118" s="T528">Land-3SG-DAT/LOC</ta>
            <ta e="T530" id="Seg_8119" s="T529">bringen-PST2.[3SG]</ta>
            <ta e="T531" id="Seg_8120" s="T530">doch</ta>
            <ta e="T532" id="Seg_8121" s="T531">Eisenmütze.[NOM]</ta>
            <ta e="T533" id="Seg_8122" s="T532">sagen-PRS.[3SG]</ta>
            <ta e="T534" id="Seg_8123" s="T533">Freund-3SG-ACC</ta>
            <ta e="T535" id="Seg_8124" s="T534">doch</ta>
            <ta e="T536" id="Seg_8125" s="T535">dieses.[NOM]</ta>
            <ta e="T537" id="Seg_8126" s="T536">hundert</ta>
            <ta e="T538" id="Seg_8127" s="T537">Schlitten.[NOM]</ta>
            <ta e="T539" id="Seg_8128" s="T538">anderer.von.zwei</ta>
            <ta e="T540" id="Seg_8129" s="T539">Seite-3SG-DAT/LOC</ta>
            <ta e="T541" id="Seg_8130" s="T540">drei</ta>
            <ta e="T542" id="Seg_8131" s="T541">weiß</ta>
            <ta e="T543" id="Seg_8132" s="T542">Rentierbulle.[NOM]</ta>
            <ta e="T544" id="Seg_8133" s="T543">einspannen-PASS/REFL-CVB.SEQ</ta>
            <ta e="T545" id="Seg_8134" s="T544">stehen-PRS.[3SG]</ta>
            <ta e="T546" id="Seg_8135" s="T545">anderer.von.zwei</ta>
            <ta e="T547" id="Seg_8136" s="T546">Seite-3SG-DAT/LOC</ta>
            <ta e="T548" id="Seg_8137" s="T547">drei</ta>
            <ta e="T549" id="Seg_8138" s="T548">schwarz</ta>
            <ta e="T550" id="Seg_8139" s="T549">Rentierbulle.[NOM]</ta>
            <ta e="T551" id="Seg_8140" s="T550">stehen-PRS.[3SG]</ta>
            <ta e="T552" id="Seg_8141" s="T551">doch</ta>
            <ta e="T553" id="Seg_8142" s="T552">1SG.[NOM]</ta>
            <ta e="T554" id="Seg_8143" s="T553">dieses</ta>
            <ta e="T555" id="Seg_8144" s="T554">drei</ta>
            <ta e="T556" id="Seg_8145" s="T555">weiß</ta>
            <ta e="T557" id="Seg_8146" s="T556">Rentierbulle-ACC</ta>
            <ta e="T558" id="Seg_8147" s="T557">töten-FUT-1SG</ta>
            <ta e="T559" id="Seg_8148" s="T558">Haut-3PL-ACC</ta>
            <ta e="T560" id="Seg_8149" s="T559">abziehen-FUT-1SG</ta>
            <ta e="T561" id="Seg_8150" s="T560">2SG.[NOM]</ta>
            <ta e="T562" id="Seg_8151" s="T561">drei</ta>
            <ta e="T563" id="Seg_8152" s="T562">schwarz-ACC</ta>
            <ta e="T564" id="Seg_8153" s="T563">töten-FUT-2SG</ta>
            <ta e="T565" id="Seg_8154" s="T564">Haut.abziehen-FUT-2SG</ta>
            <ta e="T566" id="Seg_8155" s="T565">Haut-3PL-ACC</ta>
            <ta e="T567" id="Seg_8156" s="T566">wissen-PRS-2SG</ta>
            <ta e="T568" id="Seg_8157" s="T567">dieses</ta>
            <ta e="T569" id="Seg_8158" s="T568">Schneesturm.[NOM]</ta>
            <ta e="T570" id="Seg_8159" s="T569">Herr-3SG-GEN</ta>
            <ta e="T571" id="Seg_8160" s="T570">klein</ta>
            <ta e="T572" id="Seg_8161" s="T571">Tochter-PL-3SG-ACC</ta>
            <ta e="T573" id="Seg_8162" s="T572">bringen-PST1-1SG</ta>
            <ta e="T574" id="Seg_8163" s="T573">Zauberei-1SG-INSTR</ta>
            <ta e="T575" id="Seg_8164" s="T574">jetzt</ta>
            <ta e="T576" id="Seg_8165" s="T575">dieses-3SG-3PL-ACC</ta>
            <ta e="T577" id="Seg_8166" s="T576">folgen-CVB.SEQ</ta>
            <ta e="T578" id="Seg_8167" s="T577">drei</ta>
            <ta e="T579" id="Seg_8168" s="T578">älterer.Bruder-3SG.[NOM]</ta>
            <ta e="T580" id="Seg_8169" s="T579">Schneesturm</ta>
            <ta e="T581" id="Seg_8170" s="T580">Herr-PL-3SG.[NOM]</ta>
            <ta e="T582" id="Seg_8171" s="T581">kommen-FUT-3PL</ta>
            <ta e="T583" id="Seg_8172" s="T582">jenes</ta>
            <ta e="T584" id="Seg_8173" s="T583">Mensch-PL.[NOM]</ta>
            <ta e="T585" id="Seg_8174" s="T584">kommen-TEMP-3PL</ta>
            <ta e="T586" id="Seg_8175" s="T585">hinterer.Teil.der.Jurte.[NOM]</ta>
            <ta e="T587" id="Seg_8176" s="T586">drei</ta>
            <ta e="T588" id="Seg_8177" s="T587">weiß</ta>
            <ta e="T589" id="Seg_8178" s="T588">Fell-ACC</ta>
            <ta e="T590" id="Seg_8179" s="T589">ausbreiten-FUT-1SG</ta>
            <ta e="T591" id="Seg_8180" s="T590">2SG.[NOM]</ta>
            <ta e="T592" id="Seg_8181" s="T591">dieses.[NOM]</ta>
            <ta e="T593" id="Seg_8182" s="T592">drei</ta>
            <ta e="T594" id="Seg_8183" s="T593">schwarz</ta>
            <ta e="T595" id="Seg_8184" s="T594">Haut-2SG-ACC</ta>
            <ta e="T596" id="Seg_8185" s="T595">ausbreiten-FUT.[IMP.2SG]</ta>
            <ta e="T597" id="Seg_8186" s="T596">doch</ta>
            <ta e="T598" id="Seg_8187" s="T597">groß</ta>
            <ta e="T599" id="Seg_8188" s="T598">großer.Kessel-2SG-DAT/LOC</ta>
            <ta e="T600" id="Seg_8189" s="T599">voll</ta>
            <ta e="T601" id="Seg_8190" s="T600">Wasser-3SG.[NOM]</ta>
            <ta e="T602" id="Seg_8191" s="T601">kochen-CAUS.[IMP.2SG]</ta>
            <ta e="T603" id="Seg_8192" s="T602">1SG.[NOM]</ta>
            <ta e="T604" id="Seg_8193" s="T603">wieder</ta>
            <ta e="T605" id="Seg_8194" s="T604">kochen-CAUS-FUT-1SG</ta>
            <ta e="T606" id="Seg_8195" s="T605">genau</ta>
            <ta e="T607" id="Seg_8196" s="T606">zuerst</ta>
            <ta e="T608" id="Seg_8197" s="T607">1SG-DAT/LOC</ta>
            <ta e="T609" id="Seg_8198" s="T608">Gast-VBZ-FUT-3PL</ta>
            <ta e="T610" id="Seg_8199" s="T609">1SG-ABL</ta>
            <ta e="T611" id="Seg_8200" s="T610">sich.satt.essen-CVB.SEQ</ta>
            <ta e="T612" id="Seg_8201" s="T611">zufrieden.sein-CVB.SEQ</ta>
            <ta e="T613" id="Seg_8202" s="T612">hinausgehen-TEMP-3PL</ta>
            <ta e="T614" id="Seg_8203" s="T613">2SG-DAT/LOC</ta>
            <ta e="T615" id="Seg_8204" s="T614">Gast-VBZ-FUT-3PL</ta>
            <ta e="T616" id="Seg_8205" s="T615">1SG.[NOM]</ta>
            <ta e="T617" id="Seg_8206" s="T616">3PL-DAT/LOC</ta>
            <ta e="T618" id="Seg_8207" s="T617">was-ACC</ta>
            <ta e="T619" id="Seg_8208" s="T618">INDEF</ta>
            <ta e="T620" id="Seg_8209" s="T619">zeigen-FUT-1SG</ta>
            <ta e="T621" id="Seg_8210" s="T620">hineingehen-RECP/COLL-FUT.[IMP.2SG]</ta>
            <ta e="T622" id="Seg_8211" s="T621">1SG.[NOM]</ta>
            <ta e="T623" id="Seg_8212" s="T622">was-ACC</ta>
            <ta e="T624" id="Seg_8213" s="T623">sehen-CAUS-PRS-1SG</ta>
            <ta e="T625" id="Seg_8214" s="T624">und</ta>
            <ta e="T626" id="Seg_8215" s="T625">nachmachen-CVB.SEQ</ta>
            <ta e="T627" id="Seg_8216" s="T626">3PL-DAT/LOC</ta>
            <ta e="T628" id="Seg_8217" s="T627">sehen-CAUS-FUT.[IMP.2SG]</ta>
            <ta e="T629" id="Seg_8218" s="T628">sagen-PST1-3SG</ta>
            <ta e="T630" id="Seg_8219" s="T629">lehren-PST1-3SG</ta>
            <ta e="T631" id="Seg_8220" s="T630">Freund-3SG-ACC</ta>
            <ta e="T632" id="Seg_8221" s="T631">wie.viel</ta>
            <ta e="T633" id="Seg_8222" s="T632">NEG</ta>
            <ta e="T634" id="Seg_8223" s="T633">sein-EP-NEG.CVB</ta>
            <ta e="T635" id="Seg_8224" s="T634">Schneesturm.[NOM]</ta>
            <ta e="T636" id="Seg_8225" s="T635">Herr-PL-3SG.[NOM]</ta>
            <ta e="T637" id="Seg_8226" s="T636">Wirbelwind-INSTR</ta>
            <ta e="T638" id="Seg_8227" s="T637">kommen-PST1-3PL</ta>
            <ta e="T639" id="Seg_8228" s="T638">dicht</ta>
            <ta e="T640" id="Seg_8229" s="T639">Eis.[NOM]</ta>
            <ta e="T641" id="Seg_8230" s="T640">Leute-PL.[NOM]</ta>
            <ta e="T642" id="Seg_8231" s="T641">hineingehen-CVB.SEQ-3PL</ta>
            <ta e="T643" id="Seg_8232" s="T642">völlig-EMPH-völlig</ta>
            <ta e="T644" id="Seg_8233" s="T643">hallo</ta>
            <ta e="T645" id="Seg_8234" s="T644">NEG</ta>
            <ta e="T646" id="Seg_8235" s="T645">sagen-NEG-3PL</ta>
            <ta e="T647" id="Seg_8236" s="T646">Eisenmütze.[NOM]</ta>
            <ta e="T648" id="Seg_8237" s="T647">Haus-3SG-DAT/LOC</ta>
            <ta e="T649" id="Seg_8238" s="T648">drei</ta>
            <ta e="T650" id="Seg_8239" s="T649">weiß</ta>
            <ta e="T651" id="Seg_8240" s="T650">Haut-DAT/LOC</ta>
            <ta e="T652" id="Seg_8241" s="T651">sich.setzen-PRS-3PL</ta>
            <ta e="T653" id="Seg_8242" s="T652">Eisenmütze.[NOM]</ta>
            <ta e="T654" id="Seg_8243" s="T653">Fähigkeit-VBZ-CAP.[3SG]</ta>
            <ta e="T655" id="Seg_8244" s="T654">dieses</ta>
            <ta e="T656" id="Seg_8245" s="T655">Volk-DAT/LOC</ta>
            <ta e="T657" id="Seg_8246" s="T656">kochen-CVB.SIM</ta>
            <ta e="T658" id="Seg_8247" s="T657">stehen-PTCP.PRS</ta>
            <ta e="T659" id="Seg_8248" s="T658">groß</ta>
            <ta e="T660" id="Seg_8249" s="T659">großer.Kessel-DAT/LOC</ta>
            <ta e="T661" id="Seg_8250" s="T660">knochig</ta>
            <ta e="T662" id="Seg_8251" s="T661">Schnabel-PROPR</ta>
            <ta e="T663" id="Seg_8252" s="T662">Prachttaucher.[NOM]</ta>
            <ta e="T664" id="Seg_8253" s="T663">werden-CVB.ANT</ta>
            <ta e="T665" id="Seg_8254" s="T664">aa_uu</ta>
            <ta e="T666" id="Seg_8255" s="T665">sagen-CVB.ANT</ta>
            <ta e="T667" id="Seg_8256" s="T666">tauchen-CVB.SEQ</ta>
            <ta e="T668" id="Seg_8257" s="T667">bleiben-PRS.[3SG]</ta>
            <ta e="T669" id="Seg_8258" s="T668">mit.den.Flügeln.schlagen-CVB.SIM-mit.den.Flügeln.schlagen-PTCP.PST-EP-INSTR</ta>
            <ta e="T670" id="Seg_8259" s="T669">fliegen-CVB.SEQ</ta>
            <ta e="T671" id="Seg_8260" s="T670">hinausgehen-CVB.SEQ</ta>
            <ta e="T672" id="Seg_8261" s="T671">Mutter-3SG-GEN</ta>
            <ta e="T673" id="Seg_8262" s="T672">Magen-3SG-INSTR</ta>
            <ta e="T674" id="Seg_8263" s="T673">ONOM</ta>
            <ta e="T675" id="Seg_8264" s="T674">machen-PRS.[3SG]</ta>
            <ta e="T676" id="Seg_8265" s="T675">dann</ta>
            <ta e="T677" id="Seg_8266" s="T676">Mutter-3SG-GEN</ta>
            <ta e="T678" id="Seg_8267" s="T677">Mund-3SG-ACC</ta>
            <ta e="T679" id="Seg_8268" s="T678">durch</ta>
            <ta e="T680" id="Seg_8269" s="T679">hinausgehen-CVB.SIM</ta>
            <ta e="T681" id="Seg_8270" s="T680">fliegen-PST2.[3SG]</ta>
            <ta e="T682" id="Seg_8271" s="T681">doch</ta>
            <ta e="T683" id="Seg_8272" s="T682">dann</ta>
            <ta e="T684" id="Seg_8273" s="T683">Vater-3SG-GEN</ta>
            <ta e="T685" id="Seg_8274" s="T684">Bauch-3SG-INSTR</ta>
            <ta e="T686" id="Seg_8275" s="T685">ONOM</ta>
            <ta e="T687" id="Seg_8276" s="T686">machen-PST2.[3SG]</ta>
            <ta e="T688" id="Seg_8277" s="T687">Vater-3SG-GEN</ta>
            <ta e="T689" id="Seg_8278" s="T688">Mund-3SG-INSTR</ta>
            <ta e="T690" id="Seg_8279" s="T689">soeben</ta>
            <ta e="T691" id="Seg_8280" s="T690">Horn-3SG-ACC</ta>
            <ta e="T692" id="Seg_8281" s="T691">abziehen-PTCP.PST</ta>
            <ta e="T693" id="Seg_8282" s="T692">wild</ta>
            <ta e="T694" id="Seg_8283" s="T693">Rentierbulle-3SG.[NOM]</ta>
            <ta e="T695" id="Seg_8284" s="T694">sein-CVB.SEQ</ta>
            <ta e="T696" id="Seg_8285" s="T695">hinausgehen-PRS.[3SG]</ta>
            <ta e="T697" id="Seg_8286" s="T696">dieses.[NOM]</ta>
            <ta e="T698" id="Seg_8287" s="T697">Hinterteil-ABL</ta>
            <ta e="T699" id="Seg_8288" s="T698">Eisenmütze.[NOM]</ta>
            <ta e="T700" id="Seg_8289" s="T699">Mensch.[NOM]</ta>
            <ta e="T701" id="Seg_8290" s="T700">werden-PRS.[3SG]</ta>
            <ta e="T702" id="Seg_8291" s="T701">und</ta>
            <ta e="T703" id="Seg_8292" s="T702">Bogen-INSTR</ta>
            <ta e="T704" id="Seg_8293" s="T703">dieses</ta>
            <ta e="T705" id="Seg_8294" s="T704">wild</ta>
            <ta e="T706" id="Seg_8295" s="T705">Rentierbulle-3SG-ACC</ta>
            <ta e="T707" id="Seg_8296" s="T706">durch</ta>
            <ta e="T708" id="Seg_8297" s="T707">schießen-CVB.SEQ</ta>
            <ta e="T709" id="Seg_8298" s="T708">fallen.lassen-PRS.[3SG]</ta>
            <ta e="T710" id="Seg_8299" s="T709">töten-PRS.[3SG]</ta>
            <ta e="T711" id="Seg_8300" s="T710">und</ta>
            <ta e="T712" id="Seg_8301" s="T711">dieses-DAT/LOC</ta>
            <ta e="T713" id="Seg_8302" s="T712">abziehen-EP-CAUS-EP-CAUS-CVB.SIM</ta>
            <ta e="T714" id="Seg_8303" s="T713">Haut.abziehen-CVB.SEQ</ta>
            <ta e="T715" id="Seg_8304" s="T714">Holz.[NOM]</ta>
            <ta e="T716" id="Seg_8305" s="T715">Schüssel-DAT/LOC</ta>
            <ta e="T717" id="Seg_8306" s="T716">roh-VBZ-CVB.SIM-schneiden-NMNZ-VBZ-CVB.SIM</ta>
            <ta e="T718" id="Seg_8307" s="T717">ziehen-CVB.SEQ</ta>
            <ta e="T719" id="Seg_8308" s="T718">werfen-PRS.[3SG]</ta>
            <ta e="T720" id="Seg_8309" s="T719">Gast-PL-DAT/LOC</ta>
            <ta e="T721" id="Seg_8310" s="T720">rohes.Fleisch-VBZ-EP-CAUS-CVB.SEQ</ta>
            <ta e="T722" id="Seg_8311" s="T721">Gast-3PL.[NOM]</ta>
            <ta e="T723" id="Seg_8312" s="T722">Wort.[NOM]-Sprache.[NOM]</ta>
            <ta e="T724" id="Seg_8313" s="T723">NEG.EX</ta>
            <ta e="T725" id="Seg_8314" s="T724">essen-PTCP.PRS-DAT/LOC</ta>
            <ta e="T726" id="Seg_8315" s="T725">gehen-PST2-3PL</ta>
            <ta e="T727" id="Seg_8316" s="T726">herbstlich</ta>
            <ta e="T728" id="Seg_8317" s="T727">Rentierbulle-ABL</ta>
            <ta e="T729" id="Seg_8318" s="T728">völlig</ta>
            <ta e="T730" id="Seg_8319" s="T729">Horn-PROPR</ta>
            <ta e="T731" id="Seg_8320" s="T730">Huf-3SG-ACC</ta>
            <ta e="T732" id="Seg_8321" s="T731">übrig.bleiben-CAUS-CVB.SEQ</ta>
            <ta e="T733" id="Seg_8322" s="T732">werfen-PST1-3PL</ta>
            <ta e="T734" id="Seg_8323" s="T733">Knochengürtel.[NOM]</ta>
            <ta e="T735" id="Seg_8324" s="T734">sehen-CVB.SEQ</ta>
            <ta e="T736" id="Seg_8325" s="T735">sich.setzen-EP-MED-FREQ-PST1-3SG</ta>
            <ta e="T737" id="Seg_8326" s="T736">essen-CVB.SEQ</ta>
            <ta e="T738" id="Seg_8327" s="T737">aufhören-PRS-3PL</ta>
            <ta e="T739" id="Seg_8328" s="T738">und</ta>
            <ta e="T740" id="Seg_8329" s="T739">Lippe-3PL-ACC</ta>
            <ta e="T741" id="Seg_8330" s="T740">wischen-ITER-CVB.SEQ</ta>
            <ta e="T742" id="Seg_8331" s="T741">hinausgehen-PRS-3PL</ta>
            <ta e="T743" id="Seg_8332" s="T742">Knochengürtel.[NOM]</ta>
            <ta e="T744" id="Seg_8333" s="T743">Haus-3SG-DAT/LOC</ta>
            <ta e="T745" id="Seg_8334" s="T744">springen-PRS.[3SG]</ta>
            <ta e="T746" id="Seg_8335" s="T745">Gast-PL-ACC</ta>
            <ta e="T747" id="Seg_8336" s="T746">empfangen-EP-MED-CVB.SIM</ta>
            <ta e="T748" id="Seg_8337" s="T747">Gast-3PL.[NOM]</ta>
            <ta e="T749" id="Seg_8338" s="T748">hineingehen-CVB.SEQ</ta>
            <ta e="T750" id="Seg_8339" s="T749">drei</ta>
            <ta e="T751" id="Seg_8340" s="T750">schwarz</ta>
            <ta e="T752" id="Seg_8341" s="T751">Haut-DAT/LOC</ta>
            <ta e="T753" id="Seg_8342" s="T752">sitzen-CVB.SIM</ta>
            <ta e="T754" id="Seg_8343" s="T753">geben-PRS-3PL</ta>
            <ta e="T755" id="Seg_8344" s="T754">Knochengürtel.[NOM]</ta>
            <ta e="T756" id="Seg_8345" s="T755">aa_uu</ta>
            <ta e="T757" id="Seg_8346" s="T756">sagen-CVB.ANT</ta>
            <ta e="T758" id="Seg_8347" s="T757">Sterntaucher-DIM.[NOM]</ta>
            <ta e="T759" id="Seg_8348" s="T758">werden-CVB.ANT</ta>
            <ta e="T760" id="Seg_8349" s="T759">großer.Kessel-DAT/LOC</ta>
            <ta e="T761" id="Seg_8350" s="T760">tauchen-PRS.[3SG]</ta>
            <ta e="T762" id="Seg_8351" s="T761">Mutter-3SG-GEN</ta>
            <ta e="T763" id="Seg_8352" s="T762">Bauch-3SG-DAT/LOC</ta>
            <ta e="T764" id="Seg_8353" s="T763">ONOM</ta>
            <ta e="T765" id="Seg_8354" s="T764">machen-PRS.[3SG]</ta>
            <ta e="T766" id="Seg_8355" s="T765">Mund-3SG-ACC</ta>
            <ta e="T767" id="Seg_8356" s="T766">durch</ta>
            <ta e="T768" id="Seg_8357" s="T767">hinausgehen-PRS.[3SG]</ta>
            <ta e="T769" id="Seg_8358" s="T768">Vater-3SG-GEN</ta>
            <ta e="T770" id="Seg_8359" s="T769">Bauch-3SG-DAT/LOC</ta>
            <ta e="T771" id="Seg_8360" s="T770">ONOM</ta>
            <ta e="T772" id="Seg_8361" s="T771">machen-PRS.[3SG]</ta>
            <ta e="T773" id="Seg_8362" s="T772">Vater-3SG-GEN</ta>
            <ta e="T774" id="Seg_8363" s="T773">Mund-3SG-ACC</ta>
            <ta e="T775" id="Seg_8364" s="T774">durch</ta>
            <ta e="T776" id="Seg_8365" s="T775">was-ABL</ta>
            <ta e="T777" id="Seg_8366" s="T776">INDEF</ta>
            <ta e="T778" id="Seg_8367" s="T777">schlecht</ta>
            <ta e="T779" id="Seg_8368" s="T778">Witwer-ADJZ</ta>
            <ta e="T780" id="Seg_8369" s="T779">Kalb-DIM.[NOM]</ta>
            <ta e="T781" id="Seg_8370" s="T780">werden-CVB.SEQ</ta>
            <ta e="T782" id="Seg_8371" s="T781">hinausgehen-CVB.SIM</ta>
            <ta e="T783" id="Seg_8372" s="T782">rennen-PST2.[3SG]</ta>
            <ta e="T784" id="Seg_8373" s="T783">Hinterteil-3SG-ABL</ta>
            <ta e="T785" id="Seg_8374" s="T784">Bogen-PROPR</ta>
            <ta e="T786" id="Seg_8375" s="T785">Mensch.[NOM]</ta>
            <ta e="T787" id="Seg_8376" s="T786">hinausgehen-PRS.[3SG]</ta>
            <ta e="T788" id="Seg_8377" s="T787">und</ta>
            <ta e="T789" id="Seg_8378" s="T788">Gast-PL.[NOM]</ta>
            <ta e="T790" id="Seg_8379" s="T789">Vorderteil-3PL-DAT/LOC</ta>
            <ta e="T791" id="Seg_8380" s="T790">nahe</ta>
            <ta e="T792" id="Seg_8381" s="T791">schießen-PRS.[3SG]</ta>
            <ta e="T793" id="Seg_8382" s="T792">Nahrung-VBZ-CVB.SEQ</ta>
            <ta e="T794" id="Seg_8383" s="T793">werfen-PRS.[3SG]</ta>
            <ta e="T795" id="Seg_8384" s="T794">Gast-PL-DAT/LOC</ta>
            <ta e="T796" id="Seg_8385" s="T795">ziehen-CVB.SEQ</ta>
            <ta e="T797" id="Seg_8386" s="T796">werfen-PRS.[3SG]</ta>
            <ta e="T798" id="Seg_8387" s="T797">ziehen-CVB.SEQ</ta>
            <ta e="T799" id="Seg_8388" s="T798">nur</ta>
            <ta e="T800" id="Seg_8389" s="T799">werfen-PST2-3SG</ta>
            <ta e="T801" id="Seg_8390" s="T800">Haus-3SG.[NOM]-Feuer-3SG.[NOM]</ta>
            <ta e="T802" id="Seg_8391" s="T801">wohin</ta>
            <ta e="T803" id="Seg_8392" s="T802">NEG</ta>
            <ta e="T804" id="Seg_8393" s="T803">gehen-PTCP.PST-3PL-ACC</ta>
            <ta e="T805" id="Seg_8394" s="T804">bemerken-NEG-PST1-3PL</ta>
            <ta e="T806" id="Seg_8395" s="T805">Schneesturm.[NOM]</ta>
            <ta e="T807" id="Seg_8396" s="T806">Herr-PL-3SG.[NOM]</ta>
            <ta e="T808" id="Seg_8397" s="T807">beleidigt.sein-CVB.SEQ</ta>
            <ta e="T809" id="Seg_8398" s="T808">dieses</ta>
            <ta e="T810" id="Seg_8399" s="T809">Haus-ACC</ta>
            <ta e="T811" id="Seg_8400" s="T810">Knochengürtel-ACC</ta>
            <ta e="T812" id="Seg_8401" s="T811">Mutter-COM-ACC-Vater-COM-ACC</ta>
            <ta e="T813" id="Seg_8402" s="T812">rennen-CVB.SEQ</ta>
            <ta e="T814" id="Seg_8403" s="T813">Wirbelwind-INSTR</ta>
            <ta e="T815" id="Seg_8404" s="T814">gehen-PST2-3PL</ta>
            <ta e="T816" id="Seg_8405" s="T815">nur</ta>
            <ta e="T817" id="Seg_8406" s="T816">Eisenmütze.[NOM]</ta>
            <ta e="T818" id="Seg_8407" s="T817">Mutter-COM-Vater-COM</ta>
            <ta e="T819" id="Seg_8408" s="T818">bleiben-PST2.[3SG]</ta>
            <ta e="T820" id="Seg_8409" s="T819">Schneesturm.[NOM]</ta>
            <ta e="T821" id="Seg_8410" s="T820">Tochter-3SG-ACC</ta>
            <ta e="T822" id="Seg_8411" s="T821">Frau-VBZ-CVB.SEQ</ta>
            <ta e="T823" id="Seg_8412" s="T822">reich.sein-CVB.SEQ-reich.sein-CVB.SEQ</ta>
            <ta e="T824" id="Seg_8413" s="T823">leben-PST2-3SG</ta>
            <ta e="T825" id="Seg_8414" s="T824">man.sagt</ta>
            <ta e="T826" id="Seg_8415" s="T825">letzter-3SG.[NOM]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_8416" s="T0">Железная.Шапка.[NOM]</ta>
            <ta e="T2" id="Seg_8417" s="T1">тот-DAT/LOC</ta>
            <ta e="T3" id="Seg_8418" s="T2">Костяной.Пояс.[NOM]</ta>
            <ta e="T4" id="Seg_8419" s="T3">говорить-CVB.SEQ</ta>
            <ta e="T5" id="Seg_8420" s="T4">два</ta>
            <ta e="T6" id="Seg_8421" s="T5">мальчик.[NOM]</ta>
            <ta e="T7" id="Seg_8422" s="T6">человек-PL.[NOM]</ta>
            <ta e="T8" id="Seg_8423" s="T7">жить-PST2-3PL</ta>
            <ta e="T9" id="Seg_8424" s="T8">два-COLL.[NOM]</ta>
            <ta e="T10" id="Seg_8425" s="T9">отец-PROPR-3PL</ta>
            <ta e="T11" id="Seg_8426" s="T10">мать-PROPR-3PL</ta>
            <ta e="T12" id="Seg_8427" s="T11">вот</ta>
            <ta e="T13" id="Seg_8428" s="T12">этот</ta>
            <ta e="T14" id="Seg_8429" s="T13">жить-CVB.SEQ-3PL</ta>
            <ta e="T15" id="Seg_8430" s="T14">Костяной.Пояс-3SG.[NOM]</ta>
            <ta e="T16" id="Seg_8431" s="T15">говорить-PRS.[3SG]</ta>
            <ta e="T17" id="Seg_8432" s="T16">Железная.Шапка-3SG-ACC</ta>
            <ta e="T18" id="Seg_8433" s="T17">друг</ta>
            <ta e="T19" id="Seg_8434" s="T18">этот</ta>
            <ta e="T20" id="Seg_8435" s="T19">1PL.[NOM]</ta>
            <ta e="T21" id="Seg_8436" s="T20">что.[NOM]</ta>
            <ta e="T22" id="Seg_8437" s="T21">способность-PROPR.[NOM]</ta>
            <ta e="T23" id="Seg_8438" s="T22">быть-PRS-1PL</ta>
            <ta e="T24" id="Seg_8439" s="T23">почему</ta>
            <ta e="T25" id="Seg_8440" s="T24">способность.[NOM]</ta>
            <ta e="T26" id="Seg_8441" s="T25">способность-1PL-ACC</ta>
            <ta e="T27" id="Seg_8442" s="T26">узнавать-RECP/COLL-EP-NEG-1PL</ta>
            <ta e="T28" id="Seg_8443" s="T27">порука.[NOM]</ta>
            <ta e="T29" id="Seg_8444" s="T28">отнимать-EP-RECP/COLL-CVB.PURP</ta>
            <ta e="T30" id="Seg_8445" s="T29">хотеть-PRS.[3SG]</ta>
            <ta e="T31" id="Seg_8446" s="T30">наверное</ta>
            <ta e="T32" id="Seg_8447" s="T31">друг-3SG.[NOM]</ta>
            <ta e="T33" id="Seg_8448" s="T32">ээ</ta>
            <ta e="T34" id="Seg_8449" s="T33">друг.[NOM]</ta>
            <ta e="T35" id="Seg_8450" s="T34">что-1PL-ACC</ta>
            <ta e="T36" id="Seg_8451" s="T35">отнимать-EP-RECP/COLL-CVB.SEQ</ta>
            <ta e="T37" id="Seg_8452" s="T36">порука.[NOM]</ta>
            <ta e="T38" id="Seg_8453" s="T37">отнимать-EP-RECP/COLL-FUT-1PL=Q</ta>
            <ta e="T39" id="Seg_8454" s="T38">говорить-CVB.SEQ</ta>
            <ta e="T40" id="Seg_8455" s="T39">быть-EP-MED-NEG.[3SG]</ta>
            <ta e="T41" id="Seg_8456" s="T40">друг-3SG.[NOM]</ta>
            <ta e="T42" id="Seg_8457" s="T41">выносить-CAUS-NEG.[3SG]</ta>
            <ta e="T43" id="Seg_8458" s="T42">почему</ta>
            <ta e="T44" id="Seg_8459" s="T43">друг.[NOM]</ta>
            <ta e="T45" id="Seg_8460" s="T44">2SG.[NOM]</ta>
            <ta e="T46" id="Seg_8461" s="T45">способность-2SG-ACC</ta>
            <ta e="T47" id="Seg_8462" s="T46">узнавать-PTCP.FUT-1SG-ACC</ta>
            <ta e="T48" id="Seg_8463" s="T47">хотеть-PRS-1SG</ta>
            <ta e="T49" id="Seg_8464" s="T48">друг-3SG.[NOM]</ta>
            <ta e="T50" id="Seg_8465" s="T49">быть-EP-MED-NEG.[3SG]</ta>
            <ta e="T51" id="Seg_8466" s="T50">Костяной.Пояс.[NOM]</ta>
            <ta e="T52" id="Seg_8467" s="T51">1SG.[NOM]</ta>
            <ta e="T53" id="Seg_8468" s="T52">спасаться-FUT-1SG</ta>
            <ta e="T54" id="Seg_8469" s="T53">2SG-ABL</ta>
            <ta e="T55" id="Seg_8470" s="T54">следовать.[IMP.2SG]</ta>
            <ta e="T56" id="Seg_8471" s="T55">1SG-ACC</ta>
            <ta e="T57" id="Seg_8472" s="T56">1SG.[NOM]</ta>
            <ta e="T58" id="Seg_8473" s="T57">как</ta>
            <ta e="T59" id="Seg_8474" s="T58">быть-PRS-1SG</ta>
            <ta e="T60" id="Seg_8475" s="T59">да</ta>
            <ta e="T61" id="Seg_8476" s="T60">так-INTNS</ta>
            <ta e="T62" id="Seg_8477" s="T61">быть-CVB.SEQ</ta>
            <ta e="T63" id="Seg_8478" s="T62">идти.[IMP.2SG]</ta>
            <ta e="T64" id="Seg_8479" s="T63">говорить-PRS.[3SG]</ta>
            <ta e="T65" id="Seg_8480" s="T64">друг-3SG.[NOM]</ta>
            <ta e="T66" id="Seg_8481" s="T65">следовать-FUT2-1SG</ta>
            <ta e="T67" id="Seg_8482" s="T66">Q</ta>
            <ta e="T68" id="Seg_8483" s="T67">лучше</ta>
            <ta e="T69" id="Seg_8484" s="T68">утром</ta>
            <ta e="T70" id="Seg_8485" s="T69">рано-AG.[NOM]</ta>
            <ta e="T71" id="Seg_8486" s="T70">человек.[NOM]</ta>
            <ta e="T72" id="Seg_8487" s="T71">вставать-PTCP.PRS</ta>
            <ta e="T73" id="Seg_8488" s="T72">час-3SG-DAT/LOC</ta>
            <ta e="T74" id="Seg_8489" s="T73">следовать-FUT.[IMP.2SG]</ta>
            <ta e="T75" id="Seg_8490" s="T74">1SG-ACC</ta>
            <ta e="T76" id="Seg_8491" s="T75">говорить-PRS.[3SG]</ta>
            <ta e="T77" id="Seg_8492" s="T76">Костяной.Пояс.[NOM]</ta>
            <ta e="T78" id="Seg_8493" s="T77">так</ta>
            <ta e="T79" id="Seg_8494" s="T78">говорить-EP-RECP/COLL-CVB.SEQ</ta>
            <ta e="T80" id="Seg_8495" s="T79">расходиться-PRS-3PL</ta>
            <ta e="T81" id="Seg_8496" s="T80">дом.[NOM]</ta>
            <ta e="T82" id="Seg_8497" s="T81">дом-3PL-DAT/LOC</ta>
            <ta e="T83" id="Seg_8498" s="T82">Железная.Шапка.[NOM]</ta>
            <ta e="T84" id="Seg_8499" s="T83">выйти-CVB.SEQ</ta>
            <ta e="T85" id="Seg_8500" s="T84">спать-CVB.SIM</ta>
            <ta e="T86" id="Seg_8501" s="T85">ложиться-PRS.[3SG]</ta>
            <ta e="T87" id="Seg_8502" s="T86">просыпаться-CVB.SEQ</ta>
            <ta e="T88" id="Seg_8503" s="T87">приходить-PRS.[3SG]</ta>
            <ta e="T89" id="Seg_8504" s="T88">друг-EP-1SG.[NOM]</ta>
            <ta e="T90" id="Seg_8505" s="T89">есть-3SG</ta>
            <ta e="T91" id="Seg_8506" s="T90">Q</ta>
            <ta e="T92" id="Seg_8507" s="T91">друг-3SG-DAT/LOC</ta>
            <ta e="T93" id="Seg_8508" s="T92">приходить-PST2-3SG</ta>
            <ta e="T94" id="Seg_8509" s="T93">Костяной.Пояс.[NOM]</ta>
            <ta e="T95" id="Seg_8510" s="T94">постель-3SG.[NOM]</ta>
            <ta e="T96" id="Seg_8511" s="T95">быть.пустым-CVB.SIM</ta>
            <ta e="T97" id="Seg_8512" s="T96">лежать-PRS.[3SG]</ta>
            <ta e="T98" id="Seg_8513" s="T97">идти-CVB.SEQ</ta>
            <ta e="T99" id="Seg_8514" s="T98">оставаться-PST2.[3SG]</ta>
            <ta e="T100" id="Seg_8515" s="T99">Железная.Шапка.[NOM]</ta>
            <ta e="T101" id="Seg_8516" s="T100">след.[NOM]</ta>
            <ta e="T102" id="Seg_8517" s="T101">идти.по.следам-PRS.[3SG]</ta>
            <ta e="T103" id="Seg_8518" s="T102">след-ACC</ta>
            <ta e="T104" id="Seg_8519" s="T103">найти-NEG.[3SG]</ta>
            <ta e="T105" id="Seg_8520" s="T104">последний-3SG.[NOM]</ta>
            <ta e="T106" id="Seg_8521" s="T105">быть-PRS.[3SG]</ta>
            <ta e="T107" id="Seg_8522" s="T106">входить-CVB.SEQ</ta>
            <ta e="T108" id="Seg_8523" s="T107">постель-3SG-ACC</ta>
            <ta e="T109" id="Seg_8524" s="T108">прямо-VBZ-PRS.[3SG]</ta>
            <ta e="T110" id="Seg_8525" s="T109">Костяной.Пояс.[NOM]</ta>
            <ta e="T111" id="Seg_8526" s="T110">постель-3SG-GEN</ta>
            <ta e="T112" id="Seg_8527" s="T111">нижняя.часть-3SG-ABL</ta>
            <ta e="T113" id="Seg_8528" s="T112">остромордый</ta>
            <ta e="T114" id="Seg_8529" s="T113">мышь-3SG-GEN</ta>
            <ta e="T115" id="Seg_8530" s="T114">нора-3SG.[NOM]</ta>
            <ta e="T116" id="Seg_8531" s="T115">быть.пустым-CVB.SIM</ta>
            <ta e="T117" id="Seg_8532" s="T116">лежать-PRS.[3SG]</ta>
            <ta e="T118" id="Seg_8533" s="T117">другой</ta>
            <ta e="T119" id="Seg_8534" s="T118">след.[NOM]</ta>
            <ta e="T120" id="Seg_8535" s="T119">NEG.EX</ta>
            <ta e="T121" id="Seg_8536" s="T120">Железная.Шапка.[NOM]</ta>
            <ta e="T122" id="Seg_8537" s="T121">мышь.[NOM]</ta>
            <ta e="T123" id="Seg_8538" s="T122">становиться-PST1-3SG</ta>
            <ta e="T124" id="Seg_8539" s="T123">да</ta>
            <ta e="T125" id="Seg_8540" s="T124">нора-3SG-INSTR</ta>
            <ta e="T126" id="Seg_8541" s="T125">сходить-PST1-3SG</ta>
            <ta e="T127" id="Seg_8542" s="T126">далекий-ADJZ-3SG.[NOM]</ta>
            <ta e="T128" id="Seg_8543" s="T127">нора-ABL</ta>
            <ta e="T129" id="Seg_8544" s="T128">выйти-CVB.ANT</ta>
            <ta e="T130" id="Seg_8545" s="T129">горностай.[NOM]</ta>
            <ta e="T131" id="Seg_8546" s="T130">становиться-CVB.ANT</ta>
            <ta e="T132" id="Seg_8547" s="T131">прыгать-CVB.SEQ</ta>
            <ta e="T133" id="Seg_8548" s="T132">оставаться-PRS.[3SG]</ta>
            <ta e="T134" id="Seg_8549" s="T133">Железная.Шапка.[NOM]</ta>
            <ta e="T135" id="Seg_8550" s="T134">горностай.[NOM]</ta>
            <ta e="T136" id="Seg_8551" s="T135">быть-CVB.ANT</ta>
            <ta e="T137" id="Seg_8552" s="T136">следовать-CVB.SEQ</ta>
            <ta e="T138" id="Seg_8553" s="T137">идти-PRS.[3SG]</ta>
            <ta e="T139" id="Seg_8554" s="T138">уф</ta>
            <ta e="T140" id="Seg_8555" s="T139">бежать-NMNZ.[NOM]</ta>
            <ta e="T141" id="Seg_8556" s="T140">идти-CVB.SEQ</ta>
            <ta e="T142" id="Seg_8557" s="T141">идти-PST1-3SG</ta>
            <ta e="T143" id="Seg_8558" s="T142">этот</ta>
            <ta e="T144" id="Seg_8559" s="T143">горностай.[NOM]</ta>
            <ta e="T145" id="Seg_8560" s="T144">след-3SG.[NOM]</ta>
            <ta e="T146" id="Seg_8561" s="T145">кончать-PRS.[3SG]</ta>
            <ta e="T147" id="Seg_8562" s="T146">да</ta>
            <ta e="T148" id="Seg_8563" s="T147">средний</ta>
            <ta e="T149" id="Seg_8564" s="T148">мир.[NOM]</ta>
            <ta e="T150" id="Seg_8565" s="T149">песец.[NOM]</ta>
            <ta e="T151" id="Seg_8566" s="T150">след-3SG.[NOM]</ta>
            <ta e="T152" id="Seg_8567" s="T151">идти-CVB.SIM</ta>
            <ta e="T153" id="Seg_8568" s="T152">стоять-PST1-3SG</ta>
            <ta e="T154" id="Seg_8569" s="T153">друг-3SG.[NOM]</ta>
            <ta e="T155" id="Seg_8570" s="T154">бояться-CVB.SIM</ta>
            <ta e="T156" id="Seg_8571" s="T155">идти-PST1-3SG</ta>
            <ta e="T157" id="Seg_8572" s="T156">Железная.Шапка.[NOM]</ta>
            <ta e="T158" id="Seg_8573" s="T157">песец.[NOM]</ta>
            <ta e="T159" id="Seg_8574" s="T158">становиться-PRS.[3SG]</ta>
            <ta e="T160" id="Seg_8575" s="T159">да</ta>
            <ta e="T161" id="Seg_8576" s="T160">прыгать-CVB.SEQ</ta>
            <ta e="T162" id="Seg_8577" s="T161">идти-PST1-3SG</ta>
            <ta e="T163" id="Seg_8578" s="T162">один</ta>
            <ta e="T164" id="Seg_8579" s="T163">место-DAT/LOC</ta>
            <ta e="T165" id="Seg_8580" s="T164">падать-PST2.[3SG]</ta>
            <ta e="T166" id="Seg_8581" s="T165">да</ta>
            <ta e="T167" id="Seg_8582" s="T166">Костяной.Пояс-3SG.[NOM]</ta>
            <ta e="T168" id="Seg_8583" s="T167">заяц.[NOM]</ta>
            <ta e="T169" id="Seg_8584" s="T168">становиться-CVB.SEQ</ta>
            <ta e="T170" id="Seg_8585" s="T169">прыгать-PST2.[3SG]</ta>
            <ta e="T171" id="Seg_8586" s="T170">задняя.часть-ADJZ-3SG.[NOM]</ta>
            <ta e="T172" id="Seg_8587" s="T171">заяц.[NOM]</ta>
            <ta e="T173" id="Seg_8588" s="T172">становиться-CVB.SEQ</ta>
            <ta e="T174" id="Seg_8589" s="T173">следовать-PST1-3SG</ta>
            <ta e="T175" id="Seg_8590" s="T174">Костяной.Пояс.[NOM]</ta>
            <ta e="T176" id="Seg_8591" s="T175">лиса.[NOM]</ta>
            <ta e="T177" id="Seg_8592" s="T176">становиться-PST1-3SG</ta>
            <ta e="T178" id="Seg_8593" s="T177">потом</ta>
            <ta e="T179" id="Seg_8594" s="T178">задняя.часть-ADJZ-3SG.[NOM]</ta>
            <ta e="T180" id="Seg_8595" s="T179">тоже</ta>
            <ta e="T181" id="Seg_8596" s="T180">лиса.[NOM]</ta>
            <ta e="T182" id="Seg_8597" s="T181">становиться-CVB.SEQ</ta>
            <ta e="T183" id="Seg_8598" s="T182">дорога-3SG-ACC</ta>
            <ta e="T184" id="Seg_8599" s="T183">только</ta>
            <ta e="T185" id="Seg_8600" s="T184">подражать-CVB.SEQ</ta>
            <ta e="T186" id="Seg_8601" s="T185">идти-PST1-3SG</ta>
            <ta e="T187" id="Seg_8602" s="T186">этот-3SG.[NOM]</ta>
            <ta e="T188" id="Seg_8603" s="T187">волк.[NOM]</ta>
            <ta e="T189" id="Seg_8604" s="T188">быть-CVB.SEQ</ta>
            <ta e="T190" id="Seg_8605" s="T189">бежать-CVB.SEQ</ta>
            <ta e="T191" id="Seg_8606" s="T190">вытягиваться-PST2.[3SG]</ta>
            <ta e="T192" id="Seg_8607" s="T191">задняя.часть-ADJZ-3SG.[NOM]</ta>
            <ta e="T193" id="Seg_8608" s="T192">удивляться-CVB.SIM</ta>
            <ta e="T194" id="Seg_8609" s="T193">идти-PST1-3SG</ta>
            <ta e="T195" id="Seg_8610" s="T194">этот-3SG-ADJZ-3SG.[NOM]</ta>
            <ta e="T196" id="Seg_8611" s="T195">уживаться-EP-NEG.CVB</ta>
            <ta e="T197" id="Seg_8612" s="T196">попросить-EP-MED-PST2.[3SG]</ta>
            <ta e="T198" id="Seg_8613" s="T197">MOD</ta>
            <ta e="T199" id="Seg_8614" s="T198">думать-PRS.[3SG]</ta>
            <ta e="T200" id="Seg_8615" s="T199">волк.[NOM]</ta>
            <ta e="T201" id="Seg_8616" s="T200">становиться-CVB.SEQ</ta>
            <ta e="T202" id="Seg_8617" s="T201">следовать-CVB.SEQ</ta>
            <ta e="T203" id="Seg_8618" s="T202">идти-PST1-3SG</ta>
            <ta e="T204" id="Seg_8619" s="T203">сколько-ACC-сколько-ACC</ta>
            <ta e="T205" id="Seg_8620" s="T204">идти-PST2-3SG</ta>
            <ta e="T206" id="Seg_8621" s="T205">знать-PASS/REFL-EP-NEG.[3SG]</ta>
            <ta e="T207" id="Seg_8622" s="T206">Костяной.Пояс.[NOM]</ta>
            <ta e="T208" id="Seg_8623" s="T207">росомаха.[NOM]</ta>
            <ta e="T209" id="Seg_8624" s="T208">становиться-CVB.SEQ</ta>
            <ta e="T210" id="Seg_8625" s="T209">трусить-CVB.SIM</ta>
            <ta e="T211" id="Seg_8626" s="T210">стоять-PST2.[3SG]</ta>
            <ta e="T212" id="Seg_8627" s="T211">задняя.часть-ADJZ-3SG.[NOM]</ta>
            <ta e="T213" id="Seg_8628" s="T212">опять</ta>
            <ta e="T214" id="Seg_8629" s="T213">всегда</ta>
            <ta e="T215" id="Seg_8630" s="T214">удивляться-CVB.SIM-удивляться-CVB.SIM</ta>
            <ta e="T216" id="Seg_8631" s="T215">росомаха.[NOM]</ta>
            <ta e="T217" id="Seg_8632" s="T216">становиться-CVB.SEQ</ta>
            <ta e="T218" id="Seg_8633" s="T217">следовать-CVB.SEQ</ta>
            <ta e="T219" id="Seg_8634" s="T218">идти-PST1-3SG</ta>
            <ta e="T220" id="Seg_8635" s="T219">росомаха-3SG-GEN</ta>
            <ta e="T221" id="Seg_8636" s="T220">след-3SG.[NOM]</ta>
            <ta e="T222" id="Seg_8637" s="T221">кончать-CVB.ANT-3SG</ta>
            <ta e="T223" id="Seg_8638" s="T222">медведь.[NOM]</ta>
            <ta e="T224" id="Seg_8639" s="T223">становиться-CVB.SEQ</ta>
            <ta e="T225" id="Seg_8640" s="T224">трусить-PST2.[3SG]</ta>
            <ta e="T226" id="Seg_8641" s="T225">медведь.[NOM]</ta>
            <ta e="T227" id="Seg_8642" s="T226">становиться-CVB.SEQ</ta>
            <ta e="T228" id="Seg_8643" s="T227">следовать-CVB.SEQ</ta>
            <ta e="T229" id="Seg_8644" s="T228">идти-PST1-3SG</ta>
            <ta e="T230" id="Seg_8645" s="T229">Железная.Шапка.[NOM]</ta>
            <ta e="T231" id="Seg_8646" s="T230">этот.[NOM]</ta>
            <ta e="T232" id="Seg_8647" s="T231">так.долго</ta>
            <ta e="T233" id="Seg_8648" s="T232">уф</ta>
            <ta e="T234" id="Seg_8649" s="T233">бежать-NMNZ.[NOM]</ta>
            <ta e="T235" id="Seg_8650" s="T234">путь.[NOM]</ta>
            <ta e="T236" id="Seg_8651" s="T235">медведь.[NOM]</ta>
            <ta e="T237" id="Seg_8652" s="T236">след-3SG.[NOM]</ta>
            <ta e="T238" id="Seg_8653" s="T237">кончаться-PRS.[3SG]</ta>
            <ta e="T239" id="Seg_8654" s="T238">да</ta>
            <ta e="T240" id="Seg_8655" s="T239">дикий.олень</ta>
            <ta e="T241" id="Seg_8656" s="T240">олений.бык-3SG.[NOM]</ta>
            <ta e="T242" id="Seg_8657" s="T241">становиться-CVB.SEQ</ta>
            <ta e="T243" id="Seg_8658" s="T242">прыгать-CVB.SEQ</ta>
            <ta e="T244" id="Seg_8659" s="T243">оставаться-FREQ-PST2.[3SG]</ta>
            <ta e="T245" id="Seg_8660" s="T244">удивляться-CVB.SIM</ta>
            <ta e="T246" id="Seg_8661" s="T245">только</ta>
            <ta e="T247" id="Seg_8662" s="T246">идти-PRS.[3SG]</ta>
            <ta e="T248" id="Seg_8663" s="T247">всегда</ta>
            <ta e="T249" id="Seg_8664" s="T248">следовать-PRS.[3SG]</ta>
            <ta e="T250" id="Seg_8665" s="T249">друг-3SG.[NOM]</ta>
            <ta e="T251" id="Seg_8666" s="T250">Железная.Шапка.[NOM]</ta>
            <ta e="T252" id="Seg_8667" s="T251">дикий.олень.[NOM]</ta>
            <ta e="T253" id="Seg_8668" s="T252">олений.бык-3SG.[NOM]</ta>
            <ta e="T254" id="Seg_8669" s="T253">становиться-PST1-3SG</ta>
            <ta e="T255" id="Seg_8670" s="T254">способность-2SG.[NOM]</ta>
            <ta e="T256" id="Seg_8671" s="T255">да</ta>
            <ta e="T257" id="Seg_8672" s="T256">последний-3SG.[NOM]</ta>
            <ta e="T258" id="Seg_8673" s="T257">MOD</ta>
            <ta e="T259" id="Seg_8674" s="T258">догонять-CVB.SIM</ta>
            <ta e="T260" id="Seg_8675" s="T259">MOD-1SG</ta>
            <ta e="T261" id="Seg_8676" s="T260">этот.EMPH-INTNS</ta>
            <ta e="T262" id="Seg_8677" s="T261">думать-CVB.SIM</ta>
            <ta e="T263" id="Seg_8678" s="T262">думать-PRS.[3SG]</ta>
            <ta e="T264" id="Seg_8679" s="T263">видеть-PST2-3SG</ta>
            <ta e="T265" id="Seg_8680" s="T264">один</ta>
            <ta e="T266" id="Seg_8681" s="T265">место-DAT/LOC</ta>
            <ta e="T267" id="Seg_8682" s="T266">друг-3SG.[NOM]</ta>
            <ta e="T268" id="Seg_8683" s="T267">раскорячиться-CVB.SEQ</ta>
            <ta e="T269" id="Seg_8684" s="T268">стоять-PRS.[3SG]</ta>
            <ta e="T270" id="Seg_8685" s="T269">приближаться-CVB.SEQ</ta>
            <ta e="T271" id="Seg_8686" s="T270">видеть-PST2-3SG</ta>
            <ta e="T272" id="Seg_8687" s="T271">два</ta>
            <ta e="T273" id="Seg_8688" s="T272">вдруг</ta>
            <ta e="T274" id="Seg_8689" s="T273">нога-3SG.[NOM]</ta>
            <ta e="T275" id="Seg_8690" s="T274">торчать-CVB.SEQ</ta>
            <ta e="T276" id="Seg_8691" s="T275">оставаться-PST2.[3SG]</ta>
            <ta e="T277" id="Seg_8692" s="T276">сустав-PL-3SG-INSTR</ta>
            <ta e="T278" id="Seg_8693" s="T277">резать-CVB.SEQ</ta>
            <ta e="T279" id="Seg_8694" s="T278">туша.[NOM]</ta>
            <ta e="T280" id="Seg_8695" s="T279">нога-PL-3SG-ACC</ta>
            <ta e="T281" id="Seg_8696" s="T280">кататься-CAUS-CVB.SEQ</ta>
            <ta e="T282" id="Seg_8697" s="T281">бросать-PST2.[3SG]</ta>
            <ta e="T283" id="Seg_8698" s="T282">тот.EMPH.[NOM]</ta>
            <ta e="T284" id="Seg_8699" s="T283">как-INTNS</ta>
            <ta e="T285" id="Seg_8700" s="T284">Железная.Шапка.[NOM]</ta>
            <ta e="T286" id="Seg_8701" s="T285">нога-3SG-ACC</ta>
            <ta e="T287" id="Seg_8702" s="T286">резать-CVB.ANT</ta>
            <ta e="T288" id="Seg_8703" s="T287">туша.[NOM]</ta>
            <ta e="T289" id="Seg_8704" s="T288">становиться-CVB.SEQ</ta>
            <ta e="T290" id="Seg_8705" s="T289">кататься-CVB.SEQ</ta>
            <ta e="T291" id="Seg_8706" s="T290">идти-PST1-3SG</ta>
            <ta e="T292" id="Seg_8707" s="T291">сколько</ta>
            <ta e="T293" id="Seg_8708" s="T292">NEG</ta>
            <ta e="T294" id="Seg_8709" s="T293">быть-EP-NEG.CVB</ta>
            <ta e="T295" id="Seg_8710" s="T294">туша.[NOM]</ta>
            <ta e="T296" id="Seg_8711" s="T295">громоздиться-CVB.SIM</ta>
            <ta e="T297" id="Seg_8712" s="T296">лежать-PRS.[3SG]</ta>
            <ta e="T298" id="Seg_8713" s="T297">радоваться-PRS.[3SG]</ta>
            <ta e="T299" id="Seg_8714" s="T298">достигать-PST1-1SG</ta>
            <ta e="T300" id="Seg_8715" s="T299">EMPH</ta>
            <ta e="T301" id="Seg_8716" s="T300">говорить-PRS.[3SG]</ta>
            <ta e="T302" id="Seg_8717" s="T301">этот</ta>
            <ta e="T303" id="Seg_8718" s="T302">туша-DAT/LOC</ta>
            <ta e="T304" id="Seg_8719" s="T303">доезжать-PST2-3SG</ta>
            <ta e="T305" id="Seg_8720" s="T304">туша.[NOM]</ta>
            <ta e="T306" id="Seg_8721" s="T305">пустой.[NOM]</ta>
            <ta e="T307" id="Seg_8722" s="T306">только</ta>
            <ta e="T308" id="Seg_8723" s="T307">лежать-PRS.[3SG]</ta>
            <ta e="T309" id="Seg_8724" s="T308">голова-3SG.[NOM]</ta>
            <ta e="T310" id="Seg_8725" s="T309">только</ta>
            <ta e="T311" id="Seg_8726" s="T310">кататься-CVB.SEQ</ta>
            <ta e="T312" id="Seg_8727" s="T311">оставаться-PST2.[3SG]</ta>
            <ta e="T313" id="Seg_8728" s="T312">эй</ta>
            <ta e="T314" id="Seg_8729" s="T313">ну</ta>
            <ta e="T315" id="Seg_8730" s="T314">этот</ta>
            <ta e="T316" id="Seg_8731" s="T315">почему</ta>
            <ta e="T317" id="Seg_8732" s="T316">разрезать-EP-REFL-PRS.[3SG]</ta>
            <ta e="T318" id="Seg_8733" s="T317">сам-3SG-ACC</ta>
            <ta e="T319" id="Seg_8734" s="T318">думать-CVB.SEQ</ta>
            <ta e="T320" id="Seg_8735" s="T319">думать-PRS.[3SG]</ta>
            <ta e="T321" id="Seg_8736" s="T320">так-INTNS</ta>
            <ta e="T322" id="Seg_8737" s="T321">что.[NOM]</ta>
            <ta e="T323" id="Seg_8738" s="T322">состязание-3SG.[NOM]=Q</ta>
            <ta e="T324" id="Seg_8739" s="T323">думать-CVB.SEQ</ta>
            <ta e="T325" id="Seg_8740" s="T324">удивляться-CVB.SIM</ta>
            <ta e="T326" id="Seg_8741" s="T325">идти-PST1-3SG</ta>
            <ta e="T327" id="Seg_8742" s="T326">голова.[NOM]</ta>
            <ta e="T328" id="Seg_8743" s="T327">становиться-CVB.SEQ</ta>
            <ta e="T329" id="Seg_8744" s="T328">кататься-CVB.SEQ</ta>
            <ta e="T330" id="Seg_8745" s="T329">идти-PST1-3SG</ta>
            <ta e="T331" id="Seg_8746" s="T330">только</ta>
            <ta e="T332" id="Seg_8747" s="T331">голова.[NOM]</ta>
            <ta e="T333" id="Seg_8748" s="T332">лежать.затверденно-CVB.SIM</ta>
            <ta e="T334" id="Seg_8749" s="T333">лежать-PRS.[3SG]</ta>
            <ta e="T335" id="Seg_8750" s="T334">вот</ta>
            <ta e="T336" id="Seg_8751" s="T335">достигать-PST1-1SG</ta>
            <ta e="T337" id="Seg_8752" s="T336">MOD</ta>
            <ta e="T338" id="Seg_8753" s="T337">вдруг</ta>
            <ta e="T339" id="Seg_8754" s="T338">два</ta>
            <ta e="T340" id="Seg_8755" s="T339">глаз-3SG.[NOM]</ta>
            <ta e="T341" id="Seg_8756" s="T340">снимать-REFL-CVB.SEQ</ta>
            <ta e="T342" id="Seg_8757" s="T341">идти-PST2.[3SG]</ta>
            <ta e="T343" id="Seg_8758" s="T342">вот.беда</ta>
            <ta e="T344" id="Seg_8759" s="T343">этот</ta>
            <ta e="T345" id="Seg_8760" s="T344">что.[NOM]</ta>
            <ta e="T346" id="Seg_8761" s="T345">человек-3SG.[NOM]=Q</ta>
            <ta e="T347" id="Seg_8762" s="T346">хороший.дух-ACC</ta>
            <ta e="T348" id="Seg_8763" s="T347">следовать-PRS-1SG</ta>
            <ta e="T349" id="Seg_8764" s="T348">Q</ta>
            <ta e="T350" id="Seg_8765" s="T349">злой.дух-ACC</ta>
            <ta e="T351" id="Seg_8766" s="T350">Q</ta>
            <ta e="T352" id="Seg_8767" s="T351">говорить-PRS.[3SG]</ta>
            <ta e="T353" id="Seg_8768" s="T352">два</ta>
            <ta e="T354" id="Seg_8769" s="T353">глаз-3SG.[NOM]</ta>
            <ta e="T355" id="Seg_8770" s="T354">кататься-CVB.SEQ</ta>
            <ta e="T356" id="Seg_8771" s="T355">идти-PST1-3SG</ta>
            <ta e="T357" id="Seg_8772" s="T356">глаз-PL.[NOM]</ta>
            <ta e="T358" id="Seg_8773" s="T357">дорога-3PL-INSTR</ta>
            <ta e="T359" id="Seg_8774" s="T358">когда</ta>
            <ta e="T360" id="Seg_8775" s="T359">INDEF</ta>
            <ta e="T361" id="Seg_8776" s="T360">этот</ta>
            <ta e="T362" id="Seg_8777" s="T361">два</ta>
            <ta e="T363" id="Seg_8778" s="T362">глаз.[NOM]</ta>
            <ta e="T364" id="Seg_8779" s="T363">след-3SG.[NOM]</ta>
            <ta e="T365" id="Seg_8780" s="T364">один</ta>
            <ta e="T366" id="Seg_8781" s="T365">дом-DIM-DAT/LOC</ta>
            <ta e="T367" id="Seg_8782" s="T366">доезжать-PRS.[3SG]</ta>
            <ta e="T368" id="Seg_8783" s="T367">этот</ta>
            <ta e="T369" id="Seg_8784" s="T368">доезжать-CVB.SEQ</ta>
            <ta e="T370" id="Seg_8785" s="T369">два</ta>
            <ta e="T371" id="Seg_8786" s="T370">глаз.[NOM]</ta>
            <ta e="T372" id="Seg_8787" s="T371">след-3SG.[NOM]</ta>
            <ta e="T373" id="Seg_8788" s="T372">дом-DAT/LOC</ta>
            <ta e="T374" id="Seg_8789" s="T373">входить-PST2.[3SG]</ta>
            <ta e="T375" id="Seg_8790" s="T374">вот</ta>
            <ta e="T376" id="Seg_8791" s="T375">последний-2SG.[NOM]</ta>
            <ta e="T377" id="Seg_8792" s="T376">MOD</ta>
            <ta e="T378" id="Seg_8793" s="T377">два</ta>
            <ta e="T379" id="Seg_8794" s="T378">задний</ta>
            <ta e="T380" id="Seg_8795" s="T379">глаз.[NOM]</ta>
            <ta e="T381" id="Seg_8796" s="T380">дом-DAT/LOC</ta>
            <ta e="T382" id="Seg_8797" s="T381">входить-PST1-3SG</ta>
            <ta e="T383" id="Seg_8798" s="T382">входить-PST2-3SG</ta>
            <ta e="T384" id="Seg_8799" s="T383">правый</ta>
            <ta e="T385" id="Seg_8800" s="T384">в.сторону</ta>
            <ta e="T386" id="Seg_8801" s="T385">постель-DAT/LOC</ta>
            <ta e="T387" id="Seg_8802" s="T386">старуха.[NOM]</ta>
            <ta e="T388" id="Seg_8803" s="T387">сидеть-PRS.[3SG]</ta>
            <ta e="T389" id="Seg_8804" s="T388">глаз-3PL.[NOM]</ta>
            <ta e="T390" id="Seg_8805" s="T389">задняя.часть.юрты.[NOM]</ta>
            <ta e="T391" id="Seg_8806" s="T390">поблескивать-CVB.SIM</ta>
            <ta e="T392" id="Seg_8807" s="T391">лежать-PRS-3PL</ta>
            <ta e="T393" id="Seg_8808" s="T392">тот.самый</ta>
            <ta e="T394" id="Seg_8809" s="T393">два</ta>
            <ta e="T395" id="Seg_8810" s="T394">глаз.[NOM]</ta>
            <ta e="T396" id="Seg_8811" s="T395">входить-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T397" id="Seg_8812" s="T396">передний</ta>
            <ta e="T398" id="Seg_8813" s="T397">два</ta>
            <ta e="T399" id="Seg_8814" s="T398">глаз.[NOM]</ta>
            <ta e="T400" id="Seg_8815" s="T399">язык-PROPR.[NOM]</ta>
            <ta e="T401" id="Seg_8816" s="T400">о</ta>
            <ta e="T402" id="Seg_8817" s="T401">да</ta>
            <ta e="T403" id="Seg_8818" s="T402">человек-ACC</ta>
            <ta e="T404" id="Seg_8819" s="T403">следовать-PTCP.PRS</ta>
            <ta e="T405" id="Seg_8820" s="T404">человек.[NOM]</ta>
            <ta e="T406" id="Seg_8821" s="T405">устать-HAB.[3SG]</ta>
            <ta e="T407" id="Seg_8822" s="T406">2SG.[NOM]</ta>
            <ta e="T408" id="Seg_8823" s="T407">отдыхать.[IMP.2SG]</ta>
            <ta e="T409" id="Seg_8824" s="T408">1SG.[NOM]</ta>
            <ta e="T410" id="Seg_8825" s="T409">туша-PL-1PL-ACC</ta>
            <ta e="T411" id="Seg_8826" s="T410">голова-PL-1PL-ACC</ta>
            <ta e="T412" id="Seg_8827" s="T411">собирать-ITER-CVB.SEQ</ta>
            <ta e="T413" id="Seg_8828" s="T412">принести-FUT-1SG</ta>
            <ta e="T414" id="Seg_8829" s="T413">говорить-PRS.[3SG]</ta>
            <ta e="T415" id="Seg_8830" s="T414">друг-3SG.[NOM]</ta>
            <ta e="T416" id="Seg_8831" s="T415">Железная.Шапка.[NOM]</ta>
            <ta e="T417" id="Seg_8832" s="T416">тот-DAT/LOC</ta>
            <ta e="T418" id="Seg_8833" s="T417">этот</ta>
            <ta e="T419" id="Seg_8834" s="T418">настоящий</ta>
            <ta e="T420" id="Seg_8835" s="T419">ум-EP-INSTR</ta>
            <ta e="T421" id="Seg_8836" s="T420">идти-PTCP.PRS</ta>
            <ta e="T422" id="Seg_8837" s="T421">человек-2SG</ta>
            <ta e="T423" id="Seg_8838" s="T422">Q</ta>
            <ta e="T424" id="Seg_8839" s="T423">NEG</ta>
            <ta e="T425" id="Seg_8840" s="T424">Q</ta>
            <ta e="T426" id="Seg_8841" s="T425">этот</ta>
            <ta e="T427" id="Seg_8842" s="T426">1PL.[NOM]</ta>
            <ta e="T428" id="Seg_8843" s="T427">человек.[NOM]</ta>
            <ta e="T429" id="Seg_8844" s="T428">мочь-CVB.SEQ</ta>
            <ta e="T430" id="Seg_8845" s="T429">возвращаться-EP-NEG.PTCP</ta>
            <ta e="T431" id="Seg_8846" s="T430">место-3SG-DAT/LOC</ta>
            <ta e="T432" id="Seg_8847" s="T431">приходить-PST1-1PL</ta>
            <ta e="T433" id="Seg_8848" s="T432">вот</ta>
            <ta e="T434" id="Seg_8849" s="T433">теперь</ta>
            <ta e="T435" id="Seg_8850" s="T434">2SG.[NOM]</ta>
            <ta e="T436" id="Seg_8851" s="T435">следовать-FUT.[IMP.2SG]</ta>
            <ta e="T437" id="Seg_8852" s="T436">1SG-ACC</ta>
            <ta e="T438" id="Seg_8853" s="T437">догонять-FUT.[IMP.2SG]</ta>
            <ta e="T439" id="Seg_8854" s="T438">1SG.[NOM]</ta>
            <ta e="T440" id="Seg_8855" s="T439">способность-1SG-ACC</ta>
            <ta e="T441" id="Seg_8856" s="T440">знать-PRS-2SG</ta>
            <ta e="T442" id="Seg_8857" s="T441">этот</ta>
            <ta e="T443" id="Seg_8858" s="T442">пурга</ta>
            <ta e="T444" id="Seg_8859" s="T443">господин-3SG-DAT/LOC</ta>
            <ta e="T445" id="Seg_8860" s="T444">приходить-PST1-1PL</ta>
            <ta e="T446" id="Seg_8861" s="T445">утром</ta>
            <ta e="T447" id="Seg_8862" s="T446">рано-AG.[NOM]</ta>
            <ta e="T448" id="Seg_8863" s="T447">человек.[NOM]</ta>
            <ta e="T449" id="Seg_8864" s="T448">стоять-PTCP.PRS</ta>
            <ta e="T450" id="Seg_8865" s="T449">час-3SG-DAT/LOC</ta>
            <ta e="T451" id="Seg_8866" s="T450">стоять-CVB.SEQ</ta>
            <ta e="T452" id="Seg_8867" s="T451">выйти-FUT-1SG</ta>
            <ta e="T453" id="Seg_8868" s="T452">этот-DAT/LOC</ta>
            <ta e="T454" id="Seg_8869" s="T453">сто-DISTR</ta>
            <ta e="T455" id="Seg_8870" s="T454">караван-PROPR</ta>
            <ta e="T456" id="Seg_8871" s="T455">нарта-PROPR</ta>
            <ta e="T457" id="Seg_8872" s="T456">два</ta>
            <ta e="T458" id="Seg_8873" s="T457">девушка-PL.[NOM]</ta>
            <ta e="T459" id="Seg_8874" s="T458">приходить-FUT-3PL</ta>
            <ta e="T460" id="Seg_8875" s="T459">этот-DAT/LOC</ta>
            <ta e="T461" id="Seg_8876" s="T460">1SG.[NOM]</ta>
            <ta e="T462" id="Seg_8877" s="T461">выйти-CVB.SIM</ta>
            <ta e="T463" id="Seg_8878" s="T462">бежать-CVB.SEQ-1SG</ta>
            <ta e="T464" id="Seg_8879" s="T463">передовой</ta>
            <ta e="T465" id="Seg_8880" s="T464">девушка-ACC</ta>
            <ta e="T466" id="Seg_8881" s="T465">передовой.олень-3SG-ACC</ta>
            <ta e="T467" id="Seg_8882" s="T466">шлейка-3SG-ABL</ta>
            <ta e="T468" id="Seg_8883" s="T467">держать-FUT-1SG</ta>
            <ta e="T469" id="Seg_8884" s="T468">да</ta>
            <ta e="T470" id="Seg_8885" s="T469">мчаться-FUT-1SG</ta>
            <ta e="T471" id="Seg_8886" s="T470">этот</ta>
            <ta e="T472" id="Seg_8887" s="T471">сто</ta>
            <ta e="T473" id="Seg_8888" s="T472">нарта-ABL</ta>
            <ta e="T474" id="Seg_8889" s="T473">один-3SG-DAT/LOC</ta>
            <ta e="T475" id="Seg_8890" s="T474">INDEF</ta>
            <ta e="T476" id="Seg_8891" s="T475">закреплять-EP-REFL-TEMP-2SG</ta>
            <ta e="T477" id="Seg_8892" s="T476">доезжать-MED-FUT-2SG</ta>
            <ta e="T478" id="Seg_8893" s="T477">страна-2SG-DAT/LOC</ta>
            <ta e="T479" id="Seg_8894" s="T478">потом</ta>
            <ta e="T480" id="Seg_8895" s="T479">закреплять-EP-REFL-NEG-TEMP-2SG</ta>
            <ta e="T481" id="Seg_8896" s="T480">этот</ta>
            <ta e="T482" id="Seg_8897" s="T481">мир-DAT/LOC</ta>
            <ta e="T483" id="Seg_8898" s="T482">умирать-FUT-2SG</ta>
            <ta e="T484" id="Seg_8899" s="T483">говорить-PRS.[3SG]</ta>
            <ta e="T485" id="Seg_8900" s="T484">вот</ta>
            <ta e="T486" id="Seg_8901" s="T485">друг-3SG.[NOM]</ta>
            <ta e="T487" id="Seg_8902" s="T486">Костяной.Пояс.[NOM]</ta>
            <ta e="T488" id="Seg_8903" s="T487">туша-ACC</ta>
            <ta e="T489" id="Seg_8904" s="T488">нога-PL-ACC</ta>
            <ta e="T490" id="Seg_8905" s="T489">голова-PL-ACC</ta>
            <ta e="T491" id="Seg_8906" s="T490">принести-PST1-3SG</ta>
            <ta e="T492" id="Seg_8907" s="T491">сам-3PL.[NOM]</ta>
            <ta e="T493" id="Seg_8908" s="T492">сам-3PL-INSTR</ta>
            <ta e="T494" id="Seg_8909" s="T493">человек.[NOM]</ta>
            <ta e="T495" id="Seg_8910" s="T494">быть-EMOT-CVB.SEQ</ta>
            <ta e="T496" id="Seg_8911" s="T495">оставаться-PST1-3PL</ta>
            <ta e="T497" id="Seg_8912" s="T496">спать-ITER-CVB.SEQ</ta>
            <ta e="T498" id="Seg_8913" s="T497">после</ta>
            <ta e="T499" id="Seg_8914" s="T498">Костяной.Пояс.[NOM]</ta>
            <ta e="T500" id="Seg_8915" s="T499">просыпаться-PRS.[3SG]</ta>
            <ta e="T501" id="Seg_8916" s="T500">друг-3SG.[NOM]</ta>
            <ta e="T502" id="Seg_8917" s="T501">выйти-EP-PTCP.PST</ta>
            <ta e="T503" id="Seg_8918" s="T502">шум-3SG-DAT/LOC</ta>
            <ta e="T504" id="Seg_8919" s="T503">Костяной.Пояс.[NOM]</ta>
            <ta e="T505" id="Seg_8920" s="T504">выйти-CVB.SIM</ta>
            <ta e="T506" id="Seg_8921" s="T505">бежать-PST2-3SG</ta>
            <ta e="T507" id="Seg_8922" s="T506">сто</ta>
            <ta e="T508" id="Seg_8923" s="T507">нарта.[NOM]</ta>
            <ta e="T509" id="Seg_8924" s="T508">мелькать-EP-MED-CVB.SEQ</ta>
            <ta e="T510" id="Seg_8925" s="T509">быть-TEMP-3SG</ta>
            <ta e="T511" id="Seg_8926" s="T510">последний</ta>
            <ta e="T512" id="Seg_8927" s="T511">нарта-DAT/LOC</ta>
            <ta e="T513" id="Seg_8928" s="T512">вешаться-PST1-3SG</ta>
            <ta e="T514" id="Seg_8929" s="T513">небо-EP-INSTR</ta>
            <ta e="T515" id="Seg_8930" s="T514">да</ta>
            <ta e="T516" id="Seg_8931" s="T515">земля-EP-INSTR</ta>
            <ta e="T517" id="Seg_8932" s="T516">да</ta>
            <ta e="T518" id="Seg_8933" s="T517">идти-PTCP.PRS-3SG-ACC</ta>
            <ta e="T519" id="Seg_8934" s="T518">судить-PST2.NEG-3SG</ta>
            <ta e="T520" id="Seg_8935" s="T519">EMPH</ta>
            <ta e="T521" id="Seg_8936" s="T520">позже</ta>
            <ta e="T522" id="Seg_8937" s="T521">EMPH</ta>
            <ta e="T523" id="Seg_8938" s="T522">потом</ta>
            <ta e="T524" id="Seg_8939" s="T523">сто</ta>
            <ta e="T525" id="Seg_8940" s="T524">нарта.[NOM]</ta>
            <ta e="T526" id="Seg_8941" s="T525">останавливаться-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T527" id="Seg_8942" s="T526">видеть-PST2-3SG</ta>
            <ta e="T528" id="Seg_8943" s="T527">друг-3SG.[NOM]</ta>
            <ta e="T529" id="Seg_8944" s="T528">страна-3SG-DAT/LOC</ta>
            <ta e="T530" id="Seg_8945" s="T529">доводить-PST2.[3SG]</ta>
            <ta e="T531" id="Seg_8946" s="T530">вот</ta>
            <ta e="T532" id="Seg_8947" s="T531">Железная.Шапка.[NOM]</ta>
            <ta e="T533" id="Seg_8948" s="T532">говорить-PRS.[3SG]</ta>
            <ta e="T534" id="Seg_8949" s="T533">друг-3SG-ACC</ta>
            <ta e="T535" id="Seg_8950" s="T534">вот</ta>
            <ta e="T536" id="Seg_8951" s="T535">тот.[NOM]</ta>
            <ta e="T537" id="Seg_8952" s="T536">сто</ta>
            <ta e="T538" id="Seg_8953" s="T537">нарта.[NOM]</ta>
            <ta e="T539" id="Seg_8954" s="T538">другой.из.двух</ta>
            <ta e="T540" id="Seg_8955" s="T539">сторона-3SG-DAT/LOC</ta>
            <ta e="T541" id="Seg_8956" s="T540">три</ta>
            <ta e="T542" id="Seg_8957" s="T541">белый</ta>
            <ta e="T543" id="Seg_8958" s="T542">олений.бык.[NOM]</ta>
            <ta e="T544" id="Seg_8959" s="T543">запрячь-PASS/REFL-CVB.SEQ</ta>
            <ta e="T545" id="Seg_8960" s="T544">стоять-PRS.[3SG]</ta>
            <ta e="T546" id="Seg_8961" s="T545">другой.из.двух</ta>
            <ta e="T547" id="Seg_8962" s="T546">сторона-3SG-DAT/LOC</ta>
            <ta e="T548" id="Seg_8963" s="T547">три</ta>
            <ta e="T549" id="Seg_8964" s="T548">черный</ta>
            <ta e="T550" id="Seg_8965" s="T549">олений.бык.[NOM]</ta>
            <ta e="T551" id="Seg_8966" s="T550">стоять-PRS.[3SG]</ta>
            <ta e="T552" id="Seg_8967" s="T551">вот</ta>
            <ta e="T553" id="Seg_8968" s="T552">1SG.[NOM]</ta>
            <ta e="T554" id="Seg_8969" s="T553">тот</ta>
            <ta e="T555" id="Seg_8970" s="T554">три</ta>
            <ta e="T556" id="Seg_8971" s="T555">белый</ta>
            <ta e="T557" id="Seg_8972" s="T556">олений.бык-ACC</ta>
            <ta e="T558" id="Seg_8973" s="T557">убить-FUT-1SG</ta>
            <ta e="T559" id="Seg_8974" s="T558">кожа-3PL-ACC</ta>
            <ta e="T560" id="Seg_8975" s="T559">стягивать-FUT-1SG</ta>
            <ta e="T561" id="Seg_8976" s="T560">2SG.[NOM]</ta>
            <ta e="T562" id="Seg_8977" s="T561">три</ta>
            <ta e="T563" id="Seg_8978" s="T562">черный-ACC</ta>
            <ta e="T564" id="Seg_8979" s="T563">убить-FUT-2SG</ta>
            <ta e="T565" id="Seg_8980" s="T564">сдирать.кожу-FUT-2SG</ta>
            <ta e="T566" id="Seg_8981" s="T565">кожа-3PL-ACC</ta>
            <ta e="T567" id="Seg_8982" s="T566">знать-PRS-2SG</ta>
            <ta e="T568" id="Seg_8983" s="T567">этот</ta>
            <ta e="T569" id="Seg_8984" s="T568">пурга.[NOM]</ta>
            <ta e="T570" id="Seg_8985" s="T569">господин-3SG-GEN</ta>
            <ta e="T571" id="Seg_8986" s="T570">маленький</ta>
            <ta e="T572" id="Seg_8987" s="T571">дочь-PL-3SG-ACC</ta>
            <ta e="T573" id="Seg_8988" s="T572">принести-PST1-1SG</ta>
            <ta e="T574" id="Seg_8989" s="T573">волшебство-1SG-INSTR</ta>
            <ta e="T575" id="Seg_8990" s="T574">теперь</ta>
            <ta e="T576" id="Seg_8991" s="T575">тот-3SG-3PL-ACC</ta>
            <ta e="T577" id="Seg_8992" s="T576">следовать-CVB.SEQ</ta>
            <ta e="T578" id="Seg_8993" s="T577">три</ta>
            <ta e="T579" id="Seg_8994" s="T578">старший.брат-3SG.[NOM]</ta>
            <ta e="T580" id="Seg_8995" s="T579">пурга</ta>
            <ta e="T581" id="Seg_8996" s="T580">господин-PL-3SG.[NOM]</ta>
            <ta e="T582" id="Seg_8997" s="T581">приходить-FUT-3PL</ta>
            <ta e="T583" id="Seg_8998" s="T582">тот</ta>
            <ta e="T584" id="Seg_8999" s="T583">человек-PL.[NOM]</ta>
            <ta e="T585" id="Seg_9000" s="T584">приходить-TEMP-3PL</ta>
            <ta e="T586" id="Seg_9001" s="T585">задняя.часть.юрты.[NOM]</ta>
            <ta e="T587" id="Seg_9002" s="T586">три</ta>
            <ta e="T588" id="Seg_9003" s="T587">белый</ta>
            <ta e="T589" id="Seg_9004" s="T588">шкура-ACC</ta>
            <ta e="T590" id="Seg_9005" s="T589">постелить-FUT-1SG</ta>
            <ta e="T591" id="Seg_9006" s="T590">2SG.[NOM]</ta>
            <ta e="T592" id="Seg_9007" s="T591">тот.[NOM]</ta>
            <ta e="T593" id="Seg_9008" s="T592">три</ta>
            <ta e="T594" id="Seg_9009" s="T593">черный</ta>
            <ta e="T595" id="Seg_9010" s="T594">кожа-2SG-ACC</ta>
            <ta e="T596" id="Seg_9011" s="T595">постелить-FUT.[IMP.2SG]</ta>
            <ta e="T597" id="Seg_9012" s="T596">вот</ta>
            <ta e="T598" id="Seg_9013" s="T597">большой</ta>
            <ta e="T599" id="Seg_9014" s="T598">большой.котел-2SG-DAT/LOC</ta>
            <ta e="T600" id="Seg_9015" s="T599">полный</ta>
            <ta e="T601" id="Seg_9016" s="T600">вода-3SG.[NOM]</ta>
            <ta e="T602" id="Seg_9017" s="T601">кипеть-CAUS.[IMP.2SG]</ta>
            <ta e="T603" id="Seg_9018" s="T602">1SG.[NOM]</ta>
            <ta e="T604" id="Seg_9019" s="T603">опять</ta>
            <ta e="T605" id="Seg_9020" s="T604">кипеть-CAUS-FUT-1SG</ta>
            <ta e="T606" id="Seg_9021" s="T605">точно</ta>
            <ta e="T607" id="Seg_9022" s="T606">сначала</ta>
            <ta e="T608" id="Seg_9023" s="T607">1SG-DAT/LOC</ta>
            <ta e="T609" id="Seg_9024" s="T608">гость-VBZ-FUT-3PL</ta>
            <ta e="T610" id="Seg_9025" s="T609">1SG-ABL</ta>
            <ta e="T611" id="Seg_9026" s="T610">наесться-CVB.SEQ</ta>
            <ta e="T612" id="Seg_9027" s="T611">быть.довольиым-CVB.SEQ</ta>
            <ta e="T613" id="Seg_9028" s="T612">выйти-TEMP-3PL</ta>
            <ta e="T614" id="Seg_9029" s="T613">2SG-DAT/LOC</ta>
            <ta e="T615" id="Seg_9030" s="T614">гость-VBZ-FUT-3PL</ta>
            <ta e="T616" id="Seg_9031" s="T615">1SG.[NOM]</ta>
            <ta e="T617" id="Seg_9032" s="T616">3PL-DAT/LOC</ta>
            <ta e="T618" id="Seg_9033" s="T617">что-ACC</ta>
            <ta e="T619" id="Seg_9034" s="T618">INDEF</ta>
            <ta e="T620" id="Seg_9035" s="T619">показывать-FUT-1SG</ta>
            <ta e="T621" id="Seg_9036" s="T620">входить-RECP/COLL-FUT.[IMP.2SG]</ta>
            <ta e="T622" id="Seg_9037" s="T621">1SG.[NOM]</ta>
            <ta e="T623" id="Seg_9038" s="T622">что-ACC</ta>
            <ta e="T624" id="Seg_9039" s="T623">видеть-CAUS-PRS-1SG</ta>
            <ta e="T625" id="Seg_9040" s="T624">да</ta>
            <ta e="T626" id="Seg_9041" s="T625">подражать-CVB.SEQ</ta>
            <ta e="T627" id="Seg_9042" s="T626">3PL-DAT/LOC</ta>
            <ta e="T628" id="Seg_9043" s="T627">видеть-CAUS-FUT.[IMP.2SG]</ta>
            <ta e="T629" id="Seg_9044" s="T628">говорить-PST1-3SG</ta>
            <ta e="T630" id="Seg_9045" s="T629">учить-PST1-3SG</ta>
            <ta e="T631" id="Seg_9046" s="T630">друг-3SG-ACC</ta>
            <ta e="T632" id="Seg_9047" s="T631">сколько</ta>
            <ta e="T633" id="Seg_9048" s="T632">NEG</ta>
            <ta e="T634" id="Seg_9049" s="T633">быть-EP-NEG.CVB</ta>
            <ta e="T635" id="Seg_9050" s="T634">пурга.[NOM]</ta>
            <ta e="T636" id="Seg_9051" s="T635">господин-PL-3SG.[NOM]</ta>
            <ta e="T637" id="Seg_9052" s="T636">вихрь-INSTR</ta>
            <ta e="T638" id="Seg_9053" s="T637">приходить-PST1-3PL</ta>
            <ta e="T639" id="Seg_9054" s="T638">сплошной</ta>
            <ta e="T640" id="Seg_9055" s="T639">лед.[NOM]</ta>
            <ta e="T641" id="Seg_9056" s="T640">люди-PL.[NOM]</ta>
            <ta e="T642" id="Seg_9057" s="T641">входить-CVB.SEQ-3PL</ta>
            <ta e="T643" id="Seg_9058" s="T642">насквозь-EMPH-насквозь</ta>
            <ta e="T644" id="Seg_9059" s="T643">здравствуй</ta>
            <ta e="T645" id="Seg_9060" s="T644">NEG</ta>
            <ta e="T646" id="Seg_9061" s="T645">говорить-NEG-3PL</ta>
            <ta e="T647" id="Seg_9062" s="T646">Железная.Шапка.[NOM]</ta>
            <ta e="T648" id="Seg_9063" s="T647">дом-3SG-DAT/LOC</ta>
            <ta e="T649" id="Seg_9064" s="T648">три</ta>
            <ta e="T650" id="Seg_9065" s="T649">белый</ta>
            <ta e="T651" id="Seg_9066" s="T650">кожа-DAT/LOC</ta>
            <ta e="T652" id="Seg_9067" s="T651">сесть-PRS-3PL</ta>
            <ta e="T653" id="Seg_9068" s="T652">Железная.Шапка.[NOM]</ta>
            <ta e="T654" id="Seg_9069" s="T653">способность-VBZ-CAP.[3SG]</ta>
            <ta e="T655" id="Seg_9070" s="T654">этот</ta>
            <ta e="T656" id="Seg_9071" s="T655">народ-DAT/LOC</ta>
            <ta e="T657" id="Seg_9072" s="T656">кипеть-CVB.SIM</ta>
            <ta e="T658" id="Seg_9073" s="T657">стоять-PTCP.PRS</ta>
            <ta e="T659" id="Seg_9074" s="T658">большой</ta>
            <ta e="T660" id="Seg_9075" s="T659">большой.котел-DAT/LOC</ta>
            <ta e="T661" id="Seg_9076" s="T660">костяной</ta>
            <ta e="T662" id="Seg_9077" s="T661">клюв-PROPR</ta>
            <ta e="T663" id="Seg_9078" s="T662">чернозобая.гагара.[NOM]</ta>
            <ta e="T664" id="Seg_9079" s="T663">становиться-CVB.ANT</ta>
            <ta e="T665" id="Seg_9080" s="T664">аа_уу</ta>
            <ta e="T666" id="Seg_9081" s="T665">говорить-CVB.ANT</ta>
            <ta e="T667" id="Seg_9082" s="T666">нырять-CVB.SEQ</ta>
            <ta e="T668" id="Seg_9083" s="T667">оставаться-PRS.[3SG]</ta>
            <ta e="T669" id="Seg_9084" s="T668">размахивать.крыльями-CVB.SIM-размахивать.крыльями-PTCP.PST-EP-INSTR</ta>
            <ta e="T670" id="Seg_9085" s="T669">летать-CVB.SEQ</ta>
            <ta e="T671" id="Seg_9086" s="T670">выйти-CVB.SEQ</ta>
            <ta e="T672" id="Seg_9087" s="T671">мать-3SG-GEN</ta>
            <ta e="T673" id="Seg_9088" s="T672">желудок-3SG-INSTR</ta>
            <ta e="T674" id="Seg_9089" s="T673">ONOM</ta>
            <ta e="T675" id="Seg_9090" s="T674">делать-PRS.[3SG]</ta>
            <ta e="T676" id="Seg_9091" s="T675">потом</ta>
            <ta e="T677" id="Seg_9092" s="T676">мать-3SG-GEN</ta>
            <ta e="T678" id="Seg_9093" s="T677">рот-3SG-ACC</ta>
            <ta e="T679" id="Seg_9094" s="T678">через</ta>
            <ta e="T680" id="Seg_9095" s="T679">выйти-CVB.SIM</ta>
            <ta e="T681" id="Seg_9096" s="T680">летать-PST2.[3SG]</ta>
            <ta e="T682" id="Seg_9097" s="T681">вот</ta>
            <ta e="T683" id="Seg_9098" s="T682">потом</ta>
            <ta e="T684" id="Seg_9099" s="T683">отец-3SG-GEN</ta>
            <ta e="T685" id="Seg_9100" s="T684">живот-3SG-INSTR</ta>
            <ta e="T686" id="Seg_9101" s="T685">ONOM</ta>
            <ta e="T687" id="Seg_9102" s="T686">делать-PST2.[3SG]</ta>
            <ta e="T688" id="Seg_9103" s="T687">отец-3SG-GEN</ta>
            <ta e="T689" id="Seg_9104" s="T688">рот-3SG-INSTR</ta>
            <ta e="T690" id="Seg_9105" s="T689">только.что</ta>
            <ta e="T691" id="Seg_9106" s="T690">рог-3SG-ACC</ta>
            <ta e="T692" id="Seg_9107" s="T691">сдирать-PTCP.PST</ta>
            <ta e="T693" id="Seg_9108" s="T692">дикий</ta>
            <ta e="T694" id="Seg_9109" s="T693">олений.бык-3SG.[NOM]</ta>
            <ta e="T695" id="Seg_9110" s="T694">быть-CVB.SEQ</ta>
            <ta e="T696" id="Seg_9111" s="T695">выйти-PRS.[3SG]</ta>
            <ta e="T697" id="Seg_9112" s="T696">этот.[NOM]</ta>
            <ta e="T698" id="Seg_9113" s="T697">задняя.часть-ABL</ta>
            <ta e="T699" id="Seg_9114" s="T698">Железная.Шапка.[NOM]</ta>
            <ta e="T700" id="Seg_9115" s="T699">человек.[NOM]</ta>
            <ta e="T701" id="Seg_9116" s="T700">становиться-PRS.[3SG]</ta>
            <ta e="T702" id="Seg_9117" s="T701">да</ta>
            <ta e="T703" id="Seg_9118" s="T702">лук-INSTR</ta>
            <ta e="T704" id="Seg_9119" s="T703">этот</ta>
            <ta e="T705" id="Seg_9120" s="T704">дикий</ta>
            <ta e="T706" id="Seg_9121" s="T705">олений.бык-3SG-ACC</ta>
            <ta e="T707" id="Seg_9122" s="T706">сквозь</ta>
            <ta e="T708" id="Seg_9123" s="T707">стрелять-CVB.SEQ</ta>
            <ta e="T709" id="Seg_9124" s="T708">спустить-PRS.[3SG]</ta>
            <ta e="T710" id="Seg_9125" s="T709">убить-PRS.[3SG]</ta>
            <ta e="T711" id="Seg_9126" s="T710">да</ta>
            <ta e="T712" id="Seg_9127" s="T711">этот-DAT/LOC</ta>
            <ta e="T713" id="Seg_9128" s="T712">стягивать-EP-CAUS-EP-CAUS-CVB.SIM</ta>
            <ta e="T714" id="Seg_9129" s="T713">сдирать.кожу-CVB.SEQ</ta>
            <ta e="T715" id="Seg_9130" s="T714">дерево.[NOM]</ta>
            <ta e="T716" id="Seg_9131" s="T715">миска-DAT/LOC</ta>
            <ta e="T717" id="Seg_9132" s="T716">сырой-VBZ-CVB.SIM-резать-NMNZ-VBZ-CVB.SIM</ta>
            <ta e="T718" id="Seg_9133" s="T717">тянуть-CVB.SEQ</ta>
            <ta e="T719" id="Seg_9134" s="T718">бросать-PRS.[3SG]</ta>
            <ta e="T720" id="Seg_9135" s="T719">гость-PL-DAT/LOC</ta>
            <ta e="T721" id="Seg_9136" s="T720">сырое.мясо-VBZ-EP-CAUS-CVB.SEQ</ta>
            <ta e="T722" id="Seg_9137" s="T721">гость-3PL.[NOM]</ta>
            <ta e="T723" id="Seg_9138" s="T722">слово.[NOM]-язык.[NOM]</ta>
            <ta e="T724" id="Seg_9139" s="T723">NEG.EX</ta>
            <ta e="T725" id="Seg_9140" s="T724">есть-PTCP.PRS-DAT/LOC</ta>
            <ta e="T726" id="Seg_9141" s="T725">идти-PST2-3PL</ta>
            <ta e="T727" id="Seg_9142" s="T726">осенний</ta>
            <ta e="T728" id="Seg_9143" s="T727">олений.бык-ABL</ta>
            <ta e="T729" id="Seg_9144" s="T728">вообще</ta>
            <ta e="T730" id="Seg_9145" s="T729">рог-PROPR</ta>
            <ta e="T731" id="Seg_9146" s="T730">копыто-3SG-ACC</ta>
            <ta e="T732" id="Seg_9147" s="T731">быть.лишным-CAUS-CVB.SEQ</ta>
            <ta e="T733" id="Seg_9148" s="T732">бросать-PST1-3PL</ta>
            <ta e="T734" id="Seg_9149" s="T733">Костяной.Пояс.[NOM]</ta>
            <ta e="T735" id="Seg_9150" s="T734">видеть-CVB.SEQ</ta>
            <ta e="T736" id="Seg_9151" s="T735">сесть-EP-MED-FREQ-PST1-3SG</ta>
            <ta e="T737" id="Seg_9152" s="T736">есть-CVB.SEQ</ta>
            <ta e="T738" id="Seg_9153" s="T737">кончать-PRS-3PL</ta>
            <ta e="T739" id="Seg_9154" s="T738">да</ta>
            <ta e="T740" id="Seg_9155" s="T739">губа-3PL-ACC</ta>
            <ta e="T741" id="Seg_9156" s="T740">вытирать-ITER-CVB.SEQ</ta>
            <ta e="T742" id="Seg_9157" s="T741">выйти-PRS-3PL</ta>
            <ta e="T743" id="Seg_9158" s="T742">Костяной.Пояс.[NOM]</ta>
            <ta e="T744" id="Seg_9159" s="T743">дом-3SG-DAT/LOC</ta>
            <ta e="T745" id="Seg_9160" s="T744">прыгать-PRS.[3SG]</ta>
            <ta e="T746" id="Seg_9161" s="T745">гость-PL-ACC</ta>
            <ta e="T747" id="Seg_9162" s="T746">принимать-EP-MED-CVB.SIM</ta>
            <ta e="T748" id="Seg_9163" s="T747">гость-3PL.[NOM]</ta>
            <ta e="T749" id="Seg_9164" s="T748">входить-CVB.SEQ</ta>
            <ta e="T750" id="Seg_9165" s="T749">три</ta>
            <ta e="T751" id="Seg_9166" s="T750">черный</ta>
            <ta e="T752" id="Seg_9167" s="T751">кожа-DAT/LOC</ta>
            <ta e="T753" id="Seg_9168" s="T752">сидеть-CVB.SIM</ta>
            <ta e="T754" id="Seg_9169" s="T753">давать-PRS-3PL</ta>
            <ta e="T755" id="Seg_9170" s="T754">Костяной.Пояс.[NOM]</ta>
            <ta e="T756" id="Seg_9171" s="T755">аа_уу</ta>
            <ta e="T757" id="Seg_9172" s="T756">говорить-CVB.ANT</ta>
            <ta e="T758" id="Seg_9173" s="T757">краснозобая.гагара-DIM.[NOM]</ta>
            <ta e="T759" id="Seg_9174" s="T758">становиться-CVB.ANT</ta>
            <ta e="T760" id="Seg_9175" s="T759">большой.котел-DAT/LOC</ta>
            <ta e="T761" id="Seg_9176" s="T760">нырять-PRS.[3SG]</ta>
            <ta e="T762" id="Seg_9177" s="T761">мать-3SG-GEN</ta>
            <ta e="T763" id="Seg_9178" s="T762">живот-3SG-DAT/LOC</ta>
            <ta e="T764" id="Seg_9179" s="T763">ONOM</ta>
            <ta e="T765" id="Seg_9180" s="T764">делать-PRS.[3SG]</ta>
            <ta e="T766" id="Seg_9181" s="T765">рот-3SG-ACC</ta>
            <ta e="T767" id="Seg_9182" s="T766">через</ta>
            <ta e="T768" id="Seg_9183" s="T767">выйти-PRS.[3SG]</ta>
            <ta e="T769" id="Seg_9184" s="T768">отец-3SG-GEN</ta>
            <ta e="T770" id="Seg_9185" s="T769">живот-3SG-DAT/LOC</ta>
            <ta e="T771" id="Seg_9186" s="T770">ONOM</ta>
            <ta e="T772" id="Seg_9187" s="T771">делать-PRS.[3SG]</ta>
            <ta e="T773" id="Seg_9188" s="T772">отец-3SG-GEN</ta>
            <ta e="T774" id="Seg_9189" s="T773">рот-3SG-ACC</ta>
            <ta e="T775" id="Seg_9190" s="T774">через</ta>
            <ta e="T776" id="Seg_9191" s="T775">что-ABL</ta>
            <ta e="T777" id="Seg_9192" s="T776">INDEF</ta>
            <ta e="T778" id="Seg_9193" s="T777">плохой</ta>
            <ta e="T779" id="Seg_9194" s="T778">вдовец-ADJZ</ta>
            <ta e="T780" id="Seg_9195" s="T779">теленок-DIM.[NOM]</ta>
            <ta e="T781" id="Seg_9196" s="T780">становиться-CVB.SEQ</ta>
            <ta e="T782" id="Seg_9197" s="T781">выйти-CVB.SIM</ta>
            <ta e="T783" id="Seg_9198" s="T782">бежать-PST2.[3SG]</ta>
            <ta e="T784" id="Seg_9199" s="T783">задняя.часть-3SG-ABL</ta>
            <ta e="T785" id="Seg_9200" s="T784">лук-PROPR</ta>
            <ta e="T786" id="Seg_9201" s="T785">человек.[NOM]</ta>
            <ta e="T787" id="Seg_9202" s="T786">выйти-PRS.[3SG]</ta>
            <ta e="T788" id="Seg_9203" s="T787">да</ta>
            <ta e="T789" id="Seg_9204" s="T788">гость-PL.[NOM]</ta>
            <ta e="T790" id="Seg_9205" s="T789">передняя.часть-3PL-DAT/LOC</ta>
            <ta e="T791" id="Seg_9206" s="T790">плотно</ta>
            <ta e="T792" id="Seg_9207" s="T791">стрелять-PRS.[3SG]</ta>
            <ta e="T793" id="Seg_9208" s="T792">пища-VBZ-CVB.SEQ</ta>
            <ta e="T794" id="Seg_9209" s="T793">бросать-PRS.[3SG]</ta>
            <ta e="T795" id="Seg_9210" s="T794">гость-PL-DAT/LOC</ta>
            <ta e="T796" id="Seg_9211" s="T795">тянуть-CVB.SEQ</ta>
            <ta e="T797" id="Seg_9212" s="T796">бросать-PRS.[3SG]</ta>
            <ta e="T798" id="Seg_9213" s="T797">тянуть-CVB.SEQ</ta>
            <ta e="T799" id="Seg_9214" s="T798">только</ta>
            <ta e="T800" id="Seg_9215" s="T799">бросать-PST2-3SG</ta>
            <ta e="T801" id="Seg_9216" s="T800">дом-3SG.[NOM]-огонь-3SG.[NOM]</ta>
            <ta e="T802" id="Seg_9217" s="T801">куда</ta>
            <ta e="T803" id="Seg_9218" s="T802">NEG</ta>
            <ta e="T804" id="Seg_9219" s="T803">идти-PTCP.PST-3PL-ACC</ta>
            <ta e="T805" id="Seg_9220" s="T804">замечать-NEG-PST1-3PL</ta>
            <ta e="T806" id="Seg_9221" s="T805">пурга.[NOM]</ta>
            <ta e="T807" id="Seg_9222" s="T806">господин-PL-3SG.[NOM]</ta>
            <ta e="T808" id="Seg_9223" s="T807">обижаться-CVB.SEQ</ta>
            <ta e="T809" id="Seg_9224" s="T808">этот</ta>
            <ta e="T810" id="Seg_9225" s="T809">дом-ACC</ta>
            <ta e="T811" id="Seg_9226" s="T810">Костяной.Пояс-ACC</ta>
            <ta e="T812" id="Seg_9227" s="T811">мать-COM-ACC-отец-COM-ACC</ta>
            <ta e="T813" id="Seg_9228" s="T812">мчаться-CVB.SEQ</ta>
            <ta e="T814" id="Seg_9229" s="T813">вихрь-INSTR</ta>
            <ta e="T815" id="Seg_9230" s="T814">идти-PST2-3PL</ta>
            <ta e="T816" id="Seg_9231" s="T815">только</ta>
            <ta e="T817" id="Seg_9232" s="T816">Железная.Шапка.[NOM]</ta>
            <ta e="T818" id="Seg_9233" s="T817">мать-COM-отец-COM</ta>
            <ta e="T819" id="Seg_9234" s="T818">оставаться-PST2.[3SG]</ta>
            <ta e="T820" id="Seg_9235" s="T819">пурга.[NOM]</ta>
            <ta e="T821" id="Seg_9236" s="T820">дочь-3SG-ACC</ta>
            <ta e="T822" id="Seg_9237" s="T821">жена-VBZ-CVB.SEQ</ta>
            <ta e="T823" id="Seg_9238" s="T822">быть.богатым-CVB.SEQ-быть.богатым-CVB.SEQ</ta>
            <ta e="T824" id="Seg_9239" s="T823">жить-PST2-3SG</ta>
            <ta e="T825" id="Seg_9240" s="T824">говорят</ta>
            <ta e="T826" id="Seg_9241" s="T825">последний-3SG.[NOM]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_9242" s="T0">propr-n:case</ta>
            <ta e="T2" id="Seg_9243" s="T1">dempro-pro:case</ta>
            <ta e="T3" id="Seg_9244" s="T2">propr-n:case</ta>
            <ta e="T4" id="Seg_9245" s="T3">v-v:cvb</ta>
            <ta e="T5" id="Seg_9246" s="T4">cardnum</ta>
            <ta e="T6" id="Seg_9247" s="T5">n-n:case</ta>
            <ta e="T7" id="Seg_9248" s="T6">n-n:(num)-n:case</ta>
            <ta e="T8" id="Seg_9249" s="T7">v-v:tense-v:pred.pn</ta>
            <ta e="T9" id="Seg_9250" s="T8">cardnum-cardnum&gt;collnum-n:case</ta>
            <ta e="T10" id="Seg_9251" s="T9">n-n&gt;adj-n:(pred.pn)</ta>
            <ta e="T11" id="Seg_9252" s="T10">n-n&gt;adj-n:(pred.pn)</ta>
            <ta e="T12" id="Seg_9253" s="T11">ptcl</ta>
            <ta e="T13" id="Seg_9254" s="T12">dempro</ta>
            <ta e="T14" id="Seg_9255" s="T13">v-v:cvb-v:pred.pn</ta>
            <ta e="T15" id="Seg_9256" s="T14">propr-n:(poss)-n:case</ta>
            <ta e="T16" id="Seg_9257" s="T15">v-v:tense-v:pred.pn</ta>
            <ta e="T17" id="Seg_9258" s="T16">propr-n:poss-n:case</ta>
            <ta e="T18" id="Seg_9259" s="T17">n</ta>
            <ta e="T19" id="Seg_9260" s="T18">dempro</ta>
            <ta e="T20" id="Seg_9261" s="T19">pers-pro:case</ta>
            <ta e="T21" id="Seg_9262" s="T20">que-pro:case</ta>
            <ta e="T22" id="Seg_9263" s="T21">n-n&gt;adj-n:case</ta>
            <ta e="T23" id="Seg_9264" s="T22">v-v:tense-v:pred.pn</ta>
            <ta e="T24" id="Seg_9265" s="T23">que</ta>
            <ta e="T25" id="Seg_9266" s="T24">n-n:case</ta>
            <ta e="T26" id="Seg_9267" s="T25">n-n:poss-n:case</ta>
            <ta e="T27" id="Seg_9268" s="T26">v-v&gt;v-v:(ins)-v:(neg)-v:pred.pn</ta>
            <ta e="T28" id="Seg_9269" s="T27">n-n:case</ta>
            <ta e="T29" id="Seg_9270" s="T28">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T30" id="Seg_9271" s="T29">v-v:tense-v:pred.pn</ta>
            <ta e="T31" id="Seg_9272" s="T30">adv</ta>
            <ta e="T32" id="Seg_9273" s="T31">n-n:(poss)-n:case</ta>
            <ta e="T33" id="Seg_9274" s="T32">interj</ta>
            <ta e="T34" id="Seg_9275" s="T33">n-n:case</ta>
            <ta e="T35" id="Seg_9276" s="T34">que-pro:(poss)-pro:case</ta>
            <ta e="T36" id="Seg_9277" s="T35">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T37" id="Seg_9278" s="T36">n-n:case</ta>
            <ta e="T38" id="Seg_9279" s="T37">v-v:(ins)-v&gt;v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T39" id="Seg_9280" s="T38">v-v:cvb</ta>
            <ta e="T40" id="Seg_9281" s="T39">v-v:(ins)-v&gt;v-v:(neg)-v:pred.pn</ta>
            <ta e="T41" id="Seg_9282" s="T40">n-n:(poss)-n:case</ta>
            <ta e="T42" id="Seg_9283" s="T41">v-v&gt;v-v:(neg)-v:pred.pn</ta>
            <ta e="T43" id="Seg_9284" s="T42">que</ta>
            <ta e="T44" id="Seg_9285" s="T43">n-n:case</ta>
            <ta e="T45" id="Seg_9286" s="T44">pers-pro:case</ta>
            <ta e="T46" id="Seg_9287" s="T45">n-n:poss-n:case</ta>
            <ta e="T47" id="Seg_9288" s="T46">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T48" id="Seg_9289" s="T47">v-v:tense-v:pred.pn</ta>
            <ta e="T49" id="Seg_9290" s="T48">n-n:(poss)-n:case</ta>
            <ta e="T50" id="Seg_9291" s="T49">v-v:(ins)-v&gt;v-v:(neg)-v:pred.pn</ta>
            <ta e="T51" id="Seg_9292" s="T50">propr-n:case</ta>
            <ta e="T52" id="Seg_9293" s="T51">pers-pro:case</ta>
            <ta e="T53" id="Seg_9294" s="T52">v-v:tense-v:poss.pn</ta>
            <ta e="T54" id="Seg_9295" s="T53">pers-pro:case</ta>
            <ta e="T55" id="Seg_9296" s="T54">v-v:mood.pn</ta>
            <ta e="T56" id="Seg_9297" s="T55">pers-pro:case</ta>
            <ta e="T57" id="Seg_9298" s="T56">pers-pro:case</ta>
            <ta e="T58" id="Seg_9299" s="T57">que</ta>
            <ta e="T59" id="Seg_9300" s="T58">v-v:tense-v:pred.pn</ta>
            <ta e="T60" id="Seg_9301" s="T59">conj</ta>
            <ta e="T61" id="Seg_9302" s="T60">adv-adv&gt;adv</ta>
            <ta e="T62" id="Seg_9303" s="T61">v-v:cvb</ta>
            <ta e="T63" id="Seg_9304" s="T62">v-v:mood.pn</ta>
            <ta e="T64" id="Seg_9305" s="T63">v-v:tense-v:pred.pn</ta>
            <ta e="T65" id="Seg_9306" s="T64">n-n:(poss)-n:case</ta>
            <ta e="T66" id="Seg_9307" s="T65">v-v:tense-v:pred.pn</ta>
            <ta e="T67" id="Seg_9308" s="T66">ptcl</ta>
            <ta e="T68" id="Seg_9309" s="T67">ptcl</ta>
            <ta e="T69" id="Seg_9310" s="T68">adv</ta>
            <ta e="T70" id="Seg_9311" s="T69">adv-adv&gt;n-n:case</ta>
            <ta e="T71" id="Seg_9312" s="T70">n-n:case</ta>
            <ta e="T72" id="Seg_9313" s="T71">v-v:ptcp</ta>
            <ta e="T73" id="Seg_9314" s="T72">n-n:poss-n:case</ta>
            <ta e="T74" id="Seg_9315" s="T73">v-v:(tense)-v:mood.pn</ta>
            <ta e="T75" id="Seg_9316" s="T74">pers-pro:case</ta>
            <ta e="T76" id="Seg_9317" s="T75">v-v:tense-v:pred.pn</ta>
            <ta e="T77" id="Seg_9318" s="T76">propr-n:case</ta>
            <ta e="T78" id="Seg_9319" s="T77">adv</ta>
            <ta e="T79" id="Seg_9320" s="T78">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T80" id="Seg_9321" s="T79">v-v:tense-v:pred.pn</ta>
            <ta e="T81" id="Seg_9322" s="T80">n-n:case</ta>
            <ta e="T82" id="Seg_9323" s="T81">n-n:poss-n:case</ta>
            <ta e="T83" id="Seg_9324" s="T82">propr-n:case</ta>
            <ta e="T84" id="Seg_9325" s="T83">v-v:cvb</ta>
            <ta e="T85" id="Seg_9326" s="T84">v-v:cvb</ta>
            <ta e="T86" id="Seg_9327" s="T85">v-v:tense-v:pred.pn</ta>
            <ta e="T87" id="Seg_9328" s="T86">v-v:cvb</ta>
            <ta e="T88" id="Seg_9329" s="T87">v-v:tense-v:pred.pn</ta>
            <ta e="T89" id="Seg_9330" s="T88">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T90" id="Seg_9331" s="T89">ptcl-ptcl:(poss.pn)</ta>
            <ta e="T91" id="Seg_9332" s="T90">ptcl</ta>
            <ta e="T92" id="Seg_9333" s="T91">n-n:poss-n:case</ta>
            <ta e="T93" id="Seg_9334" s="T92">v-v:tense-v:poss.pn</ta>
            <ta e="T94" id="Seg_9335" s="T93">propr-n:case</ta>
            <ta e="T95" id="Seg_9336" s="T94">n-n:(poss)-n:case</ta>
            <ta e="T96" id="Seg_9337" s="T95">v-v:cvb</ta>
            <ta e="T97" id="Seg_9338" s="T96">v-v:tense-v:pred.pn</ta>
            <ta e="T98" id="Seg_9339" s="T97">v-v:cvb</ta>
            <ta e="T99" id="Seg_9340" s="T98">v-v:tense-v:pred.pn</ta>
            <ta e="T100" id="Seg_9341" s="T99">propr-n:case</ta>
            <ta e="T101" id="Seg_9342" s="T100">n-n:case</ta>
            <ta e="T102" id="Seg_9343" s="T101">v-v:tense-v:pred.pn</ta>
            <ta e="T103" id="Seg_9344" s="T102">n-n:case</ta>
            <ta e="T104" id="Seg_9345" s="T103">v-v:(neg)-v:pred.pn</ta>
            <ta e="T105" id="Seg_9346" s="T104">adj-n:(poss)-n:case</ta>
            <ta e="T106" id="Seg_9347" s="T105">v-v:tense-v:pred.pn</ta>
            <ta e="T107" id="Seg_9348" s="T106">v-v:cvb</ta>
            <ta e="T108" id="Seg_9349" s="T107">n-n:poss-n:case</ta>
            <ta e="T109" id="Seg_9350" s="T108">adv-adv&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T110" id="Seg_9351" s="T109">propr-n:case</ta>
            <ta e="T111" id="Seg_9352" s="T110">n-n:poss-n:case</ta>
            <ta e="T112" id="Seg_9353" s="T111">n-n:poss-n:case</ta>
            <ta e="T113" id="Seg_9354" s="T112">adj</ta>
            <ta e="T114" id="Seg_9355" s="T113">n-n:poss-n:case</ta>
            <ta e="T115" id="Seg_9356" s="T114">n-n:(poss)-n:case</ta>
            <ta e="T116" id="Seg_9357" s="T115">v-v:cvb</ta>
            <ta e="T117" id="Seg_9358" s="T116">v-v:tense-v:pred.pn</ta>
            <ta e="T118" id="Seg_9359" s="T117">adj</ta>
            <ta e="T119" id="Seg_9360" s="T118">n-n:case</ta>
            <ta e="T120" id="Seg_9361" s="T119">ptcl</ta>
            <ta e="T121" id="Seg_9362" s="T120">propr-n:case</ta>
            <ta e="T122" id="Seg_9363" s="T121">n-n:case</ta>
            <ta e="T123" id="Seg_9364" s="T122">v-v:tense-v:poss.pn</ta>
            <ta e="T124" id="Seg_9365" s="T123">conj</ta>
            <ta e="T125" id="Seg_9366" s="T124">n-n:poss-n:case</ta>
            <ta e="T126" id="Seg_9367" s="T125">v-v:tense-v:poss.pn</ta>
            <ta e="T127" id="Seg_9368" s="T126">adj-adj&gt;adj-n:(poss)-n:case</ta>
            <ta e="T128" id="Seg_9369" s="T127">n-n:case</ta>
            <ta e="T129" id="Seg_9370" s="T128">v-v:cvb</ta>
            <ta e="T130" id="Seg_9371" s="T129">n-n:case</ta>
            <ta e="T131" id="Seg_9372" s="T130">v-v:cvb</ta>
            <ta e="T132" id="Seg_9373" s="T131">v-v:cvb</ta>
            <ta e="T133" id="Seg_9374" s="T132">v-v:tense-v:pred.pn</ta>
            <ta e="T134" id="Seg_9375" s="T133">propr-n:case</ta>
            <ta e="T135" id="Seg_9376" s="T134">n-n:case</ta>
            <ta e="T136" id="Seg_9377" s="T135">v-v:cvb</ta>
            <ta e="T137" id="Seg_9378" s="T136">v-v:cvb</ta>
            <ta e="T138" id="Seg_9379" s="T137">v-v:tense-v:pred.pn</ta>
            <ta e="T139" id="Seg_9380" s="T138">interj</ta>
            <ta e="T140" id="Seg_9381" s="T139">v-v&gt;n-n:case</ta>
            <ta e="T141" id="Seg_9382" s="T140">v-v:cvb</ta>
            <ta e="T142" id="Seg_9383" s="T141">v-v:tense-v:poss.pn</ta>
            <ta e="T143" id="Seg_9384" s="T142">dempro</ta>
            <ta e="T144" id="Seg_9385" s="T143">n-n:case</ta>
            <ta e="T145" id="Seg_9386" s="T144">n-n:(poss)-n:case</ta>
            <ta e="T146" id="Seg_9387" s="T145">v-v:tense-v:pred.pn</ta>
            <ta e="T147" id="Seg_9388" s="T146">conj</ta>
            <ta e="T148" id="Seg_9389" s="T147">adj</ta>
            <ta e="T149" id="Seg_9390" s="T148">n-n:case</ta>
            <ta e="T150" id="Seg_9391" s="T149">n-n:case</ta>
            <ta e="T151" id="Seg_9392" s="T150">n-n:(poss)-n:case</ta>
            <ta e="T152" id="Seg_9393" s="T151">v-v:cvb</ta>
            <ta e="T153" id="Seg_9394" s="T152">v-v:tense-v:poss.pn</ta>
            <ta e="T154" id="Seg_9395" s="T153">n-n:(poss)-n:case</ta>
            <ta e="T155" id="Seg_9396" s="T154">v-v:cvb</ta>
            <ta e="T156" id="Seg_9397" s="T155">v-v:tense-v:poss.pn</ta>
            <ta e="T157" id="Seg_9398" s="T156">propr-n:case</ta>
            <ta e="T158" id="Seg_9399" s="T157">n-n:case</ta>
            <ta e="T159" id="Seg_9400" s="T158">v-v:tense-v:pred.pn</ta>
            <ta e="T160" id="Seg_9401" s="T159">conj</ta>
            <ta e="T161" id="Seg_9402" s="T160">v-v:cvb</ta>
            <ta e="T162" id="Seg_9403" s="T161">v-v:tense-v:poss.pn</ta>
            <ta e="T163" id="Seg_9404" s="T162">cardnum</ta>
            <ta e="T164" id="Seg_9405" s="T163">n-n:case</ta>
            <ta e="T165" id="Seg_9406" s="T164">v-v:tense-v:pred.pn</ta>
            <ta e="T166" id="Seg_9407" s="T165">conj</ta>
            <ta e="T167" id="Seg_9408" s="T166">propr-n:(poss)-n:case</ta>
            <ta e="T168" id="Seg_9409" s="T167">n-n:case</ta>
            <ta e="T169" id="Seg_9410" s="T168">v-v:cvb</ta>
            <ta e="T170" id="Seg_9411" s="T169">v-v:tense-v:pred.pn</ta>
            <ta e="T171" id="Seg_9412" s="T170">n-n&gt;adj-n:(poss)-n:case</ta>
            <ta e="T172" id="Seg_9413" s="T171">n-n:case</ta>
            <ta e="T173" id="Seg_9414" s="T172">v-v:cvb</ta>
            <ta e="T174" id="Seg_9415" s="T173">v-v:tense-v:poss.pn</ta>
            <ta e="T175" id="Seg_9416" s="T174">propr-n:case</ta>
            <ta e="T176" id="Seg_9417" s="T175">n-n:case</ta>
            <ta e="T177" id="Seg_9418" s="T176">v-v:tense-v:poss.pn</ta>
            <ta e="T178" id="Seg_9419" s="T177">adv</ta>
            <ta e="T179" id="Seg_9420" s="T178">n-n&gt;adj-n:(poss)-n:case</ta>
            <ta e="T180" id="Seg_9421" s="T179">ptcl</ta>
            <ta e="T181" id="Seg_9422" s="T180">n-n:case</ta>
            <ta e="T182" id="Seg_9423" s="T181">v-v:cvb</ta>
            <ta e="T183" id="Seg_9424" s="T182">n-n:poss-n:case</ta>
            <ta e="T184" id="Seg_9425" s="T183">ptcl</ta>
            <ta e="T185" id="Seg_9426" s="T184">v-v:cvb</ta>
            <ta e="T186" id="Seg_9427" s="T185">v-v:tense-v:poss.pn</ta>
            <ta e="T187" id="Seg_9428" s="T186">dempro-pro:(poss)-pro:case</ta>
            <ta e="T188" id="Seg_9429" s="T187">n-n:case</ta>
            <ta e="T189" id="Seg_9430" s="T188">v-v:cvb</ta>
            <ta e="T190" id="Seg_9431" s="T189">v-v:cvb</ta>
            <ta e="T191" id="Seg_9432" s="T190">v-v:tense-v:pred.pn</ta>
            <ta e="T192" id="Seg_9433" s="T191">n-n&gt;adj-n:(poss)-n:case</ta>
            <ta e="T193" id="Seg_9434" s="T192">v-v:cvb</ta>
            <ta e="T194" id="Seg_9435" s="T193">v-v:tense-v:poss.pn</ta>
            <ta e="T195" id="Seg_9436" s="T194">dempro-pro:(poss)-pro&gt;adj-n:(poss)-n:case</ta>
            <ta e="T196" id="Seg_9437" s="T195">v-v:(ins)-v:cvb</ta>
            <ta e="T197" id="Seg_9438" s="T196">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T198" id="Seg_9439" s="T197">ptcl</ta>
            <ta e="T199" id="Seg_9440" s="T198">v-v:tense-v:pred.pn</ta>
            <ta e="T200" id="Seg_9441" s="T199">n-n:case</ta>
            <ta e="T201" id="Seg_9442" s="T200">v-v:cvb</ta>
            <ta e="T202" id="Seg_9443" s="T201">v-v:cvb</ta>
            <ta e="T203" id="Seg_9444" s="T202">v-v:tense-v:poss.pn</ta>
            <ta e="T204" id="Seg_9445" s="T203">que-pro:case-que-pro:case</ta>
            <ta e="T205" id="Seg_9446" s="T204">v-v:tense-v:poss.pn</ta>
            <ta e="T206" id="Seg_9447" s="T205">v-v&gt;v-v:(ins)-v:(neg)-v:pred.pn</ta>
            <ta e="T207" id="Seg_9448" s="T206">propr-n:case</ta>
            <ta e="T208" id="Seg_9449" s="T207">n-n:case</ta>
            <ta e="T209" id="Seg_9450" s="T208">v-v:cvb</ta>
            <ta e="T210" id="Seg_9451" s="T209">v-v:cvb</ta>
            <ta e="T211" id="Seg_9452" s="T210">v-v:tense-v:pred.pn</ta>
            <ta e="T212" id="Seg_9453" s="T211">n-n&gt;adj-n:(poss)-n:case</ta>
            <ta e="T213" id="Seg_9454" s="T212">ptcl</ta>
            <ta e="T214" id="Seg_9455" s="T213">adv</ta>
            <ta e="T215" id="Seg_9456" s="T214">v-v:cvb-v-v:cvb</ta>
            <ta e="T216" id="Seg_9457" s="T215">n-n:case</ta>
            <ta e="T217" id="Seg_9458" s="T216">v-v:cvb</ta>
            <ta e="T218" id="Seg_9459" s="T217">v-v:cvb</ta>
            <ta e="T219" id="Seg_9460" s="T218">v-v:tense-v:poss.pn</ta>
            <ta e="T220" id="Seg_9461" s="T219">n-n:poss-n:case</ta>
            <ta e="T221" id="Seg_9462" s="T220">n-n:(poss)-n:case</ta>
            <ta e="T222" id="Seg_9463" s="T221">v-v:cvb-v:pred.pn</ta>
            <ta e="T223" id="Seg_9464" s="T222">n-n:case</ta>
            <ta e="T224" id="Seg_9465" s="T223">v-v:cvb</ta>
            <ta e="T225" id="Seg_9466" s="T224">v-v:tense-v:pred.pn</ta>
            <ta e="T226" id="Seg_9467" s="T225">n-n:case</ta>
            <ta e="T227" id="Seg_9468" s="T226">v-v:cvb</ta>
            <ta e="T228" id="Seg_9469" s="T227">v-v:cvb</ta>
            <ta e="T229" id="Seg_9470" s="T228">v-v:tense-v:poss.pn</ta>
            <ta e="T230" id="Seg_9471" s="T229">propr-n:case</ta>
            <ta e="T231" id="Seg_9472" s="T230">dempro-pro:case</ta>
            <ta e="T232" id="Seg_9473" s="T231">post</ta>
            <ta e="T233" id="Seg_9474" s="T232">interj</ta>
            <ta e="T234" id="Seg_9475" s="T233">v-v&gt;n-n:case</ta>
            <ta e="T235" id="Seg_9476" s="T234">n-n:case</ta>
            <ta e="T236" id="Seg_9477" s="T235">n-n:case</ta>
            <ta e="T237" id="Seg_9478" s="T236">n-n:(poss)-n:case</ta>
            <ta e="T238" id="Seg_9479" s="T237">v-v:tense-v:pred.pn</ta>
            <ta e="T239" id="Seg_9480" s="T238">conj</ta>
            <ta e="T240" id="Seg_9481" s="T239">n</ta>
            <ta e="T241" id="Seg_9482" s="T240">n-n:(poss)-n:case</ta>
            <ta e="T242" id="Seg_9483" s="T241">v-v:cvb</ta>
            <ta e="T243" id="Seg_9484" s="T242">v-v:cvb</ta>
            <ta e="T244" id="Seg_9485" s="T243">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T245" id="Seg_9486" s="T244">v-v:cvb</ta>
            <ta e="T246" id="Seg_9487" s="T245">ptcl</ta>
            <ta e="T247" id="Seg_9488" s="T246">v-v:tense-v:pred.pn</ta>
            <ta e="T248" id="Seg_9489" s="T247">adv</ta>
            <ta e="T249" id="Seg_9490" s="T248">v-v:tense-v:pred.pn</ta>
            <ta e="T250" id="Seg_9491" s="T249">n-n:(poss)-n:case</ta>
            <ta e="T251" id="Seg_9492" s="T250">propr-n:case</ta>
            <ta e="T252" id="Seg_9493" s="T251">n-n:case</ta>
            <ta e="T253" id="Seg_9494" s="T252">n-n:(poss)-n:case</ta>
            <ta e="T254" id="Seg_9495" s="T253">v-v:tense-v:poss.pn</ta>
            <ta e="T255" id="Seg_9496" s="T254">n-n:(poss)-n:case</ta>
            <ta e="T256" id="Seg_9497" s="T255">conj</ta>
            <ta e="T257" id="Seg_9498" s="T256">adj-n:(poss)-n:case</ta>
            <ta e="T258" id="Seg_9499" s="T257">ptcl</ta>
            <ta e="T259" id="Seg_9500" s="T258">v-v:cvb</ta>
            <ta e="T260" id="Seg_9501" s="T259">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T261" id="Seg_9502" s="T260">dempro-pro&gt;pro</ta>
            <ta e="T262" id="Seg_9503" s="T261">v-v:cvb</ta>
            <ta e="T263" id="Seg_9504" s="T262">v-v:tense-v:pred.pn</ta>
            <ta e="T264" id="Seg_9505" s="T263">v-v:tense-v:poss.pn</ta>
            <ta e="T265" id="Seg_9506" s="T264">cardnum</ta>
            <ta e="T266" id="Seg_9507" s="T265">n-n:case</ta>
            <ta e="T267" id="Seg_9508" s="T266">n-n:(poss)-n:case</ta>
            <ta e="T268" id="Seg_9509" s="T267">v-v:cvb</ta>
            <ta e="T269" id="Seg_9510" s="T268">v-v:tense-v:pred.pn</ta>
            <ta e="T270" id="Seg_9511" s="T269">v-v:cvb</ta>
            <ta e="T271" id="Seg_9512" s="T270">v-v:tense-v:poss.pn</ta>
            <ta e="T272" id="Seg_9513" s="T271">cardnum</ta>
            <ta e="T273" id="Seg_9514" s="T272">adv</ta>
            <ta e="T274" id="Seg_9515" s="T273">n-n:(poss)-n:case</ta>
            <ta e="T275" id="Seg_9516" s="T274">v-v:cvb</ta>
            <ta e="T276" id="Seg_9517" s="T275">v-v:tense-v:pred.pn</ta>
            <ta e="T277" id="Seg_9518" s="T276">n-n:(num)-n:poss-n:case</ta>
            <ta e="T278" id="Seg_9519" s="T277">v-v:cvb</ta>
            <ta e="T279" id="Seg_9520" s="T278">n-n:case</ta>
            <ta e="T280" id="Seg_9521" s="T279">n-n:(num)-n:poss-n:case</ta>
            <ta e="T281" id="Seg_9522" s="T280">v-v&gt;v-v:cvb</ta>
            <ta e="T282" id="Seg_9523" s="T281">v-v:tense-v:pred.pn</ta>
            <ta e="T283" id="Seg_9524" s="T282">dempro-pro:case</ta>
            <ta e="T284" id="Seg_9525" s="T283">post-post&gt;post</ta>
            <ta e="T285" id="Seg_9526" s="T284">propr-n:case</ta>
            <ta e="T286" id="Seg_9527" s="T285">n-n:poss-n:case</ta>
            <ta e="T287" id="Seg_9528" s="T286">v-v:cvb</ta>
            <ta e="T288" id="Seg_9529" s="T287">n-n:case</ta>
            <ta e="T289" id="Seg_9530" s="T288">v-v:cvb</ta>
            <ta e="T290" id="Seg_9531" s="T289">v-v:cvb</ta>
            <ta e="T291" id="Seg_9532" s="T290">v-v:tense-v:poss.pn</ta>
            <ta e="T292" id="Seg_9533" s="T291">que</ta>
            <ta e="T293" id="Seg_9534" s="T292">ptcl</ta>
            <ta e="T294" id="Seg_9535" s="T293">v-v:(ins)-v:cvb</ta>
            <ta e="T295" id="Seg_9536" s="T294">n-n:case</ta>
            <ta e="T296" id="Seg_9537" s="T295">v-v:cvb</ta>
            <ta e="T297" id="Seg_9538" s="T296">v-v:tense-v:pred.pn</ta>
            <ta e="T298" id="Seg_9539" s="T297">v-v:tense-v:pred.pn</ta>
            <ta e="T299" id="Seg_9540" s="T298">v-v:tense-v:poss.pn</ta>
            <ta e="T300" id="Seg_9541" s="T299">ptcl</ta>
            <ta e="T301" id="Seg_9542" s="T300">v-v:tense-v:pred.pn</ta>
            <ta e="T302" id="Seg_9543" s="T301">dempro</ta>
            <ta e="T303" id="Seg_9544" s="T302">n-n:case</ta>
            <ta e="T304" id="Seg_9545" s="T303">v-v:tense-v:poss.pn</ta>
            <ta e="T305" id="Seg_9546" s="T304">n-n:case</ta>
            <ta e="T306" id="Seg_9547" s="T305">adj-n:case</ta>
            <ta e="T307" id="Seg_9548" s="T306">ptcl</ta>
            <ta e="T308" id="Seg_9549" s="T307">v-v:tense-v:pred.pn</ta>
            <ta e="T309" id="Seg_9550" s="T308">n-n:(poss)-n:case</ta>
            <ta e="T310" id="Seg_9551" s="T309">ptcl</ta>
            <ta e="T311" id="Seg_9552" s="T310">v-v:cvb</ta>
            <ta e="T312" id="Seg_9553" s="T311">v-v:tense-v:pred.pn</ta>
            <ta e="T313" id="Seg_9554" s="T312">interj</ta>
            <ta e="T314" id="Seg_9555" s="T313">ptcl</ta>
            <ta e="T315" id="Seg_9556" s="T314">dempro</ta>
            <ta e="T316" id="Seg_9557" s="T315">que</ta>
            <ta e="T317" id="Seg_9558" s="T316">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T318" id="Seg_9559" s="T317">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T319" id="Seg_9560" s="T318">v-v:cvb</ta>
            <ta e="T320" id="Seg_9561" s="T319">v-v:tense-v:pred.pn</ta>
            <ta e="T321" id="Seg_9562" s="T320">adv-adv&gt;adv</ta>
            <ta e="T322" id="Seg_9563" s="T321">que-pro:case</ta>
            <ta e="T323" id="Seg_9564" s="T322">n-n:(poss)-n:case-ptcl</ta>
            <ta e="T324" id="Seg_9565" s="T323">v-v:cvb</ta>
            <ta e="T325" id="Seg_9566" s="T324">v-v:cvb</ta>
            <ta e="T326" id="Seg_9567" s="T325">v-v:tense-v:poss.pn</ta>
            <ta e="T327" id="Seg_9568" s="T326">n-n:case</ta>
            <ta e="T328" id="Seg_9569" s="T327">v-v:cvb</ta>
            <ta e="T329" id="Seg_9570" s="T328">v-v:cvb</ta>
            <ta e="T330" id="Seg_9571" s="T329">v-v:tense-v:poss.pn</ta>
            <ta e="T331" id="Seg_9572" s="T330">ptcl</ta>
            <ta e="T332" id="Seg_9573" s="T331">n-n:case</ta>
            <ta e="T333" id="Seg_9574" s="T332">v-v:cvb</ta>
            <ta e="T334" id="Seg_9575" s="T333">v-v:tense-v:pred.pn</ta>
            <ta e="T335" id="Seg_9576" s="T334">ptcl</ta>
            <ta e="T336" id="Seg_9577" s="T335">v-v:tense-v:poss.pn</ta>
            <ta e="T337" id="Seg_9578" s="T336">ptcl</ta>
            <ta e="T338" id="Seg_9579" s="T337">adv</ta>
            <ta e="T339" id="Seg_9580" s="T338">cardnum</ta>
            <ta e="T340" id="Seg_9581" s="T339">n-n:(poss)-n:case</ta>
            <ta e="T341" id="Seg_9582" s="T340">v-v&gt;v-v:cvb</ta>
            <ta e="T342" id="Seg_9583" s="T341">v-v:tense-v:pred.pn</ta>
            <ta e="T343" id="Seg_9584" s="T342">interj</ta>
            <ta e="T344" id="Seg_9585" s="T343">dempro</ta>
            <ta e="T345" id="Seg_9586" s="T344">que-pro:case</ta>
            <ta e="T346" id="Seg_9587" s="T345">n-n:(poss)-n:case-ptcl</ta>
            <ta e="T347" id="Seg_9588" s="T346">n-n:case</ta>
            <ta e="T348" id="Seg_9589" s="T347">v-v:tense-v:pred.pn</ta>
            <ta e="T349" id="Seg_9590" s="T348">ptcl</ta>
            <ta e="T350" id="Seg_9591" s="T349">n-n:case</ta>
            <ta e="T351" id="Seg_9592" s="T350">ptcl</ta>
            <ta e="T352" id="Seg_9593" s="T351">v-v:tense-v:pred.pn</ta>
            <ta e="T353" id="Seg_9594" s="T352">cardnum</ta>
            <ta e="T354" id="Seg_9595" s="T353">n-n:(poss)-n:case</ta>
            <ta e="T355" id="Seg_9596" s="T354">v-v:cvb</ta>
            <ta e="T356" id="Seg_9597" s="T355">v-v:tense-v:poss.pn</ta>
            <ta e="T357" id="Seg_9598" s="T356">n-n:(num)-n:case</ta>
            <ta e="T358" id="Seg_9599" s="T357">n-n:poss-n:case</ta>
            <ta e="T359" id="Seg_9600" s="T358">que</ta>
            <ta e="T360" id="Seg_9601" s="T359">ptcl</ta>
            <ta e="T361" id="Seg_9602" s="T360">dempro</ta>
            <ta e="T362" id="Seg_9603" s="T361">cardnum</ta>
            <ta e="T363" id="Seg_9604" s="T362">n-n:case</ta>
            <ta e="T364" id="Seg_9605" s="T363">n-n:(poss)-n:case</ta>
            <ta e="T365" id="Seg_9606" s="T364">cardnum</ta>
            <ta e="T366" id="Seg_9607" s="T365">n-n&gt;n-n:case</ta>
            <ta e="T367" id="Seg_9608" s="T366">v-v:tense-v:pred.pn</ta>
            <ta e="T368" id="Seg_9609" s="T367">dempro</ta>
            <ta e="T369" id="Seg_9610" s="T368">v-v:cvb</ta>
            <ta e="T370" id="Seg_9611" s="T369">cardnum</ta>
            <ta e="T371" id="Seg_9612" s="T370">n-n:case</ta>
            <ta e="T372" id="Seg_9613" s="T371">n-n:(poss)-n:case</ta>
            <ta e="T373" id="Seg_9614" s="T372">n-n:case</ta>
            <ta e="T374" id="Seg_9615" s="T373">v-v:tense-v:pred.pn</ta>
            <ta e="T375" id="Seg_9616" s="T374">ptcl</ta>
            <ta e="T376" id="Seg_9617" s="T375">adj-n:(poss)-n:case</ta>
            <ta e="T377" id="Seg_9618" s="T376">ptcl</ta>
            <ta e="T378" id="Seg_9619" s="T377">cardnum</ta>
            <ta e="T379" id="Seg_9620" s="T378">adj</ta>
            <ta e="T380" id="Seg_9621" s="T379">n-n:case</ta>
            <ta e="T381" id="Seg_9622" s="T380">n-n:case</ta>
            <ta e="T382" id="Seg_9623" s="T381">v-v:tense-v:poss.pn</ta>
            <ta e="T383" id="Seg_9624" s="T382">v-v:tense-v:poss.pn</ta>
            <ta e="T384" id="Seg_9625" s="T383">adj</ta>
            <ta e="T385" id="Seg_9626" s="T384">post</ta>
            <ta e="T386" id="Seg_9627" s="T385">n-n:case</ta>
            <ta e="T387" id="Seg_9628" s="T386">n-n:case</ta>
            <ta e="T388" id="Seg_9629" s="T387">v-v:tense-v:pred.pn</ta>
            <ta e="T389" id="Seg_9630" s="T388">n-n:(poss)-n:case</ta>
            <ta e="T390" id="Seg_9631" s="T389">n-n:case</ta>
            <ta e="T391" id="Seg_9632" s="T390">v-v:cvb</ta>
            <ta e="T392" id="Seg_9633" s="T391">v-v:tense-v:pred.pn</ta>
            <ta e="T393" id="Seg_9634" s="T392">dempro</ta>
            <ta e="T394" id="Seg_9635" s="T393">cardnum</ta>
            <ta e="T395" id="Seg_9636" s="T394">n-n:case</ta>
            <ta e="T396" id="Seg_9637" s="T395">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T397" id="Seg_9638" s="T396">adj</ta>
            <ta e="T398" id="Seg_9639" s="T397">cardnum</ta>
            <ta e="T399" id="Seg_9640" s="T398">n-n:case</ta>
            <ta e="T400" id="Seg_9641" s="T399">n-n&gt;adj-n:case</ta>
            <ta e="T401" id="Seg_9642" s="T400">interj</ta>
            <ta e="T402" id="Seg_9643" s="T401">conj</ta>
            <ta e="T403" id="Seg_9644" s="T402">n-n:case</ta>
            <ta e="T404" id="Seg_9645" s="T403">v-v:ptcp</ta>
            <ta e="T405" id="Seg_9646" s="T404">n-n:case</ta>
            <ta e="T406" id="Seg_9647" s="T405">v-v:mood-v:pred.pn</ta>
            <ta e="T407" id="Seg_9648" s="T406">pers-pro:case</ta>
            <ta e="T408" id="Seg_9649" s="T407">v-v:mood.pn</ta>
            <ta e="T409" id="Seg_9650" s="T408">pers-pro:case</ta>
            <ta e="T410" id="Seg_9651" s="T409">n-n:(num)-n:poss-n:case</ta>
            <ta e="T411" id="Seg_9652" s="T410">n-n:(num)-n:poss-n:case</ta>
            <ta e="T412" id="Seg_9653" s="T411">v-v&gt;v-v:cvb</ta>
            <ta e="T413" id="Seg_9654" s="T412">v-v:tense-v:poss.pn</ta>
            <ta e="T414" id="Seg_9655" s="T413">v-v:tense-v:pred.pn</ta>
            <ta e="T415" id="Seg_9656" s="T414">n-n:(poss)-n:case</ta>
            <ta e="T416" id="Seg_9657" s="T415">propr-n:case</ta>
            <ta e="T417" id="Seg_9658" s="T416">dempro-pro:case</ta>
            <ta e="T418" id="Seg_9659" s="T417">dempro</ta>
            <ta e="T419" id="Seg_9660" s="T418">adj</ta>
            <ta e="T420" id="Seg_9661" s="T419">n-n:(ins)-n:case</ta>
            <ta e="T421" id="Seg_9662" s="T420">v-v:ptcp</ta>
            <ta e="T422" id="Seg_9663" s="T421">n-n:(pred.pn)</ta>
            <ta e="T423" id="Seg_9664" s="T422">ptcl</ta>
            <ta e="T424" id="Seg_9665" s="T423">ptcl</ta>
            <ta e="T425" id="Seg_9666" s="T424">ptcl</ta>
            <ta e="T426" id="Seg_9667" s="T425">dempro</ta>
            <ta e="T427" id="Seg_9668" s="T426">pers-pro:case</ta>
            <ta e="T428" id="Seg_9669" s="T427">n-n:case</ta>
            <ta e="T429" id="Seg_9670" s="T428">v-v:cvb</ta>
            <ta e="T430" id="Seg_9671" s="T429">v-v:(ins)-v:ptcp</ta>
            <ta e="T431" id="Seg_9672" s="T430">n-n:poss-n:case</ta>
            <ta e="T432" id="Seg_9673" s="T431">v-v:tense-v:poss.pn</ta>
            <ta e="T433" id="Seg_9674" s="T432">ptcl</ta>
            <ta e="T434" id="Seg_9675" s="T433">adv</ta>
            <ta e="T435" id="Seg_9676" s="T434">pers-pro:case</ta>
            <ta e="T436" id="Seg_9677" s="T435">v-v:(tense)-v:mood.pn</ta>
            <ta e="T437" id="Seg_9678" s="T436">pers-pro:case</ta>
            <ta e="T438" id="Seg_9679" s="T437">v-v:(tense)-v:mood.pn</ta>
            <ta e="T439" id="Seg_9680" s="T438">pers-pro:case</ta>
            <ta e="T440" id="Seg_9681" s="T439">n-n:poss-n:case</ta>
            <ta e="T441" id="Seg_9682" s="T440">v-v:tense-v:pred.pn</ta>
            <ta e="T442" id="Seg_9683" s="T441">dempro</ta>
            <ta e="T443" id="Seg_9684" s="T442">n</ta>
            <ta e="T444" id="Seg_9685" s="T443">n-n:poss-n:case</ta>
            <ta e="T445" id="Seg_9686" s="T444">v-v:tense-v:poss.pn</ta>
            <ta e="T446" id="Seg_9687" s="T445">adv</ta>
            <ta e="T447" id="Seg_9688" s="T446">adv-adv&gt;n-n:case</ta>
            <ta e="T448" id="Seg_9689" s="T447">n-n:case</ta>
            <ta e="T449" id="Seg_9690" s="T448">v-v:ptcp</ta>
            <ta e="T450" id="Seg_9691" s="T449">n-n:poss-n:case</ta>
            <ta e="T451" id="Seg_9692" s="T450">v-v:cvb</ta>
            <ta e="T452" id="Seg_9693" s="T451">v-v:tense-v:poss.pn</ta>
            <ta e="T453" id="Seg_9694" s="T452">dempro-pro:case</ta>
            <ta e="T454" id="Seg_9695" s="T453">cardnum-cardnum&gt;distrnum</ta>
            <ta e="T455" id="Seg_9696" s="T454">n-n&gt;adj</ta>
            <ta e="T456" id="Seg_9697" s="T455">n-n&gt;adj</ta>
            <ta e="T457" id="Seg_9698" s="T456">cardnum</ta>
            <ta e="T458" id="Seg_9699" s="T457">n-n:(num)-n:case</ta>
            <ta e="T459" id="Seg_9700" s="T458">v-v:tense-v:poss.pn</ta>
            <ta e="T460" id="Seg_9701" s="T459">dempro-pro:case</ta>
            <ta e="T461" id="Seg_9702" s="T460">pers-pro:case</ta>
            <ta e="T462" id="Seg_9703" s="T461">v-v:cvb</ta>
            <ta e="T463" id="Seg_9704" s="T462">v-v:cvb-v:pred.pn</ta>
            <ta e="T464" id="Seg_9705" s="T463">adj</ta>
            <ta e="T465" id="Seg_9706" s="T464">n-n:case</ta>
            <ta e="T466" id="Seg_9707" s="T465">n-n:poss-n:case</ta>
            <ta e="T467" id="Seg_9708" s="T466">n-n:poss-n:case</ta>
            <ta e="T468" id="Seg_9709" s="T467">v-v:tense-v:poss.pn</ta>
            <ta e="T469" id="Seg_9710" s="T468">conj</ta>
            <ta e="T470" id="Seg_9711" s="T469">v-v:tense-v:poss.pn</ta>
            <ta e="T471" id="Seg_9712" s="T470">dempro</ta>
            <ta e="T472" id="Seg_9713" s="T471">cardnum</ta>
            <ta e="T473" id="Seg_9714" s="T472">n-n:case</ta>
            <ta e="T474" id="Seg_9715" s="T473">cardnum-n:poss-n:case</ta>
            <ta e="T475" id="Seg_9716" s="T474">ptcl</ta>
            <ta e="T476" id="Seg_9717" s="T475">v-v:(ins)-v&gt;v-v:mood-v:temp.pn</ta>
            <ta e="T477" id="Seg_9718" s="T476">v-v&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T478" id="Seg_9719" s="T477">n-n:poss-n:case</ta>
            <ta e="T479" id="Seg_9720" s="T478">adv</ta>
            <ta e="T480" id="Seg_9721" s="T479">v-v:(ins)-v&gt;v-v:(neg)-v:mood-v:temp.pn</ta>
            <ta e="T481" id="Seg_9722" s="T480">dempro</ta>
            <ta e="T482" id="Seg_9723" s="T481">n-n:case</ta>
            <ta e="T483" id="Seg_9724" s="T482">v-v:tense-v:poss.pn</ta>
            <ta e="T484" id="Seg_9725" s="T483">v-v:tense-v:pred.pn</ta>
            <ta e="T485" id="Seg_9726" s="T484">ptcl</ta>
            <ta e="T486" id="Seg_9727" s="T485">n-n:(poss)-n:case</ta>
            <ta e="T487" id="Seg_9728" s="T486">propr-n:case</ta>
            <ta e="T488" id="Seg_9729" s="T487">n-n:case</ta>
            <ta e="T489" id="Seg_9730" s="T488">n-n:(num)-n:case</ta>
            <ta e="T490" id="Seg_9731" s="T489">n-n:(num)-n:case</ta>
            <ta e="T491" id="Seg_9732" s="T490">v-v:tense-v:poss.pn</ta>
            <ta e="T492" id="Seg_9733" s="T491">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T493" id="Seg_9734" s="T492">emphpro-n:poss-n:case</ta>
            <ta e="T494" id="Seg_9735" s="T493">n-n:case</ta>
            <ta e="T495" id="Seg_9736" s="T494">v-v&gt;v-v:cvb</ta>
            <ta e="T496" id="Seg_9737" s="T495">v-v:tense-v:pred.pn</ta>
            <ta e="T497" id="Seg_9738" s="T496">v-v&gt;v-v:cvb</ta>
            <ta e="T498" id="Seg_9739" s="T497">post</ta>
            <ta e="T499" id="Seg_9740" s="T498">propr-n:case</ta>
            <ta e="T500" id="Seg_9741" s="T499">v-v:tense-v:pred.pn</ta>
            <ta e="T501" id="Seg_9742" s="T500">n-n:(poss)-n:case</ta>
            <ta e="T502" id="Seg_9743" s="T501">v-v:(ins)-v:ptcp</ta>
            <ta e="T503" id="Seg_9744" s="T502">n-n:poss-n:case</ta>
            <ta e="T504" id="Seg_9745" s="T503">propr-n:case</ta>
            <ta e="T505" id="Seg_9746" s="T504">v-v:cvb</ta>
            <ta e="T506" id="Seg_9747" s="T505">v-v:tense-v:poss.pn</ta>
            <ta e="T507" id="Seg_9748" s="T506">cardnum</ta>
            <ta e="T508" id="Seg_9749" s="T507">n-n:case</ta>
            <ta e="T509" id="Seg_9750" s="T508">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T510" id="Seg_9751" s="T509">v-v:mood-v:temp.pn</ta>
            <ta e="T511" id="Seg_9752" s="T510">adj</ta>
            <ta e="T512" id="Seg_9753" s="T511">n-n:case</ta>
            <ta e="T513" id="Seg_9754" s="T512">v-v:tense-v:poss.pn</ta>
            <ta e="T514" id="Seg_9755" s="T513">n-n:(ins)-n:case</ta>
            <ta e="T515" id="Seg_9756" s="T514">conj</ta>
            <ta e="T516" id="Seg_9757" s="T515">n-n:(ins)-n:case</ta>
            <ta e="T517" id="Seg_9758" s="T516">conj</ta>
            <ta e="T518" id="Seg_9759" s="T517">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T519" id="Seg_9760" s="T518">v-v:neg-v:poss.pn</ta>
            <ta e="T520" id="Seg_9761" s="T519">ptcl</ta>
            <ta e="T521" id="Seg_9762" s="T520">adv</ta>
            <ta e="T522" id="Seg_9763" s="T521">ptcl</ta>
            <ta e="T523" id="Seg_9764" s="T522">adv</ta>
            <ta e="T524" id="Seg_9765" s="T523">cardnum</ta>
            <ta e="T525" id="Seg_9766" s="T524">n-n:case</ta>
            <ta e="T526" id="Seg_9767" s="T525">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T527" id="Seg_9768" s="T526">v-v:tense-v:poss.pn</ta>
            <ta e="T528" id="Seg_9769" s="T527">n-n:(poss)-n:case</ta>
            <ta e="T529" id="Seg_9770" s="T528">n-n:poss-n:case</ta>
            <ta e="T530" id="Seg_9771" s="T529">v-v:tense-v:pred.pn</ta>
            <ta e="T531" id="Seg_9772" s="T530">ptcl</ta>
            <ta e="T532" id="Seg_9773" s="T531">propr-n:case</ta>
            <ta e="T533" id="Seg_9774" s="T532">v-v:tense-v:pred.pn</ta>
            <ta e="T534" id="Seg_9775" s="T533">n-n:poss-n:case</ta>
            <ta e="T535" id="Seg_9776" s="T534">ptcl</ta>
            <ta e="T536" id="Seg_9777" s="T535">dempro-pro:case</ta>
            <ta e="T537" id="Seg_9778" s="T536">cardnum</ta>
            <ta e="T538" id="Seg_9779" s="T537">n-n:case</ta>
            <ta e="T539" id="Seg_9780" s="T538">adj</ta>
            <ta e="T540" id="Seg_9781" s="T539">n-n:poss-n:case</ta>
            <ta e="T541" id="Seg_9782" s="T540">cardnum</ta>
            <ta e="T542" id="Seg_9783" s="T541">adj</ta>
            <ta e="T543" id="Seg_9784" s="T542">n-n:case</ta>
            <ta e="T544" id="Seg_9785" s="T543">v-v&gt;v-v:cvb</ta>
            <ta e="T545" id="Seg_9786" s="T544">v-v:tense-v:pred.pn</ta>
            <ta e="T546" id="Seg_9787" s="T545">adj</ta>
            <ta e="T547" id="Seg_9788" s="T546">n-n:poss-n:case</ta>
            <ta e="T548" id="Seg_9789" s="T547">cardnum</ta>
            <ta e="T549" id="Seg_9790" s="T548">adj</ta>
            <ta e="T550" id="Seg_9791" s="T549">n-n:case</ta>
            <ta e="T551" id="Seg_9792" s="T550">v-v:tense-v:pred.pn</ta>
            <ta e="T552" id="Seg_9793" s="T551">ptcl</ta>
            <ta e="T553" id="Seg_9794" s="T552">pers-pro:case</ta>
            <ta e="T554" id="Seg_9795" s="T553">dempro</ta>
            <ta e="T555" id="Seg_9796" s="T554">cardnum</ta>
            <ta e="T556" id="Seg_9797" s="T555">adj</ta>
            <ta e="T557" id="Seg_9798" s="T556">n-n:case</ta>
            <ta e="T558" id="Seg_9799" s="T557">v-v:tense-v:poss.pn</ta>
            <ta e="T559" id="Seg_9800" s="T558">n-n:poss-n:case</ta>
            <ta e="T560" id="Seg_9801" s="T559">v-v:tense-v:poss.pn</ta>
            <ta e="T561" id="Seg_9802" s="T560">pers-pro:case</ta>
            <ta e="T562" id="Seg_9803" s="T561">cardnum</ta>
            <ta e="T563" id="Seg_9804" s="T562">adj-n:case</ta>
            <ta e="T564" id="Seg_9805" s="T563">v-v:tense-v:poss.pn</ta>
            <ta e="T565" id="Seg_9806" s="T564">v-v:tense-v:poss.pn</ta>
            <ta e="T566" id="Seg_9807" s="T565">n-n:poss-n:case</ta>
            <ta e="T567" id="Seg_9808" s="T566">v-v:tense-v:pred.pn</ta>
            <ta e="T568" id="Seg_9809" s="T567">dempro</ta>
            <ta e="T569" id="Seg_9810" s="T568">n-n:case</ta>
            <ta e="T570" id="Seg_9811" s="T569">n-n:poss-n:case</ta>
            <ta e="T571" id="Seg_9812" s="T570">adj</ta>
            <ta e="T572" id="Seg_9813" s="T571">n-n:(num)-n:poss-n:case</ta>
            <ta e="T573" id="Seg_9814" s="T572">v-v:tense-v:poss.pn</ta>
            <ta e="T574" id="Seg_9815" s="T573">n-n:poss-n:case</ta>
            <ta e="T575" id="Seg_9816" s="T574">adv</ta>
            <ta e="T576" id="Seg_9817" s="T575">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T577" id="Seg_9818" s="T576">v-v:cvb</ta>
            <ta e="T578" id="Seg_9819" s="T577">cardnum</ta>
            <ta e="T579" id="Seg_9820" s="T578">n-n:(poss)-n:case</ta>
            <ta e="T580" id="Seg_9821" s="T579">n</ta>
            <ta e="T581" id="Seg_9822" s="T580">n-n:(num)-n:(poss)-n:case</ta>
            <ta e="T582" id="Seg_9823" s="T581">v-v:tense-v:poss.pn</ta>
            <ta e="T583" id="Seg_9824" s="T582">dempro</ta>
            <ta e="T584" id="Seg_9825" s="T583">n-n:(num)-n:case</ta>
            <ta e="T585" id="Seg_9826" s="T584">v-v:mood-v:temp.pn</ta>
            <ta e="T586" id="Seg_9827" s="T585">n-n:case</ta>
            <ta e="T587" id="Seg_9828" s="T586">cardnum</ta>
            <ta e="T588" id="Seg_9829" s="T587">adj</ta>
            <ta e="T589" id="Seg_9830" s="T588">n-n:case</ta>
            <ta e="T590" id="Seg_9831" s="T589">v-v:tense-v:poss.pn</ta>
            <ta e="T591" id="Seg_9832" s="T590">pers-pro:case</ta>
            <ta e="T592" id="Seg_9833" s="T591">dempro-pro:case</ta>
            <ta e="T593" id="Seg_9834" s="T592">cardnum</ta>
            <ta e="T594" id="Seg_9835" s="T593">adj</ta>
            <ta e="T595" id="Seg_9836" s="T594">n-n:poss-n:case</ta>
            <ta e="T596" id="Seg_9837" s="T595">v-v:(tense)-v:mood.pn</ta>
            <ta e="T597" id="Seg_9838" s="T596">ptcl</ta>
            <ta e="T598" id="Seg_9839" s="T597">adj</ta>
            <ta e="T599" id="Seg_9840" s="T598">n-n:poss-n:case</ta>
            <ta e="T600" id="Seg_9841" s="T599">adj</ta>
            <ta e="T601" id="Seg_9842" s="T600">n-n:(poss)-n:case</ta>
            <ta e="T602" id="Seg_9843" s="T601">v-v&gt;v-v:mood.pn</ta>
            <ta e="T603" id="Seg_9844" s="T602">pers-pro:case</ta>
            <ta e="T604" id="Seg_9845" s="T603">ptcl</ta>
            <ta e="T605" id="Seg_9846" s="T604">v-v&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T606" id="Seg_9847" s="T605">adv</ta>
            <ta e="T607" id="Seg_9848" s="T606">adv</ta>
            <ta e="T608" id="Seg_9849" s="T607">pers-pro:case</ta>
            <ta e="T609" id="Seg_9850" s="T608">n-n&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T610" id="Seg_9851" s="T609">pers-pro:case</ta>
            <ta e="T611" id="Seg_9852" s="T610">v-v:cvb</ta>
            <ta e="T612" id="Seg_9853" s="T611">v-v:cvb</ta>
            <ta e="T613" id="Seg_9854" s="T612">v-v:mood-v:temp.pn</ta>
            <ta e="T614" id="Seg_9855" s="T613">pers-pro:case</ta>
            <ta e="T615" id="Seg_9856" s="T614">n-n&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T616" id="Seg_9857" s="T615">pers-pro:case</ta>
            <ta e="T617" id="Seg_9858" s="T616">pers-pro:case</ta>
            <ta e="T618" id="Seg_9859" s="T617">que-pro:case</ta>
            <ta e="T619" id="Seg_9860" s="T618">ptcl</ta>
            <ta e="T620" id="Seg_9861" s="T619">v-v:tense-v:poss.pn</ta>
            <ta e="T621" id="Seg_9862" s="T620">v-v&gt;v-v:(tense)-v:mood.pn</ta>
            <ta e="T622" id="Seg_9863" s="T621">pers-pro:case</ta>
            <ta e="T623" id="Seg_9864" s="T622">que-pro:case</ta>
            <ta e="T624" id="Seg_9865" s="T623">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T625" id="Seg_9866" s="T624">conj</ta>
            <ta e="T626" id="Seg_9867" s="T625">v-v:cvb</ta>
            <ta e="T627" id="Seg_9868" s="T626">pers-pro:case</ta>
            <ta e="T628" id="Seg_9869" s="T627">v-v&gt;v-v:(tense)-v:mood.pn</ta>
            <ta e="T629" id="Seg_9870" s="T628">v-v:tense-v:poss.pn</ta>
            <ta e="T630" id="Seg_9871" s="T629">v-v:tense-v:poss.pn</ta>
            <ta e="T631" id="Seg_9872" s="T630">n-n:poss-n:case</ta>
            <ta e="T632" id="Seg_9873" s="T631">que</ta>
            <ta e="T633" id="Seg_9874" s="T632">ptcl</ta>
            <ta e="T634" id="Seg_9875" s="T633">v-v:(ins)-v:cvb</ta>
            <ta e="T635" id="Seg_9876" s="T634">n-n:case</ta>
            <ta e="T636" id="Seg_9877" s="T635">n-n:(num)-n:(poss)-n:case</ta>
            <ta e="T637" id="Seg_9878" s="T636">n-n:case</ta>
            <ta e="T638" id="Seg_9879" s="T637">v-v:tense-v:pred.pn</ta>
            <ta e="T639" id="Seg_9880" s="T638">adj</ta>
            <ta e="T640" id="Seg_9881" s="T639">n-n:case</ta>
            <ta e="T641" id="Seg_9882" s="T640">n-n:(num)-n:case</ta>
            <ta e="T642" id="Seg_9883" s="T641">v-v:cvb-v:pred.pn</ta>
            <ta e="T643" id="Seg_9884" s="T642">adv-adv&gt;adv-adv</ta>
            <ta e="T644" id="Seg_9885" s="T643">interj</ta>
            <ta e="T645" id="Seg_9886" s="T644">ptcl</ta>
            <ta e="T646" id="Seg_9887" s="T645">v-v:(neg)-v:pred.pn</ta>
            <ta e="T647" id="Seg_9888" s="T646">propr-n:case</ta>
            <ta e="T648" id="Seg_9889" s="T647">n-n:poss-n:case</ta>
            <ta e="T649" id="Seg_9890" s="T648">cardnum</ta>
            <ta e="T650" id="Seg_9891" s="T649">adj</ta>
            <ta e="T651" id="Seg_9892" s="T650">n-n:case</ta>
            <ta e="T652" id="Seg_9893" s="T651">v-v:tense-v:pred.pn</ta>
            <ta e="T653" id="Seg_9894" s="T652">propr-n:case</ta>
            <ta e="T654" id="Seg_9895" s="T653">n-n&gt;v-v:mood-v:pred.pn</ta>
            <ta e="T655" id="Seg_9896" s="T654">dempro</ta>
            <ta e="T656" id="Seg_9897" s="T655">n-n:case</ta>
            <ta e="T657" id="Seg_9898" s="T656">v-v:cvb</ta>
            <ta e="T658" id="Seg_9899" s="T657">v-v:ptcp</ta>
            <ta e="T659" id="Seg_9900" s="T658">adj</ta>
            <ta e="T660" id="Seg_9901" s="T659">n-n:case</ta>
            <ta e="T661" id="Seg_9902" s="T660">adj</ta>
            <ta e="T662" id="Seg_9903" s="T661">n-n&gt;adj</ta>
            <ta e="T663" id="Seg_9904" s="T662">n-n:case</ta>
            <ta e="T664" id="Seg_9905" s="T663">v-v:cvb</ta>
            <ta e="T665" id="Seg_9906" s="T664">interj</ta>
            <ta e="T666" id="Seg_9907" s="T665">v-v:cvb</ta>
            <ta e="T667" id="Seg_9908" s="T666">v-v:cvb</ta>
            <ta e="T668" id="Seg_9909" s="T667">v-v:tense-v:pred.pn</ta>
            <ta e="T669" id="Seg_9910" s="T668">v-v:cvb-v-v:ptcp-v:(ins)-v:(case)</ta>
            <ta e="T670" id="Seg_9911" s="T669">v-v:cvb</ta>
            <ta e="T671" id="Seg_9912" s="T670">v-v:cvb</ta>
            <ta e="T672" id="Seg_9913" s="T671">n-n:poss-n:case</ta>
            <ta e="T673" id="Seg_9914" s="T672">n-n:poss-n:case</ta>
            <ta e="T674" id="Seg_9915" s="T673">interj</ta>
            <ta e="T675" id="Seg_9916" s="T674">v-v:tense-v:pred.pn</ta>
            <ta e="T676" id="Seg_9917" s="T675">adv</ta>
            <ta e="T677" id="Seg_9918" s="T676">n-n:poss-n:case</ta>
            <ta e="T678" id="Seg_9919" s="T677">n-n:poss-n:case</ta>
            <ta e="T679" id="Seg_9920" s="T678">post</ta>
            <ta e="T680" id="Seg_9921" s="T679">v-v:cvb</ta>
            <ta e="T681" id="Seg_9922" s="T680">v-v:tense-v:pred.pn</ta>
            <ta e="T682" id="Seg_9923" s="T681">ptcl</ta>
            <ta e="T683" id="Seg_9924" s="T682">adv</ta>
            <ta e="T684" id="Seg_9925" s="T683">n-n:poss-n:case</ta>
            <ta e="T685" id="Seg_9926" s="T684">n-n:poss-n:case</ta>
            <ta e="T686" id="Seg_9927" s="T685">interj</ta>
            <ta e="T687" id="Seg_9928" s="T686">v-v:tense-v:pred.pn</ta>
            <ta e="T688" id="Seg_9929" s="T687">n-n:poss-n:case</ta>
            <ta e="T689" id="Seg_9930" s="T688">n-n:poss-n:case</ta>
            <ta e="T690" id="Seg_9931" s="T689">adv</ta>
            <ta e="T691" id="Seg_9932" s="T690">n-n:poss-n:case</ta>
            <ta e="T692" id="Seg_9933" s="T691">v-v:ptcp</ta>
            <ta e="T693" id="Seg_9934" s="T692">adj</ta>
            <ta e="T694" id="Seg_9935" s="T693">n-n:(poss)-n:case</ta>
            <ta e="T695" id="Seg_9936" s="T694">v-v:cvb</ta>
            <ta e="T696" id="Seg_9937" s="T695">v-v:tense-v:pred.pn</ta>
            <ta e="T697" id="Seg_9938" s="T696">dempro-pro:case</ta>
            <ta e="T698" id="Seg_9939" s="T697">n-n:case</ta>
            <ta e="T699" id="Seg_9940" s="T698">propr-n:case</ta>
            <ta e="T700" id="Seg_9941" s="T699">n-n:case</ta>
            <ta e="T701" id="Seg_9942" s="T700">v-v:tense-v:pred.pn</ta>
            <ta e="T702" id="Seg_9943" s="T701">conj</ta>
            <ta e="T703" id="Seg_9944" s="T702">n-n:case</ta>
            <ta e="T704" id="Seg_9945" s="T703">dempro</ta>
            <ta e="T705" id="Seg_9946" s="T704">adj</ta>
            <ta e="T706" id="Seg_9947" s="T705">n-n:poss-n:case</ta>
            <ta e="T707" id="Seg_9948" s="T706">post</ta>
            <ta e="T708" id="Seg_9949" s="T707">v-v:cvb</ta>
            <ta e="T709" id="Seg_9950" s="T708">v-v:tense-v:pred.pn</ta>
            <ta e="T710" id="Seg_9951" s="T709">v-v:tense-v:pred.pn</ta>
            <ta e="T711" id="Seg_9952" s="T710">conj</ta>
            <ta e="T712" id="Seg_9953" s="T711">dempro-pro:case</ta>
            <ta e="T713" id="Seg_9954" s="T712">v-v:(ins)-v&gt;v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T714" id="Seg_9955" s="T713">v-v:cvb</ta>
            <ta e="T715" id="Seg_9956" s="T714">n-n:case</ta>
            <ta e="T716" id="Seg_9957" s="T715">n-n:case</ta>
            <ta e="T717" id="Seg_9958" s="T716">adj-adj&gt;v-v:cvb-v-v&gt;n-n&gt;v-v:cvb</ta>
            <ta e="T718" id="Seg_9959" s="T717">v-v:cvb</ta>
            <ta e="T719" id="Seg_9960" s="T718">v-v:tense-v:pred.pn</ta>
            <ta e="T720" id="Seg_9961" s="T719">n-n:(num)-n:case</ta>
            <ta e="T721" id="Seg_9962" s="T720">n-n&gt;v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T722" id="Seg_9963" s="T721">n-n:(poss)-n:case</ta>
            <ta e="T723" id="Seg_9964" s="T722">n-n:case-n-n:case</ta>
            <ta e="T724" id="Seg_9965" s="T723">ptcl</ta>
            <ta e="T725" id="Seg_9966" s="T724">v-v:ptcp-v:(case)</ta>
            <ta e="T726" id="Seg_9967" s="T725">v-v:tense-v:pred.pn</ta>
            <ta e="T727" id="Seg_9968" s="T726">adj</ta>
            <ta e="T728" id="Seg_9969" s="T727">n-n:case</ta>
            <ta e="T729" id="Seg_9970" s="T728">adv</ta>
            <ta e="T730" id="Seg_9971" s="T729">n-n&gt;adj</ta>
            <ta e="T731" id="Seg_9972" s="T730">n-n:poss-n:case</ta>
            <ta e="T732" id="Seg_9973" s="T731">v-v&gt;v-v:cvb</ta>
            <ta e="T733" id="Seg_9974" s="T732">v-v:tense-v:pred.pn</ta>
            <ta e="T734" id="Seg_9975" s="T733">propr-n:case</ta>
            <ta e="T735" id="Seg_9976" s="T734">v-v:cvb</ta>
            <ta e="T736" id="Seg_9977" s="T735">v-v:(ins)-v&gt;v-v&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T737" id="Seg_9978" s="T736">v-v:cvb</ta>
            <ta e="T738" id="Seg_9979" s="T737">v-v:tense-v:pred.pn</ta>
            <ta e="T739" id="Seg_9980" s="T738">conj</ta>
            <ta e="T740" id="Seg_9981" s="T739">n-n:poss-n:case</ta>
            <ta e="T741" id="Seg_9982" s="T740">v-v&gt;v-v:cvb</ta>
            <ta e="T742" id="Seg_9983" s="T741">v-v:tense-v:pred.pn</ta>
            <ta e="T743" id="Seg_9984" s="T742">propr-n:case</ta>
            <ta e="T744" id="Seg_9985" s="T743">n-n:poss-n:case</ta>
            <ta e="T745" id="Seg_9986" s="T744">v-v:tense-v:pred.pn</ta>
            <ta e="T746" id="Seg_9987" s="T745">n-n:(num)-n:case</ta>
            <ta e="T747" id="Seg_9988" s="T746">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T748" id="Seg_9989" s="T747">n-n:(poss)-n:case</ta>
            <ta e="T749" id="Seg_9990" s="T748">v-v:cvb</ta>
            <ta e="T750" id="Seg_9991" s="T749">cardnum</ta>
            <ta e="T751" id="Seg_9992" s="T750">adj</ta>
            <ta e="T752" id="Seg_9993" s="T751">n-n:case</ta>
            <ta e="T753" id="Seg_9994" s="T752">v-v:cvb</ta>
            <ta e="T754" id="Seg_9995" s="T753">v-v:tense-v:pred.pn</ta>
            <ta e="T755" id="Seg_9996" s="T754">propr-n:case</ta>
            <ta e="T756" id="Seg_9997" s="T755">interj</ta>
            <ta e="T757" id="Seg_9998" s="T756">v-v:cvb</ta>
            <ta e="T758" id="Seg_9999" s="T757">n-n&gt;n-n:case</ta>
            <ta e="T759" id="Seg_10000" s="T758">v-v:cvb</ta>
            <ta e="T760" id="Seg_10001" s="T759">n-n:case</ta>
            <ta e="T761" id="Seg_10002" s="T760">v-v:tense-v:pred.pn</ta>
            <ta e="T762" id="Seg_10003" s="T761">n-n:poss-n:case</ta>
            <ta e="T763" id="Seg_10004" s="T762">n-n:poss-n:case</ta>
            <ta e="T764" id="Seg_10005" s="T763">interj</ta>
            <ta e="T765" id="Seg_10006" s="T764">v-v:tense-v:pred.pn</ta>
            <ta e="T766" id="Seg_10007" s="T765">n-n:poss-n:case</ta>
            <ta e="T767" id="Seg_10008" s="T766">post</ta>
            <ta e="T768" id="Seg_10009" s="T767">v-v:tense-v:pred.pn</ta>
            <ta e="T769" id="Seg_10010" s="T768">n-n:poss-n:case</ta>
            <ta e="T770" id="Seg_10011" s="T769">n-n:poss-n:case</ta>
            <ta e="T771" id="Seg_10012" s="T770">interj</ta>
            <ta e="T772" id="Seg_10013" s="T771">v-v:tense-v:pred.pn</ta>
            <ta e="T773" id="Seg_10014" s="T772">n-n:poss-n:case</ta>
            <ta e="T774" id="Seg_10015" s="T773">n-n:poss-n:case</ta>
            <ta e="T775" id="Seg_10016" s="T774">post</ta>
            <ta e="T776" id="Seg_10017" s="T775">que-pro:case</ta>
            <ta e="T777" id="Seg_10018" s="T776">ptcl</ta>
            <ta e="T778" id="Seg_10019" s="T777">adj</ta>
            <ta e="T779" id="Seg_10020" s="T778">n-n&gt;adj</ta>
            <ta e="T780" id="Seg_10021" s="T779">n-n&gt;n-n:case</ta>
            <ta e="T781" id="Seg_10022" s="T780">v-v:cvb</ta>
            <ta e="T782" id="Seg_10023" s="T781">v-v:cvb</ta>
            <ta e="T783" id="Seg_10024" s="T782">v-v:tense-v:pred.pn</ta>
            <ta e="T784" id="Seg_10025" s="T783">n-n:poss-n:case</ta>
            <ta e="T785" id="Seg_10026" s="T784">n-n&gt;adj</ta>
            <ta e="T786" id="Seg_10027" s="T785">n-n:case</ta>
            <ta e="T787" id="Seg_10028" s="T786">v-v:tense-v:pred.pn</ta>
            <ta e="T788" id="Seg_10029" s="T787">conj</ta>
            <ta e="T789" id="Seg_10030" s="T788">n-n:(num)-n:case</ta>
            <ta e="T790" id="Seg_10031" s="T789">n-n:poss-n:case</ta>
            <ta e="T791" id="Seg_10032" s="T790">adv</ta>
            <ta e="T792" id="Seg_10033" s="T791">v-v:tense-v:pred.pn</ta>
            <ta e="T793" id="Seg_10034" s="T792">n-n&gt;v-v:cvb</ta>
            <ta e="T794" id="Seg_10035" s="T793">v-v:tense-v:pred.pn</ta>
            <ta e="T795" id="Seg_10036" s="T794">n-n:(num)-n:case</ta>
            <ta e="T796" id="Seg_10037" s="T795">v-v:cvb</ta>
            <ta e="T797" id="Seg_10038" s="T796">v-v:tense-v:pred.pn</ta>
            <ta e="T798" id="Seg_10039" s="T797">v-v:cvb</ta>
            <ta e="T799" id="Seg_10040" s="T798">ptcl</ta>
            <ta e="T800" id="Seg_10041" s="T799">v-v:tense-v:poss.pn</ta>
            <ta e="T801" id="Seg_10042" s="T800">n-n:(poss)-n:case-n-n:(poss)-n:case</ta>
            <ta e="T802" id="Seg_10043" s="T801">que</ta>
            <ta e="T803" id="Seg_10044" s="T802">ptcl</ta>
            <ta e="T804" id="Seg_10045" s="T803">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T805" id="Seg_10046" s="T804">v-v:(neg)-v:tense-v:pred.pn</ta>
            <ta e="T806" id="Seg_10047" s="T805">n-n:case</ta>
            <ta e="T807" id="Seg_10048" s="T806">n-n:(num)-n:(poss)-n:case</ta>
            <ta e="T808" id="Seg_10049" s="T807">v-v:cvb</ta>
            <ta e="T809" id="Seg_10050" s="T808">dempro</ta>
            <ta e="T810" id="Seg_10051" s="T809">n-n:case</ta>
            <ta e="T811" id="Seg_10052" s="T810">propr-n:case</ta>
            <ta e="T812" id="Seg_10053" s="T811">n-n:case-n:case-n-n:case-n:case</ta>
            <ta e="T813" id="Seg_10054" s="T812">v-v:cvb</ta>
            <ta e="T814" id="Seg_10055" s="T813">n-n:case</ta>
            <ta e="T815" id="Seg_10056" s="T814">v-v:tense-v:poss.pn</ta>
            <ta e="T816" id="Seg_10057" s="T815">ptcl</ta>
            <ta e="T817" id="Seg_10058" s="T816">propr-n:case</ta>
            <ta e="T818" id="Seg_10059" s="T817">n-n:case-n-n:case</ta>
            <ta e="T819" id="Seg_10060" s="T818">v-v:tense-v:pred.pn</ta>
            <ta e="T820" id="Seg_10061" s="T819">n-n:case</ta>
            <ta e="T821" id="Seg_10062" s="T820">n-n:poss-n:case</ta>
            <ta e="T822" id="Seg_10063" s="T821">n-n&gt;v-v:cvb</ta>
            <ta e="T823" id="Seg_10064" s="T822">v-v:cvb-v-v:cvb</ta>
            <ta e="T824" id="Seg_10065" s="T823">v-v:tense-v:poss.pn</ta>
            <ta e="T825" id="Seg_10066" s="T824">ptcl</ta>
            <ta e="T826" id="Seg_10067" s="T825">adj-n:(poss)-n:case</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_10068" s="T0">propr</ta>
            <ta e="T2" id="Seg_10069" s="T1">dempro</ta>
            <ta e="T3" id="Seg_10070" s="T2">propr</ta>
            <ta e="T4" id="Seg_10071" s="T3">v</ta>
            <ta e="T5" id="Seg_10072" s="T4">cardnum</ta>
            <ta e="T6" id="Seg_10073" s="T5">n</ta>
            <ta e="T7" id="Seg_10074" s="T6">n</ta>
            <ta e="T8" id="Seg_10075" s="T7">v</ta>
            <ta e="T9" id="Seg_10076" s="T8">collnum</ta>
            <ta e="T10" id="Seg_10077" s="T9">adj</ta>
            <ta e="T11" id="Seg_10078" s="T10">adj</ta>
            <ta e="T12" id="Seg_10079" s="T11">ptcl</ta>
            <ta e="T13" id="Seg_10080" s="T12">dempro</ta>
            <ta e="T14" id="Seg_10081" s="T13">v</ta>
            <ta e="T15" id="Seg_10082" s="T14">propr</ta>
            <ta e="T16" id="Seg_10083" s="T15">v</ta>
            <ta e="T17" id="Seg_10084" s="T16">propr</ta>
            <ta e="T18" id="Seg_10085" s="T17">n</ta>
            <ta e="T19" id="Seg_10086" s="T18">dempro</ta>
            <ta e="T20" id="Seg_10087" s="T19">pers</ta>
            <ta e="T21" id="Seg_10088" s="T20">que</ta>
            <ta e="T22" id="Seg_10089" s="T21">adj</ta>
            <ta e="T23" id="Seg_10090" s="T22">cop</ta>
            <ta e="T24" id="Seg_10091" s="T23">que</ta>
            <ta e="T25" id="Seg_10092" s="T24">n</ta>
            <ta e="T26" id="Seg_10093" s="T25">n</ta>
            <ta e="T27" id="Seg_10094" s="T26">v</ta>
            <ta e="T28" id="Seg_10095" s="T27">n</ta>
            <ta e="T29" id="Seg_10096" s="T28">v</ta>
            <ta e="T30" id="Seg_10097" s="T29">v</ta>
            <ta e="T31" id="Seg_10098" s="T30">adv</ta>
            <ta e="T32" id="Seg_10099" s="T31">n</ta>
            <ta e="T33" id="Seg_10100" s="T32">interj</ta>
            <ta e="T34" id="Seg_10101" s="T33">n</ta>
            <ta e="T35" id="Seg_10102" s="T34">que</ta>
            <ta e="T36" id="Seg_10103" s="T35">v</ta>
            <ta e="T37" id="Seg_10104" s="T36">n</ta>
            <ta e="T38" id="Seg_10105" s="T37">v</ta>
            <ta e="T39" id="Seg_10106" s="T38">v</ta>
            <ta e="T40" id="Seg_10107" s="T39">aux</ta>
            <ta e="T41" id="Seg_10108" s="T40">n</ta>
            <ta e="T42" id="Seg_10109" s="T41">v</ta>
            <ta e="T43" id="Seg_10110" s="T42">que</ta>
            <ta e="T44" id="Seg_10111" s="T43">n</ta>
            <ta e="T45" id="Seg_10112" s="T44">pers</ta>
            <ta e="T46" id="Seg_10113" s="T45">n</ta>
            <ta e="T47" id="Seg_10114" s="T46">v</ta>
            <ta e="T48" id="Seg_10115" s="T47">v</ta>
            <ta e="T49" id="Seg_10116" s="T48">n</ta>
            <ta e="T50" id="Seg_10117" s="T49">v</ta>
            <ta e="T51" id="Seg_10118" s="T50">propr</ta>
            <ta e="T52" id="Seg_10119" s="T51">pers</ta>
            <ta e="T53" id="Seg_10120" s="T52">v</ta>
            <ta e="T54" id="Seg_10121" s="T53">pers</ta>
            <ta e="T55" id="Seg_10122" s="T54">v</ta>
            <ta e="T56" id="Seg_10123" s="T55">pers</ta>
            <ta e="T57" id="Seg_10124" s="T56">pers</ta>
            <ta e="T58" id="Seg_10125" s="T57">que</ta>
            <ta e="T59" id="Seg_10126" s="T58">cop</ta>
            <ta e="T60" id="Seg_10127" s="T59">conj</ta>
            <ta e="T61" id="Seg_10128" s="T60">adv</ta>
            <ta e="T62" id="Seg_10129" s="T61">cop</ta>
            <ta e="T63" id="Seg_10130" s="T62">aux</ta>
            <ta e="T64" id="Seg_10131" s="T63">v</ta>
            <ta e="T65" id="Seg_10132" s="T64">n</ta>
            <ta e="T66" id="Seg_10133" s="T65">v</ta>
            <ta e="T67" id="Seg_10134" s="T66">ptcl</ta>
            <ta e="T68" id="Seg_10135" s="T67">ptcl</ta>
            <ta e="T69" id="Seg_10136" s="T68">adv</ta>
            <ta e="T70" id="Seg_10137" s="T69">n</ta>
            <ta e="T71" id="Seg_10138" s="T70">n</ta>
            <ta e="T72" id="Seg_10139" s="T71">v</ta>
            <ta e="T73" id="Seg_10140" s="T72">n</ta>
            <ta e="T74" id="Seg_10141" s="T73">v</ta>
            <ta e="T75" id="Seg_10142" s="T74">pers</ta>
            <ta e="T76" id="Seg_10143" s="T75">v</ta>
            <ta e="T77" id="Seg_10144" s="T76">propr</ta>
            <ta e="T78" id="Seg_10145" s="T77">adv</ta>
            <ta e="T79" id="Seg_10146" s="T78">v</ta>
            <ta e="T80" id="Seg_10147" s="T79">v</ta>
            <ta e="T81" id="Seg_10148" s="T80">n</ta>
            <ta e="T82" id="Seg_10149" s="T81">n</ta>
            <ta e="T83" id="Seg_10150" s="T82">propr</ta>
            <ta e="T84" id="Seg_10151" s="T83">v</ta>
            <ta e="T85" id="Seg_10152" s="T84">v</ta>
            <ta e="T86" id="Seg_10153" s="T85">v</ta>
            <ta e="T87" id="Seg_10154" s="T86">v</ta>
            <ta e="T88" id="Seg_10155" s="T87">aux</ta>
            <ta e="T89" id="Seg_10156" s="T88">n</ta>
            <ta e="T90" id="Seg_10157" s="T89">ptcl</ta>
            <ta e="T91" id="Seg_10158" s="T90">ptcl</ta>
            <ta e="T92" id="Seg_10159" s="T91">n</ta>
            <ta e="T93" id="Seg_10160" s="T92">v</ta>
            <ta e="T94" id="Seg_10161" s="T93">propr</ta>
            <ta e="T95" id="Seg_10162" s="T94">n</ta>
            <ta e="T96" id="Seg_10163" s="T95">v</ta>
            <ta e="T97" id="Seg_10164" s="T96">aux</ta>
            <ta e="T98" id="Seg_10165" s="T97">v</ta>
            <ta e="T99" id="Seg_10166" s="T98">aux</ta>
            <ta e="T100" id="Seg_10167" s="T99">propr</ta>
            <ta e="T101" id="Seg_10168" s="T100">n</ta>
            <ta e="T102" id="Seg_10169" s="T101">v</ta>
            <ta e="T103" id="Seg_10170" s="T102">n</ta>
            <ta e="T104" id="Seg_10171" s="T103">v</ta>
            <ta e="T105" id="Seg_10172" s="T104">adj</ta>
            <ta e="T106" id="Seg_10173" s="T105">cop</ta>
            <ta e="T107" id="Seg_10174" s="T106">v</ta>
            <ta e="T108" id="Seg_10175" s="T107">n</ta>
            <ta e="T109" id="Seg_10176" s="T108">v</ta>
            <ta e="T110" id="Seg_10177" s="T109">propr</ta>
            <ta e="T111" id="Seg_10178" s="T110">n</ta>
            <ta e="T112" id="Seg_10179" s="T111">n</ta>
            <ta e="T113" id="Seg_10180" s="T112">adj</ta>
            <ta e="T114" id="Seg_10181" s="T113">n</ta>
            <ta e="T115" id="Seg_10182" s="T114">n</ta>
            <ta e="T116" id="Seg_10183" s="T115">v</ta>
            <ta e="T117" id="Seg_10184" s="T116">aux</ta>
            <ta e="T118" id="Seg_10185" s="T117">adj</ta>
            <ta e="T119" id="Seg_10186" s="T118">n</ta>
            <ta e="T120" id="Seg_10187" s="T119">ptcl</ta>
            <ta e="T121" id="Seg_10188" s="T120">propr</ta>
            <ta e="T122" id="Seg_10189" s="T121">n</ta>
            <ta e="T123" id="Seg_10190" s="T122">cop</ta>
            <ta e="T124" id="Seg_10191" s="T123">conj</ta>
            <ta e="T125" id="Seg_10192" s="T124">n</ta>
            <ta e="T126" id="Seg_10193" s="T125">v</ta>
            <ta e="T127" id="Seg_10194" s="T126">n</ta>
            <ta e="T128" id="Seg_10195" s="T127">n</ta>
            <ta e="T129" id="Seg_10196" s="T128">v</ta>
            <ta e="T130" id="Seg_10197" s="T129">n</ta>
            <ta e="T131" id="Seg_10198" s="T130">cop</ta>
            <ta e="T132" id="Seg_10199" s="T131">v</ta>
            <ta e="T133" id="Seg_10200" s="T132">aux</ta>
            <ta e="T134" id="Seg_10201" s="T133">propr</ta>
            <ta e="T135" id="Seg_10202" s="T134">n</ta>
            <ta e="T136" id="Seg_10203" s="T135">cop</ta>
            <ta e="T137" id="Seg_10204" s="T136">v</ta>
            <ta e="T138" id="Seg_10205" s="T137">aux</ta>
            <ta e="T139" id="Seg_10206" s="T138">interj</ta>
            <ta e="T140" id="Seg_10207" s="T139">n</ta>
            <ta e="T141" id="Seg_10208" s="T140">v</ta>
            <ta e="T142" id="Seg_10209" s="T141">aux</ta>
            <ta e="T143" id="Seg_10210" s="T142">dempro</ta>
            <ta e="T144" id="Seg_10211" s="T143">n</ta>
            <ta e="T145" id="Seg_10212" s="T144">n</ta>
            <ta e="T146" id="Seg_10213" s="T145">v</ta>
            <ta e="T147" id="Seg_10214" s="T146">conj</ta>
            <ta e="T148" id="Seg_10215" s="T147">adj</ta>
            <ta e="T149" id="Seg_10216" s="T148">n</ta>
            <ta e="T150" id="Seg_10217" s="T149">n</ta>
            <ta e="T151" id="Seg_10218" s="T150">n</ta>
            <ta e="T152" id="Seg_10219" s="T151">v</ta>
            <ta e="T153" id="Seg_10220" s="T152">aux</ta>
            <ta e="T154" id="Seg_10221" s="T153">n</ta>
            <ta e="T155" id="Seg_10222" s="T154">v</ta>
            <ta e="T156" id="Seg_10223" s="T155">aux</ta>
            <ta e="T157" id="Seg_10224" s="T156">propr</ta>
            <ta e="T158" id="Seg_10225" s="T157">n</ta>
            <ta e="T159" id="Seg_10226" s="T158">cop</ta>
            <ta e="T160" id="Seg_10227" s="T159">conj</ta>
            <ta e="T161" id="Seg_10228" s="T160">v</ta>
            <ta e="T162" id="Seg_10229" s="T161">aux</ta>
            <ta e="T163" id="Seg_10230" s="T162">cardnum</ta>
            <ta e="T164" id="Seg_10231" s="T163">n</ta>
            <ta e="T165" id="Seg_10232" s="T164">v</ta>
            <ta e="T166" id="Seg_10233" s="T165">conj</ta>
            <ta e="T167" id="Seg_10234" s="T166">propr</ta>
            <ta e="T168" id="Seg_10235" s="T167">n</ta>
            <ta e="T169" id="Seg_10236" s="T168">cop</ta>
            <ta e="T170" id="Seg_10237" s="T169">v</ta>
            <ta e="T171" id="Seg_10238" s="T170">n</ta>
            <ta e="T172" id="Seg_10239" s="T171">n</ta>
            <ta e="T173" id="Seg_10240" s="T172">cop</ta>
            <ta e="T174" id="Seg_10241" s="T173">v</ta>
            <ta e="T175" id="Seg_10242" s="T174">propr</ta>
            <ta e="T176" id="Seg_10243" s="T175">n</ta>
            <ta e="T177" id="Seg_10244" s="T176">cop</ta>
            <ta e="T178" id="Seg_10245" s="T177">adv</ta>
            <ta e="T179" id="Seg_10246" s="T178">n</ta>
            <ta e="T180" id="Seg_10247" s="T179">ptcl</ta>
            <ta e="T181" id="Seg_10248" s="T180">n</ta>
            <ta e="T182" id="Seg_10249" s="T181">cop</ta>
            <ta e="T183" id="Seg_10250" s="T182">n</ta>
            <ta e="T184" id="Seg_10251" s="T183">ptcl</ta>
            <ta e="T185" id="Seg_10252" s="T184">v</ta>
            <ta e="T186" id="Seg_10253" s="T185">aux</ta>
            <ta e="T187" id="Seg_10254" s="T186">dempro</ta>
            <ta e="T188" id="Seg_10255" s="T187">n</ta>
            <ta e="T189" id="Seg_10256" s="T188">cop</ta>
            <ta e="T190" id="Seg_10257" s="T189">v</ta>
            <ta e="T191" id="Seg_10258" s="T190">v</ta>
            <ta e="T192" id="Seg_10259" s="T191">n</ta>
            <ta e="T193" id="Seg_10260" s="T192">v</ta>
            <ta e="T194" id="Seg_10261" s="T193">aux</ta>
            <ta e="T195" id="Seg_10262" s="T194">n</ta>
            <ta e="T196" id="Seg_10263" s="T195">v</ta>
            <ta e="T197" id="Seg_10264" s="T196">v</ta>
            <ta e="T198" id="Seg_10265" s="T197">ptcl</ta>
            <ta e="T199" id="Seg_10266" s="T198">v</ta>
            <ta e="T200" id="Seg_10267" s="T199">n</ta>
            <ta e="T201" id="Seg_10268" s="T200">cop</ta>
            <ta e="T202" id="Seg_10269" s="T201">v</ta>
            <ta e="T203" id="Seg_10270" s="T202">aux</ta>
            <ta e="T204" id="Seg_10271" s="T203">que</ta>
            <ta e="T205" id="Seg_10272" s="T204">v</ta>
            <ta e="T206" id="Seg_10273" s="T205">v</ta>
            <ta e="T207" id="Seg_10274" s="T206">propr</ta>
            <ta e="T208" id="Seg_10275" s="T207">n</ta>
            <ta e="T209" id="Seg_10276" s="T208">cop</ta>
            <ta e="T210" id="Seg_10277" s="T209">v</ta>
            <ta e="T211" id="Seg_10278" s="T210">aux</ta>
            <ta e="T212" id="Seg_10279" s="T211">n</ta>
            <ta e="T213" id="Seg_10280" s="T212">ptcl</ta>
            <ta e="T214" id="Seg_10281" s="T213">adv</ta>
            <ta e="T215" id="Seg_10282" s="T214">v</ta>
            <ta e="T216" id="Seg_10283" s="T215">n</ta>
            <ta e="T217" id="Seg_10284" s="T216">cop</ta>
            <ta e="T218" id="Seg_10285" s="T217">v</ta>
            <ta e="T219" id="Seg_10286" s="T218">aux</ta>
            <ta e="T220" id="Seg_10287" s="T219">n</ta>
            <ta e="T221" id="Seg_10288" s="T220">n</ta>
            <ta e="T222" id="Seg_10289" s="T221">v</ta>
            <ta e="T223" id="Seg_10290" s="T222">n</ta>
            <ta e="T224" id="Seg_10291" s="T223">cop</ta>
            <ta e="T225" id="Seg_10292" s="T224">v</ta>
            <ta e="T226" id="Seg_10293" s="T225">n</ta>
            <ta e="T227" id="Seg_10294" s="T226">cop</ta>
            <ta e="T228" id="Seg_10295" s="T227">v</ta>
            <ta e="T229" id="Seg_10296" s="T228">aux</ta>
            <ta e="T230" id="Seg_10297" s="T229">propr</ta>
            <ta e="T231" id="Seg_10298" s="T230">dempro</ta>
            <ta e="T232" id="Seg_10299" s="T231">post</ta>
            <ta e="T233" id="Seg_10300" s="T232">interj</ta>
            <ta e="T234" id="Seg_10301" s="T233">n</ta>
            <ta e="T235" id="Seg_10302" s="T234">n</ta>
            <ta e="T236" id="Seg_10303" s="T235">n</ta>
            <ta e="T237" id="Seg_10304" s="T236">n</ta>
            <ta e="T238" id="Seg_10305" s="T237">v</ta>
            <ta e="T239" id="Seg_10306" s="T238">conj</ta>
            <ta e="T240" id="Seg_10307" s="T239">n</ta>
            <ta e="T241" id="Seg_10308" s="T240">n</ta>
            <ta e="T242" id="Seg_10309" s="T241">cop</ta>
            <ta e="T243" id="Seg_10310" s="T242">v</ta>
            <ta e="T244" id="Seg_10311" s="T243">aux</ta>
            <ta e="T245" id="Seg_10312" s="T244">v</ta>
            <ta e="T246" id="Seg_10313" s="T245">ptcl</ta>
            <ta e="T247" id="Seg_10314" s="T246">aux</ta>
            <ta e="T248" id="Seg_10315" s="T247">adv</ta>
            <ta e="T249" id="Seg_10316" s="T248">v</ta>
            <ta e="T250" id="Seg_10317" s="T249">n</ta>
            <ta e="T251" id="Seg_10318" s="T250">propr</ta>
            <ta e="T252" id="Seg_10319" s="T251">n</ta>
            <ta e="T253" id="Seg_10320" s="T252">n</ta>
            <ta e="T254" id="Seg_10321" s="T253">cop</ta>
            <ta e="T255" id="Seg_10322" s="T254">n</ta>
            <ta e="T256" id="Seg_10323" s="T255">conj</ta>
            <ta e="T257" id="Seg_10324" s="T256">adj</ta>
            <ta e="T258" id="Seg_10325" s="T257">ptcl</ta>
            <ta e="T259" id="Seg_10326" s="T258">v</ta>
            <ta e="T260" id="Seg_10327" s="T259">ptcl</ta>
            <ta e="T261" id="Seg_10328" s="T260">dempro</ta>
            <ta e="T262" id="Seg_10329" s="T261">v</ta>
            <ta e="T263" id="Seg_10330" s="T262">v</ta>
            <ta e="T264" id="Seg_10331" s="T263">v</ta>
            <ta e="T265" id="Seg_10332" s="T264">cardnum</ta>
            <ta e="T266" id="Seg_10333" s="T265">n</ta>
            <ta e="T267" id="Seg_10334" s="T266">n</ta>
            <ta e="T268" id="Seg_10335" s="T267">v</ta>
            <ta e="T269" id="Seg_10336" s="T268">v</ta>
            <ta e="T270" id="Seg_10337" s="T269">v</ta>
            <ta e="T271" id="Seg_10338" s="T270">v</ta>
            <ta e="T272" id="Seg_10339" s="T271">cardnum</ta>
            <ta e="T273" id="Seg_10340" s="T272">adv</ta>
            <ta e="T274" id="Seg_10341" s="T273">n</ta>
            <ta e="T275" id="Seg_10342" s="T274">v</ta>
            <ta e="T276" id="Seg_10343" s="T275">aux</ta>
            <ta e="T277" id="Seg_10344" s="T276">n</ta>
            <ta e="T278" id="Seg_10345" s="T277">v</ta>
            <ta e="T279" id="Seg_10346" s="T278">n</ta>
            <ta e="T280" id="Seg_10347" s="T279">n</ta>
            <ta e="T281" id="Seg_10348" s="T280">v</ta>
            <ta e="T282" id="Seg_10349" s="T281">aux</ta>
            <ta e="T283" id="Seg_10350" s="T282">dempro</ta>
            <ta e="T284" id="Seg_10351" s="T283">post</ta>
            <ta e="T285" id="Seg_10352" s="T284">propr</ta>
            <ta e="T286" id="Seg_10353" s="T285">n</ta>
            <ta e="T287" id="Seg_10354" s="T286">v</ta>
            <ta e="T288" id="Seg_10355" s="T287">n</ta>
            <ta e="T289" id="Seg_10356" s="T288">cop</ta>
            <ta e="T290" id="Seg_10357" s="T289">v</ta>
            <ta e="T291" id="Seg_10358" s="T290">aux</ta>
            <ta e="T292" id="Seg_10359" s="T291">que</ta>
            <ta e="T293" id="Seg_10360" s="T292">ptcl</ta>
            <ta e="T294" id="Seg_10361" s="T293">cop</ta>
            <ta e="T295" id="Seg_10362" s="T294">n</ta>
            <ta e="T296" id="Seg_10363" s="T295">v</ta>
            <ta e="T297" id="Seg_10364" s="T296">aux</ta>
            <ta e="T298" id="Seg_10365" s="T297">v</ta>
            <ta e="T299" id="Seg_10366" s="T298">v</ta>
            <ta e="T300" id="Seg_10367" s="T299">ptcl</ta>
            <ta e="T301" id="Seg_10368" s="T300">v</ta>
            <ta e="T302" id="Seg_10369" s="T301">dempro</ta>
            <ta e="T303" id="Seg_10370" s="T302">n</ta>
            <ta e="T304" id="Seg_10371" s="T303">v</ta>
            <ta e="T305" id="Seg_10372" s="T304">n</ta>
            <ta e="T306" id="Seg_10373" s="T305">adj</ta>
            <ta e="T307" id="Seg_10374" s="T306">ptcl</ta>
            <ta e="T308" id="Seg_10375" s="T307">v</ta>
            <ta e="T309" id="Seg_10376" s="T308">n</ta>
            <ta e="T310" id="Seg_10377" s="T309">ptcl</ta>
            <ta e="T311" id="Seg_10378" s="T310">v</ta>
            <ta e="T312" id="Seg_10379" s="T311">aux</ta>
            <ta e="T313" id="Seg_10380" s="T312">interj</ta>
            <ta e="T314" id="Seg_10381" s="T313">ptcl</ta>
            <ta e="T315" id="Seg_10382" s="T314">dempro</ta>
            <ta e="T316" id="Seg_10383" s="T315">que</ta>
            <ta e="T317" id="Seg_10384" s="T316">v</ta>
            <ta e="T318" id="Seg_10385" s="T317">emphpro</ta>
            <ta e="T319" id="Seg_10386" s="T318">v</ta>
            <ta e="T320" id="Seg_10387" s="T319">v</ta>
            <ta e="T321" id="Seg_10388" s="T320">adv</ta>
            <ta e="T322" id="Seg_10389" s="T321">que</ta>
            <ta e="T323" id="Seg_10390" s="T322">n</ta>
            <ta e="T324" id="Seg_10391" s="T323">v</ta>
            <ta e="T325" id="Seg_10392" s="T324">v</ta>
            <ta e="T326" id="Seg_10393" s="T325">aux</ta>
            <ta e="T327" id="Seg_10394" s="T326">n</ta>
            <ta e="T328" id="Seg_10395" s="T327">cop</ta>
            <ta e="T329" id="Seg_10396" s="T328">v</ta>
            <ta e="T330" id="Seg_10397" s="T329">aux</ta>
            <ta e="T331" id="Seg_10398" s="T330">ptcl</ta>
            <ta e="T332" id="Seg_10399" s="T331">n</ta>
            <ta e="T333" id="Seg_10400" s="T332">v</ta>
            <ta e="T334" id="Seg_10401" s="T333">aux</ta>
            <ta e="T335" id="Seg_10402" s="T334">ptcl</ta>
            <ta e="T336" id="Seg_10403" s="T335">v</ta>
            <ta e="T337" id="Seg_10404" s="T336">ptcl</ta>
            <ta e="T338" id="Seg_10405" s="T337">adv</ta>
            <ta e="T339" id="Seg_10406" s="T338">cardnum</ta>
            <ta e="T340" id="Seg_10407" s="T339">n</ta>
            <ta e="T341" id="Seg_10408" s="T340">v</ta>
            <ta e="T342" id="Seg_10409" s="T341">v</ta>
            <ta e="T343" id="Seg_10410" s="T342">interj</ta>
            <ta e="T344" id="Seg_10411" s="T343">dempro</ta>
            <ta e="T345" id="Seg_10412" s="T344">que</ta>
            <ta e="T346" id="Seg_10413" s="T345">n</ta>
            <ta e="T347" id="Seg_10414" s="T346">n</ta>
            <ta e="T348" id="Seg_10415" s="T347">v</ta>
            <ta e="T349" id="Seg_10416" s="T348">ptcl</ta>
            <ta e="T350" id="Seg_10417" s="T349">n</ta>
            <ta e="T351" id="Seg_10418" s="T350">ptcl</ta>
            <ta e="T352" id="Seg_10419" s="T351">v</ta>
            <ta e="T353" id="Seg_10420" s="T352">cardnum</ta>
            <ta e="T354" id="Seg_10421" s="T353">n</ta>
            <ta e="T355" id="Seg_10422" s="T354">v</ta>
            <ta e="T356" id="Seg_10423" s="T355">aux</ta>
            <ta e="T357" id="Seg_10424" s="T356">n</ta>
            <ta e="T358" id="Seg_10425" s="T357">n</ta>
            <ta e="T359" id="Seg_10426" s="T358">que</ta>
            <ta e="T360" id="Seg_10427" s="T359">ptcl</ta>
            <ta e="T361" id="Seg_10428" s="T360">dempro</ta>
            <ta e="T362" id="Seg_10429" s="T361">cardnum</ta>
            <ta e="T363" id="Seg_10430" s="T362">n</ta>
            <ta e="T364" id="Seg_10431" s="T363">n</ta>
            <ta e="T365" id="Seg_10432" s="T364">cardnum</ta>
            <ta e="T366" id="Seg_10433" s="T365">n</ta>
            <ta e="T367" id="Seg_10434" s="T366">v</ta>
            <ta e="T368" id="Seg_10435" s="T367">dempro</ta>
            <ta e="T369" id="Seg_10436" s="T368">v</ta>
            <ta e="T370" id="Seg_10437" s="T369">cardnum</ta>
            <ta e="T371" id="Seg_10438" s="T370">n</ta>
            <ta e="T372" id="Seg_10439" s="T371">n</ta>
            <ta e="T373" id="Seg_10440" s="T372">n</ta>
            <ta e="T374" id="Seg_10441" s="T373">v</ta>
            <ta e="T375" id="Seg_10442" s="T374">ptcl</ta>
            <ta e="T376" id="Seg_10443" s="T375">adj</ta>
            <ta e="T377" id="Seg_10444" s="T376">ptcl</ta>
            <ta e="T378" id="Seg_10445" s="T377">cardnum</ta>
            <ta e="T379" id="Seg_10446" s="T378">adj</ta>
            <ta e="T380" id="Seg_10447" s="T379">n</ta>
            <ta e="T381" id="Seg_10448" s="T380">n</ta>
            <ta e="T382" id="Seg_10449" s="T381">v</ta>
            <ta e="T383" id="Seg_10450" s="T382">v</ta>
            <ta e="T384" id="Seg_10451" s="T383">adj</ta>
            <ta e="T385" id="Seg_10452" s="T384">post</ta>
            <ta e="T386" id="Seg_10453" s="T385">n</ta>
            <ta e="T387" id="Seg_10454" s="T386">n</ta>
            <ta e="T388" id="Seg_10455" s="T387">v</ta>
            <ta e="T389" id="Seg_10456" s="T388">n</ta>
            <ta e="T390" id="Seg_10457" s="T389">n</ta>
            <ta e="T391" id="Seg_10458" s="T390">v</ta>
            <ta e="T392" id="Seg_10459" s="T391">aux</ta>
            <ta e="T393" id="Seg_10460" s="T392">dempro</ta>
            <ta e="T394" id="Seg_10461" s="T393">cardnum</ta>
            <ta e="T395" id="Seg_10462" s="T394">n</ta>
            <ta e="T396" id="Seg_10463" s="T395">v</ta>
            <ta e="T397" id="Seg_10464" s="T396">adj</ta>
            <ta e="T398" id="Seg_10465" s="T397">cardnum</ta>
            <ta e="T399" id="Seg_10466" s="T398">n</ta>
            <ta e="T400" id="Seg_10467" s="T399">adj</ta>
            <ta e="T401" id="Seg_10468" s="T400">interj</ta>
            <ta e="T402" id="Seg_10469" s="T401">conj</ta>
            <ta e="T403" id="Seg_10470" s="T402">n</ta>
            <ta e="T404" id="Seg_10471" s="T403">v</ta>
            <ta e="T405" id="Seg_10472" s="T404">n</ta>
            <ta e="T406" id="Seg_10473" s="T405">v</ta>
            <ta e="T407" id="Seg_10474" s="T406">pers</ta>
            <ta e="T408" id="Seg_10475" s="T407">v</ta>
            <ta e="T409" id="Seg_10476" s="T408">pers</ta>
            <ta e="T410" id="Seg_10477" s="T409">n</ta>
            <ta e="T411" id="Seg_10478" s="T410">n</ta>
            <ta e="T412" id="Seg_10479" s="T411">v</ta>
            <ta e="T413" id="Seg_10480" s="T412">v</ta>
            <ta e="T414" id="Seg_10481" s="T413">v</ta>
            <ta e="T415" id="Seg_10482" s="T414">n</ta>
            <ta e="T416" id="Seg_10483" s="T415">propr</ta>
            <ta e="T417" id="Seg_10484" s="T416">dempro</ta>
            <ta e="T418" id="Seg_10485" s="T417">dempro</ta>
            <ta e="T419" id="Seg_10486" s="T418">adj</ta>
            <ta e="T420" id="Seg_10487" s="T419">n</ta>
            <ta e="T421" id="Seg_10488" s="T420">v</ta>
            <ta e="T422" id="Seg_10489" s="T421">n</ta>
            <ta e="T423" id="Seg_10490" s="T422">ptcl</ta>
            <ta e="T424" id="Seg_10491" s="T423">ptcl</ta>
            <ta e="T425" id="Seg_10492" s="T424">ptcl</ta>
            <ta e="T426" id="Seg_10493" s="T425">dempro</ta>
            <ta e="T427" id="Seg_10494" s="T426">pers</ta>
            <ta e="T428" id="Seg_10495" s="T427">n</ta>
            <ta e="T429" id="Seg_10496" s="T428">v</ta>
            <ta e="T430" id="Seg_10497" s="T429">v</ta>
            <ta e="T431" id="Seg_10498" s="T430">n</ta>
            <ta e="T432" id="Seg_10499" s="T431">v</ta>
            <ta e="T433" id="Seg_10500" s="T432">ptcl</ta>
            <ta e="T434" id="Seg_10501" s="T433">adv</ta>
            <ta e="T435" id="Seg_10502" s="T434">pers</ta>
            <ta e="T436" id="Seg_10503" s="T435">v</ta>
            <ta e="T437" id="Seg_10504" s="T436">pers</ta>
            <ta e="T438" id="Seg_10505" s="T437">v</ta>
            <ta e="T439" id="Seg_10506" s="T438">pers</ta>
            <ta e="T440" id="Seg_10507" s="T439">n</ta>
            <ta e="T441" id="Seg_10508" s="T440">v</ta>
            <ta e="T442" id="Seg_10509" s="T441">dempro</ta>
            <ta e="T443" id="Seg_10510" s="T442">n</ta>
            <ta e="T444" id="Seg_10511" s="T443">n</ta>
            <ta e="T445" id="Seg_10512" s="T444">v</ta>
            <ta e="T446" id="Seg_10513" s="T445">adv</ta>
            <ta e="T447" id="Seg_10514" s="T446">n</ta>
            <ta e="T448" id="Seg_10515" s="T447">n</ta>
            <ta e="T449" id="Seg_10516" s="T448">v</ta>
            <ta e="T450" id="Seg_10517" s="T449">n</ta>
            <ta e="T451" id="Seg_10518" s="T450">v</ta>
            <ta e="T452" id="Seg_10519" s="T451">v</ta>
            <ta e="T453" id="Seg_10520" s="T452">dempro</ta>
            <ta e="T454" id="Seg_10521" s="T453">distrnum</ta>
            <ta e="T455" id="Seg_10522" s="T454">adj</ta>
            <ta e="T456" id="Seg_10523" s="T455">adj</ta>
            <ta e="T457" id="Seg_10524" s="T456">cardnum</ta>
            <ta e="T458" id="Seg_10525" s="T457">n</ta>
            <ta e="T459" id="Seg_10526" s="T458">v</ta>
            <ta e="T460" id="Seg_10527" s="T459">dempro</ta>
            <ta e="T461" id="Seg_10528" s="T460">pers</ta>
            <ta e="T462" id="Seg_10529" s="T461">v</ta>
            <ta e="T463" id="Seg_10530" s="T462">v</ta>
            <ta e="T464" id="Seg_10531" s="T463">adj</ta>
            <ta e="T465" id="Seg_10532" s="T464">n</ta>
            <ta e="T466" id="Seg_10533" s="T465">n</ta>
            <ta e="T467" id="Seg_10534" s="T466">n</ta>
            <ta e="T468" id="Seg_10535" s="T467">v</ta>
            <ta e="T469" id="Seg_10536" s="T468">conj</ta>
            <ta e="T470" id="Seg_10537" s="T469">v</ta>
            <ta e="T471" id="Seg_10538" s="T470">dempro</ta>
            <ta e="T472" id="Seg_10539" s="T471">cardnum</ta>
            <ta e="T473" id="Seg_10540" s="T472">n</ta>
            <ta e="T474" id="Seg_10541" s="T473">cardnum</ta>
            <ta e="T475" id="Seg_10542" s="T474">ptcl</ta>
            <ta e="T476" id="Seg_10543" s="T475">v</ta>
            <ta e="T477" id="Seg_10544" s="T476">v</ta>
            <ta e="T478" id="Seg_10545" s="T477">n</ta>
            <ta e="T479" id="Seg_10546" s="T478">adv</ta>
            <ta e="T480" id="Seg_10547" s="T479">v</ta>
            <ta e="T481" id="Seg_10548" s="T480">dempro</ta>
            <ta e="T482" id="Seg_10549" s="T481">n</ta>
            <ta e="T483" id="Seg_10550" s="T482">v</ta>
            <ta e="T484" id="Seg_10551" s="T483">v</ta>
            <ta e="T485" id="Seg_10552" s="T484">ptcl</ta>
            <ta e="T486" id="Seg_10553" s="T485">n</ta>
            <ta e="T487" id="Seg_10554" s="T486">propr</ta>
            <ta e="T488" id="Seg_10555" s="T487">n</ta>
            <ta e="T489" id="Seg_10556" s="T488">n</ta>
            <ta e="T490" id="Seg_10557" s="T489">n</ta>
            <ta e="T491" id="Seg_10558" s="T490">v</ta>
            <ta e="T492" id="Seg_10559" s="T491">emphpro</ta>
            <ta e="T493" id="Seg_10560" s="T492">emphpro</ta>
            <ta e="T494" id="Seg_10561" s="T493">n</ta>
            <ta e="T495" id="Seg_10562" s="T494">cop</ta>
            <ta e="T496" id="Seg_10563" s="T495">aux</ta>
            <ta e="T497" id="Seg_10564" s="T496">v</ta>
            <ta e="T498" id="Seg_10565" s="T497">post</ta>
            <ta e="T499" id="Seg_10566" s="T498">propr</ta>
            <ta e="T500" id="Seg_10567" s="T499">v</ta>
            <ta e="T501" id="Seg_10568" s="T500">n</ta>
            <ta e="T502" id="Seg_10569" s="T501">v</ta>
            <ta e="T503" id="Seg_10570" s="T502">n</ta>
            <ta e="T504" id="Seg_10571" s="T503">propr</ta>
            <ta e="T505" id="Seg_10572" s="T504">v</ta>
            <ta e="T506" id="Seg_10573" s="T505">v</ta>
            <ta e="T507" id="Seg_10574" s="T506">cardnum</ta>
            <ta e="T508" id="Seg_10575" s="T507">n</ta>
            <ta e="T509" id="Seg_10576" s="T508">v</ta>
            <ta e="T510" id="Seg_10577" s="T509">aux</ta>
            <ta e="T511" id="Seg_10578" s="T510">adj</ta>
            <ta e="T512" id="Seg_10579" s="T511">n</ta>
            <ta e="T513" id="Seg_10580" s="T512">v</ta>
            <ta e="T514" id="Seg_10581" s="T513">n</ta>
            <ta e="T515" id="Seg_10582" s="T514">conj</ta>
            <ta e="T516" id="Seg_10583" s="T515">n</ta>
            <ta e="T517" id="Seg_10584" s="T516">conj</ta>
            <ta e="T518" id="Seg_10585" s="T517">v</ta>
            <ta e="T519" id="Seg_10586" s="T518">v</ta>
            <ta e="T520" id="Seg_10587" s="T519">ptcl</ta>
            <ta e="T521" id="Seg_10588" s="T520">adv</ta>
            <ta e="T522" id="Seg_10589" s="T521">ptcl</ta>
            <ta e="T523" id="Seg_10590" s="T522">adv</ta>
            <ta e="T524" id="Seg_10591" s="T523">cardnum</ta>
            <ta e="T525" id="Seg_10592" s="T524">n</ta>
            <ta e="T526" id="Seg_10593" s="T525">v</ta>
            <ta e="T527" id="Seg_10594" s="T526">v</ta>
            <ta e="T528" id="Seg_10595" s="T527">n</ta>
            <ta e="T529" id="Seg_10596" s="T528">n</ta>
            <ta e="T530" id="Seg_10597" s="T529">v</ta>
            <ta e="T531" id="Seg_10598" s="T530">ptcl</ta>
            <ta e="T532" id="Seg_10599" s="T531">propr</ta>
            <ta e="T533" id="Seg_10600" s="T532">v</ta>
            <ta e="T534" id="Seg_10601" s="T533">n</ta>
            <ta e="T535" id="Seg_10602" s="T534">ptcl</ta>
            <ta e="T536" id="Seg_10603" s="T535">dempro</ta>
            <ta e="T537" id="Seg_10604" s="T536">cardnum</ta>
            <ta e="T538" id="Seg_10605" s="T537">n</ta>
            <ta e="T539" id="Seg_10606" s="T538">adj</ta>
            <ta e="T540" id="Seg_10607" s="T539">n</ta>
            <ta e="T541" id="Seg_10608" s="T540">cardnum</ta>
            <ta e="T542" id="Seg_10609" s="T541">adj</ta>
            <ta e="T543" id="Seg_10610" s="T542">n</ta>
            <ta e="T544" id="Seg_10611" s="T543">v</ta>
            <ta e="T545" id="Seg_10612" s="T544">aux</ta>
            <ta e="T546" id="Seg_10613" s="T545">adj</ta>
            <ta e="T547" id="Seg_10614" s="T546">n</ta>
            <ta e="T548" id="Seg_10615" s="T547">cardnum</ta>
            <ta e="T549" id="Seg_10616" s="T548">adj</ta>
            <ta e="T550" id="Seg_10617" s="T549">n</ta>
            <ta e="T551" id="Seg_10618" s="T550">v</ta>
            <ta e="T552" id="Seg_10619" s="T551">ptcl</ta>
            <ta e="T553" id="Seg_10620" s="T552">pers</ta>
            <ta e="T554" id="Seg_10621" s="T553">dempro</ta>
            <ta e="T555" id="Seg_10622" s="T554">cardnum</ta>
            <ta e="T556" id="Seg_10623" s="T555">adj</ta>
            <ta e="T557" id="Seg_10624" s="T556">n</ta>
            <ta e="T558" id="Seg_10625" s="T557">v</ta>
            <ta e="T559" id="Seg_10626" s="T558">n</ta>
            <ta e="T560" id="Seg_10627" s="T559">v</ta>
            <ta e="T561" id="Seg_10628" s="T560">pers</ta>
            <ta e="T562" id="Seg_10629" s="T561">cardnum</ta>
            <ta e="T563" id="Seg_10630" s="T562">n</ta>
            <ta e="T564" id="Seg_10631" s="T563">v</ta>
            <ta e="T565" id="Seg_10632" s="T564">v</ta>
            <ta e="T566" id="Seg_10633" s="T565">n</ta>
            <ta e="T567" id="Seg_10634" s="T566">v</ta>
            <ta e="T568" id="Seg_10635" s="T567">dempro</ta>
            <ta e="T569" id="Seg_10636" s="T568">n</ta>
            <ta e="T570" id="Seg_10637" s="T569">n</ta>
            <ta e="T571" id="Seg_10638" s="T570">adj</ta>
            <ta e="T572" id="Seg_10639" s="T571">n</ta>
            <ta e="T573" id="Seg_10640" s="T572">v</ta>
            <ta e="T574" id="Seg_10641" s="T573">n</ta>
            <ta e="T575" id="Seg_10642" s="T574">adv</ta>
            <ta e="T576" id="Seg_10643" s="T575">dempro</ta>
            <ta e="T577" id="Seg_10644" s="T576">v</ta>
            <ta e="T578" id="Seg_10645" s="T577">cardnum</ta>
            <ta e="T579" id="Seg_10646" s="T578">n</ta>
            <ta e="T580" id="Seg_10647" s="T579">n</ta>
            <ta e="T581" id="Seg_10648" s="T580">n</ta>
            <ta e="T582" id="Seg_10649" s="T581">v</ta>
            <ta e="T583" id="Seg_10650" s="T582">dempro</ta>
            <ta e="T584" id="Seg_10651" s="T583">n</ta>
            <ta e="T585" id="Seg_10652" s="T584">v</ta>
            <ta e="T586" id="Seg_10653" s="T585">n</ta>
            <ta e="T587" id="Seg_10654" s="T586">cardnum</ta>
            <ta e="T588" id="Seg_10655" s="T587">adj</ta>
            <ta e="T589" id="Seg_10656" s="T588">n</ta>
            <ta e="T590" id="Seg_10657" s="T589">v</ta>
            <ta e="T591" id="Seg_10658" s="T590">pers</ta>
            <ta e="T592" id="Seg_10659" s="T591">dempro</ta>
            <ta e="T593" id="Seg_10660" s="T592">cardnum</ta>
            <ta e="T594" id="Seg_10661" s="T593">adj</ta>
            <ta e="T595" id="Seg_10662" s="T594">n</ta>
            <ta e="T596" id="Seg_10663" s="T595">v</ta>
            <ta e="T597" id="Seg_10664" s="T596">ptcl</ta>
            <ta e="T598" id="Seg_10665" s="T597">adj</ta>
            <ta e="T599" id="Seg_10666" s="T598">n</ta>
            <ta e="T600" id="Seg_10667" s="T599">adj</ta>
            <ta e="T601" id="Seg_10668" s="T600">n</ta>
            <ta e="T602" id="Seg_10669" s="T601">v</ta>
            <ta e="T603" id="Seg_10670" s="T602">pers</ta>
            <ta e="T604" id="Seg_10671" s="T603">ptcl</ta>
            <ta e="T605" id="Seg_10672" s="T604">v</ta>
            <ta e="T606" id="Seg_10673" s="T605">adv</ta>
            <ta e="T607" id="Seg_10674" s="T606">adv</ta>
            <ta e="T608" id="Seg_10675" s="T607">pers</ta>
            <ta e="T609" id="Seg_10676" s="T608">v</ta>
            <ta e="T610" id="Seg_10677" s="T609">pers</ta>
            <ta e="T611" id="Seg_10678" s="T610">v</ta>
            <ta e="T612" id="Seg_10679" s="T611">v</ta>
            <ta e="T613" id="Seg_10680" s="T612">v</ta>
            <ta e="T614" id="Seg_10681" s="T613">pers</ta>
            <ta e="T615" id="Seg_10682" s="T614">v</ta>
            <ta e="T616" id="Seg_10683" s="T615">pers</ta>
            <ta e="T617" id="Seg_10684" s="T616">pers</ta>
            <ta e="T618" id="Seg_10685" s="T617">que</ta>
            <ta e="T619" id="Seg_10686" s="T618">ptcl</ta>
            <ta e="T620" id="Seg_10687" s="T619">v</ta>
            <ta e="T621" id="Seg_10688" s="T620">v</ta>
            <ta e="T622" id="Seg_10689" s="T621">pers</ta>
            <ta e="T623" id="Seg_10690" s="T622">que</ta>
            <ta e="T624" id="Seg_10691" s="T623">v</ta>
            <ta e="T625" id="Seg_10692" s="T624">conj</ta>
            <ta e="T626" id="Seg_10693" s="T625">v</ta>
            <ta e="T627" id="Seg_10694" s="T626">pers</ta>
            <ta e="T628" id="Seg_10695" s="T627">v</ta>
            <ta e="T629" id="Seg_10696" s="T628">v</ta>
            <ta e="T630" id="Seg_10697" s="T629">v</ta>
            <ta e="T631" id="Seg_10698" s="T630">n</ta>
            <ta e="T632" id="Seg_10699" s="T631">que</ta>
            <ta e="T633" id="Seg_10700" s="T632">ptcl</ta>
            <ta e="T634" id="Seg_10701" s="T633">cop</ta>
            <ta e="T635" id="Seg_10702" s="T634">n</ta>
            <ta e="T636" id="Seg_10703" s="T635">n</ta>
            <ta e="T637" id="Seg_10704" s="T636">n</ta>
            <ta e="T638" id="Seg_10705" s="T637">v</ta>
            <ta e="T639" id="Seg_10706" s="T638">adj</ta>
            <ta e="T640" id="Seg_10707" s="T639">n</ta>
            <ta e="T641" id="Seg_10708" s="T640">n</ta>
            <ta e="T642" id="Seg_10709" s="T641">v</ta>
            <ta e="T643" id="Seg_10710" s="T642">adv</ta>
            <ta e="T644" id="Seg_10711" s="T643">interj</ta>
            <ta e="T645" id="Seg_10712" s="T644">ptcl</ta>
            <ta e="T646" id="Seg_10713" s="T645">v</ta>
            <ta e="T647" id="Seg_10714" s="T646">propr</ta>
            <ta e="T648" id="Seg_10715" s="T647">n</ta>
            <ta e="T649" id="Seg_10716" s="T648">cardnum</ta>
            <ta e="T650" id="Seg_10717" s="T649">adj</ta>
            <ta e="T651" id="Seg_10718" s="T650">n</ta>
            <ta e="T652" id="Seg_10719" s="T651">v</ta>
            <ta e="T653" id="Seg_10720" s="T652">propr</ta>
            <ta e="T654" id="Seg_10721" s="T653">v</ta>
            <ta e="T655" id="Seg_10722" s="T654">dempro</ta>
            <ta e="T656" id="Seg_10723" s="T655">n</ta>
            <ta e="T657" id="Seg_10724" s="T656">v</ta>
            <ta e="T658" id="Seg_10725" s="T657">v</ta>
            <ta e="T659" id="Seg_10726" s="T658">adj</ta>
            <ta e="T660" id="Seg_10727" s="T659">n</ta>
            <ta e="T661" id="Seg_10728" s="T660">adj</ta>
            <ta e="T662" id="Seg_10729" s="T661">adj</ta>
            <ta e="T663" id="Seg_10730" s="T662">n</ta>
            <ta e="T664" id="Seg_10731" s="T663">cop</ta>
            <ta e="T665" id="Seg_10732" s="T664">interj</ta>
            <ta e="T666" id="Seg_10733" s="T665">v</ta>
            <ta e="T667" id="Seg_10734" s="T666">v</ta>
            <ta e="T668" id="Seg_10735" s="T667">aux</ta>
            <ta e="T669" id="Seg_10736" s="T668">v</ta>
            <ta e="T670" id="Seg_10737" s="T669">v</ta>
            <ta e="T671" id="Seg_10738" s="T670">v</ta>
            <ta e="T672" id="Seg_10739" s="T671">n</ta>
            <ta e="T673" id="Seg_10740" s="T672">n</ta>
            <ta e="T674" id="Seg_10741" s="T673">interj</ta>
            <ta e="T675" id="Seg_10742" s="T674">v</ta>
            <ta e="T676" id="Seg_10743" s="T675">adv</ta>
            <ta e="T677" id="Seg_10744" s="T676">n</ta>
            <ta e="T678" id="Seg_10745" s="T677">n</ta>
            <ta e="T679" id="Seg_10746" s="T678">post</ta>
            <ta e="T680" id="Seg_10747" s="T679">v</ta>
            <ta e="T681" id="Seg_10748" s="T680">v</ta>
            <ta e="T682" id="Seg_10749" s="T681">ptcl</ta>
            <ta e="T683" id="Seg_10750" s="T682">adv</ta>
            <ta e="T684" id="Seg_10751" s="T683">n</ta>
            <ta e="T685" id="Seg_10752" s="T684">n</ta>
            <ta e="T686" id="Seg_10753" s="T685">interj</ta>
            <ta e="T687" id="Seg_10754" s="T686">v</ta>
            <ta e="T688" id="Seg_10755" s="T687">n</ta>
            <ta e="T689" id="Seg_10756" s="T688">n</ta>
            <ta e="T690" id="Seg_10757" s="T689">adv</ta>
            <ta e="T691" id="Seg_10758" s="T690">n</ta>
            <ta e="T692" id="Seg_10759" s="T691">v</ta>
            <ta e="T693" id="Seg_10760" s="T692">adj</ta>
            <ta e="T694" id="Seg_10761" s="T693">n</ta>
            <ta e="T695" id="Seg_10762" s="T694">cop</ta>
            <ta e="T696" id="Seg_10763" s="T695">v</ta>
            <ta e="T697" id="Seg_10764" s="T696">dempro</ta>
            <ta e="T698" id="Seg_10765" s="T697">n</ta>
            <ta e="T699" id="Seg_10766" s="T698">propr</ta>
            <ta e="T700" id="Seg_10767" s="T699">n</ta>
            <ta e="T701" id="Seg_10768" s="T700">cop</ta>
            <ta e="T702" id="Seg_10769" s="T701">conj</ta>
            <ta e="T703" id="Seg_10770" s="T702">n</ta>
            <ta e="T704" id="Seg_10771" s="T703">dempro</ta>
            <ta e="T705" id="Seg_10772" s="T704">adj</ta>
            <ta e="T706" id="Seg_10773" s="T705">n</ta>
            <ta e="T707" id="Seg_10774" s="T706">post</ta>
            <ta e="T708" id="Seg_10775" s="T707">v</ta>
            <ta e="T709" id="Seg_10776" s="T708">v</ta>
            <ta e="T710" id="Seg_10777" s="T709">v</ta>
            <ta e="T711" id="Seg_10778" s="T710">conj</ta>
            <ta e="T712" id="Seg_10779" s="T711">dempro</ta>
            <ta e="T713" id="Seg_10780" s="T712">v</ta>
            <ta e="T714" id="Seg_10781" s="T713">v</ta>
            <ta e="T715" id="Seg_10782" s="T714">n</ta>
            <ta e="T716" id="Seg_10783" s="T715">n</ta>
            <ta e="T717" id="Seg_10784" s="T716">v</ta>
            <ta e="T718" id="Seg_10785" s="T717">v</ta>
            <ta e="T719" id="Seg_10786" s="T718">v</ta>
            <ta e="T720" id="Seg_10787" s="T719">n</ta>
            <ta e="T721" id="Seg_10788" s="T720">v</ta>
            <ta e="T722" id="Seg_10789" s="T721">n</ta>
            <ta e="T723" id="Seg_10790" s="T722">n</ta>
            <ta e="T724" id="Seg_10791" s="T723">ptcl</ta>
            <ta e="T725" id="Seg_10792" s="T724">v</ta>
            <ta e="T726" id="Seg_10793" s="T725">aux</ta>
            <ta e="T727" id="Seg_10794" s="T726">adj</ta>
            <ta e="T728" id="Seg_10795" s="T727">n</ta>
            <ta e="T729" id="Seg_10796" s="T728">adv</ta>
            <ta e="T730" id="Seg_10797" s="T729">n</ta>
            <ta e="T731" id="Seg_10798" s="T730">n</ta>
            <ta e="T732" id="Seg_10799" s="T731">v</ta>
            <ta e="T733" id="Seg_10800" s="T732">aux</ta>
            <ta e="T734" id="Seg_10801" s="T733">propr</ta>
            <ta e="T735" id="Seg_10802" s="T734">v</ta>
            <ta e="T736" id="Seg_10803" s="T735">v</ta>
            <ta e="T737" id="Seg_10804" s="T736">v</ta>
            <ta e="T738" id="Seg_10805" s="T737">v</ta>
            <ta e="T739" id="Seg_10806" s="T738">conj</ta>
            <ta e="T740" id="Seg_10807" s="T739">n</ta>
            <ta e="T741" id="Seg_10808" s="T740">v</ta>
            <ta e="T742" id="Seg_10809" s="T741">v</ta>
            <ta e="T743" id="Seg_10810" s="T742">propr</ta>
            <ta e="T744" id="Seg_10811" s="T743">n</ta>
            <ta e="T745" id="Seg_10812" s="T744">v</ta>
            <ta e="T746" id="Seg_10813" s="T745">n</ta>
            <ta e="T747" id="Seg_10814" s="T746">v</ta>
            <ta e="T748" id="Seg_10815" s="T747">n</ta>
            <ta e="T749" id="Seg_10816" s="T748">v</ta>
            <ta e="T750" id="Seg_10817" s="T749">cardnum</ta>
            <ta e="T751" id="Seg_10818" s="T750">adj</ta>
            <ta e="T752" id="Seg_10819" s="T751">n</ta>
            <ta e="T753" id="Seg_10820" s="T752">v</ta>
            <ta e="T754" id="Seg_10821" s="T753">aux</ta>
            <ta e="T755" id="Seg_10822" s="T754">propr</ta>
            <ta e="T756" id="Seg_10823" s="T755">interj</ta>
            <ta e="T757" id="Seg_10824" s="T756">v</ta>
            <ta e="T758" id="Seg_10825" s="T757">n</ta>
            <ta e="T759" id="Seg_10826" s="T758">cop</ta>
            <ta e="T760" id="Seg_10827" s="T759">n</ta>
            <ta e="T761" id="Seg_10828" s="T760">v</ta>
            <ta e="T762" id="Seg_10829" s="T761">n</ta>
            <ta e="T763" id="Seg_10830" s="T762">n</ta>
            <ta e="T764" id="Seg_10831" s="T763">interj</ta>
            <ta e="T765" id="Seg_10832" s="T764">v</ta>
            <ta e="T766" id="Seg_10833" s="T765">n</ta>
            <ta e="T767" id="Seg_10834" s="T766">post</ta>
            <ta e="T768" id="Seg_10835" s="T767">v</ta>
            <ta e="T769" id="Seg_10836" s="T768">n</ta>
            <ta e="T770" id="Seg_10837" s="T769">n</ta>
            <ta e="T771" id="Seg_10838" s="T770">interj</ta>
            <ta e="T772" id="Seg_10839" s="T771">v</ta>
            <ta e="T773" id="Seg_10840" s="T772">n</ta>
            <ta e="T774" id="Seg_10841" s="T773">n</ta>
            <ta e="T775" id="Seg_10842" s="T774">post</ta>
            <ta e="T776" id="Seg_10843" s="T775">que</ta>
            <ta e="T777" id="Seg_10844" s="T776">ptcl</ta>
            <ta e="T778" id="Seg_10845" s="T777">adj</ta>
            <ta e="T779" id="Seg_10846" s="T778">adj</ta>
            <ta e="T780" id="Seg_10847" s="T779">n</ta>
            <ta e="T781" id="Seg_10848" s="T780">cop</ta>
            <ta e="T782" id="Seg_10849" s="T781">v</ta>
            <ta e="T783" id="Seg_10850" s="T782">v</ta>
            <ta e="T784" id="Seg_10851" s="T783">n</ta>
            <ta e="T785" id="Seg_10852" s="T784">adj</ta>
            <ta e="T786" id="Seg_10853" s="T785">n</ta>
            <ta e="T787" id="Seg_10854" s="T786">v</ta>
            <ta e="T788" id="Seg_10855" s="T787">conj</ta>
            <ta e="T789" id="Seg_10856" s="T788">n</ta>
            <ta e="T790" id="Seg_10857" s="T789">n</ta>
            <ta e="T791" id="Seg_10858" s="T790">adv</ta>
            <ta e="T792" id="Seg_10859" s="T791">v</ta>
            <ta e="T793" id="Seg_10860" s="T792">v</ta>
            <ta e="T794" id="Seg_10861" s="T793">aux</ta>
            <ta e="T795" id="Seg_10862" s="T794">n</ta>
            <ta e="T796" id="Seg_10863" s="T795">v</ta>
            <ta e="T797" id="Seg_10864" s="T796">aux</ta>
            <ta e="T798" id="Seg_10865" s="T797">v</ta>
            <ta e="T799" id="Seg_10866" s="T798">ptcl</ta>
            <ta e="T800" id="Seg_10867" s="T799">aux</ta>
            <ta e="T801" id="Seg_10868" s="T800">n</ta>
            <ta e="T802" id="Seg_10869" s="T801">que</ta>
            <ta e="T803" id="Seg_10870" s="T802">ptcl</ta>
            <ta e="T804" id="Seg_10871" s="T803">v</ta>
            <ta e="T805" id="Seg_10872" s="T804">v</ta>
            <ta e="T806" id="Seg_10873" s="T805">n</ta>
            <ta e="T807" id="Seg_10874" s="T806">n</ta>
            <ta e="T808" id="Seg_10875" s="T807">v</ta>
            <ta e="T809" id="Seg_10876" s="T808">dempro</ta>
            <ta e="T810" id="Seg_10877" s="T809">n</ta>
            <ta e="T811" id="Seg_10878" s="T810">propr</ta>
            <ta e="T812" id="Seg_10879" s="T811">n</ta>
            <ta e="T813" id="Seg_10880" s="T812">v</ta>
            <ta e="T814" id="Seg_10881" s="T813">n</ta>
            <ta e="T815" id="Seg_10882" s="T814">v</ta>
            <ta e="T816" id="Seg_10883" s="T815">ptcl</ta>
            <ta e="T817" id="Seg_10884" s="T816">propr</ta>
            <ta e="T818" id="Seg_10885" s="T817">n</ta>
            <ta e="T819" id="Seg_10886" s="T818">v</ta>
            <ta e="T820" id="Seg_10887" s="T819">n</ta>
            <ta e="T821" id="Seg_10888" s="T820">n</ta>
            <ta e="T822" id="Seg_10889" s="T821">v</ta>
            <ta e="T823" id="Seg_10890" s="T822">v</ta>
            <ta e="T824" id="Seg_10891" s="T823">v</ta>
            <ta e="T825" id="Seg_10892" s="T824">ptcl</ta>
            <ta e="T826" id="Seg_10893" s="T825">adj</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR" />
         <annotation name="SyF" tierref="SyF" />
         <annotation name="IST" tierref="IST" />
         <annotation name="Top" tierref="Top" />
         <annotation name="Foc" tierref="Foc" />
         <annotation name="BOR" tierref="BOR" />
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T8" id="Seg_10894" s="T0">Once there lived two young men, which were called Iron Cap and Bony Belt.</ta>
            <ta e="T11" id="Seg_10895" s="T8">Both of them had a father and a mother.</ta>
            <ta e="T17" id="Seg_10896" s="T11">So they lived and Bony Belt said to Iron Cap: </ta>
            <ta e="T23" id="Seg_10897" s="T17">"My friend, what are we able to do?</ta>
            <ta e="T27" id="Seg_10898" s="T23">Why don't we check our skills?"</ta>
            <ta e="T31" id="Seg_10899" s="T27">Apparently he wanted to compete.</ta>
            <ta e="T40" id="Seg_10900" s="T31">His friend: "What do we have to compete for?", he said.</ta>
            <ta e="T42" id="Seg_10901" s="T40">His friend didn't let him: </ta>
            <ta e="T44" id="Seg_10902" s="T42">"Why friend!</ta>
            <ta e="T48" id="Seg_10903" s="T44">I want to get to know what you are able to do!"</ta>
            <ta e="T50" id="Seg_10904" s="T48">His friend didn't agree.</ta>
            <ta e="T64" id="Seg_10905" s="T50">Bony Belt says: "I will escape from you, follow me; that what I turn into, turn into it, too".</ta>
            <ta e="T65" id="Seg_10906" s="T64">His friend: </ta>
            <ta e="T68" id="Seg_10907" s="T65">"I will follow you then, though."</ta>
            <ta e="T77" id="Seg_10908" s="T68">"In the morning, when the first people get up, follow me", Bony Belt says.</ta>
            <ta e="T82" id="Seg_10909" s="T77">So they agreed and went home separately.</ta>
            <ta e="T86" id="Seg_10910" s="T82">Iron Cap went and lay down to sleep.</ta>
            <ta e="T88" id="Seg_10911" s="T86">He woke up: </ta>
            <ta e="T91" id="Seg_10912" s="T88">"Is my friend there?"</ta>
            <ta e="T99" id="Seg_10913" s="T91">He come to his friend: The bed of Bony Belt is empty, he has left.</ta>
            <ta e="T106" id="Seg_10914" s="T99">Iron Cap searched for traces, he didn't find traces, no traces, at all.</ta>
            <ta e="T109" id="Seg_10915" s="T106">He goes in and scrabbles in the bed.</ta>
            <ta e="T120" id="Seg_10916" s="T109">Under the bed of Bony Belt the burrow of a shrew mouse opens, there are no other traces.</ta>
            <ta e="T126" id="Seg_10917" s="T120">Iron Cap turned into a mouse and went down the burrow.</ta>
            <ta e="T133" id="Seg_10918" s="T126">And that one, after he had went out of the burrow and had turned into an ermine, jumped off.</ta>
            <ta e="T138" id="Seg_10919" s="T133">Iron Cap also turned into an ermine and followed him.</ta>
            <ta e="T142" id="Seg_10920" s="T138">And he was running. </ta>
            <ta e="T153" id="Seg_10921" s="T142">Also the traces of the ermine ended and on the middle world traces of a polar fox continued. </ta>
            <ta e="T156" id="Seg_10922" s="T153">The friend was frightened.</ta>
            <ta e="T162" id="Seg_10923" s="T156">Iron Cap turned into a polar fox and jumped off.</ta>
            <ta e="T170" id="Seg_10924" s="T162">He came to a place, Bony Belt was jumping around as a hare.</ta>
            <ta e="T174" id="Seg_10925" s="T170">And that one turned into a hare and followed him.</ta>
            <ta e="T178" id="Seg_10926" s="T174">Bony Belt turned into a fox then.</ta>
            <ta e="T186" id="Seg_10927" s="T178">And that one, also having turned into a fox, imitated everything.</ta>
            <ta e="T191" id="Seg_10928" s="T186">That one got bigger and ran as a wolf.</ta>
            <ta e="T194" id="Seg_10929" s="T191">This one ran and wondered: </ta>
            <ta e="T199" id="Seg_10930" s="T194">"That one didn't get along and then asked for [competing]", he thinks.</ta>
            <ta e="T203" id="Seg_10931" s="T199">He turned into a wolf and followed him.</ta>
            <ta e="T206" id="Seg_10932" s="T203">How long he ran, one doesn't know.</ta>
            <ta e="T211" id="Seg_10933" s="T206">Bony Belt turned into a wolvering and went on trotting.</ta>
            <ta e="T219" id="Seg_10934" s="T211">That one, still wondering, turned into a wolverine and followed him.</ta>
            <ta e="T225" id="Seg_10935" s="T219">As the trace of the wolverine ended, this one turned into a bear and trotted on.</ta>
            <ta e="T230" id="Seg_10936" s="T225">He turned into a bear and Iron Cap followed him.</ta>
            <ta e="T235" id="Seg_10937" s="T230">All the time they were running.</ta>
            <ta e="T244" id="Seg_10938" s="T235">And the trace of the bear ended, that one turned into a reindeer bull and kept jumping on.</ta>
            <ta e="T250" id="Seg_10939" s="T244">He was wondering all the time and followed after his friend.</ta>
            <ta e="T254" id="Seg_10940" s="T250">Iron Cap turned into a wild reindeer bull.</ta>
            <ta e="T263" id="Seg_10941" s="T254">"You skills are at an end, apparently, I am still following", he thinks.</ta>
            <ta e="T269" id="Seg_10942" s="T263">He saw that his friend was standing at one place toddling.</ta>
            <ta e="T282" id="Seg_10943" s="T269">He came closer and saw, that suddenly two legs stood apart, cut off at the joints, and that the carcass rolled away the legs.</ta>
            <ta e="T291" id="Seg_10944" s="T282">And likewise Iron Cap cut off his legs and rolled on being a carcass.</ta>
            <ta e="T297" id="Seg_10945" s="T291">It was not long, the carcass piled up. </ta>
            <ta e="T301" id="Seg_10946" s="T297">He is happy, "I reached him, though", he says.</ta>
            <ta e="T312" id="Seg_10947" s="T301">He came up to that carcass, the carcass was just lying so, only the head was rolling on.</ta>
            <ta e="T320" id="Seg_10948" s="T312">"What's that, why does he cut up himself?", he thinks.</ta>
            <ta e="T326" id="Seg_10949" s="T320">"What a competition?!", he wondered and went on.</ta>
            <ta e="T330" id="Seg_10950" s="T326">He turned into a head and rolled on.</ta>
            <ta e="T334" id="Seg_10951" s="T330">A head is lying there.</ta>
            <ta e="T337" id="Seg_10952" s="T334">"Now I have reached him!"</ta>
            <ta e="T342" id="Seg_10953" s="T337">Suddenly two eyes rolled away.</ta>
            <ta e="T346" id="Seg_10954" s="T342">"What is that for a human?</ta>
            <ta e="T352" id="Seg_10955" s="T346">Do I follow a good spirit or a bad spirit?!", he says.</ta>
            <ta e="T358" id="Seg_10956" s="T352">The two eyes rolled along the trace of the [other] eyes.</ta>
            <ta e="T367" id="Seg_10957" s="T358">After some time the trace of those two eyes led to small house.</ta>
            <ta e="T374" id="Seg_10958" s="T367">Having come there the trace of the two eyes led into the house.</ta>
            <ta e="T377" id="Seg_10959" s="T374">"Well, now you have come to an end"</ta>
            <ta e="T382" id="Seg_10960" s="T377">And the two back eyes went into the house.</ta>
            <ta e="T392" id="Seg_10961" s="T382">They went in, on the right bed an old woman was sitting, the eyes were twinkling in the back part of the yurt.</ta>
            <ta e="T400" id="Seg_10962" s="T392">As the two eyes went in, the two front eyes said: </ta>
            <ta e="T406" id="Seg_10963" s="T400">"Oh, also the following human is tired.</ta>
            <ta e="T408" id="Seg_10964" s="T406">Relax.</ta>
            <ta e="T414" id="Seg_10965" s="T408">I'll get our carcasses and our heads", he says.</ta>
            <ta e="T417" id="Seg_10966" s="T414">His friend Iron Cap on that: </ta>
            <ta e="T425" id="Seg_10967" s="T417">"Are you out of your mind, or what?</ta>
            <ta e="T432" id="Seg_10968" s="T425">We came to a place, from where no human comes back!</ta>
            <ta e="T440" id="Seg_10969" s="T432">Now you will follow me, imitate everything I do..</ta>
            <ta e="T445" id="Seg_10970" s="T440">You know that we came to the master of snowstorms.</ta>
            <ta e="T452" id="Seg_10971" s="T445">In the morning, when the first people get up, I will also get up and go out.</ta>
            <ta e="T459" id="Seg_10972" s="T452">Two girls will come there, each of them with a caravan of a hundred of sledges.</ta>
            <ta e="T478" id="Seg_10973" s="T459">I'll run out, hang myself on to the shoulder strap of the front reindeer of the first girl and start running; if you hang yourself on to any of the hundred sledges, then you'll come to your land.</ta>
            <ta e="T484" id="Seg_10974" s="T478">If you don't hang yourself on, then you'll die in this world!", he says.</ta>
            <ta e="T491" id="Seg_10975" s="T484">There his friend, Bony Belt, brought the carcasses, legs and heads.</ta>
            <ta e="T496" id="Seg_10976" s="T491">They turned into themselves.</ta>
            <ta e="T503" id="Seg_10977" s="T496">After having slept, Bony Belt woke up from the noise, as his friend went out.</ta>
            <ta e="T513" id="Seg_10978" s="T503">Bony Belt ran out, as the hundred sledges passed by, he hung himself on to the last sledge.</ta>
            <ta e="T519" id="Seg_10979" s="T513">He didn't notice whether they were flying or driving.</ta>
            <ta e="T530" id="Seg_10980" s="T519">Later on, as the hundred sledges stopped, he saw that his friend has brought him into his land.</ta>
            <ta e="T534" id="Seg_10981" s="T530">Well, Iron Cap said to his friend: </ta>
            <ta e="T551" id="Seg_10982" s="T534">"So, on the one side of these hundred sledges three white reindeer bulls are tied up, on the other side three black reindeer bulls.</ta>
            <ta e="T566" id="Seg_10983" s="T551">Well, I'll kill the three white reindeer bulls and skin them, you'll kill the three black ones and skin them.</ta>
            <ta e="T574" id="Seg_10984" s="T566">Know, that I brought the youngest daughter of the master of snowstorms with my magic power.</ta>
            <ta e="T582" id="Seg_10985" s="T574">Following her, her three elder brothers, masters of snowstorms, will come.</ta>
            <ta e="T596" id="Seg_10986" s="T582">When those people are coming, I will spread out the three white furs in the back part of the house, you, though, spread out your black furs.</ta>
            <ta e="T605" id="Seg_10987" s="T596">Make a big kettle of water boiling, I will make it boiling likewise.</ta>
            <ta e="T609" id="Seg_10988" s="T605">At first they will be a guest of mine.</ta>
            <ta e="T615" id="Seg_10989" s="T609">When they leave me, being sated and content, they will be a guest of yours.</ta>
            <ta e="T621" id="Seg_10990" s="T615">I'll show them something, go with them there.</ta>
            <ta e="T631" id="Seg_10991" s="T621">What I show, you'll imitate and show them, too!", he said and taught his friend.</ta>
            <ta e="T641" id="Seg_10992" s="T631">It was not long, the masters of snowstorms were coming in the shape of whirlwinds, people of dense ice.</ta>
            <ta e="T646" id="Seg_10993" s="T641">As they went in, they didn't even say "hello".</ta>
            <ta e="T652" id="Seg_10994" s="T646">In the house of Iron Cap they sat down onto the three white furs.</ta>
            <ta e="T656" id="Seg_10995" s="T652">Iron Cap had to show those peoples his skills.</ta>
            <ta e="T681" id="Seg_10996" s="T656">He turned into a black-throated diver with a bony beak, shouting "aa-uu" he dived into the big kettle with boiling water, he bet the wings and flew out, he flew through the belly of his mother, then he flew out of the mouth of his mother.</ta>
            <ta e="T696" id="Seg_10997" s="T681">And then he flew through the belly of his father, he came out of the mouth of his father as a wild reindeer bull, who just had pulled off his antlers.</ta>
            <ta e="T709" id="Seg_10998" s="T696">After that Iron Cap turned into a human and shot with a bow through the wild reindeer bull.</ta>
            <ta e="T721" id="Seg_10999" s="T709">He killed him and skinned him, he threw the raw meat into a wooden bowl and served his guests the raw meat.</ta>
            <ta e="T726" id="Seg_11000" s="T721">The guests ate in silence.</ta>
            <ta e="T733" id="Seg_11001" s="T726">Of the autumnal reindeer bull they just left over the antlers and hooves.</ta>
            <ta e="T736" id="Seg_11002" s="T733">Bony Belt sat there and watched.</ta>
            <ta e="T742" id="Seg_11003" s="T736">They finished eating, wiped their lips down and went out.</ta>
            <ta e="T747" id="Seg_11004" s="T742">Bony belt jumped on his feet in order to receive the guests.</ta>
            <ta e="T754" id="Seg_11005" s="T747">The guests came in and sat down onto the three black furs.</ta>
            <ta e="T772" id="Seg_11006" s="T754">Bony Belt turned into a red-throated diver, shouting "Aa-uu" he dived into the big kettle, he swished into the belly of his mother, he flew out of her mouth, he swished into the belly of his father.</ta>
            <ta e="T783" id="Seg_11007" s="T772">From the mouth of his father he jumped out as an orphaned reindeer calf.</ta>
            <ta e="T794" id="Seg_11008" s="T783">After that a human with a bow came out and shot it in front of the guests, he started preparing it.</ta>
            <ta e="T797" id="Seg_11009" s="T794">He served it to the guests.</ta>
            <ta e="T800" id="Seg_11010" s="T797">He served it.</ta>
            <ta e="T805" id="Seg_11011" s="T800">They didn't notice where everything was going to.</ta>
            <ta e="T815" id="Seg_11012" s="T805">The masters of snowstorms, being offended, took this house, Bony Belt and his parents in the shape of whirlwinds along.</ta>
            <ta e="T819" id="Seg_11013" s="T815">Only Iron Cap and his parents remained.</ta>
            <ta e="T825" id="Seg_11014" s="T819">He married the daughter of the snowstorm and lived, it is said, rich and wealthy.</ta>
            <ta e="T826" id="Seg_11015" s="T825">The end.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T8" id="Seg_11016" s="T0">Es lebten einmal zwei junge Männer, die Eisenmütze und Knochengürtel genannt wurden.</ta>
            <ta e="T11" id="Seg_11017" s="T8">Beide hatten einen Vater, beide hatten eine Mutter.</ta>
            <ta e="T17" id="Seg_11018" s="T11">So lebten sie und Knochengürtel sagte zu Eisenmütze: </ta>
            <ta e="T23" id="Seg_11019" s="T17">"Mein Freund, wozu sind wir fähig?</ta>
            <ta e="T27" id="Seg_11020" s="T23">Warum finden wir unsere Fähigkeiten nicht heraus?"</ta>
            <ta e="T31" id="Seg_11021" s="T27">Er wollte sich offensichtlich messen.</ta>
            <ta e="T40" id="Seg_11022" s="T31">Sein Freund: "Was haben wir, um darum zu wetteifern?", sagte er.</ta>
            <ta e="T42" id="Seg_11023" s="T40">Der Freund ließ in nicht in Ruhe: </ta>
            <ta e="T44" id="Seg_11024" s="T42">"Warum, Freund!</ta>
            <ta e="T48" id="Seg_11025" s="T44">Ich möchte herausfinden, wozu du fähig bist!"</ta>
            <ta e="T50" id="Seg_11026" s="T48">Der Freund war nicht einverstanden.</ta>
            <ta e="T64" id="Seg_11027" s="T50">Knochengürtel sagt: "Ich laufe vor dir weg, folge mir, das was ich werde, wirst du auch".</ta>
            <ta e="T65" id="Seg_11028" s="T64">Der Freund: </ta>
            <ta e="T68" id="Seg_11029" s="T65">"Ich verfolge dich dann wohl."</ta>
            <ta e="T77" id="Seg_11030" s="T68">"Am Morgen, wenn die ersten Leute aufstehen, dann folge mir", sagt Knochengürtel.</ta>
            <ta e="T82" id="Seg_11031" s="T77">So vereinbarten sie es und gingen getrennter Wege nach Hause.</ta>
            <ta e="T86" id="Seg_11032" s="T82">Eisenmütze ging und legte sich schlafen.</ta>
            <ta e="T88" id="Seg_11033" s="T86">Er wachte auf: </ta>
            <ta e="T91" id="Seg_11034" s="T88">"Ist mein Freund wohl da?"</ta>
            <ta e="T99" id="Seg_11035" s="T91">Er kam zum Freund: Das Bett von Knochengürtel stand leer da, er ist schon gegangen.</ta>
            <ta e="T106" id="Seg_11036" s="T99">Eisenmütze suchte nach Spuren, er fand keine Spuren, überhaupt keine.</ta>
            <ta e="T109" id="Seg_11037" s="T106">Er geht hinein und wühlt im Bett.</ta>
            <ta e="T120" id="Seg_11038" s="T109">Unter dem Bett von Knochengürtel öffnet sich die Höhle einer Spitzmaus, andere Spuren gibt es nicht.</ta>
            <ta e="T126" id="Seg_11039" s="T120">Eisenmütze wurde zu einer Maus und ging hinunter in die Höhle.</ta>
            <ta e="T133" id="Seg_11040" s="T126">Und jener, nachdem er aus der Höhle hinausgegangen war und zu einem Hermelin geworden war, sprang davon.</ta>
            <ta e="T138" id="Seg_11041" s="T133">Eisenmütze wurde auch zu einem Hermelin und verfolgte ihn.</ta>
            <ta e="T142" id="Seg_11042" s="T138">Und er rannte nur so.</ta>
            <ta e="T153" id="Seg_11043" s="T142">Auch die Spuren des Hermelins endeten, und auf der mittleren Welt gingen die Spuren eines Polarfuchses weiter.</ta>
            <ta e="T156" id="Seg_11044" s="T153">Der Freund hatte Angst.</ta>
            <ta e="T162" id="Seg_11045" s="T156">Eisenmütze wurden zu einem Polarfuchs und sprang davon.</ta>
            <ta e="T170" id="Seg_11046" s="T162">Er kam an einen Ort, da sprang Knochengürtel als Hase herum.</ta>
            <ta e="T174" id="Seg_11047" s="T170">Und jener wurde zu einem Hasen und verfolgte ihn.</ta>
            <ta e="T178" id="Seg_11048" s="T174">Knochengürtel wurde dann zu einem Fuchs.</ta>
            <ta e="T186" id="Seg_11049" s="T178">Und jener, auch in einen Fuchs verwandelt, machte ihm alles nach.</ta>
            <ta e="T191" id="Seg_11050" s="T186">Jener wurde größer und lief als Wolf.</ta>
            <ta e="T194" id="Seg_11051" s="T191">Dieser lief und wunderte sich: </ta>
            <ta e="T199" id="Seg_11052" s="T194">"Jener kam nicht zurecht und bat dann wohl [um das Kräftemessen]!", denkt er.</ta>
            <ta e="T203" id="Seg_11053" s="T199">Er wurde ein Wolf und verfolgte ihn.</ta>
            <ta e="T206" id="Seg_11054" s="T203">Wie lange er lief, weiß man nicht.</ta>
            <ta e="T211" id="Seg_11055" s="T206">Knochengürtel verwandelte sich in ein Vielfraß und lief im Trab weiter.</ta>
            <ta e="T219" id="Seg_11056" s="T211">Jener, sich immer noch wundernd, wurde zu einem Vielfraß und folgte ihm.</ta>
            <ta e="T225" id="Seg_11057" s="T219">Als gerade die Spur des Vielfraßes aufhörte, verwandelte sich dieser in einen Bär und trabte weiter.</ta>
            <ta e="T230" id="Seg_11058" s="T225">Zu einem Bären geworden, folgte Eisenmütze ihm.</ta>
            <ta e="T235" id="Seg_11059" s="T230">Die ganze Zeit liefen sie nur.</ta>
            <ta e="T244" id="Seg_11060" s="T235">Und die Spur des Bären ging zuende, da war jener ein Rentierbulle geworden und sprang immer weiter.</ta>
            <ta e="T250" id="Seg_11061" s="T244">Er wunderte sich die ganze Zeit und folgte immer seinem Freund.</ta>
            <ta e="T254" id="Seg_11062" s="T250">Eisenmütze wurde zu einem wilden Rentierbullen.</ta>
            <ta e="T263" id="Seg_11063" s="T254">"Deine Fähigkeit ist wohl am Ende, ich verfolge dich wohl noch", denkt er.</ta>
            <ta e="T269" id="Seg_11064" s="T263">Er sah, dass sein Freund an einem Ort stand und schwankte.</ta>
            <ta e="T282" id="Seg_11065" s="T269">Er ging näher heran und sah, dass plötzlich zwei Beine abstanden, an den Gelenken abgeschnitten, und dass das Gerippe die Beine wegrollte.</ta>
            <ta e="T291" id="Seg_11066" s="T282">Und genauso schnitt auch Eisenmütze seine Beine ab und rollte als Gerippe.</ta>
            <ta e="T297" id="Seg_11067" s="T291">Es war nicht lange, da türmte sich das Gerippe auf.</ta>
            <ta e="T301" id="Seg_11068" s="T297">Er freute sich, "ich habe ihn erreicht!", sagt er.</ta>
            <ta e="T312" id="Seg_11069" s="T301">Er kam zu jenem Gerippe, das Gerippe lag leer da, nur der Kopf rollte weiter.</ta>
            <ta e="T320" id="Seg_11070" s="T312">"Was ist das, warum zerschneidet er sich selbst?", denkt er.</ta>
            <ta e="T326" id="Seg_11071" s="T320">"Was für ein Wettbewerb?!", wunderte er sich und ging.</ta>
            <ta e="T330" id="Seg_11072" s="T326">Er wurde ein Kopf und rollte weiter.</ta>
            <ta e="T334" id="Seg_11073" s="T330">Es liegt ein Kopf da.</ta>
            <ta e="T337" id="Seg_11074" s="T334">"Nun habe ich ihn aber erreicht!"</ta>
            <ta e="T342" id="Seg_11075" s="T337">Plötzlich rollten zwei Augen weg.</ta>
            <ta e="T346" id="Seg_11076" s="T342">"Was ist das für ein Mensch?</ta>
            <ta e="T352" id="Seg_11077" s="T346">Verfolge ich einen guten Geist oder einen schlechten Geist?!", sagt er.</ta>
            <ta e="T358" id="Seg_11078" s="T352">Die zwei Augen rollten entlang der Spur der [anderen] Augen.</ta>
            <ta e="T367" id="Seg_11079" s="T358">Irgendwann kam die Spur dieser beiden Augen zu einem Häuschen.</ta>
            <ta e="T374" id="Seg_11080" s="T367">Dort angekommen ging die Spur der zwei Augen ins Haus hinein.</ta>
            <ta e="T377" id="Seg_11081" s="T374">"Nun, jetzt bist du wohl am Ende!"</ta>
            <ta e="T382" id="Seg_11082" s="T377">Und die zwei hinteren Augen gingen ins Haus hinein.</ta>
            <ta e="T392" id="Seg_11083" s="T382">Sie gingen hinein, auf dem rechten Bett saß eine alte Frau, und die Augen funkelten im hinteren Teil der Jurte.</ta>
            <ta e="T400" id="Seg_11084" s="T392">Als die beiden Augen hereingingen, sagten die vorderen Augen: </ta>
            <ta e="T406" id="Seg_11085" s="T400">"Oh, auch der folgende Mensch ist müde.</ta>
            <ta e="T408" id="Seg_11086" s="T406">Ruh dich aus.</ta>
            <ta e="T414" id="Seg_11087" s="T408">Ich hole unsere Gerippe und unsere Köpfe", sagt er.</ta>
            <ta e="T417" id="Seg_11088" s="T414">Sein Freund Eisenmütze darauf: </ta>
            <ta e="T425" id="Seg_11089" s="T417">"Bist du bei richtigem Verstand oder nicht?</ta>
            <ta e="T432" id="Seg_11090" s="T425">Wir sind an einen Ort gekommen, von dem kein Mensch zurückkehrt!</ta>
            <ta e="T440" id="Seg_11091" s="T432">Jetzt wirst du mir folgen, mache mir alles nach.</ta>
            <ta e="T445" id="Seg_11092" s="T440">Du weißt, dass wir zum Herren des Schneesturms gekommen sind.</ta>
            <ta e="T452" id="Seg_11093" s="T445">Am Morgen, wenn die ersten Leute aufstehen, stehe ich auch auf und gehe hinaus.</ta>
            <ta e="T459" id="Seg_11094" s="T452">Dort werden zwei Mädchen kommen, jede mit einer Karawane von hundert Schlitten.</ta>
            <ta e="T478" id="Seg_11095" s="T459">Ich laufe hinaus, halte mich am Schulterriemen des Rentiers des vorderen Mädchens fest und laufe los, wenn du dich an irgendeinem der hundert Schlitten befestigst, dann kommst du in dein Land.</ta>
            <ta e="T484" id="Seg_11096" s="T478">Wenn du dich nicht befestigst, dann wirst du in dieser Welt sterben!", sagt er.</ta>
            <ta e="T491" id="Seg_11097" s="T484">Da brachte sein Freund, Knochengürtel, die Gerippe, Beine und Köpfe.</ta>
            <ta e="T496" id="Seg_11098" s="T491">Sie wurden wieder sie selbst.</ta>
            <ta e="T503" id="Seg_11099" s="T496">Nachdem sie geschlafen hatten, wachte Knochengürtel vom Lärm auf, als sein Freund hinausging.</ta>
            <ta e="T513" id="Seg_11100" s="T503">Knochengürtel rannte hinaus, als die hundert Schlitten vorbeihuschten, er hängte sich an den letzten Schlitten.</ta>
            <ta e="T519" id="Seg_11101" s="T513">Er merkte nicht, ob sie flogen oder fuhren.</ta>
            <ta e="T530" id="Seg_11102" s="T519">Dann später, als die hundert Schlitten anhielten, sah er, dass sein Freund ihn in sein Land gebracht hatte.</ta>
            <ta e="T534" id="Seg_11103" s="T530">Da sagte Eisenmütze seinem Freund: </ta>
            <ta e="T551" id="Seg_11104" s="T534">"Nun, an einer Seite dieser hundert Schlitten sind drei weißen Rentierbullem angespannt, an der anderen Seite drei schwarze Rentierbullen.</ta>
            <ta e="T566" id="Seg_11105" s="T551">Nun, ich werde die drei weißen Rentierbullen töten und ihnen das Fell abziehen, du wirst die drei Schwarzen töten und ihnen das Fell abziehen.</ta>
            <ta e="T574" id="Seg_11106" s="T566">Wisse, dass ich mit meiner Zauberkraft die jüngste Tochter des Herren des Schneesturms gebracht habe.</ta>
            <ta e="T582" id="Seg_11107" s="T574">Dieser folgend werden ihre drei älteren Bruder, Herren des Schneesturms, kommen.</ta>
            <ta e="T596" id="Seg_11108" s="T582">Wenn diese Leute kommen, dann breite ich im hinteren Teil des Hauses die drei weißen Felle aus, du aber breite deine schwarzen Felle aus.</ta>
            <ta e="T605" id="Seg_11109" s="T596">Bring in einen großen Kessel voll Wasser zum Kochen, ich bringe es auch zum kochen.</ta>
            <ta e="T609" id="Seg_11110" s="T605">Zuerst werden sie bei mir zu Gast sein.</ta>
            <ta e="T615" id="Seg_11111" s="T609">Wenn sie satt und zufrieden von mir weggehen, dann werden sie zu dir zu Besuch kommen.</ta>
            <ta e="T621" id="Seg_11112" s="T615">Ich zeige ihnen etwas, geh du mit ihnen dorthin.</ta>
            <ta e="T631" id="Seg_11113" s="T621">Was ich zeige, das machst du nach und zeigst es ihnen auch!", sagte er und lehrte seinen Freund.</ta>
            <ta e="T641" id="Seg_11114" s="T631">Es dauerte nicht lange, da kamen die Herren des Schneesturms in Gestalt von Wirbelwinden, Leute aus dichtem Eis.</ta>
            <ta e="T646" id="Seg_11115" s="T641">Als sie hineingingen, sagten sie nicht einmal "hallo".</ta>
            <ta e="T652" id="Seg_11116" s="T646">Im Haus von Eisenmütze setzten sie sich auf die drei weißen Felle.</ta>
            <ta e="T656" id="Seg_11117" s="T652">Eisenmütze musste diesen Leuten zeigen, was er konnte.</ta>
            <ta e="T681" id="Seg_11118" s="T656">Er verwandelte sich in einen Prachttaucher mit einem knöchernen Schnabel, "aa-uu" schreiend tauchte er in den großen Kessel mit kochendem Wasser, schlug mit den Flügeln und flog wieder [hinaus], er flog durch den Bauch seiner Mutter, dann flog er aus dem Mund seiner Mutter wieder hinaus.</ta>
            <ta e="T696" id="Seg_11119" s="T681">Und dann flog er durch den Bauch seines Vaters, aus dem Mund seines Vaters kam er als wilder Rentierbulle, der gerade sein Geweih abgezogen hat, heraus.</ta>
            <ta e="T709" id="Seg_11120" s="T696">Danach wurde Eisenmütze zum Menschen und schoss mit einem Bogen durch diesen wilden Rentierbullen hindurch.</ta>
            <ta e="T721" id="Seg_11121" s="T709">Er tötete ihn und zog ihm das Fell ab, das rohe Fleisch warf er in eine Holzschüssel und bewirtete seine Gäste mit dem rohen Fleisch.</ta>
            <ta e="T726" id="Seg_11122" s="T721">Die Gäste aßen schweigend.</ta>
            <ta e="T733" id="Seg_11123" s="T726">Von dem herbstlichen Rentierbullen ließen sie nur Geweih und Hufe übrig.</ta>
            <ta e="T736" id="Seg_11124" s="T733">Knochengürtel saß und schaute sich das an.</ta>
            <ta e="T742" id="Seg_11125" s="T736">Sie hörten auf zu essen, wischten sich die Lippen ab und gingen hinaus.</ta>
            <ta e="T747" id="Seg_11126" s="T742">Knochengürtel sprang auf, um die Gäste zu empfangen.</ta>
            <ta e="T754" id="Seg_11127" s="T747">Die Gäste kamen herein und setzten sich auf die drei schwarzen Felle.</ta>
            <ta e="T772" id="Seg_11128" s="T754">Knochengürtel wurde zu einem Sterntaucher, machte "Aa-uu!" und tauchte in den großen Kessel, er sauste in den Bauch seiner Mutter, er flog durch ihren Mund hinaus, er sauste in den Bauch seines Vaters.</ta>
            <ta e="T783" id="Seg_11129" s="T772">Aus dem Mund des Vaters sprang er als verwaistes Rentierkalb hinaus.</ta>
            <ta e="T794" id="Seg_11130" s="T783">Danach kam ein Mensch mit Bogen heraus und erschoss es nahe vor den Gästen, er fing an es zuzubereiten.</ta>
            <ta e="T797" id="Seg_11131" s="T794">Er servierte es den Gästen.</ta>
            <ta e="T800" id="Seg_11132" s="T797">Er servierte es.</ta>
            <ta e="T805" id="Seg_11133" s="T800">Sie bemerkten nicht, wohin alles ging.</ta>
            <ta e="T815" id="Seg_11134" s="T805">Die Herren des Schneesturms, da sie beleidigt waren, nahmen dieses Haus, Knochengürtel und seine Eltern als Wirbelwind mit.</ta>
            <ta e="T819" id="Seg_11135" s="T815">Es blieben nur Eisenmütze und seine Eltern. </ta>
            <ta e="T825" id="Seg_11136" s="T819">Er nahm die Tochter des Schneesturms zur Frau und lebte, so sagt man, in Reichtum und Wohlstand.</ta>
            <ta e="T826" id="Seg_11137" s="T825">Ende.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T8" id="Seg_11138" s="T0">Жили два молодых человека — Железная Шапка и Костяной Пояс.</ta>
            <ta e="T11" id="Seg_11139" s="T8">У обоих отец с матерью были.</ta>
            <ta e="T17" id="Seg_11140" s="T11">Вот однажды Костяной Пояс говорит Железной Шапке: </ta>
            <ta e="T23" id="Seg_11141" s="T17">"Друг, ну на что же мы способны?</ta>
            <ta e="T27" id="Seg_11142" s="T23">Почему не испытать друг друга?"</ta>
            <ta e="T31" id="Seg_11143" s="T27">Видно, хотел состязаться.</ta>
            <ta e="T40" id="Seg_11144" s="T31">Приятель:‎‎ "Э-э, друг, что нам делить друг с другом?", сказал.</ta>
            <ta e="T42" id="Seg_11145" s="T40">А товарищ никак не отвязывался: </ta>
            <ta e="T44" id="Seg_11146" s="T42">— Как что, друг!</ta>
            <ta e="T48" id="Seg_11147" s="T44">Хочу узнать, на что ты способен!</ta>
            <ta e="T50" id="Seg_11148" s="T48">Приятель не согласился.</ta>
            <ta e="T64" id="Seg_11149" s="T50">Костяной Пояс говорит: "Я буду убегать от тебя, ты преследуй меня, кем я стану, тем и ты становись". </ta>
            <ta e="T65" id="Seg_11150" s="T64">На это друг: </ta>
            <ta e="T68" id="Seg_11151" s="T65">"Пусть так, попробую."</ta>
            <ta e="T77" id="Seg_11152" s="T68">"Утром, когда встают самые ранние люди, последуешь за мной", говорит Костяной Пояс.</ta>
            <ta e="T82" id="Seg_11153" s="T77">Так договорившись, разошлись по домам.</ta>
            <ta e="T86" id="Seg_11154" s="T82">Железная Шапка пошел к себе спать.</ta>
            <ta e="T88" id="Seg_11155" s="T86">Проснулся: </ta>
            <ta e="T91" id="Seg_11156" s="T88">"дома ли приятель?"</ta>
            <ta e="T99" id="Seg_11157" s="T91">Пришел к приятелю: постель Костяного Пояса пуста — уже ушел.</ta>
            <ta e="T106" id="Seg_11158" s="T99">Железная Шапка стал искать, следов не нашел — совсем нет.</ta>
            <ta e="T109" id="Seg_11159" s="T106">Снова входит и роется в постели.</ta>
            <ta e="T120" id="Seg_11160" s="T109">Под постелью Костяного Пояса зияет дыра — нора остромордой мыши, других следов нет.</ta>
            <ta e="T126" id="Seg_11161" s="T120">Железная Шапка превратился в мышь и нырнул в нору.</ta>
            <ta e="T133" id="Seg_11162" s="T126">А тот [Костяной Пояс], выскочив из норы, обернулся горностаем и умчался.</ta>
            <ta e="T138" id="Seg_11163" s="T133">Железная Шапка, тотчас обернувшись горностаем, за ним последовал.</ta>
            <ta e="T142" id="Seg_11164" s="T138">Мчится только наметом.</ta>
            <ta e="T153" id="Seg_11165" s="T142">Кончились следы горностая, дальше по Средней земле пошли следы песца.</ta>
            <ta e="T156" id="Seg_11166" s="T153">Друг только удивляется.</ta>
            <ta e="T162" id="Seg_11167" s="T156">Железная Шапка в песца превратился и несется.</ta>
            <ta e="T170" id="Seg_11168" s="T162">Прыгнул здесь песцом Костяной Пояс, дальше сиганул зайцем.</ta>
            <ta e="T174" id="Seg_11169" s="T170">И тот, став зайцем, его преследует.</ta>
            <ta e="T178" id="Seg_11170" s="T174">Потом Костяной Пояс лисой обернулся.</ta>
            <ta e="T186" id="Seg_11171" s="T178">И тот, тоже лисой обернувшись, след в след идет.</ta>
            <ta e="T191" id="Seg_11172" s="T186">Тот волком, вытянувшись, полетел.</ta>
            <ta e="T194" id="Seg_11173" s="T191">Этот мчится и восхищается: </ta>
            <ta e="T199" id="Seg_11174" s="T194">"Видно, не мог совладать с такой силой, вот и напросился [состязаться]!", думает.</ta>
            <ta e="T203" id="Seg_11175" s="T199">Волком став, преследует.</ta>
            <ta e="T206" id="Seg_11176" s="T203">Сколько шел — неизвестно.</ta>
            <ta e="T211" id="Seg_11177" s="T206">Костяной Пояс, в росомаху превратившись, трусцой пошел.</ta>
            <ta e="T219" id="Seg_11178" s="T211">Этот, не переставая восхищаться, в росомаху превратился и преследует.</ta>
            <ta e="T225" id="Seg_11179" s="T219">Как только оборвался след росомахи, этот, став медведем, дальше затопал.</ta>
            <ta e="T230" id="Seg_11180" s="T225">Медведем обернувшись, пошел следом Железная Шапка.</ta>
            <ta e="T235" id="Seg_11181" s="T230">Все время на пределе мчались.</ta>
            <ta e="T244" id="Seg_11182" s="T235">Как только исчез след медведя, дальше прыгнул олень-самец.</ta>
            <ta e="T250" id="Seg_11183" s="T244">Всё восхищаясь, весь век друг его преследует.</ta>
            <ta e="T254" id="Seg_11184" s="T250">Железная Шапка тут диким оленем-самцом стал.</ta>
            <ta e="T263" id="Seg_11185" s="T254">"Оборотничеству твоему, видно, скоро предел, нагоню вот-вот", думает.</ta>
            <ta e="T269" id="Seg_11186" s="T263">Увидел: в одном месте друг его, раскорячившись, стоит.</ta>
            <ta e="T282" id="Seg_11187" s="T269">Подошел: только две ноги остались торчать, а туша вместе с суставами, отделившись, укатилась.</ta>
            <ta e="T291" id="Seg_11188" s="T282">Так же и Железная Шапка оставил ноги — покатился тушей.</ta>
            <ta e="T297" id="Seg_11189" s="T291">Недолго шел, впереди туша громоздится.</ta>
            <ta e="T301" id="Seg_11190" s="T297">Радуется: "Настиг-таки!", говорит. </ta>
            <ta e="T312" id="Seg_11191" s="T301">Подкатился к той туше — только туша громоздится, дальше одна голова покатилась.</ta>
            <ta e="T320" id="Seg_11192" s="T312">"Что такое, зачем калечит себя?", думает.</ta>
            <ta e="T326" id="Seg_11193" s="T320">"На что такое состязание?!", досадует.</ta>
            <ta e="T330" id="Seg_11194" s="T326">Став головой, покатился.</ta>
            <ta e="T334" id="Seg_11195" s="T330">Вот голова лежит.</ta>
            <ta e="T337" id="Seg_11196" s="T334">"Наконец-то настиг!" </ta>
            <ta e="T342" id="Seg_11197" s="T337">Теперь два глазных яблока покатились.</ta>
            <ta e="T346" id="Seg_11198" s="T342">"Ну что это за человек?</ta>
            <ta e="T352" id="Seg_11199" s="T346">Преследую человека айыы или абаасы?!", говорит.</ta>
            <ta e="T358" id="Seg_11200" s="T352">Два глазных яблока катятся по следу тех.</ta>
            <ta e="T367" id="Seg_11201" s="T358">Через какое-то время следы тех двух глаз к одному домику привели.</ta>
            <ta e="T374" id="Seg_11202" s="T367">Следы глаз ведут в дом.</ta>
            <ta e="T377" id="Seg_11203" s="T374">"Ну, видать, это твой предел!"</ta>
            <ta e="T382" id="Seg_11204" s="T377">Два преследующих глаза вошли в дом.</ta>
            <ta e="T392" id="Seg_11205" s="T382">Вошли, на средней лавке старуха сидит, а глаза на передней лавке поблескивают.</ta>
            <ta e="T400" id="Seg_11206" s="T392">Когда вкатились два глаза, первые два глаза сказали: </ta>
            <ta e="T406" id="Seg_11207" s="T400">— О, и преследующий тоже устает.</ta>
            <ta e="T408" id="Seg_11208" s="T406">Ты отдохни.</ta>
            <ta e="T414" id="Seg_11209" s="T408">Я туши, головы наши соберу и принесу", говорит.</ta>
            <ta e="T417" id="Seg_11210" s="T414">Товарищ его, Железная Шапка, на это говорит: </ta>
            <ta e="T425" id="Seg_11211" s="T417">"В трезвом ли ты уме?</ta>
            <ta e="T432" id="Seg_11212" s="T425">Мы достигли земли, откуда нет возврата!</ta>
            <ta e="T440" id="Seg_11213" s="T432">Теперь будешь следовать за мной, постигай мои оборотничества.</ta>
            <ta e="T445" id="Seg_11214" s="T440">Знаешь, мы пришли в страну Духа пурги.</ta>
            <ta e="T452" id="Seg_11215" s="T445">Поутру, как только встанут самые ранние люди, я выйду.</ta>
            <ta e="T459" id="Seg_11216" s="T452">Тут подъедут две девушки, каждая с сотней нарт в караване.</ta>
            <ta e="T478" id="Seg_11217" s="T459">Я выскочу, уцеплюсь за шлейку передового оленя первой девушки и полечу, если ты успеешь зацепиться за какую-либо из сотни нарт, достигнешь родины.</ta>
            <ta e="T484" id="Seg_11218" s="T478">Если не зацепишься, пропадешь в этом краю!", сказал.</ta>
            <ta e="T491" id="Seg_11219" s="T484">Вот друг его, Костяной Пояс, принес туши, ноги и головы.</ta>
            <ta e="T496" id="Seg_11220" s="T491">Стали они снова сами собой.</ta>
            <ta e="T503" id="Seg_11221" s="T496">Переночевали, Костяной Пояс проснулся от шороха, когда выходил товарищ.</ta>
            <ta e="T513" id="Seg_11222" s="T503">Выскочил во двор Костяной Пояс и, когда сотня нарт промелькнула мимо, успел ухватиться за последние нарты.</ta>
            <ta e="T519" id="Seg_11223" s="T513">Не заметил он, по небу ли, по земле ли несся.</ta>
            <ta e="T530" id="Seg_11224" s="T519">В конце концов, позднее позднего, когда остановилась сотня нарт, опомнился и увидел, что товарищ привел его на родину.</ta>
            <ta e="T534" id="Seg_11225" s="T530">Вот Железная Шапка говорит другу: </ta>
            <ta e="T551" id="Seg_11226" s="T534">— Вот по одному краю ста нарт запряжены три белых оленя-самца, а по другому краю стоят три черных оленя-самца.</ta>
            <ta e="T566" id="Seg_11227" s="T551">Вот я забью трех белых, сниму шкуры, ты забьешь трех черных, снимешь шкуры.</ta>
            <ta e="T574" id="Seg_11228" s="T566">Знай: своим волшебством я привел сюда младшую дочь Духа пурги.</ta>
            <ta e="T582" id="Seg_11229" s="T574">По следу ее сюда прибудут скоро три ее брата — Духи пурги.</ta>
            <ta e="T596" id="Seg_11230" s="T582">Когда прибудут те люди, на переднюю лавку постелю три белые шкуры, а ты у себя постелешь три черные шкуры.</ta>
            <ta e="T605" id="Seg_11231" s="T596">Полный котел воды вскипятишь, я тоже вскипячу.</ta>
            <ta e="T609" id="Seg_11232" s="T605">Первыми у меня будут гостевать.</ta>
            <ta e="T615" id="Seg_11233" s="T609">Если выйдут сытыми и довольными от меня, у тебя погостюют.</ta>
            <ta e="T621" id="Seg_11234" s="T615">Я им покажу кое-что, зайдешь с ними и посмотришь.</ta>
            <ta e="T631" id="Seg_11235" s="T621">Что я ни покажу, повторишь у себя и покажешь им!", так наставлял товарища.</ta>
            <ta e="T641" id="Seg_11236" s="T631">Много ли [времени] прошло, прилетели в виде вихрей Духи пурги — люди из сплошного льда.</ta>
            <ta e="T646" id="Seg_11237" s="T641">Войдя, даже "здравствуй" не сказали.</ta>
            <ta e="T652" id="Seg_11238" s="T646">В доме Железной Шапки уселись на трех белых шкурах.</ta>
            <ta e="T656" id="Seg_11239" s="T652">Железной Шапке приходится показывать этим людям свое искусство.</ta>
            <ta e="T681" id="Seg_11240" s="T656">Превратившись в чернозобую гагару с костяным клювом, с криком "Аа-уу" нырнул в большой котел с кипящей водой, вынырнул, взлетел и в живот матери шмыгнул, оттуда через рот матери вылетел.</ta>
            <ta e="T696" id="Seg_11241" s="T681">А потом юркнул в живот отца, через рот отца выскочил осенним оленем-самцом с только что очищенными рогами.</ta>
            <ta e="T709" id="Seg_11242" s="T696">Тут Железная Шапка обернулся человеком и прострелил из лука насквозь этого осеннего оленя-самца.</ta>
            <ta e="T721" id="Seg_11243" s="T709">Забив, сразу же содрал шкуру, а сырое мясо нарезал в деревянную миску и стал угощать гостей свежатиной.</ta>
            <ta e="T726" id="Seg_11244" s="T721">Гости молча принялись за еду.</ta>
            <ta e="T733" id="Seg_11245" s="T726">От осеннего оленя-самца оставили только рога да копыта.</ta>
            <ta e="T736" id="Seg_11246" s="T733">Костяной Пояс сидит себе поглядывает.</ta>
            <ta e="T742" id="Seg_11247" s="T736">Кончили есть и, вытерев губы, вышли.</ta>
            <ta e="T747" id="Seg_11248" s="T742">Костяной Пояс бросился встретить гостей.</ta>
            <ta e="T754" id="Seg_11249" s="T747">Гости заходят и рассаживаются на трех черных шкурах.</ta>
            <ta e="T772" id="Seg_11250" s="T754">Костяной Пояс, превратившись в краснозобую малую гагару, с криком "А-уук!" нырнул в котел с водой, в живот матери шмыгнул, вылетел через ее рот, в живот отца юркнул.</ta>
            <ta e="T783" id="Seg_11251" s="T772">Из отцовского рта выскочил худющим олененком-заморышем.</ta>
            <ta e="T794" id="Seg_11252" s="T783">Следом вышел человек с луком и свалил олененка перед гостями, стал свежевать.</ta>
            <ta e="T797" id="Seg_11253" s="T794">Гостям накрывает.</ta>
            <ta e="T800" id="Seg_11254" s="T797">Накрыл.</ta>
            <ta e="T805" id="Seg_11255" s="T800">Не заметили, куда все делось.</ta>
            <ta e="T815" id="Seg_11256" s="T805">Духи пурги, обидевшись, подхватили вихрями дом с Костяным Поясом и его родителями и унесли с собой.</ta>
            <ta e="T819" id="Seg_11257" s="T815">Только и остался Железная Шапка с отцом и матерью. </ta>
            <ta e="T825" id="Seg_11258" s="T819">Взяв в жены дочь Пурги, зажил, говорят, в богатстве и достатке.</ta>
            <ta e="T826" id="Seg_11259" s="T825">Конец.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T8" id="Seg_11260" s="T0">Жили два молодых человека — Железная Шапка и Костяной Пояс.</ta>
            <ta e="T11" id="Seg_11261" s="T8">У обоих отец с матерью были.</ta>
            <ta e="T17" id="Seg_11262" s="T11">Вот однажды Костяной Пояс говорит Железной Шапке: </ta>
            <ta e="T23" id="Seg_11263" s="T17">— Друг, ну на что же мы способны?</ta>
            <ta e="T27" id="Seg_11264" s="T23">Почему не испытать друг друга?</ta>
            <ta e="T31" id="Seg_11265" s="T27">— видно, хочет состязаться.</ta>
            <ta e="T40" id="Seg_11266" s="T31">Приятель не хочет:‎‎ — Э-э, друг, что нам делить друг с другом?</ta>
            <ta e="T42" id="Seg_11267" s="T40">А товарищ никак не отвязывается: </ta>
            <ta e="T44" id="Seg_11268" s="T42">— Как что, друг!</ta>
            <ta e="T48" id="Seg_11269" s="T44">Хочу узнать, на что ты способен!</ta>
            <ta e="T50" id="Seg_11270" s="T48">Приятель не соглашается.</ta>
            <ta e="T64" id="Seg_11271" s="T50">Костяной Пояс говорит: — Я буду убегать от тебя, ты преследуй меня, кем я стану, тем и ты становись.</ta>
            <ta e="T65" id="Seg_11272" s="T64">На это друг: </ta>
            <ta e="T68" id="Seg_11273" s="T65">— Пусть так, попробую.</ta>
            <ta e="T77" id="Seg_11274" s="T68">— Утром, когда встают самые ранние люди, последуешь за мной, — говорит Костяной Пояс.</ta>
            <ta e="T82" id="Seg_11275" s="T77">Так договорившись, разошлись по домам.</ta>
            <ta e="T86" id="Seg_11276" s="T82">Железная Шапка пошел к себе спать.</ta>
            <ta e="T88" id="Seg_11277" s="T86">Проснувшись, думает: </ta>
            <ta e="T91" id="Seg_11278" s="T88">дома ли приятель?</ta>
            <ta e="T99" id="Seg_11279" s="T91">Пришел к приятелю: постель Костяного Пояса пуста — уже ушел.</ta>
            <ta e="T106" id="Seg_11280" s="T99">Железная Шапка стал искать, следов не нашел — совсем нет.</ta>
            <ta e="T109" id="Seg_11281" s="T106">Снова входит и роется в постели.</ta>
            <ta e="T120" id="Seg_11282" s="T109">Под постелью Костяного Пояса зияет дыра — нора остромордой мыши, других следов нет.</ta>
            <ta e="T126" id="Seg_11283" s="T120">Железная Шапка превратился в мышь и нырнул в нору.</ta>
            <ta e="T133" id="Seg_11284" s="T126">А тот [Костяной Пояс], выскочив из норы, обернулся горностаем и умчался.</ta>
            <ta e="T138" id="Seg_11285" s="T133">Железная Шапка, тотчас обернувшись горностаем, за ним последовал.</ta>
            <ta e="T142" id="Seg_11286" s="T138">Мчится только наметом.</ta>
            <ta e="T153" id="Seg_11287" s="T142">Кончились следы горностая, дальше по Средней земле пошли следы песца.</ta>
            <ta e="T156" id="Seg_11288" s="T153">Друг только удивляется.</ta>
            <ta e="T162" id="Seg_11289" s="T156">Железная Шапка в песца превратился и несется.</ta>
            <ta e="T170" id="Seg_11290" s="T162">Прыгнул здесь песцом Костяной Пояс, дальше сиганул зайцем.</ta>
            <ta e="T174" id="Seg_11291" s="T170">И тот, став зайцем, его преследует.</ta>
            <ta e="T178" id="Seg_11292" s="T174">Потом Костяной Пояс лисой обернулся.</ta>
            <ta e="T186" id="Seg_11293" s="T178">И тот, тоже лисой обернувшись, след в след идет.</ta>
            <ta e="T191" id="Seg_11294" s="T186">Тот волком, вытянувшись, полетел.</ta>
            <ta e="T194" id="Seg_11295" s="T191">Этот мчится и восхищается: </ta>
            <ta e="T199" id="Seg_11296" s="T194">"Видно, не мог совладать с такой силой, вот и напросился [состязаться]!" — думает.</ta>
            <ta e="T203" id="Seg_11297" s="T199">Волком став, преследует.</ta>
            <ta e="T206" id="Seg_11298" s="T203">Сколько шел — неизвестно.</ta>
            <ta e="T211" id="Seg_11299" s="T206">Костяной Пояс, в росомаху превратившись, трусцой пошел.</ta>
            <ta e="T219" id="Seg_11300" s="T211">Этот, не переставая восхищаться, в росомаху превратился и преследует.</ta>
            <ta e="T225" id="Seg_11301" s="T219">Как только оборвался след росомахи, этот, став медведем, дальше затопал.</ta>
            <ta e="T230" id="Seg_11302" s="T225">Медведем обернувшись, пошел следом Железная Шапка.</ta>
            <ta e="T235" id="Seg_11303" s="T230">Все время на пределе мчались.</ta>
            <ta e="T244" id="Seg_11304" s="T235">Как только исчез след медведя, дальше прыгнул олень-самец.</ta>
            <ta e="T250" id="Seg_11305" s="T244">Всё восхищаясь, весь век друг его преследует.</ta>
            <ta e="T254" id="Seg_11306" s="T250">Железная Шапка тут диким оленем-самцом стал.</ta>
            <ta e="T263" id="Seg_11307" s="T254">"Оборотничеству твоему, видно, скоро предел, нагоню вот-вот", — думает.</ta>
            <ta e="T269" id="Seg_11308" s="T263">Увидел: в одном месте друг его, раскорячившись, стоит.</ta>
            <ta e="T282" id="Seg_11309" s="T269">Подошел: только две ноги остались торчать, а туша вместе с суставами, отделившись, укатилась.</ta>
            <ta e="T291" id="Seg_11310" s="T282">Так же и Железная Шапка оставил ноги — покатился тушей.</ta>
            <ta e="T297" id="Seg_11311" s="T291">Недолго шел, впереди туша громоздится.</ta>
            <ta e="T301" id="Seg_11312" s="T297">Радуется: "Настиг-таки!" </ta>
            <ta e="T312" id="Seg_11313" s="T301">Подкатился к той туше — только туша громоздится, дальше одна голова покатилась.</ta>
            <ta e="T320" id="Seg_11314" s="T312">"Что такое, зачем калечит себя?</ta>
            <ta e="T326" id="Seg_11315" s="T320">На что такое состязание?!" — досадует.</ta>
            <ta e="T330" id="Seg_11316" s="T326">Став головой, покатился.</ta>
            <ta e="T334" id="Seg_11317" s="T330">Вот голова лежит.</ta>
            <ta e="T337" id="Seg_11318" s="T334">"Наконец-то настиг!" </ta>
            <ta e="T342" id="Seg_11319" s="T337">Теперь два глазных яблока покатились.</ta>
            <ta e="T346" id="Seg_11320" s="T342">"Ну что это за человек?</ta>
            <ta e="T352" id="Seg_11321" s="T346">Преследую человека айыы или абаасы?!" — говорит.</ta>
            <ta e="T358" id="Seg_11322" s="T352">Два глазных яблока катятся по следу тех.</ta>
            <ta e="T367" id="Seg_11323" s="T358">Через какое-то время следы тех двух глаз к одному домику привели.</ta>
            <ta e="T374" id="Seg_11324" s="T367">Следы глаз ведут в дом.</ta>
            <ta e="T377" id="Seg_11325" s="T374">"Ну, видать, это твой предел!" </ta>
            <ta e="T382" id="Seg_11326" s="T377">— два преследующих глаза вошли в дом.</ta>
            <ta e="T392" id="Seg_11327" s="T382">Вошли, на средней лавке старуха сидит, а глаза на передней лавке поблескивают.</ta>
            <ta e="T400" id="Seg_11328" s="T392">Когда вкатились два глаза, первые два глаза сказали: </ta>
            <ta e="T406" id="Seg_11329" s="T400">— О, и преследующий тоже устает.</ta>
            <ta e="T408" id="Seg_11330" s="T406">Ты отдохни.</ta>
            <ta e="T414" id="Seg_11331" s="T408">Я туши, головы наши соберу и принесу.</ta>
            <ta e="T417" id="Seg_11332" s="T414">Товарищ его, Железная Шапка, на это говорит: </ta>
            <ta e="T425" id="Seg_11333" s="T417">— В трезвом ли ты уме?</ta>
            <ta e="T432" id="Seg_11334" s="T425">Мы достигли земли, откуда нет возврата!</ta>
            <ta e="T440" id="Seg_11335" s="T432">Теперь будешь следовать за мной, постигай мои оборотничества.</ta>
            <ta e="T445" id="Seg_11336" s="T440">Знаешь, мы пришли в страну Духа пурги.</ta>
            <ta e="T452" id="Seg_11337" s="T445">Поутру, как только встанут самые ранние люди, я выйду.</ta>
            <ta e="T459" id="Seg_11338" s="T452">Тут подъедут две девушки, каждая с сотней нарт в караване.</ta>
            <ta e="T478" id="Seg_11339" s="T459">Я выскочу, уцеплюсь за шлейку передового оленя первой девушки и полечу, если ты успеешь зацепиться за какую-либо из сотни нарт, достигнешь родины.</ta>
            <ta e="T484" id="Seg_11340" s="T478">Если не зацепишься, пропадешь в этом краю! — сказал.</ta>
            <ta e="T491" id="Seg_11341" s="T484">Вот друг его, Костяной Пояс, принес туши, ноги и головы.</ta>
            <ta e="T496" id="Seg_11342" s="T491">Стали они снова сами собой.</ta>
            <ta e="T503" id="Seg_11343" s="T496">Переночевали, Костяной Пояс проснулся от шороха, когда выходил товарищ.</ta>
            <ta e="T513" id="Seg_11344" s="T503">Выскочил во двор Костяной Пояс и, когда сотня нарт промелькнула мимо, успел ухватиться за последние нарты.</ta>
            <ta e="T519" id="Seg_11345" s="T513">Не заметил он, по небу ли, по земле ли несся.</ta>
            <ta e="T530" id="Seg_11346" s="T519">В конце концов, позднее позднего, когда остановилась сотня нарт, опомнился и увидел, что товарищ привел его на родину.</ta>
            <ta e="T534" id="Seg_11347" s="T530">Вот Железная Шапка говорит другу: </ta>
            <ta e="T551" id="Seg_11348" s="T534">— Вот по одному краю ста нарт запряжены три белых оленя-самца, а по другому краю стоят три черных оленя-самца.</ta>
            <ta e="T566" id="Seg_11349" s="T551">Вот я забью трех белых, сниму шкуры, ты забьешь трех черных, снимешь шкуры.</ta>
            <ta e="T574" id="Seg_11350" s="T566">Знай: своим волшебством я привел сюда младшую дочь Духа пурги.</ta>
            <ta e="T582" id="Seg_11351" s="T574">По следу ее сюда прибудут скоро три ее брата — Духи пурги.</ta>
            <ta e="T596" id="Seg_11352" s="T582">Когда прибудут те люди, на переднюю лавку постелю три белые шкуры, а ты у себя постелешь три черные шкуры.</ta>
            <ta e="T605" id="Seg_11353" s="T596">Полный котел воды вскипятишь, я тоже вскипячу.</ta>
            <ta e="T609" id="Seg_11354" s="T605">Первыми у меня будут гостевать.</ta>
            <ta e="T615" id="Seg_11355" s="T609">Если выйдут сытыми и довольными от меня, у тебя погостюют.</ta>
            <ta e="T621" id="Seg_11356" s="T615">Я им покажу кое-что, зайдешь с ними и посмотришь.</ta>
            <ta e="T631" id="Seg_11357" s="T621">Что я ни покажу, повторишь у себя и покажешь им! — так наставлял товарища.</ta>
            <ta e="T641" id="Seg_11358" s="T631">Много ли [времени] прошло, прилетели в виде вихрей Духи пурги — люди из сплошного льда.</ta>
            <ta e="T646" id="Seg_11359" s="T641">Войдя, даже "здравствуй" не сказали.</ta>
            <ta e="T652" id="Seg_11360" s="T646">В доме Железной Шапки уселись на трех белых шкурах.</ta>
            <ta e="T656" id="Seg_11361" s="T652">Железной Шапке приходится показывать этим людям свое искусство.</ta>
            <ta e="T681" id="Seg_11362" s="T656">Превратившись в чернозобую гагару с костяным клювом, с криком "Аа-уу" нырнул в большой котел с кипящей водой, вынырнул, взлетел и в живот матери шмыгнул, оттуда через рот матери вылетел.</ta>
            <ta e="T696" id="Seg_11363" s="T681">А потом юркнул в живот отца, через рот отца выскочил осенним оленем-самцом с только что очищенными рогами.</ta>
            <ta e="T709" id="Seg_11364" s="T696">Тут Железная Шапка обернулся человеком и прострелил из лука насквозь этого осеннего оленя-самца.</ta>
            <ta e="T721" id="Seg_11365" s="T709">Забив, сразу же содрал шкуру, а сырое мясо нарезал в деревянную миску и стал угощать гостей свежатиной.</ta>
            <ta e="T726" id="Seg_11366" s="T721">Гости молча принялись за еду.</ta>
            <ta e="T733" id="Seg_11367" s="T726">От осеннего оленя-самца оставили только рога да копыта.</ta>
            <ta e="T736" id="Seg_11368" s="T733">Костяной Пояс сидит себе поглядывает.</ta>
            <ta e="T742" id="Seg_11369" s="T736">Кончили есть и, вытерев губы, вышли.</ta>
            <ta e="T747" id="Seg_11370" s="T742">Костяной Пояс бросился встретить гостей.</ta>
            <ta e="T754" id="Seg_11371" s="T747">Гости заходят и рассаживаются на трех черных шкурах.</ta>
            <ta e="T772" id="Seg_11372" s="T754">Костяной Пояс, превратившись в краснозобую малую гагару, с криком "А-уук!" нырнул в котел с водой, в живот матери шмыгнул, вылетел через ее рот, в живот отца юркнул.</ta>
            <ta e="T783" id="Seg_11373" s="T772">Из отцовского рта выскочил худющим олененком-заморышем.</ta>
            <ta e="T794" id="Seg_11374" s="T783">Следом вышел человек с луком и свалил олененка перед гостями, стал свежевать.</ta>
            <ta e="T797" id="Seg_11375" s="T794">Гостям накрывает.</ta>
            <ta e="T800" id="Seg_11376" s="T797">Накрыл было </ta>
            <ta e="T805" id="Seg_11377" s="T800">— не заметили, куда все делось.</ta>
            <ta e="T815" id="Seg_11378" s="T805">Духи пурги, обидевшись, подхватили вихрями дом с Костяным Поясом и его родителями и унесли с собой.</ta>
            <ta e="T819" id="Seg_11379" s="T815">Только и остался Железная Шапка с отцом и матерью. </ta>
            <ta e="T825" id="Seg_11380" s="T819">Взяв в жены дочь Пурги, зажил, говорят, в богатстве и достатке.</ta>
            <ta e="T826" id="Seg_11381" s="T825">Конец.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T459" id="Seg_11382" s="T452">[DCh]: In the original text the first word is "manɨgam". As this form is unknown by native speakers and the form without final -m fits well, it seems to be a typo in the original.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T559" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
            <conversion-tli id="T571" />
            <conversion-tli id="T572" />
            <conversion-tli id="T573" />
            <conversion-tli id="T574" />
            <conversion-tli id="T575" />
            <conversion-tli id="T576" />
            <conversion-tli id="T577" />
            <conversion-tli id="T578" />
            <conversion-tli id="T579" />
            <conversion-tli id="T580" />
            <conversion-tli id="T581" />
            <conversion-tli id="T582" />
            <conversion-tli id="T583" />
            <conversion-tli id="T584" />
            <conversion-tli id="T585" />
            <conversion-tli id="T586" />
            <conversion-tli id="T587" />
            <conversion-tli id="T588" />
            <conversion-tli id="T589" />
            <conversion-tli id="T590" />
            <conversion-tli id="T591" />
            <conversion-tli id="T592" />
            <conversion-tli id="T593" />
            <conversion-tli id="T594" />
            <conversion-tli id="T595" />
            <conversion-tli id="T596" />
            <conversion-tli id="T597" />
            <conversion-tli id="T598" />
            <conversion-tli id="T599" />
            <conversion-tli id="T600" />
            <conversion-tli id="T601" />
            <conversion-tli id="T602" />
            <conversion-tli id="T603" />
            <conversion-tli id="T604" />
            <conversion-tli id="T605" />
            <conversion-tli id="T606" />
            <conversion-tli id="T607" />
            <conversion-tli id="T608" />
            <conversion-tli id="T609" />
            <conversion-tli id="T610" />
            <conversion-tli id="T611" />
            <conversion-tli id="T612" />
            <conversion-tli id="T613" />
            <conversion-tli id="T614" />
            <conversion-tli id="T615" />
            <conversion-tli id="T616" />
            <conversion-tli id="T617" />
            <conversion-tli id="T618" />
            <conversion-tli id="T619" />
            <conversion-tli id="T620" />
            <conversion-tli id="T621" />
            <conversion-tli id="T622" />
            <conversion-tli id="T623" />
            <conversion-tli id="T624" />
            <conversion-tli id="T625" />
            <conversion-tli id="T626" />
            <conversion-tli id="T627" />
            <conversion-tli id="T628" />
            <conversion-tli id="T629" />
            <conversion-tli id="T630" />
            <conversion-tli id="T631" />
            <conversion-tli id="T632" />
            <conversion-tli id="T633" />
            <conversion-tli id="T634" />
            <conversion-tli id="T635" />
            <conversion-tli id="T636" />
            <conversion-tli id="T637" />
            <conversion-tli id="T638" />
            <conversion-tli id="T639" />
            <conversion-tli id="T640" />
            <conversion-tli id="T641" />
            <conversion-tli id="T642" />
            <conversion-tli id="T643" />
            <conversion-tli id="T644" />
            <conversion-tli id="T645" />
            <conversion-tli id="T646" />
            <conversion-tli id="T647" />
            <conversion-tli id="T648" />
            <conversion-tli id="T649" />
            <conversion-tli id="T650" />
            <conversion-tli id="T651" />
            <conversion-tli id="T652" />
            <conversion-tli id="T653" />
            <conversion-tli id="T654" />
            <conversion-tli id="T655" />
            <conversion-tli id="T656" />
            <conversion-tli id="T657" />
            <conversion-tli id="T658" />
            <conversion-tli id="T659" />
            <conversion-tli id="T660" />
            <conversion-tli id="T661" />
            <conversion-tli id="T662" />
            <conversion-tli id="T663" />
            <conversion-tli id="T664" />
            <conversion-tli id="T665" />
            <conversion-tli id="T666" />
            <conversion-tli id="T667" />
            <conversion-tli id="T668" />
            <conversion-tli id="T669" />
            <conversion-tli id="T670" />
            <conversion-tli id="T671" />
            <conversion-tli id="T672" />
            <conversion-tli id="T673" />
            <conversion-tli id="T674" />
            <conversion-tli id="T675" />
            <conversion-tli id="T676" />
            <conversion-tli id="T677" />
            <conversion-tli id="T678" />
            <conversion-tli id="T679" />
            <conversion-tli id="T680" />
            <conversion-tli id="T681" />
            <conversion-tli id="T682" />
            <conversion-tli id="T683" />
            <conversion-tli id="T684" />
            <conversion-tli id="T685" />
            <conversion-tli id="T686" />
            <conversion-tli id="T687" />
            <conversion-tli id="T688" />
            <conversion-tli id="T689" />
            <conversion-tli id="T690" />
            <conversion-tli id="T691" />
            <conversion-tli id="T692" />
            <conversion-tli id="T693" />
            <conversion-tli id="T694" />
            <conversion-tli id="T695" />
            <conversion-tli id="T696" />
            <conversion-tli id="T697" />
            <conversion-tli id="T698" />
            <conversion-tli id="T699" />
            <conversion-tli id="T700" />
            <conversion-tli id="T701" />
            <conversion-tli id="T702" />
            <conversion-tli id="T703" />
            <conversion-tli id="T704" />
            <conversion-tli id="T705" />
            <conversion-tli id="T706" />
            <conversion-tli id="T707" />
            <conversion-tli id="T708" />
            <conversion-tli id="T709" />
            <conversion-tli id="T710" />
            <conversion-tli id="T711" />
            <conversion-tli id="T712" />
            <conversion-tli id="T713" />
            <conversion-tli id="T714" />
            <conversion-tli id="T715" />
            <conversion-tli id="T716" />
            <conversion-tli id="T717" />
            <conversion-tli id="T718" />
            <conversion-tli id="T719" />
            <conversion-tli id="T720" />
            <conversion-tli id="T721" />
            <conversion-tli id="T722" />
            <conversion-tli id="T723" />
            <conversion-tli id="T724" />
            <conversion-tli id="T725" />
            <conversion-tli id="T726" />
            <conversion-tli id="T727" />
            <conversion-tli id="T728" />
            <conversion-tli id="T729" />
            <conversion-tli id="T730" />
            <conversion-tli id="T731" />
            <conversion-tli id="T732" />
            <conversion-tli id="T733" />
            <conversion-tli id="T734" />
            <conversion-tli id="T735" />
            <conversion-tli id="T736" />
            <conversion-tli id="T737" />
            <conversion-tli id="T738" />
            <conversion-tli id="T739" />
            <conversion-tli id="T740" />
            <conversion-tli id="T741" />
            <conversion-tli id="T742" />
            <conversion-tli id="T743" />
            <conversion-tli id="T744" />
            <conversion-tli id="T745" />
            <conversion-tli id="T746" />
            <conversion-tli id="T747" />
            <conversion-tli id="T748" />
            <conversion-tli id="T749" />
            <conversion-tli id="T750" />
            <conversion-tli id="T751" />
            <conversion-tli id="T752" />
            <conversion-tli id="T753" />
            <conversion-tli id="T754" />
            <conversion-tli id="T755" />
            <conversion-tli id="T756" />
            <conversion-tli id="T757" />
            <conversion-tli id="T758" />
            <conversion-tli id="T759" />
            <conversion-tli id="T760" />
            <conversion-tli id="T761" />
            <conversion-tli id="T762" />
            <conversion-tli id="T763" />
            <conversion-tli id="T764" />
            <conversion-tli id="T765" />
            <conversion-tli id="T766" />
            <conversion-tli id="T767" />
            <conversion-tli id="T768" />
            <conversion-tli id="T769" />
            <conversion-tli id="T770" />
            <conversion-tli id="T771" />
            <conversion-tli id="T772" />
            <conversion-tli id="T773" />
            <conversion-tli id="T774" />
            <conversion-tli id="T775" />
            <conversion-tli id="T776" />
            <conversion-tli id="T777" />
            <conversion-tli id="T778" />
            <conversion-tli id="T779" />
            <conversion-tli id="T780" />
            <conversion-tli id="T781" />
            <conversion-tli id="T782" />
            <conversion-tli id="T783" />
            <conversion-tli id="T784" />
            <conversion-tli id="T785" />
            <conversion-tli id="T786" />
            <conversion-tli id="T787" />
            <conversion-tli id="T788" />
            <conversion-tli id="T789" />
            <conversion-tli id="T790" />
            <conversion-tli id="T791" />
            <conversion-tli id="T792" />
            <conversion-tli id="T793" />
            <conversion-tli id="T794" />
            <conversion-tli id="T795" />
            <conversion-tli id="T796" />
            <conversion-tli id="T797" />
            <conversion-tli id="T798" />
            <conversion-tli id="T799" />
            <conversion-tli id="T800" />
            <conversion-tli id="T801" />
            <conversion-tli id="T802" />
            <conversion-tli id="T803" />
            <conversion-tli id="T804" />
            <conversion-tli id="T805" />
            <conversion-tli id="T806" />
            <conversion-tli id="T807" />
            <conversion-tli id="T808" />
            <conversion-tli id="T809" />
            <conversion-tli id="T810" />
            <conversion-tli id="T811" />
            <conversion-tli id="T812" />
            <conversion-tli id="T813" />
            <conversion-tli id="T814" />
            <conversion-tli id="T815" />
            <conversion-tli id="T816" />
            <conversion-tli id="T817" />
            <conversion-tli id="T818" />
            <conversion-tli id="T819" />
            <conversion-tli id="T820" />
            <conversion-tli id="T821" />
            <conversion-tli id="T822" />
            <conversion-tli id="T823" />
            <conversion-tli id="T824" />
            <conversion-tli id="T825" />
            <conversion-tli id="T826" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg"
                          name="gg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top"
                          name="Top"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc"
                          name="Foc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
