<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID8B40DC6A-F5C8-63FC-B616-AFE32CE3EEB3">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>AkEE_1990_PearlBeard_flk</transcription-name>
         <referenced-file url="AkEE_19900810_PearlBeard_flk.wav" />
         <referenced-file url="AkEE_19900810_PearlBeard_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\flk\AkEE_19900810_PearlBeard_flk\AkEE_19900810_PearlBeard_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">837</ud-information>
            <ud-information attribute-name="# HIAT:w">615</ud-information>
            <ud-information attribute-name="# e">615</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">1</ud-information>
            <ud-information attribute-name="# HIAT:u">99</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="AkEE">
            <abbreviation>AkEE</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.0" type="appl" />
         <tli id="T1" time="1.113" type="appl" />
         <tli id="T2" time="2.225" type="appl" />
         <tli id="T3" time="3.338" type="appl" />
         <tli id="T4" time="4.45" type="appl" />
         <tli id="T5" time="6.142995038625892" />
         <tli id="T6" time="6.21" type="appl" />
         <tli id="T7" time="6.857" type="appl" />
         <tli id="T8" time="7.504" type="appl" />
         <tli id="T9" time="8.424331070742692" />
         <tli id="T10" time="8.903" type="appl" />
         <tli id="T11" time="9.654" type="appl" />
         <tli id="T12" time="10.406" type="appl" />
         <tli id="T13" time="11.158" type="appl" />
         <tli id="T14" time="11.91" type="appl" />
         <tli id="T15" time="12.661" type="appl" />
         <tli id="T16" time="13.492999095028559" />
         <tli id="T17" time="14.248" type="appl" />
         <tli id="T18" time="15.083" type="appl" />
         <tli id="T19" time="15.918" type="appl" />
         <tli id="T20" time="16.753" type="appl" />
         <tli id="T21" time="17.661332800448502" />
         <tli id="T22" time="18.153" type="appl" />
         <tli id="T23" time="18.718" type="appl" />
         <tli id="T24" time="19.283" type="appl" />
         <tli id="T25" time="19.849" type="appl" />
         <tli id="T26" time="20.414" type="appl" />
         <tli id="T27" time="20.979" type="appl" />
         <tli id="T28" time="21.946484383257175" />
         <tli id="T29" time="22.288" type="appl" />
         <tli id="T30" time="23.032" type="appl" />
         <tli id="T31" time="23.776" type="appl" />
         <tli id="T32" time="24.52" type="appl" />
         <tli id="T33" time="25.1" type="appl" />
         <tli id="T34" time="25.681" type="appl" />
         <tli id="T35" time="26.474330890391197" />
         <tli id="T36" time="27.045" type="appl" />
         <tli id="T37" time="27.828" type="appl" />
         <tli id="T38" time="28.612" type="appl" />
         <tli id="T39" time="29.48266527872638" />
         <tli id="T40" time="30.068" type="appl" />
         <tli id="T41" time="30.739" type="appl" />
         <tli id="T42" time="31.411" type="appl" />
         <tli id="T43" time="32.083" type="appl" />
         <tli id="T44" time="32.754" type="appl" />
         <tli id="T45" time="33.579330471338714" />
         <tli id="T46" time="33.985" type="appl" />
         <tli id="T47" time="34.545" type="appl" />
         <tli id="T48" time="35.104" type="appl" />
         <tli id="T49" time="35.664" type="appl" />
         <tli id="T50" time="36.223" type="appl" />
         <tli id="T51" time="36.782" type="appl" />
         <tli id="T52" time="37.342" type="appl" />
         <tli id="T53" time="37.94099997329131" />
         <tli id="T54" time="38.686" type="appl" />
         <tli id="T55" time="39.446339032725604" />
         <tli id="T56" time="40.61966262065187" />
         <tli id="T57" time="40.626329231946904" />
         <tli id="T58" time="41.75298654080792" />
         <tli id="T59" time="42.15433086356676" />
         <tli id="T60" time="42.59" type="appl" />
         <tli id="T61" time="43.278" type="appl" />
         <tli id="T62" time="43.967" type="appl" />
         <tli id="T63" time="44.62933309311781" />
         <tli id="T64" time="45.284" type="appl" />
         <tli id="T65" time="45.911" type="appl" />
         <tli id="T66" time="46.48566858619947" />
         <tli id="T67" time="47.096" type="appl" />
         <tli id="T68" time="47.653" type="appl" />
         <tli id="T69" time="48.21" type="appl" />
         <tli id="T70" time="48.768" type="appl" />
         <tli id="T71" time="49.325" type="appl" />
         <tli id="T72" time="49.882" type="appl" />
         <tli id="T73" time="50.47899804855746" />
         <tli id="T74" time="51.096" type="appl" />
         <tli id="T75" time="51.753" type="appl" />
         <tli id="T76" time="52.41" type="appl" />
         <tli id="T77" time="53.067" type="appl" />
         <tli id="T78" time="53.724" type="appl" />
         <tli id="T79" time="54.82621129037272" />
         <tli id="T80" time="57.33285713730611" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" time="57.379523416371356" />
         <tli id="T84" time="58.41951477839691" />
         <tli id="T85" time="58.642331938135754" />
         <tli id="T86" time="59.432" type="appl" />
         <tli id="T87" time="60.57949683798844" />
         <tli id="T88" time="61.11" type="appl" />
         <tli id="T89" time="61.844" type="appl" />
         <tli id="T90" time="62.578" type="appl" />
         <tli id="T91" time="63.312" type="appl" />
         <tli id="T92" time="64.046" type="appl" />
         <tli id="T93" time="64.78" type="appl" />
         <tli id="T94" time="65.68066540613293" />
         <tli id="T95" time="66.125" type="appl" />
         <tli id="T96" time="66.736" type="appl" />
         <tli id="T97" time="67.347" type="appl" />
         <tli id="T98" time="67.958" type="appl" />
         <tli id="T99" time="68.57" type="appl" />
         <tli id="T100" time="69.181" type="appl" />
         <tli id="T101" time="69.792" type="appl" />
         <tli id="T102" time="70.403" type="appl" />
         <tli id="T103" time="71.50607275055177" />
         <tli id="T104" time="71.695" type="appl" />
         <tli id="T105" time="72.376" type="appl" />
         <tli id="T106" time="73.058" type="appl" />
         <tli id="T107" time="73.8256628594686" />
         <tli id="T108" time="74.437" type="appl" />
         <tli id="T109" time="75.135" type="appl" />
         <tli id="T110" time="75.834" type="appl" />
         <tli id="T111" time="76.532" type="appl" />
         <tli id="T112" time="77.38333513302234" />
         <tli id="T113" time="77.871" type="appl" />
         <tli id="T114" time="78.511" type="appl" />
         <tli id="T115" time="79.20533171860822" />
         <tli id="T116" time="79.619" type="appl" />
         <tli id="T117" time="80.087" type="appl" />
         <tli id="T118" time="80.554" type="appl" />
         <tli id="T119" time="81.022" type="appl" />
         <tli id="T120" time="81.58233020486843" />
         <tli id="T121" time="82.069" type="appl" />
         <tli id="T122" time="82.65" type="appl" />
         <tli id="T123" time="83.23" type="appl" />
         <tli id="T124" time="83.811" type="appl" />
         <tli id="T125" time="84.75099529277671" />
         <tli id="T126" time="85.001" type="appl" />
         <tli id="T127" time="85.611" type="appl" />
         <tli id="T128" time="86.222" type="appl" />
         <tli id="T129" time="86.832" type="appl" />
         <tli id="T130" time="87.442" type="appl" />
         <tli id="T131" time="88.42533326399356" />
         <tli id="T132" time="88.554" type="appl" />
         <tli id="T133" time="89.057" type="appl" />
         <tli id="T134" time="89.559" type="appl" />
         <tli id="T135" time="90.062" type="appl" />
         <tli id="T136" time="90.58400023034136" />
         <tli id="T137" time="91.197" type="appl" />
         <tli id="T138" time="91.83" type="appl" />
         <tli id="T139" time="92.463" type="appl" />
         <tli id="T140" time="93.096" type="appl" />
         <tli id="T141" time="93.729" type="appl" />
         <tli id="T142" time="94.362" type="appl" />
         <tli id="T143" time="95.16166403203555" />
         <tli id="T144" time="95.532" type="appl" />
         <tli id="T145" time="96.068" type="appl" />
         <tli id="T146" time="96.604" type="appl" />
         <tli id="T147" time="97.141" type="appl" />
         <tli id="T148" time="97.678" type="appl" />
         <tli id="T149" time="98.32733435315112" />
         <tli id="T150" time="98.885" type="appl" />
         <tli id="T151" time="99.557" type="appl" />
         <tli id="T152" time="100.228" type="appl" />
         <tli id="T153" time="100.9" type="appl" />
         <tli id="T154" time="101.571" type="appl" />
         <tli id="T155" time="102.243" type="appl" />
         <tli id="T156" time="103.46066411250409" />
         <tli id="T157" time="103.658" type="appl" />
         <tli id="T158" time="104.402" type="appl" />
         <tli id="T159" time="105.145" type="appl" />
         <tli id="T160" time="105.889" type="appl" />
         <tli id="T161" time="106.702" type="appl" />
         <tli id="T162" time="107.514" type="appl" />
         <tli id="T163" time="108.327" type="appl" />
         <tli id="T164" time="109.14" type="appl" />
         <tli id="T165" time="109.952" type="appl" />
         <tli id="T166" time="110.765" type="appl" />
         <tli id="T167" time="111.577" type="appl" />
         <tli id="T168" time="112.85000018892838" />
         <tli id="T169" time="113.159" type="appl" />
         <tli id="T170" time="113.928" type="appl" />
         <tli id="T171" time="114.696" type="appl" />
         <tli id="T172" time="115.465" type="appl" />
         <tli id="T173" time="116.234" type="appl" />
         <tli id="T174" time="117.002" type="appl" />
         <tli id="T175" time="117.771" type="appl" />
         <tli id="T176" time="118.93901211473003" />
         <tli id="T177" time="119.424" type="appl" />
         <tli id="T178" time="120.309" type="appl" />
         <tli id="T179" time="121.193" type="appl" />
         <tli id="T180" time="121.961" type="appl" />
         <tli id="T181" time="122.729" type="appl" />
         <tli id="T182" time="123.497" type="appl" />
         <tli id="T183" time="124.265" type="appl" />
         <tli id="T184" time="125.09" type="appl" />
         <tli id="T185" time="125.915" type="appl" />
         <tli id="T186" time="126.74" type="appl" />
         <tli id="T187" time="127.565" type="appl" />
         <tli id="T188" time="128.39" type="appl" />
         <tli id="T189" time="129.072" type="appl" />
         <tli id="T190" time="129.755" type="appl" />
         <tli id="T191" time="130.437" type="appl" />
         <tli id="T192" time="131.12" type="appl" />
         <tli id="T193" time="131.802" type="appl" />
         <tli id="T194" time="132.543" type="appl" />
         <tli id="T195" time="133.284" type="appl" />
         <tli id="T196" time="134.024" type="appl" />
         <tli id="T197" time="134.88499946199735" />
         <tli id="T198" time="135.557" type="appl" />
         <tli id="T199" time="136.35" type="appl" />
         <tli id="T200" time="137.142" type="appl" />
         <tli id="T201" time="137.935" type="appl" />
         <tli id="T202" time="139.3655091227191" />
         <tli id="T203" time="139.812" type="appl" />
         <tli id="T204" time="140.898" type="appl" />
         <tli id="T205" time="141.984" type="appl" />
         <tli id="T206" time="143.069" type="appl" />
         <tli id="T207" time="144.154" type="appl" />
         <tli id="T208" time="145.29333749144797" />
         <tli id="T209" time="146.123" type="appl" />
         <tli id="T210" time="147.006" type="appl" />
         <tli id="T211" time="147.889" type="appl" />
         <tli id="T212" time="148.772" type="appl" />
         <tli id="T213" time="149.654" type="appl" />
         <tli id="T214" time="150.537" type="appl" />
         <tli id="T215" time="151.42" type="appl" />
         <tli id="T216" time="152.303" type="appl" />
         <tli id="T217" time="153.173" type="appl" />
         <tli id="T218" time="154.044" type="appl" />
         <tli id="T219" time="154.914" type="appl" />
         <tli id="T220" time="155.784" type="appl" />
         <tli id="T221" time="156.654" type="appl" />
         <tli id="T222" time="157.524" type="appl" />
         <tli id="T223" time="158.395" type="appl" />
         <tli id="T224" time="159.46500363839655" />
         <tli id="T225" time="159.993" type="appl" />
         <tli id="T226" time="160.72" type="appl" />
         <tli id="T227" time="161.448" type="appl" />
         <tli id="T228" time="162.175" type="appl" />
         <tli id="T229" time="162.903" type="appl" />
         <tli id="T230" time="163.465" type="appl" />
         <tli id="T231" time="164.028" type="appl" />
         <tli id="T232" time="164.59" type="appl" />
         <tli id="T233" time="165.153" type="appl" />
         <tli id="T234" time="165.715" type="appl" />
         <tli id="T235" time="166.278" type="appl" />
         <tli id="T236" time="166.84" type="appl" />
         <tli id="T237" time="167.403" type="appl" />
         <tli id="T238" time="168.20499614981415" />
         <tli id="T239" time="168.604" type="appl" />
         <tli id="T240" time="169.243" type="appl" />
         <tli id="T241" time="169.882" type="appl" />
         <tli id="T242" time="170.521" type="appl" />
         <tli id="T243" time="171.16" type="appl" />
         <tli id="T244" time="171.894" type="appl" />
         <tli id="T245" time="172.628" type="appl" />
         <tli id="T246" time="173.362" type="appl" />
         <tli id="T247" time="174.095" type="appl" />
         <tli id="T248" time="174.829" type="appl" />
         <tli id="T249" time="175.563" type="appl" />
         <tli id="T250" time="176.4369980888401" />
         <tli id="T251" time="176.946" type="appl" />
         <tli id="T252" time="177.596" type="appl" />
         <tli id="T253" time="178.245" type="appl" />
         <tli id="T254" time="178.894" type="appl" />
         <tli id="T255" time="179.544" type="appl" />
         <tli id="T256" time="180.193" type="appl" />
         <tli id="T257" time="180.842" type="appl" />
         <tli id="T258" time="181.492" type="appl" />
         <tli id="T259" time="182.20766630457862" />
         <tli id="T260" time="182.889" type="appl" />
         <tli id="T261" time="183.637" type="appl" />
         <tli id="T262" time="184.385" type="appl" />
         <tli id="T263" time="185.134" type="appl" />
         <tli id="T264" time="185.882" type="appl" />
         <tli id="T265" time="186.63" type="appl" />
         <tli id="T266" time="187.56466608117012" />
         <tli id="T267" time="188.422" type="appl" />
         <tli id="T268" time="189.53267056862492" />
         <tli id="T269" time="190.098" type="appl" />
         <tli id="T270" time="190.729" type="appl" />
         <tli id="T271" time="191.361" type="appl" />
         <tli id="T272" time="191.993" type="appl" />
         <tli id="T273" time="192.625" type="appl" />
         <tli id="T274" time="193.256" type="appl" />
         <tli id="T275" time="194.13466359532842" />
         <tli id="T276" time="194.538" type="appl" />
         <tli id="T277" time="195.189" type="appl" />
         <tli id="T278" time="195.839" type="appl" />
         <tli id="T279" time="196.49" type="appl" />
         <tli id="T280" time="197.14" type="appl" />
         <tli id="T281" time="197.791" type="appl" />
         <tli id="T282" time="198.441" type="appl" />
         <tli id="T283" time="199.092" type="appl" />
         <tli id="T284" time="199.742" type="appl" />
         <tli id="T285" time="200.393" type="appl" />
         <tli id="T286" time="201.04967126473002" />
         <tli id="T287" time="201.696" type="appl" />
         <tli id="T288" time="202.348" type="appl" />
         <tli id="T289" time="203.001" type="appl" />
         <tli id="T290" time="203.69399825977612" />
         <tli id="T291" time="204.32" type="appl" />
         <tli id="T292" time="204.985" type="appl" />
         <tli id="T293" time="205.65" type="appl" />
         <tli id="T294" time="206.316" type="appl" />
         <tli id="T295" time="206.982" type="appl" />
         <tli id="T296" time="207.647" type="appl" />
         <tli id="T297" time="208.312" type="appl" />
         <tli id="T298" time="209.09800285428767" />
         <tli id="T299" time="209.558" type="appl" />
         <tli id="T300" time="210.138" type="appl" />
         <tli id="T301" time="210.718" type="appl" />
         <tli id="T302" time="211.297" type="appl" />
         <tli id="T303" time="211.877" type="appl" />
         <tli id="T304" time="212.6636633427428" />
         <tli id="T305" time="213.247" type="appl" />
         <tli id="T306" time="214.037" type="appl" />
         <tli id="T307" time="214.826" type="appl" />
         <tli id="T308" time="215.616" type="appl" />
         <tli id="T309" time="216.406" type="appl" />
         <tli id="T310" time="217.196" type="appl" />
         <tli id="T311" time="217.986" type="appl" />
         <tli id="T312" time="218.775" type="appl" />
         <tli id="T313" time="219.565" type="appl" />
         <tli id="T314" time="220.355" type="appl" />
         <tli id="T315" time="221.145" type="appl" />
         <tli id="T316" time="221.935" type="appl" />
         <tli id="T317" time="222.724" type="appl" />
         <tli id="T318" time="223.514" type="appl" />
         <tli id="T319" time="224.69732641421317" />
         <tli id="T320" time="225.022" type="appl" />
         <tli id="T321" time="225.74" type="appl" />
         <tli id="T322" time="226.457" type="appl" />
         <tli id="T323" time="227.175" type="appl" />
         <tli id="T324" time="227.893" type="appl" />
         <tli id="T325" time="229.00432189111135" />
         <tli id="T326" time="229.164" type="appl" />
         <tli id="T327" time="229.718" type="appl" />
         <tli id="T328" time="230.271" type="appl" />
         <tli id="T329" time="230.824" type="appl" />
         <tli id="T330" time="231.378" type="appl" />
         <tli id="T331" time="231.931" type="appl" />
         <tli id="T332" time="232.484" type="appl" />
         <tli id="T333" time="233.038" type="appl" />
         <tli id="T334" time="233.86433360809306" />
         <tli id="T335" time="234.316" type="appl" />
         <tli id="T336" time="235.041" type="appl" />
         <tli id="T337" time="235.766" type="appl" />
         <tli id="T338" time="236.491" type="appl" />
         <tli id="T339" time="237.216" type="appl" />
         <tli id="T340" time="238.62468469450403" />
         <tli id="T341" time="238.645" type="appl" />
         <tli id="T342" time="239.349" type="appl" />
         <tli id="T343" time="240.053" type="appl" />
         <tli id="T344" time="240.758" type="appl" />
         <tli id="T345" time="241.462" type="appl" />
         <tli id="T346" time="242.19934250406243" />
         <tli id="T347" time="242.712" type="appl" />
         <tli id="T348" time="243.258" type="appl" />
         <tli id="T349" time="243.804" type="appl" />
         <tli id="T350" time="244.48333395032176" />
         <tli id="T351" time="245.252" type="appl" />
         <tli id="T352" time="246.6979509727921" />
         <tli id="T353" time="247.087" type="appl" />
         <tli id="T354" time="248.021" type="appl" />
         <tli id="T355" time="248.954" type="appl" />
         <tli id="T356" time="249.887" type="appl" />
         <tli id="T357" time="250.821" type="appl" />
         <tli id="T358" time="252.15123901213124" />
         <tli id="T359" time="252.291" type="appl" />
         <tli id="T360" time="252.828" type="appl" />
         <tli id="T361" time="253.365" type="appl" />
         <tli id="T362" time="253.901" type="appl" />
         <tli id="T363" time="254.438" type="appl" />
         <tli id="T364" time="254.975" type="appl" />
         <tli id="T365" time="255.6119915177659" />
         <tli id="T366" time="256.192" type="appl" />
         <tli id="T367" time="256.871" type="appl" />
         <tli id="T368" time="257.551" type="appl" />
         <tli id="T369" time="258.231" type="appl" />
         <tli id="T370" time="258.91" type="appl" />
         <tli id="T371" time="259.59" type="appl" />
         <tli id="T372" time="260.27" type="appl" />
         <tli id="T373" time="260.949" type="appl" />
         <tli id="T374" time="262.0111571174889" />
         <tli id="T375" time="262.324" type="appl" />
         <tli id="T376" time="263.018" type="appl" />
         <tli id="T377" time="263.713" type="appl" />
         <tli id="T378" time="264.48134493390256" />
         <tli id="T379" time="265.275" type="appl" />
         <tli id="T380" time="266.143" type="appl" />
         <tli id="T381" time="267.04999547416986" />
         <tli id="T382" time="267.647" type="appl" />
         <tli id="T383" time="268.285" type="appl" />
         <tli id="T384" time="269.00867712239176" />
         <tli id="T385" time="269.471" type="appl" />
         <tli id="T386" time="270.021" type="appl" />
         <tli id="T387" time="270.57" type="appl" />
         <tli id="T388" time="271.119" type="appl" />
         <tli id="T389" time="271.668" type="appl" />
         <tli id="T390" time="272.218" type="appl" />
         <tli id="T391" time="272.74033883623827" />
         <tli id="T392" time="273.323" type="appl" />
         <tli id="T393" time="273.88" type="appl" />
         <tli id="T394" time="274.436" type="appl" />
         <tli id="T395" time="274.993" type="appl" />
         <tli id="T396" time="275.549" type="appl" />
         <tli id="T397" time="276.106" type="appl" />
         <tli id="T398" time="276.662" type="appl" />
         <tli id="T399" time="277.3923314476545" />
         <tli id="T400" time="277.751" type="appl" />
         <tli id="T401" time="278.282" type="appl" />
         <tli id="T402" time="278.814" type="appl" />
         <tli id="T403" time="279.346" type="appl" />
         <tli id="T404" time="279.877" type="appl" />
         <tli id="T405" time="280.66233032931984" />
         <tli id="T406" time="281.013" type="appl" />
         <tli id="T407" time="281.616" type="appl" />
         <tli id="T408" time="282.22" type="appl" />
         <tli id="T409" time="282.824" type="appl" />
         <tli id="T410" time="283.427" type="appl" />
         <tli id="T411" time="284.031" type="appl" />
         <tli id="T412" time="284.635" type="appl" />
         <tli id="T413" time="285.238" type="appl" />
         <tli id="T414" time="285.86866729478857" />
         <tli id="T415" time="286.499" type="appl" />
         <tli id="T416" time="287.156" type="appl" />
         <tli id="T417" time="287.812" type="appl" />
         <tli id="T418" time="288.469" type="appl" />
         <tli id="T419" time="289.7642599387221" />
         <tli id="T420" time="289.984" type="appl" />
         <tli id="T421" time="290.842" type="appl" />
         <tli id="T422" time="291.701" type="appl" />
         <tli id="T423" time="292.559" type="appl" />
         <tli id="T424" time="293.417" type="appl" />
         <tli id="T425" time="294.276" type="appl" />
         <tli id="T426" time="295.134" type="appl" />
         <tli id="T427" time="296.5375370144782" />
         <tli id="T428" time="296.73" type="appl" />
         <tli id="T429" time="297.467" type="appl" />
         <tli id="T430" time="298.205" type="appl" />
         <tli id="T431" time="298.942" type="appl" />
         <tli id="T432" time="299.68" type="appl" />
         <tli id="T433" time="300.417" type="appl" />
         <tli id="T434" time="301.48416259539465" />
         <tli id="T435" time="301.824" type="appl" />
         <tli id="T436" time="302.492" type="appl" />
         <tli id="T437" time="303.161" type="appl" />
         <tli id="T438" time="304.61746990406135" />
         <tli id="T439" time="304.802" type="appl" />
         <tli id="T440" time="305.775" type="appl" />
         <tli id="T441" time="307.08411608322456" />
         <tli id="T442" time="307.246" type="appl" />
         <tli id="T443" time="307.745" type="appl" />
         <tli id="T444" time="308.244" type="appl" />
         <tli id="T445" time="308.742" type="appl" />
         <tli id="T446" time="309.241" type="appl" />
         <tli id="T447" time="309.74" type="appl" />
         <tli id="T448" time="310.239" type="appl" />
         <tli id="T449" time="310.738" type="appl" />
         <tli id="T450" time="311.236" type="appl" />
         <tli id="T451" time="311.735" type="appl" />
         <tli id="T452" time="312.234" type="appl" />
         <tli id="T453" time="312.9374008002658" />
         <tli id="T454" time="313.445" type="appl" />
         <tli id="T455" time="314.157" type="appl" />
         <tli id="T456" time="314.869" type="appl" />
         <tli id="T457" time="315.581" type="appl" />
         <tli id="T458" time="316.293" type="appl" />
         <tli id="T459" time="317.0983297820317" />
         <tli id="T460" time="317.694" type="appl" />
         <tli id="T461" time="318.383" type="appl" />
         <tli id="T462" time="319.071" type="appl" />
         <tli id="T463" time="319.76" type="appl" />
         <tli id="T464" time="320.5556708576677" />
         <tli id="T465" time="321.004" type="appl" />
         <tli id="T466" time="321.56" type="appl" />
         <tli id="T467" time="322.116" type="appl" />
         <tli id="T468" time="322.671" type="appl" />
         <tli id="T469" time="323.226" type="appl" />
         <tli id="T470" time="323.782" type="appl" />
         <tli id="T471" time="324.338" type="appl" />
         <tli id="T472" time="324.893" type="appl" />
         <tli id="T473" time="325.404" type="appl" />
         <tli id="T474" time="325.915" type="appl" />
         <tli id="T475" time="326.426" type="appl" />
         <tli id="T476" time="326.938" type="appl" />
         <tli id="T477" time="327.449" type="appl" />
         <tli id="T478" time="328.1706076094221" />
         <tli id="T479" time="328.688" type="appl" />
         <tli id="T480" time="329.415" type="appl" />
         <tli id="T481" time="330.142" type="appl" />
         <tli id="T482" time="330.87" type="appl" />
         <tli id="T483" time="331.411" type="appl" />
         <tli id="T484" time="331.952" type="appl" />
         <tli id="T485" time="332.492" type="appl" />
         <tli id="T486" time="333.033" type="appl" />
         <tli id="T487" time="333.673" type="appl" />
         <tli id="T488" time="334.313" type="appl" />
         <tli id="T489" time="334.953" type="appl" />
         <tli id="T490" time="335.593" type="appl" />
         <tli id="T491" time="336.233" type="appl" />
         <tli id="T492" time="336.873" type="appl" />
         <tli id="T493" time="337.513" type="appl" />
         <tli id="T494" time="338.153" type="appl" />
         <tli id="T495" time="338.83965961926435" />
         <tli id="T496" time="339.309" type="appl" />
         <tli id="T497" time="339.824" type="appl" />
         <tli id="T498" time="340.42667768778796" />
         <tli id="T499" time="341.21" type="appl" />
         <tli id="T500" time="341.9733315082363" />
         <tli id="T501" time="342.673" type="appl" />
         <tli id="T502" time="343.265" type="appl" />
         <tli id="T503" time="343.858" type="appl" />
         <tli id="T504" time="344.45" type="appl" />
         <tli id="T505" time="345.1363260703262" />
         <tli id="T506" time="345.828" type="appl" />
         <tli id="T507" time="346.612" type="appl" />
         <tli id="T508" time="347.397" type="appl" />
         <tli id="T509" time="348.182" type="appl" />
         <tli id="T510" time="348.966" type="appl" />
         <tli id="T511" time="349.751" type="appl" />
         <tli id="T512" time="350.726" type="appl" />
         <tli id="T513" time="351.6666624555691" />
         <tli id="T514" time="352.222" type="appl" />
         <tli id="T515" time="352.744" type="appl" />
         <tli id="T516" time="353.266" type="appl" />
         <tli id="T517" time="353.788" type="appl" />
         <tli id="T518" time="354.31" type="appl" />
         <tli id="T519" time="354.833" type="appl" />
         <tli id="T520" time="355.355" type="appl" />
         <tli id="T521" time="355.877" type="appl" />
         <tli id="T522" time="356.399" type="appl" />
         <tli id="T523" time="356.921" type="appl" />
         <tli id="T524" time="358.3903566098184" />
         <tli id="T525" time="359.017" type="appl" />
         <tli id="T526" time="359.555" type="appl" />
         <tli id="T527" time="360.092" type="appl" />
         <tli id="T528" time="360.63" type="appl" />
         <tli id="T529" time="361.168" type="appl" />
         <tli id="T530" time="361.706" type="appl" />
         <tli id="T531" time="362.295" type="appl" />
         <tli id="T532" time="362.885" type="appl" />
         <tli id="T533" time="363.474" type="appl" />
         <tli id="T534" time="364.063" type="appl" />
         <tli id="T535" time="364.652" type="appl" />
         <tli id="T536" time="365.242" type="appl" />
         <tli id="T537" time="365.831" type="appl" />
         <tli id="T538" time="366.5" type="appl" />
         <tli id="T539" time="367.168" type="appl" />
         <tli id="T540" time="367.837" type="appl" />
         <tli id="T541" time="368.506" type="appl" />
         <tli id="T542" time="369.174" type="appl" />
         <tli id="T543" time="369.843" type="appl" />
         <tli id="T544" time="370.599" type="appl" />
         <tli id="T545" time="371.356" type="appl" />
         <tli id="T546" time="372.112" type="appl" />
         <tli id="T547" time="372.868" type="appl" />
         <tli id="T548" time="373.625" type="appl" />
         <tli id="T549" time="374.4476711574228" />
         <tli id="T550" time="375.514" type="appl" />
         <tli id="T551" time="376.648" type="appl" />
         <tli id="T552" time="377.782" type="appl" />
         <tli id="T553" time="378.915" type="appl" />
         <tli id="T554" time="379.51" type="appl" />
         <tli id="T555" time="380.105" type="appl" />
         <tli id="T556" time="380.7" type="appl" />
         <tli id="T557" time="381.296" type="appl" />
         <tli id="T558" time="381.891" type="appl" />
         <tli id="T559" time="382.486" type="appl" />
         <tli id="T560" time="383.22767635733584" />
         <tli id="T561" time="383.652" type="appl" />
         <tli id="T562" time="384.224" type="appl" />
         <tli id="T563" time="384.795" type="appl" />
         <tli id="T564" time="385.367" type="appl" />
         <tli id="T565" time="385.938" type="appl" />
         <tli id="T566" time="386.51" type="appl" />
         <tli id="T567" time="387.3876678468884" />
         <tli id="T568" time="387.765" type="appl" />
         <tli id="T569" time="388.449" type="appl" />
         <tli id="T570" time="389.132" type="appl" />
         <tli id="T571" time="389.816" type="appl" />
         <tli id="T572" time="390.5" type="appl" />
         <tli id="T573" time="391.184" type="appl" />
         <tli id="T574" time="391.868" type="appl" />
         <tli id="T575" time="392.551" type="appl" />
         <tli id="T576" time="393.235" type="appl" />
         <tli id="T577" time="394.01232636537594" />
         <tli id="T578" time="394.706" type="appl" />
         <tli id="T579" time="395.494" type="appl" />
         <tli id="T580" time="396.282" type="appl" />
         <tli id="T581" time="397.069" type="appl" />
         <tli id="T582" time="397.856" type="appl" />
         <tli id="T583" time="399.3033501274519" />
         <tli id="T584" time="399.732" type="appl" />
         <tli id="T585" time="400.819" type="appl" />
         <tli id="T586" time="401.907" type="appl" />
         <tli id="T587" time="403.3006606765354" />
         <tli id="T588" time="403.909" type="appl" />
         <tli id="T589" time="404.824" type="appl" />
         <tli id="T590" time="405.739" type="appl" />
         <tli id="T591" time="406.654" type="appl" />
         <tli id="T592" time="407.57" type="appl" />
         <tli id="T593" time="408.485" type="appl" />
         <tli id="T594" time="409.4" type="appl" />
         <tli id="T595" time="410.315" type="appl" />
         <tli id="T596" time="411.4433274200628" />
         <tli id="T597" time="412.007" type="appl" />
         <tli id="T598" time="412.784" type="appl" />
         <tli id="T599" time="413.561" type="appl" />
         <tli id="T600" time="414.338" type="appl" />
         <tli id="T601" time="415.114" type="appl" />
         <tli id="T602" time="415.891" type="appl" />
         <tli id="T603" time="416.668" type="appl" />
         <tli id="T604" time="417.445" type="appl" />
         <tli id="T605" time="418.222" type="appl" />
         <tli id="T606" time="418.999" type="appl" />
         <tli id="T607" time="419.776" type="appl" />
         <tli id="T608" time="420.552" type="appl" />
         <tli id="T609" time="421.329" type="appl" />
         <tli id="T610" time="422.106" type="appl" />
         <tli id="T611" time="422.883" type="appl" />
         <tli id="T612" time="423.91332801322835" />
         <tli id="T613" time="425.417" type="appl" />
         <tli id="T614" time="427.174" type="appl" />
         <tli id="T615" time="428.931" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="AkEE"
                      type="t">
         <timeline-fork end="T121" start="T120">
            <tli id="T120.tx.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T615" id="Seg_0" n="sc" s="T0">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Ihilleːŋ</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">oloŋkonu</ts>
                  <nts id="Seg_8" n="HIAT:ip">,</nts>
                  <nts id="Seg_9" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_11" n="HIAT:w" s="T2">aːta</ts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_13" n="HIAT:ip">"</nts>
                  <ts e="T4" id="Seg_15" n="HIAT:w" s="T3">Oguru͡o</ts>
                  <nts id="Seg_16" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_18" n="HIAT:w" s="T4">bɨtɨk</ts>
                  <nts id="Seg_19" n="HIAT:ip">"</nts>
                  <nts id="Seg_20" n="HIAT:ip">.</nts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T9" id="Seg_23" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_25" n="HIAT:w" s="T5">Biːr</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_28" n="HIAT:w" s="T6">ogonnʼor</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_31" n="HIAT:w" s="T7">baːr</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_34" n="HIAT:w" s="T8">ebit</ts>
                  <nts id="Seg_35" n="HIAT:ip">.</nts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_38" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_40" n="HIAT:w" s="T9">Ogonnʼor</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_43" n="HIAT:w" s="T10">bɨtɨga</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_46" n="HIAT:w" s="T11">arɨttaːk</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_49" n="HIAT:w" s="T12">bu͡olan</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_52" n="HIAT:w" s="T13">baraːn</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_55" n="HIAT:w" s="T14">uhun</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_58" n="HIAT:w" s="T15">bagajɨ</ts>
                  <nts id="Seg_59" n="HIAT:ip">.</nts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_62" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_64" n="HIAT:w" s="T16">Bɨtɨktara</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_67" n="HIAT:w" s="T17">oguru͡olaːk</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_70" n="HIAT:w" s="T18">hap</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_73" n="HIAT:w" s="T19">kördük</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_76" n="HIAT:w" s="T20">gilbeŋniːller</ts>
                  <nts id="Seg_77" n="HIAT:ip">.</nts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T28" id="Seg_80" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_82" n="HIAT:w" s="T21">Ol</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_85" n="HIAT:w" s="T22">ihin</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_88" n="HIAT:w" s="T23">ogonnʼoru</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_91" n="HIAT:w" s="T24">aːttaːbɨttar</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_94" n="HIAT:w" s="T25">Oguru͡o</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_97" n="HIAT:w" s="T26">Bɨtɨk</ts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_100" n="HIAT:w" s="T27">di͡en</ts>
                  <nts id="Seg_101" n="HIAT:ip">.</nts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T32" id="Seg_104" n="HIAT:u" s="T28">
                  <ts e="T29" id="Seg_106" n="HIAT:w" s="T28">Oguru͡o</ts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_109" n="HIAT:w" s="T29">bɨtɨk</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_112" n="HIAT:w" s="T30">üs</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_115" n="HIAT:w" s="T31">kɨːstaːk</ts>
                  <nts id="Seg_116" n="HIAT:ip">.</nts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T35" id="Seg_119" n="HIAT:u" s="T32">
                  <ts e="T33" id="Seg_121" n="HIAT:w" s="T32">Kɨhɨn</ts>
                  <nts id="Seg_122" n="HIAT:ip">,</nts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_125" n="HIAT:w" s="T33">hajɨn</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_128" n="HIAT:w" s="T34">ilimnener</ts>
                  <nts id="Seg_129" n="HIAT:ip">.</nts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T39" id="Seg_132" n="HIAT:u" s="T35">
                  <ts e="T36" id="Seg_134" n="HIAT:w" s="T35">Balɨgɨnan</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_137" n="HIAT:w" s="T36">ere</ts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_140" n="HIAT:w" s="T37">iːtillen</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_143" n="HIAT:w" s="T38">olorollor</ts>
                  <nts id="Seg_144" n="HIAT:ip">.</nts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T45" id="Seg_147" n="HIAT:u" s="T39">
                  <ts e="T40" id="Seg_149" n="HIAT:w" s="T39">Biːrde</ts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_152" n="HIAT:w" s="T40">Oguru͡o</ts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_155" n="HIAT:w" s="T41">Bɨtɨk</ts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_158" n="HIAT:w" s="T42">ilim</ts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_161" n="HIAT:w" s="T43">körüne</ts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_164" n="HIAT:w" s="T44">barbɨt</ts>
                  <nts id="Seg_165" n="HIAT:ip">.</nts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T53" id="Seg_168" n="HIAT:u" s="T45">
                  <ts e="T46" id="Seg_170" n="HIAT:w" s="T45">Ojbonugar</ts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_173" n="HIAT:w" s="T46">bökčöjörün</ts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_176" n="HIAT:w" s="T47">kɨtta</ts>
                  <nts id="Seg_177" n="HIAT:ip">,</nts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_180" n="HIAT:w" s="T48">kim</ts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_183" n="HIAT:w" s="T49">ire</ts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_186" n="HIAT:w" s="T50">bɨtɨgɨttan</ts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_189" n="HIAT:w" s="T51">kaban</ts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_192" n="HIAT:w" s="T52">ɨlbɨt</ts>
                  <nts id="Seg_193" n="HIAT:ip">.</nts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T55" id="Seg_196" n="HIAT:u" s="T53">
                  <ts e="T54" id="Seg_198" n="HIAT:w" s="T53">Amattan</ts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_201" n="HIAT:w" s="T54">ɨːppat</ts>
                  <nts id="Seg_202" n="HIAT:ip">.</nts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T59" id="Seg_205" n="HIAT:u" s="T55">
                  <nts id="Seg_206" n="HIAT:ip">"</nts>
                  <ts e="T56" id="Seg_208" n="HIAT:w" s="T55">Kim</ts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_211" n="HIAT:w" s="T56">bu͡olu͡oj</ts>
                  <nts id="Seg_212" n="HIAT:ip">"</nts>
                  <nts id="Seg_213" n="HIAT:ip">,</nts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_216" n="HIAT:w" s="T57">diː</ts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_219" n="HIAT:w" s="T58">hanaːbɨt</ts>
                  <nts id="Seg_220" n="HIAT:ip">.</nts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T63" id="Seg_223" n="HIAT:u" s="T59">
                  <ts e="T60" id="Seg_225" n="HIAT:w" s="T59">Ojbonton</ts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_228" n="HIAT:w" s="T60">bɨkpɨt</ts>
                  <nts id="Seg_229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_231" n="HIAT:w" s="T61">uː</ts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_234" n="HIAT:w" s="T62">iččite</ts>
                  <nts id="Seg_235" n="HIAT:ip">.</nts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T73" id="Seg_238" n="HIAT:u" s="T63">
                  <nts id="Seg_239" n="HIAT:ip">"</nts>
                  <ts e="T64" id="Seg_241" n="HIAT:w" s="T63">Oguru͡o</ts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_244" n="HIAT:w" s="T64">Bɨtɨk</ts>
                  <nts id="Seg_245" n="HIAT:ip">"</nts>
                  <nts id="Seg_246" n="HIAT:ip">,</nts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_249" n="HIAT:w" s="T65">diːr</ts>
                  <nts id="Seg_250" n="HIAT:ip">,</nts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_252" n="HIAT:ip">"</nts>
                  <ts e="T67" id="Seg_254" n="HIAT:w" s="T66">kɨːskɨn</ts>
                  <nts id="Seg_255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_257" n="HIAT:w" s="T67">bi͡eregin</ts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_260" n="HIAT:w" s="T68">duː</ts>
                  <nts id="Seg_261" n="HIAT:ip">,</nts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_264" n="HIAT:w" s="T69">hu͡ok</ts>
                  <nts id="Seg_265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_267" n="HIAT:w" s="T70">duː</ts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_270" n="HIAT:w" s="T71">dʼaktar</ts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_273" n="HIAT:w" s="T72">gɨnɨ͡akpɨn</ts>
                  <nts id="Seg_274" n="HIAT:ip">?</nts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T79" id="Seg_277" n="HIAT:u" s="T73">
                  <ts e="T74" id="Seg_279" n="HIAT:w" s="T73">Bi͡erbetekkine</ts>
                  <nts id="Seg_280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_282" n="HIAT:w" s="T74">biːr</ts>
                  <nts id="Seg_283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_285" n="HIAT:w" s="T75">da</ts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_288" n="HIAT:w" s="T76">balɨgɨ</ts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_291" n="HIAT:w" s="T77">ɨlɨ͡aŋ</ts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_294" n="HIAT:w" s="T78">hu͡oga</ts>
                  <nts id="Seg_295" n="HIAT:ip">.</nts>
                  <nts id="Seg_296" n="HIAT:ip">"</nts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T85" id="Seg_299" n="HIAT:u" s="T79">
                  <nts id="Seg_300" n="HIAT:ip">"</nts>
                  <ts e="T80" id="Seg_302" n="HIAT:w" s="T79">Tu͡ogunan</ts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_305" n="HIAT:w" s="T80">kergetterbin</ts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_308" n="HIAT:w" s="T81">iːti͡emij</ts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_311" n="HIAT:w" s="T82">ke</ts>
                  <nts id="Seg_312" n="HIAT:ip">"</nts>
                  <nts id="Seg_313" n="HIAT:ip">,</nts>
                  <nts id="Seg_314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_316" n="HIAT:w" s="T83">diː</ts>
                  <nts id="Seg_317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_319" n="HIAT:w" s="T84">hanaːbɨt</ts>
                  <nts id="Seg_320" n="HIAT:ip">.</nts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T87" id="Seg_323" n="HIAT:u" s="T85">
                  <ts e="T86" id="Seg_325" n="HIAT:w" s="T85">Onton</ts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_328" n="HIAT:w" s="T86">eːktemmit</ts>
                  <nts id="Seg_329" n="HIAT:ip">.</nts>
                  <nts id="Seg_330" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T94" id="Seg_332" n="HIAT:u" s="T87">
                  <nts id="Seg_333" n="HIAT:ip">"</nts>
                  <ts e="T88" id="Seg_335" n="HIAT:w" s="T87">Albɨnnaːtakkɨna</ts>
                  <nts id="Seg_336" n="HIAT:ip">,</nts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_339" n="HIAT:w" s="T88">hin</ts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_342" n="HIAT:w" s="T89">biːr</ts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_345" n="HIAT:w" s="T90">bulu͡om</ts>
                  <nts id="Seg_346" n="HIAT:ip">"</nts>
                  <nts id="Seg_347" n="HIAT:ip">,</nts>
                  <nts id="Seg_348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_350" n="HIAT:w" s="T91">di͡ebit</ts>
                  <nts id="Seg_351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_353" n="HIAT:w" s="T92">uː</ts>
                  <nts id="Seg_354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_356" n="HIAT:w" s="T93">iččite</ts>
                  <nts id="Seg_357" n="HIAT:ip">.</nts>
                  <nts id="Seg_358" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T103" id="Seg_360" n="HIAT:u" s="T94">
                  <ts e="T95" id="Seg_362" n="HIAT:w" s="T94">Ogonnʼor</ts>
                  <nts id="Seg_363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_365" n="HIAT:w" s="T95">gereːpketin</ts>
                  <nts id="Seg_366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_368" n="HIAT:w" s="T96">kaban</ts>
                  <nts id="Seg_369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_371" n="HIAT:w" s="T97">ɨlan</ts>
                  <nts id="Seg_372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_374" n="HIAT:w" s="T98">baraːn</ts>
                  <nts id="Seg_375" n="HIAT:ip">,</nts>
                  <nts id="Seg_376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_378" n="HIAT:w" s="T99">buːs</ts>
                  <nts id="Seg_379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_381" n="HIAT:w" s="T100">ihinen</ts>
                  <nts id="Seg_382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_384" n="HIAT:w" s="T101">baran</ts>
                  <nts id="Seg_385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_387" n="HIAT:w" s="T102">kaːlbɨt</ts>
                  <nts id="Seg_388" n="HIAT:ip">.</nts>
                  <nts id="Seg_389" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T107" id="Seg_391" n="HIAT:u" s="T103">
                  <ts e="T104" id="Seg_393" n="HIAT:w" s="T103">Oguru͡o</ts>
                  <nts id="Seg_394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_396" n="HIAT:w" s="T104">Bɨtɨk</ts>
                  <nts id="Seg_397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_399" n="HIAT:w" s="T105">bert</ts>
                  <nts id="Seg_400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_402" n="HIAT:w" s="T106">hutaːbɨt</ts>
                  <nts id="Seg_403" n="HIAT:ip">.</nts>
                  <nts id="Seg_404" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T112" id="Seg_406" n="HIAT:u" s="T107">
                  <ts e="T108" id="Seg_408" n="HIAT:w" s="T107">Uraha</ts>
                  <nts id="Seg_409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_411" n="HIAT:w" s="T108">dʼi͡etiger</ts>
                  <nts id="Seg_412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_414" n="HIAT:w" s="T109">kelen</ts>
                  <nts id="Seg_415" n="HIAT:ip">,</nts>
                  <nts id="Seg_416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_418" n="HIAT:w" s="T110">kɨrgɨttarɨgar</ts>
                  <nts id="Seg_419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_421" n="HIAT:w" s="T111">kepseːbit</ts>
                  <nts id="Seg_422" n="HIAT:ip">.</nts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T115" id="Seg_425" n="HIAT:u" s="T112">
                  <ts e="T113" id="Seg_427" n="HIAT:w" s="T112">Ulakan</ts>
                  <nts id="Seg_428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_430" n="HIAT:w" s="T113">kɨːha</ts>
                  <nts id="Seg_431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_433" n="HIAT:w" s="T114">ü͡örbüt</ts>
                  <nts id="Seg_434" n="HIAT:ip">:</nts>
                  <nts id="Seg_435" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T120" id="Seg_437" n="HIAT:u" s="T115">
                  <nts id="Seg_438" n="HIAT:ip">"</nts>
                  <ts e="T116" id="Seg_440" n="HIAT:w" s="T115">Min</ts>
                  <nts id="Seg_441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_443" n="HIAT:w" s="T116">erge</ts>
                  <nts id="Seg_444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_446" n="HIAT:w" s="T117">barɨ͡am</ts>
                  <nts id="Seg_447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_449" n="HIAT:w" s="T118">uː</ts>
                  <nts id="Seg_450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_452" n="HIAT:w" s="T119">iččitiger</ts>
                  <nts id="Seg_453" n="HIAT:ip">.</nts>
                  <nts id="Seg_454" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T125" id="Seg_456" n="HIAT:u" s="T120">
                  <ts e="T120.tx.1" id="Seg_458" n="HIAT:w" s="T120">Aːn</ts>
                  <nts id="Seg_459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_461" n="HIAT:w" s="T120.tx.1">dojdu</ts>
                  <nts id="Seg_462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_464" n="HIAT:w" s="T121">ürdütünen</ts>
                  <nts id="Seg_465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_467" n="HIAT:w" s="T122">muŋ</ts>
                  <nts id="Seg_468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_470" n="HIAT:w" s="T123">baːj</ts>
                  <nts id="Seg_471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_473" n="HIAT:w" s="T124">bu͡olu͡om</ts>
                  <nts id="Seg_474" n="HIAT:ip">.</nts>
                  <nts id="Seg_475" n="HIAT:ip">"</nts>
                  <nts id="Seg_476" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T131" id="Seg_478" n="HIAT:u" s="T125">
                  <ts e="T126" id="Seg_480" n="HIAT:w" s="T125">Oguru͡o</ts>
                  <nts id="Seg_481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_483" n="HIAT:w" s="T126">Bɨtɨk</ts>
                  <nts id="Seg_484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_486" n="HIAT:w" s="T127">kɨːhɨn</ts>
                  <nts id="Seg_487" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_489" n="HIAT:w" s="T128">erge</ts>
                  <nts id="Seg_490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_492" n="HIAT:w" s="T129">bi͡eren</ts>
                  <nts id="Seg_493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_495" n="HIAT:w" s="T130">keːspit</ts>
                  <nts id="Seg_496" n="HIAT:ip">.</nts>
                  <nts id="Seg_497" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T136" id="Seg_499" n="HIAT:u" s="T131">
                  <ts e="T132" id="Seg_501" n="HIAT:w" s="T131">Uː</ts>
                  <nts id="Seg_502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_504" n="HIAT:w" s="T132">iččite</ts>
                  <nts id="Seg_505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_507" n="HIAT:w" s="T133">baːj</ts>
                  <nts id="Seg_508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_510" n="HIAT:w" s="T134">da</ts>
                  <nts id="Seg_511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_513" n="HIAT:w" s="T135">baːj</ts>
                  <nts id="Seg_514" n="HIAT:ip">.</nts>
                  <nts id="Seg_515" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T143" id="Seg_517" n="HIAT:u" s="T136">
                  <ts e="T137" id="Seg_519" n="HIAT:w" s="T136">Baːjɨn</ts>
                  <nts id="Seg_520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_521" n="HIAT:ip">(</nts>
                  <ts e="T138" id="Seg_523" n="HIAT:w" s="T137">uhuga</ts>
                  <nts id="Seg_524" n="HIAT:ip">)</nts>
                  <nts id="Seg_525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_527" n="HIAT:w" s="T138">köstübet</ts>
                  <nts id="Seg_528" n="HIAT:ip">,</nts>
                  <nts id="Seg_529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_531" n="HIAT:w" s="T139">töhö</ts>
                  <nts id="Seg_532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_534" n="HIAT:w" s="T140">emete</ts>
                  <nts id="Seg_535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_537" n="HIAT:w" s="T141">kostoːk</ts>
                  <nts id="Seg_538" n="HIAT:ip">,</nts>
                  <nts id="Seg_539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_541" n="HIAT:w" s="T142">dʼi͡eleːk</ts>
                  <nts id="Seg_542" n="HIAT:ip">.</nts>
                  <nts id="Seg_543" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T149" id="Seg_545" n="HIAT:u" s="T143">
                  <ts e="T144" id="Seg_547" n="HIAT:w" s="T143">Biːr</ts>
                  <nts id="Seg_548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_550" n="HIAT:w" s="T144">kosko</ts>
                  <nts id="Seg_551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_553" n="HIAT:w" s="T145">ulakan</ts>
                  <nts id="Seg_554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_556" n="HIAT:w" s="T146">buːs</ts>
                  <nts id="Seg_557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_559" n="HIAT:w" s="T147">kolbuja</ts>
                  <nts id="Seg_560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_562" n="HIAT:w" s="T148">turar</ts>
                  <nts id="Seg_563" n="HIAT:ip">.</nts>
                  <nts id="Seg_564" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T156" id="Seg_566" n="HIAT:u" s="T149">
                  <ts e="T150" id="Seg_568" n="HIAT:w" s="T149">Uː</ts>
                  <nts id="Seg_569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_571" n="HIAT:w" s="T150">iččite</ts>
                  <nts id="Seg_572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_574" n="HIAT:w" s="T151">dʼaktarɨn</ts>
                  <nts id="Seg_575" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_577" n="HIAT:w" s="T152">egelen</ts>
                  <nts id="Seg_578" n="HIAT:ip">,</nts>
                  <nts id="Seg_579" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_581" n="HIAT:w" s="T153">buːs</ts>
                  <nts id="Seg_582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_584" n="HIAT:w" s="T154">kolbujanɨ</ts>
                  <nts id="Seg_585" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_587" n="HIAT:w" s="T155">köllörbüt</ts>
                  <nts id="Seg_588" n="HIAT:ip">.</nts>
                  <nts id="Seg_589" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T160" id="Seg_591" n="HIAT:u" s="T156">
                  <nts id="Seg_592" n="HIAT:ip">"</nts>
                  <ts e="T157" id="Seg_594" n="HIAT:w" s="T156">Kanna</ts>
                  <nts id="Seg_595" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_597" n="HIAT:w" s="T157">hanɨːr</ts>
                  <nts id="Seg_598" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_600" n="HIAT:w" s="T158">hɨrɨt</ts>
                  <nts id="Seg_601" n="HIAT:ip">,</nts>
                  <nts id="Seg_602" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_604" n="HIAT:w" s="T159">kör</ts>
                  <nts id="Seg_605" n="HIAT:ip">.</nts>
                  <nts id="Seg_606" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T168" id="Seg_608" n="HIAT:u" s="T160">
                  <ts e="T161" id="Seg_610" n="HIAT:w" s="T160">Bu</ts>
                  <nts id="Seg_611" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_613" n="HIAT:w" s="T161">ere</ts>
                  <nts id="Seg_614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_615" n="HIAT:ip">(</nts>
                  <nts id="Seg_616" n="HIAT:ip">(</nts>
                  <ats e="T163" id="Seg_617" n="HIAT:non-pho" s="T162">PAUSE</ats>
                  <nts id="Seg_618" n="HIAT:ip">)</nts>
                  <nts id="Seg_619" n="HIAT:ip">)</nts>
                  <nts id="Seg_620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_622" n="HIAT:w" s="T163">kolbuja</ts>
                  <nts id="Seg_623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_625" n="HIAT:w" s="T164">ihiger</ts>
                  <nts id="Seg_626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_628" n="HIAT:w" s="T165">uguma</ts>
                  <nts id="Seg_629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_631" n="HIAT:w" s="T166">čömüjegin</ts>
                  <nts id="Seg_632" n="HIAT:ip">"</nts>
                  <nts id="Seg_633" n="HIAT:ip">,</nts>
                  <nts id="Seg_634" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_636" n="HIAT:w" s="T167">di͡ebit</ts>
                  <nts id="Seg_637" n="HIAT:ip">.</nts>
                  <nts id="Seg_638" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T176" id="Seg_640" n="HIAT:u" s="T168">
                  <ts e="T169" id="Seg_642" n="HIAT:w" s="T168">Ere</ts>
                  <nts id="Seg_643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_645" n="HIAT:w" s="T169">barbɨtɨn</ts>
                  <nts id="Seg_646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_648" n="HIAT:w" s="T170">kenne</ts>
                  <nts id="Seg_649" n="HIAT:ip">,</nts>
                  <nts id="Seg_650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_652" n="HIAT:w" s="T171">bu</ts>
                  <nts id="Seg_653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_655" n="HIAT:w" s="T172">dʼaktar</ts>
                  <nts id="Seg_656" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_658" n="HIAT:w" s="T173">tehijbekke</ts>
                  <nts id="Seg_659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_661" n="HIAT:w" s="T174">čömüjetin</ts>
                  <nts id="Seg_662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_664" n="HIAT:w" s="T175">ukput</ts>
                  <nts id="Seg_665" n="HIAT:ip">.</nts>
                  <nts id="Seg_666" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T179" id="Seg_668" n="HIAT:u" s="T176">
                  <ts e="T177" id="Seg_670" n="HIAT:w" s="T176">Uː</ts>
                  <nts id="Seg_671" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_673" n="HIAT:w" s="T177">buːs</ts>
                  <nts id="Seg_674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_676" n="HIAT:w" s="T178">bu͡olbut</ts>
                  <nts id="Seg_677" n="HIAT:ip">.</nts>
                  <nts id="Seg_678" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T183" id="Seg_680" n="HIAT:u" s="T179">
                  <ts e="T180" id="Seg_682" n="HIAT:w" s="T179">Araččɨ</ts>
                  <nts id="Seg_683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_685" n="HIAT:w" s="T180">hulbu</ts>
                  <nts id="Seg_686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_688" n="HIAT:w" s="T181">tarpɨt</ts>
                  <nts id="Seg_689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_691" n="HIAT:w" s="T182">čömüjetin</ts>
                  <nts id="Seg_692" n="HIAT:ip">.</nts>
                  <nts id="Seg_693" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T188" id="Seg_695" n="HIAT:u" s="T183">
                  <ts e="T184" id="Seg_697" n="HIAT:w" s="T183">Körbüte</ts>
                  <nts id="Seg_698" n="HIAT:ip">,</nts>
                  <nts id="Seg_699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_701" n="HIAT:w" s="T184">töjön</ts>
                  <nts id="Seg_702" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_704" n="HIAT:w" s="T185">čömüjete</ts>
                  <nts id="Seg_705" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_707" n="HIAT:w" s="T186">tuːra</ts>
                  <nts id="Seg_708" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_710" n="HIAT:w" s="T187">barbɨt</ts>
                  <nts id="Seg_711" n="HIAT:ip">.</nts>
                  <nts id="Seg_712" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T193" id="Seg_714" n="HIAT:u" s="T188">
                  <ts e="T189" id="Seg_716" n="HIAT:w" s="T188">Ontutun</ts>
                  <nts id="Seg_717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_719" n="HIAT:w" s="T189">kam</ts>
                  <nts id="Seg_720" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_722" n="HIAT:w" s="T190">baːjan</ts>
                  <nts id="Seg_723" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_725" n="HIAT:w" s="T191">baraːn</ts>
                  <nts id="Seg_726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_728" n="HIAT:w" s="T192">oloror</ts>
                  <nts id="Seg_729" n="HIAT:ip">.</nts>
                  <nts id="Seg_730" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T197" id="Seg_732" n="HIAT:u" s="T193">
                  <ts e="T194" id="Seg_734" n="HIAT:w" s="T193">Kuttanan</ts>
                  <nts id="Seg_735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_737" n="HIAT:w" s="T194">karaktara</ts>
                  <nts id="Seg_738" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_740" n="HIAT:w" s="T195">bɨlčaččɨ</ts>
                  <nts id="Seg_741" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_743" n="HIAT:w" s="T196">barbɨttar</ts>
                  <nts id="Seg_744" n="HIAT:ip">.</nts>
                  <nts id="Seg_745" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T202" id="Seg_747" n="HIAT:u" s="T197">
                  <ts e="T198" id="Seg_749" n="HIAT:w" s="T197">Uː</ts>
                  <nts id="Seg_750" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_752" n="HIAT:w" s="T198">iččite</ts>
                  <nts id="Seg_753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_755" n="HIAT:w" s="T199">kelen</ts>
                  <nts id="Seg_756" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_758" n="HIAT:w" s="T200">honno</ts>
                  <nts id="Seg_759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_761" n="HIAT:w" s="T201">taːjda</ts>
                  <nts id="Seg_762" n="HIAT:ip">.</nts>
                  <nts id="Seg_763" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T208" id="Seg_765" n="HIAT:u" s="T202">
                  <nts id="Seg_766" n="HIAT:ip">"</nts>
                  <ts e="T203" id="Seg_768" n="HIAT:w" s="T202">Mini͡eke</ts>
                  <nts id="Seg_769" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_771" n="HIAT:w" s="T203">en</ts>
                  <nts id="Seg_772" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_774" n="HIAT:w" s="T204">dʼaktar</ts>
                  <nts id="Seg_775" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_777" n="HIAT:w" s="T205">bu͡olbat</ts>
                  <nts id="Seg_778" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_780" n="HIAT:w" s="T206">kördükkün</ts>
                  <nts id="Seg_781" n="HIAT:ip">"</nts>
                  <nts id="Seg_782" n="HIAT:ip">,</nts>
                  <nts id="Seg_783" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_785" n="HIAT:w" s="T207">di͡ebit</ts>
                  <nts id="Seg_786" n="HIAT:ip">.</nts>
                  <nts id="Seg_787" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T216" id="Seg_789" n="HIAT:u" s="T208">
                  <ts e="T209" id="Seg_791" n="HIAT:w" s="T208">Dʼaktarɨ</ts>
                  <nts id="Seg_792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_794" n="HIAT:w" s="T209">muŋ</ts>
                  <nts id="Seg_795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_797" n="HIAT:w" s="T210">ɨraːktaːgɨ</ts>
                  <nts id="Seg_798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_800" n="HIAT:w" s="T211">dʼi͡ege</ts>
                  <nts id="Seg_801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_803" n="HIAT:w" s="T212">kohugar</ts>
                  <nts id="Seg_804" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_806" n="HIAT:w" s="T213">illen</ts>
                  <nts id="Seg_807" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_809" n="HIAT:w" s="T214">kataːn</ts>
                  <nts id="Seg_810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_812" n="HIAT:w" s="T215">keːspit</ts>
                  <nts id="Seg_813" n="HIAT:ip">.</nts>
                  <nts id="Seg_814" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T224" id="Seg_816" n="HIAT:u" s="T216">
                  <ts e="T217" id="Seg_818" n="HIAT:w" s="T216">Aːnɨn</ts>
                  <nts id="Seg_819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_821" n="HIAT:w" s="T217">čipičči</ts>
                  <nts id="Seg_822" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_824" n="HIAT:w" s="T218">katɨːs</ts>
                  <nts id="Seg_825" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_827" n="HIAT:w" s="T219">ü͡öhünen</ts>
                  <nts id="Seg_828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_830" n="HIAT:w" s="T220">oŋohullubut</ts>
                  <nts id="Seg_831" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_833" n="HIAT:w" s="T221">hiliminen</ts>
                  <nts id="Seg_834" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_836" n="HIAT:w" s="T222">kam</ts>
                  <nts id="Seg_837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_839" n="HIAT:w" s="T223">bispit</ts>
                  <nts id="Seg_840" n="HIAT:ip">.</nts>
                  <nts id="Seg_841" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T229" id="Seg_843" n="HIAT:u" s="T224">
                  <ts e="T225" id="Seg_845" n="HIAT:w" s="T224">Oguru͡o</ts>
                  <nts id="Seg_846" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_848" n="HIAT:w" s="T225">Bɨtɨk</ts>
                  <nts id="Seg_849" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_851" n="HIAT:w" s="T226">emi͡e</ts>
                  <nts id="Seg_852" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_854" n="HIAT:w" s="T227">ilimnene</ts>
                  <nts id="Seg_855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_857" n="HIAT:w" s="T228">kelbit</ts>
                  <nts id="Seg_858" n="HIAT:ip">.</nts>
                  <nts id="Seg_859" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T238" id="Seg_861" n="HIAT:u" s="T229">
                  <ts e="T230" id="Seg_863" n="HIAT:w" s="T229">Ojbonugar</ts>
                  <nts id="Seg_864" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_866" n="HIAT:w" s="T230">bökčöjörün</ts>
                  <nts id="Seg_867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_869" n="HIAT:w" s="T231">kɨtta</ts>
                  <nts id="Seg_870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_872" n="HIAT:w" s="T232">uː</ts>
                  <nts id="Seg_873" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_875" n="HIAT:w" s="T233">iččite</ts>
                  <nts id="Seg_876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_878" n="HIAT:w" s="T234">emi͡e</ts>
                  <nts id="Seg_879" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_881" n="HIAT:w" s="T235">bɨtɨgɨttan</ts>
                  <nts id="Seg_882" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_884" n="HIAT:w" s="T236">kaban</ts>
                  <nts id="Seg_885" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_887" n="HIAT:w" s="T237">ɨlbɨt</ts>
                  <nts id="Seg_888" n="HIAT:ip">.</nts>
                  <nts id="Seg_889" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T243" id="Seg_891" n="HIAT:u" s="T238">
                  <nts id="Seg_892" n="HIAT:ip">"</nts>
                  <ts e="T239" id="Seg_894" n="HIAT:w" s="T238">Egel</ts>
                  <nts id="Seg_895" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_897" n="HIAT:w" s="T239">ikkis</ts>
                  <nts id="Seg_898" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_900" n="HIAT:w" s="T240">kɨːskɨn</ts>
                  <nts id="Seg_901" n="HIAT:ip">,</nts>
                  <nts id="Seg_902" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_904" n="HIAT:w" s="T241">dʼaktar</ts>
                  <nts id="Seg_905" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_907" n="HIAT:w" s="T242">gɨnɨ͡akpɨn</ts>
                  <nts id="Seg_908" n="HIAT:ip">.</nts>
                  <nts id="Seg_909" n="HIAT:ip">"</nts>
                  <nts id="Seg_910" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T250" id="Seg_912" n="HIAT:u" s="T243">
                  <ts e="T244" id="Seg_914" n="HIAT:w" s="T243">Ogonnʼor</ts>
                  <nts id="Seg_915" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_917" n="HIAT:w" s="T244">muŋu</ts>
                  <nts id="Seg_918" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_920" n="HIAT:w" s="T245">bɨstɨ͡a</ts>
                  <nts id="Seg_921" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_923" n="HIAT:w" s="T246">du͡o</ts>
                  <nts id="Seg_924" n="HIAT:ip">,</nts>
                  <nts id="Seg_925" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_927" n="HIAT:w" s="T247">orto</ts>
                  <nts id="Seg_928" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_930" n="HIAT:w" s="T248">kɨːhɨn</ts>
                  <nts id="Seg_931" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_933" n="HIAT:w" s="T249">bi͡erbit</ts>
                  <nts id="Seg_934" n="HIAT:ip">.</nts>
                  <nts id="Seg_935" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T259" id="Seg_937" n="HIAT:u" s="T250">
                  <ts e="T251" id="Seg_939" n="HIAT:w" s="T250">Bu</ts>
                  <nts id="Seg_940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_942" n="HIAT:w" s="T251">dʼaktar</ts>
                  <nts id="Seg_943" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_945" n="HIAT:w" s="T252">baːj</ts>
                  <nts id="Seg_946" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_948" n="HIAT:w" s="T253">körüneriger</ts>
                  <nts id="Seg_949" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_951" n="HIAT:w" s="T254">holoto</ts>
                  <nts id="Seg_952" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_954" n="HIAT:w" s="T255">hu͡ok</ts>
                  <nts id="Seg_955" n="HIAT:ip">,</nts>
                  <nts id="Seg_956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_958" n="HIAT:w" s="T256">högön</ts>
                  <nts id="Seg_959" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_961" n="HIAT:w" s="T257">baraːn</ts>
                  <nts id="Seg_962" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_964" n="HIAT:w" s="T258">büppet</ts>
                  <nts id="Seg_965" n="HIAT:ip">.</nts>
                  <nts id="Seg_966" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T266" id="Seg_968" n="HIAT:u" s="T259">
                  <ts e="T260" id="Seg_970" n="HIAT:w" s="T259">Biːrde</ts>
                  <nts id="Seg_971" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_973" n="HIAT:w" s="T260">maː</ts>
                  <nts id="Seg_974" n="HIAT:ip">,</nts>
                  <nts id="Seg_975" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_977" n="HIAT:w" s="T261">buːs</ts>
                  <nts id="Seg_978" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_980" n="HIAT:w" s="T262">kolbujaga</ts>
                  <nts id="Seg_981" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_983" n="HIAT:w" s="T263">egelbit</ts>
                  <nts id="Seg_984" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_986" n="HIAT:w" s="T264">uː</ts>
                  <nts id="Seg_987" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_989" n="HIAT:w" s="T265">iččite</ts>
                  <nts id="Seg_990" n="HIAT:ip">.</nts>
                  <nts id="Seg_991" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T268" id="Seg_993" n="HIAT:u" s="T266">
                  <ts e="T267" id="Seg_995" n="HIAT:w" s="T266">Egelen</ts>
                  <nts id="Seg_996" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_998" n="HIAT:w" s="T267">hereppit</ts>
                  <nts id="Seg_999" n="HIAT:ip">:</nts>
                  <nts id="Seg_1000" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T275" id="Seg_1002" n="HIAT:u" s="T268">
                  <nts id="Seg_1003" n="HIAT:ip">"</nts>
                  <ts e="T269" id="Seg_1005" n="HIAT:w" s="T268">Bu</ts>
                  <nts id="Seg_1006" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_1008" n="HIAT:w" s="T269">kolbujaga</ts>
                  <nts id="Seg_1009" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_1011" n="HIAT:w" s="T270">iliːgin</ts>
                  <nts id="Seg_1012" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_1014" n="HIAT:w" s="T271">da</ts>
                  <nts id="Seg_1015" n="HIAT:ip">,</nts>
                  <nts id="Seg_1016" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_1018" n="HIAT:w" s="T272">čömüjegin</ts>
                  <nts id="Seg_1019" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_1021" n="HIAT:w" s="T273">da</ts>
                  <nts id="Seg_1022" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_1024" n="HIAT:w" s="T274">ugaːjagɨn</ts>
                  <nts id="Seg_1025" n="HIAT:ip">.</nts>
                  <nts id="Seg_1026" n="HIAT:ip">"</nts>
                  <nts id="Seg_1027" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T286" id="Seg_1029" n="HIAT:u" s="T275">
                  <ts e="T276" id="Seg_1031" n="HIAT:w" s="T275">Uː</ts>
                  <nts id="Seg_1032" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_1034" n="HIAT:w" s="T276">iččite</ts>
                  <nts id="Seg_1035" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_1037" n="HIAT:w" s="T277">hɨrgalana</ts>
                  <nts id="Seg_1038" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_1040" n="HIAT:w" s="T278">barbɨtɨn</ts>
                  <nts id="Seg_1041" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_1043" n="HIAT:w" s="T279">kenne</ts>
                  <nts id="Seg_1044" n="HIAT:ip">,</nts>
                  <nts id="Seg_1045" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_1047" n="HIAT:w" s="T280">dʼaktara</ts>
                  <nts id="Seg_1048" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_1050" n="HIAT:w" s="T281">buːs</ts>
                  <nts id="Seg_1051" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_1053" n="HIAT:w" s="T282">kolbujaga</ts>
                  <nts id="Seg_1054" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_1056" n="HIAT:w" s="T283">kelen</ts>
                  <nts id="Seg_1057" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_1059" n="HIAT:w" s="T284">čömüjetin</ts>
                  <nts id="Seg_1060" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_1062" n="HIAT:w" s="T285">ukput</ts>
                  <nts id="Seg_1063" n="HIAT:ip">.</nts>
                  <nts id="Seg_1064" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T290" id="Seg_1066" n="HIAT:u" s="T286">
                  <ts e="T287" id="Seg_1068" n="HIAT:w" s="T286">Ortokuː</ts>
                  <nts id="Seg_1069" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_1071" n="HIAT:w" s="T287">čömüjete</ts>
                  <nts id="Seg_1072" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_1074" n="HIAT:w" s="T288">tuːra</ts>
                  <nts id="Seg_1075" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_1077" n="HIAT:w" s="T289">toŋmut</ts>
                  <nts id="Seg_1078" n="HIAT:ip">.</nts>
                  <nts id="Seg_1079" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T298" id="Seg_1081" n="HIAT:u" s="T290">
                  <ts e="T291" id="Seg_1083" n="HIAT:w" s="T290">Čömüjetin</ts>
                  <nts id="Seg_1084" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_1086" n="HIAT:w" s="T291">kam</ts>
                  <nts id="Seg_1087" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_1089" n="HIAT:w" s="T292">kelgijbit</ts>
                  <nts id="Seg_1090" n="HIAT:ip">,</nts>
                  <nts id="Seg_1091" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_1093" n="HIAT:w" s="T293">uː</ts>
                  <nts id="Seg_1094" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1096" n="HIAT:w" s="T294">iččite</ts>
                  <nts id="Seg_1097" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1099" n="HIAT:w" s="T295">kelen</ts>
                  <nts id="Seg_1100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_1102" n="HIAT:w" s="T296">honno</ts>
                  <nts id="Seg_1103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_1105" n="HIAT:w" s="T297">taːjbɨt</ts>
                  <nts id="Seg_1106" n="HIAT:ip">.</nts>
                  <nts id="Seg_1107" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T319" id="Seg_1109" n="HIAT:u" s="T298">
                  <nts id="Seg_1110" n="HIAT:ip">"</nts>
                  <ts e="T299" id="Seg_1112" n="HIAT:w" s="T298">Mini͡eke</ts>
                  <nts id="Seg_1113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_1115" n="HIAT:w" s="T299">istigene</ts>
                  <nts id="Seg_1116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_1118" n="HIAT:w" s="T300">hu͡o</ts>
                  <nts id="Seg_1119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_1121" n="HIAT:w" s="T301">dʼaktar</ts>
                  <nts id="Seg_1122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1124" n="HIAT:w" s="T302">naːdata</ts>
                  <nts id="Seg_1125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_1127" n="HIAT:w" s="T303">hu͡ok</ts>
                  <nts id="Seg_1128" n="HIAT:ip">"</nts>
                  <nts id="Seg_1129" n="HIAT:ip">,</nts>
                  <nts id="Seg_1130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_1132" n="HIAT:w" s="T304">di͡en</ts>
                  <nts id="Seg_1133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_1135" n="HIAT:w" s="T305">baraːn</ts>
                  <nts id="Seg_1136" n="HIAT:ip">,</nts>
                  <nts id="Seg_1137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_1139" n="HIAT:w" s="T306">ɨraːktaːgɨ</ts>
                  <nts id="Seg_1140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_1142" n="HIAT:w" s="T307">dʼi͡e</ts>
                  <nts id="Seg_1143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1145" n="HIAT:w" s="T308">kohugar</ts>
                  <nts id="Seg_1146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_1148" n="HIAT:w" s="T309">kaːjan</ts>
                  <nts id="Seg_1149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_1151" n="HIAT:w" s="T310">keːspit</ts>
                  <nts id="Seg_1152" n="HIAT:ip">,</nts>
                  <nts id="Seg_1153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_1155" n="HIAT:w" s="T311">aːnɨn</ts>
                  <nts id="Seg_1156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_1158" n="HIAT:w" s="T312">čipičči</ts>
                  <nts id="Seg_1159" n="HIAT:ip">,</nts>
                  <nts id="Seg_1160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_1162" n="HIAT:w" s="T313">katɨːs</ts>
                  <nts id="Seg_1163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_1165" n="HIAT:w" s="T314">ü͡öhünen</ts>
                  <nts id="Seg_1166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1168" n="HIAT:w" s="T315">oŋohullubut</ts>
                  <nts id="Seg_1169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_1171" n="HIAT:w" s="T316">hiliminen</ts>
                  <nts id="Seg_1172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_1174" n="HIAT:w" s="T317">kam</ts>
                  <nts id="Seg_1175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1177" n="HIAT:w" s="T318">bispit</ts>
                  <nts id="Seg_1178" n="HIAT:ip">.</nts>
                  <nts id="Seg_1179" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T325" id="Seg_1181" n="HIAT:u" s="T319">
                  <ts e="T320" id="Seg_1183" n="HIAT:w" s="T319">Oguru͡o</ts>
                  <nts id="Seg_1184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1186" n="HIAT:w" s="T320">Bɨtɨk</ts>
                  <nts id="Seg_1187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_1189" n="HIAT:w" s="T321">emi͡e</ts>
                  <nts id="Seg_1190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1192" n="HIAT:w" s="T322">ilim</ts>
                  <nts id="Seg_1193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_1195" n="HIAT:w" s="T323">körüne</ts>
                  <nts id="Seg_1196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1198" n="HIAT:w" s="T324">kelbit</ts>
                  <nts id="Seg_1199" n="HIAT:ip">.</nts>
                  <nts id="Seg_1200" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T334" id="Seg_1202" n="HIAT:u" s="T325">
                  <ts e="T326" id="Seg_1204" n="HIAT:w" s="T325">Ojbonugar</ts>
                  <nts id="Seg_1205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1207" n="HIAT:w" s="T326">bökčöjörün</ts>
                  <nts id="Seg_1208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_1210" n="HIAT:w" s="T327">kɨtta</ts>
                  <nts id="Seg_1211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_1213" n="HIAT:w" s="T328">uː</ts>
                  <nts id="Seg_1214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_1216" n="HIAT:w" s="T329">iččite</ts>
                  <nts id="Seg_1217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_1219" n="HIAT:w" s="T330">emi͡e</ts>
                  <nts id="Seg_1220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_1222" n="HIAT:w" s="T331">bɨtɨgɨttan</ts>
                  <nts id="Seg_1223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_1225" n="HIAT:w" s="T332">kaban</ts>
                  <nts id="Seg_1226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_1228" n="HIAT:w" s="T333">ɨlbɨt</ts>
                  <nts id="Seg_1229" n="HIAT:ip">.</nts>
                  <nts id="Seg_1230" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T340" id="Seg_1232" n="HIAT:u" s="T334">
                  <nts id="Seg_1233" n="HIAT:ip">"</nts>
                  <ts e="T335" id="Seg_1235" n="HIAT:w" s="T334">Egel</ts>
                  <nts id="Seg_1236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_1238" n="HIAT:w" s="T335">ɨlgɨn</ts>
                  <nts id="Seg_1239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_1241" n="HIAT:w" s="T336">kɨːskɨn</ts>
                  <nts id="Seg_1242" n="HIAT:ip">,</nts>
                  <nts id="Seg_1243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_1245" n="HIAT:w" s="T337">dʼaktar</ts>
                  <nts id="Seg_1246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_1248" n="HIAT:w" s="T338">gɨnɨ͡akpɨn</ts>
                  <nts id="Seg_1249" n="HIAT:ip">"</nts>
                  <nts id="Seg_1250" n="HIAT:ip">,</nts>
                  <nts id="Seg_1251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_1253" n="HIAT:w" s="T339">di͡ebit</ts>
                  <nts id="Seg_1254" n="HIAT:ip">.</nts>
                  <nts id="Seg_1255" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T346" id="Seg_1257" n="HIAT:u" s="T340">
                  <ts e="T341" id="Seg_1259" n="HIAT:w" s="T340">Muŋu</ts>
                  <nts id="Seg_1260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_1262" n="HIAT:w" s="T341">bɨstɨ͡a</ts>
                  <nts id="Seg_1263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1265" n="HIAT:w" s="T342">du͡o</ts>
                  <nts id="Seg_1266" n="HIAT:ip">,</nts>
                  <nts id="Seg_1267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_1269" n="HIAT:w" s="T343">ɨlgɨn</ts>
                  <nts id="Seg_1270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_1272" n="HIAT:w" s="T344">kɨːhɨn</ts>
                  <nts id="Seg_1273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_1275" n="HIAT:w" s="T345">ilpit</ts>
                  <nts id="Seg_1276" n="HIAT:ip">.</nts>
                  <nts id="Seg_1277" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T350" id="Seg_1279" n="HIAT:u" s="T346">
                  <ts e="T347" id="Seg_1281" n="HIAT:w" s="T346">Baran</ts>
                  <nts id="Seg_1282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1284" n="HIAT:w" s="T347">ihenner</ts>
                  <nts id="Seg_1285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_1287" n="HIAT:w" s="T348">kɨːha</ts>
                  <nts id="Seg_1288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_1290" n="HIAT:w" s="T349">di͡ebit</ts>
                  <nts id="Seg_1291" n="HIAT:ip">:</nts>
                  <nts id="Seg_1292" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T352" id="Seg_1294" n="HIAT:u" s="T350">
                  <nts id="Seg_1295" n="HIAT:ip">"</nts>
                  <ts e="T351" id="Seg_1297" n="HIAT:w" s="T350">Kuttanɨma</ts>
                  <nts id="Seg_1298" n="HIAT:ip">,</nts>
                  <nts id="Seg_1299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_1301" n="HIAT:w" s="T351">teːte</ts>
                  <nts id="Seg_1302" n="HIAT:ip">.</nts>
                  <nts id="Seg_1303" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T358" id="Seg_1305" n="HIAT:u" s="T352">
                  <ts e="T353" id="Seg_1307" n="HIAT:w" s="T352">Min</ts>
                  <nts id="Seg_1308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_1310" n="HIAT:w" s="T353">honno</ts>
                  <nts id="Seg_1311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_1313" n="HIAT:w" s="T354">keli͡em</ts>
                  <nts id="Seg_1314" n="HIAT:ip">,</nts>
                  <nts id="Seg_1315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_1317" n="HIAT:w" s="T355">edʼiːjderbin</ts>
                  <nts id="Seg_1318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_1320" n="HIAT:w" s="T356">emi͡e</ts>
                  <nts id="Seg_1321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_1323" n="HIAT:w" s="T357">egeli͡em</ts>
                  <nts id="Seg_1324" n="HIAT:ip">.</nts>
                  <nts id="Seg_1325" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T365" id="Seg_1327" n="HIAT:u" s="T358">
                  <ts e="T359" id="Seg_1329" n="HIAT:w" s="T358">Uː</ts>
                  <nts id="Seg_1330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_1332" n="HIAT:w" s="T359">iččite</ts>
                  <nts id="Seg_1333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_1335" n="HIAT:w" s="T360">miniginneːger</ts>
                  <nts id="Seg_1336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_1338" n="HIAT:w" s="T361">kubulgata</ts>
                  <nts id="Seg_1339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_1341" n="HIAT:w" s="T362">hu͡o</ts>
                  <nts id="Seg_1342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_1344" n="HIAT:w" s="T363">bu͡olu͡o</ts>
                  <nts id="Seg_1345" n="HIAT:ip">,</nts>
                  <nts id="Seg_1346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_1348" n="HIAT:w" s="T364">badaga</ts>
                  <nts id="Seg_1349" n="HIAT:ip">.</nts>
                  <nts id="Seg_1350" n="HIAT:ip">"</nts>
                  <nts id="Seg_1351" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T374" id="Seg_1353" n="HIAT:u" s="T365">
                  <ts e="T366" id="Seg_1355" n="HIAT:w" s="T365">Uː</ts>
                  <nts id="Seg_1356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_1358" n="HIAT:w" s="T366">iččite</ts>
                  <nts id="Seg_1359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_1361" n="HIAT:w" s="T367">ühüs</ts>
                  <nts id="Seg_1362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_1364" n="HIAT:w" s="T368">dʼaktarɨn</ts>
                  <nts id="Seg_1365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_1367" n="HIAT:w" s="T369">buːs</ts>
                  <nts id="Seg_1368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_1370" n="HIAT:w" s="T370">kolbujaga</ts>
                  <nts id="Seg_1371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_1373" n="HIAT:w" s="T371">egelen</ts>
                  <nts id="Seg_1374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_1376" n="HIAT:w" s="T372">baraːn</ts>
                  <nts id="Seg_1377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_1379" n="HIAT:w" s="T373">di͡ebit</ts>
                  <nts id="Seg_1380" n="HIAT:ip">:</nts>
                  <nts id="Seg_1381" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T378" id="Seg_1383" n="HIAT:u" s="T374">
                  <nts id="Seg_1384" n="HIAT:ip">"</nts>
                  <ts e="T375" id="Seg_1386" n="HIAT:w" s="T374">Bu</ts>
                  <nts id="Seg_1387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_1389" n="HIAT:w" s="T375">kolbujaga</ts>
                  <nts id="Seg_1390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_1392" n="HIAT:w" s="T376">iliːgin</ts>
                  <nts id="Seg_1393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378" id="Seg_1395" n="HIAT:w" s="T377">uguma</ts>
                  <nts id="Seg_1396" n="HIAT:ip">.</nts>
                  <nts id="Seg_1397" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T381" id="Seg_1399" n="HIAT:u" s="T378">
                  <ts e="T379" id="Seg_1401" n="HIAT:w" s="T378">Tönnön</ts>
                  <nts id="Seg_1402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_1404" n="HIAT:w" s="T379">kellekpine</ts>
                  <nts id="Seg_1405" n="HIAT:ip">,</nts>
                  <nts id="Seg_1406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_1408" n="HIAT:w" s="T380">körü͡öm</ts>
                  <nts id="Seg_1409" n="HIAT:ip">.</nts>
                  <nts id="Seg_1410" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T384" id="Seg_1412" n="HIAT:u" s="T381">
                  <ts e="T382" id="Seg_1414" n="HIAT:w" s="T381">Minigin</ts>
                  <nts id="Seg_1415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_1417" n="HIAT:w" s="T382">albɨnnɨ͡aŋ</ts>
                  <nts id="Seg_1418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_1420" n="HIAT:w" s="T383">hu͡oga</ts>
                  <nts id="Seg_1421" n="HIAT:ip">.</nts>
                  <nts id="Seg_1422" n="HIAT:ip">"</nts>
                  <nts id="Seg_1423" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T391" id="Seg_1425" n="HIAT:u" s="T384">
                  <nts id="Seg_1426" n="HIAT:ip">"</nts>
                  <ts e="T385" id="Seg_1428" n="HIAT:w" s="T384">Togo</ts>
                  <nts id="Seg_1429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T386" id="Seg_1431" n="HIAT:w" s="T385">minigin</ts>
                  <nts id="Seg_1432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_1434" n="HIAT:w" s="T386">hereteːr</ts>
                  <nts id="Seg_1435" n="HIAT:ip">"</nts>
                  <nts id="Seg_1436" n="HIAT:ip">,</nts>
                  <nts id="Seg_1437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_1439" n="HIAT:w" s="T387">diː</ts>
                  <nts id="Seg_1440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_1442" n="HIAT:w" s="T388">hanaːbɨt</ts>
                  <nts id="Seg_1443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_1445" n="HIAT:w" s="T389">bu</ts>
                  <nts id="Seg_1446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T391" id="Seg_1448" n="HIAT:w" s="T390">dʼaktar</ts>
                  <nts id="Seg_1449" n="HIAT:ip">.</nts>
                  <nts id="Seg_1450" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T399" id="Seg_1452" n="HIAT:u" s="T391">
                  <nts id="Seg_1453" n="HIAT:ip">"</nts>
                  <ts e="T392" id="Seg_1455" n="HIAT:w" s="T391">Tu͡ok</ts>
                  <nts id="Seg_1456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T393" id="Seg_1458" n="HIAT:w" s="T392">bu͡olu͡oj</ts>
                  <nts id="Seg_1459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_1461" n="HIAT:w" s="T393">dʼe</ts>
                  <nts id="Seg_1462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_1464" n="HIAT:w" s="T394">iliːbin</ts>
                  <nts id="Seg_1465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T396" id="Seg_1467" n="HIAT:w" s="T395">uktakpɨna</ts>
                  <nts id="Seg_1468" n="HIAT:ip">,</nts>
                  <nts id="Seg_1469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_1471" n="HIAT:w" s="T396">uː</ts>
                  <nts id="Seg_1472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_1474" n="HIAT:w" s="T397">uː</ts>
                  <nts id="Seg_1475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_1477" n="HIAT:w" s="T398">kördük</ts>
                  <nts id="Seg_1478" n="HIAT:ip">.</nts>
                  <nts id="Seg_1479" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T405" id="Seg_1481" n="HIAT:u" s="T399">
                  <ts e="T400" id="Seg_1483" n="HIAT:w" s="T399">Badaga</ts>
                  <nts id="Seg_1484" n="HIAT:ip">,</nts>
                  <nts id="Seg_1485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_1487" n="HIAT:w" s="T400">tu͡ok</ts>
                  <nts id="Seg_1488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T402" id="Seg_1490" n="HIAT:w" s="T401">ere</ts>
                  <nts id="Seg_1491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_1493" n="HIAT:w" s="T402">albaha</ts>
                  <nts id="Seg_1494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T404" id="Seg_1496" n="HIAT:w" s="T403">baːra</ts>
                  <nts id="Seg_1497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T405" id="Seg_1499" n="HIAT:w" s="T404">bu͡olu͡o</ts>
                  <nts id="Seg_1500" n="HIAT:ip">.</nts>
                  <nts id="Seg_1501" n="HIAT:ip">"</nts>
                  <nts id="Seg_1502" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T414" id="Seg_1504" n="HIAT:u" s="T405">
                  <ts e="T406" id="Seg_1506" n="HIAT:w" s="T405">Nʼelmanɨ</ts>
                  <nts id="Seg_1507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T407" id="Seg_1509" n="HIAT:w" s="T406">kaban</ts>
                  <nts id="Seg_1510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T408" id="Seg_1512" n="HIAT:w" s="T407">ɨlan</ts>
                  <nts id="Seg_1513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T409" id="Seg_1515" n="HIAT:w" s="T408">baraːn</ts>
                  <nts id="Seg_1516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_1518" n="HIAT:w" s="T409">kuturugun</ts>
                  <nts id="Seg_1519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T411" id="Seg_1521" n="HIAT:w" s="T410">ukput</ts>
                  <nts id="Seg_1522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T412" id="Seg_1524" n="HIAT:w" s="T411">buːs</ts>
                  <nts id="Seg_1525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T413" id="Seg_1527" n="HIAT:w" s="T412">kolbuja</ts>
                  <nts id="Seg_1528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414" id="Seg_1530" n="HIAT:w" s="T413">ihiger</ts>
                  <nts id="Seg_1531" n="HIAT:ip">.</nts>
                  <nts id="Seg_1532" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T419" id="Seg_1534" n="HIAT:u" s="T414">
                  <ts e="T415" id="Seg_1536" n="HIAT:w" s="T414">Nʼelma</ts>
                  <nts id="Seg_1537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T416" id="Seg_1539" n="HIAT:w" s="T415">kuturuga</ts>
                  <nts id="Seg_1540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T417" id="Seg_1542" n="HIAT:w" s="T416">tuːra</ts>
                  <nts id="Seg_1543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T418" id="Seg_1545" n="HIAT:w" s="T417">toston</ts>
                  <nts id="Seg_1546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T419" id="Seg_1548" n="HIAT:w" s="T418">kaːlbɨt</ts>
                  <nts id="Seg_1549" n="HIAT:ip">.</nts>
                  <nts id="Seg_1550" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T427" id="Seg_1552" n="HIAT:u" s="T419">
                  <ts e="T420" id="Seg_1554" n="HIAT:w" s="T419">Oguru͡o</ts>
                  <nts id="Seg_1555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_1557" n="HIAT:w" s="T420">Bɨtɨk</ts>
                  <nts id="Seg_1558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T422" id="Seg_1560" n="HIAT:w" s="T421">kɨːha</ts>
                  <nts id="Seg_1561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T423" id="Seg_1563" n="HIAT:w" s="T422">taːjda</ts>
                  <nts id="Seg_1564" n="HIAT:ip">,</nts>
                  <nts id="Seg_1565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T424" id="Seg_1567" n="HIAT:w" s="T423">togo</ts>
                  <nts id="Seg_1568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T425" id="Seg_1570" n="HIAT:w" s="T424">uː</ts>
                  <nts id="Seg_1571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T426" id="Seg_1573" n="HIAT:w" s="T425">iččite</ts>
                  <nts id="Seg_1574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T427" id="Seg_1576" n="HIAT:w" s="T426">hereppitin</ts>
                  <nts id="Seg_1577" n="HIAT:ip">.</nts>
                  <nts id="Seg_1578" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T434" id="Seg_1580" n="HIAT:u" s="T427">
                  <ts e="T428" id="Seg_1582" n="HIAT:w" s="T427">Kolbuja</ts>
                  <nts id="Seg_1583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T429" id="Seg_1585" n="HIAT:w" s="T428">attɨtɨgar</ts>
                  <nts id="Seg_1586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_1588" n="HIAT:w" s="T429">oloron</ts>
                  <nts id="Seg_1589" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T431" id="Seg_1591" n="HIAT:w" s="T430">baraːn</ts>
                  <nts id="Seg_1592" n="HIAT:ip">,</nts>
                  <nts id="Seg_1593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_1595" n="HIAT:w" s="T431">ilim</ts>
                  <nts id="Seg_1596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T433" id="Seg_1598" n="HIAT:w" s="T432">abɨraktana</ts>
                  <nts id="Seg_1599" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T434" id="Seg_1601" n="HIAT:w" s="T433">oloror</ts>
                  <nts id="Seg_1602" n="HIAT:ip">.</nts>
                  <nts id="Seg_1603" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T438" id="Seg_1605" n="HIAT:u" s="T434">
                  <ts e="T435" id="Seg_1607" n="HIAT:w" s="T434">Uː</ts>
                  <nts id="Seg_1608" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_1610" n="HIAT:w" s="T435">iččite</ts>
                  <nts id="Seg_1611" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T437" id="Seg_1613" n="HIAT:w" s="T436">kelen</ts>
                  <nts id="Seg_1614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T438" id="Seg_1616" n="HIAT:w" s="T437">höktö</ts>
                  <nts id="Seg_1617" n="HIAT:ip">:</nts>
                  <nts id="Seg_1618" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T441" id="Seg_1620" n="HIAT:u" s="T438">
                  <nts id="Seg_1621" n="HIAT:ip">"</nts>
                  <ts e="T439" id="Seg_1623" n="HIAT:w" s="T438">Tu͡ok</ts>
                  <nts id="Seg_1624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440" id="Seg_1626" n="HIAT:w" s="T439">ilimin</ts>
                  <nts id="Seg_1627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_1629" n="HIAT:w" s="T440">abɨraktɨːgɨn</ts>
                  <nts id="Seg_1630" n="HIAT:ip">?</nts>
                  <nts id="Seg_1631" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T453" id="Seg_1633" n="HIAT:u" s="T441">
                  <ts e="T442" id="Seg_1635" n="HIAT:w" s="T441">Uː</ts>
                  <nts id="Seg_1636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T443" id="Seg_1638" n="HIAT:w" s="T442">ihiger</ts>
                  <nts id="Seg_1639" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T444" id="Seg_1641" n="HIAT:w" s="T443">balɨk</ts>
                  <nts id="Seg_1642" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T445" id="Seg_1644" n="HIAT:w" s="T444">ügüs</ts>
                  <nts id="Seg_1645" n="HIAT:ip">,</nts>
                  <nts id="Seg_1646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T446" id="Seg_1648" n="HIAT:w" s="T445">ilime</ts>
                  <nts id="Seg_1649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T447" id="Seg_1651" n="HIAT:w" s="T446">da</ts>
                  <nts id="Seg_1652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T448" id="Seg_1654" n="HIAT:w" s="T447">hu͡ok</ts>
                  <nts id="Seg_1655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T449" id="Seg_1657" n="HIAT:w" s="T448">min</ts>
                  <nts id="Seg_1658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T450" id="Seg_1660" n="HIAT:w" s="T449">eni͡eke</ts>
                  <nts id="Seg_1661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T451" id="Seg_1663" n="HIAT:w" s="T450">kahɨ</ts>
                  <nts id="Seg_1664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T452" id="Seg_1666" n="HIAT:w" s="T451">hanɨːrɨ</ts>
                  <nts id="Seg_1667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T453" id="Seg_1669" n="HIAT:w" s="T452">egeli͡em</ts>
                  <nts id="Seg_1670" n="HIAT:ip">.</nts>
                  <nts id="Seg_1671" n="HIAT:ip">"</nts>
                  <nts id="Seg_1672" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T464" id="Seg_1674" n="HIAT:u" s="T453">
                  <nts id="Seg_1675" n="HIAT:ip">"</nts>
                  <ts e="T454" id="Seg_1677" n="HIAT:w" s="T453">Balɨk</ts>
                  <nts id="Seg_1678" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T455" id="Seg_1680" n="HIAT:w" s="T454">ügüs</ts>
                  <nts id="Seg_1681" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T456" id="Seg_1683" n="HIAT:w" s="T455">bu͡olan</ts>
                  <nts id="Seg_1684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T457" id="Seg_1686" n="HIAT:w" s="T456">baraːn</ts>
                  <nts id="Seg_1687" n="HIAT:ip">"</nts>
                  <nts id="Seg_1688" n="HIAT:ip">,</nts>
                  <nts id="Seg_1689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T458" id="Seg_1691" n="HIAT:w" s="T457">diːr</ts>
                  <nts id="Seg_1692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T459" id="Seg_1694" n="HIAT:w" s="T458">dʼaktara</ts>
                  <nts id="Seg_1695" n="HIAT:ip">,</nts>
                  <nts id="Seg_1696" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1697" n="HIAT:ip">"</nts>
                  <ts e="T460" id="Seg_1699" n="HIAT:w" s="T459">biːr</ts>
                  <nts id="Seg_1700" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T461" id="Seg_1702" n="HIAT:w" s="T460">daː</ts>
                  <nts id="Seg_1703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T462" id="Seg_1705" n="HIAT:w" s="T461">taba</ts>
                  <nts id="Seg_1706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T463" id="Seg_1708" n="HIAT:w" s="T462">ete</ts>
                  <nts id="Seg_1709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T464" id="Seg_1711" n="HIAT:w" s="T463">hu͡ok</ts>
                  <nts id="Seg_1712" n="HIAT:ip">.</nts>
                  <nts id="Seg_1713" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T472" id="Seg_1715" n="HIAT:u" s="T464">
                  <ts e="T465" id="Seg_1717" n="HIAT:w" s="T464">Min</ts>
                  <nts id="Seg_1718" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T466" id="Seg_1720" n="HIAT:w" s="T465">taba</ts>
                  <nts id="Seg_1721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T467" id="Seg_1723" n="HIAT:w" s="T466">etiger</ts>
                  <nts id="Seg_1724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T468" id="Seg_1726" n="HIAT:w" s="T467">ü͡öremmippin</ts>
                  <nts id="Seg_1727" n="HIAT:ip">,</nts>
                  <nts id="Seg_1728" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T469" id="Seg_1730" n="HIAT:w" s="T468">taba</ts>
                  <nts id="Seg_1731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T470" id="Seg_1733" n="HIAT:w" s="T469">etin</ts>
                  <nts id="Seg_1734" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T471" id="Seg_1736" n="HIAT:w" s="T470">hi͡ekpin</ts>
                  <nts id="Seg_1737" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T472" id="Seg_1739" n="HIAT:w" s="T471">bagarabɨn</ts>
                  <nts id="Seg_1740" n="HIAT:ip">.</nts>
                  <nts id="Seg_1741" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T478" id="Seg_1743" n="HIAT:u" s="T472">
                  <ts e="T473" id="Seg_1745" n="HIAT:w" s="T472">Bu</ts>
                  <nts id="Seg_1746" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T474" id="Seg_1748" n="HIAT:w" s="T473">buːs</ts>
                  <nts id="Seg_1749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T475" id="Seg_1751" n="HIAT:w" s="T474">kolbuja</ts>
                  <nts id="Seg_1752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T476" id="Seg_1754" n="HIAT:w" s="T475">ihiger</ts>
                  <nts id="Seg_1755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T477" id="Seg_1757" n="HIAT:w" s="T476">taba</ts>
                  <nts id="Seg_1758" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T478" id="Seg_1760" n="HIAT:w" s="T477">ügüs</ts>
                  <nts id="Seg_1761" n="HIAT:ip">.</nts>
                  <nts id="Seg_1762" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T482" id="Seg_1764" n="HIAT:u" s="T478">
                  <ts e="T479" id="Seg_1766" n="HIAT:w" s="T478">Balɨk</ts>
                  <nts id="Seg_1767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T480" id="Seg_1769" n="HIAT:w" s="T479">kördük</ts>
                  <nts id="Seg_1770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T481" id="Seg_1772" n="HIAT:w" s="T480">karbɨː</ts>
                  <nts id="Seg_1773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T482" id="Seg_1775" n="HIAT:w" s="T481">hɨldʼallar</ts>
                  <nts id="Seg_1776" n="HIAT:ip">.</nts>
                  <nts id="Seg_1777" n="HIAT:ip">"</nts>
                  <nts id="Seg_1778" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T486" id="Seg_1780" n="HIAT:u" s="T482">
                  <ts e="T483" id="Seg_1782" n="HIAT:w" s="T482">Uː</ts>
                  <nts id="Seg_1783" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T484" id="Seg_1785" n="HIAT:w" s="T483">iččite</ts>
                  <nts id="Seg_1786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T485" id="Seg_1788" n="HIAT:w" s="T484">dʼe</ts>
                  <nts id="Seg_1789" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T486" id="Seg_1791" n="HIAT:w" s="T485">küler</ts>
                  <nts id="Seg_1792" n="HIAT:ip">:</nts>
                  <nts id="Seg_1793" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T495" id="Seg_1795" n="HIAT:u" s="T486">
                  <nts id="Seg_1796" n="HIAT:ip">"</nts>
                  <ts e="T487" id="Seg_1798" n="HIAT:w" s="T486">Bu</ts>
                  <nts id="Seg_1799" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T488" id="Seg_1801" n="HIAT:w" s="T487">buːs</ts>
                  <nts id="Seg_1802" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T489" id="Seg_1804" n="HIAT:w" s="T488">kolbujaga</ts>
                  <nts id="Seg_1805" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T490" id="Seg_1807" n="HIAT:w" s="T489">tu͡ok</ts>
                  <nts id="Seg_1808" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T491" id="Seg_1810" n="HIAT:w" s="T490">tabata</ts>
                  <nts id="Seg_1811" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T492" id="Seg_1813" n="HIAT:w" s="T491">keli͡ej</ts>
                  <nts id="Seg_1814" n="HIAT:ip">,</nts>
                  <nts id="Seg_1815" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T493" id="Seg_1817" n="HIAT:w" s="T492">manna</ts>
                  <nts id="Seg_1818" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T494" id="Seg_1820" n="HIAT:w" s="T493">uː</ts>
                  <nts id="Seg_1821" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T495" id="Seg_1823" n="HIAT:w" s="T494">ere</ts>
                  <nts id="Seg_1824" n="HIAT:ip">.</nts>
                  <nts id="Seg_1825" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T498" id="Seg_1827" n="HIAT:u" s="T495">
                  <ts e="T496" id="Seg_1829" n="HIAT:w" s="T495">Min</ts>
                  <nts id="Seg_1830" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T497" id="Seg_1832" n="HIAT:w" s="T496">bilebin</ts>
                  <nts id="Seg_1833" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T498" id="Seg_1835" n="HIAT:w" s="T497">eːt</ts>
                  <nts id="Seg_1836" n="HIAT:ip">.</nts>
                  <nts id="Seg_1837" n="HIAT:ip">"</nts>
                  <nts id="Seg_1838" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T500" id="Seg_1840" n="HIAT:u" s="T498">
                  <ts e="T499" id="Seg_1842" n="HIAT:w" s="T498">Dʼaktara</ts>
                  <nts id="Seg_1843" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T500" id="Seg_1845" n="HIAT:w" s="T499">di͡ebit</ts>
                  <nts id="Seg_1846" n="HIAT:ip">:</nts>
                  <nts id="Seg_1847" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T505" id="Seg_1849" n="HIAT:u" s="T500">
                  <nts id="Seg_1850" n="HIAT:ip">"</nts>
                  <ts e="T501" id="Seg_1852" n="HIAT:w" s="T500">Min</ts>
                  <nts id="Seg_1853" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T502" id="Seg_1855" n="HIAT:w" s="T501">körbütüm</ts>
                  <nts id="Seg_1856" n="HIAT:ip">,</nts>
                  <nts id="Seg_1857" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T503" id="Seg_1859" n="HIAT:w" s="T502">tabalar</ts>
                  <nts id="Seg_1860" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T504" id="Seg_1862" n="HIAT:w" s="T503">karbɨː</ts>
                  <nts id="Seg_1863" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T505" id="Seg_1865" n="HIAT:w" s="T504">hɨldʼallar</ts>
                  <nts id="Seg_1866" n="HIAT:ip">.</nts>
                  <nts id="Seg_1867" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T511" id="Seg_1869" n="HIAT:u" s="T505">
                  <ts e="T506" id="Seg_1871" n="HIAT:w" s="T505">Horoktor</ts>
                  <nts id="Seg_1872" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T507" id="Seg_1874" n="HIAT:w" s="T506">čeːlkeːler</ts>
                  <nts id="Seg_1875" n="HIAT:ip">,</nts>
                  <nts id="Seg_1876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T508" id="Seg_1878" n="HIAT:w" s="T507">horoktor</ts>
                  <nts id="Seg_1879" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T509" id="Seg_1881" n="HIAT:w" s="T508">bugdiːler</ts>
                  <nts id="Seg_1882" n="HIAT:ip">,</nts>
                  <nts id="Seg_1883" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T510" id="Seg_1885" n="HIAT:w" s="T509">aŋardara</ts>
                  <nts id="Seg_1886" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T511" id="Seg_1888" n="HIAT:w" s="T510">karalar</ts>
                  <nts id="Seg_1889" n="HIAT:ip">.</nts>
                  <nts id="Seg_1890" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T513" id="Seg_1892" n="HIAT:u" s="T511">
                  <ts e="T512" id="Seg_1894" n="HIAT:w" s="T511">Bütün</ts>
                  <nts id="Seg_1895" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T513" id="Seg_1897" n="HIAT:w" s="T512">ü͡ör</ts>
                  <nts id="Seg_1898" n="HIAT:ip">.</nts>
                  <nts id="Seg_1899" n="HIAT:ip">"</nts>
                  <nts id="Seg_1900" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T524" id="Seg_1902" n="HIAT:u" s="T513">
                  <nts id="Seg_1903" n="HIAT:ip">"</nts>
                  <ts e="T514" id="Seg_1905" n="HIAT:w" s="T513">Aŋɨr</ts>
                  <nts id="Seg_1906" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T515" id="Seg_1908" n="HIAT:w" s="T514">dʼaktargɨn</ts>
                  <nts id="Seg_1909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T516" id="Seg_1911" n="HIAT:w" s="T515">ebit</ts>
                  <nts id="Seg_1912" n="HIAT:ip">"</nts>
                  <nts id="Seg_1913" n="HIAT:ip">,</nts>
                  <nts id="Seg_1914" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T517" id="Seg_1916" n="HIAT:w" s="T516">kɨjŋanna</ts>
                  <nts id="Seg_1917" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T518" id="Seg_1919" n="HIAT:w" s="T517">uː</ts>
                  <nts id="Seg_1920" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T519" id="Seg_1922" n="HIAT:w" s="T518">iččite</ts>
                  <nts id="Seg_1923" n="HIAT:ip">,</nts>
                  <nts id="Seg_1924" n="HIAT:ip">"</nts>
                  <nts id="Seg_1925" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T520" id="Seg_1927" n="HIAT:w" s="T519">tu͡ok</ts>
                  <nts id="Seg_1928" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T521" id="Seg_1930" n="HIAT:w" s="T520">tabata</ts>
                  <nts id="Seg_1931" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T522" id="Seg_1933" n="HIAT:w" s="T521">uː</ts>
                  <nts id="Seg_1934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T523" id="Seg_1936" n="HIAT:w" s="T522">ihiger</ts>
                  <nts id="Seg_1937" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T524" id="Seg_1939" n="HIAT:w" s="T523">hɨldʼɨ͡aj</ts>
                  <nts id="Seg_1940" n="HIAT:ip">?</nts>
                  <nts id="Seg_1941" n="HIAT:ip">"</nts>
                  <nts id="Seg_1942" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T530" id="Seg_1944" n="HIAT:u" s="T524">
                  <nts id="Seg_1945" n="HIAT:ip">"</nts>
                  <ts e="T525" id="Seg_1947" n="HIAT:w" s="T524">Bejem</ts>
                  <nts id="Seg_1948" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T526" id="Seg_1950" n="HIAT:w" s="T525">bilegin</ts>
                  <nts id="Seg_1951" n="HIAT:ip">,</nts>
                  <nts id="Seg_1952" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T527" id="Seg_1954" n="HIAT:w" s="T526">bihigi</ts>
                  <nts id="Seg_1955" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T528" id="Seg_1957" n="HIAT:w" s="T527">uː</ts>
                  <nts id="Seg_1958" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T529" id="Seg_1960" n="HIAT:w" s="T528">ihiger</ts>
                  <nts id="Seg_1961" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T530" id="Seg_1963" n="HIAT:w" s="T529">olorobut</ts>
                  <nts id="Seg_1964" n="HIAT:ip">.</nts>
                  <nts id="Seg_1965" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T543" id="Seg_1967" n="HIAT:u" s="T530">
                  <ts e="T531" id="Seg_1969" n="HIAT:w" s="T530">Ol</ts>
                  <nts id="Seg_1970" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T532" id="Seg_1972" n="HIAT:w" s="T531">ihin</ts>
                  <nts id="Seg_1973" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T533" id="Seg_1975" n="HIAT:w" s="T532">tabalar</ts>
                  <nts id="Seg_1976" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T534" id="Seg_1978" n="HIAT:w" s="T533">emi͡e</ts>
                  <nts id="Seg_1979" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T535" id="Seg_1981" n="HIAT:w" s="T534">uː</ts>
                  <nts id="Seg_1982" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T536" id="Seg_1984" n="HIAT:w" s="T535">ihiger</ts>
                  <nts id="Seg_1985" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T537" id="Seg_1987" n="HIAT:w" s="T536">hɨldʼallar</ts>
                  <nts id="Seg_1988" n="HIAT:ip">"</nts>
                  <nts id="Seg_1989" n="HIAT:ip">,</nts>
                  <nts id="Seg_1990" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T538" id="Seg_1992" n="HIAT:w" s="T537">di͡ebit</ts>
                  <nts id="Seg_1993" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T539" id="Seg_1995" n="HIAT:w" s="T538">dʼaktara</ts>
                  <nts id="Seg_1996" n="HIAT:ip">,</nts>
                  <nts id="Seg_1997" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1998" n="HIAT:ip">"</nts>
                  <ts e="T540" id="Seg_2000" n="HIAT:w" s="T539">kör</ts>
                  <nts id="Seg_2001" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T541" id="Seg_2003" n="HIAT:w" s="T540">dʼe</ts>
                  <nts id="Seg_2004" n="HIAT:ip">,</nts>
                  <nts id="Seg_2005" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T542" id="Seg_2007" n="HIAT:w" s="T541">kolbuja</ts>
                  <nts id="Seg_2008" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T543" id="Seg_2010" n="HIAT:w" s="T542">ihin</ts>
                  <nts id="Seg_2011" n="HIAT:ip">.</nts>
                  <nts id="Seg_2012" n="HIAT:ip">"</nts>
                  <nts id="Seg_2013" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T549" id="Seg_2015" n="HIAT:u" s="T543">
                  <ts e="T544" id="Seg_2017" n="HIAT:w" s="T543">Uː</ts>
                  <nts id="Seg_2018" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T545" id="Seg_2020" n="HIAT:w" s="T544">iččite</ts>
                  <nts id="Seg_2021" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T546" id="Seg_2023" n="HIAT:w" s="T545">kɨjŋanan</ts>
                  <nts id="Seg_2024" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T547" id="Seg_2026" n="HIAT:w" s="T546">baraːn</ts>
                  <nts id="Seg_2027" n="HIAT:ip">,</nts>
                  <nts id="Seg_2028" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T548" id="Seg_2030" n="HIAT:w" s="T547">umnan</ts>
                  <nts id="Seg_2031" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T549" id="Seg_2033" n="HIAT:w" s="T548">keːspit</ts>
                  <nts id="Seg_2034" n="HIAT:ip">.</nts>
                  <nts id="Seg_2035" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T553" id="Seg_2037" n="HIAT:u" s="T549">
                  <ts e="T550" id="Seg_2039" n="HIAT:w" s="T549">Kolbujatɨgar</ts>
                  <nts id="Seg_2040" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T551" id="Seg_2042" n="HIAT:w" s="T550">öŋöjbüt</ts>
                  <nts id="Seg_2043" n="HIAT:ip">,</nts>
                  <nts id="Seg_2044" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T552" id="Seg_2046" n="HIAT:w" s="T551">tabalarɨ</ts>
                  <nts id="Seg_2047" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T553" id="Seg_2049" n="HIAT:w" s="T552">köröːrü</ts>
                  <nts id="Seg_2050" n="HIAT:ip">.</nts>
                  <nts id="Seg_2051" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T560" id="Seg_2053" n="HIAT:u" s="T553">
                  <ts e="T554" id="Seg_2055" n="HIAT:w" s="T553">Dʼaktara</ts>
                  <nts id="Seg_2056" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T555" id="Seg_2058" n="HIAT:w" s="T554">kelen</ts>
                  <nts id="Seg_2059" n="HIAT:ip">,</nts>
                  <nts id="Seg_2060" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T556" id="Seg_2062" n="HIAT:w" s="T555">menʼiːtin</ts>
                  <nts id="Seg_2063" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T557" id="Seg_2065" n="HIAT:w" s="T556">uː</ts>
                  <nts id="Seg_2066" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T558" id="Seg_2068" n="HIAT:w" s="T557">ihiger</ts>
                  <nts id="Seg_2069" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T559" id="Seg_2071" n="HIAT:w" s="T558">hamnarɨ</ts>
                  <nts id="Seg_2072" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T560" id="Seg_2074" n="HIAT:w" s="T559">battaːbɨt</ts>
                  <nts id="Seg_2075" n="HIAT:ip">.</nts>
                  <nts id="Seg_2076" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T567" id="Seg_2078" n="HIAT:u" s="T560">
                  <ts e="T561" id="Seg_2080" n="HIAT:w" s="T560">Uː</ts>
                  <nts id="Seg_2081" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T562" id="Seg_2083" n="HIAT:w" s="T561">iččitin</ts>
                  <nts id="Seg_2084" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T563" id="Seg_2086" n="HIAT:w" s="T562">menʼiːte</ts>
                  <nts id="Seg_2087" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T564" id="Seg_2089" n="HIAT:w" s="T563">tuːra</ts>
                  <nts id="Seg_2090" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T565" id="Seg_2092" n="HIAT:w" s="T564">barbɨt</ts>
                  <nts id="Seg_2093" n="HIAT:ip">,</nts>
                  <nts id="Seg_2094" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T566" id="Seg_2096" n="HIAT:w" s="T565">honno</ts>
                  <nts id="Seg_2097" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T567" id="Seg_2099" n="HIAT:w" s="T566">ölbüt</ts>
                  <nts id="Seg_2100" n="HIAT:ip">.</nts>
                  <nts id="Seg_2101" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T577" id="Seg_2103" n="HIAT:u" s="T567">
                  <ts e="T568" id="Seg_2105" n="HIAT:w" s="T567">Oguru͡o</ts>
                  <nts id="Seg_2106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T569" id="Seg_2108" n="HIAT:w" s="T568">Bɨtɨk</ts>
                  <nts id="Seg_2109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T570" id="Seg_2111" n="HIAT:w" s="T569">ɨlgɨn</ts>
                  <nts id="Seg_2112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T571" id="Seg_2114" n="HIAT:w" s="T570">kɨːha</ts>
                  <nts id="Seg_2115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T572" id="Seg_2117" n="HIAT:w" s="T571">aːntan</ts>
                  <nts id="Seg_2118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T573" id="Seg_2120" n="HIAT:w" s="T572">aːnɨ</ts>
                  <nts id="Seg_2121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T574" id="Seg_2123" n="HIAT:w" s="T573">kerije</ts>
                  <nts id="Seg_2124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T575" id="Seg_2126" n="HIAT:w" s="T574">hɨldʼan</ts>
                  <nts id="Seg_2127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T576" id="Seg_2129" n="HIAT:w" s="T575">edʼiːjderin</ts>
                  <nts id="Seg_2130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T577" id="Seg_2132" n="HIAT:w" s="T576">bulbut</ts>
                  <nts id="Seg_2133" n="HIAT:ip">.</nts>
                  <nts id="Seg_2134" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T583" id="Seg_2136" n="HIAT:u" s="T577">
                  <ts e="T578" id="Seg_2138" n="HIAT:w" s="T577">Aːnnarɨn</ts>
                  <nts id="Seg_2139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T579" id="Seg_2141" n="HIAT:w" s="T578">hilimnerin</ts>
                  <nts id="Seg_2142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T580" id="Seg_2144" n="HIAT:w" s="T579">bahaːgɨnan</ts>
                  <nts id="Seg_2145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T581" id="Seg_2147" n="HIAT:w" s="T580">kɨhɨjan</ts>
                  <nts id="Seg_2148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T582" id="Seg_2150" n="HIAT:w" s="T581">ɨlan</ts>
                  <nts id="Seg_2151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T583" id="Seg_2153" n="HIAT:w" s="T582">boskoloːbut</ts>
                  <nts id="Seg_2154" n="HIAT:ip">.</nts>
                  <nts id="Seg_2155" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T587" id="Seg_2157" n="HIAT:u" s="T583">
                  <ts e="T584" id="Seg_2159" n="HIAT:w" s="T583">Ühü͡önnere</ts>
                  <nts id="Seg_2160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T585" id="Seg_2162" n="HIAT:w" s="T584">uraha</ts>
                  <nts id="Seg_2163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T586" id="Seg_2165" n="HIAT:w" s="T585">dʼi͡eleriger</ts>
                  <nts id="Seg_2166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T587" id="Seg_2168" n="HIAT:w" s="T586">kelbitter</ts>
                  <nts id="Seg_2169" n="HIAT:ip">.</nts>
                  <nts id="Seg_2170" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T596" id="Seg_2172" n="HIAT:u" s="T587">
                  <ts e="T588" id="Seg_2174" n="HIAT:w" s="T587">Oguru͡o</ts>
                  <nts id="Seg_2175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T589" id="Seg_2177" n="HIAT:w" s="T588">Bɨtɨk</ts>
                  <nts id="Seg_2178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T590" id="Seg_2180" n="HIAT:w" s="T589">ü͡ören</ts>
                  <nts id="Seg_2181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T591" id="Seg_2183" n="HIAT:w" s="T590">kɨrgɨttarɨgar</ts>
                  <nts id="Seg_2184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T592" id="Seg_2186" n="HIAT:w" s="T591">kɨspɨt</ts>
                  <nts id="Seg_2187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T593" id="Seg_2189" n="HIAT:w" s="T592">kɨhar</ts>
                  <nts id="Seg_2190" n="HIAT:ip">,</nts>
                  <nts id="Seg_2191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T594" id="Seg_2193" n="HIAT:w" s="T593">busput</ts>
                  <nts id="Seg_2194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T595" id="Seg_2196" n="HIAT:w" s="T594">balɨgɨ</ts>
                  <nts id="Seg_2197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T596" id="Seg_2199" n="HIAT:w" s="T595">ahatar</ts>
                  <nts id="Seg_2200" n="HIAT:ip">.</nts>
                  <nts id="Seg_2201" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T612" id="Seg_2203" n="HIAT:u" s="T596">
                  <ts e="T597" id="Seg_2205" n="HIAT:w" s="T596">Ulakan</ts>
                  <nts id="Seg_2206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T598" id="Seg_2208" n="HIAT:w" s="T597">kɨːha</ts>
                  <nts id="Seg_2209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T599" id="Seg_2211" n="HIAT:w" s="T598">tojon</ts>
                  <nts id="Seg_2212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T600" id="Seg_2214" n="HIAT:w" s="T599">čömüjetin</ts>
                  <nts id="Seg_2215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T601" id="Seg_2217" n="HIAT:w" s="T600">körön</ts>
                  <nts id="Seg_2218" n="HIAT:ip">,</nts>
                  <nts id="Seg_2219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T602" id="Seg_2221" n="HIAT:w" s="T601">onuga</ts>
                  <nts id="Seg_2222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T603" id="Seg_2224" n="HIAT:w" s="T602">orto</ts>
                  <nts id="Seg_2225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T604" id="Seg_2227" n="HIAT:w" s="T603">kɨːha</ts>
                  <nts id="Seg_2228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T605" id="Seg_2230" n="HIAT:w" s="T604">ortokuː</ts>
                  <nts id="Seg_2231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T606" id="Seg_2233" n="HIAT:w" s="T605">čömüjetin</ts>
                  <nts id="Seg_2234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T607" id="Seg_2236" n="HIAT:w" s="T606">körön</ts>
                  <nts id="Seg_2237" n="HIAT:ip">,</nts>
                  <nts id="Seg_2238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T608" id="Seg_2240" n="HIAT:w" s="T607">oččogo</ts>
                  <nts id="Seg_2241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T609" id="Seg_2243" n="HIAT:w" s="T608">ere</ts>
                  <nts id="Seg_2244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T610" id="Seg_2246" n="HIAT:w" s="T609">uː</ts>
                  <nts id="Seg_2247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T611" id="Seg_2249" n="HIAT:w" s="T610">iččitin</ts>
                  <nts id="Seg_2250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T612" id="Seg_2252" n="HIAT:w" s="T611">öjdüːller</ts>
                  <nts id="Seg_2253" n="HIAT:ip">.</nts>
                  <nts id="Seg_2254" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T615" id="Seg_2256" n="HIAT:u" s="T612">
                  <ts e="T613" id="Seg_2258" n="HIAT:w" s="T612">Oloŋkoloːbuta</ts>
                  <nts id="Seg_2259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T614" id="Seg_2261" n="HIAT:w" s="T613">Ogdu͡o</ts>
                  <nts id="Seg_2262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T615" id="Seg_2264" n="HIAT:w" s="T614">Aksʼonava</ts>
                  <nts id="Seg_2265" n="HIAT:ip">.</nts>
                  <nts id="Seg_2266" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T615" id="Seg_2267" n="sc" s="T0">
               <ts e="T1" id="Seg_2269" n="e" s="T0">Ihilleːŋ </ts>
               <ts e="T2" id="Seg_2271" n="e" s="T1">oloŋkonu, </ts>
               <ts e="T3" id="Seg_2273" n="e" s="T2">aːta </ts>
               <ts e="T4" id="Seg_2275" n="e" s="T3">"Oguru͡o </ts>
               <ts e="T5" id="Seg_2277" n="e" s="T4">bɨtɨk". </ts>
               <ts e="T6" id="Seg_2279" n="e" s="T5">Biːr </ts>
               <ts e="T7" id="Seg_2281" n="e" s="T6">ogonnʼor </ts>
               <ts e="T8" id="Seg_2283" n="e" s="T7">baːr </ts>
               <ts e="T9" id="Seg_2285" n="e" s="T8">ebit. </ts>
               <ts e="T10" id="Seg_2287" n="e" s="T9">Ogonnʼor </ts>
               <ts e="T11" id="Seg_2289" n="e" s="T10">bɨtɨga </ts>
               <ts e="T12" id="Seg_2291" n="e" s="T11">arɨttaːk </ts>
               <ts e="T13" id="Seg_2293" n="e" s="T12">bu͡olan </ts>
               <ts e="T14" id="Seg_2295" n="e" s="T13">baraːn </ts>
               <ts e="T15" id="Seg_2297" n="e" s="T14">uhun </ts>
               <ts e="T16" id="Seg_2299" n="e" s="T15">bagajɨ. </ts>
               <ts e="T17" id="Seg_2301" n="e" s="T16">Bɨtɨktara </ts>
               <ts e="T18" id="Seg_2303" n="e" s="T17">oguru͡olaːk </ts>
               <ts e="T19" id="Seg_2305" n="e" s="T18">hap </ts>
               <ts e="T20" id="Seg_2307" n="e" s="T19">kördük </ts>
               <ts e="T21" id="Seg_2309" n="e" s="T20">gilbeŋniːller. </ts>
               <ts e="T22" id="Seg_2311" n="e" s="T21">Ol </ts>
               <ts e="T23" id="Seg_2313" n="e" s="T22">ihin </ts>
               <ts e="T24" id="Seg_2315" n="e" s="T23">ogonnʼoru </ts>
               <ts e="T25" id="Seg_2317" n="e" s="T24">aːttaːbɨttar </ts>
               <ts e="T26" id="Seg_2319" n="e" s="T25">Oguru͡o </ts>
               <ts e="T27" id="Seg_2321" n="e" s="T26">Bɨtɨk </ts>
               <ts e="T28" id="Seg_2323" n="e" s="T27">di͡en. </ts>
               <ts e="T29" id="Seg_2325" n="e" s="T28">Oguru͡o </ts>
               <ts e="T30" id="Seg_2327" n="e" s="T29">bɨtɨk </ts>
               <ts e="T31" id="Seg_2329" n="e" s="T30">üs </ts>
               <ts e="T32" id="Seg_2331" n="e" s="T31">kɨːstaːk. </ts>
               <ts e="T33" id="Seg_2333" n="e" s="T32">Kɨhɨn, </ts>
               <ts e="T34" id="Seg_2335" n="e" s="T33">hajɨn </ts>
               <ts e="T35" id="Seg_2337" n="e" s="T34">ilimnener. </ts>
               <ts e="T36" id="Seg_2339" n="e" s="T35">Balɨgɨnan </ts>
               <ts e="T37" id="Seg_2341" n="e" s="T36">ere </ts>
               <ts e="T38" id="Seg_2343" n="e" s="T37">iːtillen </ts>
               <ts e="T39" id="Seg_2345" n="e" s="T38">olorollor. </ts>
               <ts e="T40" id="Seg_2347" n="e" s="T39">Biːrde </ts>
               <ts e="T41" id="Seg_2349" n="e" s="T40">Oguru͡o </ts>
               <ts e="T42" id="Seg_2351" n="e" s="T41">Bɨtɨk </ts>
               <ts e="T43" id="Seg_2353" n="e" s="T42">ilim </ts>
               <ts e="T44" id="Seg_2355" n="e" s="T43">körüne </ts>
               <ts e="T45" id="Seg_2357" n="e" s="T44">barbɨt. </ts>
               <ts e="T46" id="Seg_2359" n="e" s="T45">Ojbonugar </ts>
               <ts e="T47" id="Seg_2361" n="e" s="T46">bökčöjörün </ts>
               <ts e="T48" id="Seg_2363" n="e" s="T47">kɨtta, </ts>
               <ts e="T49" id="Seg_2365" n="e" s="T48">kim </ts>
               <ts e="T50" id="Seg_2367" n="e" s="T49">ire </ts>
               <ts e="T51" id="Seg_2369" n="e" s="T50">bɨtɨgɨttan </ts>
               <ts e="T52" id="Seg_2371" n="e" s="T51">kaban </ts>
               <ts e="T53" id="Seg_2373" n="e" s="T52">ɨlbɨt. </ts>
               <ts e="T54" id="Seg_2375" n="e" s="T53">Amattan </ts>
               <ts e="T55" id="Seg_2377" n="e" s="T54">ɨːppat. </ts>
               <ts e="T56" id="Seg_2379" n="e" s="T55">"Kim </ts>
               <ts e="T57" id="Seg_2381" n="e" s="T56">bu͡olu͡oj", </ts>
               <ts e="T58" id="Seg_2383" n="e" s="T57">diː </ts>
               <ts e="T59" id="Seg_2385" n="e" s="T58">hanaːbɨt. </ts>
               <ts e="T60" id="Seg_2387" n="e" s="T59">Ojbonton </ts>
               <ts e="T61" id="Seg_2389" n="e" s="T60">bɨkpɨt </ts>
               <ts e="T62" id="Seg_2391" n="e" s="T61">uː </ts>
               <ts e="T63" id="Seg_2393" n="e" s="T62">iččite. </ts>
               <ts e="T64" id="Seg_2395" n="e" s="T63">"Oguru͡o </ts>
               <ts e="T65" id="Seg_2397" n="e" s="T64">Bɨtɨk", </ts>
               <ts e="T66" id="Seg_2399" n="e" s="T65">diːr, </ts>
               <ts e="T67" id="Seg_2401" n="e" s="T66">"kɨːskɨn </ts>
               <ts e="T68" id="Seg_2403" n="e" s="T67">bi͡eregin </ts>
               <ts e="T69" id="Seg_2405" n="e" s="T68">duː, </ts>
               <ts e="T70" id="Seg_2407" n="e" s="T69">hu͡ok </ts>
               <ts e="T71" id="Seg_2409" n="e" s="T70">duː </ts>
               <ts e="T72" id="Seg_2411" n="e" s="T71">dʼaktar </ts>
               <ts e="T73" id="Seg_2413" n="e" s="T72">gɨnɨ͡akpɨn? </ts>
               <ts e="T74" id="Seg_2415" n="e" s="T73">Bi͡erbetekkine </ts>
               <ts e="T75" id="Seg_2417" n="e" s="T74">biːr </ts>
               <ts e="T76" id="Seg_2419" n="e" s="T75">da </ts>
               <ts e="T77" id="Seg_2421" n="e" s="T76">balɨgɨ </ts>
               <ts e="T78" id="Seg_2423" n="e" s="T77">ɨlɨ͡aŋ </ts>
               <ts e="T79" id="Seg_2425" n="e" s="T78">hu͡oga." </ts>
               <ts e="T80" id="Seg_2427" n="e" s="T79">"Tu͡ogunan </ts>
               <ts e="T81" id="Seg_2429" n="e" s="T80">kergetterbin </ts>
               <ts e="T82" id="Seg_2431" n="e" s="T81">iːti͡emij </ts>
               <ts e="T83" id="Seg_2433" n="e" s="T82">ke", </ts>
               <ts e="T84" id="Seg_2435" n="e" s="T83">diː </ts>
               <ts e="T85" id="Seg_2437" n="e" s="T84">hanaːbɨt. </ts>
               <ts e="T86" id="Seg_2439" n="e" s="T85">Onton </ts>
               <ts e="T87" id="Seg_2441" n="e" s="T86">eːktemmit. </ts>
               <ts e="T88" id="Seg_2443" n="e" s="T87">"Albɨnnaːtakkɨna, </ts>
               <ts e="T89" id="Seg_2445" n="e" s="T88">hin </ts>
               <ts e="T90" id="Seg_2447" n="e" s="T89">biːr </ts>
               <ts e="T91" id="Seg_2449" n="e" s="T90">bulu͡om", </ts>
               <ts e="T92" id="Seg_2451" n="e" s="T91">di͡ebit </ts>
               <ts e="T93" id="Seg_2453" n="e" s="T92">uː </ts>
               <ts e="T94" id="Seg_2455" n="e" s="T93">iččite. </ts>
               <ts e="T95" id="Seg_2457" n="e" s="T94">Ogonnʼor </ts>
               <ts e="T96" id="Seg_2459" n="e" s="T95">gereːpketin </ts>
               <ts e="T97" id="Seg_2461" n="e" s="T96">kaban </ts>
               <ts e="T98" id="Seg_2463" n="e" s="T97">ɨlan </ts>
               <ts e="T99" id="Seg_2465" n="e" s="T98">baraːn, </ts>
               <ts e="T100" id="Seg_2467" n="e" s="T99">buːs </ts>
               <ts e="T101" id="Seg_2469" n="e" s="T100">ihinen </ts>
               <ts e="T102" id="Seg_2471" n="e" s="T101">baran </ts>
               <ts e="T103" id="Seg_2473" n="e" s="T102">kaːlbɨt. </ts>
               <ts e="T104" id="Seg_2475" n="e" s="T103">Oguru͡o </ts>
               <ts e="T105" id="Seg_2477" n="e" s="T104">Bɨtɨk </ts>
               <ts e="T106" id="Seg_2479" n="e" s="T105">bert </ts>
               <ts e="T107" id="Seg_2481" n="e" s="T106">hutaːbɨt. </ts>
               <ts e="T108" id="Seg_2483" n="e" s="T107">Uraha </ts>
               <ts e="T109" id="Seg_2485" n="e" s="T108">dʼi͡etiger </ts>
               <ts e="T110" id="Seg_2487" n="e" s="T109">kelen, </ts>
               <ts e="T111" id="Seg_2489" n="e" s="T110">kɨrgɨttarɨgar </ts>
               <ts e="T112" id="Seg_2491" n="e" s="T111">kepseːbit. </ts>
               <ts e="T113" id="Seg_2493" n="e" s="T112">Ulakan </ts>
               <ts e="T114" id="Seg_2495" n="e" s="T113">kɨːha </ts>
               <ts e="T115" id="Seg_2497" n="e" s="T114">ü͡örbüt: </ts>
               <ts e="T116" id="Seg_2499" n="e" s="T115">"Min </ts>
               <ts e="T117" id="Seg_2501" n="e" s="T116">erge </ts>
               <ts e="T118" id="Seg_2503" n="e" s="T117">barɨ͡am </ts>
               <ts e="T119" id="Seg_2505" n="e" s="T118">uː </ts>
               <ts e="T120" id="Seg_2507" n="e" s="T119">iččitiger. </ts>
               <ts e="T121" id="Seg_2509" n="e" s="T120">Aːn dojdu </ts>
               <ts e="T122" id="Seg_2511" n="e" s="T121">ürdütünen </ts>
               <ts e="T123" id="Seg_2513" n="e" s="T122">muŋ </ts>
               <ts e="T124" id="Seg_2515" n="e" s="T123">baːj </ts>
               <ts e="T125" id="Seg_2517" n="e" s="T124">bu͡olu͡om." </ts>
               <ts e="T126" id="Seg_2519" n="e" s="T125">Oguru͡o </ts>
               <ts e="T127" id="Seg_2521" n="e" s="T126">Bɨtɨk </ts>
               <ts e="T128" id="Seg_2523" n="e" s="T127">kɨːhɨn </ts>
               <ts e="T129" id="Seg_2525" n="e" s="T128">erge </ts>
               <ts e="T130" id="Seg_2527" n="e" s="T129">bi͡eren </ts>
               <ts e="T131" id="Seg_2529" n="e" s="T130">keːspit. </ts>
               <ts e="T132" id="Seg_2531" n="e" s="T131">Uː </ts>
               <ts e="T133" id="Seg_2533" n="e" s="T132">iččite </ts>
               <ts e="T134" id="Seg_2535" n="e" s="T133">baːj </ts>
               <ts e="T135" id="Seg_2537" n="e" s="T134">da </ts>
               <ts e="T136" id="Seg_2539" n="e" s="T135">baːj. </ts>
               <ts e="T137" id="Seg_2541" n="e" s="T136">Baːjɨn </ts>
               <ts e="T138" id="Seg_2543" n="e" s="T137">(uhuga) </ts>
               <ts e="T139" id="Seg_2545" n="e" s="T138">köstübet, </ts>
               <ts e="T140" id="Seg_2547" n="e" s="T139">töhö </ts>
               <ts e="T141" id="Seg_2549" n="e" s="T140">emete </ts>
               <ts e="T142" id="Seg_2551" n="e" s="T141">kostoːk, </ts>
               <ts e="T143" id="Seg_2553" n="e" s="T142">dʼi͡eleːk. </ts>
               <ts e="T144" id="Seg_2555" n="e" s="T143">Biːr </ts>
               <ts e="T145" id="Seg_2557" n="e" s="T144">kosko </ts>
               <ts e="T146" id="Seg_2559" n="e" s="T145">ulakan </ts>
               <ts e="T147" id="Seg_2561" n="e" s="T146">buːs </ts>
               <ts e="T148" id="Seg_2563" n="e" s="T147">kolbuja </ts>
               <ts e="T149" id="Seg_2565" n="e" s="T148">turar. </ts>
               <ts e="T150" id="Seg_2567" n="e" s="T149">Uː </ts>
               <ts e="T151" id="Seg_2569" n="e" s="T150">iččite </ts>
               <ts e="T152" id="Seg_2571" n="e" s="T151">dʼaktarɨn </ts>
               <ts e="T153" id="Seg_2573" n="e" s="T152">egelen, </ts>
               <ts e="T154" id="Seg_2575" n="e" s="T153">buːs </ts>
               <ts e="T155" id="Seg_2577" n="e" s="T154">kolbujanɨ </ts>
               <ts e="T156" id="Seg_2579" n="e" s="T155">köllörbüt. </ts>
               <ts e="T157" id="Seg_2581" n="e" s="T156">"Kanna </ts>
               <ts e="T158" id="Seg_2583" n="e" s="T157">hanɨːr </ts>
               <ts e="T159" id="Seg_2585" n="e" s="T158">hɨrɨt, </ts>
               <ts e="T160" id="Seg_2587" n="e" s="T159">kör. </ts>
               <ts e="T161" id="Seg_2589" n="e" s="T160">Bu </ts>
               <ts e="T162" id="Seg_2591" n="e" s="T161">ere </ts>
               <ts e="T163" id="Seg_2593" n="e" s="T162">((PAUSE)) </ts>
               <ts e="T164" id="Seg_2595" n="e" s="T163">kolbuja </ts>
               <ts e="T165" id="Seg_2597" n="e" s="T164">ihiger </ts>
               <ts e="T166" id="Seg_2599" n="e" s="T165">uguma </ts>
               <ts e="T167" id="Seg_2601" n="e" s="T166">čömüjegin", </ts>
               <ts e="T168" id="Seg_2603" n="e" s="T167">di͡ebit. </ts>
               <ts e="T169" id="Seg_2605" n="e" s="T168">Ere </ts>
               <ts e="T170" id="Seg_2607" n="e" s="T169">barbɨtɨn </ts>
               <ts e="T171" id="Seg_2609" n="e" s="T170">kenne, </ts>
               <ts e="T172" id="Seg_2611" n="e" s="T171">bu </ts>
               <ts e="T173" id="Seg_2613" n="e" s="T172">dʼaktar </ts>
               <ts e="T174" id="Seg_2615" n="e" s="T173">tehijbekke </ts>
               <ts e="T175" id="Seg_2617" n="e" s="T174">čömüjetin </ts>
               <ts e="T176" id="Seg_2619" n="e" s="T175">ukput. </ts>
               <ts e="T177" id="Seg_2621" n="e" s="T176">Uː </ts>
               <ts e="T178" id="Seg_2623" n="e" s="T177">buːs </ts>
               <ts e="T179" id="Seg_2625" n="e" s="T178">bu͡olbut. </ts>
               <ts e="T180" id="Seg_2627" n="e" s="T179">Araččɨ </ts>
               <ts e="T181" id="Seg_2629" n="e" s="T180">hulbu </ts>
               <ts e="T182" id="Seg_2631" n="e" s="T181">tarpɨt </ts>
               <ts e="T183" id="Seg_2633" n="e" s="T182">čömüjetin. </ts>
               <ts e="T184" id="Seg_2635" n="e" s="T183">Körbüte, </ts>
               <ts e="T185" id="Seg_2637" n="e" s="T184">töjön </ts>
               <ts e="T186" id="Seg_2639" n="e" s="T185">čömüjete </ts>
               <ts e="T187" id="Seg_2641" n="e" s="T186">tuːra </ts>
               <ts e="T188" id="Seg_2643" n="e" s="T187">barbɨt. </ts>
               <ts e="T189" id="Seg_2645" n="e" s="T188">Ontutun </ts>
               <ts e="T190" id="Seg_2647" n="e" s="T189">kam </ts>
               <ts e="T191" id="Seg_2649" n="e" s="T190">baːjan </ts>
               <ts e="T192" id="Seg_2651" n="e" s="T191">baraːn </ts>
               <ts e="T193" id="Seg_2653" n="e" s="T192">oloror. </ts>
               <ts e="T194" id="Seg_2655" n="e" s="T193">Kuttanan </ts>
               <ts e="T195" id="Seg_2657" n="e" s="T194">karaktara </ts>
               <ts e="T196" id="Seg_2659" n="e" s="T195">bɨlčaččɨ </ts>
               <ts e="T197" id="Seg_2661" n="e" s="T196">barbɨttar. </ts>
               <ts e="T198" id="Seg_2663" n="e" s="T197">Uː </ts>
               <ts e="T199" id="Seg_2665" n="e" s="T198">iččite </ts>
               <ts e="T200" id="Seg_2667" n="e" s="T199">kelen </ts>
               <ts e="T201" id="Seg_2669" n="e" s="T200">honno </ts>
               <ts e="T202" id="Seg_2671" n="e" s="T201">taːjda. </ts>
               <ts e="T203" id="Seg_2673" n="e" s="T202">"Mini͡eke </ts>
               <ts e="T204" id="Seg_2675" n="e" s="T203">en </ts>
               <ts e="T205" id="Seg_2677" n="e" s="T204">dʼaktar </ts>
               <ts e="T206" id="Seg_2679" n="e" s="T205">bu͡olbat </ts>
               <ts e="T207" id="Seg_2681" n="e" s="T206">kördükkün", </ts>
               <ts e="T208" id="Seg_2683" n="e" s="T207">di͡ebit. </ts>
               <ts e="T209" id="Seg_2685" n="e" s="T208">Dʼaktarɨ </ts>
               <ts e="T210" id="Seg_2687" n="e" s="T209">muŋ </ts>
               <ts e="T211" id="Seg_2689" n="e" s="T210">ɨraːktaːgɨ </ts>
               <ts e="T212" id="Seg_2691" n="e" s="T211">dʼi͡ege </ts>
               <ts e="T213" id="Seg_2693" n="e" s="T212">kohugar </ts>
               <ts e="T214" id="Seg_2695" n="e" s="T213">illen </ts>
               <ts e="T215" id="Seg_2697" n="e" s="T214">kataːn </ts>
               <ts e="T216" id="Seg_2699" n="e" s="T215">keːspit. </ts>
               <ts e="T217" id="Seg_2701" n="e" s="T216">Aːnɨn </ts>
               <ts e="T218" id="Seg_2703" n="e" s="T217">čipičči </ts>
               <ts e="T219" id="Seg_2705" n="e" s="T218">katɨːs </ts>
               <ts e="T220" id="Seg_2707" n="e" s="T219">ü͡öhünen </ts>
               <ts e="T221" id="Seg_2709" n="e" s="T220">oŋohullubut </ts>
               <ts e="T222" id="Seg_2711" n="e" s="T221">hiliminen </ts>
               <ts e="T223" id="Seg_2713" n="e" s="T222">kam </ts>
               <ts e="T224" id="Seg_2715" n="e" s="T223">bispit. </ts>
               <ts e="T225" id="Seg_2717" n="e" s="T224">Oguru͡o </ts>
               <ts e="T226" id="Seg_2719" n="e" s="T225">Bɨtɨk </ts>
               <ts e="T227" id="Seg_2721" n="e" s="T226">emi͡e </ts>
               <ts e="T228" id="Seg_2723" n="e" s="T227">ilimnene </ts>
               <ts e="T229" id="Seg_2725" n="e" s="T228">kelbit. </ts>
               <ts e="T230" id="Seg_2727" n="e" s="T229">Ojbonugar </ts>
               <ts e="T231" id="Seg_2729" n="e" s="T230">bökčöjörün </ts>
               <ts e="T232" id="Seg_2731" n="e" s="T231">kɨtta </ts>
               <ts e="T233" id="Seg_2733" n="e" s="T232">uː </ts>
               <ts e="T234" id="Seg_2735" n="e" s="T233">iččite </ts>
               <ts e="T235" id="Seg_2737" n="e" s="T234">emi͡e </ts>
               <ts e="T236" id="Seg_2739" n="e" s="T235">bɨtɨgɨttan </ts>
               <ts e="T237" id="Seg_2741" n="e" s="T236">kaban </ts>
               <ts e="T238" id="Seg_2743" n="e" s="T237">ɨlbɨt. </ts>
               <ts e="T239" id="Seg_2745" n="e" s="T238">"Egel </ts>
               <ts e="T240" id="Seg_2747" n="e" s="T239">ikkis </ts>
               <ts e="T241" id="Seg_2749" n="e" s="T240">kɨːskɨn, </ts>
               <ts e="T242" id="Seg_2751" n="e" s="T241">dʼaktar </ts>
               <ts e="T243" id="Seg_2753" n="e" s="T242">gɨnɨ͡akpɨn." </ts>
               <ts e="T244" id="Seg_2755" n="e" s="T243">Ogonnʼor </ts>
               <ts e="T245" id="Seg_2757" n="e" s="T244">muŋu </ts>
               <ts e="T246" id="Seg_2759" n="e" s="T245">bɨstɨ͡a </ts>
               <ts e="T247" id="Seg_2761" n="e" s="T246">du͡o, </ts>
               <ts e="T248" id="Seg_2763" n="e" s="T247">orto </ts>
               <ts e="T249" id="Seg_2765" n="e" s="T248">kɨːhɨn </ts>
               <ts e="T250" id="Seg_2767" n="e" s="T249">bi͡erbit. </ts>
               <ts e="T251" id="Seg_2769" n="e" s="T250">Bu </ts>
               <ts e="T252" id="Seg_2771" n="e" s="T251">dʼaktar </ts>
               <ts e="T253" id="Seg_2773" n="e" s="T252">baːj </ts>
               <ts e="T254" id="Seg_2775" n="e" s="T253">körüneriger </ts>
               <ts e="T255" id="Seg_2777" n="e" s="T254">holoto </ts>
               <ts e="T256" id="Seg_2779" n="e" s="T255">hu͡ok, </ts>
               <ts e="T257" id="Seg_2781" n="e" s="T256">högön </ts>
               <ts e="T258" id="Seg_2783" n="e" s="T257">baraːn </ts>
               <ts e="T259" id="Seg_2785" n="e" s="T258">büppet. </ts>
               <ts e="T260" id="Seg_2787" n="e" s="T259">Biːrde </ts>
               <ts e="T261" id="Seg_2789" n="e" s="T260">maː, </ts>
               <ts e="T262" id="Seg_2791" n="e" s="T261">buːs </ts>
               <ts e="T263" id="Seg_2793" n="e" s="T262">kolbujaga </ts>
               <ts e="T264" id="Seg_2795" n="e" s="T263">egelbit </ts>
               <ts e="T265" id="Seg_2797" n="e" s="T264">uː </ts>
               <ts e="T266" id="Seg_2799" n="e" s="T265">iččite. </ts>
               <ts e="T267" id="Seg_2801" n="e" s="T266">Egelen </ts>
               <ts e="T268" id="Seg_2803" n="e" s="T267">hereppit: </ts>
               <ts e="T269" id="Seg_2805" n="e" s="T268">"Bu </ts>
               <ts e="T270" id="Seg_2807" n="e" s="T269">kolbujaga </ts>
               <ts e="T271" id="Seg_2809" n="e" s="T270">iliːgin </ts>
               <ts e="T272" id="Seg_2811" n="e" s="T271">da, </ts>
               <ts e="T273" id="Seg_2813" n="e" s="T272">čömüjegin </ts>
               <ts e="T274" id="Seg_2815" n="e" s="T273">da </ts>
               <ts e="T275" id="Seg_2817" n="e" s="T274">ugaːjagɨn." </ts>
               <ts e="T276" id="Seg_2819" n="e" s="T275">Uː </ts>
               <ts e="T277" id="Seg_2821" n="e" s="T276">iččite </ts>
               <ts e="T278" id="Seg_2823" n="e" s="T277">hɨrgalana </ts>
               <ts e="T279" id="Seg_2825" n="e" s="T278">barbɨtɨn </ts>
               <ts e="T280" id="Seg_2827" n="e" s="T279">kenne, </ts>
               <ts e="T281" id="Seg_2829" n="e" s="T280">dʼaktara </ts>
               <ts e="T282" id="Seg_2831" n="e" s="T281">buːs </ts>
               <ts e="T283" id="Seg_2833" n="e" s="T282">kolbujaga </ts>
               <ts e="T284" id="Seg_2835" n="e" s="T283">kelen </ts>
               <ts e="T285" id="Seg_2837" n="e" s="T284">čömüjetin </ts>
               <ts e="T286" id="Seg_2839" n="e" s="T285">ukput. </ts>
               <ts e="T287" id="Seg_2841" n="e" s="T286">Ortokuː </ts>
               <ts e="T288" id="Seg_2843" n="e" s="T287">čömüjete </ts>
               <ts e="T289" id="Seg_2845" n="e" s="T288">tuːra </ts>
               <ts e="T290" id="Seg_2847" n="e" s="T289">toŋmut. </ts>
               <ts e="T291" id="Seg_2849" n="e" s="T290">Čömüjetin </ts>
               <ts e="T292" id="Seg_2851" n="e" s="T291">kam </ts>
               <ts e="T293" id="Seg_2853" n="e" s="T292">kelgijbit, </ts>
               <ts e="T294" id="Seg_2855" n="e" s="T293">uː </ts>
               <ts e="T295" id="Seg_2857" n="e" s="T294">iččite </ts>
               <ts e="T296" id="Seg_2859" n="e" s="T295">kelen </ts>
               <ts e="T297" id="Seg_2861" n="e" s="T296">honno </ts>
               <ts e="T298" id="Seg_2863" n="e" s="T297">taːjbɨt. </ts>
               <ts e="T299" id="Seg_2865" n="e" s="T298">"Mini͡eke </ts>
               <ts e="T300" id="Seg_2867" n="e" s="T299">istigene </ts>
               <ts e="T301" id="Seg_2869" n="e" s="T300">hu͡o </ts>
               <ts e="T302" id="Seg_2871" n="e" s="T301">dʼaktar </ts>
               <ts e="T303" id="Seg_2873" n="e" s="T302">naːdata </ts>
               <ts e="T304" id="Seg_2875" n="e" s="T303">hu͡ok", </ts>
               <ts e="T305" id="Seg_2877" n="e" s="T304">di͡en </ts>
               <ts e="T306" id="Seg_2879" n="e" s="T305">baraːn, </ts>
               <ts e="T307" id="Seg_2881" n="e" s="T306">ɨraːktaːgɨ </ts>
               <ts e="T308" id="Seg_2883" n="e" s="T307">dʼi͡e </ts>
               <ts e="T309" id="Seg_2885" n="e" s="T308">kohugar </ts>
               <ts e="T310" id="Seg_2887" n="e" s="T309">kaːjan </ts>
               <ts e="T311" id="Seg_2889" n="e" s="T310">keːspit, </ts>
               <ts e="T312" id="Seg_2891" n="e" s="T311">aːnɨn </ts>
               <ts e="T313" id="Seg_2893" n="e" s="T312">čipičči, </ts>
               <ts e="T314" id="Seg_2895" n="e" s="T313">katɨːs </ts>
               <ts e="T315" id="Seg_2897" n="e" s="T314">ü͡öhünen </ts>
               <ts e="T316" id="Seg_2899" n="e" s="T315">oŋohullubut </ts>
               <ts e="T317" id="Seg_2901" n="e" s="T316">hiliminen </ts>
               <ts e="T318" id="Seg_2903" n="e" s="T317">kam </ts>
               <ts e="T319" id="Seg_2905" n="e" s="T318">bispit. </ts>
               <ts e="T320" id="Seg_2907" n="e" s="T319">Oguru͡o </ts>
               <ts e="T321" id="Seg_2909" n="e" s="T320">Bɨtɨk </ts>
               <ts e="T322" id="Seg_2911" n="e" s="T321">emi͡e </ts>
               <ts e="T323" id="Seg_2913" n="e" s="T322">ilim </ts>
               <ts e="T324" id="Seg_2915" n="e" s="T323">körüne </ts>
               <ts e="T325" id="Seg_2917" n="e" s="T324">kelbit. </ts>
               <ts e="T326" id="Seg_2919" n="e" s="T325">Ojbonugar </ts>
               <ts e="T327" id="Seg_2921" n="e" s="T326">bökčöjörün </ts>
               <ts e="T328" id="Seg_2923" n="e" s="T327">kɨtta </ts>
               <ts e="T329" id="Seg_2925" n="e" s="T328">uː </ts>
               <ts e="T330" id="Seg_2927" n="e" s="T329">iččite </ts>
               <ts e="T331" id="Seg_2929" n="e" s="T330">emi͡e </ts>
               <ts e="T332" id="Seg_2931" n="e" s="T331">bɨtɨgɨttan </ts>
               <ts e="T333" id="Seg_2933" n="e" s="T332">kaban </ts>
               <ts e="T334" id="Seg_2935" n="e" s="T333">ɨlbɨt. </ts>
               <ts e="T335" id="Seg_2937" n="e" s="T334">"Egel </ts>
               <ts e="T336" id="Seg_2939" n="e" s="T335">ɨlgɨn </ts>
               <ts e="T337" id="Seg_2941" n="e" s="T336">kɨːskɨn, </ts>
               <ts e="T338" id="Seg_2943" n="e" s="T337">dʼaktar </ts>
               <ts e="T339" id="Seg_2945" n="e" s="T338">gɨnɨ͡akpɨn", </ts>
               <ts e="T340" id="Seg_2947" n="e" s="T339">di͡ebit. </ts>
               <ts e="T341" id="Seg_2949" n="e" s="T340">Muŋu </ts>
               <ts e="T342" id="Seg_2951" n="e" s="T341">bɨstɨ͡a </ts>
               <ts e="T343" id="Seg_2953" n="e" s="T342">du͡o, </ts>
               <ts e="T344" id="Seg_2955" n="e" s="T343">ɨlgɨn </ts>
               <ts e="T345" id="Seg_2957" n="e" s="T344">kɨːhɨn </ts>
               <ts e="T346" id="Seg_2959" n="e" s="T345">ilpit. </ts>
               <ts e="T347" id="Seg_2961" n="e" s="T346">Baran </ts>
               <ts e="T348" id="Seg_2963" n="e" s="T347">ihenner </ts>
               <ts e="T349" id="Seg_2965" n="e" s="T348">kɨːha </ts>
               <ts e="T350" id="Seg_2967" n="e" s="T349">di͡ebit: </ts>
               <ts e="T351" id="Seg_2969" n="e" s="T350">"Kuttanɨma, </ts>
               <ts e="T352" id="Seg_2971" n="e" s="T351">teːte. </ts>
               <ts e="T353" id="Seg_2973" n="e" s="T352">Min </ts>
               <ts e="T354" id="Seg_2975" n="e" s="T353">honno </ts>
               <ts e="T355" id="Seg_2977" n="e" s="T354">keli͡em, </ts>
               <ts e="T356" id="Seg_2979" n="e" s="T355">edʼiːjderbin </ts>
               <ts e="T357" id="Seg_2981" n="e" s="T356">emi͡e </ts>
               <ts e="T358" id="Seg_2983" n="e" s="T357">egeli͡em. </ts>
               <ts e="T359" id="Seg_2985" n="e" s="T358">Uː </ts>
               <ts e="T360" id="Seg_2987" n="e" s="T359">iččite </ts>
               <ts e="T361" id="Seg_2989" n="e" s="T360">miniginneːger </ts>
               <ts e="T362" id="Seg_2991" n="e" s="T361">kubulgata </ts>
               <ts e="T363" id="Seg_2993" n="e" s="T362">hu͡o </ts>
               <ts e="T364" id="Seg_2995" n="e" s="T363">bu͡olu͡o, </ts>
               <ts e="T365" id="Seg_2997" n="e" s="T364">badaga." </ts>
               <ts e="T366" id="Seg_2999" n="e" s="T365">Uː </ts>
               <ts e="T367" id="Seg_3001" n="e" s="T366">iččite </ts>
               <ts e="T368" id="Seg_3003" n="e" s="T367">ühüs </ts>
               <ts e="T369" id="Seg_3005" n="e" s="T368">dʼaktarɨn </ts>
               <ts e="T370" id="Seg_3007" n="e" s="T369">buːs </ts>
               <ts e="T371" id="Seg_3009" n="e" s="T370">kolbujaga </ts>
               <ts e="T372" id="Seg_3011" n="e" s="T371">egelen </ts>
               <ts e="T373" id="Seg_3013" n="e" s="T372">baraːn </ts>
               <ts e="T374" id="Seg_3015" n="e" s="T373">di͡ebit: </ts>
               <ts e="T375" id="Seg_3017" n="e" s="T374">"Bu </ts>
               <ts e="T376" id="Seg_3019" n="e" s="T375">kolbujaga </ts>
               <ts e="T377" id="Seg_3021" n="e" s="T376">iliːgin </ts>
               <ts e="T378" id="Seg_3023" n="e" s="T377">uguma. </ts>
               <ts e="T379" id="Seg_3025" n="e" s="T378">Tönnön </ts>
               <ts e="T380" id="Seg_3027" n="e" s="T379">kellekpine, </ts>
               <ts e="T381" id="Seg_3029" n="e" s="T380">körü͡öm. </ts>
               <ts e="T382" id="Seg_3031" n="e" s="T381">Minigin </ts>
               <ts e="T383" id="Seg_3033" n="e" s="T382">albɨnnɨ͡aŋ </ts>
               <ts e="T384" id="Seg_3035" n="e" s="T383">hu͡oga." </ts>
               <ts e="T385" id="Seg_3037" n="e" s="T384">"Togo </ts>
               <ts e="T386" id="Seg_3039" n="e" s="T385">minigin </ts>
               <ts e="T387" id="Seg_3041" n="e" s="T386">hereteːr", </ts>
               <ts e="T388" id="Seg_3043" n="e" s="T387">diː </ts>
               <ts e="T389" id="Seg_3045" n="e" s="T388">hanaːbɨt </ts>
               <ts e="T390" id="Seg_3047" n="e" s="T389">bu </ts>
               <ts e="T391" id="Seg_3049" n="e" s="T390">dʼaktar. </ts>
               <ts e="T392" id="Seg_3051" n="e" s="T391">"Tu͡ok </ts>
               <ts e="T393" id="Seg_3053" n="e" s="T392">bu͡olu͡oj </ts>
               <ts e="T394" id="Seg_3055" n="e" s="T393">dʼe </ts>
               <ts e="T395" id="Seg_3057" n="e" s="T394">iliːbin </ts>
               <ts e="T396" id="Seg_3059" n="e" s="T395">uktakpɨna, </ts>
               <ts e="T397" id="Seg_3061" n="e" s="T396">uː </ts>
               <ts e="T398" id="Seg_3063" n="e" s="T397">uː </ts>
               <ts e="T399" id="Seg_3065" n="e" s="T398">kördük. </ts>
               <ts e="T400" id="Seg_3067" n="e" s="T399">Badaga, </ts>
               <ts e="T401" id="Seg_3069" n="e" s="T400">tu͡ok </ts>
               <ts e="T402" id="Seg_3071" n="e" s="T401">ere </ts>
               <ts e="T403" id="Seg_3073" n="e" s="T402">albaha </ts>
               <ts e="T404" id="Seg_3075" n="e" s="T403">baːra </ts>
               <ts e="T405" id="Seg_3077" n="e" s="T404">bu͡olu͡o." </ts>
               <ts e="T406" id="Seg_3079" n="e" s="T405">Nʼelmanɨ </ts>
               <ts e="T407" id="Seg_3081" n="e" s="T406">kaban </ts>
               <ts e="T408" id="Seg_3083" n="e" s="T407">ɨlan </ts>
               <ts e="T409" id="Seg_3085" n="e" s="T408">baraːn </ts>
               <ts e="T410" id="Seg_3087" n="e" s="T409">kuturugun </ts>
               <ts e="T411" id="Seg_3089" n="e" s="T410">ukput </ts>
               <ts e="T412" id="Seg_3091" n="e" s="T411">buːs </ts>
               <ts e="T413" id="Seg_3093" n="e" s="T412">kolbuja </ts>
               <ts e="T414" id="Seg_3095" n="e" s="T413">ihiger. </ts>
               <ts e="T415" id="Seg_3097" n="e" s="T414">Nʼelma </ts>
               <ts e="T416" id="Seg_3099" n="e" s="T415">kuturuga </ts>
               <ts e="T417" id="Seg_3101" n="e" s="T416">tuːra </ts>
               <ts e="T418" id="Seg_3103" n="e" s="T417">toston </ts>
               <ts e="T419" id="Seg_3105" n="e" s="T418">kaːlbɨt. </ts>
               <ts e="T420" id="Seg_3107" n="e" s="T419">Oguru͡o </ts>
               <ts e="T421" id="Seg_3109" n="e" s="T420">Bɨtɨk </ts>
               <ts e="T422" id="Seg_3111" n="e" s="T421">kɨːha </ts>
               <ts e="T423" id="Seg_3113" n="e" s="T422">taːjda, </ts>
               <ts e="T424" id="Seg_3115" n="e" s="T423">togo </ts>
               <ts e="T425" id="Seg_3117" n="e" s="T424">uː </ts>
               <ts e="T426" id="Seg_3119" n="e" s="T425">iččite </ts>
               <ts e="T427" id="Seg_3121" n="e" s="T426">hereppitin. </ts>
               <ts e="T428" id="Seg_3123" n="e" s="T427">Kolbuja </ts>
               <ts e="T429" id="Seg_3125" n="e" s="T428">attɨtɨgar </ts>
               <ts e="T430" id="Seg_3127" n="e" s="T429">oloron </ts>
               <ts e="T431" id="Seg_3129" n="e" s="T430">baraːn, </ts>
               <ts e="T432" id="Seg_3131" n="e" s="T431">ilim </ts>
               <ts e="T433" id="Seg_3133" n="e" s="T432">abɨraktana </ts>
               <ts e="T434" id="Seg_3135" n="e" s="T433">oloror. </ts>
               <ts e="T435" id="Seg_3137" n="e" s="T434">Uː </ts>
               <ts e="T436" id="Seg_3139" n="e" s="T435">iččite </ts>
               <ts e="T437" id="Seg_3141" n="e" s="T436">kelen </ts>
               <ts e="T438" id="Seg_3143" n="e" s="T437">höktö: </ts>
               <ts e="T439" id="Seg_3145" n="e" s="T438">"Tu͡ok </ts>
               <ts e="T440" id="Seg_3147" n="e" s="T439">ilimin </ts>
               <ts e="T441" id="Seg_3149" n="e" s="T440">abɨraktɨːgɨn? </ts>
               <ts e="T442" id="Seg_3151" n="e" s="T441">Uː </ts>
               <ts e="T443" id="Seg_3153" n="e" s="T442">ihiger </ts>
               <ts e="T444" id="Seg_3155" n="e" s="T443">balɨk </ts>
               <ts e="T445" id="Seg_3157" n="e" s="T444">ügüs, </ts>
               <ts e="T446" id="Seg_3159" n="e" s="T445">ilime </ts>
               <ts e="T447" id="Seg_3161" n="e" s="T446">da </ts>
               <ts e="T448" id="Seg_3163" n="e" s="T447">hu͡ok </ts>
               <ts e="T449" id="Seg_3165" n="e" s="T448">min </ts>
               <ts e="T450" id="Seg_3167" n="e" s="T449">eni͡eke </ts>
               <ts e="T451" id="Seg_3169" n="e" s="T450">kahɨ </ts>
               <ts e="T452" id="Seg_3171" n="e" s="T451">hanɨːrɨ </ts>
               <ts e="T453" id="Seg_3173" n="e" s="T452">egeli͡em." </ts>
               <ts e="T454" id="Seg_3175" n="e" s="T453">"Balɨk </ts>
               <ts e="T455" id="Seg_3177" n="e" s="T454">ügüs </ts>
               <ts e="T456" id="Seg_3179" n="e" s="T455">bu͡olan </ts>
               <ts e="T457" id="Seg_3181" n="e" s="T456">baraːn", </ts>
               <ts e="T458" id="Seg_3183" n="e" s="T457">diːr </ts>
               <ts e="T459" id="Seg_3185" n="e" s="T458">dʼaktara, </ts>
               <ts e="T460" id="Seg_3187" n="e" s="T459">"biːr </ts>
               <ts e="T461" id="Seg_3189" n="e" s="T460">daː </ts>
               <ts e="T462" id="Seg_3191" n="e" s="T461">taba </ts>
               <ts e="T463" id="Seg_3193" n="e" s="T462">ete </ts>
               <ts e="T464" id="Seg_3195" n="e" s="T463">hu͡ok. </ts>
               <ts e="T465" id="Seg_3197" n="e" s="T464">Min </ts>
               <ts e="T466" id="Seg_3199" n="e" s="T465">taba </ts>
               <ts e="T467" id="Seg_3201" n="e" s="T466">etiger </ts>
               <ts e="T468" id="Seg_3203" n="e" s="T467">ü͡öremmippin, </ts>
               <ts e="T469" id="Seg_3205" n="e" s="T468">taba </ts>
               <ts e="T470" id="Seg_3207" n="e" s="T469">etin </ts>
               <ts e="T471" id="Seg_3209" n="e" s="T470">hi͡ekpin </ts>
               <ts e="T472" id="Seg_3211" n="e" s="T471">bagarabɨn. </ts>
               <ts e="T473" id="Seg_3213" n="e" s="T472">Bu </ts>
               <ts e="T474" id="Seg_3215" n="e" s="T473">buːs </ts>
               <ts e="T475" id="Seg_3217" n="e" s="T474">kolbuja </ts>
               <ts e="T476" id="Seg_3219" n="e" s="T475">ihiger </ts>
               <ts e="T477" id="Seg_3221" n="e" s="T476">taba </ts>
               <ts e="T478" id="Seg_3223" n="e" s="T477">ügüs. </ts>
               <ts e="T479" id="Seg_3225" n="e" s="T478">Balɨk </ts>
               <ts e="T480" id="Seg_3227" n="e" s="T479">kördük </ts>
               <ts e="T481" id="Seg_3229" n="e" s="T480">karbɨː </ts>
               <ts e="T482" id="Seg_3231" n="e" s="T481">hɨldʼallar." </ts>
               <ts e="T483" id="Seg_3233" n="e" s="T482">Uː </ts>
               <ts e="T484" id="Seg_3235" n="e" s="T483">iččite </ts>
               <ts e="T485" id="Seg_3237" n="e" s="T484">dʼe </ts>
               <ts e="T486" id="Seg_3239" n="e" s="T485">küler: </ts>
               <ts e="T487" id="Seg_3241" n="e" s="T486">"Bu </ts>
               <ts e="T488" id="Seg_3243" n="e" s="T487">buːs </ts>
               <ts e="T489" id="Seg_3245" n="e" s="T488">kolbujaga </ts>
               <ts e="T490" id="Seg_3247" n="e" s="T489">tu͡ok </ts>
               <ts e="T491" id="Seg_3249" n="e" s="T490">tabata </ts>
               <ts e="T492" id="Seg_3251" n="e" s="T491">keli͡ej, </ts>
               <ts e="T493" id="Seg_3253" n="e" s="T492">manna </ts>
               <ts e="T494" id="Seg_3255" n="e" s="T493">uː </ts>
               <ts e="T495" id="Seg_3257" n="e" s="T494">ere. </ts>
               <ts e="T496" id="Seg_3259" n="e" s="T495">Min </ts>
               <ts e="T497" id="Seg_3261" n="e" s="T496">bilebin </ts>
               <ts e="T498" id="Seg_3263" n="e" s="T497">eːt." </ts>
               <ts e="T499" id="Seg_3265" n="e" s="T498">Dʼaktara </ts>
               <ts e="T500" id="Seg_3267" n="e" s="T499">di͡ebit: </ts>
               <ts e="T501" id="Seg_3269" n="e" s="T500">"Min </ts>
               <ts e="T502" id="Seg_3271" n="e" s="T501">körbütüm, </ts>
               <ts e="T503" id="Seg_3273" n="e" s="T502">tabalar </ts>
               <ts e="T504" id="Seg_3275" n="e" s="T503">karbɨː </ts>
               <ts e="T505" id="Seg_3277" n="e" s="T504">hɨldʼallar. </ts>
               <ts e="T506" id="Seg_3279" n="e" s="T505">Horoktor </ts>
               <ts e="T507" id="Seg_3281" n="e" s="T506">čeːlkeːler, </ts>
               <ts e="T508" id="Seg_3283" n="e" s="T507">horoktor </ts>
               <ts e="T509" id="Seg_3285" n="e" s="T508">bugdiːler, </ts>
               <ts e="T510" id="Seg_3287" n="e" s="T509">aŋardara </ts>
               <ts e="T511" id="Seg_3289" n="e" s="T510">karalar. </ts>
               <ts e="T512" id="Seg_3291" n="e" s="T511">Bütün </ts>
               <ts e="T513" id="Seg_3293" n="e" s="T512">ü͡ör." </ts>
               <ts e="T514" id="Seg_3295" n="e" s="T513">"Aŋɨr </ts>
               <ts e="T515" id="Seg_3297" n="e" s="T514">dʼaktargɨn </ts>
               <ts e="T516" id="Seg_3299" n="e" s="T515">ebit", </ts>
               <ts e="T517" id="Seg_3301" n="e" s="T516">kɨjŋanna </ts>
               <ts e="T518" id="Seg_3303" n="e" s="T517">uː </ts>
               <ts e="T519" id="Seg_3305" n="e" s="T518">iččite," </ts>
               <ts e="T520" id="Seg_3307" n="e" s="T519">tu͡ok </ts>
               <ts e="T521" id="Seg_3309" n="e" s="T520">tabata </ts>
               <ts e="T522" id="Seg_3311" n="e" s="T521">uː </ts>
               <ts e="T523" id="Seg_3313" n="e" s="T522">ihiger </ts>
               <ts e="T524" id="Seg_3315" n="e" s="T523">hɨldʼɨ͡aj?" </ts>
               <ts e="T525" id="Seg_3317" n="e" s="T524">"Bejem </ts>
               <ts e="T526" id="Seg_3319" n="e" s="T525">bilegin, </ts>
               <ts e="T527" id="Seg_3321" n="e" s="T526">bihigi </ts>
               <ts e="T528" id="Seg_3323" n="e" s="T527">uː </ts>
               <ts e="T529" id="Seg_3325" n="e" s="T528">ihiger </ts>
               <ts e="T530" id="Seg_3327" n="e" s="T529">olorobut. </ts>
               <ts e="T531" id="Seg_3329" n="e" s="T530">Ol </ts>
               <ts e="T532" id="Seg_3331" n="e" s="T531">ihin </ts>
               <ts e="T533" id="Seg_3333" n="e" s="T532">tabalar </ts>
               <ts e="T534" id="Seg_3335" n="e" s="T533">emi͡e </ts>
               <ts e="T535" id="Seg_3337" n="e" s="T534">uː </ts>
               <ts e="T536" id="Seg_3339" n="e" s="T535">ihiger </ts>
               <ts e="T537" id="Seg_3341" n="e" s="T536">hɨldʼallar", </ts>
               <ts e="T538" id="Seg_3343" n="e" s="T537">di͡ebit </ts>
               <ts e="T539" id="Seg_3345" n="e" s="T538">dʼaktara, </ts>
               <ts e="T540" id="Seg_3347" n="e" s="T539">"kör </ts>
               <ts e="T541" id="Seg_3349" n="e" s="T540">dʼe, </ts>
               <ts e="T542" id="Seg_3351" n="e" s="T541">kolbuja </ts>
               <ts e="T543" id="Seg_3353" n="e" s="T542">ihin." </ts>
               <ts e="T544" id="Seg_3355" n="e" s="T543">Uː </ts>
               <ts e="T545" id="Seg_3357" n="e" s="T544">iččite </ts>
               <ts e="T546" id="Seg_3359" n="e" s="T545">kɨjŋanan </ts>
               <ts e="T547" id="Seg_3361" n="e" s="T546">baraːn, </ts>
               <ts e="T548" id="Seg_3363" n="e" s="T547">umnan </ts>
               <ts e="T549" id="Seg_3365" n="e" s="T548">keːspit. </ts>
               <ts e="T550" id="Seg_3367" n="e" s="T549">Kolbujatɨgar </ts>
               <ts e="T551" id="Seg_3369" n="e" s="T550">öŋöjbüt, </ts>
               <ts e="T552" id="Seg_3371" n="e" s="T551">tabalarɨ </ts>
               <ts e="T553" id="Seg_3373" n="e" s="T552">köröːrü. </ts>
               <ts e="T554" id="Seg_3375" n="e" s="T553">Dʼaktara </ts>
               <ts e="T555" id="Seg_3377" n="e" s="T554">kelen, </ts>
               <ts e="T556" id="Seg_3379" n="e" s="T555">menʼiːtin </ts>
               <ts e="T557" id="Seg_3381" n="e" s="T556">uː </ts>
               <ts e="T558" id="Seg_3383" n="e" s="T557">ihiger </ts>
               <ts e="T559" id="Seg_3385" n="e" s="T558">hamnarɨ </ts>
               <ts e="T560" id="Seg_3387" n="e" s="T559">battaːbɨt. </ts>
               <ts e="T561" id="Seg_3389" n="e" s="T560">Uː </ts>
               <ts e="T562" id="Seg_3391" n="e" s="T561">iččitin </ts>
               <ts e="T563" id="Seg_3393" n="e" s="T562">menʼiːte </ts>
               <ts e="T564" id="Seg_3395" n="e" s="T563">tuːra </ts>
               <ts e="T565" id="Seg_3397" n="e" s="T564">barbɨt, </ts>
               <ts e="T566" id="Seg_3399" n="e" s="T565">honno </ts>
               <ts e="T567" id="Seg_3401" n="e" s="T566">ölbüt. </ts>
               <ts e="T568" id="Seg_3403" n="e" s="T567">Oguru͡o </ts>
               <ts e="T569" id="Seg_3405" n="e" s="T568">Bɨtɨk </ts>
               <ts e="T570" id="Seg_3407" n="e" s="T569">ɨlgɨn </ts>
               <ts e="T571" id="Seg_3409" n="e" s="T570">kɨːha </ts>
               <ts e="T572" id="Seg_3411" n="e" s="T571">aːntan </ts>
               <ts e="T573" id="Seg_3413" n="e" s="T572">aːnɨ </ts>
               <ts e="T574" id="Seg_3415" n="e" s="T573">kerije </ts>
               <ts e="T575" id="Seg_3417" n="e" s="T574">hɨldʼan </ts>
               <ts e="T576" id="Seg_3419" n="e" s="T575">edʼiːjderin </ts>
               <ts e="T577" id="Seg_3421" n="e" s="T576">bulbut. </ts>
               <ts e="T578" id="Seg_3423" n="e" s="T577">Aːnnarɨn </ts>
               <ts e="T579" id="Seg_3425" n="e" s="T578">hilimnerin </ts>
               <ts e="T580" id="Seg_3427" n="e" s="T579">bahaːgɨnan </ts>
               <ts e="T581" id="Seg_3429" n="e" s="T580">kɨhɨjan </ts>
               <ts e="T582" id="Seg_3431" n="e" s="T581">ɨlan </ts>
               <ts e="T583" id="Seg_3433" n="e" s="T582">boskoloːbut. </ts>
               <ts e="T584" id="Seg_3435" n="e" s="T583">Ühü͡önnere </ts>
               <ts e="T585" id="Seg_3437" n="e" s="T584">uraha </ts>
               <ts e="T586" id="Seg_3439" n="e" s="T585">dʼi͡eleriger </ts>
               <ts e="T587" id="Seg_3441" n="e" s="T586">kelbitter. </ts>
               <ts e="T588" id="Seg_3443" n="e" s="T587">Oguru͡o </ts>
               <ts e="T589" id="Seg_3445" n="e" s="T588">Bɨtɨk </ts>
               <ts e="T590" id="Seg_3447" n="e" s="T589">ü͡ören </ts>
               <ts e="T591" id="Seg_3449" n="e" s="T590">kɨrgɨttarɨgar </ts>
               <ts e="T592" id="Seg_3451" n="e" s="T591">kɨspɨt </ts>
               <ts e="T593" id="Seg_3453" n="e" s="T592">kɨhar, </ts>
               <ts e="T594" id="Seg_3455" n="e" s="T593">busput </ts>
               <ts e="T595" id="Seg_3457" n="e" s="T594">balɨgɨ </ts>
               <ts e="T596" id="Seg_3459" n="e" s="T595">ahatar. </ts>
               <ts e="T597" id="Seg_3461" n="e" s="T596">Ulakan </ts>
               <ts e="T598" id="Seg_3463" n="e" s="T597">kɨːha </ts>
               <ts e="T599" id="Seg_3465" n="e" s="T598">tojon </ts>
               <ts e="T600" id="Seg_3467" n="e" s="T599">čömüjetin </ts>
               <ts e="T601" id="Seg_3469" n="e" s="T600">körön, </ts>
               <ts e="T602" id="Seg_3471" n="e" s="T601">onuga </ts>
               <ts e="T603" id="Seg_3473" n="e" s="T602">orto </ts>
               <ts e="T604" id="Seg_3475" n="e" s="T603">kɨːha </ts>
               <ts e="T605" id="Seg_3477" n="e" s="T604">ortokuː </ts>
               <ts e="T606" id="Seg_3479" n="e" s="T605">čömüjetin </ts>
               <ts e="T607" id="Seg_3481" n="e" s="T606">körön, </ts>
               <ts e="T608" id="Seg_3483" n="e" s="T607">oččogo </ts>
               <ts e="T609" id="Seg_3485" n="e" s="T608">ere </ts>
               <ts e="T610" id="Seg_3487" n="e" s="T609">uː </ts>
               <ts e="T611" id="Seg_3489" n="e" s="T610">iččitin </ts>
               <ts e="T612" id="Seg_3491" n="e" s="T611">öjdüːller. </ts>
               <ts e="T613" id="Seg_3493" n="e" s="T612">Oloŋkoloːbuta </ts>
               <ts e="T614" id="Seg_3495" n="e" s="T613">Ogdu͡o </ts>
               <ts e="T615" id="Seg_3497" n="e" s="T614">Aksʼonava. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T5" id="Seg_3498" s="T0">AkEE_19900810_PearlBeard_flk.001 (001.001)</ta>
            <ta e="T9" id="Seg_3499" s="T5">AkEE_19900810_PearlBeard_flk.002 (001.002)</ta>
            <ta e="T16" id="Seg_3500" s="T9">AkEE_19900810_PearlBeard_flk.003 (001.003)</ta>
            <ta e="T21" id="Seg_3501" s="T16">AkEE_19900810_PearlBeard_flk.004 (001.004)</ta>
            <ta e="T28" id="Seg_3502" s="T21">AkEE_19900810_PearlBeard_flk.005 (001.005)</ta>
            <ta e="T32" id="Seg_3503" s="T28">AkEE_19900810_PearlBeard_flk.006 (001.006)</ta>
            <ta e="T35" id="Seg_3504" s="T32">AkEE_19900810_PearlBeard_flk.007 (001.007)</ta>
            <ta e="T39" id="Seg_3505" s="T35">AkEE_19900810_PearlBeard_flk.008 (001.008)</ta>
            <ta e="T45" id="Seg_3506" s="T39">AkEE_19900810_PearlBeard_flk.009 (001.009)</ta>
            <ta e="T53" id="Seg_3507" s="T45">AkEE_19900810_PearlBeard_flk.010 (001.010)</ta>
            <ta e="T55" id="Seg_3508" s="T53">AkEE_19900810_PearlBeard_flk.011 (001.011)</ta>
            <ta e="T59" id="Seg_3509" s="T55">AkEE_19900810_PearlBeard_flk.012 (001.012)</ta>
            <ta e="T63" id="Seg_3510" s="T59">AkEE_19900810_PearlBeard_flk.013 (001.014)</ta>
            <ta e="T73" id="Seg_3511" s="T63">AkEE_19900810_PearlBeard_flk.014 (001.015)</ta>
            <ta e="T79" id="Seg_3512" s="T73">AkEE_19900810_PearlBeard_flk.015 (001.017)</ta>
            <ta e="T85" id="Seg_3513" s="T79">AkEE_19900810_PearlBeard_flk.016 (001.018)</ta>
            <ta e="T87" id="Seg_3514" s="T85">AkEE_19900810_PearlBeard_flk.017 (001.020)</ta>
            <ta e="T94" id="Seg_3515" s="T87">AkEE_19900810_PearlBeard_flk.018 (001.021)</ta>
            <ta e="T103" id="Seg_3516" s="T94">AkEE_19900810_PearlBeard_flk.019 (001.022)</ta>
            <ta e="T107" id="Seg_3517" s="T103">AkEE_19900810_PearlBeard_flk.020 (001.023)</ta>
            <ta e="T112" id="Seg_3518" s="T107">AkEE_19900810_PearlBeard_flk.021 (001.024)</ta>
            <ta e="T115" id="Seg_3519" s="T112">AkEE_19900810_PearlBeard_flk.022 (001.025)</ta>
            <ta e="T120" id="Seg_3520" s="T115">AkEE_19900810_PearlBeard_flk.023 (001.026)</ta>
            <ta e="T125" id="Seg_3521" s="T120">AkEE_19900810_PearlBeard_flk.024 (001.027)</ta>
            <ta e="T131" id="Seg_3522" s="T125">AkEE_19900810_PearlBeard_flk.025 (001.028)</ta>
            <ta e="T136" id="Seg_3523" s="T131">AkEE_19900810_PearlBeard_flk.026 (001.029)</ta>
            <ta e="T143" id="Seg_3524" s="T136">AkEE_19900810_PearlBeard_flk.027 (001.030)</ta>
            <ta e="T149" id="Seg_3525" s="T143">AkEE_19900810_PearlBeard_flk.028 (001.031)</ta>
            <ta e="T156" id="Seg_3526" s="T149">AkEE_19900810_PearlBeard_flk.029 (001.032)</ta>
            <ta e="T160" id="Seg_3527" s="T156">AkEE_19900810_PearlBeard_flk.030 (001.033)</ta>
            <ta e="T168" id="Seg_3528" s="T160">AkEE_19900810_PearlBeard_flk.031 (001.034)</ta>
            <ta e="T176" id="Seg_3529" s="T168">AkEE_19900810_PearlBeard_flk.032 (001.035)</ta>
            <ta e="T179" id="Seg_3530" s="T176">AkEE_19900810_PearlBeard_flk.033 (001.036)</ta>
            <ta e="T183" id="Seg_3531" s="T179">AkEE_19900810_PearlBeard_flk.034 (001.037)</ta>
            <ta e="T188" id="Seg_3532" s="T183">AkEE_19900810_PearlBeard_flk.035 (001.038)</ta>
            <ta e="T193" id="Seg_3533" s="T188">AkEE_19900810_PearlBeard_flk.036 (001.039)</ta>
            <ta e="T197" id="Seg_3534" s="T193">AkEE_19900810_PearlBeard_flk.037 (001.040)</ta>
            <ta e="T202" id="Seg_3535" s="T197">AkEE_19900810_PearlBeard_flk.038 (001.041)</ta>
            <ta e="T208" id="Seg_3536" s="T202">AkEE_19900810_PearlBeard_flk.039 (001.042)</ta>
            <ta e="T216" id="Seg_3537" s="T208">AkEE_19900810_PearlBeard_flk.040 (001.043)</ta>
            <ta e="T224" id="Seg_3538" s="T216">AkEE_19900810_PearlBeard_flk.041 (001.044)</ta>
            <ta e="T229" id="Seg_3539" s="T224">AkEE_19900810_PearlBeard_flk.042 (001.045)</ta>
            <ta e="T238" id="Seg_3540" s="T229">AkEE_19900810_PearlBeard_flk.043 (001.046)</ta>
            <ta e="T243" id="Seg_3541" s="T238">AkEE_19900810_PearlBeard_flk.044 (001.047)</ta>
            <ta e="T250" id="Seg_3542" s="T243">AkEE_19900810_PearlBeard_flk.045 (001.048)</ta>
            <ta e="T259" id="Seg_3543" s="T250">AkEE_19900810_PearlBeard_flk.046 (001.049)</ta>
            <ta e="T266" id="Seg_3544" s="T259">AkEE_19900810_PearlBeard_flk.047 (001.050)</ta>
            <ta e="T268" id="Seg_3545" s="T266">AkEE_19900810_PearlBeard_flk.048 (001.051)</ta>
            <ta e="T275" id="Seg_3546" s="T268">AkEE_19900810_PearlBeard_flk.049 (001.052)</ta>
            <ta e="T286" id="Seg_3547" s="T275">AkEE_19900810_PearlBeard_flk.050 (001.053)</ta>
            <ta e="T290" id="Seg_3548" s="T286">AkEE_19900810_PearlBeard_flk.051 (001.054)</ta>
            <ta e="T298" id="Seg_3549" s="T290">AkEE_19900810_PearlBeard_flk.052 (001.055)</ta>
            <ta e="T319" id="Seg_3550" s="T298">AkEE_19900810_PearlBeard_flk.053 (001.056)</ta>
            <ta e="T325" id="Seg_3551" s="T319">AkEE_19900810_PearlBeard_flk.054 (001.058)</ta>
            <ta e="T334" id="Seg_3552" s="T325">AkEE_19900810_PearlBeard_flk.055 (001.059)</ta>
            <ta e="T340" id="Seg_3553" s="T334">AkEE_19900810_PearlBeard_flk.056 (001.060)</ta>
            <ta e="T346" id="Seg_3554" s="T340">AkEE_19900810_PearlBeard_flk.057 (001.061)</ta>
            <ta e="T350" id="Seg_3555" s="T346">AkEE_19900810_PearlBeard_flk.058 (001.062)</ta>
            <ta e="T352" id="Seg_3556" s="T350">AkEE_19900810_PearlBeard_flk.059 (001.063)</ta>
            <ta e="T358" id="Seg_3557" s="T352">AkEE_19900810_PearlBeard_flk.060 (001.064)</ta>
            <ta e="T365" id="Seg_3558" s="T358">AkEE_19900810_PearlBeard_flk.061 (001.065)</ta>
            <ta e="T374" id="Seg_3559" s="T365">AkEE_19900810_PearlBeard_flk.062 (001.066)</ta>
            <ta e="T378" id="Seg_3560" s="T374">AkEE_19900810_PearlBeard_flk.063 (001.067)</ta>
            <ta e="T381" id="Seg_3561" s="T378">AkEE_19900810_PearlBeard_flk.064 (001.068)</ta>
            <ta e="T384" id="Seg_3562" s="T381">AkEE_19900810_PearlBeard_flk.065 (001.069)</ta>
            <ta e="T391" id="Seg_3563" s="T384">AkEE_19900810_PearlBeard_flk.066 (001.070)</ta>
            <ta e="T399" id="Seg_3564" s="T391">AkEE_19900810_PearlBeard_flk.067 (001.071)</ta>
            <ta e="T405" id="Seg_3565" s="T399">AkEE_19900810_PearlBeard_flk.068 (001.072)</ta>
            <ta e="T414" id="Seg_3566" s="T405">AkEE_19900810_PearlBeard_flk.069 (001.073)</ta>
            <ta e="T419" id="Seg_3567" s="T414">AkEE_19900810_PearlBeard_flk.070 (001.074)</ta>
            <ta e="T427" id="Seg_3568" s="T419">AkEE_19900810_PearlBeard_flk.071 (001.075)</ta>
            <ta e="T434" id="Seg_3569" s="T427">AkEE_19900810_PearlBeard_flk.072 (001.076)</ta>
            <ta e="T438" id="Seg_3570" s="T434">AkEE_19900810_PearlBeard_flk.073 (001.077)</ta>
            <ta e="T441" id="Seg_3571" s="T438">AkEE_19900810_PearlBeard_flk.074 (001.078)</ta>
            <ta e="T453" id="Seg_3572" s="T441">AkEE_19900810_PearlBeard_flk.075 (001.079)</ta>
            <ta e="T464" id="Seg_3573" s="T453">AkEE_19900810_PearlBeard_flk.076 (001.080)</ta>
            <ta e="T472" id="Seg_3574" s="T464">AkEE_19900810_PearlBeard_flk.077 (001.082)</ta>
            <ta e="T478" id="Seg_3575" s="T472">AkEE_19900810_PearlBeard_flk.078 (001.083)</ta>
            <ta e="T482" id="Seg_3576" s="T478">AkEE_19900810_PearlBeard_flk.079 (001.084)</ta>
            <ta e="T486" id="Seg_3577" s="T482">AkEE_19900810_PearlBeard_flk.080 (001.085)</ta>
            <ta e="T495" id="Seg_3578" s="T486">AkEE_19900810_PearlBeard_flk.081 (001.086)</ta>
            <ta e="T498" id="Seg_3579" s="T495">AkEE_19900810_PearlBeard_flk.082 (001.087)</ta>
            <ta e="T500" id="Seg_3580" s="T498">AkEE_19900810_PearlBeard_flk.083 (001.088)</ta>
            <ta e="T505" id="Seg_3581" s="T500">AkEE_19900810_PearlBeard_flk.084 (001.089)</ta>
            <ta e="T511" id="Seg_3582" s="T505">AkEE_19900810_PearlBeard_flk.085 (001.090)</ta>
            <ta e="T513" id="Seg_3583" s="T511">AkEE_19900810_PearlBeard_flk.086 (001.091)</ta>
            <ta e="T524" id="Seg_3584" s="T513">AkEE_19900810_PearlBeard_flk.087 (001.092)</ta>
            <ta e="T530" id="Seg_3585" s="T524">AkEE_19900810_PearlBeard_flk.088 (001.093)</ta>
            <ta e="T543" id="Seg_3586" s="T530">AkEE_19900810_PearlBeard_flk.089 (001.094)</ta>
            <ta e="T549" id="Seg_3587" s="T543">AkEE_19900810_PearlBeard_flk.090 (001.096)</ta>
            <ta e="T553" id="Seg_3588" s="T549">AkEE_19900810_PearlBeard_flk.091 (001.097)</ta>
            <ta e="T560" id="Seg_3589" s="T553">AkEE_19900810_PearlBeard_flk.092 (001.098)</ta>
            <ta e="T567" id="Seg_3590" s="T560">AkEE_19900810_PearlBeard_flk.093 (001.099)</ta>
            <ta e="T577" id="Seg_3591" s="T567">AkEE_19900810_PearlBeard_flk.094 (001.100)</ta>
            <ta e="T583" id="Seg_3592" s="T577">AkEE_19900810_PearlBeard_flk.095 (001.101)</ta>
            <ta e="T587" id="Seg_3593" s="T583">AkEE_19900810_PearlBeard_flk.096 (001.102)</ta>
            <ta e="T596" id="Seg_3594" s="T587">AkEE_19900810_PearlBeard_flk.097 (001.103)</ta>
            <ta e="T612" id="Seg_3595" s="T596">AkEE_19900810_PearlBeard_flk.098 (001.104)</ta>
            <ta e="T615" id="Seg_3596" s="T612">AkEE_19900810_PearlBeard_flk.099 (001.105)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T5" id="Seg_3597" s="T0">Иһиллээӈ олоӈкону, аата "Огуруо бытык".</ta>
            <ta e="T9" id="Seg_3598" s="T5">Биир огонньор баар эбит.</ta>
            <ta e="T16" id="Seg_3599" s="T9">Огонньор бытыга арыттаак буолан бараан уһун багайы.</ta>
            <ta e="T21" id="Seg_3600" s="T16">Бытыктара огуруолах һап көрдүк гилбэӈнииллэр.</ta>
            <ta e="T28" id="Seg_3601" s="T21">Ол иһин огонньору ааттабыттар Огуруо бытык диэн.</ta>
            <ta e="T32" id="Seg_3602" s="T28">Огуруо бытык үс кыыстаак.</ta>
            <ta e="T35" id="Seg_3603" s="T32">Кыһын, һайын илимнэнэр.</ta>
            <ta e="T39" id="Seg_3604" s="T35">Балыгынан эрэ иитиллэн олороллор.</ta>
            <ta e="T45" id="Seg_3605" s="T39">Биирдэ Огуруо Бытык илим көрүнэ барбыт.</ta>
            <ta e="T53" id="Seg_3606" s="T45">Ойбонугар бөкчөйөрүн кытта, ким ирэ бытыгыттан кабан ылбыт.</ta>
            <ta e="T55" id="Seg_3607" s="T53">Аматтан ыппат.</ta>
            <ta e="T59" id="Seg_3608" s="T55">‎"Ким боулой?" – дии һанаабыт.</ta>
            <ta e="T63" id="Seg_3609" s="T59">Ойбонтон быкпыт Уу иччитэ.</ta>
            <ta e="T73" id="Seg_3610" s="T63">Огуруо Бытык диир: "Кыыскын биэрэгин дуу, һуок дуу дьактар гынэкпын?</ta>
            <ta e="T79" id="Seg_3611" s="T73">Биэрбэтэккина биир даа балыгы ылыаӈ һуога".</ta>
            <ta e="T85" id="Seg_3612" s="T79">"Туогунан кэргэттэрбин иитиэмий ке?" – дии һанабыт.</ta>
            <ta e="T87" id="Seg_3613" s="T85">Онтон ээктэммит.</ta>
            <ta e="T94" id="Seg_3614" s="T87">"Албыннаатаккына, һин биир булуом", – диэбит уу иччитэ.</ta>
            <ta e="T103" id="Seg_3615" s="T94">Огонньор гэрээпкэтин кабан ылан бараан, буус иһинэн баран каалбыт.</ta>
            <ta e="T107" id="Seg_3616" s="T103">Огуруо бытык бэрт һутаабыт.</ta>
            <ta e="T112" id="Seg_3617" s="T107">Урааһа дьиэтигэр кэлэн, кыргыттарыгар кэпсээбит.</ta>
            <ta e="T115" id="Seg_3618" s="T112">Улакан кыыһа үөрбүт:</ta>
            <ta e="T120" id="Seg_3619" s="T115">"Мин эргэ барыам Уу иччитигэр,</ta>
            <ta e="T125" id="Seg_3620" s="T120">Аан дойду үрдүтүнэн муӈ баай буолуом".</ta>
            <ta e="T131" id="Seg_3621" s="T125">Огуруо Бытык кыыһын эргэ биэрэн кээспит.</ta>
            <ta e="T136" id="Seg_3622" s="T131">Уу иччитэ баай да баай.</ta>
            <ta e="T143" id="Seg_3623" s="T136">Байын һуоһа көстүбэт, төһө эмэтэ косток, дьиэлэк.</ta>
            <ta e="T149" id="Seg_3624" s="T143">Биир коско улакан буус колбуйа турар.</ta>
            <ta e="T156" id="Seg_3625" s="T149">Уу иччитэ дьактарын эгэлэн, буус колбуйаны көллөрбүт.</ta>
            <ta e="T160" id="Seg_3626" s="T156">"Канна һаныыр һырыт, көр.</ta>
            <ta e="T168" id="Seg_3627" s="T160">Бу эрэ.. колбуйа иһигэр угума чөмүйэгин", – диэбит.</ta>
            <ta e="T176" id="Seg_3628" s="T168">Эрэ барбытын кэннэ, бу дьактар тэһийбэкка чөмүйэтин укпут.</ta>
            <ta e="T179" id="Seg_3629" s="T176">Уу буус буолбут.</ta>
            <ta e="T183" id="Seg_3630" s="T179">Араччы (арыччы) һулбу тарпыт чөмүйэтин.</ta>
            <ta e="T188" id="Seg_3631" s="T183">Көрбүтэ, төйөн (тойон) чөмүйэтэ туура барбыт.</ta>
            <ta e="T193" id="Seg_3632" s="T188">Онтутун кам баайан бараан олорор.</ta>
            <ta e="T197" id="Seg_3633" s="T193">Куттаннан карактара былчаччы барбытар.</ta>
            <ta e="T202" id="Seg_3634" s="T197">Уу иччитэ кэлэн һонно таайда.</ta>
            <ta e="T208" id="Seg_3635" s="T202">"Миниэкэ эн дьактар буолбат көрдүккүн", – диэбит.</ta>
            <ta e="T216" id="Seg_3636" s="T208">Дьактары муӈ ыраактаагы дьиэга (дьиэ) коһугар иллэн катаан кээспит.</ta>
            <ta e="T224" id="Seg_3637" s="T216">Аанын чипиччи катыыс үөһүнээн оӈоһуллубут һилиминэн кам биспит.</ta>
            <ta e="T229" id="Seg_3638" s="T224">Огуруо бытык эмиэ илимнэнэ кэлбит.</ta>
            <ta e="T238" id="Seg_3639" s="T229">Ойбонугар бөкчөйөрүн кытта Уу иччита эмиэ бытыгыттан кабан ылбыт.</ta>
            <ta e="T243" id="Seg_3640" s="T238">"Эгэл иккис кыыскын, дьактар гыныакпын."</ta>
            <ta e="T250" id="Seg_3641" s="T243">Огонньор муӈу (муӈа) быстыа дуо, орто кыыһын биэрбит.</ta>
            <ta e="T259" id="Seg_3642" s="T250">Бу дьактар баай көрүнэригэр һолото һуок, һөгөн бараан бүппэт.</ta>
            <ta e="T266" id="Seg_3643" s="T259">Биирдэ маа, буус колбүйага эгэлбит Уу иччитэ.</ta>
            <ta e="T268" id="Seg_3644" s="T266">Эгэлэн һэрэппит:</ta>
            <ta e="T275" id="Seg_3645" s="T268">"Бу колбуйага илиигин да, чөмүйэгин да угаайагын."</ta>
            <ta e="T286" id="Seg_3646" s="T275">Уу иччита һыргалана барбытын кэннэ, дьактара буус колбуйага кэлэн чөмүйэтин укпут.</ta>
            <ta e="T290" id="Seg_3647" s="T286">Ортокуу чөмүйэтэ туура тоӈмут.</ta>
            <ta e="T298" id="Seg_3648" s="T290">Чөмүйэтин кам кэлгийбит, Уу иччитэ кэлэн һонно таайбыт.</ta>
            <ta e="T319" id="Seg_3649" s="T298">"Миниэкэ истигэнэ һуок дьактар наадата һок", – диэн бараан, ыраактаагы дьиэ коһугар каайан кээспит, аанын чипиччи, катыыс үөһүнэн оӈоһуллуһут һилиминэн кам биспит.</ta>
            <ta e="T325" id="Seg_3650" s="T319">Огуруо Бытык эмиэ илим көрүнэ кэлбит.</ta>
            <ta e="T334" id="Seg_3651" s="T325">Ойбунугар бөкчөйөрүн кытта уу иччитэ эмиэ бытыгыттан кабан ылбыт.</ta>
            <ta e="T340" id="Seg_3652" s="T334">"Эгэл ылгын кыыскын, дьактар гыныакпын", – диэбит.</ta>
            <ta e="T346" id="Seg_3653" s="T340">Муӈу (муӈа) быстыа дуо ылгын кыыһын илпит.</ta>
            <ta e="T350" id="Seg_3654" s="T346">Баран иһэннэр кыыһа диэбит:</ta>
            <ta e="T352" id="Seg_3655" s="T350">"Куттаныма, тээтэ.</ta>
            <ta e="T358" id="Seg_3656" s="T352">Мин һонно кэлиэм эдьиийдэрбин эмиэ эгэлиэм.</ta>
            <ta e="T365" id="Seg_3657" s="T358">Уу иччэтэ минигиннээгэр кубулгата һуо буолуо, бадага".</ta>
            <ta e="T374" id="Seg_3658" s="T365">Уу иччэтэ үһүс дьактарын буус колбуйага эгэлэн бараан, диэбит: </ta>
            <ta e="T378" id="Seg_3659" s="T374">"Бу колбуйага илиигин угума.</ta>
            <ta e="T381" id="Seg_3660" s="T378">Төӈӈөн кэллэкпинэ, көрүөм.</ta>
            <ta e="T384" id="Seg_3661" s="T381">Минигин албынныаӈ һуога."</ta>
            <ta e="T391" id="Seg_3662" s="T384">"Того минигин һэрэтээр", дии һанаабыт бу дьактар.</ta>
            <ta e="T399" id="Seg_3663" s="T391">"Туок буолуой дьэ илиибин уктакпына, уу уу көрдүк.</ta>
            <ta e="T405" id="Seg_3664" s="T399">Бадага, туок эрэ албаһа бара буолуо." </ta>
            <ta e="T414" id="Seg_3665" s="T405">Нэлманы кабан ылан бараан кутуругун укпут буус колбуйа иһигэр.</ta>
            <ta e="T419" id="Seg_3666" s="T414">Нэлма кутуруга туура тостон каалбыт.</ta>
            <ta e="T427" id="Seg_3667" s="T419">Огуруо бытык кыыһа таайда, того уу иччэтэ һэрэппитин. </ta>
            <ta e="T434" id="Seg_3668" s="T427">Колбуйа аттытыгар олорон бараан, илим абырактана олорор.</ta>
            <ta e="T438" id="Seg_3669" s="T434">Уу иччэтэ кэлэн һөктө: </ta>
            <ta e="T441" id="Seg_3670" s="T438">"Туок илимин абырактыыгын?</ta>
            <ta e="T453" id="Seg_3671" s="T441">Уу иһигэр балык үгүс, илимэ да һуок мин эниэкэ каһы һаныыры эгэлэм." </ta>
            <ta e="T464" id="Seg_3672" s="T453">"Балык үгүс буолан бараан", диир дьактара, "биир даа таба этэ һуок.</ta>
            <ta e="T472" id="Seg_3673" s="T464">Мин таба этигэр үөрэммиппин, таба этин һиэкпин багарабын.</ta>
            <ta e="T478" id="Seg_3674" s="T472">Бу буус колбуйа иһигэр таба үгүс.</ta>
            <ta e="T482" id="Seg_3675" s="T478">Балык көрдүк карбыы һылдьаллар."</ta>
            <ta e="T486" id="Seg_3676" s="T482">Уу иччэтэ дьэ күлэр:</ta>
            <ta e="T495" id="Seg_3677" s="T486">"Бу буус колбуйага туок табата кэлиэй, манна уу эрэ.</ta>
            <ta e="T498" id="Seg_3678" s="T495">Мин билэбин ээт."</ta>
            <ta e="T500" id="Seg_3679" s="T498">Дьактара диэбит:</ta>
            <ta e="T505" id="Seg_3680" s="T500">"Мин көрбүтүм, табалар карбыы һылдьаллар.</ta>
            <ta e="T511" id="Seg_3681" s="T505">Һороктор чээлкээлэр, һороктор бугдиилэр, аӈардара каралар.</ta>
            <ta e="T513" id="Seg_3682" s="T511">Бүтүн үөр."</ta>
            <ta e="T524" id="Seg_3683" s="T513">"Аӈыр дьактаргын эбит", кыйӈанна Уу иччитэ, "туок табата уу иһигэр һылдьыай?"</ta>
            <ta e="T530" id="Seg_3684" s="T524">"Бэйэн (бэйэӈ) билэгин, биһиги уу иһигэр олоробут.</ta>
            <ta e="T543" id="Seg_3685" s="T530">Ол иһин табалар эмиэ уу иһигэр һылдьалар", диэбит дьактара, "көр дьэ, колбуйа иһин."</ta>
            <ta e="T549" id="Seg_3686" s="T543">Уу иччитэ кыйӈанан бараан, умнан (умсан) кээспит.</ta>
            <ta e="T553" id="Seg_3687" s="T549">Колбуйатыгар өӈүйбүт, табалары көрөөрү.</ta>
            <ta e="T560" id="Seg_3688" s="T553">Дьактара кэлэн, мэньиитин уу иһигэр һамнары баттаабыт.</ta>
            <ta e="T567" id="Seg_3689" s="T560">Уу иччитэн мэньиитэ туура барбыт, һонно өлбүт.</ta>
            <ta e="T577" id="Seg_3690" s="T567">Огуруо бытык ылгын кыыһа антан аны кэрийэ һылдьан эдьиийдэрин булбут.</ta>
            <ta e="T583" id="Seg_3691" s="T577">Аанарын һилимнэрин баһаагынан кыһыйан ылан босколообут.</ta>
            <ta e="T587" id="Seg_3692" s="T583">Үһүөннэрэ ураһа дьиэлирэгэр кэлбитэр.</ta>
            <ta e="T596" id="Seg_3693" s="T587">Огуруо бытык үөрэн кыргыттарыгар кыспыт кыһар, буспут балыгы аһатар.</ta>
            <ta e="T612" id="Seg_3694" s="T596">Улакан кыһа тойон чомүйэтын көрөн, онуга орто кыһа ортокуу чөмүйэтин көрөн, оччого эрэ уу иччитин өйдүүллэр.</ta>
            <ta e="T615" id="Seg_3695" s="T612">Олоӈколообута Огдуо Аксённова.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T5" id="Seg_3696" s="T0">Ihilleːŋ oloŋkonu, aːta "Oguru͡o bɨtɨk". </ta>
            <ta e="T9" id="Seg_3697" s="T5">Biːr ogonnʼor baːr ebit. </ta>
            <ta e="T16" id="Seg_3698" s="T9">Ogonnʼor bɨtɨga arɨttaːk bu͡olan baraːn uhun bagajɨ. </ta>
            <ta e="T21" id="Seg_3699" s="T16">Bɨtɨktara oguru͡olaːk hap kördük gilbeŋniːller. </ta>
            <ta e="T28" id="Seg_3700" s="T21">Ol ihin ogonnʼoru aːttaːbɨttar Oguru͡o Bɨtɨk di͡en. </ta>
            <ta e="T32" id="Seg_3701" s="T28">Oguru͡o bɨtɨk üs kɨːstaːk. </ta>
            <ta e="T35" id="Seg_3702" s="T32">Kɨhɨn, hajɨn ilimnener. </ta>
            <ta e="T39" id="Seg_3703" s="T35">Balɨgɨnan ere iːtillen olorollor. </ta>
            <ta e="T45" id="Seg_3704" s="T39">Biːrde Oguru͡o Bɨtɨk ilim körüne barbɨt. </ta>
            <ta e="T53" id="Seg_3705" s="T45">Ojbonugar bökčöjörün kɨtta, kim ire bɨtɨgɨttan kaban ɨlbɨt. </ta>
            <ta e="T55" id="Seg_3706" s="T53">Amattan ɨːppat. </ta>
            <ta e="T59" id="Seg_3707" s="T55">"Kim bu͡olu͡oj?", diː hanaːbɨt. </ta>
            <ta e="T63" id="Seg_3708" s="T59">Ojbonton bɨkpɨt uː iččite. </ta>
            <ta e="T73" id="Seg_3709" s="T63">"Oguru͡o Bɨtɨk", diːr, "kɨːskɨn bi͡eregin duː, hu͡ok duː dʼaktar gɨnɨ͡akpɨn? </ta>
            <ta e="T79" id="Seg_3710" s="T73">Bi͡erbetekkine biːr da balɨgɨ ɨlɨ͡aŋ hu͡oga." </ta>
            <ta e="T85" id="Seg_3711" s="T79">"Tu͡ogunan kergetterbin iːti͡emij ke?", diː hanaːbɨt. </ta>
            <ta e="T87" id="Seg_3712" s="T85">Onton eːktemmit. </ta>
            <ta e="T94" id="Seg_3713" s="T87">"Albɨnnaːtakkɨna, hin biːr bulu͡om", di͡ebit uː iččite. </ta>
            <ta e="T103" id="Seg_3714" s="T94">Ogonnʼor gereːpketin kaban ɨlan baraːn, buːs ihinen baran kaːlbɨt. </ta>
            <ta e="T107" id="Seg_3715" s="T103">Oguru͡o Bɨtɨk bert hutaːbɨt. </ta>
            <ta e="T112" id="Seg_3716" s="T107">Uraha dʼi͡etiger kelen, kɨrgɨttarɨgar kepseːbit. </ta>
            <ta e="T115" id="Seg_3717" s="T112">Ulakan kɨːha ü͡örbüt: </ta>
            <ta e="T120" id="Seg_3718" s="T115">"Min erge barɨ͡am uː iččitiger. </ta>
            <ta e="T125" id="Seg_3719" s="T120">Aːn dojdu ürdütünen muŋ baːj bu͡olu͡om." </ta>
            <ta e="T131" id="Seg_3720" s="T125">Oguru͡o Bɨtɨk kɨːhɨn erge bi͡eren keːspit. </ta>
            <ta e="T136" id="Seg_3721" s="T131">Uː iččite baːj da baːj. </ta>
            <ta e="T143" id="Seg_3722" s="T136">Baːjɨn (uhuga) köstübet, töhö emete kostoːk, dʼi͡eleːk. </ta>
            <ta e="T149" id="Seg_3723" s="T143">Biːr kosko ulakan buːs kolbuja turar. </ta>
            <ta e="T156" id="Seg_3724" s="T149">Uː iččite dʼaktarɨn egelen, buːs kolbujanɨ köllörbüt. </ta>
            <ta e="T160" id="Seg_3725" s="T156">"Kanna hanɨːr hɨrɨt, kör. </ta>
            <ta e="T168" id="Seg_3726" s="T160">Bu ere ((PAUSE)) kolbuja ihiger uguma čömüjegin", di͡ebit. </ta>
            <ta e="T176" id="Seg_3727" s="T168">Ere barbɨtɨn kenne, bu dʼaktar tehijbekke čömüjetin ukput. </ta>
            <ta e="T179" id="Seg_3728" s="T176">Uː buːs bu͡olbut. </ta>
            <ta e="T183" id="Seg_3729" s="T179">Araččɨ hulbu tarpɨt čömüjetin. </ta>
            <ta e="T188" id="Seg_3730" s="T183">Körbüte, töjön čömüjete tuːra barbɨt. </ta>
            <ta e="T193" id="Seg_3731" s="T188">Ontutun kam baːjan baraːn oloror. </ta>
            <ta e="T197" id="Seg_3732" s="T193">Kuttanan karaktara bɨlčaččɨ barbɨttar. </ta>
            <ta e="T202" id="Seg_3733" s="T197">Uː iččite kelen honno taːjda. </ta>
            <ta e="T208" id="Seg_3734" s="T202">"Mini͡eke en dʼaktar bu͡olbat kördükkün", di͡ebit. </ta>
            <ta e="T216" id="Seg_3735" s="T208">Dʼaktarɨ muŋ ɨraːktaːgɨ dʼi͡ege kohugar illen kataːn keːspit. </ta>
            <ta e="T224" id="Seg_3736" s="T216">Aːnɨn čipičči katɨːs ü͡öhünen oŋohullubut hiliminen kam bispit. </ta>
            <ta e="T229" id="Seg_3737" s="T224">Oguru͡o Bɨtɨk emi͡e ilimnene kelbit. </ta>
            <ta e="T238" id="Seg_3738" s="T229">Ojbonugar bökčöjörün kɨtta uː iččite emi͡e bɨtɨgɨttan kaban ɨlbɨt. </ta>
            <ta e="T243" id="Seg_3739" s="T238">"Egel ikkis kɨːskɨn, dʼaktar gɨnɨ͡akpɨn." </ta>
            <ta e="T250" id="Seg_3740" s="T243">Ogonnʼor muŋu bɨstɨ͡a du͡o, orto kɨːhɨn bi͡erbit. </ta>
            <ta e="T259" id="Seg_3741" s="T250">Bu dʼaktar baːj körüneriger holoto hu͡ok, högön baraːn büppet. </ta>
            <ta e="T266" id="Seg_3742" s="T259">Biːrde maː, buːs kolbujaga egelbit uː iččite. </ta>
            <ta e="T268" id="Seg_3743" s="T266">Egelen hereppit: </ta>
            <ta e="T275" id="Seg_3744" s="T268">"Bu kolbujaga iliːgin da, čömüjegin da ugaːjagɨn." </ta>
            <ta e="T286" id="Seg_3745" s="T275">Uː iččite hɨrgalana barbɨtɨn kenne, dʼaktara buːs kolbujaga kelen čömüjetin ukput. </ta>
            <ta e="T290" id="Seg_3746" s="T286">Ortokuː čömüjete tuːra toŋmut. </ta>
            <ta e="T298" id="Seg_3747" s="T290">Čömüjetin kam kelgijbit, uː iččite kelen honno taːjbɨt. </ta>
            <ta e="T319" id="Seg_3748" s="T298">"Mini͡eke istigene hu͡o dʼaktar naːdata hu͡ok", di͡en baraːn, ɨraːktaːgɨ dʼi͡e kohugar kaːjan keːspit, aːnɨn čipičči, katɨːs ü͡öhünen oŋohullubut hiliminen kam bispit. </ta>
            <ta e="T325" id="Seg_3749" s="T319">Oguru͡o Bɨtɨk emi͡e ilim körüne kelbit. </ta>
            <ta e="T334" id="Seg_3750" s="T325">Ojbonugar bökčöjörün kɨtta uː iččite emi͡e bɨtɨgɨttan kaban ɨlbɨt. </ta>
            <ta e="T340" id="Seg_3751" s="T334">"Egel ɨlgɨn kɨːskɨn, dʼaktar gɨnɨ͡akpɨn", di͡ebit. </ta>
            <ta e="T346" id="Seg_3752" s="T340">Muŋu bɨstɨ͡a du͡o, ɨlgɨn kɨːhɨn ilpit. </ta>
            <ta e="T350" id="Seg_3753" s="T346">Baran ihenner kɨːha di͡ebit: </ta>
            <ta e="T352" id="Seg_3754" s="T350">"Kuttanɨma, teːte. </ta>
            <ta e="T358" id="Seg_3755" s="T352">Min honno keli͡em, edʼiːjderbin emi͡e egeli͡em. </ta>
            <ta e="T365" id="Seg_3756" s="T358">Uː iččite miniginneːger kubulgata hu͡o bu͡olu͡o, badaga." </ta>
            <ta e="T374" id="Seg_3757" s="T365">Uː iččite ühüs dʼaktarɨn buːs kolbujaga egelen baraːn di͡ebit: </ta>
            <ta e="T378" id="Seg_3758" s="T374">"Bu kolbujaga iliːgin uguma. </ta>
            <ta e="T381" id="Seg_3759" s="T378">Tönnön kellekpine, körü͡öm. </ta>
            <ta e="T384" id="Seg_3760" s="T381">Minigin albɨnnɨ͡aŋ hu͡oga." </ta>
            <ta e="T391" id="Seg_3761" s="T384">"Togo minigin hereteːr", diː hanaːbɨt bu dʼaktar. </ta>
            <ta e="T399" id="Seg_3762" s="T391">"Tu͡ok bu͡olu͡oj dʼe iliːbin uktakpɨna, uː uː kördük. </ta>
            <ta e="T405" id="Seg_3763" s="T399">Badaga, tu͡ok ere albaha baːra bu͡olu͡o." </ta>
            <ta e="T414" id="Seg_3764" s="T405">Nʼelmanɨ kaban ɨlan baraːn kuturugun ukput buːs kolbuja ihiger. </ta>
            <ta e="T419" id="Seg_3765" s="T414">Nʼelma kuturuga tuːra toston kaːlbɨt. </ta>
            <ta e="T427" id="Seg_3766" s="T419">Oguru͡o Bɨtɨk kɨːha taːjda, togo uː iččite hereppitin. </ta>
            <ta e="T434" id="Seg_3767" s="T427">Kolbuja attɨtɨgar oloron baraːn, ilim abɨraktana oloror. </ta>
            <ta e="T438" id="Seg_3768" s="T434">Uː iččite kelen höktö: </ta>
            <ta e="T441" id="Seg_3769" s="T438">"Tu͡ok ilimin abɨraktɨːgɨn? </ta>
            <ta e="T453" id="Seg_3770" s="T441">Uː ihiger balɨk ügüs, ilime da hu͡ok min eni͡eke kahɨ hanɨːrɨ egeli͡em." </ta>
            <ta e="T464" id="Seg_3771" s="T453">"Balɨk ügüs bu͡olan baraːn", diːr dʼaktara, "biːr daː taba ete hu͡ok. </ta>
            <ta e="T472" id="Seg_3772" s="T464">Min taba etiger ü͡öremmippin, taba etin hi͡ekpin bagarabɨn. </ta>
            <ta e="T478" id="Seg_3773" s="T472">Bu buːs kolbuja ihiger taba ügüs. </ta>
            <ta e="T482" id="Seg_3774" s="T478">Balɨk kördük karbɨː hɨldʼallar." </ta>
            <ta e="T486" id="Seg_3775" s="T482">Uː iččite dʼe küler: </ta>
            <ta e="T495" id="Seg_3776" s="T486">"Bu buːs kolbujaga tu͡ok tabata keli͡ej, manna uː ere. </ta>
            <ta e="T498" id="Seg_3777" s="T495">Min bilebin eːt." </ta>
            <ta e="T500" id="Seg_3778" s="T498">Dʼaktara di͡ebit: </ta>
            <ta e="T505" id="Seg_3779" s="T500">"Min körbütüm, tabalar karbɨː hɨldʼallar. </ta>
            <ta e="T511" id="Seg_3780" s="T505">Horoktor čeːlkeːler, horoktor bugdiːler, aŋardara karalar. </ta>
            <ta e="T513" id="Seg_3781" s="T511">Bütün ü͡ör." </ta>
            <ta e="T524" id="Seg_3782" s="T513">"Aŋɨr dʼaktargɨn ebit", kɨjŋanna uː iččite," tu͡ok tabata uː ihiger hɨldʼɨ͡aj?" </ta>
            <ta e="T530" id="Seg_3783" s="T524">"Bejem bilegin, bihigi uː ihiger olorobut. </ta>
            <ta e="T543" id="Seg_3784" s="T530">Ol ihin tabalar emi͡e uː ihiger hɨldʼallar", di͡ebit dʼaktara, "kör dʼe, kolbuja ihin." </ta>
            <ta e="T549" id="Seg_3785" s="T543">Uː iččite kɨjŋanan baraːn, umnan keːspit. </ta>
            <ta e="T553" id="Seg_3786" s="T549">Kolbujatɨgar öŋöjbüt, tabalarɨ köröːrü. </ta>
            <ta e="T560" id="Seg_3787" s="T553">Dʼaktara kelen, menʼiːtin uː ihiger hamnarɨ battaːbɨt. </ta>
            <ta e="T567" id="Seg_3788" s="T560">Uː iččitin menʼiːte tuːra barbɨt, honno ölbüt. </ta>
            <ta e="T577" id="Seg_3789" s="T567">Oguru͡o Bɨtɨk ɨlgɨn kɨːha aːntan aːnɨ kerije hɨldʼan edʼiːjderin bulbut. </ta>
            <ta e="T583" id="Seg_3790" s="T577">Aːnnarɨn hilimnerin bahaːgɨnan kɨhɨjan ɨlan boskoloːbut. </ta>
            <ta e="T587" id="Seg_3791" s="T583">Ühü͡önnere uraha dʼi͡eleriger kelbitter. </ta>
            <ta e="T596" id="Seg_3792" s="T587">Oguru͡o Bɨtɨk ü͡ören kɨrgɨttarɨgar kɨspɨt kɨhar, busput balɨgɨ ahatar. </ta>
            <ta e="T612" id="Seg_3793" s="T596">Ulakan kɨːha tojon čömüjetin körön, onuga orto kɨːha ortokuː čömüjetin körön, oččogo ere uː iččitin öjdüːller. </ta>
            <ta e="T615" id="Seg_3794" s="T612">Oloŋkoloːbuta Ogdu͡o Aksʼonava. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_3795" s="T0">ihilleː-ŋ</ta>
            <ta e="T2" id="Seg_3796" s="T1">oloŋko-nu</ta>
            <ta e="T3" id="Seg_3797" s="T2">aːt-a</ta>
            <ta e="T4" id="Seg_3798" s="T3">oguru͡o</ta>
            <ta e="T5" id="Seg_3799" s="T4">bɨtɨk</ta>
            <ta e="T6" id="Seg_3800" s="T5">biːr</ta>
            <ta e="T7" id="Seg_3801" s="T6">ogonnʼor</ta>
            <ta e="T8" id="Seg_3802" s="T7">baːr</ta>
            <ta e="T9" id="Seg_3803" s="T8">e-bit</ta>
            <ta e="T10" id="Seg_3804" s="T9">ogonnʼor</ta>
            <ta e="T11" id="Seg_3805" s="T10">bɨtɨg-a</ta>
            <ta e="T12" id="Seg_3806" s="T11">arɨt-taːk</ta>
            <ta e="T13" id="Seg_3807" s="T12">bu͡ol-an</ta>
            <ta e="T14" id="Seg_3808" s="T13">baraːn</ta>
            <ta e="T15" id="Seg_3809" s="T14">uhun</ta>
            <ta e="T16" id="Seg_3810" s="T15">bagajɨ</ta>
            <ta e="T17" id="Seg_3811" s="T16">bɨtɨk-tar-a</ta>
            <ta e="T18" id="Seg_3812" s="T17">oguru͡o-laːk</ta>
            <ta e="T19" id="Seg_3813" s="T18">hap</ta>
            <ta e="T20" id="Seg_3814" s="T19">kördük</ta>
            <ta e="T21" id="Seg_3815" s="T20">gilbe-ŋniː-l-ler</ta>
            <ta e="T22" id="Seg_3816" s="T21">ol</ta>
            <ta e="T23" id="Seg_3817" s="T22">ihin</ta>
            <ta e="T24" id="Seg_3818" s="T23">ogonnʼor-u</ta>
            <ta e="T25" id="Seg_3819" s="T24">aːt-taː-bɨt-tar</ta>
            <ta e="T26" id="Seg_3820" s="T25">oguru͡o</ta>
            <ta e="T27" id="Seg_3821" s="T26">bɨtɨk</ta>
            <ta e="T28" id="Seg_3822" s="T27">di͡e-n</ta>
            <ta e="T29" id="Seg_3823" s="T28">oguru͡o</ta>
            <ta e="T30" id="Seg_3824" s="T29">bɨtɨk</ta>
            <ta e="T31" id="Seg_3825" s="T30">üs</ta>
            <ta e="T32" id="Seg_3826" s="T31">kɨːs-taːk</ta>
            <ta e="T33" id="Seg_3827" s="T32">kɨhɨn</ta>
            <ta e="T34" id="Seg_3828" s="T33">hajɨn</ta>
            <ta e="T35" id="Seg_3829" s="T34">ilim-nen-er</ta>
            <ta e="T36" id="Seg_3830" s="T35">balɨg-ɨ-nan</ta>
            <ta e="T37" id="Seg_3831" s="T36">ere</ta>
            <ta e="T38" id="Seg_3832" s="T37">iːt-i-ll-en</ta>
            <ta e="T39" id="Seg_3833" s="T38">olor-ol-lor</ta>
            <ta e="T40" id="Seg_3834" s="T39">biːrde</ta>
            <ta e="T41" id="Seg_3835" s="T40">oguru͡o</ta>
            <ta e="T42" id="Seg_3836" s="T41">bɨtɨk</ta>
            <ta e="T43" id="Seg_3837" s="T42">ilim</ta>
            <ta e="T44" id="Seg_3838" s="T43">kör-ü-n-e</ta>
            <ta e="T45" id="Seg_3839" s="T44">bar-bɨt</ta>
            <ta e="T46" id="Seg_3840" s="T45">ojbon-u-gar</ta>
            <ta e="T47" id="Seg_3841" s="T46">bökčöj-ör-ü-n</ta>
            <ta e="T48" id="Seg_3842" s="T47">kɨtta</ta>
            <ta e="T49" id="Seg_3843" s="T48">kim</ta>
            <ta e="T50" id="Seg_3844" s="T49">ire</ta>
            <ta e="T51" id="Seg_3845" s="T50">bɨtɨg-ɨ-ttan</ta>
            <ta e="T52" id="Seg_3846" s="T51">kab-an</ta>
            <ta e="T53" id="Seg_3847" s="T52">ɨl-bɨt</ta>
            <ta e="T54" id="Seg_3848" s="T53">amattan</ta>
            <ta e="T55" id="Seg_3849" s="T54">ɨːp-pat</ta>
            <ta e="T56" id="Seg_3850" s="T55">kim</ta>
            <ta e="T57" id="Seg_3851" s="T56">bu͡ol-u͡o=j</ta>
            <ta e="T58" id="Seg_3852" s="T57">d-iː</ta>
            <ta e="T59" id="Seg_3853" s="T58">hanaː-bɨt</ta>
            <ta e="T60" id="Seg_3854" s="T59">ojbon-ton</ta>
            <ta e="T61" id="Seg_3855" s="T60">bɨk-pɨt</ta>
            <ta e="T62" id="Seg_3856" s="T61">uː</ta>
            <ta e="T63" id="Seg_3857" s="T62">ičči-te</ta>
            <ta e="T64" id="Seg_3858" s="T63">oguru͡o</ta>
            <ta e="T65" id="Seg_3859" s="T64">bɨtɨk</ta>
            <ta e="T66" id="Seg_3860" s="T65">diː-r</ta>
            <ta e="T67" id="Seg_3861" s="T66">kɨːs-kɨ-n</ta>
            <ta e="T68" id="Seg_3862" s="T67">bi͡er-e-gin</ta>
            <ta e="T69" id="Seg_3863" s="T68">duː</ta>
            <ta e="T70" id="Seg_3864" s="T69">hu͡ok</ta>
            <ta e="T71" id="Seg_3865" s="T70">duː</ta>
            <ta e="T72" id="Seg_3866" s="T71">dʼaktar</ta>
            <ta e="T73" id="Seg_3867" s="T72">gɨn-ɨ͡ak-pɨ-n</ta>
            <ta e="T74" id="Seg_3868" s="T73">bi͡er-be-tek-kine</ta>
            <ta e="T75" id="Seg_3869" s="T74">biːr</ta>
            <ta e="T76" id="Seg_3870" s="T75">da</ta>
            <ta e="T77" id="Seg_3871" s="T76">balɨg-ɨ</ta>
            <ta e="T78" id="Seg_3872" s="T77">ɨl-ɨ͡a-ŋ</ta>
            <ta e="T79" id="Seg_3873" s="T78">hu͡og-a</ta>
            <ta e="T80" id="Seg_3874" s="T79">tu͡og-u-nan</ta>
            <ta e="T81" id="Seg_3875" s="T80">kerget-ter-bi-n</ta>
            <ta e="T82" id="Seg_3876" s="T81">iːt-i͡e-m=ij</ta>
            <ta e="T83" id="Seg_3877" s="T82">ke</ta>
            <ta e="T84" id="Seg_3878" s="T83">d-iː</ta>
            <ta e="T85" id="Seg_3879" s="T84">hanaː-bɨt</ta>
            <ta e="T86" id="Seg_3880" s="T85">onton</ta>
            <ta e="T87" id="Seg_3881" s="T86">eːktem-mit</ta>
            <ta e="T88" id="Seg_3882" s="T87">albɨn-naː-tak-kɨna</ta>
            <ta e="T89" id="Seg_3883" s="T88">hin</ta>
            <ta e="T90" id="Seg_3884" s="T89">biːr</ta>
            <ta e="T91" id="Seg_3885" s="T90">bul-u͡o-m</ta>
            <ta e="T92" id="Seg_3886" s="T91">di͡e-bit</ta>
            <ta e="T93" id="Seg_3887" s="T92">uː</ta>
            <ta e="T94" id="Seg_3888" s="T93">ičči-te</ta>
            <ta e="T95" id="Seg_3889" s="T94">ogonnʼor</ta>
            <ta e="T96" id="Seg_3890" s="T95">gereːpke-ti-n</ta>
            <ta e="T97" id="Seg_3891" s="T96">kab-an</ta>
            <ta e="T98" id="Seg_3892" s="T97">ɨl-an</ta>
            <ta e="T99" id="Seg_3893" s="T98">baraːn</ta>
            <ta e="T100" id="Seg_3894" s="T99">buːs</ta>
            <ta e="T101" id="Seg_3895" s="T100">ih-i-nen</ta>
            <ta e="T102" id="Seg_3896" s="T101">bar-an</ta>
            <ta e="T103" id="Seg_3897" s="T102">kaːl-bɨt</ta>
            <ta e="T104" id="Seg_3898" s="T103">oguru͡o</ta>
            <ta e="T105" id="Seg_3899" s="T104">bɨtɨk</ta>
            <ta e="T106" id="Seg_3900" s="T105">bert</ta>
            <ta e="T107" id="Seg_3901" s="T106">hutaː-bɨt</ta>
            <ta e="T108" id="Seg_3902" s="T107">uraha</ta>
            <ta e="T109" id="Seg_3903" s="T108">dʼi͡e-ti-ger</ta>
            <ta e="T110" id="Seg_3904" s="T109">kel-en</ta>
            <ta e="T111" id="Seg_3905" s="T110">kɨrgɨt-tar-ɨ-gar</ta>
            <ta e="T112" id="Seg_3906" s="T111">kepseː-bit</ta>
            <ta e="T113" id="Seg_3907" s="T112">ulakan</ta>
            <ta e="T114" id="Seg_3908" s="T113">kɨːh-a</ta>
            <ta e="T115" id="Seg_3909" s="T114">ü͡ör-büt</ta>
            <ta e="T116" id="Seg_3910" s="T115">min</ta>
            <ta e="T117" id="Seg_3911" s="T116">er-ge</ta>
            <ta e="T118" id="Seg_3912" s="T117">bar-ɨ͡a-m</ta>
            <ta e="T119" id="Seg_3913" s="T118">uː</ta>
            <ta e="T120" id="Seg_3914" s="T119">ičči-ti-ger</ta>
            <ta e="T121" id="Seg_3915" s="T120">aːn dojdu</ta>
            <ta e="T122" id="Seg_3916" s="T121">ürdü-tü-nen</ta>
            <ta e="T123" id="Seg_3917" s="T122">muŋ</ta>
            <ta e="T124" id="Seg_3918" s="T123">baːj</ta>
            <ta e="T125" id="Seg_3919" s="T124">bu͡ol-u͡o-m</ta>
            <ta e="T126" id="Seg_3920" s="T125">oguru͡o</ta>
            <ta e="T127" id="Seg_3921" s="T126">bɨtɨk</ta>
            <ta e="T128" id="Seg_3922" s="T127">kɨːh-ɨ-n</ta>
            <ta e="T129" id="Seg_3923" s="T128">er-ge</ta>
            <ta e="T130" id="Seg_3924" s="T129">bi͡er-en</ta>
            <ta e="T131" id="Seg_3925" s="T130">keːs-pit</ta>
            <ta e="T132" id="Seg_3926" s="T131">uː</ta>
            <ta e="T133" id="Seg_3927" s="T132">ičči-te</ta>
            <ta e="T134" id="Seg_3928" s="T133">baːj</ta>
            <ta e="T135" id="Seg_3929" s="T134">da</ta>
            <ta e="T136" id="Seg_3930" s="T135">baːj</ta>
            <ta e="T137" id="Seg_3931" s="T136">baːj-ɨ-n</ta>
            <ta e="T138" id="Seg_3932" s="T137">uhug-a</ta>
            <ta e="T139" id="Seg_3933" s="T138">köst-ü-bet</ta>
            <ta e="T140" id="Seg_3934" s="T139">töhö</ta>
            <ta e="T141" id="Seg_3935" s="T140">emete</ta>
            <ta e="T142" id="Seg_3936" s="T141">kos-toːk</ta>
            <ta e="T143" id="Seg_3937" s="T142">dʼi͡e-leːk</ta>
            <ta e="T144" id="Seg_3938" s="T143">biːr</ta>
            <ta e="T145" id="Seg_3939" s="T144">kos-ko</ta>
            <ta e="T146" id="Seg_3940" s="T145">ulakan</ta>
            <ta e="T147" id="Seg_3941" s="T146">buːs</ta>
            <ta e="T148" id="Seg_3942" s="T147">kolbuja</ta>
            <ta e="T149" id="Seg_3943" s="T148">tur-ar</ta>
            <ta e="T150" id="Seg_3944" s="T149">uː</ta>
            <ta e="T151" id="Seg_3945" s="T150">ičči-te</ta>
            <ta e="T152" id="Seg_3946" s="T151">dʼaktar-ɨ-n</ta>
            <ta e="T153" id="Seg_3947" s="T152">egel-en</ta>
            <ta e="T154" id="Seg_3948" s="T153">buːs</ta>
            <ta e="T155" id="Seg_3949" s="T154">kolbuja-nɨ</ta>
            <ta e="T156" id="Seg_3950" s="T155">köllör-büt</ta>
            <ta e="T157" id="Seg_3951" s="T156">kanna</ta>
            <ta e="T158" id="Seg_3952" s="T157">hanɨː-r</ta>
            <ta e="T159" id="Seg_3953" s="T158">hɨrɨt</ta>
            <ta e="T160" id="Seg_3954" s="T159">kör</ta>
            <ta e="T161" id="Seg_3955" s="T160">bu</ta>
            <ta e="T162" id="Seg_3956" s="T161">ere</ta>
            <ta e="T164" id="Seg_3957" s="T163">kolbuja</ta>
            <ta e="T165" id="Seg_3958" s="T164">ih-i-ger</ta>
            <ta e="T166" id="Seg_3959" s="T165">ug-u-ma</ta>
            <ta e="T167" id="Seg_3960" s="T166">čömüje-gi-n</ta>
            <ta e="T168" id="Seg_3961" s="T167">di͡e-bit</ta>
            <ta e="T169" id="Seg_3962" s="T168">er-e</ta>
            <ta e="T170" id="Seg_3963" s="T169">bar-bɨt-ɨ-n</ta>
            <ta e="T171" id="Seg_3964" s="T170">kenne</ta>
            <ta e="T172" id="Seg_3965" s="T171">bu</ta>
            <ta e="T173" id="Seg_3966" s="T172">dʼaktar</ta>
            <ta e="T174" id="Seg_3967" s="T173">tehij-bekke</ta>
            <ta e="T175" id="Seg_3968" s="T174">čömüje-ti-n</ta>
            <ta e="T176" id="Seg_3969" s="T175">uk-put</ta>
            <ta e="T177" id="Seg_3970" s="T176">uː</ta>
            <ta e="T178" id="Seg_3971" s="T177">buːs</ta>
            <ta e="T179" id="Seg_3972" s="T178">bu͡ol-but</ta>
            <ta e="T180" id="Seg_3973" s="T179">araččɨ</ta>
            <ta e="T181" id="Seg_3974" s="T180">hulbu</ta>
            <ta e="T182" id="Seg_3975" s="T181">tar-pɨt</ta>
            <ta e="T183" id="Seg_3976" s="T182">čömüje-ti-n</ta>
            <ta e="T184" id="Seg_3977" s="T183">kör-büt-e</ta>
            <ta e="T185" id="Seg_3978" s="T184">töjön</ta>
            <ta e="T186" id="Seg_3979" s="T185">čömüje-te</ta>
            <ta e="T187" id="Seg_3980" s="T186">tuːra</ta>
            <ta e="T188" id="Seg_3981" s="T187">bar-bɨt</ta>
            <ta e="T189" id="Seg_3982" s="T188">on-tu-tu-n</ta>
            <ta e="T190" id="Seg_3983" s="T189">kam</ta>
            <ta e="T191" id="Seg_3984" s="T190">baːj-an</ta>
            <ta e="T192" id="Seg_3985" s="T191">baraːn</ta>
            <ta e="T193" id="Seg_3986" s="T192">olor-or</ta>
            <ta e="T194" id="Seg_3987" s="T193">kuttan-an</ta>
            <ta e="T195" id="Seg_3988" s="T194">karak-tar-a</ta>
            <ta e="T196" id="Seg_3989" s="T195">bɨlča-ččɨ</ta>
            <ta e="T197" id="Seg_3990" s="T196">bar-bɨt-tar</ta>
            <ta e="T198" id="Seg_3991" s="T197">uː</ta>
            <ta e="T199" id="Seg_3992" s="T198">ičči-te</ta>
            <ta e="T200" id="Seg_3993" s="T199">kel-en</ta>
            <ta e="T201" id="Seg_3994" s="T200">honno</ta>
            <ta e="T202" id="Seg_3995" s="T201">taːj-d-a</ta>
            <ta e="T203" id="Seg_3996" s="T202">mini͡e-ke</ta>
            <ta e="T204" id="Seg_3997" s="T203">en</ta>
            <ta e="T205" id="Seg_3998" s="T204">dʼaktar</ta>
            <ta e="T206" id="Seg_3999" s="T205">bu͡ol-bat</ta>
            <ta e="T207" id="Seg_4000" s="T206">kördük-kün</ta>
            <ta e="T208" id="Seg_4001" s="T207">di͡e-bit</ta>
            <ta e="T209" id="Seg_4002" s="T208">dʼaktar-ɨ</ta>
            <ta e="T210" id="Seg_4003" s="T209">muŋ</ta>
            <ta e="T211" id="Seg_4004" s="T210">ɨraːk-taːgɨ</ta>
            <ta e="T212" id="Seg_4005" s="T211">dʼi͡e-ge</ta>
            <ta e="T213" id="Seg_4006" s="T212">koh-u-gar</ta>
            <ta e="T214" id="Seg_4007" s="T213">ill-en</ta>
            <ta e="T215" id="Seg_4008" s="T214">kataː-n</ta>
            <ta e="T216" id="Seg_4009" s="T215">keːs-pit</ta>
            <ta e="T217" id="Seg_4010" s="T216">aːn-ɨ-n</ta>
            <ta e="T218" id="Seg_4011" s="T217">čipičči</ta>
            <ta e="T219" id="Seg_4012" s="T218">katɨːs</ta>
            <ta e="T220" id="Seg_4013" s="T219">ü͡öh-ü-nen</ta>
            <ta e="T221" id="Seg_4014" s="T220">oŋohull-u-but</ta>
            <ta e="T222" id="Seg_4015" s="T221">hilim-i-nen</ta>
            <ta e="T223" id="Seg_4016" s="T222">kam</ta>
            <ta e="T224" id="Seg_4017" s="T223">bis-pit</ta>
            <ta e="T225" id="Seg_4018" s="T224">oguru͡o</ta>
            <ta e="T226" id="Seg_4019" s="T225">bɨtɨk</ta>
            <ta e="T227" id="Seg_4020" s="T226">emi͡e</ta>
            <ta e="T228" id="Seg_4021" s="T227">ilim-nen-e</ta>
            <ta e="T229" id="Seg_4022" s="T228">kel-bit</ta>
            <ta e="T230" id="Seg_4023" s="T229">ojbon-u-gar</ta>
            <ta e="T231" id="Seg_4024" s="T230">bökčöj-ör-ü-n</ta>
            <ta e="T232" id="Seg_4025" s="T231">kɨtta</ta>
            <ta e="T233" id="Seg_4026" s="T232">uː</ta>
            <ta e="T234" id="Seg_4027" s="T233">ičči-te</ta>
            <ta e="T235" id="Seg_4028" s="T234">emi͡e</ta>
            <ta e="T236" id="Seg_4029" s="T235">bɨtɨg-ɨ-ttan</ta>
            <ta e="T237" id="Seg_4030" s="T236">kab-an</ta>
            <ta e="T238" id="Seg_4031" s="T237">ɨl-bɨt</ta>
            <ta e="T239" id="Seg_4032" s="T238">egel</ta>
            <ta e="T240" id="Seg_4033" s="T239">ikki-s</ta>
            <ta e="T241" id="Seg_4034" s="T240">kɨːs-kɨ-n</ta>
            <ta e="T242" id="Seg_4035" s="T241">dʼaktar</ta>
            <ta e="T243" id="Seg_4036" s="T242">gɨn-ɨ͡ak-pɨ-n</ta>
            <ta e="T244" id="Seg_4037" s="T243">ogonnʼor</ta>
            <ta e="T245" id="Seg_4038" s="T244">muŋ-u</ta>
            <ta e="T246" id="Seg_4039" s="T245">bɨst-ɨ͡a</ta>
            <ta e="T247" id="Seg_4040" s="T246">du͡o</ta>
            <ta e="T248" id="Seg_4041" s="T247">orto</ta>
            <ta e="T249" id="Seg_4042" s="T248">kɨːh-ɨ-n</ta>
            <ta e="T250" id="Seg_4043" s="T249">bi͡er-bit</ta>
            <ta e="T251" id="Seg_4044" s="T250">bu</ta>
            <ta e="T252" id="Seg_4045" s="T251">dʼaktar</ta>
            <ta e="T253" id="Seg_4046" s="T252">baːj</ta>
            <ta e="T254" id="Seg_4047" s="T253">kör-ü-n-er-i-ger</ta>
            <ta e="T255" id="Seg_4048" s="T254">holo-to</ta>
            <ta e="T256" id="Seg_4049" s="T255">hu͡ok</ta>
            <ta e="T257" id="Seg_4050" s="T256">hög-ön</ta>
            <ta e="T258" id="Seg_4051" s="T257">baraːn</ta>
            <ta e="T259" id="Seg_4052" s="T258">büp-pet</ta>
            <ta e="T260" id="Seg_4053" s="T259">biːrde</ta>
            <ta e="T261" id="Seg_4054" s="T260">maː</ta>
            <ta e="T262" id="Seg_4055" s="T261">buːs</ta>
            <ta e="T263" id="Seg_4056" s="T262">kolbuja-ga</ta>
            <ta e="T264" id="Seg_4057" s="T263">egel-bit</ta>
            <ta e="T265" id="Seg_4058" s="T264">uː</ta>
            <ta e="T266" id="Seg_4059" s="T265">ičči-te</ta>
            <ta e="T267" id="Seg_4060" s="T266">egel-en</ta>
            <ta e="T268" id="Seg_4061" s="T267">herep-pit</ta>
            <ta e="T269" id="Seg_4062" s="T268">bu</ta>
            <ta e="T270" id="Seg_4063" s="T269">kolbuja-ga</ta>
            <ta e="T271" id="Seg_4064" s="T270">iliː-gi-n</ta>
            <ta e="T272" id="Seg_4065" s="T271">da</ta>
            <ta e="T273" id="Seg_4066" s="T272">čömüje-gi-n</ta>
            <ta e="T274" id="Seg_4067" s="T273">da</ta>
            <ta e="T275" id="Seg_4068" s="T274">ug-aːja-gɨn</ta>
            <ta e="T276" id="Seg_4069" s="T275">uː</ta>
            <ta e="T277" id="Seg_4070" s="T276">ičči-te</ta>
            <ta e="T278" id="Seg_4071" s="T277">hɨrga-lan-a</ta>
            <ta e="T279" id="Seg_4072" s="T278">bar-bɨt-ɨ-n</ta>
            <ta e="T280" id="Seg_4073" s="T279">kenne</ta>
            <ta e="T281" id="Seg_4074" s="T280">dʼaktar-a</ta>
            <ta e="T282" id="Seg_4075" s="T281">buːs</ta>
            <ta e="T283" id="Seg_4076" s="T282">kolbuja-ga</ta>
            <ta e="T284" id="Seg_4077" s="T283">kel-en</ta>
            <ta e="T285" id="Seg_4078" s="T284">čömüje-ti-n</ta>
            <ta e="T286" id="Seg_4079" s="T285">uk-put</ta>
            <ta e="T287" id="Seg_4080" s="T286">orto-kuː</ta>
            <ta e="T288" id="Seg_4081" s="T287">čömüje-te</ta>
            <ta e="T289" id="Seg_4082" s="T288">tuːra</ta>
            <ta e="T290" id="Seg_4083" s="T289">toŋ-mut</ta>
            <ta e="T291" id="Seg_4084" s="T290">čömüje-ti-n</ta>
            <ta e="T292" id="Seg_4085" s="T291">kam</ta>
            <ta e="T293" id="Seg_4086" s="T292">kelgij-bit</ta>
            <ta e="T294" id="Seg_4087" s="T293">uː</ta>
            <ta e="T295" id="Seg_4088" s="T294">ičči-te</ta>
            <ta e="T296" id="Seg_4089" s="T295">kel-en</ta>
            <ta e="T297" id="Seg_4090" s="T296">honno</ta>
            <ta e="T298" id="Seg_4091" s="T297">taːj-bɨt</ta>
            <ta e="T299" id="Seg_4092" s="T298">mini͡e-ke</ta>
            <ta e="T300" id="Seg_4093" s="T299">istigen-e</ta>
            <ta e="T301" id="Seg_4094" s="T300">hu͡o</ta>
            <ta e="T302" id="Seg_4095" s="T301">dʼaktar</ta>
            <ta e="T303" id="Seg_4096" s="T302">naːda-ta</ta>
            <ta e="T304" id="Seg_4097" s="T303">hu͡ok</ta>
            <ta e="T305" id="Seg_4098" s="T304">di͡e-n</ta>
            <ta e="T306" id="Seg_4099" s="T305">baraːn</ta>
            <ta e="T307" id="Seg_4100" s="T306">ɨraːk-taːgɨ</ta>
            <ta e="T308" id="Seg_4101" s="T307">dʼi͡e</ta>
            <ta e="T309" id="Seg_4102" s="T308">koh-u-gar</ta>
            <ta e="T310" id="Seg_4103" s="T309">kaːj-an</ta>
            <ta e="T311" id="Seg_4104" s="T310">keːs-pit</ta>
            <ta e="T312" id="Seg_4105" s="T311">aːn-ɨ-n</ta>
            <ta e="T313" id="Seg_4106" s="T312">čipičči</ta>
            <ta e="T314" id="Seg_4107" s="T313">katɨːs</ta>
            <ta e="T315" id="Seg_4108" s="T314">ü͡öh-ü-nen</ta>
            <ta e="T316" id="Seg_4109" s="T315">oŋohull-u-but</ta>
            <ta e="T317" id="Seg_4110" s="T316">hilim-i-nen</ta>
            <ta e="T318" id="Seg_4111" s="T317">kam</ta>
            <ta e="T319" id="Seg_4112" s="T318">bis-pit</ta>
            <ta e="T320" id="Seg_4113" s="T319">oguru͡o</ta>
            <ta e="T321" id="Seg_4114" s="T320">bɨtɨk</ta>
            <ta e="T322" id="Seg_4115" s="T321">emi͡e</ta>
            <ta e="T323" id="Seg_4116" s="T322">ilim</ta>
            <ta e="T324" id="Seg_4117" s="T323">kör-ü-n-e</ta>
            <ta e="T325" id="Seg_4118" s="T324">kel-bit</ta>
            <ta e="T326" id="Seg_4119" s="T325">ojbon-u-gar</ta>
            <ta e="T327" id="Seg_4120" s="T326">bökčöj-ör-ü-n</ta>
            <ta e="T328" id="Seg_4121" s="T327">kɨtta</ta>
            <ta e="T329" id="Seg_4122" s="T328">uː</ta>
            <ta e="T330" id="Seg_4123" s="T329">ičči-te</ta>
            <ta e="T331" id="Seg_4124" s="T330">emi͡e</ta>
            <ta e="T332" id="Seg_4125" s="T331">bɨtɨg-ɨ-ttan</ta>
            <ta e="T333" id="Seg_4126" s="T332">kab-an</ta>
            <ta e="T334" id="Seg_4127" s="T333">ɨl-bɨt</ta>
            <ta e="T335" id="Seg_4128" s="T334">egel</ta>
            <ta e="T336" id="Seg_4129" s="T335">ɨlgɨn</ta>
            <ta e="T337" id="Seg_4130" s="T336">kɨːs-kɨ-n</ta>
            <ta e="T338" id="Seg_4131" s="T337">dʼaktar</ta>
            <ta e="T339" id="Seg_4132" s="T338">gɨn-ɨ͡ak-pɨ-n</ta>
            <ta e="T340" id="Seg_4133" s="T339">di͡e-bit</ta>
            <ta e="T341" id="Seg_4134" s="T340">muŋ-u</ta>
            <ta e="T342" id="Seg_4135" s="T341">bɨst-ɨ͡a</ta>
            <ta e="T343" id="Seg_4136" s="T342">du͡o</ta>
            <ta e="T344" id="Seg_4137" s="T343">ɨlgɨn</ta>
            <ta e="T345" id="Seg_4138" s="T344">kɨːh-ɨ-n</ta>
            <ta e="T346" id="Seg_4139" s="T345">il-pit</ta>
            <ta e="T347" id="Seg_4140" s="T346">bar-an</ta>
            <ta e="T348" id="Seg_4141" s="T347">ih-en-ner</ta>
            <ta e="T349" id="Seg_4142" s="T348">kɨːh-a</ta>
            <ta e="T350" id="Seg_4143" s="T349">di͡e-bit</ta>
            <ta e="T351" id="Seg_4144" s="T350">kuttan-ɨ-ma</ta>
            <ta e="T352" id="Seg_4145" s="T351">teːte</ta>
            <ta e="T353" id="Seg_4146" s="T352">min</ta>
            <ta e="T354" id="Seg_4147" s="T353">honno</ta>
            <ta e="T355" id="Seg_4148" s="T354">kel-i͡e-m</ta>
            <ta e="T356" id="Seg_4149" s="T355">edʼiːj-der-bi-n</ta>
            <ta e="T357" id="Seg_4150" s="T356">emi͡e</ta>
            <ta e="T358" id="Seg_4151" s="T357">egel-i͡e-m</ta>
            <ta e="T359" id="Seg_4152" s="T358">uː</ta>
            <ta e="T360" id="Seg_4153" s="T359">ičči-te</ta>
            <ta e="T361" id="Seg_4154" s="T360">minigi-nneːger</ta>
            <ta e="T362" id="Seg_4155" s="T361">kubulgat-a</ta>
            <ta e="T363" id="Seg_4156" s="T362">hu͡o</ta>
            <ta e="T364" id="Seg_4157" s="T363">bu͡ol-u͡o</ta>
            <ta e="T365" id="Seg_4158" s="T364">badaga</ta>
            <ta e="T366" id="Seg_4159" s="T365">uː</ta>
            <ta e="T367" id="Seg_4160" s="T366">ičči-te</ta>
            <ta e="T368" id="Seg_4161" s="T367">üh-üs</ta>
            <ta e="T369" id="Seg_4162" s="T368">dʼaktar-ɨ-n</ta>
            <ta e="T370" id="Seg_4163" s="T369">buːs</ta>
            <ta e="T371" id="Seg_4164" s="T370">kolbuja-ga</ta>
            <ta e="T372" id="Seg_4165" s="T371">egel-en</ta>
            <ta e="T373" id="Seg_4166" s="T372">baraːn</ta>
            <ta e="T374" id="Seg_4167" s="T373">di͡e-bit</ta>
            <ta e="T375" id="Seg_4168" s="T374">bu</ta>
            <ta e="T376" id="Seg_4169" s="T375">kolbuja-ga</ta>
            <ta e="T377" id="Seg_4170" s="T376">iliː-gi-n</ta>
            <ta e="T378" id="Seg_4171" s="T377">ug-u-ma</ta>
            <ta e="T379" id="Seg_4172" s="T378">tönn-ön</ta>
            <ta e="T380" id="Seg_4173" s="T379">kel-lek-pine</ta>
            <ta e="T381" id="Seg_4174" s="T380">kör-ü͡ö-m</ta>
            <ta e="T382" id="Seg_4175" s="T381">minigi-n</ta>
            <ta e="T383" id="Seg_4176" s="T382">albɨn-n-ɨ͡a-ŋ</ta>
            <ta e="T384" id="Seg_4177" s="T383">hu͡og-a</ta>
            <ta e="T385" id="Seg_4178" s="T384">togo</ta>
            <ta e="T386" id="Seg_4179" s="T385">minigi-n</ta>
            <ta e="T387" id="Seg_4180" s="T386">heret-eːr</ta>
            <ta e="T388" id="Seg_4181" s="T387">d-iː</ta>
            <ta e="T389" id="Seg_4182" s="T388">hanaː-bɨt</ta>
            <ta e="T390" id="Seg_4183" s="T389">bu</ta>
            <ta e="T391" id="Seg_4184" s="T390">dʼaktar</ta>
            <ta e="T392" id="Seg_4185" s="T391">tu͡ok</ta>
            <ta e="T393" id="Seg_4186" s="T392">bu͡ol-u͡o=j</ta>
            <ta e="T394" id="Seg_4187" s="T393">dʼe</ta>
            <ta e="T395" id="Seg_4188" s="T394">iliː-bi-n</ta>
            <ta e="T396" id="Seg_4189" s="T395">uk-tak-pɨna</ta>
            <ta e="T397" id="Seg_4190" s="T396">uː</ta>
            <ta e="T398" id="Seg_4191" s="T397">uː</ta>
            <ta e="T399" id="Seg_4192" s="T398">kördük</ta>
            <ta e="T400" id="Seg_4193" s="T399">badaga</ta>
            <ta e="T401" id="Seg_4194" s="T400">tu͡ok</ta>
            <ta e="T402" id="Seg_4195" s="T401">ere</ta>
            <ta e="T403" id="Seg_4196" s="T402">albah-a</ta>
            <ta e="T404" id="Seg_4197" s="T403">baːr-a</ta>
            <ta e="T405" id="Seg_4198" s="T404">bu͡olu͡o</ta>
            <ta e="T406" id="Seg_4199" s="T405">nʼelma-nɨ</ta>
            <ta e="T407" id="Seg_4200" s="T406">kab-an</ta>
            <ta e="T408" id="Seg_4201" s="T407">ɨl-an</ta>
            <ta e="T409" id="Seg_4202" s="T408">baraːn</ta>
            <ta e="T410" id="Seg_4203" s="T409">kuturug-u-n</ta>
            <ta e="T411" id="Seg_4204" s="T410">uk-put</ta>
            <ta e="T412" id="Seg_4205" s="T411">buːs</ta>
            <ta e="T413" id="Seg_4206" s="T412">kolbuja</ta>
            <ta e="T414" id="Seg_4207" s="T413">ih-i-ger</ta>
            <ta e="T415" id="Seg_4208" s="T414">nʼelma</ta>
            <ta e="T416" id="Seg_4209" s="T415">kuturug-a</ta>
            <ta e="T417" id="Seg_4210" s="T416">tuːra</ta>
            <ta e="T418" id="Seg_4211" s="T417">tost-on</ta>
            <ta e="T419" id="Seg_4212" s="T418">kaːl-bɨt</ta>
            <ta e="T420" id="Seg_4213" s="T419">oguru͡o</ta>
            <ta e="T421" id="Seg_4214" s="T420">bɨtɨk</ta>
            <ta e="T422" id="Seg_4215" s="T421">kɨːh-a</ta>
            <ta e="T423" id="Seg_4216" s="T422">taːj-d-a</ta>
            <ta e="T424" id="Seg_4217" s="T423">togo</ta>
            <ta e="T425" id="Seg_4218" s="T424">uː</ta>
            <ta e="T426" id="Seg_4219" s="T425">ičči-te</ta>
            <ta e="T427" id="Seg_4220" s="T426">herep-pit-i-n</ta>
            <ta e="T428" id="Seg_4221" s="T427">kolbuja</ta>
            <ta e="T429" id="Seg_4222" s="T428">attɨ-tɨ-gar</ta>
            <ta e="T430" id="Seg_4223" s="T429">olor-on</ta>
            <ta e="T431" id="Seg_4224" s="T430">baraːn</ta>
            <ta e="T432" id="Seg_4225" s="T431">ilim</ta>
            <ta e="T433" id="Seg_4226" s="T432">abɨrakt-a-n-a</ta>
            <ta e="T434" id="Seg_4227" s="T433">olor-or</ta>
            <ta e="T435" id="Seg_4228" s="T434">uː</ta>
            <ta e="T436" id="Seg_4229" s="T435">ičči-te</ta>
            <ta e="T437" id="Seg_4230" s="T436">kel-en</ta>
            <ta e="T438" id="Seg_4231" s="T437">hök-t-ö</ta>
            <ta e="T439" id="Seg_4232" s="T438">tu͡ok</ta>
            <ta e="T440" id="Seg_4233" s="T439">ilim-i-n</ta>
            <ta e="T441" id="Seg_4234" s="T440">abɨrakt-ɨː-gɨn</ta>
            <ta e="T442" id="Seg_4235" s="T441">uː</ta>
            <ta e="T443" id="Seg_4236" s="T442">ih-i-ger</ta>
            <ta e="T444" id="Seg_4237" s="T443">balɨk</ta>
            <ta e="T445" id="Seg_4238" s="T444">ügüs</ta>
            <ta e="T446" id="Seg_4239" s="T445">ilim-e</ta>
            <ta e="T447" id="Seg_4240" s="T446">da</ta>
            <ta e="T448" id="Seg_4241" s="T447">hu͡ok</ta>
            <ta e="T449" id="Seg_4242" s="T448">min</ta>
            <ta e="T450" id="Seg_4243" s="T449">eni͡e-ke</ta>
            <ta e="T451" id="Seg_4244" s="T450">kah-ɨ</ta>
            <ta e="T452" id="Seg_4245" s="T451">hanɨː-r-ɨ</ta>
            <ta e="T453" id="Seg_4246" s="T452">egel-i͡e-m</ta>
            <ta e="T454" id="Seg_4247" s="T453">balɨk</ta>
            <ta e="T455" id="Seg_4248" s="T454">ügüs</ta>
            <ta e="T456" id="Seg_4249" s="T455">bu͡ol-an</ta>
            <ta e="T457" id="Seg_4250" s="T456">baraːn</ta>
            <ta e="T458" id="Seg_4251" s="T457">diː-r</ta>
            <ta e="T459" id="Seg_4252" s="T458">dʼaktar-a</ta>
            <ta e="T460" id="Seg_4253" s="T459">biːr</ta>
            <ta e="T461" id="Seg_4254" s="T460">daː</ta>
            <ta e="T462" id="Seg_4255" s="T461">taba</ta>
            <ta e="T463" id="Seg_4256" s="T462">et-e</ta>
            <ta e="T464" id="Seg_4257" s="T463">hu͡ok</ta>
            <ta e="T465" id="Seg_4258" s="T464">min</ta>
            <ta e="T466" id="Seg_4259" s="T465">taba</ta>
            <ta e="T467" id="Seg_4260" s="T466">et-i-ger</ta>
            <ta e="T468" id="Seg_4261" s="T467">ü͡örem-mip-pin</ta>
            <ta e="T469" id="Seg_4262" s="T468">taba</ta>
            <ta e="T470" id="Seg_4263" s="T469">et-i-n</ta>
            <ta e="T471" id="Seg_4264" s="T470">h-i͡ek-pi-n</ta>
            <ta e="T472" id="Seg_4265" s="T471">bagar-a-bɨn</ta>
            <ta e="T473" id="Seg_4266" s="T472">bu</ta>
            <ta e="T474" id="Seg_4267" s="T473">buːs</ta>
            <ta e="T475" id="Seg_4268" s="T474">kolbuja</ta>
            <ta e="T476" id="Seg_4269" s="T475">ih-i-ger</ta>
            <ta e="T477" id="Seg_4270" s="T476">taba</ta>
            <ta e="T478" id="Seg_4271" s="T477">ügüs</ta>
            <ta e="T479" id="Seg_4272" s="T478">balɨk</ta>
            <ta e="T480" id="Seg_4273" s="T479">kördük</ta>
            <ta e="T481" id="Seg_4274" s="T480">karb-ɨː</ta>
            <ta e="T482" id="Seg_4275" s="T481">hɨldʼ-al-lar</ta>
            <ta e="T483" id="Seg_4276" s="T482">uː</ta>
            <ta e="T484" id="Seg_4277" s="T483">ičči-te</ta>
            <ta e="T485" id="Seg_4278" s="T484">dʼe</ta>
            <ta e="T486" id="Seg_4279" s="T485">kül-er</ta>
            <ta e="T487" id="Seg_4280" s="T486">bu</ta>
            <ta e="T488" id="Seg_4281" s="T487">buːs</ta>
            <ta e="T489" id="Seg_4282" s="T488">kolbuja-ga</ta>
            <ta e="T490" id="Seg_4283" s="T489">tu͡ok</ta>
            <ta e="T491" id="Seg_4284" s="T490">taba-ta</ta>
            <ta e="T492" id="Seg_4285" s="T491">kel-i͡e=j</ta>
            <ta e="T493" id="Seg_4286" s="T492">manna</ta>
            <ta e="T494" id="Seg_4287" s="T493">uː</ta>
            <ta e="T495" id="Seg_4288" s="T494">ere</ta>
            <ta e="T496" id="Seg_4289" s="T495">min</ta>
            <ta e="T497" id="Seg_4290" s="T496">bil-e-bin</ta>
            <ta e="T498" id="Seg_4291" s="T497">eːt</ta>
            <ta e="T499" id="Seg_4292" s="T498">dʼaktar-a</ta>
            <ta e="T500" id="Seg_4293" s="T499">di͡e-bit</ta>
            <ta e="T501" id="Seg_4294" s="T500">min</ta>
            <ta e="T502" id="Seg_4295" s="T501">kör-büt-ü-m</ta>
            <ta e="T503" id="Seg_4296" s="T502">taba-lar</ta>
            <ta e="T504" id="Seg_4297" s="T503">karb-ɨː</ta>
            <ta e="T505" id="Seg_4298" s="T504">hɨldʼ-al-lar</ta>
            <ta e="T506" id="Seg_4299" s="T505">horok-tor</ta>
            <ta e="T507" id="Seg_4300" s="T506">čeːlkeː-ler</ta>
            <ta e="T508" id="Seg_4301" s="T507">horok-tor</ta>
            <ta e="T509" id="Seg_4302" s="T508">bugdiː-ler</ta>
            <ta e="T510" id="Seg_4303" s="T509">aŋar-dara</ta>
            <ta e="T511" id="Seg_4304" s="T510">kara-lar</ta>
            <ta e="T512" id="Seg_4305" s="T511">bütün</ta>
            <ta e="T513" id="Seg_4306" s="T512">ü͡ör</ta>
            <ta e="T514" id="Seg_4307" s="T513">aŋɨr</ta>
            <ta e="T515" id="Seg_4308" s="T514">dʼaktar-gɨn</ta>
            <ta e="T516" id="Seg_4309" s="T515">e-bit</ta>
            <ta e="T517" id="Seg_4310" s="T516">kɨjŋan-n-a</ta>
            <ta e="T518" id="Seg_4311" s="T517">uː</ta>
            <ta e="T519" id="Seg_4312" s="T518">ičči-te</ta>
            <ta e="T520" id="Seg_4313" s="T519">tu͡ok</ta>
            <ta e="T521" id="Seg_4314" s="T520">taba-ta</ta>
            <ta e="T522" id="Seg_4315" s="T521">uː</ta>
            <ta e="T523" id="Seg_4316" s="T522">ih-i-ger</ta>
            <ta e="T524" id="Seg_4317" s="T523">hɨldʼ-ɨ͡a=j</ta>
            <ta e="T525" id="Seg_4318" s="T524">beje-m</ta>
            <ta e="T526" id="Seg_4319" s="T525">bil-e-gin</ta>
            <ta e="T527" id="Seg_4320" s="T526">bihigi</ta>
            <ta e="T528" id="Seg_4321" s="T527">uː</ta>
            <ta e="T529" id="Seg_4322" s="T528">ih-i-ger</ta>
            <ta e="T530" id="Seg_4323" s="T529">olor-o-but</ta>
            <ta e="T531" id="Seg_4324" s="T530">ol</ta>
            <ta e="T532" id="Seg_4325" s="T531">ihin</ta>
            <ta e="T533" id="Seg_4326" s="T532">taba-lar</ta>
            <ta e="T534" id="Seg_4327" s="T533">emi͡e</ta>
            <ta e="T535" id="Seg_4328" s="T534">uː</ta>
            <ta e="T536" id="Seg_4329" s="T535">ih-i-ger</ta>
            <ta e="T537" id="Seg_4330" s="T536">hɨldʼ-al-lar</ta>
            <ta e="T538" id="Seg_4331" s="T537">di͡e-bit</ta>
            <ta e="T539" id="Seg_4332" s="T538">dʼaktar-a</ta>
            <ta e="T540" id="Seg_4333" s="T539">kör</ta>
            <ta e="T541" id="Seg_4334" s="T540">dʼe</ta>
            <ta e="T542" id="Seg_4335" s="T541">kolbuja</ta>
            <ta e="T543" id="Seg_4336" s="T542">ih-i-n</ta>
            <ta e="T544" id="Seg_4337" s="T543">uː</ta>
            <ta e="T545" id="Seg_4338" s="T544">ičči-te</ta>
            <ta e="T546" id="Seg_4339" s="T545">kɨjŋan-an</ta>
            <ta e="T547" id="Seg_4340" s="T546">baraːn</ta>
            <ta e="T548" id="Seg_4341" s="T547">umn-an</ta>
            <ta e="T549" id="Seg_4342" s="T548">keːs-pit</ta>
            <ta e="T550" id="Seg_4343" s="T549">kolbuja-tɨ-gar</ta>
            <ta e="T551" id="Seg_4344" s="T550">öŋöj-büt</ta>
            <ta e="T552" id="Seg_4345" s="T551">taba-lar-ɨ</ta>
            <ta e="T553" id="Seg_4346" s="T552">kör-öːrü</ta>
            <ta e="T554" id="Seg_4347" s="T553">dʼaktar-a</ta>
            <ta e="T555" id="Seg_4348" s="T554">kel-en</ta>
            <ta e="T556" id="Seg_4349" s="T555">menʼiː-ti-n</ta>
            <ta e="T557" id="Seg_4350" s="T556">uː</ta>
            <ta e="T558" id="Seg_4351" s="T557">ih-i-ger</ta>
            <ta e="T559" id="Seg_4352" s="T558">hamnar-ɨ</ta>
            <ta e="T560" id="Seg_4353" s="T559">battaː-bɨt</ta>
            <ta e="T561" id="Seg_4354" s="T560">uː</ta>
            <ta e="T562" id="Seg_4355" s="T561">ičči-ti-n</ta>
            <ta e="T563" id="Seg_4356" s="T562">menʼiː-te</ta>
            <ta e="T564" id="Seg_4357" s="T563">tuːra</ta>
            <ta e="T565" id="Seg_4358" s="T564">bar-bɨt</ta>
            <ta e="T566" id="Seg_4359" s="T565">honno</ta>
            <ta e="T567" id="Seg_4360" s="T566">öl-büt</ta>
            <ta e="T568" id="Seg_4361" s="T567">oguru͡o</ta>
            <ta e="T569" id="Seg_4362" s="T568">bɨtɨk</ta>
            <ta e="T570" id="Seg_4363" s="T569">ɨlgɨn</ta>
            <ta e="T571" id="Seg_4364" s="T570">kɨːh-a</ta>
            <ta e="T572" id="Seg_4365" s="T571">aːn-tan</ta>
            <ta e="T573" id="Seg_4366" s="T572">aːn-ɨ</ta>
            <ta e="T574" id="Seg_4367" s="T573">kerij-e</ta>
            <ta e="T575" id="Seg_4368" s="T574">hɨldʼ-an</ta>
            <ta e="T576" id="Seg_4369" s="T575">edʼiːj-der-i-n</ta>
            <ta e="T577" id="Seg_4370" s="T576">bul-but</ta>
            <ta e="T578" id="Seg_4371" s="T577">aːn-narɨ-n</ta>
            <ta e="T579" id="Seg_4372" s="T578">hilim-neri-n</ta>
            <ta e="T580" id="Seg_4373" s="T579">bahaːg-ɨ-nan</ta>
            <ta e="T581" id="Seg_4374" s="T580">kɨhɨj-an</ta>
            <ta e="T582" id="Seg_4375" s="T581">ɨl-an</ta>
            <ta e="T583" id="Seg_4376" s="T582">boskoloː-but</ta>
            <ta e="T584" id="Seg_4377" s="T583">üh-ü͡ön-nere</ta>
            <ta e="T585" id="Seg_4378" s="T584">uraha</ta>
            <ta e="T586" id="Seg_4379" s="T585">dʼi͡e-leri-ger</ta>
            <ta e="T587" id="Seg_4380" s="T586">kel-bit-ter</ta>
            <ta e="T588" id="Seg_4381" s="T587">oguru͡o</ta>
            <ta e="T589" id="Seg_4382" s="T588">bɨtɨk</ta>
            <ta e="T590" id="Seg_4383" s="T589">ü͡ör-en</ta>
            <ta e="T591" id="Seg_4384" s="T590">kɨrgɨt-tar-ɨ-gar</ta>
            <ta e="T592" id="Seg_4385" s="T591">kɨs-pɨt</ta>
            <ta e="T593" id="Seg_4386" s="T592">kɨh-ar</ta>
            <ta e="T594" id="Seg_4387" s="T593">bus-put</ta>
            <ta e="T595" id="Seg_4388" s="T594">balɨg-ɨ</ta>
            <ta e="T596" id="Seg_4389" s="T595">ah-a-t-ar</ta>
            <ta e="T597" id="Seg_4390" s="T596">ulakan</ta>
            <ta e="T598" id="Seg_4391" s="T597">kɨːh-a</ta>
            <ta e="T599" id="Seg_4392" s="T598">tojon</ta>
            <ta e="T600" id="Seg_4393" s="T599">čömüje-ti-n</ta>
            <ta e="T601" id="Seg_4394" s="T600">kör-ön</ta>
            <ta e="T602" id="Seg_4395" s="T601">onu-ga</ta>
            <ta e="T603" id="Seg_4396" s="T602">orto</ta>
            <ta e="T604" id="Seg_4397" s="T603">kɨːh-a</ta>
            <ta e="T605" id="Seg_4398" s="T604">orto-kuː</ta>
            <ta e="T606" id="Seg_4399" s="T605">čömüje-ti-n</ta>
            <ta e="T607" id="Seg_4400" s="T606">kör-ön</ta>
            <ta e="T608" id="Seg_4401" s="T607">oččogo</ta>
            <ta e="T609" id="Seg_4402" s="T608">ere</ta>
            <ta e="T610" id="Seg_4403" s="T609">uː</ta>
            <ta e="T611" id="Seg_4404" s="T610">ičči-ti-n</ta>
            <ta e="T612" id="Seg_4405" s="T611">öjdüː-l-ler</ta>
            <ta e="T613" id="Seg_4406" s="T612">oloŋko-loː-but-a</ta>
            <ta e="T614" id="Seg_4407" s="T613">Ogdu͡o</ta>
            <ta e="T615" id="Seg_4408" s="T614">Aksʼonava</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_4409" s="T0">ihilleː-ŋ</ta>
            <ta e="T2" id="Seg_4410" s="T1">oloŋko-nI</ta>
            <ta e="T3" id="Seg_4411" s="T2">aːt-tA</ta>
            <ta e="T4" id="Seg_4412" s="T3">oguru͡o</ta>
            <ta e="T5" id="Seg_4413" s="T4">bɨtɨk</ta>
            <ta e="T6" id="Seg_4414" s="T5">biːr</ta>
            <ta e="T7" id="Seg_4415" s="T6">ogonnʼor</ta>
            <ta e="T8" id="Seg_4416" s="T7">baːr</ta>
            <ta e="T9" id="Seg_4417" s="T8">e-BIT</ta>
            <ta e="T10" id="Seg_4418" s="T9">ogonnʼor</ta>
            <ta e="T11" id="Seg_4419" s="T10">bɨtɨk-tA</ta>
            <ta e="T12" id="Seg_4420" s="T11">arɨt-LAːK</ta>
            <ta e="T13" id="Seg_4421" s="T12">bu͡ol-An</ta>
            <ta e="T14" id="Seg_4422" s="T13">baran</ta>
            <ta e="T15" id="Seg_4423" s="T14">uhun</ta>
            <ta e="T16" id="Seg_4424" s="T15">bagajɨ</ta>
            <ta e="T17" id="Seg_4425" s="T16">bɨtɨk-LAr-tA</ta>
            <ta e="T18" id="Seg_4426" s="T17">oguru͡o-LAːK</ta>
            <ta e="T19" id="Seg_4427" s="T18">hap</ta>
            <ta e="T20" id="Seg_4428" s="T19">kördük</ta>
            <ta e="T21" id="Seg_4429" s="T20">gilbej-ŋnAː-Ar-LAr</ta>
            <ta e="T22" id="Seg_4430" s="T21">ol</ta>
            <ta e="T23" id="Seg_4431" s="T22">ihin</ta>
            <ta e="T24" id="Seg_4432" s="T23">ogonnʼor-nI</ta>
            <ta e="T25" id="Seg_4433" s="T24">aːt-LAː-BIT-LAr</ta>
            <ta e="T26" id="Seg_4434" s="T25">oguru͡o</ta>
            <ta e="T27" id="Seg_4435" s="T26">bɨtɨk</ta>
            <ta e="T28" id="Seg_4436" s="T27">di͡e-An</ta>
            <ta e="T29" id="Seg_4437" s="T28">oguru͡o</ta>
            <ta e="T30" id="Seg_4438" s="T29">bɨtɨk</ta>
            <ta e="T31" id="Seg_4439" s="T30">üs</ta>
            <ta e="T32" id="Seg_4440" s="T31">kɨːs-LAːK</ta>
            <ta e="T33" id="Seg_4441" s="T32">kɨhɨn</ta>
            <ta e="T34" id="Seg_4442" s="T33">hajɨn</ta>
            <ta e="T35" id="Seg_4443" s="T34">ilim-LAN-Ar</ta>
            <ta e="T36" id="Seg_4444" s="T35">balɨk-I-nAn</ta>
            <ta e="T37" id="Seg_4445" s="T36">ere</ta>
            <ta e="T38" id="Seg_4446" s="T37">iːt-I-LIN-An</ta>
            <ta e="T39" id="Seg_4447" s="T38">olor-Ar-LAr</ta>
            <ta e="T40" id="Seg_4448" s="T39">biːrde</ta>
            <ta e="T41" id="Seg_4449" s="T40">oguru͡o</ta>
            <ta e="T42" id="Seg_4450" s="T41">bɨtɨk</ta>
            <ta e="T43" id="Seg_4451" s="T42">ilim</ta>
            <ta e="T44" id="Seg_4452" s="T43">kör-I-n-A</ta>
            <ta e="T45" id="Seg_4453" s="T44">bar-BIT</ta>
            <ta e="T46" id="Seg_4454" s="T45">ojbon-tI-GAr</ta>
            <ta e="T47" id="Seg_4455" s="T46">bökčöj-Ar-tI-n</ta>
            <ta e="T48" id="Seg_4456" s="T47">kɨtta</ta>
            <ta e="T49" id="Seg_4457" s="T48">kim</ta>
            <ta e="T50" id="Seg_4458" s="T49">ere</ta>
            <ta e="T51" id="Seg_4459" s="T50">bɨtɨk-tI-ttAn</ta>
            <ta e="T52" id="Seg_4460" s="T51">kap-An</ta>
            <ta e="T53" id="Seg_4461" s="T52">ɨl-BIT</ta>
            <ta e="T54" id="Seg_4462" s="T53">amattan</ta>
            <ta e="T55" id="Seg_4463" s="T54">ɨːt-BAT</ta>
            <ta e="T56" id="Seg_4464" s="T55">kim</ta>
            <ta e="T57" id="Seg_4465" s="T56">bu͡ol-IAK.[tA]=Ij</ta>
            <ta e="T58" id="Seg_4466" s="T57">di͡e-A</ta>
            <ta e="T59" id="Seg_4467" s="T58">hanaː-BIT</ta>
            <ta e="T60" id="Seg_4468" s="T59">ojbon-ttAn</ta>
            <ta e="T61" id="Seg_4469" s="T60">bɨk-BIT</ta>
            <ta e="T62" id="Seg_4470" s="T61">uː</ta>
            <ta e="T63" id="Seg_4471" s="T62">ičči-tA</ta>
            <ta e="T64" id="Seg_4472" s="T63">oguru͡o</ta>
            <ta e="T65" id="Seg_4473" s="T64">bɨtɨk</ta>
            <ta e="T66" id="Seg_4474" s="T65">di͡e-Ar</ta>
            <ta e="T67" id="Seg_4475" s="T66">kɨːs-GI-n</ta>
            <ta e="T68" id="Seg_4476" s="T67">bi͡er-A-GIn</ta>
            <ta e="T69" id="Seg_4477" s="T68">du͡o</ta>
            <ta e="T70" id="Seg_4478" s="T69">hu͡ok</ta>
            <ta e="T71" id="Seg_4479" s="T70">du͡o</ta>
            <ta e="T72" id="Seg_4480" s="T71">dʼaktar</ta>
            <ta e="T73" id="Seg_4481" s="T72">gɨn-IAK-BI-n</ta>
            <ta e="T74" id="Seg_4482" s="T73">bi͡er-BA-TAK-GInA</ta>
            <ta e="T75" id="Seg_4483" s="T74">biːr</ta>
            <ta e="T76" id="Seg_4484" s="T75">da</ta>
            <ta e="T77" id="Seg_4485" s="T76">balɨk-nI</ta>
            <ta e="T78" id="Seg_4486" s="T77">ɨl-IAK-ŋ</ta>
            <ta e="T79" id="Seg_4487" s="T78">hu͡ok-tA</ta>
            <ta e="T80" id="Seg_4488" s="T79">tu͡ok-I-nAn</ta>
            <ta e="T81" id="Seg_4489" s="T80">kergen-LAr-BI-n</ta>
            <ta e="T82" id="Seg_4490" s="T81">iːt-IAK-m=Ij</ta>
            <ta e="T83" id="Seg_4491" s="T82">ka</ta>
            <ta e="T84" id="Seg_4492" s="T83">di͡e-A</ta>
            <ta e="T85" id="Seg_4493" s="T84">hanaː-BIT</ta>
            <ta e="T86" id="Seg_4494" s="T85">onton</ta>
            <ta e="T87" id="Seg_4495" s="T86">eːkten-BIT</ta>
            <ta e="T88" id="Seg_4496" s="T87">albun-LAː-TAK-GInA</ta>
            <ta e="T89" id="Seg_4497" s="T88">hin</ta>
            <ta e="T90" id="Seg_4498" s="T89">biːr</ta>
            <ta e="T91" id="Seg_4499" s="T90">bul-IAK-m</ta>
            <ta e="T92" id="Seg_4500" s="T91">di͡e-BIT</ta>
            <ta e="T93" id="Seg_4501" s="T92">uː</ta>
            <ta e="T94" id="Seg_4502" s="T93">ičči-tA</ta>
            <ta e="T95" id="Seg_4503" s="T94">ogonnʼor</ta>
            <ta e="T96" id="Seg_4504" s="T95">gereːpke-tI-n</ta>
            <ta e="T97" id="Seg_4505" s="T96">kap-An</ta>
            <ta e="T98" id="Seg_4506" s="T97">ɨl-An</ta>
            <ta e="T99" id="Seg_4507" s="T98">baran</ta>
            <ta e="T100" id="Seg_4508" s="T99">buːs</ta>
            <ta e="T101" id="Seg_4509" s="T100">is-tI-nAn</ta>
            <ta e="T102" id="Seg_4510" s="T101">bar-An</ta>
            <ta e="T103" id="Seg_4511" s="T102">kaːl-BIT</ta>
            <ta e="T104" id="Seg_4512" s="T103">oguru͡o</ta>
            <ta e="T105" id="Seg_4513" s="T104">bɨtɨk</ta>
            <ta e="T106" id="Seg_4514" s="T105">bert</ta>
            <ta e="T107" id="Seg_4515" s="T106">hutaː-BIT</ta>
            <ta e="T108" id="Seg_4516" s="T107">uraha</ta>
            <ta e="T109" id="Seg_4517" s="T108">dʼi͡e-tI-GAr</ta>
            <ta e="T110" id="Seg_4518" s="T109">kel-An</ta>
            <ta e="T111" id="Seg_4519" s="T110">kɨːs-LAr-tI-GAr</ta>
            <ta e="T112" id="Seg_4520" s="T111">kepseː-BIT</ta>
            <ta e="T113" id="Seg_4521" s="T112">ulakan</ta>
            <ta e="T114" id="Seg_4522" s="T113">kɨːs-tA</ta>
            <ta e="T115" id="Seg_4523" s="T114">ü͡ör-BIT</ta>
            <ta e="T116" id="Seg_4524" s="T115">min</ta>
            <ta e="T117" id="Seg_4525" s="T116">er-GA</ta>
            <ta e="T118" id="Seg_4526" s="T117">bar-IAK-m</ta>
            <ta e="T119" id="Seg_4527" s="T118">uː</ta>
            <ta e="T120" id="Seg_4528" s="T119">ičči-tI-GAr</ta>
            <ta e="T121" id="Seg_4529" s="T120">aːn dojdu</ta>
            <ta e="T122" id="Seg_4530" s="T121">ürüt-tI-nAn</ta>
            <ta e="T123" id="Seg_4531" s="T122">muŋ</ta>
            <ta e="T124" id="Seg_4532" s="T123">baːj</ta>
            <ta e="T125" id="Seg_4533" s="T124">bu͡ol-IAK-m</ta>
            <ta e="T126" id="Seg_4534" s="T125">oguru͡o</ta>
            <ta e="T127" id="Seg_4535" s="T126">bɨtɨk</ta>
            <ta e="T128" id="Seg_4536" s="T127">kɨːs-tI-n</ta>
            <ta e="T129" id="Seg_4537" s="T128">er-GA</ta>
            <ta e="T130" id="Seg_4538" s="T129">bi͡er-An</ta>
            <ta e="T131" id="Seg_4539" s="T130">keːs-BIT</ta>
            <ta e="T132" id="Seg_4540" s="T131">uː</ta>
            <ta e="T133" id="Seg_4541" s="T132">ičči-tA</ta>
            <ta e="T134" id="Seg_4542" s="T133">baːj</ta>
            <ta e="T135" id="Seg_4543" s="T134">da</ta>
            <ta e="T136" id="Seg_4544" s="T135">baːj</ta>
            <ta e="T137" id="Seg_4545" s="T136">baːj-tI-n</ta>
            <ta e="T138" id="Seg_4546" s="T137">uhuk-tA</ta>
            <ta e="T139" id="Seg_4547" s="T138">köhün-I-BAT</ta>
            <ta e="T140" id="Seg_4548" s="T139">töhö</ta>
            <ta e="T141" id="Seg_4549" s="T140">eme</ta>
            <ta e="T142" id="Seg_4550" s="T141">kos-LAːK</ta>
            <ta e="T143" id="Seg_4551" s="T142">dʼi͡e-LAːK</ta>
            <ta e="T144" id="Seg_4552" s="T143">biːr</ta>
            <ta e="T145" id="Seg_4553" s="T144">kos-GA</ta>
            <ta e="T146" id="Seg_4554" s="T145">ulakan</ta>
            <ta e="T147" id="Seg_4555" s="T146">buːs</ta>
            <ta e="T148" id="Seg_4556" s="T147">kolbuja</ta>
            <ta e="T149" id="Seg_4557" s="T148">tur-Ar</ta>
            <ta e="T150" id="Seg_4558" s="T149">uː</ta>
            <ta e="T151" id="Seg_4559" s="T150">ičči-tA</ta>
            <ta e="T152" id="Seg_4560" s="T151">dʼaktar-tI-n</ta>
            <ta e="T153" id="Seg_4561" s="T152">egel-An</ta>
            <ta e="T154" id="Seg_4562" s="T153">buːs</ta>
            <ta e="T155" id="Seg_4563" s="T154">kolbuja-nI</ta>
            <ta e="T156" id="Seg_4564" s="T155">köllör-BIT</ta>
            <ta e="T157" id="Seg_4565" s="T156">kanna</ta>
            <ta e="T158" id="Seg_4566" s="T157">hanaː-Ar</ta>
            <ta e="T159" id="Seg_4567" s="T158">hɨrɨt</ta>
            <ta e="T160" id="Seg_4568" s="T159">kör</ta>
            <ta e="T161" id="Seg_4569" s="T160">bu</ta>
            <ta e="T162" id="Seg_4570" s="T161">ere</ta>
            <ta e="T164" id="Seg_4571" s="T163">kolbuja</ta>
            <ta e="T165" id="Seg_4572" s="T164">is-tI-GAr</ta>
            <ta e="T166" id="Seg_4573" s="T165">uk-I-m</ta>
            <ta e="T167" id="Seg_4574" s="T166">čömüje-GI-n</ta>
            <ta e="T168" id="Seg_4575" s="T167">di͡e-BIT</ta>
            <ta e="T169" id="Seg_4576" s="T168">er-tA</ta>
            <ta e="T170" id="Seg_4577" s="T169">bar-BIT-tI-n</ta>
            <ta e="T171" id="Seg_4578" s="T170">genne</ta>
            <ta e="T172" id="Seg_4579" s="T171">bu</ta>
            <ta e="T173" id="Seg_4580" s="T172">dʼaktar</ta>
            <ta e="T174" id="Seg_4581" s="T173">tehij-BAkkA</ta>
            <ta e="T175" id="Seg_4582" s="T174">čömüje-tI-n</ta>
            <ta e="T176" id="Seg_4583" s="T175">uk-BIT</ta>
            <ta e="T177" id="Seg_4584" s="T176">uː</ta>
            <ta e="T178" id="Seg_4585" s="T177">buːs</ta>
            <ta e="T179" id="Seg_4586" s="T178">bu͡ol-BIT</ta>
            <ta e="T180" id="Seg_4587" s="T179">arɨːččɨ</ta>
            <ta e="T181" id="Seg_4588" s="T180">hulbu</ta>
            <ta e="T182" id="Seg_4589" s="T181">tart-BIT</ta>
            <ta e="T183" id="Seg_4590" s="T182">čömüje-tI-n</ta>
            <ta e="T184" id="Seg_4591" s="T183">kör-BIT-tA</ta>
            <ta e="T185" id="Seg_4592" s="T184">tojon</ta>
            <ta e="T186" id="Seg_4593" s="T185">čömüje-tA</ta>
            <ta e="T187" id="Seg_4594" s="T186">tuːra</ta>
            <ta e="T188" id="Seg_4595" s="T187">bar-BIT</ta>
            <ta e="T189" id="Seg_4596" s="T188">ol-tI-tI-n</ta>
            <ta e="T190" id="Seg_4597" s="T189">kam</ta>
            <ta e="T191" id="Seg_4598" s="T190">baːj-An</ta>
            <ta e="T192" id="Seg_4599" s="T191">baran</ta>
            <ta e="T193" id="Seg_4600" s="T192">olor-Ar</ta>
            <ta e="T194" id="Seg_4601" s="T193">kuttan-An</ta>
            <ta e="T195" id="Seg_4602" s="T194">karak-LAr-tA</ta>
            <ta e="T196" id="Seg_4603" s="T195">bɨlčaj-ččI</ta>
            <ta e="T197" id="Seg_4604" s="T196">bar-BIT-LAr</ta>
            <ta e="T198" id="Seg_4605" s="T197">uː</ta>
            <ta e="T199" id="Seg_4606" s="T198">ičči-tA</ta>
            <ta e="T200" id="Seg_4607" s="T199">kel-An</ta>
            <ta e="T201" id="Seg_4608" s="T200">honno</ta>
            <ta e="T202" id="Seg_4609" s="T201">taːj-TI-tA</ta>
            <ta e="T203" id="Seg_4610" s="T202">min-GA</ta>
            <ta e="T204" id="Seg_4611" s="T203">en</ta>
            <ta e="T205" id="Seg_4612" s="T204">dʼaktar</ta>
            <ta e="T206" id="Seg_4613" s="T205">bu͡ol-BAT</ta>
            <ta e="T207" id="Seg_4614" s="T206">kördük-GIn</ta>
            <ta e="T208" id="Seg_4615" s="T207">di͡e-BIT</ta>
            <ta e="T209" id="Seg_4616" s="T208">dʼaktar-nI</ta>
            <ta e="T210" id="Seg_4617" s="T209">muŋ</ta>
            <ta e="T211" id="Seg_4618" s="T210">ɨraːk-LAːgI</ta>
            <ta e="T212" id="Seg_4619" s="T211">dʼi͡e-GA</ta>
            <ta e="T213" id="Seg_4620" s="T212">kos-tI-GAr</ta>
            <ta e="T214" id="Seg_4621" s="T213">ilin-An</ta>
            <ta e="T215" id="Seg_4622" s="T214">kataː-An</ta>
            <ta e="T216" id="Seg_4623" s="T215">keːs-BIT</ta>
            <ta e="T217" id="Seg_4624" s="T216">aːn-tI-n</ta>
            <ta e="T218" id="Seg_4625" s="T217">čipičči</ta>
            <ta e="T219" id="Seg_4626" s="T218">katɨːs</ta>
            <ta e="T220" id="Seg_4627" s="T219">ü͡ös-tI-nAn</ta>
            <ta e="T221" id="Seg_4628" s="T220">oŋohulun-I-BIT</ta>
            <ta e="T222" id="Seg_4629" s="T221">hilim-I-nAn</ta>
            <ta e="T223" id="Seg_4630" s="T222">kam</ta>
            <ta e="T224" id="Seg_4631" s="T223">bis-BIT</ta>
            <ta e="T225" id="Seg_4632" s="T224">oguru͡o</ta>
            <ta e="T226" id="Seg_4633" s="T225">bɨtɨk</ta>
            <ta e="T227" id="Seg_4634" s="T226">emi͡e</ta>
            <ta e="T228" id="Seg_4635" s="T227">ilim-LAN-A</ta>
            <ta e="T229" id="Seg_4636" s="T228">kel-BIT</ta>
            <ta e="T230" id="Seg_4637" s="T229">ojbon-tI-GAr</ta>
            <ta e="T231" id="Seg_4638" s="T230">bökčöj-Ar-tI-n</ta>
            <ta e="T232" id="Seg_4639" s="T231">kɨtta</ta>
            <ta e="T233" id="Seg_4640" s="T232">uː</ta>
            <ta e="T234" id="Seg_4641" s="T233">ičči-tA</ta>
            <ta e="T235" id="Seg_4642" s="T234">emi͡e</ta>
            <ta e="T236" id="Seg_4643" s="T235">bɨtɨk-tI-ttAn</ta>
            <ta e="T237" id="Seg_4644" s="T236">kap-An</ta>
            <ta e="T238" id="Seg_4645" s="T237">ɨl-BIT</ta>
            <ta e="T239" id="Seg_4646" s="T238">egel</ta>
            <ta e="T240" id="Seg_4647" s="T239">ikki-Is</ta>
            <ta e="T241" id="Seg_4648" s="T240">kɨːs-GI-n</ta>
            <ta e="T242" id="Seg_4649" s="T241">dʼaktar</ta>
            <ta e="T243" id="Seg_4650" s="T242">gɨn-IAK-BI-n</ta>
            <ta e="T244" id="Seg_4651" s="T243">ogonnʼor</ta>
            <ta e="T245" id="Seg_4652" s="T244">muŋ-nI</ta>
            <ta e="T246" id="Seg_4653" s="T245">bɨhɨn-IAK.[tA]</ta>
            <ta e="T247" id="Seg_4654" s="T246">du͡o</ta>
            <ta e="T248" id="Seg_4655" s="T247">orto</ta>
            <ta e="T249" id="Seg_4656" s="T248">kɨːs-tI-n</ta>
            <ta e="T250" id="Seg_4657" s="T249">bi͡er-BIT</ta>
            <ta e="T251" id="Seg_4658" s="T250">bu</ta>
            <ta e="T252" id="Seg_4659" s="T251">dʼaktar</ta>
            <ta e="T253" id="Seg_4660" s="T252">baːj</ta>
            <ta e="T254" id="Seg_4661" s="T253">kör-I-n-Ar-tI-GAr</ta>
            <ta e="T255" id="Seg_4662" s="T254">holo-tA</ta>
            <ta e="T256" id="Seg_4663" s="T255">hu͡ok</ta>
            <ta e="T257" id="Seg_4664" s="T256">hök-An</ta>
            <ta e="T258" id="Seg_4665" s="T257">baran</ta>
            <ta e="T259" id="Seg_4666" s="T258">büt-BAT</ta>
            <ta e="T260" id="Seg_4667" s="T259">biːrde</ta>
            <ta e="T261" id="Seg_4668" s="T260">bu</ta>
            <ta e="T262" id="Seg_4669" s="T261">buːs</ta>
            <ta e="T263" id="Seg_4670" s="T262">kolbuja-GA</ta>
            <ta e="T264" id="Seg_4671" s="T263">egel-BIT</ta>
            <ta e="T265" id="Seg_4672" s="T264">uː</ta>
            <ta e="T266" id="Seg_4673" s="T265">ičči-tA</ta>
            <ta e="T267" id="Seg_4674" s="T266">egel-An</ta>
            <ta e="T268" id="Seg_4675" s="T267">heret-BIT</ta>
            <ta e="T269" id="Seg_4676" s="T268">bu</ta>
            <ta e="T270" id="Seg_4677" s="T269">kolbuja-GA</ta>
            <ta e="T271" id="Seg_4678" s="T270">iliː-GI-n</ta>
            <ta e="T272" id="Seg_4679" s="T271">da</ta>
            <ta e="T273" id="Seg_4680" s="T272">čömüje-GI-n</ta>
            <ta e="T274" id="Seg_4681" s="T273">da</ta>
            <ta e="T275" id="Seg_4682" s="T274">uk-AːjA-GIn</ta>
            <ta e="T276" id="Seg_4683" s="T275">uː</ta>
            <ta e="T277" id="Seg_4684" s="T276">ičči-tA</ta>
            <ta e="T278" id="Seg_4685" s="T277">hɨrga-LAN-A</ta>
            <ta e="T279" id="Seg_4686" s="T278">bar-BIT-tI-n</ta>
            <ta e="T280" id="Seg_4687" s="T279">genne</ta>
            <ta e="T281" id="Seg_4688" s="T280">dʼaktar-tA</ta>
            <ta e="T282" id="Seg_4689" s="T281">buːs</ta>
            <ta e="T283" id="Seg_4690" s="T282">kolbuja-GA</ta>
            <ta e="T284" id="Seg_4691" s="T283">kel-An</ta>
            <ta e="T285" id="Seg_4692" s="T284">čömüje-tI-n</ta>
            <ta e="T286" id="Seg_4693" s="T285">uk-BIT</ta>
            <ta e="T287" id="Seg_4694" s="T286">orto-GI</ta>
            <ta e="T288" id="Seg_4695" s="T287">čömüje-tA</ta>
            <ta e="T289" id="Seg_4696" s="T288">tuːra</ta>
            <ta e="T290" id="Seg_4697" s="T289">toŋ-BIT</ta>
            <ta e="T291" id="Seg_4698" s="T290">čömüje-tI-n</ta>
            <ta e="T292" id="Seg_4699" s="T291">kam</ta>
            <ta e="T293" id="Seg_4700" s="T292">kelgij-BIT</ta>
            <ta e="T294" id="Seg_4701" s="T293">uː</ta>
            <ta e="T295" id="Seg_4702" s="T294">ičči-tA</ta>
            <ta e="T296" id="Seg_4703" s="T295">kel-An</ta>
            <ta e="T297" id="Seg_4704" s="T296">honno</ta>
            <ta e="T298" id="Seg_4705" s="T297">taːj-BIT</ta>
            <ta e="T299" id="Seg_4706" s="T298">min-GA</ta>
            <ta e="T300" id="Seg_4707" s="T299">istigen-tA</ta>
            <ta e="T301" id="Seg_4708" s="T300">hu͡ok</ta>
            <ta e="T302" id="Seg_4709" s="T301">dʼaktar</ta>
            <ta e="T303" id="Seg_4710" s="T302">naːda-tA</ta>
            <ta e="T304" id="Seg_4711" s="T303">hu͡ok</ta>
            <ta e="T305" id="Seg_4712" s="T304">di͡e-An</ta>
            <ta e="T306" id="Seg_4713" s="T305">baran</ta>
            <ta e="T307" id="Seg_4714" s="T306">ɨraːk-LAːgI</ta>
            <ta e="T308" id="Seg_4715" s="T307">dʼi͡e</ta>
            <ta e="T309" id="Seg_4716" s="T308">kos-tI-GAr</ta>
            <ta e="T310" id="Seg_4717" s="T309">kaːj-An</ta>
            <ta e="T311" id="Seg_4718" s="T310">keːs-BIT</ta>
            <ta e="T312" id="Seg_4719" s="T311">aːn-tI-n</ta>
            <ta e="T313" id="Seg_4720" s="T312">čipičči</ta>
            <ta e="T314" id="Seg_4721" s="T313">katɨːs</ta>
            <ta e="T315" id="Seg_4722" s="T314">ü͡ös-tI-nAn</ta>
            <ta e="T316" id="Seg_4723" s="T315">oŋohulun-I-BIT</ta>
            <ta e="T317" id="Seg_4724" s="T316">hilim-I-nAn</ta>
            <ta e="T318" id="Seg_4725" s="T317">kam</ta>
            <ta e="T319" id="Seg_4726" s="T318">bis-BIT</ta>
            <ta e="T320" id="Seg_4727" s="T319">oguru͡o</ta>
            <ta e="T321" id="Seg_4728" s="T320">bɨtɨk</ta>
            <ta e="T322" id="Seg_4729" s="T321">emi͡e</ta>
            <ta e="T323" id="Seg_4730" s="T322">ilim</ta>
            <ta e="T324" id="Seg_4731" s="T323">kör-I-n-A</ta>
            <ta e="T325" id="Seg_4732" s="T324">kel-BIT</ta>
            <ta e="T326" id="Seg_4733" s="T325">ojbon-tI-GAr</ta>
            <ta e="T327" id="Seg_4734" s="T326">bökčöj-Ar-tI-n</ta>
            <ta e="T328" id="Seg_4735" s="T327">kɨtta</ta>
            <ta e="T329" id="Seg_4736" s="T328">uː</ta>
            <ta e="T330" id="Seg_4737" s="T329">ičči-tA</ta>
            <ta e="T331" id="Seg_4738" s="T330">emi͡e</ta>
            <ta e="T332" id="Seg_4739" s="T331">bɨtɨk-tI-ttAn</ta>
            <ta e="T333" id="Seg_4740" s="T332">kap-An</ta>
            <ta e="T334" id="Seg_4741" s="T333">ɨl-BIT</ta>
            <ta e="T335" id="Seg_4742" s="T334">egel</ta>
            <ta e="T336" id="Seg_4743" s="T335">ɨlgɨn</ta>
            <ta e="T337" id="Seg_4744" s="T336">kɨːs-GI-n</ta>
            <ta e="T338" id="Seg_4745" s="T337">dʼaktar</ta>
            <ta e="T339" id="Seg_4746" s="T338">gɨn-IAK-BI-n</ta>
            <ta e="T340" id="Seg_4747" s="T339">di͡e-BIT</ta>
            <ta e="T341" id="Seg_4748" s="T340">muŋ-nI</ta>
            <ta e="T342" id="Seg_4749" s="T341">bɨhɨn-IAK.[tA]</ta>
            <ta e="T343" id="Seg_4750" s="T342">du͡o</ta>
            <ta e="T344" id="Seg_4751" s="T343">ɨlgɨn</ta>
            <ta e="T345" id="Seg_4752" s="T344">kɨːs-tI-n</ta>
            <ta e="T346" id="Seg_4753" s="T345">ilt-BIT</ta>
            <ta e="T347" id="Seg_4754" s="T346">bar-An</ta>
            <ta e="T348" id="Seg_4755" s="T347">is-An-LAr</ta>
            <ta e="T349" id="Seg_4756" s="T348">kɨːs-tA</ta>
            <ta e="T350" id="Seg_4757" s="T349">di͡e-BIT</ta>
            <ta e="T351" id="Seg_4758" s="T350">kuttan-I-m</ta>
            <ta e="T352" id="Seg_4759" s="T351">teːte</ta>
            <ta e="T353" id="Seg_4760" s="T352">min</ta>
            <ta e="T354" id="Seg_4761" s="T353">honno</ta>
            <ta e="T355" id="Seg_4762" s="T354">kel-IAK-m</ta>
            <ta e="T356" id="Seg_4763" s="T355">edʼij-LAr-BI-n</ta>
            <ta e="T357" id="Seg_4764" s="T356">emi͡e</ta>
            <ta e="T358" id="Seg_4765" s="T357">egel-IAK-m</ta>
            <ta e="T359" id="Seg_4766" s="T358">uː</ta>
            <ta e="T360" id="Seg_4767" s="T359">ičči-tA</ta>
            <ta e="T361" id="Seg_4768" s="T360">min-TAːgAr</ta>
            <ta e="T362" id="Seg_4769" s="T361">kubulgat-tA</ta>
            <ta e="T363" id="Seg_4770" s="T362">hu͡ok</ta>
            <ta e="T364" id="Seg_4771" s="T363">bu͡ol-IAK.[tA]</ta>
            <ta e="T365" id="Seg_4772" s="T364">badaga</ta>
            <ta e="T366" id="Seg_4773" s="T365">uː</ta>
            <ta e="T367" id="Seg_4774" s="T366">ičči-tA</ta>
            <ta e="T368" id="Seg_4775" s="T367">üs-Is</ta>
            <ta e="T369" id="Seg_4776" s="T368">dʼaktar-tI-n</ta>
            <ta e="T370" id="Seg_4777" s="T369">buːs</ta>
            <ta e="T371" id="Seg_4778" s="T370">kolbuja-GA</ta>
            <ta e="T372" id="Seg_4779" s="T371">egel-An</ta>
            <ta e="T373" id="Seg_4780" s="T372">baran</ta>
            <ta e="T374" id="Seg_4781" s="T373">di͡e-BIT</ta>
            <ta e="T375" id="Seg_4782" s="T374">bu</ta>
            <ta e="T376" id="Seg_4783" s="T375">kolbuja-GA</ta>
            <ta e="T377" id="Seg_4784" s="T376">iliː-GI-n</ta>
            <ta e="T378" id="Seg_4785" s="T377">uk-I-m</ta>
            <ta e="T379" id="Seg_4786" s="T378">tönün-An</ta>
            <ta e="T380" id="Seg_4787" s="T379">kel-TAK-BInA</ta>
            <ta e="T381" id="Seg_4788" s="T380">kör-IAK-m</ta>
            <ta e="T382" id="Seg_4789" s="T381">min-n</ta>
            <ta e="T383" id="Seg_4790" s="T382">albun-LAː-IAK-ŋ</ta>
            <ta e="T384" id="Seg_4791" s="T383">hu͡ok-tA</ta>
            <ta e="T385" id="Seg_4792" s="T384">togo</ta>
            <ta e="T386" id="Seg_4793" s="T385">min-n</ta>
            <ta e="T387" id="Seg_4794" s="T386">heret-Ar</ta>
            <ta e="T388" id="Seg_4795" s="T387">di͡e-A</ta>
            <ta e="T389" id="Seg_4796" s="T388">hanaː-BIT</ta>
            <ta e="T390" id="Seg_4797" s="T389">bu</ta>
            <ta e="T391" id="Seg_4798" s="T390">dʼaktar</ta>
            <ta e="T392" id="Seg_4799" s="T391">tu͡ok</ta>
            <ta e="T393" id="Seg_4800" s="T392">bu͡ol-IAK.[tA]=Ij</ta>
            <ta e="T394" id="Seg_4801" s="T393">dʼe</ta>
            <ta e="T395" id="Seg_4802" s="T394">iliː-BI-n</ta>
            <ta e="T396" id="Seg_4803" s="T395">uk-TAK-BInA</ta>
            <ta e="T397" id="Seg_4804" s="T396">uː</ta>
            <ta e="T398" id="Seg_4805" s="T397">uː</ta>
            <ta e="T399" id="Seg_4806" s="T398">kördük</ta>
            <ta e="T400" id="Seg_4807" s="T399">badaga</ta>
            <ta e="T401" id="Seg_4808" s="T400">tu͡ok</ta>
            <ta e="T402" id="Seg_4809" s="T401">ere</ta>
            <ta e="T403" id="Seg_4810" s="T402">albas-tA</ta>
            <ta e="T404" id="Seg_4811" s="T403">baːr-tA</ta>
            <ta e="T405" id="Seg_4812" s="T404">bu͡olu͡o</ta>
            <ta e="T406" id="Seg_4813" s="T405">nʼelma-nI</ta>
            <ta e="T407" id="Seg_4814" s="T406">kap-An</ta>
            <ta e="T408" id="Seg_4815" s="T407">ɨl-An</ta>
            <ta e="T409" id="Seg_4816" s="T408">baran</ta>
            <ta e="T410" id="Seg_4817" s="T409">kuturuk-tI-n</ta>
            <ta e="T411" id="Seg_4818" s="T410">uk-BIT</ta>
            <ta e="T412" id="Seg_4819" s="T411">buːs</ta>
            <ta e="T413" id="Seg_4820" s="T412">kolbuja</ta>
            <ta e="T414" id="Seg_4821" s="T413">is-tI-GAr</ta>
            <ta e="T415" id="Seg_4822" s="T414">nʼelma</ta>
            <ta e="T416" id="Seg_4823" s="T415">kuturuk-tA</ta>
            <ta e="T417" id="Seg_4824" s="T416">tuːra</ta>
            <ta e="T418" id="Seg_4825" s="T417">tohut-An</ta>
            <ta e="T419" id="Seg_4826" s="T418">kaːl-BIT</ta>
            <ta e="T420" id="Seg_4827" s="T419">oguru͡o</ta>
            <ta e="T421" id="Seg_4828" s="T420">bɨtɨk</ta>
            <ta e="T422" id="Seg_4829" s="T421">kɨːs-tA</ta>
            <ta e="T423" id="Seg_4830" s="T422">taːj-TI-tA</ta>
            <ta e="T424" id="Seg_4831" s="T423">togo</ta>
            <ta e="T425" id="Seg_4832" s="T424">uː</ta>
            <ta e="T426" id="Seg_4833" s="T425">ičči-tA</ta>
            <ta e="T427" id="Seg_4834" s="T426">heret-BIT-tI-n</ta>
            <ta e="T428" id="Seg_4835" s="T427">kolbuja</ta>
            <ta e="T429" id="Seg_4836" s="T428">attɨ-tI-GAr</ta>
            <ta e="T430" id="Seg_4837" s="T429">olor-An</ta>
            <ta e="T431" id="Seg_4838" s="T430">baran</ta>
            <ta e="T432" id="Seg_4839" s="T431">ilim</ta>
            <ta e="T433" id="Seg_4840" s="T432">abɨraktaː-A-n-A</ta>
            <ta e="T434" id="Seg_4841" s="T433">olor-Ar</ta>
            <ta e="T435" id="Seg_4842" s="T434">uː</ta>
            <ta e="T436" id="Seg_4843" s="T435">ičči-tA</ta>
            <ta e="T437" id="Seg_4844" s="T436">kel-An</ta>
            <ta e="T438" id="Seg_4845" s="T437">hök-TI-tA</ta>
            <ta e="T439" id="Seg_4846" s="T438">tu͡ok</ta>
            <ta e="T440" id="Seg_4847" s="T439">ilim-tI-n</ta>
            <ta e="T441" id="Seg_4848" s="T440">abɨraktaː-A-GIn</ta>
            <ta e="T442" id="Seg_4849" s="T441">uː</ta>
            <ta e="T443" id="Seg_4850" s="T442">is-tI-GAr</ta>
            <ta e="T444" id="Seg_4851" s="T443">balɨk</ta>
            <ta e="T445" id="Seg_4852" s="T444">ügüs</ta>
            <ta e="T446" id="Seg_4853" s="T445">ilim-tA</ta>
            <ta e="T447" id="Seg_4854" s="T446">da</ta>
            <ta e="T448" id="Seg_4855" s="T447">hu͡ok</ta>
            <ta e="T449" id="Seg_4856" s="T448">min</ta>
            <ta e="T450" id="Seg_4857" s="T449">en-GA</ta>
            <ta e="T451" id="Seg_4858" s="T450">kas-nI</ta>
            <ta e="T452" id="Seg_4859" s="T451">hanaː-Ar-nI</ta>
            <ta e="T453" id="Seg_4860" s="T452">egel-IAK-m</ta>
            <ta e="T454" id="Seg_4861" s="T453">balɨk</ta>
            <ta e="T455" id="Seg_4862" s="T454">ügüs</ta>
            <ta e="T456" id="Seg_4863" s="T455">bu͡ol-An</ta>
            <ta e="T457" id="Seg_4864" s="T456">baran</ta>
            <ta e="T458" id="Seg_4865" s="T457">di͡e-Ar</ta>
            <ta e="T459" id="Seg_4866" s="T458">dʼaktar-tA</ta>
            <ta e="T460" id="Seg_4867" s="T459">biːr</ta>
            <ta e="T461" id="Seg_4868" s="T460">da</ta>
            <ta e="T462" id="Seg_4869" s="T461">taba</ta>
            <ta e="T463" id="Seg_4870" s="T462">et-tA</ta>
            <ta e="T464" id="Seg_4871" s="T463">hu͡ok</ta>
            <ta e="T465" id="Seg_4872" s="T464">min</ta>
            <ta e="T466" id="Seg_4873" s="T465">taba</ta>
            <ta e="T467" id="Seg_4874" s="T466">et-tI-GAr</ta>
            <ta e="T468" id="Seg_4875" s="T467">ü͡ören-BIT-BIn</ta>
            <ta e="T469" id="Seg_4876" s="T468">taba</ta>
            <ta e="T470" id="Seg_4877" s="T469">et-tI-n</ta>
            <ta e="T471" id="Seg_4878" s="T470">hi͡e-IAK-BI-n</ta>
            <ta e="T472" id="Seg_4879" s="T471">bagar-A-BIn</ta>
            <ta e="T473" id="Seg_4880" s="T472">bu</ta>
            <ta e="T474" id="Seg_4881" s="T473">buːs</ta>
            <ta e="T475" id="Seg_4882" s="T474">kolbuja</ta>
            <ta e="T476" id="Seg_4883" s="T475">is-tI-GAr</ta>
            <ta e="T477" id="Seg_4884" s="T476">taba</ta>
            <ta e="T478" id="Seg_4885" s="T477">ügüs</ta>
            <ta e="T479" id="Seg_4886" s="T478">balɨk</ta>
            <ta e="T480" id="Seg_4887" s="T479">kördük</ta>
            <ta e="T481" id="Seg_4888" s="T480">karbaː-A</ta>
            <ta e="T482" id="Seg_4889" s="T481">hɨrɨt-Ar-LAr</ta>
            <ta e="T483" id="Seg_4890" s="T482">uː</ta>
            <ta e="T484" id="Seg_4891" s="T483">ičči-tA</ta>
            <ta e="T485" id="Seg_4892" s="T484">dʼe</ta>
            <ta e="T486" id="Seg_4893" s="T485">kül-Ar</ta>
            <ta e="T487" id="Seg_4894" s="T486">bu</ta>
            <ta e="T488" id="Seg_4895" s="T487">buːs</ta>
            <ta e="T489" id="Seg_4896" s="T488">kolbuja-GA</ta>
            <ta e="T490" id="Seg_4897" s="T489">tu͡ok</ta>
            <ta e="T491" id="Seg_4898" s="T490">taba-tA</ta>
            <ta e="T492" id="Seg_4899" s="T491">kel-IAK.[tA]=Ij</ta>
            <ta e="T493" id="Seg_4900" s="T492">manna</ta>
            <ta e="T494" id="Seg_4901" s="T493">uː</ta>
            <ta e="T495" id="Seg_4902" s="T494">ere</ta>
            <ta e="T496" id="Seg_4903" s="T495">min</ta>
            <ta e="T497" id="Seg_4904" s="T496">bil-A-BIn</ta>
            <ta e="T498" id="Seg_4905" s="T497">eːt</ta>
            <ta e="T499" id="Seg_4906" s="T498">dʼaktar-tA</ta>
            <ta e="T500" id="Seg_4907" s="T499">di͡e-BIT</ta>
            <ta e="T501" id="Seg_4908" s="T500">min</ta>
            <ta e="T502" id="Seg_4909" s="T501">kör-BIT-I-m</ta>
            <ta e="T503" id="Seg_4910" s="T502">taba-LAr</ta>
            <ta e="T504" id="Seg_4911" s="T503">karbaː-A</ta>
            <ta e="T505" id="Seg_4912" s="T504">hɨrɨt-Ar-LAr</ta>
            <ta e="T506" id="Seg_4913" s="T505">horok-LAr</ta>
            <ta e="T507" id="Seg_4914" s="T506">čeːlkeː-LAr</ta>
            <ta e="T508" id="Seg_4915" s="T507">horok-LAr</ta>
            <ta e="T509" id="Seg_4916" s="T508">bugdi-LAr</ta>
            <ta e="T510" id="Seg_4917" s="T509">aŋar-LArA</ta>
            <ta e="T511" id="Seg_4918" s="T510">kara-LAr</ta>
            <ta e="T512" id="Seg_4919" s="T511">bütün</ta>
            <ta e="T513" id="Seg_4920" s="T512">ü͡ör</ta>
            <ta e="T514" id="Seg_4921" s="T513">aŋɨr</ta>
            <ta e="T515" id="Seg_4922" s="T514">dʼaktar-GIn</ta>
            <ta e="T516" id="Seg_4923" s="T515">e-BIT</ta>
            <ta e="T517" id="Seg_4924" s="T516">kɨjgan-TI-tA</ta>
            <ta e="T518" id="Seg_4925" s="T517">uː</ta>
            <ta e="T519" id="Seg_4926" s="T518">ičči-tA</ta>
            <ta e="T520" id="Seg_4927" s="T519">tu͡ok</ta>
            <ta e="T521" id="Seg_4928" s="T520">taba-tA</ta>
            <ta e="T522" id="Seg_4929" s="T521">uː</ta>
            <ta e="T523" id="Seg_4930" s="T522">is-tI-GAr</ta>
            <ta e="T524" id="Seg_4931" s="T523">hɨrɨt-IAK.[tA]=Ij</ta>
            <ta e="T525" id="Seg_4932" s="T524">beje-m</ta>
            <ta e="T526" id="Seg_4933" s="T525">bil-A-GIn</ta>
            <ta e="T527" id="Seg_4934" s="T526">bihigi</ta>
            <ta e="T528" id="Seg_4935" s="T527">uː</ta>
            <ta e="T529" id="Seg_4936" s="T528">is-tI-GAr</ta>
            <ta e="T530" id="Seg_4937" s="T529">olor-A-BIt</ta>
            <ta e="T531" id="Seg_4938" s="T530">ol</ta>
            <ta e="T532" id="Seg_4939" s="T531">ihin</ta>
            <ta e="T533" id="Seg_4940" s="T532">taba-LAr</ta>
            <ta e="T534" id="Seg_4941" s="T533">emi͡e</ta>
            <ta e="T535" id="Seg_4942" s="T534">uː</ta>
            <ta e="T536" id="Seg_4943" s="T535">is-tI-GAr</ta>
            <ta e="T537" id="Seg_4944" s="T536">hɨrɨt-Ar-LAr</ta>
            <ta e="T538" id="Seg_4945" s="T537">di͡e-BIT</ta>
            <ta e="T539" id="Seg_4946" s="T538">dʼaktar-tA</ta>
            <ta e="T540" id="Seg_4947" s="T539">kör</ta>
            <ta e="T541" id="Seg_4948" s="T540">dʼe</ta>
            <ta e="T542" id="Seg_4949" s="T541">kolbuja</ta>
            <ta e="T543" id="Seg_4950" s="T542">is-tI-n</ta>
            <ta e="T544" id="Seg_4951" s="T543">uː</ta>
            <ta e="T545" id="Seg_4952" s="T544">ičči-tA</ta>
            <ta e="T546" id="Seg_4953" s="T545">kɨjgan-An</ta>
            <ta e="T547" id="Seg_4954" s="T546">baran</ta>
            <ta e="T548" id="Seg_4955" s="T547">umun-An</ta>
            <ta e="T549" id="Seg_4956" s="T548">keːs-BIT</ta>
            <ta e="T550" id="Seg_4957" s="T549">kolbuja-tI-GAr</ta>
            <ta e="T551" id="Seg_4958" s="T550">öŋöj-BIT</ta>
            <ta e="T552" id="Seg_4959" s="T551">taba-LAr-nI</ta>
            <ta e="T553" id="Seg_4960" s="T552">kör-AːrI</ta>
            <ta e="T554" id="Seg_4961" s="T553">dʼaktar-tA</ta>
            <ta e="T555" id="Seg_4962" s="T554">kel-An</ta>
            <ta e="T556" id="Seg_4963" s="T555">menʼiː-tI-n</ta>
            <ta e="T557" id="Seg_4964" s="T556">uː</ta>
            <ta e="T558" id="Seg_4965" s="T557">is-tI-GAr</ta>
            <ta e="T559" id="Seg_4966" s="T558">hamnar-I</ta>
            <ta e="T560" id="Seg_4967" s="T559">battaː-BIT</ta>
            <ta e="T561" id="Seg_4968" s="T560">uː</ta>
            <ta e="T562" id="Seg_4969" s="T561">ičči-tI-n</ta>
            <ta e="T563" id="Seg_4970" s="T562">menʼiː-tA</ta>
            <ta e="T564" id="Seg_4971" s="T563">tuːra</ta>
            <ta e="T565" id="Seg_4972" s="T564">bar-BIT</ta>
            <ta e="T566" id="Seg_4973" s="T565">honno</ta>
            <ta e="T567" id="Seg_4974" s="T566">öl-BIT</ta>
            <ta e="T568" id="Seg_4975" s="T567">oguru͡o</ta>
            <ta e="T569" id="Seg_4976" s="T568">bɨtɨk</ta>
            <ta e="T570" id="Seg_4977" s="T569">ɨlgɨn</ta>
            <ta e="T571" id="Seg_4978" s="T570">kɨːs-tA</ta>
            <ta e="T572" id="Seg_4979" s="T571">aːn-ttAn</ta>
            <ta e="T573" id="Seg_4980" s="T572">aːn-nI</ta>
            <ta e="T574" id="Seg_4981" s="T573">kerij-A</ta>
            <ta e="T575" id="Seg_4982" s="T574">hɨrɨt-An</ta>
            <ta e="T576" id="Seg_4983" s="T575">edʼij-LAr-tI-n</ta>
            <ta e="T577" id="Seg_4984" s="T576">bul-BIT</ta>
            <ta e="T578" id="Seg_4985" s="T577">aːn-LArI-n</ta>
            <ta e="T579" id="Seg_4986" s="T578">hilim-LArI-n</ta>
            <ta e="T580" id="Seg_4987" s="T579">bahak-I-nAn</ta>
            <ta e="T581" id="Seg_4988" s="T580">kɨhɨj-An</ta>
            <ta e="T582" id="Seg_4989" s="T581">ɨl-An</ta>
            <ta e="T583" id="Seg_4990" s="T582">boskoloː-BIT</ta>
            <ta e="T584" id="Seg_4991" s="T583">üs-IAn-LArA</ta>
            <ta e="T585" id="Seg_4992" s="T584">uraha</ta>
            <ta e="T586" id="Seg_4993" s="T585">dʼi͡e-LArI-GAr</ta>
            <ta e="T587" id="Seg_4994" s="T586">kel-BIT-LAr</ta>
            <ta e="T588" id="Seg_4995" s="T587">oguru͡o</ta>
            <ta e="T589" id="Seg_4996" s="T588">bɨtɨk</ta>
            <ta e="T590" id="Seg_4997" s="T589">ü͡ör-An</ta>
            <ta e="T591" id="Seg_4998" s="T590">kɨːs-LAr-tI-GAr</ta>
            <ta e="T592" id="Seg_4999" s="T591">kɨs-BIT</ta>
            <ta e="T593" id="Seg_5000" s="T592">kɨs-Ar</ta>
            <ta e="T594" id="Seg_5001" s="T593">bus-BIT</ta>
            <ta e="T595" id="Seg_5002" s="T594">balɨk-nI</ta>
            <ta e="T596" id="Seg_5003" s="T595">ahaː-A-t-Ar</ta>
            <ta e="T597" id="Seg_5004" s="T596">ulakan</ta>
            <ta e="T598" id="Seg_5005" s="T597">kɨːs-tA</ta>
            <ta e="T599" id="Seg_5006" s="T598">tojon</ta>
            <ta e="T600" id="Seg_5007" s="T599">čömüje-tI-n</ta>
            <ta e="T601" id="Seg_5008" s="T600">kör-An</ta>
            <ta e="T602" id="Seg_5009" s="T601">ol-GA</ta>
            <ta e="T603" id="Seg_5010" s="T602">orto</ta>
            <ta e="T604" id="Seg_5011" s="T603">kɨːs-tA</ta>
            <ta e="T605" id="Seg_5012" s="T604">orto-GI</ta>
            <ta e="T606" id="Seg_5013" s="T605">čömüje-tI-n</ta>
            <ta e="T607" id="Seg_5014" s="T606">kör-An</ta>
            <ta e="T608" id="Seg_5015" s="T607">oččogo</ta>
            <ta e="T609" id="Seg_5016" s="T608">ere</ta>
            <ta e="T610" id="Seg_5017" s="T609">uː</ta>
            <ta e="T611" id="Seg_5018" s="T610">ičči-tI-n</ta>
            <ta e="T612" id="Seg_5019" s="T611">öjdöː-Ar-LAr</ta>
            <ta e="T613" id="Seg_5020" s="T612">oloŋko-LAː-BIT-tA</ta>
            <ta e="T614" id="Seg_5021" s="T613">Ogdu͡o</ta>
            <ta e="T615" id="Seg_5022" s="T614">Aksʼonava</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_5023" s="T0">listen-IMP.2PL</ta>
            <ta e="T2" id="Seg_5024" s="T1">tale-ACC</ta>
            <ta e="T3" id="Seg_5025" s="T2">name-3SG.[NOM]</ta>
            <ta e="T4" id="Seg_5026" s="T3">beads.[NOM]</ta>
            <ta e="T5" id="Seg_5027" s="T4">beard.[NOM]</ta>
            <ta e="T6" id="Seg_5028" s="T5">one</ta>
            <ta e="T7" id="Seg_5029" s="T6">old.man.[NOM]</ta>
            <ta e="T8" id="Seg_5030" s="T7">there.is</ta>
            <ta e="T9" id="Seg_5031" s="T8">be-PST2.[3SG]</ta>
            <ta e="T10" id="Seg_5032" s="T9">old.man.[NOM]</ta>
            <ta e="T11" id="Seg_5033" s="T10">beard-3SG.[NOM]</ta>
            <ta e="T12" id="Seg_5034" s="T11">space.in.between-PROPR.[NOM]</ta>
            <ta e="T13" id="Seg_5035" s="T12">be-CVB.SEQ</ta>
            <ta e="T14" id="Seg_5036" s="T13">after</ta>
            <ta e="T15" id="Seg_5037" s="T14">long.[NOM]</ta>
            <ta e="T16" id="Seg_5038" s="T15">very</ta>
            <ta e="T17" id="Seg_5039" s="T16">beard-PL-3SG.[NOM]</ta>
            <ta e="T18" id="Seg_5040" s="T17">beads-PROPR</ta>
            <ta e="T19" id="Seg_5041" s="T18">thread.[NOM]</ta>
            <ta e="T20" id="Seg_5042" s="T19">similar</ta>
            <ta e="T21" id="Seg_5043" s="T20">twinkle-ITER-PRS-3PL</ta>
            <ta e="T22" id="Seg_5044" s="T21">that.[NOM]</ta>
            <ta e="T23" id="Seg_5045" s="T22">because.of</ta>
            <ta e="T24" id="Seg_5046" s="T23">old.man-ACC</ta>
            <ta e="T25" id="Seg_5047" s="T24">name-VBZ-PST2-3PL</ta>
            <ta e="T26" id="Seg_5048" s="T25">beads.[NOM]</ta>
            <ta e="T27" id="Seg_5049" s="T26">beard.[NOM]</ta>
            <ta e="T28" id="Seg_5050" s="T27">say-CVB.SEQ</ta>
            <ta e="T29" id="Seg_5051" s="T28">beads.[NOM]</ta>
            <ta e="T30" id="Seg_5052" s="T29">beard.[NOM]</ta>
            <ta e="T31" id="Seg_5053" s="T30">three</ta>
            <ta e="T32" id="Seg_5054" s="T31">daughter-PROPR.[NOM]</ta>
            <ta e="T33" id="Seg_5055" s="T32">in.winter</ta>
            <ta e="T34" id="Seg_5056" s="T33">in.summer</ta>
            <ta e="T35" id="Seg_5057" s="T34">net-VBZ-PRS.[3SG]</ta>
            <ta e="T36" id="Seg_5058" s="T35">fish-EP-INSTR</ta>
            <ta e="T37" id="Seg_5059" s="T36">just</ta>
            <ta e="T38" id="Seg_5060" s="T37">feed-EP-PASS/REFL-CVB.SEQ</ta>
            <ta e="T39" id="Seg_5061" s="T38">sit-PRS-3PL</ta>
            <ta e="T40" id="Seg_5062" s="T39">once</ta>
            <ta e="T41" id="Seg_5063" s="T40">beads.[NOM]</ta>
            <ta e="T42" id="Seg_5064" s="T41">beard.[NOM]</ta>
            <ta e="T43" id="Seg_5065" s="T42">net.[NOM]</ta>
            <ta e="T44" id="Seg_5066" s="T43">see-EP-MED-CVB.SIM</ta>
            <ta e="T45" id="Seg_5067" s="T44">go-PST2.[3SG]</ta>
            <ta e="T46" id="Seg_5068" s="T45">icehole-3SG-DAT/LOC</ta>
            <ta e="T47" id="Seg_5069" s="T46">bow-PTCP.PRS-3SG-ACC</ta>
            <ta e="T48" id="Seg_5070" s="T47">with</ta>
            <ta e="T49" id="Seg_5071" s="T48">who.[NOM]</ta>
            <ta e="T50" id="Seg_5072" s="T49">INDEF</ta>
            <ta e="T51" id="Seg_5073" s="T50">beard-3SG-ABL</ta>
            <ta e="T52" id="Seg_5074" s="T51">grab-CVB.SEQ</ta>
            <ta e="T53" id="Seg_5075" s="T52">take-PST2.[3SG]</ta>
            <ta e="T54" id="Seg_5076" s="T53">at.all</ta>
            <ta e="T55" id="Seg_5077" s="T54">release-NEG.[3SG]</ta>
            <ta e="T56" id="Seg_5078" s="T55">who.[NOM]</ta>
            <ta e="T57" id="Seg_5079" s="T56">be-FUT.[3SG]=Q</ta>
            <ta e="T58" id="Seg_5080" s="T57">think-CVB.SIM</ta>
            <ta e="T59" id="Seg_5081" s="T58">think-PST2.[3SG]</ta>
            <ta e="T60" id="Seg_5082" s="T59">icehole-ABL</ta>
            <ta e="T61" id="Seg_5083" s="T60">lean.out-PST2.[3SG]</ta>
            <ta e="T62" id="Seg_5084" s="T61">water.[NOM]</ta>
            <ta e="T63" id="Seg_5085" s="T62">master-3SG.[NOM]</ta>
            <ta e="T64" id="Seg_5086" s="T63">beads.[NOM]</ta>
            <ta e="T65" id="Seg_5087" s="T64">beard.[NOM]</ta>
            <ta e="T66" id="Seg_5088" s="T65">say-PRS.[3SG]</ta>
            <ta e="T67" id="Seg_5089" s="T66">daughter-2SG-ACC</ta>
            <ta e="T68" id="Seg_5090" s="T67">give-PRS-2SG</ta>
            <ta e="T69" id="Seg_5091" s="T68">Q</ta>
            <ta e="T70" id="Seg_5092" s="T69">NEG</ta>
            <ta e="T71" id="Seg_5093" s="T70">Q</ta>
            <ta e="T72" id="Seg_5094" s="T71">woman.[NOM]</ta>
            <ta e="T73" id="Seg_5095" s="T72">make-PTCP.FUT-1SG-ACC</ta>
            <ta e="T74" id="Seg_5096" s="T73">give-NEG-TEMP-2SG</ta>
            <ta e="T75" id="Seg_5097" s="T74">one</ta>
            <ta e="T76" id="Seg_5098" s="T75">NEG</ta>
            <ta e="T77" id="Seg_5099" s="T76">fish-ACC</ta>
            <ta e="T78" id="Seg_5100" s="T77">take-FUT-2SG</ta>
            <ta e="T79" id="Seg_5101" s="T78">NEG-3SG</ta>
            <ta e="T80" id="Seg_5102" s="T79">what-EP-INSTR</ta>
            <ta e="T81" id="Seg_5103" s="T80">family-PL-1SG-ACC</ta>
            <ta e="T82" id="Seg_5104" s="T81">feed-FUT-1SG=Q</ta>
            <ta e="T83" id="Seg_5105" s="T82">well</ta>
            <ta e="T84" id="Seg_5106" s="T83">think-CVB.SIM</ta>
            <ta e="T85" id="Seg_5107" s="T84">think-PST2.[3SG]</ta>
            <ta e="T86" id="Seg_5108" s="T85">then</ta>
            <ta e="T87" id="Seg_5109" s="T86">agree-PST2.[3SG]</ta>
            <ta e="T88" id="Seg_5110" s="T87">deception-VBZ-TEMP-2SG</ta>
            <ta e="T89" id="Seg_5111" s="T88">however</ta>
            <ta e="T90" id="Seg_5112" s="T89">whatever</ta>
            <ta e="T91" id="Seg_5113" s="T90">find-FUT-1SG</ta>
            <ta e="T92" id="Seg_5114" s="T91">say-PST2.[3SG]</ta>
            <ta e="T93" id="Seg_5115" s="T92">water.[NOM]</ta>
            <ta e="T94" id="Seg_5116" s="T93">master-3SG.[NOM]</ta>
            <ta e="T95" id="Seg_5117" s="T94">old.man.[NOM]</ta>
            <ta e="T96" id="Seg_5118" s="T95">mitten-3SG-ACC</ta>
            <ta e="T97" id="Seg_5119" s="T96">grab-CVB.SEQ</ta>
            <ta e="T98" id="Seg_5120" s="T97">take-CVB.SEQ</ta>
            <ta e="T99" id="Seg_5121" s="T98">after</ta>
            <ta e="T100" id="Seg_5122" s="T99">ice.[NOM]</ta>
            <ta e="T101" id="Seg_5123" s="T100">inside-3SG-INSTR</ta>
            <ta e="T102" id="Seg_5124" s="T101">go-CVB.SEQ</ta>
            <ta e="T103" id="Seg_5125" s="T102">stay-PST2.[3SG]</ta>
            <ta e="T104" id="Seg_5126" s="T103">beads.[NOM]</ta>
            <ta e="T105" id="Seg_5127" s="T104">beard.[NOM]</ta>
            <ta e="T106" id="Seg_5128" s="T105">very</ta>
            <ta e="T107" id="Seg_5129" s="T106">be.confused-PST2.[3SG]</ta>
            <ta e="T108" id="Seg_5130" s="T107">pole.[NOM]</ta>
            <ta e="T109" id="Seg_5131" s="T108">tent-3SG-DAT/LOC</ta>
            <ta e="T110" id="Seg_5132" s="T109">come-CVB.SEQ</ta>
            <ta e="T111" id="Seg_5133" s="T110">daughter-PL-3SG-DAT/LOC</ta>
            <ta e="T112" id="Seg_5134" s="T111">tell-PST2.[3SG]</ta>
            <ta e="T113" id="Seg_5135" s="T112">big</ta>
            <ta e="T114" id="Seg_5136" s="T113">daughter-3SG.[NOM]</ta>
            <ta e="T115" id="Seg_5137" s="T114">be.happy-PST2.[3SG]</ta>
            <ta e="T116" id="Seg_5138" s="T115">1SG.[NOM]</ta>
            <ta e="T117" id="Seg_5139" s="T116">husband-DAT/LOC</ta>
            <ta e="T118" id="Seg_5140" s="T117">go-FUT-1SG</ta>
            <ta e="T119" id="Seg_5141" s="T118">water.[NOM]</ta>
            <ta e="T120" id="Seg_5142" s="T119">master-3SG-DAT/LOC</ta>
            <ta e="T121" id="Seg_5143" s="T120">world.[NOM]</ta>
            <ta e="T122" id="Seg_5144" s="T121">upper.part-3SG-INSTR</ta>
            <ta e="T123" id="Seg_5145" s="T122">most</ta>
            <ta e="T124" id="Seg_5146" s="T123">rich.[NOM]</ta>
            <ta e="T125" id="Seg_5147" s="T124">be-FUT-1SG</ta>
            <ta e="T126" id="Seg_5148" s="T125">beads.[NOM]</ta>
            <ta e="T127" id="Seg_5149" s="T126">beard.[NOM]</ta>
            <ta e="T128" id="Seg_5150" s="T127">daughter-3SG-ACC</ta>
            <ta e="T129" id="Seg_5151" s="T128">husband-DAT/LOC</ta>
            <ta e="T130" id="Seg_5152" s="T129">give-CVB.SEQ</ta>
            <ta e="T131" id="Seg_5153" s="T130">throw-PST2.[3SG]</ta>
            <ta e="T132" id="Seg_5154" s="T131">water.[NOM]</ta>
            <ta e="T133" id="Seg_5155" s="T132">master-3SG.[NOM]</ta>
            <ta e="T134" id="Seg_5156" s="T133">rich.[NOM]</ta>
            <ta e="T135" id="Seg_5157" s="T134">and</ta>
            <ta e="T136" id="Seg_5158" s="T135">rich.[NOM]</ta>
            <ta e="T137" id="Seg_5159" s="T136">wealth-3SG-GEN</ta>
            <ta e="T138" id="Seg_5160" s="T137">end-3SG.[NOM]</ta>
            <ta e="T139" id="Seg_5161" s="T138">to.be.on.view-EP-NEG.[3SG]</ta>
            <ta e="T140" id="Seg_5162" s="T139">how.much</ta>
            <ta e="T141" id="Seg_5163" s="T140">INDEF</ta>
            <ta e="T142" id="Seg_5164" s="T141">room-PROPR.[NOM]</ta>
            <ta e="T143" id="Seg_5165" s="T142">house-PROPR.[NOM]</ta>
            <ta e="T144" id="Seg_5166" s="T143">one</ta>
            <ta e="T145" id="Seg_5167" s="T144">room-DAT/LOC</ta>
            <ta e="T146" id="Seg_5168" s="T145">big</ta>
            <ta e="T147" id="Seg_5169" s="T146">ice.[NOM]</ta>
            <ta e="T148" id="Seg_5170" s="T147">chest.[NOM]</ta>
            <ta e="T149" id="Seg_5171" s="T148">stand-PRS.[3SG]</ta>
            <ta e="T150" id="Seg_5172" s="T149">water.[NOM]</ta>
            <ta e="T151" id="Seg_5173" s="T150">master-3SG.[NOM]</ta>
            <ta e="T152" id="Seg_5174" s="T151">woman-3SG-ACC</ta>
            <ta e="T153" id="Seg_5175" s="T152">get-CVB.SEQ</ta>
            <ta e="T154" id="Seg_5176" s="T153">ice.[NOM]</ta>
            <ta e="T155" id="Seg_5177" s="T154">chest-ACC</ta>
            <ta e="T156" id="Seg_5178" s="T155">show-PST2.[3SG]</ta>
            <ta e="T157" id="Seg_5179" s="T156">where</ta>
            <ta e="T158" id="Seg_5180" s="T157">think-PTCP.PRS</ta>
            <ta e="T159" id="Seg_5181" s="T158">go.[IMP.2SG]</ta>
            <ta e="T160" id="Seg_5182" s="T159">see.[IMP.2SG]</ta>
            <ta e="T161" id="Seg_5183" s="T160">this.[NOM]</ta>
            <ta e="T162" id="Seg_5184" s="T161">just</ta>
            <ta e="T164" id="Seg_5185" s="T163">chest.[NOM]</ta>
            <ta e="T165" id="Seg_5186" s="T164">inside-3SG-DAT/LOC</ta>
            <ta e="T166" id="Seg_5187" s="T165">stick-EP-NEG.[IMP.2SG]</ta>
            <ta e="T167" id="Seg_5188" s="T166">finger-2SG-ACC</ta>
            <ta e="T168" id="Seg_5189" s="T167">say-PST2.[3SG]</ta>
            <ta e="T169" id="Seg_5190" s="T168">husband-3SG.[NOM]</ta>
            <ta e="T170" id="Seg_5191" s="T169">go-PTCP.PST-3SG-ACC</ta>
            <ta e="T171" id="Seg_5192" s="T170">after</ta>
            <ta e="T172" id="Seg_5193" s="T171">this</ta>
            <ta e="T173" id="Seg_5194" s="T172">woman.[NOM]</ta>
            <ta e="T174" id="Seg_5195" s="T173">bear-NEG.CVB.SIM</ta>
            <ta e="T175" id="Seg_5196" s="T174">finger-3SG-ACC</ta>
            <ta e="T176" id="Seg_5197" s="T175">stick-PST2.[3SG]</ta>
            <ta e="T177" id="Seg_5198" s="T176">water.[NOM]</ta>
            <ta e="T178" id="Seg_5199" s="T177">ice.[NOM]</ta>
            <ta e="T179" id="Seg_5200" s="T178">become-PST2.[3SG]</ta>
            <ta e="T180" id="Seg_5201" s="T179">hardly</ta>
            <ta e="T181" id="Seg_5202" s="T180">PFV</ta>
            <ta e="T182" id="Seg_5203" s="T181">pull-PST2.[3SG]</ta>
            <ta e="T183" id="Seg_5204" s="T182">finger-3SG-ACC</ta>
            <ta e="T184" id="Seg_5205" s="T183">see-PST2-3SG</ta>
            <ta e="T185" id="Seg_5206" s="T184">lord.[NOM]</ta>
            <ta e="T186" id="Seg_5207" s="T185">finger-3SG.[NOM]</ta>
            <ta e="T187" id="Seg_5208" s="T186">completely</ta>
            <ta e="T188" id="Seg_5209" s="T187">go-PST2.[3SG]</ta>
            <ta e="T189" id="Seg_5210" s="T188">that-3SG-3SG-ACC</ta>
            <ta e="T190" id="Seg_5211" s="T189">strongly</ta>
            <ta e="T191" id="Seg_5212" s="T190">tie-CVB.SEQ</ta>
            <ta e="T192" id="Seg_5213" s="T191">after</ta>
            <ta e="T193" id="Seg_5214" s="T192">sit-PRS.[3SG]</ta>
            <ta e="T194" id="Seg_5215" s="T193">be.afraid-CVB.SEQ</ta>
            <ta e="T195" id="Seg_5216" s="T194">eye-PL-3SG.[NOM]</ta>
            <ta e="T196" id="Seg_5217" s="T195">come.out-ADVZ</ta>
            <ta e="T197" id="Seg_5218" s="T196">go-PST2-3PL</ta>
            <ta e="T198" id="Seg_5219" s="T197">water.[NOM]</ta>
            <ta e="T199" id="Seg_5220" s="T198">master-3SG.[NOM]</ta>
            <ta e="T200" id="Seg_5221" s="T199">come-CVB.SEQ</ta>
            <ta e="T201" id="Seg_5222" s="T200">immediately</ta>
            <ta e="T202" id="Seg_5223" s="T201">guess-PST1-3SG</ta>
            <ta e="T203" id="Seg_5224" s="T202">1SG-DAT/LOC</ta>
            <ta e="T204" id="Seg_5225" s="T203">2SG.[NOM]</ta>
            <ta e="T205" id="Seg_5226" s="T204">woman.[NOM]</ta>
            <ta e="T206" id="Seg_5227" s="T205">be-NEG.PTCP.[NOM]</ta>
            <ta e="T207" id="Seg_5228" s="T206">like-2SG</ta>
            <ta e="T208" id="Seg_5229" s="T207">say-PST2.[3SG]</ta>
            <ta e="T209" id="Seg_5230" s="T208">woman-ACC</ta>
            <ta e="T210" id="Seg_5231" s="T209">most</ta>
            <ta e="T211" id="Seg_5232" s="T210">distant-ADJZ</ta>
            <ta e="T212" id="Seg_5233" s="T211">house-DAT/LOC</ta>
            <ta e="T213" id="Seg_5234" s="T212">room-3SG-DAT/LOC</ta>
            <ta e="T214" id="Seg_5235" s="T213">take.away-CVB.SEQ</ta>
            <ta e="T215" id="Seg_5236" s="T214">lock-CVB.SEQ</ta>
            <ta e="T216" id="Seg_5237" s="T215">throw-PST2.[3SG]</ta>
            <ta e="T217" id="Seg_5238" s="T216">door-3SG-ACC</ta>
            <ta e="T218" id="Seg_5239" s="T217">tightly</ta>
            <ta e="T219" id="Seg_5240" s="T218">sturgeon.[NOM]</ta>
            <ta e="T220" id="Seg_5241" s="T219">stomach-3SG-INSTR</ta>
            <ta e="T221" id="Seg_5242" s="T220">be.made-EP-PTCP.PST</ta>
            <ta e="T222" id="Seg_5243" s="T221">glue-EP-INSTR</ta>
            <ta e="T223" id="Seg_5244" s="T222">strongly</ta>
            <ta e="T224" id="Seg_5245" s="T223">make.dirty-PST2.[3SG]</ta>
            <ta e="T225" id="Seg_5246" s="T224">beads.[NOM]</ta>
            <ta e="T226" id="Seg_5247" s="T225">beard.[NOM]</ta>
            <ta e="T227" id="Seg_5248" s="T226">again</ta>
            <ta e="T228" id="Seg_5249" s="T227">net-VBZ-CVB.SIM</ta>
            <ta e="T229" id="Seg_5250" s="T228">come-PST2.[3SG]</ta>
            <ta e="T230" id="Seg_5251" s="T229">icehole-3SG-DAT/LOC</ta>
            <ta e="T231" id="Seg_5252" s="T230">bow-PTCP.PRS-3SG-ACC</ta>
            <ta e="T232" id="Seg_5253" s="T231">with</ta>
            <ta e="T233" id="Seg_5254" s="T232">water.[NOM]</ta>
            <ta e="T234" id="Seg_5255" s="T233">master-3SG.[NOM]</ta>
            <ta e="T235" id="Seg_5256" s="T234">again</ta>
            <ta e="T236" id="Seg_5257" s="T235">beard-3SG-ABL</ta>
            <ta e="T237" id="Seg_5258" s="T236">grab-CVB.SEQ</ta>
            <ta e="T238" id="Seg_5259" s="T237">take-PST2.[3SG]</ta>
            <ta e="T239" id="Seg_5260" s="T238">get.[IMP.2SG]</ta>
            <ta e="T240" id="Seg_5261" s="T239">two-ORD</ta>
            <ta e="T241" id="Seg_5262" s="T240">daughter-2SG-ACC</ta>
            <ta e="T242" id="Seg_5263" s="T241">woman.[NOM]</ta>
            <ta e="T243" id="Seg_5264" s="T242">make-PTCP.FUT-1SG-ACC</ta>
            <ta e="T244" id="Seg_5265" s="T243">old.man.[NOM]</ta>
            <ta e="T245" id="Seg_5266" s="T244">misery-ACC</ta>
            <ta e="T246" id="Seg_5267" s="T245">come.to.an.end-FUT.[3SG]</ta>
            <ta e="T247" id="Seg_5268" s="T246">Q</ta>
            <ta e="T248" id="Seg_5269" s="T247">middle</ta>
            <ta e="T249" id="Seg_5270" s="T248">daughter-3SG-ACC</ta>
            <ta e="T250" id="Seg_5271" s="T249">give-PST2.[3SG]</ta>
            <ta e="T251" id="Seg_5272" s="T250">this</ta>
            <ta e="T252" id="Seg_5273" s="T251">woman.[NOM]</ta>
            <ta e="T253" id="Seg_5274" s="T252">wealth.[NOM]</ta>
            <ta e="T254" id="Seg_5275" s="T253">see-EP-MED-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T255" id="Seg_5276" s="T254">free.time-POSS</ta>
            <ta e="T256" id="Seg_5277" s="T255">NEG.[3SG]</ta>
            <ta e="T257" id="Seg_5278" s="T256">wonder-CVB.SEQ</ta>
            <ta e="T258" id="Seg_5279" s="T257">after</ta>
            <ta e="T259" id="Seg_5280" s="T258">stop-NEG.[3SG]</ta>
            <ta e="T260" id="Seg_5281" s="T259">once</ta>
            <ta e="T261" id="Seg_5282" s="T260">this</ta>
            <ta e="T262" id="Seg_5283" s="T261">ice.[NOM]</ta>
            <ta e="T263" id="Seg_5284" s="T262">chest-DAT/LOC</ta>
            <ta e="T264" id="Seg_5285" s="T263">bring-PST2.[3SG]</ta>
            <ta e="T265" id="Seg_5286" s="T264">water.[NOM]</ta>
            <ta e="T266" id="Seg_5287" s="T265">master-3SG.[NOM]</ta>
            <ta e="T267" id="Seg_5288" s="T266">bring-CVB.SEQ</ta>
            <ta e="T268" id="Seg_5289" s="T267">warn-PST2.[3SG]</ta>
            <ta e="T269" id="Seg_5290" s="T268">this</ta>
            <ta e="T270" id="Seg_5291" s="T269">chest-DAT/LOC</ta>
            <ta e="T271" id="Seg_5292" s="T270">hand-2SG-ACC</ta>
            <ta e="T272" id="Seg_5293" s="T271">NEG</ta>
            <ta e="T273" id="Seg_5294" s="T272">finger-2SG-ACC</ta>
            <ta e="T274" id="Seg_5295" s="T273">NEG</ta>
            <ta e="T275" id="Seg_5296" s="T274">stick-APPR-2SG</ta>
            <ta e="T276" id="Seg_5297" s="T275">water.[NOM]</ta>
            <ta e="T277" id="Seg_5298" s="T276">master-3SG.[NOM]</ta>
            <ta e="T278" id="Seg_5299" s="T277">sledge-VBZ-CVB.SIM</ta>
            <ta e="T279" id="Seg_5300" s="T278">go-PTCP.PST-3SG-ACC</ta>
            <ta e="T280" id="Seg_5301" s="T279">after</ta>
            <ta e="T281" id="Seg_5302" s="T280">woman-3SG.[NOM]</ta>
            <ta e="T282" id="Seg_5303" s="T281">ice.[NOM]</ta>
            <ta e="T283" id="Seg_5304" s="T282">chest-DAT/LOC</ta>
            <ta e="T284" id="Seg_5305" s="T283">come-CVB.SEQ</ta>
            <ta e="T285" id="Seg_5306" s="T284">finger-3SG-ACC</ta>
            <ta e="T286" id="Seg_5307" s="T285">stick-PST2.[3SG]</ta>
            <ta e="T287" id="Seg_5308" s="T286">middle-ADJZ</ta>
            <ta e="T288" id="Seg_5309" s="T287">finger-3SG.[NOM]</ta>
            <ta e="T289" id="Seg_5310" s="T288">completely</ta>
            <ta e="T290" id="Seg_5311" s="T289">freeze-PST2.[3SG]</ta>
            <ta e="T291" id="Seg_5312" s="T290">finger-3SG-ACC</ta>
            <ta e="T292" id="Seg_5313" s="T291">strongly</ta>
            <ta e="T293" id="Seg_5314" s="T292">tie-PST2.[3SG]</ta>
            <ta e="T294" id="Seg_5315" s="T293">water.[NOM]</ta>
            <ta e="T295" id="Seg_5316" s="T294">master-3SG.[NOM]</ta>
            <ta e="T296" id="Seg_5317" s="T295">come-CVB.SEQ</ta>
            <ta e="T297" id="Seg_5318" s="T296">immediately</ta>
            <ta e="T298" id="Seg_5319" s="T297">guess-PST2.[3SG]</ta>
            <ta e="T299" id="Seg_5320" s="T298">1SG-DAT/LOC</ta>
            <ta e="T300" id="Seg_5321" s="T299">obedient-POSS</ta>
            <ta e="T301" id="Seg_5322" s="T300">NEG</ta>
            <ta e="T302" id="Seg_5323" s="T301">woman.[NOM]</ta>
            <ta e="T303" id="Seg_5324" s="T302">need-POSS</ta>
            <ta e="T304" id="Seg_5325" s="T303">NEG.[3SG]</ta>
            <ta e="T305" id="Seg_5326" s="T304">say-CVB.SEQ</ta>
            <ta e="T306" id="Seg_5327" s="T305">after</ta>
            <ta e="T307" id="Seg_5328" s="T306">distant-ADJZ</ta>
            <ta e="T308" id="Seg_5329" s="T307">house.[NOM]</ta>
            <ta e="T309" id="Seg_5330" s="T308">room-3SG-DAT/LOC</ta>
            <ta e="T310" id="Seg_5331" s="T309">block-CVB.SEQ</ta>
            <ta e="T311" id="Seg_5332" s="T310">throw-PST2.[3SG]</ta>
            <ta e="T312" id="Seg_5333" s="T311">door-3SG-ACC</ta>
            <ta e="T313" id="Seg_5334" s="T312">tightly</ta>
            <ta e="T314" id="Seg_5335" s="T313">sturgeon.[NOM]</ta>
            <ta e="T315" id="Seg_5336" s="T314">stomach-3SG-INSTR</ta>
            <ta e="T316" id="Seg_5337" s="T315">be.made-EP-PTCP.PST</ta>
            <ta e="T317" id="Seg_5338" s="T316">glue-EP-INSTR</ta>
            <ta e="T318" id="Seg_5339" s="T317">strongly</ta>
            <ta e="T319" id="Seg_5340" s="T318">make.dirty-PST2.[3SG]</ta>
            <ta e="T320" id="Seg_5341" s="T319">beads.[NOM]</ta>
            <ta e="T321" id="Seg_5342" s="T320">beard.[NOM]</ta>
            <ta e="T322" id="Seg_5343" s="T321">again</ta>
            <ta e="T323" id="Seg_5344" s="T322">net.[NOM]</ta>
            <ta e="T324" id="Seg_5345" s="T323">see-EP-MED-CVB.SIM</ta>
            <ta e="T325" id="Seg_5346" s="T324">come-PST2.[3SG]</ta>
            <ta e="T326" id="Seg_5347" s="T325">icehole-3SG-DAT/LOC</ta>
            <ta e="T327" id="Seg_5348" s="T326">bow-PTCP.PRS-3SG-ACC</ta>
            <ta e="T328" id="Seg_5349" s="T327">with</ta>
            <ta e="T329" id="Seg_5350" s="T328">water.[NOM]</ta>
            <ta e="T330" id="Seg_5351" s="T329">master-3SG.[NOM]</ta>
            <ta e="T331" id="Seg_5352" s="T330">again</ta>
            <ta e="T332" id="Seg_5353" s="T331">beard-3SG-ABL</ta>
            <ta e="T333" id="Seg_5354" s="T332">grab-CVB.SEQ</ta>
            <ta e="T334" id="Seg_5355" s="T333">take-PST2.[3SG]</ta>
            <ta e="T335" id="Seg_5356" s="T334">bring.[IMP.2SG]</ta>
            <ta e="T336" id="Seg_5357" s="T335">little</ta>
            <ta e="T337" id="Seg_5358" s="T336">daughter-2SG-ACC</ta>
            <ta e="T338" id="Seg_5359" s="T337">woman.[NOM]</ta>
            <ta e="T339" id="Seg_5360" s="T338">make-PTCP.FUT-1SG-ACC</ta>
            <ta e="T340" id="Seg_5361" s="T339">say-PST2.[3SG]</ta>
            <ta e="T341" id="Seg_5362" s="T340">misery-ACC</ta>
            <ta e="T342" id="Seg_5363" s="T341">come.to.an.end-FUT.[3SG]</ta>
            <ta e="T343" id="Seg_5364" s="T342">Q</ta>
            <ta e="T344" id="Seg_5365" s="T343">little</ta>
            <ta e="T345" id="Seg_5366" s="T344">daughter-3SG-ACC</ta>
            <ta e="T346" id="Seg_5367" s="T345">bring-PST2.[3SG]</ta>
            <ta e="T347" id="Seg_5368" s="T346">go-CVB.SEQ</ta>
            <ta e="T348" id="Seg_5369" s="T347">go-CVB.SEQ-3PL</ta>
            <ta e="T349" id="Seg_5370" s="T348">daughter-3SG.[NOM]</ta>
            <ta e="T350" id="Seg_5371" s="T349">say-PST2.[3SG]</ta>
            <ta e="T351" id="Seg_5372" s="T350">be.afraid-EP-NEG.[IMP.2SG]</ta>
            <ta e="T352" id="Seg_5373" s="T351">father.[NOM]</ta>
            <ta e="T353" id="Seg_5374" s="T352">1SG.[NOM]</ta>
            <ta e="T354" id="Seg_5375" s="T353">immediately</ta>
            <ta e="T355" id="Seg_5376" s="T354">come-FUT-1SG</ta>
            <ta e="T356" id="Seg_5377" s="T355">older.sister-PL-1SG-ACC</ta>
            <ta e="T357" id="Seg_5378" s="T356">also</ta>
            <ta e="T358" id="Seg_5379" s="T357">bring-FUT-1SG</ta>
            <ta e="T359" id="Seg_5380" s="T358">water.[NOM]</ta>
            <ta e="T360" id="Seg_5381" s="T359">master-3SG.[NOM]</ta>
            <ta e="T361" id="Seg_5382" s="T360">1SG-COMP</ta>
            <ta e="T362" id="Seg_5383" s="T361">slyness-POSS</ta>
            <ta e="T363" id="Seg_5384" s="T362">NEG</ta>
            <ta e="T364" id="Seg_5385" s="T363">be-FUT.[3SG]</ta>
            <ta e="T365" id="Seg_5386" s="T364">probably</ta>
            <ta e="T366" id="Seg_5387" s="T365">water.[NOM]</ta>
            <ta e="T367" id="Seg_5388" s="T366">master-3SG.[NOM]</ta>
            <ta e="T368" id="Seg_5389" s="T367">three-ORD</ta>
            <ta e="T369" id="Seg_5390" s="T368">woman-3SG-ACC</ta>
            <ta e="T370" id="Seg_5391" s="T369">ice.[NOM]</ta>
            <ta e="T371" id="Seg_5392" s="T370">chest-DAT/LOC</ta>
            <ta e="T372" id="Seg_5393" s="T371">bring-CVB.SEQ</ta>
            <ta e="T373" id="Seg_5394" s="T372">after</ta>
            <ta e="T374" id="Seg_5395" s="T373">say-PST2.[3SG]</ta>
            <ta e="T375" id="Seg_5396" s="T374">this</ta>
            <ta e="T376" id="Seg_5397" s="T375">chest-DAT/LOC</ta>
            <ta e="T377" id="Seg_5398" s="T376">hand-2SG-ACC</ta>
            <ta e="T378" id="Seg_5399" s="T377">stick-EP-NEG.[IMP.2SG]</ta>
            <ta e="T379" id="Seg_5400" s="T378">come.back-CVB.SEQ</ta>
            <ta e="T380" id="Seg_5401" s="T379">come-TEMP-1SG</ta>
            <ta e="T381" id="Seg_5402" s="T380">see-FUT-1SG</ta>
            <ta e="T382" id="Seg_5403" s="T381">1SG-ACC</ta>
            <ta e="T383" id="Seg_5404" s="T382">deception-VBZ-FUT-2SG</ta>
            <ta e="T384" id="Seg_5405" s="T383">NEG-3SG</ta>
            <ta e="T385" id="Seg_5406" s="T384">why</ta>
            <ta e="T386" id="Seg_5407" s="T385">1SG-ACC</ta>
            <ta e="T387" id="Seg_5408" s="T386">warn-PRS.[3SG]</ta>
            <ta e="T388" id="Seg_5409" s="T387">think-CVB.SIM</ta>
            <ta e="T389" id="Seg_5410" s="T388">think-PST2.[3SG]</ta>
            <ta e="T390" id="Seg_5411" s="T389">this</ta>
            <ta e="T391" id="Seg_5412" s="T390">woman.[NOM]</ta>
            <ta e="T392" id="Seg_5413" s="T391">what.[NOM]</ta>
            <ta e="T393" id="Seg_5414" s="T392">be-FUT.[3SG]=Q</ta>
            <ta e="T394" id="Seg_5415" s="T393">well</ta>
            <ta e="T395" id="Seg_5416" s="T394">hand-1SG-ACC</ta>
            <ta e="T396" id="Seg_5417" s="T395">stick-TEMP-1SG</ta>
            <ta e="T397" id="Seg_5418" s="T396">water.[NOM]</ta>
            <ta e="T398" id="Seg_5419" s="T397">water.[NOM]</ta>
            <ta e="T399" id="Seg_5420" s="T398">similar</ta>
            <ta e="T400" id="Seg_5421" s="T399">probably</ta>
            <ta e="T401" id="Seg_5422" s="T400">what.[NOM]</ta>
            <ta e="T402" id="Seg_5423" s="T401">INDEF</ta>
            <ta e="T403" id="Seg_5424" s="T402">trick-3SG.[NOM]</ta>
            <ta e="T404" id="Seg_5425" s="T403">there.is-3SG</ta>
            <ta e="T405" id="Seg_5426" s="T404">probably</ta>
            <ta e="T406" id="Seg_5427" s="T405">nelma-ACC</ta>
            <ta e="T407" id="Seg_5428" s="T406">grab-CVB.SEQ</ta>
            <ta e="T408" id="Seg_5429" s="T407">take-CVB.SEQ</ta>
            <ta e="T409" id="Seg_5430" s="T408">after</ta>
            <ta e="T410" id="Seg_5431" s="T409">tail-3SG-ACC</ta>
            <ta e="T411" id="Seg_5432" s="T410">stick-PST2.[3SG]</ta>
            <ta e="T412" id="Seg_5433" s="T411">ice.[NOM]</ta>
            <ta e="T413" id="Seg_5434" s="T412">chest.[NOM]</ta>
            <ta e="T414" id="Seg_5435" s="T413">inside-3SG-DAT/LOC</ta>
            <ta e="T415" id="Seg_5436" s="T414">nelma.[NOM]</ta>
            <ta e="T416" id="Seg_5437" s="T415">tail-3SG.[NOM]</ta>
            <ta e="T417" id="Seg_5438" s="T416">completely</ta>
            <ta e="T418" id="Seg_5439" s="T417">break-CVB.SEQ</ta>
            <ta e="T419" id="Seg_5440" s="T418">stay-PST2.[3SG]</ta>
            <ta e="T420" id="Seg_5441" s="T419">beads.[NOM]</ta>
            <ta e="T421" id="Seg_5442" s="T420">beard.[NOM]</ta>
            <ta e="T422" id="Seg_5443" s="T421">daughter-3SG.[NOM]</ta>
            <ta e="T423" id="Seg_5444" s="T422">guess-PST1-3SG</ta>
            <ta e="T424" id="Seg_5445" s="T423">why</ta>
            <ta e="T425" id="Seg_5446" s="T424">water.[NOM]</ta>
            <ta e="T426" id="Seg_5447" s="T425">master-3SG.[NOM]</ta>
            <ta e="T427" id="Seg_5448" s="T426">warn-PTCP.PST-3SG-ACC</ta>
            <ta e="T428" id="Seg_5449" s="T427">chest.[NOM]</ta>
            <ta e="T429" id="Seg_5450" s="T428">place.beneath-3SG-DAT/LOC</ta>
            <ta e="T430" id="Seg_5451" s="T429">sit-CVB.SEQ</ta>
            <ta e="T431" id="Seg_5452" s="T430">after</ta>
            <ta e="T432" id="Seg_5453" s="T431">net.[NOM]</ta>
            <ta e="T433" id="Seg_5454" s="T432">patch-EP-MED-CVB.SIM</ta>
            <ta e="T434" id="Seg_5455" s="T433">sit-PRS.[3SG]</ta>
            <ta e="T435" id="Seg_5456" s="T434">water.[NOM]</ta>
            <ta e="T436" id="Seg_5457" s="T435">master-3SG.[NOM]</ta>
            <ta e="T437" id="Seg_5458" s="T436">come-CVB.SEQ</ta>
            <ta e="T438" id="Seg_5459" s="T437">wonder-PST1-3SG</ta>
            <ta e="T439" id="Seg_5460" s="T438">what</ta>
            <ta e="T440" id="Seg_5461" s="T439">net-3SG-ACC</ta>
            <ta e="T441" id="Seg_5462" s="T440">patch-PRS-2SG</ta>
            <ta e="T442" id="Seg_5463" s="T441">water.[NOM]</ta>
            <ta e="T443" id="Seg_5464" s="T442">inside-3SG-DAT/LOC</ta>
            <ta e="T444" id="Seg_5465" s="T443">fish.[NOM]</ta>
            <ta e="T445" id="Seg_5466" s="T444">many</ta>
            <ta e="T446" id="Seg_5467" s="T445">net-POSS</ta>
            <ta e="T447" id="Seg_5468" s="T446">and</ta>
            <ta e="T448" id="Seg_5469" s="T447">NEG</ta>
            <ta e="T449" id="Seg_5470" s="T448">1SG.[NOM]</ta>
            <ta e="T450" id="Seg_5471" s="T449">2SG-DAT/LOC</ta>
            <ta e="T451" id="Seg_5472" s="T450">how.much-ACC</ta>
            <ta e="T452" id="Seg_5473" s="T451">think-PTCP.PRS-ACC</ta>
            <ta e="T453" id="Seg_5474" s="T452">bring-FUT-1SG</ta>
            <ta e="T454" id="Seg_5475" s="T453">fish.[NOM]</ta>
            <ta e="T455" id="Seg_5476" s="T454">many</ta>
            <ta e="T456" id="Seg_5477" s="T455">be-CVB.SEQ</ta>
            <ta e="T457" id="Seg_5478" s="T456">after</ta>
            <ta e="T458" id="Seg_5479" s="T457">say-PRS.[3SG]</ta>
            <ta e="T459" id="Seg_5480" s="T458">woman-3SG.[NOM]</ta>
            <ta e="T460" id="Seg_5481" s="T459">one</ta>
            <ta e="T461" id="Seg_5482" s="T460">NEG</ta>
            <ta e="T462" id="Seg_5483" s="T461">reindeer.[NOM]</ta>
            <ta e="T463" id="Seg_5484" s="T462">meat-3SG.[NOM]</ta>
            <ta e="T464" id="Seg_5485" s="T463">NEG.EX</ta>
            <ta e="T465" id="Seg_5486" s="T464">1SG.[NOM]</ta>
            <ta e="T466" id="Seg_5487" s="T465">reindeer.[NOM]</ta>
            <ta e="T467" id="Seg_5488" s="T466">meat-3SG-DAT/LOC</ta>
            <ta e="T468" id="Seg_5489" s="T467">learn-PST2-1SG</ta>
            <ta e="T469" id="Seg_5490" s="T468">reindeer.[NOM]</ta>
            <ta e="T470" id="Seg_5491" s="T469">meat-3SG-ACC</ta>
            <ta e="T471" id="Seg_5492" s="T470">eat-PTCP.FUT-1SG-ACC</ta>
            <ta e="T472" id="Seg_5493" s="T471">want-PRS-1SG</ta>
            <ta e="T473" id="Seg_5494" s="T472">this</ta>
            <ta e="T474" id="Seg_5495" s="T473">ice.[NOM]</ta>
            <ta e="T475" id="Seg_5496" s="T474">chest.[NOM]</ta>
            <ta e="T476" id="Seg_5497" s="T475">inside-3SG-DAT/LOC</ta>
            <ta e="T477" id="Seg_5498" s="T476">reindeer.[NOM]</ta>
            <ta e="T478" id="Seg_5499" s="T477">many</ta>
            <ta e="T479" id="Seg_5500" s="T478">fish.[NOM]</ta>
            <ta e="T480" id="Seg_5501" s="T479">similar</ta>
            <ta e="T481" id="Seg_5502" s="T480">swim-CVB.SIM</ta>
            <ta e="T482" id="Seg_5503" s="T481">go-PRS-3PL</ta>
            <ta e="T483" id="Seg_5504" s="T482">water.[NOM]</ta>
            <ta e="T484" id="Seg_5505" s="T483">master-3SG.[NOM]</ta>
            <ta e="T485" id="Seg_5506" s="T484">well</ta>
            <ta e="T486" id="Seg_5507" s="T485">laugh-PRS.[3SG]</ta>
            <ta e="T487" id="Seg_5508" s="T486">this</ta>
            <ta e="T488" id="Seg_5509" s="T487">ice.[NOM]</ta>
            <ta e="T489" id="Seg_5510" s="T488">chest-DAT/LOC</ta>
            <ta e="T490" id="Seg_5511" s="T489">what.[NOM]</ta>
            <ta e="T491" id="Seg_5512" s="T490">reindeer-3SG.[NOM]</ta>
            <ta e="T492" id="Seg_5513" s="T491">come-FUT.[3SG]=Q</ta>
            <ta e="T493" id="Seg_5514" s="T492">here</ta>
            <ta e="T494" id="Seg_5515" s="T493">water.[NOM]</ta>
            <ta e="T495" id="Seg_5516" s="T494">just</ta>
            <ta e="T496" id="Seg_5517" s="T495">1SG.[NOM]</ta>
            <ta e="T497" id="Seg_5518" s="T496">know-PRS-1SG</ta>
            <ta e="T498" id="Seg_5519" s="T497">EVID</ta>
            <ta e="T499" id="Seg_5520" s="T498">woman-3SG.[NOM]</ta>
            <ta e="T500" id="Seg_5521" s="T499">say-PST2.[3SG]</ta>
            <ta e="T501" id="Seg_5522" s="T500">1SG.[NOM]</ta>
            <ta e="T502" id="Seg_5523" s="T501">see-PST2-EP-1SG</ta>
            <ta e="T503" id="Seg_5524" s="T502">reindeer-PL.[NOM]</ta>
            <ta e="T504" id="Seg_5525" s="T503">swim-CVB.SIM</ta>
            <ta e="T505" id="Seg_5526" s="T504">go-PRS-3PL</ta>
            <ta e="T506" id="Seg_5527" s="T505">some-PL.[NOM]</ta>
            <ta e="T507" id="Seg_5528" s="T506">white-PL.[NOM]</ta>
            <ta e="T508" id="Seg_5529" s="T507">some-PL.[NOM]</ta>
            <ta e="T509" id="Seg_5530" s="T508">spotted-PL.[NOM]</ta>
            <ta e="T510" id="Seg_5531" s="T509">other.of.two-3PL.[NOM]</ta>
            <ta e="T511" id="Seg_5532" s="T510">black-PL.[NOM]</ta>
            <ta e="T512" id="Seg_5533" s="T511">intact</ta>
            <ta e="T513" id="Seg_5534" s="T512">herd.[NOM]</ta>
            <ta e="T514" id="Seg_5535" s="T513">stupid</ta>
            <ta e="T515" id="Seg_5536" s="T514">woman-2SG</ta>
            <ta e="T516" id="Seg_5537" s="T515">be-PST2.[3SG]</ta>
            <ta e="T517" id="Seg_5538" s="T516">get.angry-PST1-3SG</ta>
            <ta e="T518" id="Seg_5539" s="T517">water.[NOM]</ta>
            <ta e="T519" id="Seg_5540" s="T518">master-3SG.[NOM]</ta>
            <ta e="T520" id="Seg_5541" s="T519">what.[NOM]</ta>
            <ta e="T521" id="Seg_5542" s="T520">reindeer-3SG.[NOM]</ta>
            <ta e="T522" id="Seg_5543" s="T521">water.[NOM]</ta>
            <ta e="T523" id="Seg_5544" s="T522">inside-3SG-DAT/LOC</ta>
            <ta e="T524" id="Seg_5545" s="T523">go-FUT.[3SG]=Q</ta>
            <ta e="T525" id="Seg_5546" s="T524">self-1SG.[NOM]</ta>
            <ta e="T526" id="Seg_5547" s="T525">know-PRS-2SG</ta>
            <ta e="T527" id="Seg_5548" s="T526">1PL.[NOM]</ta>
            <ta e="T528" id="Seg_5549" s="T527">water.[NOM]</ta>
            <ta e="T529" id="Seg_5550" s="T528">inside-3SG-DAT/LOC</ta>
            <ta e="T530" id="Seg_5551" s="T529">live-PRS-1PL</ta>
            <ta e="T531" id="Seg_5552" s="T530">that.[NOM]</ta>
            <ta e="T532" id="Seg_5553" s="T531">because.of</ta>
            <ta e="T533" id="Seg_5554" s="T532">reindeer-PL.[NOM]</ta>
            <ta e="T534" id="Seg_5555" s="T533">also</ta>
            <ta e="T535" id="Seg_5556" s="T534">water.[NOM]</ta>
            <ta e="T536" id="Seg_5557" s="T535">inside-3SG-DAT/LOC</ta>
            <ta e="T537" id="Seg_5558" s="T536">go-PRS-3PL</ta>
            <ta e="T538" id="Seg_5559" s="T537">say-PST2.[3SG]</ta>
            <ta e="T539" id="Seg_5560" s="T538">woman-3SG.[NOM]</ta>
            <ta e="T540" id="Seg_5561" s="T539">see.[IMP.2SG]</ta>
            <ta e="T541" id="Seg_5562" s="T540">well</ta>
            <ta e="T542" id="Seg_5563" s="T541">chest.[NOM]</ta>
            <ta e="T543" id="Seg_5564" s="T542">inside-3SG-ACC</ta>
            <ta e="T544" id="Seg_5565" s="T543">water.[NOM]</ta>
            <ta e="T545" id="Seg_5566" s="T544">master-3SG.[NOM]</ta>
            <ta e="T546" id="Seg_5567" s="T545">get.angry-CVB.SEQ</ta>
            <ta e="T547" id="Seg_5568" s="T546">after</ta>
            <ta e="T548" id="Seg_5569" s="T547">forget-CVB.SEQ</ta>
            <ta e="T549" id="Seg_5570" s="T548">throw-PST2.[3SG]</ta>
            <ta e="T550" id="Seg_5571" s="T549">chest-3SG-DAT/LOC</ta>
            <ta e="T551" id="Seg_5572" s="T550">look.into-PST2.[3SG]</ta>
            <ta e="T552" id="Seg_5573" s="T551">reindeer-PL-ACC</ta>
            <ta e="T553" id="Seg_5574" s="T552">see-CVB.PURP</ta>
            <ta e="T554" id="Seg_5575" s="T553">woman-3SG.[NOM]</ta>
            <ta e="T555" id="Seg_5576" s="T554">come-CVB.SEQ</ta>
            <ta e="T556" id="Seg_5577" s="T555">head-3SG-ACC</ta>
            <ta e="T557" id="Seg_5578" s="T556">water.[NOM]</ta>
            <ta e="T558" id="Seg_5579" s="T557">inside-3SG-DAT/LOC</ta>
            <ta e="T559" id="Seg_5580" s="T558">destroy-ADVZ</ta>
            <ta e="T560" id="Seg_5581" s="T559">pull.down-PST2.[3SG]</ta>
            <ta e="T561" id="Seg_5582" s="T560">water.[NOM]</ta>
            <ta e="T562" id="Seg_5583" s="T561">master-3SG-GEN</ta>
            <ta e="T563" id="Seg_5584" s="T562">head-3SG.[NOM]</ta>
            <ta e="T564" id="Seg_5585" s="T563">completely</ta>
            <ta e="T565" id="Seg_5586" s="T564">go-PST2.[3SG]</ta>
            <ta e="T566" id="Seg_5587" s="T565">immediately</ta>
            <ta e="T567" id="Seg_5588" s="T566">die-PST2.[3SG]</ta>
            <ta e="T568" id="Seg_5589" s="T567">beads.[NOM]</ta>
            <ta e="T569" id="Seg_5590" s="T568">beard.[NOM]</ta>
            <ta e="T570" id="Seg_5591" s="T569">little</ta>
            <ta e="T571" id="Seg_5592" s="T570">daughter-3SG.[NOM]</ta>
            <ta e="T572" id="Seg_5593" s="T571">door-ABL</ta>
            <ta e="T573" id="Seg_5594" s="T572">door-ACC</ta>
            <ta e="T574" id="Seg_5595" s="T573">go.around-CVB.SIM</ta>
            <ta e="T575" id="Seg_5596" s="T574">go-CVB.SEQ</ta>
            <ta e="T576" id="Seg_5597" s="T575">older.sister-PL-3SG-ACC</ta>
            <ta e="T577" id="Seg_5598" s="T576">find-PST2.[3SG]</ta>
            <ta e="T578" id="Seg_5599" s="T577">door-3PL-GEN</ta>
            <ta e="T579" id="Seg_5600" s="T578">glue-3PL-ACC</ta>
            <ta e="T580" id="Seg_5601" s="T579">knife-EP-INSTR</ta>
            <ta e="T581" id="Seg_5602" s="T580">scrape-CVB.SEQ</ta>
            <ta e="T582" id="Seg_5603" s="T581">take-CVB.SEQ</ta>
            <ta e="T583" id="Seg_5604" s="T582">free-PST2.[3SG]</ta>
            <ta e="T584" id="Seg_5605" s="T583">three-COLL-3PL.[NOM]</ta>
            <ta e="T585" id="Seg_5606" s="T584">pole.[NOM]</ta>
            <ta e="T586" id="Seg_5607" s="T585">tent-3PL-DAT/LOC</ta>
            <ta e="T587" id="Seg_5608" s="T586">come-PST2-3PL</ta>
            <ta e="T588" id="Seg_5609" s="T587">beads.[NOM]</ta>
            <ta e="T589" id="Seg_5610" s="T588">beard.[NOM]</ta>
            <ta e="T590" id="Seg_5611" s="T589">be.happy-CVB.SEQ</ta>
            <ta e="T591" id="Seg_5612" s="T590">daughter-PL-3SG-DAT/LOC</ta>
            <ta e="T592" id="Seg_5613" s="T591">slice-PTCP.PST.[NOM]</ta>
            <ta e="T593" id="Seg_5614" s="T592">slice-PRS.[3SG]</ta>
            <ta e="T594" id="Seg_5615" s="T593">boil-PTCP.PST</ta>
            <ta e="T595" id="Seg_5616" s="T594">fish-ACC</ta>
            <ta e="T596" id="Seg_5617" s="T595">eat-EP-CAUS-PRS.[3SG]</ta>
            <ta e="T597" id="Seg_5618" s="T596">big</ta>
            <ta e="T598" id="Seg_5619" s="T597">daughter-3SG.[NOM]</ta>
            <ta e="T599" id="Seg_5620" s="T598">lord.[NOM]</ta>
            <ta e="T600" id="Seg_5621" s="T599">finger-3SG-ACC</ta>
            <ta e="T601" id="Seg_5622" s="T600">see-CVB.SEQ</ta>
            <ta e="T602" id="Seg_5623" s="T601">that-DAT/LOC</ta>
            <ta e="T603" id="Seg_5624" s="T602">middle</ta>
            <ta e="T604" id="Seg_5625" s="T603">daughter-3SG.[NOM]</ta>
            <ta e="T605" id="Seg_5626" s="T604">middle-ADJZ</ta>
            <ta e="T606" id="Seg_5627" s="T605">finger-3SG-ACC</ta>
            <ta e="T607" id="Seg_5628" s="T606">see-CVB.SEQ</ta>
            <ta e="T608" id="Seg_5629" s="T607">then</ta>
            <ta e="T609" id="Seg_5630" s="T608">just</ta>
            <ta e="T610" id="Seg_5631" s="T609">water.[NOM]</ta>
            <ta e="T611" id="Seg_5632" s="T610">master-3SG-ACC</ta>
            <ta e="T612" id="Seg_5633" s="T611">remember-PRS-3PL</ta>
            <ta e="T613" id="Seg_5634" s="T612">tale-VBZ-PST2-3SG</ta>
            <ta e="T614" id="Seg_5635" s="T613">Ogdo</ta>
            <ta e="T615" id="Seg_5636" s="T614">Aksyonova.[NOM]</ta>
         </annotation>
         <annotation name="gg" tierref="gg">
            <ta e="T1" id="Seg_5637" s="T0">zuhören-IMP.2PL</ta>
            <ta e="T2" id="Seg_5638" s="T1">Märchen-ACC</ta>
            <ta e="T3" id="Seg_5639" s="T2">Name-3SG.[NOM]</ta>
            <ta e="T4" id="Seg_5640" s="T3">Perlen.[NOM]</ta>
            <ta e="T5" id="Seg_5641" s="T4">Bart.[NOM]</ta>
            <ta e="T6" id="Seg_5642" s="T5">eins</ta>
            <ta e="T7" id="Seg_5643" s="T6">alter.Mann.[NOM]</ta>
            <ta e="T8" id="Seg_5644" s="T7">es.gibt</ta>
            <ta e="T9" id="Seg_5645" s="T8">sein-PST2.[3SG]</ta>
            <ta e="T10" id="Seg_5646" s="T9">alter.Mann.[NOM]</ta>
            <ta e="T11" id="Seg_5647" s="T10">Bart-3SG.[NOM]</ta>
            <ta e="T12" id="Seg_5648" s="T11">Zwischenraum-PROPR.[NOM]</ta>
            <ta e="T13" id="Seg_5649" s="T12">sein-CVB.SEQ</ta>
            <ta e="T14" id="Seg_5650" s="T13">nachdem</ta>
            <ta e="T15" id="Seg_5651" s="T14">lang.[NOM]</ta>
            <ta e="T16" id="Seg_5652" s="T15">sehr</ta>
            <ta e="T17" id="Seg_5653" s="T16">Bart-PL-3SG.[NOM]</ta>
            <ta e="T18" id="Seg_5654" s="T17">Perlen-PROPR</ta>
            <ta e="T19" id="Seg_5655" s="T18">Faden.[NOM]</ta>
            <ta e="T20" id="Seg_5656" s="T19">ähnlich</ta>
            <ta e="T21" id="Seg_5657" s="T20">blitzen-ITER-PRS-3PL</ta>
            <ta e="T22" id="Seg_5658" s="T21">jenes.[NOM]</ta>
            <ta e="T23" id="Seg_5659" s="T22">wegen</ta>
            <ta e="T24" id="Seg_5660" s="T23">alter.Mann-ACC</ta>
            <ta e="T25" id="Seg_5661" s="T24">Name-VBZ-PST2-3PL</ta>
            <ta e="T26" id="Seg_5662" s="T25">Perlen.[NOM]</ta>
            <ta e="T27" id="Seg_5663" s="T26">Bart.[NOM]</ta>
            <ta e="T28" id="Seg_5664" s="T27">sagen-CVB.SEQ</ta>
            <ta e="T29" id="Seg_5665" s="T28">Perlen.[NOM]</ta>
            <ta e="T30" id="Seg_5666" s="T29">Bart.[NOM]</ta>
            <ta e="T31" id="Seg_5667" s="T30">drei</ta>
            <ta e="T32" id="Seg_5668" s="T31">Tochter-PROPR.[NOM]</ta>
            <ta e="T33" id="Seg_5669" s="T32">im.Winter</ta>
            <ta e="T34" id="Seg_5670" s="T33">im.Sommer</ta>
            <ta e="T35" id="Seg_5671" s="T34">Netz-VBZ-PRS.[3SG]</ta>
            <ta e="T36" id="Seg_5672" s="T35">Fisch-EP-INSTR</ta>
            <ta e="T37" id="Seg_5673" s="T36">nur</ta>
            <ta e="T38" id="Seg_5674" s="T37">füttern-EP-PASS/REFL-CVB.SEQ</ta>
            <ta e="T39" id="Seg_5675" s="T38">sitzen-PRS-3PL</ta>
            <ta e="T40" id="Seg_5676" s="T39">einmal</ta>
            <ta e="T41" id="Seg_5677" s="T40">Perlen.[NOM]</ta>
            <ta e="T42" id="Seg_5678" s="T41">Bart.[NOM]</ta>
            <ta e="T43" id="Seg_5679" s="T42">Netz.[NOM]</ta>
            <ta e="T44" id="Seg_5680" s="T43">sehen-EP-MED-CVB.SIM</ta>
            <ta e="T45" id="Seg_5681" s="T44">gehen-PST2.[3SG]</ta>
            <ta e="T46" id="Seg_5682" s="T45">Eisloch-3SG-DAT/LOC</ta>
            <ta e="T47" id="Seg_5683" s="T46">beugen-PTCP.PRS-3SG-ACC</ta>
            <ta e="T48" id="Seg_5684" s="T47">mit</ta>
            <ta e="T49" id="Seg_5685" s="T48">wer.[NOM]</ta>
            <ta e="T50" id="Seg_5686" s="T49">INDEF</ta>
            <ta e="T51" id="Seg_5687" s="T50">Bart-3SG-ABL</ta>
            <ta e="T52" id="Seg_5688" s="T51">greifen-CVB.SEQ</ta>
            <ta e="T53" id="Seg_5689" s="T52">nehmen-PST2.[3SG]</ta>
            <ta e="T54" id="Seg_5690" s="T53">völlig</ta>
            <ta e="T55" id="Seg_5691" s="T54">lassen-NEG.[3SG]</ta>
            <ta e="T56" id="Seg_5692" s="T55">wer.[NOM]</ta>
            <ta e="T57" id="Seg_5693" s="T56">sein-FUT.[3SG]=Q</ta>
            <ta e="T58" id="Seg_5694" s="T57">denken-CVB.SIM</ta>
            <ta e="T59" id="Seg_5695" s="T58">denken-PST2.[3SG]</ta>
            <ta e="T60" id="Seg_5696" s="T59">Eisloch-ABL</ta>
            <ta e="T61" id="Seg_5697" s="T60">sich.hinauslehnen-PST2.[3SG]</ta>
            <ta e="T62" id="Seg_5698" s="T61">Wasser.[NOM]</ta>
            <ta e="T63" id="Seg_5699" s="T62">Herr-3SG.[NOM]</ta>
            <ta e="T64" id="Seg_5700" s="T63">Perlen.[NOM]</ta>
            <ta e="T65" id="Seg_5701" s="T64">Bart.[NOM]</ta>
            <ta e="T66" id="Seg_5702" s="T65">sagen-PRS.[3SG]</ta>
            <ta e="T67" id="Seg_5703" s="T66">Tochter-2SG-ACC</ta>
            <ta e="T68" id="Seg_5704" s="T67">geben-PRS-2SG</ta>
            <ta e="T69" id="Seg_5705" s="T68">Q</ta>
            <ta e="T70" id="Seg_5706" s="T69">NEG</ta>
            <ta e="T71" id="Seg_5707" s="T70">Q</ta>
            <ta e="T72" id="Seg_5708" s="T71">Frau.[NOM]</ta>
            <ta e="T73" id="Seg_5709" s="T72">machen-PTCP.FUT-1SG-ACC</ta>
            <ta e="T74" id="Seg_5710" s="T73">geben-NEG-TEMP-2SG</ta>
            <ta e="T75" id="Seg_5711" s="T74">eins</ta>
            <ta e="T76" id="Seg_5712" s="T75">NEG</ta>
            <ta e="T77" id="Seg_5713" s="T76">Fisch-ACC</ta>
            <ta e="T78" id="Seg_5714" s="T77">nehmen-FUT-2SG</ta>
            <ta e="T79" id="Seg_5715" s="T78">NEG-3SG</ta>
            <ta e="T80" id="Seg_5716" s="T79">was-EP-INSTR</ta>
            <ta e="T81" id="Seg_5717" s="T80">Familie-PL-1SG-ACC</ta>
            <ta e="T82" id="Seg_5718" s="T81">füttern-FUT-1SG=Q</ta>
            <ta e="T83" id="Seg_5719" s="T82">nun</ta>
            <ta e="T84" id="Seg_5720" s="T83">denken-CVB.SIM</ta>
            <ta e="T85" id="Seg_5721" s="T84">denken-PST2.[3SG]</ta>
            <ta e="T86" id="Seg_5722" s="T85">dann</ta>
            <ta e="T87" id="Seg_5723" s="T86">zustimmen-PST2.[3SG]</ta>
            <ta e="T88" id="Seg_5724" s="T87">Betrug-VBZ-TEMP-2SG</ta>
            <ta e="T89" id="Seg_5725" s="T88">doch</ta>
            <ta e="T90" id="Seg_5726" s="T89">einerlei</ta>
            <ta e="T91" id="Seg_5727" s="T90">finden-FUT-1SG</ta>
            <ta e="T92" id="Seg_5728" s="T91">sagen-PST2.[3SG]</ta>
            <ta e="T93" id="Seg_5729" s="T92">Wasser.[NOM]</ta>
            <ta e="T94" id="Seg_5730" s="T93">Herr-3SG.[NOM]</ta>
            <ta e="T95" id="Seg_5731" s="T94">alter.Mann.[NOM]</ta>
            <ta e="T96" id="Seg_5732" s="T95">Fäustling-3SG-ACC</ta>
            <ta e="T97" id="Seg_5733" s="T96">greifen-CVB.SEQ</ta>
            <ta e="T98" id="Seg_5734" s="T97">nehmen-CVB.SEQ</ta>
            <ta e="T99" id="Seg_5735" s="T98">nachdem</ta>
            <ta e="T100" id="Seg_5736" s="T99">Eis.[NOM]</ta>
            <ta e="T101" id="Seg_5737" s="T100">Inneres-3SG-INSTR</ta>
            <ta e="T102" id="Seg_5738" s="T101">gehen-CVB.SEQ</ta>
            <ta e="T103" id="Seg_5739" s="T102">bleiben-PST2.[3SG]</ta>
            <ta e="T104" id="Seg_5740" s="T103">Perlen.[NOM]</ta>
            <ta e="T105" id="Seg_5741" s="T104">Bart.[NOM]</ta>
            <ta e="T106" id="Seg_5742" s="T105">sehr</ta>
            <ta e="T107" id="Seg_5743" s="T106">verwirrt.sein-PST2.[3SG]</ta>
            <ta e="T108" id="Seg_5744" s="T107">Stange.[NOM]</ta>
            <ta e="T109" id="Seg_5745" s="T108">Zelt-3SG-DAT/LOC</ta>
            <ta e="T110" id="Seg_5746" s="T109">kommen-CVB.SEQ</ta>
            <ta e="T111" id="Seg_5747" s="T110">Tochter-PL-3SG-DAT/LOC</ta>
            <ta e="T112" id="Seg_5748" s="T111">erzählen-PST2.[3SG]</ta>
            <ta e="T113" id="Seg_5749" s="T112">groß</ta>
            <ta e="T114" id="Seg_5750" s="T113">Tochter-3SG.[NOM]</ta>
            <ta e="T115" id="Seg_5751" s="T114">sich.freuen-PST2.[3SG]</ta>
            <ta e="T116" id="Seg_5752" s="T115">1SG.[NOM]</ta>
            <ta e="T117" id="Seg_5753" s="T116">Ehemann-DAT/LOC</ta>
            <ta e="T118" id="Seg_5754" s="T117">gehen-FUT-1SG</ta>
            <ta e="T119" id="Seg_5755" s="T118">Wasser.[NOM]</ta>
            <ta e="T120" id="Seg_5756" s="T119">Herr-3SG-DAT/LOC</ta>
            <ta e="T121" id="Seg_5757" s="T120">Welt.[NOM]</ta>
            <ta e="T122" id="Seg_5758" s="T121">oberer.Teil-3SG-INSTR</ta>
            <ta e="T123" id="Seg_5759" s="T122">meist</ta>
            <ta e="T124" id="Seg_5760" s="T123">reich.[NOM]</ta>
            <ta e="T125" id="Seg_5761" s="T124">sein-FUT-1SG</ta>
            <ta e="T126" id="Seg_5762" s="T125">Perlen.[NOM]</ta>
            <ta e="T127" id="Seg_5763" s="T126">Bart.[NOM]</ta>
            <ta e="T128" id="Seg_5764" s="T127">Tochter-3SG-ACC</ta>
            <ta e="T129" id="Seg_5765" s="T128">Ehemann-DAT/LOC</ta>
            <ta e="T130" id="Seg_5766" s="T129">geben-CVB.SEQ</ta>
            <ta e="T131" id="Seg_5767" s="T130">werfen-PST2.[3SG]</ta>
            <ta e="T132" id="Seg_5768" s="T131">Wasser.[NOM]</ta>
            <ta e="T133" id="Seg_5769" s="T132">Herr-3SG.[NOM]</ta>
            <ta e="T134" id="Seg_5770" s="T133">reich.[NOM]</ta>
            <ta e="T135" id="Seg_5771" s="T134">und</ta>
            <ta e="T136" id="Seg_5772" s="T135">reich.[NOM]</ta>
            <ta e="T137" id="Seg_5773" s="T136">Reichtum-3SG-GEN</ta>
            <ta e="T138" id="Seg_5774" s="T137">Ende-3SG.[NOM]</ta>
            <ta e="T139" id="Seg_5775" s="T138">zu.sehen.sein-EP-NEG.[3SG]</ta>
            <ta e="T140" id="Seg_5776" s="T139">wie.viel</ta>
            <ta e="T141" id="Seg_5777" s="T140">INDEF</ta>
            <ta e="T142" id="Seg_5778" s="T141">Zimmer-PROPR.[NOM]</ta>
            <ta e="T143" id="Seg_5779" s="T142">Haus-PROPR.[NOM]</ta>
            <ta e="T144" id="Seg_5780" s="T143">eins</ta>
            <ta e="T145" id="Seg_5781" s="T144">Zimmer-DAT/LOC</ta>
            <ta e="T146" id="Seg_5782" s="T145">groß</ta>
            <ta e="T147" id="Seg_5783" s="T146">Eis.[NOM]</ta>
            <ta e="T148" id="Seg_5784" s="T147">Truhe.[NOM]</ta>
            <ta e="T149" id="Seg_5785" s="T148">stehen-PRS.[3SG]</ta>
            <ta e="T150" id="Seg_5786" s="T149">Wasser.[NOM]</ta>
            <ta e="T151" id="Seg_5787" s="T150">Herr-3SG.[NOM]</ta>
            <ta e="T152" id="Seg_5788" s="T151">Frau-3SG-ACC</ta>
            <ta e="T153" id="Seg_5789" s="T152">holen-CVB.SEQ</ta>
            <ta e="T154" id="Seg_5790" s="T153">Eis.[NOM]</ta>
            <ta e="T155" id="Seg_5791" s="T154">Truhe-ACC</ta>
            <ta e="T156" id="Seg_5792" s="T155">zeigen-PST2.[3SG]</ta>
            <ta e="T157" id="Seg_5793" s="T156">wo</ta>
            <ta e="T158" id="Seg_5794" s="T157">denken-PTCP.PRS</ta>
            <ta e="T159" id="Seg_5795" s="T158">gehen.[IMP.2SG]</ta>
            <ta e="T160" id="Seg_5796" s="T159">sehen.[IMP.2SG]</ta>
            <ta e="T161" id="Seg_5797" s="T160">dieses.[NOM]</ta>
            <ta e="T162" id="Seg_5798" s="T161">nur</ta>
            <ta e="T164" id="Seg_5799" s="T163">Truhe.[NOM]</ta>
            <ta e="T165" id="Seg_5800" s="T164">Inneres-3SG-DAT/LOC</ta>
            <ta e="T166" id="Seg_5801" s="T165">stecken-EP-NEG.[IMP.2SG]</ta>
            <ta e="T167" id="Seg_5802" s="T166">Finger-2SG-ACC</ta>
            <ta e="T168" id="Seg_5803" s="T167">sagen-PST2.[3SG]</ta>
            <ta e="T169" id="Seg_5804" s="T168">Ehemann-3SG.[NOM]</ta>
            <ta e="T170" id="Seg_5805" s="T169">gehen-PTCP.PST-3SG-ACC</ta>
            <ta e="T171" id="Seg_5806" s="T170">nachdem</ta>
            <ta e="T172" id="Seg_5807" s="T171">dieses</ta>
            <ta e="T173" id="Seg_5808" s="T172">Frau.[NOM]</ta>
            <ta e="T174" id="Seg_5809" s="T173">aushalten-NEG.CVB.SIM</ta>
            <ta e="T175" id="Seg_5810" s="T174">Finger-3SG-ACC</ta>
            <ta e="T176" id="Seg_5811" s="T175">stecken-PST2.[3SG]</ta>
            <ta e="T177" id="Seg_5812" s="T176">Wasser.[NOM]</ta>
            <ta e="T178" id="Seg_5813" s="T177">Eis.[NOM]</ta>
            <ta e="T179" id="Seg_5814" s="T178">werden-PST2.[3SG]</ta>
            <ta e="T180" id="Seg_5815" s="T179">kaum</ta>
            <ta e="T181" id="Seg_5816" s="T180">PFV</ta>
            <ta e="T182" id="Seg_5817" s="T181">ziehen-PST2.[3SG]</ta>
            <ta e="T183" id="Seg_5818" s="T182">Finger-3SG-ACC</ta>
            <ta e="T184" id="Seg_5819" s="T183">sehen-PST2-3SG</ta>
            <ta e="T185" id="Seg_5820" s="T184">Herr.[NOM]</ta>
            <ta e="T186" id="Seg_5821" s="T185">Finger-3SG.[NOM]</ta>
            <ta e="T187" id="Seg_5822" s="T186">völlig</ta>
            <ta e="T188" id="Seg_5823" s="T187">gehen-PST2.[3SG]</ta>
            <ta e="T189" id="Seg_5824" s="T188">jenes-3SG-3SG-ACC</ta>
            <ta e="T190" id="Seg_5825" s="T189">heftig</ta>
            <ta e="T191" id="Seg_5826" s="T190">binden-CVB.SEQ</ta>
            <ta e="T192" id="Seg_5827" s="T191">nachdem</ta>
            <ta e="T193" id="Seg_5828" s="T192">sitzen-PRS.[3SG]</ta>
            <ta e="T194" id="Seg_5829" s="T193">Angst.haben-CVB.SEQ</ta>
            <ta e="T195" id="Seg_5830" s="T194">Auge-PL-3SG.[NOM]</ta>
            <ta e="T196" id="Seg_5831" s="T195">herauskommen-ADVZ</ta>
            <ta e="T197" id="Seg_5832" s="T196">gehen-PST2-3PL</ta>
            <ta e="T198" id="Seg_5833" s="T197">Wasser.[NOM]</ta>
            <ta e="T199" id="Seg_5834" s="T198">Herr-3SG.[NOM]</ta>
            <ta e="T200" id="Seg_5835" s="T199">kommen-CVB.SEQ</ta>
            <ta e="T201" id="Seg_5836" s="T200">sofort</ta>
            <ta e="T202" id="Seg_5837" s="T201">erraten-PST1-3SG</ta>
            <ta e="T203" id="Seg_5838" s="T202">1SG-DAT/LOC</ta>
            <ta e="T204" id="Seg_5839" s="T203">2SG.[NOM]</ta>
            <ta e="T205" id="Seg_5840" s="T204">Frau.[NOM]</ta>
            <ta e="T206" id="Seg_5841" s="T205">sein-NEG.PTCP.[NOM]</ta>
            <ta e="T207" id="Seg_5842" s="T206">wie-2SG</ta>
            <ta e="T208" id="Seg_5843" s="T207">sagen-PST2.[3SG]</ta>
            <ta e="T209" id="Seg_5844" s="T208">Frau-ACC</ta>
            <ta e="T210" id="Seg_5845" s="T209">meist</ta>
            <ta e="T211" id="Seg_5846" s="T210">fern-ADJZ</ta>
            <ta e="T212" id="Seg_5847" s="T211">Haus-DAT/LOC</ta>
            <ta e="T213" id="Seg_5848" s="T212">Zimmer-3SG-DAT/LOC</ta>
            <ta e="T214" id="Seg_5849" s="T213">mitnehmen-CVB.SEQ</ta>
            <ta e="T215" id="Seg_5850" s="T214">verriegeln-CVB.SEQ</ta>
            <ta e="T216" id="Seg_5851" s="T215">werfen-PST2.[3SG]</ta>
            <ta e="T217" id="Seg_5852" s="T216">Tür-3SG-ACC</ta>
            <ta e="T218" id="Seg_5853" s="T217">eng</ta>
            <ta e="T219" id="Seg_5854" s="T218">Stör.[NOM]</ta>
            <ta e="T220" id="Seg_5855" s="T219">Magen-3SG-INSTR</ta>
            <ta e="T221" id="Seg_5856" s="T220">gemacht.werden-EP-PTCP.PST</ta>
            <ta e="T222" id="Seg_5857" s="T221">Klebe-EP-INSTR</ta>
            <ta e="T223" id="Seg_5858" s="T222">heftig</ta>
            <ta e="T224" id="Seg_5859" s="T223">vollschmieren-PST2.[3SG]</ta>
            <ta e="T225" id="Seg_5860" s="T224">Perlen.[NOM]</ta>
            <ta e="T226" id="Seg_5861" s="T225">Bart.[NOM]</ta>
            <ta e="T227" id="Seg_5862" s="T226">wieder</ta>
            <ta e="T228" id="Seg_5863" s="T227">Netz-VBZ-CVB.SIM</ta>
            <ta e="T229" id="Seg_5864" s="T228">kommen-PST2.[3SG]</ta>
            <ta e="T230" id="Seg_5865" s="T229">Eisloch-3SG-DAT/LOC</ta>
            <ta e="T231" id="Seg_5866" s="T230">beugen-PTCP.PRS-3SG-ACC</ta>
            <ta e="T232" id="Seg_5867" s="T231">mit</ta>
            <ta e="T233" id="Seg_5868" s="T232">Wasser.[NOM]</ta>
            <ta e="T234" id="Seg_5869" s="T233">Herr-3SG.[NOM]</ta>
            <ta e="T235" id="Seg_5870" s="T234">wieder</ta>
            <ta e="T236" id="Seg_5871" s="T235">Bart-3SG-ABL</ta>
            <ta e="T237" id="Seg_5872" s="T236">greifen-CVB.SEQ</ta>
            <ta e="T238" id="Seg_5873" s="T237">nehmen-PST2.[3SG]</ta>
            <ta e="T239" id="Seg_5874" s="T238">holen.[IMP.2SG]</ta>
            <ta e="T240" id="Seg_5875" s="T239">zwei-ORD</ta>
            <ta e="T241" id="Seg_5876" s="T240">Tochter-2SG-ACC</ta>
            <ta e="T242" id="Seg_5877" s="T241">Frau.[NOM]</ta>
            <ta e="T243" id="Seg_5878" s="T242">machen-PTCP.FUT-1SG-ACC</ta>
            <ta e="T244" id="Seg_5879" s="T243">alter.Mann.[NOM]</ta>
            <ta e="T245" id="Seg_5880" s="T244">Unglück-ACC</ta>
            <ta e="T246" id="Seg_5881" s="T245">zu.Ende.gehen-FUT.[3SG]</ta>
            <ta e="T247" id="Seg_5882" s="T246">Q</ta>
            <ta e="T248" id="Seg_5883" s="T247">mittlerer</ta>
            <ta e="T249" id="Seg_5884" s="T248">Tochter-3SG-ACC</ta>
            <ta e="T250" id="Seg_5885" s="T249">geben-PST2.[3SG]</ta>
            <ta e="T251" id="Seg_5886" s="T250">dieses</ta>
            <ta e="T252" id="Seg_5887" s="T251">Frau.[NOM]</ta>
            <ta e="T253" id="Seg_5888" s="T252">Reichtum.[NOM]</ta>
            <ta e="T254" id="Seg_5889" s="T253">sehen-EP-MED-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T255" id="Seg_5890" s="T254">Freizeit-POSS</ta>
            <ta e="T256" id="Seg_5891" s="T255">NEG.[3SG]</ta>
            <ta e="T257" id="Seg_5892" s="T256">sich.wundern-CVB.SEQ</ta>
            <ta e="T258" id="Seg_5893" s="T257">nachdem</ta>
            <ta e="T259" id="Seg_5894" s="T258">aufhören-NEG.[3SG]</ta>
            <ta e="T260" id="Seg_5895" s="T259">einmal</ta>
            <ta e="T261" id="Seg_5896" s="T260">dieses</ta>
            <ta e="T262" id="Seg_5897" s="T261">Eis.[NOM]</ta>
            <ta e="T263" id="Seg_5898" s="T262">Truhe-DAT/LOC</ta>
            <ta e="T264" id="Seg_5899" s="T263">bringen-PST2.[3SG]</ta>
            <ta e="T265" id="Seg_5900" s="T264">Wasser.[NOM]</ta>
            <ta e="T266" id="Seg_5901" s="T265">Herr-3SG.[NOM]</ta>
            <ta e="T267" id="Seg_5902" s="T266">bringen-CVB.SEQ</ta>
            <ta e="T268" id="Seg_5903" s="T267">warnen-PST2.[3SG]</ta>
            <ta e="T269" id="Seg_5904" s="T268">dieses</ta>
            <ta e="T270" id="Seg_5905" s="T269">Truhe-DAT/LOC</ta>
            <ta e="T271" id="Seg_5906" s="T270">Hand-2SG-ACC</ta>
            <ta e="T272" id="Seg_5907" s="T271">NEG</ta>
            <ta e="T273" id="Seg_5908" s="T272">Finger-2SG-ACC</ta>
            <ta e="T274" id="Seg_5909" s="T273">NEG</ta>
            <ta e="T275" id="Seg_5910" s="T274">stecken-APPR-2SG</ta>
            <ta e="T276" id="Seg_5911" s="T275">Wasser.[NOM]</ta>
            <ta e="T277" id="Seg_5912" s="T276">Herr-3SG.[NOM]</ta>
            <ta e="T278" id="Seg_5913" s="T277">Schlitten-VBZ-CVB.SIM</ta>
            <ta e="T279" id="Seg_5914" s="T278">gehen-PTCP.PST-3SG-ACC</ta>
            <ta e="T280" id="Seg_5915" s="T279">nachdem</ta>
            <ta e="T281" id="Seg_5916" s="T280">Frau-3SG.[NOM]</ta>
            <ta e="T282" id="Seg_5917" s="T281">Eis.[NOM]</ta>
            <ta e="T283" id="Seg_5918" s="T282">Truhe-DAT/LOC</ta>
            <ta e="T284" id="Seg_5919" s="T283">kommen-CVB.SEQ</ta>
            <ta e="T285" id="Seg_5920" s="T284">Finger-3SG-ACC</ta>
            <ta e="T286" id="Seg_5921" s="T285">stecken-PST2.[3SG]</ta>
            <ta e="T287" id="Seg_5922" s="T286">Mitte-ADJZ</ta>
            <ta e="T288" id="Seg_5923" s="T287">Finger-3SG.[NOM]</ta>
            <ta e="T289" id="Seg_5924" s="T288">völlig</ta>
            <ta e="T290" id="Seg_5925" s="T289">frieren-PST2.[3SG]</ta>
            <ta e="T291" id="Seg_5926" s="T290">Finger-3SG-ACC</ta>
            <ta e="T292" id="Seg_5927" s="T291">heftig</ta>
            <ta e="T293" id="Seg_5928" s="T292">anbinden-PST2.[3SG]</ta>
            <ta e="T294" id="Seg_5929" s="T293">Wasser.[NOM]</ta>
            <ta e="T295" id="Seg_5930" s="T294">Herr-3SG.[NOM]</ta>
            <ta e="T296" id="Seg_5931" s="T295">kommen-CVB.SEQ</ta>
            <ta e="T297" id="Seg_5932" s="T296">sofort</ta>
            <ta e="T298" id="Seg_5933" s="T297">erraten-PST2.[3SG]</ta>
            <ta e="T299" id="Seg_5934" s="T298">1SG-DAT/LOC</ta>
            <ta e="T300" id="Seg_5935" s="T299">gehorsam-POSS</ta>
            <ta e="T301" id="Seg_5936" s="T300">NEG</ta>
            <ta e="T302" id="Seg_5937" s="T301">Frau.[NOM]</ta>
            <ta e="T303" id="Seg_5938" s="T302">Bedarf-POSS</ta>
            <ta e="T304" id="Seg_5939" s="T303">NEG.[3SG]</ta>
            <ta e="T305" id="Seg_5940" s="T304">sagen-CVB.SEQ</ta>
            <ta e="T306" id="Seg_5941" s="T305">nachdem</ta>
            <ta e="T307" id="Seg_5942" s="T306">fern-ADJZ</ta>
            <ta e="T308" id="Seg_5943" s="T307">Haus.[NOM]</ta>
            <ta e="T309" id="Seg_5944" s="T308">Zimmer-3SG-DAT/LOC</ta>
            <ta e="T310" id="Seg_5945" s="T309">versperren-CVB.SEQ</ta>
            <ta e="T311" id="Seg_5946" s="T310">werfen-PST2.[3SG]</ta>
            <ta e="T312" id="Seg_5947" s="T311">Tür-3SG-ACC</ta>
            <ta e="T313" id="Seg_5948" s="T312">eng</ta>
            <ta e="T314" id="Seg_5949" s="T313">Stör.[NOM]</ta>
            <ta e="T315" id="Seg_5950" s="T314">Magen-3SG-INSTR</ta>
            <ta e="T316" id="Seg_5951" s="T315">gemacht.werden-EP-PTCP.PST</ta>
            <ta e="T317" id="Seg_5952" s="T316">Klebe-EP-INSTR</ta>
            <ta e="T318" id="Seg_5953" s="T317">heftig</ta>
            <ta e="T319" id="Seg_5954" s="T318">vollschmieren-PST2.[3SG]</ta>
            <ta e="T320" id="Seg_5955" s="T319">Perlen.[NOM]</ta>
            <ta e="T321" id="Seg_5956" s="T320">Bart.[NOM]</ta>
            <ta e="T322" id="Seg_5957" s="T321">wieder</ta>
            <ta e="T323" id="Seg_5958" s="T322">Netz.[NOM]</ta>
            <ta e="T324" id="Seg_5959" s="T323">sehen-EP-MED-CVB.SIM</ta>
            <ta e="T325" id="Seg_5960" s="T324">kommen-PST2.[3SG]</ta>
            <ta e="T326" id="Seg_5961" s="T325">Eisloch-3SG-DAT/LOC</ta>
            <ta e="T327" id="Seg_5962" s="T326">beugen-PTCP.PRS-3SG-ACC</ta>
            <ta e="T328" id="Seg_5963" s="T327">mit</ta>
            <ta e="T329" id="Seg_5964" s="T328">Wasser.[NOM]</ta>
            <ta e="T330" id="Seg_5965" s="T329">Herr-3SG.[NOM]</ta>
            <ta e="T331" id="Seg_5966" s="T330">wieder</ta>
            <ta e="T332" id="Seg_5967" s="T331">Bart-3SG-ABL</ta>
            <ta e="T333" id="Seg_5968" s="T332">greifen-CVB.SEQ</ta>
            <ta e="T334" id="Seg_5969" s="T333">nehmen-PST2.[3SG]</ta>
            <ta e="T335" id="Seg_5970" s="T334">bringen.[IMP.2SG]</ta>
            <ta e="T336" id="Seg_5971" s="T335">klein</ta>
            <ta e="T337" id="Seg_5972" s="T336">Tochter-2SG-ACC</ta>
            <ta e="T338" id="Seg_5973" s="T337">Frau.[NOM]</ta>
            <ta e="T339" id="Seg_5974" s="T338">machen-PTCP.FUT-1SG-ACC</ta>
            <ta e="T340" id="Seg_5975" s="T339">sagen-PST2.[3SG]</ta>
            <ta e="T341" id="Seg_5976" s="T340">Unglück-ACC</ta>
            <ta e="T342" id="Seg_5977" s="T341">zu.Ende.gehen-FUT.[3SG]</ta>
            <ta e="T343" id="Seg_5978" s="T342">Q</ta>
            <ta e="T344" id="Seg_5979" s="T343">klein</ta>
            <ta e="T345" id="Seg_5980" s="T344">Tochter-3SG-ACC</ta>
            <ta e="T346" id="Seg_5981" s="T345">bringen-PST2.[3SG]</ta>
            <ta e="T347" id="Seg_5982" s="T346">gehen-CVB.SEQ</ta>
            <ta e="T348" id="Seg_5983" s="T347">gehen-CVB.SEQ-3PL</ta>
            <ta e="T349" id="Seg_5984" s="T348">Tochter-3SG.[NOM]</ta>
            <ta e="T350" id="Seg_5985" s="T349">sagen-PST2.[3SG]</ta>
            <ta e="T351" id="Seg_5986" s="T350">Angst.haben-EP-NEG.[IMP.2SG]</ta>
            <ta e="T352" id="Seg_5987" s="T351">Vater.[NOM]</ta>
            <ta e="T353" id="Seg_5988" s="T352">1SG.[NOM]</ta>
            <ta e="T354" id="Seg_5989" s="T353">sofort</ta>
            <ta e="T355" id="Seg_5990" s="T354">kommen-FUT-1SG</ta>
            <ta e="T356" id="Seg_5991" s="T355">ältere.Schwester-PL-1SG-ACC</ta>
            <ta e="T357" id="Seg_5992" s="T356">auch</ta>
            <ta e="T358" id="Seg_5993" s="T357">bringen-FUT-1SG</ta>
            <ta e="T359" id="Seg_5994" s="T358">Wasser.[NOM]</ta>
            <ta e="T360" id="Seg_5995" s="T359">Herr-3SG.[NOM]</ta>
            <ta e="T361" id="Seg_5996" s="T360">1SG-COMP</ta>
            <ta e="T362" id="Seg_5997" s="T361">Schläue-POSS</ta>
            <ta e="T363" id="Seg_5998" s="T362">NEG</ta>
            <ta e="T364" id="Seg_5999" s="T363">sein-FUT.[3SG]</ta>
            <ta e="T365" id="Seg_6000" s="T364">wohl</ta>
            <ta e="T366" id="Seg_6001" s="T365">Wasser.[NOM]</ta>
            <ta e="T367" id="Seg_6002" s="T366">Herr-3SG.[NOM]</ta>
            <ta e="T368" id="Seg_6003" s="T367">drei-ORD</ta>
            <ta e="T369" id="Seg_6004" s="T368">Frau-3SG-ACC</ta>
            <ta e="T370" id="Seg_6005" s="T369">Eis.[NOM]</ta>
            <ta e="T371" id="Seg_6006" s="T370">Truhe-DAT/LOC</ta>
            <ta e="T372" id="Seg_6007" s="T371">bringen-CVB.SEQ</ta>
            <ta e="T373" id="Seg_6008" s="T372">nachdem</ta>
            <ta e="T374" id="Seg_6009" s="T373">sagen-PST2.[3SG]</ta>
            <ta e="T375" id="Seg_6010" s="T374">dieses</ta>
            <ta e="T376" id="Seg_6011" s="T375">Truhe-DAT/LOC</ta>
            <ta e="T377" id="Seg_6012" s="T376">Hand-2SG-ACC</ta>
            <ta e="T378" id="Seg_6013" s="T377">stecken-EP-NEG.[IMP.2SG]</ta>
            <ta e="T379" id="Seg_6014" s="T378">zurückkommen-CVB.SEQ</ta>
            <ta e="T380" id="Seg_6015" s="T379">kommen-TEMP-1SG</ta>
            <ta e="T381" id="Seg_6016" s="T380">sehen-FUT-1SG</ta>
            <ta e="T382" id="Seg_6017" s="T381">1SG-ACC</ta>
            <ta e="T383" id="Seg_6018" s="T382">Betrug-VBZ-FUT-2SG</ta>
            <ta e="T384" id="Seg_6019" s="T383">NEG-3SG</ta>
            <ta e="T385" id="Seg_6020" s="T384">warum</ta>
            <ta e="T386" id="Seg_6021" s="T385">1SG-ACC</ta>
            <ta e="T387" id="Seg_6022" s="T386">warnen-PRS.[3SG]</ta>
            <ta e="T388" id="Seg_6023" s="T387">denken-CVB.SIM</ta>
            <ta e="T389" id="Seg_6024" s="T388">denken-PST2.[3SG]</ta>
            <ta e="T390" id="Seg_6025" s="T389">dieses</ta>
            <ta e="T391" id="Seg_6026" s="T390">Frau.[NOM]</ta>
            <ta e="T392" id="Seg_6027" s="T391">was.[NOM]</ta>
            <ta e="T393" id="Seg_6028" s="T392">sein-FUT.[3SG]=Q</ta>
            <ta e="T394" id="Seg_6029" s="T393">doch</ta>
            <ta e="T395" id="Seg_6030" s="T394">Hand-1SG-ACC</ta>
            <ta e="T396" id="Seg_6031" s="T395">stecken-TEMP-1SG</ta>
            <ta e="T397" id="Seg_6032" s="T396">Wasser.[NOM]</ta>
            <ta e="T398" id="Seg_6033" s="T397">Wasser.[NOM]</ta>
            <ta e="T399" id="Seg_6034" s="T398">ähnlich</ta>
            <ta e="T400" id="Seg_6035" s="T399">wohl</ta>
            <ta e="T401" id="Seg_6036" s="T400">was.[NOM]</ta>
            <ta e="T402" id="Seg_6037" s="T401">INDEF</ta>
            <ta e="T403" id="Seg_6038" s="T402">Trick-3SG.[NOM]</ta>
            <ta e="T404" id="Seg_6039" s="T403">es.gibt-3SG</ta>
            <ta e="T405" id="Seg_6040" s="T404">wahrscheinlich</ta>
            <ta e="T406" id="Seg_6041" s="T405">Weißlachs-ACC</ta>
            <ta e="T407" id="Seg_6042" s="T406">greifen-CVB.SEQ</ta>
            <ta e="T408" id="Seg_6043" s="T407">nehmen-CVB.SEQ</ta>
            <ta e="T409" id="Seg_6044" s="T408">nachdem</ta>
            <ta e="T410" id="Seg_6045" s="T409">Schwanz-3SG-ACC</ta>
            <ta e="T411" id="Seg_6046" s="T410">stecken-PST2.[3SG]</ta>
            <ta e="T412" id="Seg_6047" s="T411">Eis.[NOM]</ta>
            <ta e="T413" id="Seg_6048" s="T412">Truhe.[NOM]</ta>
            <ta e="T414" id="Seg_6049" s="T413">Inneres-3SG-DAT/LOC</ta>
            <ta e="T415" id="Seg_6050" s="T414">Weißlachs.[NOM]</ta>
            <ta e="T416" id="Seg_6051" s="T415">Schwanz-3SG.[NOM]</ta>
            <ta e="T417" id="Seg_6052" s="T416">völlig</ta>
            <ta e="T418" id="Seg_6053" s="T417">brechen-CVB.SEQ</ta>
            <ta e="T419" id="Seg_6054" s="T418">bleiben-PST2.[3SG]</ta>
            <ta e="T420" id="Seg_6055" s="T419">Perlen.[NOM]</ta>
            <ta e="T421" id="Seg_6056" s="T420">Bart.[NOM]</ta>
            <ta e="T422" id="Seg_6057" s="T421">Tochter-3SG.[NOM]</ta>
            <ta e="T423" id="Seg_6058" s="T422">erraten-PST1-3SG</ta>
            <ta e="T424" id="Seg_6059" s="T423">warum</ta>
            <ta e="T425" id="Seg_6060" s="T424">Wasser.[NOM]</ta>
            <ta e="T426" id="Seg_6061" s="T425">Herr-3SG.[NOM]</ta>
            <ta e="T427" id="Seg_6062" s="T426">warnen-PTCP.PST-3SG-ACC</ta>
            <ta e="T428" id="Seg_6063" s="T427">Truhe.[NOM]</ta>
            <ta e="T429" id="Seg_6064" s="T428">Platz.neben-3SG-DAT/LOC</ta>
            <ta e="T430" id="Seg_6065" s="T429">sitzen-CVB.SEQ</ta>
            <ta e="T431" id="Seg_6066" s="T430">nachdem</ta>
            <ta e="T432" id="Seg_6067" s="T431">Netz.[NOM]</ta>
            <ta e="T433" id="Seg_6068" s="T432">flicken-EP-MED-CVB.SIM</ta>
            <ta e="T434" id="Seg_6069" s="T433">sitzen-PRS.[3SG]</ta>
            <ta e="T435" id="Seg_6070" s="T434">Wasser.[NOM]</ta>
            <ta e="T436" id="Seg_6071" s="T435">Herr-3SG.[NOM]</ta>
            <ta e="T437" id="Seg_6072" s="T436">kommen-CVB.SEQ</ta>
            <ta e="T438" id="Seg_6073" s="T437">sich.wundern-PST1-3SG</ta>
            <ta e="T439" id="Seg_6074" s="T438">was</ta>
            <ta e="T440" id="Seg_6075" s="T439">Netz-3SG-ACC</ta>
            <ta e="T441" id="Seg_6076" s="T440">flicken-PRS-2SG</ta>
            <ta e="T442" id="Seg_6077" s="T441">Wasser.[NOM]</ta>
            <ta e="T443" id="Seg_6078" s="T442">Inneres-3SG-DAT/LOC</ta>
            <ta e="T444" id="Seg_6079" s="T443">Fisch.[NOM]</ta>
            <ta e="T445" id="Seg_6080" s="T444">viel</ta>
            <ta e="T446" id="Seg_6081" s="T445">Netz-POSS</ta>
            <ta e="T447" id="Seg_6082" s="T446">und</ta>
            <ta e="T448" id="Seg_6083" s="T447">NEG</ta>
            <ta e="T449" id="Seg_6084" s="T448">1SG.[NOM]</ta>
            <ta e="T450" id="Seg_6085" s="T449">2SG-DAT/LOC</ta>
            <ta e="T451" id="Seg_6086" s="T450">wie.viel-ACC</ta>
            <ta e="T452" id="Seg_6087" s="T451">denken-PTCP.PRS-ACC</ta>
            <ta e="T453" id="Seg_6088" s="T452">bringen-FUT-1SG</ta>
            <ta e="T454" id="Seg_6089" s="T453">Fisch.[NOM]</ta>
            <ta e="T455" id="Seg_6090" s="T454">viel</ta>
            <ta e="T456" id="Seg_6091" s="T455">sein-CVB.SEQ</ta>
            <ta e="T457" id="Seg_6092" s="T456">nachdem</ta>
            <ta e="T458" id="Seg_6093" s="T457">sagen-PRS.[3SG]</ta>
            <ta e="T459" id="Seg_6094" s="T458">Frau-3SG.[NOM]</ta>
            <ta e="T460" id="Seg_6095" s="T459">eins</ta>
            <ta e="T461" id="Seg_6096" s="T460">NEG</ta>
            <ta e="T462" id="Seg_6097" s="T461">Rentier.[NOM]</ta>
            <ta e="T463" id="Seg_6098" s="T462">Fleisch-3SG.[NOM]</ta>
            <ta e="T464" id="Seg_6099" s="T463">NEG.EX</ta>
            <ta e="T465" id="Seg_6100" s="T464">1SG.[NOM]</ta>
            <ta e="T466" id="Seg_6101" s="T465">Rentier.[NOM]</ta>
            <ta e="T467" id="Seg_6102" s="T466">Fleisch-3SG-DAT/LOC</ta>
            <ta e="T468" id="Seg_6103" s="T467">lernen-PST2-1SG</ta>
            <ta e="T469" id="Seg_6104" s="T468">Rentier.[NOM]</ta>
            <ta e="T470" id="Seg_6105" s="T469">Fleisch-3SG-ACC</ta>
            <ta e="T471" id="Seg_6106" s="T470">essen-PTCP.FUT-1SG-ACC</ta>
            <ta e="T472" id="Seg_6107" s="T471">wollen-PRS-1SG</ta>
            <ta e="T473" id="Seg_6108" s="T472">dieses</ta>
            <ta e="T474" id="Seg_6109" s="T473">Eis.[NOM]</ta>
            <ta e="T475" id="Seg_6110" s="T474">Truhe.[NOM]</ta>
            <ta e="T476" id="Seg_6111" s="T475">Inneres-3SG-DAT/LOC</ta>
            <ta e="T477" id="Seg_6112" s="T476">Rentier.[NOM]</ta>
            <ta e="T478" id="Seg_6113" s="T477">viel</ta>
            <ta e="T479" id="Seg_6114" s="T478">Fisch.[NOM]</ta>
            <ta e="T480" id="Seg_6115" s="T479">ähnlich</ta>
            <ta e="T481" id="Seg_6116" s="T480">schwimmen-CVB.SIM</ta>
            <ta e="T482" id="Seg_6117" s="T481">gehen-PRS-3PL</ta>
            <ta e="T483" id="Seg_6118" s="T482">Wasser.[NOM]</ta>
            <ta e="T484" id="Seg_6119" s="T483">Herr-3SG.[NOM]</ta>
            <ta e="T485" id="Seg_6120" s="T484">doch</ta>
            <ta e="T486" id="Seg_6121" s="T485">lachen-PRS.[3SG]</ta>
            <ta e="T487" id="Seg_6122" s="T486">dieses</ta>
            <ta e="T488" id="Seg_6123" s="T487">Eis.[NOM]</ta>
            <ta e="T489" id="Seg_6124" s="T488">Truhe-DAT/LOC</ta>
            <ta e="T490" id="Seg_6125" s="T489">was.[NOM]</ta>
            <ta e="T491" id="Seg_6126" s="T490">Rentier-3SG.[NOM]</ta>
            <ta e="T492" id="Seg_6127" s="T491">kommen-FUT.[3SG]=Q</ta>
            <ta e="T493" id="Seg_6128" s="T492">hier</ta>
            <ta e="T494" id="Seg_6129" s="T493">Wasser.[NOM]</ta>
            <ta e="T495" id="Seg_6130" s="T494">nur</ta>
            <ta e="T496" id="Seg_6131" s="T495">1SG.[NOM]</ta>
            <ta e="T497" id="Seg_6132" s="T496">wissen-PRS-1SG</ta>
            <ta e="T498" id="Seg_6133" s="T497">EVID</ta>
            <ta e="T499" id="Seg_6134" s="T498">Frau-3SG.[NOM]</ta>
            <ta e="T500" id="Seg_6135" s="T499">sagen-PST2.[3SG]</ta>
            <ta e="T501" id="Seg_6136" s="T500">1SG.[NOM]</ta>
            <ta e="T502" id="Seg_6137" s="T501">sehen-PST2-EP-1SG</ta>
            <ta e="T503" id="Seg_6138" s="T502">Rentier-PL.[NOM]</ta>
            <ta e="T504" id="Seg_6139" s="T503">schwimmen-CVB.SIM</ta>
            <ta e="T505" id="Seg_6140" s="T504">gehen-PRS-3PL</ta>
            <ta e="T506" id="Seg_6141" s="T505">(irgend)ein-PL.[NOM]</ta>
            <ta e="T507" id="Seg_6142" s="T506">weiß-PL.[NOM]</ta>
            <ta e="T508" id="Seg_6143" s="T507">(irgend)ein-PL.[NOM]</ta>
            <ta e="T509" id="Seg_6144" s="T508">scheckig-PL.[NOM]</ta>
            <ta e="T510" id="Seg_6145" s="T509">anderer.von.zwei-3PL.[NOM]</ta>
            <ta e="T511" id="Seg_6146" s="T510">schwarz-PL.[NOM]</ta>
            <ta e="T512" id="Seg_6147" s="T511">ganz</ta>
            <ta e="T513" id="Seg_6148" s="T512">Herde.[NOM]</ta>
            <ta e="T514" id="Seg_6149" s="T513">dumm</ta>
            <ta e="T515" id="Seg_6150" s="T514">Frau-2SG</ta>
            <ta e="T516" id="Seg_6151" s="T515">sein-PST2.[3SG]</ta>
            <ta e="T517" id="Seg_6152" s="T516">böse.werden-PST1-3SG</ta>
            <ta e="T518" id="Seg_6153" s="T517">Wasser.[NOM]</ta>
            <ta e="T519" id="Seg_6154" s="T518">Herr-3SG.[NOM]</ta>
            <ta e="T520" id="Seg_6155" s="T519">was.[NOM]</ta>
            <ta e="T521" id="Seg_6156" s="T520">Rentier-3SG.[NOM]</ta>
            <ta e="T522" id="Seg_6157" s="T521">Wasser.[NOM]</ta>
            <ta e="T523" id="Seg_6158" s="T522">Inneres-3SG-DAT/LOC</ta>
            <ta e="T524" id="Seg_6159" s="T523">gehen-FUT.[3SG]=Q</ta>
            <ta e="T525" id="Seg_6160" s="T524">selbst-1SG.[NOM]</ta>
            <ta e="T526" id="Seg_6161" s="T525">wissen-PRS-2SG</ta>
            <ta e="T527" id="Seg_6162" s="T526">1PL.[NOM]</ta>
            <ta e="T528" id="Seg_6163" s="T527">Wasser.[NOM]</ta>
            <ta e="T529" id="Seg_6164" s="T528">Inneres-3SG-DAT/LOC</ta>
            <ta e="T530" id="Seg_6165" s="T529">leben-PRS-1PL</ta>
            <ta e="T531" id="Seg_6166" s="T530">jenes.[NOM]</ta>
            <ta e="T532" id="Seg_6167" s="T531">wegen</ta>
            <ta e="T533" id="Seg_6168" s="T532">Rentier-PL.[NOM]</ta>
            <ta e="T534" id="Seg_6169" s="T533">auch</ta>
            <ta e="T535" id="Seg_6170" s="T534">Wasser.[NOM]</ta>
            <ta e="T536" id="Seg_6171" s="T535">Inneres-3SG-DAT/LOC</ta>
            <ta e="T537" id="Seg_6172" s="T536">gehen-PRS-3PL</ta>
            <ta e="T538" id="Seg_6173" s="T537">sagen-PST2.[3SG]</ta>
            <ta e="T539" id="Seg_6174" s="T538">Frau-3SG.[NOM]</ta>
            <ta e="T540" id="Seg_6175" s="T539">sehen.[IMP.2SG]</ta>
            <ta e="T541" id="Seg_6176" s="T540">doch</ta>
            <ta e="T542" id="Seg_6177" s="T541">Truhe.[NOM]</ta>
            <ta e="T543" id="Seg_6178" s="T542">Inneres-3SG-ACC</ta>
            <ta e="T544" id="Seg_6179" s="T543">Wasser.[NOM]</ta>
            <ta e="T545" id="Seg_6180" s="T544">Herr-3SG.[NOM]</ta>
            <ta e="T546" id="Seg_6181" s="T545">böse.werden-CVB.SEQ</ta>
            <ta e="T547" id="Seg_6182" s="T546">nachdem</ta>
            <ta e="T548" id="Seg_6183" s="T547">vergessen-CVB.SEQ</ta>
            <ta e="T549" id="Seg_6184" s="T548">werfen-PST2.[3SG]</ta>
            <ta e="T550" id="Seg_6185" s="T549">Truhe-3SG-DAT/LOC</ta>
            <ta e="T551" id="Seg_6186" s="T550">hineinschauen-PST2.[3SG]</ta>
            <ta e="T552" id="Seg_6187" s="T551">Rentier-PL-ACC</ta>
            <ta e="T553" id="Seg_6188" s="T552">sehen-CVB.PURP</ta>
            <ta e="T554" id="Seg_6189" s="T553">Frau-3SG.[NOM]</ta>
            <ta e="T555" id="Seg_6190" s="T554">kommen-CVB.SEQ</ta>
            <ta e="T556" id="Seg_6191" s="T555">Kopf-3SG-ACC</ta>
            <ta e="T557" id="Seg_6192" s="T556">Wasser.[NOM]</ta>
            <ta e="T558" id="Seg_6193" s="T557">Inneres-3SG-DAT/LOC</ta>
            <ta e="T559" id="Seg_6194" s="T558">zerstören-ADVZ</ta>
            <ta e="T560" id="Seg_6195" s="T559">herunterziehen-PST2.[3SG]</ta>
            <ta e="T561" id="Seg_6196" s="T560">Wasser.[NOM]</ta>
            <ta e="T562" id="Seg_6197" s="T561">Herr-3SG-GEN</ta>
            <ta e="T563" id="Seg_6198" s="T562">Kopf-3SG.[NOM]</ta>
            <ta e="T564" id="Seg_6199" s="T563">völlig</ta>
            <ta e="T565" id="Seg_6200" s="T564">gehen-PST2.[3SG]</ta>
            <ta e="T566" id="Seg_6201" s="T565">sofort</ta>
            <ta e="T567" id="Seg_6202" s="T566">sterben-PST2.[3SG]</ta>
            <ta e="T568" id="Seg_6203" s="T567">Perlen.[NOM]</ta>
            <ta e="T569" id="Seg_6204" s="T568">Bart.[NOM]</ta>
            <ta e="T570" id="Seg_6205" s="T569">klein</ta>
            <ta e="T571" id="Seg_6206" s="T570">Tochter-3SG.[NOM]</ta>
            <ta e="T572" id="Seg_6207" s="T571">Tür-ABL</ta>
            <ta e="T573" id="Seg_6208" s="T572">Tür-ACC</ta>
            <ta e="T574" id="Seg_6209" s="T573">herumgehen-CVB.SIM</ta>
            <ta e="T575" id="Seg_6210" s="T574">gehen-CVB.SEQ</ta>
            <ta e="T576" id="Seg_6211" s="T575">ältere.Schwester-PL-3SG-ACC</ta>
            <ta e="T577" id="Seg_6212" s="T576">finden-PST2.[3SG]</ta>
            <ta e="T578" id="Seg_6213" s="T577">Tür-3PL-GEN</ta>
            <ta e="T579" id="Seg_6214" s="T578">Klebe-3PL-ACC</ta>
            <ta e="T580" id="Seg_6215" s="T579">Messer-EP-INSTR</ta>
            <ta e="T581" id="Seg_6216" s="T580">kratzen-CVB.SEQ</ta>
            <ta e="T582" id="Seg_6217" s="T581">nehmen-CVB.SEQ</ta>
            <ta e="T583" id="Seg_6218" s="T582">befreien-PST2.[3SG]</ta>
            <ta e="T584" id="Seg_6219" s="T583">drei-COLL-3PL.[NOM]</ta>
            <ta e="T585" id="Seg_6220" s="T584">Stange.[NOM]</ta>
            <ta e="T586" id="Seg_6221" s="T585">Zelt-3PL-DAT/LOC</ta>
            <ta e="T587" id="Seg_6222" s="T586">kommen-PST2-3PL</ta>
            <ta e="T588" id="Seg_6223" s="T587">Perlen.[NOM]</ta>
            <ta e="T589" id="Seg_6224" s="T588">Bart.[NOM]</ta>
            <ta e="T590" id="Seg_6225" s="T589">sich.freuen-CVB.SEQ</ta>
            <ta e="T591" id="Seg_6226" s="T590">Tochter-PL-3SG-DAT/LOC</ta>
            <ta e="T592" id="Seg_6227" s="T591">hobeln-PTCP.PST.[NOM]</ta>
            <ta e="T593" id="Seg_6228" s="T592">hobeln-PRS.[3SG]</ta>
            <ta e="T594" id="Seg_6229" s="T593">kochen-PTCP.PST</ta>
            <ta e="T595" id="Seg_6230" s="T594">Fisch-ACC</ta>
            <ta e="T596" id="Seg_6231" s="T595">essen-EP-CAUS-PRS.[3SG]</ta>
            <ta e="T597" id="Seg_6232" s="T596">groß</ta>
            <ta e="T598" id="Seg_6233" s="T597">Tochter-3SG.[NOM]</ta>
            <ta e="T599" id="Seg_6234" s="T598">Herr.[NOM]</ta>
            <ta e="T600" id="Seg_6235" s="T599">Finger-3SG-ACC</ta>
            <ta e="T601" id="Seg_6236" s="T600">sehen-CVB.SEQ</ta>
            <ta e="T602" id="Seg_6237" s="T601">jenes-DAT/LOC</ta>
            <ta e="T603" id="Seg_6238" s="T602">mittlerer</ta>
            <ta e="T604" id="Seg_6239" s="T603">Tochter-3SG.[NOM]</ta>
            <ta e="T605" id="Seg_6240" s="T604">Mitte-ADJZ</ta>
            <ta e="T606" id="Seg_6241" s="T605">Finger-3SG-ACC</ta>
            <ta e="T607" id="Seg_6242" s="T606">sehen-CVB.SEQ</ta>
            <ta e="T608" id="Seg_6243" s="T607">dann</ta>
            <ta e="T609" id="Seg_6244" s="T608">nur</ta>
            <ta e="T610" id="Seg_6245" s="T609">Wasser.[NOM]</ta>
            <ta e="T611" id="Seg_6246" s="T610">Herr-3SG-ACC</ta>
            <ta e="T612" id="Seg_6247" s="T611">erinnern-PRS-3PL</ta>
            <ta e="T613" id="Seg_6248" s="T612">Märchen-VBZ-PST2-3SG</ta>
            <ta e="T614" id="Seg_6249" s="T613">Ogdo</ta>
            <ta e="T615" id="Seg_6250" s="T614">Aksjonova.[NOM]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_6251" s="T0">слушать-IMP.2PL</ta>
            <ta e="T2" id="Seg_6252" s="T1">сказка-ACC</ta>
            <ta e="T3" id="Seg_6253" s="T2">имя-3SG.[NOM]</ta>
            <ta e="T4" id="Seg_6254" s="T3">бисер.[NOM]</ta>
            <ta e="T5" id="Seg_6255" s="T4">борода.[NOM]</ta>
            <ta e="T6" id="Seg_6256" s="T5">один</ta>
            <ta e="T7" id="Seg_6257" s="T6">старик.[NOM]</ta>
            <ta e="T8" id="Seg_6258" s="T7">есть</ta>
            <ta e="T9" id="Seg_6259" s="T8">быть-PST2.[3SG]</ta>
            <ta e="T10" id="Seg_6260" s="T9">старик.[NOM]</ta>
            <ta e="T11" id="Seg_6261" s="T10">борода-3SG.[NOM]</ta>
            <ta e="T12" id="Seg_6262" s="T11">промежуток-PROPR.[NOM]</ta>
            <ta e="T13" id="Seg_6263" s="T12">быть-CVB.SEQ</ta>
            <ta e="T14" id="Seg_6264" s="T13">после</ta>
            <ta e="T15" id="Seg_6265" s="T14">длинный.[NOM]</ta>
            <ta e="T16" id="Seg_6266" s="T15">очень</ta>
            <ta e="T17" id="Seg_6267" s="T16">борода-PL-3SG.[NOM]</ta>
            <ta e="T18" id="Seg_6268" s="T17">бисер-PROPR</ta>
            <ta e="T19" id="Seg_6269" s="T18">нить.[NOM]</ta>
            <ta e="T20" id="Seg_6270" s="T19">подобно</ta>
            <ta e="T21" id="Seg_6271" s="T20">блеснуть-ITER-PRS-3PL</ta>
            <ta e="T22" id="Seg_6272" s="T21">тот.[NOM]</ta>
            <ta e="T23" id="Seg_6273" s="T22">из_за</ta>
            <ta e="T24" id="Seg_6274" s="T23">старик-ACC</ta>
            <ta e="T25" id="Seg_6275" s="T24">имя-VBZ-PST2-3PL</ta>
            <ta e="T26" id="Seg_6276" s="T25">бисер.[NOM]</ta>
            <ta e="T27" id="Seg_6277" s="T26">борода.[NOM]</ta>
            <ta e="T28" id="Seg_6278" s="T27">говорить-CVB.SEQ</ta>
            <ta e="T29" id="Seg_6279" s="T28">бисер.[NOM]</ta>
            <ta e="T30" id="Seg_6280" s="T29">борода.[NOM]</ta>
            <ta e="T31" id="Seg_6281" s="T30">три</ta>
            <ta e="T32" id="Seg_6282" s="T31">дочь-PROPR.[NOM]</ta>
            <ta e="T33" id="Seg_6283" s="T32">зимой</ta>
            <ta e="T34" id="Seg_6284" s="T33">летом</ta>
            <ta e="T35" id="Seg_6285" s="T34">сеть-VBZ-PRS.[3SG]</ta>
            <ta e="T36" id="Seg_6286" s="T35">рыба-EP-INSTR</ta>
            <ta e="T37" id="Seg_6287" s="T36">только</ta>
            <ta e="T38" id="Seg_6288" s="T37">кормить-EP-PASS/REFL-CVB.SEQ</ta>
            <ta e="T39" id="Seg_6289" s="T38">сидеть-PRS-3PL</ta>
            <ta e="T40" id="Seg_6290" s="T39">однажды</ta>
            <ta e="T41" id="Seg_6291" s="T40">бисер.[NOM]</ta>
            <ta e="T42" id="Seg_6292" s="T41">борода.[NOM]</ta>
            <ta e="T43" id="Seg_6293" s="T42">сеть.[NOM]</ta>
            <ta e="T44" id="Seg_6294" s="T43">видеть-EP-MED-CVB.SIM</ta>
            <ta e="T45" id="Seg_6295" s="T44">идти-PST2.[3SG]</ta>
            <ta e="T46" id="Seg_6296" s="T45">прорубь-3SG-DAT/LOC</ta>
            <ta e="T47" id="Seg_6297" s="T46">нагибать-PTCP.PRS-3SG-ACC</ta>
            <ta e="T48" id="Seg_6298" s="T47">с</ta>
            <ta e="T49" id="Seg_6299" s="T48">кто.[NOM]</ta>
            <ta e="T50" id="Seg_6300" s="T49">INDEF</ta>
            <ta e="T51" id="Seg_6301" s="T50">борода-3SG-ABL</ta>
            <ta e="T52" id="Seg_6302" s="T51">хватать-CVB.SEQ</ta>
            <ta e="T53" id="Seg_6303" s="T52">взять-PST2.[3SG]</ta>
            <ta e="T54" id="Seg_6304" s="T53">вообще</ta>
            <ta e="T55" id="Seg_6305" s="T54">пустить-NEG.[3SG]</ta>
            <ta e="T56" id="Seg_6306" s="T55">кто.[NOM]</ta>
            <ta e="T57" id="Seg_6307" s="T56">быть-FUT.[3SG]=Q</ta>
            <ta e="T58" id="Seg_6308" s="T57">думать-CVB.SIM</ta>
            <ta e="T59" id="Seg_6309" s="T58">думать-PST2.[3SG]</ta>
            <ta e="T60" id="Seg_6310" s="T59">прорубь-ABL</ta>
            <ta e="T61" id="Seg_6311" s="T60">высунуться-PST2.[3SG]</ta>
            <ta e="T62" id="Seg_6312" s="T61">вода.[NOM]</ta>
            <ta e="T63" id="Seg_6313" s="T62">господин-3SG.[NOM]</ta>
            <ta e="T64" id="Seg_6314" s="T63">бисер.[NOM]</ta>
            <ta e="T65" id="Seg_6315" s="T64">борода.[NOM]</ta>
            <ta e="T66" id="Seg_6316" s="T65">говорить-PRS.[3SG]</ta>
            <ta e="T67" id="Seg_6317" s="T66">дочь-2SG-ACC</ta>
            <ta e="T68" id="Seg_6318" s="T67">давать-PRS-2SG</ta>
            <ta e="T69" id="Seg_6319" s="T68">Q</ta>
            <ta e="T70" id="Seg_6320" s="T69">NEG</ta>
            <ta e="T71" id="Seg_6321" s="T70">Q</ta>
            <ta e="T72" id="Seg_6322" s="T71">жена.[NOM]</ta>
            <ta e="T73" id="Seg_6323" s="T72">делать-PTCP.FUT-1SG-ACC</ta>
            <ta e="T74" id="Seg_6324" s="T73">давать-NEG-TEMP-2SG</ta>
            <ta e="T75" id="Seg_6325" s="T74">один</ta>
            <ta e="T76" id="Seg_6326" s="T75">NEG</ta>
            <ta e="T77" id="Seg_6327" s="T76">рыба-ACC</ta>
            <ta e="T78" id="Seg_6328" s="T77">взять-FUT-2SG</ta>
            <ta e="T79" id="Seg_6329" s="T78">NEG-3SG</ta>
            <ta e="T80" id="Seg_6330" s="T79">что-EP-INSTR</ta>
            <ta e="T81" id="Seg_6331" s="T80">семья-PL-1SG-ACC</ta>
            <ta e="T82" id="Seg_6332" s="T81">кормить-FUT-1SG=Q</ta>
            <ta e="T83" id="Seg_6333" s="T82">вот</ta>
            <ta e="T84" id="Seg_6334" s="T83">думать-CVB.SIM</ta>
            <ta e="T85" id="Seg_6335" s="T84">думать-PST2.[3SG]</ta>
            <ta e="T86" id="Seg_6336" s="T85">потом</ta>
            <ta e="T87" id="Seg_6337" s="T86">согласить-PST2.[3SG]</ta>
            <ta e="T88" id="Seg_6338" s="T87">обман-VBZ-TEMP-2SG</ta>
            <ta e="T89" id="Seg_6339" s="T88">ведь</ta>
            <ta e="T90" id="Seg_6340" s="T89">все.равно</ta>
            <ta e="T91" id="Seg_6341" s="T90">найти-FUT-1SG</ta>
            <ta e="T92" id="Seg_6342" s="T91">говорить-PST2.[3SG]</ta>
            <ta e="T93" id="Seg_6343" s="T92">вода.[NOM]</ta>
            <ta e="T94" id="Seg_6344" s="T93">господин-3SG.[NOM]</ta>
            <ta e="T95" id="Seg_6345" s="T94">старик.[NOM]</ta>
            <ta e="T96" id="Seg_6346" s="T95">рукавицы-3SG-ACC</ta>
            <ta e="T97" id="Seg_6347" s="T96">хватать-CVB.SEQ</ta>
            <ta e="T98" id="Seg_6348" s="T97">взять-CVB.SEQ</ta>
            <ta e="T99" id="Seg_6349" s="T98">после</ta>
            <ta e="T100" id="Seg_6350" s="T99">лед.[NOM]</ta>
            <ta e="T101" id="Seg_6351" s="T100">нутро-3SG-INSTR</ta>
            <ta e="T102" id="Seg_6352" s="T101">идти-CVB.SEQ</ta>
            <ta e="T103" id="Seg_6353" s="T102">оставаться-PST2.[3SG]</ta>
            <ta e="T104" id="Seg_6354" s="T103">бисер.[NOM]</ta>
            <ta e="T105" id="Seg_6355" s="T104">борода.[NOM]</ta>
            <ta e="T106" id="Seg_6356" s="T105">очень</ta>
            <ta e="T107" id="Seg_6357" s="T106">расстроиться-PST2.[3SG]</ta>
            <ta e="T108" id="Seg_6358" s="T107">шест.[NOM]</ta>
            <ta e="T109" id="Seg_6359" s="T108">чум-3SG-DAT/LOC</ta>
            <ta e="T110" id="Seg_6360" s="T109">приходить-CVB.SEQ</ta>
            <ta e="T111" id="Seg_6361" s="T110">дочь-PL-3SG-DAT/LOC</ta>
            <ta e="T112" id="Seg_6362" s="T111">рассказывать-PST2.[3SG]</ta>
            <ta e="T113" id="Seg_6363" s="T112">большой</ta>
            <ta e="T114" id="Seg_6364" s="T113">дочь-3SG.[NOM]</ta>
            <ta e="T115" id="Seg_6365" s="T114">радоваться-PST2.[3SG]</ta>
            <ta e="T116" id="Seg_6366" s="T115">1SG.[NOM]</ta>
            <ta e="T117" id="Seg_6367" s="T116">муж-DAT/LOC</ta>
            <ta e="T118" id="Seg_6368" s="T117">идти-FUT-1SG</ta>
            <ta e="T119" id="Seg_6369" s="T118">вода.[NOM]</ta>
            <ta e="T120" id="Seg_6370" s="T119">господин-3SG-DAT/LOC</ta>
            <ta e="T121" id="Seg_6371" s="T120">мир.[NOM]</ta>
            <ta e="T122" id="Seg_6372" s="T121">верхняя.часть-3SG-INSTR</ta>
            <ta e="T123" id="Seg_6373" s="T122">самый</ta>
            <ta e="T124" id="Seg_6374" s="T123">богатый.[NOM]</ta>
            <ta e="T125" id="Seg_6375" s="T124">быть-FUT-1SG</ta>
            <ta e="T126" id="Seg_6376" s="T125">бисер.[NOM]</ta>
            <ta e="T127" id="Seg_6377" s="T126">борода.[NOM]</ta>
            <ta e="T128" id="Seg_6378" s="T127">дочь-3SG-ACC</ta>
            <ta e="T129" id="Seg_6379" s="T128">муж-DAT/LOC</ta>
            <ta e="T130" id="Seg_6380" s="T129">давать-CVB.SEQ</ta>
            <ta e="T131" id="Seg_6381" s="T130">бросать-PST2.[3SG]</ta>
            <ta e="T132" id="Seg_6382" s="T131">вода.[NOM]</ta>
            <ta e="T133" id="Seg_6383" s="T132">господин-3SG.[NOM]</ta>
            <ta e="T134" id="Seg_6384" s="T133">богатый.[NOM]</ta>
            <ta e="T135" id="Seg_6385" s="T134">да</ta>
            <ta e="T136" id="Seg_6386" s="T135">богатый.[NOM]</ta>
            <ta e="T137" id="Seg_6387" s="T136">богатство-3SG-GEN</ta>
            <ta e="T138" id="Seg_6388" s="T137">конец-3SG.[NOM]</ta>
            <ta e="T139" id="Seg_6389" s="T138">быть.видно-EP-NEG.[3SG]</ta>
            <ta e="T140" id="Seg_6390" s="T139">сколько</ta>
            <ta e="T141" id="Seg_6391" s="T140">INDEF</ta>
            <ta e="T142" id="Seg_6392" s="T141">комната-PROPR.[NOM]</ta>
            <ta e="T143" id="Seg_6393" s="T142">дом-PROPR.[NOM]</ta>
            <ta e="T144" id="Seg_6394" s="T143">один</ta>
            <ta e="T145" id="Seg_6395" s="T144">комната-DAT/LOC</ta>
            <ta e="T146" id="Seg_6396" s="T145">большой</ta>
            <ta e="T147" id="Seg_6397" s="T146">лед.[NOM]</ta>
            <ta e="T148" id="Seg_6398" s="T147">сундук.[NOM]</ta>
            <ta e="T149" id="Seg_6399" s="T148">стоять-PRS.[3SG]</ta>
            <ta e="T150" id="Seg_6400" s="T149">вода.[NOM]</ta>
            <ta e="T151" id="Seg_6401" s="T150">господин-3SG.[NOM]</ta>
            <ta e="T152" id="Seg_6402" s="T151">жена-3SG-ACC</ta>
            <ta e="T153" id="Seg_6403" s="T152">приносить-CVB.SEQ</ta>
            <ta e="T154" id="Seg_6404" s="T153">лед.[NOM]</ta>
            <ta e="T155" id="Seg_6405" s="T154">сундук-ACC</ta>
            <ta e="T156" id="Seg_6406" s="T155">показывать-PST2.[3SG]</ta>
            <ta e="T157" id="Seg_6407" s="T156">где</ta>
            <ta e="T158" id="Seg_6408" s="T157">думать-PTCP.PRS</ta>
            <ta e="T159" id="Seg_6409" s="T158">идти.[IMP.2SG]</ta>
            <ta e="T160" id="Seg_6410" s="T159">видеть.[IMP.2SG]</ta>
            <ta e="T161" id="Seg_6411" s="T160">этот.[NOM]</ta>
            <ta e="T162" id="Seg_6412" s="T161">только</ta>
            <ta e="T164" id="Seg_6413" s="T163">сундук.[NOM]</ta>
            <ta e="T165" id="Seg_6414" s="T164">нутро-3SG-DAT/LOC</ta>
            <ta e="T166" id="Seg_6415" s="T165">вставлять-EP-NEG.[IMP.2SG]</ta>
            <ta e="T167" id="Seg_6416" s="T166">палец-2SG-ACC</ta>
            <ta e="T168" id="Seg_6417" s="T167">говорить-PST2.[3SG]</ta>
            <ta e="T169" id="Seg_6418" s="T168">муж-3SG.[NOM]</ta>
            <ta e="T170" id="Seg_6419" s="T169">идти-PTCP.PST-3SG-ACC</ta>
            <ta e="T171" id="Seg_6420" s="T170">после.того</ta>
            <ta e="T172" id="Seg_6421" s="T171">этот</ta>
            <ta e="T173" id="Seg_6422" s="T172">жена.[NOM]</ta>
            <ta e="T174" id="Seg_6423" s="T173">выносить-NEG.CVB.SIM</ta>
            <ta e="T175" id="Seg_6424" s="T174">палец-3SG-ACC</ta>
            <ta e="T176" id="Seg_6425" s="T175">вставлять-PST2.[3SG]</ta>
            <ta e="T177" id="Seg_6426" s="T176">вода.[NOM]</ta>
            <ta e="T178" id="Seg_6427" s="T177">лед.[NOM]</ta>
            <ta e="T179" id="Seg_6428" s="T178">становиться-PST2.[3SG]</ta>
            <ta e="T180" id="Seg_6429" s="T179">еле</ta>
            <ta e="T181" id="Seg_6430" s="T180">PFV</ta>
            <ta e="T182" id="Seg_6431" s="T181">тянуть-PST2.[3SG]</ta>
            <ta e="T183" id="Seg_6432" s="T182">палец-3SG-ACC</ta>
            <ta e="T184" id="Seg_6433" s="T183">видеть-PST2-3SG</ta>
            <ta e="T185" id="Seg_6434" s="T184">господин.[NOM]</ta>
            <ta e="T186" id="Seg_6435" s="T185">палец-3SG.[NOM]</ta>
            <ta e="T187" id="Seg_6436" s="T186">совсем</ta>
            <ta e="T188" id="Seg_6437" s="T187">идти-PST2.[3SG]</ta>
            <ta e="T189" id="Seg_6438" s="T188">тот-3SG-3SG-ACC</ta>
            <ta e="T190" id="Seg_6439" s="T189">крепко</ta>
            <ta e="T191" id="Seg_6440" s="T190">связывать-CVB.SEQ</ta>
            <ta e="T192" id="Seg_6441" s="T191">после</ta>
            <ta e="T193" id="Seg_6442" s="T192">сидеть-PRS.[3SG]</ta>
            <ta e="T194" id="Seg_6443" s="T193">бояться-CVB.SEQ</ta>
            <ta e="T195" id="Seg_6444" s="T194">глаз-PL-3SG.[NOM]</ta>
            <ta e="T196" id="Seg_6445" s="T195">выкатывать-ADVZ</ta>
            <ta e="T197" id="Seg_6446" s="T196">идти-PST2-3PL</ta>
            <ta e="T198" id="Seg_6447" s="T197">вода.[NOM]</ta>
            <ta e="T199" id="Seg_6448" s="T198">господин-3SG.[NOM]</ta>
            <ta e="T200" id="Seg_6449" s="T199">приходить-CVB.SEQ</ta>
            <ta e="T201" id="Seg_6450" s="T200">сразу</ta>
            <ta e="T202" id="Seg_6451" s="T201">догадываться-PST1-3SG</ta>
            <ta e="T203" id="Seg_6452" s="T202">1SG-DAT/LOC</ta>
            <ta e="T204" id="Seg_6453" s="T203">2SG.[NOM]</ta>
            <ta e="T205" id="Seg_6454" s="T204">жена.[NOM]</ta>
            <ta e="T206" id="Seg_6455" s="T205">быть-NEG.PTCP.[NOM]</ta>
            <ta e="T207" id="Seg_6456" s="T206">как-2SG</ta>
            <ta e="T208" id="Seg_6457" s="T207">говорить-PST2.[3SG]</ta>
            <ta e="T209" id="Seg_6458" s="T208">жена-ACC</ta>
            <ta e="T210" id="Seg_6459" s="T209">самый</ta>
            <ta e="T211" id="Seg_6460" s="T210">далекий-ADJZ</ta>
            <ta e="T212" id="Seg_6461" s="T211">дом-DAT/LOC</ta>
            <ta e="T213" id="Seg_6462" s="T212">комната-3SG-DAT/LOC</ta>
            <ta e="T214" id="Seg_6463" s="T213">уносить-CVB.SEQ</ta>
            <ta e="T215" id="Seg_6464" s="T214">запирать-CVB.SEQ</ta>
            <ta e="T216" id="Seg_6465" s="T215">бросать-PST2.[3SG]</ta>
            <ta e="T217" id="Seg_6466" s="T216">дверь-3SG-ACC</ta>
            <ta e="T218" id="Seg_6467" s="T217">плотно</ta>
            <ta e="T219" id="Seg_6468" s="T218">осетр.[NOM]</ta>
            <ta e="T220" id="Seg_6469" s="T219">желудок-3SG-INSTR</ta>
            <ta e="T221" id="Seg_6470" s="T220">делаться-EP-PTCP.PST</ta>
            <ta e="T222" id="Seg_6471" s="T221">клей-EP-INSTR</ta>
            <ta e="T223" id="Seg_6472" s="T222">крепко</ta>
            <ta e="T224" id="Seg_6473" s="T223">замазать-PST2.[3SG]</ta>
            <ta e="T225" id="Seg_6474" s="T224">бисер.[NOM]</ta>
            <ta e="T226" id="Seg_6475" s="T225">борода.[NOM]</ta>
            <ta e="T227" id="Seg_6476" s="T226">опять</ta>
            <ta e="T228" id="Seg_6477" s="T227">сеть-VBZ-CVB.SIM</ta>
            <ta e="T229" id="Seg_6478" s="T228">приходить-PST2.[3SG]</ta>
            <ta e="T230" id="Seg_6479" s="T229">прорубь-3SG-DAT/LOC</ta>
            <ta e="T231" id="Seg_6480" s="T230">нагибать-PTCP.PRS-3SG-ACC</ta>
            <ta e="T232" id="Seg_6481" s="T231">с</ta>
            <ta e="T233" id="Seg_6482" s="T232">вода.[NOM]</ta>
            <ta e="T234" id="Seg_6483" s="T233">господин-3SG.[NOM]</ta>
            <ta e="T235" id="Seg_6484" s="T234">опять</ta>
            <ta e="T236" id="Seg_6485" s="T235">борода-3SG-ABL</ta>
            <ta e="T237" id="Seg_6486" s="T236">хватать-CVB.SEQ</ta>
            <ta e="T238" id="Seg_6487" s="T237">взять-PST2.[3SG]</ta>
            <ta e="T239" id="Seg_6488" s="T238">приносить.[IMP.2SG]</ta>
            <ta e="T240" id="Seg_6489" s="T239">два-ORD</ta>
            <ta e="T241" id="Seg_6490" s="T240">дочь-2SG-ACC</ta>
            <ta e="T242" id="Seg_6491" s="T241">жена.[NOM]</ta>
            <ta e="T243" id="Seg_6492" s="T242">делать-PTCP.FUT-1SG-ACC</ta>
            <ta e="T244" id="Seg_6493" s="T243">старик.[NOM]</ta>
            <ta e="T245" id="Seg_6494" s="T244">беда-ACC</ta>
            <ta e="T246" id="Seg_6495" s="T245">кончаться-FUT.[3SG]</ta>
            <ta e="T247" id="Seg_6496" s="T246">Q</ta>
            <ta e="T248" id="Seg_6497" s="T247">средний</ta>
            <ta e="T249" id="Seg_6498" s="T248">дочь-3SG-ACC</ta>
            <ta e="T250" id="Seg_6499" s="T249">давать-PST2.[3SG]</ta>
            <ta e="T251" id="Seg_6500" s="T250">этот</ta>
            <ta e="T252" id="Seg_6501" s="T251">жена.[NOM]</ta>
            <ta e="T253" id="Seg_6502" s="T252">богатство.[NOM]</ta>
            <ta e="T254" id="Seg_6503" s="T253">видеть-EP-MED-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T255" id="Seg_6504" s="T254">досуг-POSS</ta>
            <ta e="T256" id="Seg_6505" s="T255">NEG.[3SG]</ta>
            <ta e="T257" id="Seg_6506" s="T256">удивляться-CVB.SEQ</ta>
            <ta e="T258" id="Seg_6507" s="T257">после</ta>
            <ta e="T259" id="Seg_6508" s="T258">кончать-NEG.[3SG]</ta>
            <ta e="T260" id="Seg_6509" s="T259">однажды</ta>
            <ta e="T261" id="Seg_6510" s="T260">этот</ta>
            <ta e="T262" id="Seg_6511" s="T261">лед.[NOM]</ta>
            <ta e="T263" id="Seg_6512" s="T262">сундук-DAT/LOC</ta>
            <ta e="T264" id="Seg_6513" s="T263">принести-PST2.[3SG]</ta>
            <ta e="T265" id="Seg_6514" s="T264">вода.[NOM]</ta>
            <ta e="T266" id="Seg_6515" s="T265">господин-3SG.[NOM]</ta>
            <ta e="T267" id="Seg_6516" s="T266">принести-CVB.SEQ</ta>
            <ta e="T268" id="Seg_6517" s="T267">предупреждать-PST2.[3SG]</ta>
            <ta e="T269" id="Seg_6518" s="T268">этот</ta>
            <ta e="T270" id="Seg_6519" s="T269">сундук-DAT/LOC</ta>
            <ta e="T271" id="Seg_6520" s="T270">рука-2SG-ACC</ta>
            <ta e="T272" id="Seg_6521" s="T271">NEG</ta>
            <ta e="T273" id="Seg_6522" s="T272">палец-2SG-ACC</ta>
            <ta e="T274" id="Seg_6523" s="T273">NEG</ta>
            <ta e="T275" id="Seg_6524" s="T274">вставлять-APPR-2SG</ta>
            <ta e="T276" id="Seg_6525" s="T275">вода.[NOM]</ta>
            <ta e="T277" id="Seg_6526" s="T276">господин-3SG.[NOM]</ta>
            <ta e="T278" id="Seg_6527" s="T277">сани-VBZ-CVB.SIM</ta>
            <ta e="T279" id="Seg_6528" s="T278">идти-PTCP.PST-3SG-ACC</ta>
            <ta e="T280" id="Seg_6529" s="T279">после.того</ta>
            <ta e="T281" id="Seg_6530" s="T280">жена-3SG.[NOM]</ta>
            <ta e="T282" id="Seg_6531" s="T281">лед.[NOM]</ta>
            <ta e="T283" id="Seg_6532" s="T282">сундук-DAT/LOC</ta>
            <ta e="T284" id="Seg_6533" s="T283">приходить-CVB.SEQ</ta>
            <ta e="T285" id="Seg_6534" s="T284">палец-3SG-ACC</ta>
            <ta e="T286" id="Seg_6535" s="T285">вставлять-PST2.[3SG]</ta>
            <ta e="T287" id="Seg_6536" s="T286">середина-ADJZ</ta>
            <ta e="T288" id="Seg_6537" s="T287">палец-3SG.[NOM]</ta>
            <ta e="T289" id="Seg_6538" s="T288">совсем</ta>
            <ta e="T290" id="Seg_6539" s="T289">мерзнуть-PST2.[3SG]</ta>
            <ta e="T291" id="Seg_6540" s="T290">палец-3SG-ACC</ta>
            <ta e="T292" id="Seg_6541" s="T291">крепко</ta>
            <ta e="T293" id="Seg_6542" s="T292">обвязывать-PST2.[3SG]</ta>
            <ta e="T294" id="Seg_6543" s="T293">вода.[NOM]</ta>
            <ta e="T295" id="Seg_6544" s="T294">господин-3SG.[NOM]</ta>
            <ta e="T296" id="Seg_6545" s="T295">приходить-CVB.SEQ</ta>
            <ta e="T297" id="Seg_6546" s="T296">сразу</ta>
            <ta e="T298" id="Seg_6547" s="T297">догадываться-PST2.[3SG]</ta>
            <ta e="T299" id="Seg_6548" s="T298">1SG-DAT/LOC</ta>
            <ta e="T300" id="Seg_6549" s="T299">послушный-POSS</ta>
            <ta e="T301" id="Seg_6550" s="T300">NEG</ta>
            <ta e="T302" id="Seg_6551" s="T301">жена.[NOM]</ta>
            <ta e="T303" id="Seg_6552" s="T302">потребность-POSS</ta>
            <ta e="T304" id="Seg_6553" s="T303">NEG.[3SG]</ta>
            <ta e="T305" id="Seg_6554" s="T304">говорить-CVB.SEQ</ta>
            <ta e="T306" id="Seg_6555" s="T305">после</ta>
            <ta e="T307" id="Seg_6556" s="T306">далекий-ADJZ</ta>
            <ta e="T308" id="Seg_6557" s="T307">дом.[NOM]</ta>
            <ta e="T309" id="Seg_6558" s="T308">комната-3SG-DAT/LOC</ta>
            <ta e="T310" id="Seg_6559" s="T309">заложить-CVB.SEQ</ta>
            <ta e="T311" id="Seg_6560" s="T310">бросать-PST2.[3SG]</ta>
            <ta e="T312" id="Seg_6561" s="T311">дверь-3SG-ACC</ta>
            <ta e="T313" id="Seg_6562" s="T312">плотно</ta>
            <ta e="T314" id="Seg_6563" s="T313">осетр.[NOM]</ta>
            <ta e="T315" id="Seg_6564" s="T314">желудок-3SG-INSTR</ta>
            <ta e="T316" id="Seg_6565" s="T315">делаться-EP-PTCP.PST</ta>
            <ta e="T317" id="Seg_6566" s="T316">клей-EP-INSTR</ta>
            <ta e="T318" id="Seg_6567" s="T317">крепко</ta>
            <ta e="T319" id="Seg_6568" s="T318">замазать-PST2.[3SG]</ta>
            <ta e="T320" id="Seg_6569" s="T319">бисер.[NOM]</ta>
            <ta e="T321" id="Seg_6570" s="T320">борода.[NOM]</ta>
            <ta e="T322" id="Seg_6571" s="T321">опять</ta>
            <ta e="T323" id="Seg_6572" s="T322">сеть.[NOM]</ta>
            <ta e="T324" id="Seg_6573" s="T323">видеть-EP-MED-CVB.SIM</ta>
            <ta e="T325" id="Seg_6574" s="T324">приходить-PST2.[3SG]</ta>
            <ta e="T326" id="Seg_6575" s="T325">прорубь-3SG-DAT/LOC</ta>
            <ta e="T327" id="Seg_6576" s="T326">нагибать-PTCP.PRS-3SG-ACC</ta>
            <ta e="T328" id="Seg_6577" s="T327">с</ta>
            <ta e="T329" id="Seg_6578" s="T328">вода.[NOM]</ta>
            <ta e="T330" id="Seg_6579" s="T329">господин-3SG.[NOM]</ta>
            <ta e="T331" id="Seg_6580" s="T330">опять</ta>
            <ta e="T332" id="Seg_6581" s="T331">борода-3SG-ABL</ta>
            <ta e="T333" id="Seg_6582" s="T332">хватать-CVB.SEQ</ta>
            <ta e="T334" id="Seg_6583" s="T333">взять-PST2.[3SG]</ta>
            <ta e="T335" id="Seg_6584" s="T334">принести.[IMP.2SG]</ta>
            <ta e="T336" id="Seg_6585" s="T335">маленький</ta>
            <ta e="T337" id="Seg_6586" s="T336">дочь-2SG-ACC</ta>
            <ta e="T338" id="Seg_6587" s="T337">жена.[NOM]</ta>
            <ta e="T339" id="Seg_6588" s="T338">делать-PTCP.FUT-1SG-ACC</ta>
            <ta e="T340" id="Seg_6589" s="T339">говорить-PST2.[3SG]</ta>
            <ta e="T341" id="Seg_6590" s="T340">беда-ACC</ta>
            <ta e="T342" id="Seg_6591" s="T341">кончаться-FUT.[3SG]</ta>
            <ta e="T343" id="Seg_6592" s="T342">Q</ta>
            <ta e="T344" id="Seg_6593" s="T343">маленький</ta>
            <ta e="T345" id="Seg_6594" s="T344">дочь-3SG-ACC</ta>
            <ta e="T346" id="Seg_6595" s="T345">приносить-PST2.[3SG]</ta>
            <ta e="T347" id="Seg_6596" s="T346">идти-CVB.SEQ</ta>
            <ta e="T348" id="Seg_6597" s="T347">идти-CVB.SEQ-3PL</ta>
            <ta e="T349" id="Seg_6598" s="T348">дочь-3SG.[NOM]</ta>
            <ta e="T350" id="Seg_6599" s="T349">говорить-PST2.[3SG]</ta>
            <ta e="T351" id="Seg_6600" s="T350">бояться-EP-NEG.[IMP.2SG]</ta>
            <ta e="T352" id="Seg_6601" s="T351">отец.[NOM]</ta>
            <ta e="T353" id="Seg_6602" s="T352">1SG.[NOM]</ta>
            <ta e="T354" id="Seg_6603" s="T353">сразу</ta>
            <ta e="T355" id="Seg_6604" s="T354">приходить-FUT-1SG</ta>
            <ta e="T356" id="Seg_6605" s="T355">старшая.сестра-PL-1SG-ACC</ta>
            <ta e="T357" id="Seg_6606" s="T356">тоже</ta>
            <ta e="T358" id="Seg_6607" s="T357">принести-FUT-1SG</ta>
            <ta e="T359" id="Seg_6608" s="T358">вода.[NOM]</ta>
            <ta e="T360" id="Seg_6609" s="T359">господин-3SG.[NOM]</ta>
            <ta e="T361" id="Seg_6610" s="T360">1SG-COMP</ta>
            <ta e="T362" id="Seg_6611" s="T361">хитрость-POSS</ta>
            <ta e="T363" id="Seg_6612" s="T362">NEG</ta>
            <ta e="T364" id="Seg_6613" s="T363">быть-FUT.[3SG]</ta>
            <ta e="T365" id="Seg_6614" s="T364">видно</ta>
            <ta e="T366" id="Seg_6615" s="T365">вода.[NOM]</ta>
            <ta e="T367" id="Seg_6616" s="T366">господин-3SG.[NOM]</ta>
            <ta e="T368" id="Seg_6617" s="T367">три-ORD</ta>
            <ta e="T369" id="Seg_6618" s="T368">жена-3SG-ACC</ta>
            <ta e="T370" id="Seg_6619" s="T369">лед.[NOM]</ta>
            <ta e="T371" id="Seg_6620" s="T370">сундук-DAT/LOC</ta>
            <ta e="T372" id="Seg_6621" s="T371">принести-CVB.SEQ</ta>
            <ta e="T373" id="Seg_6622" s="T372">после</ta>
            <ta e="T374" id="Seg_6623" s="T373">говорить-PST2.[3SG]</ta>
            <ta e="T375" id="Seg_6624" s="T374">этот</ta>
            <ta e="T376" id="Seg_6625" s="T375">сундук-DAT/LOC</ta>
            <ta e="T377" id="Seg_6626" s="T376">рука-2SG-ACC</ta>
            <ta e="T378" id="Seg_6627" s="T377">вставлять-EP-NEG.[IMP.2SG]</ta>
            <ta e="T379" id="Seg_6628" s="T378">возвращаться-CVB.SEQ</ta>
            <ta e="T380" id="Seg_6629" s="T379">приходить-TEMP-1SG</ta>
            <ta e="T381" id="Seg_6630" s="T380">видеть-FUT-1SG</ta>
            <ta e="T382" id="Seg_6631" s="T381">1SG-ACC</ta>
            <ta e="T383" id="Seg_6632" s="T382">обман-VBZ-FUT-2SG</ta>
            <ta e="T384" id="Seg_6633" s="T383">NEG-3SG</ta>
            <ta e="T385" id="Seg_6634" s="T384">почему</ta>
            <ta e="T386" id="Seg_6635" s="T385">1SG-ACC</ta>
            <ta e="T387" id="Seg_6636" s="T386">предупреждать-PRS.[3SG]</ta>
            <ta e="T388" id="Seg_6637" s="T387">думать-CVB.SIM</ta>
            <ta e="T389" id="Seg_6638" s="T388">думать-PST2.[3SG]</ta>
            <ta e="T390" id="Seg_6639" s="T389">этот</ta>
            <ta e="T391" id="Seg_6640" s="T390">жена.[NOM]</ta>
            <ta e="T392" id="Seg_6641" s="T391">что.[NOM]</ta>
            <ta e="T393" id="Seg_6642" s="T392">быть-FUT.[3SG]=Q</ta>
            <ta e="T394" id="Seg_6643" s="T393">вот</ta>
            <ta e="T395" id="Seg_6644" s="T394">рука-1SG-ACC</ta>
            <ta e="T396" id="Seg_6645" s="T395">вставлять-TEMP-1SG</ta>
            <ta e="T397" id="Seg_6646" s="T396">вода.[NOM]</ta>
            <ta e="T398" id="Seg_6647" s="T397">вода.[NOM]</ta>
            <ta e="T399" id="Seg_6648" s="T398">подобно</ta>
            <ta e="T400" id="Seg_6649" s="T399">видно</ta>
            <ta e="T401" id="Seg_6650" s="T400">что.[NOM]</ta>
            <ta e="T402" id="Seg_6651" s="T401">INDEF</ta>
            <ta e="T403" id="Seg_6652" s="T402">уловка-3SG.[NOM]</ta>
            <ta e="T404" id="Seg_6653" s="T403">есть-3SG</ta>
            <ta e="T405" id="Seg_6654" s="T404">наверное</ta>
            <ta e="T406" id="Seg_6655" s="T405">нельма-ACC</ta>
            <ta e="T407" id="Seg_6656" s="T406">хватать-CVB.SEQ</ta>
            <ta e="T408" id="Seg_6657" s="T407">взять-CVB.SEQ</ta>
            <ta e="T409" id="Seg_6658" s="T408">после</ta>
            <ta e="T410" id="Seg_6659" s="T409">хвост-3SG-ACC</ta>
            <ta e="T411" id="Seg_6660" s="T410">вставлять-PST2.[3SG]</ta>
            <ta e="T412" id="Seg_6661" s="T411">лед.[NOM]</ta>
            <ta e="T413" id="Seg_6662" s="T412">сундук.[NOM]</ta>
            <ta e="T414" id="Seg_6663" s="T413">нутро-3SG-DAT/LOC</ta>
            <ta e="T415" id="Seg_6664" s="T414">нельма.[NOM]</ta>
            <ta e="T416" id="Seg_6665" s="T415">хвост-3SG.[NOM]</ta>
            <ta e="T417" id="Seg_6666" s="T416">совсем</ta>
            <ta e="T418" id="Seg_6667" s="T417">ломать-CVB.SEQ</ta>
            <ta e="T419" id="Seg_6668" s="T418">оставаться-PST2.[3SG]</ta>
            <ta e="T420" id="Seg_6669" s="T419">бисер.[NOM]</ta>
            <ta e="T421" id="Seg_6670" s="T420">борода.[NOM]</ta>
            <ta e="T422" id="Seg_6671" s="T421">дочь-3SG.[NOM]</ta>
            <ta e="T423" id="Seg_6672" s="T422">догадываться-PST1-3SG</ta>
            <ta e="T424" id="Seg_6673" s="T423">почему</ta>
            <ta e="T425" id="Seg_6674" s="T424">вода.[NOM]</ta>
            <ta e="T426" id="Seg_6675" s="T425">господин-3SG.[NOM]</ta>
            <ta e="T427" id="Seg_6676" s="T426">предупреждать-PTCP.PST-3SG-ACC</ta>
            <ta e="T428" id="Seg_6677" s="T427">сундук.[NOM]</ta>
            <ta e="T429" id="Seg_6678" s="T428">место.около-3SG-DAT/LOC</ta>
            <ta e="T430" id="Seg_6679" s="T429">сидеть-CVB.SEQ</ta>
            <ta e="T431" id="Seg_6680" s="T430">после</ta>
            <ta e="T432" id="Seg_6681" s="T431">сеть.[NOM]</ta>
            <ta e="T433" id="Seg_6682" s="T432">латать-EP-MED-CVB.SIM</ta>
            <ta e="T434" id="Seg_6683" s="T433">сидеть-PRS.[3SG]</ta>
            <ta e="T435" id="Seg_6684" s="T434">вода.[NOM]</ta>
            <ta e="T436" id="Seg_6685" s="T435">господин-3SG.[NOM]</ta>
            <ta e="T437" id="Seg_6686" s="T436">приходить-CVB.SEQ</ta>
            <ta e="T438" id="Seg_6687" s="T437">удивляться-PST1-3SG</ta>
            <ta e="T439" id="Seg_6688" s="T438">что</ta>
            <ta e="T440" id="Seg_6689" s="T439">сеть-3SG-ACC</ta>
            <ta e="T441" id="Seg_6690" s="T440">латать-PRS-2SG</ta>
            <ta e="T442" id="Seg_6691" s="T441">вода.[NOM]</ta>
            <ta e="T443" id="Seg_6692" s="T442">нутро-3SG-DAT/LOC</ta>
            <ta e="T444" id="Seg_6693" s="T443">рыба.[NOM]</ta>
            <ta e="T445" id="Seg_6694" s="T444">много</ta>
            <ta e="T446" id="Seg_6695" s="T445">сеть-POSS</ta>
            <ta e="T447" id="Seg_6696" s="T446">да</ta>
            <ta e="T448" id="Seg_6697" s="T447">NEG</ta>
            <ta e="T449" id="Seg_6698" s="T448">1SG.[NOM]</ta>
            <ta e="T450" id="Seg_6699" s="T449">2SG-DAT/LOC</ta>
            <ta e="T451" id="Seg_6700" s="T450">сколько-ACC</ta>
            <ta e="T452" id="Seg_6701" s="T451">думать-PTCP.PRS-ACC</ta>
            <ta e="T453" id="Seg_6702" s="T452">принести-FUT-1SG</ta>
            <ta e="T454" id="Seg_6703" s="T453">рыба.[NOM]</ta>
            <ta e="T455" id="Seg_6704" s="T454">много</ta>
            <ta e="T456" id="Seg_6705" s="T455">быть-CVB.SEQ</ta>
            <ta e="T457" id="Seg_6706" s="T456">после</ta>
            <ta e="T458" id="Seg_6707" s="T457">говорить-PRS.[3SG]</ta>
            <ta e="T459" id="Seg_6708" s="T458">жена-3SG.[NOM]</ta>
            <ta e="T460" id="Seg_6709" s="T459">один</ta>
            <ta e="T461" id="Seg_6710" s="T460">NEG</ta>
            <ta e="T462" id="Seg_6711" s="T461">олень.[NOM]</ta>
            <ta e="T463" id="Seg_6712" s="T462">мясо-3SG.[NOM]</ta>
            <ta e="T464" id="Seg_6713" s="T463">NEG.EX</ta>
            <ta e="T465" id="Seg_6714" s="T464">1SG.[NOM]</ta>
            <ta e="T466" id="Seg_6715" s="T465">олень.[NOM]</ta>
            <ta e="T467" id="Seg_6716" s="T466">мясо-3SG-DAT/LOC</ta>
            <ta e="T468" id="Seg_6717" s="T467">учиться-PST2-1SG</ta>
            <ta e="T469" id="Seg_6718" s="T468">олень.[NOM]</ta>
            <ta e="T470" id="Seg_6719" s="T469">мясо-3SG-ACC</ta>
            <ta e="T471" id="Seg_6720" s="T470">есть-PTCP.FUT-1SG-ACC</ta>
            <ta e="T472" id="Seg_6721" s="T471">хотеть-PRS-1SG</ta>
            <ta e="T473" id="Seg_6722" s="T472">этот</ta>
            <ta e="T474" id="Seg_6723" s="T473">лед.[NOM]</ta>
            <ta e="T475" id="Seg_6724" s="T474">сундук.[NOM]</ta>
            <ta e="T476" id="Seg_6725" s="T475">нутро-3SG-DAT/LOC</ta>
            <ta e="T477" id="Seg_6726" s="T476">олень.[NOM]</ta>
            <ta e="T478" id="Seg_6727" s="T477">много</ta>
            <ta e="T479" id="Seg_6728" s="T478">рыба.[NOM]</ta>
            <ta e="T480" id="Seg_6729" s="T479">подобно</ta>
            <ta e="T481" id="Seg_6730" s="T480">плавать-CVB.SIM</ta>
            <ta e="T482" id="Seg_6731" s="T481">идти-PRS-3PL</ta>
            <ta e="T483" id="Seg_6732" s="T482">вода.[NOM]</ta>
            <ta e="T484" id="Seg_6733" s="T483">господин-3SG.[NOM]</ta>
            <ta e="T485" id="Seg_6734" s="T484">вот</ta>
            <ta e="T486" id="Seg_6735" s="T485">смеяться-PRS.[3SG]</ta>
            <ta e="T487" id="Seg_6736" s="T486">этот</ta>
            <ta e="T488" id="Seg_6737" s="T487">лед.[NOM]</ta>
            <ta e="T489" id="Seg_6738" s="T488">сундук-DAT/LOC</ta>
            <ta e="T490" id="Seg_6739" s="T489">что.[NOM]</ta>
            <ta e="T491" id="Seg_6740" s="T490">олень-3SG.[NOM]</ta>
            <ta e="T492" id="Seg_6741" s="T491">приходить-FUT.[3SG]=Q</ta>
            <ta e="T493" id="Seg_6742" s="T492">здесь</ta>
            <ta e="T494" id="Seg_6743" s="T493">вода.[NOM]</ta>
            <ta e="T495" id="Seg_6744" s="T494">только</ta>
            <ta e="T496" id="Seg_6745" s="T495">1SG.[NOM]</ta>
            <ta e="T497" id="Seg_6746" s="T496">знать-PRS-1SG</ta>
            <ta e="T498" id="Seg_6747" s="T497">EVID</ta>
            <ta e="T499" id="Seg_6748" s="T498">жена-3SG.[NOM]</ta>
            <ta e="T500" id="Seg_6749" s="T499">говорить-PST2.[3SG]</ta>
            <ta e="T501" id="Seg_6750" s="T500">1SG.[NOM]</ta>
            <ta e="T502" id="Seg_6751" s="T501">видеть-PST2-EP-1SG</ta>
            <ta e="T503" id="Seg_6752" s="T502">олень-PL.[NOM]</ta>
            <ta e="T504" id="Seg_6753" s="T503">плавать-CVB.SIM</ta>
            <ta e="T505" id="Seg_6754" s="T504">идти-PRS-3PL</ta>
            <ta e="T506" id="Seg_6755" s="T505">некоторый-PL.[NOM]</ta>
            <ta e="T507" id="Seg_6756" s="T506">белый-PL.[NOM]</ta>
            <ta e="T508" id="Seg_6757" s="T507">некоторый-PL.[NOM]</ta>
            <ta e="T509" id="Seg_6758" s="T508">пятнистый-PL.[NOM]</ta>
            <ta e="T510" id="Seg_6759" s="T509">другой.из.двух-3PL.[NOM]</ta>
            <ta e="T511" id="Seg_6760" s="T510">черный-PL.[NOM]</ta>
            <ta e="T512" id="Seg_6761" s="T511">целый</ta>
            <ta e="T513" id="Seg_6762" s="T512">стадо.[NOM]</ta>
            <ta e="T514" id="Seg_6763" s="T513">глупый</ta>
            <ta e="T515" id="Seg_6764" s="T514">жена-2SG</ta>
            <ta e="T516" id="Seg_6765" s="T515">быть-PST2.[3SG]</ta>
            <ta e="T517" id="Seg_6766" s="T516">рассердиться-PST1-3SG</ta>
            <ta e="T518" id="Seg_6767" s="T517">вода.[NOM]</ta>
            <ta e="T519" id="Seg_6768" s="T518">господин-3SG.[NOM]</ta>
            <ta e="T520" id="Seg_6769" s="T519">что.[NOM]</ta>
            <ta e="T521" id="Seg_6770" s="T520">олень-3SG.[NOM]</ta>
            <ta e="T522" id="Seg_6771" s="T521">вода.[NOM]</ta>
            <ta e="T523" id="Seg_6772" s="T522">нутро-3SG-DAT/LOC</ta>
            <ta e="T524" id="Seg_6773" s="T523">идти-FUT.[3SG]=Q</ta>
            <ta e="T525" id="Seg_6774" s="T524">сам-1SG.[NOM]</ta>
            <ta e="T526" id="Seg_6775" s="T525">знать-PRS-2SG</ta>
            <ta e="T527" id="Seg_6776" s="T526">1PL.[NOM]</ta>
            <ta e="T528" id="Seg_6777" s="T527">вода.[NOM]</ta>
            <ta e="T529" id="Seg_6778" s="T528">нутро-3SG-DAT/LOC</ta>
            <ta e="T530" id="Seg_6779" s="T529">жить-PRS-1PL</ta>
            <ta e="T531" id="Seg_6780" s="T530">тот.[NOM]</ta>
            <ta e="T532" id="Seg_6781" s="T531">из_за</ta>
            <ta e="T533" id="Seg_6782" s="T532">олень-PL.[NOM]</ta>
            <ta e="T534" id="Seg_6783" s="T533">тоже</ta>
            <ta e="T535" id="Seg_6784" s="T534">вода.[NOM]</ta>
            <ta e="T536" id="Seg_6785" s="T535">нутро-3SG-DAT/LOC</ta>
            <ta e="T537" id="Seg_6786" s="T536">идти-PRS-3PL</ta>
            <ta e="T538" id="Seg_6787" s="T537">говорить-PST2.[3SG]</ta>
            <ta e="T539" id="Seg_6788" s="T538">жена-3SG.[NOM]</ta>
            <ta e="T540" id="Seg_6789" s="T539">видеть.[IMP.2SG]</ta>
            <ta e="T541" id="Seg_6790" s="T540">вот</ta>
            <ta e="T542" id="Seg_6791" s="T541">сундук.[NOM]</ta>
            <ta e="T543" id="Seg_6792" s="T542">нутро-3SG-ACC</ta>
            <ta e="T544" id="Seg_6793" s="T543">вода.[NOM]</ta>
            <ta e="T545" id="Seg_6794" s="T544">господин-3SG.[NOM]</ta>
            <ta e="T546" id="Seg_6795" s="T545">рассердиться-CVB.SEQ</ta>
            <ta e="T547" id="Seg_6796" s="T546">после</ta>
            <ta e="T548" id="Seg_6797" s="T547">забывать-CVB.SEQ</ta>
            <ta e="T549" id="Seg_6798" s="T548">бросать-PST2.[3SG]</ta>
            <ta e="T550" id="Seg_6799" s="T549">сундук-3SG-DAT/LOC</ta>
            <ta e="T551" id="Seg_6800" s="T550">загладывать-PST2.[3SG]</ta>
            <ta e="T552" id="Seg_6801" s="T551">олень-PL-ACC</ta>
            <ta e="T553" id="Seg_6802" s="T552">видеть-CVB.PURP</ta>
            <ta e="T554" id="Seg_6803" s="T553">жена-3SG.[NOM]</ta>
            <ta e="T555" id="Seg_6804" s="T554">приходить-CVB.SEQ</ta>
            <ta e="T556" id="Seg_6805" s="T555">голова-3SG-ACC</ta>
            <ta e="T557" id="Seg_6806" s="T556">вода.[NOM]</ta>
            <ta e="T558" id="Seg_6807" s="T557">нутро-3SG-DAT/LOC</ta>
            <ta e="T559" id="Seg_6808" s="T558">разрушать-ADVZ</ta>
            <ta e="T560" id="Seg_6809" s="T559">перевешивать-PST2.[3SG]</ta>
            <ta e="T561" id="Seg_6810" s="T560">вода.[NOM]</ta>
            <ta e="T562" id="Seg_6811" s="T561">господин-3SG-GEN</ta>
            <ta e="T563" id="Seg_6812" s="T562">голова-3SG.[NOM]</ta>
            <ta e="T564" id="Seg_6813" s="T563">совсем</ta>
            <ta e="T565" id="Seg_6814" s="T564">идти-PST2.[3SG]</ta>
            <ta e="T566" id="Seg_6815" s="T565">сразу</ta>
            <ta e="T567" id="Seg_6816" s="T566">умирать-PST2.[3SG]</ta>
            <ta e="T568" id="Seg_6817" s="T567">бисер.[NOM]</ta>
            <ta e="T569" id="Seg_6818" s="T568">борода.[NOM]</ta>
            <ta e="T570" id="Seg_6819" s="T569">маленький</ta>
            <ta e="T571" id="Seg_6820" s="T570">дочь-3SG.[NOM]</ta>
            <ta e="T572" id="Seg_6821" s="T571">дверь-ABL</ta>
            <ta e="T573" id="Seg_6822" s="T572">дверь-ACC</ta>
            <ta e="T574" id="Seg_6823" s="T573">обходить-CVB.SIM</ta>
            <ta e="T575" id="Seg_6824" s="T574">идти-CVB.SEQ</ta>
            <ta e="T576" id="Seg_6825" s="T575">старшая.сестра-PL-3SG-ACC</ta>
            <ta e="T577" id="Seg_6826" s="T576">найти-PST2.[3SG]</ta>
            <ta e="T578" id="Seg_6827" s="T577">дверь-3PL-GEN</ta>
            <ta e="T579" id="Seg_6828" s="T578">клей-3PL-ACC</ta>
            <ta e="T580" id="Seg_6829" s="T579">нож-EP-INSTR</ta>
            <ta e="T581" id="Seg_6830" s="T580">скрести-CVB.SEQ</ta>
            <ta e="T582" id="Seg_6831" s="T581">взять-CVB.SEQ</ta>
            <ta e="T583" id="Seg_6832" s="T582">освободить-PST2.[3SG]</ta>
            <ta e="T584" id="Seg_6833" s="T583">три-COLL-3PL.[NOM]</ta>
            <ta e="T585" id="Seg_6834" s="T584">шест.[NOM]</ta>
            <ta e="T586" id="Seg_6835" s="T585">чум-3PL-DAT/LOC</ta>
            <ta e="T587" id="Seg_6836" s="T586">приходить-PST2-3PL</ta>
            <ta e="T588" id="Seg_6837" s="T587">бисер.[NOM]</ta>
            <ta e="T589" id="Seg_6838" s="T588">борода.[NOM]</ta>
            <ta e="T590" id="Seg_6839" s="T589">радоваться-CVB.SEQ</ta>
            <ta e="T591" id="Seg_6840" s="T590">дочь-PL-3SG-DAT/LOC</ta>
            <ta e="T592" id="Seg_6841" s="T591">строгать-PTCP.PST.[NOM]</ta>
            <ta e="T593" id="Seg_6842" s="T592">строгать-PRS.[3SG]</ta>
            <ta e="T594" id="Seg_6843" s="T593">вариться-PTCP.PST</ta>
            <ta e="T595" id="Seg_6844" s="T594">рыба-ACC</ta>
            <ta e="T596" id="Seg_6845" s="T595">есть-EP-CAUS-PRS.[3SG]</ta>
            <ta e="T597" id="Seg_6846" s="T596">большой</ta>
            <ta e="T598" id="Seg_6847" s="T597">дочь-3SG.[NOM]</ta>
            <ta e="T599" id="Seg_6848" s="T598">господин.[NOM]</ta>
            <ta e="T600" id="Seg_6849" s="T599">палец-3SG-ACC</ta>
            <ta e="T601" id="Seg_6850" s="T600">видеть-CVB.SEQ</ta>
            <ta e="T602" id="Seg_6851" s="T601">тот-DAT/LOC</ta>
            <ta e="T603" id="Seg_6852" s="T602">средний</ta>
            <ta e="T604" id="Seg_6853" s="T603">дочь-3SG.[NOM]</ta>
            <ta e="T605" id="Seg_6854" s="T604">середина-ADJZ</ta>
            <ta e="T606" id="Seg_6855" s="T605">палец-3SG-ACC</ta>
            <ta e="T607" id="Seg_6856" s="T606">видеть-CVB.SEQ</ta>
            <ta e="T608" id="Seg_6857" s="T607">тогда</ta>
            <ta e="T609" id="Seg_6858" s="T608">только</ta>
            <ta e="T610" id="Seg_6859" s="T609">вода.[NOM]</ta>
            <ta e="T611" id="Seg_6860" s="T610">господин-3SG-ACC</ta>
            <ta e="T612" id="Seg_6861" s="T611">помнить-PRS-3PL</ta>
            <ta e="T613" id="Seg_6862" s="T612">сказка-VBZ-PST2-3SG</ta>
            <ta e="T614" id="Seg_6863" s="T613">Огдуо</ta>
            <ta e="T615" id="Seg_6864" s="T614">Аксенова.[NOM]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_6865" s="T0">v-v:mood.pn</ta>
            <ta e="T2" id="Seg_6866" s="T1">n-n:case</ta>
            <ta e="T3" id="Seg_6867" s="T2">n-n:(poss)-n:case</ta>
            <ta e="T4" id="Seg_6868" s="T3">n-n:case</ta>
            <ta e="T5" id="Seg_6869" s="T4">n-n:case</ta>
            <ta e="T6" id="Seg_6870" s="T5">cardnum</ta>
            <ta e="T7" id="Seg_6871" s="T6">n-n:case</ta>
            <ta e="T8" id="Seg_6872" s="T7">ptcl</ta>
            <ta e="T9" id="Seg_6873" s="T8">v-v:tense-v:pred.pn</ta>
            <ta e="T10" id="Seg_6874" s="T9">n-n:case</ta>
            <ta e="T11" id="Seg_6875" s="T10">n-n:(poss)-n:case</ta>
            <ta e="T12" id="Seg_6876" s="T11">n-n&gt;adj-n:case</ta>
            <ta e="T13" id="Seg_6877" s="T12">v-v:cvb</ta>
            <ta e="T14" id="Seg_6878" s="T13">post</ta>
            <ta e="T15" id="Seg_6879" s="T14">adj-n:case</ta>
            <ta e="T16" id="Seg_6880" s="T15">ptcl</ta>
            <ta e="T17" id="Seg_6881" s="T16">n-n:(num)-n:(poss)-n:case</ta>
            <ta e="T18" id="Seg_6882" s="T17">n-n&gt;adj</ta>
            <ta e="T19" id="Seg_6883" s="T18">n-n:case</ta>
            <ta e="T20" id="Seg_6884" s="T19">post</ta>
            <ta e="T21" id="Seg_6885" s="T20">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T22" id="Seg_6886" s="T21">dempro-pro:case</ta>
            <ta e="T23" id="Seg_6887" s="T22">post</ta>
            <ta e="T24" id="Seg_6888" s="T23">n-n:case</ta>
            <ta e="T25" id="Seg_6889" s="T24">n-n&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T26" id="Seg_6890" s="T25">n-n:case</ta>
            <ta e="T27" id="Seg_6891" s="T26">n-n:case</ta>
            <ta e="T28" id="Seg_6892" s="T27">v-v:cvb</ta>
            <ta e="T29" id="Seg_6893" s="T28">n-n:case</ta>
            <ta e="T30" id="Seg_6894" s="T29">n-n:case</ta>
            <ta e="T31" id="Seg_6895" s="T30">cardnum</ta>
            <ta e="T32" id="Seg_6896" s="T31">n-n&gt;adj-n:case</ta>
            <ta e="T33" id="Seg_6897" s="T32">adv</ta>
            <ta e="T34" id="Seg_6898" s="T33">adv</ta>
            <ta e="T35" id="Seg_6899" s="T34">n-n&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T36" id="Seg_6900" s="T35">n-n:(ins)-n:case</ta>
            <ta e="T37" id="Seg_6901" s="T36">ptcl</ta>
            <ta e="T38" id="Seg_6902" s="T37">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T39" id="Seg_6903" s="T38">v-v:tense-v:pred.pn</ta>
            <ta e="T40" id="Seg_6904" s="T39">adv</ta>
            <ta e="T41" id="Seg_6905" s="T40">n-n:case</ta>
            <ta e="T42" id="Seg_6906" s="T41">n-n:case</ta>
            <ta e="T43" id="Seg_6907" s="T42">n-n:case</ta>
            <ta e="T44" id="Seg_6908" s="T43">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T45" id="Seg_6909" s="T44">v-v:tense-v:pred.pn</ta>
            <ta e="T46" id="Seg_6910" s="T45">n-n:poss-n:case</ta>
            <ta e="T47" id="Seg_6911" s="T46">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T48" id="Seg_6912" s="T47">post</ta>
            <ta e="T49" id="Seg_6913" s="T48">que-pro:case</ta>
            <ta e="T50" id="Seg_6914" s="T49">ptcl</ta>
            <ta e="T51" id="Seg_6915" s="T50">n-n:poss-n:case</ta>
            <ta e="T52" id="Seg_6916" s="T51">v-v:cvb</ta>
            <ta e="T53" id="Seg_6917" s="T52">v-v:tense-v:pred.pn</ta>
            <ta e="T54" id="Seg_6918" s="T53">adv</ta>
            <ta e="T55" id="Seg_6919" s="T54">v-v:(neg)-v:pred.pn</ta>
            <ta e="T56" id="Seg_6920" s="T55">que-pro:case</ta>
            <ta e="T57" id="Seg_6921" s="T56">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T58" id="Seg_6922" s="T57">v-v:cvb</ta>
            <ta e="T59" id="Seg_6923" s="T58">v-v:tense-v:pred.pn</ta>
            <ta e="T60" id="Seg_6924" s="T59">n-n:case</ta>
            <ta e="T61" id="Seg_6925" s="T60">v-v:tense-v:pred.pn</ta>
            <ta e="T62" id="Seg_6926" s="T61">n-n:case</ta>
            <ta e="T63" id="Seg_6927" s="T62">n-n:(poss)-n:case</ta>
            <ta e="T64" id="Seg_6928" s="T63">n-n:case</ta>
            <ta e="T65" id="Seg_6929" s="T64">n-n:case</ta>
            <ta e="T66" id="Seg_6930" s="T65">v-v:tense-v:pred.pn</ta>
            <ta e="T67" id="Seg_6931" s="T66">n-n:poss-n:case</ta>
            <ta e="T68" id="Seg_6932" s="T67">v-v:tense-v:pred.pn</ta>
            <ta e="T69" id="Seg_6933" s="T68">ptcl</ta>
            <ta e="T70" id="Seg_6934" s="T69">ptcl</ta>
            <ta e="T71" id="Seg_6935" s="T70">ptcl</ta>
            <ta e="T72" id="Seg_6936" s="T71">n-n:case</ta>
            <ta e="T73" id="Seg_6937" s="T72">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T74" id="Seg_6938" s="T73">v-v:(neg)-v:mood-v:temp.pn</ta>
            <ta e="T75" id="Seg_6939" s="T74">cardnum</ta>
            <ta e="T76" id="Seg_6940" s="T75">ptcl</ta>
            <ta e="T77" id="Seg_6941" s="T76">n-n:case</ta>
            <ta e="T78" id="Seg_6942" s="T77">v-v:tense-v:poss.pn</ta>
            <ta e="T79" id="Seg_6943" s="T78">ptcl-ptcl:(poss.pn)</ta>
            <ta e="T80" id="Seg_6944" s="T79">que-pro:(ins)-pro:case</ta>
            <ta e="T81" id="Seg_6945" s="T80">n-n:(num)-n:poss-n:case</ta>
            <ta e="T82" id="Seg_6946" s="T81">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T83" id="Seg_6947" s="T82">ptcl</ta>
            <ta e="T84" id="Seg_6948" s="T83">v-v:cvb</ta>
            <ta e="T85" id="Seg_6949" s="T84">v-v:tense-v:pred.pn</ta>
            <ta e="T86" id="Seg_6950" s="T85">adv</ta>
            <ta e="T87" id="Seg_6951" s="T86">v-v:tense-v:pred.pn</ta>
            <ta e="T88" id="Seg_6952" s="T87">n-n&gt;v-v:mood-v:temp.pn</ta>
            <ta e="T89" id="Seg_6953" s="T88">ptcl</ta>
            <ta e="T90" id="Seg_6954" s="T89">ptcl</ta>
            <ta e="T91" id="Seg_6955" s="T90">v-v:tense-v:poss.pn</ta>
            <ta e="T92" id="Seg_6956" s="T91">v-v:tense-v:pred.pn</ta>
            <ta e="T93" id="Seg_6957" s="T92">n-n:case</ta>
            <ta e="T94" id="Seg_6958" s="T93">n-n:(poss)-n:case</ta>
            <ta e="T95" id="Seg_6959" s="T94">n-n:case</ta>
            <ta e="T96" id="Seg_6960" s="T95">n-n:poss-n:case</ta>
            <ta e="T97" id="Seg_6961" s="T96">v-v:cvb</ta>
            <ta e="T98" id="Seg_6962" s="T97">v-v:cvb</ta>
            <ta e="T99" id="Seg_6963" s="T98">post</ta>
            <ta e="T100" id="Seg_6964" s="T99">n-n:case</ta>
            <ta e="T101" id="Seg_6965" s="T100">n-n:poss-n:case</ta>
            <ta e="T102" id="Seg_6966" s="T101">v-v:cvb</ta>
            <ta e="T103" id="Seg_6967" s="T102">v-v:tense-v:pred.pn</ta>
            <ta e="T104" id="Seg_6968" s="T103">n-n:case</ta>
            <ta e="T105" id="Seg_6969" s="T104">n-n:case</ta>
            <ta e="T106" id="Seg_6970" s="T105">adv</ta>
            <ta e="T107" id="Seg_6971" s="T106">v-v:tense-v:pred.pn</ta>
            <ta e="T108" id="Seg_6972" s="T107">n-n:case</ta>
            <ta e="T109" id="Seg_6973" s="T108">n-n:poss-n:case</ta>
            <ta e="T110" id="Seg_6974" s="T109">v-v:cvb</ta>
            <ta e="T111" id="Seg_6975" s="T110">n-n:(num)-n:poss-n:case</ta>
            <ta e="T112" id="Seg_6976" s="T111">v-v:tense-v:pred.pn</ta>
            <ta e="T113" id="Seg_6977" s="T112">adj</ta>
            <ta e="T114" id="Seg_6978" s="T113">n-n:(poss)-n:case</ta>
            <ta e="T115" id="Seg_6979" s="T114">v-v:tense-v:pred.pn</ta>
            <ta e="T116" id="Seg_6980" s="T115">pers-pro:case</ta>
            <ta e="T117" id="Seg_6981" s="T116">n-n:case</ta>
            <ta e="T118" id="Seg_6982" s="T117">v-v:tense-v:poss.pn</ta>
            <ta e="T119" id="Seg_6983" s="T118">n-n:case</ta>
            <ta e="T120" id="Seg_6984" s="T119">n-n:poss-n:case</ta>
            <ta e="T121" id="Seg_6985" s="T120">n-n:case</ta>
            <ta e="T122" id="Seg_6986" s="T121">n-n:poss-n:case</ta>
            <ta e="T123" id="Seg_6987" s="T122">ptcl</ta>
            <ta e="T124" id="Seg_6988" s="T123">adj-n:case</ta>
            <ta e="T125" id="Seg_6989" s="T124">v-v:tense-v:poss.pn</ta>
            <ta e="T126" id="Seg_6990" s="T125">n-n:case</ta>
            <ta e="T127" id="Seg_6991" s="T126">n-n:case</ta>
            <ta e="T128" id="Seg_6992" s="T127">n-n:poss-n:case</ta>
            <ta e="T129" id="Seg_6993" s="T128">n-n:case</ta>
            <ta e="T130" id="Seg_6994" s="T129">v-v:cvb</ta>
            <ta e="T131" id="Seg_6995" s="T130">v-v:tense-v:pred.pn</ta>
            <ta e="T132" id="Seg_6996" s="T131">n-n:case</ta>
            <ta e="T133" id="Seg_6997" s="T132">n-n:(poss)-n:case</ta>
            <ta e="T134" id="Seg_6998" s="T133">adj-n:case</ta>
            <ta e="T135" id="Seg_6999" s="T134">conj</ta>
            <ta e="T136" id="Seg_7000" s="T135">adj-n:case</ta>
            <ta e="T137" id="Seg_7001" s="T136">n-n:poss-n:case</ta>
            <ta e="T138" id="Seg_7002" s="T137">n-n:(poss)-n:case</ta>
            <ta e="T139" id="Seg_7003" s="T138">v-v:(ins)-v:(neg)-v:pred.pn</ta>
            <ta e="T140" id="Seg_7004" s="T139">que</ta>
            <ta e="T141" id="Seg_7005" s="T140">ptcl</ta>
            <ta e="T142" id="Seg_7006" s="T141">n-n&gt;adj-n:case</ta>
            <ta e="T143" id="Seg_7007" s="T142">n-n&gt;adj-n:case</ta>
            <ta e="T144" id="Seg_7008" s="T143">cardnum</ta>
            <ta e="T145" id="Seg_7009" s="T144">n-n:case</ta>
            <ta e="T146" id="Seg_7010" s="T145">adj</ta>
            <ta e="T147" id="Seg_7011" s="T146">n-n:case</ta>
            <ta e="T148" id="Seg_7012" s="T147">n-n:case</ta>
            <ta e="T149" id="Seg_7013" s="T148">v-v:tense-v:pred.pn</ta>
            <ta e="T150" id="Seg_7014" s="T149">n-n:case</ta>
            <ta e="T151" id="Seg_7015" s="T150">n-n:(poss)-n:case</ta>
            <ta e="T152" id="Seg_7016" s="T151">n-n:poss-n:case</ta>
            <ta e="T153" id="Seg_7017" s="T152">v-v:cvb</ta>
            <ta e="T154" id="Seg_7018" s="T153">n-n:case</ta>
            <ta e="T155" id="Seg_7019" s="T154">n-n:case</ta>
            <ta e="T156" id="Seg_7020" s="T155">v-v:tense-v:pred.pn</ta>
            <ta e="T157" id="Seg_7021" s="T156">que</ta>
            <ta e="T158" id="Seg_7022" s="T157">v-v:ptcp</ta>
            <ta e="T159" id="Seg_7023" s="T158">v-v:mood.pn</ta>
            <ta e="T160" id="Seg_7024" s="T159">v-v:mood.pn</ta>
            <ta e="T161" id="Seg_7025" s="T160">dempro-n:case</ta>
            <ta e="T162" id="Seg_7026" s="T161">ptcl</ta>
            <ta e="T164" id="Seg_7027" s="T163">n-n:case</ta>
            <ta e="T165" id="Seg_7028" s="T164">n-n:poss-n:case</ta>
            <ta e="T166" id="Seg_7029" s="T165">v-v:(ins)-v:(neg)-v:mood.pn</ta>
            <ta e="T167" id="Seg_7030" s="T166">n-n:poss-n:case</ta>
            <ta e="T168" id="Seg_7031" s="T167">v-v:tense-v:pred.pn</ta>
            <ta e="T169" id="Seg_7032" s="T168">n-n:(poss)-n:case</ta>
            <ta e="T170" id="Seg_7033" s="T169">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T171" id="Seg_7034" s="T170">post</ta>
            <ta e="T172" id="Seg_7035" s="T171">dempro</ta>
            <ta e="T173" id="Seg_7036" s="T172">n-n:case</ta>
            <ta e="T174" id="Seg_7037" s="T173">v-v:cvb</ta>
            <ta e="T175" id="Seg_7038" s="T174">n-n:poss-n:case</ta>
            <ta e="T176" id="Seg_7039" s="T175">v-v:tense-v:pred.pn</ta>
            <ta e="T177" id="Seg_7040" s="T176">n-n:case</ta>
            <ta e="T178" id="Seg_7041" s="T177">n-n:case</ta>
            <ta e="T179" id="Seg_7042" s="T178">v-v:tense-v:pred.pn</ta>
            <ta e="T180" id="Seg_7043" s="T179">adv</ta>
            <ta e="T181" id="Seg_7044" s="T180">ptcl</ta>
            <ta e="T182" id="Seg_7045" s="T181">v-v:tense-v:pred.pn</ta>
            <ta e="T183" id="Seg_7046" s="T182">n-n:poss-n:case</ta>
            <ta e="T184" id="Seg_7047" s="T183">v-v:tense-v:poss.pn</ta>
            <ta e="T185" id="Seg_7048" s="T184">n-n:case</ta>
            <ta e="T186" id="Seg_7049" s="T185">n-n:(poss)-n:case</ta>
            <ta e="T187" id="Seg_7050" s="T186">ptcl</ta>
            <ta e="T188" id="Seg_7051" s="T187">v-v:tense-v:pred.pn</ta>
            <ta e="T189" id="Seg_7052" s="T188">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T190" id="Seg_7053" s="T189">adv</ta>
            <ta e="T191" id="Seg_7054" s="T190">v-v:cvb</ta>
            <ta e="T192" id="Seg_7055" s="T191">post</ta>
            <ta e="T193" id="Seg_7056" s="T192">v-v:tense-v:pred.pn</ta>
            <ta e="T194" id="Seg_7057" s="T193">v-v:cvb</ta>
            <ta e="T195" id="Seg_7058" s="T194">n-n:(num)-n:(poss)-n:case</ta>
            <ta e="T196" id="Seg_7059" s="T195">v-v&gt;adv</ta>
            <ta e="T197" id="Seg_7060" s="T196">v-v:tense-v:pred.pn</ta>
            <ta e="T198" id="Seg_7061" s="T197">n-n:case</ta>
            <ta e="T199" id="Seg_7062" s="T198">n-n:(poss)-n:case</ta>
            <ta e="T200" id="Seg_7063" s="T199">v-v:cvb</ta>
            <ta e="T201" id="Seg_7064" s="T200">adv</ta>
            <ta e="T202" id="Seg_7065" s="T201">v-v:tense-v:poss.pn</ta>
            <ta e="T203" id="Seg_7066" s="T202">pers-pro:case</ta>
            <ta e="T204" id="Seg_7067" s="T203">pers-pro:case</ta>
            <ta e="T205" id="Seg_7068" s="T204">n-n:case</ta>
            <ta e="T206" id="Seg_7069" s="T205">v-v:ptcp-v:(case)</ta>
            <ta e="T207" id="Seg_7070" s="T206">post-n:(pred.pn)</ta>
            <ta e="T208" id="Seg_7071" s="T207">v-v:tense-v:pred.pn</ta>
            <ta e="T209" id="Seg_7072" s="T208">n-n:case</ta>
            <ta e="T210" id="Seg_7073" s="T209">ptcl</ta>
            <ta e="T211" id="Seg_7074" s="T210">adj-adj&gt;adj</ta>
            <ta e="T212" id="Seg_7075" s="T211">n-n:case</ta>
            <ta e="T213" id="Seg_7076" s="T212">n-n:poss-n:case</ta>
            <ta e="T214" id="Seg_7077" s="T213">v-v:cvb</ta>
            <ta e="T215" id="Seg_7078" s="T214">v-v:cvb</ta>
            <ta e="T216" id="Seg_7079" s="T215">v-v:tense-v:pred.pn</ta>
            <ta e="T217" id="Seg_7080" s="T216">n-n:poss-n:case</ta>
            <ta e="T218" id="Seg_7081" s="T217">adv</ta>
            <ta e="T219" id="Seg_7082" s="T218">n-n:case</ta>
            <ta e="T220" id="Seg_7083" s="T219">n-n:poss-n:case</ta>
            <ta e="T221" id="Seg_7084" s="T220">v-v:(ins)-v:ptcp</ta>
            <ta e="T222" id="Seg_7085" s="T221">n-n:(ins)-n:case</ta>
            <ta e="T223" id="Seg_7086" s="T222">adv</ta>
            <ta e="T224" id="Seg_7087" s="T223">v-v:tense-v:pred.pn</ta>
            <ta e="T225" id="Seg_7088" s="T224">n-n:case</ta>
            <ta e="T226" id="Seg_7089" s="T225">n-n:case</ta>
            <ta e="T227" id="Seg_7090" s="T226">ptcl</ta>
            <ta e="T228" id="Seg_7091" s="T227">n-n&gt;v-v:cvb</ta>
            <ta e="T229" id="Seg_7092" s="T228">v-v:tense-v:pred.pn</ta>
            <ta e="T230" id="Seg_7093" s="T229">n-n:poss-n:case</ta>
            <ta e="T231" id="Seg_7094" s="T230">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T232" id="Seg_7095" s="T231">post</ta>
            <ta e="T233" id="Seg_7096" s="T232">n-n:case</ta>
            <ta e="T234" id="Seg_7097" s="T233">n-n:(poss)-n:case</ta>
            <ta e="T235" id="Seg_7098" s="T234">ptcl</ta>
            <ta e="T236" id="Seg_7099" s="T235">n-n:poss-n:case</ta>
            <ta e="T237" id="Seg_7100" s="T236">v-v:cvb</ta>
            <ta e="T238" id="Seg_7101" s="T237">v-v:tense-v:pred.pn</ta>
            <ta e="T239" id="Seg_7102" s="T238">v-v:mood.pn</ta>
            <ta e="T240" id="Seg_7103" s="T239">cardnum-cardnum&gt;ordnum</ta>
            <ta e="T241" id="Seg_7104" s="T240">n-n:poss-n:case</ta>
            <ta e="T242" id="Seg_7105" s="T241">n-n:case</ta>
            <ta e="T243" id="Seg_7106" s="T242">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T244" id="Seg_7107" s="T243">n-n:case</ta>
            <ta e="T245" id="Seg_7108" s="T244">n-n:case</ta>
            <ta e="T246" id="Seg_7109" s="T245">v-v:tense-v:poss.pn</ta>
            <ta e="T247" id="Seg_7110" s="T246">ptcl</ta>
            <ta e="T248" id="Seg_7111" s="T247">adj</ta>
            <ta e="T249" id="Seg_7112" s="T248">n-n:poss-n:case</ta>
            <ta e="T250" id="Seg_7113" s="T249">v-v:tense-v:pred.pn</ta>
            <ta e="T251" id="Seg_7114" s="T250">dempro</ta>
            <ta e="T252" id="Seg_7115" s="T251">n-n:case</ta>
            <ta e="T253" id="Seg_7116" s="T252">n-n:case</ta>
            <ta e="T254" id="Seg_7117" s="T253">v-v:(ins)-v&gt;v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T255" id="Seg_7118" s="T254">n-n:(poss)</ta>
            <ta e="T256" id="Seg_7119" s="T255">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T257" id="Seg_7120" s="T256">v-v:cvb</ta>
            <ta e="T258" id="Seg_7121" s="T257">post</ta>
            <ta e="T259" id="Seg_7122" s="T258">v-v:(neg)-v:pred.pn</ta>
            <ta e="T260" id="Seg_7123" s="T259">adv</ta>
            <ta e="T261" id="Seg_7124" s="T260">dempro</ta>
            <ta e="T262" id="Seg_7125" s="T261">n-n:case</ta>
            <ta e="T263" id="Seg_7126" s="T262">n-n:case</ta>
            <ta e="T264" id="Seg_7127" s="T263">v-v:tense-v:pred.pn</ta>
            <ta e="T265" id="Seg_7128" s="T264">n-n:case</ta>
            <ta e="T266" id="Seg_7129" s="T265">n-n:(poss)-n:case</ta>
            <ta e="T267" id="Seg_7130" s="T266">v-v:cvb</ta>
            <ta e="T268" id="Seg_7131" s="T267">v-v:tense-v:pred.pn</ta>
            <ta e="T269" id="Seg_7132" s="T268">dempro</ta>
            <ta e="T270" id="Seg_7133" s="T269">n-n:case</ta>
            <ta e="T271" id="Seg_7134" s="T270">n-n:poss-n:case</ta>
            <ta e="T272" id="Seg_7135" s="T271">ptcl</ta>
            <ta e="T273" id="Seg_7136" s="T272">n-n:poss-n:case</ta>
            <ta e="T274" id="Seg_7137" s="T273">ptcl</ta>
            <ta e="T275" id="Seg_7138" s="T274">v-v:mood-v:pred.pn</ta>
            <ta e="T276" id="Seg_7139" s="T275">n-n:case</ta>
            <ta e="T277" id="Seg_7140" s="T276">n-n:(poss)-n:case</ta>
            <ta e="T278" id="Seg_7141" s="T277">n-n&gt;v-v:cvb</ta>
            <ta e="T279" id="Seg_7142" s="T278">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T280" id="Seg_7143" s="T279">post</ta>
            <ta e="T281" id="Seg_7144" s="T280">n-n:(poss)-n:case</ta>
            <ta e="T282" id="Seg_7145" s="T281">n-n:case</ta>
            <ta e="T283" id="Seg_7146" s="T282">n-n:case</ta>
            <ta e="T284" id="Seg_7147" s="T283">v-v:cvb</ta>
            <ta e="T285" id="Seg_7148" s="T284">n-n:poss-n:case</ta>
            <ta e="T286" id="Seg_7149" s="T285">v-v:tense-v:pred.pn</ta>
            <ta e="T287" id="Seg_7150" s="T286">n-n&gt;adj</ta>
            <ta e="T288" id="Seg_7151" s="T287">n-n:(poss)-n:case</ta>
            <ta e="T289" id="Seg_7152" s="T288">ptcl</ta>
            <ta e="T290" id="Seg_7153" s="T289">v-v:tense-v:pred.pn</ta>
            <ta e="T291" id="Seg_7154" s="T290">n-n:poss-n:case</ta>
            <ta e="T292" id="Seg_7155" s="T291">adv</ta>
            <ta e="T293" id="Seg_7156" s="T292">v-v:tense-v:pred.pn</ta>
            <ta e="T294" id="Seg_7157" s="T293">n-n:case</ta>
            <ta e="T295" id="Seg_7158" s="T294">n-n:(poss)-n:case</ta>
            <ta e="T296" id="Seg_7159" s="T295">v-v:cvb</ta>
            <ta e="T297" id="Seg_7160" s="T296">adv</ta>
            <ta e="T298" id="Seg_7161" s="T297">v-v:tense-v:pred.pn</ta>
            <ta e="T299" id="Seg_7162" s="T298">pers-pro:case</ta>
            <ta e="T300" id="Seg_7163" s="T299">adj-n:(poss)</ta>
            <ta e="T301" id="Seg_7164" s="T300">ptcl</ta>
            <ta e="T302" id="Seg_7165" s="T301">n-n:case</ta>
            <ta e="T303" id="Seg_7166" s="T302">n-n:(poss)</ta>
            <ta e="T304" id="Seg_7167" s="T303">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T305" id="Seg_7168" s="T304">v-v:cvb</ta>
            <ta e="T306" id="Seg_7169" s="T305">post</ta>
            <ta e="T307" id="Seg_7170" s="T306">adj-adj&gt;adj</ta>
            <ta e="T308" id="Seg_7171" s="T307">n-n:case</ta>
            <ta e="T309" id="Seg_7172" s="T308">n-n:poss-n:case</ta>
            <ta e="T310" id="Seg_7173" s="T309">v-v:cvb</ta>
            <ta e="T311" id="Seg_7174" s="T310">v-v:tense-v:pred.pn</ta>
            <ta e="T312" id="Seg_7175" s="T311">n-n:poss-n:case</ta>
            <ta e="T313" id="Seg_7176" s="T312">adv</ta>
            <ta e="T314" id="Seg_7177" s="T313">n-n:case</ta>
            <ta e="T315" id="Seg_7178" s="T314">n-n:poss-n:case</ta>
            <ta e="T316" id="Seg_7179" s="T315">v-v:(ins)-v:ptcp</ta>
            <ta e="T317" id="Seg_7180" s="T316">n-n:(ins)-n:case</ta>
            <ta e="T318" id="Seg_7181" s="T317">adv</ta>
            <ta e="T319" id="Seg_7182" s="T318">v-v:tense-v:pred.pn</ta>
            <ta e="T320" id="Seg_7183" s="T319">n-n:case</ta>
            <ta e="T321" id="Seg_7184" s="T320">n-n:case</ta>
            <ta e="T322" id="Seg_7185" s="T321">ptcl</ta>
            <ta e="T323" id="Seg_7186" s="T322">n-n:case</ta>
            <ta e="T324" id="Seg_7187" s="T323">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T325" id="Seg_7188" s="T324">v-v:tense-v:pred.pn</ta>
            <ta e="T326" id="Seg_7189" s="T325">n-n:poss-n:case</ta>
            <ta e="T327" id="Seg_7190" s="T326">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T328" id="Seg_7191" s="T327">post</ta>
            <ta e="T329" id="Seg_7192" s="T328">n-n:case</ta>
            <ta e="T330" id="Seg_7193" s="T329">n-n:(poss)-n:case</ta>
            <ta e="T331" id="Seg_7194" s="T330">ptcl</ta>
            <ta e="T332" id="Seg_7195" s="T331">n-n:poss-n:case</ta>
            <ta e="T333" id="Seg_7196" s="T332">v-v:cvb</ta>
            <ta e="T334" id="Seg_7197" s="T333">v-v:tense-v:pred.pn</ta>
            <ta e="T335" id="Seg_7198" s="T334">v-v:mood.pn</ta>
            <ta e="T336" id="Seg_7199" s="T335">adj</ta>
            <ta e="T337" id="Seg_7200" s="T336">n-n:poss-n:case</ta>
            <ta e="T338" id="Seg_7201" s="T337">n-n:case</ta>
            <ta e="T339" id="Seg_7202" s="T338">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T340" id="Seg_7203" s="T339">v-v:tense-v:pred.pn</ta>
            <ta e="T341" id="Seg_7204" s="T340">n-n:case</ta>
            <ta e="T342" id="Seg_7205" s="T341">v-v:tense-v:poss.pn</ta>
            <ta e="T343" id="Seg_7206" s="T342">ptcl</ta>
            <ta e="T344" id="Seg_7207" s="T343">adj</ta>
            <ta e="T345" id="Seg_7208" s="T344">n-n:poss-n:case</ta>
            <ta e="T346" id="Seg_7209" s="T345">v-v:tense-v:pred.pn</ta>
            <ta e="T347" id="Seg_7210" s="T346">v-v:cvb</ta>
            <ta e="T348" id="Seg_7211" s="T347">v-v:cvb-v:pred.pn</ta>
            <ta e="T349" id="Seg_7212" s="T348">n-n:(poss)-n:case</ta>
            <ta e="T350" id="Seg_7213" s="T349">v-v:tense-v:pred.pn</ta>
            <ta e="T351" id="Seg_7214" s="T350">v-v:(ins)-v:(neg)-v:mood.pn</ta>
            <ta e="T352" id="Seg_7215" s="T351">n-n:case</ta>
            <ta e="T353" id="Seg_7216" s="T352">pers-pro:case</ta>
            <ta e="T354" id="Seg_7217" s="T353">adv</ta>
            <ta e="T355" id="Seg_7218" s="T354">v-v:tense-v:poss.pn</ta>
            <ta e="T356" id="Seg_7219" s="T355">n-n:(num)-n:poss-n:case</ta>
            <ta e="T357" id="Seg_7220" s="T356">ptcl</ta>
            <ta e="T358" id="Seg_7221" s="T357">v-v:tense-v:poss.pn</ta>
            <ta e="T359" id="Seg_7222" s="T358">n-n:case</ta>
            <ta e="T360" id="Seg_7223" s="T359">n-n:(poss)-n:case</ta>
            <ta e="T361" id="Seg_7224" s="T360">pers-pro:case</ta>
            <ta e="T362" id="Seg_7225" s="T361">n-n:(poss)</ta>
            <ta e="T363" id="Seg_7226" s="T362">ptcl</ta>
            <ta e="T364" id="Seg_7227" s="T363">v-v:tense-v:poss.pn</ta>
            <ta e="T365" id="Seg_7228" s="T364">ptcl</ta>
            <ta e="T366" id="Seg_7229" s="T365">n-n:case</ta>
            <ta e="T367" id="Seg_7230" s="T366">n-n:(poss)-n:case</ta>
            <ta e="T368" id="Seg_7231" s="T367">cardnum-cardnum&gt;ordnum</ta>
            <ta e="T369" id="Seg_7232" s="T368">n-n:poss-n:case</ta>
            <ta e="T370" id="Seg_7233" s="T369">n-n:case</ta>
            <ta e="T371" id="Seg_7234" s="T370">n-n:case</ta>
            <ta e="T372" id="Seg_7235" s="T371">v-v:cvb</ta>
            <ta e="T373" id="Seg_7236" s="T372">post</ta>
            <ta e="T374" id="Seg_7237" s="T373">v-v:tense-v:pred.pn</ta>
            <ta e="T375" id="Seg_7238" s="T374">dempro</ta>
            <ta e="T376" id="Seg_7239" s="T375">n-n:case</ta>
            <ta e="T377" id="Seg_7240" s="T376">n-n:poss-n:case</ta>
            <ta e="T378" id="Seg_7241" s="T377">v-v:(ins)-v:(neg)-v:mood.pn</ta>
            <ta e="T379" id="Seg_7242" s="T378">v-v:cvb</ta>
            <ta e="T380" id="Seg_7243" s="T379">v-v:mood-v:temp.pn</ta>
            <ta e="T381" id="Seg_7244" s="T380">v-v:tense-v:poss.pn</ta>
            <ta e="T382" id="Seg_7245" s="T381">pers-pro:case</ta>
            <ta e="T383" id="Seg_7246" s="T382">n-n&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T384" id="Seg_7247" s="T383">ptcl-ptcl:(poss.pn)</ta>
            <ta e="T385" id="Seg_7248" s="T384">que</ta>
            <ta e="T386" id="Seg_7249" s="T385">pers-pro:case</ta>
            <ta e="T387" id="Seg_7250" s="T386">v-v:tense-v:pred.pn</ta>
            <ta e="T388" id="Seg_7251" s="T387">v-v:cvb</ta>
            <ta e="T389" id="Seg_7252" s="T388">v-v:tense-v:pred.pn</ta>
            <ta e="T390" id="Seg_7253" s="T389">dempro</ta>
            <ta e="T391" id="Seg_7254" s="T390">n-n:case</ta>
            <ta e="T392" id="Seg_7255" s="T391">que-pro:case</ta>
            <ta e="T393" id="Seg_7256" s="T392">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T394" id="Seg_7257" s="T393">ptcl</ta>
            <ta e="T395" id="Seg_7258" s="T394">n-n:poss-n:case</ta>
            <ta e="T396" id="Seg_7259" s="T395">v-v:mood-v:temp.pn</ta>
            <ta e="T397" id="Seg_7260" s="T396">n-n:case</ta>
            <ta e="T398" id="Seg_7261" s="T397">n-n:case</ta>
            <ta e="T399" id="Seg_7262" s="T398">post</ta>
            <ta e="T400" id="Seg_7263" s="T399">ptcl</ta>
            <ta e="T401" id="Seg_7264" s="T400">que-pro:case</ta>
            <ta e="T402" id="Seg_7265" s="T401">ptcl</ta>
            <ta e="T403" id="Seg_7266" s="T402">n-n:(poss)-n:case</ta>
            <ta e="T404" id="Seg_7267" s="T403">ptcl-ptcl:(poss.pn)</ta>
            <ta e="T405" id="Seg_7268" s="T404">adv</ta>
            <ta e="T406" id="Seg_7269" s="T405">n-n:case</ta>
            <ta e="T407" id="Seg_7270" s="T406">v-v:cvb</ta>
            <ta e="T408" id="Seg_7271" s="T407">v-v:cvb</ta>
            <ta e="T409" id="Seg_7272" s="T408">post</ta>
            <ta e="T410" id="Seg_7273" s="T409">n-n:poss-n:case</ta>
            <ta e="T411" id="Seg_7274" s="T410">v-v:tense-v:pred.pn</ta>
            <ta e="T412" id="Seg_7275" s="T411">n-n:case</ta>
            <ta e="T413" id="Seg_7276" s="T412">n-n:case</ta>
            <ta e="T414" id="Seg_7277" s="T413">n-n:poss-n:case</ta>
            <ta e="T415" id="Seg_7278" s="T414">n-n:case</ta>
            <ta e="T416" id="Seg_7279" s="T415">n-n:(poss)-n:case</ta>
            <ta e="T417" id="Seg_7280" s="T416">ptcl</ta>
            <ta e="T418" id="Seg_7281" s="T417">v-v:cvb</ta>
            <ta e="T419" id="Seg_7282" s="T418">v-v:tense-v:pred.pn</ta>
            <ta e="T420" id="Seg_7283" s="T419">n-n:case</ta>
            <ta e="T421" id="Seg_7284" s="T420">n-n:case</ta>
            <ta e="T422" id="Seg_7285" s="T421">n-n:(poss)-n:case</ta>
            <ta e="T423" id="Seg_7286" s="T422">v-v:tense-v:poss.pn</ta>
            <ta e="T424" id="Seg_7287" s="T423">que</ta>
            <ta e="T425" id="Seg_7288" s="T424">n-n:case</ta>
            <ta e="T426" id="Seg_7289" s="T425">n-n:(poss)-n:case</ta>
            <ta e="T427" id="Seg_7290" s="T426">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T428" id="Seg_7291" s="T427">n-n:case</ta>
            <ta e="T429" id="Seg_7292" s="T428">n-n:poss-n:case</ta>
            <ta e="T430" id="Seg_7293" s="T429">v-v:cvb</ta>
            <ta e="T431" id="Seg_7294" s="T430">post</ta>
            <ta e="T432" id="Seg_7295" s="T431">n-n:case</ta>
            <ta e="T433" id="Seg_7296" s="T432">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T434" id="Seg_7297" s="T433">v-v:tense-v:pred.pn</ta>
            <ta e="T435" id="Seg_7298" s="T434">n-n:case</ta>
            <ta e="T436" id="Seg_7299" s="T435">n-n:(poss)-n:case</ta>
            <ta e="T437" id="Seg_7300" s="T436">v-v:cvb</ta>
            <ta e="T438" id="Seg_7301" s="T437">v-v:tense-v:poss.pn</ta>
            <ta e="T439" id="Seg_7302" s="T438">que</ta>
            <ta e="T440" id="Seg_7303" s="T439">n-n:poss-n:case</ta>
            <ta e="T441" id="Seg_7304" s="T440">v-v:tense-v:pred.pn</ta>
            <ta e="T442" id="Seg_7305" s="T441">n-n:case</ta>
            <ta e="T443" id="Seg_7306" s="T442">n-n:poss-n:case</ta>
            <ta e="T444" id="Seg_7307" s="T443">n-n:case</ta>
            <ta e="T445" id="Seg_7308" s="T444">quant</ta>
            <ta e="T446" id="Seg_7309" s="T445">n-n:(poss)</ta>
            <ta e="T447" id="Seg_7310" s="T446">conj</ta>
            <ta e="T448" id="Seg_7311" s="T447">ptcl</ta>
            <ta e="T449" id="Seg_7312" s="T448">pers-pro:case</ta>
            <ta e="T450" id="Seg_7313" s="T449">pers-pro:case</ta>
            <ta e="T451" id="Seg_7314" s="T450">que-pro:case</ta>
            <ta e="T452" id="Seg_7315" s="T451">v-v:ptcp-v:(case)</ta>
            <ta e="T453" id="Seg_7316" s="T452">v-v:tense-v:poss.pn</ta>
            <ta e="T454" id="Seg_7317" s="T453">n-n:case</ta>
            <ta e="T455" id="Seg_7318" s="T454">quant</ta>
            <ta e="T456" id="Seg_7319" s="T455">v-v:cvb</ta>
            <ta e="T457" id="Seg_7320" s="T456">post</ta>
            <ta e="T458" id="Seg_7321" s="T457">v-v:tense-v:pred.pn</ta>
            <ta e="T459" id="Seg_7322" s="T458">n-n:(poss)-n:case</ta>
            <ta e="T460" id="Seg_7323" s="T459">cardnum</ta>
            <ta e="T461" id="Seg_7324" s="T460">ptcl</ta>
            <ta e="T462" id="Seg_7325" s="T461">n-n:case</ta>
            <ta e="T463" id="Seg_7326" s="T462">n-n:(poss)-n:case</ta>
            <ta e="T464" id="Seg_7327" s="T463">ptcl</ta>
            <ta e="T465" id="Seg_7328" s="T464">pers-pro:case</ta>
            <ta e="T466" id="Seg_7329" s="T465">n-n:case</ta>
            <ta e="T467" id="Seg_7330" s="T466">n-n:poss-n:case</ta>
            <ta e="T468" id="Seg_7331" s="T467">v-v:tense-v:pred.pn</ta>
            <ta e="T469" id="Seg_7332" s="T468">n-n:case</ta>
            <ta e="T470" id="Seg_7333" s="T469">n-n:poss-n:case</ta>
            <ta e="T471" id="Seg_7334" s="T470">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T472" id="Seg_7335" s="T471">v-v:tense-v:pred.pn</ta>
            <ta e="T473" id="Seg_7336" s="T472">dempro</ta>
            <ta e="T474" id="Seg_7337" s="T473">n-n:case</ta>
            <ta e="T475" id="Seg_7338" s="T474">n-n:case</ta>
            <ta e="T476" id="Seg_7339" s="T475">n-n:poss-n:case</ta>
            <ta e="T477" id="Seg_7340" s="T476">n-n:case</ta>
            <ta e="T478" id="Seg_7341" s="T477">quant</ta>
            <ta e="T479" id="Seg_7342" s="T478">n-n:case</ta>
            <ta e="T480" id="Seg_7343" s="T479">post</ta>
            <ta e="T481" id="Seg_7344" s="T480">v-v:cvb</ta>
            <ta e="T482" id="Seg_7345" s="T481">v-v:tense-v:pred.pn</ta>
            <ta e="T483" id="Seg_7346" s="T482">n-n:case</ta>
            <ta e="T484" id="Seg_7347" s="T483">n-n:(poss)-n:case</ta>
            <ta e="T485" id="Seg_7348" s="T484">ptcl</ta>
            <ta e="T486" id="Seg_7349" s="T485">v-v:tense-v:pred.pn</ta>
            <ta e="T487" id="Seg_7350" s="T486">dempro</ta>
            <ta e="T488" id="Seg_7351" s="T487">n-n:case</ta>
            <ta e="T489" id="Seg_7352" s="T488">n-n:case</ta>
            <ta e="T490" id="Seg_7353" s="T489">que-pro:case</ta>
            <ta e="T491" id="Seg_7354" s="T490">n-n:(poss)-n:case</ta>
            <ta e="T492" id="Seg_7355" s="T491">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T493" id="Seg_7356" s="T492">adv</ta>
            <ta e="T494" id="Seg_7357" s="T493">n-n:case</ta>
            <ta e="T495" id="Seg_7358" s="T494">ptcl</ta>
            <ta e="T496" id="Seg_7359" s="T495">pers-pro:case</ta>
            <ta e="T497" id="Seg_7360" s="T496">v-v:tense-v:pred.pn</ta>
            <ta e="T498" id="Seg_7361" s="T497">ptcl</ta>
            <ta e="T499" id="Seg_7362" s="T498">n-n:(poss)-n:case</ta>
            <ta e="T500" id="Seg_7363" s="T499">v-v:tense-v:pred.pn</ta>
            <ta e="T501" id="Seg_7364" s="T500">pers-pro:case</ta>
            <ta e="T502" id="Seg_7365" s="T501">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T503" id="Seg_7366" s="T502">n-n:(num)-n:case</ta>
            <ta e="T504" id="Seg_7367" s="T503">v-v:cvb</ta>
            <ta e="T505" id="Seg_7368" s="T504">v-v:tense-v:pred.pn</ta>
            <ta e="T506" id="Seg_7369" s="T505">indfpro-pro:(num)-pro:case</ta>
            <ta e="T507" id="Seg_7370" s="T506">adj-n:(num)-n:case</ta>
            <ta e="T508" id="Seg_7371" s="T507">indfpro-pro:(num)-pro:case</ta>
            <ta e="T509" id="Seg_7372" s="T508">adj-n:(num)-n:case</ta>
            <ta e="T510" id="Seg_7373" s="T509">adj-n:(poss)-n:case</ta>
            <ta e="T511" id="Seg_7374" s="T510">adj-n:(num)-n:case</ta>
            <ta e="T512" id="Seg_7375" s="T511">adj</ta>
            <ta e="T513" id="Seg_7376" s="T512">n-n:case</ta>
            <ta e="T514" id="Seg_7377" s="T513">adj</ta>
            <ta e="T515" id="Seg_7378" s="T514">n-n:(pred.pn)</ta>
            <ta e="T516" id="Seg_7379" s="T515">v-v:tense-v:pred.pn</ta>
            <ta e="T517" id="Seg_7380" s="T516">v-v:tense-v:poss.pn</ta>
            <ta e="T518" id="Seg_7381" s="T517">n-n:case</ta>
            <ta e="T519" id="Seg_7382" s="T518">n-n:(poss)-n:case</ta>
            <ta e="T520" id="Seg_7383" s="T519">que-pro:case</ta>
            <ta e="T521" id="Seg_7384" s="T520">n-n:(poss)-n:case</ta>
            <ta e="T522" id="Seg_7385" s="T521">n-n:case</ta>
            <ta e="T523" id="Seg_7386" s="T522">n-n:poss-n:case</ta>
            <ta e="T524" id="Seg_7387" s="T523">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T525" id="Seg_7388" s="T524">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T526" id="Seg_7389" s="T525">v-v:tense-v:pred.pn</ta>
            <ta e="T527" id="Seg_7390" s="T526">pers-pro:case</ta>
            <ta e="T528" id="Seg_7391" s="T527">n-n:case</ta>
            <ta e="T529" id="Seg_7392" s="T528">n-n:poss-n:case</ta>
            <ta e="T530" id="Seg_7393" s="T529">v-v:tense-v:pred.pn</ta>
            <ta e="T531" id="Seg_7394" s="T530">dempro-pro:case</ta>
            <ta e="T532" id="Seg_7395" s="T531">post</ta>
            <ta e="T533" id="Seg_7396" s="T532">n-n:(num)-n:case</ta>
            <ta e="T534" id="Seg_7397" s="T533">ptcl</ta>
            <ta e="T535" id="Seg_7398" s="T534">n-n:case</ta>
            <ta e="T536" id="Seg_7399" s="T535">n-n:poss-n:case</ta>
            <ta e="T537" id="Seg_7400" s="T536">v-v:tense-v:pred.pn</ta>
            <ta e="T538" id="Seg_7401" s="T537">v-v:tense-v:pred.pn</ta>
            <ta e="T539" id="Seg_7402" s="T538">n-n:(poss)-n:case</ta>
            <ta e="T540" id="Seg_7403" s="T539">v-v:mood.pn</ta>
            <ta e="T541" id="Seg_7404" s="T540">ptcl</ta>
            <ta e="T542" id="Seg_7405" s="T541">n-n:case</ta>
            <ta e="T543" id="Seg_7406" s="T542">n-n:poss-n:case</ta>
            <ta e="T544" id="Seg_7407" s="T543">n-n:case</ta>
            <ta e="T545" id="Seg_7408" s="T544">n-n:(poss)-n:case</ta>
            <ta e="T546" id="Seg_7409" s="T545">v-v:cvb</ta>
            <ta e="T547" id="Seg_7410" s="T546">post</ta>
            <ta e="T548" id="Seg_7411" s="T547">v-v:cvb</ta>
            <ta e="T549" id="Seg_7412" s="T548">v-v:tense-v:pred.pn</ta>
            <ta e="T550" id="Seg_7413" s="T549">n-n:poss-n:case</ta>
            <ta e="T551" id="Seg_7414" s="T550">v-v:tense-v:pred.pn</ta>
            <ta e="T552" id="Seg_7415" s="T551">n-n:(num)-n:case</ta>
            <ta e="T553" id="Seg_7416" s="T552">v-v:cvb</ta>
            <ta e="T554" id="Seg_7417" s="T553">n-n:(poss)-n:case</ta>
            <ta e="T555" id="Seg_7418" s="T554">v-v:cvb</ta>
            <ta e="T556" id="Seg_7419" s="T555">n-n:poss-n:case</ta>
            <ta e="T557" id="Seg_7420" s="T556">n-n:case</ta>
            <ta e="T558" id="Seg_7421" s="T557">n-n:poss-n:case</ta>
            <ta e="T559" id="Seg_7422" s="T558">v-v&gt;adv</ta>
            <ta e="T560" id="Seg_7423" s="T559">v-v:tense-v:pred.pn</ta>
            <ta e="T561" id="Seg_7424" s="T560">n-n:case</ta>
            <ta e="T562" id="Seg_7425" s="T561">n-n:poss-n:case</ta>
            <ta e="T563" id="Seg_7426" s="T562">n-n:(poss)-n:case</ta>
            <ta e="T564" id="Seg_7427" s="T563">ptcl</ta>
            <ta e="T565" id="Seg_7428" s="T564">v-v:tense-v:pred.pn</ta>
            <ta e="T566" id="Seg_7429" s="T565">adv</ta>
            <ta e="T567" id="Seg_7430" s="T566">v-v:tense-v:pred.pn</ta>
            <ta e="T568" id="Seg_7431" s="T567">n-n:case</ta>
            <ta e="T569" id="Seg_7432" s="T568">n-n:case</ta>
            <ta e="T570" id="Seg_7433" s="T569">adj</ta>
            <ta e="T571" id="Seg_7434" s="T570">n-n:(poss)-n:case</ta>
            <ta e="T572" id="Seg_7435" s="T571">n-n:case</ta>
            <ta e="T573" id="Seg_7436" s="T572">n-n:case</ta>
            <ta e="T574" id="Seg_7437" s="T573">v-v:cvb</ta>
            <ta e="T575" id="Seg_7438" s="T574">v-v:cvb</ta>
            <ta e="T576" id="Seg_7439" s="T575">n-n:(num)-n:poss-n:case</ta>
            <ta e="T577" id="Seg_7440" s="T576">v-v:tense-v:pred.pn</ta>
            <ta e="T578" id="Seg_7441" s="T577">n-n:poss-n:case</ta>
            <ta e="T579" id="Seg_7442" s="T578">n-n:poss-n:case</ta>
            <ta e="T580" id="Seg_7443" s="T579">n-n:(ins)-n:case</ta>
            <ta e="T581" id="Seg_7444" s="T580">v-v:cvb</ta>
            <ta e="T582" id="Seg_7445" s="T581">v-v:cvb</ta>
            <ta e="T583" id="Seg_7446" s="T582">v-v:tense-v:pred.pn</ta>
            <ta e="T584" id="Seg_7447" s="T583">cardnum-cardnum&gt;collnum-n:(poss)-n:case</ta>
            <ta e="T585" id="Seg_7448" s="T584">n-n:case</ta>
            <ta e="T586" id="Seg_7449" s="T585">n-n:poss-n:case</ta>
            <ta e="T587" id="Seg_7450" s="T586">v-v:tense-v:pred.pn</ta>
            <ta e="T588" id="Seg_7451" s="T587">n-n:case</ta>
            <ta e="T589" id="Seg_7452" s="T588">n-n:case</ta>
            <ta e="T590" id="Seg_7453" s="T589">v-v:cvb</ta>
            <ta e="T591" id="Seg_7454" s="T590">n-n:(num)-n:poss-n:case</ta>
            <ta e="T592" id="Seg_7455" s="T591">v-v:ptcp-v:(case)</ta>
            <ta e="T593" id="Seg_7456" s="T592">v-v:tense-v:pred.pn</ta>
            <ta e="T594" id="Seg_7457" s="T593">v-v:ptcp</ta>
            <ta e="T595" id="Seg_7458" s="T594">n-n:case</ta>
            <ta e="T596" id="Seg_7459" s="T595">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T597" id="Seg_7460" s="T596">adj</ta>
            <ta e="T598" id="Seg_7461" s="T597">n-n:(poss)-n:case</ta>
            <ta e="T599" id="Seg_7462" s="T598">n-n:case</ta>
            <ta e="T600" id="Seg_7463" s="T599">n-n:poss-n:case</ta>
            <ta e="T601" id="Seg_7464" s="T600">v-v:cvb</ta>
            <ta e="T602" id="Seg_7465" s="T601">dempro-pro:case</ta>
            <ta e="T603" id="Seg_7466" s="T602">adj</ta>
            <ta e="T604" id="Seg_7467" s="T603">n-n:(poss)-n:case</ta>
            <ta e="T605" id="Seg_7468" s="T604">n-n&gt;adj</ta>
            <ta e="T606" id="Seg_7469" s="T605">n-n:poss-n:case</ta>
            <ta e="T607" id="Seg_7470" s="T606">v-v:cvb</ta>
            <ta e="T608" id="Seg_7471" s="T607">adv</ta>
            <ta e="T609" id="Seg_7472" s="T608">ptcl</ta>
            <ta e="T610" id="Seg_7473" s="T609">n-n:case</ta>
            <ta e="T611" id="Seg_7474" s="T610">n-n:poss-n:case</ta>
            <ta e="T612" id="Seg_7475" s="T611">v-v:tense-v:pred.pn</ta>
            <ta e="T613" id="Seg_7476" s="T612">n-n&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T614" id="Seg_7477" s="T613">propr</ta>
            <ta e="T615" id="Seg_7478" s="T614">n-n:case</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_7479" s="T0">v</ta>
            <ta e="T2" id="Seg_7480" s="T1">n</ta>
            <ta e="T3" id="Seg_7481" s="T2">n</ta>
            <ta e="T4" id="Seg_7482" s="T3">n</ta>
            <ta e="T5" id="Seg_7483" s="T4">n</ta>
            <ta e="T6" id="Seg_7484" s="T5">cardnum</ta>
            <ta e="T7" id="Seg_7485" s="T6">n</ta>
            <ta e="T8" id="Seg_7486" s="T7">ptcl</ta>
            <ta e="T9" id="Seg_7487" s="T8">cop</ta>
            <ta e="T10" id="Seg_7488" s="T9">n</ta>
            <ta e="T11" id="Seg_7489" s="T10">n</ta>
            <ta e="T12" id="Seg_7490" s="T11">adj</ta>
            <ta e="T13" id="Seg_7491" s="T12">cop</ta>
            <ta e="T14" id="Seg_7492" s="T13">post</ta>
            <ta e="T15" id="Seg_7493" s="T14">adj</ta>
            <ta e="T16" id="Seg_7494" s="T15">ptcl</ta>
            <ta e="T17" id="Seg_7495" s="T16">n</ta>
            <ta e="T18" id="Seg_7496" s="T17">adj</ta>
            <ta e="T19" id="Seg_7497" s="T18">n</ta>
            <ta e="T20" id="Seg_7498" s="T19">post</ta>
            <ta e="T21" id="Seg_7499" s="T20">v</ta>
            <ta e="T22" id="Seg_7500" s="T21">dempro</ta>
            <ta e="T23" id="Seg_7501" s="T22">post</ta>
            <ta e="T24" id="Seg_7502" s="T23">n</ta>
            <ta e="T25" id="Seg_7503" s="T24">v</ta>
            <ta e="T26" id="Seg_7504" s="T25">n</ta>
            <ta e="T27" id="Seg_7505" s="T26">n</ta>
            <ta e="T28" id="Seg_7506" s="T27">v</ta>
            <ta e="T29" id="Seg_7507" s="T28">n</ta>
            <ta e="T30" id="Seg_7508" s="T29">n</ta>
            <ta e="T31" id="Seg_7509" s="T30">cardnum</ta>
            <ta e="T32" id="Seg_7510" s="T31">adj</ta>
            <ta e="T33" id="Seg_7511" s="T32">adv</ta>
            <ta e="T34" id="Seg_7512" s="T33">adv</ta>
            <ta e="T35" id="Seg_7513" s="T34">v</ta>
            <ta e="T36" id="Seg_7514" s="T35">n</ta>
            <ta e="T37" id="Seg_7515" s="T36">ptcl</ta>
            <ta e="T38" id="Seg_7516" s="T37">v</ta>
            <ta e="T39" id="Seg_7517" s="T38">aux</ta>
            <ta e="T40" id="Seg_7518" s="T39">adv</ta>
            <ta e="T41" id="Seg_7519" s="T40">n</ta>
            <ta e="T42" id="Seg_7520" s="T41">n</ta>
            <ta e="T43" id="Seg_7521" s="T42">n</ta>
            <ta e="T44" id="Seg_7522" s="T43">v</ta>
            <ta e="T45" id="Seg_7523" s="T44">v</ta>
            <ta e="T46" id="Seg_7524" s="T45">n</ta>
            <ta e="T47" id="Seg_7525" s="T46">v</ta>
            <ta e="T48" id="Seg_7526" s="T47">post</ta>
            <ta e="T49" id="Seg_7527" s="T48">que</ta>
            <ta e="T50" id="Seg_7528" s="T49">ptcl</ta>
            <ta e="T51" id="Seg_7529" s="T50">n</ta>
            <ta e="T52" id="Seg_7530" s="T51">v</ta>
            <ta e="T53" id="Seg_7531" s="T52">aux</ta>
            <ta e="T54" id="Seg_7532" s="T53">adv</ta>
            <ta e="T55" id="Seg_7533" s="T54">v</ta>
            <ta e="T56" id="Seg_7534" s="T55">que</ta>
            <ta e="T57" id="Seg_7535" s="T56">cop</ta>
            <ta e="T58" id="Seg_7536" s="T57">v</ta>
            <ta e="T59" id="Seg_7537" s="T58">v</ta>
            <ta e="T60" id="Seg_7538" s="T59">n</ta>
            <ta e="T61" id="Seg_7539" s="T60">v</ta>
            <ta e="T62" id="Seg_7540" s="T61">n</ta>
            <ta e="T63" id="Seg_7541" s="T62">n</ta>
            <ta e="T64" id="Seg_7542" s="T63">n</ta>
            <ta e="T65" id="Seg_7543" s="T64">n</ta>
            <ta e="T66" id="Seg_7544" s="T65">v</ta>
            <ta e="T67" id="Seg_7545" s="T66">n</ta>
            <ta e="T68" id="Seg_7546" s="T67">v</ta>
            <ta e="T69" id="Seg_7547" s="T68">ptcl</ta>
            <ta e="T70" id="Seg_7548" s="T69">ptcl</ta>
            <ta e="T71" id="Seg_7549" s="T70">ptcl</ta>
            <ta e="T72" id="Seg_7550" s="T71">n</ta>
            <ta e="T73" id="Seg_7551" s="T72">v</ta>
            <ta e="T74" id="Seg_7552" s="T73">v</ta>
            <ta e="T75" id="Seg_7553" s="T74">cardnum</ta>
            <ta e="T76" id="Seg_7554" s="T75">ptcl</ta>
            <ta e="T77" id="Seg_7555" s="T76">n</ta>
            <ta e="T78" id="Seg_7556" s="T77">v</ta>
            <ta e="T79" id="Seg_7557" s="T78">ptcl</ta>
            <ta e="T80" id="Seg_7558" s="T79">que</ta>
            <ta e="T81" id="Seg_7559" s="T80">n</ta>
            <ta e="T82" id="Seg_7560" s="T81">v</ta>
            <ta e="T83" id="Seg_7561" s="T82">ptcl</ta>
            <ta e="T84" id="Seg_7562" s="T83">v</ta>
            <ta e="T85" id="Seg_7563" s="T84">v</ta>
            <ta e="T86" id="Seg_7564" s="T85">adv</ta>
            <ta e="T87" id="Seg_7565" s="T86">v</ta>
            <ta e="T88" id="Seg_7566" s="T87">v</ta>
            <ta e="T89" id="Seg_7567" s="T88">ptcl</ta>
            <ta e="T90" id="Seg_7568" s="T89">ptcl</ta>
            <ta e="T91" id="Seg_7569" s="T90">v</ta>
            <ta e="T92" id="Seg_7570" s="T91">v</ta>
            <ta e="T93" id="Seg_7571" s="T92">n</ta>
            <ta e="T94" id="Seg_7572" s="T93">n</ta>
            <ta e="T95" id="Seg_7573" s="T94">n</ta>
            <ta e="T96" id="Seg_7574" s="T95">n</ta>
            <ta e="T97" id="Seg_7575" s="T96">v</ta>
            <ta e="T98" id="Seg_7576" s="T97">aux</ta>
            <ta e="T99" id="Seg_7577" s="T98">post</ta>
            <ta e="T100" id="Seg_7578" s="T99">n</ta>
            <ta e="T101" id="Seg_7579" s="T100">n</ta>
            <ta e="T102" id="Seg_7580" s="T101">v</ta>
            <ta e="T103" id="Seg_7581" s="T102">aux</ta>
            <ta e="T104" id="Seg_7582" s="T103">n</ta>
            <ta e="T105" id="Seg_7583" s="T104">n</ta>
            <ta e="T106" id="Seg_7584" s="T105">adv</ta>
            <ta e="T107" id="Seg_7585" s="T106">v</ta>
            <ta e="T108" id="Seg_7586" s="T107">n</ta>
            <ta e="T109" id="Seg_7587" s="T108">n</ta>
            <ta e="T110" id="Seg_7588" s="T109">v</ta>
            <ta e="T111" id="Seg_7589" s="T110">n</ta>
            <ta e="T112" id="Seg_7590" s="T111">v</ta>
            <ta e="T113" id="Seg_7591" s="T112">adj</ta>
            <ta e="T114" id="Seg_7592" s="T113">n</ta>
            <ta e="T115" id="Seg_7593" s="T114">v</ta>
            <ta e="T116" id="Seg_7594" s="T115">pers</ta>
            <ta e="T117" id="Seg_7595" s="T116">n</ta>
            <ta e="T118" id="Seg_7596" s="T117">v</ta>
            <ta e="T119" id="Seg_7597" s="T118">n</ta>
            <ta e="T120" id="Seg_7598" s="T119">n</ta>
            <ta e="T121" id="Seg_7599" s="T120">n</ta>
            <ta e="T122" id="Seg_7600" s="T121">n</ta>
            <ta e="T123" id="Seg_7601" s="T122">ptcl</ta>
            <ta e="T124" id="Seg_7602" s="T123">n</ta>
            <ta e="T125" id="Seg_7603" s="T124">cop</ta>
            <ta e="T126" id="Seg_7604" s="T125">n</ta>
            <ta e="T127" id="Seg_7605" s="T126">n</ta>
            <ta e="T128" id="Seg_7606" s="T127">n</ta>
            <ta e="T129" id="Seg_7607" s="T128">n</ta>
            <ta e="T130" id="Seg_7608" s="T129">v</ta>
            <ta e="T131" id="Seg_7609" s="T130">aux</ta>
            <ta e="T132" id="Seg_7610" s="T131">n</ta>
            <ta e="T133" id="Seg_7611" s="T132">n</ta>
            <ta e="T134" id="Seg_7612" s="T133">adj</ta>
            <ta e="T135" id="Seg_7613" s="T134">conj</ta>
            <ta e="T136" id="Seg_7614" s="T135">adj</ta>
            <ta e="T137" id="Seg_7615" s="T136">n</ta>
            <ta e="T138" id="Seg_7616" s="T137">n</ta>
            <ta e="T139" id="Seg_7617" s="T138">v</ta>
            <ta e="T140" id="Seg_7618" s="T139">que</ta>
            <ta e="T141" id="Seg_7619" s="T140">ptcl</ta>
            <ta e="T142" id="Seg_7620" s="T141">adj</ta>
            <ta e="T143" id="Seg_7621" s="T142">adj</ta>
            <ta e="T144" id="Seg_7622" s="T143">cardnum</ta>
            <ta e="T145" id="Seg_7623" s="T144">n</ta>
            <ta e="T146" id="Seg_7624" s="T145">adj</ta>
            <ta e="T147" id="Seg_7625" s="T146">n</ta>
            <ta e="T148" id="Seg_7626" s="T147">n</ta>
            <ta e="T149" id="Seg_7627" s="T148">v</ta>
            <ta e="T150" id="Seg_7628" s="T149">n</ta>
            <ta e="T151" id="Seg_7629" s="T150">n</ta>
            <ta e="T152" id="Seg_7630" s="T151">n</ta>
            <ta e="T153" id="Seg_7631" s="T152">v</ta>
            <ta e="T154" id="Seg_7632" s="T153">n</ta>
            <ta e="T155" id="Seg_7633" s="T154">n</ta>
            <ta e="T156" id="Seg_7634" s="T155">v</ta>
            <ta e="T157" id="Seg_7635" s="T156">que</ta>
            <ta e="T158" id="Seg_7636" s="T157">v</ta>
            <ta e="T159" id="Seg_7637" s="T158">v</ta>
            <ta e="T160" id="Seg_7638" s="T159">v</ta>
            <ta e="T161" id="Seg_7639" s="T160">dempro</ta>
            <ta e="T162" id="Seg_7640" s="T161">ptcl</ta>
            <ta e="T164" id="Seg_7641" s="T163">n</ta>
            <ta e="T165" id="Seg_7642" s="T164">n</ta>
            <ta e="T166" id="Seg_7643" s="T165">v</ta>
            <ta e="T167" id="Seg_7644" s="T166">n</ta>
            <ta e="T168" id="Seg_7645" s="T167">v</ta>
            <ta e="T169" id="Seg_7646" s="T168">n</ta>
            <ta e="T170" id="Seg_7647" s="T169">v</ta>
            <ta e="T171" id="Seg_7648" s="T170">post</ta>
            <ta e="T172" id="Seg_7649" s="T171">dempro</ta>
            <ta e="T173" id="Seg_7650" s="T172">n</ta>
            <ta e="T174" id="Seg_7651" s="T173">v</ta>
            <ta e="T175" id="Seg_7652" s="T174">n</ta>
            <ta e="T176" id="Seg_7653" s="T175">v</ta>
            <ta e="T177" id="Seg_7654" s="T176">n</ta>
            <ta e="T178" id="Seg_7655" s="T177">n</ta>
            <ta e="T179" id="Seg_7656" s="T178">cop</ta>
            <ta e="T180" id="Seg_7657" s="T179">adv</ta>
            <ta e="T181" id="Seg_7658" s="T180">ptcl</ta>
            <ta e="T182" id="Seg_7659" s="T181">v</ta>
            <ta e="T183" id="Seg_7660" s="T182">n</ta>
            <ta e="T184" id="Seg_7661" s="T183">v</ta>
            <ta e="T185" id="Seg_7662" s="T184">n</ta>
            <ta e="T186" id="Seg_7663" s="T185">n</ta>
            <ta e="T187" id="Seg_7664" s="T186">ptcl</ta>
            <ta e="T188" id="Seg_7665" s="T187">v</ta>
            <ta e="T189" id="Seg_7666" s="T188">dempro</ta>
            <ta e="T190" id="Seg_7667" s="T189">adv</ta>
            <ta e="T191" id="Seg_7668" s="T190">v</ta>
            <ta e="T192" id="Seg_7669" s="T191">post</ta>
            <ta e="T193" id="Seg_7670" s="T192">v</ta>
            <ta e="T194" id="Seg_7671" s="T193">v</ta>
            <ta e="T195" id="Seg_7672" s="T194">n</ta>
            <ta e="T196" id="Seg_7673" s="T195">adv</ta>
            <ta e="T197" id="Seg_7674" s="T196">v</ta>
            <ta e="T198" id="Seg_7675" s="T197">n</ta>
            <ta e="T199" id="Seg_7676" s="T198">n</ta>
            <ta e="T200" id="Seg_7677" s="T199">v</ta>
            <ta e="T201" id="Seg_7678" s="T200">adv</ta>
            <ta e="T202" id="Seg_7679" s="T201">v</ta>
            <ta e="T203" id="Seg_7680" s="T202">pers</ta>
            <ta e="T204" id="Seg_7681" s="T203">pers</ta>
            <ta e="T205" id="Seg_7682" s="T204">n</ta>
            <ta e="T206" id="Seg_7683" s="T205">cop</ta>
            <ta e="T207" id="Seg_7684" s="T206">post</ta>
            <ta e="T208" id="Seg_7685" s="T207">v</ta>
            <ta e="T209" id="Seg_7686" s="T208">n</ta>
            <ta e="T210" id="Seg_7687" s="T209">ptcl</ta>
            <ta e="T211" id="Seg_7688" s="T210">adj</ta>
            <ta e="T212" id="Seg_7689" s="T211">n</ta>
            <ta e="T213" id="Seg_7690" s="T212">n</ta>
            <ta e="T214" id="Seg_7691" s="T213">v</ta>
            <ta e="T215" id="Seg_7692" s="T214">v</ta>
            <ta e="T216" id="Seg_7693" s="T215">aux</ta>
            <ta e="T217" id="Seg_7694" s="T216">n</ta>
            <ta e="T218" id="Seg_7695" s="T217">adv</ta>
            <ta e="T219" id="Seg_7696" s="T218">n</ta>
            <ta e="T220" id="Seg_7697" s="T219">n</ta>
            <ta e="T221" id="Seg_7698" s="T220">adj</ta>
            <ta e="T222" id="Seg_7699" s="T221">n</ta>
            <ta e="T223" id="Seg_7700" s="T222">adv</ta>
            <ta e="T224" id="Seg_7701" s="T223">v</ta>
            <ta e="T225" id="Seg_7702" s="T224">n</ta>
            <ta e="T226" id="Seg_7703" s="T225">n</ta>
            <ta e="T227" id="Seg_7704" s="T226">ptcl</ta>
            <ta e="T228" id="Seg_7705" s="T227">v</ta>
            <ta e="T229" id="Seg_7706" s="T228">v</ta>
            <ta e="T230" id="Seg_7707" s="T229">n</ta>
            <ta e="T231" id="Seg_7708" s="T230">v</ta>
            <ta e="T232" id="Seg_7709" s="T231">post</ta>
            <ta e="T233" id="Seg_7710" s="T232">n</ta>
            <ta e="T234" id="Seg_7711" s="T233">n</ta>
            <ta e="T235" id="Seg_7712" s="T234">ptcl</ta>
            <ta e="T236" id="Seg_7713" s="T235">n</ta>
            <ta e="T237" id="Seg_7714" s="T236">v</ta>
            <ta e="T238" id="Seg_7715" s="T237">aux</ta>
            <ta e="T239" id="Seg_7716" s="T238">v</ta>
            <ta e="T240" id="Seg_7717" s="T239">ordnum</ta>
            <ta e="T241" id="Seg_7718" s="T240">n</ta>
            <ta e="T242" id="Seg_7719" s="T241">n</ta>
            <ta e="T243" id="Seg_7720" s="T242">v</ta>
            <ta e="T244" id="Seg_7721" s="T243">n</ta>
            <ta e="T245" id="Seg_7722" s="T244">n</ta>
            <ta e="T246" id="Seg_7723" s="T245">v</ta>
            <ta e="T247" id="Seg_7724" s="T246">ptcl</ta>
            <ta e="T248" id="Seg_7725" s="T247">adj</ta>
            <ta e="T249" id="Seg_7726" s="T248">n</ta>
            <ta e="T250" id="Seg_7727" s="T249">v</ta>
            <ta e="T251" id="Seg_7728" s="T250">dempro</ta>
            <ta e="T252" id="Seg_7729" s="T251">n</ta>
            <ta e="T253" id="Seg_7730" s="T252">n</ta>
            <ta e="T254" id="Seg_7731" s="T253">v</ta>
            <ta e="T255" id="Seg_7732" s="T254">n</ta>
            <ta e="T256" id="Seg_7733" s="T255">ptcl</ta>
            <ta e="T257" id="Seg_7734" s="T256">v</ta>
            <ta e="T258" id="Seg_7735" s="T257">post</ta>
            <ta e="T259" id="Seg_7736" s="T258">v</ta>
            <ta e="T260" id="Seg_7737" s="T259">adv</ta>
            <ta e="T261" id="Seg_7738" s="T260">dempro</ta>
            <ta e="T262" id="Seg_7739" s="T261">n</ta>
            <ta e="T263" id="Seg_7740" s="T262">n</ta>
            <ta e="T264" id="Seg_7741" s="T263">v</ta>
            <ta e="T265" id="Seg_7742" s="T264">n</ta>
            <ta e="T266" id="Seg_7743" s="T265">n</ta>
            <ta e="T267" id="Seg_7744" s="T266">v</ta>
            <ta e="T268" id="Seg_7745" s="T267">v</ta>
            <ta e="T269" id="Seg_7746" s="T268">dempro</ta>
            <ta e="T270" id="Seg_7747" s="T269">n</ta>
            <ta e="T271" id="Seg_7748" s="T270">n</ta>
            <ta e="T272" id="Seg_7749" s="T271">ptcl</ta>
            <ta e="T273" id="Seg_7750" s="T272">n</ta>
            <ta e="T274" id="Seg_7751" s="T273">ptcl</ta>
            <ta e="T275" id="Seg_7752" s="T274">v</ta>
            <ta e="T276" id="Seg_7753" s="T275">n</ta>
            <ta e="T277" id="Seg_7754" s="T276">n</ta>
            <ta e="T278" id="Seg_7755" s="T277">v</ta>
            <ta e="T279" id="Seg_7756" s="T278">v</ta>
            <ta e="T280" id="Seg_7757" s="T279">post</ta>
            <ta e="T281" id="Seg_7758" s="T280">n</ta>
            <ta e="T282" id="Seg_7759" s="T281">n</ta>
            <ta e="T283" id="Seg_7760" s="T282">n</ta>
            <ta e="T284" id="Seg_7761" s="T283">v</ta>
            <ta e="T285" id="Seg_7762" s="T284">n</ta>
            <ta e="T286" id="Seg_7763" s="T285">v</ta>
            <ta e="T287" id="Seg_7764" s="T286">adj</ta>
            <ta e="T288" id="Seg_7765" s="T287">n</ta>
            <ta e="T289" id="Seg_7766" s="T288">ptcl</ta>
            <ta e="T290" id="Seg_7767" s="T289">v</ta>
            <ta e="T291" id="Seg_7768" s="T290">n</ta>
            <ta e="T292" id="Seg_7769" s="T291">adv</ta>
            <ta e="T293" id="Seg_7770" s="T292">v</ta>
            <ta e="T294" id="Seg_7771" s="T293">n</ta>
            <ta e="T295" id="Seg_7772" s="T294">n</ta>
            <ta e="T296" id="Seg_7773" s="T295">v</ta>
            <ta e="T297" id="Seg_7774" s="T296">adv</ta>
            <ta e="T298" id="Seg_7775" s="T297">v</ta>
            <ta e="T299" id="Seg_7776" s="T298">pers</ta>
            <ta e="T300" id="Seg_7777" s="T299">adj</ta>
            <ta e="T301" id="Seg_7778" s="T300">ptcl</ta>
            <ta e="T302" id="Seg_7779" s="T301">n</ta>
            <ta e="T303" id="Seg_7780" s="T302">n</ta>
            <ta e="T304" id="Seg_7781" s="T303">ptcl</ta>
            <ta e="T305" id="Seg_7782" s="T304">v</ta>
            <ta e="T306" id="Seg_7783" s="T305">post</ta>
            <ta e="T307" id="Seg_7784" s="T306">adj</ta>
            <ta e="T308" id="Seg_7785" s="T307">n</ta>
            <ta e="T309" id="Seg_7786" s="T308">n</ta>
            <ta e="T310" id="Seg_7787" s="T309">v</ta>
            <ta e="T311" id="Seg_7788" s="T310">aux</ta>
            <ta e="T312" id="Seg_7789" s="T311">n</ta>
            <ta e="T313" id="Seg_7790" s="T312">adv</ta>
            <ta e="T314" id="Seg_7791" s="T313">n</ta>
            <ta e="T315" id="Seg_7792" s="T314">n</ta>
            <ta e="T316" id="Seg_7793" s="T315">v</ta>
            <ta e="T317" id="Seg_7794" s="T316">n</ta>
            <ta e="T318" id="Seg_7795" s="T317">adv</ta>
            <ta e="T319" id="Seg_7796" s="T318">v</ta>
            <ta e="T320" id="Seg_7797" s="T319">n</ta>
            <ta e="T321" id="Seg_7798" s="T320">n</ta>
            <ta e="T322" id="Seg_7799" s="T321">ptcl</ta>
            <ta e="T323" id="Seg_7800" s="T322">n</ta>
            <ta e="T324" id="Seg_7801" s="T323">v</ta>
            <ta e="T325" id="Seg_7802" s="T324">v</ta>
            <ta e="T326" id="Seg_7803" s="T325">n</ta>
            <ta e="T327" id="Seg_7804" s="T326">v</ta>
            <ta e="T328" id="Seg_7805" s="T327">post</ta>
            <ta e="T329" id="Seg_7806" s="T328">n</ta>
            <ta e="T330" id="Seg_7807" s="T329">n</ta>
            <ta e="T331" id="Seg_7808" s="T330">ptcl</ta>
            <ta e="T332" id="Seg_7809" s="T331">n</ta>
            <ta e="T333" id="Seg_7810" s="T332">v</ta>
            <ta e="T334" id="Seg_7811" s="T333">aux</ta>
            <ta e="T335" id="Seg_7812" s="T334">v</ta>
            <ta e="T336" id="Seg_7813" s="T335">adj</ta>
            <ta e="T337" id="Seg_7814" s="T336">n</ta>
            <ta e="T338" id="Seg_7815" s="T337">n</ta>
            <ta e="T339" id="Seg_7816" s="T338">v</ta>
            <ta e="T340" id="Seg_7817" s="T339">v</ta>
            <ta e="T341" id="Seg_7818" s="T340">n</ta>
            <ta e="T342" id="Seg_7819" s="T341">v</ta>
            <ta e="T343" id="Seg_7820" s="T342">ptcl</ta>
            <ta e="T344" id="Seg_7821" s="T343">adj</ta>
            <ta e="T345" id="Seg_7822" s="T344">n</ta>
            <ta e="T346" id="Seg_7823" s="T345">v</ta>
            <ta e="T347" id="Seg_7824" s="T346">v</ta>
            <ta e="T348" id="Seg_7825" s="T347">aux</ta>
            <ta e="T349" id="Seg_7826" s="T348">n</ta>
            <ta e="T350" id="Seg_7827" s="T349">v</ta>
            <ta e="T351" id="Seg_7828" s="T350">v</ta>
            <ta e="T352" id="Seg_7829" s="T351">n</ta>
            <ta e="T353" id="Seg_7830" s="T352">pers</ta>
            <ta e="T354" id="Seg_7831" s="T353">adv</ta>
            <ta e="T355" id="Seg_7832" s="T354">v</ta>
            <ta e="T356" id="Seg_7833" s="T355">n</ta>
            <ta e="T357" id="Seg_7834" s="T356">ptcl</ta>
            <ta e="T358" id="Seg_7835" s="T357">v</ta>
            <ta e="T359" id="Seg_7836" s="T358">n</ta>
            <ta e="T360" id="Seg_7837" s="T359">n</ta>
            <ta e="T361" id="Seg_7838" s="T360">pers</ta>
            <ta e="T362" id="Seg_7839" s="T361">n</ta>
            <ta e="T363" id="Seg_7840" s="T362">ptcl</ta>
            <ta e="T364" id="Seg_7841" s="T363">cop</ta>
            <ta e="T365" id="Seg_7842" s="T364">ptcl</ta>
            <ta e="T366" id="Seg_7843" s="T365">n</ta>
            <ta e="T367" id="Seg_7844" s="T366">n</ta>
            <ta e="T368" id="Seg_7845" s="T367">ordnum</ta>
            <ta e="T369" id="Seg_7846" s="T368">n</ta>
            <ta e="T370" id="Seg_7847" s="T369">n</ta>
            <ta e="T371" id="Seg_7848" s="T370">n</ta>
            <ta e="T372" id="Seg_7849" s="T371">v</ta>
            <ta e="T373" id="Seg_7850" s="T372">post</ta>
            <ta e="T374" id="Seg_7851" s="T373">v</ta>
            <ta e="T375" id="Seg_7852" s="T374">dempro</ta>
            <ta e="T376" id="Seg_7853" s="T375">n</ta>
            <ta e="T377" id="Seg_7854" s="T376">n</ta>
            <ta e="T378" id="Seg_7855" s="T377">v</ta>
            <ta e="T379" id="Seg_7856" s="T378">v</ta>
            <ta e="T380" id="Seg_7857" s="T379">aux</ta>
            <ta e="T381" id="Seg_7858" s="T380">v</ta>
            <ta e="T382" id="Seg_7859" s="T381">pers</ta>
            <ta e="T383" id="Seg_7860" s="T382">v</ta>
            <ta e="T384" id="Seg_7861" s="T383">ptcl</ta>
            <ta e="T385" id="Seg_7862" s="T384">que</ta>
            <ta e="T386" id="Seg_7863" s="T385">pers</ta>
            <ta e="T387" id="Seg_7864" s="T386">v</ta>
            <ta e="T388" id="Seg_7865" s="T387">v</ta>
            <ta e="T389" id="Seg_7866" s="T388">v</ta>
            <ta e="T390" id="Seg_7867" s="T389">dempro</ta>
            <ta e="T391" id="Seg_7868" s="T390">n</ta>
            <ta e="T392" id="Seg_7869" s="T391">que</ta>
            <ta e="T393" id="Seg_7870" s="T392">cop</ta>
            <ta e="T394" id="Seg_7871" s="T393">ptcl</ta>
            <ta e="T395" id="Seg_7872" s="T394">n</ta>
            <ta e="T396" id="Seg_7873" s="T395">v</ta>
            <ta e="T397" id="Seg_7874" s="T396">n</ta>
            <ta e="T398" id="Seg_7875" s="T397">n</ta>
            <ta e="T399" id="Seg_7876" s="T398">post</ta>
            <ta e="T400" id="Seg_7877" s="T399">ptcl</ta>
            <ta e="T401" id="Seg_7878" s="T400">que</ta>
            <ta e="T402" id="Seg_7879" s="T401">ptcl</ta>
            <ta e="T403" id="Seg_7880" s="T402">n</ta>
            <ta e="T404" id="Seg_7881" s="T403">ptcl</ta>
            <ta e="T405" id="Seg_7882" s="T404">adv</ta>
            <ta e="T406" id="Seg_7883" s="T405">n</ta>
            <ta e="T407" id="Seg_7884" s="T406">v</ta>
            <ta e="T408" id="Seg_7885" s="T407">aux</ta>
            <ta e="T409" id="Seg_7886" s="T408">post</ta>
            <ta e="T410" id="Seg_7887" s="T409">n</ta>
            <ta e="T411" id="Seg_7888" s="T410">v</ta>
            <ta e="T412" id="Seg_7889" s="T411">n</ta>
            <ta e="T413" id="Seg_7890" s="T412">n</ta>
            <ta e="T414" id="Seg_7891" s="T413">n</ta>
            <ta e="T415" id="Seg_7892" s="T414">n</ta>
            <ta e="T416" id="Seg_7893" s="T415">n</ta>
            <ta e="T417" id="Seg_7894" s="T416">ptcl</ta>
            <ta e="T418" id="Seg_7895" s="T417">v</ta>
            <ta e="T419" id="Seg_7896" s="T418">aux</ta>
            <ta e="T420" id="Seg_7897" s="T419">n</ta>
            <ta e="T421" id="Seg_7898" s="T420">n</ta>
            <ta e="T422" id="Seg_7899" s="T421">n</ta>
            <ta e="T423" id="Seg_7900" s="T422">v</ta>
            <ta e="T424" id="Seg_7901" s="T423">que</ta>
            <ta e="T425" id="Seg_7902" s="T424">n</ta>
            <ta e="T426" id="Seg_7903" s="T425">n</ta>
            <ta e="T427" id="Seg_7904" s="T426">v</ta>
            <ta e="T428" id="Seg_7905" s="T427">n</ta>
            <ta e="T429" id="Seg_7906" s="T428">n</ta>
            <ta e="T430" id="Seg_7907" s="T429">v</ta>
            <ta e="T431" id="Seg_7908" s="T430">post</ta>
            <ta e="T432" id="Seg_7909" s="T431">n</ta>
            <ta e="T433" id="Seg_7910" s="T432">v</ta>
            <ta e="T434" id="Seg_7911" s="T433">aux</ta>
            <ta e="T435" id="Seg_7912" s="T434">n</ta>
            <ta e="T436" id="Seg_7913" s="T435">n</ta>
            <ta e="T437" id="Seg_7914" s="T436">v</ta>
            <ta e="T438" id="Seg_7915" s="T437">v</ta>
            <ta e="T439" id="Seg_7916" s="T438">que</ta>
            <ta e="T440" id="Seg_7917" s="T439">n</ta>
            <ta e="T441" id="Seg_7918" s="T440">v</ta>
            <ta e="T442" id="Seg_7919" s="T441">n</ta>
            <ta e="T443" id="Seg_7920" s="T442">n</ta>
            <ta e="T444" id="Seg_7921" s="T443">n</ta>
            <ta e="T445" id="Seg_7922" s="T444">quant</ta>
            <ta e="T446" id="Seg_7923" s="T445">n</ta>
            <ta e="T447" id="Seg_7924" s="T446">conj</ta>
            <ta e="T448" id="Seg_7925" s="T447">ptcl</ta>
            <ta e="T449" id="Seg_7926" s="T448">pers</ta>
            <ta e="T450" id="Seg_7927" s="T449">pers</ta>
            <ta e="T451" id="Seg_7928" s="T450">que</ta>
            <ta e="T452" id="Seg_7929" s="T451">v</ta>
            <ta e="T453" id="Seg_7930" s="T452">v</ta>
            <ta e="T454" id="Seg_7931" s="T453">n</ta>
            <ta e="T455" id="Seg_7932" s="T454">quant</ta>
            <ta e="T456" id="Seg_7933" s="T455">cop</ta>
            <ta e="T457" id="Seg_7934" s="T456">post</ta>
            <ta e="T458" id="Seg_7935" s="T457">v</ta>
            <ta e="T459" id="Seg_7936" s="T458">n</ta>
            <ta e="T460" id="Seg_7937" s="T459">cardnum</ta>
            <ta e="T461" id="Seg_7938" s="T460">ptcl</ta>
            <ta e="T462" id="Seg_7939" s="T461">n</ta>
            <ta e="T463" id="Seg_7940" s="T462">n</ta>
            <ta e="T464" id="Seg_7941" s="T463">ptcl</ta>
            <ta e="T465" id="Seg_7942" s="T464">pers</ta>
            <ta e="T466" id="Seg_7943" s="T465">n</ta>
            <ta e="T467" id="Seg_7944" s="T466">n</ta>
            <ta e="T468" id="Seg_7945" s="T467">v</ta>
            <ta e="T469" id="Seg_7946" s="T468">n</ta>
            <ta e="T470" id="Seg_7947" s="T469">n</ta>
            <ta e="T471" id="Seg_7948" s="T470">v</ta>
            <ta e="T472" id="Seg_7949" s="T471">v</ta>
            <ta e="T473" id="Seg_7950" s="T472">dempro</ta>
            <ta e="T474" id="Seg_7951" s="T473">n</ta>
            <ta e="T475" id="Seg_7952" s="T474">n</ta>
            <ta e="T476" id="Seg_7953" s="T475">n</ta>
            <ta e="T477" id="Seg_7954" s="T476">n</ta>
            <ta e="T478" id="Seg_7955" s="T477">quant</ta>
            <ta e="T479" id="Seg_7956" s="T478">n</ta>
            <ta e="T480" id="Seg_7957" s="T479">post</ta>
            <ta e="T481" id="Seg_7958" s="T480">v</ta>
            <ta e="T482" id="Seg_7959" s="T481">aux</ta>
            <ta e="T483" id="Seg_7960" s="T482">n</ta>
            <ta e="T484" id="Seg_7961" s="T483">n</ta>
            <ta e="T485" id="Seg_7962" s="T484">ptcl</ta>
            <ta e="T486" id="Seg_7963" s="T485">v</ta>
            <ta e="T487" id="Seg_7964" s="T486">dempro</ta>
            <ta e="T488" id="Seg_7965" s="T487">n</ta>
            <ta e="T489" id="Seg_7966" s="T488">n</ta>
            <ta e="T490" id="Seg_7967" s="T489">que</ta>
            <ta e="T491" id="Seg_7968" s="T490">n</ta>
            <ta e="T492" id="Seg_7969" s="T491">v</ta>
            <ta e="T493" id="Seg_7970" s="T492">adv</ta>
            <ta e="T494" id="Seg_7971" s="T493">n</ta>
            <ta e="T495" id="Seg_7972" s="T494">ptcl</ta>
            <ta e="T496" id="Seg_7973" s="T495">pers</ta>
            <ta e="T497" id="Seg_7974" s="T496">v</ta>
            <ta e="T498" id="Seg_7975" s="T497">ptcl</ta>
            <ta e="T499" id="Seg_7976" s="T498">n</ta>
            <ta e="T500" id="Seg_7977" s="T499">v</ta>
            <ta e="T501" id="Seg_7978" s="T500">pers</ta>
            <ta e="T502" id="Seg_7979" s="T501">v</ta>
            <ta e="T503" id="Seg_7980" s="T502">n</ta>
            <ta e="T504" id="Seg_7981" s="T503">v</ta>
            <ta e="T505" id="Seg_7982" s="T504">aux</ta>
            <ta e="T506" id="Seg_7983" s="T505">indfpro</ta>
            <ta e="T507" id="Seg_7984" s="T506">adj</ta>
            <ta e="T508" id="Seg_7985" s="T507">indfpro</ta>
            <ta e="T509" id="Seg_7986" s="T508">adj</ta>
            <ta e="T510" id="Seg_7987" s="T509">n</ta>
            <ta e="T511" id="Seg_7988" s="T510">adj</ta>
            <ta e="T512" id="Seg_7989" s="T511">adj</ta>
            <ta e="T513" id="Seg_7990" s="T512">n</ta>
            <ta e="T514" id="Seg_7991" s="T513">adj</ta>
            <ta e="T515" id="Seg_7992" s="T514">n</ta>
            <ta e="T516" id="Seg_7993" s="T515">aux</ta>
            <ta e="T517" id="Seg_7994" s="T516">v</ta>
            <ta e="T518" id="Seg_7995" s="T517">n</ta>
            <ta e="T519" id="Seg_7996" s="T518">n</ta>
            <ta e="T520" id="Seg_7997" s="T519">que</ta>
            <ta e="T521" id="Seg_7998" s="T520">n</ta>
            <ta e="T522" id="Seg_7999" s="T521">n</ta>
            <ta e="T523" id="Seg_8000" s="T522">n</ta>
            <ta e="T524" id="Seg_8001" s="T523">v</ta>
            <ta e="T525" id="Seg_8002" s="T524">emphpro</ta>
            <ta e="T526" id="Seg_8003" s="T525">v</ta>
            <ta e="T527" id="Seg_8004" s="T526">pers</ta>
            <ta e="T528" id="Seg_8005" s="T527">n</ta>
            <ta e="T529" id="Seg_8006" s="T528">n</ta>
            <ta e="T530" id="Seg_8007" s="T529">v</ta>
            <ta e="T531" id="Seg_8008" s="T530">dempro</ta>
            <ta e="T532" id="Seg_8009" s="T531">post</ta>
            <ta e="T533" id="Seg_8010" s="T532">n</ta>
            <ta e="T534" id="Seg_8011" s="T533">ptcl</ta>
            <ta e="T535" id="Seg_8012" s="T534">n</ta>
            <ta e="T536" id="Seg_8013" s="T535">n</ta>
            <ta e="T537" id="Seg_8014" s="T536">v</ta>
            <ta e="T538" id="Seg_8015" s="T537">v</ta>
            <ta e="T539" id="Seg_8016" s="T538">n</ta>
            <ta e="T540" id="Seg_8017" s="T539">v</ta>
            <ta e="T541" id="Seg_8018" s="T540">ptcl</ta>
            <ta e="T542" id="Seg_8019" s="T541">n</ta>
            <ta e="T543" id="Seg_8020" s="T542">n</ta>
            <ta e="T544" id="Seg_8021" s="T543">n</ta>
            <ta e="T545" id="Seg_8022" s="T544">n</ta>
            <ta e="T546" id="Seg_8023" s="T545">v</ta>
            <ta e="T547" id="Seg_8024" s="T546">post</ta>
            <ta e="T548" id="Seg_8025" s="T547">v</ta>
            <ta e="T549" id="Seg_8026" s="T548">aux</ta>
            <ta e="T550" id="Seg_8027" s="T549">n</ta>
            <ta e="T551" id="Seg_8028" s="T550">v</ta>
            <ta e="T552" id="Seg_8029" s="T551">n</ta>
            <ta e="T553" id="Seg_8030" s="T552">v</ta>
            <ta e="T554" id="Seg_8031" s="T553">n</ta>
            <ta e="T555" id="Seg_8032" s="T554">v</ta>
            <ta e="T556" id="Seg_8033" s="T555">n</ta>
            <ta e="T557" id="Seg_8034" s="T556">n</ta>
            <ta e="T558" id="Seg_8035" s="T557">n</ta>
            <ta e="T559" id="Seg_8036" s="T558">adv</ta>
            <ta e="T560" id="Seg_8037" s="T559">v</ta>
            <ta e="T561" id="Seg_8038" s="T560">n</ta>
            <ta e="T562" id="Seg_8039" s="T561">n</ta>
            <ta e="T563" id="Seg_8040" s="T562">n</ta>
            <ta e="T564" id="Seg_8041" s="T563">ptcl</ta>
            <ta e="T565" id="Seg_8042" s="T564">v</ta>
            <ta e="T566" id="Seg_8043" s="T565">adv</ta>
            <ta e="T567" id="Seg_8044" s="T566">v</ta>
            <ta e="T568" id="Seg_8045" s="T567">n</ta>
            <ta e="T569" id="Seg_8046" s="T568">n</ta>
            <ta e="T570" id="Seg_8047" s="T569">adj</ta>
            <ta e="T571" id="Seg_8048" s="T570">n</ta>
            <ta e="T572" id="Seg_8049" s="T571">n</ta>
            <ta e="T573" id="Seg_8050" s="T572">n</ta>
            <ta e="T574" id="Seg_8051" s="T573">v</ta>
            <ta e="T575" id="Seg_8052" s="T574">aux</ta>
            <ta e="T576" id="Seg_8053" s="T575">n</ta>
            <ta e="T577" id="Seg_8054" s="T576">v</ta>
            <ta e="T578" id="Seg_8055" s="T577">n</ta>
            <ta e="T579" id="Seg_8056" s="T578">n</ta>
            <ta e="T580" id="Seg_8057" s="T579">n</ta>
            <ta e="T581" id="Seg_8058" s="T580">v</ta>
            <ta e="T582" id="Seg_8059" s="T581">aux</ta>
            <ta e="T583" id="Seg_8060" s="T582">v</ta>
            <ta e="T584" id="Seg_8061" s="T583">collnum</ta>
            <ta e="T585" id="Seg_8062" s="T584">n</ta>
            <ta e="T586" id="Seg_8063" s="T585">n</ta>
            <ta e="T587" id="Seg_8064" s="T586">v</ta>
            <ta e="T588" id="Seg_8065" s="T587">n</ta>
            <ta e="T589" id="Seg_8066" s="T588">n</ta>
            <ta e="T590" id="Seg_8067" s="T589">v</ta>
            <ta e="T591" id="Seg_8068" s="T590">n</ta>
            <ta e="T592" id="Seg_8069" s="T591">n</ta>
            <ta e="T593" id="Seg_8070" s="T592">v</ta>
            <ta e="T594" id="Seg_8071" s="T593">adj</ta>
            <ta e="T595" id="Seg_8072" s="T594">n</ta>
            <ta e="T596" id="Seg_8073" s="T595">v</ta>
            <ta e="T597" id="Seg_8074" s="T596">adj</ta>
            <ta e="T598" id="Seg_8075" s="T597">n</ta>
            <ta e="T599" id="Seg_8076" s="T598">n</ta>
            <ta e="T600" id="Seg_8077" s="T599">n</ta>
            <ta e="T601" id="Seg_8078" s="T600">v</ta>
            <ta e="T602" id="Seg_8079" s="T601">dempro</ta>
            <ta e="T603" id="Seg_8080" s="T602">adj</ta>
            <ta e="T604" id="Seg_8081" s="T603">n</ta>
            <ta e="T605" id="Seg_8082" s="T604">adj</ta>
            <ta e="T606" id="Seg_8083" s="T605">n</ta>
            <ta e="T607" id="Seg_8084" s="T606">v</ta>
            <ta e="T608" id="Seg_8085" s="T607">adv</ta>
            <ta e="T609" id="Seg_8086" s="T608">ptcl</ta>
            <ta e="T610" id="Seg_8087" s="T609">n</ta>
            <ta e="T611" id="Seg_8088" s="T610">n</ta>
            <ta e="T612" id="Seg_8089" s="T611">v</ta>
            <ta e="T613" id="Seg_8090" s="T612">v</ta>
            <ta e="T614" id="Seg_8091" s="T613">propr</ta>
            <ta e="T615" id="Seg_8092" s="T614">propr</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_8093" s="T0">0.2.h:A</ta>
            <ta e="T2" id="Seg_8094" s="T1">np:Th</ta>
            <ta e="T3" id="Seg_8095" s="T2">0.3:Poss</ta>
            <ta e="T7" id="Seg_8096" s="T6">np.h:Th</ta>
            <ta e="T10" id="Seg_8097" s="T9">np.h:Poss</ta>
            <ta e="T11" id="Seg_8098" s="T10">np:Th</ta>
            <ta e="T17" id="Seg_8099" s="T16">0.3.h:Poss np:Th</ta>
            <ta e="T24" id="Seg_8100" s="T23">np.h:Th</ta>
            <ta e="T25" id="Seg_8101" s="T24">0.3.h:A</ta>
            <ta e="T30" id="Seg_8102" s="T29">np.h:Poss</ta>
            <ta e="T32" id="Seg_8103" s="T31">np.h:Th</ta>
            <ta e="T33" id="Seg_8104" s="T32">adv:Time</ta>
            <ta e="T34" id="Seg_8105" s="T33">adv:Time</ta>
            <ta e="T35" id="Seg_8106" s="T34">0.3.h:A</ta>
            <ta e="T36" id="Seg_8107" s="T35">np:Ins</ta>
            <ta e="T39" id="Seg_8108" s="T37">0.3.h:A</ta>
            <ta e="T40" id="Seg_8109" s="T39">adv:Time</ta>
            <ta e="T42" id="Seg_8110" s="T41">np.h:A</ta>
            <ta e="T43" id="Seg_8111" s="T42">np:Th</ta>
            <ta e="T46" id="Seg_8112" s="T45">np:G</ta>
            <ta e="T47" id="Seg_8113" s="T46">0.3.h:A</ta>
            <ta e="T50" id="Seg_8114" s="T48">pro.h:A</ta>
            <ta e="T51" id="Seg_8115" s="T50">0.3.h:Poss np:L</ta>
            <ta e="T55" id="Seg_8116" s="T54">0.3.h:A</ta>
            <ta e="T56" id="Seg_8117" s="T55">pro.h:Th</ta>
            <ta e="T59" id="Seg_8118" s="T58">0.3.h:E</ta>
            <ta e="T60" id="Seg_8119" s="T59">np:So</ta>
            <ta e="T63" id="Seg_8120" s="T62">np.h:A</ta>
            <ta e="T66" id="Seg_8121" s="T65">0.3.h:A</ta>
            <ta e="T67" id="Seg_8122" s="T66">0.2.h:Poss np.h:Th</ta>
            <ta e="T68" id="Seg_8123" s="T67">0.2.h:A</ta>
            <ta e="T73" id="Seg_8124" s="T72">0.1.h:A</ta>
            <ta e="T74" id="Seg_8125" s="T73">0.2.h:A</ta>
            <ta e="T77" id="Seg_8126" s="T76">np:Th</ta>
            <ta e="T79" id="Seg_8127" s="T77">0.2.h:A</ta>
            <ta e="T80" id="Seg_8128" s="T79">pro:Ins</ta>
            <ta e="T81" id="Seg_8129" s="T80">np.h:Th</ta>
            <ta e="T82" id="Seg_8130" s="T81">0.1.h:A</ta>
            <ta e="T85" id="Seg_8131" s="T84">0.3.h:E</ta>
            <ta e="T86" id="Seg_8132" s="T85">adv:Time</ta>
            <ta e="T87" id="Seg_8133" s="T86">0.3.h:A</ta>
            <ta e="T88" id="Seg_8134" s="T87">0.2.h:A</ta>
            <ta e="T91" id="Seg_8135" s="T90">0.1.h:E</ta>
            <ta e="T94" id="Seg_8136" s="T93">np.h:A</ta>
            <ta e="T95" id="Seg_8137" s="T94">np.h:Poss</ta>
            <ta e="T96" id="Seg_8138" s="T95">np:Th</ta>
            <ta e="T98" id="Seg_8139" s="T96">0.3.h:A</ta>
            <ta e="T101" id="Seg_8140" s="T100">np:Path</ta>
            <ta e="T103" id="Seg_8141" s="T101">0.3.h:A</ta>
            <ta e="T105" id="Seg_8142" s="T104">np.h:E</ta>
            <ta e="T109" id="Seg_8143" s="T108">0.3.h:Poss np:G</ta>
            <ta e="T110" id="Seg_8144" s="T109">0.3.h:Th</ta>
            <ta e="T111" id="Seg_8145" s="T110">0.3.h:Poss np.h:R</ta>
            <ta e="T112" id="Seg_8146" s="T111">0.3.h:A</ta>
            <ta e="T114" id="Seg_8147" s="T113">np.h:E</ta>
            <ta e="T116" id="Seg_8148" s="T115">pro.h:A</ta>
            <ta e="T120" id="Seg_8149" s="T119">np.h:P</ta>
            <ta e="T122" id="Seg_8150" s="T121">np:L</ta>
            <ta e="T125" id="Seg_8151" s="T124">0.1.h:Th</ta>
            <ta e="T127" id="Seg_8152" s="T126">np.h:A</ta>
            <ta e="T128" id="Seg_8153" s="T127">0.3.h:Poss np.h:Th</ta>
            <ta e="T132" id="Seg_8154" s="T131">np:Poss</ta>
            <ta e="T133" id="Seg_8155" s="T132">np.h:Th</ta>
            <ta e="T137" id="Seg_8156" s="T136">np:Poss</ta>
            <ta e="T138" id="Seg_8157" s="T137">np:St</ta>
            <ta e="T142" id="Seg_8158" s="T141">0.3.h:Poss np:Th</ta>
            <ta e="T143" id="Seg_8159" s="T142">0.3.h:Poss np:Th</ta>
            <ta e="T145" id="Seg_8160" s="T144">np:L</ta>
            <ta e="T148" id="Seg_8161" s="T147">np:Th</ta>
            <ta e="T151" id="Seg_8162" s="T150">np.h:A</ta>
            <ta e="T152" id="Seg_8163" s="T151">0.3.h:Poss np.h:Th</ta>
            <ta e="T155" id="Seg_8164" s="T154">np:Th</ta>
            <ta e="T157" id="Seg_8165" s="T156">pro:L</ta>
            <ta e="T159" id="Seg_8166" s="T158">0.2.h:A</ta>
            <ta e="T160" id="Seg_8167" s="T159">0.2.h:A</ta>
            <ta e="T165" id="Seg_8168" s="T164">np:G</ta>
            <ta e="T166" id="Seg_8169" s="T165">0.2.h:A</ta>
            <ta e="T167" id="Seg_8170" s="T166">0.2.h:Poss np:Th</ta>
            <ta e="T168" id="Seg_8171" s="T167">0.3.h:A</ta>
            <ta e="T169" id="Seg_8172" s="T168">0.3.h:Poss np.h:A</ta>
            <ta e="T173" id="Seg_8173" s="T172">np.h:A</ta>
            <ta e="T174" id="Seg_8174" s="T173">0.3.h:E</ta>
            <ta e="T175" id="Seg_8175" s="T174">0.3.h:Poss np:Th</ta>
            <ta e="T177" id="Seg_8176" s="T176">np:Th</ta>
            <ta e="T182" id="Seg_8177" s="T181">0.3.h:A</ta>
            <ta e="T183" id="Seg_8178" s="T182">0.3.h:Poss np:Th</ta>
            <ta e="T184" id="Seg_8179" s="T183">0.3.h:E</ta>
            <ta e="T186" id="Seg_8180" s="T185">0.3.h:Poss np:P</ta>
            <ta e="T189" id="Seg_8181" s="T188">np:Th</ta>
            <ta e="T191" id="Seg_8182" s="T190">0.3.h:A</ta>
            <ta e="T193" id="Seg_8183" s="T192">0.3.h:Th</ta>
            <ta e="T194" id="Seg_8184" s="T193">0.3.h:E</ta>
            <ta e="T195" id="Seg_8185" s="T194">0.3.h:Poss np:Th</ta>
            <ta e="T199" id="Seg_8186" s="T198">np.h:E</ta>
            <ta e="T200" id="Seg_8187" s="T199">0.3.h:A</ta>
            <ta e="T203" id="Seg_8188" s="T202">pro.h:Poss</ta>
            <ta e="T204" id="Seg_8189" s="T203">pro.h:Th</ta>
            <ta e="T208" id="Seg_8190" s="T207">0.3.h:A</ta>
            <ta e="T209" id="Seg_8191" s="T208">np.h:Th</ta>
            <ta e="T213" id="Seg_8192" s="T212">np:G</ta>
            <ta e="T214" id="Seg_8193" s="T213">0.3.h:A</ta>
            <ta e="T216" id="Seg_8194" s="T214">0.3.h:A</ta>
            <ta e="T217" id="Seg_8195" s="T216">np:Th</ta>
            <ta e="T219" id="Seg_8196" s="T218">np:Poss</ta>
            <ta e="T220" id="Seg_8197" s="T219">np:Ins</ta>
            <ta e="T222" id="Seg_8198" s="T221">np:Ins</ta>
            <ta e="T224" id="Seg_8199" s="T223">0.3.h:A</ta>
            <ta e="T226" id="Seg_8200" s="T225">np.h:A</ta>
            <ta e="T230" id="Seg_8201" s="T229">np:G</ta>
            <ta e="T231" id="Seg_8202" s="T230">0.3.h:A</ta>
            <ta e="T234" id="Seg_8203" s="T233">np.h:A</ta>
            <ta e="T236" id="Seg_8204" s="T235">0.3.h:Poss np:L</ta>
            <ta e="T239" id="Seg_8205" s="T238">0.2.h:A</ta>
            <ta e="T241" id="Seg_8206" s="T240">0.2.h:Poss np.h:Th</ta>
            <ta e="T243" id="Seg_8207" s="T242">0.1.h:A</ta>
            <ta e="T244" id="Seg_8208" s="T243">np.h:E</ta>
            <ta e="T249" id="Seg_8209" s="T248">0.3.h:Poss np.h:Th</ta>
            <ta e="T250" id="Seg_8210" s="T249">0.3.h:A</ta>
            <ta e="T252" id="Seg_8211" s="T251">np.h:A</ta>
            <ta e="T253" id="Seg_8212" s="T252">np:Th</ta>
            <ta e="T255" id="Seg_8213" s="T254">np:Th</ta>
            <ta e="T256" id="Seg_8214" s="T255">0.3.h:Poss</ta>
            <ta e="T260" id="Seg_8215" s="T259">adv:Time</ta>
            <ta e="T263" id="Seg_8216" s="T262">np:G</ta>
            <ta e="T266" id="Seg_8217" s="T265">np.h:A</ta>
            <ta e="T267" id="Seg_8218" s="T266">0.3.h:A</ta>
            <ta e="T268" id="Seg_8219" s="T267">0.3.h:A</ta>
            <ta e="T270" id="Seg_8220" s="T269">np:G</ta>
            <ta e="T271" id="Seg_8221" s="T270">0.2.h:Poss np:Th</ta>
            <ta e="T273" id="Seg_8222" s="T272">0.2.h:Poss np:Th</ta>
            <ta e="T275" id="Seg_8223" s="T274">0.2.h:A</ta>
            <ta e="T277" id="Seg_8224" s="T276">np.h:A</ta>
            <ta e="T281" id="Seg_8225" s="T280">0.3.h:Poss np.h:A</ta>
            <ta e="T283" id="Seg_8226" s="T282">np:G</ta>
            <ta e="T284" id="Seg_8227" s="T283">0.3.h:A</ta>
            <ta e="T285" id="Seg_8228" s="T284">0.3.h:Poss np:Th</ta>
            <ta e="T288" id="Seg_8229" s="T287">0.3.h:Poss np:P</ta>
            <ta e="T291" id="Seg_8230" s="T290">0.3.h:Poss np:Th</ta>
            <ta e="T293" id="Seg_8231" s="T292">0.3.h:A</ta>
            <ta e="T295" id="Seg_8232" s="T294">np.h:E</ta>
            <ta e="T296" id="Seg_8233" s="T295">0.3.h:A</ta>
            <ta e="T299" id="Seg_8234" s="T298">pro.h:E</ta>
            <ta e="T305" id="Seg_8235" s="T304">0.3.h:A</ta>
            <ta e="T308" id="Seg_8236" s="T307">np:Poss</ta>
            <ta e="T309" id="Seg_8237" s="T308">np:L</ta>
            <ta e="T311" id="Seg_8238" s="T309">0.3.h:A</ta>
            <ta e="T312" id="Seg_8239" s="T311">np:Th</ta>
            <ta e="T314" id="Seg_8240" s="T313">np:Poss</ta>
            <ta e="T315" id="Seg_8241" s="T314">np:Ins</ta>
            <ta e="T317" id="Seg_8242" s="T316">np:Ins</ta>
            <ta e="T319" id="Seg_8243" s="T318">0.3.h:A</ta>
            <ta e="T321" id="Seg_8244" s="T320">np.h:A</ta>
            <ta e="T323" id="Seg_8245" s="T322">np:Th</ta>
            <ta e="T324" id="Seg_8246" s="T323">0.3.h:A</ta>
            <ta e="T326" id="Seg_8247" s="T325">np:G</ta>
            <ta e="T327" id="Seg_8248" s="T326">0.3.h:A</ta>
            <ta e="T330" id="Seg_8249" s="T329">np.h:A</ta>
            <ta e="T332" id="Seg_8250" s="T331">0.3.h:Poss np:L</ta>
            <ta e="T335" id="Seg_8251" s="T334">0.2.h:A</ta>
            <ta e="T337" id="Seg_8252" s="T336">0.2.h:Poss np.h:Th</ta>
            <ta e="T339" id="Seg_8253" s="T338">0.1.h:A</ta>
            <ta e="T340" id="Seg_8254" s="T339">0.3.h:A</ta>
            <ta e="T342" id="Seg_8255" s="T341">0.3.h:E</ta>
            <ta e="T345" id="Seg_8256" s="T344">0.3.h:Poss np.h:Th</ta>
            <ta e="T346" id="Seg_8257" s="T345">0.3.h:A</ta>
            <ta e="T348" id="Seg_8258" s="T346">0.3.h:A</ta>
            <ta e="T349" id="Seg_8259" s="T348">np.h:A</ta>
            <ta e="T351" id="Seg_8260" s="T350">0.2.h:E</ta>
            <ta e="T353" id="Seg_8261" s="T352">pro.h:A</ta>
            <ta e="T354" id="Seg_8262" s="T353">adv:Time</ta>
            <ta e="T356" id="Seg_8263" s="T355">0.1.h:Poss np.h:Th</ta>
            <ta e="T358" id="Seg_8264" s="T357">0.1.h:A</ta>
            <ta e="T360" id="Seg_8265" s="T359">np.h:Th</ta>
            <ta e="T367" id="Seg_8266" s="T366">np.h:A</ta>
            <ta e="T369" id="Seg_8267" s="T368">0.3.h:Poss np.h:Th</ta>
            <ta e="T371" id="Seg_8268" s="T370">np:G</ta>
            <ta e="T372" id="Seg_8269" s="T371">0.3.h:A</ta>
            <ta e="T376" id="Seg_8270" s="T375">np:G</ta>
            <ta e="T377" id="Seg_8271" s="T376">0.2.h:Poss np:Th</ta>
            <ta e="T378" id="Seg_8272" s="T377">0.2.h:A</ta>
            <ta e="T380" id="Seg_8273" s="T378">0.1.h:A</ta>
            <ta e="T381" id="Seg_8274" s="T380">0.1.h:A</ta>
            <ta e="T382" id="Seg_8275" s="T381">pro.h:P</ta>
            <ta e="T384" id="Seg_8276" s="T382">0.2.h:A</ta>
            <ta e="T385" id="Seg_8277" s="T384">pro:Cau</ta>
            <ta e="T386" id="Seg_8278" s="T385">pro.h:R</ta>
            <ta e="T387" id="Seg_8279" s="T386">0.3.h:A</ta>
            <ta e="T391" id="Seg_8280" s="T390">np.h:E</ta>
            <ta e="T392" id="Seg_8281" s="T391">pro:Th</ta>
            <ta e="T395" id="Seg_8282" s="T394">0.1.h:Poss np:Th</ta>
            <ta e="T396" id="Seg_8283" s="T395">0.1.h:A</ta>
            <ta e="T403" id="Seg_8284" s="T402">np:Th</ta>
            <ta e="T406" id="Seg_8285" s="T405">np:Th</ta>
            <ta e="T408" id="Seg_8286" s="T406">0.3.h:A</ta>
            <ta e="T410" id="Seg_8287" s="T409">0.3.h:Poss np:Th</ta>
            <ta e="T411" id="Seg_8288" s="T410">0.3.h:A</ta>
            <ta e="T414" id="Seg_8289" s="T413">np:G</ta>
            <ta e="T415" id="Seg_8290" s="T414">np:Poss</ta>
            <ta e="T416" id="Seg_8291" s="T415">np:P</ta>
            <ta e="T421" id="Seg_8292" s="T420">np.h:Poss</ta>
            <ta e="T422" id="Seg_8293" s="T421">np.h:E</ta>
            <ta e="T424" id="Seg_8294" s="T423">pro:Cau</ta>
            <ta e="T426" id="Seg_8295" s="T425">np.h:A</ta>
            <ta e="T429" id="Seg_8296" s="T428">np:L</ta>
            <ta e="T430" id="Seg_8297" s="T429">0.3.h:Th</ta>
            <ta e="T432" id="Seg_8298" s="T431">np:P</ta>
            <ta e="T434" id="Seg_8299" s="T432">0.3.h:A</ta>
            <ta e="T436" id="Seg_8300" s="T435">np.h:E</ta>
            <ta e="T437" id="Seg_8301" s="T436">0.3.h:A</ta>
            <ta e="T440" id="Seg_8302" s="T439">np:P</ta>
            <ta e="T441" id="Seg_8303" s="T440">0.2.h:A</ta>
            <ta e="T443" id="Seg_8304" s="T442">np:L</ta>
            <ta e="T449" id="Seg_8305" s="T448">pro.h:A</ta>
            <ta e="T450" id="Seg_8306" s="T449">pro.h:R</ta>
            <ta e="T451" id="Seg_8307" s="T450">pro:Th</ta>
            <ta e="T459" id="Seg_8308" s="T458">np.h:A</ta>
            <ta e="T463" id="Seg_8309" s="T462">np:Th</ta>
            <ta e="T465" id="Seg_8310" s="T464">pro.h:Th</ta>
            <ta e="T470" id="Seg_8311" s="T469">np:P</ta>
            <ta e="T471" id="Seg_8312" s="T470">0.1.h:A</ta>
            <ta e="T472" id="Seg_8313" s="T471">0.1.h:E</ta>
            <ta e="T476" id="Seg_8314" s="T475">np:L</ta>
            <ta e="T477" id="Seg_8315" s="T476">np:Th</ta>
            <ta e="T482" id="Seg_8316" s="T480">0.3.h:A</ta>
            <ta e="T484" id="Seg_8317" s="T483">np.h:E</ta>
            <ta e="T489" id="Seg_8318" s="T488">np:G</ta>
            <ta e="T491" id="Seg_8319" s="T490">np.h:A</ta>
            <ta e="T493" id="Seg_8320" s="T492">adv:L</ta>
            <ta e="T494" id="Seg_8321" s="T493">np:Th</ta>
            <ta e="T496" id="Seg_8322" s="T495">pro.h:E</ta>
            <ta e="T499" id="Seg_8323" s="T498">np.h:A</ta>
            <ta e="T501" id="Seg_8324" s="T500">pro.h:E</ta>
            <ta e="T503" id="Seg_8325" s="T502">np:A</ta>
            <ta e="T506" id="Seg_8326" s="T505">pro:Th</ta>
            <ta e="T508" id="Seg_8327" s="T507">pro:Th</ta>
            <ta e="T510" id="Seg_8328" s="T509">np:Th</ta>
            <ta e="T513" id="Seg_8329" s="T512">np:Th</ta>
            <ta e="T519" id="Seg_8330" s="T518">np.h:E</ta>
            <ta e="T521" id="Seg_8331" s="T520">np:A</ta>
            <ta e="T523" id="Seg_8332" s="T522">np:L</ta>
            <ta e="T525" id="Seg_8333" s="T524">pro.h:E</ta>
            <ta e="T527" id="Seg_8334" s="T526">pro.h:Th</ta>
            <ta e="T529" id="Seg_8335" s="T528">np:L</ta>
            <ta e="T531" id="Seg_8336" s="T530">pro:Cau</ta>
            <ta e="T533" id="Seg_8337" s="T532">np.h:A</ta>
            <ta e="T536" id="Seg_8338" s="T535">np:L</ta>
            <ta e="T539" id="Seg_8339" s="T538">np.h:A</ta>
            <ta e="T540" id="Seg_8340" s="T539">0.2.h:A</ta>
            <ta e="T543" id="Seg_8341" s="T542">np:Th</ta>
            <ta e="T545" id="Seg_8342" s="T544">np.h:E</ta>
            <ta e="T549" id="Seg_8343" s="T547">0.3.h:E</ta>
            <ta e="T550" id="Seg_8344" s="T549">np:G</ta>
            <ta e="T551" id="Seg_8345" s="T550">0.3.h:A</ta>
            <ta e="T552" id="Seg_8346" s="T551">np:St</ta>
            <ta e="T553" id="Seg_8347" s="T552">0.3.h:E</ta>
            <ta e="T554" id="Seg_8348" s="T553">np.h:A</ta>
            <ta e="T555" id="Seg_8349" s="T554">0.3.h:A</ta>
            <ta e="T556" id="Seg_8350" s="T555">0.3.h:Poss np:Th</ta>
            <ta e="T558" id="Seg_8351" s="T557">np:G</ta>
            <ta e="T562" id="Seg_8352" s="T561">np.h:Poss</ta>
            <ta e="T563" id="Seg_8353" s="T562">np:P</ta>
            <ta e="T566" id="Seg_8354" s="T565">adv:Time</ta>
            <ta e="T567" id="Seg_8355" s="T566">0.3.h:P</ta>
            <ta e="T568" id="Seg_8356" s="T567">np.h:E</ta>
            <ta e="T569" id="Seg_8357" s="T568">np.h:Poss</ta>
            <ta e="T571" id="Seg_8358" s="T570">np.h:A</ta>
            <ta e="T572" id="Seg_8359" s="T571">np:So</ta>
            <ta e="T573" id="Seg_8360" s="T572">np:G</ta>
            <ta e="T575" id="Seg_8361" s="T574">0.3.h:A</ta>
            <ta e="T576" id="Seg_8362" s="T575">0.3.h:Poss np.h:Th</ta>
            <ta e="T579" id="Seg_8363" s="T578">np:P</ta>
            <ta e="T580" id="Seg_8364" s="T579">np:Ins</ta>
            <ta e="T582" id="Seg_8365" s="T580">0.3.h:A</ta>
            <ta e="T583" id="Seg_8366" s="T582">0.3.h:A</ta>
            <ta e="T584" id="Seg_8367" s="T583">np.h:Th</ta>
            <ta e="T586" id="Seg_8368" s="T585">0.3.h:Poss np:G</ta>
            <ta e="T589" id="Seg_8369" s="T588">np.h:A</ta>
            <ta e="T590" id="Seg_8370" s="T589">0.3.h:E</ta>
            <ta e="T591" id="Seg_8371" s="T590">0.3.h:Poss np.h:B</ta>
            <ta e="T592" id="Seg_8372" s="T591">np:P</ta>
            <ta e="T595" id="Seg_8373" s="T594">np:Th</ta>
            <ta e="T596" id="Seg_8374" s="T595">0.3.h:A</ta>
            <ta e="T598" id="Seg_8375" s="T597">np.h:E</ta>
            <ta e="T600" id="Seg_8376" s="T599">0.3.h:Poss np:St</ta>
            <ta e="T604" id="Seg_8377" s="T603">np.h:E</ta>
            <ta e="T606" id="Seg_8378" s="T605">0.3.h:Poss np:St</ta>
            <ta e="T608" id="Seg_8379" s="T607">adv:Time</ta>
            <ta e="T611" id="Seg_8380" s="T610">np.h:St</ta>
            <ta e="T612" id="Seg_8381" s="T611">0.3.h:E</ta>
            <ta e="T615" id="Seg_8382" s="T613">np.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1" id="Seg_8383" s="T0">0.2.h:S</ta>
            <ta e="T2" id="Seg_8384" s="T1">np:O</ta>
            <ta e="T3" id="Seg_8385" s="T2">np:S</ta>
            <ta e="T5" id="Seg_8386" s="T3">n:pred</ta>
            <ta e="T7" id="Seg_8387" s="T6">np.h:S</ta>
            <ta e="T8" id="Seg_8388" s="T7">ptcl:pred</ta>
            <ta e="T11" id="Seg_8389" s="T10">np:S</ta>
            <ta e="T12" id="Seg_8390" s="T11">adj:pred</ta>
            <ta e="T15" id="Seg_8391" s="T14">adj:pred</ta>
            <ta e="T17" id="Seg_8392" s="T16">np:S</ta>
            <ta e="T21" id="Seg_8393" s="T20">v:pred</ta>
            <ta e="T24" id="Seg_8394" s="T23">np.h:O</ta>
            <ta e="T25" id="Seg_8395" s="T24">0.3.h:S v:pred</ta>
            <ta e="T30" id="Seg_8396" s="T29">np.h:S</ta>
            <ta e="T32" id="Seg_8397" s="T31">adj:pred</ta>
            <ta e="T35" id="Seg_8398" s="T34">0.3.h:S v:pred</ta>
            <ta e="T39" id="Seg_8399" s="T37">0.3.h:S v:pred</ta>
            <ta e="T42" id="Seg_8400" s="T41">np.h:S</ta>
            <ta e="T44" id="Seg_8401" s="T42">s:purp</ta>
            <ta e="T45" id="Seg_8402" s="T44">v:pred</ta>
            <ta e="T48" id="Seg_8403" s="T45">s:temp</ta>
            <ta e="T50" id="Seg_8404" s="T48">pro.h:S</ta>
            <ta e="T53" id="Seg_8405" s="T51">v:pred</ta>
            <ta e="T55" id="Seg_8406" s="T54">0.3.h:S v:pred</ta>
            <ta e="T56" id="Seg_8407" s="T55">pro.h:S</ta>
            <ta e="T57" id="Seg_8408" s="T56">cop</ta>
            <ta e="T59" id="Seg_8409" s="T58">0.3.h:S v:pred</ta>
            <ta e="T61" id="Seg_8410" s="T60">v:pred</ta>
            <ta e="T63" id="Seg_8411" s="T62">np.h:S</ta>
            <ta e="T66" id="Seg_8412" s="T65">0.3.h:S v:pred</ta>
            <ta e="T67" id="Seg_8413" s="T66">np.h:O</ta>
            <ta e="T68" id="Seg_8414" s="T67">0.2.h:S v:pred</ta>
            <ta e="T73" id="Seg_8415" s="T71">s:purp</ta>
            <ta e="T74" id="Seg_8416" s="T73">s:cond</ta>
            <ta e="T77" id="Seg_8417" s="T76">np:O</ta>
            <ta e="T79" id="Seg_8418" s="T77">0.2.h:S v:pred</ta>
            <ta e="T81" id="Seg_8419" s="T80">np.h:O</ta>
            <ta e="T82" id="Seg_8420" s="T81">0.1.h:S v:pred</ta>
            <ta e="T85" id="Seg_8421" s="T84">0.3.h:S v:pred</ta>
            <ta e="T87" id="Seg_8422" s="T86">0.3.h:S v:pred</ta>
            <ta e="T88" id="Seg_8423" s="T87">s:cond</ta>
            <ta e="T91" id="Seg_8424" s="T90">0.1.h:S v:pred</ta>
            <ta e="T92" id="Seg_8425" s="T91">v:pred</ta>
            <ta e="T94" id="Seg_8426" s="T93">np.h:S</ta>
            <ta e="T99" id="Seg_8427" s="T94">s:temp</ta>
            <ta e="T103" id="Seg_8428" s="T101">0.3.h:S v:pred</ta>
            <ta e="T105" id="Seg_8429" s="T104">np.h:S</ta>
            <ta e="T107" id="Seg_8430" s="T106">v:pred</ta>
            <ta e="T110" id="Seg_8431" s="T107">s:adv</ta>
            <ta e="T112" id="Seg_8432" s="T111">0.3.h:S v:pred</ta>
            <ta e="T114" id="Seg_8433" s="T113">np.h:S</ta>
            <ta e="T115" id="Seg_8434" s="T114">v:pred</ta>
            <ta e="T116" id="Seg_8435" s="T115">pro.h:S</ta>
            <ta e="T118" id="Seg_8436" s="T117">v:pred</ta>
            <ta e="T124" id="Seg_8437" s="T123">n:pred</ta>
            <ta e="T125" id="Seg_8438" s="T124">0.1.h:S cop</ta>
            <ta e="T127" id="Seg_8439" s="T126">np.h:S</ta>
            <ta e="T128" id="Seg_8440" s="T127">np.h:O</ta>
            <ta e="T131" id="Seg_8441" s="T129">v:pred</ta>
            <ta e="T133" id="Seg_8442" s="T132">np.h:S</ta>
            <ta e="T134" id="Seg_8443" s="T133">adj:pred</ta>
            <ta e="T136" id="Seg_8444" s="T135">adj:pred</ta>
            <ta e="T138" id="Seg_8445" s="T137">np:S</ta>
            <ta e="T139" id="Seg_8446" s="T138">v:pred</ta>
            <ta e="T142" id="Seg_8447" s="T141">0.3.h:S adj:pred</ta>
            <ta e="T143" id="Seg_8448" s="T142">0.3.h:S adj:pred</ta>
            <ta e="T148" id="Seg_8449" s="T147">np:S</ta>
            <ta e="T149" id="Seg_8450" s="T148">v:pred</ta>
            <ta e="T151" id="Seg_8451" s="T150">np.h:S</ta>
            <ta e="T153" id="Seg_8452" s="T151">s:adv</ta>
            <ta e="T155" id="Seg_8453" s="T154">np:O</ta>
            <ta e="T156" id="Seg_8454" s="T155">v:pred</ta>
            <ta e="T159" id="Seg_8455" s="T158">0.2.h:S v:pred</ta>
            <ta e="T160" id="Seg_8456" s="T159">0.2.h:S v:pred</ta>
            <ta e="T166" id="Seg_8457" s="T165">0.2.h:S v:pred</ta>
            <ta e="T167" id="Seg_8458" s="T166">np:O</ta>
            <ta e="T168" id="Seg_8459" s="T167">0.3.h:S v:pred</ta>
            <ta e="T171" id="Seg_8460" s="T168">s:temp</ta>
            <ta e="T173" id="Seg_8461" s="T172">np.h:S</ta>
            <ta e="T174" id="Seg_8462" s="T173">s:adv</ta>
            <ta e="T175" id="Seg_8463" s="T174">np:O</ta>
            <ta e="T176" id="Seg_8464" s="T175">v:pred</ta>
            <ta e="T177" id="Seg_8465" s="T176">np:S</ta>
            <ta e="T178" id="Seg_8466" s="T177">n:pred</ta>
            <ta e="T179" id="Seg_8467" s="T178">cop</ta>
            <ta e="T182" id="Seg_8468" s="T181">0.3.h:S v:pred</ta>
            <ta e="T183" id="Seg_8469" s="T182">np:O</ta>
            <ta e="T184" id="Seg_8470" s="T183">0.3.h:S v:pred</ta>
            <ta e="T186" id="Seg_8471" s="T185">np:S</ta>
            <ta e="T188" id="Seg_8472" s="T187">v:pred</ta>
            <ta e="T192" id="Seg_8473" s="T188">s:temp</ta>
            <ta e="T193" id="Seg_8474" s="T192">0.3.h:S v:pred</ta>
            <ta e="T194" id="Seg_8475" s="T193">s:adv</ta>
            <ta e="T195" id="Seg_8476" s="T194">np:S</ta>
            <ta e="T197" id="Seg_8477" s="T196">v:pred</ta>
            <ta e="T199" id="Seg_8478" s="T198">np.h:S</ta>
            <ta e="T200" id="Seg_8479" s="T199">s:adv</ta>
            <ta e="T202" id="Seg_8480" s="T201">v:pred</ta>
            <ta e="T204" id="Seg_8481" s="T203">pro.h:S</ta>
            <ta e="T205" id="Seg_8482" s="T204">n:pred</ta>
            <ta e="T206" id="Seg_8483" s="T205">cop</ta>
            <ta e="T208" id="Seg_8484" s="T207">0.3.h:S v:pred</ta>
            <ta e="T214" id="Seg_8485" s="T208">s:adv</ta>
            <ta e="T216" id="Seg_8486" s="T214">0.3.h:S v:pred</ta>
            <ta e="T217" id="Seg_8487" s="T216">np:O</ta>
            <ta e="T224" id="Seg_8488" s="T223">0.3.h:S v:pred</ta>
            <ta e="T226" id="Seg_8489" s="T225">np.h:S</ta>
            <ta e="T228" id="Seg_8490" s="T227">s:purp</ta>
            <ta e="T229" id="Seg_8491" s="T228">v:pred</ta>
            <ta e="T232" id="Seg_8492" s="T229">s:temp</ta>
            <ta e="T234" id="Seg_8493" s="T233">np.h:S</ta>
            <ta e="T238" id="Seg_8494" s="T236">v:pred</ta>
            <ta e="T239" id="Seg_8495" s="T238">0.2.h:S v:pred</ta>
            <ta e="T241" id="Seg_8496" s="T240">np.h:O</ta>
            <ta e="T243" id="Seg_8497" s="T241">s:purp</ta>
            <ta e="T244" id="Seg_8498" s="T243">np.h:S</ta>
            <ta e="T246" id="Seg_8499" s="T245">v:pred</ta>
            <ta e="T249" id="Seg_8500" s="T248">np.h:O</ta>
            <ta e="T250" id="Seg_8501" s="T249">0.3.h:S v:pred</ta>
            <ta e="T254" id="Seg_8502" s="T250">s:temp</ta>
            <ta e="T256" id="Seg_8503" s="T255">0.3.h:S ptcl:pred</ta>
            <ta e="T259" id="Seg_8504" s="T258">0.3.h:S v:pred</ta>
            <ta e="T264" id="Seg_8505" s="T263">v:pred</ta>
            <ta e="T266" id="Seg_8506" s="T265">np.h:S</ta>
            <ta e="T267" id="Seg_8507" s="T266">s:adv</ta>
            <ta e="T268" id="Seg_8508" s="T267">0.3.h:S v:pred</ta>
            <ta e="T271" id="Seg_8509" s="T270">np:O</ta>
            <ta e="T273" id="Seg_8510" s="T272">np:O</ta>
            <ta e="T275" id="Seg_8511" s="T274">0.2.h:S v:pred</ta>
            <ta e="T280" id="Seg_8512" s="T275">s:temp</ta>
            <ta e="T281" id="Seg_8513" s="T280">np.h:S</ta>
            <ta e="T284" id="Seg_8514" s="T281">s:adv</ta>
            <ta e="T285" id="Seg_8515" s="T284">np:O</ta>
            <ta e="T286" id="Seg_8516" s="T285">v:pred</ta>
            <ta e="T288" id="Seg_8517" s="T287">np:S</ta>
            <ta e="T290" id="Seg_8518" s="T289">v:pred</ta>
            <ta e="T291" id="Seg_8519" s="T290">np:O</ta>
            <ta e="T293" id="Seg_8520" s="T292">0.3.h:S v:pred</ta>
            <ta e="T295" id="Seg_8521" s="T294">np.h:S</ta>
            <ta e="T296" id="Seg_8522" s="T295">s:adv</ta>
            <ta e="T298" id="Seg_8523" s="T297">v:pred</ta>
            <ta e="T303" id="Seg_8524" s="T302">np:S</ta>
            <ta e="T304" id="Seg_8525" s="T303">ptcl:pred</ta>
            <ta e="T306" id="Seg_8526" s="T304">s:temp</ta>
            <ta e="T311" id="Seg_8527" s="T309">0.3.h:S v:pred</ta>
            <ta e="T312" id="Seg_8528" s="T311">np:O</ta>
            <ta e="T319" id="Seg_8529" s="T318">0.3.h:S v:pred</ta>
            <ta e="T321" id="Seg_8530" s="T320">np.h:S</ta>
            <ta e="T324" id="Seg_8531" s="T322">s:purp</ta>
            <ta e="T325" id="Seg_8532" s="T324">v:pred</ta>
            <ta e="T328" id="Seg_8533" s="T325">s:temp</ta>
            <ta e="T330" id="Seg_8534" s="T329">np.h:S</ta>
            <ta e="T334" id="Seg_8535" s="T332">v:pred</ta>
            <ta e="T335" id="Seg_8536" s="T334">0.2.h:S v:pred</ta>
            <ta e="T337" id="Seg_8537" s="T336">np.h:O</ta>
            <ta e="T339" id="Seg_8538" s="T337">s:purp</ta>
            <ta e="T340" id="Seg_8539" s="T339">0.3.h:S v:pred</ta>
            <ta e="T342" id="Seg_8540" s="T341">0.3.h:S v:pred</ta>
            <ta e="T345" id="Seg_8541" s="T344">np.h:O</ta>
            <ta e="T346" id="Seg_8542" s="T345">0.3.h:S v:pred</ta>
            <ta e="T348" id="Seg_8543" s="T346">s:adv</ta>
            <ta e="T349" id="Seg_8544" s="T348">np.h:S</ta>
            <ta e="T350" id="Seg_8545" s="T349">v:pred</ta>
            <ta e="T351" id="Seg_8546" s="T350">0.2.h:S v:pred</ta>
            <ta e="T353" id="Seg_8547" s="T352">pro.h:S</ta>
            <ta e="T355" id="Seg_8548" s="T354">v:pred</ta>
            <ta e="T356" id="Seg_8549" s="T355">np.h:O</ta>
            <ta e="T358" id="Seg_8550" s="T357">0.1.h:S v:pred</ta>
            <ta e="T360" id="Seg_8551" s="T359">np:S</ta>
            <ta e="T363" id="Seg_8552" s="T362">ptcl:pred</ta>
            <ta e="T367" id="Seg_8553" s="T366">np.h:S</ta>
            <ta e="T373" id="Seg_8554" s="T367">s:temp</ta>
            <ta e="T374" id="Seg_8555" s="T373">v:pred</ta>
            <ta e="T377" id="Seg_8556" s="T376">np:O</ta>
            <ta e="T378" id="Seg_8557" s="T377">0.2.h:S v:pred</ta>
            <ta e="T380" id="Seg_8558" s="T378">s:temp</ta>
            <ta e="T381" id="Seg_8559" s="T380">0.1.h:S v:pred</ta>
            <ta e="T382" id="Seg_8560" s="T381">pro.h:O</ta>
            <ta e="T384" id="Seg_8561" s="T382">0.2.h:S v:pred</ta>
            <ta e="T386" id="Seg_8562" s="T385">pro.h:O</ta>
            <ta e="T387" id="Seg_8563" s="T386">0.3.h:S v:pred</ta>
            <ta e="T389" id="Seg_8564" s="T388">v:pred</ta>
            <ta e="T391" id="Seg_8565" s="T390">np.h:S</ta>
            <ta e="T392" id="Seg_8566" s="T391">pro:S</ta>
            <ta e="T393" id="Seg_8567" s="T392">cop</ta>
            <ta e="T396" id="Seg_8568" s="T394">s:cond</ta>
            <ta e="T397" id="Seg_8569" s="T396">np:S</ta>
            <ta e="T398" id="Seg_8570" s="T397">n:pred</ta>
            <ta e="T403" id="Seg_8571" s="T402">np:S</ta>
            <ta e="T404" id="Seg_8572" s="T403">ptcl:pred</ta>
            <ta e="T409" id="Seg_8573" s="T405">s:temp</ta>
            <ta e="T410" id="Seg_8574" s="T409">np:O</ta>
            <ta e="T411" id="Seg_8575" s="T410">0.3.h:S v:pred</ta>
            <ta e="T416" id="Seg_8576" s="T415">np:S</ta>
            <ta e="T419" id="Seg_8577" s="T417">v:pred</ta>
            <ta e="T422" id="Seg_8578" s="T421">np.h:S</ta>
            <ta e="T423" id="Seg_8579" s="T422">v:pred</ta>
            <ta e="T427" id="Seg_8580" s="T423">s:comp</ta>
            <ta e="T431" id="Seg_8581" s="T427">s:temp</ta>
            <ta e="T432" id="Seg_8582" s="T431">np:O</ta>
            <ta e="T434" id="Seg_8583" s="T432">0.3.h:S v:pred</ta>
            <ta e="T436" id="Seg_8584" s="T435">np.h:S</ta>
            <ta e="T437" id="Seg_8585" s="T436">s:adv</ta>
            <ta e="T438" id="Seg_8586" s="T437">v:pred</ta>
            <ta e="T440" id="Seg_8587" s="T439">np:O</ta>
            <ta e="T441" id="Seg_8588" s="T440">0.2.h:S v:pred</ta>
            <ta e="T444" id="Seg_8589" s="T443">n:pred</ta>
            <ta e="T449" id="Seg_8590" s="T448">pro.h:S</ta>
            <ta e="T452" id="Seg_8591" s="T451">s:rel</ta>
            <ta e="T453" id="Seg_8592" s="T452">v:pred</ta>
            <ta e="T454" id="Seg_8593" s="T453">np:S</ta>
            <ta e="T455" id="Seg_8594" s="T454">quant:pred</ta>
            <ta e="T456" id="Seg_8595" s="T455">cop</ta>
            <ta e="T458" id="Seg_8596" s="T457">v:pred</ta>
            <ta e="T459" id="Seg_8597" s="T458">np.h:S</ta>
            <ta e="T463" id="Seg_8598" s="T462">np:S</ta>
            <ta e="T464" id="Seg_8599" s="T463">ptcl:pred</ta>
            <ta e="T465" id="Seg_8600" s="T464">pro.h:S</ta>
            <ta e="T468" id="Seg_8601" s="T467">v:pred</ta>
            <ta e="T471" id="Seg_8602" s="T468">s:comp</ta>
            <ta e="T472" id="Seg_8603" s="T471">0.1.h:S v:pred</ta>
            <ta e="T477" id="Seg_8604" s="T476">n:pred</ta>
            <ta e="T482" id="Seg_8605" s="T480">0.3.h:S v:pred</ta>
            <ta e="T484" id="Seg_8606" s="T483">np.h:S</ta>
            <ta e="T486" id="Seg_8607" s="T485">v:pred</ta>
            <ta e="T491" id="Seg_8608" s="T490">np.h:S</ta>
            <ta e="T492" id="Seg_8609" s="T491">v:pred</ta>
            <ta e="T494" id="Seg_8610" s="T493">n:pred</ta>
            <ta e="T496" id="Seg_8611" s="T495">pro.h:S</ta>
            <ta e="T497" id="Seg_8612" s="T496">v:pred</ta>
            <ta e="T499" id="Seg_8613" s="T498">np.h:S</ta>
            <ta e="T500" id="Seg_8614" s="T499">v:pred</ta>
            <ta e="T501" id="Seg_8615" s="T500">pro.h:S</ta>
            <ta e="T502" id="Seg_8616" s="T501">v:pred</ta>
            <ta e="T503" id="Seg_8617" s="T502">np:S</ta>
            <ta e="T505" id="Seg_8618" s="T503">v:pred</ta>
            <ta e="T506" id="Seg_8619" s="T505">pro:S</ta>
            <ta e="T507" id="Seg_8620" s="T506">adj:pred</ta>
            <ta e="T508" id="Seg_8621" s="T507">pro:S</ta>
            <ta e="T509" id="Seg_8622" s="T508">adj:pred</ta>
            <ta e="T510" id="Seg_8623" s="T509">np:S</ta>
            <ta e="T511" id="Seg_8624" s="T510">adj:pred</ta>
            <ta e="T513" id="Seg_8625" s="T512">n:pred</ta>
            <ta e="T515" id="Seg_8626" s="T514">n:pred</ta>
            <ta e="T517" id="Seg_8627" s="T516">v:pred</ta>
            <ta e="T519" id="Seg_8628" s="T518">np.h:S</ta>
            <ta e="T521" id="Seg_8629" s="T520">np:S</ta>
            <ta e="T524" id="Seg_8630" s="T523">v:pred</ta>
            <ta e="T525" id="Seg_8631" s="T524">pro.h:S</ta>
            <ta e="T526" id="Seg_8632" s="T525">v:pred</ta>
            <ta e="T527" id="Seg_8633" s="T526">pro.h:S</ta>
            <ta e="T530" id="Seg_8634" s="T529">v:pred</ta>
            <ta e="T533" id="Seg_8635" s="T532">np.h:S</ta>
            <ta e="T537" id="Seg_8636" s="T536">v:pred</ta>
            <ta e="T538" id="Seg_8637" s="T537">v:pred</ta>
            <ta e="T539" id="Seg_8638" s="T538">np.h:S</ta>
            <ta e="T540" id="Seg_8639" s="T539">0.2.h:S v:pred</ta>
            <ta e="T543" id="Seg_8640" s="T542">np:O</ta>
            <ta e="T547" id="Seg_8641" s="T543">s:temp</ta>
            <ta e="T549" id="Seg_8642" s="T547">0.3.h:S v:pred</ta>
            <ta e="T551" id="Seg_8643" s="T550">0.3.h:S v:pred</ta>
            <ta e="T553" id="Seg_8644" s="T551">s:purp</ta>
            <ta e="T554" id="Seg_8645" s="T553">np.h:S</ta>
            <ta e="T555" id="Seg_8646" s="T554">s:adv</ta>
            <ta e="T556" id="Seg_8647" s="T555">np:O</ta>
            <ta e="T560" id="Seg_8648" s="T559">v:pred</ta>
            <ta e="T563" id="Seg_8649" s="T562">np:S</ta>
            <ta e="T565" id="Seg_8650" s="T564">v:pred</ta>
            <ta e="T567" id="Seg_8651" s="T566">0.3.h:S v:pred</ta>
            <ta e="T568" id="Seg_8652" s="T567">np.h:S</ta>
            <ta e="T575" id="Seg_8653" s="T570">s:adv</ta>
            <ta e="T576" id="Seg_8654" s="T575">np.h:O</ta>
            <ta e="T577" id="Seg_8655" s="T576">v:pred</ta>
            <ta e="T582" id="Seg_8656" s="T577">s:adv</ta>
            <ta e="T583" id="Seg_8657" s="T582">0.3.h:S v:pred</ta>
            <ta e="T584" id="Seg_8658" s="T583">np.h:S</ta>
            <ta e="T587" id="Seg_8659" s="T586">v:pred</ta>
            <ta e="T589" id="Seg_8660" s="T588">np.h:S</ta>
            <ta e="T590" id="Seg_8661" s="T589">s:adv</ta>
            <ta e="T592" id="Seg_8662" s="T591">np:O</ta>
            <ta e="T593" id="Seg_8663" s="T592">v:pred</ta>
            <ta e="T595" id="Seg_8664" s="T594">np:O</ta>
            <ta e="T596" id="Seg_8665" s="T595">0.3.h:S v:pred</ta>
            <ta e="T601" id="Seg_8666" s="T596">s:temp</ta>
            <ta e="T607" id="Seg_8667" s="T601">s:temp</ta>
            <ta e="T611" id="Seg_8668" s="T610">np.h:O</ta>
            <ta e="T612" id="Seg_8669" s="T611">0.3.h:S v:pred</ta>
            <ta e="T613" id="Seg_8670" s="T612">v:pred</ta>
            <ta e="T615" id="Seg_8671" s="T613">np.h:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST">
            <ta e="T1" id="Seg_8672" s="T0">0.new</ta>
            <ta e="T2" id="Seg_8673" s="T1">new</ta>
            <ta e="T3" id="Seg_8674" s="T2">accs-inf</ta>
            <ta e="T7" id="Seg_8675" s="T6">new</ta>
            <ta e="T10" id="Seg_8676" s="T9">giv-active</ta>
            <ta e="T11" id="Seg_8677" s="T10">accs-inf</ta>
            <ta e="T17" id="Seg_8678" s="T16">accs-inf</ta>
            <ta e="T24" id="Seg_8679" s="T23">giv-inactive</ta>
            <ta e="T30" id="Seg_8680" s="T29">giv-active</ta>
            <ta e="T32" id="Seg_8681" s="T31">new</ta>
            <ta e="T35" id="Seg_8682" s="T34">0.giv-active</ta>
            <ta e="T39" id="Seg_8683" s="T37">0.accs-aggr</ta>
            <ta e="T42" id="Seg_8684" s="T41">giv-active</ta>
            <ta e="T43" id="Seg_8685" s="T42">new</ta>
            <ta e="T46" id="Seg_8686" s="T45">new</ta>
            <ta e="T47" id="Seg_8687" s="T46">0.giv-active</ta>
            <ta e="T50" id="Seg_8688" s="T48">new</ta>
            <ta e="T51" id="Seg_8689" s="T50">giv-inactive</ta>
            <ta e="T55" id="Seg_8690" s="T54">0.giv-active</ta>
            <ta e="T59" id="Seg_8691" s="T58">0.giv-inactive 0.quot-th</ta>
            <ta e="T60" id="Seg_8692" s="T59">giv-inactive</ta>
            <ta e="T63" id="Seg_8693" s="T62">new</ta>
            <ta e="T65" id="Seg_8694" s="T64">giv-inactive-Q</ta>
            <ta e="T66" id="Seg_8695" s="T65">0.giv-active 0.quot-sp</ta>
            <ta e="T67" id="Seg_8696" s="T66">giv-inactive-Q</ta>
            <ta e="T68" id="Seg_8697" s="T67">0.giv-active-Q</ta>
            <ta e="T73" id="Seg_8698" s="T72">0.giv-active-Q</ta>
            <ta e="T74" id="Seg_8699" s="T73">0.giv-active-Q</ta>
            <ta e="T79" id="Seg_8700" s="T77">0.giv-active-Q</ta>
            <ta e="T81" id="Seg_8701" s="T80">accs-inf-Q</ta>
            <ta e="T82" id="Seg_8702" s="T81">0.giv-active-Q</ta>
            <ta e="T85" id="Seg_8703" s="T84">0.giv-active 0.quot-th</ta>
            <ta e="T87" id="Seg_8704" s="T86">0.giv-active</ta>
            <ta e="T88" id="Seg_8705" s="T87">0.giv-active-Q</ta>
            <ta e="T91" id="Seg_8706" s="T90">0.giv-inactive-Q</ta>
            <ta e="T92" id="Seg_8707" s="T91">quot-sp</ta>
            <ta e="T94" id="Seg_8708" s="T93">giv-active</ta>
            <ta e="T95" id="Seg_8709" s="T94">giv-active</ta>
            <ta e="T96" id="Seg_8710" s="T95">accs-inf</ta>
            <ta e="T98" id="Seg_8711" s="T96">0.giv-active</ta>
            <ta e="T100" id="Seg_8712" s="T99">accs-inf</ta>
            <ta e="T101" id="Seg_8713" s="T100">accs-inf</ta>
            <ta e="T103" id="Seg_8714" s="T101">0.giv-active</ta>
            <ta e="T105" id="Seg_8715" s="T104">giv-active</ta>
            <ta e="T109" id="Seg_8716" s="T108">new</ta>
            <ta e="T110" id="Seg_8717" s="T109">0.giv-active</ta>
            <ta e="T111" id="Seg_8718" s="T110">giv-inactive</ta>
            <ta e="T112" id="Seg_8719" s="T111">0.giv-active</ta>
            <ta e="T114" id="Seg_8720" s="T113">giv-active</ta>
            <ta e="T116" id="Seg_8721" s="T115">giv-active-Q</ta>
            <ta e="T120" id="Seg_8722" s="T119">giv-inactive-Q</ta>
            <ta e="T121" id="Seg_8723" s="T120">accs-gen-Q</ta>
            <ta e="T122" id="Seg_8724" s="T121">accs-inf-Q</ta>
            <ta e="T125" id="Seg_8725" s="T124">0.giv-active-Q</ta>
            <ta e="T127" id="Seg_8726" s="T126">giv-inactive</ta>
            <ta e="T128" id="Seg_8727" s="T127">giv-active</ta>
            <ta e="T133" id="Seg_8728" s="T132">giv-inactive</ta>
            <ta e="T137" id="Seg_8729" s="T136">new</ta>
            <ta e="T138" id="Seg_8730" s="T137">accs-inf</ta>
            <ta e="T142" id="Seg_8731" s="T141">new</ta>
            <ta e="T143" id="Seg_8732" s="T142">new</ta>
            <ta e="T145" id="Seg_8733" s="T144">accs-inf</ta>
            <ta e="T148" id="Seg_8734" s="T147">new</ta>
            <ta e="T151" id="Seg_8735" s="T150">giv-inactive</ta>
            <ta e="T152" id="Seg_8736" s="T151">giv-inactive</ta>
            <ta e="T155" id="Seg_8737" s="T154">giv-active</ta>
            <ta e="T156" id="Seg_8738" s="T155">0.giv-active</ta>
            <ta e="T159" id="Seg_8739" s="T158">0.giv-active-Q</ta>
            <ta e="T160" id="Seg_8740" s="T159">0.giv-active-Q</ta>
            <ta e="T164" id="Seg_8741" s="T163">giv-inactive-Q</ta>
            <ta e="T165" id="Seg_8742" s="T164">accs-inf-Q</ta>
            <ta e="T166" id="Seg_8743" s="T165">0.giv-active-Q</ta>
            <ta e="T167" id="Seg_8744" s="T166">accs-inf-Q</ta>
            <ta e="T168" id="Seg_8745" s="T167">0.giv-inactive 0.quot-sp</ta>
            <ta e="T169" id="Seg_8746" s="T168">giv-active</ta>
            <ta e="T173" id="Seg_8747" s="T172">giv-active</ta>
            <ta e="T174" id="Seg_8748" s="T173">0.giv-active</ta>
            <ta e="T175" id="Seg_8749" s="T174">giv-active</ta>
            <ta e="T177" id="Seg_8750" s="T176">new</ta>
            <ta e="T178" id="Seg_8751" s="T177">new</ta>
            <ta e="T182" id="Seg_8752" s="T181">0.giv-inactive</ta>
            <ta e="T183" id="Seg_8753" s="T182">giv-inactive</ta>
            <ta e="T184" id="Seg_8754" s="T183">0.giv-active</ta>
            <ta e="T186" id="Seg_8755" s="T185">giv-active</ta>
            <ta e="T189" id="Seg_8756" s="T188">giv-active</ta>
            <ta e="T191" id="Seg_8757" s="T190">0.giv-active</ta>
            <ta e="T193" id="Seg_8758" s="T192">0.giv-active</ta>
            <ta e="T194" id="Seg_8759" s="T193">0.giv-active</ta>
            <ta e="T195" id="Seg_8760" s="T194">accs-inf</ta>
            <ta e="T199" id="Seg_8761" s="T198">giv-inactive</ta>
            <ta e="T202" id="Seg_8762" s="T201">0.giv-active</ta>
            <ta e="T203" id="Seg_8763" s="T202">giv-active-Q</ta>
            <ta e="T204" id="Seg_8764" s="T203">giv-inactive-Q</ta>
            <ta e="T208" id="Seg_8765" s="T207">0.giv-active 0.quot-sp</ta>
            <ta e="T209" id="Seg_8766" s="T208">giv-active</ta>
            <ta e="T212" id="Seg_8767" s="T211">giv-inactive</ta>
            <ta e="T213" id="Seg_8768" s="T212">accs-inf</ta>
            <ta e="T214" id="Seg_8769" s="T213">0.giv-active</ta>
            <ta e="T216" id="Seg_8770" s="T214">0.giv-active</ta>
            <ta e="T217" id="Seg_8771" s="T216">accs-inf</ta>
            <ta e="T219" id="Seg_8772" s="T218">new</ta>
            <ta e="T220" id="Seg_8773" s="T219">accs-inf</ta>
            <ta e="T222" id="Seg_8774" s="T221">new</ta>
            <ta e="T224" id="Seg_8775" s="T223">0.giv-active</ta>
            <ta e="T226" id="Seg_8776" s="T225">giv-inactive</ta>
            <ta e="T230" id="Seg_8777" s="T229">giv-inactive</ta>
            <ta e="T231" id="Seg_8778" s="T230">0.giv-active</ta>
            <ta e="T234" id="Seg_8779" s="T233">giv-inactive</ta>
            <ta e="T236" id="Seg_8780" s="T235">giv-inactive</ta>
            <ta e="T239" id="Seg_8781" s="T238">0.giv-active-Q</ta>
            <ta e="T241" id="Seg_8782" s="T240">giv-inactive-Q</ta>
            <ta e="T243" id="Seg_8783" s="T242">0.giv-active-Q</ta>
            <ta e="T244" id="Seg_8784" s="T243">giv-active</ta>
            <ta e="T249" id="Seg_8785" s="T248">giv-active</ta>
            <ta e="T250" id="Seg_8786" s="T249">0.giv-active</ta>
            <ta e="T252" id="Seg_8787" s="T251">giv-active</ta>
            <ta e="T253" id="Seg_8788" s="T252">giv-inactive</ta>
            <ta e="T259" id="Seg_8789" s="T258">0.giv-active</ta>
            <ta e="T263" id="Seg_8790" s="T262">giv-inactive</ta>
            <ta e="T266" id="Seg_8791" s="T265">giv-inactive</ta>
            <ta e="T267" id="Seg_8792" s="T266">0.giv-active</ta>
            <ta e="T268" id="Seg_8793" s="T267">0.giv-active 0.quot-sp</ta>
            <ta e="T270" id="Seg_8794" s="T269">giv-inactive-Q</ta>
            <ta e="T271" id="Seg_8795" s="T270">accs-inf-Q</ta>
            <ta e="T273" id="Seg_8796" s="T272">accs-inf-Q</ta>
            <ta e="T275" id="Seg_8797" s="T274">0.giv-inactive-Q</ta>
            <ta e="T277" id="Seg_8798" s="T276">giv-inactive</ta>
            <ta e="T281" id="Seg_8799" s="T280">giv-active</ta>
            <ta e="T283" id="Seg_8800" s="T282">giv-active</ta>
            <ta e="T284" id="Seg_8801" s="T283">0.giv-active</ta>
            <ta e="T285" id="Seg_8802" s="T284">giv-active</ta>
            <ta e="T288" id="Seg_8803" s="T287">giv-active</ta>
            <ta e="T291" id="Seg_8804" s="T290">giv-active</ta>
            <ta e="T293" id="Seg_8805" s="T292">0.giv-inactive</ta>
            <ta e="T295" id="Seg_8806" s="T294">giv-inactive</ta>
            <ta e="T296" id="Seg_8807" s="T295">0.giv-active</ta>
            <ta e="T299" id="Seg_8808" s="T298">giv-active-Q</ta>
            <ta e="T305" id="Seg_8809" s="T304">0.giv-active 0.quot-sp</ta>
            <ta e="T308" id="Seg_8810" s="T307">giv-inactive</ta>
            <ta e="T309" id="Seg_8811" s="T308">giv-inactive</ta>
            <ta e="T311" id="Seg_8812" s="T309">0.giv-active</ta>
            <ta e="T312" id="Seg_8813" s="T311">giv-inactive</ta>
            <ta e="T314" id="Seg_8814" s="T313">giv-inactive</ta>
            <ta e="T315" id="Seg_8815" s="T314">giv-inactive</ta>
            <ta e="T317" id="Seg_8816" s="T316">giv-inactive</ta>
            <ta e="T319" id="Seg_8817" s="T318">0.giv-active</ta>
            <ta e="T321" id="Seg_8818" s="T320">giv-inactive</ta>
            <ta e="T323" id="Seg_8819" s="T322">giv-inactive</ta>
            <ta e="T324" id="Seg_8820" s="T323">0.giv-active</ta>
            <ta e="T326" id="Seg_8821" s="T325">giv-inactive</ta>
            <ta e="T327" id="Seg_8822" s="T326">0.giv-active</ta>
            <ta e="T330" id="Seg_8823" s="T329">giv-inactive</ta>
            <ta e="T332" id="Seg_8824" s="T331">giv-inactive</ta>
            <ta e="T335" id="Seg_8825" s="T334">0.giv-active-Q</ta>
            <ta e="T337" id="Seg_8826" s="T336">giv-inactive-Q</ta>
            <ta e="T339" id="Seg_8827" s="T338">0.giv-active-Q</ta>
            <ta e="T340" id="Seg_8828" s="T339">0.giv-active 0.quot-sp</ta>
            <ta e="T342" id="Seg_8829" s="T341">0.giv-active</ta>
            <ta e="T345" id="Seg_8830" s="T344">giv-active</ta>
            <ta e="T346" id="Seg_8831" s="T345">0.giv-active</ta>
            <ta e="T348" id="Seg_8832" s="T346">0.accs-gen</ta>
            <ta e="T349" id="Seg_8833" s="T348">giv-active</ta>
            <ta e="T350" id="Seg_8834" s="T349">quot-sp</ta>
            <ta e="T351" id="Seg_8835" s="T350">0.giv-active-Q</ta>
            <ta e="T352" id="Seg_8836" s="T351">giv-active-Q</ta>
            <ta e="T353" id="Seg_8837" s="T352">giv-inactive-Q</ta>
            <ta e="T356" id="Seg_8838" s="T355">giv-inactive-Q</ta>
            <ta e="T358" id="Seg_8839" s="T357">0.giv-active-Q</ta>
            <ta e="T360" id="Seg_8840" s="T359">giv-inactive-Q</ta>
            <ta e="T361" id="Seg_8841" s="T360">giv-active-Q</ta>
            <ta e="T362" id="Seg_8842" s="T361">accs-inf-Q</ta>
            <ta e="T367" id="Seg_8843" s="T366">giv-active</ta>
            <ta e="T369" id="Seg_8844" s="T368">giv-active</ta>
            <ta e="T371" id="Seg_8845" s="T370">giv-inactive</ta>
            <ta e="T372" id="Seg_8846" s="T371">0.giv-active</ta>
            <ta e="T374" id="Seg_8847" s="T373">quot-sp</ta>
            <ta e="T376" id="Seg_8848" s="T375">giv-active-Q</ta>
            <ta e="T377" id="Seg_8849" s="T376">accs-inf-Q</ta>
            <ta e="T378" id="Seg_8850" s="T377">0.giv-active-Q</ta>
            <ta e="T380" id="Seg_8851" s="T378">0.giv-inactive-Q</ta>
            <ta e="T381" id="Seg_8852" s="T380">0.giv-active-Q</ta>
            <ta e="T382" id="Seg_8853" s="T381">giv-active-Q</ta>
            <ta e="T384" id="Seg_8854" s="T382">0.giv-inactive-Q</ta>
            <ta e="T386" id="Seg_8855" s="T385">giv-active-Q</ta>
            <ta e="T387" id="Seg_8856" s="T386">0.giv-active-Q</ta>
            <ta e="T389" id="Seg_8857" s="T388">quot-th</ta>
            <ta e="T391" id="Seg_8858" s="T390">giv-active</ta>
            <ta e="T395" id="Seg_8859" s="T394">giv-inactive-Q</ta>
            <ta e="T396" id="Seg_8860" s="T395">0.giv-active-Q</ta>
            <ta e="T403" id="Seg_8861" s="T402">new-Q</ta>
            <ta e="T406" id="Seg_8862" s="T405">new</ta>
            <ta e="T408" id="Seg_8863" s="T406">0.giv-inactive</ta>
            <ta e="T410" id="Seg_8864" s="T409">accs-inf</ta>
            <ta e="T411" id="Seg_8865" s="T410">0.giv-active</ta>
            <ta e="T413" id="Seg_8866" s="T412">giv-inactive</ta>
            <ta e="T414" id="Seg_8867" s="T413">giv-inactive</ta>
            <ta e="T415" id="Seg_8868" s="T414">giv-active</ta>
            <ta e="T416" id="Seg_8869" s="T415">giv-active</ta>
            <ta e="T421" id="Seg_8870" s="T420">giv-inactive</ta>
            <ta e="T422" id="Seg_8871" s="T421">giv-inactive</ta>
            <ta e="T426" id="Seg_8872" s="T425">giv-inactive</ta>
            <ta e="T428" id="Seg_8873" s="T427">giv-inactive</ta>
            <ta e="T429" id="Seg_8874" s="T428">accs-inf</ta>
            <ta e="T430" id="Seg_8875" s="T429">0.giv-active</ta>
            <ta e="T432" id="Seg_8876" s="T431">new</ta>
            <ta e="T434" id="Seg_8877" s="T432">0.giv-active</ta>
            <ta e="T436" id="Seg_8878" s="T435">giv-inactive</ta>
            <ta e="T437" id="Seg_8879" s="T436">0.giv-active</ta>
            <ta e="T440" id="Seg_8880" s="T439">giv-inactive-Q</ta>
            <ta e="T441" id="Seg_8881" s="T440">0.giv-inactive-Q</ta>
            <ta e="T442" id="Seg_8882" s="T441">accs-gen-Q</ta>
            <ta e="T443" id="Seg_8883" s="T442">accs-inf-Q</ta>
            <ta e="T444" id="Seg_8884" s="T443">new-Q</ta>
            <ta e="T449" id="Seg_8885" s="T448">giv-inactive-Q</ta>
            <ta e="T450" id="Seg_8886" s="T449">giv-active-Q</ta>
            <ta e="T454" id="Seg_8887" s="T453">giv-active-Q</ta>
            <ta e="T458" id="Seg_8888" s="T457">quot-sp</ta>
            <ta e="T459" id="Seg_8889" s="T458">giv-active</ta>
            <ta e="T463" id="Seg_8890" s="T462">new-Q</ta>
            <ta e="T465" id="Seg_8891" s="T464">giv-inactive-Q</ta>
            <ta e="T467" id="Seg_8892" s="T466">giv-active-Q</ta>
            <ta e="T470" id="Seg_8893" s="T469">giv-active-Q</ta>
            <ta e="T471" id="Seg_8894" s="T470">0.giv-active-Q</ta>
            <ta e="T472" id="Seg_8895" s="T471">0.giv-active-Q</ta>
            <ta e="T475" id="Seg_8896" s="T474">giv-inactive-Q</ta>
            <ta e="T476" id="Seg_8897" s="T475">giv-inactive-Q</ta>
            <ta e="T477" id="Seg_8898" s="T476">new-Q</ta>
            <ta e="T482" id="Seg_8899" s="T480">0.giv-active-Q</ta>
            <ta e="T484" id="Seg_8900" s="T483">giv-inactive</ta>
            <ta e="T486" id="Seg_8901" s="T485">quot-sp</ta>
            <ta e="T489" id="Seg_8902" s="T488">giv-inactive-Q</ta>
            <ta e="T494" id="Seg_8903" s="T493">giv-inactive-Q</ta>
            <ta e="T496" id="Seg_8904" s="T495">giv-inactive-Q</ta>
            <ta e="T499" id="Seg_8905" s="T498">giv-inactive</ta>
            <ta e="T500" id="Seg_8906" s="T499">quot-sp</ta>
            <ta e="T501" id="Seg_8907" s="T500">giv-active-Q</ta>
            <ta e="T503" id="Seg_8908" s="T502">giv-inactive-Q</ta>
            <ta e="T506" id="Seg_8909" s="T505">accs-inf-Q</ta>
            <ta e="T508" id="Seg_8910" s="T507">accs-inf-Q</ta>
            <ta e="T510" id="Seg_8911" s="T509">accs-inf-Q</ta>
            <ta e="T515" id="Seg_8912" s="T514">giv-inactive-Q</ta>
            <ta e="T517" id="Seg_8913" s="T516">quot-sp</ta>
            <ta e="T519" id="Seg_8914" s="T518">giv-inactive</ta>
            <ta e="T522" id="Seg_8915" s="T521">giv-inactive-Q</ta>
            <ta e="T523" id="Seg_8916" s="T522">giv-inactive-Q</ta>
            <ta e="T525" id="Seg_8917" s="T524">giv-active-Q</ta>
            <ta e="T527" id="Seg_8918" s="T526">accs-aggr-Q</ta>
            <ta e="T528" id="Seg_8919" s="T527">giv-active-Q</ta>
            <ta e="T529" id="Seg_8920" s="T528">giv-active-Q</ta>
            <ta e="T533" id="Seg_8921" s="T532">giv-inactive-Q</ta>
            <ta e="T535" id="Seg_8922" s="T534">giv-active-Q</ta>
            <ta e="T536" id="Seg_8923" s="T535">giv-active-Q</ta>
            <ta e="T538" id="Seg_8924" s="T537">quot-sp</ta>
            <ta e="T539" id="Seg_8925" s="T538">giv-inactive</ta>
            <ta e="T540" id="Seg_8926" s="T539">0.giv-inactive-Q</ta>
            <ta e="T542" id="Seg_8927" s="T541">giv-inactive-Q</ta>
            <ta e="T543" id="Seg_8928" s="T542">giv-inactive-Q</ta>
            <ta e="T545" id="Seg_8929" s="T544">giv-active</ta>
            <ta e="T549" id="Seg_8930" s="T547">0.giv-active</ta>
            <ta e="T550" id="Seg_8931" s="T549">giv-inactive</ta>
            <ta e="T551" id="Seg_8932" s="T550">0.giv-active</ta>
            <ta e="T552" id="Seg_8933" s="T551">giv-inactive</ta>
            <ta e="T553" id="Seg_8934" s="T552">0.giv-active</ta>
            <ta e="T554" id="Seg_8935" s="T553">giv-inactive</ta>
            <ta e="T556" id="Seg_8936" s="T555">accs-inf</ta>
            <ta e="T557" id="Seg_8937" s="T556">giv-inactive</ta>
            <ta e="T558" id="Seg_8938" s="T557">giv-inactive</ta>
            <ta e="T560" id="Seg_8939" s="T559">0.giv-active</ta>
            <ta e="T562" id="Seg_8940" s="T561">giv-inactive</ta>
            <ta e="T563" id="Seg_8941" s="T562">giv-active</ta>
            <ta e="T567" id="Seg_8942" s="T566">0.giv-active</ta>
            <ta e="T569" id="Seg_8943" s="T568">giv-inactive</ta>
            <ta e="T571" id="Seg_8944" s="T570">giv-inactive</ta>
            <ta e="T572" id="Seg_8945" s="T571">accs-inf</ta>
            <ta e="T573" id="Seg_8946" s="T572">accs-inf</ta>
            <ta e="T576" id="Seg_8947" s="T575">giv-inactive</ta>
            <ta e="T577" id="Seg_8948" s="T576">0.giv-active</ta>
            <ta e="T578" id="Seg_8949" s="T577">giv-inactive</ta>
            <ta e="T579" id="Seg_8950" s="T578">giv-inactive</ta>
            <ta e="T580" id="Seg_8951" s="T579">new</ta>
            <ta e="T582" id="Seg_8952" s="T580">0.giv-active</ta>
            <ta e="T583" id="Seg_8953" s="T582">0.giv-active</ta>
            <ta e="T584" id="Seg_8954" s="T583">giv-active</ta>
            <ta e="T586" id="Seg_8955" s="T585">giv-inactive</ta>
            <ta e="T589" id="Seg_8956" s="T588">giv-inactive</ta>
            <ta e="T590" id="Seg_8957" s="T589">0.giv-active</ta>
            <ta e="T591" id="Seg_8958" s="T590">giv-active</ta>
            <ta e="T592" id="Seg_8959" s="T591">new</ta>
            <ta e="T595" id="Seg_8960" s="T594">new</ta>
            <ta e="T596" id="Seg_8961" s="T595">0.giv-active</ta>
            <ta e="T598" id="Seg_8962" s="T597">giv-active</ta>
            <ta e="T600" id="Seg_8963" s="T599">giv-inactive</ta>
            <ta e="T604" id="Seg_8964" s="T603">giv-active</ta>
            <ta e="T606" id="Seg_8965" s="T605">giv-inactive</ta>
            <ta e="T611" id="Seg_8966" s="T610">giv-inactive</ta>
            <ta e="T612" id="Seg_8967" s="T611">0.giv-active</ta>
            <ta e="T615" id="Seg_8968" s="T613">new</ta>
         </annotation>
         <annotation name="Top" tierref="Top">
            <ta e="T3" id="Seg_8969" s="T2">top.int.concr</ta>
            <ta e="T9" id="Seg_8970" s="T8">0.top.int.abstr</ta>
            <ta e="T11" id="Seg_8971" s="T9">top.int.concr</ta>
            <ta e="T17" id="Seg_8972" s="T16">top.int.concr</ta>
            <ta e="T24" id="Seg_8973" s="T23">top.int.concr</ta>
            <ta e="T30" id="Seg_8974" s="T28">top.int.concr</ta>
            <ta e="T33" id="Seg_8975" s="T32">top.int.concr</ta>
            <ta e="T34" id="Seg_8976" s="T33">top.int.concr</ta>
            <ta e="T39" id="Seg_8977" s="T37">0.top.int.concr</ta>
            <ta e="T45" id="Seg_8978" s="T44">0.top.int.abstr.</ta>
            <ta e="T48" id="Seg_8979" s="T45">top.int.concr</ta>
            <ta e="T55" id="Seg_8980" s="T54">0.top.int.concr</ta>
            <ta e="T59" id="Seg_8981" s="T58">0.top.int.concr</ta>
            <ta e="T60" id="Seg_8982" s="T59">top.int.concr</ta>
            <ta e="T74" id="Seg_8983" s="T73">top.int.concr</ta>
            <ta e="T85" id="Seg_8984" s="T84">0.top.int.concr</ta>
            <ta e="T87" id="Seg_8985" s="T86">0.top.int.concr</ta>
            <ta e="T88" id="Seg_8986" s="T87">top.int.concr</ta>
            <ta e="T99" id="Seg_8987" s="T94">top.int.concr</ta>
            <ta e="T105" id="Seg_8988" s="T103">top.int.concr</ta>
            <ta e="T110" id="Seg_8989" s="T109">0.top.int.concr</ta>
            <ta e="T112" id="Seg_8990" s="T111">0.top.int.concr</ta>
            <ta e="T114" id="Seg_8991" s="T112">top.int.concr</ta>
            <ta e="T116" id="Seg_8992" s="T115">top.int.concr</ta>
            <ta e="T125" id="Seg_8993" s="T124">0.top.int.concr</ta>
            <ta e="T127" id="Seg_8994" s="T125">top.int.concr</ta>
            <ta e="T133" id="Seg_8995" s="T131">top.int.concr</ta>
            <ta e="T138" id="Seg_8996" s="T136">top.int.concr</ta>
            <ta e="T142" id="Seg_8997" s="T141">0.top.int.concr</ta>
            <ta e="T143" id="Seg_8998" s="T142">0.top.int.concr</ta>
            <ta e="T145" id="Seg_8999" s="T143">top.int.concr</ta>
            <ta e="T151" id="Seg_9000" s="T149">top.int.concr</ta>
            <ta e="T171" id="Seg_9001" s="T168">top.int.concr</ta>
            <ta e="T177" id="Seg_9002" s="T176">top.int.concr</ta>
            <ta e="T183" id="Seg_9003" s="T182">0.top.int.concr</ta>
            <ta e="T186" id="Seg_9004" s="T184">top.int.concr</ta>
            <ta e="T193" id="Seg_9005" s="T192">0.top.int.concr</ta>
            <ta e="T195" id="Seg_9006" s="T194">top.int.concr</ta>
            <ta e="T199" id="Seg_9007" s="T197">top.int.concr</ta>
            <ta e="T207" id="Seg_9008" s="T206">0.top.int.concr</ta>
            <ta e="T216" id="Seg_9009" s="T214">0.top.int.concr</ta>
            <ta e="T224" id="Seg_9010" s="T223">0.top.int.concr</ta>
            <ta e="T226" id="Seg_9011" s="T224">top.int.concr</ta>
            <ta e="T232" id="Seg_9012" s="T229">top.int.concr</ta>
            <ta e="T244" id="Seg_9013" s="T243">top.int.concr</ta>
            <ta e="T250" id="Seg_9014" s="T249">0.top.int.concr</ta>
            <ta e="T254" id="Seg_9015" s="T250">top.int.concr</ta>
            <ta e="T264" id="Seg_9016" s="T263">0.top.int.abstr.</ta>
            <ta e="T268" id="Seg_9017" s="T267">0.top.int.concr</ta>
            <ta e="T280" id="Seg_9018" s="T275">top.int.concr</ta>
            <ta e="T288" id="Seg_9019" s="T286">top.int.concr</ta>
            <ta e="T293" id="Seg_9020" s="T292">0.top.int.concr</ta>
            <ta e="T295" id="Seg_9021" s="T293">top.int.concr</ta>
            <ta e="T299" id="Seg_9022" s="T298">top.int.concr</ta>
            <ta e="T311" id="Seg_9023" s="T309">0.top.int.concr</ta>
            <ta e="T319" id="Seg_9024" s="T317">0.top.int.concr</ta>
            <ta e="T321" id="Seg_9025" s="T319">top.int.concr</ta>
            <ta e="T328" id="Seg_9026" s="T325">top.int.concr</ta>
            <ta e="T342" id="Seg_9027" s="T341">0.top.int.concr</ta>
            <ta e="T346" id="Seg_9028" s="T345">0.top.int.concr</ta>
            <ta e="T348" id="Seg_9029" s="T346">top.int.concr</ta>
            <ta e="T353" id="Seg_9030" s="T352">0.top.int.concr</ta>
            <ta e="T358" id="Seg_9031" s="T357">0.top.int.concr</ta>
            <ta e="T360" id="Seg_9032" s="T358">top.int.concr</ta>
            <ta e="T367" id="Seg_9033" s="T365">top.int.concr</ta>
            <ta e="T380" id="Seg_9034" s="T378">top.int.concr</ta>
            <ta e="T384" id="Seg_9035" s="T382">0.top.int.concr</ta>
            <ta e="T397" id="Seg_9036" s="T396">top.int.concr</ta>
            <ta e="T405" id="Seg_9037" s="T404">0.top.int.abstr</ta>
            <ta e="T408" id="Seg_9038" s="T407">0.top.int.concr</ta>
            <ta e="T411" id="Seg_9039" s="T410">0.top.int.concr</ta>
            <ta e="T416" id="Seg_9040" s="T414">top.int.concr</ta>
            <ta e="T422" id="Seg_9041" s="T419">top.int.concr</ta>
            <ta e="T430" id="Seg_9042" s="T429">0.top.int.concr</ta>
            <ta e="T434" id="Seg_9043" s="T432">0.top.int.concr</ta>
            <ta e="T436" id="Seg_9044" s="T434">top.int.concr</ta>
            <ta e="T443" id="Seg_9045" s="T441">top.int.concr</ta>
            <ta e="T453" id="Seg_9046" s="T452">0.top.int.concr</ta>
            <ta e="T454" id="Seg_9047" s="T453">top.int.concr</ta>
            <ta e="T463" id="Seg_9048" s="T459">top.int.concr.contr</ta>
            <ta e="T465" id="Seg_9049" s="T464">top.int.concr</ta>
            <ta e="T472" id="Seg_9050" s="T471">0.top.int.concr</ta>
            <ta e="T476" id="Seg_9051" s="T472">top.int.concr</ta>
            <ta e="T482" id="Seg_9052" s="T480">0.top.int.concr</ta>
            <ta e="T484" id="Seg_9053" s="T482">top.int.concr</ta>
            <ta e="T493" id="Seg_9054" s="T492">top.int.concr</ta>
            <ta e="T496" id="Seg_9055" s="T495">top.int.concr</ta>
            <ta e="T499" id="Seg_9056" s="T498">top.int.concr</ta>
            <ta e="T501" id="Seg_9057" s="T500">top.int.concr</ta>
            <ta e="T503" id="Seg_9058" s="T502">top.int.concr</ta>
            <ta e="T506" id="Seg_9059" s="T505">top.int.concr</ta>
            <ta e="T508" id="Seg_9060" s="T507">top.int.concr.contr</ta>
            <ta e="T510" id="Seg_9061" s="T509">top.int.concr.contr</ta>
            <ta e="T515" id="Seg_9062" s="T514">0.top.int.concr</ta>
            <ta e="T527" id="Seg_9063" s="T526">top.int.concr</ta>
            <ta e="T533" id="Seg_9064" s="T532">top.int.concr</ta>
            <ta e="T547" id="Seg_9065" s="T546">0.top.int.concr</ta>
            <ta e="T551" id="Seg_9066" s="T550">0.top.int.concr</ta>
            <ta e="T554" id="Seg_9067" s="T553">top.int.concr</ta>
            <ta e="T563" id="Seg_9068" s="T560">top.int.concr</ta>
            <ta e="T567" id="Seg_9069" s="T566">0.top.int.concr</ta>
            <ta e="T571" id="Seg_9070" s="T567">top.int.concr</ta>
            <ta e="T583" id="Seg_9071" s="T582">0.top.int.concr</ta>
            <ta e="T584" id="Seg_9072" s="T583">top.int.concr</ta>
            <ta e="T589" id="Seg_9073" s="T587">top.int.concr</ta>
            <ta e="T596" id="Seg_9074" s="T595">0.top.int.concr</ta>
            <ta e="T601" id="Seg_9075" s="T596">top.int.concr</ta>
            <ta e="T607" id="Seg_9076" s="T601">top.int.concr</ta>
            <ta e="T613" id="Seg_9077" s="T612">0.top.int.abstr</ta>
         </annotation>
         <annotation name="Foc" tierref="Foc">
            <ta e="T2" id="Seg_9078" s="T0">foc.int</ta>
            <ta e="T5" id="Seg_9079" s="T3">foc.nar</ta>
            <ta e="T9" id="Seg_9080" s="T5">foc.wid</ta>
            <ta e="T16" id="Seg_9081" s="T11">foc.int</ta>
            <ta e="T21" id="Seg_9082" s="T17">foc.int</ta>
            <ta e="T27" id="Seg_9083" s="T25">foc.nar</ta>
            <ta e="T32" id="Seg_9084" s="T30">foc.int</ta>
            <ta e="T35" id="Seg_9085" s="T34">foc.int</ta>
            <ta e="T37" id="Seg_9086" s="T35">foc.nar</ta>
            <ta e="T45" id="Seg_9087" s="T39">foc.wid</ta>
            <ta e="T53" id="Seg_9088" s="T48">foc.wid</ta>
            <ta e="T54" id="Seg_9089" s="T53">foc.nar</ta>
            <ta e="T56" id="Seg_9090" s="T55">foc.nar</ta>
            <ta e="T59" id="Seg_9091" s="T57">foc.int</ta>
            <ta e="T63" id="Seg_9092" s="T61">foc.nar</ta>
            <ta e="T69" id="Seg_9093" s="T68">foc.ver</ta>
            <ta e="T71" id="Seg_9094" s="T70">foc.ver</ta>
            <ta e="T79" id="Seg_9095" s="T74">foc.int</ta>
            <ta e="T80" id="Seg_9096" s="T79">foc.nar</ta>
            <ta e="T85" id="Seg_9097" s="T83">foc.int</ta>
            <ta e="T87" id="Seg_9098" s="T85">foc.int</ta>
            <ta e="T91" id="Seg_9099" s="T88">foc.wid</ta>
            <ta e="T103" id="Seg_9100" s="T99">foc.int</ta>
            <ta e="T107" id="Seg_9101" s="T105">foc.int</ta>
            <ta e="T110" id="Seg_9102" s="T107">foc.int</ta>
            <ta e="T112" id="Seg_9103" s="T110">foc.int</ta>
            <ta e="T115" id="Seg_9104" s="T114">foc.int</ta>
            <ta e="T120" id="Seg_9105" s="T118">foc.nar</ta>
            <ta e="T124" id="Seg_9106" s="T122">foc.nar</ta>
            <ta e="T131" id="Seg_9107" s="T127">foc.int</ta>
            <ta e="T136" id="Seg_9108" s="T133">foc.int</ta>
            <ta e="T139" id="Seg_9109" s="T138">foc.int</ta>
            <ta e="T143" id="Seg_9110" s="T139">foc.int</ta>
            <ta e="T149" id="Seg_9111" s="T143">foc.wid</ta>
            <ta e="T156" id="Seg_9112" s="T151">foc.int</ta>
            <ta e="T158" id="Seg_9113" s="T156">foc.nar</ta>
            <ta e="T160" id="Seg_9114" s="T159">foc.int</ta>
            <ta e="T167" id="Seg_9115" s="T163">foc.int</ta>
            <ta e="T176" id="Seg_9116" s="T171">foc.wid</ta>
            <ta e="T179" id="Seg_9117" s="T177">foc.int</ta>
            <ta e="T182" id="Seg_9118" s="T179">foc.nar</ta>
            <ta e="T188" id="Seg_9119" s="T186">foc.int</ta>
            <ta e="T193" id="Seg_9120" s="T188">foc.int</ta>
            <ta e="T197" id="Seg_9121" s="T193">foc.wid</ta>
            <ta e="T202" id="Seg_9122" s="T199">foc.int</ta>
            <ta e="T207" id="Seg_9123" s="T202">foc.int</ta>
            <ta e="T216" id="Seg_9124" s="T208">foc.int</ta>
            <ta e="T224" id="Seg_9125" s="T216">foc.int</ta>
            <ta e="T229" id="Seg_9126" s="T226">foc.int</ta>
            <ta e="T238" id="Seg_9127" s="T232">foc.wid</ta>
            <ta e="T240" id="Seg_9128" s="T239">foc.nar</ta>
            <ta e="T247" id="Seg_9129" s="T244">foc.int</ta>
            <ta e="T250" id="Seg_9130" s="T249">foc.nar</ta>
            <ta e="T256" id="Seg_9131" s="T254">foc.int</ta>
            <ta e="T259" id="Seg_9132" s="T256">foc.int</ta>
            <ta e="T266" id="Seg_9133" s="T259">foc.wid</ta>
            <ta e="T268" id="Seg_9134" s="T266">foc.int</ta>
            <ta e="T272" id="Seg_9135" s="T270">foc.nar</ta>
            <ta e="T274" id="Seg_9136" s="T272">foc.nar</ta>
            <ta e="T286" id="Seg_9137" s="T280">foc.wid</ta>
            <ta e="T290" id="Seg_9138" s="T288">foc.int</ta>
            <ta e="T293" id="Seg_9139" s="T290">foc.int</ta>
            <ta e="T298" id="Seg_9140" s="T295">foc.int</ta>
            <ta e="T304" id="Seg_9141" s="T299">foc.int</ta>
            <ta e="T311" id="Seg_9142" s="T306">foc.int</ta>
            <ta e="T319" id="Seg_9143" s="T311">foc.int</ta>
            <ta e="T325" id="Seg_9144" s="T321">foc.int</ta>
            <ta e="T334" id="Seg_9145" s="T328">foc.wid</ta>
            <ta e="T336" id="Seg_9146" s="T335">foc.nar</ta>
            <ta e="T343" id="Seg_9147" s="T340">foc.int</ta>
            <ta e="T346" id="Seg_9148" s="T345">foc.nar</ta>
            <ta e="T350" id="Seg_9149" s="T348">foc.wid</ta>
            <ta e="T351" id="Seg_9150" s="T350">foc.nar</ta>
            <ta e="T355" id="Seg_9151" s="T353">foc.int</ta>
            <ta e="T357" id="Seg_9152" s="T355">foc.nar</ta>
            <ta e="T364" id="Seg_9153" s="T360">foc.int</ta>
            <ta e="T374" id="Seg_9154" s="T367">foc.int</ta>
            <ta e="T378" id="Seg_9155" s="T376">foc.int</ta>
            <ta e="T381" id="Seg_9156" s="T380">foc.nar</ta>
            <ta e="T384" id="Seg_9157" s="T381">foc.int</ta>
            <ta e="T385" id="Seg_9158" s="T384">foc.nar</ta>
            <ta e="T392" id="Seg_9159" s="T391">foc.nar</ta>
            <ta e="T399" id="Seg_9160" s="T397">foc.int</ta>
            <ta e="T403" id="Seg_9161" s="T400">foc.nar</ta>
            <ta e="T406" id="Seg_9162" s="T405">foc.nar</ta>
            <ta e="T414" id="Seg_9163" s="T411">foc.nar</ta>
            <ta e="T419" id="Seg_9164" s="T416">foc.int</ta>
            <ta e="T427" id="Seg_9165" s="T423">foc.nar</ta>
            <ta e="T431" id="Seg_9166" s="T427">foc.int</ta>
            <ta e="T434" id="Seg_9167" s="T431">foc.int</ta>
            <ta e="T438" id="Seg_9168" s="T436">foc.int</ta>
            <ta e="T440" id="Seg_9169" s="T438">foc.nar</ta>
            <ta e="T445" id="Seg_9170" s="T444">foc.nar</ta>
            <ta e="T452" id="Seg_9171" s="T450">foc.nar</ta>
            <ta e="T455" id="Seg_9172" s="T454">foc.nar</ta>
            <ta e="T464" id="Seg_9173" s="T463">foc.contr</ta>
            <ta e="T467" id="Seg_9174" s="T465">foc.nar</ta>
            <ta e="T470" id="Seg_9175" s="T468">foc.nar</ta>
            <ta e="T478" id="Seg_9176" s="T477">foc.nar</ta>
            <ta e="T482" id="Seg_9177" s="T478">foc.int</ta>
            <ta e="T486" id="Seg_9178" s="T485">foc.int</ta>
            <ta e="T491" id="Seg_9179" s="T489">foc.nar</ta>
            <ta e="T495" id="Seg_9180" s="T493">foc.nar</ta>
            <ta e="T498" id="Seg_9181" s="T496">foc.nar</ta>
            <ta e="T500" id="Seg_9182" s="T499">foc.int</ta>
            <ta e="T502" id="Seg_9183" s="T501">foc.int</ta>
            <ta e="T505" id="Seg_9184" s="T503">foc.int</ta>
            <ta e="T507" id="Seg_9185" s="T506">foc.int</ta>
            <ta e="T509" id="Seg_9186" s="T508">foc.contr</ta>
            <ta e="T511" id="Seg_9187" s="T510">foc.contr</ta>
            <ta e="T515" id="Seg_9188" s="T513">foc.int</ta>
            <ta e="T521" id="Seg_9189" s="T519">foc.nar</ta>
            <ta e="T529" id="Seg_9190" s="T527">foc.nar</ta>
            <ta e="T536" id="Seg_9191" s="T534">foc.nar</ta>
            <ta e="T543" id="Seg_9192" s="T541">foc.nar</ta>
            <ta e="T547" id="Seg_9193" s="T543">foc.int</ta>
            <ta e="T553" id="Seg_9194" s="T551">foc.nar</ta>
            <ta e="T560" id="Seg_9195" s="T554">foc.int</ta>
            <ta e="T565" id="Seg_9196" s="T563">foc.int</ta>
            <ta e="T566" id="Seg_9197" s="T565">foc.nar</ta>
            <ta e="T577" id="Seg_9198" s="T571">foc.int</ta>
            <ta e="T583" id="Seg_9199" s="T577">foc.int</ta>
            <ta e="T587" id="Seg_9200" s="T584">foc.int</ta>
            <ta e="T592" id="Seg_9201" s="T591">foc.nar</ta>
            <ta e="T595" id="Seg_9202" s="T593">foc.nar</ta>
            <ta e="T609" id="Seg_9203" s="T607">foc.nar</ta>
            <ta e="T615" id="Seg_9204" s="T613">foc.nar</ta>
         </annotation>
         <annotation name="BOR" tierref="BOR">
            <ta e="T135" id="Seg_9205" s="T134">RUS:gram</ta>
            <ta e="T303" id="Seg_9206" s="T302">RUS:cult</ta>
            <ta e="T406" id="Seg_9207" s="T405">RUS:cult</ta>
            <ta e="T415" id="Seg_9208" s="T414">RUS:cult</ta>
            <ta e="T447" id="Seg_9209" s="T446">RUS:gram</ta>
            <ta e="T507" id="Seg_9210" s="T506">EV:core</ta>
            <ta e="T615" id="Seg_9211" s="T614">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon">
            <ta e="T507" id="Seg_9212" s="T506">Vsub Vsub</ta>
         </annotation>
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T303" id="Seg_9213" s="T302">dir:infl</ta>
            <ta e="T406" id="Seg_9214" s="T405">dir:infl</ta>
            <ta e="T415" id="Seg_9215" s="T414">dir:bare</ta>
            <ta e="T447" id="Seg_9216" s="T446">dir:bare</ta>
            <ta e="T507" id="Seg_9217" s="T506">dir:infl</ta>
            <ta e="T615" id="Seg_9218" s="T614">dir:bare</ta>
         </annotation>
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T5" id="Seg_9219" s="T0">Listen to a tale, its name is "Pearl Beard".</ta>
            <ta e="T9" id="Seg_9220" s="T5">There was man, apparently.</ta>
            <ta e="T16" id="Seg_9221" s="T9">The old man's beard was sparse, but nevertheless very long.</ta>
            <ta e="T21" id="Seg_9222" s="T16">The hairs of his beard twinkled like beaded threads.</ta>
            <ta e="T28" id="Seg_9223" s="T21">Therefore the old man was called "Pearl Beard".</ta>
            <ta e="T32" id="Seg_9224" s="T28">Pearl Beard has three daughters.</ta>
            <ta e="T35" id="Seg_9225" s="T32">In winter, in summer he fishes with a net.</ta>
            <ta e="T39" id="Seg_9226" s="T35">They feed themselves only on fish. </ta>
            <ta e="T45" id="Seg_9227" s="T39">Once Pearl Beard went to check the nets.</ta>
            <ta e="T53" id="Seg_9228" s="T45">When he just was bending down to the icehole, somebody grabbed him at his beard.</ta>
            <ta e="T55" id="Seg_9229" s="T53">[That one] doesn't release him at all.</ta>
            <ta e="T59" id="Seg_9230" s="T55">"Who may this be?", he thought.</ta>
            <ta e="T63" id="Seg_9231" s="T59">Out of the icehole the master of water leaned out.</ta>
            <ta e="T73" id="Seg_9232" s="T63">"Pearl Beard", he said, "do you give me your daughter or not, for that I can marry her?</ta>
            <ta e="T79" id="Seg_9233" s="T73">If you don't give her, you won't catch a single fish anymore."</ta>
            <ta e="T85" id="Seg_9234" s="T79">"On what shall I feed my family", he thought.</ta>
            <ta e="T87" id="Seg_9235" s="T85">Then he agreed.</ta>
            <ta e="T94" id="Seg_9236" s="T87">"If you cheat me, I'll find you anyway", the master of water said.</ta>
            <ta e="T103" id="Seg_9237" s="T94">He took the old man's mitten and disappeared under the ice.</ta>
            <ta e="T107" id="Seg_9238" s="T103">Pearl Beard was very confused.</ta>
            <ta e="T112" id="Seg_9239" s="T107">He came to his pole tent and told [it] to his daughters.</ta>
            <ta e="T115" id="Seg_9240" s="T112">The oldest daughter was happy:</ta>
            <ta e="T120" id="Seg_9241" s="T115">"I'll marry the master of water.</ta>
            <ta e="T125" id="Seg_9242" s="T120">I'll be the richest one on the world."</ta>
            <ta e="T131" id="Seg_9243" s="T125">Pearl Beard married his daughter off.</ta>
            <ta e="T136" id="Seg_9244" s="T131">The master of water is very rich.</ta>
            <ta e="T143" id="Seg_9245" s="T136">The (end?) of his wealth is not visible, he has a lot of rooms, houses.</ta>
            <ta e="T149" id="Seg_9246" s="T143">In one room a big ice chest is standing.</ta>
            <ta e="T156" id="Seg_9247" s="T149">The master of water brought his wife and showed her the ice chest.</ta>
            <ta e="T160" id="Seg_9248" s="T156">"Go there and look around where you want.</ta>
            <ta e="T168" id="Seg_9249" s="T160">Just don't stick your finger into this chest", he said.</ta>
            <ta e="T176" id="Seg_9250" s="T168">After her husband had gone, this woman impatiently stuck her finger in.</ta>
            <ta e="T179" id="Seg_9251" s="T176">The water became ice.</ta>
            <ta e="T183" id="Seg_9252" s="T179">She hardly got her finger out.</ta>
            <ta e="T188" id="Seg_9253" s="T183">She saw that her thumb was completely torn off.</ta>
            <ta e="T193" id="Seg_9254" s="T188">She tied it up strongly and sits.</ta>
            <ta e="T197" id="Seg_9255" s="T193">Out of fear her eyes are opened wide.</ta>
            <ta e="T202" id="Seg_9256" s="T197">The master of water came and guessed it immediately.</ta>
            <ta e="T208" id="Seg_9257" s="T202">"You can't be my wife", he said.</ta>
            <ta e="T216" id="Seg_9258" s="T208">He brought the woman to the most distant room of the house and locked her in.</ta>
            <ta e="T224" id="Seg_9259" s="T216">He slathered the door with glue made out of a sturgeon's stomach.</ta>
            <ta e="T229" id="Seg_9260" s="T224">Pearl Beard again went fishing.</ta>
            <ta e="T238" id="Seg_9261" s="T229">When he was bending down to the icehole, the master of water grabbed him again at his beard.</ta>
            <ta e="T243" id="Seg_9262" s="T238">"Bring your second daughter, for that I can marry her."</ta>
            <ta e="T250" id="Seg_9263" s="T243">What to do for the old man, he gives [away] his middle daughter.</ta>
            <ta e="T259" id="Seg_9264" s="T250">When this woman looks at the wealth, she can't take a rest, she doesn't stop wondering.</ta>
            <ta e="T266" id="Seg_9265" s="T259">Once the master of water brought her to the ice chest.</ta>
            <ta e="T268" id="Seg_9266" s="T266">He brought her and warned her:</ta>
            <ta e="T275" id="Seg_9267" s="T268">"Don't stuck your hand or finger into this chest."</ta>
            <ta e="T286" id="Seg_9268" s="T275">After the master of water had gone off with his sledge, his wife came to the ice chest and stuck her finger in.</ta>
            <ta e="T290" id="Seg_9269" s="T286">Her middle finger froze completely.</ta>
            <ta e="T298" id="Seg_9270" s="T290">She tied up her finger strongly, the master of water came and guessed it immediately.</ta>
            <ta e="T319" id="Seg_9271" s="T298">"I don't need a disobedient wife", he said, locked her in the most distant room of the house and slathered the door abundantly with glue made out of a sturgeon's stomach. </ta>
            <ta e="T325" id="Seg_9272" s="T319">Pearl Beard came again to check the nets.</ta>
            <ta e="T334" id="Seg_9273" s="T325">When he bent down to the icehole, the master of water again grabbed him at his beard.</ta>
            <ta e="T340" id="Seg_9274" s="T334">"Bring your youngest daughter, for that I can marry her", he said.</ta>
            <ta e="T346" id="Seg_9275" s="T340">What shall he do, he brought the youngest daughter.</ta>
            <ta e="T350" id="Seg_9276" s="T346">When they went off, the daughter said:</ta>
            <ta e="T352" id="Seg_9277" s="T350">"Don't be afraid, dad.</ta>
            <ta e="T358" id="Seg_9278" s="T352">I'll come soon and bring my older sisters, too.</ta>
            <ta e="T365" id="Seg_9279" s="T358">The master of water won't be slier than I, apparently."</ta>
            <ta e="T374" id="Seg_9280" s="T365">The master of water brought his third wife to the ice chest and said:</ta>
            <ta e="T378" id="Seg_9281" s="T374">"Don't stick yout hand into this chest.</ta>
            <ta e="T381" id="Seg_9282" s="T378">When I'll come back, I'll look after it.</ta>
            <ta e="T384" id="Seg_9283" s="T381">You won't cheat me."</ta>
            <ta e="T391" id="Seg_9284" s="T384">"Why does he warn me", this woman thought.</ta>
            <ta e="T399" id="Seg_9285" s="T391">"What will be, if I stuck in my hand, water is like water.</ta>
            <ta e="T405" id="Seg_9286" s="T399">There is, apparently, some trick."</ta>
            <ta e="T414" id="Seg_9287" s="T405">She grabbed a nelma and stuck its tail into the ice chest.</ta>
            <ta e="T419" id="Seg_9288" s="T414">The nelma's tail broke off completely.</ta>
            <ta e="T427" id="Seg_9289" s="T419">Pearl Beard's understood why the master of water had warned her.</ta>
            <ta e="T434" id="Seg_9290" s="T427">She sits beneath the chests and patches nets.</ta>
            <ta e="T438" id="Seg_9291" s="T434">The master of water came and wondered:</ta>
            <ta e="T441" id="Seg_9292" s="T438">"What kind of a net are you patching?</ta>
            <ta e="T453" id="Seg_9293" s="T441">In the water there is a lot of fish, I'll bring you as much as you want, even without nets."</ta>
            <ta e="T464" id="Seg_9294" s="T453">"There may be a lot of fish", the woman said, "but there is not a single [piece of] reindeer meat.</ta>
            <ta e="T472" id="Seg_9295" s="T464">I'm used to reindeer mear, I want to eat reindeer meat.</ta>
            <ta e="T478" id="Seg_9296" s="T472">In this ice chest, there are many reindeer.</ta>
            <ta e="T482" id="Seg_9297" s="T478">They are swimming like reindeer."</ta>
            <ta e="T486" id="Seg_9298" s="T482">The master of water laughs:</ta>
            <ta e="T495" id="Seg_9299" s="T486">"What kind of reindeer comes into this ice chest, there is only water.</ta>
            <ta e="T498" id="Seg_9300" s="T495">I do know that, though."</ta>
            <ta e="T500" id="Seg_9301" s="T498">The woman said:</ta>
            <ta e="T505" id="Seg_9302" s="T500">"I have seen that reindeer are swimming there.</ta>
            <ta e="T511" id="Seg_9303" s="T505">Some of them are white, some are spotted and some are black.</ta>
            <ta e="T513" id="Seg_9304" s="T511">A complete herd."</ta>
            <ta e="T524" id="Seg_9305" s="T513">"You are apparently a stupid woman", the master of water got angry, "what kind of reindeer will go in the water?"</ta>
            <ta e="T530" id="Seg_9306" s="T524">"You know yourself that we are living in the water.</ta>
            <ta e="T543" id="Seg_9307" s="T530">Therefore also the reindeer are going in the water", the woman said, "do look at the chest's inside, then".</ta>
            <ta e="T549" id="Seg_9308" s="T543">The master of water got angry and (forgot?).</ta>
            <ta e="T553" id="Seg_9309" s="T549">He looked into the chest in order to see the reindeer.</ta>
            <ta e="T560" id="Seg_9310" s="T553">The woman came and pulled his head into the water.</ta>
            <ta e="T567" id="Seg_9311" s="T560">The head of the master of water tore off completely, he died immediately.</ta>
            <ta e="T577" id="Seg_9312" s="T567">Pearl Beard's youngest went from door to door and found her older sisters.</ta>
            <ta e="T583" id="Seg_9313" s="T577">She scraped away the glue from the doors with a knife and freed them.</ta>
            <ta e="T587" id="Seg_9314" s="T583">The three of them came to the pole tent.</ta>
            <ta e="T596" id="Seg_9315" s="T587">Out of joy Pearl Beard made sliced fish for their daughters, he fed them with boiled fish.</ta>
            <ta e="T612" id="Seg_9316" s="T596">When the oldest daughter sees her thumb, when the middle daughter sees her middle finger, only then they remember the master of water.</ta>
            <ta e="T615" id="Seg_9317" s="T612">The tale was told by Ogdo Aksyonova.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T5" id="Seg_9318" s="T0">Hört einem Märchen zu, es heißt "Perlenbart".</ta>
            <ta e="T9" id="Seg_9319" s="T5">Es war wohl einmal ein Mann.</ta>
            <ta e="T16" id="Seg_9320" s="T9">Der Bart des alten Mannes war zwar licht, aber dennoch sehr lang.</ta>
            <ta e="T21" id="Seg_9321" s="T16">Seine Barthaare funkeln wie perlenbesetzte Fäden.</ta>
            <ta e="T28" id="Seg_9322" s="T21">Deshalb nannte man den alten Mann "Perlenbart".</ta>
            <ta e="T32" id="Seg_9323" s="T28">Perlenbart hat drei Töchter.</ta>
            <ta e="T35" id="Seg_9324" s="T32">Im Winter, im Sommer fischt er mit dem Netz.</ta>
            <ta e="T39" id="Seg_9325" s="T35">Sie ernähren sich nur von Fisch.</ta>
            <ta e="T45" id="Seg_9326" s="T39">Einmal ging Perlenbart, um die Netze zu kontrollieren.</ta>
            <ta e="T53" id="Seg_9327" s="T45">Als er sich gerade zum Eisloch hinunterbeugte, griff ihn irgendwer an seinem Bart.</ta>
            <ta e="T55" id="Seg_9328" s="T53">Er lässt ihn überhaupt nicht los.</ta>
            <ta e="T59" id="Seg_9329" s="T55">"Wer mag das sein?", dachte er.</ta>
            <ta e="T63" id="Seg_9330" s="T59">Aus dem Eisloch lehnte sich der Herr des Wassers.</ta>
            <ta e="T73" id="Seg_9331" s="T63">"Perlenbart", sagt er, "gibst du mir deine Tochter oder nicht, damit ich sie heiraten kann?</ta>
            <ta e="T79" id="Seg_9332" s="T73">Wenn du sie nicht gibst, wirst du nicht einen Fisch mehr fangen."</ta>
            <ta e="T85" id="Seg_9333" s="T79">"Womit soll ich meine Familie ernähren?", dachte er.</ta>
            <ta e="T87" id="Seg_9334" s="T85">Dann stimmte er zu.</ta>
            <ta e="T94" id="Seg_9335" s="T87">"Wenn du mich betrügst, ich finde dich doch", sagte der Herr des Wassers.</ta>
            <ta e="T103" id="Seg_9336" s="T94">Er nahm den Fäustling des alten Mannes und verschwand unter dem Eis.</ta>
            <ta e="T107" id="Seg_9337" s="T103">Perlenbart war sehr verwirrt.</ta>
            <ta e="T112" id="Seg_9338" s="T107">Er kam zu seinem Stangenzelt und erzählte seinen Töchtern.</ta>
            <ta e="T115" id="Seg_9339" s="T112">Die älteste Tochter freute sich:</ta>
            <ta e="T120" id="Seg_9340" s="T115">"Ich heirate den Herrn des Wassers.</ta>
            <ta e="T125" id="Seg_9341" s="T120">Ich werde die Reichste auf der Welt sein."</ta>
            <ta e="T131" id="Seg_9342" s="T125">Perlenbart verheiratete seine Tochter.</ta>
            <ta e="T136" id="Seg_9343" s="T131">Der Herr des Wassers ist sehr reich.</ta>
            <ta e="T143" id="Seg_9344" s="T136">Das (Ende?) seines Reichtums ist nicht zu sehen, er hat sehr viele Zimmer, Häuser.</ta>
            <ta e="T149" id="Seg_9345" s="T143">In einem Zimmer steht eine große Eistruhe.</ta>
            <ta e="T156" id="Seg_9346" s="T149">Der Herr des Wassers holte seine Frau und zeigte ihr die Eistruhe.</ta>
            <ta e="T160" id="Seg_9347" s="T156">"Gehe hin und schau, wo es dir beliebt.</ta>
            <ta e="T168" id="Seg_9348" s="T160">Stecke deinen Finger nur nicht in diese Truhe hinein", sagte er.</ta>
            <ta e="T176" id="Seg_9349" s="T168">Nachdem ihr Mann gegangen war, steckte diese Frau ungeduldig ihren Finger hinein.</ta>
            <ta e="T179" id="Seg_9350" s="T176">Das Wasser wurde zu Eis.</ta>
            <ta e="T183" id="Seg_9351" s="T179">Sie bekam ihren Finger kaum heraus.</ta>
            <ta e="T188" id="Seg_9352" s="T183">Sie sah, dass ihr Daumen ganz abgerissen war.</ta>
            <ta e="T193" id="Seg_9353" s="T188">Sie band ihn ganz fest und sitzt.</ta>
            <ta e="T197" id="Seg_9354" s="T193">Vor Angst standen ihre Augen weit offen.</ta>
            <ta e="T202" id="Seg_9355" s="T197">Der Herr des Wassers kam und erriet es sofort.</ta>
            <ta e="T208" id="Seg_9356" s="T202">"Du kannst nicht meine Frau sein", sagte er.</ta>
            <ta e="T216" id="Seg_9357" s="T208">Er brachte die Frau ins entfernteste Zimmer des Hauses und sperrte sie ein.</ta>
            <ta e="T224" id="Seg_9358" s="T216">Er schmierte die Tür mit aus dem Magen eines Störs gemachter Klebe voll.</ta>
            <ta e="T229" id="Seg_9359" s="T224">Perlenbart ging wieder fischen.</ta>
            <ta e="T238" id="Seg_9360" s="T229">Als er sich zum Eisloch hinunterbeugte, griff ihn der Herr des Wassers wieder am Bart.</ta>
            <ta e="T243" id="Seg_9361" s="T238">"Hol deine zweite Tochter, damit ich sie heiraten kann."</ta>
            <ta e="T250" id="Seg_9362" s="T243">Was soll der alte Mann machen, er gab seine mittlere Tochter.</ta>
            <ta e="T259" id="Seg_9363" s="T250">Als diese Frau sich den Reichtum ansieht, kann sie nicht verschnaufen, sie hört nicht auf sich zu wundern.</ta>
            <ta e="T266" id="Seg_9364" s="T259">Einmal brachte sie der Herr des Wassers zur Eistruhe.</ta>
            <ta e="T268" id="Seg_9365" s="T266">Er brachte sie und warnte sie:</ta>
            <ta e="T275" id="Seg_9366" s="T268">"Stecke weder Hand noch Finger in diese Truhe."</ta>
            <ta e="T286" id="Seg_9367" s="T275">Nachdem der Herr des Wassers auf seinem Schlitten weggefahren war, kam seine Frau zur Eistruhe und steckte ihren Finger hinein.</ta>
            <ta e="T290" id="Seg_9368" s="T286">Ihr Mittelfinger fror völlig ab.</ta>
            <ta e="T298" id="Seg_9369" s="T290">Sie band ihren Finger kräftig fest, der Herr des Wassers kam und erriet es sofort.</ta>
            <ta e="T319" id="Seg_9370" s="T298">"Ich brauche keine ungehorsame Frau", sagte er, sperrte sie in das entfernteste Zimmer des Hauses und schmierte die Tür reichlich mit aus dem Magen eines Störs gemachter Klebe voll.</ta>
            <ta e="T325" id="Seg_9371" s="T319">Perlenbart kam wieder, um die Netze zu kontrollieren.</ta>
            <ta e="T334" id="Seg_9372" s="T325">Als er sich zum Eisloch hinabbeugte, griff ihn der Herr des Wassers wieder am Bart.</ta>
            <ta e="T340" id="Seg_9373" s="T334">"Bring deine jüngste Tochter, damit ich sie heiraten kann", sagte er.</ta>
            <ta e="T346" id="Seg_9374" s="T340">Was soll er machen, er brachte die jüngste Tochter.</ta>
            <ta e="T350" id="Seg_9375" s="T346">Als sie gingen, sagte die Tochter:</ta>
            <ta e="T352" id="Seg_9376" s="T350">"Hab keine Angst, Vater.</ta>
            <ta e="T358" id="Seg_9377" s="T352">Ich komme bald und bringe auch meine älteren Schwestern mit.</ta>
            <ta e="T365" id="Seg_9378" s="T358">Der Herr des Wassers wird wohl nicht schlauer sein als ich."</ta>
            <ta e="T374" id="Seg_9379" s="T365">Der Herr des Wassers brachte seine dritte Frau zur Eistruhe und sagte:</ta>
            <ta e="T378" id="Seg_9380" s="T374">"Stecke deine Hand nicht in diese Truhe.</ta>
            <ta e="T381" id="Seg_9381" s="T378">Wenn ich wiederkomme, schaue ich nach.</ta>
            <ta e="T384" id="Seg_9382" s="T381">Du wirst mich nicht betrügen."</ta>
            <ta e="T391" id="Seg_9383" s="T384">"Warum warnt er mich", dachte diese Frau.</ta>
            <ta e="T399" id="Seg_9384" s="T391">"Was ist, wenn ich meine Hand hineinstecke, Wasser ist wie Wasser.</ta>
            <ta e="T405" id="Seg_9385" s="T399">Irgendeinen Trick wird es wohl geben."</ta>
            <ta e="T414" id="Seg_9386" s="T405">Sie griff einen Weißlachs und steckte seinen Schwanz in die Eistruhe.</ta>
            <ta e="T419" id="Seg_9387" s="T414">Der Schwanz des Weißlachses brach vollständig ab.</ta>
            <ta e="T427" id="Seg_9388" s="T419">Die Tochter von Perlenbart verstand, warum der Herr des Wassers sie gewarnt hatte.</ta>
            <ta e="T434" id="Seg_9389" s="T427">Sie sitzt neben der Truhe und flickt Netze.</ta>
            <ta e="T438" id="Seg_9390" s="T434">Der Herr des Wassers kam und wunderte sich:</ta>
            <ta e="T441" id="Seg_9391" s="T438">"Was für ein Netz flickst du da?</ta>
            <ta e="T453" id="Seg_9392" s="T441">Im Wasser gibt es viele Fische, auch ohne Netze bringe ich dir so viel, wie du möchtest."</ta>
            <ta e="T464" id="Seg_9393" s="T453">"Fische gibt es wohl viel", sagt die Frau, "aber es gibt nicht ein [Stück] Rentierfleisch.</ta>
            <ta e="T472" id="Seg_9394" s="T464">Ich bin an Rentierfleisch gewöhnt, ich möchte Rentierfleisch essen.</ta>
            <ta e="T478" id="Seg_9395" s="T472">In dieser Eistruhe sind viele Rentiere.</ta>
            <ta e="T482" id="Seg_9396" s="T478">Wie Fische schwimmen sie."</ta>
            <ta e="T486" id="Seg_9397" s="T482">Der Herr des Wassers lacht:</ta>
            <ta e="T495" id="Seg_9398" s="T486">"Was für ein Rentier kommt in diese Eistruhe, dort ist nur Wasser.</ta>
            <ta e="T498" id="Seg_9399" s="T495">Das weiß ich doch wohl."</ta>
            <ta e="T500" id="Seg_9400" s="T498">Die Frau sagte:</ta>
            <ta e="T505" id="Seg_9401" s="T500">"Ich habe gesehen, dass dort Rentiere schwimmen.</ta>
            <ta e="T511" id="Seg_9402" s="T505">Die einen sind weiß, die einen scheckig und die anderen sind schwarz.</ta>
            <ta e="T513" id="Seg_9403" s="T511">Eine ganze Herde."</ta>
            <ta e="T524" id="Seg_9404" s="T513">"Du bist offenbar eine dumme Frau", ärgerte sich der Herr des Wassers, "was für ein Rentier geht denn im Wasser?"</ta>
            <ta e="T530" id="Seg_9405" s="T524">"Du weißt selbst, dass wir im Wasser leben.</ta>
            <ta e="T543" id="Seg_9406" s="T530">Deshalb gehen auch die Rentiere im Wasser", sagte die Frau, "schau dir doch das Innere der Truhe an".</ta>
            <ta e="T549" id="Seg_9407" s="T543">Der Herr des Wassers wurde böse und (vergaß?).</ta>
            <ta e="T553" id="Seg_9408" s="T549">Er schaute in die Truhe, um die Rentiere zu sehen.</ta>
            <ta e="T560" id="Seg_9409" s="T553">Die Frau kam und zog seinen Kopf ins Wasser hinein.</ta>
            <ta e="T567" id="Seg_9410" s="T560">Der Kopf des Herrn des Wassers riss ganz ab, er starb sofort.</ta>
            <ta e="T577" id="Seg_9411" s="T567">Die jüngste Tochter von Perlenbart ging von Tür zu Tür und fand ihre älteren Schwestern.</ta>
            <ta e="T583" id="Seg_9412" s="T577">Sie kratzte die Klebe an den Türen mit einem Messer ab und befreite sie.</ta>
            <ta e="T587" id="Seg_9413" s="T583">Zu dritt kamen sie zum Stangenzelt.</ta>
            <ta e="T596" id="Seg_9414" s="T587">Perlenbart macht vor Freude für seine Töchter gehobelten Fisch, er verköstigt sie mit gekochtem Fisch.</ta>
            <ta e="T612" id="Seg_9415" s="T596">Wenn die älteste Tochter ihren Daumen sieht, wenn die mittlere Tochter ihren Mittelfinger sieht, nur dann erinnern sie sich an den Herrn des Wassers.</ta>
            <ta e="T615" id="Seg_9416" s="T612">Das Märchen erzählte Ogdo Aksjonova.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T5" id="Seg_9417" s="T0">Слушайте сказку, она называется "Бисерная борода".</ta>
            <ta e="T9" id="Seg_9418" s="T5">Был один старик, оказывается.</ta>
            <ta e="T16" id="Seg_9419" s="T9">У старика борода реденькая, но несмотря на это очень длинная.</ta>
            <ta e="T21" id="Seg_9420" s="T16">Пряди его бороды блестят, как нити с бисером.</ta>
            <ta e="T28" id="Seg_9421" s="T21">Поэтому старика прозвали Бисерная борода.</ta>
            <ta e="T32" id="Seg_9422" s="T28">У Бисерной бороды было три дочери.</ta>
            <ta e="T35" id="Seg_9423" s="T32">Зимой и летом рыбачит.</ta>
            <ta e="T39" id="Seg_9424" s="T35">Рыбой только живут.</ta>
            <ta e="T45" id="Seg_9425" s="T39">Однажды Бисерная борода пошёл проверять сети.</ta>
            <ta e="T53" id="Seg_9426" s="T45">Только нагнулся к проруби, кто-то за бороду его схватил.</ta>
            <ta e="T55" id="Seg_9427" s="T53">Никак не отпускает.</ta>
            <ta e="T59" id="Seg_9428" s="T55">"Кто это может быть?" – подумал он.</ta>
            <ta e="T63" id="Seg_9429" s="T59">Из проруби показался Хозяин воды.</ta>
            <ta e="T73" id="Seg_9430" s="T63">"Бисерная Борода," говорит он, "дочку отдашь или нет мне в жёны?</ta>
            <ta e="T79" id="Seg_9431" s="T73">Если не отдашь, ни одной рыбы не поймаешь".</ta>
            <ta e="T85" id="Seg_9432" s="T79">"Чем я семью буду кормить?" – подумал он.</ta>
            <ta e="T87" id="Seg_9433" s="T85">Потом согласился.</ta>
            <ta e="T94" id="Seg_9434" s="T87">"Если обманешь, всё равно найду", – сказал Хозяин воды.</ta>
            <ta e="T103" id="Seg_9435" s="T94">Рукавицы старика схватил и под лёд ушёл.</ta>
            <ta e="T107" id="Seg_9436" s="T103">Бисерная борода очень расстроился.</ta>
            <ta e="T112" id="Seg_9437" s="T107">Придя домой, своим дочерям рассказал.</ta>
            <ta e="T115" id="Seg_9438" s="T112">Старшая дочка обрадовалась:</ta>
            <ta e="T120" id="Seg_9439" s="T115">"Я выйду замуж за Хозяина воды,</ta>
            <ta e="T125" id="Seg_9440" s="T120">во всём мире самая богатая буду".</ta>
            <ta e="T131" id="Seg_9441" s="T125">Бисерная Борода выдал дочку замуж.</ta>
            <ta e="T136" id="Seg_9442" s="T131">Хозяин воды богатый-богатый.</ta>
            <ta e="T143" id="Seg_9443" s="T136">Богатству (края?) не видно, много комнат, домов.</ta>
            <ta e="T149" id="Seg_9444" s="T143">В одной комнате большой ледяной сундук стоит.</ta>
            <ta e="T156" id="Seg_9445" s="T149">Хозяин воды, приведя жену, показал ей ледяной сундук.</ta>
            <ta e="T160" id="Seg_9446" s="T156">"Где хочешь гуляй, смотри.</ta>
            <ta e="T168" id="Seg_9447" s="T160">Только в этот… сундук не засовывай палец свой", – сказал.</ta>
            <ta e="T176" id="Seg_9448" s="T168">Когда ушёл муж, эта женщина с нетерпением палец свой засунула. </ta>
            <ta e="T179" id="Seg_9449" s="T176">Вода в лёд превратилась.</ta>
            <ta e="T183" id="Seg_9450" s="T179">Еле выдернула палец свой.</ta>
            <ta e="T188" id="Seg_9451" s="T183">Увидела, большой палец оторвался.</ta>
            <ta e="T193" id="Seg_9452" s="T188">Его туго перевязала и сидит.</ta>
            <ta e="T197" id="Seg_9453" s="T193">От испуга глаза у неё расширились.</ta>
            <ta e="T202" id="Seg_9454" s="T197">Хозяин воды пришёл и сразу догадался.</ta>
            <ta e="T208" id="Seg_9455" s="T202">"Ты не сможешь быть моей женой", – сказал.</ta>
            <ta e="T216" id="Seg_9456" s="T208">Жену в самую крайнюю комнату отвёл и запер.</ta>
            <ta e="T224" id="Seg_9457" s="T216">Дверь плотно замазал клеем, сделанным из осетрового желудка.</ta>
            <ta e="T229" id="Seg_9458" s="T224">Бисерная борода опять на рыбалку пошёл.</ta>
            <ta e="T238" id="Seg_9459" s="T229">К проруби как только нагнулся, Хозяин воды опять его за бороду схватил.</ta>
            <ta e="T243" id="Seg_9460" s="T238">"Дай вторую дочку, чтобы мне женой стала."</ta>
            <ta e="T250" id="Seg_9461" s="T243">Старику делать нечего, среднюю дочь отдал.</ta>
            <ta e="T259" id="Seg_9462" s="T250">Эта женщина на богатство не может наглядеться, всё удивляется.</ta>
            <ta e="T266" id="Seg_9463" s="T259">Однажды вот к ледяному сундуку привёл её Хозяин воды.</ta>
            <ta e="T268" id="Seg_9464" s="T266">Привёл и предупредил: </ta>
            <ta e="T275" id="Seg_9465" s="T268">"В этот сундук ни руку, ни палец не засовывай."</ta>
            <ta e="T286" id="Seg_9466" s="T275">Хозяин воды когда на оленьей упряжке ускакал, его жена к ледяному сундуку подошла и палец свой засунула. </ta>
            <ta e="T290" id="Seg_9467" s="T286">Средний палец замёрз и оторвался.</ta>
            <ta e="T298" id="Seg_9468" s="T290">Палец свой крепко перевязала, Хозяин воды, прийдя, сразу догадался.</ta>
            <ta e="T319" id="Seg_9469" s="T298">"Мне непослушная жена не нужна", – сказал и в дальней комнате закрыл, а дверь плотно замазал клеем, сделанным из осетрового желудка.</ta>
            <ta e="T325" id="Seg_9470" s="T319">Бисерная Борода опять сети проверять пришёл.</ta>
            <ta e="T334" id="Seg_9471" s="T325">К проруби как только нагнулся, Хозяин воды опять его за бороду схватил.</ta>
            <ta e="T340" id="Seg_9472" s="T334">"Дай младшую дочь за меня замуж", – сказал.</ta>
            <ta e="T346" id="Seg_9473" s="T340">Делать нечего, младшую дочь отвёл.</ta>
            <ta e="T350" id="Seg_9474" s="T346">Когда шли, дочка сказала:</ta>
            <ta e="T352" id="Seg_9475" s="T350">"Не бойся, папа.</ta>
            <ta e="T358" id="Seg_9476" s="T352">Я быстро приду и сестёр тоже приведу.</ta>
            <ta e="T365" id="Seg_9477" s="T358">Хозяин воды не хитрее меня, наверное".</ta>
            <ta e="T374" id="Seg_9478" s="T365">Хозяин воды третью жену привёл к ледяному сундуку и сказал:</ta>
            <ta e="T378" id="Seg_9479" s="T374">"В этот сундук руку не суй.</ta>
            <ta e="T381" id="Seg_9480" s="T378">Когда вернусь, посмотрю.</ta>
            <ta e="T384" id="Seg_9481" s="T381">Меня не обманешь."</ta>
            <ta e="T391" id="Seg_9482" s="T384">"Почему меня предупреждает", подумала эта женщина.</ta>
            <ta e="T399" id="Seg_9483" s="T391">"Что будет, если руку засуну, вода как вода.</ta>
            <ta e="T405" id="Seg_9484" s="T399">Наверное, что-то здесь не так."</ta>
            <ta e="T414" id="Seg_9485" s="T405">Нельму схватила, хвост её засунула в ледяной сундук. </ta>
            <ta e="T419" id="Seg_9486" s="T414">Хвост у нельмы сразу оторвался.</ta>
            <ta e="T427" id="Seg_9487" s="T419">Дочь Бисерной бороды догадалась, почему Хозяин воды её предупредил.</ta>
            <ta e="T434" id="Seg_9488" s="T427">Сидя возле сундука, стала чинить сети.</ta>
            <ta e="T438" id="Seg_9489" s="T434">Хозяин воды удивился: </ta>
            <ta e="T441" id="Seg_9490" s="T438">"Что за сети ты чинишь?</ta>
            <ta e="T453" id="Seg_9491" s="T441">Под водой рыбы много, я тебе без сети сколько хочешь принести могу."</ta>
            <ta e="T464" id="Seg_9492" s="T453">"Рыбы-то много", сказала жена, "никакого оленьего мяса нет.</ta>
            <ta e="T472" id="Seg_9493" s="T464">Я к оленьему мясу привыкла, оленье мясо хочу есть.</ta>
            <ta e="T478" id="Seg_9494" s="T472">Вот в этом ледяном сундуке оленей много.</ta>
            <ta e="T482" id="Seg_9495" s="T478">Как рыбы плавают."</ta>
            <ta e="T486" id="Seg_9496" s="T482">Хозяин воды как засмеялся:</ta>
            <ta e="T495" id="Seg_9497" s="T486">"В этом ледяном сундуке откуда возьмуться олени, здесь вода только.</ta>
            <ta e="T498" id="Seg_9498" s="T495">Я знаю."</ta>
            <ta e="T500" id="Seg_9499" s="T498">Жена сказала: </ta>
            <ta e="T505" id="Seg_9500" s="T500">"Я видела, как олени плавают.</ta>
            <ta e="T511" id="Seg_9501" s="T505">Одни белые, другие пятнистые, остальные чёрные.</ta>
            <ta e="T513" id="Seg_9502" s="T511">Целое стадо."</ta>
            <ta e="T524" id="Seg_9503" s="T513">"Глупая женщина ты, оказывается", рассердился Хозяин воды, "какие олени под водой будут ходить?"</ta>
            <ta e="T530" id="Seg_9504" s="T524">"Сам знаешь, мы под водой живём.</ta>
            <ta e="T543" id="Seg_9505" s="T530">Поэтому олени тоже под водой ходят", сказала жена, "посмотри внутрь сундука."</ta>
            <ta e="T549" id="Seg_9506" s="T543">Хозяин воды, рассердившись, (?).</ta>
            <ta e="T553" id="Seg_9507" s="T549">В сундук заглянул, чтобы оленей увидеть.</ta>
            <ta e="T560" id="Seg_9508" s="T553">Жена подошла и [его] голову в воду засунула.</ta>
            <ta e="T567" id="Seg_9509" s="T560">Голова у Хозяина воды оторвалась, и он сразу умер.</ta>
            <ta e="T577" id="Seg_9510" s="T567">Младшая дочь Бисерной бороды от двери к двери ходила, сестёр нашла.</ta>
            <ta e="T583" id="Seg_9511" s="T577">Дверной клей ножом соскребла и освободила их.</ta>
            <ta e="T587" id="Seg_9512" s="T583">Втроём к чуму своему пришли.</ta>
            <ta e="T596" id="Seg_9513" s="T587">Бисерная борода от радости дочкам строганину делает, варёной рыбой кормит.</ta>
            <ta e="T612" id="Seg_9514" s="T596">Когда старшая дочка на свой большой палец посмотрит, а средняя на свой средний палец посмотрит, только тогда Хозяина воды вспоминают.</ta>
            <ta e="T615" id="Seg_9515" s="T612">Сказку рассказала Огдо Аксёнова.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T5" id="Seg_9516" s="T0">Слушайте сказку, называется "Бисерная борода".</ta>
            <ta e="T9" id="Seg_9517" s="T5">Один старик был, оказывается.</ta>
            <ta e="T16" id="Seg_9518" s="T9">У старика борода реденькая, но несмотря на это очень длинная.</ta>
            <ta e="T21" id="Seg_9519" s="T16">Бороды его (волосы бороды) как с бисером нити блистят.</ta>
            <ta e="T28" id="Seg_9520" s="T21">Позтому старика прозвали Бисерная борода.</ta>
            <ta e="T32" id="Seg_9521" s="T28">У Бисерной бороды было три дочери.</ta>
            <ta e="T35" id="Seg_9522" s="T32">Зимой и летом рыбачит.</ta>
            <ta e="T39" id="Seg_9523" s="T35">Рыбой только живут.</ta>
            <ta e="T45" id="Seg_9524" s="T39">Однажды Бисерная борода проверять сети пошёл.</ta>
            <ta e="T53" id="Seg_9525" s="T45">В прорубь как только нагнулся, кто-то за бороду его схватил.</ta>
            <ta e="T55" id="Seg_9526" s="T53">Никак не отпустит.</ta>
            <ta e="T59" id="Seg_9527" s="T55">"Кто это может быть?" – подумал.</ta>
            <ta e="T63" id="Seg_9528" s="T59">Из проруби показался Хозяин воды (Водяной).</ta>
            <ta e="T73" id="Seg_9529" s="T63">Бисерная Борода говорит: "Дочку отдашь или нет мне в жёны?</ta>
            <ta e="T79" id="Seg_9530" s="T73">Если не отдашь, ни одной рыбы не поймаешь".</ta>
            <ta e="T85" id="Seg_9531" s="T79">"Чем я семью буду кормить?" – подумал.</ta>
            <ta e="T87" id="Seg_9532" s="T85">Потом согласился.</ta>
            <ta e="T94" id="Seg_9533" s="T87">"Если обманешь, всё равно найду", – сказал Хозяин воды.</ta>
            <ta e="T103" id="Seg_9534" s="T94">Старика рукавицы схватив, под лёд ушёл.</ta>
            <ta e="T107" id="Seg_9535" s="T103">Бисерная борода очень расстроился.</ta>
            <ta e="T112" id="Seg_9536" s="T107">К своему чуму прийдя, своим дочерям рассказал.</ta>
            <ta e="T115" id="Seg_9537" s="T112">Старшая дочка обрадовалась:</ta>
            <ta e="T120" id="Seg_9538" s="T115">"Я выйду замуж за Хозяина воды,</ta>
            <ta e="T125" id="Seg_9539" s="T120">во всём мире самая богатая буду".</ta>
            <ta e="T131" id="Seg_9540" s="T125">Бисерная Борода выдал дочку замуж.</ta>
            <ta e="T136" id="Seg_9541" s="T131">Хозяин воды богатый-богатый.</ta>
            <ta e="T143" id="Seg_9542" s="T136">Богатству края(?) не видно, много комнат, домов.</ta>
            <ta e="T149" id="Seg_9543" s="T143">В одной комнате большой ледяной сундук стоит.</ta>
            <ta e="T156" id="Seg_9544" s="T149">Хозяин воды приведя жену, ледяной сундук показал ей.</ta>
            <ta e="T160" id="Seg_9545" s="T156">"Где хочешь гуляй, смотри.</ta>
            <ta e="T168" id="Seg_9546" s="T160">Только в этот.. сундук не засовывай палец свой", – сказал.</ta>
            <ta e="T176" id="Seg_9547" s="T168">Когда ушёл муж, эта женщина с нетерпением палец свой засунула. </ta>
            <ta e="T179" id="Seg_9548" s="T176">Вода в лёд превратилась.</ta>
            <ta e="T183" id="Seg_9549" s="T179">Еле выдернула палец свой.</ta>
            <ta e="T188" id="Seg_9550" s="T183">Увидела, большой палец оторвался.</ta>
            <ta e="T193" id="Seg_9551" s="T188">Его туго перевязала и сидит.</ta>
            <ta e="T197" id="Seg_9552" s="T193">От испуга глаза широко расширились.</ta>
            <ta e="T202" id="Seg_9553" s="T197">Хозяин воды пришёл и сразу догадался.</ta>
            <ta e="T208" id="Seg_9554" s="T202">"Мне женой ты не сможешь стать", – сказал.</ta>
            <ta e="T216" id="Seg_9555" s="T208">Жену в самую крайнюю комнату отведя, запер.</ta>
            <ta e="T224" id="Seg_9556" s="T216">Дверь плотно из осетрового желудка сделанным клеем сильно замазал.</ta>
            <ta e="T229" id="Seg_9557" s="T224">Бисерная борода опять на рыбалку пришёл.</ta>
            <ta e="T238" id="Seg_9558" s="T229">В проруб как только нагнулся, Хозяин воды опять за бороду схватил.</ta>
            <ta e="T243" id="Seg_9559" s="T238">"Дай вторую дочку, чтобы женой сделать мне."</ta>
            <ta e="T250" id="Seg_9560" s="T243">Старику делать нечего, среднюю дочь отдал.</ta>
            <ta e="T259" id="Seg_9561" s="T250">Эта женщина на это богатство не может наглядеться, всё удивляется.</ta>
            <ta e="T266" id="Seg_9562" s="T259">Однажды вот к ледяному сундуку привёл Хозяин воды.</ta>
            <ta e="T268" id="Seg_9563" s="T266">Привёл и предупредил: </ta>
            <ta e="T275" id="Seg_9564" s="T268">"В этот сундук ни руку, ни палец не засовывай."</ta>
            <ta e="T286" id="Seg_9565" s="T275">Хозяин воды когда на оленьей упряжке ускакал, его жена к ледяному сундуку подошла и палец свой засунула. </ta>
            <ta e="T290" id="Seg_9566" s="T286">Средний палец оторвавшись замёрз.</ta>
            <ta e="T298" id="Seg_9567" s="T290">Палец свой крепко перевязала, Хозяин воды прийдя, сразу догадался.</ta>
            <ta e="T319" id="Seg_9568" s="T298">"Мне непослушная девушка не нужна", – сказав, в дальней комнате закрыл, дверь плотно осетровым желужком сделанным клеем замазал.</ta>
            <ta e="T325" id="Seg_9569" s="T319">Бисерная Борода опять сети проверять пришёл.</ta>
            <ta e="T334" id="Seg_9570" s="T325">В прорубь как только нагнулся, Хозяин воды опять за бороду схватил.</ta>
            <ta e="T340" id="Seg_9571" s="T334">"Дай младшую дочь за меня замуж", – сказал.</ta>
            <ta e="T346" id="Seg_9572" s="T340">Делать нечего, младшую дочь отвёл.</ta>
            <ta e="T350" id="Seg_9573" s="T346">Когда шли, дочка сказала:</ta>
            <ta e="T352" id="Seg_9574" s="T350">"Не бойся, папа.</ta>
            <ta e="T358" id="Seg_9575" s="T352">Я быстро приду и сестёр тоже приведу.</ta>
            <ta e="T365" id="Seg_9576" s="T358">Хозяин воды не хитрее меня, кажется".</ta>
            <ta e="T374" id="Seg_9577" s="T365">Хозяин воды третью жену привёл к ледяному сундуку и сказал:</ta>
            <ta e="T378" id="Seg_9578" s="T374">"В этот сундук руку не суй.</ta>
            <ta e="T381" id="Seg_9579" s="T378">Когда вернусь, посмотрю.</ta>
            <ta e="T384" id="Seg_9580" s="T381">Меня не обманешь."</ta>
            <ta e="T391" id="Seg_9581" s="T384">"Почему меня предупреждает", подумала эта женщина.</ta>
            <ta e="T399" id="Seg_9582" s="T391">"Что будет, если руку засуну, вода как вода.</ta>
            <ta e="T405" id="Seg_9583" s="T399">Наверное, что-то не то есть, наверно."</ta>
            <ta e="T414" id="Seg_9584" s="T405">Нельму схватив, хвост засунул в ледяной сундук. </ta>
            <ta e="T419" id="Seg_9585" s="T414">Нельмы хвост сразу оторвался.</ta>
            <ta e="T427" id="Seg_9586" s="T419">Бисерной бороды дочь догадалась, почему Хозяин воды её предупредил.</ta>
            <ta e="T434" id="Seg_9587" s="T427">Возле сундука сидя, чинить сети стал</ta>
            <ta e="T438" id="Seg_9588" s="T434">Хозяин воды удивился: </ta>
            <ta e="T441" id="Seg_9589" s="T438">"Что за сети ты чинишь?</ta>
            <ta e="T453" id="Seg_9590" s="T441">Под водой рыбы много, без сети я тебе сколько хочешь принести могу."</ta>
            <ta e="T464" id="Seg_9591" s="T453">"Рыбы-то много", сказала жена, "ни одного оленьего мяса нет.</ta>
            <ta e="T472" id="Seg_9592" s="T464">Я к оленьему мясу привыкла, оленье мясо хочу есть.</ta>
            <ta e="T478" id="Seg_9593" s="T472">Вот в этом ледяном сундуке оленей много.</ta>
            <ta e="T482" id="Seg_9594" s="T478">Как рыбы плавают."</ta>
            <ta e="T486" id="Seg_9595" s="T482">Хозяин воды как засмеялся:</ta>
            <ta e="T495" id="Seg_9596" s="T486">"В этом ледяном сундуке откуда возьмуться олени, здесь вода только.</ta>
            <ta e="T498" id="Seg_9597" s="T495">Я знаю."</ta>
            <ta e="T500" id="Seg_9598" s="T498">Жена сказала: </ta>
            <ta e="T505" id="Seg_9599" s="T500">"Я видела, как олени плавают.</ta>
            <ta e="T511" id="Seg_9600" s="T505">Одни белые, другие пятнистые, остальные чёрные.</ta>
            <ta e="T513" id="Seg_9601" s="T511">Целое стадо."</ta>
            <ta e="T524" id="Seg_9602" s="T513">"Глупая женщина ты, оказывается", рассердился Хозяин воды, "какие олени под водой будут ходить?"</ta>
            <ta e="T530" id="Seg_9603" s="T524">"Сам знаешь, мы под водой живём.</ta>
            <ta e="T543" id="Seg_9604" s="T530">Поэтому олени тоже под водой ходят", сказала жена, "посмотри внутрь сундука."</ta>
            <ta e="T549" id="Seg_9605" s="T543">Хозяин воды, рассердившись, нырнул напрочь.</ta>
            <ta e="T553" id="Seg_9606" s="T549">В сундук заглянул, чтобы оленей увидеть.</ta>
            <ta e="T560" id="Seg_9607" s="T553">Жена подойдя, голову в воду засунула.</ta>
            <ta e="T567" id="Seg_9608" s="T560">Хозяина воды голова оторвалась, сразу умер.</ta>
            <ta e="T577" id="Seg_9609" s="T567">Бисерной бороды младшая дочка из двери к двери ходила, сестёр нашла.</ta>
            <ta e="T583" id="Seg_9610" s="T577">Дверные клеи ножом соскребла и свободнее сделала.</ta>
            <ta e="T587" id="Seg_9611" s="T583">Втроём к чуму своему пришли.</ta>
            <ta e="T596" id="Seg_9612" s="T587">Бисерная борода от радости дочкам страганину делает, вареной рыбой кормит.</ta>
            <ta e="T612" id="Seg_9613" s="T596">Старшая дочка на свой большой палец смотря, потом средняя дочка на свой средний палец смотря, только тогда Хозяина воды вспоминают.</ta>
            <ta e="T615" id="Seg_9614" s="T612">Сказку расказала Огдуо Аксёнова.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T530" id="Seg_9615" s="T524">[DCh]: The form "bejem" of "beje-" is unexpected here, instead one would expect the form "bejeŋ".</ta>
            <ta e="T549" id="Seg_9616" s="T543">[DCh]: Not clear what is meant here with the last part of the sentence.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T559" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
            <conversion-tli id="T571" />
            <conversion-tli id="T572" />
            <conversion-tli id="T573" />
            <conversion-tli id="T574" />
            <conversion-tli id="T575" />
            <conversion-tli id="T576" />
            <conversion-tli id="T577" />
            <conversion-tli id="T578" />
            <conversion-tli id="T579" />
            <conversion-tli id="T580" />
            <conversion-tli id="T581" />
            <conversion-tli id="T582" />
            <conversion-tli id="T583" />
            <conversion-tli id="T584" />
            <conversion-tli id="T585" />
            <conversion-tli id="T586" />
            <conversion-tli id="T587" />
            <conversion-tli id="T588" />
            <conversion-tli id="T589" />
            <conversion-tli id="T590" />
            <conversion-tli id="T591" />
            <conversion-tli id="T592" />
            <conversion-tli id="T593" />
            <conversion-tli id="T594" />
            <conversion-tli id="T595" />
            <conversion-tli id="T596" />
            <conversion-tli id="T597" />
            <conversion-tli id="T598" />
            <conversion-tli id="T599" />
            <conversion-tli id="T600" />
            <conversion-tli id="T601" />
            <conversion-tli id="T602" />
            <conversion-tli id="T603" />
            <conversion-tli id="T604" />
            <conversion-tli id="T605" />
            <conversion-tli id="T606" />
            <conversion-tli id="T607" />
            <conversion-tli id="T608" />
            <conversion-tli id="T609" />
            <conversion-tli id="T610" />
            <conversion-tli id="T611" />
            <conversion-tli id="T612" />
            <conversion-tli id="T613" />
            <conversion-tli id="T614" />
            <conversion-tli id="T615" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg"
                          name="gg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top"
                          name="Top"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc"
                          name="Foc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
