<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID1DDE6298-3D51-C240-61C0-00B8BCD66563">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>BaRD_1930_MasterOfWorld_flk</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\flk\BaRD_1930_MasterOfWorld_flk\BaRD_1930_MasterOfWorld_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">221</ud-information>
            <ud-information attribute-name="# HIAT:w">172</ud-information>
            <ud-information attribute-name="# e">172</ud-information>
            <ud-information attribute-name="# HIAT:u">22</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="BaRD">
            <abbreviation>BaRD</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="4.0" type="appl" />
         <tli id="T1" time="4.5" type="appl" />
         <tli id="T2" time="5.0" type="appl" />
         <tli id="T3" time="5.5" type="appl" />
         <tli id="T4" time="6.0" type="appl" />
         <tli id="T5" time="6.5" type="appl" />
         <tli id="T6" time="7.0" type="appl" />
         <tli id="T7" time="7.5" type="appl" />
         <tli id="T8" time="8.0" type="appl" />
         <tli id="T9" time="8.5" type="appl" />
         <tli id="T10" time="9.0" type="appl" />
         <tli id="T11" time="9.5" type="appl" />
         <tli id="T12" time="10.0" type="appl" />
         <tli id="T13" time="10.5" type="appl" />
         <tli id="T14" time="11.0" type="appl" />
         <tli id="T15" time="11.5" type="appl" />
         <tli id="T16" time="12.0" type="appl" />
         <tli id="T17" time="12.5" type="appl" />
         <tli id="T18" time="13.0" type="appl" />
         <tli id="T19" time="13.5" type="appl" />
         <tli id="T20" time="14.0" type="appl" />
         <tli id="T21" time="14.5" type="appl" />
         <tli id="T22" time="15.0" type="appl" />
         <tli id="T23" time="15.5" type="appl" />
         <tli id="T24" time="16.0" type="appl" />
         <tli id="T25" time="16.5" type="appl" />
         <tli id="T26" time="17.0" type="appl" />
         <tli id="T27" time="17.5" type="appl" />
         <tli id="T28" time="18.0" type="appl" />
         <tli id="T29" time="18.5" type="appl" />
         <tli id="T30" time="19.0" type="appl" />
         <tli id="T31" time="19.5" type="appl" />
         <tli id="T32" time="20.0" type="appl" />
         <tli id="T33" time="20.5" type="appl" />
         <tli id="T34" time="21.0" type="appl" />
         <tli id="T35" time="21.5" type="appl" />
         <tli id="T36" time="22.0" type="appl" />
         <tli id="T37" time="22.5" type="appl" />
         <tli id="T38" time="23.0" type="appl" />
         <tli id="T39" time="23.5" type="appl" />
         <tli id="T40" time="24.0" type="appl" />
         <tli id="T41" time="24.5" type="appl" />
         <tli id="T42" time="25.0" type="appl" />
         <tli id="T43" time="25.5" type="appl" />
         <tli id="T44" time="26.0" type="appl" />
         <tli id="T45" time="26.5" type="appl" />
         <tli id="T46" time="27.0" type="appl" />
         <tli id="T47" time="27.5" type="appl" />
         <tli id="T48" time="28.0" type="appl" />
         <tli id="T49" time="28.5" type="appl" />
         <tli id="T50" time="29.0" type="appl" />
         <tli id="T51" time="29.5" type="appl" />
         <tli id="T52" time="30.0" type="appl" />
         <tli id="T53" time="30.5" type="appl" />
         <tli id="T54" time="31.0" type="appl" />
         <tli id="T55" time="31.5" type="appl" />
         <tli id="T56" time="32.0" type="appl" />
         <tli id="T57" time="32.5" type="appl" />
         <tli id="T58" time="33.0" type="appl" />
         <tli id="T59" time="33.5" type="appl" />
         <tli id="T60" time="34.0" type="appl" />
         <tli id="T61" time="34.5" type="appl" />
         <tli id="T62" time="35.0" type="appl" />
         <tli id="T63" time="35.5" type="appl" />
         <tli id="T64" time="36.0" type="appl" />
         <tli id="T65" time="36.5" type="appl" />
         <tli id="T66" time="37.0" type="appl" />
         <tli id="T67" time="37.5" type="appl" />
         <tli id="T68" time="38.0" type="appl" />
         <tli id="T69" time="38.5" type="appl" />
         <tli id="T70" time="39.0" type="appl" />
         <tli id="T71" time="39.5" type="appl" />
         <tli id="T72" time="40.0" type="appl" />
         <tli id="T73" time="40.5" type="appl" />
         <tli id="T74" time="41.0" type="appl" />
         <tli id="T75" time="41.5" type="appl" />
         <tli id="T76" time="42.0" type="appl" />
         <tli id="T77" time="42.5" type="appl" />
         <tli id="T78" time="43.0" type="appl" />
         <tli id="T79" time="43.5" type="appl" />
         <tli id="T80" time="44.0" type="appl" />
         <tli id="T81" time="44.5" type="appl" />
         <tli id="T82" time="45.0" type="appl" />
         <tli id="T83" time="45.5" type="appl" />
         <tli id="T84" time="46.0" type="appl" />
         <tli id="T85" time="46.5" type="appl" />
         <tli id="T86" time="47.0" type="appl" />
         <tli id="T87" time="47.5" type="appl" />
         <tli id="T88" time="48.0" type="appl" />
         <tli id="T89" time="48.5" type="appl" />
         <tli id="T90" time="49.0" type="appl" />
         <tli id="T91" time="49.5" type="appl" />
         <tli id="T92" time="50.0" type="appl" />
         <tli id="T93" time="50.5" type="appl" />
         <tli id="T94" time="51.0" type="appl" />
         <tli id="T95" time="51.5" type="appl" />
         <tli id="T96" time="52.0" type="appl" />
         <tli id="T97" time="52.5" type="appl" />
         <tli id="T98" time="53.0" type="appl" />
         <tli id="T99" time="53.5" type="appl" />
         <tli id="T100" time="54.0" type="appl" />
         <tli id="T101" time="54.5" type="appl" />
         <tli id="T102" time="55.0" type="appl" />
         <tli id="T103" time="55.5" type="appl" />
         <tli id="T104" time="56.0" type="appl" />
         <tli id="T105" time="56.5" type="appl" />
         <tli id="T106" time="57.0" type="appl" />
         <tli id="T107" time="57.5" type="appl" />
         <tli id="T108" time="58.0" type="appl" />
         <tli id="T109" time="58.5" type="appl" />
         <tli id="T110" time="59.0" type="appl" />
         <tli id="T111" time="59.5" type="appl" />
         <tli id="T112" time="60.0" type="appl" />
         <tli id="T113" time="60.5" type="appl" />
         <tli id="T114" time="61.0" type="appl" />
         <tli id="T115" time="61.5" type="appl" />
         <tli id="T116" time="62.0" type="appl" />
         <tli id="T117" time="62.5" type="appl" />
         <tli id="T118" time="63.0" type="appl" />
         <tli id="T119" time="63.5" type="appl" />
         <tli id="T120" time="64.0" type="appl" />
         <tli id="T121" time="64.5" type="appl" />
         <tli id="T122" time="65.0" type="appl" />
         <tli id="T123" time="65.5" type="appl" />
         <tli id="T124" time="66.0" type="appl" />
         <tli id="T125" time="66.5" type="appl" />
         <tli id="T126" time="67.0" type="appl" />
         <tli id="T127" time="67.5" type="appl" />
         <tli id="T128" time="68.0" type="appl" />
         <tli id="T129" time="68.5" type="appl" />
         <tli id="T130" time="69.0" type="appl" />
         <tli id="T131" time="69.5" type="appl" />
         <tli id="T132" time="70.0" type="appl" />
         <tli id="T133" time="70.5" type="appl" />
         <tli id="T134" time="71.0" type="appl" />
         <tli id="T135" time="71.5" type="appl" />
         <tli id="T136" time="72.0" type="appl" />
         <tli id="T137" time="72.5" type="appl" />
         <tli id="T138" time="73.0" type="appl" />
         <tli id="T139" time="73.5" type="appl" />
         <tli id="T140" time="74.0" type="appl" />
         <tli id="T141" time="74.5" type="appl" />
         <tli id="T142" time="75.0" type="appl" />
         <tli id="T143" time="75.5" type="appl" />
         <tli id="T144" time="76.0" type="appl" />
         <tli id="T145" time="76.5" type="appl" />
         <tli id="T146" time="77.0" type="appl" />
         <tli id="T147" time="77.5" type="appl" />
         <tli id="T148" time="78.0" type="appl" />
         <tli id="T149" time="78.5" type="appl" />
         <tli id="T150" time="79.0" type="appl" />
         <tli id="T151" time="79.5" type="appl" />
         <tli id="T152" time="80.0" type="appl" />
         <tli id="T153" time="80.5" type="appl" />
         <tli id="T154" time="81.0" type="appl" />
         <tli id="T155" time="81.5" type="appl" />
         <tli id="T156" time="82.0" type="appl" />
         <tli id="T157" time="82.5" type="appl" />
         <tli id="T158" time="83.0" type="appl" />
         <tli id="T159" time="83.5" type="appl" />
         <tli id="T160" time="84.0" type="appl" />
         <tli id="T161" time="84.5" type="appl" />
         <tli id="T162" time="85.0" type="appl" />
         <tli id="T163" time="85.5" type="appl" />
         <tli id="T164" time="86.0" type="appl" />
         <tli id="T165" time="86.5" type="appl" />
         <tli id="T166" time="87.0" type="appl" />
         <tli id="T167" time="87.5" type="appl" />
         <tli id="T168" time="88.0" type="appl" />
         <tli id="T169" time="88.5" type="appl" />
         <tli id="T170" time="89.0" type="appl" />
         <tli id="T171" time="89.5" type="appl" />
         <tli id="T172" time="90.0" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="BaRD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T172" id="Seg_0" n="sc" s="T0">
               <ts e="T3" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Hir</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">iččileːk</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">bu͡olar</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T12" id="Seg_14" n="HIAT:u" s="T3">
                  <ts e="T4" id="Seg_16" n="HIAT:w" s="T3">Ol</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">hir</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">iččititten</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">kuttanan</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">ogoloru</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">ki͡ehe</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">tahaːra</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">ɨːppat</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_40" n="HIAT:w" s="T11">etiler</ts>
                  <nts id="Seg_41" n="HIAT:ip">.</nts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T19" id="Seg_44" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_46" n="HIAT:w" s="T12">Ol</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_49" n="HIAT:w" s="T13">hir</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_52" n="HIAT:w" s="T14">iččite</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_55" n="HIAT:w" s="T15">hɨldʼar</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_58" n="HIAT:w" s="T16">hahɨllarɨ</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_61" n="HIAT:w" s="T17">kölünen</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_64" n="HIAT:w" s="T18">baran</ts>
                  <nts id="Seg_65" n="HIAT:ip">.</nts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T27" id="Seg_68" n="HIAT:u" s="T19">
                  <ts e="T20" id="Seg_70" n="HIAT:w" s="T19">Bɨlɨr</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_73" n="HIAT:w" s="T20">min</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_76" n="HIAT:w" s="T21">ijem</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_79" n="HIAT:w" s="T22">bɨraːtɨn</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_82" n="HIAT:w" s="T23">ilpite</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_85" n="HIAT:w" s="T24">ogurduk</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_88" n="HIAT:w" s="T25">hir</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_91" n="HIAT:w" s="T26">iččite</ts>
                  <nts id="Seg_92" n="HIAT:ip">.</nts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T43" id="Seg_95" n="HIAT:u" s="T27">
                  <ts e="T28" id="Seg_97" n="HIAT:w" s="T27">Oburgu</ts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_100" n="HIAT:w" s="T28">ogonu</ts>
                  <nts id="Seg_101" n="HIAT:ip">,</nts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_104" n="HIAT:w" s="T29">tahaːra</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_107" n="HIAT:w" s="T30">hɨrɨttagɨna</ts>
                  <nts id="Seg_108" n="HIAT:ip">,</nts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_111" n="HIAT:w" s="T31">biːr</ts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_114" n="HIAT:w" s="T32">ogonnʼor</ts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_117" n="HIAT:w" s="T33">kelbit</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_120" n="HIAT:w" s="T34">hahɨlɨ</ts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_123" n="HIAT:w" s="T35">kölünen</ts>
                  <nts id="Seg_124" n="HIAT:ip">,</nts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_127" n="HIAT:w" s="T36">ogonu</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_130" n="HIAT:w" s="T37">ɨlbɨt</ts>
                  <nts id="Seg_131" n="HIAT:ip">,</nts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_134" n="HIAT:w" s="T38">hɨrgatɨgar</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_137" n="HIAT:w" s="T39">bɨrakpɨt</ts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_140" n="HIAT:w" s="T40">da</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_143" n="HIAT:w" s="T41">kötüten</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_146" n="HIAT:w" s="T42">ispit</ts>
                  <nts id="Seg_147" n="HIAT:ip">.</nts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T48" id="Seg_150" n="HIAT:u" s="T43">
                  <ts e="T44" id="Seg_152" n="HIAT:w" s="T43">Dʼi͡etiger</ts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_155" n="HIAT:w" s="T44">bɨrakpɨt</ts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_158" n="HIAT:w" s="T45">daː</ts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_161" n="HIAT:w" s="T46">töttöru</ts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_164" n="HIAT:w" s="T47">taksɨbɨt</ts>
                  <nts id="Seg_165" n="HIAT:ip">.</nts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T54" id="Seg_168" n="HIAT:u" s="T48">
                  <ts e="T49" id="Seg_170" n="HIAT:w" s="T48">Manɨga</ts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_173" n="HIAT:w" s="T49">bu</ts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_176" n="HIAT:w" s="T50">ogogo</ts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_179" n="HIAT:w" s="T51">biːr</ts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_182" n="HIAT:w" s="T52">emeːksin</ts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_185" n="HIAT:w" s="T53">kelbit</ts>
                  <nts id="Seg_186" n="HIAT:ip">.</nts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T72" id="Seg_189" n="HIAT:u" s="T54">
                  <nts id="Seg_190" n="HIAT:ip">"</nts>
                  <ts e="T55" id="Seg_192" n="HIAT:w" s="T54">Dʼe</ts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_195" n="HIAT:w" s="T55">kugu</ts>
                  <nts id="Seg_196" n="HIAT:ip">"</nts>
                  <nts id="Seg_197" n="HIAT:ip">,</nts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_200" n="HIAT:w" s="T56">di͡ebit</ts>
                  <nts id="Seg_201" n="HIAT:ip">,</nts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_203" n="HIAT:ip">"</nts>
                  <ts e="T58" id="Seg_205" n="HIAT:w" s="T57">eheŋ</ts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_208" n="HIAT:w" s="T58">tagɨsta</ts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_211" n="HIAT:w" s="T59">ɨttarɨn</ts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_214" n="HIAT:w" s="T60">bulguta</ts>
                  <nts id="Seg_215" n="HIAT:ip">,</nts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_218" n="HIAT:w" s="T61">kelen</ts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_221" n="HIAT:w" s="T62">ahattagɨna</ts>
                  <nts id="Seg_222" n="HIAT:ip">,</nts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_225" n="HIAT:w" s="T63">mantan</ts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_228" n="HIAT:w" s="T64">dojdugar</ts>
                  <nts id="Seg_229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_231" n="HIAT:w" s="T65">ere</ts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_234" n="HIAT:w" s="T66">bararga</ts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_237" n="HIAT:w" s="T67">bagalaːk</ts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_240" n="HIAT:w" s="T68">bu͡ollakkɨna</ts>
                  <nts id="Seg_241" n="HIAT:ip">,</nts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_244" n="HIAT:w" s="T69">bi͡erer</ts>
                  <nts id="Seg_245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_247" n="HIAT:w" s="T70">ahɨn</ts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_250" n="HIAT:w" s="T71">hi͡ejegin</ts>
                  <nts id="Seg_251" n="HIAT:ip">.</nts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T81" id="Seg_254" n="HIAT:u" s="T72">
                  <ts e="T73" id="Seg_256" n="HIAT:w" s="T72">Manna</ts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_259" n="HIAT:w" s="T73">kaːlan</ts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_262" n="HIAT:w" s="T74">kergen</ts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_265" n="HIAT:w" s="T75">bu͡olu͡ok</ts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_268" n="HIAT:w" s="T76">bagalaːk</ts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_271" n="HIAT:w" s="T77">bu͡ollakkɨna</ts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_274" n="HIAT:w" s="T78">oččogo</ts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_277" n="HIAT:w" s="T79">ere</ts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_280" n="HIAT:w" s="T80">ahaːr</ts>
                  <nts id="Seg_281" n="HIAT:ip">.</nts>
                  <nts id="Seg_282" n="HIAT:ip">"</nts>
                  <nts id="Seg_283" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T88" id="Seg_285" n="HIAT:u" s="T81">
                  <ts e="T82" id="Seg_287" n="HIAT:w" s="T81">Ehete</ts>
                  <nts id="Seg_288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_290" n="HIAT:w" s="T82">kiːren</ts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_293" n="HIAT:w" s="T83">kündülüː</ts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_296" n="HIAT:w" s="T84">hataːbɨt</ts>
                  <nts id="Seg_297" n="HIAT:ip">,</nts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_300" n="HIAT:w" s="T85">tugu</ts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_303" n="HIAT:w" s="T86">da</ts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_306" n="HIAT:w" s="T87">hi͡ebetek</ts>
                  <nts id="Seg_307" n="HIAT:ip">.</nts>
                  <nts id="Seg_308" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T92" id="Seg_310" n="HIAT:u" s="T88">
                  <ts e="T89" id="Seg_312" n="HIAT:w" s="T88">Hogurduk</ts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_315" n="HIAT:w" s="T89">bi͡ek</ts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_318" n="HIAT:w" s="T90">hi͡emne</ts>
                  <nts id="Seg_319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_321" n="HIAT:w" s="T91">olorbut</ts>
                  <nts id="Seg_322" n="HIAT:ip">.</nts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T98" id="Seg_325" n="HIAT:u" s="T92">
                  <ts e="T93" id="Seg_327" n="HIAT:w" s="T92">Bu</ts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_330" n="HIAT:w" s="T93">ogo</ts>
                  <nts id="Seg_331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_333" n="HIAT:w" s="T94">agata</ts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_336" n="HIAT:w" s="T95">nuːčča</ts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_339" n="HIAT:w" s="T96">aptaːga</ts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_342" n="HIAT:w" s="T97">ete</ts>
                  <nts id="Seg_343" n="HIAT:ip">.</nts>
                  <nts id="Seg_344" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T105" id="Seg_346" n="HIAT:u" s="T98">
                  <ts e="T99" id="Seg_348" n="HIAT:w" s="T98">Ol</ts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_351" n="HIAT:w" s="T99">üs</ts>
                  <nts id="Seg_352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_354" n="HIAT:w" s="T100">tögül</ts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_357" n="HIAT:w" s="T101">ojunu</ts>
                  <nts id="Seg_358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_360" n="HIAT:w" s="T102">kɨːrdarbɨt</ts>
                  <nts id="Seg_361" n="HIAT:ip">,</nts>
                  <nts id="Seg_362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_364" n="HIAT:w" s="T103">kömötö</ts>
                  <nts id="Seg_365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_367" n="HIAT:w" s="T104">hu͡ok</ts>
                  <nts id="Seg_368" n="HIAT:ip">.</nts>
                  <nts id="Seg_369" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T108" id="Seg_371" n="HIAT:u" s="T105">
                  <ts e="T106" id="Seg_373" n="HIAT:w" s="T105">Olor</ts>
                  <nts id="Seg_374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_376" n="HIAT:w" s="T106">kepseːbitter</ts>
                  <nts id="Seg_377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_379" n="HIAT:w" s="T107">ere</ts>
                  <nts id="Seg_380" n="HIAT:ip">:</nts>
                  <nts id="Seg_381" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T113" id="Seg_383" n="HIAT:u" s="T108">
                  <nts id="Seg_384" n="HIAT:ip">"</nts>
                  <ts e="T109" id="Seg_386" n="HIAT:w" s="T108">Ogogun</ts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_389" n="HIAT:w" s="T109">hir</ts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_392" n="HIAT:w" s="T110">iččite</ts>
                  <nts id="Seg_393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_395" n="HIAT:w" s="T111">ɨlbɨt</ts>
                  <nts id="Seg_396" n="HIAT:ip">"</nts>
                  <nts id="Seg_397" n="HIAT:ip">,</nts>
                  <nts id="Seg_398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_400" n="HIAT:w" s="T112">di͡en</ts>
                  <nts id="Seg_401" n="HIAT:ip">.</nts>
                  <nts id="Seg_402" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T123" id="Seg_404" n="HIAT:u" s="T113">
                  <ts e="T114" id="Seg_406" n="HIAT:w" s="T113">Ogo</ts>
                  <nts id="Seg_407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_409" n="HIAT:w" s="T114">olordoguna</ts>
                  <nts id="Seg_410" n="HIAT:ip">,</nts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_413" n="HIAT:w" s="T115">üs</ts>
                  <nts id="Seg_414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_416" n="HIAT:w" s="T116">tögül</ts>
                  <nts id="Seg_417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_419" n="HIAT:w" s="T117">ister</ts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_422" n="HIAT:w" s="T118">düŋür</ts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_425" n="HIAT:w" s="T119">tɨ͡ahɨn</ts>
                  <nts id="Seg_426" n="HIAT:ip">,</nts>
                  <nts id="Seg_427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_429" n="HIAT:w" s="T120">ojun</ts>
                  <nts id="Seg_430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_432" n="HIAT:w" s="T121">haŋata</ts>
                  <nts id="Seg_433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_435" n="HIAT:w" s="T122">ihiller</ts>
                  <nts id="Seg_436" n="HIAT:ip">:</nts>
                  <nts id="Seg_437" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T128" id="Seg_439" n="HIAT:u" s="T123">
                  <nts id="Seg_440" n="HIAT:ip">"</nts>
                  <ts e="T124" id="Seg_442" n="HIAT:w" s="T123">Aːnɨ</ts>
                  <nts id="Seg_443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_445" n="HIAT:w" s="T124">ahɨŋ</ts>
                  <nts id="Seg_446" n="HIAT:ip">,</nts>
                  <nts id="Seg_447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_449" n="HIAT:w" s="T125">ogonu</ts>
                  <nts id="Seg_450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_452" n="HIAT:w" s="T126">bi͡eriŋ</ts>
                  <nts id="Seg_453" n="HIAT:ip">"</nts>
                  <nts id="Seg_454" n="HIAT:ip">,</nts>
                  <nts id="Seg_455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_457" n="HIAT:w" s="T127">di͡en</ts>
                  <nts id="Seg_458" n="HIAT:ip">.</nts>
                  <nts id="Seg_459" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T134" id="Seg_461" n="HIAT:u" s="T128">
                  <ts e="T129" id="Seg_463" n="HIAT:w" s="T128">Bi͡ek</ts>
                  <nts id="Seg_464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_466" n="HIAT:w" s="T129">onu</ts>
                  <nts id="Seg_467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_469" n="HIAT:w" s="T130">aspattar</ts>
                  <nts id="Seg_470" n="HIAT:ip">,</nts>
                  <nts id="Seg_471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_473" n="HIAT:w" s="T131">ojuttar</ts>
                  <nts id="Seg_474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_476" n="HIAT:w" s="T132">kötüːn</ts>
                  <nts id="Seg_477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_479" n="HIAT:w" s="T133">barallar</ts>
                  <nts id="Seg_480" n="HIAT:ip">.</nts>
                  <nts id="Seg_481" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T146" id="Seg_483" n="HIAT:u" s="T134">
                  <ts e="T135" id="Seg_485" n="HIAT:w" s="T134">Ogo</ts>
                  <nts id="Seg_486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_488" n="HIAT:w" s="T135">agata</ts>
                  <nts id="Seg_489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_491" n="HIAT:w" s="T136">dʼe</ts>
                  <nts id="Seg_492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_494" n="HIAT:w" s="T137">bejete</ts>
                  <nts id="Seg_495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_497" n="HIAT:w" s="T138">horunar</ts>
                  <nts id="Seg_498" n="HIAT:ip">,</nts>
                  <nts id="Seg_499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_501" n="HIAT:w" s="T139">hir</ts>
                  <nts id="Seg_502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_504" n="HIAT:w" s="T140">iččitin</ts>
                  <nts id="Seg_505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_507" n="HIAT:w" s="T141">dʼi͡etin</ts>
                  <nts id="Seg_508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_510" n="HIAT:w" s="T142">ürdüger</ts>
                  <nts id="Seg_511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_513" n="HIAT:w" s="T143">taksan</ts>
                  <nts id="Seg_514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_516" n="HIAT:w" s="T144">bɨha</ts>
                  <nts id="Seg_517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_519" n="HIAT:w" s="T145">iːktiːr</ts>
                  <nts id="Seg_520" n="HIAT:ip">.</nts>
                  <nts id="Seg_521" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T156" id="Seg_523" n="HIAT:u" s="T146">
                  <ts e="T147" id="Seg_525" n="HIAT:w" s="T146">Onton</ts>
                  <nts id="Seg_526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_528" n="HIAT:w" s="T147">kɨjakanan</ts>
                  <nts id="Seg_529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_531" n="HIAT:w" s="T148">hir</ts>
                  <nts id="Seg_532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_534" n="HIAT:w" s="T149">iččite</ts>
                  <nts id="Seg_535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_537" n="HIAT:w" s="T150">ɨlar</ts>
                  <nts id="Seg_538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_540" n="HIAT:w" s="T151">ogonu</ts>
                  <nts id="Seg_541" n="HIAT:ip">,</nts>
                  <nts id="Seg_542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_544" n="HIAT:w" s="T152">hɨrgatɨgar</ts>
                  <nts id="Seg_545" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_547" n="HIAT:w" s="T153">keːher</ts>
                  <nts id="Seg_548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_550" n="HIAT:w" s="T154">da</ts>
                  <nts id="Seg_551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_553" n="HIAT:w" s="T155">kötüter</ts>
                  <nts id="Seg_554" n="HIAT:ip">.</nts>
                  <nts id="Seg_555" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T164" id="Seg_557" n="HIAT:u" s="T156">
                  <ts e="T157" id="Seg_559" n="HIAT:w" s="T156">Dʼi͡etin</ts>
                  <nts id="Seg_560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_562" n="HIAT:w" s="T157">tahɨgar</ts>
                  <nts id="Seg_563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_565" n="HIAT:w" s="T158">tijen</ts>
                  <nts id="Seg_566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_568" n="HIAT:w" s="T159">baran</ts>
                  <nts id="Seg_569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_571" n="HIAT:w" s="T160">hɨtar</ts>
                  <nts id="Seg_572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_574" n="HIAT:w" s="T161">maska</ts>
                  <nts id="Seg_575" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_577" n="HIAT:w" s="T162">miːnneren</ts>
                  <nts id="Seg_578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_580" n="HIAT:w" s="T163">kebiher</ts>
                  <nts id="Seg_581" n="HIAT:ip">.</nts>
                  <nts id="Seg_582" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T172" id="Seg_584" n="HIAT:u" s="T164">
                  <ts e="T165" id="Seg_586" n="HIAT:w" s="T164">Onu</ts>
                  <nts id="Seg_587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_589" n="HIAT:w" s="T165">dʼi͡etitten</ts>
                  <nts id="Seg_590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_592" n="HIAT:w" s="T166">hüppüte</ts>
                  <nts id="Seg_593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_595" n="HIAT:w" s="T167">üs</ts>
                  <nts id="Seg_596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_598" n="HIAT:w" s="T168">ɨj</ts>
                  <nts id="Seg_599" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_601" n="HIAT:w" s="T169">bu͡oltun</ts>
                  <nts id="Seg_602" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_604" n="HIAT:w" s="T170">genne</ts>
                  <nts id="Seg_605" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_607" n="HIAT:w" s="T171">bulallar</ts>
                  <nts id="Seg_608" n="HIAT:ip">.</nts>
                  <nts id="Seg_609" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T172" id="Seg_610" n="sc" s="T0">
               <ts e="T1" id="Seg_612" n="e" s="T0">Hir </ts>
               <ts e="T2" id="Seg_614" n="e" s="T1">iččileːk </ts>
               <ts e="T3" id="Seg_616" n="e" s="T2">bu͡olar. </ts>
               <ts e="T4" id="Seg_618" n="e" s="T3">Ol </ts>
               <ts e="T5" id="Seg_620" n="e" s="T4">hir </ts>
               <ts e="T6" id="Seg_622" n="e" s="T5">iččititten </ts>
               <ts e="T7" id="Seg_624" n="e" s="T6">kuttanan </ts>
               <ts e="T8" id="Seg_626" n="e" s="T7">ogoloru </ts>
               <ts e="T9" id="Seg_628" n="e" s="T8">ki͡ehe </ts>
               <ts e="T10" id="Seg_630" n="e" s="T9">tahaːra </ts>
               <ts e="T11" id="Seg_632" n="e" s="T10">ɨːppat </ts>
               <ts e="T12" id="Seg_634" n="e" s="T11">etiler. </ts>
               <ts e="T13" id="Seg_636" n="e" s="T12">Ol </ts>
               <ts e="T14" id="Seg_638" n="e" s="T13">hir </ts>
               <ts e="T15" id="Seg_640" n="e" s="T14">iččite </ts>
               <ts e="T16" id="Seg_642" n="e" s="T15">hɨldʼar </ts>
               <ts e="T17" id="Seg_644" n="e" s="T16">hahɨllarɨ </ts>
               <ts e="T18" id="Seg_646" n="e" s="T17">kölünen </ts>
               <ts e="T19" id="Seg_648" n="e" s="T18">baran. </ts>
               <ts e="T20" id="Seg_650" n="e" s="T19">Bɨlɨr </ts>
               <ts e="T21" id="Seg_652" n="e" s="T20">min </ts>
               <ts e="T22" id="Seg_654" n="e" s="T21">ijem </ts>
               <ts e="T23" id="Seg_656" n="e" s="T22">bɨraːtɨn </ts>
               <ts e="T24" id="Seg_658" n="e" s="T23">ilpite </ts>
               <ts e="T25" id="Seg_660" n="e" s="T24">ogurduk </ts>
               <ts e="T26" id="Seg_662" n="e" s="T25">hir </ts>
               <ts e="T27" id="Seg_664" n="e" s="T26">iččite. </ts>
               <ts e="T28" id="Seg_666" n="e" s="T27">Oburgu </ts>
               <ts e="T29" id="Seg_668" n="e" s="T28">ogonu, </ts>
               <ts e="T30" id="Seg_670" n="e" s="T29">tahaːra </ts>
               <ts e="T31" id="Seg_672" n="e" s="T30">hɨrɨttagɨna, </ts>
               <ts e="T32" id="Seg_674" n="e" s="T31">biːr </ts>
               <ts e="T33" id="Seg_676" n="e" s="T32">ogonnʼor </ts>
               <ts e="T34" id="Seg_678" n="e" s="T33">kelbit </ts>
               <ts e="T35" id="Seg_680" n="e" s="T34">hahɨlɨ </ts>
               <ts e="T36" id="Seg_682" n="e" s="T35">kölünen, </ts>
               <ts e="T37" id="Seg_684" n="e" s="T36">ogonu </ts>
               <ts e="T38" id="Seg_686" n="e" s="T37">ɨlbɨt, </ts>
               <ts e="T39" id="Seg_688" n="e" s="T38">hɨrgatɨgar </ts>
               <ts e="T40" id="Seg_690" n="e" s="T39">bɨrakpɨt </ts>
               <ts e="T41" id="Seg_692" n="e" s="T40">da </ts>
               <ts e="T42" id="Seg_694" n="e" s="T41">kötüten </ts>
               <ts e="T43" id="Seg_696" n="e" s="T42">ispit. </ts>
               <ts e="T44" id="Seg_698" n="e" s="T43">Dʼi͡etiger </ts>
               <ts e="T45" id="Seg_700" n="e" s="T44">bɨrakpɨt </ts>
               <ts e="T46" id="Seg_702" n="e" s="T45">daː </ts>
               <ts e="T47" id="Seg_704" n="e" s="T46">töttöru </ts>
               <ts e="T48" id="Seg_706" n="e" s="T47">taksɨbɨt. </ts>
               <ts e="T49" id="Seg_708" n="e" s="T48">Manɨga </ts>
               <ts e="T50" id="Seg_710" n="e" s="T49">bu </ts>
               <ts e="T51" id="Seg_712" n="e" s="T50">ogogo </ts>
               <ts e="T52" id="Seg_714" n="e" s="T51">biːr </ts>
               <ts e="T53" id="Seg_716" n="e" s="T52">emeːksin </ts>
               <ts e="T54" id="Seg_718" n="e" s="T53">kelbit. </ts>
               <ts e="T55" id="Seg_720" n="e" s="T54">"Dʼe </ts>
               <ts e="T56" id="Seg_722" n="e" s="T55">kugu", </ts>
               <ts e="T57" id="Seg_724" n="e" s="T56">di͡ebit, </ts>
               <ts e="T58" id="Seg_726" n="e" s="T57">"eheŋ </ts>
               <ts e="T59" id="Seg_728" n="e" s="T58">tagɨsta </ts>
               <ts e="T60" id="Seg_730" n="e" s="T59">ɨttarɨn </ts>
               <ts e="T61" id="Seg_732" n="e" s="T60">bulguta, </ts>
               <ts e="T62" id="Seg_734" n="e" s="T61">kelen </ts>
               <ts e="T63" id="Seg_736" n="e" s="T62">ahattagɨna, </ts>
               <ts e="T64" id="Seg_738" n="e" s="T63">mantan </ts>
               <ts e="T65" id="Seg_740" n="e" s="T64">dojdugar </ts>
               <ts e="T66" id="Seg_742" n="e" s="T65">ere </ts>
               <ts e="T67" id="Seg_744" n="e" s="T66">bararga </ts>
               <ts e="T68" id="Seg_746" n="e" s="T67">bagalaːk </ts>
               <ts e="T69" id="Seg_748" n="e" s="T68">bu͡ollakkɨna, </ts>
               <ts e="T70" id="Seg_750" n="e" s="T69">bi͡erer </ts>
               <ts e="T71" id="Seg_752" n="e" s="T70">ahɨn </ts>
               <ts e="T72" id="Seg_754" n="e" s="T71">hi͡ejegin. </ts>
               <ts e="T73" id="Seg_756" n="e" s="T72">Manna </ts>
               <ts e="T74" id="Seg_758" n="e" s="T73">kaːlan </ts>
               <ts e="T75" id="Seg_760" n="e" s="T74">kergen </ts>
               <ts e="T76" id="Seg_762" n="e" s="T75">bu͡olu͡ok </ts>
               <ts e="T77" id="Seg_764" n="e" s="T76">bagalaːk </ts>
               <ts e="T78" id="Seg_766" n="e" s="T77">bu͡ollakkɨna </ts>
               <ts e="T79" id="Seg_768" n="e" s="T78">oččogo </ts>
               <ts e="T80" id="Seg_770" n="e" s="T79">ere </ts>
               <ts e="T81" id="Seg_772" n="e" s="T80">ahaːr." </ts>
               <ts e="T82" id="Seg_774" n="e" s="T81">Ehete </ts>
               <ts e="T83" id="Seg_776" n="e" s="T82">kiːren </ts>
               <ts e="T84" id="Seg_778" n="e" s="T83">kündülüː </ts>
               <ts e="T85" id="Seg_780" n="e" s="T84">hataːbɨt, </ts>
               <ts e="T86" id="Seg_782" n="e" s="T85">tugu </ts>
               <ts e="T87" id="Seg_784" n="e" s="T86">da </ts>
               <ts e="T88" id="Seg_786" n="e" s="T87">hi͡ebetek. </ts>
               <ts e="T89" id="Seg_788" n="e" s="T88">Hogurduk </ts>
               <ts e="T90" id="Seg_790" n="e" s="T89">bi͡ek </ts>
               <ts e="T91" id="Seg_792" n="e" s="T90">hi͡emne </ts>
               <ts e="T92" id="Seg_794" n="e" s="T91">olorbut. </ts>
               <ts e="T93" id="Seg_796" n="e" s="T92">Bu </ts>
               <ts e="T94" id="Seg_798" n="e" s="T93">ogo </ts>
               <ts e="T95" id="Seg_800" n="e" s="T94">agata </ts>
               <ts e="T96" id="Seg_802" n="e" s="T95">nuːčča </ts>
               <ts e="T97" id="Seg_804" n="e" s="T96">aptaːga </ts>
               <ts e="T98" id="Seg_806" n="e" s="T97">ete. </ts>
               <ts e="T99" id="Seg_808" n="e" s="T98">Ol </ts>
               <ts e="T100" id="Seg_810" n="e" s="T99">üs </ts>
               <ts e="T101" id="Seg_812" n="e" s="T100">tögül </ts>
               <ts e="T102" id="Seg_814" n="e" s="T101">ojunu </ts>
               <ts e="T103" id="Seg_816" n="e" s="T102">kɨːrdarbɨt, </ts>
               <ts e="T104" id="Seg_818" n="e" s="T103">kömötö </ts>
               <ts e="T105" id="Seg_820" n="e" s="T104">hu͡ok. </ts>
               <ts e="T106" id="Seg_822" n="e" s="T105">Olor </ts>
               <ts e="T107" id="Seg_824" n="e" s="T106">kepseːbitter </ts>
               <ts e="T108" id="Seg_826" n="e" s="T107">ere: </ts>
               <ts e="T109" id="Seg_828" n="e" s="T108">"Ogogun </ts>
               <ts e="T110" id="Seg_830" n="e" s="T109">hir </ts>
               <ts e="T111" id="Seg_832" n="e" s="T110">iččite </ts>
               <ts e="T112" id="Seg_834" n="e" s="T111">ɨlbɨt", </ts>
               <ts e="T113" id="Seg_836" n="e" s="T112">di͡en. </ts>
               <ts e="T114" id="Seg_838" n="e" s="T113">Ogo </ts>
               <ts e="T115" id="Seg_840" n="e" s="T114">olordoguna, </ts>
               <ts e="T116" id="Seg_842" n="e" s="T115">üs </ts>
               <ts e="T117" id="Seg_844" n="e" s="T116">tögül </ts>
               <ts e="T118" id="Seg_846" n="e" s="T117">ister </ts>
               <ts e="T119" id="Seg_848" n="e" s="T118">düŋür </ts>
               <ts e="T120" id="Seg_850" n="e" s="T119">tɨ͡ahɨn, </ts>
               <ts e="T121" id="Seg_852" n="e" s="T120">ojun </ts>
               <ts e="T122" id="Seg_854" n="e" s="T121">haŋata </ts>
               <ts e="T123" id="Seg_856" n="e" s="T122">ihiller: </ts>
               <ts e="T124" id="Seg_858" n="e" s="T123">"Aːnɨ </ts>
               <ts e="T125" id="Seg_860" n="e" s="T124">ahɨŋ, </ts>
               <ts e="T126" id="Seg_862" n="e" s="T125">ogonu </ts>
               <ts e="T127" id="Seg_864" n="e" s="T126">bi͡eriŋ", </ts>
               <ts e="T128" id="Seg_866" n="e" s="T127">di͡en. </ts>
               <ts e="T129" id="Seg_868" n="e" s="T128">Bi͡ek </ts>
               <ts e="T130" id="Seg_870" n="e" s="T129">onu </ts>
               <ts e="T131" id="Seg_872" n="e" s="T130">aspattar, </ts>
               <ts e="T132" id="Seg_874" n="e" s="T131">ojuttar </ts>
               <ts e="T133" id="Seg_876" n="e" s="T132">kötüːn </ts>
               <ts e="T134" id="Seg_878" n="e" s="T133">barallar. </ts>
               <ts e="T135" id="Seg_880" n="e" s="T134">Ogo </ts>
               <ts e="T136" id="Seg_882" n="e" s="T135">agata </ts>
               <ts e="T137" id="Seg_884" n="e" s="T136">dʼe </ts>
               <ts e="T138" id="Seg_886" n="e" s="T137">bejete </ts>
               <ts e="T139" id="Seg_888" n="e" s="T138">horunar, </ts>
               <ts e="T140" id="Seg_890" n="e" s="T139">hir </ts>
               <ts e="T141" id="Seg_892" n="e" s="T140">iččitin </ts>
               <ts e="T142" id="Seg_894" n="e" s="T141">dʼi͡etin </ts>
               <ts e="T143" id="Seg_896" n="e" s="T142">ürdüger </ts>
               <ts e="T144" id="Seg_898" n="e" s="T143">taksan </ts>
               <ts e="T145" id="Seg_900" n="e" s="T144">bɨha </ts>
               <ts e="T146" id="Seg_902" n="e" s="T145">iːktiːr. </ts>
               <ts e="T147" id="Seg_904" n="e" s="T146">Onton </ts>
               <ts e="T148" id="Seg_906" n="e" s="T147">kɨjakanan </ts>
               <ts e="T149" id="Seg_908" n="e" s="T148">hir </ts>
               <ts e="T150" id="Seg_910" n="e" s="T149">iččite </ts>
               <ts e="T151" id="Seg_912" n="e" s="T150">ɨlar </ts>
               <ts e="T152" id="Seg_914" n="e" s="T151">ogonu, </ts>
               <ts e="T153" id="Seg_916" n="e" s="T152">hɨrgatɨgar </ts>
               <ts e="T154" id="Seg_918" n="e" s="T153">keːher </ts>
               <ts e="T155" id="Seg_920" n="e" s="T154">da </ts>
               <ts e="T156" id="Seg_922" n="e" s="T155">kötüter. </ts>
               <ts e="T157" id="Seg_924" n="e" s="T156">Dʼi͡etin </ts>
               <ts e="T158" id="Seg_926" n="e" s="T157">tahɨgar </ts>
               <ts e="T159" id="Seg_928" n="e" s="T158">tijen </ts>
               <ts e="T160" id="Seg_930" n="e" s="T159">baran </ts>
               <ts e="T161" id="Seg_932" n="e" s="T160">hɨtar </ts>
               <ts e="T162" id="Seg_934" n="e" s="T161">maska </ts>
               <ts e="T163" id="Seg_936" n="e" s="T162">miːnneren </ts>
               <ts e="T164" id="Seg_938" n="e" s="T163">kebiher. </ts>
               <ts e="T165" id="Seg_940" n="e" s="T164">Onu </ts>
               <ts e="T166" id="Seg_942" n="e" s="T165">dʼi͡etitten </ts>
               <ts e="T167" id="Seg_944" n="e" s="T166">hüppüte </ts>
               <ts e="T168" id="Seg_946" n="e" s="T167">üs </ts>
               <ts e="T169" id="Seg_948" n="e" s="T168">ɨj </ts>
               <ts e="T170" id="Seg_950" n="e" s="T169">bu͡oltun </ts>
               <ts e="T171" id="Seg_952" n="e" s="T170">genne </ts>
               <ts e="T172" id="Seg_954" n="e" s="T171">bulallar. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T3" id="Seg_955" s="T0">BaRD_1930_MasterOfWorld_flk.001 (001.001)</ta>
            <ta e="T12" id="Seg_956" s="T3">BaRD_1930_MasterOfWorld_flk.002 (001.002)</ta>
            <ta e="T19" id="Seg_957" s="T12">BaRD_1930_MasterOfWorld_flk.003 (001.003)</ta>
            <ta e="T27" id="Seg_958" s="T19">BaRD_1930_MasterOfWorld_flk.004 (001.004)</ta>
            <ta e="T43" id="Seg_959" s="T27">BaRD_1930_MasterOfWorld_flk.005 (001.005)</ta>
            <ta e="T48" id="Seg_960" s="T43">BaRD_1930_MasterOfWorld_flk.006 (001.006)</ta>
            <ta e="T54" id="Seg_961" s="T48">BaRD_1930_MasterOfWorld_flk.007 (001.007)</ta>
            <ta e="T72" id="Seg_962" s="T54">BaRD_1930_MasterOfWorld_flk.008 (001.008)</ta>
            <ta e="T81" id="Seg_963" s="T72">BaRD_1930_MasterOfWorld_flk.009 (001.009)</ta>
            <ta e="T88" id="Seg_964" s="T81">BaRD_1930_MasterOfWorld_flk.010 (001.010)</ta>
            <ta e="T92" id="Seg_965" s="T88">BaRD_1930_MasterOfWorld_flk.011 (001.011)</ta>
            <ta e="T98" id="Seg_966" s="T92">BaRD_1930_MasterOfWorld_flk.012 (001.012)</ta>
            <ta e="T105" id="Seg_967" s="T98">BaRD_1930_MasterOfWorld_flk.013 (001.013)</ta>
            <ta e="T108" id="Seg_968" s="T105">BaRD_1930_MasterOfWorld_flk.014 (001.014)</ta>
            <ta e="T113" id="Seg_969" s="T108">BaRD_1930_MasterOfWorld_flk.015 (001.015)</ta>
            <ta e="T123" id="Seg_970" s="T113">BaRD_1930_MasterOfWorld_flk.016 (001.016)</ta>
            <ta e="T128" id="Seg_971" s="T123">BaRD_1930_MasterOfWorld_flk.017 (001.017)</ta>
            <ta e="T134" id="Seg_972" s="T128">BaRD_1930_MasterOfWorld_flk.018 (001.018)</ta>
            <ta e="T146" id="Seg_973" s="T134">BaRD_1930_MasterOfWorld_flk.019 (001.019)</ta>
            <ta e="T156" id="Seg_974" s="T146">BaRD_1930_MasterOfWorld_flk.020 (001.020)</ta>
            <ta e="T164" id="Seg_975" s="T156">BaRD_1930_MasterOfWorld_flk.021 (001.021)</ta>
            <ta e="T172" id="Seg_976" s="T164">BaRD_1930_MasterOfWorld_flk.022 (001.022)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T3" id="Seg_977" s="T0">һир иччилээк буолар.</ta>
            <ta e="T12" id="Seg_978" s="T3">Ол һир иччититтэн куттанан оголору киэһэ таһаара ыыппат этилэр.</ta>
            <ta e="T19" id="Seg_979" s="T12">Ол һир иччитэ һылдьар һаһыллары көлүнэн баран.</ta>
            <ta e="T27" id="Seg_980" s="T19">Былыр мин ийэм быраатын илпитэ огурдук һир иччитэ.</ta>
            <ta e="T43" id="Seg_981" s="T27">Обургу огону, таһаара һырыттагына, биир огонньор кэлбит һаһылы көлүнэн, огону ылбыт, һыргатыгар быракпыт да көтүтэн испит.</ta>
            <ta e="T48" id="Seg_982" s="T43">Дьиэтигэр быракпыт даа төттөру таксыбыт.</ta>
            <ta e="T54" id="Seg_983" s="T48">Маныга бу огого биир эмээксин келбит.</ta>
            <ta e="T72" id="Seg_984" s="T54">— Дьэ кугу[у] *, — диэбит, — эһэҥ тагыста ыттарын булгута, кэлэн аһаттагына, мантан дойдугар эрэ барарга багалаак буоллаккына, биэрэр аһын һиэйэгин.</ta>
            <ta e="T81" id="Seg_985" s="T72">Манна каалан кэргэн буолуок багалаак буоллаккына оччого эрэ аһаар.</ta>
            <ta e="T88" id="Seg_986" s="T81">Эһэтэ киирэн күндүлүү һатаабыт, тугу да һиэбэтэк.</ta>
            <ta e="T92" id="Seg_987" s="T88">һогурдук биэк һиэмнэ олорбут.</ta>
            <ta e="T98" id="Seg_988" s="T92">Бу ого агата нуучча аптаага этэ.</ta>
            <ta e="T105" id="Seg_989" s="T98">Ол үс төгүл ойуну кыырдарбыт, көмөтө һуок.</ta>
            <ta e="T108" id="Seg_990" s="T105">Олор кэпсээбиттэр эрэ:</ta>
            <ta e="T113" id="Seg_991" s="T108">— Огогун һир иччитэ ылбыт, — диэн.</ta>
            <ta e="T123" id="Seg_992" s="T113">Ого олордогуна, үс төгүл истэр дүҥүр тыаһын, ойун һаҥата иһиллэр:</ta>
            <ta e="T128" id="Seg_993" s="T123">— Ааны аһыҥ, огону биэриҥ, — диэн.</ta>
            <ta e="T134" id="Seg_994" s="T128">Биэк ону аспаттар, ойуттар көтүүн бараллар.</ta>
            <ta e="T146" id="Seg_995" s="T134">Ого агата дьэ бэйэтэ һорунар, һир иччитин дьиэтин үрдүгэр таксан быһа ииктиир.</ta>
            <ta e="T156" id="Seg_996" s="T146">Онтон кыйаканан һир иччитэ ылар огону, һыргатыгар кээһэр да көтүтэр.</ta>
            <ta e="T164" id="Seg_997" s="T156">Дьиэтин таһыгар тийэн баран һытар маска мииннэрэн кэбиһэр.</ta>
            <ta e="T172" id="Seg_998" s="T164">Ону дьиэтиттэн һүппүтэ үс ый буолтун гэннэ булаллар.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T3" id="Seg_999" s="T0">Hir iččileːk bu͡olar. </ta>
            <ta e="T12" id="Seg_1000" s="T3">Ol hir iččititten kuttanan ogoloru ki͡ehe tahaːra ɨːppat etiler. </ta>
            <ta e="T19" id="Seg_1001" s="T12">Ol hir iččite hɨldʼar hahɨllarɨ kölünen baran. </ta>
            <ta e="T27" id="Seg_1002" s="T19">Bɨlɨr min ijem bɨraːtɨn ilpite ogurduk hir iččite. </ta>
            <ta e="T43" id="Seg_1003" s="T27">Oburgu ogonu, tahaːra hɨrɨttagɨna, biːr ogonnʼor kelbit hahɨlɨ kölünen, ogonu ɨlbɨt, hɨrgatɨgar bɨrakpɨt da kötüten ispit. </ta>
            <ta e="T48" id="Seg_1004" s="T43">Dʼi͡etiger bɨrakpɨt daː töttöru taksɨbɨt. </ta>
            <ta e="T54" id="Seg_1005" s="T48">Manɨga bu ogogo biːr emeːksin kelbit. </ta>
            <ta e="T72" id="Seg_1006" s="T54">"Dʼe kugu", di͡ebit, "eheŋ tagɨsta ɨttarɨn bulguta, kelen ahattagɨna, mantan dojdugar ere bararga bagalaːk bu͡ollakkɨna, bi͡erer ahɨn hi͡ejegin. </ta>
            <ta e="T81" id="Seg_1007" s="T72">Manna kaːlan kergen bu͡olu͡ok bagalaːk bu͡ollakkɨna oččogo ere ahaːr." </ta>
            <ta e="T88" id="Seg_1008" s="T81">Ehete kiːren kündülüː hataːbɨt, tugu da hi͡ebetek. </ta>
            <ta e="T92" id="Seg_1009" s="T88">Hogurduk bi͡ek hi͡emne olorbut. </ta>
            <ta e="T98" id="Seg_1010" s="T92">Bu ogo agata nuːčča aptaːga ete. </ta>
            <ta e="T105" id="Seg_1011" s="T98">Ol üs tögül ojunu kɨːrdarbɨt, kömötö hu͡ok. </ta>
            <ta e="T108" id="Seg_1012" s="T105">Olor kepseːbitter ere: </ta>
            <ta e="T113" id="Seg_1013" s="T108">"Ogogun hir iččite ɨlbɨt", di͡en. </ta>
            <ta e="T123" id="Seg_1014" s="T113">Ogo olordoguna, üs tögül ister düŋür tɨ͡ahɨn, ojun haŋata ihiller: </ta>
            <ta e="T128" id="Seg_1015" s="T123">"Aːnɨ ahɨŋ, ogonu bi͡eriŋ", di͡en. </ta>
            <ta e="T134" id="Seg_1016" s="T128">Bi͡ek onu aspattar, ojuttar kötüːn barallar. </ta>
            <ta e="T146" id="Seg_1017" s="T134">Ogo agata dʼe bejete horunar, hir iččitin dʼi͡etin ürdüger taksan bɨha iːktiːr. </ta>
            <ta e="T156" id="Seg_1018" s="T146">Onton kɨjakanan hir iččite ɨlar ogonu, hɨrgatɨgar keːher da kötüter. </ta>
            <ta e="T164" id="Seg_1019" s="T156">Dʼi͡etin tahɨgar tijen baran hɨtar maska miːnneren kebiher. </ta>
            <ta e="T172" id="Seg_1020" s="T164">Onu dʼi͡etitten hüppüte üs ɨj bu͡oltun genne bulallar. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_1021" s="T0">hir</ta>
            <ta e="T2" id="Seg_1022" s="T1">ičči-leːk</ta>
            <ta e="T3" id="Seg_1023" s="T2">bu͡ol-ar</ta>
            <ta e="T4" id="Seg_1024" s="T3">ol</ta>
            <ta e="T5" id="Seg_1025" s="T4">hir</ta>
            <ta e="T6" id="Seg_1026" s="T5">ičči-ti-tten</ta>
            <ta e="T7" id="Seg_1027" s="T6">kuttan-an</ta>
            <ta e="T8" id="Seg_1028" s="T7">ogo-lor-u</ta>
            <ta e="T9" id="Seg_1029" s="T8">ki͡ehe</ta>
            <ta e="T10" id="Seg_1030" s="T9">tahaːra</ta>
            <ta e="T11" id="Seg_1031" s="T10">ɨːp-pat</ta>
            <ta e="T12" id="Seg_1032" s="T11">e-ti-ler</ta>
            <ta e="T13" id="Seg_1033" s="T12">ol</ta>
            <ta e="T14" id="Seg_1034" s="T13">hir</ta>
            <ta e="T15" id="Seg_1035" s="T14">ičči-te</ta>
            <ta e="T16" id="Seg_1036" s="T15">hɨldʼ-ar</ta>
            <ta e="T17" id="Seg_1037" s="T16">hahɨl-lar-ɨ</ta>
            <ta e="T18" id="Seg_1038" s="T17">kölün-en</ta>
            <ta e="T19" id="Seg_1039" s="T18">baran</ta>
            <ta e="T20" id="Seg_1040" s="T19">bɨlɨr</ta>
            <ta e="T21" id="Seg_1041" s="T20">min</ta>
            <ta e="T22" id="Seg_1042" s="T21">ije-m</ta>
            <ta e="T23" id="Seg_1043" s="T22">bɨraːt-ɨ-n</ta>
            <ta e="T24" id="Seg_1044" s="T23">il-pit-e</ta>
            <ta e="T25" id="Seg_1045" s="T24">ogurduk</ta>
            <ta e="T26" id="Seg_1046" s="T25">hir</ta>
            <ta e="T27" id="Seg_1047" s="T26">ičči-te</ta>
            <ta e="T28" id="Seg_1048" s="T27">oburgu</ta>
            <ta e="T29" id="Seg_1049" s="T28">ogo-nu</ta>
            <ta e="T30" id="Seg_1050" s="T29">tahaːra</ta>
            <ta e="T31" id="Seg_1051" s="T30">hɨrɨt-tag-ɨna</ta>
            <ta e="T32" id="Seg_1052" s="T31">biːr</ta>
            <ta e="T33" id="Seg_1053" s="T32">ogonnʼor</ta>
            <ta e="T34" id="Seg_1054" s="T33">kel-bit</ta>
            <ta e="T35" id="Seg_1055" s="T34">hahɨl-ɨ</ta>
            <ta e="T36" id="Seg_1056" s="T35">kölün-en</ta>
            <ta e="T37" id="Seg_1057" s="T36">ogo-nu</ta>
            <ta e="T38" id="Seg_1058" s="T37">ɨl-bɨt</ta>
            <ta e="T39" id="Seg_1059" s="T38">hɨrga-tɨ-gar</ta>
            <ta e="T40" id="Seg_1060" s="T39">bɨrak-pɨt</ta>
            <ta e="T41" id="Seg_1061" s="T40">da</ta>
            <ta e="T42" id="Seg_1062" s="T41">kötüt-en</ta>
            <ta e="T43" id="Seg_1063" s="T42">is-pit</ta>
            <ta e="T44" id="Seg_1064" s="T43">dʼi͡e-ti-ger</ta>
            <ta e="T45" id="Seg_1065" s="T44">bɨrak-pɨt</ta>
            <ta e="T46" id="Seg_1066" s="T45">daː</ta>
            <ta e="T47" id="Seg_1067" s="T46">töttöru</ta>
            <ta e="T48" id="Seg_1068" s="T47">taks-ɨ-bɨt</ta>
            <ta e="T49" id="Seg_1069" s="T48">manɨ-ga</ta>
            <ta e="T50" id="Seg_1070" s="T49">bu</ta>
            <ta e="T51" id="Seg_1071" s="T50">ogo-go</ta>
            <ta e="T52" id="Seg_1072" s="T51">biːr</ta>
            <ta e="T53" id="Seg_1073" s="T52">emeːksin</ta>
            <ta e="T54" id="Seg_1074" s="T53">kel-bit</ta>
            <ta e="T55" id="Seg_1075" s="T54">dʼe</ta>
            <ta e="T56" id="Seg_1076" s="T55">kugu</ta>
            <ta e="T57" id="Seg_1077" s="T56">di͡e-bit</ta>
            <ta e="T58" id="Seg_1078" s="T57">ehe-ŋ</ta>
            <ta e="T59" id="Seg_1079" s="T58">tagɨs-t-a</ta>
            <ta e="T60" id="Seg_1080" s="T59">ɨt-tar-ɨ-n</ta>
            <ta e="T61" id="Seg_1081" s="T60">bulgut-a</ta>
            <ta e="T62" id="Seg_1082" s="T61">kel-en</ta>
            <ta e="T63" id="Seg_1083" s="T62">ah-a-t-tag-ɨna</ta>
            <ta e="T64" id="Seg_1084" s="T63">mantan</ta>
            <ta e="T65" id="Seg_1085" s="T64">dojdu-ga-r</ta>
            <ta e="T66" id="Seg_1086" s="T65">ere</ta>
            <ta e="T67" id="Seg_1087" s="T66">bar-ar-ga</ta>
            <ta e="T68" id="Seg_1088" s="T67">baga-laːk</ta>
            <ta e="T69" id="Seg_1089" s="T68">bu͡ol-lak-kɨna</ta>
            <ta e="T70" id="Seg_1090" s="T69">bi͡er-er</ta>
            <ta e="T71" id="Seg_1091" s="T70">ah-ɨ-n</ta>
            <ta e="T72" id="Seg_1092" s="T71">hi͡e-je-gin</ta>
            <ta e="T73" id="Seg_1093" s="T72">manna</ta>
            <ta e="T74" id="Seg_1094" s="T73">kaːl-an</ta>
            <ta e="T75" id="Seg_1095" s="T74">kergen</ta>
            <ta e="T76" id="Seg_1096" s="T75">bu͡ol-u͡ok</ta>
            <ta e="T77" id="Seg_1097" s="T76">baga-laːk</ta>
            <ta e="T78" id="Seg_1098" s="T77">bu͡ol-lak-kɨna</ta>
            <ta e="T79" id="Seg_1099" s="T78">oččogo</ta>
            <ta e="T80" id="Seg_1100" s="T79">ere</ta>
            <ta e="T81" id="Seg_1101" s="T80">ah-aːr</ta>
            <ta e="T82" id="Seg_1102" s="T81">ehe-te</ta>
            <ta e="T83" id="Seg_1103" s="T82">kiːr-en</ta>
            <ta e="T84" id="Seg_1104" s="T83">kündül-üː</ta>
            <ta e="T85" id="Seg_1105" s="T84">hataː-bɨt</ta>
            <ta e="T86" id="Seg_1106" s="T85">tug-u</ta>
            <ta e="T87" id="Seg_1107" s="T86">da</ta>
            <ta e="T88" id="Seg_1108" s="T87">hi͡e-betek</ta>
            <ta e="T89" id="Seg_1109" s="T88">hogurduk</ta>
            <ta e="T90" id="Seg_1110" s="T89">bi͡ek</ta>
            <ta e="T91" id="Seg_1111" s="T90">hi͡e-mne</ta>
            <ta e="T92" id="Seg_1112" s="T91">olor-but</ta>
            <ta e="T93" id="Seg_1113" s="T92">bu</ta>
            <ta e="T94" id="Seg_1114" s="T93">ogo</ta>
            <ta e="T95" id="Seg_1115" s="T94">aga-ta</ta>
            <ta e="T96" id="Seg_1116" s="T95">nuːčča</ta>
            <ta e="T97" id="Seg_1117" s="T96">aptaːg-a</ta>
            <ta e="T98" id="Seg_1118" s="T97">e-t-e</ta>
            <ta e="T99" id="Seg_1119" s="T98">ol</ta>
            <ta e="T100" id="Seg_1120" s="T99">üs</ta>
            <ta e="T101" id="Seg_1121" s="T100">tögül</ta>
            <ta e="T102" id="Seg_1122" s="T101">ojun-u</ta>
            <ta e="T103" id="Seg_1123" s="T102">kɨːr-dar-bɨt</ta>
            <ta e="T104" id="Seg_1124" s="T103">kömö-tö</ta>
            <ta e="T105" id="Seg_1125" s="T104">hu͡ok</ta>
            <ta e="T106" id="Seg_1126" s="T105">o-lor</ta>
            <ta e="T107" id="Seg_1127" s="T106">kepseː-bit-ter</ta>
            <ta e="T108" id="Seg_1128" s="T107">ere</ta>
            <ta e="T109" id="Seg_1129" s="T108">ogo-gu-n</ta>
            <ta e="T110" id="Seg_1130" s="T109">hir</ta>
            <ta e="T111" id="Seg_1131" s="T110">ičči-te</ta>
            <ta e="T112" id="Seg_1132" s="T111">ɨl-bɨt</ta>
            <ta e="T113" id="Seg_1133" s="T112">di͡e-n</ta>
            <ta e="T114" id="Seg_1134" s="T113">ogo</ta>
            <ta e="T115" id="Seg_1135" s="T114">olor-dog-una</ta>
            <ta e="T116" id="Seg_1136" s="T115">üs</ta>
            <ta e="T117" id="Seg_1137" s="T116">tögül</ta>
            <ta e="T118" id="Seg_1138" s="T117">ist-er</ta>
            <ta e="T119" id="Seg_1139" s="T118">düŋür</ta>
            <ta e="T120" id="Seg_1140" s="T119">tɨ͡ah-ɨ-n</ta>
            <ta e="T121" id="Seg_1141" s="T120">ojun</ta>
            <ta e="T122" id="Seg_1142" s="T121">haŋa-ta</ta>
            <ta e="T123" id="Seg_1143" s="T122">ihill-er</ta>
            <ta e="T124" id="Seg_1144" s="T123">aːn-ɨ</ta>
            <ta e="T125" id="Seg_1145" s="T124">ah-ɨ-ŋ</ta>
            <ta e="T126" id="Seg_1146" s="T125">ogo-nu</ta>
            <ta e="T127" id="Seg_1147" s="T126">bi͡er-i-ŋ</ta>
            <ta e="T128" id="Seg_1148" s="T127">di͡e-n</ta>
            <ta e="T129" id="Seg_1149" s="T128">bi͡ek</ta>
            <ta e="T130" id="Seg_1150" s="T129">o-nu</ta>
            <ta e="T131" id="Seg_1151" s="T130">as-pat-tar</ta>
            <ta e="T132" id="Seg_1152" s="T131">ojut-tar</ta>
            <ta e="T133" id="Seg_1153" s="T132">kötüːn</ta>
            <ta e="T134" id="Seg_1154" s="T133">bar-al-lar</ta>
            <ta e="T135" id="Seg_1155" s="T134">ogo</ta>
            <ta e="T136" id="Seg_1156" s="T135">aga-ta</ta>
            <ta e="T137" id="Seg_1157" s="T136">dʼe</ta>
            <ta e="T138" id="Seg_1158" s="T137">beje-te</ta>
            <ta e="T139" id="Seg_1159" s="T138">horun-ar</ta>
            <ta e="T140" id="Seg_1160" s="T139">hir</ta>
            <ta e="T141" id="Seg_1161" s="T140">ičči-ti-n</ta>
            <ta e="T142" id="Seg_1162" s="T141">dʼi͡e-ti-n</ta>
            <ta e="T143" id="Seg_1163" s="T142">ürd-ü-ger</ta>
            <ta e="T144" id="Seg_1164" s="T143">taks-an</ta>
            <ta e="T145" id="Seg_1165" s="T144">bɨha</ta>
            <ta e="T146" id="Seg_1166" s="T145">iːktiː-r</ta>
            <ta e="T147" id="Seg_1167" s="T146">onton</ta>
            <ta e="T148" id="Seg_1168" s="T147">kɨjakan-an</ta>
            <ta e="T149" id="Seg_1169" s="T148">hir</ta>
            <ta e="T150" id="Seg_1170" s="T149">ičči-te</ta>
            <ta e="T151" id="Seg_1171" s="T150">ɨl-ar</ta>
            <ta e="T152" id="Seg_1172" s="T151">ogo-nu</ta>
            <ta e="T153" id="Seg_1173" s="T152">hɨrga-tɨ-gar</ta>
            <ta e="T154" id="Seg_1174" s="T153">keːh-er</ta>
            <ta e="T155" id="Seg_1175" s="T154">da</ta>
            <ta e="T156" id="Seg_1176" s="T155">kötüt-er</ta>
            <ta e="T157" id="Seg_1177" s="T156">dʼi͡e-ti-n</ta>
            <ta e="T158" id="Seg_1178" s="T157">tah-ɨ-gar</ta>
            <ta e="T159" id="Seg_1179" s="T158">tij-en</ta>
            <ta e="T160" id="Seg_1180" s="T159">baran</ta>
            <ta e="T161" id="Seg_1181" s="T160">hɨt-ar</ta>
            <ta e="T162" id="Seg_1182" s="T161">mas-ka</ta>
            <ta e="T163" id="Seg_1183" s="T162">miːn-ner-en</ta>
            <ta e="T164" id="Seg_1184" s="T163">kebih-er</ta>
            <ta e="T165" id="Seg_1185" s="T164">o-nu</ta>
            <ta e="T166" id="Seg_1186" s="T165">dʼi͡e-ti-tten</ta>
            <ta e="T167" id="Seg_1187" s="T166">hüp-püt-e</ta>
            <ta e="T168" id="Seg_1188" s="T167">üs</ta>
            <ta e="T169" id="Seg_1189" s="T168">ɨj</ta>
            <ta e="T170" id="Seg_1190" s="T169">bu͡ol-t-u-n</ta>
            <ta e="T171" id="Seg_1191" s="T170">genne</ta>
            <ta e="T172" id="Seg_1192" s="T171">bul-al-lar</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_1193" s="T0">hir</ta>
            <ta e="T2" id="Seg_1194" s="T1">ičči-LAːK</ta>
            <ta e="T3" id="Seg_1195" s="T2">bu͡ol-Ar</ta>
            <ta e="T4" id="Seg_1196" s="T3">ol</ta>
            <ta e="T5" id="Seg_1197" s="T4">hir</ta>
            <ta e="T6" id="Seg_1198" s="T5">ičči-tI-ttAn</ta>
            <ta e="T7" id="Seg_1199" s="T6">kuttan-An</ta>
            <ta e="T8" id="Seg_1200" s="T7">ogo-LAr-nI</ta>
            <ta e="T9" id="Seg_1201" s="T8">ki͡ehe</ta>
            <ta e="T10" id="Seg_1202" s="T9">tahaːra</ta>
            <ta e="T11" id="Seg_1203" s="T10">ɨːt-BAT</ta>
            <ta e="T12" id="Seg_1204" s="T11">e-TI-LAr</ta>
            <ta e="T13" id="Seg_1205" s="T12">ol</ta>
            <ta e="T14" id="Seg_1206" s="T13">hir</ta>
            <ta e="T15" id="Seg_1207" s="T14">ičči-tA</ta>
            <ta e="T16" id="Seg_1208" s="T15">hɨrɨt-Ar</ta>
            <ta e="T17" id="Seg_1209" s="T16">hahɨl-LAr-nI</ta>
            <ta e="T18" id="Seg_1210" s="T17">kölün-An</ta>
            <ta e="T19" id="Seg_1211" s="T18">baran</ta>
            <ta e="T20" id="Seg_1212" s="T19">bɨlɨr</ta>
            <ta e="T21" id="Seg_1213" s="T20">min</ta>
            <ta e="T22" id="Seg_1214" s="T21">inʼe-m</ta>
            <ta e="T23" id="Seg_1215" s="T22">bɨraːt-tI-n</ta>
            <ta e="T24" id="Seg_1216" s="T23">ilt-BIT-tA</ta>
            <ta e="T25" id="Seg_1217" s="T24">ogorduk</ta>
            <ta e="T26" id="Seg_1218" s="T25">hir</ta>
            <ta e="T27" id="Seg_1219" s="T26">ičči-tA</ta>
            <ta e="T28" id="Seg_1220" s="T27">oburgu</ta>
            <ta e="T29" id="Seg_1221" s="T28">ogo-nI</ta>
            <ta e="T30" id="Seg_1222" s="T29">tahaːra</ta>
            <ta e="T31" id="Seg_1223" s="T30">hɨrɨt-TAK-InA</ta>
            <ta e="T32" id="Seg_1224" s="T31">biːr</ta>
            <ta e="T33" id="Seg_1225" s="T32">ogonnʼor</ta>
            <ta e="T34" id="Seg_1226" s="T33">kel-BIT</ta>
            <ta e="T35" id="Seg_1227" s="T34">hahɨl-nI</ta>
            <ta e="T36" id="Seg_1228" s="T35">kölün-An</ta>
            <ta e="T37" id="Seg_1229" s="T36">ogo-nI</ta>
            <ta e="T38" id="Seg_1230" s="T37">ɨl-BIT</ta>
            <ta e="T39" id="Seg_1231" s="T38">hɨrga-tI-GAr</ta>
            <ta e="T40" id="Seg_1232" s="T39">bɨrak-BIT</ta>
            <ta e="T41" id="Seg_1233" s="T40">da</ta>
            <ta e="T42" id="Seg_1234" s="T41">kötüt-An</ta>
            <ta e="T43" id="Seg_1235" s="T42">is-BIT</ta>
            <ta e="T44" id="Seg_1236" s="T43">dʼi͡e-tI-GAr</ta>
            <ta e="T45" id="Seg_1237" s="T44">bɨrak-BIT</ta>
            <ta e="T46" id="Seg_1238" s="T45">da</ta>
            <ta e="T47" id="Seg_1239" s="T46">töttörü</ta>
            <ta e="T48" id="Seg_1240" s="T47">tagɨs-I-BIT</ta>
            <ta e="T49" id="Seg_1241" s="T48">bu-GA</ta>
            <ta e="T50" id="Seg_1242" s="T49">bu</ta>
            <ta e="T51" id="Seg_1243" s="T50">ogo-GA</ta>
            <ta e="T52" id="Seg_1244" s="T51">biːr</ta>
            <ta e="T53" id="Seg_1245" s="T52">emeːksin</ta>
            <ta e="T54" id="Seg_1246" s="T53">kel-BIT</ta>
            <ta e="T55" id="Seg_1247" s="T54">dʼe</ta>
            <ta e="T56" id="Seg_1248" s="T55">kuːgu</ta>
            <ta e="T57" id="Seg_1249" s="T56">di͡e-BIT</ta>
            <ta e="T58" id="Seg_1250" s="T57">ehe-ŋ</ta>
            <ta e="T59" id="Seg_1251" s="T58">tagɨs-TI-tA</ta>
            <ta e="T60" id="Seg_1252" s="T59">ɨt-LAr-tI-n</ta>
            <ta e="T61" id="Seg_1253" s="T60">bulgut-A</ta>
            <ta e="T62" id="Seg_1254" s="T61">kel-An</ta>
            <ta e="T63" id="Seg_1255" s="T62">ahaː-A-t-TAK-InA</ta>
            <ta e="T64" id="Seg_1256" s="T63">mantan</ta>
            <ta e="T65" id="Seg_1257" s="T64">dojdu-GA-r</ta>
            <ta e="T66" id="Seg_1258" s="T65">ere</ta>
            <ta e="T67" id="Seg_1259" s="T66">bar-Ar-GA</ta>
            <ta e="T68" id="Seg_1260" s="T67">baga-LAːK</ta>
            <ta e="T69" id="Seg_1261" s="T68">bu͡ol-TAK-GInA</ta>
            <ta e="T70" id="Seg_1262" s="T69">bi͡er-Ar</ta>
            <ta e="T71" id="Seg_1263" s="T70">as-tI-n</ta>
            <ta e="T72" id="Seg_1264" s="T71">hi͡e-AːjA-GIn</ta>
            <ta e="T73" id="Seg_1265" s="T72">manna</ta>
            <ta e="T74" id="Seg_1266" s="T73">kaːl-An</ta>
            <ta e="T75" id="Seg_1267" s="T74">kergen</ta>
            <ta e="T76" id="Seg_1268" s="T75">bu͡ol-IAK</ta>
            <ta e="T77" id="Seg_1269" s="T76">baga-LAːK</ta>
            <ta e="T78" id="Seg_1270" s="T77">bu͡ol-TAK-GInA</ta>
            <ta e="T79" id="Seg_1271" s="T78">oččogo</ta>
            <ta e="T80" id="Seg_1272" s="T79">ere</ta>
            <ta e="T81" id="Seg_1273" s="T80">ahaː-Aːr</ta>
            <ta e="T82" id="Seg_1274" s="T81">ehe-tA</ta>
            <ta e="T83" id="Seg_1275" s="T82">kiːr-An</ta>
            <ta e="T84" id="Seg_1276" s="T83">kündüleː-A</ta>
            <ta e="T85" id="Seg_1277" s="T84">hataː-BIT</ta>
            <ta e="T86" id="Seg_1278" s="T85">tu͡ok-nI</ta>
            <ta e="T87" id="Seg_1279" s="T86">da</ta>
            <ta e="T88" id="Seg_1280" s="T87">hi͡e-BAtAK</ta>
            <ta e="T89" id="Seg_1281" s="T88">hogurduk</ta>
            <ta e="T90" id="Seg_1282" s="T89">bi͡ek</ta>
            <ta e="T91" id="Seg_1283" s="T90">hi͡e-mInA</ta>
            <ta e="T92" id="Seg_1284" s="T91">olor-BIT</ta>
            <ta e="T93" id="Seg_1285" s="T92">bu</ta>
            <ta e="T94" id="Seg_1286" s="T93">ogo</ta>
            <ta e="T95" id="Seg_1287" s="T94">aga-tA</ta>
            <ta e="T96" id="Seg_1288" s="T95">nuːčča</ta>
            <ta e="T97" id="Seg_1289" s="T96">aptaːk-tA</ta>
            <ta e="T98" id="Seg_1290" s="T97">e-TI-tA</ta>
            <ta e="T99" id="Seg_1291" s="T98">ol</ta>
            <ta e="T100" id="Seg_1292" s="T99">üs</ta>
            <ta e="T101" id="Seg_1293" s="T100">tögül</ta>
            <ta e="T102" id="Seg_1294" s="T101">ojun-nI</ta>
            <ta e="T103" id="Seg_1295" s="T102">kɨːr-TAr-BIT</ta>
            <ta e="T104" id="Seg_1296" s="T103">kömö-tA</ta>
            <ta e="T105" id="Seg_1297" s="T104">hu͡ok</ta>
            <ta e="T106" id="Seg_1298" s="T105">ol-LAr</ta>
            <ta e="T107" id="Seg_1299" s="T106">kepseː-BIT-LAr</ta>
            <ta e="T108" id="Seg_1300" s="T107">ere</ta>
            <ta e="T109" id="Seg_1301" s="T108">ogo-GI-n</ta>
            <ta e="T110" id="Seg_1302" s="T109">hir</ta>
            <ta e="T111" id="Seg_1303" s="T110">ičči-tA</ta>
            <ta e="T112" id="Seg_1304" s="T111">ɨl-BIT</ta>
            <ta e="T113" id="Seg_1305" s="T112">di͡e-An</ta>
            <ta e="T114" id="Seg_1306" s="T113">ogo</ta>
            <ta e="T115" id="Seg_1307" s="T114">olor-TAK-InA</ta>
            <ta e="T116" id="Seg_1308" s="T115">üs</ta>
            <ta e="T117" id="Seg_1309" s="T116">tögül</ta>
            <ta e="T118" id="Seg_1310" s="T117">ihit-Ar</ta>
            <ta e="T119" id="Seg_1311" s="T118">düŋür</ta>
            <ta e="T120" id="Seg_1312" s="T119">tɨ͡as-tI-n</ta>
            <ta e="T121" id="Seg_1313" s="T120">ojun</ta>
            <ta e="T122" id="Seg_1314" s="T121">haŋa-tA</ta>
            <ta e="T123" id="Seg_1315" s="T122">ihilin-Ar</ta>
            <ta e="T124" id="Seg_1316" s="T123">aːn-nI</ta>
            <ta e="T125" id="Seg_1317" s="T124">as-I-ŋ</ta>
            <ta e="T126" id="Seg_1318" s="T125">ogo-nI</ta>
            <ta e="T127" id="Seg_1319" s="T126">bi͡er-I-ŋ</ta>
            <ta e="T128" id="Seg_1320" s="T127">di͡e-An</ta>
            <ta e="T129" id="Seg_1321" s="T128">bi͡ek</ta>
            <ta e="T130" id="Seg_1322" s="T129">ol-nI</ta>
            <ta e="T131" id="Seg_1323" s="T130">as-BAT-LAr</ta>
            <ta e="T132" id="Seg_1324" s="T131">ojun-LAr</ta>
            <ta e="T133" id="Seg_1325" s="T132">kötüːn</ta>
            <ta e="T134" id="Seg_1326" s="T133">bar-Ar-LAr</ta>
            <ta e="T135" id="Seg_1327" s="T134">ogo</ta>
            <ta e="T136" id="Seg_1328" s="T135">aga-tA</ta>
            <ta e="T137" id="Seg_1329" s="T136">dʼe</ta>
            <ta e="T138" id="Seg_1330" s="T137">beje-tA</ta>
            <ta e="T139" id="Seg_1331" s="T138">horun-Ar</ta>
            <ta e="T140" id="Seg_1332" s="T139">hir</ta>
            <ta e="T141" id="Seg_1333" s="T140">ičči-tI-n</ta>
            <ta e="T142" id="Seg_1334" s="T141">dʼi͡e-tI-n</ta>
            <ta e="T143" id="Seg_1335" s="T142">ürüt-tI-GAr</ta>
            <ta e="T144" id="Seg_1336" s="T143">tagɨs-An</ta>
            <ta e="T145" id="Seg_1337" s="T144">bɨha</ta>
            <ta e="T146" id="Seg_1338" s="T145">iːkteː-Ar</ta>
            <ta e="T147" id="Seg_1339" s="T146">onton</ta>
            <ta e="T148" id="Seg_1340" s="T147">kɨjgan-An</ta>
            <ta e="T149" id="Seg_1341" s="T148">hir</ta>
            <ta e="T150" id="Seg_1342" s="T149">ičči-tA</ta>
            <ta e="T151" id="Seg_1343" s="T150">ɨl-Ar</ta>
            <ta e="T152" id="Seg_1344" s="T151">ogo-nI</ta>
            <ta e="T153" id="Seg_1345" s="T152">hɨrga-tI-GAr</ta>
            <ta e="T154" id="Seg_1346" s="T153">keːs-Ar</ta>
            <ta e="T155" id="Seg_1347" s="T154">da</ta>
            <ta e="T156" id="Seg_1348" s="T155">kötüt-Ar</ta>
            <ta e="T157" id="Seg_1349" s="T156">dʼi͡e-tI-n</ta>
            <ta e="T158" id="Seg_1350" s="T157">tas-tI-GAr</ta>
            <ta e="T159" id="Seg_1351" s="T158">tij-An</ta>
            <ta e="T160" id="Seg_1352" s="T159">baran</ta>
            <ta e="T161" id="Seg_1353" s="T160">hɨt-Ar</ta>
            <ta e="T162" id="Seg_1354" s="T161">mas-GA</ta>
            <ta e="T163" id="Seg_1355" s="T162">miːn-TAr-An</ta>
            <ta e="T164" id="Seg_1356" s="T163">keːs-Ar</ta>
            <ta e="T165" id="Seg_1357" s="T164">ol-nI</ta>
            <ta e="T166" id="Seg_1358" s="T165">dʼi͡e-tI-ttAn</ta>
            <ta e="T167" id="Seg_1359" s="T166">hüt-BIT-tA</ta>
            <ta e="T168" id="Seg_1360" s="T167">üs</ta>
            <ta e="T169" id="Seg_1361" s="T168">ɨj</ta>
            <ta e="T170" id="Seg_1362" s="T169">bu͡ol-BIT-tI-n</ta>
            <ta e="T171" id="Seg_1363" s="T170">genne</ta>
            <ta e="T172" id="Seg_1364" s="T171">bul-Ar-LAr</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_1365" s="T0">place.[NOM]</ta>
            <ta e="T2" id="Seg_1366" s="T1">master-PROPR.[NOM]</ta>
            <ta e="T3" id="Seg_1367" s="T2">be-PRS.[3SG]</ta>
            <ta e="T4" id="Seg_1368" s="T3">that</ta>
            <ta e="T5" id="Seg_1369" s="T4">earth.[NOM]</ta>
            <ta e="T6" id="Seg_1370" s="T5">master-3SG-ABL</ta>
            <ta e="T7" id="Seg_1371" s="T6">be.afraid-CVB.SEQ</ta>
            <ta e="T8" id="Seg_1372" s="T7">child-PL-ACC</ta>
            <ta e="T9" id="Seg_1373" s="T8">in.the.evening</ta>
            <ta e="T10" id="Seg_1374" s="T9">out</ta>
            <ta e="T11" id="Seg_1375" s="T10">release-NEG.PTCP</ta>
            <ta e="T12" id="Seg_1376" s="T11">be-PST1-3PL</ta>
            <ta e="T13" id="Seg_1377" s="T12">that</ta>
            <ta e="T14" id="Seg_1378" s="T13">earth.[NOM]</ta>
            <ta e="T15" id="Seg_1379" s="T14">master-3SG.[NOM]</ta>
            <ta e="T16" id="Seg_1380" s="T15">drive-PRS.[3SG]</ta>
            <ta e="T17" id="Seg_1381" s="T16">fox-PL-ACC</ta>
            <ta e="T18" id="Seg_1382" s="T17">harness-CVB.SEQ</ta>
            <ta e="T19" id="Seg_1383" s="T18">after</ta>
            <ta e="T20" id="Seg_1384" s="T19">long.ago</ta>
            <ta e="T21" id="Seg_1385" s="T20">1SG.[NOM]</ta>
            <ta e="T22" id="Seg_1386" s="T21">mother-1SG.[NOM]</ta>
            <ta e="T23" id="Seg_1387" s="T22">brother-3SG-ACC</ta>
            <ta e="T24" id="Seg_1388" s="T23">carry-PST2-3SG</ta>
            <ta e="T25" id="Seg_1389" s="T24">like.that</ta>
            <ta e="T26" id="Seg_1390" s="T25">earth.[NOM]</ta>
            <ta e="T27" id="Seg_1391" s="T26">master-3SG.[NOM]</ta>
            <ta e="T28" id="Seg_1392" s="T27">quite.big</ta>
            <ta e="T29" id="Seg_1393" s="T28">child-ACC</ta>
            <ta e="T30" id="Seg_1394" s="T29">outside</ta>
            <ta e="T31" id="Seg_1395" s="T30">go-TEMP-3SG</ta>
            <ta e="T32" id="Seg_1396" s="T31">one</ta>
            <ta e="T33" id="Seg_1397" s="T32">old.man.[NOM]</ta>
            <ta e="T34" id="Seg_1398" s="T33">come-PST2.[3SG]</ta>
            <ta e="T35" id="Seg_1399" s="T34">fox-ACC</ta>
            <ta e="T36" id="Seg_1400" s="T35">harness-CVB.SEQ</ta>
            <ta e="T37" id="Seg_1401" s="T36">child-ACC</ta>
            <ta e="T38" id="Seg_1402" s="T37">take-PST2.[3SG]</ta>
            <ta e="T39" id="Seg_1403" s="T38">sledge-3SG-DAT/LOC</ta>
            <ta e="T40" id="Seg_1404" s="T39">throw-PST2.[3SG]</ta>
            <ta e="T41" id="Seg_1405" s="T40">and</ta>
            <ta e="T42" id="Seg_1406" s="T41">run-CVB.SEQ</ta>
            <ta e="T43" id="Seg_1407" s="T42">go-PST2.[3SG]</ta>
            <ta e="T44" id="Seg_1408" s="T43">house-3SG-DAT/LOC</ta>
            <ta e="T45" id="Seg_1409" s="T44">throw-PST2.[3SG]</ta>
            <ta e="T46" id="Seg_1410" s="T45">and</ta>
            <ta e="T47" id="Seg_1411" s="T46">back</ta>
            <ta e="T48" id="Seg_1412" s="T47">go.out-EP-PST2.[3SG]</ta>
            <ta e="T49" id="Seg_1413" s="T48">this-DAT/LOC</ta>
            <ta e="T50" id="Seg_1414" s="T49">this</ta>
            <ta e="T51" id="Seg_1415" s="T50">child-DAT/LOC</ta>
            <ta e="T52" id="Seg_1416" s="T51">one</ta>
            <ta e="T53" id="Seg_1417" s="T52">old.woman.[NOM]</ta>
            <ta e="T54" id="Seg_1418" s="T53">come-PST2.[3SG]</ta>
            <ta e="T55" id="Seg_1419" s="T54">well</ta>
            <ta e="T56" id="Seg_1420" s="T55">small.child.[NOM]</ta>
            <ta e="T57" id="Seg_1421" s="T56">say-PST2.[3SG]</ta>
            <ta e="T58" id="Seg_1422" s="T57">grandfather-2SG.[NOM]</ta>
            <ta e="T59" id="Seg_1423" s="T58">go.out-PST1-3SG</ta>
            <ta e="T60" id="Seg_1424" s="T59">dog-PL-3SG-ACC</ta>
            <ta e="T61" id="Seg_1425" s="T60">unhitch-CVB.SIM</ta>
            <ta e="T62" id="Seg_1426" s="T61">come-CVB.SEQ</ta>
            <ta e="T63" id="Seg_1427" s="T62">eat-EP-CAUS-TEMP-3SG</ta>
            <ta e="T64" id="Seg_1428" s="T63">from.here</ta>
            <ta e="T65" id="Seg_1429" s="T64">country-2SG-DAT/LOC</ta>
            <ta e="T66" id="Seg_1430" s="T65">just</ta>
            <ta e="T67" id="Seg_1431" s="T66">go-PTCP.PRS-DAT/LOC</ta>
            <ta e="T68" id="Seg_1432" s="T67">wish-PROPR.[NOM]</ta>
            <ta e="T69" id="Seg_1433" s="T68">be-TEMP-2SG</ta>
            <ta e="T70" id="Seg_1434" s="T69">give-PTCP.PRS</ta>
            <ta e="T71" id="Seg_1435" s="T70">food-3SG-ACC</ta>
            <ta e="T72" id="Seg_1436" s="T71">eat-APPR-2SG</ta>
            <ta e="T73" id="Seg_1437" s="T72">here</ta>
            <ta e="T74" id="Seg_1438" s="T73">stay-CVB.SEQ</ta>
            <ta e="T75" id="Seg_1439" s="T74">family.[NOM]</ta>
            <ta e="T76" id="Seg_1440" s="T75">be-PTCP.FUT</ta>
            <ta e="T77" id="Seg_1441" s="T76">wish-PROPR.[NOM]</ta>
            <ta e="T78" id="Seg_1442" s="T77">be-TEMP-2SG</ta>
            <ta e="T79" id="Seg_1443" s="T78">then</ta>
            <ta e="T80" id="Seg_1444" s="T79">just</ta>
            <ta e="T81" id="Seg_1445" s="T80">eat-FUT.[IMP.2SG]</ta>
            <ta e="T82" id="Seg_1446" s="T81">grandfather-3SG.[NOM]</ta>
            <ta e="T83" id="Seg_1447" s="T82">go.in-CVB.SEQ</ta>
            <ta e="T84" id="Seg_1448" s="T83">host-CVB.SIM</ta>
            <ta e="T85" id="Seg_1449" s="T84">do.in.vain-PST2.[3SG]</ta>
            <ta e="T86" id="Seg_1450" s="T85">what-ACC</ta>
            <ta e="T87" id="Seg_1451" s="T86">NEG</ta>
            <ta e="T88" id="Seg_1452" s="T87">eat-PST2.NEG.[3SG]</ta>
            <ta e="T89" id="Seg_1453" s="T88">so</ta>
            <ta e="T90" id="Seg_1454" s="T89">always</ta>
            <ta e="T91" id="Seg_1455" s="T90">eat-NEG.CVB</ta>
            <ta e="T92" id="Seg_1456" s="T91">sit-PST2.[3SG]</ta>
            <ta e="T93" id="Seg_1457" s="T92">this</ta>
            <ta e="T94" id="Seg_1458" s="T93">child.[NOM]</ta>
            <ta e="T95" id="Seg_1459" s="T94">father-3SG.[NOM]</ta>
            <ta e="T96" id="Seg_1460" s="T95">Russian</ta>
            <ta e="T97" id="Seg_1461" s="T96">magician-3SG.[NOM]</ta>
            <ta e="T98" id="Seg_1462" s="T97">be-PST1-3SG</ta>
            <ta e="T99" id="Seg_1463" s="T98">that</ta>
            <ta e="T100" id="Seg_1464" s="T99">three</ta>
            <ta e="T101" id="Seg_1465" s="T100">time.[NOM]</ta>
            <ta e="T102" id="Seg_1466" s="T101">shaman-ACC</ta>
            <ta e="T103" id="Seg_1467" s="T102">shamanize-CAUS-PST2.[3SG]</ta>
            <ta e="T104" id="Seg_1468" s="T103">help-POSS</ta>
            <ta e="T105" id="Seg_1469" s="T104">NEG</ta>
            <ta e="T106" id="Seg_1470" s="T105">that-PL.[NOM]</ta>
            <ta e="T107" id="Seg_1471" s="T106">tell-PST2-3PL</ta>
            <ta e="T108" id="Seg_1472" s="T107">just</ta>
            <ta e="T109" id="Seg_1473" s="T108">child-2SG-ACC</ta>
            <ta e="T110" id="Seg_1474" s="T109">earth.[NOM]</ta>
            <ta e="T111" id="Seg_1475" s="T110">master-3SG.[NOM]</ta>
            <ta e="T112" id="Seg_1476" s="T111">take-PST2.[3SG]</ta>
            <ta e="T113" id="Seg_1477" s="T112">say-CVB.SEQ</ta>
            <ta e="T114" id="Seg_1478" s="T113">child.[NOM]</ta>
            <ta e="T115" id="Seg_1479" s="T114">sit-TEMP-3SG</ta>
            <ta e="T116" id="Seg_1480" s="T115">three</ta>
            <ta e="T117" id="Seg_1481" s="T116">time.[NOM]</ta>
            <ta e="T118" id="Seg_1482" s="T117">hear-PRS.[3SG]</ta>
            <ta e="T119" id="Seg_1483" s="T118">drum.of.the.shaman.[NOM]</ta>
            <ta e="T120" id="Seg_1484" s="T119">sound-3SG-ACC</ta>
            <ta e="T121" id="Seg_1485" s="T120">shaman.[NOM]</ta>
            <ta e="T122" id="Seg_1486" s="T121">voice-3SG.[NOM]</ta>
            <ta e="T123" id="Seg_1487" s="T122">be.heard-PRS.[3SG]</ta>
            <ta e="T124" id="Seg_1488" s="T123">door-ACC</ta>
            <ta e="T125" id="Seg_1489" s="T124">push-EP-IMP.2PL</ta>
            <ta e="T126" id="Seg_1490" s="T125">child-ACC</ta>
            <ta e="T127" id="Seg_1491" s="T126">give-EP-IMP.2PL</ta>
            <ta e="T128" id="Seg_1492" s="T127">say-CVB.SEQ</ta>
            <ta e="T129" id="Seg_1493" s="T128">always</ta>
            <ta e="T130" id="Seg_1494" s="T129">that-ACC</ta>
            <ta e="T131" id="Seg_1495" s="T130">push-NEG-3PL</ta>
            <ta e="T132" id="Seg_1496" s="T131">shaman-PL.[NOM]</ta>
            <ta e="T133" id="Seg_1497" s="T132">in.vain</ta>
            <ta e="T134" id="Seg_1498" s="T133">go-PRS-3PL</ta>
            <ta e="T135" id="Seg_1499" s="T134">child.[NOM]</ta>
            <ta e="T136" id="Seg_1500" s="T135">father-3SG.[NOM]</ta>
            <ta e="T137" id="Seg_1501" s="T136">well</ta>
            <ta e="T138" id="Seg_1502" s="T137">self-3SG.[NOM]</ta>
            <ta e="T139" id="Seg_1503" s="T138">get.ready-PRS.[3SG]</ta>
            <ta e="T140" id="Seg_1504" s="T139">earth.[NOM]</ta>
            <ta e="T141" id="Seg_1505" s="T140">master-3SG-GEN</ta>
            <ta e="T142" id="Seg_1506" s="T141">house-3SG-GEN</ta>
            <ta e="T143" id="Seg_1507" s="T142">upper.part-3SG-DAT/LOC</ta>
            <ta e="T144" id="Seg_1508" s="T143">go.out-CVB.SEQ</ta>
            <ta e="T145" id="Seg_1509" s="T144">through</ta>
            <ta e="T146" id="Seg_1510" s="T145">pee-PRS.[3SG]</ta>
            <ta e="T147" id="Seg_1511" s="T146">then</ta>
            <ta e="T148" id="Seg_1512" s="T147">get.angry-CVB.SEQ</ta>
            <ta e="T149" id="Seg_1513" s="T148">earth.[NOM]</ta>
            <ta e="T150" id="Seg_1514" s="T149">master-3SG.[NOM]</ta>
            <ta e="T151" id="Seg_1515" s="T150">take-PRS.[3SG]</ta>
            <ta e="T152" id="Seg_1516" s="T151">child-ACC</ta>
            <ta e="T153" id="Seg_1517" s="T152">sledge-3SG-DAT/LOC</ta>
            <ta e="T154" id="Seg_1518" s="T153">throw-PRS.[3SG]</ta>
            <ta e="T155" id="Seg_1519" s="T154">and</ta>
            <ta e="T156" id="Seg_1520" s="T155">run-PRS.[3SG]</ta>
            <ta e="T157" id="Seg_1521" s="T156">house-3SG-GEN</ta>
            <ta e="T158" id="Seg_1522" s="T157">edge-3SG-DAT/LOC</ta>
            <ta e="T159" id="Seg_1523" s="T158">reach-CVB.SEQ</ta>
            <ta e="T160" id="Seg_1524" s="T159">after</ta>
            <ta e="T161" id="Seg_1525" s="T160">lie-PTCP.PRS</ta>
            <ta e="T162" id="Seg_1526" s="T161">wood-DAT/LOC</ta>
            <ta e="T163" id="Seg_1527" s="T162">mount-CAUS-CVB.SEQ</ta>
            <ta e="T164" id="Seg_1528" s="T163">throw-PRS.[3SG]</ta>
            <ta e="T165" id="Seg_1529" s="T164">that-ACC</ta>
            <ta e="T166" id="Seg_1530" s="T165">house-3SG-ABL</ta>
            <ta e="T167" id="Seg_1531" s="T166">get.lost-PTCP.PST-3SG.[NOM]</ta>
            <ta e="T168" id="Seg_1532" s="T167">three</ta>
            <ta e="T169" id="Seg_1533" s="T168">month.[NOM]</ta>
            <ta e="T170" id="Seg_1534" s="T169">be-PTCP.PST-3SG-ACC</ta>
            <ta e="T171" id="Seg_1535" s="T170">after</ta>
            <ta e="T172" id="Seg_1536" s="T171">find-PRS-3PL</ta>
         </annotation>
         <annotation name="gg" tierref="gg">
            <ta e="T1" id="Seg_1537" s="T0">Ort.[NOM]</ta>
            <ta e="T2" id="Seg_1538" s="T1">Herr-PROPR.[NOM]</ta>
            <ta e="T3" id="Seg_1539" s="T2">sein-PRS.[3SG]</ta>
            <ta e="T4" id="Seg_1540" s="T3">jenes</ta>
            <ta e="T5" id="Seg_1541" s="T4">Erde.[NOM]</ta>
            <ta e="T6" id="Seg_1542" s="T5">Herr-3SG-ABL</ta>
            <ta e="T7" id="Seg_1543" s="T6">Angst.haben-CVB.SEQ</ta>
            <ta e="T8" id="Seg_1544" s="T7">Kind-PL-ACC</ta>
            <ta e="T9" id="Seg_1545" s="T8">am.Abend</ta>
            <ta e="T10" id="Seg_1546" s="T9">nach.draußen</ta>
            <ta e="T11" id="Seg_1547" s="T10">lassen-NEG.PTCP</ta>
            <ta e="T12" id="Seg_1548" s="T11">sein-PST1-3PL</ta>
            <ta e="T13" id="Seg_1549" s="T12">jenes</ta>
            <ta e="T14" id="Seg_1550" s="T13">Erde.[NOM]</ta>
            <ta e="T15" id="Seg_1551" s="T14">Herr-3SG.[NOM]</ta>
            <ta e="T16" id="Seg_1552" s="T15">fahren-PRS.[3SG]</ta>
            <ta e="T17" id="Seg_1553" s="T16">Fuchs-PL-ACC</ta>
            <ta e="T18" id="Seg_1554" s="T17">einspannen-CVB.SEQ</ta>
            <ta e="T19" id="Seg_1555" s="T18">nachdem</ta>
            <ta e="T20" id="Seg_1556" s="T19">vor.langer.Zeit</ta>
            <ta e="T21" id="Seg_1557" s="T20">1SG.[NOM]</ta>
            <ta e="T22" id="Seg_1558" s="T21">Mutter-1SG.[NOM]</ta>
            <ta e="T23" id="Seg_1559" s="T22">Bruder-3SG-ACC</ta>
            <ta e="T24" id="Seg_1560" s="T23">tragen-PST2-3SG</ta>
            <ta e="T25" id="Seg_1561" s="T24">so</ta>
            <ta e="T26" id="Seg_1562" s="T25">Erde.[NOM]</ta>
            <ta e="T27" id="Seg_1563" s="T26">Herr-3SG.[NOM]</ta>
            <ta e="T28" id="Seg_1564" s="T27">recht.groß</ta>
            <ta e="T29" id="Seg_1565" s="T28">Kind-ACC</ta>
            <ta e="T30" id="Seg_1566" s="T29">draußen</ta>
            <ta e="T31" id="Seg_1567" s="T30">gehen-TEMP-3SG</ta>
            <ta e="T32" id="Seg_1568" s="T31">eins</ta>
            <ta e="T33" id="Seg_1569" s="T32">alter.Mann.[NOM]</ta>
            <ta e="T34" id="Seg_1570" s="T33">kommen-PST2.[3SG]</ta>
            <ta e="T35" id="Seg_1571" s="T34">Fuchs-ACC</ta>
            <ta e="T36" id="Seg_1572" s="T35">einspannen-CVB.SEQ</ta>
            <ta e="T37" id="Seg_1573" s="T36">Kind-ACC</ta>
            <ta e="T38" id="Seg_1574" s="T37">nehmen-PST2.[3SG]</ta>
            <ta e="T39" id="Seg_1575" s="T38">Schlitten-3SG-DAT/LOC</ta>
            <ta e="T40" id="Seg_1576" s="T39">werfen-PST2.[3SG]</ta>
            <ta e="T41" id="Seg_1577" s="T40">und</ta>
            <ta e="T42" id="Seg_1578" s="T41">rennen-CVB.SEQ</ta>
            <ta e="T43" id="Seg_1579" s="T42">gehen-PST2.[3SG]</ta>
            <ta e="T44" id="Seg_1580" s="T43">Haus-3SG-DAT/LOC</ta>
            <ta e="T45" id="Seg_1581" s="T44">werfen-PST2.[3SG]</ta>
            <ta e="T46" id="Seg_1582" s="T45">und</ta>
            <ta e="T47" id="Seg_1583" s="T46">zurück</ta>
            <ta e="T48" id="Seg_1584" s="T47">hinausgehen-EP-PST2.[3SG]</ta>
            <ta e="T49" id="Seg_1585" s="T48">dieses-DAT/LOC</ta>
            <ta e="T50" id="Seg_1586" s="T49">dieses</ta>
            <ta e="T51" id="Seg_1587" s="T50">Kind-DAT/LOC</ta>
            <ta e="T52" id="Seg_1588" s="T51">eins</ta>
            <ta e="T53" id="Seg_1589" s="T52">Alte.[NOM]</ta>
            <ta e="T54" id="Seg_1590" s="T53">kommen-PST2.[3SG]</ta>
            <ta e="T55" id="Seg_1591" s="T54">doch</ta>
            <ta e="T56" id="Seg_1592" s="T55">Kindchen.[NOM]</ta>
            <ta e="T57" id="Seg_1593" s="T56">sagen-PST2.[3SG]</ta>
            <ta e="T58" id="Seg_1594" s="T57">Großvater-2SG.[NOM]</ta>
            <ta e="T59" id="Seg_1595" s="T58">hinausgehen-PST1-3SG</ta>
            <ta e="T60" id="Seg_1596" s="T59">Hund-PL-3SG-ACC</ta>
            <ta e="T61" id="Seg_1597" s="T60">ausspannen-CVB.SIM</ta>
            <ta e="T62" id="Seg_1598" s="T61">kommen-CVB.SEQ</ta>
            <ta e="T63" id="Seg_1599" s="T62">essen-EP-CAUS-TEMP-3SG</ta>
            <ta e="T64" id="Seg_1600" s="T63">von.hier</ta>
            <ta e="T65" id="Seg_1601" s="T64">Land-2SG-DAT/LOC</ta>
            <ta e="T66" id="Seg_1602" s="T65">nur</ta>
            <ta e="T67" id="Seg_1603" s="T66">gehen-PTCP.PRS-DAT/LOC</ta>
            <ta e="T68" id="Seg_1604" s="T67">Wunsch-PROPR.[NOM]</ta>
            <ta e="T69" id="Seg_1605" s="T68">sein-TEMP-2SG</ta>
            <ta e="T70" id="Seg_1606" s="T69">geben-PTCP.PRS</ta>
            <ta e="T71" id="Seg_1607" s="T70">Nahrung-3SG-ACC</ta>
            <ta e="T72" id="Seg_1608" s="T71">essen-APPR-2SG</ta>
            <ta e="T73" id="Seg_1609" s="T72">hier</ta>
            <ta e="T74" id="Seg_1610" s="T73">bleiben-CVB.SEQ</ta>
            <ta e="T75" id="Seg_1611" s="T74">Familie.[NOM]</ta>
            <ta e="T76" id="Seg_1612" s="T75">sein-PTCP.FUT</ta>
            <ta e="T77" id="Seg_1613" s="T76">Wunsch-PROPR.[NOM]</ta>
            <ta e="T78" id="Seg_1614" s="T77">sein-TEMP-2SG</ta>
            <ta e="T79" id="Seg_1615" s="T78">dann</ta>
            <ta e="T80" id="Seg_1616" s="T79">nur</ta>
            <ta e="T81" id="Seg_1617" s="T80">essen-FUT.[IMP.2SG]</ta>
            <ta e="T82" id="Seg_1618" s="T81">Großvater-3SG.[NOM]</ta>
            <ta e="T83" id="Seg_1619" s="T82">hineingehen-CVB.SEQ</ta>
            <ta e="T84" id="Seg_1620" s="T83">bewirten-CVB.SIM</ta>
            <ta e="T85" id="Seg_1621" s="T84">vergeblich.tun-PST2.[3SG]</ta>
            <ta e="T86" id="Seg_1622" s="T85">was-ACC</ta>
            <ta e="T87" id="Seg_1623" s="T86">NEG</ta>
            <ta e="T88" id="Seg_1624" s="T87">essen-PST2.NEG.[3SG]</ta>
            <ta e="T89" id="Seg_1625" s="T88">so</ta>
            <ta e="T90" id="Seg_1626" s="T89">immer</ta>
            <ta e="T91" id="Seg_1627" s="T90">essen-NEG.CVB</ta>
            <ta e="T92" id="Seg_1628" s="T91">sitzen-PST2.[3SG]</ta>
            <ta e="T93" id="Seg_1629" s="T92">dieses</ta>
            <ta e="T94" id="Seg_1630" s="T93">Kind.[NOM]</ta>
            <ta e="T95" id="Seg_1631" s="T94">Vater-3SG.[NOM]</ta>
            <ta e="T96" id="Seg_1632" s="T95">russisch</ta>
            <ta e="T97" id="Seg_1633" s="T96">Zauberer-3SG.[NOM]</ta>
            <ta e="T98" id="Seg_1634" s="T97">sein-PST1-3SG</ta>
            <ta e="T99" id="Seg_1635" s="T98">jenes</ta>
            <ta e="T100" id="Seg_1636" s="T99">drei</ta>
            <ta e="T101" id="Seg_1637" s="T100">Mal.[NOM]</ta>
            <ta e="T102" id="Seg_1638" s="T101">Schamane-ACC</ta>
            <ta e="T103" id="Seg_1639" s="T102">schamanisieren-CAUS-PST2.[3SG]</ta>
            <ta e="T104" id="Seg_1640" s="T103">Hilfe-POSS</ta>
            <ta e="T105" id="Seg_1641" s="T104">NEG</ta>
            <ta e="T106" id="Seg_1642" s="T105">jenes-PL.[NOM]</ta>
            <ta e="T107" id="Seg_1643" s="T106">erzählen-PST2-3PL</ta>
            <ta e="T108" id="Seg_1644" s="T107">nur</ta>
            <ta e="T109" id="Seg_1645" s="T108">Kind-2SG-ACC</ta>
            <ta e="T110" id="Seg_1646" s="T109">Erde.[NOM]</ta>
            <ta e="T111" id="Seg_1647" s="T110">Herr-3SG.[NOM]</ta>
            <ta e="T112" id="Seg_1648" s="T111">nehmen-PST2.[3SG]</ta>
            <ta e="T113" id="Seg_1649" s="T112">sagen-CVB.SEQ</ta>
            <ta e="T114" id="Seg_1650" s="T113">Kind.[NOM]</ta>
            <ta e="T115" id="Seg_1651" s="T114">sitzen-TEMP-3SG</ta>
            <ta e="T116" id="Seg_1652" s="T115">drei</ta>
            <ta e="T117" id="Seg_1653" s="T116">Mal.[NOM]</ta>
            <ta e="T118" id="Seg_1654" s="T117">hören-PRS.[3SG]</ta>
            <ta e="T119" id="Seg_1655" s="T118">Schamanentrommel.[NOM]</ta>
            <ta e="T120" id="Seg_1656" s="T119">Geräusch-3SG-ACC</ta>
            <ta e="T121" id="Seg_1657" s="T120">Schamane.[NOM]</ta>
            <ta e="T122" id="Seg_1658" s="T121">Stimme-3SG.[NOM]</ta>
            <ta e="T123" id="Seg_1659" s="T122">gehört.werden-PRS.[3SG]</ta>
            <ta e="T124" id="Seg_1660" s="T123">Tür-ACC</ta>
            <ta e="T125" id="Seg_1661" s="T124">stoßen-EP-IMP.2PL</ta>
            <ta e="T126" id="Seg_1662" s="T125">Kind-ACC</ta>
            <ta e="T127" id="Seg_1663" s="T126">geben-EP-IMP.2PL</ta>
            <ta e="T128" id="Seg_1664" s="T127">sagen-CVB.SEQ</ta>
            <ta e="T129" id="Seg_1665" s="T128">immer</ta>
            <ta e="T130" id="Seg_1666" s="T129">jenes-ACC</ta>
            <ta e="T131" id="Seg_1667" s="T130">stoßen-NEG-3PL</ta>
            <ta e="T132" id="Seg_1668" s="T131">Schamane-PL.[NOM]</ta>
            <ta e="T133" id="Seg_1669" s="T132">vergebens</ta>
            <ta e="T134" id="Seg_1670" s="T133">gehen-PRS-3PL</ta>
            <ta e="T135" id="Seg_1671" s="T134">Kind.[NOM]</ta>
            <ta e="T136" id="Seg_1672" s="T135">Vater-3SG.[NOM]</ta>
            <ta e="T137" id="Seg_1673" s="T136">doch</ta>
            <ta e="T138" id="Seg_1674" s="T137">selbst-3SG.[NOM]</ta>
            <ta e="T139" id="Seg_1675" s="T138">sich.bereit.machen-PRS.[3SG]</ta>
            <ta e="T140" id="Seg_1676" s="T139">Erde.[NOM]</ta>
            <ta e="T141" id="Seg_1677" s="T140">Herr-3SG-GEN</ta>
            <ta e="T142" id="Seg_1678" s="T141">Haus-3SG-GEN</ta>
            <ta e="T143" id="Seg_1679" s="T142">oberer.Teil-3SG-DAT/LOC</ta>
            <ta e="T144" id="Seg_1680" s="T143">hinausgehen-CVB.SEQ</ta>
            <ta e="T145" id="Seg_1681" s="T144">durch</ta>
            <ta e="T146" id="Seg_1682" s="T145">pinkeln-PRS.[3SG]</ta>
            <ta e="T147" id="Seg_1683" s="T146">dann</ta>
            <ta e="T148" id="Seg_1684" s="T147">böse.werden-CVB.SEQ</ta>
            <ta e="T149" id="Seg_1685" s="T148">Erde.[NOM]</ta>
            <ta e="T150" id="Seg_1686" s="T149">Herr-3SG.[NOM]</ta>
            <ta e="T151" id="Seg_1687" s="T150">nehmen-PRS.[3SG]</ta>
            <ta e="T152" id="Seg_1688" s="T151">Kind-ACC</ta>
            <ta e="T153" id="Seg_1689" s="T152">Schlitten-3SG-DAT/LOC</ta>
            <ta e="T154" id="Seg_1690" s="T153">werfen-PRS.[3SG]</ta>
            <ta e="T155" id="Seg_1691" s="T154">und</ta>
            <ta e="T156" id="Seg_1692" s="T155">rennen-PRS.[3SG]</ta>
            <ta e="T157" id="Seg_1693" s="T156">Haus-3SG-GEN</ta>
            <ta e="T158" id="Seg_1694" s="T157">Rand-3SG-DAT/LOC</ta>
            <ta e="T159" id="Seg_1695" s="T158">ankommen-CVB.SEQ</ta>
            <ta e="T160" id="Seg_1696" s="T159">nachdem</ta>
            <ta e="T161" id="Seg_1697" s="T160">liegen-PTCP.PRS</ta>
            <ta e="T162" id="Seg_1698" s="T161">Holz-DAT/LOC</ta>
            <ta e="T163" id="Seg_1699" s="T162">besteigen-CAUS-CVB.SEQ</ta>
            <ta e="T164" id="Seg_1700" s="T163">werfen-PRS.[3SG]</ta>
            <ta e="T165" id="Seg_1701" s="T164">jenes-ACC</ta>
            <ta e="T166" id="Seg_1702" s="T165">Haus-3SG-ABL</ta>
            <ta e="T167" id="Seg_1703" s="T166">verlorengehen-PTCP.PST-3SG.[NOM]</ta>
            <ta e="T168" id="Seg_1704" s="T167">drei</ta>
            <ta e="T169" id="Seg_1705" s="T168">Monat.[NOM]</ta>
            <ta e="T170" id="Seg_1706" s="T169">sein-PTCP.PST-3SG-ACC</ta>
            <ta e="T171" id="Seg_1707" s="T170">nachdem</ta>
            <ta e="T172" id="Seg_1708" s="T171">finden-PRS-3PL</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_1709" s="T0">место.[NOM]</ta>
            <ta e="T2" id="Seg_1710" s="T1">господин-PROPR.[NOM]</ta>
            <ta e="T3" id="Seg_1711" s="T2">быть-PRS.[3SG]</ta>
            <ta e="T4" id="Seg_1712" s="T3">тот</ta>
            <ta e="T5" id="Seg_1713" s="T4">земля.[NOM]</ta>
            <ta e="T6" id="Seg_1714" s="T5">господин-3SG-ABL</ta>
            <ta e="T7" id="Seg_1715" s="T6">бояться-CVB.SEQ</ta>
            <ta e="T8" id="Seg_1716" s="T7">ребенок-PL-ACC</ta>
            <ta e="T9" id="Seg_1717" s="T8">вечером</ta>
            <ta e="T10" id="Seg_1718" s="T9">на.улицу</ta>
            <ta e="T11" id="Seg_1719" s="T10">пустить-NEG.PTCP</ta>
            <ta e="T12" id="Seg_1720" s="T11">быть-PST1-3PL</ta>
            <ta e="T13" id="Seg_1721" s="T12">тот</ta>
            <ta e="T14" id="Seg_1722" s="T13">земля.[NOM]</ta>
            <ta e="T15" id="Seg_1723" s="T14">господин-3SG.[NOM]</ta>
            <ta e="T16" id="Seg_1724" s="T15">ехать-PRS.[3SG]</ta>
            <ta e="T17" id="Seg_1725" s="T16">лиса-PL-ACC</ta>
            <ta e="T18" id="Seg_1726" s="T17">запрячь-CVB.SEQ</ta>
            <ta e="T19" id="Seg_1727" s="T18">после</ta>
            <ta e="T20" id="Seg_1728" s="T19">давно</ta>
            <ta e="T21" id="Seg_1729" s="T20">1SG.[NOM]</ta>
            <ta e="T22" id="Seg_1730" s="T21">мать-1SG.[NOM]</ta>
            <ta e="T23" id="Seg_1731" s="T22">брат-3SG-ACC</ta>
            <ta e="T24" id="Seg_1732" s="T23">носить-PST2-3SG</ta>
            <ta e="T25" id="Seg_1733" s="T24">так</ta>
            <ta e="T26" id="Seg_1734" s="T25">земля.[NOM]</ta>
            <ta e="T27" id="Seg_1735" s="T26">господин-3SG.[NOM]</ta>
            <ta e="T28" id="Seg_1736" s="T27">довольно.большой</ta>
            <ta e="T29" id="Seg_1737" s="T28">ребенок-ACC</ta>
            <ta e="T30" id="Seg_1738" s="T29">на.улице</ta>
            <ta e="T31" id="Seg_1739" s="T30">идти-TEMP-3SG</ta>
            <ta e="T32" id="Seg_1740" s="T31">один</ta>
            <ta e="T33" id="Seg_1741" s="T32">старик.[NOM]</ta>
            <ta e="T34" id="Seg_1742" s="T33">приходить-PST2.[3SG]</ta>
            <ta e="T35" id="Seg_1743" s="T34">лиса-ACC</ta>
            <ta e="T36" id="Seg_1744" s="T35">запрячь-CVB.SEQ</ta>
            <ta e="T37" id="Seg_1745" s="T36">ребенок-ACC</ta>
            <ta e="T38" id="Seg_1746" s="T37">взять-PST2.[3SG]</ta>
            <ta e="T39" id="Seg_1747" s="T38">сани-3SG-DAT/LOC</ta>
            <ta e="T40" id="Seg_1748" s="T39">бросать-PST2.[3SG]</ta>
            <ta e="T41" id="Seg_1749" s="T40">да</ta>
            <ta e="T42" id="Seg_1750" s="T41">мчаться-CVB.SEQ</ta>
            <ta e="T43" id="Seg_1751" s="T42">идти-PST2.[3SG]</ta>
            <ta e="T44" id="Seg_1752" s="T43">дом-3SG-DAT/LOC</ta>
            <ta e="T45" id="Seg_1753" s="T44">бросать-PST2.[3SG]</ta>
            <ta e="T46" id="Seg_1754" s="T45">да</ta>
            <ta e="T47" id="Seg_1755" s="T46">назад</ta>
            <ta e="T48" id="Seg_1756" s="T47">выйти-EP-PST2.[3SG]</ta>
            <ta e="T49" id="Seg_1757" s="T48">этот-DAT/LOC</ta>
            <ta e="T50" id="Seg_1758" s="T49">этот</ta>
            <ta e="T51" id="Seg_1759" s="T50">ребенок-DAT/LOC</ta>
            <ta e="T52" id="Seg_1760" s="T51">один</ta>
            <ta e="T53" id="Seg_1761" s="T52">старуха.[NOM]</ta>
            <ta e="T54" id="Seg_1762" s="T53">приходить-PST2.[3SG]</ta>
            <ta e="T55" id="Seg_1763" s="T54">вот</ta>
            <ta e="T56" id="Seg_1764" s="T55">детенок.[NOM]</ta>
            <ta e="T57" id="Seg_1765" s="T56">говорить-PST2.[3SG]</ta>
            <ta e="T58" id="Seg_1766" s="T57">дедушка-2SG.[NOM]</ta>
            <ta e="T59" id="Seg_1767" s="T58">выйти-PST1-3SG</ta>
            <ta e="T60" id="Seg_1768" s="T59">собака-PL-3SG-ACC</ta>
            <ta e="T61" id="Seg_1769" s="T60">распрягать-CVB.SIM</ta>
            <ta e="T62" id="Seg_1770" s="T61">приходить-CVB.SEQ</ta>
            <ta e="T63" id="Seg_1771" s="T62">есть-EP-CAUS-TEMP-3SG</ta>
            <ta e="T64" id="Seg_1772" s="T63">отсюда</ta>
            <ta e="T65" id="Seg_1773" s="T64">страна-2SG-DAT/LOC</ta>
            <ta e="T66" id="Seg_1774" s="T65">только</ta>
            <ta e="T67" id="Seg_1775" s="T66">идти-PTCP.PRS-DAT/LOC</ta>
            <ta e="T68" id="Seg_1776" s="T67">желание-PROPR.[NOM]</ta>
            <ta e="T69" id="Seg_1777" s="T68">быть-TEMP-2SG</ta>
            <ta e="T70" id="Seg_1778" s="T69">давать-PTCP.PRS</ta>
            <ta e="T71" id="Seg_1779" s="T70">пища-3SG-ACC</ta>
            <ta e="T72" id="Seg_1780" s="T71">есть-APPR-2SG</ta>
            <ta e="T73" id="Seg_1781" s="T72">здесь</ta>
            <ta e="T74" id="Seg_1782" s="T73">оставаться-CVB.SEQ</ta>
            <ta e="T75" id="Seg_1783" s="T74">семья.[NOM]</ta>
            <ta e="T76" id="Seg_1784" s="T75">быть-PTCP.FUT</ta>
            <ta e="T77" id="Seg_1785" s="T76">желание-PROPR.[NOM]</ta>
            <ta e="T78" id="Seg_1786" s="T77">быть-TEMP-2SG</ta>
            <ta e="T79" id="Seg_1787" s="T78">тогда</ta>
            <ta e="T80" id="Seg_1788" s="T79">только</ta>
            <ta e="T81" id="Seg_1789" s="T80">есть-FUT.[IMP.2SG]</ta>
            <ta e="T82" id="Seg_1790" s="T81">дедушка-3SG.[NOM]</ta>
            <ta e="T83" id="Seg_1791" s="T82">входить-CVB.SEQ</ta>
            <ta e="T84" id="Seg_1792" s="T83">угощать-CVB.SIM</ta>
            <ta e="T85" id="Seg_1793" s="T84">делать.напрасно-PST2.[3SG]</ta>
            <ta e="T86" id="Seg_1794" s="T85">что-ACC</ta>
            <ta e="T87" id="Seg_1795" s="T86">NEG</ta>
            <ta e="T88" id="Seg_1796" s="T87">есть-PST2.NEG.[3SG]</ta>
            <ta e="T89" id="Seg_1797" s="T88">так</ta>
            <ta e="T90" id="Seg_1798" s="T89">всегда</ta>
            <ta e="T91" id="Seg_1799" s="T90">есть-NEG.CVB</ta>
            <ta e="T92" id="Seg_1800" s="T91">сидеть-PST2.[3SG]</ta>
            <ta e="T93" id="Seg_1801" s="T92">этот</ta>
            <ta e="T94" id="Seg_1802" s="T93">ребенок.[NOM]</ta>
            <ta e="T95" id="Seg_1803" s="T94">отец-3SG.[NOM]</ta>
            <ta e="T96" id="Seg_1804" s="T95">русский</ta>
            <ta e="T97" id="Seg_1805" s="T96">колдун-3SG.[NOM]</ta>
            <ta e="T98" id="Seg_1806" s="T97">быть-PST1-3SG</ta>
            <ta e="T99" id="Seg_1807" s="T98">тот</ta>
            <ta e="T100" id="Seg_1808" s="T99">три</ta>
            <ta e="T101" id="Seg_1809" s="T100">раз.[NOM]</ta>
            <ta e="T102" id="Seg_1810" s="T101">шаман-ACC</ta>
            <ta e="T103" id="Seg_1811" s="T102">камлать-CAUS-PST2.[3SG]</ta>
            <ta e="T104" id="Seg_1812" s="T103">помощь-POSS</ta>
            <ta e="T105" id="Seg_1813" s="T104">NEG</ta>
            <ta e="T106" id="Seg_1814" s="T105">тот-PL.[NOM]</ta>
            <ta e="T107" id="Seg_1815" s="T106">рассказывать-PST2-3PL</ta>
            <ta e="T108" id="Seg_1816" s="T107">только</ta>
            <ta e="T109" id="Seg_1817" s="T108">ребенок-2SG-ACC</ta>
            <ta e="T110" id="Seg_1818" s="T109">земля.[NOM]</ta>
            <ta e="T111" id="Seg_1819" s="T110">господин-3SG.[NOM]</ta>
            <ta e="T112" id="Seg_1820" s="T111">взять-PST2.[3SG]</ta>
            <ta e="T113" id="Seg_1821" s="T112">говорить-CVB.SEQ</ta>
            <ta e="T114" id="Seg_1822" s="T113">ребенок.[NOM]</ta>
            <ta e="T115" id="Seg_1823" s="T114">сидеть-TEMP-3SG</ta>
            <ta e="T116" id="Seg_1824" s="T115">три</ta>
            <ta e="T117" id="Seg_1825" s="T116">раз.[NOM]</ta>
            <ta e="T118" id="Seg_1826" s="T117">слышать-PRS.[3SG]</ta>
            <ta e="T119" id="Seg_1827" s="T118">шаманский.бубен.[NOM]</ta>
            <ta e="T120" id="Seg_1828" s="T119">звук-3SG-ACC</ta>
            <ta e="T121" id="Seg_1829" s="T120">шаман.[NOM]</ta>
            <ta e="T122" id="Seg_1830" s="T121">голос-3SG.[NOM]</ta>
            <ta e="T123" id="Seg_1831" s="T122">слышаться-PRS.[3SG]</ta>
            <ta e="T124" id="Seg_1832" s="T123">дверь-ACC</ta>
            <ta e="T125" id="Seg_1833" s="T124">толкать-EP-IMP.2PL</ta>
            <ta e="T126" id="Seg_1834" s="T125">ребенок-ACC</ta>
            <ta e="T127" id="Seg_1835" s="T126">давать-EP-IMP.2PL</ta>
            <ta e="T128" id="Seg_1836" s="T127">говорить-CVB.SEQ</ta>
            <ta e="T129" id="Seg_1837" s="T128">всегда</ta>
            <ta e="T130" id="Seg_1838" s="T129">тот-ACC</ta>
            <ta e="T131" id="Seg_1839" s="T130">толкать-NEG-3PL</ta>
            <ta e="T132" id="Seg_1840" s="T131">шаман-PL.[NOM]</ta>
            <ta e="T133" id="Seg_1841" s="T132">зря</ta>
            <ta e="T134" id="Seg_1842" s="T133">идти-PRS-3PL</ta>
            <ta e="T135" id="Seg_1843" s="T134">ребенок.[NOM]</ta>
            <ta e="T136" id="Seg_1844" s="T135">отец-3SG.[NOM]</ta>
            <ta e="T137" id="Seg_1845" s="T136">вот</ta>
            <ta e="T138" id="Seg_1846" s="T137">сам-3SG.[NOM]</ta>
            <ta e="T139" id="Seg_1847" s="T138">собраться-PRS.[3SG]</ta>
            <ta e="T140" id="Seg_1848" s="T139">земля.[NOM]</ta>
            <ta e="T141" id="Seg_1849" s="T140">господин-3SG-GEN</ta>
            <ta e="T142" id="Seg_1850" s="T141">дом-3SG-GEN</ta>
            <ta e="T143" id="Seg_1851" s="T142">верхняя.часть-3SG-DAT/LOC</ta>
            <ta e="T144" id="Seg_1852" s="T143">выйти-CVB.SEQ</ta>
            <ta e="T145" id="Seg_1853" s="T144">сквозь</ta>
            <ta e="T146" id="Seg_1854" s="T145">мочиться-PRS.[3SG]</ta>
            <ta e="T147" id="Seg_1855" s="T146">потом</ta>
            <ta e="T148" id="Seg_1856" s="T147">рассердиться-CVB.SEQ</ta>
            <ta e="T149" id="Seg_1857" s="T148">земля.[NOM]</ta>
            <ta e="T150" id="Seg_1858" s="T149">господин-3SG.[NOM]</ta>
            <ta e="T151" id="Seg_1859" s="T150">взять-PRS.[3SG]</ta>
            <ta e="T152" id="Seg_1860" s="T151">ребенок-ACC</ta>
            <ta e="T153" id="Seg_1861" s="T152">сани-3SG-DAT/LOC</ta>
            <ta e="T154" id="Seg_1862" s="T153">бросать-PRS.[3SG]</ta>
            <ta e="T155" id="Seg_1863" s="T154">да</ta>
            <ta e="T156" id="Seg_1864" s="T155">мчаться-PRS.[3SG]</ta>
            <ta e="T157" id="Seg_1865" s="T156">дом-3SG-GEN</ta>
            <ta e="T158" id="Seg_1866" s="T157">край-3SG-DAT/LOC</ta>
            <ta e="T159" id="Seg_1867" s="T158">доезжать-CVB.SEQ</ta>
            <ta e="T160" id="Seg_1868" s="T159">после</ta>
            <ta e="T161" id="Seg_1869" s="T160">лежать-PTCP.PRS</ta>
            <ta e="T162" id="Seg_1870" s="T161">дерево-DAT/LOC</ta>
            <ta e="T163" id="Seg_1871" s="T162">садиться-CAUS-CVB.SEQ</ta>
            <ta e="T164" id="Seg_1872" s="T163">бросать-PRS.[3SG]</ta>
            <ta e="T165" id="Seg_1873" s="T164">тот-ACC</ta>
            <ta e="T166" id="Seg_1874" s="T165">дом-3SG-ABL</ta>
            <ta e="T167" id="Seg_1875" s="T166">пропадать-PTCP.PST-3SG.[NOM]</ta>
            <ta e="T168" id="Seg_1876" s="T167">три</ta>
            <ta e="T169" id="Seg_1877" s="T168">месяц.[NOM]</ta>
            <ta e="T170" id="Seg_1878" s="T169">быть-PTCP.PST-3SG-ACC</ta>
            <ta e="T171" id="Seg_1879" s="T170">после.того</ta>
            <ta e="T172" id="Seg_1880" s="T171">найти-PRS-3PL</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_1881" s="T0">n-n:case</ta>
            <ta e="T2" id="Seg_1882" s="T1">n-n&gt;adj-n:case</ta>
            <ta e="T3" id="Seg_1883" s="T2">v-v:tense-v:pred.pn</ta>
            <ta e="T4" id="Seg_1884" s="T3">dempro</ta>
            <ta e="T5" id="Seg_1885" s="T4">n-n:case</ta>
            <ta e="T6" id="Seg_1886" s="T5">n-n:poss-n:case</ta>
            <ta e="T7" id="Seg_1887" s="T6">v-v:cvb</ta>
            <ta e="T8" id="Seg_1888" s="T7">n-n:(num)-n:case</ta>
            <ta e="T9" id="Seg_1889" s="T8">adv</ta>
            <ta e="T10" id="Seg_1890" s="T9">adv</ta>
            <ta e="T11" id="Seg_1891" s="T10">v-v:ptcp</ta>
            <ta e="T12" id="Seg_1892" s="T11">v-v:tense-v:pred.pn</ta>
            <ta e="T13" id="Seg_1893" s="T12">dempro</ta>
            <ta e="T14" id="Seg_1894" s="T13">n-n:case</ta>
            <ta e="T15" id="Seg_1895" s="T14">n-n:(poss)-n:case</ta>
            <ta e="T16" id="Seg_1896" s="T15">v-v:tense-v:pred.pn</ta>
            <ta e="T17" id="Seg_1897" s="T16">n-n:(num)-n:case</ta>
            <ta e="T18" id="Seg_1898" s="T17">v-v:cvb</ta>
            <ta e="T19" id="Seg_1899" s="T18">post</ta>
            <ta e="T20" id="Seg_1900" s="T19">adv</ta>
            <ta e="T21" id="Seg_1901" s="T20">pers-pro:case</ta>
            <ta e="T22" id="Seg_1902" s="T21">n-n:(poss)-n:case</ta>
            <ta e="T23" id="Seg_1903" s="T22">n-n:poss-n:case</ta>
            <ta e="T24" id="Seg_1904" s="T23">v-v:tense-v:poss.pn</ta>
            <ta e="T25" id="Seg_1905" s="T24">adv</ta>
            <ta e="T26" id="Seg_1906" s="T25">n-n:case</ta>
            <ta e="T27" id="Seg_1907" s="T26">n-n:(poss)-n:case</ta>
            <ta e="T28" id="Seg_1908" s="T27">adj</ta>
            <ta e="T29" id="Seg_1909" s="T28">n-n:case</ta>
            <ta e="T30" id="Seg_1910" s="T29">adv</ta>
            <ta e="T31" id="Seg_1911" s="T30">v-v:mood-v:temp.pn</ta>
            <ta e="T32" id="Seg_1912" s="T31">cardnum</ta>
            <ta e="T33" id="Seg_1913" s="T32">n-n:case</ta>
            <ta e="T34" id="Seg_1914" s="T33">v-v:tense-v:pred.pn</ta>
            <ta e="T35" id="Seg_1915" s="T34">n-n:case</ta>
            <ta e="T36" id="Seg_1916" s="T35">v-v:cvb</ta>
            <ta e="T37" id="Seg_1917" s="T36">n-n:case</ta>
            <ta e="T38" id="Seg_1918" s="T37">v-v:tense-v:pred.pn</ta>
            <ta e="T39" id="Seg_1919" s="T38">n-n:poss-n:case</ta>
            <ta e="T40" id="Seg_1920" s="T39">v-v:tense-v:pred.pn</ta>
            <ta e="T41" id="Seg_1921" s="T40">conj</ta>
            <ta e="T42" id="Seg_1922" s="T41">v-v:cvb</ta>
            <ta e="T43" id="Seg_1923" s="T42">v-v:tense-v:pred.pn</ta>
            <ta e="T44" id="Seg_1924" s="T43">n-n:poss-n:case</ta>
            <ta e="T45" id="Seg_1925" s="T44">v-v:tense-v:pred.pn</ta>
            <ta e="T46" id="Seg_1926" s="T45">conj</ta>
            <ta e="T47" id="Seg_1927" s="T46">adv</ta>
            <ta e="T48" id="Seg_1928" s="T47">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T49" id="Seg_1929" s="T48">dempro-pro:case</ta>
            <ta e="T50" id="Seg_1930" s="T49">dempro</ta>
            <ta e="T51" id="Seg_1931" s="T50">n-n:case</ta>
            <ta e="T52" id="Seg_1932" s="T51">cardnum</ta>
            <ta e="T53" id="Seg_1933" s="T52">n-n:case</ta>
            <ta e="T54" id="Seg_1934" s="T53">v-v:tense-v:pred.pn</ta>
            <ta e="T55" id="Seg_1935" s="T54">ptcl</ta>
            <ta e="T56" id="Seg_1936" s="T55">n-n:case</ta>
            <ta e="T57" id="Seg_1937" s="T56">v-v:tense-v:pred.pn</ta>
            <ta e="T58" id="Seg_1938" s="T57">n-n:(poss)-n:case</ta>
            <ta e="T59" id="Seg_1939" s="T58">v-v:tense-v:poss.pn</ta>
            <ta e="T60" id="Seg_1940" s="T59">n-n:(num)-n:poss-n:case</ta>
            <ta e="T61" id="Seg_1941" s="T60">v-v:cvb</ta>
            <ta e="T62" id="Seg_1942" s="T61">v-v:cvb</ta>
            <ta e="T63" id="Seg_1943" s="T62">v-v:(ins)-v&gt;v-v:mood-v:temp.pn</ta>
            <ta e="T64" id="Seg_1944" s="T63">adv</ta>
            <ta e="T65" id="Seg_1945" s="T64">n-n:poss-n:case</ta>
            <ta e="T66" id="Seg_1946" s="T65">ptcl</ta>
            <ta e="T67" id="Seg_1947" s="T66">v-v:ptcp-v:(case)</ta>
            <ta e="T68" id="Seg_1948" s="T67">n-n&gt;adj-n:case</ta>
            <ta e="T69" id="Seg_1949" s="T68">v-v:mood-v:temp.pn</ta>
            <ta e="T70" id="Seg_1950" s="T69">v-v:ptcp</ta>
            <ta e="T71" id="Seg_1951" s="T70">n-n:poss-n:case</ta>
            <ta e="T72" id="Seg_1952" s="T71">v-v:mood-v:pred.pn</ta>
            <ta e="T73" id="Seg_1953" s="T72">adv</ta>
            <ta e="T74" id="Seg_1954" s="T73">v-v:cvb</ta>
            <ta e="T75" id="Seg_1955" s="T74">n-n:case</ta>
            <ta e="T76" id="Seg_1956" s="T75">v-v:ptcp</ta>
            <ta e="T77" id="Seg_1957" s="T76">n-n&gt;adj-n:case</ta>
            <ta e="T78" id="Seg_1958" s="T77">v-v:mood-v:temp.pn</ta>
            <ta e="T79" id="Seg_1959" s="T78">adv</ta>
            <ta e="T80" id="Seg_1960" s="T79">ptcl</ta>
            <ta e="T81" id="Seg_1961" s="T80">v-v:(tense)-v:mood.pn</ta>
            <ta e="T82" id="Seg_1962" s="T81">n-n:(poss)-n:case</ta>
            <ta e="T83" id="Seg_1963" s="T82">v-v:cvb</ta>
            <ta e="T84" id="Seg_1964" s="T83">v-v:cvb</ta>
            <ta e="T85" id="Seg_1965" s="T84">v-v:tense-v:pred.pn</ta>
            <ta e="T86" id="Seg_1966" s="T85">que-pro:case</ta>
            <ta e="T87" id="Seg_1967" s="T86">ptcl</ta>
            <ta e="T88" id="Seg_1968" s="T87">v-v:neg-v:pred.pn</ta>
            <ta e="T89" id="Seg_1969" s="T88">adv</ta>
            <ta e="T90" id="Seg_1970" s="T89">adv</ta>
            <ta e="T91" id="Seg_1971" s="T90">v-v:cvb</ta>
            <ta e="T92" id="Seg_1972" s="T91">v-v:tense-v:pred.pn</ta>
            <ta e="T93" id="Seg_1973" s="T92">dempro</ta>
            <ta e="T94" id="Seg_1974" s="T93">n-n:case</ta>
            <ta e="T95" id="Seg_1975" s="T94">n-n:(poss)-n:case</ta>
            <ta e="T96" id="Seg_1976" s="T95">adj</ta>
            <ta e="T97" id="Seg_1977" s="T96">n-n:(poss)-n:case</ta>
            <ta e="T98" id="Seg_1978" s="T97">v-v:tense-v:poss.pn</ta>
            <ta e="T99" id="Seg_1979" s="T98">dempro</ta>
            <ta e="T100" id="Seg_1980" s="T99">cardnum</ta>
            <ta e="T101" id="Seg_1981" s="T100">n-n:case</ta>
            <ta e="T102" id="Seg_1982" s="T101">n-n:case</ta>
            <ta e="T103" id="Seg_1983" s="T102">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T104" id="Seg_1984" s="T103">n-n:(poss)</ta>
            <ta e="T105" id="Seg_1985" s="T104">ptcl</ta>
            <ta e="T106" id="Seg_1986" s="T105">dempro-pro:(num)-pro:case</ta>
            <ta e="T107" id="Seg_1987" s="T106">v-v:tense-v:pred.pn</ta>
            <ta e="T108" id="Seg_1988" s="T107">ptcl</ta>
            <ta e="T109" id="Seg_1989" s="T108">n-n:poss-n:case</ta>
            <ta e="T110" id="Seg_1990" s="T109">n-n:case</ta>
            <ta e="T111" id="Seg_1991" s="T110">n-n:(poss)-n:case</ta>
            <ta e="T112" id="Seg_1992" s="T111">v-v:tense-v:pred.pn</ta>
            <ta e="T113" id="Seg_1993" s="T112">v-v:cvb</ta>
            <ta e="T114" id="Seg_1994" s="T113">n-n:case</ta>
            <ta e="T115" id="Seg_1995" s="T114">v-v:mood-v:temp.pn</ta>
            <ta e="T116" id="Seg_1996" s="T115">cardnum</ta>
            <ta e="T117" id="Seg_1997" s="T116">n-n:case</ta>
            <ta e="T118" id="Seg_1998" s="T117">v-v:tense-v:pred.pn</ta>
            <ta e="T119" id="Seg_1999" s="T118">n-n:case</ta>
            <ta e="T120" id="Seg_2000" s="T119">n-n:poss-n:case</ta>
            <ta e="T121" id="Seg_2001" s="T120">n-n:case</ta>
            <ta e="T122" id="Seg_2002" s="T121">n-n:(poss)-n:case</ta>
            <ta e="T123" id="Seg_2003" s="T122">v-v:tense-v:pred.pn</ta>
            <ta e="T124" id="Seg_2004" s="T123">n-n:case</ta>
            <ta e="T125" id="Seg_2005" s="T124">v-v:(ins)-v:mood.pn</ta>
            <ta e="T126" id="Seg_2006" s="T125">n-n:case</ta>
            <ta e="T127" id="Seg_2007" s="T126">v-v:(ins)-v:mood.pn</ta>
            <ta e="T128" id="Seg_2008" s="T127">v-v:cvb</ta>
            <ta e="T129" id="Seg_2009" s="T128">adv</ta>
            <ta e="T130" id="Seg_2010" s="T129">dempro-pro:case</ta>
            <ta e="T131" id="Seg_2011" s="T130">v-v:(neg)-v:pred.pn</ta>
            <ta e="T132" id="Seg_2012" s="T131">n-n:(num)-n:case</ta>
            <ta e="T133" id="Seg_2013" s="T132">adv</ta>
            <ta e="T134" id="Seg_2014" s="T133">v-v:tense-v:pred.pn</ta>
            <ta e="T135" id="Seg_2015" s="T134">n-n:case</ta>
            <ta e="T136" id="Seg_2016" s="T135">n-n:(poss)-n:case</ta>
            <ta e="T137" id="Seg_2017" s="T136">ptcl</ta>
            <ta e="T138" id="Seg_2018" s="T137">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T139" id="Seg_2019" s="T138">v-v:tense-v:pred.pn</ta>
            <ta e="T140" id="Seg_2020" s="T139">n-n:case</ta>
            <ta e="T141" id="Seg_2021" s="T140">n-n:poss-n:case</ta>
            <ta e="T142" id="Seg_2022" s="T141">n-n:poss-n:case</ta>
            <ta e="T143" id="Seg_2023" s="T142">n-n:poss-n:case</ta>
            <ta e="T144" id="Seg_2024" s="T143">v-v:cvb</ta>
            <ta e="T145" id="Seg_2025" s="T144">adv</ta>
            <ta e="T146" id="Seg_2026" s="T145">v-v:tense-v:pred.pn</ta>
            <ta e="T147" id="Seg_2027" s="T146">adv</ta>
            <ta e="T148" id="Seg_2028" s="T147">v-v:cvb</ta>
            <ta e="T149" id="Seg_2029" s="T148">n-n:case</ta>
            <ta e="T150" id="Seg_2030" s="T149">n-n:(poss)-n:case</ta>
            <ta e="T151" id="Seg_2031" s="T150">v-v:tense-v:pred.pn</ta>
            <ta e="T152" id="Seg_2032" s="T151">n-n:case</ta>
            <ta e="T153" id="Seg_2033" s="T152">n-n:poss-n:case</ta>
            <ta e="T154" id="Seg_2034" s="T153">v-v:tense-v:pred.pn</ta>
            <ta e="T155" id="Seg_2035" s="T154">conj</ta>
            <ta e="T156" id="Seg_2036" s="T155">v-v:tense-v:pred.pn</ta>
            <ta e="T157" id="Seg_2037" s="T156">n-n:poss-n:case</ta>
            <ta e="T158" id="Seg_2038" s="T157">n-n:poss-n:case</ta>
            <ta e="T159" id="Seg_2039" s="T158">v-v:cvb</ta>
            <ta e="T160" id="Seg_2040" s="T159">post</ta>
            <ta e="T161" id="Seg_2041" s="T160">v-v:ptcp</ta>
            <ta e="T162" id="Seg_2042" s="T161">n-n:case</ta>
            <ta e="T163" id="Seg_2043" s="T162">v-v&gt;v-v:cvb</ta>
            <ta e="T164" id="Seg_2044" s="T163">v-v:tense-v:pred.pn</ta>
            <ta e="T165" id="Seg_2045" s="T164">dempro-pro:case</ta>
            <ta e="T166" id="Seg_2046" s="T165">n-n:poss-n:case</ta>
            <ta e="T167" id="Seg_2047" s="T166">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T168" id="Seg_2048" s="T167">cardnum</ta>
            <ta e="T169" id="Seg_2049" s="T168">n-n:case</ta>
            <ta e="T170" id="Seg_2050" s="T169">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T171" id="Seg_2051" s="T170">post</ta>
            <ta e="T172" id="Seg_2052" s="T171">v-v:tense-v:pred.pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_2053" s="T0">n</ta>
            <ta e="T2" id="Seg_2054" s="T1">adj</ta>
            <ta e="T3" id="Seg_2055" s="T2">cop</ta>
            <ta e="T4" id="Seg_2056" s="T3">dempro</ta>
            <ta e="T5" id="Seg_2057" s="T4">n</ta>
            <ta e="T6" id="Seg_2058" s="T5">n</ta>
            <ta e="T7" id="Seg_2059" s="T6">v</ta>
            <ta e="T8" id="Seg_2060" s="T7">n</ta>
            <ta e="T9" id="Seg_2061" s="T8">adv</ta>
            <ta e="T10" id="Seg_2062" s="T9">adv</ta>
            <ta e="T11" id="Seg_2063" s="T10">v</ta>
            <ta e="T12" id="Seg_2064" s="T11">aux</ta>
            <ta e="T13" id="Seg_2065" s="T12">dempro</ta>
            <ta e="T14" id="Seg_2066" s="T13">n</ta>
            <ta e="T15" id="Seg_2067" s="T14">n</ta>
            <ta e="T16" id="Seg_2068" s="T15">v</ta>
            <ta e="T17" id="Seg_2069" s="T16">n</ta>
            <ta e="T18" id="Seg_2070" s="T17">v</ta>
            <ta e="T19" id="Seg_2071" s="T18">post</ta>
            <ta e="T20" id="Seg_2072" s="T19">adv</ta>
            <ta e="T21" id="Seg_2073" s="T20">pers</ta>
            <ta e="T22" id="Seg_2074" s="T21">n</ta>
            <ta e="T23" id="Seg_2075" s="T22">n</ta>
            <ta e="T24" id="Seg_2076" s="T23">v</ta>
            <ta e="T25" id="Seg_2077" s="T24">adv</ta>
            <ta e="T26" id="Seg_2078" s="T25">n</ta>
            <ta e="T27" id="Seg_2079" s="T26">n</ta>
            <ta e="T28" id="Seg_2080" s="T27">adj</ta>
            <ta e="T29" id="Seg_2081" s="T28">n</ta>
            <ta e="T30" id="Seg_2082" s="T29">adv</ta>
            <ta e="T31" id="Seg_2083" s="T30">v</ta>
            <ta e="T32" id="Seg_2084" s="T31">cardnum</ta>
            <ta e="T33" id="Seg_2085" s="T32">n</ta>
            <ta e="T34" id="Seg_2086" s="T33">v</ta>
            <ta e="T35" id="Seg_2087" s="T34">n</ta>
            <ta e="T36" id="Seg_2088" s="T35">v</ta>
            <ta e="T37" id="Seg_2089" s="T36">n</ta>
            <ta e="T38" id="Seg_2090" s="T37">v</ta>
            <ta e="T39" id="Seg_2091" s="T38">n</ta>
            <ta e="T40" id="Seg_2092" s="T39">v</ta>
            <ta e="T41" id="Seg_2093" s="T40">conj</ta>
            <ta e="T42" id="Seg_2094" s="T41">v</ta>
            <ta e="T43" id="Seg_2095" s="T42">aux</ta>
            <ta e="T44" id="Seg_2096" s="T43">n</ta>
            <ta e="T45" id="Seg_2097" s="T44">v</ta>
            <ta e="T46" id="Seg_2098" s="T45">conj</ta>
            <ta e="T47" id="Seg_2099" s="T46">adv</ta>
            <ta e="T48" id="Seg_2100" s="T47">v</ta>
            <ta e="T49" id="Seg_2101" s="T48">dempro</ta>
            <ta e="T50" id="Seg_2102" s="T49">dempro</ta>
            <ta e="T51" id="Seg_2103" s="T50">n</ta>
            <ta e="T52" id="Seg_2104" s="T51">cardnum</ta>
            <ta e="T53" id="Seg_2105" s="T52">n</ta>
            <ta e="T54" id="Seg_2106" s="T53">v</ta>
            <ta e="T55" id="Seg_2107" s="T54">ptcl</ta>
            <ta e="T56" id="Seg_2108" s="T55">n</ta>
            <ta e="T57" id="Seg_2109" s="T56">v</ta>
            <ta e="T58" id="Seg_2110" s="T57">n</ta>
            <ta e="T59" id="Seg_2111" s="T58">v</ta>
            <ta e="T60" id="Seg_2112" s="T59">n</ta>
            <ta e="T61" id="Seg_2113" s="T60">v</ta>
            <ta e="T62" id="Seg_2114" s="T61">v</ta>
            <ta e="T63" id="Seg_2115" s="T62">v</ta>
            <ta e="T64" id="Seg_2116" s="T63">adv</ta>
            <ta e="T65" id="Seg_2117" s="T64">n</ta>
            <ta e="T66" id="Seg_2118" s="T65">ptcl</ta>
            <ta e="T67" id="Seg_2119" s="T66">v</ta>
            <ta e="T68" id="Seg_2120" s="T67">adj</ta>
            <ta e="T69" id="Seg_2121" s="T68">cop</ta>
            <ta e="T70" id="Seg_2122" s="T69">v</ta>
            <ta e="T71" id="Seg_2123" s="T70">n</ta>
            <ta e="T72" id="Seg_2124" s="T71">v</ta>
            <ta e="T73" id="Seg_2125" s="T72">adv</ta>
            <ta e="T74" id="Seg_2126" s="T73">v</ta>
            <ta e="T75" id="Seg_2127" s="T74">n</ta>
            <ta e="T76" id="Seg_2128" s="T75">cop</ta>
            <ta e="T77" id="Seg_2129" s="T76">adj</ta>
            <ta e="T78" id="Seg_2130" s="T77">cop</ta>
            <ta e="T79" id="Seg_2131" s="T78">adv</ta>
            <ta e="T80" id="Seg_2132" s="T79">ptcl</ta>
            <ta e="T81" id="Seg_2133" s="T80">v</ta>
            <ta e="T82" id="Seg_2134" s="T81">n</ta>
            <ta e="T83" id="Seg_2135" s="T82">v</ta>
            <ta e="T84" id="Seg_2136" s="T83">v</ta>
            <ta e="T85" id="Seg_2137" s="T84">v</ta>
            <ta e="T86" id="Seg_2138" s="T85">que</ta>
            <ta e="T87" id="Seg_2139" s="T86">ptcl</ta>
            <ta e="T88" id="Seg_2140" s="T87">v</ta>
            <ta e="T89" id="Seg_2141" s="T88">adv</ta>
            <ta e="T90" id="Seg_2142" s="T89">adv</ta>
            <ta e="T91" id="Seg_2143" s="T90">v</ta>
            <ta e="T92" id="Seg_2144" s="T91">v</ta>
            <ta e="T93" id="Seg_2145" s="T92">dempro</ta>
            <ta e="T94" id="Seg_2146" s="T93">n</ta>
            <ta e="T95" id="Seg_2147" s="T94">n</ta>
            <ta e="T96" id="Seg_2148" s="T95">adj</ta>
            <ta e="T97" id="Seg_2149" s="T96">n</ta>
            <ta e="T98" id="Seg_2150" s="T97">cop</ta>
            <ta e="T99" id="Seg_2151" s="T98">dempro</ta>
            <ta e="T100" id="Seg_2152" s="T99">cardnum</ta>
            <ta e="T101" id="Seg_2153" s="T100">n</ta>
            <ta e="T102" id="Seg_2154" s="T101">n</ta>
            <ta e="T103" id="Seg_2155" s="T102">v</ta>
            <ta e="T104" id="Seg_2156" s="T103">n</ta>
            <ta e="T105" id="Seg_2157" s="T104">ptcl</ta>
            <ta e="T106" id="Seg_2158" s="T105">dempro</ta>
            <ta e="T107" id="Seg_2159" s="T106">v</ta>
            <ta e="T108" id="Seg_2160" s="T107">ptcl</ta>
            <ta e="T109" id="Seg_2161" s="T108">n</ta>
            <ta e="T110" id="Seg_2162" s="T109">n</ta>
            <ta e="T111" id="Seg_2163" s="T110">n</ta>
            <ta e="T112" id="Seg_2164" s="T111">v</ta>
            <ta e="T113" id="Seg_2165" s="T112">v</ta>
            <ta e="T114" id="Seg_2166" s="T113">n</ta>
            <ta e="T115" id="Seg_2167" s="T114">v</ta>
            <ta e="T116" id="Seg_2168" s="T115">cardnum</ta>
            <ta e="T117" id="Seg_2169" s="T116">n</ta>
            <ta e="T118" id="Seg_2170" s="T117">v</ta>
            <ta e="T119" id="Seg_2171" s="T118">n</ta>
            <ta e="T120" id="Seg_2172" s="T119">n</ta>
            <ta e="T121" id="Seg_2173" s="T120">n</ta>
            <ta e="T122" id="Seg_2174" s="T121">n</ta>
            <ta e="T123" id="Seg_2175" s="T122">v</ta>
            <ta e="T124" id="Seg_2176" s="T123">n</ta>
            <ta e="T125" id="Seg_2177" s="T124">v</ta>
            <ta e="T126" id="Seg_2178" s="T125">n</ta>
            <ta e="T127" id="Seg_2179" s="T126">v</ta>
            <ta e="T128" id="Seg_2180" s="T127">v</ta>
            <ta e="T129" id="Seg_2181" s="T128">adv</ta>
            <ta e="T130" id="Seg_2182" s="T129">dempro</ta>
            <ta e="T131" id="Seg_2183" s="T130">v</ta>
            <ta e="T132" id="Seg_2184" s="T131">n</ta>
            <ta e="T133" id="Seg_2185" s="T132">adv</ta>
            <ta e="T134" id="Seg_2186" s="T133">v</ta>
            <ta e="T135" id="Seg_2187" s="T134">n</ta>
            <ta e="T136" id="Seg_2188" s="T135">n</ta>
            <ta e="T137" id="Seg_2189" s="T136">ptcl</ta>
            <ta e="T138" id="Seg_2190" s="T137">emphpro</ta>
            <ta e="T139" id="Seg_2191" s="T138">v</ta>
            <ta e="T140" id="Seg_2192" s="T139">n</ta>
            <ta e="T141" id="Seg_2193" s="T140">n</ta>
            <ta e="T142" id="Seg_2194" s="T141">n</ta>
            <ta e="T143" id="Seg_2195" s="T142">n</ta>
            <ta e="T144" id="Seg_2196" s="T143">v</ta>
            <ta e="T145" id="Seg_2197" s="T144">adv</ta>
            <ta e="T146" id="Seg_2198" s="T145">v</ta>
            <ta e="T147" id="Seg_2199" s="T146">adv</ta>
            <ta e="T148" id="Seg_2200" s="T147">v</ta>
            <ta e="T149" id="Seg_2201" s="T148">n</ta>
            <ta e="T150" id="Seg_2202" s="T149">n</ta>
            <ta e="T151" id="Seg_2203" s="T150">v</ta>
            <ta e="T152" id="Seg_2204" s="T151">n</ta>
            <ta e="T153" id="Seg_2205" s="T152">n</ta>
            <ta e="T154" id="Seg_2206" s="T153">v</ta>
            <ta e="T155" id="Seg_2207" s="T154">conj</ta>
            <ta e="T156" id="Seg_2208" s="T155">v</ta>
            <ta e="T157" id="Seg_2209" s="T156">n</ta>
            <ta e="T158" id="Seg_2210" s="T157">n</ta>
            <ta e="T159" id="Seg_2211" s="T158">v</ta>
            <ta e="T160" id="Seg_2212" s="T159">post</ta>
            <ta e="T161" id="Seg_2213" s="T160">v</ta>
            <ta e="T162" id="Seg_2214" s="T161">n</ta>
            <ta e="T163" id="Seg_2215" s="T162">v</ta>
            <ta e="T164" id="Seg_2216" s="T163">aux</ta>
            <ta e="T165" id="Seg_2217" s="T164">dempro</ta>
            <ta e="T166" id="Seg_2218" s="T165">n</ta>
            <ta e="T167" id="Seg_2219" s="T166">v</ta>
            <ta e="T168" id="Seg_2220" s="T167">cardnum</ta>
            <ta e="T169" id="Seg_2221" s="T168">n</ta>
            <ta e="T170" id="Seg_2222" s="T169">cop</ta>
            <ta e="T171" id="Seg_2223" s="T170">post</ta>
            <ta e="T172" id="Seg_2224" s="T171">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_2225" s="T0">np:Poss</ta>
            <ta e="T2" id="Seg_2226" s="T1">np:Th</ta>
            <ta e="T6" id="Seg_2227" s="T5">np.h:St</ta>
            <ta e="T8" id="Seg_2228" s="T7">np.h:P</ta>
            <ta e="T9" id="Seg_2229" s="T8">adv:Time</ta>
            <ta e="T10" id="Seg_2230" s="T9">adv:G</ta>
            <ta e="T12" id="Seg_2231" s="T10">0.3.h:A</ta>
            <ta e="T15" id="Seg_2232" s="T14">np.h:A</ta>
            <ta e="T17" id="Seg_2233" s="T16">np:P</ta>
            <ta e="T20" id="Seg_2234" s="T19">adv:Time</ta>
            <ta e="T21" id="Seg_2235" s="T20">pro.h:Poss</ta>
            <ta e="T22" id="Seg_2236" s="T21">np.h:Poss</ta>
            <ta e="T23" id="Seg_2237" s="T22">np.h:Th</ta>
            <ta e="T27" id="Seg_2238" s="T26">np.h:A</ta>
            <ta e="T31" id="Seg_2239" s="T30">0.3.h:A</ta>
            <ta e="T33" id="Seg_2240" s="T32">np.h:A</ta>
            <ta e="T35" id="Seg_2241" s="T34">np:P</ta>
            <ta e="T37" id="Seg_2242" s="T36">np.h:Th</ta>
            <ta e="T38" id="Seg_2243" s="T37">0.3.h:A</ta>
            <ta e="T39" id="Seg_2244" s="T38">np:G</ta>
            <ta e="T40" id="Seg_2245" s="T39">0.3.h:A</ta>
            <ta e="T43" id="Seg_2246" s="T41">0.3.h:A</ta>
            <ta e="T44" id="Seg_2247" s="T43">np:G</ta>
            <ta e="T45" id="Seg_2248" s="T44">0.3.h:A</ta>
            <ta e="T48" id="Seg_2249" s="T47">0.3.h:A</ta>
            <ta e="T51" id="Seg_2250" s="T50">np:G</ta>
            <ta e="T53" id="Seg_2251" s="T52">np.h:A</ta>
            <ta e="T57" id="Seg_2252" s="T56">0.3.h:A</ta>
            <ta e="T58" id="Seg_2253" s="T57">np.h:A</ta>
            <ta e="T60" id="Seg_2254" s="T59">np:P</ta>
            <ta e="T63" id="Seg_2255" s="T62">0.3.h:A</ta>
            <ta e="T69" id="Seg_2256" s="T68">0.2.h:E</ta>
            <ta e="T71" id="Seg_2257" s="T70">np:P</ta>
            <ta e="T72" id="Seg_2258" s="T71">0.2.h:A</ta>
            <ta e="T78" id="Seg_2259" s="T77">0.2.h:E</ta>
            <ta e="T81" id="Seg_2260" s="T80">0.2.h:A</ta>
            <ta e="T82" id="Seg_2261" s="T81">np.h:A</ta>
            <ta e="T86" id="Seg_2262" s="T85">pro:P</ta>
            <ta e="T88" id="Seg_2263" s="T87">0.3.h:A</ta>
            <ta e="T92" id="Seg_2264" s="T91">0.3.h:Th</ta>
            <ta e="T94" id="Seg_2265" s="T93">np.h:Poss</ta>
            <ta e="T95" id="Seg_2266" s="T94">np.h:Th</ta>
            <ta e="T102" id="Seg_2267" s="T101">np.h:P</ta>
            <ta e="T103" id="Seg_2268" s="T102">0.3.h:A</ta>
            <ta e="T106" id="Seg_2269" s="T105">pro.h:A</ta>
            <ta e="T109" id="Seg_2270" s="T108">0.2.h:Poss np.h:Th</ta>
            <ta e="T111" id="Seg_2271" s="T110">np.h:A</ta>
            <ta e="T114" id="Seg_2272" s="T113">np.h:Th</ta>
            <ta e="T118" id="Seg_2273" s="T117">0.3.h:E</ta>
            <ta e="T120" id="Seg_2274" s="T119">np:Th</ta>
            <ta e="T121" id="Seg_2275" s="T120">np.h:Poss</ta>
            <ta e="T122" id="Seg_2276" s="T121">np:St</ta>
            <ta e="T124" id="Seg_2277" s="T123">np:P</ta>
            <ta e="T125" id="Seg_2278" s="T124">0.2.h:A</ta>
            <ta e="T126" id="Seg_2279" s="T125">np.h:Th</ta>
            <ta e="T127" id="Seg_2280" s="T126">0.2.h:A</ta>
            <ta e="T130" id="Seg_2281" s="T129">pro:P</ta>
            <ta e="T131" id="Seg_2282" s="T130">0.3.h:A</ta>
            <ta e="T132" id="Seg_2283" s="T131">np.h:A</ta>
            <ta e="T135" id="Seg_2284" s="T134">np.h:Poss</ta>
            <ta e="T136" id="Seg_2285" s="T135">np.h:A</ta>
            <ta e="T146" id="Seg_2286" s="T145">0.3.h:A</ta>
            <ta e="T150" id="Seg_2287" s="T149">np.h:A</ta>
            <ta e="T152" id="Seg_2288" s="T151">np.h:Th</ta>
            <ta e="T153" id="Seg_2289" s="T152">np:G</ta>
            <ta e="T154" id="Seg_2290" s="T153">0.3.h:A</ta>
            <ta e="T156" id="Seg_2291" s="T155">0.3.h:A</ta>
            <ta e="T158" id="Seg_2292" s="T157">np:G</ta>
            <ta e="T162" id="Seg_2293" s="T161">np:G</ta>
            <ta e="T164" id="Seg_2294" s="T162">0.3.h:A</ta>
            <ta e="T165" id="Seg_2295" s="T164">pro.h:Th</ta>
            <ta e="T172" id="Seg_2296" s="T171">0.3.h:E</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1" id="Seg_2297" s="T0">np:S</ta>
            <ta e="T2" id="Seg_2298" s="T1">adj:pred</ta>
            <ta e="T3" id="Seg_2299" s="T2">cop</ta>
            <ta e="T7" id="Seg_2300" s="T3">s:adv</ta>
            <ta e="T8" id="Seg_2301" s="T7">np.h:O</ta>
            <ta e="T12" id="Seg_2302" s="T10">0.3.h:S v:pred</ta>
            <ta e="T15" id="Seg_2303" s="T14">np.h:S</ta>
            <ta e="T16" id="Seg_2304" s="T15">v:pred</ta>
            <ta e="T19" id="Seg_2305" s="T16">s:temp</ta>
            <ta e="T23" id="Seg_2306" s="T22">np.h:O</ta>
            <ta e="T24" id="Seg_2307" s="T23">v:pred</ta>
            <ta e="T27" id="Seg_2308" s="T26">np.h:S</ta>
            <ta e="T31" id="Seg_2309" s="T29">s:temp</ta>
            <ta e="T33" id="Seg_2310" s="T32">np.h:S</ta>
            <ta e="T34" id="Seg_2311" s="T33">v:pred</ta>
            <ta e="T36" id="Seg_2312" s="T34">s:adv</ta>
            <ta e="T37" id="Seg_2313" s="T36">np.h:O</ta>
            <ta e="T38" id="Seg_2314" s="T37">0.3.h:S v:pred</ta>
            <ta e="T40" id="Seg_2315" s="T39">0.3.h:S v:pred</ta>
            <ta e="T43" id="Seg_2316" s="T41">0.3.h:S v:pred</ta>
            <ta e="T45" id="Seg_2317" s="T44">0.3.h:S v:pred</ta>
            <ta e="T48" id="Seg_2318" s="T47">0.3.h:S v:pred</ta>
            <ta e="T53" id="Seg_2319" s="T52">np.h:S</ta>
            <ta e="T54" id="Seg_2320" s="T53">v:pred</ta>
            <ta e="T57" id="Seg_2321" s="T56">0.3.h:S v:pred</ta>
            <ta e="T58" id="Seg_2322" s="T57">np.h:S</ta>
            <ta e="T59" id="Seg_2323" s="T58">v:pred</ta>
            <ta e="T61" id="Seg_2324" s="T59">s:purp</ta>
            <ta e="T63" id="Seg_2325" s="T61">s:temp</ta>
            <ta e="T69" id="Seg_2326" s="T63">s:cond</ta>
            <ta e="T70" id="Seg_2327" s="T69">s:rel</ta>
            <ta e="T71" id="Seg_2328" s="T70">np:O</ta>
            <ta e="T72" id="Seg_2329" s="T71">0.2.h:S v:pred</ta>
            <ta e="T78" id="Seg_2330" s="T72">s:cond</ta>
            <ta e="T81" id="Seg_2331" s="T80">0.2.h:S v:pred</ta>
            <ta e="T82" id="Seg_2332" s="T81">np.h:S</ta>
            <ta e="T83" id="Seg_2333" s="T82">s:adv</ta>
            <ta e="T85" id="Seg_2334" s="T84">v:pred</ta>
            <ta e="T86" id="Seg_2335" s="T85">pro:O</ta>
            <ta e="T88" id="Seg_2336" s="T87">0.3.h:S v:pred</ta>
            <ta e="T91" id="Seg_2337" s="T90">s:adv</ta>
            <ta e="T92" id="Seg_2338" s="T91">0.3.h:S v:pred</ta>
            <ta e="T95" id="Seg_2339" s="T94">np.h:S</ta>
            <ta e="T97" id="Seg_2340" s="T96">n:pred</ta>
            <ta e="T98" id="Seg_2341" s="T97">cop</ta>
            <ta e="T102" id="Seg_2342" s="T101">np.h:O</ta>
            <ta e="T103" id="Seg_2343" s="T102">0.3.h:S v:pred</ta>
            <ta e="T106" id="Seg_2344" s="T105">pro.h:S</ta>
            <ta e="T107" id="Seg_2345" s="T106">v:pred</ta>
            <ta e="T111" id="Seg_2346" s="T110">np.h:S</ta>
            <ta e="T112" id="Seg_2347" s="T111">v:pred</ta>
            <ta e="T115" id="Seg_2348" s="T113">s:temp</ta>
            <ta e="T118" id="Seg_2349" s="T117">0.3.h:S v:pred</ta>
            <ta e="T120" id="Seg_2350" s="T119">np:O</ta>
            <ta e="T122" id="Seg_2351" s="T121">np:S</ta>
            <ta e="T123" id="Seg_2352" s="T122">v:pred</ta>
            <ta e="T124" id="Seg_2353" s="T123">np:O</ta>
            <ta e="T125" id="Seg_2354" s="T124">0.2.h:S v:pred</ta>
            <ta e="T126" id="Seg_2355" s="T125">np.h:O</ta>
            <ta e="T127" id="Seg_2356" s="T126">0.2.h:S v:pred</ta>
            <ta e="T130" id="Seg_2357" s="T129">pro:O</ta>
            <ta e="T131" id="Seg_2358" s="T130">0.3.h:S v:pred</ta>
            <ta e="T132" id="Seg_2359" s="T131">np.h:S</ta>
            <ta e="T134" id="Seg_2360" s="T133">v:pred</ta>
            <ta e="T136" id="Seg_2361" s="T135">np.h:S</ta>
            <ta e="T139" id="Seg_2362" s="T138">v:pred</ta>
            <ta e="T146" id="Seg_2363" s="T145">0.3.h:S v:pred</ta>
            <ta e="T148" id="Seg_2364" s="T147">s:adv</ta>
            <ta e="T150" id="Seg_2365" s="T149">np.h:S</ta>
            <ta e="T151" id="Seg_2366" s="T150">v:pred</ta>
            <ta e="T152" id="Seg_2367" s="T151">np.h:O</ta>
            <ta e="T154" id="Seg_2368" s="T153">0.3.h:S v:pred</ta>
            <ta e="T156" id="Seg_2369" s="T155">0.3.h:S v:pred</ta>
            <ta e="T160" id="Seg_2370" s="T156">s:temp</ta>
            <ta e="T161" id="Seg_2371" s="T160">s:rel</ta>
            <ta e="T164" id="Seg_2372" s="T162">0.3.h:S v:pred</ta>
            <ta e="T165" id="Seg_2373" s="T164">pro.h:O</ta>
            <ta e="T171" id="Seg_2374" s="T165">s:temp</ta>
            <ta e="T172" id="Seg_2375" s="T171">0.3.h:S v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="Top" tierref="Top" />
         <annotation name="Foc" tierref="Foc" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T23" id="Seg_2376" s="T22">RUS:core</ta>
            <ta e="T41" id="Seg_2377" s="T40">RUS:gram</ta>
            <ta e="T46" id="Seg_2378" s="T45">RUS:gram</ta>
            <ta e="T90" id="Seg_2379" s="T89">RUS:core</ta>
            <ta e="T129" id="Seg_2380" s="T128">RUS:core</ta>
            <ta e="T155" id="Seg_2381" s="T154">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon">
            <ta e="T23" id="Seg_2382" s="T22">medVins Vsub</ta>
            <ta e="T46" id="Seg_2383" s="T45">Vsub</ta>
            <ta e="T90" id="Seg_2384" s="T89">Vsub</ta>
            <ta e="T129" id="Seg_2385" s="T128">Vsub</ta>
         </annotation>
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T23" id="Seg_2386" s="T22">dir:infl</ta>
            <ta e="T41" id="Seg_2387" s="T40">dir:bare</ta>
            <ta e="T46" id="Seg_2388" s="T45">dir:bare</ta>
            <ta e="T90" id="Seg_2389" s="T89">dir:bare</ta>
            <ta e="T129" id="Seg_2390" s="T128">dir:bare</ta>
            <ta e="T155" id="Seg_2391" s="T154">dir:bare</ta>
         </annotation>
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T3" id="Seg_2392" s="T0">The earth has its own master.</ta>
            <ta e="T12" id="Seg_2393" s="T3">As [people] were afraid of that earth master, children were not allowed to go out in the evening.</ta>
            <ta e="T19" id="Seg_2394" s="T12">That earth master harnesses foxes and drives [around].</ta>
            <ta e="T27" id="Seg_2395" s="T19">Long ago such an earth master carried away my mother's brother.</ta>
            <ta e="T43" id="Seg_2396" s="T27">As it was playing outside, an old man with harnessed foxes came to the child, took it, threw it into the sledge and drove away.</ta>
            <ta e="T48" id="Seg_2397" s="T43">He threw it into his house and went out again.</ta>
            <ta e="T54" id="Seg_2398" s="T48">There one old woman came to that child.</ta>
            <ta e="T72" id="Seg_2399" s="T54">"Well, my dear child", she said, "your grandfather went out to unhitch the dogs. When he comes and gives you something to eat, if you want to go back home, don't eat what he will give to you.</ta>
            <ta e="T81" id="Seg_2400" s="T72">If you want to stay here and belong to the family then just eat."</ta>
            <ta e="T88" id="Seg_2401" s="T81">The grandfather came in and hosted [the child] in vain, it didn't eat anything.</ta>
            <ta e="T92" id="Seg_2402" s="T88">It was sitting the whole time without eating.</ta>
            <ta e="T98" id="Seg_2403" s="T92">This child's father was a russian magician.</ta>
            <ta e="T105" id="Seg_2404" s="T98">Three times he let shamans shamanize, they couldn't help.</ta>
            <ta e="T108" id="Seg_2405" s="T105">They just told him:</ta>
            <ta e="T113" id="Seg_2406" s="T108">"An earth master took your child", they said.</ta>
            <ta e="T123" id="Seg_2407" s="T113">As the child was sitting there, it heard three times the sound of the shaman drum, a shaman's voice was heard:</ta>
            <ta e="T128" id="Seg_2408" s="T123">"Open the door, give the child", it said.</ta>
            <ta e="T134" id="Seg_2409" s="T128">They don't open the door, the shamans go back having done nothing.</ta>
            <ta e="T146" id="Seg_2410" s="T134">Then the child's father gets ready himself, he goes outside and pees onto the house of the earth master.</ta>
            <ta e="T156" id="Seg_2411" s="T146">The earth master gets angry, takes the child, throws it into the sledge and drives off.</ta>
            <ta e="T164" id="Seg_2412" s="T156">He comes to a house and seats it onto a log lying there.</ta>
            <ta e="T172" id="Seg_2413" s="T164">It is found three month after he had got lost.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T3" id="Seg_2414" s="T0">Die Erde hat ihre Geister.</ta>
            <ta e="T12" id="Seg_2415" s="T3">Da man vor diesem Erdgeist Angst hatte, ließ man die Kinder abends nicht hinaus. </ta>
            <ta e="T19" id="Seg_2416" s="T12">Dieser Erdgeist spannt Füchse an und fährt [herum].</ta>
            <ta e="T27" id="Seg_2417" s="T19">Vor langer Zeit hat so ein Erdgeist den Bruder meiner Mutter mitgenommen.</ta>
            <ta e="T43" id="Seg_2418" s="T27">Als es draußen spielte, kam ein alter Mann, der Füchse angespannt hatte, zu dem Kind, er nahm das Kind, warf es in den Schlitten und fuhr davon.</ta>
            <ta e="T48" id="Seg_2419" s="T43">Er warf es in sein Haus und ging selber wieder hinaus.</ta>
            <ta e="T54" id="Seg_2420" s="T48">Da kam zu diesem Kind eine alte Frau.</ta>
            <ta e="T72" id="Seg_2421" s="T54">"Nun, mein Kindchen", sagte sie, "der Großvater ist hinausgegangen, um die Hunde auszuspannen; wenn er kommt und dir zu essen gibt, wenn du den Wunsch hast von hier nach Hause zu kommen, dann iss nicht, was er dir gibt.</ta>
            <ta e="T81" id="Seg_2422" s="T72">Wenn du hier bleiben möchtest und zur Familie gehören möchtest, dann iss nur."</ta>
            <ta e="T88" id="Seg_2423" s="T81">Der Großvater kam herein und bewirtete es vergeblich, jenes aß nichts.</ta>
            <ta e="T92" id="Seg_2424" s="T88">So saß es die ganze Zeit ohne etwas zu essen.</ta>
            <ta e="T98" id="Seg_2425" s="T92">Der Vater dieses Kindes war ein russischer Zauberer.</ta>
            <ta e="T105" id="Seg_2426" s="T98">Er ließ drei mal Schamanen schamanisieren, sie waren keine Hilfe.</ta>
            <ta e="T108" id="Seg_2427" s="T105">Sie erzählten ihm nur:</ta>
            <ta e="T113" id="Seg_2428" s="T108">"Dein Kind nahm ein Erdgeist", sagten sie.</ta>
            <ta e="T123" id="Seg_2429" s="T113">Als das Kind da saß, hörte es drei mal das Geräusch der Schamanentrommel, die Stimme eines Schamanen war zu hören:</ta>
            <ta e="T128" id="Seg_2430" s="T123">"Öffnet die Tür, gebt das Kind", sagte sie.</ta>
            <ta e="T134" id="Seg_2431" s="T128">Es wird nicht geöffnet, die Schamanen gehen unverrichteter Dinge.</ta>
            <ta e="T146" id="Seg_2432" s="T134">Da macht sich der Vater des Kindes selbst fertig, er geht nach draußen und pinkelt auf das Haus des Erdgeistes.</ta>
            <ta e="T156" id="Seg_2433" s="T146">Der Erdgeist wird böse, nimmt das Kind, wirft es in seinen Schlitten und fährt davon.</ta>
            <ta e="T164" id="Seg_2434" s="T156">Er kommt zu einer Hütte, er setzt es auf Holz, das dort herumliegt.</ta>
            <ta e="T172" id="Seg_2435" s="T164">Es wird drei Monate, nachdem er von zuhause verschwunden war, gefunden.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T3" id="Seg_2436" s="T0">Каждая местность имеет своего духа-хозяина.</ta>
            <ta e="T12" id="Seg_2437" s="T3">Боясь того духа-хозяина земли, вечером не разрешали детям выходить во двор.</ta>
            <ta e="T19" id="Seg_2438" s="T12">Тот дух-хозяин земли ездит, запрягая [в нарты] лисиц.</ta>
            <ta e="T27" id="Seg_2439" s="T19">В старину брата моей матери увозил такой дух-хозяин земли.</ta>
            <ta e="T43" id="Seg_2440" s="T27">Когда довольно большой уже ребенок играл во дворе, подъехал к нему один старик на нарте с упряжкой из лисиц, взял ребенка, бросил на свою нарту и умчался.</ta>
            <ta e="T48" id="Seg_2441" s="T43">Закинул его в свой дом, а сам тут же вышел.</ta>
            <ta e="T54" id="Seg_2442" s="T48">Тут к этому ребенку одна старушка подошла.</ta>
            <ta e="T72" id="Seg_2443" s="T54">"Ну, милый", сказала, "это дедушка твой вышел своих собак распрягать. Когда войдет и станет тебя угощать, если только хочешь вернуться домой, то, что он тебе предложит, не ешь.</ta>
            <ta e="T81" id="Seg_2444" s="T72">Если хочешь здесь остаться и стать его ребенком, тогда только ешь."</ta>
            <ta e="T88" id="Seg_2445" s="T81">Дедушка вошел и стал угощать, ничего [тот] не попробовал.</ta>
            <ta e="T92" id="Seg_2446" s="T88">Так век-то сидел, совсем не притрагиваясь.</ta>
            <ta e="T98" id="Seg_2447" s="T92">Отец этого ребенка был русским колдуном.</ta>
            <ta e="T105" id="Seg_2448" s="T98">[Он] трижды приглашал шаманов камлать — пользы не было.</ta>
            <ta e="T108" id="Seg_2449" s="T105">Те лишь поведали ему:</ta>
            <ta e="T113" id="Seg_2450" s="T108">"Ребенка твоего дух-хозяин земли забрал", сказали.</ta>
            <ta e="T123" id="Seg_2451" s="T113">Когда ребенок там сидел, трижды грохот бубна раздавался, слышался голос шамана:</ta>
            <ta e="T128" id="Seg_2452" s="T123">"Откройте дверь, отдайте ребенка", говорил.</ta>
            <ta e="T134" id="Seg_2453" s="T128">Век-то не открывают, шаманы попусту возвращаются.</ta>
            <ta e="T146" id="Seg_2454" s="T134">Вот отец ребенка сам сонирается, на жилище духа-хозяина, земли взобравшись, мочится.</ta>
            <ta e="T156" id="Seg_2455" s="T146">Рассердившись, дух-хозяин земли хбатает ребенка, бросает на свои нарты и мчится.</ta>
            <ta e="T164" id="Seg_2456" s="T156">Подъехав к жилищу, садит его на бревно, что лежит там.</ta>
            <ta e="T172" id="Seg_2457" s="T164">Его находят там через три месяца после того, как он пропал из дому.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T3" id="Seg_2458" s="T0">Каждая местность обычно имеет своего духа-хозяина.</ta>
            <ta e="T12" id="Seg_2459" s="T3">Боясь того духа-хозяина земли, вечером не разрешали детям выходить во двор.</ta>
            <ta e="T19" id="Seg_2460" s="T12">Тот дух-хозяин земли ездит, запрягая [в нарты] лисиц.</ta>
            <ta e="T27" id="Seg_2461" s="T19">В старину брата моей матери увозил такой дух-хозяин земли.</ta>
            <ta e="T43" id="Seg_2462" s="T27">Когда довольно большой уже ребенок играл во дворе, подъехал к нему один старик на нарте с упряжкой из лисиц, взял ребенка, бросил на свою нарту и умчался.</ta>
            <ta e="T48" id="Seg_2463" s="T43">Закинул его в свой дом, а сам тут же вышел.</ta>
            <ta e="T54" id="Seg_2464" s="T48">Тут к этому ребенку одна старушка подошла.</ta>
            <ta e="T72" id="Seg_2465" s="T54">— Ну, милый, — сказала, — это дедушка твой вышел своих собак распрягать. Когда войдет и станет тебя угощать, если только хочешь вернуться домой, то, что он тебе предложит, не ешь.</ta>
            <ta e="T81" id="Seg_2466" s="T72">Если хочешь здесь остаться и стать его ребенком, тогда только ешь.</ta>
            <ta e="T88" id="Seg_2467" s="T81">Дедушка вошел и стал угощать, ничего [тот] не попробовал.</ta>
            <ta e="T92" id="Seg_2468" s="T88">Так век-то сидел, совсем не притрагиваясь.</ta>
            <ta e="T98" id="Seg_2469" s="T92">Отец этого ребенка был русским колдуном.</ta>
            <ta e="T105" id="Seg_2470" s="T98">[Он] трижды приглашал шаманов камлать — пользы не было.</ta>
            <ta e="T108" id="Seg_2471" s="T105">Те лишь поведали ему:</ta>
            <ta e="T113" id="Seg_2472" s="T108">— Ребенка твоего дух-хозяин земли забрал.</ta>
            <ta e="T123" id="Seg_2473" s="T113">Когда ребенок там сидел, трижды грохот бубна раздавался, слышался голос шамана:</ta>
            <ta e="T128" id="Seg_2474" s="T123">— Откройте дверь, отдайте ребенка, — говорил.</ta>
            <ta e="T134" id="Seg_2475" s="T128">Век-то не открывали, шаманы попусту возвращались.</ta>
            <ta e="T146" id="Seg_2476" s="T134">Вот отец ребенка сам решился [забрать ребенка], на жилище духа-хозяина, земли взобравшись, стал мочиться.</ta>
            <ta e="T156" id="Seg_2477" s="T146">Рассердившись, дух-хозяин земли схватил ребенка, бросил на свои нарты и помчался.</ta>
            <ta e="T164" id="Seg_2478" s="T156">Подъехав к жилищу, посадил его на бревно, что лежало там.</ta>
            <ta e="T172" id="Seg_2479" s="T164">Его нашли там через три месяца после того, как он пропал из дому.</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg"
                          name="gg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top"
                          name="Top"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc"
                          name="Foc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
