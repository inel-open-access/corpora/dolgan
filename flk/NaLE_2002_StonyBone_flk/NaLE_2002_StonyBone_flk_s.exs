<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDD6045085-E8A6-2E51-FE7A-8452749AC04F">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>NaLE_2002_StonyBone_flk</transcription-name>
         <referenced-file url="NaLE_2002_StonyBone_flk.wav" />
         <referenced-file url="NaLE_2002_StonyBone_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\flk\NaLE_2002_StonyBone_flk\NaLE_2002_StonyBone_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">649</ud-information>
            <ud-information attribute-name="# HIAT:w">460</ud-information>
            <ud-information attribute-name="# e">460</ud-information>
            <ud-information attribute-name="# HIAT:u">57</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="NaLE">
            <abbreviation>NaLE</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.0" type="appl" />
         <tli id="T1" time="0.738" type="appl" />
         <tli id="T2" time="1.475" type="appl" />
         <tli id="T3" time="2.213" type="appl" />
         <tli id="T4" time="2.95" type="appl" />
         <tli id="T5" time="3.688" type="appl" />
         <tli id="T6" time="4.425" type="appl" />
         <tli id="T7" time="5.993238173509058" />
         <tli id="T8" time="6.9110013791669145" type="intp" />
         <tli id="T9" time="7.828764584824771" type="intp" />
         <tli id="T10" time="8.746527790482629" />
         <tli id="T11" time="9.342" type="appl" />
         <tli id="T12" time="9.894" type="appl" />
         <tli id="T13" time="10.447" type="appl" />
         <tli id="T14" time="10.999" type="appl" />
         <tli id="T15" time="12.259805340470699" />
         <tli id="T16" time="12.661" type="appl" />
         <tli id="T17" time="13.772" type="appl" />
         <tli id="T18" time="14.882" type="appl" />
         <tli id="T19" time="15.992" type="appl" />
         <tli id="T20" time="17.103" type="appl" />
         <tli id="T21" time="18.213" type="appl" />
         <tli id="T22" time="19.072" type="appl" />
         <tli id="T23" time="19.931" type="appl" />
         <tli id="T24" time="20.79" type="appl" />
         <tli id="T25" time="21.649" type="appl" />
         <tli id="T26" time="22.508" type="appl" />
         <tli id="T27" time="23.367" type="appl" />
         <tli id="T28" time="24.226" type="appl" />
         <tli id="T29" time="25.086" type="appl" />
         <tli id="T30" time="25.945" type="appl" />
         <tli id="T31" time="26.804" type="appl" />
         <tli id="T32" time="27.663" type="appl" />
         <tli id="T33" time="28.522" type="appl" />
         <tli id="T34" time="29.381" type="appl" />
         <tli id="T35" time="30.586181023425535" />
         <tli id="T36" time="31.102" type="appl" />
         <tli id="T37" time="31.965" type="appl" />
         <tli id="T38" time="32.828" type="appl" />
         <tli id="T39" time="33.99279360035894" />
         <tli id="T40" time="34.59" type="appl" />
         <tli id="T41" time="35.49" type="appl" />
         <tli id="T42" time="36.39" type="appl" />
         <tli id="T43" time="37.29" type="appl" />
         <tli id="T44" time="38.19" type="appl" />
         <tli id="T45" time="39.09" type="appl" />
         <tli id="T46" time="39.99" type="appl" />
         <tli id="T47" time="40.89" type="appl" />
         <tli id="T48" time="41.79" type="appl" />
         <tli id="T49" time="42.69" type="appl" />
         <tli id="T50" time="43.59" type="appl" />
         <tli id="T51" time="44.49" type="appl" />
         <tli id="T52" time="45.278" type="appl" />
         <tli id="T53" time="46.066" type="appl" />
         <tli id="T54" time="46.854" type="appl" />
         <tli id="T55" time="47.641" type="appl" />
         <tli id="T56" time="48.429" type="appl" />
         <tli id="T57" time="49.217" type="appl" />
         <tli id="T58" time="50.005" type="appl" />
         <tli id="T59" time="50.793" type="appl" />
         <tli id="T60" time="51.581" type="appl" />
         <tli id="T61" time="52.369" type="appl" />
         <tli id="T62" time="53.156" type="appl" />
         <tli id="T63" time="53.944" type="appl" />
         <tli id="T64" time="54.732" type="appl" />
         <tli id="T65" time="55.52" type="appl" />
         <tli id="T66" time="55.962" type="appl" />
         <tli id="T67" time="56.404" type="appl" />
         <tli id="T68" time="56.846" type="appl" />
         <tli id="T69" time="59.5390546469515" />
         <tli id="T70" time="59.99904734324974" />
         <tli id="T71" time="64.63230711031181" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" time="65.852" type="appl" />
         <tli id="T77" time="66.471" type="appl" />
         <tli id="T78" time="67.091" type="appl" />
         <tli id="T79" time="68.18558402519538" />
         <tli id="T80" time="68.519" type="appl" />
         <tli id="T81" time="69.327" type="appl" />
         <tli id="T82" time="70.136" type="appl" />
         <tli id="T83" time="70.945" type="appl" />
         <tli id="T84" time="71.753" type="appl" />
         <tli id="T85" time="72.562" type="appl" />
         <tli id="T86" time="73.371" type="appl" />
         <tli id="T87" time="74.179" type="appl" />
         <tli id="T88" time="75.17213976027601" />
         <tli id="T89" time="75.502" type="appl" />
         <tli id="T90" time="76.016" type="appl" />
         <tli id="T91" time="76.529" type="appl" />
         <tli id="T92" time="77.043" type="appl" />
         <tli id="T93" time="77.557" type="appl" />
         <tli id="T94" time="78.071" type="appl" />
         <tli id="T95" time="78.585" type="appl" />
         <tli id="T96" time="79.098" type="appl" />
         <tli id="T97" time="79.612" type="appl" />
         <tli id="T98" time="80.126" type="appl" />
         <tli id="T99" time="80.64" type="appl" />
         <tli id="T100" time="81.154" type="appl" />
         <tli id="T101" time="81.667" type="appl" />
         <tli id="T102" time="82.181" type="appl" />
         <tli id="T103" time="82.695" type="appl" />
         <tli id="T104" time="83.21773643295103" type="intp" />
         <tli id="T105" time="83.74047286590206" type="intp" />
         <tli id="T106" time="84.2632092988531" type="intp" />
         <tli id="T107" time="84.78594573180413" type="intp" />
         <tli id="T108" time="85.30868216475517" type="intp" />
         <tli id="T109" time="85.8314185977062" type="intp" />
         <tli id="T110" time="86.35415503065724" type="intp" />
         <tli id="T111" time="86.87689146360827" type="intp" />
         <tli id="T112" time="87.39962789655931" type="intp" />
         <tli id="T113" time="87.92236432951034" type="intp" />
         <tli id="T114" time="88.44510076246138" type="intp" />
         <tli id="T115" time="88.96783719541241" type="intp" />
         <tli id="T116" time="89.49057362836345" type="intp" />
         <tli id="T117" time="90.01331006131448" type="intp" />
         <tli id="T118" time="90.53604649426552" type="intp" />
         <tli id="T119" time="91.05878292721655" type="intp" />
         <tli id="T120" time="91.58151936016759" type="intp" />
         <tli id="T121" time="92.10425579311863" type="intp" />
         <tli id="T122" time="92.62699222606966" type="intp" />
         <tli id="T123" time="93.1497286590207" type="intp" />
         <tli id="T124" time="93.67246509197173" type="intp" />
         <tli id="T125" time="94.19520152492277" type="intp" />
         <tli id="T126" time="94.7179379578738" type="intp" />
         <tli id="T127" time="95.24067439082484" type="intp" />
         <tli id="T128" time="95.76341082377589" type="intp" />
         <tli id="T129" time="96.28614725672693" type="intp" />
         <tli id="T130" time="96.80888368967797" type="intp" />
         <tli id="T131" time="97.331620122629" type="intp" />
         <tli id="T132" time="97.85435655558005" type="intp" />
         <tli id="T133" time="98.3770929885311" type="intp" />
         <tli id="T134" time="98.89982942148214" />
         <tli id="T135" time="99.746" type="appl" />
         <tli id="T136" time="100.788" type="appl" />
         <tli id="T137" time="101.829" type="appl" />
         <tli id="T138" time="102.87" type="appl" />
         <tli id="T139" time="103.485" type="appl" />
         <tli id="T140" time="104.1" type="appl" />
         <tli id="T141" time="104.715" type="appl" />
         <tli id="T142" time="105.33" type="appl" />
         <tli id="T143" time="105.945" type="appl" />
         <tli id="T144" time="106.56" type="appl" />
         <tli id="T145" time="108.87827124555054" />
         <tli id="T146" time="112.80699792826019" />
         <tli id="T147" />
         <tli id="T148" />
         <tli id="T149" />
         <tli id="T150" time="112.807" type="appl" />
         <tli id="T151" time="114.2315195807316" />
         <tli id="T152" time="115.412" type="appl" />
         <tli id="T153" time="116.124" type="appl" />
         <tli id="T154" time="116.837" type="appl" />
         <tli id="T155" time="117.549" type="appl" />
         <tli id="T156" time="118.261" type="appl" />
         <tli id="T157" time="118.973" type="appl" />
         <tli id="T158" time="119.685" type="appl" />
         <tli id="T159" time="120.398" type="appl" />
         <tli id="T160" time="121.11" type="appl" />
         <tli id="T161" time="121.70199992378951" />
         <tli id="T162" time="122.746" type="appl" />
         <tli id="T163" time="123.67" type="appl" />
         <tli id="T164" time="124.593" type="appl" />
         <tli id="T165" time="125.517" type="appl" />
         <tli id="T166" time="126.441" type="appl" />
         <tli id="T167" time="127.365" type="appl" />
         <tli id="T168" time="128.289" type="appl" />
         <tli id="T169" time="129.212" type="appl" />
         <tli id="T170" time="130.136" type="appl" />
         <tli id="T171" time="130.82667275180307" />
         <tli id="T172" time="131.738" type="appl" />
         <tli id="T173" time="132.417" type="appl" />
         <tli id="T174" time="133.095" type="appl" />
         <tli id="T175" time="134.019" type="appl" />
         <tli id="T176" time="134.942" type="appl" />
         <tli id="T177" time="135.866" type="appl" />
         <tli id="T178" time="136.65667132970833" />
         <tli id="T179" time="137.302" type="appl" />
         <tli id="T180" time="137.815" type="appl" />
         <tli id="T181" time="138.328" type="appl" />
         <tli id="T182" time="138.84" type="appl" />
         <tli id="T183" time="139.352" type="appl" />
         <tli id="T184" time="139.865" type="appl" />
         <tli id="T185" time="140.378" type="appl" />
         <tli id="T186" time="140.9833344023712" />
         <tli id="T187" time="141.972" type="appl" />
         <tli id="T188" time="143.00633613559023" />
         <tli id="T189" time="143.718" type="appl" />
         <tli id="T190" time="144.384" type="appl" />
         <tli id="T191" time="145.049" type="appl" />
         <tli id="T192" time="145.714" type="appl" />
         <tli id="T193" time="146.38" type="appl" />
         <tli id="T194" time="147.03832940781365" />
         <tli id="T195" time="147.5812099662704" type="intp" />
         <tli id="T196" time="148.12409052472714" type="intp" />
         <tli id="T197" time="148.66697108318388" type="intp" />
         <tli id="T198" time="149.20985164164063" type="intp" />
         <tli id="T199" time="149.75273220009734" type="intp" />
         <tli id="T200" time="150.29561275855406" type="intp" />
         <tli id="T201" time="150.8384933170108" type="intp" />
         <tli id="T202" time="151.38137387546755" type="intp" />
         <tli id="T203" time="151.92425443392426" />
         <tli id="T204" time="152.379048645055" type="intp" />
         <tli id="T205" time="152.83384285618575" type="intp" />
         <tli id="T206" time="153.28863706731653" type="intp" />
         <tli id="T207" time="153.74343127844728" type="intp" />
         <tli id="T208" time="154.19822548957802" type="intp" />
         <tli id="T209" time="154.6530197007088" type="intp" />
         <tli id="T210" time="155.10781391183957" type="intp" />
         <tli id="T211" time="155.56260812297032" />
         <tli id="T212" time="155.986" type="appl" />
         <tli id="T213" time="156.382" type="appl" />
         <tli id="T214" time="156.779" type="appl" />
         <tli id="T215" time="157.176" type="appl" />
         <tli id="T216" time="157.573" type="appl" />
         <tli id="T217" time="157.969" type="appl" />
         <tli id="T218" time="158.41933359760338" />
         <tli id="T219" time="158.733" type="appl" />
         <tli id="T220" time="159.1" type="appl" />
         <tli id="T221" time="159.466" type="appl" />
         <tli id="T222" time="160.99744370438682" />
         <tli id="T223" time="163.23740813853482" />
         <tli id="T224" />
         <tli id="T225" />
         <tli id="T226" time="163.73073363891265" />
         <tli id="T227" time="164.178" type="appl" />
         <tli id="T228" time="165.001" type="appl" />
         <tli id="T229" time="165.559" type="appl" />
         <tli id="T230" time="166.117" type="appl" />
         <tli id="T231" time="166.676" type="appl" />
         <tli id="T232" time="167.234" type="appl" />
         <tli id="T233" time="167.792" type="appl" />
         <tli id="T234" time="168.85065234553662" />
         <tli id="T235" time="169.36990336019858" type="intp" />
         <tli id="T236" time="169.88915437486054" type="intp" />
         <tli id="T237" time="170.4084053895225" type="intp" />
         <tli id="T238" time="170.92765640418446" type="intp" />
         <tli id="T239" time="171.44690741884642" type="intp" />
         <tli id="T240" time="171.96615843350838" type="intp" />
         <tli id="T241" time="172.48540944817034" type="intp" />
         <tli id="T242" time="173.00466046283228" type="intp" />
         <tli id="T243" time="173.5239114774942" type="intp" />
         <tli id="T244" time="174.04316249215617" type="intp" />
         <tli id="T245" time="174.56241350681813" type="intp" />
         <tli id="T246" time="175.08166452148006" type="intp" />
         <tli id="T247" time="175.600915536142" type="intp" />
         <tli id="T248" time="176.12016655080396" type="intp" />
         <tli id="T249" time="176.63941756546592" type="intp" />
         <tli id="T250" time="177.15866858012785" type="intp" />
         <tli id="T251" time="177.67791959478978" type="intp" />
         <tli id="T252" time="178.19717060945175" />
         <tli id="T253" time="180.05714107709247" />
         <tli id="T254" time="180.327" type="appl" />
         <tli id="T255" time="181.433" type="appl" />
         <tli id="T256" time="182.54" type="appl" />
         <tli id="T257" time="183.647" type="appl" />
         <tli id="T258" time="184.753" type="appl" />
         <tli id="T259" time="185.77333937856977" />
         <tli id="T260" time="186.48550255010602" type="intp" />
         <tli id="T261" time="187.19766572164227" type="intp" />
         <tli id="T262" time="187.9098288931785" type="intp" />
         <tli id="T263" time="188.6219920647148" type="intp" />
         <tli id="T264" time="189.33415523625104" />
         <tli id="T265" time="189.629" type="appl" />
         <tli id="T266" time="190.115" type="appl" />
         <tli id="T267" time="190.602" type="appl" />
         <tli id="T268" time="191.089" type="appl" />
         <tli id="T269" time="191.575" type="appl" />
         <tli id="T270" time="192.062" type="appl" />
         <tli id="T271" time="192.549" type="appl" />
         <tli id="T272" time="193.036" type="appl" />
         <tli id="T273" time="193.522" type="appl" />
         <tli id="T274" time="194.009" type="appl" />
         <tli id="T275" time="194.496" type="appl" />
         <tli id="T276" time="194.982" type="appl" />
         <tli id="T277" time="195.469" type="appl" />
         <tli id="T278" time="195.956" type="appl" />
         <tli id="T279" time="196.442" type="appl" />
         <tli id="T280" time="197.43019856337122" />
         <tli id="T281" time="198.004" type="appl" />
         <tli id="T282" time="199.08" type="appl" />
         <tli id="T283" time="200.155" type="appl" />
         <tli id="T284" time="201.23" type="appl" />
         <tli id="T285" time="202.306" type="appl" />
         <tli id="T286" time="203.28100410257304" />
         <tli id="T287" time="203.915" type="appl" />
         <tli id="T288" time="204.45" type="appl" />
         <tli id="T289" time="204.984" type="appl" />
         <tli id="T290" time="205.518" type="appl" />
         <tli id="T291" time="206.053" type="appl" />
         <tli id="T292" time="206.587" type="appl" />
         <tli id="T293" time="207.121" type="appl" />
         <tli id="T294" time="207.656" type="appl" />
         <tli id="T295" time="208.19" type="appl" />
         <tli id="T296" time="208.799" type="intp" />
         <tli id="T297" time="209.40800000000002" type="intp" />
         <tli id="T298" time="210.01700000000002" type="intp" />
         <tli id="T299" time="210.62600000000003" type="intp" />
         <tli id="T300" time="211.235" type="intp" />
         <tli id="T301" time="211.844" type="intp" />
         <tli id="T302" time="212.453" type="appl" />
         <tli id="T303" time="213.027" type="appl" />
         <tli id="T304" time="213.602" type="appl" />
         <tli id="T305" time="214.176" type="appl" />
         <tli id="T306" time="214.5500048624047" />
         <tli id="T307" time="215.28498668204622" type="intp" />
         <tli id="T308" time="216.01996850168774" type="intp" />
         <tli id="T309" time="216.75495032132926" type="intp" />
         <tli id="T310" time="217.48993214097078" type="intp" />
         <tli id="T311" time="218.2249139606123" type="intp" />
         <tli id="T312" time="218.95989578025382" type="intp" />
         <tli id="T313" time="219.69487759989534" type="intp" />
         <tli id="T314" time="220.42985941953685" type="intp" />
         <tli id="T315" time="221.16484123917837" type="intp" />
         <tli id="T316" time="221.8998230588199" type="intp" />
         <tli id="T317" time="222.6348048784614" type="intp" />
         <tli id="T318" time="223.3697866981029" />
         <tli id="T319" time="224.00977653643088" type="intp" />
         <tli id="T320" time="224.64976637475888" type="intp" />
         <tli id="T321" time="225.2897562130869" type="intp" />
         <tli id="T322" time="225.92974605141487" />
         <tli id="T323" time="225.989" type="appl" />
         <tli id="T324" time="226.239" type="appl" />
         <tli id="T325" time="226.488" type="appl" />
         <tli id="T326" time="226.737" type="appl" />
         <tli id="T327" time="226.986" type="appl" />
         <tli id="T328" time="227.236" type="appl" />
         <tli id="T329" time="227.485" type="appl" />
         <tli id="T330" time="227.734" type="appl" />
         <tli id="T331" time="227.983" type="appl" />
         <tli id="T332" time="228.233" type="appl" />
         <tli id="T333" time="228.482" type="appl" />
         <tli id="T334" time="228.731" type="appl" />
         <tli id="T335" time="228.98" type="appl" />
         <tli id="T336" time="229.23" type="appl" />
         <tli id="T337" time="234.9429362745342" />
         <tli id="T338" time="235.53" type="appl" />
         <tli id="T339" time="236.328" type="appl" />
         <tli id="T340" time="237.126" type="appl" />
         <tli id="T341" time="237.924" type="appl" />
         <tli id="T342" time="238.722" type="appl" />
         <tli id="T343" time="239.52" type="appl" />
         <tli id="T344" time="240.318" type="appl" />
         <tli id="T345" time="241.116" type="appl" />
         <tli id="T346" time="241.914" type="appl" />
         <tli id="T347" time="242.712" type="appl" />
         <tli id="T348" time="243.51" type="appl" />
         <tli id="T349" time="244.308" type="appl" />
         <tli id="T350" time="245.106" type="appl" />
         <tli id="T351" time="245.904" type="appl" />
         <tli id="T352" time="246.702" type="appl" />
         <tli id="T353" time="247.70666590542987" />
         <tli id="T354" time="248.226" type="appl" />
         <tli id="T355" time="248.952" type="appl" />
         <tli id="T356" time="249.679" type="appl" />
         <tli id="T357" time="250.405" type="appl" />
         <tli id="T358" time="251.131" type="appl" />
         <tli id="T359" time="251.581" type="appl" />
         <tli id="T360" time="252.031" type="appl" />
         <tli id="T361" time="252.481" type="appl" />
         <tli id="T362" time="252.931" type="appl" />
         <tli id="T363" time="253.381" type="appl" />
         <tli id="T364" time="253.831" type="appl" />
         <tli id="T365" time="254.281" type="appl" />
         <tli id="T366" time="254.88" type="appl" />
         <tli id="T367" time="255.478" type="appl" />
         <tli id="T368" time="256.076" type="appl" />
         <tli id="T369" time="256.6083214411485" />
         <tli id="T370" time="257.12" type="appl" />
         <tli id="T371" time="257.566" type="appl" />
         <tli id="T372" time="258.011" type="appl" />
         <tli id="T373" time="258.457" type="appl" />
         <tli id="T374" time="258.902" type="appl" />
         <tli id="T375" time="259.348" type="appl" />
         <tli id="T376" time="259.793" type="appl" />
         <tli id="T377" time="260.55" type="appl" />
         <tli id="T378" time="261.307" type="appl" />
         <tli id="T379" time="262.064" type="appl" />
         <tli id="T380" time="263.15990209105985" type="intp" />
         <tli id="T381" time="264.2558041821197" />
         <tli id="T382" time="264.5239020910598" type="intp" />
         <tli id="T383" time="264.792" type="appl" />
         <tli id="T384" time="266.156" type="appl" />
         <tli id="T385" time="267.52" type="appl" />
         <tli id="T386" time="268.883" type="appl" />
         <tli id="T387" time="270.247" type="appl" />
         <tli id="T388" time="271.611" type="appl" />
         <tli id="T389" time="272.975" type="appl" />
         <tli id="T390" time="274.54566684274016" />
         <tli id="T391" time="275.264" type="appl" />
         <tli id="T392" time="276.189" type="appl" />
         <tli id="T393" time="277.114" type="appl" />
         <tli id="T394" time="278.039" type="appl" />
         <tli id="T395" time="278.964" type="appl" />
         <tli id="T396" time="279.889" type="appl" />
         <tli id="T397" time="280.813" type="appl" />
         <tli id="T398" time="281.738" type="appl" />
         <tli id="T399" time="282.663" type="appl" />
         <tli id="T400" time="283.588" type="appl" />
         <tli id="T401" time="284.513" type="appl" />
         <tli id="T402" time="285.438" type="appl" />
         <tli id="T403" time="286.363" type="appl" />
         <tli id="T404" time="286.807" type="appl" />
         <tli id="T405" time="287.25" type="appl" />
         <tli id="T406" time="287.694" type="appl" />
         <tli id="T407" time="288.138" type="appl" />
         <tli id="T408" time="288.581" type="appl" />
         <tli id="T409" time="288.9116626997364" />
         <tli id="T410" time="289.793" type="appl" />
         <tli id="T411" time="290.561" type="appl" />
         <tli id="T412" time="291.328" type="appl" />
         <tli id="T413" time="292.096" type="appl" />
         <tli id="T414" time="292.864" type="appl" />
         <tli id="T415" time="293.632" type="appl" />
         <tli id="T416" time="294.399" type="appl" />
         <tli id="T417" time="295.167" type="appl" />
         <tli id="T418" time="295.95498836676495" />
         <tli id="T419" time="296.505" type="appl" />
         <tli id="T420" time="297.075" type="appl" />
         <tli id="T421" time="297.645" type="appl" />
         <tli id="T422" time="298.215" type="appl" />
         <tli id="T423" time="298.786" type="appl" />
         <tli id="T424" time="299.356" type="appl" />
         <tli id="T425" time="299.926" type="appl" />
         <tli id="T426" time="300.496" type="appl" />
         <tli id="T427" time="301.066" type="appl" />
         <tli id="T428" time="301.542660063137" />
         <tli id="T429" time="301.746" type="appl" />
         <tli id="T430" time="301.856" type="appl" />
         <tli id="T431" time="303.6751782866347" />
         <tli id="T432" time="315.2949937887774" />
         <tli id="T433" />
         <tli id="T434" />
         <tli id="T435" />
         <tli id="T436" />
         <tli id="T437" />
         <tli id="T438" />
         <tli id="T439" />
         <tli id="T440" />
         <tli id="T441" />
         <tli id="T442" />
         <tli id="T443" />
         <tli id="T444" />
         <tli id="T445" />
         <tli id="T446" />
         <tli id="T447" />
         <tli id="T448" time="315.60165558630956" />
         <tli id="T449" time="316.072" type="appl" />
         <tli id="T450" time="316.664" type="appl" />
         <tli id="T451" time="317.257" type="appl" />
         <tli id="T452" time="317.849" type="appl" />
         <tli id="T453" time="319.13493281874537" />
         <tli id="T454" time="319.48" type="appl" />
         <tli id="T455" time="320.519" type="appl" />
         <tli id="T456" time="321.558" type="appl" />
         <tli id="T457" time="322.597" type="appl" />
         <tli id="T458" time="323.636" type="appl" />
         <tli id="T459" time="324.675" type="appl" />
         <tli id="T460" time="325.714" type="appl" />
         <tli id="T461" time="326.6329908489285" />
         <tli id="T462" time="327.698" type="appl" />
         <tli id="T463" time="328.643" type="appl" />
         <tli id="T464" time="329.587" type="appl" />
         <tli id="T465" time="330.532" type="appl" />
         <tli id="T466" time="331.477" type="appl" />
         <tli id="T467" time="332.422" type="appl" />
         <tli id="T468" time="333.366" type="appl" />
         <tli id="T469" time="334.311" type="appl" />
         <tli id="T470" time="335.256" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="NaLE"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T470" id="Seg_0" n="sc" s="T0">
               <ts e="T7" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Min</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">bu</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">öhü</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">istibitim</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">kergetterbin</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_19" n="HIAT:w" s="T5">gɨtta</ts>
                  <nts id="Seg_20" n="HIAT:ip">…</nts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_23" n="HIAT:u" s="T7">
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">Školaga</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_28" n="HIAT:w" s="T8">kiːrer</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_31" n="HIAT:w" s="T9">dʼɨlbar</ts>
                  <nts id="Seg_32" n="HIAT:ip">.</nts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T15" id="Seg_35" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">Ontuŋ</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_40" n="HIAT:w" s="T11">anɨga</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_43" n="HIAT:w" s="T12">di͡eri</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_46" n="HIAT:w" s="T13">öjbör</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_49" n="HIAT:w" s="T14">baːr</ts>
                  <nts id="Seg_50" n="HIAT:ip">.</nts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_53" n="HIAT:u" s="T15">
                  <ts e="T16" id="Seg_55" n="HIAT:w" s="T15">Buːrdukkaːn</ts>
                  <nts id="Seg_56" n="HIAT:ip">,</nts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_59" n="HIAT:w" s="T16">maːmam</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_62" n="HIAT:w" s="T17">edʼiːje</ts>
                  <nts id="Seg_63" n="HIAT:ip">,</nts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_66" n="HIAT:w" s="T18">Kɨlabɨja</ts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_69" n="HIAT:w" s="T19">kepseːčči</ts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_72" n="HIAT:w" s="T20">ete</ts>
                  <nts id="Seg_73" n="HIAT:ip">.</nts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T35" id="Seg_76" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_78" n="HIAT:w" s="T21">ɨraːk-ɨraːk</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_81" n="HIAT:w" s="T22">mu͡oraga</ts>
                  <nts id="Seg_82" n="HIAT:ip">,</nts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_85" n="HIAT:w" s="T23">töröːbüt</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_88" n="HIAT:w" s="T24">hiriger</ts>
                  <nts id="Seg_89" n="HIAT:ip">,</nts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_92" n="HIAT:w" s="T25">bejetin</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_95" n="HIAT:w" s="T26">töröːbüt</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_98" n="HIAT:w" s="T27">hiriger</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_101" n="HIAT:w" s="T28">kergetterin</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_104" n="HIAT:w" s="T29">kɨtta</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_107" n="HIAT:w" s="T30">ajannɨː</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_110" n="HIAT:w" s="T31">hɨldʼɨbɨta</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_113" n="HIAT:w" s="T32">basku͡oj</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_116" n="HIAT:w" s="T33">bagajɨ</ts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_119" n="HIAT:w" s="T34">u͡ol</ts>
                  <nts id="Seg_120" n="HIAT:ip">.</nts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T39" id="Seg_123" n="HIAT:u" s="T35">
                  <ts e="T36" id="Seg_125" n="HIAT:w" s="T35">Ontuŋ</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_128" n="HIAT:w" s="T36">aːta</ts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_131" n="HIAT:w" s="T37">Bulčut</ts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_134" n="HIAT:w" s="T38">ete</ts>
                  <nts id="Seg_135" n="HIAT:ip">.</nts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T51" id="Seg_138" n="HIAT:u" s="T39">
                  <ts e="T40" id="Seg_140" n="HIAT:w" s="T39">Atɨn</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_143" n="HIAT:w" s="T40">hirge</ts>
                  <nts id="Seg_144" n="HIAT:ip">,</nts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_146" n="HIAT:ip">(</nts>
                  <ts e="T43" id="Seg_148" n="HIAT:w" s="T41">gin-</ts>
                  <nts id="Seg_149" n="HIAT:ip">)</nts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_152" n="HIAT:w" s="T43">ginilerten</ts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_155" n="HIAT:w" s="T44">ɨraːkkaːn</ts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_158" n="HIAT:w" s="T45">hahɨː</ts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_161" n="HIAT:w" s="T46">hɨldʼɨbɨta</ts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_164" n="HIAT:w" s="T47">basku͡oj</ts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_167" n="HIAT:w" s="T48">kɨːs</ts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_170" n="HIAT:w" s="T49">kergetterin</ts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_173" n="HIAT:w" s="T50">kɨtta</ts>
                  <nts id="Seg_174" n="HIAT:ip">.</nts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T65" id="Seg_177" n="HIAT:u" s="T51">
                  <ts e="T52" id="Seg_179" n="HIAT:w" s="T51">Iti</ts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_182" n="HIAT:w" s="T52">hiŋil</ts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_185" n="HIAT:w" s="T53">eː</ts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_188" n="HIAT:w" s="T54">u͡olu</ts>
                  <nts id="Seg_189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_191" n="HIAT:w" s="T55">kɨtta</ts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_194" n="HIAT:w" s="T56">iti</ts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_197" n="HIAT:w" s="T57">kɨːs</ts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_200" n="HIAT:w" s="T58">körsöːččü</ts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_203" n="HIAT:w" s="T59">ebitter</ts>
                  <nts id="Seg_204" n="HIAT:ip">,</nts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_207" n="HIAT:w" s="T60">bagarsaːččɨ</ts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_210" n="HIAT:w" s="T61">ebitter</ts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_213" n="HIAT:w" s="T62">bejelerin</ts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_216" n="HIAT:w" s="T63">töröːbüt</ts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_219" n="HIAT:w" s="T64">hirderiger</ts>
                  <nts id="Seg_220" n="HIAT:ip">.</nts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T70" id="Seg_223" n="HIAT:u" s="T65">
                  <ts e="T66" id="Seg_225" n="HIAT:w" s="T65">Biːrde</ts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_228" n="HIAT:w" s="T66">kɨːha</ts>
                  <nts id="Seg_229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_231" n="HIAT:w" s="T67">Bulčutun</ts>
                  <nts id="Seg_232" n="HIAT:ip">,</nts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_235" n="HIAT:w" s="T68">u͡olun</ts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_238" n="HIAT:w" s="T69">haŋarar</ts>
                  <nts id="Seg_239" n="HIAT:ip">:</nts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T75" id="Seg_242" n="HIAT:u" s="T70">
                  <nts id="Seg_243" n="HIAT:ip">"</nts>
                  <ts e="T71" id="Seg_245" n="HIAT:w" s="T70">Dokuːl</ts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_248" n="HIAT:w" s="T71">bihigi</ts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_251" n="HIAT:w" s="T72">kergetterbititten</ts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_254" n="HIAT:w" s="T73">kistene</ts>
                  <nts id="Seg_255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_257" n="HIAT:w" s="T74">hɨldʼɨ͡akpɨtɨj</ts>
                  <nts id="Seg_258" n="HIAT:ip">?</nts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T79" id="Seg_261" n="HIAT:u" s="T75">
                  <ts e="T76" id="Seg_263" n="HIAT:w" s="T75">Kel</ts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_266" n="HIAT:w" s="T76">bihi͡eke</ts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_268" n="HIAT:ip">–</nts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_271" n="HIAT:w" s="T77">ɨ͡allammɨt</ts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_274" n="HIAT:w" s="T78">bu͡ol</ts>
                  <nts id="Seg_275" n="HIAT:ip">.</nts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T88" id="Seg_278" n="HIAT:u" s="T79">
                  <ts e="T80" id="Seg_280" n="HIAT:w" s="T79">Kergetterbin</ts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_283" n="HIAT:w" s="T80">kɨtta</ts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_286" n="HIAT:w" s="T81">haŋarɨs</ts>
                  <nts id="Seg_287" n="HIAT:ip">,</nts>
                  <nts id="Seg_288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_290" n="HIAT:w" s="T82">agabɨn</ts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_293" n="HIAT:w" s="T83">kɨtta</ts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_296" n="HIAT:w" s="T84">haŋarɨs</ts>
                  <nts id="Seg_297" n="HIAT:ip">,</nts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_300" n="HIAT:w" s="T85">agabɨttan</ts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_303" n="HIAT:w" s="T86">kördöː</ts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_306" n="HIAT:w" s="T87">minigin</ts>
                  <nts id="Seg_307" n="HIAT:ip">.</nts>
                  <nts id="Seg_308" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T103" id="Seg_310" n="HIAT:u" s="T88">
                  <ts e="T89" id="Seg_312" n="HIAT:w" s="T88">Agam</ts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_315" n="HIAT:w" s="T89">taːk-daː</ts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_318" n="HIAT:w" s="T92">üčügej</ts>
                  <nts id="Seg_319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_321" n="HIAT:w" s="T93">bagaj</ts>
                  <nts id="Seg_322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_324" n="HIAT:w" s="T94">kihi</ts>
                  <nts id="Seg_325" n="HIAT:ip">,</nts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_328" n="HIAT:w" s="T95">daː</ts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_331" n="HIAT:w" s="T96">bu͡ollar</ts>
                  <nts id="Seg_332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_334" n="HIAT:w" s="T97">nʼɨmalaːk</ts>
                  <nts id="Seg_335" n="HIAT:ip">,</nts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_338" n="HIAT:w" s="T98">dʼe</ts>
                  <nts id="Seg_339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_341" n="HIAT:w" s="T99">ol</ts>
                  <nts id="Seg_342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_344" n="HIAT:w" s="T100">nʼɨmatɨttan</ts>
                  <nts id="Seg_345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_347" n="HIAT:w" s="T101">en</ts>
                  <nts id="Seg_348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_350" n="HIAT:w" s="T102">kečehime</ts>
                  <nts id="Seg_351" n="HIAT:ip">.</nts>
                  <nts id="Seg_352" n="HIAT:ip">"</nts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T134" id="Seg_355" n="HIAT:u" s="T103">
                  <ts e="T104" id="Seg_357" n="HIAT:w" s="T103">Dʼe</ts>
                  <nts id="Seg_358" n="HIAT:ip">,</nts>
                  <nts id="Seg_359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_361" n="HIAT:w" s="T104">ol</ts>
                  <nts id="Seg_362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_364" n="HIAT:w" s="T105">kördükkaːn</ts>
                  <nts id="Seg_365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_367" n="HIAT:w" s="T106">haŋarda</ts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_370" n="HIAT:w" s="T107">daː</ts>
                  <nts id="Seg_371" n="HIAT:ip">,</nts>
                  <nts id="Seg_372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_374" n="HIAT:w" s="T108">u͡olbut</ts>
                  <nts id="Seg_375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_377" n="HIAT:w" s="T109">ol</ts>
                  <nts id="Seg_378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_380" n="HIAT:w" s="T110">kördükkeːn</ts>
                  <nts id="Seg_381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_383" n="HIAT:w" s="T111">kas</ts>
                  <nts id="Seg_384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_386" n="HIAT:w" s="T112">daː</ts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_389" n="HIAT:w" s="T113">kün</ts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_392" n="HIAT:w" s="T114">bu͡olan</ts>
                  <nts id="Seg_393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_395" n="HIAT:w" s="T115">baraːn</ts>
                  <nts id="Seg_396" n="HIAT:ip">,</nts>
                  <nts id="Seg_397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_399" n="HIAT:w" s="T116">itiː</ts>
                  <nts id="Seg_400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_402" n="HIAT:w" s="T117">bagajɨ</ts>
                  <nts id="Seg_403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_405" n="HIAT:w" s="T118">küŋŋe</ts>
                  <nts id="Seg_406" n="HIAT:ip">,</nts>
                  <nts id="Seg_407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_409" n="HIAT:w" s="T119">haːs</ts>
                  <nts id="Seg_410" n="HIAT:ip">,</nts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_413" n="HIAT:w" s="T120">kühü͡örü</ts>
                  <nts id="Seg_414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_416" n="HIAT:w" s="T121">künneːk</ts>
                  <nts id="Seg_417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_419" n="HIAT:w" s="T122">bagajɨ</ts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_422" n="HIAT:w" s="T123">küŋŋe</ts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_425" n="HIAT:w" s="T124">dʼe</ts>
                  <nts id="Seg_426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_428" n="HIAT:w" s="T125">uːčaktanan</ts>
                  <nts id="Seg_429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_431" n="HIAT:w" s="T126">baraːn</ts>
                  <nts id="Seg_432" n="HIAT:ip">,</nts>
                  <nts id="Seg_433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_435" n="HIAT:w" s="T127">dʼe</ts>
                  <nts id="Seg_436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_438" n="HIAT:w" s="T128">keler</ts>
                  <nts id="Seg_439" n="HIAT:ip">,</nts>
                  <nts id="Seg_440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_442" n="HIAT:w" s="T129">bejetin</ts>
                  <nts id="Seg_443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_444" n="HIAT:ip">(</nts>
                  <ts e="T132" id="Seg_446" n="HIAT:w" s="T130">aːkul-</ts>
                  <nts id="Seg_447" n="HIAT:ip">)</nts>
                  <nts id="Seg_448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_450" n="HIAT:w" s="T132">čeːlkeː</ts>
                  <nts id="Seg_451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_453" n="HIAT:w" s="T133">aːkulaːk</ts>
                  <nts id="Seg_454" n="HIAT:ip">.</nts>
                  <nts id="Seg_455" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T138" id="Seg_457" n="HIAT:u" s="T134">
                  <ts e="T135" id="Seg_459" n="HIAT:w" s="T134">Ontutunan</ts>
                  <nts id="Seg_460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_462" n="HIAT:w" s="T135">ɨ͡allana</ts>
                  <nts id="Seg_463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_465" n="HIAT:w" s="T136">barda</ts>
                  <nts id="Seg_466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_468" n="HIAT:w" s="T137">kɨːhɨgar</ts>
                  <nts id="Seg_469" n="HIAT:ip">.</nts>
                  <nts id="Seg_470" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T145" id="Seg_472" n="HIAT:u" s="T138">
                  <ts e="T139" id="Seg_474" n="HIAT:w" s="T138">Kiːreːtin</ts>
                  <nts id="Seg_475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_477" n="HIAT:w" s="T139">hirge</ts>
                  <nts id="Seg_478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_480" n="HIAT:w" s="T140">di͡eri</ts>
                  <nts id="Seg_481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_483" n="HIAT:w" s="T141">meniːtin</ts>
                  <nts id="Seg_484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_486" n="HIAT:w" s="T142">kergetteriger</ts>
                  <nts id="Seg_487" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_489" n="HIAT:w" s="T143">bökčöŋnöːn</ts>
                  <nts id="Seg_490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_492" n="HIAT:w" s="T144">baraːn</ts>
                  <nts id="Seg_493" n="HIAT:ip">:</nts>
                  <nts id="Seg_494" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T151" id="Seg_496" n="HIAT:u" s="T145">
                  <nts id="Seg_497" n="HIAT:ip">"</nts>
                  <ts e="T146" id="Seg_499" n="HIAT:w" s="T145">Doroːboŋ</ts>
                  <nts id="Seg_500" n="HIAT:ip">,</nts>
                  <nts id="Seg_501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_503" n="HIAT:w" s="T146">doroːboŋ</ts>
                  <nts id="Seg_504" n="HIAT:ip">,</nts>
                  <nts id="Seg_505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_507" n="HIAT:w" s="T147">kɨrdʼagastar</ts>
                  <nts id="Seg_508" n="HIAT:ip">"</nts>
                  <nts id="Seg_509" n="HIAT:ip">,</nts>
                  <nts id="Seg_510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_512" n="HIAT:w" s="T148">di͡ete</ts>
                  <nts id="Seg_513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_515" n="HIAT:w" s="T149">Bulčut</ts>
                  <nts id="Seg_516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_518" n="HIAT:w" s="T150">u͡olbut</ts>
                  <nts id="Seg_519" n="HIAT:ip">.</nts>
                  <nts id="Seg_520" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T161" id="Seg_522" n="HIAT:u" s="T151">
                  <nts id="Seg_523" n="HIAT:ip">"</nts>
                  <ts e="T152" id="Seg_525" n="HIAT:w" s="T151">Dʼe</ts>
                  <nts id="Seg_526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_528" n="HIAT:w" s="T152">bu</ts>
                  <nts id="Seg_529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_531" n="HIAT:w" s="T153">kellim</ts>
                  <nts id="Seg_532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_534" n="HIAT:w" s="T154">ehi͡eke</ts>
                  <nts id="Seg_535" n="HIAT:ip">,</nts>
                  <nts id="Seg_536" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_537" n="HIAT:ip">(</nts>
                  <ts e="T156" id="Seg_539" n="HIAT:w" s="T155">eni͡e</ts>
                  <nts id="Seg_540" n="HIAT:ip">)</nts>
                  <nts id="Seg_541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_543" n="HIAT:w" s="T156">edʼi͡e</ts>
                  <nts id="Seg_544" n="HIAT:ip">,</nts>
                  <nts id="Seg_545" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_547" n="HIAT:w" s="T157">aga</ts>
                  <nts id="Seg_548" n="HIAT:ip">,</nts>
                  <nts id="Seg_549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_551" n="HIAT:w" s="T158">ehigitten</ts>
                  <nts id="Seg_552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_554" n="HIAT:w" s="T159">kɨːskɨtɨn</ts>
                  <nts id="Seg_555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_557" n="HIAT:w" s="T160">kördüːbün</ts>
                  <nts id="Seg_558" n="HIAT:ip">.</nts>
                  <nts id="Seg_559" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T171" id="Seg_561" n="HIAT:u" s="T161">
                  <ts e="T162" id="Seg_563" n="HIAT:w" s="T161">Tugun</ts>
                  <nts id="Seg_564" n="HIAT:ip">,</nts>
                  <nts id="Seg_565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_567" n="HIAT:w" s="T162">dokuːl</ts>
                  <nts id="Seg_568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_570" n="HIAT:w" s="T163">kisti͡ekpitij</ts>
                  <nts id="Seg_571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_573" n="HIAT:w" s="T164">bihigi</ts>
                  <nts id="Seg_574" n="HIAT:ip">,</nts>
                  <nts id="Seg_575" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_577" n="HIAT:w" s="T165">bihigi</ts>
                  <nts id="Seg_578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_580" n="HIAT:w" s="T166">domnuŋŋuttan</ts>
                  <nts id="Seg_581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_583" n="HIAT:w" s="T167">bagarsabɨt</ts>
                  <nts id="Seg_584" n="HIAT:ip">,</nts>
                  <nts id="Seg_585" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_587" n="HIAT:w" s="T168">beje-bejebitin</ts>
                  <nts id="Seg_588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_590" n="HIAT:w" s="T169">hürdeːk</ts>
                  <nts id="Seg_591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_593" n="HIAT:w" s="T170">kɨlɨːlɨːbɨt</ts>
                  <nts id="Seg_594" n="HIAT:ip">.</nts>
                  <nts id="Seg_595" n="HIAT:ip">"</nts>
                  <nts id="Seg_596" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T178" id="Seg_598" n="HIAT:u" s="T171">
                  <nts id="Seg_599" n="HIAT:ip">"</nts>
                  <ts e="T172" id="Seg_601" n="HIAT:w" s="T171">Dʼe</ts>
                  <nts id="Seg_602" n="HIAT:ip">,</nts>
                  <nts id="Seg_603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_605" n="HIAT:w" s="T172">kajtak</ts>
                  <nts id="Seg_606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_608" n="HIAT:w" s="T173">gɨnɨ͡aŋɨj</ts>
                  <nts id="Seg_609" n="HIAT:ip">"</nts>
                  <nts id="Seg_610" n="HIAT:ip">,</nts>
                  <nts id="Seg_611" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_613" n="HIAT:w" s="T174">di͡ete</ts>
                  <nts id="Seg_614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_616" n="HIAT:w" s="T175">kɨlaŋnɨː-kɨlaŋnɨː</ts>
                  <nts id="Seg_617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_619" n="HIAT:w" s="T176">kɨːs</ts>
                  <nts id="Seg_620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_622" n="HIAT:w" s="T177">agata</ts>
                  <nts id="Seg_623" n="HIAT:ip">.</nts>
                  <nts id="Seg_624" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T186" id="Seg_626" n="HIAT:u" s="T178">
                  <ts e="T179" id="Seg_628" n="HIAT:w" s="T178">Onton</ts>
                  <nts id="Seg_629" n="HIAT:ip">,</nts>
                  <nts id="Seg_630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_631" n="HIAT:ip">"</nts>
                  <ts e="T180" id="Seg_633" n="HIAT:w" s="T179">dʼe</ts>
                  <nts id="Seg_634" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_636" n="HIAT:w" s="T180">nʼɨmalanaːrɨ</ts>
                  <nts id="Seg_637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_639" n="HIAT:w" s="T181">gɨnna</ts>
                  <nts id="Seg_640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_642" n="HIAT:w" s="T182">bɨhɨlaːk</ts>
                  <nts id="Seg_643" n="HIAT:ip">"</nts>
                  <nts id="Seg_644" n="HIAT:ip">,</nts>
                  <nts id="Seg_645" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_647" n="HIAT:w" s="T183">diː</ts>
                  <nts id="Seg_648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_650" n="HIAT:w" s="T184">hanaːta</ts>
                  <nts id="Seg_651" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_653" n="HIAT:w" s="T185">u͡olbut</ts>
                  <nts id="Seg_654" n="HIAT:ip">.</nts>
                  <nts id="Seg_655" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T188" id="Seg_657" n="HIAT:u" s="T186">
                  <ts e="T187" id="Seg_659" n="HIAT:w" s="T186">Kirdik</ts>
                  <nts id="Seg_660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_662" n="HIAT:w" s="T187">ebit</ts>
                  <nts id="Seg_663" n="HIAT:ip">.</nts>
                  <nts id="Seg_664" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T194" id="Seg_666" n="HIAT:u" s="T188">
                  <ts e="T189" id="Seg_668" n="HIAT:w" s="T188">Kɨːspɨt</ts>
                  <nts id="Seg_669" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_671" n="HIAT:w" s="T189">agata</ts>
                  <nts id="Seg_672" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_674" n="HIAT:w" s="T190">tuːgu</ts>
                  <nts id="Seg_675" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_677" n="HIAT:w" s="T191">ere</ts>
                  <nts id="Seg_678" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_680" n="HIAT:w" s="T192">bɨrakta</ts>
                  <nts id="Seg_681" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_683" n="HIAT:w" s="T193">eːt</ts>
                  <nts id="Seg_684" n="HIAT:ip">:</nts>
                  <nts id="Seg_685" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T203" id="Seg_687" n="HIAT:u" s="T194">
                  <nts id="Seg_688" n="HIAT:ip">"</nts>
                  <ts e="T195" id="Seg_690" n="HIAT:w" s="T194">Čɨk</ts>
                  <nts id="Seg_691" n="HIAT:ip">"</nts>
                  <nts id="Seg_692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_694" n="HIAT:w" s="T195">gɨnan</ts>
                  <nts id="Seg_695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_697" n="HIAT:w" s="T196">keːste</ts>
                  <nts id="Seg_698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_700" n="HIAT:w" s="T197">bu</ts>
                  <nts id="Seg_701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_703" n="HIAT:w" s="T198">ačaːk</ts>
                  <nts id="Seg_704" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_706" n="HIAT:w" s="T199">ürdütünen</ts>
                  <nts id="Seg_707" n="HIAT:ip">,</nts>
                  <nts id="Seg_708" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_710" n="HIAT:w" s="T200">diː</ts>
                  <nts id="Seg_711" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_713" n="HIAT:w" s="T201">hanaːta</ts>
                  <nts id="Seg_714" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_716" n="HIAT:w" s="T202">onton</ts>
                  <nts id="Seg_717" n="HIAT:ip">:</nts>
                  <nts id="Seg_718" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T211" id="Seg_720" n="HIAT:u" s="T203">
                  <nts id="Seg_721" n="HIAT:ip">"</nts>
                  <ts e="T204" id="Seg_723" n="HIAT:w" s="T203">Tutu͡o</ts>
                  <nts id="Seg_724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_726" n="HIAT:w" s="T204">duː</ts>
                  <nts id="Seg_727" n="HIAT:ip">,</nts>
                  <nts id="Seg_728" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_730" n="HIAT:w" s="T205">tutu͡o</ts>
                  <nts id="Seg_731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_733" n="HIAT:w" s="T206">du</ts>
                  <nts id="Seg_734" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_736" n="HIAT:w" s="T207">hu͡oga</ts>
                  <nts id="Seg_737" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_739" n="HIAT:w" s="T208">duː</ts>
                  <nts id="Seg_740" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_742" n="HIAT:w" s="T209">bu</ts>
                  <nts id="Seg_743" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_745" n="HIAT:w" s="T210">oŋu͡ogu</ts>
                  <nts id="Seg_746" n="HIAT:ip">.</nts>
                  <nts id="Seg_747" n="HIAT:ip">"</nts>
                  <nts id="Seg_748" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T218" id="Seg_750" n="HIAT:u" s="T211">
                  <ts e="T212" id="Seg_752" n="HIAT:w" s="T211">Ka</ts>
                  <nts id="Seg_753" n="HIAT:ip">,</nts>
                  <nts id="Seg_754" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_756" n="HIAT:w" s="T212">u͡olbut</ts>
                  <nts id="Seg_757" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_759" n="HIAT:w" s="T213">dʼe</ts>
                  <nts id="Seg_760" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_762" n="HIAT:w" s="T214">türgen</ts>
                  <nts id="Seg_763" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_765" n="HIAT:w" s="T215">bagajdɨk</ts>
                  <nts id="Seg_766" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_768" n="HIAT:w" s="T216">tutar</ts>
                  <nts id="Seg_769" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_771" n="HIAT:w" s="T217">diːn</ts>
                  <nts id="Seg_772" n="HIAT:ip">.</nts>
                  <nts id="Seg_773" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T222" id="Seg_775" n="HIAT:u" s="T218">
                  <ts e="T219" id="Seg_777" n="HIAT:w" s="T218">Onu</ts>
                  <nts id="Seg_778" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_780" n="HIAT:w" s="T219">ɨjɨtar</ts>
                  <nts id="Seg_781" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_783" n="HIAT:w" s="T220">kɨːspɨt</ts>
                  <nts id="Seg_784" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_786" n="HIAT:w" s="T221">agata</ts>
                  <nts id="Seg_787" n="HIAT:ip">:</nts>
                  <nts id="Seg_788" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T226" id="Seg_790" n="HIAT:u" s="T222">
                  <nts id="Seg_791" n="HIAT:ip">"</nts>
                  <ts e="T223" id="Seg_793" n="HIAT:w" s="T222">Dʼe</ts>
                  <nts id="Seg_794" n="HIAT:ip">,</nts>
                  <nts id="Seg_795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_797" n="HIAT:w" s="T223">tugu</ts>
                  <nts id="Seg_798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_800" n="HIAT:w" s="T224">tuttuŋ</ts>
                  <nts id="Seg_801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_803" n="HIAT:w" s="T225">iti</ts>
                  <nts id="Seg_804" n="HIAT:ip">?</nts>
                  <nts id="Seg_805" n="HIAT:ip">"</nts>
                  <nts id="Seg_806" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T234" id="Seg_808" n="HIAT:u" s="T226">
                  <nts id="Seg_809" n="HIAT:ip">"</nts>
                  <ts e="T227" id="Seg_811" n="HIAT:w" s="T226">Kajtak</ts>
                  <nts id="Seg_812" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_814" n="HIAT:w" s="T227">tuːgu</ts>
                  <nts id="Seg_815" n="HIAT:ip">"</nts>
                  <nts id="Seg_816" n="HIAT:ip">,</nts>
                  <nts id="Seg_817" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_819" n="HIAT:w" s="T228">diː</ts>
                  <nts id="Seg_820" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_822" n="HIAT:w" s="T229">ɨjɨtta</ts>
                  <nts id="Seg_823" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_825" n="HIAT:w" s="T230">ol</ts>
                  <nts id="Seg_826" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_828" n="HIAT:w" s="T231">ke</ts>
                  <nts id="Seg_829" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_831" n="HIAT:w" s="T232">kim</ts>
                  <nts id="Seg_832" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_834" n="HIAT:w" s="T233">u͡olbut</ts>
                  <nts id="Seg_835" n="HIAT:ip">.</nts>
                  <nts id="Seg_836" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T247" id="Seg_838" n="HIAT:u" s="T234">
                  <nts id="Seg_839" n="HIAT:ip">"</nts>
                  <ts e="T235" id="Seg_841" n="HIAT:w" s="T234">Min</ts>
                  <nts id="Seg_842" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_843" n="HIAT:ip">(</nts>
                  <ts e="T237" id="Seg_845" n="HIAT:w" s="T235">sra-</ts>
                  <nts id="Seg_846" n="HIAT:ip">)</nts>
                  <nts id="Seg_847" n="HIAT:ip">,</nts>
                  <nts id="Seg_848" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_850" n="HIAT:w" s="T237">min</ts>
                  <nts id="Seg_851" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_853" n="HIAT:w" s="T238">kördüm</ts>
                  <nts id="Seg_854" n="HIAT:ip">,</nts>
                  <nts id="Seg_855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_857" n="HIAT:w" s="T239">taːjdɨm</ts>
                  <nts id="Seg_858" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_859" n="HIAT:ip">–</nts>
                  <nts id="Seg_860" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_862" n="HIAT:w" s="T240">tobuk</ts>
                  <nts id="Seg_863" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_865" n="HIAT:w" s="T241">oŋu͡oga</ts>
                  <nts id="Seg_866" n="HIAT:ip">,</nts>
                  <nts id="Seg_867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_869" n="HIAT:w" s="T242">kim</ts>
                  <nts id="Seg_870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_872" n="HIAT:w" s="T243">taːjbat</ts>
                  <nts id="Seg_873" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_875" n="HIAT:w" s="T244">ol</ts>
                  <nts id="Seg_876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_878" n="HIAT:w" s="T245">tobuk</ts>
                  <nts id="Seg_879" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_881" n="HIAT:w" s="T246">oŋu͡ogun</ts>
                  <nts id="Seg_882" n="HIAT:ip">?</nts>
                  <nts id="Seg_883" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T253" id="Seg_885" n="HIAT:u" s="T247">
                  <ts e="T248" id="Seg_887" n="HIAT:w" s="T247">Kim</ts>
                  <nts id="Seg_888" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_890" n="HIAT:w" s="T248">taːjɨmɨ͡aj</ts>
                  <nts id="Seg_891" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_893" n="HIAT:w" s="T249">ol</ts>
                  <nts id="Seg_894" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_896" n="HIAT:w" s="T250">tobuk</ts>
                  <nts id="Seg_897" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_899" n="HIAT:w" s="T251">oŋu͡ogun</ts>
                  <nts id="Seg_900" n="HIAT:ip">"</nts>
                  <nts id="Seg_901" n="HIAT:ip">,</nts>
                  <nts id="Seg_902" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_904" n="HIAT:w" s="T252">di͡ete</ts>
                  <nts id="Seg_905" n="HIAT:ip">.</nts>
                  <nts id="Seg_906" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T259" id="Seg_908" n="HIAT:u" s="T253">
                  <ts e="T254" id="Seg_910" n="HIAT:w" s="T253">Agabɨt</ts>
                  <nts id="Seg_911" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_912" n="HIAT:ip">(</nts>
                  <ts e="T256" id="Seg_914" n="HIAT:w" s="T254">nʼɨmaŋ-</ts>
                  <nts id="Seg_915" n="HIAT:ip">)</nts>
                  <nts id="Seg_916" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_918" n="HIAT:w" s="T256">nʼɨmaŋnɨː-nʼɨmaŋnɨː</ts>
                  <nts id="Seg_919" n="HIAT:ip">,</nts>
                  <nts id="Seg_920" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_922" n="HIAT:w" s="T257">ɨmaŋnɨː-ɨmaŋnɨː</ts>
                  <nts id="Seg_923" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_925" n="HIAT:w" s="T258">di͡ete</ts>
                  <nts id="Seg_926" n="HIAT:ip">:</nts>
                  <nts id="Seg_927" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T264" id="Seg_929" n="HIAT:u" s="T259">
                  <nts id="Seg_930" n="HIAT:ip">"</nts>
                  <ts e="T260" id="Seg_932" n="HIAT:w" s="T259">Dʼe</ts>
                  <nts id="Seg_933" n="HIAT:ip">,</nts>
                  <nts id="Seg_934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_936" n="HIAT:w" s="T260">taːjdɨŋ</ts>
                  <nts id="Seg_937" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_939" n="HIAT:w" s="T261">tobuk</ts>
                  <nts id="Seg_940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_942" n="HIAT:w" s="T262">oŋu͡ogun</ts>
                  <nts id="Seg_943" n="HIAT:ip">,</nts>
                  <nts id="Seg_944" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_946" n="HIAT:w" s="T263">üčügej</ts>
                  <nts id="Seg_947" n="HIAT:ip">.</nts>
                  <nts id="Seg_948" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T280" id="Seg_950" n="HIAT:u" s="T264">
                  <ts e="T265" id="Seg_952" n="HIAT:w" s="T264">Dʼe</ts>
                  <nts id="Seg_953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_955" n="HIAT:w" s="T265">bu</ts>
                  <nts id="Seg_956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_957" n="HIAT:ip">(</nts>
                  <ts e="T268" id="Seg_959" n="HIAT:w" s="T266">tobu-</ts>
                  <nts id="Seg_960" n="HIAT:ip">)</nts>
                  <nts id="Seg_961" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_963" n="HIAT:w" s="T268">tobuk</ts>
                  <nts id="Seg_964" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_966" n="HIAT:w" s="T269">oŋu͡ogugar</ts>
                  <nts id="Seg_967" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_969" n="HIAT:w" s="T270">kantan</ts>
                  <nts id="Seg_970" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_972" n="HIAT:w" s="T271">emete</ts>
                  <nts id="Seg_973" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_974" n="HIAT:ip">(</nts>
                  <ts e="T274" id="Seg_976" n="HIAT:w" s="T272">köröŋŋ-</ts>
                  <nts id="Seg_977" n="HIAT:ip">)</nts>
                  <nts id="Seg_978" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_980" n="HIAT:w" s="T274">köröŋŋün</ts>
                  <nts id="Seg_981" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_983" n="HIAT:w" s="T275">biːr</ts>
                  <nts id="Seg_984" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_986" n="HIAT:w" s="T276">emete</ts>
                  <nts id="Seg_987" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_989" n="HIAT:w" s="T277">iŋiːri</ts>
                  <nts id="Seg_990" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_992" n="HIAT:w" s="T278">bulu͡oŋ</ts>
                  <nts id="Seg_993" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_995" n="HIAT:w" s="T279">dʼuru</ts>
                  <nts id="Seg_996" n="HIAT:ip">?</nts>
                  <nts id="Seg_997" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T286" id="Seg_999" n="HIAT:u" s="T280">
                  <ts e="T281" id="Seg_1001" n="HIAT:w" s="T280">Oččogo</ts>
                  <nts id="Seg_1002" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_1004" n="HIAT:w" s="T281">min</ts>
                  <nts id="Seg_1005" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_1007" n="HIAT:w" s="T282">haŋabɨn</ts>
                  <nts id="Seg_1008" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_1010" n="HIAT:w" s="T283">ɨlɨ͡am</ts>
                  <nts id="Seg_1011" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_1013" n="HIAT:w" s="T284">hu͡oga</ts>
                  <nts id="Seg_1014" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_1016" n="HIAT:w" s="T285">töttörü</ts>
                  <nts id="Seg_1017" n="HIAT:ip">.</nts>
                  <nts id="Seg_1018" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T295" id="Seg_1020" n="HIAT:u" s="T286">
                  <ts e="T287" id="Seg_1022" n="HIAT:w" s="T286">Bullakkɨna</ts>
                  <nts id="Seg_1023" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_1025" n="HIAT:w" s="T287">biːr</ts>
                  <nts id="Seg_1026" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_1028" n="HIAT:w" s="T288">emete</ts>
                  <nts id="Seg_1029" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_1031" n="HIAT:w" s="T289">iŋiːri</ts>
                  <nts id="Seg_1032" n="HIAT:ip">,</nts>
                  <nts id="Seg_1033" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_1035" n="HIAT:w" s="T290">oččogo</ts>
                  <nts id="Seg_1036" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_1038" n="HIAT:w" s="T291">min</ts>
                  <nts id="Seg_1039" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_1041" n="HIAT:w" s="T292">kɨːspɨn</ts>
                  <nts id="Seg_1042" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_1044" n="HIAT:w" s="T293">eni͡eke</ts>
                  <nts id="Seg_1045" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1047" n="HIAT:w" s="T294">bi͡eri͡em</ts>
                  <nts id="Seg_1048" n="HIAT:ip">.</nts>
                  <nts id="Seg_1049" n="HIAT:ip">"</nts>
                  <nts id="Seg_1050" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T301" id="Seg_1052" n="HIAT:u" s="T295">
                  <ts e="T296" id="Seg_1054" n="HIAT:w" s="T295">Dʼe</ts>
                  <nts id="Seg_1055" n="HIAT:ip">,</nts>
                  <nts id="Seg_1056" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_1058" n="HIAT:w" s="T296">u͡olbut</ts>
                  <nts id="Seg_1059" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_1061" n="HIAT:w" s="T297">körö</ts>
                  <nts id="Seg_1062" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_1064" n="HIAT:w" s="T298">hataːta</ts>
                  <nts id="Seg_1065" n="HIAT:ip">,</nts>
                  <nts id="Seg_1066" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_1068" n="HIAT:w" s="T299">körö</ts>
                  <nts id="Seg_1069" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_1071" n="HIAT:w" s="T300">hataːta</ts>
                  <nts id="Seg_1072" n="HIAT:ip">.</nts>
                  <nts id="Seg_1073" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T306" id="Seg_1075" n="HIAT:u" s="T301">
                  <ts e="T302" id="Seg_1077" n="HIAT:w" s="T301">Onton</ts>
                  <nts id="Seg_1078" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1080" n="HIAT:w" s="T302">orguːjkaːnnɨk</ts>
                  <nts id="Seg_1081" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_1083" n="HIAT:w" s="T303">bejete</ts>
                  <nts id="Seg_1084" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_1086" n="HIAT:w" s="T304">bejetiger</ts>
                  <nts id="Seg_1087" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_1089" n="HIAT:w" s="T305">haŋarar</ts>
                  <nts id="Seg_1090" n="HIAT:ip">:</nts>
                  <nts id="Seg_1091" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T318" id="Seg_1093" n="HIAT:u" s="T306">
                  <nts id="Seg_1094" n="HIAT:ip">"</nts>
                  <ts e="T307" id="Seg_1096" n="HIAT:w" s="T306">O</ts>
                  <nts id="Seg_1097" n="HIAT:ip">,</nts>
                  <nts id="Seg_1098" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_1100" n="HIAT:w" s="T307">dʼe</ts>
                  <nts id="Seg_1101" n="HIAT:ip">,</nts>
                  <nts id="Seg_1102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1104" n="HIAT:w" s="T308">hürdeːk</ts>
                  <nts id="Seg_1105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_1107" n="HIAT:w" s="T309">ebit</ts>
                  <nts id="Seg_1108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_1110" n="HIAT:w" s="T310">dʼe</ts>
                  <nts id="Seg_1111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1112" n="HIAT:ip">–</nts>
                  <nts id="Seg_1113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_1115" n="HIAT:w" s="T311">ile</ts>
                  <nts id="Seg_1116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_1118" n="HIAT:w" s="T312">taːska</ts>
                  <nts id="Seg_1119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_1121" n="HIAT:w" s="T313">talɨ</ts>
                  <nts id="Seg_1122" n="HIAT:ip">,</nts>
                  <nts id="Seg_1123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_1125" n="HIAT:w" s="T314">tu͡ok</ts>
                  <nts id="Seg_1126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1128" n="HIAT:w" s="T315">basku͡oj</ts>
                  <nts id="Seg_1129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_1131" n="HIAT:w" s="T316">oŋu͡ogaj</ts>
                  <nts id="Seg_1132" n="HIAT:ip">,</nts>
                  <nts id="Seg_1133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_1135" n="HIAT:w" s="T317">ogolor</ts>
                  <nts id="Seg_1136" n="HIAT:ip">.</nts>
                  <nts id="Seg_1137" n="HIAT:ip">"</nts>
                  <nts id="Seg_1138" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T322" id="Seg_1140" n="HIAT:u" s="T318">
                  <ts e="T319" id="Seg_1142" n="HIAT:w" s="T318">Ontuŋ</ts>
                  <nts id="Seg_1143" n="HIAT:ip">,</nts>
                  <nts id="Seg_1144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_1146" n="HIAT:w" s="T319">kaja</ts>
                  <nts id="Seg_1147" n="HIAT:ip">,</nts>
                  <nts id="Seg_1148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1150" n="HIAT:w" s="T320">bilbet</ts>
                  <nts id="Seg_1151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_1153" n="HIAT:w" s="T321">ebit</ts>
                  <nts id="Seg_1154" n="HIAT:ip">.</nts>
                  <nts id="Seg_1155" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T337" id="Seg_1157" n="HIAT:u" s="T322">
                  <ts e="T323" id="Seg_1159" n="HIAT:w" s="T322">Gini</ts>
                  <nts id="Seg_1160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_1162" n="HIAT:w" s="T323">gini</ts>
                  <nts id="Seg_1163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1165" n="HIAT:w" s="T324">iti</ts>
                  <nts id="Seg_1166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_1168" n="HIAT:w" s="T325">agalara</ts>
                  <nts id="Seg_1169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1171" n="HIAT:w" s="T326">ere</ts>
                  <nts id="Seg_1172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_1174" n="HIAT:w" s="T327">iti</ts>
                  <nts id="Seg_1175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_1177" n="HIAT:w" s="T328">biler</ts>
                  <nts id="Seg_1178" n="HIAT:ip">,</nts>
                  <nts id="Seg_1179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_1181" n="HIAT:w" s="T329">biːr</ts>
                  <nts id="Seg_1182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_1184" n="HIAT:w" s="T330">hire</ts>
                  <nts id="Seg_1185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_1187" n="HIAT:w" s="T331">ontuŋ</ts>
                  <nts id="Seg_1188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_1190" n="HIAT:w" s="T332">amattan</ts>
                  <nts id="Seg_1191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_1193" n="HIAT:w" s="T333">biːr</ts>
                  <nts id="Seg_1194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_1196" n="HIAT:w" s="T334">daː</ts>
                  <nts id="Seg_1197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_1199" n="HIAT:w" s="T335">iŋiːre</ts>
                  <nts id="Seg_1200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_1202" n="HIAT:w" s="T336">hu͡ok</ts>
                  <nts id="Seg_1203" n="HIAT:ip">.</nts>
                  <nts id="Seg_1204" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T353" id="Seg_1206" n="HIAT:u" s="T337">
                  <ts e="T338" id="Seg_1208" n="HIAT:w" s="T337">Biːrger</ts>
                  <nts id="Seg_1209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_1211" n="HIAT:w" s="T338">sirin</ts>
                  <nts id="Seg_1212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_1214" n="HIAT:w" s="T339">körü͡öŋ</ts>
                  <nts id="Seg_1215" n="HIAT:ip">,</nts>
                  <nts id="Seg_1216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_1218" n="HIAT:w" s="T340">üčügejkeːnnik</ts>
                  <nts id="Seg_1219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_1221" n="HIAT:w" s="T341">kördökkö</ts>
                  <nts id="Seg_1222" n="HIAT:ip">,</nts>
                  <nts id="Seg_1223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1225" n="HIAT:w" s="T342">ontuŋ</ts>
                  <nts id="Seg_1226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_1228" n="HIAT:w" s="T343">kɨːs</ts>
                  <nts id="Seg_1229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1230" n="HIAT:ip">(</nts>
                  <ts e="T346" id="Seg_1232" n="HIAT:w" s="T344">hɨr-</ts>
                  <nts id="Seg_1233" n="HIAT:ip">)</nts>
                  <nts id="Seg_1234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_1236" n="HIAT:w" s="T346">hɨrajɨgar</ts>
                  <nts id="Seg_1237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1239" n="HIAT:w" s="T347">talɨ</ts>
                  <nts id="Seg_1240" n="HIAT:ip">,</nts>
                  <nts id="Seg_1241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_1243" n="HIAT:w" s="T348">dʼaktar</ts>
                  <nts id="Seg_1244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_1246" n="HIAT:w" s="T349">hɨrajɨgar</ts>
                  <nts id="Seg_1247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_1249" n="HIAT:w" s="T350">talɨ</ts>
                  <nts id="Seg_1250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1251" n="HIAT:ip">–</nts>
                  <nts id="Seg_1252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_1254" n="HIAT:w" s="T351">biːrges</ts>
                  <nts id="Seg_1255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_1257" n="HIAT:w" s="T352">öttüge</ts>
                  <nts id="Seg_1258" n="HIAT:ip">.</nts>
                  <nts id="Seg_1259" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T358" id="Seg_1261" n="HIAT:u" s="T353">
                  <ts e="T354" id="Seg_1263" n="HIAT:w" s="T353">Ontugun</ts>
                  <nts id="Seg_1264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_1266" n="HIAT:w" s="T354">agabɨt</ts>
                  <nts id="Seg_1267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_1269" n="HIAT:w" s="T355">ire</ts>
                  <nts id="Seg_1270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_1272" n="HIAT:w" s="T356">biler</ts>
                  <nts id="Seg_1273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_1275" n="HIAT:w" s="T357">bu͡o</ts>
                  <nts id="Seg_1276" n="HIAT:ip">.</nts>
                  <nts id="Seg_1277" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T365" id="Seg_1279" n="HIAT:u" s="T358">
                  <ts e="T359" id="Seg_1281" n="HIAT:w" s="T358">Dʼe</ts>
                  <nts id="Seg_1282" n="HIAT:ip">,</nts>
                  <nts id="Seg_1283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_1285" n="HIAT:w" s="T359">kajdi͡ek</ts>
                  <nts id="Seg_1286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_1288" n="HIAT:w" s="T360">de</ts>
                  <nts id="Seg_1289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_1291" n="HIAT:w" s="T361">bu͡olu͡ogun</ts>
                  <nts id="Seg_1292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_1294" n="HIAT:w" s="T362">bert</ts>
                  <nts id="Seg_1295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_1297" n="HIAT:w" s="T363">Bulčut</ts>
                  <nts id="Seg_1298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_1300" n="HIAT:w" s="T364">u͡olbut</ts>
                  <nts id="Seg_1301" n="HIAT:ip">.</nts>
                  <nts id="Seg_1302" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T369" id="Seg_1304" n="HIAT:u" s="T365">
                  <ts e="T366" id="Seg_1306" n="HIAT:w" s="T365">Kɨjŋanaːrɨ</ts>
                  <nts id="Seg_1307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_1309" n="HIAT:w" s="T366">da</ts>
                  <nts id="Seg_1310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_1312" n="HIAT:w" s="T367">gɨnar</ts>
                  <nts id="Seg_1313" n="HIAT:ip">,</nts>
                  <nts id="Seg_1314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_1316" n="HIAT:w" s="T368">kečeher</ts>
                  <nts id="Seg_1317" n="HIAT:ip">.</nts>
                  <nts id="Seg_1318" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T376" id="Seg_1320" n="HIAT:u" s="T369">
                  <ts e="T370" id="Seg_1322" n="HIAT:w" s="T369">Onton</ts>
                  <nts id="Seg_1323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_1325" n="HIAT:w" s="T370">dʼe</ts>
                  <nts id="Seg_1326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_1328" n="HIAT:w" s="T371">kɨlas</ts>
                  <nts id="Seg_1329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_1331" n="HIAT:w" s="T372">gɨnar</ts>
                  <nts id="Seg_1332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_1334" n="HIAT:w" s="T373">diːn</ts>
                  <nts id="Seg_1335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_1337" n="HIAT:w" s="T374">kɨːhɨn</ts>
                  <nts id="Seg_1338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_1340" n="HIAT:w" s="T375">di͡et</ts>
                  <nts id="Seg_1341" n="HIAT:ip">.</nts>
                  <nts id="Seg_1342" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T379" id="Seg_1344" n="HIAT:u" s="T376">
                  <ts e="T377" id="Seg_1346" n="HIAT:w" s="T376">Kɨːha</ts>
                  <nts id="Seg_1347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378" id="Seg_1349" n="HIAT:w" s="T377">nʼɨmalanna</ts>
                  <nts id="Seg_1350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_1352" n="HIAT:w" s="T378">emi͡e</ts>
                  <nts id="Seg_1353" n="HIAT:ip">.</nts>
                  <nts id="Seg_1354" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T390" id="Seg_1356" n="HIAT:u" s="T379">
                  <nts id="Seg_1357" n="HIAT:ip">"</nts>
                  <ts e="T380" id="Seg_1359" n="HIAT:w" s="T379">Kajtak</ts>
                  <nts id="Seg_1360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_1362" n="HIAT:w" s="T380">gɨnɨ͡amɨj</ts>
                  <nts id="Seg_1363" n="HIAT:ip">"</nts>
                  <nts id="Seg_1364" n="HIAT:ip">,</nts>
                  <nts id="Seg_1365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T382" id="Seg_1367" n="HIAT:w" s="T381">di͡en</ts>
                  <nts id="Seg_1368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_1370" n="HIAT:w" s="T382">baran</ts>
                  <nts id="Seg_1371" n="HIAT:ip">,</nts>
                  <nts id="Seg_1372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_1374" n="HIAT:w" s="T383">kumaːrdartan</ts>
                  <nts id="Seg_1375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_1377" n="HIAT:w" s="T384">ilbiner</ts>
                  <nts id="Seg_1378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T386" id="Seg_1380" n="HIAT:w" s="T385">kördük</ts>
                  <nts id="Seg_1381" n="HIAT:ip">,</nts>
                  <nts id="Seg_1382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_1384" n="HIAT:w" s="T386">iliːtin</ts>
                  <nts id="Seg_1385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_1387" n="HIAT:w" s="T387">orguːjkaːnnɨk</ts>
                  <nts id="Seg_1388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_1390" n="HIAT:w" s="T388">bugurdukkaːn</ts>
                  <nts id="Seg_1391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_1393" n="HIAT:w" s="T389">ɨlla</ts>
                  <nts id="Seg_1394" n="HIAT:ip">.</nts>
                  <nts id="Seg_1395" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T403" id="Seg_1397" n="HIAT:u" s="T390">
                  <ts e="T391" id="Seg_1399" n="HIAT:w" s="T390">Onton</ts>
                  <nts id="Seg_1400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_1402" n="HIAT:w" s="T391">bu</ts>
                  <nts id="Seg_1403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T393" id="Seg_1405" n="HIAT:w" s="T392">orguːjkaːnnɨk</ts>
                  <nts id="Seg_1406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_1408" n="HIAT:w" s="T393">bu</ts>
                  <nts id="Seg_1409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_1411" n="HIAT:w" s="T394">bugurdukkaːn</ts>
                  <nts id="Seg_1412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1413" n="HIAT:ip">(</nts>
                  <ts e="T396" id="Seg_1415" n="HIAT:w" s="T395">hɨrajɨnan</ts>
                  <nts id="Seg_1416" n="HIAT:ip">)</nts>
                  <nts id="Seg_1417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_1419" n="HIAT:w" s="T396">hɨrajɨn</ts>
                  <nts id="Seg_1420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_1422" n="HIAT:w" s="T397">dʼe</ts>
                  <nts id="Seg_1423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_1425" n="HIAT:w" s="T398">kimniːr</ts>
                  <nts id="Seg_1426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T400" id="Seg_1428" n="HIAT:w" s="T399">köllörör</ts>
                  <nts id="Seg_1429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1430" n="HIAT:ip">–</nts>
                  <nts id="Seg_1431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_1433" n="HIAT:w" s="T400">kumaːrdartan</ts>
                  <nts id="Seg_1434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T402" id="Seg_1436" n="HIAT:w" s="T401">tebenerge</ts>
                  <nts id="Seg_1437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_1439" n="HIAT:w" s="T402">talɨ</ts>
                  <nts id="Seg_1440" n="HIAT:ip">.</nts>
                  <nts id="Seg_1441" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T409" id="Seg_1443" n="HIAT:u" s="T403">
                  <ts e="T404" id="Seg_1445" n="HIAT:w" s="T403">Oː</ts>
                  <nts id="Seg_1446" n="HIAT:ip">,</nts>
                  <nts id="Seg_1447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T405" id="Seg_1449" n="HIAT:w" s="T404">dʼe</ts>
                  <nts id="Seg_1450" n="HIAT:ip">,</nts>
                  <nts id="Seg_1451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_1453" n="HIAT:w" s="T405">u͡olbut</ts>
                  <nts id="Seg_1454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T407" id="Seg_1456" n="HIAT:w" s="T406">dʼe</ts>
                  <nts id="Seg_1457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T408" id="Seg_1459" n="HIAT:w" s="T407">ü͡örer</ts>
                  <nts id="Seg_1460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T409" id="Seg_1462" n="HIAT:w" s="T408">diːn</ts>
                  <nts id="Seg_1463" n="HIAT:ip">.</nts>
                  <nts id="Seg_1464" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T418" id="Seg_1466" n="HIAT:u" s="T409">
                  <ts e="T410" id="Seg_1468" n="HIAT:w" s="T409">Türgen</ts>
                  <nts id="Seg_1469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T411" id="Seg_1471" n="HIAT:w" s="T410">bagajɨtɨk</ts>
                  <nts id="Seg_1472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T412" id="Seg_1474" n="HIAT:w" s="T411">bahagɨn</ts>
                  <nts id="Seg_1475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T413" id="Seg_1477" n="HIAT:w" s="T412">ɨlla</ts>
                  <nts id="Seg_1478" n="HIAT:ip">,</nts>
                  <nts id="Seg_1479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414" id="Seg_1481" n="HIAT:w" s="T413">onton</ts>
                  <nts id="Seg_1482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T415" id="Seg_1484" n="HIAT:w" s="T414">orguːjkaːnnɨk</ts>
                  <nts id="Seg_1485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T416" id="Seg_1487" n="HIAT:w" s="T415">kötöktö</ts>
                  <nts id="Seg_1488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T417" id="Seg_1490" n="HIAT:w" s="T416">tobuk</ts>
                  <nts id="Seg_1491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T418" id="Seg_1493" n="HIAT:w" s="T417">oŋu͡ogun</ts>
                  <nts id="Seg_1494" n="HIAT:ip">.</nts>
                  <nts id="Seg_1495" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T428" id="Seg_1497" n="HIAT:u" s="T418">
                  <ts e="T419" id="Seg_1499" n="HIAT:w" s="T418">Bɨ͡aj</ts>
                  <nts id="Seg_1500" n="HIAT:ip">,</nts>
                  <nts id="Seg_1501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T420" id="Seg_1503" n="HIAT:w" s="T419">türgen</ts>
                  <nts id="Seg_1504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_1506" n="HIAT:w" s="T420">türgen</ts>
                  <nts id="Seg_1507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T422" id="Seg_1509" n="HIAT:w" s="T421">bagajtɨk</ts>
                  <nts id="Seg_1510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T423" id="Seg_1512" n="HIAT:w" s="T422">bahagɨnan</ts>
                  <nts id="Seg_1513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T424" id="Seg_1515" n="HIAT:w" s="T423">iŋiːr</ts>
                  <nts id="Seg_1516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T425" id="Seg_1518" n="HIAT:w" s="T424">munʼaːnɨ</ts>
                  <nts id="Seg_1519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T426" id="Seg_1521" n="HIAT:w" s="T425">tardan</ts>
                  <nts id="Seg_1522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T427" id="Seg_1524" n="HIAT:w" s="T426">ɨlbat</ts>
                  <nts id="Seg_1525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T428" id="Seg_1527" n="HIAT:w" s="T427">du͡o</ts>
                  <nts id="Seg_1528" n="HIAT:ip">.</nts>
                  <nts id="Seg_1529" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T431" id="Seg_1531" n="HIAT:u" s="T428">
                  <ts e="T429" id="Seg_1533" n="HIAT:w" s="T428">Onno</ts>
                  <nts id="Seg_1534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_1536" n="HIAT:w" s="T429">agata</ts>
                  <nts id="Seg_1537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T431" id="Seg_1539" n="HIAT:w" s="T430">di͡ete</ts>
                  <nts id="Seg_1540" n="HIAT:ip">:</nts>
                  <nts id="Seg_1541" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T448" id="Seg_1543" n="HIAT:u" s="T431">
                  <nts id="Seg_1544" n="HIAT:ip">"</nts>
                  <ts e="T432" id="Seg_1546" n="HIAT:w" s="T431">Dʼe</ts>
                  <nts id="Seg_1547" n="HIAT:ip">,</nts>
                  <nts id="Seg_1548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T433" id="Seg_1550" n="HIAT:w" s="T432">kajtak</ts>
                  <nts id="Seg_1551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T434" id="Seg_1553" n="HIAT:w" s="T433">gɨnɨ͡aŋɨj</ts>
                  <nts id="Seg_1554" n="HIAT:ip">,</nts>
                  <nts id="Seg_1555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T435" id="Seg_1557" n="HIAT:w" s="T434">min</ts>
                  <nts id="Seg_1558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_1560" n="HIAT:w" s="T435">di͡ebitim</ts>
                  <nts id="Seg_1561" n="HIAT:ip">,</nts>
                  <nts id="Seg_1562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T437" id="Seg_1564" n="HIAT:w" s="T436">tɨlbɨn</ts>
                  <nts id="Seg_1565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T438" id="Seg_1567" n="HIAT:w" s="T437">töttörü</ts>
                  <nts id="Seg_1568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T439" id="Seg_1570" n="HIAT:w" s="T438">ɨlɨ͡am</ts>
                  <nts id="Seg_1571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440" id="Seg_1573" n="HIAT:w" s="T439">hu͡oga</ts>
                  <nts id="Seg_1574" n="HIAT:ip">,</nts>
                  <nts id="Seg_1575" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_1577" n="HIAT:w" s="T440">itinnik</ts>
                  <nts id="Seg_1578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T442" id="Seg_1580" n="HIAT:w" s="T441">uːs</ts>
                  <nts id="Seg_1581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T443" id="Seg_1583" n="HIAT:w" s="T442">kihi</ts>
                  <nts id="Seg_1584" n="HIAT:ip">,</nts>
                  <nts id="Seg_1585" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T444" id="Seg_1587" n="HIAT:w" s="T443">itinnik</ts>
                  <nts id="Seg_1588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T445" id="Seg_1590" n="HIAT:w" s="T444">bulčut</ts>
                  <nts id="Seg_1591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T446" id="Seg_1593" n="HIAT:w" s="T445">kihi</ts>
                  <nts id="Seg_1594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T447" id="Seg_1596" n="HIAT:w" s="T446">barɨtɨn</ts>
                  <nts id="Seg_1597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T448" id="Seg_1599" n="HIAT:w" s="T447">biler</ts>
                  <nts id="Seg_1600" n="HIAT:ip">.</nts>
                  <nts id="Seg_1601" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T453" id="Seg_1603" n="HIAT:u" s="T448">
                  <ts e="T449" id="Seg_1605" n="HIAT:w" s="T448">Dʼe</ts>
                  <nts id="Seg_1606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T450" id="Seg_1608" n="HIAT:w" s="T449">bi͡erebin</ts>
                  <nts id="Seg_1609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T451" id="Seg_1611" n="HIAT:w" s="T450">eni͡eke</ts>
                  <nts id="Seg_1612" n="HIAT:ip">,</nts>
                  <nts id="Seg_1613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T452" id="Seg_1615" n="HIAT:w" s="T451">Bulčut</ts>
                  <nts id="Seg_1616" n="HIAT:ip">,</nts>
                  <nts id="Seg_1617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T453" id="Seg_1619" n="HIAT:w" s="T452">kɨːspɨn</ts>
                  <nts id="Seg_1620" n="HIAT:ip">.</nts>
                  <nts id="Seg_1621" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T461" id="Seg_1623" n="HIAT:u" s="T453">
                  <ts e="T454" id="Seg_1625" n="HIAT:w" s="T453">Eteŋŋetik</ts>
                  <nts id="Seg_1626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T455" id="Seg_1628" n="HIAT:w" s="T454">oloruŋ</ts>
                  <nts id="Seg_1629" n="HIAT:ip">,</nts>
                  <nts id="Seg_1630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T456" id="Seg_1632" n="HIAT:w" s="T455">üčügejdik</ts>
                  <nts id="Seg_1633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T457" id="Seg_1635" n="HIAT:w" s="T456">oloruŋ</ts>
                  <nts id="Seg_1636" n="HIAT:ip">,</nts>
                  <nts id="Seg_1637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T458" id="Seg_1639" n="HIAT:w" s="T457">ügüs</ts>
                  <nts id="Seg_1640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T459" id="Seg_1642" n="HIAT:w" s="T458">ogonu</ts>
                  <nts id="Seg_1643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T460" id="Seg_1645" n="HIAT:w" s="T459">ogolonuŋ</ts>
                  <nts id="Seg_1646" n="HIAT:ip">"</nts>
                  <nts id="Seg_1647" n="HIAT:ip">,</nts>
                  <nts id="Seg_1648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T461" id="Seg_1650" n="HIAT:w" s="T460">di͡ete</ts>
                  <nts id="Seg_1651" n="HIAT:ip">.</nts>
                  <nts id="Seg_1652" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T470" id="Seg_1654" n="HIAT:u" s="T461">
                  <ts e="T462" id="Seg_1656" n="HIAT:w" s="T461">Dʼe</ts>
                  <nts id="Seg_1657" n="HIAT:ip">,</nts>
                  <nts id="Seg_1658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T463" id="Seg_1660" n="HIAT:w" s="T462">ol</ts>
                  <nts id="Seg_1661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T464" id="Seg_1663" n="HIAT:w" s="T463">kördükkeːn</ts>
                  <nts id="Seg_1664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T465" id="Seg_1666" n="HIAT:w" s="T464">Bulčut</ts>
                  <nts id="Seg_1667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T466" id="Seg_1669" n="HIAT:w" s="T465">kɨːhɨn</ts>
                  <nts id="Seg_1670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T467" id="Seg_1672" n="HIAT:w" s="T466">kɨtta</ts>
                  <nts id="Seg_1673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T468" id="Seg_1675" n="HIAT:w" s="T467">üčügej</ts>
                  <nts id="Seg_1676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T469" id="Seg_1678" n="HIAT:w" s="T468">bagajtɨk</ts>
                  <nts id="Seg_1679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T470" id="Seg_1681" n="HIAT:w" s="T469">olorbuttara</ts>
                  <nts id="Seg_1682" n="HIAT:ip">.</nts>
                  <nts id="Seg_1683" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T470" id="Seg_1684" n="sc" s="T0">
               <ts e="T1" id="Seg_1686" n="e" s="T0">Min </ts>
               <ts e="T2" id="Seg_1688" n="e" s="T1">bu </ts>
               <ts e="T3" id="Seg_1690" n="e" s="T2">öhü </ts>
               <ts e="T4" id="Seg_1692" n="e" s="T3">istibitim </ts>
               <ts e="T5" id="Seg_1694" n="e" s="T4">kergetterbin </ts>
               <ts e="T7" id="Seg_1696" n="e" s="T5">gɨtta… </ts>
               <ts e="T8" id="Seg_1698" n="e" s="T7">Školaga </ts>
               <ts e="T9" id="Seg_1700" n="e" s="T8">kiːrer </ts>
               <ts e="T10" id="Seg_1702" n="e" s="T9">dʼɨlbar. </ts>
               <ts e="T11" id="Seg_1704" n="e" s="T10">Ontuŋ </ts>
               <ts e="T12" id="Seg_1706" n="e" s="T11">anɨga </ts>
               <ts e="T13" id="Seg_1708" n="e" s="T12">di͡eri </ts>
               <ts e="T14" id="Seg_1710" n="e" s="T13">öjbör </ts>
               <ts e="T15" id="Seg_1712" n="e" s="T14">baːr. </ts>
               <ts e="T16" id="Seg_1714" n="e" s="T15">Buːrdukkaːn, </ts>
               <ts e="T17" id="Seg_1716" n="e" s="T16">maːmam </ts>
               <ts e="T18" id="Seg_1718" n="e" s="T17">edʼiːje, </ts>
               <ts e="T19" id="Seg_1720" n="e" s="T18">Kɨlabɨja </ts>
               <ts e="T20" id="Seg_1722" n="e" s="T19">kepseːčči </ts>
               <ts e="T21" id="Seg_1724" n="e" s="T20">ete. </ts>
               <ts e="T22" id="Seg_1726" n="e" s="T21">ɨraːk-ɨraːk </ts>
               <ts e="T23" id="Seg_1728" n="e" s="T22">mu͡oraga, </ts>
               <ts e="T24" id="Seg_1730" n="e" s="T23">töröːbüt </ts>
               <ts e="T25" id="Seg_1732" n="e" s="T24">hiriger, </ts>
               <ts e="T26" id="Seg_1734" n="e" s="T25">bejetin </ts>
               <ts e="T27" id="Seg_1736" n="e" s="T26">töröːbüt </ts>
               <ts e="T28" id="Seg_1738" n="e" s="T27">hiriger </ts>
               <ts e="T29" id="Seg_1740" n="e" s="T28">kergetterin </ts>
               <ts e="T30" id="Seg_1742" n="e" s="T29">kɨtta </ts>
               <ts e="T31" id="Seg_1744" n="e" s="T30">ajannɨː </ts>
               <ts e="T32" id="Seg_1746" n="e" s="T31">hɨldʼɨbɨta </ts>
               <ts e="T33" id="Seg_1748" n="e" s="T32">basku͡oj </ts>
               <ts e="T34" id="Seg_1750" n="e" s="T33">bagajɨ </ts>
               <ts e="T35" id="Seg_1752" n="e" s="T34">u͡ol. </ts>
               <ts e="T36" id="Seg_1754" n="e" s="T35">Ontuŋ </ts>
               <ts e="T37" id="Seg_1756" n="e" s="T36">aːta </ts>
               <ts e="T38" id="Seg_1758" n="e" s="T37">Bulčut </ts>
               <ts e="T39" id="Seg_1760" n="e" s="T38">ete. </ts>
               <ts e="T40" id="Seg_1762" n="e" s="T39">Atɨn </ts>
               <ts e="T41" id="Seg_1764" n="e" s="T40">hirge, </ts>
               <ts e="T43" id="Seg_1766" n="e" s="T41">(gin-) </ts>
               <ts e="T44" id="Seg_1768" n="e" s="T43">ginilerten </ts>
               <ts e="T45" id="Seg_1770" n="e" s="T44">ɨraːkkaːn </ts>
               <ts e="T46" id="Seg_1772" n="e" s="T45">hahɨː </ts>
               <ts e="T47" id="Seg_1774" n="e" s="T46">hɨldʼɨbɨta </ts>
               <ts e="T48" id="Seg_1776" n="e" s="T47">basku͡oj </ts>
               <ts e="T49" id="Seg_1778" n="e" s="T48">kɨːs </ts>
               <ts e="T50" id="Seg_1780" n="e" s="T49">kergetterin </ts>
               <ts e="T51" id="Seg_1782" n="e" s="T50">kɨtta. </ts>
               <ts e="T52" id="Seg_1784" n="e" s="T51">Iti </ts>
               <ts e="T53" id="Seg_1786" n="e" s="T52">hiŋil </ts>
               <ts e="T54" id="Seg_1788" n="e" s="T53">eː </ts>
               <ts e="T55" id="Seg_1790" n="e" s="T54">u͡olu </ts>
               <ts e="T56" id="Seg_1792" n="e" s="T55">kɨtta </ts>
               <ts e="T57" id="Seg_1794" n="e" s="T56">iti </ts>
               <ts e="T58" id="Seg_1796" n="e" s="T57">kɨːs </ts>
               <ts e="T59" id="Seg_1798" n="e" s="T58">körsöːččü </ts>
               <ts e="T60" id="Seg_1800" n="e" s="T59">ebitter, </ts>
               <ts e="T61" id="Seg_1802" n="e" s="T60">bagarsaːččɨ </ts>
               <ts e="T62" id="Seg_1804" n="e" s="T61">ebitter </ts>
               <ts e="T63" id="Seg_1806" n="e" s="T62">bejelerin </ts>
               <ts e="T64" id="Seg_1808" n="e" s="T63">töröːbüt </ts>
               <ts e="T65" id="Seg_1810" n="e" s="T64">hirderiger. </ts>
               <ts e="T66" id="Seg_1812" n="e" s="T65">Biːrde </ts>
               <ts e="T67" id="Seg_1814" n="e" s="T66">kɨːha </ts>
               <ts e="T68" id="Seg_1816" n="e" s="T67">Bulčutun, </ts>
               <ts e="T69" id="Seg_1818" n="e" s="T68">u͡olun </ts>
               <ts e="T70" id="Seg_1820" n="e" s="T69">haŋarar: </ts>
               <ts e="T71" id="Seg_1822" n="e" s="T70">"Dokuːl </ts>
               <ts e="T72" id="Seg_1824" n="e" s="T71">bihigi </ts>
               <ts e="T73" id="Seg_1826" n="e" s="T72">kergetterbititten </ts>
               <ts e="T74" id="Seg_1828" n="e" s="T73">kistene </ts>
               <ts e="T75" id="Seg_1830" n="e" s="T74">hɨldʼɨ͡akpɨtɨj? </ts>
               <ts e="T76" id="Seg_1832" n="e" s="T75">Kel </ts>
               <ts e="T77" id="Seg_1834" n="e" s="T76">bihi͡eke – </ts>
               <ts e="T78" id="Seg_1836" n="e" s="T77">ɨ͡allammɨt </ts>
               <ts e="T79" id="Seg_1838" n="e" s="T78">bu͡ol. </ts>
               <ts e="T80" id="Seg_1840" n="e" s="T79">Kergetterbin </ts>
               <ts e="T81" id="Seg_1842" n="e" s="T80">kɨtta </ts>
               <ts e="T82" id="Seg_1844" n="e" s="T81">haŋarɨs, </ts>
               <ts e="T83" id="Seg_1846" n="e" s="T82">agabɨn </ts>
               <ts e="T84" id="Seg_1848" n="e" s="T83">kɨtta </ts>
               <ts e="T85" id="Seg_1850" n="e" s="T84">haŋarɨs, </ts>
               <ts e="T86" id="Seg_1852" n="e" s="T85">agabɨttan </ts>
               <ts e="T87" id="Seg_1854" n="e" s="T86">kördöː </ts>
               <ts e="T88" id="Seg_1856" n="e" s="T87">minigin. </ts>
               <ts e="T89" id="Seg_1858" n="e" s="T88">Agam </ts>
               <ts e="T92" id="Seg_1860" n="e" s="T89">taːk-daː </ts>
               <ts e="T93" id="Seg_1862" n="e" s="T92">üčügej </ts>
               <ts e="T94" id="Seg_1864" n="e" s="T93">bagaj </ts>
               <ts e="T95" id="Seg_1866" n="e" s="T94">kihi, </ts>
               <ts e="T96" id="Seg_1868" n="e" s="T95">daː </ts>
               <ts e="T97" id="Seg_1870" n="e" s="T96">bu͡ollar </ts>
               <ts e="T98" id="Seg_1872" n="e" s="T97">nʼɨmalaːk, </ts>
               <ts e="T99" id="Seg_1874" n="e" s="T98">dʼe </ts>
               <ts e="T100" id="Seg_1876" n="e" s="T99">ol </ts>
               <ts e="T101" id="Seg_1878" n="e" s="T100">nʼɨmatɨttan </ts>
               <ts e="T102" id="Seg_1880" n="e" s="T101">en </ts>
               <ts e="T103" id="Seg_1882" n="e" s="T102">kečehime." </ts>
               <ts e="T104" id="Seg_1884" n="e" s="T103">Dʼe, </ts>
               <ts e="T105" id="Seg_1886" n="e" s="T104">ol </ts>
               <ts e="T106" id="Seg_1888" n="e" s="T105">kördükkaːn </ts>
               <ts e="T107" id="Seg_1890" n="e" s="T106">haŋarda </ts>
               <ts e="T108" id="Seg_1892" n="e" s="T107">daː, </ts>
               <ts e="T109" id="Seg_1894" n="e" s="T108">u͡olbut </ts>
               <ts e="T110" id="Seg_1896" n="e" s="T109">ol </ts>
               <ts e="T111" id="Seg_1898" n="e" s="T110">kördükkeːn </ts>
               <ts e="T112" id="Seg_1900" n="e" s="T111">kas </ts>
               <ts e="T113" id="Seg_1902" n="e" s="T112">daː </ts>
               <ts e="T114" id="Seg_1904" n="e" s="T113">kün </ts>
               <ts e="T115" id="Seg_1906" n="e" s="T114">bu͡olan </ts>
               <ts e="T116" id="Seg_1908" n="e" s="T115">baraːn, </ts>
               <ts e="T117" id="Seg_1910" n="e" s="T116">itiː </ts>
               <ts e="T118" id="Seg_1912" n="e" s="T117">bagajɨ </ts>
               <ts e="T119" id="Seg_1914" n="e" s="T118">küŋŋe, </ts>
               <ts e="T120" id="Seg_1916" n="e" s="T119">haːs, </ts>
               <ts e="T121" id="Seg_1918" n="e" s="T120">kühü͡örü </ts>
               <ts e="T122" id="Seg_1920" n="e" s="T121">künneːk </ts>
               <ts e="T123" id="Seg_1922" n="e" s="T122">bagajɨ </ts>
               <ts e="T124" id="Seg_1924" n="e" s="T123">küŋŋe </ts>
               <ts e="T125" id="Seg_1926" n="e" s="T124">dʼe </ts>
               <ts e="T126" id="Seg_1928" n="e" s="T125">uːčaktanan </ts>
               <ts e="T127" id="Seg_1930" n="e" s="T126">baraːn, </ts>
               <ts e="T128" id="Seg_1932" n="e" s="T127">dʼe </ts>
               <ts e="T129" id="Seg_1934" n="e" s="T128">keler, </ts>
               <ts e="T130" id="Seg_1936" n="e" s="T129">bejetin </ts>
               <ts e="T132" id="Seg_1938" n="e" s="T130">(aːkul-) </ts>
               <ts e="T133" id="Seg_1940" n="e" s="T132">čeːlkeː </ts>
               <ts e="T134" id="Seg_1942" n="e" s="T133">aːkulaːk. </ts>
               <ts e="T135" id="Seg_1944" n="e" s="T134">Ontutunan </ts>
               <ts e="T136" id="Seg_1946" n="e" s="T135">ɨ͡allana </ts>
               <ts e="T137" id="Seg_1948" n="e" s="T136">barda </ts>
               <ts e="T138" id="Seg_1950" n="e" s="T137">kɨːhɨgar. </ts>
               <ts e="T139" id="Seg_1952" n="e" s="T138">Kiːreːtin </ts>
               <ts e="T140" id="Seg_1954" n="e" s="T139">hirge </ts>
               <ts e="T141" id="Seg_1956" n="e" s="T140">di͡eri </ts>
               <ts e="T142" id="Seg_1958" n="e" s="T141">meniːtin </ts>
               <ts e="T143" id="Seg_1960" n="e" s="T142">kergetteriger </ts>
               <ts e="T144" id="Seg_1962" n="e" s="T143">bökčöŋnöːn </ts>
               <ts e="T145" id="Seg_1964" n="e" s="T144">baraːn: </ts>
               <ts e="T146" id="Seg_1966" n="e" s="T145">"Doroːboŋ, </ts>
               <ts e="T147" id="Seg_1968" n="e" s="T146">doroːboŋ, </ts>
               <ts e="T148" id="Seg_1970" n="e" s="T147">kɨrdʼagastar", </ts>
               <ts e="T149" id="Seg_1972" n="e" s="T148">di͡ete </ts>
               <ts e="T150" id="Seg_1974" n="e" s="T149">Bulčut </ts>
               <ts e="T151" id="Seg_1976" n="e" s="T150">u͡olbut. </ts>
               <ts e="T152" id="Seg_1978" n="e" s="T151">"Dʼe </ts>
               <ts e="T153" id="Seg_1980" n="e" s="T152">bu </ts>
               <ts e="T154" id="Seg_1982" n="e" s="T153">kellim </ts>
               <ts e="T155" id="Seg_1984" n="e" s="T154">ehi͡eke, </ts>
               <ts e="T156" id="Seg_1986" n="e" s="T155">(eni͡e) </ts>
               <ts e="T157" id="Seg_1988" n="e" s="T156">edʼi͡e, </ts>
               <ts e="T158" id="Seg_1990" n="e" s="T157">aga, </ts>
               <ts e="T159" id="Seg_1992" n="e" s="T158">ehigitten </ts>
               <ts e="T160" id="Seg_1994" n="e" s="T159">kɨːskɨtɨn </ts>
               <ts e="T161" id="Seg_1996" n="e" s="T160">kördüːbün. </ts>
               <ts e="T162" id="Seg_1998" n="e" s="T161">Tugun, </ts>
               <ts e="T163" id="Seg_2000" n="e" s="T162">dokuːl </ts>
               <ts e="T164" id="Seg_2002" n="e" s="T163">kisti͡ekpitij </ts>
               <ts e="T165" id="Seg_2004" n="e" s="T164">bihigi, </ts>
               <ts e="T166" id="Seg_2006" n="e" s="T165">bihigi </ts>
               <ts e="T167" id="Seg_2008" n="e" s="T166">domnuŋŋuttan </ts>
               <ts e="T168" id="Seg_2010" n="e" s="T167">bagarsabɨt, </ts>
               <ts e="T169" id="Seg_2012" n="e" s="T168">beje-bejebitin </ts>
               <ts e="T170" id="Seg_2014" n="e" s="T169">hürdeːk </ts>
               <ts e="T171" id="Seg_2016" n="e" s="T170">kɨlɨːlɨːbɨt." </ts>
               <ts e="T172" id="Seg_2018" n="e" s="T171">"Dʼe, </ts>
               <ts e="T173" id="Seg_2020" n="e" s="T172">kajtak </ts>
               <ts e="T174" id="Seg_2022" n="e" s="T173">gɨnɨ͡aŋɨj", </ts>
               <ts e="T175" id="Seg_2024" n="e" s="T174">di͡ete </ts>
               <ts e="T176" id="Seg_2026" n="e" s="T175">kɨlaŋnɨː-kɨlaŋnɨː </ts>
               <ts e="T177" id="Seg_2028" n="e" s="T176">kɨːs </ts>
               <ts e="T178" id="Seg_2030" n="e" s="T177">agata. </ts>
               <ts e="T179" id="Seg_2032" n="e" s="T178">Onton, </ts>
               <ts e="T180" id="Seg_2034" n="e" s="T179">"dʼe </ts>
               <ts e="T181" id="Seg_2036" n="e" s="T180">nʼɨmalanaːrɨ </ts>
               <ts e="T182" id="Seg_2038" n="e" s="T181">gɨnna </ts>
               <ts e="T183" id="Seg_2040" n="e" s="T182">bɨhɨlaːk", </ts>
               <ts e="T184" id="Seg_2042" n="e" s="T183">diː </ts>
               <ts e="T185" id="Seg_2044" n="e" s="T184">hanaːta </ts>
               <ts e="T186" id="Seg_2046" n="e" s="T185">u͡olbut. </ts>
               <ts e="T187" id="Seg_2048" n="e" s="T186">Kirdik </ts>
               <ts e="T188" id="Seg_2050" n="e" s="T187">ebit. </ts>
               <ts e="T189" id="Seg_2052" n="e" s="T188">Kɨːspɨt </ts>
               <ts e="T190" id="Seg_2054" n="e" s="T189">agata </ts>
               <ts e="T191" id="Seg_2056" n="e" s="T190">tuːgu </ts>
               <ts e="T192" id="Seg_2058" n="e" s="T191">ere </ts>
               <ts e="T193" id="Seg_2060" n="e" s="T192">bɨrakta </ts>
               <ts e="T194" id="Seg_2062" n="e" s="T193">eːt: </ts>
               <ts e="T195" id="Seg_2064" n="e" s="T194">"Čɨk" </ts>
               <ts e="T196" id="Seg_2066" n="e" s="T195">gɨnan </ts>
               <ts e="T197" id="Seg_2068" n="e" s="T196">keːste </ts>
               <ts e="T198" id="Seg_2070" n="e" s="T197">bu </ts>
               <ts e="T199" id="Seg_2072" n="e" s="T198">ačaːk </ts>
               <ts e="T200" id="Seg_2074" n="e" s="T199">ürdütünen, </ts>
               <ts e="T201" id="Seg_2076" n="e" s="T200">diː </ts>
               <ts e="T202" id="Seg_2078" n="e" s="T201">hanaːta </ts>
               <ts e="T203" id="Seg_2080" n="e" s="T202">onton: </ts>
               <ts e="T204" id="Seg_2082" n="e" s="T203">"Tutu͡o </ts>
               <ts e="T205" id="Seg_2084" n="e" s="T204">duː, </ts>
               <ts e="T206" id="Seg_2086" n="e" s="T205">tutu͡o </ts>
               <ts e="T207" id="Seg_2088" n="e" s="T206">du </ts>
               <ts e="T208" id="Seg_2090" n="e" s="T207">hu͡oga </ts>
               <ts e="T209" id="Seg_2092" n="e" s="T208">duː </ts>
               <ts e="T210" id="Seg_2094" n="e" s="T209">bu </ts>
               <ts e="T211" id="Seg_2096" n="e" s="T210">oŋu͡ogu." </ts>
               <ts e="T212" id="Seg_2098" n="e" s="T211">Ka, </ts>
               <ts e="T213" id="Seg_2100" n="e" s="T212">u͡olbut </ts>
               <ts e="T214" id="Seg_2102" n="e" s="T213">dʼe </ts>
               <ts e="T215" id="Seg_2104" n="e" s="T214">türgen </ts>
               <ts e="T216" id="Seg_2106" n="e" s="T215">bagajdɨk </ts>
               <ts e="T217" id="Seg_2108" n="e" s="T216">tutar </ts>
               <ts e="T218" id="Seg_2110" n="e" s="T217">diːn. </ts>
               <ts e="T219" id="Seg_2112" n="e" s="T218">Onu </ts>
               <ts e="T220" id="Seg_2114" n="e" s="T219">ɨjɨtar </ts>
               <ts e="T221" id="Seg_2116" n="e" s="T220">kɨːspɨt </ts>
               <ts e="T222" id="Seg_2118" n="e" s="T221">agata: </ts>
               <ts e="T223" id="Seg_2120" n="e" s="T222">"Dʼe, </ts>
               <ts e="T224" id="Seg_2122" n="e" s="T223">tugu </ts>
               <ts e="T225" id="Seg_2124" n="e" s="T224">tuttuŋ </ts>
               <ts e="T226" id="Seg_2126" n="e" s="T225">iti?" </ts>
               <ts e="T227" id="Seg_2128" n="e" s="T226">"Kajtak </ts>
               <ts e="T228" id="Seg_2130" n="e" s="T227">tuːgu", </ts>
               <ts e="T229" id="Seg_2132" n="e" s="T228">diː </ts>
               <ts e="T230" id="Seg_2134" n="e" s="T229">ɨjɨtta </ts>
               <ts e="T231" id="Seg_2136" n="e" s="T230">ol </ts>
               <ts e="T232" id="Seg_2138" n="e" s="T231">ke </ts>
               <ts e="T233" id="Seg_2140" n="e" s="T232">kim </ts>
               <ts e="T234" id="Seg_2142" n="e" s="T233">u͡olbut. </ts>
               <ts e="T235" id="Seg_2144" n="e" s="T234">"Min </ts>
               <ts e="T237" id="Seg_2146" n="e" s="T235">(sra-), </ts>
               <ts e="T238" id="Seg_2148" n="e" s="T237">min </ts>
               <ts e="T239" id="Seg_2150" n="e" s="T238">kördüm, </ts>
               <ts e="T240" id="Seg_2152" n="e" s="T239">taːjdɨm – </ts>
               <ts e="T241" id="Seg_2154" n="e" s="T240">tobuk </ts>
               <ts e="T242" id="Seg_2156" n="e" s="T241">oŋu͡oga, </ts>
               <ts e="T243" id="Seg_2158" n="e" s="T242">kim </ts>
               <ts e="T244" id="Seg_2160" n="e" s="T243">taːjbat </ts>
               <ts e="T245" id="Seg_2162" n="e" s="T244">ol </ts>
               <ts e="T246" id="Seg_2164" n="e" s="T245">tobuk </ts>
               <ts e="T247" id="Seg_2166" n="e" s="T246">oŋu͡ogun? </ts>
               <ts e="T248" id="Seg_2168" n="e" s="T247">Kim </ts>
               <ts e="T249" id="Seg_2170" n="e" s="T248">taːjɨmɨ͡aj </ts>
               <ts e="T250" id="Seg_2172" n="e" s="T249">ol </ts>
               <ts e="T251" id="Seg_2174" n="e" s="T250">tobuk </ts>
               <ts e="T252" id="Seg_2176" n="e" s="T251">oŋu͡ogun", </ts>
               <ts e="T253" id="Seg_2178" n="e" s="T252">di͡ete. </ts>
               <ts e="T254" id="Seg_2180" n="e" s="T253">Agabɨt </ts>
               <ts e="T256" id="Seg_2182" n="e" s="T254">(nʼɨmaŋ-) </ts>
               <ts e="T257" id="Seg_2184" n="e" s="T256">nʼɨmaŋnɨː-nʼɨmaŋnɨː, </ts>
               <ts e="T258" id="Seg_2186" n="e" s="T257">ɨmaŋnɨː-ɨmaŋnɨː </ts>
               <ts e="T259" id="Seg_2188" n="e" s="T258">di͡ete: </ts>
               <ts e="T260" id="Seg_2190" n="e" s="T259">"Dʼe, </ts>
               <ts e="T261" id="Seg_2192" n="e" s="T260">taːjdɨŋ </ts>
               <ts e="T262" id="Seg_2194" n="e" s="T261">tobuk </ts>
               <ts e="T263" id="Seg_2196" n="e" s="T262">oŋu͡ogun, </ts>
               <ts e="T264" id="Seg_2198" n="e" s="T263">üčügej. </ts>
               <ts e="T265" id="Seg_2200" n="e" s="T264">Dʼe </ts>
               <ts e="T266" id="Seg_2202" n="e" s="T265">bu </ts>
               <ts e="T268" id="Seg_2204" n="e" s="T266">(tobu-) </ts>
               <ts e="T269" id="Seg_2206" n="e" s="T268">tobuk </ts>
               <ts e="T270" id="Seg_2208" n="e" s="T269">oŋu͡ogugar </ts>
               <ts e="T271" id="Seg_2210" n="e" s="T270">kantan </ts>
               <ts e="T272" id="Seg_2212" n="e" s="T271">emete </ts>
               <ts e="T274" id="Seg_2214" n="e" s="T272">(köröŋŋ-) </ts>
               <ts e="T275" id="Seg_2216" n="e" s="T274">köröŋŋün </ts>
               <ts e="T276" id="Seg_2218" n="e" s="T275">biːr </ts>
               <ts e="T277" id="Seg_2220" n="e" s="T276">emete </ts>
               <ts e="T278" id="Seg_2222" n="e" s="T277">iŋiːri </ts>
               <ts e="T279" id="Seg_2224" n="e" s="T278">bulu͡oŋ </ts>
               <ts e="T280" id="Seg_2226" n="e" s="T279">dʼuru? </ts>
               <ts e="T281" id="Seg_2228" n="e" s="T280">Oččogo </ts>
               <ts e="T282" id="Seg_2230" n="e" s="T281">min </ts>
               <ts e="T283" id="Seg_2232" n="e" s="T282">haŋabɨn </ts>
               <ts e="T284" id="Seg_2234" n="e" s="T283">ɨlɨ͡am </ts>
               <ts e="T285" id="Seg_2236" n="e" s="T284">hu͡oga </ts>
               <ts e="T286" id="Seg_2238" n="e" s="T285">töttörü. </ts>
               <ts e="T287" id="Seg_2240" n="e" s="T286">Bullakkɨna </ts>
               <ts e="T288" id="Seg_2242" n="e" s="T287">biːr </ts>
               <ts e="T289" id="Seg_2244" n="e" s="T288">emete </ts>
               <ts e="T290" id="Seg_2246" n="e" s="T289">iŋiːri, </ts>
               <ts e="T291" id="Seg_2248" n="e" s="T290">oččogo </ts>
               <ts e="T292" id="Seg_2250" n="e" s="T291">min </ts>
               <ts e="T293" id="Seg_2252" n="e" s="T292">kɨːspɨn </ts>
               <ts e="T294" id="Seg_2254" n="e" s="T293">eni͡eke </ts>
               <ts e="T295" id="Seg_2256" n="e" s="T294">bi͡eri͡em." </ts>
               <ts e="T296" id="Seg_2258" n="e" s="T295">Dʼe, </ts>
               <ts e="T297" id="Seg_2260" n="e" s="T296">u͡olbut </ts>
               <ts e="T298" id="Seg_2262" n="e" s="T297">körö </ts>
               <ts e="T299" id="Seg_2264" n="e" s="T298">hataːta, </ts>
               <ts e="T300" id="Seg_2266" n="e" s="T299">körö </ts>
               <ts e="T301" id="Seg_2268" n="e" s="T300">hataːta. </ts>
               <ts e="T302" id="Seg_2270" n="e" s="T301">Onton </ts>
               <ts e="T303" id="Seg_2272" n="e" s="T302">orguːjkaːnnɨk </ts>
               <ts e="T304" id="Seg_2274" n="e" s="T303">bejete </ts>
               <ts e="T305" id="Seg_2276" n="e" s="T304">bejetiger </ts>
               <ts e="T306" id="Seg_2278" n="e" s="T305">haŋarar: </ts>
               <ts e="T307" id="Seg_2280" n="e" s="T306">"O, </ts>
               <ts e="T308" id="Seg_2282" n="e" s="T307">dʼe, </ts>
               <ts e="T309" id="Seg_2284" n="e" s="T308">hürdeːk </ts>
               <ts e="T310" id="Seg_2286" n="e" s="T309">ebit </ts>
               <ts e="T311" id="Seg_2288" n="e" s="T310">dʼe – </ts>
               <ts e="T312" id="Seg_2290" n="e" s="T311">ile </ts>
               <ts e="T313" id="Seg_2292" n="e" s="T312">taːska </ts>
               <ts e="T314" id="Seg_2294" n="e" s="T313">talɨ, </ts>
               <ts e="T315" id="Seg_2296" n="e" s="T314">tu͡ok </ts>
               <ts e="T316" id="Seg_2298" n="e" s="T315">basku͡oj </ts>
               <ts e="T317" id="Seg_2300" n="e" s="T316">oŋu͡ogaj, </ts>
               <ts e="T318" id="Seg_2302" n="e" s="T317">ogolor." </ts>
               <ts e="T319" id="Seg_2304" n="e" s="T318">Ontuŋ, </ts>
               <ts e="T320" id="Seg_2306" n="e" s="T319">kaja, </ts>
               <ts e="T321" id="Seg_2308" n="e" s="T320">bilbet </ts>
               <ts e="T322" id="Seg_2310" n="e" s="T321">ebit. </ts>
               <ts e="T323" id="Seg_2312" n="e" s="T322">Gini </ts>
               <ts e="T324" id="Seg_2314" n="e" s="T323">gini </ts>
               <ts e="T325" id="Seg_2316" n="e" s="T324">iti </ts>
               <ts e="T326" id="Seg_2318" n="e" s="T325">agalara </ts>
               <ts e="T327" id="Seg_2320" n="e" s="T326">ere </ts>
               <ts e="T328" id="Seg_2322" n="e" s="T327">iti </ts>
               <ts e="T329" id="Seg_2324" n="e" s="T328">biler, </ts>
               <ts e="T330" id="Seg_2326" n="e" s="T329">biːr </ts>
               <ts e="T331" id="Seg_2328" n="e" s="T330">hire </ts>
               <ts e="T332" id="Seg_2330" n="e" s="T331">ontuŋ </ts>
               <ts e="T333" id="Seg_2332" n="e" s="T332">amattan </ts>
               <ts e="T334" id="Seg_2334" n="e" s="T333">biːr </ts>
               <ts e="T335" id="Seg_2336" n="e" s="T334">daː </ts>
               <ts e="T336" id="Seg_2338" n="e" s="T335">iŋiːre </ts>
               <ts e="T337" id="Seg_2340" n="e" s="T336">hu͡ok. </ts>
               <ts e="T338" id="Seg_2342" n="e" s="T337">Biːrger </ts>
               <ts e="T339" id="Seg_2344" n="e" s="T338">sirin </ts>
               <ts e="T340" id="Seg_2346" n="e" s="T339">körü͡öŋ, </ts>
               <ts e="T341" id="Seg_2348" n="e" s="T340">üčügejkeːnnik </ts>
               <ts e="T342" id="Seg_2350" n="e" s="T341">kördökkö, </ts>
               <ts e="T343" id="Seg_2352" n="e" s="T342">ontuŋ </ts>
               <ts e="T344" id="Seg_2354" n="e" s="T343">kɨːs </ts>
               <ts e="T346" id="Seg_2356" n="e" s="T344">(hɨr-) </ts>
               <ts e="T347" id="Seg_2358" n="e" s="T346">hɨrajɨgar </ts>
               <ts e="T348" id="Seg_2360" n="e" s="T347">talɨ, </ts>
               <ts e="T349" id="Seg_2362" n="e" s="T348">dʼaktar </ts>
               <ts e="T350" id="Seg_2364" n="e" s="T349">hɨrajɨgar </ts>
               <ts e="T351" id="Seg_2366" n="e" s="T350">talɨ – </ts>
               <ts e="T352" id="Seg_2368" n="e" s="T351">biːrges </ts>
               <ts e="T353" id="Seg_2370" n="e" s="T352">öttüge. </ts>
               <ts e="T354" id="Seg_2372" n="e" s="T353">Ontugun </ts>
               <ts e="T355" id="Seg_2374" n="e" s="T354">agabɨt </ts>
               <ts e="T356" id="Seg_2376" n="e" s="T355">ire </ts>
               <ts e="T357" id="Seg_2378" n="e" s="T356">biler </ts>
               <ts e="T358" id="Seg_2380" n="e" s="T357">bu͡o. </ts>
               <ts e="T359" id="Seg_2382" n="e" s="T358">Dʼe, </ts>
               <ts e="T360" id="Seg_2384" n="e" s="T359">kajdi͡ek </ts>
               <ts e="T361" id="Seg_2386" n="e" s="T360">de </ts>
               <ts e="T362" id="Seg_2388" n="e" s="T361">bu͡olu͡ogun </ts>
               <ts e="T363" id="Seg_2390" n="e" s="T362">bert </ts>
               <ts e="T364" id="Seg_2392" n="e" s="T363">Bulčut </ts>
               <ts e="T365" id="Seg_2394" n="e" s="T364">u͡olbut. </ts>
               <ts e="T366" id="Seg_2396" n="e" s="T365">Kɨjŋanaːrɨ </ts>
               <ts e="T367" id="Seg_2398" n="e" s="T366">da </ts>
               <ts e="T368" id="Seg_2400" n="e" s="T367">gɨnar, </ts>
               <ts e="T369" id="Seg_2402" n="e" s="T368">kečeher. </ts>
               <ts e="T370" id="Seg_2404" n="e" s="T369">Onton </ts>
               <ts e="T371" id="Seg_2406" n="e" s="T370">dʼe </ts>
               <ts e="T372" id="Seg_2408" n="e" s="T371">kɨlas </ts>
               <ts e="T373" id="Seg_2410" n="e" s="T372">gɨnar </ts>
               <ts e="T374" id="Seg_2412" n="e" s="T373">diːn </ts>
               <ts e="T375" id="Seg_2414" n="e" s="T374">kɨːhɨn </ts>
               <ts e="T376" id="Seg_2416" n="e" s="T375">di͡et. </ts>
               <ts e="T377" id="Seg_2418" n="e" s="T376">Kɨːha </ts>
               <ts e="T378" id="Seg_2420" n="e" s="T377">nʼɨmalanna </ts>
               <ts e="T379" id="Seg_2422" n="e" s="T378">emi͡e. </ts>
               <ts e="T380" id="Seg_2424" n="e" s="T379">"Kajtak </ts>
               <ts e="T381" id="Seg_2426" n="e" s="T380">gɨnɨ͡amɨj", </ts>
               <ts e="T382" id="Seg_2428" n="e" s="T381">di͡en </ts>
               <ts e="T383" id="Seg_2430" n="e" s="T382">baran, </ts>
               <ts e="T384" id="Seg_2432" n="e" s="T383">kumaːrdartan </ts>
               <ts e="T385" id="Seg_2434" n="e" s="T384">ilbiner </ts>
               <ts e="T386" id="Seg_2436" n="e" s="T385">kördük, </ts>
               <ts e="T387" id="Seg_2438" n="e" s="T386">iliːtin </ts>
               <ts e="T388" id="Seg_2440" n="e" s="T387">orguːjkaːnnɨk </ts>
               <ts e="T389" id="Seg_2442" n="e" s="T388">bugurdukkaːn </ts>
               <ts e="T390" id="Seg_2444" n="e" s="T389">ɨlla. </ts>
               <ts e="T391" id="Seg_2446" n="e" s="T390">Onton </ts>
               <ts e="T392" id="Seg_2448" n="e" s="T391">bu </ts>
               <ts e="T393" id="Seg_2450" n="e" s="T392">orguːjkaːnnɨk </ts>
               <ts e="T394" id="Seg_2452" n="e" s="T393">bu </ts>
               <ts e="T395" id="Seg_2454" n="e" s="T394">bugurdukkaːn </ts>
               <ts e="T396" id="Seg_2456" n="e" s="T395">(hɨrajɨnan) </ts>
               <ts e="T397" id="Seg_2458" n="e" s="T396">hɨrajɨn </ts>
               <ts e="T398" id="Seg_2460" n="e" s="T397">dʼe </ts>
               <ts e="T399" id="Seg_2462" n="e" s="T398">kimniːr </ts>
               <ts e="T400" id="Seg_2464" n="e" s="T399">köllörör – </ts>
               <ts e="T401" id="Seg_2466" n="e" s="T400">kumaːrdartan </ts>
               <ts e="T402" id="Seg_2468" n="e" s="T401">tebenerge </ts>
               <ts e="T403" id="Seg_2470" n="e" s="T402">talɨ. </ts>
               <ts e="T404" id="Seg_2472" n="e" s="T403">Oː, </ts>
               <ts e="T405" id="Seg_2474" n="e" s="T404">dʼe, </ts>
               <ts e="T406" id="Seg_2476" n="e" s="T405">u͡olbut </ts>
               <ts e="T407" id="Seg_2478" n="e" s="T406">dʼe </ts>
               <ts e="T408" id="Seg_2480" n="e" s="T407">ü͡örer </ts>
               <ts e="T409" id="Seg_2482" n="e" s="T408">diːn. </ts>
               <ts e="T410" id="Seg_2484" n="e" s="T409">Türgen </ts>
               <ts e="T411" id="Seg_2486" n="e" s="T410">bagajɨtɨk </ts>
               <ts e="T412" id="Seg_2488" n="e" s="T411">bahagɨn </ts>
               <ts e="T413" id="Seg_2490" n="e" s="T412">ɨlla, </ts>
               <ts e="T414" id="Seg_2492" n="e" s="T413">onton </ts>
               <ts e="T415" id="Seg_2494" n="e" s="T414">orguːjkaːnnɨk </ts>
               <ts e="T416" id="Seg_2496" n="e" s="T415">kötöktö </ts>
               <ts e="T417" id="Seg_2498" n="e" s="T416">tobuk </ts>
               <ts e="T418" id="Seg_2500" n="e" s="T417">oŋu͡ogun. </ts>
               <ts e="T419" id="Seg_2502" n="e" s="T418">Bɨ͡aj, </ts>
               <ts e="T420" id="Seg_2504" n="e" s="T419">türgen </ts>
               <ts e="T421" id="Seg_2506" n="e" s="T420">türgen </ts>
               <ts e="T422" id="Seg_2508" n="e" s="T421">bagajtɨk </ts>
               <ts e="T423" id="Seg_2510" n="e" s="T422">bahagɨnan </ts>
               <ts e="T424" id="Seg_2512" n="e" s="T423">iŋiːr </ts>
               <ts e="T425" id="Seg_2514" n="e" s="T424">munʼaːnɨ </ts>
               <ts e="T426" id="Seg_2516" n="e" s="T425">tardan </ts>
               <ts e="T427" id="Seg_2518" n="e" s="T426">ɨlbat </ts>
               <ts e="T428" id="Seg_2520" n="e" s="T427">du͡o. </ts>
               <ts e="T429" id="Seg_2522" n="e" s="T428">Onno </ts>
               <ts e="T430" id="Seg_2524" n="e" s="T429">agata </ts>
               <ts e="T431" id="Seg_2526" n="e" s="T430">di͡ete: </ts>
               <ts e="T432" id="Seg_2528" n="e" s="T431">"Dʼe, </ts>
               <ts e="T433" id="Seg_2530" n="e" s="T432">kajtak </ts>
               <ts e="T434" id="Seg_2532" n="e" s="T433">gɨnɨ͡aŋɨj, </ts>
               <ts e="T435" id="Seg_2534" n="e" s="T434">min </ts>
               <ts e="T436" id="Seg_2536" n="e" s="T435">di͡ebitim, </ts>
               <ts e="T437" id="Seg_2538" n="e" s="T436">tɨlbɨn </ts>
               <ts e="T438" id="Seg_2540" n="e" s="T437">töttörü </ts>
               <ts e="T439" id="Seg_2542" n="e" s="T438">ɨlɨ͡am </ts>
               <ts e="T440" id="Seg_2544" n="e" s="T439">hu͡oga, </ts>
               <ts e="T441" id="Seg_2546" n="e" s="T440">itinnik </ts>
               <ts e="T442" id="Seg_2548" n="e" s="T441">uːs </ts>
               <ts e="T443" id="Seg_2550" n="e" s="T442">kihi, </ts>
               <ts e="T444" id="Seg_2552" n="e" s="T443">itinnik </ts>
               <ts e="T445" id="Seg_2554" n="e" s="T444">bulčut </ts>
               <ts e="T446" id="Seg_2556" n="e" s="T445">kihi </ts>
               <ts e="T447" id="Seg_2558" n="e" s="T446">barɨtɨn </ts>
               <ts e="T448" id="Seg_2560" n="e" s="T447">biler. </ts>
               <ts e="T449" id="Seg_2562" n="e" s="T448">Dʼe </ts>
               <ts e="T450" id="Seg_2564" n="e" s="T449">bi͡erebin </ts>
               <ts e="T451" id="Seg_2566" n="e" s="T450">eni͡eke, </ts>
               <ts e="T452" id="Seg_2568" n="e" s="T451">Bulčut, </ts>
               <ts e="T453" id="Seg_2570" n="e" s="T452">kɨːspɨn. </ts>
               <ts e="T454" id="Seg_2572" n="e" s="T453">Eteŋŋetik </ts>
               <ts e="T455" id="Seg_2574" n="e" s="T454">oloruŋ, </ts>
               <ts e="T456" id="Seg_2576" n="e" s="T455">üčügejdik </ts>
               <ts e="T457" id="Seg_2578" n="e" s="T456">oloruŋ, </ts>
               <ts e="T458" id="Seg_2580" n="e" s="T457">ügüs </ts>
               <ts e="T459" id="Seg_2582" n="e" s="T458">ogonu </ts>
               <ts e="T460" id="Seg_2584" n="e" s="T459">ogolonuŋ", </ts>
               <ts e="T461" id="Seg_2586" n="e" s="T460">di͡ete. </ts>
               <ts e="T462" id="Seg_2588" n="e" s="T461">Dʼe, </ts>
               <ts e="T463" id="Seg_2590" n="e" s="T462">ol </ts>
               <ts e="T464" id="Seg_2592" n="e" s="T463">kördükkeːn </ts>
               <ts e="T465" id="Seg_2594" n="e" s="T464">Bulčut </ts>
               <ts e="T466" id="Seg_2596" n="e" s="T465">kɨːhɨn </ts>
               <ts e="T467" id="Seg_2598" n="e" s="T466">kɨtta </ts>
               <ts e="T468" id="Seg_2600" n="e" s="T467">üčügej </ts>
               <ts e="T469" id="Seg_2602" n="e" s="T468">bagajtɨk </ts>
               <ts e="T470" id="Seg_2604" n="e" s="T469">olorbuttara. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T7" id="Seg_2605" s="T0">NaLE_2002_StonyBone_flk.001 (001.001)</ta>
            <ta e="T10" id="Seg_2606" s="T7">NaLE_2002_StonyBone_flk.002 (001.002)</ta>
            <ta e="T15" id="Seg_2607" s="T10">NaLE_2002_StonyBone_flk.003 (001.003)</ta>
            <ta e="T21" id="Seg_2608" s="T15">NaLE_2002_StonyBone_flk.004 (001.004)</ta>
            <ta e="T35" id="Seg_2609" s="T21">NaLE_2002_StonyBone_flk.005 (001.005)</ta>
            <ta e="T39" id="Seg_2610" s="T35">NaLE_2002_StonyBone_flk.006 (001.006)</ta>
            <ta e="T51" id="Seg_2611" s="T39">NaLE_2002_StonyBone_flk.007 (001.007)</ta>
            <ta e="T65" id="Seg_2612" s="T51">NaLE_2002_StonyBone_flk.008 (001.008)</ta>
            <ta e="T70" id="Seg_2613" s="T65">NaLE_2002_StonyBone_flk.009 (001.009)</ta>
            <ta e="T75" id="Seg_2614" s="T70">NaLE_2002_StonyBone_flk.010 (001.009)</ta>
            <ta e="T79" id="Seg_2615" s="T75">NaLE_2002_StonyBone_flk.011 (001.010)</ta>
            <ta e="T88" id="Seg_2616" s="T79">NaLE_2002_StonyBone_flk.012 (001.011)</ta>
            <ta e="T103" id="Seg_2617" s="T88">NaLE_2002_StonyBone_flk.013 (001.012)</ta>
            <ta e="T134" id="Seg_2618" s="T103">NaLE_2002_StonyBone_flk.014 (001.013)</ta>
            <ta e="T138" id="Seg_2619" s="T134">NaLE_2002_StonyBone_flk.015 (001.014)</ta>
            <ta e="T145" id="Seg_2620" s="T138">NaLE_2002_StonyBone_flk.016 (001.015)</ta>
            <ta e="T151" id="Seg_2621" s="T145">NaLE_2002_StonyBone_flk.017 (001.015)</ta>
            <ta e="T161" id="Seg_2622" s="T151">NaLE_2002_StonyBone_flk.018 (001.017)</ta>
            <ta e="T171" id="Seg_2623" s="T161">NaLE_2002_StonyBone_flk.019 (001.018)</ta>
            <ta e="T178" id="Seg_2624" s="T171">NaLE_2002_StonyBone_flk.020 (001.019)</ta>
            <ta e="T186" id="Seg_2625" s="T178">NaLE_2002_StonyBone_flk.021 (001.021)</ta>
            <ta e="T188" id="Seg_2626" s="T186">NaLE_2002_StonyBone_flk.022 (001.022)</ta>
            <ta e="T194" id="Seg_2627" s="T188">NaLE_2002_StonyBone_flk.023 (001.023)</ta>
            <ta e="T203" id="Seg_2628" s="T194">NaLE_2002_StonyBone_flk.024 (001.024)</ta>
            <ta e="T211" id="Seg_2629" s="T203">NaLE_2002_StonyBone_flk.025 (001.025)</ta>
            <ta e="T218" id="Seg_2630" s="T211">NaLE_2002_StonyBone_flk.026 (001.026)</ta>
            <ta e="T222" id="Seg_2631" s="T218">NaLE_2002_StonyBone_flk.027 (001.027)</ta>
            <ta e="T226" id="Seg_2632" s="T222">NaLE_2002_StonyBone_flk.028 (001.027)</ta>
            <ta e="T234" id="Seg_2633" s="T226">NaLE_2002_StonyBone_flk.029 (001.028)</ta>
            <ta e="T247" id="Seg_2634" s="T234">NaLE_2002_StonyBone_flk.030 (001.030)</ta>
            <ta e="T253" id="Seg_2635" s="T247">NaLE_2002_StonyBone_flk.031 (001.031)</ta>
            <ta e="T259" id="Seg_2636" s="T253">NaLE_2002_StonyBone_flk.032 (001.033)</ta>
            <ta e="T264" id="Seg_2637" s="T259">NaLE_2002_StonyBone_flk.033 (001.034)</ta>
            <ta e="T280" id="Seg_2638" s="T264">NaLE_2002_StonyBone_flk.034 (001.035)</ta>
            <ta e="T286" id="Seg_2639" s="T280">NaLE_2002_StonyBone_flk.035 (001.036)</ta>
            <ta e="T295" id="Seg_2640" s="T286">NaLE_2002_StonyBone_flk.036 (001.037)</ta>
            <ta e="T301" id="Seg_2641" s="T295">NaLE_2002_StonyBone_flk.037 (001.038)</ta>
            <ta e="T306" id="Seg_2642" s="T301">NaLE_2002_StonyBone_flk.038 (001.039)</ta>
            <ta e="T318" id="Seg_2643" s="T306">NaLE_2002_StonyBone_flk.039 (001.040)</ta>
            <ta e="T322" id="Seg_2644" s="T318">NaLE_2002_StonyBone_flk.040 (001.041)</ta>
            <ta e="T337" id="Seg_2645" s="T322">NaLE_2002_StonyBone_flk.041 (001.042)</ta>
            <ta e="T353" id="Seg_2646" s="T337">NaLE_2002_StonyBone_flk.042 (001.043)</ta>
            <ta e="T358" id="Seg_2647" s="T353">NaLE_2002_StonyBone_flk.043 (001.044)</ta>
            <ta e="T365" id="Seg_2648" s="T358">NaLE_2002_StonyBone_flk.044 (001.045)</ta>
            <ta e="T369" id="Seg_2649" s="T365">NaLE_2002_StonyBone_flk.045 (001.046)</ta>
            <ta e="T376" id="Seg_2650" s="T369">NaLE_2002_StonyBone_flk.046 (001.047)</ta>
            <ta e="T379" id="Seg_2651" s="T376">NaLE_2002_StonyBone_flk.047 (001.048)</ta>
            <ta e="T390" id="Seg_2652" s="T379">NaLE_2002_StonyBone_flk.048 (001.049)</ta>
            <ta e="T403" id="Seg_2653" s="T390">NaLE_2002_StonyBone_flk.049 (001.051)</ta>
            <ta e="T409" id="Seg_2654" s="T403">NaLE_2002_StonyBone_flk.050 (001.052)</ta>
            <ta e="T418" id="Seg_2655" s="T409">NaLE_2002_StonyBone_flk.051 (001.053)</ta>
            <ta e="T428" id="Seg_2656" s="T418">NaLE_2002_StonyBone_flk.052 (001.054)</ta>
            <ta e="T431" id="Seg_2657" s="T428">NaLE_2002_StonyBone_flk.053 (001.055)</ta>
            <ta e="T448" id="Seg_2658" s="T431">NaLE_2002_StonyBone_flk.054 (001.055)</ta>
            <ta e="T453" id="Seg_2659" s="T448">NaLE_2002_StonyBone_flk.055 (001.056)</ta>
            <ta e="T461" id="Seg_2660" s="T453">NaLE_2002_StonyBone_flk.056 (001.057)</ta>
            <ta e="T470" id="Seg_2661" s="T461">NaLE_2002_StonyBone_flk.057 (001.058)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T7" id="Seg_2662" s="T0">Мин бу өһү истибитим кэргэттэрбит гытта… </ta>
            <ta e="T10" id="Seg_2663" s="T7">Школага киирэр дьылбар.</ta>
            <ta e="T15" id="Seg_2664" s="T10">Онтуӈ аныга диэри өйбөр баар.</ta>
            <ta e="T21" id="Seg_2665" s="T15">Буурдуккаан, маамам эдьиийэ, Кылабыйа кэпсээччи этэ.</ta>
            <ta e="T35" id="Seg_2666" s="T21">Ыраак-ыраак муорага, төрөөбүт һиригэр, бэйэтин төрөөбүт һиригэр кэргэттэрин гытта айанныы һылдьыбыта боскуой багайы уол.</ta>
            <ta e="T39" id="Seg_2667" s="T35">Oнтуӈ аата Булчут этэ.</ta>
            <ta e="T51" id="Seg_2668" s="T39">Атын һиргэ, гин… гинилэртэн ырааккаан һаһыы һылдьыбыта боскуой кыыс кэргэттэрин кытта.</ta>
            <ta e="T65" id="Seg_2669" s="T51">Ити һиӈил э-э уону кытта ити кыыс көрсөөччү эбиттэр, багарсааччы эбиттэр бэйэлэрин төрөөбүт һирдэригэр.</ta>
            <ta e="T70" id="Seg_2670" s="T65">Биирдэ кыыһа Булчутун, уолун һаӈарар: </ta>
            <ta e="T75" id="Seg_2671" s="T70">"Докуул биһиги кэргэттэрбититтэн кистэнэ һылдьыакпытый?</ta>
            <ta e="T79" id="Seg_2672" s="T75">Кэл биһиэкэ – ыалламмыт буол.</ta>
            <ta e="T88" id="Seg_2673" s="T79">Кэргэттэрбин кытта һаӈарыс, агабын кытта һаӈарыс, агабыттан көрдөө минигин.</ta>
            <ta e="T103" id="Seg_2674" s="T88">Агам таак-даа үчүгэй багай киһи, даа буоллар ньымалаак, дьэ ол ньыматыттан эн кэчэһимэ". </ta>
            <ta e="T134" id="Seg_2675" s="T103">Дьэ, ол көрдүккээн һаӈарда даа, уолбут ол көрдүккээн кас даа күн буолан бараан, итии багайы күӈӈэ, һаас, күһүөрү күннээк багайы күӈӈэ дьэ уучактанан бараан, дьэ кэлэр, бэйэтин (ааку..) чээлкээ аакулаак.</ta>
            <ta e="T138" id="Seg_2676" s="T134">Oнтутунан ыаллана барда кыыһыгар.</ta>
            <ta e="T145" id="Seg_2677" s="T138">Киирээтин һиргэ диэри мэниитин кэргэттэригэр бөкчөӈнөөн бараан: ‎‎</ta>
            <ta e="T151" id="Seg_2678" s="T145">"Дорообоӈ, дорообоӈ, кырдьагастар!" – диэтэ Булчут уолбут.</ta>
            <ta e="T161" id="Seg_2679" s="T151">"Дьэ бу кэллим эһиэкэ, (эниэ) эдьиэ, ага, эһигиттэн кыыскытын көрдүүбүн.</ta>
            <ta e="T171" id="Seg_2680" s="T161">Тугун, докуул кистиэкпитий биһиги, биһиги домнуӈӈуттан багарсабыт, бэйэ-бэйэбитин һүрдээк кылыылыыбыт".</ta>
            <ta e="T178" id="Seg_2681" s="T171">"Дьэ, кайтак гыныаӈый!?"– диэтэ кылаӈныы-кылаӈныы кыыс агата.</ta>
            <ta e="T186" id="Seg_2682" s="T178">Онтон, дьэ ньымаланаары гынна быһылаак, дии һанаата уолбут.</ta>
            <ta e="T188" id="Seg_2683" s="T186">Кирдик эбит.</ta>
            <ta e="T194" id="Seg_2684" s="T188">Кыыспыт агата туугу эрэ быракта ээт: </ta>
            <ta e="T203" id="Seg_2685" s="T194">"Чык" гынан кээстэ бу ачаак үрдүтүнэн, дии һанаата онтон: </ta>
            <ta e="T211" id="Seg_2686" s="T203">"Тутуо дуу, тутуо(ду) һуога дуу бу оӈуогу."</ta>
            <ta e="T218" id="Seg_2687" s="T211">Ка, дьэ уолбу дьэ түргэн багайдык тутар диин.</ta>
            <ta e="T222" id="Seg_2688" s="T218">Ону ыйытар кыыспыт агата: </ta>
            <ta e="T226" id="Seg_2689" s="T222">"Дьэ, тугу туттуӈ ити?"</ta>
            <ta e="T234" id="Seg_2690" s="T226">"Кайтак туугу? – дии ыйытта ол кэ (ким) уолбут. </ta>
            <ta e="T247" id="Seg_2691" s="T234">"Мин (сра.. о..), мин көрдүм, таайдым – тобук оӈуога, ким таайбат ол тобук оӈуогун?</ta>
            <ta e="T253" id="Seg_2692" s="T247">Ким тайымыай ол тобук оӈуогун?" – диэтэ.</ta>
            <ta e="T259" id="Seg_2693" s="T253">Агабыт ньымал… ньымаӈныы-ньымаӈныы, ымаӈныы-ымаӈныы диэтэ: </ta>
            <ta e="T264" id="Seg_2694" s="T259">"Дьэ, таайдыӈ тобук оӈуогун, үчүгэй.</ta>
            <ta e="T280" id="Seg_2695" s="T264">Дьэ бу (тобу) тобук оӈуогугар кантан эмэтэ (көрөӈӈ) көрөӈӈүн биир эмэтэ иӈиири булуоӈ дьуру? </ta>
            <ta e="T286" id="Seg_2696" s="T280">Оччого мин һаӈабын ылыам һуога төттөрү.</ta>
            <ta e="T295" id="Seg_2697" s="T286">Буллаккына биир эмэтэ иӈиири, оччого мин кыыспын эниэкэ биэриэм". </ta>
            <ta e="T301" id="Seg_2698" s="T295">Дьэ, уолбут көрө һатаата, көрө һатаата.</ta>
            <ta e="T306" id="Seg_2699" s="T301">Онтон оргууйкааннык бэйэтэ бэйэтигэр һаӈарар: </ta>
            <ta e="T318" id="Seg_2700" s="T306">"О, дьэ, һүрдээк эбит дьэ – илэ тааска талы, туок боскуой оӈуогай, оголор". </ta>
            <ta e="T322" id="Seg_2701" s="T318">Онтуӈ, кайа, билбэт эбит.</ta>
            <ta e="T337" id="Seg_2702" s="T322">Гини (гини) ити агалара эрэ ити билэр: Биир һирэ онтуӈ аматтан биир даа иӈиирэ һуок.</ta>
            <ta e="T353" id="Seg_2703" s="T337">Бииргэр һирин көрүөӈ, үчүгэйкээнник көрдөккө, онтуӈ кыыс (һыр) һырайыгар талы, дьактар һырайыгар талы – бииргэс өттүгэ.</ta>
            <ta e="T358" id="Seg_2704" s="T353">Онтугун агабыт эрэ билэр буо.</ta>
            <ta e="T365" id="Seg_2705" s="T358">Дьэ, кайдиэк дэ буолуогун бэрт Булчут уолбут.</ta>
            <ta e="T369" id="Seg_2706" s="T365">Кыйӈанаары да гынар, кэчэһэр.</ta>
            <ta e="T376" id="Seg_2707" s="T369">Онтон дьэ кылас гынар диин кыыһын диэт. </ta>
            <ta e="T379" id="Seg_2708" s="T376">Кыыһа ньымаланна эмиэ.</ta>
            <ta e="T390" id="Seg_2709" s="T379">Кайтак гыныамый? – диэн баран, кумаардартан илбинэр көрдүк, илиитин оргууйкааннык бугурдуккаан ылла.</ta>
            <ta e="T403" id="Seg_2710" s="T390">Oнтон бу оргууйкааннык (бу) бугурдуккаан (һырайынан) һырайын (ти кимниир) көллөрөр – кумаардартан тэбэнэргэ талы.</ta>
            <ta e="T409" id="Seg_2711" s="T403">О-о, дьэ, уолбут дьэ үөрэр диин.</ta>
            <ta e="T418" id="Seg_2712" s="T409">Түргэн багайдык баһагын ылла, онтон оргууйкааннык көтөктө тобук оӈуогун.</ta>
            <ta e="T428" id="Seg_2713" s="T418">Быай, (түргэн) түргэн багайдык баһагынан иӈиир муньааны тардан ылбат дуо.</ta>
            <ta e="T431" id="Seg_2714" s="T428">Онно агата диэтэ: </ta>
            <ta e="T448" id="Seg_2715" s="T431">"Дьэ, кайтак гыныаӈый, мин диэбитим, тылбын төттөрү ылыам һуога, итинник уус киһи, итинник булчут киһи барытын билэр.</ta>
            <ta e="T453" id="Seg_2716" s="T448">Дьэ биэрэбин эниэкэ, Булчут, кыыспын.</ta>
            <ta e="T461" id="Seg_2717" s="T453">Этэӈӈэтик олоруӈ, үчүгэйдик олоруӈ, үгүс огону оголонуӈ", – диэтэ.</ta>
            <ta e="T470" id="Seg_2718" s="T461">Дьэ, ол көрдүккээн Булчут кыыһын кытта үчүгэй багайдык олорбуттара.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T7" id="Seg_2719" s="T0">Min bu öhü istibitim kergetterbin gɨtta…</ta>
            <ta e="T10" id="Seg_2720" s="T7">Školaga kiːrer dʼɨlbar. </ta>
            <ta e="T15" id="Seg_2721" s="T10">Ontuŋ anɨga di͡eri öjbör baːr. </ta>
            <ta e="T21" id="Seg_2722" s="T15">Buːrdukkaːn, maːmam edʼiːje, Kɨlabɨja kepseːčči ete. </ta>
            <ta e="T35" id="Seg_2723" s="T21">ɨraːk-ɨraːk mu͡oraga, töröːbüt hiriger, bejetin töröːbüt hiriger kergetterin kɨtta ajannɨː hɨldʼɨbɨta basku͡oj bagajɨ u͡ol. </ta>
            <ta e="T39" id="Seg_2724" s="T35">Ontuŋ aːta Bulčut ete. </ta>
            <ta e="T51" id="Seg_2725" s="T39">Atɨn hirge, (gin-) ginilerten ɨraːkkaːn hahɨː hɨldʼɨbɨta basku͡oj kɨːs kergetterin kɨtta. </ta>
            <ta e="T65" id="Seg_2726" s="T51">Iti hiŋil eː u͡olu kɨtta iti kɨːs körsöːččü ebitter, bagarsaːččɨ ebitter bejelerin töröːbüt hirderiger. </ta>
            <ta e="T70" id="Seg_2727" s="T65">Biːrde kɨːha Bulčutun, u͡olun haŋarar: </ta>
            <ta e="T75" id="Seg_2728" s="T70">"Dokuːl bihigi kergetterbititten kistene hɨldʼɨ͡akpɨtɨj? </ta>
            <ta e="T79" id="Seg_2729" s="T75">Kel bihi͡eke – ɨ͡allammɨt bu͡ol. </ta>
            <ta e="T88" id="Seg_2730" s="T79">Kergetterbin kɨtta haŋarɨs, agabɨn kɨtta haŋarɨs, agabɨttan kördöː minigin. </ta>
            <ta e="T103" id="Seg_2731" s="T88">Agam taːk-daː üčügej bagaj kihi, daː bu͡ollar nʼɨmalaːk, dʼe ol nʼɨmatɨttan en kečehime." </ta>
            <ta e="T134" id="Seg_2732" s="T103">Dʼe, ol kördükkaːn haŋarda daː, u͡olbut ol kördükkeːn kas daː kün bu͡olan baraːn, itiː bagajɨ küŋŋe, haːs, kühü͡örü künneːk bagajɨ küŋŋe dʼe uːčaktanan baraːn, dʼe keler, bejetin (aːkul-) čeːlkeː aːkulaːk. </ta>
            <ta e="T138" id="Seg_2733" s="T134">Ontutunan ɨ͡allana barda kɨːhɨgar. </ta>
            <ta e="T145" id="Seg_2734" s="T138">Kiːreːtin hirge di͡eri meniːtin kergetteriger bökčöŋnöːn baraːn: </ta>
            <ta e="T151" id="Seg_2735" s="T145">"Doroːboŋ, doroːboŋ, kɨrdʼagastar", di͡ete Bulčut u͡olbut. </ta>
            <ta e="T161" id="Seg_2736" s="T151">"Dʼe bu kellim ehi͡eke, (eni͡e) edʼi͡e, aga, ehigitten kɨːskɨtɨn kördüːbün. </ta>
            <ta e="T171" id="Seg_2737" s="T161">Tugun, dokuːl kisti͡ekpitij bihigi, bihigi domnuŋŋuttan bagarsabɨt, beje-bejebitin hürdeːk kɨlɨːlɨːbɨt." </ta>
            <ta e="T178" id="Seg_2738" s="T171">"Dʼe, kajtak gɨnɨ͡aŋɨj", di͡ete kɨlaŋnɨː-kɨlaŋnɨː kɨːs agata. </ta>
            <ta e="T186" id="Seg_2739" s="T178">Onton, "dʼe nʼɨmalanaːrɨ gɨnna bɨhɨlaːk", diː hanaːta u͡olbut. </ta>
            <ta e="T188" id="Seg_2740" s="T186">Kirdik ebit. </ta>
            <ta e="T194" id="Seg_2741" s="T188">Kɨːspɨt agata tuːgu ere bɨrakta eːt: </ta>
            <ta e="T203" id="Seg_2742" s="T194">"Čɨk" gɨnan keːste bu ačaːk ürdütünen, diː hanaːta onton:</ta>
            <ta e="T211" id="Seg_2743" s="T203">"Tutu͡o duː, tutu͡o du hu͡oga duː bu oŋu͡ogu." </ta>
            <ta e="T218" id="Seg_2744" s="T211">Ka, u͡olbut dʼe türgen bagajdɨk tutar diːn. </ta>
            <ta e="T222" id="Seg_2745" s="T218">Onu ɨjɨtar kɨːspɨt agata: </ta>
            <ta e="T226" id="Seg_2746" s="T222">"Dʼe, tugu tuttuŋ iti?" </ta>
            <ta e="T234" id="Seg_2747" s="T226">"Kajtak tuːgu", diː ɨjɨtta ol ke kim u͡olbut.</ta>
            <ta e="T247" id="Seg_2748" s="T234">"Min (sra-), min kördüm, taːjdɨm – tobuk oŋu͡oga, kim taːjbat ol tobuk oŋu͡ogun? </ta>
            <ta e="T253" id="Seg_2749" s="T247">Kim taːjɨmɨ͡aj ol tobuk oŋu͡ogun", di͡ete. </ta>
            <ta e="T259" id="Seg_2750" s="T253">Agabɨt (nʼɨmaŋ-) nʼɨmaŋnɨː-nʼɨmaŋnɨː, ɨmaŋnɨː-ɨmaŋnɨː di͡ete:</ta>
            <ta e="T264" id="Seg_2751" s="T259">"Dʼe, taːjdɨŋ tobuk oŋu͡ogun, üčügej. </ta>
            <ta e="T280" id="Seg_2752" s="T264">Dʼe bu (tobu-) tobuk oŋu͡ogugar kantan emete (köröŋŋ-) köröŋŋün biːr emete iŋiːri bulu͡oŋ dʼuru? </ta>
            <ta e="T286" id="Seg_2753" s="T280">Oččogo min haŋabɨn ɨlɨ͡am hu͡oga töttörü. </ta>
            <ta e="T295" id="Seg_2754" s="T286">Bullakkɨna biːr emete iŋiːri, oččogo min kɨːspɨn eni͡eke bi͡eri͡em." </ta>
            <ta e="T301" id="Seg_2755" s="T295">Dʼe, u͡olbut körö hataːta, körö hataːta. </ta>
            <ta e="T306" id="Seg_2756" s="T301">Onton orguːjkaːnnɨk bejete bejetiger haŋarar: </ta>
            <ta e="T318" id="Seg_2757" s="T306">"O, dʼe, hürdeːk ebit dʼe – ile taːska talɨ, tu͡ok basku͡oj oŋu͡ogaj, ogolor." </ta>
            <ta e="T322" id="Seg_2758" s="T318">Ontuŋ, kaja, bilbet ebit. </ta>
            <ta e="T337" id="Seg_2759" s="T322">Gini gini iti agalara ere iti biler, biːr hire ontuŋ amattan biːr daː iŋiːre hu͡ok. </ta>
            <ta e="T353" id="Seg_2760" s="T337">Biːrger sirin körü͡öŋ, üčügejkeːnnik kördökkö, ontuŋ kɨːs (hɨr-) hɨrajɨgar talɨ, dʼaktar hɨrajɨgar talɨ – biːrges öttüge. </ta>
            <ta e="T358" id="Seg_2761" s="T353">Ontugun agabɨt ire biler bu͡o. </ta>
            <ta e="T365" id="Seg_2762" s="T358">Dʼe, kajdi͡ek de bu͡olu͡ogun bert Bulčut u͡olbut. </ta>
            <ta e="T369" id="Seg_2763" s="T365">Kɨjŋanaːrɨ da gɨnar, kečeher. </ta>
            <ta e="T376" id="Seg_2764" s="T369">Onton dʼe kɨlas gɨnar diːn kɨːhɨn di͡et. </ta>
            <ta e="T379" id="Seg_2765" s="T376">Kɨːha nʼɨmalanna emi͡e. </ta>
            <ta e="T390" id="Seg_2766" s="T379">"Kajtak gɨnɨ͡amɨj", di͡en baran, kumaːrdartan ilbiner kördük, iliːtin orguːjkaːnnɨk bugurdukkaːn ɨlla. </ta>
            <ta e="T403" id="Seg_2767" s="T390">Onton bu orguːjkaːnnɨk bu bugurdukkaːn (hɨrajɨnan) hɨrajɨn dʼe kimniːr köllörör – kumaːrdartan tebenerge talɨ. </ta>
            <ta e="T409" id="Seg_2768" s="T403">Oː, dʼe, u͡olbut dʼe ü͡örer diːn. </ta>
            <ta e="T418" id="Seg_2769" s="T409">Türgen bagajɨtɨk bahagɨn ɨlla, onton orguːjkaːnnɨk kötöktö tobuk oŋu͡ogun. </ta>
            <ta e="T428" id="Seg_2770" s="T418">Bɨ͡aj, türgen türgen bagajtɨk bahagɨnan iŋiːr munʼaːnɨ tardan ɨlbat du͡o. </ta>
            <ta e="T431" id="Seg_2771" s="T428">Onno agata di͡ete: </ta>
            <ta e="T448" id="Seg_2772" s="T431">"Dʼe, kajtak gɨnɨ͡aŋɨj, min di͡ebitim, tɨlbɨn töttörü ɨlɨ͡am hu͡oga, itinnik uːs kihi, itinnik bulčut kihi barɨtɨn biler. </ta>
            <ta e="T453" id="Seg_2773" s="T448">Dʼe bi͡erebin eni͡eke, Bulčut, kɨːspɨn. </ta>
            <ta e="T461" id="Seg_2774" s="T453">Eteŋŋetik oloruŋ, üčügejdik oloruŋ, ügüs ogonu ogolonuŋ", di͡ete. </ta>
            <ta e="T470" id="Seg_2775" s="T461">Dʼe, ol kördükkeːn Bulčut kɨːhɨn kɨtta üčügej bagajtɨk olorbuttara. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_2776" s="T0">min</ta>
            <ta e="T2" id="Seg_2777" s="T1">bu</ta>
            <ta e="T3" id="Seg_2778" s="T2">öh-ü</ta>
            <ta e="T4" id="Seg_2779" s="T3">ist-i-bit-i-m</ta>
            <ta e="T5" id="Seg_2780" s="T4">kerget-ter-bi-n</ta>
            <ta e="T7" id="Seg_2781" s="T5">gɨtta</ta>
            <ta e="T8" id="Seg_2782" s="T7">usku͡ola-ga</ta>
            <ta e="T9" id="Seg_2783" s="T8">kiːr-er</ta>
            <ta e="T10" id="Seg_2784" s="T9">dʼɨl-ba-r</ta>
            <ta e="T11" id="Seg_2785" s="T10">on-tu-ŋ</ta>
            <ta e="T12" id="Seg_2786" s="T11">anɨ-ga</ta>
            <ta e="T13" id="Seg_2787" s="T12">di͡eri</ta>
            <ta e="T14" id="Seg_2788" s="T13">öj-bö-r</ta>
            <ta e="T15" id="Seg_2789" s="T14">baːr</ta>
            <ta e="T16" id="Seg_2790" s="T15">buːrduk-kaːn</ta>
            <ta e="T17" id="Seg_2791" s="T16">maːma-m</ta>
            <ta e="T18" id="Seg_2792" s="T17">edʼiːj-e</ta>
            <ta e="T19" id="Seg_2793" s="T18">Kɨlabɨja</ta>
            <ta e="T20" id="Seg_2794" s="T19">kepseː-čči</ta>
            <ta e="T21" id="Seg_2795" s="T20">e-t-e</ta>
            <ta e="T22" id="Seg_2796" s="T21">ɨraːk-ɨraːk</ta>
            <ta e="T23" id="Seg_2797" s="T22">mu͡ora-ga</ta>
            <ta e="T24" id="Seg_2798" s="T23">töröː-büt</ta>
            <ta e="T25" id="Seg_2799" s="T24">hir-i-ger</ta>
            <ta e="T26" id="Seg_2800" s="T25">beje-ti-n</ta>
            <ta e="T27" id="Seg_2801" s="T26">töröː-büt</ta>
            <ta e="T28" id="Seg_2802" s="T27">hir-i-ger</ta>
            <ta e="T29" id="Seg_2803" s="T28">kerget-ter-i-n</ta>
            <ta e="T30" id="Seg_2804" s="T29">kɨtta</ta>
            <ta e="T31" id="Seg_2805" s="T30">ajann-ɨː</ta>
            <ta e="T32" id="Seg_2806" s="T31">hɨldʼ-ɨ-bɨt-a</ta>
            <ta e="T33" id="Seg_2807" s="T32">basku͡oj</ta>
            <ta e="T34" id="Seg_2808" s="T33">bagajɨ</ta>
            <ta e="T35" id="Seg_2809" s="T34">u͡ol</ta>
            <ta e="T36" id="Seg_2810" s="T35">on-tu-ŋ</ta>
            <ta e="T37" id="Seg_2811" s="T36">aːt-a</ta>
            <ta e="T38" id="Seg_2812" s="T37">Bulčut</ta>
            <ta e="T39" id="Seg_2813" s="T38">e-t-e</ta>
            <ta e="T40" id="Seg_2814" s="T39">atɨn</ta>
            <ta e="T41" id="Seg_2815" s="T40">hir-ge</ta>
            <ta e="T44" id="Seg_2816" s="T43">giniler-ten</ta>
            <ta e="T45" id="Seg_2817" s="T44">ɨraːk-kaːn</ta>
            <ta e="T46" id="Seg_2818" s="T45">hah-ɨː</ta>
            <ta e="T47" id="Seg_2819" s="T46">hɨldʼ-ɨ-bɨt-a</ta>
            <ta e="T48" id="Seg_2820" s="T47">basku͡oj</ta>
            <ta e="T49" id="Seg_2821" s="T48">kɨːs</ta>
            <ta e="T50" id="Seg_2822" s="T49">kerget-ter-i-n</ta>
            <ta e="T51" id="Seg_2823" s="T50">kɨtta</ta>
            <ta e="T52" id="Seg_2824" s="T51">iti</ta>
            <ta e="T53" id="Seg_2825" s="T52">hiŋil</ta>
            <ta e="T54" id="Seg_2826" s="T53">eː</ta>
            <ta e="T55" id="Seg_2827" s="T54">u͡ol-u</ta>
            <ta e="T56" id="Seg_2828" s="T55">kɨtta</ta>
            <ta e="T57" id="Seg_2829" s="T56">iti</ta>
            <ta e="T58" id="Seg_2830" s="T57">kɨːs</ta>
            <ta e="T59" id="Seg_2831" s="T58">körs-öːččü</ta>
            <ta e="T60" id="Seg_2832" s="T59">e-bit-ter</ta>
            <ta e="T61" id="Seg_2833" s="T60">bagar-s-aːččɨ</ta>
            <ta e="T62" id="Seg_2834" s="T61">e-bit-ter</ta>
            <ta e="T63" id="Seg_2835" s="T62">beje-leri-n</ta>
            <ta e="T64" id="Seg_2836" s="T63">töröː-büt</ta>
            <ta e="T65" id="Seg_2837" s="T64">hir-deri-ger</ta>
            <ta e="T66" id="Seg_2838" s="T65">biːrde</ta>
            <ta e="T67" id="Seg_2839" s="T66">kɨːh-a</ta>
            <ta e="T68" id="Seg_2840" s="T67">Bulčut-u-n</ta>
            <ta e="T69" id="Seg_2841" s="T68">u͡ol-u-n</ta>
            <ta e="T70" id="Seg_2842" s="T69">haŋar-ar</ta>
            <ta e="T71" id="Seg_2843" s="T70">dokuːl</ta>
            <ta e="T72" id="Seg_2844" s="T71">bihigi</ta>
            <ta e="T73" id="Seg_2845" s="T72">kerget-ter-biti-tten</ta>
            <ta e="T74" id="Seg_2846" s="T73">kisten-e</ta>
            <ta e="T75" id="Seg_2847" s="T74">hɨldʼ-ɨ͡ak-pɨt=ɨj</ta>
            <ta e="T76" id="Seg_2848" s="T75">kel</ta>
            <ta e="T77" id="Seg_2849" s="T76">bihi͡e-ke</ta>
            <ta e="T78" id="Seg_2850" s="T77">ɨ͡allam-mɨt</ta>
            <ta e="T79" id="Seg_2851" s="T78">bu͡ol</ta>
            <ta e="T80" id="Seg_2852" s="T79">kerget-ter-bi-n</ta>
            <ta e="T81" id="Seg_2853" s="T80">kɨtta</ta>
            <ta e="T82" id="Seg_2854" s="T81">haŋar-ɨ-s</ta>
            <ta e="T83" id="Seg_2855" s="T82">aga-bɨ-n</ta>
            <ta e="T84" id="Seg_2856" s="T83">kɨtta</ta>
            <ta e="T85" id="Seg_2857" s="T84">haŋar-ɨ-s</ta>
            <ta e="T86" id="Seg_2858" s="T85">aga-bɨ-ttan</ta>
            <ta e="T87" id="Seg_2859" s="T86">kördöː</ta>
            <ta e="T88" id="Seg_2860" s="T87">minigi-n</ta>
            <ta e="T89" id="Seg_2861" s="T88">aga-m</ta>
            <ta e="T93" id="Seg_2862" s="T92">üčügej</ta>
            <ta e="T94" id="Seg_2863" s="T93">bagaj</ta>
            <ta e="T95" id="Seg_2864" s="T94">kihi</ta>
            <ta e="T96" id="Seg_2865" s="T95">daː</ta>
            <ta e="T97" id="Seg_2866" s="T96">bu͡ol-lar</ta>
            <ta e="T98" id="Seg_2867" s="T97">nʼɨma-laːk</ta>
            <ta e="T99" id="Seg_2868" s="T98">dʼe</ta>
            <ta e="T100" id="Seg_2869" s="T99">ol</ta>
            <ta e="T101" id="Seg_2870" s="T100">nʼɨma-tɨ-ttan</ta>
            <ta e="T102" id="Seg_2871" s="T101">en</ta>
            <ta e="T103" id="Seg_2872" s="T102">kečeh-i-me</ta>
            <ta e="T104" id="Seg_2873" s="T103">dʼe</ta>
            <ta e="T105" id="Seg_2874" s="T104">ol</ta>
            <ta e="T106" id="Seg_2875" s="T105">kördük-kaːn</ta>
            <ta e="T107" id="Seg_2876" s="T106">haŋar-d-a</ta>
            <ta e="T108" id="Seg_2877" s="T107">daː</ta>
            <ta e="T109" id="Seg_2878" s="T108">u͡ol-but</ta>
            <ta e="T110" id="Seg_2879" s="T109">ol</ta>
            <ta e="T111" id="Seg_2880" s="T110">kördük-keːn</ta>
            <ta e="T112" id="Seg_2881" s="T111">kas</ta>
            <ta e="T113" id="Seg_2882" s="T112">daː</ta>
            <ta e="T114" id="Seg_2883" s="T113">kün</ta>
            <ta e="T115" id="Seg_2884" s="T114">bu͡ol-an</ta>
            <ta e="T116" id="Seg_2885" s="T115">baraːn</ta>
            <ta e="T117" id="Seg_2886" s="T116">itiː</ta>
            <ta e="T118" id="Seg_2887" s="T117">bagajɨ</ta>
            <ta e="T119" id="Seg_2888" s="T118">küŋ-ŋe</ta>
            <ta e="T120" id="Seg_2889" s="T119">haːs</ta>
            <ta e="T121" id="Seg_2890" s="T120">kühü͡örü</ta>
            <ta e="T122" id="Seg_2891" s="T121">kün-neːk</ta>
            <ta e="T123" id="Seg_2892" s="T122">bagajɨ</ta>
            <ta e="T124" id="Seg_2893" s="T123">küŋ-ŋe</ta>
            <ta e="T125" id="Seg_2894" s="T124">dʼe</ta>
            <ta e="T126" id="Seg_2895" s="T125">uːčak-tan-an</ta>
            <ta e="T127" id="Seg_2896" s="T126">bar-aːn</ta>
            <ta e="T128" id="Seg_2897" s="T127">dʼe</ta>
            <ta e="T129" id="Seg_2898" s="T128">kel-er</ta>
            <ta e="T130" id="Seg_2899" s="T129">beje-ti-n</ta>
            <ta e="T133" id="Seg_2900" s="T132">čeːlkeː</ta>
            <ta e="T134" id="Seg_2901" s="T133">aːku-laːk</ta>
            <ta e="T135" id="Seg_2902" s="T134">on-tu-tu-nan</ta>
            <ta e="T136" id="Seg_2903" s="T135">ɨ͡allan-a</ta>
            <ta e="T137" id="Seg_2904" s="T136">bar-d-a</ta>
            <ta e="T138" id="Seg_2905" s="T137">kɨːh-ɨ-gar</ta>
            <ta e="T139" id="Seg_2906" s="T138">kiːr-eːt-in</ta>
            <ta e="T140" id="Seg_2907" s="T139">hir-ge</ta>
            <ta e="T141" id="Seg_2908" s="T140">di͡eri</ta>
            <ta e="T142" id="Seg_2909" s="T141">meniː-ti-n</ta>
            <ta e="T143" id="Seg_2910" s="T142">kerget-ter-i-ger</ta>
            <ta e="T144" id="Seg_2911" s="T143">bökčö-ŋnöː-n</ta>
            <ta e="T145" id="Seg_2912" s="T144">baraːn</ta>
            <ta e="T146" id="Seg_2913" s="T145">doroːbo-ŋ</ta>
            <ta e="T147" id="Seg_2914" s="T146">doroːbo-ŋ</ta>
            <ta e="T148" id="Seg_2915" s="T147">kɨrdʼagas-tar</ta>
            <ta e="T149" id="Seg_2916" s="T148">di͡e-t-e</ta>
            <ta e="T150" id="Seg_2917" s="T149">Bulčut</ta>
            <ta e="T151" id="Seg_2918" s="T150">u͡ol-but</ta>
            <ta e="T152" id="Seg_2919" s="T151">dʼe</ta>
            <ta e="T153" id="Seg_2920" s="T152">bu</ta>
            <ta e="T154" id="Seg_2921" s="T153">kel-li-m</ta>
            <ta e="T155" id="Seg_2922" s="T154">ehi͡e-ke</ta>
            <ta e="T157" id="Seg_2923" s="T156">edʼi͡e</ta>
            <ta e="T158" id="Seg_2924" s="T157">aga</ta>
            <ta e="T159" id="Seg_2925" s="T158">ehigi-tten</ta>
            <ta e="T160" id="Seg_2926" s="T159">kɨːs-kɨtɨ-n</ta>
            <ta e="T161" id="Seg_2927" s="T160">körd-üː-bün</ta>
            <ta e="T162" id="Seg_2928" s="T161">tug-u-n</ta>
            <ta e="T163" id="Seg_2929" s="T162">dokuːl</ta>
            <ta e="T164" id="Seg_2930" s="T163">kist-i͡ek-pit=ij</ta>
            <ta e="T165" id="Seg_2931" s="T164">bihigi</ta>
            <ta e="T166" id="Seg_2932" s="T165">bihigi</ta>
            <ta e="T167" id="Seg_2933" s="T166">domnuŋ-ŋu-ttan</ta>
            <ta e="T168" id="Seg_2934" s="T167">bagar-s-a-bɨt</ta>
            <ta e="T169" id="Seg_2935" s="T168">beje-beje-biti-n</ta>
            <ta e="T170" id="Seg_2936" s="T169">hürdeːk</ta>
            <ta e="T171" id="Seg_2937" s="T170">kɨlɨːl-ɨː-bɨt</ta>
            <ta e="T172" id="Seg_2938" s="T171">dʼe</ta>
            <ta e="T173" id="Seg_2939" s="T172">kajtak</ta>
            <ta e="T174" id="Seg_2940" s="T173">gɨn-ɨ͡a-ŋ=ɨj</ta>
            <ta e="T175" id="Seg_2941" s="T174">di͡e-t-e</ta>
            <ta e="T176" id="Seg_2942" s="T175">kɨlaŋn-ɨː-kɨlaŋn-ɨː</ta>
            <ta e="T177" id="Seg_2943" s="T176">kɨːs</ta>
            <ta e="T178" id="Seg_2944" s="T177">aga-ta</ta>
            <ta e="T179" id="Seg_2945" s="T178">onton</ta>
            <ta e="T180" id="Seg_2946" s="T179">dʼe</ta>
            <ta e="T181" id="Seg_2947" s="T180">nʼɨma-lan-aːrɨ</ta>
            <ta e="T182" id="Seg_2948" s="T181">gɨn-n-a</ta>
            <ta e="T183" id="Seg_2949" s="T182">bɨhɨlaːk</ta>
            <ta e="T184" id="Seg_2950" s="T183">d-iː</ta>
            <ta e="T185" id="Seg_2951" s="T184">hanaː-t-a</ta>
            <ta e="T186" id="Seg_2952" s="T185">u͡ol-but</ta>
            <ta e="T187" id="Seg_2953" s="T186">kirdik</ta>
            <ta e="T188" id="Seg_2954" s="T187">e-bit</ta>
            <ta e="T189" id="Seg_2955" s="T188">kɨːs-pɨt</ta>
            <ta e="T190" id="Seg_2956" s="T189">aga-ta</ta>
            <ta e="T191" id="Seg_2957" s="T190">tuːg-u</ta>
            <ta e="T192" id="Seg_2958" s="T191">ere</ta>
            <ta e="T193" id="Seg_2959" s="T192">bɨrak-t-a</ta>
            <ta e="T194" id="Seg_2960" s="T193">eːt</ta>
            <ta e="T195" id="Seg_2961" s="T194">čɨk</ta>
            <ta e="T196" id="Seg_2962" s="T195">gɨn-an</ta>
            <ta e="T197" id="Seg_2963" s="T196">keːs-t-e</ta>
            <ta e="T198" id="Seg_2964" s="T197">bu</ta>
            <ta e="T199" id="Seg_2965" s="T198">ačaːk</ta>
            <ta e="T200" id="Seg_2966" s="T199">ürdü-tü-nen</ta>
            <ta e="T201" id="Seg_2967" s="T200">d-iː</ta>
            <ta e="T202" id="Seg_2968" s="T201">hanaː-t-a</ta>
            <ta e="T203" id="Seg_2969" s="T202">onton</ta>
            <ta e="T204" id="Seg_2970" s="T203">tut-u͡o</ta>
            <ta e="T205" id="Seg_2971" s="T204">duː</ta>
            <ta e="T206" id="Seg_2972" s="T205">tut-u͡o</ta>
            <ta e="T207" id="Seg_2973" s="T206">du</ta>
            <ta e="T208" id="Seg_2974" s="T207">hu͡og-a</ta>
            <ta e="T209" id="Seg_2975" s="T208">duː</ta>
            <ta e="T210" id="Seg_2976" s="T209">bu</ta>
            <ta e="T211" id="Seg_2977" s="T210">oŋu͡og-u</ta>
            <ta e="T212" id="Seg_2978" s="T211">ka</ta>
            <ta e="T213" id="Seg_2979" s="T212">u͡ol-but</ta>
            <ta e="T214" id="Seg_2980" s="T213">dʼe</ta>
            <ta e="T215" id="Seg_2981" s="T214">türgen</ta>
            <ta e="T216" id="Seg_2982" s="T215">bagaj-dɨk</ta>
            <ta e="T217" id="Seg_2983" s="T216">tut-ar</ta>
            <ta e="T218" id="Seg_2984" s="T217">diːn</ta>
            <ta e="T219" id="Seg_2985" s="T218">o-nu</ta>
            <ta e="T220" id="Seg_2986" s="T219">ɨjɨt-ar</ta>
            <ta e="T221" id="Seg_2987" s="T220">kɨːs-pɨt</ta>
            <ta e="T222" id="Seg_2988" s="T221">aga-ta</ta>
            <ta e="T223" id="Seg_2989" s="T222">dʼe</ta>
            <ta e="T224" id="Seg_2990" s="T223">tug-u</ta>
            <ta e="T225" id="Seg_2991" s="T224">tut-tu-ŋ</ta>
            <ta e="T226" id="Seg_2992" s="T225">iti</ta>
            <ta e="T227" id="Seg_2993" s="T226">kajtak</ta>
            <ta e="T228" id="Seg_2994" s="T227">tuːg-u</ta>
            <ta e="T229" id="Seg_2995" s="T228">d-iː</ta>
            <ta e="T230" id="Seg_2996" s="T229">ɨjɨt-t-a</ta>
            <ta e="T231" id="Seg_2997" s="T230">ol</ta>
            <ta e="T232" id="Seg_2998" s="T231">ke</ta>
            <ta e="T233" id="Seg_2999" s="T232">kim</ta>
            <ta e="T234" id="Seg_3000" s="T233">u͡ol-but</ta>
            <ta e="T235" id="Seg_3001" s="T234">min</ta>
            <ta e="T238" id="Seg_3002" s="T237">min</ta>
            <ta e="T239" id="Seg_3003" s="T238">kör-dü-m</ta>
            <ta e="T240" id="Seg_3004" s="T239">taːj-dɨ-m</ta>
            <ta e="T241" id="Seg_3005" s="T240">tobuk</ta>
            <ta e="T242" id="Seg_3006" s="T241">oŋu͡og-a</ta>
            <ta e="T243" id="Seg_3007" s="T242">kim</ta>
            <ta e="T244" id="Seg_3008" s="T243">taːj-bat</ta>
            <ta e="T245" id="Seg_3009" s="T244">ol</ta>
            <ta e="T246" id="Seg_3010" s="T245">tobuk</ta>
            <ta e="T247" id="Seg_3011" s="T246">oŋu͡og-u-n</ta>
            <ta e="T248" id="Seg_3012" s="T247">kim</ta>
            <ta e="T249" id="Seg_3013" s="T248">taːj-ɨ-m-ɨ͡a=j</ta>
            <ta e="T250" id="Seg_3014" s="T249">ol</ta>
            <ta e="T251" id="Seg_3015" s="T250">tobuk</ta>
            <ta e="T252" id="Seg_3016" s="T251">oŋu͡og-u-n</ta>
            <ta e="T253" id="Seg_3017" s="T252">di͡e-t-e</ta>
            <ta e="T254" id="Seg_3018" s="T253">aga-bɨt</ta>
            <ta e="T257" id="Seg_3019" s="T256">nʼɨma-ŋn-ɨː-nʼɨma-ŋn-ɨː</ta>
            <ta e="T258" id="Seg_3020" s="T257">ɨmaŋn-ɨː-ɨmaŋn-ɨː</ta>
            <ta e="T259" id="Seg_3021" s="T258">di͡e-t-e</ta>
            <ta e="T260" id="Seg_3022" s="T259">dʼe</ta>
            <ta e="T261" id="Seg_3023" s="T260">taːj-dɨ-ŋ</ta>
            <ta e="T262" id="Seg_3024" s="T261">tobuk</ta>
            <ta e="T263" id="Seg_3025" s="T262">oŋu͡og-u-n</ta>
            <ta e="T264" id="Seg_3026" s="T263">üčügej</ta>
            <ta e="T265" id="Seg_3027" s="T264">dʼe</ta>
            <ta e="T266" id="Seg_3028" s="T265">bu</ta>
            <ta e="T269" id="Seg_3029" s="T268">tobuk</ta>
            <ta e="T270" id="Seg_3030" s="T269">oŋu͡og-u-gar</ta>
            <ta e="T271" id="Seg_3031" s="T270">kantan</ta>
            <ta e="T272" id="Seg_3032" s="T271">emete</ta>
            <ta e="T275" id="Seg_3033" s="T274">kör-öŋ-ŋün</ta>
            <ta e="T276" id="Seg_3034" s="T275">biːr</ta>
            <ta e="T277" id="Seg_3035" s="T276">emete</ta>
            <ta e="T278" id="Seg_3036" s="T277">iŋiːr-i</ta>
            <ta e="T279" id="Seg_3037" s="T278">bul-u͡o-ŋ</ta>
            <ta e="T280" id="Seg_3038" s="T279">dʼuru</ta>
            <ta e="T281" id="Seg_3039" s="T280">oččogo</ta>
            <ta e="T282" id="Seg_3040" s="T281">min</ta>
            <ta e="T283" id="Seg_3041" s="T282">haŋa-bɨ-n</ta>
            <ta e="T284" id="Seg_3042" s="T283">ɨl-ɨ͡a-m</ta>
            <ta e="T285" id="Seg_3043" s="T284">hu͡og-a</ta>
            <ta e="T286" id="Seg_3044" s="T285">töttörü</ta>
            <ta e="T287" id="Seg_3045" s="T286">bul-lak-kɨna</ta>
            <ta e="T288" id="Seg_3046" s="T287">biːr</ta>
            <ta e="T289" id="Seg_3047" s="T288">emete</ta>
            <ta e="T290" id="Seg_3048" s="T289">iŋiːr-i</ta>
            <ta e="T291" id="Seg_3049" s="T290">oččogo</ta>
            <ta e="T292" id="Seg_3050" s="T291">min</ta>
            <ta e="T293" id="Seg_3051" s="T292">kɨːs-pɨ-n</ta>
            <ta e="T294" id="Seg_3052" s="T293">eni͡e-ke</ta>
            <ta e="T295" id="Seg_3053" s="T294">bi͡er-i͡e-m</ta>
            <ta e="T296" id="Seg_3054" s="T295">dʼe</ta>
            <ta e="T297" id="Seg_3055" s="T296">u͡ol-but</ta>
            <ta e="T298" id="Seg_3056" s="T297">kör-ö</ta>
            <ta e="T299" id="Seg_3057" s="T298">hataː-t-a</ta>
            <ta e="T300" id="Seg_3058" s="T299">kör-ö</ta>
            <ta e="T301" id="Seg_3059" s="T300">hataː-t-a</ta>
            <ta e="T302" id="Seg_3060" s="T301">onton</ta>
            <ta e="T303" id="Seg_3061" s="T302">orguːjkaːn-nɨk</ta>
            <ta e="T304" id="Seg_3062" s="T303">beje-te</ta>
            <ta e="T305" id="Seg_3063" s="T304">beje-ti-ger</ta>
            <ta e="T306" id="Seg_3064" s="T305">haŋar-ar</ta>
            <ta e="T307" id="Seg_3065" s="T306">o</ta>
            <ta e="T308" id="Seg_3066" s="T307">dʼe</ta>
            <ta e="T309" id="Seg_3067" s="T308">hürdeːk</ta>
            <ta e="T310" id="Seg_3068" s="T309">e-bit</ta>
            <ta e="T311" id="Seg_3069" s="T310">dʼe</ta>
            <ta e="T312" id="Seg_3070" s="T311">ile</ta>
            <ta e="T313" id="Seg_3071" s="T312">taːs-ka</ta>
            <ta e="T314" id="Seg_3072" s="T313">talɨ</ta>
            <ta e="T315" id="Seg_3073" s="T314">tu͡ok</ta>
            <ta e="T316" id="Seg_3074" s="T315">basku͡oj</ta>
            <ta e="T317" id="Seg_3075" s="T316">oŋu͡og-a=j</ta>
            <ta e="T318" id="Seg_3076" s="T317">ogo-lor</ta>
            <ta e="T319" id="Seg_3077" s="T318">on-tu-ŋ</ta>
            <ta e="T320" id="Seg_3078" s="T319">kaja</ta>
            <ta e="T321" id="Seg_3079" s="T320">bil-bet</ta>
            <ta e="T322" id="Seg_3080" s="T321">e-bit</ta>
            <ta e="T323" id="Seg_3081" s="T322">gini</ta>
            <ta e="T324" id="Seg_3082" s="T323">gini</ta>
            <ta e="T325" id="Seg_3083" s="T324">iti</ta>
            <ta e="T326" id="Seg_3084" s="T325">aga-lara</ta>
            <ta e="T327" id="Seg_3085" s="T326">ere</ta>
            <ta e="T328" id="Seg_3086" s="T327">iti</ta>
            <ta e="T329" id="Seg_3087" s="T328">bil-er</ta>
            <ta e="T330" id="Seg_3088" s="T329">biːr</ta>
            <ta e="T331" id="Seg_3089" s="T330">hir-e</ta>
            <ta e="T332" id="Seg_3090" s="T331">on-tu-ŋ</ta>
            <ta e="T333" id="Seg_3091" s="T332">amattan</ta>
            <ta e="T334" id="Seg_3092" s="T333">biːr</ta>
            <ta e="T335" id="Seg_3093" s="T334">daː</ta>
            <ta e="T336" id="Seg_3094" s="T335">iŋiːr-e</ta>
            <ta e="T337" id="Seg_3095" s="T336">hu͡ok</ta>
            <ta e="T338" id="Seg_3096" s="T337">biːr-ge-r</ta>
            <ta e="T339" id="Seg_3097" s="T338">sir-i-n</ta>
            <ta e="T340" id="Seg_3098" s="T339">kör-ü͡ö-ŋ</ta>
            <ta e="T341" id="Seg_3099" s="T340">üčügej-keːn-nik</ta>
            <ta e="T342" id="Seg_3100" s="T341">kör-dök-kö</ta>
            <ta e="T343" id="Seg_3101" s="T342">on-tu-ŋ</ta>
            <ta e="T344" id="Seg_3102" s="T343">kɨːs</ta>
            <ta e="T347" id="Seg_3103" s="T346">hɨraj-ɨ-gar</ta>
            <ta e="T348" id="Seg_3104" s="T347">talɨ</ta>
            <ta e="T349" id="Seg_3105" s="T348">dʼaktar</ta>
            <ta e="T350" id="Seg_3106" s="T349">hɨraj-ɨ-gar</ta>
            <ta e="T351" id="Seg_3107" s="T350">talɨ</ta>
            <ta e="T352" id="Seg_3108" s="T351">biːrges</ta>
            <ta e="T353" id="Seg_3109" s="T352">ött-ü-ge</ta>
            <ta e="T354" id="Seg_3110" s="T353">on-tu-gu-n</ta>
            <ta e="T355" id="Seg_3111" s="T354">aga-bɨt</ta>
            <ta e="T356" id="Seg_3112" s="T355">ire</ta>
            <ta e="T357" id="Seg_3113" s="T356">bil-er</ta>
            <ta e="T358" id="Seg_3114" s="T357">bu͡o</ta>
            <ta e="T359" id="Seg_3115" s="T358">dʼe</ta>
            <ta e="T360" id="Seg_3116" s="T359">kajdi͡ek</ta>
            <ta e="T361" id="Seg_3117" s="T360">de</ta>
            <ta e="T362" id="Seg_3118" s="T361">bu͡ol-u͡og-u-n</ta>
            <ta e="T363" id="Seg_3119" s="T362">bert</ta>
            <ta e="T364" id="Seg_3120" s="T363">Bulčut</ta>
            <ta e="T365" id="Seg_3121" s="T364">u͡ol-but</ta>
            <ta e="T366" id="Seg_3122" s="T365">kɨjŋan-aːrɨ</ta>
            <ta e="T367" id="Seg_3123" s="T366">da</ta>
            <ta e="T368" id="Seg_3124" s="T367">gɨn-ar</ta>
            <ta e="T369" id="Seg_3125" s="T368">kečeh-er</ta>
            <ta e="T370" id="Seg_3126" s="T369">onton</ta>
            <ta e="T371" id="Seg_3127" s="T370">dʼe</ta>
            <ta e="T372" id="Seg_3128" s="T371">kɨla-s</ta>
            <ta e="T373" id="Seg_3129" s="T372">gɨn-ar</ta>
            <ta e="T374" id="Seg_3130" s="T373">diːn</ta>
            <ta e="T375" id="Seg_3131" s="T374">kɨːh-ɨ-n</ta>
            <ta e="T376" id="Seg_3132" s="T375">di͡et</ta>
            <ta e="T377" id="Seg_3133" s="T376">kɨːh-a</ta>
            <ta e="T378" id="Seg_3134" s="T377">nʼɨma-lan-n-a</ta>
            <ta e="T379" id="Seg_3135" s="T378">emi͡e</ta>
            <ta e="T380" id="Seg_3136" s="T379">kajtak</ta>
            <ta e="T381" id="Seg_3137" s="T380">gɨn-ɨ͡a-m=ɨj</ta>
            <ta e="T382" id="Seg_3138" s="T381">di͡e-n</ta>
            <ta e="T383" id="Seg_3139" s="T382">baran</ta>
            <ta e="T384" id="Seg_3140" s="T383">kumaːr-dar-tan</ta>
            <ta e="T385" id="Seg_3141" s="T384">ilbin-er</ta>
            <ta e="T386" id="Seg_3142" s="T385">kördük</ta>
            <ta e="T387" id="Seg_3143" s="T386">iliː-ti-n</ta>
            <ta e="T388" id="Seg_3144" s="T387">orguːjkaːn-nɨk</ta>
            <ta e="T389" id="Seg_3145" s="T388">bugurduk-kaːn</ta>
            <ta e="T390" id="Seg_3146" s="T389">ɨl-l-a</ta>
            <ta e="T391" id="Seg_3147" s="T390">onton</ta>
            <ta e="T392" id="Seg_3148" s="T391">bu</ta>
            <ta e="T393" id="Seg_3149" s="T392">orguːjkaːn-nɨk</ta>
            <ta e="T394" id="Seg_3150" s="T393">bu</ta>
            <ta e="T395" id="Seg_3151" s="T394">bugurduk-kaːn</ta>
            <ta e="T396" id="Seg_3152" s="T395">hɨraj-ɨ-nan</ta>
            <ta e="T397" id="Seg_3153" s="T396">hɨraj-ɨ-n</ta>
            <ta e="T398" id="Seg_3154" s="T397">dʼe</ta>
            <ta e="T399" id="Seg_3155" s="T398">kim-niː-r</ta>
            <ta e="T400" id="Seg_3156" s="T399">köllör-ör</ta>
            <ta e="T401" id="Seg_3157" s="T400">kumaːr-dar-tan</ta>
            <ta e="T402" id="Seg_3158" s="T401">teb-e-n-er-ge</ta>
            <ta e="T403" id="Seg_3159" s="T402">talɨ</ta>
            <ta e="T404" id="Seg_3160" s="T403">oː</ta>
            <ta e="T405" id="Seg_3161" s="T404">dʼe</ta>
            <ta e="T406" id="Seg_3162" s="T405">u͡ol-but</ta>
            <ta e="T407" id="Seg_3163" s="T406">dʼe</ta>
            <ta e="T408" id="Seg_3164" s="T407">ü͡ör-er</ta>
            <ta e="T409" id="Seg_3165" s="T408">diːn</ta>
            <ta e="T410" id="Seg_3166" s="T409">türgen</ta>
            <ta e="T411" id="Seg_3167" s="T410">bagajɨ-tɨk</ta>
            <ta e="T412" id="Seg_3168" s="T411">bahag-ɨ-n</ta>
            <ta e="T413" id="Seg_3169" s="T412">ɨl-l-a</ta>
            <ta e="T414" id="Seg_3170" s="T413">onton</ta>
            <ta e="T415" id="Seg_3171" s="T414">orguːjkaːn-nɨk</ta>
            <ta e="T416" id="Seg_3172" s="T415">kötök-t-ö</ta>
            <ta e="T417" id="Seg_3173" s="T416">tobuk</ta>
            <ta e="T418" id="Seg_3174" s="T417">oŋu͡og-u-n</ta>
            <ta e="T419" id="Seg_3175" s="T418">bɨ͡aj</ta>
            <ta e="T420" id="Seg_3176" s="T419">türgen</ta>
            <ta e="T421" id="Seg_3177" s="T420">türgen</ta>
            <ta e="T422" id="Seg_3178" s="T421">bagaj-tɨk</ta>
            <ta e="T423" id="Seg_3179" s="T422">bahag-ɨ-nan</ta>
            <ta e="T424" id="Seg_3180" s="T423">iŋiːr</ta>
            <ta e="T425" id="Seg_3181" s="T424">munʼaː-nɨ</ta>
            <ta e="T426" id="Seg_3182" s="T425">tard-an</ta>
            <ta e="T427" id="Seg_3183" s="T426">ɨl-bat</ta>
            <ta e="T428" id="Seg_3184" s="T427">du͡o</ta>
            <ta e="T429" id="Seg_3185" s="T428">onno</ta>
            <ta e="T430" id="Seg_3186" s="T429">aga-ta</ta>
            <ta e="T431" id="Seg_3187" s="T430">di͡e-t-e</ta>
            <ta e="T432" id="Seg_3188" s="T431">dʼe</ta>
            <ta e="T433" id="Seg_3189" s="T432">kajtak</ta>
            <ta e="T434" id="Seg_3190" s="T433">gɨn-ɨ͡a-ŋ=ɨj</ta>
            <ta e="T435" id="Seg_3191" s="T434">min</ta>
            <ta e="T436" id="Seg_3192" s="T435">di͡e-bit-i-m</ta>
            <ta e="T437" id="Seg_3193" s="T436">tɨl-bɨ-n</ta>
            <ta e="T438" id="Seg_3194" s="T437">töttörü</ta>
            <ta e="T439" id="Seg_3195" s="T438">ɨl-ɨ͡a-m</ta>
            <ta e="T440" id="Seg_3196" s="T439">hu͡og-a</ta>
            <ta e="T441" id="Seg_3197" s="T440">itinnik</ta>
            <ta e="T442" id="Seg_3198" s="T441">uːs</ta>
            <ta e="T443" id="Seg_3199" s="T442">kihi</ta>
            <ta e="T444" id="Seg_3200" s="T443">itinnik</ta>
            <ta e="T445" id="Seg_3201" s="T444">bulčut</ta>
            <ta e="T446" id="Seg_3202" s="T445">kihi</ta>
            <ta e="T447" id="Seg_3203" s="T446">barɨ-tɨ-n</ta>
            <ta e="T448" id="Seg_3204" s="T447">bil-er</ta>
            <ta e="T449" id="Seg_3205" s="T448">dʼe</ta>
            <ta e="T450" id="Seg_3206" s="T449">bi͡er-e-bin</ta>
            <ta e="T451" id="Seg_3207" s="T450">eni͡e-ke</ta>
            <ta e="T452" id="Seg_3208" s="T451">Bulčut</ta>
            <ta e="T453" id="Seg_3209" s="T452">kɨːs-pɨ-n</ta>
            <ta e="T454" id="Seg_3210" s="T453">eteŋŋe-tik</ta>
            <ta e="T455" id="Seg_3211" s="T454">olor-u-ŋ</ta>
            <ta e="T456" id="Seg_3212" s="T455">üčügej-dik</ta>
            <ta e="T457" id="Seg_3213" s="T456">olor-u-ŋ</ta>
            <ta e="T458" id="Seg_3214" s="T457">ügüs</ta>
            <ta e="T459" id="Seg_3215" s="T458">ogo-nu</ta>
            <ta e="T460" id="Seg_3216" s="T459">ogo-lon-u-ŋ</ta>
            <ta e="T461" id="Seg_3217" s="T460">di͡e-t-e</ta>
            <ta e="T462" id="Seg_3218" s="T461">dʼe</ta>
            <ta e="T463" id="Seg_3219" s="T462">ol</ta>
            <ta e="T464" id="Seg_3220" s="T463">kördük-keːn</ta>
            <ta e="T465" id="Seg_3221" s="T464">Bulčut</ta>
            <ta e="T466" id="Seg_3222" s="T465">kɨːh-ɨ-n</ta>
            <ta e="T467" id="Seg_3223" s="T466">kɨtta</ta>
            <ta e="T468" id="Seg_3224" s="T467">üčügej</ta>
            <ta e="T469" id="Seg_3225" s="T468">bagaj-tɨk</ta>
            <ta e="T470" id="Seg_3226" s="T469">olor-but-tara</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_3227" s="T0">min</ta>
            <ta e="T2" id="Seg_3228" s="T1">bu</ta>
            <ta e="T3" id="Seg_3229" s="T2">ös-nI</ta>
            <ta e="T4" id="Seg_3230" s="T3">ihit-I-BIT-I-m</ta>
            <ta e="T5" id="Seg_3231" s="T4">kergen-LAr-BI-n</ta>
            <ta e="T7" id="Seg_3232" s="T5">kɨtta</ta>
            <ta e="T8" id="Seg_3233" s="T7">usku͡ola-GA</ta>
            <ta e="T9" id="Seg_3234" s="T8">kiːr-Ar</ta>
            <ta e="T10" id="Seg_3235" s="T9">dʼɨl-BA-r</ta>
            <ta e="T11" id="Seg_3236" s="T10">ol-tI-ŋ</ta>
            <ta e="T12" id="Seg_3237" s="T11">anɨ-GA</ta>
            <ta e="T13" id="Seg_3238" s="T12">di͡eri</ta>
            <ta e="T14" id="Seg_3239" s="T13">öj-BA-r</ta>
            <ta e="T15" id="Seg_3240" s="T14">baːr</ta>
            <ta e="T16" id="Seg_3241" s="T15">bugurduk-kAːN</ta>
            <ta e="T17" id="Seg_3242" s="T16">maːma-m</ta>
            <ta e="T18" id="Seg_3243" s="T17">edʼij-tA</ta>
            <ta e="T19" id="Seg_3244" s="T18">Kɨlabɨja</ta>
            <ta e="T20" id="Seg_3245" s="T19">kepseː-AːččI</ta>
            <ta e="T21" id="Seg_3246" s="T20">e-TI-tA</ta>
            <ta e="T22" id="Seg_3247" s="T21">ɨraːk-ɨraːk</ta>
            <ta e="T23" id="Seg_3248" s="T22">mu͡ora-GA</ta>
            <ta e="T24" id="Seg_3249" s="T23">töröː-BIT</ta>
            <ta e="T25" id="Seg_3250" s="T24">hir-tI-GAr</ta>
            <ta e="T26" id="Seg_3251" s="T25">beje-tI-n</ta>
            <ta e="T27" id="Seg_3252" s="T26">töröː-BIT</ta>
            <ta e="T28" id="Seg_3253" s="T27">hir-tI-GAr</ta>
            <ta e="T29" id="Seg_3254" s="T28">kergen-LAr-tI-n</ta>
            <ta e="T30" id="Seg_3255" s="T29">kɨtta</ta>
            <ta e="T31" id="Seg_3256" s="T30">ajannaː-A</ta>
            <ta e="T32" id="Seg_3257" s="T31">hɨrɨt-I-BIT-tA</ta>
            <ta e="T33" id="Seg_3258" s="T32">bosku͡oj</ta>
            <ta e="T34" id="Seg_3259" s="T33">bagajɨ</ta>
            <ta e="T35" id="Seg_3260" s="T34">u͡ol</ta>
            <ta e="T36" id="Seg_3261" s="T35">ol-tI-ŋ</ta>
            <ta e="T37" id="Seg_3262" s="T36">aːt-tA</ta>
            <ta e="T38" id="Seg_3263" s="T37">Bulčut</ta>
            <ta e="T39" id="Seg_3264" s="T38">e-TI-tA</ta>
            <ta e="T40" id="Seg_3265" s="T39">atɨn</ta>
            <ta e="T41" id="Seg_3266" s="T40">hir-GA</ta>
            <ta e="T44" id="Seg_3267" s="T43">giniler-ttAn</ta>
            <ta e="T45" id="Seg_3268" s="T44">ɨraːk-kAːN</ta>
            <ta e="T46" id="Seg_3269" s="T45">has-A</ta>
            <ta e="T47" id="Seg_3270" s="T46">hɨrɨt-I-BIT-tA</ta>
            <ta e="T48" id="Seg_3271" s="T47">bosku͡oj</ta>
            <ta e="T49" id="Seg_3272" s="T48">kɨːs</ta>
            <ta e="T50" id="Seg_3273" s="T49">kergen-LAr-tI-n</ta>
            <ta e="T51" id="Seg_3274" s="T50">kɨtta</ta>
            <ta e="T52" id="Seg_3275" s="T51">iti</ta>
            <ta e="T53" id="Seg_3276" s="T52">hiŋil</ta>
            <ta e="T54" id="Seg_3277" s="T53">eː</ta>
            <ta e="T55" id="Seg_3278" s="T54">u͡ol-nI</ta>
            <ta e="T56" id="Seg_3279" s="T55">kɨtta</ta>
            <ta e="T57" id="Seg_3280" s="T56">iti</ta>
            <ta e="T58" id="Seg_3281" s="T57">kɨːs</ta>
            <ta e="T59" id="Seg_3282" s="T58">körüs-AːččI</ta>
            <ta e="T60" id="Seg_3283" s="T59">e-BIT-LAr</ta>
            <ta e="T61" id="Seg_3284" s="T60">bagar-s-AːččI</ta>
            <ta e="T62" id="Seg_3285" s="T61">e-BIT-LAr</ta>
            <ta e="T63" id="Seg_3286" s="T62">beje-LArI-n</ta>
            <ta e="T64" id="Seg_3287" s="T63">töröː-BIT</ta>
            <ta e="T65" id="Seg_3288" s="T64">hir-LArI-GAr</ta>
            <ta e="T66" id="Seg_3289" s="T65">biːrde</ta>
            <ta e="T67" id="Seg_3290" s="T66">kɨːs-tA</ta>
            <ta e="T68" id="Seg_3291" s="T67">Bulčut-tI-n</ta>
            <ta e="T69" id="Seg_3292" s="T68">u͡ol-tI-n</ta>
            <ta e="T70" id="Seg_3293" s="T69">haŋar-Ar</ta>
            <ta e="T71" id="Seg_3294" s="T70">dokuːl</ta>
            <ta e="T72" id="Seg_3295" s="T71">bihigi</ta>
            <ta e="T73" id="Seg_3296" s="T72">kergen-LAr-BItI-ttAn</ta>
            <ta e="T74" id="Seg_3297" s="T73">kisten-A</ta>
            <ta e="T75" id="Seg_3298" s="T74">hɨrɨt-IAK-BIt=Ij</ta>
            <ta e="T76" id="Seg_3299" s="T75">kel</ta>
            <ta e="T77" id="Seg_3300" s="T76">bihigi-GA</ta>
            <ta e="T78" id="Seg_3301" s="T77">ɨ͡allan-BIT</ta>
            <ta e="T79" id="Seg_3302" s="T78">bu͡ol</ta>
            <ta e="T80" id="Seg_3303" s="T79">kergen-LAr-BI-n</ta>
            <ta e="T81" id="Seg_3304" s="T80">kɨtta</ta>
            <ta e="T82" id="Seg_3305" s="T81">haŋar-I-s</ta>
            <ta e="T83" id="Seg_3306" s="T82">aga-BI-n</ta>
            <ta e="T84" id="Seg_3307" s="T83">kɨtta</ta>
            <ta e="T85" id="Seg_3308" s="T84">haŋar-I-s</ta>
            <ta e="T86" id="Seg_3309" s="T85">aga-BI-ttAn</ta>
            <ta e="T87" id="Seg_3310" s="T86">kördöː</ta>
            <ta e="T88" id="Seg_3311" s="T87">min-n</ta>
            <ta e="T89" id="Seg_3312" s="T88">aga-m</ta>
            <ta e="T93" id="Seg_3313" s="T92">üčügej</ta>
            <ta e="T94" id="Seg_3314" s="T93">bagajɨ</ta>
            <ta e="T95" id="Seg_3315" s="T94">kihi</ta>
            <ta e="T96" id="Seg_3316" s="T95">da</ta>
            <ta e="T97" id="Seg_3317" s="T96">bu͡ol-TAR</ta>
            <ta e="T98" id="Seg_3318" s="T97">nʼɨma-LAːK</ta>
            <ta e="T99" id="Seg_3319" s="T98">dʼe</ta>
            <ta e="T100" id="Seg_3320" s="T99">ol</ta>
            <ta e="T101" id="Seg_3321" s="T100">nʼɨma-tI-ttAn</ta>
            <ta e="T102" id="Seg_3322" s="T101">en</ta>
            <ta e="T103" id="Seg_3323" s="T102">kečes-I-m</ta>
            <ta e="T104" id="Seg_3324" s="T103">dʼe</ta>
            <ta e="T105" id="Seg_3325" s="T104">ol</ta>
            <ta e="T106" id="Seg_3326" s="T105">kördük-kAːN</ta>
            <ta e="T107" id="Seg_3327" s="T106">haŋar-TI-tA</ta>
            <ta e="T108" id="Seg_3328" s="T107">da</ta>
            <ta e="T109" id="Seg_3329" s="T108">u͡ol-BIt</ta>
            <ta e="T110" id="Seg_3330" s="T109">ol</ta>
            <ta e="T111" id="Seg_3331" s="T110">kördük-kAːN</ta>
            <ta e="T112" id="Seg_3332" s="T111">kas</ta>
            <ta e="T113" id="Seg_3333" s="T112">da</ta>
            <ta e="T114" id="Seg_3334" s="T113">kün</ta>
            <ta e="T115" id="Seg_3335" s="T114">bu͡ol-An</ta>
            <ta e="T116" id="Seg_3336" s="T115">baran</ta>
            <ta e="T117" id="Seg_3337" s="T116">itiː</ta>
            <ta e="T118" id="Seg_3338" s="T117">bagajɨ</ta>
            <ta e="T119" id="Seg_3339" s="T118">kün-GA</ta>
            <ta e="T120" id="Seg_3340" s="T119">haːs</ta>
            <ta e="T121" id="Seg_3341" s="T120">kühü͡örü</ta>
            <ta e="T122" id="Seg_3342" s="T121">kün-LAːK</ta>
            <ta e="T123" id="Seg_3343" s="T122">bagajɨ</ta>
            <ta e="T124" id="Seg_3344" s="T123">kün-GA</ta>
            <ta e="T125" id="Seg_3345" s="T124">dʼe</ta>
            <ta e="T126" id="Seg_3346" s="T125">ugučak-LAN-An</ta>
            <ta e="T127" id="Seg_3347" s="T126">bar-An</ta>
            <ta e="T128" id="Seg_3348" s="T127">dʼe</ta>
            <ta e="T129" id="Seg_3349" s="T128">kel-Ar</ta>
            <ta e="T130" id="Seg_3350" s="T129">beje-tI-n</ta>
            <ta e="T133" id="Seg_3351" s="T132">čeːlkeː</ta>
            <ta e="T134" id="Seg_3352" s="T133">aːku-LAːK</ta>
            <ta e="T135" id="Seg_3353" s="T134">ol-tI-tI-nAn</ta>
            <ta e="T136" id="Seg_3354" s="T135">ɨ͡allan-A</ta>
            <ta e="T137" id="Seg_3355" s="T136">bar-TI-tA</ta>
            <ta e="T138" id="Seg_3356" s="T137">kɨːs-tI-GAr</ta>
            <ta e="T139" id="Seg_3357" s="T138">kiːr-AːT-In</ta>
            <ta e="T140" id="Seg_3358" s="T139">hir-GA</ta>
            <ta e="T141" id="Seg_3359" s="T140">di͡eri</ta>
            <ta e="T142" id="Seg_3360" s="T141">menʼiː-tI-n</ta>
            <ta e="T143" id="Seg_3361" s="T142">kergen-LAr-tI-GAr</ta>
            <ta e="T144" id="Seg_3362" s="T143">bökčöj-ŋnAː-An</ta>
            <ta e="T145" id="Seg_3363" s="T144">baran</ta>
            <ta e="T146" id="Seg_3364" s="T145">doroːbo-ŋ</ta>
            <ta e="T147" id="Seg_3365" s="T146">doroːbo-ŋ</ta>
            <ta e="T148" id="Seg_3366" s="T147">kɨrdʼagas-LAr</ta>
            <ta e="T149" id="Seg_3367" s="T148">di͡e-TI-tA</ta>
            <ta e="T150" id="Seg_3368" s="T149">Bulčut</ta>
            <ta e="T151" id="Seg_3369" s="T150">u͡ol-BIt</ta>
            <ta e="T152" id="Seg_3370" s="T151">dʼe</ta>
            <ta e="T153" id="Seg_3371" s="T152">bu</ta>
            <ta e="T154" id="Seg_3372" s="T153">kel-TI-m</ta>
            <ta e="T155" id="Seg_3373" s="T154">ehigi-GA</ta>
            <ta e="T157" id="Seg_3374" s="T156">edʼij</ta>
            <ta e="T158" id="Seg_3375" s="T157">aga</ta>
            <ta e="T159" id="Seg_3376" s="T158">ehigi-ttAn</ta>
            <ta e="T160" id="Seg_3377" s="T159">kɨːs-GItI-n</ta>
            <ta e="T161" id="Seg_3378" s="T160">kördöː-A-BIn</ta>
            <ta e="T162" id="Seg_3379" s="T161">tu͡ok-tI-n</ta>
            <ta e="T163" id="Seg_3380" s="T162">dokuːl</ta>
            <ta e="T164" id="Seg_3381" s="T163">kisteː-IAK-BIt=Ij</ta>
            <ta e="T165" id="Seg_3382" s="T164">bihigi</ta>
            <ta e="T166" id="Seg_3383" s="T165">bihigi</ta>
            <ta e="T167" id="Seg_3384" s="T166">dabnu͡o-GI-ttAn</ta>
            <ta e="T168" id="Seg_3385" s="T167">bagar-s-A-BIt</ta>
            <ta e="T169" id="Seg_3386" s="T168">beje-beje-BItI-n</ta>
            <ta e="T170" id="Seg_3387" s="T169">hürdeːk</ta>
            <ta e="T171" id="Seg_3388" s="T170">kɨlɨːlaː-A-BIt</ta>
            <ta e="T172" id="Seg_3389" s="T171">dʼe</ta>
            <ta e="T173" id="Seg_3390" s="T172">kajdak</ta>
            <ta e="T174" id="Seg_3391" s="T173">gɨn-IAK-ŋ=Ij</ta>
            <ta e="T175" id="Seg_3392" s="T174">di͡e-TI-tA</ta>
            <ta e="T176" id="Seg_3393" s="T175">kɨlaŋnaː-A-kɨlaŋnaː-A</ta>
            <ta e="T177" id="Seg_3394" s="T176">kɨːs</ta>
            <ta e="T178" id="Seg_3395" s="T177">aga-tA</ta>
            <ta e="T179" id="Seg_3396" s="T178">onton</ta>
            <ta e="T180" id="Seg_3397" s="T179">dʼe</ta>
            <ta e="T181" id="Seg_3398" s="T180">nʼɨma-LAN-AːrI</ta>
            <ta e="T182" id="Seg_3399" s="T181">gɨn-TI-tA</ta>
            <ta e="T183" id="Seg_3400" s="T182">bɨhɨːlaːk</ta>
            <ta e="T184" id="Seg_3401" s="T183">di͡e-A</ta>
            <ta e="T185" id="Seg_3402" s="T184">hanaː-TI-tA</ta>
            <ta e="T186" id="Seg_3403" s="T185">u͡ol-BIt</ta>
            <ta e="T187" id="Seg_3404" s="T186">kirdik</ta>
            <ta e="T188" id="Seg_3405" s="T187">e-BIT</ta>
            <ta e="T189" id="Seg_3406" s="T188">kɨːs-BIt</ta>
            <ta e="T190" id="Seg_3407" s="T189">aga-tA</ta>
            <ta e="T191" id="Seg_3408" s="T190">tu͡ok-nI</ta>
            <ta e="T192" id="Seg_3409" s="T191">ere</ta>
            <ta e="T193" id="Seg_3410" s="T192">bɨrak-TI-tA</ta>
            <ta e="T194" id="Seg_3411" s="T193">eːt</ta>
            <ta e="T195" id="Seg_3412" s="T194">čɨk</ta>
            <ta e="T196" id="Seg_3413" s="T195">gɨn-An</ta>
            <ta e="T197" id="Seg_3414" s="T196">keːs-TI-tA</ta>
            <ta e="T198" id="Seg_3415" s="T197">bu</ta>
            <ta e="T199" id="Seg_3416" s="T198">ačaːk</ta>
            <ta e="T200" id="Seg_3417" s="T199">ürüt-tI-nAn</ta>
            <ta e="T201" id="Seg_3418" s="T200">di͡e-A</ta>
            <ta e="T202" id="Seg_3419" s="T201">hanaː-TI-tA</ta>
            <ta e="T203" id="Seg_3420" s="T202">onton</ta>
            <ta e="T204" id="Seg_3421" s="T203">tut-IAK.[tA]</ta>
            <ta e="T205" id="Seg_3422" s="T204">du͡o</ta>
            <ta e="T206" id="Seg_3423" s="T205">tut-IAK.[tA]</ta>
            <ta e="T207" id="Seg_3424" s="T206">du͡o</ta>
            <ta e="T208" id="Seg_3425" s="T207">hu͡ok-tA</ta>
            <ta e="T209" id="Seg_3426" s="T208">du͡o</ta>
            <ta e="T210" id="Seg_3427" s="T209">bu</ta>
            <ta e="T211" id="Seg_3428" s="T210">oŋu͡ok-nI</ta>
            <ta e="T212" id="Seg_3429" s="T211">ka</ta>
            <ta e="T213" id="Seg_3430" s="T212">u͡ol-BIt</ta>
            <ta e="T214" id="Seg_3431" s="T213">dʼe</ta>
            <ta e="T215" id="Seg_3432" s="T214">türgen</ta>
            <ta e="T216" id="Seg_3433" s="T215">bagajɨ-LIk</ta>
            <ta e="T217" id="Seg_3434" s="T216">tut-Ar</ta>
            <ta e="T218" id="Seg_3435" s="T217">diː</ta>
            <ta e="T219" id="Seg_3436" s="T218">ol-nI</ta>
            <ta e="T220" id="Seg_3437" s="T219">ɨjɨt-Ar</ta>
            <ta e="T221" id="Seg_3438" s="T220">kɨːs-BIt</ta>
            <ta e="T222" id="Seg_3439" s="T221">aga-tA</ta>
            <ta e="T223" id="Seg_3440" s="T222">dʼe</ta>
            <ta e="T224" id="Seg_3441" s="T223">tu͡ok-nI</ta>
            <ta e="T225" id="Seg_3442" s="T224">tut-TI-ŋ</ta>
            <ta e="T226" id="Seg_3443" s="T225">iti</ta>
            <ta e="T227" id="Seg_3444" s="T226">kajdak</ta>
            <ta e="T228" id="Seg_3445" s="T227">tu͡ok-nI</ta>
            <ta e="T229" id="Seg_3446" s="T228">di͡e-A</ta>
            <ta e="T230" id="Seg_3447" s="T229">ɨjɨt-TI-tA</ta>
            <ta e="T231" id="Seg_3448" s="T230">ol</ta>
            <ta e="T232" id="Seg_3449" s="T231">ka</ta>
            <ta e="T233" id="Seg_3450" s="T232">kim</ta>
            <ta e="T234" id="Seg_3451" s="T233">u͡ol-BIt</ta>
            <ta e="T235" id="Seg_3452" s="T234">min</ta>
            <ta e="T238" id="Seg_3453" s="T237">min</ta>
            <ta e="T239" id="Seg_3454" s="T238">kör-TI-m</ta>
            <ta e="T240" id="Seg_3455" s="T239">taːj-TI-m</ta>
            <ta e="T241" id="Seg_3456" s="T240">tobuk</ta>
            <ta e="T242" id="Seg_3457" s="T241">oŋu͡ok-tA</ta>
            <ta e="T243" id="Seg_3458" s="T242">kim</ta>
            <ta e="T244" id="Seg_3459" s="T243">taːj-BAT</ta>
            <ta e="T245" id="Seg_3460" s="T244">ol</ta>
            <ta e="T246" id="Seg_3461" s="T245">tobuk</ta>
            <ta e="T247" id="Seg_3462" s="T246">oŋu͡ok-tI-n</ta>
            <ta e="T248" id="Seg_3463" s="T247">kim</ta>
            <ta e="T249" id="Seg_3464" s="T248">taːj-I-m-IAK.[tA]=Ij</ta>
            <ta e="T250" id="Seg_3465" s="T249">ol</ta>
            <ta e="T251" id="Seg_3466" s="T250">tobuk</ta>
            <ta e="T252" id="Seg_3467" s="T251">oŋu͡ok-tI-n</ta>
            <ta e="T253" id="Seg_3468" s="T252">di͡e-TI-tA</ta>
            <ta e="T254" id="Seg_3469" s="T253">aga-BIt</ta>
            <ta e="T257" id="Seg_3470" s="T256">nʼɨma-ŋnAː-A-nʼɨma-ŋnAː-A</ta>
            <ta e="T258" id="Seg_3471" s="T257">ɨmaŋnaː-A-ɨmaŋnaː-A</ta>
            <ta e="T259" id="Seg_3472" s="T258">di͡e-TI-tA</ta>
            <ta e="T260" id="Seg_3473" s="T259">dʼe</ta>
            <ta e="T261" id="Seg_3474" s="T260">taːj-TI-ŋ</ta>
            <ta e="T262" id="Seg_3475" s="T261">tobuk</ta>
            <ta e="T263" id="Seg_3476" s="T262">oŋu͡ok-tI-n</ta>
            <ta e="T264" id="Seg_3477" s="T263">üčügej</ta>
            <ta e="T265" id="Seg_3478" s="T264">dʼe</ta>
            <ta e="T266" id="Seg_3479" s="T265">bu</ta>
            <ta e="T269" id="Seg_3480" s="T268">tobuk</ta>
            <ta e="T270" id="Seg_3481" s="T269">oŋu͡ok-tI-GAr</ta>
            <ta e="T271" id="Seg_3482" s="T270">kantan</ta>
            <ta e="T272" id="Seg_3483" s="T271">eme</ta>
            <ta e="T275" id="Seg_3484" s="T274">kör-An-GIn</ta>
            <ta e="T276" id="Seg_3485" s="T275">biːr</ta>
            <ta e="T277" id="Seg_3486" s="T276">eme</ta>
            <ta e="T278" id="Seg_3487" s="T277">iŋiːr-nI</ta>
            <ta e="T279" id="Seg_3488" s="T278">bul-IAK-ŋ</ta>
            <ta e="T280" id="Seg_3489" s="T279">dʼürü</ta>
            <ta e="T281" id="Seg_3490" s="T280">oččogo</ta>
            <ta e="T282" id="Seg_3491" s="T281">min</ta>
            <ta e="T283" id="Seg_3492" s="T282">haŋa-BI-n</ta>
            <ta e="T284" id="Seg_3493" s="T283">ɨl-IAK-m</ta>
            <ta e="T285" id="Seg_3494" s="T284">hu͡ok-tA</ta>
            <ta e="T286" id="Seg_3495" s="T285">töttörü</ta>
            <ta e="T287" id="Seg_3496" s="T286">bul-TAK-GInA</ta>
            <ta e="T288" id="Seg_3497" s="T287">biːr</ta>
            <ta e="T289" id="Seg_3498" s="T288">eme</ta>
            <ta e="T290" id="Seg_3499" s="T289">iŋiːr-nI</ta>
            <ta e="T291" id="Seg_3500" s="T290">oččogo</ta>
            <ta e="T292" id="Seg_3501" s="T291">min</ta>
            <ta e="T293" id="Seg_3502" s="T292">kɨːs-BI-n</ta>
            <ta e="T294" id="Seg_3503" s="T293">en-GA</ta>
            <ta e="T295" id="Seg_3504" s="T294">bi͡er-IAK-m</ta>
            <ta e="T296" id="Seg_3505" s="T295">dʼe</ta>
            <ta e="T297" id="Seg_3506" s="T296">u͡ol-BIt</ta>
            <ta e="T298" id="Seg_3507" s="T297">kör-A</ta>
            <ta e="T299" id="Seg_3508" s="T298">hataː-TI-tA</ta>
            <ta e="T300" id="Seg_3509" s="T299">kör-A</ta>
            <ta e="T301" id="Seg_3510" s="T300">hataː-TI-tA</ta>
            <ta e="T302" id="Seg_3511" s="T301">onton</ta>
            <ta e="T303" id="Seg_3512" s="T302">orgujakaːn-LIk</ta>
            <ta e="T304" id="Seg_3513" s="T303">beje-tA</ta>
            <ta e="T305" id="Seg_3514" s="T304">beje-tI-GAr</ta>
            <ta e="T306" id="Seg_3515" s="T305">haŋar-Ar</ta>
            <ta e="T307" id="Seg_3516" s="T306">o</ta>
            <ta e="T308" id="Seg_3517" s="T307">dʼe</ta>
            <ta e="T309" id="Seg_3518" s="T308">hürdeːk</ta>
            <ta e="T310" id="Seg_3519" s="T309">e-BIT</ta>
            <ta e="T311" id="Seg_3520" s="T310">dʼe</ta>
            <ta e="T312" id="Seg_3521" s="T311">ile</ta>
            <ta e="T313" id="Seg_3522" s="T312">taːs-GA</ta>
            <ta e="T314" id="Seg_3523" s="T313">talɨ</ta>
            <ta e="T315" id="Seg_3524" s="T314">tu͡ok</ta>
            <ta e="T316" id="Seg_3525" s="T315">bosku͡oj</ta>
            <ta e="T317" id="Seg_3526" s="T316">oŋu͡ok-tA=Ij</ta>
            <ta e="T318" id="Seg_3527" s="T317">ogo-LAr</ta>
            <ta e="T319" id="Seg_3528" s="T318">ol-tI-ŋ</ta>
            <ta e="T320" id="Seg_3529" s="T319">kaja</ta>
            <ta e="T321" id="Seg_3530" s="T320">bil-BAT</ta>
            <ta e="T322" id="Seg_3531" s="T321">e-BIT</ta>
            <ta e="T323" id="Seg_3532" s="T322">gini</ta>
            <ta e="T324" id="Seg_3533" s="T323">gini</ta>
            <ta e="T325" id="Seg_3534" s="T324">iti</ta>
            <ta e="T326" id="Seg_3535" s="T325">aga-LArA</ta>
            <ta e="T327" id="Seg_3536" s="T326">ere</ta>
            <ta e="T328" id="Seg_3537" s="T327">iti</ta>
            <ta e="T329" id="Seg_3538" s="T328">bil-Ar</ta>
            <ta e="T330" id="Seg_3539" s="T329">biːr</ta>
            <ta e="T331" id="Seg_3540" s="T330">hir-tA</ta>
            <ta e="T332" id="Seg_3541" s="T331">ol-tI-ŋ</ta>
            <ta e="T333" id="Seg_3542" s="T332">amattan</ta>
            <ta e="T334" id="Seg_3543" s="T333">biːr</ta>
            <ta e="T335" id="Seg_3544" s="T334">da</ta>
            <ta e="T336" id="Seg_3545" s="T335">iŋiːr-tA</ta>
            <ta e="T337" id="Seg_3546" s="T336">hu͡ok</ta>
            <ta e="T338" id="Seg_3547" s="T337">biːr-GA-r</ta>
            <ta e="T339" id="Seg_3548" s="T338">hir-tI-n</ta>
            <ta e="T340" id="Seg_3549" s="T339">kör-IAK-ŋ</ta>
            <ta e="T341" id="Seg_3550" s="T340">üčügej-kAːN-LIk</ta>
            <ta e="T342" id="Seg_3551" s="T341">kör-TAK-GA</ta>
            <ta e="T343" id="Seg_3552" s="T342">ol-tI-ŋ</ta>
            <ta e="T344" id="Seg_3553" s="T343">kɨːs</ta>
            <ta e="T347" id="Seg_3554" s="T346">hɨraj-tI-GAr</ta>
            <ta e="T348" id="Seg_3555" s="T347">talɨ</ta>
            <ta e="T349" id="Seg_3556" s="T348">dʼaktar</ta>
            <ta e="T350" id="Seg_3557" s="T349">hɨraj-tI-GAr</ta>
            <ta e="T351" id="Seg_3558" s="T350">talɨ</ta>
            <ta e="T352" id="Seg_3559" s="T351">biːrges</ta>
            <ta e="T353" id="Seg_3560" s="T352">örüt-tI-GA</ta>
            <ta e="T354" id="Seg_3561" s="T353">ol-tI-GI-n</ta>
            <ta e="T355" id="Seg_3562" s="T354">aga-BIt</ta>
            <ta e="T356" id="Seg_3563" s="T355">ere</ta>
            <ta e="T357" id="Seg_3564" s="T356">bil-Ar</ta>
            <ta e="T358" id="Seg_3565" s="T357">bu͡o</ta>
            <ta e="T359" id="Seg_3566" s="T358">dʼe</ta>
            <ta e="T360" id="Seg_3567" s="T359">kajdi͡ek</ta>
            <ta e="T361" id="Seg_3568" s="T360">dʼe</ta>
            <ta e="T362" id="Seg_3569" s="T361">bu͡ol-IAK-tI-n</ta>
            <ta e="T363" id="Seg_3570" s="T362">bert</ta>
            <ta e="T364" id="Seg_3571" s="T363">Bulčut</ta>
            <ta e="T365" id="Seg_3572" s="T364">u͡ol-BIt</ta>
            <ta e="T366" id="Seg_3573" s="T365">kɨjgan-AːrI</ta>
            <ta e="T367" id="Seg_3574" s="T366">da</ta>
            <ta e="T368" id="Seg_3575" s="T367">gɨn-Ar</ta>
            <ta e="T369" id="Seg_3576" s="T368">kečes-Ar</ta>
            <ta e="T370" id="Seg_3577" s="T369">onton</ta>
            <ta e="T371" id="Seg_3578" s="T370">dʼe</ta>
            <ta e="T372" id="Seg_3579" s="T371">kɨlaj-s</ta>
            <ta e="T373" id="Seg_3580" s="T372">gɨn-Ar</ta>
            <ta e="T374" id="Seg_3581" s="T373">diː</ta>
            <ta e="T375" id="Seg_3582" s="T374">kɨːs-tI-n</ta>
            <ta e="T376" id="Seg_3583" s="T375">dek</ta>
            <ta e="T377" id="Seg_3584" s="T376">kɨːs-tA</ta>
            <ta e="T378" id="Seg_3585" s="T377">nʼɨma-LAN-TI-tA</ta>
            <ta e="T379" id="Seg_3586" s="T378">emi͡e</ta>
            <ta e="T380" id="Seg_3587" s="T379">kajdak</ta>
            <ta e="T381" id="Seg_3588" s="T380">gɨn-IAK-m=Ij</ta>
            <ta e="T382" id="Seg_3589" s="T381">di͡e-An</ta>
            <ta e="T383" id="Seg_3590" s="T382">baran</ta>
            <ta e="T384" id="Seg_3591" s="T383">kumaːr-LAr-ttAn</ta>
            <ta e="T385" id="Seg_3592" s="T384">ilbin-Ar</ta>
            <ta e="T386" id="Seg_3593" s="T385">kördük</ta>
            <ta e="T387" id="Seg_3594" s="T386">iliː-tI-n</ta>
            <ta e="T388" id="Seg_3595" s="T387">orgujakaːn-LIk</ta>
            <ta e="T389" id="Seg_3596" s="T388">bugurduk-kAːN</ta>
            <ta e="T390" id="Seg_3597" s="T389">ɨl-TI-tA</ta>
            <ta e="T391" id="Seg_3598" s="T390">onton</ta>
            <ta e="T392" id="Seg_3599" s="T391">bu</ta>
            <ta e="T393" id="Seg_3600" s="T392">orgujakaːn-LIk</ta>
            <ta e="T394" id="Seg_3601" s="T393">bu</ta>
            <ta e="T395" id="Seg_3602" s="T394">bugurduk-kAːN</ta>
            <ta e="T396" id="Seg_3603" s="T395">hɨraj-tI-nAn</ta>
            <ta e="T397" id="Seg_3604" s="T396">hɨraj-tI-n</ta>
            <ta e="T398" id="Seg_3605" s="T397">dʼe</ta>
            <ta e="T399" id="Seg_3606" s="T398">kim-LAː-Ar</ta>
            <ta e="T400" id="Seg_3607" s="T399">köllör-Ar</ta>
            <ta e="T401" id="Seg_3608" s="T400">kumaːr-LAr-ttAn</ta>
            <ta e="T402" id="Seg_3609" s="T401">tep-A-n-Ar-GA</ta>
            <ta e="T403" id="Seg_3610" s="T402">talɨ</ta>
            <ta e="T404" id="Seg_3611" s="T403">oː</ta>
            <ta e="T405" id="Seg_3612" s="T404">dʼe</ta>
            <ta e="T406" id="Seg_3613" s="T405">u͡ol-BIt</ta>
            <ta e="T407" id="Seg_3614" s="T406">dʼe</ta>
            <ta e="T408" id="Seg_3615" s="T407">ü͡ör-Ar</ta>
            <ta e="T409" id="Seg_3616" s="T408">diː</ta>
            <ta e="T410" id="Seg_3617" s="T409">türgen</ta>
            <ta e="T411" id="Seg_3618" s="T410">bagajɨ-LIk</ta>
            <ta e="T412" id="Seg_3619" s="T411">bahak-tI-n</ta>
            <ta e="T413" id="Seg_3620" s="T412">ɨl-TI-tA</ta>
            <ta e="T414" id="Seg_3621" s="T413">onton</ta>
            <ta e="T415" id="Seg_3622" s="T414">orgujakaːn-LIk</ta>
            <ta e="T416" id="Seg_3623" s="T415">kötök-TI-tA</ta>
            <ta e="T417" id="Seg_3624" s="T416">tobuk</ta>
            <ta e="T418" id="Seg_3625" s="T417">oŋu͡ok-tI-n</ta>
            <ta e="T419" id="Seg_3626" s="T418">bɨ͡aj</ta>
            <ta e="T420" id="Seg_3627" s="T419">türgen</ta>
            <ta e="T421" id="Seg_3628" s="T420">türgen</ta>
            <ta e="T422" id="Seg_3629" s="T421">bagajɨ-LIk</ta>
            <ta e="T423" id="Seg_3630" s="T422">bahak-tI-nAn</ta>
            <ta e="T424" id="Seg_3631" s="T423">iŋiːr</ta>
            <ta e="T425" id="Seg_3632" s="T424">munʼaː-nI</ta>
            <ta e="T426" id="Seg_3633" s="T425">tart-An</ta>
            <ta e="T427" id="Seg_3634" s="T426">ɨl-BAT</ta>
            <ta e="T428" id="Seg_3635" s="T427">du͡o</ta>
            <ta e="T429" id="Seg_3636" s="T428">onno</ta>
            <ta e="T430" id="Seg_3637" s="T429">aga-tA</ta>
            <ta e="T431" id="Seg_3638" s="T430">di͡e-TI-tA</ta>
            <ta e="T432" id="Seg_3639" s="T431">dʼe</ta>
            <ta e="T433" id="Seg_3640" s="T432">kajdak</ta>
            <ta e="T434" id="Seg_3641" s="T433">gɨn-IAK-ŋ=Ij</ta>
            <ta e="T435" id="Seg_3642" s="T434">min</ta>
            <ta e="T436" id="Seg_3643" s="T435">di͡e-BIT-I-m</ta>
            <ta e="T437" id="Seg_3644" s="T436">tɨl-BI-n</ta>
            <ta e="T438" id="Seg_3645" s="T437">töttörü</ta>
            <ta e="T439" id="Seg_3646" s="T438">ɨl-IAK-m</ta>
            <ta e="T440" id="Seg_3647" s="T439">hu͡ok-tA</ta>
            <ta e="T441" id="Seg_3648" s="T440">itinnik</ta>
            <ta e="T442" id="Seg_3649" s="T441">uːs</ta>
            <ta e="T443" id="Seg_3650" s="T442">kihi</ta>
            <ta e="T444" id="Seg_3651" s="T443">itinnik</ta>
            <ta e="T445" id="Seg_3652" s="T444">bulčut</ta>
            <ta e="T446" id="Seg_3653" s="T445">kihi</ta>
            <ta e="T447" id="Seg_3654" s="T446">barɨ-tI-n</ta>
            <ta e="T448" id="Seg_3655" s="T447">bil-Ar</ta>
            <ta e="T449" id="Seg_3656" s="T448">dʼe</ta>
            <ta e="T450" id="Seg_3657" s="T449">bi͡er-A-BIn</ta>
            <ta e="T451" id="Seg_3658" s="T450">en-GA</ta>
            <ta e="T452" id="Seg_3659" s="T451">Bulčut</ta>
            <ta e="T453" id="Seg_3660" s="T452">kɨːs-BI-n</ta>
            <ta e="T454" id="Seg_3661" s="T453">eteŋŋe-LIk</ta>
            <ta e="T455" id="Seg_3662" s="T454">olor-I-ŋ</ta>
            <ta e="T456" id="Seg_3663" s="T455">üčügej-LIk</ta>
            <ta e="T457" id="Seg_3664" s="T456">olor-I-ŋ</ta>
            <ta e="T458" id="Seg_3665" s="T457">ügüs</ta>
            <ta e="T459" id="Seg_3666" s="T458">ogo-nI</ta>
            <ta e="T460" id="Seg_3667" s="T459">ogo-LAN-I-ŋ</ta>
            <ta e="T461" id="Seg_3668" s="T460">di͡e-TI-tA</ta>
            <ta e="T462" id="Seg_3669" s="T461">dʼe</ta>
            <ta e="T463" id="Seg_3670" s="T462">ol</ta>
            <ta e="T464" id="Seg_3671" s="T463">kördük-kAːN</ta>
            <ta e="T465" id="Seg_3672" s="T464">Bulčut</ta>
            <ta e="T466" id="Seg_3673" s="T465">kɨːs-tI-n</ta>
            <ta e="T467" id="Seg_3674" s="T466">kɨtta</ta>
            <ta e="T468" id="Seg_3675" s="T467">üčügej</ta>
            <ta e="T469" id="Seg_3676" s="T468">bagajɨ-LIk</ta>
            <ta e="T470" id="Seg_3677" s="T469">olor-BIT-LArA</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_3678" s="T0">1SG.[NOM]</ta>
            <ta e="T2" id="Seg_3679" s="T1">this</ta>
            <ta e="T3" id="Seg_3680" s="T2">story-ACC</ta>
            <ta e="T4" id="Seg_3681" s="T3">hear-EP-PST2-EP-1SG</ta>
            <ta e="T5" id="Seg_3682" s="T4">parents-PL-1SG-ACC</ta>
            <ta e="T7" id="Seg_3683" s="T5">with</ta>
            <ta e="T8" id="Seg_3684" s="T7">school-DAT/LOC</ta>
            <ta e="T9" id="Seg_3685" s="T8">go.in-PTCP.PRS</ta>
            <ta e="T10" id="Seg_3686" s="T9">year-1SG-DAT/LOC</ta>
            <ta e="T11" id="Seg_3687" s="T10">that-3SG-2SG.[NOM]</ta>
            <ta e="T12" id="Seg_3688" s="T11">now-DAT/LOC</ta>
            <ta e="T13" id="Seg_3689" s="T12">until</ta>
            <ta e="T14" id="Seg_3690" s="T13">memory-1SG-DAT/LOC</ta>
            <ta e="T15" id="Seg_3691" s="T14">there.is</ta>
            <ta e="T16" id="Seg_3692" s="T15">like.this-INTNS</ta>
            <ta e="T17" id="Seg_3693" s="T16">mum-1SG.[NOM]</ta>
            <ta e="T18" id="Seg_3694" s="T17">older.sister-3SG.[NOM]</ta>
            <ta e="T19" id="Seg_3695" s="T18">Claudia.[NOM]</ta>
            <ta e="T20" id="Seg_3696" s="T19">tell-PTCP.HAB</ta>
            <ta e="T21" id="Seg_3697" s="T20">be-PST1-3SG</ta>
            <ta e="T22" id="Seg_3698" s="T21">distant-distant</ta>
            <ta e="T23" id="Seg_3699" s="T22">tundra-DAT/LOC</ta>
            <ta e="T24" id="Seg_3700" s="T23">be.born-PTCP.PST</ta>
            <ta e="T25" id="Seg_3701" s="T24">place-3SG-DAT/LOC</ta>
            <ta e="T26" id="Seg_3702" s="T25">self-3SG-GEN</ta>
            <ta e="T27" id="Seg_3703" s="T26">be.born-PTCP.PST</ta>
            <ta e="T28" id="Seg_3704" s="T27">place-3SG-DAT/LOC</ta>
            <ta e="T29" id="Seg_3705" s="T28">family-PL-3SG-ACC</ta>
            <ta e="T30" id="Seg_3706" s="T29">with</ta>
            <ta e="T31" id="Seg_3707" s="T30">travel-CVB.SIM</ta>
            <ta e="T32" id="Seg_3708" s="T31">go-EP-PST2-3SG</ta>
            <ta e="T33" id="Seg_3709" s="T32">beautiful</ta>
            <ta e="T34" id="Seg_3710" s="T33">very</ta>
            <ta e="T35" id="Seg_3711" s="T34">boy.[NOM]</ta>
            <ta e="T36" id="Seg_3712" s="T35">that-3SG-2SG.[NOM]</ta>
            <ta e="T37" id="Seg_3713" s="T36">name-3SG.[NOM]</ta>
            <ta e="T38" id="Seg_3714" s="T37">Hunter.[NOM]</ta>
            <ta e="T39" id="Seg_3715" s="T38">be-PST1-3SG</ta>
            <ta e="T40" id="Seg_3716" s="T39">different</ta>
            <ta e="T41" id="Seg_3717" s="T40">place-DAT/LOC</ta>
            <ta e="T44" id="Seg_3718" s="T43">3PL-ABL</ta>
            <ta e="T45" id="Seg_3719" s="T44">far.away-INTNS</ta>
            <ta e="T46" id="Seg_3720" s="T45">hide-CVB.SIM</ta>
            <ta e="T47" id="Seg_3721" s="T46">go-EP-PST2-3SG</ta>
            <ta e="T48" id="Seg_3722" s="T47">beautiful</ta>
            <ta e="T49" id="Seg_3723" s="T48">girl.[NOM]</ta>
            <ta e="T50" id="Seg_3724" s="T49">family-PL-3SG-ACC</ta>
            <ta e="T51" id="Seg_3725" s="T50">with</ta>
            <ta e="T52" id="Seg_3726" s="T51">that.[NOM]</ta>
            <ta e="T53" id="Seg_3727" s="T52">young</ta>
            <ta e="T54" id="Seg_3728" s="T53">eh</ta>
            <ta e="T55" id="Seg_3729" s="T54">boy-ACC</ta>
            <ta e="T56" id="Seg_3730" s="T55">with</ta>
            <ta e="T57" id="Seg_3731" s="T56">that.[NOM]</ta>
            <ta e="T58" id="Seg_3732" s="T57">girl.[NOM]</ta>
            <ta e="T59" id="Seg_3733" s="T58">meet-PTCP.HAB</ta>
            <ta e="T60" id="Seg_3734" s="T59">be-PST2-3PL</ta>
            <ta e="T61" id="Seg_3735" s="T60">love-RECP/COLL-PTCP.HAB</ta>
            <ta e="T62" id="Seg_3736" s="T61">be-PST2-3PL</ta>
            <ta e="T63" id="Seg_3737" s="T62">self-3PL-GEN</ta>
            <ta e="T64" id="Seg_3738" s="T63">be.born-PTCP.PST</ta>
            <ta e="T65" id="Seg_3739" s="T64">earth-3PL-DAT/LOC</ta>
            <ta e="T66" id="Seg_3740" s="T65">once</ta>
            <ta e="T67" id="Seg_3741" s="T66">girl-3SG.[NOM]</ta>
            <ta e="T68" id="Seg_3742" s="T67">Hunter-3SG-ACC</ta>
            <ta e="T69" id="Seg_3743" s="T68">boy-3SG-ACC</ta>
            <ta e="T70" id="Seg_3744" s="T69">speak-PRS.[3SG]</ta>
            <ta e="T71" id="Seg_3745" s="T70">how.long</ta>
            <ta e="T72" id="Seg_3746" s="T71">1PL.[NOM]</ta>
            <ta e="T73" id="Seg_3747" s="T72">parents-PL-1PL-ABL</ta>
            <ta e="T74" id="Seg_3748" s="T73">hide-CVB.SIM</ta>
            <ta e="T75" id="Seg_3749" s="T74">go-FUT-1PL=Q</ta>
            <ta e="T76" id="Seg_3750" s="T75">come.[IMP.2SG]</ta>
            <ta e="T77" id="Seg_3751" s="T76">1PL-DAT/LOC</ta>
            <ta e="T78" id="Seg_3752" s="T77">visit-PTCP.PST</ta>
            <ta e="T79" id="Seg_3753" s="T78">be.[IMP.2SG]</ta>
            <ta e="T80" id="Seg_3754" s="T79">parents-PL-1SG-ACC</ta>
            <ta e="T81" id="Seg_3755" s="T80">with</ta>
            <ta e="T82" id="Seg_3756" s="T81">speak-EP-RECP/COLL.[IMP.2SG]</ta>
            <ta e="T83" id="Seg_3757" s="T82">father-1SG-ACC</ta>
            <ta e="T84" id="Seg_3758" s="T83">with</ta>
            <ta e="T85" id="Seg_3759" s="T84">speak-EP-RECP/COLL.[IMP.2SG]</ta>
            <ta e="T86" id="Seg_3760" s="T85">father-1SG-ABL</ta>
            <ta e="T87" id="Seg_3761" s="T86">beg.[IMP.2SG]</ta>
            <ta e="T88" id="Seg_3762" s="T87">1SG-ACC</ta>
            <ta e="T89" id="Seg_3763" s="T88">father-1SG.[NOM]</ta>
            <ta e="T93" id="Seg_3764" s="T92">good</ta>
            <ta e="T94" id="Seg_3765" s="T93">very</ta>
            <ta e="T95" id="Seg_3766" s="T94">human.being.[NOM]</ta>
            <ta e="T96" id="Seg_3767" s="T95">and</ta>
            <ta e="T97" id="Seg_3768" s="T96">be-COND.[3SG]</ta>
            <ta e="T98" id="Seg_3769" s="T97">slyness-PROPR.[NOM]</ta>
            <ta e="T99" id="Seg_3770" s="T98">well</ta>
            <ta e="T100" id="Seg_3771" s="T99">that</ta>
            <ta e="T101" id="Seg_3772" s="T100">slyness-3SG-ABL</ta>
            <ta e="T102" id="Seg_3773" s="T101">2SG.[NOM]</ta>
            <ta e="T103" id="Seg_3774" s="T102">fear-EP-NEG.[IMP.2SG]</ta>
            <ta e="T104" id="Seg_3775" s="T103">well</ta>
            <ta e="T105" id="Seg_3776" s="T104">that.[NOM]</ta>
            <ta e="T106" id="Seg_3777" s="T105">like-INTNS</ta>
            <ta e="T107" id="Seg_3778" s="T106">speak-PST1-3SG</ta>
            <ta e="T108" id="Seg_3779" s="T107">and</ta>
            <ta e="T109" id="Seg_3780" s="T108">boy-1PL.[NOM]</ta>
            <ta e="T110" id="Seg_3781" s="T109">that.[NOM]</ta>
            <ta e="T111" id="Seg_3782" s="T110">like-INTNS</ta>
            <ta e="T112" id="Seg_3783" s="T111">how.much</ta>
            <ta e="T113" id="Seg_3784" s="T112">INDEF</ta>
            <ta e="T114" id="Seg_3785" s="T113">day.[NOM]</ta>
            <ta e="T115" id="Seg_3786" s="T114">be-CVB.SEQ</ta>
            <ta e="T116" id="Seg_3787" s="T115">after</ta>
            <ta e="T117" id="Seg_3788" s="T116">warm</ta>
            <ta e="T118" id="Seg_3789" s="T117">very</ta>
            <ta e="T119" id="Seg_3790" s="T118">day-DAT/LOC</ta>
            <ta e="T120" id="Seg_3791" s="T119">in.spring</ta>
            <ta e="T121" id="Seg_3792" s="T120">towards.autumn</ta>
            <ta e="T122" id="Seg_3793" s="T121">sun-PROPR</ta>
            <ta e="T123" id="Seg_3794" s="T122">very</ta>
            <ta e="T124" id="Seg_3795" s="T123">day-DAT/LOC</ta>
            <ta e="T125" id="Seg_3796" s="T124">well</ta>
            <ta e="T126" id="Seg_3797" s="T125">riding.reindeer-VBZ-CVB.SEQ</ta>
            <ta e="T127" id="Seg_3798" s="T126">go-CVB.SEQ</ta>
            <ta e="T128" id="Seg_3799" s="T127">well</ta>
            <ta e="T129" id="Seg_3800" s="T128">come-PRS.[3SG]</ta>
            <ta e="T130" id="Seg_3801" s="T129">self-3SG-GEN</ta>
            <ta e="T133" id="Seg_3802" s="T132">white</ta>
            <ta e="T134" id="Seg_3803" s="T133">tamed.reindeer-PROPR.[NOM]</ta>
            <ta e="T135" id="Seg_3804" s="T134">that-3SG-3SG-INSTR</ta>
            <ta e="T136" id="Seg_3805" s="T135">visit-CVB.SIM</ta>
            <ta e="T137" id="Seg_3806" s="T136">go-PST1-3SG</ta>
            <ta e="T138" id="Seg_3807" s="T137">girl-3SG-DAT/LOC</ta>
            <ta e="T139" id="Seg_3808" s="T138">go.in-CVB.ANT-3SG</ta>
            <ta e="T140" id="Seg_3809" s="T139">earth-DAT/LOC</ta>
            <ta e="T141" id="Seg_3810" s="T140">until</ta>
            <ta e="T142" id="Seg_3811" s="T141">head-3SG-ACC</ta>
            <ta e="T143" id="Seg_3812" s="T142">parents-PL-3SG-DAT/LOC</ta>
            <ta e="T144" id="Seg_3813" s="T143">bow-ITER-CVB.SEQ</ta>
            <ta e="T145" id="Seg_3814" s="T144">after</ta>
            <ta e="T146" id="Seg_3815" s="T145">hello-IMP.2PL</ta>
            <ta e="T147" id="Seg_3816" s="T146">hello-IMP.2PL</ta>
            <ta e="T148" id="Seg_3817" s="T147">old-PL.[NOM]</ta>
            <ta e="T149" id="Seg_3818" s="T148">say-PST1-3SG</ta>
            <ta e="T150" id="Seg_3819" s="T149">Hunter.[NOM]</ta>
            <ta e="T151" id="Seg_3820" s="T150">boy-1PL.[NOM]</ta>
            <ta e="T152" id="Seg_3821" s="T151">well</ta>
            <ta e="T153" id="Seg_3822" s="T152">this</ta>
            <ta e="T154" id="Seg_3823" s="T153">come-PST1-1SG</ta>
            <ta e="T155" id="Seg_3824" s="T154">2PL-DAT/LOC</ta>
            <ta e="T157" id="Seg_3825" s="T156">mother.[NOM]</ta>
            <ta e="T158" id="Seg_3826" s="T157">father.[NOM]</ta>
            <ta e="T159" id="Seg_3827" s="T158">2PL-ABL</ta>
            <ta e="T160" id="Seg_3828" s="T159">daughter-2PL-ACC</ta>
            <ta e="T161" id="Seg_3829" s="T160">beg-PRS-1SG</ta>
            <ta e="T162" id="Seg_3830" s="T161">what-3SG-ACC</ta>
            <ta e="T163" id="Seg_3831" s="T162">how.long</ta>
            <ta e="T164" id="Seg_3832" s="T163">hide-FUT-1PL=Q</ta>
            <ta e="T165" id="Seg_3833" s="T164">1PL.[NOM]</ta>
            <ta e="T166" id="Seg_3834" s="T165">1PL.[NOM]</ta>
            <ta e="T167" id="Seg_3835" s="T166">long.ago-ADJZ-ABL</ta>
            <ta e="T168" id="Seg_3836" s="T167">love-RECP/COLL-PRS-1PL</ta>
            <ta e="T169" id="Seg_3837" s="T168">self-self-1PL-ACC</ta>
            <ta e="T170" id="Seg_3838" s="T169">very</ta>
            <ta e="T171" id="Seg_3839" s="T170">esteem-PRS-1PL</ta>
            <ta e="T172" id="Seg_3840" s="T171">well</ta>
            <ta e="T173" id="Seg_3841" s="T172">how</ta>
            <ta e="T174" id="Seg_3842" s="T173">make-FUT-2SG=Q</ta>
            <ta e="T175" id="Seg_3843" s="T174">say-PST1-3SG</ta>
            <ta e="T176" id="Seg_3844" s="T175">shimmer-CVB.SIM-shimmer-CVB.SIM</ta>
            <ta e="T177" id="Seg_3845" s="T176">girl.[NOM]</ta>
            <ta e="T178" id="Seg_3846" s="T177">father-3SG.[NOM]</ta>
            <ta e="T179" id="Seg_3847" s="T178">then</ta>
            <ta e="T180" id="Seg_3848" s="T179">well</ta>
            <ta e="T181" id="Seg_3849" s="T180">slyness-REFL/MED-CVB.PURP</ta>
            <ta e="T182" id="Seg_3850" s="T181">want-PST1-3SG</ta>
            <ta e="T183" id="Seg_3851" s="T182">apparently</ta>
            <ta e="T184" id="Seg_3852" s="T183">think-CVB.SIM</ta>
            <ta e="T185" id="Seg_3853" s="T184">think-PST1-3SG</ta>
            <ta e="T186" id="Seg_3854" s="T185">boy-1PL.[NOM]</ta>
            <ta e="T187" id="Seg_3855" s="T186">truth.[NOM]</ta>
            <ta e="T188" id="Seg_3856" s="T187">be-PST2.[3SG]</ta>
            <ta e="T189" id="Seg_3857" s="T188">girl-1PL.[NOM]</ta>
            <ta e="T190" id="Seg_3858" s="T189">father-3SG.[NOM]</ta>
            <ta e="T191" id="Seg_3859" s="T190">what-ACC</ta>
            <ta e="T192" id="Seg_3860" s="T191">just</ta>
            <ta e="T193" id="Seg_3861" s="T192">throw-PST1-3SG</ta>
            <ta e="T194" id="Seg_3862" s="T193">EVID</ta>
            <ta e="T195" id="Seg_3863" s="T194">chik</ta>
            <ta e="T196" id="Seg_3864" s="T195">make-CVB.SEQ</ta>
            <ta e="T197" id="Seg_3865" s="T196">throw-PST1-3SG</ta>
            <ta e="T198" id="Seg_3866" s="T197">this</ta>
            <ta e="T199" id="Seg_3867" s="T198">stove.[NOM]</ta>
            <ta e="T200" id="Seg_3868" s="T199">upper.part-3SG-INSTR</ta>
            <ta e="T201" id="Seg_3869" s="T200">think-CVB.SIM</ta>
            <ta e="T202" id="Seg_3870" s="T201">think-PST1-3SG</ta>
            <ta e="T203" id="Seg_3871" s="T202">then</ta>
            <ta e="T204" id="Seg_3872" s="T203">hold-FUT.[3SG]</ta>
            <ta e="T205" id="Seg_3873" s="T204">Q</ta>
            <ta e="T206" id="Seg_3874" s="T205">hold-FUT.[3SG]</ta>
            <ta e="T207" id="Seg_3875" s="T206">Q</ta>
            <ta e="T208" id="Seg_3876" s="T207">NEG-3SG</ta>
            <ta e="T209" id="Seg_3877" s="T208">Q</ta>
            <ta e="T210" id="Seg_3878" s="T209">this</ta>
            <ta e="T211" id="Seg_3879" s="T210">bone-ACC</ta>
            <ta e="T212" id="Seg_3880" s="T211">well</ta>
            <ta e="T213" id="Seg_3881" s="T212">boy-1PL.[NOM]</ta>
            <ta e="T214" id="Seg_3882" s="T213">well</ta>
            <ta e="T215" id="Seg_3883" s="T214">fast</ta>
            <ta e="T216" id="Seg_3884" s="T215">very-ADVZ</ta>
            <ta e="T217" id="Seg_3885" s="T216">grab-PRS.[3SG]</ta>
            <ta e="T218" id="Seg_3886" s="T217">EMPH</ta>
            <ta e="T219" id="Seg_3887" s="T218">that-ACC</ta>
            <ta e="T220" id="Seg_3888" s="T219">ask-PRS.[3SG]</ta>
            <ta e="T221" id="Seg_3889" s="T220">girl-1PL.[NOM]</ta>
            <ta e="T222" id="Seg_3890" s="T221">father-3SG.[NOM]</ta>
            <ta e="T223" id="Seg_3891" s="T222">well</ta>
            <ta e="T224" id="Seg_3892" s="T223">what-ACC</ta>
            <ta e="T225" id="Seg_3893" s="T224">grab-PST1-2SG</ta>
            <ta e="T226" id="Seg_3894" s="T225">that</ta>
            <ta e="T227" id="Seg_3895" s="T226">how</ta>
            <ta e="T228" id="Seg_3896" s="T227">what-ACC</ta>
            <ta e="T229" id="Seg_3897" s="T228">say-CVB.SIM</ta>
            <ta e="T230" id="Seg_3898" s="T229">ask-PST1-3SG</ta>
            <ta e="T231" id="Seg_3899" s="T230">that</ta>
            <ta e="T232" id="Seg_3900" s="T231">well</ta>
            <ta e="T233" id="Seg_3901" s="T232">who.[NOM]</ta>
            <ta e="T234" id="Seg_3902" s="T233">boy-1PL.[NOM]</ta>
            <ta e="T235" id="Seg_3903" s="T234">1SG.[NOM]</ta>
            <ta e="T238" id="Seg_3904" s="T237">1SG.[NOM]</ta>
            <ta e="T239" id="Seg_3905" s="T238">see-PST1-1SG</ta>
            <ta e="T240" id="Seg_3906" s="T239">realize-PST1-1SG</ta>
            <ta e="T241" id="Seg_3907" s="T240">knee.[NOM]</ta>
            <ta e="T242" id="Seg_3908" s="T241">bone-3SG.[NOM]</ta>
            <ta e="T243" id="Seg_3909" s="T242">who.[NOM]</ta>
            <ta e="T244" id="Seg_3910" s="T243">realize-NEG.[3SG]</ta>
            <ta e="T245" id="Seg_3911" s="T244">that</ta>
            <ta e="T246" id="Seg_3912" s="T245">knee.[NOM]</ta>
            <ta e="T247" id="Seg_3913" s="T246">bone-3SG-ACC</ta>
            <ta e="T248" id="Seg_3914" s="T247">who.[NOM]</ta>
            <ta e="T249" id="Seg_3915" s="T248">realize-EP-NEG-FUT.[3SG]=Q</ta>
            <ta e="T250" id="Seg_3916" s="T249">that</ta>
            <ta e="T251" id="Seg_3917" s="T250">knee.[NOM]</ta>
            <ta e="T252" id="Seg_3918" s="T251">bone-3SG-ACC</ta>
            <ta e="T253" id="Seg_3919" s="T252">say-PST1-3SG</ta>
            <ta e="T254" id="Seg_3920" s="T253">father-1PL.[NOM]</ta>
            <ta e="T257" id="Seg_3921" s="T256">slyness-VBZ-CVB.SIM-slyness-VBZ-CVB.SIM</ta>
            <ta e="T258" id="Seg_3922" s="T257">smile-CVB.SIM-smile-CVB.SIM</ta>
            <ta e="T259" id="Seg_3923" s="T258">say-PST1-3SG</ta>
            <ta e="T260" id="Seg_3924" s="T259">well</ta>
            <ta e="T261" id="Seg_3925" s="T260">guess-PST1-2SG</ta>
            <ta e="T262" id="Seg_3926" s="T261">knee.[NOM]</ta>
            <ta e="T263" id="Seg_3927" s="T262">bone-3SG-ACC</ta>
            <ta e="T264" id="Seg_3928" s="T263">good.[NOM]</ta>
            <ta e="T265" id="Seg_3929" s="T264">well</ta>
            <ta e="T266" id="Seg_3930" s="T265">this</ta>
            <ta e="T269" id="Seg_3931" s="T268">knee.[NOM]</ta>
            <ta e="T270" id="Seg_3932" s="T269">bone-3SG-DAT/LOC</ta>
            <ta e="T271" id="Seg_3933" s="T270">where.from</ta>
            <ta e="T272" id="Seg_3934" s="T271">INDEF</ta>
            <ta e="T275" id="Seg_3935" s="T274">see-CVB.SEQ-2SG</ta>
            <ta e="T276" id="Seg_3936" s="T275">one</ta>
            <ta e="T277" id="Seg_3937" s="T276">INDEF</ta>
            <ta e="T278" id="Seg_3938" s="T277">sinew-ACC</ta>
            <ta e="T279" id="Seg_3939" s="T278">find-FUT-2SG</ta>
            <ta e="T280" id="Seg_3940" s="T279">Q</ta>
            <ta e="T281" id="Seg_3941" s="T280">then</ta>
            <ta e="T282" id="Seg_3942" s="T281">1SG.[NOM]</ta>
            <ta e="T283" id="Seg_3943" s="T282">word-1SG-ACC</ta>
            <ta e="T284" id="Seg_3944" s="T283">take-FUT-1SG</ta>
            <ta e="T285" id="Seg_3945" s="T284">NEG-3SG</ta>
            <ta e="T286" id="Seg_3946" s="T285">back</ta>
            <ta e="T287" id="Seg_3947" s="T286">find-TEMP-2SG</ta>
            <ta e="T288" id="Seg_3948" s="T287">one</ta>
            <ta e="T289" id="Seg_3949" s="T288">INDEF</ta>
            <ta e="T290" id="Seg_3950" s="T289">sinew-ACC</ta>
            <ta e="T291" id="Seg_3951" s="T290">then</ta>
            <ta e="T292" id="Seg_3952" s="T291">1SG.[NOM]</ta>
            <ta e="T293" id="Seg_3953" s="T292">daughter-1SG-ACC</ta>
            <ta e="T294" id="Seg_3954" s="T293">2SG-DAT/LOC</ta>
            <ta e="T295" id="Seg_3955" s="T294">give-FUT-1SG</ta>
            <ta e="T296" id="Seg_3956" s="T295">well</ta>
            <ta e="T297" id="Seg_3957" s="T296">boy-1PL.[NOM]</ta>
            <ta e="T298" id="Seg_3958" s="T297">see-CVB.SIM</ta>
            <ta e="T299" id="Seg_3959" s="T298">try-PST1-3SG</ta>
            <ta e="T300" id="Seg_3960" s="T299">see-CVB.SIM</ta>
            <ta e="T301" id="Seg_3961" s="T300">try-PST1-3SG</ta>
            <ta e="T302" id="Seg_3962" s="T301">then</ta>
            <ta e="T303" id="Seg_3963" s="T302">silently-ADVZ</ta>
            <ta e="T304" id="Seg_3964" s="T303">self-3SG.[NOM]</ta>
            <ta e="T305" id="Seg_3965" s="T304">self-3SG-DAT/LOC</ta>
            <ta e="T306" id="Seg_3966" s="T305">speak-PRS.[3SG]</ta>
            <ta e="T307" id="Seg_3967" s="T306">oh</ta>
            <ta e="T308" id="Seg_3968" s="T307">well</ta>
            <ta e="T309" id="Seg_3969" s="T308">very</ta>
            <ta e="T310" id="Seg_3970" s="T309">be-PST2.[3SG]</ta>
            <ta e="T311" id="Seg_3971" s="T310">well</ta>
            <ta e="T312" id="Seg_3972" s="T311">indeed</ta>
            <ta e="T313" id="Seg_3973" s="T312">stone-DAT/LOC</ta>
            <ta e="T314" id="Seg_3974" s="T313">similar</ta>
            <ta e="T315" id="Seg_3975" s="T314">what.[NOM]</ta>
            <ta e="T316" id="Seg_3976" s="T315">beautiful</ta>
            <ta e="T317" id="Seg_3977" s="T316">bone-3SG.[NOM]=Q</ta>
            <ta e="T318" id="Seg_3978" s="T317">child-PL.[NOM]</ta>
            <ta e="T319" id="Seg_3979" s="T318">that-3SG-2SG.[NOM]</ta>
            <ta e="T320" id="Seg_3980" s="T319">well</ta>
            <ta e="T321" id="Seg_3981" s="T320">know-NEG.PTCP</ta>
            <ta e="T322" id="Seg_3982" s="T321">be-PST2.[3SG]</ta>
            <ta e="T323" id="Seg_3983" s="T322">3SG.[NOM]</ta>
            <ta e="T324" id="Seg_3984" s="T323">3SG.[NOM]</ta>
            <ta e="T325" id="Seg_3985" s="T324">that.[NOM]</ta>
            <ta e="T326" id="Seg_3986" s="T325">father-3PL.[NOM]</ta>
            <ta e="T327" id="Seg_3987" s="T326">just</ta>
            <ta e="T328" id="Seg_3988" s="T327">that.[NOM]</ta>
            <ta e="T329" id="Seg_3989" s="T328">know-PRS.[3SG]</ta>
            <ta e="T330" id="Seg_3990" s="T329">one</ta>
            <ta e="T331" id="Seg_3991" s="T330">place-3SG.[NOM]</ta>
            <ta e="T332" id="Seg_3992" s="T331">that-3SG-2SG.[NOM]</ta>
            <ta e="T333" id="Seg_3993" s="T332">at.all</ta>
            <ta e="T334" id="Seg_3994" s="T333">one</ta>
            <ta e="T335" id="Seg_3995" s="T334">NEG</ta>
            <ta e="T336" id="Seg_3996" s="T335">sinew-POSS</ta>
            <ta e="T337" id="Seg_3997" s="T336">NEG.[3SG]</ta>
            <ta e="T338" id="Seg_3998" s="T337">one-2SG-DAT/LOC</ta>
            <ta e="T339" id="Seg_3999" s="T338">place-3SG-ACC</ta>
            <ta e="T340" id="Seg_4000" s="T339">see-FUT-2SG</ta>
            <ta e="T341" id="Seg_4001" s="T340">good-INTNS-ADVZ</ta>
            <ta e="T342" id="Seg_4002" s="T341">see-PTCP.COND-DAT/LOC</ta>
            <ta e="T343" id="Seg_4003" s="T342">that-3SG-2SG.[NOM]</ta>
            <ta e="T344" id="Seg_4004" s="T343">girl.[NOM]</ta>
            <ta e="T347" id="Seg_4005" s="T346">face-3SG-DAT/LOC</ta>
            <ta e="T348" id="Seg_4006" s="T347">similar</ta>
            <ta e="T349" id="Seg_4007" s="T348">woman.[NOM]</ta>
            <ta e="T350" id="Seg_4008" s="T349">face-3SG-DAT/LOC</ta>
            <ta e="T351" id="Seg_4009" s="T350">similar</ta>
            <ta e="T352" id="Seg_4010" s="T351">one.out.of.two</ta>
            <ta e="T353" id="Seg_4011" s="T352">side-3SG-DAT/LOC</ta>
            <ta e="T354" id="Seg_4012" s="T353">that-3SG-2SG-ACC</ta>
            <ta e="T355" id="Seg_4013" s="T354">father-1PL.[NOM]</ta>
            <ta e="T356" id="Seg_4014" s="T355">just</ta>
            <ta e="T357" id="Seg_4015" s="T356">know-PRS.[3SG]</ta>
            <ta e="T358" id="Seg_4016" s="T357">EMPH</ta>
            <ta e="T359" id="Seg_4017" s="T358">well</ta>
            <ta e="T360" id="Seg_4018" s="T359">whereto</ta>
            <ta e="T361" id="Seg_4019" s="T360">well</ta>
            <ta e="T362" id="Seg_4020" s="T361">become-PTCP.FUT-3SG-ACC</ta>
            <ta e="T363" id="Seg_4021" s="T362">very</ta>
            <ta e="T364" id="Seg_4022" s="T363">Hunter.[NOM]</ta>
            <ta e="T365" id="Seg_4023" s="T364">boy-1PL.[NOM]</ta>
            <ta e="T366" id="Seg_4024" s="T365">get.angry-CVB.PURP</ta>
            <ta e="T367" id="Seg_4025" s="T366">and</ta>
            <ta e="T368" id="Seg_4026" s="T367">want-PRS.[3SG]</ta>
            <ta e="T369" id="Seg_4027" s="T368">fear-PRS.[3SG]</ta>
            <ta e="T370" id="Seg_4028" s="T369">then</ta>
            <ta e="T371" id="Seg_4029" s="T370">well</ta>
            <ta e="T372" id="Seg_4030" s="T371">glance-NMNZ.[NOM]</ta>
            <ta e="T373" id="Seg_4031" s="T372">make-PRS.[3SG]</ta>
            <ta e="T374" id="Seg_4032" s="T373">EMPH</ta>
            <ta e="T375" id="Seg_4033" s="T374">daughter-3SG-ACC</ta>
            <ta e="T376" id="Seg_4034" s="T375">to</ta>
            <ta e="T377" id="Seg_4035" s="T376">girl-3SG.[NOM]</ta>
            <ta e="T378" id="Seg_4036" s="T377">slyness-VBZ-PST1-3SG</ta>
            <ta e="T379" id="Seg_4037" s="T378">also</ta>
            <ta e="T380" id="Seg_4038" s="T379">how</ta>
            <ta e="T381" id="Seg_4039" s="T380">make-FUT-1SG=Q</ta>
            <ta e="T382" id="Seg_4040" s="T381">think-CVB.SEQ</ta>
            <ta e="T383" id="Seg_4041" s="T382">after</ta>
            <ta e="T384" id="Seg_4042" s="T383">mosquito-PL-ABL</ta>
            <ta e="T385" id="Seg_4043" s="T384">chase.away-PTCP.PRS.[NOM]</ta>
            <ta e="T386" id="Seg_4044" s="T385">similar</ta>
            <ta e="T387" id="Seg_4045" s="T386">hand-3SG-ACC</ta>
            <ta e="T388" id="Seg_4046" s="T387">silently-ADVZ</ta>
            <ta e="T389" id="Seg_4047" s="T388">like.this-INTNS</ta>
            <ta e="T390" id="Seg_4048" s="T389">take-PST1-3SG</ta>
            <ta e="T391" id="Seg_4049" s="T390">then</ta>
            <ta e="T392" id="Seg_4050" s="T391">this</ta>
            <ta e="T393" id="Seg_4051" s="T392">silently-ADVZ</ta>
            <ta e="T394" id="Seg_4052" s="T393">this</ta>
            <ta e="T395" id="Seg_4053" s="T394">like.this-INTNS</ta>
            <ta e="T396" id="Seg_4054" s="T395">face-3SG-INSTR</ta>
            <ta e="T397" id="Seg_4055" s="T396">face-3SG-ACC</ta>
            <ta e="T398" id="Seg_4056" s="T397">well</ta>
            <ta e="T399" id="Seg_4057" s="T398">who-VBZ-PRS.[3SG]</ta>
            <ta e="T400" id="Seg_4058" s="T399">show-PRS.[3SG]</ta>
            <ta e="T401" id="Seg_4059" s="T400">mosquito-PL-ABL</ta>
            <ta e="T402" id="Seg_4060" s="T401">kick-EP-MED-PTCP.PRS-DAT/LOC</ta>
            <ta e="T403" id="Seg_4061" s="T402">similar</ta>
            <ta e="T404" id="Seg_4062" s="T403">oh</ta>
            <ta e="T405" id="Seg_4063" s="T404">well</ta>
            <ta e="T406" id="Seg_4064" s="T405">boy-1PL.[NOM]</ta>
            <ta e="T407" id="Seg_4065" s="T406">well</ta>
            <ta e="T408" id="Seg_4066" s="T407">be.happy-PRS.[3SG]</ta>
            <ta e="T409" id="Seg_4067" s="T408">EMPH</ta>
            <ta e="T410" id="Seg_4068" s="T409">fast</ta>
            <ta e="T411" id="Seg_4069" s="T410">very-ADVZ</ta>
            <ta e="T412" id="Seg_4070" s="T411">knife-3SG-ACC</ta>
            <ta e="T413" id="Seg_4071" s="T412">take-PST1-3SG</ta>
            <ta e="T414" id="Seg_4072" s="T413">then</ta>
            <ta e="T415" id="Seg_4073" s="T414">silently-ADVZ</ta>
            <ta e="T416" id="Seg_4074" s="T415">raise-PST1-3SG</ta>
            <ta e="T417" id="Seg_4075" s="T416">knee.[NOM]</ta>
            <ta e="T418" id="Seg_4076" s="T417">bone-3SG-ACC</ta>
            <ta e="T419" id="Seg_4077" s="T418">oh</ta>
            <ta e="T420" id="Seg_4078" s="T419">fast</ta>
            <ta e="T421" id="Seg_4079" s="T420">fast</ta>
            <ta e="T422" id="Seg_4080" s="T421">very-ADVZ</ta>
            <ta e="T423" id="Seg_4081" s="T422">knife-3SG-INSTR</ta>
            <ta e="T424" id="Seg_4082" s="T423">sinew.[NOM]</ta>
            <ta e="T425" id="Seg_4083" s="T424">INTNS-ACC</ta>
            <ta e="T426" id="Seg_4084" s="T425">pull-CVB.SEQ</ta>
            <ta e="T427" id="Seg_4085" s="T426">take-NEG.[3SG]</ta>
            <ta e="T428" id="Seg_4086" s="T427">Q</ta>
            <ta e="T429" id="Seg_4087" s="T428">there</ta>
            <ta e="T430" id="Seg_4088" s="T429">father-3SG.[NOM]</ta>
            <ta e="T431" id="Seg_4089" s="T430">say-PST1-3SG</ta>
            <ta e="T432" id="Seg_4090" s="T431">well</ta>
            <ta e="T433" id="Seg_4091" s="T432">how</ta>
            <ta e="T434" id="Seg_4092" s="T433">make-FUT-2SG=Q</ta>
            <ta e="T435" id="Seg_4093" s="T434">1SG.[NOM]</ta>
            <ta e="T436" id="Seg_4094" s="T435">say-PST2-EP-1SG</ta>
            <ta e="T437" id="Seg_4095" s="T436">word-1SG-ACC</ta>
            <ta e="T438" id="Seg_4096" s="T437">back</ta>
            <ta e="T439" id="Seg_4097" s="T438">take-FUT-1SG</ta>
            <ta e="T440" id="Seg_4098" s="T439">NEG-3SG</ta>
            <ta e="T441" id="Seg_4099" s="T440">such</ta>
            <ta e="T442" id="Seg_4100" s="T441">master.[NOM]</ta>
            <ta e="T443" id="Seg_4101" s="T442">human.being.[NOM]</ta>
            <ta e="T444" id="Seg_4102" s="T443">such</ta>
            <ta e="T445" id="Seg_4103" s="T444">hunter.[NOM]</ta>
            <ta e="T446" id="Seg_4104" s="T445">human.being.[NOM]</ta>
            <ta e="T447" id="Seg_4105" s="T446">every-3SG-ACC</ta>
            <ta e="T448" id="Seg_4106" s="T447">know-PRS.[3SG]</ta>
            <ta e="T449" id="Seg_4107" s="T448">well</ta>
            <ta e="T450" id="Seg_4108" s="T449">give-PRS-1SG</ta>
            <ta e="T451" id="Seg_4109" s="T450">2SG-DAT/LOC</ta>
            <ta e="T452" id="Seg_4110" s="T451">Hunter.[NOM]</ta>
            <ta e="T453" id="Seg_4111" s="T452">daughter-1SG-ACC</ta>
            <ta e="T454" id="Seg_4112" s="T453">happy-ADVZ</ta>
            <ta e="T455" id="Seg_4113" s="T454">live-EP-IMP.2PL</ta>
            <ta e="T456" id="Seg_4114" s="T455">good-ADVZ</ta>
            <ta e="T457" id="Seg_4115" s="T456">live-EP-IMP.2PL</ta>
            <ta e="T458" id="Seg_4116" s="T457">many</ta>
            <ta e="T459" id="Seg_4117" s="T458">child-ACC</ta>
            <ta e="T460" id="Seg_4118" s="T459">child-VBZ-EP-IMP.2PL</ta>
            <ta e="T461" id="Seg_4119" s="T460">say-PST1-3SG</ta>
            <ta e="T462" id="Seg_4120" s="T461">well</ta>
            <ta e="T463" id="Seg_4121" s="T462">that</ta>
            <ta e="T464" id="Seg_4122" s="T463">like-INTNS</ta>
            <ta e="T465" id="Seg_4123" s="T464">Hunter.[NOM]</ta>
            <ta e="T466" id="Seg_4124" s="T465">girl-3SG-ACC</ta>
            <ta e="T467" id="Seg_4125" s="T466">with</ta>
            <ta e="T468" id="Seg_4126" s="T467">good</ta>
            <ta e="T469" id="Seg_4127" s="T468">very-ADVZ</ta>
            <ta e="T470" id="Seg_4128" s="T469">live-PST2-3PL</ta>
         </annotation>
         <annotation name="gg" tierref="gg">
            <ta e="T1" id="Seg_4129" s="T0">1SG.[NOM]</ta>
            <ta e="T2" id="Seg_4130" s="T1">dieses</ta>
            <ta e="T3" id="Seg_4131" s="T2">Erzählung-ACC</ta>
            <ta e="T4" id="Seg_4132" s="T3">hören-EP-PST2-EP-1SG</ta>
            <ta e="T5" id="Seg_4133" s="T4">Eltern-PL-1SG-ACC</ta>
            <ta e="T7" id="Seg_4134" s="T5">mit</ta>
            <ta e="T8" id="Seg_4135" s="T7">Schule-DAT/LOC</ta>
            <ta e="T9" id="Seg_4136" s="T8">hineingehen-PTCP.PRS</ta>
            <ta e="T10" id="Seg_4137" s="T9">Jahr-1SG-DAT/LOC</ta>
            <ta e="T11" id="Seg_4138" s="T10">jenes-3SG-2SG.[NOM]</ta>
            <ta e="T12" id="Seg_4139" s="T11">jetzt-DAT/LOC</ta>
            <ta e="T13" id="Seg_4140" s="T12">bis.zu</ta>
            <ta e="T14" id="Seg_4141" s="T13">Gedächtnis-1SG-DAT/LOC</ta>
            <ta e="T15" id="Seg_4142" s="T14">es.gibt</ta>
            <ta e="T16" id="Seg_4143" s="T15">so-INTNS</ta>
            <ta e="T17" id="Seg_4144" s="T16">Mama-1SG.[NOM]</ta>
            <ta e="T18" id="Seg_4145" s="T17">ältere.Schwester-3SG.[NOM]</ta>
            <ta e="T19" id="Seg_4146" s="T18">Claudia.[NOM]</ta>
            <ta e="T20" id="Seg_4147" s="T19">erzählen-PTCP.HAB</ta>
            <ta e="T21" id="Seg_4148" s="T20">sein-PST1-3SG</ta>
            <ta e="T22" id="Seg_4149" s="T21">fern-fern</ta>
            <ta e="T23" id="Seg_4150" s="T22">Tundra-DAT/LOC</ta>
            <ta e="T24" id="Seg_4151" s="T23">geboren.werden-PTCP.PST</ta>
            <ta e="T25" id="Seg_4152" s="T24">Ort-3SG-DAT/LOC</ta>
            <ta e="T26" id="Seg_4153" s="T25">selbst-3SG-GEN</ta>
            <ta e="T27" id="Seg_4154" s="T26">geboren.werden-PTCP.PST</ta>
            <ta e="T28" id="Seg_4155" s="T27">Ort-3SG-DAT/LOC</ta>
            <ta e="T29" id="Seg_4156" s="T28">Familie-PL-3SG-ACC</ta>
            <ta e="T30" id="Seg_4157" s="T29">mit</ta>
            <ta e="T31" id="Seg_4158" s="T30">reisen-CVB.SIM</ta>
            <ta e="T32" id="Seg_4159" s="T31">gehen-EP-PST2-3SG</ta>
            <ta e="T33" id="Seg_4160" s="T32">schön</ta>
            <ta e="T34" id="Seg_4161" s="T33">sehr</ta>
            <ta e="T35" id="Seg_4162" s="T34">Junge.[NOM]</ta>
            <ta e="T36" id="Seg_4163" s="T35">jenes-3SG-2SG.[NOM]</ta>
            <ta e="T37" id="Seg_4164" s="T36">Name-3SG.[NOM]</ta>
            <ta e="T38" id="Seg_4165" s="T37">Jäger.[NOM]</ta>
            <ta e="T39" id="Seg_4166" s="T38">sein-PST1-3SG</ta>
            <ta e="T40" id="Seg_4167" s="T39">anders</ta>
            <ta e="T41" id="Seg_4168" s="T40">Ort-DAT/LOC</ta>
            <ta e="T44" id="Seg_4169" s="T43">3PL-ABL</ta>
            <ta e="T45" id="Seg_4170" s="T44">weit.weg-INTNS</ta>
            <ta e="T46" id="Seg_4171" s="T45">sich.verstecken-CVB.SIM</ta>
            <ta e="T47" id="Seg_4172" s="T46">gehen-EP-PST2-3SG</ta>
            <ta e="T48" id="Seg_4173" s="T47">schön</ta>
            <ta e="T49" id="Seg_4174" s="T48">Mädchen.[NOM]</ta>
            <ta e="T50" id="Seg_4175" s="T49">Familie-PL-3SG-ACC</ta>
            <ta e="T51" id="Seg_4176" s="T50">mit</ta>
            <ta e="T52" id="Seg_4177" s="T51">dieses.[NOM]</ta>
            <ta e="T53" id="Seg_4178" s="T52">jung</ta>
            <ta e="T54" id="Seg_4179" s="T53">äh</ta>
            <ta e="T55" id="Seg_4180" s="T54">Junge-ACC</ta>
            <ta e="T56" id="Seg_4181" s="T55">mit</ta>
            <ta e="T57" id="Seg_4182" s="T56">dieses.[NOM]</ta>
            <ta e="T58" id="Seg_4183" s="T57">Mädchen.[NOM]</ta>
            <ta e="T59" id="Seg_4184" s="T58">treffen-PTCP.HAB</ta>
            <ta e="T60" id="Seg_4185" s="T59">sein-PST2-3PL</ta>
            <ta e="T61" id="Seg_4186" s="T60">lieben-RECP/COLL-PTCP.HAB</ta>
            <ta e="T62" id="Seg_4187" s="T61">sein-PST2-3PL</ta>
            <ta e="T63" id="Seg_4188" s="T62">selbst-3PL-GEN</ta>
            <ta e="T64" id="Seg_4189" s="T63">geboren.werden-PTCP.PST</ta>
            <ta e="T65" id="Seg_4190" s="T64">Erde-3PL-DAT/LOC</ta>
            <ta e="T66" id="Seg_4191" s="T65">einmal</ta>
            <ta e="T67" id="Seg_4192" s="T66">Mädchen-3SG.[NOM]</ta>
            <ta e="T68" id="Seg_4193" s="T67">Jäger-3SG-ACC</ta>
            <ta e="T69" id="Seg_4194" s="T68">Junge-3SG-ACC</ta>
            <ta e="T70" id="Seg_4195" s="T69">sprechen-PRS.[3SG]</ta>
            <ta e="T71" id="Seg_4196" s="T70">wie.lange</ta>
            <ta e="T72" id="Seg_4197" s="T71">1PL.[NOM]</ta>
            <ta e="T73" id="Seg_4198" s="T72">Eltern-PL-1PL-ABL</ta>
            <ta e="T74" id="Seg_4199" s="T73">sich.verstecken-CVB.SIM</ta>
            <ta e="T75" id="Seg_4200" s="T74">gehen-FUT-1PL=Q</ta>
            <ta e="T76" id="Seg_4201" s="T75">kommen.[IMP.2SG]</ta>
            <ta e="T77" id="Seg_4202" s="T76">1PL-DAT/LOC</ta>
            <ta e="T78" id="Seg_4203" s="T77">besuchen-PTCP.PST</ta>
            <ta e="T79" id="Seg_4204" s="T78">sein.[IMP.2SG]</ta>
            <ta e="T80" id="Seg_4205" s="T79">Eltern-PL-1SG-ACC</ta>
            <ta e="T81" id="Seg_4206" s="T80">mit</ta>
            <ta e="T82" id="Seg_4207" s="T81">sprechen-EP-RECP/COLL.[IMP.2SG]</ta>
            <ta e="T83" id="Seg_4208" s="T82">Vater-1SG-ACC</ta>
            <ta e="T84" id="Seg_4209" s="T83">mit</ta>
            <ta e="T85" id="Seg_4210" s="T84">sprechen-EP-RECP/COLL.[IMP.2SG]</ta>
            <ta e="T86" id="Seg_4211" s="T85">Vater-1SG-ABL</ta>
            <ta e="T87" id="Seg_4212" s="T86">bitten.[IMP.2SG]</ta>
            <ta e="T88" id="Seg_4213" s="T87">1SG-ACC</ta>
            <ta e="T89" id="Seg_4214" s="T88">Vater-1SG.[NOM]</ta>
            <ta e="T93" id="Seg_4215" s="T92">gut</ta>
            <ta e="T94" id="Seg_4216" s="T93">sehr</ta>
            <ta e="T95" id="Seg_4217" s="T94">Mensch.[NOM]</ta>
            <ta e="T96" id="Seg_4218" s="T95">und</ta>
            <ta e="T97" id="Seg_4219" s="T96">sein-COND.[3SG]</ta>
            <ta e="T98" id="Seg_4220" s="T97">Schläue-PROPR.[NOM]</ta>
            <ta e="T99" id="Seg_4221" s="T98">doch</ta>
            <ta e="T100" id="Seg_4222" s="T99">jenes</ta>
            <ta e="T101" id="Seg_4223" s="T100">Schläue-3SG-ABL</ta>
            <ta e="T102" id="Seg_4224" s="T101">2SG.[NOM]</ta>
            <ta e="T103" id="Seg_4225" s="T102">fürchten-EP-NEG.[IMP.2SG]</ta>
            <ta e="T104" id="Seg_4226" s="T103">doch</ta>
            <ta e="T105" id="Seg_4227" s="T104">jenes.[NOM]</ta>
            <ta e="T106" id="Seg_4228" s="T105">wie-INTNS</ta>
            <ta e="T107" id="Seg_4229" s="T106">sprechen-PST1-3SG</ta>
            <ta e="T108" id="Seg_4230" s="T107">und</ta>
            <ta e="T109" id="Seg_4231" s="T108">Junge-1PL.[NOM]</ta>
            <ta e="T110" id="Seg_4232" s="T109">jenes.[NOM]</ta>
            <ta e="T111" id="Seg_4233" s="T110">wie-INTNS</ta>
            <ta e="T112" id="Seg_4234" s="T111">wie.viel</ta>
            <ta e="T113" id="Seg_4235" s="T112">INDEF</ta>
            <ta e="T114" id="Seg_4236" s="T113">Tag.[NOM]</ta>
            <ta e="T115" id="Seg_4237" s="T114">sein-CVB.SEQ</ta>
            <ta e="T116" id="Seg_4238" s="T115">nachdem</ta>
            <ta e="T117" id="Seg_4239" s="T116">warm</ta>
            <ta e="T118" id="Seg_4240" s="T117">sehr</ta>
            <ta e="T119" id="Seg_4241" s="T118">Tag-DAT/LOC</ta>
            <ta e="T120" id="Seg_4242" s="T119">im.Frühling</ta>
            <ta e="T121" id="Seg_4243" s="T120">gegen.Herbst</ta>
            <ta e="T122" id="Seg_4244" s="T121">Sonne-PROPR</ta>
            <ta e="T123" id="Seg_4245" s="T122">sehr</ta>
            <ta e="T124" id="Seg_4246" s="T123">Tag-DAT/LOC</ta>
            <ta e="T125" id="Seg_4247" s="T124">doch</ta>
            <ta e="T126" id="Seg_4248" s="T125">Reitrentier-VBZ-CVB.SEQ</ta>
            <ta e="T127" id="Seg_4249" s="T126">gehen-CVB.SEQ</ta>
            <ta e="T128" id="Seg_4250" s="T127">doch</ta>
            <ta e="T129" id="Seg_4251" s="T128">kommen-PRS.[3SG]</ta>
            <ta e="T130" id="Seg_4252" s="T129">selbst-3SG-GEN</ta>
            <ta e="T133" id="Seg_4253" s="T132">weiß</ta>
            <ta e="T134" id="Seg_4254" s="T133">gezähmtes.Rentier-PROPR.[NOM]</ta>
            <ta e="T135" id="Seg_4255" s="T134">jenes-3SG-3SG-INSTR</ta>
            <ta e="T136" id="Seg_4256" s="T135">besuchen-CVB.SIM</ta>
            <ta e="T137" id="Seg_4257" s="T136">gehen-PST1-3SG</ta>
            <ta e="T138" id="Seg_4258" s="T137">Mädchen-3SG-DAT/LOC</ta>
            <ta e="T139" id="Seg_4259" s="T138">hineingehen-CVB.ANT-3SG</ta>
            <ta e="T140" id="Seg_4260" s="T139">Erde-DAT/LOC</ta>
            <ta e="T141" id="Seg_4261" s="T140">bis.zu</ta>
            <ta e="T142" id="Seg_4262" s="T141">Kopf-3SG-ACC</ta>
            <ta e="T143" id="Seg_4263" s="T142">Eltern-PL-3SG-DAT/LOC</ta>
            <ta e="T144" id="Seg_4264" s="T143">beugen-ITER-CVB.SEQ</ta>
            <ta e="T145" id="Seg_4265" s="T144">nachdem</ta>
            <ta e="T146" id="Seg_4266" s="T145">hallo-IMP.2PL</ta>
            <ta e="T147" id="Seg_4267" s="T146">hallo-IMP.2PL</ta>
            <ta e="T148" id="Seg_4268" s="T147">alt-PL.[NOM]</ta>
            <ta e="T149" id="Seg_4269" s="T148">sagen-PST1-3SG</ta>
            <ta e="T150" id="Seg_4270" s="T149">Jäger.[NOM]</ta>
            <ta e="T151" id="Seg_4271" s="T150">Junge-1PL.[NOM]</ta>
            <ta e="T152" id="Seg_4272" s="T151">doch</ta>
            <ta e="T153" id="Seg_4273" s="T152">dieses</ta>
            <ta e="T154" id="Seg_4274" s="T153">kommen-PST1-1SG</ta>
            <ta e="T155" id="Seg_4275" s="T154">2PL-DAT/LOC</ta>
            <ta e="T157" id="Seg_4276" s="T156">Mutter.[NOM]</ta>
            <ta e="T158" id="Seg_4277" s="T157">Vater.[NOM]</ta>
            <ta e="T159" id="Seg_4278" s="T158">2PL-ABL</ta>
            <ta e="T160" id="Seg_4279" s="T159">Tochter-2PL-ACC</ta>
            <ta e="T161" id="Seg_4280" s="T160">bitten-PRS-1SG</ta>
            <ta e="T162" id="Seg_4281" s="T161">was-3SG-ACC</ta>
            <ta e="T163" id="Seg_4282" s="T162">wie.lange</ta>
            <ta e="T164" id="Seg_4283" s="T163">verstecken-FUT-1PL=Q</ta>
            <ta e="T165" id="Seg_4284" s="T164">1PL.[NOM]</ta>
            <ta e="T166" id="Seg_4285" s="T165">1PL.[NOM]</ta>
            <ta e="T167" id="Seg_4286" s="T166">vor.langem-ADJZ-ABL</ta>
            <ta e="T168" id="Seg_4287" s="T167">lieben-RECP/COLL-PRS-1PL</ta>
            <ta e="T169" id="Seg_4288" s="T168">selbst-selbst-1PL-ACC</ta>
            <ta e="T170" id="Seg_4289" s="T169">sehr</ta>
            <ta e="T171" id="Seg_4290" s="T170">achten-PRS-1PL</ta>
            <ta e="T172" id="Seg_4291" s="T171">doch</ta>
            <ta e="T173" id="Seg_4292" s="T172">wie</ta>
            <ta e="T174" id="Seg_4293" s="T173">machen-FUT-2SG=Q</ta>
            <ta e="T175" id="Seg_4294" s="T174">sagen-PST1-3SG</ta>
            <ta e="T176" id="Seg_4295" s="T175">schimmern-CVB.SIM-schimmern-CVB.SIM</ta>
            <ta e="T177" id="Seg_4296" s="T176">Mädchen.[NOM]</ta>
            <ta e="T178" id="Seg_4297" s="T177">Vater-3SG.[NOM]</ta>
            <ta e="T179" id="Seg_4298" s="T178">dann</ta>
            <ta e="T180" id="Seg_4299" s="T179">doch</ta>
            <ta e="T181" id="Seg_4300" s="T180">Schläue-REFL/MED-CVB.PURP</ta>
            <ta e="T182" id="Seg_4301" s="T181">wollen-PST1-3SG</ta>
            <ta e="T183" id="Seg_4302" s="T182">offenbar</ta>
            <ta e="T184" id="Seg_4303" s="T183">denken-CVB.SIM</ta>
            <ta e="T185" id="Seg_4304" s="T184">denken-PST1-3SG</ta>
            <ta e="T186" id="Seg_4305" s="T185">Junge-1PL.[NOM]</ta>
            <ta e="T187" id="Seg_4306" s="T186">Wahrheit.[NOM]</ta>
            <ta e="T188" id="Seg_4307" s="T187">sein-PST2.[3SG]</ta>
            <ta e="T189" id="Seg_4308" s="T188">Mädchen-1PL.[NOM]</ta>
            <ta e="T190" id="Seg_4309" s="T189">Vater-3SG.[NOM]</ta>
            <ta e="T191" id="Seg_4310" s="T190">was-ACC</ta>
            <ta e="T192" id="Seg_4311" s="T191">nur</ta>
            <ta e="T193" id="Seg_4312" s="T192">werfen-PST1-3SG</ta>
            <ta e="T194" id="Seg_4313" s="T193">EVID</ta>
            <ta e="T195" id="Seg_4314" s="T194">tschik</ta>
            <ta e="T196" id="Seg_4315" s="T195">machen-CVB.SEQ</ta>
            <ta e="T197" id="Seg_4316" s="T196">werfen-PST1-3SG</ta>
            <ta e="T198" id="Seg_4317" s="T197">dieses</ta>
            <ta e="T199" id="Seg_4318" s="T198">Herd.[NOM]</ta>
            <ta e="T200" id="Seg_4319" s="T199">oberer.Teil-3SG-INSTR</ta>
            <ta e="T201" id="Seg_4320" s="T200">denken-CVB.SIM</ta>
            <ta e="T202" id="Seg_4321" s="T201">denken-PST1-3SG</ta>
            <ta e="T203" id="Seg_4322" s="T202">dann</ta>
            <ta e="T204" id="Seg_4323" s="T203">halten-FUT.[3SG]</ta>
            <ta e="T205" id="Seg_4324" s="T204">Q</ta>
            <ta e="T206" id="Seg_4325" s="T205">halten-FUT.[3SG]</ta>
            <ta e="T207" id="Seg_4326" s="T206">Q</ta>
            <ta e="T208" id="Seg_4327" s="T207">NEG-3SG</ta>
            <ta e="T209" id="Seg_4328" s="T208">Q</ta>
            <ta e="T210" id="Seg_4329" s="T209">dieses</ta>
            <ta e="T211" id="Seg_4330" s="T210">Knochen-ACC</ta>
            <ta e="T212" id="Seg_4331" s="T211">nun</ta>
            <ta e="T213" id="Seg_4332" s="T212">Junge-1PL.[NOM]</ta>
            <ta e="T214" id="Seg_4333" s="T213">doch</ta>
            <ta e="T215" id="Seg_4334" s="T214">schnell</ta>
            <ta e="T216" id="Seg_4335" s="T215">sehr-ADVZ</ta>
            <ta e="T217" id="Seg_4336" s="T216">greifen-PRS.[3SG]</ta>
            <ta e="T218" id="Seg_4337" s="T217">EMPH</ta>
            <ta e="T219" id="Seg_4338" s="T218">jenes-ACC</ta>
            <ta e="T220" id="Seg_4339" s="T219">fragen-PRS.[3SG]</ta>
            <ta e="T221" id="Seg_4340" s="T220">Mädchen-1PL.[NOM]</ta>
            <ta e="T222" id="Seg_4341" s="T221">Vater-3SG.[NOM]</ta>
            <ta e="T223" id="Seg_4342" s="T222">doch</ta>
            <ta e="T224" id="Seg_4343" s="T223">was-ACC</ta>
            <ta e="T225" id="Seg_4344" s="T224">greifen-PST1-2SG</ta>
            <ta e="T226" id="Seg_4345" s="T225">dieses</ta>
            <ta e="T227" id="Seg_4346" s="T226">wie</ta>
            <ta e="T228" id="Seg_4347" s="T227">was-ACC</ta>
            <ta e="T229" id="Seg_4348" s="T228">sagen-CVB.SIM</ta>
            <ta e="T230" id="Seg_4349" s="T229">fragen-PST1-3SG</ta>
            <ta e="T231" id="Seg_4350" s="T230">jenes</ta>
            <ta e="T232" id="Seg_4351" s="T231">nun</ta>
            <ta e="T233" id="Seg_4352" s="T232">wer.[NOM]</ta>
            <ta e="T234" id="Seg_4353" s="T233">Junge-1PL.[NOM]</ta>
            <ta e="T235" id="Seg_4354" s="T234">1SG.[NOM]</ta>
            <ta e="T238" id="Seg_4355" s="T237">1SG.[NOM]</ta>
            <ta e="T239" id="Seg_4356" s="T238">sehen-PST1-1SG</ta>
            <ta e="T240" id="Seg_4357" s="T239">erkennen-PST1-1SG</ta>
            <ta e="T241" id="Seg_4358" s="T240">Knie.[NOM]</ta>
            <ta e="T242" id="Seg_4359" s="T241">Knochen-3SG.[NOM]</ta>
            <ta e="T243" id="Seg_4360" s="T242">wer.[NOM]</ta>
            <ta e="T244" id="Seg_4361" s="T243">erkennen-NEG.[3SG]</ta>
            <ta e="T245" id="Seg_4362" s="T244">jenes</ta>
            <ta e="T246" id="Seg_4363" s="T245">Knie.[NOM]</ta>
            <ta e="T247" id="Seg_4364" s="T246">Knochen-3SG-ACC</ta>
            <ta e="T248" id="Seg_4365" s="T247">wer.[NOM]</ta>
            <ta e="T249" id="Seg_4366" s="T248">erkennen-EP-NEG-FUT.[3SG]=Q</ta>
            <ta e="T250" id="Seg_4367" s="T249">jenes</ta>
            <ta e="T251" id="Seg_4368" s="T250">Knie.[NOM]</ta>
            <ta e="T252" id="Seg_4369" s="T251">Knochen-3SG-ACC</ta>
            <ta e="T253" id="Seg_4370" s="T252">sagen-PST1-3SG</ta>
            <ta e="T254" id="Seg_4371" s="T253">Vater-1PL.[NOM]</ta>
            <ta e="T257" id="Seg_4372" s="T256">Schläue-VBZ-CVB.SIM-Schläue-VBZ-CVB.SIM</ta>
            <ta e="T258" id="Seg_4373" s="T257">lächeln-CVB.SIM-lächeln-CVB.SIM</ta>
            <ta e="T259" id="Seg_4374" s="T258">sagen-PST1-3SG</ta>
            <ta e="T260" id="Seg_4375" s="T259">doch</ta>
            <ta e="T261" id="Seg_4376" s="T260">erraten-PST1-2SG</ta>
            <ta e="T262" id="Seg_4377" s="T261">Knie.[NOM]</ta>
            <ta e="T263" id="Seg_4378" s="T262">Knochen-3SG-ACC</ta>
            <ta e="T264" id="Seg_4379" s="T263">gut.[NOM]</ta>
            <ta e="T265" id="Seg_4380" s="T264">doch</ta>
            <ta e="T266" id="Seg_4381" s="T265">dieses</ta>
            <ta e="T269" id="Seg_4382" s="T268">Knie.[NOM]</ta>
            <ta e="T270" id="Seg_4383" s="T269">Knochen-3SG-DAT/LOC</ta>
            <ta e="T271" id="Seg_4384" s="T270">woher</ta>
            <ta e="T272" id="Seg_4385" s="T271">INDEF</ta>
            <ta e="T275" id="Seg_4386" s="T274">sehen-CVB.SEQ-2SG</ta>
            <ta e="T276" id="Seg_4387" s="T275">eins</ta>
            <ta e="T277" id="Seg_4388" s="T276">INDEF</ta>
            <ta e="T278" id="Seg_4389" s="T277">Sehne-ACC</ta>
            <ta e="T279" id="Seg_4390" s="T278">finden-FUT-2SG</ta>
            <ta e="T280" id="Seg_4391" s="T279">Q</ta>
            <ta e="T281" id="Seg_4392" s="T280">dann</ta>
            <ta e="T282" id="Seg_4393" s="T281">1SG.[NOM]</ta>
            <ta e="T283" id="Seg_4394" s="T282">Wort-1SG-ACC</ta>
            <ta e="T284" id="Seg_4395" s="T283">nehmen-FUT-1SG</ta>
            <ta e="T285" id="Seg_4396" s="T284">NEG-3SG</ta>
            <ta e="T286" id="Seg_4397" s="T285">zurück</ta>
            <ta e="T287" id="Seg_4398" s="T286">finden-TEMP-2SG</ta>
            <ta e="T288" id="Seg_4399" s="T287">eins</ta>
            <ta e="T289" id="Seg_4400" s="T288">INDEF</ta>
            <ta e="T290" id="Seg_4401" s="T289">Sehne-ACC</ta>
            <ta e="T291" id="Seg_4402" s="T290">dann</ta>
            <ta e="T292" id="Seg_4403" s="T291">1SG.[NOM]</ta>
            <ta e="T293" id="Seg_4404" s="T292">Tochter-1SG-ACC</ta>
            <ta e="T294" id="Seg_4405" s="T293">2SG-DAT/LOC</ta>
            <ta e="T295" id="Seg_4406" s="T294">geben-FUT-1SG</ta>
            <ta e="T296" id="Seg_4407" s="T295">doch</ta>
            <ta e="T297" id="Seg_4408" s="T296">Junge-1PL.[NOM]</ta>
            <ta e="T298" id="Seg_4409" s="T297">sehen-CVB.SIM</ta>
            <ta e="T299" id="Seg_4410" s="T298">versuchen-PST1-3SG</ta>
            <ta e="T300" id="Seg_4411" s="T299">sehen-CVB.SIM</ta>
            <ta e="T301" id="Seg_4412" s="T300">versuchen-PST1-3SG</ta>
            <ta e="T302" id="Seg_4413" s="T301">dann</ta>
            <ta e="T303" id="Seg_4414" s="T302">ganz.leise-ADVZ</ta>
            <ta e="T304" id="Seg_4415" s="T303">selbst-3SG.[NOM]</ta>
            <ta e="T305" id="Seg_4416" s="T304">selbst-3SG-DAT/LOC</ta>
            <ta e="T306" id="Seg_4417" s="T305">sprechen-PRS.[3SG]</ta>
            <ta e="T307" id="Seg_4418" s="T306">oh</ta>
            <ta e="T308" id="Seg_4419" s="T307">doch</ta>
            <ta e="T309" id="Seg_4420" s="T308">sehr</ta>
            <ta e="T310" id="Seg_4421" s="T309">sein-PST2.[3SG]</ta>
            <ta e="T311" id="Seg_4422" s="T310">doch</ta>
            <ta e="T312" id="Seg_4423" s="T311">tatsächlich</ta>
            <ta e="T313" id="Seg_4424" s="T312">Stein-DAT/LOC</ta>
            <ta e="T314" id="Seg_4425" s="T313">ähnlich</ta>
            <ta e="T315" id="Seg_4426" s="T314">was.[NOM]</ta>
            <ta e="T316" id="Seg_4427" s="T315">schön</ta>
            <ta e="T317" id="Seg_4428" s="T316">Knochen-3SG.[NOM]=Q</ta>
            <ta e="T318" id="Seg_4429" s="T317">Kind-PL.[NOM]</ta>
            <ta e="T319" id="Seg_4430" s="T318">jenes-3SG-2SG.[NOM]</ta>
            <ta e="T320" id="Seg_4431" s="T319">na</ta>
            <ta e="T321" id="Seg_4432" s="T320">wissen-NEG.PTCP</ta>
            <ta e="T322" id="Seg_4433" s="T321">sein-PST2.[3SG]</ta>
            <ta e="T323" id="Seg_4434" s="T322">3SG.[NOM]</ta>
            <ta e="T324" id="Seg_4435" s="T323">3SG.[NOM]</ta>
            <ta e="T325" id="Seg_4436" s="T324">dieses.[NOM]</ta>
            <ta e="T326" id="Seg_4437" s="T325">Vater-3PL.[NOM]</ta>
            <ta e="T327" id="Seg_4438" s="T326">nur</ta>
            <ta e="T328" id="Seg_4439" s="T327">dieses.[NOM]</ta>
            <ta e="T329" id="Seg_4440" s="T328">wissen-PRS.[3SG]</ta>
            <ta e="T330" id="Seg_4441" s="T329">eins</ta>
            <ta e="T331" id="Seg_4442" s="T330">Ort-3SG.[NOM]</ta>
            <ta e="T332" id="Seg_4443" s="T331">jenes-3SG-2SG.[NOM]</ta>
            <ta e="T333" id="Seg_4444" s="T332">völlig</ta>
            <ta e="T334" id="Seg_4445" s="T333">eins</ta>
            <ta e="T335" id="Seg_4446" s="T334">NEG</ta>
            <ta e="T336" id="Seg_4447" s="T335">Sehne-POSS</ta>
            <ta e="T337" id="Seg_4448" s="T336">NEG.[3SG]</ta>
            <ta e="T338" id="Seg_4449" s="T337">eins-2SG-DAT/LOC</ta>
            <ta e="T339" id="Seg_4450" s="T338">Ort-3SG-ACC</ta>
            <ta e="T340" id="Seg_4451" s="T339">sehen-FUT-2SG</ta>
            <ta e="T341" id="Seg_4452" s="T340">gut-INTNS-ADVZ</ta>
            <ta e="T342" id="Seg_4453" s="T341">sehen-PTCP.COND-DAT/LOC</ta>
            <ta e="T343" id="Seg_4454" s="T342">jenes-3SG-2SG.[NOM]</ta>
            <ta e="T344" id="Seg_4455" s="T343">Mädchen.[NOM]</ta>
            <ta e="T347" id="Seg_4456" s="T346">Gesicht-3SG-DAT/LOC</ta>
            <ta e="T348" id="Seg_4457" s="T347">ähnlich</ta>
            <ta e="T349" id="Seg_4458" s="T348">Frau.[NOM]</ta>
            <ta e="T350" id="Seg_4459" s="T349">Gesicht-3SG-DAT/LOC</ta>
            <ta e="T351" id="Seg_4460" s="T350">ähnlich</ta>
            <ta e="T352" id="Seg_4461" s="T351">einer.von.zwei</ta>
            <ta e="T353" id="Seg_4462" s="T352">Seite-3SG-DAT/LOC</ta>
            <ta e="T354" id="Seg_4463" s="T353">jenes-3SG-2SG-ACC</ta>
            <ta e="T355" id="Seg_4464" s="T354">Vater-1PL.[NOM]</ta>
            <ta e="T356" id="Seg_4465" s="T355">nur</ta>
            <ta e="T357" id="Seg_4466" s="T356">wissen-PRS.[3SG]</ta>
            <ta e="T358" id="Seg_4467" s="T357">EMPH</ta>
            <ta e="T359" id="Seg_4468" s="T358">doch</ta>
            <ta e="T360" id="Seg_4469" s="T359">wohin</ta>
            <ta e="T361" id="Seg_4470" s="T360">doch</ta>
            <ta e="T362" id="Seg_4471" s="T361">werden-PTCP.FUT-3SG-ACC</ta>
            <ta e="T363" id="Seg_4472" s="T362">sehr</ta>
            <ta e="T364" id="Seg_4473" s="T363">Jäger.[NOM]</ta>
            <ta e="T365" id="Seg_4474" s="T364">Junge-1PL.[NOM]</ta>
            <ta e="T366" id="Seg_4475" s="T365">böse.werden-CVB.PURP</ta>
            <ta e="T367" id="Seg_4476" s="T366">und</ta>
            <ta e="T368" id="Seg_4477" s="T367">wollen-PRS.[3SG]</ta>
            <ta e="T369" id="Seg_4478" s="T368">fürchten-PRS.[3SG]</ta>
            <ta e="T370" id="Seg_4479" s="T369">dann</ta>
            <ta e="T371" id="Seg_4480" s="T370">doch</ta>
            <ta e="T372" id="Seg_4481" s="T371">blicken-NMNZ.[NOM]</ta>
            <ta e="T373" id="Seg_4482" s="T372">machen-PRS.[3SG]</ta>
            <ta e="T374" id="Seg_4483" s="T373">EMPH</ta>
            <ta e="T375" id="Seg_4484" s="T374">Tochter-3SG-ACC</ta>
            <ta e="T376" id="Seg_4485" s="T375">zu</ta>
            <ta e="T377" id="Seg_4486" s="T376">Mädchen-3SG.[NOM]</ta>
            <ta e="T378" id="Seg_4487" s="T377">Schläue-VBZ-PST1-3SG</ta>
            <ta e="T379" id="Seg_4488" s="T378">auch</ta>
            <ta e="T380" id="Seg_4489" s="T379">wie</ta>
            <ta e="T381" id="Seg_4490" s="T380">machen-FUT-1SG=Q</ta>
            <ta e="T382" id="Seg_4491" s="T381">denken-CVB.SEQ</ta>
            <ta e="T383" id="Seg_4492" s="T382">nachdem</ta>
            <ta e="T384" id="Seg_4493" s="T383">Mücke-PL-ABL</ta>
            <ta e="T385" id="Seg_4494" s="T384">verjagen-PTCP.PRS.[NOM]</ta>
            <ta e="T386" id="Seg_4495" s="T385">ähnlich</ta>
            <ta e="T387" id="Seg_4496" s="T386">Hand-3SG-ACC</ta>
            <ta e="T388" id="Seg_4497" s="T387">ganz.leise-ADVZ</ta>
            <ta e="T389" id="Seg_4498" s="T388">so-INTNS</ta>
            <ta e="T390" id="Seg_4499" s="T389">nehmen-PST1-3SG</ta>
            <ta e="T391" id="Seg_4500" s="T390">dann</ta>
            <ta e="T392" id="Seg_4501" s="T391">dieses</ta>
            <ta e="T393" id="Seg_4502" s="T392">ganz.leise-ADVZ</ta>
            <ta e="T394" id="Seg_4503" s="T393">dieses</ta>
            <ta e="T395" id="Seg_4504" s="T394">so-INTNS</ta>
            <ta e="T396" id="Seg_4505" s="T395">Gesicht-3SG-INSTR</ta>
            <ta e="T397" id="Seg_4506" s="T396">Gesicht-3SG-ACC</ta>
            <ta e="T398" id="Seg_4507" s="T397">doch</ta>
            <ta e="T399" id="Seg_4508" s="T398">wer-VBZ-PRS.[3SG]</ta>
            <ta e="T400" id="Seg_4509" s="T399">zeigen-PRS.[3SG]</ta>
            <ta e="T401" id="Seg_4510" s="T400">Mücke-PL-ABL</ta>
            <ta e="T402" id="Seg_4511" s="T401">treten-EP-MED-PTCP.PRS-DAT/LOC</ta>
            <ta e="T403" id="Seg_4512" s="T402">ähnlich</ta>
            <ta e="T404" id="Seg_4513" s="T403">oh</ta>
            <ta e="T405" id="Seg_4514" s="T404">doch</ta>
            <ta e="T406" id="Seg_4515" s="T405">Junge-1PL.[NOM]</ta>
            <ta e="T407" id="Seg_4516" s="T406">doch</ta>
            <ta e="T408" id="Seg_4517" s="T407">sich.freuen-PRS.[3SG]</ta>
            <ta e="T409" id="Seg_4518" s="T408">EMPH</ta>
            <ta e="T410" id="Seg_4519" s="T409">schnell</ta>
            <ta e="T411" id="Seg_4520" s="T410">sehr-ADVZ</ta>
            <ta e="T412" id="Seg_4521" s="T411">Messer-3SG-ACC</ta>
            <ta e="T413" id="Seg_4522" s="T412">nehmen-PST1-3SG</ta>
            <ta e="T414" id="Seg_4523" s="T413">dann</ta>
            <ta e="T415" id="Seg_4524" s="T414">ganz.leise-ADVZ</ta>
            <ta e="T416" id="Seg_4525" s="T415">heben-PST1-3SG</ta>
            <ta e="T417" id="Seg_4526" s="T416">Knie.[NOM]</ta>
            <ta e="T418" id="Seg_4527" s="T417">Knochen-3SG-ACC</ta>
            <ta e="T419" id="Seg_4528" s="T418">oh</ta>
            <ta e="T420" id="Seg_4529" s="T419">schnell</ta>
            <ta e="T421" id="Seg_4530" s="T420">schnell</ta>
            <ta e="T422" id="Seg_4531" s="T421">sehr-ADVZ</ta>
            <ta e="T423" id="Seg_4532" s="T422">Messer-3SG-INSTR</ta>
            <ta e="T424" id="Seg_4533" s="T423">Sehne.[NOM]</ta>
            <ta e="T425" id="Seg_4534" s="T424">INTNS-ACC</ta>
            <ta e="T426" id="Seg_4535" s="T425">ziehen-CVB.SEQ</ta>
            <ta e="T427" id="Seg_4536" s="T426">nehmen-NEG.[3SG]</ta>
            <ta e="T428" id="Seg_4537" s="T427">Q</ta>
            <ta e="T429" id="Seg_4538" s="T428">dort</ta>
            <ta e="T430" id="Seg_4539" s="T429">Vater-3SG.[NOM]</ta>
            <ta e="T431" id="Seg_4540" s="T430">sagen-PST1-3SG</ta>
            <ta e="T432" id="Seg_4541" s="T431">doch</ta>
            <ta e="T433" id="Seg_4542" s="T432">wie</ta>
            <ta e="T434" id="Seg_4543" s="T433">machen-FUT-2SG=Q</ta>
            <ta e="T435" id="Seg_4544" s="T434">1SG.[NOM]</ta>
            <ta e="T436" id="Seg_4545" s="T435">sagen-PST2-EP-1SG</ta>
            <ta e="T437" id="Seg_4546" s="T436">Wort-1SG-ACC</ta>
            <ta e="T438" id="Seg_4547" s="T437">zurück</ta>
            <ta e="T439" id="Seg_4548" s="T438">nehmen-FUT-1SG</ta>
            <ta e="T440" id="Seg_4549" s="T439">NEG-3SG</ta>
            <ta e="T441" id="Seg_4550" s="T440">solch</ta>
            <ta e="T442" id="Seg_4551" s="T441">Meister.[NOM]</ta>
            <ta e="T443" id="Seg_4552" s="T442">Mensch.[NOM]</ta>
            <ta e="T444" id="Seg_4553" s="T443">solch</ta>
            <ta e="T445" id="Seg_4554" s="T444">Jäger.[NOM]</ta>
            <ta e="T446" id="Seg_4555" s="T445">Mensch.[NOM]</ta>
            <ta e="T447" id="Seg_4556" s="T446">jeder-3SG-ACC</ta>
            <ta e="T448" id="Seg_4557" s="T447">wissen-PRS.[3SG]</ta>
            <ta e="T449" id="Seg_4558" s="T448">doch</ta>
            <ta e="T450" id="Seg_4559" s="T449">geben-PRS-1SG</ta>
            <ta e="T451" id="Seg_4560" s="T450">2SG-DAT/LOC</ta>
            <ta e="T452" id="Seg_4561" s="T451">Jäger.[NOM]</ta>
            <ta e="T453" id="Seg_4562" s="T452">Tochter-1SG-ACC</ta>
            <ta e="T454" id="Seg_4563" s="T453">glücklich-ADVZ</ta>
            <ta e="T455" id="Seg_4564" s="T454">leben-EP-IMP.2PL</ta>
            <ta e="T456" id="Seg_4565" s="T455">gut-ADVZ</ta>
            <ta e="T457" id="Seg_4566" s="T456">leben-EP-IMP.2PL</ta>
            <ta e="T458" id="Seg_4567" s="T457">viel</ta>
            <ta e="T459" id="Seg_4568" s="T458">Kind-ACC</ta>
            <ta e="T460" id="Seg_4569" s="T459">Kind-VBZ-EP-IMP.2PL</ta>
            <ta e="T461" id="Seg_4570" s="T460">sagen-PST1-3SG</ta>
            <ta e="T462" id="Seg_4571" s="T461">doch</ta>
            <ta e="T463" id="Seg_4572" s="T462">jenes</ta>
            <ta e="T464" id="Seg_4573" s="T463">wie-INTNS</ta>
            <ta e="T465" id="Seg_4574" s="T464">Jäger.[NOM]</ta>
            <ta e="T466" id="Seg_4575" s="T465">Mädchen-3SG-ACC</ta>
            <ta e="T467" id="Seg_4576" s="T466">mit</ta>
            <ta e="T468" id="Seg_4577" s="T467">gut</ta>
            <ta e="T469" id="Seg_4578" s="T468">sehr-ADVZ</ta>
            <ta e="T470" id="Seg_4579" s="T469">leben-PST2-3PL</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_4580" s="T0">1SG.[NOM]</ta>
            <ta e="T2" id="Seg_4581" s="T1">этот</ta>
            <ta e="T3" id="Seg_4582" s="T2">рассказ-ACC</ta>
            <ta e="T4" id="Seg_4583" s="T3">слышать-EP-PST2-EP-1SG</ta>
            <ta e="T5" id="Seg_4584" s="T4">родители-PL-1SG-ACC</ta>
            <ta e="T7" id="Seg_4585" s="T5">с</ta>
            <ta e="T8" id="Seg_4586" s="T7">школа-DAT/LOC</ta>
            <ta e="T9" id="Seg_4587" s="T8">входить-PTCP.PRS</ta>
            <ta e="T10" id="Seg_4588" s="T9">год-1SG-DAT/LOC</ta>
            <ta e="T11" id="Seg_4589" s="T10">тот-3SG-2SG.[NOM]</ta>
            <ta e="T12" id="Seg_4590" s="T11">теперь-DAT/LOC</ta>
            <ta e="T13" id="Seg_4591" s="T12">пока</ta>
            <ta e="T14" id="Seg_4592" s="T13">память-1SG-DAT/LOC</ta>
            <ta e="T15" id="Seg_4593" s="T14">есть</ta>
            <ta e="T16" id="Seg_4594" s="T15">так-INTNS</ta>
            <ta e="T17" id="Seg_4595" s="T16">мама-1SG.[NOM]</ta>
            <ta e="T18" id="Seg_4596" s="T17">старшая.сестра-3SG.[NOM]</ta>
            <ta e="T19" id="Seg_4597" s="T18">Клавдия.[NOM]</ta>
            <ta e="T20" id="Seg_4598" s="T19">рассказывать-PTCP.HAB</ta>
            <ta e="T21" id="Seg_4599" s="T20">быть-PST1-3SG</ta>
            <ta e="T22" id="Seg_4600" s="T21">далекий-далекий</ta>
            <ta e="T23" id="Seg_4601" s="T22">тундра-DAT/LOC</ta>
            <ta e="T24" id="Seg_4602" s="T23">родиться-PTCP.PST</ta>
            <ta e="T25" id="Seg_4603" s="T24">место-3SG-DAT/LOC</ta>
            <ta e="T26" id="Seg_4604" s="T25">сам-3SG-GEN</ta>
            <ta e="T27" id="Seg_4605" s="T26">родиться-PTCP.PST</ta>
            <ta e="T28" id="Seg_4606" s="T27">место-3SG-DAT/LOC</ta>
            <ta e="T29" id="Seg_4607" s="T28">семья-PL-3SG-ACC</ta>
            <ta e="T30" id="Seg_4608" s="T29">с</ta>
            <ta e="T31" id="Seg_4609" s="T30">путешествовать-CVB.SIM</ta>
            <ta e="T32" id="Seg_4610" s="T31">идти-EP-PST2-3SG</ta>
            <ta e="T33" id="Seg_4611" s="T32">красивый</ta>
            <ta e="T34" id="Seg_4612" s="T33">очень</ta>
            <ta e="T35" id="Seg_4613" s="T34">мальчик.[NOM]</ta>
            <ta e="T36" id="Seg_4614" s="T35">тот-3SG-2SG.[NOM]</ta>
            <ta e="T37" id="Seg_4615" s="T36">имя-3SG.[NOM]</ta>
            <ta e="T38" id="Seg_4616" s="T37">Охотник.[NOM]</ta>
            <ta e="T39" id="Seg_4617" s="T38">быть-PST1-3SG</ta>
            <ta e="T40" id="Seg_4618" s="T39">другой</ta>
            <ta e="T41" id="Seg_4619" s="T40">место-DAT/LOC</ta>
            <ta e="T44" id="Seg_4620" s="T43">3PL-ABL</ta>
            <ta e="T45" id="Seg_4621" s="T44">далеко-INTNS</ta>
            <ta e="T46" id="Seg_4622" s="T45">прятаться-CVB.SIM</ta>
            <ta e="T47" id="Seg_4623" s="T46">идти-EP-PST2-3SG</ta>
            <ta e="T48" id="Seg_4624" s="T47">красивый</ta>
            <ta e="T49" id="Seg_4625" s="T48">девушка.[NOM]</ta>
            <ta e="T50" id="Seg_4626" s="T49">семья-PL-3SG-ACC</ta>
            <ta e="T51" id="Seg_4627" s="T50">с</ta>
            <ta e="T52" id="Seg_4628" s="T51">тот.[NOM]</ta>
            <ta e="T53" id="Seg_4629" s="T52">молодой</ta>
            <ta e="T54" id="Seg_4630" s="T53">ээ</ta>
            <ta e="T55" id="Seg_4631" s="T54">мальчик-ACC</ta>
            <ta e="T56" id="Seg_4632" s="T55">с</ta>
            <ta e="T57" id="Seg_4633" s="T56">тот.[NOM]</ta>
            <ta e="T58" id="Seg_4634" s="T57">девушка.[NOM]</ta>
            <ta e="T59" id="Seg_4635" s="T58">встречать-PTCP.HAB</ta>
            <ta e="T60" id="Seg_4636" s="T59">быть-PST2-3PL</ta>
            <ta e="T61" id="Seg_4637" s="T60">любить-RECP/COLL-PTCP.HAB</ta>
            <ta e="T62" id="Seg_4638" s="T61">быть-PST2-3PL</ta>
            <ta e="T63" id="Seg_4639" s="T62">сам-3PL-GEN</ta>
            <ta e="T64" id="Seg_4640" s="T63">родиться-PTCP.PST</ta>
            <ta e="T65" id="Seg_4641" s="T64">земля-3PL-DAT/LOC</ta>
            <ta e="T66" id="Seg_4642" s="T65">однажды</ta>
            <ta e="T67" id="Seg_4643" s="T66">девушка-3SG.[NOM]</ta>
            <ta e="T68" id="Seg_4644" s="T67">Охотник-3SG-ACC</ta>
            <ta e="T69" id="Seg_4645" s="T68">мальчик-3SG-ACC</ta>
            <ta e="T70" id="Seg_4646" s="T69">говорить-PRS.[3SG]</ta>
            <ta e="T71" id="Seg_4647" s="T70">доколе</ta>
            <ta e="T72" id="Seg_4648" s="T71">1PL.[NOM]</ta>
            <ta e="T73" id="Seg_4649" s="T72">родители-PL-1PL-ABL</ta>
            <ta e="T74" id="Seg_4650" s="T73">прятаться-CVB.SIM</ta>
            <ta e="T75" id="Seg_4651" s="T74">идти-FUT-1PL=Q</ta>
            <ta e="T76" id="Seg_4652" s="T75">приходить.[IMP.2SG]</ta>
            <ta e="T77" id="Seg_4653" s="T76">1PL-DAT/LOC</ta>
            <ta e="T78" id="Seg_4654" s="T77">посещать-PTCP.PST</ta>
            <ta e="T79" id="Seg_4655" s="T78">быть.[IMP.2SG]</ta>
            <ta e="T80" id="Seg_4656" s="T79">родители-PL-1SG-ACC</ta>
            <ta e="T81" id="Seg_4657" s="T80">с</ta>
            <ta e="T82" id="Seg_4658" s="T81">говорить-EP-RECP/COLL.[IMP.2SG]</ta>
            <ta e="T83" id="Seg_4659" s="T82">отец-1SG-ACC</ta>
            <ta e="T84" id="Seg_4660" s="T83">с</ta>
            <ta e="T85" id="Seg_4661" s="T84">говорить-EP-RECP/COLL.[IMP.2SG]</ta>
            <ta e="T86" id="Seg_4662" s="T85">отец-1SG-ABL</ta>
            <ta e="T87" id="Seg_4663" s="T86">попросить.[IMP.2SG]</ta>
            <ta e="T88" id="Seg_4664" s="T87">1SG-ACC</ta>
            <ta e="T89" id="Seg_4665" s="T88">отец-1SG.[NOM]</ta>
            <ta e="T93" id="Seg_4666" s="T92">хороший</ta>
            <ta e="T94" id="Seg_4667" s="T93">очень</ta>
            <ta e="T95" id="Seg_4668" s="T94">человек.[NOM]</ta>
            <ta e="T96" id="Seg_4669" s="T95">да</ta>
            <ta e="T97" id="Seg_4670" s="T96">быть-COND.[3SG]</ta>
            <ta e="T98" id="Seg_4671" s="T97">хитрость-PROPR.[NOM]</ta>
            <ta e="T99" id="Seg_4672" s="T98">вот</ta>
            <ta e="T100" id="Seg_4673" s="T99">тот</ta>
            <ta e="T101" id="Seg_4674" s="T100">хитрость-3SG-ABL</ta>
            <ta e="T102" id="Seg_4675" s="T101">2SG.[NOM]</ta>
            <ta e="T103" id="Seg_4676" s="T102">опасаться-EP-NEG.[IMP.2SG]</ta>
            <ta e="T104" id="Seg_4677" s="T103">вот</ta>
            <ta e="T105" id="Seg_4678" s="T104">тот.[NOM]</ta>
            <ta e="T106" id="Seg_4679" s="T105">как-INTNS</ta>
            <ta e="T107" id="Seg_4680" s="T106">говорить-PST1-3SG</ta>
            <ta e="T108" id="Seg_4681" s="T107">да</ta>
            <ta e="T109" id="Seg_4682" s="T108">мальчик-1PL.[NOM]</ta>
            <ta e="T110" id="Seg_4683" s="T109">тот.[NOM]</ta>
            <ta e="T111" id="Seg_4684" s="T110">как-INTNS</ta>
            <ta e="T112" id="Seg_4685" s="T111">сколько</ta>
            <ta e="T113" id="Seg_4686" s="T112">INDEF</ta>
            <ta e="T114" id="Seg_4687" s="T113">день.[NOM]</ta>
            <ta e="T115" id="Seg_4688" s="T114">быть-CVB.SEQ</ta>
            <ta e="T116" id="Seg_4689" s="T115">после</ta>
            <ta e="T117" id="Seg_4690" s="T116">теплый</ta>
            <ta e="T118" id="Seg_4691" s="T117">очень</ta>
            <ta e="T119" id="Seg_4692" s="T118">день-DAT/LOC</ta>
            <ta e="T120" id="Seg_4693" s="T119">весной</ta>
            <ta e="T121" id="Seg_4694" s="T120">к.осени</ta>
            <ta e="T122" id="Seg_4695" s="T121">солнце-PROPR</ta>
            <ta e="T123" id="Seg_4696" s="T122">очень</ta>
            <ta e="T124" id="Seg_4697" s="T123">день-DAT/LOC</ta>
            <ta e="T125" id="Seg_4698" s="T124">вот</ta>
            <ta e="T126" id="Seg_4699" s="T125">верховой.олень-VBZ-CVB.SEQ</ta>
            <ta e="T127" id="Seg_4700" s="T126">идти-CVB.SEQ</ta>
            <ta e="T128" id="Seg_4701" s="T127">вот</ta>
            <ta e="T129" id="Seg_4702" s="T128">приходить-PRS.[3SG]</ta>
            <ta e="T130" id="Seg_4703" s="T129">сам-3SG-GEN</ta>
            <ta e="T133" id="Seg_4704" s="T132">белый</ta>
            <ta e="T134" id="Seg_4705" s="T133">ручной.олень-PROPR.[NOM]</ta>
            <ta e="T135" id="Seg_4706" s="T134">тот-3SG-3SG-INSTR</ta>
            <ta e="T136" id="Seg_4707" s="T135">посещать-CVB.SIM</ta>
            <ta e="T137" id="Seg_4708" s="T136">идти-PST1-3SG</ta>
            <ta e="T138" id="Seg_4709" s="T137">девушка-3SG-DAT/LOC</ta>
            <ta e="T139" id="Seg_4710" s="T138">входить-CVB.ANT-3SG</ta>
            <ta e="T140" id="Seg_4711" s="T139">земля-DAT/LOC</ta>
            <ta e="T141" id="Seg_4712" s="T140">пока</ta>
            <ta e="T142" id="Seg_4713" s="T141">голова-3SG-ACC</ta>
            <ta e="T143" id="Seg_4714" s="T142">родители-PL-3SG-DAT/LOC</ta>
            <ta e="T144" id="Seg_4715" s="T143">нагибать-ITER-CVB.SEQ</ta>
            <ta e="T145" id="Seg_4716" s="T144">после</ta>
            <ta e="T146" id="Seg_4717" s="T145">здравствуй-IMP.2PL</ta>
            <ta e="T147" id="Seg_4718" s="T146">здравствуй-IMP.2PL</ta>
            <ta e="T148" id="Seg_4719" s="T147">старый-PL.[NOM]</ta>
            <ta e="T149" id="Seg_4720" s="T148">говорить-PST1-3SG</ta>
            <ta e="T150" id="Seg_4721" s="T149">Охотник.[NOM]</ta>
            <ta e="T151" id="Seg_4722" s="T150">мальчик-1PL.[NOM]</ta>
            <ta e="T152" id="Seg_4723" s="T151">вот</ta>
            <ta e="T153" id="Seg_4724" s="T152">этот</ta>
            <ta e="T154" id="Seg_4725" s="T153">приходить-PST1-1SG</ta>
            <ta e="T155" id="Seg_4726" s="T154">2PL-DAT/LOC</ta>
            <ta e="T157" id="Seg_4727" s="T156">мать.[NOM]</ta>
            <ta e="T158" id="Seg_4728" s="T157">отец.[NOM]</ta>
            <ta e="T159" id="Seg_4729" s="T158">2PL-ABL</ta>
            <ta e="T160" id="Seg_4730" s="T159">дочь-2PL-ACC</ta>
            <ta e="T161" id="Seg_4731" s="T160">попросить-PRS-1SG</ta>
            <ta e="T162" id="Seg_4732" s="T161">что-3SG-ACC</ta>
            <ta e="T163" id="Seg_4733" s="T162">доколе</ta>
            <ta e="T164" id="Seg_4734" s="T163">скрывать-FUT-1PL=Q</ta>
            <ta e="T165" id="Seg_4735" s="T164">1PL.[NOM]</ta>
            <ta e="T166" id="Seg_4736" s="T165">1PL.[NOM]</ta>
            <ta e="T167" id="Seg_4737" s="T166">давно-ADJZ-ABL</ta>
            <ta e="T168" id="Seg_4738" s="T167">любить-RECP/COLL-PRS-1PL</ta>
            <ta e="T169" id="Seg_4739" s="T168">сам-сам-1PL-ACC</ta>
            <ta e="T170" id="Seg_4740" s="T169">очень</ta>
            <ta e="T171" id="Seg_4741" s="T170">уважать-PRS-1PL</ta>
            <ta e="T172" id="Seg_4742" s="T171">вот</ta>
            <ta e="T173" id="Seg_4743" s="T172">как</ta>
            <ta e="T174" id="Seg_4744" s="T173">делать-FUT-2SG=Q</ta>
            <ta e="T175" id="Seg_4745" s="T174">говорить-PST1-3SG</ta>
            <ta e="T176" id="Seg_4746" s="T175">мерцать-CVB.SIM-мерцать-CVB.SIM</ta>
            <ta e="T177" id="Seg_4747" s="T176">девушка.[NOM]</ta>
            <ta e="T178" id="Seg_4748" s="T177">отец-3SG.[NOM]</ta>
            <ta e="T179" id="Seg_4749" s="T178">потом</ta>
            <ta e="T180" id="Seg_4750" s="T179">вот</ta>
            <ta e="T181" id="Seg_4751" s="T180">хитрость-REFL/MED-CVB.PURP</ta>
            <ta e="T182" id="Seg_4752" s="T181">хотеть-PST1-3SG</ta>
            <ta e="T183" id="Seg_4753" s="T182">наверное</ta>
            <ta e="T184" id="Seg_4754" s="T183">думать-CVB.SIM</ta>
            <ta e="T185" id="Seg_4755" s="T184">думать-PST1-3SG</ta>
            <ta e="T186" id="Seg_4756" s="T185">мальчик-1PL.[NOM]</ta>
            <ta e="T187" id="Seg_4757" s="T186">правда.[NOM]</ta>
            <ta e="T188" id="Seg_4758" s="T187">быть-PST2.[3SG]</ta>
            <ta e="T189" id="Seg_4759" s="T188">девушка-1PL.[NOM]</ta>
            <ta e="T190" id="Seg_4760" s="T189">отец-3SG.[NOM]</ta>
            <ta e="T191" id="Seg_4761" s="T190">что-ACC</ta>
            <ta e="T192" id="Seg_4762" s="T191">только</ta>
            <ta e="T193" id="Seg_4763" s="T192">бросать-PST1-3SG</ta>
            <ta e="T194" id="Seg_4764" s="T193">EVID</ta>
            <ta e="T195" id="Seg_4765" s="T194">чык</ta>
            <ta e="T196" id="Seg_4766" s="T195">делать-CVB.SEQ</ta>
            <ta e="T197" id="Seg_4767" s="T196">бросать-PST1-3SG</ta>
            <ta e="T198" id="Seg_4768" s="T197">этот</ta>
            <ta e="T199" id="Seg_4769" s="T198">очаг.[NOM]</ta>
            <ta e="T200" id="Seg_4770" s="T199">верхняя.часть-3SG-INSTR</ta>
            <ta e="T201" id="Seg_4771" s="T200">думать-CVB.SIM</ta>
            <ta e="T202" id="Seg_4772" s="T201">думать-PST1-3SG</ta>
            <ta e="T203" id="Seg_4773" s="T202">потом</ta>
            <ta e="T204" id="Seg_4774" s="T203">держать-FUT.[3SG]</ta>
            <ta e="T205" id="Seg_4775" s="T204">Q</ta>
            <ta e="T206" id="Seg_4776" s="T205">держать-FUT.[3SG]</ta>
            <ta e="T207" id="Seg_4777" s="T206">Q</ta>
            <ta e="T208" id="Seg_4778" s="T207">NEG-3SG</ta>
            <ta e="T209" id="Seg_4779" s="T208">Q</ta>
            <ta e="T210" id="Seg_4780" s="T209">этот</ta>
            <ta e="T211" id="Seg_4781" s="T210">кость-ACC</ta>
            <ta e="T212" id="Seg_4782" s="T211">вот</ta>
            <ta e="T213" id="Seg_4783" s="T212">мальчик-1PL.[NOM]</ta>
            <ta e="T214" id="Seg_4784" s="T213">вот</ta>
            <ta e="T215" id="Seg_4785" s="T214">быстрый</ta>
            <ta e="T216" id="Seg_4786" s="T215">очень-ADVZ</ta>
            <ta e="T217" id="Seg_4787" s="T216">хватать-PRS.[3SG]</ta>
            <ta e="T218" id="Seg_4788" s="T217">EMPH</ta>
            <ta e="T219" id="Seg_4789" s="T218">тот-ACC</ta>
            <ta e="T220" id="Seg_4790" s="T219">спрашивать-PRS.[3SG]</ta>
            <ta e="T221" id="Seg_4791" s="T220">девушка-1PL.[NOM]</ta>
            <ta e="T222" id="Seg_4792" s="T221">отец-3SG.[NOM]</ta>
            <ta e="T223" id="Seg_4793" s="T222">вот</ta>
            <ta e="T224" id="Seg_4794" s="T223">что-ACC</ta>
            <ta e="T225" id="Seg_4795" s="T224">хватать-PST1-2SG</ta>
            <ta e="T226" id="Seg_4796" s="T225">тот</ta>
            <ta e="T227" id="Seg_4797" s="T226">как</ta>
            <ta e="T228" id="Seg_4798" s="T227">что-ACC</ta>
            <ta e="T229" id="Seg_4799" s="T228">говорить-CVB.SIM</ta>
            <ta e="T230" id="Seg_4800" s="T229">спрашивать-PST1-3SG</ta>
            <ta e="T231" id="Seg_4801" s="T230">тот</ta>
            <ta e="T232" id="Seg_4802" s="T231">вот</ta>
            <ta e="T233" id="Seg_4803" s="T232">кто.[NOM]</ta>
            <ta e="T234" id="Seg_4804" s="T233">мальчик-1PL.[NOM]</ta>
            <ta e="T235" id="Seg_4805" s="T234">1SG.[NOM]</ta>
            <ta e="T238" id="Seg_4806" s="T237">1SG.[NOM]</ta>
            <ta e="T239" id="Seg_4807" s="T238">видеть-PST1-1SG</ta>
            <ta e="T240" id="Seg_4808" s="T239">узнавать-PST1-1SG</ta>
            <ta e="T241" id="Seg_4809" s="T240">колено.[NOM]</ta>
            <ta e="T242" id="Seg_4810" s="T241">кость-3SG.[NOM]</ta>
            <ta e="T243" id="Seg_4811" s="T242">кто.[NOM]</ta>
            <ta e="T244" id="Seg_4812" s="T243">узнавать-NEG.[3SG]</ta>
            <ta e="T245" id="Seg_4813" s="T244">тот</ta>
            <ta e="T246" id="Seg_4814" s="T245">колено.[NOM]</ta>
            <ta e="T247" id="Seg_4815" s="T246">кость-3SG-ACC</ta>
            <ta e="T248" id="Seg_4816" s="T247">кто.[NOM]</ta>
            <ta e="T249" id="Seg_4817" s="T248">узнавать-EP-NEG-FUT.[3SG]=Q</ta>
            <ta e="T250" id="Seg_4818" s="T249">тот</ta>
            <ta e="T251" id="Seg_4819" s="T250">колено.[NOM]</ta>
            <ta e="T252" id="Seg_4820" s="T251">кость-3SG-ACC</ta>
            <ta e="T253" id="Seg_4821" s="T252">говорить-PST1-3SG</ta>
            <ta e="T254" id="Seg_4822" s="T253">отец-1PL.[NOM]</ta>
            <ta e="T257" id="Seg_4823" s="T256">хитрость-VBZ-CVB.SIM-хитрость-VBZ-CVB.SIM</ta>
            <ta e="T258" id="Seg_4824" s="T257">улыбаться-CVB.SIM-улыбаться-CVB.SIM</ta>
            <ta e="T259" id="Seg_4825" s="T258">говорить-PST1-3SG</ta>
            <ta e="T260" id="Seg_4826" s="T259">вот</ta>
            <ta e="T261" id="Seg_4827" s="T260">догадываться-PST1-2SG</ta>
            <ta e="T262" id="Seg_4828" s="T261">колено.[NOM]</ta>
            <ta e="T263" id="Seg_4829" s="T262">кость-3SG-ACC</ta>
            <ta e="T264" id="Seg_4830" s="T263">хороший.[NOM]</ta>
            <ta e="T265" id="Seg_4831" s="T264">вот</ta>
            <ta e="T266" id="Seg_4832" s="T265">этот</ta>
            <ta e="T269" id="Seg_4833" s="T268">колено.[NOM]</ta>
            <ta e="T270" id="Seg_4834" s="T269">кость-3SG-DAT/LOC</ta>
            <ta e="T271" id="Seg_4835" s="T270">откуда</ta>
            <ta e="T272" id="Seg_4836" s="T271">INDEF</ta>
            <ta e="T275" id="Seg_4837" s="T274">видеть-CVB.SEQ-2SG</ta>
            <ta e="T276" id="Seg_4838" s="T275">один</ta>
            <ta e="T277" id="Seg_4839" s="T276">INDEF</ta>
            <ta e="T278" id="Seg_4840" s="T277">сухожилие-ACC</ta>
            <ta e="T279" id="Seg_4841" s="T278">найти-FUT-2SG</ta>
            <ta e="T280" id="Seg_4842" s="T279">Q</ta>
            <ta e="T281" id="Seg_4843" s="T280">тогда</ta>
            <ta e="T282" id="Seg_4844" s="T281">1SG.[NOM]</ta>
            <ta e="T283" id="Seg_4845" s="T282">слово-1SG-ACC</ta>
            <ta e="T284" id="Seg_4846" s="T283">взять-FUT-1SG</ta>
            <ta e="T285" id="Seg_4847" s="T284">NEG-3SG</ta>
            <ta e="T286" id="Seg_4848" s="T285">назад</ta>
            <ta e="T287" id="Seg_4849" s="T286">найти-TEMP-2SG</ta>
            <ta e="T288" id="Seg_4850" s="T287">один</ta>
            <ta e="T289" id="Seg_4851" s="T288">INDEF</ta>
            <ta e="T290" id="Seg_4852" s="T289">сухожилие-ACC</ta>
            <ta e="T291" id="Seg_4853" s="T290">тогда</ta>
            <ta e="T292" id="Seg_4854" s="T291">1SG.[NOM]</ta>
            <ta e="T293" id="Seg_4855" s="T292">дочь-1SG-ACC</ta>
            <ta e="T294" id="Seg_4856" s="T293">2SG-DAT/LOC</ta>
            <ta e="T295" id="Seg_4857" s="T294">давать-FUT-1SG</ta>
            <ta e="T296" id="Seg_4858" s="T295">вот</ta>
            <ta e="T297" id="Seg_4859" s="T296">мальчик-1PL.[NOM]</ta>
            <ta e="T298" id="Seg_4860" s="T297">видеть-CVB.SIM</ta>
            <ta e="T299" id="Seg_4861" s="T298">пытаться-PST1-3SG</ta>
            <ta e="T300" id="Seg_4862" s="T299">видеть-CVB.SIM</ta>
            <ta e="T301" id="Seg_4863" s="T300">пытаться-PST1-3SG</ta>
            <ta e="T302" id="Seg_4864" s="T301">потом</ta>
            <ta e="T303" id="Seg_4865" s="T302">потихоньку-ADVZ</ta>
            <ta e="T304" id="Seg_4866" s="T303">сам-3SG.[NOM]</ta>
            <ta e="T305" id="Seg_4867" s="T304">сам-3SG-DAT/LOC</ta>
            <ta e="T306" id="Seg_4868" s="T305">говорить-PRS.[3SG]</ta>
            <ta e="T307" id="Seg_4869" s="T306">о</ta>
            <ta e="T308" id="Seg_4870" s="T307">вот</ta>
            <ta e="T309" id="Seg_4871" s="T308">очень</ta>
            <ta e="T310" id="Seg_4872" s="T309">быть-PST2.[3SG]</ta>
            <ta e="T311" id="Seg_4873" s="T310">вот</ta>
            <ta e="T312" id="Seg_4874" s="T311">вправду</ta>
            <ta e="T313" id="Seg_4875" s="T312">камень-DAT/LOC</ta>
            <ta e="T314" id="Seg_4876" s="T313">подобно</ta>
            <ta e="T315" id="Seg_4877" s="T314">что.[NOM]</ta>
            <ta e="T316" id="Seg_4878" s="T315">красивый</ta>
            <ta e="T317" id="Seg_4879" s="T316">кость-3SG.[NOM]=Q</ta>
            <ta e="T318" id="Seg_4880" s="T317">ребенок-PL.[NOM]</ta>
            <ta e="T319" id="Seg_4881" s="T318">тот-3SG-2SG.[NOM]</ta>
            <ta e="T320" id="Seg_4882" s="T319">эй</ta>
            <ta e="T321" id="Seg_4883" s="T320">знать-NEG.PTCP</ta>
            <ta e="T322" id="Seg_4884" s="T321">быть-PST2.[3SG]</ta>
            <ta e="T323" id="Seg_4885" s="T322">3SG.[NOM]</ta>
            <ta e="T324" id="Seg_4886" s="T323">3SG.[NOM]</ta>
            <ta e="T325" id="Seg_4887" s="T324">тот.[NOM]</ta>
            <ta e="T326" id="Seg_4888" s="T325">отец-3PL.[NOM]</ta>
            <ta e="T327" id="Seg_4889" s="T326">только</ta>
            <ta e="T328" id="Seg_4890" s="T327">тот.[NOM]</ta>
            <ta e="T329" id="Seg_4891" s="T328">знать-PRS.[3SG]</ta>
            <ta e="T330" id="Seg_4892" s="T329">один</ta>
            <ta e="T331" id="Seg_4893" s="T330">место-3SG.[NOM]</ta>
            <ta e="T332" id="Seg_4894" s="T331">тот-3SG-2SG.[NOM]</ta>
            <ta e="T333" id="Seg_4895" s="T332">вообще</ta>
            <ta e="T334" id="Seg_4896" s="T333">один</ta>
            <ta e="T335" id="Seg_4897" s="T334">NEG</ta>
            <ta e="T336" id="Seg_4898" s="T335">сухожилие-POSS</ta>
            <ta e="T337" id="Seg_4899" s="T336">NEG.[3SG]</ta>
            <ta e="T338" id="Seg_4900" s="T337">один-2SG-DAT/LOC</ta>
            <ta e="T339" id="Seg_4901" s="T338">место-3SG-ACC</ta>
            <ta e="T340" id="Seg_4902" s="T339">видеть-FUT-2SG</ta>
            <ta e="T341" id="Seg_4903" s="T340">хороший-INTNS-ADVZ</ta>
            <ta e="T342" id="Seg_4904" s="T341">видеть-PTCP.COND-DAT/LOC</ta>
            <ta e="T343" id="Seg_4905" s="T342">тот-3SG-2SG.[NOM]</ta>
            <ta e="T344" id="Seg_4906" s="T343">девушка.[NOM]</ta>
            <ta e="T347" id="Seg_4907" s="T346">лицо-3SG-DAT/LOC</ta>
            <ta e="T348" id="Seg_4908" s="T347">подобно</ta>
            <ta e="T349" id="Seg_4909" s="T348">жена.[NOM]</ta>
            <ta e="T350" id="Seg_4910" s="T349">лицо-3SG-DAT/LOC</ta>
            <ta e="T351" id="Seg_4911" s="T350">подобно</ta>
            <ta e="T352" id="Seg_4912" s="T351">один.из.двух</ta>
            <ta e="T353" id="Seg_4913" s="T352">сторона-3SG-DAT/LOC</ta>
            <ta e="T354" id="Seg_4914" s="T353">тот-3SG-2SG-ACC</ta>
            <ta e="T355" id="Seg_4915" s="T354">отец-1PL.[NOM]</ta>
            <ta e="T356" id="Seg_4916" s="T355">только</ta>
            <ta e="T357" id="Seg_4917" s="T356">знать-PRS.[3SG]</ta>
            <ta e="T358" id="Seg_4918" s="T357">EMPH</ta>
            <ta e="T359" id="Seg_4919" s="T358">вот</ta>
            <ta e="T360" id="Seg_4920" s="T359">куда</ta>
            <ta e="T361" id="Seg_4921" s="T360">вот</ta>
            <ta e="T362" id="Seg_4922" s="T361">становиться-PTCP.FUT-3SG-ACC</ta>
            <ta e="T363" id="Seg_4923" s="T362">очень</ta>
            <ta e="T364" id="Seg_4924" s="T363">Охотник.[NOM]</ta>
            <ta e="T365" id="Seg_4925" s="T364">мальчик-1PL.[NOM]</ta>
            <ta e="T366" id="Seg_4926" s="T365">рассердиться-CVB.PURP</ta>
            <ta e="T367" id="Seg_4927" s="T366">да</ta>
            <ta e="T368" id="Seg_4928" s="T367">хотеть-PRS.[3SG]</ta>
            <ta e="T369" id="Seg_4929" s="T368">опасаться-PRS.[3SG]</ta>
            <ta e="T370" id="Seg_4930" s="T369">потом</ta>
            <ta e="T371" id="Seg_4931" s="T370">вот</ta>
            <ta e="T372" id="Seg_4932" s="T371">глянуть-NMNZ.[NOM]</ta>
            <ta e="T373" id="Seg_4933" s="T372">делать-PRS.[3SG]</ta>
            <ta e="T374" id="Seg_4934" s="T373">EMPH</ta>
            <ta e="T375" id="Seg_4935" s="T374">дочь-3SG-ACC</ta>
            <ta e="T376" id="Seg_4936" s="T375">к</ta>
            <ta e="T377" id="Seg_4937" s="T376">девушка-3SG.[NOM]</ta>
            <ta e="T378" id="Seg_4938" s="T377">хитрость-VBZ-PST1-3SG</ta>
            <ta e="T379" id="Seg_4939" s="T378">тоже</ta>
            <ta e="T380" id="Seg_4940" s="T379">как</ta>
            <ta e="T381" id="Seg_4941" s="T380">делать-FUT-1SG=Q</ta>
            <ta e="T382" id="Seg_4942" s="T381">думать-CVB.SEQ</ta>
            <ta e="T383" id="Seg_4943" s="T382">после</ta>
            <ta e="T384" id="Seg_4944" s="T383">комар-PL-ABL</ta>
            <ta e="T385" id="Seg_4945" s="T384">отмахиваться-PTCP.PRS.[NOM]</ta>
            <ta e="T386" id="Seg_4946" s="T385">подобно</ta>
            <ta e="T387" id="Seg_4947" s="T386">рука-3SG-ACC</ta>
            <ta e="T388" id="Seg_4948" s="T387">потихоньку-ADVZ</ta>
            <ta e="T389" id="Seg_4949" s="T388">так-INTNS</ta>
            <ta e="T390" id="Seg_4950" s="T389">взять-PST1-3SG</ta>
            <ta e="T391" id="Seg_4951" s="T390">потом</ta>
            <ta e="T392" id="Seg_4952" s="T391">этот</ta>
            <ta e="T393" id="Seg_4953" s="T392">потихоньку-ADVZ</ta>
            <ta e="T394" id="Seg_4954" s="T393">этот</ta>
            <ta e="T395" id="Seg_4955" s="T394">так-INTNS</ta>
            <ta e="T396" id="Seg_4956" s="T395">лицо-3SG-INSTR</ta>
            <ta e="T397" id="Seg_4957" s="T396">лицо-3SG-ACC</ta>
            <ta e="T398" id="Seg_4958" s="T397">вот</ta>
            <ta e="T399" id="Seg_4959" s="T398">кто-VBZ-PRS.[3SG]</ta>
            <ta e="T400" id="Seg_4960" s="T399">показывать-PRS.[3SG]</ta>
            <ta e="T401" id="Seg_4961" s="T400">комар-PL-ABL</ta>
            <ta e="T402" id="Seg_4962" s="T401">пнуть-EP-MED-PTCP.PRS-DAT/LOC</ta>
            <ta e="T403" id="Seg_4963" s="T402">подобно</ta>
            <ta e="T404" id="Seg_4964" s="T403">о</ta>
            <ta e="T405" id="Seg_4965" s="T404">вот</ta>
            <ta e="T406" id="Seg_4966" s="T405">мальчик-1PL.[NOM]</ta>
            <ta e="T407" id="Seg_4967" s="T406">вот</ta>
            <ta e="T408" id="Seg_4968" s="T407">радоваться-PRS.[3SG]</ta>
            <ta e="T409" id="Seg_4969" s="T408">EMPH</ta>
            <ta e="T410" id="Seg_4970" s="T409">быстрый</ta>
            <ta e="T411" id="Seg_4971" s="T410">очень-ADVZ</ta>
            <ta e="T412" id="Seg_4972" s="T411">нож-3SG-ACC</ta>
            <ta e="T413" id="Seg_4973" s="T412">взять-PST1-3SG</ta>
            <ta e="T414" id="Seg_4974" s="T413">потом</ta>
            <ta e="T415" id="Seg_4975" s="T414">потихоньку-ADVZ</ta>
            <ta e="T416" id="Seg_4976" s="T415">поднимать-PST1-3SG</ta>
            <ta e="T417" id="Seg_4977" s="T416">колено.[NOM]</ta>
            <ta e="T418" id="Seg_4978" s="T417">кость-3SG-ACC</ta>
            <ta e="T419" id="Seg_4979" s="T418">ой</ta>
            <ta e="T420" id="Seg_4980" s="T419">быстрый</ta>
            <ta e="T421" id="Seg_4981" s="T420">быстрый</ta>
            <ta e="T422" id="Seg_4982" s="T421">очень-ADVZ</ta>
            <ta e="T423" id="Seg_4983" s="T422">нож-3SG-INSTR</ta>
            <ta e="T424" id="Seg_4984" s="T423">сухожилие.[NOM]</ta>
            <ta e="T425" id="Seg_4985" s="T424">INTNS-ACC</ta>
            <ta e="T426" id="Seg_4986" s="T425">тянуть-CVB.SEQ</ta>
            <ta e="T427" id="Seg_4987" s="T426">взять-NEG.[3SG]</ta>
            <ta e="T428" id="Seg_4988" s="T427">Q</ta>
            <ta e="T429" id="Seg_4989" s="T428">там</ta>
            <ta e="T430" id="Seg_4990" s="T429">отец-3SG.[NOM]</ta>
            <ta e="T431" id="Seg_4991" s="T430">говорить-PST1-3SG</ta>
            <ta e="T432" id="Seg_4992" s="T431">вот</ta>
            <ta e="T433" id="Seg_4993" s="T432">как</ta>
            <ta e="T434" id="Seg_4994" s="T433">делать-FUT-2SG=Q</ta>
            <ta e="T435" id="Seg_4995" s="T434">1SG.[NOM]</ta>
            <ta e="T436" id="Seg_4996" s="T435">говорить-PST2-EP-1SG</ta>
            <ta e="T437" id="Seg_4997" s="T436">слово-1SG-ACC</ta>
            <ta e="T438" id="Seg_4998" s="T437">назад</ta>
            <ta e="T439" id="Seg_4999" s="T438">взять-FUT-1SG</ta>
            <ta e="T440" id="Seg_5000" s="T439">NEG-3SG</ta>
            <ta e="T441" id="Seg_5001" s="T440">такой</ta>
            <ta e="T442" id="Seg_5002" s="T441">мастер.[NOM]</ta>
            <ta e="T443" id="Seg_5003" s="T442">человек.[NOM]</ta>
            <ta e="T444" id="Seg_5004" s="T443">такой</ta>
            <ta e="T445" id="Seg_5005" s="T444">охотник.[NOM]</ta>
            <ta e="T446" id="Seg_5006" s="T445">человек.[NOM]</ta>
            <ta e="T447" id="Seg_5007" s="T446">каждый-3SG-ACC</ta>
            <ta e="T448" id="Seg_5008" s="T447">знать-PRS.[3SG]</ta>
            <ta e="T449" id="Seg_5009" s="T448">вот</ta>
            <ta e="T450" id="Seg_5010" s="T449">давать-PRS-1SG</ta>
            <ta e="T451" id="Seg_5011" s="T450">2SG-DAT/LOC</ta>
            <ta e="T452" id="Seg_5012" s="T451">Охотник.[NOM]</ta>
            <ta e="T453" id="Seg_5013" s="T452">дочь-1SG-ACC</ta>
            <ta e="T454" id="Seg_5014" s="T453">счастливый-ADVZ</ta>
            <ta e="T455" id="Seg_5015" s="T454">жить-EP-IMP.2PL</ta>
            <ta e="T456" id="Seg_5016" s="T455">хороший-ADVZ</ta>
            <ta e="T457" id="Seg_5017" s="T456">жить-EP-IMP.2PL</ta>
            <ta e="T458" id="Seg_5018" s="T457">много</ta>
            <ta e="T459" id="Seg_5019" s="T458">ребенок-ACC</ta>
            <ta e="T460" id="Seg_5020" s="T459">ребенок-VBZ-EP-IMP.2PL</ta>
            <ta e="T461" id="Seg_5021" s="T460">говорить-PST1-3SG</ta>
            <ta e="T462" id="Seg_5022" s="T461">вот</ta>
            <ta e="T463" id="Seg_5023" s="T462">тот</ta>
            <ta e="T464" id="Seg_5024" s="T463">как-INTNS</ta>
            <ta e="T465" id="Seg_5025" s="T464">Охотник.[NOM]</ta>
            <ta e="T466" id="Seg_5026" s="T465">девушка-3SG-ACC</ta>
            <ta e="T467" id="Seg_5027" s="T466">с</ta>
            <ta e="T468" id="Seg_5028" s="T467">хороший</ta>
            <ta e="T469" id="Seg_5029" s="T468">очень-ADVZ</ta>
            <ta e="T470" id="Seg_5030" s="T469">жить-PST2-3PL</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_5031" s="T0">pers-pro:case</ta>
            <ta e="T2" id="Seg_5032" s="T1">dempro</ta>
            <ta e="T3" id="Seg_5033" s="T2">n-n:case</ta>
            <ta e="T4" id="Seg_5034" s="T3">v-v:(ins)-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T5" id="Seg_5035" s="T4">n-n:(num)-n:poss-n:case</ta>
            <ta e="T7" id="Seg_5036" s="T5">post</ta>
            <ta e="T8" id="Seg_5037" s="T7">n-n:case</ta>
            <ta e="T9" id="Seg_5038" s="T8">v-v:ptcp</ta>
            <ta e="T10" id="Seg_5039" s="T9">n-n:poss-n:case</ta>
            <ta e="T11" id="Seg_5040" s="T10">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T12" id="Seg_5041" s="T11">adv-n:case</ta>
            <ta e="T13" id="Seg_5042" s="T12">post</ta>
            <ta e="T14" id="Seg_5043" s="T13">n-n:poss-n:case</ta>
            <ta e="T15" id="Seg_5044" s="T14">ptcl</ta>
            <ta e="T16" id="Seg_5045" s="T15">adv-adv&gt;adv</ta>
            <ta e="T17" id="Seg_5046" s="T16">n-n:(poss)-n:case</ta>
            <ta e="T18" id="Seg_5047" s="T17">n-n:(poss)-n:case</ta>
            <ta e="T19" id="Seg_5048" s="T18">propr-n:case</ta>
            <ta e="T20" id="Seg_5049" s="T19">v-v:ptcp</ta>
            <ta e="T21" id="Seg_5050" s="T20">v-v:tense-v:poss.pn</ta>
            <ta e="T22" id="Seg_5051" s="T21">adj-adj</ta>
            <ta e="T23" id="Seg_5052" s="T22">n-n:case</ta>
            <ta e="T24" id="Seg_5053" s="T23">v-v:ptcp</ta>
            <ta e="T25" id="Seg_5054" s="T24">n-n:poss-n:case</ta>
            <ta e="T26" id="Seg_5055" s="T25">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T27" id="Seg_5056" s="T26">v-v:ptcp</ta>
            <ta e="T28" id="Seg_5057" s="T27">n-n:poss-n:case</ta>
            <ta e="T29" id="Seg_5058" s="T28">n-n:(num)-n:poss-n:case</ta>
            <ta e="T30" id="Seg_5059" s="T29">post</ta>
            <ta e="T31" id="Seg_5060" s="T30">v-v:cvb</ta>
            <ta e="T32" id="Seg_5061" s="T31">v-v:(ins)-v:tense-v:poss.pn</ta>
            <ta e="T33" id="Seg_5062" s="T32">adj</ta>
            <ta e="T34" id="Seg_5063" s="T33">ptcl</ta>
            <ta e="T35" id="Seg_5064" s="T34">n-n:case</ta>
            <ta e="T36" id="Seg_5065" s="T35">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T37" id="Seg_5066" s="T36">n-n:(poss)-n:case</ta>
            <ta e="T38" id="Seg_5067" s="T37">propr-n:case</ta>
            <ta e="T39" id="Seg_5068" s="T38">v-v:tense-v:poss.pn</ta>
            <ta e="T40" id="Seg_5069" s="T39">adj</ta>
            <ta e="T41" id="Seg_5070" s="T40">n-n:case</ta>
            <ta e="T44" id="Seg_5071" s="T43">pers-pro:case</ta>
            <ta e="T45" id="Seg_5072" s="T44">adv-adv&gt;adv</ta>
            <ta e="T46" id="Seg_5073" s="T45">v-v:cvb</ta>
            <ta e="T47" id="Seg_5074" s="T46">v-v:(ins)-v:tense-v:poss.pn</ta>
            <ta e="T48" id="Seg_5075" s="T47">adj</ta>
            <ta e="T49" id="Seg_5076" s="T48">n-n:case</ta>
            <ta e="T50" id="Seg_5077" s="T49">n-n:(num)-n:poss-n:case</ta>
            <ta e="T51" id="Seg_5078" s="T50">post</ta>
            <ta e="T52" id="Seg_5079" s="T51">dempro-pro:case</ta>
            <ta e="T53" id="Seg_5080" s="T52">adj</ta>
            <ta e="T54" id="Seg_5081" s="T53">interj</ta>
            <ta e="T55" id="Seg_5082" s="T54">n-n:case</ta>
            <ta e="T56" id="Seg_5083" s="T55">post</ta>
            <ta e="T57" id="Seg_5084" s="T56">dempro-pro:case</ta>
            <ta e="T58" id="Seg_5085" s="T57">n-n:case</ta>
            <ta e="T59" id="Seg_5086" s="T58">v-v:ptcp</ta>
            <ta e="T60" id="Seg_5087" s="T59">v-v:tense-v:pred.pn</ta>
            <ta e="T61" id="Seg_5088" s="T60">v-v&gt;v-v:ptcp</ta>
            <ta e="T62" id="Seg_5089" s="T61">v-v:tense-v:pred.pn</ta>
            <ta e="T63" id="Seg_5090" s="T62">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T64" id="Seg_5091" s="T63">v-v:ptcp</ta>
            <ta e="T65" id="Seg_5092" s="T64">n-n:poss-n:case</ta>
            <ta e="T66" id="Seg_5093" s="T65">adv</ta>
            <ta e="T67" id="Seg_5094" s="T66">n-n:(poss)-n:case</ta>
            <ta e="T68" id="Seg_5095" s="T67">propr-n:poss-n:case</ta>
            <ta e="T69" id="Seg_5096" s="T68">n-n:poss-n:case</ta>
            <ta e="T70" id="Seg_5097" s="T69">v-v:tense-v:pred.pn</ta>
            <ta e="T71" id="Seg_5098" s="T70">adv</ta>
            <ta e="T72" id="Seg_5099" s="T71">pers-pro:case</ta>
            <ta e="T73" id="Seg_5100" s="T72">n-n:(num)-n:poss-n:case</ta>
            <ta e="T74" id="Seg_5101" s="T73">v-v:cvb</ta>
            <ta e="T75" id="Seg_5102" s="T74">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T76" id="Seg_5103" s="T75">v-v:mood.pn</ta>
            <ta e="T77" id="Seg_5104" s="T76">pers-pro:case</ta>
            <ta e="T78" id="Seg_5105" s="T77">v-v:ptcp</ta>
            <ta e="T79" id="Seg_5106" s="T78">v-v:mood.pn</ta>
            <ta e="T80" id="Seg_5107" s="T79">n-n:(num)-n:poss-n:case</ta>
            <ta e="T81" id="Seg_5108" s="T80">post</ta>
            <ta e="T82" id="Seg_5109" s="T81">v-v:(ins)-v&gt;v-v:mood.pn</ta>
            <ta e="T83" id="Seg_5110" s="T82">n-n:poss-n:case</ta>
            <ta e="T84" id="Seg_5111" s="T83">post</ta>
            <ta e="T85" id="Seg_5112" s="T84">v-v:(ins)-v&gt;v-v:mood.pn</ta>
            <ta e="T86" id="Seg_5113" s="T85">n-n:poss-n:case</ta>
            <ta e="T87" id="Seg_5114" s="T86">v-v:mood.pn</ta>
            <ta e="T88" id="Seg_5115" s="T87">pers-pro:case</ta>
            <ta e="T89" id="Seg_5116" s="T88">n-n:(poss)-n:case</ta>
            <ta e="T93" id="Seg_5117" s="T92">adj</ta>
            <ta e="T94" id="Seg_5118" s="T93">ptcl</ta>
            <ta e="T95" id="Seg_5119" s="T94">n-n:case</ta>
            <ta e="T96" id="Seg_5120" s="T95">conj</ta>
            <ta e="T97" id="Seg_5121" s="T96">v-v:mood-v:pred.pn</ta>
            <ta e="T98" id="Seg_5122" s="T97">n-n&gt;adj-n:case</ta>
            <ta e="T99" id="Seg_5123" s="T98">ptcl</ta>
            <ta e="T100" id="Seg_5124" s="T99">dempro</ta>
            <ta e="T101" id="Seg_5125" s="T100">n-n:poss-n:case</ta>
            <ta e="T102" id="Seg_5126" s="T101">pers-pro:case</ta>
            <ta e="T103" id="Seg_5127" s="T102">v-v:(ins)-v:(neg)-v:mood.pn</ta>
            <ta e="T104" id="Seg_5128" s="T103">ptcl</ta>
            <ta e="T105" id="Seg_5129" s="T104">dempro-pro:case</ta>
            <ta e="T106" id="Seg_5130" s="T105">post-post&gt;post</ta>
            <ta e="T107" id="Seg_5131" s="T106">v-v:tense-v:poss.pn</ta>
            <ta e="T108" id="Seg_5132" s="T107">conj</ta>
            <ta e="T109" id="Seg_5133" s="T108">n-n:(poss)-n:case</ta>
            <ta e="T110" id="Seg_5134" s="T109">dempro-pro:case</ta>
            <ta e="T111" id="Seg_5135" s="T110">post-post&gt;post</ta>
            <ta e="T112" id="Seg_5136" s="T111">que</ta>
            <ta e="T113" id="Seg_5137" s="T112">ptcl</ta>
            <ta e="T114" id="Seg_5138" s="T113">n-n:case</ta>
            <ta e="T115" id="Seg_5139" s="T114">v-v:cvb</ta>
            <ta e="T116" id="Seg_5140" s="T115">post</ta>
            <ta e="T117" id="Seg_5141" s="T116">adj</ta>
            <ta e="T118" id="Seg_5142" s="T117">ptcl</ta>
            <ta e="T119" id="Seg_5143" s="T118">n-n:case</ta>
            <ta e="T120" id="Seg_5144" s="T119">adv</ta>
            <ta e="T121" id="Seg_5145" s="T120">adv</ta>
            <ta e="T122" id="Seg_5146" s="T121">n-n&gt;adj</ta>
            <ta e="T123" id="Seg_5147" s="T122">ptcl</ta>
            <ta e="T124" id="Seg_5148" s="T123">n-n:case</ta>
            <ta e="T125" id="Seg_5149" s="T124">ptcl</ta>
            <ta e="T126" id="Seg_5150" s="T125">n-n&gt;v-v:cvb</ta>
            <ta e="T127" id="Seg_5151" s="T126">v-v:cvb</ta>
            <ta e="T128" id="Seg_5152" s="T127">ptcl</ta>
            <ta e="T129" id="Seg_5153" s="T128">v-v:tense-v:pred.pn</ta>
            <ta e="T130" id="Seg_5154" s="T129">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T133" id="Seg_5155" s="T132">adj</ta>
            <ta e="T134" id="Seg_5156" s="T133">n-n&gt;adj-n:case</ta>
            <ta e="T135" id="Seg_5157" s="T134">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T136" id="Seg_5158" s="T135">v-v:cvb</ta>
            <ta e="T137" id="Seg_5159" s="T136">v-v:tense-v:poss.pn</ta>
            <ta e="T138" id="Seg_5160" s="T137">n-n:poss-n:case</ta>
            <ta e="T139" id="Seg_5161" s="T138">v-v:cvb-v:pred.pn</ta>
            <ta e="T140" id="Seg_5162" s="T139">n-n:case</ta>
            <ta e="T141" id="Seg_5163" s="T140">post</ta>
            <ta e="T142" id="Seg_5164" s="T141">n-n:poss-n:case</ta>
            <ta e="T143" id="Seg_5165" s="T142">n-n:(num)-n:poss-n:case</ta>
            <ta e="T144" id="Seg_5166" s="T143">v-v&gt;v-v:cvb</ta>
            <ta e="T145" id="Seg_5167" s="T144">post</ta>
            <ta e="T146" id="Seg_5168" s="T145">interj-v:mood.pn</ta>
            <ta e="T147" id="Seg_5169" s="T146">interj-v:mood.pn</ta>
            <ta e="T148" id="Seg_5170" s="T147">adj-n:(num)-n:case</ta>
            <ta e="T149" id="Seg_5171" s="T148">v-v:tense-v:poss.pn</ta>
            <ta e="T150" id="Seg_5172" s="T149">propr-n:case</ta>
            <ta e="T151" id="Seg_5173" s="T150">n-n:(poss)-n:case</ta>
            <ta e="T152" id="Seg_5174" s="T151">ptcl</ta>
            <ta e="T153" id="Seg_5175" s="T152">dempro</ta>
            <ta e="T154" id="Seg_5176" s="T153">v-v:tense-v:poss.pn</ta>
            <ta e="T155" id="Seg_5177" s="T154">pers-pro:case</ta>
            <ta e="T157" id="Seg_5178" s="T156">n-n:case</ta>
            <ta e="T158" id="Seg_5179" s="T157">n-n:case</ta>
            <ta e="T159" id="Seg_5180" s="T158">pers-pro:case</ta>
            <ta e="T160" id="Seg_5181" s="T159">n-n:poss-n:case</ta>
            <ta e="T161" id="Seg_5182" s="T160">v-v:tense-v:pred.pn</ta>
            <ta e="T162" id="Seg_5183" s="T161">que-pro:(poss)-pro:case</ta>
            <ta e="T163" id="Seg_5184" s="T162">adv</ta>
            <ta e="T164" id="Seg_5185" s="T163">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T165" id="Seg_5186" s="T164">pers-pro:case</ta>
            <ta e="T166" id="Seg_5187" s="T165">pers-pro:case</ta>
            <ta e="T167" id="Seg_5188" s="T166">adv-adv&gt;adj-n:case</ta>
            <ta e="T168" id="Seg_5189" s="T167">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T169" id="Seg_5190" s="T168">emphpro-emphpro-pro:(poss)-pro:case</ta>
            <ta e="T170" id="Seg_5191" s="T169">adv</ta>
            <ta e="T171" id="Seg_5192" s="T170">v-v:tense-v:pred.pn</ta>
            <ta e="T172" id="Seg_5193" s="T171">ptcl</ta>
            <ta e="T173" id="Seg_5194" s="T172">que</ta>
            <ta e="T174" id="Seg_5195" s="T173">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T175" id="Seg_5196" s="T174">v-v:tense-v:poss.pn</ta>
            <ta e="T176" id="Seg_5197" s="T175">v-v:cvb-v-v:cvb</ta>
            <ta e="T177" id="Seg_5198" s="T176">n-n:case</ta>
            <ta e="T178" id="Seg_5199" s="T177">n-n:(poss)-n:case</ta>
            <ta e="T179" id="Seg_5200" s="T178">adv</ta>
            <ta e="T180" id="Seg_5201" s="T179">ptcl</ta>
            <ta e="T181" id="Seg_5202" s="T180">n-n&gt;v-v:cvb</ta>
            <ta e="T182" id="Seg_5203" s="T181">v-v:tense-v:poss.pn</ta>
            <ta e="T183" id="Seg_5204" s="T182">adv</ta>
            <ta e="T184" id="Seg_5205" s="T183">v-v:cvb</ta>
            <ta e="T185" id="Seg_5206" s="T184">v-v:tense-v:poss.pn</ta>
            <ta e="T186" id="Seg_5207" s="T185">n-n:(poss)-n:case</ta>
            <ta e="T187" id="Seg_5208" s="T186">n-n:case</ta>
            <ta e="T188" id="Seg_5209" s="T187">v-v:tense-v:pred.pn</ta>
            <ta e="T189" id="Seg_5210" s="T188">n-n:(poss)-n:case</ta>
            <ta e="T190" id="Seg_5211" s="T189">n-n:(poss)-n:case</ta>
            <ta e="T191" id="Seg_5212" s="T190">que-pro:case</ta>
            <ta e="T192" id="Seg_5213" s="T191">ptcl</ta>
            <ta e="T193" id="Seg_5214" s="T192">v-v:tense-v:poss.pn</ta>
            <ta e="T194" id="Seg_5215" s="T193">ptcl</ta>
            <ta e="T195" id="Seg_5216" s="T194">interj</ta>
            <ta e="T196" id="Seg_5217" s="T195">v-v:cvb</ta>
            <ta e="T197" id="Seg_5218" s="T196">v-v:tense-v:poss.pn</ta>
            <ta e="T198" id="Seg_5219" s="T197">dempro</ta>
            <ta e="T199" id="Seg_5220" s="T198">n-n:case</ta>
            <ta e="T200" id="Seg_5221" s="T199">n-n:poss-n:case</ta>
            <ta e="T201" id="Seg_5222" s="T200">v-v:cvb</ta>
            <ta e="T202" id="Seg_5223" s="T201">v-v:tense-v:poss.pn</ta>
            <ta e="T203" id="Seg_5224" s="T202">adv</ta>
            <ta e="T204" id="Seg_5225" s="T203">v-v:tense-v:poss.pn</ta>
            <ta e="T205" id="Seg_5226" s="T204">ptcl</ta>
            <ta e="T206" id="Seg_5227" s="T205">v-v:tense-v:poss.pn</ta>
            <ta e="T207" id="Seg_5228" s="T206">ptcl</ta>
            <ta e="T208" id="Seg_5229" s="T207">ptcl-ptcl:(poss.pn)</ta>
            <ta e="T209" id="Seg_5230" s="T208">ptcl</ta>
            <ta e="T210" id="Seg_5231" s="T209">dempro</ta>
            <ta e="T211" id="Seg_5232" s="T210">n-n:case</ta>
            <ta e="T212" id="Seg_5233" s="T211">ptcl</ta>
            <ta e="T213" id="Seg_5234" s="T212">n-n:(poss)-n:case</ta>
            <ta e="T214" id="Seg_5235" s="T213">ptcl</ta>
            <ta e="T215" id="Seg_5236" s="T214">adj</ta>
            <ta e="T216" id="Seg_5237" s="T215">ptcl-adj&gt;adv</ta>
            <ta e="T217" id="Seg_5238" s="T216">v-v:tense-v:pred.pn</ta>
            <ta e="T218" id="Seg_5239" s="T217">ptcl</ta>
            <ta e="T219" id="Seg_5240" s="T218">dempro-pro:case</ta>
            <ta e="T220" id="Seg_5241" s="T219">v-v:tense-v:pred.pn</ta>
            <ta e="T221" id="Seg_5242" s="T220">n-n:(poss)-n:case</ta>
            <ta e="T222" id="Seg_5243" s="T221">n-n:(poss)-n:case</ta>
            <ta e="T223" id="Seg_5244" s="T222">ptcl</ta>
            <ta e="T224" id="Seg_5245" s="T223">que-pro:case</ta>
            <ta e="T225" id="Seg_5246" s="T224">v-v:tense-v:poss.pn</ta>
            <ta e="T226" id="Seg_5247" s="T225">dempro</ta>
            <ta e="T227" id="Seg_5248" s="T226">que</ta>
            <ta e="T228" id="Seg_5249" s="T227">que-pro:case</ta>
            <ta e="T229" id="Seg_5250" s="T228">v-v:cvb</ta>
            <ta e="T230" id="Seg_5251" s="T229">v-v:tense-v:poss.pn</ta>
            <ta e="T231" id="Seg_5252" s="T230">dempro</ta>
            <ta e="T232" id="Seg_5253" s="T231">ptcl</ta>
            <ta e="T233" id="Seg_5254" s="T232">que-pro:case</ta>
            <ta e="T234" id="Seg_5255" s="T233">n-n:(poss)-n:case</ta>
            <ta e="T235" id="Seg_5256" s="T234">pers-pro:case</ta>
            <ta e="T238" id="Seg_5257" s="T237">pers-pro:case</ta>
            <ta e="T239" id="Seg_5258" s="T238">v-v:tense-v:poss.pn</ta>
            <ta e="T240" id="Seg_5259" s="T239">v-v:tense-v:poss.pn</ta>
            <ta e="T241" id="Seg_5260" s="T240">n-n:case</ta>
            <ta e="T242" id="Seg_5261" s="T241">n-n:(poss)-n:case</ta>
            <ta e="T243" id="Seg_5262" s="T242">que-pro:case</ta>
            <ta e="T244" id="Seg_5263" s="T243">v-v:(neg)-v:pred.pn</ta>
            <ta e="T245" id="Seg_5264" s="T244">dempro</ta>
            <ta e="T246" id="Seg_5265" s="T245">n-n:case</ta>
            <ta e="T247" id="Seg_5266" s="T246">n-n:poss-n:case</ta>
            <ta e="T248" id="Seg_5267" s="T247">que-pro:case</ta>
            <ta e="T249" id="Seg_5268" s="T248">v-v:(ins)-v:(neg)-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T250" id="Seg_5269" s="T249">dempro</ta>
            <ta e="T251" id="Seg_5270" s="T250">n-n:case</ta>
            <ta e="T252" id="Seg_5271" s="T251">n-n:poss-n:case</ta>
            <ta e="T253" id="Seg_5272" s="T252">v-v:tense-v:poss.pn</ta>
            <ta e="T254" id="Seg_5273" s="T253">n-n:(poss)-n:case</ta>
            <ta e="T257" id="Seg_5274" s="T256">n-n&gt;v-v:cvb-n-n&gt;v-v:cvb</ta>
            <ta e="T258" id="Seg_5275" s="T257">v-v:cvb-v-v:cvb</ta>
            <ta e="T259" id="Seg_5276" s="T258">v-v:tense-v:poss.pn</ta>
            <ta e="T260" id="Seg_5277" s="T259">ptcl</ta>
            <ta e="T261" id="Seg_5278" s="T260">v-v:tense-v:poss.pn</ta>
            <ta e="T262" id="Seg_5279" s="T261">n-n:case</ta>
            <ta e="T263" id="Seg_5280" s="T262">n-n:poss-n:case</ta>
            <ta e="T264" id="Seg_5281" s="T263">adj-n:case</ta>
            <ta e="T265" id="Seg_5282" s="T264">ptcl</ta>
            <ta e="T266" id="Seg_5283" s="T265">dempro</ta>
            <ta e="T269" id="Seg_5284" s="T268">n-n:case</ta>
            <ta e="T270" id="Seg_5285" s="T269">n-n:poss-n:case</ta>
            <ta e="T271" id="Seg_5286" s="T270">que</ta>
            <ta e="T272" id="Seg_5287" s="T271">ptcl</ta>
            <ta e="T275" id="Seg_5288" s="T274">v-v:cvb-v:pred.pn</ta>
            <ta e="T276" id="Seg_5289" s="T275">cardnum</ta>
            <ta e="T277" id="Seg_5290" s="T276">ptcl</ta>
            <ta e="T278" id="Seg_5291" s="T277">n-n:case</ta>
            <ta e="T279" id="Seg_5292" s="T278">v-v:tense-v:poss.pn</ta>
            <ta e="T280" id="Seg_5293" s="T279">ptcl</ta>
            <ta e="T281" id="Seg_5294" s="T280">adv</ta>
            <ta e="T282" id="Seg_5295" s="T281">pers-pro:case</ta>
            <ta e="T283" id="Seg_5296" s="T282">n-n:poss-n:case</ta>
            <ta e="T284" id="Seg_5297" s="T283">v-v:tense-v:poss.pn</ta>
            <ta e="T285" id="Seg_5298" s="T284">ptcl-ptcl:(poss.pn)</ta>
            <ta e="T286" id="Seg_5299" s="T285">adv</ta>
            <ta e="T287" id="Seg_5300" s="T286">v-v:mood-v:temp.pn</ta>
            <ta e="T288" id="Seg_5301" s="T287">cardnum</ta>
            <ta e="T289" id="Seg_5302" s="T288">ptcl</ta>
            <ta e="T290" id="Seg_5303" s="T289">n-n:case</ta>
            <ta e="T291" id="Seg_5304" s="T290">adv</ta>
            <ta e="T292" id="Seg_5305" s="T291">pers-pro:case</ta>
            <ta e="T293" id="Seg_5306" s="T292">n-n:poss-n:case</ta>
            <ta e="T294" id="Seg_5307" s="T293">pers-pro:case</ta>
            <ta e="T295" id="Seg_5308" s="T294">v-v:tense-v:poss.pn</ta>
            <ta e="T296" id="Seg_5309" s="T295">ptcl</ta>
            <ta e="T297" id="Seg_5310" s="T296">n-n:(poss)-n:case</ta>
            <ta e="T298" id="Seg_5311" s="T297">v-v:cvb</ta>
            <ta e="T299" id="Seg_5312" s="T298">v-v:tense-v:poss.pn</ta>
            <ta e="T300" id="Seg_5313" s="T299">v-v:cvb</ta>
            <ta e="T301" id="Seg_5314" s="T300">v-v:tense-v:poss.pn</ta>
            <ta e="T302" id="Seg_5315" s="T301">adv</ta>
            <ta e="T303" id="Seg_5316" s="T302">adv-adj&gt;adv</ta>
            <ta e="T304" id="Seg_5317" s="T303">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T305" id="Seg_5318" s="T304">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T306" id="Seg_5319" s="T305">v-v:tense-v:pred.pn</ta>
            <ta e="T307" id="Seg_5320" s="T306">interj</ta>
            <ta e="T308" id="Seg_5321" s="T307">ptcl</ta>
            <ta e="T309" id="Seg_5322" s="T308">adv</ta>
            <ta e="T310" id="Seg_5323" s="T309">v-v:tense-v:pred.pn</ta>
            <ta e="T311" id="Seg_5324" s="T310">ptcl</ta>
            <ta e="T312" id="Seg_5325" s="T311">adv</ta>
            <ta e="T313" id="Seg_5326" s="T312">n-n:case</ta>
            <ta e="T314" id="Seg_5327" s="T313">post</ta>
            <ta e="T315" id="Seg_5328" s="T314">que-pro:case</ta>
            <ta e="T316" id="Seg_5329" s="T315">adj</ta>
            <ta e="T317" id="Seg_5330" s="T316">n-n:(poss)-n:case-ptcl</ta>
            <ta e="T318" id="Seg_5331" s="T317">n-n:(num)-n:case</ta>
            <ta e="T319" id="Seg_5332" s="T318">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T320" id="Seg_5333" s="T319">interj</ta>
            <ta e="T321" id="Seg_5334" s="T320">v-v:ptcp</ta>
            <ta e="T322" id="Seg_5335" s="T321">v-v:tense-v:pred.pn</ta>
            <ta e="T323" id="Seg_5336" s="T322">pers-pro:case</ta>
            <ta e="T324" id="Seg_5337" s="T323">pers-pro:case</ta>
            <ta e="T325" id="Seg_5338" s="T324">dempro-pro:case</ta>
            <ta e="T326" id="Seg_5339" s="T325">n-n:(poss)-n:case</ta>
            <ta e="T327" id="Seg_5340" s="T326">ptcl</ta>
            <ta e="T328" id="Seg_5341" s="T327">dempro-pro:case</ta>
            <ta e="T329" id="Seg_5342" s="T328">v-v:tense-v:pred.pn</ta>
            <ta e="T330" id="Seg_5343" s="T329">cardnum</ta>
            <ta e="T331" id="Seg_5344" s="T330">n-n:(poss)-n:case</ta>
            <ta e="T332" id="Seg_5345" s="T331">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T333" id="Seg_5346" s="T332">adv</ta>
            <ta e="T334" id="Seg_5347" s="T333">cardnum</ta>
            <ta e="T335" id="Seg_5348" s="T334">ptcl</ta>
            <ta e="T336" id="Seg_5349" s="T335">n-n:(poss)</ta>
            <ta e="T337" id="Seg_5350" s="T336">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T338" id="Seg_5351" s="T337">cardnum-n:poss-n:case</ta>
            <ta e="T339" id="Seg_5352" s="T338">n-n:poss-n:case</ta>
            <ta e="T340" id="Seg_5353" s="T339">v-v:tense-v:poss.pn</ta>
            <ta e="T341" id="Seg_5354" s="T340">adj-adj&gt;adj-adj&gt;adv</ta>
            <ta e="T342" id="Seg_5355" s="T341">v-v:ptcp-v:(case)</ta>
            <ta e="T343" id="Seg_5356" s="T342">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T344" id="Seg_5357" s="T343">n-n:case</ta>
            <ta e="T347" id="Seg_5358" s="T346">n-n:poss-n:case</ta>
            <ta e="T348" id="Seg_5359" s="T347">post</ta>
            <ta e="T349" id="Seg_5360" s="T348">n-n:case</ta>
            <ta e="T350" id="Seg_5361" s="T349">n-n:poss-n:case</ta>
            <ta e="T351" id="Seg_5362" s="T350">post</ta>
            <ta e="T352" id="Seg_5363" s="T351">adj</ta>
            <ta e="T353" id="Seg_5364" s="T352">n-n:poss-n:case</ta>
            <ta e="T354" id="Seg_5365" s="T353">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T355" id="Seg_5366" s="T354">n-n:(poss)-n:case</ta>
            <ta e="T356" id="Seg_5367" s="T355">ptcl</ta>
            <ta e="T357" id="Seg_5368" s="T356">v-v:tense-v:pred.pn</ta>
            <ta e="T358" id="Seg_5369" s="T357">ptcl</ta>
            <ta e="T359" id="Seg_5370" s="T358">ptcl</ta>
            <ta e="T360" id="Seg_5371" s="T359">que</ta>
            <ta e="T361" id="Seg_5372" s="T360">ptcl</ta>
            <ta e="T362" id="Seg_5373" s="T361">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T363" id="Seg_5374" s="T362">adv</ta>
            <ta e="T364" id="Seg_5375" s="T363">propr-n:case</ta>
            <ta e="T365" id="Seg_5376" s="T364">n-n:(poss)-n:case</ta>
            <ta e="T366" id="Seg_5377" s="T365">v-v:cvb</ta>
            <ta e="T367" id="Seg_5378" s="T366">conj</ta>
            <ta e="T368" id="Seg_5379" s="T367">v-v:tense-v:pred.pn</ta>
            <ta e="T369" id="Seg_5380" s="T368">v-v:tense-v:pred.pn</ta>
            <ta e="T370" id="Seg_5381" s="T369">adv</ta>
            <ta e="T371" id="Seg_5382" s="T370">ptcl</ta>
            <ta e="T372" id="Seg_5383" s="T371">v-v&gt;n-n:case</ta>
            <ta e="T373" id="Seg_5384" s="T372">v-v:tense-v:pred.pn</ta>
            <ta e="T374" id="Seg_5385" s="T373">ptcl</ta>
            <ta e="T375" id="Seg_5386" s="T374">n-n:poss-n:case</ta>
            <ta e="T376" id="Seg_5387" s="T375">post</ta>
            <ta e="T377" id="Seg_5388" s="T376">n-n:(poss)-n:case</ta>
            <ta e="T378" id="Seg_5389" s="T377">n-n&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T379" id="Seg_5390" s="T378">ptcl</ta>
            <ta e="T380" id="Seg_5391" s="T379">que</ta>
            <ta e="T381" id="Seg_5392" s="T380">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T382" id="Seg_5393" s="T381">v-v:cvb</ta>
            <ta e="T383" id="Seg_5394" s="T382">post</ta>
            <ta e="T384" id="Seg_5395" s="T383">n-n:(num)-n:case</ta>
            <ta e="T385" id="Seg_5396" s="T384">v-v:ptcp-v:(case)</ta>
            <ta e="T386" id="Seg_5397" s="T385">post</ta>
            <ta e="T387" id="Seg_5398" s="T386">n-n:poss-n:case</ta>
            <ta e="T388" id="Seg_5399" s="T387">adv-adv&gt;adv</ta>
            <ta e="T389" id="Seg_5400" s="T388">adv-adv&gt;adv</ta>
            <ta e="T390" id="Seg_5401" s="T389">v-v:tense-v:poss.pn</ta>
            <ta e="T391" id="Seg_5402" s="T390">adv</ta>
            <ta e="T392" id="Seg_5403" s="T391">dempro</ta>
            <ta e="T393" id="Seg_5404" s="T392">adv-adv&gt;adv</ta>
            <ta e="T394" id="Seg_5405" s="T393">dempro</ta>
            <ta e="T395" id="Seg_5406" s="T394">adv-adv&gt;adv</ta>
            <ta e="T396" id="Seg_5407" s="T395">n-n:poss-n:case</ta>
            <ta e="T397" id="Seg_5408" s="T396">n-n:poss-n:case</ta>
            <ta e="T398" id="Seg_5409" s="T397">ptcl</ta>
            <ta e="T399" id="Seg_5410" s="T398">que-que&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T400" id="Seg_5411" s="T399">v-v:tense-v:pred.pn</ta>
            <ta e="T401" id="Seg_5412" s="T400">n-n:(num)-n:case</ta>
            <ta e="T402" id="Seg_5413" s="T401">v-v:(ins)-v&gt;v-v:ptcp-v:(case)</ta>
            <ta e="T403" id="Seg_5414" s="T402">post</ta>
            <ta e="T404" id="Seg_5415" s="T403">interj</ta>
            <ta e="T405" id="Seg_5416" s="T404">ptcl</ta>
            <ta e="T406" id="Seg_5417" s="T405">n-n:(poss)-n:case</ta>
            <ta e="T407" id="Seg_5418" s="T406">ptcl</ta>
            <ta e="T408" id="Seg_5419" s="T407">v-v:tense-v:pred.pn</ta>
            <ta e="T409" id="Seg_5420" s="T408">ptcl</ta>
            <ta e="T410" id="Seg_5421" s="T409">adj</ta>
            <ta e="T411" id="Seg_5422" s="T410">ptcl-ptcl&gt;adv</ta>
            <ta e="T412" id="Seg_5423" s="T411">n-n:poss-n:case</ta>
            <ta e="T413" id="Seg_5424" s="T412">v-v:tense-v:poss.pn</ta>
            <ta e="T414" id="Seg_5425" s="T413">adv</ta>
            <ta e="T415" id="Seg_5426" s="T414">adv-adv&gt;adv</ta>
            <ta e="T416" id="Seg_5427" s="T415">v-v:tense-v:poss.pn</ta>
            <ta e="T417" id="Seg_5428" s="T416">n-n:case</ta>
            <ta e="T418" id="Seg_5429" s="T417">n-n:poss-n:case</ta>
            <ta e="T419" id="Seg_5430" s="T418">interj</ta>
            <ta e="T420" id="Seg_5431" s="T419">adj</ta>
            <ta e="T421" id="Seg_5432" s="T420">adj</ta>
            <ta e="T422" id="Seg_5433" s="T421">ptcl-ptcl&gt;adv</ta>
            <ta e="T423" id="Seg_5434" s="T422">n-n:poss-n:case</ta>
            <ta e="T424" id="Seg_5435" s="T423">n-n:case</ta>
            <ta e="T425" id="Seg_5436" s="T424">post-n:case</ta>
            <ta e="T426" id="Seg_5437" s="T425">v-v:cvb</ta>
            <ta e="T427" id="Seg_5438" s="T426">v-v:(neg)-v:pred.pn</ta>
            <ta e="T428" id="Seg_5439" s="T427">ptcl</ta>
            <ta e="T429" id="Seg_5440" s="T428">adv</ta>
            <ta e="T430" id="Seg_5441" s="T429">n-n:(poss)-n:case</ta>
            <ta e="T431" id="Seg_5442" s="T430">v-v:tense-v:poss.pn</ta>
            <ta e="T432" id="Seg_5443" s="T431">ptcl</ta>
            <ta e="T433" id="Seg_5444" s="T432">que</ta>
            <ta e="T434" id="Seg_5445" s="T433">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T435" id="Seg_5446" s="T434">pers-pro:case</ta>
            <ta e="T436" id="Seg_5447" s="T435">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T437" id="Seg_5448" s="T436">n-n:poss-n:case</ta>
            <ta e="T438" id="Seg_5449" s="T437">adv</ta>
            <ta e="T439" id="Seg_5450" s="T438">v-v:tense-v:poss.pn</ta>
            <ta e="T440" id="Seg_5451" s="T439">ptcl-ptcl:(poss.pn)</ta>
            <ta e="T441" id="Seg_5452" s="T440">dempro</ta>
            <ta e="T442" id="Seg_5453" s="T441">n-n:case</ta>
            <ta e="T443" id="Seg_5454" s="T442">n-n:case</ta>
            <ta e="T444" id="Seg_5455" s="T443">dempro</ta>
            <ta e="T445" id="Seg_5456" s="T444">n-n:case</ta>
            <ta e="T446" id="Seg_5457" s="T445">n-n:case</ta>
            <ta e="T447" id="Seg_5458" s="T446">adj-n:poss-n:case</ta>
            <ta e="T448" id="Seg_5459" s="T447">v-v:tense-v:pred.pn</ta>
            <ta e="T449" id="Seg_5460" s="T448">ptcl</ta>
            <ta e="T450" id="Seg_5461" s="T449">v-v:tense-v:pred.pn</ta>
            <ta e="T451" id="Seg_5462" s="T450">pers-pro:case</ta>
            <ta e="T452" id="Seg_5463" s="T451">propr-n:case</ta>
            <ta e="T453" id="Seg_5464" s="T452">n-n:poss-n:case</ta>
            <ta e="T454" id="Seg_5465" s="T453">adj-adj&gt;adv</ta>
            <ta e="T455" id="Seg_5466" s="T454">v-v:(ins)-v:mood.pn</ta>
            <ta e="T456" id="Seg_5467" s="T455">adj-adj&gt;adv</ta>
            <ta e="T457" id="Seg_5468" s="T456">v-v:(ins)-v:mood.pn</ta>
            <ta e="T458" id="Seg_5469" s="T457">quant</ta>
            <ta e="T459" id="Seg_5470" s="T458">n-n:case</ta>
            <ta e="T460" id="Seg_5471" s="T459">n-n&gt;v-v:(ins)-v:mood.pn</ta>
            <ta e="T461" id="Seg_5472" s="T460">v-v:tense-v:poss.pn</ta>
            <ta e="T462" id="Seg_5473" s="T461">ptcl</ta>
            <ta e="T463" id="Seg_5474" s="T462">dempro</ta>
            <ta e="T464" id="Seg_5475" s="T463">post-post&gt;post</ta>
            <ta e="T465" id="Seg_5476" s="T464">propr-n:case</ta>
            <ta e="T466" id="Seg_5477" s="T465">n-n:poss-n:case</ta>
            <ta e="T467" id="Seg_5478" s="T466">post</ta>
            <ta e="T468" id="Seg_5479" s="T467">adj</ta>
            <ta e="T469" id="Seg_5480" s="T468">ptcl-ptcl&gt;adv</ta>
            <ta e="T470" id="Seg_5481" s="T469">v-v:tense-v:poss.pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_5482" s="T0">pers</ta>
            <ta e="T2" id="Seg_5483" s="T1">dempro</ta>
            <ta e="T3" id="Seg_5484" s="T2">n</ta>
            <ta e="T4" id="Seg_5485" s="T3">v</ta>
            <ta e="T5" id="Seg_5486" s="T4">n</ta>
            <ta e="T7" id="Seg_5487" s="T5">post</ta>
            <ta e="T8" id="Seg_5488" s="T7">n</ta>
            <ta e="T9" id="Seg_5489" s="T8">v</ta>
            <ta e="T10" id="Seg_5490" s="T9">n</ta>
            <ta e="T11" id="Seg_5491" s="T10">dempro</ta>
            <ta e="T12" id="Seg_5492" s="T11">adv</ta>
            <ta e="T13" id="Seg_5493" s="T12">post</ta>
            <ta e="T14" id="Seg_5494" s="T13">n</ta>
            <ta e="T15" id="Seg_5495" s="T14">ptcl</ta>
            <ta e="T16" id="Seg_5496" s="T15">adv</ta>
            <ta e="T17" id="Seg_5497" s="T16">n</ta>
            <ta e="T18" id="Seg_5498" s="T17">n</ta>
            <ta e="T19" id="Seg_5499" s="T18">propr</ta>
            <ta e="T20" id="Seg_5500" s="T19">v</ta>
            <ta e="T21" id="Seg_5501" s="T20">aux</ta>
            <ta e="T22" id="Seg_5502" s="T21">adj</ta>
            <ta e="T23" id="Seg_5503" s="T22">n</ta>
            <ta e="T24" id="Seg_5504" s="T23">v</ta>
            <ta e="T25" id="Seg_5505" s="T24">n</ta>
            <ta e="T26" id="Seg_5506" s="T25">emphpro</ta>
            <ta e="T27" id="Seg_5507" s="T26">v</ta>
            <ta e="T28" id="Seg_5508" s="T27">n</ta>
            <ta e="T29" id="Seg_5509" s="T28">n</ta>
            <ta e="T30" id="Seg_5510" s="T29">post</ta>
            <ta e="T31" id="Seg_5511" s="T30">v</ta>
            <ta e="T32" id="Seg_5512" s="T31">aux</ta>
            <ta e="T33" id="Seg_5513" s="T32">adj</ta>
            <ta e="T34" id="Seg_5514" s="T33">ptcl</ta>
            <ta e="T35" id="Seg_5515" s="T34">n</ta>
            <ta e="T36" id="Seg_5516" s="T35">dempro</ta>
            <ta e="T37" id="Seg_5517" s="T36">n</ta>
            <ta e="T38" id="Seg_5518" s="T37">propr</ta>
            <ta e="T39" id="Seg_5519" s="T38">cop</ta>
            <ta e="T40" id="Seg_5520" s="T39">adj</ta>
            <ta e="T41" id="Seg_5521" s="T40">n</ta>
            <ta e="T44" id="Seg_5522" s="T43">pers</ta>
            <ta e="T45" id="Seg_5523" s="T44">adv</ta>
            <ta e="T46" id="Seg_5524" s="T45">v</ta>
            <ta e="T47" id="Seg_5525" s="T46">aux</ta>
            <ta e="T48" id="Seg_5526" s="T47">adj</ta>
            <ta e="T49" id="Seg_5527" s="T48">n</ta>
            <ta e="T50" id="Seg_5528" s="T49">n</ta>
            <ta e="T51" id="Seg_5529" s="T50">post</ta>
            <ta e="T52" id="Seg_5530" s="T51">dempro</ta>
            <ta e="T53" id="Seg_5531" s="T52">adj</ta>
            <ta e="T54" id="Seg_5532" s="T53">interj</ta>
            <ta e="T55" id="Seg_5533" s="T54">n</ta>
            <ta e="T56" id="Seg_5534" s="T55">post</ta>
            <ta e="T57" id="Seg_5535" s="T56">dempro</ta>
            <ta e="T58" id="Seg_5536" s="T57">n</ta>
            <ta e="T59" id="Seg_5537" s="T58">v</ta>
            <ta e="T60" id="Seg_5538" s="T59">aux</ta>
            <ta e="T61" id="Seg_5539" s="T60">v</ta>
            <ta e="T62" id="Seg_5540" s="T61">aux</ta>
            <ta e="T63" id="Seg_5541" s="T62">emphpro</ta>
            <ta e="T64" id="Seg_5542" s="T63">v</ta>
            <ta e="T65" id="Seg_5543" s="T64">n</ta>
            <ta e="T66" id="Seg_5544" s="T65">adv</ta>
            <ta e="T67" id="Seg_5545" s="T66">n</ta>
            <ta e="T68" id="Seg_5546" s="T67">propr</ta>
            <ta e="T69" id="Seg_5547" s="T68">n</ta>
            <ta e="T70" id="Seg_5548" s="T69">v</ta>
            <ta e="T71" id="Seg_5549" s="T70">adv</ta>
            <ta e="T72" id="Seg_5550" s="T71">pers</ta>
            <ta e="T73" id="Seg_5551" s="T72">n</ta>
            <ta e="T74" id="Seg_5552" s="T73">v</ta>
            <ta e="T75" id="Seg_5553" s="T74">aux</ta>
            <ta e="T76" id="Seg_5554" s="T75">v</ta>
            <ta e="T77" id="Seg_5555" s="T76">pers</ta>
            <ta e="T78" id="Seg_5556" s="T77">v</ta>
            <ta e="T79" id="Seg_5557" s="T78">aux</ta>
            <ta e="T80" id="Seg_5558" s="T79">n</ta>
            <ta e="T81" id="Seg_5559" s="T80">post</ta>
            <ta e="T82" id="Seg_5560" s="T81">v</ta>
            <ta e="T83" id="Seg_5561" s="T82">n</ta>
            <ta e="T84" id="Seg_5562" s="T83">post</ta>
            <ta e="T85" id="Seg_5563" s="T84">v</ta>
            <ta e="T86" id="Seg_5564" s="T85">n</ta>
            <ta e="T87" id="Seg_5565" s="T86">v</ta>
            <ta e="T88" id="Seg_5566" s="T87">pers</ta>
            <ta e="T89" id="Seg_5567" s="T88">n</ta>
            <ta e="T93" id="Seg_5568" s="T92">adj</ta>
            <ta e="T94" id="Seg_5569" s="T93">ptcl</ta>
            <ta e="T95" id="Seg_5570" s="T94">n</ta>
            <ta e="T96" id="Seg_5571" s="T95">conj</ta>
            <ta e="T97" id="Seg_5572" s="T96">v</ta>
            <ta e="T98" id="Seg_5573" s="T97">adj</ta>
            <ta e="T99" id="Seg_5574" s="T98">ptcl</ta>
            <ta e="T100" id="Seg_5575" s="T99">dempro</ta>
            <ta e="T101" id="Seg_5576" s="T100">n</ta>
            <ta e="T102" id="Seg_5577" s="T101">pers</ta>
            <ta e="T103" id="Seg_5578" s="T102">v</ta>
            <ta e="T104" id="Seg_5579" s="T103">ptcl</ta>
            <ta e="T105" id="Seg_5580" s="T104">dempro</ta>
            <ta e="T106" id="Seg_5581" s="T105">post</ta>
            <ta e="T107" id="Seg_5582" s="T106">v</ta>
            <ta e="T108" id="Seg_5583" s="T107">conj</ta>
            <ta e="T109" id="Seg_5584" s="T108">n</ta>
            <ta e="T110" id="Seg_5585" s="T109">dempro</ta>
            <ta e="T111" id="Seg_5586" s="T110">post</ta>
            <ta e="T112" id="Seg_5587" s="T111">que</ta>
            <ta e="T113" id="Seg_5588" s="T112">ptcl</ta>
            <ta e="T114" id="Seg_5589" s="T113">n</ta>
            <ta e="T115" id="Seg_5590" s="T114">cop</ta>
            <ta e="T116" id="Seg_5591" s="T115">post</ta>
            <ta e="T117" id="Seg_5592" s="T116">adj</ta>
            <ta e="T118" id="Seg_5593" s="T117">ptcl</ta>
            <ta e="T119" id="Seg_5594" s="T118">n</ta>
            <ta e="T120" id="Seg_5595" s="T119">adv</ta>
            <ta e="T121" id="Seg_5596" s="T120">adv</ta>
            <ta e="T122" id="Seg_5597" s="T121">adj</ta>
            <ta e="T123" id="Seg_5598" s="T122">ptcl</ta>
            <ta e="T124" id="Seg_5599" s="T123">n</ta>
            <ta e="T125" id="Seg_5600" s="T124">ptcl</ta>
            <ta e="T126" id="Seg_5601" s="T125">v</ta>
            <ta e="T127" id="Seg_5602" s="T126">v</ta>
            <ta e="T128" id="Seg_5603" s="T127">ptcl</ta>
            <ta e="T129" id="Seg_5604" s="T128">v</ta>
            <ta e="T130" id="Seg_5605" s="T129">emphpro</ta>
            <ta e="T133" id="Seg_5606" s="T132">adj</ta>
            <ta e="T134" id="Seg_5607" s="T133">adj</ta>
            <ta e="T135" id="Seg_5608" s="T134">dempro</ta>
            <ta e="T136" id="Seg_5609" s="T135">v</ta>
            <ta e="T137" id="Seg_5610" s="T136">aux</ta>
            <ta e="T138" id="Seg_5611" s="T137">n</ta>
            <ta e="T139" id="Seg_5612" s="T138">v</ta>
            <ta e="T140" id="Seg_5613" s="T139">n</ta>
            <ta e="T141" id="Seg_5614" s="T140">post</ta>
            <ta e="T142" id="Seg_5615" s="T141">n</ta>
            <ta e="T143" id="Seg_5616" s="T142">n</ta>
            <ta e="T144" id="Seg_5617" s="T143">v</ta>
            <ta e="T145" id="Seg_5618" s="T144">post</ta>
            <ta e="T146" id="Seg_5619" s="T145">interj</ta>
            <ta e="T147" id="Seg_5620" s="T146">interj</ta>
            <ta e="T148" id="Seg_5621" s="T147">n</ta>
            <ta e="T149" id="Seg_5622" s="T148">v</ta>
            <ta e="T150" id="Seg_5623" s="T149">propr</ta>
            <ta e="T151" id="Seg_5624" s="T150">n</ta>
            <ta e="T152" id="Seg_5625" s="T151">ptcl</ta>
            <ta e="T153" id="Seg_5626" s="T152">dempro</ta>
            <ta e="T154" id="Seg_5627" s="T153">v</ta>
            <ta e="T155" id="Seg_5628" s="T154">pers</ta>
            <ta e="T157" id="Seg_5629" s="T156">n</ta>
            <ta e="T158" id="Seg_5630" s="T157">n</ta>
            <ta e="T159" id="Seg_5631" s="T158">pers</ta>
            <ta e="T160" id="Seg_5632" s="T159">n</ta>
            <ta e="T161" id="Seg_5633" s="T160">v</ta>
            <ta e="T162" id="Seg_5634" s="T161">que</ta>
            <ta e="T163" id="Seg_5635" s="T162">adv</ta>
            <ta e="T164" id="Seg_5636" s="T163">v</ta>
            <ta e="T165" id="Seg_5637" s="T164">pers</ta>
            <ta e="T166" id="Seg_5638" s="T165">pers</ta>
            <ta e="T167" id="Seg_5639" s="T166">adj</ta>
            <ta e="T168" id="Seg_5640" s="T167">v</ta>
            <ta e="T169" id="Seg_5641" s="T168">emphpro</ta>
            <ta e="T170" id="Seg_5642" s="T169">adv</ta>
            <ta e="T171" id="Seg_5643" s="T170">v</ta>
            <ta e="T172" id="Seg_5644" s="T171">ptcl</ta>
            <ta e="T173" id="Seg_5645" s="T172">que</ta>
            <ta e="T174" id="Seg_5646" s="T173">v</ta>
            <ta e="T175" id="Seg_5647" s="T174">v</ta>
            <ta e="T176" id="Seg_5648" s="T175">v</ta>
            <ta e="T177" id="Seg_5649" s="T176">n</ta>
            <ta e="T178" id="Seg_5650" s="T177">n</ta>
            <ta e="T179" id="Seg_5651" s="T178">adv</ta>
            <ta e="T180" id="Seg_5652" s="T179">ptcl</ta>
            <ta e="T181" id="Seg_5653" s="T180">v</ta>
            <ta e="T182" id="Seg_5654" s="T181">v</ta>
            <ta e="T183" id="Seg_5655" s="T182">adv</ta>
            <ta e="T184" id="Seg_5656" s="T183">v</ta>
            <ta e="T185" id="Seg_5657" s="T184">v</ta>
            <ta e="T186" id="Seg_5658" s="T185">n</ta>
            <ta e="T187" id="Seg_5659" s="T186">n</ta>
            <ta e="T188" id="Seg_5660" s="T187">cop</ta>
            <ta e="T189" id="Seg_5661" s="T188">n</ta>
            <ta e="T190" id="Seg_5662" s="T189">n</ta>
            <ta e="T191" id="Seg_5663" s="T190">que</ta>
            <ta e="T192" id="Seg_5664" s="T191">ptcl</ta>
            <ta e="T193" id="Seg_5665" s="T192">v</ta>
            <ta e="T194" id="Seg_5666" s="T193">ptcl</ta>
            <ta e="T195" id="Seg_5667" s="T194">interj</ta>
            <ta e="T196" id="Seg_5668" s="T195">v</ta>
            <ta e="T197" id="Seg_5669" s="T196">v</ta>
            <ta e="T198" id="Seg_5670" s="T197">dempro</ta>
            <ta e="T199" id="Seg_5671" s="T198">n</ta>
            <ta e="T200" id="Seg_5672" s="T199">n</ta>
            <ta e="T201" id="Seg_5673" s="T200">v</ta>
            <ta e="T202" id="Seg_5674" s="T201">v</ta>
            <ta e="T203" id="Seg_5675" s="T202">adv</ta>
            <ta e="T204" id="Seg_5676" s="T203">v</ta>
            <ta e="T205" id="Seg_5677" s="T204">ptcl</ta>
            <ta e="T206" id="Seg_5678" s="T205">v</ta>
            <ta e="T207" id="Seg_5679" s="T206">ptcl</ta>
            <ta e="T208" id="Seg_5680" s="T207">ptcl</ta>
            <ta e="T209" id="Seg_5681" s="T208">ptcl</ta>
            <ta e="T210" id="Seg_5682" s="T209">dempro</ta>
            <ta e="T211" id="Seg_5683" s="T210">n</ta>
            <ta e="T212" id="Seg_5684" s="T211">ptcl</ta>
            <ta e="T213" id="Seg_5685" s="T212">n</ta>
            <ta e="T214" id="Seg_5686" s="T213">ptcl</ta>
            <ta e="T215" id="Seg_5687" s="T214">adj</ta>
            <ta e="T216" id="Seg_5688" s="T215">adv</ta>
            <ta e="T217" id="Seg_5689" s="T216">v</ta>
            <ta e="T218" id="Seg_5690" s="T217">ptcl</ta>
            <ta e="T219" id="Seg_5691" s="T218">dempro</ta>
            <ta e="T220" id="Seg_5692" s="T219">v</ta>
            <ta e="T221" id="Seg_5693" s="T220">n</ta>
            <ta e="T222" id="Seg_5694" s="T221">n</ta>
            <ta e="T223" id="Seg_5695" s="T222">ptcl</ta>
            <ta e="T224" id="Seg_5696" s="T223">que</ta>
            <ta e="T225" id="Seg_5697" s="T224">v</ta>
            <ta e="T226" id="Seg_5698" s="T225">dempro</ta>
            <ta e="T227" id="Seg_5699" s="T226">que</ta>
            <ta e="T228" id="Seg_5700" s="T227">que</ta>
            <ta e="T229" id="Seg_5701" s="T228">v</ta>
            <ta e="T230" id="Seg_5702" s="T229">v</ta>
            <ta e="T231" id="Seg_5703" s="T230">dempro</ta>
            <ta e="T232" id="Seg_5704" s="T231">ptcl</ta>
            <ta e="T233" id="Seg_5705" s="T232">que</ta>
            <ta e="T234" id="Seg_5706" s="T233">n</ta>
            <ta e="T235" id="Seg_5707" s="T234">pers</ta>
            <ta e="T238" id="Seg_5708" s="T237">pers</ta>
            <ta e="T239" id="Seg_5709" s="T238">v</ta>
            <ta e="T240" id="Seg_5710" s="T239">v</ta>
            <ta e="T241" id="Seg_5711" s="T240">n</ta>
            <ta e="T242" id="Seg_5712" s="T241">n</ta>
            <ta e="T243" id="Seg_5713" s="T242">que</ta>
            <ta e="T244" id="Seg_5714" s="T243">v</ta>
            <ta e="T245" id="Seg_5715" s="T244">dempro</ta>
            <ta e="T246" id="Seg_5716" s="T245">n</ta>
            <ta e="T247" id="Seg_5717" s="T246">n</ta>
            <ta e="T248" id="Seg_5718" s="T247">que</ta>
            <ta e="T249" id="Seg_5719" s="T248">v</ta>
            <ta e="T250" id="Seg_5720" s="T249">dempro</ta>
            <ta e="T251" id="Seg_5721" s="T250">n</ta>
            <ta e="T252" id="Seg_5722" s="T251">n</ta>
            <ta e="T253" id="Seg_5723" s="T252">v</ta>
            <ta e="T254" id="Seg_5724" s="T253">n</ta>
            <ta e="T257" id="Seg_5725" s="T256">v</ta>
            <ta e="T258" id="Seg_5726" s="T257">v</ta>
            <ta e="T259" id="Seg_5727" s="T258">v</ta>
            <ta e="T260" id="Seg_5728" s="T259">ptcl</ta>
            <ta e="T261" id="Seg_5729" s="T260">v</ta>
            <ta e="T262" id="Seg_5730" s="T261">n</ta>
            <ta e="T263" id="Seg_5731" s="T262">n</ta>
            <ta e="T264" id="Seg_5732" s="T263">adj</ta>
            <ta e="T265" id="Seg_5733" s="T264">ptcl</ta>
            <ta e="T266" id="Seg_5734" s="T265">dempro</ta>
            <ta e="T269" id="Seg_5735" s="T268">n</ta>
            <ta e="T270" id="Seg_5736" s="T269">n</ta>
            <ta e="T271" id="Seg_5737" s="T270">que</ta>
            <ta e="T272" id="Seg_5738" s="T271">ptcl</ta>
            <ta e="T275" id="Seg_5739" s="T274">v</ta>
            <ta e="T276" id="Seg_5740" s="T275">cardnum</ta>
            <ta e="T277" id="Seg_5741" s="T276">ptcl</ta>
            <ta e="T278" id="Seg_5742" s="T277">n</ta>
            <ta e="T279" id="Seg_5743" s="T278">v</ta>
            <ta e="T280" id="Seg_5744" s="T279">ptcl</ta>
            <ta e="T281" id="Seg_5745" s="T280">adv</ta>
            <ta e="T282" id="Seg_5746" s="T281">pers</ta>
            <ta e="T283" id="Seg_5747" s="T282">n</ta>
            <ta e="T284" id="Seg_5748" s="T283">v</ta>
            <ta e="T285" id="Seg_5749" s="T284">ptcl</ta>
            <ta e="T286" id="Seg_5750" s="T285">adv</ta>
            <ta e="T287" id="Seg_5751" s="T286">v</ta>
            <ta e="T288" id="Seg_5752" s="T287">cardnum</ta>
            <ta e="T289" id="Seg_5753" s="T288">ptcl</ta>
            <ta e="T290" id="Seg_5754" s="T289">n</ta>
            <ta e="T291" id="Seg_5755" s="T290">adv</ta>
            <ta e="T292" id="Seg_5756" s="T291">pers</ta>
            <ta e="T293" id="Seg_5757" s="T292">n</ta>
            <ta e="T294" id="Seg_5758" s="T293">pers</ta>
            <ta e="T295" id="Seg_5759" s="T294">v</ta>
            <ta e="T296" id="Seg_5760" s="T295">ptcl</ta>
            <ta e="T297" id="Seg_5761" s="T296">n</ta>
            <ta e="T298" id="Seg_5762" s="T297">v</ta>
            <ta e="T299" id="Seg_5763" s="T298">v</ta>
            <ta e="T300" id="Seg_5764" s="T299">v</ta>
            <ta e="T301" id="Seg_5765" s="T300">v</ta>
            <ta e="T302" id="Seg_5766" s="T301">adv</ta>
            <ta e="T303" id="Seg_5767" s="T302">adv</ta>
            <ta e="T304" id="Seg_5768" s="T303">emphpro</ta>
            <ta e="T305" id="Seg_5769" s="T304">emphpro</ta>
            <ta e="T306" id="Seg_5770" s="T305">v</ta>
            <ta e="T307" id="Seg_5771" s="T306">interj</ta>
            <ta e="T308" id="Seg_5772" s="T307">ptcl</ta>
            <ta e="T309" id="Seg_5773" s="T308">adv</ta>
            <ta e="T310" id="Seg_5774" s="T309">cop</ta>
            <ta e="T311" id="Seg_5775" s="T310">ptcl</ta>
            <ta e="T312" id="Seg_5776" s="T311">adv</ta>
            <ta e="T313" id="Seg_5777" s="T312">n</ta>
            <ta e="T314" id="Seg_5778" s="T313">post</ta>
            <ta e="T315" id="Seg_5779" s="T314">que</ta>
            <ta e="T316" id="Seg_5780" s="T315">adj</ta>
            <ta e="T317" id="Seg_5781" s="T316">n</ta>
            <ta e="T318" id="Seg_5782" s="T317">n</ta>
            <ta e="T319" id="Seg_5783" s="T318">dempro</ta>
            <ta e="T320" id="Seg_5784" s="T319">interj</ta>
            <ta e="T321" id="Seg_5785" s="T320">v</ta>
            <ta e="T322" id="Seg_5786" s="T321">aux</ta>
            <ta e="T323" id="Seg_5787" s="T322">pers</ta>
            <ta e="T324" id="Seg_5788" s="T323">pers</ta>
            <ta e="T325" id="Seg_5789" s="T324">dempro</ta>
            <ta e="T326" id="Seg_5790" s="T325">n</ta>
            <ta e="T327" id="Seg_5791" s="T326">ptcl</ta>
            <ta e="T328" id="Seg_5792" s="T327">dempro</ta>
            <ta e="T329" id="Seg_5793" s="T328">v</ta>
            <ta e="T330" id="Seg_5794" s="T329">cardnum</ta>
            <ta e="T331" id="Seg_5795" s="T330">n</ta>
            <ta e="T332" id="Seg_5796" s="T331">dempro</ta>
            <ta e="T333" id="Seg_5797" s="T332">adv</ta>
            <ta e="T334" id="Seg_5798" s="T333">cardnum</ta>
            <ta e="T335" id="Seg_5799" s="T334">ptcl</ta>
            <ta e="T336" id="Seg_5800" s="T335">n</ta>
            <ta e="T337" id="Seg_5801" s="T336">ptcl</ta>
            <ta e="T338" id="Seg_5802" s="T337">cardnum</ta>
            <ta e="T339" id="Seg_5803" s="T338">n</ta>
            <ta e="T340" id="Seg_5804" s="T339">v</ta>
            <ta e="T341" id="Seg_5805" s="T340">adv</ta>
            <ta e="T342" id="Seg_5806" s="T341">v</ta>
            <ta e="T343" id="Seg_5807" s="T342">dempro</ta>
            <ta e="T344" id="Seg_5808" s="T343">n</ta>
            <ta e="T347" id="Seg_5809" s="T346">n</ta>
            <ta e="T348" id="Seg_5810" s="T347">post</ta>
            <ta e="T349" id="Seg_5811" s="T348">n</ta>
            <ta e="T350" id="Seg_5812" s="T349">n</ta>
            <ta e="T351" id="Seg_5813" s="T350">post</ta>
            <ta e="T352" id="Seg_5814" s="T351">adj</ta>
            <ta e="T353" id="Seg_5815" s="T352">n</ta>
            <ta e="T354" id="Seg_5816" s="T353">dempro</ta>
            <ta e="T355" id="Seg_5817" s="T354">n</ta>
            <ta e="T356" id="Seg_5818" s="T355">ptcl</ta>
            <ta e="T357" id="Seg_5819" s="T356">v</ta>
            <ta e="T358" id="Seg_5820" s="T357">ptcl</ta>
            <ta e="T359" id="Seg_5821" s="T358">ptcl</ta>
            <ta e="T360" id="Seg_5822" s="T359">que</ta>
            <ta e="T361" id="Seg_5823" s="T360">ptcl</ta>
            <ta e="T362" id="Seg_5824" s="T361">cop</ta>
            <ta e="T363" id="Seg_5825" s="T362">adv</ta>
            <ta e="T364" id="Seg_5826" s="T363">propr</ta>
            <ta e="T365" id="Seg_5827" s="T364">n</ta>
            <ta e="T366" id="Seg_5828" s="T365">v</ta>
            <ta e="T367" id="Seg_5829" s="T366">conj</ta>
            <ta e="T368" id="Seg_5830" s="T367">v</ta>
            <ta e="T369" id="Seg_5831" s="T368">v</ta>
            <ta e="T370" id="Seg_5832" s="T369">adv</ta>
            <ta e="T371" id="Seg_5833" s="T370">ptcl</ta>
            <ta e="T372" id="Seg_5834" s="T371">n</ta>
            <ta e="T373" id="Seg_5835" s="T372">v</ta>
            <ta e="T374" id="Seg_5836" s="T373">ptcl</ta>
            <ta e="T375" id="Seg_5837" s="T374">n</ta>
            <ta e="T376" id="Seg_5838" s="T375">post</ta>
            <ta e="T377" id="Seg_5839" s="T376">n</ta>
            <ta e="T378" id="Seg_5840" s="T377">v</ta>
            <ta e="T379" id="Seg_5841" s="T378">ptcl</ta>
            <ta e="T380" id="Seg_5842" s="T379">que</ta>
            <ta e="T381" id="Seg_5843" s="T380">v</ta>
            <ta e="T382" id="Seg_5844" s="T381">v</ta>
            <ta e="T383" id="Seg_5845" s="T382">post</ta>
            <ta e="T384" id="Seg_5846" s="T383">n</ta>
            <ta e="T385" id="Seg_5847" s="T384">v</ta>
            <ta e="T386" id="Seg_5848" s="T385">post</ta>
            <ta e="T387" id="Seg_5849" s="T386">n</ta>
            <ta e="T388" id="Seg_5850" s="T387">adv</ta>
            <ta e="T389" id="Seg_5851" s="T388">adv</ta>
            <ta e="T390" id="Seg_5852" s="T389">v</ta>
            <ta e="T391" id="Seg_5853" s="T390">adv</ta>
            <ta e="T392" id="Seg_5854" s="T391">dempro</ta>
            <ta e="T393" id="Seg_5855" s="T392">adv</ta>
            <ta e="T394" id="Seg_5856" s="T393">dempro</ta>
            <ta e="T395" id="Seg_5857" s="T394">adv</ta>
            <ta e="T396" id="Seg_5858" s="T395">n</ta>
            <ta e="T397" id="Seg_5859" s="T396">n</ta>
            <ta e="T398" id="Seg_5860" s="T397">ptcl</ta>
            <ta e="T399" id="Seg_5861" s="T398">v</ta>
            <ta e="T400" id="Seg_5862" s="T399">v</ta>
            <ta e="T401" id="Seg_5863" s="T400">n</ta>
            <ta e="T402" id="Seg_5864" s="T401">v</ta>
            <ta e="T403" id="Seg_5865" s="T402">post</ta>
            <ta e="T404" id="Seg_5866" s="T403">interj</ta>
            <ta e="T405" id="Seg_5867" s="T404">ptcl</ta>
            <ta e="T406" id="Seg_5868" s="T405">n</ta>
            <ta e="T407" id="Seg_5869" s="T406">ptcl</ta>
            <ta e="T408" id="Seg_5870" s="T407">v</ta>
            <ta e="T409" id="Seg_5871" s="T408">ptcl</ta>
            <ta e="T410" id="Seg_5872" s="T409">adj</ta>
            <ta e="T411" id="Seg_5873" s="T410">adv</ta>
            <ta e="T412" id="Seg_5874" s="T411">n</ta>
            <ta e="T413" id="Seg_5875" s="T412">v</ta>
            <ta e="T414" id="Seg_5876" s="T413">adv</ta>
            <ta e="T415" id="Seg_5877" s="T414">adv</ta>
            <ta e="T416" id="Seg_5878" s="T415">v</ta>
            <ta e="T417" id="Seg_5879" s="T416">n</ta>
            <ta e="T418" id="Seg_5880" s="T417">n</ta>
            <ta e="T419" id="Seg_5881" s="T418">interj</ta>
            <ta e="T420" id="Seg_5882" s="T419">adj</ta>
            <ta e="T421" id="Seg_5883" s="T420">adj</ta>
            <ta e="T422" id="Seg_5884" s="T421">adv</ta>
            <ta e="T423" id="Seg_5885" s="T422">n</ta>
            <ta e="T424" id="Seg_5886" s="T423">n</ta>
            <ta e="T425" id="Seg_5887" s="T424">post</ta>
            <ta e="T426" id="Seg_5888" s="T425">v</ta>
            <ta e="T427" id="Seg_5889" s="T426">aux</ta>
            <ta e="T428" id="Seg_5890" s="T427">ptcl</ta>
            <ta e="T429" id="Seg_5891" s="T428">adv</ta>
            <ta e="T430" id="Seg_5892" s="T429">n</ta>
            <ta e="T431" id="Seg_5893" s="T430">v</ta>
            <ta e="T432" id="Seg_5894" s="T431">ptcl</ta>
            <ta e="T433" id="Seg_5895" s="T432">que</ta>
            <ta e="T434" id="Seg_5896" s="T433">v</ta>
            <ta e="T435" id="Seg_5897" s="T434">pers</ta>
            <ta e="T436" id="Seg_5898" s="T435">v</ta>
            <ta e="T437" id="Seg_5899" s="T436">n</ta>
            <ta e="T438" id="Seg_5900" s="T437">adv</ta>
            <ta e="T439" id="Seg_5901" s="T438">v</ta>
            <ta e="T440" id="Seg_5902" s="T439">ptcl</ta>
            <ta e="T441" id="Seg_5903" s="T440">dempro</ta>
            <ta e="T442" id="Seg_5904" s="T441">n</ta>
            <ta e="T443" id="Seg_5905" s="T442">n</ta>
            <ta e="T444" id="Seg_5906" s="T443">dempro</ta>
            <ta e="T445" id="Seg_5907" s="T444">n</ta>
            <ta e="T446" id="Seg_5908" s="T445">n</ta>
            <ta e="T447" id="Seg_5909" s="T446">adj</ta>
            <ta e="T448" id="Seg_5910" s="T447">v</ta>
            <ta e="T449" id="Seg_5911" s="T448">ptcl</ta>
            <ta e="T450" id="Seg_5912" s="T449">v</ta>
            <ta e="T451" id="Seg_5913" s="T450">pers</ta>
            <ta e="T452" id="Seg_5914" s="T451">propr</ta>
            <ta e="T453" id="Seg_5915" s="T452">n</ta>
            <ta e="T454" id="Seg_5916" s="T453">adv</ta>
            <ta e="T455" id="Seg_5917" s="T454">v</ta>
            <ta e="T456" id="Seg_5918" s="T455">adv</ta>
            <ta e="T457" id="Seg_5919" s="T456">v</ta>
            <ta e="T458" id="Seg_5920" s="T457">quant</ta>
            <ta e="T459" id="Seg_5921" s="T458">n</ta>
            <ta e="T460" id="Seg_5922" s="T459">v</ta>
            <ta e="T461" id="Seg_5923" s="T460">v</ta>
            <ta e="T462" id="Seg_5924" s="T461">ptcl</ta>
            <ta e="T463" id="Seg_5925" s="T462">dempro</ta>
            <ta e="T464" id="Seg_5926" s="T463">post</ta>
            <ta e="T465" id="Seg_5927" s="T464">propr</ta>
            <ta e="T466" id="Seg_5928" s="T465">n</ta>
            <ta e="T467" id="Seg_5929" s="T466">post</ta>
            <ta e="T468" id="Seg_5930" s="T467">adj</ta>
            <ta e="T469" id="Seg_5931" s="T468">adv</ta>
            <ta e="T470" id="Seg_5932" s="T469">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_5933" s="T0">pro.h:E</ta>
            <ta e="T3" id="Seg_5934" s="T2">np:Th</ta>
            <ta e="T7" id="Seg_5935" s="T4">0.1.h:Poss pp:Com</ta>
            <ta e="T8" id="Seg_5936" s="T7">np:G</ta>
            <ta e="T9" id="Seg_5937" s="T8">0.1.h:A</ta>
            <ta e="T10" id="Seg_5938" s="T9">n:Time</ta>
            <ta e="T11" id="Seg_5939" s="T10">pro:Th</ta>
            <ta e="T14" id="Seg_5940" s="T13">0.1.h:Poss np:L</ta>
            <ta e="T17" id="Seg_5941" s="T16">0.1.h:Poss np.h:Poss</ta>
            <ta e="T18" id="Seg_5942" s="T17">np.h:A</ta>
            <ta e="T23" id="Seg_5943" s="T22">np:L</ta>
            <ta e="T25" id="Seg_5944" s="T24">np:L</ta>
            <ta e="T26" id="Seg_5945" s="T25">pro.h:Poss</ta>
            <ta e="T28" id="Seg_5946" s="T27">np:L</ta>
            <ta e="T30" id="Seg_5947" s="T28">0.3.h:Poss pp:Com</ta>
            <ta e="T35" id="Seg_5948" s="T34">np.h:A</ta>
            <ta e="T36" id="Seg_5949" s="T35">pro.h:Poss</ta>
            <ta e="T37" id="Seg_5950" s="T36">np:Th</ta>
            <ta e="T41" id="Seg_5951" s="T40">np:L</ta>
            <ta e="T44" id="Seg_5952" s="T43">pro:So</ta>
            <ta e="T49" id="Seg_5953" s="T48">np.h:A</ta>
            <ta e="T51" id="Seg_5954" s="T49">0.3.h:Poss pp:Com</ta>
            <ta e="T56" id="Seg_5955" s="T54">pp:Com</ta>
            <ta e="T58" id="Seg_5956" s="T57">np.h:A</ta>
            <ta e="T62" id="Seg_5957" s="T61">0.3.h:E</ta>
            <ta e="T65" id="Seg_5958" s="T64">0.3.h:Poss np:L</ta>
            <ta e="T67" id="Seg_5959" s="T66">np.h:A</ta>
            <ta e="T68" id="Seg_5960" s="T67">np.h:R</ta>
            <ta e="T69" id="Seg_5961" s="T68">np.h:R</ta>
            <ta e="T72" id="Seg_5962" s="T71">pro.h:A</ta>
            <ta e="T73" id="Seg_5963" s="T72">0.1.h:Poss</ta>
            <ta e="T76" id="Seg_5964" s="T75">0.2.h:A</ta>
            <ta e="T77" id="Seg_5965" s="T76">pro:G</ta>
            <ta e="T79" id="Seg_5966" s="T78">0.2.h:A</ta>
            <ta e="T81" id="Seg_5967" s="T79">0.1.h:Poss pp:Com</ta>
            <ta e="T82" id="Seg_5968" s="T81">0.2.h:A</ta>
            <ta e="T84" id="Seg_5969" s="T82">0.1.h:Poss pp:Com</ta>
            <ta e="T85" id="Seg_5970" s="T84">0.2.h:A</ta>
            <ta e="T86" id="Seg_5971" s="T85">0.1.h:Poss np.h:R</ta>
            <ta e="T87" id="Seg_5972" s="T86">0.2.h:A</ta>
            <ta e="T88" id="Seg_5973" s="T87">pro.h:Th</ta>
            <ta e="T89" id="Seg_5974" s="T88">0.1.h:Poss np.h:Th</ta>
            <ta e="T97" id="Seg_5975" s="T96">0.3.h:Th</ta>
            <ta e="T101" id="Seg_5976" s="T100">0.3.h:Poss</ta>
            <ta e="T102" id="Seg_5977" s="T101">pro.h:E</ta>
            <ta e="T107" id="Seg_5978" s="T106">0.3.h:A</ta>
            <ta e="T109" id="Seg_5979" s="T108">np.h:A</ta>
            <ta e="T114" id="Seg_5980" s="T113">np:Th</ta>
            <ta e="T119" id="Seg_5981" s="T118">n:Time</ta>
            <ta e="T124" id="Seg_5982" s="T123">n:Time</ta>
            <ta e="T126" id="Seg_5983" s="T125">0.3.h:A</ta>
            <ta e="T127" id="Seg_5984" s="T126">0.3.h:A</ta>
            <ta e="T130" id="Seg_5985" s="T129">pro.h:Poss</ta>
            <ta e="T134" id="Seg_5986" s="T133">np:Th</ta>
            <ta e="T135" id="Seg_5987" s="T134">pro:Com</ta>
            <ta e="T137" id="Seg_5988" s="T136">0.3.h:A</ta>
            <ta e="T138" id="Seg_5989" s="T137">np:G</ta>
            <ta e="T139" id="Seg_5990" s="T138">0.3.h:Th</ta>
            <ta e="T142" id="Seg_5991" s="T141">0.3.h:Poss np:Th</ta>
            <ta e="T143" id="Seg_5992" s="T142">0.3.h:Poss</ta>
            <ta e="T144" id="Seg_5993" s="T143">0.3.h:A</ta>
            <ta e="T151" id="Seg_5994" s="T150">np.h:A</ta>
            <ta e="T154" id="Seg_5995" s="T153">0.1.h:A</ta>
            <ta e="T155" id="Seg_5996" s="T154">pro:G</ta>
            <ta e="T159" id="Seg_5997" s="T158">pro.h:R</ta>
            <ta e="T160" id="Seg_5998" s="T159">0.2.h:Poss np.h:Th</ta>
            <ta e="T161" id="Seg_5999" s="T160">0.1.h:A</ta>
            <ta e="T165" id="Seg_6000" s="T164">pro.h:A</ta>
            <ta e="T166" id="Seg_6001" s="T165">pro.h:E</ta>
            <ta e="T169" id="Seg_6002" s="T168">pro.h:Th</ta>
            <ta e="T171" id="Seg_6003" s="T170">0.1.h:A</ta>
            <ta e="T177" id="Seg_6004" s="T176">np.h:Poss</ta>
            <ta e="T178" id="Seg_6005" s="T177">np.h:A</ta>
            <ta e="T181" id="Seg_6006" s="T180">0.3.h:A</ta>
            <ta e="T182" id="Seg_6007" s="T181">0.3.h:E</ta>
            <ta e="T186" id="Seg_6008" s="T185">np.h:E</ta>
            <ta e="T187" id="Seg_6009" s="T186">np:Th</ta>
            <ta e="T189" id="Seg_6010" s="T188">np.h:Poss</ta>
            <ta e="T190" id="Seg_6011" s="T189">np.h:A</ta>
            <ta e="T191" id="Seg_6012" s="T190">pro:Th</ta>
            <ta e="T197" id="Seg_6013" s="T196">0.3.h:A</ta>
            <ta e="T200" id="Seg_6014" s="T199">np:Path</ta>
            <ta e="T202" id="Seg_6015" s="T201">0.3.h:E</ta>
            <ta e="T204" id="Seg_6016" s="T203">0.3.h:A</ta>
            <ta e="T206" id="Seg_6017" s="T205">0.3.h:A</ta>
            <ta e="T211" id="Seg_6018" s="T210">np:Th</ta>
            <ta e="T213" id="Seg_6019" s="T212">np.h:A</ta>
            <ta e="T221" id="Seg_6020" s="T220">np.h:Poss</ta>
            <ta e="T222" id="Seg_6021" s="T221">np.h:A</ta>
            <ta e="T224" id="Seg_6022" s="T223">pro:Th</ta>
            <ta e="T225" id="Seg_6023" s="T224">0.2.h:A</ta>
            <ta e="T234" id="Seg_6024" s="T233">np.h:A</ta>
            <ta e="T235" id="Seg_6025" s="T234">pro.h:E</ta>
            <ta e="T238" id="Seg_6026" s="T237">pro.h:E</ta>
            <ta e="T240" id="Seg_6027" s="T239">0.1.h:E</ta>
            <ta e="T242" id="Seg_6028" s="T241">0.3:Th</ta>
            <ta e="T243" id="Seg_6029" s="T242">pro.h:E</ta>
            <ta e="T247" id="Seg_6030" s="T246">np:Th</ta>
            <ta e="T248" id="Seg_6031" s="T247">pro.h:E</ta>
            <ta e="T252" id="Seg_6032" s="T251">np:Th</ta>
            <ta e="T253" id="Seg_6033" s="T252">0.3.h:A</ta>
            <ta e="T254" id="Seg_6034" s="T253">np.h:A</ta>
            <ta e="T261" id="Seg_6035" s="T260">0.2.h:E</ta>
            <ta e="T263" id="Seg_6036" s="T262">np:Th</ta>
            <ta e="T270" id="Seg_6037" s="T269">np:G</ta>
            <ta e="T275" id="Seg_6038" s="T274">0.2.h:A</ta>
            <ta e="T278" id="Seg_6039" s="T277">np:Th</ta>
            <ta e="T279" id="Seg_6040" s="T278">0.2.h:E</ta>
            <ta e="T282" id="Seg_6041" s="T281">pro.h:A</ta>
            <ta e="T283" id="Seg_6042" s="T282">0.1.h:Poss np:Th</ta>
            <ta e="T287" id="Seg_6043" s="T286">0.2.h:E</ta>
            <ta e="T290" id="Seg_6044" s="T289">np:Th</ta>
            <ta e="T292" id="Seg_6045" s="T291">pro.h:Poss</ta>
            <ta e="T293" id="Seg_6046" s="T292">np.h:Th</ta>
            <ta e="T294" id="Seg_6047" s="T293">pro.h:R</ta>
            <ta e="T295" id="Seg_6048" s="T294">0.1.h:A</ta>
            <ta e="T297" id="Seg_6049" s="T296">np.h:A</ta>
            <ta e="T301" id="Seg_6050" s="T300">0.3.h:A</ta>
            <ta e="T304" id="Seg_6051" s="T303">pro.h:A</ta>
            <ta e="T305" id="Seg_6052" s="T304">pro.h:R</ta>
            <ta e="T319" id="Seg_6053" s="T318">pro.h:E</ta>
            <ta e="T325" id="Seg_6054" s="T324">pro:Th</ta>
            <ta e="T326" id="Seg_6055" s="T325">np.h:E</ta>
            <ta e="T331" id="Seg_6056" s="T330">np:Poss</ta>
            <ta e="T336" id="Seg_6057" s="T335">np:Th</ta>
            <ta e="T343" id="Seg_6058" s="T342">pro:Th</ta>
            <ta e="T344" id="Seg_6059" s="T343">np.h:Poss</ta>
            <ta e="T349" id="Seg_6060" s="T348">np.h:Poss</ta>
            <ta e="T354" id="Seg_6061" s="T353">pro:Th</ta>
            <ta e="T355" id="Seg_6062" s="T354">np.h:E</ta>
            <ta e="T366" id="Seg_6063" s="T365">0.3.h:E</ta>
            <ta e="T368" id="Seg_6064" s="T367">0.3.h:E</ta>
            <ta e="T369" id="Seg_6065" s="T368">0.3.h:E</ta>
            <ta e="T373" id="Seg_6066" s="T372">0.3.h:A</ta>
            <ta e="T376" id="Seg_6067" s="T374">pp:G</ta>
            <ta e="T377" id="Seg_6068" s="T376">np.h:Th</ta>
            <ta e="T381" id="Seg_6069" s="T380">0.1.h:A</ta>
            <ta e="T382" id="Seg_6070" s="T381">0.3.h:E</ta>
            <ta e="T385" id="Seg_6071" s="T384">0.3.h:A</ta>
            <ta e="T387" id="Seg_6072" s="T386">0.3.h:Poss np:Th</ta>
            <ta e="T390" id="Seg_6073" s="T389">0.3.h:A</ta>
            <ta e="T397" id="Seg_6074" s="T396">np:Th</ta>
            <ta e="T399" id="Seg_6075" s="T398">0.3.h:A</ta>
            <ta e="T400" id="Seg_6076" s="T399">0.3.h:A</ta>
            <ta e="T402" id="Seg_6077" s="T401">0.3.h:A</ta>
            <ta e="T406" id="Seg_6078" s="T405">np.h:E</ta>
            <ta e="T412" id="Seg_6079" s="T411">0.3.h:Poss np:Th</ta>
            <ta e="T413" id="Seg_6080" s="T412">0.3.h:A</ta>
            <ta e="T416" id="Seg_6081" s="T415">0.3.h:A</ta>
            <ta e="T418" id="Seg_6082" s="T417">np:Th</ta>
            <ta e="T423" id="Seg_6083" s="T422">np:Ins</ta>
            <ta e="T424" id="Seg_6084" s="T423">np:Th</ta>
            <ta e="T427" id="Seg_6085" s="T426">0.3.h:A</ta>
            <ta e="T430" id="Seg_6086" s="T429">np.h:A</ta>
            <ta e="T435" id="Seg_6087" s="T434">pro.h:A</ta>
            <ta e="T437" id="Seg_6088" s="T436">0.1.h:Poss np:Th</ta>
            <ta e="T439" id="Seg_6089" s="T438">0.1.h:A</ta>
            <ta e="T443" id="Seg_6090" s="T442">np.h:E</ta>
            <ta e="T446" id="Seg_6091" s="T445">np.h:E</ta>
            <ta e="T447" id="Seg_6092" s="T446">np:Th</ta>
            <ta e="T450" id="Seg_6093" s="T449">0.1.h:A</ta>
            <ta e="T451" id="Seg_6094" s="T450">pro.h:R</ta>
            <ta e="T453" id="Seg_6095" s="T452">0.1.h:Poss np.h:Th</ta>
            <ta e="T455" id="Seg_6096" s="T454">0.2.h:Th</ta>
            <ta e="T457" id="Seg_6097" s="T456">0.2.h:Th</ta>
            <ta e="T459" id="Seg_6098" s="T458">np.h:P</ta>
            <ta e="T460" id="Seg_6099" s="T459">0.2.h:A</ta>
            <ta e="T461" id="Seg_6100" s="T460">0.3.h:A</ta>
            <ta e="T465" id="Seg_6101" s="T464">np.h:Th</ta>
            <ta e="T467" id="Seg_6102" s="T465">pp:Com</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1" id="Seg_6103" s="T0">pro.h:S</ta>
            <ta e="T3" id="Seg_6104" s="T2">np:O</ta>
            <ta e="T4" id="Seg_6105" s="T3">v:pred</ta>
            <ta e="T9" id="Seg_6106" s="T7">s:rel</ta>
            <ta e="T11" id="Seg_6107" s="T10">pro:S</ta>
            <ta e="T15" id="Seg_6108" s="T14">ptcl:pred</ta>
            <ta e="T18" id="Seg_6109" s="T17">np.h:S</ta>
            <ta e="T21" id="Seg_6110" s="T20">v:pred</ta>
            <ta e="T32" id="Seg_6111" s="T31">v:pred</ta>
            <ta e="T35" id="Seg_6112" s="T34">np.h:S</ta>
            <ta e="T37" id="Seg_6113" s="T36">np:S</ta>
            <ta e="T38" id="Seg_6114" s="T37">n:pred</ta>
            <ta e="T39" id="Seg_6115" s="T38">cop</ta>
            <ta e="T47" id="Seg_6116" s="T46">v:pred</ta>
            <ta e="T49" id="Seg_6117" s="T48">np.h:S</ta>
            <ta e="T58" id="Seg_6118" s="T57">np.h:S</ta>
            <ta e="T60" id="Seg_6119" s="T59">v:pred</ta>
            <ta e="T62" id="Seg_6120" s="T61">0.3.h:S v:pred</ta>
            <ta e="T67" id="Seg_6121" s="T66">np.h:S</ta>
            <ta e="T68" id="Seg_6122" s="T67">np.h:O</ta>
            <ta e="T69" id="Seg_6123" s="T68">np.h:O</ta>
            <ta e="T70" id="Seg_6124" s="T69">v:pred</ta>
            <ta e="T72" id="Seg_6125" s="T71">pro.h:S</ta>
            <ta e="T75" id="Seg_6126" s="T74">v:pred</ta>
            <ta e="T76" id="Seg_6127" s="T75">0.2.h:S v:pred</ta>
            <ta e="T79" id="Seg_6128" s="T78">0.2.h:S v:pred</ta>
            <ta e="T82" id="Seg_6129" s="T81">0.2.h:S v:pred</ta>
            <ta e="T85" id="Seg_6130" s="T84">0.2.h:S v:pred</ta>
            <ta e="T87" id="Seg_6131" s="T86">0.2.h:S v:pred</ta>
            <ta e="T88" id="Seg_6132" s="T87">pro.h:O</ta>
            <ta e="T89" id="Seg_6133" s="T88">np.h:S</ta>
            <ta e="T95" id="Seg_6134" s="T94">n:pred</ta>
            <ta e="T97" id="Seg_6135" s="T96">0.3.h:S cop</ta>
            <ta e="T98" id="Seg_6136" s="T97">adj:pred</ta>
            <ta e="T102" id="Seg_6137" s="T101">pro.h:S</ta>
            <ta e="T103" id="Seg_6138" s="T102">v:pred</ta>
            <ta e="T107" id="Seg_6139" s="T106">0.3.h:S v:pred</ta>
            <ta e="T109" id="Seg_6140" s="T108">np.h:S</ta>
            <ta e="T116" id="Seg_6141" s="T111">s:temp</ta>
            <ta e="T127" id="Seg_6142" s="T125">s:adv</ta>
            <ta e="T129" id="Seg_6143" s="T128">v:pred</ta>
            <ta e="T134" id="Seg_6144" s="T133">0.3.h:S adj:pred</ta>
            <ta e="T137" id="Seg_6145" s="T136">0.3.h:S v:pred</ta>
            <ta e="T139" id="Seg_6146" s="T138">s:temp</ta>
            <ta e="T145" id="Seg_6147" s="T139">s:temp</ta>
            <ta e="T149" id="Seg_6148" s="T148">v:pred</ta>
            <ta e="T151" id="Seg_6149" s="T150">np.h:S</ta>
            <ta e="T154" id="Seg_6150" s="T153">0.1.h:S v:pred</ta>
            <ta e="T160" id="Seg_6151" s="T159">np.h:O</ta>
            <ta e="T161" id="Seg_6152" s="T160">0.1.h:S v:pred</ta>
            <ta e="T164" id="Seg_6153" s="T163">v:pred</ta>
            <ta e="T165" id="Seg_6154" s="T164">pro.h:S</ta>
            <ta e="T166" id="Seg_6155" s="T165">pro.h:S</ta>
            <ta e="T168" id="Seg_6156" s="T167">v:pred</ta>
            <ta e="T169" id="Seg_6157" s="T168">pro.h:O</ta>
            <ta e="T171" id="Seg_6158" s="T170">0.1.h:S v:pred</ta>
            <ta e="T175" id="Seg_6159" s="T174">v:pred</ta>
            <ta e="T176" id="Seg_6160" s="T175">s:adv</ta>
            <ta e="T178" id="Seg_6161" s="T177">np.h:S</ta>
            <ta e="T181" id="Seg_6162" s="T180">s:comp</ta>
            <ta e="T182" id="Seg_6163" s="T181">0.3.h:S v:pred</ta>
            <ta e="T185" id="Seg_6164" s="T184">v:pred</ta>
            <ta e="T186" id="Seg_6165" s="T185">np.h:S</ta>
            <ta e="T187" id="Seg_6166" s="T186">np:S</ta>
            <ta e="T188" id="Seg_6167" s="T187">cop</ta>
            <ta e="T190" id="Seg_6168" s="T189">np.h:S</ta>
            <ta e="T191" id="Seg_6169" s="T190">pro:O</ta>
            <ta e="T193" id="Seg_6170" s="T192">v:pred</ta>
            <ta e="T196" id="Seg_6171" s="T194">s:adv</ta>
            <ta e="T197" id="Seg_6172" s="T196">0.3.h:S v:pred</ta>
            <ta e="T202" id="Seg_6173" s="T201">0.3.h:S v:pred</ta>
            <ta e="T204" id="Seg_6174" s="T203">0.3.h:S v:pred</ta>
            <ta e="T206" id="Seg_6175" s="T205">0.3.h:S v:pred</ta>
            <ta e="T211" id="Seg_6176" s="T210">np:O</ta>
            <ta e="T213" id="Seg_6177" s="T212">np.h:S</ta>
            <ta e="T217" id="Seg_6178" s="T216">v:pred</ta>
            <ta e="T220" id="Seg_6179" s="T219">v:pred</ta>
            <ta e="T222" id="Seg_6180" s="T221">np.h:S</ta>
            <ta e="T224" id="Seg_6181" s="T223">pro:O</ta>
            <ta e="T225" id="Seg_6182" s="T224">0.2.h:S v:pred</ta>
            <ta e="T230" id="Seg_6183" s="T229">v:pred</ta>
            <ta e="T234" id="Seg_6184" s="T233">np.h:S</ta>
            <ta e="T238" id="Seg_6185" s="T237">pro.h:S</ta>
            <ta e="T239" id="Seg_6186" s="T238">v:pred</ta>
            <ta e="T240" id="Seg_6187" s="T239">0.1.h:S v:pred</ta>
            <ta e="T242" id="Seg_6188" s="T241">0.3:S n:pred</ta>
            <ta e="T243" id="Seg_6189" s="T242">pro.h:S</ta>
            <ta e="T244" id="Seg_6190" s="T243">v:pred</ta>
            <ta e="T247" id="Seg_6191" s="T246">np:O</ta>
            <ta e="T248" id="Seg_6192" s="T247">pro.h:S</ta>
            <ta e="T249" id="Seg_6193" s="T248">v:pred</ta>
            <ta e="T252" id="Seg_6194" s="T251">np:O</ta>
            <ta e="T253" id="Seg_6195" s="T252">0.3.h:S v:pred</ta>
            <ta e="T254" id="Seg_6196" s="T253">np.h:S</ta>
            <ta e="T257" id="Seg_6197" s="T256">s:adv</ta>
            <ta e="T258" id="Seg_6198" s="T257">s:adv</ta>
            <ta e="T259" id="Seg_6199" s="T258">v:pred</ta>
            <ta e="T261" id="Seg_6200" s="T260">0.2.h:S v:pred</ta>
            <ta e="T263" id="Seg_6201" s="T262">np:O</ta>
            <ta e="T275" id="Seg_6202" s="T265">s:temp</ta>
            <ta e="T278" id="Seg_6203" s="T277">np:O</ta>
            <ta e="T279" id="Seg_6204" s="T278">0.2.h:S v:pred</ta>
            <ta e="T282" id="Seg_6205" s="T281">pro.h:S</ta>
            <ta e="T283" id="Seg_6206" s="T282">np:O</ta>
            <ta e="T284" id="Seg_6207" s="T283">v:pred</ta>
            <ta e="T290" id="Seg_6208" s="T286">s:cond</ta>
            <ta e="T293" id="Seg_6209" s="T292">np.h:O</ta>
            <ta e="T295" id="Seg_6210" s="T294">0.1.h:S v:pred</ta>
            <ta e="T297" id="Seg_6211" s="T296">np.h:S</ta>
            <ta e="T299" id="Seg_6212" s="T298">v:pred</ta>
            <ta e="T301" id="Seg_6213" s="T300">0.3.h:S v:pred</ta>
            <ta e="T304" id="Seg_6214" s="T303">pro.h:S</ta>
            <ta e="T306" id="Seg_6215" s="T305">v:pred</ta>
            <ta e="T319" id="Seg_6216" s="T318">pro.h:S</ta>
            <ta e="T322" id="Seg_6217" s="T321">v:pred</ta>
            <ta e="T325" id="Seg_6218" s="T324">pro:O</ta>
            <ta e="T326" id="Seg_6219" s="T325">np.h:S</ta>
            <ta e="T329" id="Seg_6220" s="T328">v:pred</ta>
            <ta e="T331" id="Seg_6221" s="T330">np:S</ta>
            <ta e="T337" id="Seg_6222" s="T336">ptcl:pred</ta>
            <ta e="T342" id="Seg_6223" s="T340">s:cond</ta>
            <ta e="T343" id="Seg_6224" s="T342">pro:S</ta>
            <ta e="T347" id="Seg_6225" s="T346">n:pred</ta>
            <ta e="T354" id="Seg_6226" s="T353">pro:O</ta>
            <ta e="T355" id="Seg_6227" s="T354">np.h:S</ta>
            <ta e="T357" id="Seg_6228" s="T356">v:pred</ta>
            <ta e="T366" id="Seg_6229" s="T365">s:comp</ta>
            <ta e="T368" id="Seg_6230" s="T367">0.3.h:S v:pred</ta>
            <ta e="T369" id="Seg_6231" s="T368">0.3.h:S v:pred</ta>
            <ta e="T372" id="Seg_6232" s="T371">np:O</ta>
            <ta e="T373" id="Seg_6233" s="T372">0.3.h:S v:pred</ta>
            <ta e="T377" id="Seg_6234" s="T376">np.h:S</ta>
            <ta e="T378" id="Seg_6235" s="T377">v:pred</ta>
            <ta e="T381" id="Seg_6236" s="T380">0.1.h:S v:pred</ta>
            <ta e="T383" id="Seg_6237" s="T381">s:temp</ta>
            <ta e="T386" id="Seg_6238" s="T383">s:adv</ta>
            <ta e="T387" id="Seg_6239" s="T386">np:O</ta>
            <ta e="T390" id="Seg_6240" s="T389">0.3.h:S v:pred</ta>
            <ta e="T397" id="Seg_6241" s="T396">np:O</ta>
            <ta e="T399" id="Seg_6242" s="T398">0.3.h:S v:pred</ta>
            <ta e="T400" id="Seg_6243" s="T399">0.3.h:S v:pred</ta>
            <ta e="T403" id="Seg_6244" s="T400">s:adv</ta>
            <ta e="T406" id="Seg_6245" s="T405">np.h:S</ta>
            <ta e="T408" id="Seg_6246" s="T407">v:pred</ta>
            <ta e="T412" id="Seg_6247" s="T411">np:O</ta>
            <ta e="T413" id="Seg_6248" s="T412">0.3.h:S v:pred</ta>
            <ta e="T416" id="Seg_6249" s="T415">0.3.h:S v:pred</ta>
            <ta e="T418" id="Seg_6250" s="T417">np:O</ta>
            <ta e="T424" id="Seg_6251" s="T423">np:O</ta>
            <ta e="T427" id="Seg_6252" s="T426">0.3.h:S v:pred</ta>
            <ta e="T430" id="Seg_6253" s="T429">np.h:S</ta>
            <ta e="T431" id="Seg_6254" s="T430">v:pred</ta>
            <ta e="T435" id="Seg_6255" s="T434">pro.h:S</ta>
            <ta e="T436" id="Seg_6256" s="T435">v:pred</ta>
            <ta e="T437" id="Seg_6257" s="T436">np:O</ta>
            <ta e="T439" id="Seg_6258" s="T438">0.1.h:S v:pred</ta>
            <ta e="T443" id="Seg_6259" s="T442">np.h:S</ta>
            <ta e="T446" id="Seg_6260" s="T445">np.h:S</ta>
            <ta e="T447" id="Seg_6261" s="T446">np:O</ta>
            <ta e="T448" id="Seg_6262" s="T447">v:pred</ta>
            <ta e="T450" id="Seg_6263" s="T449">0.1.h:S v:pred</ta>
            <ta e="T453" id="Seg_6264" s="T452">np.h:O</ta>
            <ta e="T455" id="Seg_6265" s="T454">0.2.h:S v:pred</ta>
            <ta e="T457" id="Seg_6266" s="T456">0.2.h:S v:pred</ta>
            <ta e="T459" id="Seg_6267" s="T458">np.h:O</ta>
            <ta e="T460" id="Seg_6268" s="T459">0.2.h:S v:pred</ta>
            <ta e="T461" id="Seg_6269" s="T460">0.3.h:S v:pred</ta>
            <ta e="T465" id="Seg_6270" s="T464">np.h:S</ta>
            <ta e="T470" id="Seg_6271" s="T469">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST">
            <ta e="T1" id="Seg_6272" s="T0">accs-sit</ta>
            <ta e="T3" id="Seg_6273" s="T2">accs-sit</ta>
            <ta e="T5" id="Seg_6274" s="T4">accs-inf</ta>
            <ta e="T8" id="Seg_6275" s="T7">accs-inf</ta>
            <ta e="T10" id="Seg_6276" s="T9">accs-inf</ta>
            <ta e="T11" id="Seg_6277" s="T10">giv-active</ta>
            <ta e="T14" id="Seg_6278" s="T13">accs-inf</ta>
            <ta e="T17" id="Seg_6279" s="T16">accs-inf</ta>
            <ta e="T18" id="Seg_6280" s="T17">accs-inf</ta>
            <ta e="T23" id="Seg_6281" s="T22">accs-gen</ta>
            <ta e="T25" id="Seg_6282" s="T24">new</ta>
            <ta e="T28" id="Seg_6283" s="T27">giv-active</ta>
            <ta e="T29" id="Seg_6284" s="T28">accs-inf</ta>
            <ta e="T35" id="Seg_6285" s="T34">new</ta>
            <ta e="T36" id="Seg_6286" s="T35">giv-active</ta>
            <ta e="T37" id="Seg_6287" s="T36">accs-inf</ta>
            <ta e="T41" id="Seg_6288" s="T40">accs-inf</ta>
            <ta e="T44" id="Seg_6289" s="T43">accs-aggr</ta>
            <ta e="T49" id="Seg_6290" s="T48">new</ta>
            <ta e="T50" id="Seg_6291" s="T49">accs-inf</ta>
            <ta e="T55" id="Seg_6292" s="T54">giv-active</ta>
            <ta e="T58" id="Seg_6293" s="T57">giv-active</ta>
            <ta e="T62" id="Seg_6294" s="T61">0.accs-aggr</ta>
            <ta e="T65" id="Seg_6295" s="T64">accs-aggr</ta>
            <ta e="T67" id="Seg_6296" s="T66">giv-active</ta>
            <ta e="T68" id="Seg_6297" s="T67">giv-active</ta>
            <ta e="T69" id="Seg_6298" s="T68">giv-active</ta>
            <ta e="T70" id="Seg_6299" s="T69">quot-sp</ta>
            <ta e="T72" id="Seg_6300" s="T71">giv-active-Q</ta>
            <ta e="T73" id="Seg_6301" s="T72">accs-aggr-Q</ta>
            <ta e="T76" id="Seg_6302" s="T75">0.giv-active-Q</ta>
            <ta e="T77" id="Seg_6303" s="T76">giv-active-Q</ta>
            <ta e="T79" id="Seg_6304" s="T78">0.giv-active-Q</ta>
            <ta e="T80" id="Seg_6305" s="T79">giv-active-Q</ta>
            <ta e="T82" id="Seg_6306" s="T81">0.giv-active-Q</ta>
            <ta e="T83" id="Seg_6307" s="T82">accs-inf-Q</ta>
            <ta e="T85" id="Seg_6308" s="T84">0.giv-active-Q</ta>
            <ta e="T86" id="Seg_6309" s="T85">giv-active-Q</ta>
            <ta e="T87" id="Seg_6310" s="T86">0.giv-active-Q</ta>
            <ta e="T88" id="Seg_6311" s="T87">giv-inactive-Q</ta>
            <ta e="T89" id="Seg_6312" s="T88">giv-active-Q</ta>
            <ta e="T97" id="Seg_6313" s="T96">0.giv-active-Q</ta>
            <ta e="T101" id="Seg_6314" s="T100">accs-inf-Q</ta>
            <ta e="T102" id="Seg_6315" s="T101">0.giv-active-Q</ta>
            <ta e="T107" id="Seg_6316" s="T106">0.giv-inactive</ta>
            <ta e="T109" id="Seg_6317" s="T108">giv-active</ta>
            <ta e="T119" id="Seg_6318" s="T118">new</ta>
            <ta e="T124" id="Seg_6319" s="T123">new</ta>
            <ta e="T126" id="Seg_6320" s="T125">0.giv-active</ta>
            <ta e="T127" id="Seg_6321" s="T126">0.giv-active</ta>
            <ta e="T134" id="Seg_6322" s="T133">0.giv-active new</ta>
            <ta e="T135" id="Seg_6323" s="T134">giv-active</ta>
            <ta e="T137" id="Seg_6324" s="T136">0.giv-active</ta>
            <ta e="T138" id="Seg_6325" s="T137">giv-inactive</ta>
            <ta e="T139" id="Seg_6326" s="T138">0.giv-active</ta>
            <ta e="T140" id="Seg_6327" s="T139">accs-sit</ta>
            <ta e="T142" id="Seg_6328" s="T141">accs-inf</ta>
            <ta e="T143" id="Seg_6329" s="T142">giv-inactive</ta>
            <ta e="T144" id="Seg_6330" s="T143">0.giv-active</ta>
            <ta e="T148" id="Seg_6331" s="T147">giv-active-Q</ta>
            <ta e="T149" id="Seg_6332" s="T148">quot-sp</ta>
            <ta e="T151" id="Seg_6333" s="T150">giv-active</ta>
            <ta e="T154" id="Seg_6334" s="T153">0.giv-active-Q</ta>
            <ta e="T155" id="Seg_6335" s="T154">giv-active-Q</ta>
            <ta e="T157" id="Seg_6336" s="T156">accs-inf-Q</ta>
            <ta e="T158" id="Seg_6337" s="T157">giv-inactive-Q</ta>
            <ta e="T159" id="Seg_6338" s="T158">giv-active-Q</ta>
            <ta e="T160" id="Seg_6339" s="T159">giv-inactive-Q</ta>
            <ta e="T161" id="Seg_6340" s="T160">0.giv-active-Q</ta>
            <ta e="T165" id="Seg_6341" s="T164">giv-active-Q</ta>
            <ta e="T166" id="Seg_6342" s="T165">giv-active-Q</ta>
            <ta e="T169" id="Seg_6343" s="T168">giv-active-Q</ta>
            <ta e="T171" id="Seg_6344" s="T170">0.giv-active-Q</ta>
            <ta e="T175" id="Seg_6345" s="T174">quot-sp</ta>
            <ta e="T178" id="Seg_6346" s="T177">giv-inactive</ta>
            <ta e="T181" id="Seg_6347" s="T180">0.giv-active-Q</ta>
            <ta e="T182" id="Seg_6348" s="T181">0.giv-active-Q</ta>
            <ta e="T185" id="Seg_6349" s="T184">quot-th</ta>
            <ta e="T186" id="Seg_6350" s="T185">giv-inactive</ta>
            <ta e="T189" id="Seg_6351" s="T188">giv-inactive</ta>
            <ta e="T190" id="Seg_6352" s="T189">giv-inactive</ta>
            <ta e="T191" id="Seg_6353" s="T190">new</ta>
            <ta e="T197" id="Seg_6354" s="T196">0.giv-active</ta>
            <ta e="T199" id="Seg_6355" s="T198">accs-sit</ta>
            <ta e="T200" id="Seg_6356" s="T199">accs-inf</ta>
            <ta e="T202" id="Seg_6357" s="T201">0.giv-active 0.quot-th</ta>
            <ta e="T204" id="Seg_6358" s="T203">0.giv-inactive-Q</ta>
            <ta e="T206" id="Seg_6359" s="T205">0.giv-active-Q</ta>
            <ta e="T211" id="Seg_6360" s="T210">giv-inactive-Q</ta>
            <ta e="T213" id="Seg_6361" s="T212">giv-active</ta>
            <ta e="T220" id="Seg_6362" s="T219">quot-sp</ta>
            <ta e="T222" id="Seg_6363" s="T221">giv-inactive</ta>
            <ta e="T225" id="Seg_6364" s="T224">0.giv-active-Q</ta>
            <ta e="T230" id="Seg_6365" s="T229">quot-sp</ta>
            <ta e="T234" id="Seg_6366" s="T233">giv-active</ta>
            <ta e="T235" id="Seg_6367" s="T234">giv-active-Q</ta>
            <ta e="T238" id="Seg_6368" s="T237">giv-active-Q</ta>
            <ta e="T240" id="Seg_6369" s="T239">0.giv-active-Q</ta>
            <ta e="T242" id="Seg_6370" s="T241">0.giv-inactive-Q</ta>
            <ta e="T247" id="Seg_6371" s="T246">giv-active-Q</ta>
            <ta e="T252" id="Seg_6372" s="T251">giv-active-Q</ta>
            <ta e="T253" id="Seg_6373" s="T252">0.giv-active 0.quot-sp</ta>
            <ta e="T254" id="Seg_6374" s="T253">giv-inactive</ta>
            <ta e="T259" id="Seg_6375" s="T258">quot-sp</ta>
            <ta e="T261" id="Seg_6376" s="T260">0.giv-inactive-Q</ta>
            <ta e="T263" id="Seg_6377" s="T262">giv-inactive-Q</ta>
            <ta e="T270" id="Seg_6378" s="T269">giv-active-Q</ta>
            <ta e="T275" id="Seg_6379" s="T274">0.giv-active-Q</ta>
            <ta e="T278" id="Seg_6380" s="T277">new-Q</ta>
            <ta e="T279" id="Seg_6381" s="T278">0.giv-active-Q</ta>
            <ta e="T282" id="Seg_6382" s="T281">giv-inactive-Q</ta>
            <ta e="T283" id="Seg_6383" s="T282">accs-inf-Q</ta>
            <ta e="T287" id="Seg_6384" s="T286">0.giv-inactive-Q</ta>
            <ta e="T290" id="Seg_6385" s="T289">giv-inactive-Q</ta>
            <ta e="T292" id="Seg_6386" s="T291">giv-active-Q</ta>
            <ta e="T293" id="Seg_6387" s="T292">giv-inactive-Q</ta>
            <ta e="T294" id="Seg_6388" s="T293">giv-active-Q</ta>
            <ta e="T297" id="Seg_6389" s="T296">giv-active</ta>
            <ta e="T301" id="Seg_6390" s="T300">0.giv-active</ta>
            <ta e="T304" id="Seg_6391" s="T303">giv-active</ta>
            <ta e="T306" id="Seg_6392" s="T305">quot-sp</ta>
            <ta e="T319" id="Seg_6393" s="T318">giv-inactive</ta>
            <ta e="T325" id="Seg_6394" s="T324">new</ta>
            <ta e="T326" id="Seg_6395" s="T325">giv-inactive</ta>
            <ta e="T331" id="Seg_6396" s="T330">accs-inf</ta>
            <ta e="T343" id="Seg_6397" s="T342">giv-active</ta>
            <ta e="T354" id="Seg_6398" s="T353">giv-active</ta>
            <ta e="T355" id="Seg_6399" s="T354">giv-inactive</ta>
            <ta e="T365" id="Seg_6400" s="T364">giv-inactive</ta>
            <ta e="T366" id="Seg_6401" s="T365">0.giv-active</ta>
            <ta e="T368" id="Seg_6402" s="T367">0.giv-active</ta>
            <ta e="T369" id="Seg_6403" s="T368">0.giv-active</ta>
            <ta e="T373" id="Seg_6404" s="T372">0.giv-active</ta>
            <ta e="T375" id="Seg_6405" s="T374">giv-inactive</ta>
            <ta e="T377" id="Seg_6406" s="T376">giv-active</ta>
            <ta e="T381" id="Seg_6407" s="T380">0.giv-active-Q</ta>
            <ta e="T382" id="Seg_6408" s="T381">0.giv-active 0.quot-th</ta>
            <ta e="T385" id="Seg_6409" s="T384">0.giv-active</ta>
            <ta e="T387" id="Seg_6410" s="T386">accs-inf</ta>
            <ta e="T390" id="Seg_6411" s="T389">0.giv-active</ta>
            <ta e="T397" id="Seg_6412" s="T396">giv-inactive</ta>
            <ta e="T399" id="Seg_6413" s="T398">0.giv-active</ta>
            <ta e="T400" id="Seg_6414" s="T399">0.giv-active</ta>
            <ta e="T402" id="Seg_6415" s="T401">0.giv-active</ta>
            <ta e="T406" id="Seg_6416" s="T405">giv-inactive</ta>
            <ta e="T412" id="Seg_6417" s="T411">new</ta>
            <ta e="T413" id="Seg_6418" s="T412">0.giv-active</ta>
            <ta e="T416" id="Seg_6419" s="T415">0.giv-active</ta>
            <ta e="T418" id="Seg_6420" s="T417">giv-inactive</ta>
            <ta e="T423" id="Seg_6421" s="T422">giv-active</ta>
            <ta e="T424" id="Seg_6422" s="T423">new</ta>
            <ta e="T427" id="Seg_6423" s="T426">0.giv-active</ta>
            <ta e="T430" id="Seg_6424" s="T429">giv-inactive</ta>
            <ta e="T431" id="Seg_6425" s="T430">quot-sp</ta>
            <ta e="T435" id="Seg_6426" s="T434">giv-active-Q</ta>
            <ta e="T437" id="Seg_6427" s="T436">giv-inactive-Q</ta>
            <ta e="T439" id="Seg_6428" s="T438">0.giv-active-Q</ta>
            <ta e="T443" id="Seg_6429" s="T442">giv-active-Q</ta>
            <ta e="T446" id="Seg_6430" s="T445">giv-active-Q</ta>
            <ta e="T450" id="Seg_6431" s="T449">0.giv-active-Q</ta>
            <ta e="T451" id="Seg_6432" s="T450">giv-active-Q</ta>
            <ta e="T453" id="Seg_6433" s="T452">giv-inactive-Q</ta>
            <ta e="T455" id="Seg_6434" s="T454">0.giv-active-Q</ta>
            <ta e="T457" id="Seg_6435" s="T456">0.giv-active-Q</ta>
            <ta e="T459" id="Seg_6436" s="T458">new-Q</ta>
            <ta e="T460" id="Seg_6437" s="T459">0.giv-active-Q</ta>
            <ta e="T461" id="Seg_6438" s="T460">0.giv-active 0.quot-sp</ta>
            <ta e="T465" id="Seg_6439" s="T464">giv-active</ta>
            <ta e="T466" id="Seg_6440" s="T465">giv-active</ta>
         </annotation>
         <annotation name="Top" tierref="Top">
            <ta e="T1" id="Seg_6441" s="T0">top.int.concr</ta>
            <ta e="T11" id="Seg_6442" s="T10">top.int.concr</ta>
            <ta e="T32" id="Seg_6443" s="T31">0.top.int.abstr</ta>
            <ta e="T37" id="Seg_6444" s="T35">top.int.concr</ta>
            <ta e="T41" id="Seg_6445" s="T39">top.int.concr</ta>
            <ta e="T58" id="Seg_6446" s="T51">top.int.concr</ta>
            <ta e="T62" id="Seg_6447" s="T61">0.top.int.concr</ta>
            <ta e="T89" id="Seg_6448" s="T88">top.int.concr</ta>
            <ta e="T97" id="Seg_6449" s="T96">0.top.int.concr</ta>
            <ta e="T101" id="Seg_6450" s="T99">top.int.concr</ta>
            <ta e="T107" id="Seg_6451" s="T106">0.top.int.concr</ta>
            <ta e="T109" id="Seg_6452" s="T108">top.int.concr</ta>
            <ta e="T134" id="Seg_6453" s="T133">0.top.int.concr</ta>
            <ta e="T135" id="Seg_6454" s="T134">top.int.concr</ta>
            <ta e="T139" id="Seg_6455" s="T138">top.int.concr</ta>
            <ta e="T154" id="Seg_6456" s="T153">0.top.int.concr</ta>
            <ta e="T161" id="Seg_6457" s="T160">0.top.int.concr</ta>
            <ta e="T166" id="Seg_6458" s="T165">top.int.concr</ta>
            <ta e="T171" id="Seg_6459" s="T170">0.top.int.concr</ta>
            <ta e="T178" id="Seg_6460" s="T177">top.int.concr</ta>
            <ta e="T182" id="Seg_6461" s="T181">0.top.int.concr</ta>
            <ta e="T186" id="Seg_6462" s="T185">top.int.concr</ta>
            <ta e="T190" id="Seg_6463" s="T188">top.int.concr</ta>
            <ta e="T197" id="Seg_6464" s="T196">0.top.int.concr</ta>
            <ta e="T202" id="Seg_6465" s="T201">0.top.int.concr</ta>
            <ta e="T213" id="Seg_6466" s="T212">top.int.concr</ta>
            <ta e="T234" id="Seg_6467" s="T233">top.int.concr</ta>
            <ta e="T238" id="Seg_6468" s="T237">top.int.concr</ta>
            <ta e="T240" id="Seg_6469" s="T239">0.top.int.concr</ta>
            <ta e="T254" id="Seg_6470" s="T253">top.int.concr</ta>
            <ta e="T261" id="Seg_6471" s="T260">0.top.int.concr</ta>
            <ta e="T281" id="Seg_6472" s="T280">top.int.concr</ta>
            <ta e="T293" id="Seg_6473" s="T291">top.int.concr</ta>
            <ta e="T297" id="Seg_6474" s="T296">top.int.concr</ta>
            <ta e="T301" id="Seg_6475" s="T300">0.top.int.concr</ta>
            <ta e="T306" id="Seg_6476" s="T305">0.top.int.concr</ta>
            <ta e="T319" id="Seg_6477" s="T318">top.int.concr</ta>
            <ta e="T325" id="Seg_6478" s="T324">top.int.concr</ta>
            <ta e="T331" id="Seg_6479" s="T329">top.int.concr</ta>
            <ta e="T343" id="Seg_6480" s="T342">top.int.concr</ta>
            <ta e="T354" id="Seg_6481" s="T353">top.int.concr</ta>
            <ta e="T368" id="Seg_6482" s="T367">0.top.int.concr</ta>
            <ta e="T369" id="Seg_6483" s="T368">0.top.int.concr</ta>
            <ta e="T373" id="Seg_6484" s="T372">0.top.int.concr</ta>
            <ta e="T377" id="Seg_6485" s="T376">top.int.concr</ta>
            <ta e="T390" id="Seg_6486" s="T389">0.top.int.concr</ta>
            <ta e="T400" id="Seg_6487" s="T399">0.top.int.concr</ta>
            <ta e="T406" id="Seg_6488" s="T405">top.int.concr</ta>
            <ta e="T413" id="Seg_6489" s="T412">0.top.int.concr</ta>
            <ta e="T416" id="Seg_6490" s="T415">0.top.int.concr</ta>
            <ta e="T427" id="Seg_6491" s="T426">0.top.int.concr</ta>
            <ta e="T435" id="Seg_6492" s="T434">top.int.concr</ta>
            <ta e="T437" id="Seg_6493" s="T436">top.int.concr</ta>
            <ta e="T443" id="Seg_6494" s="T440">top.int.concr</ta>
            <ta e="T446" id="Seg_6495" s="T443">top.int.concr</ta>
            <ta e="T450" id="Seg_6496" s="T449">0.top.int.concr</ta>
            <ta e="T467" id="Seg_6497" s="T464">top.int.concr</ta>
         </annotation>
         <annotation name="Foc" tierref="Foc">
            <ta e="T7" id="Seg_6498" s="T1">foc.int</ta>
            <ta e="T15" id="Seg_6499" s="T11">foc.int</ta>
            <ta e="T18" id="Seg_6500" s="T16">foc.nar</ta>
            <ta e="T19" id="Seg_6501" s="T18">foc.nar</ta>
            <ta e="T35" id="Seg_6502" s="T21">foc.wid</ta>
            <ta e="T38" id="Seg_6503" s="T37">foc.nar</ta>
            <ta e="T51" id="Seg_6504" s="T47">foc.nar</ta>
            <ta e="T60" id="Seg_6505" s="T58">foc.int</ta>
            <ta e="T65" id="Seg_6506" s="T60">foc.int</ta>
            <ta e="T70" id="Seg_6507" s="T65">foc.wid</ta>
            <ta e="T71" id="Seg_6508" s="T70">foc.nar</ta>
            <ta e="T77" id="Seg_6509" s="T75">foc.int</ta>
            <ta e="T79" id="Seg_6510" s="T77">foc.int</ta>
            <ta e="T82" id="Seg_6511" s="T79">foc.int</ta>
            <ta e="T85" id="Seg_6512" s="T82">foc.int</ta>
            <ta e="T88" id="Seg_6513" s="T85">foc.int</ta>
            <ta e="T95" id="Seg_6514" s="T92">foc.int</ta>
            <ta e="T98" id="Seg_6515" s="T97">foc.nar</ta>
            <ta e="T103" id="Seg_6516" s="T102">foc.int</ta>
            <ta e="T107" id="Seg_6517" s="T104">foc.int</ta>
            <ta e="T129" id="Seg_6518" s="T116">foc.int</ta>
            <ta e="T134" id="Seg_6519" s="T129">foc.int</ta>
            <ta e="T138" id="Seg_6520" s="T135">foc.int</ta>
            <ta e="T145" id="Seg_6521" s="T139">foc.int</ta>
            <ta e="T155" id="Seg_6522" s="T152">foc.int</ta>
            <ta e="T160" id="Seg_6523" s="T159">foc.nar</ta>
            <ta e="T163" id="Seg_6524" s="T162">foc.nar</ta>
            <ta e="T168" id="Seg_6525" s="T166">foc.int</ta>
            <ta e="T171" id="Seg_6526" s="T168">foc.int</ta>
            <ta e="T176" id="Seg_6527" s="T174">foc.int</ta>
            <ta e="T181" id="Seg_6528" s="T180">foc.nar</ta>
            <ta e="T185" id="Seg_6529" s="T183">foc.int</ta>
            <ta e="T187" id="Seg_6530" s="T186">foc.ver</ta>
            <ta e="T194" id="Seg_6531" s="T190">foc.int</ta>
            <ta e="T196" id="Seg_6532" s="T194">foc.nar</ta>
            <ta e="T203" id="Seg_6533" s="T200">foc.int</ta>
            <ta e="T205" id="Seg_6534" s="T204">foc.ver</ta>
            <ta e="T208" id="Seg_6535" s="T207">foc.ver</ta>
            <ta e="T216" id="Seg_6536" s="T214">foc.nar</ta>
            <ta e="T222" id="Seg_6537" s="T218">foc.wid</ta>
            <ta e="T224" id="Seg_6538" s="T223">foc.nar</ta>
            <ta e="T227" id="Seg_6539" s="T226">foc.nar</ta>
            <ta e="T234" id="Seg_6540" s="T228">foc.int</ta>
            <ta e="T239" id="Seg_6541" s="T238">foc.int</ta>
            <ta e="T240" id="Seg_6542" s="T239">foc.int</ta>
            <ta e="T242" id="Seg_6543" s="T240">foc.nar</ta>
            <ta e="T243" id="Seg_6544" s="T242">foc.nar</ta>
            <ta e="T248" id="Seg_6545" s="T247">foc.nar</ta>
            <ta e="T259" id="Seg_6546" s="T256">foc.int</ta>
            <ta e="T261" id="Seg_6547" s="T260">foc.ver</ta>
            <ta e="T280" id="Seg_6548" s="T279">foc.ver</ta>
            <ta e="T286" id="Seg_6549" s="T280">foc.wid</ta>
            <ta e="T295" id="Seg_6550" s="T294">foc.nar</ta>
            <ta e="T299" id="Seg_6551" s="T297">foc.int</ta>
            <ta e="T301" id="Seg_6552" s="T299">foc.int</ta>
            <ta e="T306" id="Seg_6553" s="T301">foc.int</ta>
            <ta e="T313" id="Seg_6554" s="T312">foc.nar</ta>
            <ta e="T316" id="Seg_6555" s="T315">foc.nar</ta>
            <ta e="T322" id="Seg_6556" s="T320">foc.int</ta>
            <ta e="T327" id="Seg_6557" s="T325">foc.nar</ta>
            <ta e="T337" id="Seg_6558" s="T333">foc.int</ta>
            <ta e="T347" id="Seg_6559" s="T343">foc.nar</ta>
            <ta e="T350" id="Seg_6560" s="T348">foc.nar</ta>
            <ta e="T356" id="Seg_6561" s="T354">foc.nar</ta>
            <ta e="T368" id="Seg_6562" s="T365">foc.int</ta>
            <ta e="T369" id="Seg_6563" s="T368">foc.int</ta>
            <ta e="T376" id="Seg_6564" s="T369">foc.int</ta>
            <ta e="T379" id="Seg_6565" s="T377">foc.int</ta>
            <ta e="T380" id="Seg_6566" s="T379">foc.nar</ta>
            <ta e="T390" id="Seg_6567" s="T383">foc.int</ta>
            <ta e="T400" id="Seg_6568" s="T390">foc.int</ta>
            <ta e="T408" id="Seg_6569" s="T407">foc.nar</ta>
            <ta e="T413" id="Seg_6570" s="T409">foc.int</ta>
            <ta e="T418" id="Seg_6571" s="T414">foc.int</ta>
            <ta e="T427" id="Seg_6572" s="T419">foc.int</ta>
            <ta e="T431" id="Seg_6573" s="T428">foc.wid</ta>
            <ta e="T436" id="Seg_6574" s="T435">foc.int</ta>
            <ta e="T440" id="Seg_6575" s="T438">foc.int</ta>
            <ta e="T448" id="Seg_6576" s="T446">foc.int</ta>
            <ta e="T453" id="Seg_6577" s="T452">foc.nar</ta>
            <ta e="T455" id="Seg_6578" s="T453">foc.int</ta>
            <ta e="T457" id="Seg_6579" s="T455">foc.int</ta>
            <ta e="T460" id="Seg_6580" s="T457">foc.int</ta>
            <ta e="T470" id="Seg_6581" s="T467">foc.int</ta>
         </annotation>
         <annotation name="BOR" tierref="BOR">
            <ta e="T8" id="Seg_6582" s="T7">RUS:cult</ta>
            <ta e="T16" id="Seg_6583" s="T15">EV:gram (INTNS)</ta>
            <ta e="T17" id="Seg_6584" s="T16">RUS:cult</ta>
            <ta e="T19" id="Seg_6585" s="T18">RUS:cult</ta>
            <ta e="T33" id="Seg_6586" s="T32">RUS:core</ta>
            <ta e="T45" id="Seg_6587" s="T44">EV:gram (INTNS)</ta>
            <ta e="T48" id="Seg_6588" s="T47">RUS:core</ta>
            <ta e="T71" id="Seg_6589" s="T70">RUS:cult</ta>
            <ta e="T96" id="Seg_6590" s="T95">RUS:gram</ta>
            <ta e="T106" id="Seg_6591" s="T105">EV:gram (INTNS)</ta>
            <ta e="T108" id="Seg_6592" s="T107">RUS:gram</ta>
            <ta e="T111" id="Seg_6593" s="T110">EV:gram (INTNS)</ta>
            <ta e="T126" id="Seg_6594" s="T125">EV:cult</ta>
            <ta e="T133" id="Seg_6595" s="T132">EV:core</ta>
            <ta e="T134" id="Seg_6596" s="T133">EV:cult</ta>
            <ta e="T146" id="Seg_6597" s="T145">RUS:disc</ta>
            <ta e="T147" id="Seg_6598" s="T146">RUS:disc</ta>
            <ta e="T163" id="Seg_6599" s="T162">RUS:cult</ta>
            <ta e="T167" id="Seg_6600" s="T166">RUS:core</ta>
            <ta e="T199" id="Seg_6601" s="T198">RUS:cult</ta>
            <ta e="T316" id="Seg_6602" s="T315">RUS:core</ta>
            <ta e="T341" id="Seg_6603" s="T340">EV:gram (INTNS)</ta>
            <ta e="T367" id="Seg_6604" s="T366">RUS:gram</ta>
            <ta e="T384" id="Seg_6605" s="T383">RUS:cult</ta>
            <ta e="T389" id="Seg_6606" s="T388">EV:gram (INTNS)</ta>
            <ta e="T395" id="Seg_6607" s="T394">EV:gram (INTNS)</ta>
            <ta e="T401" id="Seg_6608" s="T400">RUS:cult</ta>
            <ta e="T464" id="Seg_6609" s="T463">EV:gram (INTNS)</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T7" id="Seg_6610" s="T0">I heard this story, when with my parents…</ta>
            <ta e="T10" id="Seg_6611" s="T7">In the year when I started school.</ta>
            <ta e="T15" id="Seg_6612" s="T10">It is in my mind until now.</ta>
            <ta e="T21" id="Seg_6613" s="T15">So well, my mama's sister, Claudia, has told it.</ta>
            <ta e="T35" id="Seg_6614" s="T21">In the tundra far away, in his homeland, where he himself was born, a very beautiful boy was travelling [nomadizing] with his family.</ta>
            <ta e="T39" id="Seg_6615" s="T35">His name was "Hunter".</ta>
            <ta e="T51" id="Seg_6616" s="T39">At another place, far away from them, a very beautiful girl was nomadizing with her family.</ta>
            <ta e="T65" id="Seg_6617" s="T51">This young boy and this girl met, they loved each other in their homeland.</ta>
            <ta e="T70" id="Seg_6618" s="T65">Once the girl said to Hunter, to the boy: </ta>
            <ta e="T75" id="Seg_6619" s="T70">"How long will we hide from our parents?</ta>
            <ta e="T79" id="Seg_6620" s="T75">Come to us, like for a visit.</ta>
            <ta e="T88" id="Seg_6621" s="T79">Speak with my parents, speak with my father, ask my father for me.</ta>
            <ta e="T103" id="Seg_6622" s="T88">My father is a very good man, even if he is sly, don't be afraid of his slyness."</ta>
            <ta e="T134" id="Seg_6623" s="T103">Well, so she said, and after some day, on a very warm day, on a day in spring, around autumn, he came with a riding reindeer, he had a tamed reindeer.</ta>
            <ta e="T138" id="Seg_6624" s="T134">He went with it to visit the girl.</ta>
            <ta e="T145" id="Seg_6625" s="T138">Having come there, he bowed before her parents: </ta>
            <ta e="T151" id="Seg_6626" s="T145">"Hello, hello, old people!", Hunter said, the boy.</ta>
            <ta e="T161" id="Seg_6627" s="T151">"Well, I have come, mother, father, I ask for your daughter.</ta>
            <ta e="T171" id="Seg_6628" s="T161">How long will we hide, we love each other for a long time, we esteem each other a lot."</ta>
            <ta e="T178" id="Seg_6629" s="T171">"Well, what shall I say!?", the father said with shimmering eyes.</ta>
            <ta e="T186" id="Seg_6630" s="T178">"Now he wants to trick me", the boy thought.</ta>
            <ta e="T188" id="Seg_6631" s="T186">And indeed.</ta>
            <ta e="T194" id="Seg_6632" s="T188">The girl's father threw something away.</ta>
            <ta e="T203" id="Seg_6633" s="T194">It made "chik" and he threw it across the stove and thought:</ta>
            <ta e="T211" id="Seg_6634" s="T203">"Will he catch this bone or not."</ta>
            <ta e="T218" id="Seg_6635" s="T211">And the boy catches it quickly.</ta>
            <ta e="T222" id="Seg_6636" s="T218">There the girl's father asks: </ta>
            <ta e="T226" id="Seg_6637" s="T222">"Well, what did you catch there?"</ta>
            <ta e="T234" id="Seg_6638" s="T226">"How what?", the boy asked there.</ta>
            <ta e="T247" id="Seg_6639" s="T234">"I saw it and recognized it – it is a knee bone, who wouldn't recognize this knee bone?</ta>
            <ta e="T253" id="Seg_6640" s="T247">Who wouldn't recognize this knee bone?", he said.</ta>
            <ta e="T259" id="Seg_6641" s="T253">The father smiled slily and said:</ta>
            <ta e="T264" id="Seg_6642" s="T259">"Well, you did recognize the knee bone, well.</ta>
            <ta e="T280" id="Seg_6643" s="T264">Now, when you look at this knee bone, do you find any sinew?</ta>
            <ta e="T286" id="Seg_6644" s="T280">Then I won't take back my word.</ta>
            <ta e="T295" id="Seg_6645" s="T286">If you find any sinew, then I will give you my daughter."</ta>
            <ta e="T301" id="Seg_6646" s="T295">Well, the boy tried to see it, he tried to see.</ta>
            <ta e="T306" id="Seg_6647" s="T301">Then he said to himself very silently:</ta>
            <ta e="T318" id="Seg_6648" s="T306">"Oh, well, really, really like a stone, what a beautiful bone, guys."</ta>
            <ta e="T322" id="Seg_6649" s="T318">Well, he doesn't know it, apparently.</ta>
            <ta e="T337" id="Seg_6650" s="T322">Only his father knows it: On one side there aren't sinews at all.</ta>
            <ta e="T353" id="Seg_6651" s="T337">Looking on one side, if one looks thoroughly, then it is like a girl's face, like a woman's face, on one of the two sides.</ta>
            <ta e="T358" id="Seg_6652" s="T353">But that only the father knows.</ta>
            <ta e="T365" id="Seg_6653" s="T358">Well, the boy Hunter was at his wit's end.</ta>
            <ta e="T369" id="Seg_6654" s="T365">He wants to get angry, he is afraid.</ta>
            <ta e="T376" id="Seg_6655" s="T369">And there he looks to the girl.</ta>
            <ta e="T379" id="Seg_6656" s="T376">The girl was also sly.</ta>
            <ta e="T390" id="Seg_6657" s="T379">"How do I do?", she thought, and as if she was chasing away mosquitos she carefully took his hand.</ta>
            <ta e="T403" id="Seg_6658" s="T390">Then she very carefully showed the face, as if she would strike mosquitos.</ta>
            <ta e="T409" id="Seg_6659" s="T403">Oh, the boy is happy, though.</ta>
            <ta e="T418" id="Seg_6660" s="T409">He took very quickly his knife, then he lifted the knee bone very carefully.</ta>
            <ta e="T428" id="Seg_6661" s="T418">Oh, and he very quickly pulls out a sinew with his knife.</ta>
            <ta e="T431" id="Seg_6662" s="T428">There the father said: </ta>
            <ta e="T448" id="Seg_6663" s="T431">"Well, what shall I do, I did say it, I won't take back my word, such a capable man, such a hunter knows everything.</ta>
            <ta e="T453" id="Seg_6664" s="T448">Well, I'll give to you, Hunter, my daughter.</ta>
            <ta e="T461" id="Seg_6665" s="T453">Live happily, live well, have many children", he said.</ta>
            <ta e="T470" id="Seg_6666" s="T461">And so Hunter and his girl lived very well.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T7" id="Seg_6667" s="T0">Ich hörte diese Erzählung, als mit meinen Eltern… </ta>
            <ta e="T10" id="Seg_6668" s="T7">In dem Jahr, als ich in die Schule kam.</ta>
            <ta e="T15" id="Seg_6669" s="T10">Es ist mir bis jetzt im Gedächtnis.</ta>
            <ta e="T21" id="Seg_6670" s="T15">Nun gut, die Schwester meiner Mama, Claudia, hat es erzählt.</ta>
            <ta e="T35" id="Seg_6671" s="T21">In der fernen, fernen Tundra, in seiner Heimat, wo er selber geboren wurde, reiste [nomadisierte] gemeinsam mit seiner Familie ein sehr schöner Junge.</ta>
            <ta e="T39" id="Seg_6672" s="T35">Sein Name war "Jäger".</ta>
            <ta e="T51" id="Seg_6673" s="T39">An einem anderen Ort, weit weg von ihnen, nomadisierte ein schönes Mädchen mit seiner Familie.</ta>
            <ta e="T65" id="Seg_6674" s="T51">Dieser junge Junge und dieses Mädchen trafen sich, sie liebten einander in ihrer Heimat.</ta>
            <ta e="T70" id="Seg_6675" s="T65">Einmal sagte das Mädchen zu Jäger, dem Jungen: </ta>
            <ta e="T75" id="Seg_6676" s="T70">"Wie lange verstecken wir uns vor unseren Eltern?</ta>
            <ta e="T79" id="Seg_6677" s="T75">Komm zu uns, wie zu Besuch.</ta>
            <ta e="T88" id="Seg_6678" s="T79">Sprich mit meinen Eltern, sprich mit meinem Vater, bitte bei meinem Vater um mich.</ta>
            <ta e="T103" id="Seg_6679" s="T88">Mein Vater ist ein sehr guter Mensch, wenn er auch schlau ist, hab keine Angst vor seiner Schläue." </ta>
            <ta e="T134" id="Seg_6680" s="T103">Nun, so sprach sie, und so nach einigen Tagen, an einem sehr warmen Tag, an einem Tag im Frühling, gegen Herbst, kam er mit einem Reitrentier, er hatte ein gezähmtes Rentier.</ta>
            <ta e="T138" id="Seg_6681" s="T134">Damit besuchte er das Mädchen.</ta>
            <ta e="T145" id="Seg_6682" s="T138">Nachdem er angekommen waren, verbeugte er sich vor ihren Eltern: </ta>
            <ta e="T151" id="Seg_6683" s="T145">"Seid gegrüßt, seid gegrüßt, Alte!", sagte Jäger, der Junge.</ta>
            <ta e="T161" id="Seg_6684" s="T151">"Nun, ich bin gekommen, Mutter, Vater, ich bitte um eure Tochter.</ta>
            <ta e="T171" id="Seg_6685" s="T161">Wie lange verstecken wir uns noch, wir lieben uns seit langem, wir achten einander sehr."</ta>
            <ta e="T178" id="Seg_6686" s="T171">"Nun, was soll ich sagen!?", sagte der Vater mit schimmernden Augen.</ta>
            <ta e="T186" id="Seg_6687" s="T178">"Jetzt möchte er mich überlisten", dachte der Junge.</ta>
            <ta e="T188" id="Seg_6688" s="T186">Und tatsächlich.</ta>
            <ta e="T194" id="Seg_6689" s="T188">Der Vater des Mädchens warf irgendetwas weg. </ta>
            <ta e="T203" id="Seg_6690" s="T194">Es machte "tschik" und er warf es über den Herd und dachte dann: </ta>
            <ta e="T211" id="Seg_6691" s="T203">"Fängt er diesen Knochen oder fängt er ihn nicht."</ta>
            <ta e="T218" id="Seg_6692" s="T211">Und der Junge greift ihn schnell.</ta>
            <ta e="T222" id="Seg_6693" s="T218">Da fragt der Vater des Mädchens: </ta>
            <ta e="T226" id="Seg_6694" s="T222">"Nun, was hast du da gefangen?"</ta>
            <ta e="T234" id="Seg_6695" s="T226">"Wie was?", fragte da der Junge. </ta>
            <ta e="T247" id="Seg_6696" s="T234">"Ich habe es gesehen und erkannt – es ist ein Knieknochen, wer erkennt denn diesen Knieknochen nicht?</ta>
            <ta e="T253" id="Seg_6697" s="T247">Wer wird diesen Knieknochen nicht erkennen?", sagte er.</ta>
            <ta e="T259" id="Seg_6698" s="T253">Der Vater lächelte schlau und sagte: </ta>
            <ta e="T264" id="Seg_6699" s="T259">"Nun, du hast den Knieknochen erkannt, gut.</ta>
            <ta e="T280" id="Seg_6700" s="T264">Nun, wenn du auf diesen Knieknochen schaust, findest du irgendeine Sehne?</ta>
            <ta e="T286" id="Seg_6701" s="T280">Dann nehme ich mein Wort nicht zurück.</ta>
            <ta e="T295" id="Seg_6702" s="T286">Wenn du irgendeine Sehne findest, dann gebe ich dir meine Tochter." </ta>
            <ta e="T301" id="Seg_6703" s="T295">Nun, der Junge versuchte es zu sehen, er versuchte es zu sehen.</ta>
            <ta e="T306" id="Seg_6704" s="T301">Danach sagte er ganz leise zu sich selber: </ta>
            <ta e="T318" id="Seg_6705" s="T306">"Oh, nun, wirklich, wirklich wie ein Stein, was für ein schöner Knochen, Kinder." </ta>
            <ta e="T322" id="Seg_6706" s="T318">Nun, er weiß es offenbar nicht.</ta>
            <ta e="T337" id="Seg_6707" s="T322">Nur sein Vater weiß es: Auf einer Seite gibt es überhaupt keine Sehnen.</ta>
            <ta e="T353" id="Seg_6708" s="T337">Man schaut auf eine Seite, wenn man genau schaut, dann ist sie wie das Gesicht eines Mädchens, wie das Gesicht einer Frau, auf einer der beiden Seiten.</ta>
            <ta e="T358" id="Seg_6709" s="T353">Aber das weiß nur der Vater.</ta>
            <ta e="T365" id="Seg_6710" s="T358">Nun, der Junge Jäger weiß nicht weiter.</ta>
            <ta e="T369" id="Seg_6711" s="T365">Er möchte böse werden, er hat Angst.</ta>
            <ta e="T376" id="Seg_6712" s="T369">Und dann blickt er zum Mädchen.</ta>
            <ta e="T379" id="Seg_6713" s="T376">Das Mädchen war auch schlau.</ta>
            <ta e="T390" id="Seg_6714" s="T379">"Wie mache ich es?", dachte sie, und als ob sie Mücken verjagen würde, nahm sie vorsichtig seine Hand.</ta>
            <ta e="T403" id="Seg_6715" s="T390">Dann zeigt sie ganz vorsichtig das Gesicht, als ob sie Mücken erschlagen würde.</ta>
            <ta e="T409" id="Seg_6716" s="T403">Oh, da freut sich der Junge.</ta>
            <ta e="T418" id="Seg_6717" s="T409">Er nahm sehr schnell sein Messer, dann hob er ganz vorsichtig den Knieknochen an.</ta>
            <ta e="T428" id="Seg_6718" s="T418">Oh, und er zieht sehr schnell mit dem Messer eine Sehne heraus.</ta>
            <ta e="T431" id="Seg_6719" s="T428">Da sagte der Vater: </ta>
            <ta e="T448" id="Seg_6720" s="T431">"Nun, was will man machen, ich habe es gesagt, ich nehme mein Wort nicht zurück, so ein fähiger Mensch, so ein Jäger weiß alles.</ta>
            <ta e="T453" id="Seg_6721" s="T448">Nun, ich gebe dir, Jäger, meine Tochter.</ta>
            <ta e="T461" id="Seg_6722" s="T453">Lebt glücklich, lebt gut, habt viele Kinder", sagte er.</ta>
            <ta e="T470" id="Seg_6723" s="T461">Und so lebten Jäger und sein Mädchen sehr gut.</ta>
         </annotation>
         <annotation name="fr" tierref="fr" />
         <annotation name="ltr" tierref="ltr">
            <ta e="T7" id="Seg_6724" s="T0">Я этот рассказ слышала, когда вместе с родителями… </ta>
            <ta e="T10" id="Seg_6725" s="T7">Год когда я поступила в школу.</ta>
            <ta e="T15" id="Seg_6726" s="T10">Он до сих в моей памяти есть.</ta>
            <ta e="T21" id="Seg_6727" s="T15">Вот так, моей мамы сестра, Клавдия рассказывала.</ta>
            <ta e="T35" id="Seg_6728" s="T21">В далёкой-далёкой тундре, на родной земле, где он сам родился, вместе с родителями ездил (кочевал) очень красивый парень.</ta>
            <ta e="T39" id="Seg_6729" s="T35">Eго звали Охотник.</ta>
            <ta e="T51" id="Seg_6730" s="T39">На другой земле, далековато от них кочевала красивая девушка вместе со своими родителями.</ta>
            <ta e="T65" id="Seg_6731" s="T51">Этот молодой парень с этой девушкой встречались, любили друг друга на своей родной земле.</ta>
            <ta e="T70" id="Seg_6732" s="T65">Однажды девушка Охотнику, парню говорит: </ta>
            <ta e="T75" id="Seg_6733" s="T70">"Доколе мы от родителей будем скрываться?</ta>
            <ta e="T79" id="Seg_6734" s="T75">Приходи (приезжай) к нам – как будто в гости.</ta>
            <ta e="T88" id="Seg_6735" s="T79">С родителями поговори, с отцом моим поговори, у отца попроси меня.</ta>
            <ta e="T103" id="Seg_6736" s="T88">Отец так-то очень хороший человек, несмотря на это, он с хитростью, но от этой хитрости не опасайся". </ta>
            <ta e="T134" id="Seg_6737" s="T103">Вот, после этих слов парень таким образом через несколько дней, в очень теплый денёк, весной, в очень солнечный день верхом на олене вот приехал, своего (ручного оленя..) белый ручной олень у негo.</ta>
            <ta e="T138" id="Seg_6738" s="T134">На нём в гости поехал к девушке.</ta>
            <ta e="T145" id="Seg_6739" s="T138">Как зашёл, до земли головой её родителям поклонившись: ‎‎</ta>
            <ta e="T151" id="Seg_6740" s="T145">"Здравствуйте, здравствуйте, старики!" – сказал парень Охотник.</ta>
            <ta e="T161" id="Seg_6741" s="T151">"Вот приехал к вам, (сес..) сестра, отец, у вас прошу девушку.</ta>
            <ta e="T171" id="Seg_6742" s="T161">Зачем, доколе скрывать нам, мы давно любим друг друга, очень уважаем".</ta>
            <ta e="T178" id="Seg_6743" s="T171">"Ну, что сделаешь!? – сказал с искрящимися глазами отец девушки.</ta>
            <ta e="T186" id="Seg_6744" s="T178">Затем, вот хитрить хочет, подумал парень.</ta>
            <ta e="T188" id="Seg_6745" s="T186">И правда.</ta>
            <ta e="T194" id="Seg_6746" s="T188">Отец девушки что-то выбросил: </ta>
            <ta e="T203" id="Seg_6747" s="T194">Со звуком "чык" выбросил над очагом, с мыслями: </ta>
            <ta e="T211" id="Seg_6748" s="T203">"Поймает или не поймает эту кость."</ta>
            <ta e="T218" id="Seg_6749" s="T211">Вот, парень быстро схватил.</ta>
            <ta e="T222" id="Seg_6750" s="T218">На это спрашивает отец девушки: </ta>
            <ta e="T226" id="Seg_6751" s="T222">"Ну, что ты поймал?"</ta>
            <ta e="T234" id="Seg_6752" s="T226">"Как что?" – спросил этот парень. </ta>
            <ta e="T247" id="Seg_6753" s="T234">"Я (сразу), я посмотрел, догадался – коленная кость, кто не узнает эту коленную кость?</ta>
            <ta e="T253" id="Seg_6754" s="T247">– сказал.</ta>
            <ta e="T259" id="Seg_6755" s="T253">Отец хитр… с хитростью и прищулившись сказал: </ta>
            <ta e="T264" id="Seg_6756" s="T259">"Вот, узнал коленную кость, хорошо.</ta>
            <ta e="T280" id="Seg_6757" s="T264">Вот, на (кол) коленной косточке где-нибудь (увид) увидишь ли хоть одну жилку?</ta>
            <ta e="T286" id="Seg_6758" s="T280">Тогда я своё слово не возьму обратно.</ta>
            <ta e="T295" id="Seg_6759" s="T286">Если найдёшь хоть одну жилку, тогда я свою дочь за тебя отдам". </ta>
            <ta e="T301" id="Seg_6760" s="T295">Вот, парень присматривался, присматривался.</ta>
            <ta e="T306" id="Seg_6761" s="T301">Потом тихонько сам себе говорит: </ta>
            <ta e="T318" id="Seg_6762" s="T306">"О, вот, вот это да – точно как камень, что за красивая кость, друзья". </ta>
            <ta e="T322" id="Seg_6763" s="T318">Вот он, оказывается, не знает.</ta>
            <ta e="T337" id="Seg_6764" s="T322">Ее (ее) это отец только он знает: Одно место (одна сторона) вообще ни одной жилы не имеет. </ta>
            <ta e="T353" id="Seg_6765" s="T337">На одную сторону посмторишь, если хорошенько присмотреться, то, словно на девичье (лиц) лицо похоже, как женское лицо – на одной стороне.</ta>
            <ta e="T358" id="Seg_6766" s="T353">Об этом знает только отец.</ta>
            <ta e="T365" id="Seg_6767" s="T358">Вот, не знает как быть парень Охотник.</ta>
            <ta e="T369" id="Seg_6768" s="T365">Сердиться хочет, опасается.</ta>
            <ta e="T376" id="Seg_6769" s="T369">А потом как глянет в сторону девушки.</ta>
            <ta e="T379" id="Seg_6770" s="T376">Девушка тоже хитрости набралась.</ta>
            <ta e="T390" id="Seg_6771" s="T379">Как буду делать? – сказав, словно от комаров отмахиваясь, свою руку осторожненько вот так взяла.</ta>
            <ta e="T403" id="Seg_6772" s="T390">A потом вот осторожненько (во) вот так (лицом) лицо (это самое) показывает – словно от комаров отмахивается.</ta>
            <ta e="T409" id="Seg_6773" s="T403">О-о, вот, парень как обрадуется.</ta>
            <ta e="T418" id="Seg_6774" s="T409">Быстро взял свой нож, потом осторожненько приподнял коленную кость.</ta>
            <ta e="T428" id="Seg_6775" s="T418">Ой, (быстро) очень быстро ножом жилы как вытянул.</ta>
            <ta e="T431" id="Seg_6776" s="T428">На это отец сказал: </ta>
            <ta e="T448" id="Seg_6777" s="T431">"Ну, что делать, я говорил, своё слово обратно не возьму, такой умелый человек, такой охотник всё знает.</ta>
            <ta e="T453" id="Seg_6778" s="T448">Вот отдаю тебе, Охотник, дочь свою.</ta>
            <ta e="T461" id="Seg_6779" s="T453">Счастливо живите, хорошо живите, много детей рожайте", – сказал.</ta>
            <ta e="T470" id="Seg_6780" s="T461">Вот, таким образом Охотник со своей девушкой очень хорошо зажили.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T428" id="Seg_6781" s="T418">[DCh]: Apparently the negative form "ɨlbat" is unexpected here, instead one would expect "ɨlbɨt".</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg"
                          name="gg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top"
                          name="Top"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc"
                          name="Foc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
