<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDB0991B74-54E7-E5EE-B926-9833D34C1E6A">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>GoI_1930_ShamansTurnedIntoSwans_flk</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\flk\GoI_1930_ShamansTurnedIntoSwans_flk\GoI_1930_ShamansTurnedIntoSwans_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">186</ud-information>
            <ud-information attribute-name="# HIAT:w">134</ud-information>
            <ud-information attribute-name="# e">134</ud-information>
            <ud-information attribute-name="# HIAT:u">19</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="GoI">
            <abbreviation>GoI</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="4.0" type="appl" />
         <tli id="T1" time="4.5" type="appl" />
         <tli id="T2" time="5.0" type="appl" />
         <tli id="T3" time="5.5" type="appl" />
         <tli id="T4" time="6.0" type="appl" />
         <tli id="T5" time="6.5" type="appl" />
         <tli id="T6" time="7.0" type="appl" />
         <tli id="T7" time="7.5" type="appl" />
         <tli id="T8" time="8.0" type="appl" />
         <tli id="T9" time="8.5" type="appl" />
         <tli id="T10" time="9.0" type="appl" />
         <tli id="T11" time="9.5" type="appl" />
         <tli id="T12" time="10.0" type="appl" />
         <tli id="T13" time="10.5" type="appl" />
         <tli id="T14" time="11.0" type="appl" />
         <tli id="T15" time="11.5" type="appl" />
         <tli id="T16" time="12.0" type="appl" />
         <tli id="T17" time="12.5" type="appl" />
         <tli id="T18" time="13.0" type="appl" />
         <tli id="T19" time="13.5" type="appl" />
         <tli id="T20" time="14.0" type="appl" />
         <tli id="T21" time="14.5" type="appl" />
         <tli id="T22" time="15.0" type="appl" />
         <tli id="T23" time="15.5" type="appl" />
         <tli id="T24" time="16.0" type="appl" />
         <tli id="T25" time="16.5" type="appl" />
         <tli id="T26" time="17.0" type="appl" />
         <tli id="T27" time="17.5" type="appl" />
         <tli id="T28" time="18.0" type="appl" />
         <tli id="T29" time="18.5" type="appl" />
         <tli id="T30" time="19.0" type="appl" />
         <tli id="T31" time="19.5" type="appl" />
         <tli id="T32" time="20.0" type="appl" />
         <tli id="T33" time="20.5" type="appl" />
         <tli id="T34" time="21.0" type="appl" />
         <tli id="T35" time="21.5" type="appl" />
         <tli id="T36" time="22.0" type="appl" />
         <tli id="T37" time="22.5" type="appl" />
         <tli id="T38" time="23.0" type="appl" />
         <tli id="T39" time="23.5" type="appl" />
         <tli id="T40" time="24.0" type="appl" />
         <tli id="T41" time="24.5" type="appl" />
         <tli id="T42" time="25.0" type="appl" />
         <tli id="T43" time="25.5" type="appl" />
         <tli id="T44" time="26.0" type="appl" />
         <tli id="T45" time="26.5" type="appl" />
         <tli id="T46" time="27.0" type="appl" />
         <tli id="T47" time="27.5" type="appl" />
         <tli id="T48" time="28.0" type="appl" />
         <tli id="T49" time="28.5" type="appl" />
         <tli id="T50" time="29.0" type="appl" />
         <tli id="T51" time="29.5" type="appl" />
         <tli id="T52" time="30.0" type="appl" />
         <tli id="T53" time="30.5" type="appl" />
         <tli id="T54" time="31.0" type="appl" />
         <tli id="T55" time="31.5" type="appl" />
         <tli id="T56" time="32.0" type="appl" />
         <tli id="T57" time="32.5" type="appl" />
         <tli id="T58" time="33.0" type="appl" />
         <tli id="T59" time="33.5" type="appl" />
         <tli id="T60" time="34.0" type="appl" />
         <tli id="T61" time="34.5" type="appl" />
         <tli id="T62" time="35.0" type="appl" />
         <tli id="T63" time="35.5" type="appl" />
         <tli id="T64" time="36.0" type="appl" />
         <tli id="T65" time="36.5" type="appl" />
         <tli id="T66" time="37.0" type="appl" />
         <tli id="T67" time="37.5" type="appl" />
         <tli id="T68" time="38.0" type="appl" />
         <tli id="T69" time="38.5" type="appl" />
         <tli id="T70" time="39.0" type="appl" />
         <tli id="T71" time="39.5" type="appl" />
         <tli id="T72" time="40.0" type="appl" />
         <tli id="T73" time="40.5" type="appl" />
         <tli id="T74" time="41.0" type="appl" />
         <tli id="T75" time="41.5" type="appl" />
         <tli id="T76" time="42.0" type="appl" />
         <tli id="T77" time="42.5" type="appl" />
         <tli id="T78" time="43.0" type="appl" />
         <tli id="T79" time="43.5" type="appl" />
         <tli id="T80" time="44.0" type="appl" />
         <tli id="T81" time="44.5" type="appl" />
         <tli id="T82" time="45.0" type="appl" />
         <tli id="T83" time="45.5" type="appl" />
         <tli id="T84" time="46.0" type="appl" />
         <tli id="T85" time="46.5" type="appl" />
         <tli id="T86" time="47.0" type="appl" />
         <tli id="T87" time="47.5" type="appl" />
         <tli id="T88" time="48.0" type="appl" />
         <tli id="T89" time="48.5" type="appl" />
         <tli id="T90" time="49.0" type="appl" />
         <tli id="T91" time="49.5" type="appl" />
         <tli id="T92" time="50.0" type="appl" />
         <tli id="T93" time="50.5" type="appl" />
         <tli id="T94" time="51.0" type="appl" />
         <tli id="T95" time="51.5" type="appl" />
         <tli id="T96" time="52.0" type="appl" />
         <tli id="T97" time="52.5" type="appl" />
         <tli id="T98" time="53.0" type="appl" />
         <tli id="T99" time="53.5" type="appl" />
         <tli id="T100" time="54.0" type="appl" />
         <tli id="T101" time="54.5" type="appl" />
         <tli id="T102" time="55.0" type="appl" />
         <tli id="T103" time="55.5" type="appl" />
         <tli id="T104" time="56.0" type="appl" />
         <tli id="T105" time="56.5" type="appl" />
         <tli id="T106" time="57.0" type="appl" />
         <tli id="T107" time="57.5" type="appl" />
         <tli id="T108" time="58.0" type="appl" />
         <tli id="T109" time="58.5" type="appl" />
         <tli id="T110" time="59.0" type="appl" />
         <tli id="T111" time="59.5" type="appl" />
         <tli id="T112" time="60.0" type="appl" />
         <tli id="T113" time="60.5" type="appl" />
         <tli id="T114" time="61.0" type="appl" />
         <tli id="T115" time="61.5" type="appl" />
         <tli id="T116" time="62.0" type="appl" />
         <tli id="T117" time="62.5" type="appl" />
         <tli id="T118" time="63.0" type="appl" />
         <tli id="T119" time="63.5" type="appl" />
         <tli id="T120" time="64.0" type="appl" />
         <tli id="T121" time="64.5" type="appl" />
         <tli id="T122" time="65.0" type="appl" />
         <tli id="T123" time="65.5" type="appl" />
         <tli id="T124" time="66.0" type="appl" />
         <tli id="T125" time="66.5" type="appl" />
         <tli id="T126" time="67.0" type="appl" />
         <tli id="T127" time="67.5" type="appl" />
         <tli id="T128" time="68.0" type="appl" />
         <tli id="T129" time="68.5" type="appl" />
         <tli id="T130" time="69.0" type="appl" />
         <tli id="T131" time="69.5" type="appl" />
         <tli id="T132" time="70.0" type="appl" />
         <tli id="T133" time="70.5" type="appl" />
         <tli id="T134" time="71.0" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="GoI"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T134" id="Seg_0" n="sc" s="T0">
               <ts e="T6" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Bɨlɨr</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">ikki</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">ojun</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">kɨːra</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">turannar</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">eppitter</ts>
                  <nts id="Seg_20" n="HIAT:ip">:</nts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T13" id="Seg_23" n="HIAT:u" s="T6">
                  <nts id="Seg_24" n="HIAT:ip">"</nts>
                  <ts e="T7" id="Seg_26" n="HIAT:w" s="T6">Bihigi</ts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_29" n="HIAT:w" s="T7">kuba</ts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_32" n="HIAT:w" s="T8">bu͡olan</ts>
                  <nts id="Seg_33" n="HIAT:ip">,</nts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_36" n="HIAT:w" s="T9">kötör</ts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_39" n="HIAT:w" s="T10">barar</ts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_42" n="HIAT:w" s="T11">hiriger</ts>
                  <nts id="Seg_43" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_45" n="HIAT:w" s="T12">kötü͡ökpüt</ts>
                  <nts id="Seg_46" n="HIAT:ip">.</nts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T39" id="Seg_49" n="HIAT:u" s="T13">
                  <ts e="T14" id="Seg_51" n="HIAT:w" s="T13">Anɨ</ts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_54" n="HIAT:w" s="T14">haːs</ts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_57" n="HIAT:w" s="T15">togus</ts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_60" n="HIAT:w" s="T16">kuba</ts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_63" n="HIAT:w" s="T17">bu͡olan</ts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_66" n="HIAT:w" s="T18">dʼi͡egitin</ts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_69" n="HIAT:w" s="T19">üste</ts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_72" n="HIAT:w" s="T20">ergiji͡ekpit</ts>
                  <nts id="Seg_73" n="HIAT:ip">,</nts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_76" n="HIAT:w" s="T21">bu</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_79" n="HIAT:w" s="T22">biri͡emege</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_82" n="HIAT:w" s="T23">bihigini</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_85" n="HIAT:w" s="T24">tohujaːrɨŋ</ts>
                  <nts id="Seg_86" n="HIAT:ip">,</nts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_89" n="HIAT:w" s="T25">togus</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_92" n="HIAT:w" s="T26">čeːlkeː</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_95" n="HIAT:w" s="T27">tugutu</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_98" n="HIAT:w" s="T28">ölöröŋŋüt</ts>
                  <nts id="Seg_99" n="HIAT:ip">,</nts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_102" n="HIAT:w" s="T29">manɨ</ts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_105" n="HIAT:w" s="T30">etin</ts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_108" n="HIAT:w" s="T31">dʼonton</ts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_111" n="HIAT:w" s="T32">ɨraːk</ts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_114" n="HIAT:w" s="T33">turu͡orbut</ts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_117" n="HIAT:w" s="T34">haŋa</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_120" n="HIAT:w" s="T35">öldüːnneːk</ts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_123" n="HIAT:w" s="T36">uraha</ts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_126" n="HIAT:w" s="T37">ihiger</ts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_129" n="HIAT:w" s="T38">uːraŋŋɨt</ts>
                  <nts id="Seg_130" n="HIAT:ip">.</nts>
                  <nts id="Seg_131" n="HIAT:ip">"</nts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T44" id="Seg_134" n="HIAT:u" s="T39">
                  <ts e="T40" id="Seg_136" n="HIAT:w" s="T39">Kuba</ts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_139" n="HIAT:w" s="T40">bu͡olbuttar</ts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_142" n="HIAT:w" s="T41">daː</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_145" n="HIAT:w" s="T42">köppüttere</ts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_148" n="HIAT:w" s="T43">ajɨːta</ts>
                  <nts id="Seg_149" n="HIAT:ip">.</nts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T50" id="Seg_152" n="HIAT:u" s="T44">
                  <ts e="T45" id="Seg_154" n="HIAT:w" s="T44">Kaːs-kuba</ts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_157" n="HIAT:w" s="T45">haːrɨːr</ts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_160" n="HIAT:w" s="T46">hiriger</ts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_163" n="HIAT:w" s="T47">tijbitter</ts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_166" n="HIAT:w" s="T48">baː</ts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_169" n="HIAT:w" s="T49">ojuttar</ts>
                  <nts id="Seg_170" n="HIAT:ip">.</nts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T54" id="Seg_173" n="HIAT:u" s="T50">
                  <nts id="Seg_174" n="HIAT:ip">"</nts>
                  <ts e="T51" id="Seg_176" n="HIAT:w" s="T50">Dʼe</ts>
                  <nts id="Seg_177" n="HIAT:ip">,</nts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_180" n="HIAT:w" s="T51">haːrɨ͡ak</ts>
                  <nts id="Seg_181" n="HIAT:ip">"</nts>
                  <nts id="Seg_182" n="HIAT:ip">,</nts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_185" n="HIAT:w" s="T52">di͡ebit</ts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_188" n="HIAT:w" s="T53">biːrgehe</ts>
                  <nts id="Seg_189" n="HIAT:ip">.</nts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T57" id="Seg_192" n="HIAT:u" s="T54">
                  <nts id="Seg_193" n="HIAT:ip">"</nts>
                  <ts e="T55" id="Seg_195" n="HIAT:w" s="T54">Ebege</ts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_198" n="HIAT:w" s="T55">haːrɨ͡ak</ts>
                  <nts id="Seg_199" n="HIAT:ip">"</nts>
                  <nts id="Seg_200" n="HIAT:ip">,</nts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_203" n="HIAT:w" s="T56">di͡ebit</ts>
                  <nts id="Seg_204" n="HIAT:ip">.</nts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T60" id="Seg_207" n="HIAT:u" s="T57">
                  <nts id="Seg_208" n="HIAT:ip">"</nts>
                  <ts e="T58" id="Seg_210" n="HIAT:w" s="T57">Dʼon</ts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_213" n="HIAT:w" s="T58">kü͡ölge</ts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_216" n="HIAT:w" s="T59">ölörü͡öktere</ts>
                  <nts id="Seg_217" n="HIAT:ip">.</nts>
                  <nts id="Seg_218" n="HIAT:ip">"</nts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T65" id="Seg_221" n="HIAT:u" s="T60">
                  <ts e="T61" id="Seg_223" n="HIAT:w" s="T60">Dogoro</ts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_226" n="HIAT:w" s="T61">istibetek</ts>
                  <nts id="Seg_227" n="HIAT:ip">,</nts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_230" n="HIAT:w" s="T62">kü͡ölge</ts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_233" n="HIAT:w" s="T63">haːrɨː</ts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_236" n="HIAT:w" s="T64">kaːlbɨt</ts>
                  <nts id="Seg_237" n="HIAT:ip">.</nts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T76" id="Seg_240" n="HIAT:u" s="T65">
                  <ts e="T66" id="Seg_242" n="HIAT:w" s="T65">Bu</ts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_245" n="HIAT:w" s="T66">kaːlbɨtɨgar</ts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_248" n="HIAT:w" s="T67">kajdak</ts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_251" n="HIAT:w" s="T68">bu͡olu͡oj</ts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_253" n="HIAT:ip">—</nts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_256" n="HIAT:w" s="T69">biːrde</ts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_259" n="HIAT:w" s="T70">haŋata</ts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_262" n="HIAT:w" s="T71">köːkün</ts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_265" n="HIAT:w" s="T72">bu͡olbut</ts>
                  <nts id="Seg_266" n="HIAT:ip">,</nts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_269" n="HIAT:w" s="T73">manɨ</ts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_272" n="HIAT:w" s="T74">dogoro</ts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_275" n="HIAT:w" s="T75">istibit</ts>
                  <nts id="Seg_276" n="HIAT:ip">.</nts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T79" id="Seg_279" n="HIAT:u" s="T76">
                  <ts e="T77" id="Seg_281" n="HIAT:w" s="T76">Ikki</ts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_284" n="HIAT:w" s="T77">kihi</ts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_287" n="HIAT:w" s="T78">bulbuttar</ts>
                  <nts id="Seg_288" n="HIAT:ip">.</nts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T83" id="Seg_291" n="HIAT:u" s="T79">
                  <ts e="T80" id="Seg_293" n="HIAT:w" s="T79">Bu</ts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_296" n="HIAT:w" s="T80">kihilerten</ts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_299" n="HIAT:w" s="T81">biːrdere</ts>
                  <nts id="Seg_300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_302" n="HIAT:w" s="T82">eter</ts>
                  <nts id="Seg_303" n="HIAT:ip">:</nts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T89" id="Seg_306" n="HIAT:u" s="T83">
                  <nts id="Seg_307" n="HIAT:ip">"</nts>
                  <ts e="T84" id="Seg_309" n="HIAT:w" s="T83">Tu͡ok</ts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_312" n="HIAT:w" s="T84">dʼiktitej</ts>
                  <nts id="Seg_313" n="HIAT:ip">,</nts>
                  <nts id="Seg_314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_316" n="HIAT:w" s="T85">dogor</ts>
                  <nts id="Seg_317" n="HIAT:ip">,</nts>
                  <nts id="Seg_318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_320" n="HIAT:w" s="T86">kihiliː</ts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_323" n="HIAT:w" s="T87">ü͡ögülüːr</ts>
                  <nts id="Seg_324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_326" n="HIAT:w" s="T88">kurduk</ts>
                  <nts id="Seg_327" n="HIAT:ip">.</nts>
                  <nts id="Seg_328" n="HIAT:ip">"</nts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T92" id="Seg_331" n="HIAT:u" s="T89">
                  <ts e="T90" id="Seg_333" n="HIAT:w" s="T89">Onu</ts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_336" n="HIAT:w" s="T90">dogoro</ts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_339" n="HIAT:w" s="T91">eter</ts>
                  <nts id="Seg_340" n="HIAT:ip">:</nts>
                  <nts id="Seg_341" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T101" id="Seg_343" n="HIAT:u" s="T92">
                  <nts id="Seg_344" n="HIAT:ip">"</nts>
                  <ts e="T93" id="Seg_346" n="HIAT:w" s="T92">Tu͡ok</ts>
                  <nts id="Seg_347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_349" n="HIAT:w" s="T93">bu͡olu͡oj</ts>
                  <nts id="Seg_350" n="HIAT:ip">,</nts>
                  <nts id="Seg_351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_353" n="HIAT:w" s="T94">kötör</ts>
                  <nts id="Seg_354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_356" n="HIAT:w" s="T95">bu͡ollaga</ts>
                  <nts id="Seg_357" n="HIAT:ip">"</nts>
                  <nts id="Seg_358" n="HIAT:ip">,</nts>
                  <nts id="Seg_359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_361" n="HIAT:w" s="T96">di͡en</ts>
                  <nts id="Seg_362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_364" n="HIAT:w" s="T97">baː</ts>
                  <nts id="Seg_365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_367" n="HIAT:w" s="T98">ojun-kubanɨ</ts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_370" n="HIAT:w" s="T99">ɨtan</ts>
                  <nts id="Seg_371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_373" n="HIAT:w" s="T100">keːher</ts>
                  <nts id="Seg_374" n="HIAT:ip">.</nts>
                  <nts id="Seg_375" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T112" id="Seg_377" n="HIAT:u" s="T101">
                  <ts e="T102" id="Seg_379" n="HIAT:w" s="T101">ɨtan</ts>
                  <nts id="Seg_380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_382" n="HIAT:w" s="T102">baran</ts>
                  <nts id="Seg_383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_385" n="HIAT:w" s="T103">ɨlallar</ts>
                  <nts id="Seg_386" n="HIAT:ip">,</nts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_389" n="HIAT:w" s="T104">ɨlallar</ts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_392" n="HIAT:w" s="T105">da</ts>
                  <nts id="Seg_393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_395" n="HIAT:w" s="T106">hüleller</ts>
                  <nts id="Seg_396" n="HIAT:ip">,</nts>
                  <nts id="Seg_397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_399" n="HIAT:w" s="T107">körbüttere</ts>
                  <nts id="Seg_400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_401" n="HIAT:ip">—</nts>
                  <nts id="Seg_402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_404" n="HIAT:w" s="T108">telegiːleːk</ts>
                  <nts id="Seg_405" n="HIAT:ip">,</nts>
                  <nts id="Seg_406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_408" n="HIAT:w" s="T109">ojun</ts>
                  <nts id="Seg_409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_411" n="HIAT:w" s="T110">taŋastaːk</ts>
                  <nts id="Seg_412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_414" n="HIAT:w" s="T111">kihi</ts>
                  <nts id="Seg_415" n="HIAT:ip">.</nts>
                  <nts id="Seg_416" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T116" id="Seg_418" n="HIAT:u" s="T112">
                  <ts e="T113" id="Seg_420" n="HIAT:w" s="T112">Tugu</ts>
                  <nts id="Seg_421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_423" n="HIAT:w" s="T113">gɨnɨ͡aktaraj</ts>
                  <nts id="Seg_424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_426" n="HIAT:w" s="T114">bɨragan</ts>
                  <nts id="Seg_427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_429" n="HIAT:w" s="T115">keːspitter</ts>
                  <nts id="Seg_430" n="HIAT:ip">.</nts>
                  <nts id="Seg_431" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T125" id="Seg_433" n="HIAT:u" s="T116">
                  <ts e="T117" id="Seg_435" n="HIAT:w" s="T116">Dogoro</ts>
                  <nts id="Seg_436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_438" n="HIAT:w" s="T117">keler</ts>
                  <nts id="Seg_439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_441" n="HIAT:w" s="T118">eppit</ts>
                  <nts id="Seg_442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_444" n="HIAT:w" s="T119">kemiger</ts>
                  <nts id="Seg_445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_447" n="HIAT:w" s="T120">baː</ts>
                  <nts id="Seg_448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_450" n="HIAT:w" s="T121">čeːlkeː</ts>
                  <nts id="Seg_451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_453" n="HIAT:w" s="T122">tugut</ts>
                  <nts id="Seg_454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_456" n="HIAT:w" s="T123">etteːk</ts>
                  <nts id="Seg_457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_459" n="HIAT:w" s="T124">urahaga</ts>
                  <nts id="Seg_460" n="HIAT:ip">.</nts>
                  <nts id="Seg_461" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T134" id="Seg_463" n="HIAT:u" s="T125">
                  <ts e="T126" id="Seg_465" n="HIAT:w" s="T125">Dʼe</ts>
                  <nts id="Seg_466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_468" n="HIAT:w" s="T126">kubatɨn</ts>
                  <nts id="Seg_469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_471" n="HIAT:w" s="T127">dʼühüŋün</ts>
                  <nts id="Seg_472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_474" n="HIAT:w" s="T128">ularɨtan</ts>
                  <nts id="Seg_475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_477" n="HIAT:w" s="T129">kihi</ts>
                  <nts id="Seg_478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_480" n="HIAT:w" s="T130">bu͡olan</ts>
                  <nts id="Seg_481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_483" n="HIAT:w" s="T131">kaːlar</ts>
                  <nts id="Seg_484" n="HIAT:ip">,</nts>
                  <nts id="Seg_485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_487" n="HIAT:w" s="T132">dogoro</ts>
                  <nts id="Seg_488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_490" n="HIAT:w" s="T133">ölbütün</ts>
                  <nts id="Seg_491" n="HIAT:ip">.</nts>
                  <nts id="Seg_492" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T134" id="Seg_493" n="sc" s="T0">
               <ts e="T1" id="Seg_495" n="e" s="T0">Bɨlɨr </ts>
               <ts e="T2" id="Seg_497" n="e" s="T1">ikki </ts>
               <ts e="T3" id="Seg_499" n="e" s="T2">ojun </ts>
               <ts e="T4" id="Seg_501" n="e" s="T3">kɨːra </ts>
               <ts e="T5" id="Seg_503" n="e" s="T4">turannar </ts>
               <ts e="T6" id="Seg_505" n="e" s="T5">eppitter: </ts>
               <ts e="T7" id="Seg_507" n="e" s="T6">"Bihigi </ts>
               <ts e="T8" id="Seg_509" n="e" s="T7">kuba </ts>
               <ts e="T9" id="Seg_511" n="e" s="T8">bu͡olan, </ts>
               <ts e="T10" id="Seg_513" n="e" s="T9">kötör </ts>
               <ts e="T11" id="Seg_515" n="e" s="T10">barar </ts>
               <ts e="T12" id="Seg_517" n="e" s="T11">hiriger </ts>
               <ts e="T13" id="Seg_519" n="e" s="T12">kötü͡ökpüt. </ts>
               <ts e="T14" id="Seg_521" n="e" s="T13">Anɨ </ts>
               <ts e="T15" id="Seg_523" n="e" s="T14">haːs </ts>
               <ts e="T16" id="Seg_525" n="e" s="T15">togus </ts>
               <ts e="T17" id="Seg_527" n="e" s="T16">kuba </ts>
               <ts e="T18" id="Seg_529" n="e" s="T17">bu͡olan </ts>
               <ts e="T19" id="Seg_531" n="e" s="T18">dʼi͡egitin </ts>
               <ts e="T20" id="Seg_533" n="e" s="T19">üste </ts>
               <ts e="T21" id="Seg_535" n="e" s="T20">ergiji͡ekpit, </ts>
               <ts e="T22" id="Seg_537" n="e" s="T21">bu </ts>
               <ts e="T23" id="Seg_539" n="e" s="T22">biri͡emege </ts>
               <ts e="T24" id="Seg_541" n="e" s="T23">bihigini </ts>
               <ts e="T25" id="Seg_543" n="e" s="T24">tohujaːrɨŋ, </ts>
               <ts e="T26" id="Seg_545" n="e" s="T25">togus </ts>
               <ts e="T27" id="Seg_547" n="e" s="T26">čeːlkeː </ts>
               <ts e="T28" id="Seg_549" n="e" s="T27">tugutu </ts>
               <ts e="T29" id="Seg_551" n="e" s="T28">ölöröŋŋüt, </ts>
               <ts e="T30" id="Seg_553" n="e" s="T29">manɨ </ts>
               <ts e="T31" id="Seg_555" n="e" s="T30">etin </ts>
               <ts e="T32" id="Seg_557" n="e" s="T31">dʼonton </ts>
               <ts e="T33" id="Seg_559" n="e" s="T32">ɨraːk </ts>
               <ts e="T34" id="Seg_561" n="e" s="T33">turu͡orbut </ts>
               <ts e="T35" id="Seg_563" n="e" s="T34">haŋa </ts>
               <ts e="T36" id="Seg_565" n="e" s="T35">öldüːnneːk </ts>
               <ts e="T37" id="Seg_567" n="e" s="T36">uraha </ts>
               <ts e="T38" id="Seg_569" n="e" s="T37">ihiger </ts>
               <ts e="T39" id="Seg_571" n="e" s="T38">uːraŋŋɨt." </ts>
               <ts e="T40" id="Seg_573" n="e" s="T39">Kuba </ts>
               <ts e="T41" id="Seg_575" n="e" s="T40">bu͡olbuttar </ts>
               <ts e="T42" id="Seg_577" n="e" s="T41">daː </ts>
               <ts e="T43" id="Seg_579" n="e" s="T42">köppüttere </ts>
               <ts e="T44" id="Seg_581" n="e" s="T43">ajɨːta. </ts>
               <ts e="T45" id="Seg_583" n="e" s="T44">Kaːs-kuba </ts>
               <ts e="T46" id="Seg_585" n="e" s="T45">haːrɨːr </ts>
               <ts e="T47" id="Seg_587" n="e" s="T46">hiriger </ts>
               <ts e="T48" id="Seg_589" n="e" s="T47">tijbitter </ts>
               <ts e="T49" id="Seg_591" n="e" s="T48">baː </ts>
               <ts e="T50" id="Seg_593" n="e" s="T49">ojuttar. </ts>
               <ts e="T51" id="Seg_595" n="e" s="T50">"Dʼe, </ts>
               <ts e="T52" id="Seg_597" n="e" s="T51">haːrɨ͡ak", </ts>
               <ts e="T53" id="Seg_599" n="e" s="T52">di͡ebit </ts>
               <ts e="T54" id="Seg_601" n="e" s="T53">biːrgehe. </ts>
               <ts e="T55" id="Seg_603" n="e" s="T54">"Ebege </ts>
               <ts e="T56" id="Seg_605" n="e" s="T55">haːrɨ͡ak", </ts>
               <ts e="T57" id="Seg_607" n="e" s="T56">di͡ebit. </ts>
               <ts e="T58" id="Seg_609" n="e" s="T57">"Dʼon </ts>
               <ts e="T59" id="Seg_611" n="e" s="T58">kü͡ölge </ts>
               <ts e="T60" id="Seg_613" n="e" s="T59">ölörü͡öktere." </ts>
               <ts e="T61" id="Seg_615" n="e" s="T60">Dogoro </ts>
               <ts e="T62" id="Seg_617" n="e" s="T61">istibetek, </ts>
               <ts e="T63" id="Seg_619" n="e" s="T62">kü͡ölge </ts>
               <ts e="T64" id="Seg_621" n="e" s="T63">haːrɨː </ts>
               <ts e="T65" id="Seg_623" n="e" s="T64">kaːlbɨt. </ts>
               <ts e="T66" id="Seg_625" n="e" s="T65">Bu </ts>
               <ts e="T67" id="Seg_627" n="e" s="T66">kaːlbɨtɨgar </ts>
               <ts e="T68" id="Seg_629" n="e" s="T67">kajdak </ts>
               <ts e="T69" id="Seg_631" n="e" s="T68">bu͡olu͡oj — </ts>
               <ts e="T70" id="Seg_633" n="e" s="T69">biːrde </ts>
               <ts e="T71" id="Seg_635" n="e" s="T70">haŋata </ts>
               <ts e="T72" id="Seg_637" n="e" s="T71">köːkün </ts>
               <ts e="T73" id="Seg_639" n="e" s="T72">bu͡olbut, </ts>
               <ts e="T74" id="Seg_641" n="e" s="T73">manɨ </ts>
               <ts e="T75" id="Seg_643" n="e" s="T74">dogoro </ts>
               <ts e="T76" id="Seg_645" n="e" s="T75">istibit. </ts>
               <ts e="T77" id="Seg_647" n="e" s="T76">Ikki </ts>
               <ts e="T78" id="Seg_649" n="e" s="T77">kihi </ts>
               <ts e="T79" id="Seg_651" n="e" s="T78">bulbuttar. </ts>
               <ts e="T80" id="Seg_653" n="e" s="T79">Bu </ts>
               <ts e="T81" id="Seg_655" n="e" s="T80">kihilerten </ts>
               <ts e="T82" id="Seg_657" n="e" s="T81">biːrdere </ts>
               <ts e="T83" id="Seg_659" n="e" s="T82">eter: </ts>
               <ts e="T84" id="Seg_661" n="e" s="T83">"Tu͡ok </ts>
               <ts e="T85" id="Seg_663" n="e" s="T84">dʼiktitej, </ts>
               <ts e="T86" id="Seg_665" n="e" s="T85">dogor, </ts>
               <ts e="T87" id="Seg_667" n="e" s="T86">kihiliː </ts>
               <ts e="T88" id="Seg_669" n="e" s="T87">ü͡ögülüːr </ts>
               <ts e="T89" id="Seg_671" n="e" s="T88">kurduk." </ts>
               <ts e="T90" id="Seg_673" n="e" s="T89">Onu </ts>
               <ts e="T91" id="Seg_675" n="e" s="T90">dogoro </ts>
               <ts e="T92" id="Seg_677" n="e" s="T91">eter: </ts>
               <ts e="T93" id="Seg_679" n="e" s="T92">"Tu͡ok </ts>
               <ts e="T94" id="Seg_681" n="e" s="T93">bu͡olu͡oj, </ts>
               <ts e="T95" id="Seg_683" n="e" s="T94">kötör </ts>
               <ts e="T96" id="Seg_685" n="e" s="T95">bu͡ollaga", </ts>
               <ts e="T97" id="Seg_687" n="e" s="T96">di͡en </ts>
               <ts e="T98" id="Seg_689" n="e" s="T97">baː </ts>
               <ts e="T99" id="Seg_691" n="e" s="T98">ojun-kubanɨ </ts>
               <ts e="T100" id="Seg_693" n="e" s="T99">ɨtan </ts>
               <ts e="T101" id="Seg_695" n="e" s="T100">keːher. </ts>
               <ts e="T102" id="Seg_697" n="e" s="T101">ɨtan </ts>
               <ts e="T103" id="Seg_699" n="e" s="T102">baran </ts>
               <ts e="T104" id="Seg_701" n="e" s="T103">ɨlallar, </ts>
               <ts e="T105" id="Seg_703" n="e" s="T104">ɨlallar </ts>
               <ts e="T106" id="Seg_705" n="e" s="T105">da </ts>
               <ts e="T107" id="Seg_707" n="e" s="T106">hüleller, </ts>
               <ts e="T108" id="Seg_709" n="e" s="T107">körbüttere — </ts>
               <ts e="T109" id="Seg_711" n="e" s="T108">telegiːleːk, </ts>
               <ts e="T110" id="Seg_713" n="e" s="T109">ojun </ts>
               <ts e="T111" id="Seg_715" n="e" s="T110">taŋastaːk </ts>
               <ts e="T112" id="Seg_717" n="e" s="T111">kihi. </ts>
               <ts e="T113" id="Seg_719" n="e" s="T112">Tugu </ts>
               <ts e="T114" id="Seg_721" n="e" s="T113">gɨnɨ͡aktaraj </ts>
               <ts e="T115" id="Seg_723" n="e" s="T114">bɨragan </ts>
               <ts e="T116" id="Seg_725" n="e" s="T115">keːspitter. </ts>
               <ts e="T117" id="Seg_727" n="e" s="T116">Dogoro </ts>
               <ts e="T118" id="Seg_729" n="e" s="T117">keler </ts>
               <ts e="T119" id="Seg_731" n="e" s="T118">eppit </ts>
               <ts e="T120" id="Seg_733" n="e" s="T119">kemiger </ts>
               <ts e="T121" id="Seg_735" n="e" s="T120">baː </ts>
               <ts e="T122" id="Seg_737" n="e" s="T121">čeːlkeː </ts>
               <ts e="T123" id="Seg_739" n="e" s="T122">tugut </ts>
               <ts e="T124" id="Seg_741" n="e" s="T123">etteːk </ts>
               <ts e="T125" id="Seg_743" n="e" s="T124">urahaga. </ts>
               <ts e="T126" id="Seg_745" n="e" s="T125">Dʼe </ts>
               <ts e="T127" id="Seg_747" n="e" s="T126">kubatɨn </ts>
               <ts e="T128" id="Seg_749" n="e" s="T127">dʼühüŋün </ts>
               <ts e="T129" id="Seg_751" n="e" s="T128">ularɨtan </ts>
               <ts e="T130" id="Seg_753" n="e" s="T129">kihi </ts>
               <ts e="T131" id="Seg_755" n="e" s="T130">bu͡olan </ts>
               <ts e="T132" id="Seg_757" n="e" s="T131">kaːlar, </ts>
               <ts e="T133" id="Seg_759" n="e" s="T132">dogoro </ts>
               <ts e="T134" id="Seg_761" n="e" s="T133">ölbütün. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T6" id="Seg_762" s="T0">GoI_1930_ShamansTurnedIntoSwans_flk.001 (001.001)</ta>
            <ta e="T13" id="Seg_763" s="T6">GoI_1930_ShamansTurnedIntoSwans_flk.002 (001.002)</ta>
            <ta e="T39" id="Seg_764" s="T13">GoI_1930_ShamansTurnedIntoSwans_flk.003 (001.003)</ta>
            <ta e="T44" id="Seg_765" s="T39">GoI_1930_ShamansTurnedIntoSwans_flk.004 (001.004)</ta>
            <ta e="T50" id="Seg_766" s="T44">GoI_1930_ShamansTurnedIntoSwans_flk.005 (001.005)</ta>
            <ta e="T54" id="Seg_767" s="T50">GoI_1930_ShamansTurnedIntoSwans_flk.006 (001.006)</ta>
            <ta e="T57" id="Seg_768" s="T54">GoI_1930_ShamansTurnedIntoSwans_flk.007 (001.007)</ta>
            <ta e="T60" id="Seg_769" s="T57">GoI_1930_ShamansTurnedIntoSwans_flk.008 (001.008)</ta>
            <ta e="T65" id="Seg_770" s="T60">GoI_1930_ShamansTurnedIntoSwans_flk.009 (001.009)</ta>
            <ta e="T76" id="Seg_771" s="T65">GoI_1930_ShamansTurnedIntoSwans_flk.010 (001.010)</ta>
            <ta e="T79" id="Seg_772" s="T76">GoI_1930_ShamansTurnedIntoSwans_flk.011 (001.011)</ta>
            <ta e="T83" id="Seg_773" s="T79">GoI_1930_ShamansTurnedIntoSwans_flk.012 (001.012)</ta>
            <ta e="T89" id="Seg_774" s="T83">GoI_1930_ShamansTurnedIntoSwans_flk.013 (001.013)</ta>
            <ta e="T92" id="Seg_775" s="T89">GoI_1930_ShamansTurnedIntoSwans_flk.014 (001.014)</ta>
            <ta e="T101" id="Seg_776" s="T92">GoI_1930_ShamansTurnedIntoSwans_flk.015 (001.015)</ta>
            <ta e="T112" id="Seg_777" s="T101">GoI_1930_ShamansTurnedIntoSwans_flk.016 (001.016)</ta>
            <ta e="T116" id="Seg_778" s="T112">GoI_1930_ShamansTurnedIntoSwans_flk.017 (001.017)</ta>
            <ta e="T125" id="Seg_779" s="T116">GoI_1930_ShamansTurnedIntoSwans_flk.018 (001.018)</ta>
            <ta e="T134" id="Seg_780" s="T125">GoI_1930_ShamansTurnedIntoSwans_flk.019 (001.019)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T6" id="Seg_781" s="T0">Былыр икки ойун кыыра тураннар эппиттэр:</ta>
            <ta e="T13" id="Seg_782" s="T6">— Биһиги куба буолан, көтөр барар һиригэр көтүөкпүт.</ta>
            <ta e="T39" id="Seg_783" s="T13">Аны һаас тогус куба буолан дьиэгитин үстэ эргийиэкпит, бу бириэмэгэ биһигини тоһуйаарыҥ, тогус чээлкээ тугуту өлөрөҥҥүт, маны этин дьонтон ыраак туруорбут һаҥа өлдүүннээк* ураһа иһигэр уураҥҥыт.</ta>
            <ta e="T44" id="Seg_784" s="T39">Куба буолбуттар даа көппүттэрэ айыыта.</ta>
            <ta e="T50" id="Seg_785" s="T44">Каас-куба һаарыыр һиригэр тийбиттэр баа ойуттар.</ta>
            <ta e="T54" id="Seg_786" s="T50">— Дьэ, һаарыак, — диэбит бииргэһэ.</ta>
            <ta e="T57" id="Seg_787" s="T54">— Эбэгэ һаарыак, — диэбит.</ta>
            <ta e="T60" id="Seg_788" s="T57">— Дьон күөлгэ өлөрүөктэрэ.</ta>
            <ta e="T65" id="Seg_789" s="T60">Догоро истибэтэк, күөлгэ һаарыы каалбыт.</ta>
            <ta e="T76" id="Seg_790" s="T65">Бу каалбытыгар кайдак буолуой — биирдэ һаҥата көөкүн буолбут, маны догоро истибит.</ta>
            <ta e="T79" id="Seg_791" s="T76">Икки киһи булбуттар.</ta>
            <ta e="T83" id="Seg_792" s="T79">Бу киһилэртэн биирдэрэ этэр:</ta>
            <ta e="T89" id="Seg_793" s="T83">— Туок дьиктитэй, догор, киһилии үөгүлүүр курдук.</ta>
            <ta e="T92" id="Seg_794" s="T89">Ону догоро этэр:</ta>
            <ta e="T101" id="Seg_795" s="T92">— Туок буолуой, көтөр буоллага, — диэн баа ойун-кубаны ытан кээһэр.</ta>
            <ta e="T112" id="Seg_796" s="T101">Ытан баран ылаллар, ылаллар да һүлэллэр, көрбүттэрэ — тэлэгиилээк, ойун таҥастаак киһи.</ta>
            <ta e="T116" id="Seg_797" s="T112">Тугу гыныактарай быраган кээспиттэр.</ta>
            <ta e="T125" id="Seg_798" s="T116">Догоро кэлэр эппит кэмигэр баа чээлкээ тугут эттээк ураһага.</ta>
            <ta e="T134" id="Seg_799" s="T125">Дьэ кубатын дьүһүҥүн уларытан киһи буолан каалар, догоро өлбүтүн[эн].</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T6" id="Seg_800" s="T0">Bɨlɨr ikki ojun kɨːra turannar eppitter: </ta>
            <ta e="T13" id="Seg_801" s="T6">"Bihigi kuba bu͡olan, kötör barar hiriger kötü͡ökpüt. </ta>
            <ta e="T39" id="Seg_802" s="T13">Anɨ haːs togus kuba bu͡olan dʼi͡egitin üste ergiji͡ekpit, bu biri͡emege bihigini tohujaːrɨŋ, togus čeːlkeː tugutu ölöröŋŋüt, manɨ etin dʼonton ɨraːk turu͡orbut haŋa öldüːnneːk uraha ihiger uːraŋŋɨt." </ta>
            <ta e="T44" id="Seg_803" s="T39">Kuba bu͡olbuttar daː köppüttere ajɨːta. </ta>
            <ta e="T50" id="Seg_804" s="T44">Kaːs-kuba haːrɨːr hiriger tijbitter baː ojuttar. </ta>
            <ta e="T54" id="Seg_805" s="T50">"Dʼe, haːrɨ͡ak", di͡ebit biːrgehe. </ta>
            <ta e="T57" id="Seg_806" s="T54">"Ebege haːrɨ͡ak", di͡ebit. </ta>
            <ta e="T60" id="Seg_807" s="T57">"Dʼon kü͡ölge ölörü͡öktere." </ta>
            <ta e="T65" id="Seg_808" s="T60">Dogoro istibetek, kü͡ölge haːrɨː kaːlbɨt. </ta>
            <ta e="T76" id="Seg_809" s="T65">Bu kaːlbɨtɨgar kajdak bu͡olu͡oj — biːrde haŋata köːkün bu͡olbut, manɨ dogoro istibit. </ta>
            <ta e="T79" id="Seg_810" s="T76">Ikki kihi bulbuttar. </ta>
            <ta e="T83" id="Seg_811" s="T79">Bu kihilerten biːrdere eter: </ta>
            <ta e="T89" id="Seg_812" s="T83">"Tu͡ok dʼiktitej, dogor, kihiliː ü͡ögülüːr kurduk." </ta>
            <ta e="T92" id="Seg_813" s="T89">Onu dogoro eter: </ta>
            <ta e="T101" id="Seg_814" s="T92">"Tu͡ok bu͡olu͡oj, kötör bu͡ollaga", di͡en baː ojun-kubanɨ ɨtan keːher. </ta>
            <ta e="T112" id="Seg_815" s="T101">ɨtan baran ɨlallar, ɨlallar da hüleller, körbüttere — telegiːleːk, ojun taŋastaːk kihi. </ta>
            <ta e="T116" id="Seg_816" s="T112">Tugu gɨnɨ͡aktaraj bɨragan keːspitter. </ta>
            <ta e="T125" id="Seg_817" s="T116">Dogoro keler eppit kemiger baː čeːlkeː tugut etteːk urahaga. </ta>
            <ta e="T134" id="Seg_818" s="T125">Dʼe kubatɨn dʼühüŋün ularɨtan kihi bu͡olan kaːlar, dogoro ölbütün. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_819" s="T0">bɨlɨr</ta>
            <ta e="T2" id="Seg_820" s="T1">ikki</ta>
            <ta e="T3" id="Seg_821" s="T2">ojun</ta>
            <ta e="T4" id="Seg_822" s="T3">kɨːr-a</ta>
            <ta e="T5" id="Seg_823" s="T4">tur-an-nar</ta>
            <ta e="T6" id="Seg_824" s="T5">ep-pit-ter</ta>
            <ta e="T7" id="Seg_825" s="T6">bihigi</ta>
            <ta e="T8" id="Seg_826" s="T7">kuba</ta>
            <ta e="T9" id="Seg_827" s="T8">bu͡ol-an</ta>
            <ta e="T10" id="Seg_828" s="T9">kötör</ta>
            <ta e="T11" id="Seg_829" s="T10">bar-ar</ta>
            <ta e="T12" id="Seg_830" s="T11">hir-i-ger</ta>
            <ta e="T13" id="Seg_831" s="T12">köt-ü͡ök-püt</ta>
            <ta e="T14" id="Seg_832" s="T13">anɨ</ta>
            <ta e="T15" id="Seg_833" s="T14">haːs</ta>
            <ta e="T16" id="Seg_834" s="T15">togus</ta>
            <ta e="T17" id="Seg_835" s="T16">kuba</ta>
            <ta e="T18" id="Seg_836" s="T17">bu͡ol-an</ta>
            <ta e="T19" id="Seg_837" s="T18">dʼi͡e-giti-n</ta>
            <ta e="T20" id="Seg_838" s="T19">üs-te</ta>
            <ta e="T21" id="Seg_839" s="T20">ergij-i͡ek-pit</ta>
            <ta e="T22" id="Seg_840" s="T21">bu</ta>
            <ta e="T23" id="Seg_841" s="T22">biri͡eme-ge</ta>
            <ta e="T24" id="Seg_842" s="T23">bihigi-ni</ta>
            <ta e="T25" id="Seg_843" s="T24">tohuj-aːr-ɨ-ŋ</ta>
            <ta e="T26" id="Seg_844" s="T25">togus</ta>
            <ta e="T27" id="Seg_845" s="T26">čeːlkeː</ta>
            <ta e="T28" id="Seg_846" s="T27">tugut-u</ta>
            <ta e="T29" id="Seg_847" s="T28">ölör-öŋ-ŋüt</ta>
            <ta e="T30" id="Seg_848" s="T29">ma-nɨ</ta>
            <ta e="T31" id="Seg_849" s="T30">et-i-n</ta>
            <ta e="T32" id="Seg_850" s="T31">dʼon-ton</ta>
            <ta e="T33" id="Seg_851" s="T32">ɨraːk</ta>
            <ta e="T34" id="Seg_852" s="T33">turu͡or-but</ta>
            <ta e="T35" id="Seg_853" s="T34">haŋa</ta>
            <ta e="T36" id="Seg_854" s="T35">öldüːn-neːk</ta>
            <ta e="T37" id="Seg_855" s="T36">uraha</ta>
            <ta e="T38" id="Seg_856" s="T37">ih-i-ger</ta>
            <ta e="T39" id="Seg_857" s="T38">uːr-aŋ-ŋɨt</ta>
            <ta e="T40" id="Seg_858" s="T39">kuba</ta>
            <ta e="T41" id="Seg_859" s="T40">bu͡ol-but-tar</ta>
            <ta e="T42" id="Seg_860" s="T41">daː</ta>
            <ta e="T43" id="Seg_861" s="T42">köp-püt-tere</ta>
            <ta e="T44" id="Seg_862" s="T43">ajɨːta</ta>
            <ta e="T45" id="Seg_863" s="T44">kaːs-kuba</ta>
            <ta e="T46" id="Seg_864" s="T45">haːrɨː-r</ta>
            <ta e="T47" id="Seg_865" s="T46">hir-i-ger</ta>
            <ta e="T48" id="Seg_866" s="T47">tij-bit-ter</ta>
            <ta e="T49" id="Seg_867" s="T48">baː</ta>
            <ta e="T50" id="Seg_868" s="T49">ojut-tar</ta>
            <ta e="T51" id="Seg_869" s="T50">dʼe</ta>
            <ta e="T52" id="Seg_870" s="T51">haːr-ɨ͡ak</ta>
            <ta e="T53" id="Seg_871" s="T52">di͡e-bit</ta>
            <ta e="T54" id="Seg_872" s="T53">biːrgeh-e</ta>
            <ta e="T55" id="Seg_873" s="T54">ebe-ge</ta>
            <ta e="T56" id="Seg_874" s="T55">haːr-ɨ͡ak</ta>
            <ta e="T57" id="Seg_875" s="T56">di͡e-bit</ta>
            <ta e="T58" id="Seg_876" s="T57">dʼon</ta>
            <ta e="T59" id="Seg_877" s="T58">kü͡öl-ge</ta>
            <ta e="T60" id="Seg_878" s="T59">ölör-ü͡ök-tere</ta>
            <ta e="T61" id="Seg_879" s="T60">dogor-o</ta>
            <ta e="T62" id="Seg_880" s="T61">ist-i-betek</ta>
            <ta e="T63" id="Seg_881" s="T62">kü͡öl-ge</ta>
            <ta e="T64" id="Seg_882" s="T63">haːr-ɨː</ta>
            <ta e="T65" id="Seg_883" s="T64">kaːl-bɨt</ta>
            <ta e="T66" id="Seg_884" s="T65">bu</ta>
            <ta e="T67" id="Seg_885" s="T66">kaːl-bɨt-ɨ-gar</ta>
            <ta e="T68" id="Seg_886" s="T67">kajdak</ta>
            <ta e="T69" id="Seg_887" s="T68">bu͡ol-u͡o=j</ta>
            <ta e="T70" id="Seg_888" s="T69">biːrde</ta>
            <ta e="T71" id="Seg_889" s="T70">haŋa-ta</ta>
            <ta e="T72" id="Seg_890" s="T71">köːkün</ta>
            <ta e="T73" id="Seg_891" s="T72">bu͡ol-but</ta>
            <ta e="T74" id="Seg_892" s="T73">ma-nɨ</ta>
            <ta e="T75" id="Seg_893" s="T74">dogor-o</ta>
            <ta e="T76" id="Seg_894" s="T75">ist-i-bit</ta>
            <ta e="T77" id="Seg_895" s="T76">ikki</ta>
            <ta e="T78" id="Seg_896" s="T77">kihi</ta>
            <ta e="T79" id="Seg_897" s="T78">bul-but-tar</ta>
            <ta e="T80" id="Seg_898" s="T79">bu</ta>
            <ta e="T81" id="Seg_899" s="T80">kihi-ler-ten</ta>
            <ta e="T82" id="Seg_900" s="T81">biːr-dere</ta>
            <ta e="T83" id="Seg_901" s="T82">et-er</ta>
            <ta e="T84" id="Seg_902" s="T83">tu͡ok</ta>
            <ta e="T85" id="Seg_903" s="T84">dʼikti-te=j</ta>
            <ta e="T86" id="Seg_904" s="T85">dogor</ta>
            <ta e="T87" id="Seg_905" s="T86">kihi-liː</ta>
            <ta e="T88" id="Seg_906" s="T87">ü͡ögülüː-r</ta>
            <ta e="T89" id="Seg_907" s="T88">kurduk</ta>
            <ta e="T90" id="Seg_908" s="T89">o-nu</ta>
            <ta e="T91" id="Seg_909" s="T90">dogor-o</ta>
            <ta e="T92" id="Seg_910" s="T91">et-er</ta>
            <ta e="T93" id="Seg_911" s="T92">tu͡ok</ta>
            <ta e="T94" id="Seg_912" s="T93">bu͡ol-u͡o=j</ta>
            <ta e="T95" id="Seg_913" s="T94">kötör</ta>
            <ta e="T96" id="Seg_914" s="T95">bu͡ollaga</ta>
            <ta e="T97" id="Seg_915" s="T96">di͡e-n</ta>
            <ta e="T98" id="Seg_916" s="T97">baː</ta>
            <ta e="T99" id="Seg_917" s="T98">ojun-kuba-nɨ</ta>
            <ta e="T100" id="Seg_918" s="T99">ɨt-an</ta>
            <ta e="T101" id="Seg_919" s="T100">keːh-er</ta>
            <ta e="T102" id="Seg_920" s="T101">ɨt-an</ta>
            <ta e="T103" id="Seg_921" s="T102">baran</ta>
            <ta e="T104" id="Seg_922" s="T103">ɨl-al-lar</ta>
            <ta e="T105" id="Seg_923" s="T104">ɨl-al-lar</ta>
            <ta e="T106" id="Seg_924" s="T105">da</ta>
            <ta e="T107" id="Seg_925" s="T106">hül-el-ler</ta>
            <ta e="T108" id="Seg_926" s="T107">kör-büt-tere</ta>
            <ta e="T109" id="Seg_927" s="T108">telegiː-leːk</ta>
            <ta e="T110" id="Seg_928" s="T109">ojun</ta>
            <ta e="T111" id="Seg_929" s="T110">taŋas-taːk</ta>
            <ta e="T112" id="Seg_930" s="T111">kihi</ta>
            <ta e="T113" id="Seg_931" s="T112">tug-u</ta>
            <ta e="T114" id="Seg_932" s="T113">gɨn-ɨ͡ak-tara=j</ta>
            <ta e="T115" id="Seg_933" s="T114">bɨrag-an</ta>
            <ta e="T116" id="Seg_934" s="T115">keːs-pit-ter</ta>
            <ta e="T117" id="Seg_935" s="T116">dogor-o</ta>
            <ta e="T118" id="Seg_936" s="T117">kel-er</ta>
            <ta e="T119" id="Seg_937" s="T118">ep-pit</ta>
            <ta e="T120" id="Seg_938" s="T119">kem-i-ger</ta>
            <ta e="T121" id="Seg_939" s="T120">baː</ta>
            <ta e="T122" id="Seg_940" s="T121">čeːlkeː</ta>
            <ta e="T123" id="Seg_941" s="T122">tugut</ta>
            <ta e="T124" id="Seg_942" s="T123">et-teːk</ta>
            <ta e="T125" id="Seg_943" s="T124">uraha-ga</ta>
            <ta e="T126" id="Seg_944" s="T125">dʼe</ta>
            <ta e="T127" id="Seg_945" s="T126">kuba-tɨ-n</ta>
            <ta e="T128" id="Seg_946" s="T127">dʼühün-ü-n</ta>
            <ta e="T129" id="Seg_947" s="T128">ularɨt-an</ta>
            <ta e="T130" id="Seg_948" s="T129">kihi</ta>
            <ta e="T131" id="Seg_949" s="T130">bu͡ol-an</ta>
            <ta e="T132" id="Seg_950" s="T131">kaːl-ar</ta>
            <ta e="T133" id="Seg_951" s="T132">dogor-o</ta>
            <ta e="T134" id="Seg_952" s="T133">öl-büt-ü-n</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_953" s="T0">bɨlɨr</ta>
            <ta e="T2" id="Seg_954" s="T1">ikki</ta>
            <ta e="T3" id="Seg_955" s="T2">ojun</ta>
            <ta e="T4" id="Seg_956" s="T3">kɨːr-A</ta>
            <ta e="T5" id="Seg_957" s="T4">tur-An-LAr</ta>
            <ta e="T6" id="Seg_958" s="T5">et-BIT-LAr</ta>
            <ta e="T7" id="Seg_959" s="T6">bihigi</ta>
            <ta e="T8" id="Seg_960" s="T7">kuba</ta>
            <ta e="T9" id="Seg_961" s="T8">bu͡ol-An</ta>
            <ta e="T10" id="Seg_962" s="T9">kötör</ta>
            <ta e="T11" id="Seg_963" s="T10">bar-Ar</ta>
            <ta e="T12" id="Seg_964" s="T11">hir-tI-GAr</ta>
            <ta e="T13" id="Seg_965" s="T12">köt-IAK-BIt</ta>
            <ta e="T14" id="Seg_966" s="T13">anɨ</ta>
            <ta e="T15" id="Seg_967" s="T14">haːs</ta>
            <ta e="T16" id="Seg_968" s="T15">togus</ta>
            <ta e="T17" id="Seg_969" s="T16">kuba</ta>
            <ta e="T18" id="Seg_970" s="T17">bu͡ol-An</ta>
            <ta e="T19" id="Seg_971" s="T18">dʼi͡e-GItI-n</ta>
            <ta e="T20" id="Seg_972" s="T19">üs-TA</ta>
            <ta e="T21" id="Seg_973" s="T20">ergij-IAK-BIt</ta>
            <ta e="T22" id="Seg_974" s="T21">bu</ta>
            <ta e="T23" id="Seg_975" s="T22">biri͡eme-GA</ta>
            <ta e="T24" id="Seg_976" s="T23">bihigi-nI</ta>
            <ta e="T25" id="Seg_977" s="T24">tohuj-Aːr-I-ŋ</ta>
            <ta e="T26" id="Seg_978" s="T25">togus</ta>
            <ta e="T27" id="Seg_979" s="T26">čeːlkeː</ta>
            <ta e="T28" id="Seg_980" s="T27">tugut-nI</ta>
            <ta e="T29" id="Seg_981" s="T28">ölör-An-GIt</ta>
            <ta e="T30" id="Seg_982" s="T29">bu-nI</ta>
            <ta e="T31" id="Seg_983" s="T30">et-tI-n</ta>
            <ta e="T32" id="Seg_984" s="T31">dʼon-ttAn</ta>
            <ta e="T33" id="Seg_985" s="T32">ɨraːk</ta>
            <ta e="T34" id="Seg_986" s="T33">turu͡or-BIT</ta>
            <ta e="T35" id="Seg_987" s="T34">haŋa</ta>
            <ta e="T36" id="Seg_988" s="T35">öldüːn-LAːK</ta>
            <ta e="T37" id="Seg_989" s="T36">uraha</ta>
            <ta e="T38" id="Seg_990" s="T37">is-tI-GAr</ta>
            <ta e="T39" id="Seg_991" s="T38">uːr-An-GIt</ta>
            <ta e="T40" id="Seg_992" s="T39">kuba</ta>
            <ta e="T41" id="Seg_993" s="T40">bu͡ol-BIT-LAr</ta>
            <ta e="T42" id="Seg_994" s="T41">da</ta>
            <ta e="T43" id="Seg_995" s="T42">köt-BIT-LArA</ta>
            <ta e="T44" id="Seg_996" s="T43">ajɨːta</ta>
            <ta e="T45" id="Seg_997" s="T44">kaːs-kuba</ta>
            <ta e="T46" id="Seg_998" s="T45">haːraː-Ar</ta>
            <ta e="T47" id="Seg_999" s="T46">hir-tI-GAr</ta>
            <ta e="T48" id="Seg_1000" s="T47">tij-BIT-LAr</ta>
            <ta e="T49" id="Seg_1001" s="T48">bu</ta>
            <ta e="T50" id="Seg_1002" s="T49">ojun-LAr</ta>
            <ta e="T51" id="Seg_1003" s="T50">dʼe</ta>
            <ta e="T52" id="Seg_1004" s="T51">haːraː-IAk</ta>
            <ta e="T53" id="Seg_1005" s="T52">di͡e-BIT</ta>
            <ta e="T54" id="Seg_1006" s="T53">biːrges-tA</ta>
            <ta e="T55" id="Seg_1007" s="T54">ebe-GA</ta>
            <ta e="T56" id="Seg_1008" s="T55">haːraː-IAk</ta>
            <ta e="T57" id="Seg_1009" s="T56">di͡e-BIT</ta>
            <ta e="T58" id="Seg_1010" s="T57">dʼon</ta>
            <ta e="T59" id="Seg_1011" s="T58">kü͡öl-GA</ta>
            <ta e="T60" id="Seg_1012" s="T59">ölör-IAK-LArA</ta>
            <ta e="T61" id="Seg_1013" s="T60">dogor-tA</ta>
            <ta e="T62" id="Seg_1014" s="T61">ihit-I-BAtAK</ta>
            <ta e="T63" id="Seg_1015" s="T62">kü͡öl-GA</ta>
            <ta e="T64" id="Seg_1016" s="T63">haːraː-A</ta>
            <ta e="T65" id="Seg_1017" s="T64">kaːl-BIT</ta>
            <ta e="T66" id="Seg_1018" s="T65">bu</ta>
            <ta e="T67" id="Seg_1019" s="T66">kaːl-BIT-tI-GAr</ta>
            <ta e="T68" id="Seg_1020" s="T67">kajdak</ta>
            <ta e="T69" id="Seg_1021" s="T68">bu͡ol-IAK.[tA]=Ij</ta>
            <ta e="T70" id="Seg_1022" s="T69">biːrde</ta>
            <ta e="T71" id="Seg_1023" s="T70">haŋa-tA</ta>
            <ta e="T72" id="Seg_1024" s="T71">köːkün</ta>
            <ta e="T73" id="Seg_1025" s="T72">bu͡ol-BIT</ta>
            <ta e="T74" id="Seg_1026" s="T73">bu-nI</ta>
            <ta e="T75" id="Seg_1027" s="T74">dogor-tA</ta>
            <ta e="T76" id="Seg_1028" s="T75">ihit-I-BIT</ta>
            <ta e="T77" id="Seg_1029" s="T76">ikki</ta>
            <ta e="T78" id="Seg_1030" s="T77">kihi</ta>
            <ta e="T79" id="Seg_1031" s="T78">bul-BIT-LAr</ta>
            <ta e="T80" id="Seg_1032" s="T79">bu</ta>
            <ta e="T81" id="Seg_1033" s="T80">kihi-LAr-ttAn</ta>
            <ta e="T82" id="Seg_1034" s="T81">biːr-LArA</ta>
            <ta e="T83" id="Seg_1035" s="T82">et-Ar</ta>
            <ta e="T84" id="Seg_1036" s="T83">tu͡ok</ta>
            <ta e="T85" id="Seg_1037" s="T84">dʼikti-tA=Ij</ta>
            <ta e="T86" id="Seg_1038" s="T85">dogor</ta>
            <ta e="T87" id="Seg_1039" s="T86">kihi-LIː</ta>
            <ta e="T88" id="Seg_1040" s="T87">ü͡ögüleː-Ar</ta>
            <ta e="T89" id="Seg_1041" s="T88">kurduk</ta>
            <ta e="T90" id="Seg_1042" s="T89">ol-nI</ta>
            <ta e="T91" id="Seg_1043" s="T90">dogor-tA</ta>
            <ta e="T92" id="Seg_1044" s="T91">et-Ar</ta>
            <ta e="T93" id="Seg_1045" s="T92">tu͡ok</ta>
            <ta e="T94" id="Seg_1046" s="T93">bu͡ol-IAK.[tA]=Ij</ta>
            <ta e="T95" id="Seg_1047" s="T94">kötör</ta>
            <ta e="T96" id="Seg_1048" s="T95">bu͡ollaga</ta>
            <ta e="T97" id="Seg_1049" s="T96">di͡e-An</ta>
            <ta e="T98" id="Seg_1050" s="T97">bu</ta>
            <ta e="T99" id="Seg_1051" s="T98">ojun-kuba-nI</ta>
            <ta e="T100" id="Seg_1052" s="T99">ɨt-An</ta>
            <ta e="T101" id="Seg_1053" s="T100">keːs-Ar</ta>
            <ta e="T102" id="Seg_1054" s="T101">ɨt-An</ta>
            <ta e="T103" id="Seg_1055" s="T102">baran</ta>
            <ta e="T104" id="Seg_1056" s="T103">ɨl-Ar-LAr</ta>
            <ta e="T105" id="Seg_1057" s="T104">ɨl-Ar-LAr</ta>
            <ta e="T106" id="Seg_1058" s="T105">da</ta>
            <ta e="T107" id="Seg_1059" s="T106">hül-Ar-LAr</ta>
            <ta e="T108" id="Seg_1060" s="T107">kör-BIT-LArA</ta>
            <ta e="T109" id="Seg_1061" s="T108">telegiː-LAːK</ta>
            <ta e="T110" id="Seg_1062" s="T109">ojun</ta>
            <ta e="T111" id="Seg_1063" s="T110">taŋas-LAːK</ta>
            <ta e="T112" id="Seg_1064" s="T111">kihi</ta>
            <ta e="T113" id="Seg_1065" s="T112">tu͡ok-nI</ta>
            <ta e="T114" id="Seg_1066" s="T113">gɨn-IAK-LArA=Ij</ta>
            <ta e="T115" id="Seg_1067" s="T114">bɨrak-An</ta>
            <ta e="T116" id="Seg_1068" s="T115">keːs-BIT-LAr</ta>
            <ta e="T117" id="Seg_1069" s="T116">dogor-tA</ta>
            <ta e="T118" id="Seg_1070" s="T117">kel-Ar</ta>
            <ta e="T119" id="Seg_1071" s="T118">et-BIT</ta>
            <ta e="T120" id="Seg_1072" s="T119">kem-tI-GAr</ta>
            <ta e="T121" id="Seg_1073" s="T120">bu</ta>
            <ta e="T122" id="Seg_1074" s="T121">čeːlkeː</ta>
            <ta e="T123" id="Seg_1075" s="T122">tugut</ta>
            <ta e="T124" id="Seg_1076" s="T123">et-LAːK</ta>
            <ta e="T125" id="Seg_1077" s="T124">uraha-GA</ta>
            <ta e="T126" id="Seg_1078" s="T125">dʼe</ta>
            <ta e="T127" id="Seg_1079" s="T126">kuba-tI-n</ta>
            <ta e="T128" id="Seg_1080" s="T127">dʼühün-tI-n</ta>
            <ta e="T129" id="Seg_1081" s="T128">ularɨt-An</ta>
            <ta e="T130" id="Seg_1082" s="T129">kihi</ta>
            <ta e="T131" id="Seg_1083" s="T130">bu͡ol-An</ta>
            <ta e="T132" id="Seg_1084" s="T131">kaːl-Ar</ta>
            <ta e="T133" id="Seg_1085" s="T132">dogor-tA</ta>
            <ta e="T134" id="Seg_1086" s="T133">öl-BIT-tI-n</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_1087" s="T0">long.ago</ta>
            <ta e="T2" id="Seg_1088" s="T1">two</ta>
            <ta e="T3" id="Seg_1089" s="T2">shaman.[NOM]</ta>
            <ta e="T4" id="Seg_1090" s="T3">shamanize-CVB.SIM</ta>
            <ta e="T5" id="Seg_1091" s="T4">stand-CVB.SEQ-3PL</ta>
            <ta e="T6" id="Seg_1092" s="T5">speak-PST2-3PL</ta>
            <ta e="T7" id="Seg_1093" s="T6">1PL.[NOM]</ta>
            <ta e="T8" id="Seg_1094" s="T7">swan.[NOM]</ta>
            <ta e="T9" id="Seg_1095" s="T8">become-CVB.SEQ</ta>
            <ta e="T10" id="Seg_1096" s="T9">bird.[NOM]</ta>
            <ta e="T11" id="Seg_1097" s="T10">go-PTCP.PRS</ta>
            <ta e="T12" id="Seg_1098" s="T11">place-3SG-DAT/LOC</ta>
            <ta e="T13" id="Seg_1099" s="T12">fly-FUT-1PL</ta>
            <ta e="T14" id="Seg_1100" s="T13">now</ta>
            <ta e="T15" id="Seg_1101" s="T14">in.spring</ta>
            <ta e="T16" id="Seg_1102" s="T15">nine</ta>
            <ta e="T17" id="Seg_1103" s="T16">swan.[NOM]</ta>
            <ta e="T18" id="Seg_1104" s="T17">be-CVB.SEQ</ta>
            <ta e="T19" id="Seg_1105" s="T18">house-2PL-ACC</ta>
            <ta e="T20" id="Seg_1106" s="T19">three-MLTP</ta>
            <ta e="T21" id="Seg_1107" s="T20">turn-FUT-1PL</ta>
            <ta e="T22" id="Seg_1108" s="T21">this</ta>
            <ta e="T23" id="Seg_1109" s="T22">time-DAT/LOC</ta>
            <ta e="T24" id="Seg_1110" s="T23">1PL-ACC</ta>
            <ta e="T25" id="Seg_1111" s="T24">receive-FUT-EP-IMP.2PL</ta>
            <ta e="T26" id="Seg_1112" s="T25">nine</ta>
            <ta e="T27" id="Seg_1113" s="T26">white</ta>
            <ta e="T28" id="Seg_1114" s="T27">reindeer.calf-ACC</ta>
            <ta e="T29" id="Seg_1115" s="T28">kill-CVB.SEQ-2PL</ta>
            <ta e="T30" id="Seg_1116" s="T29">this-ACC</ta>
            <ta e="T31" id="Seg_1117" s="T30">meat-3SG-ACC</ta>
            <ta e="T32" id="Seg_1118" s="T31">people-ABL</ta>
            <ta e="T33" id="Seg_1119" s="T32">far.away</ta>
            <ta e="T34" id="Seg_1120" s="T33">place-PTCP.PST</ta>
            <ta e="T35" id="Seg_1121" s="T34">new</ta>
            <ta e="T36" id="Seg_1122" s="T35">leather.cover.for.a.tent-PROPR</ta>
            <ta e="T37" id="Seg_1123" s="T36">pole.tent.[NOM]</ta>
            <ta e="T38" id="Seg_1124" s="T37">inside-3SG-DAT/LOC</ta>
            <ta e="T39" id="Seg_1125" s="T38">lay-CVB.SEQ-2PL</ta>
            <ta e="T40" id="Seg_1126" s="T39">swan.[NOM]</ta>
            <ta e="T41" id="Seg_1127" s="T40">become-PST2-3PL</ta>
            <ta e="T42" id="Seg_1128" s="T41">and</ta>
            <ta e="T43" id="Seg_1129" s="T42">fly-PST2-3PL</ta>
            <ta e="T44" id="Seg_1130" s="T43">EMPH</ta>
            <ta e="T45" id="Seg_1131" s="T44">goose.[NOM]-swan.[NOM]</ta>
            <ta e="T46" id="Seg_1132" s="T45">moult-PTCP.PRS</ta>
            <ta e="T47" id="Seg_1133" s="T46">place-3SG-DAT/LOC</ta>
            <ta e="T48" id="Seg_1134" s="T47">reach-PST2-3PL</ta>
            <ta e="T49" id="Seg_1135" s="T48">this</ta>
            <ta e="T50" id="Seg_1136" s="T49">shaman-PL.[NOM]</ta>
            <ta e="T51" id="Seg_1137" s="T50">well</ta>
            <ta e="T52" id="Seg_1138" s="T51">moult-IMP.1DU</ta>
            <ta e="T53" id="Seg_1139" s="T52">say-PST2.[3SG]</ta>
            <ta e="T54" id="Seg_1140" s="T53">one.out.of.two-3SG.[NOM]</ta>
            <ta e="T55" id="Seg_1141" s="T54">river-DAT/LOC</ta>
            <ta e="T56" id="Seg_1142" s="T55">moult-IMP.1DU</ta>
            <ta e="T57" id="Seg_1143" s="T56">say-PST2.[3SG]</ta>
            <ta e="T58" id="Seg_1144" s="T57">people.[NOM]</ta>
            <ta e="T59" id="Seg_1145" s="T58">lake-DAT/LOC</ta>
            <ta e="T60" id="Seg_1146" s="T59">kill-FUT-3PL</ta>
            <ta e="T61" id="Seg_1147" s="T60">friend-3SG.[NOM]</ta>
            <ta e="T62" id="Seg_1148" s="T61">hear-EP-PST2.NEG.[3SG]</ta>
            <ta e="T63" id="Seg_1149" s="T62">lake-DAT/LOC</ta>
            <ta e="T64" id="Seg_1150" s="T63">moult-CVB.SIM</ta>
            <ta e="T65" id="Seg_1151" s="T64">stay-PST2.[3SG]</ta>
            <ta e="T66" id="Seg_1152" s="T65">this</ta>
            <ta e="T67" id="Seg_1153" s="T66">stay-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T68" id="Seg_1154" s="T67">how</ta>
            <ta e="T69" id="Seg_1155" s="T68">be-FUT.[3SG]=Q</ta>
            <ta e="T70" id="Seg_1156" s="T69">once</ta>
            <ta e="T71" id="Seg_1157" s="T70">voice-3SG.[NOM]</ta>
            <ta e="T72" id="Seg_1158" s="T71">mumbling.[NOM]</ta>
            <ta e="T73" id="Seg_1159" s="T72">become-PST2.[3SG]</ta>
            <ta e="T74" id="Seg_1160" s="T73">this-ACC</ta>
            <ta e="T75" id="Seg_1161" s="T74">friend-3SG.[NOM]</ta>
            <ta e="T76" id="Seg_1162" s="T75">hear-EP-PST2.[3SG]</ta>
            <ta e="T77" id="Seg_1163" s="T76">two</ta>
            <ta e="T78" id="Seg_1164" s="T77">human.being.[NOM]</ta>
            <ta e="T79" id="Seg_1165" s="T78">find-PST2-3PL</ta>
            <ta e="T80" id="Seg_1166" s="T79">this</ta>
            <ta e="T81" id="Seg_1167" s="T80">human.being-PL-ABL</ta>
            <ta e="T82" id="Seg_1168" s="T81">one-3PL.[NOM]</ta>
            <ta e="T83" id="Seg_1169" s="T82">speak-PRS.[3SG]</ta>
            <ta e="T84" id="Seg_1170" s="T83">what.[NOM]</ta>
            <ta e="T85" id="Seg_1171" s="T84">miracle-3SG.[NOM]=Q</ta>
            <ta e="T86" id="Seg_1172" s="T85">friend.[NOM]</ta>
            <ta e="T87" id="Seg_1173" s="T86">human.being-SIM</ta>
            <ta e="T88" id="Seg_1174" s="T87">shout-PTCP.PRS.[NOM]</ta>
            <ta e="T89" id="Seg_1175" s="T88">like</ta>
            <ta e="T90" id="Seg_1176" s="T89">that-ACC</ta>
            <ta e="T91" id="Seg_1177" s="T90">friend-3SG.[NOM]</ta>
            <ta e="T92" id="Seg_1178" s="T91">speak-PRS.[3SG]</ta>
            <ta e="T93" id="Seg_1179" s="T92">what.[NOM]</ta>
            <ta e="T94" id="Seg_1180" s="T93">be-FUT.[3SG]=Q</ta>
            <ta e="T95" id="Seg_1181" s="T94">bird.[NOM]</ta>
            <ta e="T96" id="Seg_1182" s="T95">MOD</ta>
            <ta e="T97" id="Seg_1183" s="T96">say-CVB.SEQ</ta>
            <ta e="T98" id="Seg_1184" s="T97">this</ta>
            <ta e="T99" id="Seg_1185" s="T98">shaman-swan-ACC</ta>
            <ta e="T100" id="Seg_1186" s="T99">shoot-CVB.SEQ</ta>
            <ta e="T101" id="Seg_1187" s="T100">throw-PRS.[3SG]</ta>
            <ta e="T102" id="Seg_1188" s="T101">shoot-CVB.SEQ</ta>
            <ta e="T103" id="Seg_1189" s="T102">after</ta>
            <ta e="T104" id="Seg_1190" s="T103">take-PRS-3PL</ta>
            <ta e="T105" id="Seg_1191" s="T104">take-PRS-3PL</ta>
            <ta e="T106" id="Seg_1192" s="T105">and</ta>
            <ta e="T107" id="Seg_1193" s="T106">skin-PRS-3PL</ta>
            <ta e="T108" id="Seg_1194" s="T107">see-PST2-3PL</ta>
            <ta e="T109" id="Seg_1195" s="T108">belt-PROPR</ta>
            <ta e="T110" id="Seg_1196" s="T109">shaman.[NOM]</ta>
            <ta e="T111" id="Seg_1197" s="T110">clothes-PROPR</ta>
            <ta e="T112" id="Seg_1198" s="T111">human.being.[NOM]</ta>
            <ta e="T113" id="Seg_1199" s="T112">what-ACC</ta>
            <ta e="T114" id="Seg_1200" s="T113">make-FUT-3PL=Q</ta>
            <ta e="T115" id="Seg_1201" s="T114">throw-CVB.SEQ</ta>
            <ta e="T116" id="Seg_1202" s="T115">throw-PST2-3PL</ta>
            <ta e="T117" id="Seg_1203" s="T116">friend-3SG.[NOM]</ta>
            <ta e="T118" id="Seg_1204" s="T117">come-PRS.[3SG]</ta>
            <ta e="T119" id="Seg_1205" s="T118">speak-PTCP.PST</ta>
            <ta e="T120" id="Seg_1206" s="T119">time-3SG-DAT/LOC</ta>
            <ta e="T121" id="Seg_1207" s="T120">this</ta>
            <ta e="T122" id="Seg_1208" s="T121">white</ta>
            <ta e="T123" id="Seg_1209" s="T122">calf.[NOM]</ta>
            <ta e="T124" id="Seg_1210" s="T123">meat-PROPR</ta>
            <ta e="T125" id="Seg_1211" s="T124">pole.tent-DAT/LOC</ta>
            <ta e="T126" id="Seg_1212" s="T125">well</ta>
            <ta e="T127" id="Seg_1213" s="T126">swan-3SG-GEN</ta>
            <ta e="T128" id="Seg_1214" s="T127">shape-3SG-ACC</ta>
            <ta e="T129" id="Seg_1215" s="T128">change-CVB.SEQ</ta>
            <ta e="T130" id="Seg_1216" s="T129">human.being.[NOM]</ta>
            <ta e="T131" id="Seg_1217" s="T130">become-CVB.SEQ</ta>
            <ta e="T132" id="Seg_1218" s="T131">stay-PRS.[3SG]</ta>
            <ta e="T133" id="Seg_1219" s="T132">friend-3SG.[NOM]</ta>
            <ta e="T134" id="Seg_1220" s="T133">die-PTCP.PST-3SG-ACC</ta>
         </annotation>
         <annotation name="gg" tierref="gg">
            <ta e="T1" id="Seg_1221" s="T0">vor.langer.Zeit</ta>
            <ta e="T2" id="Seg_1222" s="T1">zwei</ta>
            <ta e="T3" id="Seg_1223" s="T2">Schamane.[NOM]</ta>
            <ta e="T4" id="Seg_1224" s="T3">schamanisieren-CVB.SIM</ta>
            <ta e="T5" id="Seg_1225" s="T4">stehen-CVB.SEQ-3PL</ta>
            <ta e="T6" id="Seg_1226" s="T5">sprechen-PST2-3PL</ta>
            <ta e="T7" id="Seg_1227" s="T6">1PL.[NOM]</ta>
            <ta e="T8" id="Seg_1228" s="T7">Schwan.[NOM]</ta>
            <ta e="T9" id="Seg_1229" s="T8">werden-CVB.SEQ</ta>
            <ta e="T10" id="Seg_1230" s="T9">Vogel.[NOM]</ta>
            <ta e="T11" id="Seg_1231" s="T10">gehen-PTCP.PRS</ta>
            <ta e="T12" id="Seg_1232" s="T11">Ort-3SG-DAT/LOC</ta>
            <ta e="T13" id="Seg_1233" s="T12">fliegen-FUT-1PL</ta>
            <ta e="T14" id="Seg_1234" s="T13">jetzt</ta>
            <ta e="T15" id="Seg_1235" s="T14">im.Frühling</ta>
            <ta e="T16" id="Seg_1236" s="T15">neun</ta>
            <ta e="T17" id="Seg_1237" s="T16">Schwan.[NOM]</ta>
            <ta e="T18" id="Seg_1238" s="T17">sein-CVB.SEQ</ta>
            <ta e="T19" id="Seg_1239" s="T18">Haus-2PL-ACC</ta>
            <ta e="T20" id="Seg_1240" s="T19">drei-MLTP</ta>
            <ta e="T21" id="Seg_1241" s="T20">sich.drehen-FUT-1PL</ta>
            <ta e="T22" id="Seg_1242" s="T21">dieses</ta>
            <ta e="T23" id="Seg_1243" s="T22">Zeit-DAT/LOC</ta>
            <ta e="T24" id="Seg_1244" s="T23">1PL-ACC</ta>
            <ta e="T25" id="Seg_1245" s="T24">empfangen-FUT-EP-IMP.2PL</ta>
            <ta e="T26" id="Seg_1246" s="T25">neun</ta>
            <ta e="T27" id="Seg_1247" s="T26">weiß</ta>
            <ta e="T28" id="Seg_1248" s="T27">Rentierkalb-ACC</ta>
            <ta e="T29" id="Seg_1249" s="T28">töten-CVB.SEQ-2PL</ta>
            <ta e="T30" id="Seg_1250" s="T29">dieses-ACC</ta>
            <ta e="T31" id="Seg_1251" s="T30">Fleisch-3SG-ACC</ta>
            <ta e="T32" id="Seg_1252" s="T31">Leute-ABL</ta>
            <ta e="T33" id="Seg_1253" s="T32">weit.weg</ta>
            <ta e="T34" id="Seg_1254" s="T33">stellen-PTCP.PST</ta>
            <ta e="T35" id="Seg_1255" s="T34">neu</ta>
            <ta e="T36" id="Seg_1256" s="T35">Lederdecke.eines.Zeltes-PROPR</ta>
            <ta e="T37" id="Seg_1257" s="T36">Stangenzelt.[NOM]</ta>
            <ta e="T38" id="Seg_1258" s="T37">Inneres-3SG-DAT/LOC</ta>
            <ta e="T39" id="Seg_1259" s="T38">legen-CVB.SEQ-2PL</ta>
            <ta e="T40" id="Seg_1260" s="T39">Schwan.[NOM]</ta>
            <ta e="T41" id="Seg_1261" s="T40">werden-PST2-3PL</ta>
            <ta e="T42" id="Seg_1262" s="T41">und</ta>
            <ta e="T43" id="Seg_1263" s="T42">fliegen-PST2-3PL</ta>
            <ta e="T44" id="Seg_1264" s="T43">EMPH</ta>
            <ta e="T45" id="Seg_1265" s="T44">Gans.[NOM]-Schwan.[NOM]</ta>
            <ta e="T46" id="Seg_1266" s="T45">sich.mausern-PTCP.PRS</ta>
            <ta e="T47" id="Seg_1267" s="T46">Ort-3SG-DAT/LOC</ta>
            <ta e="T48" id="Seg_1268" s="T47">ankommen-PST2-3PL</ta>
            <ta e="T49" id="Seg_1269" s="T48">dieses</ta>
            <ta e="T50" id="Seg_1270" s="T49">Schamane-PL.[NOM]</ta>
            <ta e="T51" id="Seg_1271" s="T50">doch</ta>
            <ta e="T52" id="Seg_1272" s="T51">sich.mausern-IMP.1DU</ta>
            <ta e="T53" id="Seg_1273" s="T52">sagen-PST2.[3SG]</ta>
            <ta e="T54" id="Seg_1274" s="T53">einer.von.zwei-3SG.[NOM]</ta>
            <ta e="T55" id="Seg_1275" s="T54">Fluss-DAT/LOC</ta>
            <ta e="T56" id="Seg_1276" s="T55">sich.mausern-IMP.1DU</ta>
            <ta e="T57" id="Seg_1277" s="T56">sagen-PST2.[3SG]</ta>
            <ta e="T58" id="Seg_1278" s="T57">Volk.[NOM]</ta>
            <ta e="T59" id="Seg_1279" s="T58">See-DAT/LOC</ta>
            <ta e="T60" id="Seg_1280" s="T59">töten-FUT-3PL</ta>
            <ta e="T61" id="Seg_1281" s="T60">Freund-3SG.[NOM]</ta>
            <ta e="T62" id="Seg_1282" s="T61">hören-EP-PST2.NEG.[3SG]</ta>
            <ta e="T63" id="Seg_1283" s="T62">See-DAT/LOC</ta>
            <ta e="T64" id="Seg_1284" s="T63">sich.mausern-CVB.SIM</ta>
            <ta e="T65" id="Seg_1285" s="T64">bleiben-PST2.[3SG]</ta>
            <ta e="T66" id="Seg_1286" s="T65">dieses</ta>
            <ta e="T67" id="Seg_1287" s="T66">bleiben-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T68" id="Seg_1288" s="T67">wie</ta>
            <ta e="T69" id="Seg_1289" s="T68">sein-FUT.[3SG]=Q</ta>
            <ta e="T70" id="Seg_1290" s="T69">einmal</ta>
            <ta e="T71" id="Seg_1291" s="T70">Stimme-3SG.[NOM]</ta>
            <ta e="T72" id="Seg_1292" s="T71">Gemurmel.[NOM]</ta>
            <ta e="T73" id="Seg_1293" s="T72">werden-PST2.[3SG]</ta>
            <ta e="T74" id="Seg_1294" s="T73">dieses-ACC</ta>
            <ta e="T75" id="Seg_1295" s="T74">Freund-3SG.[NOM]</ta>
            <ta e="T76" id="Seg_1296" s="T75">hören-EP-PST2.[3SG]</ta>
            <ta e="T77" id="Seg_1297" s="T76">zwei</ta>
            <ta e="T78" id="Seg_1298" s="T77">Mensch.[NOM]</ta>
            <ta e="T79" id="Seg_1299" s="T78">finden-PST2-3PL</ta>
            <ta e="T80" id="Seg_1300" s="T79">dieses</ta>
            <ta e="T81" id="Seg_1301" s="T80">Mensch-PL-ABL</ta>
            <ta e="T82" id="Seg_1302" s="T81">eins-3PL.[NOM]</ta>
            <ta e="T83" id="Seg_1303" s="T82">sprechen-PRS.[3SG]</ta>
            <ta e="T84" id="Seg_1304" s="T83">was.[NOM]</ta>
            <ta e="T85" id="Seg_1305" s="T84">Wunder-3SG.[NOM]=Q</ta>
            <ta e="T86" id="Seg_1306" s="T85">Freund.[NOM]</ta>
            <ta e="T87" id="Seg_1307" s="T86">Mensch-SIM</ta>
            <ta e="T88" id="Seg_1308" s="T87">schreien-PTCP.PRS.[NOM]</ta>
            <ta e="T89" id="Seg_1309" s="T88">wie</ta>
            <ta e="T90" id="Seg_1310" s="T89">jenes-ACC</ta>
            <ta e="T91" id="Seg_1311" s="T90">Freund-3SG.[NOM]</ta>
            <ta e="T92" id="Seg_1312" s="T91">sprechen-PRS.[3SG]</ta>
            <ta e="T93" id="Seg_1313" s="T92">was.[NOM]</ta>
            <ta e="T94" id="Seg_1314" s="T93">sein-FUT.[3SG]=Q</ta>
            <ta e="T95" id="Seg_1315" s="T94">Vogel.[NOM]</ta>
            <ta e="T96" id="Seg_1316" s="T95">MOD</ta>
            <ta e="T97" id="Seg_1317" s="T96">sagen-CVB.SEQ</ta>
            <ta e="T98" id="Seg_1318" s="T97">dieses</ta>
            <ta e="T99" id="Seg_1319" s="T98">Schamane-Schwan-ACC</ta>
            <ta e="T100" id="Seg_1320" s="T99">schießen-CVB.SEQ</ta>
            <ta e="T101" id="Seg_1321" s="T100">werfen-PRS.[3SG]</ta>
            <ta e="T102" id="Seg_1322" s="T101">schießen-CVB.SEQ</ta>
            <ta e="T103" id="Seg_1323" s="T102">nachdem</ta>
            <ta e="T104" id="Seg_1324" s="T103">nehmen-PRS-3PL</ta>
            <ta e="T105" id="Seg_1325" s="T104">nehmen-PRS-3PL</ta>
            <ta e="T106" id="Seg_1326" s="T105">und</ta>
            <ta e="T107" id="Seg_1327" s="T106">Haut.abziehen-PRS-3PL</ta>
            <ta e="T108" id="Seg_1328" s="T107">sehen-PST2-3PL</ta>
            <ta e="T109" id="Seg_1329" s="T108">Gürtel-PROPR</ta>
            <ta e="T110" id="Seg_1330" s="T109">Schamane.[NOM]</ta>
            <ta e="T111" id="Seg_1331" s="T110">Kleidung-PROPR</ta>
            <ta e="T112" id="Seg_1332" s="T111">Mensch.[NOM]</ta>
            <ta e="T113" id="Seg_1333" s="T112">was-ACC</ta>
            <ta e="T114" id="Seg_1334" s="T113">machen-FUT-3PL=Q</ta>
            <ta e="T115" id="Seg_1335" s="T114">werfen-CVB.SEQ</ta>
            <ta e="T116" id="Seg_1336" s="T115">werfen-PST2-3PL</ta>
            <ta e="T117" id="Seg_1337" s="T116">Freund-3SG.[NOM]</ta>
            <ta e="T118" id="Seg_1338" s="T117">kommen-PRS.[3SG]</ta>
            <ta e="T119" id="Seg_1339" s="T118">sprechen-PTCP.PST</ta>
            <ta e="T120" id="Seg_1340" s="T119">Zeit-3SG-DAT/LOC</ta>
            <ta e="T121" id="Seg_1341" s="T120">dieses</ta>
            <ta e="T122" id="Seg_1342" s="T121">weiß</ta>
            <ta e="T123" id="Seg_1343" s="T122">Kalb.[NOM]</ta>
            <ta e="T124" id="Seg_1344" s="T123">Fleisch-PROPR</ta>
            <ta e="T125" id="Seg_1345" s="T124">Stangenzelt-DAT/LOC</ta>
            <ta e="T126" id="Seg_1346" s="T125">doch</ta>
            <ta e="T127" id="Seg_1347" s="T126">Schwan-3SG-GEN</ta>
            <ta e="T128" id="Seg_1348" s="T127">Gestalt-3SG-ACC</ta>
            <ta e="T129" id="Seg_1349" s="T128">ändern-CVB.SEQ</ta>
            <ta e="T130" id="Seg_1350" s="T129">Mensch.[NOM]</ta>
            <ta e="T131" id="Seg_1351" s="T130">werden-CVB.SEQ</ta>
            <ta e="T132" id="Seg_1352" s="T131">bleiben-PRS.[3SG]</ta>
            <ta e="T133" id="Seg_1353" s="T132">Freund-3SG.[NOM]</ta>
            <ta e="T134" id="Seg_1354" s="T133">sterben-PTCP.PST-3SG-ACC</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_1355" s="T0">давно</ta>
            <ta e="T2" id="Seg_1356" s="T1">два</ta>
            <ta e="T3" id="Seg_1357" s="T2">шаман.[NOM]</ta>
            <ta e="T4" id="Seg_1358" s="T3">камлать-CVB.SIM</ta>
            <ta e="T5" id="Seg_1359" s="T4">стоять-CVB.SEQ-3PL</ta>
            <ta e="T6" id="Seg_1360" s="T5">говорить-PST2-3PL</ta>
            <ta e="T7" id="Seg_1361" s="T6">1PL.[NOM]</ta>
            <ta e="T8" id="Seg_1362" s="T7">лебедь.[NOM]</ta>
            <ta e="T9" id="Seg_1363" s="T8">становиться-CVB.SEQ</ta>
            <ta e="T10" id="Seg_1364" s="T9">птица.[NOM]</ta>
            <ta e="T11" id="Seg_1365" s="T10">идти-PTCP.PRS</ta>
            <ta e="T12" id="Seg_1366" s="T11">место-3SG-DAT/LOC</ta>
            <ta e="T13" id="Seg_1367" s="T12">летать-FUT-1PL</ta>
            <ta e="T14" id="Seg_1368" s="T13">теперь</ta>
            <ta e="T15" id="Seg_1369" s="T14">весной</ta>
            <ta e="T16" id="Seg_1370" s="T15">девять</ta>
            <ta e="T17" id="Seg_1371" s="T16">лебедь.[NOM]</ta>
            <ta e="T18" id="Seg_1372" s="T17">быть-CVB.SEQ</ta>
            <ta e="T19" id="Seg_1373" s="T18">дом-2PL-ACC</ta>
            <ta e="T20" id="Seg_1374" s="T19">три-MLTP</ta>
            <ta e="T21" id="Seg_1375" s="T20">вертеться-FUT-1PL</ta>
            <ta e="T22" id="Seg_1376" s="T21">этот</ta>
            <ta e="T23" id="Seg_1377" s="T22">время-DAT/LOC</ta>
            <ta e="T24" id="Seg_1378" s="T23">1PL-ACC</ta>
            <ta e="T25" id="Seg_1379" s="T24">принимать-FUT-EP-IMP.2PL</ta>
            <ta e="T26" id="Seg_1380" s="T25">девять</ta>
            <ta e="T27" id="Seg_1381" s="T26">белый</ta>
            <ta e="T28" id="Seg_1382" s="T27">олененок-ACC</ta>
            <ta e="T29" id="Seg_1383" s="T28">убить-CVB.SEQ-2PL</ta>
            <ta e="T30" id="Seg_1384" s="T29">этот-ACC</ta>
            <ta e="T31" id="Seg_1385" s="T30">мясо-3SG-ACC</ta>
            <ta e="T32" id="Seg_1386" s="T31">люди-ABL</ta>
            <ta e="T33" id="Seg_1387" s="T32">далеко</ta>
            <ta e="T34" id="Seg_1388" s="T33">ставить-PTCP.PST</ta>
            <ta e="T35" id="Seg_1389" s="T34">новый</ta>
            <ta e="T36" id="Seg_1390" s="T35">кожаная.покрышка.чума-PROPR</ta>
            <ta e="T37" id="Seg_1391" s="T36">чум.[NOM]</ta>
            <ta e="T38" id="Seg_1392" s="T37">нутро-3SG-DAT/LOC</ta>
            <ta e="T39" id="Seg_1393" s="T38">класть-CVB.SEQ-2PL</ta>
            <ta e="T40" id="Seg_1394" s="T39">лебедь.[NOM]</ta>
            <ta e="T41" id="Seg_1395" s="T40">становиться-PST2-3PL</ta>
            <ta e="T42" id="Seg_1396" s="T41">да</ta>
            <ta e="T43" id="Seg_1397" s="T42">летать-PST2-3PL</ta>
            <ta e="T44" id="Seg_1398" s="T43">EMPH</ta>
            <ta e="T45" id="Seg_1399" s="T44">гусь.[NOM]-лебедь.[NOM]</ta>
            <ta e="T46" id="Seg_1400" s="T45">линять-PTCP.PRS</ta>
            <ta e="T47" id="Seg_1401" s="T46">место-3SG-DAT/LOC</ta>
            <ta e="T48" id="Seg_1402" s="T47">доезжать-PST2-3PL</ta>
            <ta e="T49" id="Seg_1403" s="T48">этот</ta>
            <ta e="T50" id="Seg_1404" s="T49">шаман-PL.[NOM]</ta>
            <ta e="T51" id="Seg_1405" s="T50">вот</ta>
            <ta e="T52" id="Seg_1406" s="T51">линять-IMP.1DU</ta>
            <ta e="T53" id="Seg_1407" s="T52">говорить-PST2.[3SG]</ta>
            <ta e="T54" id="Seg_1408" s="T53">один.из.двух-3SG.[NOM]</ta>
            <ta e="T55" id="Seg_1409" s="T54">река-DAT/LOC</ta>
            <ta e="T56" id="Seg_1410" s="T55">линять-IMP.1DU</ta>
            <ta e="T57" id="Seg_1411" s="T56">говорить-PST2.[3SG]</ta>
            <ta e="T58" id="Seg_1412" s="T57">народ.[NOM]</ta>
            <ta e="T59" id="Seg_1413" s="T58">озеро-DAT/LOC</ta>
            <ta e="T60" id="Seg_1414" s="T59">убить-FUT-3PL</ta>
            <ta e="T61" id="Seg_1415" s="T60">друг-3SG.[NOM]</ta>
            <ta e="T62" id="Seg_1416" s="T61">слышать-EP-PST2.NEG.[3SG]</ta>
            <ta e="T63" id="Seg_1417" s="T62">озеро-DAT/LOC</ta>
            <ta e="T64" id="Seg_1418" s="T63">линять-CVB.SIM</ta>
            <ta e="T65" id="Seg_1419" s="T64">оставаться-PST2.[3SG]</ta>
            <ta e="T66" id="Seg_1420" s="T65">этот</ta>
            <ta e="T67" id="Seg_1421" s="T66">оставаться-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T68" id="Seg_1422" s="T67">как</ta>
            <ta e="T69" id="Seg_1423" s="T68">быть-FUT.[3SG]=Q</ta>
            <ta e="T70" id="Seg_1424" s="T69">однажды</ta>
            <ta e="T71" id="Seg_1425" s="T70">голос-3SG.[NOM]</ta>
            <ta e="T72" id="Seg_1426" s="T71">бормотанье.[NOM]</ta>
            <ta e="T73" id="Seg_1427" s="T72">становиться-PST2.[3SG]</ta>
            <ta e="T74" id="Seg_1428" s="T73">этот-ACC</ta>
            <ta e="T75" id="Seg_1429" s="T74">друг-3SG.[NOM]</ta>
            <ta e="T76" id="Seg_1430" s="T75">слышать-EP-PST2.[3SG]</ta>
            <ta e="T77" id="Seg_1431" s="T76">два</ta>
            <ta e="T78" id="Seg_1432" s="T77">человек.[NOM]</ta>
            <ta e="T79" id="Seg_1433" s="T78">найти-PST2-3PL</ta>
            <ta e="T80" id="Seg_1434" s="T79">этот</ta>
            <ta e="T81" id="Seg_1435" s="T80">человек-PL-ABL</ta>
            <ta e="T82" id="Seg_1436" s="T81">один-3PL.[NOM]</ta>
            <ta e="T83" id="Seg_1437" s="T82">говорить-PRS.[3SG]</ta>
            <ta e="T84" id="Seg_1438" s="T83">что.[NOM]</ta>
            <ta e="T85" id="Seg_1439" s="T84">чудо-3SG.[NOM]=Q</ta>
            <ta e="T86" id="Seg_1440" s="T85">друг.[NOM]</ta>
            <ta e="T87" id="Seg_1441" s="T86">человек-SIM</ta>
            <ta e="T88" id="Seg_1442" s="T87">кричать-PTCP.PRS.[NOM]</ta>
            <ta e="T89" id="Seg_1443" s="T88">как</ta>
            <ta e="T90" id="Seg_1444" s="T89">тот-ACC</ta>
            <ta e="T91" id="Seg_1445" s="T90">друг-3SG.[NOM]</ta>
            <ta e="T92" id="Seg_1446" s="T91">говорить-PRS.[3SG]</ta>
            <ta e="T93" id="Seg_1447" s="T92">что.[NOM]</ta>
            <ta e="T94" id="Seg_1448" s="T93">быть-FUT.[3SG]=Q</ta>
            <ta e="T95" id="Seg_1449" s="T94">птица.[NOM]</ta>
            <ta e="T96" id="Seg_1450" s="T95">MOD</ta>
            <ta e="T97" id="Seg_1451" s="T96">говорить-CVB.SEQ</ta>
            <ta e="T98" id="Seg_1452" s="T97">этот</ta>
            <ta e="T99" id="Seg_1453" s="T98">шаман-лебедь-ACC</ta>
            <ta e="T100" id="Seg_1454" s="T99">стрелять-CVB.SEQ</ta>
            <ta e="T101" id="Seg_1455" s="T100">бросать-PRS.[3SG]</ta>
            <ta e="T102" id="Seg_1456" s="T101">стрелять-CVB.SEQ</ta>
            <ta e="T103" id="Seg_1457" s="T102">после</ta>
            <ta e="T104" id="Seg_1458" s="T103">взять-PRS-3PL</ta>
            <ta e="T105" id="Seg_1459" s="T104">взять-PRS-3PL</ta>
            <ta e="T106" id="Seg_1460" s="T105">да</ta>
            <ta e="T107" id="Seg_1461" s="T106">сдирать.кожу-PRS-3PL</ta>
            <ta e="T108" id="Seg_1462" s="T107">видеть-PST2-3PL</ta>
            <ta e="T109" id="Seg_1463" s="T108">пояс-PROPR</ta>
            <ta e="T110" id="Seg_1464" s="T109">шаман.[NOM]</ta>
            <ta e="T111" id="Seg_1465" s="T110">одежда-PROPR</ta>
            <ta e="T112" id="Seg_1466" s="T111">человек.[NOM]</ta>
            <ta e="T113" id="Seg_1467" s="T112">что-ACC</ta>
            <ta e="T114" id="Seg_1468" s="T113">делать-FUT-3PL=Q</ta>
            <ta e="T115" id="Seg_1469" s="T114">бросать-CVB.SEQ</ta>
            <ta e="T116" id="Seg_1470" s="T115">бросать-PST2-3PL</ta>
            <ta e="T117" id="Seg_1471" s="T116">друг-3SG.[NOM]</ta>
            <ta e="T118" id="Seg_1472" s="T117">приходить-PRS.[3SG]</ta>
            <ta e="T119" id="Seg_1473" s="T118">говорить-PTCP.PST</ta>
            <ta e="T120" id="Seg_1474" s="T119">час-3SG-DAT/LOC</ta>
            <ta e="T121" id="Seg_1475" s="T120">этот</ta>
            <ta e="T122" id="Seg_1476" s="T121">белый</ta>
            <ta e="T123" id="Seg_1477" s="T122">теленок.[NOM]</ta>
            <ta e="T124" id="Seg_1478" s="T123">мясо-PROPR</ta>
            <ta e="T125" id="Seg_1479" s="T124">чум-DAT/LOC</ta>
            <ta e="T126" id="Seg_1480" s="T125">вот</ta>
            <ta e="T127" id="Seg_1481" s="T126">лебедь-3SG-GEN</ta>
            <ta e="T128" id="Seg_1482" s="T127">образ-3SG-ACC</ta>
            <ta e="T129" id="Seg_1483" s="T128">сменить-CVB.SEQ</ta>
            <ta e="T130" id="Seg_1484" s="T129">человек.[NOM]</ta>
            <ta e="T131" id="Seg_1485" s="T130">становиться-CVB.SEQ</ta>
            <ta e="T132" id="Seg_1486" s="T131">оставаться-PRS.[3SG]</ta>
            <ta e="T133" id="Seg_1487" s="T132">друг-3SG.[NOM]</ta>
            <ta e="T134" id="Seg_1488" s="T133">умирать-PTCP.PST-3SG-ACC</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_1489" s="T0">adv</ta>
            <ta e="T2" id="Seg_1490" s="T1">cardnum</ta>
            <ta e="T3" id="Seg_1491" s="T2">n-n:case</ta>
            <ta e="T4" id="Seg_1492" s="T3">v-v:cvb</ta>
            <ta e="T5" id="Seg_1493" s="T4">v-v:cvb-v:pred.pn</ta>
            <ta e="T6" id="Seg_1494" s="T5">v-v:tense-v:pred.pn</ta>
            <ta e="T7" id="Seg_1495" s="T6">pers-pro:case</ta>
            <ta e="T8" id="Seg_1496" s="T7">n-n:case</ta>
            <ta e="T9" id="Seg_1497" s="T8">v-v:cvb</ta>
            <ta e="T10" id="Seg_1498" s="T9">n-n:case</ta>
            <ta e="T11" id="Seg_1499" s="T10">v-v:ptcp</ta>
            <ta e="T12" id="Seg_1500" s="T11">n-n:poss-n:case</ta>
            <ta e="T13" id="Seg_1501" s="T12">v-v:tense-v:poss.pn</ta>
            <ta e="T14" id="Seg_1502" s="T13">adv</ta>
            <ta e="T15" id="Seg_1503" s="T14">adv</ta>
            <ta e="T16" id="Seg_1504" s="T15">cardnum</ta>
            <ta e="T17" id="Seg_1505" s="T16">n-n:case</ta>
            <ta e="T18" id="Seg_1506" s="T17">v-v:cvb</ta>
            <ta e="T19" id="Seg_1507" s="T18">n-n:poss-n:case</ta>
            <ta e="T20" id="Seg_1508" s="T19">cardnum-cardnum&gt;adv</ta>
            <ta e="T21" id="Seg_1509" s="T20">v-v:tense-v:poss.pn</ta>
            <ta e="T22" id="Seg_1510" s="T21">dempro</ta>
            <ta e="T23" id="Seg_1511" s="T22">n-n:case</ta>
            <ta e="T24" id="Seg_1512" s="T23">pers-pro:case</ta>
            <ta e="T25" id="Seg_1513" s="T24">v-v:(tense)-v:(ins)-v:mood.pn</ta>
            <ta e="T26" id="Seg_1514" s="T25">cardnum</ta>
            <ta e="T27" id="Seg_1515" s="T26">adj</ta>
            <ta e="T28" id="Seg_1516" s="T27">n-n:case</ta>
            <ta e="T29" id="Seg_1517" s="T28">v-v:cvb-v:pred.pn</ta>
            <ta e="T30" id="Seg_1518" s="T29">dempro-pro:case</ta>
            <ta e="T31" id="Seg_1519" s="T30">n-n:poss-n:case</ta>
            <ta e="T32" id="Seg_1520" s="T31">n-n:case</ta>
            <ta e="T33" id="Seg_1521" s="T32">adv</ta>
            <ta e="T34" id="Seg_1522" s="T33">v-v:ptcp</ta>
            <ta e="T35" id="Seg_1523" s="T34">adj</ta>
            <ta e="T36" id="Seg_1524" s="T35">n-n&gt;adj</ta>
            <ta e="T37" id="Seg_1525" s="T36">n-n:case</ta>
            <ta e="T38" id="Seg_1526" s="T37">n-n:poss-n:case</ta>
            <ta e="T39" id="Seg_1527" s="T38">v-v:cvb-v:pred.pn</ta>
            <ta e="T40" id="Seg_1528" s="T39">n-n:case</ta>
            <ta e="T41" id="Seg_1529" s="T40">v-v:tense-v:pred.pn</ta>
            <ta e="T42" id="Seg_1530" s="T41">conj</ta>
            <ta e="T43" id="Seg_1531" s="T42">v-v:tense-v:poss.pn</ta>
            <ta e="T44" id="Seg_1532" s="T43">ptcl</ta>
            <ta e="T45" id="Seg_1533" s="T44">n-n:case-n-n:case</ta>
            <ta e="T46" id="Seg_1534" s="T45">v-v:ptcp</ta>
            <ta e="T47" id="Seg_1535" s="T46">n-n:poss-n:case</ta>
            <ta e="T48" id="Seg_1536" s="T47">v-v:tense-v:pred.pn</ta>
            <ta e="T49" id="Seg_1537" s="T48">dempro</ta>
            <ta e="T50" id="Seg_1538" s="T49">n-n:(num)-n:case</ta>
            <ta e="T51" id="Seg_1539" s="T50">ptcl</ta>
            <ta e="T52" id="Seg_1540" s="T51">v-v:mood.pn</ta>
            <ta e="T53" id="Seg_1541" s="T52">v-v:tense-v:pred.pn</ta>
            <ta e="T54" id="Seg_1542" s="T53">adj-n:(poss)-n:case</ta>
            <ta e="T55" id="Seg_1543" s="T54">n-n:case</ta>
            <ta e="T56" id="Seg_1544" s="T55">v-v:mood.pn</ta>
            <ta e="T57" id="Seg_1545" s="T56">v-v:tense-v:pred.pn</ta>
            <ta e="T58" id="Seg_1546" s="T57">n-n:case</ta>
            <ta e="T59" id="Seg_1547" s="T58">n-n:case</ta>
            <ta e="T60" id="Seg_1548" s="T59">v-v:tense-v:poss.pn</ta>
            <ta e="T61" id="Seg_1549" s="T60">n-n:(poss)-n:case</ta>
            <ta e="T62" id="Seg_1550" s="T61">v-v:(ins)-v:neg-v:pred.pn</ta>
            <ta e="T63" id="Seg_1551" s="T62">n-n:case</ta>
            <ta e="T64" id="Seg_1552" s="T63">v-v:cvb</ta>
            <ta e="T65" id="Seg_1553" s="T64">v-v:tense-v:pred.pn</ta>
            <ta e="T66" id="Seg_1554" s="T65">dempro</ta>
            <ta e="T67" id="Seg_1555" s="T66">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T68" id="Seg_1556" s="T67">que</ta>
            <ta e="T69" id="Seg_1557" s="T68">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T70" id="Seg_1558" s="T69">adv</ta>
            <ta e="T71" id="Seg_1559" s="T70">n-n:(poss)-n:case</ta>
            <ta e="T72" id="Seg_1560" s="T71">n-n:case</ta>
            <ta e="T73" id="Seg_1561" s="T72">v-v:tense-v:pred.pn</ta>
            <ta e="T74" id="Seg_1562" s="T73">dempro-pro:case</ta>
            <ta e="T75" id="Seg_1563" s="T74">n-n:(poss)-n:case</ta>
            <ta e="T76" id="Seg_1564" s="T75">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T77" id="Seg_1565" s="T76">cardnum</ta>
            <ta e="T78" id="Seg_1566" s="T77">n-n:case</ta>
            <ta e="T79" id="Seg_1567" s="T78">v-v:tense-v:pred.pn</ta>
            <ta e="T80" id="Seg_1568" s="T79">dempro</ta>
            <ta e="T81" id="Seg_1569" s="T80">n-n:(num)-n:case</ta>
            <ta e="T82" id="Seg_1570" s="T81">cardnum-n:(poss)-n:case</ta>
            <ta e="T83" id="Seg_1571" s="T82">v-v:tense-v:pred.pn</ta>
            <ta e="T84" id="Seg_1572" s="T83">que-pro:case</ta>
            <ta e="T85" id="Seg_1573" s="T84">n-n:(poss)-n:case-ptcl</ta>
            <ta e="T86" id="Seg_1574" s="T85">n-n:case</ta>
            <ta e="T87" id="Seg_1575" s="T86">n-n&gt;adv</ta>
            <ta e="T88" id="Seg_1576" s="T87">v-v:ptcp-v:(case)</ta>
            <ta e="T89" id="Seg_1577" s="T88">post</ta>
            <ta e="T90" id="Seg_1578" s="T89">dempro-pro:case</ta>
            <ta e="T91" id="Seg_1579" s="T90">n-n:(poss)-n:case</ta>
            <ta e="T92" id="Seg_1580" s="T91">v-v:tense-v:pred.pn</ta>
            <ta e="T93" id="Seg_1581" s="T92">que-pro:case</ta>
            <ta e="T94" id="Seg_1582" s="T93">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T95" id="Seg_1583" s="T94">n-n:case</ta>
            <ta e="T96" id="Seg_1584" s="T95">ptcl</ta>
            <ta e="T97" id="Seg_1585" s="T96">v-v:cvb</ta>
            <ta e="T98" id="Seg_1586" s="T97">dempro</ta>
            <ta e="T99" id="Seg_1587" s="T98">n-n-n:case</ta>
            <ta e="T100" id="Seg_1588" s="T99">v-v:cvb</ta>
            <ta e="T101" id="Seg_1589" s="T100">v-v:tense-v:pred.pn</ta>
            <ta e="T102" id="Seg_1590" s="T101">v-v:cvb</ta>
            <ta e="T103" id="Seg_1591" s="T102">post</ta>
            <ta e="T104" id="Seg_1592" s="T103">v-v:tense-v:pred.pn</ta>
            <ta e="T105" id="Seg_1593" s="T104">v-v:tense-v:pred.pn</ta>
            <ta e="T106" id="Seg_1594" s="T105">conj</ta>
            <ta e="T107" id="Seg_1595" s="T106">v-v:tense-v:pred.pn</ta>
            <ta e="T108" id="Seg_1596" s="T107">v-v:tense-v:poss.pn</ta>
            <ta e="T109" id="Seg_1597" s="T108">n-n&gt;adj</ta>
            <ta e="T110" id="Seg_1598" s="T109">n-n:case</ta>
            <ta e="T111" id="Seg_1599" s="T110">n-n&gt;adj</ta>
            <ta e="T112" id="Seg_1600" s="T111">n-n:case</ta>
            <ta e="T113" id="Seg_1601" s="T112">que-pro:case</ta>
            <ta e="T114" id="Seg_1602" s="T113">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T115" id="Seg_1603" s="T114">v-v:cvb</ta>
            <ta e="T116" id="Seg_1604" s="T115">v-v:tense-v:pred.pn</ta>
            <ta e="T117" id="Seg_1605" s="T116">n-n:(poss)-n:case</ta>
            <ta e="T118" id="Seg_1606" s="T117">v-v:tense-v:pred.pn</ta>
            <ta e="T119" id="Seg_1607" s="T118">v-v:ptcp</ta>
            <ta e="T120" id="Seg_1608" s="T119">n-n:poss-n:case</ta>
            <ta e="T121" id="Seg_1609" s="T120">dempro</ta>
            <ta e="T122" id="Seg_1610" s="T121">adj</ta>
            <ta e="T123" id="Seg_1611" s="T122">n-n:case</ta>
            <ta e="T124" id="Seg_1612" s="T123">n-n&gt;adj</ta>
            <ta e="T125" id="Seg_1613" s="T124">n-n:case</ta>
            <ta e="T126" id="Seg_1614" s="T125">ptcl</ta>
            <ta e="T127" id="Seg_1615" s="T126">n-n:poss-n:case</ta>
            <ta e="T128" id="Seg_1616" s="T127">n-n:poss-n:case</ta>
            <ta e="T129" id="Seg_1617" s="T128">v-v:cvb</ta>
            <ta e="T130" id="Seg_1618" s="T129">n-n:case</ta>
            <ta e="T131" id="Seg_1619" s="T130">v-v:cvb</ta>
            <ta e="T132" id="Seg_1620" s="T131">v-v:tense-v:pred.pn</ta>
            <ta e="T133" id="Seg_1621" s="T132">n-n:(poss)-n:case</ta>
            <ta e="T134" id="Seg_1622" s="T133">v-v:ptcp-v:(poss)-v:(case)</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_1623" s="T0">adv</ta>
            <ta e="T2" id="Seg_1624" s="T1">cardnum</ta>
            <ta e="T3" id="Seg_1625" s="T2">n</ta>
            <ta e="T4" id="Seg_1626" s="T3">v</ta>
            <ta e="T5" id="Seg_1627" s="T4">aux</ta>
            <ta e="T6" id="Seg_1628" s="T5">v</ta>
            <ta e="T7" id="Seg_1629" s="T6">pers</ta>
            <ta e="T8" id="Seg_1630" s="T7">n</ta>
            <ta e="T9" id="Seg_1631" s="T8">cop</ta>
            <ta e="T10" id="Seg_1632" s="T9">n</ta>
            <ta e="T11" id="Seg_1633" s="T10">v</ta>
            <ta e="T12" id="Seg_1634" s="T11">n</ta>
            <ta e="T13" id="Seg_1635" s="T12">v</ta>
            <ta e="T14" id="Seg_1636" s="T13">adv</ta>
            <ta e="T15" id="Seg_1637" s="T14">adv</ta>
            <ta e="T16" id="Seg_1638" s="T15">cardnum</ta>
            <ta e="T17" id="Seg_1639" s="T16">n</ta>
            <ta e="T18" id="Seg_1640" s="T17">cop</ta>
            <ta e="T19" id="Seg_1641" s="T18">n</ta>
            <ta e="T20" id="Seg_1642" s="T19">adv</ta>
            <ta e="T21" id="Seg_1643" s="T20">v</ta>
            <ta e="T22" id="Seg_1644" s="T21">dempro</ta>
            <ta e="T23" id="Seg_1645" s="T22">n</ta>
            <ta e="T24" id="Seg_1646" s="T23">pers</ta>
            <ta e="T25" id="Seg_1647" s="T24">v</ta>
            <ta e="T26" id="Seg_1648" s="T25">cardnum</ta>
            <ta e="T27" id="Seg_1649" s="T26">adj</ta>
            <ta e="T28" id="Seg_1650" s="T27">n</ta>
            <ta e="T29" id="Seg_1651" s="T28">v</ta>
            <ta e="T30" id="Seg_1652" s="T29">dempro</ta>
            <ta e="T31" id="Seg_1653" s="T30">n</ta>
            <ta e="T32" id="Seg_1654" s="T31">n</ta>
            <ta e="T33" id="Seg_1655" s="T32">adv</ta>
            <ta e="T34" id="Seg_1656" s="T33">v</ta>
            <ta e="T35" id="Seg_1657" s="T34">adj</ta>
            <ta e="T36" id="Seg_1658" s="T35">adj</ta>
            <ta e="T37" id="Seg_1659" s="T36">n</ta>
            <ta e="T38" id="Seg_1660" s="T37">n</ta>
            <ta e="T39" id="Seg_1661" s="T38">v</ta>
            <ta e="T40" id="Seg_1662" s="T39">n</ta>
            <ta e="T41" id="Seg_1663" s="T40">cop</ta>
            <ta e="T42" id="Seg_1664" s="T41">conj</ta>
            <ta e="T43" id="Seg_1665" s="T42">v</ta>
            <ta e="T44" id="Seg_1666" s="T43">ptcl</ta>
            <ta e="T45" id="Seg_1667" s="T44">n</ta>
            <ta e="T46" id="Seg_1668" s="T45">v</ta>
            <ta e="T47" id="Seg_1669" s="T46">n</ta>
            <ta e="T48" id="Seg_1670" s="T47">v</ta>
            <ta e="T49" id="Seg_1671" s="T48">dempro</ta>
            <ta e="T50" id="Seg_1672" s="T49">n</ta>
            <ta e="T51" id="Seg_1673" s="T50">ptcl</ta>
            <ta e="T52" id="Seg_1674" s="T51">v</ta>
            <ta e="T53" id="Seg_1675" s="T52">v</ta>
            <ta e="T54" id="Seg_1676" s="T53">adj</ta>
            <ta e="T55" id="Seg_1677" s="T54">n</ta>
            <ta e="T56" id="Seg_1678" s="T55">v</ta>
            <ta e="T57" id="Seg_1679" s="T56">v</ta>
            <ta e="T58" id="Seg_1680" s="T57">n</ta>
            <ta e="T59" id="Seg_1681" s="T58">n</ta>
            <ta e="T60" id="Seg_1682" s="T59">v</ta>
            <ta e="T61" id="Seg_1683" s="T60">n</ta>
            <ta e="T62" id="Seg_1684" s="T61">v</ta>
            <ta e="T63" id="Seg_1685" s="T62">n</ta>
            <ta e="T64" id="Seg_1686" s="T63">v</ta>
            <ta e="T65" id="Seg_1687" s="T64">aux</ta>
            <ta e="T66" id="Seg_1688" s="T65">dempro</ta>
            <ta e="T67" id="Seg_1689" s="T66">n</ta>
            <ta e="T68" id="Seg_1690" s="T67">que</ta>
            <ta e="T69" id="Seg_1691" s="T68">cop</ta>
            <ta e="T70" id="Seg_1692" s="T69">adv</ta>
            <ta e="T71" id="Seg_1693" s="T70">n</ta>
            <ta e="T72" id="Seg_1694" s="T71">n</ta>
            <ta e="T73" id="Seg_1695" s="T72">cop</ta>
            <ta e="T74" id="Seg_1696" s="T73">dempro</ta>
            <ta e="T75" id="Seg_1697" s="T74">n</ta>
            <ta e="T76" id="Seg_1698" s="T75">v</ta>
            <ta e="T77" id="Seg_1699" s="T76">cardnum</ta>
            <ta e="T78" id="Seg_1700" s="T77">n</ta>
            <ta e="T79" id="Seg_1701" s="T78">v</ta>
            <ta e="T80" id="Seg_1702" s="T79">dempro</ta>
            <ta e="T81" id="Seg_1703" s="T80">n</ta>
            <ta e="T82" id="Seg_1704" s="T81">cardnum</ta>
            <ta e="T83" id="Seg_1705" s="T82">v</ta>
            <ta e="T84" id="Seg_1706" s="T83">que</ta>
            <ta e="T85" id="Seg_1707" s="T84">n</ta>
            <ta e="T86" id="Seg_1708" s="T85">n</ta>
            <ta e="T87" id="Seg_1709" s="T86">adv</ta>
            <ta e="T88" id="Seg_1710" s="T87">v</ta>
            <ta e="T89" id="Seg_1711" s="T88">post</ta>
            <ta e="T90" id="Seg_1712" s="T89">dempro</ta>
            <ta e="T91" id="Seg_1713" s="T90">n</ta>
            <ta e="T92" id="Seg_1714" s="T91">v</ta>
            <ta e="T93" id="Seg_1715" s="T92">que</ta>
            <ta e="T94" id="Seg_1716" s="T93">cop</ta>
            <ta e="T95" id="Seg_1717" s="T94">n</ta>
            <ta e="T96" id="Seg_1718" s="T95">ptcl</ta>
            <ta e="T97" id="Seg_1719" s="T96">v</ta>
            <ta e="T98" id="Seg_1720" s="T97">dempro</ta>
            <ta e="T99" id="Seg_1721" s="T98">n</ta>
            <ta e="T100" id="Seg_1722" s="T99">v</ta>
            <ta e="T101" id="Seg_1723" s="T100">aux</ta>
            <ta e="T102" id="Seg_1724" s="T101">v</ta>
            <ta e="T103" id="Seg_1725" s="T102">post</ta>
            <ta e="T104" id="Seg_1726" s="T103">v</ta>
            <ta e="T105" id="Seg_1727" s="T104">v</ta>
            <ta e="T106" id="Seg_1728" s="T105">conj</ta>
            <ta e="T107" id="Seg_1729" s="T106">v</ta>
            <ta e="T108" id="Seg_1730" s="T107">v</ta>
            <ta e="T109" id="Seg_1731" s="T108">adj</ta>
            <ta e="T110" id="Seg_1732" s="T109">n</ta>
            <ta e="T111" id="Seg_1733" s="T110">adj</ta>
            <ta e="T112" id="Seg_1734" s="T111">n</ta>
            <ta e="T113" id="Seg_1735" s="T112">que</ta>
            <ta e="T114" id="Seg_1736" s="T113">v</ta>
            <ta e="T115" id="Seg_1737" s="T114">v</ta>
            <ta e="T116" id="Seg_1738" s="T115">aux</ta>
            <ta e="T117" id="Seg_1739" s="T116">n</ta>
            <ta e="T118" id="Seg_1740" s="T117">v</ta>
            <ta e="T119" id="Seg_1741" s="T118">v</ta>
            <ta e="T120" id="Seg_1742" s="T119">n</ta>
            <ta e="T121" id="Seg_1743" s="T120">dempro</ta>
            <ta e="T122" id="Seg_1744" s="T121">adj</ta>
            <ta e="T123" id="Seg_1745" s="T122">n</ta>
            <ta e="T124" id="Seg_1746" s="T123">adj</ta>
            <ta e="T125" id="Seg_1747" s="T124">n</ta>
            <ta e="T126" id="Seg_1748" s="T125">ptcl</ta>
            <ta e="T127" id="Seg_1749" s="T126">n</ta>
            <ta e="T128" id="Seg_1750" s="T127">n</ta>
            <ta e="T129" id="Seg_1751" s="T128">v</ta>
            <ta e="T130" id="Seg_1752" s="T129">n</ta>
            <ta e="T131" id="Seg_1753" s="T130">cop</ta>
            <ta e="T132" id="Seg_1754" s="T131">aux</ta>
            <ta e="T133" id="Seg_1755" s="T132">n</ta>
            <ta e="T134" id="Seg_1756" s="T133">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR" />
         <annotation name="SyF" tierref="SyF" />
         <annotation name="IST" tierref="IST" />
         <annotation name="Top" tierref="Top" />
         <annotation name="Foc" tierref="Foc" />
         <annotation name="BOR" tierref="BOR" />
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T6" id="Seg_1757" s="T0">Long ago two shamans were shamanizing, they said:</ta>
            <ta e="T13" id="Seg_1758" s="T6">"We will turn into swans and fly there where the birds fly to.</ta>
            <ta e="T39" id="Seg_1759" s="T13">Next spring we will fly in the shape of nine swans around your settlement three times, receive us at that time, kill nine white reindeer calves and put their meat into new pole tents with leather cover, that stands far away from the people's dwellings."</ta>
            <ta e="T44" id="Seg_1760" s="T39">They turned into swans and flew away.</ta>
            <ta e="T50" id="Seg_1761" s="T44">Those shamans reached the place where geese and swans are moulting.</ta>
            <ta e="T54" id="Seg_1762" s="T50">"Let us moult here", one of them said.</ta>
            <ta e="T57" id="Seg_1763" s="T54">"Let us moult at the river", he said.</ta>
            <ta e="T60" id="Seg_1764" s="T57">"At the lake people will kill us".</ta>
            <ta e="T65" id="Seg_1765" s="T60">His friend didn't listen to him and stayed at the lake for moulting.</ta>
            <ta e="T76" id="Seg_1766" s="T65">What happened with the one who stayed? Once his scream was heard, his friend heard this.</ta>
            <ta e="T79" id="Seg_1767" s="T76">Two humans found him.</ta>
            <ta e="T83" id="Seg_1768" s="T79">One of those humans said:</ta>
            <ta e="T89" id="Seg_1769" s="T83">"What a miracle, my friend, it shouts like a human."</ta>
            <ta e="T92" id="Seg_1770" s="T89">His friend said on that:</ta>
            <ta e="T101" id="Seg_1771" s="T92">"So what, it is still a bird", he said and shot that shaman-swan.</ta>
            <ta e="T112" id="Seg_1772" s="T101">He shot him, they took him and skinned him, they saw that it was a human in shaman clothing with a belt.</ta>
            <ta e="T116" id="Seg_1773" s="T112">What should they do, they threw him away.</ta>
            <ta e="T125" id="Seg_1774" s="T116">His friend came at the named time to the pole tent with the meat of the white calves.</ta>
            <ta e="T134" id="Seg_1775" s="T125">There he left his swan shape and turned back into a human, but his friend had died.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T6" id="Seg_1776" s="T0">Vor langer Zeit schamanisierten zwei Schamanen und sagten:</ta>
            <ta e="T13" id="Seg_1777" s="T6">"Wir verwandeln uns in Schwäne und fliegen dorthin, wohin die Vögel fliegen.</ta>
            <ta e="T39" id="Seg_1778" s="T13">Im nächsten Frühling umfliegen wir in Gestalt von neun Schwänen drei mal eure Siedlung, empfangt uns zu der Zeit, tötet neun weiße Rentierkälber und legt ihr Fleisch in neue Stangenzelte mit Lederdecke, die weit weg von den Menschen aufgestellt sind."</ta>
            <ta e="T44" id="Seg_1779" s="T39">Sie verwandelten sich in Schwäne und flogen los.</ta>
            <ta e="T50" id="Seg_1780" s="T44">Diese Schamanen kamen dorthin, wo sich Gänse und Schwäne mausern.</ta>
            <ta e="T54" id="Seg_1781" s="T50">"Lass uns hier mausern", sagte einer von ihnen.</ta>
            <ta e="T57" id="Seg_1782" s="T54">"Lass uns am Fluss mausern", sagte er.</ta>
            <ta e="T60" id="Seg_1783" s="T57">"Am See werden uns die Leute töten."</ta>
            <ta e="T65" id="Seg_1784" s="T60">Sein Freund hörte nicht auf ihn, er blieb am See zur Mauser.</ta>
            <ta e="T76" id="Seg_1785" s="T65">Wie erging es dem Gebliebenen? Einmal ertönte sein lauter Schrei, das hörte sein Freund.</ta>
            <ta e="T79" id="Seg_1786" s="T76">Zwei Menschen fanden ihn.</ta>
            <ta e="T83" id="Seg_1787" s="T79">Einer von diesen Menschen sagte:</ta>
            <ta e="T89" id="Seg_1788" s="T83">"Was für ein Wunder, mein Freund, er schreit wie ein Mensch."</ta>
            <ta e="T92" id="Seg_1789" s="T89">Darauf sagte sein Freund:</ta>
            <ta e="T101" id="Seg_1790" s="T92">"Na und, es ist doch ein Vogel", sagte er und erschoss den Schamanenschwan.</ta>
            <ta e="T112" id="Seg_1791" s="T101">Er erschoss ihn, sie nahmen ihn und häuteten ihn, sie sahen, dass es ein Mensch in Schamanenkleidung und mit Gürtel war.</ta>
            <ta e="T116" id="Seg_1792" s="T112">Was sollten sie tun, sie warfen ihn weg.</ta>
            <ta e="T125" id="Seg_1793" s="T116">Sein Freund kam zur besprochenen Zeit in das Stangenzelt mit dem Fleisch der weißen Kälber.</ta>
            <ta e="T134" id="Seg_1794" s="T125">Da verließ er seine Schwanengestalt und wurde wieder ein Mensch, sein Freund aber war gestorben.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T6" id="Seg_1795" s="T0">В старину два шамана во время камлания сказали:</ta>
            <ta e="T13" id="Seg_1796" s="T6">"Мы, превратившись в лебедей, улетим в тот край, куда улетают [на зиму] птицы.</ta>
            <ta e="T39" id="Seg_1797" s="T13">Следующей весной в числе девяти лебедей три раза облетим ваше стойбище, в это время встречайте нас, забейте девять белых тугутов, их мясо положите в урасу с новыми покрышками, поставленную поодаль от места, где стоят жилища людей."</ta>
            <ta e="T44" id="Seg_1798" s="T39">Превратились в лебедей и улетели.</ta>
            <ta e="T50" id="Seg_1799" s="T44">Эти шаманы прилетели туда, где линяли гуси-лебеди.</ta>
            <ta e="T54" id="Seg_1800" s="T50">"Давай линять", сказал один.</ta>
            <ta e="T57" id="Seg_1801" s="T54">"На реке будем линять", сказал.</ta>
            <ta e="T60" id="Seg_1802" s="T57">"На озере люди убьют."</ta>
            <ta e="T65" id="Seg_1803" s="T60">Друг его не послушался, остался линять на озере.</ta>
            <ta e="T76" id="Seg_1804" s="T65">Как спастись оставшемуся? Однажды послышался его предсмертный крик. Это услышал друг.</ta>
            <ta e="T79" id="Seg_1805" s="T76">Два человека нашли оставшегося на озере.</ta>
            <ta e="T83" id="Seg_1806" s="T79">Один из них сказал:</ta>
            <ta e="T89" id="Seg_1807" s="T83">"Что за диво, друг, будто по-человечески кричит."</ta>
            <ta e="T92" id="Seg_1808" s="T89">На это другой:</ta>
            <ta e="T101" id="Seg_1809" s="T92">"Ну и что, птица ведь", да того лебедя-шамана и застрелил.</ta>
            <ta e="T112" id="Seg_1810" s="T101">Застрелив, взяли и стали снимать шкуру, видят: человек в шаманской одежде и с поясом.</ta>
            <ta e="T116" id="Seg_1811" s="T112">Что делать, выбросили.</ta>
            <ta e="T125" id="Seg_1812" s="T116">Друг его прилетает в назначенное время в ту урасу, где мясо белых тугутов было приготовлено.</ta>
            <ta e="T134" id="Seg_1813" s="T125">Вот лебяжий облик сменив, стал человеком, а друг его так и умер.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T6" id="Seg_1814" s="T0">В старину два шамана во время камлания сказали:</ta>
            <ta e="T13" id="Seg_1815" s="T6">— Мы, превратившись в лебедей, улетим в тот край, куда улетают [на зиму] птицы.</ta>
            <ta e="T39" id="Seg_1816" s="T13">Следующей весной в числе девяти лебедей три раза облетим ваше стойбище, в это время встречайте нас, забейте девять белых тугутов, их мясо положите в урасу с новыми покрышками, поставленную поодаль от места, где стоят жилища людей.</ta>
            <ta e="T44" id="Seg_1817" s="T39">Превратились в лебедей и улетели.</ta>
            <ta e="T50" id="Seg_1818" s="T44">Эти шаманы прилетели туда, где линяли гуси-лебеди.</ta>
            <ta e="T54" id="Seg_1819" s="T50">— Давай линять, — сказал один.</ta>
            <ta e="T57" id="Seg_1820" s="T54">— На реке будем линять, — сказал.</ta>
            <ta e="T60" id="Seg_1821" s="T57">— На озере люди убьют.</ta>
            <ta e="T65" id="Seg_1822" s="T60">Друг его не послушался, остался линять на озере.</ta>
            <ta e="T76" id="Seg_1823" s="T65">Как спастись оставшемуся? Однажды послышался его предсмертный крик. Это услышал друг.</ta>
            <ta e="T79" id="Seg_1824" s="T76">Два человека нашли оставшегося на озере.</ta>
            <ta e="T83" id="Seg_1825" s="T79">Один из них сказал:</ta>
            <ta e="T89" id="Seg_1826" s="T83">— Что за диво, друг, будто по-человечески кричит.</ta>
            <ta e="T92" id="Seg_1827" s="T89">На это другой:</ta>
            <ta e="T101" id="Seg_1828" s="T92">— Ну и что, птица ведь, — да того лебедя-шамана и застрелил.</ta>
            <ta e="T112" id="Seg_1829" s="T101">Застрелив, взяли и стали снимать шкуру, видят: человек в шаманской одежде и с поясом.</ta>
            <ta e="T116" id="Seg_1830" s="T112">Что делать, выбросили.</ta>
            <ta e="T125" id="Seg_1831" s="T116">Друг его прилетает в назначенное время в ту урасу, где мясо белых тугутов было приготовлено.</ta>
            <ta e="T134" id="Seg_1832" s="T125">Вот лебяжий облик сменив, стал человеком, а друг его так и умер.</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg"
                          name="gg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top"
                          name="Top"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc"
                          name="Foc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
