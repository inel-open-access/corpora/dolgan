<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDAB9A783A-9A48-3C3F-BC41-2B4E5BE31D42">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>YeSV_1964_OldManHares_flk</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\flk\ErSV_1964_OldManHares_flk\ErSV_1964_OldManHares_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">457</ud-information>
            <ud-information attribute-name="# HIAT:w">333</ud-information>
            <ud-information attribute-name="# e">333</ud-information>
            <ud-information attribute-name="# HIAT:u">58</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="YeSV">
            <abbreviation>ErSV</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="4.0" type="appl" />
         <tli id="T1" time="4.5" type="appl" />
         <tli id="T2" time="5.0" type="appl" />
         <tli id="T3" time="5.5" type="appl" />
         <tli id="T4" time="6.0" type="appl" />
         <tli id="T5" time="6.5" type="appl" />
         <tli id="T6" time="7.0" type="appl" />
         <tli id="T7" time="7.5" type="appl" />
         <tli id="T8" time="8.0" type="appl" />
         <tli id="T9" time="8.5" type="appl" />
         <tli id="T10" time="9.0" type="appl" />
         <tli id="T11" time="9.5" type="appl" />
         <tli id="T12" time="10.0" type="appl" />
         <tli id="T13" time="10.5" type="appl" />
         <tli id="T14" time="11.0" type="appl" />
         <tli id="T15" time="11.5" type="appl" />
         <tli id="T16" time="12.0" type="appl" />
         <tli id="T17" time="12.5" type="appl" />
         <tli id="T18" time="13.0" type="appl" />
         <tli id="T19" time="13.5" type="appl" />
         <tli id="T20" time="14.0" type="appl" />
         <tli id="T21" time="14.5" type="appl" />
         <tli id="T22" time="15.0" type="appl" />
         <tli id="T23" time="15.5" type="appl" />
         <tli id="T24" time="16.0" type="appl" />
         <tli id="T25" time="16.5" type="appl" />
         <tli id="T26" time="17.0" type="appl" />
         <tli id="T27" time="17.5" type="appl" />
         <tli id="T28" time="18.0" type="appl" />
         <tli id="T29" time="18.5" type="appl" />
         <tli id="T30" time="19.0" type="appl" />
         <tli id="T31" time="19.5" type="appl" />
         <tli id="T32" time="20.0" type="appl" />
         <tli id="T33" time="20.5" type="appl" />
         <tli id="T34" time="21.0" type="appl" />
         <tli id="T35" time="21.5" type="appl" />
         <tli id="T36" time="22.0" type="appl" />
         <tli id="T37" time="22.5" type="appl" />
         <tli id="T38" time="23.0" type="appl" />
         <tli id="T39" time="23.5" type="appl" />
         <tli id="T40" time="24.0" type="appl" />
         <tli id="T41" time="24.5" type="appl" />
         <tli id="T42" time="25.0" type="appl" />
         <tli id="T43" time="25.5" type="appl" />
         <tli id="T44" time="26.0" type="appl" />
         <tli id="T45" time="26.5" type="appl" />
         <tli id="T46" time="27.0" type="appl" />
         <tli id="T47" time="27.5" type="appl" />
         <tli id="T48" time="28.0" type="appl" />
         <tli id="T49" time="28.5" type="appl" />
         <tli id="T50" time="29.0" type="appl" />
         <tli id="T51" time="29.5" type="appl" />
         <tli id="T52" time="30.0" type="appl" />
         <tli id="T53" time="30.5" type="appl" />
         <tli id="T54" time="31.0" type="appl" />
         <tli id="T55" time="31.5" type="appl" />
         <tli id="T56" time="32.0" type="appl" />
         <tli id="T57" time="32.5" type="appl" />
         <tli id="T58" time="33.0" type="appl" />
         <tli id="T59" time="33.5" type="appl" />
         <tli id="T60" time="34.0" type="appl" />
         <tli id="T61" time="34.5" type="appl" />
         <tli id="T62" time="35.0" type="appl" />
         <tli id="T63" time="35.5" type="appl" />
         <tli id="T64" time="36.0" type="appl" />
         <tli id="T65" time="36.5" type="appl" />
         <tli id="T66" time="37.0" type="appl" />
         <tli id="T67" time="37.5" type="appl" />
         <tli id="T68" time="38.0" type="appl" />
         <tli id="T69" time="38.5" type="appl" />
         <tli id="T70" time="39.0" type="appl" />
         <tli id="T71" time="39.5" type="appl" />
         <tli id="T72" time="40.0" type="appl" />
         <tli id="T73" time="40.5" type="appl" />
         <tli id="T74" time="41.0" type="appl" />
         <tli id="T75" time="41.5" type="appl" />
         <tli id="T76" time="42.0" type="appl" />
         <tli id="T77" time="42.5" type="appl" />
         <tli id="T78" time="43.0" type="appl" />
         <tli id="T79" time="43.5" type="appl" />
         <tli id="T80" time="44.0" type="appl" />
         <tli id="T81" time="44.5" type="appl" />
         <tli id="T82" time="45.0" type="appl" />
         <tli id="T83" time="45.5" type="appl" />
         <tli id="T84" time="46.0" type="appl" />
         <tli id="T85" time="46.5" type="appl" />
         <tli id="T86" time="47.0" type="appl" />
         <tli id="T87" time="47.5" type="appl" />
         <tli id="T88" time="48.0" type="appl" />
         <tli id="T89" time="48.5" type="appl" />
         <tli id="T90" time="49.0" type="appl" />
         <tli id="T91" time="49.5" type="appl" />
         <tli id="T92" time="50.0" type="appl" />
         <tli id="T93" time="50.5" type="appl" />
         <tli id="T94" time="51.0" type="appl" />
         <tli id="T95" time="51.5" type="appl" />
         <tli id="T96" time="52.0" type="appl" />
         <tli id="T97" time="52.5" type="appl" />
         <tli id="T98" time="53.0" type="appl" />
         <tli id="T99" time="53.5" type="appl" />
         <tli id="T100" time="54.0" type="appl" />
         <tli id="T101" time="54.5" type="appl" />
         <tli id="T102" time="55.0" type="appl" />
         <tli id="T103" time="55.5" type="appl" />
         <tli id="T104" time="56.0" type="appl" />
         <tli id="T105" time="56.5" type="appl" />
         <tli id="T106" time="57.0" type="appl" />
         <tli id="T107" time="57.5" type="appl" />
         <tli id="T108" time="58.0" type="appl" />
         <tli id="T109" time="58.5" type="appl" />
         <tli id="T110" time="59.0" type="appl" />
         <tli id="T111" time="59.5" type="appl" />
         <tli id="T112" time="60.0" type="appl" />
         <tli id="T113" time="60.5" type="appl" />
         <tli id="T114" time="61.0" type="appl" />
         <tli id="T115" time="61.5" type="appl" />
         <tli id="T116" time="62.0" type="appl" />
         <tli id="T117" time="62.5" type="appl" />
         <tli id="T118" time="63.0" type="appl" />
         <tli id="T119" time="63.5" type="appl" />
         <tli id="T120" time="64.0" type="appl" />
         <tli id="T121" time="64.5" type="appl" />
         <tli id="T122" time="65.0" type="appl" />
         <tli id="T123" time="65.5" type="appl" />
         <tli id="T124" time="66.0" type="appl" />
         <tli id="T330" time="66.16666666666667" type="intp" />
         <tli id="T331" time="66.3" type="intp" />
         <tli id="T125" time="66.5" type="appl" />
         <tli id="T126" time="67.0" type="appl" />
         <tli id="T127" time="67.5" type="appl" />
         <tli id="T128" time="68.0" type="appl" />
         <tli id="T129" time="68.5" type="appl" />
         <tli id="T130" time="69.0" type="appl" />
         <tli id="T131" time="69.5" type="appl" />
         <tli id="T132" time="70.0" type="appl" />
         <tli id="T335" time="70.051" type="intp" />
         <tli id="T133" time="70.5" type="appl" />
         <tli id="T134" time="71.0" type="appl" />
         <tli id="T135" time="71.5" type="appl" />
         <tli id="T136" time="72.0" type="appl" />
         <tli id="T137" time="72.5" type="appl" />
         <tli id="T138" time="73.0" type="appl" />
         <tli id="T139" time="73.5" type="appl" />
         <tli id="T140" time="74.0" type="appl" />
         <tli id="T141" time="74.5" type="appl" />
         <tli id="T142" time="75.0" type="appl" />
         <tli id="T143" time="75.5" type="appl" />
         <tli id="T144" time="76.0" type="appl" />
         <tli id="T145" time="76.5" type="appl" />
         <tli id="T146" time="77.0" type="appl" />
         <tli id="T147" time="77.5" type="appl" />
         <tli id="T148" time="78.0" type="appl" />
         <tli id="T149" time="78.5" type="appl" />
         <tli id="T150" time="79.0" type="appl" />
         <tli id="T151" time="79.5" type="appl" />
         <tli id="T152" time="80.0" type="appl" />
         <tli id="T153" time="80.5" type="appl" />
         <tli id="T154" time="81.0" type="appl" />
         <tli id="T155" time="81.5" type="appl" />
         <tli id="T156" time="82.0" type="appl" />
         <tli id="T157" time="82.5" type="appl" />
         <tli id="T158" time="83.0" type="appl" />
         <tli id="T159" time="83.5" type="appl" />
         <tli id="T160" time="84.0" type="appl" />
         <tli id="T161" time="84.5" type="appl" />
         <tli id="T162" time="85.0" type="appl" />
         <tli id="T163" time="85.5" type="appl" />
         <tli id="T164" time="86.0" type="appl" />
         <tli id="T165" time="86.5" type="appl" />
         <tli id="T166" time="87.0" type="appl" />
         <tli id="T167" time="87.5" type="appl" />
         <tli id="T168" time="88.0" type="appl" />
         <tli id="T169" time="88.5" type="appl" />
         <tli id="T170" time="89.0" type="appl" />
         <tli id="T171" time="89.5" type="appl" />
         <tli id="T172" time="90.0" type="appl" />
         <tli id="T173" time="90.5" type="appl" />
         <tli id="T174" time="91.0" type="appl" />
         <tli id="T175" time="91.5" type="appl" />
         <tli id="T176" time="92.0" type="appl" />
         <tli id="T177" time="92.5" type="appl" />
         <tli id="T178" time="93.0" type="appl" />
         <tli id="T179" time="93.5" type="appl" />
         <tli id="T180" time="94.0" type="appl" />
         <tli id="T181" time="94.5" type="appl" />
         <tli id="T182" time="95.0" type="appl" />
         <tli id="T183" time="95.5" type="appl" />
         <tli id="T184" time="96.0" type="appl" />
         <tli id="T332" time="96.2" type="intp" />
         <tli id="T185" time="96.5" type="appl" />
         <tli id="T186" time="97.0" type="appl" />
         <tli id="T187" time="97.5" type="appl" />
         <tli id="T188" time="98.0" type="appl" />
         <tli id="T189" time="98.5" type="appl" />
         <tli id="T190" time="99.0" type="appl" />
         <tli id="T191" time="99.5" type="appl" />
         <tli id="T192" time="100.0" type="appl" />
         <tli id="T193" time="100.5" type="appl" />
         <tli id="T333" time="100.7" type="intp" />
         <tli id="T194" time="101.0" type="appl" />
         <tli id="T195" time="101.5" type="appl" />
         <tli id="T196" time="102.0" type="appl" />
         <tli id="T334" time="102.2" type="intp" />
         <tli id="T197" time="102.5" type="appl" />
         <tli id="T198" time="103.0" type="appl" />
         <tli id="T199" time="103.5" type="appl" />
         <tli id="T200" time="104.0" type="appl" />
         <tli id="T201" time="104.5" type="appl" />
         <tli id="T202" time="105.0" type="appl" />
         <tli id="T203" time="105.5" type="appl" />
         <tli id="T204" time="106.0" type="appl" />
         <tli id="T205" time="106.5" type="appl" />
         <tli id="T206" time="107.0" type="appl" />
         <tli id="T207" time="107.5" type="appl" />
         <tli id="T208" time="108.0" type="appl" />
         <tli id="T209" time="108.5" type="appl" />
         <tli id="T210" time="109.0" type="appl" />
         <tli id="T211" time="109.5" type="appl" />
         <tli id="T212" time="110.0" type="appl" />
         <tli id="T213" time="110.5" type="appl" />
         <tli id="T214" time="111.0" type="appl" />
         <tli id="T215" time="111.5" type="appl" />
         <tli id="T216" time="112.0" type="appl" />
         <tli id="T217" time="112.5" type="appl" />
         <tli id="T218" time="113.0" type="appl" />
         <tli id="T219" time="113.5" type="appl" />
         <tli id="T220" time="114.0" type="appl" />
         <tli id="T221" time="114.5" type="appl" />
         <tli id="T222" time="115.0" type="appl" />
         <tli id="T223" time="115.5" type="appl" />
         <tli id="T224" time="116.0" type="appl" />
         <tli id="T225" time="116.5" type="appl" />
         <tli id="T226" time="117.0" type="appl" />
         <tli id="T227" time="117.5" type="appl" />
         <tli id="T228" time="118.0" type="appl" />
         <tli id="T229" time="118.5" type="appl" />
         <tli id="T230" time="119.0" type="appl" />
         <tli id="T231" time="119.5" type="appl" />
         <tli id="T232" time="120.0" type="appl" />
         <tli id="T233" time="120.5" type="appl" />
         <tli id="T234" time="121.0" type="appl" />
         <tli id="T235" time="121.5" type="appl" />
         <tli id="T236" time="122.0" type="appl" />
         <tli id="T237" time="122.5" type="appl" />
         <tli id="T238" time="123.0" type="appl" />
         <tli id="T239" time="123.5" type="appl" />
         <tli id="T240" time="124.0" type="appl" />
         <tli id="T241" time="124.5" type="appl" />
         <tli id="T242" time="125.0" type="appl" />
         <tli id="T243" time="125.5" type="appl" />
         <tli id="T244" time="126.0" type="appl" />
         <tli id="T245" time="126.5" type="appl" />
         <tli id="T246" time="127.0" type="appl" />
         <tli id="T247" time="127.5" type="appl" />
         <tli id="T248" time="128.0" type="appl" />
         <tli id="T249" time="128.5" type="appl" />
         <tli id="T250" time="129.0" type="appl" />
         <tli id="T251" time="129.5" type="appl" />
         <tli id="T252" time="130.0" type="appl" />
         <tli id="T253" time="130.5" type="appl" />
         <tli id="T254" time="131.0" type="appl" />
         <tli id="T255" time="131.5" type="appl" />
         <tli id="T256" time="132.0" type="appl" />
         <tli id="T257" time="132.5" type="appl" />
         <tli id="T258" time="133.0" type="appl" />
         <tli id="T259" time="133.5" type="appl" />
         <tli id="T260" time="134.0" type="appl" />
         <tli id="T261" time="134.5" type="appl" />
         <tli id="T262" time="135.0" type="appl" />
         <tli id="T263" time="135.5" type="appl" />
         <tli id="T264" time="136.0" type="appl" />
         <tli id="T265" time="136.5" type="appl" />
         <tli id="T266" time="137.0" type="appl" />
         <tli id="T267" time="137.5" type="appl" />
         <tli id="T268" time="138.0" type="appl" />
         <tli id="T269" time="138.5" type="appl" />
         <tli id="T270" time="139.0" type="appl" />
         <tli id="T271" time="139.5" type="appl" />
         <tli id="T272" time="140.0" type="appl" />
         <tli id="T273" time="140.5" type="appl" />
         <tli id="T274" time="141.0" type="appl" />
         <tli id="T275" time="141.5" type="appl" />
         <tli id="T276" time="142.0" type="appl" />
         <tli id="T277" time="142.5" type="appl" />
         <tli id="T278" time="143.0" type="appl" />
         <tli id="T279" time="143.5" type="appl" />
         <tli id="T280" time="144.0" type="appl" />
         <tli id="T281" time="144.5" type="appl" />
         <tli id="T282" time="145.0" type="appl" />
         <tli id="T283" time="145.5" type="appl" />
         <tli id="T284" time="146.0" type="appl" />
         <tli id="T285" time="146.5" type="appl" />
         <tli id="T286" time="147.0" type="appl" />
         <tli id="T287" time="147.5" type="appl" />
         <tli id="T288" time="148.0" type="appl" />
         <tli id="T289" time="148.5" type="appl" />
         <tli id="T290" time="149.0" type="appl" />
         <tli id="T291" time="149.5" type="appl" />
         <tli id="T292" time="150.0" type="appl" />
         <tli id="T293" time="150.5" type="appl" />
         <tli id="T294" time="151.0" type="appl" />
         <tli id="T295" time="151.5" type="appl" />
         <tli id="T296" time="152.0" type="appl" />
         <tli id="T297" time="152.5" type="appl" />
         <tli id="T298" time="153.0" type="appl" />
         <tli id="T299" time="153.5" type="appl" />
         <tli id="T300" time="154.0" type="appl" />
         <tli id="T301" time="154.5" type="appl" />
         <tli id="T302" time="155.0" type="appl" />
         <tli id="T303" time="155.5" type="appl" />
         <tli id="T304" time="156.0" type="appl" />
         <tli id="T305" time="156.5" type="appl" />
         <tli id="T306" time="157.0" type="appl" />
         <tli id="T307" time="157.5" type="appl" />
         <tli id="T308" time="158.0" type="appl" />
         <tli id="T309" time="158.5" type="appl" />
         <tli id="T310" time="159.0" type="appl" />
         <tli id="T311" time="159.5" type="appl" />
         <tli id="T312" time="160.0" type="appl" />
         <tli id="T313" time="160.5" type="appl" />
         <tli id="T314" time="161.0" type="appl" />
         <tli id="T315" time="161.5" type="appl" />
         <tli id="T316" time="162.0" type="appl" />
         <tli id="T317" time="162.5" type="appl" />
         <tli id="T318" time="163.0" type="appl" />
         <tli id="T319" time="163.5" type="appl" />
         <tli id="T320" time="164.0" type="appl" />
         <tli id="T321" time="164.5" type="appl" />
         <tli id="T322" time="165.0" type="appl" />
         <tli id="T323" time="165.5" type="appl" />
         <tli id="T324" time="166.0" type="appl" />
         <tli id="T325" time="166.5" type="appl" />
         <tli id="T326" time="167.0" type="appl" />
         <tli id="T327" time="167.5" type="appl" />
         <tli id="T328" time="168.0" type="appl" />
         <tli id="T329" time="168.5" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="YeSV"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T329" id="Seg_0" n="sc" s="T0">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Bɨlɨr</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">dʼon</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">olorbut</ts>
                  <nts id="Seg_11" n="HIAT:ip">,</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_14" n="HIAT:w" s="T3">toŋustar</ts>
                  <nts id="Seg_15" n="HIAT:ip">.</nts>
                  <nts id="Seg_16" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T13" id="Seg_18" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_20" n="HIAT:w" s="T4">Biːr</ts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_23" n="HIAT:w" s="T5">ogonnʼordoːktor</ts>
                  <nts id="Seg_24" n="HIAT:ip">,</nts>
                  <nts id="Seg_25" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_27" n="HIAT:w" s="T6">buntulara</ts>
                  <nts id="Seg_28" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_30" n="HIAT:w" s="T7">hübehit</ts>
                  <nts id="Seg_31" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_33" n="HIAT:w" s="T8">ete</ts>
                  <nts id="Seg_34" n="HIAT:ip">,</nts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_37" n="HIAT:w" s="T9">gini</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_40" n="HIAT:w" s="T10">hübetin</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_43" n="HIAT:w" s="T11">bɨspat</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_46" n="HIAT:w" s="T12">etiler</ts>
                  <nts id="Seg_47" n="HIAT:ip">.</nts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_50" n="HIAT:u" s="T13">
                  <ts e="T14" id="Seg_52" n="HIAT:w" s="T13">Kaja</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_55" n="HIAT:w" s="T14">hirge</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_58" n="HIAT:w" s="T15">ologurallarɨn</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_61" n="HIAT:w" s="T16">hübeliːr</ts>
                  <nts id="Seg_62" n="HIAT:ip">.</nts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T24" id="Seg_65" n="HIAT:u" s="T17">
                  <nts id="Seg_66" n="HIAT:ip">"</nts>
                  <ts e="T18" id="Seg_68" n="HIAT:w" s="T17">Mastaːk</ts>
                  <nts id="Seg_69" n="HIAT:ip">,</nts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_72" n="HIAT:w" s="T18">ičiges</ts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_75" n="HIAT:w" s="T19">hirge</ts>
                  <nts id="Seg_76" n="HIAT:ip">,</nts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_79" n="HIAT:w" s="T20">onnuk</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_82" n="HIAT:w" s="T21">hirge</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_85" n="HIAT:w" s="T22">oloksuju͡okput</ts>
                  <nts id="Seg_86" n="HIAT:ip">"</nts>
                  <nts id="Seg_87" n="HIAT:ip">,</nts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_90" n="HIAT:w" s="T23">diːr</ts>
                  <nts id="Seg_91" n="HIAT:ip">.</nts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T31" id="Seg_94" n="HIAT:u" s="T24">
                  <ts e="T25" id="Seg_96" n="HIAT:w" s="T24">Onno</ts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_99" n="HIAT:w" s="T25">tijen</ts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_102" n="HIAT:w" s="T26">ologurbuttar</ts>
                  <nts id="Seg_103" n="HIAT:ip">,</nts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_106" n="HIAT:w" s="T27">balagan</ts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_109" n="HIAT:w" s="T28">tuttubuttar</ts>
                  <nts id="Seg_110" n="HIAT:ip">,</nts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_113" n="HIAT:w" s="T29">horoktor</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_115" n="HIAT:ip">—</nts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_118" n="HIAT:w" s="T30">golomo</ts>
                  <nts id="Seg_119" n="HIAT:ip">.</nts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T35" id="Seg_122" n="HIAT:u" s="T31">
                  <ts e="T32" id="Seg_124" n="HIAT:w" s="T31">Üs</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_127" n="HIAT:w" s="T32">ɨ͡al</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_130" n="HIAT:w" s="T33">dʼulkaːk</ts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_133" n="HIAT:w" s="T34">bu͡olbuttar</ts>
                  <nts id="Seg_134" n="HIAT:ip">.</nts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T43" id="Seg_137" n="HIAT:u" s="T35">
                  <ts e="T36" id="Seg_139" n="HIAT:w" s="T35">Kaččaga</ts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_142" n="HIAT:w" s="T36">ere</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_145" n="HIAT:w" s="T37">hapaːs</ts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_148" n="HIAT:w" s="T38">ettere</ts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_151" n="HIAT:w" s="T39">barammɨt</ts>
                  <nts id="Seg_152" n="HIAT:ip">,</nts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_155" n="HIAT:w" s="T40">bu</ts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_158" n="HIAT:w" s="T41">kihiler</ts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_161" n="HIAT:w" s="T42">korgujbuttar</ts>
                  <nts id="Seg_162" n="HIAT:ip">.</nts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T51" id="Seg_165" n="HIAT:u" s="T43">
                  <ts e="T44" id="Seg_167" n="HIAT:w" s="T43">Tabalarɨn</ts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_170" n="HIAT:w" s="T44">hi͡ekterin</ts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_172" n="HIAT:ip">—</nts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_175" n="HIAT:w" s="T45">tabalara</ts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_178" n="HIAT:w" s="T46">aksɨːlaːk</ts>
                  <nts id="Seg_179" n="HIAT:ip">,</nts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_182" n="HIAT:w" s="T47">elete</ts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_185" n="HIAT:w" s="T48">bi͡es</ts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_188" n="HIAT:w" s="T49">tü͡ört</ts>
                  <nts id="Seg_189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_191" n="HIAT:w" s="T50">bu͡ollaga</ts>
                  <nts id="Seg_192" n="HIAT:ip">.</nts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T53" id="Seg_195" n="HIAT:u" s="T51">
                  <ts e="T52" id="Seg_197" n="HIAT:w" s="T51">Ogonnʼor</ts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_200" n="HIAT:w" s="T52">hübeliːr</ts>
                  <nts id="Seg_201" n="HIAT:ip">:</nts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T58" id="Seg_204" n="HIAT:u" s="T53">
                  <nts id="Seg_205" n="HIAT:ip">"</nts>
                  <ts e="T54" id="Seg_207" n="HIAT:w" s="T53">Tababɨtɨn</ts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_210" n="HIAT:w" s="T54">hi͡etekpitine</ts>
                  <nts id="Seg_211" n="HIAT:ip">,</nts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_214" n="HIAT:w" s="T55">bu͡or</ts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_217" n="HIAT:w" s="T56">dʼadaŋɨ</ts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_220" n="HIAT:w" s="T57">bu͡olu͡okput</ts>
                  <nts id="Seg_221" n="HIAT:ip">.</nts>
                  <nts id="Seg_222" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T60" id="Seg_224" n="HIAT:u" s="T58">
                  <ts e="T59" id="Seg_226" n="HIAT:w" s="T58">Hi͡emi͡egiŋ</ts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_229" n="HIAT:w" s="T59">tabanɨ</ts>
                  <nts id="Seg_230" n="HIAT:ip">.</nts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T61" id="Seg_233" n="HIAT:u" s="T60">
                  <ts e="T61" id="Seg_235" n="HIAT:w" s="T60">Hübeli͡em</ts>
                  <nts id="Seg_236" n="HIAT:ip">:</nts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T67" id="Seg_239" n="HIAT:u" s="T61">
                  <ts e="T62" id="Seg_241" n="HIAT:w" s="T61">Munna</ts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_244" n="HIAT:w" s="T62">bu</ts>
                  <nts id="Seg_245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_247" n="HIAT:w" s="T63">ürekke</ts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_250" n="HIAT:w" s="T64">ɨ͡allarbɨt</ts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_253" n="HIAT:w" s="T65">uskaːttar</ts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_256" n="HIAT:w" s="T66">baːllar</ts>
                  <nts id="Seg_257" n="HIAT:ip">.</nts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T69" id="Seg_260" n="HIAT:u" s="T67">
                  <ts e="T68" id="Seg_262" n="HIAT:w" s="T67">Onno</ts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_265" n="HIAT:w" s="T68">ojunnaːktar</ts>
                  <nts id="Seg_266" n="HIAT:ip">.</nts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T74" id="Seg_269" n="HIAT:u" s="T69">
                  <ts e="T70" id="Seg_271" n="HIAT:w" s="T69">Bihigi</ts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_274" n="HIAT:w" s="T70">albunnaːmmɨt</ts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_277" n="HIAT:w" s="T71">munna</ts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_280" n="HIAT:w" s="T72">egeli͡ekke</ts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_283" n="HIAT:w" s="T73">ginileri</ts>
                  <nts id="Seg_284" n="HIAT:ip">.</nts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T77" id="Seg_287" n="HIAT:u" s="T74">
                  <ts e="T75" id="Seg_289" n="HIAT:w" s="T74">Min</ts>
                  <nts id="Seg_290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_292" n="HIAT:w" s="T75">ɨ͡aldʼɨbɨt</ts>
                  <nts id="Seg_293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_295" n="HIAT:w" s="T76">bu͡oluːm</ts>
                  <nts id="Seg_296" n="HIAT:ip">.</nts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T79" id="Seg_299" n="HIAT:u" s="T77">
                  <ts e="T78" id="Seg_301" n="HIAT:w" s="T77">Ojunnarɨn</ts>
                  <nts id="Seg_302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_304" n="HIAT:w" s="T78">ɨgɨrɨŋ</ts>
                  <nts id="Seg_305" n="HIAT:ip">.</nts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T84" id="Seg_308" n="HIAT:u" s="T79">
                  <ts e="T80" id="Seg_310" n="HIAT:w" s="T79">Ginini</ts>
                  <nts id="Seg_311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_313" n="HIAT:w" s="T80">kɨtta</ts>
                  <nts id="Seg_314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_316" n="HIAT:w" s="T81">elbek</ts>
                  <nts id="Seg_317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_319" n="HIAT:w" s="T82">uskaːn</ts>
                  <nts id="Seg_320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_322" n="HIAT:w" s="T83">keli͡e</ts>
                  <nts id="Seg_323" n="HIAT:ip">.</nts>
                  <nts id="Seg_324" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T90" id="Seg_326" n="HIAT:u" s="T84">
                  <ts e="T85" id="Seg_328" n="HIAT:w" s="T84">Dʼe</ts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_331" n="HIAT:w" s="T85">oloru</ts>
                  <nts id="Seg_332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_334" n="HIAT:w" s="T86">bihigi</ts>
                  <nts id="Seg_335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_337" n="HIAT:w" s="T87">ölörtü͡ökpüt</ts>
                  <nts id="Seg_338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_340" n="HIAT:w" s="T88">u͡onna</ts>
                  <nts id="Seg_341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_343" n="HIAT:w" s="T89">hi͡ekpit</ts>
                  <nts id="Seg_344" n="HIAT:ip">.</nts>
                  <nts id="Seg_345" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T99" id="Seg_347" n="HIAT:u" s="T90">
                  <ts e="T91" id="Seg_349" n="HIAT:w" s="T90">Oččogo</ts>
                  <nts id="Seg_350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_352" n="HIAT:w" s="T91">inni</ts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_355" n="HIAT:w" s="T92">di͡ekki</ts>
                  <nts id="Seg_356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_358" n="HIAT:w" s="T93">atɨllɨ͡akpɨt</ts>
                  <nts id="Seg_359" n="HIAT:ip">,</nts>
                  <nts id="Seg_360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_362" n="HIAT:w" s="T94">dʼɨl</ts>
                  <nts id="Seg_363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_365" n="HIAT:w" s="T95">imijeriger</ts>
                  <nts id="Seg_366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_368" n="HIAT:w" s="T96">tiji͡ekpit</ts>
                  <nts id="Seg_369" n="HIAT:ip">,</nts>
                  <nts id="Seg_370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_372" n="HIAT:w" s="T97">bultanan</ts>
                  <nts id="Seg_373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_375" n="HIAT:w" s="T98">ahɨ͡akpɨt</ts>
                  <nts id="Seg_376" n="HIAT:ip">.</nts>
                  <nts id="Seg_377" n="HIAT:ip">"</nts>
                  <nts id="Seg_378" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T103" id="Seg_380" n="HIAT:u" s="T99">
                  <ts e="T100" id="Seg_382" n="HIAT:w" s="T99">Kihilere</ts>
                  <nts id="Seg_383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_385" n="HIAT:w" s="T100">uskaːnnarɨn</ts>
                  <nts id="Seg_386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_388" n="HIAT:w" s="T101">kördüː</ts>
                  <nts id="Seg_389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_391" n="HIAT:w" s="T102">bardɨlar</ts>
                  <nts id="Seg_392" n="HIAT:ip">.</nts>
                  <nts id="Seg_393" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T110" id="Seg_395" n="HIAT:u" s="T103">
                  <ts e="T104" id="Seg_397" n="HIAT:w" s="T103">Ogonnʼor</ts>
                  <nts id="Seg_398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_400" n="HIAT:w" s="T104">ɨ͡aldʼɨbɨta</ts>
                  <nts id="Seg_401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_403" n="HIAT:w" s="T105">bu͡olla</ts>
                  <nts id="Seg_404" n="HIAT:ip">,</nts>
                  <nts id="Seg_405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_407" n="HIAT:w" s="T106">ataktɨːn</ts>
                  <nts id="Seg_408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_410" n="HIAT:w" s="T107">hu͡organ</ts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_413" n="HIAT:w" s="T108">ihiger</ts>
                  <nts id="Seg_414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_416" n="HIAT:w" s="T109">hɨtta</ts>
                  <nts id="Seg_417" n="HIAT:ip">.</nts>
                  <nts id="Seg_418" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T112" id="Seg_420" n="HIAT:u" s="T110">
                  <ts e="T111" id="Seg_422" n="HIAT:w" s="T110">Emeːksiniger</ts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_425" n="HIAT:w" s="T111">eter</ts>
                  <nts id="Seg_426" n="HIAT:ip">:</nts>
                  <nts id="Seg_427" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T121" id="Seg_429" n="HIAT:u" s="T112">
                  <nts id="Seg_430" n="HIAT:ip">"</nts>
                  <ts e="T113" id="Seg_432" n="HIAT:w" s="T112">Uskaːn</ts>
                  <nts id="Seg_433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_435" n="HIAT:w" s="T113">ojuna</ts>
                  <nts id="Seg_436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_438" n="HIAT:w" s="T114">kelen</ts>
                  <nts id="Seg_439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_441" n="HIAT:w" s="T115">kɨːrdagɨna</ts>
                  <nts id="Seg_442" n="HIAT:ip">,</nts>
                  <nts id="Seg_443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_445" n="HIAT:w" s="T116">tünnükkün</ts>
                  <nts id="Seg_446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_448" n="HIAT:w" s="T117">ü͡öleskin</ts>
                  <nts id="Seg_449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_451" n="HIAT:w" s="T118">bü͡öleːr</ts>
                  <nts id="Seg_452" n="HIAT:ip">,</nts>
                  <nts id="Seg_453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_455" n="HIAT:w" s="T119">aːŋŋɨn</ts>
                  <nts id="Seg_456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_458" n="HIAT:w" s="T120">kaːjaːr</ts>
                  <nts id="Seg_459" n="HIAT:ip">.</nts>
                  <nts id="Seg_460" n="HIAT:ip">"</nts>
                  <nts id="Seg_461" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T125" id="Seg_463" n="HIAT:u" s="T121">
                  <ts e="T122" id="Seg_465" n="HIAT:w" s="T121">ɨnčɨktɨː-ɨnčɨktɨː</ts>
                  <nts id="Seg_466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_468" n="HIAT:w" s="T122">ülügüdejbite</ts>
                  <nts id="Seg_469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_471" n="HIAT:w" s="T123">bu͡olan</ts>
                  <nts id="Seg_472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_473" n="HIAT:ip">"</nts>
                  <ts e="T330" id="Seg_475" n="HIAT:w" s="T124">ɨː</ts>
                  <nts id="Seg_476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_478" n="HIAT:w" s="T330">ɨː</ts>
                  <nts id="Seg_479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_481" n="HIAT:w" s="T331">ɨ</ts>
                  <nts id="Seg_482" n="HIAT:ip">!</nts>
                  <nts id="Seg_483" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T132" id="Seg_485" n="HIAT:u" s="T125">
                  <ts e="T126" id="Seg_487" n="HIAT:w" s="T125">Oldoːnbo</ts>
                  <nts id="Seg_488" n="HIAT:ip">"</nts>
                  <nts id="Seg_489" n="HIAT:ip">,</nts>
                  <nts id="Seg_490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_492" n="HIAT:w" s="T126">di͡egim</ts>
                  <nts id="Seg_493" n="HIAT:ip">,</nts>
                  <nts id="Seg_494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_496" n="HIAT:w" s="T127">oččogo</ts>
                  <nts id="Seg_497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_499" n="HIAT:w" s="T128">oldoːnu</ts>
                  <nts id="Seg_500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_502" n="HIAT:w" s="T129">tuttaraːr</ts>
                  <nts id="Seg_503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_505" n="HIAT:w" s="T130">mini͡eke</ts>
                  <nts id="Seg_506" n="HIAT:ip">.</nts>
                  <nts id="Seg_507" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T148" id="Seg_509" n="HIAT:u" s="T132">
                  <nts id="Seg_510" n="HIAT:ip">"</nts>
                  <ts e="T335" id="Seg_512" n="HIAT:w" s="T132">ɨ-ɨ</ts>
                  <nts id="Seg_513" n="HIAT:ip">,</nts>
                  <nts id="Seg_514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_516" n="HIAT:w" s="T335">godoroːbo</ts>
                  <nts id="Seg_517" n="HIAT:ip">"</nts>
                  <nts id="Seg_518" n="HIAT:ip">,</nts>
                  <nts id="Seg_519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_521" n="HIAT:w" s="T133">di͡egim</ts>
                  <nts id="Seg_522" n="HIAT:ip">,</nts>
                  <nts id="Seg_523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_525" n="HIAT:w" s="T134">oččogo</ts>
                  <nts id="Seg_526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_528" n="HIAT:w" s="T135">en</ts>
                  <nts id="Seg_529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_531" n="HIAT:w" s="T136">gedereːni</ts>
                  <nts id="Seg_532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_534" n="HIAT:w" s="T137">tutaːr</ts>
                  <nts id="Seg_535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_537" n="HIAT:w" s="T138">aːŋŋa</ts>
                  <nts id="Seg_538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_540" n="HIAT:w" s="T139">tohujaːr</ts>
                  <nts id="Seg_541" n="HIAT:ip">,</nts>
                  <nts id="Seg_542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_544" n="HIAT:w" s="T140">oččogo</ts>
                  <nts id="Seg_545" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_547" n="HIAT:w" s="T141">min</ts>
                  <nts id="Seg_548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_550" n="HIAT:w" s="T142">ojon</ts>
                  <nts id="Seg_551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_553" n="HIAT:w" s="T143">turan</ts>
                  <nts id="Seg_554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_556" n="HIAT:w" s="T144">čokujtaːn</ts>
                  <nts id="Seg_557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_559" n="HIAT:w" s="T145">barɨ͡am</ts>
                  <nts id="Seg_560" n="HIAT:ip">,</nts>
                  <nts id="Seg_561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_563" n="HIAT:w" s="T146">ojunnarɨn</ts>
                  <nts id="Seg_564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_566" n="HIAT:w" s="T147">uruttu͡om</ts>
                  <nts id="Seg_567" n="HIAT:ip">.</nts>
                  <nts id="Seg_568" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T151" id="Seg_570" n="HIAT:u" s="T148">
                  <ts e="T149" id="Seg_572" n="HIAT:w" s="T148">En</ts>
                  <nts id="Seg_573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_575" n="HIAT:w" s="T149">emi͡e</ts>
                  <nts id="Seg_576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_578" n="HIAT:w" s="T150">tuttumaktaːr</ts>
                  <nts id="Seg_579" n="HIAT:ip">.</nts>
                  <nts id="Seg_580" n="HIAT:ip">"</nts>
                  <nts id="Seg_581" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T157" id="Seg_583" n="HIAT:u" s="T151">
                  <ts e="T152" id="Seg_585" n="HIAT:w" s="T151">Ogonnʼor</ts>
                  <nts id="Seg_586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_588" n="HIAT:w" s="T152">hɨtar</ts>
                  <nts id="Seg_589" n="HIAT:ip">,</nts>
                  <nts id="Seg_590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_592" n="HIAT:w" s="T153">ɨnčɨktɨːr</ts>
                  <nts id="Seg_593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_595" n="HIAT:w" s="T154">kihini</ts>
                  <nts id="Seg_596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_598" n="HIAT:w" s="T155">bilbet</ts>
                  <nts id="Seg_599" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_601" n="HIAT:w" s="T156">bu͡ola-bu͡ola</ts>
                  <nts id="Seg_602" n="HIAT:ip">.</nts>
                  <nts id="Seg_603" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T165" id="Seg_605" n="HIAT:u" s="T157">
                  <ts e="T158" id="Seg_607" n="HIAT:w" s="T157">Uskaːn</ts>
                  <nts id="Seg_608" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_610" n="HIAT:w" s="T158">ojuna</ts>
                  <nts id="Seg_611" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_613" n="HIAT:w" s="T159">keler</ts>
                  <nts id="Seg_614" n="HIAT:ip">,</nts>
                  <nts id="Seg_615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_617" n="HIAT:w" s="T160">ginini</ts>
                  <nts id="Seg_618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_620" n="HIAT:w" s="T161">kɨtta</ts>
                  <nts id="Seg_621" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_623" n="HIAT:w" s="T162">elbek</ts>
                  <nts id="Seg_624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_626" n="HIAT:w" s="T163">uskaːn</ts>
                  <nts id="Seg_627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_629" n="HIAT:w" s="T164">kelbit</ts>
                  <nts id="Seg_630" n="HIAT:ip">.</nts>
                  <nts id="Seg_631" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T170" id="Seg_633" n="HIAT:u" s="T165">
                  <ts e="T166" id="Seg_635" n="HIAT:w" s="T165">Balagan</ts>
                  <nts id="Seg_636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_638" n="HIAT:w" s="T166">ihe</ts>
                  <nts id="Seg_639" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_641" n="HIAT:w" s="T167">uskaːn</ts>
                  <nts id="Seg_642" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_644" n="HIAT:w" s="T168">agaj</ts>
                  <nts id="Seg_645" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_647" n="HIAT:w" s="T169">bu͡olbut</ts>
                  <nts id="Seg_648" n="HIAT:ip">.</nts>
                  <nts id="Seg_649" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T176" id="Seg_651" n="HIAT:u" s="T170">
                  <ts e="T171" id="Seg_653" n="HIAT:w" s="T170">Emeːksin</ts>
                  <nts id="Seg_654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_656" n="HIAT:w" s="T171">tünnügü</ts>
                  <nts id="Seg_657" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_659" n="HIAT:w" s="T172">ü͡ölehi</ts>
                  <nts id="Seg_660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_662" n="HIAT:w" s="T173">bü͡öleːbit</ts>
                  <nts id="Seg_663" n="HIAT:ip">,</nts>
                  <nts id="Seg_664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_666" n="HIAT:w" s="T174">aːnɨ</ts>
                  <nts id="Seg_667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_669" n="HIAT:w" s="T175">kaːjbɨt</ts>
                  <nts id="Seg_670" n="HIAT:ip">.</nts>
                  <nts id="Seg_671" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T181" id="Seg_673" n="HIAT:u" s="T176">
                  <ts e="T177" id="Seg_675" n="HIAT:w" s="T176">Uskaːn</ts>
                  <nts id="Seg_676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_678" n="HIAT:w" s="T177">ojuna</ts>
                  <nts id="Seg_679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_681" n="HIAT:w" s="T178">kɨːrbɨt</ts>
                  <nts id="Seg_682" n="HIAT:ip">,</nts>
                  <nts id="Seg_683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_685" n="HIAT:w" s="T179">hotoru</ts>
                  <nts id="Seg_686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_688" n="HIAT:w" s="T180">ekkireːbit</ts>
                  <nts id="Seg_689" n="HIAT:ip">.</nts>
                  <nts id="Seg_690" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T185" id="Seg_692" n="HIAT:u" s="T181">
                  <ts e="T182" id="Seg_694" n="HIAT:w" s="T181">Ogonnʼor</ts>
                  <nts id="Seg_695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_697" n="HIAT:w" s="T182">bergeːbit</ts>
                  <nts id="Seg_698" n="HIAT:ip">,</nts>
                  <nts id="Seg_699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_701" n="HIAT:w" s="T183">ülügüdüjde</ts>
                  <nts id="Seg_702" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_703" n="HIAT:ip">"</nts>
                  <ts e="T332" id="Seg_705" n="HIAT:w" s="T184">ɨ</ts>
                  <nts id="Seg_706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_708" n="HIAT:w" s="T332">ɨ</ts>
                  <nts id="Seg_709" n="HIAT:ip">!</nts>
                  <nts id="Seg_710" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T187" id="Seg_712" n="HIAT:u" s="T185">
                  <ts e="T186" id="Seg_714" n="HIAT:w" s="T185">Oldoːnbo</ts>
                  <nts id="Seg_715" n="HIAT:ip">"</nts>
                  <nts id="Seg_716" n="HIAT:ip">,</nts>
                  <nts id="Seg_717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_719" n="HIAT:w" s="T186">di͡ebit</ts>
                  <nts id="Seg_720" n="HIAT:ip">.</nts>
                  <nts id="Seg_721" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T193" id="Seg_723" n="HIAT:u" s="T187">
                  <ts e="T188" id="Seg_725" n="HIAT:w" s="T187">Emeːksin</ts>
                  <nts id="Seg_726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_728" n="HIAT:w" s="T188">ojon</ts>
                  <nts id="Seg_729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_731" n="HIAT:w" s="T189">turan</ts>
                  <nts id="Seg_732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_734" n="HIAT:w" s="T190">ogonnʼorgo</ts>
                  <nts id="Seg_735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_737" n="HIAT:w" s="T191">oldoːnu</ts>
                  <nts id="Seg_738" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_740" n="HIAT:w" s="T192">tuttarbɨt</ts>
                  <nts id="Seg_741" n="HIAT:ip">.</nts>
                  <nts id="Seg_742" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T194" id="Seg_744" n="HIAT:u" s="T193">
                  <nts id="Seg_745" n="HIAT:ip">"</nts>
                  <ts e="T333" id="Seg_747" n="HIAT:w" s="T193">ɨ</ts>
                  <nts id="Seg_748" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_750" n="HIAT:w" s="T333">ɨ</ts>
                  <nts id="Seg_751" n="HIAT:ip">!</nts>
                  <nts id="Seg_752" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T196" id="Seg_754" n="HIAT:u" s="T194">
                  <ts e="T195" id="Seg_756" n="HIAT:w" s="T194">Godoroːbo</ts>
                  <nts id="Seg_757" n="HIAT:ip">"</nts>
                  <nts id="Seg_758" n="HIAT:ip">,</nts>
                  <nts id="Seg_759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_761" n="HIAT:w" s="T195">di͡ebit</ts>
                  <nts id="Seg_762" n="HIAT:ip">.</nts>
                  <nts id="Seg_763" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T197" id="Seg_765" n="HIAT:u" s="T196">
                  <nts id="Seg_766" n="HIAT:ip">"</nts>
                  <ts e="T334" id="Seg_768" n="HIAT:w" s="T196">ɨ</ts>
                  <nts id="Seg_769" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_771" n="HIAT:w" s="T334">ɨ</ts>
                  <nts id="Seg_772" n="HIAT:ip">!</nts>
                  <nts id="Seg_773" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T198" id="Seg_775" n="HIAT:u" s="T197">
                  <ts e="T198" id="Seg_777" n="HIAT:w" s="T197">Hükečaːnma</ts>
                  <nts id="Seg_778" n="HIAT:ip">!</nts>
                  <nts id="Seg_779" n="HIAT:ip">"</nts>
                  <nts id="Seg_780" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T206" id="Seg_782" n="HIAT:u" s="T198">
                  <ts e="T199" id="Seg_784" n="HIAT:w" s="T198">Innʼe</ts>
                  <nts id="Seg_785" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_787" n="HIAT:w" s="T199">di͡etin</ts>
                  <nts id="Seg_788" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_790" n="HIAT:w" s="T200">emeːksine</ts>
                  <nts id="Seg_791" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_793" n="HIAT:w" s="T201">gedereːni</ts>
                  <nts id="Seg_794" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_796" n="HIAT:w" s="T202">kappɨt</ts>
                  <nts id="Seg_797" n="HIAT:ip">,</nts>
                  <nts id="Seg_798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_800" n="HIAT:w" s="T203">dʼulkaːga</ts>
                  <nts id="Seg_801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_803" n="HIAT:w" s="T204">hükečaːnɨ</ts>
                  <nts id="Seg_804" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_806" n="HIAT:w" s="T205">kappɨt</ts>
                  <nts id="Seg_807" n="HIAT:ip">.</nts>
                  <nts id="Seg_808" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T209" id="Seg_810" n="HIAT:u" s="T206">
                  <ts e="T207" id="Seg_812" n="HIAT:w" s="T206">Ojunnara</ts>
                  <nts id="Seg_813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_815" n="HIAT:w" s="T207">ekkiriː</ts>
                  <nts id="Seg_816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_818" n="HIAT:w" s="T208">turar</ts>
                  <nts id="Seg_819" n="HIAT:ip">:</nts>
                  <nts id="Seg_820" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T213" id="Seg_822" n="HIAT:u" s="T209">
                  <nts id="Seg_823" n="HIAT:ip">"</nts>
                  <ts e="T210" id="Seg_825" n="HIAT:w" s="T209">Eduk</ts>
                  <nts id="Seg_826" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_828" n="HIAT:w" s="T210">dʼuːduk</ts>
                  <nts id="Seg_829" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_831" n="HIAT:w" s="T211">judʼoŋoː</ts>
                  <nts id="Seg_832" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_834" n="HIAT:w" s="T212">bimiː</ts>
                  <nts id="Seg_835" n="HIAT:ip">!</nts>
                  <nts id="Seg_836" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T215" id="Seg_838" n="HIAT:u" s="T213">
                  <ts e="T214" id="Seg_840" n="HIAT:w" s="T213">Paːpalʼdi͡ek</ts>
                  <nts id="Seg_841" n="HIAT:ip">"</nts>
                  <nts id="Seg_842" n="HIAT:ip">,</nts>
                  <nts id="Seg_843" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_845" n="HIAT:w" s="T214">diː-diː</ts>
                  <nts id="Seg_846" n="HIAT:ip">.</nts>
                  <nts id="Seg_847" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T225" id="Seg_849" n="HIAT:u" s="T215">
                  <ts e="T216" id="Seg_851" n="HIAT:w" s="T215">Ol</ts>
                  <nts id="Seg_852" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_854" n="HIAT:w" s="T216">aːta</ts>
                  <nts id="Seg_855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_856" n="HIAT:ip">"</nts>
                  <ts e="T218" id="Seg_858" n="HIAT:w" s="T217">Bu</ts>
                  <nts id="Seg_859" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_861" n="HIAT:w" s="T218">dʼi͡etten</ts>
                  <nts id="Seg_862" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_864" n="HIAT:w" s="T219">taksɨ͡ak</ts>
                  <nts id="Seg_865" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_867" n="HIAT:w" s="T220">bu͡olu͡ok</ts>
                  <nts id="Seg_868" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_870" n="HIAT:w" s="T221">bu͡ollarbɨn</ts>
                  <nts id="Seg_871" n="HIAT:ip">"</nts>
                  <nts id="Seg_872" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_874" n="HIAT:w" s="T223">di͡en</ts>
                  <nts id="Seg_875" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_877" n="HIAT:w" s="T224">toŋustuː</ts>
                  <nts id="Seg_878" n="HIAT:ip">.</nts>
                  <nts id="Seg_879" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T233" id="Seg_881" n="HIAT:u" s="T225">
                  <ts e="T226" id="Seg_883" n="HIAT:w" s="T225">Ogonnʼor</ts>
                  <nts id="Seg_884" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_886" n="HIAT:w" s="T226">onu</ts>
                  <nts id="Seg_887" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_889" n="HIAT:w" s="T227">isteːt</ts>
                  <nts id="Seg_890" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_892" n="HIAT:w" s="T228">ojon</ts>
                  <nts id="Seg_893" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_895" n="HIAT:w" s="T229">turar</ts>
                  <nts id="Seg_896" n="HIAT:ip">,</nts>
                  <nts id="Seg_897" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_899" n="HIAT:w" s="T230">ojunnara</ts>
                  <nts id="Seg_900" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_902" n="HIAT:w" s="T231">ojbut</ts>
                  <nts id="Seg_903" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_905" n="HIAT:w" s="T232">ükeːptüːŋŋe</ts>
                  <nts id="Seg_906" n="HIAT:ip">.</nts>
                  <nts id="Seg_907" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T246" id="Seg_909" n="HIAT:u" s="T233">
                  <ts e="T234" id="Seg_911" n="HIAT:w" s="T233">Ogonnʼor</ts>
                  <nts id="Seg_912" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_914" n="HIAT:w" s="T234">oldoːnunan</ts>
                  <nts id="Seg_915" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_917" n="HIAT:w" s="T235">ükeːptüːŋŋe</ts>
                  <nts id="Seg_918" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_920" n="HIAT:w" s="T236">kɨhaja</ts>
                  <nts id="Seg_921" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_923" n="HIAT:w" s="T237">oksor</ts>
                  <nts id="Seg_924" n="HIAT:ip">,</nts>
                  <nts id="Seg_925" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_927" n="HIAT:w" s="T238">ojun-uskaːn</ts>
                  <nts id="Seg_928" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_930" n="HIAT:w" s="T239">aharan</ts>
                  <nts id="Seg_931" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_933" n="HIAT:w" s="T240">bi͡erbit</ts>
                  <nts id="Seg_934" n="HIAT:ip">,</nts>
                  <nts id="Seg_935" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_937" n="HIAT:w" s="T241">ikki</ts>
                  <nts id="Seg_938" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_940" n="HIAT:w" s="T242">kulgaːgɨn</ts>
                  <nts id="Seg_941" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_943" n="HIAT:w" s="T243">ere</ts>
                  <nts id="Seg_944" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_946" n="HIAT:w" s="T244">melti</ts>
                  <nts id="Seg_947" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_949" n="HIAT:w" s="T245">oksubut</ts>
                  <nts id="Seg_950" n="HIAT:ip">.</nts>
                  <nts id="Seg_951" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T258" id="Seg_953" n="HIAT:u" s="T246">
                  <ts e="T247" id="Seg_955" n="HIAT:w" s="T246">Uskaːn</ts>
                  <nts id="Seg_956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_958" n="HIAT:w" s="T247">ojuna</ts>
                  <nts id="Seg_959" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_961" n="HIAT:w" s="T248">kɨs</ts>
                  <nts id="Seg_962" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_964" n="HIAT:w" s="T249">maska</ts>
                  <nts id="Seg_965" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_967" n="HIAT:w" s="T250">ojbut</ts>
                  <nts id="Seg_968" n="HIAT:ip">,</nts>
                  <nts id="Seg_969" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_971" n="HIAT:w" s="T251">ojoːtun</ts>
                  <nts id="Seg_972" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_974" n="HIAT:w" s="T252">emeːksin</ts>
                  <nts id="Seg_975" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_977" n="HIAT:w" s="T253">kedereːninen</ts>
                  <nts id="Seg_978" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_980" n="HIAT:w" s="T254">oksor</ts>
                  <nts id="Seg_981" n="HIAT:ip">,</nts>
                  <nts id="Seg_982" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_984" n="HIAT:w" s="T255">kelin</ts>
                  <nts id="Seg_985" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_987" n="HIAT:w" s="T256">agaj</ts>
                  <nts id="Seg_988" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_990" n="HIAT:w" s="T257">atagɨgar</ts>
                  <nts id="Seg_991" n="HIAT:ip">.</nts>
                  <nts id="Seg_992" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T262" id="Seg_994" n="HIAT:u" s="T258">
                  <ts e="T259" id="Seg_996" n="HIAT:w" s="T258">Ikki</ts>
                  <nts id="Seg_997" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_999" n="HIAT:w" s="T259">kelin</ts>
                  <nts id="Seg_1000" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_1002" n="HIAT:w" s="T260">atagɨn</ts>
                  <nts id="Seg_1003" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_1005" n="HIAT:w" s="T261">tohutar</ts>
                  <nts id="Seg_1006" n="HIAT:ip">.</nts>
                  <nts id="Seg_1007" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T274" id="Seg_1009" n="HIAT:u" s="T262">
                  <ts e="T263" id="Seg_1011" n="HIAT:w" s="T262">Uskaːn</ts>
                  <nts id="Seg_1012" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_1014" n="HIAT:w" s="T263">ojuna</ts>
                  <nts id="Seg_1015" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_1017" n="HIAT:w" s="T264">kɨs</ts>
                  <nts id="Seg_1018" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_1020" n="HIAT:w" s="T265">mastan</ts>
                  <nts id="Seg_1021" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_1023" n="HIAT:w" s="T266">ü͡ölehinen</ts>
                  <nts id="Seg_1024" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_1026" n="HIAT:w" s="T267">ojor</ts>
                  <nts id="Seg_1027" n="HIAT:ip">,</nts>
                  <nts id="Seg_1028" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_1030" n="HIAT:w" s="T268">onuga</ts>
                  <nts id="Seg_1031" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_1033" n="HIAT:w" s="T269">uskaːn</ts>
                  <nts id="Seg_1034" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_1036" n="HIAT:w" s="T270">emeːksine</ts>
                  <nts id="Seg_1037" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_1039" n="HIAT:w" s="T271">emi͡e</ts>
                  <nts id="Seg_1040" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_1042" n="HIAT:w" s="T272">ü͡ölehinen</ts>
                  <nts id="Seg_1043" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_1045" n="HIAT:w" s="T273">ojbut</ts>
                  <nts id="Seg_1046" n="HIAT:ip">.</nts>
                  <nts id="Seg_1047" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T281" id="Seg_1049" n="HIAT:u" s="T274">
                  <ts e="T275" id="Seg_1051" n="HIAT:w" s="T274">Dʼi͡ege</ts>
                  <nts id="Seg_1052" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_1054" n="HIAT:w" s="T275">baːr</ts>
                  <nts id="Seg_1055" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_1057" n="HIAT:w" s="T276">uskaːttarɨ</ts>
                  <nts id="Seg_1058" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_1060" n="HIAT:w" s="T277">barɨlarɨn</ts>
                  <nts id="Seg_1061" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_1063" n="HIAT:w" s="T278">imičči</ts>
                  <nts id="Seg_1064" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_1066" n="HIAT:w" s="T279">ölörtöːn</ts>
                  <nts id="Seg_1067" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_1069" n="HIAT:w" s="T280">keːspitter</ts>
                  <nts id="Seg_1070" n="HIAT:ip">.</nts>
                  <nts id="Seg_1071" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T287" id="Seg_1073" n="HIAT:u" s="T281">
                  <ts e="T282" id="Seg_1075" n="HIAT:w" s="T281">Manɨ</ts>
                  <nts id="Seg_1076" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_1078" n="HIAT:w" s="T282">hi͡enner</ts>
                  <nts id="Seg_1079" n="HIAT:ip">,</nts>
                  <nts id="Seg_1080" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_1082" n="HIAT:w" s="T283">haːs</ts>
                  <nts id="Seg_1083" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_1085" n="HIAT:w" s="T284">dʼɨl</ts>
                  <nts id="Seg_1086" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_1088" n="HIAT:w" s="T285">imijeriger</ts>
                  <nts id="Seg_1089" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_1091" n="HIAT:w" s="T286">tijbitter</ts>
                  <nts id="Seg_1092" n="HIAT:ip">.</nts>
                  <nts id="Seg_1093" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T291" id="Seg_1095" n="HIAT:u" s="T287">
                  <ts e="T288" id="Seg_1097" n="HIAT:w" s="T287">Urut</ts>
                  <nts id="Seg_1098" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_1100" n="HIAT:w" s="T288">uskaːnɨ</ts>
                  <nts id="Seg_1101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_1103" n="HIAT:w" s="T289">hi͡ebet</ts>
                  <nts id="Seg_1104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_1106" n="HIAT:w" s="T290">ühüler</ts>
                  <nts id="Seg_1107" n="HIAT:ip">.</nts>
                  <nts id="Seg_1108" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T297" id="Seg_1110" n="HIAT:u" s="T291">
                  <ts e="T292" id="Seg_1112" n="HIAT:w" s="T291">Hitinten</ts>
                  <nts id="Seg_1113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_1115" n="HIAT:w" s="T292">dʼe</ts>
                  <nts id="Seg_1116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_1118" n="HIAT:w" s="T293">uskaːnɨ</ts>
                  <nts id="Seg_1119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1121" n="HIAT:w" s="T294">hiːr</ts>
                  <nts id="Seg_1122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1124" n="HIAT:w" s="T295">bu͡olbuttar</ts>
                  <nts id="Seg_1125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_1127" n="HIAT:w" s="T296">dʼonnor</ts>
                  <nts id="Seg_1128" n="HIAT:ip">.</nts>
                  <nts id="Seg_1129" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T303" id="Seg_1131" n="HIAT:u" s="T297">
                  <ts e="T298" id="Seg_1133" n="HIAT:w" s="T297">Küreːbit</ts>
                  <nts id="Seg_1134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_1136" n="HIAT:w" s="T298">uskaːn</ts>
                  <nts id="Seg_1137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_1139" n="HIAT:w" s="T299">ojunuttan</ts>
                  <nts id="Seg_1140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_1142" n="HIAT:w" s="T300">kɨːl</ts>
                  <nts id="Seg_1143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_1145" n="HIAT:w" s="T301">uskaːn</ts>
                  <nts id="Seg_1146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1148" n="HIAT:w" s="T302">ü͡öskeːbit</ts>
                  <nts id="Seg_1149" n="HIAT:ip">.</nts>
                  <nts id="Seg_1150" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T310" id="Seg_1152" n="HIAT:u" s="T303">
                  <ts e="T304" id="Seg_1154" n="HIAT:w" s="T303">Haŋata</ts>
                  <nts id="Seg_1155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_1157" n="HIAT:w" s="T304">da</ts>
                  <nts id="Seg_1158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_1160" n="HIAT:w" s="T305">hu͡ok</ts>
                  <nts id="Seg_1161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_1163" n="HIAT:w" s="T306">bu͡olbut</ts>
                  <nts id="Seg_1164" n="HIAT:ip">,</nts>
                  <nts id="Seg_1165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_1167" n="HIAT:w" s="T307">dʼonton</ts>
                  <nts id="Seg_1168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1170" n="HIAT:w" s="T308">kürener</ts>
                  <nts id="Seg_1171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_1173" n="HIAT:w" s="T309">bu͡olbut</ts>
                  <nts id="Seg_1174" n="HIAT:ip">.</nts>
                  <nts id="Seg_1175" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T328" id="Seg_1177" n="HIAT:u" s="T310">
                  <ts e="T311" id="Seg_1179" n="HIAT:w" s="T310">Bili</ts>
                  <nts id="Seg_1180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_1182" n="HIAT:w" s="T311">ogonnʼor</ts>
                  <nts id="Seg_1183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_1185" n="HIAT:w" s="T312">oksubututtan</ts>
                  <nts id="Seg_1186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_1188" n="HIAT:w" s="T313">uskaːn</ts>
                  <nts id="Seg_1189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_1191" n="HIAT:w" s="T314">kulgaːgɨn</ts>
                  <nts id="Seg_1192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1194" n="HIAT:w" s="T315">töbötö</ts>
                  <nts id="Seg_1195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_1197" n="HIAT:w" s="T316">üjetten</ts>
                  <nts id="Seg_1198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_1200" n="HIAT:w" s="T317">üjege</ts>
                  <nts id="Seg_1201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1203" n="HIAT:w" s="T318">beli͡eleːk</ts>
                  <nts id="Seg_1204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_1206" n="HIAT:w" s="T319">bu͡olbut</ts>
                  <nts id="Seg_1207" n="HIAT:ip">;</nts>
                  <nts id="Seg_1208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1210" n="HIAT:w" s="T320">emeːksin</ts>
                  <nts id="Seg_1211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_1213" n="HIAT:w" s="T321">tostu</ts>
                  <nts id="Seg_1214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1216" n="HIAT:w" s="T322">oksubut</ts>
                  <nts id="Seg_1217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_1219" n="HIAT:w" s="T323">kelin</ts>
                  <nts id="Seg_1220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1222" n="HIAT:w" s="T324">ataga</ts>
                  <nts id="Seg_1223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_1225" n="HIAT:w" s="T325">tuhunan</ts>
                  <nts id="Seg_1226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1228" n="HIAT:w" s="T326">hühü͡ök</ts>
                  <nts id="Seg_1229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_1231" n="HIAT:w" s="T327">bu͡olbut</ts>
                  <nts id="Seg_1232" n="HIAT:ip">.</nts>
                  <nts id="Seg_1233" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T329" id="Seg_1235" n="HIAT:u" s="T328">
                  <ts e="T329" id="Seg_1237" n="HIAT:w" s="T328">Elete</ts>
                  <nts id="Seg_1238" n="HIAT:ip">.</nts>
                  <nts id="Seg_1239" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T329" id="Seg_1240" n="sc" s="T0">
               <ts e="T1" id="Seg_1242" n="e" s="T0">Bɨlɨr </ts>
               <ts e="T2" id="Seg_1244" n="e" s="T1">dʼon </ts>
               <ts e="T3" id="Seg_1246" n="e" s="T2">olorbut, </ts>
               <ts e="T4" id="Seg_1248" n="e" s="T3">toŋustar. </ts>
               <ts e="T5" id="Seg_1250" n="e" s="T4">Biːr </ts>
               <ts e="T6" id="Seg_1252" n="e" s="T5">ogonnʼordoːktor, </ts>
               <ts e="T7" id="Seg_1254" n="e" s="T6">buntulara </ts>
               <ts e="T8" id="Seg_1256" n="e" s="T7">hübehit </ts>
               <ts e="T9" id="Seg_1258" n="e" s="T8">ete, </ts>
               <ts e="T10" id="Seg_1260" n="e" s="T9">gini </ts>
               <ts e="T11" id="Seg_1262" n="e" s="T10">hübetin </ts>
               <ts e="T12" id="Seg_1264" n="e" s="T11">bɨspat </ts>
               <ts e="T13" id="Seg_1266" n="e" s="T12">etiler. </ts>
               <ts e="T14" id="Seg_1268" n="e" s="T13">Kaja </ts>
               <ts e="T15" id="Seg_1270" n="e" s="T14">hirge </ts>
               <ts e="T16" id="Seg_1272" n="e" s="T15">ologurallarɨn </ts>
               <ts e="T17" id="Seg_1274" n="e" s="T16">hübeliːr. </ts>
               <ts e="T18" id="Seg_1276" n="e" s="T17">"Mastaːk, </ts>
               <ts e="T19" id="Seg_1278" n="e" s="T18">ičiges </ts>
               <ts e="T20" id="Seg_1280" n="e" s="T19">hirge, </ts>
               <ts e="T21" id="Seg_1282" n="e" s="T20">onnuk </ts>
               <ts e="T22" id="Seg_1284" n="e" s="T21">hirge </ts>
               <ts e="T23" id="Seg_1286" n="e" s="T22">oloksuju͡okput", </ts>
               <ts e="T24" id="Seg_1288" n="e" s="T23">diːr. </ts>
               <ts e="T25" id="Seg_1290" n="e" s="T24">Onno </ts>
               <ts e="T26" id="Seg_1292" n="e" s="T25">tijen </ts>
               <ts e="T27" id="Seg_1294" n="e" s="T26">ologurbuttar, </ts>
               <ts e="T28" id="Seg_1296" n="e" s="T27">balagan </ts>
               <ts e="T29" id="Seg_1298" n="e" s="T28">tuttubuttar, </ts>
               <ts e="T30" id="Seg_1300" n="e" s="T29">horoktor — </ts>
               <ts e="T31" id="Seg_1302" n="e" s="T30">golomo. </ts>
               <ts e="T32" id="Seg_1304" n="e" s="T31">Üs </ts>
               <ts e="T33" id="Seg_1306" n="e" s="T32">ɨ͡al </ts>
               <ts e="T34" id="Seg_1308" n="e" s="T33">dʼulkaːk </ts>
               <ts e="T35" id="Seg_1310" n="e" s="T34">bu͡olbuttar. </ts>
               <ts e="T36" id="Seg_1312" n="e" s="T35">Kaččaga </ts>
               <ts e="T37" id="Seg_1314" n="e" s="T36">ere </ts>
               <ts e="T38" id="Seg_1316" n="e" s="T37">hapaːs </ts>
               <ts e="T39" id="Seg_1318" n="e" s="T38">ettere </ts>
               <ts e="T40" id="Seg_1320" n="e" s="T39">barammɨt, </ts>
               <ts e="T41" id="Seg_1322" n="e" s="T40">bu </ts>
               <ts e="T42" id="Seg_1324" n="e" s="T41">kihiler </ts>
               <ts e="T43" id="Seg_1326" n="e" s="T42">korgujbuttar. </ts>
               <ts e="T44" id="Seg_1328" n="e" s="T43">Tabalarɨn </ts>
               <ts e="T45" id="Seg_1330" n="e" s="T44">hi͡ekterin — </ts>
               <ts e="T46" id="Seg_1332" n="e" s="T45">tabalara </ts>
               <ts e="T47" id="Seg_1334" n="e" s="T46">aksɨːlaːk, </ts>
               <ts e="T48" id="Seg_1336" n="e" s="T47">elete </ts>
               <ts e="T49" id="Seg_1338" n="e" s="T48">bi͡es </ts>
               <ts e="T50" id="Seg_1340" n="e" s="T49">tü͡ört </ts>
               <ts e="T51" id="Seg_1342" n="e" s="T50">bu͡ollaga. </ts>
               <ts e="T52" id="Seg_1344" n="e" s="T51">Ogonnʼor </ts>
               <ts e="T53" id="Seg_1346" n="e" s="T52">hübeliːr: </ts>
               <ts e="T54" id="Seg_1348" n="e" s="T53">"Tababɨtɨn </ts>
               <ts e="T55" id="Seg_1350" n="e" s="T54">hi͡etekpitine, </ts>
               <ts e="T56" id="Seg_1352" n="e" s="T55">bu͡or </ts>
               <ts e="T57" id="Seg_1354" n="e" s="T56">dʼadaŋɨ </ts>
               <ts e="T58" id="Seg_1356" n="e" s="T57">bu͡olu͡okput. </ts>
               <ts e="T59" id="Seg_1358" n="e" s="T58">Hi͡emi͡egiŋ </ts>
               <ts e="T60" id="Seg_1360" n="e" s="T59">tabanɨ. </ts>
               <ts e="T61" id="Seg_1362" n="e" s="T60">Hübeli͡em: </ts>
               <ts e="T62" id="Seg_1364" n="e" s="T61">Munna </ts>
               <ts e="T63" id="Seg_1366" n="e" s="T62">bu </ts>
               <ts e="T64" id="Seg_1368" n="e" s="T63">ürekke </ts>
               <ts e="T65" id="Seg_1370" n="e" s="T64">ɨ͡allarbɨt </ts>
               <ts e="T66" id="Seg_1372" n="e" s="T65">uskaːttar </ts>
               <ts e="T67" id="Seg_1374" n="e" s="T66">baːllar. </ts>
               <ts e="T68" id="Seg_1376" n="e" s="T67">Onno </ts>
               <ts e="T69" id="Seg_1378" n="e" s="T68">ojunnaːktar. </ts>
               <ts e="T70" id="Seg_1380" n="e" s="T69">Bihigi </ts>
               <ts e="T71" id="Seg_1382" n="e" s="T70">albunnaːmmɨt </ts>
               <ts e="T72" id="Seg_1384" n="e" s="T71">munna </ts>
               <ts e="T73" id="Seg_1386" n="e" s="T72">egeli͡ekke </ts>
               <ts e="T74" id="Seg_1388" n="e" s="T73">ginileri. </ts>
               <ts e="T75" id="Seg_1390" n="e" s="T74">Min </ts>
               <ts e="T76" id="Seg_1392" n="e" s="T75">ɨ͡aldʼɨbɨt </ts>
               <ts e="T77" id="Seg_1394" n="e" s="T76">bu͡oluːm. </ts>
               <ts e="T78" id="Seg_1396" n="e" s="T77">Ojunnarɨn </ts>
               <ts e="T79" id="Seg_1398" n="e" s="T78">ɨgɨrɨŋ. </ts>
               <ts e="T80" id="Seg_1400" n="e" s="T79">Ginini </ts>
               <ts e="T81" id="Seg_1402" n="e" s="T80">kɨtta </ts>
               <ts e="T82" id="Seg_1404" n="e" s="T81">elbek </ts>
               <ts e="T83" id="Seg_1406" n="e" s="T82">uskaːn </ts>
               <ts e="T84" id="Seg_1408" n="e" s="T83">keli͡e. </ts>
               <ts e="T85" id="Seg_1410" n="e" s="T84">Dʼe </ts>
               <ts e="T86" id="Seg_1412" n="e" s="T85">oloru </ts>
               <ts e="T87" id="Seg_1414" n="e" s="T86">bihigi </ts>
               <ts e="T88" id="Seg_1416" n="e" s="T87">ölörtü͡ökpüt </ts>
               <ts e="T89" id="Seg_1418" n="e" s="T88">u͡onna </ts>
               <ts e="T90" id="Seg_1420" n="e" s="T89">hi͡ekpit. </ts>
               <ts e="T91" id="Seg_1422" n="e" s="T90">Oččogo </ts>
               <ts e="T92" id="Seg_1424" n="e" s="T91">inni </ts>
               <ts e="T93" id="Seg_1426" n="e" s="T92">di͡ekki </ts>
               <ts e="T94" id="Seg_1428" n="e" s="T93">atɨllɨ͡akpɨt, </ts>
               <ts e="T95" id="Seg_1430" n="e" s="T94">dʼɨl </ts>
               <ts e="T96" id="Seg_1432" n="e" s="T95">imijeriger </ts>
               <ts e="T97" id="Seg_1434" n="e" s="T96">tiji͡ekpit, </ts>
               <ts e="T98" id="Seg_1436" n="e" s="T97">bultanan </ts>
               <ts e="T99" id="Seg_1438" n="e" s="T98">ahɨ͡akpɨt." </ts>
               <ts e="T100" id="Seg_1440" n="e" s="T99">Kihilere </ts>
               <ts e="T101" id="Seg_1442" n="e" s="T100">uskaːnnarɨn </ts>
               <ts e="T102" id="Seg_1444" n="e" s="T101">kördüː </ts>
               <ts e="T103" id="Seg_1446" n="e" s="T102">bardɨlar. </ts>
               <ts e="T104" id="Seg_1448" n="e" s="T103">Ogonnʼor </ts>
               <ts e="T105" id="Seg_1450" n="e" s="T104">ɨ͡aldʼɨbɨta </ts>
               <ts e="T106" id="Seg_1452" n="e" s="T105">bu͡olla, </ts>
               <ts e="T107" id="Seg_1454" n="e" s="T106">ataktɨːn </ts>
               <ts e="T108" id="Seg_1456" n="e" s="T107">hu͡organ </ts>
               <ts e="T109" id="Seg_1458" n="e" s="T108">ihiger </ts>
               <ts e="T110" id="Seg_1460" n="e" s="T109">hɨtta. </ts>
               <ts e="T111" id="Seg_1462" n="e" s="T110">Emeːksiniger </ts>
               <ts e="T112" id="Seg_1464" n="e" s="T111">eter: </ts>
               <ts e="T113" id="Seg_1466" n="e" s="T112">"Uskaːn </ts>
               <ts e="T114" id="Seg_1468" n="e" s="T113">ojuna </ts>
               <ts e="T115" id="Seg_1470" n="e" s="T114">kelen </ts>
               <ts e="T116" id="Seg_1472" n="e" s="T115">kɨːrdagɨna, </ts>
               <ts e="T117" id="Seg_1474" n="e" s="T116">tünnükkün </ts>
               <ts e="T118" id="Seg_1476" n="e" s="T117">ü͡öleskin </ts>
               <ts e="T119" id="Seg_1478" n="e" s="T118">bü͡öleːr, </ts>
               <ts e="T120" id="Seg_1480" n="e" s="T119">aːŋŋɨn </ts>
               <ts e="T121" id="Seg_1482" n="e" s="T120">kaːjaːr." </ts>
               <ts e="T122" id="Seg_1484" n="e" s="T121">ɨnčɨktɨː-ɨnčɨktɨː </ts>
               <ts e="T123" id="Seg_1486" n="e" s="T122">ülügüdejbite </ts>
               <ts e="T124" id="Seg_1488" n="e" s="T123">bu͡olan </ts>
               <ts e="T330" id="Seg_1490" n="e" s="T124">"ɨː </ts>
               <ts e="T331" id="Seg_1492" n="e" s="T330">ɨː </ts>
               <ts e="T125" id="Seg_1494" n="e" s="T331">ɨ! </ts>
               <ts e="T126" id="Seg_1496" n="e" s="T125">Oldoːnbo", </ts>
               <ts e="T127" id="Seg_1498" n="e" s="T126">di͡egim, </ts>
               <ts e="T128" id="Seg_1500" n="e" s="T127">oččogo </ts>
               <ts e="T129" id="Seg_1502" n="e" s="T128">oldoːnu </ts>
               <ts e="T130" id="Seg_1504" n="e" s="T129">tuttaraːr </ts>
               <ts e="T132" id="Seg_1506" n="e" s="T130">mini͡eke. </ts>
               <ts e="T335" id="Seg_1508" n="e" s="T132">"ɨ-ɨ, </ts>
               <ts e="T133" id="Seg_1510" n="e" s="T335">godoroːbo", </ts>
               <ts e="T134" id="Seg_1512" n="e" s="T133">di͡egim, </ts>
               <ts e="T135" id="Seg_1514" n="e" s="T134">oččogo </ts>
               <ts e="T136" id="Seg_1516" n="e" s="T135">en </ts>
               <ts e="T137" id="Seg_1518" n="e" s="T136">gedereːni </ts>
               <ts e="T138" id="Seg_1520" n="e" s="T137">tutaːr </ts>
               <ts e="T139" id="Seg_1522" n="e" s="T138">aːŋŋa </ts>
               <ts e="T140" id="Seg_1524" n="e" s="T139">tohujaːr, </ts>
               <ts e="T141" id="Seg_1526" n="e" s="T140">oččogo </ts>
               <ts e="T142" id="Seg_1528" n="e" s="T141">min </ts>
               <ts e="T143" id="Seg_1530" n="e" s="T142">ojon </ts>
               <ts e="T144" id="Seg_1532" n="e" s="T143">turan </ts>
               <ts e="T145" id="Seg_1534" n="e" s="T144">čokujtaːn </ts>
               <ts e="T146" id="Seg_1536" n="e" s="T145">barɨ͡am, </ts>
               <ts e="T147" id="Seg_1538" n="e" s="T146">ojunnarɨn </ts>
               <ts e="T148" id="Seg_1540" n="e" s="T147">uruttu͡om. </ts>
               <ts e="T149" id="Seg_1542" n="e" s="T148">En </ts>
               <ts e="T150" id="Seg_1544" n="e" s="T149">emi͡e </ts>
               <ts e="T151" id="Seg_1546" n="e" s="T150">tuttumaktaːr." </ts>
               <ts e="T152" id="Seg_1548" n="e" s="T151">Ogonnʼor </ts>
               <ts e="T153" id="Seg_1550" n="e" s="T152">hɨtar, </ts>
               <ts e="T154" id="Seg_1552" n="e" s="T153">ɨnčɨktɨːr </ts>
               <ts e="T155" id="Seg_1554" n="e" s="T154">kihini </ts>
               <ts e="T156" id="Seg_1556" n="e" s="T155">bilbet </ts>
               <ts e="T157" id="Seg_1558" n="e" s="T156">bu͡ola-bu͡ola. </ts>
               <ts e="T158" id="Seg_1560" n="e" s="T157">Uskaːn </ts>
               <ts e="T159" id="Seg_1562" n="e" s="T158">ojuna </ts>
               <ts e="T160" id="Seg_1564" n="e" s="T159">keler, </ts>
               <ts e="T161" id="Seg_1566" n="e" s="T160">ginini </ts>
               <ts e="T162" id="Seg_1568" n="e" s="T161">kɨtta </ts>
               <ts e="T163" id="Seg_1570" n="e" s="T162">elbek </ts>
               <ts e="T164" id="Seg_1572" n="e" s="T163">uskaːn </ts>
               <ts e="T165" id="Seg_1574" n="e" s="T164">kelbit. </ts>
               <ts e="T166" id="Seg_1576" n="e" s="T165">Balagan </ts>
               <ts e="T167" id="Seg_1578" n="e" s="T166">ihe </ts>
               <ts e="T168" id="Seg_1580" n="e" s="T167">uskaːn </ts>
               <ts e="T169" id="Seg_1582" n="e" s="T168">agaj </ts>
               <ts e="T170" id="Seg_1584" n="e" s="T169">bu͡olbut. </ts>
               <ts e="T171" id="Seg_1586" n="e" s="T170">Emeːksin </ts>
               <ts e="T172" id="Seg_1588" n="e" s="T171">tünnügü </ts>
               <ts e="T173" id="Seg_1590" n="e" s="T172">ü͡ölehi </ts>
               <ts e="T174" id="Seg_1592" n="e" s="T173">bü͡öleːbit, </ts>
               <ts e="T175" id="Seg_1594" n="e" s="T174">aːnɨ </ts>
               <ts e="T176" id="Seg_1596" n="e" s="T175">kaːjbɨt. </ts>
               <ts e="T177" id="Seg_1598" n="e" s="T176">Uskaːn </ts>
               <ts e="T178" id="Seg_1600" n="e" s="T177">ojuna </ts>
               <ts e="T179" id="Seg_1602" n="e" s="T178">kɨːrbɨt, </ts>
               <ts e="T180" id="Seg_1604" n="e" s="T179">hotoru </ts>
               <ts e="T181" id="Seg_1606" n="e" s="T180">ekkireːbit. </ts>
               <ts e="T182" id="Seg_1608" n="e" s="T181">Ogonnʼor </ts>
               <ts e="T183" id="Seg_1610" n="e" s="T182">bergeːbit, </ts>
               <ts e="T184" id="Seg_1612" n="e" s="T183">ülügüdüjde </ts>
               <ts e="T332" id="Seg_1614" n="e" s="T184">"ɨ </ts>
               <ts e="T185" id="Seg_1616" n="e" s="T332">ɨ! </ts>
               <ts e="T186" id="Seg_1618" n="e" s="T185">Oldoːnbo", </ts>
               <ts e="T187" id="Seg_1620" n="e" s="T186">di͡ebit. </ts>
               <ts e="T188" id="Seg_1622" n="e" s="T187">Emeːksin </ts>
               <ts e="T189" id="Seg_1624" n="e" s="T188">ojon </ts>
               <ts e="T190" id="Seg_1626" n="e" s="T189">turan </ts>
               <ts e="T191" id="Seg_1628" n="e" s="T190">ogonnʼorgo </ts>
               <ts e="T192" id="Seg_1630" n="e" s="T191">oldoːnu </ts>
               <ts e="T193" id="Seg_1632" n="e" s="T192">tuttarbɨt. </ts>
               <ts e="T333" id="Seg_1634" n="e" s="T193">"ɨ </ts>
               <ts e="T194" id="Seg_1636" n="e" s="T333">ɨ! </ts>
               <ts e="T195" id="Seg_1638" n="e" s="T194">Godoroːbo", </ts>
               <ts e="T196" id="Seg_1640" n="e" s="T195">di͡ebit. </ts>
               <ts e="T334" id="Seg_1642" n="e" s="T196">"ɨ </ts>
               <ts e="T197" id="Seg_1644" n="e" s="T334">ɨ! </ts>
               <ts e="T198" id="Seg_1646" n="e" s="T197">Hükečaːnma!" </ts>
               <ts e="T199" id="Seg_1648" n="e" s="T198">Innʼe </ts>
               <ts e="T200" id="Seg_1650" n="e" s="T199">di͡etin </ts>
               <ts e="T201" id="Seg_1652" n="e" s="T200">emeːksine </ts>
               <ts e="T202" id="Seg_1654" n="e" s="T201">gedereːni </ts>
               <ts e="T203" id="Seg_1656" n="e" s="T202">kappɨt, </ts>
               <ts e="T204" id="Seg_1658" n="e" s="T203">dʼulkaːga </ts>
               <ts e="T205" id="Seg_1660" n="e" s="T204">hükečaːnɨ </ts>
               <ts e="T206" id="Seg_1662" n="e" s="T205">kappɨt. </ts>
               <ts e="T207" id="Seg_1664" n="e" s="T206">Ojunnara </ts>
               <ts e="T208" id="Seg_1666" n="e" s="T207">ekkiriː </ts>
               <ts e="T209" id="Seg_1668" n="e" s="T208">turar: </ts>
               <ts e="T210" id="Seg_1670" n="e" s="T209">"Eduk </ts>
               <ts e="T211" id="Seg_1672" n="e" s="T210">dʼuːduk </ts>
               <ts e="T212" id="Seg_1674" n="e" s="T211">judʼoŋoː </ts>
               <ts e="T213" id="Seg_1676" n="e" s="T212">bimiː! </ts>
               <ts e="T214" id="Seg_1678" n="e" s="T213">Paːpalʼdi͡ek", </ts>
               <ts e="T215" id="Seg_1680" n="e" s="T214">diː-diː. </ts>
               <ts e="T216" id="Seg_1682" n="e" s="T215">Ol </ts>
               <ts e="T217" id="Seg_1684" n="e" s="T216">aːta </ts>
               <ts e="T218" id="Seg_1686" n="e" s="T217">"Bu </ts>
               <ts e="T219" id="Seg_1688" n="e" s="T218">dʼi͡etten </ts>
               <ts e="T220" id="Seg_1690" n="e" s="T219">taksɨ͡ak </ts>
               <ts e="T221" id="Seg_1692" n="e" s="T220">bu͡olu͡ok </ts>
               <ts e="T223" id="Seg_1694" n="e" s="T221">bu͡ollarbɨn" </ts>
               <ts e="T224" id="Seg_1696" n="e" s="T223">di͡en </ts>
               <ts e="T225" id="Seg_1698" n="e" s="T224">toŋustuː. </ts>
               <ts e="T226" id="Seg_1700" n="e" s="T225">Ogonnʼor </ts>
               <ts e="T227" id="Seg_1702" n="e" s="T226">onu </ts>
               <ts e="T228" id="Seg_1704" n="e" s="T227">isteːt </ts>
               <ts e="T229" id="Seg_1706" n="e" s="T228">ojon </ts>
               <ts e="T230" id="Seg_1708" n="e" s="T229">turar, </ts>
               <ts e="T231" id="Seg_1710" n="e" s="T230">ojunnara </ts>
               <ts e="T232" id="Seg_1712" n="e" s="T231">ojbut </ts>
               <ts e="T233" id="Seg_1714" n="e" s="T232">ükeːptüːŋŋe. </ts>
               <ts e="T234" id="Seg_1716" n="e" s="T233">Ogonnʼor </ts>
               <ts e="T235" id="Seg_1718" n="e" s="T234">oldoːnunan </ts>
               <ts e="T236" id="Seg_1720" n="e" s="T235">ükeːptüːŋŋe </ts>
               <ts e="T237" id="Seg_1722" n="e" s="T236">kɨhaja </ts>
               <ts e="T238" id="Seg_1724" n="e" s="T237">oksor, </ts>
               <ts e="T239" id="Seg_1726" n="e" s="T238">ojun-uskaːn </ts>
               <ts e="T240" id="Seg_1728" n="e" s="T239">aharan </ts>
               <ts e="T241" id="Seg_1730" n="e" s="T240">bi͡erbit, </ts>
               <ts e="T242" id="Seg_1732" n="e" s="T241">ikki </ts>
               <ts e="T243" id="Seg_1734" n="e" s="T242">kulgaːgɨn </ts>
               <ts e="T244" id="Seg_1736" n="e" s="T243">ere </ts>
               <ts e="T245" id="Seg_1738" n="e" s="T244">melti </ts>
               <ts e="T246" id="Seg_1740" n="e" s="T245">oksubut. </ts>
               <ts e="T247" id="Seg_1742" n="e" s="T246">Uskaːn </ts>
               <ts e="T248" id="Seg_1744" n="e" s="T247">ojuna </ts>
               <ts e="T249" id="Seg_1746" n="e" s="T248">kɨs </ts>
               <ts e="T250" id="Seg_1748" n="e" s="T249">maska </ts>
               <ts e="T251" id="Seg_1750" n="e" s="T250">ojbut, </ts>
               <ts e="T252" id="Seg_1752" n="e" s="T251">ojoːtun </ts>
               <ts e="T253" id="Seg_1754" n="e" s="T252">emeːksin </ts>
               <ts e="T254" id="Seg_1756" n="e" s="T253">kedereːninen </ts>
               <ts e="T255" id="Seg_1758" n="e" s="T254">oksor, </ts>
               <ts e="T256" id="Seg_1760" n="e" s="T255">kelin </ts>
               <ts e="T257" id="Seg_1762" n="e" s="T256">agaj </ts>
               <ts e="T258" id="Seg_1764" n="e" s="T257">atagɨgar. </ts>
               <ts e="T259" id="Seg_1766" n="e" s="T258">Ikki </ts>
               <ts e="T260" id="Seg_1768" n="e" s="T259">kelin </ts>
               <ts e="T261" id="Seg_1770" n="e" s="T260">atagɨn </ts>
               <ts e="T262" id="Seg_1772" n="e" s="T261">tohutar. </ts>
               <ts e="T263" id="Seg_1774" n="e" s="T262">Uskaːn </ts>
               <ts e="T264" id="Seg_1776" n="e" s="T263">ojuna </ts>
               <ts e="T265" id="Seg_1778" n="e" s="T264">kɨs </ts>
               <ts e="T266" id="Seg_1780" n="e" s="T265">mastan </ts>
               <ts e="T267" id="Seg_1782" n="e" s="T266">ü͡ölehinen </ts>
               <ts e="T268" id="Seg_1784" n="e" s="T267">ojor, </ts>
               <ts e="T269" id="Seg_1786" n="e" s="T268">onuga </ts>
               <ts e="T270" id="Seg_1788" n="e" s="T269">uskaːn </ts>
               <ts e="T271" id="Seg_1790" n="e" s="T270">emeːksine </ts>
               <ts e="T272" id="Seg_1792" n="e" s="T271">emi͡e </ts>
               <ts e="T273" id="Seg_1794" n="e" s="T272">ü͡ölehinen </ts>
               <ts e="T274" id="Seg_1796" n="e" s="T273">ojbut. </ts>
               <ts e="T275" id="Seg_1798" n="e" s="T274">Dʼi͡ege </ts>
               <ts e="T276" id="Seg_1800" n="e" s="T275">baːr </ts>
               <ts e="T277" id="Seg_1802" n="e" s="T276">uskaːttarɨ </ts>
               <ts e="T278" id="Seg_1804" n="e" s="T277">barɨlarɨn </ts>
               <ts e="T279" id="Seg_1806" n="e" s="T278">imičči </ts>
               <ts e="T280" id="Seg_1808" n="e" s="T279">ölörtöːn </ts>
               <ts e="T281" id="Seg_1810" n="e" s="T280">keːspitter. </ts>
               <ts e="T282" id="Seg_1812" n="e" s="T281">Manɨ </ts>
               <ts e="T283" id="Seg_1814" n="e" s="T282">hi͡enner, </ts>
               <ts e="T284" id="Seg_1816" n="e" s="T283">haːs </ts>
               <ts e="T285" id="Seg_1818" n="e" s="T284">dʼɨl </ts>
               <ts e="T286" id="Seg_1820" n="e" s="T285">imijeriger </ts>
               <ts e="T287" id="Seg_1822" n="e" s="T286">tijbitter. </ts>
               <ts e="T288" id="Seg_1824" n="e" s="T287">Urut </ts>
               <ts e="T289" id="Seg_1826" n="e" s="T288">uskaːnɨ </ts>
               <ts e="T290" id="Seg_1828" n="e" s="T289">hi͡ebet </ts>
               <ts e="T291" id="Seg_1830" n="e" s="T290">ühüler. </ts>
               <ts e="T292" id="Seg_1832" n="e" s="T291">Hitinten </ts>
               <ts e="T293" id="Seg_1834" n="e" s="T292">dʼe </ts>
               <ts e="T294" id="Seg_1836" n="e" s="T293">uskaːnɨ </ts>
               <ts e="T295" id="Seg_1838" n="e" s="T294">hiːr </ts>
               <ts e="T296" id="Seg_1840" n="e" s="T295">bu͡olbuttar </ts>
               <ts e="T297" id="Seg_1842" n="e" s="T296">dʼonnor. </ts>
               <ts e="T298" id="Seg_1844" n="e" s="T297">Küreːbit </ts>
               <ts e="T299" id="Seg_1846" n="e" s="T298">uskaːn </ts>
               <ts e="T300" id="Seg_1848" n="e" s="T299">ojunuttan </ts>
               <ts e="T301" id="Seg_1850" n="e" s="T300">kɨːl </ts>
               <ts e="T302" id="Seg_1852" n="e" s="T301">uskaːn </ts>
               <ts e="T303" id="Seg_1854" n="e" s="T302">ü͡öskeːbit. </ts>
               <ts e="T304" id="Seg_1856" n="e" s="T303">Haŋata </ts>
               <ts e="T305" id="Seg_1858" n="e" s="T304">da </ts>
               <ts e="T306" id="Seg_1860" n="e" s="T305">hu͡ok </ts>
               <ts e="T307" id="Seg_1862" n="e" s="T306">bu͡olbut, </ts>
               <ts e="T308" id="Seg_1864" n="e" s="T307">dʼonton </ts>
               <ts e="T309" id="Seg_1866" n="e" s="T308">kürener </ts>
               <ts e="T310" id="Seg_1868" n="e" s="T309">bu͡olbut. </ts>
               <ts e="T311" id="Seg_1870" n="e" s="T310">Bili </ts>
               <ts e="T312" id="Seg_1872" n="e" s="T311">ogonnʼor </ts>
               <ts e="T313" id="Seg_1874" n="e" s="T312">oksubututtan </ts>
               <ts e="T314" id="Seg_1876" n="e" s="T313">uskaːn </ts>
               <ts e="T315" id="Seg_1878" n="e" s="T314">kulgaːgɨn </ts>
               <ts e="T316" id="Seg_1880" n="e" s="T315">töbötö </ts>
               <ts e="T317" id="Seg_1882" n="e" s="T316">üjetten </ts>
               <ts e="T318" id="Seg_1884" n="e" s="T317">üjege </ts>
               <ts e="T319" id="Seg_1886" n="e" s="T318">beli͡eleːk </ts>
               <ts e="T320" id="Seg_1888" n="e" s="T319">bu͡olbut; </ts>
               <ts e="T321" id="Seg_1890" n="e" s="T320">emeːksin </ts>
               <ts e="T322" id="Seg_1892" n="e" s="T321">tostu </ts>
               <ts e="T323" id="Seg_1894" n="e" s="T322">oksubut </ts>
               <ts e="T324" id="Seg_1896" n="e" s="T323">kelin </ts>
               <ts e="T325" id="Seg_1898" n="e" s="T324">ataga </ts>
               <ts e="T326" id="Seg_1900" n="e" s="T325">tuhunan </ts>
               <ts e="T327" id="Seg_1902" n="e" s="T326">hühü͡ök </ts>
               <ts e="T328" id="Seg_1904" n="e" s="T327">bu͡olbut. </ts>
               <ts e="T329" id="Seg_1906" n="e" s="T328">Elete. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_1907" s="T0">ErSV_1964_OldManHares_flk.001</ta>
            <ta e="T13" id="Seg_1908" s="T4">ErSV_1964_OldManHares_flk.002</ta>
            <ta e="T17" id="Seg_1909" s="T13">ErSV_1964_OldManHares_flk.003</ta>
            <ta e="T24" id="Seg_1910" s="T17">ErSV_1964_OldManHares_flk.004</ta>
            <ta e="T31" id="Seg_1911" s="T24">ErSV_1964_OldManHares_flk.005</ta>
            <ta e="T35" id="Seg_1912" s="T31">ErSV_1964_OldManHares_flk.006</ta>
            <ta e="T43" id="Seg_1913" s="T35">ErSV_1964_OldManHares_flk.007</ta>
            <ta e="T51" id="Seg_1914" s="T43">ErSV_1964_OldManHares_flk.008</ta>
            <ta e="T53" id="Seg_1915" s="T51">ErSV_1964_OldManHares_flk.009</ta>
            <ta e="T58" id="Seg_1916" s="T53">ErSV_1964_OldManHares_flk.010</ta>
            <ta e="T60" id="Seg_1917" s="T58">ErSV_1964_OldManHares_flk.011</ta>
            <ta e="T61" id="Seg_1918" s="T60">ErSV_1964_OldManHares_flk.012</ta>
            <ta e="T67" id="Seg_1919" s="T61">ErSV_1964_OldManHares_flk.013</ta>
            <ta e="T69" id="Seg_1920" s="T67">ErSV_1964_OldManHares_flk.014</ta>
            <ta e="T74" id="Seg_1921" s="T69">ErSV_1964_OldManHares_flk.015</ta>
            <ta e="T77" id="Seg_1922" s="T74">ErSV_1964_OldManHares_flk.016</ta>
            <ta e="T79" id="Seg_1923" s="T77">ErSV_1964_OldManHares_flk.017</ta>
            <ta e="T84" id="Seg_1924" s="T79">ErSV_1964_OldManHares_flk.018</ta>
            <ta e="T90" id="Seg_1925" s="T84">ErSV_1964_OldManHares_flk.019</ta>
            <ta e="T99" id="Seg_1926" s="T90">ErSV_1964_OldManHares_flk.020</ta>
            <ta e="T103" id="Seg_1927" s="T99">ErSV_1964_OldManHares_flk.021</ta>
            <ta e="T110" id="Seg_1928" s="T103">ErSV_1964_OldManHares_flk.022</ta>
            <ta e="T112" id="Seg_1929" s="T110">ErSV_1964_OldManHares_flk.023</ta>
            <ta e="T121" id="Seg_1930" s="T112">ErSV_1964_OldManHares_flk.024</ta>
            <ta e="T125" id="Seg_1931" s="T121">ErSV_1964_OldManHares_flk.025</ta>
            <ta e="T132" id="Seg_1932" s="T125">ErSV_1964_OldManHares_flk.026</ta>
            <ta e="T148" id="Seg_1933" s="T132">ErSV_1964_OldManHares_flk.027</ta>
            <ta e="T151" id="Seg_1934" s="T148">ErSV_1964_OldManHares_flk.028</ta>
            <ta e="T157" id="Seg_1935" s="T151">ErSV_1964_OldManHares_flk.029</ta>
            <ta e="T165" id="Seg_1936" s="T157">ErSV_1964_OldManHares_flk.030</ta>
            <ta e="T170" id="Seg_1937" s="T165">ErSV_1964_OldManHares_flk.031</ta>
            <ta e="T176" id="Seg_1938" s="T170">ErSV_1964_OldManHares_flk.032</ta>
            <ta e="T181" id="Seg_1939" s="T176">ErSV_1964_OldManHares_flk.033</ta>
            <ta e="T185" id="Seg_1940" s="T181">ErSV_1964_OldManHares_flk.034</ta>
            <ta e="T187" id="Seg_1941" s="T185">ErSV_1964_OldManHares_flk.035</ta>
            <ta e="T193" id="Seg_1942" s="T187">ErSV_1964_OldManHares_flk.036</ta>
            <ta e="T194" id="Seg_1943" s="T193">ErSV_1964_OldManHares_flk.037</ta>
            <ta e="T196" id="Seg_1944" s="T194">ErSV_1964_OldManHares_flk.038</ta>
            <ta e="T197" id="Seg_1945" s="T196">ErSV_1964_OldManHares_flk.039</ta>
            <ta e="T198" id="Seg_1946" s="T197">ErSV_1964_OldManHares_flk.040</ta>
            <ta e="T206" id="Seg_1947" s="T198">ErSV_1964_OldManHares_flk.041</ta>
            <ta e="T209" id="Seg_1948" s="T206">ErSV_1964_OldManHares_flk.042</ta>
            <ta e="T213" id="Seg_1949" s="T209">ErSV_1964_OldManHares_flk.043</ta>
            <ta e="T215" id="Seg_1950" s="T213">ErSV_1964_OldManHares_flk.044</ta>
            <ta e="T225" id="Seg_1951" s="T215">ErSV_1964_OldManHares_flk.045</ta>
            <ta e="T233" id="Seg_1952" s="T225">ErSV_1964_OldManHares_flk.046</ta>
            <ta e="T246" id="Seg_1953" s="T233">ErSV_1964_OldManHares_flk.047</ta>
            <ta e="T258" id="Seg_1954" s="T246">ErSV_1964_OldManHares_flk.048</ta>
            <ta e="T262" id="Seg_1955" s="T258">ErSV_1964_OldManHares_flk.049</ta>
            <ta e="T274" id="Seg_1956" s="T262">ErSV_1964_OldManHares_flk.050</ta>
            <ta e="T281" id="Seg_1957" s="T274">ErSV_1964_OldManHares_flk.051</ta>
            <ta e="T287" id="Seg_1958" s="T281">ErSV_1964_OldManHares_flk.052</ta>
            <ta e="T291" id="Seg_1959" s="T287">ErSV_1964_OldManHares_flk.053</ta>
            <ta e="T297" id="Seg_1960" s="T291">ErSV_1964_OldManHares_flk.054</ta>
            <ta e="T303" id="Seg_1961" s="T297">ErSV_1964_OldManHares_flk.055</ta>
            <ta e="T310" id="Seg_1962" s="T303">ErSV_1964_OldManHares_flk.056</ta>
            <ta e="T328" id="Seg_1963" s="T310">ErSV_1964_OldManHares_flk.057</ta>
            <ta e="T329" id="Seg_1964" s="T328">ErSV_1964_OldManHares_flk.058</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T4" id="Seg_1965" s="T0">Былыр дьон олорбут, тоҥустар.</ta>
            <ta e="T13" id="Seg_1966" s="T4">Биир огонньордооктор, бунтулара һүбэһит этэ, гини һүбэтин быспат этилэр.</ta>
            <ta e="T17" id="Seg_1967" s="T13">Кайа һиргэ ологуралларын һүбэлиир.</ta>
            <ta e="T24" id="Seg_1968" s="T17">— Мастаак, ичигэс һиргэ, оннук һиргэ олоксуйуокпут! — диир.</ta>
            <ta e="T31" id="Seg_1969" s="T24">Онно тийэн ологурбуттар, балаган туттубуттар, һороктор — голомо.</ta>
            <ta e="T35" id="Seg_1970" s="T31">Үс ыал дьулкаак буолбуттар.</ta>
            <ta e="T43" id="Seg_1971" s="T35">Каччага эрэ һапаас эттэрэ бараммыт, бу киһилэр коргуйбуттар.</ta>
            <ta e="T51" id="Seg_1972" s="T43">Табаларын һиэктэрин — табалара аксыылаак, элэтэ биэс-түөрт буоллага.</ta>
            <ta e="T53" id="Seg_1973" s="T51">Огонньор һүбэлиир: </ta>
            <ta e="T58" id="Seg_1974" s="T53">— Табабытын һиэтэкпитинэ, буор дьадаҥы буолуокпут.</ta>
            <ta e="T60" id="Seg_1975" s="T58">Һиэмиэгиҥ табаны.</ta>
            <ta e="T61" id="Seg_1976" s="T60">Һүбэлиэм.</ta>
            <ta e="T67" id="Seg_1977" s="T61">Мунна бу үрэккэ ыалларбыт ускааттар бааллар.</ta>
            <ta e="T69" id="Seg_1978" s="T67">Онно ойуннаактар.</ta>
            <ta e="T74" id="Seg_1979" s="T69">Биһиги албуннааммыт мунна эгэлиэккэ гинилэри.</ta>
            <ta e="T77" id="Seg_1980" s="T74">Мин ыалдьыбыт буолуум.</ta>
            <ta e="T79" id="Seg_1981" s="T77">Ойуннарын ыгырыҥ.</ta>
            <ta e="T84" id="Seg_1982" s="T79">Гинини кытта элбэк ускаан кэлиэ.</ta>
            <ta e="T90" id="Seg_1983" s="T84">Дьэ олору биһиги өлөртүөкпүт уонна һиэкпит.</ta>
            <ta e="T99" id="Seg_1984" s="T90">Оччого инни диэкки атыллыакпыт, дьыл имийэригэр тийиэкпит, бултанан аһыакпыт.</ta>
            <ta e="T103" id="Seg_1985" s="T99">Киһилэрэ ускааннарын көрдүү бардылар.</ta>
            <ta e="T110" id="Seg_1986" s="T103">Огонньор ыалдьыбыта буолла, атактыын һуорган иһигэр һытта.</ta>
            <ta e="T112" id="Seg_1987" s="T110">Эмээксинигэр этэр: </ta>
            <ta e="T121" id="Seg_1988" s="T112">— Ускаан ойуна кэлэн кыырдагына, түннүккүн-үөлэскин бүөлээр, ааҥҥын каайаар.</ta>
            <ta e="T125" id="Seg_1989" s="T121">Ынчыктыы-ынчыктыы үлүгүдэйбитэ буолан: "Ыы-ыы-ы!</ta>
            <ta e="T132" id="Seg_1990" s="T125">Олдоонбо!" — диэгим, оччого олдоону туттараар миниэкэ; "Ы-ы!</ta>
            <ta e="T148" id="Seg_1991" s="T132">Годорообо!" — диэгим, оччого эн гэдэрээни тутаар ааҥҥа тоһуйаар, оччого мин ойон туран чокуйтаан барыам, ойуннарын уруттуом.</ta>
            <ta e="T151" id="Seg_1992" s="T148">Эн эмиэ туттумактаар.</ta>
            <ta e="T157" id="Seg_1993" s="T151">Огонньор һытар, ынчыктыыр киһини билбэт буола-буола.</ta>
            <ta e="T165" id="Seg_1994" s="T157">Ускаан ойуна кэлэр, гинини кытта элбэк ускаан кэлбит.</ta>
            <ta e="T170" id="Seg_1995" s="T165">Балаган иһэ ускаан агай буолбут.</ta>
            <ta e="T176" id="Seg_1996" s="T170">Эмээксин түннүгү-үөлэһи бүөлээбит, ааны каайбыт.</ta>
            <ta e="T181" id="Seg_1997" s="T176">Ускаан ойуна кыырбыт, һотору эккирээбит.</ta>
            <ta e="T185" id="Seg_1998" s="T181">Огонньор бэргээбит, үлүгүдүйдэ: "Ы-ы!</ta>
            <ta e="T187" id="Seg_1999" s="T185">Олдоонбо!" диэбит.</ta>
            <ta e="T193" id="Seg_2000" s="T187">Эмээксин ойон туран огонньорго олдоону туттарбыт.</ta>
            <ta e="T194" id="Seg_2001" s="T193">"Ы-ы!</ta>
            <ta e="T196" id="Seg_2002" s="T194">Годорообо! — диэбит.</ta>
            <ta e="T197" id="Seg_2003" s="T196">— Ы-ы!</ta>
            <ta e="T198" id="Seg_2004" s="T197">Һүкэчаанма!"</ta>
            <ta e="T206" id="Seg_2005" s="T198">Инньэ диэтин эмээксинэ гэдэрээни каппыт, дьулкаага һүкэчааны каппыт.</ta>
            <ta e="T209" id="Seg_2006" s="T206">Ойуннара эккирии турар: </ta>
            <ta e="T213" id="Seg_2007" s="T209">— Эдук дьуудук юдьоҥоо бимии!</ta>
            <ta e="T215" id="Seg_2008" s="T213">Паапальдиэк!— дии-дии.</ta>
            <ta e="T225" id="Seg_2009" s="T215">Ол аата "Бу дьиэттэн таксыак буолуок буолларбын!" …диэн тоҥустуу.</ta>
            <ta e="T233" id="Seg_2010" s="T225">Огонньор ону истээт ойон турар, ойуннара ойбут үкээптүүҥҥэ.</ta>
            <ta e="T246" id="Seg_2011" s="T233">Огонньор олдоонунан үкээптүүҥҥэ кыһайа оксор, ойун-ускаан аһаран биэрбит, икки кулгаагын эрэ мэлти оксубут.</ta>
            <ta e="T258" id="Seg_2012" s="T246">Ускаан ойуна кыс маска ойбут, ойоотун эмээксин кэдэрээнинэн оксор, кэлин агай атагыгар.</ta>
            <ta e="T262" id="Seg_2013" s="T258">Икки кэлин атагын тоһутар.</ta>
            <ta e="T274" id="Seg_2014" s="T262">Ускаан ойуна кыс мастан үөлэһинэн ойор, онуга ускаан эмээксинэ эмиэ үөлэһинэн ойбут.</ta>
            <ta e="T281" id="Seg_2015" s="T274">Дьиэгэ баар ускааттары барыларын имиччи өлөртөөн кээспиттэр.</ta>
            <ta e="T287" id="Seg_2016" s="T281">Маны һиэннэр, һаас дьыл имийэригэр тийбиттэр.</ta>
            <ta e="T291" id="Seg_2017" s="T287">Урут ускааны һиэбэт үһүлэр.</ta>
            <ta e="T297" id="Seg_2018" s="T291">Һитинтэн дьэ ускааны һиир буолбуттар дьоннор.</ta>
            <ta e="T303" id="Seg_2019" s="T297">Күрээбит ускаан ойунуттан кыыл ускаан үөскээбит.</ta>
            <ta e="T310" id="Seg_2020" s="T303">Һаҥата да һуок буолбут, дьонтон күрэнэр буолбут.</ta>
            <ta e="T328" id="Seg_2021" s="T310">Били огонньор оксубутуттан ускаан кулгаагын төбөтө үйэттэн үйэгэ бэлиэлээк буолбут; эмээксин тосту оксубут кэлин атага туһунан һүһүөк буолбут.</ta>
            <ta e="T329" id="Seg_2022" s="T328">Элэтэ.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_2023" s="T0">Bɨlɨr dʼon olorbut, toŋustar. </ta>
            <ta e="T13" id="Seg_2024" s="T4">Biːr ogonnʼordoːktor, buntulara hübehit ete, gini hübetin bɨspat etiler. </ta>
            <ta e="T17" id="Seg_2025" s="T13">Kaja hirge ologurallarɨn hübeliːr. </ta>
            <ta e="T24" id="Seg_2026" s="T17">"Mastaːk, ičiges hirge, onnuk hirge oloksuju͡okput", diːr. </ta>
            <ta e="T31" id="Seg_2027" s="T24">Onno tijen ologurbuttar, balagan tuttubuttar, horoktor — golomo. </ta>
            <ta e="T35" id="Seg_2028" s="T31">Üs ɨ͡al dʼulkaːk bu͡olbuttar. </ta>
            <ta e="T43" id="Seg_2029" s="T35">Kaččaga ere hapaːs ettere barammɨt, bu kihiler korgujbuttar. </ta>
            <ta e="T51" id="Seg_2030" s="T43">Tabalarɨn hi͡ekterin — tabalara aksɨːlaːk, elete bi͡es tü͡ört bu͡ollaga. </ta>
            <ta e="T53" id="Seg_2031" s="T51">Ogonnʼor hübeliːr: </ta>
            <ta e="T58" id="Seg_2032" s="T53">"Tababɨtɨn hi͡etekpitine, bu͡or dʼadaŋɨ bu͡olu͡okput. </ta>
            <ta e="T60" id="Seg_2033" s="T58">Hi͡emi͡egiŋ tabanɨ. </ta>
            <ta e="T61" id="Seg_2034" s="T60">Hübeli͡em: </ta>
            <ta e="T67" id="Seg_2035" s="T61">Munna bu ürekke ɨ͡allarbɨt uskaːttar baːllar. </ta>
            <ta e="T69" id="Seg_2036" s="T67">Onno ojunnaːktar. </ta>
            <ta e="T74" id="Seg_2037" s="T69">Bihigi albunnaːmmɨt munna egeli͡ekke ginileri. </ta>
            <ta e="T77" id="Seg_2038" s="T74">Min ɨ͡aldʼɨbɨt bu͡oluːm. </ta>
            <ta e="T79" id="Seg_2039" s="T77">Ojunnarɨn ɨgɨrɨŋ. </ta>
            <ta e="T84" id="Seg_2040" s="T79">Ginini kɨtta elbek uskaːn keli͡e. </ta>
            <ta e="T90" id="Seg_2041" s="T84">Dʼe oloru bihigi ölörtü͡ökpüt u͡onna hi͡ekpit. </ta>
            <ta e="T99" id="Seg_2042" s="T90">Oččogo inni di͡ekki atɨllɨ͡akpɨt, dʼɨl imijeriger tiji͡ekpit, bultanan ahɨ͡akpɨt." </ta>
            <ta e="T103" id="Seg_2043" s="T99">Kihilere uskaːnnarɨn kördüː bardɨlar. </ta>
            <ta e="T110" id="Seg_2044" s="T103">Ogonnʼor ɨ͡aldʼɨbɨta bu͡olla, ataktɨːn hu͡organ ihiger hɨtta. </ta>
            <ta e="T112" id="Seg_2045" s="T110">Emeːksiniger eter: </ta>
            <ta e="T121" id="Seg_2046" s="T112">"Uskaːn ojuna kelen kɨːrdagɨna, tünnükkün ü͡öleskin bü͡öleːr, aːŋŋɨn kaːjaːr." </ta>
            <ta e="T125" id="Seg_2047" s="T121">ɨnčɨktɨː-ɨnčɨktɨː ülügüdejbite bu͡olan "ɨː ɨː ɨ! </ta>
            <ta e="T132" id="Seg_2048" s="T125">Oldoːnbo", di͡egim, oččogo oldoːnu tuttaraːr mini͡eke. </ta>
            <ta e="T148" id="Seg_2049" s="T132">"ɨ-ɨ, godoroːbo", di͡egim, oččogo en gedereːni tutaːr aːŋŋa tohujaːr, oččogo min ojon turan čokujtaːn barɨ͡am, ojunnarɨn uruttu͡om. </ta>
            <ta e="T151" id="Seg_2050" s="T148">En emi͡e tuttumaktaːr." </ta>
            <ta e="T157" id="Seg_2051" s="T151">Ogonnʼor hɨtar, ɨnčɨktɨːr kihini bilbet bu͡ola-bu͡ola. </ta>
            <ta e="T165" id="Seg_2052" s="T157">Uskaːn ojuna keler, ginini kɨtta elbek uskaːn kelbit. </ta>
            <ta e="T170" id="Seg_2053" s="T165">Balagan ihe uskaːn agaj bu͡olbut. </ta>
            <ta e="T176" id="Seg_2054" s="T170">Emeːksin tünnügü ü͡ölehi bü͡öleːbit, aːnɨ kaːjbɨt. </ta>
            <ta e="T181" id="Seg_2055" s="T176">Uskaːn ojuna kɨːrbɨt, hotoru ekkireːbit. </ta>
            <ta e="T185" id="Seg_2056" s="T181">Ogonnʼor bergeːbit, ülügüdüjde "ɨ ɨ! </ta>
            <ta e="T187" id="Seg_2057" s="T185">Oldoːnbo", di͡ebit. </ta>
            <ta e="T193" id="Seg_2058" s="T187">Emeːksin ojon turan ogonnʼorgo oldoːnu tuttarbɨt. </ta>
            <ta e="T194" id="Seg_2059" s="T193">"ɨ ɨ! </ta>
            <ta e="T196" id="Seg_2060" s="T194">Godoroːbo", di͡ebit. </ta>
            <ta e="T197" id="Seg_2061" s="T196">"ɨ ɨ! </ta>
            <ta e="T198" id="Seg_2062" s="T197">Hükečaːnma!" </ta>
            <ta e="T206" id="Seg_2063" s="T198">Innʼe di͡etin emeːksine gedereːni kappɨt, dʼulkaːga hükečaːnɨ kappɨt. </ta>
            <ta e="T209" id="Seg_2064" s="T206">Ojunnara ekkiriː turar: </ta>
            <ta e="T213" id="Seg_2065" s="T209">"Eduk dʼuːduk judʼoŋoː bimiː! </ta>
            <ta e="T215" id="Seg_2066" s="T213">Paːpalʼdi͡ek", diː-diː. </ta>
            <ta e="T225" id="Seg_2067" s="T215">Ol aːta "Bu dʼi͡etten taksɨ͡ak bu͡olu͡ok bu͡ollarbɨn" di͡en toŋustuː. </ta>
            <ta e="T233" id="Seg_2068" s="T225">Ogonnʼor onu isteːt ojon turar, ojunnara ojbut ükeːptüːŋŋe. </ta>
            <ta e="T246" id="Seg_2069" s="T233">Ogonnʼor oldoːnunan ükeːptüːŋŋe kɨhaja oksor, ojun-uskaːn aharan bi͡erbit, ikki kulgaːgɨn ere melti oksubut. </ta>
            <ta e="T258" id="Seg_2070" s="T246">Uskaːn ojuna kɨs maska ojbut, ojoːtun emeːksin kedereːninen oksor, kelin agaj atagɨgar. </ta>
            <ta e="T262" id="Seg_2071" s="T258">Ikki kelin atagɨn tohutar. </ta>
            <ta e="T274" id="Seg_2072" s="T262">Uskaːn ojuna kɨs mastan ü͡ölehinen ojor, onuga uskaːn emeːksine emi͡e ü͡ölehinen ojbut. </ta>
            <ta e="T281" id="Seg_2073" s="T274">Dʼi͡ege baːr uskaːttarɨ barɨlarɨn imičči ölörtöːn keːspitter. </ta>
            <ta e="T287" id="Seg_2074" s="T281">Manɨ hi͡enner, haːs dʼɨl imijeriger tijbitter. </ta>
            <ta e="T291" id="Seg_2075" s="T287">Urut uskaːnɨ hi͡ebet ühüler. </ta>
            <ta e="T297" id="Seg_2076" s="T291">Hitinten dʼe uskaːnɨ hiːr bu͡olbuttar dʼonnor. </ta>
            <ta e="T303" id="Seg_2077" s="T297">Küreːbit uskaːn ojunuttan kɨːl uskaːn ü͡öskeːbit. </ta>
            <ta e="T310" id="Seg_2078" s="T303">Haŋata da hu͡ok bu͡olbut, dʼonton kürener bu͡olbut. </ta>
            <ta e="T328" id="Seg_2079" s="T310">Bili ogonnʼor oksubututtan uskaːn kulgaːgɨn töbötö üjetten üjege beli͡eleːk bu͡olbut; emeːksin tostu oksubut kelin ataga tuhunan hühü͡ök bu͡olbut. </ta>
            <ta e="T329" id="Seg_2080" s="T328">Elete. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_2081" s="T0">bɨlɨr</ta>
            <ta e="T2" id="Seg_2082" s="T1">dʼon</ta>
            <ta e="T3" id="Seg_2083" s="T2">olor-but</ta>
            <ta e="T4" id="Seg_2084" s="T3">toŋus-tar</ta>
            <ta e="T5" id="Seg_2085" s="T4">biːr</ta>
            <ta e="T6" id="Seg_2086" s="T5">ogonnʼor-doːk-tor</ta>
            <ta e="T7" id="Seg_2087" s="T6">bun-tu-lara</ta>
            <ta e="T8" id="Seg_2088" s="T7">hübehit</ta>
            <ta e="T9" id="Seg_2089" s="T8">e-t-e</ta>
            <ta e="T10" id="Seg_2090" s="T9">gini</ta>
            <ta e="T11" id="Seg_2091" s="T10">hübe-ti-n</ta>
            <ta e="T12" id="Seg_2092" s="T11">bɨs-pat</ta>
            <ta e="T13" id="Seg_2093" s="T12">e-ti-ler</ta>
            <ta e="T14" id="Seg_2094" s="T13">kaja</ta>
            <ta e="T15" id="Seg_2095" s="T14">hir-ge</ta>
            <ta e="T16" id="Seg_2096" s="T15">ologur-al-larɨ-n</ta>
            <ta e="T17" id="Seg_2097" s="T16">hübeliː-r</ta>
            <ta e="T18" id="Seg_2098" s="T17">mas-taːk</ta>
            <ta e="T19" id="Seg_2099" s="T18">ičiges</ta>
            <ta e="T20" id="Seg_2100" s="T19">hir-ge</ta>
            <ta e="T21" id="Seg_2101" s="T20">onnuk</ta>
            <ta e="T22" id="Seg_2102" s="T21">hir-ge</ta>
            <ta e="T23" id="Seg_2103" s="T22">olok-suj-u͡ok-put</ta>
            <ta e="T24" id="Seg_2104" s="T23">diː-r</ta>
            <ta e="T25" id="Seg_2105" s="T24">onno</ta>
            <ta e="T26" id="Seg_2106" s="T25">tij-en</ta>
            <ta e="T27" id="Seg_2107" s="T26">ologur-but-tar</ta>
            <ta e="T28" id="Seg_2108" s="T27">balagan</ta>
            <ta e="T29" id="Seg_2109" s="T28">tutt-u-but-tar</ta>
            <ta e="T30" id="Seg_2110" s="T29">horok-tor</ta>
            <ta e="T31" id="Seg_2111" s="T30">golomo</ta>
            <ta e="T32" id="Seg_2112" s="T31">üs</ta>
            <ta e="T33" id="Seg_2113" s="T32">ɨ͡al</ta>
            <ta e="T34" id="Seg_2114" s="T33">dʼulkaːk</ta>
            <ta e="T35" id="Seg_2115" s="T34">bu͡ol-but-tar</ta>
            <ta e="T36" id="Seg_2116" s="T35">kaččaga</ta>
            <ta e="T37" id="Seg_2117" s="T36">ere</ta>
            <ta e="T38" id="Seg_2118" s="T37">hapaːs</ta>
            <ta e="T39" id="Seg_2119" s="T38">et-tere</ta>
            <ta e="T40" id="Seg_2120" s="T39">baram-mɨt</ta>
            <ta e="T41" id="Seg_2121" s="T40">bu</ta>
            <ta e="T42" id="Seg_2122" s="T41">kihi-ler</ta>
            <ta e="T43" id="Seg_2123" s="T42">korguj-but-tar</ta>
            <ta e="T44" id="Seg_2124" s="T43">taba-larɨ-n</ta>
            <ta e="T45" id="Seg_2125" s="T44">h-i͡ek-teri-n</ta>
            <ta e="T46" id="Seg_2126" s="T45">taba-lara</ta>
            <ta e="T47" id="Seg_2127" s="T46">aksɨːlaːk</ta>
            <ta e="T48" id="Seg_2128" s="T47">ele-te</ta>
            <ta e="T49" id="Seg_2129" s="T48">bi͡es</ta>
            <ta e="T50" id="Seg_2130" s="T49">tü͡ört</ta>
            <ta e="T51" id="Seg_2131" s="T50">bu͡ollaga</ta>
            <ta e="T52" id="Seg_2132" s="T51">ogonnʼor</ta>
            <ta e="T53" id="Seg_2133" s="T52">hübeliː-r</ta>
            <ta e="T54" id="Seg_2134" s="T53">taba-bɨtɨ-n</ta>
            <ta e="T55" id="Seg_2135" s="T54">hi͡e-tek-pitine</ta>
            <ta e="T56" id="Seg_2136" s="T55">bu͡or</ta>
            <ta e="T57" id="Seg_2137" s="T56">dʼadaŋɨ</ta>
            <ta e="T58" id="Seg_2138" s="T57">bu͡ol-u͡ok-put</ta>
            <ta e="T59" id="Seg_2139" s="T58">hi͡e-m-i͡egiŋ</ta>
            <ta e="T60" id="Seg_2140" s="T59">taba-nɨ</ta>
            <ta e="T61" id="Seg_2141" s="T60">hübel-i͡e-m</ta>
            <ta e="T62" id="Seg_2142" s="T61">munna</ta>
            <ta e="T63" id="Seg_2143" s="T62">bu</ta>
            <ta e="T64" id="Seg_2144" s="T63">ürek-ke</ta>
            <ta e="T65" id="Seg_2145" s="T64">ɨ͡al-lar-bɨt</ta>
            <ta e="T66" id="Seg_2146" s="T65">uskaːt-tar</ta>
            <ta e="T67" id="Seg_2147" s="T66">baːl-lar</ta>
            <ta e="T68" id="Seg_2148" s="T67">onno</ta>
            <ta e="T69" id="Seg_2149" s="T68">ojun-naːk-tar</ta>
            <ta e="T70" id="Seg_2150" s="T69">bihigi</ta>
            <ta e="T71" id="Seg_2151" s="T70">albunnaː-m-mɨt</ta>
            <ta e="T72" id="Seg_2152" s="T71">munna</ta>
            <ta e="T73" id="Seg_2153" s="T72">egel-i͡ek-ke</ta>
            <ta e="T74" id="Seg_2154" s="T73">giniler-i</ta>
            <ta e="T75" id="Seg_2155" s="T74">min</ta>
            <ta e="T76" id="Seg_2156" s="T75">ɨ͡aldʼ-ɨ-bɨt</ta>
            <ta e="T77" id="Seg_2157" s="T76">bu͡ol-uːm</ta>
            <ta e="T78" id="Seg_2158" s="T77">ojun-narɨ-n</ta>
            <ta e="T79" id="Seg_2159" s="T78">ɨgɨr-ɨ-ŋ</ta>
            <ta e="T80" id="Seg_2160" s="T79">gini-ni</ta>
            <ta e="T81" id="Seg_2161" s="T80">kɨtta</ta>
            <ta e="T82" id="Seg_2162" s="T81">elbek</ta>
            <ta e="T83" id="Seg_2163" s="T82">uskaːn</ta>
            <ta e="T84" id="Seg_2164" s="T83">kel-i͡e</ta>
            <ta e="T85" id="Seg_2165" s="T84">dʼe</ta>
            <ta e="T86" id="Seg_2166" s="T85">o-lor-u</ta>
            <ta e="T87" id="Seg_2167" s="T86">bihigi</ta>
            <ta e="T88" id="Seg_2168" s="T87">ölör-t-ü͡ök-püt</ta>
            <ta e="T89" id="Seg_2169" s="T88">u͡onna</ta>
            <ta e="T90" id="Seg_2170" s="T89">h-i͡ek-pit</ta>
            <ta e="T91" id="Seg_2171" s="T90">oččogo</ta>
            <ta e="T92" id="Seg_2172" s="T91">inni</ta>
            <ta e="T93" id="Seg_2173" s="T92">di͡ekki</ta>
            <ta e="T94" id="Seg_2174" s="T93">atɨll-ɨ͡ak-pɨt</ta>
            <ta e="T95" id="Seg_2175" s="T94">dʼɨl</ta>
            <ta e="T96" id="Seg_2176" s="T95">imij-er-i-ger</ta>
            <ta e="T97" id="Seg_2177" s="T96">tij-i͡ek-pit</ta>
            <ta e="T98" id="Seg_2178" s="T97">bult-a-nan</ta>
            <ta e="T99" id="Seg_2179" s="T98">ah-ɨ͡ak-pɨt</ta>
            <ta e="T100" id="Seg_2180" s="T99">kihi-lere</ta>
            <ta e="T101" id="Seg_2181" s="T100">uskaːn-narɨ-n</ta>
            <ta e="T102" id="Seg_2182" s="T101">körd-üː</ta>
            <ta e="T103" id="Seg_2183" s="T102">bar-dɨ-lar</ta>
            <ta e="T104" id="Seg_2184" s="T103">ogonnʼor</ta>
            <ta e="T105" id="Seg_2185" s="T104">ɨ͡aldʼ-ɨ-bɨt-a</ta>
            <ta e="T106" id="Seg_2186" s="T105">bu͡ol-l-a</ta>
            <ta e="T107" id="Seg_2187" s="T106">atak-tɨːn</ta>
            <ta e="T108" id="Seg_2188" s="T107">hu͡organ</ta>
            <ta e="T109" id="Seg_2189" s="T108">ih-i-ger</ta>
            <ta e="T110" id="Seg_2190" s="T109">hɨt-t-a</ta>
            <ta e="T111" id="Seg_2191" s="T110">emeːksin-i-ger</ta>
            <ta e="T112" id="Seg_2192" s="T111">et-er</ta>
            <ta e="T113" id="Seg_2193" s="T112">uskaːn</ta>
            <ta e="T114" id="Seg_2194" s="T113">ojun-a</ta>
            <ta e="T115" id="Seg_2195" s="T114">kel-en</ta>
            <ta e="T116" id="Seg_2196" s="T115">kɨːr-dag-ɨna</ta>
            <ta e="T117" id="Seg_2197" s="T116">tünnük-kü-n</ta>
            <ta e="T118" id="Seg_2198" s="T117">ü͡öles-ki-n</ta>
            <ta e="T119" id="Seg_2199" s="T118">bü͡öl-eːr</ta>
            <ta e="T120" id="Seg_2200" s="T119">aːŋ-ŋɨ-n</ta>
            <ta e="T121" id="Seg_2201" s="T120">kaːj-aːr</ta>
            <ta e="T122" id="Seg_2202" s="T121">ɨnčɨkt-ɨː-ɨnčɨkt-ɨː</ta>
            <ta e="T123" id="Seg_2203" s="T122">ülügüdej-bit-e</ta>
            <ta e="T124" id="Seg_2204" s="T123">bu͡ol-an</ta>
            <ta e="T330" id="Seg_2205" s="T124">ɨː</ta>
            <ta e="T331" id="Seg_2206" s="T330">ɨː</ta>
            <ta e="T125" id="Seg_2207" s="T331">ɨ</ta>
            <ta e="T127" id="Seg_2208" s="T126">d-i͡eg-i-m</ta>
            <ta e="T128" id="Seg_2209" s="T127">oččogo</ta>
            <ta e="T129" id="Seg_2210" s="T128">oldoːn-u</ta>
            <ta e="T130" id="Seg_2211" s="T129">tuttar-aːr</ta>
            <ta e="T132" id="Seg_2212" s="T130">mini͡e-ke</ta>
            <ta e="T335" id="Seg_2213" s="T132">ɨ-ɨ</ta>
            <ta e="T134" id="Seg_2214" s="T133">d-i͡eg-i-m</ta>
            <ta e="T135" id="Seg_2215" s="T134">oččogo</ta>
            <ta e="T136" id="Seg_2216" s="T135">en</ta>
            <ta e="T137" id="Seg_2217" s="T136">gedereː-ni</ta>
            <ta e="T138" id="Seg_2218" s="T137">tut-aːr</ta>
            <ta e="T139" id="Seg_2219" s="T138">aːŋ-ŋa</ta>
            <ta e="T140" id="Seg_2220" s="T139">tohuj-aːr</ta>
            <ta e="T141" id="Seg_2221" s="T140">oččogo</ta>
            <ta e="T142" id="Seg_2222" s="T141">min</ta>
            <ta e="T143" id="Seg_2223" s="T142">oj-on</ta>
            <ta e="T144" id="Seg_2224" s="T143">tur-an</ta>
            <ta e="T145" id="Seg_2225" s="T144">čokuj-taː-n</ta>
            <ta e="T146" id="Seg_2226" s="T145">bar-ɨ͡a-m</ta>
            <ta e="T147" id="Seg_2227" s="T146">ojun-narɨ-n</ta>
            <ta e="T148" id="Seg_2228" s="T147">urutt-u͡o-m</ta>
            <ta e="T149" id="Seg_2229" s="T148">en</ta>
            <ta e="T150" id="Seg_2230" s="T149">emi͡e</ta>
            <ta e="T151" id="Seg_2231" s="T150">tutt-u-makt-aːr</ta>
            <ta e="T152" id="Seg_2232" s="T151">ogonnʼor</ta>
            <ta e="T153" id="Seg_2233" s="T152">hɨt-ar</ta>
            <ta e="T154" id="Seg_2234" s="T153">ɨnčɨktɨː-r</ta>
            <ta e="T155" id="Seg_2235" s="T154">kihi-ni</ta>
            <ta e="T156" id="Seg_2236" s="T155">bil-bet</ta>
            <ta e="T157" id="Seg_2237" s="T156">bu͡ol-a-bu͡ol-a</ta>
            <ta e="T158" id="Seg_2238" s="T157">uskaːn</ta>
            <ta e="T159" id="Seg_2239" s="T158">ojun-a</ta>
            <ta e="T160" id="Seg_2240" s="T159">kel-er</ta>
            <ta e="T161" id="Seg_2241" s="T160">gini-ni</ta>
            <ta e="T162" id="Seg_2242" s="T161">kɨtta</ta>
            <ta e="T163" id="Seg_2243" s="T162">elbek</ta>
            <ta e="T164" id="Seg_2244" s="T163">uskaːn</ta>
            <ta e="T165" id="Seg_2245" s="T164">kel-bit</ta>
            <ta e="T166" id="Seg_2246" s="T165">balagan</ta>
            <ta e="T167" id="Seg_2247" s="T166">ih-e</ta>
            <ta e="T168" id="Seg_2248" s="T167">uskaːn</ta>
            <ta e="T169" id="Seg_2249" s="T168">agaj</ta>
            <ta e="T170" id="Seg_2250" s="T169">bu͡ol-but</ta>
            <ta e="T171" id="Seg_2251" s="T170">emeːksin</ta>
            <ta e="T172" id="Seg_2252" s="T171">tünnüg-ü</ta>
            <ta e="T173" id="Seg_2253" s="T172">ü͡öleh-i</ta>
            <ta e="T174" id="Seg_2254" s="T173">bü͡öleː-bit</ta>
            <ta e="T175" id="Seg_2255" s="T174">aːn-ɨ</ta>
            <ta e="T176" id="Seg_2256" s="T175">kaːj-bɨt</ta>
            <ta e="T177" id="Seg_2257" s="T176">uskaːn</ta>
            <ta e="T178" id="Seg_2258" s="T177">ojun-a</ta>
            <ta e="T179" id="Seg_2259" s="T178">kɨːr-bɨt</ta>
            <ta e="T180" id="Seg_2260" s="T179">hotoru</ta>
            <ta e="T181" id="Seg_2261" s="T180">ekkireː-bit</ta>
            <ta e="T182" id="Seg_2262" s="T181">ogonnʼor</ta>
            <ta e="T183" id="Seg_2263" s="T182">bergeː-bit</ta>
            <ta e="T184" id="Seg_2264" s="T183">ülügüdüjd-e</ta>
            <ta e="T332" id="Seg_2265" s="T184">ɨ</ta>
            <ta e="T185" id="Seg_2266" s="T332">ɨ</ta>
            <ta e="T187" id="Seg_2267" s="T186">di͡e-bit</ta>
            <ta e="T188" id="Seg_2268" s="T187">emeːksin</ta>
            <ta e="T189" id="Seg_2269" s="T188">oj-on</ta>
            <ta e="T190" id="Seg_2270" s="T189">tur-an</ta>
            <ta e="T191" id="Seg_2271" s="T190">ogonnʼor-go</ta>
            <ta e="T192" id="Seg_2272" s="T191">oldoːn-u</ta>
            <ta e="T193" id="Seg_2273" s="T192">tuttar-bɨt</ta>
            <ta e="T333" id="Seg_2274" s="T193">ɨ</ta>
            <ta e="T194" id="Seg_2275" s="T333">ɨ</ta>
            <ta e="T196" id="Seg_2276" s="T195">di͡e-bit</ta>
            <ta e="T334" id="Seg_2277" s="T196">ɨ</ta>
            <ta e="T197" id="Seg_2278" s="T334">ɨ</ta>
            <ta e="T199" id="Seg_2279" s="T198">innʼe</ta>
            <ta e="T200" id="Seg_2280" s="T199">di͡e-t-in</ta>
            <ta e="T201" id="Seg_2281" s="T200">emeːksin-e</ta>
            <ta e="T202" id="Seg_2282" s="T201">gedereː-ni</ta>
            <ta e="T203" id="Seg_2283" s="T202">kap-pɨt</ta>
            <ta e="T204" id="Seg_2284" s="T203">dʼulkaːg-a</ta>
            <ta e="T205" id="Seg_2285" s="T204">hükečaːn-ɨ</ta>
            <ta e="T206" id="Seg_2286" s="T205">kap-pɨt</ta>
            <ta e="T207" id="Seg_2287" s="T206">ojun-nara</ta>
            <ta e="T208" id="Seg_2288" s="T207">ekkir-iː</ta>
            <ta e="T209" id="Seg_2289" s="T208">tur-ar</ta>
            <ta e="T215" id="Seg_2290" s="T214">d-iː-d-iː</ta>
            <ta e="T216" id="Seg_2291" s="T215">ol</ta>
            <ta e="T217" id="Seg_2292" s="T216">aːt-a</ta>
            <ta e="T218" id="Seg_2293" s="T217">bu</ta>
            <ta e="T219" id="Seg_2294" s="T218">dʼi͡e-tten</ta>
            <ta e="T220" id="Seg_2295" s="T219">taks-ɨ͡ak</ta>
            <ta e="T221" id="Seg_2296" s="T220">bu͡ol-u͡ok</ta>
            <ta e="T223" id="Seg_2297" s="T221">bu͡ol-lar-bɨn</ta>
            <ta e="T224" id="Seg_2298" s="T223">di͡e-n</ta>
            <ta e="T225" id="Seg_2299" s="T224">toŋus-tuː</ta>
            <ta e="T226" id="Seg_2300" s="T225">ogonnʼor</ta>
            <ta e="T227" id="Seg_2301" s="T226">o-nu</ta>
            <ta e="T228" id="Seg_2302" s="T227">ist-eːt</ta>
            <ta e="T229" id="Seg_2303" s="T228">oj-on</ta>
            <ta e="T230" id="Seg_2304" s="T229">tur-ar</ta>
            <ta e="T231" id="Seg_2305" s="T230">ojun-nara</ta>
            <ta e="T232" id="Seg_2306" s="T231">oj-but</ta>
            <ta e="T233" id="Seg_2307" s="T232">ükeːptüːŋ-ŋe</ta>
            <ta e="T234" id="Seg_2308" s="T233">ogonnʼor</ta>
            <ta e="T235" id="Seg_2309" s="T234">oldoːn-u-nan</ta>
            <ta e="T236" id="Seg_2310" s="T235">ükeːptüːŋ-ŋe</ta>
            <ta e="T237" id="Seg_2311" s="T236">kɨhaj-a</ta>
            <ta e="T238" id="Seg_2312" s="T237">oks-or</ta>
            <ta e="T239" id="Seg_2313" s="T238">ojun-uskaːn</ta>
            <ta e="T240" id="Seg_2314" s="T239">ahar-an</ta>
            <ta e="T241" id="Seg_2315" s="T240">bi͡er-bit</ta>
            <ta e="T242" id="Seg_2316" s="T241">ikki</ta>
            <ta e="T243" id="Seg_2317" s="T242">kulgaːg-ɨ-n</ta>
            <ta e="T244" id="Seg_2318" s="T243">ere</ta>
            <ta e="T245" id="Seg_2319" s="T244">melti</ta>
            <ta e="T246" id="Seg_2320" s="T245">oks-u-but</ta>
            <ta e="T247" id="Seg_2321" s="T246">uskaːn</ta>
            <ta e="T248" id="Seg_2322" s="T247">ojun-a</ta>
            <ta e="T249" id="Seg_2323" s="T248">kɨs</ta>
            <ta e="T250" id="Seg_2324" s="T249">mas-ka</ta>
            <ta e="T251" id="Seg_2325" s="T250">oj-but</ta>
            <ta e="T252" id="Seg_2326" s="T251">oj-oːt-un</ta>
            <ta e="T253" id="Seg_2327" s="T252">emeːksin</ta>
            <ta e="T254" id="Seg_2328" s="T253">kedereːn-i-nen</ta>
            <ta e="T255" id="Seg_2329" s="T254">oks-or</ta>
            <ta e="T256" id="Seg_2330" s="T255">kelin</ta>
            <ta e="T257" id="Seg_2331" s="T256">agaj</ta>
            <ta e="T258" id="Seg_2332" s="T257">atag-ɨ-gar</ta>
            <ta e="T259" id="Seg_2333" s="T258">ikki</ta>
            <ta e="T260" id="Seg_2334" s="T259">kelin</ta>
            <ta e="T261" id="Seg_2335" s="T260">atag-ɨ-n</ta>
            <ta e="T262" id="Seg_2336" s="T261">tohut-ar</ta>
            <ta e="T263" id="Seg_2337" s="T262">uskaːn</ta>
            <ta e="T264" id="Seg_2338" s="T263">ojun-a</ta>
            <ta e="T265" id="Seg_2339" s="T264">kɨs</ta>
            <ta e="T266" id="Seg_2340" s="T265">mas-tan</ta>
            <ta e="T267" id="Seg_2341" s="T266">ü͡öleh-i-nen</ta>
            <ta e="T268" id="Seg_2342" s="T267">oj-or</ta>
            <ta e="T269" id="Seg_2343" s="T268">onu-ga</ta>
            <ta e="T270" id="Seg_2344" s="T269">uskaːn</ta>
            <ta e="T271" id="Seg_2345" s="T270">emeːksin-e</ta>
            <ta e="T272" id="Seg_2346" s="T271">emi͡e</ta>
            <ta e="T273" id="Seg_2347" s="T272">ü͡öleh-i-nen</ta>
            <ta e="T274" id="Seg_2348" s="T273">oj-but</ta>
            <ta e="T275" id="Seg_2349" s="T274">dʼi͡e-ge</ta>
            <ta e="T276" id="Seg_2350" s="T275">baːr</ta>
            <ta e="T277" id="Seg_2351" s="T276">uskaːt-tar-ɨ</ta>
            <ta e="T278" id="Seg_2352" s="T277">barɨ-larɨ-n</ta>
            <ta e="T279" id="Seg_2353" s="T278">imičči</ta>
            <ta e="T280" id="Seg_2354" s="T279">ölör-töː-n</ta>
            <ta e="T281" id="Seg_2355" s="T280">keːs-pit-ter</ta>
            <ta e="T282" id="Seg_2356" s="T281">ma-nɨ</ta>
            <ta e="T283" id="Seg_2357" s="T282">hi͡e-n-ner</ta>
            <ta e="T284" id="Seg_2358" s="T283">haːs</ta>
            <ta e="T285" id="Seg_2359" s="T284">dʼɨl</ta>
            <ta e="T286" id="Seg_2360" s="T285">imij-er-i-ger</ta>
            <ta e="T287" id="Seg_2361" s="T286">tij-bit-ter</ta>
            <ta e="T288" id="Seg_2362" s="T287">urut</ta>
            <ta e="T289" id="Seg_2363" s="T288">uskaːn-ɨ</ta>
            <ta e="T290" id="Seg_2364" s="T289">hi͡e-bet</ta>
            <ta e="T291" id="Seg_2365" s="T290">ühü-ler</ta>
            <ta e="T292" id="Seg_2366" s="T291">hitin-ten</ta>
            <ta e="T293" id="Seg_2367" s="T292">dʼe</ta>
            <ta e="T294" id="Seg_2368" s="T293">uskaːn-ɨ</ta>
            <ta e="T295" id="Seg_2369" s="T294">hiː-r</ta>
            <ta e="T296" id="Seg_2370" s="T295">bu͡ol-but-tar</ta>
            <ta e="T297" id="Seg_2371" s="T296">dʼon-nor</ta>
            <ta e="T298" id="Seg_2372" s="T297">küreː-bit</ta>
            <ta e="T299" id="Seg_2373" s="T298">uskaːn</ta>
            <ta e="T300" id="Seg_2374" s="T299">ojun-u-ttan</ta>
            <ta e="T301" id="Seg_2375" s="T300">kɨːl</ta>
            <ta e="T302" id="Seg_2376" s="T301">uskaːn</ta>
            <ta e="T303" id="Seg_2377" s="T302">ü͡öskeː-bit</ta>
            <ta e="T304" id="Seg_2378" s="T303">haŋa-ta</ta>
            <ta e="T305" id="Seg_2379" s="T304">da</ta>
            <ta e="T306" id="Seg_2380" s="T305">hu͡ok</ta>
            <ta e="T307" id="Seg_2381" s="T306">bu͡ol-but</ta>
            <ta e="T308" id="Seg_2382" s="T307">dʼon-ton</ta>
            <ta e="T309" id="Seg_2383" s="T308">küren-er</ta>
            <ta e="T310" id="Seg_2384" s="T309">bu͡ol-but</ta>
            <ta e="T311" id="Seg_2385" s="T310">bili</ta>
            <ta e="T312" id="Seg_2386" s="T311">ogonnʼor</ta>
            <ta e="T313" id="Seg_2387" s="T312">oks-u-but-u-ttan</ta>
            <ta e="T314" id="Seg_2388" s="T313">uskaːn</ta>
            <ta e="T315" id="Seg_2389" s="T314">kulgaːg-ɨ-n</ta>
            <ta e="T316" id="Seg_2390" s="T315">töbö-tö</ta>
            <ta e="T317" id="Seg_2391" s="T316">üje-tten</ta>
            <ta e="T318" id="Seg_2392" s="T317">üje-ge</ta>
            <ta e="T319" id="Seg_2393" s="T318">beli͡e-leːk</ta>
            <ta e="T320" id="Seg_2394" s="T319">bu͡ol-but</ta>
            <ta e="T321" id="Seg_2395" s="T320">emeːksin</ta>
            <ta e="T322" id="Seg_2396" s="T321">tostu</ta>
            <ta e="T323" id="Seg_2397" s="T322">oks-u-but</ta>
            <ta e="T324" id="Seg_2398" s="T323">kelin</ta>
            <ta e="T325" id="Seg_2399" s="T324">atag-a</ta>
            <ta e="T326" id="Seg_2400" s="T325">tuh-u-nan</ta>
            <ta e="T327" id="Seg_2401" s="T326">hühü͡ök</ta>
            <ta e="T328" id="Seg_2402" s="T327">bu͡ol-but</ta>
            <ta e="T329" id="Seg_2403" s="T328">ele-te</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_2404" s="T0">bɨlɨr</ta>
            <ta e="T2" id="Seg_2405" s="T1">dʼon</ta>
            <ta e="T3" id="Seg_2406" s="T2">olor-BIT</ta>
            <ta e="T4" id="Seg_2407" s="T3">toŋus-LAr</ta>
            <ta e="T5" id="Seg_2408" s="T4">biːr</ta>
            <ta e="T6" id="Seg_2409" s="T5">ogonnʼor-LAːK-LAr</ta>
            <ta e="T7" id="Seg_2410" s="T6">bu-tI-LArA</ta>
            <ta e="T8" id="Seg_2411" s="T7">hübehit</ta>
            <ta e="T9" id="Seg_2412" s="T8">e-TI-tA</ta>
            <ta e="T10" id="Seg_2413" s="T9">gini</ta>
            <ta e="T11" id="Seg_2414" s="T10">hübe-tI-n</ta>
            <ta e="T12" id="Seg_2415" s="T11">bɨs-BAT</ta>
            <ta e="T13" id="Seg_2416" s="T12">e-TI-LAr</ta>
            <ta e="T14" id="Seg_2417" s="T13">kaja</ta>
            <ta e="T15" id="Seg_2418" s="T14">hir-GA</ta>
            <ta e="T16" id="Seg_2419" s="T15">ologur-Ar-LArI-n</ta>
            <ta e="T17" id="Seg_2420" s="T16">hübeleː-Ar</ta>
            <ta e="T18" id="Seg_2421" s="T17">mas-LAːK</ta>
            <ta e="T19" id="Seg_2422" s="T18">ičiges</ta>
            <ta e="T20" id="Seg_2423" s="T19">hir-GA</ta>
            <ta e="T21" id="Seg_2424" s="T20">onnuk</ta>
            <ta e="T22" id="Seg_2425" s="T21">hir-GA</ta>
            <ta e="T23" id="Seg_2426" s="T22">olok-TIj-IAK-BIt</ta>
            <ta e="T24" id="Seg_2427" s="T23">di͡e-Ar</ta>
            <ta e="T25" id="Seg_2428" s="T24">onno</ta>
            <ta e="T26" id="Seg_2429" s="T25">tij-An</ta>
            <ta e="T27" id="Seg_2430" s="T26">ologur-BIT-LAr</ta>
            <ta e="T28" id="Seg_2431" s="T27">balagan</ta>
            <ta e="T29" id="Seg_2432" s="T28">tutun-I-BIT-LAr</ta>
            <ta e="T30" id="Seg_2433" s="T29">horok-LAr</ta>
            <ta e="T31" id="Seg_2434" s="T30">golomo</ta>
            <ta e="T32" id="Seg_2435" s="T31">üs</ta>
            <ta e="T33" id="Seg_2436" s="T32">ɨ͡al</ta>
            <ta e="T34" id="Seg_2437" s="T33">dʼulkaːk</ta>
            <ta e="T35" id="Seg_2438" s="T34">bu͡ol-BIT-LAr</ta>
            <ta e="T36" id="Seg_2439" s="T35">kaččaga</ta>
            <ta e="T37" id="Seg_2440" s="T36">ere</ta>
            <ta e="T38" id="Seg_2441" s="T37">hapaːs</ta>
            <ta e="T39" id="Seg_2442" s="T38">et-LArA</ta>
            <ta e="T40" id="Seg_2443" s="T39">baran-BIT</ta>
            <ta e="T41" id="Seg_2444" s="T40">bu</ta>
            <ta e="T42" id="Seg_2445" s="T41">kihi-LAr</ta>
            <ta e="T43" id="Seg_2446" s="T42">korguj-BIT-LAr</ta>
            <ta e="T44" id="Seg_2447" s="T43">taba-LArI-n</ta>
            <ta e="T45" id="Seg_2448" s="T44">hi͡e-IAK-LArI-n</ta>
            <ta e="T46" id="Seg_2449" s="T45">taba-LArA</ta>
            <ta e="T47" id="Seg_2450" s="T46">aksɨːlaːk</ta>
            <ta e="T48" id="Seg_2451" s="T47">ele-tA</ta>
            <ta e="T49" id="Seg_2452" s="T48">bi͡es</ta>
            <ta e="T50" id="Seg_2453" s="T49">tü͡ört</ta>
            <ta e="T51" id="Seg_2454" s="T50">bu͡ollaga</ta>
            <ta e="T52" id="Seg_2455" s="T51">ogonnʼor</ta>
            <ta e="T53" id="Seg_2456" s="T52">hübeleː-Ar</ta>
            <ta e="T54" id="Seg_2457" s="T53">taba-BItI-n</ta>
            <ta e="T55" id="Seg_2458" s="T54">hi͡e-TAK-BItInA</ta>
            <ta e="T56" id="Seg_2459" s="T55">bu͡or</ta>
            <ta e="T57" id="Seg_2460" s="T56">dʼadaŋɨ</ta>
            <ta e="T58" id="Seg_2461" s="T57">bu͡ol-IAK-BIt</ta>
            <ta e="T59" id="Seg_2462" s="T58">hi͡e-m-IAgIŋ</ta>
            <ta e="T60" id="Seg_2463" s="T59">taba-nI</ta>
            <ta e="T61" id="Seg_2464" s="T60">hübeleː-IAK-m</ta>
            <ta e="T62" id="Seg_2465" s="T61">manna</ta>
            <ta e="T63" id="Seg_2466" s="T62">bu</ta>
            <ta e="T64" id="Seg_2467" s="T63">ürek-GA</ta>
            <ta e="T65" id="Seg_2468" s="T64">ɨ͡al-LAr-BIt</ta>
            <ta e="T66" id="Seg_2469" s="T65">uskaːn-LAr</ta>
            <ta e="T67" id="Seg_2470" s="T66">baːr-LAr</ta>
            <ta e="T68" id="Seg_2471" s="T67">onno</ta>
            <ta e="T69" id="Seg_2472" s="T68">ojun-LAːK-LAr</ta>
            <ta e="T70" id="Seg_2473" s="T69">bihigi</ta>
            <ta e="T71" id="Seg_2474" s="T70">albunnaː-An-BIt</ta>
            <ta e="T72" id="Seg_2475" s="T71">manna</ta>
            <ta e="T73" id="Seg_2476" s="T72">egel-IAK-GA</ta>
            <ta e="T74" id="Seg_2477" s="T73">giniler-tI</ta>
            <ta e="T75" id="Seg_2478" s="T74">min</ta>
            <ta e="T76" id="Seg_2479" s="T75">ɨ͡arɨj-I-BIT</ta>
            <ta e="T77" id="Seg_2480" s="T76">bu͡ol-Iːm</ta>
            <ta e="T78" id="Seg_2481" s="T77">ojun-LArI-n</ta>
            <ta e="T79" id="Seg_2482" s="T78">ɨgɨr-I-ŋ</ta>
            <ta e="T80" id="Seg_2483" s="T79">gini-nI</ta>
            <ta e="T81" id="Seg_2484" s="T80">kɨtta</ta>
            <ta e="T82" id="Seg_2485" s="T81">elbek</ta>
            <ta e="T83" id="Seg_2486" s="T82">uskaːn</ta>
            <ta e="T84" id="Seg_2487" s="T83">kel-IAK.[tA]</ta>
            <ta e="T85" id="Seg_2488" s="T84">dʼe</ta>
            <ta e="T86" id="Seg_2489" s="T85">ol-LAr-nI</ta>
            <ta e="T87" id="Seg_2490" s="T86">bihigi</ta>
            <ta e="T88" id="Seg_2491" s="T87">ölör-t-IAK-BIt</ta>
            <ta e="T89" id="Seg_2492" s="T88">u͡onna</ta>
            <ta e="T90" id="Seg_2493" s="T89">hi͡e-IAK-BIt</ta>
            <ta e="T91" id="Seg_2494" s="T90">oččogo</ta>
            <ta e="T92" id="Seg_2495" s="T91">ilin</ta>
            <ta e="T93" id="Seg_2496" s="T92">di͡ekki</ta>
            <ta e="T94" id="Seg_2497" s="T93">atɨllaː-IAK-BIt</ta>
            <ta e="T95" id="Seg_2498" s="T94">dʼɨl</ta>
            <ta e="T96" id="Seg_2499" s="T95">imij-Ar-tI-GAr</ta>
            <ta e="T97" id="Seg_2500" s="T96">tij-IAK-BIt</ta>
            <ta e="T98" id="Seg_2501" s="T97">bult-A-nAn</ta>
            <ta e="T99" id="Seg_2502" s="T98">ahaː-IAK-BIt</ta>
            <ta e="T100" id="Seg_2503" s="T99">kihi-LArA</ta>
            <ta e="T101" id="Seg_2504" s="T100">uskaːn-LArI-n</ta>
            <ta e="T102" id="Seg_2505" s="T101">kördöː-A</ta>
            <ta e="T103" id="Seg_2506" s="T102">bar-TI-LAr</ta>
            <ta e="T104" id="Seg_2507" s="T103">ogonnʼor</ta>
            <ta e="T105" id="Seg_2508" s="T104">ɨ͡arɨj-I-BIT-tA</ta>
            <ta e="T106" id="Seg_2509" s="T105">bu͡ol-TI-tA</ta>
            <ta e="T107" id="Seg_2510" s="T106">atak-LIːN</ta>
            <ta e="T108" id="Seg_2511" s="T107">hu͡organ</ta>
            <ta e="T109" id="Seg_2512" s="T108">is-tI-GAr</ta>
            <ta e="T110" id="Seg_2513" s="T109">hɨt-TI-tA</ta>
            <ta e="T111" id="Seg_2514" s="T110">emeːksin-tI-GAr</ta>
            <ta e="T112" id="Seg_2515" s="T111">et-Ar</ta>
            <ta e="T113" id="Seg_2516" s="T112">uskaːn</ta>
            <ta e="T114" id="Seg_2517" s="T113">ojun-tA</ta>
            <ta e="T115" id="Seg_2518" s="T114">kel-An</ta>
            <ta e="T116" id="Seg_2519" s="T115">kɨːr-TAK-InA</ta>
            <ta e="T117" id="Seg_2520" s="T116">tünnük-GI-n</ta>
            <ta e="T118" id="Seg_2521" s="T117">ü͡öles-GI-n</ta>
            <ta e="T119" id="Seg_2522" s="T118">bü͡öleː-Aːr</ta>
            <ta e="T120" id="Seg_2523" s="T119">aːn-GI-n</ta>
            <ta e="T121" id="Seg_2524" s="T120">kaːj-Aːr</ta>
            <ta e="T122" id="Seg_2525" s="T121">ɨnčɨktaː-A-ɨnčɨktaː-A</ta>
            <ta e="T123" id="Seg_2526" s="T122">ülügüdej-BIT-tA</ta>
            <ta e="T124" id="Seg_2527" s="T123">bu͡ol-An</ta>
            <ta e="T330" id="Seg_2528" s="T124">ɨː</ta>
            <ta e="T331" id="Seg_2529" s="T330">ɨː</ta>
            <ta e="T125" id="Seg_2530" s="T331">ɨ</ta>
            <ta e="T127" id="Seg_2531" s="T126">di͡e-IAK-I-m</ta>
            <ta e="T128" id="Seg_2532" s="T127">oččogo</ta>
            <ta e="T129" id="Seg_2533" s="T128">oldoːn-nI</ta>
            <ta e="T130" id="Seg_2534" s="T129">tuttar-Aːr</ta>
            <ta e="T132" id="Seg_2535" s="T130">min-GA</ta>
            <ta e="T335" id="Seg_2536" s="T132">ɨ-ɨ</ta>
            <ta e="T134" id="Seg_2537" s="T133">di͡e-IAK-I-m</ta>
            <ta e="T135" id="Seg_2538" s="T134">oččogo</ta>
            <ta e="T136" id="Seg_2539" s="T135">en</ta>
            <ta e="T137" id="Seg_2540" s="T136">gedereː-nI</ta>
            <ta e="T138" id="Seg_2541" s="T137">tut-Aːr</ta>
            <ta e="T139" id="Seg_2542" s="T138">aːn-GA</ta>
            <ta e="T140" id="Seg_2543" s="T139">tohuj-Aːr</ta>
            <ta e="T141" id="Seg_2544" s="T140">oččogo</ta>
            <ta e="T142" id="Seg_2545" s="T141">min</ta>
            <ta e="T143" id="Seg_2546" s="T142">oj-An</ta>
            <ta e="T144" id="Seg_2547" s="T143">tur-An</ta>
            <ta e="T145" id="Seg_2548" s="T144">čokuj-TAː-An</ta>
            <ta e="T146" id="Seg_2549" s="T145">bar-IAK-m</ta>
            <ta e="T147" id="Seg_2550" s="T146">ojun-LArI-n</ta>
            <ta e="T148" id="Seg_2551" s="T147">uruttaː-IAK-m</ta>
            <ta e="T149" id="Seg_2552" s="T148">en</ta>
            <ta e="T150" id="Seg_2553" s="T149">emi͡e</ta>
            <ta e="T151" id="Seg_2554" s="T150">tutun-I-BAktAː-Aːr</ta>
            <ta e="T152" id="Seg_2555" s="T151">ogonnʼor</ta>
            <ta e="T153" id="Seg_2556" s="T152">hɨt-Ar</ta>
            <ta e="T154" id="Seg_2557" s="T153">ɨnčɨktaː-Ar</ta>
            <ta e="T155" id="Seg_2558" s="T154">kihi-nI</ta>
            <ta e="T156" id="Seg_2559" s="T155">bil-BAT</ta>
            <ta e="T157" id="Seg_2560" s="T156">bu͡ol-A-bu͡ol-A</ta>
            <ta e="T158" id="Seg_2561" s="T157">uskaːn</ta>
            <ta e="T159" id="Seg_2562" s="T158">ojun-tA</ta>
            <ta e="T160" id="Seg_2563" s="T159">kel-Ar</ta>
            <ta e="T161" id="Seg_2564" s="T160">gini-nI</ta>
            <ta e="T162" id="Seg_2565" s="T161">kɨtta</ta>
            <ta e="T163" id="Seg_2566" s="T162">elbek</ta>
            <ta e="T164" id="Seg_2567" s="T163">uskaːn</ta>
            <ta e="T165" id="Seg_2568" s="T164">kel-BIT</ta>
            <ta e="T166" id="Seg_2569" s="T165">balagan</ta>
            <ta e="T167" id="Seg_2570" s="T166">is-tA</ta>
            <ta e="T168" id="Seg_2571" s="T167">uskaːn</ta>
            <ta e="T169" id="Seg_2572" s="T168">agaj</ta>
            <ta e="T170" id="Seg_2573" s="T169">bu͡ol-BIT</ta>
            <ta e="T171" id="Seg_2574" s="T170">emeːksin</ta>
            <ta e="T172" id="Seg_2575" s="T171">tünnük-nI</ta>
            <ta e="T173" id="Seg_2576" s="T172">ü͡öles-nI</ta>
            <ta e="T174" id="Seg_2577" s="T173">bü͡öleː-BIT</ta>
            <ta e="T175" id="Seg_2578" s="T174">aːn-nI</ta>
            <ta e="T176" id="Seg_2579" s="T175">kaːj-BIT</ta>
            <ta e="T177" id="Seg_2580" s="T176">uskaːn</ta>
            <ta e="T178" id="Seg_2581" s="T177">ojun-tA</ta>
            <ta e="T179" id="Seg_2582" s="T178">kɨːr-BIT</ta>
            <ta e="T180" id="Seg_2583" s="T179">hotoru</ta>
            <ta e="T181" id="Seg_2584" s="T180">ekkireː-BIT</ta>
            <ta e="T182" id="Seg_2585" s="T181">ogonnʼor</ta>
            <ta e="T183" id="Seg_2586" s="T182">bergeː-BIT</ta>
            <ta e="T184" id="Seg_2587" s="T183">ülügüdej-A</ta>
            <ta e="T332" id="Seg_2588" s="T184">ɨ</ta>
            <ta e="T185" id="Seg_2589" s="T332">ɨ</ta>
            <ta e="T187" id="Seg_2590" s="T186">di͡e-BIT</ta>
            <ta e="T188" id="Seg_2591" s="T187">emeːksin</ta>
            <ta e="T189" id="Seg_2592" s="T188">oj-An</ta>
            <ta e="T190" id="Seg_2593" s="T189">tur-An</ta>
            <ta e="T191" id="Seg_2594" s="T190">ogonnʼor-GA</ta>
            <ta e="T192" id="Seg_2595" s="T191">oldoːn-nI</ta>
            <ta e="T193" id="Seg_2596" s="T192">tuttar-BIT</ta>
            <ta e="T333" id="Seg_2597" s="T193">ɨ</ta>
            <ta e="T194" id="Seg_2598" s="T333">ɨ</ta>
            <ta e="T196" id="Seg_2599" s="T195">di͡e-BIT</ta>
            <ta e="T334" id="Seg_2600" s="T196">ɨ</ta>
            <ta e="T197" id="Seg_2601" s="T334">ɨ</ta>
            <ta e="T199" id="Seg_2602" s="T198">innʼe</ta>
            <ta e="T200" id="Seg_2603" s="T199">di͡e-AːT-In</ta>
            <ta e="T201" id="Seg_2604" s="T200">emeːksin-tA</ta>
            <ta e="T202" id="Seg_2605" s="T201">gedereː-nI</ta>
            <ta e="T203" id="Seg_2606" s="T202">kap-BIT</ta>
            <ta e="T204" id="Seg_2607" s="T203">dʼulkaːk-tA</ta>
            <ta e="T205" id="Seg_2608" s="T204">hükečaːn-nI</ta>
            <ta e="T206" id="Seg_2609" s="T205">kap-BIT</ta>
            <ta e="T207" id="Seg_2610" s="T206">ojun-LArA</ta>
            <ta e="T208" id="Seg_2611" s="T207">ekkireː-A</ta>
            <ta e="T209" id="Seg_2612" s="T208">tur-Ar</ta>
            <ta e="T215" id="Seg_2613" s="T214">di͡e-A-di͡e-A</ta>
            <ta e="T216" id="Seg_2614" s="T215">ol</ta>
            <ta e="T217" id="Seg_2615" s="T216">aːt-tA</ta>
            <ta e="T218" id="Seg_2616" s="T217">bu</ta>
            <ta e="T219" id="Seg_2617" s="T218">dʼi͡e-ttAn</ta>
            <ta e="T220" id="Seg_2618" s="T219">tagɨs-IAK</ta>
            <ta e="T221" id="Seg_2619" s="T220">bu͡ol-IAK</ta>
            <ta e="T223" id="Seg_2620" s="T221">bu͡ol-TAR-BIn</ta>
            <ta e="T224" id="Seg_2621" s="T223">di͡e-An</ta>
            <ta e="T225" id="Seg_2622" s="T224">toŋus-LIː</ta>
            <ta e="T226" id="Seg_2623" s="T225">ogonnʼor</ta>
            <ta e="T227" id="Seg_2624" s="T226">ol-nI</ta>
            <ta e="T228" id="Seg_2625" s="T227">ihit-AːT</ta>
            <ta e="T229" id="Seg_2626" s="T228">oj-An</ta>
            <ta e="T230" id="Seg_2627" s="T229">tur-Ar</ta>
            <ta e="T231" id="Seg_2628" s="T230">ojun-LArA</ta>
            <ta e="T232" id="Seg_2629" s="T231">oj-BIT</ta>
            <ta e="T233" id="Seg_2630" s="T232">ükeːptüːn-GA</ta>
            <ta e="T234" id="Seg_2631" s="T233">ogonnʼor</ta>
            <ta e="T235" id="Seg_2632" s="T234">oldoːn-tI-nAn</ta>
            <ta e="T236" id="Seg_2633" s="T235">ükeːptüːn-GA</ta>
            <ta e="T237" id="Seg_2634" s="T236">kɨhaj-A</ta>
            <ta e="T238" id="Seg_2635" s="T237">ogus-Ar</ta>
            <ta e="T239" id="Seg_2636" s="T238">ojun-uskaːn</ta>
            <ta e="T240" id="Seg_2637" s="T239">ahar-An</ta>
            <ta e="T241" id="Seg_2638" s="T240">bi͡er-BIT</ta>
            <ta e="T242" id="Seg_2639" s="T241">ikki</ta>
            <ta e="T243" id="Seg_2640" s="T242">kulgaːk-tI-n</ta>
            <ta e="T244" id="Seg_2641" s="T243">ere</ta>
            <ta e="T245" id="Seg_2642" s="T244">meldʼi</ta>
            <ta e="T246" id="Seg_2643" s="T245">ogus-I-BIT</ta>
            <ta e="T247" id="Seg_2644" s="T246">uskaːn</ta>
            <ta e="T248" id="Seg_2645" s="T247">ojun-tA</ta>
            <ta e="T249" id="Seg_2646" s="T248">kɨs</ta>
            <ta e="T250" id="Seg_2647" s="T249">mas-GA</ta>
            <ta e="T251" id="Seg_2648" s="T250">oj-BIT</ta>
            <ta e="T252" id="Seg_2649" s="T251">oj-AːT-In</ta>
            <ta e="T253" id="Seg_2650" s="T252">emeːksin</ta>
            <ta e="T254" id="Seg_2651" s="T253">gedereː-tI-nAn</ta>
            <ta e="T255" id="Seg_2652" s="T254">ogus-Ar</ta>
            <ta e="T256" id="Seg_2653" s="T255">kelin</ta>
            <ta e="T257" id="Seg_2654" s="T256">agaj</ta>
            <ta e="T258" id="Seg_2655" s="T257">atak-tI-GAr</ta>
            <ta e="T259" id="Seg_2656" s="T258">ikki</ta>
            <ta e="T260" id="Seg_2657" s="T259">kelin</ta>
            <ta e="T261" id="Seg_2658" s="T260">atak-tI-n</ta>
            <ta e="T262" id="Seg_2659" s="T261">tohut-Ar</ta>
            <ta e="T263" id="Seg_2660" s="T262">uskaːn</ta>
            <ta e="T264" id="Seg_2661" s="T263">ojun-tA</ta>
            <ta e="T265" id="Seg_2662" s="T264">kɨs</ta>
            <ta e="T266" id="Seg_2663" s="T265">mas-ttAn</ta>
            <ta e="T267" id="Seg_2664" s="T266">ü͡öles-tI-nAn</ta>
            <ta e="T268" id="Seg_2665" s="T267">oj-Ar</ta>
            <ta e="T269" id="Seg_2666" s="T268">ol-GA</ta>
            <ta e="T270" id="Seg_2667" s="T269">uskaːn</ta>
            <ta e="T271" id="Seg_2668" s="T270">emeːksin-tA</ta>
            <ta e="T272" id="Seg_2669" s="T271">emi͡e</ta>
            <ta e="T273" id="Seg_2670" s="T272">ü͡öles-tI-nAn</ta>
            <ta e="T274" id="Seg_2671" s="T273">oj-BIT</ta>
            <ta e="T275" id="Seg_2672" s="T274">dʼi͡e-GA</ta>
            <ta e="T276" id="Seg_2673" s="T275">baːr</ta>
            <ta e="T277" id="Seg_2674" s="T276">uskaːn-LAr-nI</ta>
            <ta e="T278" id="Seg_2675" s="T277">barɨ-LArI-n</ta>
            <ta e="T279" id="Seg_2676" s="T278">imičči</ta>
            <ta e="T280" id="Seg_2677" s="T279">ölör-TAː-An</ta>
            <ta e="T281" id="Seg_2678" s="T280">keːs-BIT-LAr</ta>
            <ta e="T282" id="Seg_2679" s="T281">bu-nI</ta>
            <ta e="T283" id="Seg_2680" s="T282">hi͡e-An-LAr</ta>
            <ta e="T284" id="Seg_2681" s="T283">haːs</ta>
            <ta e="T285" id="Seg_2682" s="T284">dʼɨl</ta>
            <ta e="T286" id="Seg_2683" s="T285">imij-Ar-tI-GAr</ta>
            <ta e="T287" id="Seg_2684" s="T286">tij-BIT-LAr</ta>
            <ta e="T288" id="Seg_2685" s="T287">urut</ta>
            <ta e="T289" id="Seg_2686" s="T288">uskaːn-nI</ta>
            <ta e="T290" id="Seg_2687" s="T289">hi͡e-BAT</ta>
            <ta e="T291" id="Seg_2688" s="T290">ühü-LAr</ta>
            <ta e="T292" id="Seg_2689" s="T291">hiti-ttAn</ta>
            <ta e="T293" id="Seg_2690" s="T292">dʼe</ta>
            <ta e="T294" id="Seg_2691" s="T293">uskaːn-nI</ta>
            <ta e="T295" id="Seg_2692" s="T294">hi͡e-Ar</ta>
            <ta e="T296" id="Seg_2693" s="T295">bu͡ol-BIT-LAr</ta>
            <ta e="T297" id="Seg_2694" s="T296">dʼon-LAr</ta>
            <ta e="T298" id="Seg_2695" s="T297">küreː-BIT</ta>
            <ta e="T299" id="Seg_2696" s="T298">uskaːn</ta>
            <ta e="T300" id="Seg_2697" s="T299">ojun-tI-ttAn</ta>
            <ta e="T301" id="Seg_2698" s="T300">kɨːl</ta>
            <ta e="T302" id="Seg_2699" s="T301">uskaːn</ta>
            <ta e="T303" id="Seg_2700" s="T302">ü͡öskeː-BIT</ta>
            <ta e="T304" id="Seg_2701" s="T303">haŋa-tA</ta>
            <ta e="T305" id="Seg_2702" s="T304">da</ta>
            <ta e="T306" id="Seg_2703" s="T305">hu͡ok</ta>
            <ta e="T307" id="Seg_2704" s="T306">bu͡ol-BIT</ta>
            <ta e="T308" id="Seg_2705" s="T307">dʼon-ttAn</ta>
            <ta e="T309" id="Seg_2706" s="T308">küren-Ar</ta>
            <ta e="T310" id="Seg_2707" s="T309">bu͡ol-BIT</ta>
            <ta e="T311" id="Seg_2708" s="T310">bili</ta>
            <ta e="T312" id="Seg_2709" s="T311">ogonnʼor</ta>
            <ta e="T313" id="Seg_2710" s="T312">ogus-I-BIT-tI-ttAn</ta>
            <ta e="T314" id="Seg_2711" s="T313">uskaːn</ta>
            <ta e="T315" id="Seg_2712" s="T314">kulgaːk-tI-n</ta>
            <ta e="T316" id="Seg_2713" s="T315">töbö-tA</ta>
            <ta e="T317" id="Seg_2714" s="T316">üje-ttAn</ta>
            <ta e="T318" id="Seg_2715" s="T317">üje-GA</ta>
            <ta e="T319" id="Seg_2716" s="T318">beli͡e-LAːK</ta>
            <ta e="T320" id="Seg_2717" s="T319">bu͡ol-BIT</ta>
            <ta e="T321" id="Seg_2718" s="T320">emeːksin</ta>
            <ta e="T322" id="Seg_2719" s="T321">tostu</ta>
            <ta e="T323" id="Seg_2720" s="T322">ogus-I-BIT</ta>
            <ta e="T324" id="Seg_2721" s="T323">kelin</ta>
            <ta e="T325" id="Seg_2722" s="T324">atak-tA</ta>
            <ta e="T326" id="Seg_2723" s="T325">tus-tI-nAn</ta>
            <ta e="T327" id="Seg_2724" s="T326">hühü͡ök</ta>
            <ta e="T328" id="Seg_2725" s="T327">bu͡ol-BIT</ta>
            <ta e="T329" id="Seg_2726" s="T328">ele-tA</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_2727" s="T0">long.ago</ta>
            <ta e="T2" id="Seg_2728" s="T1">people.[NOM]</ta>
            <ta e="T3" id="Seg_2729" s="T2">live-PST2.[3SG]</ta>
            <ta e="T4" id="Seg_2730" s="T3">Evenki-PL.[NOM]</ta>
            <ta e="T5" id="Seg_2731" s="T4">one</ta>
            <ta e="T6" id="Seg_2732" s="T5">old.man-PROPR-3PL</ta>
            <ta e="T7" id="Seg_2733" s="T6">this-3SG-3PL.[NOM]</ta>
            <ta e="T8" id="Seg_2734" s="T7">leader.[NOM]</ta>
            <ta e="T9" id="Seg_2735" s="T8">be-PST1-3SG</ta>
            <ta e="T10" id="Seg_2736" s="T9">3SG.[NOM]</ta>
            <ta e="T11" id="Seg_2737" s="T10">advise-3SG-ACC</ta>
            <ta e="T12" id="Seg_2738" s="T11">cut-NEG.PTCP</ta>
            <ta e="T13" id="Seg_2739" s="T12">be-PST1-3PL</ta>
            <ta e="T14" id="Seg_2740" s="T13">what.kind.of</ta>
            <ta e="T15" id="Seg_2741" s="T14">place-DAT/LOC</ta>
            <ta e="T16" id="Seg_2742" s="T15">live.permanently-PTCP.PRS-3PL-ACC</ta>
            <ta e="T17" id="Seg_2743" s="T16">advise-PRS.[3SG]</ta>
            <ta e="T18" id="Seg_2744" s="T17">forest-PROPR</ta>
            <ta e="T19" id="Seg_2745" s="T18">warm</ta>
            <ta e="T20" id="Seg_2746" s="T19">place-DAT/LOC</ta>
            <ta e="T21" id="Seg_2747" s="T20">such</ta>
            <ta e="T22" id="Seg_2748" s="T21">place-DAT/LOC</ta>
            <ta e="T23" id="Seg_2749" s="T22">life-INCH-FUT-1PL</ta>
            <ta e="T24" id="Seg_2750" s="T23">say-PRS.[3SG]</ta>
            <ta e="T25" id="Seg_2751" s="T24">thither</ta>
            <ta e="T26" id="Seg_2752" s="T25">reach-CVB.SEQ</ta>
            <ta e="T27" id="Seg_2753" s="T26">settle-PST2-3PL</ta>
            <ta e="T28" id="Seg_2754" s="T27">yurt.[NOM]</ta>
            <ta e="T29" id="Seg_2755" s="T28">build.up-EP-PST2-3PL</ta>
            <ta e="T30" id="Seg_2756" s="T29">some-PL.[NOM]</ta>
            <ta e="T31" id="Seg_2757" s="T30">four_cornered.poletent.[NOM]</ta>
            <ta e="T32" id="Seg_2758" s="T31">three</ta>
            <ta e="T33" id="Seg_2759" s="T32">neighbour.[NOM]</ta>
            <ta e="T34" id="Seg_2760" s="T33">member.of.house.[NOM]</ta>
            <ta e="T35" id="Seg_2761" s="T34">become-PST2-3PL</ta>
            <ta e="T36" id="Seg_2762" s="T35">when</ta>
            <ta e="T37" id="Seg_2763" s="T36">INDEF</ta>
            <ta e="T38" id="Seg_2764" s="T37">reserves.[NOM]</ta>
            <ta e="T39" id="Seg_2765" s="T38">meat-3PL.[NOM]</ta>
            <ta e="T40" id="Seg_2766" s="T39">end-PST2.[3SG]</ta>
            <ta e="T41" id="Seg_2767" s="T40">this</ta>
            <ta e="T42" id="Seg_2768" s="T41">human.being-PL.[NOM]</ta>
            <ta e="T43" id="Seg_2769" s="T42">hunger-PST2-3PL</ta>
            <ta e="T44" id="Seg_2770" s="T43">reindeer-3PL-ACC</ta>
            <ta e="T45" id="Seg_2771" s="T44">eat-PTCP.FUT-3PL-ACC</ta>
            <ta e="T46" id="Seg_2772" s="T45">reindeer-3PL.[NOM]</ta>
            <ta e="T47" id="Seg_2773" s="T46">small.amount.[NOM]</ta>
            <ta e="T48" id="Seg_2774" s="T47">last-3SG.[NOM]</ta>
            <ta e="T49" id="Seg_2775" s="T48">five</ta>
            <ta e="T50" id="Seg_2776" s="T49">four</ta>
            <ta e="T51" id="Seg_2777" s="T50">MOD</ta>
            <ta e="T52" id="Seg_2778" s="T51">old.man.[NOM]</ta>
            <ta e="T53" id="Seg_2779" s="T52">advise-PRS.[3SG]</ta>
            <ta e="T54" id="Seg_2780" s="T53">reindeer-1PL-ACC</ta>
            <ta e="T55" id="Seg_2781" s="T54">eat-TEMP-1PL</ta>
            <ta e="T56" id="Seg_2782" s="T55">earth.[NOM]</ta>
            <ta e="T57" id="Seg_2783" s="T56">poor.[NOM]</ta>
            <ta e="T58" id="Seg_2784" s="T57">be-FUT-1PL</ta>
            <ta e="T59" id="Seg_2785" s="T58">eat-NEG-IMP.1PL</ta>
            <ta e="T60" id="Seg_2786" s="T59">reindeer-ACC</ta>
            <ta e="T61" id="Seg_2787" s="T60">advise-FUT-1SG</ta>
            <ta e="T62" id="Seg_2788" s="T61">here</ta>
            <ta e="T63" id="Seg_2789" s="T62">this</ta>
            <ta e="T64" id="Seg_2790" s="T63">river-DAT/LOC</ta>
            <ta e="T65" id="Seg_2791" s="T64">neighbour-PL-1PL.[NOM]</ta>
            <ta e="T66" id="Seg_2792" s="T65">hare-PL.[NOM]</ta>
            <ta e="T67" id="Seg_2793" s="T66">there.is-3PL</ta>
            <ta e="T68" id="Seg_2794" s="T67">there</ta>
            <ta e="T69" id="Seg_2795" s="T68">shaman-PROPR-3PL</ta>
            <ta e="T70" id="Seg_2796" s="T69">1PL.[NOM]</ta>
            <ta e="T71" id="Seg_2797" s="T70">betray-CVB.SEQ-1PL</ta>
            <ta e="T72" id="Seg_2798" s="T71">hither</ta>
            <ta e="T73" id="Seg_2799" s="T72">bring-PTCP.FUT-DAT/LOC</ta>
            <ta e="T74" id="Seg_2800" s="T73">3PL-ACC</ta>
            <ta e="T75" id="Seg_2801" s="T74">1SG.[NOM]</ta>
            <ta e="T76" id="Seg_2802" s="T75">be.sick-EP-PTCP.PST</ta>
            <ta e="T77" id="Seg_2803" s="T76">be-IMP.1SG</ta>
            <ta e="T78" id="Seg_2804" s="T77">shaman-3PL-ACC</ta>
            <ta e="T79" id="Seg_2805" s="T78">invite-EP-IMP.2PL</ta>
            <ta e="T80" id="Seg_2806" s="T79">3SG-ACC</ta>
            <ta e="T81" id="Seg_2807" s="T80">with</ta>
            <ta e="T82" id="Seg_2808" s="T81">many</ta>
            <ta e="T83" id="Seg_2809" s="T82">hare.[NOM]</ta>
            <ta e="T84" id="Seg_2810" s="T83">come-FUT.[3SG]</ta>
            <ta e="T85" id="Seg_2811" s="T84">well</ta>
            <ta e="T86" id="Seg_2812" s="T85">that-PL-ACC</ta>
            <ta e="T87" id="Seg_2813" s="T86">1PL.[NOM]</ta>
            <ta e="T88" id="Seg_2814" s="T87">kill-MED-FUT-1PL</ta>
            <ta e="T89" id="Seg_2815" s="T88">and</ta>
            <ta e="T90" id="Seg_2816" s="T89">eat-FUT-1PL</ta>
            <ta e="T91" id="Seg_2817" s="T90">then</ta>
            <ta e="T92" id="Seg_2818" s="T91">front.[NOM]</ta>
            <ta e="T93" id="Seg_2819" s="T92">in.the.direction</ta>
            <ta e="T94" id="Seg_2820" s="T93">step.over-FUT-1PL</ta>
            <ta e="T95" id="Seg_2821" s="T94">year.[NOM]</ta>
            <ta e="T96" id="Seg_2822" s="T95">get.warmer-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T97" id="Seg_2823" s="T96">reach-FUT-1PL</ta>
            <ta e="T98" id="Seg_2824" s="T97">hunt-EP-INSTR</ta>
            <ta e="T99" id="Seg_2825" s="T98">eat-FUT-1PL</ta>
            <ta e="T100" id="Seg_2826" s="T99">human.being-3PL.[NOM]</ta>
            <ta e="T101" id="Seg_2827" s="T100">hare-3PL-ACC</ta>
            <ta e="T102" id="Seg_2828" s="T101">search-CVB.SIM</ta>
            <ta e="T103" id="Seg_2829" s="T102">go-PST1-3PL</ta>
            <ta e="T104" id="Seg_2830" s="T103">old.man.[NOM]</ta>
            <ta e="T105" id="Seg_2831" s="T104">be.sick-EP-PST2-3SG</ta>
            <ta e="T106" id="Seg_2832" s="T105">be-PST1-3SG</ta>
            <ta e="T107" id="Seg_2833" s="T106">shoes-COM</ta>
            <ta e="T108" id="Seg_2834" s="T107">blanket.[NOM]</ta>
            <ta e="T109" id="Seg_2835" s="T108">inside-3SG-DAT/LOC</ta>
            <ta e="T110" id="Seg_2836" s="T109">lie.down-PST1-3SG</ta>
            <ta e="T111" id="Seg_2837" s="T110">old.woman-3SG-DAT/LOC</ta>
            <ta e="T112" id="Seg_2838" s="T111">speak-PRS.[3SG]</ta>
            <ta e="T113" id="Seg_2839" s="T112">hare.[NOM]</ta>
            <ta e="T114" id="Seg_2840" s="T113">shaman-3SG.[NOM]</ta>
            <ta e="T115" id="Seg_2841" s="T114">come-CVB.SEQ</ta>
            <ta e="T116" id="Seg_2842" s="T115">shamanize-TEMP-3SG</ta>
            <ta e="T117" id="Seg_2843" s="T116">window-2SG-ACC</ta>
            <ta e="T118" id="Seg_2844" s="T117">chimney-2SG-ACC</ta>
            <ta e="T119" id="Seg_2845" s="T118">cover-FUT.[IMP.2SG]</ta>
            <ta e="T120" id="Seg_2846" s="T119">door-2SG-ACC</ta>
            <ta e="T121" id="Seg_2847" s="T120">block-FUT.[IMP.2SG]</ta>
            <ta e="T122" id="Seg_2848" s="T121">moan-CVB.SIM-moan-CVB.SIM</ta>
            <ta e="T123" id="Seg_2849" s="T122">be.delirious-PST2-3SG</ta>
            <ta e="T124" id="Seg_2850" s="T123">be-CVB.SEQ</ta>
            <ta e="T330" id="Seg_2851" s="T124">eh</ta>
            <ta e="T331" id="Seg_2852" s="T330">eh</ta>
            <ta e="T125" id="Seg_2853" s="T331">eh</ta>
            <ta e="T127" id="Seg_2854" s="T126">say-FUT-EP-1SG</ta>
            <ta e="T128" id="Seg_2855" s="T127">then</ta>
            <ta e="T129" id="Seg_2856" s="T128">hook.for.the.kettle-ACC</ta>
            <ta e="T130" id="Seg_2857" s="T129">hand-FUT.[IMP.2SG]</ta>
            <ta e="T132" id="Seg_2858" s="T130">1SG-DAT/LOC</ta>
            <ta e="T335" id="Seg_2859" s="T132">eh-eh</ta>
            <ta e="T134" id="Seg_2860" s="T133">say-FUT-EP-1SG</ta>
            <ta e="T135" id="Seg_2861" s="T134">then</ta>
            <ta e="T136" id="Seg_2862" s="T135">2SG.[NOM]</ta>
            <ta e="T137" id="Seg_2863" s="T136">scraper.for.leather-ACC</ta>
            <ta e="T138" id="Seg_2864" s="T137">grab-FUT.[IMP.2SG]</ta>
            <ta e="T139" id="Seg_2865" s="T138">door-DAT/LOC</ta>
            <ta e="T140" id="Seg_2866" s="T139">position.oneself-FUT.[IMP.2SG]</ta>
            <ta e="T141" id="Seg_2867" s="T140">then</ta>
            <ta e="T142" id="Seg_2868" s="T141">1SG.[NOM]</ta>
            <ta e="T143" id="Seg_2869" s="T142">jump-CVB.SEQ</ta>
            <ta e="T144" id="Seg_2870" s="T143">stand-CVB.SEQ</ta>
            <ta e="T145" id="Seg_2871" s="T144">hit-ITER-CVB.SEQ</ta>
            <ta e="T146" id="Seg_2872" s="T145">go-FUT-1SG</ta>
            <ta e="T147" id="Seg_2873" s="T146">shaman-3PL-ACC</ta>
            <ta e="T148" id="Seg_2874" s="T147">slay-FUT-1SG</ta>
            <ta e="T149" id="Seg_2875" s="T148">2SG.[NOM]</ta>
            <ta e="T150" id="Seg_2876" s="T149">also</ta>
            <ta e="T151" id="Seg_2877" s="T150">catch-EP-INCH-FUT.[IMP.2SG]</ta>
            <ta e="T152" id="Seg_2878" s="T151">old.man.[NOM]</ta>
            <ta e="T153" id="Seg_2879" s="T152">lie.down-PRS.[3SG]</ta>
            <ta e="T154" id="Seg_2880" s="T153">moan-PRS.[3SG]</ta>
            <ta e="T155" id="Seg_2881" s="T154">human.being-ACC</ta>
            <ta e="T156" id="Seg_2882" s="T155">know-NEG.PTCP</ta>
            <ta e="T157" id="Seg_2883" s="T156">be-CVB.SIM-be-CVB.SIM</ta>
            <ta e="T158" id="Seg_2884" s="T157">hare.[NOM]</ta>
            <ta e="T159" id="Seg_2885" s="T158">shaman-3SG.[NOM]</ta>
            <ta e="T160" id="Seg_2886" s="T159">come-PRS.[3SG]</ta>
            <ta e="T161" id="Seg_2887" s="T160">3SG-ACC</ta>
            <ta e="T162" id="Seg_2888" s="T161">with</ta>
            <ta e="T163" id="Seg_2889" s="T162">many</ta>
            <ta e="T164" id="Seg_2890" s="T163">hare.[NOM]</ta>
            <ta e="T165" id="Seg_2891" s="T164">come-PST2.[3SG]</ta>
            <ta e="T166" id="Seg_2892" s="T165">yurt.[NOM]</ta>
            <ta e="T167" id="Seg_2893" s="T166">inside-3SG.[NOM]</ta>
            <ta e="T168" id="Seg_2894" s="T167">hare.[NOM]</ta>
            <ta e="T169" id="Seg_2895" s="T168">only</ta>
            <ta e="T170" id="Seg_2896" s="T169">become-PST2.[3SG]</ta>
            <ta e="T171" id="Seg_2897" s="T170">old.woman.[NOM]</ta>
            <ta e="T172" id="Seg_2898" s="T171">window-ACC</ta>
            <ta e="T173" id="Seg_2899" s="T172">chimney-ACC</ta>
            <ta e="T174" id="Seg_2900" s="T173">jam-PST2.[3SG]</ta>
            <ta e="T175" id="Seg_2901" s="T174">door-ACC</ta>
            <ta e="T176" id="Seg_2902" s="T175">block-PST2.[3SG]</ta>
            <ta e="T177" id="Seg_2903" s="T176">hare.[NOM]</ta>
            <ta e="T178" id="Seg_2904" s="T177">shaman-3SG.[NOM]</ta>
            <ta e="T179" id="Seg_2905" s="T178">shamanize-PST2.[3SG]</ta>
            <ta e="T180" id="Seg_2906" s="T179">soon</ta>
            <ta e="T181" id="Seg_2907" s="T180">jump-PST2.[3SG]</ta>
            <ta e="T182" id="Seg_2908" s="T181">old.man.[NOM]</ta>
            <ta e="T183" id="Seg_2909" s="T182">intensify-PST2.[3SG]</ta>
            <ta e="T184" id="Seg_2910" s="T183">be.delirious-CVB.SIM</ta>
            <ta e="T332" id="Seg_2911" s="T184">eh</ta>
            <ta e="T185" id="Seg_2912" s="T332">eh</ta>
            <ta e="T187" id="Seg_2913" s="T186">say-PST2.[3SG]</ta>
            <ta e="T188" id="Seg_2914" s="T187">old.woman.[NOM]</ta>
            <ta e="T189" id="Seg_2915" s="T188">jump-CVB.SEQ</ta>
            <ta e="T190" id="Seg_2916" s="T189">stand.up-CVB.SEQ</ta>
            <ta e="T191" id="Seg_2917" s="T190">old.man-DAT/LOC</ta>
            <ta e="T192" id="Seg_2918" s="T191">hook.for.the.kettle-ACC</ta>
            <ta e="T193" id="Seg_2919" s="T192">hand-PST2.[3SG]</ta>
            <ta e="T333" id="Seg_2920" s="T193">eh</ta>
            <ta e="T194" id="Seg_2921" s="T333">eh</ta>
            <ta e="T196" id="Seg_2922" s="T195">say-PST2.[3SG]</ta>
            <ta e="T334" id="Seg_2923" s="T196">eh</ta>
            <ta e="T197" id="Seg_2924" s="T334">eh</ta>
            <ta e="T199" id="Seg_2925" s="T198">so</ta>
            <ta e="T200" id="Seg_2926" s="T199">say-CVB.ANT-3SG</ta>
            <ta e="T201" id="Seg_2927" s="T200">old.woman-3SG.[NOM]</ta>
            <ta e="T202" id="Seg_2928" s="T201">scraper.for.leather-ACC</ta>
            <ta e="T203" id="Seg_2929" s="T202">grab-PST2.[3SG]</ta>
            <ta e="T204" id="Seg_2930" s="T203">neighbour-3SG.[NOM]</ta>
            <ta e="T205" id="Seg_2931" s="T204">small.axe-ACC</ta>
            <ta e="T206" id="Seg_2932" s="T205">grab-PST2.[3SG]</ta>
            <ta e="T207" id="Seg_2933" s="T206">shaman-3PL.[NOM]</ta>
            <ta e="T208" id="Seg_2934" s="T207">jump-CVB.SIM</ta>
            <ta e="T209" id="Seg_2935" s="T208">stand-PRS.[3SG]</ta>
            <ta e="T215" id="Seg_2936" s="T214">say-CVB.SIM-say-CVB.SIM</ta>
            <ta e="T216" id="Seg_2937" s="T215">that.[NOM]</ta>
            <ta e="T217" id="Seg_2938" s="T216">name-3SG.[NOM]</ta>
            <ta e="T218" id="Seg_2939" s="T217">this</ta>
            <ta e="T219" id="Seg_2940" s="T218">tent-ABL</ta>
            <ta e="T220" id="Seg_2941" s="T219">go.out-PTCP.FUT</ta>
            <ta e="T221" id="Seg_2942" s="T220">be-PTCP.FUT</ta>
            <ta e="T223" id="Seg_2943" s="T221">be-COND-1SG</ta>
            <ta e="T224" id="Seg_2944" s="T223">say-CVB.SEQ</ta>
            <ta e="T225" id="Seg_2945" s="T224">Evenki-SIM</ta>
            <ta e="T226" id="Seg_2946" s="T225">old.man.[NOM]</ta>
            <ta e="T227" id="Seg_2947" s="T226">that-ACC</ta>
            <ta e="T228" id="Seg_2948" s="T227">hear-CVB.ANT</ta>
            <ta e="T229" id="Seg_2949" s="T228">jump-CVB.SEQ</ta>
            <ta e="T230" id="Seg_2950" s="T229">stand.up-PRS.[3SG]</ta>
            <ta e="T231" id="Seg_2951" s="T230">shaman-3PL.[NOM]</ta>
            <ta e="T232" id="Seg_2952" s="T231">jump-PST2.[3SG]</ta>
            <ta e="T233" id="Seg_2953" s="T232">bar.for.the.kettle-DAT/LOC</ta>
            <ta e="T234" id="Seg_2954" s="T233">old.man.[NOM]</ta>
            <ta e="T235" id="Seg_2955" s="T234">hook.for.the.kettle-3SG-INSTR</ta>
            <ta e="T236" id="Seg_2956" s="T235">bar.for.the.kettle-DAT/LOC</ta>
            <ta e="T237" id="Seg_2957" s="T236">press-CVB.SIM</ta>
            <ta e="T238" id="Seg_2958" s="T237">beat-PRS.[3SG]</ta>
            <ta e="T239" id="Seg_2959" s="T238">shaman-hare.[NOM]</ta>
            <ta e="T240" id="Seg_2960" s="T239">turn-CVB.SEQ</ta>
            <ta e="T241" id="Seg_2961" s="T240">give-PST2.[3SG]</ta>
            <ta e="T242" id="Seg_2962" s="T241">two</ta>
            <ta e="T243" id="Seg_2963" s="T242">ear-3SG-ACC</ta>
            <ta e="T244" id="Seg_2964" s="T243">just</ta>
            <ta e="T245" id="Seg_2965" s="T244">across</ta>
            <ta e="T246" id="Seg_2966" s="T245">beat-EP-PST2.[3SG]</ta>
            <ta e="T247" id="Seg_2967" s="T246">hare.[NOM]</ta>
            <ta e="T248" id="Seg_2968" s="T247">shaman-3SG.[NOM]</ta>
            <ta e="T249" id="Seg_2969" s="T248">winter.[NOM]</ta>
            <ta e="T250" id="Seg_2970" s="T249">wood-DAT/LOC</ta>
            <ta e="T251" id="Seg_2971" s="T250">jump-PST2.[3SG]</ta>
            <ta e="T252" id="Seg_2972" s="T251">jump-CVB.ANT-3SG</ta>
            <ta e="T253" id="Seg_2973" s="T252">old.woman.[NOM]</ta>
            <ta e="T254" id="Seg_2974" s="T253">scraper.for.leather-3SG-INSTR</ta>
            <ta e="T255" id="Seg_2975" s="T254">beat-PRS.[3SG]</ta>
            <ta e="T256" id="Seg_2976" s="T255">back</ta>
            <ta e="T257" id="Seg_2977" s="T256">only</ta>
            <ta e="T258" id="Seg_2978" s="T257">leg-3SG-DAT/LOC</ta>
            <ta e="T259" id="Seg_2979" s="T258">two</ta>
            <ta e="T260" id="Seg_2980" s="T259">back</ta>
            <ta e="T261" id="Seg_2981" s="T260">leg-3SG-ACC</ta>
            <ta e="T262" id="Seg_2982" s="T261">break-PRS.[3SG]</ta>
            <ta e="T263" id="Seg_2983" s="T262">hare.[NOM]</ta>
            <ta e="T264" id="Seg_2984" s="T263">shaman-3SG.[NOM]</ta>
            <ta e="T265" id="Seg_2985" s="T264">winter.[NOM]</ta>
            <ta e="T266" id="Seg_2986" s="T265">wood-ABL</ta>
            <ta e="T267" id="Seg_2987" s="T266">chimney-3SG-INSTR</ta>
            <ta e="T268" id="Seg_2988" s="T267">jump-PRS.[3SG]</ta>
            <ta e="T269" id="Seg_2989" s="T268">that-DAT/LOC</ta>
            <ta e="T270" id="Seg_2990" s="T269">hare.[NOM]</ta>
            <ta e="T271" id="Seg_2991" s="T270">old.woman-3SG.[NOM]</ta>
            <ta e="T272" id="Seg_2992" s="T271">also</ta>
            <ta e="T273" id="Seg_2993" s="T272">chimney-3SG-INSTR</ta>
            <ta e="T274" id="Seg_2994" s="T273">jump-PST2.[3SG]</ta>
            <ta e="T275" id="Seg_2995" s="T274">house-DAT/LOC</ta>
            <ta e="T276" id="Seg_2996" s="T275">there.is</ta>
            <ta e="T277" id="Seg_2997" s="T276">hare-PL-ACC</ta>
            <ta e="T278" id="Seg_2998" s="T277">every-3PL-ACC</ta>
            <ta e="T279" id="Seg_2999" s="T278">completely</ta>
            <ta e="T280" id="Seg_3000" s="T279">kill-ITER-CVB.SEQ</ta>
            <ta e="T281" id="Seg_3001" s="T280">throw-PST2-3PL</ta>
            <ta e="T282" id="Seg_3002" s="T281">this-ACC</ta>
            <ta e="T283" id="Seg_3003" s="T282">eat-CVB.SEQ-3PL</ta>
            <ta e="T284" id="Seg_3004" s="T283">spring.[NOM]</ta>
            <ta e="T285" id="Seg_3005" s="T284">year.[NOM]</ta>
            <ta e="T286" id="Seg_3006" s="T285">get.warmer-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T287" id="Seg_3007" s="T286">reach-PST2-3PL</ta>
            <ta e="T288" id="Seg_3008" s="T287">before</ta>
            <ta e="T289" id="Seg_3009" s="T288">hare-ACC</ta>
            <ta e="T290" id="Seg_3010" s="T289">eat-NEG.PTCP</ta>
            <ta e="T291" id="Seg_3011" s="T290">it.is.said-3PL</ta>
            <ta e="T292" id="Seg_3012" s="T291">that.EMPH-ABL</ta>
            <ta e="T293" id="Seg_3013" s="T292">well</ta>
            <ta e="T294" id="Seg_3014" s="T293">hare-ACC</ta>
            <ta e="T295" id="Seg_3015" s="T294">eat-PTCP.PRS</ta>
            <ta e="T296" id="Seg_3016" s="T295">become-PST2-3PL</ta>
            <ta e="T297" id="Seg_3017" s="T296">people-PL.[NOM]</ta>
            <ta e="T298" id="Seg_3018" s="T297">escape-PTCP.PST</ta>
            <ta e="T299" id="Seg_3019" s="T298">hare.[NOM]</ta>
            <ta e="T300" id="Seg_3020" s="T299">shaman-3SG-ABL</ta>
            <ta e="T301" id="Seg_3021" s="T300">wild</ta>
            <ta e="T302" id="Seg_3022" s="T301">hare.[NOM]</ta>
            <ta e="T303" id="Seg_3023" s="T302">arise-PST2.[3SG]</ta>
            <ta e="T304" id="Seg_3024" s="T303">word-POSS</ta>
            <ta e="T305" id="Seg_3025" s="T304">NEG</ta>
            <ta e="T306" id="Seg_3026" s="T305">NEG</ta>
            <ta e="T307" id="Seg_3027" s="T306">become-PST2.[3SG]</ta>
            <ta e="T308" id="Seg_3028" s="T307">people-ABL</ta>
            <ta e="T309" id="Seg_3029" s="T308">run.away-PTCP.PRS</ta>
            <ta e="T310" id="Seg_3030" s="T309">become-PST2.[3SG]</ta>
            <ta e="T311" id="Seg_3031" s="T310">exactly.this</ta>
            <ta e="T312" id="Seg_3032" s="T311">old.man.[NOM]</ta>
            <ta e="T313" id="Seg_3033" s="T312">beat-EP-PTCP.PST-3SG-ABL</ta>
            <ta e="T314" id="Seg_3034" s="T313">hare.[NOM]</ta>
            <ta e="T315" id="Seg_3035" s="T314">ear-3SG-GEN</ta>
            <ta e="T316" id="Seg_3036" s="T315">top-3SG.[NOM]</ta>
            <ta e="T317" id="Seg_3037" s="T316">time-ABL</ta>
            <ta e="T318" id="Seg_3038" s="T317">time-DAT/LOC</ta>
            <ta e="T319" id="Seg_3039" s="T318">mark-PROPR.[NOM]</ta>
            <ta e="T320" id="Seg_3040" s="T319">become-PST2.[3SG]</ta>
            <ta e="T321" id="Seg_3041" s="T320">old.woman.[NOM]</ta>
            <ta e="T322" id="Seg_3042" s="T321">broken</ta>
            <ta e="T323" id="Seg_3043" s="T322">beat-EP-PTCP.PST</ta>
            <ta e="T324" id="Seg_3044" s="T323">back</ta>
            <ta e="T325" id="Seg_3045" s="T324">leg-3SG.[NOM]</ta>
            <ta e="T326" id="Seg_3046" s="T325">side-3SG-INSTR</ta>
            <ta e="T327" id="Seg_3047" s="T326">joint.[NOM]</ta>
            <ta e="T328" id="Seg_3048" s="T327">become-PST2.[3SG]</ta>
            <ta e="T329" id="Seg_3049" s="T328">last-3SG.[NOM]</ta>
         </annotation>
         <annotation name="gg" tierref="gd">
            <ta e="T1" id="Seg_3050" s="T0">vor.langer.Zeit</ta>
            <ta e="T2" id="Seg_3051" s="T1">Leute.[NOM]</ta>
            <ta e="T3" id="Seg_3052" s="T2">leben-PST2.[3SG]</ta>
            <ta e="T4" id="Seg_3053" s="T3">Ewenke-PL.[NOM]</ta>
            <ta e="T5" id="Seg_3054" s="T4">eins</ta>
            <ta e="T6" id="Seg_3055" s="T5">alter.Mann-PROPR-3PL</ta>
            <ta e="T7" id="Seg_3056" s="T6">dieses-3SG-3PL.[NOM]</ta>
            <ta e="T8" id="Seg_3057" s="T7">Anführer.[NOM]</ta>
            <ta e="T9" id="Seg_3058" s="T8">sein-PST1-3SG</ta>
            <ta e="T10" id="Seg_3059" s="T9">3SG.[NOM]</ta>
            <ta e="T11" id="Seg_3060" s="T10">Rat-3SG-ACC</ta>
            <ta e="T12" id="Seg_3061" s="T11">schneiden-NEG.PTCP</ta>
            <ta e="T13" id="Seg_3062" s="T12">sein-PST1-3PL</ta>
            <ta e="T14" id="Seg_3063" s="T13">was.für.ein</ta>
            <ta e="T15" id="Seg_3064" s="T14">Ort-DAT/LOC</ta>
            <ta e="T16" id="Seg_3065" s="T15">dauerhaft.wohnen-PTCP.PRS-3PL-ACC</ta>
            <ta e="T17" id="Seg_3066" s="T16">raten-PRS.[3SG]</ta>
            <ta e="T18" id="Seg_3067" s="T17">Wald-PROPR</ta>
            <ta e="T19" id="Seg_3068" s="T18">warm</ta>
            <ta e="T20" id="Seg_3069" s="T19">Ort-DAT/LOC</ta>
            <ta e="T21" id="Seg_3070" s="T20">solch</ta>
            <ta e="T22" id="Seg_3071" s="T21">Ort-DAT/LOC</ta>
            <ta e="T23" id="Seg_3072" s="T22">Leben-INCH-FUT-1PL</ta>
            <ta e="T24" id="Seg_3073" s="T23">sagen-PRS.[3SG]</ta>
            <ta e="T25" id="Seg_3074" s="T24">dorthin</ta>
            <ta e="T26" id="Seg_3075" s="T25">ankommen-CVB.SEQ</ta>
            <ta e="T27" id="Seg_3076" s="T26">sich.niederlassen-PST2-3PL</ta>
            <ta e="T28" id="Seg_3077" s="T27">Jurte.[NOM]</ta>
            <ta e="T29" id="Seg_3078" s="T28">aufbauen-EP-PST2-3PL</ta>
            <ta e="T30" id="Seg_3079" s="T29">mancher-PL.[NOM]</ta>
            <ta e="T31" id="Seg_3080" s="T30">viereckiges.Stangenzelt.[NOM]</ta>
            <ta e="T32" id="Seg_3081" s="T31">drei</ta>
            <ta e="T33" id="Seg_3082" s="T32">Nachbar.[NOM]</ta>
            <ta e="T34" id="Seg_3083" s="T33">Hausangehöriger.[NOM]</ta>
            <ta e="T35" id="Seg_3084" s="T34">werden-PST2-3PL</ta>
            <ta e="T36" id="Seg_3085" s="T35">wann</ta>
            <ta e="T37" id="Seg_3086" s="T36">INDEF</ta>
            <ta e="T38" id="Seg_3087" s="T37">Vorrat.[NOM]</ta>
            <ta e="T39" id="Seg_3088" s="T38">Fleisch-3PL.[NOM]</ta>
            <ta e="T40" id="Seg_3089" s="T39">enden-PST2.[3SG]</ta>
            <ta e="T41" id="Seg_3090" s="T40">dieses</ta>
            <ta e="T42" id="Seg_3091" s="T41">Mensch-PL.[NOM]</ta>
            <ta e="T43" id="Seg_3092" s="T42">hungern-PST2-3PL</ta>
            <ta e="T44" id="Seg_3093" s="T43">Rentier-3PL-ACC</ta>
            <ta e="T45" id="Seg_3094" s="T44">essen-PTCP.FUT-3PL-ACC</ta>
            <ta e="T46" id="Seg_3095" s="T45">Rentier-3PL.[NOM]</ta>
            <ta e="T47" id="Seg_3096" s="T46">kleine.Menge.[NOM]</ta>
            <ta e="T48" id="Seg_3097" s="T47">letzter-3SG.[NOM]</ta>
            <ta e="T49" id="Seg_3098" s="T48">fünf</ta>
            <ta e="T50" id="Seg_3099" s="T49">vier</ta>
            <ta e="T51" id="Seg_3100" s="T50">MOD</ta>
            <ta e="T52" id="Seg_3101" s="T51">alter.Mann.[NOM]</ta>
            <ta e="T53" id="Seg_3102" s="T52">raten-PRS.[3SG]</ta>
            <ta e="T54" id="Seg_3103" s="T53">Rentier-1PL-ACC</ta>
            <ta e="T55" id="Seg_3104" s="T54">essen-TEMP-1PL</ta>
            <ta e="T56" id="Seg_3105" s="T55">Boden.[NOM]</ta>
            <ta e="T57" id="Seg_3106" s="T56">arm.[NOM]</ta>
            <ta e="T58" id="Seg_3107" s="T57">sein-FUT-1PL</ta>
            <ta e="T59" id="Seg_3108" s="T58">essen-NEG-IMP.1PL</ta>
            <ta e="T60" id="Seg_3109" s="T59">Rentier-ACC</ta>
            <ta e="T61" id="Seg_3110" s="T60">raten-FUT-1SG</ta>
            <ta e="T62" id="Seg_3111" s="T61">hier</ta>
            <ta e="T63" id="Seg_3112" s="T62">dieses</ta>
            <ta e="T64" id="Seg_3113" s="T63">Fluss-DAT/LOC</ta>
            <ta e="T65" id="Seg_3114" s="T64">Nachbar-PL-1PL.[NOM]</ta>
            <ta e="T66" id="Seg_3115" s="T65">Hase-PL.[NOM]</ta>
            <ta e="T67" id="Seg_3116" s="T66">es.gibt-3PL</ta>
            <ta e="T68" id="Seg_3117" s="T67">dort</ta>
            <ta e="T69" id="Seg_3118" s="T68">Schamane-PROPR-3PL</ta>
            <ta e="T70" id="Seg_3119" s="T69">1PL.[NOM]</ta>
            <ta e="T71" id="Seg_3120" s="T70">betrügen-CVB.SEQ-1PL</ta>
            <ta e="T72" id="Seg_3121" s="T71">hierher</ta>
            <ta e="T73" id="Seg_3122" s="T72">bringen-PTCP.FUT-DAT/LOC</ta>
            <ta e="T74" id="Seg_3123" s="T73">3PL-ACC</ta>
            <ta e="T75" id="Seg_3124" s="T74">1SG.[NOM]</ta>
            <ta e="T76" id="Seg_3125" s="T75">krank.sein-EP-PTCP.PST</ta>
            <ta e="T77" id="Seg_3126" s="T76">sein-IMP.1SG</ta>
            <ta e="T78" id="Seg_3127" s="T77">Schamane-3PL-ACC</ta>
            <ta e="T79" id="Seg_3128" s="T78">einladen-EP-IMP.2PL</ta>
            <ta e="T80" id="Seg_3129" s="T79">3SG-ACC</ta>
            <ta e="T81" id="Seg_3130" s="T80">mit</ta>
            <ta e="T82" id="Seg_3131" s="T81">viel</ta>
            <ta e="T83" id="Seg_3132" s="T82">Hase.[NOM]</ta>
            <ta e="T84" id="Seg_3133" s="T83">kommen-FUT.[3SG]</ta>
            <ta e="T85" id="Seg_3134" s="T84">doch</ta>
            <ta e="T86" id="Seg_3135" s="T85">jenes-PL-ACC</ta>
            <ta e="T87" id="Seg_3136" s="T86">1PL.[NOM]</ta>
            <ta e="T88" id="Seg_3137" s="T87">töten-MED-FUT-1PL</ta>
            <ta e="T89" id="Seg_3138" s="T88">und</ta>
            <ta e="T90" id="Seg_3139" s="T89">essen-FUT-1PL</ta>
            <ta e="T91" id="Seg_3140" s="T90">dann</ta>
            <ta e="T92" id="Seg_3141" s="T91">Vorderseite.[NOM]</ta>
            <ta e="T93" id="Seg_3142" s="T92">in.Richtung</ta>
            <ta e="T94" id="Seg_3143" s="T93">übertreten-FUT-1PL</ta>
            <ta e="T95" id="Seg_3144" s="T94">Jahr.[NOM]</ta>
            <ta e="T96" id="Seg_3145" s="T95">wärmer.werden-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T97" id="Seg_3146" s="T96">ankommen-FUT-1PL</ta>
            <ta e="T98" id="Seg_3147" s="T97">Jagd-EP-INSTR</ta>
            <ta e="T99" id="Seg_3148" s="T98">essen-FUT-1PL</ta>
            <ta e="T100" id="Seg_3149" s="T99">Mensch-3PL.[NOM]</ta>
            <ta e="T101" id="Seg_3150" s="T100">Hase-3PL-ACC</ta>
            <ta e="T102" id="Seg_3151" s="T101">suchen-CVB.SIM</ta>
            <ta e="T103" id="Seg_3152" s="T102">gehen-PST1-3PL</ta>
            <ta e="T104" id="Seg_3153" s="T103">alter.Mann.[NOM]</ta>
            <ta e="T105" id="Seg_3154" s="T104">krank.sein-EP-PST2-3SG</ta>
            <ta e="T106" id="Seg_3155" s="T105">sein-PST1-3SG</ta>
            <ta e="T107" id="Seg_3156" s="T106">Schuhe-COM</ta>
            <ta e="T108" id="Seg_3157" s="T107">Bettdecke.[NOM]</ta>
            <ta e="T109" id="Seg_3158" s="T108">Inneres-3SG-DAT/LOC</ta>
            <ta e="T110" id="Seg_3159" s="T109">sich.hinlegen-PST1-3SG</ta>
            <ta e="T111" id="Seg_3160" s="T110">Alte-3SG-DAT/LOC</ta>
            <ta e="T112" id="Seg_3161" s="T111">sprechen-PRS.[3SG]</ta>
            <ta e="T113" id="Seg_3162" s="T112">Hase.[NOM]</ta>
            <ta e="T114" id="Seg_3163" s="T113">Schamane-3SG.[NOM]</ta>
            <ta e="T115" id="Seg_3164" s="T114">kommen-CVB.SEQ</ta>
            <ta e="T116" id="Seg_3165" s="T115">schamanisieren-TEMP-3SG</ta>
            <ta e="T117" id="Seg_3166" s="T116">Fenster-2SG-ACC</ta>
            <ta e="T118" id="Seg_3167" s="T117">Rauchloch-2SG-ACC</ta>
            <ta e="T119" id="Seg_3168" s="T118">verstopfen-FUT.[IMP.2SG]</ta>
            <ta e="T120" id="Seg_3169" s="T119">Tür-2SG-ACC</ta>
            <ta e="T121" id="Seg_3170" s="T120">versperren-FUT.[IMP.2SG]</ta>
            <ta e="T122" id="Seg_3171" s="T121">stöhnen-CVB.SIM-stöhnen-CVB.SIM</ta>
            <ta e="T123" id="Seg_3172" s="T122">delirieren-PST2-3SG</ta>
            <ta e="T124" id="Seg_3173" s="T123">sein-CVB.SEQ</ta>
            <ta e="T330" id="Seg_3174" s="T124">eh</ta>
            <ta e="T331" id="Seg_3175" s="T330">eh</ta>
            <ta e="T125" id="Seg_3176" s="T331">eh</ta>
            <ta e="T127" id="Seg_3177" s="T126">sagen-FUT-EP-1SG</ta>
            <ta e="T128" id="Seg_3178" s="T127">dann</ta>
            <ta e="T129" id="Seg_3179" s="T128">Haken.für.den.Kessel-ACC</ta>
            <ta e="T130" id="Seg_3180" s="T129">überreichen-FUT.[IMP.2SG]</ta>
            <ta e="T132" id="Seg_3181" s="T130">1SG-DAT/LOC</ta>
            <ta e="T335" id="Seg_3182" s="T132">eh-eh</ta>
            <ta e="T134" id="Seg_3183" s="T133">sagen-FUT-EP-1SG</ta>
            <ta e="T135" id="Seg_3184" s="T134">dann</ta>
            <ta e="T136" id="Seg_3185" s="T135">2SG.[NOM]</ta>
            <ta e="T137" id="Seg_3186" s="T136">Lederschaber-ACC</ta>
            <ta e="T138" id="Seg_3187" s="T137">greifen-FUT.[IMP.2SG]</ta>
            <ta e="T139" id="Seg_3188" s="T138">Tür-DAT/LOC</ta>
            <ta e="T140" id="Seg_3189" s="T139">sich.hinstellen-FUT.[IMP.2SG]</ta>
            <ta e="T141" id="Seg_3190" s="T140">dann</ta>
            <ta e="T142" id="Seg_3191" s="T141">1SG.[NOM]</ta>
            <ta e="T143" id="Seg_3192" s="T142">springen-CVB.SEQ</ta>
            <ta e="T144" id="Seg_3193" s="T143">anfangen-CVB.SEQ</ta>
            <ta e="T145" id="Seg_3194" s="T144">schlagen-ITER-CVB.SEQ</ta>
            <ta e="T146" id="Seg_3195" s="T145">gehen-FUT-1SG</ta>
            <ta e="T147" id="Seg_3196" s="T146">Schamane-3PL-ACC</ta>
            <ta e="T148" id="Seg_3197" s="T147">umbringen-FUT-1SG</ta>
            <ta e="T149" id="Seg_3198" s="T148">2SG.[NOM]</ta>
            <ta e="T150" id="Seg_3199" s="T149">auch</ta>
            <ta e="T151" id="Seg_3200" s="T150">ergreifen-EP-INCH-FUT.[IMP.2SG]</ta>
            <ta e="T152" id="Seg_3201" s="T151">alter.Mann.[NOM]</ta>
            <ta e="T153" id="Seg_3202" s="T152">sich.hinlegen-PRS.[3SG]</ta>
            <ta e="T154" id="Seg_3203" s="T153">stöhnen-PRS.[3SG]</ta>
            <ta e="T155" id="Seg_3204" s="T154">Mensch-ACC</ta>
            <ta e="T156" id="Seg_3205" s="T155">wissen-NEG.PTCP</ta>
            <ta e="T157" id="Seg_3206" s="T156">sein-CVB.SIM-sein-CVB.SIM</ta>
            <ta e="T158" id="Seg_3207" s="T157">Hase.[NOM]</ta>
            <ta e="T159" id="Seg_3208" s="T158">Schamane-3SG.[NOM]</ta>
            <ta e="T160" id="Seg_3209" s="T159">kommen-PRS.[3SG]</ta>
            <ta e="T161" id="Seg_3210" s="T160">3SG-ACC</ta>
            <ta e="T162" id="Seg_3211" s="T161">mit</ta>
            <ta e="T163" id="Seg_3212" s="T162">viel</ta>
            <ta e="T164" id="Seg_3213" s="T163">Hase.[NOM]</ta>
            <ta e="T165" id="Seg_3214" s="T164">kommen-PST2.[3SG]</ta>
            <ta e="T166" id="Seg_3215" s="T165">Jurte.[NOM]</ta>
            <ta e="T167" id="Seg_3216" s="T166">Inneres-3SG.[NOM]</ta>
            <ta e="T168" id="Seg_3217" s="T167">Hase.[NOM]</ta>
            <ta e="T169" id="Seg_3218" s="T168">nur</ta>
            <ta e="T170" id="Seg_3219" s="T169">werden-PST2.[3SG]</ta>
            <ta e="T171" id="Seg_3220" s="T170">Alte.[NOM]</ta>
            <ta e="T172" id="Seg_3221" s="T171">Fenster-ACC</ta>
            <ta e="T173" id="Seg_3222" s="T172">Rauchloch-ACC</ta>
            <ta e="T174" id="Seg_3223" s="T173">verstopfen-PST2.[3SG]</ta>
            <ta e="T175" id="Seg_3224" s="T174">Tür-ACC</ta>
            <ta e="T176" id="Seg_3225" s="T175">versperren-PST2.[3SG]</ta>
            <ta e="T177" id="Seg_3226" s="T176">Hase.[NOM]</ta>
            <ta e="T178" id="Seg_3227" s="T177">Schamane-3SG.[NOM]</ta>
            <ta e="T179" id="Seg_3228" s="T178">schamanisieren-PST2.[3SG]</ta>
            <ta e="T180" id="Seg_3229" s="T179">bald</ta>
            <ta e="T181" id="Seg_3230" s="T180">springen-PST2.[3SG]</ta>
            <ta e="T182" id="Seg_3231" s="T181">alter.Mann.[NOM]</ta>
            <ta e="T183" id="Seg_3232" s="T182">stärker.werden-PST2.[3SG]</ta>
            <ta e="T184" id="Seg_3233" s="T183">delirieren-CVB.SIM</ta>
            <ta e="T332" id="Seg_3234" s="T184">eh</ta>
            <ta e="T185" id="Seg_3235" s="T332">eh</ta>
            <ta e="T187" id="Seg_3236" s="T186">sagen-PST2.[3SG]</ta>
            <ta e="T188" id="Seg_3237" s="T187">Alte.[NOM]</ta>
            <ta e="T189" id="Seg_3238" s="T188">springen-CVB.SEQ</ta>
            <ta e="T190" id="Seg_3239" s="T189">aufstehen-CVB.SEQ</ta>
            <ta e="T191" id="Seg_3240" s="T190">alter.Mann-DAT/LOC</ta>
            <ta e="T192" id="Seg_3241" s="T191">Haken.für.den.Kessel-ACC</ta>
            <ta e="T193" id="Seg_3242" s="T192">überreichen-PST2.[3SG]</ta>
            <ta e="T333" id="Seg_3243" s="T193">eh</ta>
            <ta e="T194" id="Seg_3244" s="T333">eh</ta>
            <ta e="T196" id="Seg_3245" s="T195">sagen-PST2.[3SG]</ta>
            <ta e="T334" id="Seg_3246" s="T196">eh</ta>
            <ta e="T197" id="Seg_3247" s="T334">eh</ta>
            <ta e="T199" id="Seg_3248" s="T198">so</ta>
            <ta e="T200" id="Seg_3249" s="T199">sagen-CVB.ANT-3SG</ta>
            <ta e="T201" id="Seg_3250" s="T200">Alte-3SG.[NOM]</ta>
            <ta e="T202" id="Seg_3251" s="T201">Lederschaber-ACC</ta>
            <ta e="T203" id="Seg_3252" s="T202">greifen-PST2.[3SG]</ta>
            <ta e="T204" id="Seg_3253" s="T203">Nachbar-3SG.[NOM]</ta>
            <ta e="T205" id="Seg_3254" s="T204">kleines.Beil-ACC</ta>
            <ta e="T206" id="Seg_3255" s="T205">greifen-PST2.[3SG]</ta>
            <ta e="T207" id="Seg_3256" s="T206">Schamane-3PL.[NOM]</ta>
            <ta e="T208" id="Seg_3257" s="T207">springen-CVB.SIM</ta>
            <ta e="T209" id="Seg_3258" s="T208">stehen-PRS.[3SG]</ta>
            <ta e="T215" id="Seg_3259" s="T214">sagen-CVB.SIM-sagen-CVB.SIM</ta>
            <ta e="T216" id="Seg_3260" s="T215">jenes.[NOM]</ta>
            <ta e="T217" id="Seg_3261" s="T216">Name-3SG.[NOM]</ta>
            <ta e="T218" id="Seg_3262" s="T217">dieses</ta>
            <ta e="T219" id="Seg_3263" s="T218">Zelt-ABL</ta>
            <ta e="T220" id="Seg_3264" s="T219">hinausgehen-PTCP.FUT</ta>
            <ta e="T221" id="Seg_3265" s="T220">sein-PTCP.FUT</ta>
            <ta e="T223" id="Seg_3266" s="T221">sein-COND-1SG</ta>
            <ta e="T224" id="Seg_3267" s="T223">sagen-CVB.SEQ</ta>
            <ta e="T225" id="Seg_3268" s="T224">Ewenke-SIM</ta>
            <ta e="T226" id="Seg_3269" s="T225">alter.Mann.[NOM]</ta>
            <ta e="T227" id="Seg_3270" s="T226">jenes-ACC</ta>
            <ta e="T228" id="Seg_3271" s="T227">hören-CVB.ANT</ta>
            <ta e="T229" id="Seg_3272" s="T228">springen-CVB.SEQ</ta>
            <ta e="T230" id="Seg_3273" s="T229">aufstehen-PRS.[3SG]</ta>
            <ta e="T231" id="Seg_3274" s="T230">Schamane-3PL.[NOM]</ta>
            <ta e="T232" id="Seg_3275" s="T231">springen-PST2.[3SG]</ta>
            <ta e="T233" id="Seg_3276" s="T232">Stange.für.den.Kessel-DAT/LOC</ta>
            <ta e="T234" id="Seg_3277" s="T233">alter.Mann.[NOM]</ta>
            <ta e="T235" id="Seg_3278" s="T234">Haken.für.den.Kessel-3SG-INSTR</ta>
            <ta e="T236" id="Seg_3279" s="T235">Stange.für.den.Kessel-DAT/LOC</ta>
            <ta e="T237" id="Seg_3280" s="T236">drücken-CVB.SIM</ta>
            <ta e="T238" id="Seg_3281" s="T237">schlagen-PRS.[3SG]</ta>
            <ta e="T239" id="Seg_3282" s="T238">Schamane-Hase.[NOM]</ta>
            <ta e="T240" id="Seg_3283" s="T239">sich.drehen-CVB.SEQ</ta>
            <ta e="T241" id="Seg_3284" s="T240">geben-PST2.[3SG]</ta>
            <ta e="T242" id="Seg_3285" s="T241">zwei</ta>
            <ta e="T243" id="Seg_3286" s="T242">Ohr-3SG-ACC</ta>
            <ta e="T244" id="Seg_3287" s="T243">nur</ta>
            <ta e="T245" id="Seg_3288" s="T244">quer.über</ta>
            <ta e="T246" id="Seg_3289" s="T245">schlagen-EP-PST2.[3SG]</ta>
            <ta e="T247" id="Seg_3290" s="T246">Hase.[NOM]</ta>
            <ta e="T248" id="Seg_3291" s="T247">Schamane-3SG.[NOM]</ta>
            <ta e="T249" id="Seg_3292" s="T248">Winter.[NOM]</ta>
            <ta e="T250" id="Seg_3293" s="T249">Holz-DAT/LOC</ta>
            <ta e="T251" id="Seg_3294" s="T250">springen-PST2.[3SG]</ta>
            <ta e="T252" id="Seg_3295" s="T251">springen-CVB.ANT-3SG</ta>
            <ta e="T253" id="Seg_3296" s="T252">Alte.[NOM]</ta>
            <ta e="T254" id="Seg_3297" s="T253">Lederschaber-3SG-INSTR</ta>
            <ta e="T255" id="Seg_3298" s="T254">schlagen-PRS.[3SG]</ta>
            <ta e="T256" id="Seg_3299" s="T255">hinterer</ta>
            <ta e="T257" id="Seg_3300" s="T256">nur</ta>
            <ta e="T258" id="Seg_3301" s="T257">Bein-3SG-DAT/LOC</ta>
            <ta e="T259" id="Seg_3302" s="T258">zwei</ta>
            <ta e="T260" id="Seg_3303" s="T259">hinterer</ta>
            <ta e="T261" id="Seg_3304" s="T260">Bein-3SG-ACC</ta>
            <ta e="T262" id="Seg_3305" s="T261">brechen-PRS.[3SG]</ta>
            <ta e="T263" id="Seg_3306" s="T262">Hase.[NOM]</ta>
            <ta e="T264" id="Seg_3307" s="T263">Schamane-3SG.[NOM]</ta>
            <ta e="T265" id="Seg_3308" s="T264">Winter.[NOM]</ta>
            <ta e="T266" id="Seg_3309" s="T265">Holz-ABL</ta>
            <ta e="T267" id="Seg_3310" s="T266">Rauchloch-3SG-INSTR</ta>
            <ta e="T268" id="Seg_3311" s="T267">springen-PRS.[3SG]</ta>
            <ta e="T269" id="Seg_3312" s="T268">jenes-DAT/LOC</ta>
            <ta e="T270" id="Seg_3313" s="T269">Hase.[NOM]</ta>
            <ta e="T271" id="Seg_3314" s="T270">Alte-3SG.[NOM]</ta>
            <ta e="T272" id="Seg_3315" s="T271">auch</ta>
            <ta e="T273" id="Seg_3316" s="T272">Rauchloch-3SG-INSTR</ta>
            <ta e="T274" id="Seg_3317" s="T273">springen-PST2.[3SG]</ta>
            <ta e="T275" id="Seg_3318" s="T274">Haus-DAT/LOC</ta>
            <ta e="T276" id="Seg_3319" s="T275">es.gibt</ta>
            <ta e="T277" id="Seg_3320" s="T276">Hase-PL-ACC</ta>
            <ta e="T278" id="Seg_3321" s="T277">jeder-3PL-ACC</ta>
            <ta e="T279" id="Seg_3322" s="T278">vollständig</ta>
            <ta e="T280" id="Seg_3323" s="T279">töten-ITER-CVB.SEQ</ta>
            <ta e="T281" id="Seg_3324" s="T280">werfen-PST2-3PL</ta>
            <ta e="T282" id="Seg_3325" s="T281">dieses-ACC</ta>
            <ta e="T283" id="Seg_3326" s="T282">essen-CVB.SEQ-3PL</ta>
            <ta e="T284" id="Seg_3327" s="T283">Frühling.[NOM]</ta>
            <ta e="T285" id="Seg_3328" s="T284">Jahr.[NOM]</ta>
            <ta e="T286" id="Seg_3329" s="T285">wärmer.werden-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T287" id="Seg_3330" s="T286">ankommen-PST2-3PL</ta>
            <ta e="T288" id="Seg_3331" s="T287">früher</ta>
            <ta e="T289" id="Seg_3332" s="T288">Hase-ACC</ta>
            <ta e="T290" id="Seg_3333" s="T289">essen-NEG.PTCP</ta>
            <ta e="T291" id="Seg_3334" s="T290">man.sagt-3PL</ta>
            <ta e="T292" id="Seg_3335" s="T291">dieses.EMPH-ABL</ta>
            <ta e="T293" id="Seg_3336" s="T292">doch</ta>
            <ta e="T294" id="Seg_3337" s="T293">Hase-ACC</ta>
            <ta e="T295" id="Seg_3338" s="T294">essen-PTCP.PRS</ta>
            <ta e="T296" id="Seg_3339" s="T295">werden-PST2-3PL</ta>
            <ta e="T297" id="Seg_3340" s="T296">Leute-PL.[NOM]</ta>
            <ta e="T298" id="Seg_3341" s="T297">entfliehen-PTCP.PST</ta>
            <ta e="T299" id="Seg_3342" s="T298">Hase.[NOM]</ta>
            <ta e="T300" id="Seg_3343" s="T299">Schamane-3SG-ABL</ta>
            <ta e="T301" id="Seg_3344" s="T300">wild</ta>
            <ta e="T302" id="Seg_3345" s="T301">Hase.[NOM]</ta>
            <ta e="T303" id="Seg_3346" s="T302">entstehen-PST2.[3SG]</ta>
            <ta e="T304" id="Seg_3347" s="T303">Wort-POSS</ta>
            <ta e="T305" id="Seg_3348" s="T304">NEG</ta>
            <ta e="T306" id="Seg_3349" s="T305">NEG</ta>
            <ta e="T307" id="Seg_3350" s="T306">werden-PST2.[3SG]</ta>
            <ta e="T308" id="Seg_3351" s="T307">Leute-ABL</ta>
            <ta e="T309" id="Seg_3352" s="T308">weglaufen-PTCP.PRS</ta>
            <ta e="T310" id="Seg_3353" s="T309">werden-PST2.[3SG]</ta>
            <ta e="T311" id="Seg_3354" s="T310">eben.der</ta>
            <ta e="T312" id="Seg_3355" s="T311">alter.Mann.[NOM]</ta>
            <ta e="T313" id="Seg_3356" s="T312">schlagen-EP-PTCP.PST-3SG-ABL</ta>
            <ta e="T314" id="Seg_3357" s="T313">Hase.[NOM]</ta>
            <ta e="T315" id="Seg_3358" s="T314">Ohr-3SG-GEN</ta>
            <ta e="T316" id="Seg_3359" s="T315">Spitze-3SG.[NOM]</ta>
            <ta e="T317" id="Seg_3360" s="T316">Zeit-ABL</ta>
            <ta e="T318" id="Seg_3361" s="T317">Zeit-DAT/LOC</ta>
            <ta e="T319" id="Seg_3362" s="T318">Kennzeichen-PROPR.[NOM]</ta>
            <ta e="T320" id="Seg_3363" s="T319">werden-PST2.[3SG]</ta>
            <ta e="T321" id="Seg_3364" s="T320">Alte.[NOM]</ta>
            <ta e="T322" id="Seg_3365" s="T321">kaputt</ta>
            <ta e="T323" id="Seg_3366" s="T322">schlagen-EP-PTCP.PST</ta>
            <ta e="T324" id="Seg_3367" s="T323">hinterer</ta>
            <ta e="T325" id="Seg_3368" s="T324">Bein-3SG.[NOM]</ta>
            <ta e="T326" id="Seg_3369" s="T325">Seite-3SG-INSTR</ta>
            <ta e="T327" id="Seg_3370" s="T326">Gelenk.[NOM]</ta>
            <ta e="T328" id="Seg_3371" s="T327">werden-PST2.[3SG]</ta>
            <ta e="T329" id="Seg_3372" s="T328">letzter-3SG.[NOM]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_3373" s="T0">давно</ta>
            <ta e="T2" id="Seg_3374" s="T1">люди.[NOM]</ta>
            <ta e="T3" id="Seg_3375" s="T2">жить-PST2.[3SG]</ta>
            <ta e="T4" id="Seg_3376" s="T3">эвенк-PL.[NOM]</ta>
            <ta e="T5" id="Seg_3377" s="T4">один</ta>
            <ta e="T6" id="Seg_3378" s="T5">старик-PROPR-3PL</ta>
            <ta e="T7" id="Seg_3379" s="T6">этот-3SG-3PL.[NOM]</ta>
            <ta e="T8" id="Seg_3380" s="T7">руководитель.[NOM]</ta>
            <ta e="T9" id="Seg_3381" s="T8">быть-PST1-3SG</ta>
            <ta e="T10" id="Seg_3382" s="T9">3SG.[NOM]</ta>
            <ta e="T11" id="Seg_3383" s="T10">совет-3SG-ACC</ta>
            <ta e="T12" id="Seg_3384" s="T11">резать-NEG.PTCP</ta>
            <ta e="T13" id="Seg_3385" s="T12">быть-PST1-3PL</ta>
            <ta e="T14" id="Seg_3386" s="T13">какой</ta>
            <ta e="T15" id="Seg_3387" s="T14">место-DAT/LOC</ta>
            <ta e="T16" id="Seg_3388" s="T15">жить.постоянно-PTCP.PRS-3PL-ACC</ta>
            <ta e="T17" id="Seg_3389" s="T16">советовать-PRS.[3SG]</ta>
            <ta e="T18" id="Seg_3390" s="T17">лес-PROPR</ta>
            <ta e="T19" id="Seg_3391" s="T18">теплый</ta>
            <ta e="T20" id="Seg_3392" s="T19">место-DAT/LOC</ta>
            <ta e="T21" id="Seg_3393" s="T20">такой</ta>
            <ta e="T22" id="Seg_3394" s="T21">место-DAT/LOC</ta>
            <ta e="T23" id="Seg_3395" s="T22">жизнь-INCH-FUT-1PL</ta>
            <ta e="T24" id="Seg_3396" s="T23">говорить-PRS.[3SG]</ta>
            <ta e="T25" id="Seg_3397" s="T24">туда</ta>
            <ta e="T26" id="Seg_3398" s="T25">доезжать-CVB.SEQ</ta>
            <ta e="T27" id="Seg_3399" s="T26">поселиться-PST2-3PL</ta>
            <ta e="T28" id="Seg_3400" s="T27">юрта.[NOM]</ta>
            <ta e="T29" id="Seg_3401" s="T28">построить-EP-PST2-3PL</ta>
            <ta e="T30" id="Seg_3402" s="T29">некоторый-PL.[NOM]</ta>
            <ta e="T31" id="Seg_3403" s="T30">четырехугольный.чум.[NOM]</ta>
            <ta e="T32" id="Seg_3404" s="T31">три</ta>
            <ta e="T33" id="Seg_3405" s="T32">сосед.[NOM]</ta>
            <ta e="T34" id="Seg_3406" s="T33">сожитель.[NOM]</ta>
            <ta e="T35" id="Seg_3407" s="T34">становиться-PST2-3PL</ta>
            <ta e="T36" id="Seg_3408" s="T35">когда</ta>
            <ta e="T37" id="Seg_3409" s="T36">INDEF</ta>
            <ta e="T38" id="Seg_3410" s="T37">запас.[NOM]</ta>
            <ta e="T39" id="Seg_3411" s="T38">мясо-3PL.[NOM]</ta>
            <ta e="T40" id="Seg_3412" s="T39">кончаться-PST2.[3SG]</ta>
            <ta e="T41" id="Seg_3413" s="T40">этот</ta>
            <ta e="T42" id="Seg_3414" s="T41">человек-PL.[NOM]</ta>
            <ta e="T43" id="Seg_3415" s="T42">голодать-PST2-3PL</ta>
            <ta e="T44" id="Seg_3416" s="T43">олень-3PL-ACC</ta>
            <ta e="T45" id="Seg_3417" s="T44">есть-PTCP.FUT-3PL-ACC</ta>
            <ta e="T46" id="Seg_3418" s="T45">олень-3PL.[NOM]</ta>
            <ta e="T47" id="Seg_3419" s="T46">небольшое.количество.[NOM]</ta>
            <ta e="T48" id="Seg_3420" s="T47">последний-3SG.[NOM]</ta>
            <ta e="T49" id="Seg_3421" s="T48">пять</ta>
            <ta e="T50" id="Seg_3422" s="T49">четыре</ta>
            <ta e="T51" id="Seg_3423" s="T50">MOD</ta>
            <ta e="T52" id="Seg_3424" s="T51">старик.[NOM]</ta>
            <ta e="T53" id="Seg_3425" s="T52">советовать-PRS.[3SG]</ta>
            <ta e="T54" id="Seg_3426" s="T53">олень-1PL-ACC</ta>
            <ta e="T55" id="Seg_3427" s="T54">есть-TEMP-1PL</ta>
            <ta e="T56" id="Seg_3428" s="T55">земля.[NOM]</ta>
            <ta e="T57" id="Seg_3429" s="T56">бедный.[NOM]</ta>
            <ta e="T58" id="Seg_3430" s="T57">быть-FUT-1PL</ta>
            <ta e="T59" id="Seg_3431" s="T58">есть-NEG-IMP.1PL</ta>
            <ta e="T60" id="Seg_3432" s="T59">олень-ACC</ta>
            <ta e="T61" id="Seg_3433" s="T60">советовать-FUT-1SG</ta>
            <ta e="T62" id="Seg_3434" s="T61">здесь</ta>
            <ta e="T63" id="Seg_3435" s="T62">этот</ta>
            <ta e="T64" id="Seg_3436" s="T63">река-DAT/LOC</ta>
            <ta e="T65" id="Seg_3437" s="T64">сосед-PL-1PL.[NOM]</ta>
            <ta e="T66" id="Seg_3438" s="T65">заяц-PL.[NOM]</ta>
            <ta e="T67" id="Seg_3439" s="T66">есть-3PL</ta>
            <ta e="T68" id="Seg_3440" s="T67">там</ta>
            <ta e="T69" id="Seg_3441" s="T68">шаман-PROPR-3PL</ta>
            <ta e="T70" id="Seg_3442" s="T69">1PL.[NOM]</ta>
            <ta e="T71" id="Seg_3443" s="T70">обманывать-CVB.SEQ-1PL</ta>
            <ta e="T72" id="Seg_3444" s="T71">сюда</ta>
            <ta e="T73" id="Seg_3445" s="T72">принести-PTCP.FUT-DAT/LOC</ta>
            <ta e="T74" id="Seg_3446" s="T73">3PL-ACC</ta>
            <ta e="T75" id="Seg_3447" s="T74">1SG.[NOM]</ta>
            <ta e="T76" id="Seg_3448" s="T75">быть.больным-EP-PTCP.PST</ta>
            <ta e="T77" id="Seg_3449" s="T76">быть-IMP.1SG</ta>
            <ta e="T78" id="Seg_3450" s="T77">шаман-3PL-ACC</ta>
            <ta e="T79" id="Seg_3451" s="T78">зазывать-EP-IMP.2PL</ta>
            <ta e="T80" id="Seg_3452" s="T79">3SG-ACC</ta>
            <ta e="T81" id="Seg_3453" s="T80">с</ta>
            <ta e="T82" id="Seg_3454" s="T81">много</ta>
            <ta e="T83" id="Seg_3455" s="T82">заяц.[NOM]</ta>
            <ta e="T84" id="Seg_3456" s="T83">приходить-FUT.[3SG]</ta>
            <ta e="T85" id="Seg_3457" s="T84">вот</ta>
            <ta e="T86" id="Seg_3458" s="T85">тот-PL-ACC</ta>
            <ta e="T87" id="Seg_3459" s="T86">1PL.[NOM]</ta>
            <ta e="T88" id="Seg_3460" s="T87">убить-MED-FUT-1PL</ta>
            <ta e="T89" id="Seg_3461" s="T88">и</ta>
            <ta e="T90" id="Seg_3462" s="T89">есть-FUT-1PL</ta>
            <ta e="T91" id="Seg_3463" s="T90">тогда</ta>
            <ta e="T92" id="Seg_3464" s="T91">передняя.часть.[NOM]</ta>
            <ta e="T93" id="Seg_3465" s="T92">в.сторону</ta>
            <ta e="T94" id="Seg_3466" s="T93">перешагивать-FUT-1PL</ta>
            <ta e="T95" id="Seg_3467" s="T94">год.[NOM]</ta>
            <ta e="T96" id="Seg_3468" s="T95">стать.теплее-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T97" id="Seg_3469" s="T96">доезжать-FUT-1PL</ta>
            <ta e="T98" id="Seg_3470" s="T97">охота-EP-INSTR</ta>
            <ta e="T99" id="Seg_3471" s="T98">есть-FUT-1PL</ta>
            <ta e="T100" id="Seg_3472" s="T99">человек-3PL.[NOM]</ta>
            <ta e="T101" id="Seg_3473" s="T100">заяц-3PL-ACC</ta>
            <ta e="T102" id="Seg_3474" s="T101">искать-CVB.SIM</ta>
            <ta e="T103" id="Seg_3475" s="T102">идти-PST1-3PL</ta>
            <ta e="T104" id="Seg_3476" s="T103">старик.[NOM]</ta>
            <ta e="T105" id="Seg_3477" s="T104">быть.больным-EP-PST2-3SG</ta>
            <ta e="T106" id="Seg_3478" s="T105">быть-PST1-3SG</ta>
            <ta e="T107" id="Seg_3479" s="T106">обувь-COM</ta>
            <ta e="T108" id="Seg_3480" s="T107">одеяло.[NOM]</ta>
            <ta e="T109" id="Seg_3481" s="T108">нутро-3SG-DAT/LOC</ta>
            <ta e="T110" id="Seg_3482" s="T109">ложиться-PST1-3SG</ta>
            <ta e="T111" id="Seg_3483" s="T110">старуха-3SG-DAT/LOC</ta>
            <ta e="T112" id="Seg_3484" s="T111">говорить-PRS.[3SG]</ta>
            <ta e="T113" id="Seg_3485" s="T112">заяц.[NOM]</ta>
            <ta e="T114" id="Seg_3486" s="T113">шаман-3SG.[NOM]</ta>
            <ta e="T115" id="Seg_3487" s="T114">приходить-CVB.SEQ</ta>
            <ta e="T116" id="Seg_3488" s="T115">камлать-TEMP-3SG</ta>
            <ta e="T117" id="Seg_3489" s="T116">окно-2SG-ACC</ta>
            <ta e="T118" id="Seg_3490" s="T117">дымоход-2SG-ACC</ta>
            <ta e="T119" id="Seg_3491" s="T118">закрывать-FUT.[IMP.2SG]</ta>
            <ta e="T120" id="Seg_3492" s="T119">дверь-2SG-ACC</ta>
            <ta e="T121" id="Seg_3493" s="T120">заложить-FUT.[IMP.2SG]</ta>
            <ta e="T122" id="Seg_3494" s="T121">бредить-CVB.SIM-бредить-CVB.SIM</ta>
            <ta e="T123" id="Seg_3495" s="T122">бредить-PST2-3SG</ta>
            <ta e="T124" id="Seg_3496" s="T123">быть-CVB.SEQ</ta>
            <ta e="T330" id="Seg_3497" s="T124">ыы</ta>
            <ta e="T331" id="Seg_3498" s="T330">ыы</ta>
            <ta e="T125" id="Seg_3499" s="T331">ы</ta>
            <ta e="T127" id="Seg_3500" s="T126">говорить-FUT-EP-1SG</ta>
            <ta e="T128" id="Seg_3501" s="T127">тогда</ta>
            <ta e="T129" id="Seg_3502" s="T128">крюк.для.котла-ACC</ta>
            <ta e="T130" id="Seg_3503" s="T129">подать-FUT.[IMP.2SG]</ta>
            <ta e="T132" id="Seg_3504" s="T130">1SG-DAT/LOC</ta>
            <ta e="T335" id="Seg_3505" s="T132">ы-ы</ta>
            <ta e="T134" id="Seg_3506" s="T133">говорить-FUT-EP-1SG</ta>
            <ta e="T135" id="Seg_3507" s="T134">тогда</ta>
            <ta e="T136" id="Seg_3508" s="T135">2SG.[NOM]</ta>
            <ta e="T137" id="Seg_3509" s="T136">кожемялка-ACC</ta>
            <ta e="T138" id="Seg_3510" s="T137">хватать-FUT.[IMP.2SG]</ta>
            <ta e="T139" id="Seg_3511" s="T138">дверь-DAT/LOC</ta>
            <ta e="T140" id="Seg_3512" s="T139">стать-FUT.[IMP.2SG]</ta>
            <ta e="T141" id="Seg_3513" s="T140">тогда</ta>
            <ta e="T142" id="Seg_3514" s="T141">1SG.[NOM]</ta>
            <ta e="T143" id="Seg_3515" s="T142">прыгать-CVB.SEQ</ta>
            <ta e="T144" id="Seg_3516" s="T143">стоять-CVB.SEQ</ta>
            <ta e="T145" id="Seg_3517" s="T144">ударять-ITER-CVB.SEQ</ta>
            <ta e="T146" id="Seg_3518" s="T145">идти-FUT-1SG</ta>
            <ta e="T147" id="Seg_3519" s="T146">шаман-3PL-ACC</ta>
            <ta e="T148" id="Seg_3520" s="T147">уложить-FUT-1SG</ta>
            <ta e="T149" id="Seg_3521" s="T148">2SG.[NOM]</ta>
            <ta e="T150" id="Seg_3522" s="T149">тоже</ta>
            <ta e="T151" id="Seg_3523" s="T150">поймать-EP-INCH-FUT.[IMP.2SG]</ta>
            <ta e="T152" id="Seg_3524" s="T151">старик.[NOM]</ta>
            <ta e="T153" id="Seg_3525" s="T152">ложиться-PRS.[3SG]</ta>
            <ta e="T154" id="Seg_3526" s="T153">бредить-PRS.[3SG]</ta>
            <ta e="T155" id="Seg_3527" s="T154">человек-ACC</ta>
            <ta e="T156" id="Seg_3528" s="T155">знать-NEG.PTCP</ta>
            <ta e="T157" id="Seg_3529" s="T156">быть-CVB.SIM-быть-CVB.SIM</ta>
            <ta e="T158" id="Seg_3530" s="T157">заяц.[NOM]</ta>
            <ta e="T159" id="Seg_3531" s="T158">шаман-3SG.[NOM]</ta>
            <ta e="T160" id="Seg_3532" s="T159">приходить-PRS.[3SG]</ta>
            <ta e="T161" id="Seg_3533" s="T160">3SG-ACC</ta>
            <ta e="T162" id="Seg_3534" s="T161">с</ta>
            <ta e="T163" id="Seg_3535" s="T162">много</ta>
            <ta e="T164" id="Seg_3536" s="T163">заяц.[NOM]</ta>
            <ta e="T165" id="Seg_3537" s="T164">приходить-PST2.[3SG]</ta>
            <ta e="T166" id="Seg_3538" s="T165">юрта.[NOM]</ta>
            <ta e="T167" id="Seg_3539" s="T166">нутро-3SG.[NOM]</ta>
            <ta e="T168" id="Seg_3540" s="T167">заяц.[NOM]</ta>
            <ta e="T169" id="Seg_3541" s="T168">только</ta>
            <ta e="T170" id="Seg_3542" s="T169">становиться-PST2.[3SG]</ta>
            <ta e="T171" id="Seg_3543" s="T170">старуха.[NOM]</ta>
            <ta e="T172" id="Seg_3544" s="T171">окно-ACC</ta>
            <ta e="T173" id="Seg_3545" s="T172">дымоход-ACC</ta>
            <ta e="T174" id="Seg_3546" s="T173">заткнуть-PST2.[3SG]</ta>
            <ta e="T175" id="Seg_3547" s="T174">дверь-ACC</ta>
            <ta e="T176" id="Seg_3548" s="T175">заложить-PST2.[3SG]</ta>
            <ta e="T177" id="Seg_3549" s="T176">заяц.[NOM]</ta>
            <ta e="T178" id="Seg_3550" s="T177">шаман-3SG.[NOM]</ta>
            <ta e="T179" id="Seg_3551" s="T178">камлать-PST2.[3SG]</ta>
            <ta e="T180" id="Seg_3552" s="T179">скоро</ta>
            <ta e="T181" id="Seg_3553" s="T180">прыгать-PST2.[3SG]</ta>
            <ta e="T182" id="Seg_3554" s="T181">старик.[NOM]</ta>
            <ta e="T183" id="Seg_3555" s="T182">усиливаться-PST2.[3SG]</ta>
            <ta e="T184" id="Seg_3556" s="T183">бредить-CVB.SIM</ta>
            <ta e="T332" id="Seg_3557" s="T184">ы</ta>
            <ta e="T185" id="Seg_3558" s="T332">ы</ta>
            <ta e="T187" id="Seg_3559" s="T186">говорить-PST2.[3SG]</ta>
            <ta e="T188" id="Seg_3560" s="T187">старуха.[NOM]</ta>
            <ta e="T189" id="Seg_3561" s="T188">прыгать-CVB.SEQ</ta>
            <ta e="T190" id="Seg_3562" s="T189">вставать-CVB.SEQ</ta>
            <ta e="T191" id="Seg_3563" s="T190">старик-DAT/LOC</ta>
            <ta e="T192" id="Seg_3564" s="T191">крюк.для.котла-ACC</ta>
            <ta e="T193" id="Seg_3565" s="T192">подать-PST2.[3SG]</ta>
            <ta e="T333" id="Seg_3566" s="T193">ы</ta>
            <ta e="T194" id="Seg_3567" s="T333">ы</ta>
            <ta e="T196" id="Seg_3568" s="T195">говорить-PST2.[3SG]</ta>
            <ta e="T334" id="Seg_3569" s="T196">ы</ta>
            <ta e="T197" id="Seg_3570" s="T334">ы</ta>
            <ta e="T199" id="Seg_3571" s="T198">так</ta>
            <ta e="T200" id="Seg_3572" s="T199">говорить-CVB.ANT-3SG</ta>
            <ta e="T201" id="Seg_3573" s="T200">старуха-3SG.[NOM]</ta>
            <ta e="T202" id="Seg_3574" s="T201">кожемялка-ACC</ta>
            <ta e="T203" id="Seg_3575" s="T202">хватать-PST2.[3SG]</ta>
            <ta e="T204" id="Seg_3576" s="T203">сосед-3SG.[NOM]</ta>
            <ta e="T205" id="Seg_3577" s="T204">маленький.топор-ACC</ta>
            <ta e="T206" id="Seg_3578" s="T205">хватать-PST2.[3SG]</ta>
            <ta e="T207" id="Seg_3579" s="T206">шаман-3PL.[NOM]</ta>
            <ta e="T208" id="Seg_3580" s="T207">прыгать-CVB.SIM</ta>
            <ta e="T209" id="Seg_3581" s="T208">стоять-PRS.[3SG]</ta>
            <ta e="T215" id="Seg_3582" s="T214">говорить-CVB.SIM-говорить-CVB.SIM</ta>
            <ta e="T216" id="Seg_3583" s="T215">тот.[NOM]</ta>
            <ta e="T217" id="Seg_3584" s="T216">имя-3SG.[NOM]</ta>
            <ta e="T218" id="Seg_3585" s="T217">этот</ta>
            <ta e="T219" id="Seg_3586" s="T218">чум-ABL</ta>
            <ta e="T220" id="Seg_3587" s="T219">выйти-PTCP.FUT</ta>
            <ta e="T221" id="Seg_3588" s="T220">быть-PTCP.FUT</ta>
            <ta e="T223" id="Seg_3589" s="T221">быть-COND-1SG</ta>
            <ta e="T224" id="Seg_3590" s="T223">говорить-CVB.SEQ</ta>
            <ta e="T225" id="Seg_3591" s="T224">эвенк-SIM</ta>
            <ta e="T226" id="Seg_3592" s="T225">старик.[NOM]</ta>
            <ta e="T227" id="Seg_3593" s="T226">тот-ACC</ta>
            <ta e="T228" id="Seg_3594" s="T227">слышать-CVB.ANT</ta>
            <ta e="T229" id="Seg_3595" s="T228">прыгать-CVB.SEQ</ta>
            <ta e="T230" id="Seg_3596" s="T229">вставать-PRS.[3SG]</ta>
            <ta e="T231" id="Seg_3597" s="T230">шаман-3PL.[NOM]</ta>
            <ta e="T232" id="Seg_3598" s="T231">прыгать-PST2.[3SG]</ta>
            <ta e="T233" id="Seg_3599" s="T232">шест.для.котла-DAT/LOC</ta>
            <ta e="T234" id="Seg_3600" s="T233">старик.[NOM]</ta>
            <ta e="T235" id="Seg_3601" s="T234">крюк.для.котла-3SG-INSTR</ta>
            <ta e="T236" id="Seg_3602" s="T235">шест.для.котла-DAT/LOC</ta>
            <ta e="T237" id="Seg_3603" s="T236">прижать-CVB.SIM</ta>
            <ta e="T238" id="Seg_3604" s="T237">бить-PRS.[3SG]</ta>
            <ta e="T239" id="Seg_3605" s="T238">шаман-заяц.[NOM]</ta>
            <ta e="T240" id="Seg_3606" s="T239">вертеться-CVB.SEQ</ta>
            <ta e="T241" id="Seg_3607" s="T240">давать-PST2.[3SG]</ta>
            <ta e="T242" id="Seg_3608" s="T241">два</ta>
            <ta e="T243" id="Seg_3609" s="T242">ухо-3SG-ACC</ta>
            <ta e="T244" id="Seg_3610" s="T243">только</ta>
            <ta e="T245" id="Seg_3611" s="T244">наискось</ta>
            <ta e="T246" id="Seg_3612" s="T245">бить-EP-PST2.[3SG]</ta>
            <ta e="T247" id="Seg_3613" s="T246">заяц.[NOM]</ta>
            <ta e="T248" id="Seg_3614" s="T247">шаман-3SG.[NOM]</ta>
            <ta e="T249" id="Seg_3615" s="T248">зима.[NOM]</ta>
            <ta e="T250" id="Seg_3616" s="T249">дерево-DAT/LOC</ta>
            <ta e="T251" id="Seg_3617" s="T250">прыгать-PST2.[3SG]</ta>
            <ta e="T252" id="Seg_3618" s="T251">прыгать-CVB.ANT-3SG</ta>
            <ta e="T253" id="Seg_3619" s="T252">старуха.[NOM]</ta>
            <ta e="T254" id="Seg_3620" s="T253">кожемялка-3SG-INSTR</ta>
            <ta e="T255" id="Seg_3621" s="T254">бить-PRS.[3SG]</ta>
            <ta e="T256" id="Seg_3622" s="T255">задний</ta>
            <ta e="T257" id="Seg_3623" s="T256">только</ta>
            <ta e="T258" id="Seg_3624" s="T257">нога-3SG-DAT/LOC</ta>
            <ta e="T259" id="Seg_3625" s="T258">два</ta>
            <ta e="T260" id="Seg_3626" s="T259">задний</ta>
            <ta e="T261" id="Seg_3627" s="T260">нога-3SG-ACC</ta>
            <ta e="T262" id="Seg_3628" s="T261">ломать-PRS.[3SG]</ta>
            <ta e="T263" id="Seg_3629" s="T262">заяц.[NOM]</ta>
            <ta e="T264" id="Seg_3630" s="T263">шаман-3SG.[NOM]</ta>
            <ta e="T265" id="Seg_3631" s="T264">зима.[NOM]</ta>
            <ta e="T266" id="Seg_3632" s="T265">дерево-ABL</ta>
            <ta e="T267" id="Seg_3633" s="T266">дымоход-3SG-INSTR</ta>
            <ta e="T268" id="Seg_3634" s="T267">прыгать-PRS.[3SG]</ta>
            <ta e="T269" id="Seg_3635" s="T268">тот-DAT/LOC</ta>
            <ta e="T270" id="Seg_3636" s="T269">заяц.[NOM]</ta>
            <ta e="T271" id="Seg_3637" s="T270">старуха-3SG.[NOM]</ta>
            <ta e="T272" id="Seg_3638" s="T271">тоже</ta>
            <ta e="T273" id="Seg_3639" s="T272">дымоход-3SG-INSTR</ta>
            <ta e="T274" id="Seg_3640" s="T273">прыгать-PST2.[3SG]</ta>
            <ta e="T275" id="Seg_3641" s="T274">дом-DAT/LOC</ta>
            <ta e="T276" id="Seg_3642" s="T275">есть</ta>
            <ta e="T277" id="Seg_3643" s="T276">заяц-PL-ACC</ta>
            <ta e="T278" id="Seg_3644" s="T277">каждый-3PL-ACC</ta>
            <ta e="T279" id="Seg_3645" s="T278">совсем</ta>
            <ta e="T280" id="Seg_3646" s="T279">убить-ITER-CVB.SEQ</ta>
            <ta e="T281" id="Seg_3647" s="T280">бросать-PST2-3PL</ta>
            <ta e="T282" id="Seg_3648" s="T281">этот-ACC</ta>
            <ta e="T283" id="Seg_3649" s="T282">есть-CVB.SEQ-3PL</ta>
            <ta e="T284" id="Seg_3650" s="T283">весна.[NOM]</ta>
            <ta e="T285" id="Seg_3651" s="T284">год.[NOM]</ta>
            <ta e="T286" id="Seg_3652" s="T285">стать.теплее-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T287" id="Seg_3653" s="T286">доезжать-PST2-3PL</ta>
            <ta e="T288" id="Seg_3654" s="T287">раньше</ta>
            <ta e="T289" id="Seg_3655" s="T288">заяц-ACC</ta>
            <ta e="T290" id="Seg_3656" s="T289">есть-NEG.PTCP</ta>
            <ta e="T291" id="Seg_3657" s="T290">говорят-3PL</ta>
            <ta e="T292" id="Seg_3658" s="T291">тот.EMPH-ABL</ta>
            <ta e="T293" id="Seg_3659" s="T292">вот</ta>
            <ta e="T294" id="Seg_3660" s="T293">заяц-ACC</ta>
            <ta e="T295" id="Seg_3661" s="T294">есть-PTCP.PRS</ta>
            <ta e="T296" id="Seg_3662" s="T295">становиться-PST2-3PL</ta>
            <ta e="T297" id="Seg_3663" s="T296">люди-PL.[NOM]</ta>
            <ta e="T298" id="Seg_3664" s="T297">спасаться-PTCP.PST</ta>
            <ta e="T299" id="Seg_3665" s="T298">заяц.[NOM]</ta>
            <ta e="T300" id="Seg_3666" s="T299">шаман-3SG-ABL</ta>
            <ta e="T301" id="Seg_3667" s="T300">дикий</ta>
            <ta e="T302" id="Seg_3668" s="T301">заяц.[NOM]</ta>
            <ta e="T303" id="Seg_3669" s="T302">создаваться-PST2.[3SG]</ta>
            <ta e="T304" id="Seg_3670" s="T303">слово-POSS</ta>
            <ta e="T305" id="Seg_3671" s="T304">NEG</ta>
            <ta e="T306" id="Seg_3672" s="T305">NEG</ta>
            <ta e="T307" id="Seg_3673" s="T306">становиться-PST2.[3SG]</ta>
            <ta e="T308" id="Seg_3674" s="T307">люди-ABL</ta>
            <ta e="T309" id="Seg_3675" s="T308">убегать-PTCP.PRS</ta>
            <ta e="T310" id="Seg_3676" s="T309">становиться-PST2.[3SG]</ta>
            <ta e="T311" id="Seg_3677" s="T310">тот.самый</ta>
            <ta e="T312" id="Seg_3678" s="T311">старик.[NOM]</ta>
            <ta e="T313" id="Seg_3679" s="T312">бить-EP-PTCP.PST-3SG-ABL</ta>
            <ta e="T314" id="Seg_3680" s="T313">заяц.[NOM]</ta>
            <ta e="T315" id="Seg_3681" s="T314">ухо-3SG-GEN</ta>
            <ta e="T316" id="Seg_3682" s="T315">верхушка-3SG.[NOM]</ta>
            <ta e="T317" id="Seg_3683" s="T316">время-ABL</ta>
            <ta e="T318" id="Seg_3684" s="T317">время-DAT/LOC</ta>
            <ta e="T319" id="Seg_3685" s="T318">метка-PROPR.[NOM]</ta>
            <ta e="T320" id="Seg_3686" s="T319">становиться-PST2.[3SG]</ta>
            <ta e="T321" id="Seg_3687" s="T320">старуха.[NOM]</ta>
            <ta e="T322" id="Seg_3688" s="T321">сломав</ta>
            <ta e="T323" id="Seg_3689" s="T322">бить-EP-PTCP.PST</ta>
            <ta e="T324" id="Seg_3690" s="T323">задний</ta>
            <ta e="T325" id="Seg_3691" s="T324">нога-3SG.[NOM]</ta>
            <ta e="T326" id="Seg_3692" s="T325">сторона-3SG-INSTR</ta>
            <ta e="T327" id="Seg_3693" s="T326">сустав.[NOM]</ta>
            <ta e="T328" id="Seg_3694" s="T327">становиться-PST2.[3SG]</ta>
            <ta e="T329" id="Seg_3695" s="T328">последний-3SG.[NOM]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_3696" s="T0">adv</ta>
            <ta e="T2" id="Seg_3697" s="T1">n-n:case</ta>
            <ta e="T3" id="Seg_3698" s="T2">v-v:tense-v:pred.pn</ta>
            <ta e="T4" id="Seg_3699" s="T3">n-n:(num)-n:case</ta>
            <ta e="T5" id="Seg_3700" s="T4">cardnum</ta>
            <ta e="T6" id="Seg_3701" s="T5">n-n&gt;adj-n:(pred.pn)</ta>
            <ta e="T7" id="Seg_3702" s="T6">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T8" id="Seg_3703" s="T7">n-n:case</ta>
            <ta e="T9" id="Seg_3704" s="T8">v-v:tense-v:poss.pn</ta>
            <ta e="T10" id="Seg_3705" s="T9">pers-pro:case</ta>
            <ta e="T11" id="Seg_3706" s="T10">n-n:poss-n:case</ta>
            <ta e="T12" id="Seg_3707" s="T11">v-v:ptcp</ta>
            <ta e="T13" id="Seg_3708" s="T12">v-v:tense-v:pred.pn</ta>
            <ta e="T14" id="Seg_3709" s="T13">que</ta>
            <ta e="T15" id="Seg_3710" s="T14">n-n:case</ta>
            <ta e="T16" id="Seg_3711" s="T15">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T17" id="Seg_3712" s="T16">v-v:tense-v:pred.pn</ta>
            <ta e="T18" id="Seg_3713" s="T17">n-n&gt;adj</ta>
            <ta e="T19" id="Seg_3714" s="T18">adj</ta>
            <ta e="T20" id="Seg_3715" s="T19">n-n:case</ta>
            <ta e="T21" id="Seg_3716" s="T20">dempro</ta>
            <ta e="T22" id="Seg_3717" s="T21">n-n:case</ta>
            <ta e="T23" id="Seg_3718" s="T22">n-n&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T24" id="Seg_3719" s="T23">v-v:tense-v:pred.pn</ta>
            <ta e="T25" id="Seg_3720" s="T24">adv</ta>
            <ta e="T26" id="Seg_3721" s="T25">v-v:cvb</ta>
            <ta e="T27" id="Seg_3722" s="T26">v-v:tense-v:pred.pn</ta>
            <ta e="T28" id="Seg_3723" s="T27">n-n:case</ta>
            <ta e="T29" id="Seg_3724" s="T28">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T30" id="Seg_3725" s="T29">indfpro-pro:(num)-pro:case</ta>
            <ta e="T31" id="Seg_3726" s="T30">n-n:case</ta>
            <ta e="T32" id="Seg_3727" s="T31">cardnum</ta>
            <ta e="T33" id="Seg_3728" s="T32">n-n:case</ta>
            <ta e="T34" id="Seg_3729" s="T33">n-n:case</ta>
            <ta e="T35" id="Seg_3730" s="T34">v-v:tense-v:pred.pn</ta>
            <ta e="T36" id="Seg_3731" s="T35">que</ta>
            <ta e="T37" id="Seg_3732" s="T36">ptcl</ta>
            <ta e="T38" id="Seg_3733" s="T37">n-n:case</ta>
            <ta e="T39" id="Seg_3734" s="T38">n-n:(poss)-n:case</ta>
            <ta e="T40" id="Seg_3735" s="T39">v-v:tense-v:pred.pn</ta>
            <ta e="T41" id="Seg_3736" s="T40">dempro</ta>
            <ta e="T42" id="Seg_3737" s="T41">n-n:(num)-n:case</ta>
            <ta e="T43" id="Seg_3738" s="T42">v-v:tense-v:pred.pn</ta>
            <ta e="T44" id="Seg_3739" s="T43">n-n:poss-n:case</ta>
            <ta e="T45" id="Seg_3740" s="T44">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T46" id="Seg_3741" s="T45">n-n:(poss)-n:case</ta>
            <ta e="T47" id="Seg_3742" s="T46">n-n:case</ta>
            <ta e="T48" id="Seg_3743" s="T47">adj-n:(poss)-n:case</ta>
            <ta e="T49" id="Seg_3744" s="T48">cardnum</ta>
            <ta e="T50" id="Seg_3745" s="T49">cardnum</ta>
            <ta e="T51" id="Seg_3746" s="T50">ptcl</ta>
            <ta e="T52" id="Seg_3747" s="T51">n-n:case</ta>
            <ta e="T53" id="Seg_3748" s="T52">v-v:tense-v:pred.pn</ta>
            <ta e="T54" id="Seg_3749" s="T53">n-n:poss-n:case</ta>
            <ta e="T55" id="Seg_3750" s="T54">v-v:mood-v:temp.pn</ta>
            <ta e="T56" id="Seg_3751" s="T55">n-n:case</ta>
            <ta e="T57" id="Seg_3752" s="T56">adj-n:case</ta>
            <ta e="T58" id="Seg_3753" s="T57">v-v:tense-v:poss.pn</ta>
            <ta e="T59" id="Seg_3754" s="T58">v-v:(neg)-v:mood.pn</ta>
            <ta e="T60" id="Seg_3755" s="T59">n-n:case</ta>
            <ta e="T61" id="Seg_3756" s="T60">v-v:tense-v:poss.pn</ta>
            <ta e="T62" id="Seg_3757" s="T61">adv</ta>
            <ta e="T63" id="Seg_3758" s="T62">dempro</ta>
            <ta e="T64" id="Seg_3759" s="T63">n-n:case</ta>
            <ta e="T65" id="Seg_3760" s="T64">n-n:(num)-n:(poss)-n:case</ta>
            <ta e="T66" id="Seg_3761" s="T65">n-n:(num)-n:case</ta>
            <ta e="T67" id="Seg_3762" s="T66">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T68" id="Seg_3763" s="T67">adv</ta>
            <ta e="T69" id="Seg_3764" s="T68">n-n&gt;adj-n:(pred.pn)</ta>
            <ta e="T70" id="Seg_3765" s="T69">pers-pro:case</ta>
            <ta e="T71" id="Seg_3766" s="T70">v-v:cvb-v:pred.pn</ta>
            <ta e="T72" id="Seg_3767" s="T71">adv</ta>
            <ta e="T73" id="Seg_3768" s="T72">v-v:ptcp-v:(case)</ta>
            <ta e="T74" id="Seg_3769" s="T73">pers-pro:case</ta>
            <ta e="T75" id="Seg_3770" s="T74">pers-pro:case</ta>
            <ta e="T76" id="Seg_3771" s="T75">v-v:(ins)-v:ptcp</ta>
            <ta e="T77" id="Seg_3772" s="T76">v-v:mood.pn</ta>
            <ta e="T78" id="Seg_3773" s="T77">n-n:poss-n:case</ta>
            <ta e="T79" id="Seg_3774" s="T78">v-v:(ins)-v:mood.pn</ta>
            <ta e="T80" id="Seg_3775" s="T79">pers-pro:case</ta>
            <ta e="T81" id="Seg_3776" s="T80">post</ta>
            <ta e="T82" id="Seg_3777" s="T81">quant</ta>
            <ta e="T83" id="Seg_3778" s="T82">n-n:case</ta>
            <ta e="T84" id="Seg_3779" s="T83">v-v:tense-v:poss.pn</ta>
            <ta e="T85" id="Seg_3780" s="T84">ptcl</ta>
            <ta e="T86" id="Seg_3781" s="T85">dempro-pro:(num)-pro:case</ta>
            <ta e="T87" id="Seg_3782" s="T86">pers-pro:case</ta>
            <ta e="T88" id="Seg_3783" s="T87">v-v&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T89" id="Seg_3784" s="T88">conj</ta>
            <ta e="T90" id="Seg_3785" s="T89">v-v:tense-v:poss.pn</ta>
            <ta e="T91" id="Seg_3786" s="T90">adv</ta>
            <ta e="T92" id="Seg_3787" s="T91">n-n:case</ta>
            <ta e="T93" id="Seg_3788" s="T92">post</ta>
            <ta e="T94" id="Seg_3789" s="T93">v-v:tense-v:poss.pn</ta>
            <ta e="T95" id="Seg_3790" s="T94">n-n:case</ta>
            <ta e="T96" id="Seg_3791" s="T95">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T97" id="Seg_3792" s="T96">v-v:tense-v:poss.pn</ta>
            <ta e="T98" id="Seg_3793" s="T97">n-n:(ins)-n:case</ta>
            <ta e="T99" id="Seg_3794" s="T98">v-v:tense-v:poss.pn</ta>
            <ta e="T100" id="Seg_3795" s="T99">n-n:(poss)-n:case</ta>
            <ta e="T101" id="Seg_3796" s="T100">n-n:poss-n:case</ta>
            <ta e="T102" id="Seg_3797" s="T101">v-v:cvb</ta>
            <ta e="T103" id="Seg_3798" s="T102">v-v:tense-v:pred.pn</ta>
            <ta e="T104" id="Seg_3799" s="T103">n-n:case</ta>
            <ta e="T105" id="Seg_3800" s="T104">v-v:(ins)-v:tense-v:poss.pn</ta>
            <ta e="T106" id="Seg_3801" s="T105">v-v:tense-v:poss.pn</ta>
            <ta e="T107" id="Seg_3802" s="T106">n-n:case</ta>
            <ta e="T108" id="Seg_3803" s="T107">n-n:case</ta>
            <ta e="T109" id="Seg_3804" s="T108">n-n:poss-n:case</ta>
            <ta e="T110" id="Seg_3805" s="T109">v-v:tense-v:poss.pn</ta>
            <ta e="T111" id="Seg_3806" s="T110">n-n:poss-n:case</ta>
            <ta e="T112" id="Seg_3807" s="T111">v-v:tense-v:pred.pn</ta>
            <ta e="T113" id="Seg_3808" s="T112">n-n:case</ta>
            <ta e="T114" id="Seg_3809" s="T113">n-n:(poss)-n:case</ta>
            <ta e="T115" id="Seg_3810" s="T114">v-v:cvb</ta>
            <ta e="T116" id="Seg_3811" s="T115">v-v:mood-v:temp.pn</ta>
            <ta e="T117" id="Seg_3812" s="T116">n-n:poss-n:case</ta>
            <ta e="T118" id="Seg_3813" s="T117">n-n:poss-n:case</ta>
            <ta e="T119" id="Seg_3814" s="T118">v-v:(tense)-v:mood.pn</ta>
            <ta e="T120" id="Seg_3815" s="T119">n-n:poss-n:case</ta>
            <ta e="T121" id="Seg_3816" s="T120">v-v:(tense)-v:mood.pn</ta>
            <ta e="T122" id="Seg_3817" s="T121">v-v:cvb-v-v:cvb</ta>
            <ta e="T123" id="Seg_3818" s="T122">v-v:tense-v:poss.pn</ta>
            <ta e="T124" id="Seg_3819" s="T123">v-v:cvb</ta>
            <ta e="T330" id="Seg_3820" s="T124">interj</ta>
            <ta e="T331" id="Seg_3821" s="T330">interj</ta>
            <ta e="T125" id="Seg_3822" s="T331">interj</ta>
            <ta e="T127" id="Seg_3823" s="T126">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T128" id="Seg_3824" s="T127">adv</ta>
            <ta e="T129" id="Seg_3825" s="T128">n-n:case</ta>
            <ta e="T130" id="Seg_3826" s="T129">v-v:(tense)-v:mood.pn</ta>
            <ta e="T132" id="Seg_3827" s="T130">pers-pro:case</ta>
            <ta e="T335" id="Seg_3828" s="T132">interj</ta>
            <ta e="T134" id="Seg_3829" s="T133">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T135" id="Seg_3830" s="T134">adv</ta>
            <ta e="T136" id="Seg_3831" s="T135">pers-pro:case</ta>
            <ta e="T137" id="Seg_3832" s="T136">n-n:case</ta>
            <ta e="T138" id="Seg_3833" s="T137">v-v:(tense)-v:mood.pn</ta>
            <ta e="T139" id="Seg_3834" s="T138">n-n:case</ta>
            <ta e="T140" id="Seg_3835" s="T139">v-v:(tense)-v:mood.pn</ta>
            <ta e="T141" id="Seg_3836" s="T140">adv</ta>
            <ta e="T142" id="Seg_3837" s="T141">pers-pro:case</ta>
            <ta e="T143" id="Seg_3838" s="T142">v-v:cvb</ta>
            <ta e="T144" id="Seg_3839" s="T143">v-v:cvb</ta>
            <ta e="T145" id="Seg_3840" s="T144">v-v&gt;v-v:cvb</ta>
            <ta e="T146" id="Seg_3841" s="T145">v-v:tense-v:poss.pn</ta>
            <ta e="T147" id="Seg_3842" s="T146">n-n:poss-n:case</ta>
            <ta e="T148" id="Seg_3843" s="T147">v-v:tense-v:poss.pn</ta>
            <ta e="T149" id="Seg_3844" s="T148">pers-pro:case</ta>
            <ta e="T150" id="Seg_3845" s="T149">ptcl</ta>
            <ta e="T151" id="Seg_3846" s="T150">v-v:(ins)-v&gt;v-v:(tense)-v:mood.pn</ta>
            <ta e="T152" id="Seg_3847" s="T151">n-n:case</ta>
            <ta e="T153" id="Seg_3848" s="T152">v-v:tense-v:pred.pn</ta>
            <ta e="T154" id="Seg_3849" s="T153">v-v:tense-v:pred.pn</ta>
            <ta e="T155" id="Seg_3850" s="T154">n-n:case</ta>
            <ta e="T156" id="Seg_3851" s="T155">v-v:ptcp</ta>
            <ta e="T157" id="Seg_3852" s="T156">v-v:cvb-v-v:cvb</ta>
            <ta e="T158" id="Seg_3853" s="T157">n-n:case</ta>
            <ta e="T159" id="Seg_3854" s="T158">n-n:(poss)-n:case</ta>
            <ta e="T160" id="Seg_3855" s="T159">v-v:tense-v:pred.pn</ta>
            <ta e="T161" id="Seg_3856" s="T160">pers-pro:case</ta>
            <ta e="T162" id="Seg_3857" s="T161">post</ta>
            <ta e="T163" id="Seg_3858" s="T162">quant</ta>
            <ta e="T164" id="Seg_3859" s="T163">n-n:case</ta>
            <ta e="T165" id="Seg_3860" s="T164">v-v:tense-v:pred.pn</ta>
            <ta e="T166" id="Seg_3861" s="T165">n-n:case</ta>
            <ta e="T167" id="Seg_3862" s="T166">n-n:(poss)-n:case</ta>
            <ta e="T168" id="Seg_3863" s="T167">n-n:case</ta>
            <ta e="T169" id="Seg_3864" s="T168">ptcl</ta>
            <ta e="T170" id="Seg_3865" s="T169">v-v:tense-v:pred.pn</ta>
            <ta e="T171" id="Seg_3866" s="T170">n-n:case</ta>
            <ta e="T172" id="Seg_3867" s="T171">n-n:case</ta>
            <ta e="T173" id="Seg_3868" s="T172">n-n:case</ta>
            <ta e="T174" id="Seg_3869" s="T173">v-v:tense-v:pred.pn</ta>
            <ta e="T175" id="Seg_3870" s="T174">n-n:case</ta>
            <ta e="T176" id="Seg_3871" s="T175">v-v:tense-v:pred.pn</ta>
            <ta e="T177" id="Seg_3872" s="T176">n-n:case</ta>
            <ta e="T178" id="Seg_3873" s="T177">n-n:(poss)-n:case</ta>
            <ta e="T179" id="Seg_3874" s="T178">v-v:tense-v:pred.pn</ta>
            <ta e="T180" id="Seg_3875" s="T179">adv</ta>
            <ta e="T181" id="Seg_3876" s="T180">v-v:tense-v:pred.pn</ta>
            <ta e="T182" id="Seg_3877" s="T181">n-n:case</ta>
            <ta e="T183" id="Seg_3878" s="T182">v-v:tense-v:pred.pn</ta>
            <ta e="T184" id="Seg_3879" s="T183">v-v:cvb</ta>
            <ta e="T332" id="Seg_3880" s="T184">interj</ta>
            <ta e="T185" id="Seg_3881" s="T332">interj</ta>
            <ta e="T187" id="Seg_3882" s="T186">v-v:tense-v:pred.pn</ta>
            <ta e="T188" id="Seg_3883" s="T187">n-n:case</ta>
            <ta e="T189" id="Seg_3884" s="T188">v-v:cvb</ta>
            <ta e="T190" id="Seg_3885" s="T189">v-v:cvb</ta>
            <ta e="T191" id="Seg_3886" s="T190">n-n:case</ta>
            <ta e="T192" id="Seg_3887" s="T191">n-n:case</ta>
            <ta e="T193" id="Seg_3888" s="T192">v-v:tense-v:pred.pn</ta>
            <ta e="T333" id="Seg_3889" s="T193">interj</ta>
            <ta e="T194" id="Seg_3890" s="T333">interj</ta>
            <ta e="T196" id="Seg_3891" s="T195">v-v:tense-v:pred.pn</ta>
            <ta e="T334" id="Seg_3892" s="T196">interj</ta>
            <ta e="T197" id="Seg_3893" s="T334">interj</ta>
            <ta e="T199" id="Seg_3894" s="T198">adv</ta>
            <ta e="T200" id="Seg_3895" s="T199">v-v:cvb-v:pred.pn</ta>
            <ta e="T201" id="Seg_3896" s="T200">n-n:(poss)-n:case</ta>
            <ta e="T202" id="Seg_3897" s="T201">n-n:case</ta>
            <ta e="T203" id="Seg_3898" s="T202">v-v:tense-v:pred.pn</ta>
            <ta e="T204" id="Seg_3899" s="T203">n-n:(poss)-n:case</ta>
            <ta e="T205" id="Seg_3900" s="T204">n-n:case</ta>
            <ta e="T206" id="Seg_3901" s="T205">v-v:tense-v:pred.pn</ta>
            <ta e="T207" id="Seg_3902" s="T206">n-n:(poss)-n:case</ta>
            <ta e="T208" id="Seg_3903" s="T207">v-v:cvb</ta>
            <ta e="T209" id="Seg_3904" s="T208">v-v:tense-v:pred.pn</ta>
            <ta e="T215" id="Seg_3905" s="T214">v-v:cvb-v-v:cvb</ta>
            <ta e="T216" id="Seg_3906" s="T215">dempro-pro:case</ta>
            <ta e="T217" id="Seg_3907" s="T216">n-n:(poss)-n:case</ta>
            <ta e="T218" id="Seg_3908" s="T217">dempro</ta>
            <ta e="T219" id="Seg_3909" s="T218">n-n:case</ta>
            <ta e="T220" id="Seg_3910" s="T219">v-v:ptcp</ta>
            <ta e="T221" id="Seg_3911" s="T220">v-v:ptcp</ta>
            <ta e="T223" id="Seg_3912" s="T221">v-v:mood-v:pred.pn</ta>
            <ta e="T224" id="Seg_3913" s="T223">v-v:cvb</ta>
            <ta e="T225" id="Seg_3914" s="T224">n-n&gt;adv</ta>
            <ta e="T226" id="Seg_3915" s="T225">n-n:case</ta>
            <ta e="T227" id="Seg_3916" s="T226">dempro-pro:case</ta>
            <ta e="T228" id="Seg_3917" s="T227">v-v:cvb</ta>
            <ta e="T229" id="Seg_3918" s="T228">v-v:cvb</ta>
            <ta e="T230" id="Seg_3919" s="T229">v-v:tense-v:pred.pn</ta>
            <ta e="T231" id="Seg_3920" s="T230">n-n:(poss)-n:case</ta>
            <ta e="T232" id="Seg_3921" s="T231">v-v:tense-v:pred.pn</ta>
            <ta e="T233" id="Seg_3922" s="T232">n-n:case</ta>
            <ta e="T234" id="Seg_3923" s="T233">n-n:case</ta>
            <ta e="T235" id="Seg_3924" s="T234">n-n:poss-n:case</ta>
            <ta e="T236" id="Seg_3925" s="T235">n-n:case</ta>
            <ta e="T237" id="Seg_3926" s="T236">v-v:cvb</ta>
            <ta e="T238" id="Seg_3927" s="T237">v-v:tense-v:pred.pn</ta>
            <ta e="T239" id="Seg_3928" s="T238">n-n-n:case</ta>
            <ta e="T240" id="Seg_3929" s="T239">v-v:cvb</ta>
            <ta e="T241" id="Seg_3930" s="T240">v-v:tense-v:pred.pn</ta>
            <ta e="T242" id="Seg_3931" s="T241">cardnum</ta>
            <ta e="T243" id="Seg_3932" s="T242">n-n:poss-n:case</ta>
            <ta e="T244" id="Seg_3933" s="T243">ptcl</ta>
            <ta e="T245" id="Seg_3934" s="T244">post</ta>
            <ta e="T246" id="Seg_3935" s="T245">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T247" id="Seg_3936" s="T246">n-n:case</ta>
            <ta e="T248" id="Seg_3937" s="T247">n-n:(poss)-n:case</ta>
            <ta e="T249" id="Seg_3938" s="T248">n-n:case</ta>
            <ta e="T250" id="Seg_3939" s="T249">n-n:case</ta>
            <ta e="T251" id="Seg_3940" s="T250">v-v:tense-v:pred.pn</ta>
            <ta e="T252" id="Seg_3941" s="T251">v-v:cvb-v:pred.pn</ta>
            <ta e="T253" id="Seg_3942" s="T252">n-n:case</ta>
            <ta e="T254" id="Seg_3943" s="T253">n-n:poss-n:case</ta>
            <ta e="T255" id="Seg_3944" s="T254">v-v:tense-v:pred.pn</ta>
            <ta e="T256" id="Seg_3945" s="T255">adj</ta>
            <ta e="T257" id="Seg_3946" s="T256">ptcl</ta>
            <ta e="T258" id="Seg_3947" s="T257">n-n:poss-n:case</ta>
            <ta e="T259" id="Seg_3948" s="T258">cardnum</ta>
            <ta e="T260" id="Seg_3949" s="T259">adj</ta>
            <ta e="T261" id="Seg_3950" s="T260">n-n:poss-n:case</ta>
            <ta e="T262" id="Seg_3951" s="T261">v-v:tense-v:pred.pn</ta>
            <ta e="T263" id="Seg_3952" s="T262">n-n:case</ta>
            <ta e="T264" id="Seg_3953" s="T263">n-n:(poss)-n:case</ta>
            <ta e="T265" id="Seg_3954" s="T264">n-n:case</ta>
            <ta e="T266" id="Seg_3955" s="T265">n-n:case</ta>
            <ta e="T267" id="Seg_3956" s="T266">n-n:poss-n:case</ta>
            <ta e="T268" id="Seg_3957" s="T267">v-v:tense-v:pred.pn</ta>
            <ta e="T269" id="Seg_3958" s="T268">dempro-pro:case</ta>
            <ta e="T270" id="Seg_3959" s="T269">n-n:case</ta>
            <ta e="T271" id="Seg_3960" s="T270">n-n:(poss)-n:case</ta>
            <ta e="T272" id="Seg_3961" s="T271">ptcl</ta>
            <ta e="T273" id="Seg_3962" s="T272">n-n:poss-n:case</ta>
            <ta e="T274" id="Seg_3963" s="T273">v-v:tense-v:pred.pn</ta>
            <ta e="T275" id="Seg_3964" s="T274">n-n:case</ta>
            <ta e="T276" id="Seg_3965" s="T275">ptcl</ta>
            <ta e="T277" id="Seg_3966" s="T276">n-n:(num)-n:case</ta>
            <ta e="T278" id="Seg_3967" s="T277">adj-n:poss-n:case</ta>
            <ta e="T279" id="Seg_3968" s="T278">adv</ta>
            <ta e="T280" id="Seg_3969" s="T279">v-v&gt;v-v:cvb</ta>
            <ta e="T281" id="Seg_3970" s="T280">v-v:tense-v:pred.pn</ta>
            <ta e="T282" id="Seg_3971" s="T281">dempro-pro:case</ta>
            <ta e="T283" id="Seg_3972" s="T282">v-v:cvb-v:pred.pn</ta>
            <ta e="T284" id="Seg_3973" s="T283">n-n:case</ta>
            <ta e="T285" id="Seg_3974" s="T284">n-n:case</ta>
            <ta e="T286" id="Seg_3975" s="T285">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T287" id="Seg_3976" s="T286">v-v:tense-v:pred.pn</ta>
            <ta e="T288" id="Seg_3977" s="T287">adv</ta>
            <ta e="T289" id="Seg_3978" s="T288">n-n:case</ta>
            <ta e="T290" id="Seg_3979" s="T289">v-v:ptcp</ta>
            <ta e="T291" id="Seg_3980" s="T290">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T292" id="Seg_3981" s="T291">dempro-pro:case</ta>
            <ta e="T293" id="Seg_3982" s="T292">ptcl</ta>
            <ta e="T294" id="Seg_3983" s="T293">n-n:case</ta>
            <ta e="T295" id="Seg_3984" s="T294">v-v:ptcp</ta>
            <ta e="T296" id="Seg_3985" s="T295">v-v:tense-v:pred.pn</ta>
            <ta e="T297" id="Seg_3986" s="T296">n-n:(num)-n:case</ta>
            <ta e="T298" id="Seg_3987" s="T297">v-v:ptcp</ta>
            <ta e="T299" id="Seg_3988" s="T298">n-n:case</ta>
            <ta e="T300" id="Seg_3989" s="T299">n-n:poss-n:case</ta>
            <ta e="T301" id="Seg_3990" s="T300">adj</ta>
            <ta e="T302" id="Seg_3991" s="T301">n-n:case</ta>
            <ta e="T303" id="Seg_3992" s="T302">v-v:tense-v:pred.pn</ta>
            <ta e="T304" id="Seg_3993" s="T303">n-n:(poss)</ta>
            <ta e="T305" id="Seg_3994" s="T304">ptcl</ta>
            <ta e="T306" id="Seg_3995" s="T305">ptcl</ta>
            <ta e="T307" id="Seg_3996" s="T306">v-v:tense-v:pred.pn</ta>
            <ta e="T308" id="Seg_3997" s="T307">n-n:case</ta>
            <ta e="T309" id="Seg_3998" s="T308">v-v:ptcp</ta>
            <ta e="T310" id="Seg_3999" s="T309">v-v:tense-v:pred.pn</ta>
            <ta e="T311" id="Seg_4000" s="T310">dempro</ta>
            <ta e="T312" id="Seg_4001" s="T311">n-n:case</ta>
            <ta e="T313" id="Seg_4002" s="T312">v-v:(ins)-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T314" id="Seg_4003" s="T313">n-n:case</ta>
            <ta e="T315" id="Seg_4004" s="T314">n-n:poss-n:case</ta>
            <ta e="T316" id="Seg_4005" s="T315">n-n:(poss)-n:case</ta>
            <ta e="T317" id="Seg_4006" s="T316">n-n:case</ta>
            <ta e="T318" id="Seg_4007" s="T317">n-n:case</ta>
            <ta e="T319" id="Seg_4008" s="T318">n-n&gt;adj-n:case</ta>
            <ta e="T320" id="Seg_4009" s="T319">v-v:tense-v:pred.pn</ta>
            <ta e="T321" id="Seg_4010" s="T320">n-n:case</ta>
            <ta e="T322" id="Seg_4011" s="T321">adv</ta>
            <ta e="T323" id="Seg_4012" s="T322">v-v:(ins)-v:ptcp</ta>
            <ta e="T324" id="Seg_4013" s="T323">adj</ta>
            <ta e="T325" id="Seg_4014" s="T324">n-n:(poss)-n:case</ta>
            <ta e="T326" id="Seg_4015" s="T325">n-n:poss-n:case</ta>
            <ta e="T327" id="Seg_4016" s="T326">n-n:case</ta>
            <ta e="T328" id="Seg_4017" s="T327">v-v:tense-v:pred.pn</ta>
            <ta e="T329" id="Seg_4018" s="T328">adj-n:(poss)-n:case</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_4019" s="T0">adv</ta>
            <ta e="T2" id="Seg_4020" s="T1">n</ta>
            <ta e="T3" id="Seg_4021" s="T2">v</ta>
            <ta e="T4" id="Seg_4022" s="T3">n</ta>
            <ta e="T5" id="Seg_4023" s="T4">cardnum</ta>
            <ta e="T6" id="Seg_4024" s="T5">adj</ta>
            <ta e="T7" id="Seg_4025" s="T6">dempro</ta>
            <ta e="T8" id="Seg_4026" s="T7">n</ta>
            <ta e="T9" id="Seg_4027" s="T8">cop</ta>
            <ta e="T10" id="Seg_4028" s="T9">pers</ta>
            <ta e="T11" id="Seg_4029" s="T10">n</ta>
            <ta e="T12" id="Seg_4030" s="T11">v</ta>
            <ta e="T13" id="Seg_4031" s="T12">aux</ta>
            <ta e="T14" id="Seg_4032" s="T13">que</ta>
            <ta e="T15" id="Seg_4033" s="T14">n</ta>
            <ta e="T16" id="Seg_4034" s="T15">v</ta>
            <ta e="T17" id="Seg_4035" s="T16">v</ta>
            <ta e="T18" id="Seg_4036" s="T17">adj</ta>
            <ta e="T19" id="Seg_4037" s="T18">adj</ta>
            <ta e="T20" id="Seg_4038" s="T19">n</ta>
            <ta e="T21" id="Seg_4039" s="T20">dempro</ta>
            <ta e="T22" id="Seg_4040" s="T21">n</ta>
            <ta e="T23" id="Seg_4041" s="T22">v</ta>
            <ta e="T24" id="Seg_4042" s="T23">v</ta>
            <ta e="T25" id="Seg_4043" s="T24">adv</ta>
            <ta e="T26" id="Seg_4044" s="T25">v</ta>
            <ta e="T27" id="Seg_4045" s="T26">v</ta>
            <ta e="T28" id="Seg_4046" s="T27">n</ta>
            <ta e="T29" id="Seg_4047" s="T28">v</ta>
            <ta e="T30" id="Seg_4048" s="T29">indfpro</ta>
            <ta e="T31" id="Seg_4049" s="T30">n</ta>
            <ta e="T32" id="Seg_4050" s="T31">cardnum</ta>
            <ta e="T33" id="Seg_4051" s="T32">n</ta>
            <ta e="T34" id="Seg_4052" s="T33">n</ta>
            <ta e="T35" id="Seg_4053" s="T34">cop</ta>
            <ta e="T36" id="Seg_4054" s="T35">que</ta>
            <ta e="T37" id="Seg_4055" s="T36">ptcl</ta>
            <ta e="T38" id="Seg_4056" s="T37">n</ta>
            <ta e="T39" id="Seg_4057" s="T38">n</ta>
            <ta e="T40" id="Seg_4058" s="T39">v</ta>
            <ta e="T41" id="Seg_4059" s="T40">dempro</ta>
            <ta e="T42" id="Seg_4060" s="T41">n</ta>
            <ta e="T43" id="Seg_4061" s="T42">v</ta>
            <ta e="T44" id="Seg_4062" s="T43">n</ta>
            <ta e="T45" id="Seg_4063" s="T44">v</ta>
            <ta e="T46" id="Seg_4064" s="T45">n</ta>
            <ta e="T47" id="Seg_4065" s="T46">n</ta>
            <ta e="T48" id="Seg_4066" s="T47">adj</ta>
            <ta e="T49" id="Seg_4067" s="T48">cardnum</ta>
            <ta e="T50" id="Seg_4068" s="T49">cardnum</ta>
            <ta e="T51" id="Seg_4069" s="T50">ptcl</ta>
            <ta e="T52" id="Seg_4070" s="T51">n</ta>
            <ta e="T53" id="Seg_4071" s="T52">v</ta>
            <ta e="T54" id="Seg_4072" s="T53">n</ta>
            <ta e="T55" id="Seg_4073" s="T54">v</ta>
            <ta e="T56" id="Seg_4074" s="T55">n</ta>
            <ta e="T57" id="Seg_4075" s="T56">adj</ta>
            <ta e="T58" id="Seg_4076" s="T57">cop</ta>
            <ta e="T59" id="Seg_4077" s="T58">v</ta>
            <ta e="T60" id="Seg_4078" s="T59">n</ta>
            <ta e="T61" id="Seg_4079" s="T60">v</ta>
            <ta e="T62" id="Seg_4080" s="T61">adv</ta>
            <ta e="T63" id="Seg_4081" s="T62">dempro</ta>
            <ta e="T64" id="Seg_4082" s="T63">n</ta>
            <ta e="T65" id="Seg_4083" s="T64">n</ta>
            <ta e="T66" id="Seg_4084" s="T65">n</ta>
            <ta e="T67" id="Seg_4085" s="T66">ptcl</ta>
            <ta e="T68" id="Seg_4086" s="T67">adv</ta>
            <ta e="T69" id="Seg_4087" s="T68">adj</ta>
            <ta e="T70" id="Seg_4088" s="T69">pers</ta>
            <ta e="T71" id="Seg_4089" s="T70">v</ta>
            <ta e="T72" id="Seg_4090" s="T71">adv</ta>
            <ta e="T73" id="Seg_4091" s="T72">v</ta>
            <ta e="T74" id="Seg_4092" s="T73">pers</ta>
            <ta e="T75" id="Seg_4093" s="T74">pers</ta>
            <ta e="T76" id="Seg_4094" s="T75">v</ta>
            <ta e="T77" id="Seg_4095" s="T76">aux</ta>
            <ta e="T78" id="Seg_4096" s="T77">n</ta>
            <ta e="T79" id="Seg_4097" s="T78">v</ta>
            <ta e="T80" id="Seg_4098" s="T79">pers</ta>
            <ta e="T81" id="Seg_4099" s="T80">post</ta>
            <ta e="T82" id="Seg_4100" s="T81">quant</ta>
            <ta e="T83" id="Seg_4101" s="T82">n</ta>
            <ta e="T84" id="Seg_4102" s="T83">v</ta>
            <ta e="T85" id="Seg_4103" s="T84">ptcl</ta>
            <ta e="T86" id="Seg_4104" s="T85">dempro</ta>
            <ta e="T87" id="Seg_4105" s="T86">pers</ta>
            <ta e="T88" id="Seg_4106" s="T87">v</ta>
            <ta e="T89" id="Seg_4107" s="T88">conj</ta>
            <ta e="T90" id="Seg_4108" s="T89">v</ta>
            <ta e="T91" id="Seg_4109" s="T90">adv</ta>
            <ta e="T92" id="Seg_4110" s="T91">n</ta>
            <ta e="T93" id="Seg_4111" s="T92">post</ta>
            <ta e="T94" id="Seg_4112" s="T93">v</ta>
            <ta e="T95" id="Seg_4113" s="T94">n</ta>
            <ta e="T96" id="Seg_4114" s="T95">v</ta>
            <ta e="T97" id="Seg_4115" s="T96">v</ta>
            <ta e="T98" id="Seg_4116" s="T97">n</ta>
            <ta e="T99" id="Seg_4117" s="T98">v</ta>
            <ta e="T100" id="Seg_4118" s="T99">n</ta>
            <ta e="T101" id="Seg_4119" s="T100">n</ta>
            <ta e="T102" id="Seg_4120" s="T101">v</ta>
            <ta e="T103" id="Seg_4121" s="T102">v</ta>
            <ta e="T104" id="Seg_4122" s="T103">n</ta>
            <ta e="T105" id="Seg_4123" s="T104">v</ta>
            <ta e="T106" id="Seg_4124" s="T105">aux</ta>
            <ta e="T107" id="Seg_4125" s="T106">n</ta>
            <ta e="T108" id="Seg_4126" s="T107">n</ta>
            <ta e="T109" id="Seg_4127" s="T108">n</ta>
            <ta e="T110" id="Seg_4128" s="T109">v</ta>
            <ta e="T111" id="Seg_4129" s="T110">n</ta>
            <ta e="T112" id="Seg_4130" s="T111">v</ta>
            <ta e="T113" id="Seg_4131" s="T112">n</ta>
            <ta e="T114" id="Seg_4132" s="T113">n</ta>
            <ta e="T115" id="Seg_4133" s="T114">v</ta>
            <ta e="T116" id="Seg_4134" s="T115">v</ta>
            <ta e="T117" id="Seg_4135" s="T116">n</ta>
            <ta e="T118" id="Seg_4136" s="T117">n</ta>
            <ta e="T119" id="Seg_4137" s="T118">v</ta>
            <ta e="T120" id="Seg_4138" s="T119">n</ta>
            <ta e="T121" id="Seg_4139" s="T120">v</ta>
            <ta e="T122" id="Seg_4140" s="T121">v</ta>
            <ta e="T123" id="Seg_4141" s="T122">v</ta>
            <ta e="T124" id="Seg_4142" s="T123">cop</ta>
            <ta e="T330" id="Seg_4143" s="T124">interj</ta>
            <ta e="T331" id="Seg_4144" s="T330">interj</ta>
            <ta e="T125" id="Seg_4145" s="T331">interj</ta>
            <ta e="T127" id="Seg_4146" s="T126">v</ta>
            <ta e="T128" id="Seg_4147" s="T127">adv</ta>
            <ta e="T129" id="Seg_4148" s="T128">n</ta>
            <ta e="T130" id="Seg_4149" s="T129">v</ta>
            <ta e="T132" id="Seg_4150" s="T130">pers</ta>
            <ta e="T335" id="Seg_4151" s="T132">interj</ta>
            <ta e="T134" id="Seg_4152" s="T133">v</ta>
            <ta e="T135" id="Seg_4153" s="T134">adv</ta>
            <ta e="T136" id="Seg_4154" s="T135">pers</ta>
            <ta e="T137" id="Seg_4155" s="T136">n</ta>
            <ta e="T138" id="Seg_4156" s="T137">v</ta>
            <ta e="T139" id="Seg_4157" s="T138">n</ta>
            <ta e="T140" id="Seg_4158" s="T139">v</ta>
            <ta e="T141" id="Seg_4159" s="T140">adv</ta>
            <ta e="T142" id="Seg_4160" s="T141">pers</ta>
            <ta e="T143" id="Seg_4161" s="T142">v</ta>
            <ta e="T144" id="Seg_4162" s="T143">aux</ta>
            <ta e="T145" id="Seg_4163" s="T144">v</ta>
            <ta e="T146" id="Seg_4164" s="T145">aux</ta>
            <ta e="T147" id="Seg_4165" s="T146">n</ta>
            <ta e="T148" id="Seg_4166" s="T147">v</ta>
            <ta e="T149" id="Seg_4167" s="T148">pers</ta>
            <ta e="T150" id="Seg_4168" s="T149">ptcl</ta>
            <ta e="T151" id="Seg_4169" s="T150">v</ta>
            <ta e="T152" id="Seg_4170" s="T151">n</ta>
            <ta e="T153" id="Seg_4171" s="T152">v</ta>
            <ta e="T154" id="Seg_4172" s="T153">v</ta>
            <ta e="T155" id="Seg_4173" s="T154">n</ta>
            <ta e="T156" id="Seg_4174" s="T155">v</ta>
            <ta e="T157" id="Seg_4175" s="T156">aux</ta>
            <ta e="T158" id="Seg_4176" s="T157">n</ta>
            <ta e="T159" id="Seg_4177" s="T158">n</ta>
            <ta e="T160" id="Seg_4178" s="T159">v</ta>
            <ta e="T161" id="Seg_4179" s="T160">pers</ta>
            <ta e="T162" id="Seg_4180" s="T161">post</ta>
            <ta e="T163" id="Seg_4181" s="T162">quant</ta>
            <ta e="T164" id="Seg_4182" s="T163">n</ta>
            <ta e="T165" id="Seg_4183" s="T164">v</ta>
            <ta e="T166" id="Seg_4184" s="T165">n</ta>
            <ta e="T167" id="Seg_4185" s="T166">n</ta>
            <ta e="T168" id="Seg_4186" s="T167">n</ta>
            <ta e="T169" id="Seg_4187" s="T168">ptcl</ta>
            <ta e="T170" id="Seg_4188" s="T169">cop</ta>
            <ta e="T171" id="Seg_4189" s="T170">n</ta>
            <ta e="T172" id="Seg_4190" s="T171">n</ta>
            <ta e="T173" id="Seg_4191" s="T172">n</ta>
            <ta e="T174" id="Seg_4192" s="T173">v</ta>
            <ta e="T175" id="Seg_4193" s="T174">n</ta>
            <ta e="T176" id="Seg_4194" s="T175">v</ta>
            <ta e="T177" id="Seg_4195" s="T176">n</ta>
            <ta e="T178" id="Seg_4196" s="T177">n</ta>
            <ta e="T179" id="Seg_4197" s="T178">v</ta>
            <ta e="T180" id="Seg_4198" s="T179">adv</ta>
            <ta e="T181" id="Seg_4199" s="T180">v</ta>
            <ta e="T182" id="Seg_4200" s="T181">n</ta>
            <ta e="T183" id="Seg_4201" s="T182">v</ta>
            <ta e="T184" id="Seg_4202" s="T183">v</ta>
            <ta e="T332" id="Seg_4203" s="T184">interj</ta>
            <ta e="T185" id="Seg_4204" s="T332">interj</ta>
            <ta e="T187" id="Seg_4205" s="T186">v</ta>
            <ta e="T188" id="Seg_4206" s="T187">n</ta>
            <ta e="T189" id="Seg_4207" s="T188">v</ta>
            <ta e="T190" id="Seg_4208" s="T189">v</ta>
            <ta e="T191" id="Seg_4209" s="T190">n</ta>
            <ta e="T192" id="Seg_4210" s="T191">n</ta>
            <ta e="T193" id="Seg_4211" s="T192">v</ta>
            <ta e="T333" id="Seg_4212" s="T193">interj</ta>
            <ta e="T194" id="Seg_4213" s="T333">interj</ta>
            <ta e="T196" id="Seg_4214" s="T195">v</ta>
            <ta e="T334" id="Seg_4215" s="T196">interj</ta>
            <ta e="T197" id="Seg_4216" s="T334">interj</ta>
            <ta e="T199" id="Seg_4217" s="T198">adv</ta>
            <ta e="T200" id="Seg_4218" s="T199">v</ta>
            <ta e="T201" id="Seg_4219" s="T200">n</ta>
            <ta e="T202" id="Seg_4220" s="T201">n</ta>
            <ta e="T203" id="Seg_4221" s="T202">v</ta>
            <ta e="T204" id="Seg_4222" s="T203">n</ta>
            <ta e="T205" id="Seg_4223" s="T204">n</ta>
            <ta e="T206" id="Seg_4224" s="T205">v</ta>
            <ta e="T207" id="Seg_4225" s="T206">n</ta>
            <ta e="T208" id="Seg_4226" s="T207">v</ta>
            <ta e="T209" id="Seg_4227" s="T208">aux</ta>
            <ta e="T215" id="Seg_4228" s="T214">v</ta>
            <ta e="T216" id="Seg_4229" s="T215">dempro</ta>
            <ta e="T217" id="Seg_4230" s="T216">n</ta>
            <ta e="T218" id="Seg_4231" s="T217">dempro</ta>
            <ta e="T219" id="Seg_4232" s="T218">n</ta>
            <ta e="T220" id="Seg_4233" s="T219">v</ta>
            <ta e="T221" id="Seg_4234" s="T220">aux</ta>
            <ta e="T223" id="Seg_4235" s="T221">aux</ta>
            <ta e="T224" id="Seg_4236" s="T223">v</ta>
            <ta e="T225" id="Seg_4237" s="T224">adv</ta>
            <ta e="T226" id="Seg_4238" s="T225">n</ta>
            <ta e="T227" id="Seg_4239" s="T226">dempro</ta>
            <ta e="T228" id="Seg_4240" s="T227">v</ta>
            <ta e="T229" id="Seg_4241" s="T228">v</ta>
            <ta e="T230" id="Seg_4242" s="T229">aux</ta>
            <ta e="T231" id="Seg_4243" s="T230">n</ta>
            <ta e="T232" id="Seg_4244" s="T231">v</ta>
            <ta e="T233" id="Seg_4245" s="T232">n</ta>
            <ta e="T234" id="Seg_4246" s="T233">n</ta>
            <ta e="T235" id="Seg_4247" s="T234">n</ta>
            <ta e="T236" id="Seg_4248" s="T235">n</ta>
            <ta e="T237" id="Seg_4249" s="T236">v</ta>
            <ta e="T238" id="Seg_4250" s="T237">v</ta>
            <ta e="T239" id="Seg_4251" s="T238">n</ta>
            <ta e="T240" id="Seg_4252" s="T239">v</ta>
            <ta e="T241" id="Seg_4253" s="T240">aux</ta>
            <ta e="T242" id="Seg_4254" s="T241">cardnum</ta>
            <ta e="T243" id="Seg_4255" s="T242">n</ta>
            <ta e="T244" id="Seg_4256" s="T243">ptcl</ta>
            <ta e="T245" id="Seg_4257" s="T244">post</ta>
            <ta e="T246" id="Seg_4258" s="T245">v</ta>
            <ta e="T247" id="Seg_4259" s="T246">n</ta>
            <ta e="T248" id="Seg_4260" s="T247">n</ta>
            <ta e="T249" id="Seg_4261" s="T248">n</ta>
            <ta e="T250" id="Seg_4262" s="T249">n</ta>
            <ta e="T251" id="Seg_4263" s="T250">v</ta>
            <ta e="T252" id="Seg_4264" s="T251">v</ta>
            <ta e="T253" id="Seg_4265" s="T252">n</ta>
            <ta e="T254" id="Seg_4266" s="T253">n</ta>
            <ta e="T255" id="Seg_4267" s="T254">v</ta>
            <ta e="T256" id="Seg_4268" s="T255">adj</ta>
            <ta e="T257" id="Seg_4269" s="T256">ptcl</ta>
            <ta e="T258" id="Seg_4270" s="T257">n</ta>
            <ta e="T259" id="Seg_4271" s="T258">cardnum</ta>
            <ta e="T260" id="Seg_4272" s="T259">adj</ta>
            <ta e="T261" id="Seg_4273" s="T260">n</ta>
            <ta e="T262" id="Seg_4274" s="T261">v</ta>
            <ta e="T263" id="Seg_4275" s="T262">n</ta>
            <ta e="T264" id="Seg_4276" s="T263">n</ta>
            <ta e="T265" id="Seg_4277" s="T264">n</ta>
            <ta e="T266" id="Seg_4278" s="T265">n</ta>
            <ta e="T267" id="Seg_4279" s="T266">n</ta>
            <ta e="T268" id="Seg_4280" s="T267">v</ta>
            <ta e="T269" id="Seg_4281" s="T268">dempro</ta>
            <ta e="T270" id="Seg_4282" s="T269">n</ta>
            <ta e="T271" id="Seg_4283" s="T270">n</ta>
            <ta e="T272" id="Seg_4284" s="T271">ptcl</ta>
            <ta e="T273" id="Seg_4285" s="T272">n</ta>
            <ta e="T274" id="Seg_4286" s="T273">v</ta>
            <ta e="T275" id="Seg_4287" s="T274">n</ta>
            <ta e="T276" id="Seg_4288" s="T275">ptcl</ta>
            <ta e="T277" id="Seg_4289" s="T276">n</ta>
            <ta e="T278" id="Seg_4290" s="T277">adj</ta>
            <ta e="T279" id="Seg_4291" s="T278">adv</ta>
            <ta e="T280" id="Seg_4292" s="T279">v</ta>
            <ta e="T281" id="Seg_4293" s="T280">aux</ta>
            <ta e="T282" id="Seg_4294" s="T281">dempro</ta>
            <ta e="T283" id="Seg_4295" s="T282">v</ta>
            <ta e="T284" id="Seg_4296" s="T283">n</ta>
            <ta e="T285" id="Seg_4297" s="T284">n</ta>
            <ta e="T286" id="Seg_4298" s="T285">v</ta>
            <ta e="T287" id="Seg_4299" s="T286">v</ta>
            <ta e="T288" id="Seg_4300" s="T287">adv</ta>
            <ta e="T289" id="Seg_4301" s="T288">n</ta>
            <ta e="T290" id="Seg_4302" s="T289">v</ta>
            <ta e="T291" id="Seg_4303" s="T290">ptcl</ta>
            <ta e="T292" id="Seg_4304" s="T291">dempro</ta>
            <ta e="T293" id="Seg_4305" s="T292">ptcl</ta>
            <ta e="T294" id="Seg_4306" s="T293">n</ta>
            <ta e="T295" id="Seg_4307" s="T294">v</ta>
            <ta e="T296" id="Seg_4308" s="T295">aux</ta>
            <ta e="T297" id="Seg_4309" s="T296">n</ta>
            <ta e="T298" id="Seg_4310" s="T297">v</ta>
            <ta e="T299" id="Seg_4311" s="T298">n</ta>
            <ta e="T300" id="Seg_4312" s="T299">n</ta>
            <ta e="T301" id="Seg_4313" s="T300">adj</ta>
            <ta e="T302" id="Seg_4314" s="T301">n</ta>
            <ta e="T303" id="Seg_4315" s="T302">v</ta>
            <ta e="T304" id="Seg_4316" s="T303">n</ta>
            <ta e="T305" id="Seg_4317" s="T304">ptcl</ta>
            <ta e="T306" id="Seg_4318" s="T305">ptcl</ta>
            <ta e="T307" id="Seg_4319" s="T306">cop</ta>
            <ta e="T308" id="Seg_4320" s="T307">n</ta>
            <ta e="T309" id="Seg_4321" s="T308">v</ta>
            <ta e="T310" id="Seg_4322" s="T309">aux</ta>
            <ta e="T311" id="Seg_4323" s="T310">dempro</ta>
            <ta e="T312" id="Seg_4324" s="T311">n</ta>
            <ta e="T313" id="Seg_4325" s="T312">v</ta>
            <ta e="T314" id="Seg_4326" s="T313">n</ta>
            <ta e="T315" id="Seg_4327" s="T314">n</ta>
            <ta e="T316" id="Seg_4328" s="T315">n</ta>
            <ta e="T317" id="Seg_4329" s="T316">n</ta>
            <ta e="T318" id="Seg_4330" s="T317">n</ta>
            <ta e="T319" id="Seg_4331" s="T318">adj</ta>
            <ta e="T320" id="Seg_4332" s="T319">cop</ta>
            <ta e="T321" id="Seg_4333" s="T320">n</ta>
            <ta e="T322" id="Seg_4334" s="T321">adv</ta>
            <ta e="T323" id="Seg_4335" s="T322">v</ta>
            <ta e="T324" id="Seg_4336" s="T323">adj</ta>
            <ta e="T325" id="Seg_4337" s="T324">n</ta>
            <ta e="T326" id="Seg_4338" s="T325">n</ta>
            <ta e="T327" id="Seg_4339" s="T326">n</ta>
            <ta e="T328" id="Seg_4340" s="T327">cop</ta>
            <ta e="T329" id="Seg_4341" s="T328">adj</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_4342" s="T0">adv:Time</ta>
            <ta e="T2" id="Seg_4343" s="T1">np.h:Th</ta>
            <ta e="T4" id="Seg_4344" s="T3">np.h:Th</ta>
            <ta e="T6" id="Seg_4345" s="T5">0.3.h:Th</ta>
            <ta e="T7" id="Seg_4346" s="T6">0.3.h:Poss pro.h:Th</ta>
            <ta e="T10" id="Seg_4347" s="T9">pro.h:Poss</ta>
            <ta e="T11" id="Seg_4348" s="T10">np:P</ta>
            <ta e="T13" id="Seg_4349" s="T11">0.3.h:A</ta>
            <ta e="T15" id="Seg_4350" s="T14">np:L</ta>
            <ta e="T16" id="Seg_4351" s="T15">0.3.h:A</ta>
            <ta e="T17" id="Seg_4352" s="T16">0.3.h:A</ta>
            <ta e="T20" id="Seg_4353" s="T19">np:L</ta>
            <ta e="T22" id="Seg_4354" s="T21">np:L</ta>
            <ta e="T23" id="Seg_4355" s="T22">0.1.h:Th</ta>
            <ta e="T24" id="Seg_4356" s="T23">0.3.h:A</ta>
            <ta e="T25" id="Seg_4357" s="T24">adv:G</ta>
            <ta e="T27" id="Seg_4358" s="T26">0.3.h:A</ta>
            <ta e="T28" id="Seg_4359" s="T27">np:P</ta>
            <ta e="T29" id="Seg_4360" s="T28">0.3.h:A</ta>
            <ta e="T30" id="Seg_4361" s="T29">pro.h:A</ta>
            <ta e="T31" id="Seg_4362" s="T30">np:P</ta>
            <ta e="T33" id="Seg_4363" s="T32">np.h:Th</ta>
            <ta e="T37" id="Seg_4364" s="T35">pro:Time</ta>
            <ta e="T39" id="Seg_4365" s="T38">0.3.h:Poss np:Th</ta>
            <ta e="T42" id="Seg_4366" s="T41">np.h:Th</ta>
            <ta e="T44" id="Seg_4367" s="T43">0.3.h:Poss np:P</ta>
            <ta e="T45" id="Seg_4368" s="T44">0.3.h:A</ta>
            <ta e="T46" id="Seg_4369" s="T45">0.3.h:Poss np:Th</ta>
            <ta e="T52" id="Seg_4370" s="T51">0.3.h:A</ta>
            <ta e="T54" id="Seg_4371" s="T53">0.1.h:Poss np:P</ta>
            <ta e="T55" id="Seg_4372" s="T54">0.1.h:A</ta>
            <ta e="T58" id="Seg_4373" s="T57">0.1.h:Th</ta>
            <ta e="T59" id="Seg_4374" s="T58">0.1.h:A</ta>
            <ta e="T60" id="Seg_4375" s="T59">np:P</ta>
            <ta e="T61" id="Seg_4376" s="T60">0.1.h:A</ta>
            <ta e="T62" id="Seg_4377" s="T61">adv:L</ta>
            <ta e="T64" id="Seg_4378" s="T63">np:L</ta>
            <ta e="T65" id="Seg_4379" s="T64">0.1.h:Poss np:Th</ta>
            <ta e="T66" id="Seg_4380" s="T65">np.h:Th</ta>
            <ta e="T68" id="Seg_4381" s="T67">adv:L</ta>
            <ta e="T69" id="Seg_4382" s="T68">0.3.h:Th</ta>
            <ta e="T70" id="Seg_4383" s="T69">pro.h:A</ta>
            <ta e="T72" id="Seg_4384" s="T71">adv:G</ta>
            <ta e="T74" id="Seg_4385" s="T73">pro.h:Th</ta>
            <ta e="T75" id="Seg_4386" s="T74">pro.h:Th</ta>
            <ta e="T78" id="Seg_4387" s="T77">0.3.h:Poss np.h:Th</ta>
            <ta e="T79" id="Seg_4388" s="T78">0.2.h:A</ta>
            <ta e="T81" id="Seg_4389" s="T79">pp:Com</ta>
            <ta e="T83" id="Seg_4390" s="T82">np.h:A</ta>
            <ta e="T86" id="Seg_4391" s="T85">pro.h:P</ta>
            <ta e="T87" id="Seg_4392" s="T86">pro.h:A</ta>
            <ta e="T91" id="Seg_4393" s="T90">adv:Time</ta>
            <ta e="T93" id="Seg_4394" s="T91">pp:L</ta>
            <ta e="T94" id="Seg_4395" s="T93">0.1.h:A</ta>
            <ta e="T95" id="Seg_4396" s="T94">np:Th</ta>
            <ta e="T97" id="Seg_4397" s="T96">0.1.h:Th</ta>
            <ta e="T98" id="Seg_4398" s="T97">np:Ins</ta>
            <ta e="T99" id="Seg_4399" s="T98">0.1.h:A</ta>
            <ta e="T100" id="Seg_4400" s="T99">np.h:A</ta>
            <ta e="T101" id="Seg_4401" s="T100">np.h:P</ta>
            <ta e="T104" id="Seg_4402" s="T103">np.h:Th</ta>
            <ta e="T107" id="Seg_4403" s="T106">np:Com</ta>
            <ta e="T108" id="Seg_4404" s="T107">np:Poss</ta>
            <ta e="T109" id="Seg_4405" s="T108">np:G</ta>
            <ta e="T110" id="Seg_4406" s="T109">0.3.h:A</ta>
            <ta e="T111" id="Seg_4407" s="T110">np.h:R</ta>
            <ta e="T112" id="Seg_4408" s="T111">0.3.h:P</ta>
            <ta e="T113" id="Seg_4409" s="T112">np.h:Poss</ta>
            <ta e="T114" id="Seg_4410" s="T113">np.h:A</ta>
            <ta e="T117" id="Seg_4411" s="T116">np:P</ta>
            <ta e="T118" id="Seg_4412" s="T117">np:P</ta>
            <ta e="T119" id="Seg_4413" s="T118">0.2.h:A</ta>
            <ta e="T120" id="Seg_4414" s="T119">np:P</ta>
            <ta e="T121" id="Seg_4415" s="T120">0.2.h:A</ta>
            <ta e="T124" id="Seg_4416" s="T123">0.3.h:E</ta>
            <ta e="T127" id="Seg_4417" s="T126">0.1.h:A</ta>
            <ta e="T128" id="Seg_4418" s="T127">adv:Time</ta>
            <ta e="T129" id="Seg_4419" s="T128">np:Th</ta>
            <ta e="T130" id="Seg_4420" s="T129">0.2.h:A</ta>
            <ta e="T132" id="Seg_4421" s="T130">pro.h:R</ta>
            <ta e="T134" id="Seg_4422" s="T133">0.1.h:A</ta>
            <ta e="T135" id="Seg_4423" s="T134">adv:Time</ta>
            <ta e="T136" id="Seg_4424" s="T135">pro.h:A</ta>
            <ta e="T137" id="Seg_4425" s="T136">np:P</ta>
            <ta e="T139" id="Seg_4426" s="T138">np:L</ta>
            <ta e="T140" id="Seg_4427" s="T139">0.2.h:A</ta>
            <ta e="T141" id="Seg_4428" s="T140">adv:Time</ta>
            <ta e="T142" id="Seg_4429" s="T141">pro.h:A</ta>
            <ta e="T147" id="Seg_4430" s="T146">0.3.h:Poss 0.3.h:P</ta>
            <ta e="T149" id="Seg_4431" s="T148">pro.h:A</ta>
            <ta e="T152" id="Seg_4432" s="T151">np.h:A</ta>
            <ta e="T158" id="Seg_4433" s="T157">np.h:Poss</ta>
            <ta e="T159" id="Seg_4434" s="T158">np.h:A</ta>
            <ta e="T162" id="Seg_4435" s="T160">pp:Com</ta>
            <ta e="T164" id="Seg_4436" s="T163">np.h:A</ta>
            <ta e="T166" id="Seg_4437" s="T165">np:Poss</ta>
            <ta e="T167" id="Seg_4438" s="T166">np:Th</ta>
            <ta e="T171" id="Seg_4439" s="T170">np.h:A</ta>
            <ta e="T172" id="Seg_4440" s="T171">np:P</ta>
            <ta e="T173" id="Seg_4441" s="T172">np:P</ta>
            <ta e="T175" id="Seg_4442" s="T174">np:P</ta>
            <ta e="T176" id="Seg_4443" s="T175">0.3.h:A</ta>
            <ta e="T177" id="Seg_4444" s="T176">np.h:Poss</ta>
            <ta e="T178" id="Seg_4445" s="T177">np.h:A</ta>
            <ta e="T181" id="Seg_4446" s="T180">0.3.h:A</ta>
            <ta e="T182" id="Seg_4447" s="T181">np.h:Th</ta>
            <ta e="T184" id="Seg_4448" s="T183">0.3.h:E</ta>
            <ta e="T187" id="Seg_4449" s="T186">0.3.h:A</ta>
            <ta e="T188" id="Seg_4450" s="T187">np.h:A</ta>
            <ta e="T191" id="Seg_4451" s="T190">np.h:R</ta>
            <ta e="T192" id="Seg_4452" s="T191">np:Th</ta>
            <ta e="T196" id="Seg_4453" s="T195">0.3.h:A</ta>
            <ta e="T201" id="Seg_4454" s="T200">np.h:A</ta>
            <ta e="T202" id="Seg_4455" s="T201">np:P</ta>
            <ta e="T204" id="Seg_4456" s="T203">np.h:A</ta>
            <ta e="T205" id="Seg_4457" s="T204">np:P</ta>
            <ta e="T207" id="Seg_4458" s="T206">np.h:A</ta>
            <ta e="T215" id="Seg_4459" s="T214">0.3.h:A</ta>
            <ta e="T217" id="Seg_4460" s="T216">np:Th</ta>
            <ta e="T219" id="Seg_4461" s="T218">np:So</ta>
            <ta e="T223" id="Seg_4462" s="T221">0.1.h:Th</ta>
            <ta e="T226" id="Seg_4463" s="T225">np.h:A</ta>
            <ta e="T227" id="Seg_4464" s="T226">pro:Th</ta>
            <ta e="T231" id="Seg_4465" s="T230">np.h:A</ta>
            <ta e="T233" id="Seg_4466" s="T232">np:G</ta>
            <ta e="T234" id="Seg_4467" s="T233">np.h:A</ta>
            <ta e="T235" id="Seg_4468" s="T234">np:Ins</ta>
            <ta e="T236" id="Seg_4469" s="T235">np:G</ta>
            <ta e="T239" id="Seg_4470" s="T238">np.h:A</ta>
            <ta e="T243" id="Seg_4471" s="T242">np:Th</ta>
            <ta e="T246" id="Seg_4472" s="T245">0.3.h:E</ta>
            <ta e="T247" id="Seg_4473" s="T246">np.h:Poss</ta>
            <ta e="T248" id="Seg_4474" s="T247">np.h:A</ta>
            <ta e="T250" id="Seg_4475" s="T249">np:G</ta>
            <ta e="T253" id="Seg_4476" s="T252">np.h:A</ta>
            <ta e="T254" id="Seg_4477" s="T253">np:Ins</ta>
            <ta e="T258" id="Seg_4478" s="T257">np:G</ta>
            <ta e="T261" id="Seg_4479" s="T260">0.3.h:Poss np:P</ta>
            <ta e="T262" id="Seg_4480" s="T261">0.3.h:A</ta>
            <ta e="T263" id="Seg_4481" s="T262">np.h:Poss</ta>
            <ta e="T264" id="Seg_4482" s="T263">np.h:A</ta>
            <ta e="T266" id="Seg_4483" s="T265">np:So</ta>
            <ta e="T267" id="Seg_4484" s="T266">np:Path</ta>
            <ta e="T269" id="Seg_4485" s="T268">pro:Time</ta>
            <ta e="T270" id="Seg_4486" s="T269">np.h:Poss</ta>
            <ta e="T271" id="Seg_4487" s="T270">np.h:A</ta>
            <ta e="T273" id="Seg_4488" s="T272">np:Path</ta>
            <ta e="T275" id="Seg_4489" s="T274">np:L</ta>
            <ta e="T277" id="Seg_4490" s="T276">np.h:P</ta>
            <ta e="T281" id="Seg_4491" s="T279">0.3.h:A</ta>
            <ta e="T282" id="Seg_4492" s="T281">pro.h:P</ta>
            <ta e="T283" id="Seg_4493" s="T282">0.3.h:A</ta>
            <ta e="T285" id="Seg_4494" s="T284">np:Th</ta>
            <ta e="T287" id="Seg_4495" s="T286">0.3.h:Th</ta>
            <ta e="T288" id="Seg_4496" s="T287">adv:Time</ta>
            <ta e="T289" id="Seg_4497" s="T288">np:P</ta>
            <ta e="T292" id="Seg_4498" s="T291">pro:Time</ta>
            <ta e="T294" id="Seg_4499" s="T293">np:P</ta>
            <ta e="T297" id="Seg_4500" s="T296">np.h:Th</ta>
            <ta e="T299" id="Seg_4501" s="T298">np.h:Poss</ta>
            <ta e="T300" id="Seg_4502" s="T299">np:So</ta>
            <ta e="T302" id="Seg_4503" s="T301">np:Th</ta>
            <ta e="T304" id="Seg_4504" s="T303">0.3.h:Poss np:Th</ta>
            <ta e="T308" id="Seg_4505" s="T307">np:So</ta>
            <ta e="T310" id="Seg_4506" s="T309">0.3.h:Th</ta>
            <ta e="T312" id="Seg_4507" s="T311">np.h:A</ta>
            <ta e="T314" id="Seg_4508" s="T313">np.h:Poss</ta>
            <ta e="T315" id="Seg_4509" s="T314">np:Poss</ta>
            <ta e="T316" id="Seg_4510" s="T315">np:Th</ta>
            <ta e="T317" id="Seg_4511" s="T316">n:Time</ta>
            <ta e="T318" id="Seg_4512" s="T317">n:Time</ta>
            <ta e="T321" id="Seg_4513" s="T320">np.h:A</ta>
            <ta e="T325" id="Seg_4514" s="T324">0.3.h:Poss np:Th</ta>
            <ta e="T329" id="Seg_4515" s="T328">0.3:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_4516" s="T1">np.h:S</ta>
            <ta e="T3" id="Seg_4517" s="T2">v:pred</ta>
            <ta e="T6" id="Seg_4518" s="T5">0.3.h:S adj:pred</ta>
            <ta e="T7" id="Seg_4519" s="T6">pro.h:S</ta>
            <ta e="T8" id="Seg_4520" s="T7">n:pred</ta>
            <ta e="T9" id="Seg_4521" s="T8">cop</ta>
            <ta e="T11" id="Seg_4522" s="T10">np:O</ta>
            <ta e="T13" id="Seg_4523" s="T11">0.3.h:S v:pred</ta>
            <ta e="T16" id="Seg_4524" s="T13">s:comp</ta>
            <ta e="T17" id="Seg_4525" s="T16">0.3.h:S v:pred</ta>
            <ta e="T23" id="Seg_4526" s="T22">0.1.h:S v:pred</ta>
            <ta e="T24" id="Seg_4527" s="T23">0.3.h:S v:pred</ta>
            <ta e="T26" id="Seg_4528" s="T24">s:adv</ta>
            <ta e="T27" id="Seg_4529" s="T26">0.3.h:S v:pred</ta>
            <ta e="T28" id="Seg_4530" s="T27">np:O</ta>
            <ta e="T29" id="Seg_4531" s="T28">0.3.h:S v:pred</ta>
            <ta e="T30" id="Seg_4532" s="T29">pro.h:S</ta>
            <ta e="T31" id="Seg_4533" s="T30">np:O</ta>
            <ta e="T33" id="Seg_4534" s="T32">np.h:S</ta>
            <ta e="T34" id="Seg_4535" s="T33">n:pred</ta>
            <ta e="T35" id="Seg_4536" s="T34">cop</ta>
            <ta e="T39" id="Seg_4537" s="T38">np:S</ta>
            <ta e="T40" id="Seg_4538" s="T39">v:pred</ta>
            <ta e="T42" id="Seg_4539" s="T41">np.h:S</ta>
            <ta e="T43" id="Seg_4540" s="T42">v:pred</ta>
            <ta e="T45" id="Seg_4541" s="T43">s:comp</ta>
            <ta e="T46" id="Seg_4542" s="T45">np:S</ta>
            <ta e="T47" id="Seg_4543" s="T46">adj:pred</ta>
            <ta e="T50" id="Seg_4544" s="T48">adj:pred</ta>
            <ta e="T51" id="Seg_4545" s="T50">ptcl:pred</ta>
            <ta e="T52" id="Seg_4546" s="T51">np.h:S</ta>
            <ta e="T53" id="Seg_4547" s="T52">v:pred</ta>
            <ta e="T55" id="Seg_4548" s="T53">s:temp</ta>
            <ta e="T57" id="Seg_4549" s="T56">adj:pred</ta>
            <ta e="T58" id="Seg_4550" s="T57">0.1.h:S cop</ta>
            <ta e="T59" id="Seg_4551" s="T58">0.1.h:S v:pred</ta>
            <ta e="T60" id="Seg_4552" s="T59">np:O</ta>
            <ta e="T61" id="Seg_4553" s="T60">0.1.h:S v:pred</ta>
            <ta e="T66" id="Seg_4554" s="T65">np.h:S</ta>
            <ta e="T67" id="Seg_4555" s="T66">ptcl:pred</ta>
            <ta e="T69" id="Seg_4556" s="T68">0.3.h:S adj:pred</ta>
            <ta e="T70" id="Seg_4557" s="T69">pro.h:S</ta>
            <ta e="T71" id="Seg_4558" s="T70">v:pred</ta>
            <ta e="T74" id="Seg_4559" s="T71">s:purp</ta>
            <ta e="T75" id="Seg_4560" s="T74">pro.h:S</ta>
            <ta e="T76" id="Seg_4561" s="T75">s:adv</ta>
            <ta e="T77" id="Seg_4562" s="T76">cop</ta>
            <ta e="T78" id="Seg_4563" s="T77">np.h:O</ta>
            <ta e="T79" id="Seg_4564" s="T78">0.2.h:S v:pred</ta>
            <ta e="T83" id="Seg_4565" s="T82">np.h:O</ta>
            <ta e="T84" id="Seg_4566" s="T83">np.h:O</ta>
            <ta e="T86" id="Seg_4567" s="T85">pro.h:O</ta>
            <ta e="T87" id="Seg_4568" s="T86">pro.h:S</ta>
            <ta e="T88" id="Seg_4569" s="T87">v:pred</ta>
            <ta e="T90" id="Seg_4570" s="T89">v:pred</ta>
            <ta e="T94" id="Seg_4571" s="T93">0.1.h:S v:pred</ta>
            <ta e="T97" id="Seg_4572" s="T96">0.1.h:S v:pred</ta>
            <ta e="T99" id="Seg_4573" s="T98">0.1.h:S v:pred</ta>
            <ta e="T100" id="Seg_4574" s="T99">np.h:S</ta>
            <ta e="T102" id="Seg_4575" s="T100">s:purp</ta>
            <ta e="T103" id="Seg_4576" s="T102">v:pred</ta>
            <ta e="T104" id="Seg_4577" s="T103">np.h:S</ta>
            <ta e="T105" id="Seg_4578" s="T104">s:adv</ta>
            <ta e="T106" id="Seg_4579" s="T105">cop</ta>
            <ta e="T110" id="Seg_4580" s="T109">0.3.h:S v:pred</ta>
            <ta e="T112" id="Seg_4581" s="T111">0.3.h:S v:pred</ta>
            <ta e="T116" id="Seg_4582" s="T112">s:temp</ta>
            <ta e="T117" id="Seg_4583" s="T116">np:O</ta>
            <ta e="T118" id="Seg_4584" s="T117">np:O</ta>
            <ta e="T119" id="Seg_4585" s="T118">0.2.h:S v:pred</ta>
            <ta e="T120" id="Seg_4586" s="T119">np:O</ta>
            <ta e="T121" id="Seg_4587" s="T120">0.2.h:S v:pred</ta>
            <ta e="T122" id="Seg_4588" s="T121">s:adv</ta>
            <ta e="T123" id="Seg_4589" s="T122">s:adv</ta>
            <ta e="T124" id="Seg_4590" s="T123">0.3.h:S cop</ta>
            <ta e="T127" id="Seg_4591" s="T126">0.1.h:S v:pred</ta>
            <ta e="T129" id="Seg_4592" s="T128">np:O</ta>
            <ta e="T130" id="Seg_4593" s="T129">0.2.h:S v:pred</ta>
            <ta e="T134" id="Seg_4594" s="T133">0.1.h:S v:pred</ta>
            <ta e="T136" id="Seg_4595" s="T135">pro.h:S</ta>
            <ta e="T137" id="Seg_4596" s="T136">np:O</ta>
            <ta e="T138" id="Seg_4597" s="T137">v:pred</ta>
            <ta e="T140" id="Seg_4598" s="T139">0.2.h:S v:pred</ta>
            <ta e="T142" id="Seg_4599" s="T141">pro.h:S</ta>
            <ta e="T144" id="Seg_4600" s="T142">s:adv</ta>
            <ta e="T146" id="Seg_4601" s="T144">v:pred</ta>
            <ta e="T147" id="Seg_4602" s="T146">np.h:O</ta>
            <ta e="T148" id="Seg_4603" s="T147">0.1.h:S v:pred</ta>
            <ta e="T149" id="Seg_4604" s="T148">pro.h:S</ta>
            <ta e="T151" id="Seg_4605" s="T150">v:pred</ta>
            <ta e="T152" id="Seg_4606" s="T151">np.h:S</ta>
            <ta e="T153" id="Seg_4607" s="T152">v:pred</ta>
            <ta e="T154" id="Seg_4608" s="T153">0.3.h:S v:pred</ta>
            <ta e="T157" id="Seg_4609" s="T154">s:adv</ta>
            <ta e="T159" id="Seg_4610" s="T158">np.h:S</ta>
            <ta e="T160" id="Seg_4611" s="T159">v:pred</ta>
            <ta e="T164" id="Seg_4612" s="T163">np.h:S</ta>
            <ta e="T165" id="Seg_4613" s="T164">v:pred</ta>
            <ta e="T167" id="Seg_4614" s="T166">np:S</ta>
            <ta e="T168" id="Seg_4615" s="T167">n:pred</ta>
            <ta e="T170" id="Seg_4616" s="T169">cop</ta>
            <ta e="T171" id="Seg_4617" s="T170">np.h:S</ta>
            <ta e="T172" id="Seg_4618" s="T171">np:O</ta>
            <ta e="T173" id="Seg_4619" s="T172">np:O</ta>
            <ta e="T174" id="Seg_4620" s="T173">v:pred</ta>
            <ta e="T175" id="Seg_4621" s="T174">np:O</ta>
            <ta e="T176" id="Seg_4622" s="T175">0.3.h:S v:pred</ta>
            <ta e="T178" id="Seg_4623" s="T177">np.h:S</ta>
            <ta e="T179" id="Seg_4624" s="T178">v:pred</ta>
            <ta e="T181" id="Seg_4625" s="T180">0.3.h:S v:pred</ta>
            <ta e="T182" id="Seg_4626" s="T181">np.h:S</ta>
            <ta e="T183" id="Seg_4627" s="T182">v:pred</ta>
            <ta e="T184" id="Seg_4628" s="T183">s:adv</ta>
            <ta e="T187" id="Seg_4629" s="T186">0.3.h:S v:pred</ta>
            <ta e="T188" id="Seg_4630" s="T187">np.h:S</ta>
            <ta e="T190" id="Seg_4631" s="T188">s:adv</ta>
            <ta e="T192" id="Seg_4632" s="T191">np:O</ta>
            <ta e="T193" id="Seg_4633" s="T192">v:pred</ta>
            <ta e="T196" id="Seg_4634" s="T195">0.3.h:S v:pred</ta>
            <ta e="T200" id="Seg_4635" s="T198">s:temp</ta>
            <ta e="T201" id="Seg_4636" s="T200">np.h:S</ta>
            <ta e="T202" id="Seg_4637" s="T201">np:O</ta>
            <ta e="T203" id="Seg_4638" s="T202">v:pred</ta>
            <ta e="T204" id="Seg_4639" s="T203">np.h:S</ta>
            <ta e="T205" id="Seg_4640" s="T204">np:O</ta>
            <ta e="T206" id="Seg_4641" s="T205">v:pred</ta>
            <ta e="T207" id="Seg_4642" s="T206">np.h:S</ta>
            <ta e="T209" id="Seg_4643" s="T207">v:pred</ta>
            <ta e="T215" id="Seg_4644" s="T214">s:adv</ta>
            <ta e="T217" id="Seg_4645" s="T216">np:S</ta>
            <ta e="T221" id="Seg_4646" s="T219">s:comp</ta>
            <ta e="T223" id="Seg_4647" s="T221">0.1.h:S cop</ta>
            <ta e="T225" id="Seg_4648" s="T223">s:adv</ta>
            <ta e="T228" id="Seg_4649" s="T226">s:temp</ta>
            <ta e="T230" id="Seg_4650" s="T228">0.3.h:S v:pred</ta>
            <ta e="T231" id="Seg_4651" s="T230">np.h:S</ta>
            <ta e="T232" id="Seg_4652" s="T231">v:pred</ta>
            <ta e="T234" id="Seg_4653" s="T233">np.h:S</ta>
            <ta e="T238" id="Seg_4654" s="T236">v:pred</ta>
            <ta e="T239" id="Seg_4655" s="T238">np.h:S</ta>
            <ta e="T241" id="Seg_4656" s="T239">v:pred</ta>
            <ta e="T245" id="Seg_4657" s="T241">s:adv</ta>
            <ta e="T246" id="Seg_4658" s="T245">0.3.h:S v:pred</ta>
            <ta e="T248" id="Seg_4659" s="T247">np.h:S</ta>
            <ta e="T251" id="Seg_4660" s="T250">v:pred</ta>
            <ta e="T252" id="Seg_4661" s="T251">s:adv</ta>
            <ta e="T253" id="Seg_4662" s="T252">np.h:S</ta>
            <ta e="T255" id="Seg_4663" s="T254">v:pred</ta>
            <ta e="T261" id="Seg_4664" s="T260">np:O</ta>
            <ta e="T262" id="Seg_4665" s="T261">0.3.h:S v:pred</ta>
            <ta e="T264" id="Seg_4666" s="T263">np.h:S</ta>
            <ta e="T268" id="Seg_4667" s="T267">v:pred</ta>
            <ta e="T271" id="Seg_4668" s="T270">np.h:S</ta>
            <ta e="T274" id="Seg_4669" s="T273">v:pred</ta>
            <ta e="T277" id="Seg_4670" s="T276">np.h:O</ta>
            <ta e="T281" id="Seg_4671" s="T279">0.3.h:S v:pred</ta>
            <ta e="T283" id="Seg_4672" s="T281">s:adv</ta>
            <ta e="T287" id="Seg_4673" s="T286">0.3.h:S v:pred</ta>
            <ta e="T290" id="Seg_4674" s="T288">s:comp</ta>
            <ta e="T291" id="Seg_4675" s="T290">ptcl:pred</ta>
            <ta e="T295" id="Seg_4676" s="T293">s:comp</ta>
            <ta e="T296" id="Seg_4677" s="T295">cop</ta>
            <ta e="T297" id="Seg_4678" s="T296">np.h:S</ta>
            <ta e="T302" id="Seg_4679" s="T301">n:pred</ta>
            <ta e="T303" id="Seg_4680" s="T302">cop</ta>
            <ta e="T304" id="Seg_4681" s="T303">np:S</ta>
            <ta e="T306" id="Seg_4682" s="T305">ptcl:pred</ta>
            <ta e="T307" id="Seg_4683" s="T306">cop</ta>
            <ta e="T309" id="Seg_4684" s="T308">s:comp</ta>
            <ta e="T310" id="Seg_4685" s="T309">0.3.h:S cop</ta>
            <ta e="T316" id="Seg_4686" s="T315">np:S</ta>
            <ta e="T319" id="Seg_4687" s="T318">adj:pred</ta>
            <ta e="T320" id="Seg_4688" s="T319">cop</ta>
            <ta e="T325" id="Seg_4689" s="T324">np:S</ta>
            <ta e="T327" id="Seg_4690" s="T326">n:pred</ta>
            <ta e="T328" id="Seg_4691" s="T327">cop</ta>
            <ta e="T329" id="Seg_4692" s="T328">adj:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST">
            <ta e="T2" id="Seg_4693" s="T1">new</ta>
            <ta e="T4" id="Seg_4694" s="T3">new</ta>
            <ta e="T6" id="Seg_4695" s="T5">0.giv-active new</ta>
            <ta e="T7" id="Seg_4696" s="T6">giv-active</ta>
            <ta e="T10" id="Seg_4697" s="T9">giv-active</ta>
            <ta e="T11" id="Seg_4698" s="T10">accs-inf</ta>
            <ta e="T13" id="Seg_4699" s="T11">0.giv-active</ta>
            <ta e="T15" id="Seg_4700" s="T14">new</ta>
            <ta e="T17" id="Seg_4701" s="T16">0.giv-active</ta>
            <ta e="T20" id="Seg_4702" s="T19">new-Q</ta>
            <ta e="T22" id="Seg_4703" s="T21">giv-active-Q</ta>
            <ta e="T23" id="Seg_4704" s="T22">0.giv-active-Q</ta>
            <ta e="T24" id="Seg_4705" s="T23">0.quot-sp</ta>
            <ta e="T27" id="Seg_4706" s="T26">0.giv-inactive</ta>
            <ta e="T28" id="Seg_4707" s="T27">new</ta>
            <ta e="T29" id="Seg_4708" s="T28">0.giv-active</ta>
            <ta e="T30" id="Seg_4709" s="T29">accs-inf</ta>
            <ta e="T31" id="Seg_4710" s="T30">new</ta>
            <ta e="T33" id="Seg_4711" s="T32">accs-inf</ta>
            <ta e="T38" id="Seg_4712" s="T37">new</ta>
            <ta e="T42" id="Seg_4713" s="T41">giv-inactive</ta>
            <ta e="T44" id="Seg_4714" s="T43">new</ta>
            <ta e="T46" id="Seg_4715" s="T45">giv-active</ta>
            <ta e="T52" id="Seg_4716" s="T51">giv-inactive</ta>
            <ta e="T53" id="Seg_4717" s="T52">quot-sp</ta>
            <ta e="T54" id="Seg_4718" s="T53">giv-inactive-Q</ta>
            <ta e="T55" id="Seg_4719" s="T54">0.giv-inactive-Q</ta>
            <ta e="T58" id="Seg_4720" s="T57">0.giv-active-Q</ta>
            <ta e="T59" id="Seg_4721" s="T58">0.giv-active-Q</ta>
            <ta e="T60" id="Seg_4722" s="T59">giv-active-Q</ta>
            <ta e="T61" id="Seg_4723" s="T60">0.giv-active-Q</ta>
            <ta e="T64" id="Seg_4724" s="T63">accs-sit-Q</ta>
            <ta e="T65" id="Seg_4725" s="T64">accs-inf-Q</ta>
            <ta e="T66" id="Seg_4726" s="T65">new-Q</ta>
            <ta e="T69" id="Seg_4727" s="T68">0.giv-active-Q new-Q</ta>
            <ta e="T70" id="Seg_4728" s="T69">giv-inactive-Q</ta>
            <ta e="T74" id="Seg_4729" s="T73">giv-active-Q</ta>
            <ta e="T75" id="Seg_4730" s="T74">giv-inactive-Q</ta>
            <ta e="T78" id="Seg_4731" s="T77">giv-inactive-Q</ta>
            <ta e="T79" id="Seg_4732" s="T78">0.giv-inactive-Q</ta>
            <ta e="T80" id="Seg_4733" s="T79">giv-active-Q</ta>
            <ta e="T83" id="Seg_4734" s="T82">giv-inactive-Q</ta>
            <ta e="T86" id="Seg_4735" s="T85">giv-active-Q</ta>
            <ta e="T87" id="Seg_4736" s="T86">giv-inactive-Q</ta>
            <ta e="T94" id="Seg_4737" s="T93">0.giv-active-Q</ta>
            <ta e="T95" id="Seg_4738" s="T94">accs-gen-Q</ta>
            <ta e="T97" id="Seg_4739" s="T96">0.giv-active-Q</ta>
            <ta e="T99" id="Seg_4740" s="T98">0.giv-active-Q</ta>
            <ta e="T100" id="Seg_4741" s="T99">giv-active</ta>
            <ta e="T101" id="Seg_4742" s="T100">giv-inactive</ta>
            <ta e="T104" id="Seg_4743" s="T103">giv-inactive</ta>
            <ta e="T107" id="Seg_4744" s="T106">new</ta>
            <ta e="T108" id="Seg_4745" s="T107">new</ta>
            <ta e="T109" id="Seg_4746" s="T108">accs-inf</ta>
            <ta e="T110" id="Seg_4747" s="T109">0.giv-active</ta>
            <ta e="T111" id="Seg_4748" s="T110">giv-inactive</ta>
            <ta e="T112" id="Seg_4749" s="T111">0.quot-sp</ta>
            <ta e="T113" id="Seg_4750" s="T112">giv-inactive-Q</ta>
            <ta e="T114" id="Seg_4751" s="T113">giv-inactive-Q</ta>
            <ta e="T117" id="Seg_4752" s="T116">accs-sit-Q</ta>
            <ta e="T118" id="Seg_4753" s="T117">accs-sit-Q</ta>
            <ta e="T119" id="Seg_4754" s="T118">0.giv-active-Q</ta>
            <ta e="T120" id="Seg_4755" s="T119">accs-sit-Q</ta>
            <ta e="T121" id="Seg_4756" s="T120">0.giv-active-Q</ta>
            <ta e="T123" id="Seg_4757" s="T122">0.giv-inactive</ta>
            <ta e="T127" id="Seg_4758" s="T126">0.quot-sp-Q</ta>
            <ta e="T129" id="Seg_4759" s="T128">new-Q</ta>
            <ta e="T130" id="Seg_4760" s="T129">0.giv-inactive-Q</ta>
            <ta e="T132" id="Seg_4761" s="T130">giv-active-Q</ta>
            <ta e="T134" id="Seg_4762" s="T133">0.quot-sp-Q</ta>
            <ta e="T136" id="Seg_4763" s="T135">giv-active-Q</ta>
            <ta e="T137" id="Seg_4764" s="T136">new-Q</ta>
            <ta e="T138" id="Seg_4765" s="T137">0.giv-inactive-Q</ta>
            <ta e="T139" id="Seg_4766" s="T138">giv-inactive-Q</ta>
            <ta e="T140" id="Seg_4767" s="T139">0.giv-active-Q</ta>
            <ta e="T142" id="Seg_4768" s="T141">giv-active-Q</ta>
            <ta e="T147" id="Seg_4769" s="T146">giv-inactive-Q</ta>
            <ta e="T148" id="Seg_4770" s="T147">0.giv-active-Q</ta>
            <ta e="T149" id="Seg_4771" s="T148">giv-inactive-Q</ta>
            <ta e="T152" id="Seg_4772" s="T151">giv-inactive</ta>
            <ta e="T158" id="Seg_4773" s="T157">giv-inactive</ta>
            <ta e="T159" id="Seg_4774" s="T158">giv-inactive</ta>
            <ta e="T161" id="Seg_4775" s="T160">giv-active</ta>
            <ta e="T164" id="Seg_4776" s="T163">giv-active</ta>
            <ta e="T166" id="Seg_4777" s="T165">giv-inactive</ta>
            <ta e="T167" id="Seg_4778" s="T166">accs-inf</ta>
            <ta e="T168" id="Seg_4779" s="T167">giv-active</ta>
            <ta e="T171" id="Seg_4780" s="T170">giv-inactive</ta>
            <ta e="T172" id="Seg_4781" s="T171">giv-inactive</ta>
            <ta e="T173" id="Seg_4782" s="T172">giv-inactive</ta>
            <ta e="T175" id="Seg_4783" s="T174">giv-inactive</ta>
            <ta e="T177" id="Seg_4784" s="T176">giv-inactive</ta>
            <ta e="T178" id="Seg_4785" s="T177">giv-inactive</ta>
            <ta e="T181" id="Seg_4786" s="T180">0.giv-active</ta>
            <ta e="T182" id="Seg_4787" s="T181">giv-inactive</ta>
            <ta e="T187" id="Seg_4788" s="T186">0.quot-sp</ta>
            <ta e="T188" id="Seg_4789" s="T187">giv-inactive</ta>
            <ta e="T191" id="Seg_4790" s="T190">giv-active</ta>
            <ta e="T192" id="Seg_4791" s="T191">giv-inactive</ta>
            <ta e="T196" id="Seg_4792" s="T195">0.quot-sp</ta>
            <ta e="T200" id="Seg_4793" s="T199">0.quot-sp</ta>
            <ta e="T201" id="Seg_4794" s="T200">giv-inactive</ta>
            <ta e="T202" id="Seg_4795" s="T201">giv-inactive</ta>
            <ta e="T204" id="Seg_4796" s="T203">accs-sit</ta>
            <ta e="T205" id="Seg_4797" s="T204">new</ta>
            <ta e="T207" id="Seg_4798" s="T206">giv-inactive</ta>
            <ta e="T215" id="Seg_4799" s="T214">0.quot-sp</ta>
            <ta e="T217" id="Seg_4800" s="T216">accs-inf</ta>
            <ta e="T219" id="Seg_4801" s="T218">accs-sit-Q</ta>
            <ta e="T223" id="Seg_4802" s="T221">0.giv-inactive-Q</ta>
            <ta e="T224" id="Seg_4803" s="T223">0.quot-sp</ta>
            <ta e="T226" id="Seg_4804" s="T225">giv-inactive</ta>
            <ta e="T227" id="Seg_4805" s="T226">giv-active</ta>
            <ta e="T231" id="Seg_4806" s="T230">giv-inactive</ta>
            <ta e="T233" id="Seg_4807" s="T232">accs-sit</ta>
            <ta e="T234" id="Seg_4808" s="T233">giv-active</ta>
            <ta e="T235" id="Seg_4809" s="T234">giv-inactive</ta>
            <ta e="T236" id="Seg_4810" s="T235">giv-active</ta>
            <ta e="T239" id="Seg_4811" s="T238">giv-active</ta>
            <ta e="T243" id="Seg_4812" s="T242">accs-inf</ta>
            <ta e="T246" id="Seg_4813" s="T245">0.giv-active</ta>
            <ta e="T247" id="Seg_4814" s="T246">giv-inactive</ta>
            <ta e="T248" id="Seg_4815" s="T247">giv-active</ta>
            <ta e="T250" id="Seg_4816" s="T249">accs-sit</ta>
            <ta e="T253" id="Seg_4817" s="T252">giv-inactive</ta>
            <ta e="T254" id="Seg_4818" s="T253">giv-inactive</ta>
            <ta e="T258" id="Seg_4819" s="T257">accs-inf</ta>
            <ta e="T261" id="Seg_4820" s="T260">giv-active</ta>
            <ta e="T262" id="Seg_4821" s="T261">0.giv-active-Q</ta>
            <ta e="T263" id="Seg_4822" s="T262">giv-inactive</ta>
            <ta e="T264" id="Seg_4823" s="T263">giv-inactive</ta>
            <ta e="T266" id="Seg_4824" s="T265">giv-inactive</ta>
            <ta e="T267" id="Seg_4825" s="T266">giv-inactive</ta>
            <ta e="T270" id="Seg_4826" s="T269">giv-active</ta>
            <ta e="T271" id="Seg_4827" s="T270">accs-inf</ta>
            <ta e="T273" id="Seg_4828" s="T272">giv-active</ta>
            <ta e="T275" id="Seg_4829" s="T274">giv-inactive</ta>
            <ta e="T277" id="Seg_4830" s="T276">giv-inactive</ta>
            <ta e="T281" id="Seg_4831" s="T280">0.giv-inactive</ta>
            <ta e="T282" id="Seg_4832" s="T281">giv-active</ta>
            <ta e="T283" id="Seg_4833" s="T282">0.giv-active</ta>
            <ta e="T287" id="Seg_4834" s="T286">0.giv-active</ta>
            <ta e="T289" id="Seg_4835" s="T288">giv-inactive</ta>
            <ta e="T294" id="Seg_4836" s="T293">giv-active</ta>
            <ta e="T297" id="Seg_4837" s="T296">accs-gen</ta>
            <ta e="T300" id="Seg_4838" s="T299">giv-inactive</ta>
            <ta e="T307" id="Seg_4839" s="T306">0.giv-active</ta>
            <ta e="T308" id="Seg_4840" s="T307">giv-active</ta>
            <ta e="T310" id="Seg_4841" s="T309">0.giv-active</ta>
            <ta e="T312" id="Seg_4842" s="T311">giv-inactive</ta>
            <ta e="T314" id="Seg_4843" s="T313">giv-active</ta>
            <ta e="T315" id="Seg_4844" s="T314">giv-inactive</ta>
            <ta e="T316" id="Seg_4845" s="T315">accs-inf</ta>
            <ta e="T321" id="Seg_4846" s="T320">giv-inactive</ta>
            <ta e="T325" id="Seg_4847" s="T324">giv-inactive</ta>
            <ta e="T326" id="Seg_4848" s="T325">accs-inf</ta>
         </annotation>
         <annotation name="Top" tierref="Top">
            <ta e="T1" id="Seg_4849" s="T0">top.int.concr.</ta>
            <ta e="T6" id="Seg_4850" s="T5">0.top.int.concr</ta>
            <ta e="T7" id="Seg_4851" s="T6">top.int.concr</ta>
            <ta e="T13" id="Seg_4852" s="T11">0.top.int.concr</ta>
            <ta e="T17" id="Seg_4853" s="T16">0.top.int.concr</ta>
            <ta e="T23" id="Seg_4854" s="T22">0.top.int.concr</ta>
            <ta e="T27" id="Seg_4855" s="T26">0.top.int.concr</ta>
            <ta e="T29" id="Seg_4856" s="T28">0.top.int.concr</ta>
            <ta e="T30" id="Seg_4857" s="T29">top.int.concr</ta>
            <ta e="T33" id="Seg_4858" s="T31">top.int.concr</ta>
            <ta e="T37" id="Seg_4859" s="T35">top.int.concr.</ta>
            <ta e="T42" id="Seg_4860" s="T40">top.int.concr</ta>
            <ta e="T46" id="Seg_4861" s="T45">top.int.concr</ta>
            <ta e="T52" id="Seg_4862" s="T51">top.int.concr</ta>
            <ta e="T55" id="Seg_4863" s="T54">0.top.int.concr</ta>
            <ta e="T58" id="Seg_4864" s="T57">0.top.int.concr</ta>
            <ta e="T59" id="Seg_4865" s="T58">0.top.int.concr</ta>
            <ta e="T61" id="Seg_4866" s="T60">0.top.int.concr</ta>
            <ta e="T64" id="Seg_4867" s="T61">top.int.concr</ta>
            <ta e="T69" id="Seg_4868" s="T68">0.top.int.concr</ta>
            <ta e="T70" id="Seg_4869" s="T69">top.int.concr</ta>
            <ta e="T75" id="Seg_4870" s="T74">top.int.concr</ta>
            <ta e="T79" id="Seg_4871" s="T78">0.top.int.concr</ta>
            <ta e="T81" id="Seg_4872" s="T79">top.int.concr</ta>
            <ta e="T86" id="Seg_4873" s="T85">top.int.concr</ta>
            <ta e="T91" id="Seg_4874" s="T90">top.int.concr.</ta>
            <ta e="T97" id="Seg_4875" s="T96">0.top.int.concr</ta>
            <ta e="T99" id="Seg_4876" s="T98">0.top.int.concr</ta>
            <ta e="T100" id="Seg_4877" s="T99">top.int.concr</ta>
            <ta e="T104" id="Seg_4878" s="T103">top.int.concr</ta>
            <ta e="T110" id="Seg_4879" s="T109">0.top.int.concr</ta>
            <ta e="T112" id="Seg_4880" s="T111">0.top.int.concr</ta>
            <ta e="T114" id="Seg_4881" s="T112">top.int.concr</ta>
            <ta e="T119" id="Seg_4882" s="T118">0.top.int.concr</ta>
            <ta e="T121" id="Seg_4883" s="T120">0.top.int.concr</ta>
            <ta e="T123" id="Seg_4884" s="T122">0.top.int.concr</ta>
            <ta e="T128" id="Seg_4885" s="T127">top.int.concr.</ta>
            <ta e="T135" id="Seg_4886" s="T134">top.int.concr.</ta>
            <ta e="T140" id="Seg_4887" s="T139">0.top.int.abstr.</ta>
            <ta e="T141" id="Seg_4888" s="T140">top.int.concr.</ta>
            <ta e="T149" id="Seg_4889" s="T148">top.int.concr</ta>
            <ta e="T152" id="Seg_4890" s="T151">top.int.concr</ta>
            <ta e="T159" id="Seg_4891" s="T157">top.int.concr</ta>
            <ta e="T162" id="Seg_4892" s="T160">top.int.concr</ta>
            <ta e="T167" id="Seg_4893" s="T165">top.int.concr</ta>
            <ta e="T171" id="Seg_4894" s="T170">top.int.concr</ta>
            <ta e="T178" id="Seg_4895" s="T176">top.int.concr</ta>
            <ta e="T181" id="Seg_4896" s="T180">0.top.int.concr</ta>
            <ta e="T182" id="Seg_4897" s="T181">top.int.concr</ta>
            <ta e="T188" id="Seg_4898" s="T187">top.int.concr</ta>
            <ta e="T201" id="Seg_4899" s="T200">top.int.concr</ta>
            <ta e="T204" id="Seg_4900" s="T203">top.int.concr.contr.</ta>
            <ta e="T207" id="Seg_4901" s="T206">top.int.concr</ta>
            <ta e="T217" id="Seg_4902" s="T215">top.int.concr</ta>
            <ta e="T226" id="Seg_4903" s="T225">top.int.concr</ta>
            <ta e="T231" id="Seg_4904" s="T230">top.int.concr.contr.</ta>
            <ta e="T234" id="Seg_4905" s="T233">top.int.concr</ta>
            <ta e="T239" id="Seg_4906" s="T238">top.int.concr.contr.</ta>
            <ta e="T246" id="Seg_4907" s="T245">0.top.int.concr</ta>
            <ta e="T248" id="Seg_4908" s="T246">top.int.concr</ta>
            <ta e="T253" id="Seg_4909" s="T252">top.int.concr</ta>
            <ta e="T262" id="Seg_4910" s="T261">0.top.int.concr</ta>
            <ta e="T264" id="Seg_4911" s="T262">top.int.concr</ta>
            <ta e="T269" id="Seg_4912" s="T268">top.int.concr.</ta>
            <ta e="T278" id="Seg_4913" s="T274">top.int.concr</ta>
            <ta e="T283" id="Seg_4914" s="T282">0.top.int.concr</ta>
            <ta e="T286" id="Seg_4915" s="T283">top.int.concr.</ta>
            <ta e="T288" id="Seg_4916" s="T287">top.int.concr.</ta>
            <ta e="T292" id="Seg_4917" s="T291">top.int.concr.</ta>
            <ta e="T299" id="Seg_4918" s="T297">top.int.concr</ta>
            <ta e="T307" id="Seg_4919" s="T306">0.top.int.concr</ta>
            <ta e="T310" id="Seg_4920" s="T309">0.top.int.concr</ta>
            <ta e="T316" id="Seg_4921" s="T310">top.int.concr</ta>
            <ta e="T325" id="Seg_4922" s="T320">top.int.concr.contr.</ta>
         </annotation>
         <annotation name="Foc" tierref="Foc">
            <ta e="T3" id="Seg_4923" s="T0">foc.wid</ta>
            <ta e="T6" id="Seg_4924" s="T4">foc.int</ta>
            <ta e="T9" id="Seg_4925" s="T7">foc.int</ta>
            <ta e="T13" id="Seg_4926" s="T9">foc.int</ta>
            <ta e="T16" id="Seg_4927" s="T13">foc.nar</ta>
            <ta e="T20" id="Seg_4928" s="T17">foc.nar</ta>
            <ta e="T22" id="Seg_4929" s="T20">foc.nar</ta>
            <ta e="T27" id="Seg_4930" s="T24">foc.int</ta>
            <ta e="T29" id="Seg_4931" s="T27">foc.int</ta>
            <ta e="T31" id="Seg_4932" s="T30">foc.contr</ta>
            <ta e="T35" id="Seg_4933" s="T31">foc.wid</ta>
            <ta e="T40" id="Seg_4934" s="T35">foc.wid</ta>
            <ta e="T43" id="Seg_4935" s="T42">foc.int</ta>
            <ta e="T47" id="Seg_4936" s="T46">foc.nar</ta>
            <ta e="T53" id="Seg_4937" s="T51">foc.wid</ta>
            <ta e="T55" id="Seg_4938" s="T53">foc.int</ta>
            <ta e="T57" id="Seg_4939" s="T55">foc.nar</ta>
            <ta e="T60" id="Seg_4940" s="T58">foc.ver</ta>
            <ta e="T61" id="Seg_4941" s="T60">foc.int</ta>
            <ta e="T66" id="Seg_4942" s="T64">foc.nar</ta>
            <ta e="T69" id="Seg_4943" s="T68">foc.nar</ta>
            <ta e="T74" id="Seg_4944" s="T70">foc.int</ta>
            <ta e="T77" id="Seg_4945" s="T75">foc.int</ta>
            <ta e="T79" id="Seg_4946" s="T77">foc.int</ta>
            <ta e="T84" id="Seg_4947" s="T79">foc.wid</ta>
            <ta e="T90" id="Seg_4948" s="T87">foc.int</ta>
            <ta e="T94" id="Seg_4949" s="T90">foc.wid</ta>
            <ta e="T97" id="Seg_4950" s="T94">foc.int</ta>
            <ta e="T99" id="Seg_4951" s="T97">foc.int</ta>
            <ta e="T103" id="Seg_4952" s="T100">foc.int</ta>
            <ta e="T106" id="Seg_4953" s="T104">foc.int</ta>
            <ta e="T110" id="Seg_4954" s="T106">foc.int</ta>
            <ta e="T111" id="Seg_4955" s="T110">foc.nar</ta>
            <ta e="T116" id="Seg_4956" s="T114">foc.int</ta>
            <ta e="T119" id="Seg_4957" s="T116">foc.int</ta>
            <ta e="T121" id="Seg_4958" s="T119">foc.int</ta>
            <ta e="T124" id="Seg_4959" s="T121">foc.wid</ta>
            <ta e="T129" id="Seg_4960" s="T128">foc.nar</ta>
            <ta e="T137" id="Seg_4961" s="T136">foc.nar</ta>
            <ta e="T140" id="Seg_4962" s="T138">foc.int</ta>
            <ta e="T148" id="Seg_4963" s="T140">foc.wid</ta>
            <ta e="T149" id="Seg_4964" s="T148">foc.nar</ta>
            <ta e="T157" id="Seg_4965" s="T152">foc.int</ta>
            <ta e="T160" id="Seg_4966" s="T159">foc.int</ta>
            <ta e="T165" id="Seg_4967" s="T160">foc.wid</ta>
            <ta e="T170" id="Seg_4968" s="T167">foc.int</ta>
            <ta e="T172" id="Seg_4969" s="T171">foc.nar</ta>
            <ta e="T173" id="Seg_4970" s="T172">foc.nar</ta>
            <ta e="T175" id="Seg_4971" s="T174">foc.nar</ta>
            <ta e="T179" id="Seg_4972" s="T178">foc.int</ta>
            <ta e="T181" id="Seg_4973" s="T179">foc.int</ta>
            <ta e="T183" id="Seg_4974" s="T182">foc.int</ta>
            <ta e="T192" id="Seg_4975" s="T191">foc.nar</ta>
            <ta e="T202" id="Seg_4976" s="T201">foc.contr</ta>
            <ta e="T205" id="Seg_4977" s="T204">foc.contr</ta>
            <ta e="T209" id="Seg_4978" s="T207">foc.int</ta>
            <ta e="T225" id="Seg_4979" s="T217">foc.int</ta>
            <ta e="T230" id="Seg_4980" s="T226">foc.int</ta>
            <ta e="T233" id="Seg_4981" s="T231">foc.int</ta>
            <ta e="T238" id="Seg_4982" s="T234">foc.int</ta>
            <ta e="T241" id="Seg_4983" s="T239">foc.int</ta>
            <ta e="T243" id="Seg_4984" s="T241">foc.nar</ta>
            <ta e="T251" id="Seg_4985" s="T248">foc.int</ta>
            <ta e="T255" id="Seg_4986" s="T253">foc.int</ta>
            <ta e="T261" id="Seg_4987" s="T258">foc.nar</ta>
            <ta e="T268" id="Seg_4988" s="T264">foc.int</ta>
            <ta e="T274" id="Seg_4989" s="T268">foc.wid</ta>
            <ta e="T279" id="Seg_4990" s="T278">foc.nar</ta>
            <ta e="T283" id="Seg_4991" s="T281">foc.int</ta>
            <ta e="T287" id="Seg_4992" s="T283">foc.wid</ta>
            <ta e="T291" id="Seg_4993" s="T287">foc.wid</ta>
            <ta e="T297" id="Seg_4994" s="T291">foc.wid</ta>
            <ta e="T303" id="Seg_4995" s="T299">foc.int</ta>
            <ta e="T305" id="Seg_4996" s="T303">foc.nar</ta>
            <ta e="T309" id="Seg_4997" s="T307">foc.nar</ta>
            <ta e="T320" id="Seg_4998" s="T318">foc.int</ta>
            <ta e="T328" id="Seg_4999" s="T325">foc.int</ta>
         </annotation>
         <annotation name="BOR" tierref="TIE0" />
         <annotation name="BOR-Phon" tierref="TIE2" />
         <annotation name="BOR-Morph" tierref="TIE3" />
         <annotation name="CS" tierref="TIE4" />
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_5000" s="T0">Long ago people lived, Evenki.</ta>
            <ta e="T13" id="Seg_5001" s="T4">They had an old man, this one was their leader, everyone followed his advise.</ta>
            <ta e="T17" id="Seg_5002" s="T13">He advises at what kind of place they should settle down. </ta>
            <ta e="T24" id="Seg_5003" s="T17">"We will live at such a place that is woody and warm!", he says.</ta>
            <ta e="T31" id="Seg_5004" s="T24">They arrived there and settled down, they built their yurt, some their poletent.</ta>
            <ta e="T35" id="Seg_5005" s="T31">Three neighbours became house mates.</ta>
            <ta e="T43" id="Seg_5006" s="T35">Eventually their meat reserves ran out, these people famished.</ta>
            <ta e="T51" id="Seg_5007" s="T43">Eating their reindeer — their reindeer were few, in the end maybe four or five.</ta>
            <ta e="T53" id="Seg_5008" s="T51">The old man advises: </ta>
            <ta e="T58" id="Seg_5009" s="T53">"If we eat our reindeer, we will be very poor.</ta>
            <ta e="T60" id="Seg_5010" s="T58">Let's not eat our reindeer.</ta>
            <ta e="T61" id="Seg_5011" s="T60">I advise:</ta>
            <ta e="T67" id="Seg_5012" s="T61">Here, at this river, there are our neighbours, the hares.</ta>
            <ta e="T69" id="Seg_5013" s="T67">They have their shamans there.</ta>
            <ta e="T74" id="Seg_5014" s="T69">We will betray them, to bring them here.</ta>
            <ta e="T77" id="Seg_5015" s="T74">I must sham illness.</ta>
            <ta e="T79" id="Seg_5016" s="T77">Invite their shamans.</ta>
            <ta e="T84" id="Seg_5017" s="T79">With them, many hares will come.</ta>
            <ta e="T90" id="Seg_5018" s="T84">And these, we will kill and eat.</ta>
            <ta e="T99" id="Seg_5019" s="T90">Then we can go on, we will make it to the warmer days of the year and live off the hunt."</ta>
            <ta e="T103" id="Seg_5020" s="T99">The people went for the hares.</ta>
            <ta e="T110" id="Seg_5021" s="T103">The old man shammed illness and lay down on his bed with his shoes on.</ta>
            <ta e="T112" id="Seg_5022" s="T110">To his old woman he says: </ta>
            <ta e="T121" id="Seg_5023" s="T112">"When the shaman of the hares comes and shamanizes, clog the window and the smoke hole."</ta>
            <ta e="T125" id="Seg_5024" s="T121">He moaned as if he was delirious: "Eh-eh-eh!</ta>
            <ta e="T132" id="Seg_5025" s="T125">Oldoonbo!", I will say, then you'll give me the hook. </ta>
            <ta e="T148" id="Seg_5026" s="T132">"Eh-eh! Godoroobo!", I will say, then you'll grab the leather scraper and position yourself at the door, then I'll jump up and start beating, I will kill their shaman. </ta>
            <ta e="T151" id="Seg_5027" s="T148">You will also try to attack him."</ta>
            <ta e="T157" id="Seg_5028" s="T151">The old man lies down as if he was delirious.</ta>
            <ta e="T165" id="Seg_5029" s="T157">The shaman of the hares comes, many hares came with him.</ta>
            <ta e="T170" id="Seg_5030" s="T165">The interior of the poletent got full of hares.</ta>
            <ta e="T176" id="Seg_5031" s="T170">The old woman clogged the window and the smoke hole, she blocked the door.</ta>
            <ta e="T181" id="Seg_5032" s="T176">The shaman of the hares shamanized, soon he jumped.</ta>
            <ta e="T185" id="Seg_5033" s="T181">The old man got sicker and he raved: "Eh-eh!</ta>
            <ta e="T187" id="Seg_5034" s="T185">Oldonboo!", he said.</ta>
            <ta e="T193" id="Seg_5035" s="T187">The old woman jumped up and passed the old man the hook.</ta>
            <ta e="T194" id="Seg_5036" s="T193">"Eh-eh</ta>
            <ta e="T196" id="Seg_5037" s="T194">Godoroobo!", he said.</ta>
            <ta e="T197" id="Seg_5038" s="T196">"Eh-eh!</ta>
            <ta e="T198" id="Seg_5039" s="T197">Hükechaanma!"</ta>
            <ta e="T206" id="Seg_5040" s="T198">As he said this, the old woman grabbed the leather scraper and the neighbour grabbed a small axe.</ta>
            <ta e="T209" id="Seg_5041" s="T206">And the shaman jumps: </ta>
            <ta e="T213" id="Seg_5042" s="T209">"Eduk djuuduk judjongoo bimii!</ta>
            <ta e="T215" id="Seg_5043" s="T213">Paapaldjiek!", he said.</ta>
            <ta e="T225" id="Seg_5044" s="T215">"I wonder if I will succeed getting out of this tent!" does this mean in Evenki.</ta>
            <ta e="T233" id="Seg_5045" s="T225">The old man had heard this and jumped up, the shaman jumped on the bar over the fire.</ta>
            <ta e="T246" id="Seg_5046" s="T233">The old man hit the pole over the fire with the hook, the shaman hare turned around and lost his two ears only. </ta>
            <ta e="T258" id="Seg_5047" s="T246">As the shaman of the hares had jumped onto the wood for the winter, the woman slashed at him with the leather scraper, she only hit his hind legs.</ta>
            <ta e="T262" id="Seg_5048" s="T258">She broke the two hind legs.</ta>
            <ta e="T274" id="Seg_5049" s="T262">The shaman of the hares jumped from the wood for the winter into the chimney, after him, his old wife also jumped into the chimney. </ta>
            <ta e="T281" id="Seg_5050" s="T274">All the hares in the tent were killed.</ta>
            <ta e="T287" id="Seg_5051" s="T281">They ate them, until it got warmer in spring, they lived this way.</ta>
            <ta e="T291" id="Seg_5052" s="T287">In the past, it is said, hares were not eaten.</ta>
            <ta e="T297" id="Seg_5053" s="T291">Only since then people started to eat hares.</ta>
            <ta e="T303" id="Seg_5054" s="T297">The shaman of the hares, who had escaped, became a wild hare.</ta>
            <ta e="T310" id="Seg_5055" s="T303">He forgot how to speak and he started to flee from people.</ta>
            <ta e="T328" id="Seg_5056" s="T310">The tip of the hare's ear was marked forever by the old man's blow, the hind legs broken by the old woman grew together.</ta>
            <ta e="T329" id="Seg_5057" s="T328">The end.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_5058" s="T0">Vor langer Zeit lebten Leute, Ewenken.</ta>
            <ta e="T13" id="Seg_5059" s="T4">Sie hatten einen alten Mann, dieser war ihr Anführer, auf seinen Rat hörten alle.</ta>
            <ta e="T17" id="Seg_5060" s="T13">Er rät, an was für einem Platz sie dauerhaft wohnen sollen.</ta>
            <ta e="T24" id="Seg_5061" s="T17">"Wir werden an so einem Ort leben, wo es Wald gibt und wo es warm ist!", sagt er.</ta>
            <ta e="T31" id="Seg_5062" s="T24">Sie kamen dorthin und ließen sich dort nieder - sie bauten ihre Jurte auf, einige ihr Stangenzelt.</ta>
            <ta e="T35" id="Seg_5063" s="T31">Drei Nachbarn fingen an zusammen zu wohnen.</ta>
            <ta e="T43" id="Seg_5064" s="T35">Irgendwann ging ihr Vorrat an Fleisch zu Ende, diese Menschen hungerten.</ta>
            <ta e="T51" id="Seg_5065" s="T43">Ihre Rentiere essen — Rentiere hatten sie wenig, am Ende vielleicht vier oder fünf.</ta>
            <ta e="T53" id="Seg_5066" s="T51">Der alte Mann rät: </ta>
            <ta e="T58" id="Seg_5067" s="T53">"Wenn wir unsere Rentiere essen, dann werden wir bettelarm.</ta>
            <ta e="T60" id="Seg_5068" s="T58">Lasst uns nicht die Rentiere essen.</ta>
            <ta e="T61" id="Seg_5069" s="T60">Ich rate:</ta>
            <ta e="T67" id="Seg_5070" s="T61">Hier am Fluss gibt es unsere Nachbarn, die Hasen.</ta>
            <ta e="T69" id="Seg_5071" s="T67">Sie haben dort ihren Schamanen.</ta>
            <ta e="T74" id="Seg_5072" s="T69">Wir betrügen sie, um sie hierher zu bringen.</ta>
            <ta e="T77" id="Seg_5073" s="T74">Ich muss mich krank stellen.</ta>
            <ta e="T79" id="Seg_5074" s="T77">Ladet ihren Schamanen ein.</ta>
            <ta e="T84" id="Seg_5075" s="T79">Mit ihm werden viele Hasen kommen.</ta>
            <ta e="T90" id="Seg_5076" s="T84">Und diese werden wir töten und essen.</ta>
            <ta e="T99" id="Seg_5077" s="T90">Dann geht es weiter für uns, wir erreichen die wärmeren Tage des Jahres und ernähren uns von der Jagd."</ta>
            <ta e="T103" id="Seg_5078" s="T99">Die Leute gingen und suchten die Hasen.</ta>
            <ta e="T110" id="Seg_5079" s="T103">Der alte Mann tat krank und legte sich mit Schuhen ins Bett.</ta>
            <ta e="T112" id="Seg_5080" s="T110">Zu seiner alten Frau sagt er: </ta>
            <ta e="T121" id="Seg_5081" s="T112">"Wenn der Schamane der Hasen kommt und schamanisiert, dann verstopfe das Fenster und das Rauchloch, versperr die Tür."</ta>
            <ta e="T125" id="Seg_5082" s="T121">Er stöhnte wie im Delirium: "Eh-eh-eh!</ta>
            <ta e="T132" id="Seg_5083" s="T125">Oldoonbo!", sage ich, dann gibst du mir den Haken. </ta>
            <ta e="T148" id="Seg_5084" s="T132">"Eh-eh! Godoroobo!", sage ich, dann greifst du den Lederschaber und stellst dich an die Tür, dann springe ich auf und fange an zu schlagen, ich werde ihren Schamanen umbringen.</ta>
            <ta e="T151" id="Seg_5085" s="T148">Du versuchst auch ihn zu greifen."</ta>
            <ta e="T157" id="Seg_5086" s="T151">Der alte Mann legt sich hin, als ob er im Delirium wäre.</ta>
            <ta e="T165" id="Seg_5087" s="T157">Der Schamane der Hasen kommt, mit ihm kamen viele Hasen.</ta>
            <ta e="T170" id="Seg_5088" s="T165">Das Innere des der Jurte wurde voll von Hasen.</ta>
            <ta e="T176" id="Seg_5089" s="T170">Die alte Frau verstopfte das Fenster und das Rauchloch, sie versperrte die Tür.</ta>
            <ta e="T181" id="Seg_5090" s="T176">Der Schamane der Hasen schamanisierte, bald sprang er.</ta>
            <ta e="T185" id="Seg_5091" s="T181">Dem alten Mann ging es schlechter und er delirierte: "Eh-eh!</ta>
            <ta e="T187" id="Seg_5092" s="T185">Oldoonbo!", sagte er.</ta>
            <ta e="T193" id="Seg_5093" s="T187">Die alte Frau sprang auf und reichte dem alten Mann den Haken.</ta>
            <ta e="T194" id="Seg_5094" s="T193">"Eh-eh</ta>
            <ta e="T196" id="Seg_5095" s="T194">Godoroobo!", sagte er.</ta>
            <ta e="T197" id="Seg_5096" s="T196">"Eh-eh!</ta>
            <ta e="T198" id="Seg_5097" s="T197">Hüketschaanma!"</ta>
            <ta e="T206" id="Seg_5098" s="T198">Als er das gesagt hatte, griff die alte Frau den Lederschaber und der Nachbar griff ein kleines Beil.</ta>
            <ta e="T209" id="Seg_5099" s="T206">Und der Schamane springt: </ta>
            <ta e="T213" id="Seg_5100" s="T209">"Eduk djuuduk judjongoo bimii!</ta>
            <ta e="T215" id="Seg_5101" s="T213">Paapaldjiek!", sagte er.</ta>
            <ta e="T225" id="Seg_5102" s="T215">"Gelingt es mir wohl aus diesem Zelt hinauszugehen!" bedeutet das auf Ewenkisch</ta>
            <ta e="T233" id="Seg_5103" s="T225">Der alte Mann hatte das gehört und sprang auf, der Schamane sprang auf die Stange über dem Feuer.</ta>
            <ta e="T246" id="Seg_5104" s="T233">Der alte Mann schlug mit dem Haken an die Stange über dem Feuer, der Schamanenhase drehte sich um und verlor nur seine beide Ohren.</ta>
            <ta e="T258" id="Seg_5105" s="T246">Als der Schamane der Hasen auf das Winterholz gesprungen war, schlägt die alte Frau mit dem Lederschaber nach ihm, sie traf nur die Hinterläufe.</ta>
            <ta e="T262" id="Seg_5106" s="T258">Sie brach die beiden Hinterläufe.</ta>
            <ta e="T274" id="Seg_5107" s="T262">Der Schamane der Hasen sprang vom Winterholz durchs Rauchloch, hinter ihm her sprang seine alte Häsin auch durchs Rauchloch.</ta>
            <ta e="T281" id="Seg_5108" s="T274">Die Hasen im Zelt brachten sie alle um.</ta>
            <ta e="T287" id="Seg_5109" s="T281">Diese aßen sie, bis es im Frühling wärmer wurde, lebten sie so.</ta>
            <ta e="T291" id="Seg_5110" s="T287">Früher, sagt man, aß man keine Hasen.</ta>
            <ta e="T297" id="Seg_5111" s="T291">Erst von da an fingen die Leute an, Hasen zu essen.</ta>
            <ta e="T303" id="Seg_5112" s="T297">Aus dem entflohenen Schamanen der Hasen wurde ein wilder Hase.</ta>
            <ta e="T310" id="Seg_5113" s="T303">Er verlernte das Sprechen und fing an vor den Menschen wegzulaufen.</ta>
            <ta e="T328" id="Seg_5114" s="T310">Vom Schlag des alten Mannes blieb auf ewige Zeit eine Marke an der Spitze der Ohren des Hasen, die von der Frau gebrochenen Hinterläufe wuchsen zusammen.</ta>
            <ta e="T329" id="Seg_5115" s="T328">Ende.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_5116" s="T0">В старину жили люди, эвенки.</ta>
            <ta e="T13" id="Seg_5117" s="T4">Был у них старейшиной один старик, все слушались его советов, оказывается.</ta>
            <ta e="T17" id="Seg_5118" s="T13">Он советует в каком месте постоянно жить.</ta>
            <ta e="T24" id="Seg_5119" s="T17">"Мы остановимся на таком месте, где растет лес и тепло!", — говорит.</ta>
            <ta e="T31" id="Seg_5120" s="T24">Туда переехав, поселились, поставили себе юрты, некоторые — чумы-голома.</ta>
            <ta e="T35" id="Seg_5121" s="T31">Три соседа стали жить вместе.</ta>
            <ta e="T43" id="Seg_5122" s="T35">К какому-то времени их запасы мяса кончились, стали эти люди голодать.</ta>
            <ta e="T51" id="Seg_5123" s="T43">Съесть оленей — оленей наперечет, всего четыре-пять голов.</ta>
            <ta e="T53" id="Seg_5124" s="T51">Старик советует: </ta>
            <ta e="T58" id="Seg_5125" s="T53">"Если съедим оленей, станем последними нищими.</ta>
            <ta e="T60" id="Seg_5126" s="T58">Не будем есть оленей.</ta>
            <ta e="T61" id="Seg_5127" s="T60">Советую:</ta>
            <ta e="T67" id="Seg_5128" s="T61">По этой речке живут наши соседи зайцы.</ta>
            <ta e="T69" id="Seg_5129" s="T67">У них там свой шаман.</ta>
            <ta e="T74" id="Seg_5130" s="T69">Мы обманом приведем их сюда.</ta>
            <ta e="T77" id="Seg_5131" s="T74">Я больным притворюсь.</ta>
            <ta e="T79" id="Seg_5132" s="T77">Пригласите их шамана.</ta>
            <ta e="T84" id="Seg_5133" s="T79">С ним придет много зайцев.</ta>
            <ta e="T90" id="Seg_5134" s="T84">Мы их побьем и съедим.</ta>
            <ta e="T99" id="Seg_5135" s="T90">Тогда только перебьемся, доживем до теплых дней и будем кормиться охотой."</ta>
            <ta e="T103" id="Seg_5136" s="T99">Люди пошли искать зайцев.</ta>
            <ta e="T110" id="Seg_5137" s="T103">Старик больным притворился, не снимая обуви, улегся в постель.</ta>
            <ta e="T112" id="Seg_5138" s="T110">Старухе говорит: </ta>
            <ta e="T121" id="Seg_5139" s="T112">"Когда придет и станет камлать заячий шаман, заткни окно и дымоход, заложи дверь."</ta>
            <ta e="T125" id="Seg_5140" s="T121">Со стоном, будто в бреду: "Ыы-ыы-ы! </ta>
            <ta e="T132" id="Seg_5141" s="T125">Олдоонбо!", скажу, тогда мне подашь олдоон. </ta>
            <ta e="T148" id="Seg_5142" s="T132">"Ы-ы! Годорообо!", — скажу, тогда ты схватишь гэдэрээн и станешь у дверей. Тут же я вскочу и начну их бить, первым шамана уложить попытаюсь.</ta>
            <ta e="T151" id="Seg_5143" s="T148">Ты тоже не отставай."</ta>
            <ta e="T157" id="Seg_5144" s="T151">Старик ложится, словно в бреду.</ta>
            <ta e="T165" id="Seg_5145" s="T157">Заячий шаман приходит, с ним много зайцев.</ta>
            <ta e="T170" id="Seg_5146" s="T165">В балагане полным-полно зайцев стало.</ta>
            <ta e="T176" id="Seg_5147" s="T170">Старуха заткнула окно и дымоход, заложила дверь.</ta>
            <ta e="T181" id="Seg_5148" s="T176">Заячий шаман начал камлать, вскоре запрыгал-заплясал.</ta>
            <ta e="T185" id="Seg_5149" s="T181">Старик притворяется, будто ему хуже, бредит. "Ы-ы!</ta>
            <ta e="T187" id="Seg_5150" s="T185">Олдоонбо!", сказал.</ta>
            <ta e="T193" id="Seg_5151" s="T187">Старуха вскочила и подала старику олдоон.</ta>
            <ta e="T194" id="Seg_5152" s="T193">"Ы-ы!</ta>
            <ta e="T196" id="Seg_5153" s="T194">Годорообо!", сказал.</ta>
            <ta e="T197" id="Seg_5154" s="T196">"Ы-ы!</ta>
            <ta e="T198" id="Seg_5155" s="T197">Сукачаанма!"</ta>
            <ta e="T206" id="Seg_5156" s="T198">Как только это сказал, старуха схватила гэдэрээн, а сосед — топорик.</ta>
            <ta e="T209" id="Seg_5157" s="T206">А шаман все пляшет: </ta>
            <ta e="T213" id="Seg_5158" s="T209">"Эдук дьуудук юдьоонгоо бимии!</ta>
            <ta e="T215" id="Seg_5159" s="T213">Паапальдьиэк!", говоря.</ta>
            <ta e="T225" id="Seg_5160" s="T215">"Удастся ли выскочить из этого чума!" это значит по-эвенкийски. </ta>
            <ta e="T233" id="Seg_5161" s="T225">Старик, это услышав, вскочил, а шаман прыгнул на шест над очагом.</ta>
            <ta e="T246" id="Seg_5162" s="T233">Старик крюком ударил так, чтоб пришлепнуть его к шесту над очагом, заяц-шаман увернулся — задел наискось только уши.</ta>
            <ta e="T258" id="Seg_5163" s="T246">Когда заячий шаман прыгнул на кучу дров [у печки], старуха кожемялкой ударила по задним лапам.</ta>
            <ta e="T262" id="Seg_5164" s="T258">И переломила две задние лапы.</ta>
            <ta e="T274" id="Seg_5165" s="T262">Заячий шаман с кучи дров выпрыгнул в дымоход, за ним старуха заячья тоже выпрыгнула в дымоход.</ta>
            <ta e="T281" id="Seg_5166" s="T274">Оставшихся в чуме зайцев всех перебили.</ta>
            <ta e="T287" id="Seg_5167" s="T281">Ими питаясь, дожили до теплых весенних дней.</ta>
            <ta e="T291" id="Seg_5168" s="T287">Раньше, говорят, зайцев не ели,</ta>
            <ta e="T297" id="Seg_5169" s="T291">С тех только пор люди стали есть зайцев.</ta>
            <ta e="T303" id="Seg_5170" s="T297">От спасшегося заячьего шамана расплодился дикий заяц.</ta>
            <ta e="T310" id="Seg_5171" s="T303">Только разучился разговаривать и стал избегать людей.</ta>
            <ta e="T328" id="Seg_5172" s="T310">От удара старика на вечные времена осталась метка на кончике ушей зайца; перебитые задние лапы так и срослись в отдельный сустав.</ta>
            <ta e="T329" id="Seg_5173" s="T328">Конец.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T4" id="Seg_5174" s="T0">В старину жили люди, эвенки.</ta>
            <ta e="T13" id="Seg_5175" s="T4">Был у них старейшиной один старик, все слушались его советов, оказывается.</ta>
            <ta e="T17" id="Seg_5176" s="T13">Он указывал, где раскинуть стойбище.</ta>
            <ta e="T24" id="Seg_5177" s="T17">— Мы остановимся на таком месте, где растет лес и тепло! — говорит.</ta>
            <ta e="T31" id="Seg_5178" s="T24">Туда переехав, поселились, поставили себе балаганы, некоторые — чумы-голома.</ta>
            <ta e="T35" id="Seg_5179" s="T31">Три семьи стали жить вместе.</ta>
            <ta e="T43" id="Seg_5180" s="T35">К какому-то времени запасы мяса кончились, стали эти люди голодать.</ta>
            <ta e="T51" id="Seg_5181" s="T43">Забить оленей — оленей наперечет, всего четыре-пять голов.</ta>
            <ta e="T53" id="Seg_5182" s="T51">Старик советует: </ta>
            <ta e="T58" id="Seg_5183" s="T53">— Если забьем оленей, станем последними нищими.</ta>
            <ta e="T60" id="Seg_5184" s="T58">Не будем забивать оленей.</ta>
            <ta e="T61" id="Seg_5185" s="T60">Советую:</ta>
            <ta e="T67" id="Seg_5186" s="T61">по этой речке живут наши соседи зайцы.</ta>
            <ta e="T69" id="Seg_5187" s="T67">У них свой шаман.</ta>
            <ta e="T74" id="Seg_5188" s="T69">Мы обманом приведем их сюда.</ta>
            <ta e="T77" id="Seg_5189" s="T74">Я больным притворюсь.</ta>
            <ta e="T79" id="Seg_5190" s="T77">Пригласите их шамана.</ta>
            <ta e="T84" id="Seg_5191" s="T79">С ним придет много зайцев.</ta>
            <ta e="T90" id="Seg_5192" s="T84">Мы их побьем и съедим.</ta>
            <ta e="T99" id="Seg_5193" s="T90">Тогда только перебьемся, доживем до теплых дней и будем кормиться охотой.</ta>
            <ta e="T103" id="Seg_5194" s="T99">Люди пошли искать зайцев.</ta>
            <ta e="T110" id="Seg_5195" s="T103">Старик больным притворился, не снимая обуви, улегся в постель.</ta>
            <ta e="T112" id="Seg_5196" s="T110">Старухе говорит: </ta>
            <ta e="T121" id="Seg_5197" s="T112">— Когда придет и станет камлать заячий шаман, заткни окна и дымоход, заложи дверь.</ta>
            <ta e="T125" id="Seg_5198" s="T121">Со стоном, будто в бреду: "Ыы-ыы-ы, </ta>
            <ta e="T132" id="Seg_5199" s="T125">Олдоонбо!" — скажу, тогда мне подашь олдоон. </ta>
            <ta e="T148" id="Seg_5200" s="T132">"Ы-ы! Годорообо!" — скажу, тогда ты схватишь гэдэрээн и станешь у дверей. Тут же я вскочу и начну их бить, первым шамана уложить попытаюсь.</ta>
            <ta e="T151" id="Seg_5201" s="T148">Ты тоже не отставай.</ta>
            <ta e="T157" id="Seg_5202" s="T151">Старик ложится, словно в бреду.</ta>
            <ta e="T165" id="Seg_5203" s="T157">Заячий шаман приходит, с ним много зайцев.</ta>
            <ta e="T170" id="Seg_5204" s="T165">В балагане полным-полно зайцев стало.</ta>
            <ta e="T176" id="Seg_5205" s="T170">Старуха заткнула окна и дымоход, заложила дверь.</ta>
            <ta e="T181" id="Seg_5206" s="T176">Заячий шаман начал камлать, вскоре запрыгал-заплясал.</ta>
            <ta e="T185" id="Seg_5207" s="T181">Старик притворяется, будто ему хуже, бредит. "Ы-ы!</ta>
            <ta e="T187" id="Seg_5208" s="T185">Олдоонбо!" — сказал.</ta>
            <ta e="T193" id="Seg_5209" s="T187">Старуха вскочила и сунула старику олдоон.</ta>
            <ta e="T194" id="Seg_5210" s="T193">"Ы-ы!</ta>
            <ta e="T196" id="Seg_5211" s="T194">Годорообо! — сказал.</ta>
            <ta e="T197" id="Seg_5212" s="T196">— Ы-ы!</ta>
            <ta e="T198" id="Seg_5213" s="T197">Сукачаанма!"</ta>
            <ta e="T206" id="Seg_5214" s="T198">Как только это сказал, старуха схватила гэдэрээн, а сосед — топорик.</ta>
            <ta e="T209" id="Seg_5215" s="T206">А шаман все пляшет: </ta>
            <ta e="T213" id="Seg_5216" s="T209">— Эдук дьуудук юдьоонгоо бимии!</ta>
            <ta e="T215" id="Seg_5217" s="T213">Паапальдьиэк! — говоря.</ta>
            <ta e="T225" id="Seg_5218" s="T215">"Удастся ли выскочить из этого чума!" по-эвенкийски значит. </ta>
            <ta e="T233" id="Seg_5219" s="T225">Старик, это услышав, вскочил, а шаман прыгнул на юкээптюю.</ta>
            <ta e="T246" id="Seg_5220" s="T233">Старик олдооном ударил так, чтоб пришлепнуть его к юкээптююну, заяц-шаман увернулся — задел наискось только уши.</ta>
            <ta e="T258" id="Seg_5221" s="T246">Когда заячий шаман прыгнул на кучу дров [у печки], старуха гэдэрээном ударила по задним лапам.</ta>
            <ta e="T262" id="Seg_5222" s="T258">И переломила две задние лапы.</ta>
            <ta e="T274" id="Seg_5223" s="T262">Заячий шаман с кучи дров выпрыгнул в дымоход, за ним старуха заячья тоже выпрыгнула в дымоход.</ta>
            <ta e="T281" id="Seg_5224" s="T274">Оставшихся в чуме зайцев всех перебили.</ta>
            <ta e="T287" id="Seg_5225" s="T281">Ими питаясь, дожили до теплых весенних дней.</ta>
            <ta e="T291" id="Seg_5226" s="T287">Раньше, говорят, зайцев не ели,</ta>
            <ta e="T297" id="Seg_5227" s="T291">С тех только пор люди стали есть зайцев.</ta>
            <ta e="T303" id="Seg_5228" s="T297">От спасшегося заячьего шамана расплодился дикий заяц.</ta>
            <ta e="T310" id="Seg_5229" s="T303">Только разучился разговаривать и стал избегать людей.</ta>
            <ta e="T328" id="Seg_5230" s="T310">От удара старика на вечные времена осталась черная мета на кончике ушей зайца; перебитые задние лапы так и срослись в отдельный сустав.</ta>
            <ta e="T329" id="Seg_5231" s="T328">Конец.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T13" id="Seg_5232" s="T4">[DCh]: "hübe- bɨspat" 'not to cut advise' means apparently "follow advise"</ta>
            <ta e="T58" id="Seg_5233" s="T53">[DCh]: "bu͡or dʼadaŋɨ" 'earth-poor' means apparently 'very poor'.</ta>
            <ta e="T60" id="Seg_5234" s="T58">[DCh]: literally 'let's not eat a/one reindeer'</ta>
            <ta e="T132" id="Seg_5235" s="T125">[DCh]: "oldoːnbo" is Evenki; it is ACC.SG of "oldoːn", which means 'hook for hanging up the kettle'.</ta>
            <ta e="T148" id="Seg_5236" s="T132">[DCh]: "godoroːbo" is somehow adapted/dolganized Evenki; it is ACC.SG of "godoroː" (actually "kǝdǝrǝː"), which means 'scraper for leather'.</ta>
            <ta e="T157" id="Seg_5237" s="T151">[DCh]: "kihini bilbet bu͡ola-bu͡ola" means literally 'not knowing a human.being' which is apparently a metaphor for 'being delirious'.</ta>
            <ta e="T185" id="Seg_5238" s="T181">[DCh]: "bergeː" is actually used speaking about pain or disease, so it means that the old man's "disease" is getting more intensive.</ta>
            <ta e="T196" id="Seg_5239" s="T194">‎‎[DCh]: "godoroːbo" is somehow adapted/dolganized Evenki; it is ACC.SG of "godoroː" (actually "kǝdǝrǝː"), which means 'scraper for leather'.</ta>
            <ta e="T198" id="Seg_5240" s="T197">[DCh]: "hükečaːn" (actually "sukəčəːn") is Evenki and means 'small axe', "hükečaːnma" is ACC.SG.</ta>
            <ta e="T213" id="Seg_5241" s="T209">[DCh]: This and the following sentence is Evenki, together they mean 'May I succeed to jump out of this tent'.</ta>
            <ta e="T215" id="Seg_5242" s="T213">[DCh]: This and the last sentence is Evenki, together they mean 'May I succeed to jump out of this tent'</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T335" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T332" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T333" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T334" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg"
                          name="gg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top"
                          name="Top"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc"
                          name="Foc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
