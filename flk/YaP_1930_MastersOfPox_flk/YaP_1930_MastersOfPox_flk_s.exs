<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID80766411-664C-0BC7-CB73-D1F69FCB5EDC">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>YaP_1931_MastersOfPox_flk</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\flk\YaP_1930_MastersOfPox_flk\YaP_1930_MastersOfPox_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">137</ud-information>
            <ud-information attribute-name="# HIAT:w">104</ud-information>
            <ud-information attribute-name="# e">104</ud-information>
            <ud-information attribute-name="# HIAT:u">16</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="YaP">
            <abbreviation>YaP</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="4.0" type="appl" />
         <tli id="T1" time="4.5" type="appl" />
         <tli id="T2" time="5.0" type="appl" />
         <tli id="T3" time="5.5" type="appl" />
         <tli id="T4" time="6.0" type="appl" />
         <tli id="T5" time="6.5" type="appl" />
         <tli id="T6" time="7.0" type="appl" />
         <tli id="T7" time="7.5" type="appl" />
         <tli id="T8" time="8.0" type="appl" />
         <tli id="T9" time="8.5" type="appl" />
         <tli id="T10" time="9.0" type="appl" />
         <tli id="T11" time="9.5" type="appl" />
         <tli id="T12" time="10.0" type="appl" />
         <tli id="T13" time="10.5" type="appl" />
         <tli id="T14" time="11.0" type="appl" />
         <tli id="T15" time="11.5" type="appl" />
         <tli id="T16" time="12.0" type="appl" />
         <tli id="T17" time="12.5" type="appl" />
         <tli id="T18" time="13.0" type="appl" />
         <tli id="T19" time="13.5" type="appl" />
         <tli id="T20" time="14.0" type="appl" />
         <tli id="T21" time="14.5" type="appl" />
         <tli id="T22" time="15.0" type="appl" />
         <tli id="T23" time="15.5" type="appl" />
         <tli id="T24" time="16.0" type="appl" />
         <tli id="T25" time="16.5" type="appl" />
         <tli id="T26" time="17.0" type="appl" />
         <tli id="T27" time="17.5" type="appl" />
         <tli id="T28" time="18.0" type="appl" />
         <tli id="T29" time="18.5" type="appl" />
         <tli id="T30" time="19.0" type="appl" />
         <tli id="T31" time="19.5" type="appl" />
         <tli id="T32" time="20.0" type="appl" />
         <tli id="T33" time="20.5" type="appl" />
         <tli id="T34" time="21.0" type="appl" />
         <tli id="T35" time="21.5" type="appl" />
         <tli id="T36" time="22.0" type="appl" />
         <tli id="T37" time="22.5" type="appl" />
         <tli id="T38" time="23.0" type="appl" />
         <tli id="T39" time="23.5" type="appl" />
         <tli id="T40" time="24.0" type="appl" />
         <tli id="T41" time="24.5" type="appl" />
         <tli id="T42" time="25.0" type="appl" />
         <tli id="T43" time="25.5" type="appl" />
         <tli id="T44" time="26.0" type="appl" />
         <tli id="T45" time="26.5" type="appl" />
         <tli id="T46" time="27.0" type="appl" />
         <tli id="T47" time="27.5" type="appl" />
         <tli id="T48" time="28.0" type="appl" />
         <tli id="T49" time="28.5" type="appl" />
         <tli id="T50" time="29.0" type="appl" />
         <tli id="T51" time="29.5" type="appl" />
         <tli id="T52" time="30.0" type="appl" />
         <tli id="T53" time="30.5" type="appl" />
         <tli id="T54" time="31.0" type="appl" />
         <tli id="T55" time="31.5" type="appl" />
         <tli id="T56" time="32.0" type="appl" />
         <tli id="T57" time="32.5" type="appl" />
         <tli id="T58" time="33.0" type="appl" />
         <tli id="T59" time="33.5" type="appl" />
         <tli id="T60" time="34.0" type="appl" />
         <tli id="T61" time="34.5" type="appl" />
         <tli id="T62" time="35.0" type="appl" />
         <tli id="T63" time="35.5" type="appl" />
         <tli id="T64" time="36.0" type="appl" />
         <tli id="T65" time="36.5" type="appl" />
         <tli id="T66" time="37.0" type="appl" />
         <tli id="T67" time="37.5" type="appl" />
         <tli id="T68" time="38.0" type="appl" />
         <tli id="T69" time="38.5" type="appl" />
         <tli id="T70" time="39.0" type="appl" />
         <tli id="T71" time="39.5" type="appl" />
         <tli id="T72" time="40.0" type="appl" />
         <tli id="T73" time="40.5" type="appl" />
         <tli id="T74" time="41.0" type="appl" />
         <tli id="T75" time="41.5" type="appl" />
         <tli id="T76" time="42.0" type="appl" />
         <tli id="T77" time="42.5" type="appl" />
         <tli id="T78" time="43.0" type="appl" />
         <tli id="T79" time="43.5" type="appl" />
         <tli id="T80" time="44.0" type="appl" />
         <tli id="T81" time="44.5" type="appl" />
         <tli id="T82" time="45.0" type="appl" />
         <tli id="T83" time="45.5" type="appl" />
         <tli id="T84" time="46.0" type="appl" />
         <tli id="T85" time="46.5" type="appl" />
         <tli id="T86" time="47.0" type="appl" />
         <tli id="T87" time="47.5" type="appl" />
         <tli id="T88" time="48.0" type="appl" />
         <tli id="T89" time="48.5" type="appl" />
         <tli id="T90" time="49.0" type="appl" />
         <tli id="T91" time="49.5" type="appl" />
         <tli id="T92" time="50.0" type="appl" />
         <tli id="T93" time="50.5" type="appl" />
         <tli id="T94" time="51.0" type="appl" />
         <tli id="T95" time="51.5" type="appl" />
         <tli id="T96" time="52.0" type="appl" />
         <tli id="T97" time="52.5" type="appl" />
         <tli id="T98" time="53.0" type="appl" />
         <tli id="T99" time="53.5" type="appl" />
         <tli id="T100" time="54.0" type="appl" />
         <tli id="T101" time="54.5" type="appl" />
         <tli id="T102" time="55.0" type="appl" />
         <tli id="T103" time="55.5" type="appl" />
         <tli id="T104" time="56.0" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="YaP"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T104" id="Seg_0" n="sc" s="T0">
               <ts e="T9" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Bɨlɨr</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">biːr</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">hogotokkoːn</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">oloror</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">kihi͡eke</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">ikki</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">dʼaktar</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">kiːren</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_28" n="HIAT:w" s="T8">kelbitter</ts>
                  <nts id="Seg_29" n="HIAT:ip">.</nts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T12" id="Seg_32" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">Manɨ</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">köröːt</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_40" n="HIAT:w" s="T11">kihilere</ts>
                  <nts id="Seg_41" n="HIAT:ip">:</nts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_44" n="HIAT:u" s="T12">
                  <nts id="Seg_45" n="HIAT:ip">"</nts>
                  <ts e="T13" id="Seg_47" n="HIAT:w" s="T12">Dʼe</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_50" n="HIAT:w" s="T13">min</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_53" n="HIAT:w" s="T14">tu͡ojarbɨn</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_56" n="HIAT:w" s="T15">körüŋ</ts>
                  <nts id="Seg_57" n="HIAT:ip">"</nts>
                  <nts id="Seg_58" n="HIAT:ip">,</nts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_61" n="HIAT:w" s="T16">di͡en</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_64" n="HIAT:w" s="T17">hirge</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_67" n="HIAT:w" s="T18">ihinen</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_70" n="HIAT:w" s="T19">hɨtaːran</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_73" n="HIAT:w" s="T20">ɨllaːn</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_76" n="HIAT:w" s="T21">barbɨt</ts>
                  <nts id="Seg_77" n="HIAT:ip">,</nts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_80" n="HIAT:w" s="T22">bu</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_83" n="HIAT:w" s="T23">ɨllaːbɨtɨgar</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_86" n="HIAT:w" s="T24">hu͡ol</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_89" n="HIAT:w" s="T25">arɨllɨbɨt</ts>
                  <nts id="Seg_90" n="HIAT:ip">.</nts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T31" id="Seg_93" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_95" n="HIAT:w" s="T26">Manɨ</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_98" n="HIAT:w" s="T27">ɨllaːn</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_101" n="HIAT:w" s="T28">büten</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_104" n="HIAT:w" s="T29">baran</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_107" n="HIAT:w" s="T30">eppit</ts>
                  <nts id="Seg_108" n="HIAT:ip">:</nts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T36" id="Seg_111" n="HIAT:u" s="T31">
                  <nts id="Seg_112" n="HIAT:ip">"</nts>
                  <ts e="T32" id="Seg_114" n="HIAT:w" s="T31">Bu</ts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_117" n="HIAT:w" s="T32">hu͡olunan</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_120" n="HIAT:w" s="T33">barɨŋ</ts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_123" n="HIAT:w" s="T34">ehigi</ts>
                  <nts id="Seg_124" n="HIAT:ip">"</nts>
                  <nts id="Seg_125" n="HIAT:ip">,</nts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_128" n="HIAT:w" s="T35">di͡ebit</ts>
                  <nts id="Seg_129" n="HIAT:ip">.</nts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T41" id="Seg_132" n="HIAT:u" s="T36">
                  <ts e="T37" id="Seg_134" n="HIAT:w" s="T36">Dʼaktallar</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_137" n="HIAT:w" s="T37">ɨjbɨt</ts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_140" n="HIAT:w" s="T38">hu͡olunan</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_143" n="HIAT:w" s="T39">baran</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_146" n="HIAT:w" s="T40">kaːlbɨttar</ts>
                  <nts id="Seg_147" n="HIAT:ip">.</nts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T50" id="Seg_150" n="HIAT:u" s="T41">
                  <ts e="T42" id="Seg_152" n="HIAT:w" s="T41">Kas</ts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_155" n="HIAT:w" s="T42">da</ts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_158" n="HIAT:w" s="T43">konon</ts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_161" n="HIAT:w" s="T44">baran</ts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_164" n="HIAT:w" s="T45">hubu</ts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_167" n="HIAT:w" s="T46">kihi͡eke</ts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_170" n="HIAT:w" s="T47">emi͡e</ts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_173" n="HIAT:w" s="T48">tönnön</ts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_176" n="HIAT:w" s="T49">kelbitter</ts>
                  <nts id="Seg_177" n="HIAT:ip">.</nts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T55" id="Seg_180" n="HIAT:u" s="T50">
                  <nts id="Seg_181" n="HIAT:ip">"</nts>
                  <ts e="T51" id="Seg_183" n="HIAT:w" s="T50">Kaja</ts>
                  <nts id="Seg_184" n="HIAT:ip">,</nts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_187" n="HIAT:w" s="T51">tu͡ok</ts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_190" n="HIAT:w" s="T52">hiriger</ts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_193" n="HIAT:w" s="T53">ɨːta</ts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_196" n="HIAT:w" s="T54">hɨrɨttɨŋ</ts>
                  <nts id="Seg_197" n="HIAT:ip">.</nts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T64" id="Seg_200" n="HIAT:u" s="T55">
                  <ts e="T56" id="Seg_202" n="HIAT:w" s="T55">Bihigi</ts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_205" n="HIAT:w" s="T56">aŋaːrdas</ts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_208" n="HIAT:w" s="T57">külügü</ts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_211" n="HIAT:w" s="T58">ere</ts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_214" n="HIAT:w" s="T59">körömmüt</ts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_217" n="HIAT:w" s="T60">korgujan</ts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_220" n="HIAT:w" s="T61">ölö</ts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_223" n="HIAT:w" s="T62">hɨstɨbɨt</ts>
                  <nts id="Seg_224" n="HIAT:ip">"</nts>
                  <nts id="Seg_225" n="HIAT:ip">,</nts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_228" n="HIAT:w" s="T63">di͡ebitter</ts>
                  <nts id="Seg_229" n="HIAT:ip">.</nts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T70" id="Seg_232" n="HIAT:u" s="T64">
                  <ts e="T65" id="Seg_234" n="HIAT:w" s="T64">Bu</ts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_237" n="HIAT:w" s="T65">oloror</ts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_240" n="HIAT:w" s="T66">kihi</ts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_243" n="HIAT:w" s="T67">üs</ts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_246" n="HIAT:w" s="T68">ɨttaːk</ts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_249" n="HIAT:w" s="T69">ebit</ts>
                  <nts id="Seg_250" n="HIAT:ip">.</nts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T76" id="Seg_253" n="HIAT:u" s="T70">
                  <ts e="T71" id="Seg_255" n="HIAT:w" s="T70">Dʼe</ts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_258" n="HIAT:w" s="T71">bu</ts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_261" n="HIAT:w" s="T72">ɨttarɨn</ts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_264" n="HIAT:w" s="T73">algaːn</ts>
                  <nts id="Seg_265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_267" n="HIAT:w" s="T74">ɨːtalɨːr</ts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_270" n="HIAT:w" s="T75">diː</ts>
                  <nts id="Seg_271" n="HIAT:ip">:</nts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T82" id="Seg_274" n="HIAT:u" s="T76">
                  <nts id="Seg_275" n="HIAT:ip">"</nts>
                  <ts e="T77" id="Seg_277" n="HIAT:w" s="T76">Bu</ts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_280" n="HIAT:w" s="T77">min</ts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_283" n="HIAT:w" s="T78">ehi͡eke</ts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_286" n="HIAT:w" s="T79">bi͡erer</ts>
                  <nts id="Seg_287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_289" n="HIAT:w" s="T80">belegim</ts>
                  <nts id="Seg_290" n="HIAT:ip">"</nts>
                  <nts id="Seg_291" n="HIAT:ip">,</nts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_294" n="HIAT:w" s="T81">diːr</ts>
                  <nts id="Seg_295" n="HIAT:ip">.</nts>
                  <nts id="Seg_296" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T86" id="Seg_298" n="HIAT:u" s="T82">
                  <nts id="Seg_299" n="HIAT:ip">"</nts>
                  <ts e="T83" id="Seg_301" n="HIAT:w" s="T82">Manɨ</ts>
                  <nts id="Seg_302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_304" n="HIAT:w" s="T83">ɨlan</ts>
                  <nts id="Seg_305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_307" n="HIAT:w" s="T84">barɨŋ</ts>
                  <nts id="Seg_308" n="HIAT:ip">"</nts>
                  <nts id="Seg_309" n="HIAT:ip">,</nts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_312" n="HIAT:w" s="T85">diːr</ts>
                  <nts id="Seg_313" n="HIAT:ip">.</nts>
                  <nts id="Seg_314" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T89" id="Seg_316" n="HIAT:u" s="T86">
                  <ts e="T87" id="Seg_318" n="HIAT:w" s="T86">Dʼaktallara</ts>
                  <nts id="Seg_319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_321" n="HIAT:w" s="T87">baran</ts>
                  <nts id="Seg_322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_324" n="HIAT:w" s="T88">kaːlallar</ts>
                  <nts id="Seg_325" n="HIAT:ip">.</nts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T100" id="Seg_328" n="HIAT:u" s="T89">
                  <ts e="T90" id="Seg_330" n="HIAT:w" s="T89">Hogurduk</ts>
                  <nts id="Seg_331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_333" n="HIAT:w" s="T90">bɨlɨr</ts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_336" n="HIAT:w" s="T91">ojuna</ts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_339" n="HIAT:w" s="T92">hu͡ok</ts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_342" n="HIAT:w" s="T93">huturgu</ts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_345" n="HIAT:w" s="T94">kihi</ts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_348" n="HIAT:w" s="T95">bejetin</ts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_351" n="HIAT:w" s="T96">nehili͡egin</ts>
                  <nts id="Seg_352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_354" n="HIAT:w" s="T97">bɨːhaːbɨt</ts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_357" n="HIAT:w" s="T98">ühü</ts>
                  <nts id="Seg_358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_360" n="HIAT:w" s="T99">maːtɨskalartan</ts>
                  <nts id="Seg_361" n="HIAT:ip">.</nts>
                  <nts id="Seg_362" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T104" id="Seg_364" n="HIAT:u" s="T100">
                  <ts e="T101" id="Seg_366" n="HIAT:w" s="T100">Baː</ts>
                  <nts id="Seg_367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_369" n="HIAT:w" s="T101">dʼaktallar</ts>
                  <nts id="Seg_370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_372" n="HIAT:w" s="T102">bu͡ospalar</ts>
                  <nts id="Seg_373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_375" n="HIAT:w" s="T103">ebit</ts>
                  <nts id="Seg_376" n="HIAT:ip">.</nts>
                  <nts id="Seg_377" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T104" id="Seg_378" n="sc" s="T0">
               <ts e="T1" id="Seg_380" n="e" s="T0">Bɨlɨr </ts>
               <ts e="T2" id="Seg_382" n="e" s="T1">biːr </ts>
               <ts e="T3" id="Seg_384" n="e" s="T2">hogotokkoːn </ts>
               <ts e="T4" id="Seg_386" n="e" s="T3">oloror </ts>
               <ts e="T5" id="Seg_388" n="e" s="T4">kihi͡eke </ts>
               <ts e="T6" id="Seg_390" n="e" s="T5">ikki </ts>
               <ts e="T7" id="Seg_392" n="e" s="T6">dʼaktar </ts>
               <ts e="T8" id="Seg_394" n="e" s="T7">kiːren </ts>
               <ts e="T9" id="Seg_396" n="e" s="T8">kelbitter. </ts>
               <ts e="T10" id="Seg_398" n="e" s="T9">Manɨ </ts>
               <ts e="T11" id="Seg_400" n="e" s="T10">köröːt </ts>
               <ts e="T12" id="Seg_402" n="e" s="T11">kihilere: </ts>
               <ts e="T13" id="Seg_404" n="e" s="T12">"Dʼe </ts>
               <ts e="T14" id="Seg_406" n="e" s="T13">min </ts>
               <ts e="T15" id="Seg_408" n="e" s="T14">tu͡ojarbɨn </ts>
               <ts e="T16" id="Seg_410" n="e" s="T15">körüŋ", </ts>
               <ts e="T17" id="Seg_412" n="e" s="T16">di͡en </ts>
               <ts e="T18" id="Seg_414" n="e" s="T17">hirge </ts>
               <ts e="T19" id="Seg_416" n="e" s="T18">ihinen </ts>
               <ts e="T20" id="Seg_418" n="e" s="T19">hɨtaːran </ts>
               <ts e="T21" id="Seg_420" n="e" s="T20">ɨllaːn </ts>
               <ts e="T22" id="Seg_422" n="e" s="T21">barbɨt, </ts>
               <ts e="T23" id="Seg_424" n="e" s="T22">bu </ts>
               <ts e="T24" id="Seg_426" n="e" s="T23">ɨllaːbɨtɨgar </ts>
               <ts e="T25" id="Seg_428" n="e" s="T24">hu͡ol </ts>
               <ts e="T26" id="Seg_430" n="e" s="T25">arɨllɨbɨt. </ts>
               <ts e="T27" id="Seg_432" n="e" s="T26">Manɨ </ts>
               <ts e="T28" id="Seg_434" n="e" s="T27">ɨllaːn </ts>
               <ts e="T29" id="Seg_436" n="e" s="T28">büten </ts>
               <ts e="T30" id="Seg_438" n="e" s="T29">baran </ts>
               <ts e="T31" id="Seg_440" n="e" s="T30">eppit: </ts>
               <ts e="T32" id="Seg_442" n="e" s="T31">"Bu </ts>
               <ts e="T33" id="Seg_444" n="e" s="T32">hu͡olunan </ts>
               <ts e="T34" id="Seg_446" n="e" s="T33">barɨŋ </ts>
               <ts e="T35" id="Seg_448" n="e" s="T34">ehigi", </ts>
               <ts e="T36" id="Seg_450" n="e" s="T35">di͡ebit. </ts>
               <ts e="T37" id="Seg_452" n="e" s="T36">Dʼaktallar </ts>
               <ts e="T38" id="Seg_454" n="e" s="T37">ɨjbɨt </ts>
               <ts e="T39" id="Seg_456" n="e" s="T38">hu͡olunan </ts>
               <ts e="T40" id="Seg_458" n="e" s="T39">baran </ts>
               <ts e="T41" id="Seg_460" n="e" s="T40">kaːlbɨttar. </ts>
               <ts e="T42" id="Seg_462" n="e" s="T41">Kas </ts>
               <ts e="T43" id="Seg_464" n="e" s="T42">da </ts>
               <ts e="T44" id="Seg_466" n="e" s="T43">konon </ts>
               <ts e="T45" id="Seg_468" n="e" s="T44">baran </ts>
               <ts e="T46" id="Seg_470" n="e" s="T45">hubu </ts>
               <ts e="T47" id="Seg_472" n="e" s="T46">kihi͡eke </ts>
               <ts e="T48" id="Seg_474" n="e" s="T47">emi͡e </ts>
               <ts e="T49" id="Seg_476" n="e" s="T48">tönnön </ts>
               <ts e="T50" id="Seg_478" n="e" s="T49">kelbitter. </ts>
               <ts e="T51" id="Seg_480" n="e" s="T50">"Kaja, </ts>
               <ts e="T52" id="Seg_482" n="e" s="T51">tu͡ok </ts>
               <ts e="T53" id="Seg_484" n="e" s="T52">hiriger </ts>
               <ts e="T54" id="Seg_486" n="e" s="T53">ɨːta </ts>
               <ts e="T55" id="Seg_488" n="e" s="T54">hɨrɨttɨŋ. </ts>
               <ts e="T56" id="Seg_490" n="e" s="T55">Bihigi </ts>
               <ts e="T57" id="Seg_492" n="e" s="T56">aŋaːrdas </ts>
               <ts e="T58" id="Seg_494" n="e" s="T57">külügü </ts>
               <ts e="T59" id="Seg_496" n="e" s="T58">ere </ts>
               <ts e="T60" id="Seg_498" n="e" s="T59">körömmüt </ts>
               <ts e="T61" id="Seg_500" n="e" s="T60">korgujan </ts>
               <ts e="T62" id="Seg_502" n="e" s="T61">ölö </ts>
               <ts e="T63" id="Seg_504" n="e" s="T62">hɨstɨbɨt", </ts>
               <ts e="T64" id="Seg_506" n="e" s="T63">di͡ebitter. </ts>
               <ts e="T65" id="Seg_508" n="e" s="T64">Bu </ts>
               <ts e="T66" id="Seg_510" n="e" s="T65">oloror </ts>
               <ts e="T67" id="Seg_512" n="e" s="T66">kihi </ts>
               <ts e="T68" id="Seg_514" n="e" s="T67">üs </ts>
               <ts e="T69" id="Seg_516" n="e" s="T68">ɨttaːk </ts>
               <ts e="T70" id="Seg_518" n="e" s="T69">ebit. </ts>
               <ts e="T71" id="Seg_520" n="e" s="T70">Dʼe </ts>
               <ts e="T72" id="Seg_522" n="e" s="T71">bu </ts>
               <ts e="T73" id="Seg_524" n="e" s="T72">ɨttarɨn </ts>
               <ts e="T74" id="Seg_526" n="e" s="T73">algaːn </ts>
               <ts e="T75" id="Seg_528" n="e" s="T74">ɨːtalɨːr </ts>
               <ts e="T76" id="Seg_530" n="e" s="T75">diː: </ts>
               <ts e="T77" id="Seg_532" n="e" s="T76">"Bu </ts>
               <ts e="T78" id="Seg_534" n="e" s="T77">min </ts>
               <ts e="T79" id="Seg_536" n="e" s="T78">ehi͡eke </ts>
               <ts e="T80" id="Seg_538" n="e" s="T79">bi͡erer </ts>
               <ts e="T81" id="Seg_540" n="e" s="T80">belegim", </ts>
               <ts e="T82" id="Seg_542" n="e" s="T81">diːr. </ts>
               <ts e="T83" id="Seg_544" n="e" s="T82">"Manɨ </ts>
               <ts e="T84" id="Seg_546" n="e" s="T83">ɨlan </ts>
               <ts e="T85" id="Seg_548" n="e" s="T84">barɨŋ", </ts>
               <ts e="T86" id="Seg_550" n="e" s="T85">diːr. </ts>
               <ts e="T87" id="Seg_552" n="e" s="T86">Dʼaktallara </ts>
               <ts e="T88" id="Seg_554" n="e" s="T87">baran </ts>
               <ts e="T89" id="Seg_556" n="e" s="T88">kaːlallar. </ts>
               <ts e="T90" id="Seg_558" n="e" s="T89">Hogurduk </ts>
               <ts e="T91" id="Seg_560" n="e" s="T90">bɨlɨr </ts>
               <ts e="T92" id="Seg_562" n="e" s="T91">ojuna </ts>
               <ts e="T93" id="Seg_564" n="e" s="T92">hu͡ok </ts>
               <ts e="T94" id="Seg_566" n="e" s="T93">huturgu </ts>
               <ts e="T95" id="Seg_568" n="e" s="T94">kihi </ts>
               <ts e="T96" id="Seg_570" n="e" s="T95">bejetin </ts>
               <ts e="T97" id="Seg_572" n="e" s="T96">nehili͡egin </ts>
               <ts e="T98" id="Seg_574" n="e" s="T97">bɨːhaːbɨt </ts>
               <ts e="T99" id="Seg_576" n="e" s="T98">ühü </ts>
               <ts e="T100" id="Seg_578" n="e" s="T99">maːtɨskalartan. </ts>
               <ts e="T101" id="Seg_580" n="e" s="T100">Baː </ts>
               <ts e="T102" id="Seg_582" n="e" s="T101">dʼaktallar </ts>
               <ts e="T103" id="Seg_584" n="e" s="T102">bu͡ospalar </ts>
               <ts e="T104" id="Seg_586" n="e" s="T103">ebit. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T9" id="Seg_587" s="T0">YaP_1930_MastersOfPox_flk.001 (001.001)</ta>
            <ta e="T12" id="Seg_588" s="T9">YaP_1930_MastersOfPox_flk.002 (001.002)</ta>
            <ta e="T26" id="Seg_589" s="T12">YaP_1930_MastersOfPox_flk.003 (001.003)</ta>
            <ta e="T31" id="Seg_590" s="T26">YaP_1930_MastersOfPox_flk.004 (001.004)</ta>
            <ta e="T36" id="Seg_591" s="T31">YaP_1930_MastersOfPox_flk.005 (001.005)</ta>
            <ta e="T41" id="Seg_592" s="T36">YaP_1930_MastersOfPox_flk.006 (001.006)</ta>
            <ta e="T50" id="Seg_593" s="T41">YaP_1930_MastersOfPox_flk.007 (001.007)</ta>
            <ta e="T55" id="Seg_594" s="T50">YaP_1930_MastersOfPox_flk.008 (001.008)</ta>
            <ta e="T64" id="Seg_595" s="T55">YaP_1930_MastersOfPox_flk.009 (001.009)</ta>
            <ta e="T70" id="Seg_596" s="T64">YaP_1930_MastersOfPox_flk.010 (001.010)</ta>
            <ta e="T76" id="Seg_597" s="T70">YaP_1930_MastersOfPox_flk.011 (001.011)</ta>
            <ta e="T82" id="Seg_598" s="T76">YaP_1930_MastersOfPox_flk.012 (001.012)</ta>
            <ta e="T86" id="Seg_599" s="T82">YaP_1930_MastersOfPox_flk.013 (001.013)</ta>
            <ta e="T89" id="Seg_600" s="T86">YaP_1930_MastersOfPox_flk.014 (001.014)</ta>
            <ta e="T100" id="Seg_601" s="T89">YaP_1930_MastersOfPox_flk.015 (001.015)</ta>
            <ta e="T104" id="Seg_602" s="T100">YaP_1930_MastersOfPox_flk.016 (001.016)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T9" id="Seg_603" s="T0">Былыр биир һоготоккоон олорор киһиэкэ икки дьактар киирэн кэлбиттэр.</ta>
            <ta e="T12" id="Seg_604" s="T9">Маны көрөөт киһилэрэ:</ta>
            <ta e="T26" id="Seg_605" s="T12">— Дьэ мин туойарбын көрүҥ, — диэн һиргэ иһинэн һытааран ыллаан барбыт, бу ыллаабытыгар һуол арыллыбыт.</ta>
            <ta e="T31" id="Seg_606" s="T26">Маны ыллаан бүтэн баран эппит:</ta>
            <ta e="T36" id="Seg_607" s="T31">— Бу һуолунан барыҥ эһиги, — диэбит.</ta>
            <ta e="T41" id="Seg_608" s="T36">Дьакталлар ыйбыт һуолунан баран каалбыттар.</ta>
            <ta e="T50" id="Seg_609" s="T41">Кас да конон баран һубу киһиэкэ эмиэ төннөн кэлбиттэр.</ta>
            <ta e="T55" id="Seg_610" s="T50">— Кайа, туок һиригэр ыыта һырыттыҥ.</ta>
            <ta e="T64" id="Seg_611" s="T55">Биһиги аҥаардас күлүгү эрэ көрөммүт коргуйан өлө һыстыбыт, — диэбиттэр.</ta>
            <ta e="T70" id="Seg_612" s="T64">Бу олорор киһи үс ыттаак эбит.</ta>
            <ta e="T76" id="Seg_613" s="T70">Дьэ бу ыттарын алгаан ыыта-лыыр дии:</ta>
            <ta e="T82" id="Seg_614" s="T76">— Бу мин эһиэкэ биэрэр бэлэгим, — диир.</ta>
            <ta e="T86" id="Seg_615" s="T82">— Маны ылан барыҥ, — диир.</ta>
            <ta e="T89" id="Seg_616" s="T86">Дьакталлара баран каалаллар.</ta>
            <ta e="T100" id="Seg_617" s="T89">һогурдук былыр ойуна һуок һутургу киһи бэйэтин нэһилиэгин быыһаабыт үһү маатыскалартан*.</ta>
            <ta e="T104" id="Seg_618" s="T100">Баа дьакталлар буоспалар эбит.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T9" id="Seg_619" s="T0">Bɨlɨr biːr hogotokkoːn oloror kihi͡eke ikki dʼaktar kiːren kelbitter. </ta>
            <ta e="T12" id="Seg_620" s="T9">Manɨ köröːt kihilere: </ta>
            <ta e="T26" id="Seg_621" s="T12">"Dʼe min tu͡ojarbɨn körüŋ", di͡en hirge ihinen hɨtaːran ɨllaːn barbɨt, bu ɨllaːbɨtɨgar hu͡ol arɨllɨbɨt. </ta>
            <ta e="T31" id="Seg_622" s="T26">Manɨ ɨllaːn büten baran eppit:</ta>
            <ta e="T36" id="Seg_623" s="T31">"Bu hu͡olunan barɨŋ ehigi", di͡ebit. </ta>
            <ta e="T41" id="Seg_624" s="T36">Dʼaktallar ɨjbɨt hu͡olunan baran kaːlbɨttar. </ta>
            <ta e="T50" id="Seg_625" s="T41">Kas da konon baran hubu kihi͡eke emi͡e tönnön kelbitter. </ta>
            <ta e="T55" id="Seg_626" s="T50">"Kaja, tu͡ok hiriger ɨːta hɨrɨttɨŋ. </ta>
            <ta e="T64" id="Seg_627" s="T55">Bihigi aŋaːrdas külügü ere körömmüt korgujan ölö hɨstɨbɨt", di͡ebitter. </ta>
            <ta e="T70" id="Seg_628" s="T64">Bu oloror kihi üs ɨttaːk ebit. </ta>
            <ta e="T76" id="Seg_629" s="T70">Dʼe bu ɨttarɨn algaːn ɨːtalɨːr diː: </ta>
            <ta e="T82" id="Seg_630" s="T76">"Bu min ehi͡eke bi͡erer belegim", diːr. </ta>
            <ta e="T86" id="Seg_631" s="T82">"Manɨ ɨlan barɨŋ", diːr. </ta>
            <ta e="T89" id="Seg_632" s="T86">Dʼaktallara baran kaːlallar. </ta>
            <ta e="T100" id="Seg_633" s="T89">Hogurduk bɨlɨr ojuna hu͡ok huturgu kihi bejetin nehili͡egin bɨːhaːbɨt ühü maːtɨskalartan. </ta>
            <ta e="T104" id="Seg_634" s="T100">Baː dʼaktallar bu͡ospalar ebit. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_635" s="T0">bɨlɨr</ta>
            <ta e="T2" id="Seg_636" s="T1">biːr</ta>
            <ta e="T3" id="Seg_637" s="T2">hogotok-koːn</ta>
            <ta e="T4" id="Seg_638" s="T3">olor-or</ta>
            <ta e="T5" id="Seg_639" s="T4">kihi͡e-ke</ta>
            <ta e="T6" id="Seg_640" s="T5">ikki</ta>
            <ta e="T7" id="Seg_641" s="T6">dʼaktar</ta>
            <ta e="T8" id="Seg_642" s="T7">kiːr-en</ta>
            <ta e="T9" id="Seg_643" s="T8">kel-bit-ter</ta>
            <ta e="T10" id="Seg_644" s="T9">ma-nɨ</ta>
            <ta e="T11" id="Seg_645" s="T10">kör-öːt</ta>
            <ta e="T12" id="Seg_646" s="T11">kihi-lere</ta>
            <ta e="T13" id="Seg_647" s="T12">dʼe</ta>
            <ta e="T14" id="Seg_648" s="T13">min</ta>
            <ta e="T15" id="Seg_649" s="T14">tu͡oj-ar-bɨ-n</ta>
            <ta e="T16" id="Seg_650" s="T15">kör-ü-ŋ</ta>
            <ta e="T17" id="Seg_651" s="T16">di͡e-n</ta>
            <ta e="T18" id="Seg_652" s="T17">hir-ge</ta>
            <ta e="T19" id="Seg_653" s="T18">ih-i-nen</ta>
            <ta e="T20" id="Seg_654" s="T19">hɨtaːr-an</ta>
            <ta e="T21" id="Seg_655" s="T20">ɨllaː-n</ta>
            <ta e="T22" id="Seg_656" s="T21">bar-bɨt</ta>
            <ta e="T23" id="Seg_657" s="T22">bu</ta>
            <ta e="T24" id="Seg_658" s="T23">ɨllaː-bɨt-ɨ-gar</ta>
            <ta e="T25" id="Seg_659" s="T24">hu͡ol</ta>
            <ta e="T26" id="Seg_660" s="T25">arɨ-ll-ɨ-bɨt</ta>
            <ta e="T27" id="Seg_661" s="T26">ma-nɨ</ta>
            <ta e="T28" id="Seg_662" s="T27">ɨllaː-n</ta>
            <ta e="T29" id="Seg_663" s="T28">büt-en</ta>
            <ta e="T30" id="Seg_664" s="T29">baran</ta>
            <ta e="T31" id="Seg_665" s="T30">ep-pit</ta>
            <ta e="T32" id="Seg_666" s="T31">bu</ta>
            <ta e="T33" id="Seg_667" s="T32">hu͡ol-u-nan</ta>
            <ta e="T34" id="Seg_668" s="T33">bar-ɨ-ŋ</ta>
            <ta e="T35" id="Seg_669" s="T34">ehigi</ta>
            <ta e="T36" id="Seg_670" s="T35">di͡e-bit</ta>
            <ta e="T37" id="Seg_671" s="T36">dʼaktal-lar</ta>
            <ta e="T38" id="Seg_672" s="T37">ɨj-bɨt</ta>
            <ta e="T39" id="Seg_673" s="T38">hu͡ol-u-nan</ta>
            <ta e="T40" id="Seg_674" s="T39">bar-an</ta>
            <ta e="T41" id="Seg_675" s="T40">kaːl-bɨt-tar</ta>
            <ta e="T42" id="Seg_676" s="T41">kas</ta>
            <ta e="T43" id="Seg_677" s="T42">da</ta>
            <ta e="T44" id="Seg_678" s="T43">kon-on</ta>
            <ta e="T45" id="Seg_679" s="T44">baran</ta>
            <ta e="T46" id="Seg_680" s="T45">hubu</ta>
            <ta e="T47" id="Seg_681" s="T46">kihi͡e-ke</ta>
            <ta e="T48" id="Seg_682" s="T47">emi͡e</ta>
            <ta e="T49" id="Seg_683" s="T48">tönn-ön</ta>
            <ta e="T50" id="Seg_684" s="T49">kel-bit-ter</ta>
            <ta e="T51" id="Seg_685" s="T50">kaja</ta>
            <ta e="T52" id="Seg_686" s="T51">tu͡ok</ta>
            <ta e="T53" id="Seg_687" s="T52">hir-i-ger</ta>
            <ta e="T54" id="Seg_688" s="T53">ɨːt-a</ta>
            <ta e="T55" id="Seg_689" s="T54">hɨrɨt-tɨ-ŋ</ta>
            <ta e="T56" id="Seg_690" s="T55">bihigi</ta>
            <ta e="T57" id="Seg_691" s="T56">aŋaːrdas</ta>
            <ta e="T58" id="Seg_692" s="T57">külüg-ü</ta>
            <ta e="T59" id="Seg_693" s="T58">ere</ta>
            <ta e="T60" id="Seg_694" s="T59">kör-öm-müt</ta>
            <ta e="T61" id="Seg_695" s="T60">korguj-an</ta>
            <ta e="T62" id="Seg_696" s="T61">öl-ö</ta>
            <ta e="T63" id="Seg_697" s="T62">hɨst-ɨ-bɨt</ta>
            <ta e="T64" id="Seg_698" s="T63">di͡e-bit-ter</ta>
            <ta e="T65" id="Seg_699" s="T64">bu</ta>
            <ta e="T66" id="Seg_700" s="T65">olor-or</ta>
            <ta e="T67" id="Seg_701" s="T66">kihi</ta>
            <ta e="T68" id="Seg_702" s="T67">üs</ta>
            <ta e="T69" id="Seg_703" s="T68">ɨt-taːk</ta>
            <ta e="T70" id="Seg_704" s="T69">e-bit</ta>
            <ta e="T71" id="Seg_705" s="T70">dʼe</ta>
            <ta e="T72" id="Seg_706" s="T71">bu</ta>
            <ta e="T73" id="Seg_707" s="T72">ɨt-tar-ɨ-n</ta>
            <ta e="T74" id="Seg_708" s="T73">algaː-n</ta>
            <ta e="T75" id="Seg_709" s="T74">ɨːt-alɨː-r</ta>
            <ta e="T76" id="Seg_710" s="T75">diː</ta>
            <ta e="T77" id="Seg_711" s="T76">bu</ta>
            <ta e="T78" id="Seg_712" s="T77">min</ta>
            <ta e="T79" id="Seg_713" s="T78">ehi͡e-ke</ta>
            <ta e="T80" id="Seg_714" s="T79">bi͡er-er</ta>
            <ta e="T81" id="Seg_715" s="T80">beleg-i-m</ta>
            <ta e="T82" id="Seg_716" s="T81">diː-r</ta>
            <ta e="T83" id="Seg_717" s="T82">ma-nɨ</ta>
            <ta e="T84" id="Seg_718" s="T83">ɨl-an</ta>
            <ta e="T85" id="Seg_719" s="T84">bar-ɨ-ŋ</ta>
            <ta e="T86" id="Seg_720" s="T85">diː-r</ta>
            <ta e="T87" id="Seg_721" s="T86">dʼaktal-lara</ta>
            <ta e="T88" id="Seg_722" s="T87">bar-an</ta>
            <ta e="T89" id="Seg_723" s="T88">kaːl-al-lar</ta>
            <ta e="T90" id="Seg_724" s="T89">hogurduk</ta>
            <ta e="T91" id="Seg_725" s="T90">bɨlɨr</ta>
            <ta e="T92" id="Seg_726" s="T91">ojun-a</ta>
            <ta e="T93" id="Seg_727" s="T92">hu͡ok</ta>
            <ta e="T94" id="Seg_728" s="T93">huturgu</ta>
            <ta e="T95" id="Seg_729" s="T94">kihi</ta>
            <ta e="T96" id="Seg_730" s="T95">beje-ti-n</ta>
            <ta e="T97" id="Seg_731" s="T96">nehili͡eg-i-n</ta>
            <ta e="T98" id="Seg_732" s="T97">bɨːhaː-bɨt</ta>
            <ta e="T99" id="Seg_733" s="T98">ühü</ta>
            <ta e="T100" id="Seg_734" s="T99">maːtɨska-lar-tan</ta>
            <ta e="T101" id="Seg_735" s="T100">baː</ta>
            <ta e="T102" id="Seg_736" s="T101">dʼaktal-lar</ta>
            <ta e="T103" id="Seg_737" s="T102">bu͡ospa-lar</ta>
            <ta e="T104" id="Seg_738" s="T103">e-bit</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_739" s="T0">bɨlɨr</ta>
            <ta e="T2" id="Seg_740" s="T1">biːr</ta>
            <ta e="T3" id="Seg_741" s="T2">čogotok-kAːN</ta>
            <ta e="T4" id="Seg_742" s="T3">olor-Ar</ta>
            <ta e="T5" id="Seg_743" s="T4">kihi-GA</ta>
            <ta e="T6" id="Seg_744" s="T5">ikki</ta>
            <ta e="T7" id="Seg_745" s="T6">dʼaktar</ta>
            <ta e="T8" id="Seg_746" s="T7">kiːr-An</ta>
            <ta e="T9" id="Seg_747" s="T8">kel-BIT-LAr</ta>
            <ta e="T10" id="Seg_748" s="T9">bu-nI</ta>
            <ta e="T11" id="Seg_749" s="T10">kör-AːT</ta>
            <ta e="T12" id="Seg_750" s="T11">kihi-LArA</ta>
            <ta e="T13" id="Seg_751" s="T12">dʼe</ta>
            <ta e="T14" id="Seg_752" s="T13">min</ta>
            <ta e="T15" id="Seg_753" s="T14">tu͡oj-Ar-BI-n</ta>
            <ta e="T16" id="Seg_754" s="T15">kör-I-ŋ</ta>
            <ta e="T17" id="Seg_755" s="T16">di͡e-An</ta>
            <ta e="T18" id="Seg_756" s="T17">hir-GA</ta>
            <ta e="T19" id="Seg_757" s="T18">is-tI-nAn</ta>
            <ta e="T20" id="Seg_758" s="T19">hɨtaːr-An</ta>
            <ta e="T21" id="Seg_759" s="T20">ɨllaː-An</ta>
            <ta e="T22" id="Seg_760" s="T21">bar-BIT</ta>
            <ta e="T23" id="Seg_761" s="T22">bu</ta>
            <ta e="T24" id="Seg_762" s="T23">ɨllaː-BIT-tI-GAr</ta>
            <ta e="T25" id="Seg_763" s="T24">hu͡ol</ta>
            <ta e="T26" id="Seg_764" s="T25">arɨj-LIN-I-BIT</ta>
            <ta e="T27" id="Seg_765" s="T26">bu-nI</ta>
            <ta e="T28" id="Seg_766" s="T27">ɨllaː-An</ta>
            <ta e="T29" id="Seg_767" s="T28">büt-An</ta>
            <ta e="T30" id="Seg_768" s="T29">baran</ta>
            <ta e="T31" id="Seg_769" s="T30">et-BIT</ta>
            <ta e="T32" id="Seg_770" s="T31">bu</ta>
            <ta e="T33" id="Seg_771" s="T32">hu͡ol-tI-nAn</ta>
            <ta e="T34" id="Seg_772" s="T33">bar-I-ŋ</ta>
            <ta e="T35" id="Seg_773" s="T34">ehigi</ta>
            <ta e="T36" id="Seg_774" s="T35">di͡e-BIT</ta>
            <ta e="T37" id="Seg_775" s="T36">dʼaktar-LAr</ta>
            <ta e="T38" id="Seg_776" s="T37">ɨj-BIT</ta>
            <ta e="T39" id="Seg_777" s="T38">hu͡ol-tI-nAn</ta>
            <ta e="T40" id="Seg_778" s="T39">bar-An</ta>
            <ta e="T41" id="Seg_779" s="T40">kaːl-BIT-LAr</ta>
            <ta e="T42" id="Seg_780" s="T41">kas</ta>
            <ta e="T43" id="Seg_781" s="T42">da</ta>
            <ta e="T44" id="Seg_782" s="T43">kon-An</ta>
            <ta e="T45" id="Seg_783" s="T44">baran</ta>
            <ta e="T46" id="Seg_784" s="T45">hubu</ta>
            <ta e="T47" id="Seg_785" s="T46">kihi-GA</ta>
            <ta e="T48" id="Seg_786" s="T47">emi͡e</ta>
            <ta e="T49" id="Seg_787" s="T48">tönün-An</ta>
            <ta e="T50" id="Seg_788" s="T49">kel-BIT-LAr</ta>
            <ta e="T51" id="Seg_789" s="T50">kaja</ta>
            <ta e="T52" id="Seg_790" s="T51">tu͡ok</ta>
            <ta e="T53" id="Seg_791" s="T52">hir-tI-GAr</ta>
            <ta e="T54" id="Seg_792" s="T53">ɨːt-A</ta>
            <ta e="T55" id="Seg_793" s="T54">hɨrɨt-TI-ŋ</ta>
            <ta e="T56" id="Seg_794" s="T55">bihigi</ta>
            <ta e="T57" id="Seg_795" s="T56">aŋaːrdas</ta>
            <ta e="T58" id="Seg_796" s="T57">külük-nI</ta>
            <ta e="T59" id="Seg_797" s="T58">ere</ta>
            <ta e="T60" id="Seg_798" s="T59">kör-An-BIt</ta>
            <ta e="T61" id="Seg_799" s="T60">korguj-An</ta>
            <ta e="T62" id="Seg_800" s="T61">öl-A</ta>
            <ta e="T63" id="Seg_801" s="T62">hɨhɨn-I-BIT</ta>
            <ta e="T64" id="Seg_802" s="T63">di͡e-BIT-LAr</ta>
            <ta e="T65" id="Seg_803" s="T64">bu</ta>
            <ta e="T66" id="Seg_804" s="T65">olor-Ar</ta>
            <ta e="T67" id="Seg_805" s="T66">kihi</ta>
            <ta e="T68" id="Seg_806" s="T67">üs</ta>
            <ta e="T69" id="Seg_807" s="T68">ɨt-LAːK</ta>
            <ta e="T70" id="Seg_808" s="T69">e-BIT</ta>
            <ta e="T71" id="Seg_809" s="T70">dʼe</ta>
            <ta e="T72" id="Seg_810" s="T71">bu</ta>
            <ta e="T73" id="Seg_811" s="T72">ɨt-LAr-tI-n</ta>
            <ta e="T74" id="Seg_812" s="T73">algaː-An</ta>
            <ta e="T75" id="Seg_813" s="T74">ɨːt-AlAː-Ar</ta>
            <ta e="T76" id="Seg_814" s="T75">diː</ta>
            <ta e="T77" id="Seg_815" s="T76">bu</ta>
            <ta e="T78" id="Seg_816" s="T77">min</ta>
            <ta e="T79" id="Seg_817" s="T78">ehigi-GA</ta>
            <ta e="T80" id="Seg_818" s="T79">bi͡er-Ar</ta>
            <ta e="T81" id="Seg_819" s="T80">belek-I-m</ta>
            <ta e="T82" id="Seg_820" s="T81">di͡e-Ar</ta>
            <ta e="T83" id="Seg_821" s="T82">bu-nI</ta>
            <ta e="T84" id="Seg_822" s="T83">ɨl-An</ta>
            <ta e="T85" id="Seg_823" s="T84">bar-I-ŋ</ta>
            <ta e="T86" id="Seg_824" s="T85">di͡e-Ar</ta>
            <ta e="T87" id="Seg_825" s="T86">dʼaktar-LArA</ta>
            <ta e="T88" id="Seg_826" s="T87">bar-An</ta>
            <ta e="T89" id="Seg_827" s="T88">kaːl-Ar-LAr</ta>
            <ta e="T90" id="Seg_828" s="T89">hogurduk</ta>
            <ta e="T91" id="Seg_829" s="T90">bɨlɨr</ta>
            <ta e="T92" id="Seg_830" s="T91">ojun-tA</ta>
            <ta e="T93" id="Seg_831" s="T92">hu͡ok</ta>
            <ta e="T94" id="Seg_832" s="T93">huturgu</ta>
            <ta e="T95" id="Seg_833" s="T94">kihi</ta>
            <ta e="T96" id="Seg_834" s="T95">beje-tI-n</ta>
            <ta e="T97" id="Seg_835" s="T96">nehili͡ek-tI-n</ta>
            <ta e="T98" id="Seg_836" s="T97">bɨːhaː-BIT</ta>
            <ta e="T99" id="Seg_837" s="T98">ühü</ta>
            <ta e="T100" id="Seg_838" s="T99">maːtɨska-LAr-ttAn</ta>
            <ta e="T101" id="Seg_839" s="T100">bu</ta>
            <ta e="T102" id="Seg_840" s="T101">dʼaktar-LAr</ta>
            <ta e="T103" id="Seg_841" s="T102">bu͡ospa-LAr</ta>
            <ta e="T104" id="Seg_842" s="T103">e-BIT</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_843" s="T0">long.ago</ta>
            <ta e="T2" id="Seg_844" s="T1">one</ta>
            <ta e="T3" id="Seg_845" s="T2">alone-INTNS</ta>
            <ta e="T4" id="Seg_846" s="T3">live-PTCP.PRS</ta>
            <ta e="T5" id="Seg_847" s="T4">human.being-DAT/LOC</ta>
            <ta e="T6" id="Seg_848" s="T5">two</ta>
            <ta e="T7" id="Seg_849" s="T6">woman.[NOM]</ta>
            <ta e="T8" id="Seg_850" s="T7">go.in-CVB.SEQ</ta>
            <ta e="T9" id="Seg_851" s="T8">come-PST2-3PL</ta>
            <ta e="T10" id="Seg_852" s="T9">this-ACC</ta>
            <ta e="T11" id="Seg_853" s="T10">see-CVB.ANT</ta>
            <ta e="T12" id="Seg_854" s="T11">human.being-3PL.[NOM]</ta>
            <ta e="T13" id="Seg_855" s="T12">well</ta>
            <ta e="T14" id="Seg_856" s="T13">1SG.[NOM]</ta>
            <ta e="T15" id="Seg_857" s="T14">sing-PTCP.PRS-1SG-ACC</ta>
            <ta e="T16" id="Seg_858" s="T15">see-EP-IMP.2PL</ta>
            <ta e="T17" id="Seg_859" s="T16">say-CVB.SEQ</ta>
            <ta e="T18" id="Seg_860" s="T17">earth-DAT/LOC</ta>
            <ta e="T19" id="Seg_861" s="T18">inside-3SG-INSTR</ta>
            <ta e="T20" id="Seg_862" s="T19">lay-CVB.SEQ</ta>
            <ta e="T21" id="Seg_863" s="T20">sing-CVB.SEQ</ta>
            <ta e="T22" id="Seg_864" s="T21">go-PST2.[3SG]</ta>
            <ta e="T23" id="Seg_865" s="T22">this</ta>
            <ta e="T24" id="Seg_866" s="T23">sing-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T25" id="Seg_867" s="T24">way.[NOM]</ta>
            <ta e="T26" id="Seg_868" s="T25">open-PASS/REFL-EP-PST2.[3SG]</ta>
            <ta e="T27" id="Seg_869" s="T26">this-ACC</ta>
            <ta e="T28" id="Seg_870" s="T27">sing-CVB.SEQ</ta>
            <ta e="T29" id="Seg_871" s="T28">stop-CVB.SEQ</ta>
            <ta e="T30" id="Seg_872" s="T29">after</ta>
            <ta e="T31" id="Seg_873" s="T30">speak-PST2.[3SG]</ta>
            <ta e="T32" id="Seg_874" s="T31">this</ta>
            <ta e="T33" id="Seg_875" s="T32">way-3SG-INSTR</ta>
            <ta e="T34" id="Seg_876" s="T33">go-EP-IMP.2PL</ta>
            <ta e="T35" id="Seg_877" s="T34">2PL.[NOM]</ta>
            <ta e="T36" id="Seg_878" s="T35">say-PST2.[3SG]</ta>
            <ta e="T37" id="Seg_879" s="T36">woman-PL.[NOM]</ta>
            <ta e="T38" id="Seg_880" s="T37">show-PTCP.PST</ta>
            <ta e="T39" id="Seg_881" s="T38">way-3SG-INSTR</ta>
            <ta e="T40" id="Seg_882" s="T39">go-CVB.SEQ</ta>
            <ta e="T41" id="Seg_883" s="T40">stay-PST2-3PL</ta>
            <ta e="T42" id="Seg_884" s="T41">how.much</ta>
            <ta e="T43" id="Seg_885" s="T42">INDEF</ta>
            <ta e="T44" id="Seg_886" s="T43">overnight-CVB.SEQ</ta>
            <ta e="T45" id="Seg_887" s="T44">after</ta>
            <ta e="T46" id="Seg_888" s="T45">this.EMPH</ta>
            <ta e="T47" id="Seg_889" s="T46">human.being-DAT/LOC</ta>
            <ta e="T48" id="Seg_890" s="T47">again</ta>
            <ta e="T49" id="Seg_891" s="T48">come.back-CVB.SEQ</ta>
            <ta e="T50" id="Seg_892" s="T49">come-PST2-3PL</ta>
            <ta e="T51" id="Seg_893" s="T50">well</ta>
            <ta e="T52" id="Seg_894" s="T51">what.[NOM]</ta>
            <ta e="T53" id="Seg_895" s="T52">place-3SG-DAT/LOC</ta>
            <ta e="T54" id="Seg_896" s="T53">send-CVB.SIM</ta>
            <ta e="T55" id="Seg_897" s="T54">go-PST1-2SG</ta>
            <ta e="T56" id="Seg_898" s="T55">1PL.[NOM]</ta>
            <ta e="T57" id="Seg_899" s="T56">one_sided</ta>
            <ta e="T58" id="Seg_900" s="T57">shadow-ACC</ta>
            <ta e="T59" id="Seg_901" s="T58">just</ta>
            <ta e="T60" id="Seg_902" s="T59">see-CVB.SEQ-1PL</ta>
            <ta e="T61" id="Seg_903" s="T60">hunger-CVB.SEQ</ta>
            <ta e="T62" id="Seg_904" s="T61">die-CVB.SIM</ta>
            <ta e="T63" id="Seg_905" s="T62">stick-EP-PST2.[3SG]</ta>
            <ta e="T64" id="Seg_906" s="T63">say-PST2-3PL</ta>
            <ta e="T65" id="Seg_907" s="T64">this</ta>
            <ta e="T66" id="Seg_908" s="T65">live-PTCP.PRS</ta>
            <ta e="T67" id="Seg_909" s="T66">human.being.[NOM]</ta>
            <ta e="T68" id="Seg_910" s="T67">three</ta>
            <ta e="T69" id="Seg_911" s="T68">dog-PROPR.[NOM]</ta>
            <ta e="T70" id="Seg_912" s="T69">be-PST2.[3SG]</ta>
            <ta e="T71" id="Seg_913" s="T70">well</ta>
            <ta e="T72" id="Seg_914" s="T71">this</ta>
            <ta e="T73" id="Seg_915" s="T72">dog-PL-3SG-ACC</ta>
            <ta e="T74" id="Seg_916" s="T73">bewitch-CVB.SEQ</ta>
            <ta e="T75" id="Seg_917" s="T74">release-FREQ-PRS.[3SG]</ta>
            <ta e="T76" id="Seg_918" s="T75">EMPH</ta>
            <ta e="T77" id="Seg_919" s="T76">this.[NOM]</ta>
            <ta e="T78" id="Seg_920" s="T77">1SG.[NOM]</ta>
            <ta e="T79" id="Seg_921" s="T78">2PL-DAT/LOC</ta>
            <ta e="T80" id="Seg_922" s="T79">give-PTCP.PRS</ta>
            <ta e="T81" id="Seg_923" s="T80">present-EP-1SG.[NOM]</ta>
            <ta e="T82" id="Seg_924" s="T81">say-PRS.[3SG]</ta>
            <ta e="T83" id="Seg_925" s="T82">this-ACC</ta>
            <ta e="T84" id="Seg_926" s="T83">take-CVB.SEQ</ta>
            <ta e="T85" id="Seg_927" s="T84">go-EP-IMP.2PL</ta>
            <ta e="T86" id="Seg_928" s="T85">say-PRS.[3SG]</ta>
            <ta e="T87" id="Seg_929" s="T86">woman-3PL.[NOM]</ta>
            <ta e="T88" id="Seg_930" s="T87">go-CVB.SEQ</ta>
            <ta e="T89" id="Seg_931" s="T88">stay-PRS-3PL</ta>
            <ta e="T90" id="Seg_932" s="T89">so</ta>
            <ta e="T91" id="Seg_933" s="T90">long.ago</ta>
            <ta e="T92" id="Seg_934" s="T91">shaman-POSS</ta>
            <ta e="T93" id="Seg_935" s="T92">NEG</ta>
            <ta e="T94" id="Seg_936" s="T93">simple</ta>
            <ta e="T95" id="Seg_937" s="T94">human.being.[NOM]</ta>
            <ta e="T96" id="Seg_938" s="T95">self-3SG-GEN</ta>
            <ta e="T97" id="Seg_939" s="T96">nasleg-3SG-ACC</ta>
            <ta e="T98" id="Seg_940" s="T97">save-PST2.[3SG]</ta>
            <ta e="T99" id="Seg_941" s="T98">it.is.said</ta>
            <ta e="T100" id="Seg_942" s="T99">mummy-PL-ABL</ta>
            <ta e="T101" id="Seg_943" s="T100">this</ta>
            <ta e="T102" id="Seg_944" s="T101">woman-PL.[NOM]</ta>
            <ta e="T103" id="Seg_945" s="T102">pock-PL.[NOM]</ta>
            <ta e="T104" id="Seg_946" s="T103">be-PST2.[3SG]</ta>
         </annotation>
         <annotation name="gg" tierref="gg">
            <ta e="T1" id="Seg_947" s="T0">vor.langer.Zeit</ta>
            <ta e="T2" id="Seg_948" s="T1">eins</ta>
            <ta e="T3" id="Seg_949" s="T2">alleine-INTNS</ta>
            <ta e="T4" id="Seg_950" s="T3">leben-PTCP.PRS</ta>
            <ta e="T5" id="Seg_951" s="T4">Mensch-DAT/LOC</ta>
            <ta e="T6" id="Seg_952" s="T5">zwei</ta>
            <ta e="T7" id="Seg_953" s="T6">Frau.[NOM]</ta>
            <ta e="T8" id="Seg_954" s="T7">hineingehen-CVB.SEQ</ta>
            <ta e="T9" id="Seg_955" s="T8">kommen-PST2-3PL</ta>
            <ta e="T10" id="Seg_956" s="T9">dieses-ACC</ta>
            <ta e="T11" id="Seg_957" s="T10">sehen-CVB.ANT</ta>
            <ta e="T12" id="Seg_958" s="T11">Mensch-3PL.[NOM]</ta>
            <ta e="T13" id="Seg_959" s="T12">doch</ta>
            <ta e="T14" id="Seg_960" s="T13">1SG.[NOM]</ta>
            <ta e="T15" id="Seg_961" s="T14">singen-PTCP.PRS-1SG-ACC</ta>
            <ta e="T16" id="Seg_962" s="T15">sehen-EP-IMP.2PL</ta>
            <ta e="T17" id="Seg_963" s="T16">sagen-CVB.SEQ</ta>
            <ta e="T18" id="Seg_964" s="T17">Erde-DAT/LOC</ta>
            <ta e="T19" id="Seg_965" s="T18">Inneres-3SG-INSTR</ta>
            <ta e="T20" id="Seg_966" s="T19">legen-CVB.SEQ</ta>
            <ta e="T21" id="Seg_967" s="T20">singen-CVB.SEQ</ta>
            <ta e="T22" id="Seg_968" s="T21">gehen-PST2.[3SG]</ta>
            <ta e="T23" id="Seg_969" s="T22">dieses</ta>
            <ta e="T24" id="Seg_970" s="T23">singen-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T25" id="Seg_971" s="T24">Weg.[NOM]</ta>
            <ta e="T26" id="Seg_972" s="T25">öffnen-PASS/REFL-EP-PST2.[3SG]</ta>
            <ta e="T27" id="Seg_973" s="T26">dieses-ACC</ta>
            <ta e="T28" id="Seg_974" s="T27">singen-CVB.SEQ</ta>
            <ta e="T29" id="Seg_975" s="T28">aufhören-CVB.SEQ</ta>
            <ta e="T30" id="Seg_976" s="T29">nachdem</ta>
            <ta e="T31" id="Seg_977" s="T30">sprechen-PST2.[3SG]</ta>
            <ta e="T32" id="Seg_978" s="T31">dieses</ta>
            <ta e="T33" id="Seg_979" s="T32">Weg-3SG-INSTR</ta>
            <ta e="T34" id="Seg_980" s="T33">gehen-EP-IMP.2PL</ta>
            <ta e="T35" id="Seg_981" s="T34">2PL.[NOM]</ta>
            <ta e="T36" id="Seg_982" s="T35">sagen-PST2.[3SG]</ta>
            <ta e="T37" id="Seg_983" s="T36">Frau-PL.[NOM]</ta>
            <ta e="T38" id="Seg_984" s="T37">zeigen-PTCP.PST</ta>
            <ta e="T39" id="Seg_985" s="T38">Weg-3SG-INSTR</ta>
            <ta e="T40" id="Seg_986" s="T39">gehen-CVB.SEQ</ta>
            <ta e="T41" id="Seg_987" s="T40">bleiben-PST2-3PL</ta>
            <ta e="T42" id="Seg_988" s="T41">wie.viel</ta>
            <ta e="T43" id="Seg_989" s="T42">INDEF</ta>
            <ta e="T44" id="Seg_990" s="T43">übernachten-CVB.SEQ</ta>
            <ta e="T45" id="Seg_991" s="T44">nachdem</ta>
            <ta e="T46" id="Seg_992" s="T45">dieses.EMPH</ta>
            <ta e="T47" id="Seg_993" s="T46">Mensch-DAT/LOC</ta>
            <ta e="T48" id="Seg_994" s="T47">wieder</ta>
            <ta e="T49" id="Seg_995" s="T48">zurückkommen-CVB.SEQ</ta>
            <ta e="T50" id="Seg_996" s="T49">kommen-PST2-3PL</ta>
            <ta e="T51" id="Seg_997" s="T50">na</ta>
            <ta e="T52" id="Seg_998" s="T51">was.[NOM]</ta>
            <ta e="T53" id="Seg_999" s="T52">Ort-3SG-DAT/LOC</ta>
            <ta e="T54" id="Seg_1000" s="T53">schicken-CVB.SIM</ta>
            <ta e="T55" id="Seg_1001" s="T54">gehen-PST1-2SG</ta>
            <ta e="T56" id="Seg_1002" s="T55">1PL.[NOM]</ta>
            <ta e="T57" id="Seg_1003" s="T56">einseitig</ta>
            <ta e="T58" id="Seg_1004" s="T57">Schatten-ACC</ta>
            <ta e="T59" id="Seg_1005" s="T58">nur</ta>
            <ta e="T60" id="Seg_1006" s="T59">sehen-CVB.SEQ-1PL</ta>
            <ta e="T61" id="Seg_1007" s="T60">hungern-CVB.SEQ</ta>
            <ta e="T62" id="Seg_1008" s="T61">sterben-CVB.SIM</ta>
            <ta e="T63" id="Seg_1009" s="T62">haften-EP-PST2.[3SG]</ta>
            <ta e="T64" id="Seg_1010" s="T63">sagen-PST2-3PL</ta>
            <ta e="T65" id="Seg_1011" s="T64">dieses</ta>
            <ta e="T66" id="Seg_1012" s="T65">leben-PTCP.PRS</ta>
            <ta e="T67" id="Seg_1013" s="T66">Mensch.[NOM]</ta>
            <ta e="T68" id="Seg_1014" s="T67">drei</ta>
            <ta e="T69" id="Seg_1015" s="T68">Hund-PROPR.[NOM]</ta>
            <ta e="T70" id="Seg_1016" s="T69">sein-PST2.[3SG]</ta>
            <ta e="T71" id="Seg_1017" s="T70">doch</ta>
            <ta e="T72" id="Seg_1018" s="T71">dieses</ta>
            <ta e="T73" id="Seg_1019" s="T72">Hund-PL-3SG-ACC</ta>
            <ta e="T74" id="Seg_1020" s="T73">verhexen-CVB.SEQ</ta>
            <ta e="T75" id="Seg_1021" s="T74">lassen-FREQ-PRS.[3SG]</ta>
            <ta e="T76" id="Seg_1022" s="T75">EMPH</ta>
            <ta e="T77" id="Seg_1023" s="T76">dieses.[NOM]</ta>
            <ta e="T78" id="Seg_1024" s="T77">1SG.[NOM]</ta>
            <ta e="T79" id="Seg_1025" s="T78">2PL-DAT/LOC</ta>
            <ta e="T80" id="Seg_1026" s="T79">geben-PTCP.PRS</ta>
            <ta e="T81" id="Seg_1027" s="T80">Geschenk-EP-1SG.[NOM]</ta>
            <ta e="T82" id="Seg_1028" s="T81">sagen-PRS.[3SG]</ta>
            <ta e="T83" id="Seg_1029" s="T82">dieses-ACC</ta>
            <ta e="T84" id="Seg_1030" s="T83">nehmen-CVB.SEQ</ta>
            <ta e="T85" id="Seg_1031" s="T84">gehen-EP-IMP.2PL</ta>
            <ta e="T86" id="Seg_1032" s="T85">sagen-PRS.[3SG]</ta>
            <ta e="T87" id="Seg_1033" s="T86">Frau-3PL.[NOM]</ta>
            <ta e="T88" id="Seg_1034" s="T87">gehen-CVB.SEQ</ta>
            <ta e="T89" id="Seg_1035" s="T88">bleiben-PRS-3PL</ta>
            <ta e="T90" id="Seg_1036" s="T89">so</ta>
            <ta e="T91" id="Seg_1037" s="T90">vor.langer.Zeit</ta>
            <ta e="T92" id="Seg_1038" s="T91">Schamane-POSS</ta>
            <ta e="T93" id="Seg_1039" s="T92">NEG</ta>
            <ta e="T94" id="Seg_1040" s="T93">einfach</ta>
            <ta e="T95" id="Seg_1041" s="T94">Mensch.[NOM]</ta>
            <ta e="T96" id="Seg_1042" s="T95">selbst-3SG-GEN</ta>
            <ta e="T97" id="Seg_1043" s="T96">Nasleg-3SG-ACC</ta>
            <ta e="T98" id="Seg_1044" s="T97">retten-PST2.[3SG]</ta>
            <ta e="T99" id="Seg_1045" s="T98">man.sagt</ta>
            <ta e="T100" id="Seg_1046" s="T99">Mütterchen-PL-ABL</ta>
            <ta e="T101" id="Seg_1047" s="T100">dieses</ta>
            <ta e="T102" id="Seg_1048" s="T101">Frau-PL.[NOM]</ta>
            <ta e="T103" id="Seg_1049" s="T102">Pocke-PL.[NOM]</ta>
            <ta e="T104" id="Seg_1050" s="T103">sein-PST2.[3SG]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_1051" s="T0">давно</ta>
            <ta e="T2" id="Seg_1052" s="T1">один</ta>
            <ta e="T3" id="Seg_1053" s="T2">одиноко-INTNS</ta>
            <ta e="T4" id="Seg_1054" s="T3">жить-PTCP.PRS</ta>
            <ta e="T5" id="Seg_1055" s="T4">человек-DAT/LOC</ta>
            <ta e="T6" id="Seg_1056" s="T5">два</ta>
            <ta e="T7" id="Seg_1057" s="T6">жена.[NOM]</ta>
            <ta e="T8" id="Seg_1058" s="T7">входить-CVB.SEQ</ta>
            <ta e="T9" id="Seg_1059" s="T8">приходить-PST2-3PL</ta>
            <ta e="T10" id="Seg_1060" s="T9">этот-ACC</ta>
            <ta e="T11" id="Seg_1061" s="T10">видеть-CVB.ANT</ta>
            <ta e="T12" id="Seg_1062" s="T11">человек-3PL.[NOM]</ta>
            <ta e="T13" id="Seg_1063" s="T12">вот</ta>
            <ta e="T14" id="Seg_1064" s="T13">1SG.[NOM]</ta>
            <ta e="T15" id="Seg_1065" s="T14">петь-PTCP.PRS-1SG-ACC</ta>
            <ta e="T16" id="Seg_1066" s="T15">видеть-EP-IMP.2PL</ta>
            <ta e="T17" id="Seg_1067" s="T16">говорить-CVB.SEQ</ta>
            <ta e="T18" id="Seg_1068" s="T17">земля-DAT/LOC</ta>
            <ta e="T19" id="Seg_1069" s="T18">нутро-3SG-INSTR</ta>
            <ta e="T20" id="Seg_1070" s="T19">класть-CVB.SEQ</ta>
            <ta e="T21" id="Seg_1071" s="T20">петь-CVB.SEQ</ta>
            <ta e="T22" id="Seg_1072" s="T21">идти-PST2.[3SG]</ta>
            <ta e="T23" id="Seg_1073" s="T22">этот</ta>
            <ta e="T24" id="Seg_1074" s="T23">петь-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T25" id="Seg_1075" s="T24">дорога.[NOM]</ta>
            <ta e="T26" id="Seg_1076" s="T25">открывать-PASS/REFL-EP-PST2.[3SG]</ta>
            <ta e="T27" id="Seg_1077" s="T26">этот-ACC</ta>
            <ta e="T28" id="Seg_1078" s="T27">петь-CVB.SEQ</ta>
            <ta e="T29" id="Seg_1079" s="T28">кончать-CVB.SEQ</ta>
            <ta e="T30" id="Seg_1080" s="T29">после</ta>
            <ta e="T31" id="Seg_1081" s="T30">говорить-PST2.[3SG]</ta>
            <ta e="T32" id="Seg_1082" s="T31">этот</ta>
            <ta e="T33" id="Seg_1083" s="T32">дорога-3SG-INSTR</ta>
            <ta e="T34" id="Seg_1084" s="T33">идти-EP-IMP.2PL</ta>
            <ta e="T35" id="Seg_1085" s="T34">2PL.[NOM]</ta>
            <ta e="T36" id="Seg_1086" s="T35">говорить-PST2.[3SG]</ta>
            <ta e="T37" id="Seg_1087" s="T36">жена-PL.[NOM]</ta>
            <ta e="T38" id="Seg_1088" s="T37">показывать-PTCP.PST</ta>
            <ta e="T39" id="Seg_1089" s="T38">дорога-3SG-INSTR</ta>
            <ta e="T40" id="Seg_1090" s="T39">идти-CVB.SEQ</ta>
            <ta e="T41" id="Seg_1091" s="T40">оставаться-PST2-3PL</ta>
            <ta e="T42" id="Seg_1092" s="T41">сколько</ta>
            <ta e="T43" id="Seg_1093" s="T42">INDEF</ta>
            <ta e="T44" id="Seg_1094" s="T43">ночевать-CVB.SEQ</ta>
            <ta e="T45" id="Seg_1095" s="T44">после</ta>
            <ta e="T46" id="Seg_1096" s="T45">этот.EMPH</ta>
            <ta e="T47" id="Seg_1097" s="T46">человек-DAT/LOC</ta>
            <ta e="T48" id="Seg_1098" s="T47">опять</ta>
            <ta e="T49" id="Seg_1099" s="T48">возвращаться-CVB.SEQ</ta>
            <ta e="T50" id="Seg_1100" s="T49">приходить-PST2-3PL</ta>
            <ta e="T51" id="Seg_1101" s="T50">эй</ta>
            <ta e="T52" id="Seg_1102" s="T51">что.[NOM]</ta>
            <ta e="T53" id="Seg_1103" s="T52">место-3SG-DAT/LOC</ta>
            <ta e="T54" id="Seg_1104" s="T53">послать-CVB.SIM</ta>
            <ta e="T55" id="Seg_1105" s="T54">идти-PST1-2SG</ta>
            <ta e="T56" id="Seg_1106" s="T55">1PL.[NOM]</ta>
            <ta e="T57" id="Seg_1107" s="T56">односторонный</ta>
            <ta e="T58" id="Seg_1108" s="T57">тень-ACC</ta>
            <ta e="T59" id="Seg_1109" s="T58">только</ta>
            <ta e="T60" id="Seg_1110" s="T59">видеть-CVB.SEQ-1PL</ta>
            <ta e="T61" id="Seg_1111" s="T60">голодать-CVB.SEQ</ta>
            <ta e="T62" id="Seg_1112" s="T61">умирать-CVB.SIM</ta>
            <ta e="T63" id="Seg_1113" s="T62">прилипать-EP-PST2.[3SG]</ta>
            <ta e="T64" id="Seg_1114" s="T63">говорить-PST2-3PL</ta>
            <ta e="T65" id="Seg_1115" s="T64">этот</ta>
            <ta e="T66" id="Seg_1116" s="T65">жить-PTCP.PRS</ta>
            <ta e="T67" id="Seg_1117" s="T66">человек.[NOM]</ta>
            <ta e="T68" id="Seg_1118" s="T67">три</ta>
            <ta e="T69" id="Seg_1119" s="T68">собака-PROPR.[NOM]</ta>
            <ta e="T70" id="Seg_1120" s="T69">быть-PST2.[3SG]</ta>
            <ta e="T71" id="Seg_1121" s="T70">вот</ta>
            <ta e="T72" id="Seg_1122" s="T71">этот</ta>
            <ta e="T73" id="Seg_1123" s="T72">собака-PL-3SG-ACC</ta>
            <ta e="T74" id="Seg_1124" s="T73">заклинать-CVB.SEQ</ta>
            <ta e="T75" id="Seg_1125" s="T74">пустить-FREQ-PRS.[3SG]</ta>
            <ta e="T76" id="Seg_1126" s="T75">EMPH</ta>
            <ta e="T77" id="Seg_1127" s="T76">этот.[NOM]</ta>
            <ta e="T78" id="Seg_1128" s="T77">1SG.[NOM]</ta>
            <ta e="T79" id="Seg_1129" s="T78">2PL-DAT/LOC</ta>
            <ta e="T80" id="Seg_1130" s="T79">давать-PTCP.PRS</ta>
            <ta e="T81" id="Seg_1131" s="T80">подарок-EP-1SG.[NOM]</ta>
            <ta e="T82" id="Seg_1132" s="T81">говорить-PRS.[3SG]</ta>
            <ta e="T83" id="Seg_1133" s="T82">этот-ACC</ta>
            <ta e="T84" id="Seg_1134" s="T83">взять-CVB.SEQ</ta>
            <ta e="T85" id="Seg_1135" s="T84">идти-EP-IMP.2PL</ta>
            <ta e="T86" id="Seg_1136" s="T85">говорить-PRS.[3SG]</ta>
            <ta e="T87" id="Seg_1137" s="T86">жена-3PL.[NOM]</ta>
            <ta e="T88" id="Seg_1138" s="T87">идти-CVB.SEQ</ta>
            <ta e="T89" id="Seg_1139" s="T88">оставаться-PRS-3PL</ta>
            <ta e="T90" id="Seg_1140" s="T89">так</ta>
            <ta e="T91" id="Seg_1141" s="T90">давно</ta>
            <ta e="T92" id="Seg_1142" s="T91">шаман-POSS</ta>
            <ta e="T93" id="Seg_1143" s="T92">NEG</ta>
            <ta e="T94" id="Seg_1144" s="T93">простой</ta>
            <ta e="T95" id="Seg_1145" s="T94">человек.[NOM]</ta>
            <ta e="T96" id="Seg_1146" s="T95">сам-3SG-GEN</ta>
            <ta e="T97" id="Seg_1147" s="T96">наслег-3SG-ACC</ta>
            <ta e="T98" id="Seg_1148" s="T97">спасать-PST2.[3SG]</ta>
            <ta e="T99" id="Seg_1149" s="T98">говорят</ta>
            <ta e="T100" id="Seg_1150" s="T99">матушка-PL-ABL</ta>
            <ta e="T101" id="Seg_1151" s="T100">этот</ta>
            <ta e="T102" id="Seg_1152" s="T101">жена-PL.[NOM]</ta>
            <ta e="T103" id="Seg_1153" s="T102">оспа-PL.[NOM]</ta>
            <ta e="T104" id="Seg_1154" s="T103">быть-PST2.[3SG]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_1155" s="T0">adv</ta>
            <ta e="T2" id="Seg_1156" s="T1">cardnum</ta>
            <ta e="T3" id="Seg_1157" s="T2">adv-adv&gt;adv</ta>
            <ta e="T4" id="Seg_1158" s="T3">v-v:ptcp</ta>
            <ta e="T5" id="Seg_1159" s="T4">n-n:case</ta>
            <ta e="T6" id="Seg_1160" s="T5">cardnum</ta>
            <ta e="T7" id="Seg_1161" s="T6">n-n:case</ta>
            <ta e="T8" id="Seg_1162" s="T7">v-v:cvb</ta>
            <ta e="T9" id="Seg_1163" s="T8">v-v:tense-v:pred.pn</ta>
            <ta e="T10" id="Seg_1164" s="T9">dempro-pro:case</ta>
            <ta e="T11" id="Seg_1165" s="T10">v-v:cvb</ta>
            <ta e="T12" id="Seg_1166" s="T11">n-n:(poss)-n:case</ta>
            <ta e="T13" id="Seg_1167" s="T12">ptcl</ta>
            <ta e="T14" id="Seg_1168" s="T13">pers-pro:case</ta>
            <ta e="T15" id="Seg_1169" s="T14">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T16" id="Seg_1170" s="T15">v-v:(ins)-v:mood.pn</ta>
            <ta e="T17" id="Seg_1171" s="T16">v-v:cvb</ta>
            <ta e="T18" id="Seg_1172" s="T17">n-n:case</ta>
            <ta e="T19" id="Seg_1173" s="T18">n-n:poss-n:case</ta>
            <ta e="T20" id="Seg_1174" s="T19">v-v:cvb</ta>
            <ta e="T21" id="Seg_1175" s="T20">v-v:cvb</ta>
            <ta e="T22" id="Seg_1176" s="T21">v-v:tense-v:pred.pn</ta>
            <ta e="T23" id="Seg_1177" s="T22">dempro</ta>
            <ta e="T24" id="Seg_1178" s="T23">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T25" id="Seg_1179" s="T24">n-n:case</ta>
            <ta e="T26" id="Seg_1180" s="T25">v-v&gt;v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T27" id="Seg_1181" s="T26">dempro-pro:case</ta>
            <ta e="T28" id="Seg_1182" s="T27">v-v:cvb</ta>
            <ta e="T29" id="Seg_1183" s="T28">v-v:cvb</ta>
            <ta e="T30" id="Seg_1184" s="T29">post</ta>
            <ta e="T31" id="Seg_1185" s="T30">v-v:tense-v:pred.pn</ta>
            <ta e="T32" id="Seg_1186" s="T31">dempro</ta>
            <ta e="T33" id="Seg_1187" s="T32">n-n:poss-n:case</ta>
            <ta e="T34" id="Seg_1188" s="T33">v-v:(ins)-v:mood.pn</ta>
            <ta e="T35" id="Seg_1189" s="T34">pers-pro:case</ta>
            <ta e="T36" id="Seg_1190" s="T35">v-v:tense-v:pred.pn</ta>
            <ta e="T37" id="Seg_1191" s="T36">n-n:(num)-n:case</ta>
            <ta e="T38" id="Seg_1192" s="T37">v-v:ptcp</ta>
            <ta e="T39" id="Seg_1193" s="T38">n-n:poss-n:case</ta>
            <ta e="T40" id="Seg_1194" s="T39">v-v:cvb</ta>
            <ta e="T41" id="Seg_1195" s="T40">v-v:tense-v:pred.pn</ta>
            <ta e="T42" id="Seg_1196" s="T41">que</ta>
            <ta e="T43" id="Seg_1197" s="T42">ptcl</ta>
            <ta e="T44" id="Seg_1198" s="T43">v-v:cvb</ta>
            <ta e="T45" id="Seg_1199" s="T44">post</ta>
            <ta e="T46" id="Seg_1200" s="T45">dempro</ta>
            <ta e="T47" id="Seg_1201" s="T46">n-n:case</ta>
            <ta e="T48" id="Seg_1202" s="T47">ptcl</ta>
            <ta e="T49" id="Seg_1203" s="T48">v-v:cvb</ta>
            <ta e="T50" id="Seg_1204" s="T49">v-v:tense-v:pred.pn</ta>
            <ta e="T51" id="Seg_1205" s="T50">interj</ta>
            <ta e="T52" id="Seg_1206" s="T51">que-pro:case</ta>
            <ta e="T53" id="Seg_1207" s="T52">n-n:poss-n:case</ta>
            <ta e="T54" id="Seg_1208" s="T53">v-v:cvb</ta>
            <ta e="T55" id="Seg_1209" s="T54">v-v:tense-v:poss.pn</ta>
            <ta e="T56" id="Seg_1210" s="T55">pers-pro:case</ta>
            <ta e="T57" id="Seg_1211" s="T56">adj</ta>
            <ta e="T58" id="Seg_1212" s="T57">n-n:case</ta>
            <ta e="T59" id="Seg_1213" s="T58">ptcl</ta>
            <ta e="T60" id="Seg_1214" s="T59">v-v:cvb-v:pred.pn</ta>
            <ta e="T61" id="Seg_1215" s="T60">v-v:cvb</ta>
            <ta e="T62" id="Seg_1216" s="T61">v-v:cvb</ta>
            <ta e="T63" id="Seg_1217" s="T62">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T64" id="Seg_1218" s="T63">v-v:tense-v:pred.pn</ta>
            <ta e="T65" id="Seg_1219" s="T64">dempro</ta>
            <ta e="T66" id="Seg_1220" s="T65">v-v:ptcp</ta>
            <ta e="T67" id="Seg_1221" s="T66">n-n:case</ta>
            <ta e="T68" id="Seg_1222" s="T67">cardnum</ta>
            <ta e="T69" id="Seg_1223" s="T68">n-n&gt;adj-n:case</ta>
            <ta e="T70" id="Seg_1224" s="T69">v-v:tense-v:pred.pn</ta>
            <ta e="T71" id="Seg_1225" s="T70">ptcl</ta>
            <ta e="T72" id="Seg_1226" s="T71">dempro</ta>
            <ta e="T73" id="Seg_1227" s="T72">n-n:(num)-n:poss-n:case</ta>
            <ta e="T74" id="Seg_1228" s="T73">v-v:cvb</ta>
            <ta e="T75" id="Seg_1229" s="T74">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T76" id="Seg_1230" s="T75">ptcl</ta>
            <ta e="T77" id="Seg_1231" s="T76">dempro-n:case</ta>
            <ta e="T78" id="Seg_1232" s="T77">pers-pro:case</ta>
            <ta e="T79" id="Seg_1233" s="T78">pers-pro:case</ta>
            <ta e="T80" id="Seg_1234" s="T79">v-v:ptcp</ta>
            <ta e="T81" id="Seg_1235" s="T80">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T82" id="Seg_1236" s="T81">v-v:tense-v:pred.pn</ta>
            <ta e="T83" id="Seg_1237" s="T82">dempro-pro:case</ta>
            <ta e="T84" id="Seg_1238" s="T83">v-v:cvb</ta>
            <ta e="T85" id="Seg_1239" s="T84">v-v:(ins)-v:mood.pn</ta>
            <ta e="T86" id="Seg_1240" s="T85">v-v:tense-v:pred.pn</ta>
            <ta e="T87" id="Seg_1241" s="T86">n-n:(poss)-n:case</ta>
            <ta e="T88" id="Seg_1242" s="T87">v-v:cvb</ta>
            <ta e="T89" id="Seg_1243" s="T88">v-v:tense-v:pred.pn</ta>
            <ta e="T90" id="Seg_1244" s="T89">adv</ta>
            <ta e="T91" id="Seg_1245" s="T90">adv</ta>
            <ta e="T92" id="Seg_1246" s="T91">n-n:(poss)</ta>
            <ta e="T93" id="Seg_1247" s="T92">ptcl</ta>
            <ta e="T94" id="Seg_1248" s="T93">adj</ta>
            <ta e="T95" id="Seg_1249" s="T94">n-n:case</ta>
            <ta e="T96" id="Seg_1250" s="T95">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T97" id="Seg_1251" s="T96">n-n:poss-n:case</ta>
            <ta e="T98" id="Seg_1252" s="T97">v-v:tense-v:pred.pn</ta>
            <ta e="T99" id="Seg_1253" s="T98">ptcl</ta>
            <ta e="T100" id="Seg_1254" s="T99">n-n:(num)-n:case</ta>
            <ta e="T101" id="Seg_1255" s="T100">dempro</ta>
            <ta e="T102" id="Seg_1256" s="T101">n-n:(num)-n:case</ta>
            <ta e="T103" id="Seg_1257" s="T102">n-n:(num)-n:case</ta>
            <ta e="T104" id="Seg_1258" s="T103">v-v:tense-v:pred.pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_1259" s="T0">adv</ta>
            <ta e="T2" id="Seg_1260" s="T1">cardnum</ta>
            <ta e="T3" id="Seg_1261" s="T2">adv</ta>
            <ta e="T4" id="Seg_1262" s="T3">v</ta>
            <ta e="T5" id="Seg_1263" s="T4">n</ta>
            <ta e="T6" id="Seg_1264" s="T5">cardnum</ta>
            <ta e="T7" id="Seg_1265" s="T6">n</ta>
            <ta e="T8" id="Seg_1266" s="T7">v</ta>
            <ta e="T9" id="Seg_1267" s="T8">v</ta>
            <ta e="T10" id="Seg_1268" s="T9">dempro</ta>
            <ta e="T11" id="Seg_1269" s="T10">v</ta>
            <ta e="T12" id="Seg_1270" s="T11">n</ta>
            <ta e="T13" id="Seg_1271" s="T12">ptcl</ta>
            <ta e="T14" id="Seg_1272" s="T13">pers</ta>
            <ta e="T15" id="Seg_1273" s="T14">v</ta>
            <ta e="T16" id="Seg_1274" s="T15">v</ta>
            <ta e="T17" id="Seg_1275" s="T16">v</ta>
            <ta e="T18" id="Seg_1276" s="T17">n</ta>
            <ta e="T19" id="Seg_1277" s="T18">n</ta>
            <ta e="T20" id="Seg_1278" s="T19">v</ta>
            <ta e="T21" id="Seg_1279" s="T20">v</ta>
            <ta e="T22" id="Seg_1280" s="T21">aux</ta>
            <ta e="T23" id="Seg_1281" s="T22">dempro</ta>
            <ta e="T24" id="Seg_1282" s="T23">v</ta>
            <ta e="T25" id="Seg_1283" s="T24">n</ta>
            <ta e="T26" id="Seg_1284" s="T25">v</ta>
            <ta e="T27" id="Seg_1285" s="T26">dempro</ta>
            <ta e="T28" id="Seg_1286" s="T27">v</ta>
            <ta e="T29" id="Seg_1287" s="T28">v</ta>
            <ta e="T30" id="Seg_1288" s="T29">post</ta>
            <ta e="T31" id="Seg_1289" s="T30">v</ta>
            <ta e="T32" id="Seg_1290" s="T31">dempro</ta>
            <ta e="T33" id="Seg_1291" s="T32">n</ta>
            <ta e="T34" id="Seg_1292" s="T33">v</ta>
            <ta e="T35" id="Seg_1293" s="T34">pers</ta>
            <ta e="T36" id="Seg_1294" s="T35">v</ta>
            <ta e="T37" id="Seg_1295" s="T36">n</ta>
            <ta e="T38" id="Seg_1296" s="T37">v</ta>
            <ta e="T39" id="Seg_1297" s="T38">n</ta>
            <ta e="T40" id="Seg_1298" s="T39">v</ta>
            <ta e="T41" id="Seg_1299" s="T40">aux</ta>
            <ta e="T42" id="Seg_1300" s="T41">que</ta>
            <ta e="T43" id="Seg_1301" s="T42">ptcl</ta>
            <ta e="T44" id="Seg_1302" s="T43">v</ta>
            <ta e="T45" id="Seg_1303" s="T44">post</ta>
            <ta e="T46" id="Seg_1304" s="T45">dempro</ta>
            <ta e="T47" id="Seg_1305" s="T46">n</ta>
            <ta e="T48" id="Seg_1306" s="T47">ptcl</ta>
            <ta e="T49" id="Seg_1307" s="T48">v</ta>
            <ta e="T50" id="Seg_1308" s="T49">v</ta>
            <ta e="T51" id="Seg_1309" s="T50">interj</ta>
            <ta e="T52" id="Seg_1310" s="T51">que</ta>
            <ta e="T53" id="Seg_1311" s="T52">n</ta>
            <ta e="T54" id="Seg_1312" s="T53">v</ta>
            <ta e="T55" id="Seg_1313" s="T54">aux</ta>
            <ta e="T56" id="Seg_1314" s="T55">pers</ta>
            <ta e="T57" id="Seg_1315" s="T56">adj</ta>
            <ta e="T58" id="Seg_1316" s="T57">n</ta>
            <ta e="T59" id="Seg_1317" s="T58">ptcl</ta>
            <ta e="T60" id="Seg_1318" s="T59">v</ta>
            <ta e="T61" id="Seg_1319" s="T60">v</ta>
            <ta e="T62" id="Seg_1320" s="T61">v</ta>
            <ta e="T63" id="Seg_1321" s="T62">v</ta>
            <ta e="T64" id="Seg_1322" s="T63">v</ta>
            <ta e="T65" id="Seg_1323" s="T64">dempro</ta>
            <ta e="T66" id="Seg_1324" s="T65">v</ta>
            <ta e="T67" id="Seg_1325" s="T66">n</ta>
            <ta e="T68" id="Seg_1326" s="T67">cardnum</ta>
            <ta e="T69" id="Seg_1327" s="T68">adj</ta>
            <ta e="T70" id="Seg_1328" s="T69">cop</ta>
            <ta e="T71" id="Seg_1329" s="T70">ptcl</ta>
            <ta e="T72" id="Seg_1330" s="T71">dempro</ta>
            <ta e="T73" id="Seg_1331" s="T72">n</ta>
            <ta e="T74" id="Seg_1332" s="T73">v</ta>
            <ta e="T75" id="Seg_1333" s="T74">v</ta>
            <ta e="T76" id="Seg_1334" s="T75">ptcl</ta>
            <ta e="T77" id="Seg_1335" s="T76">dempro</ta>
            <ta e="T78" id="Seg_1336" s="T77">pers</ta>
            <ta e="T79" id="Seg_1337" s="T78">pers</ta>
            <ta e="T80" id="Seg_1338" s="T79">v</ta>
            <ta e="T81" id="Seg_1339" s="T80">n</ta>
            <ta e="T82" id="Seg_1340" s="T81">v</ta>
            <ta e="T83" id="Seg_1341" s="T82">dempro</ta>
            <ta e="T84" id="Seg_1342" s="T83">v</ta>
            <ta e="T85" id="Seg_1343" s="T84">v</ta>
            <ta e="T86" id="Seg_1344" s="T85">v</ta>
            <ta e="T87" id="Seg_1345" s="T86">n</ta>
            <ta e="T88" id="Seg_1346" s="T87">v</ta>
            <ta e="T89" id="Seg_1347" s="T88">aux</ta>
            <ta e="T90" id="Seg_1348" s="T89">adv</ta>
            <ta e="T91" id="Seg_1349" s="T90">adv</ta>
            <ta e="T92" id="Seg_1350" s="T91">n</ta>
            <ta e="T93" id="Seg_1351" s="T92">ptcl</ta>
            <ta e="T94" id="Seg_1352" s="T93">adj</ta>
            <ta e="T95" id="Seg_1353" s="T94">n</ta>
            <ta e="T96" id="Seg_1354" s="T95">emphpro</ta>
            <ta e="T97" id="Seg_1355" s="T96">n</ta>
            <ta e="T98" id="Seg_1356" s="T97">v</ta>
            <ta e="T99" id="Seg_1357" s="T98">ptcl</ta>
            <ta e="T100" id="Seg_1358" s="T99">n</ta>
            <ta e="T101" id="Seg_1359" s="T100">dempro</ta>
            <ta e="T102" id="Seg_1360" s="T101">n</ta>
            <ta e="T103" id="Seg_1361" s="T102">n</ta>
            <ta e="T104" id="Seg_1362" s="T103">cop</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_1363" s="T0">adv:Time</ta>
            <ta e="T5" id="Seg_1364" s="T4">np:G</ta>
            <ta e="T7" id="Seg_1365" s="T6">np.h:A</ta>
            <ta e="T10" id="Seg_1366" s="T9">pro.h:St</ta>
            <ta e="T14" id="Seg_1367" s="T13">pro.h:A</ta>
            <ta e="T16" id="Seg_1368" s="T15">0.2.h:A</ta>
            <ta e="T22" id="Seg_1369" s="T20">0.3.h:A</ta>
            <ta e="T24" id="Seg_1370" s="T23">0.3.h:A</ta>
            <ta e="T25" id="Seg_1371" s="T24">np:Th</ta>
            <ta e="T31" id="Seg_1372" s="T30">0.3.h:A</ta>
            <ta e="T33" id="Seg_1373" s="T32">np:Path</ta>
            <ta e="T35" id="Seg_1374" s="T34">pro.h:A</ta>
            <ta e="T36" id="Seg_1375" s="T35">0.3.h:A</ta>
            <ta e="T37" id="Seg_1376" s="T36">np.h:A</ta>
            <ta e="T39" id="Seg_1377" s="T38">np:Path</ta>
            <ta e="T47" id="Seg_1378" s="T46">np:G</ta>
            <ta e="T50" id="Seg_1379" s="T49">0.3.h:A</ta>
            <ta e="T53" id="Seg_1380" s="T52">np:G</ta>
            <ta e="T55" id="Seg_1381" s="T53">0.2.h:A</ta>
            <ta e="T56" id="Seg_1382" s="T55">pro.h:E</ta>
            <ta e="T58" id="Seg_1383" s="T57">np:St</ta>
            <ta e="T67" id="Seg_1384" s="T66">np.h:Poss</ta>
            <ta e="T69" id="Seg_1385" s="T68">np:Th</ta>
            <ta e="T73" id="Seg_1386" s="T72">np:Th</ta>
            <ta e="T75" id="Seg_1387" s="T74">0.3.h:A</ta>
            <ta e="T77" id="Seg_1388" s="T76">pro:Th</ta>
            <ta e="T78" id="Seg_1389" s="T77">pro.h:A</ta>
            <ta e="T79" id="Seg_1390" s="T78">pro.h:R</ta>
            <ta e="T82" id="Seg_1391" s="T81">0.3.h:A</ta>
            <ta e="T83" id="Seg_1392" s="T82">pro:Th</ta>
            <ta e="T85" id="Seg_1393" s="T84">0.2.h:A</ta>
            <ta e="T86" id="Seg_1394" s="T85">0.3.h:A</ta>
            <ta e="T87" id="Seg_1395" s="T86">np.h:A</ta>
            <ta e="T95" id="Seg_1396" s="T94">np.h:A</ta>
            <ta e="T96" id="Seg_1397" s="T95">pro.h:Poss</ta>
            <ta e="T97" id="Seg_1398" s="T96">np.h:P</ta>
            <ta e="T102" id="Seg_1399" s="T101">np.h:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T4" id="Seg_1400" s="T1">s:rel</ta>
            <ta e="T7" id="Seg_1401" s="T6">np.h:S</ta>
            <ta e="T9" id="Seg_1402" s="T8">v:pred</ta>
            <ta e="T11" id="Seg_1403" s="T9">s:adv</ta>
            <ta e="T15" id="Seg_1404" s="T13">s:comp</ta>
            <ta e="T16" id="Seg_1405" s="T15">0.2.h:S v:pred</ta>
            <ta e="T22" id="Seg_1406" s="T20">0.3.h:S v:pred</ta>
            <ta e="T24" id="Seg_1407" s="T22">s:temp</ta>
            <ta e="T25" id="Seg_1408" s="T24">np:S</ta>
            <ta e="T26" id="Seg_1409" s="T25">v:pred</ta>
            <ta e="T30" id="Seg_1410" s="T27">s:temp</ta>
            <ta e="T31" id="Seg_1411" s="T30">0.3.h:S v:pred</ta>
            <ta e="T34" id="Seg_1412" s="T33">v:pred</ta>
            <ta e="T35" id="Seg_1413" s="T34">pro.h:S</ta>
            <ta e="T36" id="Seg_1414" s="T35">0.3.h:S v:pred</ta>
            <ta e="T37" id="Seg_1415" s="T36">np.h:S</ta>
            <ta e="T41" id="Seg_1416" s="T39">v:pred</ta>
            <ta e="T45" id="Seg_1417" s="T41">s:temp</ta>
            <ta e="T50" id="Seg_1418" s="T49">0.3.h:S v:pred</ta>
            <ta e="T55" id="Seg_1419" s="T53">0.2.h:S v:pred</ta>
            <ta e="T56" id="Seg_1420" s="T55">pro.h:S</ta>
            <ta e="T58" id="Seg_1421" s="T57">np:O</ta>
            <ta e="T60" id="Seg_1422" s="T59">v:pred</ta>
            <ta e="T63" id="Seg_1423" s="T60">s:rel</ta>
            <ta e="T67" id="Seg_1424" s="T66">np.h:S</ta>
            <ta e="T69" id="Seg_1425" s="T68">adj:pred</ta>
            <ta e="T70" id="Seg_1426" s="T69">cop</ta>
            <ta e="T73" id="Seg_1427" s="T72">np:O</ta>
            <ta e="T74" id="Seg_1428" s="T73">s:temp</ta>
            <ta e="T75" id="Seg_1429" s="T74">0.3.h:S v:pred</ta>
            <ta e="T77" id="Seg_1430" s="T76">pro:S</ta>
            <ta e="T80" id="Seg_1431" s="T77">s:rel</ta>
            <ta e="T81" id="Seg_1432" s="T80">n:pred</ta>
            <ta e="T82" id="Seg_1433" s="T81">0.3.h:S v:pred</ta>
            <ta e="T84" id="Seg_1434" s="T82">s:temp</ta>
            <ta e="T85" id="Seg_1435" s="T84">0.2.h:S v:pred</ta>
            <ta e="T86" id="Seg_1436" s="T85">0.3.h:S v:pred</ta>
            <ta e="T87" id="Seg_1437" s="T86">np.h:S</ta>
            <ta e="T89" id="Seg_1438" s="T87">v:pred</ta>
            <ta e="T95" id="Seg_1439" s="T94">np.h:S</ta>
            <ta e="T97" id="Seg_1440" s="T96">np.h:O</ta>
            <ta e="T98" id="Seg_1441" s="T97">v:pred</ta>
            <ta e="T102" id="Seg_1442" s="T101">np.h:S</ta>
            <ta e="T103" id="Seg_1443" s="T102">n:pred</ta>
            <ta e="T104" id="Seg_1444" s="T103">cop</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="Top" tierref="Top" />
         <annotation name="Foc" tierref="Foc" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T97" id="Seg_1445" s="T96">RUS:cult</ta>
            <ta e="T100" id="Seg_1446" s="T99">RUS:cult</ta>
            <ta e="T103" id="Seg_1447" s="T102">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon">
            <ta e="T97" id="Seg_1448" s="T96">Vsub Csub medVins Vsub</ta>
            <ta e="T100" id="Seg_1449" s="T99">Vsub Csub</ta>
            <ta e="T103" id="Seg_1450" s="T102">inCins Vsub</ta>
         </annotation>
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T97" id="Seg_1451" s="T96">dir:infl</ta>
            <ta e="T100" id="Seg_1452" s="T99">dir:infl</ta>
            <ta e="T103" id="Seg_1453" s="T102">dir:infl</ta>
         </annotation>
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T9" id="Seg_1454" s="T0">Long ago two women came to a lonesome human.</ta>
            <ta e="T12" id="Seg_1455" s="T9">That human saw them:</ta>
            <ta e="T26" id="Seg_1456" s="T12">"Look what I sing about", he said, and laying on the ground he started to sing, while he was singing, a way opened itself.</ta>
            <ta e="T31" id="Seg_1457" s="T26">After he stopped singing he said:</ta>
            <ta e="T36" id="Seg_1458" s="T31">"Walk along this way", he said.</ta>
            <ta e="T41" id="Seg_1459" s="T36">The women walked the way they were shown.</ta>
            <ta e="T50" id="Seg_1460" s="T41">After a while they came back to that human.</ta>
            <ta e="T55" id="Seg_1461" s="T50">Well, what kind of place did you send us to?</ta>
            <ta e="T64" id="Seg_1462" s="T55">We saw only shadows and bearly died from starving", they said. </ta>
            <ta e="T70" id="Seg_1463" s="T64">That human had, apparently, three dogs.</ta>
            <ta e="T76" id="Seg_1464" s="T70">He gave them his bewitched dogs:</ta>
            <ta e="T82" id="Seg_1465" s="T76">"This is my present for you", he said.</ta>
            <ta e="T86" id="Seg_1466" s="T82">"Take them and go", he said.</ta>
            <ta e="T89" id="Seg_1467" s="T86">The women went away.</ta>
            <ta e="T100" id="Seg_1468" s="T89">This way a simple human, not a shaman, saved his nasleg from mummies, they say.</ta>
            <ta e="T104" id="Seg_1469" s="T100">Those women were pox [spirits].</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T9" id="Seg_1470" s="T0">Vor langer Zeit kamen einmal zu einem ganz alleine lebenden Menschen zwei Frauen.</ta>
            <ta e="T12" id="Seg_1471" s="T9">Dieser Mensch sah sie:</ta>
            <ta e="T26" id="Seg_1472" s="T12">"Seht, worüber ich singe", sagte er, er legte sich mit dem Gesicht zur Erde, fing an zu singen und während er sang, öffnete sich ein Weg.</ta>
            <ta e="T31" id="Seg_1473" s="T26">Nachdem er aufgehört hatte zu singen, sagte er:</ta>
            <ta e="T36" id="Seg_1474" s="T31">"Geht diesen Weg entlang", sagte er.</ta>
            <ta e="T41" id="Seg_1475" s="T36">Die Frauen gingen den Weg entlang, der ihnen gezeigt wurde.</ta>
            <ta e="T50" id="Seg_1476" s="T41">Nach einigen Tagen kamen sie wieder zu diesem Menschen zurück.</ta>
            <ta e="T55" id="Seg_1477" s="T50">"Nun, wo hast du uns hingeschickt?</ta>
            <ta e="T64" id="Seg_1478" s="T55">Wir haben nur Schatten gesehen, die vor Hunger fast gestorben sind", sagten sie.</ta>
            <ta e="T70" id="Seg_1479" s="T64">Dieser Mensch hatte offenbar drei Hunde.</ta>
            <ta e="T76" id="Seg_1480" s="T70">Diese Hunde gab er ihnen und verhexte sie:</ta>
            <ta e="T82" id="Seg_1481" s="T76">"Nun das ist meine Gabe an euch", sagte er.</ta>
            <ta e="T86" id="Seg_1482" s="T82">"Nehmt sie und geht", sagte er.</ta>
            <ta e="T89" id="Seg_1483" s="T86">Die Frauen gingen.</ta>
            <ta e="T100" id="Seg_1484" s="T89">So rettete vor langer Zeit ein einfacher Mensch, kein Schamane, seinen Nasleg/seine Leute vor den Mütterchen.</ta>
            <ta e="T104" id="Seg_1485" s="T100">Diese Frauen waren nämlich [Geister der] Pocken.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T9" id="Seg_1486" s="T0">В старину к человеку, живущему одиноко, вдруг вошли две женщины.</ta>
            <ta e="T12" id="Seg_1487" s="T9">Увидев их, человек тут же сказал:</ta>
            <ta e="T26" id="Seg_1488" s="T12">— Внимайте, о чем я пою, — и, лежа ничком, стал петь, когда запел, открылась будто дорога.</ta>
            <ta e="T31" id="Seg_1489" s="T26">Закончив петь, он сказал:</ta>
            <ta e="T36" id="Seg_1490" s="T31">— Вы идите по этой дороге — сказал.</ta>
            <ta e="T41" id="Seg_1491" s="T36">Женщины сразу ушли по дороге, указанной им.</ta>
            <ta e="T50" id="Seg_1492" s="T41">Через несколько дней вернулись к тому же человеку.</ta>
            <ta e="T55" id="Seg_1493" s="T50">— Что за диво, куда ты нас посылаешь?</ta>
            <ta e="T64" id="Seg_1494" s="T55">Там только тени, чуть не погибли от голода, — сказали.</ta>
            <ta e="T70" id="Seg_1495" s="T64">Этот человек, оказывается, имел трех собак.</ta>
            <ta e="T76" id="Seg_1496" s="T70">Заклиная, пожертвовал им этих собак:</ta>
            <ta e="T82" id="Seg_1497" s="T76">— Вот этот мой дар вам, — сказал.</ta>
            <ta e="T86" id="Seg_1498" s="T82">— Примите и уходите, — сказал.</ta>
            <ta e="T89" id="Seg_1499" s="T86">Женщины сразу ушли.</ta>
            <ta e="T100" id="Seg_1500" s="T89">Вот так простой человек, не шаман, спас от "матушек" свой наслег, говорят.</ta>
            <ta e="T104" id="Seg_1501" s="T100">Те женщины, оказывается, были духами оспы.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T9" id="Seg_1502" s="T0">В старину к человеку, живущему одиноко, вдруг вошли две женщины.</ta>
            <ta e="T12" id="Seg_1503" s="T9">Увидев их, человек тут же сказал:</ta>
            <ta e="T26" id="Seg_1504" s="T12">— Внимайте, о чем я пою, — и, лежа ничком, стал петь, когда запел, открылась будто дорога.</ta>
            <ta e="T31" id="Seg_1505" s="T26">Закончив петь, он сказал:</ta>
            <ta e="T36" id="Seg_1506" s="T31">— Вы идите по этой дороге.</ta>
            <ta e="T41" id="Seg_1507" s="T36">Женщины сразу ушли по дороге, указанной им.</ta>
            <ta e="T50" id="Seg_1508" s="T41">Через несколько дней вернулись к тому же человеку.</ta>
            <ta e="T55" id="Seg_1509" s="T50">— Что за диво, куда ты нас посылаешь?</ta>
            <ta e="T64" id="Seg_1510" s="T55">Там только тени, чуть не погибли от голода, — сказали.</ta>
            <ta e="T70" id="Seg_1511" s="T64">Этот человек, оказывается, имел трех собак.</ta>
            <ta e="T76" id="Seg_1512" s="T70">Заклиная, пожертвовал им этих собак:</ta>
            <ta e="T82" id="Seg_1513" s="T76">— Вот этот мой дар вам, — говорит.</ta>
            <ta e="T86" id="Seg_1514" s="T82">— Примите благосклонно и уходите, — говорит.</ta>
            <ta e="T89" id="Seg_1515" s="T86">Женщины сразу ушли.</ta>
            <ta e="T100" id="Seg_1516" s="T89">Вот так простой человек, не шаман, спас от "матушек" свой наслег, говорят.</ta>
            <ta e="T104" id="Seg_1517" s="T100">Те женщины, оказывается, были духами оспы.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T100" id="Seg_1518" s="T89">[DCh]: "maːtɨska" is a taboo expression for the masters of pox which according to Dolgan belief appear in the shape of Russian women.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg"
                          name="gg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top"
                          name="Top"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc"
                          name="Foc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
