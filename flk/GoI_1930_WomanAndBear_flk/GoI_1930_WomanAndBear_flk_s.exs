<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDE5B02D00-3959-EC22-3C0E-FA080D5AA2CF">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>GoI_1930_WomanAndBear_flk</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\flk\GoI_1930_WomanAndBear_flk\GoI_1930_WomanAndBear_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">202</ud-information>
            <ud-information attribute-name="# HIAT:w">160</ud-information>
            <ud-information attribute-name="# e">160</ud-information>
            <ud-information attribute-name="# HIAT:u">19</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="GoI">
            <abbreviation>GoI</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="4.0" type="appl" />
         <tli id="T1" time="4.5" type="appl" />
         <tli id="T2" time="5.0" type="appl" />
         <tli id="T3" time="5.5" type="appl" />
         <tli id="T4" time="6.0" type="appl" />
         <tli id="T5" time="6.5" type="appl" />
         <tli id="T6" time="7.0" type="appl" />
         <tli id="T7" time="7.5" type="appl" />
         <tli id="T8" time="8.0" type="appl" />
         <tli id="T9" time="8.5" type="appl" />
         <tli id="T10" time="9.0" type="appl" />
         <tli id="T11" time="9.5" type="appl" />
         <tli id="T12" time="10.0" type="appl" />
         <tli id="T13" time="10.5" type="appl" />
         <tli id="T14" time="11.0" type="appl" />
         <tli id="T15" time="11.5" type="appl" />
         <tli id="T16" time="12.0" type="appl" />
         <tli id="T17" time="12.5" type="appl" />
         <tli id="T18" time="13.0" type="appl" />
         <tli id="T19" time="13.5" type="appl" />
         <tli id="T20" time="14.0" type="appl" />
         <tli id="T21" time="14.5" type="appl" />
         <tli id="T22" time="15.0" type="appl" />
         <tli id="T23" time="15.5" type="appl" />
         <tli id="T24" time="16.0" type="appl" />
         <tli id="T25" time="16.5" type="appl" />
         <tli id="T26" time="17.0" type="appl" />
         <tli id="T27" time="17.5" type="appl" />
         <tli id="T28" time="18.0" type="appl" />
         <tli id="T29" time="18.5" type="appl" />
         <tli id="T30" time="19.0" type="appl" />
         <tli id="T31" time="19.5" type="appl" />
         <tli id="T32" time="20.0" type="appl" />
         <tli id="T33" time="20.5" type="appl" />
         <tli id="T34" time="21.0" type="appl" />
         <tli id="T35" time="21.5" type="appl" />
         <tli id="T36" time="22.0" type="appl" />
         <tli id="T37" time="22.5" type="appl" />
         <tli id="T38" time="23.0" type="appl" />
         <tli id="T39" time="23.5" type="appl" />
         <tli id="T40" time="24.0" type="appl" />
         <tli id="T41" time="24.5" type="appl" />
         <tli id="T42" time="25.0" type="appl" />
         <tli id="T43" time="25.5" type="appl" />
         <tli id="T44" time="26.0" type="appl" />
         <tli id="T45" time="26.5" type="appl" />
         <tli id="T46" time="27.0" type="appl" />
         <tli id="T47" time="27.5" type="appl" />
         <tli id="T48" time="28.0" type="appl" />
         <tli id="T49" time="28.5" type="appl" />
         <tli id="T50" time="29.0" type="appl" />
         <tli id="T51" time="29.5" type="appl" />
         <tli id="T52" time="30.0" type="appl" />
         <tli id="T53" time="30.5" type="appl" />
         <tli id="T54" time="31.0" type="appl" />
         <tli id="T55" time="31.5" type="appl" />
         <tli id="T56" time="32.0" type="appl" />
         <tli id="T57" time="32.5" type="appl" />
         <tli id="T58" time="33.0" type="appl" />
         <tli id="T59" time="33.5" type="appl" />
         <tli id="T60" time="34.0" type="appl" />
         <tli id="T61" time="34.5" type="appl" />
         <tli id="T62" time="35.0" type="appl" />
         <tli id="T63" time="35.5" type="appl" />
         <tli id="T64" time="36.0" type="appl" />
         <tli id="T65" time="36.5" type="appl" />
         <tli id="T66" time="37.0" type="appl" />
         <tli id="T67" time="37.5" type="appl" />
         <tli id="T68" time="38.0" type="appl" />
         <tli id="T69" time="38.5" type="appl" />
         <tli id="T70" time="39.0" type="appl" />
         <tli id="T71" time="39.5" type="appl" />
         <tli id="T72" time="40.0" type="appl" />
         <tli id="T73" time="40.5" type="appl" />
         <tli id="T74" time="41.0" type="appl" />
         <tli id="T75" time="41.5" type="appl" />
         <tli id="T76" time="42.0" type="appl" />
         <tli id="T77" time="42.5" type="appl" />
         <tli id="T78" time="43.0" type="appl" />
         <tli id="T79" time="43.5" type="appl" />
         <tli id="T80" time="44.0" type="appl" />
         <tli id="T81" time="44.5" type="appl" />
         <tli id="T82" time="45.0" type="appl" />
         <tli id="T83" time="45.5" type="appl" />
         <tli id="T84" time="46.0" type="appl" />
         <tli id="T85" time="46.5" type="appl" />
         <tli id="T86" time="47.0" type="appl" />
         <tli id="T87" time="47.5" type="appl" />
         <tli id="T88" time="48.0" type="appl" />
         <tli id="T89" time="48.5" type="appl" />
         <tli id="T90" time="49.0" type="appl" />
         <tli id="T91" time="49.5" type="appl" />
         <tli id="T92" time="50.0" type="appl" />
         <tli id="T93" time="50.5" type="appl" />
         <tli id="T94" time="51.0" type="appl" />
         <tli id="T95" time="51.5" type="appl" />
         <tli id="T96" time="52.0" type="appl" />
         <tli id="T97" time="52.5" type="appl" />
         <tli id="T98" time="53.0" type="appl" />
         <tli id="T99" time="53.5" type="appl" />
         <tli id="T100" time="54.0" type="appl" />
         <tli id="T101" time="54.5" type="appl" />
         <tli id="T102" time="55.0" type="appl" />
         <tli id="T103" time="55.5" type="appl" />
         <tli id="T104" time="56.0" type="appl" />
         <tli id="T105" time="56.5" type="appl" />
         <tli id="T106" time="57.0" type="appl" />
         <tli id="T107" time="57.5" type="appl" />
         <tli id="T108" time="58.0" type="appl" />
         <tli id="T109" time="58.5" type="appl" />
         <tli id="T110" time="59.0" type="appl" />
         <tli id="T111" time="59.5" type="appl" />
         <tli id="T112" time="60.0" type="appl" />
         <tli id="T113" time="60.5" type="appl" />
         <tli id="T114" time="61.0" type="appl" />
         <tli id="T115" time="61.5" type="appl" />
         <tli id="T116" time="62.0" type="appl" />
         <tli id="T117" time="62.5" type="appl" />
         <tli id="T118" time="63.0" type="appl" />
         <tli id="T119" time="63.5" type="appl" />
         <tli id="T120" time="64.0" type="appl" />
         <tli id="T121" time="64.5" type="appl" />
         <tli id="T122" time="65.0" type="appl" />
         <tli id="T123" time="65.5" type="appl" />
         <tli id="T124" time="66.0" type="appl" />
         <tli id="T125" time="66.5" type="appl" />
         <tli id="T126" time="67.0" type="appl" />
         <tli id="T127" time="67.5" type="appl" />
         <tli id="T128" time="68.0" type="appl" />
         <tli id="T129" time="68.5" type="appl" />
         <tli id="T130" time="69.0" type="appl" />
         <tli id="T131" time="69.5" type="appl" />
         <tli id="T132" time="70.0" type="appl" />
         <tli id="T133" time="70.5" type="appl" />
         <tli id="T134" time="71.0" type="appl" />
         <tli id="T135" time="71.5" type="appl" />
         <tli id="T136" time="72.0" type="appl" />
         <tli id="T137" time="72.5" type="appl" />
         <tli id="T138" time="73.0" type="appl" />
         <tli id="T139" time="73.5" type="appl" />
         <tli id="T140" time="74.0" type="appl" />
         <tli id="T141" time="74.5" type="appl" />
         <tli id="T142" time="75.0" type="appl" />
         <tli id="T143" time="75.5" type="appl" />
         <tli id="T144" time="76.0" type="appl" />
         <tli id="T145" time="76.5" type="appl" />
         <tli id="T146" time="77.0" type="appl" />
         <tli id="T147" time="77.5" type="appl" />
         <tli id="T148" time="78.0" type="appl" />
         <tli id="T149" time="78.5" type="appl" />
         <tli id="T150" time="79.0" type="appl" />
         <tli id="T151" time="79.5" type="appl" />
         <tli id="T152" time="80.0" type="appl" />
         <tli id="T153" time="80.5" type="appl" />
         <tli id="T154" time="81.0" type="appl" />
         <tli id="T155" time="81.5" type="appl" />
         <tli id="T156" time="82.0" type="appl" />
         <tli id="T157" time="82.5" type="appl" />
         <tli id="T158" time="83.0" type="appl" />
         <tli id="T159" time="83.5" type="appl" />
         <tli id="T160" time="84.0" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="GoI"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T160" id="Seg_0" n="sc" s="T0">
               <ts e="T8" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Bɨlɨr</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">biːr</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">bihikteːk</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">ogoloːk</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">dʼaktar</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">ojuːrga</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">mummut</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">kühün</ts>
                  <nts id="Seg_26" n="HIAT:ip">.</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T15" id="Seg_29" n="HIAT:u" s="T8">
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">Bu</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">hɨldʼan</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">hu͡olun</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_40" n="HIAT:w" s="T11">bulumna</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_43" n="HIAT:w" s="T12">ebeke</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_46" n="HIAT:w" s="T13">dʼi͡etiger</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_49" n="HIAT:w" s="T14">kelbit</ts>
                  <nts id="Seg_50" n="HIAT:ip">.</nts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_53" n="HIAT:u" s="T15">
                  <ts e="T16" id="Seg_55" n="HIAT:w" s="T15">Bu</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_58" n="HIAT:w" s="T16">kelen</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_61" n="HIAT:w" s="T17">kiːrer</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_64" n="HIAT:w" s="T18">daː</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_67" n="HIAT:w" s="T19">eter</ts>
                  <nts id="Seg_68" n="HIAT:ip">:</nts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T25" id="Seg_71" n="HIAT:u" s="T20">
                  <nts id="Seg_72" n="HIAT:ip">"</nts>
                  <ts e="T21" id="Seg_74" n="HIAT:w" s="T20">Dʼe</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_77" n="HIAT:w" s="T21">min</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_80" n="HIAT:w" s="T22">ölörüm</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_83" n="HIAT:w" s="T23">hin</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_86" n="HIAT:w" s="T24">biːr</ts>
                  <nts id="Seg_87" n="HIAT:ip">.</nts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T29" id="Seg_90" n="HIAT:u" s="T25">
                  <ts e="T26" id="Seg_92" n="HIAT:w" s="T25">Miːgin</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_95" n="HIAT:w" s="T26">hi͡ek</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_98" n="HIAT:w" s="T27">bu͡ollakkɨna</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_101" n="HIAT:w" s="T28">hi͡e</ts>
                  <nts id="Seg_102" n="HIAT:ip">.</nts>
                  <nts id="Seg_103" n="HIAT:ip">"</nts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T36" id="Seg_106" n="HIAT:u" s="T29">
                  <ts e="T30" id="Seg_108" n="HIAT:w" s="T29">Baː</ts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_111" n="HIAT:w" s="T30">ebekete</ts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_114" n="HIAT:w" s="T31">turan</ts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_117" n="HIAT:w" s="T32">manna</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_120" n="HIAT:w" s="T33">ulagaː</ts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_123" n="HIAT:w" s="T34">hɨgarɨjan</ts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_126" n="HIAT:w" s="T35">bi͡erer</ts>
                  <nts id="Seg_127" n="HIAT:ip">.</nts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T39" id="Seg_130" n="HIAT:u" s="T36">
                  <ts e="T37" id="Seg_132" n="HIAT:w" s="T36">Manna</ts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_135" n="HIAT:w" s="T37">dʼaktar</ts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_138" n="HIAT:w" s="T38">oloror</ts>
                  <nts id="Seg_139" n="HIAT:ip">.</nts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T46" id="Seg_142" n="HIAT:u" s="T39">
                  <ts e="T40" id="Seg_144" n="HIAT:w" s="T39">Manɨ</ts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_147" n="HIAT:w" s="T40">turan</ts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_150" n="HIAT:w" s="T41">kiniler</ts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_153" n="HIAT:w" s="T42">ebekeni</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_156" n="HIAT:w" s="T43">kɨtta</ts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_159" n="HIAT:w" s="T44">utujan</ts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_162" n="HIAT:w" s="T45">kaːlallar</ts>
                  <nts id="Seg_163" n="HIAT:ip">.</nts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T51" id="Seg_166" n="HIAT:u" s="T46">
                  <ts e="T47" id="Seg_168" n="HIAT:w" s="T46">Ebeke</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_171" n="HIAT:w" s="T47">ikki</ts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_174" n="HIAT:w" s="T48">tögülleːn</ts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_177" n="HIAT:w" s="T49">utujar</ts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_180" n="HIAT:w" s="T50">diːller</ts>
                  <nts id="Seg_181" n="HIAT:ip">.</nts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T56" id="Seg_184" n="HIAT:u" s="T51">
                  <ts e="T52" id="Seg_186" n="HIAT:w" s="T51">Dʼɨl</ts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_189" n="HIAT:w" s="T52">ortotugar</ts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_192" n="HIAT:w" s="T53">Miku͡oliŋŋa</ts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_195" n="HIAT:w" s="T54">uhuktar</ts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_198" n="HIAT:w" s="T55">diːller</ts>
                  <nts id="Seg_199" n="HIAT:ip">.</nts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T69" id="Seg_202" n="HIAT:u" s="T56">
                  <ts e="T57" id="Seg_204" n="HIAT:w" s="T56">Bu</ts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_207" n="HIAT:w" s="T57">uhuktubuta</ts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_210" n="HIAT:w" s="T58">dʼaktara</ts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_213" n="HIAT:w" s="T59">kini</ts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_216" n="HIAT:w" s="T60">tilegin</ts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_219" n="HIAT:w" s="T61">eme</ts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_222" n="HIAT:w" s="T62">hɨtar</ts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_225" n="HIAT:w" s="T63">bu͡olar</ts>
                  <nts id="Seg_226" n="HIAT:ip">,</nts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_229" n="HIAT:w" s="T64">onton</ts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_232" n="HIAT:w" s="T65">totor</ts>
                  <nts id="Seg_233" n="HIAT:ip">,</nts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_236" n="HIAT:w" s="T66">ogotun</ts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_239" n="HIAT:w" s="T67">da</ts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_242" n="HIAT:w" s="T68">totoror</ts>
                  <nts id="Seg_243" n="HIAT:ip">.</nts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T74" id="Seg_246" n="HIAT:u" s="T69">
                  <ts e="T70" id="Seg_248" n="HIAT:w" s="T69">Dʼe</ts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_251" n="HIAT:w" s="T70">ikki</ts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_254" n="HIAT:w" s="T71">tögül</ts>
                  <nts id="Seg_255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_257" n="HIAT:w" s="T72">utujannar</ts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_260" n="HIAT:w" s="T73">uhuktallar</ts>
                  <nts id="Seg_261" n="HIAT:ip">.</nts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T79" id="Seg_264" n="HIAT:u" s="T74">
                  <ts e="T75" id="Seg_266" n="HIAT:w" s="T74">Dʼaktar</ts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_269" n="HIAT:w" s="T75">baː</ts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_272" n="HIAT:w" s="T76">kɨːlɨn</ts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_275" n="HIAT:w" s="T77">kɨtta</ts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_278" n="HIAT:w" s="T78">taksallar</ts>
                  <nts id="Seg_279" n="HIAT:ip">.</nts>
                  <nts id="Seg_280" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T86" id="Seg_282" n="HIAT:u" s="T79">
                  <ts e="T80" id="Seg_284" n="HIAT:w" s="T79">Taksallar</ts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_287" n="HIAT:w" s="T80">daː</ts>
                  <nts id="Seg_288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_290" n="HIAT:w" s="T81">ebekete</ts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_293" n="HIAT:w" s="T82">ɨmɨjaktɨː</ts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_296" n="HIAT:w" s="T83">hɨldʼan</ts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_299" n="HIAT:w" s="T84">dʼaktarɨn</ts>
                  <nts id="Seg_300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_302" n="HIAT:w" s="T85">ahatar</ts>
                  <nts id="Seg_303" n="HIAT:ip">.</nts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T103" id="Seg_306" n="HIAT:u" s="T86">
                  <ts e="T87" id="Seg_308" n="HIAT:w" s="T86">Bu</ts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_311" n="HIAT:w" s="T87">hɨrɨttaktarɨna</ts>
                  <nts id="Seg_312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_314" n="HIAT:w" s="T88">kiniler</ts>
                  <nts id="Seg_315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_317" n="HIAT:w" s="T89">huːrtarɨttan</ts>
                  <nts id="Seg_318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_320" n="HIAT:w" s="T90">biːr</ts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_323" n="HIAT:w" s="T91">kihi</ts>
                  <nts id="Seg_324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_326" n="HIAT:w" s="T92">bultuː</ts>
                  <nts id="Seg_327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_329" n="HIAT:w" s="T93">hɨldʼan</ts>
                  <nts id="Seg_330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_332" n="HIAT:w" s="T94">körör</ts>
                  <nts id="Seg_333" n="HIAT:ip">,</nts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_336" n="HIAT:w" s="T95">körör</ts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_339" n="HIAT:w" s="T96">daː</ts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_342" n="HIAT:w" s="T97">baː</ts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_345" n="HIAT:w" s="T98">dʼaktarɨ</ts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_348" n="HIAT:w" s="T99">taːjar</ts>
                  <nts id="Seg_349" n="HIAT:ip">,</nts>
                  <nts id="Seg_350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_352" n="HIAT:w" s="T100">kepsiːr</ts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_355" n="HIAT:w" s="T101">dʼonnorugar</ts>
                  <nts id="Seg_356" n="HIAT:ip">,</nts>
                  <nts id="Seg_357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_359" n="HIAT:w" s="T102">eriger</ts>
                  <nts id="Seg_360" n="HIAT:ip">.</nts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T117" id="Seg_363" n="HIAT:u" s="T103">
                  <ts e="T104" id="Seg_365" n="HIAT:w" s="T103">Manɨ</ts>
                  <nts id="Seg_366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_368" n="HIAT:w" s="T104">turan</ts>
                  <nts id="Seg_369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_371" n="HIAT:w" s="T105">barɨ</ts>
                  <nts id="Seg_372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_374" n="HIAT:w" s="T106">dʼono</ts>
                  <nts id="Seg_375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_377" n="HIAT:w" s="T107">ü͡öŋen</ts>
                  <nts id="Seg_378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_380" n="HIAT:w" s="T108">kelen</ts>
                  <nts id="Seg_381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_383" n="HIAT:w" s="T109">turan</ts>
                  <nts id="Seg_384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_386" n="HIAT:w" s="T110">baran</ts>
                  <nts id="Seg_387" n="HIAT:ip">,</nts>
                  <nts id="Seg_388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_389" n="HIAT:ip">"</nts>
                  <ts e="T112" id="Seg_391" n="HIAT:w" s="T111">togo</ts>
                  <nts id="Seg_392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_394" n="HIAT:w" s="T112">ölörü͡ökpütüj</ts>
                  <nts id="Seg_395" n="HIAT:ip">"</nts>
                  <nts id="Seg_396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_398" n="HIAT:w" s="T113">di͡en</ts>
                  <nts id="Seg_399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_401" n="HIAT:w" s="T114">algɨːllar</ts>
                  <nts id="Seg_402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_404" n="HIAT:w" s="T115">bokulu͡omnuː-bokulu͡omnuː</ts>
                  <nts id="Seg_405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_407" n="HIAT:w" s="T116">ebekeni</ts>
                  <nts id="Seg_408" n="HIAT:ip">:</nts>
                  <nts id="Seg_409" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T133" id="Seg_411" n="HIAT:u" s="T117">
                  <nts id="Seg_412" n="HIAT:ip">"</nts>
                  <ts e="T118" id="Seg_414" n="HIAT:w" s="T117">Ebe</ts>
                  <nts id="Seg_415" n="HIAT:ip">,</nts>
                  <nts id="Seg_416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_418" n="HIAT:w" s="T118">kihini</ts>
                  <nts id="Seg_419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_421" n="HIAT:w" s="T119">bi͡eri͡ek</ts>
                  <nts id="Seg_422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_424" n="HIAT:w" s="T120">kemiŋ</ts>
                  <nts id="Seg_425" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_427" n="HIAT:w" s="T121">bu͡olla</ts>
                  <nts id="Seg_428" n="HIAT:ip">,</nts>
                  <nts id="Seg_429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_431" n="HIAT:w" s="T122">iːppikker</ts>
                  <nts id="Seg_432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_434" n="HIAT:w" s="T123">pasiːba</ts>
                  <nts id="Seg_435" n="HIAT:ip">,</nts>
                  <nts id="Seg_436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_438" n="HIAT:w" s="T124">dʼaktarɨ</ts>
                  <nts id="Seg_439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_441" n="HIAT:w" s="T125">tönnör</ts>
                  <nts id="Seg_442" n="HIAT:ip">,</nts>
                  <nts id="Seg_443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_445" n="HIAT:w" s="T126">kojut</ts>
                  <nts id="Seg_446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_448" n="HIAT:w" s="T127">ogogo</ts>
                  <nts id="Seg_449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_451" n="HIAT:w" s="T128">daː</ts>
                  <nts id="Seg_452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_454" n="HIAT:w" s="T129">kepsi͡ekpitin</ts>
                  <nts id="Seg_455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_457" n="HIAT:w" s="T130">en</ts>
                  <nts id="Seg_458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_460" n="HIAT:w" s="T131">iːppikkin</ts>
                  <nts id="Seg_461" n="HIAT:ip">"</nts>
                  <nts id="Seg_462" n="HIAT:ip">,</nts>
                  <nts id="Seg_463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_465" n="HIAT:w" s="T132">diːller</ts>
                  <nts id="Seg_466" n="HIAT:ip">.</nts>
                  <nts id="Seg_467" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T152" id="Seg_469" n="HIAT:u" s="T133">
                  <ts e="T134" id="Seg_471" n="HIAT:w" s="T133">Manɨ͡aga</ts>
                  <nts id="Seg_472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_474" n="HIAT:w" s="T134">ebekeː</ts>
                  <nts id="Seg_475" n="HIAT:ip">,</nts>
                  <nts id="Seg_476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_478" n="HIAT:w" s="T135">üːgülüːr</ts>
                  <nts id="Seg_479" n="HIAT:ip">,</nts>
                  <nts id="Seg_480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_482" n="HIAT:w" s="T136">köːkü</ts>
                  <nts id="Seg_483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_485" n="HIAT:w" s="T137">ögülüːr</ts>
                  <nts id="Seg_486" n="HIAT:ip">,</nts>
                  <nts id="Seg_487" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_489" n="HIAT:w" s="T138">köːkü</ts>
                  <nts id="Seg_490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_492" n="HIAT:w" s="T139">hürdeːk</ts>
                  <nts id="Seg_493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_495" n="HIAT:w" s="T140">bagajɨtɨk</ts>
                  <nts id="Seg_496" n="HIAT:ip">,</nts>
                  <nts id="Seg_497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_499" n="HIAT:w" s="T141">ol</ts>
                  <nts id="Seg_500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_502" n="HIAT:w" s="T142">gɨnan</ts>
                  <nts id="Seg_503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_505" n="HIAT:w" s="T143">baran</ts>
                  <nts id="Seg_506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_508" n="HIAT:w" s="T144">dʼaktarɨn</ts>
                  <nts id="Seg_509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_511" n="HIAT:w" s="T145">di͡eg</ts>
                  <nts id="Seg_512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_514" n="HIAT:w" s="T146">körör</ts>
                  <nts id="Seg_515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_517" n="HIAT:w" s="T147">daː</ts>
                  <nts id="Seg_518" n="HIAT:ip">,</nts>
                  <nts id="Seg_519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_521" n="HIAT:w" s="T148">dʼon</ts>
                  <nts id="Seg_522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_524" n="HIAT:w" s="T149">di͡eg</ts>
                  <nts id="Seg_525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_527" n="HIAT:w" s="T150">hapsɨjan</ts>
                  <nts id="Seg_528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_530" n="HIAT:w" s="T151">keːher</ts>
                  <nts id="Seg_531" n="HIAT:ip">.</nts>
                  <nts id="Seg_532" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T160" id="Seg_534" n="HIAT:u" s="T152">
                  <ts e="T153" id="Seg_536" n="HIAT:w" s="T152">Dʼaktara</ts>
                  <nts id="Seg_537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_539" n="HIAT:w" s="T153">dʼon</ts>
                  <nts id="Seg_540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_542" n="HIAT:w" s="T154">di͡eg</ts>
                  <nts id="Seg_543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_545" n="HIAT:w" s="T155">barar</ts>
                  <nts id="Seg_546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_548" n="HIAT:w" s="T156">daː</ts>
                  <nts id="Seg_549" n="HIAT:ip">,</nts>
                  <nts id="Seg_550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_552" n="HIAT:w" s="T157">ebekeːlere</ts>
                  <nts id="Seg_553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_555" n="HIAT:w" s="T158">hüːren</ts>
                  <nts id="Seg_556" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_558" n="HIAT:w" s="T159">kaːlar</ts>
                  <nts id="Seg_559" n="HIAT:ip">.</nts>
                  <nts id="Seg_560" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T160" id="Seg_561" n="sc" s="T0">
               <ts e="T1" id="Seg_563" n="e" s="T0">Bɨlɨr </ts>
               <ts e="T2" id="Seg_565" n="e" s="T1">biːr </ts>
               <ts e="T3" id="Seg_567" n="e" s="T2">bihikteːk </ts>
               <ts e="T4" id="Seg_569" n="e" s="T3">ogoloːk </ts>
               <ts e="T5" id="Seg_571" n="e" s="T4">dʼaktar </ts>
               <ts e="T6" id="Seg_573" n="e" s="T5">ojuːrga </ts>
               <ts e="T7" id="Seg_575" n="e" s="T6">mummut </ts>
               <ts e="T8" id="Seg_577" n="e" s="T7">kühün. </ts>
               <ts e="T9" id="Seg_579" n="e" s="T8">Bu </ts>
               <ts e="T10" id="Seg_581" n="e" s="T9">hɨldʼan </ts>
               <ts e="T11" id="Seg_583" n="e" s="T10">hu͡olun </ts>
               <ts e="T12" id="Seg_585" n="e" s="T11">bulumna </ts>
               <ts e="T13" id="Seg_587" n="e" s="T12">ebeke </ts>
               <ts e="T14" id="Seg_589" n="e" s="T13">dʼi͡etiger </ts>
               <ts e="T15" id="Seg_591" n="e" s="T14">kelbit. </ts>
               <ts e="T16" id="Seg_593" n="e" s="T15">Bu </ts>
               <ts e="T17" id="Seg_595" n="e" s="T16">kelen </ts>
               <ts e="T18" id="Seg_597" n="e" s="T17">kiːrer </ts>
               <ts e="T19" id="Seg_599" n="e" s="T18">daː </ts>
               <ts e="T20" id="Seg_601" n="e" s="T19">eter: </ts>
               <ts e="T21" id="Seg_603" n="e" s="T20">"Dʼe </ts>
               <ts e="T22" id="Seg_605" n="e" s="T21">min </ts>
               <ts e="T23" id="Seg_607" n="e" s="T22">ölörüm </ts>
               <ts e="T24" id="Seg_609" n="e" s="T23">hin </ts>
               <ts e="T25" id="Seg_611" n="e" s="T24">biːr. </ts>
               <ts e="T26" id="Seg_613" n="e" s="T25">Miːgin </ts>
               <ts e="T27" id="Seg_615" n="e" s="T26">hi͡ek </ts>
               <ts e="T28" id="Seg_617" n="e" s="T27">bu͡ollakkɨna </ts>
               <ts e="T29" id="Seg_619" n="e" s="T28">hi͡e." </ts>
               <ts e="T30" id="Seg_621" n="e" s="T29">Baː </ts>
               <ts e="T31" id="Seg_623" n="e" s="T30">ebekete </ts>
               <ts e="T32" id="Seg_625" n="e" s="T31">turan </ts>
               <ts e="T33" id="Seg_627" n="e" s="T32">manna </ts>
               <ts e="T34" id="Seg_629" n="e" s="T33">ulagaː </ts>
               <ts e="T35" id="Seg_631" n="e" s="T34">hɨgarɨjan </ts>
               <ts e="T36" id="Seg_633" n="e" s="T35">bi͡erer. </ts>
               <ts e="T37" id="Seg_635" n="e" s="T36">Manna </ts>
               <ts e="T38" id="Seg_637" n="e" s="T37">dʼaktar </ts>
               <ts e="T39" id="Seg_639" n="e" s="T38">oloror. </ts>
               <ts e="T40" id="Seg_641" n="e" s="T39">Manɨ </ts>
               <ts e="T41" id="Seg_643" n="e" s="T40">turan </ts>
               <ts e="T42" id="Seg_645" n="e" s="T41">kiniler </ts>
               <ts e="T43" id="Seg_647" n="e" s="T42">ebekeni </ts>
               <ts e="T44" id="Seg_649" n="e" s="T43">kɨtta </ts>
               <ts e="T45" id="Seg_651" n="e" s="T44">utujan </ts>
               <ts e="T46" id="Seg_653" n="e" s="T45">kaːlallar. </ts>
               <ts e="T47" id="Seg_655" n="e" s="T46">Ebeke </ts>
               <ts e="T48" id="Seg_657" n="e" s="T47">ikki </ts>
               <ts e="T49" id="Seg_659" n="e" s="T48">tögülleːn </ts>
               <ts e="T50" id="Seg_661" n="e" s="T49">utujar </ts>
               <ts e="T51" id="Seg_663" n="e" s="T50">diːller. </ts>
               <ts e="T52" id="Seg_665" n="e" s="T51">Dʼɨl </ts>
               <ts e="T53" id="Seg_667" n="e" s="T52">ortotugar </ts>
               <ts e="T54" id="Seg_669" n="e" s="T53">Miku͡oliŋŋa </ts>
               <ts e="T55" id="Seg_671" n="e" s="T54">uhuktar </ts>
               <ts e="T56" id="Seg_673" n="e" s="T55">diːller. </ts>
               <ts e="T57" id="Seg_675" n="e" s="T56">Bu </ts>
               <ts e="T58" id="Seg_677" n="e" s="T57">uhuktubuta </ts>
               <ts e="T59" id="Seg_679" n="e" s="T58">dʼaktara </ts>
               <ts e="T60" id="Seg_681" n="e" s="T59">kini </ts>
               <ts e="T61" id="Seg_683" n="e" s="T60">tilegin </ts>
               <ts e="T62" id="Seg_685" n="e" s="T61">eme </ts>
               <ts e="T63" id="Seg_687" n="e" s="T62">hɨtar </ts>
               <ts e="T64" id="Seg_689" n="e" s="T63">bu͡olar, </ts>
               <ts e="T65" id="Seg_691" n="e" s="T64">onton </ts>
               <ts e="T66" id="Seg_693" n="e" s="T65">totor, </ts>
               <ts e="T67" id="Seg_695" n="e" s="T66">ogotun </ts>
               <ts e="T68" id="Seg_697" n="e" s="T67">da </ts>
               <ts e="T69" id="Seg_699" n="e" s="T68">totoror. </ts>
               <ts e="T70" id="Seg_701" n="e" s="T69">Dʼe </ts>
               <ts e="T71" id="Seg_703" n="e" s="T70">ikki </ts>
               <ts e="T72" id="Seg_705" n="e" s="T71">tögül </ts>
               <ts e="T73" id="Seg_707" n="e" s="T72">utujannar </ts>
               <ts e="T74" id="Seg_709" n="e" s="T73">uhuktallar. </ts>
               <ts e="T75" id="Seg_711" n="e" s="T74">Dʼaktar </ts>
               <ts e="T76" id="Seg_713" n="e" s="T75">baː </ts>
               <ts e="T77" id="Seg_715" n="e" s="T76">kɨːlɨn </ts>
               <ts e="T78" id="Seg_717" n="e" s="T77">kɨtta </ts>
               <ts e="T79" id="Seg_719" n="e" s="T78">taksallar. </ts>
               <ts e="T80" id="Seg_721" n="e" s="T79">Taksallar </ts>
               <ts e="T81" id="Seg_723" n="e" s="T80">daː </ts>
               <ts e="T82" id="Seg_725" n="e" s="T81">ebekete </ts>
               <ts e="T83" id="Seg_727" n="e" s="T82">ɨmɨjaktɨː </ts>
               <ts e="T84" id="Seg_729" n="e" s="T83">hɨldʼan </ts>
               <ts e="T85" id="Seg_731" n="e" s="T84">dʼaktarɨn </ts>
               <ts e="T86" id="Seg_733" n="e" s="T85">ahatar. </ts>
               <ts e="T87" id="Seg_735" n="e" s="T86">Bu </ts>
               <ts e="T88" id="Seg_737" n="e" s="T87">hɨrɨttaktarɨna </ts>
               <ts e="T89" id="Seg_739" n="e" s="T88">kiniler </ts>
               <ts e="T90" id="Seg_741" n="e" s="T89">huːrtarɨttan </ts>
               <ts e="T91" id="Seg_743" n="e" s="T90">biːr </ts>
               <ts e="T92" id="Seg_745" n="e" s="T91">kihi </ts>
               <ts e="T93" id="Seg_747" n="e" s="T92">bultuː </ts>
               <ts e="T94" id="Seg_749" n="e" s="T93">hɨldʼan </ts>
               <ts e="T95" id="Seg_751" n="e" s="T94">körör, </ts>
               <ts e="T96" id="Seg_753" n="e" s="T95">körör </ts>
               <ts e="T97" id="Seg_755" n="e" s="T96">daː </ts>
               <ts e="T98" id="Seg_757" n="e" s="T97">baː </ts>
               <ts e="T99" id="Seg_759" n="e" s="T98">dʼaktarɨ </ts>
               <ts e="T100" id="Seg_761" n="e" s="T99">taːjar, </ts>
               <ts e="T101" id="Seg_763" n="e" s="T100">kepsiːr </ts>
               <ts e="T102" id="Seg_765" n="e" s="T101">dʼonnorugar, </ts>
               <ts e="T103" id="Seg_767" n="e" s="T102">eriger. </ts>
               <ts e="T104" id="Seg_769" n="e" s="T103">Manɨ </ts>
               <ts e="T105" id="Seg_771" n="e" s="T104">turan </ts>
               <ts e="T106" id="Seg_773" n="e" s="T105">barɨ </ts>
               <ts e="T107" id="Seg_775" n="e" s="T106">dʼono </ts>
               <ts e="T108" id="Seg_777" n="e" s="T107">ü͡öŋen </ts>
               <ts e="T109" id="Seg_779" n="e" s="T108">kelen </ts>
               <ts e="T110" id="Seg_781" n="e" s="T109">turan </ts>
               <ts e="T111" id="Seg_783" n="e" s="T110">baran, </ts>
               <ts e="T112" id="Seg_785" n="e" s="T111">"togo </ts>
               <ts e="T113" id="Seg_787" n="e" s="T112">ölörü͡ökpütüj" </ts>
               <ts e="T114" id="Seg_789" n="e" s="T113">di͡en </ts>
               <ts e="T115" id="Seg_791" n="e" s="T114">algɨːllar </ts>
               <ts e="T116" id="Seg_793" n="e" s="T115">bokulu͡omnuː-bokulu͡omnuː </ts>
               <ts e="T117" id="Seg_795" n="e" s="T116">ebekeni: </ts>
               <ts e="T118" id="Seg_797" n="e" s="T117">"Ebe, </ts>
               <ts e="T119" id="Seg_799" n="e" s="T118">kihini </ts>
               <ts e="T120" id="Seg_801" n="e" s="T119">bi͡eri͡ek </ts>
               <ts e="T121" id="Seg_803" n="e" s="T120">kemiŋ </ts>
               <ts e="T122" id="Seg_805" n="e" s="T121">bu͡olla, </ts>
               <ts e="T123" id="Seg_807" n="e" s="T122">iːppikker </ts>
               <ts e="T124" id="Seg_809" n="e" s="T123">pasiːba, </ts>
               <ts e="T125" id="Seg_811" n="e" s="T124">dʼaktarɨ </ts>
               <ts e="T126" id="Seg_813" n="e" s="T125">tönnör, </ts>
               <ts e="T127" id="Seg_815" n="e" s="T126">kojut </ts>
               <ts e="T128" id="Seg_817" n="e" s="T127">ogogo </ts>
               <ts e="T129" id="Seg_819" n="e" s="T128">daː </ts>
               <ts e="T130" id="Seg_821" n="e" s="T129">kepsi͡ekpitin </ts>
               <ts e="T131" id="Seg_823" n="e" s="T130">en </ts>
               <ts e="T132" id="Seg_825" n="e" s="T131">iːppikkin", </ts>
               <ts e="T133" id="Seg_827" n="e" s="T132">diːller. </ts>
               <ts e="T134" id="Seg_829" n="e" s="T133">Manɨ͡aga </ts>
               <ts e="T135" id="Seg_831" n="e" s="T134">ebekeː, </ts>
               <ts e="T136" id="Seg_833" n="e" s="T135">üːgülüːr, </ts>
               <ts e="T137" id="Seg_835" n="e" s="T136">köːkü </ts>
               <ts e="T138" id="Seg_837" n="e" s="T137">ögülüːr, </ts>
               <ts e="T139" id="Seg_839" n="e" s="T138">köːkü </ts>
               <ts e="T140" id="Seg_841" n="e" s="T139">hürdeːk </ts>
               <ts e="T141" id="Seg_843" n="e" s="T140">bagajɨtɨk, </ts>
               <ts e="T142" id="Seg_845" n="e" s="T141">ol </ts>
               <ts e="T143" id="Seg_847" n="e" s="T142">gɨnan </ts>
               <ts e="T144" id="Seg_849" n="e" s="T143">baran </ts>
               <ts e="T145" id="Seg_851" n="e" s="T144">dʼaktarɨn </ts>
               <ts e="T146" id="Seg_853" n="e" s="T145">di͡eg </ts>
               <ts e="T147" id="Seg_855" n="e" s="T146">körör </ts>
               <ts e="T148" id="Seg_857" n="e" s="T147">daː, </ts>
               <ts e="T149" id="Seg_859" n="e" s="T148">dʼon </ts>
               <ts e="T150" id="Seg_861" n="e" s="T149">di͡eg </ts>
               <ts e="T151" id="Seg_863" n="e" s="T150">hapsɨjan </ts>
               <ts e="T152" id="Seg_865" n="e" s="T151">keːher. </ts>
               <ts e="T153" id="Seg_867" n="e" s="T152">Dʼaktara </ts>
               <ts e="T154" id="Seg_869" n="e" s="T153">dʼon </ts>
               <ts e="T155" id="Seg_871" n="e" s="T154">di͡eg </ts>
               <ts e="T156" id="Seg_873" n="e" s="T155">barar </ts>
               <ts e="T157" id="Seg_875" n="e" s="T156">daː, </ts>
               <ts e="T158" id="Seg_877" n="e" s="T157">ebekeːlere </ts>
               <ts e="T159" id="Seg_879" n="e" s="T158">hüːren </ts>
               <ts e="T160" id="Seg_881" n="e" s="T159">kaːlar. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T8" id="Seg_882" s="T0">GoI_1930_WomanAndBear_flk.001 (001.001)</ta>
            <ta e="T15" id="Seg_883" s="T8">GoI_1930_WomanAndBear_flk.002 (001.002)</ta>
            <ta e="T20" id="Seg_884" s="T15">GoI_1930_WomanAndBear_flk.003 (001.003)</ta>
            <ta e="T25" id="Seg_885" s="T20">GoI_1930_WomanAndBear_flk.004 (001.004)</ta>
            <ta e="T29" id="Seg_886" s="T25">GoI_1930_WomanAndBear_flk.005 (001.005)</ta>
            <ta e="T36" id="Seg_887" s="T29">GoI_1930_WomanAndBear_flk.006 (001.006)</ta>
            <ta e="T39" id="Seg_888" s="T36">GoI_1930_WomanAndBear_flk.007 (001.007)</ta>
            <ta e="T46" id="Seg_889" s="T39">GoI_1930_WomanAndBear_flk.008 (001.008)</ta>
            <ta e="T51" id="Seg_890" s="T46">GoI_1930_WomanAndBear_flk.009 (001.009)</ta>
            <ta e="T56" id="Seg_891" s="T51">GoI_1930_WomanAndBear_flk.010 (001.010)</ta>
            <ta e="T69" id="Seg_892" s="T56">GoI_1930_WomanAndBear_flk.011 (001.011)</ta>
            <ta e="T74" id="Seg_893" s="T69">GoI_1930_WomanAndBear_flk.012 (001.012)</ta>
            <ta e="T79" id="Seg_894" s="T74">GoI_1930_WomanAndBear_flk.013 (001.013)</ta>
            <ta e="T86" id="Seg_895" s="T79">GoI_1930_WomanAndBear_flk.014 (001.014)</ta>
            <ta e="T103" id="Seg_896" s="T86">GoI_1930_WomanAndBear_flk.015 (001.015)</ta>
            <ta e="T117" id="Seg_897" s="T103">GoI_1930_WomanAndBear_flk.016 (001.016)</ta>
            <ta e="T133" id="Seg_898" s="T117">GoI_1930_WomanAndBear_flk.017 (001.017)</ta>
            <ta e="T152" id="Seg_899" s="T133">GoI_1930_WomanAndBear_flk.018 (001.018)</ta>
            <ta e="T160" id="Seg_900" s="T152">GoI_1930_WomanAndBear_flk.019 (001.019)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T8" id="Seg_901" s="T0">Былыр биир биһиктээк оголоок дьактар ойуурга муммут күһүн.</ta>
            <ta e="T15" id="Seg_902" s="T8">Бу һылдьан һуолун булумна эбэкэ дьиэтигэр кэлбит.</ta>
            <ta e="T20" id="Seg_903" s="T15">Бу кэлэн киирэр даа этэр:</ta>
            <ta e="T25" id="Seg_904" s="T20">— Дьэ мин өлөрүм һин биир.</ta>
            <ta e="T29" id="Seg_905" s="T25">Миигин һиэк буоллаккына һиэ.</ta>
            <ta e="T36" id="Seg_906" s="T29">Баа эбэкэтэ туран манна улагаа һыгарыйан биэрэр.</ta>
            <ta e="T39" id="Seg_907" s="T36">Манна дьактар олорор.</ta>
            <ta e="T46" id="Seg_908" s="T39">Маны туран кинилэр эбэкэни кытта утуйан каалаллар.</ta>
            <ta e="T51" id="Seg_909" s="T46">Эбэкэ икки төгүллээн утуйар дииллэр.</ta>
            <ta e="T56" id="Seg_910" s="T51">Дьыл ортотугар Микуолиҥҥа уһуктар дииллэр.</ta>
            <ta e="T69" id="Seg_911" s="T56">Бу уһуктубута дьактара кини тилэгин эмэ һытар буолар, онтон тотор, оготун да тоторор.</ta>
            <ta e="T74" id="Seg_912" s="T69">Дьэ икки төгүл утуйаннар уһукталлар.</ta>
            <ta e="T79" id="Seg_913" s="T74">Дьактар баа кыылын кытта таксаллар.</ta>
            <ta e="T86" id="Seg_914" s="T79">Таксаллар даа эбэкэтэ ымыйактыы һылдьан дьактарын аһатар.</ta>
            <ta e="T103" id="Seg_915" s="T86">Бу һырыттактарына кинилэр һууртарыттан биир киһи бултуу һылдьан көрөр, көрөр даа баа дьактары таайар, кэпсиир дьонноругар, эригэр.</ta>
            <ta e="T117" id="Seg_916" s="T103">Маны туран бары дьоно үөҥэн кэлэн туран баран, "того өлөрүөкпүтүй?" диэн алгыыллар бокулуомнуу-бокулуомнуу эбэкэни:</ta>
            <ta e="T133" id="Seg_917" s="T117">— Эбэ, киһини биэриэк кэмиҥ буолла, ииппиккэр пасииба, дьактары төннөр, койут огого даа кэпсиэкпитин эн ииппиккин, — дииллэр.</ta>
            <ta e="T152" id="Seg_918" s="T133">Маныага эбэкээ үгүлүүр, көөкү өгүлүүр, көөкү һүрдээк багайытык, ол гынан баран дьактарын диэг көрөр даа, дьон диэг һапсыйан кээһэр.</ta>
            <ta e="T160" id="Seg_919" s="T152">Дьактара дьон диэг барар даа, эбэкээлэрэ һүүрэн каалар.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T8" id="Seg_920" s="T0">Bɨlɨr biːr bihikteːk ogoloːk dʼaktar ojuːrga mummut kühün. </ta>
            <ta e="T15" id="Seg_921" s="T8">Bu hɨldʼan hu͡olun bulumna ebeke dʼi͡etiger kelbit. </ta>
            <ta e="T20" id="Seg_922" s="T15">Bu kelen kiːrer daː eter:</ta>
            <ta e="T25" id="Seg_923" s="T20">"Dʼe min ölörüm hin biːr. </ta>
            <ta e="T29" id="Seg_924" s="T25">Miːgin hi͡ek bu͡ollakkɨna hi͡e." </ta>
            <ta e="T36" id="Seg_925" s="T29">Baː ebekete turan manna ulagaː hɨgarɨjan bi͡erer. </ta>
            <ta e="T39" id="Seg_926" s="T36">Manna dʼaktar oloror. </ta>
            <ta e="T46" id="Seg_927" s="T39">Manɨ turan kiniler ebekeni kɨtta utujan kaːlallar. </ta>
            <ta e="T51" id="Seg_928" s="T46">Ebeke ikki tögülleːn utujar diːller. </ta>
            <ta e="T56" id="Seg_929" s="T51">Dʼɨl ortotugar Miku͡oliŋŋa uhuktar diːller. </ta>
            <ta e="T69" id="Seg_930" s="T56">Bu uhuktubuta dʼaktara kini tilegin eme hɨtar bu͡olar, onton totor, ogotun da totoror. </ta>
            <ta e="T74" id="Seg_931" s="T69">Dʼe ikki tögül utujannar uhuktallar. </ta>
            <ta e="T79" id="Seg_932" s="T74">Dʼaktar baː kɨːlɨn kɨtta taksallar. </ta>
            <ta e="T86" id="Seg_933" s="T79">Taksallar daː ebekete ɨmɨjaktɨː hɨldʼan dʼaktarɨn ahatar. </ta>
            <ta e="T103" id="Seg_934" s="T86">Bu hɨrɨttaktarɨna kiniler huːrtarɨttan biːr kihi bultuː hɨldʼan körör, körör daː baː dʼaktarɨ taːjar, kepsiːr dʼonnorugar, eriger. </ta>
            <ta e="T117" id="Seg_935" s="T103">Manɨ turan barɨ dʼono ü͡öŋen kelen turan baran, "togo ölörü͡ökpütüj" di͡en algɨːllar bokulu͡omnuː-bokulu͡omnuː ebekeni: </ta>
            <ta e="T133" id="Seg_936" s="T117">"Ebe, kihini bi͡eri͡ek kemiŋ bu͡olla, iːppikker pasiːba, dʼaktarɨ tönnör, kojut ogogo daː kepsi͡ekpitin en iːppikkin", diːller. </ta>
            <ta e="T152" id="Seg_937" s="T133">Manɨ͡aga ebekeː, üːgülüːr, köːkü ögülüːr, köːkü hürdeːk bagajɨtɨk, ol gɨnan baran dʼaktarɨn di͡eg körör daː, dʼon di͡eg hapsɨjan keːher. </ta>
            <ta e="T160" id="Seg_938" s="T152">Dʼaktara dʼon di͡eg barar daː, ebekeːlere hüːren kaːlar. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_939" s="T0">bɨlɨr</ta>
            <ta e="T2" id="Seg_940" s="T1">biːr</ta>
            <ta e="T3" id="Seg_941" s="T2">bihik-teːk</ta>
            <ta e="T4" id="Seg_942" s="T3">ogo-loːk</ta>
            <ta e="T5" id="Seg_943" s="T4">dʼaktar</ta>
            <ta e="T6" id="Seg_944" s="T5">ojuːr-ga</ta>
            <ta e="T7" id="Seg_945" s="T6">mum-mut</ta>
            <ta e="T8" id="Seg_946" s="T7">kühün</ta>
            <ta e="T9" id="Seg_947" s="T8">bu</ta>
            <ta e="T10" id="Seg_948" s="T9">hɨldʼ-an</ta>
            <ta e="T11" id="Seg_949" s="T10">hu͡ol-u-n</ta>
            <ta e="T12" id="Seg_950" s="T11">bul-u-mna</ta>
            <ta e="T13" id="Seg_951" s="T12">ebeke</ta>
            <ta e="T14" id="Seg_952" s="T13">dʼi͡e-ti-ger</ta>
            <ta e="T15" id="Seg_953" s="T14">kel-bit</ta>
            <ta e="T16" id="Seg_954" s="T15">bu</ta>
            <ta e="T17" id="Seg_955" s="T16">kel-en</ta>
            <ta e="T18" id="Seg_956" s="T17">kiːr-er</ta>
            <ta e="T19" id="Seg_957" s="T18">daː</ta>
            <ta e="T20" id="Seg_958" s="T19">et-er</ta>
            <ta e="T21" id="Seg_959" s="T20">dʼe</ta>
            <ta e="T22" id="Seg_960" s="T21">min</ta>
            <ta e="T23" id="Seg_961" s="T22">öl-ör-ü-m</ta>
            <ta e="T24" id="Seg_962" s="T23">hin</ta>
            <ta e="T25" id="Seg_963" s="T24">biːr</ta>
            <ta e="T26" id="Seg_964" s="T25">miːgi-n</ta>
            <ta e="T27" id="Seg_965" s="T26">h-i͡ek</ta>
            <ta e="T28" id="Seg_966" s="T27">bu͡ol-lak-kɨna</ta>
            <ta e="T29" id="Seg_967" s="T28">hi͡e</ta>
            <ta e="T30" id="Seg_968" s="T29">baː</ta>
            <ta e="T31" id="Seg_969" s="T30">ebeke-te</ta>
            <ta e="T32" id="Seg_970" s="T31">tur-an</ta>
            <ta e="T33" id="Seg_971" s="T32">manna</ta>
            <ta e="T34" id="Seg_972" s="T33">ulagaː</ta>
            <ta e="T35" id="Seg_973" s="T34">hɨgarɨj-an</ta>
            <ta e="T36" id="Seg_974" s="T35">bi͡er-er</ta>
            <ta e="T37" id="Seg_975" s="T36">manna</ta>
            <ta e="T38" id="Seg_976" s="T37">dʼaktar</ta>
            <ta e="T39" id="Seg_977" s="T38">olor-or</ta>
            <ta e="T40" id="Seg_978" s="T39">ma-nɨ</ta>
            <ta e="T41" id="Seg_979" s="T40">tur-an</ta>
            <ta e="T42" id="Seg_980" s="T41">kiniler</ta>
            <ta e="T43" id="Seg_981" s="T42">ebeke-ni</ta>
            <ta e="T44" id="Seg_982" s="T43">kɨtta</ta>
            <ta e="T45" id="Seg_983" s="T44">utuj-an</ta>
            <ta e="T46" id="Seg_984" s="T45">kaːl-al-lar</ta>
            <ta e="T47" id="Seg_985" s="T46">ebeke</ta>
            <ta e="T48" id="Seg_986" s="T47">ikki</ta>
            <ta e="T49" id="Seg_987" s="T48">tögül-leː-n</ta>
            <ta e="T50" id="Seg_988" s="T49">utuj-ar</ta>
            <ta e="T51" id="Seg_989" s="T50">diː-l-ler</ta>
            <ta e="T52" id="Seg_990" s="T51">dʼɨl</ta>
            <ta e="T53" id="Seg_991" s="T52">orto-tu-gar</ta>
            <ta e="T54" id="Seg_992" s="T53">Miku͡oliŋ-ŋa</ta>
            <ta e="T55" id="Seg_993" s="T54">uhukt-ar</ta>
            <ta e="T56" id="Seg_994" s="T55">diː-l-ler</ta>
            <ta e="T57" id="Seg_995" s="T56">bu</ta>
            <ta e="T58" id="Seg_996" s="T57">uhukt-u-but-a</ta>
            <ta e="T59" id="Seg_997" s="T58">dʼaktar-a</ta>
            <ta e="T60" id="Seg_998" s="T59">kini</ta>
            <ta e="T61" id="Seg_999" s="T60">tileg-i-n</ta>
            <ta e="T62" id="Seg_1000" s="T61">em-e</ta>
            <ta e="T63" id="Seg_1001" s="T62">hɨt-ar</ta>
            <ta e="T64" id="Seg_1002" s="T63">bu͡ol-ar</ta>
            <ta e="T65" id="Seg_1003" s="T64">onton</ta>
            <ta e="T66" id="Seg_1004" s="T65">tot-or</ta>
            <ta e="T67" id="Seg_1005" s="T66">ogo-tu-n</ta>
            <ta e="T68" id="Seg_1006" s="T67">da</ta>
            <ta e="T69" id="Seg_1007" s="T68">tot-o-r-or</ta>
            <ta e="T70" id="Seg_1008" s="T69">dʼe</ta>
            <ta e="T71" id="Seg_1009" s="T70">ikki</ta>
            <ta e="T72" id="Seg_1010" s="T71">tögül</ta>
            <ta e="T73" id="Seg_1011" s="T72">utuj-an-nar</ta>
            <ta e="T74" id="Seg_1012" s="T73">uhukt-al-lar</ta>
            <ta e="T75" id="Seg_1013" s="T74">dʼaktar</ta>
            <ta e="T76" id="Seg_1014" s="T75">baː</ta>
            <ta e="T77" id="Seg_1015" s="T76">kɨːl-ɨ-n</ta>
            <ta e="T78" id="Seg_1016" s="T77">kɨtta</ta>
            <ta e="T79" id="Seg_1017" s="T78">taks-al-lar</ta>
            <ta e="T80" id="Seg_1018" s="T79">taks-al-lar</ta>
            <ta e="T81" id="Seg_1019" s="T80">daː</ta>
            <ta e="T82" id="Seg_1020" s="T81">ebeke-te</ta>
            <ta e="T83" id="Seg_1021" s="T82">ɨmɨjak-t-ɨː</ta>
            <ta e="T84" id="Seg_1022" s="T83">hɨldʼ-an</ta>
            <ta e="T85" id="Seg_1023" s="T84">dʼaktar-ɨ-n</ta>
            <ta e="T86" id="Seg_1024" s="T85">ah-a-t-ar</ta>
            <ta e="T87" id="Seg_1025" s="T86">bu</ta>
            <ta e="T88" id="Seg_1026" s="T87">hɨrɨt-tak-tarɨna</ta>
            <ta e="T89" id="Seg_1027" s="T88">kiniler</ta>
            <ta e="T90" id="Seg_1028" s="T89">huːr-tarɨ-ttan</ta>
            <ta e="T91" id="Seg_1029" s="T90">biːr</ta>
            <ta e="T92" id="Seg_1030" s="T91">kihi</ta>
            <ta e="T93" id="Seg_1031" s="T92">bult-uː</ta>
            <ta e="T94" id="Seg_1032" s="T93">hɨldʼ-an</ta>
            <ta e="T95" id="Seg_1033" s="T94">kör-ör</ta>
            <ta e="T96" id="Seg_1034" s="T95">kör-ör</ta>
            <ta e="T97" id="Seg_1035" s="T96">daː</ta>
            <ta e="T98" id="Seg_1036" s="T97">baː</ta>
            <ta e="T99" id="Seg_1037" s="T98">dʼaktar-ɨ</ta>
            <ta e="T100" id="Seg_1038" s="T99">taːj-ar</ta>
            <ta e="T101" id="Seg_1039" s="T100">kepsiː-r</ta>
            <ta e="T102" id="Seg_1040" s="T101">dʼon-nor-u-gar</ta>
            <ta e="T103" id="Seg_1041" s="T102">er-i-ger</ta>
            <ta e="T104" id="Seg_1042" s="T103">ma-nɨ</ta>
            <ta e="T105" id="Seg_1043" s="T104">tur-an</ta>
            <ta e="T106" id="Seg_1044" s="T105">barɨ</ta>
            <ta e="T107" id="Seg_1045" s="T106">dʼon-o</ta>
            <ta e="T108" id="Seg_1046" s="T107">ü͡öŋ-en</ta>
            <ta e="T109" id="Seg_1047" s="T108">kel-en</ta>
            <ta e="T110" id="Seg_1048" s="T109">tur-an</ta>
            <ta e="T111" id="Seg_1049" s="T110">baran</ta>
            <ta e="T112" id="Seg_1050" s="T111">togo</ta>
            <ta e="T113" id="Seg_1051" s="T112">ölör-ü͡ök-püt=üj</ta>
            <ta e="T114" id="Seg_1052" s="T113">di͡e-n</ta>
            <ta e="T115" id="Seg_1053" s="T114">algɨː-l-lar</ta>
            <ta e="T116" id="Seg_1054" s="T115">bokulu͡omn-uː-bokulu͡omn-uː</ta>
            <ta e="T117" id="Seg_1055" s="T116">ebeke-ni</ta>
            <ta e="T118" id="Seg_1056" s="T117">ebe</ta>
            <ta e="T119" id="Seg_1057" s="T118">kihi-ni</ta>
            <ta e="T120" id="Seg_1058" s="T119">bi͡er-i͡ek</ta>
            <ta e="T121" id="Seg_1059" s="T120">kem-i-ŋ</ta>
            <ta e="T122" id="Seg_1060" s="T121">bu͡ol-l-a</ta>
            <ta e="T123" id="Seg_1061" s="T122">iːp-pik-ke-r</ta>
            <ta e="T124" id="Seg_1062" s="T123">pasiːba</ta>
            <ta e="T125" id="Seg_1063" s="T124">dʼaktar-ɨ</ta>
            <ta e="T126" id="Seg_1064" s="T125">tönn-ö-r</ta>
            <ta e="T127" id="Seg_1065" s="T126">kojut</ta>
            <ta e="T128" id="Seg_1066" s="T127">ogo-go</ta>
            <ta e="T129" id="Seg_1067" s="T128">daː</ta>
            <ta e="T130" id="Seg_1068" s="T129">keps-i͡ek-piti-n</ta>
            <ta e="T131" id="Seg_1069" s="T130">en</ta>
            <ta e="T132" id="Seg_1070" s="T131">iːp-pik-ki-n</ta>
            <ta e="T133" id="Seg_1071" s="T132">diː-l-ler</ta>
            <ta e="T134" id="Seg_1072" s="T133">manɨ͡a-ga</ta>
            <ta e="T135" id="Seg_1073" s="T134">ebekeː</ta>
            <ta e="T136" id="Seg_1074" s="T135">üːgülüː-r</ta>
            <ta e="T137" id="Seg_1075" s="T136">köːkü</ta>
            <ta e="T138" id="Seg_1076" s="T137">ögülüː-r</ta>
            <ta e="T139" id="Seg_1077" s="T138">köːkü</ta>
            <ta e="T140" id="Seg_1078" s="T139">hürdeːk</ta>
            <ta e="T141" id="Seg_1079" s="T140">bagajɨ-tɨk</ta>
            <ta e="T142" id="Seg_1080" s="T141">ol</ta>
            <ta e="T143" id="Seg_1081" s="T142">gɨn-an</ta>
            <ta e="T144" id="Seg_1082" s="T143">baran</ta>
            <ta e="T145" id="Seg_1083" s="T144">dʼaktar-ɨ-n</ta>
            <ta e="T146" id="Seg_1084" s="T145">di͡eg</ta>
            <ta e="T147" id="Seg_1085" s="T146">kör-ör</ta>
            <ta e="T148" id="Seg_1086" s="T147">daː</ta>
            <ta e="T149" id="Seg_1087" s="T148">dʼon</ta>
            <ta e="T150" id="Seg_1088" s="T149">di͡eg</ta>
            <ta e="T151" id="Seg_1089" s="T150">hapsɨj-an</ta>
            <ta e="T152" id="Seg_1090" s="T151">keːh-er</ta>
            <ta e="T153" id="Seg_1091" s="T152">dʼaktar-a</ta>
            <ta e="T154" id="Seg_1092" s="T153">dʼon</ta>
            <ta e="T155" id="Seg_1093" s="T154">di͡eg</ta>
            <ta e="T156" id="Seg_1094" s="T155">bar-ar</ta>
            <ta e="T157" id="Seg_1095" s="T156">daː</ta>
            <ta e="T158" id="Seg_1096" s="T157">ebekeː-lere</ta>
            <ta e="T159" id="Seg_1097" s="T158">hüːr-en</ta>
            <ta e="T160" id="Seg_1098" s="T159">kaːl-ar</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_1099" s="T0">bɨlɨr</ta>
            <ta e="T2" id="Seg_1100" s="T1">biːr</ta>
            <ta e="T3" id="Seg_1101" s="T2">bihik-LAːK</ta>
            <ta e="T4" id="Seg_1102" s="T3">ogo-LAːK</ta>
            <ta e="T5" id="Seg_1103" s="T4">dʼaktar</ta>
            <ta e="T6" id="Seg_1104" s="T5">ojuːr-GA</ta>
            <ta e="T7" id="Seg_1105" s="T6">mun-BIT</ta>
            <ta e="T8" id="Seg_1106" s="T7">kühün</ta>
            <ta e="T9" id="Seg_1107" s="T8">bu</ta>
            <ta e="T10" id="Seg_1108" s="T9">hɨrɨt-An</ta>
            <ta e="T11" id="Seg_1109" s="T10">hu͡ol-tI-n</ta>
            <ta e="T12" id="Seg_1110" s="T11">bul-I-mInA</ta>
            <ta e="T13" id="Seg_1111" s="T12">ebekeː</ta>
            <ta e="T14" id="Seg_1112" s="T13">dʼi͡e-tI-GAr</ta>
            <ta e="T15" id="Seg_1113" s="T14">kel-BIT</ta>
            <ta e="T16" id="Seg_1114" s="T15">bu</ta>
            <ta e="T17" id="Seg_1115" s="T16">kel-An</ta>
            <ta e="T18" id="Seg_1116" s="T17">kiːr-Ar</ta>
            <ta e="T19" id="Seg_1117" s="T18">da</ta>
            <ta e="T20" id="Seg_1118" s="T19">et-Ar</ta>
            <ta e="T21" id="Seg_1119" s="T20">dʼe</ta>
            <ta e="T22" id="Seg_1120" s="T21">min</ta>
            <ta e="T23" id="Seg_1121" s="T22">öl-Ar-I-m</ta>
            <ta e="T24" id="Seg_1122" s="T23">hin</ta>
            <ta e="T25" id="Seg_1123" s="T24">biːr</ta>
            <ta e="T26" id="Seg_1124" s="T25">min-n</ta>
            <ta e="T27" id="Seg_1125" s="T26">hi͡e-IAK</ta>
            <ta e="T28" id="Seg_1126" s="T27">bu͡ol-TAK-GInA</ta>
            <ta e="T29" id="Seg_1127" s="T28">hi͡e</ta>
            <ta e="T30" id="Seg_1128" s="T29">bu</ta>
            <ta e="T31" id="Seg_1129" s="T30">ebekeː-tA</ta>
            <ta e="T32" id="Seg_1130" s="T31">tur-An</ta>
            <ta e="T33" id="Seg_1131" s="T32">manna</ta>
            <ta e="T34" id="Seg_1132" s="T33">ulaga</ta>
            <ta e="T35" id="Seg_1133" s="T34">hɨgarɨj-An</ta>
            <ta e="T36" id="Seg_1134" s="T35">bi͡er-Ar</ta>
            <ta e="T37" id="Seg_1135" s="T36">manna</ta>
            <ta e="T38" id="Seg_1136" s="T37">dʼaktar</ta>
            <ta e="T39" id="Seg_1137" s="T38">olor-Ar</ta>
            <ta e="T40" id="Seg_1138" s="T39">bu-nI</ta>
            <ta e="T41" id="Seg_1139" s="T40">tur-An</ta>
            <ta e="T42" id="Seg_1140" s="T41">giniler</ta>
            <ta e="T43" id="Seg_1141" s="T42">ebekeː-nI</ta>
            <ta e="T44" id="Seg_1142" s="T43">kɨtta</ta>
            <ta e="T45" id="Seg_1143" s="T44">utuj-An</ta>
            <ta e="T46" id="Seg_1144" s="T45">kaːl-Ar-LAr</ta>
            <ta e="T47" id="Seg_1145" s="T46">ebekeː</ta>
            <ta e="T48" id="Seg_1146" s="T47">ikki</ta>
            <ta e="T49" id="Seg_1147" s="T48">tögül-LAː-An</ta>
            <ta e="T50" id="Seg_1148" s="T49">utuj-Ar</ta>
            <ta e="T51" id="Seg_1149" s="T50">di͡e-Ar-LAr</ta>
            <ta e="T52" id="Seg_1150" s="T51">dʼɨl</ta>
            <ta e="T53" id="Seg_1151" s="T52">orto-tI-GAr</ta>
            <ta e="T54" id="Seg_1152" s="T53">Miku͡olin-GA</ta>
            <ta e="T55" id="Seg_1153" s="T54">uhugun-Ar</ta>
            <ta e="T56" id="Seg_1154" s="T55">di͡e-Ar-LAr</ta>
            <ta e="T57" id="Seg_1155" s="T56">bu</ta>
            <ta e="T58" id="Seg_1156" s="T57">uhugun-I-BIT-tA</ta>
            <ta e="T59" id="Seg_1157" s="T58">dʼaktar-tA</ta>
            <ta e="T60" id="Seg_1158" s="T59">gini</ta>
            <ta e="T61" id="Seg_1159" s="T60">tilek-tI-n</ta>
            <ta e="T62" id="Seg_1160" s="T61">em-A</ta>
            <ta e="T63" id="Seg_1161" s="T62">hɨt-Ar</ta>
            <ta e="T64" id="Seg_1162" s="T63">bu͡ol-Ar</ta>
            <ta e="T65" id="Seg_1163" s="T64">onton</ta>
            <ta e="T66" id="Seg_1164" s="T65">tot-Ar</ta>
            <ta e="T67" id="Seg_1165" s="T66">ogo-tI-n</ta>
            <ta e="T68" id="Seg_1166" s="T67">da</ta>
            <ta e="T69" id="Seg_1167" s="T68">tot-A-r-Ar</ta>
            <ta e="T70" id="Seg_1168" s="T69">dʼe</ta>
            <ta e="T71" id="Seg_1169" s="T70">ikki</ta>
            <ta e="T72" id="Seg_1170" s="T71">tögül</ta>
            <ta e="T73" id="Seg_1171" s="T72">utuj-An-LAr</ta>
            <ta e="T74" id="Seg_1172" s="T73">uhugun-Ar-LAr</ta>
            <ta e="T75" id="Seg_1173" s="T74">dʼaktar</ta>
            <ta e="T76" id="Seg_1174" s="T75">bu</ta>
            <ta e="T77" id="Seg_1175" s="T76">kɨːl-tI-n</ta>
            <ta e="T78" id="Seg_1176" s="T77">kɨtta</ta>
            <ta e="T79" id="Seg_1177" s="T78">tagɨs-Ar-LAr</ta>
            <ta e="T80" id="Seg_1178" s="T79">tagɨs-Ar-LAr</ta>
            <ta e="T81" id="Seg_1179" s="T80">da</ta>
            <ta e="T82" id="Seg_1180" s="T81">ebekeː-tA</ta>
            <ta e="T83" id="Seg_1181" s="T82">ɨmɨjak-LAː-A</ta>
            <ta e="T84" id="Seg_1182" s="T83">hɨrɨt-An</ta>
            <ta e="T85" id="Seg_1183" s="T84">dʼaktar-tI-n</ta>
            <ta e="T86" id="Seg_1184" s="T85">ahaː-A-t-Ar</ta>
            <ta e="T87" id="Seg_1185" s="T86">bu</ta>
            <ta e="T88" id="Seg_1186" s="T87">hɨrɨt-TAK-TArInA</ta>
            <ta e="T89" id="Seg_1187" s="T88">giniler</ta>
            <ta e="T90" id="Seg_1188" s="T89">huːrt-LArI-ttAn</ta>
            <ta e="T91" id="Seg_1189" s="T90">biːr</ta>
            <ta e="T92" id="Seg_1190" s="T91">kihi</ta>
            <ta e="T93" id="Seg_1191" s="T92">bultaː-A</ta>
            <ta e="T94" id="Seg_1192" s="T93">hɨrɨt-An</ta>
            <ta e="T95" id="Seg_1193" s="T94">kör-Ar</ta>
            <ta e="T96" id="Seg_1194" s="T95">kör-Ar</ta>
            <ta e="T97" id="Seg_1195" s="T96">da</ta>
            <ta e="T98" id="Seg_1196" s="T97">bu</ta>
            <ta e="T99" id="Seg_1197" s="T98">dʼaktar-nI</ta>
            <ta e="T100" id="Seg_1198" s="T99">taːj-Ar</ta>
            <ta e="T101" id="Seg_1199" s="T100">kepseː-Ar</ta>
            <ta e="T102" id="Seg_1200" s="T101">dʼon-LAr-tI-GAr</ta>
            <ta e="T103" id="Seg_1201" s="T102">er-tI-GAr</ta>
            <ta e="T104" id="Seg_1202" s="T103">bu-nI</ta>
            <ta e="T105" id="Seg_1203" s="T104">tur-An</ta>
            <ta e="T106" id="Seg_1204" s="T105">barɨ</ta>
            <ta e="T107" id="Seg_1205" s="T106">dʼon-tA</ta>
            <ta e="T108" id="Seg_1206" s="T107">ü͡öŋ-An</ta>
            <ta e="T109" id="Seg_1207" s="T108">kel-An</ta>
            <ta e="T110" id="Seg_1208" s="T109">tur-An</ta>
            <ta e="T111" id="Seg_1209" s="T110">baran</ta>
            <ta e="T112" id="Seg_1210" s="T111">togo</ta>
            <ta e="T113" id="Seg_1211" s="T112">ölör-IAK-BIt=Ij</ta>
            <ta e="T114" id="Seg_1212" s="T113">di͡e-An</ta>
            <ta e="T115" id="Seg_1213" s="T114">algaː-Ar-LAr</ta>
            <ta e="T116" id="Seg_1214" s="T115">bokulu͡omnaː-A-bokulu͡omnaː-A</ta>
            <ta e="T117" id="Seg_1215" s="T116">ebekeː-nI</ta>
            <ta e="T118" id="Seg_1216" s="T117">ebe</ta>
            <ta e="T119" id="Seg_1217" s="T118">kihi-nI</ta>
            <ta e="T120" id="Seg_1218" s="T119">bi͡er-IAK</ta>
            <ta e="T121" id="Seg_1219" s="T120">kem-I-ŋ</ta>
            <ta e="T122" id="Seg_1220" s="T121">bu͡ol-TI-tA</ta>
            <ta e="T123" id="Seg_1221" s="T122">iːt-BIT-GA-r</ta>
            <ta e="T124" id="Seg_1222" s="T123">pasiːba</ta>
            <ta e="T125" id="Seg_1223" s="T124">dʼaktar-nI</ta>
            <ta e="T126" id="Seg_1224" s="T125">tönün-A-r</ta>
            <ta e="T127" id="Seg_1225" s="T126">kojut</ta>
            <ta e="T128" id="Seg_1226" s="T127">ogo-GA</ta>
            <ta e="T129" id="Seg_1227" s="T128">da</ta>
            <ta e="T130" id="Seg_1228" s="T129">kepseː-IAK-BItI-n</ta>
            <ta e="T131" id="Seg_1229" s="T130">en</ta>
            <ta e="T132" id="Seg_1230" s="T131">iːt-BIT-GI-n</ta>
            <ta e="T133" id="Seg_1231" s="T132">di͡e-Ar-LAr</ta>
            <ta e="T134" id="Seg_1232" s="T133">bu-GA</ta>
            <ta e="T135" id="Seg_1233" s="T134">ebekeː</ta>
            <ta e="T136" id="Seg_1234" s="T135">ü͡ögüleː-Ar</ta>
            <ta e="T137" id="Seg_1235" s="T136">köːkü</ta>
            <ta e="T138" id="Seg_1236" s="T137">ü͡ögüleː-Ar</ta>
            <ta e="T139" id="Seg_1237" s="T138">köːkü</ta>
            <ta e="T140" id="Seg_1238" s="T139">hürdeːk</ta>
            <ta e="T141" id="Seg_1239" s="T140">bagajɨ-LIk</ta>
            <ta e="T142" id="Seg_1240" s="T141">ol</ta>
            <ta e="T143" id="Seg_1241" s="T142">gɨn-An</ta>
            <ta e="T144" id="Seg_1242" s="T143">baran</ta>
            <ta e="T145" id="Seg_1243" s="T144">dʼaktar-tI-n</ta>
            <ta e="T146" id="Seg_1244" s="T145">dek</ta>
            <ta e="T147" id="Seg_1245" s="T146">kör-Ar</ta>
            <ta e="T148" id="Seg_1246" s="T147">da</ta>
            <ta e="T149" id="Seg_1247" s="T148">dʼon</ta>
            <ta e="T150" id="Seg_1248" s="T149">dek</ta>
            <ta e="T151" id="Seg_1249" s="T150">hapsɨj-An</ta>
            <ta e="T152" id="Seg_1250" s="T151">keːs-Ar</ta>
            <ta e="T153" id="Seg_1251" s="T152">dʼaktar-tA</ta>
            <ta e="T154" id="Seg_1252" s="T153">dʼon</ta>
            <ta e="T155" id="Seg_1253" s="T154">dek</ta>
            <ta e="T156" id="Seg_1254" s="T155">bar-Ar</ta>
            <ta e="T157" id="Seg_1255" s="T156">da</ta>
            <ta e="T158" id="Seg_1256" s="T157">ebekeː-LArA</ta>
            <ta e="T159" id="Seg_1257" s="T158">hüːr-An</ta>
            <ta e="T160" id="Seg_1258" s="T159">kaːl-Ar</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_1259" s="T0">long.ago</ta>
            <ta e="T2" id="Seg_1260" s="T1">one</ta>
            <ta e="T3" id="Seg_1261" s="T2">cradle-PROPR</ta>
            <ta e="T4" id="Seg_1262" s="T3">child-PROPR</ta>
            <ta e="T5" id="Seg_1263" s="T4">woman.[NOM]</ta>
            <ta e="T6" id="Seg_1264" s="T5">forest-DAT/LOC</ta>
            <ta e="T7" id="Seg_1265" s="T6">get.lost-PST2.[3SG]</ta>
            <ta e="T8" id="Seg_1266" s="T7">in.autumn</ta>
            <ta e="T9" id="Seg_1267" s="T8">this</ta>
            <ta e="T10" id="Seg_1268" s="T9">go-CVB.SEQ</ta>
            <ta e="T11" id="Seg_1269" s="T10">way-3SG-ACC</ta>
            <ta e="T12" id="Seg_1270" s="T11">find-EP-NEG.CVB</ta>
            <ta e="T13" id="Seg_1271" s="T12">bear.[NOM]</ta>
            <ta e="T14" id="Seg_1272" s="T13">house-3SG-DAT/LOC</ta>
            <ta e="T15" id="Seg_1273" s="T14">come-PST2.[3SG]</ta>
            <ta e="T16" id="Seg_1274" s="T15">this</ta>
            <ta e="T17" id="Seg_1275" s="T16">come-CVB.SEQ</ta>
            <ta e="T18" id="Seg_1276" s="T17">go.in-PRS.[3SG]</ta>
            <ta e="T19" id="Seg_1277" s="T18">and</ta>
            <ta e="T20" id="Seg_1278" s="T19">speak-PRS.[3SG]</ta>
            <ta e="T21" id="Seg_1279" s="T20">well</ta>
            <ta e="T22" id="Seg_1280" s="T21">1SG.[NOM]</ta>
            <ta e="T23" id="Seg_1281" s="T22">die-PTCP.PRS-EP-1SG.[NOM]</ta>
            <ta e="T24" id="Seg_1282" s="T23">however</ta>
            <ta e="T25" id="Seg_1283" s="T24">whatever</ta>
            <ta e="T26" id="Seg_1284" s="T25">1SG-ACC</ta>
            <ta e="T27" id="Seg_1285" s="T26">eat-PTCP.FUT</ta>
            <ta e="T28" id="Seg_1286" s="T27">be-TEMP-2SG</ta>
            <ta e="T29" id="Seg_1287" s="T28">eat.[IMP.2SG]</ta>
            <ta e="T30" id="Seg_1288" s="T29">this</ta>
            <ta e="T31" id="Seg_1289" s="T30">bear-3SG.[NOM]</ta>
            <ta e="T32" id="Seg_1290" s="T31">stand-CVB.SEQ</ta>
            <ta e="T33" id="Seg_1291" s="T32">here</ta>
            <ta e="T34" id="Seg_1292" s="T33">place.behind.[NOM]</ta>
            <ta e="T35" id="Seg_1293" s="T34">move-CVB.SEQ</ta>
            <ta e="T36" id="Seg_1294" s="T35">give-PRS.[3SG]</ta>
            <ta e="T37" id="Seg_1295" s="T36">here</ta>
            <ta e="T38" id="Seg_1296" s="T37">woman.[NOM]</ta>
            <ta e="T39" id="Seg_1297" s="T38">live-PRS.[3SG]</ta>
            <ta e="T40" id="Seg_1298" s="T39">this-ACC</ta>
            <ta e="T41" id="Seg_1299" s="T40">stand-CVB.SEQ</ta>
            <ta e="T42" id="Seg_1300" s="T41">3PL.[NOM]</ta>
            <ta e="T43" id="Seg_1301" s="T42">bear-ACC</ta>
            <ta e="T44" id="Seg_1302" s="T43">with</ta>
            <ta e="T45" id="Seg_1303" s="T44">fall.asleep-CVB.SEQ</ta>
            <ta e="T46" id="Seg_1304" s="T45">stay-PRS-3PL</ta>
            <ta e="T47" id="Seg_1305" s="T46">bear.[NOM]</ta>
            <ta e="T48" id="Seg_1306" s="T47">two</ta>
            <ta e="T49" id="Seg_1307" s="T48">time-VBZ-CVB.SEQ</ta>
            <ta e="T50" id="Seg_1308" s="T49">fall.asleep-PRS.[3SG]</ta>
            <ta e="T51" id="Seg_1309" s="T50">say-PRS-3PL</ta>
            <ta e="T52" id="Seg_1310" s="T51">year.[NOM]</ta>
            <ta e="T53" id="Seg_1311" s="T52">middle-3SG-DAT/LOC</ta>
            <ta e="T54" id="Seg_1312" s="T53">St.Nicholas.Day-DAT/LOC</ta>
            <ta e="T55" id="Seg_1313" s="T54">wake.up-PRS.[3SG]</ta>
            <ta e="T56" id="Seg_1314" s="T55">say-PRS-3PL</ta>
            <ta e="T57" id="Seg_1315" s="T56">this</ta>
            <ta e="T58" id="Seg_1316" s="T57">wake.up-EP-PST2-3SG</ta>
            <ta e="T59" id="Seg_1317" s="T58">woman-3SG.[NOM]</ta>
            <ta e="T60" id="Seg_1318" s="T59">3SG.[NOM]</ta>
            <ta e="T61" id="Seg_1319" s="T60">paw-3SG-ACC</ta>
            <ta e="T62" id="Seg_1320" s="T61">suck-CVB.SIM</ta>
            <ta e="T63" id="Seg_1321" s="T62">lie-PTCP.PRS</ta>
            <ta e="T64" id="Seg_1322" s="T63">be-PRS.[3SG]</ta>
            <ta e="T65" id="Seg_1323" s="T64">then</ta>
            <ta e="T66" id="Seg_1324" s="T65">eat.ones.fill-PRS.[3SG]</ta>
            <ta e="T67" id="Seg_1325" s="T66">child-3SG-ACC</ta>
            <ta e="T68" id="Seg_1326" s="T67">and</ta>
            <ta e="T69" id="Seg_1327" s="T68">eat.ones.fill-EP-CAUS-PRS.[3SG]</ta>
            <ta e="T70" id="Seg_1328" s="T69">well</ta>
            <ta e="T71" id="Seg_1329" s="T70">two</ta>
            <ta e="T72" id="Seg_1330" s="T71">time.[NOM]</ta>
            <ta e="T73" id="Seg_1331" s="T72">sleep-CVB.SEQ-3PL</ta>
            <ta e="T74" id="Seg_1332" s="T73">wake.up-PRS-3PL</ta>
            <ta e="T75" id="Seg_1333" s="T74">woman.[NOM]</ta>
            <ta e="T76" id="Seg_1334" s="T75">this</ta>
            <ta e="T77" id="Seg_1335" s="T76">animal-3SG-ACC</ta>
            <ta e="T78" id="Seg_1336" s="T77">with</ta>
            <ta e="T79" id="Seg_1337" s="T78">go.out-PRS-3PL</ta>
            <ta e="T80" id="Seg_1338" s="T79">go.out-PRS-3PL</ta>
            <ta e="T81" id="Seg_1339" s="T80">and</ta>
            <ta e="T82" id="Seg_1340" s="T81">bear-3SG.[NOM]</ta>
            <ta e="T83" id="Seg_1341" s="T82">root-VBZ-CVB.SIM</ta>
            <ta e="T84" id="Seg_1342" s="T83">go-CVB.SEQ</ta>
            <ta e="T85" id="Seg_1343" s="T84">woman-3SG-ACC</ta>
            <ta e="T86" id="Seg_1344" s="T85">eat-EP-CAUS-PRS.[3SG]</ta>
            <ta e="T87" id="Seg_1345" s="T86">this</ta>
            <ta e="T88" id="Seg_1346" s="T87">go-TEMP-3PL</ta>
            <ta e="T89" id="Seg_1347" s="T88">3PL.[NOM]</ta>
            <ta e="T90" id="Seg_1348" s="T89">temporary.settlement-3PL-ABL</ta>
            <ta e="T91" id="Seg_1349" s="T90">one</ta>
            <ta e="T92" id="Seg_1350" s="T91">human.being.[NOM]</ta>
            <ta e="T93" id="Seg_1351" s="T92">hunt-CVB.SIM</ta>
            <ta e="T94" id="Seg_1352" s="T93">go-CVB.SEQ</ta>
            <ta e="T95" id="Seg_1353" s="T94">see-PRS.[3SG]</ta>
            <ta e="T96" id="Seg_1354" s="T95">see-PRS.[3SG]</ta>
            <ta e="T97" id="Seg_1355" s="T96">and</ta>
            <ta e="T98" id="Seg_1356" s="T97">this</ta>
            <ta e="T99" id="Seg_1357" s="T98">woman-ACC</ta>
            <ta e="T100" id="Seg_1358" s="T99">realize-PRS.[3SG]</ta>
            <ta e="T101" id="Seg_1359" s="T100">tell-PRS.[3SG]</ta>
            <ta e="T102" id="Seg_1360" s="T101">people-PL-3SG-DAT/LOC</ta>
            <ta e="T103" id="Seg_1361" s="T102">man-3SG-DAT/LOC</ta>
            <ta e="T104" id="Seg_1362" s="T103">this-ACC</ta>
            <ta e="T105" id="Seg_1363" s="T104">stand-CVB.SEQ</ta>
            <ta e="T106" id="Seg_1364" s="T105">every</ta>
            <ta e="T107" id="Seg_1365" s="T106">people-3SG.[NOM]</ta>
            <ta e="T108" id="Seg_1366" s="T107">stalk-CVB.SEQ</ta>
            <ta e="T109" id="Seg_1367" s="T108">come-CVB.SEQ</ta>
            <ta e="T110" id="Seg_1368" s="T109">stand-CVB.SEQ</ta>
            <ta e="T111" id="Seg_1369" s="T110">after</ta>
            <ta e="T112" id="Seg_1370" s="T111">why</ta>
            <ta e="T113" id="Seg_1371" s="T112">kill-FUT-1PL=Q</ta>
            <ta e="T114" id="Seg_1372" s="T113">say-CVB.SEQ</ta>
            <ta e="T115" id="Seg_1373" s="T114">mumble.to.oneself-PRS-3PL</ta>
            <ta e="T116" id="Seg_1374" s="T115">bow.to-CVB.SIM-bow.to-CVB.SIM</ta>
            <ta e="T117" id="Seg_1375" s="T116">bear-ACC</ta>
            <ta e="T118" id="Seg_1376" s="T117">bear.[NOM]</ta>
            <ta e="T119" id="Seg_1377" s="T118">human.being-ACC</ta>
            <ta e="T120" id="Seg_1378" s="T119">give-PTCP.FUT</ta>
            <ta e="T121" id="Seg_1379" s="T120">time-EP-2SG.[NOM]</ta>
            <ta e="T122" id="Seg_1380" s="T121">become-PST1-3SG</ta>
            <ta e="T123" id="Seg_1381" s="T122">feed-PTCP.PST-2SG-DAT/LOC</ta>
            <ta e="T124" id="Seg_1382" s="T123">thanks</ta>
            <ta e="T125" id="Seg_1383" s="T124">woman-ACC</ta>
            <ta e="T126" id="Seg_1384" s="T125">come.back-EP-CAUS.[IMP.2SG]</ta>
            <ta e="T127" id="Seg_1385" s="T126">later</ta>
            <ta e="T128" id="Seg_1386" s="T127">child-DAT/LOC</ta>
            <ta e="T129" id="Seg_1387" s="T128">and</ta>
            <ta e="T130" id="Seg_1388" s="T129">tell-PTCP.FUT-1PL-ACC</ta>
            <ta e="T131" id="Seg_1389" s="T130">2SG.[NOM]</ta>
            <ta e="T132" id="Seg_1390" s="T131">feed-PTCP.PST-2SG-ACC</ta>
            <ta e="T133" id="Seg_1391" s="T132">say-PRS-3PL</ta>
            <ta e="T134" id="Seg_1392" s="T133">this-DAT/LOC</ta>
            <ta e="T135" id="Seg_1393" s="T134">bear.[NOM]</ta>
            <ta e="T136" id="Seg_1394" s="T135">shout-PRS.[3SG]</ta>
            <ta e="T137" id="Seg_1395" s="T136">very</ta>
            <ta e="T138" id="Seg_1396" s="T137">shout-PRS.[3SG]</ta>
            <ta e="T139" id="Seg_1397" s="T138">very</ta>
            <ta e="T140" id="Seg_1398" s="T139">very</ta>
            <ta e="T141" id="Seg_1399" s="T140">very-ADVZ</ta>
            <ta e="T142" id="Seg_1400" s="T141">that.[NOM]</ta>
            <ta e="T143" id="Seg_1401" s="T142">make-CVB.SEQ</ta>
            <ta e="T144" id="Seg_1402" s="T143">after</ta>
            <ta e="T145" id="Seg_1403" s="T144">woman-3SG-ACC</ta>
            <ta e="T146" id="Seg_1404" s="T145">to</ta>
            <ta e="T147" id="Seg_1405" s="T146">see-PRS.[3SG]</ta>
            <ta e="T148" id="Seg_1406" s="T147">and</ta>
            <ta e="T149" id="Seg_1407" s="T148">people.[NOM]</ta>
            <ta e="T150" id="Seg_1408" s="T149">to</ta>
            <ta e="T151" id="Seg_1409" s="T150">wave-CVB.SEQ</ta>
            <ta e="T152" id="Seg_1410" s="T151">throw-PRS.[3SG]</ta>
            <ta e="T153" id="Seg_1411" s="T152">woman-3SG.[NOM]</ta>
            <ta e="T154" id="Seg_1412" s="T153">people.[NOM]</ta>
            <ta e="T155" id="Seg_1413" s="T154">to</ta>
            <ta e="T156" id="Seg_1414" s="T155">go-PRS.[3SG]</ta>
            <ta e="T157" id="Seg_1415" s="T156">and</ta>
            <ta e="T158" id="Seg_1416" s="T157">bear-3PL.[NOM]</ta>
            <ta e="T159" id="Seg_1417" s="T158">run-CVB.SEQ</ta>
            <ta e="T160" id="Seg_1418" s="T159">stay-PRS.[3SG]</ta>
         </annotation>
         <annotation name="gg" tierref="gg">
            <ta e="T1" id="Seg_1419" s="T0">vor.langer.Zeit</ta>
            <ta e="T2" id="Seg_1420" s="T1">eins</ta>
            <ta e="T3" id="Seg_1421" s="T2">Wiege-PROPR</ta>
            <ta e="T4" id="Seg_1422" s="T3">Kind-PROPR</ta>
            <ta e="T5" id="Seg_1423" s="T4">Frau.[NOM]</ta>
            <ta e="T6" id="Seg_1424" s="T5">Wald-DAT/LOC</ta>
            <ta e="T7" id="Seg_1425" s="T6">sich.verirren-PST2.[3SG]</ta>
            <ta e="T8" id="Seg_1426" s="T7">im.Herbst</ta>
            <ta e="T9" id="Seg_1427" s="T8">dieses</ta>
            <ta e="T10" id="Seg_1428" s="T9">gehen-CVB.SEQ</ta>
            <ta e="T11" id="Seg_1429" s="T10">Weg-3SG-ACC</ta>
            <ta e="T12" id="Seg_1430" s="T11">finden-EP-NEG.CVB</ta>
            <ta e="T13" id="Seg_1431" s="T12">Bär.[NOM]</ta>
            <ta e="T14" id="Seg_1432" s="T13">Haus-3SG-DAT/LOC</ta>
            <ta e="T15" id="Seg_1433" s="T14">kommen-PST2.[3SG]</ta>
            <ta e="T16" id="Seg_1434" s="T15">dieses</ta>
            <ta e="T17" id="Seg_1435" s="T16">kommen-CVB.SEQ</ta>
            <ta e="T18" id="Seg_1436" s="T17">hineingehen-PRS.[3SG]</ta>
            <ta e="T19" id="Seg_1437" s="T18">und</ta>
            <ta e="T20" id="Seg_1438" s="T19">sprechen-PRS.[3SG]</ta>
            <ta e="T21" id="Seg_1439" s="T20">doch</ta>
            <ta e="T22" id="Seg_1440" s="T21">1SG.[NOM]</ta>
            <ta e="T23" id="Seg_1441" s="T22">sterben-PTCP.PRS-EP-1SG.[NOM]</ta>
            <ta e="T24" id="Seg_1442" s="T23">doch</ta>
            <ta e="T25" id="Seg_1443" s="T24">einerlei</ta>
            <ta e="T26" id="Seg_1444" s="T25">1SG-ACC</ta>
            <ta e="T27" id="Seg_1445" s="T26">essen-PTCP.FUT</ta>
            <ta e="T28" id="Seg_1446" s="T27">sein-TEMP-2SG</ta>
            <ta e="T29" id="Seg_1447" s="T28">essen.[IMP.2SG]</ta>
            <ta e="T30" id="Seg_1448" s="T29">dieses</ta>
            <ta e="T31" id="Seg_1449" s="T30">Bär-3SG.[NOM]</ta>
            <ta e="T32" id="Seg_1450" s="T31">stehen-CVB.SEQ</ta>
            <ta e="T33" id="Seg_1451" s="T32">hier</ta>
            <ta e="T34" id="Seg_1452" s="T33">Platz.hinter.[NOM]</ta>
            <ta e="T35" id="Seg_1453" s="T34">sich.bewegen-CVB.SEQ</ta>
            <ta e="T36" id="Seg_1454" s="T35">geben-PRS.[3SG]</ta>
            <ta e="T37" id="Seg_1455" s="T36">hier</ta>
            <ta e="T38" id="Seg_1456" s="T37">Frau.[NOM]</ta>
            <ta e="T39" id="Seg_1457" s="T38">leben-PRS.[3SG]</ta>
            <ta e="T40" id="Seg_1458" s="T39">dieses-ACC</ta>
            <ta e="T41" id="Seg_1459" s="T40">stehen-CVB.SEQ</ta>
            <ta e="T42" id="Seg_1460" s="T41">3PL.[NOM]</ta>
            <ta e="T43" id="Seg_1461" s="T42">Bär-ACC</ta>
            <ta e="T44" id="Seg_1462" s="T43">mit</ta>
            <ta e="T45" id="Seg_1463" s="T44">einschlafen-CVB.SEQ</ta>
            <ta e="T46" id="Seg_1464" s="T45">bleiben-PRS-3PL</ta>
            <ta e="T47" id="Seg_1465" s="T46">Bär.[NOM]</ta>
            <ta e="T48" id="Seg_1466" s="T47">zwei</ta>
            <ta e="T49" id="Seg_1467" s="T48">Mal-VBZ-CVB.SEQ</ta>
            <ta e="T50" id="Seg_1468" s="T49">einschlafen-PRS.[3SG]</ta>
            <ta e="T51" id="Seg_1469" s="T50">sagen-PRS-3PL</ta>
            <ta e="T52" id="Seg_1470" s="T51">Jahr.[NOM]</ta>
            <ta e="T53" id="Seg_1471" s="T52">Mitte-3SG-DAT/LOC</ta>
            <ta e="T54" id="Seg_1472" s="T53">Nikolaustag-DAT/LOC</ta>
            <ta e="T55" id="Seg_1473" s="T54">aufwachen-PRS.[3SG]</ta>
            <ta e="T56" id="Seg_1474" s="T55">sagen-PRS-3PL</ta>
            <ta e="T57" id="Seg_1475" s="T56">dieses</ta>
            <ta e="T58" id="Seg_1476" s="T57">aufwachen-EP-PST2-3SG</ta>
            <ta e="T59" id="Seg_1477" s="T58">Frau-3SG.[NOM]</ta>
            <ta e="T60" id="Seg_1478" s="T59">3SG.[NOM]</ta>
            <ta e="T61" id="Seg_1479" s="T60">Pfote-3SG-ACC</ta>
            <ta e="T62" id="Seg_1480" s="T61">saugen-CVB.SIM</ta>
            <ta e="T63" id="Seg_1481" s="T62">liegen-PTCP.PRS</ta>
            <ta e="T64" id="Seg_1482" s="T63">sein-PRS.[3SG]</ta>
            <ta e="T65" id="Seg_1483" s="T64">dann</ta>
            <ta e="T66" id="Seg_1484" s="T65">sich.satt.essen-PRS.[3SG]</ta>
            <ta e="T67" id="Seg_1485" s="T66">Kind-3SG-ACC</ta>
            <ta e="T68" id="Seg_1486" s="T67">und</ta>
            <ta e="T69" id="Seg_1487" s="T68">sich.satt.essen-EP-CAUS-PRS.[3SG]</ta>
            <ta e="T70" id="Seg_1488" s="T69">doch</ta>
            <ta e="T71" id="Seg_1489" s="T70">zwei</ta>
            <ta e="T72" id="Seg_1490" s="T71">Mal.[NOM]</ta>
            <ta e="T73" id="Seg_1491" s="T72">schlafen-CVB.SEQ-3PL</ta>
            <ta e="T74" id="Seg_1492" s="T73">aufwachen-PRS-3PL</ta>
            <ta e="T75" id="Seg_1493" s="T74">Frau.[NOM]</ta>
            <ta e="T76" id="Seg_1494" s="T75">dieses</ta>
            <ta e="T77" id="Seg_1495" s="T76">Tier-3SG-ACC</ta>
            <ta e="T78" id="Seg_1496" s="T77">mit</ta>
            <ta e="T79" id="Seg_1497" s="T78">hinausgehen-PRS-3PL</ta>
            <ta e="T80" id="Seg_1498" s="T79">hinausgehen-PRS-3PL</ta>
            <ta e="T81" id="Seg_1499" s="T80">und</ta>
            <ta e="T82" id="Seg_1500" s="T81">Bär-3SG.[NOM]</ta>
            <ta e="T83" id="Seg_1501" s="T82">Wurzel-VBZ-CVB.SIM</ta>
            <ta e="T84" id="Seg_1502" s="T83">gehen-CVB.SEQ</ta>
            <ta e="T85" id="Seg_1503" s="T84">Frau-3SG-ACC</ta>
            <ta e="T86" id="Seg_1504" s="T85">essen-EP-CAUS-PRS.[3SG]</ta>
            <ta e="T87" id="Seg_1505" s="T86">dieses</ta>
            <ta e="T88" id="Seg_1506" s="T87">gehen-TEMP-3PL</ta>
            <ta e="T89" id="Seg_1507" s="T88">3PL.[NOM]</ta>
            <ta e="T90" id="Seg_1508" s="T89">vorübergehende.Siedlung-3PL-ABL</ta>
            <ta e="T91" id="Seg_1509" s="T90">eins</ta>
            <ta e="T92" id="Seg_1510" s="T91">Mensch.[NOM]</ta>
            <ta e="T93" id="Seg_1511" s="T92">jagen-CVB.SIM</ta>
            <ta e="T94" id="Seg_1512" s="T93">gehen-CVB.SEQ</ta>
            <ta e="T95" id="Seg_1513" s="T94">sehen-PRS.[3SG]</ta>
            <ta e="T96" id="Seg_1514" s="T95">sehen-PRS.[3SG]</ta>
            <ta e="T97" id="Seg_1515" s="T96">und</ta>
            <ta e="T98" id="Seg_1516" s="T97">dieses</ta>
            <ta e="T99" id="Seg_1517" s="T98">Frau-ACC</ta>
            <ta e="T100" id="Seg_1518" s="T99">erkennen-PRS.[3SG]</ta>
            <ta e="T101" id="Seg_1519" s="T100">erzählen-PRS.[3SG]</ta>
            <ta e="T102" id="Seg_1520" s="T101">Leute-PL-3SG-DAT/LOC</ta>
            <ta e="T103" id="Seg_1521" s="T102">Mann-3SG-DAT/LOC</ta>
            <ta e="T104" id="Seg_1522" s="T103">dieses-ACC</ta>
            <ta e="T105" id="Seg_1523" s="T104">stehen-CVB.SEQ</ta>
            <ta e="T106" id="Seg_1524" s="T105">jeder</ta>
            <ta e="T107" id="Seg_1525" s="T106">Volk-3SG.[NOM]</ta>
            <ta e="T108" id="Seg_1526" s="T107">sich.anschleichen-CVB.SEQ</ta>
            <ta e="T109" id="Seg_1527" s="T108">kommen-CVB.SEQ</ta>
            <ta e="T110" id="Seg_1528" s="T109">stehen-CVB.SEQ</ta>
            <ta e="T111" id="Seg_1529" s="T110">nachdem</ta>
            <ta e="T112" id="Seg_1530" s="T111">warum</ta>
            <ta e="T113" id="Seg_1531" s="T112">töten-FUT-1PL=Q</ta>
            <ta e="T114" id="Seg_1532" s="T113">sagen-CVB.SEQ</ta>
            <ta e="T115" id="Seg_1533" s="T114">vor.sich.hin.reden-PRS-3PL</ta>
            <ta e="T116" id="Seg_1534" s="T115">sich.verbeugen-CVB.SIM-sich.verbeugen-CVB.SIM</ta>
            <ta e="T117" id="Seg_1535" s="T116">Bär-ACC</ta>
            <ta e="T118" id="Seg_1536" s="T117">Bär.[NOM]</ta>
            <ta e="T119" id="Seg_1537" s="T118">Mensch-ACC</ta>
            <ta e="T120" id="Seg_1538" s="T119">geben-PTCP.FUT</ta>
            <ta e="T121" id="Seg_1539" s="T120">Zeit-EP-2SG.[NOM]</ta>
            <ta e="T122" id="Seg_1540" s="T121">werden-PST1-3SG</ta>
            <ta e="T123" id="Seg_1541" s="T122">füttern-PTCP.PST-2SG-DAT/LOC</ta>
            <ta e="T124" id="Seg_1542" s="T123">danke</ta>
            <ta e="T125" id="Seg_1543" s="T124">Frau-ACC</ta>
            <ta e="T126" id="Seg_1544" s="T125">zurückkommen-EP-CAUS.[IMP.2SG]</ta>
            <ta e="T127" id="Seg_1545" s="T126">später</ta>
            <ta e="T128" id="Seg_1546" s="T127">Kind-DAT/LOC</ta>
            <ta e="T129" id="Seg_1547" s="T128">und</ta>
            <ta e="T130" id="Seg_1548" s="T129">erzählen-PTCP.FUT-1PL-ACC</ta>
            <ta e="T131" id="Seg_1549" s="T130">2SG.[NOM]</ta>
            <ta e="T132" id="Seg_1550" s="T131">füttern-PTCP.PST-2SG-ACC</ta>
            <ta e="T133" id="Seg_1551" s="T132">sagen-PRS-3PL</ta>
            <ta e="T134" id="Seg_1552" s="T133">dieses-DAT/LOC</ta>
            <ta e="T135" id="Seg_1553" s="T134">Bär.[NOM]</ta>
            <ta e="T136" id="Seg_1554" s="T135">schreien-PRS.[3SG]</ta>
            <ta e="T137" id="Seg_1555" s="T136">sehr</ta>
            <ta e="T138" id="Seg_1556" s="T137">schreien-PRS.[3SG]</ta>
            <ta e="T139" id="Seg_1557" s="T138">sehr</ta>
            <ta e="T140" id="Seg_1558" s="T139">sehr</ta>
            <ta e="T141" id="Seg_1559" s="T140">sehr-ADVZ</ta>
            <ta e="T142" id="Seg_1560" s="T141">jenes.[NOM]</ta>
            <ta e="T143" id="Seg_1561" s="T142">machen-CVB.SEQ</ta>
            <ta e="T144" id="Seg_1562" s="T143">nachdem</ta>
            <ta e="T145" id="Seg_1563" s="T144">Frau-3SG-ACC</ta>
            <ta e="T146" id="Seg_1564" s="T145">zu</ta>
            <ta e="T147" id="Seg_1565" s="T146">sehen-PRS.[3SG]</ta>
            <ta e="T148" id="Seg_1566" s="T147">und</ta>
            <ta e="T149" id="Seg_1567" s="T148">Leute.[NOM]</ta>
            <ta e="T150" id="Seg_1568" s="T149">zu</ta>
            <ta e="T151" id="Seg_1569" s="T150">winken-CVB.SEQ</ta>
            <ta e="T152" id="Seg_1570" s="T151">werfen-PRS.[3SG]</ta>
            <ta e="T153" id="Seg_1571" s="T152">Frau-3SG.[NOM]</ta>
            <ta e="T154" id="Seg_1572" s="T153">Leute.[NOM]</ta>
            <ta e="T155" id="Seg_1573" s="T154">zu</ta>
            <ta e="T156" id="Seg_1574" s="T155">gehen-PRS.[3SG]</ta>
            <ta e="T157" id="Seg_1575" s="T156">und</ta>
            <ta e="T158" id="Seg_1576" s="T157">Bär-3PL.[NOM]</ta>
            <ta e="T159" id="Seg_1577" s="T158">laufen-CVB.SEQ</ta>
            <ta e="T160" id="Seg_1578" s="T159">bleiben-PRS.[3SG]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_1579" s="T0">давно</ta>
            <ta e="T2" id="Seg_1580" s="T1">один</ta>
            <ta e="T3" id="Seg_1581" s="T2">колыбель-PROPR</ta>
            <ta e="T4" id="Seg_1582" s="T3">ребенок-PROPR</ta>
            <ta e="T5" id="Seg_1583" s="T4">жена.[NOM]</ta>
            <ta e="T6" id="Seg_1584" s="T5">лес-DAT/LOC</ta>
            <ta e="T7" id="Seg_1585" s="T6">заблудиться-PST2.[3SG]</ta>
            <ta e="T8" id="Seg_1586" s="T7">осенью</ta>
            <ta e="T9" id="Seg_1587" s="T8">этот</ta>
            <ta e="T10" id="Seg_1588" s="T9">идти-CVB.SEQ</ta>
            <ta e="T11" id="Seg_1589" s="T10">дорога-3SG-ACC</ta>
            <ta e="T12" id="Seg_1590" s="T11">найти-EP-NEG.CVB</ta>
            <ta e="T13" id="Seg_1591" s="T12">медведь.[NOM]</ta>
            <ta e="T14" id="Seg_1592" s="T13">дом-3SG-DAT/LOC</ta>
            <ta e="T15" id="Seg_1593" s="T14">приходить-PST2.[3SG]</ta>
            <ta e="T16" id="Seg_1594" s="T15">этот</ta>
            <ta e="T17" id="Seg_1595" s="T16">приходить-CVB.SEQ</ta>
            <ta e="T18" id="Seg_1596" s="T17">входить-PRS.[3SG]</ta>
            <ta e="T19" id="Seg_1597" s="T18">да</ta>
            <ta e="T20" id="Seg_1598" s="T19">говорить-PRS.[3SG]</ta>
            <ta e="T21" id="Seg_1599" s="T20">вот</ta>
            <ta e="T22" id="Seg_1600" s="T21">1SG.[NOM]</ta>
            <ta e="T23" id="Seg_1601" s="T22">умирать-PTCP.PRS-EP-1SG.[NOM]</ta>
            <ta e="T24" id="Seg_1602" s="T23">ведь</ta>
            <ta e="T25" id="Seg_1603" s="T24">все.равно</ta>
            <ta e="T26" id="Seg_1604" s="T25">1SG-ACC</ta>
            <ta e="T27" id="Seg_1605" s="T26">есть-PTCP.FUT</ta>
            <ta e="T28" id="Seg_1606" s="T27">быть-TEMP-2SG</ta>
            <ta e="T29" id="Seg_1607" s="T28">есть.[IMP.2SG]</ta>
            <ta e="T30" id="Seg_1608" s="T29">этот</ta>
            <ta e="T31" id="Seg_1609" s="T30">медведь-3SG.[NOM]</ta>
            <ta e="T32" id="Seg_1610" s="T31">стоять-CVB.SEQ</ta>
            <ta e="T33" id="Seg_1611" s="T32">здесь</ta>
            <ta e="T34" id="Seg_1612" s="T33">место.позади.[NOM]</ta>
            <ta e="T35" id="Seg_1613" s="T34">двигаться-CVB.SEQ</ta>
            <ta e="T36" id="Seg_1614" s="T35">давать-PRS.[3SG]</ta>
            <ta e="T37" id="Seg_1615" s="T36">здесь</ta>
            <ta e="T38" id="Seg_1616" s="T37">жена.[NOM]</ta>
            <ta e="T39" id="Seg_1617" s="T38">жить-PRS.[3SG]</ta>
            <ta e="T40" id="Seg_1618" s="T39">этот-ACC</ta>
            <ta e="T41" id="Seg_1619" s="T40">стоять-CVB.SEQ</ta>
            <ta e="T42" id="Seg_1620" s="T41">3PL.[NOM]</ta>
            <ta e="T43" id="Seg_1621" s="T42">медведь-ACC</ta>
            <ta e="T44" id="Seg_1622" s="T43">с</ta>
            <ta e="T45" id="Seg_1623" s="T44">уснуть-CVB.SEQ</ta>
            <ta e="T46" id="Seg_1624" s="T45">оставаться-PRS-3PL</ta>
            <ta e="T47" id="Seg_1625" s="T46">медведь.[NOM]</ta>
            <ta e="T48" id="Seg_1626" s="T47">два</ta>
            <ta e="T49" id="Seg_1627" s="T48">раз-VBZ-CVB.SEQ</ta>
            <ta e="T50" id="Seg_1628" s="T49">уснуть-PRS.[3SG]</ta>
            <ta e="T51" id="Seg_1629" s="T50">говорить-PRS-3PL</ta>
            <ta e="T52" id="Seg_1630" s="T51">год.[NOM]</ta>
            <ta e="T53" id="Seg_1631" s="T52">середина-3SG-DAT/LOC</ta>
            <ta e="T54" id="Seg_1632" s="T53">Николин.день-DAT/LOC</ta>
            <ta e="T55" id="Seg_1633" s="T54">просыпаться-PRS.[3SG]</ta>
            <ta e="T56" id="Seg_1634" s="T55">говорить-PRS-3PL</ta>
            <ta e="T57" id="Seg_1635" s="T56">этот</ta>
            <ta e="T58" id="Seg_1636" s="T57">просыпаться-EP-PST2-3SG</ta>
            <ta e="T59" id="Seg_1637" s="T58">жена-3SG.[NOM]</ta>
            <ta e="T60" id="Seg_1638" s="T59">3SG.[NOM]</ta>
            <ta e="T61" id="Seg_1639" s="T60">лапа-3SG-ACC</ta>
            <ta e="T62" id="Seg_1640" s="T61">сосать-CVB.SIM</ta>
            <ta e="T63" id="Seg_1641" s="T62">лежать-PTCP.PRS</ta>
            <ta e="T64" id="Seg_1642" s="T63">быть-PRS.[3SG]</ta>
            <ta e="T65" id="Seg_1643" s="T64">потом</ta>
            <ta e="T66" id="Seg_1644" s="T65">наесться-PRS.[3SG]</ta>
            <ta e="T67" id="Seg_1645" s="T66">ребенок-3SG-ACC</ta>
            <ta e="T68" id="Seg_1646" s="T67">да</ta>
            <ta e="T69" id="Seg_1647" s="T68">наесться-EP-CAUS-PRS.[3SG]</ta>
            <ta e="T70" id="Seg_1648" s="T69">вот</ta>
            <ta e="T71" id="Seg_1649" s="T70">два</ta>
            <ta e="T72" id="Seg_1650" s="T71">раз.[NOM]</ta>
            <ta e="T73" id="Seg_1651" s="T72">спать-CVB.SEQ-3PL</ta>
            <ta e="T74" id="Seg_1652" s="T73">просыпаться-PRS-3PL</ta>
            <ta e="T75" id="Seg_1653" s="T74">жена.[NOM]</ta>
            <ta e="T76" id="Seg_1654" s="T75">этот</ta>
            <ta e="T77" id="Seg_1655" s="T76">зверь-3SG-ACC</ta>
            <ta e="T78" id="Seg_1656" s="T77">с</ta>
            <ta e="T79" id="Seg_1657" s="T78">выйти-PRS-3PL</ta>
            <ta e="T80" id="Seg_1658" s="T79">выйти-PRS-3PL</ta>
            <ta e="T81" id="Seg_1659" s="T80">да</ta>
            <ta e="T82" id="Seg_1660" s="T81">медведь-3SG.[NOM]</ta>
            <ta e="T83" id="Seg_1661" s="T82">корень-VBZ-CVB.SIM</ta>
            <ta e="T84" id="Seg_1662" s="T83">идти-CVB.SEQ</ta>
            <ta e="T85" id="Seg_1663" s="T84">жена-3SG-ACC</ta>
            <ta e="T86" id="Seg_1664" s="T85">есть-EP-CAUS-PRS.[3SG]</ta>
            <ta e="T87" id="Seg_1665" s="T86">этот</ta>
            <ta e="T88" id="Seg_1666" s="T87">идти-TEMP-3PL</ta>
            <ta e="T89" id="Seg_1667" s="T88">3PL.[NOM]</ta>
            <ta e="T90" id="Seg_1668" s="T89">стоянка-3PL-ABL</ta>
            <ta e="T91" id="Seg_1669" s="T90">один</ta>
            <ta e="T92" id="Seg_1670" s="T91">человек.[NOM]</ta>
            <ta e="T93" id="Seg_1671" s="T92">охотить-CVB.SIM</ta>
            <ta e="T94" id="Seg_1672" s="T93">идти-CVB.SEQ</ta>
            <ta e="T95" id="Seg_1673" s="T94">видеть-PRS.[3SG]</ta>
            <ta e="T96" id="Seg_1674" s="T95">видеть-PRS.[3SG]</ta>
            <ta e="T97" id="Seg_1675" s="T96">да</ta>
            <ta e="T98" id="Seg_1676" s="T97">этот</ta>
            <ta e="T99" id="Seg_1677" s="T98">жена-ACC</ta>
            <ta e="T100" id="Seg_1678" s="T99">узнавать-PRS.[3SG]</ta>
            <ta e="T101" id="Seg_1679" s="T100">рассказывать-PRS.[3SG]</ta>
            <ta e="T102" id="Seg_1680" s="T101">люди-PL-3SG-DAT/LOC</ta>
            <ta e="T103" id="Seg_1681" s="T102">мужчина-3SG-DAT/LOC</ta>
            <ta e="T104" id="Seg_1682" s="T103">этот-ACC</ta>
            <ta e="T105" id="Seg_1683" s="T104">стоять-CVB.SEQ</ta>
            <ta e="T106" id="Seg_1684" s="T105">каждый</ta>
            <ta e="T107" id="Seg_1685" s="T106">народ-3SG.[NOM]</ta>
            <ta e="T108" id="Seg_1686" s="T107">подкрадываться-CVB.SEQ</ta>
            <ta e="T109" id="Seg_1687" s="T108">приходить-CVB.SEQ</ta>
            <ta e="T110" id="Seg_1688" s="T109">стоять-CVB.SEQ</ta>
            <ta e="T111" id="Seg_1689" s="T110">после</ta>
            <ta e="T112" id="Seg_1690" s="T111">почему</ta>
            <ta e="T113" id="Seg_1691" s="T112">убить-FUT-1PL=Q</ta>
            <ta e="T114" id="Seg_1692" s="T113">говорить-CVB.SEQ</ta>
            <ta e="T115" id="Seg_1693" s="T114">приговаривать-PRS-3PL</ta>
            <ta e="T116" id="Seg_1694" s="T115">кланяться-CVB.SIM-кланяться-CVB.SIM</ta>
            <ta e="T117" id="Seg_1695" s="T116">медведь-ACC</ta>
            <ta e="T118" id="Seg_1696" s="T117">медведь.[NOM]</ta>
            <ta e="T119" id="Seg_1697" s="T118">человек-ACC</ta>
            <ta e="T120" id="Seg_1698" s="T119">давать-PTCP.FUT</ta>
            <ta e="T121" id="Seg_1699" s="T120">час-EP-2SG.[NOM]</ta>
            <ta e="T122" id="Seg_1700" s="T121">становиться-PST1-3SG</ta>
            <ta e="T123" id="Seg_1701" s="T122">кормить-PTCP.PST-2SG-DAT/LOC</ta>
            <ta e="T124" id="Seg_1702" s="T123">спасибо</ta>
            <ta e="T125" id="Seg_1703" s="T124">жена-ACC</ta>
            <ta e="T126" id="Seg_1704" s="T125">возвращаться-EP-CAUS.[IMP.2SG]</ta>
            <ta e="T127" id="Seg_1705" s="T126">позже</ta>
            <ta e="T128" id="Seg_1706" s="T127">ребенок-DAT/LOC</ta>
            <ta e="T129" id="Seg_1707" s="T128">да</ta>
            <ta e="T130" id="Seg_1708" s="T129">рассказывать-PTCP.FUT-1PL-ACC</ta>
            <ta e="T131" id="Seg_1709" s="T130">2SG.[NOM]</ta>
            <ta e="T132" id="Seg_1710" s="T131">кормить-PTCP.PST-2SG-ACC</ta>
            <ta e="T133" id="Seg_1711" s="T132">говорить-PRS-3PL</ta>
            <ta e="T134" id="Seg_1712" s="T133">этот-DAT/LOC</ta>
            <ta e="T135" id="Seg_1713" s="T134">медведь.[NOM]</ta>
            <ta e="T136" id="Seg_1714" s="T135">кричать-PRS.[3SG]</ta>
            <ta e="T137" id="Seg_1715" s="T136">очень</ta>
            <ta e="T138" id="Seg_1716" s="T137">кричать-PRS.[3SG]</ta>
            <ta e="T139" id="Seg_1717" s="T138">очень</ta>
            <ta e="T140" id="Seg_1718" s="T139">очень</ta>
            <ta e="T141" id="Seg_1719" s="T140">очень-ADVZ</ta>
            <ta e="T142" id="Seg_1720" s="T141">тот.[NOM]</ta>
            <ta e="T143" id="Seg_1721" s="T142">делать-CVB.SEQ</ta>
            <ta e="T144" id="Seg_1722" s="T143">после</ta>
            <ta e="T145" id="Seg_1723" s="T144">жена-3SG-ACC</ta>
            <ta e="T146" id="Seg_1724" s="T145">к</ta>
            <ta e="T147" id="Seg_1725" s="T146">видеть-PRS.[3SG]</ta>
            <ta e="T148" id="Seg_1726" s="T147">да</ta>
            <ta e="T149" id="Seg_1727" s="T148">люди.[NOM]</ta>
            <ta e="T150" id="Seg_1728" s="T149">к</ta>
            <ta e="T151" id="Seg_1729" s="T150">махнуть-CVB.SEQ</ta>
            <ta e="T152" id="Seg_1730" s="T151">бросать-PRS.[3SG]</ta>
            <ta e="T153" id="Seg_1731" s="T152">жена-3SG.[NOM]</ta>
            <ta e="T154" id="Seg_1732" s="T153">люди.[NOM]</ta>
            <ta e="T155" id="Seg_1733" s="T154">к</ta>
            <ta e="T156" id="Seg_1734" s="T155">идти-PRS.[3SG]</ta>
            <ta e="T157" id="Seg_1735" s="T156">да</ta>
            <ta e="T158" id="Seg_1736" s="T157">медведь-3PL.[NOM]</ta>
            <ta e="T159" id="Seg_1737" s="T158">бегать-CVB.SEQ</ta>
            <ta e="T160" id="Seg_1738" s="T159">оставаться-PRS.[3SG]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_1739" s="T0">adv</ta>
            <ta e="T2" id="Seg_1740" s="T1">cardnum</ta>
            <ta e="T3" id="Seg_1741" s="T2">n-n&gt;adj</ta>
            <ta e="T4" id="Seg_1742" s="T3">n-n&gt;adj</ta>
            <ta e="T5" id="Seg_1743" s="T4">n-n:case</ta>
            <ta e="T6" id="Seg_1744" s="T5">n-n:case</ta>
            <ta e="T7" id="Seg_1745" s="T6">v-v:tense-v:pred.pn</ta>
            <ta e="T8" id="Seg_1746" s="T7">adv</ta>
            <ta e="T9" id="Seg_1747" s="T8">dempro</ta>
            <ta e="T10" id="Seg_1748" s="T9">v-v:cvb</ta>
            <ta e="T11" id="Seg_1749" s="T10">n-n:poss-n:case</ta>
            <ta e="T12" id="Seg_1750" s="T11">v-v:(ins)-v:cvb</ta>
            <ta e="T13" id="Seg_1751" s="T12">n-n:case</ta>
            <ta e="T14" id="Seg_1752" s="T13">n-n:poss-n:case</ta>
            <ta e="T15" id="Seg_1753" s="T14">v-v:tense-v:pred.pn</ta>
            <ta e="T16" id="Seg_1754" s="T15">dempro</ta>
            <ta e="T17" id="Seg_1755" s="T16">v-v:cvb</ta>
            <ta e="T18" id="Seg_1756" s="T17">v-v:tense-v:pred.pn</ta>
            <ta e="T19" id="Seg_1757" s="T18">conj</ta>
            <ta e="T20" id="Seg_1758" s="T19">v-v:tense-v:pred.pn</ta>
            <ta e="T21" id="Seg_1759" s="T20">ptcl</ta>
            <ta e="T22" id="Seg_1760" s="T21">pers-pro:case</ta>
            <ta e="T23" id="Seg_1761" s="T22">v-v:ptcp-v:(ins)-v:(poss)-v:(case)</ta>
            <ta e="T24" id="Seg_1762" s="T23">ptcl</ta>
            <ta e="T25" id="Seg_1763" s="T24">ptcl</ta>
            <ta e="T26" id="Seg_1764" s="T25">pers-pro:case</ta>
            <ta e="T27" id="Seg_1765" s="T26">v-v:ptcp</ta>
            <ta e="T28" id="Seg_1766" s="T27">v-v:mood-v:temp.pn</ta>
            <ta e="T29" id="Seg_1767" s="T28">v-v:mood.pn</ta>
            <ta e="T30" id="Seg_1768" s="T29">dempro</ta>
            <ta e="T31" id="Seg_1769" s="T30">n-n:(poss)-n:case</ta>
            <ta e="T32" id="Seg_1770" s="T31">v-v:cvb</ta>
            <ta e="T33" id="Seg_1771" s="T32">adv</ta>
            <ta e="T34" id="Seg_1772" s="T33">n-n:case</ta>
            <ta e="T35" id="Seg_1773" s="T34">v-v:cvb</ta>
            <ta e="T36" id="Seg_1774" s="T35">v-v:tense-v:pred.pn</ta>
            <ta e="T37" id="Seg_1775" s="T36">adv</ta>
            <ta e="T38" id="Seg_1776" s="T37">n-n:case</ta>
            <ta e="T39" id="Seg_1777" s="T38">v-v:tense-v:pred.pn</ta>
            <ta e="T40" id="Seg_1778" s="T39">dempro-pro:case</ta>
            <ta e="T41" id="Seg_1779" s="T40">v-v:cvb</ta>
            <ta e="T42" id="Seg_1780" s="T41">pers-pro:case</ta>
            <ta e="T43" id="Seg_1781" s="T42">n-n:case</ta>
            <ta e="T44" id="Seg_1782" s="T43">post</ta>
            <ta e="T45" id="Seg_1783" s="T44">v-v:cvb</ta>
            <ta e="T46" id="Seg_1784" s="T45">v-v:tense-v:pred.pn</ta>
            <ta e="T47" id="Seg_1785" s="T46">n-n:case</ta>
            <ta e="T48" id="Seg_1786" s="T47">cardnum</ta>
            <ta e="T49" id="Seg_1787" s="T48">n-n&gt;v-v:cvb</ta>
            <ta e="T50" id="Seg_1788" s="T49">v-v:tense-v:pred.pn</ta>
            <ta e="T51" id="Seg_1789" s="T50">v-v:tense-v:pred.pn</ta>
            <ta e="T52" id="Seg_1790" s="T51">n-n:case</ta>
            <ta e="T53" id="Seg_1791" s="T52">n-n:poss-n:case</ta>
            <ta e="T54" id="Seg_1792" s="T53">n-n:case</ta>
            <ta e="T55" id="Seg_1793" s="T54">v-v:tense-v:pred.pn</ta>
            <ta e="T56" id="Seg_1794" s="T55">v-v:tense-v:pred.pn</ta>
            <ta e="T57" id="Seg_1795" s="T56">dempro</ta>
            <ta e="T58" id="Seg_1796" s="T57">v-v:(ins)-v:tense-v:poss.pn</ta>
            <ta e="T59" id="Seg_1797" s="T58">n-n:(poss)-n:case</ta>
            <ta e="T60" id="Seg_1798" s="T59">pers-pro:case</ta>
            <ta e="T61" id="Seg_1799" s="T60">n-n:poss-n:case</ta>
            <ta e="T62" id="Seg_1800" s="T61">v-v:cvb</ta>
            <ta e="T63" id="Seg_1801" s="T62">v-v:ptcp</ta>
            <ta e="T64" id="Seg_1802" s="T63">v-v:tense-v:pred.pn</ta>
            <ta e="T65" id="Seg_1803" s="T64">adv</ta>
            <ta e="T66" id="Seg_1804" s="T65">v-v:tense-v:pred.pn</ta>
            <ta e="T67" id="Seg_1805" s="T66">n-n:poss-n:case</ta>
            <ta e="T68" id="Seg_1806" s="T67">conj</ta>
            <ta e="T69" id="Seg_1807" s="T68">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T70" id="Seg_1808" s="T69">ptcl</ta>
            <ta e="T71" id="Seg_1809" s="T70">cardnum</ta>
            <ta e="T72" id="Seg_1810" s="T71">n-n:case</ta>
            <ta e="T73" id="Seg_1811" s="T72">v-v:cvb-v:pred.pn</ta>
            <ta e="T74" id="Seg_1812" s="T73">v-v:tense-v:pred.pn</ta>
            <ta e="T75" id="Seg_1813" s="T74">n-n:case</ta>
            <ta e="T76" id="Seg_1814" s="T75">dempro</ta>
            <ta e="T77" id="Seg_1815" s="T76">n-n:poss-n:case</ta>
            <ta e="T78" id="Seg_1816" s="T77">post</ta>
            <ta e="T79" id="Seg_1817" s="T78">v-v:tense-v:pred.pn</ta>
            <ta e="T80" id="Seg_1818" s="T79">v-v:tense-v:pred.pn</ta>
            <ta e="T81" id="Seg_1819" s="T80">conj</ta>
            <ta e="T82" id="Seg_1820" s="T81">n-n:(poss)-n:case</ta>
            <ta e="T83" id="Seg_1821" s="T82">n-n&gt;v-v:cvb</ta>
            <ta e="T84" id="Seg_1822" s="T83">v-v:cvb</ta>
            <ta e="T85" id="Seg_1823" s="T84">n-n:poss-n:case</ta>
            <ta e="T86" id="Seg_1824" s="T85">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T87" id="Seg_1825" s="T86">dempro</ta>
            <ta e="T88" id="Seg_1826" s="T87">v-v:mood-v:temp.pn</ta>
            <ta e="T89" id="Seg_1827" s="T88">pers-pro:case</ta>
            <ta e="T90" id="Seg_1828" s="T89">n-n:poss-n:case</ta>
            <ta e="T91" id="Seg_1829" s="T90">cardnum</ta>
            <ta e="T92" id="Seg_1830" s="T91">n-n:case</ta>
            <ta e="T93" id="Seg_1831" s="T92">v-v:cvb</ta>
            <ta e="T94" id="Seg_1832" s="T93">v-v:cvb</ta>
            <ta e="T95" id="Seg_1833" s="T94">v-v:tense-v:pred.pn</ta>
            <ta e="T96" id="Seg_1834" s="T95">v-v:tense-v:pred.pn</ta>
            <ta e="T97" id="Seg_1835" s="T96">conj</ta>
            <ta e="T98" id="Seg_1836" s="T97">dempro</ta>
            <ta e="T99" id="Seg_1837" s="T98">n-n:case</ta>
            <ta e="T100" id="Seg_1838" s="T99">v-v:tense-v:pred.pn</ta>
            <ta e="T101" id="Seg_1839" s="T100">v-v:tense-v:pred.pn</ta>
            <ta e="T102" id="Seg_1840" s="T101">n-n:(num)-n:poss-n:case</ta>
            <ta e="T103" id="Seg_1841" s="T102">n-n:poss-n:case</ta>
            <ta e="T104" id="Seg_1842" s="T103">dempro-pro:case</ta>
            <ta e="T105" id="Seg_1843" s="T104">v-v:cvb</ta>
            <ta e="T106" id="Seg_1844" s="T105">adj</ta>
            <ta e="T107" id="Seg_1845" s="T106">n-n:(poss)-n:case</ta>
            <ta e="T108" id="Seg_1846" s="T107">v-v:cvb</ta>
            <ta e="T109" id="Seg_1847" s="T108">v-v:cvb</ta>
            <ta e="T110" id="Seg_1848" s="T109">v-v:cvb</ta>
            <ta e="T111" id="Seg_1849" s="T110">post</ta>
            <ta e="T112" id="Seg_1850" s="T111">que</ta>
            <ta e="T113" id="Seg_1851" s="T112">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T114" id="Seg_1852" s="T113">v-v:cvb</ta>
            <ta e="T115" id="Seg_1853" s="T114">v-v:tense-v:pred.pn</ta>
            <ta e="T116" id="Seg_1854" s="T115">v-v:cvb-v-v:cvb</ta>
            <ta e="T117" id="Seg_1855" s="T116">n-n:case</ta>
            <ta e="T118" id="Seg_1856" s="T117">n-n:case</ta>
            <ta e="T119" id="Seg_1857" s="T118">n-n:case</ta>
            <ta e="T120" id="Seg_1858" s="T119">v-v:ptcp</ta>
            <ta e="T121" id="Seg_1859" s="T120">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T122" id="Seg_1860" s="T121">v-v:tense-v:poss.pn</ta>
            <ta e="T123" id="Seg_1861" s="T122">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T124" id="Seg_1862" s="T123">ptcl</ta>
            <ta e="T125" id="Seg_1863" s="T124">n-n:case</ta>
            <ta e="T126" id="Seg_1864" s="T125">v-v:(ins)-v&gt;v-v:mood.pn</ta>
            <ta e="T127" id="Seg_1865" s="T126">adv</ta>
            <ta e="T128" id="Seg_1866" s="T127">n-n:case</ta>
            <ta e="T129" id="Seg_1867" s="T128">conj</ta>
            <ta e="T130" id="Seg_1868" s="T129">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T131" id="Seg_1869" s="T130">pers-pro:case</ta>
            <ta e="T132" id="Seg_1870" s="T131">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T133" id="Seg_1871" s="T132">v-v:tense-v:pred.pn</ta>
            <ta e="T134" id="Seg_1872" s="T133">dempro-pro:case</ta>
            <ta e="T135" id="Seg_1873" s="T134">n-n:case</ta>
            <ta e="T136" id="Seg_1874" s="T135">v-v:tense-v:pred.pn</ta>
            <ta e="T137" id="Seg_1875" s="T136">ptcl</ta>
            <ta e="T138" id="Seg_1876" s="T137">v-v:tense-v:pred.pn</ta>
            <ta e="T139" id="Seg_1877" s="T138">ptcl</ta>
            <ta e="T140" id="Seg_1878" s="T139">adv</ta>
            <ta e="T141" id="Seg_1879" s="T140">ptcl-ptcl&gt;adv</ta>
            <ta e="T142" id="Seg_1880" s="T141">dempro-pro:case</ta>
            <ta e="T143" id="Seg_1881" s="T142">v-v:cvb</ta>
            <ta e="T144" id="Seg_1882" s="T143">post</ta>
            <ta e="T145" id="Seg_1883" s="T144">n-n:poss-n:case</ta>
            <ta e="T146" id="Seg_1884" s="T145">post</ta>
            <ta e="T147" id="Seg_1885" s="T146">v-v:tense-v:pred.pn</ta>
            <ta e="T148" id="Seg_1886" s="T147">conj</ta>
            <ta e="T149" id="Seg_1887" s="T148">n-n:case</ta>
            <ta e="T150" id="Seg_1888" s="T149">post</ta>
            <ta e="T151" id="Seg_1889" s="T150">v-v:cvb</ta>
            <ta e="T152" id="Seg_1890" s="T151">v-v:tense-v:pred.pn</ta>
            <ta e="T153" id="Seg_1891" s="T152">n-n:(poss)-n:case</ta>
            <ta e="T154" id="Seg_1892" s="T153">n-n:case</ta>
            <ta e="T155" id="Seg_1893" s="T154">post</ta>
            <ta e="T156" id="Seg_1894" s="T155">v-v:tense-v:pred.pn</ta>
            <ta e="T157" id="Seg_1895" s="T156">conj</ta>
            <ta e="T158" id="Seg_1896" s="T157">n-n:(poss)-n:case</ta>
            <ta e="T159" id="Seg_1897" s="T158">v-v:cvb</ta>
            <ta e="T160" id="Seg_1898" s="T159">v-v:tense-v:pred.pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_1899" s="T0">adv</ta>
            <ta e="T2" id="Seg_1900" s="T1">cardnum</ta>
            <ta e="T3" id="Seg_1901" s="T2">adj</ta>
            <ta e="T4" id="Seg_1902" s="T3">adj</ta>
            <ta e="T5" id="Seg_1903" s="T4">n</ta>
            <ta e="T6" id="Seg_1904" s="T5">n</ta>
            <ta e="T7" id="Seg_1905" s="T6">v</ta>
            <ta e="T8" id="Seg_1906" s="T7">adv</ta>
            <ta e="T9" id="Seg_1907" s="T8">dempro</ta>
            <ta e="T10" id="Seg_1908" s="T9">v</ta>
            <ta e="T11" id="Seg_1909" s="T10">n</ta>
            <ta e="T12" id="Seg_1910" s="T11">v</ta>
            <ta e="T13" id="Seg_1911" s="T12">n</ta>
            <ta e="T14" id="Seg_1912" s="T13">n</ta>
            <ta e="T15" id="Seg_1913" s="T14">v</ta>
            <ta e="T16" id="Seg_1914" s="T15">dempro</ta>
            <ta e="T17" id="Seg_1915" s="T16">v</ta>
            <ta e="T18" id="Seg_1916" s="T17">v</ta>
            <ta e="T19" id="Seg_1917" s="T18">conj</ta>
            <ta e="T20" id="Seg_1918" s="T19">v</ta>
            <ta e="T21" id="Seg_1919" s="T20">ptcl</ta>
            <ta e="T22" id="Seg_1920" s="T21">pers</ta>
            <ta e="T23" id="Seg_1921" s="T22">v</ta>
            <ta e="T24" id="Seg_1922" s="T23">ptcl</ta>
            <ta e="T25" id="Seg_1923" s="T24">ptcl</ta>
            <ta e="T26" id="Seg_1924" s="T25">pers</ta>
            <ta e="T27" id="Seg_1925" s="T26">v</ta>
            <ta e="T28" id="Seg_1926" s="T27">aux</ta>
            <ta e="T29" id="Seg_1927" s="T28">v</ta>
            <ta e="T30" id="Seg_1928" s="T29">dempro</ta>
            <ta e="T31" id="Seg_1929" s="T30">n</ta>
            <ta e="T32" id="Seg_1930" s="T31">v</ta>
            <ta e="T33" id="Seg_1931" s="T32">adv</ta>
            <ta e="T34" id="Seg_1932" s="T33">n</ta>
            <ta e="T35" id="Seg_1933" s="T34">v</ta>
            <ta e="T36" id="Seg_1934" s="T35">aux</ta>
            <ta e="T37" id="Seg_1935" s="T36">adv</ta>
            <ta e="T38" id="Seg_1936" s="T37">n</ta>
            <ta e="T39" id="Seg_1937" s="T38">v</ta>
            <ta e="T40" id="Seg_1938" s="T39">dempro</ta>
            <ta e="T41" id="Seg_1939" s="T40">v</ta>
            <ta e="T42" id="Seg_1940" s="T41">pers</ta>
            <ta e="T43" id="Seg_1941" s="T42">n</ta>
            <ta e="T44" id="Seg_1942" s="T43">post</ta>
            <ta e="T45" id="Seg_1943" s="T44">v</ta>
            <ta e="T46" id="Seg_1944" s="T45">aux</ta>
            <ta e="T47" id="Seg_1945" s="T46">n</ta>
            <ta e="T48" id="Seg_1946" s="T47">cardnum</ta>
            <ta e="T49" id="Seg_1947" s="T48">v</ta>
            <ta e="T50" id="Seg_1948" s="T49">v</ta>
            <ta e="T51" id="Seg_1949" s="T50">v</ta>
            <ta e="T52" id="Seg_1950" s="T51">n</ta>
            <ta e="T53" id="Seg_1951" s="T52">n</ta>
            <ta e="T54" id="Seg_1952" s="T53">n</ta>
            <ta e="T55" id="Seg_1953" s="T54">v</ta>
            <ta e="T56" id="Seg_1954" s="T55">v</ta>
            <ta e="T57" id="Seg_1955" s="T56">dempro</ta>
            <ta e="T58" id="Seg_1956" s="T57">v</ta>
            <ta e="T59" id="Seg_1957" s="T58">n</ta>
            <ta e="T60" id="Seg_1958" s="T59">pers</ta>
            <ta e="T61" id="Seg_1959" s="T60">n</ta>
            <ta e="T62" id="Seg_1960" s="T61">v</ta>
            <ta e="T63" id="Seg_1961" s="T62">aux</ta>
            <ta e="T64" id="Seg_1962" s="T63">aux</ta>
            <ta e="T65" id="Seg_1963" s="T64">adv</ta>
            <ta e="T66" id="Seg_1964" s="T65">v</ta>
            <ta e="T67" id="Seg_1965" s="T66">n</ta>
            <ta e="T68" id="Seg_1966" s="T67">conj</ta>
            <ta e="T69" id="Seg_1967" s="T68">v</ta>
            <ta e="T70" id="Seg_1968" s="T69">ptcl</ta>
            <ta e="T71" id="Seg_1969" s="T70">cardnum</ta>
            <ta e="T72" id="Seg_1970" s="T71">n</ta>
            <ta e="T73" id="Seg_1971" s="T72">v</ta>
            <ta e="T74" id="Seg_1972" s="T73">v</ta>
            <ta e="T75" id="Seg_1973" s="T74">n</ta>
            <ta e="T76" id="Seg_1974" s="T75">dempro</ta>
            <ta e="T77" id="Seg_1975" s="T76">n</ta>
            <ta e="T78" id="Seg_1976" s="T77">post</ta>
            <ta e="T79" id="Seg_1977" s="T78">v</ta>
            <ta e="T80" id="Seg_1978" s="T79">v</ta>
            <ta e="T81" id="Seg_1979" s="T80">conj</ta>
            <ta e="T82" id="Seg_1980" s="T81">n</ta>
            <ta e="T83" id="Seg_1981" s="T82">v</ta>
            <ta e="T84" id="Seg_1982" s="T83">aux</ta>
            <ta e="T85" id="Seg_1983" s="T84">n</ta>
            <ta e="T86" id="Seg_1984" s="T85">v</ta>
            <ta e="T87" id="Seg_1985" s="T86">dempro</ta>
            <ta e="T88" id="Seg_1986" s="T87">v</ta>
            <ta e="T89" id="Seg_1987" s="T88">pers</ta>
            <ta e="T90" id="Seg_1988" s="T89">n</ta>
            <ta e="T91" id="Seg_1989" s="T90">cardnum</ta>
            <ta e="T92" id="Seg_1990" s="T91">n</ta>
            <ta e="T93" id="Seg_1991" s="T92">v</ta>
            <ta e="T94" id="Seg_1992" s="T93">aux</ta>
            <ta e="T95" id="Seg_1993" s="T94">v</ta>
            <ta e="T96" id="Seg_1994" s="T95">v</ta>
            <ta e="T97" id="Seg_1995" s="T96">conj</ta>
            <ta e="T98" id="Seg_1996" s="T97">dempro</ta>
            <ta e="T99" id="Seg_1997" s="T98">n</ta>
            <ta e="T100" id="Seg_1998" s="T99">v</ta>
            <ta e="T101" id="Seg_1999" s="T100">v</ta>
            <ta e="T102" id="Seg_2000" s="T101">n</ta>
            <ta e="T103" id="Seg_2001" s="T102">n</ta>
            <ta e="T104" id="Seg_2002" s="T103">dempro</ta>
            <ta e="T105" id="Seg_2003" s="T104">v</ta>
            <ta e="T106" id="Seg_2004" s="T105">adj</ta>
            <ta e="T107" id="Seg_2005" s="T106">n</ta>
            <ta e="T108" id="Seg_2006" s="T107">v</ta>
            <ta e="T109" id="Seg_2007" s="T108">v</ta>
            <ta e="T110" id="Seg_2008" s="T109">aux</ta>
            <ta e="T111" id="Seg_2009" s="T110">post</ta>
            <ta e="T112" id="Seg_2010" s="T111">que</ta>
            <ta e="T113" id="Seg_2011" s="T112">v</ta>
            <ta e="T114" id="Seg_2012" s="T113">v</ta>
            <ta e="T115" id="Seg_2013" s="T114">v</ta>
            <ta e="T116" id="Seg_2014" s="T115">v</ta>
            <ta e="T117" id="Seg_2015" s="T116">n</ta>
            <ta e="T118" id="Seg_2016" s="T117">n</ta>
            <ta e="T119" id="Seg_2017" s="T118">n</ta>
            <ta e="T120" id="Seg_2018" s="T119">v</ta>
            <ta e="T121" id="Seg_2019" s="T120">n</ta>
            <ta e="T122" id="Seg_2020" s="T121">cop</ta>
            <ta e="T123" id="Seg_2021" s="T122">v</ta>
            <ta e="T124" id="Seg_2022" s="T123">ptcl</ta>
            <ta e="T125" id="Seg_2023" s="T124">n</ta>
            <ta e="T126" id="Seg_2024" s="T125">v</ta>
            <ta e="T127" id="Seg_2025" s="T126">adv</ta>
            <ta e="T128" id="Seg_2026" s="T127">n</ta>
            <ta e="T129" id="Seg_2027" s="T128">conj</ta>
            <ta e="T130" id="Seg_2028" s="T129">v</ta>
            <ta e="T131" id="Seg_2029" s="T130">pers</ta>
            <ta e="T132" id="Seg_2030" s="T131">v</ta>
            <ta e="T133" id="Seg_2031" s="T132">v</ta>
            <ta e="T134" id="Seg_2032" s="T133">dempro</ta>
            <ta e="T135" id="Seg_2033" s="T134">n</ta>
            <ta e="T136" id="Seg_2034" s="T135">v</ta>
            <ta e="T137" id="Seg_2035" s="T136">ptcl</ta>
            <ta e="T138" id="Seg_2036" s="T137">v</ta>
            <ta e="T139" id="Seg_2037" s="T138">ptcl</ta>
            <ta e="T140" id="Seg_2038" s="T139">adv</ta>
            <ta e="T141" id="Seg_2039" s="T140">adv</ta>
            <ta e="T142" id="Seg_2040" s="T141">dempro</ta>
            <ta e="T143" id="Seg_2041" s="T142">v</ta>
            <ta e="T144" id="Seg_2042" s="T143">post</ta>
            <ta e="T145" id="Seg_2043" s="T144">n</ta>
            <ta e="T146" id="Seg_2044" s="T145">post</ta>
            <ta e="T147" id="Seg_2045" s="T146">v</ta>
            <ta e="T148" id="Seg_2046" s="T147">conj</ta>
            <ta e="T149" id="Seg_2047" s="T148">n</ta>
            <ta e="T150" id="Seg_2048" s="T149">post</ta>
            <ta e="T151" id="Seg_2049" s="T150">v</ta>
            <ta e="T152" id="Seg_2050" s="T151">aux</ta>
            <ta e="T153" id="Seg_2051" s="T152">n</ta>
            <ta e="T154" id="Seg_2052" s="T153">n</ta>
            <ta e="T155" id="Seg_2053" s="T154">post</ta>
            <ta e="T156" id="Seg_2054" s="T155">v</ta>
            <ta e="T157" id="Seg_2055" s="T156">conj</ta>
            <ta e="T158" id="Seg_2056" s="T157">n</ta>
            <ta e="T159" id="Seg_2057" s="T158">v</ta>
            <ta e="T160" id="Seg_2058" s="T159">aux</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR" />
         <annotation name="SyF" tierref="SyF" />
         <annotation name="IST" tierref="IST" />
         <annotation name="Top" tierref="Top" />
         <annotation name="Foc" tierref="Foc" />
         <annotation name="BOR" tierref="BOR" />
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T8" id="Seg_2059" s="T0">Long ago in fall one woman with a newborn child got lost in the forest.</ta>
            <ta e="T15" id="Seg_2060" s="T8">Wandering around she didn't find the way and came to a bear's den.</ta>
            <ta e="T20" id="Seg_2061" s="T15">She came there, went in and said:</ta>
            <ta e="T25" id="Seg_2062" s="T20">"I have to die eitherway.</ta>
            <ta e="T29" id="Seg_2063" s="T25">If you want to eat me, then eat."</ta>
            <ta e="T36" id="Seg_2064" s="T29">The bear only moved further back into the den.</ta>
            <ta e="T39" id="Seg_2065" s="T36">So the woman lived there.</ta>
            <ta e="T46" id="Seg_2066" s="T39">So they fell asleep together with the bear.</ta>
            <ta e="T51" id="Seg_2067" s="T46">It is said that bears fall asleep twice in winter.</ta>
            <ta e="T56" id="Seg_2068" s="T51">In the middle of the winter, on St. Nicholas Day, they wake up.</ta>
            <ta e="T69" id="Seg_2069" s="T56">The woman woke up: she sucked the bear's paw, so she ate her fill and she fed her child too.</ta>
            <ta e="T74" id="Seg_2070" s="T69">Well, they fell asleep twice and woke up again.</ta>
            <ta e="T79" id="Seg_2071" s="T74">The woman went out together with the animal.</ta>
            <ta e="T86" id="Seg_2072" s="T79">They went outside, the bear gathered (edible?) roots and fed the woman.</ta>
            <ta e="T103" id="Seg_2073" s="T86">As they walked like that, one human from their settlement was hunting and saw them; seeing them he recognized that woman and told about it to his people, to her husband.</ta>
            <ta e="T117" id="Seg_2074" s="T103">Stalking to the bear all the people mumbled to themselves: "Why would we kill you?", and bowed themselves in front of the bear:</ta>
            <ta e="T133" id="Seg_2075" s="T117">"Bear, it's time to return the human, thank you for feeding her, now give the woman back, for that we can tell our children how you hosted her", they said.</ta>
            <ta e="T152" id="Seg_2076" s="T133">The bear shouted very loudly on that, he shouted very loudly, then he looked at the woman and waved in the direction of the people.</ta>
            <ta e="T160" id="Seg_2077" s="T152">The woman ran to the people and the bear ran away.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T8" id="Seg_2078" s="T0">Vor langer Zeit verirrte sich im Herbst eine Frau mit einem Kind im Wiegenalter im Wald.</ta>
            <ta e="T15" id="Seg_2079" s="T8">So herumirrend fand sie nicht den Weg und kam zu einer Bärenhöhle.</ta>
            <ta e="T20" id="Seg_2080" s="T15">Sie kam dorthin, ging hinein und sagte:</ta>
            <ta e="T25" id="Seg_2081" s="T20">"Ich muss doch so oder so sterben.</ta>
            <ta e="T29" id="Seg_2082" s="T25">Wenn du mich fressen möchtest, dann friss mich."</ta>
            <ta e="T36" id="Seg_2083" s="T29">Der Bär zog sich nur weiter in die Höhle zurück.</ta>
            <ta e="T39" id="Seg_2084" s="T36">So lebte die Frau dort.</ta>
            <ta e="T46" id="Seg_2085" s="T39">So schliefen sie gemeinsam mit dem Bären ein.</ta>
            <ta e="T51" id="Seg_2086" s="T46">Man sagt, dass Bären im Winter zwei mal einschlafen.</ta>
            <ta e="T56" id="Seg_2087" s="T51">In der Mitte des Jahres/des Winters, am Nikolaustag wachen sie auf, sagt man.</ta>
            <ta e="T69" id="Seg_2088" s="T56">Sie wachten auf, die Frau saugte an der Pfote des Bären, so sättigt sie sich, dann sättigt sie auch ihr Kind.</ta>
            <ta e="T74" id="Seg_2089" s="T69">Nun, sie schliefen zwei mal ein und wachten wieder auf.</ta>
            <ta e="T79" id="Seg_2090" s="T74">Die Frau ging mit dem Tier zusammen hinaus.</ta>
            <ta e="T86" id="Seg_2091" s="T79">Sie gingen hinaus, der Bär sammelte (essbare?) Wurzeln und ernährte die Frau.</ta>
            <ta e="T103" id="Seg_2092" s="T86">Als sie so gingen, war ein Mensch aus ihrer Siedlung auf Jagd und sah sie; er sah sie und erkannte die Frau, er erzählte es seinen Leuten und ihrem Mann.</ta>
            <ta e="T117" id="Seg_2093" s="T103">Alle ihre Leute schlichen sich da [an den Bären] an, murmelten "Warum sollen wir dich töten?" vor sich hin und verbeugten sich vor dem Bären:</ta>
            <ta e="T133" id="Seg_2094" s="T117">"Bär, es ist die Zeit gekommen, den Menschen zurückzugeben, danke, dass du sie ernährt hast, nun gib die Frau zurück, damit wir später auch den Kindern erzählen können, wie du sie bewirtet hast", sagten sie.</ta>
            <ta e="T152" id="Seg_2095" s="T133">Darauf schrie der Bär auf, er schrie sehr laut, danach sah er zu der Frau und winkte in Richtung der Leute.</ta>
            <ta e="T160" id="Seg_2096" s="T152">Die Frau lief zu den Leuten und der Bär lief weg.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T8" id="Seg_2097" s="T0">В старину одна женщина с грудным младенцем осенью заблудилась в лесу.</ta>
            <ta e="T15" id="Seg_2098" s="T8">Так блуждая, не найдя дороги, пришла к берлоге медведя.</ta>
            <ta e="T20" id="Seg_2099" s="T15">Тут же зашла в нее и сказала:</ta>
            <ta e="T25" id="Seg_2100" s="T20">"Мне ведь все равно погибать.</ta>
            <ta e="T29" id="Seg_2101" s="T25">Если захочешь съесть меня, ешь."</ta>
            <ta e="T36" id="Seg_2102" s="T29">Но медведь только отодвинулся в глубь берлоги.</ta>
            <ta e="T39" id="Seg_2103" s="T36">Женщина осталась жить. </ta>
            <ta e="T46" id="Seg_2104" s="T39">Тут-то они с медведем засыпают.</ta>
            <ta e="T51" id="Seg_2105" s="T46">Медведи зимой дважды впадают в спячку, говорят.</ta>
            <ta e="T56" id="Seg_2106" s="T51">В середине зимы, в Николин день, проснулись, говорят.</ta>
            <ta e="T69" id="Seg_2107" s="T56">Вот проснулись: женщина его лапу сосет, этим наедается, ребенку тоже дает насытиться.</ta>
            <ta e="T74" id="Seg_2108" s="T69">Ну, снова впали в спячку и проснулись.</ta>
            <ta e="T79" id="Seg_2109" s="T74">Женщина вместе с тем зверем вышла [из берлоги].</ta>
            <ta e="T86" id="Seg_2110" s="T79">Вот вышли, медведь, собирая (съедобные?) корни, кормил женщину. </ta>
            <ta e="T103" id="Seg_2111" s="T86">Когда они так ходили, один человек из ее стойбища, будучи на охоте, увидел их. Присмотревшись, сразу узнал ту женщину, рассказал ее родственникам, мужу ее.</ta>
            <ta e="T117" id="Seg_2112" s="T103">Тут все ее родственники потихоньку подкрадываяась и приговаривая: "За что нам тебя убивать?", стали медведь просить, кланяясь:</ta>
            <ta e="T133" id="Seg_2113" s="T117">"Медведь, пришло время возвратить человека, спасибо, что кормил, возврати женщину, чтобы позже мы и детям рассказывали, как ты ее приютил", сказали.</ta>
            <ta e="T152" id="Seg_2114" s="T133">На это медведь взревел, страшно так закричал, а потом поглядел на женщину и махнул (лапой) в сторону людей.</ta>
            <ta e="T160" id="Seg_2115" s="T152">Женщина пошла к людям, а медведь убежал.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T8" id="Seg_2116" s="T0">В старину одна женщина с грудным младенцем осенью заблудилась в лесу.</ta>
            <ta e="T15" id="Seg_2117" s="T8">Так блуждая, не найдя дороги, пришла к берлоге медведя.</ta>
            <ta e="T20" id="Seg_2118" s="T15">Тут же зашла в нее и говорит:</ta>
            <ta e="T25" id="Seg_2119" s="T20">— Мне ведь все равно погибать.</ta>
            <ta e="T29" id="Seg_2120" s="T25">Если захочешь съесть меня, ешь.</ta>
            <ta e="T36" id="Seg_2121" s="T29">Но медведь только отодвинулся в глубь берлоги.</ta>
            <ta e="T39" id="Seg_2122" s="T36">Женщина осталась жить. </ta>
            <ta e="T46" id="Seg_2123" s="T39">Тут-то они с медведем засыпают. </ta>
            <ta e="T51" id="Seg_2124" s="T46">Медведи зимой дважды впадают в спячку, говорят. </ta>
            <ta e="T56" id="Seg_2125" s="T51">В середине зимы, в Николин день, проснулись, говорят. </ta>
            <ta e="T69" id="Seg_2126" s="T56">Вот проснулись: женщина его лапу сосет, этим наедается, ребенку тоже дает насытиться. </ta>
            <ta e="T74" id="Seg_2127" s="T69">Ну, снова впали в спячку. </ta>
            <ta e="T79" id="Seg_2128" s="T74">Женщина вместе с тем зверем вышла [из берлоги]. </ta>
            <ta e="T86" id="Seg_2129" s="T79">Вот вышли, медведь, собирая съедобные корки, кормил женщину. </ta>
            <ta e="T103" id="Seg_2130" s="T86">Когда они так ходили, один человек из ее стойбища, будучи на охоте, увидел их. Присмотревшись, сразу узнал ту женщину, рассказал ее родственникам, мужу ее.</ta>
            <ta e="T117" id="Seg_2131" s="T103">Тут все ее родственники потихоньку подкрадываяась и приговаривая: "За что нам тебя убивать?" стали медведь просить, кланяясь:</ta>
            <ta e="T133" id="Seg_2132" s="T117">— Медведь, пришло время возвратить человека, спасибо, что кормил, возврати женщину, чтобы позже мы и детям рассказывали, как ты ее приютил.</ta>
            <ta e="T152" id="Seg_2133" s="T133">На это медведь взревел, страшно так закричал, а потом поглядел на женщину и махнул (лапой) в сторону людей.</ta>
            <ta e="T160" id="Seg_2134" s="T152">Женщина пошла к людям, а медведь убежал.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T56" id="Seg_2135" s="T51">[DCh]: St. Nicholas' day is according to the orthodoxe calender on 19th/20th december, so more or less at midwinter.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg"
                          name="gg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top"
                          name="Top"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc"
                          name="Foc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
