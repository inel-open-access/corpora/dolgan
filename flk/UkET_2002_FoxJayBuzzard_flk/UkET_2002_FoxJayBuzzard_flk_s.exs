<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDD80DA7E3-3100-5463-B48D-E5174806E5D3">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>UkET_2002_FoxJayBuzzard_flk</transcription-name>
         <referenced-file url="UkET_2002_FoxJayBuzzard_flk.wav" />
         <referenced-file url="UkET_2002_FoxJayBuzzard_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\flk\UkET_2002_FoxJayBuzzard_flk\UkET_2002_FoxJayBuzzard_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">931</ud-information>
            <ud-information attribute-name="# HIAT:w">672</ud-information>
            <ud-information attribute-name="# e">672</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">1</ud-information>
            <ud-information attribute-name="# HIAT:u">68</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="UkET">
            <abbreviation>UkET</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.0" type="appl" />
         <tli id="T1" time="0.542" type="appl" />
         <tli id="T2" time="1.084" type="appl" />
         <tli id="T3" time="1.625" type="appl" />
         <tli id="T4" time="2.167" type="appl" />
         <tli id="T5" time="2.709" type="appl" />
         <tli id="T6" time="3.251" type="appl" />
         <tli id="T7" time="3.792" type="appl" />
         <tli id="T8" time="4.334" type="appl" />
         <tli id="T9" time="4.876" type="appl" />
         <tli id="T10" time="5.25" type="appl" />
         <tli id="T11" time="5.623" type="appl" />
         <tli id="T12" time="5.997" type="appl" />
         <tli id="T13" time="6.37" type="appl" />
         <tli id="T14" time="6.744" type="appl" />
         <tli id="T15" time="7.117" type="appl" />
         <tli id="T16" time="7.491" type="appl" />
         <tli id="T17" time="7.864" type="appl" />
         <tli id="T18" time="8.351332718830882" />
         <tli id="T19" time="9.044" type="appl" />
         <tli id="T20" time="9.538" type="appl" />
         <tli id="T21" time="10.031" type="appl" />
         <tli id="T22" time="10.525" type="appl" />
         <tli id="T23" time="11.019" type="appl" />
         <tli id="T24" time="11.512" type="appl" />
         <tli id="T25" time="12.006" type="appl" />
         <tli id="T26" time="12.5" type="appl" />
         <tli id="T27" time="12.994" type="appl" />
         <tli id="T28" time="13.488" type="appl" />
         <tli id="T29" time="13.981" type="appl" />
         <tli id="T30" time="14.475" type="appl" />
         <tli id="T31" time="14.969" type="appl" />
         <tli id="T32" time="15.462" type="appl" />
         <tli id="T33" time="15.956" type="appl" />
         <tli id="T34" time="16.58999846804534" />
         <tli id="T35" time="16.914" type="appl" />
         <tli id="T36" time="17.377" type="appl" />
         <tli id="T37" time="17.841" type="appl" />
         <tli id="T38" time="18.305" type="appl" />
         <tli id="T39" time="18.768" type="appl" />
         <tli id="T40" time="19.232" type="appl" />
         <tli id="T41" time="19.695" type="appl" />
         <tli id="T42" time="20.159" type="appl" />
         <tli id="T43" time="20.623" type="appl" />
         <tli id="T44" time="21.086" type="appl" />
         <tli id="T45" time="21.55" type="appl" />
         <tli id="T46" time="22.101" type="appl" />
         <tli id="T47" time="22.652" type="appl" />
         <tli id="T48" time="23.203" type="appl" />
         <tli id="T49" time="23.754" type="appl" />
         <tli id="T50" time="24.306" type="appl" />
         <tli id="T51" time="24.857" type="appl" />
         <tli id="T52" time="25.408" type="appl" />
         <tli id="T53" time="25.959" type="appl" />
         <tli id="T54" time="26.51" type="appl" />
         <tli id="T55" time="27.353169794330668" />
         <tli id="T56" time="27.905" type="appl" />
         <tli id="T57" time="28.388" type="appl" />
         <tli id="T58" time="28.87" type="appl" />
         <tli id="T59" time="29.353" type="appl" />
         <tli id="T60" time="29.835" type="appl" />
         <tli id="T61" time="30.318" type="appl" />
         <tli id="T62" time="30.866667668054472" />
         <tli id="T63" time="31.239" type="appl" />
         <tli id="T64" time="31.677" type="appl" />
         <tli id="T65" time="32.116" type="appl" />
         <tli id="T66" time="32.555" type="appl" />
         <tli id="T67" time="32.993" type="appl" />
         <tli id="T68" time="33.432" type="appl" />
         <tli id="T69" time="33.87" type="appl" />
         <tli id="T70" time="34.309" type="appl" />
         <tli id="T71" time="34.748" type="appl" />
         <tli id="T72" time="35.186" type="appl" />
         <tli id="T73" time="35.625" type="appl" />
         <tli id="T74" time="36.064" type="appl" />
         <tli id="T75" time="36.502" type="appl" />
         <tli id="T76" time="36.941" type="appl" />
         <tli id="T77" time="37.38" type="appl" />
         <tli id="T78" time="37.818" type="appl" />
         <tli id="T79" time="38.257" type="appl" />
         <tli id="T80" time="38.695" type="appl" />
         <tli id="T81" time="39.134" type="appl" />
         <tli id="T82" time="39.573" type="appl" />
         <tli id="T83" time="40.011" type="appl" />
         <tli id="T84" time="40.48999880400263" />
         <tli id="T85" time="40.871" type="appl" />
         <tli id="T86" time="41.293" type="appl" />
         <tli id="T87" time="41.714" type="appl" />
         <tli id="T88" time="42.136" type="appl" />
         <tli id="T89" time="42.557" type="appl" />
         <tli id="T90" time="42.979" type="appl" />
         <tli id="T91" time="43.4" type="appl" />
         <tli id="T92" time="43.821" type="appl" />
         <tli id="T93" time="44.243" type="appl" />
         <tli id="T94" time="44.664" type="appl" />
         <tli id="T95" time="45.086" type="appl" />
         <tli id="T96" time="45.507" type="appl" />
         <tli id="T97" time="45.929" type="appl" />
         <tli id="T98" time="47.19305117574137" />
         <tli id="T99" time="47.561" type="appl" />
         <tli id="T100" time="47.938" type="appl" />
         <tli id="T101" time="48.315" type="appl" />
         <tli id="T102" time="48.692" type="appl" />
         <tli id="T103" time="49.39970464927864" />
         <tli id="T104" time="49.815650445487975" type="intp" />
         <tli id="T105" time="50.23159624169731" type="intp" />
         <tli id="T106" time="50.647542037906646" type="intp" />
         <tli id="T107" time="51.06348783411598" type="intp" />
         <tli id="T108" time="51.47943363032532" type="intp" />
         <tli id="T109" time="51.89537942653465" type="intp" />
         <tli id="T110" time="52.31132522274399" type="intp" />
         <tli id="T111" time="52.727271018953324" type="intp" />
         <tli id="T112" time="53.14321681516266" type="intp" />
         <tli id="T113" time="53.559162611371995" type="intp" />
         <tli id="T114" time="53.97510840758133" type="intp" />
         <tli id="T115" time="54.43772140300782" />
         <tli id="T116" time="54.807" type="appl" />
         <tli id="T117" time="55.364" type="appl" />
         <tli id="T118" time="55.92" type="appl" />
         <tli id="T119" time="56.477" type="appl" />
         <tli id="T120" time="57.034" type="appl" />
         <tli id="T121" time="57.591" type="appl" />
         <tli id="T122" time="58.148" type="appl" />
         <tli id="T123" time="58.705" type="appl" />
         <tli id="T124" time="59.261" type="appl" />
         <tli id="T125" time="59.818" type="appl" />
         <tli id="T126" time="60.375" type="appl" />
         <tli id="T127" time="61.75" type="appl" />
         <tli id="T128" time="62.359" type="appl" />
         <tli id="T129" time="62.968" type="appl" />
         <tli id="T130" time="63.577" type="appl" />
         <tli id="T131" time="64.186" type="appl" />
         <tli id="T132" time="64.795" type="appl" />
         <tli id="T133" time="65.405" type="appl" />
         <tli id="T134" time="66.014" type="appl" />
         <tli id="T135" time="66.623" type="appl" />
         <tli id="T136" time="67.232" type="appl" />
         <tli id="T137" time="67.841" type="appl" />
         <tli id="T138" time="68.45" type="appl" />
         <tli id="T139" time="68.95" type="appl" />
         <tli id="T140" time="69.449" type="appl" />
         <tli id="T141" time="69.949" type="appl" />
         <tli id="T142" time="70.448" type="appl" />
         <tli id="T143" time="70.948" type="appl" />
         <tli id="T144" time="71.447" type="appl" />
         <tli id="T145" time="71.947" type="appl" />
         <tli id="T146" time="72.446" type="appl" />
         <tli id="T147" time="72.946" type="appl" />
         <tli id="T148" time="73.445" type="appl" />
         <tli id="T149" time="73.945" type="appl" />
         <tli id="T150" time="74.444" type="appl" />
         <tli id="T151" time="74.944" type="appl" />
         <tli id="T152" time="75.443" type="appl" />
         <tli id="T153" time="75.943" type="appl" />
         <tli id="T154" time="76.442" type="appl" />
         <tli id="T155" time="76.942" type="appl" />
         <tli id="T156" time="77.441" type="appl" />
         <tli id="T157" time="77.941" type="appl" />
         <tli id="T158" time="78.44" type="appl" />
         <tli id="T159" time="78.94" type="appl" />
         <tli id="T160" time="79.439" type="appl" />
         <tli id="T161" time="79.939" type="appl" />
         <tli id="T162" time="80.438" type="appl" />
         <tli id="T163" time="81.07133169735411" />
         <tli id="T164" time="81.402" type="appl" />
         <tli id="T165" time="81.866" type="appl" />
         <tli id="T166" time="82.329" type="appl" />
         <tli id="T167" time="82.793" type="appl" />
         <tli id="T168" time="83.257" type="appl" />
         <tli id="T169" time="83.721" type="appl" />
         <tli id="T170" time="84.185" type="appl" />
         <tli id="T171" time="84.648" type="appl" />
         <tli id="T172" time="85.112" type="appl" />
         <tli id="T173" time="85.576" type="appl" />
         <tli id="T174" time="86.04" type="appl" />
         <tli id="T175" time="86.503" type="appl" />
         <tli id="T176" time="86.967" type="appl" />
         <tli id="T177" time="87.431" type="appl" />
         <tli id="T178" time="87.895" type="appl" />
         <tli id="T179" time="88.359" type="appl" />
         <tli id="T180" time="88.822" type="appl" />
         <tli id="T181" time="89.286" type="appl" />
         <tli id="T182" time="89.98999582240373" />
         <tli id="T183" time="90.198" type="appl" />
         <tli id="T184" time="90.646" type="appl" />
         <tli id="T185" time="91.095" type="appl" />
         <tli id="T186" time="91.543" type="appl" />
         <tli id="T187" time="91.991" type="appl" />
         <tli id="T188" time="92.439" type="appl" />
         <tli id="T189" time="92.888" type="appl" />
         <tli id="T190" time="93.336" type="appl" />
         <tli id="T191" time="93.784" type="appl" />
         <tli id="T192" time="94.232" type="appl" />
         <tli id="T193" time="94.68" type="appl" />
         <tli id="T194" time="95.129" type="appl" />
         <tli id="T195" time="95.577" type="appl" />
         <tli id="T196" time="96.025" type="appl" />
         <tli id="T197" time="96.473" type="appl" />
         <tli id="T198" time="96.922" type="appl" />
         <tli id="T199" time="97.37" type="appl" />
         <tli id="T200" time="98.47274458495207" />
         <tli id="T201" time="99.022" type="appl" />
         <tli id="T202" time="99.545" type="appl" />
         <tli id="T203" time="100.068" type="appl" />
         <tli id="T204" time="100.59" type="appl" />
         <tli id="T205" time="101.112" type="appl" />
         <tli id="T206" time="101.635" type="appl" />
         <tli id="T207" time="102.158" type="appl" />
         <tli id="T208" time="102.68" type="appl" />
         <tli id="T209" time="103.202" type="appl" />
         <tli id="T210" time="103.97271170177459" />
         <tli id="T211" time="104.50524773905276" type="intp" />
         <tli id="T212" time="105.03778377633094" type="intp" />
         <tli id="T213" time="105.57031981360912" type="intp" />
         <tli id="T214" time="106.1028558508873" type="intp" />
         <tli id="T215" time="106.63539188816547" type="intp" />
         <tli id="T216" time="107.16792792544365" type="intp" />
         <tli id="T217" time="107.70046396272183" type="intp" />
         <tli id="T218" time="108.233" type="appl" />
         <tli id="T219" time="108.717" type="appl" />
         <tli id="T220" time="109.2" type="appl" />
         <tli id="T221" time="109.683" type="appl" />
         <tli id="T222" time="110.167" type="appl" />
         <tli id="T223" time="110.65" type="appl" />
         <tli id="T224" time="111.133" type="appl" />
         <tli id="T225" time="111.617" type="appl" />
         <tli id="T226" time="112.1" type="appl" />
         <tli id="T227" time="112.583" type="appl" />
         <tli id="T228" time="113.067" type="appl" />
         <tli id="T229" time="113.55" type="appl" />
         <tli id="T230" time="113.97" type="appl" />
         <tli id="T231" time="114.39" type="appl" />
         <tli id="T232" time="114.81" type="appl" />
         <tli id="T233" time="115.23" type="appl" />
         <tli id="T234" time="116.27930478984051" />
         <tli id="T235" time="117.008" type="appl" />
         <tli id="T236" time="117.756" type="appl" />
         <tli id="T237" time="118.505" type="appl" />
         <tli id="T238" time="119.7592839837573" />
         <tli id="T239" time="120.414" type="appl" />
         <tli id="T240" time="120.866" type="appl" />
         <tli id="T241" time="121.318" type="appl" />
         <tli id="T242" time="121.769" type="appl" />
         <tli id="T243" time="122.122" type="appl" />
         <tli id="T244" time="122.476" type="appl" />
         <tli id="T245" time="122.829" type="appl" />
         <tli id="T246" time="123.183" type="appl" />
         <tli id="T247" time="123.536" type="appl" />
         <tli id="T248" time="123.89" type="appl" />
         <tli id="T249" time="124.243" type="appl" />
         <tli id="T250" time="124.597" type="appl" />
         <tli id="T251" time="125.13258519122269" />
         <tli id="T252" time="125.433" type="appl" />
         <tli id="T253" time="125.917" type="appl" />
         <tli id="T254" time="126.4" type="appl" />
         <tli id="T255" time="126.883" type="appl" />
         <tli id="T256" time="127.367" type="appl" />
         <tli id="T257" time="128.87256283066202" />
         <tli id="T258" time="128.88978141533102" type="intp" />
         <tli id="T259" time="128.907" type="appl" />
         <tli id="T260" time="129.436" type="appl" />
         <tli id="T261" time="129.964" type="appl" />
         <tli id="T262" time="130.493" type="appl" />
         <tli id="T263" time="131.021" type="appl" />
         <tli id="T264" time="131.55" type="appl" />
         <tli id="T265" time="131.906" type="appl" />
         <tli id="T266" time="132.261" type="appl" />
         <tli id="T267" time="132.617" type="appl" />
         <tli id="T268" time="132.972" type="appl" />
         <tli id="T269" time="133.328" type="appl" />
         <tli id="T270" time="133.683" type="appl" />
         <tli id="T271" time="134.039" type="appl" />
         <tli id="T272" time="134.394" type="appl" />
         <tli id="T273" time="134.75" type="appl" />
         <tli id="T274" time="135.106" type="appl" />
         <tli id="T275" time="135.461" type="appl" />
         <tli id="T276" time="135.817" type="appl" />
         <tli id="T277" time="136.172" type="appl" />
         <tli id="T278" time="136.528" type="appl" />
         <tli id="T279" time="136.883" type="appl" />
         <tli id="T280" time="137.239" type="appl" />
         <tli id="T281" time="137.594" type="appl" />
         <tli id="T282" time="137.95" type="appl" />
         <tli id="T283" time="138.38263772865704" type="intp" />
         <tli id="T284" time="138.8152754573141" type="intp" />
         <tli id="T285" time="139.24791318597116" type="intp" />
         <tli id="T286" time="139.6805509146282" type="intp" />
         <tli id="T287" time="140.11318864328524" type="intp" />
         <tli id="T288" time="140.5458263719423" />
         <tli id="T289" time="141.07915651660386" />
         <tli id="T290" time="141.58" type="appl" />
         <tli id="T291" time="142.31" type="appl" />
         <tli id="T292" time="143.18000333107082" />
         <tli id="T293" time="144.045" type="appl" />
         <tli id="T294" time="144.976672276655" />
         <tli id="T295" time="145.539" type="appl" />
         <tli id="T296" time="146.028" type="appl" />
         <tli id="T297" time="146.517" type="appl" />
         <tli id="T298" time="147.006" type="appl" />
         <tli id="T299" time="147.494" type="appl" />
         <tli id="T300" time="147.983" type="appl" />
         <tli id="T301" time="148.472" type="appl" />
         <tli id="T302" time="148.961" type="appl" />
         <tli id="T303" time="149.45667153332323" />
         <tli id="T304" time="149.943" type="appl" />
         <tli id="T305" time="150.435" type="appl" />
         <tli id="T306" time="150.928" type="appl" />
         <tli id="T307" time="151.42" type="appl" />
         <tli id="T308" time="151.913" type="appl" />
         <tli id="T309" time="152.501" type="appl" />
         <tli id="T310" time="152.957" type="appl" />
         <tli id="T311" time="153.413" type="appl" />
         <tli id="T312" time="153.869" type="appl" />
         <tli id="T313" time="154.326" type="appl" />
         <tli id="T314" time="154.782" type="appl" />
         <tli id="T315" time="155.238" type="appl" />
         <tli id="T316" time="155.694" type="appl" />
         <tli id="T317" time="156.7323962624212" />
         <tli id="T318" time="157.341" type="appl" />
         <tli id="T319" time="157.954" type="appl" />
         <tli id="T320" time="158.87238346787578" />
         <tli id="T321" time="159.504" type="appl" />
         <tli id="T322" time="160.041" type="appl" />
         <tli id="T323" time="160.577" type="appl" />
         <tli id="T324" time="161.114" type="appl" />
         <tli id="T325" time="161.77903275628134" />
         <tli id="T326" time="162.162" type="appl" />
         <tli id="T327" time="162.675" type="appl" />
         <tli id="T328" time="163.188" type="appl" />
         <tli id="T329" time="163.7" type="appl" />
         <tli id="T330" time="164.212" type="appl" />
         <tli id="T331" time="164.725" type="appl" />
         <tli id="T332" time="165.238" type="appl" />
         <tli id="T333" time="165.75" type="appl" />
         <tli id="T334" time="166.176" type="appl" />
         <tli id="T335" time="166.601" type="appl" />
         <tli id="T336" time="167.027" type="appl" />
         <tli id="T337" time="167.452" type="appl" />
         <tli id="T338" time="167.878" type="appl" />
         <tli id="T339" time="168.303" type="appl" />
         <tli id="T340" time="168.729" type="appl" />
         <tli id="T341" time="169.154" type="appl" />
         <tli id="T342" time="169.58" type="appl" />
         <tli id="T343" time="170.005" type="appl" />
         <tli id="T344" time="170.431" type="appl" />
         <tli id="T345" time="171.044" type="appl" />
         <tli id="T346" time="171.656" type="appl" />
         <tli id="T347" time="172.0600743434346" type="intp" />
         <tli id="T348" time="172.46414868686918" type="intp" />
         <tli id="T349" time="172.86822303030377" type="intp" />
         <tli id="T350" time="173.27229737373835" />
         <tli id="T351" time="173.5122959388361" />
         <tli id="T352" time="173.585" type="appl" />
         <tli id="T353" time="175.35895156472677" />
         <tli id="T354" time="175.968" type="appl" />
         <tli id="T355" time="176.504" type="appl" />
         <tli id="T356" time="177.04" type="appl" />
         <tli id="T357" time="177.577" type="appl" />
         <tli id="T358" time="178.114" type="appl" />
         <tli id="T359" time="179.09226257735781" />
         <tli id="T360" time="179.746" type="appl" />
         <tli id="T361" time="180.254" type="appl" />
         <tli id="T362" time="180.763" type="appl" />
         <tli id="T363" time="181.272" type="appl" />
         <tli id="T364" time="181.78" type="appl" />
         <tli id="T365" time="182.289" type="appl" />
         <tli id="T366" time="182.798" type="appl" />
         <tli id="T367" time="183.307" type="appl" />
         <tli id="T368" time="183.815" type="appl" />
         <tli id="T369" time="184.324" type="appl" />
         <tli id="T370" time="184.833" type="appl" />
         <tli id="T371" time="185.341" type="appl" />
         <tli id="T372" time="186.04332778919522" />
         <tli id="T373" time="186.419" type="appl" />
         <tli id="T374" time="186.988" type="appl" />
         <tli id="T375" time="187.557" type="appl" />
         <tli id="T376" time="188.126" type="appl" />
         <tli id="T377" time="188.694" type="appl" />
         <tli id="T378" time="189.263" type="appl" />
         <tli id="T379" time="189.832" type="appl" />
         <tli id="T380" time="190.401" type="appl" />
         <tli id="T381" time="190.896671167882" />
         <tli id="T382" time="191.5771656112498" type="intp" />
         <tli id="T383" time="192.2576600546176" type="intp" />
         <tli id="T384" time="192.93815449798538" type="intp" />
         <tli id="T385" time="193.61864894135317" type="intp" />
         <tli id="T386" time="194.29914338472096" type="intp" />
         <tli id="T387" time="194.97963782808876" type="intp" />
         <tli id="T388" time="195.66013227145655" type="intp" />
         <tli id="T389" time="196.34062671482434" type="intp" />
         <tli id="T390" time="197.02112115819213" type="intp" />
         <tli id="T391" time="197.70161560155992" type="intp" />
         <tli id="T392" time="198.3821100449277" type="intp" />
         <tli id="T393" time="199.0626044882955" type="intp" />
         <tli id="T394" time="199.74309893166333" type="intp" />
         <tli id="T395" time="200.42359337503112" />
         <tli id="T396" time="200.906" type="appl" />
         <tli id="T397" time="201.448" type="appl" />
         <tli id="T398" time="201.989" type="appl" />
         <tli id="T399" time="202.53" type="appl" />
         <tli id="T400" time="203.072" type="appl" />
         <tli id="T401" time="203.613" type="appl" />
         <tli id="T402" time="204.155" type="appl" />
         <tli id="T403" time="204.696" type="appl" />
         <tli id="T404" time="205.237" type="appl" />
         <tli id="T405" time="205.779" type="appl" />
         <tli id="T406" time="206.25333716601975" />
         <tli id="T407" time="206.739" type="appl" />
         <tli id="T408" time="207.159" type="appl" />
         <tli id="T409" time="207.578" type="appl" />
         <tli id="T410" time="207.998" type="appl" />
         <tli id="T411" time="208.417" type="appl" />
         <tli id="T412" time="208.836" type="appl" />
         <tli id="T413" time="209.256" type="appl" />
         <tli id="T414" time="209.675" type="appl" />
         <tli id="T415" time="210.095" type="appl" />
         <tli id="T416" time="210.514" type="appl" />
         <tli id="T417" time="210.934" type="appl" />
         <tli id="T418" time="211.353" type="appl" />
         <tli id="T419" time="211.772" type="appl" />
         <tli id="T420" time="212.192" type="appl" />
         <tli id="T421" time="212.611" type="appl" />
         <tli id="T422" time="213.031" type="appl" />
         <tli id="T423" time="214.53205069011966" />
         <tli id="T424" time="215.194" type="appl" />
         <tli id="T425" time="215.645" type="appl" />
         <tli id="T426" time="216.095" type="appl" />
         <tli id="T427" time="216.546" type="appl" />
         <tli id="T428" time="216.996" type="appl" />
         <tli id="T429" time="217.447" type="appl" />
         <tli id="T430" time="217.897" type="appl" />
         <tli id="T431" time="218.347" type="appl" />
         <tli id="T432" time="218.798" type="appl" />
         <tli id="T433" time="219.248" type="appl" />
         <tli id="T434" time="219.699" type="appl" />
         <tli id="T435" time="220.149" type="appl" />
         <tli id="T436" time="220.6" type="appl" />
         <tli id="T437" time="221.35867654178787" />
         <tli id="T438" time="221.7503408667737" type="intp" />
         <tli id="T439" time="222.14200519175955" type="intp" />
         <tli id="T440" time="222.5336695167454" type="intp" />
         <tli id="T441" time="222.92533384173123" type="intp" />
         <tli id="T442" time="223.31699816671707" type="intp" />
         <tli id="T443" time="223.7086624917029" type="intp" />
         <tli id="T444" time="224.10032681668878" type="intp" />
         <tli id="T445" time="224.49199114167462" type="intp" />
         <tli id="T446" time="224.88365546666046" type="intp" />
         <tli id="T447" time="225.27531979164633" type="intp" />
         <tli id="T448" time="225.6669841166322" type="intp" />
         <tli id="T449" time="225.97198229311053" />
         <tli id="T450" time="227.93863720155008" />
         <tli id="T451" time="228.2913623654058" type="intp" />
         <tli id="T452" time="228.69075391691942" type="intp" />
         <tli id="T453" time="229.09014546843304" type="intp" />
         <tli id="T454" time="229.48953701994665" type="intp" />
         <tli id="T455" time="229.88892857146027" type="intp" />
         <tli id="T456" time="230.2883201229739" type="intp" />
         <tli id="T457" time="230.6877116744875" type="intp" />
         <tli id="T458" time="231.08710322600112" type="intp" />
         <tli id="T459" time="231.48649477751474" type="intp" />
         <tli id="T460" time="231.88588632902832" type="intp" />
         <tli id="T461" time="232.28527788054194" />
         <tli id="T462" time="232.33445841040646" type="intp" />
         <tli id="T463" time="232.38363894027097" type="intp" />
         <tli id="T464" time="232.43281947013548" type="intp" />
         <tli id="T465" time="232.482" type="appl" />
         <tli id="T466" time="233.616" type="appl" />
         <tli id="T467" time="234.85000212991403" />
         <tli id="T468" time="235.17" type="appl" />
         <tli id="T469" time="235.59" type="appl" />
         <tli id="T470" time="236.01" type="appl" />
         <tli id="T471" time="236.43" type="appl" />
         <tli id="T472" time="236.85" type="appl" />
         <tli id="T473" time="237.27" type="appl" />
         <tli id="T474" time="237.69" type="appl" />
         <tli id="T475" time="238.11" type="appl" />
         <tli id="T476" time="238.53" type="appl" />
         <tli id="T477" time="239.45666729593935" />
         <tli id="T478" time="239.511" type="appl" />
         <tli id="T479" time="240.072" type="appl" />
         <tli id="T480" time="240.633" type="appl" />
         <tli id="T481" time="241.194" type="appl" />
         <tli id="T482" time="241.755" type="appl" />
         <tli id="T483" time="242.316" type="appl" />
         <tli id="T484" time="242.877" type="appl" />
         <tli id="T485" time="243.438" type="appl" />
         <tli id="T486" time="243.999" type="appl" />
         <tli id="T487" time="244.56" type="appl" />
         <tli id="T488" time="245.121" type="appl" />
         <tli id="T489" time="245.682" type="appl" />
         <tli id="T490" time="246.243" type="appl" />
         <tli id="T491" time="247.45852049616383" />
         <tli id="T492" time="247.861" type="appl" />
         <tli id="T493" time="248.281" type="appl" />
         <tli id="T494" time="248.7" type="appl" />
         <tli id="T495" time="249.119" type="appl" />
         <tli id="T496" time="249.538" type="appl" />
         <tli id="T497" time="249.958" type="appl" />
         <tli id="T498" time="250.377" type="appl" />
         <tli id="T499" time="250.796" type="appl" />
         <tli id="T500" time="251.215" type="appl" />
         <tli id="T501" time="251.634" type="appl" />
         <tli id="T502" time="252.054" type="appl" />
         <tli id="T503" time="252.473" type="appl" />
         <tli id="T504" time="252.892" type="appl" />
         <tli id="T505" time="253.312" type="appl" />
         <tli id="T506" time="253.731" type="appl" />
         <tli id="T507" time="254.04999150377472" />
         <tli id="T508" time="254.60357142857143" type="intp" />
         <tli id="T509" time="255.05714285714285" type="intp" />
         <tli id="T510" time="255.51071428571427" type="intp" />
         <tli id="T511" time="255.9642857142857" type="intp" />
         <tli id="T512" time="256.41785714285714" type="intp" />
         <tli id="T513" time="256.87142857142857" type="intp" />
         <tli id="T514" time="257.325" type="appl" />
         <tli id="T515" time="257.81" type="appl" />
         <tli id="T516" time="258.295" type="appl" />
         <tli id="T517" time="258.78" type="appl" />
         <tli id="T518" time="259.265" type="appl" />
         <tli id="T519" time="259.75" type="appl" />
         <tli id="T520" time="260.211" type="appl" />
         <tli id="T521" time="260.672" type="appl" />
         <tli id="T522" time="261.134" type="appl" />
         <tli id="T523" time="261.595" type="appl" />
         <tli id="T524" time="262.056" type="appl" />
         <tli id="T525" time="262.517" type="appl" />
         <tli id="T526" time="262.978" type="appl" />
         <tli id="T527" time="263.439" type="appl" />
         <tli id="T528" time="263.901" type="appl" />
         <tli id="T529" time="264.362" type="appl" />
         <tli id="T530" time="264.823" type="appl" />
         <tli id="T531" time="265.284" type="appl" />
         <tli id="T532" time="265.745" type="appl" />
         <tli id="T533" time="266.206" type="appl" />
         <tli id="T534" time="266.668" type="appl" />
         <tli id="T535" time="267.129" type="appl" />
         <tli id="T536" time="267.4699893933546" />
         <tli id="T537" time="267.935" type="appl" />
         <tli id="T538" time="268.28" type="appl" />
         <tli id="T539" time="268.625" type="appl" />
         <tli id="T540" time="268.97" type="appl" />
         <tli id="T541" time="269.315" type="appl" />
         <tli id="T542" time="269.66" type="appl" />
         <tli id="T543" time="270.005" type="appl" />
         <tli id="T544" time="270.35" type="appl" />
         <tli id="T545" time="270.847" type="appl" />
         <tli id="T546" time="271.344" type="appl" />
         <tli id="T547" time="271.84" type="appl" />
         <tli id="T548" time="272.337" type="appl" />
         <tli id="T549" time="272.834" type="appl" />
         <tli id="T550" time="273.331" type="appl" />
         <tli id="T551" time="273.828" type="appl" />
         <tli id="T552" time="274.325" type="appl" />
         <tli id="T553" time="274.822" type="appl" />
         <tli id="T554" time="275.318" type="appl" />
         <tli id="T555" time="275.815" type="appl" />
         <tli id="T556" time="276.6316794091521" />
         <tli id="T557" time="277.38" type="appl" />
         <tli id="T558" time="277.917" type="appl" />
         <tli id="T559" time="278.454" type="appl" />
         <tli id="T560" time="278.991" type="appl" />
         <tli id="T561" time="279.528" type="appl" />
         <tli id="T562" time="280.065" type="appl" />
         <tli id="T563" time="280.602" type="appl" />
         <tli id="T564" time="281.139" type="appl" />
         <tli id="T565" time="281.676" type="appl" />
         <tli id="T566" time="282.213" type="appl" />
         <tli id="T567" time="282.710002443103" />
         <tli id="T568" time="283.18" type="appl" />
         <tli id="T569" time="283.61" type="appl" />
         <tli id="T570" time="284.04" type="appl" />
         <tli id="T571" time="284.47" type="appl" />
         <tli id="T572" time="284.9" type="appl" />
         <tli id="T573" time="285.33" type="appl" />
         <tli id="T574" time="285.76" type="appl" />
         <tli id="T575" time="286.19" type="appl" />
         <tli id="T576" time="286.62" type="appl" />
         <tli id="T577" time="286.9500031346735" />
         <tli id="T578" time="287.473" type="appl" />
         <tli id="T579" time="287.896" type="appl" />
         <tli id="T580" time="288.319" type="appl" />
         <tli id="T581" time="288.742" type="appl" />
         <tli id="T582" time="289.165" type="appl" />
         <tli id="T583" time="289.588" type="appl" />
         <tli id="T584" time="290.012" type="appl" />
         <tli id="T585" time="290.435" type="appl" />
         <tli id="T586" time="290.858" type="appl" />
         <tli id="T587" time="291.281" type="appl" />
         <tli id="T588" time="291.704" type="appl" />
         <tli id="T589" time="292.127" type="appl" />
         <tli id="T590" time="293.2915798030182" />
         <tli id="T591" time="293.627" type="appl" />
         <tli id="T592" time="293.987" type="appl" />
         <tli id="T593" time="294.346" type="appl" />
         <tli id="T594" time="294.705" type="appl" />
         <tli id="T595" time="295.065" type="appl" />
         <tli id="T596" time="295.8373197908932" />
         <tli id="T597" time="296.433" type="appl" />
         <tli id="T598" time="297.1266610405192" />
         <tli id="T599" time="297.818" type="appl" />
         <tli id="T600" time="298.595" type="appl" />
         <tli id="T601" time="299.372" type="appl" />
         <tli id="T602" time="300.11666920553915" />
         <tli id="T603" time="300.65" type="appl" />
         <tli id="T604" time="301.149" type="appl" />
         <tli id="T605" time="301.649" type="appl" />
         <tli id="T606" time="302.149" type="appl" />
         <tli id="T607" time="302.649" type="appl" />
         <tli id="T608" time="303.148" type="appl" />
         <tli id="T609" time="304.28484740985493" />
         <tli id="T610" time="304.811" type="appl" />
         <tli id="T682" time="305.0855" type="intp" />
         <tli id="T611" time="305.36" type="appl" />
         <tli id="T612" time="305.909" type="appl" />
         <tli id="T613" time="306.458" type="appl" />
         <tli id="T614" time="307.007" type="appl" />
         <tli id="T615" time="307.556" type="appl" />
         <tli id="T616" time="308.105" type="appl" />
         <tli id="T617" time="308.654" type="appl" />
         <tli id="T618" time="309.203" type="appl" />
         <tli id="T619" time="309.752" type="appl" />
         <tli id="T620" time="310.301" type="appl" />
         <tli id="T621" time="311.42480472151186" />
         <tli id="T622" time="312.122" type="appl" />
         <tli id="T623" time="312.775" type="appl" />
         <tli id="T624" time="313.428" type="appl" />
         <tli id="T625" time="314.081" type="appl" />
         <tli id="T626" time="314.734" type="appl" />
         <tli id="T627" time="315.388" type="appl" />
         <tli id="T628" time="316.041" type="appl" />
         <tli id="T629" time="316.694" type="appl" />
         <tli id="T630" time="317.347" type="appl" />
         <tli id="T631" time="318.0" type="appl" />
         <tli id="T632" time="319.34475736973627" />
         <tli id="T633" time="319.766" type="appl" />
         <tli id="T634" time="320.186" type="appl" />
         <tli id="T635" time="320.606" type="appl" />
         <tli id="T636" time="321.027" type="appl" />
         <tli id="T637" time="321.447" type="appl" />
         <tli id="T638" time="321.867" type="appl" />
         <tli id="T639" time="322.287" type="appl" />
         <tli id="T640" time="322.707" type="appl" />
         <tli id="T641" time="323.127" type="appl" />
         <tli id="T642" time="323.548" type="appl" />
         <tli id="T643" time="323.968" type="appl" />
         <tli id="T644" time="324.388" type="appl" />
         <tli id="T645" time="324.808" type="appl" />
         <tli id="T646" time="325.228" type="appl" />
         <tli id="T647" time="325.648" type="appl" />
         <tli id="T648" time="326.069" type="appl" />
         <tli id="T649" time="326.489" type="appl" />
         <tli id="T650" time="326.909" type="appl" />
         <tli id="T651" time="327.329" type="appl" />
         <tli id="T652" time="327.749" type="appl" />
         <tli id="T653" time="328.169" type="appl" />
         <tli id="T654" time="328.59" type="appl" />
         <tli id="T655" time="329.01" type="appl" />
         <tli id="T656" time="329.43" type="appl" />
         <tli id="T657" time="330.4180244982723" />
         <tli id="T658" time="330.924" type="appl" />
         <tli id="T659" time="331.335" type="appl" />
         <tli id="T660" time="331.747" type="appl" />
         <tli id="T661" time="332.158" type="appl" />
         <tli id="T662" time="332.569" type="appl" />
         <tli id="T663" time="332.98" type="appl" />
         <tli id="T664" time="333.391" type="appl" />
         <tli id="T665" time="333.803" type="appl" />
         <tli id="T666" time="334.214" type="appl" />
         <tli id="T667" time="334.625" type="appl" />
         <tli id="T668" time="335.036" type="appl" />
         <tli id="T669" time="335.448" type="appl" />
         <tli id="T670" time="335.859" type="appl" />
         <tli id="T671" time="336.27" type="appl" />
         <tli id="T672" time="336.803875" type="intp" />
         <tli id="T673" time="337.33775" type="intp" />
         <tli id="T674" time="337.871625" type="intp" />
         <tli id="T675" time="338.4055" type="intp" />
         <tli id="T676" time="338.93937500000004" type="intp" />
         <tli id="T677" time="339.47325" type="intp" />
         <tli id="T678" time="340.051300236222" />
         <tli id="T679" time="340.541" type="appl" />
         <tli id="T680" time="340.901" type="appl" />
         <tli id="T681" time="341.262" type="appl" />
         <tli id="T683" time="341.484" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="UkET"
                      type="t">
         <timeline-fork end="T504" start="T503">
            <tli id="T503.tx.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T681" id="Seg_0" n="sc" s="T0">
               <ts e="T9" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Bu</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">urut</ts>
                  <nts id="Seg_8" n="HIAT:ip">,</nts>
                  <nts id="Seg_9" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_11" n="HIAT:w" s="T2">kuččuguj</ts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_14" n="HIAT:w" s="T3">erdeppititten</ts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_17" n="HIAT:w" s="T4">bi͡ekten</ts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_20" n="HIAT:w" s="T5">oloŋkolohon</ts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_23" n="HIAT:w" s="T6">ü͡öskeːbippit</ts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_26" n="HIAT:w" s="T7">du͡o</ts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_29" n="HIAT:w" s="T8">ol</ts>
                  <nts id="Seg_30" n="HIAT:ip">?</nts>
                  <nts id="Seg_31" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T18" id="Seg_33" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_35" n="HIAT:w" s="T9">Bu</ts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_38" n="HIAT:w" s="T10">bihigi</ts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_41" n="HIAT:w" s="T11">küččügüj</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_44" n="HIAT:w" s="T12">erdekpitten</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_47" n="HIAT:w" s="T13">tu͡ok</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_50" n="HIAT:w" s="T14">da</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_53" n="HIAT:w" s="T15">oːnnʼuː</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_56" n="HIAT:w" s="T16">hu͡ok</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_59" n="HIAT:w" s="T17">ete</ts>
                  <nts id="Seg_60" n="HIAT:ip">.</nts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T34" id="Seg_63" n="HIAT:u" s="T18">
                  <ts e="T19" id="Seg_65" n="HIAT:w" s="T18">Ba</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_68" n="HIAT:w" s="T19">ogoloru</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_71" n="HIAT:w" s="T20">ututumaːrɨlar</ts>
                  <nts id="Seg_72" n="HIAT:ip">,</nts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_75" n="HIAT:w" s="T21">kanʼaːmaːrɨlar</ts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_78" n="HIAT:w" s="T22">ba</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_81" n="HIAT:w" s="T23">teːtebitteːk</ts>
                  <nts id="Seg_82" n="HIAT:ip">,</nts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_85" n="HIAT:w" s="T24">teːtebit</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_88" n="HIAT:w" s="T25">teːtetinaːk</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_90" n="HIAT:ip">–</nts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_93" n="HIAT:w" s="T26">ogogo</ts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_96" n="HIAT:w" s="T27">oloŋkoluːr</ts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_99" n="HIAT:w" s="T28">oloŋkoloro</ts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_102" n="HIAT:w" s="T29">ete</ts>
                  <nts id="Seg_103" n="HIAT:ip">,</nts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_106" n="HIAT:w" s="T30">anaːn</ts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_109" n="HIAT:w" s="T31">ogo</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_112" n="HIAT:w" s="T32">oloŋkoto</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_115" n="HIAT:w" s="T33">di͡en</ts>
                  <nts id="Seg_116" n="HIAT:ip">.</nts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T45" id="Seg_119" n="HIAT:u" s="T34">
                  <ts e="T35" id="Seg_121" n="HIAT:w" s="T34">Bɨlɨr</ts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_124" n="HIAT:w" s="T35">hir</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_127" n="HIAT:w" s="T36">taŋara</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_130" n="HIAT:w" s="T37">ü͡ösküːrün</ts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_133" n="HIAT:w" s="T38">hagɨna</ts>
                  <nts id="Seg_134" n="HIAT:ip">,</nts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_137" n="HIAT:w" s="T39">barɨta</ts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_140" n="HIAT:w" s="T40">tɨllaːk</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_143" n="HIAT:w" s="T41">etilere</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_146" n="HIAT:w" s="T42">ühü</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_148" n="HIAT:ip">–</nts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_151" n="HIAT:w" s="T43">bultar</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_154" n="HIAT:w" s="T44">barɨlara</ts>
                  <nts id="Seg_155" n="HIAT:ip">.</nts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T55" id="Seg_158" n="HIAT:u" s="T45">
                  <ts e="T46" id="Seg_160" n="HIAT:w" s="T45">Ol</ts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_163" n="HIAT:w" s="T46">bultara</ts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_166" n="HIAT:w" s="T47">eː</ts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_169" n="HIAT:w" s="T48">östörö</ts>
                  <nts id="Seg_170" n="HIAT:ip">,</nts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_173" n="HIAT:w" s="T49">ol</ts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_176" n="HIAT:w" s="T50">bultar</ts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_179" n="HIAT:w" s="T51">hɨldʼɨbɨt</ts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_182" n="HIAT:w" s="T52">hɨrɨːlarɨn</ts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_184" n="HIAT:ip">–</nts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_187" n="HIAT:w" s="T53">onu</ts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_190" n="HIAT:w" s="T54">kepsiːller</ts>
                  <nts id="Seg_191" n="HIAT:ip">.</nts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T62" id="Seg_194" n="HIAT:u" s="T55">
                  <ts e="T56" id="Seg_196" n="HIAT:w" s="T55">Oččogo</ts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_199" n="HIAT:w" s="T56">bu͡ollagɨna</ts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_202" n="HIAT:w" s="T57">hahɨl</ts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_205" n="HIAT:w" s="T58">olorbut</ts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_208" n="HIAT:w" s="T59">ühü</ts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_211" n="HIAT:w" s="T60">bɨlɨr-bɨlɨr</ts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_214" n="HIAT:w" s="T61">üjege</ts>
                  <nts id="Seg_215" n="HIAT:ip">.</nts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T84" id="Seg_218" n="HIAT:u" s="T62">
                  <ts e="T63" id="Seg_220" n="HIAT:w" s="T62">Bu</ts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_223" n="HIAT:w" s="T63">sahɨl</ts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_226" n="HIAT:w" s="T64">bu͡ollagɨna</ts>
                  <nts id="Seg_227" n="HIAT:ip">,</nts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_230" n="HIAT:w" s="T65">haːs</ts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_233" n="HIAT:w" s="T66">beri͡emete</ts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_236" n="HIAT:w" s="T67">kelen</ts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_239" n="HIAT:w" s="T68">bu͡ollagɨna</ts>
                  <nts id="Seg_240" n="HIAT:ip">,</nts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_243" n="HIAT:w" s="T69">törüt</ts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_246" n="HIAT:w" s="T70">kantan</ts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_249" n="HIAT:w" s="T71">daː</ts>
                  <nts id="Seg_250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_252" n="HIAT:w" s="T72">as</ts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_255" n="HIAT:w" s="T73">ɨlan</ts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_258" n="HIAT:w" s="T74">hiːr</ts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_261" n="HIAT:w" s="T75">hire</ts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_264" n="HIAT:w" s="T76">hu͡ok</ts>
                  <nts id="Seg_265" n="HIAT:ip">,</nts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_268" n="HIAT:w" s="T77">kühüŋŋü</ts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_271" n="HIAT:w" s="T78">haka</ts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_274" n="HIAT:w" s="T79">aha</ts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_277" n="HIAT:w" s="T80">barammɨt</ts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_280" n="HIAT:w" s="T81">bu͡olla</ts>
                  <nts id="Seg_281" n="HIAT:ip">,</nts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_284" n="HIAT:w" s="T82">haka</ts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_287" n="HIAT:w" s="T83">astammɨta</ts>
                  <nts id="Seg_288" n="HIAT:ip">.</nts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T98" id="Seg_291" n="HIAT:u" s="T84">
                  <ts e="T85" id="Seg_293" n="HIAT:w" s="T84">Uː</ts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_296" n="HIAT:w" s="T85">kaːr</ts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_299" n="HIAT:w" s="T86">bu͡olbut</ts>
                  <nts id="Seg_300" n="HIAT:ip">,</nts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_303" n="HIAT:w" s="T87">innʼe</ts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_306" n="HIAT:w" s="T88">gɨnan</ts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_309" n="HIAT:w" s="T89">aha</ts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_312" n="HIAT:w" s="T90">hu͡ok</ts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_315" n="HIAT:w" s="T91">bu͡olan</ts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_318" n="HIAT:w" s="T92">korgujan</ts>
                  <nts id="Seg_319" n="HIAT:ip">,</nts>
                  <nts id="Seg_320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_322" n="HIAT:w" s="T93">hir</ts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_325" n="HIAT:w" s="T94">taŋara</ts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_328" n="HIAT:w" s="T95">kötön</ts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_331" n="HIAT:w" s="T96">ispite</ts>
                  <nts id="Seg_332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_334" n="HIAT:w" s="T97">sahɨl</ts>
                  <nts id="Seg_335" n="HIAT:ip">.</nts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T103" id="Seg_338" n="HIAT:u" s="T98">
                  <ts e="T99" id="Seg_340" n="HIAT:w" s="T98">Bu</ts>
                  <nts id="Seg_341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_343" n="HIAT:w" s="T99">kötön</ts>
                  <nts id="Seg_344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_346" n="HIAT:w" s="T100">ihen</ts>
                  <nts id="Seg_347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_349" n="HIAT:w" s="T101">bu͡ollagɨna</ts>
                  <nts id="Seg_350" n="HIAT:ip">…</nts>
                  <nts id="Seg_351" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T115" id="Seg_353" n="HIAT:u" s="T103">
                  <nts id="Seg_354" n="HIAT:ip">"</nts>
                  <ts e="T104" id="Seg_356" n="HIAT:w" s="T103">Oː</ts>
                  <nts id="Seg_357" n="HIAT:ip">,</nts>
                  <nts id="Seg_358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_360" n="HIAT:w" s="T104">bu</ts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_363" n="HIAT:w" s="T105">kantan</ts>
                  <nts id="Seg_364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_366" n="HIAT:w" s="T106">bulu͡omuj</ts>
                  <nts id="Seg_367" n="HIAT:ip">"</nts>
                  <nts id="Seg_368" n="HIAT:ip">,</nts>
                  <nts id="Seg_369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_371" n="HIAT:w" s="T107">di͡en</ts>
                  <nts id="Seg_372" n="HIAT:ip">,</nts>
                  <nts id="Seg_373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_375" n="HIAT:w" s="T108">mas</ts>
                  <nts id="Seg_376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_378" n="HIAT:w" s="T109">ihiger</ts>
                  <nts id="Seg_379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_381" n="HIAT:w" s="T110">kiːren</ts>
                  <nts id="Seg_382" n="HIAT:ip">,</nts>
                  <nts id="Seg_383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_385" n="HIAT:w" s="T111">mas</ts>
                  <nts id="Seg_386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_388" n="HIAT:w" s="T112">ihiger</ts>
                  <nts id="Seg_389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_391" n="HIAT:w" s="T113">kötön</ts>
                  <nts id="Seg_392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_394" n="HIAT:w" s="T114">iher</ts>
                  <nts id="Seg_395" n="HIAT:ip">.</nts>
                  <nts id="Seg_396" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T126" id="Seg_398" n="HIAT:u" s="T115">
                  <ts e="T116" id="Seg_400" n="HIAT:w" s="T115">Kajaː</ts>
                  <nts id="Seg_401" n="HIAT:ip">,</nts>
                  <nts id="Seg_402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_404" n="HIAT:w" s="T116">körör</ts>
                  <nts id="Seg_405" n="HIAT:ip">,</nts>
                  <nts id="Seg_406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_408" n="HIAT:w" s="T117">kantaŋnɨː-kantaŋnɨː</ts>
                  <nts id="Seg_409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_411" n="HIAT:w" s="T118">köppüte</ts>
                  <nts id="Seg_412" n="HIAT:ip">,</nts>
                  <nts id="Seg_413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_415" n="HIAT:w" s="T119">oruk</ts>
                  <nts id="Seg_416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_418" n="HIAT:w" s="T120">tiːt</ts>
                  <nts id="Seg_419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_421" n="HIAT:w" s="T121">töbötüger</ts>
                  <nts id="Seg_422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_424" n="HIAT:w" s="T122">har</ts>
                  <nts id="Seg_425" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_427" n="HIAT:w" s="T123">töröːn</ts>
                  <nts id="Seg_428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_430" n="HIAT:w" s="T124">baran</ts>
                  <nts id="Seg_431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_433" n="HIAT:w" s="T125">hɨtar</ts>
                  <nts id="Seg_434" n="HIAT:ip">.</nts>
                  <nts id="Seg_435" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T127" id="Seg_437" n="HIAT:u" s="T126">
                  <ts e="T127" id="Seg_439" n="HIAT:w" s="T126">Kajaː</ts>
                  <nts id="Seg_440" n="HIAT:ip">!</nts>
                  <nts id="Seg_441" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T138" id="Seg_443" n="HIAT:u" s="T127">
                  <nts id="Seg_444" n="HIAT:ip">"</nts>
                  <ts e="T128" id="Seg_446" n="HIAT:w" s="T127">Do</ts>
                  <nts id="Seg_447" n="HIAT:ip">,</nts>
                  <nts id="Seg_448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_450" n="HIAT:w" s="T128">onton</ts>
                  <nts id="Seg_451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_453" n="HIAT:w" s="T129">min</ts>
                  <nts id="Seg_454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_456" n="HIAT:w" s="T130">kajdak</ts>
                  <nts id="Seg_457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_459" n="HIAT:w" s="T131">gɨnammɨn</ts>
                  <nts id="Seg_460" n="HIAT:ip">,</nts>
                  <nts id="Seg_461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_463" n="HIAT:w" s="T132">albɨnnanammɨn</ts>
                  <nts id="Seg_464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_466" n="HIAT:w" s="T133">hi͡ebij</ts>
                  <nts id="Seg_467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_469" n="HIAT:w" s="T134">iti</ts>
                  <nts id="Seg_470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_472" n="HIAT:w" s="T135">hartan</ts>
                  <nts id="Seg_473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_475" n="HIAT:w" s="T136">tugu</ts>
                  <nts id="Seg_476" n="HIAT:ip">,</nts>
                  <nts id="Seg_477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_479" n="HIAT:w" s="T137">hɨmɨːtta</ts>
                  <nts id="Seg_480" n="HIAT:ip">?</nts>
                  <nts id="Seg_481" n="HIAT:ip">"</nts>
               </ts>
               <ts e="T163" id="Seg_483" n="HIAT:u" s="T138">
                  <nts id="Seg_484" n="HIAT:ip">,</nts>
                  <nts id="Seg_485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_487" n="HIAT:w" s="T138">di͡en</ts>
                  <nts id="Seg_488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_490" n="HIAT:w" s="T139">bu</ts>
                  <nts id="Seg_491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_493" n="HIAT:w" s="T140">kihi</ts>
                  <nts id="Seg_494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_496" n="HIAT:w" s="T141">harga</ts>
                  <nts id="Seg_497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_498" n="HIAT:ip">(</nts>
                  <ts e="T144" id="Seg_500" n="HIAT:w" s="T142">ba-</ts>
                  <nts id="Seg_501" n="HIAT:ip">)</nts>
                  <nts id="Seg_502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_504" n="HIAT:w" s="T144">hartan</ts>
                  <nts id="Seg_505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_507" n="HIAT:w" s="T145">hɨmɨːt</ts>
                  <nts id="Seg_508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_510" n="HIAT:w" s="T146">hi͡eri</ts>
                  <nts id="Seg_511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_513" n="HIAT:w" s="T147">kötön</ts>
                  <nts id="Seg_514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_516" n="HIAT:w" s="T148">kelen</ts>
                  <nts id="Seg_517" n="HIAT:ip">,</nts>
                  <nts id="Seg_518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_520" n="HIAT:w" s="T149">haːmaj</ts>
                  <nts id="Seg_521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_523" n="HIAT:w" s="T150">kimin</ts>
                  <nts id="Seg_524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_526" n="HIAT:w" s="T151">annɨtɨgar</ts>
                  <nts id="Seg_527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_529" n="HIAT:w" s="T152">kelbit</ts>
                  <nts id="Seg_530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_532" n="HIAT:w" s="T153">tiːtin</ts>
                  <nts id="Seg_533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_535" n="HIAT:w" s="T154">annɨtɨgar</ts>
                  <nts id="Seg_536" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_538" n="HIAT:w" s="T155">kelbit</ts>
                  <nts id="Seg_539" n="HIAT:ip">,</nts>
                  <nts id="Seg_540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_542" n="HIAT:w" s="T156">innʼe</ts>
                  <nts id="Seg_543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_545" n="HIAT:w" s="T157">gɨnan</ts>
                  <nts id="Seg_546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_548" n="HIAT:w" s="T158">baraːn</ts>
                  <nts id="Seg_549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_551" n="HIAT:w" s="T159">kuturugunan</ts>
                  <nts id="Seg_552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_554" n="HIAT:w" s="T160">tiːtin</ts>
                  <nts id="Seg_555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_557" n="HIAT:w" s="T161">kɨrbɨː</ts>
                  <nts id="Seg_558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_560" n="HIAT:w" s="T162">turbuta</ts>
                  <nts id="Seg_561" n="HIAT:ip">.</nts>
                  <nts id="Seg_562" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T182" id="Seg_564" n="HIAT:u" s="T163">
                  <nts id="Seg_565" n="HIAT:ip">"</nts>
                  <ts e="T164" id="Seg_567" n="HIAT:w" s="T163">Sar</ts>
                  <nts id="Seg_568" n="HIAT:ip">,</nts>
                  <nts id="Seg_569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_571" n="HIAT:w" s="T164">sar</ts>
                  <nts id="Seg_572" n="HIAT:ip">,</nts>
                  <nts id="Seg_573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_575" n="HIAT:w" s="T165">teheges</ts>
                  <nts id="Seg_576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_578" n="HIAT:w" s="T166">kihi</ts>
                  <nts id="Seg_579" n="HIAT:ip">,</nts>
                  <nts id="Seg_580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_582" n="HIAT:w" s="T167">egel</ts>
                  <nts id="Seg_583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_585" n="HIAT:w" s="T168">biːr</ts>
                  <nts id="Seg_586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_588" n="HIAT:w" s="T169">ogogun</ts>
                  <nts id="Seg_589" n="HIAT:ip">,</nts>
                  <nts id="Seg_590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_592" n="HIAT:w" s="T170">min</ts>
                  <nts id="Seg_593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_595" n="HIAT:w" s="T171">kuturugum</ts>
                  <nts id="Seg_596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_598" n="HIAT:w" s="T172">nuːčča</ts>
                  <nts id="Seg_599" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_601" n="HIAT:w" s="T173">bolotunaːgar</ts>
                  <nts id="Seg_602" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_604" n="HIAT:w" s="T174">hɨtɨː</ts>
                  <nts id="Seg_605" n="HIAT:ip">,</nts>
                  <nts id="Seg_606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_608" n="HIAT:w" s="T175">tiːttiːn</ts>
                  <nts id="Seg_609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_611" n="HIAT:w" s="T176">bagastɨːn</ts>
                  <nts id="Seg_612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_614" n="HIAT:w" s="T177">bɨha</ts>
                  <nts id="Seg_615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_617" n="HIAT:w" s="T178">okson</ts>
                  <nts id="Seg_618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_620" n="HIAT:w" s="T179">tüheri͡em</ts>
                  <nts id="Seg_621" n="HIAT:ip">,</nts>
                  <nts id="Seg_622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_624" n="HIAT:w" s="T180">bejeliːŋŋin</ts>
                  <nts id="Seg_625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_627" n="HIAT:w" s="T181">si͡em</ts>
                  <nts id="Seg_628" n="HIAT:ip">.</nts>
                  <nts id="Seg_629" n="HIAT:ip">"</nts>
                  <nts id="Seg_630" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T200" id="Seg_632" n="HIAT:u" s="T182">
                  <ts e="T183" id="Seg_634" n="HIAT:w" s="T182">Har</ts>
                  <nts id="Seg_635" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_637" n="HIAT:w" s="T183">bɨstɨ͡a</ts>
                  <nts id="Seg_638" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_640" n="HIAT:w" s="T184">du͡o</ts>
                  <nts id="Seg_641" n="HIAT:ip">,</nts>
                  <nts id="Seg_642" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_644" n="HIAT:w" s="T185">ɨtɨː-ɨtɨː</ts>
                  <nts id="Seg_645" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_647" n="HIAT:w" s="T186">biːr</ts>
                  <nts id="Seg_648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_650" n="HIAT:w" s="T187">ogotun</ts>
                  <nts id="Seg_651" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_653" n="HIAT:w" s="T188">čekeris</ts>
                  <nts id="Seg_654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_656" n="HIAT:w" s="T189">gɨnnarbɨt</ts>
                  <nts id="Seg_657" n="HIAT:ip">,</nts>
                  <nts id="Seg_658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_660" n="HIAT:w" s="T190">biːr</ts>
                  <nts id="Seg_661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_663" n="HIAT:w" s="T191">hɨmɨːtɨn</ts>
                  <nts id="Seg_664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_666" n="HIAT:w" s="T192">hi͡ebit</ts>
                  <nts id="Seg_667" n="HIAT:ip">,</nts>
                  <nts id="Seg_668" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_670" n="HIAT:w" s="T193">innʼe</ts>
                  <nts id="Seg_671" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_673" n="HIAT:w" s="T194">gɨnan</ts>
                  <nts id="Seg_674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_676" n="HIAT:w" s="T195">baraːn</ts>
                  <nts id="Seg_677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_679" n="HIAT:w" s="T196">hir</ts>
                  <nts id="Seg_680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_682" n="HIAT:w" s="T197">taŋara</ts>
                  <nts id="Seg_683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_685" n="HIAT:w" s="T198">kötön</ts>
                  <nts id="Seg_686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_688" n="HIAT:w" s="T199">kaːlbɨt</ts>
                  <nts id="Seg_689" n="HIAT:ip">.</nts>
                  <nts id="Seg_690" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T210" id="Seg_692" n="HIAT:u" s="T200">
                  <ts e="T201" id="Seg_694" n="HIAT:w" s="T200">Harsɨŋŋɨtɨn</ts>
                  <nts id="Seg_695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_697" n="HIAT:w" s="T201">turar</ts>
                  <nts id="Seg_698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_700" n="HIAT:w" s="T202">emi͡e</ts>
                  <nts id="Seg_701" n="HIAT:ip">,</nts>
                  <nts id="Seg_702" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_704" n="HIAT:w" s="T203">harɨ</ts>
                  <nts id="Seg_705" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_706" n="HIAT:ip">(</nts>
                  <ts e="T205" id="Seg_708" n="HIAT:w" s="T204">albɨnnaː-</ts>
                  <nts id="Seg_709" n="HIAT:ip">)</nts>
                  <nts id="Seg_710" n="HIAT:ip">,</nts>
                  <nts id="Seg_711" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_713" n="HIAT:w" s="T205">harɨn</ts>
                  <nts id="Seg_714" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_716" n="HIAT:w" s="T206">albɨnnaːrɨ</ts>
                  <nts id="Seg_717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_719" n="HIAT:w" s="T207">emi͡e</ts>
                  <nts id="Seg_720" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_722" n="HIAT:w" s="T208">kötön</ts>
                  <nts id="Seg_723" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_725" n="HIAT:w" s="T209">keler</ts>
                  <nts id="Seg_726" n="HIAT:ip">:</nts>
                  <nts id="Seg_727" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T217" id="Seg_729" n="HIAT:u" s="T210">
                  <nts id="Seg_730" n="HIAT:ip">"</nts>
                  <ts e="T211" id="Seg_732" n="HIAT:w" s="T210">Sar</ts>
                  <nts id="Seg_733" n="HIAT:ip">,</nts>
                  <nts id="Seg_734" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_736" n="HIAT:w" s="T211">sar</ts>
                  <nts id="Seg_737" n="HIAT:ip">,</nts>
                  <nts id="Seg_738" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_740" n="HIAT:w" s="T212">teheges</ts>
                  <nts id="Seg_741" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_743" n="HIAT:w" s="T213">kihi</ts>
                  <nts id="Seg_744" n="HIAT:ip">,</nts>
                  <nts id="Seg_745" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_747" n="HIAT:w" s="T214">egel</ts>
                  <nts id="Seg_748" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_750" n="HIAT:w" s="T215">biːr</ts>
                  <nts id="Seg_751" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_753" n="HIAT:w" s="T216">ogogun</ts>
                  <nts id="Seg_754" n="HIAT:ip">.</nts>
                  <nts id="Seg_755" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T229" id="Seg_757" n="HIAT:u" s="T217">
                  <ts e="T218" id="Seg_759" n="HIAT:w" s="T217">Min</ts>
                  <nts id="Seg_760" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_762" n="HIAT:w" s="T218">kuturugum</ts>
                  <nts id="Seg_763" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_765" n="HIAT:w" s="T219">nuːčča</ts>
                  <nts id="Seg_766" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_768" n="HIAT:w" s="T220">bolotunaːgar</ts>
                  <nts id="Seg_769" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_771" n="HIAT:w" s="T221">hɨtɨː</ts>
                  <nts id="Seg_772" n="HIAT:ip">,</nts>
                  <nts id="Seg_773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_775" n="HIAT:w" s="T222">tiːttiːŋŋin-bagastɨːŋŋɨn</ts>
                  <nts id="Seg_776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_778" n="HIAT:w" s="T223">bɨha</ts>
                  <nts id="Seg_779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_781" n="HIAT:w" s="T224">okson</ts>
                  <nts id="Seg_782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_784" n="HIAT:w" s="T225">tüheremmin</ts>
                  <nts id="Seg_785" n="HIAT:ip">,</nts>
                  <nts id="Seg_786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_788" n="HIAT:w" s="T226">anɨ</ts>
                  <nts id="Seg_789" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_791" n="HIAT:w" s="T227">bejeliːŋŋin</ts>
                  <nts id="Seg_792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_794" n="HIAT:w" s="T228">si͡em</ts>
                  <nts id="Seg_795" n="HIAT:ip">"</nts>
                  <nts id="Seg_796" n="HIAT:ip">.</nts>
                  <nts id="Seg_797" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T234" id="Seg_799" n="HIAT:u" s="T229">
                  <ts e="T230" id="Seg_801" n="HIAT:w" s="T229">Emi͡e</ts>
                  <nts id="Seg_802" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_804" n="HIAT:w" s="T230">biːr</ts>
                  <nts id="Seg_805" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_807" n="HIAT:w" s="T231">ogotun</ts>
                  <nts id="Seg_808" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_810" n="HIAT:w" s="T232">üːten</ts>
                  <nts id="Seg_811" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_813" n="HIAT:w" s="T233">keːspit</ts>
                  <nts id="Seg_814" n="HIAT:ip">.</nts>
                  <nts id="Seg_815" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T238" id="Seg_817" n="HIAT:u" s="T234">
                  <ts e="T235" id="Seg_819" n="HIAT:w" s="T234">Ikki</ts>
                  <nts id="Seg_820" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_822" n="HIAT:w" s="T235">ogotun</ts>
                  <nts id="Seg_823" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_825" n="HIAT:w" s="T236">hi͡ebit</ts>
                  <nts id="Seg_826" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_827" n="HIAT:ip">–</nts>
                  <nts id="Seg_828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_830" n="HIAT:w" s="T237">hahɨl</ts>
                  <nts id="Seg_831" n="HIAT:ip">.</nts>
                  <nts id="Seg_832" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T242" id="Seg_834" n="HIAT:u" s="T238">
                  <ts e="T239" id="Seg_836" n="HIAT:w" s="T238">Har</ts>
                  <nts id="Seg_837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_839" n="HIAT:w" s="T239">ɨtɨː</ts>
                  <nts id="Seg_840" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_842" n="HIAT:w" s="T240">oloroːktoːbut</ts>
                  <nts id="Seg_843" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_845" n="HIAT:w" s="T241">diːn</ts>
                  <nts id="Seg_846" n="HIAT:ip">.</nts>
                  <nts id="Seg_847" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T251" id="Seg_849" n="HIAT:u" s="T242">
                  <nts id="Seg_850" n="HIAT:ip">"</nts>
                  <ts e="T243" id="Seg_852" n="HIAT:w" s="T242">Doː</ts>
                  <nts id="Seg_853" n="HIAT:ip">,</nts>
                  <nts id="Seg_854" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_856" n="HIAT:w" s="T243">bu</ts>
                  <nts id="Seg_857" n="HIAT:ip">,</nts>
                  <nts id="Seg_858" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_860" n="HIAT:w" s="T244">anɨ</ts>
                  <nts id="Seg_861" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_863" n="HIAT:w" s="T245">bu͡ollagɨna</ts>
                  <nts id="Seg_864" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_866" n="HIAT:w" s="T246">bejeliːmmin</ts>
                  <nts id="Seg_867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_869" n="HIAT:w" s="T247">si͡eni</ts>
                  <nts id="Seg_870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_872" n="HIAT:w" s="T248">di͡en</ts>
                  <nts id="Seg_873" n="HIAT:ip">"</nts>
                  <nts id="Seg_874" n="HIAT:ip">,</nts>
                  <nts id="Seg_875" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_877" n="HIAT:w" s="T249">ɨtɨː</ts>
                  <nts id="Seg_878" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_880" n="HIAT:w" s="T250">olorbut</ts>
                  <nts id="Seg_881" n="HIAT:ip">.</nts>
                  <nts id="Seg_882" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T257" id="Seg_884" n="HIAT:u" s="T251">
                  <ts e="T252" id="Seg_886" n="HIAT:w" s="T251">Kaja</ts>
                  <nts id="Seg_887" n="HIAT:ip">,</nts>
                  <nts id="Seg_888" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_890" n="HIAT:w" s="T252">bu</ts>
                  <nts id="Seg_891" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_893" n="HIAT:w" s="T253">olordoguna</ts>
                  <nts id="Seg_894" n="HIAT:ip">,</nts>
                  <nts id="Seg_895" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_897" n="HIAT:w" s="T254">kukaːkiː</ts>
                  <nts id="Seg_898" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_900" n="HIAT:w" s="T255">kötön</ts>
                  <nts id="Seg_901" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_903" n="HIAT:w" s="T256">kelbit</ts>
                  <nts id="Seg_904" n="HIAT:ip">.</nts>
                  <nts id="Seg_905" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T264" id="Seg_907" n="HIAT:u" s="T257">
                  <nts id="Seg_908" n="HIAT:ip">"</nts>
                  <ts e="T258" id="Seg_910" n="HIAT:w" s="T257">Kajaː</ts>
                  <nts id="Seg_911" n="HIAT:ip">,</nts>
                  <nts id="Seg_912" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_914" n="HIAT:w" s="T258">har</ts>
                  <nts id="Seg_915" n="HIAT:ip">,</nts>
                  <nts id="Seg_916" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_918" n="HIAT:w" s="T259">tu͡oktan</ts>
                  <nts id="Seg_919" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_921" n="HIAT:w" s="T260">ɨtɨːgɨn</ts>
                  <nts id="Seg_922" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_924" n="HIAT:w" s="T261">ki͡e</ts>
                  <nts id="Seg_925" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_927" n="HIAT:w" s="T262">en</ts>
                  <nts id="Seg_928" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_930" n="HIAT:w" s="T263">bu</ts>
                  <nts id="Seg_931" n="HIAT:ip">?</nts>
                  <nts id="Seg_932" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T282" id="Seg_934" n="HIAT:u" s="T264">
                  <ts e="T265" id="Seg_936" n="HIAT:w" s="T264">Munnuk</ts>
                  <nts id="Seg_937" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_939" n="HIAT:w" s="T265">bosku͡oj</ts>
                  <nts id="Seg_940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_942" n="HIAT:w" s="T266">künneːk</ts>
                  <nts id="Seg_943" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_945" n="HIAT:w" s="T267">kün</ts>
                  <nts id="Seg_946" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_948" n="HIAT:w" s="T268">tɨŋa</ts>
                  <nts id="Seg_949" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_951" n="HIAT:w" s="T269">turdagɨna</ts>
                  <nts id="Seg_952" n="HIAT:ip">,</nts>
                  <nts id="Seg_953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_955" n="HIAT:w" s="T270">ogoloruŋ</ts>
                  <nts id="Seg_956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_958" n="HIAT:w" s="T271">da</ts>
                  <nts id="Seg_959" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_961" n="HIAT:w" s="T272">ičigeske</ts>
                  <nts id="Seg_962" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_964" n="HIAT:w" s="T273">hɨtallar</ts>
                  <nts id="Seg_965" n="HIAT:ip">,</nts>
                  <nts id="Seg_966" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_968" n="HIAT:w" s="T274">bejeŋ</ts>
                  <nts id="Seg_969" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_971" n="HIAT:w" s="T275">da</ts>
                  <nts id="Seg_972" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_974" n="HIAT:w" s="T276">ahɨ</ts>
                  <nts id="Seg_975" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_977" n="HIAT:w" s="T277">bagas</ts>
                  <nts id="Seg_978" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_980" n="HIAT:w" s="T278">egelen</ts>
                  <nts id="Seg_981" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_983" n="HIAT:w" s="T279">keler</ts>
                  <nts id="Seg_984" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_986" n="HIAT:w" s="T280">kördük</ts>
                  <nts id="Seg_987" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_989" n="HIAT:w" s="T281">bu͡o</ts>
                  <nts id="Seg_990" n="HIAT:ip">.</nts>
                  <nts id="Seg_991" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T289" id="Seg_993" n="HIAT:u" s="T282">
                  <ts e="T283" id="Seg_995" n="HIAT:w" s="T282">Tu͡oktan</ts>
                  <nts id="Seg_996" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_998" n="HIAT:w" s="T283">ɨtɨːgɨn</ts>
                  <nts id="Seg_999" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_1001" n="HIAT:w" s="T284">dʼe</ts>
                  <nts id="Seg_1002" n="HIAT:ip">,</nts>
                  <nts id="Seg_1003" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_1005" n="HIAT:w" s="T285">munnuk</ts>
                  <nts id="Seg_1006" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_1008" n="HIAT:w" s="T286">beseleːge</ts>
                  <nts id="Seg_1009" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_1011" n="HIAT:w" s="T287">oloroŋŋun</ts>
                  <nts id="Seg_1012" n="HIAT:ip">"</nts>
                  <nts id="Seg_1013" n="HIAT:ip">,</nts>
                  <nts id="Seg_1014" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_1016" n="HIAT:w" s="T288">diːr</ts>
                  <nts id="Seg_1017" n="HIAT:ip">.</nts>
                  <nts id="Seg_1018" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T292" id="Seg_1020" n="HIAT:u" s="T289">
                  <nts id="Seg_1021" n="HIAT:ip">"</nts>
                  <ts e="T290" id="Seg_1023" n="HIAT:w" s="T289">En</ts>
                  <nts id="Seg_1024" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_1026" n="HIAT:w" s="T290">kajdak</ts>
                  <nts id="Seg_1027" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_1029" n="HIAT:w" s="T291">ɨtappakkɨn</ts>
                  <nts id="Seg_1030" n="HIAT:ip">?</nts>
                  <nts id="Seg_1031" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T294" id="Seg_1033" n="HIAT:u" s="T292">
                  <ts e="T293" id="Seg_1035" n="HIAT:w" s="T292">Körbökkün</ts>
                  <nts id="Seg_1036" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_1038" n="HIAT:w" s="T293">du͡o</ts>
                  <nts id="Seg_1039" n="HIAT:ip">?</nts>
                  <nts id="Seg_1040" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T303" id="Seg_1042" n="HIAT:u" s="T294">
                  <ts e="T295" id="Seg_1044" n="HIAT:w" s="T294">Minigin</ts>
                  <nts id="Seg_1045" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1047" n="HIAT:w" s="T295">bu͡ollagɨna</ts>
                  <nts id="Seg_1048" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_1050" n="HIAT:w" s="T296">hahɨl</ts>
                  <nts id="Seg_1051" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_1053" n="HIAT:w" s="T297">bu͡olla</ts>
                  <nts id="Seg_1054" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_1056" n="HIAT:w" s="T298">ikki</ts>
                  <nts id="Seg_1057" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_1059" n="HIAT:w" s="T299">küŋŋe</ts>
                  <nts id="Seg_1060" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_1062" n="HIAT:w" s="T300">ikki</ts>
                  <nts id="Seg_1063" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_1065" n="HIAT:w" s="T301">ogobun</ts>
                  <nts id="Seg_1066" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1068" n="HIAT:w" s="T302">hi͡ete</ts>
                  <nts id="Seg_1069" n="HIAT:ip">.</nts>
                  <nts id="Seg_1070" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T308" id="Seg_1072" n="HIAT:u" s="T303">
                  <ts e="T304" id="Seg_1074" n="HIAT:w" s="T303">Anɨ</ts>
                  <nts id="Seg_1075" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_1077" n="HIAT:w" s="T304">bügün</ts>
                  <nts id="Seg_1078" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_1080" n="HIAT:w" s="T305">emi͡e</ts>
                  <nts id="Seg_1081" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_1083" n="HIAT:w" s="T306">keli͡ege</ts>
                  <nts id="Seg_1084" n="HIAT:ip">,</nts>
                  <nts id="Seg_1085" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_1087" n="HIAT:w" s="T307">hi͡ege</ts>
                  <nts id="Seg_1088" n="HIAT:ip">.</nts>
                  <nts id="Seg_1089" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T317" id="Seg_1091" n="HIAT:u" s="T308">
                  <ts e="T309" id="Seg_1093" n="HIAT:w" s="T308">Tiːttiːmmin</ts>
                  <nts id="Seg_1094" n="HIAT:ip">,</nts>
                  <nts id="Seg_1095" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_1097" n="HIAT:w" s="T309">bagastɨːmmɨn</ts>
                  <nts id="Seg_1098" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_1100" n="HIAT:w" s="T310">bɨha</ts>
                  <nts id="Seg_1101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_1103" n="HIAT:w" s="T311">okson</ts>
                  <nts id="Seg_1104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_1106" n="HIAT:w" s="T312">tüheren</ts>
                  <nts id="Seg_1107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_1109" n="HIAT:w" s="T313">bejeliːmmin</ts>
                  <nts id="Seg_1110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_1112" n="HIAT:w" s="T314">si͡eri</ts>
                  <nts id="Seg_1113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1115" n="HIAT:w" s="T315">gɨnna</ts>
                  <nts id="Seg_1116" n="HIAT:ip">"</nts>
                  <nts id="Seg_1117" n="HIAT:ip">,</nts>
                  <nts id="Seg_1118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_1120" n="HIAT:w" s="T316">diːr</ts>
                  <nts id="Seg_1121" n="HIAT:ip">.</nts>
                  <nts id="Seg_1122" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T320" id="Seg_1124" n="HIAT:u" s="T317">
                  <nts id="Seg_1125" n="HIAT:ip">"</nts>
                  <ts e="T318" id="Seg_1127" n="HIAT:w" s="T317">Tu͡oktaːgɨj</ts>
                  <nts id="Seg_1128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1130" n="HIAT:w" s="T318">ol</ts>
                  <nts id="Seg_1131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_1133" n="HIAT:w" s="T319">sahɨlɨŋ</ts>
                  <nts id="Seg_1134" n="HIAT:ip">?</nts>
                  <nts id="Seg_1135" n="HIAT:ip">"</nts>
                  <nts id="Seg_1136" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T325" id="Seg_1138" n="HIAT:u" s="T320">
                  <nts id="Seg_1139" n="HIAT:ip">"</nts>
                  <ts e="T321" id="Seg_1141" n="HIAT:w" s="T320">Kaja</ts>
                  <nts id="Seg_1142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_1144" n="HIAT:w" s="T321">kuturuga</ts>
                  <nts id="Seg_1145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1147" n="HIAT:w" s="T322">nuːčča</ts>
                  <nts id="Seg_1148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_1150" n="HIAT:w" s="T323">bolotunaːgar</ts>
                  <nts id="Seg_1151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1153" n="HIAT:w" s="T324">hɨtɨː</ts>
                  <nts id="Seg_1154" n="HIAT:ip">"</nts>
                  <nts id="Seg_1155" n="HIAT:ip">.</nts>
                  <nts id="Seg_1156" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T333" id="Seg_1158" n="HIAT:u" s="T325">
                  <nts id="Seg_1159" n="HIAT:ip">"</nts>
                  <ts e="T326" id="Seg_1161" n="HIAT:w" s="T325">Doː</ts>
                  <nts id="Seg_1162" n="HIAT:ip">,</nts>
                  <nts id="Seg_1163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1165" n="HIAT:w" s="T326">tu͡ok</ts>
                  <nts id="Seg_1166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_1168" n="HIAT:w" s="T327">aːttaːga</ts>
                  <nts id="Seg_1169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_1171" n="HIAT:w" s="T328">aŋɨr</ts>
                  <nts id="Seg_1172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_1174" n="HIAT:w" s="T329">kötörö</ts>
                  <nts id="Seg_1175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_1177" n="HIAT:w" s="T330">töröːbütüŋüj</ts>
                  <nts id="Seg_1178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_1180" n="HIAT:w" s="T331">en</ts>
                  <nts id="Seg_1181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_1183" n="HIAT:w" s="T332">munnuk</ts>
                  <nts id="Seg_1184" n="HIAT:ip">?</nts>
                  <nts id="Seg_1185" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T344" id="Seg_1187" n="HIAT:u" s="T333">
                  <ts e="T334" id="Seg_1189" n="HIAT:w" s="T333">Dʼeː</ts>
                  <nts id="Seg_1190" n="HIAT:ip">,</nts>
                  <nts id="Seg_1191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1192" n="HIAT:ip">(</nts>
                  <ts e="T335" id="Seg_1194" n="HIAT:w" s="T334">har</ts>
                  <nts id="Seg_1195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_1197" n="HIAT:w" s="T335">kajdak</ts>
                  <nts id="Seg_1198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_1200" n="HIAT:w" s="T336">gɨna</ts>
                  <nts id="Seg_1201" n="HIAT:ip">)</nts>
                  <nts id="Seg_1202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_1204" n="HIAT:w" s="T337">hahɨl</ts>
                  <nts id="Seg_1205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_1207" n="HIAT:w" s="T338">kajdak</ts>
                  <nts id="Seg_1208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_1210" n="HIAT:w" s="T339">gɨnan</ts>
                  <nts id="Seg_1211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_1213" n="HIAT:w" s="T340">en</ts>
                  <nts id="Seg_1214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_1216" n="HIAT:w" s="T341">tiːtkin</ts>
                  <nts id="Seg_1217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1219" n="HIAT:w" s="T342">bɨha</ts>
                  <nts id="Seg_1220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_1222" n="HIAT:w" s="T343">oksu͡oj</ts>
                  <nts id="Seg_1223" n="HIAT:ip">?</nts>
                  <nts id="Seg_1224" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T346" id="Seg_1226" n="HIAT:u" s="T344">
                  <ts e="T345" id="Seg_1228" n="HIAT:w" s="T344">Kuturuga</ts>
                  <nts id="Seg_1229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_1231" n="HIAT:w" s="T345">tüː</ts>
                  <nts id="Seg_1232" n="HIAT:ip">.</nts>
                  <nts id="Seg_1233" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T353" id="Seg_1235" n="HIAT:u" s="T346">
                  <ts e="T347" id="Seg_1237" n="HIAT:w" s="T346">Tüː</ts>
                  <nts id="Seg_1238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1240" n="HIAT:w" s="T347">kuturuk</ts>
                  <nts id="Seg_1241" n="HIAT:ip">,</nts>
                  <nts id="Seg_1242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_1244" n="HIAT:w" s="T348">tugu</ts>
                  <nts id="Seg_1245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_1247" n="HIAT:w" s="T349">kotu͡oŋuj</ts>
                  <nts id="Seg_1248" n="HIAT:ip">"</nts>
                  <nts id="Seg_1249" n="HIAT:ip">,</nts>
                  <nts id="Seg_1250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_1252" n="HIAT:w" s="T350">di͡e</ts>
                  <nts id="Seg_1253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_1255" n="HIAT:w" s="T351">anɨ</ts>
                  <nts id="Seg_1256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_1258" n="HIAT:w" s="T352">kellegine</ts>
                  <nts id="Seg_1259" n="HIAT:ip">.</nts>
                  <nts id="Seg_1260" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T359" id="Seg_1262" n="HIAT:u" s="T353">
                  <ts e="T354" id="Seg_1264" n="HIAT:w" s="T353">Kim</ts>
                  <nts id="Seg_1265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_1267" n="HIAT:w" s="T354">ü͡öreppitej</ts>
                  <nts id="Seg_1268" n="HIAT:ip">,</nts>
                  <nts id="Seg_1269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_1271" n="HIAT:w" s="T355">di͡etegine</ts>
                  <nts id="Seg_1272" n="HIAT:ip">,</nts>
                  <nts id="Seg_1273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_1275" n="HIAT:w" s="T356">kukaːkiː</ts>
                  <nts id="Seg_1276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_1278" n="HIAT:w" s="T357">ü͡öreppite</ts>
                  <nts id="Seg_1279" n="HIAT:ip">,</nts>
                  <nts id="Seg_1280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_1282" n="HIAT:w" s="T358">diːr</ts>
                  <nts id="Seg_1283" n="HIAT:ip">"</nts>
                  <nts id="Seg_1284" n="HIAT:ip">.</nts>
                  <nts id="Seg_1285" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T372" id="Seg_1287" n="HIAT:u" s="T359">
                  <ts e="T360" id="Seg_1289" n="HIAT:w" s="T359">Dʼe</ts>
                  <nts id="Seg_1290" n="HIAT:ip">,</nts>
                  <nts id="Seg_1291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_1293" n="HIAT:w" s="T360">har</ts>
                  <nts id="Seg_1294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1295" n="HIAT:ip">(</nts>
                  <ts e="T363" id="Seg_1297" n="HIAT:w" s="T361">ha-</ts>
                  <nts id="Seg_1298" n="HIAT:ip">)</nts>
                  <nts id="Seg_1299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_1301" n="HIAT:w" s="T363">har</ts>
                  <nts id="Seg_1302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_1304" n="HIAT:w" s="T364">dʼe</ts>
                  <nts id="Seg_1305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_1307" n="HIAT:w" s="T365">hi͡ese</ts>
                  <nts id="Seg_1308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_1310" n="HIAT:w" s="T366">hek</ts>
                  <nts id="Seg_1311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_1313" n="HIAT:w" s="T367">bu͡olbut</ts>
                  <nts id="Seg_1314" n="HIAT:ip">,</nts>
                  <nts id="Seg_1315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_1317" n="HIAT:w" s="T368">olorbut</ts>
                  <nts id="Seg_1318" n="HIAT:ip">,</nts>
                  <nts id="Seg_1319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_1321" n="HIAT:w" s="T369">kukaːkiːta</ts>
                  <nts id="Seg_1322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_1324" n="HIAT:w" s="T370">kötön</ts>
                  <nts id="Seg_1325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_1327" n="HIAT:w" s="T371">kaːlbɨt</ts>
                  <nts id="Seg_1328" n="HIAT:ip">.</nts>
                  <nts id="Seg_1329" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T381" id="Seg_1331" n="HIAT:u" s="T372">
                  <ts e="T373" id="Seg_1333" n="HIAT:w" s="T372">Oː</ts>
                  <nts id="Seg_1334" n="HIAT:ip">,</nts>
                  <nts id="Seg_1335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_1337" n="HIAT:w" s="T373">dʼe</ts>
                  <nts id="Seg_1338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_1340" n="HIAT:w" s="T374">sahɨl</ts>
                  <nts id="Seg_1341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_1343" n="HIAT:w" s="T375">kɨjŋanaːrɨ</ts>
                  <nts id="Seg_1344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_1346" n="HIAT:w" s="T376">kɨjŋammɨt</ts>
                  <nts id="Seg_1347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378" id="Seg_1349" n="HIAT:w" s="T377">bu͡olbut</ts>
                  <nts id="Seg_1350" n="HIAT:ip">,</nts>
                  <nts id="Seg_1351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_1353" n="HIAT:w" s="T378">kɨːhɨraːrɨ</ts>
                  <nts id="Seg_1354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_1356" n="HIAT:w" s="T379">kɨːhɨrbɨt</ts>
                  <nts id="Seg_1357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_1359" n="HIAT:w" s="T380">bu͡olbut</ts>
                  <nts id="Seg_1360" n="HIAT:ip">:</nts>
                  <nts id="Seg_1361" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T395" id="Seg_1363" n="HIAT:u" s="T381">
                  <ts e="T382" id="Seg_1365" n="HIAT:w" s="T381">Eŋin-eŋin</ts>
                  <nts id="Seg_1366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_1368" n="HIAT:w" s="T382">köppögü</ts>
                  <nts id="Seg_1369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_1371" n="HIAT:w" s="T383">turuta</ts>
                  <nts id="Seg_1372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_1374" n="HIAT:w" s="T384">tebi͡eliː-tebi͡eliː</ts>
                  <nts id="Seg_1375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T386" id="Seg_1377" n="HIAT:w" s="T385">ilin</ts>
                  <nts id="Seg_1378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_1380" n="HIAT:w" s="T386">kalin</ts>
                  <nts id="Seg_1381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_1383" n="HIAT:w" s="T387">atagɨnan</ts>
                  <nts id="Seg_1384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_1386" n="HIAT:w" s="T388">kajɨta</ts>
                  <nts id="Seg_1387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_1389" n="HIAT:w" s="T389">tebi͡eliː-tebi͡eliː</ts>
                  <nts id="Seg_1390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T391" id="Seg_1392" n="HIAT:w" s="T390">keleːkteːbit</ts>
                  <nts id="Seg_1393" n="HIAT:ip">,</nts>
                  <nts id="Seg_1394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_1396" n="HIAT:w" s="T391">diːn</ts>
                  <nts id="Seg_1397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1398" n="HIAT:ip">(</nts>
                  <ts e="T393" id="Seg_1400" n="HIAT:w" s="T392">harɨga</ts>
                  <nts id="Seg_1401" n="HIAT:ip">)</nts>
                  <nts id="Seg_1402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_1404" n="HIAT:w" s="T393">harɨ</ts>
                  <nts id="Seg_1405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_1407" n="HIAT:w" s="T394">kuttaːrɨ</ts>
                  <nts id="Seg_1408" n="HIAT:ip">.</nts>
                  <nts id="Seg_1409" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T406" id="Seg_1411" n="HIAT:u" s="T395">
                  <nts id="Seg_1412" n="HIAT:ip">"</nts>
                  <ts e="T396" id="Seg_1414" n="HIAT:w" s="T395">Sar</ts>
                  <nts id="Seg_1415" n="HIAT:ip">,</nts>
                  <nts id="Seg_1416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_1418" n="HIAT:w" s="T396">sar</ts>
                  <nts id="Seg_1419" n="HIAT:ip">,</nts>
                  <nts id="Seg_1420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_1422" n="HIAT:w" s="T397">onnuk-mannɨk</ts>
                  <nts id="Seg_1423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_1425" n="HIAT:w" s="T398">inʼeleːk</ts>
                  <nts id="Seg_1426" n="HIAT:ip">,</nts>
                  <nts id="Seg_1427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T400" id="Seg_1429" n="HIAT:w" s="T399">kaja</ts>
                  <nts id="Seg_1430" n="HIAT:ip">,</nts>
                  <nts id="Seg_1431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_1433" n="HIAT:w" s="T400">min</ts>
                  <nts id="Seg_1434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T402" id="Seg_1436" n="HIAT:w" s="T401">bu͡ollagɨna</ts>
                  <nts id="Seg_1437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_1439" n="HIAT:w" s="T402">emi͡e</ts>
                  <nts id="Seg_1440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T404" id="Seg_1442" n="HIAT:w" s="T403">bu͡opsa</ts>
                  <nts id="Seg_1443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T405" id="Seg_1445" n="HIAT:w" s="T404">kɨjŋanaːrɨ</ts>
                  <nts id="Seg_1446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_1448" n="HIAT:w" s="T405">kɨjŋannɨm</ts>
                  <nts id="Seg_1449" n="HIAT:ip">.</nts>
                  <nts id="Seg_1450" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T423" id="Seg_1452" n="HIAT:u" s="T406">
                  <ts e="T407" id="Seg_1454" n="HIAT:w" s="T406">Egel</ts>
                  <nts id="Seg_1455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T408" id="Seg_1457" n="HIAT:w" s="T407">biːr</ts>
                  <nts id="Seg_1458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T409" id="Seg_1460" n="HIAT:w" s="T408">ogoto</ts>
                  <nts id="Seg_1461" n="HIAT:ip">,</nts>
                  <nts id="Seg_1462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_1464" n="HIAT:w" s="T409">min</ts>
                  <nts id="Seg_1465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T411" id="Seg_1467" n="HIAT:w" s="T410">kuturugum</ts>
                  <nts id="Seg_1468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T412" id="Seg_1470" n="HIAT:w" s="T411">nʼuːčča</ts>
                  <nts id="Seg_1471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T413" id="Seg_1473" n="HIAT:w" s="T412">bolotunaːgar</ts>
                  <nts id="Seg_1474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414" id="Seg_1476" n="HIAT:w" s="T413">hɨtɨː</ts>
                  <nts id="Seg_1477" n="HIAT:ip">,</nts>
                  <nts id="Seg_1478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T415" id="Seg_1480" n="HIAT:w" s="T414">hanɨ</ts>
                  <nts id="Seg_1481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T416" id="Seg_1483" n="HIAT:w" s="T415">bejeliːŋŋin</ts>
                  <nts id="Seg_1484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1485" n="HIAT:ip">(</nts>
                  <ts e="T418" id="Seg_1487" n="HIAT:w" s="T416">tu-</ts>
                  <nts id="Seg_1488" n="HIAT:ip">)</nts>
                  <nts id="Seg_1489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T419" id="Seg_1491" n="HIAT:w" s="T418">tuːra</ts>
                  <nts id="Seg_1492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T420" id="Seg_1494" n="HIAT:w" s="T419">oksommun</ts>
                  <nts id="Seg_1495" n="HIAT:ip">,</nts>
                  <nts id="Seg_1496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_1498" n="HIAT:w" s="T420">bejeliːŋŋin</ts>
                  <nts id="Seg_1499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T422" id="Seg_1501" n="HIAT:w" s="T421">si͡em</ts>
                  <nts id="Seg_1502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T423" id="Seg_1504" n="HIAT:w" s="T422">onnoːgor</ts>
                  <nts id="Seg_1505" n="HIAT:ip">"</nts>
                  <nts id="Seg_1506" n="HIAT:ip">.</nts>
                  <nts id="Seg_1507" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T437" id="Seg_1509" n="HIAT:u" s="T423">
                  <nts id="Seg_1510" n="HIAT:ip">"</nts>
                  <ts e="T424" id="Seg_1512" n="HIAT:w" s="T423">Koː</ts>
                  <nts id="Seg_1513" n="HIAT:ip">,</nts>
                  <nts id="Seg_1514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T425" id="Seg_1516" n="HIAT:w" s="T424">aŋɨrdaːmmɨn</ts>
                  <nts id="Seg_1517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T426" id="Seg_1519" n="HIAT:w" s="T425">bu͡opsa</ts>
                  <nts id="Seg_1520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T429" id="Seg_1522" n="HIAT:w" s="T426">taːk-ta</ts>
                  <nts id="Seg_1523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_1525" n="HIAT:w" s="T429">kaːrɨjan</ts>
                  <nts id="Seg_1526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T431" id="Seg_1528" n="HIAT:w" s="T430">ikki</ts>
                  <nts id="Seg_1529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_1531" n="HIAT:w" s="T431">ogobun</ts>
                  <nts id="Seg_1532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T433" id="Seg_1534" n="HIAT:w" s="T432">hi͡ettim</ts>
                  <nts id="Seg_1535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T434" id="Seg_1537" n="HIAT:w" s="T433">min</ts>
                  <nts id="Seg_1538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T435" id="Seg_1540" n="HIAT:w" s="T434">eni͡eke</ts>
                  <nts id="Seg_1541" n="HIAT:ip">,</nts>
                  <nts id="Seg_1542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_1544" n="HIAT:w" s="T435">bejem</ts>
                  <nts id="Seg_1545" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T437" id="Seg_1547" n="HIAT:w" s="T436">aŋɨrbar</ts>
                  <nts id="Seg_1548" n="HIAT:ip">.</nts>
                  <nts id="Seg_1549" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T450" id="Seg_1551" n="HIAT:u" s="T437">
                  <ts e="T438" id="Seg_1553" n="HIAT:w" s="T437">En</ts>
                  <nts id="Seg_1554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T439" id="Seg_1556" n="HIAT:w" s="T438">bu͡ollagɨna</ts>
                  <nts id="Seg_1557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440" id="Seg_1559" n="HIAT:w" s="T439">tüː</ts>
                  <nts id="Seg_1560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_1562" n="HIAT:w" s="T440">kuturukkun</ts>
                  <nts id="Seg_1563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T442" id="Seg_1565" n="HIAT:w" s="T441">diːn</ts>
                  <nts id="Seg_1566" n="HIAT:ip">,</nts>
                  <nts id="Seg_1567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T443" id="Seg_1569" n="HIAT:w" s="T442">tüː</ts>
                  <nts id="Seg_1570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T444" id="Seg_1572" n="HIAT:w" s="T443">kuturuk</ts>
                  <nts id="Seg_1573" n="HIAT:ip">,</nts>
                  <nts id="Seg_1574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T445" id="Seg_1576" n="HIAT:w" s="T444">tugu</ts>
                  <nts id="Seg_1577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T446" id="Seg_1579" n="HIAT:w" s="T445">kotu͡oŋuj</ts>
                  <nts id="Seg_1580" n="HIAT:ip">,</nts>
                  <nts id="Seg_1581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T447" id="Seg_1583" n="HIAT:w" s="T446">mahɨ</ts>
                  <nts id="Seg_1584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T448" id="Seg_1586" n="HIAT:w" s="T447">kotu͡oŋ</ts>
                  <nts id="Seg_1587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T449" id="Seg_1589" n="HIAT:w" s="T448">du͡o</ts>
                  <nts id="Seg_1590" n="HIAT:ip">"</nts>
                  <nts id="Seg_1591" n="HIAT:ip">,</nts>
                  <nts id="Seg_1592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T450" id="Seg_1594" n="HIAT:w" s="T449">diːr</ts>
                  <nts id="Seg_1595" n="HIAT:ip">.</nts>
                  <nts id="Seg_1596" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T467" id="Seg_1598" n="HIAT:u" s="T450">
                  <nts id="Seg_1599" n="HIAT:ip">"</nts>
                  <ts e="T451" id="Seg_1601" n="HIAT:w" s="T450">Koː</ts>
                  <nts id="Seg_1602" n="HIAT:ip">,</nts>
                  <nts id="Seg_1603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T452" id="Seg_1605" n="HIAT:w" s="T451">arakata</ts>
                  <nts id="Seg_1606" n="HIAT:ip">,</nts>
                  <nts id="Seg_1607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T453" id="Seg_1609" n="HIAT:w" s="T452">tu͡ok</ts>
                  <nts id="Seg_1610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T454" id="Seg_1612" n="HIAT:w" s="T453">aːttaːga</ts>
                  <nts id="Seg_1613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T455" id="Seg_1615" n="HIAT:w" s="T454">öj</ts>
                  <nts id="Seg_1616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T456" id="Seg_1618" n="HIAT:w" s="T455">bi͡ertej</ts>
                  <nts id="Seg_1619" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T457" id="Seg_1621" n="HIAT:w" s="T456">eni͡eke</ts>
                  <nts id="Seg_1622" n="HIAT:ip">,</nts>
                  <nts id="Seg_1623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T458" id="Seg_1625" n="HIAT:w" s="T457">kajdak</ts>
                  <nts id="Seg_1626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T459" id="Seg_1628" n="HIAT:w" s="T458">gɨnaŋŋɨn</ts>
                  <nts id="Seg_1629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T460" id="Seg_1631" n="HIAT:w" s="T459">öjü</ts>
                  <nts id="Seg_1632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T461" id="Seg_1634" n="HIAT:w" s="T460">bultuŋuj</ts>
                  <nts id="Seg_1635" n="HIAT:ip">"</nts>
                  <nts id="Seg_1636" n="HIAT:ip">,</nts>
                  <nts id="Seg_1637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T462" id="Seg_1639" n="HIAT:w" s="T461">diːr</ts>
                  <nts id="Seg_1640" n="HIAT:ip">,</nts>
                  <nts id="Seg_1641" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1642" n="HIAT:ip">"</nts>
                  <ts e="T463" id="Seg_1644" n="HIAT:w" s="T462">mini͡ene</ts>
                  <nts id="Seg_1645" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T464" id="Seg_1647" n="HIAT:w" s="T463">kuturugum</ts>
                  <nts id="Seg_1648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T465" id="Seg_1650" n="HIAT:w" s="T464">tüːtün</ts>
                  <nts id="Seg_1651" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T466" id="Seg_1653" n="HIAT:w" s="T465">kantan</ts>
                  <nts id="Seg_1654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T467" id="Seg_1656" n="HIAT:w" s="T466">bilegin</ts>
                  <nts id="Seg_1657" n="HIAT:ip">?</nts>
                  <nts id="Seg_1658" n="HIAT:ip">"</nts>
                  <nts id="Seg_1659" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T477" id="Seg_1661" n="HIAT:u" s="T467">
                  <nts id="Seg_1662" n="HIAT:ip">"</nts>
                  <ts e="T468" id="Seg_1664" n="HIAT:w" s="T467">Kaja</ts>
                  <nts id="Seg_1665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T469" id="Seg_1667" n="HIAT:w" s="T468">minigin</ts>
                  <nts id="Seg_1668" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T470" id="Seg_1670" n="HIAT:w" s="T469">kukaːkiː</ts>
                  <nts id="Seg_1671" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T471" id="Seg_1673" n="HIAT:w" s="T470">ü͡öreppite</ts>
                  <nts id="Seg_1674" n="HIAT:ip">,</nts>
                  <nts id="Seg_1675" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T472" id="Seg_1677" n="HIAT:w" s="T471">kim</ts>
                  <nts id="Seg_1678" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T473" id="Seg_1680" n="HIAT:w" s="T472">bi͡eri͡ej</ts>
                  <nts id="Seg_1681" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T474" id="Seg_1683" n="HIAT:w" s="T473">mini͡eke</ts>
                  <nts id="Seg_1684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T475" id="Seg_1686" n="HIAT:w" s="T474">öj</ts>
                  <nts id="Seg_1687" n="HIAT:ip">,</nts>
                  <nts id="Seg_1688" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T476" id="Seg_1690" n="HIAT:w" s="T475">kukaːkiː</ts>
                  <nts id="Seg_1691" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T477" id="Seg_1693" n="HIAT:w" s="T476">ü͡öreppite</ts>
                  <nts id="Seg_1694" n="HIAT:ip">.</nts>
                  <nts id="Seg_1695" n="HIAT:ip">"</nts>
                  <nts id="Seg_1696" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T491" id="Seg_1698" n="HIAT:u" s="T477">
                  <nts id="Seg_1699" n="HIAT:ip">"</nts>
                  <ts e="T478" id="Seg_1701" n="HIAT:w" s="T477">Kajaː</ts>
                  <nts id="Seg_1702" n="HIAT:ip">,</nts>
                  <nts id="Seg_1703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T479" id="Seg_1705" n="HIAT:w" s="T478">onnuk-mannɨk</ts>
                  <nts id="Seg_1706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T480" id="Seg_1708" n="HIAT:w" s="T479">otto</ts>
                  <nts id="Seg_1709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T481" id="Seg_1711" n="HIAT:w" s="T480">kukaːkinɨ</ts>
                  <nts id="Seg_1712" n="HIAT:ip">,</nts>
                  <nts id="Seg_1713" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T482" id="Seg_1715" n="HIAT:w" s="T481">kaja</ts>
                  <nts id="Seg_1716" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T483" id="Seg_1718" n="HIAT:w" s="T482">hirten</ts>
                  <nts id="Seg_1719" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T484" id="Seg_1721" n="HIAT:w" s="T483">bulu͡obuj</ts>
                  <nts id="Seg_1722" n="HIAT:ip">,</nts>
                  <nts id="Seg_1723" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T485" id="Seg_1725" n="HIAT:w" s="T484">dʼe</ts>
                  <nts id="Seg_1726" n="HIAT:ip">,</nts>
                  <nts id="Seg_1727" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T486" id="Seg_1729" n="HIAT:w" s="T485">beːbe</ts>
                  <nts id="Seg_1730" n="HIAT:ip">,</nts>
                  <nts id="Seg_1731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T487" id="Seg_1733" n="HIAT:w" s="T486">dʼe</ts>
                  <nts id="Seg_1734" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T488" id="Seg_1736" n="HIAT:w" s="T487">u͡oldʼahɨ͡am</ts>
                  <nts id="Seg_1737" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T489" id="Seg_1739" n="HIAT:w" s="T488">min</ts>
                  <nts id="Seg_1740" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T490" id="Seg_1742" n="HIAT:w" s="T489">gini͡eke</ts>
                  <nts id="Seg_1743" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T491" id="Seg_1745" n="HIAT:w" s="T490">daːgɨnɨ</ts>
                  <nts id="Seg_1746" n="HIAT:ip">"</nts>
                  <nts id="Seg_1747" n="HIAT:ip">.</nts>
                  <nts id="Seg_1748" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T507" id="Seg_1750" n="HIAT:u" s="T491">
                  <ts e="T492" id="Seg_1752" n="HIAT:w" s="T491">Innʼe</ts>
                  <nts id="Seg_1753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T493" id="Seg_1755" n="HIAT:w" s="T492">gɨmmɨt</ts>
                  <nts id="Seg_1756" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T494" id="Seg_1758" n="HIAT:w" s="T493">da</ts>
                  <nts id="Seg_1759" n="HIAT:ip">,</nts>
                  <nts id="Seg_1760" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T495" id="Seg_1762" n="HIAT:w" s="T494">hir</ts>
                  <nts id="Seg_1763" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T496" id="Seg_1765" n="HIAT:w" s="T495">taŋara</ts>
                  <nts id="Seg_1766" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T497" id="Seg_1768" n="HIAT:w" s="T496">kötön</ts>
                  <nts id="Seg_1769" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T498" id="Seg_1771" n="HIAT:w" s="T497">kaːlbɨt</ts>
                  <nts id="Seg_1772" n="HIAT:ip">,</nts>
                  <nts id="Seg_1773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T499" id="Seg_1775" n="HIAT:w" s="T498">hir</ts>
                  <nts id="Seg_1776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T500" id="Seg_1778" n="HIAT:w" s="T499">taŋara</ts>
                  <nts id="Seg_1779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T501" id="Seg_1781" n="HIAT:w" s="T500">kötön</ts>
                  <nts id="Seg_1782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T502" id="Seg_1784" n="HIAT:w" s="T501">ihen</ts>
                  <nts id="Seg_1785" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T503" id="Seg_1787" n="HIAT:w" s="T502">bu͡olla</ts>
                  <nts id="Seg_1788" n="HIAT:ip">,</nts>
                  <nts id="Seg_1789" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T503.tx.1" id="Seg_1791" n="HIAT:w" s="T503">töp</ts>
                  <nts id="Seg_1792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T504" id="Seg_1794" n="HIAT:w" s="T503.tx.1">tögürük</ts>
                  <nts id="Seg_1795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T505" id="Seg_1797" n="HIAT:w" s="T504">kü͡ölge</ts>
                  <nts id="Seg_1798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T507" id="Seg_1800" n="HIAT:w" s="T505">tijen</ts>
                  <nts id="Seg_1801" n="HIAT:ip">…</nts>
                  <nts id="Seg_1802" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T513" id="Seg_1804" n="HIAT:u" s="T507">
                  <ts e="T508" id="Seg_1806" n="HIAT:w" s="T507">Bu</ts>
                  <nts id="Seg_1807" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T509" id="Seg_1809" n="HIAT:w" s="T508">kü͡öl</ts>
                  <nts id="Seg_1810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T510" id="Seg_1812" n="HIAT:w" s="T509">tu͡oktan</ts>
                  <nts id="Seg_1813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T511" id="Seg_1815" n="HIAT:w" s="T510">daː</ts>
                  <nts id="Seg_1816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T512" id="Seg_1818" n="HIAT:w" s="T511">ulakan</ts>
                  <nts id="Seg_1819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T513" id="Seg_1821" n="HIAT:w" s="T512">kajalaːk</ts>
                  <nts id="Seg_1822" n="HIAT:ip">.</nts>
                  <nts id="Seg_1823" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T519" id="Seg_1825" n="HIAT:u" s="T513">
                  <ts e="T514" id="Seg_1827" n="HIAT:w" s="T513">Haːs</ts>
                  <nts id="Seg_1828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T515" id="Seg_1830" n="HIAT:w" s="T514">kemiger</ts>
                  <nts id="Seg_1831" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T516" id="Seg_1833" n="HIAT:w" s="T515">bu͡olla</ts>
                  <nts id="Seg_1834" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T517" id="Seg_1836" n="HIAT:w" s="T516">keriːskete</ts>
                  <nts id="Seg_1837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T518" id="Seg_1839" n="HIAT:w" s="T517">barɨta</ts>
                  <nts id="Seg_1840" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T519" id="Seg_1842" n="HIAT:w" s="T518">taksɨbɨt</ts>
                  <nts id="Seg_1843" n="HIAT:ip">.</nts>
                  <nts id="Seg_1844" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T536" id="Seg_1846" n="HIAT:u" s="T519">
                  <ts e="T520" id="Seg_1848" n="HIAT:w" s="T519">Bu</ts>
                  <nts id="Seg_1849" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T521" id="Seg_1851" n="HIAT:w" s="T520">keriːskeni</ts>
                  <nts id="Seg_1852" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T522" id="Seg_1854" n="HIAT:w" s="T521">bu͡ollagɨna</ts>
                  <nts id="Seg_1855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T523" id="Seg_1857" n="HIAT:w" s="T522">kötö</ts>
                  <nts id="Seg_1858" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T524" id="Seg_1860" n="HIAT:w" s="T523">hɨldʼan</ts>
                  <nts id="Seg_1861" n="HIAT:ip">,</nts>
                  <nts id="Seg_1862" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T525" id="Seg_1864" n="HIAT:w" s="T524">bu͡opsa</ts>
                  <nts id="Seg_1865" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T526" id="Seg_1867" n="HIAT:w" s="T525">muŋ</ts>
                  <nts id="Seg_1868" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T527" id="Seg_1870" n="HIAT:w" s="T526">bu͡osku͡oj</ts>
                  <nts id="Seg_1871" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T528" id="Seg_1873" n="HIAT:w" s="T527">karadʼɨkka</ts>
                  <nts id="Seg_1874" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T529" id="Seg_1876" n="HIAT:w" s="T528">bu͡ollagɨna</ts>
                  <nts id="Seg_1877" n="HIAT:ip">,</nts>
                  <nts id="Seg_1878" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T530" id="Seg_1880" n="HIAT:w" s="T529">ölbüt</ts>
                  <nts id="Seg_1881" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T531" id="Seg_1883" n="HIAT:w" s="T530">sahɨl</ts>
                  <nts id="Seg_1884" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T532" id="Seg_1886" n="HIAT:w" s="T531">bu͡olan</ts>
                  <nts id="Seg_1887" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T533" id="Seg_1889" n="HIAT:w" s="T532">baran</ts>
                  <nts id="Seg_1890" n="HIAT:ip">,</nts>
                  <nts id="Seg_1891" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T534" id="Seg_1893" n="HIAT:w" s="T533">neles</ts>
                  <nts id="Seg_1894" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T535" id="Seg_1896" n="HIAT:w" s="T534">gɨna</ts>
                  <nts id="Seg_1897" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T536" id="Seg_1899" n="HIAT:w" s="T535">tüspüt</ts>
                  <nts id="Seg_1900" n="HIAT:ip">.</nts>
                  <nts id="Seg_1901" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T544" id="Seg_1903" n="HIAT:u" s="T536">
                  <ts e="T537" id="Seg_1905" n="HIAT:w" s="T536">Bu͡opsa</ts>
                  <nts id="Seg_1906" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T538" id="Seg_1908" n="HIAT:w" s="T537">ü͡öŋŋe</ts>
                  <nts id="Seg_1909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T539" id="Seg_1911" n="HIAT:w" s="T538">barɨtɨgar</ts>
                  <nts id="Seg_1912" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T540" id="Seg_1914" n="HIAT:w" s="T539">ɨstara</ts>
                  <nts id="Seg_1915" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T541" id="Seg_1917" n="HIAT:w" s="T540">hɨppɨt</ts>
                  <nts id="Seg_1918" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T542" id="Seg_1920" n="HIAT:w" s="T541">ol</ts>
                  <nts id="Seg_1921" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T543" id="Seg_1923" n="HIAT:w" s="T542">gɨnan</ts>
                  <nts id="Seg_1924" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T544" id="Seg_1926" n="HIAT:w" s="T543">baraːn</ts>
                  <nts id="Seg_1927" n="HIAT:ip">.</nts>
                  <nts id="Seg_1928" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T556" id="Seg_1930" n="HIAT:u" s="T544">
                  <ts e="T545" id="Seg_1932" n="HIAT:w" s="T544">Kajaː</ts>
                  <nts id="Seg_1933" n="HIAT:ip">,</nts>
                  <nts id="Seg_1934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T546" id="Seg_1936" n="HIAT:w" s="T545">kukaːkiːlar</ts>
                  <nts id="Seg_1937" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T547" id="Seg_1939" n="HIAT:w" s="T546">daːganɨ</ts>
                  <nts id="Seg_1940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T548" id="Seg_1942" n="HIAT:w" s="T547">kötö</ts>
                  <nts id="Seg_1943" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T549" id="Seg_1945" n="HIAT:w" s="T548">hɨldʼan</ts>
                  <nts id="Seg_1946" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T550" id="Seg_1948" n="HIAT:w" s="T549">eren</ts>
                  <nts id="Seg_1949" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T551" id="Seg_1951" n="HIAT:w" s="T550">körör</ts>
                  <nts id="Seg_1952" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1953" n="HIAT:ip">–</nts>
                  <nts id="Seg_1954" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T552" id="Seg_1956" n="HIAT:w" s="T551">sahɨl</ts>
                  <nts id="Seg_1957" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T553" id="Seg_1959" n="HIAT:w" s="T552">ölön</ts>
                  <nts id="Seg_1960" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T554" id="Seg_1962" n="HIAT:w" s="T553">baraːn</ts>
                  <nts id="Seg_1963" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T555" id="Seg_1965" n="HIAT:w" s="T554">nelejen</ts>
                  <nts id="Seg_1966" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T556" id="Seg_1968" n="HIAT:w" s="T555">hɨtar</ts>
                  <nts id="Seg_1969" n="HIAT:ip">.</nts>
                  <nts id="Seg_1970" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T567" id="Seg_1972" n="HIAT:u" s="T556">
                  <nts id="Seg_1973" n="HIAT:ip">"</nts>
                  <ts e="T557" id="Seg_1975" n="HIAT:w" s="T556">Oː</ts>
                  <nts id="Seg_1976" n="HIAT:ip">,</nts>
                  <nts id="Seg_1977" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T558" id="Seg_1979" n="HIAT:w" s="T557">baː</ts>
                  <nts id="Seg_1980" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T559" id="Seg_1982" n="HIAT:w" s="T558">turkarɨ</ts>
                  <nts id="Seg_1983" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T560" id="Seg_1985" n="HIAT:w" s="T559">ba</ts>
                  <nts id="Seg_1986" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T561" id="Seg_1988" n="HIAT:w" s="T560">korgujan</ts>
                  <nts id="Seg_1989" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T562" id="Seg_1991" n="HIAT:w" s="T561">bu͡ollagɨna</ts>
                  <nts id="Seg_1992" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T563" id="Seg_1994" n="HIAT:w" s="T562">har</ts>
                  <nts id="Seg_1995" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T564" id="Seg_1997" n="HIAT:w" s="T563">ogolorun</ts>
                  <nts id="Seg_1998" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T565" id="Seg_2000" n="HIAT:w" s="T564">siː</ts>
                  <nts id="Seg_2001" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T566" id="Seg_2003" n="HIAT:w" s="T565">hɨldʼɨbɨta</ts>
                  <nts id="Seg_2004" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T567" id="Seg_2006" n="HIAT:w" s="T566">bu͡olla</ts>
                  <nts id="Seg_2007" n="HIAT:ip">.</nts>
                  <nts id="Seg_2008" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T577" id="Seg_2010" n="HIAT:u" s="T567">
                  <ts e="T568" id="Seg_2012" n="HIAT:w" s="T567">Anɨ</ts>
                  <nts id="Seg_2013" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T569" id="Seg_2015" n="HIAT:w" s="T568">sara</ts>
                  <nts id="Seg_2016" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T570" id="Seg_2018" n="HIAT:w" s="T569">bu͡ollagɨna</ts>
                  <nts id="Seg_2019" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T571" id="Seg_2021" n="HIAT:w" s="T570">minigitten</ts>
                  <nts id="Seg_2022" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T572" id="Seg_2024" n="HIAT:w" s="T571">öj</ts>
                  <nts id="Seg_2025" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T573" id="Seg_2027" n="HIAT:w" s="T572">ɨlar</ts>
                  <nts id="Seg_2028" n="HIAT:ip">,</nts>
                  <nts id="Seg_2029" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T574" id="Seg_2031" n="HIAT:w" s="T573">biːr</ts>
                  <nts id="Seg_2032" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T575" id="Seg_2034" n="HIAT:w" s="T574">da</ts>
                  <nts id="Seg_2035" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T576" id="Seg_2037" n="HIAT:w" s="T575">ogotun</ts>
                  <nts id="Seg_2038" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T577" id="Seg_2040" n="HIAT:w" s="T576">bi͡erbetek</ts>
                  <nts id="Seg_2041" n="HIAT:ip">.</nts>
                  <nts id="Seg_2042" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T590" id="Seg_2044" n="HIAT:u" s="T577">
                  <ts e="T578" id="Seg_2046" n="HIAT:w" s="T577">Oččogo</ts>
                  <nts id="Seg_2047" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T579" id="Seg_2049" n="HIAT:w" s="T578">kantan</ts>
                  <nts id="Seg_2050" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T580" id="Seg_2052" n="HIAT:w" s="T579">ɨlan</ts>
                  <nts id="Seg_2053" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T581" id="Seg_2055" n="HIAT:w" s="T580">gini</ts>
                  <nts id="Seg_2056" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T582" id="Seg_2058" n="HIAT:w" s="T581">daːganɨ</ts>
                  <nts id="Seg_2059" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T583" id="Seg_2061" n="HIAT:w" s="T582">korgujbut</ts>
                  <nts id="Seg_2062" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T584" id="Seg_2064" n="HIAT:w" s="T583">kihi</ts>
                  <nts id="Seg_2065" n="HIAT:ip">,</nts>
                  <nts id="Seg_2066" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T585" id="Seg_2068" n="HIAT:w" s="T584">kantan</ts>
                  <nts id="Seg_2069" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T586" id="Seg_2071" n="HIAT:w" s="T585">ɨlan</ts>
                  <nts id="Seg_2072" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T587" id="Seg_2074" n="HIAT:w" s="T586">ahɨ͡aj</ts>
                  <nts id="Seg_2075" n="HIAT:ip">,</nts>
                  <nts id="Seg_2076" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T588" id="Seg_2078" n="HIAT:w" s="T587">dʼe</ts>
                  <nts id="Seg_2079" n="HIAT:ip">,</nts>
                  <nts id="Seg_2080" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T589" id="Seg_2082" n="HIAT:w" s="T588">okton</ts>
                  <nts id="Seg_2083" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T590" id="Seg_2085" n="HIAT:w" s="T589">ölbüt</ts>
                  <nts id="Seg_2086" n="HIAT:ip">.</nts>
                  <nts id="Seg_2087" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T596" id="Seg_2089" n="HIAT:u" s="T590">
                  <ts e="T591" id="Seg_2091" n="HIAT:w" s="T590">Dʼe</ts>
                  <nts id="Seg_2092" n="HIAT:ip">,</nts>
                  <nts id="Seg_2093" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T592" id="Seg_2095" n="HIAT:w" s="T591">en</ts>
                  <nts id="Seg_2096" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T593" id="Seg_2098" n="HIAT:w" s="T592">daːganɨ</ts>
                  <nts id="Seg_2099" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T594" id="Seg_2101" n="HIAT:w" s="T593">ölördöːkkün</ts>
                  <nts id="Seg_2102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T595" id="Seg_2104" n="HIAT:w" s="T594">ebit</ts>
                  <nts id="Seg_2105" n="HIAT:ip">"</nts>
                  <nts id="Seg_2106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T596" id="Seg_2108" n="HIAT:w" s="T595">di͡en</ts>
                  <nts id="Seg_2109" n="HIAT:ip">.</nts>
                  <nts id="Seg_2110" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T598" id="Seg_2112" n="HIAT:u" s="T596">
                  <ts e="T597" id="Seg_2114" n="HIAT:w" s="T596">Kaja</ts>
                  <nts id="Seg_2115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T598" id="Seg_2117" n="HIAT:w" s="T597">kelbit</ts>
                  <nts id="Seg_2118" n="HIAT:ip">.</nts>
                  <nts id="Seg_2119" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T602" id="Seg_2121" n="HIAT:u" s="T598">
                  <ts e="T599" id="Seg_2123" n="HIAT:w" s="T598">Loŋ-loŋ</ts>
                  <nts id="Seg_2124" n="HIAT:ip">,</nts>
                  <nts id="Seg_2125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T600" id="Seg_2127" n="HIAT:w" s="T599">loŋ-loŋ</ts>
                  <nts id="Seg_2128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2129" n="HIAT:ip">–</nts>
                  <nts id="Seg_2130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T601" id="Seg_2132" n="HIAT:w" s="T600">loŋsujbut</ts>
                  <nts id="Seg_2133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T602" id="Seg_2135" n="HIAT:w" s="T601">bu͡o</ts>
                  <nts id="Seg_2136" n="HIAT:ip">.</nts>
                  <nts id="Seg_2137" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T609" id="Seg_2139" n="HIAT:u" s="T602">
                  <ts e="T603" id="Seg_2141" n="HIAT:w" s="T602">Kantɨttan</ts>
                  <nts id="Seg_2142" n="HIAT:ip">,</nts>
                  <nts id="Seg_2143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T604" id="Seg_2145" n="HIAT:w" s="T603">karagɨttan</ts>
                  <nts id="Seg_2146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T605" id="Seg_2148" n="HIAT:w" s="T604">hi͡ekke</ts>
                  <nts id="Seg_2149" n="HIAT:ip">,</nts>
                  <nts id="Seg_2150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T606" id="Seg_2152" n="HIAT:w" s="T605">di͡en</ts>
                  <nts id="Seg_2153" n="HIAT:ip">,</nts>
                  <nts id="Seg_2154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T607" id="Seg_2156" n="HIAT:w" s="T606">karɨga</ts>
                  <nts id="Seg_2157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T608" id="Seg_2159" n="HIAT:w" s="T607">loŋsuna</ts>
                  <nts id="Seg_2160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T609" id="Seg_2162" n="HIAT:w" s="T608">hɨrɨttagɨna</ts>
                  <nts id="Seg_2163" n="HIAT:ip">.</nts>
                  <nts id="Seg_2164" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T621" id="Seg_2166" n="HIAT:u" s="T609">
                  <ts e="T610" id="Seg_2168" n="HIAT:w" s="T609">Bu͡opsa</ts>
                  <nts id="Seg_2169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2170" n="HIAT:ip">(</nts>
                  <nts id="Seg_2171" n="HIAT:ip">(</nts>
                  <ats e="T682" id="Seg_2172" n="HIAT:non-pho" s="T610">…</ats>
                  <nts id="Seg_2173" n="HIAT:ip">)</nts>
                  <nts id="Seg_2174" n="HIAT:ip">)</nts>
                  <nts id="Seg_2175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T611" id="Seg_2177" n="HIAT:w" s="T682">sahɨl</ts>
                  <nts id="Seg_2178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T612" id="Seg_2180" n="HIAT:w" s="T611">bu͡ollagɨna</ts>
                  <nts id="Seg_2181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T613" id="Seg_2183" n="HIAT:w" s="T612">ele</ts>
                  <nts id="Seg_2184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T614" id="Seg_2186" n="HIAT:w" s="T613">nʼɨmatɨnan</ts>
                  <nts id="Seg_2187" n="HIAT:ip">,</nts>
                  <nts id="Seg_2188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T615" id="Seg_2190" n="HIAT:w" s="T614">ele</ts>
                  <nts id="Seg_2191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T616" id="Seg_2193" n="HIAT:w" s="T615">kütürünen</ts>
                  <nts id="Seg_2194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T617" id="Seg_2196" n="HIAT:w" s="T616">ɨrdʼɨgɨnɨː</ts>
                  <nts id="Seg_2197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T618" id="Seg_2199" n="HIAT:w" s="T617">gɨna</ts>
                  <nts id="Seg_2200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T619" id="Seg_2202" n="HIAT:w" s="T618">ɨstanaːktaːbɨt</ts>
                  <nts id="Seg_2203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T621" id="Seg_2205" n="HIAT:w" s="T619">diːn</ts>
                  <nts id="Seg_2206" n="HIAT:ip">.</nts>
                  <nts id="Seg_2207" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T632" id="Seg_2209" n="HIAT:u" s="T621">
                  <ts e="T622" id="Seg_2211" n="HIAT:w" s="T621">Baːgɨ</ts>
                  <nts id="Seg_2212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T623" id="Seg_2214" n="HIAT:w" s="T622">sar</ts>
                  <nts id="Seg_2215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T624" id="Seg_2217" n="HIAT:w" s="T623">bu͡ollagɨna</ts>
                  <nts id="Seg_2218" n="HIAT:ip">,</nts>
                  <nts id="Seg_2219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T625" id="Seg_2221" n="HIAT:w" s="T624">kim</ts>
                  <nts id="Seg_2222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T626" id="Seg_2224" n="HIAT:w" s="T625">bu͡ollagɨna</ts>
                  <nts id="Seg_2225" n="HIAT:ip">,</nts>
                  <nts id="Seg_2226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T627" id="Seg_2228" n="HIAT:w" s="T626">kukaːkiː</ts>
                  <nts id="Seg_2229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T628" id="Seg_2231" n="HIAT:w" s="T627">bu͡ollagɨna</ts>
                  <nts id="Seg_2232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T629" id="Seg_2234" n="HIAT:w" s="T628">kü͡örejenen</ts>
                  <nts id="Seg_2235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T630" id="Seg_2237" n="HIAT:w" s="T629">üspü͡öjdeːbekke</ts>
                  <nts id="Seg_2238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T631" id="Seg_2240" n="HIAT:w" s="T630">kuturuguttan</ts>
                  <nts id="Seg_2241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T632" id="Seg_2243" n="HIAT:w" s="T631">kaptarbɨt</ts>
                  <nts id="Seg_2244" n="HIAT:ip">.</nts>
                  <nts id="Seg_2245" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T657" id="Seg_2247" n="HIAT:u" s="T632">
                  <ts e="T633" id="Seg_2249" n="HIAT:w" s="T632">Innʼe</ts>
                  <nts id="Seg_2250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T634" id="Seg_2252" n="HIAT:w" s="T633">gɨnan</ts>
                  <nts id="Seg_2253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T635" id="Seg_2255" n="HIAT:w" s="T634">munu</ts>
                  <nts id="Seg_2256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T636" id="Seg_2258" n="HIAT:w" s="T635">haŋastalahan</ts>
                  <nts id="Seg_2259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T637" id="Seg_2261" n="HIAT:w" s="T636">haŋastahan</ts>
                  <nts id="Seg_2262" n="HIAT:ip">,</nts>
                  <nts id="Seg_2263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T638" id="Seg_2265" n="HIAT:w" s="T637">uː</ts>
                  <nts id="Seg_2266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T639" id="Seg_2268" n="HIAT:w" s="T638">ihiger</ts>
                  <nts id="Seg_2269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T640" id="Seg_2271" n="HIAT:w" s="T639">čekeniten</ts>
                  <nts id="Seg_2272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T641" id="Seg_2274" n="HIAT:w" s="T640">tüheren</ts>
                  <nts id="Seg_2275" n="HIAT:ip">,</nts>
                  <nts id="Seg_2276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2277" n="HIAT:ip">(</nts>
                  <ts e="T642" id="Seg_2279" n="HIAT:w" s="T641">uː</ts>
                  <nts id="Seg_2280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T644" id="Seg_2282" n="HIAT:w" s="T642">hit-</ts>
                  <nts id="Seg_2283" n="HIAT:ip">)</nts>
                  <nts id="Seg_2284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T645" id="Seg_2286" n="HIAT:w" s="T644">uː</ts>
                  <nts id="Seg_2287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T646" id="Seg_2289" n="HIAT:w" s="T645">ihiger</ts>
                  <nts id="Seg_2290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T647" id="Seg_2292" n="HIAT:w" s="T646">tüheren</ts>
                  <nts id="Seg_2293" n="HIAT:ip">,</nts>
                  <nts id="Seg_2294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T648" id="Seg_2296" n="HIAT:w" s="T647">munuga</ts>
                  <nts id="Seg_2297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T649" id="Seg_2299" n="HIAT:w" s="T648">bulkujan</ts>
                  <nts id="Seg_2300" n="HIAT:ip">–</nts>
                  <nts id="Seg_2301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T650" id="Seg_2303" n="HIAT:w" s="T649">hiŋeleːk</ts>
                  <nts id="Seg_2304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T651" id="Seg_2306" n="HIAT:w" s="T650">uːga</ts>
                  <nts id="Seg_2307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T652" id="Seg_2309" n="HIAT:w" s="T651">bulkujan</ts>
                  <nts id="Seg_2310" n="HIAT:ip">,</nts>
                  <nts id="Seg_2311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T653" id="Seg_2313" n="HIAT:w" s="T652">tumnaran</ts>
                  <nts id="Seg_2314" n="HIAT:ip">,</nts>
                  <nts id="Seg_2315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T654" id="Seg_2317" n="HIAT:w" s="T653">mülčü</ts>
                  <nts id="Seg_2318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T655" id="Seg_2320" n="HIAT:w" s="T654">tuttaran</ts>
                  <nts id="Seg_2321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T656" id="Seg_2323" n="HIAT:w" s="T655">kötön</ts>
                  <nts id="Seg_2324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T657" id="Seg_2326" n="HIAT:w" s="T656">kaːlbɨt</ts>
                  <nts id="Seg_2327" n="HIAT:ip">.</nts>
                  <nts id="Seg_2328" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T671" id="Seg_2330" n="HIAT:u" s="T657">
                  <ts e="T658" id="Seg_2332" n="HIAT:w" s="T657">Dʼe</ts>
                  <nts id="Seg_2333" n="HIAT:ip">,</nts>
                  <nts id="Seg_2334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T659" id="Seg_2336" n="HIAT:w" s="T658">ol</ts>
                  <nts id="Seg_2337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T660" id="Seg_2339" n="HIAT:w" s="T659">sahɨl</ts>
                  <nts id="Seg_2340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T661" id="Seg_2342" n="HIAT:w" s="T660">bu͡ollagɨna</ts>
                  <nts id="Seg_2343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T662" id="Seg_2345" n="HIAT:w" s="T661">kɨtɨlga</ts>
                  <nts id="Seg_2346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T663" id="Seg_2348" n="HIAT:w" s="T662">taksan</ts>
                  <nts id="Seg_2349" n="HIAT:ip">,</nts>
                  <nts id="Seg_2350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T664" id="Seg_2352" n="HIAT:w" s="T663">kuːrduna</ts>
                  <nts id="Seg_2353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T665" id="Seg_2355" n="HIAT:w" s="T664">ɨrata</ts>
                  <nts id="Seg_2356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T666" id="Seg_2358" n="HIAT:w" s="T665">eni</ts>
                  <nts id="Seg_2359" n="HIAT:ip">,</nts>
                  <nts id="Seg_2360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T667" id="Seg_2362" n="HIAT:w" s="T666">dʼe</ts>
                  <nts id="Seg_2363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T668" id="Seg_2365" n="HIAT:w" s="T667">ol</ts>
                  <nts id="Seg_2366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T669" id="Seg_2368" n="HIAT:w" s="T668">kuːrdunan-kanʼaːn</ts>
                  <nts id="Seg_2369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T671" id="Seg_2371" n="HIAT:w" s="T669">baraːn</ts>
                  <nts id="Seg_2372" n="HIAT:ip">…</nts>
                  <nts id="Seg_2373" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T678" id="Seg_2375" n="HIAT:u" s="T671">
                  <ts e="T672" id="Seg_2377" n="HIAT:w" s="T671">Tillen</ts>
                  <nts id="Seg_2378" n="HIAT:ip">,</nts>
                  <nts id="Seg_2379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T673" id="Seg_2381" n="HIAT:w" s="T672">bajan-toton</ts>
                  <nts id="Seg_2382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T674" id="Seg_2384" n="HIAT:w" s="T673">olorbuta</ts>
                  <nts id="Seg_2385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T675" id="Seg_2387" n="HIAT:w" s="T674">duː</ts>
                  <nts id="Seg_2388" n="HIAT:ip">,</nts>
                  <nts id="Seg_2389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T676" id="Seg_2391" n="HIAT:w" s="T675">kajdi͡ek</ts>
                  <nts id="Seg_2392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T677" id="Seg_2394" n="HIAT:w" s="T676">bu͡olbuta</ts>
                  <nts id="Seg_2395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T678" id="Seg_2397" n="HIAT:w" s="T677">duː</ts>
                  <nts id="Seg_2398" n="HIAT:ip">?</nts>
                  <nts id="Seg_2399" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T681" id="Seg_2401" n="HIAT:u" s="T678">
                  <ts e="T679" id="Seg_2403" n="HIAT:w" s="T678">Dʼe</ts>
                  <nts id="Seg_2404" n="HIAT:ip">,</nts>
                  <nts id="Seg_2405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T680" id="Seg_2407" n="HIAT:w" s="T679">oloŋkobut</ts>
                  <nts id="Seg_2408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T681" id="Seg_2410" n="HIAT:w" s="T680">elete</ts>
                  <nts id="Seg_2411" n="HIAT:ip">.</nts>
                  <nts id="Seg_2412" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T681" id="Seg_2413" n="sc" s="T0">
               <ts e="T1" id="Seg_2415" n="e" s="T0">Bu </ts>
               <ts e="T2" id="Seg_2417" n="e" s="T1">urut, </ts>
               <ts e="T3" id="Seg_2419" n="e" s="T2">kuččuguj </ts>
               <ts e="T4" id="Seg_2421" n="e" s="T3">erdeppititten </ts>
               <ts e="T5" id="Seg_2423" n="e" s="T4">bi͡ekten </ts>
               <ts e="T6" id="Seg_2425" n="e" s="T5">oloŋkolohon </ts>
               <ts e="T7" id="Seg_2427" n="e" s="T6">ü͡öskeːbippit </ts>
               <ts e="T8" id="Seg_2429" n="e" s="T7">du͡o </ts>
               <ts e="T9" id="Seg_2431" n="e" s="T8">ol? </ts>
               <ts e="T10" id="Seg_2433" n="e" s="T9">Bu </ts>
               <ts e="T11" id="Seg_2435" n="e" s="T10">bihigi </ts>
               <ts e="T12" id="Seg_2437" n="e" s="T11">küččügüj </ts>
               <ts e="T13" id="Seg_2439" n="e" s="T12">erdekpitten </ts>
               <ts e="T14" id="Seg_2441" n="e" s="T13">tu͡ok </ts>
               <ts e="T15" id="Seg_2443" n="e" s="T14">da </ts>
               <ts e="T16" id="Seg_2445" n="e" s="T15">oːnnʼuː </ts>
               <ts e="T17" id="Seg_2447" n="e" s="T16">hu͡ok </ts>
               <ts e="T18" id="Seg_2449" n="e" s="T17">ete. </ts>
               <ts e="T19" id="Seg_2451" n="e" s="T18">Ba </ts>
               <ts e="T20" id="Seg_2453" n="e" s="T19">ogoloru </ts>
               <ts e="T21" id="Seg_2455" n="e" s="T20">ututumaːrɨlar, </ts>
               <ts e="T22" id="Seg_2457" n="e" s="T21">kanʼaːmaːrɨlar </ts>
               <ts e="T23" id="Seg_2459" n="e" s="T22">ba </ts>
               <ts e="T24" id="Seg_2461" n="e" s="T23">teːtebitteːk, </ts>
               <ts e="T25" id="Seg_2463" n="e" s="T24">teːtebit </ts>
               <ts e="T26" id="Seg_2465" n="e" s="T25">teːtetinaːk – </ts>
               <ts e="T27" id="Seg_2467" n="e" s="T26">ogogo </ts>
               <ts e="T28" id="Seg_2469" n="e" s="T27">oloŋkoluːr </ts>
               <ts e="T29" id="Seg_2471" n="e" s="T28">oloŋkoloro </ts>
               <ts e="T30" id="Seg_2473" n="e" s="T29">ete, </ts>
               <ts e="T31" id="Seg_2475" n="e" s="T30">anaːn </ts>
               <ts e="T32" id="Seg_2477" n="e" s="T31">ogo </ts>
               <ts e="T33" id="Seg_2479" n="e" s="T32">oloŋkoto </ts>
               <ts e="T34" id="Seg_2481" n="e" s="T33">di͡en. </ts>
               <ts e="T35" id="Seg_2483" n="e" s="T34">Bɨlɨr </ts>
               <ts e="T36" id="Seg_2485" n="e" s="T35">hir </ts>
               <ts e="T37" id="Seg_2487" n="e" s="T36">taŋara </ts>
               <ts e="T38" id="Seg_2489" n="e" s="T37">ü͡ösküːrün </ts>
               <ts e="T39" id="Seg_2491" n="e" s="T38">hagɨna, </ts>
               <ts e="T40" id="Seg_2493" n="e" s="T39">barɨta </ts>
               <ts e="T41" id="Seg_2495" n="e" s="T40">tɨllaːk </ts>
               <ts e="T42" id="Seg_2497" n="e" s="T41">etilere </ts>
               <ts e="T43" id="Seg_2499" n="e" s="T42">ühü – </ts>
               <ts e="T44" id="Seg_2501" n="e" s="T43">bultar </ts>
               <ts e="T45" id="Seg_2503" n="e" s="T44">barɨlara. </ts>
               <ts e="T46" id="Seg_2505" n="e" s="T45">Ol </ts>
               <ts e="T47" id="Seg_2507" n="e" s="T46">bultara </ts>
               <ts e="T48" id="Seg_2509" n="e" s="T47">eː </ts>
               <ts e="T49" id="Seg_2511" n="e" s="T48">östörö, </ts>
               <ts e="T50" id="Seg_2513" n="e" s="T49">ol </ts>
               <ts e="T51" id="Seg_2515" n="e" s="T50">bultar </ts>
               <ts e="T52" id="Seg_2517" n="e" s="T51">hɨldʼɨbɨt </ts>
               <ts e="T53" id="Seg_2519" n="e" s="T52">hɨrɨːlarɨn – </ts>
               <ts e="T54" id="Seg_2521" n="e" s="T53">onu </ts>
               <ts e="T55" id="Seg_2523" n="e" s="T54">kepsiːller. </ts>
               <ts e="T56" id="Seg_2525" n="e" s="T55">Oččogo </ts>
               <ts e="T57" id="Seg_2527" n="e" s="T56">bu͡ollagɨna </ts>
               <ts e="T58" id="Seg_2529" n="e" s="T57">hahɨl </ts>
               <ts e="T59" id="Seg_2531" n="e" s="T58">olorbut </ts>
               <ts e="T60" id="Seg_2533" n="e" s="T59">ühü </ts>
               <ts e="T61" id="Seg_2535" n="e" s="T60">bɨlɨr-bɨlɨr </ts>
               <ts e="T62" id="Seg_2537" n="e" s="T61">üjege. </ts>
               <ts e="T63" id="Seg_2539" n="e" s="T62">Bu </ts>
               <ts e="T64" id="Seg_2541" n="e" s="T63">sahɨl </ts>
               <ts e="T65" id="Seg_2543" n="e" s="T64">bu͡ollagɨna, </ts>
               <ts e="T66" id="Seg_2545" n="e" s="T65">haːs </ts>
               <ts e="T67" id="Seg_2547" n="e" s="T66">beri͡emete </ts>
               <ts e="T68" id="Seg_2549" n="e" s="T67">kelen </ts>
               <ts e="T69" id="Seg_2551" n="e" s="T68">bu͡ollagɨna, </ts>
               <ts e="T70" id="Seg_2553" n="e" s="T69">törüt </ts>
               <ts e="T71" id="Seg_2555" n="e" s="T70">kantan </ts>
               <ts e="T72" id="Seg_2557" n="e" s="T71">daː </ts>
               <ts e="T73" id="Seg_2559" n="e" s="T72">as </ts>
               <ts e="T74" id="Seg_2561" n="e" s="T73">ɨlan </ts>
               <ts e="T75" id="Seg_2563" n="e" s="T74">hiːr </ts>
               <ts e="T76" id="Seg_2565" n="e" s="T75">hire </ts>
               <ts e="T77" id="Seg_2567" n="e" s="T76">hu͡ok, </ts>
               <ts e="T78" id="Seg_2569" n="e" s="T77">kühüŋŋü </ts>
               <ts e="T79" id="Seg_2571" n="e" s="T78">haka </ts>
               <ts e="T80" id="Seg_2573" n="e" s="T79">aha </ts>
               <ts e="T81" id="Seg_2575" n="e" s="T80">barammɨt </ts>
               <ts e="T82" id="Seg_2577" n="e" s="T81">bu͡olla, </ts>
               <ts e="T83" id="Seg_2579" n="e" s="T82">haka </ts>
               <ts e="T84" id="Seg_2581" n="e" s="T83">astammɨta. </ts>
               <ts e="T85" id="Seg_2583" n="e" s="T84">Uː </ts>
               <ts e="T86" id="Seg_2585" n="e" s="T85">kaːr </ts>
               <ts e="T87" id="Seg_2587" n="e" s="T86">bu͡olbut, </ts>
               <ts e="T88" id="Seg_2589" n="e" s="T87">innʼe </ts>
               <ts e="T89" id="Seg_2591" n="e" s="T88">gɨnan </ts>
               <ts e="T90" id="Seg_2593" n="e" s="T89">aha </ts>
               <ts e="T91" id="Seg_2595" n="e" s="T90">hu͡ok </ts>
               <ts e="T92" id="Seg_2597" n="e" s="T91">bu͡olan </ts>
               <ts e="T93" id="Seg_2599" n="e" s="T92">korgujan, </ts>
               <ts e="T94" id="Seg_2601" n="e" s="T93">hir </ts>
               <ts e="T95" id="Seg_2603" n="e" s="T94">taŋara </ts>
               <ts e="T96" id="Seg_2605" n="e" s="T95">kötön </ts>
               <ts e="T97" id="Seg_2607" n="e" s="T96">ispite </ts>
               <ts e="T98" id="Seg_2609" n="e" s="T97">sahɨl. </ts>
               <ts e="T99" id="Seg_2611" n="e" s="T98">Bu </ts>
               <ts e="T100" id="Seg_2613" n="e" s="T99">kötön </ts>
               <ts e="T101" id="Seg_2615" n="e" s="T100">ihen </ts>
               <ts e="T103" id="Seg_2617" n="e" s="T101">bu͡ollagɨna… </ts>
               <ts e="T104" id="Seg_2619" n="e" s="T103">"Oː, </ts>
               <ts e="T105" id="Seg_2621" n="e" s="T104">bu </ts>
               <ts e="T106" id="Seg_2623" n="e" s="T105">kantan </ts>
               <ts e="T107" id="Seg_2625" n="e" s="T106">bulu͡omuj", </ts>
               <ts e="T108" id="Seg_2627" n="e" s="T107">di͡en, </ts>
               <ts e="T109" id="Seg_2629" n="e" s="T108">mas </ts>
               <ts e="T110" id="Seg_2631" n="e" s="T109">ihiger </ts>
               <ts e="T111" id="Seg_2633" n="e" s="T110">kiːren, </ts>
               <ts e="T112" id="Seg_2635" n="e" s="T111">mas </ts>
               <ts e="T113" id="Seg_2637" n="e" s="T112">ihiger </ts>
               <ts e="T114" id="Seg_2639" n="e" s="T113">kötön </ts>
               <ts e="T115" id="Seg_2641" n="e" s="T114">iher. </ts>
               <ts e="T116" id="Seg_2643" n="e" s="T115">Kajaː, </ts>
               <ts e="T117" id="Seg_2645" n="e" s="T116">körör, </ts>
               <ts e="T118" id="Seg_2647" n="e" s="T117">kantaŋnɨː-kantaŋnɨː </ts>
               <ts e="T119" id="Seg_2649" n="e" s="T118">köppüte, </ts>
               <ts e="T120" id="Seg_2651" n="e" s="T119">oruk </ts>
               <ts e="T121" id="Seg_2653" n="e" s="T120">tiːt </ts>
               <ts e="T122" id="Seg_2655" n="e" s="T121">töbötüger </ts>
               <ts e="T123" id="Seg_2657" n="e" s="T122">har </ts>
               <ts e="T124" id="Seg_2659" n="e" s="T123">töröːn </ts>
               <ts e="T125" id="Seg_2661" n="e" s="T124">baran </ts>
               <ts e="T126" id="Seg_2663" n="e" s="T125">hɨtar. </ts>
               <ts e="T127" id="Seg_2665" n="e" s="T126">Kajaː! </ts>
               <ts e="T128" id="Seg_2667" n="e" s="T127">"Do, </ts>
               <ts e="T129" id="Seg_2669" n="e" s="T128">onton </ts>
               <ts e="T130" id="Seg_2671" n="e" s="T129">min </ts>
               <ts e="T131" id="Seg_2673" n="e" s="T130">kajdak </ts>
               <ts e="T132" id="Seg_2675" n="e" s="T131">gɨnammɨn, </ts>
               <ts e="T133" id="Seg_2677" n="e" s="T132">albɨnnanammɨn </ts>
               <ts e="T134" id="Seg_2679" n="e" s="T133">hi͡ebij </ts>
               <ts e="T135" id="Seg_2681" n="e" s="T134">iti </ts>
               <ts e="T136" id="Seg_2683" n="e" s="T135">hartan </ts>
               <ts e="T137" id="Seg_2685" n="e" s="T136">tugu, </ts>
               <ts e="T138" id="Seg_2687" n="e" s="T137">hɨmɨːtta?", </ts>
               <ts e="T139" id="Seg_2689" n="e" s="T138">di͡en </ts>
               <ts e="T140" id="Seg_2691" n="e" s="T139">bu </ts>
               <ts e="T141" id="Seg_2693" n="e" s="T140">kihi </ts>
               <ts e="T142" id="Seg_2695" n="e" s="T141">harga </ts>
               <ts e="T144" id="Seg_2697" n="e" s="T142">(ba-) </ts>
               <ts e="T145" id="Seg_2699" n="e" s="T144">hartan </ts>
               <ts e="T146" id="Seg_2701" n="e" s="T145">hɨmɨːt </ts>
               <ts e="T147" id="Seg_2703" n="e" s="T146">hi͡eri </ts>
               <ts e="T148" id="Seg_2705" n="e" s="T147">kötön </ts>
               <ts e="T149" id="Seg_2707" n="e" s="T148">kelen, </ts>
               <ts e="T150" id="Seg_2709" n="e" s="T149">haːmaj </ts>
               <ts e="T151" id="Seg_2711" n="e" s="T150">kimin </ts>
               <ts e="T152" id="Seg_2713" n="e" s="T151">annɨtɨgar </ts>
               <ts e="T153" id="Seg_2715" n="e" s="T152">kelbit </ts>
               <ts e="T154" id="Seg_2717" n="e" s="T153">tiːtin </ts>
               <ts e="T155" id="Seg_2719" n="e" s="T154">annɨtɨgar </ts>
               <ts e="T156" id="Seg_2721" n="e" s="T155">kelbit, </ts>
               <ts e="T157" id="Seg_2723" n="e" s="T156">innʼe </ts>
               <ts e="T158" id="Seg_2725" n="e" s="T157">gɨnan </ts>
               <ts e="T159" id="Seg_2727" n="e" s="T158">baraːn </ts>
               <ts e="T160" id="Seg_2729" n="e" s="T159">kuturugunan </ts>
               <ts e="T161" id="Seg_2731" n="e" s="T160">tiːtin </ts>
               <ts e="T162" id="Seg_2733" n="e" s="T161">kɨrbɨː </ts>
               <ts e="T163" id="Seg_2735" n="e" s="T162">turbuta. </ts>
               <ts e="T164" id="Seg_2737" n="e" s="T163">"Sar, </ts>
               <ts e="T165" id="Seg_2739" n="e" s="T164">sar, </ts>
               <ts e="T166" id="Seg_2741" n="e" s="T165">teheges </ts>
               <ts e="T167" id="Seg_2743" n="e" s="T166">kihi, </ts>
               <ts e="T168" id="Seg_2745" n="e" s="T167">egel </ts>
               <ts e="T169" id="Seg_2747" n="e" s="T168">biːr </ts>
               <ts e="T170" id="Seg_2749" n="e" s="T169">ogogun, </ts>
               <ts e="T171" id="Seg_2751" n="e" s="T170">min </ts>
               <ts e="T172" id="Seg_2753" n="e" s="T171">kuturugum </ts>
               <ts e="T173" id="Seg_2755" n="e" s="T172">nuːčča </ts>
               <ts e="T174" id="Seg_2757" n="e" s="T173">bolotunaːgar </ts>
               <ts e="T175" id="Seg_2759" n="e" s="T174">hɨtɨː, </ts>
               <ts e="T176" id="Seg_2761" n="e" s="T175">tiːttiːn </ts>
               <ts e="T177" id="Seg_2763" n="e" s="T176">bagastɨːn </ts>
               <ts e="T178" id="Seg_2765" n="e" s="T177">bɨha </ts>
               <ts e="T179" id="Seg_2767" n="e" s="T178">okson </ts>
               <ts e="T180" id="Seg_2769" n="e" s="T179">tüheri͡em, </ts>
               <ts e="T181" id="Seg_2771" n="e" s="T180">bejeliːŋŋin </ts>
               <ts e="T182" id="Seg_2773" n="e" s="T181">si͡em." </ts>
               <ts e="T183" id="Seg_2775" n="e" s="T182">Har </ts>
               <ts e="T184" id="Seg_2777" n="e" s="T183">bɨstɨ͡a </ts>
               <ts e="T185" id="Seg_2779" n="e" s="T184">du͡o, </ts>
               <ts e="T186" id="Seg_2781" n="e" s="T185">ɨtɨː-ɨtɨː </ts>
               <ts e="T187" id="Seg_2783" n="e" s="T186">biːr </ts>
               <ts e="T188" id="Seg_2785" n="e" s="T187">ogotun </ts>
               <ts e="T189" id="Seg_2787" n="e" s="T188">čekeris </ts>
               <ts e="T190" id="Seg_2789" n="e" s="T189">gɨnnarbɨt, </ts>
               <ts e="T191" id="Seg_2791" n="e" s="T190">biːr </ts>
               <ts e="T192" id="Seg_2793" n="e" s="T191">hɨmɨːtɨn </ts>
               <ts e="T193" id="Seg_2795" n="e" s="T192">hi͡ebit, </ts>
               <ts e="T194" id="Seg_2797" n="e" s="T193">innʼe </ts>
               <ts e="T195" id="Seg_2799" n="e" s="T194">gɨnan </ts>
               <ts e="T196" id="Seg_2801" n="e" s="T195">baraːn </ts>
               <ts e="T197" id="Seg_2803" n="e" s="T196">hir </ts>
               <ts e="T198" id="Seg_2805" n="e" s="T197">taŋara </ts>
               <ts e="T199" id="Seg_2807" n="e" s="T198">kötön </ts>
               <ts e="T200" id="Seg_2809" n="e" s="T199">kaːlbɨt. </ts>
               <ts e="T201" id="Seg_2811" n="e" s="T200">Harsɨŋŋɨtɨn </ts>
               <ts e="T202" id="Seg_2813" n="e" s="T201">turar </ts>
               <ts e="T203" id="Seg_2815" n="e" s="T202">emi͡e, </ts>
               <ts e="T204" id="Seg_2817" n="e" s="T203">harɨ </ts>
               <ts e="T205" id="Seg_2819" n="e" s="T204">(albɨnnaː-), </ts>
               <ts e="T206" id="Seg_2821" n="e" s="T205">harɨn </ts>
               <ts e="T207" id="Seg_2823" n="e" s="T206">albɨnnaːrɨ </ts>
               <ts e="T208" id="Seg_2825" n="e" s="T207">emi͡e </ts>
               <ts e="T209" id="Seg_2827" n="e" s="T208">kötön </ts>
               <ts e="T210" id="Seg_2829" n="e" s="T209">keler: </ts>
               <ts e="T211" id="Seg_2831" n="e" s="T210">"Sar, </ts>
               <ts e="T212" id="Seg_2833" n="e" s="T211">sar, </ts>
               <ts e="T213" id="Seg_2835" n="e" s="T212">teheges </ts>
               <ts e="T214" id="Seg_2837" n="e" s="T213">kihi, </ts>
               <ts e="T215" id="Seg_2839" n="e" s="T214">egel </ts>
               <ts e="T216" id="Seg_2841" n="e" s="T215">biːr </ts>
               <ts e="T217" id="Seg_2843" n="e" s="T216">ogogun. </ts>
               <ts e="T218" id="Seg_2845" n="e" s="T217">Min </ts>
               <ts e="T219" id="Seg_2847" n="e" s="T218">kuturugum </ts>
               <ts e="T220" id="Seg_2849" n="e" s="T219">nuːčča </ts>
               <ts e="T221" id="Seg_2851" n="e" s="T220">bolotunaːgar </ts>
               <ts e="T222" id="Seg_2853" n="e" s="T221">hɨtɨː, </ts>
               <ts e="T223" id="Seg_2855" n="e" s="T222">tiːttiːŋŋin-bagastɨːŋŋɨn </ts>
               <ts e="T224" id="Seg_2857" n="e" s="T223">bɨha </ts>
               <ts e="T225" id="Seg_2859" n="e" s="T224">okson </ts>
               <ts e="T226" id="Seg_2861" n="e" s="T225">tüheremmin, </ts>
               <ts e="T227" id="Seg_2863" n="e" s="T226">anɨ </ts>
               <ts e="T228" id="Seg_2865" n="e" s="T227">bejeliːŋŋin </ts>
               <ts e="T229" id="Seg_2867" n="e" s="T228">si͡em". </ts>
               <ts e="T230" id="Seg_2869" n="e" s="T229">Emi͡e </ts>
               <ts e="T231" id="Seg_2871" n="e" s="T230">biːr </ts>
               <ts e="T232" id="Seg_2873" n="e" s="T231">ogotun </ts>
               <ts e="T233" id="Seg_2875" n="e" s="T232">üːten </ts>
               <ts e="T234" id="Seg_2877" n="e" s="T233">keːspit. </ts>
               <ts e="T235" id="Seg_2879" n="e" s="T234">Ikki </ts>
               <ts e="T236" id="Seg_2881" n="e" s="T235">ogotun </ts>
               <ts e="T237" id="Seg_2883" n="e" s="T236">hi͡ebit – </ts>
               <ts e="T238" id="Seg_2885" n="e" s="T237">hahɨl. </ts>
               <ts e="T239" id="Seg_2887" n="e" s="T238">Har </ts>
               <ts e="T240" id="Seg_2889" n="e" s="T239">ɨtɨː </ts>
               <ts e="T241" id="Seg_2891" n="e" s="T240">oloroːktoːbut </ts>
               <ts e="T242" id="Seg_2893" n="e" s="T241">diːn. </ts>
               <ts e="T243" id="Seg_2895" n="e" s="T242">"Doː, </ts>
               <ts e="T244" id="Seg_2897" n="e" s="T243">bu, </ts>
               <ts e="T245" id="Seg_2899" n="e" s="T244">anɨ </ts>
               <ts e="T246" id="Seg_2901" n="e" s="T245">bu͡ollagɨna </ts>
               <ts e="T247" id="Seg_2903" n="e" s="T246">bejeliːmmin </ts>
               <ts e="T248" id="Seg_2905" n="e" s="T247">si͡eni </ts>
               <ts e="T249" id="Seg_2907" n="e" s="T248">di͡en", </ts>
               <ts e="T250" id="Seg_2909" n="e" s="T249">ɨtɨː </ts>
               <ts e="T251" id="Seg_2911" n="e" s="T250">olorbut. </ts>
               <ts e="T252" id="Seg_2913" n="e" s="T251">Kaja, </ts>
               <ts e="T253" id="Seg_2915" n="e" s="T252">bu </ts>
               <ts e="T254" id="Seg_2917" n="e" s="T253">olordoguna, </ts>
               <ts e="T255" id="Seg_2919" n="e" s="T254">kukaːkiː </ts>
               <ts e="T256" id="Seg_2921" n="e" s="T255">kötön </ts>
               <ts e="T257" id="Seg_2923" n="e" s="T256">kelbit. </ts>
               <ts e="T258" id="Seg_2925" n="e" s="T257">"Kajaː, </ts>
               <ts e="T259" id="Seg_2927" n="e" s="T258">har, </ts>
               <ts e="T260" id="Seg_2929" n="e" s="T259">tu͡oktan </ts>
               <ts e="T261" id="Seg_2931" n="e" s="T260">ɨtɨːgɨn </ts>
               <ts e="T262" id="Seg_2933" n="e" s="T261">ki͡e </ts>
               <ts e="T263" id="Seg_2935" n="e" s="T262">en </ts>
               <ts e="T264" id="Seg_2937" n="e" s="T263">bu? </ts>
               <ts e="T265" id="Seg_2939" n="e" s="T264">Munnuk </ts>
               <ts e="T266" id="Seg_2941" n="e" s="T265">bosku͡oj </ts>
               <ts e="T267" id="Seg_2943" n="e" s="T266">künneːk </ts>
               <ts e="T268" id="Seg_2945" n="e" s="T267">kün </ts>
               <ts e="T269" id="Seg_2947" n="e" s="T268">tɨŋa </ts>
               <ts e="T270" id="Seg_2949" n="e" s="T269">turdagɨna, </ts>
               <ts e="T271" id="Seg_2951" n="e" s="T270">ogoloruŋ </ts>
               <ts e="T272" id="Seg_2953" n="e" s="T271">da </ts>
               <ts e="T273" id="Seg_2955" n="e" s="T272">ičigeske </ts>
               <ts e="T274" id="Seg_2957" n="e" s="T273">hɨtallar, </ts>
               <ts e="T275" id="Seg_2959" n="e" s="T274">bejeŋ </ts>
               <ts e="T276" id="Seg_2961" n="e" s="T275">da </ts>
               <ts e="T277" id="Seg_2963" n="e" s="T276">ahɨ </ts>
               <ts e="T278" id="Seg_2965" n="e" s="T277">bagas </ts>
               <ts e="T279" id="Seg_2967" n="e" s="T278">egelen </ts>
               <ts e="T280" id="Seg_2969" n="e" s="T279">keler </ts>
               <ts e="T281" id="Seg_2971" n="e" s="T280">kördük </ts>
               <ts e="T282" id="Seg_2973" n="e" s="T281">bu͡o. </ts>
               <ts e="T283" id="Seg_2975" n="e" s="T282">Tu͡oktan </ts>
               <ts e="T284" id="Seg_2977" n="e" s="T283">ɨtɨːgɨn </ts>
               <ts e="T285" id="Seg_2979" n="e" s="T284">dʼe, </ts>
               <ts e="T286" id="Seg_2981" n="e" s="T285">munnuk </ts>
               <ts e="T287" id="Seg_2983" n="e" s="T286">beseleːge </ts>
               <ts e="T288" id="Seg_2985" n="e" s="T287">oloroŋŋun", </ts>
               <ts e="T289" id="Seg_2987" n="e" s="T288">diːr. </ts>
               <ts e="T290" id="Seg_2989" n="e" s="T289">"En </ts>
               <ts e="T291" id="Seg_2991" n="e" s="T290">kajdak </ts>
               <ts e="T292" id="Seg_2993" n="e" s="T291">ɨtappakkɨn? </ts>
               <ts e="T293" id="Seg_2995" n="e" s="T292">Körbökkün </ts>
               <ts e="T294" id="Seg_2997" n="e" s="T293">du͡o? </ts>
               <ts e="T295" id="Seg_2999" n="e" s="T294">Minigin </ts>
               <ts e="T296" id="Seg_3001" n="e" s="T295">bu͡ollagɨna </ts>
               <ts e="T297" id="Seg_3003" n="e" s="T296">hahɨl </ts>
               <ts e="T298" id="Seg_3005" n="e" s="T297">bu͡olla </ts>
               <ts e="T299" id="Seg_3007" n="e" s="T298">ikki </ts>
               <ts e="T300" id="Seg_3009" n="e" s="T299">küŋŋe </ts>
               <ts e="T301" id="Seg_3011" n="e" s="T300">ikki </ts>
               <ts e="T302" id="Seg_3013" n="e" s="T301">ogobun </ts>
               <ts e="T303" id="Seg_3015" n="e" s="T302">hi͡ete. </ts>
               <ts e="T304" id="Seg_3017" n="e" s="T303">Anɨ </ts>
               <ts e="T305" id="Seg_3019" n="e" s="T304">bügün </ts>
               <ts e="T306" id="Seg_3021" n="e" s="T305">emi͡e </ts>
               <ts e="T307" id="Seg_3023" n="e" s="T306">keli͡ege, </ts>
               <ts e="T308" id="Seg_3025" n="e" s="T307">hi͡ege. </ts>
               <ts e="T309" id="Seg_3027" n="e" s="T308">Tiːttiːmmin, </ts>
               <ts e="T310" id="Seg_3029" n="e" s="T309">bagastɨːmmɨn </ts>
               <ts e="T311" id="Seg_3031" n="e" s="T310">bɨha </ts>
               <ts e="T312" id="Seg_3033" n="e" s="T311">okson </ts>
               <ts e="T313" id="Seg_3035" n="e" s="T312">tüheren </ts>
               <ts e="T314" id="Seg_3037" n="e" s="T313">bejeliːmmin </ts>
               <ts e="T315" id="Seg_3039" n="e" s="T314">si͡eri </ts>
               <ts e="T316" id="Seg_3041" n="e" s="T315">gɨnna", </ts>
               <ts e="T317" id="Seg_3043" n="e" s="T316">diːr. </ts>
               <ts e="T318" id="Seg_3045" n="e" s="T317">"Tu͡oktaːgɨj </ts>
               <ts e="T319" id="Seg_3047" n="e" s="T318">ol </ts>
               <ts e="T320" id="Seg_3049" n="e" s="T319">sahɨlɨŋ?" </ts>
               <ts e="T321" id="Seg_3051" n="e" s="T320">"Kaja </ts>
               <ts e="T322" id="Seg_3053" n="e" s="T321">kuturuga </ts>
               <ts e="T323" id="Seg_3055" n="e" s="T322">nuːčča </ts>
               <ts e="T324" id="Seg_3057" n="e" s="T323">bolotunaːgar </ts>
               <ts e="T325" id="Seg_3059" n="e" s="T324">hɨtɨː". </ts>
               <ts e="T326" id="Seg_3061" n="e" s="T325">"Doː, </ts>
               <ts e="T327" id="Seg_3063" n="e" s="T326">tu͡ok </ts>
               <ts e="T328" id="Seg_3065" n="e" s="T327">aːttaːga </ts>
               <ts e="T329" id="Seg_3067" n="e" s="T328">aŋɨr </ts>
               <ts e="T330" id="Seg_3069" n="e" s="T329">kötörö </ts>
               <ts e="T331" id="Seg_3071" n="e" s="T330">töröːbütüŋüj </ts>
               <ts e="T332" id="Seg_3073" n="e" s="T331">en </ts>
               <ts e="T333" id="Seg_3075" n="e" s="T332">munnuk? </ts>
               <ts e="T334" id="Seg_3077" n="e" s="T333">Dʼeː, </ts>
               <ts e="T335" id="Seg_3079" n="e" s="T334">(har </ts>
               <ts e="T336" id="Seg_3081" n="e" s="T335">kajdak </ts>
               <ts e="T337" id="Seg_3083" n="e" s="T336">gɨna) </ts>
               <ts e="T338" id="Seg_3085" n="e" s="T337">hahɨl </ts>
               <ts e="T339" id="Seg_3087" n="e" s="T338">kajdak </ts>
               <ts e="T340" id="Seg_3089" n="e" s="T339">gɨnan </ts>
               <ts e="T341" id="Seg_3091" n="e" s="T340">en </ts>
               <ts e="T342" id="Seg_3093" n="e" s="T341">tiːtkin </ts>
               <ts e="T343" id="Seg_3095" n="e" s="T342">bɨha </ts>
               <ts e="T344" id="Seg_3097" n="e" s="T343">oksu͡oj? </ts>
               <ts e="T345" id="Seg_3099" n="e" s="T344">Kuturuga </ts>
               <ts e="T346" id="Seg_3101" n="e" s="T345">tüː. </ts>
               <ts e="T347" id="Seg_3103" n="e" s="T346">Tüː </ts>
               <ts e="T348" id="Seg_3105" n="e" s="T347">kuturuk, </ts>
               <ts e="T349" id="Seg_3107" n="e" s="T348">tugu </ts>
               <ts e="T350" id="Seg_3109" n="e" s="T349">kotu͡oŋuj", </ts>
               <ts e="T351" id="Seg_3111" n="e" s="T350">di͡e </ts>
               <ts e="T352" id="Seg_3113" n="e" s="T351">anɨ </ts>
               <ts e="T353" id="Seg_3115" n="e" s="T352">kellegine. </ts>
               <ts e="T354" id="Seg_3117" n="e" s="T353">Kim </ts>
               <ts e="T355" id="Seg_3119" n="e" s="T354">ü͡öreppitej, </ts>
               <ts e="T356" id="Seg_3121" n="e" s="T355">di͡etegine, </ts>
               <ts e="T357" id="Seg_3123" n="e" s="T356">kukaːkiː </ts>
               <ts e="T358" id="Seg_3125" n="e" s="T357">ü͡öreppite, </ts>
               <ts e="T359" id="Seg_3127" n="e" s="T358">diːr". </ts>
               <ts e="T360" id="Seg_3129" n="e" s="T359">Dʼe, </ts>
               <ts e="T361" id="Seg_3131" n="e" s="T360">har </ts>
               <ts e="T363" id="Seg_3133" n="e" s="T361">(ha-) </ts>
               <ts e="T364" id="Seg_3135" n="e" s="T363">har </ts>
               <ts e="T365" id="Seg_3137" n="e" s="T364">dʼe </ts>
               <ts e="T366" id="Seg_3139" n="e" s="T365">hi͡ese </ts>
               <ts e="T367" id="Seg_3141" n="e" s="T366">hek </ts>
               <ts e="T368" id="Seg_3143" n="e" s="T367">bu͡olbut, </ts>
               <ts e="T369" id="Seg_3145" n="e" s="T368">olorbut, </ts>
               <ts e="T370" id="Seg_3147" n="e" s="T369">kukaːkiːta </ts>
               <ts e="T371" id="Seg_3149" n="e" s="T370">kötön </ts>
               <ts e="T372" id="Seg_3151" n="e" s="T371">kaːlbɨt. </ts>
               <ts e="T373" id="Seg_3153" n="e" s="T372">Oː, </ts>
               <ts e="T374" id="Seg_3155" n="e" s="T373">dʼe </ts>
               <ts e="T375" id="Seg_3157" n="e" s="T374">sahɨl </ts>
               <ts e="T376" id="Seg_3159" n="e" s="T375">kɨjŋanaːrɨ </ts>
               <ts e="T377" id="Seg_3161" n="e" s="T376">kɨjŋammɨt </ts>
               <ts e="T378" id="Seg_3163" n="e" s="T377">bu͡olbut, </ts>
               <ts e="T379" id="Seg_3165" n="e" s="T378">kɨːhɨraːrɨ </ts>
               <ts e="T380" id="Seg_3167" n="e" s="T379">kɨːhɨrbɨt </ts>
               <ts e="T381" id="Seg_3169" n="e" s="T380">bu͡olbut: </ts>
               <ts e="T382" id="Seg_3171" n="e" s="T381">Eŋin-eŋin </ts>
               <ts e="T383" id="Seg_3173" n="e" s="T382">köppögü </ts>
               <ts e="T384" id="Seg_3175" n="e" s="T383">turuta </ts>
               <ts e="T385" id="Seg_3177" n="e" s="T384">tebi͡eliː-tebi͡eliː </ts>
               <ts e="T386" id="Seg_3179" n="e" s="T385">ilin </ts>
               <ts e="T387" id="Seg_3181" n="e" s="T386">kalin </ts>
               <ts e="T388" id="Seg_3183" n="e" s="T387">atagɨnan </ts>
               <ts e="T389" id="Seg_3185" n="e" s="T388">kajɨta </ts>
               <ts e="T390" id="Seg_3187" n="e" s="T389">tebi͡eliː-tebi͡eliː </ts>
               <ts e="T391" id="Seg_3189" n="e" s="T390">keleːkteːbit, </ts>
               <ts e="T392" id="Seg_3191" n="e" s="T391">diːn </ts>
               <ts e="T393" id="Seg_3193" n="e" s="T392">(harɨga) </ts>
               <ts e="T394" id="Seg_3195" n="e" s="T393">harɨ </ts>
               <ts e="T395" id="Seg_3197" n="e" s="T394">kuttaːrɨ. </ts>
               <ts e="T396" id="Seg_3199" n="e" s="T395">"Sar, </ts>
               <ts e="T397" id="Seg_3201" n="e" s="T396">sar, </ts>
               <ts e="T398" id="Seg_3203" n="e" s="T397">onnuk-mannɨk </ts>
               <ts e="T399" id="Seg_3205" n="e" s="T398">inʼeleːk, </ts>
               <ts e="T400" id="Seg_3207" n="e" s="T399">kaja, </ts>
               <ts e="T401" id="Seg_3209" n="e" s="T400">min </ts>
               <ts e="T402" id="Seg_3211" n="e" s="T401">bu͡ollagɨna </ts>
               <ts e="T403" id="Seg_3213" n="e" s="T402">emi͡e </ts>
               <ts e="T404" id="Seg_3215" n="e" s="T403">bu͡opsa </ts>
               <ts e="T405" id="Seg_3217" n="e" s="T404">kɨjŋanaːrɨ </ts>
               <ts e="T406" id="Seg_3219" n="e" s="T405">kɨjŋannɨm. </ts>
               <ts e="T407" id="Seg_3221" n="e" s="T406">Egel </ts>
               <ts e="T408" id="Seg_3223" n="e" s="T407">biːr </ts>
               <ts e="T409" id="Seg_3225" n="e" s="T408">ogoto, </ts>
               <ts e="T410" id="Seg_3227" n="e" s="T409">min </ts>
               <ts e="T411" id="Seg_3229" n="e" s="T410">kuturugum </ts>
               <ts e="T412" id="Seg_3231" n="e" s="T411">nʼuːčča </ts>
               <ts e="T413" id="Seg_3233" n="e" s="T412">bolotunaːgar </ts>
               <ts e="T414" id="Seg_3235" n="e" s="T413">hɨtɨː, </ts>
               <ts e="T415" id="Seg_3237" n="e" s="T414">hanɨ </ts>
               <ts e="T416" id="Seg_3239" n="e" s="T415">bejeliːŋŋin </ts>
               <ts e="T418" id="Seg_3241" n="e" s="T416">(tu-) </ts>
               <ts e="T419" id="Seg_3243" n="e" s="T418">tuːra </ts>
               <ts e="T420" id="Seg_3245" n="e" s="T419">oksommun, </ts>
               <ts e="T421" id="Seg_3247" n="e" s="T420">bejeliːŋŋin </ts>
               <ts e="T422" id="Seg_3249" n="e" s="T421">si͡em </ts>
               <ts e="T423" id="Seg_3251" n="e" s="T422">onnoːgor". </ts>
               <ts e="T424" id="Seg_3253" n="e" s="T423">"Koː, </ts>
               <ts e="T425" id="Seg_3255" n="e" s="T424">aŋɨrdaːmmɨn </ts>
               <ts e="T426" id="Seg_3257" n="e" s="T425">bu͡opsa </ts>
               <ts e="T429" id="Seg_3259" n="e" s="T426">taːk-ta </ts>
               <ts e="T430" id="Seg_3261" n="e" s="T429">kaːrɨjan </ts>
               <ts e="T431" id="Seg_3263" n="e" s="T430">ikki </ts>
               <ts e="T432" id="Seg_3265" n="e" s="T431">ogobun </ts>
               <ts e="T433" id="Seg_3267" n="e" s="T432">hi͡ettim </ts>
               <ts e="T434" id="Seg_3269" n="e" s="T433">min </ts>
               <ts e="T435" id="Seg_3271" n="e" s="T434">eni͡eke, </ts>
               <ts e="T436" id="Seg_3273" n="e" s="T435">bejem </ts>
               <ts e="T437" id="Seg_3275" n="e" s="T436">aŋɨrbar. </ts>
               <ts e="T438" id="Seg_3277" n="e" s="T437">En </ts>
               <ts e="T439" id="Seg_3279" n="e" s="T438">bu͡ollagɨna </ts>
               <ts e="T440" id="Seg_3281" n="e" s="T439">tüː </ts>
               <ts e="T441" id="Seg_3283" n="e" s="T440">kuturukkun </ts>
               <ts e="T442" id="Seg_3285" n="e" s="T441">diːn, </ts>
               <ts e="T443" id="Seg_3287" n="e" s="T442">tüː </ts>
               <ts e="T444" id="Seg_3289" n="e" s="T443">kuturuk, </ts>
               <ts e="T445" id="Seg_3291" n="e" s="T444">tugu </ts>
               <ts e="T446" id="Seg_3293" n="e" s="T445">kotu͡oŋuj, </ts>
               <ts e="T447" id="Seg_3295" n="e" s="T446">mahɨ </ts>
               <ts e="T448" id="Seg_3297" n="e" s="T447">kotu͡oŋ </ts>
               <ts e="T449" id="Seg_3299" n="e" s="T448">du͡o", </ts>
               <ts e="T450" id="Seg_3301" n="e" s="T449">diːr. </ts>
               <ts e="T451" id="Seg_3303" n="e" s="T450">"Koː, </ts>
               <ts e="T452" id="Seg_3305" n="e" s="T451">arakata, </ts>
               <ts e="T453" id="Seg_3307" n="e" s="T452">tu͡ok </ts>
               <ts e="T454" id="Seg_3309" n="e" s="T453">aːttaːga </ts>
               <ts e="T455" id="Seg_3311" n="e" s="T454">öj </ts>
               <ts e="T456" id="Seg_3313" n="e" s="T455">bi͡ertej </ts>
               <ts e="T457" id="Seg_3315" n="e" s="T456">eni͡eke, </ts>
               <ts e="T458" id="Seg_3317" n="e" s="T457">kajdak </ts>
               <ts e="T459" id="Seg_3319" n="e" s="T458">gɨnaŋŋɨn </ts>
               <ts e="T460" id="Seg_3321" n="e" s="T459">öjü </ts>
               <ts e="T461" id="Seg_3323" n="e" s="T460">bultuŋuj", </ts>
               <ts e="T462" id="Seg_3325" n="e" s="T461">diːr, </ts>
               <ts e="T463" id="Seg_3327" n="e" s="T462">"mini͡ene </ts>
               <ts e="T464" id="Seg_3329" n="e" s="T463">kuturugum </ts>
               <ts e="T465" id="Seg_3331" n="e" s="T464">tüːtün </ts>
               <ts e="T466" id="Seg_3333" n="e" s="T465">kantan </ts>
               <ts e="T467" id="Seg_3335" n="e" s="T466">bilegin?" </ts>
               <ts e="T468" id="Seg_3337" n="e" s="T467">"Kaja </ts>
               <ts e="T469" id="Seg_3339" n="e" s="T468">minigin </ts>
               <ts e="T470" id="Seg_3341" n="e" s="T469">kukaːkiː </ts>
               <ts e="T471" id="Seg_3343" n="e" s="T470">ü͡öreppite, </ts>
               <ts e="T472" id="Seg_3345" n="e" s="T471">kim </ts>
               <ts e="T473" id="Seg_3347" n="e" s="T472">bi͡eri͡ej </ts>
               <ts e="T474" id="Seg_3349" n="e" s="T473">mini͡eke </ts>
               <ts e="T475" id="Seg_3351" n="e" s="T474">öj, </ts>
               <ts e="T476" id="Seg_3353" n="e" s="T475">kukaːkiː </ts>
               <ts e="T477" id="Seg_3355" n="e" s="T476">ü͡öreppite." </ts>
               <ts e="T478" id="Seg_3357" n="e" s="T477">"Kajaː, </ts>
               <ts e="T479" id="Seg_3359" n="e" s="T478">onnuk-mannɨk </ts>
               <ts e="T480" id="Seg_3361" n="e" s="T479">otto </ts>
               <ts e="T481" id="Seg_3363" n="e" s="T480">kukaːkinɨ, </ts>
               <ts e="T482" id="Seg_3365" n="e" s="T481">kaja </ts>
               <ts e="T483" id="Seg_3367" n="e" s="T482">hirten </ts>
               <ts e="T484" id="Seg_3369" n="e" s="T483">bulu͡obuj, </ts>
               <ts e="T485" id="Seg_3371" n="e" s="T484">dʼe, </ts>
               <ts e="T486" id="Seg_3373" n="e" s="T485">beːbe, </ts>
               <ts e="T487" id="Seg_3375" n="e" s="T486">dʼe </ts>
               <ts e="T488" id="Seg_3377" n="e" s="T487">u͡oldʼahɨ͡am </ts>
               <ts e="T489" id="Seg_3379" n="e" s="T488">min </ts>
               <ts e="T490" id="Seg_3381" n="e" s="T489">gini͡eke </ts>
               <ts e="T491" id="Seg_3383" n="e" s="T490">daːgɨnɨ". </ts>
               <ts e="T492" id="Seg_3385" n="e" s="T491">Innʼe </ts>
               <ts e="T493" id="Seg_3387" n="e" s="T492">gɨmmɨt </ts>
               <ts e="T494" id="Seg_3389" n="e" s="T493">da, </ts>
               <ts e="T495" id="Seg_3391" n="e" s="T494">hir </ts>
               <ts e="T496" id="Seg_3393" n="e" s="T495">taŋara </ts>
               <ts e="T497" id="Seg_3395" n="e" s="T496">kötön </ts>
               <ts e="T498" id="Seg_3397" n="e" s="T497">kaːlbɨt, </ts>
               <ts e="T499" id="Seg_3399" n="e" s="T498">hir </ts>
               <ts e="T500" id="Seg_3401" n="e" s="T499">taŋara </ts>
               <ts e="T501" id="Seg_3403" n="e" s="T500">kötön </ts>
               <ts e="T502" id="Seg_3405" n="e" s="T501">ihen </ts>
               <ts e="T503" id="Seg_3407" n="e" s="T502">bu͡olla, </ts>
               <ts e="T504" id="Seg_3409" n="e" s="T503">töp tögürük </ts>
               <ts e="T505" id="Seg_3411" n="e" s="T504">kü͡ölge </ts>
               <ts e="T507" id="Seg_3413" n="e" s="T505">tijen… </ts>
               <ts e="T508" id="Seg_3415" n="e" s="T507">Bu </ts>
               <ts e="T509" id="Seg_3417" n="e" s="T508">kü͡öl </ts>
               <ts e="T510" id="Seg_3419" n="e" s="T509">tu͡oktan </ts>
               <ts e="T511" id="Seg_3421" n="e" s="T510">daː </ts>
               <ts e="T512" id="Seg_3423" n="e" s="T511">ulakan </ts>
               <ts e="T513" id="Seg_3425" n="e" s="T512">kajalaːk. </ts>
               <ts e="T514" id="Seg_3427" n="e" s="T513">Haːs </ts>
               <ts e="T515" id="Seg_3429" n="e" s="T514">kemiger </ts>
               <ts e="T516" id="Seg_3431" n="e" s="T515">bu͡olla </ts>
               <ts e="T517" id="Seg_3433" n="e" s="T516">keriːskete </ts>
               <ts e="T518" id="Seg_3435" n="e" s="T517">barɨta </ts>
               <ts e="T519" id="Seg_3437" n="e" s="T518">taksɨbɨt. </ts>
               <ts e="T520" id="Seg_3439" n="e" s="T519">Bu </ts>
               <ts e="T521" id="Seg_3441" n="e" s="T520">keriːskeni </ts>
               <ts e="T522" id="Seg_3443" n="e" s="T521">bu͡ollagɨna </ts>
               <ts e="T523" id="Seg_3445" n="e" s="T522">kötö </ts>
               <ts e="T524" id="Seg_3447" n="e" s="T523">hɨldʼan, </ts>
               <ts e="T525" id="Seg_3449" n="e" s="T524">bu͡opsa </ts>
               <ts e="T526" id="Seg_3451" n="e" s="T525">muŋ </ts>
               <ts e="T527" id="Seg_3453" n="e" s="T526">bu͡osku͡oj </ts>
               <ts e="T528" id="Seg_3455" n="e" s="T527">karadʼɨkka </ts>
               <ts e="T529" id="Seg_3457" n="e" s="T528">bu͡ollagɨna, </ts>
               <ts e="T530" id="Seg_3459" n="e" s="T529">ölbüt </ts>
               <ts e="T531" id="Seg_3461" n="e" s="T530">sahɨl </ts>
               <ts e="T532" id="Seg_3463" n="e" s="T531">bu͡olan </ts>
               <ts e="T533" id="Seg_3465" n="e" s="T532">baran, </ts>
               <ts e="T534" id="Seg_3467" n="e" s="T533">neles </ts>
               <ts e="T535" id="Seg_3469" n="e" s="T534">gɨna </ts>
               <ts e="T536" id="Seg_3471" n="e" s="T535">tüspüt. </ts>
               <ts e="T537" id="Seg_3473" n="e" s="T536">Bu͡opsa </ts>
               <ts e="T538" id="Seg_3475" n="e" s="T537">ü͡öŋŋe </ts>
               <ts e="T539" id="Seg_3477" n="e" s="T538">barɨtɨgar </ts>
               <ts e="T540" id="Seg_3479" n="e" s="T539">ɨstara </ts>
               <ts e="T541" id="Seg_3481" n="e" s="T540">hɨppɨt </ts>
               <ts e="T542" id="Seg_3483" n="e" s="T541">ol </ts>
               <ts e="T543" id="Seg_3485" n="e" s="T542">gɨnan </ts>
               <ts e="T544" id="Seg_3487" n="e" s="T543">baraːn. </ts>
               <ts e="T545" id="Seg_3489" n="e" s="T544">Kajaː, </ts>
               <ts e="T546" id="Seg_3491" n="e" s="T545">kukaːkiːlar </ts>
               <ts e="T547" id="Seg_3493" n="e" s="T546">daːganɨ </ts>
               <ts e="T548" id="Seg_3495" n="e" s="T547">kötö </ts>
               <ts e="T549" id="Seg_3497" n="e" s="T548">hɨldʼan </ts>
               <ts e="T550" id="Seg_3499" n="e" s="T549">eren </ts>
               <ts e="T551" id="Seg_3501" n="e" s="T550">körör – </ts>
               <ts e="T552" id="Seg_3503" n="e" s="T551">sahɨl </ts>
               <ts e="T553" id="Seg_3505" n="e" s="T552">ölön </ts>
               <ts e="T554" id="Seg_3507" n="e" s="T553">baraːn </ts>
               <ts e="T555" id="Seg_3509" n="e" s="T554">nelejen </ts>
               <ts e="T556" id="Seg_3511" n="e" s="T555">hɨtar. </ts>
               <ts e="T557" id="Seg_3513" n="e" s="T556">"Oː, </ts>
               <ts e="T558" id="Seg_3515" n="e" s="T557">baː </ts>
               <ts e="T559" id="Seg_3517" n="e" s="T558">turkarɨ </ts>
               <ts e="T560" id="Seg_3519" n="e" s="T559">ba </ts>
               <ts e="T561" id="Seg_3521" n="e" s="T560">korgujan </ts>
               <ts e="T562" id="Seg_3523" n="e" s="T561">bu͡ollagɨna </ts>
               <ts e="T563" id="Seg_3525" n="e" s="T562">har </ts>
               <ts e="T564" id="Seg_3527" n="e" s="T563">ogolorun </ts>
               <ts e="T565" id="Seg_3529" n="e" s="T564">siː </ts>
               <ts e="T566" id="Seg_3531" n="e" s="T565">hɨldʼɨbɨta </ts>
               <ts e="T567" id="Seg_3533" n="e" s="T566">bu͡olla. </ts>
               <ts e="T568" id="Seg_3535" n="e" s="T567">Anɨ </ts>
               <ts e="T569" id="Seg_3537" n="e" s="T568">sara </ts>
               <ts e="T570" id="Seg_3539" n="e" s="T569">bu͡ollagɨna </ts>
               <ts e="T571" id="Seg_3541" n="e" s="T570">minigitten </ts>
               <ts e="T572" id="Seg_3543" n="e" s="T571">öj </ts>
               <ts e="T573" id="Seg_3545" n="e" s="T572">ɨlar, </ts>
               <ts e="T574" id="Seg_3547" n="e" s="T573">biːr </ts>
               <ts e="T575" id="Seg_3549" n="e" s="T574">da </ts>
               <ts e="T576" id="Seg_3551" n="e" s="T575">ogotun </ts>
               <ts e="T577" id="Seg_3553" n="e" s="T576">bi͡erbetek. </ts>
               <ts e="T578" id="Seg_3555" n="e" s="T577">Oččogo </ts>
               <ts e="T579" id="Seg_3557" n="e" s="T578">kantan </ts>
               <ts e="T580" id="Seg_3559" n="e" s="T579">ɨlan </ts>
               <ts e="T581" id="Seg_3561" n="e" s="T580">gini </ts>
               <ts e="T582" id="Seg_3563" n="e" s="T581">daːganɨ </ts>
               <ts e="T583" id="Seg_3565" n="e" s="T582">korgujbut </ts>
               <ts e="T584" id="Seg_3567" n="e" s="T583">kihi, </ts>
               <ts e="T585" id="Seg_3569" n="e" s="T584">kantan </ts>
               <ts e="T586" id="Seg_3571" n="e" s="T585">ɨlan </ts>
               <ts e="T587" id="Seg_3573" n="e" s="T586">ahɨ͡aj, </ts>
               <ts e="T588" id="Seg_3575" n="e" s="T587">dʼe, </ts>
               <ts e="T589" id="Seg_3577" n="e" s="T588">okton </ts>
               <ts e="T590" id="Seg_3579" n="e" s="T589">ölbüt. </ts>
               <ts e="T591" id="Seg_3581" n="e" s="T590">Dʼe, </ts>
               <ts e="T592" id="Seg_3583" n="e" s="T591">en </ts>
               <ts e="T593" id="Seg_3585" n="e" s="T592">daːganɨ </ts>
               <ts e="T594" id="Seg_3587" n="e" s="T593">ölördöːkkün </ts>
               <ts e="T595" id="Seg_3589" n="e" s="T594">ebit" </ts>
               <ts e="T596" id="Seg_3591" n="e" s="T595">di͡en. </ts>
               <ts e="T597" id="Seg_3593" n="e" s="T596">Kaja </ts>
               <ts e="T598" id="Seg_3595" n="e" s="T597">kelbit. </ts>
               <ts e="T599" id="Seg_3597" n="e" s="T598">Loŋ-loŋ, </ts>
               <ts e="T600" id="Seg_3599" n="e" s="T599">loŋ-loŋ – </ts>
               <ts e="T601" id="Seg_3601" n="e" s="T600">loŋsujbut </ts>
               <ts e="T602" id="Seg_3603" n="e" s="T601">bu͡o. </ts>
               <ts e="T603" id="Seg_3605" n="e" s="T602">Kantɨttan, </ts>
               <ts e="T604" id="Seg_3607" n="e" s="T603">karagɨttan </ts>
               <ts e="T605" id="Seg_3609" n="e" s="T604">hi͡ekke, </ts>
               <ts e="T606" id="Seg_3611" n="e" s="T605">di͡en, </ts>
               <ts e="T607" id="Seg_3613" n="e" s="T606">karɨga </ts>
               <ts e="T608" id="Seg_3615" n="e" s="T607">loŋsuna </ts>
               <ts e="T609" id="Seg_3617" n="e" s="T608">hɨrɨttagɨna. </ts>
               <ts e="T610" id="Seg_3619" n="e" s="T609">Bu͡opsa </ts>
               <ts e="T682" id="Seg_3621" n="e" s="T610">((…)) </ts>
               <ts e="T611" id="Seg_3623" n="e" s="T682">sahɨl </ts>
               <ts e="T612" id="Seg_3625" n="e" s="T611">bu͡ollagɨna </ts>
               <ts e="T613" id="Seg_3627" n="e" s="T612">ele </ts>
               <ts e="T614" id="Seg_3629" n="e" s="T613">nʼɨmatɨnan, </ts>
               <ts e="T615" id="Seg_3631" n="e" s="T614">ele </ts>
               <ts e="T616" id="Seg_3633" n="e" s="T615">kütürünen </ts>
               <ts e="T617" id="Seg_3635" n="e" s="T616">ɨrdʼɨgɨnɨː </ts>
               <ts e="T618" id="Seg_3637" n="e" s="T617">gɨna </ts>
               <ts e="T619" id="Seg_3639" n="e" s="T618">ɨstanaːktaːbɨt </ts>
               <ts e="T621" id="Seg_3641" n="e" s="T619">diːn. </ts>
               <ts e="T622" id="Seg_3643" n="e" s="T621">Baːgɨ </ts>
               <ts e="T623" id="Seg_3645" n="e" s="T622">sar </ts>
               <ts e="T624" id="Seg_3647" n="e" s="T623">bu͡ollagɨna, </ts>
               <ts e="T625" id="Seg_3649" n="e" s="T624">kim </ts>
               <ts e="T626" id="Seg_3651" n="e" s="T625">bu͡ollagɨna, </ts>
               <ts e="T627" id="Seg_3653" n="e" s="T626">kukaːkiː </ts>
               <ts e="T628" id="Seg_3655" n="e" s="T627">bu͡ollagɨna </ts>
               <ts e="T629" id="Seg_3657" n="e" s="T628">kü͡örejenen </ts>
               <ts e="T630" id="Seg_3659" n="e" s="T629">üspü͡öjdeːbekke </ts>
               <ts e="T631" id="Seg_3661" n="e" s="T630">kuturuguttan </ts>
               <ts e="T632" id="Seg_3663" n="e" s="T631">kaptarbɨt. </ts>
               <ts e="T633" id="Seg_3665" n="e" s="T632">Innʼe </ts>
               <ts e="T634" id="Seg_3667" n="e" s="T633">gɨnan </ts>
               <ts e="T635" id="Seg_3669" n="e" s="T634">munu </ts>
               <ts e="T636" id="Seg_3671" n="e" s="T635">haŋastalahan </ts>
               <ts e="T637" id="Seg_3673" n="e" s="T636">haŋastahan, </ts>
               <ts e="T638" id="Seg_3675" n="e" s="T637">uː </ts>
               <ts e="T639" id="Seg_3677" n="e" s="T638">ihiger </ts>
               <ts e="T640" id="Seg_3679" n="e" s="T639">čekeniten </ts>
               <ts e="T641" id="Seg_3681" n="e" s="T640">tüheren, </ts>
               <ts e="T642" id="Seg_3683" n="e" s="T641">(uː </ts>
               <ts e="T644" id="Seg_3685" n="e" s="T642">hit-) </ts>
               <ts e="T645" id="Seg_3687" n="e" s="T644">uː </ts>
               <ts e="T646" id="Seg_3689" n="e" s="T645">ihiger </ts>
               <ts e="T647" id="Seg_3691" n="e" s="T646">tüheren, </ts>
               <ts e="T648" id="Seg_3693" n="e" s="T647">munuga </ts>
               <ts e="T649" id="Seg_3695" n="e" s="T648">bulkujan– </ts>
               <ts e="T650" id="Seg_3697" n="e" s="T649">hiŋeleːk </ts>
               <ts e="T651" id="Seg_3699" n="e" s="T650">uːga </ts>
               <ts e="T652" id="Seg_3701" n="e" s="T651">bulkujan, </ts>
               <ts e="T653" id="Seg_3703" n="e" s="T652">tumnaran, </ts>
               <ts e="T654" id="Seg_3705" n="e" s="T653">mülčü </ts>
               <ts e="T655" id="Seg_3707" n="e" s="T654">tuttaran </ts>
               <ts e="T656" id="Seg_3709" n="e" s="T655">kötön </ts>
               <ts e="T657" id="Seg_3711" n="e" s="T656">kaːlbɨt. </ts>
               <ts e="T658" id="Seg_3713" n="e" s="T657">Dʼe, </ts>
               <ts e="T659" id="Seg_3715" n="e" s="T658">ol </ts>
               <ts e="T660" id="Seg_3717" n="e" s="T659">sahɨl </ts>
               <ts e="T661" id="Seg_3719" n="e" s="T660">bu͡ollagɨna </ts>
               <ts e="T662" id="Seg_3721" n="e" s="T661">kɨtɨlga </ts>
               <ts e="T663" id="Seg_3723" n="e" s="T662">taksan, </ts>
               <ts e="T664" id="Seg_3725" n="e" s="T663">kuːrduna </ts>
               <ts e="T665" id="Seg_3727" n="e" s="T664">ɨrata </ts>
               <ts e="T666" id="Seg_3729" n="e" s="T665">eni, </ts>
               <ts e="T667" id="Seg_3731" n="e" s="T666">dʼe </ts>
               <ts e="T668" id="Seg_3733" n="e" s="T667">ol </ts>
               <ts e="T669" id="Seg_3735" n="e" s="T668">kuːrdunan-kanʼaːn </ts>
               <ts e="T671" id="Seg_3737" n="e" s="T669">baraːn… </ts>
               <ts e="T672" id="Seg_3739" n="e" s="T671">Tillen, </ts>
               <ts e="T673" id="Seg_3741" n="e" s="T672">bajan-toton </ts>
               <ts e="T674" id="Seg_3743" n="e" s="T673">olorbuta </ts>
               <ts e="T675" id="Seg_3745" n="e" s="T674">duː, </ts>
               <ts e="T676" id="Seg_3747" n="e" s="T675">kajdi͡ek </ts>
               <ts e="T677" id="Seg_3749" n="e" s="T676">bu͡olbuta </ts>
               <ts e="T678" id="Seg_3751" n="e" s="T677">duː? </ts>
               <ts e="T679" id="Seg_3753" n="e" s="T678">Dʼe, </ts>
               <ts e="T680" id="Seg_3755" n="e" s="T679">oloŋkobut </ts>
               <ts e="T681" id="Seg_3757" n="e" s="T680">elete. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T9" id="Seg_3758" s="T0">UkET_2002_FoxJayBuzzard_flk.001 (001.001)</ta>
            <ta e="T18" id="Seg_3759" s="T9">UkET_2002_FoxJayBuzzard_flk.002 (001.002)</ta>
            <ta e="T34" id="Seg_3760" s="T18">UkET_2002_FoxJayBuzzard_flk.003 (001.003)</ta>
            <ta e="T45" id="Seg_3761" s="T34">UkET_2002_FoxJayBuzzard_flk.004 (001.004)</ta>
            <ta e="T55" id="Seg_3762" s="T45">UkET_2002_FoxJayBuzzard_flk.005 (001.005)</ta>
            <ta e="T62" id="Seg_3763" s="T55">UkET_2002_FoxJayBuzzard_flk.006 (001.006)</ta>
            <ta e="T84" id="Seg_3764" s="T62">UkET_2002_FoxJayBuzzard_flk.007 (001.007)</ta>
            <ta e="T98" id="Seg_3765" s="T84">UkET_2002_FoxJayBuzzard_flk.008 (001.008)</ta>
            <ta e="T103" id="Seg_3766" s="T98">UkET_2002_FoxJayBuzzard_flk.009 (001.009)</ta>
            <ta e="T115" id="Seg_3767" s="T103">UkET_2002_FoxJayBuzzard_flk.010 (001.010)</ta>
            <ta e="T126" id="Seg_3768" s="T115">UkET_2002_FoxJayBuzzard_flk.011 (001.011)</ta>
            <ta e="T127" id="Seg_3769" s="T126">UkET_2002_FoxJayBuzzard_flk.012 (001.012)</ta>
            <ta e="T138" id="Seg_3770" s="T127">UkET_2002_FoxJayBuzzard_flk.013 (001.013)</ta>
            <ta e="T163" id="Seg_3771" s="T138">UkET_2002_FoxJayBuzzard_flk.014 (001.014)</ta>
            <ta e="T182" id="Seg_3772" s="T163">UkET_2002_FoxJayBuzzard_flk.015 (001.015)</ta>
            <ta e="T200" id="Seg_3773" s="T182">UkET_2002_FoxJayBuzzard_flk.016 (001.016)</ta>
            <ta e="T210" id="Seg_3774" s="T200">UkET_2002_FoxJayBuzzard_flk.017 (001.017)</ta>
            <ta e="T217" id="Seg_3775" s="T210">UkET_2002_FoxJayBuzzard_flk.018 (001.018)</ta>
            <ta e="T229" id="Seg_3776" s="T217">UkET_2002_FoxJayBuzzard_flk.019 (001.019)</ta>
            <ta e="T234" id="Seg_3777" s="T229">UkET_2002_FoxJayBuzzard_flk.020 (001.020)</ta>
            <ta e="T238" id="Seg_3778" s="T234">UkET_2002_FoxJayBuzzard_flk.021 (001.021)</ta>
            <ta e="T242" id="Seg_3779" s="T238">UkET_2002_FoxJayBuzzard_flk.022 (001.022)</ta>
            <ta e="T251" id="Seg_3780" s="T242">UkET_2002_FoxJayBuzzard_flk.023 (001.023)</ta>
            <ta e="T257" id="Seg_3781" s="T251">UkET_2002_FoxJayBuzzard_flk.024 (001.024)</ta>
            <ta e="T264" id="Seg_3782" s="T257">UkET_2002_FoxJayBuzzard_flk.025 (001.025)</ta>
            <ta e="T282" id="Seg_3783" s="T264">UkET_2002_FoxJayBuzzard_flk.026 (001.026)</ta>
            <ta e="T289" id="Seg_3784" s="T282">UkET_2002_FoxJayBuzzard_flk.027 (001.027)</ta>
            <ta e="T292" id="Seg_3785" s="T289">UkET_2002_FoxJayBuzzard_flk.028 (001.029)</ta>
            <ta e="T294" id="Seg_3786" s="T292">UkET_2002_FoxJayBuzzard_flk.029 (001.030)</ta>
            <ta e="T303" id="Seg_3787" s="T294">UkET_2002_FoxJayBuzzard_flk.030 (001.031)</ta>
            <ta e="T308" id="Seg_3788" s="T303">UkET_2002_FoxJayBuzzard_flk.031 (001.032)</ta>
            <ta e="T317" id="Seg_3789" s="T308">UkET_2002_FoxJayBuzzard_flk.032 (001.033)</ta>
            <ta e="T320" id="Seg_3790" s="T317">UkET_2002_FoxJayBuzzard_flk.033 (001.034)</ta>
            <ta e="T325" id="Seg_3791" s="T320">UkET_2002_FoxJayBuzzard_flk.034 (001.035)</ta>
            <ta e="T333" id="Seg_3792" s="T325">UkET_2002_FoxJayBuzzard_flk.035 (001.036)</ta>
            <ta e="T344" id="Seg_3793" s="T333">UkET_2002_FoxJayBuzzard_flk.036 (001.037)</ta>
            <ta e="T346" id="Seg_3794" s="T344">UkET_2002_FoxJayBuzzard_flk.037 (001.038)</ta>
            <ta e="T353" id="Seg_3795" s="T346">UkET_2002_FoxJayBuzzard_flk.038 (001.039)</ta>
            <ta e="T359" id="Seg_3796" s="T353">UkET_2002_FoxJayBuzzard_flk.039 (001.041)</ta>
            <ta e="T372" id="Seg_3797" s="T359">UkET_2002_FoxJayBuzzard_flk.040 (001.042)</ta>
            <ta e="T381" id="Seg_3798" s="T372">UkET_2002_FoxJayBuzzard_flk.041 (001.043)</ta>
            <ta e="T395" id="Seg_3799" s="T381">UkET_2002_FoxJayBuzzard_flk.042 (001.044)</ta>
            <ta e="T406" id="Seg_3800" s="T395">UkET_2002_FoxJayBuzzard_flk.043 (001.045)</ta>
            <ta e="T423" id="Seg_3801" s="T406">UkET_2002_FoxJayBuzzard_flk.044 (001.046)</ta>
            <ta e="T437" id="Seg_3802" s="T423">UkET_2002_FoxJayBuzzard_flk.045 (001.047)</ta>
            <ta e="T450" id="Seg_3803" s="T437">UkET_2002_FoxJayBuzzard_flk.046 (001.048)</ta>
            <ta e="T467" id="Seg_3804" s="T450">UkET_2002_FoxJayBuzzard_flk.047 (001.050)</ta>
            <ta e="T477" id="Seg_3805" s="T467">UkET_2002_FoxJayBuzzard_flk.048 (001.052)</ta>
            <ta e="T491" id="Seg_3806" s="T477">UkET_2002_FoxJayBuzzard_flk.049 (001.053)</ta>
            <ta e="T507" id="Seg_3807" s="T491">UkET_2002_FoxJayBuzzard_flk.050 (001.054)</ta>
            <ta e="T513" id="Seg_3808" s="T507">UkET_2002_FoxJayBuzzard_flk.051 (001.055)</ta>
            <ta e="T519" id="Seg_3809" s="T513">UkET_2002_FoxJayBuzzard_flk.052 (001.056)</ta>
            <ta e="T536" id="Seg_3810" s="T519">UkET_2002_FoxJayBuzzard_flk.053 (001.057)</ta>
            <ta e="T544" id="Seg_3811" s="T536">UkET_2002_FoxJayBuzzard_flk.054 (001.058)</ta>
            <ta e="T556" id="Seg_3812" s="T544">UkET_2002_FoxJayBuzzard_flk.055 (001.059)</ta>
            <ta e="T567" id="Seg_3813" s="T556">UkET_2002_FoxJayBuzzard_flk.056 (001.060)</ta>
            <ta e="T577" id="Seg_3814" s="T567">UkET_2002_FoxJayBuzzard_flk.057 (001.061)</ta>
            <ta e="T590" id="Seg_3815" s="T577">UkET_2002_FoxJayBuzzard_flk.058 (001.062)</ta>
            <ta e="T596" id="Seg_3816" s="T590">UkET_2002_FoxJayBuzzard_flk.059 (001.063)</ta>
            <ta e="T598" id="Seg_3817" s="T596">UkET_2002_FoxJayBuzzard_flk.060 (001.064)</ta>
            <ta e="T602" id="Seg_3818" s="T598">UkET_2002_FoxJayBuzzard_flk.061 (001.065)</ta>
            <ta e="T609" id="Seg_3819" s="T602">UkET_2002_FoxJayBuzzard_flk.062 (001.066)</ta>
            <ta e="T621" id="Seg_3820" s="T609">UkET_2002_FoxJayBuzzard_flk.063 (001.067)</ta>
            <ta e="T632" id="Seg_3821" s="T621">UkET_2002_FoxJayBuzzard_flk.064 (001.068)</ta>
            <ta e="T657" id="Seg_3822" s="T632">UkET_2002_FoxJayBuzzard_flk.065 (001.069)</ta>
            <ta e="T671" id="Seg_3823" s="T657">UkET_2002_FoxJayBuzzard_flk.066 (001.070)</ta>
            <ta e="T678" id="Seg_3824" s="T671">UkET_2002_FoxJayBuzzard_flk.067 (001.071)</ta>
            <ta e="T681" id="Seg_3825" s="T678">UkET_2002_FoxJayBuzzard_flk.068 (001.072)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T9" id="Seg_3826" s="T0">Бу урут, күччүгүй эрдэппититтэн биэктэн олоӈколоһон үөскээбиппит дуо ол? </ta>
            <ta e="T18" id="Seg_3827" s="T9">Бу биһиги күччүгүй эрдэкпиттэн туок да оонньуу һуок этэ.</ta>
            <ta e="T34" id="Seg_3828" s="T18">Ба оголору утутумаарылар, каньаамаарылар ба тээтэбитээк, тээтэбит тээтэтинаак – огого олоӈколуур олоӈколоро этэ, анаан ого олоӈкото диэн.</ta>
            <ta e="T45" id="Seg_3829" s="T34">Былыр һир таӈара үөскүүрүн һагына, барыта тыллаак этилэрэ үһү – бултар барылара.</ta>
            <ta e="T55" id="Seg_3830" s="T45">Ол бултара э-э өстөрө, ол бултар һылдьыбыт һырыыларын – ону кэпсииллэр.</ta>
            <ta e="T62" id="Seg_3831" s="T55">Оччого буоллагына һаһыл олорбут үһү былыр-былыр үйэгэ.</ta>
            <ta e="T84" id="Seg_3832" s="T62">Бу саһыл буоллагына, һаас бэриэмэтэ кэлэн буоллагына, төрүт кантан даа ас ылан һиир һирэ һуок, күһүӈӈү һака аһа бараммыт буолла, һака астаммыта.</ta>
            <ta e="T98" id="Seg_3833" s="T84">Уу каар буолбут, инньэ гынан аһа һуок буолан коргуйан, һир таӈара көтөн испитэ саһыл.</ta>
            <ta e="T103" id="Seg_3834" s="T98">Бу көтөн иһэн буоллагына… </ta>
            <ta e="T115" id="Seg_3835" s="T103">"О,о, бу кантан булуомуй", – диэн, мас иһигэр киирэн, мас иһигэр көтөн иһэр.</ta>
            <ta e="T126" id="Seg_3836" s="T115">Кайа-а, көрөр, кантаӈныы-кантаӈныы көппүтэ: орук (?) тиит төбөтүгэр һар төрөөн баран һытар (?).</ta>
            <ta e="T127" id="Seg_3837" s="T126">Кайа-а?!</ta>
            <ta e="T138" id="Seg_3838" s="T127">"До (? Догоо), онтон мин кайдак гынаммын, албыннанаммын һиэбий ити һартан тугу, һымыытта?"</ta>
            <ta e="T163" id="Seg_3839" s="T138">– диэн бу киһи һарга ба.. һартан һымыт һиэри көтөн кэлэн, һаамай кимин аннытыгар кэлбит тиитин аннытыгар кэлбит, инньэ гынан бараан кутуругунан тиитин кырбыы турбута.</ta>
            <ta e="T182" id="Seg_3840" s="T163">"Сар, сар, тэһэгэс ..киһи, эгэл биир огогун, мин кутуругум ньуучча болотунаагар һытыы, тииттиин баһагастыын быһассыан (быһа оксон?) түһэриэм, бэйэлиигин сиэм." </ta>
            <ta e="T200" id="Seg_3841" s="T182">Һар быстыа дуо, ытыы-ытыы биир оготун чэкэрис гыннарбыт, биир һымыытын һиэбит, инньэ гынан бараан һир таӈара көтөн каалбыт.</ta>
            <ta e="T210" id="Seg_3842" s="T200">Һарсыӈӈытын турар эмиэ, һары албыннаа, һарын албыннаары эмиэ көтөн кэлэр: </ta>
            <ta e="T217" id="Seg_3843" s="T210">"Сар, сар, тэһэгэс киһи, эгэл биир огогун.</ta>
            <ta e="T229" id="Seg_3844" s="T217">Мин кутуругун ньуучча болотунаагар һытыы, тииттииӈӈин-багастыыӈӈын быһа оксон түһэрэӈӈин, мин аны бэйэлииӈӈин сиэм".</ta>
            <ta e="T234" id="Seg_3845" s="T229">Эмиэ биир оготун үүтэн кээспит.</ta>
            <ta e="T238" id="Seg_3846" s="T234">Икки оготун һиэбит – һаһыл.</ta>
            <ta e="T242" id="Seg_3847" s="T238">Һар ытыы олорооктообут диин.</ta>
            <ta e="T251" id="Seg_3848" s="T242">"До(догоо?), бу, аны буоллагына бэйэлииммин сиэни диэн", – ытыы олорбут. </ta>
            <ta e="T257" id="Seg_3849" s="T251">Кайа, бу олордогуна, кукаакии көтөн кэлбит.</ta>
            <ta e="T264" id="Seg_3850" s="T257">"Кайа-а, һар, туоктан ытыыгын киэ эн бу? </ta>
            <ta e="T282" id="Seg_3851" s="T264">Муннук боскуой күннээк күн тыӈ турдагына, оголоруӈ да ичигэскэ һыталлар, бэйэӈ да аһы багас эгэлэн кэлэр көрдүк буо. </ta>
            <ta e="T289" id="Seg_3852" s="T282">Туоктан ытыыгын дьэ, муннук бэсэлээгэ олороӈӈун?" – диир.</ta>
            <ta e="T292" id="Seg_3853" s="T289">"Эн кайдак ытаппаккын?</ta>
            <ta e="T294" id="Seg_3854" s="T292">Көрбөккүн дуо? </ta>
            <ta e="T303" id="Seg_3855" s="T294">Минигин буоллагына һаһыл буолла икки күӈӈэ икки огобун һиэтэ.</ta>
            <ta e="T308" id="Seg_3856" s="T303">Аны бүгүн эмиэ кэлиэгэ, һиэгэ.</ta>
            <ta e="T317" id="Seg_3857" s="T308">Тииттииммин, багастыыммын быһа оксон түһэрэн бэйэлииммин сиэри гынна" – диир.</ta>
            <ta e="T320" id="Seg_3858" s="T317">"Туоктаагый ол саһылыӈ?"</ta>
            <ta e="T325" id="Seg_3859" s="T320">"Кайа кутуруга ньуучча болотуннаагар һытыы".</ta>
            <ta e="T333" id="Seg_3860" s="T325">"Доо, туок ааттага аӈыр көтөрө төрөөбүтүӈүй эн муннук?</ta>
            <ta e="T344" id="Seg_3861" s="T333">Дьэ, (һар кайдак гына) һаһыл кайдак гынан эн тииткин быһа оксуой?</ta>
            <ta e="T346" id="Seg_3862" s="T344">Кутуруга түү.</ta>
            <ta e="T353" id="Seg_3863" s="T346">Түү кутурук, тугу котуоӈуй? – диэ аны кэллэгинэ. </ta>
            <ta e="T359" id="Seg_3864" s="T353">Ким үөрэппитэй, диэтэгинэ, кукаакии үөрэппитэ, диэр".</ta>
            <ta e="T372" id="Seg_3865" s="T359">Дьэ, һар, һар дьэ һиэсэ һэк буолбут, олорбут, кукаакиита көтөн каалбыт.</ta>
            <ta e="T381" id="Seg_3866" s="T372">О,о, дьэ саһыл кыйӈанаары кыйӈаммыт буолбут, кыыһыраары кыыһырбыт буолбут: </ta>
            <ta e="T395" id="Seg_3867" s="T381">Эӈин-эӈин көппөгү турута тэбиэлии-тэбиэлии илиӈ калина атагынан кайыта тэбиэлии-тэбиэлии кэлээктээбит, диин һарыга, һары куттаары.</ta>
            <ta e="T406" id="Seg_3868" s="T395">"Сар,сар, оннук-маннык иньэлээк, кайа, мин буоллагына эмиэ буопса кыйӈанаары кыйӈанным.</ta>
            <ta e="T423" id="Seg_3869" s="T406">Эгэл биир огото, мин кутуругум ньуучча болотунаагар һытыы, һаны бэйэлииӈӈин туу.. туура оксоммун, бэйэлииӈӈин сиэм онноогор".</ta>
            <ta e="T437" id="Seg_3870" s="T423">"Ко-о, аӈырдааӈӈын буопса таак-то каарыйан икки огобун һиэттим мин эниэкэ, бэйэм аӈырбар.</ta>
            <ta e="T450" id="Seg_3871" s="T437">Эн буоллагына түү кутуруккун диин, туу кутурук, тугу котуоӈуй, маһы котуоӈ дуо"? – диир.</ta>
            <ta e="T467" id="Seg_3872" s="T450">"Ко-о, араката, туок ааттага өй биэртэй эниэкэ, кайдак гынаӈӈын өйү бултугуй? – диир, – миниэнэ кутуругум түүтүн кантан билэгин"?</ta>
            <ta e="T477" id="Seg_3873" s="T467">"Кайа минигин кукаакии үөрэппитэ, ким биэриэй миниэкэ өй, кукаакии үөрэппитэ".</ta>
            <ta e="T491" id="Seg_3874" s="T477">"Кайа-а, оннук-маннык отто кукаакины, кайа һиртэн булуобуй, дьэ, бээбэ, дьэ уолдьаһыам мин гиниэкэ даагыны".</ta>
            <ta e="T507" id="Seg_3875" s="T491">Инньэ гыммыт да, һир таӈара көтөн каалбыт, һир таӈара көтөн иһэн буолла,.. төп төгүрүк күөлгэ тийэн… </ta>
            <ta e="T513" id="Seg_3876" s="T507">Бу күөл туоктан даа улакан кайалаак.</ta>
            <ta e="T519" id="Seg_3877" s="T513">Һаас кэмигэр буолла кэриискэтэ барыта таксыбыт.</ta>
            <ta e="T536" id="Seg_3878" s="T519">Бу кэриискэни буолла көтө һылдьан, буопса муӈ боскуой карадьыкка буоллагына, өлбүт саһыл буолан баран, нэлэс гына түспүт.</ta>
            <ta e="T544" id="Seg_3879" s="T536">Буопса үөӈӈэ барытыгар ыстара һыппыт ол гынан бараан.</ta>
            <ta e="T556" id="Seg_3880" s="T544">Кайа-а, кукаакилар дааганы көтө һылдьан эрэн көрөр – саһыл өлөн бараан нэлэйэн һытар.</ta>
            <ta e="T567" id="Seg_3881" s="T556">"О-о, баа туркары ба коргуйан буоллагына һар оголорун сии һылдьыбыта буолла.</ta>
            <ta e="T577" id="Seg_3882" s="T567">Аны сара буоллагына минигиттэн өй ылар, биир да оготун биэрбэтэк.</ta>
            <ta e="T590" id="Seg_3883" s="T577">Оччого кантан ылан гини дааганы коргуйбут киһи, кантан ылан аһыай, дьэ, октон өлбүт.</ta>
            <ta e="T596" id="Seg_3884" s="T590">Дьэ, эн дааганы өлөрдөөккүн эбит", диэн.</ta>
            <ta e="T598" id="Seg_3885" s="T596">Кайа кэлбит.</ta>
            <ta e="T602" id="Seg_3886" s="T598">Лоӈ-лоӈ, лоӈ-лоӈ – лоӈсуйбут буо.</ta>
            <ta e="T609" id="Seg_3887" s="T602">Кантыттан, карагыттан һиэккэ, диэн, карыга лоӈсуна һырыттагына. </ta>
            <ta e="T621" id="Seg_3888" s="T609">Буопса … саһыл буоллагына элэ ньыматынан, элэ күтүрүнэн ырдьыгыныы гына ыстанаактаабыт диин.</ta>
            <ta e="T632" id="Seg_3889" s="T621">Баагы сар буоллагына, ким буоллагына, кукаакии буоллагына күөрэйэнин үспүөйдээбэккэ кутуругуттан каптарбыт. </ta>
            <ta e="T657" id="Seg_3890" s="T632">Инньэ гынан муну һаӈасталаһан һаӈастаһан, уу иһигэр чэӈинитэн түһэрэн,(уу һит) уу иһигэр түһэрэн, мунуга булкуйан – һиӈэлээк ууга түһэрэн, тумнаран, мүлчү туттаран көтөн каалбыт.</ta>
            <ta e="T671" id="Seg_3891" s="T657">Дьэ, ол саһыл буоллагына кытылга таксан, куурдунна ырата эни, дьэ ол куурдунан-каньаан бараан… </ta>
            <ta e="T678" id="Seg_3892" s="T671">Тиллэн, байан-тотон олорбута дуу, кайдиэк буолбута дуу?</ta>
            <ta e="T681" id="Seg_3893" s="T678">Дьэ, олоӈкобут элэтэ.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T9" id="Seg_3894" s="T0">Bu urut, kuččuguj erdeppititten bi͡ekten oloŋkolohon ü͡öskeːbippit du͡o ol? </ta>
            <ta e="T18" id="Seg_3895" s="T9">Bu bihigi küččügüj erdekpitten tu͡ok da oːnnʼuː hu͡ok ete. </ta>
            <ta e="T34" id="Seg_3896" s="T18">Ba ogoloru ututumaːrɨlar, kanʼaːmaːrɨlar ba teːtebitteːk, teːtebit teːtetinaːk – ogogo oloŋkoluːr oloŋkoloro ete, anaːn ogo oloŋkoto di͡en. </ta>
            <ta e="T45" id="Seg_3897" s="T34">Bɨlɨr hir taŋara ü͡ösküːrün hagɨna, barɨta tɨllaːk etilere ühü – bultar barɨlara. </ta>
            <ta e="T55" id="Seg_3898" s="T45">Ol bultara eː östörö, ol bultar hɨldʼɨbɨt hɨrɨːlarɨn – onu kepsiːller. </ta>
            <ta e="T62" id="Seg_3899" s="T55">Oččogo bu͡ollagɨna hahɨl olorbut ühü bɨlɨr-bɨlɨr üjege. </ta>
            <ta e="T84" id="Seg_3900" s="T62">Bu sahɨl bu͡ollagɨna, haːs beri͡emete kelen bu͡ollagɨna, törüt kantan daː as ɨlan hiːr hire hu͡ok, kühüŋŋü haka aha barammɨt bu͡olla, haka astammɨta. </ta>
            <ta e="T98" id="Seg_3901" s="T84">Uː kaːr bu͡olbut, innʼe gɨnan aha hu͡ok bu͡olan korgujan, hir taŋara kötön ispite sahɨl. </ta>
            <ta e="T103" id="Seg_3902" s="T98">Bu kötön ihen bu͡ollagɨna… </ta>
            <ta e="T115" id="Seg_3903" s="T103">"Oː, bu kantan bulu͡omuj", di͡en, mas ihiger kiːren, mas ihiger kötön iher. </ta>
            <ta e="T126" id="Seg_3904" s="T115">Kajaː, körör, kantaŋnɨː-kantaŋnɨː köppüte, oruk tiːt töbötüger har töröːn baran hɨtar. </ta>
            <ta e="T127" id="Seg_3905" s="T126">Kajaː! </ta>
            <ta e="T138" id="Seg_3906" s="T127">"Do, onton min kajdak gɨnammɨn, albɨnnanammɨn hi͡ebij iti hartan tugu, hɨmɨːtta?", </ta>
            <ta e="T163" id="Seg_3907" s="T138">di͡en bu kihi harga (ba-) hartan hɨmɨːt hi͡eri kötön kelen, ((NOISE)) haːmaj kimin annɨtɨgar kelbit tiːtin annɨtɨgar kelbit, innʼe gɨnan baraːn kuturugunan tiːtin kɨrbɨː turbuta ((NOISE)). </ta>
            <ta e="T182" id="Seg_3908" s="T163">"Sar, sar, teheges kihi, egel biːr ogogun, min kuturugum nuːčča bolotunaːgar hɨtɨː, tiːttiːn bagastɨːn bɨha okson tüheri͡em, bejeliːŋŋin si͡em." </ta>
            <ta e="T200" id="Seg_3909" s="T182">Har bɨstɨ͡a du͡o, ɨtɨː-ɨtɨː biːr ogotun čekeris gɨnnarbɨt, biːr hɨmɨːtɨn hi͡ebit, innʼe gɨnan baraːn hir taŋara kötön kaːlbɨt ((NOISE)). </ta>
            <ta e="T210" id="Seg_3910" s="T200">Harsɨŋŋɨtɨn turar emi͡e, harɨ (albɨnnaː-), harɨn albɨnnaːrɨ emi͡e kötön keler: </ta>
            <ta e="T217" id="Seg_3911" s="T210">"Sar, sar, teheges kihi, egel biːr ogogun. </ta>
            <ta e="T229" id="Seg_3912" s="T217">Min kuturugum nuːčča bolotunaːgar hɨtɨː, tiːttiːŋŋin-bagastɨːŋŋɨn bɨha okson tüheremmin, anɨ bejeliːŋŋin si͡em". </ta>
            <ta e="T234" id="Seg_3913" s="T229">Emi͡e biːr ogotun üːten keːspit ((NOISE)). </ta>
            <ta e="T238" id="Seg_3914" s="T234">Ikki ogotun hi͡ebit – hahɨl. </ta>
            <ta e="T242" id="Seg_3915" s="T238">Har ɨtɨː oloroːktoːbut diːn. </ta>
            <ta e="T251" id="Seg_3916" s="T242">"Doː, bu, anɨ bu͡ollagɨna bejeliːmmin si͡eni di͡en", ɨtɨː olorbut. </ta>
            <ta e="T257" id="Seg_3917" s="T251">Kaja, bu olordoguna, kukaːkiː kötön kelbit ((NOISE)). </ta>
            <ta e="T264" id="Seg_3918" s="T257">"Kajaː, har, tu͡oktan ɨtɨːgɨn ki͡e en bu? </ta>
            <ta e="T282" id="Seg_3919" s="T264">Munnuk bosku͡oj künneːk kün tɨŋa turdagɨna, ogoloruŋ da ičigeske hɨtallar, bejeŋ da ahɨ bagas egelen keler kördük bu͡o. </ta>
            <ta e="T289" id="Seg_3920" s="T282">Tu͡oktan ɨtɨːgɨn dʼe, munnuk beseleːge oloroŋŋun", diːr. </ta>
            <ta e="T292" id="Seg_3921" s="T289">"En kajdak ɨtappakkɨn? </ta>
            <ta e="T294" id="Seg_3922" s="T292">Körbökkün du͡o? </ta>
            <ta e="T303" id="Seg_3923" s="T294">Minigin bu͡ollagɨna hahɨl bu͡olla ikki küŋŋe ikki ogobun hi͡ete. </ta>
            <ta e="T308" id="Seg_3924" s="T303">Anɨ bügün emi͡e keli͡ege, hi͡ege. </ta>
            <ta e="T317" id="Seg_3925" s="T308">Tiːttiːmmin, bagastɨːmmɨn bɨha okson tüheren bejeliːmmin si͡eri gɨnna", diːr ((NOISE)). </ta>
            <ta e="T320" id="Seg_3926" s="T317">"Tu͡oktaːgɨj ol sahɨlɨŋ?" </ta>
            <ta e="T325" id="Seg_3927" s="T320">"Kaja kuturuga nuːčča bolotunaːgar hɨtɨː". </ta>
            <ta e="T333" id="Seg_3928" s="T325">"Doː, tu͡ok aːttaːga aŋɨr kötörö töröːbütüŋüj en munnuk? </ta>
            <ta e="T344" id="Seg_3929" s="T333">Dʼeː, (har kajdak gɨna) hahɨl kajdak gɨnan en tiːtkin bɨha oksu͡oj? </ta>
            <ta e="T346" id="Seg_3930" s="T344">Kuturuga tüː. </ta>
            <ta e="T353" id="Seg_3931" s="T346">Tüː kuturuk, tugu kotu͡oŋuj", di͡e anɨ kellegine ((NOISE)). </ta>
            <ta e="T359" id="Seg_3932" s="T353">Kim ü͡öreppitej, di͡etegine, kukaːkiː ü͡öreppite, diːr". </ta>
            <ta e="T372" id="Seg_3933" s="T359">Dʼe, har (ha-) har dʼe hi͡ese hek bu͡olbut, olorbut, kukaːkiːta kötön kaːlbɨt. </ta>
            <ta e="T381" id="Seg_3934" s="T372">Oː, dʼe sahɨl kɨjŋanaːrɨ kɨjŋammɨt bu͡olbut, kɨːhɨraːrɨ kɨːhɨrbɨt bu͡olbut: </ta>
            <ta e="T395" id="Seg_3935" s="T381">Eŋin-eŋin köppögü turuta tebi͡eliː-tebi͡eliː ilin kalin atagɨnan kajɨta tebi͡eliː-tebi͡eliː keleːkteːbit, diːn ((NOISE)) (harɨga) harɨ kuttaːrɨ. </ta>
            <ta e="T406" id="Seg_3936" s="T395">((NOISE)) "Sar, sar, onnuk-mannɨk inʼeleːk, kaja, min bu͡ollagɨna emi͡e bu͡opsa kɨjŋanaːrɨ kɨjŋannɨm. </ta>
            <ta e="T423" id="Seg_3937" s="T406">Egel biːr ogoto, min kuturugum nʼuːčča bolotunaːgar hɨtɨː, hanɨ bejeliːŋŋin (tu-) tuːra oksommun, bejeliːŋŋin si͡em onnoːgor" ((NOISE)). </ta>
            <ta e="T437" id="Seg_3938" s="T423">"Koː, aŋɨrdaːmmɨn bu͡opsa taːk-ta kaːrɨjan ikki ogobun hi͡ettim min eni͡eke, bejem aŋɨrbar. </ta>
            <ta e="T450" id="Seg_3939" s="T437">En bu͡ollagɨna tüː kuturukkun diːn, tüː kuturuk, tugu kotu͡oŋuj, mahɨ kotu͡oŋ du͡o", diːr ((NOISE)). </ta>
            <ta e="T467" id="Seg_3940" s="T450">"Koː, arakata, tu͡ok aːttaːga öj bi͡ertej eni͡eke, kajdak gɨnaŋŋɨn öjü bultuŋuj", diːr, "mini͡ene kuturugum tüːtün kantan bilegin?" </ta>
            <ta e="T477" id="Seg_3941" s="T467">"Kaja minigin kukaːkiː ü͡öreppite, kim bi͡eri͡ej mini͡eke öj, kukaːkiː ü͡öreppite." </ta>
            <ta e="T491" id="Seg_3942" s="T477">"Kajaː, onnuk-mannɨk otto kukaːkinɨ, kaja hirten bulu͡obuj, dʼe, beːbe, dʼe u͡oldʼahɨ͡am min gini͡eke daːgɨnɨ" ((NOISE)). </ta>
            <ta e="T507" id="Seg_3943" s="T491">Innʼe gɨmmɨt da, hir taŋara kötön kaːlbɨt, hir taŋara kötön ihen bu͡olla, töp tögürük kü͡ölge tijen… </ta>
            <ta e="T513" id="Seg_3944" s="T507">Bu kü͡öl tu͡oktan daː ulakan kajalaːk. </ta>
            <ta e="T519" id="Seg_3945" s="T513">Haːs kemiger bu͡olla keriːskete barɨta taksɨbɨt. </ta>
            <ta e="T536" id="Seg_3946" s="T519">Bu keriːskeni bu͡ollagɨna kötö hɨldʼan, bu͡opsa muŋ bu͡osku͡oj karadʼɨkka bu͡ollagɨna, ölbüt sahɨl bu͡olan baran, neles gɨna tüspüt. </ta>
            <ta e="T544" id="Seg_3947" s="T536">Bu͡opsa ü͡öŋŋe barɨtɨgar ɨstara hɨppɨt ol gɨnan baraːn. </ta>
            <ta e="T556" id="Seg_3948" s="T544">Kajaː, kukaːkiːlar daːganɨ kötö hɨldʼan eren körör – sahɨl ölön baraːn nelejen hɨtar. </ta>
            <ta e="T567" id="Seg_3949" s="T556">"Oː, baː turkarɨ ba korgujan bu͡ollagɨna har ogolorun siː hɨldʼɨbɨta bu͡olla. </ta>
            <ta e="T577" id="Seg_3950" s="T567">Anɨ sara bu͡ollagɨna minigitten öj ɨlar, biːr da ogotun bi͡erbetek. </ta>
            <ta e="T590" id="Seg_3951" s="T577">Oččogo kantan ɨlan gini daːganɨ korgujbut kihi, kantan ɨlan ahɨ͡aj, dʼe, okton ölbüt ((NOISE)). </ta>
            <ta e="T596" id="Seg_3952" s="T590">Dʼe, en daːganɨ ölördöːkkün ebit" di͡en ((NOISE)). </ta>
            <ta e="T598" id="Seg_3953" s="T596">Kaja kelbit. </ta>
            <ta e="T602" id="Seg_3954" s="T598">Loŋ-loŋ, loŋ-loŋ– loŋsujbut bu͡o. </ta>
            <ta e="T609" id="Seg_3955" s="T602">Kantɨttan, karagɨttan hi͡ekke, di͡en, karɨga loŋsuna hɨrɨttagɨna ((NOISE)). </ta>
            <ta e="T621" id="Seg_3956" s="T609">Bu͡opsa ((…)) sahɨl bu͡ollagɨna ele nʼɨmatɨnan, ele kütürünen ɨrdʼɨgɨnɨː gɨna ɨstanaːktaːbɨt diːn ((NOISE)). </ta>
            <ta e="T632" id="Seg_3957" s="T621">Baːgɨ sar bu͡ollagɨna, kim bu͡ollagɨna, kukaːkiː bu͡ollagɨna kü͡örejenen üspü͡öjdeːbekke kuturuguttan kaptarbɨt ((NOISE)). </ta>
            <ta e="T657" id="Seg_3958" s="T632">Innʼe gɨnan munu haŋastalahan haŋastahan, uː ihiger čekeniten tüheren, (uː hit-) uː ihiger tüheren, munuga bulkujan– hiŋeleːk uːga bulkujan, tumnaran, mülčü tuttaran kötön kaːlbɨt ((NOISE)). </ta>
            <ta e="T671" id="Seg_3959" s="T657">Dʼe, ol sahɨl bu͡ollagɨna kɨtɨlga taksan, kuːrduna ɨrata eni, dʼe ol kuːrdunan-kanʼaːn baraːn… </ta>
            <ta e="T678" id="Seg_3960" s="T671">Tillen, bajan-toton olorbuta duː, kajdi͡ek bu͡olbuta duː? </ta>
            <ta e="T681" id="Seg_3961" s="T678">Dʼe, oloŋkobut elete.</ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_3962" s="T0">bu</ta>
            <ta e="T2" id="Seg_3963" s="T1">urut</ta>
            <ta e="T3" id="Seg_3964" s="T2">kuččuguj</ta>
            <ta e="T4" id="Seg_3965" s="T3">er-dep-piti-tten</ta>
            <ta e="T5" id="Seg_3966" s="T4">bi͡ek-ten</ta>
            <ta e="T6" id="Seg_3967" s="T5">oloŋko-l-o-h-on</ta>
            <ta e="T7" id="Seg_3968" s="T6">ü͡öskeː-bip-pit</ta>
            <ta e="T8" id="Seg_3969" s="T7">du͡o</ta>
            <ta e="T9" id="Seg_3970" s="T8">ol</ta>
            <ta e="T10" id="Seg_3971" s="T9">bu</ta>
            <ta e="T11" id="Seg_3972" s="T10">bihigi</ta>
            <ta e="T12" id="Seg_3973" s="T11">küččügüj</ta>
            <ta e="T13" id="Seg_3974" s="T12">er-dek-pi-tten</ta>
            <ta e="T14" id="Seg_3975" s="T13">tu͡ok</ta>
            <ta e="T15" id="Seg_3976" s="T14">da</ta>
            <ta e="T16" id="Seg_3977" s="T15">oːnnʼ-uː</ta>
            <ta e="T17" id="Seg_3978" s="T16">hu͡ok</ta>
            <ta e="T18" id="Seg_3979" s="T17">e-t-e</ta>
            <ta e="T19" id="Seg_3980" s="T18">ba</ta>
            <ta e="T20" id="Seg_3981" s="T19">ogo-lor-u</ta>
            <ta e="T21" id="Seg_3982" s="T20">utu-t-u-m-aːrɨ-lar</ta>
            <ta e="T22" id="Seg_3983" s="T21">kanʼaː-m-aːrɨ-lar</ta>
            <ta e="T23" id="Seg_3984" s="T22">ba</ta>
            <ta e="T24" id="Seg_3985" s="T23">teːte-bit-teːk</ta>
            <ta e="T25" id="Seg_3986" s="T24">teːte-bit</ta>
            <ta e="T26" id="Seg_3987" s="T25">teːte-ti-naːk</ta>
            <ta e="T27" id="Seg_3988" s="T26">ogo-go</ta>
            <ta e="T28" id="Seg_3989" s="T27">oloŋko-luː-r</ta>
            <ta e="T29" id="Seg_3990" s="T28">oloŋko-loro</ta>
            <ta e="T30" id="Seg_3991" s="T29">e-t-e</ta>
            <ta e="T31" id="Seg_3992" s="T30">anaːn</ta>
            <ta e="T32" id="Seg_3993" s="T31">ogo</ta>
            <ta e="T33" id="Seg_3994" s="T32">oloŋko-to</ta>
            <ta e="T34" id="Seg_3995" s="T33">di͡e-n</ta>
            <ta e="T35" id="Seg_3996" s="T34">bɨlɨr</ta>
            <ta e="T36" id="Seg_3997" s="T35">hir</ta>
            <ta e="T37" id="Seg_3998" s="T36">taŋara</ta>
            <ta e="T38" id="Seg_3999" s="T37">ü͡ösküː-r-ü-n</ta>
            <ta e="T39" id="Seg_4000" s="T38">hagɨna</ta>
            <ta e="T40" id="Seg_4001" s="T39">barɨ-ta</ta>
            <ta e="T41" id="Seg_4002" s="T40">tɨl-laːk</ta>
            <ta e="T42" id="Seg_4003" s="T41">e-ti-lere</ta>
            <ta e="T43" id="Seg_4004" s="T42">ühü</ta>
            <ta e="T44" id="Seg_4005" s="T43">bul-tar</ta>
            <ta e="T45" id="Seg_4006" s="T44">barɨ-lara</ta>
            <ta e="T46" id="Seg_4007" s="T45">ol</ta>
            <ta e="T47" id="Seg_4008" s="T46">bul-tara</ta>
            <ta e="T48" id="Seg_4009" s="T47">eː</ta>
            <ta e="T49" id="Seg_4010" s="T48">ös-törö</ta>
            <ta e="T50" id="Seg_4011" s="T49">ol</ta>
            <ta e="T51" id="Seg_4012" s="T50">bul-tar</ta>
            <ta e="T52" id="Seg_4013" s="T51">hɨldʼ-ɨ-bɨt</ta>
            <ta e="T53" id="Seg_4014" s="T52">hɨrɨː-larɨ-n</ta>
            <ta e="T54" id="Seg_4015" s="T53">o-nu</ta>
            <ta e="T55" id="Seg_4016" s="T54">kepsiː-l-ler</ta>
            <ta e="T56" id="Seg_4017" s="T55">oččogo</ta>
            <ta e="T57" id="Seg_4018" s="T56">bu͡ollagɨna</ta>
            <ta e="T58" id="Seg_4019" s="T57">hahɨl</ta>
            <ta e="T59" id="Seg_4020" s="T58">olor-but</ta>
            <ta e="T60" id="Seg_4021" s="T59">ühü</ta>
            <ta e="T61" id="Seg_4022" s="T60">bɨlɨr-bɨlɨr</ta>
            <ta e="T62" id="Seg_4023" s="T61">üje-ge</ta>
            <ta e="T63" id="Seg_4024" s="T62">bu</ta>
            <ta e="T64" id="Seg_4025" s="T63">sahɨl</ta>
            <ta e="T65" id="Seg_4026" s="T64">bu͡ollagɨna</ta>
            <ta e="T66" id="Seg_4027" s="T65">haːs</ta>
            <ta e="T67" id="Seg_4028" s="T66">beri͡eme-te</ta>
            <ta e="T68" id="Seg_4029" s="T67">kel-en</ta>
            <ta e="T69" id="Seg_4030" s="T68">bu͡ol-lag-ɨna</ta>
            <ta e="T70" id="Seg_4031" s="T69">törüt</ta>
            <ta e="T71" id="Seg_4032" s="T70">kantan</ta>
            <ta e="T72" id="Seg_4033" s="T71">daː</ta>
            <ta e="T73" id="Seg_4034" s="T72">as</ta>
            <ta e="T74" id="Seg_4035" s="T73">ɨl-an</ta>
            <ta e="T75" id="Seg_4036" s="T74">hiː-r</ta>
            <ta e="T76" id="Seg_4037" s="T75">hir-e</ta>
            <ta e="T77" id="Seg_4038" s="T76">hu͡ok</ta>
            <ta e="T78" id="Seg_4039" s="T77">kühüŋŋü</ta>
            <ta e="T79" id="Seg_4040" s="T78">haka</ta>
            <ta e="T80" id="Seg_4041" s="T79">ah-a</ta>
            <ta e="T81" id="Seg_4042" s="T80">baram-mɨt</ta>
            <ta e="T82" id="Seg_4043" s="T81">bu͡ol-l-a</ta>
            <ta e="T83" id="Seg_4044" s="T82">haka</ta>
            <ta e="T84" id="Seg_4045" s="T83">as-tam-mɨt-a</ta>
            <ta e="T85" id="Seg_4046" s="T84">uː</ta>
            <ta e="T86" id="Seg_4047" s="T85">kaːr</ta>
            <ta e="T87" id="Seg_4048" s="T86">bu͡ol-but</ta>
            <ta e="T88" id="Seg_4049" s="T87">innʼe</ta>
            <ta e="T89" id="Seg_4050" s="T88">gɨn-an</ta>
            <ta e="T90" id="Seg_4051" s="T89">ah-a</ta>
            <ta e="T91" id="Seg_4052" s="T90">hu͡ok</ta>
            <ta e="T92" id="Seg_4053" s="T91">bu͡ol-an</ta>
            <ta e="T93" id="Seg_4054" s="T92">korguj-an</ta>
            <ta e="T94" id="Seg_4055" s="T93">hir</ta>
            <ta e="T95" id="Seg_4056" s="T94">taŋara</ta>
            <ta e="T96" id="Seg_4057" s="T95">köt-ön</ta>
            <ta e="T97" id="Seg_4058" s="T96">is-pit-e</ta>
            <ta e="T98" id="Seg_4059" s="T97">sahɨl</ta>
            <ta e="T99" id="Seg_4060" s="T98">bu</ta>
            <ta e="T100" id="Seg_4061" s="T99">köt-ön</ta>
            <ta e="T101" id="Seg_4062" s="T100">ih-en</ta>
            <ta e="T103" id="Seg_4063" s="T101">bu͡ollagɨna</ta>
            <ta e="T104" id="Seg_4064" s="T103">oː</ta>
            <ta e="T105" id="Seg_4065" s="T104">bu</ta>
            <ta e="T106" id="Seg_4066" s="T105">kantan</ta>
            <ta e="T107" id="Seg_4067" s="T106">bul-u͡o-m=uj</ta>
            <ta e="T108" id="Seg_4068" s="T107">di͡e-n</ta>
            <ta e="T109" id="Seg_4069" s="T108">mas</ta>
            <ta e="T110" id="Seg_4070" s="T109">ih-i-ger</ta>
            <ta e="T111" id="Seg_4071" s="T110">kiːr-en</ta>
            <ta e="T112" id="Seg_4072" s="T111">mas</ta>
            <ta e="T113" id="Seg_4073" s="T112">ih-i-ger</ta>
            <ta e="T114" id="Seg_4074" s="T113">köt-ön</ta>
            <ta e="T115" id="Seg_4075" s="T114">ih-er</ta>
            <ta e="T116" id="Seg_4076" s="T115">kajaː</ta>
            <ta e="T117" id="Seg_4077" s="T116">kör-ör</ta>
            <ta e="T118" id="Seg_4078" s="T117">kanta-ŋn-ɨː-kanta-ŋn-ɨː</ta>
            <ta e="T119" id="Seg_4079" s="T118">köp-püt-e</ta>
            <ta e="T120" id="Seg_4080" s="T119">oruk</ta>
            <ta e="T121" id="Seg_4081" s="T120">tiːt</ta>
            <ta e="T122" id="Seg_4082" s="T121">töbö-tü-ger</ta>
            <ta e="T123" id="Seg_4083" s="T122">har</ta>
            <ta e="T124" id="Seg_4084" s="T123">töröː-n</ta>
            <ta e="T125" id="Seg_4085" s="T124">baran</ta>
            <ta e="T126" id="Seg_4086" s="T125">hɨt-ar</ta>
            <ta e="T127" id="Seg_4087" s="T126">kajaː</ta>
            <ta e="T128" id="Seg_4088" s="T127">do</ta>
            <ta e="T129" id="Seg_4089" s="T128">onton</ta>
            <ta e="T130" id="Seg_4090" s="T129">min</ta>
            <ta e="T131" id="Seg_4091" s="T130">kajdak</ta>
            <ta e="T132" id="Seg_4092" s="T131">gɨn-am-mɨn</ta>
            <ta e="T133" id="Seg_4093" s="T132">albɨn-nan-am-mɨn</ta>
            <ta e="T134" id="Seg_4094" s="T133">h-i͡e-b=ij</ta>
            <ta e="T135" id="Seg_4095" s="T134">iti</ta>
            <ta e="T136" id="Seg_4096" s="T135">har-tan</ta>
            <ta e="T137" id="Seg_4097" s="T136">tug-u</ta>
            <ta e="T138" id="Seg_4098" s="T137">hɨmɨːt-ta</ta>
            <ta e="T139" id="Seg_4099" s="T138">di͡e-n</ta>
            <ta e="T140" id="Seg_4100" s="T139">bu</ta>
            <ta e="T141" id="Seg_4101" s="T140">kihi</ta>
            <ta e="T142" id="Seg_4102" s="T141">har-ga</ta>
            <ta e="T145" id="Seg_4103" s="T144">har-tan</ta>
            <ta e="T146" id="Seg_4104" s="T145">hɨmɨːt</ta>
            <ta e="T147" id="Seg_4105" s="T146">hi͡e-ri</ta>
            <ta e="T148" id="Seg_4106" s="T147">köt-ön</ta>
            <ta e="T149" id="Seg_4107" s="T148">kel-en</ta>
            <ta e="T150" id="Seg_4108" s="T149">haːmaj</ta>
            <ta e="T151" id="Seg_4109" s="T150">kim-i-n</ta>
            <ta e="T152" id="Seg_4110" s="T151">annɨ-tɨ-gar</ta>
            <ta e="T153" id="Seg_4111" s="T152">kel-bit</ta>
            <ta e="T154" id="Seg_4112" s="T153">tiːt-i-n</ta>
            <ta e="T155" id="Seg_4113" s="T154">annɨ-tɨ-gar</ta>
            <ta e="T156" id="Seg_4114" s="T155">kel-bit</ta>
            <ta e="T157" id="Seg_4115" s="T156">innʼe</ta>
            <ta e="T158" id="Seg_4116" s="T157">gɨn-an</ta>
            <ta e="T159" id="Seg_4117" s="T158">baraːn</ta>
            <ta e="T160" id="Seg_4118" s="T159">kuturug-u-nan</ta>
            <ta e="T161" id="Seg_4119" s="T160">tiːt-i-n</ta>
            <ta e="T162" id="Seg_4120" s="T161">kɨrb-ɨː</ta>
            <ta e="T163" id="Seg_4121" s="T162">tur-but-a</ta>
            <ta e="T164" id="Seg_4122" s="T163">sar</ta>
            <ta e="T165" id="Seg_4123" s="T164">sar</ta>
            <ta e="T166" id="Seg_4124" s="T165">teheges</ta>
            <ta e="T167" id="Seg_4125" s="T166">kihi</ta>
            <ta e="T168" id="Seg_4126" s="T167">egel</ta>
            <ta e="T169" id="Seg_4127" s="T168">biːr</ta>
            <ta e="T170" id="Seg_4128" s="T169">ogo-gu-n</ta>
            <ta e="T171" id="Seg_4129" s="T170">min</ta>
            <ta e="T172" id="Seg_4130" s="T171">kuturug-u-m</ta>
            <ta e="T173" id="Seg_4131" s="T172">nuːčča</ta>
            <ta e="T174" id="Seg_4132" s="T173">bolot-u-naːgar</ta>
            <ta e="T175" id="Seg_4133" s="T174">hɨtɨː</ta>
            <ta e="T176" id="Seg_4134" s="T175">tiːt-tiːn</ta>
            <ta e="T177" id="Seg_4135" s="T176">bagas-tɨːn</ta>
            <ta e="T178" id="Seg_4136" s="T177">bɨh-a</ta>
            <ta e="T179" id="Seg_4137" s="T178">oks-on</ta>
            <ta e="T180" id="Seg_4138" s="T179">tüh-e-r-i͡e-m</ta>
            <ta e="T181" id="Seg_4139" s="T180">beje-liːŋ-ŋi-n</ta>
            <ta e="T182" id="Seg_4140" s="T181">s-i͡e-m</ta>
            <ta e="T183" id="Seg_4141" s="T182">har</ta>
            <ta e="T184" id="Seg_4142" s="T183">bɨst-ɨ͡a</ta>
            <ta e="T185" id="Seg_4143" s="T184">du͡o</ta>
            <ta e="T186" id="Seg_4144" s="T185">ɨt-ɨː-ɨt-ɨː</ta>
            <ta e="T187" id="Seg_4145" s="T186">biːr</ta>
            <ta e="T188" id="Seg_4146" s="T187">ogo-tu-n</ta>
            <ta e="T189" id="Seg_4147" s="T188">čekeri-s</ta>
            <ta e="T190" id="Seg_4148" s="T189">gɨn-nar-bɨt</ta>
            <ta e="T191" id="Seg_4149" s="T190">biːr</ta>
            <ta e="T192" id="Seg_4150" s="T191">hɨmɨːt-ɨ-n</ta>
            <ta e="T193" id="Seg_4151" s="T192">hi͡e-bit</ta>
            <ta e="T194" id="Seg_4152" s="T193">innʼe</ta>
            <ta e="T195" id="Seg_4153" s="T194">gɨn-an</ta>
            <ta e="T196" id="Seg_4154" s="T195">baraːn</ta>
            <ta e="T197" id="Seg_4155" s="T196">hir</ta>
            <ta e="T198" id="Seg_4156" s="T197">taŋara</ta>
            <ta e="T199" id="Seg_4157" s="T198">köt-ön</ta>
            <ta e="T200" id="Seg_4158" s="T199">kaːl-bɨt</ta>
            <ta e="T201" id="Seg_4159" s="T200">harsɨŋŋɨ-tɨ-n</ta>
            <ta e="T202" id="Seg_4160" s="T201">tur-ar</ta>
            <ta e="T203" id="Seg_4161" s="T202">emi͡e</ta>
            <ta e="T204" id="Seg_4162" s="T203">har-ɨ</ta>
            <ta e="T205" id="Seg_4163" s="T204">albɨn-naː</ta>
            <ta e="T206" id="Seg_4164" s="T205">har-ɨ-n</ta>
            <ta e="T207" id="Seg_4165" s="T206">albɨn-n-aːrɨ</ta>
            <ta e="T208" id="Seg_4166" s="T207">emi͡e</ta>
            <ta e="T209" id="Seg_4167" s="T208">köt-ön</ta>
            <ta e="T210" id="Seg_4168" s="T209">kel-er</ta>
            <ta e="T211" id="Seg_4169" s="T210">sar</ta>
            <ta e="T212" id="Seg_4170" s="T211">sar</ta>
            <ta e="T213" id="Seg_4171" s="T212">teheges</ta>
            <ta e="T214" id="Seg_4172" s="T213">kihi</ta>
            <ta e="T215" id="Seg_4173" s="T214">egel</ta>
            <ta e="T216" id="Seg_4174" s="T215">biːr</ta>
            <ta e="T217" id="Seg_4175" s="T216">ogo-gu-n</ta>
            <ta e="T218" id="Seg_4176" s="T217">min</ta>
            <ta e="T219" id="Seg_4177" s="T218">kuturug-u-m</ta>
            <ta e="T220" id="Seg_4178" s="T219">nuːčča</ta>
            <ta e="T221" id="Seg_4179" s="T220">bolot-u-naːgar</ta>
            <ta e="T222" id="Seg_4180" s="T221">hɨtɨː</ta>
            <ta e="T223" id="Seg_4181" s="T222">tiːt-tiːŋ-ŋi-n-bagas-tɨːŋ-ŋɨ-n</ta>
            <ta e="T224" id="Seg_4182" s="T223">bɨh-a</ta>
            <ta e="T225" id="Seg_4183" s="T224">oks-on</ta>
            <ta e="T226" id="Seg_4184" s="T225">tüh-e-r-em-min</ta>
            <ta e="T227" id="Seg_4185" s="T226">anɨ</ta>
            <ta e="T228" id="Seg_4186" s="T227">beje-liːŋ-ŋi-n</ta>
            <ta e="T229" id="Seg_4187" s="T228">s-i͡e-m</ta>
            <ta e="T230" id="Seg_4188" s="T229">emi͡e</ta>
            <ta e="T231" id="Seg_4189" s="T230">biːr</ta>
            <ta e="T232" id="Seg_4190" s="T231">ogo-tu-n</ta>
            <ta e="T233" id="Seg_4191" s="T232">üːt-en</ta>
            <ta e="T234" id="Seg_4192" s="T233">keːs-pit</ta>
            <ta e="T235" id="Seg_4193" s="T234">ikki</ta>
            <ta e="T236" id="Seg_4194" s="T235">ogo-tu-n</ta>
            <ta e="T237" id="Seg_4195" s="T236">hi͡e-bit</ta>
            <ta e="T238" id="Seg_4196" s="T237">hahɨl</ta>
            <ta e="T239" id="Seg_4197" s="T238">har</ta>
            <ta e="T240" id="Seg_4198" s="T239">ɨt-ɨː</ta>
            <ta e="T241" id="Seg_4199" s="T240">olor-oːktoː-but</ta>
            <ta e="T242" id="Seg_4200" s="T241">diːn</ta>
            <ta e="T243" id="Seg_4201" s="T242">doː</ta>
            <ta e="T244" id="Seg_4202" s="T243">bu</ta>
            <ta e="T245" id="Seg_4203" s="T244">anɨ</ta>
            <ta e="T246" id="Seg_4204" s="T245">bu͡ollagɨna</ta>
            <ta e="T247" id="Seg_4205" s="T246">beje-liːm-mi-n</ta>
            <ta e="T248" id="Seg_4206" s="T247">si͡e-n-i</ta>
            <ta e="T249" id="Seg_4207" s="T248">di͡e-n</ta>
            <ta e="T250" id="Seg_4208" s="T249">ɨt-ɨː</ta>
            <ta e="T251" id="Seg_4209" s="T250">olor-but</ta>
            <ta e="T252" id="Seg_4210" s="T251">kaja</ta>
            <ta e="T253" id="Seg_4211" s="T252">bu</ta>
            <ta e="T254" id="Seg_4212" s="T253">olor-dog-una</ta>
            <ta e="T255" id="Seg_4213" s="T254">kukaːkiː</ta>
            <ta e="T256" id="Seg_4214" s="T255">köt-ön</ta>
            <ta e="T257" id="Seg_4215" s="T256">kel-bit</ta>
            <ta e="T258" id="Seg_4216" s="T257">kajaː</ta>
            <ta e="T259" id="Seg_4217" s="T258">har</ta>
            <ta e="T260" id="Seg_4218" s="T259">tu͡ok-tan</ta>
            <ta e="T261" id="Seg_4219" s="T260">ɨt-ɨː-gɨn</ta>
            <ta e="T262" id="Seg_4220" s="T261">ki͡e</ta>
            <ta e="T263" id="Seg_4221" s="T262">en</ta>
            <ta e="T264" id="Seg_4222" s="T263">bu</ta>
            <ta e="T265" id="Seg_4223" s="T264">munnuk</ta>
            <ta e="T266" id="Seg_4224" s="T265">bosku͡oj</ta>
            <ta e="T267" id="Seg_4225" s="T266">kün-neːk</ta>
            <ta e="T268" id="Seg_4226" s="T267">kün</ta>
            <ta e="T269" id="Seg_4227" s="T268">tɨŋ-a</ta>
            <ta e="T270" id="Seg_4228" s="T269">tur-dag-ɨna</ta>
            <ta e="T271" id="Seg_4229" s="T270">ogo-lor-u-ŋ</ta>
            <ta e="T272" id="Seg_4230" s="T271">da</ta>
            <ta e="T273" id="Seg_4231" s="T272">ičiges-ke</ta>
            <ta e="T274" id="Seg_4232" s="T273">hɨt-al-lar</ta>
            <ta e="T275" id="Seg_4233" s="T274">beje-ŋ</ta>
            <ta e="T276" id="Seg_4234" s="T275">da</ta>
            <ta e="T277" id="Seg_4235" s="T276">ah-ɨ</ta>
            <ta e="T278" id="Seg_4236" s="T277">bagas</ta>
            <ta e="T279" id="Seg_4237" s="T278">egel-en</ta>
            <ta e="T280" id="Seg_4238" s="T279">kel-er</ta>
            <ta e="T281" id="Seg_4239" s="T280">kördük</ta>
            <ta e="T282" id="Seg_4240" s="T281">bu͡o</ta>
            <ta e="T283" id="Seg_4241" s="T282">tu͡ok-tan</ta>
            <ta e="T284" id="Seg_4242" s="T283">ɨt-ɨː-gɨn</ta>
            <ta e="T285" id="Seg_4243" s="T284">dʼe</ta>
            <ta e="T286" id="Seg_4244" s="T285">munnuk</ta>
            <ta e="T287" id="Seg_4245" s="T286">beseleː-ge</ta>
            <ta e="T288" id="Seg_4246" s="T287">olor-oŋ-ŋun</ta>
            <ta e="T289" id="Seg_4247" s="T288">diː-r</ta>
            <ta e="T290" id="Seg_4248" s="T289">en</ta>
            <ta e="T291" id="Seg_4249" s="T290">kajdak</ta>
            <ta e="T292" id="Seg_4250" s="T291">ɨt-a-p-pak-kɨn</ta>
            <ta e="T293" id="Seg_4251" s="T292">kör-bök-kün</ta>
            <ta e="T294" id="Seg_4252" s="T293">du͡o</ta>
            <ta e="T295" id="Seg_4253" s="T294">minigi-n</ta>
            <ta e="T296" id="Seg_4254" s="T295">bu͡ollagɨna</ta>
            <ta e="T297" id="Seg_4255" s="T296">hahɨl</ta>
            <ta e="T298" id="Seg_4256" s="T297">bu͡olla</ta>
            <ta e="T299" id="Seg_4257" s="T298">ikki</ta>
            <ta e="T300" id="Seg_4258" s="T299">küŋ-ŋe</ta>
            <ta e="T301" id="Seg_4259" s="T300">ikki</ta>
            <ta e="T302" id="Seg_4260" s="T301">ogo-bu-n</ta>
            <ta e="T303" id="Seg_4261" s="T302">hi͡e-t-e</ta>
            <ta e="T304" id="Seg_4262" s="T303">anɨ</ta>
            <ta e="T305" id="Seg_4263" s="T304">bügün</ta>
            <ta e="T306" id="Seg_4264" s="T305">emi͡e</ta>
            <ta e="T307" id="Seg_4265" s="T306">kel-i͡eg-e</ta>
            <ta e="T308" id="Seg_4266" s="T307">h-i͡eg-e</ta>
            <ta e="T309" id="Seg_4267" s="T308">tiːt-tiːm-mi-n</ta>
            <ta e="T310" id="Seg_4268" s="T309">bagas-tɨːm-mɨ-n</ta>
            <ta e="T311" id="Seg_4269" s="T310">bɨh-a</ta>
            <ta e="T312" id="Seg_4270" s="T311">oks-on</ta>
            <ta e="T313" id="Seg_4271" s="T312">tüh-e-r-en</ta>
            <ta e="T314" id="Seg_4272" s="T313">beje-liːm-mi-n</ta>
            <ta e="T315" id="Seg_4273" s="T314">si͡e-ri</ta>
            <ta e="T316" id="Seg_4274" s="T315">gɨn-n-a</ta>
            <ta e="T317" id="Seg_4275" s="T316">diː-r</ta>
            <ta e="T318" id="Seg_4276" s="T317">tu͡ok-taːg=ɨj</ta>
            <ta e="T319" id="Seg_4277" s="T318">ol</ta>
            <ta e="T320" id="Seg_4278" s="T319">sahɨl-ɨ-ŋ</ta>
            <ta e="T321" id="Seg_4279" s="T320">kaja</ta>
            <ta e="T322" id="Seg_4280" s="T321">kuturug-a</ta>
            <ta e="T323" id="Seg_4281" s="T322">nuːčča</ta>
            <ta e="T324" id="Seg_4282" s="T323">bolot-u-naːgar</ta>
            <ta e="T325" id="Seg_4283" s="T324">hɨtɨː</ta>
            <ta e="T326" id="Seg_4284" s="T325">doː</ta>
            <ta e="T327" id="Seg_4285" s="T326">tu͡ok</ta>
            <ta e="T328" id="Seg_4286" s="T327">aːt-taːg-a</ta>
            <ta e="T329" id="Seg_4287" s="T328">aŋɨr</ta>
            <ta e="T330" id="Seg_4288" s="T329">kötör-ö</ta>
            <ta e="T331" id="Seg_4289" s="T330">töröː-büt-ü-ŋ=üj</ta>
            <ta e="T332" id="Seg_4290" s="T331">en</ta>
            <ta e="T333" id="Seg_4291" s="T332">munnuk</ta>
            <ta e="T334" id="Seg_4292" s="T333">dʼeː</ta>
            <ta e="T335" id="Seg_4293" s="T334">har</ta>
            <ta e="T336" id="Seg_4294" s="T335">kajdak</ta>
            <ta e="T337" id="Seg_4295" s="T336">gɨn-a</ta>
            <ta e="T338" id="Seg_4296" s="T337">hahɨl</ta>
            <ta e="T339" id="Seg_4297" s="T338">kajdak</ta>
            <ta e="T340" id="Seg_4298" s="T339">gɨn-an</ta>
            <ta e="T341" id="Seg_4299" s="T340">en</ta>
            <ta e="T342" id="Seg_4300" s="T341">tiːt-ki-n</ta>
            <ta e="T343" id="Seg_4301" s="T342">bɨh-a</ta>
            <ta e="T344" id="Seg_4302" s="T343">oks-u͡o=j</ta>
            <ta e="T345" id="Seg_4303" s="T344">kuturug-a</ta>
            <ta e="T346" id="Seg_4304" s="T345">tüː</ta>
            <ta e="T347" id="Seg_4305" s="T346">tüː</ta>
            <ta e="T348" id="Seg_4306" s="T347">kuturuk</ta>
            <ta e="T349" id="Seg_4307" s="T348">tug-u</ta>
            <ta e="T350" id="Seg_4308" s="T349">kot-u͡o-ŋ=uj</ta>
            <ta e="T351" id="Seg_4309" s="T350">di͡e</ta>
            <ta e="T352" id="Seg_4310" s="T351">anɨ</ta>
            <ta e="T353" id="Seg_4311" s="T352">kel-leg-ine</ta>
            <ta e="T354" id="Seg_4312" s="T353">kim</ta>
            <ta e="T355" id="Seg_4313" s="T354">ü͡örep-pit-e=j</ta>
            <ta e="T356" id="Seg_4314" s="T355">di͡e-teg-ine</ta>
            <ta e="T357" id="Seg_4315" s="T356">kukaːkiː</ta>
            <ta e="T358" id="Seg_4316" s="T357">ü͡örep-pit-e</ta>
            <ta e="T359" id="Seg_4317" s="T358">d-iːr</ta>
            <ta e="T360" id="Seg_4318" s="T359">dʼe</ta>
            <ta e="T361" id="Seg_4319" s="T360">har</ta>
            <ta e="T364" id="Seg_4320" s="T363">har</ta>
            <ta e="T365" id="Seg_4321" s="T364">dʼe</ta>
            <ta e="T366" id="Seg_4322" s="T365">hi͡ese</ta>
            <ta e="T367" id="Seg_4323" s="T366">hek</ta>
            <ta e="T368" id="Seg_4324" s="T367">bu͡ol-but</ta>
            <ta e="T369" id="Seg_4325" s="T368">olor-but</ta>
            <ta e="T370" id="Seg_4326" s="T369">kukaːkiː-ta</ta>
            <ta e="T371" id="Seg_4327" s="T370">köt-ön</ta>
            <ta e="T372" id="Seg_4328" s="T371">kaːl-bɨt</ta>
            <ta e="T373" id="Seg_4329" s="T372">oː</ta>
            <ta e="T374" id="Seg_4330" s="T373">dʼe</ta>
            <ta e="T375" id="Seg_4331" s="T374">sahɨl</ta>
            <ta e="T376" id="Seg_4332" s="T375">kɨjŋan-aːrɨ</ta>
            <ta e="T377" id="Seg_4333" s="T376">kɨjŋam-mɨt</ta>
            <ta e="T378" id="Seg_4334" s="T377">bu͡ol-but</ta>
            <ta e="T379" id="Seg_4335" s="T378">kɨːhɨr-aːrɨ</ta>
            <ta e="T380" id="Seg_4336" s="T379">kɨːhɨr-bɨt</ta>
            <ta e="T381" id="Seg_4337" s="T380">bu͡ol-but</ta>
            <ta e="T382" id="Seg_4338" s="T381">eŋin-eŋin</ta>
            <ta e="T383" id="Seg_4339" s="T382">köppög-ü</ta>
            <ta e="T384" id="Seg_4340" s="T383">tur-u-t-a</ta>
            <ta e="T385" id="Seg_4341" s="T384">tebi͡el-iː-tebi͡eleː-iː</ta>
            <ta e="T386" id="Seg_4342" s="T385">ilin</ta>
            <ta e="T387" id="Seg_4343" s="T386">kalin</ta>
            <ta e="T388" id="Seg_4344" s="T387">atag-ɨ-nan</ta>
            <ta e="T389" id="Seg_4345" s="T388">kajɨt-a</ta>
            <ta e="T390" id="Seg_4346" s="T389">tebi͡el-iː-tebi͡eleː-iː</ta>
            <ta e="T391" id="Seg_4347" s="T390">kel-eːkteː-bit</ta>
            <ta e="T392" id="Seg_4348" s="T391">diːn</ta>
            <ta e="T393" id="Seg_4349" s="T392">har-ɨ-ga</ta>
            <ta e="T394" id="Seg_4350" s="T393">har-ɨ</ta>
            <ta e="T395" id="Seg_4351" s="T394">kutt-aːrɨ</ta>
            <ta e="T396" id="Seg_4352" s="T395">sar</ta>
            <ta e="T397" id="Seg_4353" s="T396">sar</ta>
            <ta e="T398" id="Seg_4354" s="T397">onnuk-mannɨk</ta>
            <ta e="T399" id="Seg_4355" s="T398">inʼe-leːk</ta>
            <ta e="T400" id="Seg_4356" s="T399">kaja</ta>
            <ta e="T401" id="Seg_4357" s="T400">min</ta>
            <ta e="T402" id="Seg_4358" s="T401">bu͡ollagɨna</ta>
            <ta e="T403" id="Seg_4359" s="T402">emi͡e</ta>
            <ta e="T404" id="Seg_4360" s="T403">bu͡opsa</ta>
            <ta e="T405" id="Seg_4361" s="T404">kɨjŋan-aːrɨ</ta>
            <ta e="T406" id="Seg_4362" s="T405">kɨjŋan-nɨ-m</ta>
            <ta e="T407" id="Seg_4363" s="T406">egel</ta>
            <ta e="T408" id="Seg_4364" s="T407">biːr</ta>
            <ta e="T409" id="Seg_4365" s="T408">ogo-to</ta>
            <ta e="T410" id="Seg_4366" s="T409">min</ta>
            <ta e="T411" id="Seg_4367" s="T410">kuturug-u-m</ta>
            <ta e="T412" id="Seg_4368" s="T411">nʼuːčča</ta>
            <ta e="T413" id="Seg_4369" s="T412">bolot-u-naːgar</ta>
            <ta e="T414" id="Seg_4370" s="T413">hɨtɨː</ta>
            <ta e="T415" id="Seg_4371" s="T414">hanɨ</ta>
            <ta e="T416" id="Seg_4372" s="T415">beje-liːŋ-ŋi-n</ta>
            <ta e="T419" id="Seg_4373" s="T418">tuːra</ta>
            <ta e="T420" id="Seg_4374" s="T419">oks-om-mun</ta>
            <ta e="T421" id="Seg_4375" s="T420">beje-liːŋ-ŋi-n</ta>
            <ta e="T422" id="Seg_4376" s="T421">s-i͡e-m</ta>
            <ta e="T423" id="Seg_4377" s="T422">onnoːgor</ta>
            <ta e="T424" id="Seg_4378" s="T423">koː</ta>
            <ta e="T425" id="Seg_4379" s="T424">aŋɨr-daː-m-mɨn</ta>
            <ta e="T426" id="Seg_4380" s="T425">bu͡opsa</ta>
            <ta e="T430" id="Seg_4381" s="T429">kaːrɨjan</ta>
            <ta e="T431" id="Seg_4382" s="T430">ikki</ta>
            <ta e="T432" id="Seg_4383" s="T431">ogo-bu-n</ta>
            <ta e="T433" id="Seg_4384" s="T432">hi͡e-t-ti-m</ta>
            <ta e="T434" id="Seg_4385" s="T433">min</ta>
            <ta e="T435" id="Seg_4386" s="T434">eni͡e-ke</ta>
            <ta e="T436" id="Seg_4387" s="T435">beje-m</ta>
            <ta e="T437" id="Seg_4388" s="T436">aŋɨr-ba-r</ta>
            <ta e="T438" id="Seg_4389" s="T437">en</ta>
            <ta e="T439" id="Seg_4390" s="T438">bu͡ollagɨna</ta>
            <ta e="T440" id="Seg_4391" s="T439">tüː</ta>
            <ta e="T441" id="Seg_4392" s="T440">kuturuk-kun</ta>
            <ta e="T442" id="Seg_4393" s="T441">diːn</ta>
            <ta e="T443" id="Seg_4394" s="T442">tüː</ta>
            <ta e="T444" id="Seg_4395" s="T443">kuturuk</ta>
            <ta e="T445" id="Seg_4396" s="T444">tug-u</ta>
            <ta e="T446" id="Seg_4397" s="T445">kot-u͡o-ŋ=uj</ta>
            <ta e="T447" id="Seg_4398" s="T446">mah-ɨ</ta>
            <ta e="T448" id="Seg_4399" s="T447">kot-u͡o-ŋ</ta>
            <ta e="T449" id="Seg_4400" s="T448">du͡o</ta>
            <ta e="T450" id="Seg_4401" s="T449">diː-r</ta>
            <ta e="T451" id="Seg_4402" s="T450">koː</ta>
            <ta e="T452" id="Seg_4403" s="T451">arakata</ta>
            <ta e="T453" id="Seg_4404" s="T452">tu͡ok</ta>
            <ta e="T454" id="Seg_4405" s="T453">aːt-taːg-a</ta>
            <ta e="T455" id="Seg_4406" s="T454">öj</ta>
            <ta e="T456" id="Seg_4407" s="T455">bi͡er-t-e=j</ta>
            <ta e="T457" id="Seg_4408" s="T456">eni͡e-ke</ta>
            <ta e="T458" id="Seg_4409" s="T457">kajdak</ta>
            <ta e="T459" id="Seg_4410" s="T458">gɨn-aŋ-ŋɨn</ta>
            <ta e="T460" id="Seg_4411" s="T459">öj-ü</ta>
            <ta e="T461" id="Seg_4412" s="T460">bul-t-u-ŋ=uj</ta>
            <ta e="T462" id="Seg_4413" s="T461">diː-r</ta>
            <ta e="T463" id="Seg_4414" s="T462">mini͡ene</ta>
            <ta e="T464" id="Seg_4415" s="T463">kuturug-u-m</ta>
            <ta e="T465" id="Seg_4416" s="T464">tüː-tü-n</ta>
            <ta e="T466" id="Seg_4417" s="T465">kantan</ta>
            <ta e="T467" id="Seg_4418" s="T466">bil-e-gin</ta>
            <ta e="T468" id="Seg_4419" s="T467">kaja</ta>
            <ta e="T469" id="Seg_4420" s="T468">minigi-n</ta>
            <ta e="T470" id="Seg_4421" s="T469">kukaːkiː</ta>
            <ta e="T471" id="Seg_4422" s="T470">ü͡örep-pit-e</ta>
            <ta e="T472" id="Seg_4423" s="T471">kim</ta>
            <ta e="T473" id="Seg_4424" s="T472">bi͡er-i͡e=j</ta>
            <ta e="T474" id="Seg_4425" s="T473">mini͡e-ke</ta>
            <ta e="T475" id="Seg_4426" s="T474">öj</ta>
            <ta e="T476" id="Seg_4427" s="T475">kukaːkiː</ta>
            <ta e="T477" id="Seg_4428" s="T476">ü͡örep-pit-e</ta>
            <ta e="T478" id="Seg_4429" s="T477">kajaː</ta>
            <ta e="T479" id="Seg_4430" s="T478">onnuk-mannɨk</ta>
            <ta e="T480" id="Seg_4431" s="T479">otto</ta>
            <ta e="T481" id="Seg_4432" s="T480">kukaːki-nɨ</ta>
            <ta e="T482" id="Seg_4433" s="T481">kaja</ta>
            <ta e="T483" id="Seg_4434" s="T482">hir-ten</ta>
            <ta e="T484" id="Seg_4435" s="T483">bul-u͡o-b=uj</ta>
            <ta e="T485" id="Seg_4436" s="T484">dʼe</ta>
            <ta e="T486" id="Seg_4437" s="T485">beːbe</ta>
            <ta e="T487" id="Seg_4438" s="T486">dʼe</ta>
            <ta e="T488" id="Seg_4439" s="T487">u͡oldʼah-ɨ͡a-m</ta>
            <ta e="T489" id="Seg_4440" s="T488">min</ta>
            <ta e="T490" id="Seg_4441" s="T489">gini͡e-ke</ta>
            <ta e="T491" id="Seg_4442" s="T490">daːgɨnɨ</ta>
            <ta e="T492" id="Seg_4443" s="T491">innʼe</ta>
            <ta e="T493" id="Seg_4444" s="T492">gɨm-mɨt</ta>
            <ta e="T494" id="Seg_4445" s="T493">da</ta>
            <ta e="T495" id="Seg_4446" s="T494">hir</ta>
            <ta e="T496" id="Seg_4447" s="T495">taŋara</ta>
            <ta e="T497" id="Seg_4448" s="T496">köt-ön</ta>
            <ta e="T498" id="Seg_4449" s="T497">kaːl-bɨt</ta>
            <ta e="T499" id="Seg_4450" s="T498">hir</ta>
            <ta e="T500" id="Seg_4451" s="T499">taŋara</ta>
            <ta e="T501" id="Seg_4452" s="T500">köt-ön</ta>
            <ta e="T502" id="Seg_4453" s="T501">ih-en</ta>
            <ta e="T503" id="Seg_4454" s="T502">bu͡ol-l-a</ta>
            <ta e="T504" id="Seg_4455" s="T503">töp-tögürük</ta>
            <ta e="T505" id="Seg_4456" s="T504">kü͡öl-ge</ta>
            <ta e="T507" id="Seg_4457" s="T505">tij-en</ta>
            <ta e="T508" id="Seg_4458" s="T507">bu</ta>
            <ta e="T509" id="Seg_4459" s="T508">kü͡öl</ta>
            <ta e="T510" id="Seg_4460" s="T509">tu͡ok-tan</ta>
            <ta e="T511" id="Seg_4461" s="T510">daː</ta>
            <ta e="T512" id="Seg_4462" s="T511">ulakan</ta>
            <ta e="T513" id="Seg_4463" s="T512">kaja-laːk</ta>
            <ta e="T514" id="Seg_4464" s="T513">haːs</ta>
            <ta e="T515" id="Seg_4465" s="T514">kem-i-ger</ta>
            <ta e="T516" id="Seg_4466" s="T515">bu͡ol-l-a</ta>
            <ta e="T517" id="Seg_4467" s="T516">keriːske-te</ta>
            <ta e="T518" id="Seg_4468" s="T517">barɨta</ta>
            <ta e="T519" id="Seg_4469" s="T518">taks-ɨ-bɨt</ta>
            <ta e="T520" id="Seg_4470" s="T519">bu</ta>
            <ta e="T521" id="Seg_4471" s="T520">keriːske-ni</ta>
            <ta e="T522" id="Seg_4472" s="T521">bu͡ollagɨna</ta>
            <ta e="T523" id="Seg_4473" s="T522">köt-ö</ta>
            <ta e="T524" id="Seg_4474" s="T523">hɨldʼ-an</ta>
            <ta e="T525" id="Seg_4475" s="T524">bu͡opsa</ta>
            <ta e="T526" id="Seg_4476" s="T525">muŋ</ta>
            <ta e="T527" id="Seg_4477" s="T526">bu͡osku͡oj</ta>
            <ta e="T528" id="Seg_4478" s="T527">karadʼɨk-ka</ta>
            <ta e="T529" id="Seg_4479" s="T528">bu͡ollagɨna</ta>
            <ta e="T530" id="Seg_4480" s="T529">öl-büt</ta>
            <ta e="T531" id="Seg_4481" s="T530">sahɨl</ta>
            <ta e="T532" id="Seg_4482" s="T531">bu͡ol-an</ta>
            <ta e="T533" id="Seg_4483" s="T532">baran</ta>
            <ta e="T534" id="Seg_4484" s="T533">nele-s</ta>
            <ta e="T535" id="Seg_4485" s="T534">gɨn-a</ta>
            <ta e="T536" id="Seg_4486" s="T535">tüs-püt</ta>
            <ta e="T537" id="Seg_4487" s="T536">bu͡opsa</ta>
            <ta e="T538" id="Seg_4488" s="T537">ü͡öŋ-ŋe</ta>
            <ta e="T539" id="Seg_4489" s="T538">barɨ-tɨ-gar</ta>
            <ta e="T540" id="Seg_4490" s="T539">ɨs-tar-a</ta>
            <ta e="T541" id="Seg_4491" s="T540">hɨp-pɨt</ta>
            <ta e="T542" id="Seg_4492" s="T541">ol</ta>
            <ta e="T543" id="Seg_4493" s="T542">gɨn-an</ta>
            <ta e="T544" id="Seg_4494" s="T543">baraːn</ta>
            <ta e="T545" id="Seg_4495" s="T544">kajaː</ta>
            <ta e="T546" id="Seg_4496" s="T545">kukaːkiː-lar</ta>
            <ta e="T547" id="Seg_4497" s="T546">daːganɨ</ta>
            <ta e="T548" id="Seg_4498" s="T547">köt-ö</ta>
            <ta e="T549" id="Seg_4499" s="T548">hɨldʼ-an</ta>
            <ta e="T550" id="Seg_4500" s="T549">er-en</ta>
            <ta e="T551" id="Seg_4501" s="T550">kör-ör</ta>
            <ta e="T552" id="Seg_4502" s="T551">sahɨl</ta>
            <ta e="T553" id="Seg_4503" s="T552">öl-ön</ta>
            <ta e="T554" id="Seg_4504" s="T553">baraːn</ta>
            <ta e="T555" id="Seg_4505" s="T554">nelej-en</ta>
            <ta e="T556" id="Seg_4506" s="T555">hɨt-ar</ta>
            <ta e="T557" id="Seg_4507" s="T556">oː</ta>
            <ta e="T558" id="Seg_4508" s="T557">baː</ta>
            <ta e="T559" id="Seg_4509" s="T558">turkarɨ</ta>
            <ta e="T560" id="Seg_4510" s="T559">ba</ta>
            <ta e="T561" id="Seg_4511" s="T560">korguj-an</ta>
            <ta e="T562" id="Seg_4512" s="T561">bu͡ollagɨna</ta>
            <ta e="T563" id="Seg_4513" s="T562">har</ta>
            <ta e="T564" id="Seg_4514" s="T563">ogo-lor-u-n</ta>
            <ta e="T565" id="Seg_4515" s="T564">s-iː</ta>
            <ta e="T566" id="Seg_4516" s="T565">hɨldʼ-ɨ-bɨt-a</ta>
            <ta e="T567" id="Seg_4517" s="T566">bu͡olla</ta>
            <ta e="T568" id="Seg_4518" s="T567">anɨ</ta>
            <ta e="T569" id="Seg_4519" s="T568">sar-a</ta>
            <ta e="T570" id="Seg_4520" s="T569">bu͡ollagɨna</ta>
            <ta e="T571" id="Seg_4521" s="T570">minigi-tten</ta>
            <ta e="T572" id="Seg_4522" s="T571">öj</ta>
            <ta e="T573" id="Seg_4523" s="T572">ɨl-ar</ta>
            <ta e="T574" id="Seg_4524" s="T573">biːr</ta>
            <ta e="T575" id="Seg_4525" s="T574">da</ta>
            <ta e="T576" id="Seg_4526" s="T575">ogo-tu-n</ta>
            <ta e="T577" id="Seg_4527" s="T576">bi͡er-betek</ta>
            <ta e="T578" id="Seg_4528" s="T577">oččogo</ta>
            <ta e="T579" id="Seg_4529" s="T578">kantan</ta>
            <ta e="T580" id="Seg_4530" s="T579">ɨl-an</ta>
            <ta e="T581" id="Seg_4531" s="T580">gini</ta>
            <ta e="T582" id="Seg_4532" s="T581">daːganɨ</ta>
            <ta e="T583" id="Seg_4533" s="T582">korguj-but</ta>
            <ta e="T584" id="Seg_4534" s="T583">kihi</ta>
            <ta e="T585" id="Seg_4535" s="T584">kantan</ta>
            <ta e="T586" id="Seg_4536" s="T585">ɨl-an</ta>
            <ta e="T587" id="Seg_4537" s="T586">ah-ɨ͡a=j</ta>
            <ta e="T588" id="Seg_4538" s="T587">dʼe</ta>
            <ta e="T589" id="Seg_4539" s="T588">okt-on</ta>
            <ta e="T590" id="Seg_4540" s="T589">öl-büt</ta>
            <ta e="T591" id="Seg_4541" s="T590">dʼe</ta>
            <ta e="T592" id="Seg_4542" s="T591">en</ta>
            <ta e="T593" id="Seg_4543" s="T592">daːganɨ</ta>
            <ta e="T594" id="Seg_4544" s="T593">öl-ör-döːk-kün</ta>
            <ta e="T595" id="Seg_4545" s="T594">e-bit</ta>
            <ta e="T596" id="Seg_4546" s="T595">di͡e-n</ta>
            <ta e="T597" id="Seg_4547" s="T596">kaja</ta>
            <ta e="T598" id="Seg_4548" s="T597">kel-bit</ta>
            <ta e="T599" id="Seg_4549" s="T598">loŋ-loŋ</ta>
            <ta e="T600" id="Seg_4550" s="T599">loŋ-loŋ</ta>
            <ta e="T601" id="Seg_4551" s="T600">loŋsuj-but</ta>
            <ta e="T602" id="Seg_4552" s="T601">bu͡o</ta>
            <ta e="T603" id="Seg_4553" s="T602">kantɨttan</ta>
            <ta e="T604" id="Seg_4554" s="T603">karag-ɨ-ttan</ta>
            <ta e="T605" id="Seg_4555" s="T604">h-i͡ek-ke</ta>
            <ta e="T606" id="Seg_4556" s="T605">di͡e-n</ta>
            <ta e="T607" id="Seg_4557" s="T606">karɨ-ga</ta>
            <ta e="T608" id="Seg_4558" s="T607">loŋsu-n-a</ta>
            <ta e="T609" id="Seg_4559" s="T608">hɨrɨt-tag-ɨna</ta>
            <ta e="T610" id="Seg_4560" s="T609">bu͡opsa</ta>
            <ta e="T611" id="Seg_4561" s="T682">sahɨl</ta>
            <ta e="T612" id="Seg_4562" s="T611">bu͡ollagɨna</ta>
            <ta e="T613" id="Seg_4563" s="T612">ele</ta>
            <ta e="T614" id="Seg_4564" s="T613">nʼɨma-tɨ-nan</ta>
            <ta e="T615" id="Seg_4565" s="T614">ele</ta>
            <ta e="T616" id="Seg_4566" s="T615">kütür-ü-nen</ta>
            <ta e="T617" id="Seg_4567" s="T616">ɨrdʼɨgɨn-ɨː</ta>
            <ta e="T618" id="Seg_4568" s="T617">gɨn-a</ta>
            <ta e="T619" id="Seg_4569" s="T618">ɨstan-aːktaː-bɨt</ta>
            <ta e="T621" id="Seg_4570" s="T619">diːn</ta>
            <ta e="T622" id="Seg_4571" s="T621">baːgɨ</ta>
            <ta e="T623" id="Seg_4572" s="T622">sar</ta>
            <ta e="T624" id="Seg_4573" s="T623">bu͡ollagɨna</ta>
            <ta e="T625" id="Seg_4574" s="T624">kim</ta>
            <ta e="T626" id="Seg_4575" s="T625">bu͡ollagɨna</ta>
            <ta e="T627" id="Seg_4576" s="T626">kukaːkiː</ta>
            <ta e="T628" id="Seg_4577" s="T627">bu͡ollagɨna</ta>
            <ta e="T629" id="Seg_4578" s="T628">kü͡örejen-en</ta>
            <ta e="T630" id="Seg_4579" s="T629">üspü͡öj-deː-bekke</ta>
            <ta e="T631" id="Seg_4580" s="T630">kuturug-u-ttan</ta>
            <ta e="T632" id="Seg_4581" s="T631">kap-tar-bɨt</ta>
            <ta e="T633" id="Seg_4582" s="T632">innʼe</ta>
            <ta e="T634" id="Seg_4583" s="T633">gɨn-an</ta>
            <ta e="T635" id="Seg_4584" s="T634">mu-nu</ta>
            <ta e="T636" id="Seg_4585" s="T635">haŋast-al-a-h-an</ta>
            <ta e="T637" id="Seg_4586" s="T636">haŋast-a-h-an</ta>
            <ta e="T638" id="Seg_4587" s="T637">uː</ta>
            <ta e="T639" id="Seg_4588" s="T638">ih-i-ger</ta>
            <ta e="T640" id="Seg_4589" s="T639">čekeni-t-en</ta>
            <ta e="T641" id="Seg_4590" s="T640">tüh-e-r-en</ta>
            <ta e="T642" id="Seg_4591" s="T641">uː</ta>
            <ta e="T645" id="Seg_4592" s="T644">uː</ta>
            <ta e="T646" id="Seg_4593" s="T645">ih-i-ger</ta>
            <ta e="T647" id="Seg_4594" s="T646">tüh-e-r-en</ta>
            <ta e="T648" id="Seg_4595" s="T647">munu-ga</ta>
            <ta e="T649" id="Seg_4596" s="T648">bulkuj-an</ta>
            <ta e="T650" id="Seg_4597" s="T649">hiŋe-leːk</ta>
            <ta e="T651" id="Seg_4598" s="T650">uː-ga</ta>
            <ta e="T652" id="Seg_4599" s="T651">bulkuj-an</ta>
            <ta e="T653" id="Seg_4600" s="T652">tumnar-an</ta>
            <ta e="T654" id="Seg_4601" s="T653">mülčü</ta>
            <ta e="T655" id="Seg_4602" s="T654">tut-tar-an</ta>
            <ta e="T656" id="Seg_4603" s="T655">köt-ön</ta>
            <ta e="T657" id="Seg_4604" s="T656">kaːl-bɨt</ta>
            <ta e="T658" id="Seg_4605" s="T657">dʼe</ta>
            <ta e="T659" id="Seg_4606" s="T658">ol</ta>
            <ta e="T660" id="Seg_4607" s="T659">sahɨl</ta>
            <ta e="T661" id="Seg_4608" s="T660">bu͡ollagɨna</ta>
            <ta e="T662" id="Seg_4609" s="T661">kɨtɨl-ga</ta>
            <ta e="T663" id="Seg_4610" s="T662">taks-an</ta>
            <ta e="T664" id="Seg_4611" s="T663">kuːr-dun-a</ta>
            <ta e="T665" id="Seg_4612" s="T664">ɨrata</ta>
            <ta e="T666" id="Seg_4613" s="T665">eni</ta>
            <ta e="T667" id="Seg_4614" s="T666">dʼe</ta>
            <ta e="T668" id="Seg_4615" s="T667">ol</ta>
            <ta e="T669" id="Seg_4616" s="T668">kuːr-dun-an-kanʼaː-n</ta>
            <ta e="T671" id="Seg_4617" s="T669">baraːn</ta>
            <ta e="T672" id="Seg_4618" s="T671">till-en</ta>
            <ta e="T673" id="Seg_4619" s="T672">baj-an-tot-on</ta>
            <ta e="T674" id="Seg_4620" s="T673">olor-but-a</ta>
            <ta e="T675" id="Seg_4621" s="T674">duː</ta>
            <ta e="T676" id="Seg_4622" s="T675">kajdi͡ek</ta>
            <ta e="T677" id="Seg_4623" s="T676">bu͡ol-but-a</ta>
            <ta e="T678" id="Seg_4624" s="T677">duː</ta>
            <ta e="T679" id="Seg_4625" s="T678">dʼe</ta>
            <ta e="T680" id="Seg_4626" s="T679">oloŋko-but</ta>
            <ta e="T681" id="Seg_4627" s="T680">ele-te</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_4628" s="T0">bu</ta>
            <ta e="T2" id="Seg_4629" s="T1">urut</ta>
            <ta e="T3" id="Seg_4630" s="T2">küččügüj</ta>
            <ta e="T4" id="Seg_4631" s="T3">er-TAK-BItI-ttAn</ta>
            <ta e="T5" id="Seg_4632" s="T4">bi͡ek-ttAn</ta>
            <ta e="T6" id="Seg_4633" s="T5">oloŋko-LAː-A-s-An</ta>
            <ta e="T7" id="Seg_4634" s="T6">ü͡öskeː-BIT-BIt</ta>
            <ta e="T8" id="Seg_4635" s="T7">du͡o</ta>
            <ta e="T9" id="Seg_4636" s="T8">ol</ta>
            <ta e="T10" id="Seg_4637" s="T9">bu</ta>
            <ta e="T11" id="Seg_4638" s="T10">bihigi</ta>
            <ta e="T12" id="Seg_4639" s="T11">küččügüj</ta>
            <ta e="T13" id="Seg_4640" s="T12">er-TAK-BI-ttAn</ta>
            <ta e="T14" id="Seg_4641" s="T13">tu͡ok</ta>
            <ta e="T15" id="Seg_4642" s="T14">da</ta>
            <ta e="T16" id="Seg_4643" s="T15">oːnnʼoː-Iː</ta>
            <ta e="T17" id="Seg_4644" s="T16">hu͡ok</ta>
            <ta e="T18" id="Seg_4645" s="T17">e-TI-tA</ta>
            <ta e="T19" id="Seg_4646" s="T18">bu</ta>
            <ta e="T20" id="Seg_4647" s="T19">ogo-LAr-nI</ta>
            <ta e="T21" id="Seg_4648" s="T20">utuj-t-I-m-AːrI-LAr</ta>
            <ta e="T22" id="Seg_4649" s="T21">kanʼaː-m-AːrI-LAr</ta>
            <ta e="T23" id="Seg_4650" s="T22">bu</ta>
            <ta e="T24" id="Seg_4651" s="T23">teːte-BIt-LAːK</ta>
            <ta e="T25" id="Seg_4652" s="T24">teːte-BIt</ta>
            <ta e="T26" id="Seg_4653" s="T25">teːte-tI-LAːK</ta>
            <ta e="T27" id="Seg_4654" s="T26">ogo-GA</ta>
            <ta e="T28" id="Seg_4655" s="T27">oloŋko-LAː-Ar</ta>
            <ta e="T29" id="Seg_4656" s="T28">oloŋko-LArA</ta>
            <ta e="T30" id="Seg_4657" s="T29">e-TI-tA</ta>
            <ta e="T31" id="Seg_4658" s="T30">anaːn</ta>
            <ta e="T32" id="Seg_4659" s="T31">ogo</ta>
            <ta e="T33" id="Seg_4660" s="T32">oloŋko-tA</ta>
            <ta e="T34" id="Seg_4661" s="T33">di͡e-An</ta>
            <ta e="T35" id="Seg_4662" s="T34">bɨlɨr</ta>
            <ta e="T36" id="Seg_4663" s="T35">hir</ta>
            <ta e="T37" id="Seg_4664" s="T36">taŋara</ta>
            <ta e="T38" id="Seg_4665" s="T37">ü͡öskeː-Ar-tI-n</ta>
            <ta e="T39" id="Seg_4666" s="T38">hagɨna</ta>
            <ta e="T40" id="Seg_4667" s="T39">barɨ-tA</ta>
            <ta e="T41" id="Seg_4668" s="T40">tɨl-LAːK</ta>
            <ta e="T42" id="Seg_4669" s="T41">e-TI-LArA</ta>
            <ta e="T43" id="Seg_4670" s="T42">ühü</ta>
            <ta e="T44" id="Seg_4671" s="T43">bult-LAr</ta>
            <ta e="T45" id="Seg_4672" s="T44">barɨ-LArA</ta>
            <ta e="T46" id="Seg_4673" s="T45">ol</ta>
            <ta e="T47" id="Seg_4674" s="T46">bult-LArA</ta>
            <ta e="T48" id="Seg_4675" s="T47">eː</ta>
            <ta e="T49" id="Seg_4676" s="T48">ös-LArA</ta>
            <ta e="T50" id="Seg_4677" s="T49">ol</ta>
            <ta e="T51" id="Seg_4678" s="T50">bult-LAr</ta>
            <ta e="T52" id="Seg_4679" s="T51">hɨrɨt-I-BIT</ta>
            <ta e="T53" id="Seg_4680" s="T52">hɨrɨː-LArI-n</ta>
            <ta e="T54" id="Seg_4681" s="T53">ol-nI</ta>
            <ta e="T55" id="Seg_4682" s="T54">kepseː-Ar-LAr</ta>
            <ta e="T56" id="Seg_4683" s="T55">oččogo</ta>
            <ta e="T57" id="Seg_4684" s="T56">bu͡ollagɨna</ta>
            <ta e="T58" id="Seg_4685" s="T57">hahɨl</ta>
            <ta e="T59" id="Seg_4686" s="T58">olor-BIT</ta>
            <ta e="T60" id="Seg_4687" s="T59">ühü</ta>
            <ta e="T61" id="Seg_4688" s="T60">bɨlɨr-bɨlɨr</ta>
            <ta e="T62" id="Seg_4689" s="T61">üje-GA</ta>
            <ta e="T63" id="Seg_4690" s="T62">bu</ta>
            <ta e="T64" id="Seg_4691" s="T63">hahɨl</ta>
            <ta e="T65" id="Seg_4692" s="T64">bu͡ollagɨna</ta>
            <ta e="T66" id="Seg_4693" s="T65">haːs</ta>
            <ta e="T67" id="Seg_4694" s="T66">biri͡eme-tA</ta>
            <ta e="T68" id="Seg_4695" s="T67">kel-An</ta>
            <ta e="T69" id="Seg_4696" s="T68">bu͡ol-TAK-InA</ta>
            <ta e="T70" id="Seg_4697" s="T69">törüt</ta>
            <ta e="T71" id="Seg_4698" s="T70">kantan</ta>
            <ta e="T72" id="Seg_4699" s="T71">da</ta>
            <ta e="T73" id="Seg_4700" s="T72">as</ta>
            <ta e="T74" id="Seg_4701" s="T73">ɨl-An</ta>
            <ta e="T75" id="Seg_4702" s="T74">hi͡e-Ar</ta>
            <ta e="T76" id="Seg_4703" s="T75">hir-tA</ta>
            <ta e="T77" id="Seg_4704" s="T76">hu͡ok</ta>
            <ta e="T78" id="Seg_4705" s="T77">kühüŋŋü</ta>
            <ta e="T79" id="Seg_4706" s="T78">haka</ta>
            <ta e="T80" id="Seg_4707" s="T79">as-tA</ta>
            <ta e="T81" id="Seg_4708" s="T80">baran-BIT</ta>
            <ta e="T82" id="Seg_4709" s="T81">bu͡ol-TI-tA</ta>
            <ta e="T83" id="Seg_4710" s="T82">haka</ta>
            <ta e="T84" id="Seg_4711" s="T83">as-LAN-BIT-tA</ta>
            <ta e="T85" id="Seg_4712" s="T84">uː</ta>
            <ta e="T86" id="Seg_4713" s="T85">kaːr</ta>
            <ta e="T87" id="Seg_4714" s="T86">bu͡ol-BIT</ta>
            <ta e="T88" id="Seg_4715" s="T87">innʼe</ta>
            <ta e="T89" id="Seg_4716" s="T88">gɨn-An</ta>
            <ta e="T90" id="Seg_4717" s="T89">as-tA</ta>
            <ta e="T91" id="Seg_4718" s="T90">hu͡ok</ta>
            <ta e="T92" id="Seg_4719" s="T91">bu͡ol-An</ta>
            <ta e="T93" id="Seg_4720" s="T92">korguj-An</ta>
            <ta e="T94" id="Seg_4721" s="T93">hir</ta>
            <ta e="T95" id="Seg_4722" s="T94">taŋara</ta>
            <ta e="T96" id="Seg_4723" s="T95">köt-An</ta>
            <ta e="T97" id="Seg_4724" s="T96">is-BIT-tA</ta>
            <ta e="T98" id="Seg_4725" s="T97">hahɨl</ta>
            <ta e="T99" id="Seg_4726" s="T98">bu</ta>
            <ta e="T100" id="Seg_4727" s="T99">köt-An</ta>
            <ta e="T101" id="Seg_4728" s="T100">is-An</ta>
            <ta e="T103" id="Seg_4729" s="T101">bu͡ollagɨna</ta>
            <ta e="T104" id="Seg_4730" s="T103">oː</ta>
            <ta e="T105" id="Seg_4731" s="T104">bu</ta>
            <ta e="T106" id="Seg_4732" s="T105">kantan</ta>
            <ta e="T107" id="Seg_4733" s="T106">bul-IAK-m=Ij</ta>
            <ta e="T108" id="Seg_4734" s="T107">di͡e-An</ta>
            <ta e="T109" id="Seg_4735" s="T108">mas</ta>
            <ta e="T110" id="Seg_4736" s="T109">is-tI-GAr</ta>
            <ta e="T111" id="Seg_4737" s="T110">kiːr-An</ta>
            <ta e="T112" id="Seg_4738" s="T111">mas</ta>
            <ta e="T113" id="Seg_4739" s="T112">is-tI-GAr</ta>
            <ta e="T114" id="Seg_4740" s="T113">köt-An</ta>
            <ta e="T115" id="Seg_4741" s="T114">is-Ar</ta>
            <ta e="T116" id="Seg_4742" s="T115">kajaː</ta>
            <ta e="T117" id="Seg_4743" s="T116">kör-Ar</ta>
            <ta e="T118" id="Seg_4744" s="T117">kantaj-ŋnAː-A-kantaj-ŋnAː-A</ta>
            <ta e="T119" id="Seg_4745" s="T118">köt-BIT-tA</ta>
            <ta e="T120" id="Seg_4746" s="T119">oruk</ta>
            <ta e="T121" id="Seg_4747" s="T120">tiːt</ta>
            <ta e="T122" id="Seg_4748" s="T121">töbö-tI-GAr</ta>
            <ta e="T123" id="Seg_4749" s="T122">har</ta>
            <ta e="T124" id="Seg_4750" s="T123">töröː-An</ta>
            <ta e="T125" id="Seg_4751" s="T124">baran</ta>
            <ta e="T126" id="Seg_4752" s="T125">hɨt-Ar</ta>
            <ta e="T127" id="Seg_4753" s="T126">kajaː</ta>
            <ta e="T128" id="Seg_4754" s="T127">doː</ta>
            <ta e="T129" id="Seg_4755" s="T128">onton</ta>
            <ta e="T130" id="Seg_4756" s="T129">min</ta>
            <ta e="T131" id="Seg_4757" s="T130">kajdak</ta>
            <ta e="T132" id="Seg_4758" s="T131">gɨn-An-BIn</ta>
            <ta e="T133" id="Seg_4759" s="T132">albun-LAN-An-BIn</ta>
            <ta e="T134" id="Seg_4760" s="T133">hi͡e-IAK-m=Ij</ta>
            <ta e="T135" id="Seg_4761" s="T134">iti</ta>
            <ta e="T136" id="Seg_4762" s="T135">har-ttAn</ta>
            <ta e="T137" id="Seg_4763" s="T136">tu͡ok-nI</ta>
            <ta e="T138" id="Seg_4764" s="T137">hɨmɨːt-TA</ta>
            <ta e="T139" id="Seg_4765" s="T138">di͡e-An</ta>
            <ta e="T140" id="Seg_4766" s="T139">bu</ta>
            <ta e="T141" id="Seg_4767" s="T140">kihi</ta>
            <ta e="T142" id="Seg_4768" s="T141">har-GA</ta>
            <ta e="T145" id="Seg_4769" s="T144">har-ttAn</ta>
            <ta e="T146" id="Seg_4770" s="T145">hɨmɨːt</ta>
            <ta e="T147" id="Seg_4771" s="T146">hi͡e-AːrI</ta>
            <ta e="T148" id="Seg_4772" s="T147">köt-An</ta>
            <ta e="T149" id="Seg_4773" s="T148">kel-An</ta>
            <ta e="T150" id="Seg_4774" s="T149">haːmaj</ta>
            <ta e="T151" id="Seg_4775" s="T150">kim-tI-n</ta>
            <ta e="T152" id="Seg_4776" s="T151">alɨn-tI-GAr</ta>
            <ta e="T153" id="Seg_4777" s="T152">kel-BIT</ta>
            <ta e="T154" id="Seg_4778" s="T153">tiːt-tI-n</ta>
            <ta e="T155" id="Seg_4779" s="T154">alɨn-tI-GAr</ta>
            <ta e="T156" id="Seg_4780" s="T155">kel-BIT</ta>
            <ta e="T157" id="Seg_4781" s="T156">innʼe</ta>
            <ta e="T158" id="Seg_4782" s="T157">gɨn-An</ta>
            <ta e="T159" id="Seg_4783" s="T158">baran</ta>
            <ta e="T160" id="Seg_4784" s="T159">kuturuk-tI-nAn</ta>
            <ta e="T161" id="Seg_4785" s="T160">tiːt-tI-n</ta>
            <ta e="T162" id="Seg_4786" s="T161">kɨrbaː-A</ta>
            <ta e="T163" id="Seg_4787" s="T162">tur-BIT-tA</ta>
            <ta e="T164" id="Seg_4788" s="T163">har</ta>
            <ta e="T165" id="Seg_4789" s="T164">har</ta>
            <ta e="T166" id="Seg_4790" s="T165">teheges</ta>
            <ta e="T167" id="Seg_4791" s="T166">kihi</ta>
            <ta e="T168" id="Seg_4792" s="T167">egel</ta>
            <ta e="T169" id="Seg_4793" s="T168">biːr</ta>
            <ta e="T170" id="Seg_4794" s="T169">ogo-GI-n</ta>
            <ta e="T171" id="Seg_4795" s="T170">min</ta>
            <ta e="T172" id="Seg_4796" s="T171">kuturuk-I-m</ta>
            <ta e="T173" id="Seg_4797" s="T172">nuːčča</ta>
            <ta e="T174" id="Seg_4798" s="T173">bolot-tI-TAːgAr</ta>
            <ta e="T175" id="Seg_4799" s="T174">hɨtɨː</ta>
            <ta e="T176" id="Seg_4800" s="T175">tiːt-LIːN</ta>
            <ta e="T177" id="Seg_4801" s="T176">bagas-LIːN</ta>
            <ta e="T178" id="Seg_4802" s="T177">bɨs-A</ta>
            <ta e="T179" id="Seg_4803" s="T178">ogus-An</ta>
            <ta e="T180" id="Seg_4804" s="T179">tüs-A-r-IAK-m</ta>
            <ta e="T181" id="Seg_4805" s="T180">beje-LIːN-GI-n</ta>
            <ta e="T182" id="Seg_4806" s="T181">hi͡e-IAK-m</ta>
            <ta e="T183" id="Seg_4807" s="T182">har</ta>
            <ta e="T184" id="Seg_4808" s="T183">bɨhɨn-IAK.[tA]</ta>
            <ta e="T185" id="Seg_4809" s="T184">du͡o</ta>
            <ta e="T186" id="Seg_4810" s="T185">ɨtaː-A-ɨtaː-A</ta>
            <ta e="T187" id="Seg_4811" s="T186">biːr</ta>
            <ta e="T188" id="Seg_4812" s="T187">ogo-tI-n</ta>
            <ta e="T189" id="Seg_4813" s="T188">čekenij-s</ta>
            <ta e="T190" id="Seg_4814" s="T189">gɨn-TAr-BIT</ta>
            <ta e="T191" id="Seg_4815" s="T190">biːr</ta>
            <ta e="T192" id="Seg_4816" s="T191">hɨmɨːt-tI-n</ta>
            <ta e="T193" id="Seg_4817" s="T192">hi͡e-BIT</ta>
            <ta e="T194" id="Seg_4818" s="T193">innʼe</ta>
            <ta e="T195" id="Seg_4819" s="T194">gɨn-An</ta>
            <ta e="T196" id="Seg_4820" s="T195">baran</ta>
            <ta e="T197" id="Seg_4821" s="T196">hir</ta>
            <ta e="T198" id="Seg_4822" s="T197">taŋara</ta>
            <ta e="T199" id="Seg_4823" s="T198">köt-An</ta>
            <ta e="T200" id="Seg_4824" s="T199">kaːl-BIT</ta>
            <ta e="T201" id="Seg_4825" s="T200">harsɨŋŋɨ-tI-n</ta>
            <ta e="T202" id="Seg_4826" s="T201">tur-Ar</ta>
            <ta e="T203" id="Seg_4827" s="T202">emi͡e</ta>
            <ta e="T204" id="Seg_4828" s="T203">har-nI</ta>
            <ta e="T205" id="Seg_4829" s="T204">albun-LAː</ta>
            <ta e="T206" id="Seg_4830" s="T205">har-tI-n</ta>
            <ta e="T207" id="Seg_4831" s="T206">albun-LAː-AːrI</ta>
            <ta e="T208" id="Seg_4832" s="T207">emi͡e</ta>
            <ta e="T209" id="Seg_4833" s="T208">köt-An</ta>
            <ta e="T210" id="Seg_4834" s="T209">kel-Ar</ta>
            <ta e="T211" id="Seg_4835" s="T210">har</ta>
            <ta e="T212" id="Seg_4836" s="T211">har</ta>
            <ta e="T213" id="Seg_4837" s="T212">teheges</ta>
            <ta e="T214" id="Seg_4838" s="T213">kihi</ta>
            <ta e="T215" id="Seg_4839" s="T214">egel</ta>
            <ta e="T216" id="Seg_4840" s="T215">biːr</ta>
            <ta e="T217" id="Seg_4841" s="T216">ogo-GI-n</ta>
            <ta e="T218" id="Seg_4842" s="T217">min</ta>
            <ta e="T219" id="Seg_4843" s="T218">kuturuk-I-m</ta>
            <ta e="T220" id="Seg_4844" s="T219">nuːčča</ta>
            <ta e="T221" id="Seg_4845" s="T220">bolot-tI-TAːgAr</ta>
            <ta e="T222" id="Seg_4846" s="T221">hɨtɨː</ta>
            <ta e="T223" id="Seg_4847" s="T222">tiːt-LIːN-GI-n-bagas-LIːN-GI-n</ta>
            <ta e="T224" id="Seg_4848" s="T223">bɨs-A</ta>
            <ta e="T225" id="Seg_4849" s="T224">ogus-An</ta>
            <ta e="T226" id="Seg_4850" s="T225">tüs-A-r-An-BIn</ta>
            <ta e="T227" id="Seg_4851" s="T226">anɨ</ta>
            <ta e="T228" id="Seg_4852" s="T227">beje-LIːN-GI-n</ta>
            <ta e="T229" id="Seg_4853" s="T228">hi͡e-IAK-m</ta>
            <ta e="T230" id="Seg_4854" s="T229">emi͡e</ta>
            <ta e="T231" id="Seg_4855" s="T230">biːr</ta>
            <ta e="T232" id="Seg_4856" s="T231">ogo-tI-n</ta>
            <ta e="T233" id="Seg_4857" s="T232">üt-An</ta>
            <ta e="T234" id="Seg_4858" s="T233">keːs-BIT</ta>
            <ta e="T235" id="Seg_4859" s="T234">ikki</ta>
            <ta e="T236" id="Seg_4860" s="T235">ogo-tI-n</ta>
            <ta e="T237" id="Seg_4861" s="T236">hi͡e-BIT</ta>
            <ta e="T238" id="Seg_4862" s="T237">hahɨl</ta>
            <ta e="T239" id="Seg_4863" s="T238">har</ta>
            <ta e="T240" id="Seg_4864" s="T239">ɨtaː-A</ta>
            <ta e="T241" id="Seg_4865" s="T240">olor-AːktAː-BIT</ta>
            <ta e="T242" id="Seg_4866" s="T241">diː</ta>
            <ta e="T243" id="Seg_4867" s="T242">doː</ta>
            <ta e="T244" id="Seg_4868" s="T243">bu</ta>
            <ta e="T245" id="Seg_4869" s="T244">anɨ</ta>
            <ta e="T246" id="Seg_4870" s="T245">bu͡ollagɨna</ta>
            <ta e="T247" id="Seg_4871" s="T246">beje-LIːN-BI-n</ta>
            <ta e="T248" id="Seg_4872" s="T247">hi͡e-An-nI</ta>
            <ta e="T249" id="Seg_4873" s="T248">di͡e-An</ta>
            <ta e="T250" id="Seg_4874" s="T249">ɨtaː-A</ta>
            <ta e="T251" id="Seg_4875" s="T250">olor-BIT</ta>
            <ta e="T252" id="Seg_4876" s="T251">kaja</ta>
            <ta e="T253" id="Seg_4877" s="T252">bu</ta>
            <ta e="T254" id="Seg_4878" s="T253">olor-TAK-InA</ta>
            <ta e="T255" id="Seg_4879" s="T254">kukaːkɨ</ta>
            <ta e="T256" id="Seg_4880" s="T255">köt-An</ta>
            <ta e="T257" id="Seg_4881" s="T256">kel-BIT</ta>
            <ta e="T258" id="Seg_4882" s="T257">kajaː</ta>
            <ta e="T259" id="Seg_4883" s="T258">har</ta>
            <ta e="T260" id="Seg_4884" s="T259">tu͡ok-ttAn</ta>
            <ta e="T261" id="Seg_4885" s="T260">ɨtaː-A-GIn</ta>
            <ta e="T262" id="Seg_4886" s="T261">keː</ta>
            <ta e="T263" id="Seg_4887" s="T262">en</ta>
            <ta e="T264" id="Seg_4888" s="T263">bu</ta>
            <ta e="T265" id="Seg_4889" s="T264">mannɨk</ta>
            <ta e="T266" id="Seg_4890" s="T265">bosku͡oj</ta>
            <ta e="T267" id="Seg_4891" s="T266">kün-LAːK</ta>
            <ta e="T268" id="Seg_4892" s="T267">kün</ta>
            <ta e="T269" id="Seg_4893" s="T268">tɨŋ-tA</ta>
            <ta e="T270" id="Seg_4894" s="T269">tur-TAK-InA</ta>
            <ta e="T271" id="Seg_4895" s="T270">ogo-LAr-I-ŋ</ta>
            <ta e="T272" id="Seg_4896" s="T271">da</ta>
            <ta e="T273" id="Seg_4897" s="T272">ičiges-GA</ta>
            <ta e="T274" id="Seg_4898" s="T273">hɨt-Ar-LAr</ta>
            <ta e="T275" id="Seg_4899" s="T274">beje-ŋ</ta>
            <ta e="T276" id="Seg_4900" s="T275">da</ta>
            <ta e="T277" id="Seg_4901" s="T276">as-nI</ta>
            <ta e="T278" id="Seg_4902" s="T277">bagas</ta>
            <ta e="T279" id="Seg_4903" s="T278">egel-An</ta>
            <ta e="T280" id="Seg_4904" s="T279">kel-Ar</ta>
            <ta e="T281" id="Seg_4905" s="T280">kördük</ta>
            <ta e="T282" id="Seg_4906" s="T281">bu͡o</ta>
            <ta e="T283" id="Seg_4907" s="T282">tu͡ok-ttAn</ta>
            <ta e="T284" id="Seg_4908" s="T283">ɨtaː-A-GIn</ta>
            <ta e="T285" id="Seg_4909" s="T284">dʼe</ta>
            <ta e="T286" id="Seg_4910" s="T285">mannɨk</ta>
            <ta e="T287" id="Seg_4911" s="T286">beseleː-GA</ta>
            <ta e="T288" id="Seg_4912" s="T287">olor-An-GIn</ta>
            <ta e="T289" id="Seg_4913" s="T288">di͡e-Ar</ta>
            <ta e="T290" id="Seg_4914" s="T289">en</ta>
            <ta e="T291" id="Seg_4915" s="T290">kajdak</ta>
            <ta e="T292" id="Seg_4916" s="T291">ɨtaː-A-t-BAT-GIn</ta>
            <ta e="T293" id="Seg_4917" s="T292">kör-BAT-GIn</ta>
            <ta e="T294" id="Seg_4918" s="T293">du͡o</ta>
            <ta e="T295" id="Seg_4919" s="T294">min-n</ta>
            <ta e="T296" id="Seg_4920" s="T295">bu͡ollagɨna</ta>
            <ta e="T297" id="Seg_4921" s="T296">hahɨl</ta>
            <ta e="T298" id="Seg_4922" s="T297">bu͡olla</ta>
            <ta e="T299" id="Seg_4923" s="T298">ikki</ta>
            <ta e="T300" id="Seg_4924" s="T299">kün-GA</ta>
            <ta e="T301" id="Seg_4925" s="T300">ikki</ta>
            <ta e="T302" id="Seg_4926" s="T301">ogo-BI-n</ta>
            <ta e="T303" id="Seg_4927" s="T302">hi͡e-TI-tA</ta>
            <ta e="T304" id="Seg_4928" s="T303">anɨ</ta>
            <ta e="T305" id="Seg_4929" s="T304">bügün</ta>
            <ta e="T306" id="Seg_4930" s="T305">emi͡e</ta>
            <ta e="T307" id="Seg_4931" s="T306">kel-IAK-tA</ta>
            <ta e="T308" id="Seg_4932" s="T307">hi͡e-IAK-tA</ta>
            <ta e="T309" id="Seg_4933" s="T308">tiːt-LIːN-BI-n</ta>
            <ta e="T310" id="Seg_4934" s="T309">bagas-LIːN-BI-n</ta>
            <ta e="T311" id="Seg_4935" s="T310">bɨs-A</ta>
            <ta e="T312" id="Seg_4936" s="T311">ogus-An</ta>
            <ta e="T313" id="Seg_4937" s="T312">tüs-A-r-An</ta>
            <ta e="T314" id="Seg_4938" s="T313">beje-LIːN-BI-n</ta>
            <ta e="T315" id="Seg_4939" s="T314">hi͡e-AːrI</ta>
            <ta e="T316" id="Seg_4940" s="T315">gɨn-TI-tA</ta>
            <ta e="T317" id="Seg_4941" s="T316">di͡e-Ar</ta>
            <ta e="T318" id="Seg_4942" s="T317">tu͡ok-LAːK=Ij</ta>
            <ta e="T319" id="Seg_4943" s="T318">ol</ta>
            <ta e="T320" id="Seg_4944" s="T319">hahɨl-I-ŋ</ta>
            <ta e="T321" id="Seg_4945" s="T320">kaja</ta>
            <ta e="T322" id="Seg_4946" s="T321">kuturuk-tA</ta>
            <ta e="T323" id="Seg_4947" s="T322">nuːčča</ta>
            <ta e="T324" id="Seg_4948" s="T323">bolot-tI-TAːgAr</ta>
            <ta e="T325" id="Seg_4949" s="T324">hɨtɨː</ta>
            <ta e="T326" id="Seg_4950" s="T325">doː</ta>
            <ta e="T327" id="Seg_4951" s="T326">tu͡ok</ta>
            <ta e="T328" id="Seg_4952" s="T327">aːt-LAːK-tA</ta>
            <ta e="T329" id="Seg_4953" s="T328">aŋɨr</ta>
            <ta e="T330" id="Seg_4954" s="T329">kötör-tA</ta>
            <ta e="T331" id="Seg_4955" s="T330">töröː-BIT-I-ŋ=Ij</ta>
            <ta e="T332" id="Seg_4956" s="T331">en</ta>
            <ta e="T333" id="Seg_4957" s="T332">mannɨk</ta>
            <ta e="T334" id="Seg_4958" s="T333">dʼe</ta>
            <ta e="T335" id="Seg_4959" s="T334">har</ta>
            <ta e="T336" id="Seg_4960" s="T335">kajdak</ta>
            <ta e="T337" id="Seg_4961" s="T336">gɨn-A</ta>
            <ta e="T338" id="Seg_4962" s="T337">hahɨl</ta>
            <ta e="T339" id="Seg_4963" s="T338">kajdak</ta>
            <ta e="T340" id="Seg_4964" s="T339">gɨn-An</ta>
            <ta e="T341" id="Seg_4965" s="T340">en</ta>
            <ta e="T342" id="Seg_4966" s="T341">tiːt-GI-n</ta>
            <ta e="T343" id="Seg_4967" s="T342">bɨs-A</ta>
            <ta e="T344" id="Seg_4968" s="T343">ogus-IAK.[tA]=Ij</ta>
            <ta e="T345" id="Seg_4969" s="T344">kuturuk-tA</ta>
            <ta e="T346" id="Seg_4970" s="T345">tüː</ta>
            <ta e="T347" id="Seg_4971" s="T346">tüː</ta>
            <ta e="T348" id="Seg_4972" s="T347">kuturuk</ta>
            <ta e="T349" id="Seg_4973" s="T348">tu͡ok-nI</ta>
            <ta e="T350" id="Seg_4974" s="T349">kot-IAK-ŋ=Ij</ta>
            <ta e="T351" id="Seg_4975" s="T350">di͡e</ta>
            <ta e="T352" id="Seg_4976" s="T351">anɨ</ta>
            <ta e="T353" id="Seg_4977" s="T352">kel-TAK-InA</ta>
            <ta e="T354" id="Seg_4978" s="T353">kim</ta>
            <ta e="T355" id="Seg_4979" s="T354">ü͡öret-BIT-tA=Ij</ta>
            <ta e="T356" id="Seg_4980" s="T355">di͡e-TAK-InA</ta>
            <ta e="T357" id="Seg_4981" s="T356">kukaːkɨ</ta>
            <ta e="T358" id="Seg_4982" s="T357">ü͡öret-BIT-tA</ta>
            <ta e="T359" id="Seg_4983" s="T358">di͡e-Aːr</ta>
            <ta e="T360" id="Seg_4984" s="T359">dʼe</ta>
            <ta e="T361" id="Seg_4985" s="T360">har</ta>
            <ta e="T364" id="Seg_4986" s="T363">har</ta>
            <ta e="T365" id="Seg_4987" s="T364">dʼe</ta>
            <ta e="T366" id="Seg_4988" s="T365">hi͡ese</ta>
            <ta e="T367" id="Seg_4989" s="T366">hek</ta>
            <ta e="T368" id="Seg_4990" s="T367">bu͡ol-BIT</ta>
            <ta e="T369" id="Seg_4991" s="T368">olor-BIT</ta>
            <ta e="T370" id="Seg_4992" s="T369">kukaːkɨ-tA</ta>
            <ta e="T371" id="Seg_4993" s="T370">köt-An</ta>
            <ta e="T372" id="Seg_4994" s="T371">kaːl-BIT</ta>
            <ta e="T373" id="Seg_4995" s="T372">oː</ta>
            <ta e="T374" id="Seg_4996" s="T373">dʼe</ta>
            <ta e="T375" id="Seg_4997" s="T374">hahɨl</ta>
            <ta e="T376" id="Seg_4998" s="T375">kɨjgan-AːrI</ta>
            <ta e="T377" id="Seg_4999" s="T376">kɨjgan-BIT</ta>
            <ta e="T378" id="Seg_5000" s="T377">bu͡ol-BIT</ta>
            <ta e="T379" id="Seg_5001" s="T378">kɨːhɨr-AːrI</ta>
            <ta e="T380" id="Seg_5002" s="T379">kɨːhɨr-BIT</ta>
            <ta e="T381" id="Seg_5003" s="T380">bu͡ol-BIT</ta>
            <ta e="T382" id="Seg_5004" s="T381">eŋin-eŋin</ta>
            <ta e="T383" id="Seg_5005" s="T382">köppök-nI</ta>
            <ta e="T384" id="Seg_5006" s="T383">tur-I-t-A</ta>
            <ta e="T385" id="Seg_5007" s="T384">tebi͡eleː-A-tebi͡eleː-A</ta>
            <ta e="T386" id="Seg_5008" s="T385">ilin</ta>
            <ta e="T387" id="Seg_5009" s="T386">kelin</ta>
            <ta e="T388" id="Seg_5010" s="T387">atak-tI-nAn</ta>
            <ta e="T389" id="Seg_5011" s="T388">kajɨt-A</ta>
            <ta e="T390" id="Seg_5012" s="T389">tebi͡eleː-A-tebi͡eleː-A</ta>
            <ta e="T391" id="Seg_5013" s="T390">kel-AːktAː-BIT</ta>
            <ta e="T392" id="Seg_5014" s="T391">diː</ta>
            <ta e="T393" id="Seg_5015" s="T392">har-tI-GA</ta>
            <ta e="T394" id="Seg_5016" s="T393">har-nI</ta>
            <ta e="T395" id="Seg_5017" s="T394">kuttaː-AːrI</ta>
            <ta e="T396" id="Seg_5018" s="T395">har</ta>
            <ta e="T397" id="Seg_5019" s="T396">har</ta>
            <ta e="T398" id="Seg_5020" s="T397">onnuk-mannɨk</ta>
            <ta e="T399" id="Seg_5021" s="T398">inʼe-LAːK</ta>
            <ta e="T400" id="Seg_5022" s="T399">kaja</ta>
            <ta e="T401" id="Seg_5023" s="T400">min</ta>
            <ta e="T402" id="Seg_5024" s="T401">bu͡ollagɨna</ta>
            <ta e="T403" id="Seg_5025" s="T402">emi͡e</ta>
            <ta e="T404" id="Seg_5026" s="T403">bu͡opsa</ta>
            <ta e="T405" id="Seg_5027" s="T404">kɨjgan-AːrI</ta>
            <ta e="T406" id="Seg_5028" s="T405">kɨjgan-TI-m</ta>
            <ta e="T407" id="Seg_5029" s="T406">egel</ta>
            <ta e="T408" id="Seg_5030" s="T407">biːr</ta>
            <ta e="T409" id="Seg_5031" s="T408">ogo-TA</ta>
            <ta e="T410" id="Seg_5032" s="T409">min</ta>
            <ta e="T411" id="Seg_5033" s="T410">kuturuk-I-m</ta>
            <ta e="T412" id="Seg_5034" s="T411">nuːčča</ta>
            <ta e="T413" id="Seg_5035" s="T412">bolot-tI-TAːgAr</ta>
            <ta e="T414" id="Seg_5036" s="T413">hɨtɨː</ta>
            <ta e="T415" id="Seg_5037" s="T414">hannɨ</ta>
            <ta e="T416" id="Seg_5038" s="T415">beje-LIːN-GI-n</ta>
            <ta e="T419" id="Seg_5039" s="T418">tuːra</ta>
            <ta e="T420" id="Seg_5040" s="T419">ogus-An-BIn</ta>
            <ta e="T421" id="Seg_5041" s="T420">beje-LIːN-GI-n</ta>
            <ta e="T422" id="Seg_5042" s="T421">hi͡e-IAK-m</ta>
            <ta e="T423" id="Seg_5043" s="T422">onnoːgor</ta>
            <ta e="T424" id="Seg_5044" s="T423">koː</ta>
            <ta e="T425" id="Seg_5045" s="T424">aŋɨr-LAː-An-BIn</ta>
            <ta e="T426" id="Seg_5046" s="T425">bu͡opsa</ta>
            <ta e="T430" id="Seg_5047" s="T429">kaːrɨ͡an</ta>
            <ta e="T431" id="Seg_5048" s="T430">ikki</ta>
            <ta e="T432" id="Seg_5049" s="T431">ogo-BI-n</ta>
            <ta e="T433" id="Seg_5050" s="T432">hi͡e-t-TI-m</ta>
            <ta e="T434" id="Seg_5051" s="T433">min</ta>
            <ta e="T435" id="Seg_5052" s="T434">en-GA</ta>
            <ta e="T436" id="Seg_5053" s="T435">beje-m</ta>
            <ta e="T437" id="Seg_5054" s="T436">aŋɨr-BA-r</ta>
            <ta e="T438" id="Seg_5055" s="T437">en</ta>
            <ta e="T439" id="Seg_5056" s="T438">bu͡ollagɨna</ta>
            <ta e="T440" id="Seg_5057" s="T439">tüː</ta>
            <ta e="T441" id="Seg_5058" s="T440">kuturuk-GIn</ta>
            <ta e="T442" id="Seg_5059" s="T441">diː</ta>
            <ta e="T443" id="Seg_5060" s="T442">tüː</ta>
            <ta e="T444" id="Seg_5061" s="T443">kuturuk</ta>
            <ta e="T445" id="Seg_5062" s="T444">tu͡ok-nI</ta>
            <ta e="T446" id="Seg_5063" s="T445">kot-IAK-ŋ=Ij</ta>
            <ta e="T447" id="Seg_5064" s="T446">mas-nI</ta>
            <ta e="T448" id="Seg_5065" s="T447">kot-IAK-ŋ</ta>
            <ta e="T449" id="Seg_5066" s="T448">du͡o</ta>
            <ta e="T450" id="Seg_5067" s="T449">di͡e-Ar</ta>
            <ta e="T451" id="Seg_5068" s="T450">koː</ta>
            <ta e="T452" id="Seg_5069" s="T451">arakata</ta>
            <ta e="T453" id="Seg_5070" s="T452">tu͡ok</ta>
            <ta e="T454" id="Seg_5071" s="T453">aːt-LAːK-tA</ta>
            <ta e="T455" id="Seg_5072" s="T454">öj</ta>
            <ta e="T456" id="Seg_5073" s="T455">bi͡er-BIT-tA=Ij</ta>
            <ta e="T457" id="Seg_5074" s="T456">en-GA</ta>
            <ta e="T458" id="Seg_5075" s="T457">kajdak</ta>
            <ta e="T459" id="Seg_5076" s="T458">gɨn-An-GIn</ta>
            <ta e="T460" id="Seg_5077" s="T459">öj-nI</ta>
            <ta e="T461" id="Seg_5078" s="T460">bul-BIT-I-ŋ=Ij</ta>
            <ta e="T462" id="Seg_5079" s="T461">di͡e-Ar</ta>
            <ta e="T463" id="Seg_5080" s="T462">mini͡ene</ta>
            <ta e="T464" id="Seg_5081" s="T463">kuturuk-I-m</ta>
            <ta e="T465" id="Seg_5082" s="T464">tüː-tI-n</ta>
            <ta e="T466" id="Seg_5083" s="T465">kantan</ta>
            <ta e="T467" id="Seg_5084" s="T466">bil-A-GIn</ta>
            <ta e="T468" id="Seg_5085" s="T467">kaja</ta>
            <ta e="T469" id="Seg_5086" s="T468">min-n</ta>
            <ta e="T470" id="Seg_5087" s="T469">kukaːkɨ</ta>
            <ta e="T471" id="Seg_5088" s="T470">ü͡öret-BIT-tA</ta>
            <ta e="T472" id="Seg_5089" s="T471">kim</ta>
            <ta e="T473" id="Seg_5090" s="T472">bi͡er-IAK.[tA]=Ij</ta>
            <ta e="T474" id="Seg_5091" s="T473">min-GA</ta>
            <ta e="T475" id="Seg_5092" s="T474">öj</ta>
            <ta e="T476" id="Seg_5093" s="T475">kukaːkɨ</ta>
            <ta e="T477" id="Seg_5094" s="T476">ü͡öret-BIT-tA</ta>
            <ta e="T478" id="Seg_5095" s="T477">kajaː</ta>
            <ta e="T479" id="Seg_5096" s="T478">onnuk-mannɨk</ta>
            <ta e="T480" id="Seg_5097" s="T479">otto</ta>
            <ta e="T481" id="Seg_5098" s="T480">kukaːkɨ-nI</ta>
            <ta e="T482" id="Seg_5099" s="T481">kaja</ta>
            <ta e="T483" id="Seg_5100" s="T482">hir-ttAn</ta>
            <ta e="T484" id="Seg_5101" s="T483">bul-IAK-m=Ij</ta>
            <ta e="T485" id="Seg_5102" s="T484">dʼe</ta>
            <ta e="T486" id="Seg_5103" s="T485">beːbe</ta>
            <ta e="T487" id="Seg_5104" s="T486">dʼe</ta>
            <ta e="T488" id="Seg_5105" s="T487">u͡oldʼas-IAK-m</ta>
            <ta e="T489" id="Seg_5106" s="T488">min</ta>
            <ta e="T490" id="Seg_5107" s="T489">gini-GA</ta>
            <ta e="T491" id="Seg_5108" s="T490">daːganɨ</ta>
            <ta e="T492" id="Seg_5109" s="T491">innʼe</ta>
            <ta e="T493" id="Seg_5110" s="T492">gɨn-BIT</ta>
            <ta e="T494" id="Seg_5111" s="T493">da</ta>
            <ta e="T495" id="Seg_5112" s="T494">hir</ta>
            <ta e="T496" id="Seg_5113" s="T495">taŋara</ta>
            <ta e="T497" id="Seg_5114" s="T496">köt-An</ta>
            <ta e="T498" id="Seg_5115" s="T497">kaːl-BIT</ta>
            <ta e="T499" id="Seg_5116" s="T498">hir</ta>
            <ta e="T500" id="Seg_5117" s="T499">taŋara</ta>
            <ta e="T501" id="Seg_5118" s="T500">köt-An</ta>
            <ta e="T502" id="Seg_5119" s="T501">is-An</ta>
            <ta e="T503" id="Seg_5120" s="T502">bu͡ol-TI-tA</ta>
            <ta e="T504" id="Seg_5121" s="T503">[C^1][V^1][C^2]-tögürük</ta>
            <ta e="T505" id="Seg_5122" s="T504">kü͡öl-GA</ta>
            <ta e="T507" id="Seg_5123" s="T505">tij-An</ta>
            <ta e="T508" id="Seg_5124" s="T507">bu</ta>
            <ta e="T509" id="Seg_5125" s="T508">kü͡öl</ta>
            <ta e="T510" id="Seg_5126" s="T509">tu͡ok-ttAn</ta>
            <ta e="T511" id="Seg_5127" s="T510">da</ta>
            <ta e="T512" id="Seg_5128" s="T511">ulakan</ta>
            <ta e="T513" id="Seg_5129" s="T512">kaja-LAːK</ta>
            <ta e="T514" id="Seg_5130" s="T513">haːs</ta>
            <ta e="T515" id="Seg_5131" s="T514">kem-tI-GAr</ta>
            <ta e="T516" id="Seg_5132" s="T515">bu͡ol-TI-tA</ta>
            <ta e="T517" id="Seg_5133" s="T516">keriske-tA</ta>
            <ta e="T518" id="Seg_5134" s="T517">barɨta</ta>
            <ta e="T519" id="Seg_5135" s="T518">tagɨs-I-BIT</ta>
            <ta e="T520" id="Seg_5136" s="T519">bu</ta>
            <ta e="T521" id="Seg_5137" s="T520">keriske-nI</ta>
            <ta e="T522" id="Seg_5138" s="T521">bu͡ollagɨna</ta>
            <ta e="T523" id="Seg_5139" s="T522">köt-A</ta>
            <ta e="T524" id="Seg_5140" s="T523">hɨrɨt-An</ta>
            <ta e="T525" id="Seg_5141" s="T524">bu͡opsa</ta>
            <ta e="T526" id="Seg_5142" s="T525">muŋ</ta>
            <ta e="T527" id="Seg_5143" s="T526">bosku͡oj</ta>
            <ta e="T528" id="Seg_5144" s="T527">kadʼɨrɨk-GA</ta>
            <ta e="T529" id="Seg_5145" s="T528">bu͡ollagɨna</ta>
            <ta e="T530" id="Seg_5146" s="T529">öl-BIT</ta>
            <ta e="T531" id="Seg_5147" s="T530">hahɨl</ta>
            <ta e="T532" id="Seg_5148" s="T531">bu͡ol-An</ta>
            <ta e="T533" id="Seg_5149" s="T532">baran</ta>
            <ta e="T534" id="Seg_5150" s="T533">nelej-s</ta>
            <ta e="T535" id="Seg_5151" s="T534">gɨn-A</ta>
            <ta e="T536" id="Seg_5152" s="T535">tüs-BIT</ta>
            <ta e="T537" id="Seg_5153" s="T536">bu͡opsa</ta>
            <ta e="T538" id="Seg_5154" s="T537">ü͡ön-GA</ta>
            <ta e="T539" id="Seg_5155" s="T538">barɨ-tI-GAr</ta>
            <ta e="T540" id="Seg_5156" s="T539">ɨs-TAr-A</ta>
            <ta e="T541" id="Seg_5157" s="T540">hɨt-BIT</ta>
            <ta e="T542" id="Seg_5158" s="T541">ol</ta>
            <ta e="T543" id="Seg_5159" s="T542">gɨn-An</ta>
            <ta e="T544" id="Seg_5160" s="T543">baran</ta>
            <ta e="T545" id="Seg_5161" s="T544">kajaː</ta>
            <ta e="T546" id="Seg_5162" s="T545">kukaːkɨ-LAr</ta>
            <ta e="T547" id="Seg_5163" s="T546">daːganɨ</ta>
            <ta e="T548" id="Seg_5164" s="T547">köt-A</ta>
            <ta e="T549" id="Seg_5165" s="T548">hɨrɨt-An</ta>
            <ta e="T550" id="Seg_5166" s="T549">er-An</ta>
            <ta e="T551" id="Seg_5167" s="T550">kör-Ar</ta>
            <ta e="T552" id="Seg_5168" s="T551">hahɨl</ta>
            <ta e="T553" id="Seg_5169" s="T552">öl-An</ta>
            <ta e="T554" id="Seg_5170" s="T553">baran</ta>
            <ta e="T555" id="Seg_5171" s="T554">nelej-An</ta>
            <ta e="T556" id="Seg_5172" s="T555">hɨt-Ar</ta>
            <ta e="T557" id="Seg_5173" s="T556">oː</ta>
            <ta e="T558" id="Seg_5174" s="T557">bu</ta>
            <ta e="T559" id="Seg_5175" s="T558">turkarɨ</ta>
            <ta e="T560" id="Seg_5176" s="T559">bu</ta>
            <ta e="T561" id="Seg_5177" s="T560">korguj-An</ta>
            <ta e="T562" id="Seg_5178" s="T561">bu͡ollagɨna</ta>
            <ta e="T563" id="Seg_5179" s="T562">har</ta>
            <ta e="T564" id="Seg_5180" s="T563">ogo-LAr-tI-n</ta>
            <ta e="T565" id="Seg_5181" s="T564">hi͡e-A</ta>
            <ta e="T566" id="Seg_5182" s="T565">hɨrɨt-I-BIT-tA</ta>
            <ta e="T567" id="Seg_5183" s="T566">bu͡olla</ta>
            <ta e="T568" id="Seg_5184" s="T567">anɨ</ta>
            <ta e="T569" id="Seg_5185" s="T568">har-tA</ta>
            <ta e="T570" id="Seg_5186" s="T569">bu͡ollagɨna</ta>
            <ta e="T571" id="Seg_5187" s="T570">min-ttAn</ta>
            <ta e="T572" id="Seg_5188" s="T571">öj</ta>
            <ta e="T573" id="Seg_5189" s="T572">ɨl-Ar</ta>
            <ta e="T574" id="Seg_5190" s="T573">biːr</ta>
            <ta e="T575" id="Seg_5191" s="T574">da</ta>
            <ta e="T576" id="Seg_5192" s="T575">ogo-tI-n</ta>
            <ta e="T577" id="Seg_5193" s="T576">bi͡er-BAtAK</ta>
            <ta e="T578" id="Seg_5194" s="T577">oččogo</ta>
            <ta e="T579" id="Seg_5195" s="T578">kantan</ta>
            <ta e="T580" id="Seg_5196" s="T579">ɨl-An</ta>
            <ta e="T581" id="Seg_5197" s="T580">gini</ta>
            <ta e="T582" id="Seg_5198" s="T581">daːganɨ</ta>
            <ta e="T583" id="Seg_5199" s="T582">korguj-BIT</ta>
            <ta e="T584" id="Seg_5200" s="T583">kihi</ta>
            <ta e="T585" id="Seg_5201" s="T584">kantan</ta>
            <ta e="T586" id="Seg_5202" s="T585">ɨl-An</ta>
            <ta e="T587" id="Seg_5203" s="T586">ahaː-IAK.[tA]=Ij</ta>
            <ta e="T588" id="Seg_5204" s="T587">dʼe</ta>
            <ta e="T589" id="Seg_5205" s="T588">ogut-An</ta>
            <ta e="T590" id="Seg_5206" s="T589">öl-BIT</ta>
            <ta e="T591" id="Seg_5207" s="T590">dʼe</ta>
            <ta e="T592" id="Seg_5208" s="T591">en</ta>
            <ta e="T593" id="Seg_5209" s="T592">daːganɨ</ta>
            <ta e="T594" id="Seg_5210" s="T593">öl-Ar-LAːK-GIn</ta>
            <ta e="T595" id="Seg_5211" s="T594">e-BIT</ta>
            <ta e="T596" id="Seg_5212" s="T595">di͡e-An</ta>
            <ta e="T597" id="Seg_5213" s="T596">kaja</ta>
            <ta e="T598" id="Seg_5214" s="T597">kel-BIT</ta>
            <ta e="T599" id="Seg_5215" s="T598">loŋ-loŋ</ta>
            <ta e="T600" id="Seg_5216" s="T599">loŋ-loŋ</ta>
            <ta e="T601" id="Seg_5217" s="T600">loŋsuj-BIT</ta>
            <ta e="T602" id="Seg_5218" s="T601">bu͡o</ta>
            <ta e="T603" id="Seg_5219" s="T602">kantɨttan</ta>
            <ta e="T604" id="Seg_5220" s="T603">karak-tI-ttAn</ta>
            <ta e="T605" id="Seg_5221" s="T604">hi͡e-IAK-GA</ta>
            <ta e="T606" id="Seg_5222" s="T605">di͡e-An</ta>
            <ta e="T607" id="Seg_5223" s="T606">karɨ-GA</ta>
            <ta e="T608" id="Seg_5224" s="T607">loŋsuj-n-A</ta>
            <ta e="T609" id="Seg_5225" s="T608">hɨrɨt-TAK-InA</ta>
            <ta e="T610" id="Seg_5226" s="T609">bu͡opsa</ta>
            <ta e="T611" id="Seg_5227" s="T682">hahɨl</ta>
            <ta e="T612" id="Seg_5228" s="T611">bu͡ollagɨna</ta>
            <ta e="T613" id="Seg_5229" s="T612">ele</ta>
            <ta e="T614" id="Seg_5230" s="T613">nʼɨma-tI-nAn</ta>
            <ta e="T615" id="Seg_5231" s="T614">ele</ta>
            <ta e="T616" id="Seg_5232" s="T615">kütür-tI-nAn</ta>
            <ta e="T617" id="Seg_5233" s="T616">ɨrdʼɨgɨnaː-A</ta>
            <ta e="T618" id="Seg_5234" s="T617">gɨn-A</ta>
            <ta e="T619" id="Seg_5235" s="T618">ɨstan-AːktAː-BIT</ta>
            <ta e="T621" id="Seg_5236" s="T619">diː</ta>
            <ta e="T622" id="Seg_5237" s="T621">baːgɨ</ta>
            <ta e="T623" id="Seg_5238" s="T622">har</ta>
            <ta e="T624" id="Seg_5239" s="T623">bu͡ollagɨna</ta>
            <ta e="T625" id="Seg_5240" s="T624">kim</ta>
            <ta e="T626" id="Seg_5241" s="T625">bu͡ollagɨna</ta>
            <ta e="T627" id="Seg_5242" s="T626">kukaːkɨ</ta>
            <ta e="T628" id="Seg_5243" s="T627">bu͡ollagɨna</ta>
            <ta e="T629" id="Seg_5244" s="T628">kü͡örejen-An</ta>
            <ta e="T630" id="Seg_5245" s="T629">üspü͡öj-LAː-BAkkA</ta>
            <ta e="T631" id="Seg_5246" s="T630">kuturuk-tI-ttAn</ta>
            <ta e="T632" id="Seg_5247" s="T631">kap-TAr-BIT</ta>
            <ta e="T633" id="Seg_5248" s="T632">innʼe</ta>
            <ta e="T634" id="Seg_5249" s="T633">gɨn-An</ta>
            <ta e="T635" id="Seg_5250" s="T634">bu-nI</ta>
            <ta e="T636" id="Seg_5251" s="T635">haŋastaː-AlAː-A-s-An</ta>
            <ta e="T637" id="Seg_5252" s="T636">haŋastaː-A-s-An</ta>
            <ta e="T638" id="Seg_5253" s="T637">uː</ta>
            <ta e="T639" id="Seg_5254" s="T638">is-tI-GAr</ta>
            <ta e="T640" id="Seg_5255" s="T639">čekenij-t-An</ta>
            <ta e="T641" id="Seg_5256" s="T640">tüs-A-r-An</ta>
            <ta e="T642" id="Seg_5257" s="T641">uː</ta>
            <ta e="T645" id="Seg_5258" s="T644">uː</ta>
            <ta e="T646" id="Seg_5259" s="T645">is-tI-GAr</ta>
            <ta e="T647" id="Seg_5260" s="T646">tüs-A-r-An</ta>
            <ta e="T648" id="Seg_5261" s="T647">bu-GA</ta>
            <ta e="T649" id="Seg_5262" s="T648">bulkuj-An</ta>
            <ta e="T650" id="Seg_5263" s="T649">hiŋe-LAːK</ta>
            <ta e="T651" id="Seg_5264" s="T650">uː-GA</ta>
            <ta e="T652" id="Seg_5265" s="T651">bulkuj-An</ta>
            <ta e="T653" id="Seg_5266" s="T652">tumnar-An</ta>
            <ta e="T654" id="Seg_5267" s="T653">mülčü</ta>
            <ta e="T655" id="Seg_5268" s="T654">tut-TAr-An</ta>
            <ta e="T656" id="Seg_5269" s="T655">köt-An</ta>
            <ta e="T657" id="Seg_5270" s="T656">kaːl-BIT</ta>
            <ta e="T658" id="Seg_5271" s="T657">dʼe</ta>
            <ta e="T659" id="Seg_5272" s="T658">ol</ta>
            <ta e="T660" id="Seg_5273" s="T659">hahɨl</ta>
            <ta e="T661" id="Seg_5274" s="T660">bu͡ollagɨna</ta>
            <ta e="T662" id="Seg_5275" s="T661">kɨtɨl-GA</ta>
            <ta e="T663" id="Seg_5276" s="T662">tagɨs-An</ta>
            <ta e="T664" id="Seg_5277" s="T663">kuːr-LIN-A</ta>
            <ta e="T665" id="Seg_5278" s="T664">ɨrata</ta>
            <ta e="T666" id="Seg_5279" s="T665">eni</ta>
            <ta e="T667" id="Seg_5280" s="T666">dʼe</ta>
            <ta e="T668" id="Seg_5281" s="T667">ol</ta>
            <ta e="T669" id="Seg_5282" s="T668">kuːr-LIN-An-kanʼaː-An</ta>
            <ta e="T671" id="Seg_5283" s="T669">baran</ta>
            <ta e="T672" id="Seg_5284" s="T671">tilin-An</ta>
            <ta e="T673" id="Seg_5285" s="T672">baːj-An-tot-An</ta>
            <ta e="T674" id="Seg_5286" s="T673">olor-BIT-tA</ta>
            <ta e="T675" id="Seg_5287" s="T674">du͡o</ta>
            <ta e="T676" id="Seg_5288" s="T675">kajdi͡ek</ta>
            <ta e="T677" id="Seg_5289" s="T676">bu͡ol-BIT-tA</ta>
            <ta e="T678" id="Seg_5290" s="T677">du͡o</ta>
            <ta e="T679" id="Seg_5291" s="T678">dʼe</ta>
            <ta e="T680" id="Seg_5292" s="T679">oloŋko-BIt</ta>
            <ta e="T681" id="Seg_5293" s="T680">ele-tA</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_5294" s="T0">this</ta>
            <ta e="T2" id="Seg_5295" s="T1">before</ta>
            <ta e="T3" id="Seg_5296" s="T2">small.[NOM]</ta>
            <ta e="T4" id="Seg_5297" s="T3">be-PTCP.COND-1PL-ABL</ta>
            <ta e="T5" id="Seg_5298" s="T4">age-ABL</ta>
            <ta e="T6" id="Seg_5299" s="T5">tale-VBZ-EP-RECP/COLL-CVB.SEQ</ta>
            <ta e="T7" id="Seg_5300" s="T6">arise-PST2-1PL</ta>
            <ta e="T8" id="Seg_5301" s="T7">Q</ta>
            <ta e="T9" id="Seg_5302" s="T8">that</ta>
            <ta e="T10" id="Seg_5303" s="T9">this</ta>
            <ta e="T11" id="Seg_5304" s="T10">1PL.[NOM]</ta>
            <ta e="T12" id="Seg_5305" s="T11">small.[NOM]</ta>
            <ta e="T13" id="Seg_5306" s="T12">be-PTCP.COND-1SG-ABL</ta>
            <ta e="T14" id="Seg_5307" s="T13">what.[NOM]</ta>
            <ta e="T15" id="Seg_5308" s="T14">NEG</ta>
            <ta e="T16" id="Seg_5309" s="T15">play-NMNZ.[NOM]</ta>
            <ta e="T17" id="Seg_5310" s="T16">NEG.EX</ta>
            <ta e="T18" id="Seg_5311" s="T17">be-PST1-3SG</ta>
            <ta e="T19" id="Seg_5312" s="T18">this.[NOM]</ta>
            <ta e="T20" id="Seg_5313" s="T19">child-PL-ACC</ta>
            <ta e="T21" id="Seg_5314" s="T20">sleep-CAUS-EP-NEG-CVB.PURP-3PL</ta>
            <ta e="T22" id="Seg_5315" s="T21">and.so.on-NEG-CVB.PURP-3PL</ta>
            <ta e="T23" id="Seg_5316" s="T22">this.[NOM]</ta>
            <ta e="T24" id="Seg_5317" s="T23">father-1PL-PROPR.[NOM]</ta>
            <ta e="T25" id="Seg_5318" s="T24">father-1PL.[NOM]</ta>
            <ta e="T26" id="Seg_5319" s="T25">father-3SG-PROPR.[NOM]</ta>
            <ta e="T27" id="Seg_5320" s="T26">child-DAT/LOC</ta>
            <ta e="T28" id="Seg_5321" s="T27">tale-VBZ-PTCP.PRS</ta>
            <ta e="T29" id="Seg_5322" s="T28">tale-3PL.[NOM]</ta>
            <ta e="T30" id="Seg_5323" s="T29">be-PST1-3SG</ta>
            <ta e="T31" id="Seg_5324" s="T30">especially</ta>
            <ta e="T32" id="Seg_5325" s="T31">child.[NOM]</ta>
            <ta e="T33" id="Seg_5326" s="T32">tale-3SG.[NOM]</ta>
            <ta e="T34" id="Seg_5327" s="T33">say-CVB.SEQ</ta>
            <ta e="T35" id="Seg_5328" s="T34">long.ago</ta>
            <ta e="T36" id="Seg_5329" s="T35">earth.[NOM]</ta>
            <ta e="T37" id="Seg_5330" s="T36">sky.[NOM]</ta>
            <ta e="T38" id="Seg_5331" s="T37">arise-PTCP.PRS-3SG-ACC</ta>
            <ta e="T39" id="Seg_5332" s="T38">when</ta>
            <ta e="T40" id="Seg_5333" s="T39">every-3SG.[NOM]</ta>
            <ta e="T41" id="Seg_5334" s="T40">language-PROPR.[NOM]</ta>
            <ta e="T42" id="Seg_5335" s="T41">be-PST1-3PL</ta>
            <ta e="T43" id="Seg_5336" s="T42">it.is.said</ta>
            <ta e="T44" id="Seg_5337" s="T43">animal-PL.[NOM]</ta>
            <ta e="T45" id="Seg_5338" s="T44">every-3PL.[NOM]</ta>
            <ta e="T46" id="Seg_5339" s="T45">that</ta>
            <ta e="T47" id="Seg_5340" s="T46">animal-3PL.[NOM]</ta>
            <ta e="T48" id="Seg_5341" s="T47">eh</ta>
            <ta e="T49" id="Seg_5342" s="T48">story-3PL.[NOM]</ta>
            <ta e="T50" id="Seg_5343" s="T49">that</ta>
            <ta e="T51" id="Seg_5344" s="T50">animal-PL.[NOM]</ta>
            <ta e="T52" id="Seg_5345" s="T51">go-EP-PTCP.PST</ta>
            <ta e="T53" id="Seg_5346" s="T52">trip-3PL-ACC</ta>
            <ta e="T54" id="Seg_5347" s="T53">that-ACC</ta>
            <ta e="T55" id="Seg_5348" s="T54">tell-PRS-3PL</ta>
            <ta e="T56" id="Seg_5349" s="T55">then</ta>
            <ta e="T57" id="Seg_5350" s="T56">though</ta>
            <ta e="T58" id="Seg_5351" s="T57">fox.[NOM]</ta>
            <ta e="T59" id="Seg_5352" s="T58">live-PST2.[3SG]</ta>
            <ta e="T60" id="Seg_5353" s="T59">it.is.said</ta>
            <ta e="T61" id="Seg_5354" s="T60">long.ago-long.ago</ta>
            <ta e="T62" id="Seg_5355" s="T61">time-DAT/LOC</ta>
            <ta e="T63" id="Seg_5356" s="T62">this</ta>
            <ta e="T64" id="Seg_5357" s="T63">fox.[NOM]</ta>
            <ta e="T65" id="Seg_5358" s="T64">though</ta>
            <ta e="T66" id="Seg_5359" s="T65">spring.[NOM]</ta>
            <ta e="T67" id="Seg_5360" s="T66">time-3SG.[NOM]</ta>
            <ta e="T68" id="Seg_5361" s="T67">come-CVB.SEQ</ta>
            <ta e="T69" id="Seg_5362" s="T68">be-TEMP-3SG</ta>
            <ta e="T70" id="Seg_5363" s="T69">at.all</ta>
            <ta e="T71" id="Seg_5364" s="T70">where.from</ta>
            <ta e="T72" id="Seg_5365" s="T71">NEG</ta>
            <ta e="T73" id="Seg_5366" s="T72">food.[NOM]</ta>
            <ta e="T74" id="Seg_5367" s="T73">take-CVB.SEQ</ta>
            <ta e="T75" id="Seg_5368" s="T74">eat-PTCP.PRS</ta>
            <ta e="T76" id="Seg_5369" s="T75">place-3SG.[NOM]</ta>
            <ta e="T77" id="Seg_5370" s="T76">NEG.EX</ta>
            <ta e="T78" id="Seg_5371" s="T77">autumnal</ta>
            <ta e="T79" id="Seg_5372" s="T78">Dolgan</ta>
            <ta e="T80" id="Seg_5373" s="T79">food-3SG.[NOM]</ta>
            <ta e="T81" id="Seg_5374" s="T80">end-PTCP.PST</ta>
            <ta e="T82" id="Seg_5375" s="T81">be-PST1-3SG</ta>
            <ta e="T83" id="Seg_5376" s="T82">Dolgan</ta>
            <ta e="T84" id="Seg_5377" s="T83">food-VBZ-PTCP.PST-3SG.[NOM]</ta>
            <ta e="T85" id="Seg_5378" s="T84">water.[NOM]</ta>
            <ta e="T86" id="Seg_5379" s="T85">snow.[NOM]</ta>
            <ta e="T87" id="Seg_5380" s="T86">become-PST2.[3SG]</ta>
            <ta e="T88" id="Seg_5381" s="T87">so</ta>
            <ta e="T89" id="Seg_5382" s="T88">make-CVB.SEQ</ta>
            <ta e="T90" id="Seg_5383" s="T89">food-POSS</ta>
            <ta e="T91" id="Seg_5384" s="T90">NEG</ta>
            <ta e="T92" id="Seg_5385" s="T91">be-CVB.SEQ</ta>
            <ta e="T93" id="Seg_5386" s="T92">hunger-CVB.SEQ</ta>
            <ta e="T94" id="Seg_5387" s="T93">earth.[NOM]</ta>
            <ta e="T95" id="Seg_5388" s="T94">sky.[NOM]</ta>
            <ta e="T96" id="Seg_5389" s="T95">run-CVB.SEQ</ta>
            <ta e="T97" id="Seg_5390" s="T96">go-PST2-3SG</ta>
            <ta e="T98" id="Seg_5391" s="T97">fox.[NOM]</ta>
            <ta e="T99" id="Seg_5392" s="T98">this</ta>
            <ta e="T100" id="Seg_5393" s="T99">run-CVB.SEQ</ta>
            <ta e="T101" id="Seg_5394" s="T100">go-CVB.SEQ</ta>
            <ta e="T103" id="Seg_5395" s="T101">though</ta>
            <ta e="T104" id="Seg_5396" s="T103">oh</ta>
            <ta e="T105" id="Seg_5397" s="T104">this</ta>
            <ta e="T106" id="Seg_5398" s="T105">where.from</ta>
            <ta e="T107" id="Seg_5399" s="T106">find-FUT-1SG=Q</ta>
            <ta e="T108" id="Seg_5400" s="T107">think-CVB.SEQ</ta>
            <ta e="T109" id="Seg_5401" s="T108">forest.[NOM]</ta>
            <ta e="T110" id="Seg_5402" s="T109">inside-3SG-DAT/LOC</ta>
            <ta e="T111" id="Seg_5403" s="T110">go.in-CVB.SEQ</ta>
            <ta e="T112" id="Seg_5404" s="T111">forest.[NOM]</ta>
            <ta e="T113" id="Seg_5405" s="T112">inside-3SG-DAT/LOC</ta>
            <ta e="T114" id="Seg_5406" s="T113">run-CVB.SEQ</ta>
            <ta e="T115" id="Seg_5407" s="T114">go-PRS.[3SG]</ta>
            <ta e="T116" id="Seg_5408" s="T115">hey</ta>
            <ta e="T117" id="Seg_5409" s="T116">see-PRS.[3SG]</ta>
            <ta e="T118" id="Seg_5410" s="T117">raise.the.head-ITER-CVB.SIM-raise.the.head-ITER-CVB.SIM</ta>
            <ta e="T119" id="Seg_5411" s="T118">run-PST2-3SG</ta>
            <ta e="T120" id="Seg_5412" s="T119">expanding</ta>
            <ta e="T121" id="Seg_5413" s="T120">larch.[NOM]</ta>
            <ta e="T122" id="Seg_5414" s="T121">top-3SG-DAT/LOC</ta>
            <ta e="T123" id="Seg_5415" s="T122">rough_legged.buzzard.[NOM]</ta>
            <ta e="T124" id="Seg_5416" s="T123">give.birth-CVB.SEQ</ta>
            <ta e="T125" id="Seg_5417" s="T124">after</ta>
            <ta e="T126" id="Seg_5418" s="T125">lie-PRS.[3SG]</ta>
            <ta e="T127" id="Seg_5419" s="T126">hey</ta>
            <ta e="T128" id="Seg_5420" s="T127">well</ta>
            <ta e="T129" id="Seg_5421" s="T128">then</ta>
            <ta e="T130" id="Seg_5422" s="T129">1SG.[NOM]</ta>
            <ta e="T131" id="Seg_5423" s="T130">how</ta>
            <ta e="T132" id="Seg_5424" s="T131">make-CVB.SEQ-1SG</ta>
            <ta e="T133" id="Seg_5425" s="T132">deception-VBZ-CVB.SEQ-1SG</ta>
            <ta e="T134" id="Seg_5426" s="T133">eat-FUT-1SG=Q</ta>
            <ta e="T135" id="Seg_5427" s="T134">that</ta>
            <ta e="T136" id="Seg_5428" s="T135">rough_legged.buzzard-ABL</ta>
            <ta e="T137" id="Seg_5429" s="T136">what-ACC</ta>
            <ta e="T138" id="Seg_5430" s="T137">egg-PART</ta>
            <ta e="T139" id="Seg_5431" s="T138">think-CVB.SEQ</ta>
            <ta e="T140" id="Seg_5432" s="T139">this</ta>
            <ta e="T141" id="Seg_5433" s="T140">MOD</ta>
            <ta e="T142" id="Seg_5434" s="T141">rough_legged.buzzard-DAT/LOC</ta>
            <ta e="T145" id="Seg_5435" s="T144">rough_legged.buzzard-ABL</ta>
            <ta e="T146" id="Seg_5436" s="T145">egg.[NOM]</ta>
            <ta e="T147" id="Seg_5437" s="T146">eat-CVB.PURP</ta>
            <ta e="T148" id="Seg_5438" s="T147">run-CVB.SEQ</ta>
            <ta e="T149" id="Seg_5439" s="T148">come-CVB.SEQ</ta>
            <ta e="T150" id="Seg_5440" s="T149">most</ta>
            <ta e="T151" id="Seg_5441" s="T150">who-3SG-GEN</ta>
            <ta e="T152" id="Seg_5442" s="T151">lower.part-3SG-DAT/LOC</ta>
            <ta e="T153" id="Seg_5443" s="T152">come-PST2.[3SG]</ta>
            <ta e="T154" id="Seg_5444" s="T153">larch-3SG-GEN</ta>
            <ta e="T155" id="Seg_5445" s="T154">lower.part-3SG-DAT/LOC</ta>
            <ta e="T156" id="Seg_5446" s="T155">come-PST2.[3SG]</ta>
            <ta e="T157" id="Seg_5447" s="T156">so</ta>
            <ta e="T158" id="Seg_5448" s="T157">make-CVB.SEQ</ta>
            <ta e="T159" id="Seg_5449" s="T158">after</ta>
            <ta e="T160" id="Seg_5450" s="T159">tail-3SG-INSTR</ta>
            <ta e="T161" id="Seg_5451" s="T160">larch-3SG-ACC</ta>
            <ta e="T162" id="Seg_5452" s="T161">thrash-CVB.SIM</ta>
            <ta e="T163" id="Seg_5453" s="T162">stand-PST2-3SG</ta>
            <ta e="T164" id="Seg_5454" s="T163">rough_legged.buzzard.[NOM]</ta>
            <ta e="T165" id="Seg_5455" s="T164">rough_legged.buzzard.[NOM]</ta>
            <ta e="T166" id="Seg_5456" s="T165">quick</ta>
            <ta e="T167" id="Seg_5457" s="T166">MOD</ta>
            <ta e="T168" id="Seg_5458" s="T167">get.[IMP.2SG]</ta>
            <ta e="T169" id="Seg_5459" s="T168">one</ta>
            <ta e="T170" id="Seg_5460" s="T169">child-2SG-ACC</ta>
            <ta e="T171" id="Seg_5461" s="T170">1SG.[NOM]</ta>
            <ta e="T172" id="Seg_5462" s="T171">tail-EP-1SG.[NOM]</ta>
            <ta e="T173" id="Seg_5463" s="T172">Russian</ta>
            <ta e="T174" id="Seg_5464" s="T173">sword-3SG-COMP</ta>
            <ta e="T175" id="Seg_5465" s="T174">sharp.[NOM]</ta>
            <ta e="T176" id="Seg_5466" s="T175">larch-COM</ta>
            <ta e="T177" id="Seg_5467" s="T176">EMPH-COM</ta>
            <ta e="T178" id="Seg_5468" s="T177">cut-CVB.SIM</ta>
            <ta e="T179" id="Seg_5469" s="T178">beat-CVB.SEQ</ta>
            <ta e="T180" id="Seg_5470" s="T179">fall-EP-CAUS-FUT-1SG</ta>
            <ta e="T181" id="Seg_5471" s="T180">self-COM-2SG-ACC</ta>
            <ta e="T182" id="Seg_5472" s="T181">eat-FUT-1SG</ta>
            <ta e="T183" id="Seg_5473" s="T182">rough_legged.buzzard.[NOM]</ta>
            <ta e="T184" id="Seg_5474" s="T183">come.to.an.end-FUT.[3SG]</ta>
            <ta e="T185" id="Seg_5475" s="T184">Q</ta>
            <ta e="T186" id="Seg_5476" s="T185">cry-CVB.SIM-cry-CVB.SIM</ta>
            <ta e="T187" id="Seg_5477" s="T186">one</ta>
            <ta e="T188" id="Seg_5478" s="T187">child-3SG-ACC</ta>
            <ta e="T189" id="Seg_5479" s="T188">roll-NMNZ.[NOM]</ta>
            <ta e="T190" id="Seg_5480" s="T189">make-CAUS-PST2.[3SG]</ta>
            <ta e="T191" id="Seg_5481" s="T190">one</ta>
            <ta e="T192" id="Seg_5482" s="T191">egg-3SG-ACC</ta>
            <ta e="T193" id="Seg_5483" s="T192">eat-PST2.[3SG]</ta>
            <ta e="T194" id="Seg_5484" s="T193">so</ta>
            <ta e="T195" id="Seg_5485" s="T194">make-CVB.SEQ</ta>
            <ta e="T196" id="Seg_5486" s="T195">after</ta>
            <ta e="T197" id="Seg_5487" s="T196">earth.[NOM]</ta>
            <ta e="T198" id="Seg_5488" s="T197">sky.[NOM]</ta>
            <ta e="T199" id="Seg_5489" s="T198">run-CVB.SEQ</ta>
            <ta e="T200" id="Seg_5490" s="T199">stay-PST2.[3SG]</ta>
            <ta e="T201" id="Seg_5491" s="T200">next.morning-3SG-ACC</ta>
            <ta e="T202" id="Seg_5492" s="T201">stand.up-PRS.[3SG]</ta>
            <ta e="T203" id="Seg_5493" s="T202">again</ta>
            <ta e="T204" id="Seg_5494" s="T203">rough_legged.buzzard-ACC</ta>
            <ta e="T205" id="Seg_5495" s="T204">deception-VBZ</ta>
            <ta e="T206" id="Seg_5496" s="T205">rough_legged.buzzard-3SG-ACC</ta>
            <ta e="T207" id="Seg_5497" s="T206">deception-VBZ-CVB.PURP</ta>
            <ta e="T208" id="Seg_5498" s="T207">again</ta>
            <ta e="T209" id="Seg_5499" s="T208">run-CVB.SEQ</ta>
            <ta e="T210" id="Seg_5500" s="T209">come-PRS.[3SG]</ta>
            <ta e="T211" id="Seg_5501" s="T210">rough_legged.buzzard.[NOM]</ta>
            <ta e="T212" id="Seg_5502" s="T211">rough_legged.buzzard.[NOM]</ta>
            <ta e="T213" id="Seg_5503" s="T212">quick</ta>
            <ta e="T214" id="Seg_5504" s="T213">MOD</ta>
            <ta e="T215" id="Seg_5505" s="T214">get.[IMP.2SG]</ta>
            <ta e="T216" id="Seg_5506" s="T215">one</ta>
            <ta e="T217" id="Seg_5507" s="T216">child-2SG-ACC</ta>
            <ta e="T218" id="Seg_5508" s="T217">1SG.[NOM]</ta>
            <ta e="T219" id="Seg_5509" s="T218">tail-EP-1SG.[NOM]</ta>
            <ta e="T220" id="Seg_5510" s="T219">Russian</ta>
            <ta e="T221" id="Seg_5511" s="T220">sword-3SG-COMP</ta>
            <ta e="T222" id="Seg_5512" s="T221">sharp.[NOM]</ta>
            <ta e="T223" id="Seg_5513" s="T222">larch-COM-2SG-ACC-EMPH-COM-2SG-ACC</ta>
            <ta e="T224" id="Seg_5514" s="T223">cut-CVB.SIM</ta>
            <ta e="T225" id="Seg_5515" s="T224">beat-CVB.SEQ</ta>
            <ta e="T226" id="Seg_5516" s="T225">fall-EP-CAUS-CVB.SEQ-1SG</ta>
            <ta e="T227" id="Seg_5517" s="T226">now</ta>
            <ta e="T228" id="Seg_5518" s="T227">self-COM-2SG-ACC</ta>
            <ta e="T229" id="Seg_5519" s="T228">eat-FUT-1SG</ta>
            <ta e="T230" id="Seg_5520" s="T229">again</ta>
            <ta e="T231" id="Seg_5521" s="T230">one</ta>
            <ta e="T232" id="Seg_5522" s="T231">child-3SG-ACC</ta>
            <ta e="T233" id="Seg_5523" s="T232">push-CVB.SEQ</ta>
            <ta e="T234" id="Seg_5524" s="T233">throw-PST2.[3SG]</ta>
            <ta e="T235" id="Seg_5525" s="T234">two</ta>
            <ta e="T236" id="Seg_5526" s="T235">child-3SG-ACC</ta>
            <ta e="T237" id="Seg_5527" s="T236">eat-PST2.[3SG]</ta>
            <ta e="T238" id="Seg_5528" s="T237">fox.[NOM]</ta>
            <ta e="T239" id="Seg_5529" s="T238">rough_legged.buzzard.[NOM]</ta>
            <ta e="T240" id="Seg_5530" s="T239">cry-CVB.SIM</ta>
            <ta e="T241" id="Seg_5531" s="T240">sit-EMOT-PST2.[3SG]</ta>
            <ta e="T242" id="Seg_5532" s="T241">EMPH</ta>
            <ta e="T243" id="Seg_5533" s="T242">well</ta>
            <ta e="T244" id="Seg_5534" s="T243">this</ta>
            <ta e="T245" id="Seg_5535" s="T244">now</ta>
            <ta e="T246" id="Seg_5536" s="T245">though</ta>
            <ta e="T247" id="Seg_5537" s="T246">self-COM-1SG-ACC</ta>
            <ta e="T248" id="Seg_5538" s="T247">eat-CVB.SEQ-ACC</ta>
            <ta e="T249" id="Seg_5539" s="T248">say-CVB.SEQ</ta>
            <ta e="T250" id="Seg_5540" s="T249">cry-CVB.SIM</ta>
            <ta e="T251" id="Seg_5541" s="T250">sit-PST2.[3SG]</ta>
            <ta e="T252" id="Seg_5542" s="T251">well</ta>
            <ta e="T253" id="Seg_5543" s="T252">this</ta>
            <ta e="T254" id="Seg_5544" s="T253">sit-TEMP-3SG</ta>
            <ta e="T255" id="Seg_5545" s="T254">Siberian.jay.[NOM]</ta>
            <ta e="T256" id="Seg_5546" s="T255">fly-CVB.SEQ</ta>
            <ta e="T257" id="Seg_5547" s="T256">come-PST2.[3SG]</ta>
            <ta e="T258" id="Seg_5548" s="T257">hey</ta>
            <ta e="T259" id="Seg_5549" s="T258">rough_legged.buzzard.[NOM]</ta>
            <ta e="T260" id="Seg_5550" s="T259">what-ABL</ta>
            <ta e="T261" id="Seg_5551" s="T260">cry-PRS-2SG</ta>
            <ta e="T262" id="Seg_5552" s="T261">EMPH</ta>
            <ta e="T263" id="Seg_5553" s="T262">2SG.[NOM]</ta>
            <ta e="T264" id="Seg_5554" s="T263">this</ta>
            <ta e="T265" id="Seg_5555" s="T264">such</ta>
            <ta e="T266" id="Seg_5556" s="T265">beautiful</ta>
            <ta e="T267" id="Seg_5557" s="T266">sun-PROPR</ta>
            <ta e="T268" id="Seg_5558" s="T267">day.[NOM]</ta>
            <ta e="T269" id="Seg_5559" s="T268">sunrise-3SG.[NOM]</ta>
            <ta e="T270" id="Seg_5560" s="T269">stand.up-TEMP-3SG</ta>
            <ta e="T271" id="Seg_5561" s="T270">child-PL-EP-2SG.[NOM]</ta>
            <ta e="T272" id="Seg_5562" s="T271">and</ta>
            <ta e="T273" id="Seg_5563" s="T272">warm-DAT/LOC</ta>
            <ta e="T274" id="Seg_5564" s="T273">lie-PRS-3PL</ta>
            <ta e="T275" id="Seg_5565" s="T274">self-2SG.[NOM]</ta>
            <ta e="T276" id="Seg_5566" s="T275">and</ta>
            <ta e="T277" id="Seg_5567" s="T276">food-ACC</ta>
            <ta e="T278" id="Seg_5568" s="T277">EMPH</ta>
            <ta e="T279" id="Seg_5569" s="T278">get-CVB.SEQ</ta>
            <ta e="T280" id="Seg_5570" s="T279">come-PTCP.PRS.[NOM]</ta>
            <ta e="T281" id="Seg_5571" s="T280">similar</ta>
            <ta e="T282" id="Seg_5572" s="T281">EMPH</ta>
            <ta e="T283" id="Seg_5573" s="T282">what-ABL</ta>
            <ta e="T284" id="Seg_5574" s="T283">cry-PRS-2SG</ta>
            <ta e="T285" id="Seg_5575" s="T284">well</ta>
            <ta e="T286" id="Seg_5576" s="T285">such</ta>
            <ta e="T287" id="Seg_5577" s="T286">happy-DAT/LOC</ta>
            <ta e="T288" id="Seg_5578" s="T287">live-CVB.SEQ-2SG</ta>
            <ta e="T289" id="Seg_5579" s="T288">say-PRS.[3SG]</ta>
            <ta e="T290" id="Seg_5580" s="T289">2SG.[NOM]</ta>
            <ta e="T291" id="Seg_5581" s="T290">how</ta>
            <ta e="T292" id="Seg_5582" s="T291">cry-EP-CAUS-NEG-2SG</ta>
            <ta e="T293" id="Seg_5583" s="T292">see-NEG-2SG</ta>
            <ta e="T294" id="Seg_5584" s="T293">Q</ta>
            <ta e="T295" id="Seg_5585" s="T294">1SG-ACC</ta>
            <ta e="T296" id="Seg_5586" s="T295">though</ta>
            <ta e="T297" id="Seg_5587" s="T296">fox.[NOM]</ta>
            <ta e="T298" id="Seg_5588" s="T297">MOD</ta>
            <ta e="T299" id="Seg_5589" s="T298">two</ta>
            <ta e="T300" id="Seg_5590" s="T299">day-DAT/LOC</ta>
            <ta e="T301" id="Seg_5591" s="T300">two</ta>
            <ta e="T302" id="Seg_5592" s="T301">child-1SG-ACC</ta>
            <ta e="T303" id="Seg_5593" s="T302">eat-PST1-3SG</ta>
            <ta e="T304" id="Seg_5594" s="T303">now</ta>
            <ta e="T305" id="Seg_5595" s="T304">today</ta>
            <ta e="T306" id="Seg_5596" s="T305">again</ta>
            <ta e="T307" id="Seg_5597" s="T306">come-FUT-3SG</ta>
            <ta e="T308" id="Seg_5598" s="T307">eat-FUT-3SG</ta>
            <ta e="T309" id="Seg_5599" s="T308">larch-COM-1SG-ACC</ta>
            <ta e="T310" id="Seg_5600" s="T309">EMPH-COM-1SG-ACC</ta>
            <ta e="T311" id="Seg_5601" s="T310">cut-CVB.SIM</ta>
            <ta e="T312" id="Seg_5602" s="T311">beat-CVB.SEQ</ta>
            <ta e="T313" id="Seg_5603" s="T312">fall-EP-CAUS-CVB.SEQ</ta>
            <ta e="T314" id="Seg_5604" s="T313">self-COM-1SG-ACC</ta>
            <ta e="T315" id="Seg_5605" s="T314">eat-CVB.PURP</ta>
            <ta e="T316" id="Seg_5606" s="T315">want-PST1-3SG</ta>
            <ta e="T317" id="Seg_5607" s="T316">say-PRS.[3SG]</ta>
            <ta e="T318" id="Seg_5608" s="T317">what-PROPR.[NOM]=Q</ta>
            <ta e="T319" id="Seg_5609" s="T318">that</ta>
            <ta e="T320" id="Seg_5610" s="T319">fox-EP-2SG.[NOM]</ta>
            <ta e="T321" id="Seg_5611" s="T320">well</ta>
            <ta e="T322" id="Seg_5612" s="T321">tail-3SG.[NOM]</ta>
            <ta e="T323" id="Seg_5613" s="T322">Russian</ta>
            <ta e="T324" id="Seg_5614" s="T323">sword-3SG-COMP</ta>
            <ta e="T325" id="Seg_5615" s="T324">sharp.[NOM]</ta>
            <ta e="T326" id="Seg_5616" s="T325">well</ta>
            <ta e="T327" id="Seg_5617" s="T326">what.[NOM]</ta>
            <ta e="T328" id="Seg_5618" s="T327">name-PROPR-3SG.[NOM]</ta>
            <ta e="T329" id="Seg_5619" s="T328">stupid</ta>
            <ta e="T330" id="Seg_5620" s="T329">bird-3SG.[NOM]</ta>
            <ta e="T331" id="Seg_5621" s="T330">be.born-PST2-EP-2SG=Q</ta>
            <ta e="T332" id="Seg_5622" s="T331">2SG.[NOM]</ta>
            <ta e="T333" id="Seg_5623" s="T332">such.[NOM]</ta>
            <ta e="T334" id="Seg_5624" s="T333">well</ta>
            <ta e="T335" id="Seg_5625" s="T334">rough_legged.buzzard.[NOM]</ta>
            <ta e="T336" id="Seg_5626" s="T335">how</ta>
            <ta e="T337" id="Seg_5627" s="T336">make-CVB.SIM</ta>
            <ta e="T338" id="Seg_5628" s="T337">fox.[NOM]</ta>
            <ta e="T339" id="Seg_5629" s="T338">how</ta>
            <ta e="T340" id="Seg_5630" s="T339">make-CVB.SEQ</ta>
            <ta e="T341" id="Seg_5631" s="T340">2SG.[NOM]</ta>
            <ta e="T342" id="Seg_5632" s="T341">larch-2SG-ACC</ta>
            <ta e="T343" id="Seg_5633" s="T342">cut-CVB.SIM</ta>
            <ta e="T344" id="Seg_5634" s="T343">beat-FUT.[3SG]=Q</ta>
            <ta e="T345" id="Seg_5635" s="T344">tail-3SG.[NOM]</ta>
            <ta e="T346" id="Seg_5636" s="T345">animal.hair.[NOM]</ta>
            <ta e="T347" id="Seg_5637" s="T346">animal.hair.[NOM]</ta>
            <ta e="T348" id="Seg_5638" s="T347">tail.[NOM]</ta>
            <ta e="T349" id="Seg_5639" s="T348">what-ACC</ta>
            <ta e="T350" id="Seg_5640" s="T349">make.it-FUT-2SG=Q</ta>
            <ta e="T351" id="Seg_5641" s="T350">say.[IMP.2SG]</ta>
            <ta e="T352" id="Seg_5642" s="T351">now</ta>
            <ta e="T353" id="Seg_5643" s="T352">come-TEMP-3SG</ta>
            <ta e="T354" id="Seg_5644" s="T353">who.[NOM]</ta>
            <ta e="T355" id="Seg_5645" s="T354">teach-PST2-3SG=Q</ta>
            <ta e="T356" id="Seg_5646" s="T355">say-TEMP-3SG</ta>
            <ta e="T357" id="Seg_5647" s="T356">Siberian.jay.[NOM]</ta>
            <ta e="T358" id="Seg_5648" s="T357">teach-PST2-3SG</ta>
            <ta e="T359" id="Seg_5649" s="T358">say-FUT.[IMP.2SG]</ta>
            <ta e="T360" id="Seg_5650" s="T359">well</ta>
            <ta e="T361" id="Seg_5651" s="T360">rough_legged.buzzard.[NOM]</ta>
            <ta e="T364" id="Seg_5652" s="T363">rough_legged.buzzard.[NOM]</ta>
            <ta e="T365" id="Seg_5653" s="T364">well</ta>
            <ta e="T366" id="Seg_5654" s="T365">better</ta>
            <ta e="T367" id="Seg_5655" s="T366">a.little</ta>
            <ta e="T368" id="Seg_5656" s="T367">become-PST2.[3SG]</ta>
            <ta e="T369" id="Seg_5657" s="T368">sit-PST2.[3SG]</ta>
            <ta e="T370" id="Seg_5658" s="T369">Siberian.jay-3SG.[NOM]</ta>
            <ta e="T371" id="Seg_5659" s="T370">fly-CVB.SEQ</ta>
            <ta e="T372" id="Seg_5660" s="T371">stay-PST2.[3SG]</ta>
            <ta e="T373" id="Seg_5661" s="T372">oh</ta>
            <ta e="T374" id="Seg_5662" s="T373">well</ta>
            <ta e="T375" id="Seg_5663" s="T374">fox.[NOM]</ta>
            <ta e="T376" id="Seg_5664" s="T375">get.angry-CVB.PURP</ta>
            <ta e="T377" id="Seg_5665" s="T376">get.angry-PTCP.PST</ta>
            <ta e="T378" id="Seg_5666" s="T377">become-PST2.[3SG]</ta>
            <ta e="T379" id="Seg_5667" s="T378">be.angry-CVB.PURP</ta>
            <ta e="T380" id="Seg_5668" s="T379">be.angry-PTCP.PST</ta>
            <ta e="T381" id="Seg_5669" s="T380">become-PST2.[3SG]</ta>
            <ta e="T382" id="Seg_5670" s="T381">different-different</ta>
            <ta e="T383" id="Seg_5671" s="T382">moss-ACC</ta>
            <ta e="T384" id="Seg_5672" s="T383">stand-EP-CAUS-CVB.SIM</ta>
            <ta e="T385" id="Seg_5673" s="T384">hit-CVB.SIM-hit-CVB.SIM</ta>
            <ta e="T386" id="Seg_5674" s="T385">front</ta>
            <ta e="T387" id="Seg_5675" s="T386">back</ta>
            <ta e="T388" id="Seg_5676" s="T387">leg-3SG-INSTR</ta>
            <ta e="T389" id="Seg_5677" s="T388">tear-CVB.SIM</ta>
            <ta e="T390" id="Seg_5678" s="T389">hit-CVB.SIM-hit-CVB.SIM</ta>
            <ta e="T391" id="Seg_5679" s="T390">come-EMOT-PST2.[3SG]</ta>
            <ta e="T392" id="Seg_5680" s="T391">EMPH</ta>
            <ta e="T393" id="Seg_5681" s="T392">rough_legged.buzzard-3SG-DAT/LOC</ta>
            <ta e="T394" id="Seg_5682" s="T393">rough_legged.buzzard-ACC</ta>
            <ta e="T395" id="Seg_5683" s="T394">scare-CVB.PURP</ta>
            <ta e="T396" id="Seg_5684" s="T395">rough_legged.buzzard.[NOM]</ta>
            <ta e="T397" id="Seg_5685" s="T396">rough_legged.buzzard.[NOM]</ta>
            <ta e="T398" id="Seg_5686" s="T397">such-such</ta>
            <ta e="T399" id="Seg_5687" s="T398">mother-PROPR.[NOM]</ta>
            <ta e="T400" id="Seg_5688" s="T399">well</ta>
            <ta e="T401" id="Seg_5689" s="T400">1SG.[NOM]</ta>
            <ta e="T402" id="Seg_5690" s="T401">though</ta>
            <ta e="T403" id="Seg_5691" s="T402">again</ta>
            <ta e="T404" id="Seg_5692" s="T403">completely</ta>
            <ta e="T405" id="Seg_5693" s="T404">get.angry-CVB.PURP</ta>
            <ta e="T406" id="Seg_5694" s="T405">get.angry-PST1-1SG</ta>
            <ta e="T407" id="Seg_5695" s="T406">get.[IMP.2SG]</ta>
            <ta e="T408" id="Seg_5696" s="T407">one</ta>
            <ta e="T409" id="Seg_5697" s="T408">child-PART</ta>
            <ta e="T410" id="Seg_5698" s="T409">1SG.[NOM]</ta>
            <ta e="T411" id="Seg_5699" s="T410">tail-EP-1SG.[NOM]</ta>
            <ta e="T412" id="Seg_5700" s="T411">Russian</ta>
            <ta e="T413" id="Seg_5701" s="T412">sword-3SG-COMP</ta>
            <ta e="T414" id="Seg_5702" s="T413">sharp.[NOM]</ta>
            <ta e="T415" id="Seg_5703" s="T414">recently</ta>
            <ta e="T416" id="Seg_5704" s="T415">self-COM-2SG-ACC</ta>
            <ta e="T419" id="Seg_5705" s="T418">completely</ta>
            <ta e="T420" id="Seg_5706" s="T419">beat-CVB.SEQ-1SG</ta>
            <ta e="T421" id="Seg_5707" s="T420">self-COM-2SG-ACC</ta>
            <ta e="T422" id="Seg_5708" s="T421">eat-FUT-1SG</ta>
            <ta e="T423" id="Seg_5709" s="T422">even</ta>
            <ta e="T424" id="Seg_5710" s="T423">EMPH</ta>
            <ta e="T425" id="Seg_5711" s="T424">stupid-VBZ-CVB.SEQ-1SG</ta>
            <ta e="T426" id="Seg_5712" s="T425">completely</ta>
            <ta e="T430" id="Seg_5713" s="T429">dear</ta>
            <ta e="T431" id="Seg_5714" s="T430">two</ta>
            <ta e="T432" id="Seg_5715" s="T431">child-1SG-ACC</ta>
            <ta e="T433" id="Seg_5716" s="T432">eat-CAUS-PST1-1SG</ta>
            <ta e="T434" id="Seg_5717" s="T433">1SG.[NOM]</ta>
            <ta e="T435" id="Seg_5718" s="T434">2SG-DAT/LOC</ta>
            <ta e="T436" id="Seg_5719" s="T435">self-1SG.[NOM]</ta>
            <ta e="T437" id="Seg_5720" s="T436">stupid-1SG-DAT/LOC</ta>
            <ta e="T438" id="Seg_5721" s="T437">2SG.[NOM]</ta>
            <ta e="T439" id="Seg_5722" s="T438">though</ta>
            <ta e="T440" id="Seg_5723" s="T439">animal.hair.[NOM]</ta>
            <ta e="T441" id="Seg_5724" s="T440">tail-2SG</ta>
            <ta e="T442" id="Seg_5725" s="T441">EMPH</ta>
            <ta e="T443" id="Seg_5726" s="T442">animal.hair.[NOM]</ta>
            <ta e="T444" id="Seg_5727" s="T443">tail.[NOM]</ta>
            <ta e="T445" id="Seg_5728" s="T444">what-ACC</ta>
            <ta e="T446" id="Seg_5729" s="T445">make.it-FUT-2SG=Q</ta>
            <ta e="T447" id="Seg_5730" s="T446">tree-ACC</ta>
            <ta e="T448" id="Seg_5731" s="T447">make.it-FUT-2SG</ta>
            <ta e="T449" id="Seg_5732" s="T448">Q</ta>
            <ta e="T450" id="Seg_5733" s="T449">say-PRS.[3SG]</ta>
            <ta e="T451" id="Seg_5734" s="T450">EMPH</ta>
            <ta e="T452" id="Seg_5735" s="T451">oh.my.god</ta>
            <ta e="T453" id="Seg_5736" s="T452">what.[NOM]</ta>
            <ta e="T454" id="Seg_5737" s="T453">name-PROPR-3SG.[NOM]</ta>
            <ta e="T455" id="Seg_5738" s="T454">mind.[NOM]</ta>
            <ta e="T456" id="Seg_5739" s="T455">give-PST2-3SG=Q</ta>
            <ta e="T457" id="Seg_5740" s="T456">2SG-DAT/LOC</ta>
            <ta e="T458" id="Seg_5741" s="T457">how</ta>
            <ta e="T459" id="Seg_5742" s="T458">make-CVB.SEQ-2SG</ta>
            <ta e="T460" id="Seg_5743" s="T459">mind-ACC</ta>
            <ta e="T461" id="Seg_5744" s="T460">find-PST2-EP-2SG=Q</ta>
            <ta e="T462" id="Seg_5745" s="T461">say-PRS.[3SG]</ta>
            <ta e="T463" id="Seg_5746" s="T462">my</ta>
            <ta e="T464" id="Seg_5747" s="T463">tail-EP-1SG.[NOM]</ta>
            <ta e="T465" id="Seg_5748" s="T464">animal.hair-3SG-ACC</ta>
            <ta e="T466" id="Seg_5749" s="T465">where.from</ta>
            <ta e="T467" id="Seg_5750" s="T466">know-PRS-2SG</ta>
            <ta e="T468" id="Seg_5751" s="T467">well</ta>
            <ta e="T469" id="Seg_5752" s="T468">1SG-ACC</ta>
            <ta e="T470" id="Seg_5753" s="T469">Siberian.jay.[NOM]</ta>
            <ta e="T471" id="Seg_5754" s="T470">teach-PST2-3SG</ta>
            <ta e="T472" id="Seg_5755" s="T471">who.[NOM]</ta>
            <ta e="T473" id="Seg_5756" s="T472">give-FUT.[3SG]=Q</ta>
            <ta e="T474" id="Seg_5757" s="T473">1SG-DAT/LOC</ta>
            <ta e="T475" id="Seg_5758" s="T474">mind.[NOM]</ta>
            <ta e="T476" id="Seg_5759" s="T475">Siberian.jay.[NOM]</ta>
            <ta e="T477" id="Seg_5760" s="T476">teach-PST2-3SG</ta>
            <ta e="T478" id="Seg_5761" s="T477">hey</ta>
            <ta e="T479" id="Seg_5762" s="T478">such-such</ta>
            <ta e="T480" id="Seg_5763" s="T479">EMPH</ta>
            <ta e="T481" id="Seg_5764" s="T480">Siberian.jay-ACC</ta>
            <ta e="T482" id="Seg_5765" s="T481">what.kind.of</ta>
            <ta e="T483" id="Seg_5766" s="T482">place-ABL</ta>
            <ta e="T484" id="Seg_5767" s="T483">find-FUT-1SG=Q</ta>
            <ta e="T485" id="Seg_5768" s="T484">well</ta>
            <ta e="T486" id="Seg_5769" s="T485">wait</ta>
            <ta e="T487" id="Seg_5770" s="T486">well</ta>
            <ta e="T488" id="Seg_5771" s="T487">meet-FUT-1SG</ta>
            <ta e="T489" id="Seg_5772" s="T488">1SG.[NOM]</ta>
            <ta e="T490" id="Seg_5773" s="T489">3SG-DAT/LOC</ta>
            <ta e="T491" id="Seg_5774" s="T490">EMPH</ta>
            <ta e="T492" id="Seg_5775" s="T491">so</ta>
            <ta e="T493" id="Seg_5776" s="T492">make-PST2.[3SG]</ta>
            <ta e="T494" id="Seg_5777" s="T493">and</ta>
            <ta e="T495" id="Seg_5778" s="T494">earth.[NOM]</ta>
            <ta e="T496" id="Seg_5779" s="T495">sky.[NOM]</ta>
            <ta e="T497" id="Seg_5780" s="T496">run-CVB.SEQ</ta>
            <ta e="T498" id="Seg_5781" s="T497">stay-PST2.[3SG]</ta>
            <ta e="T499" id="Seg_5782" s="T498">earth.[NOM]</ta>
            <ta e="T500" id="Seg_5783" s="T499">sky.[NOM]</ta>
            <ta e="T501" id="Seg_5784" s="T500">run-CVB.SEQ</ta>
            <ta e="T502" id="Seg_5785" s="T501">go-CVB.SEQ</ta>
            <ta e="T503" id="Seg_5786" s="T502">be-PST1-3SG</ta>
            <ta e="T504" id="Seg_5787" s="T503">EMPH-round</ta>
            <ta e="T505" id="Seg_5788" s="T504">lake-DAT/LOC</ta>
            <ta e="T507" id="Seg_5789" s="T505">reach-CVB.SEQ</ta>
            <ta e="T508" id="Seg_5790" s="T507">this</ta>
            <ta e="T509" id="Seg_5791" s="T508">lake.[NOM]</ta>
            <ta e="T510" id="Seg_5792" s="T509">what-ABL</ta>
            <ta e="T511" id="Seg_5793" s="T510">NEG</ta>
            <ta e="T512" id="Seg_5794" s="T511">big</ta>
            <ta e="T513" id="Seg_5795" s="T512">mountain-PROPR.[NOM]</ta>
            <ta e="T514" id="Seg_5796" s="T513">spring.[NOM]</ta>
            <ta e="T515" id="Seg_5797" s="T514">time-3SG-DAT/LOC</ta>
            <ta e="T516" id="Seg_5798" s="T515">be-PST1-3SG</ta>
            <ta e="T517" id="Seg_5799" s="T516">hill-3SG.[NOM]</ta>
            <ta e="T518" id="Seg_5800" s="T517">completely</ta>
            <ta e="T519" id="Seg_5801" s="T518">go.out-EP-PST2.[3SG]</ta>
            <ta e="T520" id="Seg_5802" s="T519">this</ta>
            <ta e="T521" id="Seg_5803" s="T520">hill-ACC</ta>
            <ta e="T522" id="Seg_5804" s="T521">though</ta>
            <ta e="T523" id="Seg_5805" s="T522">run-CVB.SIM</ta>
            <ta e="T524" id="Seg_5806" s="T523">go-CVB.SEQ</ta>
            <ta e="T525" id="Seg_5807" s="T524">completely</ta>
            <ta e="T526" id="Seg_5808" s="T525">most</ta>
            <ta e="T527" id="Seg_5809" s="T526">beautiful</ta>
            <ta e="T528" id="Seg_5810" s="T527">thawed.patch-DAT/LOC</ta>
            <ta e="T529" id="Seg_5811" s="T528">though</ta>
            <ta e="T530" id="Seg_5812" s="T529">die-PTCP.PST</ta>
            <ta e="T531" id="Seg_5813" s="T530">fox.[NOM]</ta>
            <ta e="T532" id="Seg_5814" s="T531">become-CVB.SEQ</ta>
            <ta e="T533" id="Seg_5815" s="T532">after</ta>
            <ta e="T534" id="Seg_5816" s="T533">spread.out-NMNZ.[NOM]</ta>
            <ta e="T535" id="Seg_5817" s="T534">make-CVB.SIM</ta>
            <ta e="T536" id="Seg_5818" s="T535">fall-PST2.[3SG]</ta>
            <ta e="T537" id="Seg_5819" s="T536">completely</ta>
            <ta e="T538" id="Seg_5820" s="T537">insects-DAT/LOC</ta>
            <ta e="T539" id="Seg_5821" s="T538">every-3SG-DAT/LOC</ta>
            <ta e="T540" id="Seg_5822" s="T539">scatter-PASS-CVB.SIM</ta>
            <ta e="T541" id="Seg_5823" s="T540">lie-PST2.[3SG]</ta>
            <ta e="T542" id="Seg_5824" s="T541">that</ta>
            <ta e="T543" id="Seg_5825" s="T542">make-CVB.SEQ</ta>
            <ta e="T544" id="Seg_5826" s="T543">after</ta>
            <ta e="T545" id="Seg_5827" s="T544">hey</ta>
            <ta e="T546" id="Seg_5828" s="T545">jay-PL.[NOM]</ta>
            <ta e="T547" id="Seg_5829" s="T546">EMPH</ta>
            <ta e="T548" id="Seg_5830" s="T547">fly-CVB.SIM</ta>
            <ta e="T549" id="Seg_5831" s="T548">go-CVB.SEQ</ta>
            <ta e="T550" id="Seg_5832" s="T549">be-CVB.SEQ</ta>
            <ta e="T551" id="Seg_5833" s="T550">see-PRS.[3SG]</ta>
            <ta e="T552" id="Seg_5834" s="T551">fox.[NOM]</ta>
            <ta e="T553" id="Seg_5835" s="T552">die-CVB.SEQ</ta>
            <ta e="T554" id="Seg_5836" s="T553">after</ta>
            <ta e="T555" id="Seg_5837" s="T554">spread.out-CVB.SEQ</ta>
            <ta e="T556" id="Seg_5838" s="T555">lie-PRS.[3SG]</ta>
            <ta e="T557" id="Seg_5839" s="T556">oh</ta>
            <ta e="T558" id="Seg_5840" s="T557">this.[NOM]</ta>
            <ta e="T559" id="Seg_5841" s="T558">as.long.as</ta>
            <ta e="T560" id="Seg_5842" s="T559">this.[NOM]</ta>
            <ta e="T561" id="Seg_5843" s="T560">hunger-CVB.SEQ</ta>
            <ta e="T562" id="Seg_5844" s="T561">though</ta>
            <ta e="T563" id="Seg_5845" s="T562">rough_legged.buzzard.[NOM]</ta>
            <ta e="T564" id="Seg_5846" s="T563">child-PL-3SG-ACC</ta>
            <ta e="T565" id="Seg_5847" s="T564">eat-CVB.SIM</ta>
            <ta e="T566" id="Seg_5848" s="T565">go-EP-PST2-3SG</ta>
            <ta e="T567" id="Seg_5849" s="T566">MOD</ta>
            <ta e="T568" id="Seg_5850" s="T567">now</ta>
            <ta e="T569" id="Seg_5851" s="T568">rough_legged.buzzard-3SG.[NOM]</ta>
            <ta e="T570" id="Seg_5852" s="T569">though</ta>
            <ta e="T571" id="Seg_5853" s="T570">1SG-ABL</ta>
            <ta e="T572" id="Seg_5854" s="T571">mind.[NOM]</ta>
            <ta e="T573" id="Seg_5855" s="T572">take-PRS.[3SG]</ta>
            <ta e="T574" id="Seg_5856" s="T573">one</ta>
            <ta e="T575" id="Seg_5857" s="T574">NEG</ta>
            <ta e="T576" id="Seg_5858" s="T575">child-3SG-ACC</ta>
            <ta e="T577" id="Seg_5859" s="T576">give-PST2.NEG.[3SG]</ta>
            <ta e="T578" id="Seg_5860" s="T577">then</ta>
            <ta e="T579" id="Seg_5861" s="T578">where.from</ta>
            <ta e="T580" id="Seg_5862" s="T579">take-CVB.SEQ</ta>
            <ta e="T581" id="Seg_5863" s="T580">3SG.[NOM]</ta>
            <ta e="T582" id="Seg_5864" s="T581">EMPH</ta>
            <ta e="T583" id="Seg_5865" s="T582">hunger-PST2.[3SG]</ta>
            <ta e="T584" id="Seg_5866" s="T583">MOD</ta>
            <ta e="T585" id="Seg_5867" s="T584">where.from</ta>
            <ta e="T586" id="Seg_5868" s="T585">take-CVB.SEQ</ta>
            <ta e="T587" id="Seg_5869" s="T586">eat-FUT.[3SG]=Q</ta>
            <ta e="T588" id="Seg_5870" s="T587">well</ta>
            <ta e="T589" id="Seg_5871" s="T588">hunger-CVB.SEQ</ta>
            <ta e="T590" id="Seg_5872" s="T589">die-PST2.[3SG]</ta>
            <ta e="T591" id="Seg_5873" s="T590">well</ta>
            <ta e="T592" id="Seg_5874" s="T591">2SG.[NOM]</ta>
            <ta e="T593" id="Seg_5875" s="T592">EMPH</ta>
            <ta e="T594" id="Seg_5876" s="T593">die-PRS-NEC-2SG</ta>
            <ta e="T595" id="Seg_5877" s="T594">be-PST2.[3SG]</ta>
            <ta e="T596" id="Seg_5878" s="T595">think-CVB.SEQ</ta>
            <ta e="T597" id="Seg_5879" s="T596">well</ta>
            <ta e="T598" id="Seg_5880" s="T597">come-PST2.[3SG]</ta>
            <ta e="T599" id="Seg_5881" s="T598">knock-knock</ta>
            <ta e="T600" id="Seg_5882" s="T599">knock-knock</ta>
            <ta e="T601" id="Seg_5883" s="T600">knock-PST2.[3SG]</ta>
            <ta e="T602" id="Seg_5884" s="T601">EMPH</ta>
            <ta e="T603" id="Seg_5885" s="T602">where.from</ta>
            <ta e="T604" id="Seg_5886" s="T603">eye-3SG-ABL</ta>
            <ta e="T605" id="Seg_5887" s="T604">eat-PTCP.FUT-DAT/LOC</ta>
            <ta e="T606" id="Seg_5888" s="T605">think-CVB.SEQ</ta>
            <ta e="T607" id="Seg_5889" s="T606">lower.arm-DAT/LOC</ta>
            <ta e="T608" id="Seg_5890" s="T607">knock-MED-CVB.SIM</ta>
            <ta e="T609" id="Seg_5891" s="T608">go-TEMP-3SG</ta>
            <ta e="T610" id="Seg_5892" s="T609">completely</ta>
            <ta e="T611" id="Seg_5893" s="T682">fox.[NOM]</ta>
            <ta e="T612" id="Seg_5894" s="T611">though</ta>
            <ta e="T613" id="Seg_5895" s="T612">last</ta>
            <ta e="T614" id="Seg_5896" s="T613">slyness-3SG-INSTR</ta>
            <ta e="T615" id="Seg_5897" s="T614">last</ta>
            <ta e="T616" id="Seg_5898" s="T615">beast-3SG-INSTR</ta>
            <ta e="T617" id="Seg_5899" s="T616">growl-CVB.SIM</ta>
            <ta e="T618" id="Seg_5900" s="T617">make-CVB.SIM</ta>
            <ta e="T619" id="Seg_5901" s="T618">jump-EMOT-PST2.[3SG]</ta>
            <ta e="T621" id="Seg_5902" s="T619">EMPH</ta>
            <ta e="T622" id="Seg_5903" s="T621">that</ta>
            <ta e="T623" id="Seg_5904" s="T622">rough_legged.buzzard.[NOM]</ta>
            <ta e="T624" id="Seg_5905" s="T623">though</ta>
            <ta e="T625" id="Seg_5906" s="T624">who.[NOM]</ta>
            <ta e="T626" id="Seg_5907" s="T625">though</ta>
            <ta e="T627" id="Seg_5908" s="T626">Siberian.jay.[NOM]</ta>
            <ta e="T628" id="Seg_5909" s="T627">though</ta>
            <ta e="T629" id="Seg_5910" s="T628">escape-CVB.SEQ</ta>
            <ta e="T630" id="Seg_5911" s="T629">succeed-VBZ-NEG.CVB.SIM</ta>
            <ta e="T631" id="Seg_5912" s="T630">tail-3SG-ABL</ta>
            <ta e="T632" id="Seg_5913" s="T631">catch-CAUS-PST2.[3SG]</ta>
            <ta e="T633" id="Seg_5914" s="T632">so</ta>
            <ta e="T634" id="Seg_5915" s="T633">make-CVB.SEQ</ta>
            <ta e="T635" id="Seg_5916" s="T634">this-ACC</ta>
            <ta e="T636" id="Seg_5917" s="T635">scuffle-FREQ-EP-RECP/COLL-CVB.SEQ</ta>
            <ta e="T637" id="Seg_5918" s="T636">scuffle-EP-RECP/COLL-CVB.SEQ</ta>
            <ta e="T638" id="Seg_5919" s="T637">water.[NOM]</ta>
            <ta e="T639" id="Seg_5920" s="T638">inside-3SG-DAT/LOC</ta>
            <ta e="T640" id="Seg_5921" s="T639">roll-CAUS-CVB.SEQ</ta>
            <ta e="T641" id="Seg_5922" s="T640">fall-EP-CAUS-CVB.SEQ</ta>
            <ta e="T642" id="Seg_5923" s="T641">water.[NOM]</ta>
            <ta e="T645" id="Seg_5924" s="T644">water.[NOM]</ta>
            <ta e="T646" id="Seg_5925" s="T645">inside-3SG-DAT/LOC</ta>
            <ta e="T647" id="Seg_5926" s="T646">fall-EP-CAUS-CVB.SEQ</ta>
            <ta e="T648" id="Seg_5927" s="T647">this-DAT/LOC</ta>
            <ta e="T649" id="Seg_5928" s="T648">rinse-CVB.SEQ</ta>
            <ta e="T650" id="Seg_5929" s="T649">mud-PROPR</ta>
            <ta e="T651" id="Seg_5930" s="T650">water-DAT/LOC</ta>
            <ta e="T652" id="Seg_5931" s="T651">rinse-CVB.SEQ</ta>
            <ta e="T653" id="Seg_5932" s="T652">choke-CVB.SEQ</ta>
            <ta e="T654" id="Seg_5933" s="T653">out</ta>
            <ta e="T655" id="Seg_5934" s="T654">hold-CAUS-CVB.SEQ</ta>
            <ta e="T656" id="Seg_5935" s="T655">fly-CVB.SEQ</ta>
            <ta e="T657" id="Seg_5936" s="T656">stay-PST2.[3SG]</ta>
            <ta e="T658" id="Seg_5937" s="T657">well</ta>
            <ta e="T659" id="Seg_5938" s="T658">that</ta>
            <ta e="T660" id="Seg_5939" s="T659">fox.[NOM]</ta>
            <ta e="T661" id="Seg_5940" s="T660">though</ta>
            <ta e="T662" id="Seg_5941" s="T661">shore-DAT/LOC</ta>
            <ta e="T663" id="Seg_5942" s="T662">go.out-CVB.SEQ</ta>
            <ta e="T664" id="Seg_5943" s="T663">become.dry-PASS/REFL-CVB.SIM</ta>
            <ta e="T665" id="Seg_5944" s="T664">MOD</ta>
            <ta e="T666" id="Seg_5945" s="T665">apparently</ta>
            <ta e="T667" id="Seg_5946" s="T666">well</ta>
            <ta e="T668" id="Seg_5947" s="T667">that</ta>
            <ta e="T669" id="Seg_5948" s="T668">become.dry-PASS/REFL-CVB.SEQ-and.so.on-CVB.SEQ</ta>
            <ta e="T671" id="Seg_5949" s="T669">after</ta>
            <ta e="T672" id="Seg_5950" s="T671">revive-CVB.SEQ</ta>
            <ta e="T673" id="Seg_5951" s="T672">be.rich-CVB.SEQ-eat.ones.fill-CVB.SEQ</ta>
            <ta e="T674" id="Seg_5952" s="T673">live-PST2-3SG</ta>
            <ta e="T675" id="Seg_5953" s="T674">Q</ta>
            <ta e="T676" id="Seg_5954" s="T675">whereto</ta>
            <ta e="T677" id="Seg_5955" s="T676">become-PST2-3SG</ta>
            <ta e="T678" id="Seg_5956" s="T677">Q</ta>
            <ta e="T679" id="Seg_5957" s="T678">well</ta>
            <ta e="T680" id="Seg_5958" s="T679">tale-1PL.[NOM]</ta>
            <ta e="T681" id="Seg_5959" s="T680">last-3SG.[NOM]</ta>
         </annotation>
         <annotation name="gg" tierref="gg">
            <ta e="T1" id="Seg_5960" s="T0">dieses</ta>
            <ta e="T2" id="Seg_5961" s="T1">früher</ta>
            <ta e="T3" id="Seg_5962" s="T2">klein.[NOM]</ta>
            <ta e="T4" id="Seg_5963" s="T3">sein-PTCP.COND-1PL-ABL</ta>
            <ta e="T5" id="Seg_5964" s="T4">Alter-ABL</ta>
            <ta e="T6" id="Seg_5965" s="T5">Märchen-VBZ-EP-RECP/COLL-CVB.SEQ</ta>
            <ta e="T7" id="Seg_5966" s="T6">entstehen-PST2-1PL</ta>
            <ta e="T8" id="Seg_5967" s="T7">Q</ta>
            <ta e="T9" id="Seg_5968" s="T8">jenes</ta>
            <ta e="T10" id="Seg_5969" s="T9">dieses</ta>
            <ta e="T11" id="Seg_5970" s="T10">1PL.[NOM]</ta>
            <ta e="T12" id="Seg_5971" s="T11">klein.[NOM]</ta>
            <ta e="T13" id="Seg_5972" s="T12">sein-PTCP.COND-1SG-ABL</ta>
            <ta e="T14" id="Seg_5973" s="T13">was.[NOM]</ta>
            <ta e="T15" id="Seg_5974" s="T14">NEG</ta>
            <ta e="T16" id="Seg_5975" s="T15">spielen-NMNZ.[NOM]</ta>
            <ta e="T17" id="Seg_5976" s="T16">NEG.EX</ta>
            <ta e="T18" id="Seg_5977" s="T17">sein-PST1-3SG</ta>
            <ta e="T19" id="Seg_5978" s="T18">dieses.[NOM]</ta>
            <ta e="T20" id="Seg_5979" s="T19">Kind-PL-ACC</ta>
            <ta e="T21" id="Seg_5980" s="T20">schlafen-CAUS-EP-NEG-CVB.PURP-3PL</ta>
            <ta e="T22" id="Seg_5981" s="T21">und.so.weiter-NEG-CVB.PURP-3PL</ta>
            <ta e="T23" id="Seg_5982" s="T22">dieses.[NOM]</ta>
            <ta e="T24" id="Seg_5983" s="T23">Vater-1PL-PROPR.[NOM]</ta>
            <ta e="T25" id="Seg_5984" s="T24">Vater-1PL.[NOM]</ta>
            <ta e="T26" id="Seg_5985" s="T25">Vater-3SG-PROPR.[NOM]</ta>
            <ta e="T27" id="Seg_5986" s="T26">Kind-DAT/LOC</ta>
            <ta e="T28" id="Seg_5987" s="T27">Märchen-VBZ-PTCP.PRS</ta>
            <ta e="T29" id="Seg_5988" s="T28">Märchen-3PL.[NOM]</ta>
            <ta e="T30" id="Seg_5989" s="T29">sein-PST1-3SG</ta>
            <ta e="T31" id="Seg_5990" s="T30">besonders</ta>
            <ta e="T32" id="Seg_5991" s="T31">Kind.[NOM]</ta>
            <ta e="T33" id="Seg_5992" s="T32">Märchen-3SG.[NOM]</ta>
            <ta e="T34" id="Seg_5993" s="T33">sagen-CVB.SEQ</ta>
            <ta e="T35" id="Seg_5994" s="T34">vor.langer.Zeit</ta>
            <ta e="T36" id="Seg_5995" s="T35">Erde.[NOM]</ta>
            <ta e="T37" id="Seg_5996" s="T36">Himmel.[NOM]</ta>
            <ta e="T38" id="Seg_5997" s="T37">entstehen-PTCP.PRS-3SG-ACC</ta>
            <ta e="T39" id="Seg_5998" s="T38">als</ta>
            <ta e="T40" id="Seg_5999" s="T39">jeder-3SG.[NOM]</ta>
            <ta e="T41" id="Seg_6000" s="T40">Sprache-PROPR.[NOM]</ta>
            <ta e="T42" id="Seg_6001" s="T41">sein-PST1-3PL</ta>
            <ta e="T43" id="Seg_6002" s="T42">man.sagt</ta>
            <ta e="T44" id="Seg_6003" s="T43">Tier-PL.[NOM]</ta>
            <ta e="T45" id="Seg_6004" s="T44">jeder-3PL.[NOM]</ta>
            <ta e="T46" id="Seg_6005" s="T45">jenes</ta>
            <ta e="T47" id="Seg_6006" s="T46">Tier-3PL.[NOM]</ta>
            <ta e="T48" id="Seg_6007" s="T47">äh</ta>
            <ta e="T49" id="Seg_6008" s="T48">Erzählung-3PL.[NOM]</ta>
            <ta e="T50" id="Seg_6009" s="T49">jenes</ta>
            <ta e="T51" id="Seg_6010" s="T50">Tier-PL.[NOM]</ta>
            <ta e="T52" id="Seg_6011" s="T51">gehen-EP-PTCP.PST</ta>
            <ta e="T53" id="Seg_6012" s="T52">Reise-3PL-ACC</ta>
            <ta e="T54" id="Seg_6013" s="T53">jenes-ACC</ta>
            <ta e="T55" id="Seg_6014" s="T54">erzählen-PRS-3PL</ta>
            <ta e="T56" id="Seg_6015" s="T55">dann</ta>
            <ta e="T57" id="Seg_6016" s="T56">aber</ta>
            <ta e="T58" id="Seg_6017" s="T57">Fuchs.[NOM]</ta>
            <ta e="T59" id="Seg_6018" s="T58">leben-PST2.[3SG]</ta>
            <ta e="T60" id="Seg_6019" s="T59">man.sagt</ta>
            <ta e="T61" id="Seg_6020" s="T60">vor.langer.Zeit-vor.langer.Zeit</ta>
            <ta e="T62" id="Seg_6021" s="T61">Zeit-DAT/LOC</ta>
            <ta e="T63" id="Seg_6022" s="T62">dieses</ta>
            <ta e="T64" id="Seg_6023" s="T63">Fuchs.[NOM]</ta>
            <ta e="T65" id="Seg_6024" s="T64">aber</ta>
            <ta e="T66" id="Seg_6025" s="T65">Frühling.[NOM]</ta>
            <ta e="T67" id="Seg_6026" s="T66">Zeit-3SG.[NOM]</ta>
            <ta e="T68" id="Seg_6027" s="T67">kommen-CVB.SEQ</ta>
            <ta e="T69" id="Seg_6028" s="T68">sein-TEMP-3SG</ta>
            <ta e="T70" id="Seg_6029" s="T69">ganz</ta>
            <ta e="T71" id="Seg_6030" s="T70">woher</ta>
            <ta e="T72" id="Seg_6031" s="T71">NEG</ta>
            <ta e="T73" id="Seg_6032" s="T72">Nahrung.[NOM]</ta>
            <ta e="T74" id="Seg_6033" s="T73">nehmen-CVB.SEQ</ta>
            <ta e="T75" id="Seg_6034" s="T74">essen-PTCP.PRS</ta>
            <ta e="T76" id="Seg_6035" s="T75">Ort-3SG.[NOM]</ta>
            <ta e="T77" id="Seg_6036" s="T76">NEG.EX</ta>
            <ta e="T78" id="Seg_6037" s="T77">herbstlich</ta>
            <ta e="T79" id="Seg_6038" s="T78">dolganisch</ta>
            <ta e="T80" id="Seg_6039" s="T79">Nahrung-3SG.[NOM]</ta>
            <ta e="T81" id="Seg_6040" s="T80">enden-PTCP.PST</ta>
            <ta e="T82" id="Seg_6041" s="T81">sein-PST1-3SG</ta>
            <ta e="T83" id="Seg_6042" s="T82">dolganisch</ta>
            <ta e="T84" id="Seg_6043" s="T83">Nahrung-VBZ-PTCP.PST-3SG.[NOM]</ta>
            <ta e="T85" id="Seg_6044" s="T84">Wasser.[NOM]</ta>
            <ta e="T86" id="Seg_6045" s="T85">Schnee.[NOM]</ta>
            <ta e="T87" id="Seg_6046" s="T86">werden-PST2.[3SG]</ta>
            <ta e="T88" id="Seg_6047" s="T87">so</ta>
            <ta e="T89" id="Seg_6048" s="T88">machen-CVB.SEQ</ta>
            <ta e="T90" id="Seg_6049" s="T89">Nahrung-POSS</ta>
            <ta e="T91" id="Seg_6050" s="T90">NEG</ta>
            <ta e="T92" id="Seg_6051" s="T91">sein-CVB.SEQ</ta>
            <ta e="T93" id="Seg_6052" s="T92">hungern-CVB.SEQ</ta>
            <ta e="T94" id="Seg_6053" s="T93">Erde.[NOM]</ta>
            <ta e="T95" id="Seg_6054" s="T94">Himmel.[NOM]</ta>
            <ta e="T96" id="Seg_6055" s="T95">rennen-CVB.SEQ</ta>
            <ta e="T97" id="Seg_6056" s="T96">gehen-PST2-3SG</ta>
            <ta e="T98" id="Seg_6057" s="T97">Fuchs.[NOM]</ta>
            <ta e="T99" id="Seg_6058" s="T98">dieses</ta>
            <ta e="T100" id="Seg_6059" s="T99">rennen-CVB.SEQ</ta>
            <ta e="T101" id="Seg_6060" s="T100">gehen-CVB.SEQ</ta>
            <ta e="T103" id="Seg_6061" s="T101">aber</ta>
            <ta e="T104" id="Seg_6062" s="T103">oh</ta>
            <ta e="T105" id="Seg_6063" s="T104">dieses</ta>
            <ta e="T106" id="Seg_6064" s="T105">woher</ta>
            <ta e="T107" id="Seg_6065" s="T106">finden-FUT-1SG=Q</ta>
            <ta e="T108" id="Seg_6066" s="T107">denken-CVB.SEQ</ta>
            <ta e="T109" id="Seg_6067" s="T108">Wald.[NOM]</ta>
            <ta e="T110" id="Seg_6068" s="T109">Inneres-3SG-DAT/LOC</ta>
            <ta e="T111" id="Seg_6069" s="T110">hineingehen-CVB.SEQ</ta>
            <ta e="T112" id="Seg_6070" s="T111">Wald.[NOM]</ta>
            <ta e="T113" id="Seg_6071" s="T112">Inneres-3SG-DAT/LOC</ta>
            <ta e="T114" id="Seg_6072" s="T113">rennen-CVB.SEQ</ta>
            <ta e="T115" id="Seg_6073" s="T114">gehen-PRS.[3SG]</ta>
            <ta e="T116" id="Seg_6074" s="T115">hey</ta>
            <ta e="T117" id="Seg_6075" s="T116">sehen-PRS.[3SG]</ta>
            <ta e="T118" id="Seg_6076" s="T117">den.Kopf.heben-ITER-CVB.SIM-den.Kopf.heben-ITER-CVB.SIM</ta>
            <ta e="T119" id="Seg_6077" s="T118">rennen-PST2-3SG</ta>
            <ta e="T120" id="Seg_6078" s="T119">ausladend</ta>
            <ta e="T121" id="Seg_6079" s="T120">Lärche.[NOM]</ta>
            <ta e="T122" id="Seg_6080" s="T121">Spitze-3SG-DAT/LOC</ta>
            <ta e="T123" id="Seg_6081" s="T122">Raufußbussard.[NOM]</ta>
            <ta e="T124" id="Seg_6082" s="T123">gebären-CVB.SEQ</ta>
            <ta e="T125" id="Seg_6083" s="T124">nachdem</ta>
            <ta e="T126" id="Seg_6084" s="T125">liegen-PRS.[3SG]</ta>
            <ta e="T127" id="Seg_6085" s="T126">hey</ta>
            <ta e="T128" id="Seg_6086" s="T127">nun</ta>
            <ta e="T129" id="Seg_6087" s="T128">dann</ta>
            <ta e="T130" id="Seg_6088" s="T129">1SG.[NOM]</ta>
            <ta e="T131" id="Seg_6089" s="T130">wie</ta>
            <ta e="T132" id="Seg_6090" s="T131">machen-CVB.SEQ-1SG</ta>
            <ta e="T133" id="Seg_6091" s="T132">Betrug-VBZ-CVB.SEQ-1SG</ta>
            <ta e="T134" id="Seg_6092" s="T133">essen-FUT-1SG=Q</ta>
            <ta e="T135" id="Seg_6093" s="T134">dieses</ta>
            <ta e="T136" id="Seg_6094" s="T135">Raufußbussard-ABL</ta>
            <ta e="T137" id="Seg_6095" s="T136">was-ACC</ta>
            <ta e="T138" id="Seg_6096" s="T137">Ei-PART</ta>
            <ta e="T139" id="Seg_6097" s="T138">denken-CVB.SEQ</ta>
            <ta e="T140" id="Seg_6098" s="T139">dieses</ta>
            <ta e="T141" id="Seg_6099" s="T140">MOD</ta>
            <ta e="T142" id="Seg_6100" s="T141">Raufußbussard-DAT/LOC</ta>
            <ta e="T145" id="Seg_6101" s="T144">Raufußbussard-ABL</ta>
            <ta e="T146" id="Seg_6102" s="T145">Ei.[NOM]</ta>
            <ta e="T147" id="Seg_6103" s="T146">essen-CVB.PURP</ta>
            <ta e="T148" id="Seg_6104" s="T147">rennen-CVB.SEQ</ta>
            <ta e="T149" id="Seg_6105" s="T148">kommen-CVB.SEQ</ta>
            <ta e="T150" id="Seg_6106" s="T149">meist</ta>
            <ta e="T151" id="Seg_6107" s="T150">wer-3SG-GEN</ta>
            <ta e="T152" id="Seg_6108" s="T151">Unterteil-3SG-DAT/LOC</ta>
            <ta e="T153" id="Seg_6109" s="T152">kommen-PST2.[3SG]</ta>
            <ta e="T154" id="Seg_6110" s="T153">Lärche-3SG-GEN</ta>
            <ta e="T155" id="Seg_6111" s="T154">Unterteil-3SG-DAT/LOC</ta>
            <ta e="T156" id="Seg_6112" s="T155">kommen-PST2.[3SG]</ta>
            <ta e="T157" id="Seg_6113" s="T156">so</ta>
            <ta e="T158" id="Seg_6114" s="T157">machen-CVB.SEQ</ta>
            <ta e="T159" id="Seg_6115" s="T158">nachdem</ta>
            <ta e="T160" id="Seg_6116" s="T159">Schwanz-3SG-INSTR</ta>
            <ta e="T161" id="Seg_6117" s="T160">Lärche-3SG-ACC</ta>
            <ta e="T162" id="Seg_6118" s="T161">prügeln-CVB.SIM</ta>
            <ta e="T163" id="Seg_6119" s="T162">stehen-PST2-3SG</ta>
            <ta e="T164" id="Seg_6120" s="T163">Raufußbussard.[NOM]</ta>
            <ta e="T165" id="Seg_6121" s="T164">Raufußbussard.[NOM]</ta>
            <ta e="T166" id="Seg_6122" s="T165">fix</ta>
            <ta e="T167" id="Seg_6123" s="T166">MOD</ta>
            <ta e="T168" id="Seg_6124" s="T167">holen.[IMP.2SG]</ta>
            <ta e="T169" id="Seg_6125" s="T168">eins</ta>
            <ta e="T170" id="Seg_6126" s="T169">Kind-2SG-ACC</ta>
            <ta e="T171" id="Seg_6127" s="T170">1SG.[NOM]</ta>
            <ta e="T172" id="Seg_6128" s="T171">Schwanz-EP-1SG.[NOM]</ta>
            <ta e="T173" id="Seg_6129" s="T172">russisch</ta>
            <ta e="T174" id="Seg_6130" s="T173">Schwert-3SG-COMP</ta>
            <ta e="T175" id="Seg_6131" s="T174">scharf.[NOM]</ta>
            <ta e="T176" id="Seg_6132" s="T175">Lärche-COM</ta>
            <ta e="T177" id="Seg_6133" s="T176">EMPH-COM</ta>
            <ta e="T178" id="Seg_6134" s="T177">schneiden-CVB.SIM</ta>
            <ta e="T179" id="Seg_6135" s="T178">schlagen-CVB.SEQ</ta>
            <ta e="T180" id="Seg_6136" s="T179">fallen-EP-CAUS-FUT-1SG</ta>
            <ta e="T181" id="Seg_6137" s="T180">selbst-COM-2SG-ACC</ta>
            <ta e="T182" id="Seg_6138" s="T181">essen-FUT-1SG</ta>
            <ta e="T183" id="Seg_6139" s="T182">Raufußbussard.[NOM]</ta>
            <ta e="T184" id="Seg_6140" s="T183">zu.Ende.gehen-FUT.[3SG]</ta>
            <ta e="T185" id="Seg_6141" s="T184">Q</ta>
            <ta e="T186" id="Seg_6142" s="T185">weinen-CVB.SIM-weinen-CVB.SIM</ta>
            <ta e="T187" id="Seg_6143" s="T186">eins</ta>
            <ta e="T188" id="Seg_6144" s="T187">Kind-3SG-ACC</ta>
            <ta e="T189" id="Seg_6145" s="T188">rollen-NMNZ.[NOM]</ta>
            <ta e="T190" id="Seg_6146" s="T189">machen-CAUS-PST2.[3SG]</ta>
            <ta e="T191" id="Seg_6147" s="T190">eins</ta>
            <ta e="T192" id="Seg_6148" s="T191">Ei-3SG-ACC</ta>
            <ta e="T193" id="Seg_6149" s="T192">essen-PST2.[3SG]</ta>
            <ta e="T194" id="Seg_6150" s="T193">so</ta>
            <ta e="T195" id="Seg_6151" s="T194">machen-CVB.SEQ</ta>
            <ta e="T196" id="Seg_6152" s="T195">nachdem</ta>
            <ta e="T197" id="Seg_6153" s="T196">Erde.[NOM]</ta>
            <ta e="T198" id="Seg_6154" s="T197">Himmel.[NOM]</ta>
            <ta e="T199" id="Seg_6155" s="T198">rennen-CVB.SEQ</ta>
            <ta e="T200" id="Seg_6156" s="T199">bleiben-PST2.[3SG]</ta>
            <ta e="T201" id="Seg_6157" s="T200">nächster.Morgen-3SG-ACC</ta>
            <ta e="T202" id="Seg_6158" s="T201">aufstehen-PRS.[3SG]</ta>
            <ta e="T203" id="Seg_6159" s="T202">wieder</ta>
            <ta e="T204" id="Seg_6160" s="T203">Raufußbussard-ACC</ta>
            <ta e="T205" id="Seg_6161" s="T204">Betrug-VBZ</ta>
            <ta e="T206" id="Seg_6162" s="T205">Raufußbussard-3SG-ACC</ta>
            <ta e="T207" id="Seg_6163" s="T206">Betrug-VBZ-CVB.PURP</ta>
            <ta e="T208" id="Seg_6164" s="T207">wieder</ta>
            <ta e="T209" id="Seg_6165" s="T208">rennen-CVB.SEQ</ta>
            <ta e="T210" id="Seg_6166" s="T209">kommen-PRS.[3SG]</ta>
            <ta e="T211" id="Seg_6167" s="T210">Raufußbussard.[NOM]</ta>
            <ta e="T212" id="Seg_6168" s="T211">Raufußbussard.[NOM]</ta>
            <ta e="T213" id="Seg_6169" s="T212">fix</ta>
            <ta e="T214" id="Seg_6170" s="T213">MOD</ta>
            <ta e="T215" id="Seg_6171" s="T214">holen.[IMP.2SG]</ta>
            <ta e="T216" id="Seg_6172" s="T215">eins</ta>
            <ta e="T217" id="Seg_6173" s="T216">Kind-2SG-ACC</ta>
            <ta e="T218" id="Seg_6174" s="T217">1SG.[NOM]</ta>
            <ta e="T219" id="Seg_6175" s="T218">Schwanz-EP-1SG.[NOM]</ta>
            <ta e="T220" id="Seg_6176" s="T219">russisch</ta>
            <ta e="T221" id="Seg_6177" s="T220">Schwert-3SG-COMP</ta>
            <ta e="T222" id="Seg_6178" s="T221">scharf.[NOM]</ta>
            <ta e="T223" id="Seg_6179" s="T222">Lärche-COM-2SG-ACC-EMPH-COM-2SG-ACC</ta>
            <ta e="T224" id="Seg_6180" s="T223">schneiden-CVB.SIM</ta>
            <ta e="T225" id="Seg_6181" s="T224">schlagen-CVB.SEQ</ta>
            <ta e="T226" id="Seg_6182" s="T225">fallen-EP-CAUS-CVB.SEQ-1SG</ta>
            <ta e="T227" id="Seg_6183" s="T226">jetzt</ta>
            <ta e="T228" id="Seg_6184" s="T227">selbst-COM-2SG-ACC</ta>
            <ta e="T229" id="Seg_6185" s="T228">essen-FUT-1SG</ta>
            <ta e="T230" id="Seg_6186" s="T229">wieder</ta>
            <ta e="T231" id="Seg_6187" s="T230">eins</ta>
            <ta e="T232" id="Seg_6188" s="T231">Kind-3SG-ACC</ta>
            <ta e="T233" id="Seg_6189" s="T232">stoßen-CVB.SEQ</ta>
            <ta e="T234" id="Seg_6190" s="T233">werfen-PST2.[3SG]</ta>
            <ta e="T235" id="Seg_6191" s="T234">zwei</ta>
            <ta e="T236" id="Seg_6192" s="T235">Kind-3SG-ACC</ta>
            <ta e="T237" id="Seg_6193" s="T236">essen-PST2.[3SG]</ta>
            <ta e="T238" id="Seg_6194" s="T237">Fuchs.[NOM]</ta>
            <ta e="T239" id="Seg_6195" s="T238">Raufußbussard.[NOM]</ta>
            <ta e="T240" id="Seg_6196" s="T239">weinen-CVB.SIM</ta>
            <ta e="T241" id="Seg_6197" s="T240">sitzen-EMOT-PST2.[3SG]</ta>
            <ta e="T242" id="Seg_6198" s="T241">EMPH</ta>
            <ta e="T243" id="Seg_6199" s="T242">nun</ta>
            <ta e="T244" id="Seg_6200" s="T243">dieses</ta>
            <ta e="T245" id="Seg_6201" s="T244">jetzt</ta>
            <ta e="T246" id="Seg_6202" s="T245">aber</ta>
            <ta e="T247" id="Seg_6203" s="T246">selbst-COM-1SG-ACC</ta>
            <ta e="T248" id="Seg_6204" s="T247">essen-CVB.SEQ-ACC</ta>
            <ta e="T249" id="Seg_6205" s="T248">sagen-CVB.SEQ</ta>
            <ta e="T250" id="Seg_6206" s="T249">weinen-CVB.SIM</ta>
            <ta e="T251" id="Seg_6207" s="T250">sitzen-PST2.[3SG]</ta>
            <ta e="T252" id="Seg_6208" s="T251">na</ta>
            <ta e="T253" id="Seg_6209" s="T252">dieses</ta>
            <ta e="T254" id="Seg_6210" s="T253">sitzen-TEMP-3SG</ta>
            <ta e="T255" id="Seg_6211" s="T254">Unglückshäher.[NOM]</ta>
            <ta e="T256" id="Seg_6212" s="T255">fliegen-CVB.SEQ</ta>
            <ta e="T257" id="Seg_6213" s="T256">kommen-PST2.[3SG]</ta>
            <ta e="T258" id="Seg_6214" s="T257">hey</ta>
            <ta e="T259" id="Seg_6215" s="T258">Raufußbussard.[NOM]</ta>
            <ta e="T260" id="Seg_6216" s="T259">was-ABL</ta>
            <ta e="T261" id="Seg_6217" s="T260">weinen-PRS-2SG</ta>
            <ta e="T262" id="Seg_6218" s="T261">EMPH</ta>
            <ta e="T263" id="Seg_6219" s="T262">2SG.[NOM]</ta>
            <ta e="T264" id="Seg_6220" s="T263">dieses</ta>
            <ta e="T265" id="Seg_6221" s="T264">solch</ta>
            <ta e="T266" id="Seg_6222" s="T265">schön</ta>
            <ta e="T267" id="Seg_6223" s="T266">Sonne-PROPR</ta>
            <ta e="T268" id="Seg_6224" s="T267">Tag.[NOM]</ta>
            <ta e="T269" id="Seg_6225" s="T268">Sonnenaufgang-3SG.[NOM]</ta>
            <ta e="T270" id="Seg_6226" s="T269">aufstehen-TEMP-3SG</ta>
            <ta e="T271" id="Seg_6227" s="T270">Kind-PL-EP-2SG.[NOM]</ta>
            <ta e="T272" id="Seg_6228" s="T271">und</ta>
            <ta e="T273" id="Seg_6229" s="T272">warm-DAT/LOC</ta>
            <ta e="T274" id="Seg_6230" s="T273">liegen-PRS-3PL</ta>
            <ta e="T275" id="Seg_6231" s="T274">selbst-2SG.[NOM]</ta>
            <ta e="T276" id="Seg_6232" s="T275">und</ta>
            <ta e="T277" id="Seg_6233" s="T276">Nahrung-ACC</ta>
            <ta e="T278" id="Seg_6234" s="T277">EMPH</ta>
            <ta e="T279" id="Seg_6235" s="T278">holen-CVB.SEQ</ta>
            <ta e="T280" id="Seg_6236" s="T279">kommen-PTCP.PRS.[NOM]</ta>
            <ta e="T281" id="Seg_6237" s="T280">ähnlich</ta>
            <ta e="T282" id="Seg_6238" s="T281">EMPH</ta>
            <ta e="T283" id="Seg_6239" s="T282">was-ABL</ta>
            <ta e="T284" id="Seg_6240" s="T283">weinen-PRS-2SG</ta>
            <ta e="T285" id="Seg_6241" s="T284">doch</ta>
            <ta e="T286" id="Seg_6242" s="T285">solch</ta>
            <ta e="T287" id="Seg_6243" s="T286">fröhlich-DAT/LOC</ta>
            <ta e="T288" id="Seg_6244" s="T287">leben-CVB.SEQ-2SG</ta>
            <ta e="T289" id="Seg_6245" s="T288">sagen-PRS.[3SG]</ta>
            <ta e="T290" id="Seg_6246" s="T289">2SG.[NOM]</ta>
            <ta e="T291" id="Seg_6247" s="T290">wie</ta>
            <ta e="T292" id="Seg_6248" s="T291">weinen-EP-CAUS-NEG-2SG</ta>
            <ta e="T293" id="Seg_6249" s="T292">sehen-NEG-2SG</ta>
            <ta e="T294" id="Seg_6250" s="T293">Q</ta>
            <ta e="T295" id="Seg_6251" s="T294">1SG-ACC</ta>
            <ta e="T296" id="Seg_6252" s="T295">aber</ta>
            <ta e="T297" id="Seg_6253" s="T296">Fuchs.[NOM]</ta>
            <ta e="T298" id="Seg_6254" s="T297">MOD</ta>
            <ta e="T299" id="Seg_6255" s="T298">zwei</ta>
            <ta e="T300" id="Seg_6256" s="T299">Tag-DAT/LOC</ta>
            <ta e="T301" id="Seg_6257" s="T300">zwei</ta>
            <ta e="T302" id="Seg_6258" s="T301">Kind-1SG-ACC</ta>
            <ta e="T303" id="Seg_6259" s="T302">essen-PST1-3SG</ta>
            <ta e="T304" id="Seg_6260" s="T303">jetzt</ta>
            <ta e="T305" id="Seg_6261" s="T304">heute</ta>
            <ta e="T306" id="Seg_6262" s="T305">wieder</ta>
            <ta e="T307" id="Seg_6263" s="T306">kommen-FUT-3SG</ta>
            <ta e="T308" id="Seg_6264" s="T307">essen-FUT-3SG</ta>
            <ta e="T309" id="Seg_6265" s="T308">Lärche-COM-1SG-ACC</ta>
            <ta e="T310" id="Seg_6266" s="T309">EMPH-COM-1SG-ACC</ta>
            <ta e="T311" id="Seg_6267" s="T310">schneiden-CVB.SIM</ta>
            <ta e="T312" id="Seg_6268" s="T311">schlagen-CVB.SEQ</ta>
            <ta e="T313" id="Seg_6269" s="T312">fallen-EP-CAUS-CVB.SEQ</ta>
            <ta e="T314" id="Seg_6270" s="T313">selbst-COM-1SG-ACC</ta>
            <ta e="T315" id="Seg_6271" s="T314">essen-CVB.PURP</ta>
            <ta e="T316" id="Seg_6272" s="T315">wollen-PST1-3SG</ta>
            <ta e="T317" id="Seg_6273" s="T316">sagen-PRS.[3SG]</ta>
            <ta e="T318" id="Seg_6274" s="T317">was-PROPR.[NOM]=Q</ta>
            <ta e="T319" id="Seg_6275" s="T318">jenes</ta>
            <ta e="T320" id="Seg_6276" s="T319">Fuchs-EP-2SG.[NOM]</ta>
            <ta e="T321" id="Seg_6277" s="T320">na</ta>
            <ta e="T322" id="Seg_6278" s="T321">Schwanz-3SG.[NOM]</ta>
            <ta e="T323" id="Seg_6279" s="T322">russisch</ta>
            <ta e="T324" id="Seg_6280" s="T323">Schwert-3SG-COMP</ta>
            <ta e="T325" id="Seg_6281" s="T324">scharf.[NOM]</ta>
            <ta e="T326" id="Seg_6282" s="T325">nun</ta>
            <ta e="T327" id="Seg_6283" s="T326">was.[NOM]</ta>
            <ta e="T328" id="Seg_6284" s="T327">Name-PROPR-3SG.[NOM]</ta>
            <ta e="T329" id="Seg_6285" s="T328">dumm</ta>
            <ta e="T330" id="Seg_6286" s="T329">Vogel-3SG.[NOM]</ta>
            <ta e="T331" id="Seg_6287" s="T330">geboren.werden-PST2-EP-2SG=Q</ta>
            <ta e="T332" id="Seg_6288" s="T331">2SG.[NOM]</ta>
            <ta e="T333" id="Seg_6289" s="T332">solch.[NOM]</ta>
            <ta e="T334" id="Seg_6290" s="T333">doch</ta>
            <ta e="T335" id="Seg_6291" s="T334">Raufußbussard.[NOM]</ta>
            <ta e="T336" id="Seg_6292" s="T335">wie</ta>
            <ta e="T337" id="Seg_6293" s="T336">machen-CVB.SIM</ta>
            <ta e="T338" id="Seg_6294" s="T337">Fuchs.[NOM]</ta>
            <ta e="T339" id="Seg_6295" s="T338">wie</ta>
            <ta e="T340" id="Seg_6296" s="T339">machen-CVB.SEQ</ta>
            <ta e="T341" id="Seg_6297" s="T340">2SG.[NOM]</ta>
            <ta e="T342" id="Seg_6298" s="T341">Lärche-2SG-ACC</ta>
            <ta e="T343" id="Seg_6299" s="T342">schneiden-CVB.SIM</ta>
            <ta e="T344" id="Seg_6300" s="T343">schlagen-FUT.[3SG]=Q</ta>
            <ta e="T345" id="Seg_6301" s="T344">Schwanz-3SG.[NOM]</ta>
            <ta e="T346" id="Seg_6302" s="T345">Tierhaar.[NOM]</ta>
            <ta e="T347" id="Seg_6303" s="T346">Tierhaar.[NOM]</ta>
            <ta e="T348" id="Seg_6304" s="T347">Schwanz.[NOM]</ta>
            <ta e="T349" id="Seg_6305" s="T348">was-ACC</ta>
            <ta e="T350" id="Seg_6306" s="T349">schaffen-FUT-2SG=Q</ta>
            <ta e="T351" id="Seg_6307" s="T350">sagen.[IMP.2SG]</ta>
            <ta e="T352" id="Seg_6308" s="T351">jetzt</ta>
            <ta e="T353" id="Seg_6309" s="T352">kommen-TEMP-3SG</ta>
            <ta e="T354" id="Seg_6310" s="T353">wer.[NOM]</ta>
            <ta e="T355" id="Seg_6311" s="T354">lehren-PST2-3SG=Q</ta>
            <ta e="T356" id="Seg_6312" s="T355">sagen-TEMP-3SG</ta>
            <ta e="T357" id="Seg_6313" s="T356">Unglückshäher.[NOM]</ta>
            <ta e="T358" id="Seg_6314" s="T357">lehren-PST2-3SG</ta>
            <ta e="T359" id="Seg_6315" s="T358">sagen-FUT.[IMP.2SG]</ta>
            <ta e="T360" id="Seg_6316" s="T359">doch</ta>
            <ta e="T361" id="Seg_6317" s="T360">Raufußbussard.[NOM]</ta>
            <ta e="T364" id="Seg_6318" s="T363">Raufußbussard.[NOM]</ta>
            <ta e="T365" id="Seg_6319" s="T364">doch</ta>
            <ta e="T366" id="Seg_6320" s="T365">besser</ta>
            <ta e="T367" id="Seg_6321" s="T366">ein.Bisschen</ta>
            <ta e="T368" id="Seg_6322" s="T367">werden-PST2.[3SG]</ta>
            <ta e="T369" id="Seg_6323" s="T368">sitzen-PST2.[3SG]</ta>
            <ta e="T370" id="Seg_6324" s="T369">Unglückshäher-3SG.[NOM]</ta>
            <ta e="T371" id="Seg_6325" s="T370">fliegen-CVB.SEQ</ta>
            <ta e="T372" id="Seg_6326" s="T371">bleiben-PST2.[3SG]</ta>
            <ta e="T373" id="Seg_6327" s="T372">oh</ta>
            <ta e="T374" id="Seg_6328" s="T373">doch</ta>
            <ta e="T375" id="Seg_6329" s="T374">Fuchs.[NOM]</ta>
            <ta e="T376" id="Seg_6330" s="T375">böse.werden-CVB.PURP</ta>
            <ta e="T377" id="Seg_6331" s="T376">böse.werden-PTCP.PST</ta>
            <ta e="T378" id="Seg_6332" s="T377">werden-PST2.[3SG]</ta>
            <ta e="T379" id="Seg_6333" s="T378">sich.ärgern-CVB.PURP</ta>
            <ta e="T380" id="Seg_6334" s="T379">sich.ärgern-PTCP.PST</ta>
            <ta e="T381" id="Seg_6335" s="T380">werden-PST2.[3SG]</ta>
            <ta e="T382" id="Seg_6336" s="T381">unterschiedlich-unterschiedlich</ta>
            <ta e="T383" id="Seg_6337" s="T382">Moos-ACC</ta>
            <ta e="T384" id="Seg_6338" s="T383">stehen-EP-CAUS-CVB.SIM</ta>
            <ta e="T385" id="Seg_6339" s="T384">schlagen-CVB.SIM-schlagen-CVB.SIM</ta>
            <ta e="T386" id="Seg_6340" s="T385">vorderer</ta>
            <ta e="T387" id="Seg_6341" s="T386">hinterer</ta>
            <ta e="T388" id="Seg_6342" s="T387">Bein-3SG-INSTR</ta>
            <ta e="T389" id="Seg_6343" s="T388">zerreißen-CVB.SIM</ta>
            <ta e="T390" id="Seg_6344" s="T389">schlagen-CVB.SIM-schlagen-CVB.SIM</ta>
            <ta e="T391" id="Seg_6345" s="T390">kommen-EMOT-PST2.[3SG]</ta>
            <ta e="T392" id="Seg_6346" s="T391">EMPH</ta>
            <ta e="T393" id="Seg_6347" s="T392">Raufußbussard-3SG-DAT/LOC</ta>
            <ta e="T394" id="Seg_6348" s="T393">Raufußbussard-ACC</ta>
            <ta e="T395" id="Seg_6349" s="T394">erschrecken-CVB.PURP</ta>
            <ta e="T396" id="Seg_6350" s="T395">Raufußbussard.[NOM]</ta>
            <ta e="T397" id="Seg_6351" s="T396">Raufußbussard.[NOM]</ta>
            <ta e="T398" id="Seg_6352" s="T397">solch-solch</ta>
            <ta e="T399" id="Seg_6353" s="T398">Mutter-PROPR.[NOM]</ta>
            <ta e="T400" id="Seg_6354" s="T399">na</ta>
            <ta e="T401" id="Seg_6355" s="T400">1SG.[NOM]</ta>
            <ta e="T402" id="Seg_6356" s="T401">aber</ta>
            <ta e="T403" id="Seg_6357" s="T402">wieder</ta>
            <ta e="T404" id="Seg_6358" s="T403">ganz</ta>
            <ta e="T405" id="Seg_6359" s="T404">böse.werden-CVB.PURP</ta>
            <ta e="T406" id="Seg_6360" s="T405">böse.werden-PST1-1SG</ta>
            <ta e="T407" id="Seg_6361" s="T406">holen.[IMP.2SG]</ta>
            <ta e="T408" id="Seg_6362" s="T407">eins</ta>
            <ta e="T409" id="Seg_6363" s="T408">Kind-PART</ta>
            <ta e="T410" id="Seg_6364" s="T409">1SG.[NOM]</ta>
            <ta e="T411" id="Seg_6365" s="T410">Schwanz-EP-1SG.[NOM]</ta>
            <ta e="T412" id="Seg_6366" s="T411">russisch</ta>
            <ta e="T413" id="Seg_6367" s="T412">Schwert-3SG-COMP</ta>
            <ta e="T414" id="Seg_6368" s="T413">scharf.[NOM]</ta>
            <ta e="T415" id="Seg_6369" s="T414">vor.Kurzem</ta>
            <ta e="T416" id="Seg_6370" s="T415">selbst-COM-2SG-ACC</ta>
            <ta e="T419" id="Seg_6371" s="T418">völlig</ta>
            <ta e="T420" id="Seg_6372" s="T419">schlagen-CVB.SEQ-1SG</ta>
            <ta e="T421" id="Seg_6373" s="T420">selbst-COM-2SG-ACC</ta>
            <ta e="T422" id="Seg_6374" s="T421">essen-FUT-1SG</ta>
            <ta e="T423" id="Seg_6375" s="T422">sogar</ta>
            <ta e="T424" id="Seg_6376" s="T423">EMPH</ta>
            <ta e="T425" id="Seg_6377" s="T424">dumm-VBZ-CVB.SEQ-1SG</ta>
            <ta e="T426" id="Seg_6378" s="T425">ganz</ta>
            <ta e="T430" id="Seg_6379" s="T429">lieb</ta>
            <ta e="T431" id="Seg_6380" s="T430">zwei</ta>
            <ta e="T432" id="Seg_6381" s="T431">Kind-1SG-ACC</ta>
            <ta e="T433" id="Seg_6382" s="T432">essen-CAUS-PST1-1SG</ta>
            <ta e="T434" id="Seg_6383" s="T433">1SG.[NOM]</ta>
            <ta e="T435" id="Seg_6384" s="T434">2SG-DAT/LOC</ta>
            <ta e="T436" id="Seg_6385" s="T435">selbst-1SG.[NOM]</ta>
            <ta e="T437" id="Seg_6386" s="T436">dumm-1SG-DAT/LOC</ta>
            <ta e="T438" id="Seg_6387" s="T437">2SG.[NOM]</ta>
            <ta e="T439" id="Seg_6388" s="T438">aber</ta>
            <ta e="T440" id="Seg_6389" s="T439">Tierhaar.[NOM]</ta>
            <ta e="T441" id="Seg_6390" s="T440">Schwanz-2SG</ta>
            <ta e="T442" id="Seg_6391" s="T441">EMPH</ta>
            <ta e="T443" id="Seg_6392" s="T442">Tierhaar.[NOM]</ta>
            <ta e="T444" id="Seg_6393" s="T443">Schwanz.[NOM]</ta>
            <ta e="T445" id="Seg_6394" s="T444">was-ACC</ta>
            <ta e="T446" id="Seg_6395" s="T445">schaffen-FUT-2SG=Q</ta>
            <ta e="T447" id="Seg_6396" s="T446">Baum-ACC</ta>
            <ta e="T448" id="Seg_6397" s="T447">schaffen-FUT-2SG</ta>
            <ta e="T449" id="Seg_6398" s="T448">Q</ta>
            <ta e="T450" id="Seg_6399" s="T449">sagen-PRS.[3SG]</ta>
            <ta e="T451" id="Seg_6400" s="T450">EMPH</ta>
            <ta e="T452" id="Seg_6401" s="T451">oh.mein.Gott</ta>
            <ta e="T453" id="Seg_6402" s="T452">was.[NOM]</ta>
            <ta e="T454" id="Seg_6403" s="T453">Name-PROPR-3SG.[NOM]</ta>
            <ta e="T455" id="Seg_6404" s="T454">Verstand.[NOM]</ta>
            <ta e="T456" id="Seg_6405" s="T455">geben-PST2-3SG=Q</ta>
            <ta e="T457" id="Seg_6406" s="T456">2SG-DAT/LOC</ta>
            <ta e="T458" id="Seg_6407" s="T457">wie</ta>
            <ta e="T459" id="Seg_6408" s="T458">machen-CVB.SEQ-2SG</ta>
            <ta e="T460" id="Seg_6409" s="T459">Verstand-ACC</ta>
            <ta e="T461" id="Seg_6410" s="T460">finden-PST2-EP-2SG=Q</ta>
            <ta e="T462" id="Seg_6411" s="T461">sagen-PRS.[3SG]</ta>
            <ta e="T463" id="Seg_6412" s="T462">mein</ta>
            <ta e="T464" id="Seg_6413" s="T463">Schwanz-EP-1SG.[NOM]</ta>
            <ta e="T465" id="Seg_6414" s="T464">Tierhaar-3SG-ACC</ta>
            <ta e="T466" id="Seg_6415" s="T465">woher</ta>
            <ta e="T467" id="Seg_6416" s="T466">wissen-PRS-2SG</ta>
            <ta e="T468" id="Seg_6417" s="T467">na</ta>
            <ta e="T469" id="Seg_6418" s="T468">1SG-ACC</ta>
            <ta e="T470" id="Seg_6419" s="T469">Unglückshäher.[NOM]</ta>
            <ta e="T471" id="Seg_6420" s="T470">lehren-PST2-3SG</ta>
            <ta e="T472" id="Seg_6421" s="T471">wer.[NOM]</ta>
            <ta e="T473" id="Seg_6422" s="T472">geben-FUT.[3SG]=Q</ta>
            <ta e="T474" id="Seg_6423" s="T473">1SG-DAT/LOC</ta>
            <ta e="T475" id="Seg_6424" s="T474">Verstand.[NOM]</ta>
            <ta e="T476" id="Seg_6425" s="T475">Unglückshäher.[NOM]</ta>
            <ta e="T477" id="Seg_6426" s="T476">lehren-PST2-3SG</ta>
            <ta e="T478" id="Seg_6427" s="T477">hey</ta>
            <ta e="T479" id="Seg_6428" s="T478">solch-solch</ta>
            <ta e="T480" id="Seg_6429" s="T479">EMPH</ta>
            <ta e="T481" id="Seg_6430" s="T480">Unglückshäher-ACC</ta>
            <ta e="T482" id="Seg_6431" s="T481">was.für.ein</ta>
            <ta e="T483" id="Seg_6432" s="T482">Ort-ABL</ta>
            <ta e="T484" id="Seg_6433" s="T483">finden-FUT-1SG=Q</ta>
            <ta e="T485" id="Seg_6434" s="T484">doch</ta>
            <ta e="T486" id="Seg_6435" s="T485">warte</ta>
            <ta e="T487" id="Seg_6436" s="T486">doch</ta>
            <ta e="T488" id="Seg_6437" s="T487">sich.treffen-FUT-1SG</ta>
            <ta e="T489" id="Seg_6438" s="T488">1SG.[NOM]</ta>
            <ta e="T490" id="Seg_6439" s="T489">3SG-DAT/LOC</ta>
            <ta e="T491" id="Seg_6440" s="T490">EMPH</ta>
            <ta e="T492" id="Seg_6441" s="T491">so</ta>
            <ta e="T493" id="Seg_6442" s="T492">machen-PST2.[3SG]</ta>
            <ta e="T494" id="Seg_6443" s="T493">und</ta>
            <ta e="T495" id="Seg_6444" s="T494">Erde.[NOM]</ta>
            <ta e="T496" id="Seg_6445" s="T495">Himmel.[NOM]</ta>
            <ta e="T497" id="Seg_6446" s="T496">rennen-CVB.SEQ</ta>
            <ta e="T498" id="Seg_6447" s="T497">bleiben-PST2.[3SG]</ta>
            <ta e="T499" id="Seg_6448" s="T498">Erde.[NOM]</ta>
            <ta e="T500" id="Seg_6449" s="T499">Himmel.[NOM]</ta>
            <ta e="T501" id="Seg_6450" s="T500">rennen-CVB.SEQ</ta>
            <ta e="T502" id="Seg_6451" s="T501">gehen-CVB.SEQ</ta>
            <ta e="T503" id="Seg_6452" s="T502">sein-PST1-3SG</ta>
            <ta e="T504" id="Seg_6453" s="T503">EMPH-rund</ta>
            <ta e="T505" id="Seg_6454" s="T504">See-DAT/LOC</ta>
            <ta e="T507" id="Seg_6455" s="T505">ankommen-CVB.SEQ</ta>
            <ta e="T508" id="Seg_6456" s="T507">dieses</ta>
            <ta e="T509" id="Seg_6457" s="T508">See.[NOM]</ta>
            <ta e="T510" id="Seg_6458" s="T509">was-ABL</ta>
            <ta e="T511" id="Seg_6459" s="T510">NEG</ta>
            <ta e="T512" id="Seg_6460" s="T511">groß</ta>
            <ta e="T513" id="Seg_6461" s="T512">Berg-PROPR.[NOM]</ta>
            <ta e="T514" id="Seg_6462" s="T513">Frühling.[NOM]</ta>
            <ta e="T515" id="Seg_6463" s="T514">Zeit-3SG-DAT/LOC</ta>
            <ta e="T516" id="Seg_6464" s="T515">sein-PST1-3SG</ta>
            <ta e="T517" id="Seg_6465" s="T516">Hügel-3SG.[NOM]</ta>
            <ta e="T518" id="Seg_6466" s="T517">ganz</ta>
            <ta e="T519" id="Seg_6467" s="T518">hinausgehen-EP-PST2.[3SG]</ta>
            <ta e="T520" id="Seg_6468" s="T519">dieses</ta>
            <ta e="T521" id="Seg_6469" s="T520">Hügel-ACC</ta>
            <ta e="T522" id="Seg_6470" s="T521">aber</ta>
            <ta e="T523" id="Seg_6471" s="T522">rennen-CVB.SIM</ta>
            <ta e="T524" id="Seg_6472" s="T523">gehen-CVB.SEQ</ta>
            <ta e="T525" id="Seg_6473" s="T524">ganz</ta>
            <ta e="T526" id="Seg_6474" s="T525">meist</ta>
            <ta e="T527" id="Seg_6475" s="T526">schön</ta>
            <ta e="T528" id="Seg_6476" s="T527">Loch.in.der.Schneedecke-DAT/LOC</ta>
            <ta e="T529" id="Seg_6477" s="T528">aber</ta>
            <ta e="T530" id="Seg_6478" s="T529">sterben-PTCP.PST</ta>
            <ta e="T531" id="Seg_6479" s="T530">Fuchs.[NOM]</ta>
            <ta e="T532" id="Seg_6480" s="T531">werden-CVB.SEQ</ta>
            <ta e="T533" id="Seg_6481" s="T532">nachdem</ta>
            <ta e="T534" id="Seg_6482" s="T533">sich.ausbreiten-NMNZ.[NOM]</ta>
            <ta e="T535" id="Seg_6483" s="T534">machen-CVB.SIM</ta>
            <ta e="T536" id="Seg_6484" s="T535">fallen-PST2.[3SG]</ta>
            <ta e="T537" id="Seg_6485" s="T536">ganz</ta>
            <ta e="T538" id="Seg_6486" s="T537">Insekten-DAT/LOC</ta>
            <ta e="T539" id="Seg_6487" s="T538">jeder-3SG-DAT/LOC</ta>
            <ta e="T540" id="Seg_6488" s="T539">verstreuen-PASS-CVB.SIM</ta>
            <ta e="T541" id="Seg_6489" s="T540">liegen-PST2.[3SG]</ta>
            <ta e="T542" id="Seg_6490" s="T541">jenes</ta>
            <ta e="T543" id="Seg_6491" s="T542">machen-CVB.SEQ</ta>
            <ta e="T544" id="Seg_6492" s="T543">nachdem</ta>
            <ta e="T545" id="Seg_6493" s="T544">hey</ta>
            <ta e="T546" id="Seg_6494" s="T545">Eichelhäher-PL.[NOM]</ta>
            <ta e="T547" id="Seg_6495" s="T546">EMPH</ta>
            <ta e="T548" id="Seg_6496" s="T547">fliegen-CVB.SIM</ta>
            <ta e="T549" id="Seg_6497" s="T548">gehen-CVB.SEQ</ta>
            <ta e="T550" id="Seg_6498" s="T549">sein-CVB.SEQ</ta>
            <ta e="T551" id="Seg_6499" s="T550">sehen-PRS.[3SG]</ta>
            <ta e="T552" id="Seg_6500" s="T551">Fuchs.[NOM]</ta>
            <ta e="T553" id="Seg_6501" s="T552">sterben-CVB.SEQ</ta>
            <ta e="T554" id="Seg_6502" s="T553">nachdem</ta>
            <ta e="T555" id="Seg_6503" s="T554">sich.ausbreiten-CVB.SEQ</ta>
            <ta e="T556" id="Seg_6504" s="T555">liegen-PRS.[3SG]</ta>
            <ta e="T557" id="Seg_6505" s="T556">oh</ta>
            <ta e="T558" id="Seg_6506" s="T557">dieses.[NOM]</ta>
            <ta e="T559" id="Seg_6507" s="T558">solange</ta>
            <ta e="T560" id="Seg_6508" s="T559">dieses.[NOM]</ta>
            <ta e="T561" id="Seg_6509" s="T560">hungern-CVB.SEQ</ta>
            <ta e="T562" id="Seg_6510" s="T561">aber</ta>
            <ta e="T563" id="Seg_6511" s="T562">Raufußbussard.[NOM]</ta>
            <ta e="T564" id="Seg_6512" s="T563">Kind-PL-3SG-ACC</ta>
            <ta e="T565" id="Seg_6513" s="T564">essen-CVB.SIM</ta>
            <ta e="T566" id="Seg_6514" s="T565">gehen-EP-PST2-3SG</ta>
            <ta e="T567" id="Seg_6515" s="T566">MOD</ta>
            <ta e="T568" id="Seg_6516" s="T567">jetzt</ta>
            <ta e="T569" id="Seg_6517" s="T568">Raufußbussard-3SG.[NOM]</ta>
            <ta e="T570" id="Seg_6518" s="T569">aber</ta>
            <ta e="T571" id="Seg_6519" s="T570">1SG-ABL</ta>
            <ta e="T572" id="Seg_6520" s="T571">Verstand.[NOM]</ta>
            <ta e="T573" id="Seg_6521" s="T572">nehmen-PRS.[3SG]</ta>
            <ta e="T574" id="Seg_6522" s="T573">eins</ta>
            <ta e="T575" id="Seg_6523" s="T574">NEG</ta>
            <ta e="T576" id="Seg_6524" s="T575">Kind-3SG-ACC</ta>
            <ta e="T577" id="Seg_6525" s="T576">geben-PST2.NEG.[3SG]</ta>
            <ta e="T578" id="Seg_6526" s="T577">dann</ta>
            <ta e="T579" id="Seg_6527" s="T578">woher</ta>
            <ta e="T580" id="Seg_6528" s="T579">nehmen-CVB.SEQ</ta>
            <ta e="T581" id="Seg_6529" s="T580">3SG.[NOM]</ta>
            <ta e="T582" id="Seg_6530" s="T581">EMPH</ta>
            <ta e="T583" id="Seg_6531" s="T582">hungern-PST2.[3SG]</ta>
            <ta e="T584" id="Seg_6532" s="T583">MOD</ta>
            <ta e="T585" id="Seg_6533" s="T584">woher</ta>
            <ta e="T586" id="Seg_6534" s="T585">nehmen-CVB.SEQ</ta>
            <ta e="T587" id="Seg_6535" s="T586">essen-FUT.[3SG]=Q</ta>
            <ta e="T588" id="Seg_6536" s="T587">doch</ta>
            <ta e="T589" id="Seg_6537" s="T588">hungern-CVB.SEQ</ta>
            <ta e="T590" id="Seg_6538" s="T589">sterben-PST2.[3SG]</ta>
            <ta e="T591" id="Seg_6539" s="T590">doch</ta>
            <ta e="T592" id="Seg_6540" s="T591">2SG.[NOM]</ta>
            <ta e="T593" id="Seg_6541" s="T592">EMPH</ta>
            <ta e="T594" id="Seg_6542" s="T593">sterben-PRS-NEC-2SG</ta>
            <ta e="T595" id="Seg_6543" s="T594">sein-PST2.[3SG]</ta>
            <ta e="T596" id="Seg_6544" s="T595">denken-CVB.SEQ</ta>
            <ta e="T597" id="Seg_6545" s="T596">na</ta>
            <ta e="T598" id="Seg_6546" s="T597">kommen-PST2.[3SG]</ta>
            <ta e="T599" id="Seg_6547" s="T598">klopf-klopf</ta>
            <ta e="T600" id="Seg_6548" s="T599">klopf-klopf</ta>
            <ta e="T601" id="Seg_6549" s="T600">klopfen-PST2.[3SG]</ta>
            <ta e="T602" id="Seg_6550" s="T601">EMPH</ta>
            <ta e="T603" id="Seg_6551" s="T602">woher</ta>
            <ta e="T604" id="Seg_6552" s="T603">Auge-3SG-ABL</ta>
            <ta e="T605" id="Seg_6553" s="T604">essen-PTCP.FUT-DAT/LOC</ta>
            <ta e="T606" id="Seg_6554" s="T605">denken-CVB.SEQ</ta>
            <ta e="T607" id="Seg_6555" s="T606">Unterarm-DAT/LOC</ta>
            <ta e="T608" id="Seg_6556" s="T607">klopfen-MED-CVB.SIM</ta>
            <ta e="T609" id="Seg_6557" s="T608">gehen-TEMP-3SG</ta>
            <ta e="T610" id="Seg_6558" s="T609">ganz</ta>
            <ta e="T611" id="Seg_6559" s="T682">Fuchs.[NOM]</ta>
            <ta e="T612" id="Seg_6560" s="T611">aber</ta>
            <ta e="T613" id="Seg_6561" s="T612">letzter</ta>
            <ta e="T614" id="Seg_6562" s="T613">Schläue-3SG-INSTR</ta>
            <ta e="T615" id="Seg_6563" s="T614">letzter</ta>
            <ta e="T616" id="Seg_6564" s="T615">Ungeheuer-3SG-INSTR</ta>
            <ta e="T617" id="Seg_6565" s="T616">knurren-CVB.SIM</ta>
            <ta e="T618" id="Seg_6566" s="T617">machen-CVB.SIM</ta>
            <ta e="T619" id="Seg_6567" s="T618">springen-EMOT-PST2.[3SG]</ta>
            <ta e="T621" id="Seg_6568" s="T619">EMPH</ta>
            <ta e="T622" id="Seg_6569" s="T621">jener</ta>
            <ta e="T623" id="Seg_6570" s="T622">Raufußbussard.[NOM]</ta>
            <ta e="T624" id="Seg_6571" s="T623">aber</ta>
            <ta e="T625" id="Seg_6572" s="T624">wer.[NOM]</ta>
            <ta e="T626" id="Seg_6573" s="T625">aber</ta>
            <ta e="T627" id="Seg_6574" s="T626">Unglückshäher.[NOM]</ta>
            <ta e="T628" id="Seg_6575" s="T627">aber</ta>
            <ta e="T629" id="Seg_6576" s="T628">entkommen-CVB.SEQ</ta>
            <ta e="T630" id="Seg_6577" s="T629">schaffen-VBZ-NEG.CVB.SIM</ta>
            <ta e="T631" id="Seg_6578" s="T630">Schwanz-3SG-ABL</ta>
            <ta e="T632" id="Seg_6579" s="T631">fangen-CAUS-PST2.[3SG]</ta>
            <ta e="T633" id="Seg_6580" s="T632">so</ta>
            <ta e="T634" id="Seg_6581" s="T633">machen-CVB.SEQ</ta>
            <ta e="T635" id="Seg_6582" s="T634">dieses-ACC</ta>
            <ta e="T636" id="Seg_6583" s="T635">raufen-FREQ-EP-RECP/COLL-CVB.SEQ</ta>
            <ta e="T637" id="Seg_6584" s="T636">raufen-EP-RECP/COLL-CVB.SEQ</ta>
            <ta e="T638" id="Seg_6585" s="T637">Wasser.[NOM]</ta>
            <ta e="T639" id="Seg_6586" s="T638">Inneres-3SG-DAT/LOC</ta>
            <ta e="T640" id="Seg_6587" s="T639">rollen-CAUS-CVB.SEQ</ta>
            <ta e="T641" id="Seg_6588" s="T640">fallen-EP-CAUS-CVB.SEQ</ta>
            <ta e="T642" id="Seg_6589" s="T641">Wasser.[NOM]</ta>
            <ta e="T645" id="Seg_6590" s="T644">Wasser.[NOM]</ta>
            <ta e="T646" id="Seg_6591" s="T645">Inneres-3SG-DAT/LOC</ta>
            <ta e="T647" id="Seg_6592" s="T646">fallen-EP-CAUS-CVB.SEQ</ta>
            <ta e="T648" id="Seg_6593" s="T647">dieses-DAT/LOC</ta>
            <ta e="T649" id="Seg_6594" s="T648">wässern-CVB.SEQ</ta>
            <ta e="T650" id="Seg_6595" s="T649">Schlamm-PROPR</ta>
            <ta e="T651" id="Seg_6596" s="T650">Wasser-DAT/LOC</ta>
            <ta e="T652" id="Seg_6597" s="T651">wässern-CVB.SEQ</ta>
            <ta e="T653" id="Seg_6598" s="T652">würgen-CVB.SEQ</ta>
            <ta e="T654" id="Seg_6599" s="T653">heraus</ta>
            <ta e="T655" id="Seg_6600" s="T654">halten-CAUS-CVB.SEQ</ta>
            <ta e="T656" id="Seg_6601" s="T655">fliegen-CVB.SEQ</ta>
            <ta e="T657" id="Seg_6602" s="T656">bleiben-PST2.[3SG]</ta>
            <ta e="T658" id="Seg_6603" s="T657">doch</ta>
            <ta e="T659" id="Seg_6604" s="T658">jenes</ta>
            <ta e="T660" id="Seg_6605" s="T659">Fuchs.[NOM]</ta>
            <ta e="T661" id="Seg_6606" s="T660">aber</ta>
            <ta e="T662" id="Seg_6607" s="T661">Ufer-DAT/LOC</ta>
            <ta e="T663" id="Seg_6608" s="T662">hinausgehen-CVB.SEQ</ta>
            <ta e="T664" id="Seg_6609" s="T663">trocken.werden-PASS/REFL-CVB.SIM</ta>
            <ta e="T665" id="Seg_6610" s="T664">MOD</ta>
            <ta e="T666" id="Seg_6611" s="T665">offenbar</ta>
            <ta e="T667" id="Seg_6612" s="T666">doch</ta>
            <ta e="T668" id="Seg_6613" s="T667">jenes</ta>
            <ta e="T669" id="Seg_6614" s="T668">trocken.werden-PASS/REFL-CVB.SEQ-und.so.weiter-CVB.SEQ</ta>
            <ta e="T671" id="Seg_6615" s="T669">nachdem</ta>
            <ta e="T672" id="Seg_6616" s="T671">wieder.aufleben-CVB.SEQ</ta>
            <ta e="T673" id="Seg_6617" s="T672">reich.sein-CVB.SEQ-sich.satt.essen-CVB.SEQ</ta>
            <ta e="T674" id="Seg_6618" s="T673">leben-PST2-3SG</ta>
            <ta e="T675" id="Seg_6619" s="T674">Q</ta>
            <ta e="T676" id="Seg_6620" s="T675">wohin</ta>
            <ta e="T677" id="Seg_6621" s="T676">werden-PST2-3SG</ta>
            <ta e="T678" id="Seg_6622" s="T677">Q</ta>
            <ta e="T679" id="Seg_6623" s="T678">doch</ta>
            <ta e="T680" id="Seg_6624" s="T679">Märchen-1PL.[NOM]</ta>
            <ta e="T681" id="Seg_6625" s="T680">letzter-3SG.[NOM]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_6626" s="T0">этот</ta>
            <ta e="T2" id="Seg_6627" s="T1">раньше</ta>
            <ta e="T3" id="Seg_6628" s="T2">маленький.[NOM]</ta>
            <ta e="T4" id="Seg_6629" s="T3">быть-PTCP.COND-1PL-ABL</ta>
            <ta e="T5" id="Seg_6630" s="T4">возраст-ABL</ta>
            <ta e="T6" id="Seg_6631" s="T5">сказка-VBZ-EP-RECP/COLL-CVB.SEQ</ta>
            <ta e="T7" id="Seg_6632" s="T6">создаваться-PST2-1PL</ta>
            <ta e="T8" id="Seg_6633" s="T7">Q</ta>
            <ta e="T9" id="Seg_6634" s="T8">тот</ta>
            <ta e="T10" id="Seg_6635" s="T9">этот</ta>
            <ta e="T11" id="Seg_6636" s="T10">1PL.[NOM]</ta>
            <ta e="T12" id="Seg_6637" s="T11">маленький.[NOM]</ta>
            <ta e="T13" id="Seg_6638" s="T12">быть-PTCP.COND-1SG-ABL</ta>
            <ta e="T14" id="Seg_6639" s="T13">что.[NOM]</ta>
            <ta e="T15" id="Seg_6640" s="T14">NEG</ta>
            <ta e="T16" id="Seg_6641" s="T15">играть-NMNZ.[NOM]</ta>
            <ta e="T17" id="Seg_6642" s="T16">NEG.EX</ta>
            <ta e="T18" id="Seg_6643" s="T17">быть-PST1-3SG</ta>
            <ta e="T19" id="Seg_6644" s="T18">этот.[NOM]</ta>
            <ta e="T20" id="Seg_6645" s="T19">ребенок-PL-ACC</ta>
            <ta e="T21" id="Seg_6646" s="T20">спать-CAUS-EP-NEG-CVB.PURP-3PL</ta>
            <ta e="T22" id="Seg_6647" s="T21">и.так.далее-NEG-CVB.PURP-3PL</ta>
            <ta e="T23" id="Seg_6648" s="T22">этот.[NOM]</ta>
            <ta e="T24" id="Seg_6649" s="T23">отец-1PL-PROPR.[NOM]</ta>
            <ta e="T25" id="Seg_6650" s="T24">отец-1PL.[NOM]</ta>
            <ta e="T26" id="Seg_6651" s="T25">отец-3SG-PROPR.[NOM]</ta>
            <ta e="T27" id="Seg_6652" s="T26">ребенок-DAT/LOC</ta>
            <ta e="T28" id="Seg_6653" s="T27">сказка-VBZ-PTCP.PRS</ta>
            <ta e="T29" id="Seg_6654" s="T28">сказка-3PL.[NOM]</ta>
            <ta e="T30" id="Seg_6655" s="T29">быть-PST1-3SG</ta>
            <ta e="T31" id="Seg_6656" s="T30">специально</ta>
            <ta e="T32" id="Seg_6657" s="T31">ребенок.[NOM]</ta>
            <ta e="T33" id="Seg_6658" s="T32">сказка-3SG.[NOM]</ta>
            <ta e="T34" id="Seg_6659" s="T33">говорить-CVB.SEQ</ta>
            <ta e="T35" id="Seg_6660" s="T34">давно</ta>
            <ta e="T36" id="Seg_6661" s="T35">земля.[NOM]</ta>
            <ta e="T37" id="Seg_6662" s="T36">небо.[NOM]</ta>
            <ta e="T38" id="Seg_6663" s="T37">создаваться-PTCP.PRS-3SG-ACC</ta>
            <ta e="T39" id="Seg_6664" s="T38">когда</ta>
            <ta e="T40" id="Seg_6665" s="T39">каждый-3SG.[NOM]</ta>
            <ta e="T41" id="Seg_6666" s="T40">язык-PROPR.[NOM]</ta>
            <ta e="T42" id="Seg_6667" s="T41">быть-PST1-3PL</ta>
            <ta e="T43" id="Seg_6668" s="T42">говорят</ta>
            <ta e="T44" id="Seg_6669" s="T43">животное-PL.[NOM]</ta>
            <ta e="T45" id="Seg_6670" s="T44">каждый-3PL.[NOM]</ta>
            <ta e="T46" id="Seg_6671" s="T45">тот</ta>
            <ta e="T47" id="Seg_6672" s="T46">животное-3PL.[NOM]</ta>
            <ta e="T48" id="Seg_6673" s="T47">ээ</ta>
            <ta e="T49" id="Seg_6674" s="T48">рассказ-3PL.[NOM]</ta>
            <ta e="T50" id="Seg_6675" s="T49">тот</ta>
            <ta e="T51" id="Seg_6676" s="T50">животное-PL.[NOM]</ta>
            <ta e="T52" id="Seg_6677" s="T51">идти-EP-PTCP.PST</ta>
            <ta e="T53" id="Seg_6678" s="T52">поездка-3PL-ACC</ta>
            <ta e="T54" id="Seg_6679" s="T53">тот-ACC</ta>
            <ta e="T55" id="Seg_6680" s="T54">рассказывать-PRS-3PL</ta>
            <ta e="T56" id="Seg_6681" s="T55">тогда</ta>
            <ta e="T57" id="Seg_6682" s="T56">однако</ta>
            <ta e="T58" id="Seg_6683" s="T57">лиса.[NOM]</ta>
            <ta e="T59" id="Seg_6684" s="T58">жить-PST2.[3SG]</ta>
            <ta e="T60" id="Seg_6685" s="T59">говорят</ta>
            <ta e="T61" id="Seg_6686" s="T60">давно-давно</ta>
            <ta e="T62" id="Seg_6687" s="T61">время-DAT/LOC</ta>
            <ta e="T63" id="Seg_6688" s="T62">этот</ta>
            <ta e="T64" id="Seg_6689" s="T63">лиса.[NOM]</ta>
            <ta e="T65" id="Seg_6690" s="T64">однако</ta>
            <ta e="T66" id="Seg_6691" s="T65">весна.[NOM]</ta>
            <ta e="T67" id="Seg_6692" s="T66">время-3SG.[NOM]</ta>
            <ta e="T68" id="Seg_6693" s="T67">приходить-CVB.SEQ</ta>
            <ta e="T69" id="Seg_6694" s="T68">быть-TEMP-3SG</ta>
            <ta e="T70" id="Seg_6695" s="T69">совсем</ta>
            <ta e="T71" id="Seg_6696" s="T70">откуда</ta>
            <ta e="T72" id="Seg_6697" s="T71">NEG</ta>
            <ta e="T73" id="Seg_6698" s="T72">пища.[NOM]</ta>
            <ta e="T74" id="Seg_6699" s="T73">взять-CVB.SEQ</ta>
            <ta e="T75" id="Seg_6700" s="T74">есть-PTCP.PRS</ta>
            <ta e="T76" id="Seg_6701" s="T75">место-3SG.[NOM]</ta>
            <ta e="T77" id="Seg_6702" s="T76">NEG.EX</ta>
            <ta e="T78" id="Seg_6703" s="T77">осенний</ta>
            <ta e="T79" id="Seg_6704" s="T78">долганский</ta>
            <ta e="T80" id="Seg_6705" s="T79">пища-3SG.[NOM]</ta>
            <ta e="T81" id="Seg_6706" s="T80">кончаться-PTCP.PST</ta>
            <ta e="T82" id="Seg_6707" s="T81">быть-PST1-3SG</ta>
            <ta e="T83" id="Seg_6708" s="T82">долганский</ta>
            <ta e="T84" id="Seg_6709" s="T83">пища-VBZ-PTCP.PST-3SG.[NOM]</ta>
            <ta e="T85" id="Seg_6710" s="T84">вода.[NOM]</ta>
            <ta e="T86" id="Seg_6711" s="T85">снег.[NOM]</ta>
            <ta e="T87" id="Seg_6712" s="T86">становиться-PST2.[3SG]</ta>
            <ta e="T88" id="Seg_6713" s="T87">так</ta>
            <ta e="T89" id="Seg_6714" s="T88">делать-CVB.SEQ</ta>
            <ta e="T90" id="Seg_6715" s="T89">пища-POSS</ta>
            <ta e="T91" id="Seg_6716" s="T90">NEG</ta>
            <ta e="T92" id="Seg_6717" s="T91">быть-CVB.SEQ</ta>
            <ta e="T93" id="Seg_6718" s="T92">голодать-CVB.SEQ</ta>
            <ta e="T94" id="Seg_6719" s="T93">земля.[NOM]</ta>
            <ta e="T95" id="Seg_6720" s="T94">небо.[NOM]</ta>
            <ta e="T96" id="Seg_6721" s="T95">бежать-CVB.SEQ</ta>
            <ta e="T97" id="Seg_6722" s="T96">идти-PST2-3SG</ta>
            <ta e="T98" id="Seg_6723" s="T97">лиса.[NOM]</ta>
            <ta e="T99" id="Seg_6724" s="T98">этот</ta>
            <ta e="T100" id="Seg_6725" s="T99">бежать-CVB.SEQ</ta>
            <ta e="T101" id="Seg_6726" s="T100">идти-CVB.SEQ</ta>
            <ta e="T103" id="Seg_6727" s="T101">однако</ta>
            <ta e="T104" id="Seg_6728" s="T103">о</ta>
            <ta e="T105" id="Seg_6729" s="T104">этот</ta>
            <ta e="T106" id="Seg_6730" s="T105">откуда</ta>
            <ta e="T107" id="Seg_6731" s="T106">найти-FUT-1SG=Q</ta>
            <ta e="T108" id="Seg_6732" s="T107">думать-CVB.SEQ</ta>
            <ta e="T109" id="Seg_6733" s="T108">лес.[NOM]</ta>
            <ta e="T110" id="Seg_6734" s="T109">нутро-3SG-DAT/LOC</ta>
            <ta e="T111" id="Seg_6735" s="T110">входить-CVB.SEQ</ta>
            <ta e="T112" id="Seg_6736" s="T111">лес.[NOM]</ta>
            <ta e="T113" id="Seg_6737" s="T112">нутро-3SG-DAT/LOC</ta>
            <ta e="T114" id="Seg_6738" s="T113">бежать-CVB.SEQ</ta>
            <ta e="T115" id="Seg_6739" s="T114">идти-PRS.[3SG]</ta>
            <ta e="T116" id="Seg_6740" s="T115">ой</ta>
            <ta e="T117" id="Seg_6741" s="T116">видеть-PRS.[3SG]</ta>
            <ta e="T118" id="Seg_6742" s="T117">поднимать.голову-ITER-CVB.SIM-поднимать.голову-ITER-CVB.SIM</ta>
            <ta e="T119" id="Seg_6743" s="T118">бежать-PST2-3SG</ta>
            <ta e="T120" id="Seg_6744" s="T119">развесистый</ta>
            <ta e="T121" id="Seg_6745" s="T120">лиственница.[NOM]</ta>
            <ta e="T122" id="Seg_6746" s="T121">верхушка-3SG-DAT/LOC</ta>
            <ta e="T123" id="Seg_6747" s="T122">мохнатый.канюк.[NOM]</ta>
            <ta e="T124" id="Seg_6748" s="T123">родить-CVB.SEQ</ta>
            <ta e="T125" id="Seg_6749" s="T124">после</ta>
            <ta e="T126" id="Seg_6750" s="T125">лежать-PRS.[3SG]</ta>
            <ta e="T127" id="Seg_6751" s="T126">ой</ta>
            <ta e="T128" id="Seg_6752" s="T127">ну</ta>
            <ta e="T129" id="Seg_6753" s="T128">потом</ta>
            <ta e="T130" id="Seg_6754" s="T129">1SG.[NOM]</ta>
            <ta e="T131" id="Seg_6755" s="T130">как</ta>
            <ta e="T132" id="Seg_6756" s="T131">делать-CVB.SEQ-1SG</ta>
            <ta e="T133" id="Seg_6757" s="T132">обман-VBZ-CVB.SEQ-1SG</ta>
            <ta e="T134" id="Seg_6758" s="T133">есть-FUT-1SG=Q</ta>
            <ta e="T135" id="Seg_6759" s="T134">тот</ta>
            <ta e="T136" id="Seg_6760" s="T135">мохнатый.канюк-ABL</ta>
            <ta e="T137" id="Seg_6761" s="T136">что-ACC</ta>
            <ta e="T138" id="Seg_6762" s="T137">яйцо-PART</ta>
            <ta e="T139" id="Seg_6763" s="T138">думать-CVB.SEQ</ta>
            <ta e="T140" id="Seg_6764" s="T139">этот</ta>
            <ta e="T141" id="Seg_6765" s="T140">MOD</ta>
            <ta e="T142" id="Seg_6766" s="T141">мохнатый.канюк-DAT/LOC</ta>
            <ta e="T145" id="Seg_6767" s="T144">мохнатый.канюк-ABL</ta>
            <ta e="T146" id="Seg_6768" s="T145">яйцо.[NOM]</ta>
            <ta e="T147" id="Seg_6769" s="T146">есть-CVB.PURP</ta>
            <ta e="T148" id="Seg_6770" s="T147">бежать-CVB.SEQ</ta>
            <ta e="T149" id="Seg_6771" s="T148">приходить-CVB.SEQ</ta>
            <ta e="T150" id="Seg_6772" s="T149">самый</ta>
            <ta e="T151" id="Seg_6773" s="T150">кто-3SG-GEN</ta>
            <ta e="T152" id="Seg_6774" s="T151">нижняя.часть-3SG-DAT/LOC</ta>
            <ta e="T153" id="Seg_6775" s="T152">приходить-PST2.[3SG]</ta>
            <ta e="T154" id="Seg_6776" s="T153">лиственница-3SG-GEN</ta>
            <ta e="T155" id="Seg_6777" s="T154">нижняя.часть-3SG-DAT/LOC</ta>
            <ta e="T156" id="Seg_6778" s="T155">приходить-PST2.[3SG]</ta>
            <ta e="T157" id="Seg_6779" s="T156">так</ta>
            <ta e="T158" id="Seg_6780" s="T157">делать-CVB.SEQ</ta>
            <ta e="T159" id="Seg_6781" s="T158">после</ta>
            <ta e="T160" id="Seg_6782" s="T159">хвост-3SG-INSTR</ta>
            <ta e="T161" id="Seg_6783" s="T160">лиственница-3SG-ACC</ta>
            <ta e="T162" id="Seg_6784" s="T161">бить-CVB.SIM</ta>
            <ta e="T163" id="Seg_6785" s="T162">стоять-PST2-3SG</ta>
            <ta e="T164" id="Seg_6786" s="T163">мохнатый.канюк.[NOM]</ta>
            <ta e="T165" id="Seg_6787" s="T164">мохнатый.канюк.[NOM]</ta>
            <ta e="T166" id="Seg_6788" s="T165">шустрый</ta>
            <ta e="T167" id="Seg_6789" s="T166">MOD</ta>
            <ta e="T168" id="Seg_6790" s="T167">приносить.[IMP.2SG]</ta>
            <ta e="T169" id="Seg_6791" s="T168">один</ta>
            <ta e="T170" id="Seg_6792" s="T169">ребенок-2SG-ACC</ta>
            <ta e="T171" id="Seg_6793" s="T170">1SG.[NOM]</ta>
            <ta e="T172" id="Seg_6794" s="T171">хвост-EP-1SG.[NOM]</ta>
            <ta e="T173" id="Seg_6795" s="T172">русский</ta>
            <ta e="T174" id="Seg_6796" s="T173">меч-3SG-COMP</ta>
            <ta e="T175" id="Seg_6797" s="T174">острый.[NOM]</ta>
            <ta e="T176" id="Seg_6798" s="T175">лиственница-COM</ta>
            <ta e="T177" id="Seg_6799" s="T176">EMPH-COM</ta>
            <ta e="T178" id="Seg_6800" s="T177">резать-CVB.SIM</ta>
            <ta e="T179" id="Seg_6801" s="T178">бить-CVB.SEQ</ta>
            <ta e="T180" id="Seg_6802" s="T179">падать-EP-CAUS-FUT-1SG</ta>
            <ta e="T181" id="Seg_6803" s="T180">сам-COM-2SG-ACC</ta>
            <ta e="T182" id="Seg_6804" s="T181">есть-FUT-1SG</ta>
            <ta e="T183" id="Seg_6805" s="T182">мохнатый.канюк.[NOM]</ta>
            <ta e="T184" id="Seg_6806" s="T183">кончаться-FUT.[3SG]</ta>
            <ta e="T185" id="Seg_6807" s="T184">Q</ta>
            <ta e="T186" id="Seg_6808" s="T185">плакать-CVB.SIM-плакать-CVB.SIM</ta>
            <ta e="T187" id="Seg_6809" s="T186">один</ta>
            <ta e="T188" id="Seg_6810" s="T187">ребенок-3SG-ACC</ta>
            <ta e="T189" id="Seg_6811" s="T188">кататься-NMNZ.[NOM]</ta>
            <ta e="T190" id="Seg_6812" s="T189">делать-CAUS-PST2.[3SG]</ta>
            <ta e="T191" id="Seg_6813" s="T190">один</ta>
            <ta e="T192" id="Seg_6814" s="T191">яйцо-3SG-ACC</ta>
            <ta e="T193" id="Seg_6815" s="T192">есть-PST2.[3SG]</ta>
            <ta e="T194" id="Seg_6816" s="T193">так</ta>
            <ta e="T195" id="Seg_6817" s="T194">делать-CVB.SEQ</ta>
            <ta e="T196" id="Seg_6818" s="T195">после</ta>
            <ta e="T197" id="Seg_6819" s="T196">земля.[NOM]</ta>
            <ta e="T198" id="Seg_6820" s="T197">небо.[NOM]</ta>
            <ta e="T199" id="Seg_6821" s="T198">бежать-CVB.SEQ</ta>
            <ta e="T200" id="Seg_6822" s="T199">оставаться-PST2.[3SG]</ta>
            <ta e="T201" id="Seg_6823" s="T200">следующее.утро-3SG-ACC</ta>
            <ta e="T202" id="Seg_6824" s="T201">вставать-PRS.[3SG]</ta>
            <ta e="T203" id="Seg_6825" s="T202">опять</ta>
            <ta e="T204" id="Seg_6826" s="T203">мохнатый.канюк-ACC</ta>
            <ta e="T205" id="Seg_6827" s="T204">обман-VBZ</ta>
            <ta e="T206" id="Seg_6828" s="T205">мохнатый.канюк-3SG-ACC</ta>
            <ta e="T207" id="Seg_6829" s="T206">обман-VBZ-CVB.PURP</ta>
            <ta e="T208" id="Seg_6830" s="T207">опять</ta>
            <ta e="T209" id="Seg_6831" s="T208">бежать-CVB.SEQ</ta>
            <ta e="T210" id="Seg_6832" s="T209">приходить-PRS.[3SG]</ta>
            <ta e="T211" id="Seg_6833" s="T210">мохнатый.канюк.[NOM]</ta>
            <ta e="T212" id="Seg_6834" s="T211">мохнатый.канюк.[NOM]</ta>
            <ta e="T213" id="Seg_6835" s="T212">шустрый</ta>
            <ta e="T214" id="Seg_6836" s="T213">MOD</ta>
            <ta e="T215" id="Seg_6837" s="T214">приносить.[IMP.2SG]</ta>
            <ta e="T216" id="Seg_6838" s="T215">один</ta>
            <ta e="T217" id="Seg_6839" s="T216">ребенок-2SG-ACC</ta>
            <ta e="T218" id="Seg_6840" s="T217">1SG.[NOM]</ta>
            <ta e="T219" id="Seg_6841" s="T218">хвост-EP-1SG.[NOM]</ta>
            <ta e="T220" id="Seg_6842" s="T219">русский</ta>
            <ta e="T221" id="Seg_6843" s="T220">меч-3SG-COMP</ta>
            <ta e="T222" id="Seg_6844" s="T221">острый.[NOM]</ta>
            <ta e="T223" id="Seg_6845" s="T222">лиственница-COM-2SG-ACC-EMPH-COM-2SG-ACC</ta>
            <ta e="T224" id="Seg_6846" s="T223">резать-CVB.SIM</ta>
            <ta e="T225" id="Seg_6847" s="T224">бить-CVB.SEQ</ta>
            <ta e="T226" id="Seg_6848" s="T225">падать-EP-CAUS-CVB.SEQ-1SG</ta>
            <ta e="T227" id="Seg_6849" s="T226">теперь</ta>
            <ta e="T228" id="Seg_6850" s="T227">сам-COM-2SG-ACC</ta>
            <ta e="T229" id="Seg_6851" s="T228">есть-FUT-1SG</ta>
            <ta e="T230" id="Seg_6852" s="T229">опять</ta>
            <ta e="T231" id="Seg_6853" s="T230">один</ta>
            <ta e="T232" id="Seg_6854" s="T231">ребенок-3SG-ACC</ta>
            <ta e="T233" id="Seg_6855" s="T232">толкать-CVB.SEQ</ta>
            <ta e="T234" id="Seg_6856" s="T233">бросать-PST2.[3SG]</ta>
            <ta e="T235" id="Seg_6857" s="T234">два</ta>
            <ta e="T236" id="Seg_6858" s="T235">ребенок-3SG-ACC</ta>
            <ta e="T237" id="Seg_6859" s="T236">есть-PST2.[3SG]</ta>
            <ta e="T238" id="Seg_6860" s="T237">лиса.[NOM]</ta>
            <ta e="T239" id="Seg_6861" s="T238">мохнатый.канюк.[NOM]</ta>
            <ta e="T240" id="Seg_6862" s="T239">плакать-CVB.SIM</ta>
            <ta e="T241" id="Seg_6863" s="T240">сидеть-EMOT-PST2.[3SG]</ta>
            <ta e="T242" id="Seg_6864" s="T241">EMPH</ta>
            <ta e="T243" id="Seg_6865" s="T242">ну</ta>
            <ta e="T244" id="Seg_6866" s="T243">этот</ta>
            <ta e="T245" id="Seg_6867" s="T244">теперь</ta>
            <ta e="T246" id="Seg_6868" s="T245">однако</ta>
            <ta e="T247" id="Seg_6869" s="T246">сам-COM-1SG-ACC</ta>
            <ta e="T248" id="Seg_6870" s="T247">есть-CVB.SEQ-ACC</ta>
            <ta e="T249" id="Seg_6871" s="T248">говорить-CVB.SEQ</ta>
            <ta e="T250" id="Seg_6872" s="T249">плакать-CVB.SIM</ta>
            <ta e="T251" id="Seg_6873" s="T250">сидеть-PST2.[3SG]</ta>
            <ta e="T252" id="Seg_6874" s="T251">эй</ta>
            <ta e="T253" id="Seg_6875" s="T252">этот</ta>
            <ta e="T254" id="Seg_6876" s="T253">сидеть-TEMP-3SG</ta>
            <ta e="T255" id="Seg_6877" s="T254">кукша.[NOM]</ta>
            <ta e="T256" id="Seg_6878" s="T255">летать-CVB.SEQ</ta>
            <ta e="T257" id="Seg_6879" s="T256">приходить-PST2.[3SG]</ta>
            <ta e="T258" id="Seg_6880" s="T257">ой</ta>
            <ta e="T259" id="Seg_6881" s="T258">мохнатый.канюк.[NOM]</ta>
            <ta e="T260" id="Seg_6882" s="T259">что-ABL</ta>
            <ta e="T261" id="Seg_6883" s="T260">плакать-PRS-2SG</ta>
            <ta e="T262" id="Seg_6884" s="T261">EMPH</ta>
            <ta e="T263" id="Seg_6885" s="T262">2SG.[NOM]</ta>
            <ta e="T264" id="Seg_6886" s="T263">этот</ta>
            <ta e="T265" id="Seg_6887" s="T264">такой</ta>
            <ta e="T266" id="Seg_6888" s="T265">красивый</ta>
            <ta e="T267" id="Seg_6889" s="T266">солнце-PROPR</ta>
            <ta e="T268" id="Seg_6890" s="T267">день.[NOM]</ta>
            <ta e="T269" id="Seg_6891" s="T268">рассвет-3SG.[NOM]</ta>
            <ta e="T270" id="Seg_6892" s="T269">вставать-TEMP-3SG</ta>
            <ta e="T271" id="Seg_6893" s="T270">ребенок-PL-EP-2SG.[NOM]</ta>
            <ta e="T272" id="Seg_6894" s="T271">да</ta>
            <ta e="T273" id="Seg_6895" s="T272">теплый-DAT/LOC</ta>
            <ta e="T274" id="Seg_6896" s="T273">лежать-PRS-3PL</ta>
            <ta e="T275" id="Seg_6897" s="T274">сам-2SG.[NOM]</ta>
            <ta e="T276" id="Seg_6898" s="T275">да</ta>
            <ta e="T277" id="Seg_6899" s="T276">пища-ACC</ta>
            <ta e="T278" id="Seg_6900" s="T277">EMPH</ta>
            <ta e="T279" id="Seg_6901" s="T278">приносить-CVB.SEQ</ta>
            <ta e="T280" id="Seg_6902" s="T279">приходить-PTCP.PRS.[NOM]</ta>
            <ta e="T281" id="Seg_6903" s="T280">подобно</ta>
            <ta e="T282" id="Seg_6904" s="T281">EMPH</ta>
            <ta e="T283" id="Seg_6905" s="T282">что-ABL</ta>
            <ta e="T284" id="Seg_6906" s="T283">плакать-PRS-2SG</ta>
            <ta e="T285" id="Seg_6907" s="T284">вот</ta>
            <ta e="T286" id="Seg_6908" s="T285">такой</ta>
            <ta e="T287" id="Seg_6909" s="T286">веселый-DAT/LOC</ta>
            <ta e="T288" id="Seg_6910" s="T287">жить-CVB.SEQ-2SG</ta>
            <ta e="T289" id="Seg_6911" s="T288">говорить-PRS.[3SG]</ta>
            <ta e="T290" id="Seg_6912" s="T289">2SG.[NOM]</ta>
            <ta e="T291" id="Seg_6913" s="T290">как</ta>
            <ta e="T292" id="Seg_6914" s="T291">плакать-EP-CAUS-NEG-2SG</ta>
            <ta e="T293" id="Seg_6915" s="T292">видеть-NEG-2SG</ta>
            <ta e="T294" id="Seg_6916" s="T293">Q</ta>
            <ta e="T295" id="Seg_6917" s="T294">1SG-ACC</ta>
            <ta e="T296" id="Seg_6918" s="T295">однако</ta>
            <ta e="T297" id="Seg_6919" s="T296">лиса.[NOM]</ta>
            <ta e="T298" id="Seg_6920" s="T297">MOD</ta>
            <ta e="T299" id="Seg_6921" s="T298">два</ta>
            <ta e="T300" id="Seg_6922" s="T299">день-DAT/LOC</ta>
            <ta e="T301" id="Seg_6923" s="T300">два</ta>
            <ta e="T302" id="Seg_6924" s="T301">ребенок-1SG-ACC</ta>
            <ta e="T303" id="Seg_6925" s="T302">есть-PST1-3SG</ta>
            <ta e="T304" id="Seg_6926" s="T303">теперь</ta>
            <ta e="T305" id="Seg_6927" s="T304">сегодня</ta>
            <ta e="T306" id="Seg_6928" s="T305">опять</ta>
            <ta e="T307" id="Seg_6929" s="T306">приходить-FUT-3SG</ta>
            <ta e="T308" id="Seg_6930" s="T307">есть-FUT-3SG</ta>
            <ta e="T309" id="Seg_6931" s="T308">лиственница-COM-1SG-ACC</ta>
            <ta e="T310" id="Seg_6932" s="T309">EMPH-COM-1SG-ACC</ta>
            <ta e="T311" id="Seg_6933" s="T310">резать-CVB.SIM</ta>
            <ta e="T312" id="Seg_6934" s="T311">бить-CVB.SEQ</ta>
            <ta e="T313" id="Seg_6935" s="T312">падать-EP-CAUS-CVB.SEQ</ta>
            <ta e="T314" id="Seg_6936" s="T313">сам-COM-1SG-ACC</ta>
            <ta e="T315" id="Seg_6937" s="T314">есть-CVB.PURP</ta>
            <ta e="T316" id="Seg_6938" s="T315">хотеть-PST1-3SG</ta>
            <ta e="T317" id="Seg_6939" s="T316">говорить-PRS.[3SG]</ta>
            <ta e="T318" id="Seg_6940" s="T317">что-PROPR.[NOM]=Q</ta>
            <ta e="T319" id="Seg_6941" s="T318">тот</ta>
            <ta e="T320" id="Seg_6942" s="T319">лиса-EP-2SG.[NOM]</ta>
            <ta e="T321" id="Seg_6943" s="T320">эй</ta>
            <ta e="T322" id="Seg_6944" s="T321">хвост-3SG.[NOM]</ta>
            <ta e="T323" id="Seg_6945" s="T322">русский</ta>
            <ta e="T324" id="Seg_6946" s="T323">меч-3SG-COMP</ta>
            <ta e="T325" id="Seg_6947" s="T324">острый.[NOM]</ta>
            <ta e="T326" id="Seg_6948" s="T325">ну</ta>
            <ta e="T327" id="Seg_6949" s="T326">что.[NOM]</ta>
            <ta e="T328" id="Seg_6950" s="T327">имя-PROPR-3SG.[NOM]</ta>
            <ta e="T329" id="Seg_6951" s="T328">глупый</ta>
            <ta e="T330" id="Seg_6952" s="T329">птица-3SG.[NOM]</ta>
            <ta e="T331" id="Seg_6953" s="T330">родиться-PST2-EP-2SG=Q</ta>
            <ta e="T332" id="Seg_6954" s="T331">2SG.[NOM]</ta>
            <ta e="T333" id="Seg_6955" s="T332">такой.[NOM]</ta>
            <ta e="T334" id="Seg_6956" s="T333">вот</ta>
            <ta e="T335" id="Seg_6957" s="T334">мохнатый.канюк.[NOM]</ta>
            <ta e="T336" id="Seg_6958" s="T335">как</ta>
            <ta e="T337" id="Seg_6959" s="T336">делать-CVB.SIM</ta>
            <ta e="T338" id="Seg_6960" s="T337">лиса.[NOM]</ta>
            <ta e="T339" id="Seg_6961" s="T338">как</ta>
            <ta e="T340" id="Seg_6962" s="T339">делать-CVB.SEQ</ta>
            <ta e="T341" id="Seg_6963" s="T340">2SG.[NOM]</ta>
            <ta e="T342" id="Seg_6964" s="T341">лиственница-2SG-ACC</ta>
            <ta e="T343" id="Seg_6965" s="T342">резать-CVB.SIM</ta>
            <ta e="T344" id="Seg_6966" s="T343">бить-FUT.[3SG]=Q</ta>
            <ta e="T345" id="Seg_6967" s="T344">хвост-3SG.[NOM]</ta>
            <ta e="T346" id="Seg_6968" s="T345">шерсть.[NOM]</ta>
            <ta e="T347" id="Seg_6969" s="T346">шерсть.[NOM]</ta>
            <ta e="T348" id="Seg_6970" s="T347">хвост.[NOM]</ta>
            <ta e="T349" id="Seg_6971" s="T348">что-ACC</ta>
            <ta e="T350" id="Seg_6972" s="T349">мочь-FUT-2SG=Q</ta>
            <ta e="T351" id="Seg_6973" s="T350">говорить.[IMP.2SG]</ta>
            <ta e="T352" id="Seg_6974" s="T351">теперь</ta>
            <ta e="T353" id="Seg_6975" s="T352">приходить-TEMP-3SG</ta>
            <ta e="T354" id="Seg_6976" s="T353">кто.[NOM]</ta>
            <ta e="T355" id="Seg_6977" s="T354">учить-PST2-3SG=Q</ta>
            <ta e="T356" id="Seg_6978" s="T355">говорить-TEMP-3SG</ta>
            <ta e="T357" id="Seg_6979" s="T356">кукша.[NOM]</ta>
            <ta e="T358" id="Seg_6980" s="T357">учить-PST2-3SG</ta>
            <ta e="T359" id="Seg_6981" s="T358">говорить-FUT.[IMP.2SG]</ta>
            <ta e="T360" id="Seg_6982" s="T359">вот</ta>
            <ta e="T361" id="Seg_6983" s="T360">мохнатый.канюк.[NOM]</ta>
            <ta e="T364" id="Seg_6984" s="T363">мохнатый.канюк.[NOM]</ta>
            <ta e="T365" id="Seg_6985" s="T364">вот</ta>
            <ta e="T366" id="Seg_6986" s="T365">лучше</ta>
            <ta e="T367" id="Seg_6987" s="T366">немного</ta>
            <ta e="T368" id="Seg_6988" s="T367">становиться-PST2.[3SG]</ta>
            <ta e="T369" id="Seg_6989" s="T368">сидеть-PST2.[3SG]</ta>
            <ta e="T370" id="Seg_6990" s="T369">кукша-3SG.[NOM]</ta>
            <ta e="T371" id="Seg_6991" s="T370">летать-CVB.SEQ</ta>
            <ta e="T372" id="Seg_6992" s="T371">оставаться-PST2.[3SG]</ta>
            <ta e="T373" id="Seg_6993" s="T372">о</ta>
            <ta e="T374" id="Seg_6994" s="T373">вот</ta>
            <ta e="T375" id="Seg_6995" s="T374">лиса.[NOM]</ta>
            <ta e="T376" id="Seg_6996" s="T375">рассердиться-CVB.PURP</ta>
            <ta e="T377" id="Seg_6997" s="T376">рассердиться-PTCP.PST</ta>
            <ta e="T378" id="Seg_6998" s="T377">становиться-PST2.[3SG]</ta>
            <ta e="T379" id="Seg_6999" s="T378">сердиться-CVB.PURP</ta>
            <ta e="T380" id="Seg_7000" s="T379">сердиться-PTCP.PST</ta>
            <ta e="T381" id="Seg_7001" s="T380">становиться-PST2.[3SG]</ta>
            <ta e="T382" id="Seg_7002" s="T381">разный-разный</ta>
            <ta e="T383" id="Seg_7003" s="T382">мох-ACC</ta>
            <ta e="T384" id="Seg_7004" s="T383">стоять-EP-CAUS-CVB.SIM</ta>
            <ta e="T385" id="Seg_7005" s="T384">бить-CVB.SIM-бить-CVB.SIM</ta>
            <ta e="T386" id="Seg_7006" s="T385">передний</ta>
            <ta e="T387" id="Seg_7007" s="T386">задний</ta>
            <ta e="T388" id="Seg_7008" s="T387">нога-3SG-INSTR</ta>
            <ta e="T389" id="Seg_7009" s="T388">разорвать-CVB.SIM</ta>
            <ta e="T390" id="Seg_7010" s="T389">бить-CVB.SIM-бить-CVB.SIM</ta>
            <ta e="T391" id="Seg_7011" s="T390">приходить-EMOT-PST2.[3SG]</ta>
            <ta e="T392" id="Seg_7012" s="T391">EMPH</ta>
            <ta e="T393" id="Seg_7013" s="T392">мохнатый.канюк-3SG-DAT/LOC</ta>
            <ta e="T394" id="Seg_7014" s="T393">мохнатый.канюк-ACC</ta>
            <ta e="T395" id="Seg_7015" s="T394">путать-CVB.PURP</ta>
            <ta e="T396" id="Seg_7016" s="T395">мохнатый.канюк.[NOM]</ta>
            <ta e="T397" id="Seg_7017" s="T396">мохнатый.канюк.[NOM]</ta>
            <ta e="T398" id="Seg_7018" s="T397">такой-такой</ta>
            <ta e="T399" id="Seg_7019" s="T398">мать-PROPR.[NOM]</ta>
            <ta e="T400" id="Seg_7020" s="T399">эй</ta>
            <ta e="T401" id="Seg_7021" s="T400">1SG.[NOM]</ta>
            <ta e="T402" id="Seg_7022" s="T401">однако</ta>
            <ta e="T403" id="Seg_7023" s="T402">опять</ta>
            <ta e="T404" id="Seg_7024" s="T403">вовсе</ta>
            <ta e="T405" id="Seg_7025" s="T404">рассердиться-CVB.PURP</ta>
            <ta e="T406" id="Seg_7026" s="T405">рассердиться-PST1-1SG</ta>
            <ta e="T407" id="Seg_7027" s="T406">приносить.[IMP.2SG]</ta>
            <ta e="T408" id="Seg_7028" s="T407">один</ta>
            <ta e="T409" id="Seg_7029" s="T408">ребенок-PART</ta>
            <ta e="T410" id="Seg_7030" s="T409">1SG.[NOM]</ta>
            <ta e="T411" id="Seg_7031" s="T410">хвост-EP-1SG.[NOM]</ta>
            <ta e="T412" id="Seg_7032" s="T411">русский</ta>
            <ta e="T413" id="Seg_7033" s="T412">меч-3SG-COMP</ta>
            <ta e="T414" id="Seg_7034" s="T413">острый.[NOM]</ta>
            <ta e="T415" id="Seg_7035" s="T414">недавно</ta>
            <ta e="T416" id="Seg_7036" s="T415">сам-COM-2SG-ACC</ta>
            <ta e="T419" id="Seg_7037" s="T418">совсем</ta>
            <ta e="T420" id="Seg_7038" s="T419">бить-CVB.SEQ-1SG</ta>
            <ta e="T421" id="Seg_7039" s="T420">сам-COM-2SG-ACC</ta>
            <ta e="T422" id="Seg_7040" s="T421">есть-FUT-1SG</ta>
            <ta e="T423" id="Seg_7041" s="T422">даже</ta>
            <ta e="T424" id="Seg_7042" s="T423">EMPH</ta>
            <ta e="T425" id="Seg_7043" s="T424">глупый-VBZ-CVB.SEQ-1SG</ta>
            <ta e="T426" id="Seg_7044" s="T425">вовсе</ta>
            <ta e="T430" id="Seg_7045" s="T429">дорогой</ta>
            <ta e="T431" id="Seg_7046" s="T430">два</ta>
            <ta e="T432" id="Seg_7047" s="T431">ребенок-1SG-ACC</ta>
            <ta e="T433" id="Seg_7048" s="T432">есть-CAUS-PST1-1SG</ta>
            <ta e="T434" id="Seg_7049" s="T433">1SG.[NOM]</ta>
            <ta e="T435" id="Seg_7050" s="T434">2SG-DAT/LOC</ta>
            <ta e="T436" id="Seg_7051" s="T435">сам-1SG.[NOM]</ta>
            <ta e="T437" id="Seg_7052" s="T436">глупый-1SG-DAT/LOC</ta>
            <ta e="T438" id="Seg_7053" s="T437">2SG.[NOM]</ta>
            <ta e="T439" id="Seg_7054" s="T438">однако</ta>
            <ta e="T440" id="Seg_7055" s="T439">шерсть.[NOM]</ta>
            <ta e="T441" id="Seg_7056" s="T440">хвост-2SG</ta>
            <ta e="T442" id="Seg_7057" s="T441">EMPH</ta>
            <ta e="T443" id="Seg_7058" s="T442">шерсть.[NOM]</ta>
            <ta e="T444" id="Seg_7059" s="T443">хвост.[NOM]</ta>
            <ta e="T445" id="Seg_7060" s="T444">что-ACC</ta>
            <ta e="T446" id="Seg_7061" s="T445">мочь-FUT-2SG=Q</ta>
            <ta e="T447" id="Seg_7062" s="T446">дерево-ACC</ta>
            <ta e="T448" id="Seg_7063" s="T447">мочь-FUT-2SG</ta>
            <ta e="T449" id="Seg_7064" s="T448">Q</ta>
            <ta e="T450" id="Seg_7065" s="T449">говорить-PRS.[3SG]</ta>
            <ta e="T451" id="Seg_7066" s="T450">EMPH</ta>
            <ta e="T452" id="Seg_7067" s="T451">боже.мой</ta>
            <ta e="T453" id="Seg_7068" s="T452">что.[NOM]</ta>
            <ta e="T454" id="Seg_7069" s="T453">имя-PROPR-3SG.[NOM]</ta>
            <ta e="T455" id="Seg_7070" s="T454">ум.[NOM]</ta>
            <ta e="T456" id="Seg_7071" s="T455">давать-PST2-3SG=Q</ta>
            <ta e="T457" id="Seg_7072" s="T456">2SG-DAT/LOC</ta>
            <ta e="T458" id="Seg_7073" s="T457">как</ta>
            <ta e="T459" id="Seg_7074" s="T458">делать-CVB.SEQ-2SG</ta>
            <ta e="T460" id="Seg_7075" s="T459">ум-ACC</ta>
            <ta e="T461" id="Seg_7076" s="T460">найти-PST2-EP-2SG=Q</ta>
            <ta e="T462" id="Seg_7077" s="T461">говорить-PRS.[3SG]</ta>
            <ta e="T463" id="Seg_7078" s="T462">мой</ta>
            <ta e="T464" id="Seg_7079" s="T463">хвост-EP-1SG.[NOM]</ta>
            <ta e="T465" id="Seg_7080" s="T464">шерсть-3SG-ACC</ta>
            <ta e="T466" id="Seg_7081" s="T465">откуда</ta>
            <ta e="T467" id="Seg_7082" s="T466">знать-PRS-2SG</ta>
            <ta e="T468" id="Seg_7083" s="T467">эй</ta>
            <ta e="T469" id="Seg_7084" s="T468">1SG-ACC</ta>
            <ta e="T470" id="Seg_7085" s="T469">кукша.[NOM]</ta>
            <ta e="T471" id="Seg_7086" s="T470">учить-PST2-3SG</ta>
            <ta e="T472" id="Seg_7087" s="T471">кто.[NOM]</ta>
            <ta e="T473" id="Seg_7088" s="T472">давать-FUT.[3SG]=Q</ta>
            <ta e="T474" id="Seg_7089" s="T473">1SG-DAT/LOC</ta>
            <ta e="T475" id="Seg_7090" s="T474">ум.[NOM]</ta>
            <ta e="T476" id="Seg_7091" s="T475">кукша.[NOM]</ta>
            <ta e="T477" id="Seg_7092" s="T476">учить-PST2-3SG</ta>
            <ta e="T478" id="Seg_7093" s="T477">ой</ta>
            <ta e="T479" id="Seg_7094" s="T478">такой-такой</ta>
            <ta e="T480" id="Seg_7095" s="T479">EMPH</ta>
            <ta e="T481" id="Seg_7096" s="T480">кукша-ACC</ta>
            <ta e="T482" id="Seg_7097" s="T481">какой</ta>
            <ta e="T483" id="Seg_7098" s="T482">место-ABL</ta>
            <ta e="T484" id="Seg_7099" s="T483">найти-FUT-1SG=Q</ta>
            <ta e="T485" id="Seg_7100" s="T484">вот</ta>
            <ta e="T486" id="Seg_7101" s="T485">подожди</ta>
            <ta e="T487" id="Seg_7102" s="T486">вот</ta>
            <ta e="T488" id="Seg_7103" s="T487">встречаться-FUT-1SG</ta>
            <ta e="T489" id="Seg_7104" s="T488">1SG.[NOM]</ta>
            <ta e="T490" id="Seg_7105" s="T489">3SG-DAT/LOC</ta>
            <ta e="T491" id="Seg_7106" s="T490">EMPH</ta>
            <ta e="T492" id="Seg_7107" s="T491">так</ta>
            <ta e="T493" id="Seg_7108" s="T492">делать-PST2.[3SG]</ta>
            <ta e="T494" id="Seg_7109" s="T493">да</ta>
            <ta e="T495" id="Seg_7110" s="T494">земля.[NOM]</ta>
            <ta e="T496" id="Seg_7111" s="T495">небо.[NOM]</ta>
            <ta e="T497" id="Seg_7112" s="T496">бежать-CVB.SEQ</ta>
            <ta e="T498" id="Seg_7113" s="T497">оставаться-PST2.[3SG]</ta>
            <ta e="T499" id="Seg_7114" s="T498">земля.[NOM]</ta>
            <ta e="T500" id="Seg_7115" s="T499">небо.[NOM]</ta>
            <ta e="T501" id="Seg_7116" s="T500">бежать-CVB.SEQ</ta>
            <ta e="T502" id="Seg_7117" s="T501">идти-CVB.SEQ</ta>
            <ta e="T503" id="Seg_7118" s="T502">быть-PST1-3SG</ta>
            <ta e="T504" id="Seg_7119" s="T503">EMPH-круглый</ta>
            <ta e="T505" id="Seg_7120" s="T504">озеро-DAT/LOC</ta>
            <ta e="T507" id="Seg_7121" s="T505">доезжать-CVB.SEQ</ta>
            <ta e="T508" id="Seg_7122" s="T507">этот</ta>
            <ta e="T509" id="Seg_7123" s="T508">озеро.[NOM]</ta>
            <ta e="T510" id="Seg_7124" s="T509">что-ABL</ta>
            <ta e="T511" id="Seg_7125" s="T510">NEG</ta>
            <ta e="T512" id="Seg_7126" s="T511">большой</ta>
            <ta e="T513" id="Seg_7127" s="T512">гора-PROPR.[NOM]</ta>
            <ta e="T514" id="Seg_7128" s="T513">весна.[NOM]</ta>
            <ta e="T515" id="Seg_7129" s="T514">час-3SG-DAT/LOC</ta>
            <ta e="T516" id="Seg_7130" s="T515">быть-PST1-3SG</ta>
            <ta e="T517" id="Seg_7131" s="T516">холм-3SG.[NOM]</ta>
            <ta e="T518" id="Seg_7132" s="T517">полностью</ta>
            <ta e="T519" id="Seg_7133" s="T518">выйти-EP-PST2.[3SG]</ta>
            <ta e="T520" id="Seg_7134" s="T519">этот</ta>
            <ta e="T521" id="Seg_7135" s="T520">холм-ACC</ta>
            <ta e="T522" id="Seg_7136" s="T521">однако</ta>
            <ta e="T523" id="Seg_7137" s="T522">бежать-CVB.SIM</ta>
            <ta e="T524" id="Seg_7138" s="T523">идти-CVB.SEQ</ta>
            <ta e="T525" id="Seg_7139" s="T524">вовсе</ta>
            <ta e="T526" id="Seg_7140" s="T525">самый</ta>
            <ta e="T527" id="Seg_7141" s="T526">красивый</ta>
            <ta e="T528" id="Seg_7142" s="T527">проталина-DAT/LOC</ta>
            <ta e="T529" id="Seg_7143" s="T528">однако</ta>
            <ta e="T530" id="Seg_7144" s="T529">умирать-PTCP.PST</ta>
            <ta e="T531" id="Seg_7145" s="T530">лиса.[NOM]</ta>
            <ta e="T532" id="Seg_7146" s="T531">становиться-CVB.SEQ</ta>
            <ta e="T533" id="Seg_7147" s="T532">после</ta>
            <ta e="T534" id="Seg_7148" s="T533">распространяться-NMNZ.[NOM]</ta>
            <ta e="T535" id="Seg_7149" s="T534">делать-CVB.SIM</ta>
            <ta e="T536" id="Seg_7150" s="T535">падать-PST2.[3SG]</ta>
            <ta e="T537" id="Seg_7151" s="T536">вовсе</ta>
            <ta e="T538" id="Seg_7152" s="T537">насекомые-DAT/LOC</ta>
            <ta e="T539" id="Seg_7153" s="T538">каждый-3SG-DAT/LOC</ta>
            <ta e="T540" id="Seg_7154" s="T539">разбрасывать-PASS-CVB.SIM</ta>
            <ta e="T541" id="Seg_7155" s="T540">лежать-PST2.[3SG]</ta>
            <ta e="T542" id="Seg_7156" s="T541">тот</ta>
            <ta e="T543" id="Seg_7157" s="T542">делать-CVB.SEQ</ta>
            <ta e="T544" id="Seg_7158" s="T543">после</ta>
            <ta e="T545" id="Seg_7159" s="T544">ой</ta>
            <ta e="T546" id="Seg_7160" s="T545">сойка-PL.[NOM]</ta>
            <ta e="T547" id="Seg_7161" s="T546">EMPH</ta>
            <ta e="T548" id="Seg_7162" s="T547">летать-CVB.SIM</ta>
            <ta e="T549" id="Seg_7163" s="T548">идти-CVB.SEQ</ta>
            <ta e="T550" id="Seg_7164" s="T549">быть-CVB.SEQ</ta>
            <ta e="T551" id="Seg_7165" s="T550">видеть-PRS.[3SG]</ta>
            <ta e="T552" id="Seg_7166" s="T551">лиса.[NOM]</ta>
            <ta e="T553" id="Seg_7167" s="T552">умирать-CVB.SEQ</ta>
            <ta e="T554" id="Seg_7168" s="T553">после</ta>
            <ta e="T555" id="Seg_7169" s="T554">распространяться-CVB.SEQ</ta>
            <ta e="T556" id="Seg_7170" s="T555">лежать-PRS.[3SG]</ta>
            <ta e="T557" id="Seg_7171" s="T556">о</ta>
            <ta e="T558" id="Seg_7172" s="T557">этот.[NOM]</ta>
            <ta e="T559" id="Seg_7173" s="T558">так.долго</ta>
            <ta e="T560" id="Seg_7174" s="T559">этот.[NOM]</ta>
            <ta e="T561" id="Seg_7175" s="T560">голодать-CVB.SEQ</ta>
            <ta e="T562" id="Seg_7176" s="T561">однако</ta>
            <ta e="T563" id="Seg_7177" s="T562">мохнатый.канюк.[NOM]</ta>
            <ta e="T564" id="Seg_7178" s="T563">ребенок-PL-3SG-ACC</ta>
            <ta e="T565" id="Seg_7179" s="T564">есть-CVB.SIM</ta>
            <ta e="T566" id="Seg_7180" s="T565">идти-EP-PST2-3SG</ta>
            <ta e="T567" id="Seg_7181" s="T566">MOD</ta>
            <ta e="T568" id="Seg_7182" s="T567">теперь</ta>
            <ta e="T569" id="Seg_7183" s="T568">мохнатый.канюк-3SG.[NOM]</ta>
            <ta e="T570" id="Seg_7184" s="T569">однако</ta>
            <ta e="T571" id="Seg_7185" s="T570">1SG-ABL</ta>
            <ta e="T572" id="Seg_7186" s="T571">ум.[NOM]</ta>
            <ta e="T573" id="Seg_7187" s="T572">взять-PRS.[3SG]</ta>
            <ta e="T574" id="Seg_7188" s="T573">один</ta>
            <ta e="T575" id="Seg_7189" s="T574">NEG</ta>
            <ta e="T576" id="Seg_7190" s="T575">ребенок-3SG-ACC</ta>
            <ta e="T577" id="Seg_7191" s="T576">давать-PST2.NEG.[3SG]</ta>
            <ta e="T578" id="Seg_7192" s="T577">тогда</ta>
            <ta e="T579" id="Seg_7193" s="T578">откуда</ta>
            <ta e="T580" id="Seg_7194" s="T579">взять-CVB.SEQ</ta>
            <ta e="T581" id="Seg_7195" s="T580">3SG.[NOM]</ta>
            <ta e="T582" id="Seg_7196" s="T581">EMPH</ta>
            <ta e="T583" id="Seg_7197" s="T582">голодать-PST2.[3SG]</ta>
            <ta e="T584" id="Seg_7198" s="T583">MOD</ta>
            <ta e="T585" id="Seg_7199" s="T584">откуда</ta>
            <ta e="T586" id="Seg_7200" s="T585">взять-CVB.SEQ</ta>
            <ta e="T587" id="Seg_7201" s="T586">есть-FUT.[3SG]=Q</ta>
            <ta e="T588" id="Seg_7202" s="T587">вот</ta>
            <ta e="T589" id="Seg_7203" s="T588">голодать-CVB.SEQ</ta>
            <ta e="T590" id="Seg_7204" s="T589">умирать-PST2.[3SG]</ta>
            <ta e="T591" id="Seg_7205" s="T590">вот</ta>
            <ta e="T592" id="Seg_7206" s="T591">2SG.[NOM]</ta>
            <ta e="T593" id="Seg_7207" s="T592">EMPH</ta>
            <ta e="T594" id="Seg_7208" s="T593">умирать-PRS-NEC-2SG</ta>
            <ta e="T595" id="Seg_7209" s="T594">быть-PST2.[3SG]</ta>
            <ta e="T596" id="Seg_7210" s="T595">думать-CVB.SEQ</ta>
            <ta e="T597" id="Seg_7211" s="T596">эй</ta>
            <ta e="T598" id="Seg_7212" s="T597">приходить-PST2.[3SG]</ta>
            <ta e="T599" id="Seg_7213" s="T598">тук-тук</ta>
            <ta e="T600" id="Seg_7214" s="T599">тук-тук</ta>
            <ta e="T601" id="Seg_7215" s="T600">стучать-PST2.[3SG]</ta>
            <ta e="T602" id="Seg_7216" s="T601">EMPH</ta>
            <ta e="T603" id="Seg_7217" s="T602">откуда</ta>
            <ta e="T604" id="Seg_7218" s="T603">глаз-3SG-ABL</ta>
            <ta e="T605" id="Seg_7219" s="T604">есть-PTCP.FUT-DAT/LOC</ta>
            <ta e="T606" id="Seg_7220" s="T605">думать-CVB.SEQ</ta>
            <ta e="T607" id="Seg_7221" s="T606">предплечье-DAT/LOC</ta>
            <ta e="T608" id="Seg_7222" s="T607">стучать-MED-CVB.SIM</ta>
            <ta e="T609" id="Seg_7223" s="T608">идти-TEMP-3SG</ta>
            <ta e="T610" id="Seg_7224" s="T609">вовсе</ta>
            <ta e="T611" id="Seg_7225" s="T682">лиса.[NOM]</ta>
            <ta e="T612" id="Seg_7226" s="T611">однако</ta>
            <ta e="T613" id="Seg_7227" s="T612">последний</ta>
            <ta e="T614" id="Seg_7228" s="T613">хитрость-3SG-INSTR</ta>
            <ta e="T615" id="Seg_7229" s="T614">последний</ta>
            <ta e="T616" id="Seg_7230" s="T615">чудовище-3SG-INSTR</ta>
            <ta e="T617" id="Seg_7231" s="T616">ворчать-CVB.SIM</ta>
            <ta e="T618" id="Seg_7232" s="T617">делать-CVB.SIM</ta>
            <ta e="T619" id="Seg_7233" s="T618">прыгать-EMOT-PST2.[3SG]</ta>
            <ta e="T621" id="Seg_7234" s="T619">EMPH</ta>
            <ta e="T622" id="Seg_7235" s="T621">тот</ta>
            <ta e="T623" id="Seg_7236" s="T622">мохнатый.канюк.[NOM]</ta>
            <ta e="T624" id="Seg_7237" s="T623">однако</ta>
            <ta e="T625" id="Seg_7238" s="T624">кто.[NOM]</ta>
            <ta e="T626" id="Seg_7239" s="T625">однако</ta>
            <ta e="T627" id="Seg_7240" s="T626">кукша.[NOM]</ta>
            <ta e="T628" id="Seg_7241" s="T627">однако</ta>
            <ta e="T629" id="Seg_7242" s="T628">убежать-CVB.SEQ</ta>
            <ta e="T630" id="Seg_7243" s="T629">успеть-VBZ-NEG.CVB.SIM</ta>
            <ta e="T631" id="Seg_7244" s="T630">хвост-3SG-ABL</ta>
            <ta e="T632" id="Seg_7245" s="T631">поймать-CAUS-PST2.[3SG]</ta>
            <ta e="T633" id="Seg_7246" s="T632">так</ta>
            <ta e="T634" id="Seg_7247" s="T633">делать-CVB.SEQ</ta>
            <ta e="T635" id="Seg_7248" s="T634">этот-ACC</ta>
            <ta e="T636" id="Seg_7249" s="T635">теребить-FREQ-EP-RECP/COLL-CVB.SEQ</ta>
            <ta e="T637" id="Seg_7250" s="T636">теребить-EP-RECP/COLL-CVB.SEQ</ta>
            <ta e="T638" id="Seg_7251" s="T637">вода.[NOM]</ta>
            <ta e="T639" id="Seg_7252" s="T638">нутро-3SG-DAT/LOC</ta>
            <ta e="T640" id="Seg_7253" s="T639">кататься-CAUS-CVB.SEQ</ta>
            <ta e="T641" id="Seg_7254" s="T640">падать-EP-CAUS-CVB.SEQ</ta>
            <ta e="T642" id="Seg_7255" s="T641">вода.[NOM]</ta>
            <ta e="T645" id="Seg_7256" s="T644">вода.[NOM]</ta>
            <ta e="T646" id="Seg_7257" s="T645">нутро-3SG-DAT/LOC</ta>
            <ta e="T647" id="Seg_7258" s="T646">падать-EP-CAUS-CVB.SEQ</ta>
            <ta e="T648" id="Seg_7259" s="T647">этот-DAT/LOC</ta>
            <ta e="T649" id="Seg_7260" s="T648">полоскать-CVB.SEQ</ta>
            <ta e="T650" id="Seg_7261" s="T649">ил-PROPR</ta>
            <ta e="T651" id="Seg_7262" s="T650">вода-DAT/LOC</ta>
            <ta e="T652" id="Seg_7263" s="T651">полоскать-CVB.SEQ</ta>
            <ta e="T653" id="Seg_7264" s="T652">душить-CVB.SEQ</ta>
            <ta e="T654" id="Seg_7265" s="T653">наружу</ta>
            <ta e="T655" id="Seg_7266" s="T654">держать-CAUS-CVB.SEQ</ta>
            <ta e="T656" id="Seg_7267" s="T655">летать-CVB.SEQ</ta>
            <ta e="T657" id="Seg_7268" s="T656">оставаться-PST2.[3SG]</ta>
            <ta e="T658" id="Seg_7269" s="T657">вот</ta>
            <ta e="T659" id="Seg_7270" s="T658">тот</ta>
            <ta e="T660" id="Seg_7271" s="T659">лиса.[NOM]</ta>
            <ta e="T661" id="Seg_7272" s="T660">однако</ta>
            <ta e="T662" id="Seg_7273" s="T661">берег-DAT/LOC</ta>
            <ta e="T663" id="Seg_7274" s="T662">выйти-CVB.SEQ</ta>
            <ta e="T664" id="Seg_7275" s="T663">высыхать-PASS/REFL-CVB.SIM</ta>
            <ta e="T665" id="Seg_7276" s="T664">MOD</ta>
            <ta e="T666" id="Seg_7277" s="T665">очевидно</ta>
            <ta e="T667" id="Seg_7278" s="T666">вот</ta>
            <ta e="T668" id="Seg_7279" s="T667">тот</ta>
            <ta e="T669" id="Seg_7280" s="T668">высыхать-PASS/REFL-CVB.SEQ-и.так.далее-CVB.SEQ</ta>
            <ta e="T671" id="Seg_7281" s="T669">после</ta>
            <ta e="T672" id="Seg_7282" s="T671">оживать-CVB.SEQ</ta>
            <ta e="T673" id="Seg_7283" s="T672">быть.богатым-CVB.SEQ-наесться-CVB.SEQ</ta>
            <ta e="T674" id="Seg_7284" s="T673">жить-PST2-3SG</ta>
            <ta e="T675" id="Seg_7285" s="T674">Q</ta>
            <ta e="T676" id="Seg_7286" s="T675">куда</ta>
            <ta e="T677" id="Seg_7287" s="T676">становиться-PST2-3SG</ta>
            <ta e="T678" id="Seg_7288" s="T677">Q</ta>
            <ta e="T679" id="Seg_7289" s="T678">вот</ta>
            <ta e="T680" id="Seg_7290" s="T679">сказка-1PL.[NOM]</ta>
            <ta e="T681" id="Seg_7291" s="T680">последний-3SG.[NOM]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_7292" s="T0">dempro</ta>
            <ta e="T2" id="Seg_7293" s="T1">adv</ta>
            <ta e="T3" id="Seg_7294" s="T2">adj-n:case</ta>
            <ta e="T4" id="Seg_7295" s="T3">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T5" id="Seg_7296" s="T4">n-n:case</ta>
            <ta e="T6" id="Seg_7297" s="T5">n-n&gt;v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T7" id="Seg_7298" s="T6">v-v:tense-v:pred.pn</ta>
            <ta e="T8" id="Seg_7299" s="T7">ptcl</ta>
            <ta e="T9" id="Seg_7300" s="T8">dempro</ta>
            <ta e="T10" id="Seg_7301" s="T9">dempro</ta>
            <ta e="T11" id="Seg_7302" s="T10">pers-pro:case</ta>
            <ta e="T12" id="Seg_7303" s="T11">adj.[n:case]</ta>
            <ta e="T13" id="Seg_7304" s="T12">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T14" id="Seg_7305" s="T13">que-pro:case</ta>
            <ta e="T15" id="Seg_7306" s="T14">ptcl</ta>
            <ta e="T16" id="Seg_7307" s="T15">v-v&gt;n-n:case</ta>
            <ta e="T17" id="Seg_7308" s="T16">ptcl</ta>
            <ta e="T18" id="Seg_7309" s="T17">v-v:tense-v:poss.pn</ta>
            <ta e="T19" id="Seg_7310" s="T18">dempro-pro:case</ta>
            <ta e="T20" id="Seg_7311" s="T19">n-n:(num)-n:case</ta>
            <ta e="T21" id="Seg_7312" s="T20">v-v&gt;v-v:(ins)-v:(neg)-v:cvb-v:pred.pn</ta>
            <ta e="T22" id="Seg_7313" s="T21">v-v:(neg)-v:cvb-v:pred.pn</ta>
            <ta e="T23" id="Seg_7314" s="T22">dempro-pro:case</ta>
            <ta e="T24" id="Seg_7315" s="T23">n-n:(poss)-n&gt;adj-n:case</ta>
            <ta e="T25" id="Seg_7316" s="T24">n-n:(poss)-n:case</ta>
            <ta e="T26" id="Seg_7317" s="T25">n-n:poss-n&gt;adj-n:case</ta>
            <ta e="T27" id="Seg_7318" s="T26">n-n:case</ta>
            <ta e="T28" id="Seg_7319" s="T27">n-n&gt;v-v:ptcp</ta>
            <ta e="T29" id="Seg_7320" s="T28">n-n:(poss)-n:case</ta>
            <ta e="T30" id="Seg_7321" s="T29">v-v:tense-v:poss.pn</ta>
            <ta e="T31" id="Seg_7322" s="T30">adv</ta>
            <ta e="T32" id="Seg_7323" s="T31">n-n:case</ta>
            <ta e="T33" id="Seg_7324" s="T32">n-n:(poss)-n:case</ta>
            <ta e="T34" id="Seg_7325" s="T33">v-v:cvb</ta>
            <ta e="T35" id="Seg_7326" s="T34">adv</ta>
            <ta e="T36" id="Seg_7327" s="T35">n-n:case</ta>
            <ta e="T37" id="Seg_7328" s="T36">n-n:case</ta>
            <ta e="T38" id="Seg_7329" s="T37">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T39" id="Seg_7330" s="T38">post</ta>
            <ta e="T40" id="Seg_7331" s="T39">adj-n:(poss)-n:case</ta>
            <ta e="T41" id="Seg_7332" s="T40">n-n&gt;adj-n:case</ta>
            <ta e="T42" id="Seg_7333" s="T41">v-v:tense-v:poss.pn</ta>
            <ta e="T43" id="Seg_7334" s="T42">ptcl</ta>
            <ta e="T44" id="Seg_7335" s="T43">n-n:(num)-n:case</ta>
            <ta e="T45" id="Seg_7336" s="T44">adj-n:(poss)-n:case</ta>
            <ta e="T46" id="Seg_7337" s="T45">dempro</ta>
            <ta e="T47" id="Seg_7338" s="T46">n-n:(poss)-n:case</ta>
            <ta e="T48" id="Seg_7339" s="T47">interj</ta>
            <ta e="T49" id="Seg_7340" s="T48">n-n:(poss)-n:case</ta>
            <ta e="T50" id="Seg_7341" s="T49">dempro</ta>
            <ta e="T51" id="Seg_7342" s="T50">n-n:(num)-n:case</ta>
            <ta e="T52" id="Seg_7343" s="T51">v-v:(ins)-v:ptcp</ta>
            <ta e="T53" id="Seg_7344" s="T52">n-n:poss-n:case</ta>
            <ta e="T54" id="Seg_7345" s="T53">dempro-pro:case</ta>
            <ta e="T55" id="Seg_7346" s="T54">v-v:tense-v:pred.pn</ta>
            <ta e="T56" id="Seg_7347" s="T55">adv</ta>
            <ta e="T57" id="Seg_7348" s="T56">ptcl</ta>
            <ta e="T58" id="Seg_7349" s="T57">n-n:case</ta>
            <ta e="T59" id="Seg_7350" s="T58">v-v:tense-v:pred.pn</ta>
            <ta e="T60" id="Seg_7351" s="T59">ptcl</ta>
            <ta e="T61" id="Seg_7352" s="T60">adv-adv</ta>
            <ta e="T62" id="Seg_7353" s="T61">n-n:case</ta>
            <ta e="T63" id="Seg_7354" s="T62">dempro</ta>
            <ta e="T64" id="Seg_7355" s="T63">n-n:case</ta>
            <ta e="T65" id="Seg_7356" s="T64">ptcl</ta>
            <ta e="T66" id="Seg_7357" s="T65">n-n:case</ta>
            <ta e="T67" id="Seg_7358" s="T66">n-n:(poss)-n:case</ta>
            <ta e="T68" id="Seg_7359" s="T67">v-v:cvb</ta>
            <ta e="T69" id="Seg_7360" s="T68">v-v:mood-v:temp.pn</ta>
            <ta e="T70" id="Seg_7361" s="T69">adv</ta>
            <ta e="T71" id="Seg_7362" s="T70">que</ta>
            <ta e="T72" id="Seg_7363" s="T71">ptcl</ta>
            <ta e="T73" id="Seg_7364" s="T72">n-n:case</ta>
            <ta e="T74" id="Seg_7365" s="T73">v-v:cvb</ta>
            <ta e="T75" id="Seg_7366" s="T74">v-v:ptcp</ta>
            <ta e="T76" id="Seg_7367" s="T75">n-n:(poss)-n:case</ta>
            <ta e="T77" id="Seg_7368" s="T76">ptcl</ta>
            <ta e="T78" id="Seg_7369" s="T77">adj</ta>
            <ta e="T79" id="Seg_7370" s="T78">adj</ta>
            <ta e="T80" id="Seg_7371" s="T79">n-n:(poss)-n:case</ta>
            <ta e="T81" id="Seg_7372" s="T80">v-v:ptcp</ta>
            <ta e="T82" id="Seg_7373" s="T81">v-v:tense-v:poss.pn</ta>
            <ta e="T83" id="Seg_7374" s="T82">adj</ta>
            <ta e="T84" id="Seg_7375" s="T83">n-n&gt;v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T85" id="Seg_7376" s="T84">n-n:case</ta>
            <ta e="T86" id="Seg_7377" s="T85">n-n:case</ta>
            <ta e="T87" id="Seg_7378" s="T86">v-v:tense-v:pred.pn</ta>
            <ta e="T88" id="Seg_7379" s="T87">adv</ta>
            <ta e="T89" id="Seg_7380" s="T88">v-v:cvb</ta>
            <ta e="T90" id="Seg_7381" s="T89">n-n:(poss)</ta>
            <ta e="T91" id="Seg_7382" s="T90">ptcl</ta>
            <ta e="T92" id="Seg_7383" s="T91">v-v:cvb</ta>
            <ta e="T93" id="Seg_7384" s="T92">v-v:cvb</ta>
            <ta e="T94" id="Seg_7385" s="T93">n-n:case</ta>
            <ta e="T95" id="Seg_7386" s="T94">n-n:case</ta>
            <ta e="T96" id="Seg_7387" s="T95">v-v:cvb</ta>
            <ta e="T97" id="Seg_7388" s="T96">v-v:tense-v:poss.pn</ta>
            <ta e="T98" id="Seg_7389" s="T97">n-n:case</ta>
            <ta e="T99" id="Seg_7390" s="T98">dempro</ta>
            <ta e="T100" id="Seg_7391" s="T99">v-v:cvb</ta>
            <ta e="T101" id="Seg_7392" s="T100">v-v:cvb</ta>
            <ta e="T103" id="Seg_7393" s="T101">ptcl</ta>
            <ta e="T104" id="Seg_7394" s="T103">interj</ta>
            <ta e="T105" id="Seg_7395" s="T104">dempro</ta>
            <ta e="T106" id="Seg_7396" s="T105">que</ta>
            <ta e="T107" id="Seg_7397" s="T106">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T108" id="Seg_7398" s="T107">v-v:cvb</ta>
            <ta e="T109" id="Seg_7399" s="T108">n-n:case</ta>
            <ta e="T110" id="Seg_7400" s="T109">n-n:poss-n:case</ta>
            <ta e="T111" id="Seg_7401" s="T110">v-v:cvb</ta>
            <ta e="T112" id="Seg_7402" s="T111">n-n:case</ta>
            <ta e="T113" id="Seg_7403" s="T112">n-n:poss-n:case</ta>
            <ta e="T114" id="Seg_7404" s="T113">v-v:cvb</ta>
            <ta e="T115" id="Seg_7405" s="T114">v-v:tense-v:pred.pn</ta>
            <ta e="T116" id="Seg_7406" s="T115">interj</ta>
            <ta e="T117" id="Seg_7407" s="T116">v-v:tense-v:pred.pn</ta>
            <ta e="T118" id="Seg_7408" s="T117">v-v&gt;v-v:cvb-v-v&gt;v-v:cvb</ta>
            <ta e="T119" id="Seg_7409" s="T118">v-v:tense-v:poss.pn</ta>
            <ta e="T120" id="Seg_7410" s="T119">adj</ta>
            <ta e="T121" id="Seg_7411" s="T120">n-n:case</ta>
            <ta e="T122" id="Seg_7412" s="T121">n-n:poss-n:case</ta>
            <ta e="T123" id="Seg_7413" s="T122">n-n:case</ta>
            <ta e="T124" id="Seg_7414" s="T123">v-v:cvb</ta>
            <ta e="T125" id="Seg_7415" s="T124">post</ta>
            <ta e="T126" id="Seg_7416" s="T125">v-v:tense-v:pred.pn</ta>
            <ta e="T127" id="Seg_7417" s="T126">interj</ta>
            <ta e="T128" id="Seg_7418" s="T127">ptcl</ta>
            <ta e="T129" id="Seg_7419" s="T128">adv</ta>
            <ta e="T130" id="Seg_7420" s="T129">pers-pro:case</ta>
            <ta e="T131" id="Seg_7421" s="T130">que</ta>
            <ta e="T132" id="Seg_7422" s="T131">v-v:cvb-v:pred.pn</ta>
            <ta e="T133" id="Seg_7423" s="T132">n-n&gt;v-v:cvb-v:pred.pn</ta>
            <ta e="T134" id="Seg_7424" s="T133">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T135" id="Seg_7425" s="T134">dempro</ta>
            <ta e="T136" id="Seg_7426" s="T135">n-n:case</ta>
            <ta e="T137" id="Seg_7427" s="T136">que-pro:case</ta>
            <ta e="T138" id="Seg_7428" s="T137">n-n:case</ta>
            <ta e="T139" id="Seg_7429" s="T138">v-v:cvb</ta>
            <ta e="T140" id="Seg_7430" s="T139">dempro</ta>
            <ta e="T141" id="Seg_7431" s="T140">ptcl</ta>
            <ta e="T142" id="Seg_7432" s="T141">n-n:case</ta>
            <ta e="T145" id="Seg_7433" s="T144">n-n:case</ta>
            <ta e="T146" id="Seg_7434" s="T145">n-n:case</ta>
            <ta e="T147" id="Seg_7435" s="T146">v-v:cvb</ta>
            <ta e="T148" id="Seg_7436" s="T147">v-v:cvb</ta>
            <ta e="T149" id="Seg_7437" s="T148">v-v:cvb</ta>
            <ta e="T150" id="Seg_7438" s="T149">ptcl</ta>
            <ta e="T151" id="Seg_7439" s="T150">que-pro:(poss)-pro:case</ta>
            <ta e="T152" id="Seg_7440" s="T151">n-n:poss-n:case</ta>
            <ta e="T153" id="Seg_7441" s="T152">v-v:tense-v:pred.pn</ta>
            <ta e="T154" id="Seg_7442" s="T153">n-n:poss-n:case</ta>
            <ta e="T155" id="Seg_7443" s="T154">n-n:poss-n:case</ta>
            <ta e="T156" id="Seg_7444" s="T155">v-v:tense-v:pred.pn</ta>
            <ta e="T157" id="Seg_7445" s="T156">adv</ta>
            <ta e="T158" id="Seg_7446" s="T157">v-v:cvb</ta>
            <ta e="T159" id="Seg_7447" s="T158">post</ta>
            <ta e="T160" id="Seg_7448" s="T159">n-n:poss-n:case</ta>
            <ta e="T161" id="Seg_7449" s="T160">n-n:poss-n:case</ta>
            <ta e="T162" id="Seg_7450" s="T161">v-v:cvb</ta>
            <ta e="T163" id="Seg_7451" s="T162">v-v:tense-v:poss.pn</ta>
            <ta e="T164" id="Seg_7452" s="T163">n-n:case</ta>
            <ta e="T165" id="Seg_7453" s="T164">n-n:case</ta>
            <ta e="T166" id="Seg_7454" s="T165">adj</ta>
            <ta e="T167" id="Seg_7455" s="T166">ptcl</ta>
            <ta e="T168" id="Seg_7456" s="T167">v-v:mood.pn</ta>
            <ta e="T169" id="Seg_7457" s="T168">cardnum</ta>
            <ta e="T170" id="Seg_7458" s="T169">n-n:poss-n:case</ta>
            <ta e="T171" id="Seg_7459" s="T170">pers-pro:case</ta>
            <ta e="T172" id="Seg_7460" s="T171">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T173" id="Seg_7461" s="T172">adj</ta>
            <ta e="T174" id="Seg_7462" s="T173">n-n:poss-n:case</ta>
            <ta e="T175" id="Seg_7463" s="T174">adj-n:case</ta>
            <ta e="T176" id="Seg_7464" s="T175">n-n:case</ta>
            <ta e="T177" id="Seg_7465" s="T176">ptcl-n:case</ta>
            <ta e="T178" id="Seg_7466" s="T177">v-v:cvb</ta>
            <ta e="T179" id="Seg_7467" s="T178">v-v:cvb</ta>
            <ta e="T180" id="Seg_7468" s="T179">v-v:(ins)-v&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T181" id="Seg_7469" s="T180">emphpro-pro:case-pro:(poss)-pro:case</ta>
            <ta e="T182" id="Seg_7470" s="T181">v-v:tense-v:poss.pn</ta>
            <ta e="T183" id="Seg_7471" s="T182">n-n:case</ta>
            <ta e="T184" id="Seg_7472" s="T183">v-v:tense-v:pred.pn</ta>
            <ta e="T185" id="Seg_7473" s="T184">ptcl</ta>
            <ta e="T186" id="Seg_7474" s="T185">v-v:cvb-v-v:cvb</ta>
            <ta e="T187" id="Seg_7475" s="T186">cardnum</ta>
            <ta e="T188" id="Seg_7476" s="T187">n-n:poss-n:case</ta>
            <ta e="T189" id="Seg_7477" s="T188">v-v&gt;n-n:case</ta>
            <ta e="T190" id="Seg_7478" s="T189">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T191" id="Seg_7479" s="T190">cardnum</ta>
            <ta e="T192" id="Seg_7480" s="T191">n-n:poss-n:case</ta>
            <ta e="T193" id="Seg_7481" s="T192">v-v:tense-v:pred.pn</ta>
            <ta e="T194" id="Seg_7482" s="T193">adv</ta>
            <ta e="T195" id="Seg_7483" s="T194">v-v:cvb</ta>
            <ta e="T196" id="Seg_7484" s="T195">post</ta>
            <ta e="T197" id="Seg_7485" s="T196">n-n:case</ta>
            <ta e="T198" id="Seg_7486" s="T197">n-n:case</ta>
            <ta e="T199" id="Seg_7487" s="T198">v-v:cvb</ta>
            <ta e="T200" id="Seg_7488" s="T199">v-v:tense-v:pred.pn</ta>
            <ta e="T201" id="Seg_7489" s="T200">n-n:poss-n:case</ta>
            <ta e="T202" id="Seg_7490" s="T201">v-v:tense-v:pred.pn</ta>
            <ta e="T203" id="Seg_7491" s="T202">ptcl</ta>
            <ta e="T204" id="Seg_7492" s="T203">n-n:case</ta>
            <ta e="T205" id="Seg_7493" s="T204">n-n&gt;v</ta>
            <ta e="T206" id="Seg_7494" s="T205">n-n:poss-n:case</ta>
            <ta e="T207" id="Seg_7495" s="T206">n-n&gt;v-v:cvb</ta>
            <ta e="T208" id="Seg_7496" s="T207">ptcl</ta>
            <ta e="T209" id="Seg_7497" s="T208">v-v:cvb</ta>
            <ta e="T210" id="Seg_7498" s="T209">v-v:tense-v:pred.pn</ta>
            <ta e="T211" id="Seg_7499" s="T210">n-n:case</ta>
            <ta e="T212" id="Seg_7500" s="T211">n-n:case</ta>
            <ta e="T213" id="Seg_7501" s="T212">adj</ta>
            <ta e="T214" id="Seg_7502" s="T213">ptcl</ta>
            <ta e="T215" id="Seg_7503" s="T214">v-v:mood.pn</ta>
            <ta e="T216" id="Seg_7504" s="T215">cardnum</ta>
            <ta e="T217" id="Seg_7505" s="T216">n-n:poss-n:case</ta>
            <ta e="T218" id="Seg_7506" s="T217">pers-pro:case</ta>
            <ta e="T219" id="Seg_7507" s="T218">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T220" id="Seg_7508" s="T219">adj</ta>
            <ta e="T221" id="Seg_7509" s="T220">n-n:poss-n:case</ta>
            <ta e="T222" id="Seg_7510" s="T221">adj-n:case</ta>
            <ta e="T223" id="Seg_7511" s="T222">n-n:case-n:poss-n:case-ptcl-n:case-n:poss-n:case</ta>
            <ta e="T224" id="Seg_7512" s="T223">v-v:cvb</ta>
            <ta e="T225" id="Seg_7513" s="T224">v-v:cvb</ta>
            <ta e="T226" id="Seg_7514" s="T225">v-v:(ins)-v&gt;v-v:cvb-v:pred.pn</ta>
            <ta e="T227" id="Seg_7515" s="T226">adv</ta>
            <ta e="T228" id="Seg_7516" s="T227">emphpro-pro:case-pro:(poss)-pro:case</ta>
            <ta e="T229" id="Seg_7517" s="T228">v-v:tense-v:poss.pn</ta>
            <ta e="T230" id="Seg_7518" s="T229">ptcl</ta>
            <ta e="T231" id="Seg_7519" s="T230">cardnum</ta>
            <ta e="T232" id="Seg_7520" s="T231">n-n:poss-n:case</ta>
            <ta e="T233" id="Seg_7521" s="T232">v-v:cvb</ta>
            <ta e="T234" id="Seg_7522" s="T233">v-v:tense-v:pred.pn</ta>
            <ta e="T235" id="Seg_7523" s="T234">cardnum</ta>
            <ta e="T236" id="Seg_7524" s="T235">n-n:poss-n:case</ta>
            <ta e="T237" id="Seg_7525" s="T236">v-v:tense-v:pred.pn</ta>
            <ta e="T238" id="Seg_7526" s="T237">n-n:case</ta>
            <ta e="T239" id="Seg_7527" s="T238">n-n:case</ta>
            <ta e="T240" id="Seg_7528" s="T239">v-v:cvb</ta>
            <ta e="T241" id="Seg_7529" s="T240">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T242" id="Seg_7530" s="T241">ptcl</ta>
            <ta e="T243" id="Seg_7531" s="T242">ptcl</ta>
            <ta e="T244" id="Seg_7532" s="T243">dempro</ta>
            <ta e="T245" id="Seg_7533" s="T244">adv</ta>
            <ta e="T246" id="Seg_7534" s="T245">ptcl</ta>
            <ta e="T247" id="Seg_7535" s="T246">emphpro-pro:case-pro:(poss)-pro:case</ta>
            <ta e="T248" id="Seg_7536" s="T247">v-v:cvb-v:(case)</ta>
            <ta e="T249" id="Seg_7537" s="T248">v-v:cvb</ta>
            <ta e="T250" id="Seg_7538" s="T249">v-v:cvb</ta>
            <ta e="T251" id="Seg_7539" s="T250">v-v:tense-v:pred.pn</ta>
            <ta e="T252" id="Seg_7540" s="T251">interj</ta>
            <ta e="T253" id="Seg_7541" s="T252">dempro</ta>
            <ta e="T254" id="Seg_7542" s="T253">v-v:mood-v:temp.pn</ta>
            <ta e="T255" id="Seg_7543" s="T254">n-n:case</ta>
            <ta e="T256" id="Seg_7544" s="T255">v-v:cvb</ta>
            <ta e="T257" id="Seg_7545" s="T256">v-v:tense-v:pred.pn</ta>
            <ta e="T258" id="Seg_7546" s="T257">interj</ta>
            <ta e="T259" id="Seg_7547" s="T258">n-n:case</ta>
            <ta e="T260" id="Seg_7548" s="T259">que-pro:case</ta>
            <ta e="T261" id="Seg_7549" s="T260">v-v:tense-v:pred.pn</ta>
            <ta e="T262" id="Seg_7550" s="T261">ptcl</ta>
            <ta e="T263" id="Seg_7551" s="T262">pers-pro:case</ta>
            <ta e="T264" id="Seg_7552" s="T263">dempro</ta>
            <ta e="T265" id="Seg_7553" s="T264">dempro</ta>
            <ta e="T266" id="Seg_7554" s="T265">adj</ta>
            <ta e="T267" id="Seg_7555" s="T266">n-n&gt;adj</ta>
            <ta e="T268" id="Seg_7556" s="T267">n-n:case</ta>
            <ta e="T269" id="Seg_7557" s="T268">n-n:(poss)-n:case</ta>
            <ta e="T270" id="Seg_7558" s="T269">v-v:mood-v:temp.pn</ta>
            <ta e="T271" id="Seg_7559" s="T270">n-n:(num)-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T272" id="Seg_7560" s="T271">conj</ta>
            <ta e="T273" id="Seg_7561" s="T272">adj-n:case</ta>
            <ta e="T274" id="Seg_7562" s="T273">v-v:tense-v:pred.pn</ta>
            <ta e="T275" id="Seg_7563" s="T274">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T276" id="Seg_7564" s="T275">conj</ta>
            <ta e="T277" id="Seg_7565" s="T276">n-n:case</ta>
            <ta e="T278" id="Seg_7566" s="T277">ptcl</ta>
            <ta e="T279" id="Seg_7567" s="T278">v-v:cvb</ta>
            <ta e="T280" id="Seg_7568" s="T279">v-v:ptcp-v:(case)</ta>
            <ta e="T281" id="Seg_7569" s="T280">post</ta>
            <ta e="T282" id="Seg_7570" s="T281">ptcl</ta>
            <ta e="T283" id="Seg_7571" s="T282">que-pro:case</ta>
            <ta e="T284" id="Seg_7572" s="T283">v-v:tense-v:pred.pn</ta>
            <ta e="T285" id="Seg_7573" s="T284">ptcl</ta>
            <ta e="T286" id="Seg_7574" s="T285">dempro</ta>
            <ta e="T287" id="Seg_7575" s="T286">adj-n:case</ta>
            <ta e="T288" id="Seg_7576" s="T287">v-v:cvb-v:pred.pn</ta>
            <ta e="T289" id="Seg_7577" s="T288">v-v:tense-v:pred.pn</ta>
            <ta e="T290" id="Seg_7578" s="T289">pers-pro:case</ta>
            <ta e="T291" id="Seg_7579" s="T290">que</ta>
            <ta e="T292" id="Seg_7580" s="T291">v-v:(ins)-v&gt;v-v:(neg)-v:pred.pn</ta>
            <ta e="T293" id="Seg_7581" s="T292">v-v:(neg)-v:pred.pn</ta>
            <ta e="T294" id="Seg_7582" s="T293">ptcl</ta>
            <ta e="T295" id="Seg_7583" s="T294">pers-pro:case</ta>
            <ta e="T296" id="Seg_7584" s="T295">ptcl</ta>
            <ta e="T297" id="Seg_7585" s="T296">n-n:case</ta>
            <ta e="T298" id="Seg_7586" s="T297">ptcl</ta>
            <ta e="T299" id="Seg_7587" s="T298">cardnum</ta>
            <ta e="T300" id="Seg_7588" s="T299">n-n:case</ta>
            <ta e="T301" id="Seg_7589" s="T300">cardnum</ta>
            <ta e="T302" id="Seg_7590" s="T301">n-n:poss-n:case</ta>
            <ta e="T303" id="Seg_7591" s="T302">v-v:tense-v:poss.pn</ta>
            <ta e="T304" id="Seg_7592" s="T303">adv</ta>
            <ta e="T305" id="Seg_7593" s="T304">adv</ta>
            <ta e="T306" id="Seg_7594" s="T305">ptcl</ta>
            <ta e="T307" id="Seg_7595" s="T306">v-v:tense-v:poss.pn</ta>
            <ta e="T308" id="Seg_7596" s="T307">v-v:tense-v:poss.pn</ta>
            <ta e="T309" id="Seg_7597" s="T308">n-n:case-n:poss-n:case</ta>
            <ta e="T310" id="Seg_7598" s="T309">ptcl-n:case-n:poss-n:case</ta>
            <ta e="T311" id="Seg_7599" s="T310">v-v:cvb</ta>
            <ta e="T312" id="Seg_7600" s="T311">v-v:cvb</ta>
            <ta e="T313" id="Seg_7601" s="T312">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T314" id="Seg_7602" s="T313">emphpro-pro:case-pro:(poss)-pro:case</ta>
            <ta e="T315" id="Seg_7603" s="T314">v-v:cvb</ta>
            <ta e="T316" id="Seg_7604" s="T315">v-v:tense-v:poss.pn</ta>
            <ta e="T317" id="Seg_7605" s="T316">v-v:tense-v:pred.pn</ta>
            <ta e="T318" id="Seg_7606" s="T317">que-que&gt;adj-pro:case-ptcl</ta>
            <ta e="T319" id="Seg_7607" s="T318">dempro</ta>
            <ta e="T320" id="Seg_7608" s="T319">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T321" id="Seg_7609" s="T320">interj</ta>
            <ta e="T322" id="Seg_7610" s="T321">n-n:(poss)-n:case</ta>
            <ta e="T323" id="Seg_7611" s="T322">adj</ta>
            <ta e="T324" id="Seg_7612" s="T323">n-n:poss-n:case</ta>
            <ta e="T325" id="Seg_7613" s="T324">adj-n:case</ta>
            <ta e="T326" id="Seg_7614" s="T325">ptcl</ta>
            <ta e="T327" id="Seg_7615" s="T326">que-pro:case</ta>
            <ta e="T328" id="Seg_7616" s="T327">n-n&gt;adj-n:(poss)-n:case</ta>
            <ta e="T329" id="Seg_7617" s="T328">adj</ta>
            <ta e="T330" id="Seg_7618" s="T329">n-n:(poss)-n:case</ta>
            <ta e="T331" id="Seg_7619" s="T330">v-v:tense-v:(ins)-v:poss.pn-ptcl</ta>
            <ta e="T332" id="Seg_7620" s="T331">pers-pro:case</ta>
            <ta e="T333" id="Seg_7621" s="T332">dempro-pro:case</ta>
            <ta e="T334" id="Seg_7622" s="T333">ptcl</ta>
            <ta e="T335" id="Seg_7623" s="T334">n-n:case</ta>
            <ta e="T336" id="Seg_7624" s="T335">que</ta>
            <ta e="T337" id="Seg_7625" s="T336">v-v:cvb</ta>
            <ta e="T338" id="Seg_7626" s="T337">n-n:case</ta>
            <ta e="T339" id="Seg_7627" s="T338">que</ta>
            <ta e="T340" id="Seg_7628" s="T339">v-v:cvb</ta>
            <ta e="T341" id="Seg_7629" s="T340">pers-pro:case</ta>
            <ta e="T342" id="Seg_7630" s="T341">n-n:poss-n:case</ta>
            <ta e="T343" id="Seg_7631" s="T342">v-v:cvb</ta>
            <ta e="T344" id="Seg_7632" s="T343">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T345" id="Seg_7633" s="T344">n-n:(poss)-n:case</ta>
            <ta e="T346" id="Seg_7634" s="T345">n-n:case</ta>
            <ta e="T347" id="Seg_7635" s="T346">n-n:case</ta>
            <ta e="T348" id="Seg_7636" s="T347">n-n:case</ta>
            <ta e="T349" id="Seg_7637" s="T348">que-pro:case</ta>
            <ta e="T350" id="Seg_7638" s="T349">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T351" id="Seg_7639" s="T350">v-v:mood.pn</ta>
            <ta e="T352" id="Seg_7640" s="T351">adv</ta>
            <ta e="T353" id="Seg_7641" s="T352">v-v:mood-v:temp.pn</ta>
            <ta e="T354" id="Seg_7642" s="T353">que-pro:case</ta>
            <ta e="T355" id="Seg_7643" s="T354">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T356" id="Seg_7644" s="T355">v-v:mood-v:temp.pn</ta>
            <ta e="T357" id="Seg_7645" s="T356">n-n:case</ta>
            <ta e="T358" id="Seg_7646" s="T357">v-v:tense-v:poss.pn</ta>
            <ta e="T359" id="Seg_7647" s="T358">v-v:(tense)-v:mood.pn</ta>
            <ta e="T360" id="Seg_7648" s="T359">ptcl</ta>
            <ta e="T361" id="Seg_7649" s="T360">n-n:case</ta>
            <ta e="T364" id="Seg_7650" s="T363">n-n:case</ta>
            <ta e="T365" id="Seg_7651" s="T364">ptcl</ta>
            <ta e="T366" id="Seg_7652" s="T365">adv</ta>
            <ta e="T367" id="Seg_7653" s="T366">ptcl</ta>
            <ta e="T368" id="Seg_7654" s="T367">v-v:tense-v:pred.pn</ta>
            <ta e="T369" id="Seg_7655" s="T368">v-v:tense-v:pred.pn</ta>
            <ta e="T370" id="Seg_7656" s="T369">n-n:(poss)-n:case</ta>
            <ta e="T371" id="Seg_7657" s="T370">v-v:cvb</ta>
            <ta e="T372" id="Seg_7658" s="T371">v-v:tense-v:pred.pn</ta>
            <ta e="T373" id="Seg_7659" s="T372">interj</ta>
            <ta e="T374" id="Seg_7660" s="T373">ptcl</ta>
            <ta e="T375" id="Seg_7661" s="T374">n-n:case</ta>
            <ta e="T376" id="Seg_7662" s="T375">v-v:cvb</ta>
            <ta e="T377" id="Seg_7663" s="T376">v-v:ptcp</ta>
            <ta e="T378" id="Seg_7664" s="T377">v-v:tense-v:pred.pn</ta>
            <ta e="T379" id="Seg_7665" s="T378">v-v:cvb</ta>
            <ta e="T380" id="Seg_7666" s="T379">v-v:ptcp</ta>
            <ta e="T381" id="Seg_7667" s="T380">v-v:tense-v:pred.pn</ta>
            <ta e="T382" id="Seg_7668" s="T381">adj-adj</ta>
            <ta e="T383" id="Seg_7669" s="T382">n-n:case</ta>
            <ta e="T384" id="Seg_7670" s="T383">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T385" id="Seg_7671" s="T384">v-v:cvb-v-v:cvb</ta>
            <ta e="T386" id="Seg_7672" s="T385">adj</ta>
            <ta e="T387" id="Seg_7673" s="T386">adj</ta>
            <ta e="T388" id="Seg_7674" s="T387">n-n:poss-n:case</ta>
            <ta e="T389" id="Seg_7675" s="T388">v-v:cvb</ta>
            <ta e="T390" id="Seg_7676" s="T389">v-v:cvb-v-v:cvb</ta>
            <ta e="T391" id="Seg_7677" s="T390">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T392" id="Seg_7678" s="T391">ptcl</ta>
            <ta e="T393" id="Seg_7679" s="T392">n-n:poss-n:case</ta>
            <ta e="T394" id="Seg_7680" s="T393">n-n:case</ta>
            <ta e="T395" id="Seg_7681" s="T394">v-v:cvb</ta>
            <ta e="T396" id="Seg_7682" s="T395">n-n:case</ta>
            <ta e="T397" id="Seg_7683" s="T396">n-n:case</ta>
            <ta e="T398" id="Seg_7684" s="T397">dempro-dempro</ta>
            <ta e="T399" id="Seg_7685" s="T398">n-n&gt;adj-n:case</ta>
            <ta e="T400" id="Seg_7686" s="T399">interj</ta>
            <ta e="T401" id="Seg_7687" s="T400">pers-pro:case</ta>
            <ta e="T402" id="Seg_7688" s="T401">ptcl</ta>
            <ta e="T403" id="Seg_7689" s="T402">ptcl</ta>
            <ta e="T404" id="Seg_7690" s="T403">adv</ta>
            <ta e="T405" id="Seg_7691" s="T404">v-v:cvb</ta>
            <ta e="T406" id="Seg_7692" s="T405">v-v:tense-v:poss.pn</ta>
            <ta e="T407" id="Seg_7693" s="T406">v-v:mood.pn</ta>
            <ta e="T408" id="Seg_7694" s="T407">cardnum</ta>
            <ta e="T409" id="Seg_7695" s="T408">n-n:case</ta>
            <ta e="T410" id="Seg_7696" s="T409">pers-pro:case</ta>
            <ta e="T411" id="Seg_7697" s="T410">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T412" id="Seg_7698" s="T411">adj</ta>
            <ta e="T413" id="Seg_7699" s="T412">n-n:poss-n:case</ta>
            <ta e="T414" id="Seg_7700" s="T413">adj-n:case</ta>
            <ta e="T415" id="Seg_7701" s="T414">adv</ta>
            <ta e="T416" id="Seg_7702" s="T415">emphpro-pro:case-pro:(poss)-pro:case</ta>
            <ta e="T419" id="Seg_7703" s="T418">ptcl</ta>
            <ta e="T420" id="Seg_7704" s="T419">v-v:cvb-v:pred.pn</ta>
            <ta e="T421" id="Seg_7705" s="T420">emphpro-pro:case-pro:(poss)-pro:case</ta>
            <ta e="T422" id="Seg_7706" s="T421">v-v:tense-v:poss.pn</ta>
            <ta e="T423" id="Seg_7707" s="T422">ptcl</ta>
            <ta e="T424" id="Seg_7708" s="T423">ptcl</ta>
            <ta e="T425" id="Seg_7709" s="T424">adj-adj&gt;v-v:cvb-v:pred.pn</ta>
            <ta e="T426" id="Seg_7710" s="T425">adv</ta>
            <ta e="T430" id="Seg_7711" s="T429">adj</ta>
            <ta e="T431" id="Seg_7712" s="T430">cardnum</ta>
            <ta e="T432" id="Seg_7713" s="T431">n-n:poss-n:case</ta>
            <ta e="T433" id="Seg_7714" s="T432">v-v&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T434" id="Seg_7715" s="T433">pers-pro:case</ta>
            <ta e="T435" id="Seg_7716" s="T434">pers-pro:case</ta>
            <ta e="T436" id="Seg_7717" s="T435">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T437" id="Seg_7718" s="T436">adj-n:poss-n:case</ta>
            <ta e="T438" id="Seg_7719" s="T437">pers-pro:case</ta>
            <ta e="T439" id="Seg_7720" s="T438">ptcl</ta>
            <ta e="T440" id="Seg_7721" s="T439">n-n:case</ta>
            <ta e="T441" id="Seg_7722" s="T440">n-n:(pred.pn)</ta>
            <ta e="T442" id="Seg_7723" s="T441">ptcl</ta>
            <ta e="T443" id="Seg_7724" s="T442">n-n:case</ta>
            <ta e="T444" id="Seg_7725" s="T443">n-n:case</ta>
            <ta e="T445" id="Seg_7726" s="T444">que-pro:case</ta>
            <ta e="T446" id="Seg_7727" s="T445">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T447" id="Seg_7728" s="T446">n-n:case</ta>
            <ta e="T448" id="Seg_7729" s="T447">v-v:tense-v:poss.pn</ta>
            <ta e="T449" id="Seg_7730" s="T448">ptcl</ta>
            <ta e="T450" id="Seg_7731" s="T449">v-v:tense-v:pred.pn</ta>
            <ta e="T451" id="Seg_7732" s="T450">ptcl</ta>
            <ta e="T452" id="Seg_7733" s="T451">interj</ta>
            <ta e="T453" id="Seg_7734" s="T452">que-pro:case</ta>
            <ta e="T454" id="Seg_7735" s="T453">n-n&gt;adj-n:(poss)-n:case</ta>
            <ta e="T455" id="Seg_7736" s="T454">n-n:case</ta>
            <ta e="T456" id="Seg_7737" s="T455">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T457" id="Seg_7738" s="T456">pers-pro:case</ta>
            <ta e="T458" id="Seg_7739" s="T457">que</ta>
            <ta e="T459" id="Seg_7740" s="T458">v-v:cvb-v:pred.pn</ta>
            <ta e="T460" id="Seg_7741" s="T459">n-n:case</ta>
            <ta e="T461" id="Seg_7742" s="T460">v-v:tense-v:(ins)-v:poss.pn-ptcl</ta>
            <ta e="T462" id="Seg_7743" s="T461">v-v:tense-v:pred.pn</ta>
            <ta e="T463" id="Seg_7744" s="T462">posspr</ta>
            <ta e="T464" id="Seg_7745" s="T463">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T465" id="Seg_7746" s="T464">n-n:poss-n:case</ta>
            <ta e="T466" id="Seg_7747" s="T465">que</ta>
            <ta e="T467" id="Seg_7748" s="T466">v-v:tense-v:pred.pn</ta>
            <ta e="T468" id="Seg_7749" s="T467">interj</ta>
            <ta e="T469" id="Seg_7750" s="T468">pers-pro:case</ta>
            <ta e="T470" id="Seg_7751" s="T469">n-n:case</ta>
            <ta e="T471" id="Seg_7752" s="T470">v-v:tense-v:poss.pn</ta>
            <ta e="T472" id="Seg_7753" s="T471">que-pro:case</ta>
            <ta e="T473" id="Seg_7754" s="T472">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T474" id="Seg_7755" s="T473">pers-pro:case</ta>
            <ta e="T475" id="Seg_7756" s="T474">n-n:case</ta>
            <ta e="T476" id="Seg_7757" s="T475">n-n:case</ta>
            <ta e="T477" id="Seg_7758" s="T476">v-v:tense-v:poss.pn</ta>
            <ta e="T478" id="Seg_7759" s="T477">interj</ta>
            <ta e="T479" id="Seg_7760" s="T478">dempro-dempro</ta>
            <ta e="T480" id="Seg_7761" s="T479">ptcl</ta>
            <ta e="T481" id="Seg_7762" s="T480">n-n:case</ta>
            <ta e="T482" id="Seg_7763" s="T481">que</ta>
            <ta e="T483" id="Seg_7764" s="T482">n-n:case</ta>
            <ta e="T484" id="Seg_7765" s="T483">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T485" id="Seg_7766" s="T484">ptcl</ta>
            <ta e="T486" id="Seg_7767" s="T485">interj</ta>
            <ta e="T487" id="Seg_7768" s="T486">ptcl</ta>
            <ta e="T488" id="Seg_7769" s="T487">v-v:tense-v:poss.pn</ta>
            <ta e="T489" id="Seg_7770" s="T488">pers-pro:case</ta>
            <ta e="T490" id="Seg_7771" s="T489">pers-pro:case</ta>
            <ta e="T491" id="Seg_7772" s="T490">ptcl</ta>
            <ta e="T492" id="Seg_7773" s="T491">adv</ta>
            <ta e="T493" id="Seg_7774" s="T492">v-v:tense-v:pred.pn</ta>
            <ta e="T494" id="Seg_7775" s="T493">conj</ta>
            <ta e="T495" id="Seg_7776" s="T494">n-n:case</ta>
            <ta e="T496" id="Seg_7777" s="T495">n-n:case</ta>
            <ta e="T497" id="Seg_7778" s="T496">v-v:cvb</ta>
            <ta e="T498" id="Seg_7779" s="T497">v-v:tense-v:pred.pn</ta>
            <ta e="T499" id="Seg_7780" s="T498">n-n:case</ta>
            <ta e="T500" id="Seg_7781" s="T499">n-n:case</ta>
            <ta e="T501" id="Seg_7782" s="T500">v-v:cvb</ta>
            <ta e="T502" id="Seg_7783" s="T501">v-v:cvb</ta>
            <ta e="T503" id="Seg_7784" s="T502">v-v:tense-v:poss.pn</ta>
            <ta e="T504" id="Seg_7785" s="T503">adj&gt;adj-adj</ta>
            <ta e="T505" id="Seg_7786" s="T504">n-n:case</ta>
            <ta e="T507" id="Seg_7787" s="T505">v-v:cvb</ta>
            <ta e="T508" id="Seg_7788" s="T507">dempro</ta>
            <ta e="T509" id="Seg_7789" s="T508">n-n:case</ta>
            <ta e="T510" id="Seg_7790" s="T509">que-pro:case</ta>
            <ta e="T511" id="Seg_7791" s="T510">ptcl</ta>
            <ta e="T512" id="Seg_7792" s="T511">adj</ta>
            <ta e="T513" id="Seg_7793" s="T512">n-n&gt;adj-n:case</ta>
            <ta e="T514" id="Seg_7794" s="T513">n-n:case</ta>
            <ta e="T515" id="Seg_7795" s="T514">n-n:poss-n:case</ta>
            <ta e="T516" id="Seg_7796" s="T515">v-v:tense-v:poss.pn</ta>
            <ta e="T517" id="Seg_7797" s="T516">n-n:(poss)-n:case</ta>
            <ta e="T518" id="Seg_7798" s="T517">adv</ta>
            <ta e="T519" id="Seg_7799" s="T518">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T520" id="Seg_7800" s="T519">dempro</ta>
            <ta e="T521" id="Seg_7801" s="T520">n-n:case</ta>
            <ta e="T522" id="Seg_7802" s="T521">ptcl</ta>
            <ta e="T523" id="Seg_7803" s="T522">v-v:cvb</ta>
            <ta e="T524" id="Seg_7804" s="T523">v-v:cvb</ta>
            <ta e="T525" id="Seg_7805" s="T524">adv</ta>
            <ta e="T526" id="Seg_7806" s="T525">ptcl</ta>
            <ta e="T527" id="Seg_7807" s="T526">adj</ta>
            <ta e="T528" id="Seg_7808" s="T527">n-n:case</ta>
            <ta e="T529" id="Seg_7809" s="T528">ptcl</ta>
            <ta e="T530" id="Seg_7810" s="T529">v-v:ptcp</ta>
            <ta e="T531" id="Seg_7811" s="T530">n-n:case</ta>
            <ta e="T532" id="Seg_7812" s="T531">v-v:cvb</ta>
            <ta e="T533" id="Seg_7813" s="T532">post</ta>
            <ta e="T534" id="Seg_7814" s="T533">v-v&gt;n-n:case</ta>
            <ta e="T535" id="Seg_7815" s="T534">v-v:cvb</ta>
            <ta e="T536" id="Seg_7816" s="T535">v-v:tense-v:pred.pn</ta>
            <ta e="T537" id="Seg_7817" s="T536">adv</ta>
            <ta e="T538" id="Seg_7818" s="T537">n-n:case</ta>
            <ta e="T539" id="Seg_7819" s="T538">adj-n:poss-n:case</ta>
            <ta e="T540" id="Seg_7820" s="T539">v-v&gt;v-v:cvb</ta>
            <ta e="T541" id="Seg_7821" s="T540">v-v:tense-v:pred.pn</ta>
            <ta e="T542" id="Seg_7822" s="T541">dempro</ta>
            <ta e="T543" id="Seg_7823" s="T542">v-v:cvb</ta>
            <ta e="T544" id="Seg_7824" s="T543">post</ta>
            <ta e="T545" id="Seg_7825" s="T544">interj</ta>
            <ta e="T546" id="Seg_7826" s="T545">n-n:(num)-n:case</ta>
            <ta e="T547" id="Seg_7827" s="T546">ptcl</ta>
            <ta e="T548" id="Seg_7828" s="T547">v-v:cvb</ta>
            <ta e="T549" id="Seg_7829" s="T548">v-v:cvb</ta>
            <ta e="T550" id="Seg_7830" s="T549">v-v:cvb</ta>
            <ta e="T551" id="Seg_7831" s="T550">v-v:tense-v:pred.pn</ta>
            <ta e="T552" id="Seg_7832" s="T551">n-n:case</ta>
            <ta e="T553" id="Seg_7833" s="T552">v-v:cvb</ta>
            <ta e="T554" id="Seg_7834" s="T553">post</ta>
            <ta e="T555" id="Seg_7835" s="T554">v-v:cvb</ta>
            <ta e="T556" id="Seg_7836" s="T555">v-v:tense-v:pred.pn</ta>
            <ta e="T557" id="Seg_7837" s="T556">interj</ta>
            <ta e="T558" id="Seg_7838" s="T557">dempro-pro:case</ta>
            <ta e="T559" id="Seg_7839" s="T558">post</ta>
            <ta e="T560" id="Seg_7840" s="T559">dempro-pro:case</ta>
            <ta e="T561" id="Seg_7841" s="T560">v-v:cvb</ta>
            <ta e="T562" id="Seg_7842" s="T561">ptcl</ta>
            <ta e="T563" id="Seg_7843" s="T562">n-n:case</ta>
            <ta e="T564" id="Seg_7844" s="T563">n-n:(num)-n:poss-n:case</ta>
            <ta e="T565" id="Seg_7845" s="T564">v-v:cvb</ta>
            <ta e="T566" id="Seg_7846" s="T565">v-v:(ins)-v:tense-v:poss.pn</ta>
            <ta e="T567" id="Seg_7847" s="T566">ptcl</ta>
            <ta e="T568" id="Seg_7848" s="T567">adv</ta>
            <ta e="T569" id="Seg_7849" s="T568">n-n:(poss)-n:case</ta>
            <ta e="T570" id="Seg_7850" s="T569">ptcl</ta>
            <ta e="T571" id="Seg_7851" s="T570">pers-pro:case</ta>
            <ta e="T572" id="Seg_7852" s="T571">n-n:case</ta>
            <ta e="T573" id="Seg_7853" s="T572">v-v:tense-v:pred.pn</ta>
            <ta e="T574" id="Seg_7854" s="T573">cardnum</ta>
            <ta e="T575" id="Seg_7855" s="T574">ptcl</ta>
            <ta e="T576" id="Seg_7856" s="T575">n-n:poss-n:case</ta>
            <ta e="T577" id="Seg_7857" s="T576">v-v:neg-v:pred.pn</ta>
            <ta e="T578" id="Seg_7858" s="T577">adv</ta>
            <ta e="T579" id="Seg_7859" s="T578">que</ta>
            <ta e="T580" id="Seg_7860" s="T579">v-v:cvb</ta>
            <ta e="T581" id="Seg_7861" s="T580">pers-pro:case</ta>
            <ta e="T582" id="Seg_7862" s="T581">ptcl</ta>
            <ta e="T583" id="Seg_7863" s="T582">v-v:tense-v:pred.pn</ta>
            <ta e="T584" id="Seg_7864" s="T583">ptcl</ta>
            <ta e="T585" id="Seg_7865" s="T584">que</ta>
            <ta e="T586" id="Seg_7866" s="T585">v-v:cvb</ta>
            <ta e="T587" id="Seg_7867" s="T586">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T588" id="Seg_7868" s="T587">ptcl</ta>
            <ta e="T589" id="Seg_7869" s="T588">v-v:cvb</ta>
            <ta e="T590" id="Seg_7870" s="T589">v-v:tense-v:pred.pn</ta>
            <ta e="T591" id="Seg_7871" s="T590">ptcl</ta>
            <ta e="T592" id="Seg_7872" s="T591">pers-pro:case</ta>
            <ta e="T593" id="Seg_7873" s="T592">ptcl</ta>
            <ta e="T594" id="Seg_7874" s="T593">v-v:tense-v:mood-v:pred.pn</ta>
            <ta e="T595" id="Seg_7875" s="T594">v-v:tense-v:pred.pn</ta>
            <ta e="T596" id="Seg_7876" s="T595">v-v:cvb</ta>
            <ta e="T597" id="Seg_7877" s="T596">interj</ta>
            <ta e="T598" id="Seg_7878" s="T597">v-v:tense-v:pred.pn</ta>
            <ta e="T599" id="Seg_7879" s="T598">interj-interj</ta>
            <ta e="T600" id="Seg_7880" s="T599">interj-interj</ta>
            <ta e="T601" id="Seg_7881" s="T600">v-v:tense-v:pred.pn</ta>
            <ta e="T602" id="Seg_7882" s="T601">ptcl</ta>
            <ta e="T603" id="Seg_7883" s="T602">que</ta>
            <ta e="T604" id="Seg_7884" s="T603">n-n:poss-n:case</ta>
            <ta e="T605" id="Seg_7885" s="T604">v-v:ptcp-v:(case)</ta>
            <ta e="T606" id="Seg_7886" s="T605">v-v:cvb</ta>
            <ta e="T607" id="Seg_7887" s="T606">n-n:case</ta>
            <ta e="T608" id="Seg_7888" s="T607">v-v&gt;v-v:cvb</ta>
            <ta e="T609" id="Seg_7889" s="T608">v-v:mood-v:temp.pn</ta>
            <ta e="T610" id="Seg_7890" s="T609">adv</ta>
            <ta e="T611" id="Seg_7891" s="T682">n-n:case</ta>
            <ta e="T612" id="Seg_7892" s="T611">ptcl</ta>
            <ta e="T613" id="Seg_7893" s="T612">adj</ta>
            <ta e="T614" id="Seg_7894" s="T613">n-n:poss-n:case</ta>
            <ta e="T615" id="Seg_7895" s="T614">adj</ta>
            <ta e="T616" id="Seg_7896" s="T615">n-n:poss-n:case</ta>
            <ta e="T617" id="Seg_7897" s="T616">v-v:cvb</ta>
            <ta e="T618" id="Seg_7898" s="T617">v-v:cvb</ta>
            <ta e="T619" id="Seg_7899" s="T618">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T621" id="Seg_7900" s="T619">ptcl</ta>
            <ta e="T622" id="Seg_7901" s="T621">dempro</ta>
            <ta e="T623" id="Seg_7902" s="T622">n-n:case</ta>
            <ta e="T624" id="Seg_7903" s="T623">ptcl</ta>
            <ta e="T625" id="Seg_7904" s="T624">que-pro:case</ta>
            <ta e="T626" id="Seg_7905" s="T625">ptcl</ta>
            <ta e="T627" id="Seg_7906" s="T626">n-n:case</ta>
            <ta e="T628" id="Seg_7907" s="T627">ptcl</ta>
            <ta e="T629" id="Seg_7908" s="T628">v-v:cvb</ta>
            <ta e="T630" id="Seg_7909" s="T629">v-v&gt;v-v:cvb</ta>
            <ta e="T631" id="Seg_7910" s="T630">n-n:poss-n:case</ta>
            <ta e="T632" id="Seg_7911" s="T631">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T633" id="Seg_7912" s="T632">adv</ta>
            <ta e="T634" id="Seg_7913" s="T633">v-v:cvb</ta>
            <ta e="T635" id="Seg_7914" s="T634">dempro-pro:case</ta>
            <ta e="T636" id="Seg_7915" s="T635">v-v&gt;v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T637" id="Seg_7916" s="T636">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T638" id="Seg_7917" s="T637">n-n:case</ta>
            <ta e="T639" id="Seg_7918" s="T638">n-n:poss-n:case</ta>
            <ta e="T640" id="Seg_7919" s="T639">v-v&gt;v-v:cvb</ta>
            <ta e="T641" id="Seg_7920" s="T640">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T642" id="Seg_7921" s="T641">n-n:case</ta>
            <ta e="T645" id="Seg_7922" s="T644">n-n:case</ta>
            <ta e="T646" id="Seg_7923" s="T645">n-n:poss-n:case</ta>
            <ta e="T647" id="Seg_7924" s="T646">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T648" id="Seg_7925" s="T647">dempro-pro:case</ta>
            <ta e="T649" id="Seg_7926" s="T648">v-v:cvb</ta>
            <ta e="T650" id="Seg_7927" s="T649">n-n&gt;adj</ta>
            <ta e="T651" id="Seg_7928" s="T650">n-n:case</ta>
            <ta e="T652" id="Seg_7929" s="T651">v-v:cvb</ta>
            <ta e="T653" id="Seg_7930" s="T652">v-v:cvb</ta>
            <ta e="T654" id="Seg_7931" s="T653">adv</ta>
            <ta e="T655" id="Seg_7932" s="T654">v-v&gt;v-v:cvb</ta>
            <ta e="T656" id="Seg_7933" s="T655">v-v:cvb</ta>
            <ta e="T657" id="Seg_7934" s="T656">v-v:tense-v:pred.pn</ta>
            <ta e="T658" id="Seg_7935" s="T657">ptcl</ta>
            <ta e="T659" id="Seg_7936" s="T658">dempro</ta>
            <ta e="T660" id="Seg_7937" s="T659">n-n:case</ta>
            <ta e="T661" id="Seg_7938" s="T660">ptcl</ta>
            <ta e="T662" id="Seg_7939" s="T661">n-n:case</ta>
            <ta e="T663" id="Seg_7940" s="T662">v-v:cvb</ta>
            <ta e="T664" id="Seg_7941" s="T663">v-v&gt;v-v:cvb</ta>
            <ta e="T665" id="Seg_7942" s="T664">ptcl</ta>
            <ta e="T666" id="Seg_7943" s="T665">ptcl</ta>
            <ta e="T667" id="Seg_7944" s="T666">ptcl</ta>
            <ta e="T668" id="Seg_7945" s="T667">dempro</ta>
            <ta e="T669" id="Seg_7946" s="T668">v-v&gt;v-v:cvb-v-v:cvb</ta>
            <ta e="T671" id="Seg_7947" s="T669">post</ta>
            <ta e="T672" id="Seg_7948" s="T671">v-v:cvb</ta>
            <ta e="T673" id="Seg_7949" s="T672">v-v:cvb-v-v:cvb</ta>
            <ta e="T674" id="Seg_7950" s="T673">v-v:tense-v:poss.pn</ta>
            <ta e="T675" id="Seg_7951" s="T674">ptcl</ta>
            <ta e="T676" id="Seg_7952" s="T675">que</ta>
            <ta e="T677" id="Seg_7953" s="T676">v-v:tense-v:poss.pn</ta>
            <ta e="T678" id="Seg_7954" s="T677">ptcl</ta>
            <ta e="T679" id="Seg_7955" s="T678">ptcl</ta>
            <ta e="T680" id="Seg_7956" s="T679">n-n:(poss)-n:case</ta>
            <ta e="T681" id="Seg_7957" s="T680">adj-n:(poss)-n:case</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_7958" s="T0">dempro</ta>
            <ta e="T2" id="Seg_7959" s="T1">adv</ta>
            <ta e="T3" id="Seg_7960" s="T2">adj</ta>
            <ta e="T4" id="Seg_7961" s="T3">cop</ta>
            <ta e="T5" id="Seg_7962" s="T4">n</ta>
            <ta e="T6" id="Seg_7963" s="T5">v</ta>
            <ta e="T7" id="Seg_7964" s="T6">v</ta>
            <ta e="T8" id="Seg_7965" s="T7">ptcl</ta>
            <ta e="T9" id="Seg_7966" s="T8">dempro</ta>
            <ta e="T10" id="Seg_7967" s="T9">dempro</ta>
            <ta e="T11" id="Seg_7968" s="T10">pers</ta>
            <ta e="T12" id="Seg_7969" s="T11">adj</ta>
            <ta e="T13" id="Seg_7970" s="T12">v</ta>
            <ta e="T14" id="Seg_7971" s="T13">que</ta>
            <ta e="T15" id="Seg_7972" s="T14">ptcl</ta>
            <ta e="T16" id="Seg_7973" s="T15">n</ta>
            <ta e="T17" id="Seg_7974" s="T16">ptcl</ta>
            <ta e="T18" id="Seg_7975" s="T17">cop</ta>
            <ta e="T19" id="Seg_7976" s="T18">dempro</ta>
            <ta e="T20" id="Seg_7977" s="T19">n</ta>
            <ta e="T21" id="Seg_7978" s="T20">v</ta>
            <ta e="T22" id="Seg_7979" s="T21">v</ta>
            <ta e="T23" id="Seg_7980" s="T22">dempro</ta>
            <ta e="T24" id="Seg_7981" s="T23">n</ta>
            <ta e="T25" id="Seg_7982" s="T24">n</ta>
            <ta e="T26" id="Seg_7983" s="T25">n</ta>
            <ta e="T27" id="Seg_7984" s="T26">n</ta>
            <ta e="T28" id="Seg_7985" s="T27">v</ta>
            <ta e="T29" id="Seg_7986" s="T28">n</ta>
            <ta e="T30" id="Seg_7987" s="T29">cop</ta>
            <ta e="T31" id="Seg_7988" s="T30">adv</ta>
            <ta e="T32" id="Seg_7989" s="T31">n</ta>
            <ta e="T33" id="Seg_7990" s="T32">n</ta>
            <ta e="T34" id="Seg_7991" s="T33">v</ta>
            <ta e="T35" id="Seg_7992" s="T34">adv</ta>
            <ta e="T36" id="Seg_7993" s="T35">n</ta>
            <ta e="T37" id="Seg_7994" s="T36">n</ta>
            <ta e="T38" id="Seg_7995" s="T37">v</ta>
            <ta e="T39" id="Seg_7996" s="T38">post</ta>
            <ta e="T40" id="Seg_7997" s="T39">adj</ta>
            <ta e="T41" id="Seg_7998" s="T40">adj</ta>
            <ta e="T42" id="Seg_7999" s="T41">cop</ta>
            <ta e="T43" id="Seg_8000" s="T42">ptcl</ta>
            <ta e="T44" id="Seg_8001" s="T43">n</ta>
            <ta e="T45" id="Seg_8002" s="T44">adj</ta>
            <ta e="T46" id="Seg_8003" s="T45">dempro</ta>
            <ta e="T47" id="Seg_8004" s="T46">n</ta>
            <ta e="T48" id="Seg_8005" s="T47">interj</ta>
            <ta e="T49" id="Seg_8006" s="T48">n</ta>
            <ta e="T50" id="Seg_8007" s="T49">dempro</ta>
            <ta e="T51" id="Seg_8008" s="T50">n</ta>
            <ta e="T52" id="Seg_8009" s="T51">v</ta>
            <ta e="T53" id="Seg_8010" s="T52">n</ta>
            <ta e="T54" id="Seg_8011" s="T53">dempro</ta>
            <ta e="T55" id="Seg_8012" s="T54">v</ta>
            <ta e="T56" id="Seg_8013" s="T55">adv</ta>
            <ta e="T57" id="Seg_8014" s="T56">ptcl</ta>
            <ta e="T58" id="Seg_8015" s="T57">n</ta>
            <ta e="T59" id="Seg_8016" s="T58">v</ta>
            <ta e="T60" id="Seg_8017" s="T59">ptcl</ta>
            <ta e="T61" id="Seg_8018" s="T60">adv</ta>
            <ta e="T62" id="Seg_8019" s="T61">n</ta>
            <ta e="T63" id="Seg_8020" s="T62">dempro</ta>
            <ta e="T64" id="Seg_8021" s="T63">n</ta>
            <ta e="T65" id="Seg_8022" s="T64">ptcl</ta>
            <ta e="T66" id="Seg_8023" s="T65">n</ta>
            <ta e="T67" id="Seg_8024" s="T66">n</ta>
            <ta e="T68" id="Seg_8025" s="T67">v</ta>
            <ta e="T69" id="Seg_8026" s="T68">aux</ta>
            <ta e="T70" id="Seg_8027" s="T69">adv</ta>
            <ta e="T71" id="Seg_8028" s="T70">que</ta>
            <ta e="T72" id="Seg_8029" s="T71">ptcl</ta>
            <ta e="T73" id="Seg_8030" s="T72">n</ta>
            <ta e="T74" id="Seg_8031" s="T73">v</ta>
            <ta e="T75" id="Seg_8032" s="T74">v</ta>
            <ta e="T76" id="Seg_8033" s="T75">n</ta>
            <ta e="T77" id="Seg_8034" s="T76">ptcl</ta>
            <ta e="T78" id="Seg_8035" s="T77">adj</ta>
            <ta e="T79" id="Seg_8036" s="T78">adj</ta>
            <ta e="T80" id="Seg_8037" s="T79">n</ta>
            <ta e="T81" id="Seg_8038" s="T80">v</ta>
            <ta e="T82" id="Seg_8039" s="T81">aux</ta>
            <ta e="T83" id="Seg_8040" s="T82">adj</ta>
            <ta e="T84" id="Seg_8041" s="T83">n</ta>
            <ta e="T85" id="Seg_8042" s="T84">n</ta>
            <ta e="T86" id="Seg_8043" s="T85">n</ta>
            <ta e="T87" id="Seg_8044" s="T86">cop</ta>
            <ta e="T88" id="Seg_8045" s="T87">adv</ta>
            <ta e="T89" id="Seg_8046" s="T88">v</ta>
            <ta e="T90" id="Seg_8047" s="T89">n</ta>
            <ta e="T91" id="Seg_8048" s="T90">ptcl</ta>
            <ta e="T92" id="Seg_8049" s="T91">cop</ta>
            <ta e="T93" id="Seg_8050" s="T92">v</ta>
            <ta e="T94" id="Seg_8051" s="T93">n</ta>
            <ta e="T95" id="Seg_8052" s="T94">n</ta>
            <ta e="T96" id="Seg_8053" s="T95">v</ta>
            <ta e="T97" id="Seg_8054" s="T96">aux</ta>
            <ta e="T98" id="Seg_8055" s="T97">n</ta>
            <ta e="T99" id="Seg_8056" s="T98">dempro</ta>
            <ta e="T100" id="Seg_8057" s="T99">v</ta>
            <ta e="T101" id="Seg_8058" s="T100">aux</ta>
            <ta e="T103" id="Seg_8059" s="T101">ptcl</ta>
            <ta e="T104" id="Seg_8060" s="T103">interj</ta>
            <ta e="T105" id="Seg_8061" s="T104">dempro</ta>
            <ta e="T106" id="Seg_8062" s="T105">que</ta>
            <ta e="T107" id="Seg_8063" s="T106">v</ta>
            <ta e="T108" id="Seg_8064" s="T107">v</ta>
            <ta e="T109" id="Seg_8065" s="T108">n</ta>
            <ta e="T110" id="Seg_8066" s="T109">n</ta>
            <ta e="T111" id="Seg_8067" s="T110">v</ta>
            <ta e="T112" id="Seg_8068" s="T111">n</ta>
            <ta e="T113" id="Seg_8069" s="T112">n</ta>
            <ta e="T114" id="Seg_8070" s="T113">v</ta>
            <ta e="T115" id="Seg_8071" s="T114">aux</ta>
            <ta e="T116" id="Seg_8072" s="T115">interj</ta>
            <ta e="T117" id="Seg_8073" s="T116">v</ta>
            <ta e="T118" id="Seg_8074" s="T117">v</ta>
            <ta e="T119" id="Seg_8075" s="T118">v</ta>
            <ta e="T120" id="Seg_8076" s="T119">adj</ta>
            <ta e="T121" id="Seg_8077" s="T120">n</ta>
            <ta e="T122" id="Seg_8078" s="T121">n</ta>
            <ta e="T123" id="Seg_8079" s="T122">n</ta>
            <ta e="T124" id="Seg_8080" s="T123">v</ta>
            <ta e="T125" id="Seg_8081" s="T124">post</ta>
            <ta e="T126" id="Seg_8082" s="T125">v</ta>
            <ta e="T127" id="Seg_8083" s="T126">interj</ta>
            <ta e="T128" id="Seg_8084" s="T127">ptcl</ta>
            <ta e="T129" id="Seg_8085" s="T128">adv</ta>
            <ta e="T130" id="Seg_8086" s="T129">pers</ta>
            <ta e="T131" id="Seg_8087" s="T130">que</ta>
            <ta e="T132" id="Seg_8088" s="T131">v</ta>
            <ta e="T133" id="Seg_8089" s="T132">v</ta>
            <ta e="T134" id="Seg_8090" s="T133">v</ta>
            <ta e="T135" id="Seg_8091" s="T134">dempro</ta>
            <ta e="T136" id="Seg_8092" s="T135">n</ta>
            <ta e="T137" id="Seg_8093" s="T136">que</ta>
            <ta e="T138" id="Seg_8094" s="T137">n</ta>
            <ta e="T139" id="Seg_8095" s="T138">v</ta>
            <ta e="T140" id="Seg_8096" s="T139">dempro</ta>
            <ta e="T141" id="Seg_8097" s="T140">ptcl</ta>
            <ta e="T142" id="Seg_8098" s="T141">n</ta>
            <ta e="T145" id="Seg_8099" s="T144">n</ta>
            <ta e="T146" id="Seg_8100" s="T145">n</ta>
            <ta e="T147" id="Seg_8101" s="T146">v</ta>
            <ta e="T148" id="Seg_8102" s="T147">v</ta>
            <ta e="T149" id="Seg_8103" s="T148">v</ta>
            <ta e="T150" id="Seg_8104" s="T149">ptcl</ta>
            <ta e="T151" id="Seg_8105" s="T150">que</ta>
            <ta e="T152" id="Seg_8106" s="T151">n</ta>
            <ta e="T153" id="Seg_8107" s="T152">v</ta>
            <ta e="T154" id="Seg_8108" s="T153">n</ta>
            <ta e="T155" id="Seg_8109" s="T154">n</ta>
            <ta e="T156" id="Seg_8110" s="T155">v</ta>
            <ta e="T157" id="Seg_8111" s="T156">adv</ta>
            <ta e="T158" id="Seg_8112" s="T157">v</ta>
            <ta e="T159" id="Seg_8113" s="T158">post</ta>
            <ta e="T160" id="Seg_8114" s="T159">n</ta>
            <ta e="T161" id="Seg_8115" s="T160">n</ta>
            <ta e="T162" id="Seg_8116" s="T161">v</ta>
            <ta e="T163" id="Seg_8117" s="T162">aux</ta>
            <ta e="T164" id="Seg_8118" s="T163">n</ta>
            <ta e="T165" id="Seg_8119" s="T164">n</ta>
            <ta e="T166" id="Seg_8120" s="T165">adj</ta>
            <ta e="T167" id="Seg_8121" s="T166">ptcl</ta>
            <ta e="T168" id="Seg_8122" s="T167">v</ta>
            <ta e="T169" id="Seg_8123" s="T168">cardnum</ta>
            <ta e="T170" id="Seg_8124" s="T169">n</ta>
            <ta e="T171" id="Seg_8125" s="T170">pers</ta>
            <ta e="T172" id="Seg_8126" s="T171">n</ta>
            <ta e="T173" id="Seg_8127" s="T172">adj</ta>
            <ta e="T174" id="Seg_8128" s="T173">n</ta>
            <ta e="T175" id="Seg_8129" s="T174">adj</ta>
            <ta e="T176" id="Seg_8130" s="T175">n</ta>
            <ta e="T177" id="Seg_8131" s="T176">ptcl</ta>
            <ta e="T178" id="Seg_8132" s="T177">v</ta>
            <ta e="T179" id="Seg_8133" s="T178">v</ta>
            <ta e="T180" id="Seg_8134" s="T179">v</ta>
            <ta e="T181" id="Seg_8135" s="T180">emphpro</ta>
            <ta e="T182" id="Seg_8136" s="T181">v</ta>
            <ta e="T183" id="Seg_8137" s="T182">n</ta>
            <ta e="T184" id="Seg_8138" s="T183">v</ta>
            <ta e="T185" id="Seg_8139" s="T184">ptcl</ta>
            <ta e="T186" id="Seg_8140" s="T185">v</ta>
            <ta e="T187" id="Seg_8141" s="T186">cardnum</ta>
            <ta e="T188" id="Seg_8142" s="T187">n</ta>
            <ta e="T189" id="Seg_8143" s="T188">n</ta>
            <ta e="T190" id="Seg_8144" s="T189">v</ta>
            <ta e="T191" id="Seg_8145" s="T190">cardnum</ta>
            <ta e="T192" id="Seg_8146" s="T191">n</ta>
            <ta e="T193" id="Seg_8147" s="T192">v</ta>
            <ta e="T194" id="Seg_8148" s="T193">adv</ta>
            <ta e="T195" id="Seg_8149" s="T194">v</ta>
            <ta e="T196" id="Seg_8150" s="T195">post</ta>
            <ta e="T197" id="Seg_8151" s="T196">n</ta>
            <ta e="T198" id="Seg_8152" s="T197">n</ta>
            <ta e="T199" id="Seg_8153" s="T198">v</ta>
            <ta e="T200" id="Seg_8154" s="T199">aux</ta>
            <ta e="T201" id="Seg_8155" s="T200">n</ta>
            <ta e="T202" id="Seg_8156" s="T201">v</ta>
            <ta e="T203" id="Seg_8157" s="T202">ptcl</ta>
            <ta e="T204" id="Seg_8158" s="T203">n</ta>
            <ta e="T205" id="Seg_8159" s="T204">v</ta>
            <ta e="T206" id="Seg_8160" s="T205">n</ta>
            <ta e="T207" id="Seg_8161" s="T206">v</ta>
            <ta e="T208" id="Seg_8162" s="T207">ptcl</ta>
            <ta e="T209" id="Seg_8163" s="T208">v</ta>
            <ta e="T210" id="Seg_8164" s="T209">v</ta>
            <ta e="T211" id="Seg_8165" s="T210">n</ta>
            <ta e="T212" id="Seg_8166" s="T211">n</ta>
            <ta e="T213" id="Seg_8167" s="T212">adj</ta>
            <ta e="T214" id="Seg_8168" s="T213">ptcl</ta>
            <ta e="T215" id="Seg_8169" s="T214">v</ta>
            <ta e="T216" id="Seg_8170" s="T215">cardnum</ta>
            <ta e="T217" id="Seg_8171" s="T216">n</ta>
            <ta e="T218" id="Seg_8172" s="T217">pers</ta>
            <ta e="T219" id="Seg_8173" s="T218">n</ta>
            <ta e="T220" id="Seg_8174" s="T219">adj</ta>
            <ta e="T221" id="Seg_8175" s="T220">n</ta>
            <ta e="T222" id="Seg_8176" s="T221">adj</ta>
            <ta e="T223" id="Seg_8177" s="T222">n</ta>
            <ta e="T224" id="Seg_8178" s="T223">v</ta>
            <ta e="T225" id="Seg_8179" s="T224">v</ta>
            <ta e="T226" id="Seg_8180" s="T225">v</ta>
            <ta e="T227" id="Seg_8181" s="T226">adv</ta>
            <ta e="T228" id="Seg_8182" s="T227">emphpro</ta>
            <ta e="T229" id="Seg_8183" s="T228">v</ta>
            <ta e="T230" id="Seg_8184" s="T229">ptcl</ta>
            <ta e="T231" id="Seg_8185" s="T230">cardnum</ta>
            <ta e="T232" id="Seg_8186" s="T231">n</ta>
            <ta e="T233" id="Seg_8187" s="T232">v</ta>
            <ta e="T234" id="Seg_8188" s="T233">aux</ta>
            <ta e="T235" id="Seg_8189" s="T234">cardnum</ta>
            <ta e="T236" id="Seg_8190" s="T235">n</ta>
            <ta e="T237" id="Seg_8191" s="T236">v</ta>
            <ta e="T238" id="Seg_8192" s="T237">n</ta>
            <ta e="T239" id="Seg_8193" s="T238">n</ta>
            <ta e="T240" id="Seg_8194" s="T239">v</ta>
            <ta e="T241" id="Seg_8195" s="T240">v</ta>
            <ta e="T242" id="Seg_8196" s="T241">ptcl</ta>
            <ta e="T243" id="Seg_8197" s="T242">ptcl</ta>
            <ta e="T244" id="Seg_8198" s="T243">dempro</ta>
            <ta e="T245" id="Seg_8199" s="T244">adv</ta>
            <ta e="T246" id="Seg_8200" s="T245">ptcl</ta>
            <ta e="T247" id="Seg_8201" s="T246">emphpro</ta>
            <ta e="T248" id="Seg_8202" s="T247">v</ta>
            <ta e="T249" id="Seg_8203" s="T248">v</ta>
            <ta e="T250" id="Seg_8204" s="T249">v</ta>
            <ta e="T251" id="Seg_8205" s="T250">aux</ta>
            <ta e="T252" id="Seg_8206" s="T251">interj</ta>
            <ta e="T253" id="Seg_8207" s="T252">dempro</ta>
            <ta e="T254" id="Seg_8208" s="T253">v</ta>
            <ta e="T255" id="Seg_8209" s="T254">n</ta>
            <ta e="T256" id="Seg_8210" s="T255">v</ta>
            <ta e="T257" id="Seg_8211" s="T256">aux</ta>
            <ta e="T258" id="Seg_8212" s="T257">interj</ta>
            <ta e="T259" id="Seg_8213" s="T258">n</ta>
            <ta e="T260" id="Seg_8214" s="T259">que</ta>
            <ta e="T261" id="Seg_8215" s="T260">v</ta>
            <ta e="T262" id="Seg_8216" s="T261">ptcl</ta>
            <ta e="T263" id="Seg_8217" s="T262">pers</ta>
            <ta e="T264" id="Seg_8218" s="T263">dempro</ta>
            <ta e="T265" id="Seg_8219" s="T264">dempro</ta>
            <ta e="T266" id="Seg_8220" s="T265">adj</ta>
            <ta e="T267" id="Seg_8221" s="T266">adj</ta>
            <ta e="T268" id="Seg_8222" s="T267">n</ta>
            <ta e="T269" id="Seg_8223" s="T268">n</ta>
            <ta e="T270" id="Seg_8224" s="T269">v</ta>
            <ta e="T271" id="Seg_8225" s="T270">n</ta>
            <ta e="T272" id="Seg_8226" s="T271">conj</ta>
            <ta e="T273" id="Seg_8227" s="T272">n</ta>
            <ta e="T274" id="Seg_8228" s="T273">v</ta>
            <ta e="T275" id="Seg_8229" s="T274">emphpro</ta>
            <ta e="T276" id="Seg_8230" s="T275">conj</ta>
            <ta e="T277" id="Seg_8231" s="T276">n</ta>
            <ta e="T278" id="Seg_8232" s="T277">ptcl</ta>
            <ta e="T279" id="Seg_8233" s="T278">v</ta>
            <ta e="T280" id="Seg_8234" s="T279">aux</ta>
            <ta e="T281" id="Seg_8235" s="T280">post</ta>
            <ta e="T282" id="Seg_8236" s="T281">ptcl</ta>
            <ta e="T283" id="Seg_8237" s="T282">que</ta>
            <ta e="T284" id="Seg_8238" s="T283">v</ta>
            <ta e="T285" id="Seg_8239" s="T284">ptcl</ta>
            <ta e="T286" id="Seg_8240" s="T285">dempro</ta>
            <ta e="T287" id="Seg_8241" s="T286">n</ta>
            <ta e="T288" id="Seg_8242" s="T287">v</ta>
            <ta e="T289" id="Seg_8243" s="T288">v</ta>
            <ta e="T290" id="Seg_8244" s="T289">pers</ta>
            <ta e="T291" id="Seg_8245" s="T290">que</ta>
            <ta e="T292" id="Seg_8246" s="T291">v</ta>
            <ta e="T293" id="Seg_8247" s="T292">v</ta>
            <ta e="T294" id="Seg_8248" s="T293">ptcl</ta>
            <ta e="T295" id="Seg_8249" s="T294">pers</ta>
            <ta e="T296" id="Seg_8250" s="T295">ptcl</ta>
            <ta e="T297" id="Seg_8251" s="T296">n</ta>
            <ta e="T298" id="Seg_8252" s="T297">ptcl</ta>
            <ta e="T299" id="Seg_8253" s="T298">cardnum</ta>
            <ta e="T300" id="Seg_8254" s="T299">n</ta>
            <ta e="T301" id="Seg_8255" s="T300">cardnum</ta>
            <ta e="T302" id="Seg_8256" s="T301">n</ta>
            <ta e="T303" id="Seg_8257" s="T302">v</ta>
            <ta e="T304" id="Seg_8258" s="T303">adv</ta>
            <ta e="T305" id="Seg_8259" s="T304">adv</ta>
            <ta e="T306" id="Seg_8260" s="T305">ptcl</ta>
            <ta e="T307" id="Seg_8261" s="T306">v</ta>
            <ta e="T308" id="Seg_8262" s="T307">v</ta>
            <ta e="T309" id="Seg_8263" s="T308">n</ta>
            <ta e="T310" id="Seg_8264" s="T309">ptcl</ta>
            <ta e="T311" id="Seg_8265" s="T310">v</ta>
            <ta e="T312" id="Seg_8266" s="T311">v</ta>
            <ta e="T313" id="Seg_8267" s="T312">v</ta>
            <ta e="T314" id="Seg_8268" s="T313">emphpro</ta>
            <ta e="T315" id="Seg_8269" s="T314">v</ta>
            <ta e="T316" id="Seg_8270" s="T315">v</ta>
            <ta e="T317" id="Seg_8271" s="T316">v</ta>
            <ta e="T318" id="Seg_8272" s="T317">que</ta>
            <ta e="T319" id="Seg_8273" s="T318">dempro</ta>
            <ta e="T320" id="Seg_8274" s="T319">n</ta>
            <ta e="T321" id="Seg_8275" s="T320">interj</ta>
            <ta e="T322" id="Seg_8276" s="T321">n</ta>
            <ta e="T323" id="Seg_8277" s="T322">adj</ta>
            <ta e="T324" id="Seg_8278" s="T323">n</ta>
            <ta e="T325" id="Seg_8279" s="T324">adj</ta>
            <ta e="T326" id="Seg_8280" s="T325">ptcl</ta>
            <ta e="T327" id="Seg_8281" s="T326">que</ta>
            <ta e="T328" id="Seg_8282" s="T327">n</ta>
            <ta e="T329" id="Seg_8283" s="T328">adj</ta>
            <ta e="T330" id="Seg_8284" s="T329">n</ta>
            <ta e="T331" id="Seg_8285" s="T330">v</ta>
            <ta e="T332" id="Seg_8286" s="T331">pers</ta>
            <ta e="T333" id="Seg_8287" s="T332">dempro</ta>
            <ta e="T334" id="Seg_8288" s="T333">ptcl</ta>
            <ta e="T335" id="Seg_8289" s="T334">n</ta>
            <ta e="T336" id="Seg_8290" s="T335">que</ta>
            <ta e="T337" id="Seg_8291" s="T336">v</ta>
            <ta e="T338" id="Seg_8292" s="T337">n</ta>
            <ta e="T339" id="Seg_8293" s="T338">que</ta>
            <ta e="T340" id="Seg_8294" s="T339">v</ta>
            <ta e="T341" id="Seg_8295" s="T340">pers</ta>
            <ta e="T342" id="Seg_8296" s="T341">n</ta>
            <ta e="T343" id="Seg_8297" s="T342">v</ta>
            <ta e="T344" id="Seg_8298" s="T343">v</ta>
            <ta e="T345" id="Seg_8299" s="T344">n</ta>
            <ta e="T346" id="Seg_8300" s="T345">n</ta>
            <ta e="T347" id="Seg_8301" s="T346">n</ta>
            <ta e="T348" id="Seg_8302" s="T347">n</ta>
            <ta e="T349" id="Seg_8303" s="T348">que</ta>
            <ta e="T350" id="Seg_8304" s="T349">v</ta>
            <ta e="T351" id="Seg_8305" s="T350">v</ta>
            <ta e="T352" id="Seg_8306" s="T351">adv</ta>
            <ta e="T353" id="Seg_8307" s="T352">v</ta>
            <ta e="T354" id="Seg_8308" s="T353">que</ta>
            <ta e="T355" id="Seg_8309" s="T354">v</ta>
            <ta e="T356" id="Seg_8310" s="T355">v</ta>
            <ta e="T357" id="Seg_8311" s="T356">n</ta>
            <ta e="T358" id="Seg_8312" s="T357">v</ta>
            <ta e="T359" id="Seg_8313" s="T358">v</ta>
            <ta e="T360" id="Seg_8314" s="T359">ptcl</ta>
            <ta e="T361" id="Seg_8315" s="T360">n</ta>
            <ta e="T364" id="Seg_8316" s="T363">n</ta>
            <ta e="T365" id="Seg_8317" s="T364">ptcl</ta>
            <ta e="T366" id="Seg_8318" s="T365">adv</ta>
            <ta e="T367" id="Seg_8319" s="T366">ptcl</ta>
            <ta e="T368" id="Seg_8320" s="T367">cop</ta>
            <ta e="T369" id="Seg_8321" s="T368">v</ta>
            <ta e="T370" id="Seg_8322" s="T369">n</ta>
            <ta e="T371" id="Seg_8323" s="T370">v</ta>
            <ta e="T372" id="Seg_8324" s="T371">aux</ta>
            <ta e="T373" id="Seg_8325" s="T372">interj</ta>
            <ta e="T374" id="Seg_8326" s="T373">ptcl</ta>
            <ta e="T375" id="Seg_8327" s="T374">n</ta>
            <ta e="T376" id="Seg_8328" s="T375">v</ta>
            <ta e="T377" id="Seg_8329" s="T376">v</ta>
            <ta e="T378" id="Seg_8330" s="T377">aux</ta>
            <ta e="T379" id="Seg_8331" s="T378">v</ta>
            <ta e="T380" id="Seg_8332" s="T379">v</ta>
            <ta e="T381" id="Seg_8333" s="T380">aux</ta>
            <ta e="T382" id="Seg_8334" s="T381">adj</ta>
            <ta e="T383" id="Seg_8335" s="T382">n</ta>
            <ta e="T384" id="Seg_8336" s="T383">v</ta>
            <ta e="T385" id="Seg_8337" s="T384">v</ta>
            <ta e="T386" id="Seg_8338" s="T385">adj</ta>
            <ta e="T387" id="Seg_8339" s="T386">adj</ta>
            <ta e="T388" id="Seg_8340" s="T387">n</ta>
            <ta e="T389" id="Seg_8341" s="T388">v</ta>
            <ta e="T390" id="Seg_8342" s="T389">v</ta>
            <ta e="T391" id="Seg_8343" s="T390">v</ta>
            <ta e="T392" id="Seg_8344" s="T391">ptcl</ta>
            <ta e="T393" id="Seg_8345" s="T392">n</ta>
            <ta e="T394" id="Seg_8346" s="T393">n</ta>
            <ta e="T395" id="Seg_8347" s="T394">v</ta>
            <ta e="T396" id="Seg_8348" s="T395">n</ta>
            <ta e="T397" id="Seg_8349" s="T396">n</ta>
            <ta e="T398" id="Seg_8350" s="T397">dempro</ta>
            <ta e="T399" id="Seg_8351" s="T398">adj</ta>
            <ta e="T400" id="Seg_8352" s="T399">interj</ta>
            <ta e="T401" id="Seg_8353" s="T400">pers</ta>
            <ta e="T402" id="Seg_8354" s="T401">ptcl</ta>
            <ta e="T403" id="Seg_8355" s="T402">ptcl</ta>
            <ta e="T404" id="Seg_8356" s="T403">adv</ta>
            <ta e="T405" id="Seg_8357" s="T404">v</ta>
            <ta e="T406" id="Seg_8358" s="T405">v</ta>
            <ta e="T407" id="Seg_8359" s="T406">v</ta>
            <ta e="T408" id="Seg_8360" s="T407">cardnum</ta>
            <ta e="T409" id="Seg_8361" s="T408">n</ta>
            <ta e="T410" id="Seg_8362" s="T409">pers</ta>
            <ta e="T411" id="Seg_8363" s="T410">n</ta>
            <ta e="T412" id="Seg_8364" s="T411">adj</ta>
            <ta e="T413" id="Seg_8365" s="T412">n</ta>
            <ta e="T414" id="Seg_8366" s="T413">adj</ta>
            <ta e="T415" id="Seg_8367" s="T414">adv</ta>
            <ta e="T416" id="Seg_8368" s="T415">emphpro</ta>
            <ta e="T419" id="Seg_8369" s="T418">ptcl</ta>
            <ta e="T420" id="Seg_8370" s="T419">v</ta>
            <ta e="T421" id="Seg_8371" s="T420">emphpro</ta>
            <ta e="T422" id="Seg_8372" s="T421">v</ta>
            <ta e="T423" id="Seg_8373" s="T422">ptcl</ta>
            <ta e="T424" id="Seg_8374" s="T423">ptcl</ta>
            <ta e="T425" id="Seg_8375" s="T424">v</ta>
            <ta e="T426" id="Seg_8376" s="T425">adv</ta>
            <ta e="T430" id="Seg_8377" s="T429">adj</ta>
            <ta e="T431" id="Seg_8378" s="T430">cardnum</ta>
            <ta e="T432" id="Seg_8379" s="T431">n</ta>
            <ta e="T433" id="Seg_8380" s="T432">v</ta>
            <ta e="T434" id="Seg_8381" s="T433">pers</ta>
            <ta e="T435" id="Seg_8382" s="T434">pers</ta>
            <ta e="T436" id="Seg_8383" s="T435">emphpro</ta>
            <ta e="T437" id="Seg_8384" s="T436">n</ta>
            <ta e="T438" id="Seg_8385" s="T437">pers</ta>
            <ta e="T439" id="Seg_8386" s="T438">ptcl</ta>
            <ta e="T440" id="Seg_8387" s="T439">n</ta>
            <ta e="T441" id="Seg_8388" s="T440">n</ta>
            <ta e="T442" id="Seg_8389" s="T441">ptcl</ta>
            <ta e="T443" id="Seg_8390" s="T442">n</ta>
            <ta e="T444" id="Seg_8391" s="T443">n</ta>
            <ta e="T445" id="Seg_8392" s="T444">que</ta>
            <ta e="T446" id="Seg_8393" s="T445">v</ta>
            <ta e="T447" id="Seg_8394" s="T446">n</ta>
            <ta e="T448" id="Seg_8395" s="T447">v</ta>
            <ta e="T449" id="Seg_8396" s="T448">ptcl</ta>
            <ta e="T450" id="Seg_8397" s="T449">v</ta>
            <ta e="T451" id="Seg_8398" s="T450">ptcl</ta>
            <ta e="T452" id="Seg_8399" s="T451">interj</ta>
            <ta e="T453" id="Seg_8400" s="T452">que</ta>
            <ta e="T454" id="Seg_8401" s="T453">n</ta>
            <ta e="T455" id="Seg_8402" s="T454">n</ta>
            <ta e="T456" id="Seg_8403" s="T455">v</ta>
            <ta e="T457" id="Seg_8404" s="T456">pers</ta>
            <ta e="T458" id="Seg_8405" s="T457">que</ta>
            <ta e="T459" id="Seg_8406" s="T458">v</ta>
            <ta e="T460" id="Seg_8407" s="T459">n</ta>
            <ta e="T461" id="Seg_8408" s="T460">v</ta>
            <ta e="T462" id="Seg_8409" s="T461">v</ta>
            <ta e="T463" id="Seg_8410" s="T462">posspr</ta>
            <ta e="T464" id="Seg_8411" s="T463">n</ta>
            <ta e="T465" id="Seg_8412" s="T464">n</ta>
            <ta e="T466" id="Seg_8413" s="T465">que</ta>
            <ta e="T467" id="Seg_8414" s="T466">v</ta>
            <ta e="T468" id="Seg_8415" s="T467">interj</ta>
            <ta e="T469" id="Seg_8416" s="T468">pers</ta>
            <ta e="T470" id="Seg_8417" s="T469">n</ta>
            <ta e="T471" id="Seg_8418" s="T470">v</ta>
            <ta e="T472" id="Seg_8419" s="T471">que</ta>
            <ta e="T473" id="Seg_8420" s="T472">v</ta>
            <ta e="T474" id="Seg_8421" s="T473">pers</ta>
            <ta e="T475" id="Seg_8422" s="T474">n</ta>
            <ta e="T476" id="Seg_8423" s="T475">n</ta>
            <ta e="T477" id="Seg_8424" s="T476">v</ta>
            <ta e="T478" id="Seg_8425" s="T477">interj</ta>
            <ta e="T479" id="Seg_8426" s="T478">dempro</ta>
            <ta e="T480" id="Seg_8427" s="T479">ptcl</ta>
            <ta e="T481" id="Seg_8428" s="T480">n</ta>
            <ta e="T482" id="Seg_8429" s="T481">que</ta>
            <ta e="T483" id="Seg_8430" s="T482">n</ta>
            <ta e="T484" id="Seg_8431" s="T483">v</ta>
            <ta e="T485" id="Seg_8432" s="T484">ptcl</ta>
            <ta e="T486" id="Seg_8433" s="T485">interj</ta>
            <ta e="T487" id="Seg_8434" s="T486">ptcl</ta>
            <ta e="T488" id="Seg_8435" s="T487">v</ta>
            <ta e="T489" id="Seg_8436" s="T488">pers</ta>
            <ta e="T490" id="Seg_8437" s="T489">pers</ta>
            <ta e="T491" id="Seg_8438" s="T490">ptcl</ta>
            <ta e="T492" id="Seg_8439" s="T491">adv</ta>
            <ta e="T493" id="Seg_8440" s="T492">v</ta>
            <ta e="T494" id="Seg_8441" s="T493">conj</ta>
            <ta e="T495" id="Seg_8442" s="T494">n</ta>
            <ta e="T496" id="Seg_8443" s="T495">n</ta>
            <ta e="T497" id="Seg_8444" s="T496">v</ta>
            <ta e="T498" id="Seg_8445" s="T497">aux</ta>
            <ta e="T499" id="Seg_8446" s="T498">n</ta>
            <ta e="T500" id="Seg_8447" s="T499">n</ta>
            <ta e="T501" id="Seg_8448" s="T500">v</ta>
            <ta e="T502" id="Seg_8449" s="T501">aux</ta>
            <ta e="T503" id="Seg_8450" s="T502">aux</ta>
            <ta e="T504" id="Seg_8451" s="T503">adj</ta>
            <ta e="T505" id="Seg_8452" s="T504">n</ta>
            <ta e="T507" id="Seg_8453" s="T505">v</ta>
            <ta e="T508" id="Seg_8454" s="T507">dempro</ta>
            <ta e="T509" id="Seg_8455" s="T508">n</ta>
            <ta e="T510" id="Seg_8456" s="T509">que</ta>
            <ta e="T511" id="Seg_8457" s="T510">ptcl</ta>
            <ta e="T512" id="Seg_8458" s="T511">adj</ta>
            <ta e="T513" id="Seg_8459" s="T512">adj</ta>
            <ta e="T514" id="Seg_8460" s="T513">n</ta>
            <ta e="T515" id="Seg_8461" s="T514">n</ta>
            <ta e="T516" id="Seg_8462" s="T515">cop</ta>
            <ta e="T517" id="Seg_8463" s="T516">n</ta>
            <ta e="T518" id="Seg_8464" s="T517">adv</ta>
            <ta e="T519" id="Seg_8465" s="T518">v</ta>
            <ta e="T520" id="Seg_8466" s="T519">dempro</ta>
            <ta e="T521" id="Seg_8467" s="T520">n</ta>
            <ta e="T522" id="Seg_8468" s="T521">ptcl</ta>
            <ta e="T523" id="Seg_8469" s="T522">v</ta>
            <ta e="T524" id="Seg_8470" s="T523">aux</ta>
            <ta e="T525" id="Seg_8471" s="T524">adv</ta>
            <ta e="T526" id="Seg_8472" s="T525">ptcl</ta>
            <ta e="T527" id="Seg_8473" s="T526">adj</ta>
            <ta e="T528" id="Seg_8474" s="T527">n</ta>
            <ta e="T529" id="Seg_8475" s="T528">ptcl</ta>
            <ta e="T530" id="Seg_8476" s="T529">v</ta>
            <ta e="T531" id="Seg_8477" s="T530">n</ta>
            <ta e="T532" id="Seg_8478" s="T531">cop</ta>
            <ta e="T533" id="Seg_8479" s="T532">post</ta>
            <ta e="T534" id="Seg_8480" s="T533">n</ta>
            <ta e="T535" id="Seg_8481" s="T534">v</ta>
            <ta e="T536" id="Seg_8482" s="T535">aux</ta>
            <ta e="T537" id="Seg_8483" s="T536">adv</ta>
            <ta e="T538" id="Seg_8484" s="T537">n</ta>
            <ta e="T539" id="Seg_8485" s="T538">adj</ta>
            <ta e="T540" id="Seg_8486" s="T539">v</ta>
            <ta e="T541" id="Seg_8487" s="T540">v</ta>
            <ta e="T542" id="Seg_8488" s="T541">dempro</ta>
            <ta e="T543" id="Seg_8489" s="T542">v</ta>
            <ta e="T544" id="Seg_8490" s="T543">post</ta>
            <ta e="T545" id="Seg_8491" s="T544">interj</ta>
            <ta e="T546" id="Seg_8492" s="T545">n</ta>
            <ta e="T547" id="Seg_8493" s="T546">ptcl</ta>
            <ta e="T548" id="Seg_8494" s="T547">v</ta>
            <ta e="T549" id="Seg_8495" s="T548">aux</ta>
            <ta e="T550" id="Seg_8496" s="T549">aux</ta>
            <ta e="T551" id="Seg_8497" s="T550">v</ta>
            <ta e="T552" id="Seg_8498" s="T551">n</ta>
            <ta e="T553" id="Seg_8499" s="T552">v</ta>
            <ta e="T554" id="Seg_8500" s="T553">post</ta>
            <ta e="T555" id="Seg_8501" s="T554">v</ta>
            <ta e="T556" id="Seg_8502" s="T555">v</ta>
            <ta e="T557" id="Seg_8503" s="T556">interj</ta>
            <ta e="T558" id="Seg_8504" s="T557">dempro</ta>
            <ta e="T559" id="Seg_8505" s="T558">post</ta>
            <ta e="T560" id="Seg_8506" s="T559">dempro</ta>
            <ta e="T561" id="Seg_8507" s="T560">v</ta>
            <ta e="T562" id="Seg_8508" s="T561">ptcl</ta>
            <ta e="T563" id="Seg_8509" s="T562">n</ta>
            <ta e="T564" id="Seg_8510" s="T563">n</ta>
            <ta e="T565" id="Seg_8511" s="T564">v</ta>
            <ta e="T566" id="Seg_8512" s="T565">aux</ta>
            <ta e="T567" id="Seg_8513" s="T566">ptcl</ta>
            <ta e="T568" id="Seg_8514" s="T567">adv</ta>
            <ta e="T569" id="Seg_8515" s="T568">n</ta>
            <ta e="T570" id="Seg_8516" s="T569">ptcl</ta>
            <ta e="T571" id="Seg_8517" s="T570">pers</ta>
            <ta e="T572" id="Seg_8518" s="T571">n</ta>
            <ta e="T573" id="Seg_8519" s="T572">v</ta>
            <ta e="T574" id="Seg_8520" s="T573">cardnum</ta>
            <ta e="T575" id="Seg_8521" s="T574">ptcl</ta>
            <ta e="T576" id="Seg_8522" s="T575">n</ta>
            <ta e="T577" id="Seg_8523" s="T576">v</ta>
            <ta e="T578" id="Seg_8524" s="T577">adv</ta>
            <ta e="T579" id="Seg_8525" s="T578">que</ta>
            <ta e="T580" id="Seg_8526" s="T579">v</ta>
            <ta e="T581" id="Seg_8527" s="T580">pers</ta>
            <ta e="T582" id="Seg_8528" s="T581">ptcl</ta>
            <ta e="T583" id="Seg_8529" s="T582">v</ta>
            <ta e="T584" id="Seg_8530" s="T583">ptcl</ta>
            <ta e="T585" id="Seg_8531" s="T584">que</ta>
            <ta e="T586" id="Seg_8532" s="T585">v</ta>
            <ta e="T587" id="Seg_8533" s="T586">v</ta>
            <ta e="T588" id="Seg_8534" s="T587">ptcl</ta>
            <ta e="T589" id="Seg_8535" s="T588">v</ta>
            <ta e="T590" id="Seg_8536" s="T589">v</ta>
            <ta e="T591" id="Seg_8537" s="T590">ptcl</ta>
            <ta e="T592" id="Seg_8538" s="T591">pers</ta>
            <ta e="T593" id="Seg_8539" s="T592">ptcl</ta>
            <ta e="T594" id="Seg_8540" s="T593">v</ta>
            <ta e="T595" id="Seg_8541" s="T594">aux</ta>
            <ta e="T596" id="Seg_8542" s="T595">v</ta>
            <ta e="T597" id="Seg_8543" s="T596">interj</ta>
            <ta e="T598" id="Seg_8544" s="T597">v</ta>
            <ta e="T599" id="Seg_8545" s="T598">interj</ta>
            <ta e="T600" id="Seg_8546" s="T599">interj</ta>
            <ta e="T601" id="Seg_8547" s="T600">v</ta>
            <ta e="T602" id="Seg_8548" s="T601">ptcl</ta>
            <ta e="T603" id="Seg_8549" s="T602">que</ta>
            <ta e="T604" id="Seg_8550" s="T603">n</ta>
            <ta e="T605" id="Seg_8551" s="T604">v</ta>
            <ta e="T606" id="Seg_8552" s="T605">v</ta>
            <ta e="T607" id="Seg_8553" s="T606">n</ta>
            <ta e="T608" id="Seg_8554" s="T607">v</ta>
            <ta e="T609" id="Seg_8555" s="T608">aux</ta>
            <ta e="T610" id="Seg_8556" s="T609">adv</ta>
            <ta e="T611" id="Seg_8557" s="T682">n</ta>
            <ta e="T612" id="Seg_8558" s="T611">ptcl</ta>
            <ta e="T613" id="Seg_8559" s="T612">adj</ta>
            <ta e="T614" id="Seg_8560" s="T613">n</ta>
            <ta e="T615" id="Seg_8561" s="T614">adj</ta>
            <ta e="T616" id="Seg_8562" s="T615">n</ta>
            <ta e="T617" id="Seg_8563" s="T616">v</ta>
            <ta e="T618" id="Seg_8564" s="T617">v</ta>
            <ta e="T619" id="Seg_8565" s="T618">v</ta>
            <ta e="T621" id="Seg_8566" s="T619">ptcl</ta>
            <ta e="T622" id="Seg_8567" s="T621">dempro</ta>
            <ta e="T623" id="Seg_8568" s="T622">n</ta>
            <ta e="T624" id="Seg_8569" s="T623">ptcl</ta>
            <ta e="T625" id="Seg_8570" s="T624">que</ta>
            <ta e="T626" id="Seg_8571" s="T625">ptcl</ta>
            <ta e="T627" id="Seg_8572" s="T626">n</ta>
            <ta e="T628" id="Seg_8573" s="T627">ptcl</ta>
            <ta e="T629" id="Seg_8574" s="T628">v</ta>
            <ta e="T630" id="Seg_8575" s="T629">v</ta>
            <ta e="T631" id="Seg_8576" s="T630">n</ta>
            <ta e="T632" id="Seg_8577" s="T631">v</ta>
            <ta e="T633" id="Seg_8578" s="T632">adv</ta>
            <ta e="T634" id="Seg_8579" s="T633">v</ta>
            <ta e="T635" id="Seg_8580" s="T634">dempro</ta>
            <ta e="T636" id="Seg_8581" s="T635">v</ta>
            <ta e="T637" id="Seg_8582" s="T636">v</ta>
            <ta e="T638" id="Seg_8583" s="T637">n</ta>
            <ta e="T639" id="Seg_8584" s="T638">n</ta>
            <ta e="T640" id="Seg_8585" s="T639">v</ta>
            <ta e="T641" id="Seg_8586" s="T640">aux</ta>
            <ta e="T642" id="Seg_8587" s="T641">n</ta>
            <ta e="T645" id="Seg_8588" s="T644">n</ta>
            <ta e="T646" id="Seg_8589" s="T645">n</ta>
            <ta e="T647" id="Seg_8590" s="T646">v</ta>
            <ta e="T648" id="Seg_8591" s="T647">dempro</ta>
            <ta e="T649" id="Seg_8592" s="T648">v</ta>
            <ta e="T650" id="Seg_8593" s="T649">adj</ta>
            <ta e="T651" id="Seg_8594" s="T650">n</ta>
            <ta e="T652" id="Seg_8595" s="T651">v</ta>
            <ta e="T653" id="Seg_8596" s="T652">v</ta>
            <ta e="T654" id="Seg_8597" s="T653">adv</ta>
            <ta e="T655" id="Seg_8598" s="T654">v</ta>
            <ta e="T656" id="Seg_8599" s="T655">v</ta>
            <ta e="T657" id="Seg_8600" s="T656">aux</ta>
            <ta e="T658" id="Seg_8601" s="T657">ptcl</ta>
            <ta e="T659" id="Seg_8602" s="T658">dempro</ta>
            <ta e="T660" id="Seg_8603" s="T659">n</ta>
            <ta e="T661" id="Seg_8604" s="T660">ptcl</ta>
            <ta e="T662" id="Seg_8605" s="T661">n</ta>
            <ta e="T663" id="Seg_8606" s="T662">v</ta>
            <ta e="T664" id="Seg_8607" s="T663">v</ta>
            <ta e="T665" id="Seg_8608" s="T664">ptcl</ta>
            <ta e="T666" id="Seg_8609" s="T665">ptcl</ta>
            <ta e="T667" id="Seg_8610" s="T666">ptcl</ta>
            <ta e="T668" id="Seg_8611" s="T667">dempro</ta>
            <ta e="T669" id="Seg_8612" s="T668">v</ta>
            <ta e="T671" id="Seg_8613" s="T669">post</ta>
            <ta e="T672" id="Seg_8614" s="T671">v</ta>
            <ta e="T673" id="Seg_8615" s="T672">v</ta>
            <ta e="T674" id="Seg_8616" s="T673">v</ta>
            <ta e="T675" id="Seg_8617" s="T674">ptcl</ta>
            <ta e="T676" id="Seg_8618" s="T675">que</ta>
            <ta e="T677" id="Seg_8619" s="T676">cop</ta>
            <ta e="T678" id="Seg_8620" s="T677">ptcl</ta>
            <ta e="T679" id="Seg_8621" s="T678">ptcl</ta>
            <ta e="T680" id="Seg_8622" s="T679">n</ta>
            <ta e="T681" id="Seg_8623" s="T680">adj</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR" />
         <annotation name="SyF" tierref="SyF" />
         <annotation name="IST" tierref="IST" />
         <annotation name="Top" tierref="Top" />
         <annotation name="Foc" tierref="Foc" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T5" id="Seg_8624" s="T4">RUS:cult</ta>
            <ta e="T67" id="Seg_8625" s="T66">RUS:cult</ta>
            <ta e="T150" id="Seg_8626" s="T149">RUS:gram</ta>
            <ta e="T266" id="Seg_8627" s="T265">RUS:core</ta>
            <ta e="T272" id="Seg_8628" s="T271">RUS:gram</ta>
            <ta e="T276" id="Seg_8629" s="T275">RUS:gram</ta>
            <ta e="T287" id="Seg_8630" s="T286">RUS:core</ta>
            <ta e="T404" id="Seg_8631" s="T403">RUS:mod</ta>
            <ta e="T426" id="Seg_8632" s="T425">RUS:mod</ta>
            <ta e="T494" id="Seg_8633" s="T493">RUS:gram</ta>
            <ta e="T525" id="Seg_8634" s="T524">RUS:mod</ta>
            <ta e="T527" id="Seg_8635" s="T526">RUS:core</ta>
            <ta e="T537" id="Seg_8636" s="T536">RUS:mod</ta>
            <ta e="T610" id="Seg_8637" s="T609">RUS:mod</ta>
            <ta e="T630" id="Seg_8638" s="T629">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T9" id="Seg_8639" s="T0">Well, earlier, didn't we grow up like that and tales were told since we were small?</ta>
            <ta e="T18" id="Seg_8640" s="T9">Well, when I was small, there were no games.</ta>
            <ta e="T34" id="Seg_8641" s="T18">For that the children didn't fall asleep and so on, our fathers and our fathers' fathers, a tale which is told to children, a tale especially for children.</ta>
            <ta e="T45" id="Seg_8642" s="T34">Long ago, when earth and sky were arising, all could speak, it is said, all animals.</ta>
            <ta e="T55" id="Seg_8643" s="T45">And the animals, eh, the stories, the animals' trips – this is told.</ta>
            <ta e="T62" id="Seg_8644" s="T55">There a lived a fox long time ago, it is said.</ta>
            <ta e="T84" id="Seg_8645" s="T62">The fox, though, when it became spring, nowhere food could be found, the autumnal food of the Dolgans had run out, which the Dolgans had as reserves.</ta>
            <ta e="T98" id="Seg_8646" s="T84">The water turned into snow, therefore it had no food and was starving, it went through the world, the fox.</ta>
            <ta e="T103" id="Seg_8647" s="T98">But running so...</ta>
            <ta e="T115" id="Seg_8648" s="T103">"Oh, where do I find something", it thinks, runs into the forest and runs through the forest.</ta>
            <ta e="T126" id="Seg_8649" s="T115">And it was running, constantly raising the head, and it sees that on the top of an expanding larch a rough-legged buzzard had got chicks and is lying (i.e. sitting).</ta>
            <ta e="T127" id="Seg_8650" s="T126">Was?!</ta>
            <ta e="T138" id="Seg_8651" s="T127">"Well, how do I make it, how do I betray [it], so that I will eat an egg of this rough-legged buzzard?"</ta>
            <ta e="T163" id="Seg_8652" s="T138">he thought and to the rough-legged buzzard, in order to eat an egg of the rough-legged buzzard, it came running, under him, under the larch it came, then it began to thrash the larch with its tail.</ta>
            <ta e="T182" id="Seg_8653" s="T163">"Rough-legged buzzard, rough-legged buzzard, quickly, give a child of yours, my tail is sharper than a Russian sword, I'll chop the larch at all, then I'll eat you, too."</ta>
            <ta e="T200" id="Seg_8654" s="T182">What shall the rough-legged buzzard do, crying it rolled it child out, it [the fox] ate an egg, then it ran further through the world.</ta>
            <ta e="T210" id="Seg_8655" s="T200">In the next morning it stands up again, in order to betray the rough-legged buzzed it came running again:</ta>
            <ta e="T217" id="Seg_8656" s="T210">"Rough-legged buzzard, rough-legged buzzard, quickly, bring a child of yours.</ta>
            <ta e="T229" id="Seg_8657" s="T217">My tail is sharper than a Russian sword, I'll chop the larch completely and then I'll eat you, too."</ta>
            <ta e="T234" id="Seg_8658" s="T229">Again it pushed out one child.</ta>
            <ta e="T238" id="Seg_8659" s="T234">It ate two children, the fox.</ta>
            <ta e="T242" id="Seg_8660" s="T238">The rough-buzzard sat there crying.</ta>
            <ta e="T251" id="Seg_8661" s="T242">"Well, now it will eat me, it says", it [= the buzzard] cried.</ta>
            <ta e="T257" id="Seg_8662" s="T251">Well, when it was sitting there, a Siberian jay came flying.</ta>
            <ta e="T264" id="Seg_8663" s="T257">"Hey, rough-legged buzzard, why are you crying then?</ta>
            <ta e="T282" id="Seg_8664" s="T264">When there is such a beautiful sunrise, your children are lying warm there, then you yourself can get food, too.</ta>
            <ta e="T289" id="Seg_8665" s="T282">Why are you crying, if you live so happily?", it says.</ta>
            <ta e="T292" id="Seg_8666" s="T289">"How don't you let me cry?</ta>
            <ta e="T294" id="Seg_8667" s="T292">Don't you see?</ta>
            <ta e="T303" id="Seg_8668" s="T294">The fox had eaten two children of mine in two days.</ta>
            <ta e="T308" id="Seg_8669" s="T303">And now it'll come again today and will eat.</ta>
            <ta e="T317" id="Seg_8670" s="T308">It'll chop my larch completely, it wants to eat me", it [= the buzzard] says.</ta>
            <ta e="T320" id="Seg_8671" s="T317">"What fox is that?"</ta>
            <ta e="T325" id="Seg_8672" s="T320">"Well, its tail is sharper than a Russian sword."</ta>
            <ta e="T333" id="Seg_8673" s="T325">"Well, as what kind of a stupid bird you've been born then?</ta>
            <ta e="T344" id="Seg_8674" s="T333">Well, (how the rough-legged buzzard) how the fox will chop your larch?</ta>
            <ta e="T346" id="Seg_8675" s="T344">Its tail is furry.</ta>
            <ta e="T353" id="Seg_8676" s="T346">'A furry tail, how will you make it?', say when he'll come.</ta>
            <ta e="T359" id="Seg_8677" s="T353">If he asks who taught you, then say that the Siberian jay taught you."</ta>
            <ta e="T372" id="Seg_8678" s="T359">Well, the rough-legged buzzard became a bit better, it was sitting, and the Siberian jay flew away.</ta>
            <ta e="T381" id="Seg_8679" s="T372">Oh, but the fox became very angry, it was very angry:</ta>
            <ta e="T395" id="Seg_8680" s="T381">It tore out different kinds of moss, it tore it with its front and hind legs and it came in order to frighten the rough-legged buzzard.</ta>
            <ta e="T406" id="Seg_8681" s="T395">"Rough-legged buzzard, rough-legged buzzard, having such a mother, I am here again and I am very angry.</ta>
            <ta e="T423" id="Seg_8682" s="T406">Give me a child, my tail is sharper than a Russian sword, I'll chop [the larch] together with you and I'll eat you, too."</ta>
            <ta e="T437" id="Seg_8683" s="T423">"Well, because I was so stupid, I gave you two of my dear children to eat, in my own stupidity.</ta>
            <ta e="T450" id="Seg_8684" s="T437">You have a furry tail, though, a furry tail, what will you make, how will you chop the tree?", it says.</ta>
            <ta e="T467" id="Seg_8685" s="T450">"Oh my god, who did give you reason, how did you make it to find reason?", it says, "how do you know about the fur of my tail?"</ta>
            <ta e="T477" id="Seg_8686" s="T467">"It's the Siberian jay who taught me, who'll give me reason, the Siberian jay taught me."</ta>
            <ta e="T491" id="Seg_8687" s="T477">"Well, such a Siberian jay, where will I find it, wait, I'll meet it though."</ta>
            <ta e="T507" id="Seg_8688" s="T491">So it did and it ran through the world, it ran through the world and coming to a completely round lake...</ta>
            <ta e="T513" id="Seg_8689" s="T507">This lake is located near a very big mountain.</ta>
            <ta e="T519" id="Seg_8690" s="T513">It had become spring and the hill has completely gone out [i.e. became visible].</ta>
            <ta e="T536" id="Seg_8691" s="T519">Running past the hill it came to the most beautiful patch in the snow, it turned into a dead fox and was lying there streched out.</ta>
            <ta e="T544" id="Seg_8692" s="T536">It was lying there completely surrounded by insects.</ta>
            <ta e="T556" id="Seg_8693" s="T544">Well, the Siberian jays come flying and see that the fox has died and is lying there stretched out.</ta>
            <ta e="T567" id="Seg_8694" s="T556">"Oh, having starved so long, it ate the chicks of the rough-legged buzzard.</ta>
            <ta e="T577" id="Seg_8695" s="T567">But now the rough-legged buzzard takes reason from me, it didn't give away more chicks."</ta>
            <ta e="T590" id="Seg_8696" s="T577">Where does it take something from, it's starving though, where does it take food from, well, it has died out of hunger.</ta>
            <ta e="T596" id="Seg_8697" s="T590">Well, you also have to die", it thought.</ta>
            <ta e="T598" id="Seg_8698" s="T596">Well, it came there.</ta>
            <ta e="T602" id="Seg_8699" s="T598">Knock-knock, knock-knock, it pecked.</ta>
            <ta e="T609" id="Seg_8700" s="T602">"From where, one has to eat from its eyes", it thought pecking into its lower arm.</ta>
            <ta e="T621" id="Seg_8701" s="T609">Completely... but the fox with all its slyness jumped up, growling terribly.</ta>
            <ta e="T632" id="Seg_8702" s="T621">This rough-legged buzzard, well that one, the Siberian jay did not make it to escape, though, and got caught at its tail.</ta>
            <ta e="T657" id="Seg_8703" s="T632">And so [the fox] scuffled with it, it rolled it into the water, it rolled it into the water, there it made it wet, in the muddy water it made it wet, it choked it, then it let it free and that one flew away.</ta>
            <ta e="T671" id="Seg_8704" s="T657">Well, the fox went to the shore and dried itself, having dried itself...</ta>
            <ta e="T678" id="Seg_8705" s="T671">Having revived it lived rich and satured, or what has happened to it?</ta>
            <ta e="T681" id="Seg_8706" s="T678">Well, the tale has ended.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T9" id="Seg_8707" s="T0">Nun früher, sind wir nicht so aufgewachsen und es wurden Märchen erzählt, seit wir klein waren?</ta>
            <ta e="T18" id="Seg_8708" s="T9">Nun, als ich klein war, gab es keine Spiele.</ta>
            <ta e="T34" id="Seg_8709" s="T18">Damit die Kinder nicht einschliefen und so weiter, unsere Väter und die Väter unserer Väter, ein Märchen, das Kindern erzählt wird, ein Märchen speziell für Kinder.</ta>
            <ta e="T45" id="Seg_8710" s="T34">Vor langer Zeit, als Himmel und Erde entstanden, konnten alle sprechen, sagt man, alle Tiere.</ta>
            <ta e="T55" id="Seg_8711" s="T45">Und diese Tiere, die Geschichten, die Reisen dieser Tiere – das wird erzählt.</ta>
            <ta e="T62" id="Seg_8712" s="T55">Es lebte einmal vor langer langer Zeit ein Fuchs, sagt man.</ta>
            <ta e="T84" id="Seg_8713" s="T62">Dieser Fuchs aber, als es Frühling wurde, nirgendwo war Futter zu finden, die herbstliche dolganische Nahrung war zuende gegangen, was die Dolganen als Vorrat hatten.</ta>
            <ta e="T98" id="Seg_8714" s="T84">Das Wasser wurde zu Schnee, deshalb hatte er nichts zu fressen und hungerte, er lief durch die Welt, der Fuchs.</ta>
            <ta e="T103" id="Seg_8715" s="T98">So laufend aber… </ta>
            <ta e="T115" id="Seg_8716" s="T103">"Oh, wo finde ich etwas", denkt er, läuft in den Wald und rennt durch den Wald.</ta>
            <ta e="T126" id="Seg_8717" s="T115">Und er lief immer wieder den Kopf hebend und sah, dass auf der Spitze einer ausladenden Lärche ein Raufußbussard gerade Küken bekommen hat.</ta>
            <ta e="T127" id="Seg_8718" s="T126">Was?!</ta>
            <ta e="T138" id="Seg_8719" s="T127">"Nun, wie mache ich es, wie betrüge ich ihn, dass ich von diesem Raufußbussard ein Ei essen werde?"</ta>
            <ta e="T163" id="Seg_8720" s="T138">dachte er und zum Raufußbussard, um vom Raufußbussard ein Ei zu fressen, kam er angerannt, direkt unter ihn, unter die Lärche kam er, dann fing er an, mit seinem Schwanz gegen die Lärche zu schlagen.</ta>
            <ta e="T182" id="Seg_8721" s="T163">"Raufußbussard, Raufußbussard, fix, gib mir ein Kind von dir, mein Schwanz ist schärfer als ein russisches Schwert, ich fälle die Lärche ganz und gar, dann fresse ich auch dich selbst."</ta>
            <ta e="T200" id="Seg_8722" s="T182">Was soll der Raufußbussard machen, weinend rollt er ein Kind hinaus, er [der Fuchs] fraß ein Ei, dann lief er weiter durch die Welt.</ta>
            <ta e="T210" id="Seg_8723" s="T200">Am nächsten Morgen steht er wieder auf, um den Raufußbussard zu betrügen, er kommt wieder angerannt: </ta>
            <ta e="T217" id="Seg_8724" s="T210">"Raufußbussard, Raufußbussard, fix, gib mir ein Kind von dir.</ta>
            <ta e="T229" id="Seg_8725" s="T217">Mein Schwanz ist schärfer als ein russisches Schwert, ich fälle die Lärche ganz und gar und dann fresse ich auch dich selbst."</ta>
            <ta e="T234" id="Seg_8726" s="T229">Und er stieß wieder ein Kind hinaus.</ta>
            <ta e="T238" id="Seg_8727" s="T234">Er fraß zwei Kinder, der Fuchs.</ta>
            <ta e="T242" id="Seg_8728" s="T238">Der Raufußbussard saß da und weinte.</ta>
            <ta e="T251" id="Seg_8729" s="T242">"Nun, jetzt wird er aber mich selbst fressen, sagt er", weinte er.</ta>
            <ta e="T257" id="Seg_8730" s="T251">Nun, als er da saß, kam ein Unglückshäher geflogen.</ta>
            <ta e="T264" id="Seg_8731" s="T257">"Hey, Raufußbussard, warum weinst du denn?</ta>
            <ta e="T282" id="Seg_8732" s="T264">Wenn so ein schöner Sonnenaufgang ist, deine Kinder liegen in der Wärme, dann kannst du auch selber Futter holen.</ta>
            <ta e="T289" id="Seg_8733" s="T282">Warum weinst du, wenn du so fröhlich lebst?", sagt er.</ta>
            <ta e="T292" id="Seg_8734" s="T289">"Nun, wie lässt du mich nicht weinen?</ta>
            <ta e="T294" id="Seg_8735" s="T292">Siehst du denn nicht?</ta>
            <ta e="T303" id="Seg_8736" s="T294">Mir hat der Fuchs an zwei Tagen zwei Kinder gefressen.</ta>
            <ta e="T308" id="Seg_8737" s="T303">Und jetzt kommt er heute wieder und frisst.</ta>
            <ta e="T317" id="Seg_8738" s="T308">Meine Lärche wird er ganz und gar fällen, er will mich selber fressen", sagt er [der Raufußbussard].</ta>
            <ta e="T320" id="Seg_8739" s="T317">"Was ist das für ein Fuchs?"</ta>
            <ta e="T325" id="Seg_8740" s="T320">"Na, sein Schwanz ist schärfer als ein russischer Schwert."</ta>
            <ta e="T333" id="Seg_8741" s="T325">"Nun, als was für ein dummer Vogel bist du geboren, so einer?</ta>
            <ta e="T344" id="Seg_8742" s="T333">Nun, (wie soll der Raufußbussard) wie soll der Fuchs deine Lärche abschlagen?</ta>
            <ta e="T346" id="Seg_8743" s="T344">Sein Schwanz ist pelzig.</ta>
            <ta e="T353" id="Seg_8744" s="T346">'Ein pelziger Schwanz, wie willst du es schaffen?', sag, wenn er jetzt kommt.</ta>
            <ta e="T359" id="Seg_8745" s="T353">Wenn er fragt, wer dich gelehrt hat, dann sag, dass der Unglückshäher dich gelehrt hat."</ta>
            <ta e="T372" id="Seg_8746" s="T359">Nun, da ging es dem Raufußbussard, dem Raufußbussard etwas besser, er saß und der Unglückshäher flog davon.</ta>
            <ta e="T381" id="Seg_8747" s="T372">Oh, aber der Fuchs wurde sehr böse, er ärgerte sich sehr. </ta>
            <ta e="T395" id="Seg_8748" s="T381">Unterschiedliches Moos riss er aus, mit seinen Vorder- und Hinterbeinen zerriss er es und kam, um dem Raufußbussard Angst zu machen.</ta>
            <ta e="T406" id="Seg_8749" s="T395">"Raufußbussard, Raufußbussard, der du so eine Mutter hast, ich bin es wieder und bin sehr böse geworden.</ta>
            <ta e="T423" id="Seg_8750" s="T406">Gib ein Kind, mein Schwanz ist schärfer als ein russisches Schwert, mit dir selbst fälle ich [die Lärche] und fresse auch dich selbst."</ta>
            <ta e="T437" id="Seg_8751" s="T423">"Nun, weil ich dumm war, habe ich dir zwei meiner lieben Kinder zu fressen gegeben, in meiner eigenen Dummheit.</ta>
            <ta e="T450" id="Seg_8752" s="T437">Du aber hast einen pelzigen Schwanz, einen pelzigen Schwanz, wie willst du es schaffen, wie willst du den Baum fällen?", sagt er.</ta>
            <ta e="T467" id="Seg_8753" s="T450">"Oh mein Gott, wer hat dir denn Verstand gegeben, wie hast du es gemacht, Verstand zu finden?", sagt er, "woher weißt du vom Pelz meines Schwanzes?"</ta>
            <ta e="T477" id="Seg_8754" s="T467">"Mich hat der Unglückshäher gelehrt, wer mir Verstand gibt, der Unglückshäher hat es gelehrt."</ta>
            <ta e="T491" id="Seg_8755" s="T477">"Nun so einen Unglückshäher, wo finde ich den, nun warte, ich werde mich wohl mit ihm treffen."</ta>
            <ta e="T507" id="Seg_8756" s="T491">Er machte das und rannte durch die Welt, er lief durch die Welt, kam an einen ganz runden See… </ta>
            <ta e="T513" id="Seg_8757" s="T507">Dieser See ist an einem sehr großen Berg.</ta>
            <ta e="T519" id="Seg_8758" s="T513">Es war Frühling geworden und der Hügel kam ganz heraus [d.h. war zu sehen].</ta>
            <ta e="T536" id="Seg_8759" s="T519">An diesem Hügel vorbeilaufend kam er zum schönsten Loch in der Schneedecke, er verwandelte sich in einen toten Fuchs und lag ausgestreckt dort.</ta>
            <ta e="T544" id="Seg_8760" s="T536">Er lag ganz und gar von Insekten umschwärmt da danach.</ta>
            <ta e="T556" id="Seg_8761" s="T544">Nun, die Unglückshäher kommen geflogen und sehen, dass der Fuchs, nachdem er gestorben ist, ausgestreckt da liegt.</ta>
            <ta e="T567" id="Seg_8762" s="T556">"Oh, da er so lange gehungert hatte, fraß er die Küken des Raufußbussards.</ta>
            <ta e="T577" id="Seg_8763" s="T567">Jetzt hat der Raufußbussard aber Verstand von mir angenommen, er hat kein Küken mehr gegeben."</ta>
            <ta e="T590" id="Seg_8764" s="T577">Woher nimmt er dann aber etwas, wenn er doch hungert, woher nimmt er etwas zu fressen, nun, er ist verhungert.</ta>
            <ta e="T596" id="Seg_8765" s="T590">Nun, auch du bist sterblich", dachte er.</ta>
            <ta e="T598" id="Seg_8766" s="T596">Und er kam dorthin.</ta>
            <ta e="T602" id="Seg_8767" s="T598">Klopf-klopf, klopf-klopf, pickte er.</ta>
            <ta e="T609" id="Seg_8768" s="T602">"Woher, man muss von seinen Augen fressen", dachte er, während er in den Unterarm pickte.</ta>
            <ta e="T621" id="Seg_8769" s="T609">Ganz… der Fuchs aber mit seiner ganzen Schläue sprang mit einem teuflischen Knurren auf.</ta>
            <ta e="T632" id="Seg_8770" s="T621">Dieser Raufußbussard aber, also jener, der Unglückshäher aber schaffte es nicht zu entkommen und ließ sich am Schwanz fangen.</ta>
            <ta e="T657" id="Seg_8771" s="T632">Und so raufte [der Fuchs] mit diesem, er rollte ihn ins Wasser, er rollte ihn ins Wasser, dort machte er ihn nass, im schlammigen Wasser machte er ihn nass, er würgte ihn, dann ließ er ihn los und [jener] flog davon.</ta>
            <ta e="T671" id="Seg_8772" s="T657">Nun, der Fuchs aber ging ans Ufer und trocknete sich, nachdem er sich getrocknet hatte… </ta>
            <ta e="T678" id="Seg_8773" s="T671">Wieder am Leben lebte er reich und satt, oder was ist mit ihm passiert?</ta>
            <ta e="T681" id="Seg_8774" s="T678">Nun, unser Märchen ist zuende.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T9" id="Seg_8775" s="T0">Вот давно, с наших малых лет, часто рассказывая сказки, [так] мы разве росли?</ta>
            <ta e="T18" id="Seg_8776" s="T9">Вот, когда мы были маленькими, никаких игр не было.</ta>
            <ta e="T34" id="Seg_8777" s="T18">Детей чтобы не усыпить и тому подобное, наши отцы, отцы наших отцов рассказывали ребёнку сказки, специальные детские сказки.</ta>
            <ta e="T45" id="Seg_8778" s="T34">Давно, когда возникла земля и небо, у всех был свой язык – у всех животных.</ta>
            <ta e="T55" id="Seg_8779" s="T45">И те животные с историями, теми животными ходившие тропы – вот это рассказывают.</ta>
            <ta e="T62" id="Seg_8780" s="T55">И тогда лиса жила, говорят, в давние-давние времени.</ta>
            <ta e="T84" id="Seg_8781" s="T62">И вот лиса, когда наступила весна, негде было добыть пищу, осенняя долганская еда закончилась, долганские запасы еды были.</ta>
            <ta e="T98" id="Seg_8782" s="T84">Вода превратилась в снег, из-за этого оставшись без еды, проголодавшись, по земле быстро передвигалась лиса.</ta>
            <ta e="T103" id="Seg_8783" s="T98">Вот передвигаясь… </ta>
            <ta e="T115" id="Seg_8784" s="T103">"О,о, где я найду?", – говоря, в лес заходя, по лесу быстро бежит.</ta>
            <ta e="T126" id="Seg_8785" s="T115">И во-от, смотрит, голову запрокидывая пробежала, (?) на верхушку лиственницы мохнатый канюк, родив птенцов, лежит ( ?) (т.е сидит).</ta>
            <ta e="T127" id="Seg_8786" s="T126">Что-о?!</ta>
            <ta e="T138" id="Seg_8787" s="T127">"До(?Дру)г, и потом я каким образом сделав, обманув, съем от этого мохнатого канюка это, яйцо?"</ta>
            <ta e="T163" id="Seg_8788" s="T138">– сказав этот, от мохнатого канюка в сторону прибежав, прямо от него, что внизу, пришёл к низу лиственницы и таким образом своим хвостом лиственницу стал бить.</ta>
            <ta e="T182" id="Seg_8789" s="T163">"Мохнатый канюк, мохнатый канюк, ..такой-сякой, дай одного птенца, мой хвост острее русского меча, вместе с лиственницей отрубив(?), завалю, вместе с тобой съем"</ta>
            <ta e="T200" id="Seg_8790" s="T182">Что делать мохнатому канюку, плача одного своего птенца выкатил, одно яйцо съела и , сделав это, побежала дальше.</ta>
            <ta e="T210" id="Seg_8791" s="T200">Наутро встала, чтобы обмануть мохнатого канюка, чтобы обмануть мохнатого канюка, прибежала: </ta>
            <ta e="T217" id="Seg_8792" s="T210">"Мохнатый канюк, мохнатый канюк, такой .. сякой, дай одного своего птенца,</ta>
            <ta e="T229" id="Seg_8793" s="T217">Мой хвост острее русского меча, вместе с лиственницей твоею разрублю и повалю, я сейчас вместе с тобой съем".</ta>
            <ta e="T234" id="Seg_8794" s="T229">Опять одного птенца вытолкнул.</ta>
            <ta e="T238" id="Seg_8795" s="T234">Двоих птенцов съелa – лиса.</ta>
            <ta e="T242" id="Seg_8796" s="T238">Мохнатый канюк сидит и плачет.</ta>
            <ta e="T251" id="Seg_8797" s="T242">"До (?Друг), вот, сейчас меня самого съест говоря", – заплакал.</ta>
            <ta e="T257" id="Seg_8798" s="T251">Итак, когда он сидел и плакал, птица-кукша прилетела.</ta>
            <ta e="T264" id="Seg_8799" s="T257">"Ну-у, мохнатый канюк, отчего плачешь вот ты?</ta>
            <ta e="T282" id="Seg_8800" s="T264">Когда такая хорошая погода установилась, и птенцы твои в тепле лежат, и сам еду сможешь принести.</ta>
            <ta e="T289" id="Seg_8801" s="T282">Отчего плачешь вот, в таком веселье живя?" – говорит.</ta>
            <ta e="T292" id="Seg_8802" s="T289">"Как мне не плакать?</ta>
            <ta e="T294" id="Seg_8803" s="T292">Не видишь что ли?</ta>
            <ta e="T303" id="Seg_8804" s="T294">У меня, значит, лиса за два дня двоих птенцов съела.</ta>
            <ta e="T308" id="Seg_8805" s="T303">И сейчас, сегодня опять придёт, съест.</ta>
            <ta e="T317" id="Seg_8806" s="T308">Вместе с лиственницей, со всем моим имуществом, срубив, повалит и съест меня самого", – говорит.</ta>
            <ta e="T320" id="Seg_8807" s="T317">"А что это за лиса?"</ta>
            <ta e="T325" id="Seg_8808" s="T320">"У неё хвост острее русского меча".</ta>
            <ta e="T333" id="Seg_8809" s="T325">"Да, что за глупой птицей ты родилась вот такая?</ta>
            <ta e="T344" id="Seg_8810" s="T333">Вот, (как мохнатый канюк) каким образом лиса твою лиственницу вырубит?</ta>
            <ta e="T346" id="Seg_8811" s="T344">Хвост меховой.</ta>
            <ta e="T353" id="Seg_8812" s="T346">Меховой хвост, что он осилит? – скажешь, когда она придёт.</ta>
            <ta e="T359" id="Seg_8813" s="T353">Если спросит, кто научил, скажешь – птица-кукша научила.</ta>
            <ta e="T372" id="Seg_8814" s="T359">Вот, мохнатому канюку, мохнатому канюку стало лучше, сел, а кукааки его улетел.</ta>
            <ta e="T381" id="Seg_8815" s="T372">О,о, лиса захотевши рассердиться, рассердилась, захотевши озлобиться, озлобилась: </ta>
            <ta e="T395" id="Seg_8816" s="T381">Разный мох распинала ногами, разрывая, распинывая, пришла к мохнатому канюку, чтобы можнатого канюка испугать.</ta>
            <ta e="T406" id="Seg_8817" s="T395">"Мохнатый канюк, мохнатый канюк, имеющий такую-то мать, вот я опять, захотевши рассердится, рассердилась.</ta>
            <ta e="T423" id="Seg_8818" s="T406">Дай одного птенца, мой хвост острее русского меча, вместе с тобой раз.. разрубив, вместе с тобой съем".</ta>
            <ta e="T437" id="Seg_8819" s="T423">"Ко-о, обдуривши сильно так-то несчастных двoих моих птенцов дал тебе съесть, в этом моя дурость".</ta>
            <ta e="T450" id="Seg_8820" s="T437">А ты, меховой хвост ведь, меховой хвост, что ты осилишь, дерево разве осилишь?" – говорит.</ta>
            <ta e="T467" id="Seg_8821" s="T450">"Ко-о, боже мой, что за сила тебя одарила умом, как ты ум нашёл? – говорит, – откуда знаешь о моём меховом хвосте?"</ta>
            <ta e="T477" id="Seg_8822" s="T467">"А меня птица-кукша научила, кто мне даст ум, кукша научила".</ta>
            <ta e="T491" id="Seg_8823" s="T477">"Вот, такая-сякая кукша, из какой земли я её найду? Ну, сейчас, встречусь я с ней!"</ta>
            <ta e="T507" id="Seg_8824" s="T491">Сказавши это, ускакала прочь, ускакавши прочь…, к круглому озеру прискакав… </ta>
            <ta e="T513" id="Seg_8825" s="T507">Это озеро у очень большой горы.</ta>
            <ta e="T519" id="Seg_8826" s="T513">С наступлением весны все окрестности вышли (т.е стали видны).</ta>
            <ta e="T536" id="Seg_8827" s="T519">Эти пригорки пробегая, прибежав к самым красивым проталинкам, превратившись в умершую лису, упала навзничь.</ta>
            <ta e="T544" id="Seg_8828" s="T536">Вообще разные насекомые и черви её одолели.</ta>
            <ta e="T556" id="Seg_8829" s="T544">Во-от, кукши, летая, увидели – лиса мертвая лежит, распластавшись.</ta>
            <ta e="T567" id="Seg_8830" s="T556">"О-о, тем временем сильно проголодавшись, мохнатого канюка птенцов поедала.</ta>
            <ta e="T577" id="Seg_8831" s="T567">А теперь мохнатый канюк от меня ум берёт, ни одного птенца не отдал.</ta>
            <ta e="T590" id="Seg_8832" s="T577">Конечно, и откуда же возьмёт, она тоже проголодавши, от голода умерла.</ta>
            <ta e="T596" id="Seg_8833" s="T590">Ну, ты тоже смертный", подумавши.</ta>
            <ta e="T598" id="Seg_8834" s="T596">Вот, пришёл.</ta>
            <ta e="T602" id="Seg_8835" s="T598">Лон-лон, лон-лон – постучал.</ta>
            <ta e="T609" id="Seg_8836" s="T602">Откуда, с его глаза съесть, подумав, в предплечье постучав.</ta>
            <ta e="T621" id="Seg_8837" s="T609">Вообщем … лиса оказывается всей своей хитростью, со всей силой рыча подскачила.</ta>
            <ta e="T632" id="Seg_8838" s="T621">Тот мохнатый канюк, этот самый, кукаки не успев убежать позволил себя поймать за хвост.</ta>
            <ta e="T657" id="Seg_8839" s="T632">И таким образом поволок, в воду скатившись, (вода …) в воду уронив, в ней прополоскав – в воду с илом уронив, кукша смогла улететь.</ta>
            <ta e="T671" id="Seg_8840" s="T657">Ну, та лиса, выбравшись на берег, высыхала, наверное, вот высыхнув ушла. </ta>
            <ta e="T678" id="Seg_8841" s="T671">Oживши, в богатстве и счастье жила, или куда она делась?</ta>
            <ta e="T681" id="Seg_8842" s="T678">Ну, наша сказка закончилась.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T9" id="Seg_8843" s="T0">Вот давно, с наших малых лет, часто расказывая сказки, рождались мы что-ли?</ta>
            <ta e="T18" id="Seg_8844" s="T9">Вот, когда мы маленькими никаких игр не было.</ta>
            <ta e="T34" id="Seg_8845" s="T18">Детей чтобы не усыпить, наши отцы, наших отцов отцы – сказка, которую рассказывали ребёнку, специально для ребёнка, говоря.</ta>
            <ta e="T45" id="Seg_8846" s="T34">Давно, когда появилась вся земля, все имели свой язык – все животные.</ta>
            <ta e="T55" id="Seg_8847" s="T45">И те животные с историями, теми животными ходившие тропы – вот это рассказывают.</ta>
            <ta e="T62" id="Seg_8848" s="T55">И тогда лиса жила, говорят, в давние-давние времени.</ta>
            <ta e="T84" id="Seg_8849" s="T62">И вот лиса, когда наступила весна, негде было добыть пищу, осенняя долганская еда закончилась, долганские запасы еды были.</ta>
            <ta e="T98" id="Seg_8850" s="T84">Вода превратилась в снег, из-за этого оставшись без еды, проголодавшись, по земле быстро передвигалась лиса.</ta>
            <ta e="T103" id="Seg_8851" s="T98">Вот передвигаясь… </ta>
            <ta e="T115" id="Seg_8852" s="T103">"О,о, где я найду?", – говоря, в лес заходя, по лесу быстро бежит.</ta>
            <ta e="T126" id="Seg_8853" s="T115">И во-от, смотрит, голову запрокидывая пробежала, (?) на верхушку лиственницы мохнатый канюк, родив птенцов, лежит ( ?) (т.е сидит).</ta>
            <ta e="T127" id="Seg_8854" s="T126">Что-о?!</ta>
            <ta e="T138" id="Seg_8855" s="T127">"До(?Дру)г, и потом я каким образом сделав, обманув, съем от этого мохнатого канюка это, яйцо?"</ta>
            <ta e="T163" id="Seg_8856" s="T138">– сказав этот, от мохнатого канюка в сторону прибежав, прямо от него, что внизу, пришёл к низу лиственницы и таким образом своим хвостом лиственницу стал бить.</ta>
            <ta e="T182" id="Seg_8857" s="T163">"Мохнатый канюк, мохнатый канюк, ..такой-сякой, дай одного птенца, мой хвост острее русского меча, вместе с лиственницей отрубив(?), завалю, вместе с тобой съем"</ta>
            <ta e="T200" id="Seg_8858" s="T182">Что делать мохнатому канюку, плача одного своего птенца выкатил, одно яйцо съела и , сделав это, побежала дальше.</ta>
            <ta e="T210" id="Seg_8859" s="T200">Наутро встала, чтобы обмануть мохнатого канюка, чтобы обмануть мохнатого канюка, прибежала: </ta>
            <ta e="T217" id="Seg_8860" s="T210">"Мохнатый канюк, мохнатый канюк, такой .. сякой, дай одного своего птенца,</ta>
            <ta e="T229" id="Seg_8861" s="T217">Мой хвост острее русского меча, вместе с лиственницей твоею разрублю и повалю, я сейчас вместе с тобой съем".</ta>
            <ta e="T234" id="Seg_8862" s="T229">Опять одного птенца вытолкнул.</ta>
            <ta e="T238" id="Seg_8863" s="T234">Двоих птенцов съелa – лиса.</ta>
            <ta e="T242" id="Seg_8864" s="T238">Мохнатый канюк сидит и плачет.</ta>
            <ta e="T251" id="Seg_8865" s="T242">"До (?Друг), вот, сейчас меня самого съест говоря", – заплакал.</ta>
            <ta e="T257" id="Seg_8866" s="T251">Итак, когда он сидел и плакал, птица-кукша прилетела.</ta>
            <ta e="T264" id="Seg_8867" s="T257">"Ну-у, мохнатый канюк, отчего плачешь вот ты?</ta>
            <ta e="T282" id="Seg_8868" s="T264">Когда такая хорошая погода установилась, и птенцы твои в тепле лежат, и сам еду сможешь принести.</ta>
            <ta e="T289" id="Seg_8869" s="T282">Отчего плачешь вот, в таком веселье живя?" – говорит.</ta>
            <ta e="T292" id="Seg_8870" s="T289">"Как мне не плакать?</ta>
            <ta e="T294" id="Seg_8871" s="T292">Не видишь что ли?</ta>
            <ta e="T303" id="Seg_8872" s="T294">У меня, значит, лиса за два дня двоих птенцов съела.</ta>
            <ta e="T308" id="Seg_8873" s="T303">И сейчас, сегодня опять придёт, съест.</ta>
            <ta e="T317" id="Seg_8874" s="T308">Вместе с лиственницей, со всем моим имуществом, срубив, повалит и съест меня самого", – говорит.</ta>
            <ta e="T320" id="Seg_8875" s="T317">"А что это за лиса?"</ta>
            <ta e="T325" id="Seg_8876" s="T320">"У неё хвост острее русского меча".</ta>
            <ta e="T333" id="Seg_8877" s="T325">"Да, что за глупой птицей ты родилась вот такая?</ta>
            <ta e="T344" id="Seg_8878" s="T333">Вот, (как мохнатый канюк) каким образом лиса твою лиственницу вырубит?</ta>
            <ta e="T346" id="Seg_8879" s="T344">Хвост меховой.</ta>
            <ta e="T353" id="Seg_8880" s="T346">Меховой хвост, что он осилит? – скажешь, когда она придёт.</ta>
            <ta e="T359" id="Seg_8881" s="T353">Если спросит, кто научил, скажешь – птица-кукша научила.</ta>
            <ta e="T372" id="Seg_8882" s="T359">Вот, мохнатому канюку, мохнатому канюку стало лучше, сел, а кукааки его улетел.</ta>
            <ta e="T381" id="Seg_8883" s="T372">О,о, лиса захотевши рассердиться, рассердилась, захотевши озлобиться, озлобилась: </ta>
            <ta e="T395" id="Seg_8884" s="T381">Разный мох распинала ногами, разрывая, распинывая, пришла к мохнатому канюку, чтобы можнатого канюка испугать.</ta>
            <ta e="T406" id="Seg_8885" s="T395">"Мохнатый канюк, мохнатый канюк, имеющий такую-то мать, вот я опять, захотевши рассердится, рассердилась.</ta>
            <ta e="T423" id="Seg_8886" s="T406">Дай одного птенца, мой хвост острее русского меча, вместе с тобой раз.. разрубив, вместе с тобой съем".</ta>
            <ta e="T437" id="Seg_8887" s="T423">"Ко-о, обдуривши сильно так-то несчастных двoих моих птенцов дал тебе съесть, в этом моя дурость".</ta>
            <ta e="T450" id="Seg_8888" s="T437">А ты, меховой хвост ведь, меховой хвост, что ты осилишь, дерево разве осилишь?" – говорит.</ta>
            <ta e="T467" id="Seg_8889" s="T450">"Ко-о, боже мой, что за сила тебя одарила умом, как ты ум нашёл? – говорит, – откуда знаешь о моём меховом хвосте?"</ta>
            <ta e="T477" id="Seg_8890" s="T467">"А меня птица-кукша научила, кто мне даст ум, кукша научила".</ta>
            <ta e="T491" id="Seg_8891" s="T477">"Вот, такая-сякая кукша, из какой земли я её найду? Ну, сейчас, встречусь я с ней!"</ta>
            <ta e="T507" id="Seg_8892" s="T491">Сказавши это, ускакала прочь, ускакавши прочь…, к круглому озеру прискакав… </ta>
            <ta e="T513" id="Seg_8893" s="T507">Это озеро у очень большой горы.</ta>
            <ta e="T519" id="Seg_8894" s="T513">С наступлением весны все окрестности вышли (т.е стали видны).</ta>
            <ta e="T536" id="Seg_8895" s="T519">Эти пригорки пробегая, прибежав к самым красивым проталинкам, превратившись в умершую лису, упала навзничь.</ta>
            <ta e="T544" id="Seg_8896" s="T536">Вообще разные насекомые и черви её одолели.</ta>
            <ta e="T556" id="Seg_8897" s="T544">Во-от, кукши, летая, увидели – лиса мертвая лежит, распластавшись.</ta>
            <ta e="T567" id="Seg_8898" s="T556">"О-о, тем временем сильно проголодавшись, мохнатого канюка птенцов поедала.</ta>
            <ta e="T577" id="Seg_8899" s="T567">А теперь мохнатый канюк от меня ум берёт, ни одного птенца не отдал.</ta>
            <ta e="T590" id="Seg_8900" s="T577">Конечно, и откуда же возьмёт, она тоже проголодавши, от голода умерла.</ta>
            <ta e="T596" id="Seg_8901" s="T590">Ну, ты тоже смертный", подумавши.</ta>
            <ta e="T598" id="Seg_8902" s="T596">Вот, пришёл.</ta>
            <ta e="T602" id="Seg_8903" s="T598">Лон-лон, лон-лон – постучал.</ta>
            <ta e="T609" id="Seg_8904" s="T602">Откуда, с его глаза съесть, подумав, в предплечье постучав.</ta>
            <ta e="T621" id="Seg_8905" s="T609">Вообщем … лиса оказывается всей своей хитростью, со всей силой рыча подскачила.</ta>
            <ta e="T632" id="Seg_8906" s="T621">Тот мохнатый канюк, этот самый, кукаки не успев убежать позволил себя поймать за хвост.</ta>
            <ta e="T657" id="Seg_8907" s="T632">И таким образом поволок, в воду скатившись, (вода …) в воду уронив, в ней прополоскав – в воду с илом уронив, кукша смогла улететь.</ta>
            <ta e="T671" id="Seg_8908" s="T657">Ну, та лиса, выбравшись на берег, высыхала, наверное, вот высыхнув ушла. </ta>
            <ta e="T678" id="Seg_8909" s="T671">Oживши, в богатстве и счастье жила, или куда она делась?</ta>
            <ta e="T681" id="Seg_8910" s="T678">Ну, наша сказка закончилась.</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T559" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
            <conversion-tli id="T571" />
            <conversion-tli id="T572" />
            <conversion-tli id="T573" />
            <conversion-tli id="T574" />
            <conversion-tli id="T575" />
            <conversion-tli id="T576" />
            <conversion-tli id="T577" />
            <conversion-tli id="T578" />
            <conversion-tli id="T579" />
            <conversion-tli id="T580" />
            <conversion-tli id="T581" />
            <conversion-tli id="T582" />
            <conversion-tli id="T583" />
            <conversion-tli id="T584" />
            <conversion-tli id="T585" />
            <conversion-tli id="T586" />
            <conversion-tli id="T587" />
            <conversion-tli id="T588" />
            <conversion-tli id="T589" />
            <conversion-tli id="T590" />
            <conversion-tli id="T591" />
            <conversion-tli id="T592" />
            <conversion-tli id="T593" />
            <conversion-tli id="T594" />
            <conversion-tli id="T595" />
            <conversion-tli id="T596" />
            <conversion-tli id="T597" />
            <conversion-tli id="T598" />
            <conversion-tli id="T599" />
            <conversion-tli id="T600" />
            <conversion-tli id="T601" />
            <conversion-tli id="T602" />
            <conversion-tli id="T603" />
            <conversion-tli id="T604" />
            <conversion-tli id="T605" />
            <conversion-tli id="T606" />
            <conversion-tli id="T607" />
            <conversion-tli id="T608" />
            <conversion-tli id="T609" />
            <conversion-tli id="T610" />
            <conversion-tli id="T682" />
            <conversion-tli id="T611" />
            <conversion-tli id="T612" />
            <conversion-tli id="T613" />
            <conversion-tli id="T614" />
            <conversion-tli id="T615" />
            <conversion-tli id="T616" />
            <conversion-tli id="T617" />
            <conversion-tli id="T618" />
            <conversion-tli id="T619" />
            <conversion-tli id="T620" />
            <conversion-tli id="T621" />
            <conversion-tli id="T622" />
            <conversion-tli id="T623" />
            <conversion-tli id="T624" />
            <conversion-tli id="T625" />
            <conversion-tli id="T626" />
            <conversion-tli id="T627" />
            <conversion-tli id="T628" />
            <conversion-tli id="T629" />
            <conversion-tli id="T630" />
            <conversion-tli id="T631" />
            <conversion-tli id="T632" />
            <conversion-tli id="T633" />
            <conversion-tli id="T634" />
            <conversion-tli id="T635" />
            <conversion-tli id="T636" />
            <conversion-tli id="T637" />
            <conversion-tli id="T638" />
            <conversion-tli id="T639" />
            <conversion-tli id="T640" />
            <conversion-tli id="T641" />
            <conversion-tli id="T642" />
            <conversion-tli id="T643" />
            <conversion-tli id="T644" />
            <conversion-tli id="T645" />
            <conversion-tli id="T646" />
            <conversion-tli id="T647" />
            <conversion-tli id="T648" />
            <conversion-tli id="T649" />
            <conversion-tli id="T650" />
            <conversion-tli id="T651" />
            <conversion-tli id="T652" />
            <conversion-tli id="T653" />
            <conversion-tli id="T654" />
            <conversion-tli id="T655" />
            <conversion-tli id="T656" />
            <conversion-tli id="T657" />
            <conversion-tli id="T658" />
            <conversion-tli id="T659" />
            <conversion-tli id="T660" />
            <conversion-tli id="T661" />
            <conversion-tli id="T662" />
            <conversion-tli id="T663" />
            <conversion-tli id="T664" />
            <conversion-tli id="T665" />
            <conversion-tli id="T666" />
            <conversion-tli id="T667" />
            <conversion-tli id="T668" />
            <conversion-tli id="T669" />
            <conversion-tli id="T670" />
            <conversion-tli id="T671" />
            <conversion-tli id="T672" />
            <conversion-tli id="T673" />
            <conversion-tli id="T674" />
            <conversion-tli id="T675" />
            <conversion-tli id="T676" />
            <conversion-tli id="T677" />
            <conversion-tli id="T678" />
            <conversion-tli id="T679" />
            <conversion-tli id="T680" />
            <conversion-tli id="T681" />
            <conversion-tli id="T683" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg"
                          name="gg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top"
                          name="Top"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc"
                          name="Foc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
