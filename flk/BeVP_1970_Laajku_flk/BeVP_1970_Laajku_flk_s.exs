<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDE2479A21-B3B9-2BBE-62EA-8CFB60FB62FC">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>BeVP_1970_Laajku_flk</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\flk\BeVP_1970_Laajku_flk\BeVP_1970_Laajku_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">684</ud-information>
            <ud-information attribute-name="# HIAT:w">492</ud-information>
            <ud-information attribute-name="# e">490</ud-information>
            <ud-information attribute-name="# HIAT:u">78</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="BeVP">
            <abbreviation>BeVP</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="4.0" type="appl" />
         <tli id="T1" time="4.5" type="appl" />
         <tli id="T2" time="5.0" type="appl" />
         <tli id="T3" time="5.5" type="appl" />
         <tli id="T4" time="6.0" type="appl" />
         <tli id="T5" time="6.5" type="appl" />
         <tli id="T6" time="7.0" type="appl" />
         <tli id="T7" time="7.5" type="appl" />
         <tli id="T8" time="8.0" type="appl" />
         <tli id="T9" time="8.5" type="appl" />
         <tli id="T10" time="9.0" type="appl" />
         <tli id="T11" time="9.5" type="appl" />
         <tli id="T12" time="10.0" type="appl" />
         <tli id="T13" time="10.5" type="appl" />
         <tli id="T14" time="11.0" type="appl" />
         <tli id="T15" time="11.5" type="appl" />
         <tli id="T16" time="12.0" type="appl" />
         <tli id="T17" time="12.5" type="appl" />
         <tli id="T18" time="13.0" type="appl" />
         <tli id="T19" time="13.5" type="appl" />
         <tli id="T20" time="14.0" type="appl" />
         <tli id="T21" time="14.5" type="appl" />
         <tli id="T22" time="15.0" type="appl" />
         <tli id="T23" time="15.5" type="appl" />
         <tli id="T24" time="16.0" type="appl" />
         <tli id="T25" time="16.5" type="appl" />
         <tli id="T26" time="17.0" type="appl" />
         <tli id="T27" time="17.5" type="appl" />
         <tli id="T28" time="18.0" type="appl" />
         <tli id="T29" time="18.5" type="appl" />
         <tli id="T30" time="19.0" type="appl" />
         <tli id="T31" time="19.5" type="appl" />
         <tli id="T32" time="20.0" type="appl" />
         <tli id="T33" time="20.5" type="appl" />
         <tli id="T34" time="21.0" type="appl" />
         <tli id="T35" time="21.5" type="appl" />
         <tli id="T36" time="22.0" type="appl" />
         <tli id="T37" time="22.5" type="appl" />
         <tli id="T38" time="23.0" type="appl" />
         <tli id="T39" time="23.5" type="appl" />
         <tli id="T40" time="24.0" type="appl" />
         <tli id="T41" time="24.5" type="appl" />
         <tli id="T42" time="25.0" type="appl" />
         <tli id="T43" time="25.5" type="appl" />
         <tli id="T44" time="26.0" type="appl" />
         <tli id="T45" time="26.5" type="appl" />
         <tli id="T46" time="27.0" type="appl" />
         <tli id="T47" time="27.5" type="appl" />
         <tli id="T48" time="28.0" type="appl" />
         <tli id="T49" time="28.5" type="appl" />
         <tli id="T50" time="29.0" type="appl" />
         <tli id="T51" time="29.5" type="appl" />
         <tli id="T52" time="30.0" type="appl" />
         <tli id="T53" time="30.5" type="appl" />
         <tli id="T54" time="31.0" type="appl" />
         <tli id="T55" time="31.5" type="appl" />
         <tli id="T56" time="32.0" type="appl" />
         <tli id="T57" time="32.5" type="appl" />
         <tli id="T58" time="33.0" type="appl" />
         <tli id="T59" time="33.5" type="appl" />
         <tli id="T60" time="34.0" type="appl" />
         <tli id="T61" time="34.5" type="appl" />
         <tli id="T62" time="35.0" type="appl" />
         <tli id="T63" time="35.5" type="appl" />
         <tli id="T64" time="36.0" type="appl" />
         <tli id="T65" time="36.5" type="appl" />
         <tli id="T66" time="37.0" type="appl" />
         <tli id="T67" time="37.5" type="appl" />
         <tli id="T68" time="38.0" type="appl" />
         <tli id="T69" time="38.5" type="appl" />
         <tli id="T70" time="39.0" type="appl" />
         <tli id="T71" time="39.5" type="appl" />
         <tli id="T72" time="40.0" type="appl" />
         <tli id="T73" time="40.5" type="appl" />
         <tli id="T74" time="41.0" type="appl" />
         <tli id="T75" time="41.5" type="appl" />
         <tli id="T76" time="42.0" type="appl" />
         <tli id="T77" time="42.5" type="appl" />
         <tli id="T78" time="43.0" type="appl" />
         <tli id="T79" time="43.5" type="appl" />
         <tli id="T80" time="44.0" type="appl" />
         <tli id="T81" time="44.5" type="appl" />
         <tli id="T82" time="45.0" type="appl" />
         <tli id="T83" time="45.5" type="appl" />
         <tli id="T84" time="46.0" type="appl" />
         <tli id="T85" time="46.5" type="appl" />
         <tli id="T86" time="47.0" type="appl" />
         <tli id="T87" time="47.5" type="appl" />
         <tli id="T88" time="48.0" type="appl" />
         <tli id="T89" time="48.5" type="appl" />
         <tli id="T90" time="49.0" type="appl" />
         <tli id="T91" time="49.5" type="appl" />
         <tli id="T92" time="50.0" type="appl" />
         <tli id="T93" time="50.5" type="appl" />
         <tli id="T94" time="51.0" type="appl" />
         <tli id="T95" time="51.5" type="appl" />
         <tli id="T96" time="52.0" type="appl" />
         <tli id="T97" time="52.5" type="appl" />
         <tli id="T98" time="53.0" type="appl" />
         <tli id="T99" time="53.5" type="appl" />
         <tli id="T100" time="54.0" type="appl" />
         <tli id="T101" time="54.5" type="appl" />
         <tli id="T102" time="55.0" type="appl" />
         <tli id="T103" time="55.5" type="appl" />
         <tli id="T104" time="56.0" type="appl" />
         <tli id="T105" time="56.5" type="appl" />
         <tli id="T106" time="57.0" type="appl" />
         <tli id="T107" time="57.5" type="appl" />
         <tli id="T108" time="58.0" type="appl" />
         <tli id="T109" time="58.5" type="appl" />
         <tli id="T110" time="59.0" type="appl" />
         <tli id="T111" time="59.5" type="appl" />
         <tli id="T112" time="60.0" type="appl" />
         <tli id="T113" time="60.5" type="appl" />
         <tli id="T114" time="61.0" type="appl" />
         <tli id="T115" time="61.5" type="appl" />
         <tli id="T116" time="62.0" type="appl" />
         <tli id="T117" time="62.5" type="appl" />
         <tli id="T118" time="63.0" type="appl" />
         <tli id="T119" time="63.5" type="appl" />
         <tli id="T120" time="64.0" type="appl" />
         <tli id="T121" time="64.5" type="appl" />
         <tli id="T122" time="65.0" type="appl" />
         <tli id="T123" time="65.5" type="appl" />
         <tli id="T124" time="66.0" type="appl" />
         <tli id="T125" time="66.5" type="appl" />
         <tli id="T126" time="67.0" type="appl" />
         <tli id="T127" time="67.5" type="appl" />
         <tli id="T128" time="68.0" type="appl" />
         <tli id="T129" time="68.5" type="appl" />
         <tli id="T130" time="69.0" type="appl" />
         <tli id="T131" time="69.5" type="appl" />
         <tli id="T132" time="70.0" type="appl" />
         <tli id="T133" time="70.5" type="appl" />
         <tli id="T134" time="71.0" type="appl" />
         <tli id="T135" time="71.5" type="appl" />
         <tli id="T136" time="72.0" type="appl" />
         <tli id="T137" time="72.5" type="appl" />
         <tli id="T138" time="73.0" type="appl" />
         <tli id="T139" time="73.5" type="appl" />
         <tli id="T140" time="74.0" type="appl" />
         <tli id="T141" time="74.5" type="appl" />
         <tli id="T142" time="75.0" type="appl" />
         <tli id="T143" time="75.5" type="appl" />
         <tli id="T144" time="76.0" type="appl" />
         <tli id="T145" time="76.5" type="appl" />
         <tli id="T146" time="77.0" type="appl" />
         <tli id="T147" time="77.5" type="appl" />
         <tli id="T148" time="78.0" type="appl" />
         <tli id="T149" time="78.5" type="appl" />
         <tli id="T150" time="79.0" type="appl" />
         <tli id="T151" time="79.5" type="appl" />
         <tli id="T152" time="80.0" type="appl" />
         <tli id="T153" time="80.5" type="appl" />
         <tli id="T154" time="81.0" type="appl" />
         <tli id="T155" time="81.5" type="appl" />
         <tli id="T156" time="82.0" type="appl" />
         <tli id="T157" time="82.5" type="appl" />
         <tli id="T158" time="83.0" type="appl" />
         <tli id="T159" time="83.5" type="appl" />
         <tli id="T160" time="84.0" type="appl" />
         <tli id="T161" time="84.5" type="appl" />
         <tli id="T162" time="85.0" type="appl" />
         <tli id="T163" time="85.5" type="appl" />
         <tli id="T164" time="86.0" type="appl" />
         <tli id="T165" time="86.5" type="appl" />
         <tli id="T166" time="87.0" type="appl" />
         <tli id="T167" time="87.5" type="appl" />
         <tli id="T168" time="88.0" type="appl" />
         <tli id="T169" time="88.5" type="appl" />
         <tli id="T170" time="89.0" type="appl" />
         <tli id="T171" time="89.5" type="appl" />
         <tli id="T172" time="90.0" type="appl" />
         <tli id="T173" time="90.5" type="appl" />
         <tli id="T174" time="91.0" type="appl" />
         <tli id="T175" time="91.5" type="appl" />
         <tli id="T176" time="92.0" type="appl" />
         <tli id="T177" time="92.5" type="appl" />
         <tli id="T178" time="93.0" type="appl" />
         <tli id="T179" time="93.5" type="appl" />
         <tli id="T180" time="94.0" type="appl" />
         <tli id="T181" time="94.5" type="appl" />
         <tli id="T182" time="95.0" type="appl" />
         <tli id="T183" time="95.5" type="appl" />
         <tli id="T184" time="96.0" type="appl" />
         <tli id="T185" time="96.5" type="appl" />
         <tli id="T186" time="97.0" type="appl" />
         <tli id="T187" time="97.5" type="appl" />
         <tli id="T188" time="98.0" type="appl" />
         <tli id="T189" time="98.5" type="appl" />
         <tli id="T190" time="99.0" type="appl" />
         <tli id="T191" time="99.5" type="appl" />
         <tli id="T192" time="100.0" type="appl" />
         <tli id="T193" time="100.5" type="appl" />
         <tli id="T194" time="101.0" type="appl" />
         <tli id="T195" time="101.5" type="appl" />
         <tli id="T196" time="102.0" type="appl" />
         <tli id="T197" time="102.5" type="appl" />
         <tli id="T198" time="103.0" type="appl" />
         <tli id="T199" time="103.5" type="appl" />
         <tli id="T200" time="104.0" type="appl" />
         <tli id="T201" time="104.5" type="appl" />
         <tli id="T202" time="105.0" type="appl" />
         <tli id="T203" time="105.5" type="appl" />
         <tli id="T204" time="106.0" type="appl" />
         <tli id="T205" time="106.5" type="appl" />
         <tli id="T206" time="107.0" type="appl" />
         <tli id="T207" time="107.5" type="appl" />
         <tli id="T208" time="108.0" type="appl" />
         <tli id="T209" time="108.5" type="appl" />
         <tli id="T210" time="109.0" type="appl" />
         <tli id="T211" time="109.5" type="appl" />
         <tli id="T212" time="110.0" type="appl" />
         <tli id="T213" time="110.5" type="appl" />
         <tli id="T214" time="111.0" type="appl" />
         <tli id="T215" time="111.5" type="appl" />
         <tli id="T216" time="112.0" type="appl" />
         <tli id="T217" time="112.5" type="appl" />
         <tli id="T218" time="113.0" type="appl" />
         <tli id="T219" time="113.5" type="appl" />
         <tli id="T220" time="114.0" type="appl" />
         <tli id="T221" time="114.5" type="appl" />
         <tli id="T222" time="115.0" type="appl" />
         <tli id="T223" time="115.5" type="appl" />
         <tli id="T224" time="116.0" type="appl" />
         <tli id="T225" time="116.5" type="appl" />
         <tli id="T226" time="117.0" type="appl" />
         <tli id="T227" time="117.5" type="appl" />
         <tli id="T228" time="118.0" type="appl" />
         <tli id="T229" time="118.5" type="appl" />
         <tli id="T230" time="119.0" type="appl" />
         <tli id="T231" time="119.5" type="appl" />
         <tli id="T232" time="120.0" type="appl" />
         <tli id="T233" time="120.5" type="appl" />
         <tli id="T234" time="121.0" type="appl" />
         <tli id="T235" time="121.5" type="appl" />
         <tli id="T236" time="122.0" type="appl" />
         <tli id="T237" time="122.5" type="appl" />
         <tli id="T238" time="123.0" type="appl" />
         <tli id="T239" time="123.5" type="appl" />
         <tli id="T240" time="124.0" type="appl" />
         <tli id="T241" time="124.5" type="appl" />
         <tli id="T242" time="125.0" type="appl" />
         <tli id="T243" time="125.5" type="appl" />
         <tli id="T244" time="126.0" type="appl" />
         <tli id="T245" time="126.5" type="appl" />
         <tli id="T246" time="127.0" type="appl" />
         <tli id="T247" time="127.5" type="appl" />
         <tli id="T248" time="128.0" type="appl" />
         <tli id="T249" time="128.5" type="appl" />
         <tli id="T250" time="129.0" type="appl" />
         <tli id="T251" time="129.5" type="appl" />
         <tli id="T252" time="130.0" type="appl" />
         <tli id="T253" time="130.5" type="appl" />
         <tli id="T254" time="131.0" type="appl" />
         <tli id="T255" time="131.5" type="appl" />
         <tli id="T256" time="132.0" type="appl" />
         <tli id="T257" time="132.5" type="appl" />
         <tli id="T258" time="133.0" type="appl" />
         <tli id="T259" time="133.5" type="appl" />
         <tli id="T260" time="134.0" type="appl" />
         <tli id="T261" time="134.5" type="appl" />
         <tli id="T262" time="135.0" type="appl" />
         <tli id="T263" time="135.5" type="appl" />
         <tli id="T264" time="136.0" type="appl" />
         <tli id="T265" time="136.5" type="appl" />
         <tli id="T266" time="137.0" type="appl" />
         <tli id="T267" time="137.5" type="appl" />
         <tli id="T268" time="138.0" type="appl" />
         <tli id="T269" time="138.5" type="appl" />
         <tli id="T270" time="139.0" type="appl" />
         <tli id="T271" time="139.5" type="appl" />
         <tli id="T272" time="140.0" type="appl" />
         <tli id="T273" time="140.5" type="appl" />
         <tli id="T274" time="141.0" type="appl" />
         <tli id="T275" time="141.5" type="appl" />
         <tli id="T276" time="142.0" type="appl" />
         <tli id="T277" time="142.5" type="appl" />
         <tli id="T278" time="143.0" type="appl" />
         <tli id="T279" time="143.5" type="appl" />
         <tli id="T280" time="144.0" type="appl" />
         <tli id="T281" time="144.5" type="appl" />
         <tli id="T282" time="145.0" type="appl" />
         <tli id="T283" time="145.5" type="appl" />
         <tli id="T284" time="146.0" type="appl" />
         <tli id="T285" time="146.5" type="appl" />
         <tli id="T286" time="147.0" type="appl" />
         <tli id="T287" time="147.5" type="appl" />
         <tli id="T288" time="148.0" type="appl" />
         <tli id="T289" time="148.5" type="appl" />
         <tli id="T290" time="149.0" type="appl" />
         <tli id="T291" time="149.5" type="appl" />
         <tli id="T292" time="150.0" type="appl" />
         <tli id="T293" time="150.5" type="appl" />
         <tli id="T294" time="151.0" type="appl" />
         <tli id="T295" time="151.5" type="appl" />
         <tli id="T296" time="152.0" type="appl" />
         <tli id="T297" time="152.5" type="appl" />
         <tli id="T298" time="153.0" type="appl" />
         <tli id="T299" time="153.5" type="appl" />
         <tli id="T300" time="154.0" type="appl" />
         <tli id="T301" time="154.5" type="appl" />
         <tli id="T302" time="155.0" type="appl" />
         <tli id="T303" time="155.5" type="appl" />
         <tli id="T304" time="156.0" type="appl" />
         <tli id="T305" time="156.5" type="appl" />
         <tli id="T306" time="157.0" type="appl" />
         <tli id="T307" time="157.5" type="appl" />
         <tli id="T308" time="158.0" type="appl" />
         <tli id="T309" time="158.5" type="appl" />
         <tli id="T310" time="159.0" type="appl" />
         <tli id="T311" time="159.5" type="appl" />
         <tli id="T312" time="160.0" type="appl" />
         <tli id="T313" time="160.5" type="appl" />
         <tli id="T314" time="161.0" type="appl" />
         <tli id="T315" time="161.5" type="appl" />
         <tli id="T316" time="162.0" type="appl" />
         <tli id="T317" time="162.5" type="appl" />
         <tli id="T318" time="163.0" type="appl" />
         <tli id="T319" time="163.5" type="appl" />
         <tli id="T320" time="164.0" type="appl" />
         <tli id="T321" time="164.5" type="appl" />
         <tli id="T322" time="165.0" type="appl" />
         <tli id="T323" time="165.5" type="appl" />
         <tli id="T324" time="166.0" type="appl" />
         <tli id="T325" time="166.5" type="appl" />
         <tli id="T326" time="167.0" type="appl" />
         <tli id="T327" time="167.5" type="appl" />
         <tli id="T328" time="168.0" type="appl" />
         <tli id="T329" time="168.5" type="appl" />
         <tli id="T330" time="169.0" type="appl" />
         <tli id="T331" time="169.5" type="appl" />
         <tli id="T332" time="170.0" type="appl" />
         <tli id="T333" time="170.5" type="appl" />
         <tli id="T334" time="171.0" type="appl" />
         <tli id="T335" time="171.5" type="appl" />
         <tli id="T336" time="172.0" type="appl" />
         <tli id="T337" time="172.5" type="appl" />
         <tli id="T338" time="173.0" type="appl" />
         <tli id="T339" time="173.5" type="appl" />
         <tli id="T340" time="174.0" type="appl" />
         <tli id="T341" time="174.5" type="appl" />
         <tli id="T342" time="175.0" type="appl" />
         <tli id="T343" time="175.5" type="appl" />
         <tli id="T344" time="176.0" type="appl" />
         <tli id="T345" time="176.5" type="appl" />
         <tli id="T346" time="177.0" type="appl" />
         <tli id="T347" time="177.5" type="appl" />
         <tli id="T348" time="178.0" type="appl" />
         <tli id="T349" time="178.5" type="appl" />
         <tli id="T350" time="179.0" type="appl" />
         <tli id="T351" time="179.5" type="appl" />
         <tli id="T352" time="180.0" type="appl" />
         <tli id="T353" time="180.5" type="appl" />
         <tli id="T354" time="181.0" type="appl" />
         <tli id="T355" time="181.5" type="appl" />
         <tli id="T356" time="182.0" type="appl" />
         <tli id="T357" time="182.5" type="appl" />
         <tli id="T358" time="183.0" type="appl" />
         <tli id="T359" time="183.5" type="appl" />
         <tli id="T360" time="184.0" type="appl" />
         <tli id="T361" time="184.5" type="appl" />
         <tli id="T362" time="185.0" type="appl" />
         <tli id="T363" time="185.5" type="appl" />
         <tli id="T364" time="186.0" type="appl" />
         <tli id="T365" time="186.5" type="appl" />
         <tli id="T366" time="187.0" type="appl" />
         <tli id="T367" time="187.5" type="appl" />
         <tli id="T368" time="188.0" type="appl" />
         <tli id="T369" time="188.5" type="appl" />
         <tli id="T370" time="189.0" type="appl" />
         <tli id="T371" time="189.5" type="appl" />
         <tli id="T372" time="190.0" type="appl" />
         <tli id="T373" time="190.5" type="appl" />
         <tli id="T374" time="191.0" type="appl" />
         <tli id="T375" time="191.5" type="appl" />
         <tli id="T376" time="192.0" type="appl" />
         <tli id="T377" time="192.5" type="appl" />
         <tli id="T378" time="193.0" type="appl" />
         <tli id="T379" time="193.5" type="appl" />
         <tli id="T380" time="194.0" type="appl" />
         <tli id="T381" time="194.5" type="appl" />
         <tli id="T382" time="195.0" type="appl" />
         <tli id="T383" time="195.5" type="appl" />
         <tli id="T384" time="196.0" type="appl" />
         <tli id="T385" time="196.5" type="appl" />
         <tli id="T386" time="197.0" type="appl" />
         <tli id="T387" time="197.5" type="appl" />
         <tli id="T388" time="198.0" type="appl" />
         <tli id="T389" time="198.5" type="appl" />
         <tli id="T390" time="199.0" type="appl" />
         <tli id="T391" time="199.5" type="appl" />
         <tli id="T392" time="200.0" type="appl" />
         <tli id="T393" time="200.5" type="appl" />
         <tli id="T394" time="201.0" type="appl" />
         <tli id="T395" time="201.5" type="appl" />
         <tli id="T396" time="202.0" type="appl" />
         <tli id="T397" time="202.5" type="appl" />
         <tli id="T398" time="203.0" type="appl" />
         <tli id="T399" time="203.5" type="appl" />
         <tli id="T400" time="204.0" type="appl" />
         <tli id="T401" time="204.5" type="appl" />
         <tli id="T402" time="205.0" type="appl" />
         <tli id="T403" time="205.5" type="appl" />
         <tli id="T404" time="206.0" type="appl" />
         <tli id="T405" time="206.5" type="appl" />
         <tli id="T406" time="207.0" type="appl" />
         <tli id="T407" time="207.5" type="appl" />
         <tli id="T408" time="208.0" type="appl" />
         <tli id="T409" time="208.5" type="appl" />
         <tli id="T410" time="209.0" type="appl" />
         <tli id="T411" time="209.5" type="appl" />
         <tli id="T412" time="210.0" type="appl" />
         <tli id="T413" time="210.5" type="appl" />
         <tli id="T414" time="211.0" type="appl" />
         <tli id="T415" time="211.5" type="appl" />
         <tli id="T416" time="212.0" type="appl" />
         <tli id="T417" time="212.5" type="appl" />
         <tli id="T418" time="213.0" type="appl" />
         <tli id="T419" time="213.5" type="appl" />
         <tli id="T420" time="214.0" type="appl" />
         <tli id="T421" time="214.5" type="appl" />
         <tli id="T422" time="215.0" type="appl" />
         <tli id="T423" time="215.5" type="appl" />
         <tli id="T424" time="216.0" type="appl" />
         <tli id="T425" time="216.5" type="appl" />
         <tli id="T426" time="217.0" type="appl" />
         <tli id="T427" time="217.5" type="appl" />
         <tli id="T428" time="218.0" type="appl" />
         <tli id="T429" time="218.5" type="appl" />
         <tli id="T430" time="219.0" type="appl" />
         <tli id="T431" time="219.5" type="appl" />
         <tli id="T432" time="220.0" type="appl" />
         <tli id="T433" time="220.5" type="appl" />
         <tli id="T434" time="221.0" type="appl" />
         <tli id="T435" time="221.5" type="appl" />
         <tli id="T436" time="222.0" type="appl" />
         <tli id="T437" time="222.5" type="appl" />
         <tli id="T438" time="223.0" type="appl" />
         <tli id="T439" time="223.5" type="appl" />
         <tli id="T440" time="224.0" type="appl" />
         <tli id="T441" time="224.5" type="appl" />
         <tli id="T442" time="225.0" type="appl" />
         <tli id="T443" time="225.5" type="appl" />
         <tli id="T444" time="226.0" type="appl" />
         <tli id="T445" time="226.5" type="appl" />
         <tli id="T446" time="227.0" type="appl" />
         <tli id="T447" time="227.5" type="appl" />
         <tli id="T448" time="228.0" type="appl" />
         <tli id="T449" time="228.5" type="appl" />
         <tli id="T450" time="229.0" type="appl" />
         <tli id="T451" time="229.5" type="appl" />
         <tli id="T452" time="230.0" type="appl" />
         <tli id="T453" time="230.5" type="appl" />
         <tli id="T454" time="231.0" type="appl" />
         <tli id="T455" time="231.5" type="appl" />
         <tli id="T456" time="232.0" type="appl" />
         <tli id="T457" time="232.5" type="appl" />
         <tli id="T458" time="233.0" type="appl" />
         <tli id="T459" time="233.5" type="appl" />
         <tli id="T460" time="234.0" type="appl" />
         <tli id="T461" time="234.5" type="appl" />
         <tli id="T462" time="235.0" type="appl" />
         <tli id="T463" time="235.5" type="appl" />
         <tli id="T464" time="236.0" type="appl" />
         <tli id="T465" time="236.5" type="appl" />
         <tli id="T466" time="237.0" type="appl" />
         <tli id="T467" time="237.5" type="appl" />
         <tli id="T468" time="238.0" type="appl" />
         <tli id="T469" time="238.5" type="appl" />
         <tli id="T470" time="239.0" type="appl" />
         <tli id="T471" time="239.5" type="appl" />
         <tli id="T472" time="240.0" type="appl" />
         <tli id="T473" time="240.5" type="appl" />
         <tli id="T474" time="241.0" type="appl" />
         <tli id="T475" time="241.5" type="appl" />
         <tli id="T476" time="242.0" type="appl" />
         <tli id="T477" time="242.5" type="appl" />
         <tli id="T478" time="243.0" type="appl" />
         <tli id="T479" time="243.5" type="appl" />
         <tli id="T480" time="244.0" type="appl" />
         <tli id="T481" time="244.5" type="appl" />
         <tli id="T482" time="245.0" type="appl" />
         <tli id="T483" time="245.5" type="appl" />
         <tli id="T484" time="246.0" type="appl" />
         <tli id="T485" time="246.5" type="appl" />
         <tli id="T486" time="247.0" type="appl" />
         <tli id="T487" time="247.5" type="appl" />
         <tli id="T488" time="248.0" type="appl" />
         <tli id="T489" time="248.5" type="appl" />
         <tli id="T490" time="249.0" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="BeVP"
                      type="t">
         <timeline-fork end="T30" start="T29">
            <tli id="T29.tx.1" />
         </timeline-fork>
         <timeline-fork end="T71" start="T70">
            <tli id="T70.tx.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T490" id="Seg_0" n="sc" s="T0">
               <ts e="T8" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Hogotogun</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">ü͡öskeːbit</ts>
                  <nts id="Seg_8" n="HIAT:ip">,</nts>
                  <nts id="Seg_9" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_11" n="HIAT:w" s="T2">inʼetin-agatɨn</ts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_14" n="HIAT:w" s="T3">öjdöːböt</ts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_17" n="HIAT:w" s="T4">Laːjku</ts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_20" n="HIAT:w" s="T5">dʼi͡en</ts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_23" n="HIAT:w" s="T6">ɨt</ts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_26" n="HIAT:w" s="T7">olorbut</ts>
                  <nts id="Seg_27" n="HIAT:ip">.</nts>
                  <nts id="Seg_28" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T13" id="Seg_30" n="HIAT:u" s="T8">
                  <ts e="T9" id="Seg_32" n="HIAT:w" s="T8">Biːrde</ts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_35" n="HIAT:w" s="T9">kaːma</ts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_38" n="HIAT:w" s="T10">hɨldʼɨbɨt</ts>
                  <nts id="Seg_39" n="HIAT:ip">,</nts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_42" n="HIAT:w" s="T11">kihi</ts>
                  <nts id="Seg_43" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_45" n="HIAT:w" s="T12">kördüː</ts>
                  <nts id="Seg_46" n="HIAT:ip">.</nts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T18" id="Seg_49" n="HIAT:u" s="T13">
                  <ts e="T14" id="Seg_51" n="HIAT:w" s="T13">Ol</ts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_54" n="HIAT:w" s="T14">hɨldʼan</ts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_57" n="HIAT:w" s="T15">körbüt</ts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_60" n="HIAT:w" s="T16">ürdük</ts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_63" n="HIAT:w" s="T17">hu͡opkanɨ</ts>
                  <nts id="Seg_64" n="HIAT:ip">.</nts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T25" id="Seg_67" n="HIAT:u" s="T18">
                  <nts id="Seg_68" n="HIAT:ip">"</nts>
                  <ts e="T19" id="Seg_70" n="HIAT:w" s="T18">Tu͡ok</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_73" n="HIAT:w" s="T19">bu͡olu͡oj</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_76" n="HIAT:w" s="T20">min</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_79" n="HIAT:w" s="T21">itinne</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_82" n="HIAT:w" s="T22">ɨtɨnnakpɨna</ts>
                  <nts id="Seg_83" n="HIAT:ip">"</nts>
                  <nts id="Seg_84" n="HIAT:ip">,</nts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_87" n="HIAT:w" s="T23">dʼiː</ts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_90" n="HIAT:w" s="T24">hanaːbɨt</ts>
                  <nts id="Seg_91" n="HIAT:ip">.</nts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T34" id="Seg_94" n="HIAT:u" s="T25">
                  <ts e="T26" id="Seg_96" n="HIAT:w" s="T25">Hu͡opka</ts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_99" n="HIAT:w" s="T26">ürdüger</ts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_102" n="HIAT:w" s="T27">ɨttan</ts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_105" n="HIAT:w" s="T28">körbüte</ts>
                  <nts id="Seg_106" n="HIAT:ip">,</nts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29.tx.1" id="Seg_109" n="HIAT:w" s="T29">Dʼige</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_112" n="HIAT:w" s="T29.tx.1">baːba</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_115" n="HIAT:w" s="T30">iher</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_118" n="HIAT:w" s="T31">biːrges</ts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_121" n="HIAT:w" s="T32">dʼi͡eki</ts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_124" n="HIAT:w" s="T33">öttütten</ts>
                  <nts id="Seg_125" n="HIAT:ip">.</nts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T40" id="Seg_128" n="HIAT:u" s="T34">
                  <ts e="T35" id="Seg_130" n="HIAT:w" s="T34">Hu͡opka</ts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_133" n="HIAT:w" s="T35">ortotuːgar</ts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_136" n="HIAT:w" s="T36">tijbit</ts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_139" n="HIAT:w" s="T37">da</ts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_142" n="HIAT:w" s="T38">čokčos</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_145" n="HIAT:w" s="T39">gɨmmɨt</ts>
                  <nts id="Seg_146" n="HIAT:ip">.</nts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T45" id="Seg_149" n="HIAT:u" s="T40">
                  <ts e="T41" id="Seg_151" n="HIAT:w" s="T40">Laːjku</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_154" n="HIAT:w" s="T41">kubulgat</ts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_157" n="HIAT:w" s="T42">bagajɨ</ts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_160" n="HIAT:w" s="T43">ete</ts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_163" n="HIAT:w" s="T44">ebit</ts>
                  <nts id="Seg_164" n="HIAT:ip">.</nts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T54" id="Seg_167" n="HIAT:u" s="T45">
                  <ts e="T46" id="Seg_169" n="HIAT:w" s="T45">Hahɨl</ts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_172" n="HIAT:w" s="T46">ogo</ts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_175" n="HIAT:w" s="T47">bu͡olan</ts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_178" n="HIAT:w" s="T48">baran</ts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_181" n="HIAT:w" s="T49">hu͡opka</ts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_184" n="HIAT:w" s="T50">annɨn</ts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_187" n="HIAT:w" s="T51">dʼi͡ek</ts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_190" n="HIAT:w" s="T52">čekerijen</ts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_193" n="HIAT:w" s="T53">kaːlbɨt</ts>
                  <nts id="Seg_194" n="HIAT:ip">.</nts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T57" id="Seg_197" n="HIAT:u" s="T54">
                  <nts id="Seg_198" n="HIAT:ip">"</nts>
                  <ts e="T55" id="Seg_200" n="HIAT:w" s="T54">O</ts>
                  <nts id="Seg_201" n="HIAT:ip">,</nts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_204" n="HIAT:w" s="T55">annʼɨː</ts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_207" n="HIAT:w" s="T56">da</ts>
                  <nts id="Seg_208" n="HIAT:ip">!</nts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T70" id="Seg_211" n="HIAT:u" s="T57">
                  <ts e="T58" id="Seg_213" n="HIAT:w" s="T57">Kajtak</ts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_216" n="HIAT:w" s="T58">ogolonorbun</ts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_219" n="HIAT:w" s="T59">bilbekke</ts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_222" n="HIAT:w" s="T60">kaːlbɨppɨnɨj</ts>
                  <nts id="Seg_223" n="HIAT:ip">"</nts>
                  <nts id="Seg_224" n="HIAT:ip">,</nts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_227" n="HIAT:w" s="T61">dʼi͡ebit</ts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_230" n="HIAT:w" s="T62">da</ts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_233" n="HIAT:w" s="T63">ogonu</ts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_236" n="HIAT:w" s="T64">huːluː</ts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_239" n="HIAT:w" s="T65">tutan</ts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_242" n="HIAT:w" s="T66">baran</ts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_245" n="HIAT:w" s="T67">dʼi͡etiger</ts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_248" n="HIAT:w" s="T68">ildʼe</ts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_251" n="HIAT:w" s="T69">barbɨt</ts>
                  <nts id="Seg_252" n="HIAT:ip">.</nts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T77" id="Seg_255" n="HIAT:u" s="T70">
                  <ts e="T70.tx.1" id="Seg_257" n="HIAT:w" s="T70">Dʼige</ts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_260" n="HIAT:w" s="T70.tx.1">baːba</ts>
                  <nts id="Seg_261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_263" n="HIAT:w" s="T71">ogonnʼorun</ts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_266" n="HIAT:w" s="T72">kɨtta</ts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_269" n="HIAT:w" s="T73">ebe</ts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_272" n="HIAT:w" s="T74">kɨtɨlɨgar</ts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_275" n="HIAT:w" s="T75">oloror</ts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_278" n="HIAT:w" s="T76">ebitter</ts>
                  <nts id="Seg_279" n="HIAT:ip">.</nts>
                  <nts id="Seg_280" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T83" id="Seg_282" n="HIAT:u" s="T77">
                  <ts e="T78" id="Seg_284" n="HIAT:w" s="T77">Ogoto</ts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_287" n="HIAT:w" s="T78">hu͡oktar</ts>
                  <nts id="Seg_288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_290" n="HIAT:w" s="T79">ebit</ts>
                  <nts id="Seg_291" n="HIAT:ip">,</nts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_294" n="HIAT:w" s="T80">hogotok</ts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_297" n="HIAT:w" s="T81">aːku</ts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_300" n="HIAT:w" s="T82">tabalaːktar</ts>
                  <nts id="Seg_301" n="HIAT:ip">.</nts>
                  <nts id="Seg_302" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T88" id="Seg_304" n="HIAT:u" s="T83">
                  <ts e="T84" id="Seg_306" n="HIAT:w" s="T83">Ogoloro</ts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_309" n="HIAT:w" s="T84">künnete</ts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_312" n="HIAT:w" s="T85">ulaːtan</ts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_315" n="HIAT:w" s="T86">ispit</ts>
                  <nts id="Seg_316" n="HIAT:ip">,</nts>
                  <nts id="Seg_317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_319" n="HIAT:w" s="T87">haŋalammɨt</ts>
                  <nts id="Seg_320" n="HIAT:ip">.</nts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T93" id="Seg_323" n="HIAT:u" s="T88">
                  <ts e="T89" id="Seg_325" n="HIAT:w" s="T88">Inʼete</ts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_328" n="HIAT:w" s="T89">bejetitten</ts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_331" n="HIAT:w" s="T90">hukujdaːn</ts>
                  <nts id="Seg_332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_334" n="HIAT:w" s="T91">taŋas</ts>
                  <nts id="Seg_335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_337" n="HIAT:w" s="T92">tikpit</ts>
                  <nts id="Seg_338" n="HIAT:ip">.</nts>
                  <nts id="Seg_339" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T100" id="Seg_341" n="HIAT:u" s="T93">
                  <ts e="T94" id="Seg_343" n="HIAT:w" s="T93">Bu</ts>
                  <nts id="Seg_344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_346" n="HIAT:w" s="T94">ogo</ts>
                  <nts id="Seg_347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_349" n="HIAT:w" s="T95">hüːreliː</ts>
                  <nts id="Seg_350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_352" n="HIAT:w" s="T96">hɨldʼar</ts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_355" n="HIAT:w" s="T97">bu͡olbut</ts>
                  <nts id="Seg_356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_358" n="HIAT:w" s="T98">dʼi͡etin</ts>
                  <nts id="Seg_359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_361" n="HIAT:w" s="T99">attɨgar</ts>
                  <nts id="Seg_362" n="HIAT:ip">.</nts>
                  <nts id="Seg_363" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T106" id="Seg_365" n="HIAT:u" s="T100">
                  <ts e="T101" id="Seg_367" n="HIAT:w" s="T100">Ogonnʼor</ts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_370" n="HIAT:w" s="T101">kün</ts>
                  <nts id="Seg_371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_373" n="HIAT:w" s="T102">aːjɨ</ts>
                  <nts id="Seg_374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_376" n="HIAT:w" s="T103">ilimnener</ts>
                  <nts id="Seg_377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_379" n="HIAT:w" s="T104">ideleːk</ts>
                  <nts id="Seg_380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_382" n="HIAT:w" s="T105">ebit</ts>
                  <nts id="Seg_383" n="HIAT:ip">.</nts>
                  <nts id="Seg_384" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T112" id="Seg_386" n="HIAT:u" s="T106">
                  <ts e="T107" id="Seg_388" n="HIAT:w" s="T106">Bu</ts>
                  <nts id="Seg_389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_391" n="HIAT:w" s="T107">ogonu</ts>
                  <nts id="Seg_392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_394" n="HIAT:w" s="T108">balɨgɨnan</ts>
                  <nts id="Seg_395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_397" n="HIAT:w" s="T109">agaj</ts>
                  <nts id="Seg_398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_400" n="HIAT:w" s="T110">ahatar</ts>
                  <nts id="Seg_401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_403" n="HIAT:w" s="T111">ebitter</ts>
                  <nts id="Seg_404" n="HIAT:ip">.</nts>
                  <nts id="Seg_405" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T115" id="Seg_407" n="HIAT:u" s="T112">
                  <ts e="T113" id="Seg_409" n="HIAT:w" s="T112">Laːjku</ts>
                  <nts id="Seg_410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_412" n="HIAT:w" s="T113">biːrde</ts>
                  <nts id="Seg_413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_415" n="HIAT:w" s="T114">dʼebit</ts>
                  <nts id="Seg_416" n="HIAT:ip">:</nts>
                  <nts id="Seg_417" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T120" id="Seg_419" n="HIAT:u" s="T115">
                  <nts id="Seg_420" n="HIAT:ip">"</nts>
                  <ts e="T116" id="Seg_422" n="HIAT:w" s="T115">Maːma</ts>
                  <nts id="Seg_423" n="HIAT:ip">,</nts>
                  <nts id="Seg_424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_426" n="HIAT:w" s="T116">min</ts>
                  <nts id="Seg_427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_429" n="HIAT:w" s="T117">et</ts>
                  <nts id="Seg_430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_432" n="HIAT:w" s="T118">hi͡ekpin</ts>
                  <nts id="Seg_433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_435" n="HIAT:w" s="T119">bagarabɨn</ts>
                  <nts id="Seg_436" n="HIAT:ip">.</nts>
                  <nts id="Seg_437" n="HIAT:ip">"</nts>
                  <nts id="Seg_438" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T128" id="Seg_440" n="HIAT:u" s="T120">
                  <nts id="Seg_441" n="HIAT:ip">"</nts>
                  <ts e="T121" id="Seg_443" n="HIAT:w" s="T120">Ogonnʼor</ts>
                  <nts id="Seg_444" n="HIAT:ip">,</nts>
                  <nts id="Seg_445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_447" n="HIAT:w" s="T121">če</ts>
                  <nts id="Seg_448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_450" n="HIAT:w" s="T122">olor</ts>
                  <nts id="Seg_451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_453" n="HIAT:w" s="T123">baːngaːjgin</ts>
                  <nts id="Seg_454" n="HIAT:ip">,</nts>
                  <nts id="Seg_455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_457" n="HIAT:w" s="T124">ogo</ts>
                  <nts id="Seg_458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_460" n="HIAT:w" s="T125">et</ts>
                  <nts id="Seg_461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_463" n="HIAT:w" s="T126">hi͡en</ts>
                  <nts id="Seg_464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_466" n="HIAT:w" s="T127">bagarbɨt</ts>
                  <nts id="Seg_467" n="HIAT:ip">.</nts>
                  <nts id="Seg_468" n="HIAT:ip">"</nts>
                  <nts id="Seg_469" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T132" id="Seg_471" n="HIAT:u" s="T128">
                  <ts e="T129" id="Seg_473" n="HIAT:w" s="T128">Ogonnʼor</ts>
                  <nts id="Seg_474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_476" n="HIAT:w" s="T129">tabatɨn</ts>
                  <nts id="Seg_477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_479" n="HIAT:w" s="T130">ölörön</ts>
                  <nts id="Seg_480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_482" n="HIAT:w" s="T131">keːspit</ts>
                  <nts id="Seg_483" n="HIAT:ip">.</nts>
                  <nts id="Seg_484" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T137" id="Seg_486" n="HIAT:u" s="T132">
                  <ts e="T133" id="Seg_488" n="HIAT:w" s="T132">Laːjku</ts>
                  <nts id="Seg_489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_491" n="HIAT:w" s="T133">kubulgat</ts>
                  <nts id="Seg_492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_494" n="HIAT:w" s="T134">bagata</ts>
                  <nts id="Seg_495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_497" n="HIAT:w" s="T135">ulaːtan</ts>
                  <nts id="Seg_498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_500" n="HIAT:w" s="T136">ispit</ts>
                  <nts id="Seg_501" n="HIAT:ip">.</nts>
                  <nts id="Seg_502" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T147" id="Seg_504" n="HIAT:u" s="T137">
                  <nts id="Seg_505" n="HIAT:ip">"</nts>
                  <ts e="T138" id="Seg_507" n="HIAT:w" s="T137">Maːma</ts>
                  <nts id="Seg_508" n="HIAT:ip">,</nts>
                  <nts id="Seg_509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_511" n="HIAT:w" s="T138">togo</ts>
                  <nts id="Seg_512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_514" n="HIAT:w" s="T139">ebe</ts>
                  <nts id="Seg_515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_517" n="HIAT:w" s="T140">oŋu͡or</ts>
                  <nts id="Seg_518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_520" n="HIAT:w" s="T141">tahɨmmappɨt</ts>
                  <nts id="Seg_521" n="HIAT:ip">,</nts>
                  <nts id="Seg_522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_524" n="HIAT:w" s="T142">onno</ts>
                  <nts id="Seg_525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_527" n="HIAT:w" s="T143">kiti͡eme</ts>
                  <nts id="Seg_528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_530" n="HIAT:w" s="T144">bagajɨ</ts>
                  <nts id="Seg_531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_533" n="HIAT:w" s="T145">bɨhɨːlaːk</ts>
                  <nts id="Seg_534" n="HIAT:ip">"</nts>
                  <nts id="Seg_535" n="HIAT:ip">,</nts>
                  <nts id="Seg_536" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_538" n="HIAT:w" s="T146">dʼi͡ebit</ts>
                  <nts id="Seg_539" n="HIAT:ip">.</nts>
                  <nts id="Seg_540" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T153" id="Seg_542" n="HIAT:u" s="T147">
                  <nts id="Seg_543" n="HIAT:ip">"</nts>
                  <ts e="T148" id="Seg_545" n="HIAT:w" s="T147">Kaja</ts>
                  <nts id="Seg_546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_548" n="HIAT:w" s="T148">ogonnʼor</ts>
                  <nts id="Seg_549" n="HIAT:ip">,</nts>
                  <nts id="Seg_550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_552" n="HIAT:w" s="T149">istegin</ts>
                  <nts id="Seg_553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_555" n="HIAT:w" s="T150">duː</ts>
                  <nts id="Seg_556" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_558" n="HIAT:w" s="T151">ogo</ts>
                  <nts id="Seg_559" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_561" n="HIAT:w" s="T152">haŋatɨn</ts>
                  <nts id="Seg_562" n="HIAT:ip">?</nts>
                  <nts id="Seg_563" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T160" id="Seg_565" n="HIAT:u" s="T153">
                  <ts e="T154" id="Seg_567" n="HIAT:w" s="T153">Kirdigi</ts>
                  <nts id="Seg_568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_570" n="HIAT:w" s="T154">gɨnar</ts>
                  <nts id="Seg_571" n="HIAT:ip">,</nts>
                  <nts id="Seg_572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_574" n="HIAT:w" s="T155">kanɨga</ts>
                  <nts id="Seg_575" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_577" n="HIAT:w" s="T156">dʼi͡eri</ts>
                  <nts id="Seg_578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_580" n="HIAT:w" s="T157">biːr</ts>
                  <nts id="Seg_581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_583" n="HIAT:w" s="T158">hirge</ts>
                  <nts id="Seg_584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_586" n="HIAT:w" s="T159">oloru͡okputuj</ts>
                  <nts id="Seg_587" n="HIAT:ip">?</nts>
                  <nts id="Seg_588" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T161" id="Seg_590" n="HIAT:u" s="T160">
                  <ts e="T161" id="Seg_592" n="HIAT:w" s="T160">Köhü͡ök</ts>
                  <nts id="Seg_593" n="HIAT:ip">.</nts>
                  <nts id="Seg_594" n="HIAT:ip">"</nts>
                  <nts id="Seg_595" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T168" id="Seg_597" n="HIAT:u" s="T161">
                  <ts e="T162" id="Seg_599" n="HIAT:w" s="T161">Ogonnʼor</ts>
                  <nts id="Seg_600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_602" n="HIAT:w" s="T162">öčöspökkö</ts>
                  <nts id="Seg_603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_605" n="HIAT:w" s="T163">kihilerin</ts>
                  <nts id="Seg_606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_608" n="HIAT:w" s="T164">oŋu͡or</ts>
                  <nts id="Seg_609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_611" n="HIAT:w" s="T165">tahaːran</ts>
                  <nts id="Seg_612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_614" n="HIAT:w" s="T166">keːspit</ts>
                  <nts id="Seg_615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_617" n="HIAT:w" s="T167">tɨːnnan</ts>
                  <nts id="Seg_618" n="HIAT:ip">.</nts>
                  <nts id="Seg_619" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T172" id="Seg_621" n="HIAT:u" s="T168">
                  <ts e="T169" id="Seg_623" n="HIAT:w" s="T168">Laːjku</ts>
                  <nts id="Seg_624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_626" n="HIAT:w" s="T169">üre-üre</ts>
                  <nts id="Seg_627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_629" n="HIAT:w" s="T170">hüːre</ts>
                  <nts id="Seg_630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_632" n="HIAT:w" s="T171">hɨldʼɨbɨt</ts>
                  <nts id="Seg_633" n="HIAT:ip">.</nts>
                  <nts id="Seg_634" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T182" id="Seg_636" n="HIAT:u" s="T172">
                  <ts e="T173" id="Seg_638" n="HIAT:w" s="T172">Ki͡ehelik</ts>
                  <nts id="Seg_639" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_641" n="HIAT:w" s="T173">tahɨnan</ts>
                  <nts id="Seg_642" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_644" n="HIAT:w" s="T174">bütellerin</ts>
                  <nts id="Seg_645" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_647" n="HIAT:w" s="T175">gɨtta</ts>
                  <nts id="Seg_648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_650" n="HIAT:w" s="T176">bu</ts>
                  <nts id="Seg_651" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_653" n="HIAT:w" s="T177">ogo</ts>
                  <nts id="Seg_654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_656" n="HIAT:w" s="T178">maːmatɨgar</ts>
                  <nts id="Seg_657" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_659" n="HIAT:w" s="T179">kelen</ts>
                  <nts id="Seg_660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_662" n="HIAT:w" s="T180">töhögüger</ts>
                  <nts id="Seg_663" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_665" n="HIAT:w" s="T181">olorbut</ts>
                  <nts id="Seg_666" n="HIAT:ip">.</nts>
                  <nts id="Seg_667" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T186" id="Seg_669" n="HIAT:u" s="T182">
                  <nts id="Seg_670" n="HIAT:ip">"</nts>
                  <ts e="T183" id="Seg_672" n="HIAT:w" s="T182">Maːma</ts>
                  <nts id="Seg_673" n="HIAT:ip">,</nts>
                  <nts id="Seg_674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_676" n="HIAT:w" s="T183">min</ts>
                  <nts id="Seg_677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_679" n="HIAT:w" s="T184">tɨːlanɨ͡akpɨn</ts>
                  <nts id="Seg_680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_682" n="HIAT:w" s="T185">bagarabɨn</ts>
                  <nts id="Seg_683" n="HIAT:ip">.</nts>
                  <nts id="Seg_684" n="HIAT:ip">"</nts>
                  <nts id="Seg_685" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T191" id="Seg_687" n="HIAT:u" s="T186">
                  <nts id="Seg_688" n="HIAT:ip">"</nts>
                  <ts e="T187" id="Seg_690" n="HIAT:w" s="T186">Ol</ts>
                  <nts id="Seg_691" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_693" n="HIAT:w" s="T187">tu͡ogaj</ts>
                  <nts id="Seg_694" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_696" n="HIAT:w" s="T188">emi͡e</ts>
                  <nts id="Seg_697" n="HIAT:ip">"</nts>
                  <nts id="Seg_698" n="HIAT:ip">,</nts>
                  <nts id="Seg_699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_701" n="HIAT:w" s="T189">ogonnʼor</ts>
                  <nts id="Seg_702" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_704" n="HIAT:w" s="T190">kɨːhɨrbɨt</ts>
                  <nts id="Seg_705" n="HIAT:ip">.</nts>
                  <nts id="Seg_706" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T201" id="Seg_708" n="HIAT:u" s="T191">
                  <ts e="T192" id="Seg_710" n="HIAT:w" s="T191">Oloro</ts>
                  <nts id="Seg_711" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_713" n="HIAT:w" s="T192">tühen</ts>
                  <nts id="Seg_714" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_716" n="HIAT:w" s="T193">baran</ts>
                  <nts id="Seg_717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_718" n="HIAT:ip">"</nts>
                  <ts e="T195" id="Seg_720" n="HIAT:w" s="T194">če</ts>
                  <nts id="Seg_721" n="HIAT:ip">,</nts>
                  <nts id="Seg_722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_724" n="HIAT:w" s="T195">ajɨlaːgɨn</ts>
                  <nts id="Seg_725" n="HIAT:ip">,</nts>
                  <nts id="Seg_726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_728" n="HIAT:w" s="T196">tɨːlan</ts>
                  <nts id="Seg_729" n="HIAT:ip">,</nts>
                  <nts id="Seg_730" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_732" n="HIAT:w" s="T197">ɨraːk</ts>
                  <nts id="Seg_733" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_735" n="HIAT:w" s="T198">ire</ts>
                  <nts id="Seg_736" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_738" n="HIAT:w" s="T199">barɨma</ts>
                  <nts id="Seg_739" n="HIAT:ip">,</nts>
                  <nts id="Seg_740" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_742" n="HIAT:w" s="T200">duː</ts>
                  <nts id="Seg_743" n="HIAT:ip">?</nts>
                  <nts id="Seg_744" n="HIAT:ip">"</nts>
                  <nts id="Seg_745" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T211" id="Seg_747" n="HIAT:u" s="T201">
                  <nts id="Seg_748" n="HIAT:ip">"</nts>
                  <ts e="T202" id="Seg_750" n="HIAT:w" s="T201">Hu͡ok</ts>
                  <nts id="Seg_751" n="HIAT:ip">,</nts>
                  <nts id="Seg_752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_754" n="HIAT:w" s="T202">barɨ͡am</ts>
                  <nts id="Seg_755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_757" n="HIAT:w" s="T203">hu͡oga</ts>
                  <nts id="Seg_758" n="HIAT:ip">,</nts>
                  <nts id="Seg_759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_761" n="HIAT:w" s="T204">ebe</ts>
                  <nts id="Seg_762" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_764" n="HIAT:w" s="T205">kɨtɨlɨgar</ts>
                  <nts id="Seg_765" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_767" n="HIAT:w" s="T206">ire</ts>
                  <nts id="Seg_768" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_770" n="HIAT:w" s="T207">hɨldʼɨ͡am</ts>
                  <nts id="Seg_771" n="HIAT:ip">"</nts>
                  <nts id="Seg_772" n="HIAT:ip">,</nts>
                  <nts id="Seg_773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_775" n="HIAT:w" s="T208">dʼi͡ebit</ts>
                  <nts id="Seg_776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_778" n="HIAT:w" s="T209">Laːjku</ts>
                  <nts id="Seg_779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_781" n="HIAT:w" s="T210">kütür</ts>
                  <nts id="Seg_782" n="HIAT:ip">.</nts>
                  <nts id="Seg_783" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T219" id="Seg_785" n="HIAT:u" s="T211">
                  <ts e="T212" id="Seg_787" n="HIAT:w" s="T211">Laːjku</ts>
                  <nts id="Seg_788" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_790" n="HIAT:w" s="T212">tɨːga</ts>
                  <nts id="Seg_791" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_793" n="HIAT:w" s="T213">olorbut</ts>
                  <nts id="Seg_794" n="HIAT:ip">,</nts>
                  <nts id="Seg_795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_797" n="HIAT:w" s="T214">orguːjakaːn</ts>
                  <nts id="Seg_798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_800" n="HIAT:w" s="T215">kɨtɨl</ts>
                  <nts id="Seg_801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_803" n="HIAT:w" s="T216">ustun</ts>
                  <nts id="Seg_804" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_806" n="HIAT:w" s="T217">erdine</ts>
                  <nts id="Seg_807" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_809" n="HIAT:w" s="T218">hɨldʼɨbɨt</ts>
                  <nts id="Seg_810" n="HIAT:ip">.</nts>
                  <nts id="Seg_811" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T228" id="Seg_813" n="HIAT:u" s="T219">
                  <ts e="T220" id="Seg_815" n="HIAT:w" s="T219">Ogonnʼordoːk</ts>
                  <nts id="Seg_816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_818" n="HIAT:w" s="T220">emeːksin</ts>
                  <nts id="Seg_819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_821" n="HIAT:w" s="T221">dʼi͡ege</ts>
                  <nts id="Seg_822" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_824" n="HIAT:w" s="T222">kiːrellerin</ts>
                  <nts id="Seg_825" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_827" n="HIAT:w" s="T223">kɨtta</ts>
                  <nts id="Seg_828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_830" n="HIAT:w" s="T224">ɨraːtan</ts>
                  <nts id="Seg_831" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_833" n="HIAT:w" s="T225">da</ts>
                  <nts id="Seg_834" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_836" n="HIAT:w" s="T226">ɨraːtan</ts>
                  <nts id="Seg_837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_839" n="HIAT:w" s="T227">ispit</ts>
                  <nts id="Seg_840" n="HIAT:ip">.</nts>
                  <nts id="Seg_841" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T237" id="Seg_843" n="HIAT:u" s="T228">
                  <ts e="T229" id="Seg_845" n="HIAT:w" s="T228">Bu</ts>
                  <nts id="Seg_846" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_848" n="HIAT:w" s="T229">ogo</ts>
                  <nts id="Seg_849" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_851" n="HIAT:w" s="T230">ebe</ts>
                  <nts id="Seg_852" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_854" n="HIAT:w" s="T231">ortotugar</ts>
                  <nts id="Seg_855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_857" n="HIAT:w" s="T232">tijbit</ts>
                  <nts id="Seg_858" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_860" n="HIAT:w" s="T233">daganɨ</ts>
                  <nts id="Seg_861" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_863" n="HIAT:w" s="T234">ulakan</ts>
                  <nts id="Seg_864" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_866" n="HIAT:w" s="T235">kihi</ts>
                  <nts id="Seg_867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_869" n="HIAT:w" s="T236">bu͡olbut</ts>
                  <nts id="Seg_870" n="HIAT:ip">.</nts>
                  <nts id="Seg_871" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T242" id="Seg_873" n="HIAT:u" s="T237">
                  <ts e="T238" id="Seg_875" n="HIAT:w" s="T237">Ogonnʼor</ts>
                  <nts id="Seg_876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_878" n="HIAT:w" s="T238">taksɨbɨta</ts>
                  <nts id="Seg_879" n="HIAT:ip">,</nts>
                  <nts id="Seg_880" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_882" n="HIAT:w" s="T239">körbüte</ts>
                  <nts id="Seg_883" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_885" n="HIAT:w" s="T240">u͡ola</ts>
                  <nts id="Seg_886" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_888" n="HIAT:w" s="T241">ɨraːppɨt</ts>
                  <nts id="Seg_889" n="HIAT:ip">:</nts>
                  <nts id="Seg_890" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T245" id="Seg_892" n="HIAT:u" s="T242">
                  <nts id="Seg_893" n="HIAT:ip">"</nts>
                  <ts e="T243" id="Seg_895" n="HIAT:w" s="T242">Kaja</ts>
                  <nts id="Seg_896" n="HIAT:ip">,</nts>
                  <nts id="Seg_897" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_899" n="HIAT:w" s="T243">kajdi͡ek</ts>
                  <nts id="Seg_900" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_902" n="HIAT:w" s="T244">baragɨn</ts>
                  <nts id="Seg_903" n="HIAT:ip">?</nts>
                  <nts id="Seg_904" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T247" id="Seg_906" n="HIAT:u" s="T245">
                  <ts e="T246" id="Seg_908" n="HIAT:w" s="T245">ɨraːtɨma</ts>
                  <nts id="Seg_909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_911" n="HIAT:w" s="T246">dʼiːbin</ts>
                  <nts id="Seg_912" n="HIAT:ip">.</nts>
                  <nts id="Seg_913" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T249" id="Seg_915" n="HIAT:u" s="T247">
                  <ts e="T248" id="Seg_917" n="HIAT:w" s="T247">Uːga</ts>
                  <nts id="Seg_918" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_920" n="HIAT:w" s="T248">tüstüŋ</ts>
                  <nts id="Seg_921" n="HIAT:ip">!</nts>
                  <nts id="Seg_922" n="HIAT:ip">"</nts>
                  <nts id="Seg_923" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T256" id="Seg_925" n="HIAT:u" s="T249">
                  <ts e="T250" id="Seg_927" n="HIAT:w" s="T249">Laːjku</ts>
                  <nts id="Seg_928" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_930" n="HIAT:w" s="T250">ebe</ts>
                  <nts id="Seg_931" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_933" n="HIAT:w" s="T251">ortotugar</ts>
                  <nts id="Seg_934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_936" n="HIAT:w" s="T252">tijen</ts>
                  <nts id="Seg_937" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_939" n="HIAT:w" s="T253">baran</ts>
                  <nts id="Seg_940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_942" n="HIAT:w" s="T254">ügüleːbit</ts>
                  <nts id="Seg_943" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_944" n="HIAT:ip">"</nts>
                  <ts e="T256" id="Seg_946" n="HIAT:w" s="T255">ka-ka</ts>
                  <nts id="Seg_947" n="HIAT:ip">"</nts>
                  <nts id="Seg_948" n="HIAT:ip">.</nts>
                  <nts id="Seg_949" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T262" id="Seg_951" n="HIAT:u" s="T256">
                  <nts id="Seg_952" n="HIAT:ip">"</nts>
                  <ts e="T257" id="Seg_954" n="HIAT:w" s="T256">Min</ts>
                  <nts id="Seg_955" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_957" n="HIAT:w" s="T257">Laːjkubun</ts>
                  <nts id="Seg_958" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_960" n="HIAT:w" s="T258">eːt</ts>
                  <nts id="Seg_961" n="HIAT:ip">,</nts>
                  <nts id="Seg_962" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_964" n="HIAT:w" s="T259">ehigi</ts>
                  <nts id="Seg_965" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_967" n="HIAT:w" s="T260">onu</ts>
                  <nts id="Seg_968" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_970" n="HIAT:w" s="T261">bilbetekkit</ts>
                  <nts id="Seg_971" n="HIAT:ip">!</nts>
                  <nts id="Seg_972" n="HIAT:ip">"</nts>
                  <nts id="Seg_973" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T264" id="Seg_975" n="HIAT:u" s="T262">
                  <ts e="T263" id="Seg_977" n="HIAT:w" s="T262">Laːjku</ts>
                  <nts id="Seg_978" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_980" n="HIAT:w" s="T263">küreːbit</ts>
                  <nts id="Seg_981" n="HIAT:ip">.</nts>
                  <nts id="Seg_982" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T268" id="Seg_984" n="HIAT:u" s="T264">
                  <ts e="T265" id="Seg_986" n="HIAT:w" s="T264">Ogonnʼordoːk</ts>
                  <nts id="Seg_987" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_989" n="HIAT:w" s="T265">emeːksin</ts>
                  <nts id="Seg_990" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_992" n="HIAT:w" s="T266">ölörsö</ts>
                  <nts id="Seg_993" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_995" n="HIAT:w" s="T267">kaːlbɨttar</ts>
                  <nts id="Seg_996" n="HIAT:ip">.</nts>
                  <nts id="Seg_997" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T272" id="Seg_999" n="HIAT:u" s="T268">
                  <ts e="T269" id="Seg_1001" n="HIAT:w" s="T268">Bu</ts>
                  <nts id="Seg_1002" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_1004" n="HIAT:w" s="T269">u͡ol</ts>
                  <nts id="Seg_1005" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_1007" n="HIAT:w" s="T270">kaːman</ts>
                  <nts id="Seg_1008" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_1010" n="HIAT:w" s="T271">ispit</ts>
                  <nts id="Seg_1011" n="HIAT:ip">.</nts>
                  <nts id="Seg_1012" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T281" id="Seg_1014" n="HIAT:u" s="T272">
                  <ts e="T273" id="Seg_1016" n="HIAT:w" s="T272">Töhö</ts>
                  <nts id="Seg_1017" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_1019" n="HIAT:w" s="T273">bu͡olu͡oj</ts>
                  <nts id="Seg_1020" n="HIAT:ip">,</nts>
                  <nts id="Seg_1021" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_1023" n="HIAT:w" s="T274">körbüte</ts>
                  <nts id="Seg_1024" n="HIAT:ip">,</nts>
                  <nts id="Seg_1025" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_1027" n="HIAT:w" s="T275">dʼi͡eler</ts>
                  <nts id="Seg_1028" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_1030" n="HIAT:w" s="T276">turallar</ts>
                  <nts id="Seg_1031" n="HIAT:ip">,</nts>
                  <nts id="Seg_1032" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_1034" n="HIAT:w" s="T277">hɨrgalaːk</ts>
                  <nts id="Seg_1035" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_1037" n="HIAT:w" s="T278">tabalar</ts>
                  <nts id="Seg_1038" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_1040" n="HIAT:w" s="T279">baːjɨlla</ts>
                  <nts id="Seg_1041" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_1043" n="HIAT:w" s="T280">hɨtallar</ts>
                  <nts id="Seg_1044" n="HIAT:ip">.</nts>
                  <nts id="Seg_1045" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T287" id="Seg_1047" n="HIAT:u" s="T281">
                  <ts e="T282" id="Seg_1049" n="HIAT:w" s="T281">Biːr</ts>
                  <nts id="Seg_1050" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_1052" n="HIAT:w" s="T282">dʼi͡ege</ts>
                  <nts id="Seg_1053" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_1055" n="HIAT:w" s="T283">kiːrbite</ts>
                  <nts id="Seg_1056" n="HIAT:ip">,</nts>
                  <nts id="Seg_1057" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_1059" n="HIAT:w" s="T284">hogotok</ts>
                  <nts id="Seg_1060" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_1062" n="HIAT:w" s="T285">dʼaktar</ts>
                  <nts id="Seg_1063" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_1065" n="HIAT:w" s="T286">oloror</ts>
                  <nts id="Seg_1066" n="HIAT:ip">.</nts>
                  <nts id="Seg_1067" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T289" id="Seg_1069" n="HIAT:u" s="T287">
                  <ts e="T288" id="Seg_1071" n="HIAT:w" s="T287">Laːjku</ts>
                  <nts id="Seg_1072" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_1074" n="HIAT:w" s="T288">čaːjdaːbɨt</ts>
                  <nts id="Seg_1075" n="HIAT:ip">.</nts>
                  <nts id="Seg_1076" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T295" id="Seg_1078" n="HIAT:u" s="T289">
                  <nts id="Seg_1079" n="HIAT:ip">"</nts>
                  <ts e="T290" id="Seg_1081" n="HIAT:w" s="T289">Bu</ts>
                  <nts id="Seg_1082" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_1084" n="HIAT:w" s="T290">tuguj</ts>
                  <nts id="Seg_1085" n="HIAT:ip">,</nts>
                  <nts id="Seg_1086" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_1088" n="HIAT:w" s="T291">munnʼak</ts>
                  <nts id="Seg_1089" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_1091" n="HIAT:w" s="T292">duː</ts>
                  <nts id="Seg_1092" n="HIAT:ip">,</nts>
                  <nts id="Seg_1093" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_1095" n="HIAT:w" s="T293">kurum</ts>
                  <nts id="Seg_1096" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1098" n="HIAT:w" s="T294">duː</ts>
                  <nts id="Seg_1099" n="HIAT:ip">?</nts>
                  <nts id="Seg_1100" n="HIAT:ip">"</nts>
                  <nts id="Seg_1101" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T296" id="Seg_1103" n="HIAT:u" s="T295">
                  <nts id="Seg_1104" n="HIAT:ip">"</nts>
                  <ts e="T296" id="Seg_1106" n="HIAT:w" s="T295">Hu͡ok</ts>
                  <nts id="Seg_1107" n="HIAT:ip">.</nts>
                  <nts id="Seg_1108" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T311" id="Seg_1110" n="HIAT:u" s="T296">
                  <ts e="T297" id="Seg_1112" n="HIAT:w" s="T296">Bihigi</ts>
                  <nts id="Seg_1113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_1115" n="HIAT:w" s="T297">kineːspit</ts>
                  <nts id="Seg_1116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_1118" n="HIAT:w" s="T298">biːr</ts>
                  <nts id="Seg_1119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_1121" n="HIAT:w" s="T299">kɨːstaːk</ts>
                  <nts id="Seg_1122" n="HIAT:ip">,</nts>
                  <nts id="Seg_1123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_1125" n="HIAT:w" s="T300">ol</ts>
                  <nts id="Seg_1126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_1128" n="HIAT:w" s="T301">kɨːska</ts>
                  <nts id="Seg_1129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1131" n="HIAT:w" s="T302">kineːs</ts>
                  <nts id="Seg_1132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_1134" n="HIAT:w" s="T303">taːbɨrɨnɨn</ts>
                  <nts id="Seg_1135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_1137" n="HIAT:w" s="T304">taːjbɨt</ts>
                  <nts id="Seg_1138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_1140" n="HIAT:w" s="T305">kihi</ts>
                  <nts id="Seg_1141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_1143" n="HIAT:w" s="T306">ire</ts>
                  <nts id="Seg_1144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_1146" n="HIAT:w" s="T307">er</ts>
                  <nts id="Seg_1147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1149" n="HIAT:w" s="T308">bu͡olar</ts>
                  <nts id="Seg_1150" n="HIAT:ip">"</nts>
                  <nts id="Seg_1151" n="HIAT:ip">,</nts>
                  <nts id="Seg_1152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_1154" n="HIAT:w" s="T309">dʼi͡ebit</ts>
                  <nts id="Seg_1155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_1157" n="HIAT:w" s="T310">dʼaktar</ts>
                  <nts id="Seg_1158" n="HIAT:ip">.</nts>
                  <nts id="Seg_1159" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T321" id="Seg_1161" n="HIAT:u" s="T311">
                  <ts e="T312" id="Seg_1163" n="HIAT:w" s="T311">Laːjku</ts>
                  <nts id="Seg_1164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_1166" n="HIAT:w" s="T312">taksɨbɨt</ts>
                  <nts id="Seg_1167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_1169" n="HIAT:w" s="T313">da</ts>
                  <nts id="Seg_1170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_1172" n="HIAT:w" s="T314">ebe</ts>
                  <nts id="Seg_1173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1175" n="HIAT:w" s="T315">kɨrɨːtɨgar</ts>
                  <nts id="Seg_1176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_1178" n="HIAT:w" s="T316">kiːrbit</ts>
                  <nts id="Seg_1179" n="HIAT:ip">,</nts>
                  <nts id="Seg_1180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_1182" n="HIAT:w" s="T317">otunan</ts>
                  <nts id="Seg_1183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1185" n="HIAT:w" s="T318">haptan</ts>
                  <nts id="Seg_1186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_1188" n="HIAT:w" s="T319">baran</ts>
                  <nts id="Seg_1189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1191" n="HIAT:w" s="T320">hɨppɨt</ts>
                  <nts id="Seg_1192" n="HIAT:ip">.</nts>
                  <nts id="Seg_1193" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T332" id="Seg_1195" n="HIAT:u" s="T321">
                  <ts e="T322" id="Seg_1197" n="HIAT:w" s="T321">Körbüte</ts>
                  <nts id="Seg_1198" n="HIAT:ip">,</nts>
                  <nts id="Seg_1199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1201" n="HIAT:w" s="T322">ikki</ts>
                  <nts id="Seg_1202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_1204" n="HIAT:w" s="T323">kɨːs</ts>
                  <nts id="Seg_1205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1207" n="HIAT:w" s="T324">uː</ts>
                  <nts id="Seg_1208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_1210" n="HIAT:w" s="T325">baha</ts>
                  <nts id="Seg_1211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1213" n="HIAT:w" s="T326">iheller</ts>
                  <nts id="Seg_1214" n="HIAT:ip">,</nts>
                  <nts id="Seg_1215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_1217" n="HIAT:w" s="T327">orguhu͡oktanan</ts>
                  <nts id="Seg_1218" n="HIAT:ip">,</nts>
                  <nts id="Seg_1219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_1221" n="HIAT:w" s="T328">ikki</ts>
                  <nts id="Seg_1222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_1224" n="HIAT:w" s="T329">baːgɨ</ts>
                  <nts id="Seg_1225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_1227" n="HIAT:w" s="T330">hannɨlarɨgar</ts>
                  <nts id="Seg_1228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_1230" n="HIAT:w" s="T331">hükpütter</ts>
                  <nts id="Seg_1231" n="HIAT:ip">.</nts>
                  <nts id="Seg_1232" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T339" id="Seg_1234" n="HIAT:u" s="T332">
                  <ts e="T333" id="Seg_1236" n="HIAT:w" s="T332">U͡ol</ts>
                  <nts id="Seg_1237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_1239" n="HIAT:w" s="T333">atagɨttan</ts>
                  <nts id="Seg_1240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_1242" n="HIAT:w" s="T334">baːj</ts>
                  <nts id="Seg_1243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_1245" n="HIAT:w" s="T335">kɨːs</ts>
                  <nts id="Seg_1246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_1248" n="HIAT:w" s="T336">iŋŋen</ts>
                  <nts id="Seg_1249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_1251" n="HIAT:w" s="T337">uːtun</ts>
                  <nts id="Seg_1252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_1254" n="HIAT:w" s="T338">dʼalkɨppɨt</ts>
                  <nts id="Seg_1255" n="HIAT:ip">:</nts>
                  <nts id="Seg_1256" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T353" id="Seg_1258" n="HIAT:u" s="T339">
                  <nts id="Seg_1259" n="HIAT:ip">"</nts>
                  <ts e="T340" id="Seg_1261" n="HIAT:w" s="T339">Bu</ts>
                  <nts id="Seg_1262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_1264" n="HIAT:w" s="T340">barɨta</ts>
                  <nts id="Seg_1265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_1267" n="HIAT:w" s="T341">agam</ts>
                  <nts id="Seg_1268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1270" n="HIAT:w" s="T342">ihin</ts>
                  <nts id="Seg_1271" n="HIAT:ip">,</nts>
                  <nts id="Seg_1272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_1274" n="HIAT:w" s="T343">erge</ts>
                  <nts id="Seg_1275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_1277" n="HIAT:w" s="T344">taksɨbɨt</ts>
                  <nts id="Seg_1278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_1280" n="HIAT:w" s="T345">ebitim</ts>
                  <nts id="Seg_1281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_1283" n="HIAT:w" s="T346">bu͡ollar</ts>
                  <nts id="Seg_1284" n="HIAT:ip">,</nts>
                  <nts id="Seg_1285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1287" n="HIAT:w" s="T347">uː</ts>
                  <nts id="Seg_1288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_1290" n="HIAT:w" s="T348">baha</ts>
                  <nts id="Seg_1291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_1293" n="HIAT:w" s="T349">erejdene</ts>
                  <nts id="Seg_1294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_1296" n="HIAT:w" s="T350">hɨldʼɨ͡ak</ts>
                  <nts id="Seg_1297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_1299" n="HIAT:w" s="T351">etim</ts>
                  <nts id="Seg_1300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_1302" n="HIAT:w" s="T352">duː</ts>
                  <nts id="Seg_1303" n="HIAT:ip">?</nts>
                  <nts id="Seg_1304" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T356" id="Seg_1306" n="HIAT:u" s="T353">
                  <ts e="T354" id="Seg_1308" n="HIAT:w" s="T353">Atakpɨn</ts>
                  <nts id="Seg_1309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_1311" n="HIAT:w" s="T354">ire</ts>
                  <nts id="Seg_1312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_1314" n="HIAT:w" s="T355">ilittim</ts>
                  <nts id="Seg_1315" n="HIAT:ip">.</nts>
                  <nts id="Seg_1316" n="HIAT:ip">"</nts>
                  <nts id="Seg_1317" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T361" id="Seg_1319" n="HIAT:u" s="T356">
                  <nts id="Seg_1320" n="HIAT:ip">"</nts>
                  <ts e="T357" id="Seg_1322" n="HIAT:w" s="T356">Dogoː</ts>
                  <nts id="Seg_1323" n="HIAT:ip">,</nts>
                  <nts id="Seg_1324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_1326" n="HIAT:w" s="T357">iti</ts>
                  <nts id="Seg_1327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_1329" n="HIAT:w" s="T358">teːteŋ</ts>
                  <nts id="Seg_1330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_1332" n="HIAT:w" s="T359">tugu</ts>
                  <nts id="Seg_1333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_1335" n="HIAT:w" s="T360">taːttarar</ts>
                  <nts id="Seg_1336" n="HIAT:ip">?</nts>
                  <nts id="Seg_1337" n="HIAT:ip">"</nts>
                  <nts id="Seg_1338" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T370" id="Seg_1340" n="HIAT:u" s="T361">
                  <nts id="Seg_1341" n="HIAT:ip">"</nts>
                  <ts e="T362" id="Seg_1343" n="HIAT:w" s="T361">Agam</ts>
                  <nts id="Seg_1344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_1346" n="HIAT:w" s="T362">kahan</ts>
                  <nts id="Seg_1347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_1349" n="HIAT:w" s="T363">ire</ts>
                  <nts id="Seg_1350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_1352" n="HIAT:w" s="T364">hette</ts>
                  <nts id="Seg_1353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_1355" n="HIAT:w" s="T365">bɨt</ts>
                  <nts id="Seg_1356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_1358" n="HIAT:w" s="T366">tiriːtinen</ts>
                  <nts id="Seg_1359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_1361" n="HIAT:w" s="T367">dahɨna</ts>
                  <nts id="Seg_1362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_1364" n="HIAT:w" s="T368">oŋorbut</ts>
                  <nts id="Seg_1365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_1367" n="HIAT:w" s="T369">ete</ts>
                  <nts id="Seg_1368" n="HIAT:ip">.</nts>
                  <nts id="Seg_1369" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T374" id="Seg_1371" n="HIAT:u" s="T370">
                  <ts e="T371" id="Seg_1373" n="HIAT:w" s="T370">Itini</ts>
                  <nts id="Seg_1374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_1376" n="HIAT:w" s="T371">gɨnar</ts>
                  <nts id="Seg_1377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_1379" n="HIAT:w" s="T372">ebite</ts>
                  <nts id="Seg_1380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_1382" n="HIAT:w" s="T373">duː</ts>
                  <nts id="Seg_1383" n="HIAT:ip">?</nts>
                  <nts id="Seg_1384" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T376" id="Seg_1386" n="HIAT:u" s="T374">
                  <ts e="T375" id="Seg_1388" n="HIAT:w" s="T374">Če</ts>
                  <nts id="Seg_1389" n="HIAT:ip">,</nts>
                  <nts id="Seg_1390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_1392" n="HIAT:w" s="T375">barɨ͡ak</ts>
                  <nts id="Seg_1393" n="HIAT:ip">.</nts>
                  <nts id="Seg_1394" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T384" id="Seg_1396" n="HIAT:u" s="T376">
                  <ts e="T377" id="Seg_1398" n="HIAT:w" s="T376">Uː</ts>
                  <nts id="Seg_1399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378" id="Seg_1401" n="HIAT:w" s="T377">da</ts>
                  <nts id="Seg_1402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_1404" n="HIAT:w" s="T378">tulujbat</ts>
                  <nts id="Seg_1405" n="HIAT:ip">,</nts>
                  <nts id="Seg_1406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_1408" n="HIAT:w" s="T379">mas</ts>
                  <nts id="Seg_1409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_1411" n="HIAT:w" s="T380">da</ts>
                  <nts id="Seg_1412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T382" id="Seg_1414" n="HIAT:w" s="T381">tulujbat</ts>
                  <nts id="Seg_1415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_1417" n="HIAT:w" s="T382">baččalaːk</ts>
                  <nts id="Seg_1418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_1420" n="HIAT:w" s="T383">ɨ͡aldʼɨkka</ts>
                  <nts id="Seg_1421" n="HIAT:ip">.</nts>
                  <nts id="Seg_1422" n="HIAT:ip">"</nts>
                  <nts id="Seg_1423" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T391" id="Seg_1425" n="HIAT:u" s="T384">
                  <ts e="T385" id="Seg_1427" n="HIAT:w" s="T384">Laːjku</ts>
                  <nts id="Seg_1428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T386" id="Seg_1430" n="HIAT:w" s="T385">tönnön</ts>
                  <nts id="Seg_1431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_1433" n="HIAT:w" s="T386">kelen</ts>
                  <nts id="Seg_1434" n="HIAT:ip">,</nts>
                  <nts id="Seg_1435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_1437" n="HIAT:w" s="T387">maː</ts>
                  <nts id="Seg_1438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_1440" n="HIAT:w" s="T388">dʼi͡etiger</ts>
                  <nts id="Seg_1441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_1443" n="HIAT:w" s="T389">kiːrbit</ts>
                  <nts id="Seg_1444" n="HIAT:ip">,</nts>
                  <nts id="Seg_1445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T391" id="Seg_1447" n="HIAT:w" s="T390">kommut</ts>
                  <nts id="Seg_1448" n="HIAT:ip">.</nts>
                  <nts id="Seg_1449" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T396" id="Seg_1451" n="HIAT:u" s="T391">
                  <ts e="T392" id="Seg_1453" n="HIAT:w" s="T391">Harsi͡erde</ts>
                  <nts id="Seg_1454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T393" id="Seg_1456" n="HIAT:w" s="T392">turbuta</ts>
                  <nts id="Seg_1457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1458" n="HIAT:ip">—</nts>
                  <nts id="Seg_1459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_1461" n="HIAT:w" s="T393">emi͡e</ts>
                  <nts id="Seg_1462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_1464" n="HIAT:w" s="T394">ɨ͡aldʼɨt</ts>
                  <nts id="Seg_1465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T396" id="Seg_1467" n="HIAT:w" s="T395">toloru</ts>
                  <nts id="Seg_1468" n="HIAT:ip">.</nts>
                  <nts id="Seg_1469" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T405" id="Seg_1471" n="HIAT:u" s="T396">
                  <ts e="T397" id="Seg_1473" n="HIAT:w" s="T396">Bu</ts>
                  <nts id="Seg_1474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_1476" n="HIAT:w" s="T397">u͡ol</ts>
                  <nts id="Seg_1477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_1479" n="HIAT:w" s="T398">emi͡e</ts>
                  <nts id="Seg_1480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T400" id="Seg_1482" n="HIAT:w" s="T399">kineːske</ts>
                  <nts id="Seg_1483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_1485" n="HIAT:w" s="T400">barɨːhɨ</ts>
                  <nts id="Seg_1486" n="HIAT:ip">,</nts>
                  <nts id="Seg_1487" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T402" id="Seg_1489" n="HIAT:w" s="T401">ɨ͡aldʼɨttarɨ</ts>
                  <nts id="Seg_1490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_1492" n="HIAT:w" s="T402">kɨtta</ts>
                  <nts id="Seg_1493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T404" id="Seg_1495" n="HIAT:w" s="T403">biːrge</ts>
                  <nts id="Seg_1496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T405" id="Seg_1498" n="HIAT:w" s="T404">kiːrbit</ts>
                  <nts id="Seg_1499" n="HIAT:ip">.</nts>
                  <nts id="Seg_1500" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T410" id="Seg_1502" n="HIAT:u" s="T405">
                  <ts e="T406" id="Seg_1504" n="HIAT:w" s="T405">Körbüte</ts>
                  <nts id="Seg_1505" n="HIAT:ip">,</nts>
                  <nts id="Seg_1506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T407" id="Seg_1508" n="HIAT:w" s="T406">dʼi͡eni</ts>
                  <nts id="Seg_1509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T408" id="Seg_1511" n="HIAT:w" s="T407">toloru</ts>
                  <nts id="Seg_1512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T409" id="Seg_1514" n="HIAT:w" s="T408">er</ts>
                  <nts id="Seg_1515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_1517" n="HIAT:w" s="T409">kihi</ts>
                  <nts id="Seg_1518" n="HIAT:ip">.</nts>
                  <nts id="Seg_1519" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T415" id="Seg_1521" n="HIAT:u" s="T410">
                  <nts id="Seg_1522" n="HIAT:ip">"</nts>
                  <ts e="T411" id="Seg_1524" n="HIAT:w" s="T410">O</ts>
                  <nts id="Seg_1525" n="HIAT:ip">,</nts>
                  <nts id="Seg_1526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T412" id="Seg_1528" n="HIAT:w" s="T411">bügün</ts>
                  <nts id="Seg_1529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T413" id="Seg_1531" n="HIAT:w" s="T412">haŋa</ts>
                  <nts id="Seg_1532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414" id="Seg_1534" n="HIAT:w" s="T413">ɨ͡aldʼɨt</ts>
                  <nts id="Seg_1535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T415" id="Seg_1537" n="HIAT:w" s="T414">kelbit</ts>
                  <nts id="Seg_1538" n="HIAT:ip">.</nts>
                  <nts id="Seg_1539" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T423" id="Seg_1541" n="HIAT:u" s="T415">
                  <ts e="T416" id="Seg_1543" n="HIAT:w" s="T415">Če</ts>
                  <nts id="Seg_1544" n="HIAT:ip">,</nts>
                  <nts id="Seg_1545" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T417" id="Seg_1547" n="HIAT:w" s="T416">bagar</ts>
                  <nts id="Seg_1548" n="HIAT:ip">,</nts>
                  <nts id="Seg_1549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T418" id="Seg_1551" n="HIAT:w" s="T417">en</ts>
                  <nts id="Seg_1552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T419" id="Seg_1554" n="HIAT:w" s="T418">min</ts>
                  <nts id="Seg_1555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T420" id="Seg_1557" n="HIAT:w" s="T419">taːbɨrɨmmɨn</ts>
                  <nts id="Seg_1558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_1560" n="HIAT:w" s="T420">taːjɨ͡aŋ</ts>
                  <nts id="Seg_1561" n="HIAT:ip">"</nts>
                  <nts id="Seg_1562" n="HIAT:ip">,</nts>
                  <nts id="Seg_1563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T422" id="Seg_1565" n="HIAT:w" s="T421">di͡ebit</ts>
                  <nts id="Seg_1566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T423" id="Seg_1568" n="HIAT:w" s="T422">kineːs</ts>
                  <nts id="Seg_1569" n="HIAT:ip">.</nts>
                  <nts id="Seg_1570" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T425" id="Seg_1572" n="HIAT:u" s="T423">
                  <nts id="Seg_1573" n="HIAT:ip">"</nts>
                  <ts e="T424" id="Seg_1575" n="HIAT:w" s="T423">Tu͡ok</ts>
                  <nts id="Seg_1576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T425" id="Seg_1578" n="HIAT:w" s="T424">taːbɨrɨnnaːkkɨnɨj</ts>
                  <nts id="Seg_1579" n="HIAT:ip">?</nts>
                  <nts id="Seg_1580" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T433" id="Seg_1582" n="HIAT:u" s="T425">
                  <ts e="T426" id="Seg_1584" n="HIAT:w" s="T425">Töhö</ts>
                  <nts id="Seg_1585" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T427" id="Seg_1587" n="HIAT:w" s="T426">öjüm</ts>
                  <nts id="Seg_1588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T428" id="Seg_1590" n="HIAT:w" s="T427">kotorunan</ts>
                  <nts id="Seg_1591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T429" id="Seg_1593" n="HIAT:w" s="T428">taːja</ts>
                  <nts id="Seg_1594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_1596" n="HIAT:w" s="T429">hatɨ͡ak</ts>
                  <nts id="Seg_1597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T431" id="Seg_1599" n="HIAT:w" s="T430">etim</ts>
                  <nts id="Seg_1600" n="HIAT:ip">"</nts>
                  <nts id="Seg_1601" n="HIAT:ip">,</nts>
                  <nts id="Seg_1602" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_1604" n="HIAT:w" s="T431">Laːjku</ts>
                  <nts id="Seg_1605" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T433" id="Seg_1607" n="HIAT:w" s="T432">dʼebit</ts>
                  <nts id="Seg_1608" n="HIAT:ip">.</nts>
                  <nts id="Seg_1609" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T441" id="Seg_1611" n="HIAT:u" s="T433">
                  <nts id="Seg_1612" n="HIAT:ip">"</nts>
                  <ts e="T434" id="Seg_1614" n="HIAT:w" s="T433">Min</ts>
                  <nts id="Seg_1615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T435" id="Seg_1617" n="HIAT:w" s="T434">aːttarabɨn</ts>
                  <nts id="Seg_1618" n="HIAT:ip">,</nts>
                  <nts id="Seg_1619" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_1621" n="HIAT:w" s="T435">dahɨna</ts>
                  <nts id="Seg_1622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T437" id="Seg_1624" n="HIAT:w" s="T436">tugunan</ts>
                  <nts id="Seg_1625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T438" id="Seg_1627" n="HIAT:w" s="T437">oŋohullubutuj</ts>
                  <nts id="Seg_1628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T439" id="Seg_1630" n="HIAT:w" s="T438">dʼi͡en</ts>
                  <nts id="Seg_1631" n="HIAT:ip">"</nts>
                  <nts id="Seg_1632" n="HIAT:ip">,</nts>
                  <nts id="Seg_1633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440" id="Seg_1635" n="HIAT:w" s="T439">kineːs</ts>
                  <nts id="Seg_1636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_1638" n="HIAT:w" s="T440">ɨjɨppɨt</ts>
                  <nts id="Seg_1639" n="HIAT:ip">.</nts>
                  <nts id="Seg_1640" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T448" id="Seg_1642" n="HIAT:u" s="T441">
                  <nts id="Seg_1643" n="HIAT:ip">"</nts>
                  <ts e="T442" id="Seg_1645" n="HIAT:w" s="T441">Öskötün</ts>
                  <nts id="Seg_1646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T443" id="Seg_1648" n="HIAT:w" s="T442">min</ts>
                  <nts id="Seg_1649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T444" id="Seg_1651" n="HIAT:w" s="T443">öjüm</ts>
                  <nts id="Seg_1652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T445" id="Seg_1654" n="HIAT:w" s="T444">barar</ts>
                  <nts id="Seg_1655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T446" id="Seg_1657" n="HIAT:w" s="T445">hirinen</ts>
                  <nts id="Seg_1658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T447" id="Seg_1660" n="HIAT:w" s="T446">oŋoru͡oŋ</ts>
                  <nts id="Seg_1661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T448" id="Seg_1663" n="HIAT:w" s="T447">duː</ts>
                  <nts id="Seg_1664" n="HIAT:ip">?</nts>
                  <nts id="Seg_1665" n="HIAT:ip">"</nts>
                  <nts id="Seg_1666" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T451" id="Seg_1668" n="HIAT:u" s="T448">
                  <nts id="Seg_1669" n="HIAT:ip">"</nts>
                  <ts e="T449" id="Seg_1671" n="HIAT:w" s="T448">Tiriː</ts>
                  <nts id="Seg_1672" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T450" id="Seg_1674" n="HIAT:w" s="T449">bu͡ollagɨna</ts>
                  <nts id="Seg_1675" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T451" id="Seg_1677" n="HIAT:w" s="T450">bu͡ollun</ts>
                  <nts id="Seg_1678" n="HIAT:ip">.</nts>
                  <nts id="Seg_1679" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T460" id="Seg_1681" n="HIAT:u" s="T451">
                  <ts e="T452" id="Seg_1683" n="HIAT:w" s="T451">Min</ts>
                  <nts id="Seg_1684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T453" id="Seg_1686" n="HIAT:w" s="T452">hanaːbar</ts>
                  <nts id="Seg_1687" n="HIAT:ip">,</nts>
                  <nts id="Seg_1688" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T454" id="Seg_1690" n="HIAT:w" s="T453">hette</ts>
                  <nts id="Seg_1691" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T455" id="Seg_1693" n="HIAT:w" s="T454">bɨt</ts>
                  <nts id="Seg_1694" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T456" id="Seg_1696" n="HIAT:w" s="T455">tiriːtinen</ts>
                  <nts id="Seg_1697" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T457" id="Seg_1699" n="HIAT:w" s="T456">onohullubut</ts>
                  <nts id="Seg_1700" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T458" id="Seg_1702" n="HIAT:w" s="T457">bɨhɨːlaːk</ts>
                  <nts id="Seg_1703" n="HIAT:ip">"</nts>
                  <nts id="Seg_1704" n="HIAT:ip">,</nts>
                  <nts id="Seg_1705" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T459" id="Seg_1707" n="HIAT:w" s="T458">Laːjku</ts>
                  <nts id="Seg_1708" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T460" id="Seg_1710" n="HIAT:w" s="T459">haŋarbɨt</ts>
                  <nts id="Seg_1711" n="HIAT:ip">.</nts>
                  <nts id="Seg_1712" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T464" id="Seg_1714" n="HIAT:u" s="T460">
                  <nts id="Seg_1715" n="HIAT:ip">"</nts>
                  <ts e="T461" id="Seg_1717" n="HIAT:w" s="T460">O</ts>
                  <nts id="Seg_1718" n="HIAT:ip">,</nts>
                  <nts id="Seg_1719" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T462" id="Seg_1721" n="HIAT:w" s="T461">tu͡ok</ts>
                  <nts id="Seg_1722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T463" id="Seg_1724" n="HIAT:w" s="T462">öjdöːk</ts>
                  <nts id="Seg_1725" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T464" id="Seg_1727" n="HIAT:w" s="T463">kihiteginij</ts>
                  <nts id="Seg_1728" n="HIAT:ip">?</nts>
                  <nts id="Seg_1729" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T466" id="Seg_1731" n="HIAT:u" s="T464">
                  <ts e="T465" id="Seg_1733" n="HIAT:w" s="T464">Taːjdɨŋ</ts>
                  <nts id="Seg_1734" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T466" id="Seg_1736" n="HIAT:w" s="T465">dʼiː</ts>
                  <nts id="Seg_1737" n="HIAT:ip">.</nts>
                  <nts id="Seg_1738" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T474" id="Seg_1740" n="HIAT:u" s="T466">
                  <ts e="T467" id="Seg_1742" n="HIAT:w" s="T466">Če</ts>
                  <nts id="Seg_1743" n="HIAT:ip">,</nts>
                  <nts id="Seg_1744" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T468" id="Seg_1746" n="HIAT:w" s="T467">oččogo</ts>
                  <nts id="Seg_1747" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T469" id="Seg_1749" n="HIAT:w" s="T468">min</ts>
                  <nts id="Seg_1750" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T470" id="Seg_1752" n="HIAT:w" s="T469">kɨːspɨn</ts>
                  <nts id="Seg_1753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T471" id="Seg_1755" n="HIAT:w" s="T470">dʼaktar</ts>
                  <nts id="Seg_1756" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T472" id="Seg_1758" n="HIAT:w" s="T471">gɨnar</ts>
                  <nts id="Seg_1759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T473" id="Seg_1761" n="HIAT:w" s="T472">ɨjaːktaːkkɨn</ts>
                  <nts id="Seg_1762" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T474" id="Seg_1764" n="HIAT:w" s="T473">ebit</ts>
                  <nts id="Seg_1765" n="HIAT:ip">.</nts>
                  <nts id="Seg_1766" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T478" id="Seg_1768" n="HIAT:u" s="T474">
                  <ts e="T475" id="Seg_1770" n="HIAT:w" s="T474">Kajdak</ts>
                  <nts id="Seg_1771" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T476" id="Seg_1773" n="HIAT:w" s="T475">gɨnɨ͡aŋɨj</ts>
                  <nts id="Seg_1774" n="HIAT:ip">,</nts>
                  <nts id="Seg_1775" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T477" id="Seg_1777" n="HIAT:w" s="T476">harsɨn</ts>
                  <nts id="Seg_1778" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T478" id="Seg_1780" n="HIAT:w" s="T477">kurumnu͡okput</ts>
                  <nts id="Seg_1781" n="HIAT:ip">.</nts>
                  <nts id="Seg_1782" n="HIAT:ip">"</nts>
                  <nts id="Seg_1783" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T490" id="Seg_1785" n="HIAT:u" s="T478">
                  <ts e="T479" id="Seg_1787" n="HIAT:w" s="T478">Laːjku</ts>
                  <nts id="Seg_1788" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T480" id="Seg_1790" n="HIAT:w" s="T479">baːj</ts>
                  <nts id="Seg_1791" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T481" id="Seg_1793" n="HIAT:w" s="T480">kineːs</ts>
                  <nts id="Seg_1794" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T482" id="Seg_1796" n="HIAT:w" s="T481">bosku͡oj</ts>
                  <nts id="Seg_1797" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T483" id="Seg_1799" n="HIAT:w" s="T482">kɨːhɨn</ts>
                  <nts id="Seg_1800" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T484" id="Seg_1802" n="HIAT:w" s="T483">dʼaktardanan</ts>
                  <nts id="Seg_1803" n="HIAT:ip">,</nts>
                  <nts id="Seg_1804" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T485" id="Seg_1806" n="HIAT:w" s="T484">bajan-toton</ts>
                  <nts id="Seg_1807" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T486" id="Seg_1809" n="HIAT:w" s="T485">olorbut</ts>
                  <nts id="Seg_1810" n="HIAT:ip">,</nts>
                  <nts id="Seg_1811" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T487" id="Seg_1813" n="HIAT:w" s="T486">tu͡ok</ts>
                  <nts id="Seg_1814" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T488" id="Seg_1816" n="HIAT:w" s="T487">da</ts>
                  <nts id="Seg_1817" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T489" id="Seg_1819" n="HIAT:w" s="T488">ereji</ts>
                  <nts id="Seg_1820" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T490" id="Seg_1822" n="HIAT:w" s="T489">bilbekke</ts>
                  <nts id="Seg_1823" n="HIAT:ip">.</nts>
                  <nts id="Seg_1824" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T490" id="Seg_1825" n="sc" s="T0">
               <ts e="T1" id="Seg_1827" n="e" s="T0">Hogotogun </ts>
               <ts e="T2" id="Seg_1829" n="e" s="T1">ü͡öskeːbit, </ts>
               <ts e="T3" id="Seg_1831" n="e" s="T2">inʼetin-agatɨn </ts>
               <ts e="T4" id="Seg_1833" n="e" s="T3">öjdöːböt </ts>
               <ts e="T5" id="Seg_1835" n="e" s="T4">Laːjku </ts>
               <ts e="T6" id="Seg_1837" n="e" s="T5">dʼi͡en </ts>
               <ts e="T7" id="Seg_1839" n="e" s="T6">ɨt </ts>
               <ts e="T8" id="Seg_1841" n="e" s="T7">olorbut. </ts>
               <ts e="T9" id="Seg_1843" n="e" s="T8">Biːrde </ts>
               <ts e="T10" id="Seg_1845" n="e" s="T9">kaːma </ts>
               <ts e="T11" id="Seg_1847" n="e" s="T10">hɨldʼɨbɨt, </ts>
               <ts e="T12" id="Seg_1849" n="e" s="T11">kihi </ts>
               <ts e="T13" id="Seg_1851" n="e" s="T12">kördüː. </ts>
               <ts e="T14" id="Seg_1853" n="e" s="T13">Ol </ts>
               <ts e="T15" id="Seg_1855" n="e" s="T14">hɨldʼan </ts>
               <ts e="T16" id="Seg_1857" n="e" s="T15">körbüt </ts>
               <ts e="T17" id="Seg_1859" n="e" s="T16">ürdük </ts>
               <ts e="T18" id="Seg_1861" n="e" s="T17">hu͡opkanɨ. </ts>
               <ts e="T19" id="Seg_1863" n="e" s="T18">"Tu͡ok </ts>
               <ts e="T20" id="Seg_1865" n="e" s="T19">bu͡olu͡oj </ts>
               <ts e="T21" id="Seg_1867" n="e" s="T20">min </ts>
               <ts e="T22" id="Seg_1869" n="e" s="T21">itinne </ts>
               <ts e="T23" id="Seg_1871" n="e" s="T22">ɨtɨnnakpɨna", </ts>
               <ts e="T24" id="Seg_1873" n="e" s="T23">dʼiː </ts>
               <ts e="T25" id="Seg_1875" n="e" s="T24">hanaːbɨt. </ts>
               <ts e="T26" id="Seg_1877" n="e" s="T25">Hu͡opka </ts>
               <ts e="T27" id="Seg_1879" n="e" s="T26">ürdüger </ts>
               <ts e="T28" id="Seg_1881" n="e" s="T27">ɨttan </ts>
               <ts e="T29" id="Seg_1883" n="e" s="T28">körbüte, </ts>
               <ts e="T30" id="Seg_1885" n="e" s="T29">Dʼige baːba </ts>
               <ts e="T31" id="Seg_1887" n="e" s="T30">iher </ts>
               <ts e="T32" id="Seg_1889" n="e" s="T31">biːrges </ts>
               <ts e="T33" id="Seg_1891" n="e" s="T32">dʼi͡eki </ts>
               <ts e="T34" id="Seg_1893" n="e" s="T33">öttütten. </ts>
               <ts e="T35" id="Seg_1895" n="e" s="T34">Hu͡opka </ts>
               <ts e="T36" id="Seg_1897" n="e" s="T35">ortotuːgar </ts>
               <ts e="T37" id="Seg_1899" n="e" s="T36">tijbit </ts>
               <ts e="T38" id="Seg_1901" n="e" s="T37">da </ts>
               <ts e="T39" id="Seg_1903" n="e" s="T38">čokčos </ts>
               <ts e="T40" id="Seg_1905" n="e" s="T39">gɨmmɨt. </ts>
               <ts e="T41" id="Seg_1907" n="e" s="T40">Laːjku </ts>
               <ts e="T42" id="Seg_1909" n="e" s="T41">kubulgat </ts>
               <ts e="T43" id="Seg_1911" n="e" s="T42">bagajɨ </ts>
               <ts e="T44" id="Seg_1913" n="e" s="T43">ete </ts>
               <ts e="T45" id="Seg_1915" n="e" s="T44">ebit. </ts>
               <ts e="T46" id="Seg_1917" n="e" s="T45">Hahɨl </ts>
               <ts e="T47" id="Seg_1919" n="e" s="T46">ogo </ts>
               <ts e="T48" id="Seg_1921" n="e" s="T47">bu͡olan </ts>
               <ts e="T49" id="Seg_1923" n="e" s="T48">baran </ts>
               <ts e="T50" id="Seg_1925" n="e" s="T49">hu͡opka </ts>
               <ts e="T51" id="Seg_1927" n="e" s="T50">annɨn </ts>
               <ts e="T52" id="Seg_1929" n="e" s="T51">dʼi͡ek </ts>
               <ts e="T53" id="Seg_1931" n="e" s="T52">čekerijen </ts>
               <ts e="T54" id="Seg_1933" n="e" s="T53">kaːlbɨt. </ts>
               <ts e="T55" id="Seg_1935" n="e" s="T54">"O, </ts>
               <ts e="T56" id="Seg_1937" n="e" s="T55">annʼɨː </ts>
               <ts e="T57" id="Seg_1939" n="e" s="T56">da! </ts>
               <ts e="T58" id="Seg_1941" n="e" s="T57">Kajtak </ts>
               <ts e="T59" id="Seg_1943" n="e" s="T58">ogolonorbun </ts>
               <ts e="T60" id="Seg_1945" n="e" s="T59">bilbekke </ts>
               <ts e="T61" id="Seg_1947" n="e" s="T60">kaːlbɨppɨnɨj", </ts>
               <ts e="T62" id="Seg_1949" n="e" s="T61">dʼi͡ebit </ts>
               <ts e="T63" id="Seg_1951" n="e" s="T62">da </ts>
               <ts e="T64" id="Seg_1953" n="e" s="T63">ogonu </ts>
               <ts e="T65" id="Seg_1955" n="e" s="T64">huːluː </ts>
               <ts e="T66" id="Seg_1957" n="e" s="T65">tutan </ts>
               <ts e="T67" id="Seg_1959" n="e" s="T66">baran </ts>
               <ts e="T68" id="Seg_1961" n="e" s="T67">dʼi͡etiger </ts>
               <ts e="T69" id="Seg_1963" n="e" s="T68">ildʼe </ts>
               <ts e="T70" id="Seg_1965" n="e" s="T69">barbɨt. </ts>
               <ts e="T71" id="Seg_1967" n="e" s="T70">Dʼige baːba </ts>
               <ts e="T72" id="Seg_1969" n="e" s="T71">ogonnʼorun </ts>
               <ts e="T73" id="Seg_1971" n="e" s="T72">kɨtta </ts>
               <ts e="T74" id="Seg_1973" n="e" s="T73">ebe </ts>
               <ts e="T75" id="Seg_1975" n="e" s="T74">kɨtɨlɨgar </ts>
               <ts e="T76" id="Seg_1977" n="e" s="T75">oloror </ts>
               <ts e="T77" id="Seg_1979" n="e" s="T76">ebitter. </ts>
               <ts e="T78" id="Seg_1981" n="e" s="T77">Ogoto </ts>
               <ts e="T79" id="Seg_1983" n="e" s="T78">hu͡oktar </ts>
               <ts e="T80" id="Seg_1985" n="e" s="T79">ebit, </ts>
               <ts e="T81" id="Seg_1987" n="e" s="T80">hogotok </ts>
               <ts e="T82" id="Seg_1989" n="e" s="T81">aːku </ts>
               <ts e="T83" id="Seg_1991" n="e" s="T82">tabalaːktar. </ts>
               <ts e="T84" id="Seg_1993" n="e" s="T83">Ogoloro </ts>
               <ts e="T85" id="Seg_1995" n="e" s="T84">künnete </ts>
               <ts e="T86" id="Seg_1997" n="e" s="T85">ulaːtan </ts>
               <ts e="T87" id="Seg_1999" n="e" s="T86">ispit, </ts>
               <ts e="T88" id="Seg_2001" n="e" s="T87">haŋalammɨt. </ts>
               <ts e="T89" id="Seg_2003" n="e" s="T88">Inʼete </ts>
               <ts e="T90" id="Seg_2005" n="e" s="T89">bejetitten </ts>
               <ts e="T91" id="Seg_2007" n="e" s="T90">hukujdaːn </ts>
               <ts e="T92" id="Seg_2009" n="e" s="T91">taŋas </ts>
               <ts e="T93" id="Seg_2011" n="e" s="T92">tikpit. </ts>
               <ts e="T94" id="Seg_2013" n="e" s="T93">Bu </ts>
               <ts e="T95" id="Seg_2015" n="e" s="T94">ogo </ts>
               <ts e="T96" id="Seg_2017" n="e" s="T95">hüːreliː </ts>
               <ts e="T97" id="Seg_2019" n="e" s="T96">hɨldʼar </ts>
               <ts e="T98" id="Seg_2021" n="e" s="T97">bu͡olbut </ts>
               <ts e="T99" id="Seg_2023" n="e" s="T98">dʼi͡etin </ts>
               <ts e="T100" id="Seg_2025" n="e" s="T99">attɨgar. </ts>
               <ts e="T101" id="Seg_2027" n="e" s="T100">Ogonnʼor </ts>
               <ts e="T102" id="Seg_2029" n="e" s="T101">kün </ts>
               <ts e="T103" id="Seg_2031" n="e" s="T102">aːjɨ </ts>
               <ts e="T104" id="Seg_2033" n="e" s="T103">ilimnener </ts>
               <ts e="T105" id="Seg_2035" n="e" s="T104">ideleːk </ts>
               <ts e="T106" id="Seg_2037" n="e" s="T105">ebit. </ts>
               <ts e="T107" id="Seg_2039" n="e" s="T106">Bu </ts>
               <ts e="T108" id="Seg_2041" n="e" s="T107">ogonu </ts>
               <ts e="T109" id="Seg_2043" n="e" s="T108">balɨgɨnan </ts>
               <ts e="T110" id="Seg_2045" n="e" s="T109">agaj </ts>
               <ts e="T111" id="Seg_2047" n="e" s="T110">ahatar </ts>
               <ts e="T112" id="Seg_2049" n="e" s="T111">ebitter. </ts>
               <ts e="T113" id="Seg_2051" n="e" s="T112">Laːjku </ts>
               <ts e="T114" id="Seg_2053" n="e" s="T113">biːrde </ts>
               <ts e="T115" id="Seg_2055" n="e" s="T114">dʼebit: </ts>
               <ts e="T116" id="Seg_2057" n="e" s="T115">"Maːma, </ts>
               <ts e="T117" id="Seg_2059" n="e" s="T116">min </ts>
               <ts e="T118" id="Seg_2061" n="e" s="T117">et </ts>
               <ts e="T119" id="Seg_2063" n="e" s="T118">hi͡ekpin </ts>
               <ts e="T120" id="Seg_2065" n="e" s="T119">bagarabɨn." </ts>
               <ts e="T121" id="Seg_2067" n="e" s="T120">"Ogonnʼor, </ts>
               <ts e="T122" id="Seg_2069" n="e" s="T121">če </ts>
               <ts e="T123" id="Seg_2071" n="e" s="T122">olor </ts>
               <ts e="T124" id="Seg_2073" n="e" s="T123">baːngaːjgin, </ts>
               <ts e="T125" id="Seg_2075" n="e" s="T124">ogo </ts>
               <ts e="T126" id="Seg_2077" n="e" s="T125">et </ts>
               <ts e="T127" id="Seg_2079" n="e" s="T126">hi͡en </ts>
               <ts e="T128" id="Seg_2081" n="e" s="T127">bagarbɨt." </ts>
               <ts e="T129" id="Seg_2083" n="e" s="T128">Ogonnʼor </ts>
               <ts e="T130" id="Seg_2085" n="e" s="T129">tabatɨn </ts>
               <ts e="T131" id="Seg_2087" n="e" s="T130">ölörön </ts>
               <ts e="T132" id="Seg_2089" n="e" s="T131">keːspit. </ts>
               <ts e="T133" id="Seg_2091" n="e" s="T132">Laːjku </ts>
               <ts e="T134" id="Seg_2093" n="e" s="T133">kubulgat </ts>
               <ts e="T135" id="Seg_2095" n="e" s="T134">bagata </ts>
               <ts e="T136" id="Seg_2097" n="e" s="T135">ulaːtan </ts>
               <ts e="T137" id="Seg_2099" n="e" s="T136">ispit. </ts>
               <ts e="T138" id="Seg_2101" n="e" s="T137">"Maːma, </ts>
               <ts e="T139" id="Seg_2103" n="e" s="T138">togo </ts>
               <ts e="T140" id="Seg_2105" n="e" s="T139">ebe </ts>
               <ts e="T141" id="Seg_2107" n="e" s="T140">oŋu͡or </ts>
               <ts e="T142" id="Seg_2109" n="e" s="T141">tahɨmmappɨt, </ts>
               <ts e="T143" id="Seg_2111" n="e" s="T142">onno </ts>
               <ts e="T144" id="Seg_2113" n="e" s="T143">kiti͡eme </ts>
               <ts e="T145" id="Seg_2115" n="e" s="T144">bagajɨ </ts>
               <ts e="T146" id="Seg_2117" n="e" s="T145">bɨhɨːlaːk", </ts>
               <ts e="T147" id="Seg_2119" n="e" s="T146">dʼi͡ebit. </ts>
               <ts e="T148" id="Seg_2121" n="e" s="T147">"Kaja </ts>
               <ts e="T149" id="Seg_2123" n="e" s="T148">ogonnʼor, </ts>
               <ts e="T150" id="Seg_2125" n="e" s="T149">istegin </ts>
               <ts e="T151" id="Seg_2127" n="e" s="T150">duː </ts>
               <ts e="T152" id="Seg_2129" n="e" s="T151">ogo </ts>
               <ts e="T153" id="Seg_2131" n="e" s="T152">haŋatɨn? </ts>
               <ts e="T154" id="Seg_2133" n="e" s="T153">Kirdigi </ts>
               <ts e="T155" id="Seg_2135" n="e" s="T154">gɨnar, </ts>
               <ts e="T156" id="Seg_2137" n="e" s="T155">kanɨga </ts>
               <ts e="T157" id="Seg_2139" n="e" s="T156">dʼi͡eri </ts>
               <ts e="T158" id="Seg_2141" n="e" s="T157">biːr </ts>
               <ts e="T159" id="Seg_2143" n="e" s="T158">hirge </ts>
               <ts e="T160" id="Seg_2145" n="e" s="T159">oloru͡okputuj? </ts>
               <ts e="T161" id="Seg_2147" n="e" s="T160">Köhü͡ök." </ts>
               <ts e="T162" id="Seg_2149" n="e" s="T161">Ogonnʼor </ts>
               <ts e="T163" id="Seg_2151" n="e" s="T162">öčöspökkö </ts>
               <ts e="T164" id="Seg_2153" n="e" s="T163">kihilerin </ts>
               <ts e="T165" id="Seg_2155" n="e" s="T164">oŋu͡or </ts>
               <ts e="T166" id="Seg_2157" n="e" s="T165">tahaːran </ts>
               <ts e="T167" id="Seg_2159" n="e" s="T166">keːspit </ts>
               <ts e="T168" id="Seg_2161" n="e" s="T167">tɨːnnan. </ts>
               <ts e="T169" id="Seg_2163" n="e" s="T168">Laːjku </ts>
               <ts e="T170" id="Seg_2165" n="e" s="T169">üre-üre </ts>
               <ts e="T171" id="Seg_2167" n="e" s="T170">hüːre </ts>
               <ts e="T172" id="Seg_2169" n="e" s="T171">hɨldʼɨbɨt. </ts>
               <ts e="T173" id="Seg_2171" n="e" s="T172">Ki͡ehelik </ts>
               <ts e="T174" id="Seg_2173" n="e" s="T173">tahɨnan </ts>
               <ts e="T175" id="Seg_2175" n="e" s="T174">bütellerin </ts>
               <ts e="T176" id="Seg_2177" n="e" s="T175">gɨtta </ts>
               <ts e="T177" id="Seg_2179" n="e" s="T176">bu </ts>
               <ts e="T178" id="Seg_2181" n="e" s="T177">ogo </ts>
               <ts e="T179" id="Seg_2183" n="e" s="T178">maːmatɨgar </ts>
               <ts e="T180" id="Seg_2185" n="e" s="T179">kelen </ts>
               <ts e="T181" id="Seg_2187" n="e" s="T180">töhögüger </ts>
               <ts e="T182" id="Seg_2189" n="e" s="T181">olorbut. </ts>
               <ts e="T183" id="Seg_2191" n="e" s="T182">"Maːma, </ts>
               <ts e="T184" id="Seg_2193" n="e" s="T183">min </ts>
               <ts e="T185" id="Seg_2195" n="e" s="T184">tɨːlanɨ͡akpɨn </ts>
               <ts e="T186" id="Seg_2197" n="e" s="T185">bagarabɨn." </ts>
               <ts e="T187" id="Seg_2199" n="e" s="T186">"Ol </ts>
               <ts e="T188" id="Seg_2201" n="e" s="T187">tu͡ogaj </ts>
               <ts e="T189" id="Seg_2203" n="e" s="T188">emi͡e", </ts>
               <ts e="T190" id="Seg_2205" n="e" s="T189">ogonnʼor </ts>
               <ts e="T191" id="Seg_2207" n="e" s="T190">kɨːhɨrbɨt. </ts>
               <ts e="T192" id="Seg_2209" n="e" s="T191">Oloro </ts>
               <ts e="T193" id="Seg_2211" n="e" s="T192">tühen </ts>
               <ts e="T194" id="Seg_2213" n="e" s="T193">baran </ts>
               <ts e="T195" id="Seg_2215" n="e" s="T194">"če, </ts>
               <ts e="T196" id="Seg_2217" n="e" s="T195">ajɨlaːgɨn, </ts>
               <ts e="T197" id="Seg_2219" n="e" s="T196">tɨːlan, </ts>
               <ts e="T198" id="Seg_2221" n="e" s="T197">ɨraːk </ts>
               <ts e="T199" id="Seg_2223" n="e" s="T198">ire </ts>
               <ts e="T200" id="Seg_2225" n="e" s="T199">barɨma, </ts>
               <ts e="T201" id="Seg_2227" n="e" s="T200">duː?" </ts>
               <ts e="T202" id="Seg_2229" n="e" s="T201">"Hu͡ok, </ts>
               <ts e="T203" id="Seg_2231" n="e" s="T202">barɨ͡am </ts>
               <ts e="T204" id="Seg_2233" n="e" s="T203">hu͡oga, </ts>
               <ts e="T205" id="Seg_2235" n="e" s="T204">ebe </ts>
               <ts e="T206" id="Seg_2237" n="e" s="T205">kɨtɨlɨgar </ts>
               <ts e="T207" id="Seg_2239" n="e" s="T206">ire </ts>
               <ts e="T208" id="Seg_2241" n="e" s="T207">hɨldʼɨ͡am", </ts>
               <ts e="T209" id="Seg_2243" n="e" s="T208">dʼi͡ebit </ts>
               <ts e="T210" id="Seg_2245" n="e" s="T209">Laːjku </ts>
               <ts e="T211" id="Seg_2247" n="e" s="T210">kütür. </ts>
               <ts e="T212" id="Seg_2249" n="e" s="T211">Laːjku </ts>
               <ts e="T213" id="Seg_2251" n="e" s="T212">tɨːga </ts>
               <ts e="T214" id="Seg_2253" n="e" s="T213">olorbut, </ts>
               <ts e="T215" id="Seg_2255" n="e" s="T214">orguːjakaːn </ts>
               <ts e="T216" id="Seg_2257" n="e" s="T215">kɨtɨl </ts>
               <ts e="T217" id="Seg_2259" n="e" s="T216">ustun </ts>
               <ts e="T218" id="Seg_2261" n="e" s="T217">erdine </ts>
               <ts e="T219" id="Seg_2263" n="e" s="T218">hɨldʼɨbɨt. </ts>
               <ts e="T220" id="Seg_2265" n="e" s="T219">Ogonnʼordoːk </ts>
               <ts e="T221" id="Seg_2267" n="e" s="T220">emeːksin </ts>
               <ts e="T222" id="Seg_2269" n="e" s="T221">dʼi͡ege </ts>
               <ts e="T223" id="Seg_2271" n="e" s="T222">kiːrellerin </ts>
               <ts e="T224" id="Seg_2273" n="e" s="T223">kɨtta </ts>
               <ts e="T225" id="Seg_2275" n="e" s="T224">ɨraːtan </ts>
               <ts e="T226" id="Seg_2277" n="e" s="T225">da </ts>
               <ts e="T227" id="Seg_2279" n="e" s="T226">ɨraːtan </ts>
               <ts e="T228" id="Seg_2281" n="e" s="T227">ispit. </ts>
               <ts e="T229" id="Seg_2283" n="e" s="T228">Bu </ts>
               <ts e="T230" id="Seg_2285" n="e" s="T229">ogo </ts>
               <ts e="T231" id="Seg_2287" n="e" s="T230">ebe </ts>
               <ts e="T232" id="Seg_2289" n="e" s="T231">ortotugar </ts>
               <ts e="T233" id="Seg_2291" n="e" s="T232">tijbit </ts>
               <ts e="T234" id="Seg_2293" n="e" s="T233">daganɨ </ts>
               <ts e="T235" id="Seg_2295" n="e" s="T234">ulakan </ts>
               <ts e="T236" id="Seg_2297" n="e" s="T235">kihi </ts>
               <ts e="T237" id="Seg_2299" n="e" s="T236">bu͡olbut. </ts>
               <ts e="T238" id="Seg_2301" n="e" s="T237">Ogonnʼor </ts>
               <ts e="T239" id="Seg_2303" n="e" s="T238">taksɨbɨta, </ts>
               <ts e="T240" id="Seg_2305" n="e" s="T239">körbüte </ts>
               <ts e="T241" id="Seg_2307" n="e" s="T240">u͡ola </ts>
               <ts e="T242" id="Seg_2309" n="e" s="T241">ɨraːppɨt: </ts>
               <ts e="T243" id="Seg_2311" n="e" s="T242">"Kaja, </ts>
               <ts e="T244" id="Seg_2313" n="e" s="T243">kajdi͡ek </ts>
               <ts e="T245" id="Seg_2315" n="e" s="T244">baragɨn? </ts>
               <ts e="T246" id="Seg_2317" n="e" s="T245">ɨraːtɨma </ts>
               <ts e="T247" id="Seg_2319" n="e" s="T246">dʼiːbin. </ts>
               <ts e="T248" id="Seg_2321" n="e" s="T247">Uːga </ts>
               <ts e="T249" id="Seg_2323" n="e" s="T248">tüstüŋ!" </ts>
               <ts e="T250" id="Seg_2325" n="e" s="T249">Laːjku </ts>
               <ts e="T251" id="Seg_2327" n="e" s="T250">ebe </ts>
               <ts e="T252" id="Seg_2329" n="e" s="T251">ortotugar </ts>
               <ts e="T253" id="Seg_2331" n="e" s="T252">tijen </ts>
               <ts e="T254" id="Seg_2333" n="e" s="T253">baran </ts>
               <ts e="T255" id="Seg_2335" n="e" s="T254">ügüleːbit </ts>
               <ts e="T256" id="Seg_2337" n="e" s="T255">"ka-ka". </ts>
               <ts e="T257" id="Seg_2339" n="e" s="T256">"Min </ts>
               <ts e="T258" id="Seg_2341" n="e" s="T257">Laːjkubun </ts>
               <ts e="T259" id="Seg_2343" n="e" s="T258">eːt, </ts>
               <ts e="T260" id="Seg_2345" n="e" s="T259">ehigi </ts>
               <ts e="T261" id="Seg_2347" n="e" s="T260">onu </ts>
               <ts e="T262" id="Seg_2349" n="e" s="T261">bilbetekkit!" </ts>
               <ts e="T263" id="Seg_2351" n="e" s="T262">Laːjku </ts>
               <ts e="T264" id="Seg_2353" n="e" s="T263">küreːbit. </ts>
               <ts e="T265" id="Seg_2355" n="e" s="T264">Ogonnʼordoːk </ts>
               <ts e="T266" id="Seg_2357" n="e" s="T265">emeːksin </ts>
               <ts e="T267" id="Seg_2359" n="e" s="T266">ölörsö </ts>
               <ts e="T268" id="Seg_2361" n="e" s="T267">kaːlbɨttar. </ts>
               <ts e="T269" id="Seg_2363" n="e" s="T268">Bu </ts>
               <ts e="T270" id="Seg_2365" n="e" s="T269">u͡ol </ts>
               <ts e="T271" id="Seg_2367" n="e" s="T270">kaːman </ts>
               <ts e="T272" id="Seg_2369" n="e" s="T271">ispit. </ts>
               <ts e="T273" id="Seg_2371" n="e" s="T272">Töhö </ts>
               <ts e="T274" id="Seg_2373" n="e" s="T273">bu͡olu͡oj, </ts>
               <ts e="T275" id="Seg_2375" n="e" s="T274">körbüte, </ts>
               <ts e="T276" id="Seg_2377" n="e" s="T275">dʼi͡eler </ts>
               <ts e="T277" id="Seg_2379" n="e" s="T276">turallar, </ts>
               <ts e="T278" id="Seg_2381" n="e" s="T277">hɨrgalaːk </ts>
               <ts e="T279" id="Seg_2383" n="e" s="T278">tabalar </ts>
               <ts e="T280" id="Seg_2385" n="e" s="T279">baːjɨlla </ts>
               <ts e="T281" id="Seg_2387" n="e" s="T280">hɨtallar. </ts>
               <ts e="T282" id="Seg_2389" n="e" s="T281">Biːr </ts>
               <ts e="T283" id="Seg_2391" n="e" s="T282">dʼi͡ege </ts>
               <ts e="T284" id="Seg_2393" n="e" s="T283">kiːrbite, </ts>
               <ts e="T285" id="Seg_2395" n="e" s="T284">hogotok </ts>
               <ts e="T286" id="Seg_2397" n="e" s="T285">dʼaktar </ts>
               <ts e="T287" id="Seg_2399" n="e" s="T286">oloror. </ts>
               <ts e="T288" id="Seg_2401" n="e" s="T287">Laːjku </ts>
               <ts e="T289" id="Seg_2403" n="e" s="T288">čaːjdaːbɨt. </ts>
               <ts e="T290" id="Seg_2405" n="e" s="T289">"Bu </ts>
               <ts e="T291" id="Seg_2407" n="e" s="T290">tuguj, </ts>
               <ts e="T292" id="Seg_2409" n="e" s="T291">munnʼak </ts>
               <ts e="T293" id="Seg_2411" n="e" s="T292">duː, </ts>
               <ts e="T294" id="Seg_2413" n="e" s="T293">kurum </ts>
               <ts e="T295" id="Seg_2415" n="e" s="T294">duː?" </ts>
               <ts e="T296" id="Seg_2417" n="e" s="T295">"Hu͡ok. </ts>
               <ts e="T297" id="Seg_2419" n="e" s="T296">Bihigi </ts>
               <ts e="T298" id="Seg_2421" n="e" s="T297">kineːspit </ts>
               <ts e="T299" id="Seg_2423" n="e" s="T298">biːr </ts>
               <ts e="T300" id="Seg_2425" n="e" s="T299">kɨːstaːk, </ts>
               <ts e="T301" id="Seg_2427" n="e" s="T300">ol </ts>
               <ts e="T302" id="Seg_2429" n="e" s="T301">kɨːska </ts>
               <ts e="T303" id="Seg_2431" n="e" s="T302">kineːs </ts>
               <ts e="T304" id="Seg_2433" n="e" s="T303">taːbɨrɨnɨn </ts>
               <ts e="T305" id="Seg_2435" n="e" s="T304">taːjbɨt </ts>
               <ts e="T306" id="Seg_2437" n="e" s="T305">kihi </ts>
               <ts e="T307" id="Seg_2439" n="e" s="T306">ire </ts>
               <ts e="T308" id="Seg_2441" n="e" s="T307">er </ts>
               <ts e="T309" id="Seg_2443" n="e" s="T308">bu͡olar", </ts>
               <ts e="T310" id="Seg_2445" n="e" s="T309">dʼi͡ebit </ts>
               <ts e="T311" id="Seg_2447" n="e" s="T310">dʼaktar. </ts>
               <ts e="T312" id="Seg_2449" n="e" s="T311">Laːjku </ts>
               <ts e="T313" id="Seg_2451" n="e" s="T312">taksɨbɨt </ts>
               <ts e="T314" id="Seg_2453" n="e" s="T313">da </ts>
               <ts e="T315" id="Seg_2455" n="e" s="T314">ebe </ts>
               <ts e="T316" id="Seg_2457" n="e" s="T315">kɨrɨːtɨgar </ts>
               <ts e="T317" id="Seg_2459" n="e" s="T316">kiːrbit, </ts>
               <ts e="T318" id="Seg_2461" n="e" s="T317">otunan </ts>
               <ts e="T319" id="Seg_2463" n="e" s="T318">haptan </ts>
               <ts e="T320" id="Seg_2465" n="e" s="T319">baran </ts>
               <ts e="T321" id="Seg_2467" n="e" s="T320">hɨppɨt. </ts>
               <ts e="T322" id="Seg_2469" n="e" s="T321">Körbüte, </ts>
               <ts e="T323" id="Seg_2471" n="e" s="T322">ikki </ts>
               <ts e="T324" id="Seg_2473" n="e" s="T323">kɨːs </ts>
               <ts e="T325" id="Seg_2475" n="e" s="T324">uː </ts>
               <ts e="T326" id="Seg_2477" n="e" s="T325">baha </ts>
               <ts e="T327" id="Seg_2479" n="e" s="T326">iheller, </ts>
               <ts e="T328" id="Seg_2481" n="e" s="T327">orguhu͡oktanan, </ts>
               <ts e="T329" id="Seg_2483" n="e" s="T328">ikki </ts>
               <ts e="T330" id="Seg_2485" n="e" s="T329">baːgɨ </ts>
               <ts e="T331" id="Seg_2487" n="e" s="T330">hannɨlarɨgar </ts>
               <ts e="T332" id="Seg_2489" n="e" s="T331">hükpütter. </ts>
               <ts e="T333" id="Seg_2491" n="e" s="T332">U͡ol </ts>
               <ts e="T334" id="Seg_2493" n="e" s="T333">atagɨttan </ts>
               <ts e="T335" id="Seg_2495" n="e" s="T334">baːj </ts>
               <ts e="T336" id="Seg_2497" n="e" s="T335">kɨːs </ts>
               <ts e="T337" id="Seg_2499" n="e" s="T336">iŋŋen </ts>
               <ts e="T338" id="Seg_2501" n="e" s="T337">uːtun </ts>
               <ts e="T339" id="Seg_2503" n="e" s="T338">dʼalkɨppɨt: </ts>
               <ts e="T340" id="Seg_2505" n="e" s="T339">"Bu </ts>
               <ts e="T341" id="Seg_2507" n="e" s="T340">barɨta </ts>
               <ts e="T342" id="Seg_2509" n="e" s="T341">agam </ts>
               <ts e="T343" id="Seg_2511" n="e" s="T342">ihin, </ts>
               <ts e="T344" id="Seg_2513" n="e" s="T343">erge </ts>
               <ts e="T345" id="Seg_2515" n="e" s="T344">taksɨbɨt </ts>
               <ts e="T346" id="Seg_2517" n="e" s="T345">ebitim </ts>
               <ts e="T347" id="Seg_2519" n="e" s="T346">bu͡ollar, </ts>
               <ts e="T348" id="Seg_2521" n="e" s="T347">uː </ts>
               <ts e="T349" id="Seg_2523" n="e" s="T348">baha </ts>
               <ts e="T350" id="Seg_2525" n="e" s="T349">erejdene </ts>
               <ts e="T351" id="Seg_2527" n="e" s="T350">hɨldʼɨ͡ak </ts>
               <ts e="T352" id="Seg_2529" n="e" s="T351">etim </ts>
               <ts e="T353" id="Seg_2531" n="e" s="T352">duː? </ts>
               <ts e="T354" id="Seg_2533" n="e" s="T353">Atakpɨn </ts>
               <ts e="T355" id="Seg_2535" n="e" s="T354">ire </ts>
               <ts e="T356" id="Seg_2537" n="e" s="T355">ilittim." </ts>
               <ts e="T357" id="Seg_2539" n="e" s="T356">"Dogoː, </ts>
               <ts e="T358" id="Seg_2541" n="e" s="T357">iti </ts>
               <ts e="T359" id="Seg_2543" n="e" s="T358">teːteŋ </ts>
               <ts e="T360" id="Seg_2545" n="e" s="T359">tugu </ts>
               <ts e="T361" id="Seg_2547" n="e" s="T360">taːttarar?" </ts>
               <ts e="T362" id="Seg_2549" n="e" s="T361">"Agam </ts>
               <ts e="T363" id="Seg_2551" n="e" s="T362">kahan </ts>
               <ts e="T364" id="Seg_2553" n="e" s="T363">ire </ts>
               <ts e="T365" id="Seg_2555" n="e" s="T364">hette </ts>
               <ts e="T366" id="Seg_2557" n="e" s="T365">bɨt </ts>
               <ts e="T367" id="Seg_2559" n="e" s="T366">tiriːtinen </ts>
               <ts e="T368" id="Seg_2561" n="e" s="T367">dahɨna </ts>
               <ts e="T369" id="Seg_2563" n="e" s="T368">oŋorbut </ts>
               <ts e="T370" id="Seg_2565" n="e" s="T369">ete. </ts>
               <ts e="T371" id="Seg_2567" n="e" s="T370">Itini </ts>
               <ts e="T372" id="Seg_2569" n="e" s="T371">gɨnar </ts>
               <ts e="T373" id="Seg_2571" n="e" s="T372">ebite </ts>
               <ts e="T374" id="Seg_2573" n="e" s="T373">duː? </ts>
               <ts e="T375" id="Seg_2575" n="e" s="T374">Če, </ts>
               <ts e="T376" id="Seg_2577" n="e" s="T375">barɨ͡ak. </ts>
               <ts e="T377" id="Seg_2579" n="e" s="T376">Uː </ts>
               <ts e="T378" id="Seg_2581" n="e" s="T377">da </ts>
               <ts e="T379" id="Seg_2583" n="e" s="T378">tulujbat, </ts>
               <ts e="T380" id="Seg_2585" n="e" s="T379">mas </ts>
               <ts e="T381" id="Seg_2587" n="e" s="T380">da </ts>
               <ts e="T382" id="Seg_2589" n="e" s="T381">tulujbat </ts>
               <ts e="T383" id="Seg_2591" n="e" s="T382">baččalaːk </ts>
               <ts e="T384" id="Seg_2593" n="e" s="T383">ɨ͡aldʼɨkka." </ts>
               <ts e="T385" id="Seg_2595" n="e" s="T384">Laːjku </ts>
               <ts e="T386" id="Seg_2597" n="e" s="T385">tönnön </ts>
               <ts e="T387" id="Seg_2599" n="e" s="T386">kelen, </ts>
               <ts e="T388" id="Seg_2601" n="e" s="T387">maː </ts>
               <ts e="T389" id="Seg_2603" n="e" s="T388">dʼi͡etiger </ts>
               <ts e="T390" id="Seg_2605" n="e" s="T389">kiːrbit, </ts>
               <ts e="T391" id="Seg_2607" n="e" s="T390">kommut. </ts>
               <ts e="T392" id="Seg_2609" n="e" s="T391">Harsi͡erde </ts>
               <ts e="T393" id="Seg_2611" n="e" s="T392">turbuta — </ts>
               <ts e="T394" id="Seg_2613" n="e" s="T393">emi͡e </ts>
               <ts e="T395" id="Seg_2615" n="e" s="T394">ɨ͡aldʼɨt </ts>
               <ts e="T396" id="Seg_2617" n="e" s="T395">toloru. </ts>
               <ts e="T397" id="Seg_2619" n="e" s="T396">Bu </ts>
               <ts e="T398" id="Seg_2621" n="e" s="T397">u͡ol </ts>
               <ts e="T399" id="Seg_2623" n="e" s="T398">emi͡e </ts>
               <ts e="T400" id="Seg_2625" n="e" s="T399">kineːske </ts>
               <ts e="T401" id="Seg_2627" n="e" s="T400">barɨːhɨ, </ts>
               <ts e="T402" id="Seg_2629" n="e" s="T401">ɨ͡aldʼɨttarɨ </ts>
               <ts e="T403" id="Seg_2631" n="e" s="T402">kɨtta </ts>
               <ts e="T404" id="Seg_2633" n="e" s="T403">biːrge </ts>
               <ts e="T405" id="Seg_2635" n="e" s="T404">kiːrbit. </ts>
               <ts e="T406" id="Seg_2637" n="e" s="T405">Körbüte, </ts>
               <ts e="T407" id="Seg_2639" n="e" s="T406">dʼi͡eni </ts>
               <ts e="T408" id="Seg_2641" n="e" s="T407">toloru </ts>
               <ts e="T409" id="Seg_2643" n="e" s="T408">er </ts>
               <ts e="T410" id="Seg_2645" n="e" s="T409">kihi. </ts>
               <ts e="T411" id="Seg_2647" n="e" s="T410">"O, </ts>
               <ts e="T412" id="Seg_2649" n="e" s="T411">bügün </ts>
               <ts e="T413" id="Seg_2651" n="e" s="T412">haŋa </ts>
               <ts e="T414" id="Seg_2653" n="e" s="T413">ɨ͡aldʼɨt </ts>
               <ts e="T415" id="Seg_2655" n="e" s="T414">kelbit. </ts>
               <ts e="T416" id="Seg_2657" n="e" s="T415">Če, </ts>
               <ts e="T417" id="Seg_2659" n="e" s="T416">bagar, </ts>
               <ts e="T418" id="Seg_2661" n="e" s="T417">en </ts>
               <ts e="T419" id="Seg_2663" n="e" s="T418">min </ts>
               <ts e="T420" id="Seg_2665" n="e" s="T419">taːbɨrɨmmɨn </ts>
               <ts e="T421" id="Seg_2667" n="e" s="T420">taːjɨ͡aŋ", </ts>
               <ts e="T422" id="Seg_2669" n="e" s="T421">di͡ebit </ts>
               <ts e="T423" id="Seg_2671" n="e" s="T422">kineːs. </ts>
               <ts e="T424" id="Seg_2673" n="e" s="T423">"Tu͡ok </ts>
               <ts e="T425" id="Seg_2675" n="e" s="T424">taːbɨrɨnnaːkkɨnɨj? </ts>
               <ts e="T426" id="Seg_2677" n="e" s="T425">Töhö </ts>
               <ts e="T427" id="Seg_2679" n="e" s="T426">öjüm </ts>
               <ts e="T428" id="Seg_2681" n="e" s="T427">kotorunan </ts>
               <ts e="T429" id="Seg_2683" n="e" s="T428">taːja </ts>
               <ts e="T430" id="Seg_2685" n="e" s="T429">hatɨ͡ak </ts>
               <ts e="T431" id="Seg_2687" n="e" s="T430">etim", </ts>
               <ts e="T432" id="Seg_2689" n="e" s="T431">Laːjku </ts>
               <ts e="T433" id="Seg_2691" n="e" s="T432">dʼebit. </ts>
               <ts e="T434" id="Seg_2693" n="e" s="T433">"Min </ts>
               <ts e="T435" id="Seg_2695" n="e" s="T434">aːttarabɨn, </ts>
               <ts e="T436" id="Seg_2697" n="e" s="T435">dahɨna </ts>
               <ts e="T437" id="Seg_2699" n="e" s="T436">tugunan </ts>
               <ts e="T438" id="Seg_2701" n="e" s="T437">oŋohullubutuj </ts>
               <ts e="T439" id="Seg_2703" n="e" s="T438">dʼi͡en", </ts>
               <ts e="T440" id="Seg_2705" n="e" s="T439">kineːs </ts>
               <ts e="T441" id="Seg_2707" n="e" s="T440">ɨjɨppɨt. </ts>
               <ts e="T442" id="Seg_2709" n="e" s="T441">"Öskötün </ts>
               <ts e="T443" id="Seg_2711" n="e" s="T442">min </ts>
               <ts e="T444" id="Seg_2713" n="e" s="T443">öjüm </ts>
               <ts e="T445" id="Seg_2715" n="e" s="T444">barar </ts>
               <ts e="T446" id="Seg_2717" n="e" s="T445">hirinen </ts>
               <ts e="T447" id="Seg_2719" n="e" s="T446">oŋoru͡oŋ </ts>
               <ts e="T448" id="Seg_2721" n="e" s="T447">duː?" </ts>
               <ts e="T449" id="Seg_2723" n="e" s="T448">"Tiriː </ts>
               <ts e="T450" id="Seg_2725" n="e" s="T449">bu͡ollagɨna </ts>
               <ts e="T451" id="Seg_2727" n="e" s="T450">bu͡ollun. </ts>
               <ts e="T452" id="Seg_2729" n="e" s="T451">Min </ts>
               <ts e="T453" id="Seg_2731" n="e" s="T452">hanaːbar, </ts>
               <ts e="T454" id="Seg_2733" n="e" s="T453">hette </ts>
               <ts e="T455" id="Seg_2735" n="e" s="T454">bɨt </ts>
               <ts e="T456" id="Seg_2737" n="e" s="T455">tiriːtinen </ts>
               <ts e="T457" id="Seg_2739" n="e" s="T456">onohullubut </ts>
               <ts e="T458" id="Seg_2741" n="e" s="T457">bɨhɨːlaːk", </ts>
               <ts e="T459" id="Seg_2743" n="e" s="T458">Laːjku </ts>
               <ts e="T460" id="Seg_2745" n="e" s="T459">haŋarbɨt. </ts>
               <ts e="T461" id="Seg_2747" n="e" s="T460">"O, </ts>
               <ts e="T462" id="Seg_2749" n="e" s="T461">tu͡ok </ts>
               <ts e="T463" id="Seg_2751" n="e" s="T462">öjdöːk </ts>
               <ts e="T464" id="Seg_2753" n="e" s="T463">kihiteginij? </ts>
               <ts e="T465" id="Seg_2755" n="e" s="T464">Taːjdɨŋ </ts>
               <ts e="T466" id="Seg_2757" n="e" s="T465">dʼiː. </ts>
               <ts e="T467" id="Seg_2759" n="e" s="T466">Če, </ts>
               <ts e="T468" id="Seg_2761" n="e" s="T467">oččogo </ts>
               <ts e="T469" id="Seg_2763" n="e" s="T468">min </ts>
               <ts e="T470" id="Seg_2765" n="e" s="T469">kɨːspɨn </ts>
               <ts e="T471" id="Seg_2767" n="e" s="T470">dʼaktar </ts>
               <ts e="T472" id="Seg_2769" n="e" s="T471">gɨnar </ts>
               <ts e="T473" id="Seg_2771" n="e" s="T472">ɨjaːktaːkkɨn </ts>
               <ts e="T474" id="Seg_2773" n="e" s="T473">ebit. </ts>
               <ts e="T475" id="Seg_2775" n="e" s="T474">Kajdak </ts>
               <ts e="T476" id="Seg_2777" n="e" s="T475">gɨnɨ͡aŋɨj, </ts>
               <ts e="T477" id="Seg_2779" n="e" s="T476">harsɨn </ts>
               <ts e="T478" id="Seg_2781" n="e" s="T477">kurumnu͡okput." </ts>
               <ts e="T479" id="Seg_2783" n="e" s="T478">Laːjku </ts>
               <ts e="T480" id="Seg_2785" n="e" s="T479">baːj </ts>
               <ts e="T481" id="Seg_2787" n="e" s="T480">kineːs </ts>
               <ts e="T482" id="Seg_2789" n="e" s="T481">bosku͡oj </ts>
               <ts e="T483" id="Seg_2791" n="e" s="T482">kɨːhɨn </ts>
               <ts e="T484" id="Seg_2793" n="e" s="T483">dʼaktardanan, </ts>
               <ts e="T485" id="Seg_2795" n="e" s="T484">bajan-toton </ts>
               <ts e="T486" id="Seg_2797" n="e" s="T485">olorbut, </ts>
               <ts e="T487" id="Seg_2799" n="e" s="T486">tu͡ok </ts>
               <ts e="T488" id="Seg_2801" n="e" s="T487">da </ts>
               <ts e="T489" id="Seg_2803" n="e" s="T488">ereji </ts>
               <ts e="T490" id="Seg_2805" n="e" s="T489">bilbekke. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T8" id="Seg_2806" s="T0">BeVP_1970_Laajku_flk.001 (001.001)</ta>
            <ta e="T13" id="Seg_2807" s="T8">BeVP_1970_Laajku_flk.002 (001.002)</ta>
            <ta e="T18" id="Seg_2808" s="T13">BeVP_1970_Laajku_flk.003 (001.003)</ta>
            <ta e="T25" id="Seg_2809" s="T18">BeVP_1970_Laajku_flk.004 (001.004)</ta>
            <ta e="T34" id="Seg_2810" s="T25">BeVP_1970_Laajku_flk.005 (001.006)</ta>
            <ta e="T40" id="Seg_2811" s="T34">BeVP_1970_Laajku_flk.006 (001.007)</ta>
            <ta e="T45" id="Seg_2812" s="T40">BeVP_1970_Laajku_flk.007 (001.008)</ta>
            <ta e="T54" id="Seg_2813" s="T45">BeVP_1970_Laajku_flk.008 (001.009)</ta>
            <ta e="T57" id="Seg_2814" s="T54">BeVP_1970_Laajku_flk.009 (001.010)</ta>
            <ta e="T70" id="Seg_2815" s="T57">BeVP_1970_Laajku_flk.010 (001.011)</ta>
            <ta e="T77" id="Seg_2816" s="T70">BeVP_1970_Laajku_flk.011 (001.013)</ta>
            <ta e="T83" id="Seg_2817" s="T77">BeVP_1970_Laajku_flk.012 (001.014)</ta>
            <ta e="T88" id="Seg_2818" s="T83">BeVP_1970_Laajku_flk.013 (001.015)</ta>
            <ta e="T93" id="Seg_2819" s="T88">BeVP_1970_Laajku_flk.014 (001.016)</ta>
            <ta e="T100" id="Seg_2820" s="T93">BeVP_1970_Laajku_flk.015 (001.017)</ta>
            <ta e="T106" id="Seg_2821" s="T100">BeVP_1970_Laajku_flk.016 (001.018)</ta>
            <ta e="T112" id="Seg_2822" s="T106">BeVP_1970_Laajku_flk.017 (001.019)</ta>
            <ta e="T115" id="Seg_2823" s="T112">BeVP_1970_Laajku_flk.018 (001.020)</ta>
            <ta e="T120" id="Seg_2824" s="T115">BeVP_1970_Laajku_flk.019 (001.020)</ta>
            <ta e="T128" id="Seg_2825" s="T120">BeVP_1970_Laajku_flk.020 (001.021)</ta>
            <ta e="T132" id="Seg_2826" s="T128">BeVP_1970_Laajku_flk.021 (001.022)</ta>
            <ta e="T137" id="Seg_2827" s="T132">BeVP_1970_Laajku_flk.022 (001.023)</ta>
            <ta e="T147" id="Seg_2828" s="T137">BeVP_1970_Laajku_flk.023 (001.024)</ta>
            <ta e="T153" id="Seg_2829" s="T147">BeVP_1970_Laajku_flk.024 (001.025)</ta>
            <ta e="T160" id="Seg_2830" s="T153">BeVP_1970_Laajku_flk.025 (001.026)</ta>
            <ta e="T161" id="Seg_2831" s="T160">BeVP_1970_Laajku_flk.026 (001.027)</ta>
            <ta e="T168" id="Seg_2832" s="T161">BeVP_1970_Laajku_flk.027 (001.028)</ta>
            <ta e="T172" id="Seg_2833" s="T168">BeVP_1970_Laajku_flk.028 (001.029)</ta>
            <ta e="T182" id="Seg_2834" s="T172">BeVP_1970_Laajku_flk.029 (001.030)</ta>
            <ta e="T186" id="Seg_2835" s="T182">BeVP_1970_Laajku_flk.030 (001.031)</ta>
            <ta e="T191" id="Seg_2836" s="T186">BeVP_1970_Laajku_flk.031 (001.032)</ta>
            <ta e="T201" id="Seg_2837" s="T191">BeVP_1970_Laajku_flk.032 (001.034)</ta>
            <ta e="T211" id="Seg_2838" s="T201">BeVP_1970_Laajku_flk.033 (001.035)</ta>
            <ta e="T219" id="Seg_2839" s="T211">BeVP_1970_Laajku_flk.034 (001.036)</ta>
            <ta e="T228" id="Seg_2840" s="T219">BeVP_1970_Laajku_flk.035 (001.037)</ta>
            <ta e="T237" id="Seg_2841" s="T228">BeVP_1970_Laajku_flk.036 (001.038)</ta>
            <ta e="T242" id="Seg_2842" s="T237">BeVP_1970_Laajku_flk.037 (001.039)</ta>
            <ta e="T245" id="Seg_2843" s="T242">BeVP_1970_Laajku_flk.038 (001.039)</ta>
            <ta e="T247" id="Seg_2844" s="T245">BeVP_1970_Laajku_flk.039 (001.040)</ta>
            <ta e="T249" id="Seg_2845" s="T247">BeVP_1970_Laajku_flk.040 (001.041)</ta>
            <ta e="T256" id="Seg_2846" s="T249">BeVP_1970_Laajku_flk.041 (001.042)</ta>
            <ta e="T262" id="Seg_2847" s="T256">BeVP_1970_Laajku_flk.042 (001.043)</ta>
            <ta e="T264" id="Seg_2848" s="T262">BeVP_1970_Laajku_flk.043 (001.044)</ta>
            <ta e="T268" id="Seg_2849" s="T264">BeVP_1970_Laajku_flk.044 (001.045)</ta>
            <ta e="T272" id="Seg_2850" s="T268">BeVP_1970_Laajku_flk.045 (001.046)</ta>
            <ta e="T281" id="Seg_2851" s="T272">BeVP_1970_Laajku_flk.046 (001.047)</ta>
            <ta e="T287" id="Seg_2852" s="T281">BeVP_1970_Laajku_flk.047 (001.048)</ta>
            <ta e="T289" id="Seg_2853" s="T287">BeVP_1970_Laajku_flk.048 (001.049)</ta>
            <ta e="T295" id="Seg_2854" s="T289">BeVP_1970_Laajku_flk.049 (001.050)</ta>
            <ta e="T296" id="Seg_2855" s="T295">BeVP_1970_Laajku_flk.050 (001.051)</ta>
            <ta e="T311" id="Seg_2856" s="T296">BeVP_1970_Laajku_flk.051 (001.052)</ta>
            <ta e="T321" id="Seg_2857" s="T311">BeVP_1970_Laajku_flk.052 (001.053)</ta>
            <ta e="T332" id="Seg_2858" s="T321">BeVP_1970_Laajku_flk.053 (001.054)</ta>
            <ta e="T339" id="Seg_2859" s="T332">BeVP_1970_Laajku_flk.054 (001.055)</ta>
            <ta e="T353" id="Seg_2860" s="T339">BeVP_1970_Laajku_flk.055 (001.055)</ta>
            <ta e="T356" id="Seg_2861" s="T353">BeVP_1970_Laajku_flk.056 (001.056)</ta>
            <ta e="T361" id="Seg_2862" s="T356">BeVP_1970_Laajku_flk.057 (001.057)</ta>
            <ta e="T370" id="Seg_2863" s="T361">BeVP_1970_Laajku_flk.058 (001.058)</ta>
            <ta e="T374" id="Seg_2864" s="T370">BeVP_1970_Laajku_flk.059 (001.059)</ta>
            <ta e="T376" id="Seg_2865" s="T374">BeVP_1970_Laajku_flk.060 (001.060)</ta>
            <ta e="T384" id="Seg_2866" s="T376">BeVP_1970_Laajku_flk.061 (001.061)</ta>
            <ta e="T391" id="Seg_2867" s="T384">BeVP_1970_Laajku_flk.062 (001.062)</ta>
            <ta e="T396" id="Seg_2868" s="T391">BeVP_1970_Laajku_flk.063 (001.063)</ta>
            <ta e="T405" id="Seg_2869" s="T396">BeVP_1970_Laajku_flk.064 (001.064)</ta>
            <ta e="T410" id="Seg_2870" s="T405">BeVP_1970_Laajku_flk.065 (001.065)</ta>
            <ta e="T415" id="Seg_2871" s="T410">BeVP_1970_Laajku_flk.066 (001.066)</ta>
            <ta e="T423" id="Seg_2872" s="T415">BeVP_1970_Laajku_flk.067 (001.067)</ta>
            <ta e="T425" id="Seg_2873" s="T423">BeVP_1970_Laajku_flk.068 (001.069)</ta>
            <ta e="T433" id="Seg_2874" s="T425">BeVP_1970_Laajku_flk.069 (001.070)</ta>
            <ta e="T441" id="Seg_2875" s="T433">BeVP_1970_Laajku_flk.070 (001.071)</ta>
            <ta e="T448" id="Seg_2876" s="T441">BeVP_1970_Laajku_flk.071 (001.073)</ta>
            <ta e="T451" id="Seg_2877" s="T448">BeVP_1970_Laajku_flk.072 (001.074)</ta>
            <ta e="T460" id="Seg_2878" s="T451">BeVP_1970_Laajku_flk.073 (001.075)</ta>
            <ta e="T464" id="Seg_2879" s="T460">BeVP_1970_Laajku_flk.074 (001.076)</ta>
            <ta e="T466" id="Seg_2880" s="T464">BeVP_1970_Laajku_flk.075 (001.077)</ta>
            <ta e="T474" id="Seg_2881" s="T466">BeVP_1970_Laajku_flk.076 (001.078)</ta>
            <ta e="T478" id="Seg_2882" s="T474">BeVP_1970_Laajku_flk.077 (001.079)</ta>
            <ta e="T490" id="Seg_2883" s="T478">BeVP_1970_Laajku_flk.078 (001.080)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T8" id="Seg_2884" s="T0">һоготогун үөскээбит, иньэтин-агатын өйдөөбөт Лаайку дьиэн ыт олорбут.</ta>
            <ta e="T13" id="Seg_2885" s="T8">Биирдэ каама һылдьыбыт, киһи көрдүү.</ta>
            <ta e="T18" id="Seg_2886" s="T13">Ол һылдьан көрбүт үрдүк һуопканы.</ta>
            <ta e="T25" id="Seg_2887" s="T18">"Туок буолуой мин итиннэ ытыннакпына?" — дьии һанаабыт.</ta>
            <ta e="T34" id="Seg_2888" s="T25">һуопка үрдүгэр ыттан көрбүтэ: Дьигэ-бааба иһэр бииргэс дьиэки өттүттэн.</ta>
            <ta e="T40" id="Seg_2889" s="T34">һуопка ортотуугар тийбит да чокчос гыммыт.</ta>
            <ta e="T45" id="Seg_2890" s="T40">Лаайку кубулгат багайы этэ эбит.</ta>
            <ta e="T54" id="Seg_2891" s="T45">Һаһыл ого буолан баран һуопка аннын дьиэк чэкэрийэн каалбыт.</ta>
            <ta e="T57" id="Seg_2892" s="T54">"О, анньыы да!</ta>
            <ta e="T70" id="Seg_2893" s="T57">Кайтак оголонорбун билбэккэ каалбыппыный?!" — дьиэбит да огону һуулуу тутан баран дьиэтигэр илдьэ барбыт.</ta>
            <ta e="T77" id="Seg_2894" s="T70">Дьигэ-бааба огонньорун кытта эбэ кытылыгар олорор эбиттэр.</ta>
            <ta e="T83" id="Seg_2895" s="T77">Огото һуоктар эбит, һоготок ааку табалаактар.</ta>
            <ta e="T88" id="Seg_2896" s="T83">Оголоро күннэтэ улаатан испит, һаҥаламмыт.</ta>
            <ta e="T93" id="Seg_2897" s="T88">Иньэтэ бэйэтиттэн һукуйдаан таҥас тикпит.</ta>
            <ta e="T100" id="Seg_2898" s="T93">Бу ого һүүрэлии һылдьар буолбут дьиэтин аттыгар.</ta>
            <ta e="T106" id="Seg_2899" s="T100">Огонньор күн аайы илимнэнэр идэлээк эбит.</ta>
            <ta e="T112" id="Seg_2900" s="T106">Бу огону балыгынан агай аһатар эбиттэр.</ta>
            <ta e="T115" id="Seg_2901" s="T112">Лаайку биирдэ дьэбит: </ta>
            <ta e="T120" id="Seg_2902" s="T115">— Маама, мин эт һиэкпин багарабын.</ta>
            <ta e="T128" id="Seg_2903" s="T120">— Огонньор, чэ олор баангаайгин, ого эт һиэн багарбыт.</ta>
            <ta e="T132" id="Seg_2904" s="T128">Огонньор табатын өлөрөн кээспит.</ta>
            <ta e="T137" id="Seg_2905" s="T132">Лаайку кубулгат багата улаатан испит.</ta>
            <ta e="T147" id="Seg_2906" s="T137">— Маама, того эбэ оҥуор таһыммаппыт, онно китиэмэ багайы быһыылаак, — дьиэбит.</ta>
            <ta e="T153" id="Seg_2907" s="T147">— Кайа огонньор, истэгин дуу ого һаҥатын?</ta>
            <ta e="T160" id="Seg_2908" s="T153">Кирдиги гынар, каныга дьиэри биир һиргэ олоруокпутуй?!</ta>
            <ta e="T161" id="Seg_2909" s="T160">Көһүөк.</ta>
            <ta e="T168" id="Seg_2910" s="T161">Огонньор өчөспөккө киһилэрин оҥуор таһааран кээспит тыыннан.</ta>
            <ta e="T172" id="Seg_2911" s="T168">Лаайку үрэ-үрэ һүүрэ һылдьыбыт. </ta>
            <ta e="T182" id="Seg_2912" s="T172">Киэһэлик таһынан бүтэллэрин гытта бу ого мааматыгар кэлэн төһөгүгэр олорбут.</ta>
            <ta e="T186" id="Seg_2913" s="T182">— Маама, мин тыыланыакпын багарабын.</ta>
            <ta e="T191" id="Seg_2914" s="T186">— Ол туогай эмиэ? — огонньор кыыһырбыт.</ta>
            <ta e="T201" id="Seg_2915" s="T191">Олоро түһэн баран: — Чэ, айылаагын, тыылан, ыраак ирэ барыма, дуу?</ta>
            <ta e="T211" id="Seg_2916" s="T201">— һуок, барыам һуога, эбэ кытылыгар ирэ һылдьыам, — дьиэбит Лаайку күтүр.</ta>
            <ta e="T219" id="Seg_2917" s="T211">Лаайку тыыга олорбут, оргууйакаан кытыл устун эрдинэ һыл-дьыбыт.</ta>
            <ta e="T228" id="Seg_2918" s="T219">Огонньордоок эмээксин дьиэгэ киирэллэрин кытта ыраатан да ыраатан испит.</ta>
            <ta e="T237" id="Seg_2919" s="T228">Бу ого эбэ ортотугар тийбит даганы улакан киһи буолбут.</ta>
            <ta e="T242" id="Seg_2920" s="T237">Огонньор таксыбыта, көрбүтэ уола ырааппыт: </ta>
            <ta e="T245" id="Seg_2921" s="T242">— Кайа, кайдиэк барагын?</ta>
            <ta e="T247" id="Seg_2922" s="T245">Ыраатыма дьиибин.</ta>
            <ta e="T249" id="Seg_2923" s="T247">Ууга түстүҥ!</ta>
            <ta e="T256" id="Seg_2924" s="T249">Лаайку эбэ ортотугар тийэн баран үгүлээбит: — Ка-ка!</ta>
            <ta e="T262" id="Seg_2925" s="T256">Мин Лаайкубун ээт, эһиги ону билбэтэккит!</ta>
            <ta e="T264" id="Seg_2926" s="T262">Лаайку күрээбит.</ta>
            <ta e="T268" id="Seg_2927" s="T264">Огонньордоок эмээксин өлөрсө каалбыттар.</ta>
            <ta e="T272" id="Seg_2928" s="T268">Бу уол кааман испит.</ta>
            <ta e="T281" id="Seg_2929" s="T272">Төһө буолуой, көрбүтэ: дьиэлэр тураллар, һыргалаак табалар баайылла һыталлар.</ta>
            <ta e="T287" id="Seg_2930" s="T281">Биир дьиэгэ киирбитэ, һоготок дьактар олорор.</ta>
            <ta e="T289" id="Seg_2931" s="T287">Лаайку чаайдаабыт.</ta>
            <ta e="T295" id="Seg_2932" s="T289">— Бу тугуй, мунньак дуу, курум дуу?</ta>
            <ta e="T296" id="Seg_2933" s="T295">— һуок.</ta>
            <ta e="T311" id="Seg_2934" s="T296">Биһиги кинээспит биир кыыстаак, ол кыыска кинээс таабырынын таайбыт киһи ирэ эр буолар, — дьиэбит дьактар.</ta>
            <ta e="T321" id="Seg_2935" s="T311">Лаайку таксыбыт да эбэ кырыытыгар киирбит, отунан һаптан баран һыппыт.</ta>
            <ta e="T332" id="Seg_2936" s="T321">Көрбүтэ: икки кыыс уу баһа иһэллэр, оргуһуоктанан, икки баагы һанныларыгар һүкпүттэр.</ta>
            <ta e="T339" id="Seg_2937" s="T332">Уол атагыттан баай кыыс иҥҥэн уутун дьалкыппыт: </ta>
            <ta e="T353" id="Seg_2938" s="T339">— Бу барыта агам иһин, эргэ таксыбыт эбитим буоллар, уу баһа эрэйдэнэ һылдьыак этим дуу?!</ta>
            <ta e="T356" id="Seg_2939" s="T353">Атакпын ирэ илиттим.</ta>
            <ta e="T361" id="Seg_2940" s="T356">— Догоо, ити тээтэҥ тугу тааттарар?</ta>
            <ta e="T370" id="Seg_2941" s="T361">— Агам каһан ирэ һэттэ быт тириитинэн даһына оҥорбут этэ.</ta>
            <ta e="T374" id="Seg_2942" s="T370">Итини гынар эбитэ дуу?</ta>
            <ta e="T376" id="Seg_2943" s="T374">Чэ, барыак.</ta>
            <ta e="T384" id="Seg_2944" s="T376">Уу да тулуйбат, мас да тулуйбат баччалаак ыалдьыкка.</ta>
            <ta e="T391" id="Seg_2945" s="T384">Лаайку төннөн кэлэн, маа дьиэтигэр киирбит, коммут.</ta>
            <ta e="T396" id="Seg_2946" s="T391">Һарсиэрдэ турбута — эмиэ ыалдьыт толору.</ta>
            <ta e="T405" id="Seg_2947" s="T396">Бу уол эмиэ кинээскэ барыыһы, ыалдьыттары кытта бииргэ киирбит.</ta>
            <ta e="T410" id="Seg_2948" s="T405">Көрбүтэ: дьиэни толору эр киһи.</ta>
            <ta e="T415" id="Seg_2949" s="T410">— О, бүгүн һаҥа ыалдьыт кэлбит.</ta>
            <ta e="T423" id="Seg_2950" s="T415">Чэ, багар, эн мин таабырыммын таайыаҥ? — диэбит кинээс.</ta>
            <ta e="T425" id="Seg_2951" s="T423">— Туок таабырыннааккыный?</ta>
            <ta e="T433" id="Seg_2952" s="T425">Төһө өйүм которунан таайа һатыак этим, — Лаайку дьэбит.</ta>
            <ta e="T441" id="Seg_2953" s="T433">— Мин ааттарабын: даһына тугунан оҥоһуллубутуй дьиэн? — кинээс ыйыппыт.</ta>
            <ta e="T448" id="Seg_2954" s="T441">— Өскөтүн мин өйүм барар һиринэн оҥоруоҥ дуу?</ta>
            <ta e="T451" id="Seg_2955" s="T448">— Тирии буоллагына буоллун.</ta>
            <ta e="T460" id="Seg_2956" s="T451">Мин һанаабар, һэттэ быт тириитинэн оноһуллубут быһыылаак, — Лаайку һаҥарбыт.</ta>
            <ta e="T464" id="Seg_2957" s="T460">— О, туок өйдөөк киһитэгиний?</ta>
            <ta e="T466" id="Seg_2958" s="T464">Таайдыҥ дьии.</ta>
            <ta e="T474" id="Seg_2959" s="T466">Чэ, оччого мин кыыспын дьактар гынар ыйаактааккын эбит.</ta>
            <ta e="T478" id="Seg_2960" s="T474">Кайдак гыныаҥый, һарсын курумнуокпут.</ta>
            <ta e="T490" id="Seg_2961" s="T478">Лаайку баай кинээс боскуой кыыһын дьактарданан, байан-тотон олорбут, туок да эрэйи билбэккэ.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T8" id="Seg_2962" s="T0">Hogotogun ü͡öskeːbit, inʼetin-agatɨn öjdöːböt Laːjku dʼi͡en ɨt olorbut. </ta>
            <ta e="T13" id="Seg_2963" s="T8">Biːrde kaːma hɨldʼɨbɨt, kihi kördüː. </ta>
            <ta e="T18" id="Seg_2964" s="T13">Ol hɨldʼan körbüt ürdük hu͡opkanɨ. </ta>
            <ta e="T25" id="Seg_2965" s="T18">"Tu͡ok bu͡olu͡oj min itinne ɨtɨnnakpɨna", dʼiː hanaːbɨt. </ta>
            <ta e="T34" id="Seg_2966" s="T25">Hu͡opka ürdüger ɨttan körbüte, Dʼige baːba iher biːrges dʼi͡eki öttütten. </ta>
            <ta e="T40" id="Seg_2967" s="T34">Hu͡opka ortotuːgar tijbit da čokčos gɨmmɨt. </ta>
            <ta e="T45" id="Seg_2968" s="T40">Laːjku kubulgat bagajɨ ete ebit. </ta>
            <ta e="T54" id="Seg_2969" s="T45">Hahɨl ogo bu͡olan baran hu͡opka annɨn dʼi͡ek čekerijen kaːlbɨt. </ta>
            <ta e="T57" id="Seg_2970" s="T54">"O, annʼɨː da! </ta>
            <ta e="T70" id="Seg_2971" s="T57">Kajtak ogolonorbun bilbekke kaːlbɨppɨnɨj", dʼi͡ebit da ogonu huːluː tutan baran dʼi͡etiger ildʼe barbɨt. </ta>
            <ta e="T77" id="Seg_2972" s="T70">Dʼige baːba ogonnʼorun kɨtta ebe kɨtɨlɨgar oloror ebitter. </ta>
            <ta e="T83" id="Seg_2973" s="T77">Ogoto hu͡oktar ebit, hogotok aːku tabalaːktar. </ta>
            <ta e="T88" id="Seg_2974" s="T83">Ogoloro künnete ulaːtan ispit, haŋalammɨt. </ta>
            <ta e="T93" id="Seg_2975" s="T88">Inʼete bejetitten hukujdaːn taŋas tikpit. </ta>
            <ta e="T100" id="Seg_2976" s="T93">Bu ogo hüːreliː hɨldʼar bu͡olbut dʼi͡etin attɨgar. </ta>
            <ta e="T106" id="Seg_2977" s="T100">Ogonnʼor kün aːjɨ ilimnener ideleːk ebit. </ta>
            <ta e="T112" id="Seg_2978" s="T106">Bu ogonu balɨgɨnan agaj ahatar ebitter. </ta>
            <ta e="T115" id="Seg_2979" s="T112">Laːjku biːrde dʼebit: </ta>
            <ta e="T120" id="Seg_2980" s="T115">"Maːma, min et hi͡ekpin bagarabɨn." </ta>
            <ta e="T128" id="Seg_2981" s="T120">"Ogonnʼor, če olor baːngaːjgin, ogo et hi͡en bagarbɨt." </ta>
            <ta e="T132" id="Seg_2982" s="T128">Ogonnʼor tabatɨn ölörön keːspit. </ta>
            <ta e="T137" id="Seg_2983" s="T132">Laːjku kubulgat bagata ulaːtan ispit. </ta>
            <ta e="T147" id="Seg_2984" s="T137">"Maːma, togo ebe oŋu͡or tahɨmmappɨt, onno kiti͡eme bagajɨ bɨhɨːlaːk", dʼi͡ebit. </ta>
            <ta e="T153" id="Seg_2985" s="T147">"Kaja ogonnʼor, istegin duː ogo haŋatɨn? </ta>
            <ta e="T160" id="Seg_2986" s="T153">Kirdigi gɨnar, kanɨga dʼi͡eri biːr hirge oloru͡okputuj? </ta>
            <ta e="T161" id="Seg_2987" s="T160">Köhü͡ök." </ta>
            <ta e="T168" id="Seg_2988" s="T161">Ogonnʼor öčöspökkö kihilerin oŋu͡or tahaːran keːspit tɨːnnan. </ta>
            <ta e="T172" id="Seg_2989" s="T168">Laːjku üre-üre hüːre hɨldʼɨbɨt. </ta>
            <ta e="T182" id="Seg_2990" s="T172">Ki͡ehelik tahɨnan bütellerin gɨtta bu ogo maːmatɨgar kelen töhögüger olorbut. </ta>
            <ta e="T186" id="Seg_2991" s="T182">"Maːma, min tɨːlanɨ͡akpɨn bagarabɨn." </ta>
            <ta e="T191" id="Seg_2992" s="T186">"Ol tu͡ogaj emi͡e", ogonnʼor kɨːhɨrbɨt. </ta>
            <ta e="T201" id="Seg_2993" s="T191">Oloro tühen baran "če, ajɨlaːgɨn, tɨːlan, ɨraːk ire barɨma, duː?" </ta>
            <ta e="T211" id="Seg_2994" s="T201">"Hu͡ok, barɨ͡am hu͡oga, ebe kɨtɨlɨgar ire hɨldʼɨ͡am", dʼi͡ebit Laːjku kütür. </ta>
            <ta e="T219" id="Seg_2995" s="T211">Laːjku tɨːga olorbut, orguːjakaːn kɨtɨl ustun erdine hɨldʼɨbɨt. </ta>
            <ta e="T228" id="Seg_2996" s="T219">Ogonnʼordoːk emeːksin dʼi͡ege kiːrellerin kɨtta ɨraːtan da ɨraːtan ispit. </ta>
            <ta e="T237" id="Seg_2997" s="T228">Bu ogo ebe ortotugar tijbit daganɨ ulakan kihi bu͡olbut. </ta>
            <ta e="T242" id="Seg_2998" s="T237">Ogonnʼor taksɨbɨta, körbüte u͡ola ɨraːppɨt: </ta>
            <ta e="T245" id="Seg_2999" s="T242">"Kaja, kajdi͡ek baragɨn? </ta>
            <ta e="T247" id="Seg_3000" s="T245">ɨraːtɨma dʼiːbin. </ta>
            <ta e="T249" id="Seg_3001" s="T247">Uːga tüstüŋ!" </ta>
            <ta e="T256" id="Seg_3002" s="T249">Laːjku ebe ortotugar tijen baran ügüleːbit "ka-ka". </ta>
            <ta e="T262" id="Seg_3003" s="T256">"Min Laːjkubun eːt, ehigi onu bilbetekkit!" </ta>
            <ta e="T264" id="Seg_3004" s="T262">Laːjku küreːbit. </ta>
            <ta e="T268" id="Seg_3005" s="T264">Ogonnʼordoːk emeːksin ölörsö kaːlbɨttar. </ta>
            <ta e="T272" id="Seg_3006" s="T268">Bu u͡ol kaːman ispit. </ta>
            <ta e="T281" id="Seg_3007" s="T272">Töhö bu͡olu͡oj, körbüte, dʼi͡eler turallar, hɨrgalaːk tabalar baːjɨlla hɨtallar. </ta>
            <ta e="T287" id="Seg_3008" s="T281">Biːr dʼi͡ege kiːrbite, hogotok dʼaktar oloror. </ta>
            <ta e="T289" id="Seg_3009" s="T287">Laːjku čaːjdaːbɨt. </ta>
            <ta e="T295" id="Seg_3010" s="T289">"Bu tuguj, munnʼak duː, kurum duː?" </ta>
            <ta e="T296" id="Seg_3011" s="T295">"Hu͡ok. </ta>
            <ta e="T311" id="Seg_3012" s="T296">Bihigi kineːspit biːr kɨːstaːk, ol kɨːska kineːs taːbɨrɨnɨn taːjbɨt kihi ire er bu͡olar", dʼi͡ebit dʼaktar. </ta>
            <ta e="T321" id="Seg_3013" s="T311">Laːjku taksɨbɨt da ebe kɨrɨːtɨgar kiːrbit, otunan haptan baran hɨppɨt. </ta>
            <ta e="T332" id="Seg_3014" s="T321">Körbüte, ikki kɨːs uː baha iheller, orguhu͡oktanan, ikki baːgɨ hannɨlarɨgar hükpütter. </ta>
            <ta e="T339" id="Seg_3015" s="T332">U͡ol atagɨttan baːj kɨːs iŋŋen uːtun dʼalkɨppɨt: </ta>
            <ta e="T353" id="Seg_3016" s="T339">"Bu barɨta agam ihin, erge taksɨbɨt ebitim bu͡ollar, uː baha erejdene hɨldʼɨ͡ak etim duː? </ta>
            <ta e="T356" id="Seg_3017" s="T353">Atakpɨn ire ilittim." </ta>
            <ta e="T361" id="Seg_3018" s="T356">"Dogoː, iti teːteŋ tugu taːttarar?" </ta>
            <ta e="T370" id="Seg_3019" s="T361">"Agam kahan ire hette bɨt tiriːtinen dahɨna oŋorbut ete. </ta>
            <ta e="T374" id="Seg_3020" s="T370">Itini gɨnar ebite duː? </ta>
            <ta e="T376" id="Seg_3021" s="T374">Če, barɨ͡ak. </ta>
            <ta e="T384" id="Seg_3022" s="T376">Uː da tulujbat, mas da tulujbat baččalaːk ɨ͡aldʼɨkka." </ta>
            <ta e="T391" id="Seg_3023" s="T384">Laːjku tönnön kelen, maː dʼi͡etiger kiːrbit, kommut. </ta>
            <ta e="T396" id="Seg_3024" s="T391">Harsi͡erde turbuta — emi͡e ɨ͡aldʼɨt toloru. </ta>
            <ta e="T405" id="Seg_3025" s="T396">Bu u͡ol emi͡e kineːske barɨːhɨ, ɨ͡aldʼɨttarɨ kɨtta biːrge kiːrbit. </ta>
            <ta e="T410" id="Seg_3026" s="T405">Körbüte, dʼi͡eni toloru er kihi. </ta>
            <ta e="T415" id="Seg_3027" s="T410">"O, bügün haŋa ɨ͡aldʼɨt kelbit. </ta>
            <ta e="T423" id="Seg_3028" s="T415">Če, bagar, en min taːbɨrɨmmɨn taːjɨ͡aŋ", di͡ebit kineːs. </ta>
            <ta e="T425" id="Seg_3029" s="T423">"Tu͡ok taːbɨrɨnnaːkkɨnɨj? </ta>
            <ta e="T433" id="Seg_3030" s="T425">Töhö öjüm kotorunan taːja hatɨ͡ak etim", Laːjku dʼebit. </ta>
            <ta e="T441" id="Seg_3031" s="T433">"Min aːttarabɨn, dahɨna tugunan oŋohullubutuj dʼi͡en", kineːs ɨjɨppɨt. </ta>
            <ta e="T448" id="Seg_3032" s="T441">"Öskötün min öjüm barar hirinen oŋoru͡oŋ duː?" </ta>
            <ta e="T451" id="Seg_3033" s="T448">"Tiriː bu͡ollagɨna bu͡ollun. </ta>
            <ta e="T460" id="Seg_3034" s="T451">Min hanaːbar, hette bɨt tiriːtinen onohullubut bɨhɨːlaːk", Laːjku haŋarbɨt. </ta>
            <ta e="T464" id="Seg_3035" s="T460">"O, tu͡ok öjdöːk kihiteginij? </ta>
            <ta e="T466" id="Seg_3036" s="T464">Taːjdɨŋ dʼiː. </ta>
            <ta e="T474" id="Seg_3037" s="T466">Če, oččogo min kɨːspɨn dʼaktar gɨnar ɨjaːktaːkkɨn ebit. </ta>
            <ta e="T478" id="Seg_3038" s="T474">Kajdak gɨnɨ͡aŋɨj, harsɨn kurumnu͡okput." </ta>
            <ta e="T490" id="Seg_3039" s="T478">Laːjku baːj kineːs bosku͡oj kɨːhɨn dʼaktardanan, bajan-toton olorbut, tu͡ok da ereji bilbekke. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_3040" s="T0">hogotogun</ta>
            <ta e="T2" id="Seg_3041" s="T1">ü͡öskeː-bit</ta>
            <ta e="T3" id="Seg_3042" s="T2">inʼe-ti-n-aga-tɨ-n</ta>
            <ta e="T4" id="Seg_3043" s="T3">öjdöː-böt</ta>
            <ta e="T5" id="Seg_3044" s="T4">Laːjku</ta>
            <ta e="T6" id="Seg_3045" s="T5">dʼi͡e-n</ta>
            <ta e="T7" id="Seg_3046" s="T6">ɨt</ta>
            <ta e="T8" id="Seg_3047" s="T7">olor-but</ta>
            <ta e="T9" id="Seg_3048" s="T8">biːrde</ta>
            <ta e="T10" id="Seg_3049" s="T9">kaːm-a</ta>
            <ta e="T11" id="Seg_3050" s="T10">hɨldʼ-ɨ-bɨt</ta>
            <ta e="T12" id="Seg_3051" s="T11">kihi</ta>
            <ta e="T13" id="Seg_3052" s="T12">körd-üː</ta>
            <ta e="T14" id="Seg_3053" s="T13">ol</ta>
            <ta e="T15" id="Seg_3054" s="T14">hɨldʼ-an</ta>
            <ta e="T16" id="Seg_3055" s="T15">kör-büt</ta>
            <ta e="T17" id="Seg_3056" s="T16">ürdük</ta>
            <ta e="T18" id="Seg_3057" s="T17">hu͡opka-nɨ</ta>
            <ta e="T19" id="Seg_3058" s="T18">tu͡ok</ta>
            <ta e="T20" id="Seg_3059" s="T19">bu͡ol-u͡o=j</ta>
            <ta e="T21" id="Seg_3060" s="T20">min</ta>
            <ta e="T22" id="Seg_3061" s="T21">itinne</ta>
            <ta e="T23" id="Seg_3062" s="T22">ɨtɨn-nak-pɨna</ta>
            <ta e="T24" id="Seg_3063" s="T23">dʼ-iː</ta>
            <ta e="T25" id="Seg_3064" s="T24">hanaː-bɨt</ta>
            <ta e="T26" id="Seg_3065" s="T25">hu͡opka</ta>
            <ta e="T27" id="Seg_3066" s="T26">ürd-ü-ger</ta>
            <ta e="T28" id="Seg_3067" s="T27">ɨtt-an</ta>
            <ta e="T29" id="Seg_3068" s="T28">kör-büt-e</ta>
            <ta e="T30" id="Seg_3069" s="T29">Dʼige baːba</ta>
            <ta e="T31" id="Seg_3070" s="T30">ih-er</ta>
            <ta e="T32" id="Seg_3071" s="T31">biːrges</ta>
            <ta e="T33" id="Seg_3072" s="T32">dʼi͡eki</ta>
            <ta e="T34" id="Seg_3073" s="T33">ött-ü-tten</ta>
            <ta e="T35" id="Seg_3074" s="T34">hu͡opka</ta>
            <ta e="T36" id="Seg_3075" s="T35">orto-tuː-gar</ta>
            <ta e="T37" id="Seg_3076" s="T36">tij-bit</ta>
            <ta e="T38" id="Seg_3077" s="T37">da</ta>
            <ta e="T39" id="Seg_3078" s="T38">čokčo-s</ta>
            <ta e="T40" id="Seg_3079" s="T39">gɨm-mɨt</ta>
            <ta e="T41" id="Seg_3080" s="T40">Laːjku</ta>
            <ta e="T42" id="Seg_3081" s="T41">kubulgat</ta>
            <ta e="T43" id="Seg_3082" s="T42">bagajɨ</ta>
            <ta e="T44" id="Seg_3083" s="T43">e-t-e</ta>
            <ta e="T45" id="Seg_3084" s="T44">e-bit</ta>
            <ta e="T46" id="Seg_3085" s="T45">hahɨl</ta>
            <ta e="T47" id="Seg_3086" s="T46">ogo</ta>
            <ta e="T48" id="Seg_3087" s="T47">bu͡ol-an</ta>
            <ta e="T49" id="Seg_3088" s="T48">baran</ta>
            <ta e="T50" id="Seg_3089" s="T49">hu͡opka</ta>
            <ta e="T51" id="Seg_3090" s="T50">ann-ɨ-n</ta>
            <ta e="T52" id="Seg_3091" s="T51">dʼi͡ek</ta>
            <ta e="T53" id="Seg_3092" s="T52">čekerij-en</ta>
            <ta e="T54" id="Seg_3093" s="T53">kaːl-bɨt</ta>
            <ta e="T55" id="Seg_3094" s="T54">o</ta>
            <ta e="T56" id="Seg_3095" s="T55">annʼɨː</ta>
            <ta e="T57" id="Seg_3096" s="T56">da</ta>
            <ta e="T58" id="Seg_3097" s="T57">kajtak</ta>
            <ta e="T59" id="Seg_3098" s="T58">ogo-lon-or-bu-n</ta>
            <ta e="T60" id="Seg_3099" s="T59">bil-bekke</ta>
            <ta e="T61" id="Seg_3100" s="T60">kaːl-bɨp-pɨn=ɨj</ta>
            <ta e="T62" id="Seg_3101" s="T61">dʼi͡e-bit</ta>
            <ta e="T63" id="Seg_3102" s="T62">da</ta>
            <ta e="T64" id="Seg_3103" s="T63">ogo-nu</ta>
            <ta e="T65" id="Seg_3104" s="T64">huːl-uː</ta>
            <ta e="T66" id="Seg_3105" s="T65">tut-an</ta>
            <ta e="T67" id="Seg_3106" s="T66">baran</ta>
            <ta e="T68" id="Seg_3107" s="T67">dʼi͡e-ti-ger</ta>
            <ta e="T69" id="Seg_3108" s="T68">ildʼ-e</ta>
            <ta e="T70" id="Seg_3109" s="T69">bar-bɨt</ta>
            <ta e="T71" id="Seg_3110" s="T70">Dʼige baːba</ta>
            <ta e="T72" id="Seg_3111" s="T71">ogonnʼor-u-n</ta>
            <ta e="T73" id="Seg_3112" s="T72">kɨtta</ta>
            <ta e="T74" id="Seg_3113" s="T73">ebe</ta>
            <ta e="T75" id="Seg_3114" s="T74">kɨtɨl-ɨ-gar</ta>
            <ta e="T76" id="Seg_3115" s="T75">olor-or</ta>
            <ta e="T77" id="Seg_3116" s="T76">e-bit-ter</ta>
            <ta e="T78" id="Seg_3117" s="T77">ogo-to</ta>
            <ta e="T79" id="Seg_3118" s="T78">hu͡ok-tar</ta>
            <ta e="T80" id="Seg_3119" s="T79">e-bit</ta>
            <ta e="T81" id="Seg_3120" s="T80">hogotok</ta>
            <ta e="T82" id="Seg_3121" s="T81">aːku</ta>
            <ta e="T83" id="Seg_3122" s="T82">taba-laːk-tar</ta>
            <ta e="T84" id="Seg_3123" s="T83">ogo-loro</ta>
            <ta e="T85" id="Seg_3124" s="T84">künnete</ta>
            <ta e="T86" id="Seg_3125" s="T85">ulaːt-an</ta>
            <ta e="T87" id="Seg_3126" s="T86">is-pit</ta>
            <ta e="T88" id="Seg_3127" s="T87">haŋa-lam-mɨt</ta>
            <ta e="T89" id="Seg_3128" s="T88">inʼe-te</ta>
            <ta e="T90" id="Seg_3129" s="T89">beje-ti-tten</ta>
            <ta e="T91" id="Seg_3130" s="T90">hukuj-daːn</ta>
            <ta e="T92" id="Seg_3131" s="T91">taŋas</ta>
            <ta e="T93" id="Seg_3132" s="T92">tik-pit</ta>
            <ta e="T94" id="Seg_3133" s="T93">bu</ta>
            <ta e="T95" id="Seg_3134" s="T94">ogo</ta>
            <ta e="T96" id="Seg_3135" s="T95">hüːrel-iː</ta>
            <ta e="T97" id="Seg_3136" s="T96">hɨldʼ-ar</ta>
            <ta e="T98" id="Seg_3137" s="T97">bu͡ol-but</ta>
            <ta e="T99" id="Seg_3138" s="T98">dʼi͡e-ti-n</ta>
            <ta e="T100" id="Seg_3139" s="T99">attɨ-gar</ta>
            <ta e="T101" id="Seg_3140" s="T100">ogonnʼor</ta>
            <ta e="T102" id="Seg_3141" s="T101">kün</ta>
            <ta e="T103" id="Seg_3142" s="T102">aːjɨ</ta>
            <ta e="T104" id="Seg_3143" s="T103">ilim-nen-er</ta>
            <ta e="T105" id="Seg_3144" s="T104">ide-leːk</ta>
            <ta e="T106" id="Seg_3145" s="T105">e-bit</ta>
            <ta e="T107" id="Seg_3146" s="T106">bu</ta>
            <ta e="T108" id="Seg_3147" s="T107">ogo-nu</ta>
            <ta e="T109" id="Seg_3148" s="T108">balɨg-ɨ-nan</ta>
            <ta e="T110" id="Seg_3149" s="T109">agaj</ta>
            <ta e="T111" id="Seg_3150" s="T110">ah-a-t-ar</ta>
            <ta e="T112" id="Seg_3151" s="T111">e-bit-ter</ta>
            <ta e="T113" id="Seg_3152" s="T112">Laːjku</ta>
            <ta e="T114" id="Seg_3153" s="T113">biːrde</ta>
            <ta e="T115" id="Seg_3154" s="T114">dʼe-bit</ta>
            <ta e="T116" id="Seg_3155" s="T115">maːma</ta>
            <ta e="T117" id="Seg_3156" s="T116">min</ta>
            <ta e="T118" id="Seg_3157" s="T117">et</ta>
            <ta e="T119" id="Seg_3158" s="T118">h-i͡ek-pi-n</ta>
            <ta e="T120" id="Seg_3159" s="T119">bagar-a-bɨn</ta>
            <ta e="T121" id="Seg_3160" s="T120">ogonnʼor</ta>
            <ta e="T122" id="Seg_3161" s="T121">če</ta>
            <ta e="T123" id="Seg_3162" s="T122">olor</ta>
            <ta e="T124" id="Seg_3163" s="T123">baːngaːj-gi-n</ta>
            <ta e="T125" id="Seg_3164" s="T124">ogo</ta>
            <ta e="T126" id="Seg_3165" s="T125">et</ta>
            <ta e="T127" id="Seg_3166" s="T126">h-i͡e-n</ta>
            <ta e="T128" id="Seg_3167" s="T127">bagar-bɨt</ta>
            <ta e="T129" id="Seg_3168" s="T128">ogonnʼor</ta>
            <ta e="T130" id="Seg_3169" s="T129">taba-tɨ-n</ta>
            <ta e="T131" id="Seg_3170" s="T130">ölör-ön</ta>
            <ta e="T132" id="Seg_3171" s="T131">keːs-pit</ta>
            <ta e="T133" id="Seg_3172" s="T132">Laːjku</ta>
            <ta e="T134" id="Seg_3173" s="T133">kubulgat</ta>
            <ta e="T135" id="Seg_3174" s="T134">baga-ta</ta>
            <ta e="T136" id="Seg_3175" s="T135">ulaːt-an</ta>
            <ta e="T137" id="Seg_3176" s="T136">is-pit</ta>
            <ta e="T138" id="Seg_3177" s="T137">maːma</ta>
            <ta e="T139" id="Seg_3178" s="T138">togo</ta>
            <ta e="T140" id="Seg_3179" s="T139">ebe</ta>
            <ta e="T141" id="Seg_3180" s="T140">oŋu͡or</ta>
            <ta e="T142" id="Seg_3181" s="T141">tah-ɨ-m-map-pɨt</ta>
            <ta e="T143" id="Seg_3182" s="T142">onno</ta>
            <ta e="T144" id="Seg_3183" s="T143">kiti͡eme</ta>
            <ta e="T145" id="Seg_3184" s="T144">bagajɨ</ta>
            <ta e="T146" id="Seg_3185" s="T145">bɨhɨːlaːk</ta>
            <ta e="T147" id="Seg_3186" s="T146">dʼi͡e-bit</ta>
            <ta e="T148" id="Seg_3187" s="T147">kaja</ta>
            <ta e="T149" id="Seg_3188" s="T148">ogonnʼor</ta>
            <ta e="T150" id="Seg_3189" s="T149">ist-e-gin</ta>
            <ta e="T151" id="Seg_3190" s="T150">duː</ta>
            <ta e="T152" id="Seg_3191" s="T151">ogo</ta>
            <ta e="T153" id="Seg_3192" s="T152">haŋa-tɨ-n</ta>
            <ta e="T154" id="Seg_3193" s="T153">kirdig-i</ta>
            <ta e="T155" id="Seg_3194" s="T154">gɨn-ar</ta>
            <ta e="T156" id="Seg_3195" s="T155">kanɨ-ga</ta>
            <ta e="T157" id="Seg_3196" s="T156">dʼi͡eri</ta>
            <ta e="T158" id="Seg_3197" s="T157">biːr</ta>
            <ta e="T159" id="Seg_3198" s="T158">hir-ge</ta>
            <ta e="T160" id="Seg_3199" s="T159">olor-u͡ok-put=uj</ta>
            <ta e="T161" id="Seg_3200" s="T160">köh-ü͡ök</ta>
            <ta e="T162" id="Seg_3201" s="T161">ogonnʼor</ta>
            <ta e="T163" id="Seg_3202" s="T162">öčös-pökkö</ta>
            <ta e="T164" id="Seg_3203" s="T163">kihi-ler-i-n</ta>
            <ta e="T165" id="Seg_3204" s="T164">oŋu͡or</ta>
            <ta e="T166" id="Seg_3205" s="T165">tahaːr-an</ta>
            <ta e="T167" id="Seg_3206" s="T166">keːs-pit</ta>
            <ta e="T168" id="Seg_3207" s="T167">tɨː-nnan</ta>
            <ta e="T169" id="Seg_3208" s="T168">Laːjku</ta>
            <ta e="T170" id="Seg_3209" s="T169">ür-e-ür-e</ta>
            <ta e="T171" id="Seg_3210" s="T170">hüːr-e</ta>
            <ta e="T172" id="Seg_3211" s="T171">hɨldʼ-ɨ-bɨt</ta>
            <ta e="T173" id="Seg_3212" s="T172">ki͡ehe-lik</ta>
            <ta e="T174" id="Seg_3213" s="T173">tah-ɨ-n-an</ta>
            <ta e="T175" id="Seg_3214" s="T174">büt-el-leri-n</ta>
            <ta e="T176" id="Seg_3215" s="T175">gɨtta</ta>
            <ta e="T177" id="Seg_3216" s="T176">bu</ta>
            <ta e="T178" id="Seg_3217" s="T177">ogo</ta>
            <ta e="T179" id="Seg_3218" s="T178">maːma-tɨ-gar</ta>
            <ta e="T180" id="Seg_3219" s="T179">kel-en</ta>
            <ta e="T181" id="Seg_3220" s="T180">töhög-ü-ger</ta>
            <ta e="T182" id="Seg_3221" s="T181">olor-but</ta>
            <ta e="T183" id="Seg_3222" s="T182">maːma</ta>
            <ta e="T184" id="Seg_3223" s="T183">min</ta>
            <ta e="T185" id="Seg_3224" s="T184">tɨː-lan-ɨ͡ak-pɨ-n</ta>
            <ta e="T186" id="Seg_3225" s="T185">bagar-a-bɨn</ta>
            <ta e="T187" id="Seg_3226" s="T186">ol</ta>
            <ta e="T188" id="Seg_3227" s="T187">tu͡og=aj</ta>
            <ta e="T189" id="Seg_3228" s="T188">emi͡e</ta>
            <ta e="T190" id="Seg_3229" s="T189">ogonnʼor</ta>
            <ta e="T191" id="Seg_3230" s="T190">kɨːhɨr-bɨt</ta>
            <ta e="T192" id="Seg_3231" s="T191">olor-o</ta>
            <ta e="T193" id="Seg_3232" s="T192">tüh-en</ta>
            <ta e="T194" id="Seg_3233" s="T193">baran</ta>
            <ta e="T195" id="Seg_3234" s="T194">če</ta>
            <ta e="T196" id="Seg_3235" s="T195">ajɨlaːgɨn</ta>
            <ta e="T197" id="Seg_3236" s="T196">tɨː-lan</ta>
            <ta e="T198" id="Seg_3237" s="T197">ɨraːk</ta>
            <ta e="T199" id="Seg_3238" s="T198">ire</ta>
            <ta e="T200" id="Seg_3239" s="T199">bar-ɨ-ma</ta>
            <ta e="T201" id="Seg_3240" s="T200">duː</ta>
            <ta e="T202" id="Seg_3241" s="T201">hu͡ok</ta>
            <ta e="T203" id="Seg_3242" s="T202">bar-ɨ͡a-m</ta>
            <ta e="T204" id="Seg_3243" s="T203">hu͡og-a</ta>
            <ta e="T205" id="Seg_3244" s="T204">ebe</ta>
            <ta e="T206" id="Seg_3245" s="T205">kɨtɨl-ɨ-gar</ta>
            <ta e="T207" id="Seg_3246" s="T206">ire</ta>
            <ta e="T208" id="Seg_3247" s="T207">hɨldʼ-ɨ͡a-m</ta>
            <ta e="T209" id="Seg_3248" s="T208">dʼi͡e-bit</ta>
            <ta e="T210" id="Seg_3249" s="T209">Laːjku</ta>
            <ta e="T211" id="Seg_3250" s="T210">kütür</ta>
            <ta e="T212" id="Seg_3251" s="T211">Laːjku</ta>
            <ta e="T213" id="Seg_3252" s="T212">tɨː-ga</ta>
            <ta e="T214" id="Seg_3253" s="T213">olor-but</ta>
            <ta e="T215" id="Seg_3254" s="T214">orguːjakaːn</ta>
            <ta e="T216" id="Seg_3255" s="T215">kɨtɨl</ta>
            <ta e="T217" id="Seg_3256" s="T216">ustun</ta>
            <ta e="T218" id="Seg_3257" s="T217">erd-i-n-e</ta>
            <ta e="T219" id="Seg_3258" s="T218">hɨldʼ-ɨ-bɨt</ta>
            <ta e="T220" id="Seg_3259" s="T219">ogonnʼor-doːk</ta>
            <ta e="T221" id="Seg_3260" s="T220">emeːksin</ta>
            <ta e="T222" id="Seg_3261" s="T221">dʼi͡e-ge</ta>
            <ta e="T223" id="Seg_3262" s="T222">kiːr-el-leri-n</ta>
            <ta e="T224" id="Seg_3263" s="T223">kɨtta</ta>
            <ta e="T225" id="Seg_3264" s="T224">ɨraːt-an</ta>
            <ta e="T226" id="Seg_3265" s="T225">da</ta>
            <ta e="T227" id="Seg_3266" s="T226">ɨraːt-an</ta>
            <ta e="T228" id="Seg_3267" s="T227">is-pit</ta>
            <ta e="T229" id="Seg_3268" s="T228">bu</ta>
            <ta e="T230" id="Seg_3269" s="T229">ogo</ta>
            <ta e="T231" id="Seg_3270" s="T230">ebe</ta>
            <ta e="T232" id="Seg_3271" s="T231">orto-tu-gar</ta>
            <ta e="T233" id="Seg_3272" s="T232">tij-bit</ta>
            <ta e="T234" id="Seg_3273" s="T233">daganɨ</ta>
            <ta e="T235" id="Seg_3274" s="T234">ulakan</ta>
            <ta e="T236" id="Seg_3275" s="T235">kihi</ta>
            <ta e="T237" id="Seg_3276" s="T236">bu͡ol-but</ta>
            <ta e="T238" id="Seg_3277" s="T237">ogonnʼor</ta>
            <ta e="T239" id="Seg_3278" s="T238">taks-ɨ-bɨt-a</ta>
            <ta e="T240" id="Seg_3279" s="T239">kör-büt-e</ta>
            <ta e="T241" id="Seg_3280" s="T240">u͡ol-a</ta>
            <ta e="T242" id="Seg_3281" s="T241">ɨraːp-pɨt</ta>
            <ta e="T243" id="Seg_3282" s="T242">kaja</ta>
            <ta e="T244" id="Seg_3283" s="T243">kajdi͡ek</ta>
            <ta e="T245" id="Seg_3284" s="T244">bar-a-gɨn</ta>
            <ta e="T246" id="Seg_3285" s="T245">ɨraːt-ɨ-ma</ta>
            <ta e="T247" id="Seg_3286" s="T246">dʼ-iː-bin</ta>
            <ta e="T248" id="Seg_3287" s="T247">uː-ga</ta>
            <ta e="T249" id="Seg_3288" s="T248">tüs-tü-ŋ</ta>
            <ta e="T250" id="Seg_3289" s="T249">Laːjku</ta>
            <ta e="T251" id="Seg_3290" s="T250">ebe</ta>
            <ta e="T252" id="Seg_3291" s="T251">orto-tu-gar</ta>
            <ta e="T253" id="Seg_3292" s="T252">tij-en</ta>
            <ta e="T254" id="Seg_3293" s="T253">baran</ta>
            <ta e="T255" id="Seg_3294" s="T254">ügüleː-bit</ta>
            <ta e="T256" id="Seg_3295" s="T255">ka-ka</ta>
            <ta e="T257" id="Seg_3296" s="T256">min</ta>
            <ta e="T258" id="Seg_3297" s="T257">Laːjku-bun</ta>
            <ta e="T259" id="Seg_3298" s="T258">eːt</ta>
            <ta e="T260" id="Seg_3299" s="T259">ehigi</ta>
            <ta e="T261" id="Seg_3300" s="T260">o-nu</ta>
            <ta e="T262" id="Seg_3301" s="T261">bil-betek-kit</ta>
            <ta e="T263" id="Seg_3302" s="T262">Laːjku</ta>
            <ta e="T264" id="Seg_3303" s="T263">küreː-bit</ta>
            <ta e="T265" id="Seg_3304" s="T264">ogonnʼor-doːk</ta>
            <ta e="T266" id="Seg_3305" s="T265">emeːksin</ta>
            <ta e="T267" id="Seg_3306" s="T266">ölörs-ö</ta>
            <ta e="T268" id="Seg_3307" s="T267">kaːl-bɨt-tar</ta>
            <ta e="T269" id="Seg_3308" s="T268">bu</ta>
            <ta e="T270" id="Seg_3309" s="T269">u͡ol</ta>
            <ta e="T271" id="Seg_3310" s="T270">kaːm-an</ta>
            <ta e="T272" id="Seg_3311" s="T271">is-pit</ta>
            <ta e="T273" id="Seg_3312" s="T272">töhö</ta>
            <ta e="T274" id="Seg_3313" s="T273">bu͡ol-u͡o=j</ta>
            <ta e="T275" id="Seg_3314" s="T274">kör-büt-e</ta>
            <ta e="T276" id="Seg_3315" s="T275">dʼi͡e-ler</ta>
            <ta e="T277" id="Seg_3316" s="T276">tur-al-lar</ta>
            <ta e="T278" id="Seg_3317" s="T277">hɨrga-laːk</ta>
            <ta e="T279" id="Seg_3318" s="T278">taba-lar</ta>
            <ta e="T280" id="Seg_3319" s="T279">baːj-ɨ-ll-a</ta>
            <ta e="T281" id="Seg_3320" s="T280">hɨt-al-lar</ta>
            <ta e="T282" id="Seg_3321" s="T281">biːr</ta>
            <ta e="T283" id="Seg_3322" s="T282">dʼi͡e-ge</ta>
            <ta e="T284" id="Seg_3323" s="T283">kiːr-bit-e</ta>
            <ta e="T285" id="Seg_3324" s="T284">hogotok</ta>
            <ta e="T286" id="Seg_3325" s="T285">dʼaktar</ta>
            <ta e="T287" id="Seg_3326" s="T286">olor-or</ta>
            <ta e="T288" id="Seg_3327" s="T287">Laːjku</ta>
            <ta e="T289" id="Seg_3328" s="T288">čaːj-daː-bɨt</ta>
            <ta e="T290" id="Seg_3329" s="T289">bu</ta>
            <ta e="T291" id="Seg_3330" s="T290">tug=uj</ta>
            <ta e="T292" id="Seg_3331" s="T291">munnʼak</ta>
            <ta e="T293" id="Seg_3332" s="T292">duː</ta>
            <ta e="T294" id="Seg_3333" s="T293">kurum</ta>
            <ta e="T295" id="Seg_3334" s="T294">duː</ta>
            <ta e="T296" id="Seg_3335" s="T295">hu͡ok</ta>
            <ta e="T297" id="Seg_3336" s="T296">bihigi</ta>
            <ta e="T298" id="Seg_3337" s="T297">kineːs-pit</ta>
            <ta e="T299" id="Seg_3338" s="T298">biːr</ta>
            <ta e="T300" id="Seg_3339" s="T299">kɨːs-taːk</ta>
            <ta e="T301" id="Seg_3340" s="T300">ol</ta>
            <ta e="T302" id="Seg_3341" s="T301">kɨːs-ka</ta>
            <ta e="T303" id="Seg_3342" s="T302">kineːs</ta>
            <ta e="T304" id="Seg_3343" s="T303">taːbɨrɨn-ɨ-n</ta>
            <ta e="T305" id="Seg_3344" s="T304">taːj-bɨt</ta>
            <ta e="T306" id="Seg_3345" s="T305">kihi</ta>
            <ta e="T307" id="Seg_3346" s="T306">ire</ta>
            <ta e="T308" id="Seg_3347" s="T307">er</ta>
            <ta e="T309" id="Seg_3348" s="T308">bu͡ol-ar</ta>
            <ta e="T310" id="Seg_3349" s="T309">dʼi͡e-bit</ta>
            <ta e="T311" id="Seg_3350" s="T310">dʼaktar</ta>
            <ta e="T312" id="Seg_3351" s="T311">Laːjku</ta>
            <ta e="T313" id="Seg_3352" s="T312">taks-ɨ-bɨt</ta>
            <ta e="T314" id="Seg_3353" s="T313">da</ta>
            <ta e="T315" id="Seg_3354" s="T314">ebe</ta>
            <ta e="T316" id="Seg_3355" s="T315">kɨrɨː-tɨ-gar</ta>
            <ta e="T317" id="Seg_3356" s="T316">kiːr-bit</ta>
            <ta e="T318" id="Seg_3357" s="T317">ot-u-nan</ta>
            <ta e="T319" id="Seg_3358" s="T318">hap-t-an</ta>
            <ta e="T320" id="Seg_3359" s="T319">baran</ta>
            <ta e="T321" id="Seg_3360" s="T320">hɨp-pɨt</ta>
            <ta e="T322" id="Seg_3361" s="T321">kör-büt-e</ta>
            <ta e="T323" id="Seg_3362" s="T322">ikki</ta>
            <ta e="T324" id="Seg_3363" s="T323">kɨːs</ta>
            <ta e="T325" id="Seg_3364" s="T324">uː</ta>
            <ta e="T326" id="Seg_3365" s="T325">bas-a</ta>
            <ta e="T327" id="Seg_3366" s="T326">ih-el-ler</ta>
            <ta e="T328" id="Seg_3367" s="T327">orguhu͡ok-tan-an</ta>
            <ta e="T329" id="Seg_3368" s="T328">ikki</ta>
            <ta e="T330" id="Seg_3369" s="T329">baːg-ɨ</ta>
            <ta e="T331" id="Seg_3370" s="T330">hannɨ-larɨ-gar</ta>
            <ta e="T332" id="Seg_3371" s="T331">hük-püt-ter</ta>
            <ta e="T333" id="Seg_3372" s="T332">u͡ol</ta>
            <ta e="T334" id="Seg_3373" s="T333">atag-ɨ-ttan</ta>
            <ta e="T335" id="Seg_3374" s="T334">baːj</ta>
            <ta e="T336" id="Seg_3375" s="T335">kɨːs</ta>
            <ta e="T337" id="Seg_3376" s="T336">iŋŋ-en</ta>
            <ta e="T338" id="Seg_3377" s="T337">uː-tu-n</ta>
            <ta e="T339" id="Seg_3378" s="T338">dʼalkɨp-pɨt</ta>
            <ta e="T340" id="Seg_3379" s="T339">bu</ta>
            <ta e="T341" id="Seg_3380" s="T340">barɨta</ta>
            <ta e="T342" id="Seg_3381" s="T341">aga-m</ta>
            <ta e="T343" id="Seg_3382" s="T342">ihin</ta>
            <ta e="T344" id="Seg_3383" s="T343">er-ge</ta>
            <ta e="T345" id="Seg_3384" s="T344">taks-ɨ-bɨt</ta>
            <ta e="T346" id="Seg_3385" s="T345">e-bit-i-m</ta>
            <ta e="T347" id="Seg_3386" s="T346">bu͡ol-lar</ta>
            <ta e="T348" id="Seg_3387" s="T347">uː</ta>
            <ta e="T349" id="Seg_3388" s="T348">bas-a</ta>
            <ta e="T350" id="Seg_3389" s="T349">erejden-e</ta>
            <ta e="T351" id="Seg_3390" s="T350">hɨldʼ-ɨ͡ak</ta>
            <ta e="T352" id="Seg_3391" s="T351">e-ti-m</ta>
            <ta e="T353" id="Seg_3392" s="T352">duː</ta>
            <ta e="T354" id="Seg_3393" s="T353">atak-pɨ-n</ta>
            <ta e="T355" id="Seg_3394" s="T354">ire</ta>
            <ta e="T356" id="Seg_3395" s="T355">ilit-ti-m</ta>
            <ta e="T357" id="Seg_3396" s="T356">dogoː</ta>
            <ta e="T358" id="Seg_3397" s="T357">iti</ta>
            <ta e="T359" id="Seg_3398" s="T358">teːte-ŋ</ta>
            <ta e="T360" id="Seg_3399" s="T359">tug-u</ta>
            <ta e="T361" id="Seg_3400" s="T360">taːt-tar-ar</ta>
            <ta e="T362" id="Seg_3401" s="T361">aga-m</ta>
            <ta e="T363" id="Seg_3402" s="T362">kahan</ta>
            <ta e="T364" id="Seg_3403" s="T363">ire</ta>
            <ta e="T365" id="Seg_3404" s="T364">hette</ta>
            <ta e="T366" id="Seg_3405" s="T365">bɨt</ta>
            <ta e="T367" id="Seg_3406" s="T366">tiriː-ti-nen</ta>
            <ta e="T368" id="Seg_3407" s="T367">dahɨna</ta>
            <ta e="T369" id="Seg_3408" s="T368">oŋor-but</ta>
            <ta e="T370" id="Seg_3409" s="T369">e-t-e</ta>
            <ta e="T371" id="Seg_3410" s="T370">iti-ni</ta>
            <ta e="T372" id="Seg_3411" s="T371">gɨn-ar</ta>
            <ta e="T373" id="Seg_3412" s="T372">e-bit-e</ta>
            <ta e="T374" id="Seg_3413" s="T373">duː</ta>
            <ta e="T375" id="Seg_3414" s="T374">če</ta>
            <ta e="T376" id="Seg_3415" s="T375">bar-ɨ͡ak</ta>
            <ta e="T377" id="Seg_3416" s="T376">uː</ta>
            <ta e="T378" id="Seg_3417" s="T377">da</ta>
            <ta e="T379" id="Seg_3418" s="T378">tuluj-bat</ta>
            <ta e="T380" id="Seg_3419" s="T379">mas</ta>
            <ta e="T381" id="Seg_3420" s="T380">da</ta>
            <ta e="T382" id="Seg_3421" s="T381">tuluj-bat</ta>
            <ta e="T383" id="Seg_3422" s="T382">bačča-laːk</ta>
            <ta e="T384" id="Seg_3423" s="T383">ɨ͡aldʼɨk-ka</ta>
            <ta e="T385" id="Seg_3424" s="T384">Laːjku</ta>
            <ta e="T386" id="Seg_3425" s="T385">tönn-ön</ta>
            <ta e="T387" id="Seg_3426" s="T386">kel-en</ta>
            <ta e="T388" id="Seg_3427" s="T387">maː</ta>
            <ta e="T389" id="Seg_3428" s="T388">dʼi͡e-ti-ger</ta>
            <ta e="T390" id="Seg_3429" s="T389">kiːr-bit</ta>
            <ta e="T391" id="Seg_3430" s="T390">kom-mut</ta>
            <ta e="T392" id="Seg_3431" s="T391">harsi͡erde</ta>
            <ta e="T393" id="Seg_3432" s="T392">tur-but-a</ta>
            <ta e="T394" id="Seg_3433" s="T393">emi͡e</ta>
            <ta e="T395" id="Seg_3434" s="T394">ɨ͡aldʼɨt</ta>
            <ta e="T396" id="Seg_3435" s="T395">toloru</ta>
            <ta e="T397" id="Seg_3436" s="T396">bu</ta>
            <ta e="T398" id="Seg_3437" s="T397">u͡ol</ta>
            <ta e="T399" id="Seg_3438" s="T398">emi͡e</ta>
            <ta e="T400" id="Seg_3439" s="T399">kineːs-ke</ta>
            <ta e="T401" id="Seg_3440" s="T400">bar-ɨːhɨ</ta>
            <ta e="T402" id="Seg_3441" s="T401">ɨ͡aldʼɨt-tar-ɨ</ta>
            <ta e="T403" id="Seg_3442" s="T402">kɨtta</ta>
            <ta e="T404" id="Seg_3443" s="T403">biːrge</ta>
            <ta e="T405" id="Seg_3444" s="T404">kiːr-bit</ta>
            <ta e="T406" id="Seg_3445" s="T405">kör-büt-e</ta>
            <ta e="T407" id="Seg_3446" s="T406">dʼi͡e-ni</ta>
            <ta e="T408" id="Seg_3447" s="T407">toloru</ta>
            <ta e="T409" id="Seg_3448" s="T408">er</ta>
            <ta e="T410" id="Seg_3449" s="T409">kihi</ta>
            <ta e="T411" id="Seg_3450" s="T410">o</ta>
            <ta e="T412" id="Seg_3451" s="T411">bügün</ta>
            <ta e="T413" id="Seg_3452" s="T412">haŋa</ta>
            <ta e="T414" id="Seg_3453" s="T413">ɨ͡aldʼɨt</ta>
            <ta e="T415" id="Seg_3454" s="T414">kel-bit</ta>
            <ta e="T416" id="Seg_3455" s="T415">če</ta>
            <ta e="T417" id="Seg_3456" s="T416">bagar</ta>
            <ta e="T418" id="Seg_3457" s="T417">en</ta>
            <ta e="T419" id="Seg_3458" s="T418">min</ta>
            <ta e="T420" id="Seg_3459" s="T419">taːbɨrɨm-mɨ-n</ta>
            <ta e="T421" id="Seg_3460" s="T420">taːj-ɨ͡a-ŋ</ta>
            <ta e="T422" id="Seg_3461" s="T421">di͡e-bit</ta>
            <ta e="T423" id="Seg_3462" s="T422">kineːs</ta>
            <ta e="T424" id="Seg_3463" s="T423">tu͡ok</ta>
            <ta e="T425" id="Seg_3464" s="T424">taːbɨrɨn-naːk-kɨn=ɨj</ta>
            <ta e="T426" id="Seg_3465" s="T425">töhö</ta>
            <ta e="T427" id="Seg_3466" s="T426">öj-ü-m</ta>
            <ta e="T428" id="Seg_3467" s="T427">kotor-u-n-an</ta>
            <ta e="T429" id="Seg_3468" s="T428">taːj-a</ta>
            <ta e="T430" id="Seg_3469" s="T429">hat-ɨ͡ak</ta>
            <ta e="T431" id="Seg_3470" s="T430">e-ti-m</ta>
            <ta e="T432" id="Seg_3471" s="T431">Laːjku</ta>
            <ta e="T433" id="Seg_3472" s="T432">dʼe-bit</ta>
            <ta e="T434" id="Seg_3473" s="T433">min</ta>
            <ta e="T435" id="Seg_3474" s="T434">aːt-t-a-r-a-bɨn</ta>
            <ta e="T436" id="Seg_3475" s="T435">dahɨna</ta>
            <ta e="T437" id="Seg_3476" s="T436">tug-u-nan</ta>
            <ta e="T438" id="Seg_3477" s="T437">oŋohull-u-but=uj</ta>
            <ta e="T439" id="Seg_3478" s="T438">dʼi͡e-n</ta>
            <ta e="T440" id="Seg_3479" s="T439">kineːs</ta>
            <ta e="T441" id="Seg_3480" s="T440">ɨjɨp-pɨt</ta>
            <ta e="T442" id="Seg_3481" s="T441">öskötün</ta>
            <ta e="T443" id="Seg_3482" s="T442">min</ta>
            <ta e="T444" id="Seg_3483" s="T443">öj-ü-m</ta>
            <ta e="T445" id="Seg_3484" s="T444">bar-ar</ta>
            <ta e="T446" id="Seg_3485" s="T445">hir-i-nen</ta>
            <ta e="T447" id="Seg_3486" s="T446">oŋor-u͡o-ŋ</ta>
            <ta e="T448" id="Seg_3487" s="T447">duː</ta>
            <ta e="T449" id="Seg_3488" s="T448">tiriː</ta>
            <ta e="T450" id="Seg_3489" s="T449">bu͡ol-lag-ɨna</ta>
            <ta e="T451" id="Seg_3490" s="T450">bu͡ol-lun</ta>
            <ta e="T452" id="Seg_3491" s="T451">min</ta>
            <ta e="T453" id="Seg_3492" s="T452">hanaː-ba-r</ta>
            <ta e="T454" id="Seg_3493" s="T453">hette</ta>
            <ta e="T455" id="Seg_3494" s="T454">bɨt</ta>
            <ta e="T456" id="Seg_3495" s="T455">tiriː-ti-nen</ta>
            <ta e="T457" id="Seg_3496" s="T456">onohull-u-but</ta>
            <ta e="T458" id="Seg_3497" s="T457">bɨhɨːlaːk</ta>
            <ta e="T459" id="Seg_3498" s="T458">Laːjku</ta>
            <ta e="T460" id="Seg_3499" s="T459">haŋar-bɨt</ta>
            <ta e="T461" id="Seg_3500" s="T460">o</ta>
            <ta e="T462" id="Seg_3501" s="T461">tu͡ok</ta>
            <ta e="T463" id="Seg_3502" s="T462">öj-döːk</ta>
            <ta e="T464" id="Seg_3503" s="T463">kihi-te-gin=ij</ta>
            <ta e="T465" id="Seg_3504" s="T464">taːj-dɨ-ŋ</ta>
            <ta e="T466" id="Seg_3505" s="T465">dʼiː</ta>
            <ta e="T467" id="Seg_3506" s="T466">če</ta>
            <ta e="T468" id="Seg_3507" s="T467">oččogo</ta>
            <ta e="T469" id="Seg_3508" s="T468">min</ta>
            <ta e="T470" id="Seg_3509" s="T469">kɨːs-pɨ-n</ta>
            <ta e="T471" id="Seg_3510" s="T470">dʼaktar</ta>
            <ta e="T472" id="Seg_3511" s="T471">gɨn-ar</ta>
            <ta e="T473" id="Seg_3512" s="T472">ɨjaːk-taːk-kɨn</ta>
            <ta e="T474" id="Seg_3513" s="T473">e-bit</ta>
            <ta e="T475" id="Seg_3514" s="T474">kajdak</ta>
            <ta e="T476" id="Seg_3515" s="T475">gɨn-ɨ͡a-ŋ=ɨj</ta>
            <ta e="T477" id="Seg_3516" s="T476">harsɨn</ta>
            <ta e="T478" id="Seg_3517" s="T477">kurumn-u͡ok-put</ta>
            <ta e="T479" id="Seg_3518" s="T478">Laːjku</ta>
            <ta e="T480" id="Seg_3519" s="T479">baːj</ta>
            <ta e="T481" id="Seg_3520" s="T480">kineːs</ta>
            <ta e="T482" id="Seg_3521" s="T481">bosku͡oj</ta>
            <ta e="T483" id="Seg_3522" s="T482">kɨːh-ɨ-n</ta>
            <ta e="T484" id="Seg_3523" s="T483">dʼaktar-dan-an</ta>
            <ta e="T485" id="Seg_3524" s="T484">baj-an-tot-on</ta>
            <ta e="T486" id="Seg_3525" s="T485">olor-but</ta>
            <ta e="T487" id="Seg_3526" s="T486">tu͡ok</ta>
            <ta e="T488" id="Seg_3527" s="T487">da</ta>
            <ta e="T489" id="Seg_3528" s="T488">erej-i</ta>
            <ta e="T490" id="Seg_3529" s="T489">bil-bekke</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_3530" s="T0">hogotogun</ta>
            <ta e="T2" id="Seg_3531" s="T1">ü͡öskeː-BIT</ta>
            <ta e="T3" id="Seg_3532" s="T2">inʼe-tI-n-aga-tI-n</ta>
            <ta e="T4" id="Seg_3533" s="T3">öjdöː-BAT</ta>
            <ta e="T5" id="Seg_3534" s="T4">Laːjku</ta>
            <ta e="T6" id="Seg_3535" s="T5">dʼi͡e-An</ta>
            <ta e="T7" id="Seg_3536" s="T6">ɨt</ta>
            <ta e="T8" id="Seg_3537" s="T7">olor-BIT</ta>
            <ta e="T9" id="Seg_3538" s="T8">biːrde</ta>
            <ta e="T10" id="Seg_3539" s="T9">kaːm-A</ta>
            <ta e="T11" id="Seg_3540" s="T10">hɨrɨt-I-BIT</ta>
            <ta e="T12" id="Seg_3541" s="T11">kihi</ta>
            <ta e="T13" id="Seg_3542" s="T12">kördöː-A</ta>
            <ta e="T14" id="Seg_3543" s="T13">ol</ta>
            <ta e="T15" id="Seg_3544" s="T14">hɨrɨt-An</ta>
            <ta e="T16" id="Seg_3545" s="T15">kör-BIT</ta>
            <ta e="T17" id="Seg_3546" s="T16">ürdük</ta>
            <ta e="T18" id="Seg_3547" s="T17">hu͡opka-nI</ta>
            <ta e="T19" id="Seg_3548" s="T18">tu͡ok</ta>
            <ta e="T20" id="Seg_3549" s="T19">bu͡ol-IAK.[tA]=Ij</ta>
            <ta e="T21" id="Seg_3550" s="T20">min</ta>
            <ta e="T22" id="Seg_3551" s="T21">itinne</ta>
            <ta e="T23" id="Seg_3552" s="T22">ɨtɨn-TAK-BInA</ta>
            <ta e="T24" id="Seg_3553" s="T23">di͡e-A</ta>
            <ta e="T25" id="Seg_3554" s="T24">hanaː-BIT</ta>
            <ta e="T26" id="Seg_3555" s="T25">hu͡opka</ta>
            <ta e="T27" id="Seg_3556" s="T26">ürüt-tI-GAr</ta>
            <ta e="T28" id="Seg_3557" s="T27">ɨtɨn-An</ta>
            <ta e="T29" id="Seg_3558" s="T28">kör-BIT-tA</ta>
            <ta e="T30" id="Seg_3559" s="T29">Dʼige baːba</ta>
            <ta e="T31" id="Seg_3560" s="T30">is-Ar</ta>
            <ta e="T32" id="Seg_3561" s="T31">biːrges</ta>
            <ta e="T33" id="Seg_3562" s="T32">di͡egi</ta>
            <ta e="T34" id="Seg_3563" s="T33">örüt-tI-ttAn</ta>
            <ta e="T35" id="Seg_3564" s="T34">hu͡opka</ta>
            <ta e="T36" id="Seg_3565" s="T35">orto-tI-GAr</ta>
            <ta e="T37" id="Seg_3566" s="T36">tij-BIT</ta>
            <ta e="T38" id="Seg_3567" s="T37">da</ta>
            <ta e="T39" id="Seg_3568" s="T38">čokčoj-s</ta>
            <ta e="T40" id="Seg_3569" s="T39">gɨn-BIT</ta>
            <ta e="T41" id="Seg_3570" s="T40">Laːjku</ta>
            <ta e="T42" id="Seg_3571" s="T41">kubulgat</ta>
            <ta e="T43" id="Seg_3572" s="T42">bagajɨ</ta>
            <ta e="T44" id="Seg_3573" s="T43">e-TI-tA</ta>
            <ta e="T45" id="Seg_3574" s="T44">e-BIT</ta>
            <ta e="T46" id="Seg_3575" s="T45">hahɨl</ta>
            <ta e="T47" id="Seg_3576" s="T46">ogo</ta>
            <ta e="T48" id="Seg_3577" s="T47">bu͡ol-An</ta>
            <ta e="T49" id="Seg_3578" s="T48">baran</ta>
            <ta e="T50" id="Seg_3579" s="T49">hu͡opka</ta>
            <ta e="T51" id="Seg_3580" s="T50">alɨn-tI-n</ta>
            <ta e="T52" id="Seg_3581" s="T51">dek</ta>
            <ta e="T53" id="Seg_3582" s="T52">čekenij-An</ta>
            <ta e="T54" id="Seg_3583" s="T53">kaːl-BIT</ta>
            <ta e="T55" id="Seg_3584" s="T54">o</ta>
            <ta e="T56" id="Seg_3585" s="T55">anʼɨː</ta>
            <ta e="T57" id="Seg_3586" s="T56">da</ta>
            <ta e="T58" id="Seg_3587" s="T57">kajdak</ta>
            <ta e="T59" id="Seg_3588" s="T58">ogo-LAN-Ar-BI-n</ta>
            <ta e="T60" id="Seg_3589" s="T59">bil-BAkkA</ta>
            <ta e="T61" id="Seg_3590" s="T60">kaːl-BIT-BIn=Ij</ta>
            <ta e="T62" id="Seg_3591" s="T61">dʼi͡e-BIT</ta>
            <ta e="T63" id="Seg_3592" s="T62">da</ta>
            <ta e="T64" id="Seg_3593" s="T63">ogo-nI</ta>
            <ta e="T65" id="Seg_3594" s="T64">huːlaː-A</ta>
            <ta e="T66" id="Seg_3595" s="T65">tut-An</ta>
            <ta e="T67" id="Seg_3596" s="T66">baran</ta>
            <ta e="T68" id="Seg_3597" s="T67">dʼi͡e-tI-GAr</ta>
            <ta e="T69" id="Seg_3598" s="T68">ilt-A</ta>
            <ta e="T70" id="Seg_3599" s="T69">bar-BIT</ta>
            <ta e="T71" id="Seg_3600" s="T70">Dʼige baːba</ta>
            <ta e="T72" id="Seg_3601" s="T71">ogonnʼor-tI-n</ta>
            <ta e="T73" id="Seg_3602" s="T72">kɨtta</ta>
            <ta e="T74" id="Seg_3603" s="T73">ebe</ta>
            <ta e="T75" id="Seg_3604" s="T74">kɨtɨl-tI-GAr</ta>
            <ta e="T76" id="Seg_3605" s="T75">olor-Ar</ta>
            <ta e="T77" id="Seg_3606" s="T76">e-BIT-LAr</ta>
            <ta e="T78" id="Seg_3607" s="T77">ogo-tA</ta>
            <ta e="T79" id="Seg_3608" s="T78">hu͡ok-LAr</ta>
            <ta e="T80" id="Seg_3609" s="T79">e-BIT</ta>
            <ta e="T81" id="Seg_3610" s="T80">čogotok</ta>
            <ta e="T82" id="Seg_3611" s="T81">aːku</ta>
            <ta e="T83" id="Seg_3612" s="T82">taba-LAːK-LAr</ta>
            <ta e="T84" id="Seg_3613" s="T83">ogo-LArA</ta>
            <ta e="T85" id="Seg_3614" s="T84">künnete</ta>
            <ta e="T86" id="Seg_3615" s="T85">ulaːt-An</ta>
            <ta e="T87" id="Seg_3616" s="T86">is-BIT</ta>
            <ta e="T88" id="Seg_3617" s="T87">haŋa-LAN-BIT</ta>
            <ta e="T89" id="Seg_3618" s="T88">inʼe-tA</ta>
            <ta e="T90" id="Seg_3619" s="T89">beje-tI-ttAn</ta>
            <ta e="T91" id="Seg_3620" s="T90">hokuj-LIːN</ta>
            <ta e="T92" id="Seg_3621" s="T91">taŋas</ta>
            <ta e="T93" id="Seg_3622" s="T92">tik-BIT</ta>
            <ta e="T94" id="Seg_3623" s="T93">bu</ta>
            <ta e="T95" id="Seg_3624" s="T94">ogo</ta>
            <ta e="T96" id="Seg_3625" s="T95">hüːreleː-A</ta>
            <ta e="T97" id="Seg_3626" s="T96">hɨrɨt-Ar</ta>
            <ta e="T98" id="Seg_3627" s="T97">bu͡ol-BIT</ta>
            <ta e="T99" id="Seg_3628" s="T98">dʼi͡e-tI-n</ta>
            <ta e="T100" id="Seg_3629" s="T99">attɨ-GAr</ta>
            <ta e="T101" id="Seg_3630" s="T100">ogonnʼor</ta>
            <ta e="T102" id="Seg_3631" s="T101">kün</ta>
            <ta e="T103" id="Seg_3632" s="T102">aːjɨ</ta>
            <ta e="T104" id="Seg_3633" s="T103">ilim-LAN-Ar</ta>
            <ta e="T105" id="Seg_3634" s="T104">ide-LAːK</ta>
            <ta e="T106" id="Seg_3635" s="T105">e-BIT</ta>
            <ta e="T107" id="Seg_3636" s="T106">bu</ta>
            <ta e="T108" id="Seg_3637" s="T107">ogo-nI</ta>
            <ta e="T109" id="Seg_3638" s="T108">balɨk-I-nAn</ta>
            <ta e="T110" id="Seg_3639" s="T109">agaj</ta>
            <ta e="T111" id="Seg_3640" s="T110">ahaː-A-t-Ar</ta>
            <ta e="T112" id="Seg_3641" s="T111">e-BIT-LAr</ta>
            <ta e="T113" id="Seg_3642" s="T112">Laːjku</ta>
            <ta e="T114" id="Seg_3643" s="T113">biːrde</ta>
            <ta e="T115" id="Seg_3644" s="T114">dʼe-BIT</ta>
            <ta e="T116" id="Seg_3645" s="T115">maːma</ta>
            <ta e="T117" id="Seg_3646" s="T116">min</ta>
            <ta e="T118" id="Seg_3647" s="T117">et</ta>
            <ta e="T119" id="Seg_3648" s="T118">hi͡e-IAK-BI-n</ta>
            <ta e="T120" id="Seg_3649" s="T119">bagar-A-BIn</ta>
            <ta e="T121" id="Seg_3650" s="T120">ogonnʼor</ta>
            <ta e="T122" id="Seg_3651" s="T121">dʼe</ta>
            <ta e="T123" id="Seg_3652" s="T122">olor</ta>
            <ta e="T124" id="Seg_3653" s="T123">baːngaːj-GI-n</ta>
            <ta e="T125" id="Seg_3654" s="T124">ogo</ta>
            <ta e="T126" id="Seg_3655" s="T125">et</ta>
            <ta e="T127" id="Seg_3656" s="T126">h-IAK.[tI]-n</ta>
            <ta e="T128" id="Seg_3657" s="T127">bagar-BIT</ta>
            <ta e="T129" id="Seg_3658" s="T128">ogonnʼor</ta>
            <ta e="T130" id="Seg_3659" s="T129">taba-tI-n</ta>
            <ta e="T131" id="Seg_3660" s="T130">ölör-An</ta>
            <ta e="T132" id="Seg_3661" s="T131">keːs-BIT</ta>
            <ta e="T133" id="Seg_3662" s="T132">Laːjku</ta>
            <ta e="T134" id="Seg_3663" s="T133">kubulgat</ta>
            <ta e="T135" id="Seg_3664" s="T134">baga-tA</ta>
            <ta e="T136" id="Seg_3665" s="T135">ulaːt-An</ta>
            <ta e="T137" id="Seg_3666" s="T136">is-BIT</ta>
            <ta e="T138" id="Seg_3667" s="T137">maːma</ta>
            <ta e="T139" id="Seg_3668" s="T138">togo</ta>
            <ta e="T140" id="Seg_3669" s="T139">ebe</ta>
            <ta e="T141" id="Seg_3670" s="T140">onu͡or</ta>
            <ta e="T142" id="Seg_3671" s="T141">tas-I-n-BAT-BIt</ta>
            <ta e="T143" id="Seg_3672" s="T142">onno</ta>
            <ta e="T144" id="Seg_3673" s="T143">kiti͡eme</ta>
            <ta e="T145" id="Seg_3674" s="T144">bagajɨ</ta>
            <ta e="T146" id="Seg_3675" s="T145">bɨhɨːlaːk</ta>
            <ta e="T147" id="Seg_3676" s="T146">dʼi͡e-BIT</ta>
            <ta e="T148" id="Seg_3677" s="T147">kaja</ta>
            <ta e="T149" id="Seg_3678" s="T148">ogonnʼor</ta>
            <ta e="T150" id="Seg_3679" s="T149">ihit-A-GIn</ta>
            <ta e="T151" id="Seg_3680" s="T150">du͡o</ta>
            <ta e="T152" id="Seg_3681" s="T151">ogo</ta>
            <ta e="T153" id="Seg_3682" s="T152">haŋa-tI-n</ta>
            <ta e="T154" id="Seg_3683" s="T153">kirdik-nI</ta>
            <ta e="T155" id="Seg_3684" s="T154">gɨn-Ar</ta>
            <ta e="T156" id="Seg_3685" s="T155">kannɨk-GA</ta>
            <ta e="T157" id="Seg_3686" s="T156">di͡eri</ta>
            <ta e="T158" id="Seg_3687" s="T157">biːr</ta>
            <ta e="T159" id="Seg_3688" s="T158">hir-GA</ta>
            <ta e="T160" id="Seg_3689" s="T159">olor-IAK-BIt=Ij</ta>
            <ta e="T161" id="Seg_3690" s="T160">kös-IAk</ta>
            <ta e="T162" id="Seg_3691" s="T161">ogonnʼor</ta>
            <ta e="T163" id="Seg_3692" s="T162">öčös-BAkkA</ta>
            <ta e="T164" id="Seg_3693" s="T163">kihi-LAr-tI-n</ta>
            <ta e="T165" id="Seg_3694" s="T164">onu͡or</ta>
            <ta e="T166" id="Seg_3695" s="T165">tahaːr-An</ta>
            <ta e="T167" id="Seg_3696" s="T166">keːs-BIT</ta>
            <ta e="T168" id="Seg_3697" s="T167">tɨː-nAn</ta>
            <ta e="T169" id="Seg_3698" s="T168">Laːjku</ta>
            <ta e="T170" id="Seg_3699" s="T169">ür-A-ür-A</ta>
            <ta e="T171" id="Seg_3700" s="T170">hüːr-A</ta>
            <ta e="T172" id="Seg_3701" s="T171">hɨrɨt-I-BIT</ta>
            <ta e="T173" id="Seg_3702" s="T172">ki͡ehe-LIk</ta>
            <ta e="T174" id="Seg_3703" s="T173">tas-I-n-An</ta>
            <ta e="T175" id="Seg_3704" s="T174">büt-Ar-LArI-n</ta>
            <ta e="T176" id="Seg_3705" s="T175">kɨtta</ta>
            <ta e="T177" id="Seg_3706" s="T176">bu</ta>
            <ta e="T178" id="Seg_3707" s="T177">ogo</ta>
            <ta e="T179" id="Seg_3708" s="T178">maːma-tI-GAr</ta>
            <ta e="T180" id="Seg_3709" s="T179">kel-An</ta>
            <ta e="T181" id="Seg_3710" s="T180">töhök-tI-GAr</ta>
            <ta e="T182" id="Seg_3711" s="T181">olor-BIT</ta>
            <ta e="T183" id="Seg_3712" s="T182">maːma</ta>
            <ta e="T184" id="Seg_3713" s="T183">min</ta>
            <ta e="T185" id="Seg_3714" s="T184">tɨː-LAN-IAK-BI-n</ta>
            <ta e="T186" id="Seg_3715" s="T185">bagar-A-BIn</ta>
            <ta e="T187" id="Seg_3716" s="T186">ol</ta>
            <ta e="T188" id="Seg_3717" s="T187">tu͡ok=Ij</ta>
            <ta e="T189" id="Seg_3718" s="T188">emi͡e</ta>
            <ta e="T190" id="Seg_3719" s="T189">ogonnʼor</ta>
            <ta e="T191" id="Seg_3720" s="T190">kɨːhɨr-BIT</ta>
            <ta e="T192" id="Seg_3721" s="T191">olor-A</ta>
            <ta e="T193" id="Seg_3722" s="T192">tüs-An</ta>
            <ta e="T194" id="Seg_3723" s="T193">baran</ta>
            <ta e="T195" id="Seg_3724" s="T194">dʼe</ta>
            <ta e="T196" id="Seg_3725" s="T195">ajɨlaːgɨn</ta>
            <ta e="T197" id="Seg_3726" s="T196">tɨː-LAN</ta>
            <ta e="T198" id="Seg_3727" s="T197">ɨraːk</ta>
            <ta e="T199" id="Seg_3728" s="T198">ere</ta>
            <ta e="T200" id="Seg_3729" s="T199">bar-I-m</ta>
            <ta e="T201" id="Seg_3730" s="T200">du͡o</ta>
            <ta e="T202" id="Seg_3731" s="T201">hu͡ok</ta>
            <ta e="T203" id="Seg_3732" s="T202">bar-IAK-m</ta>
            <ta e="T204" id="Seg_3733" s="T203">hu͡ok-tA</ta>
            <ta e="T205" id="Seg_3734" s="T204">ebe</ta>
            <ta e="T206" id="Seg_3735" s="T205">kɨtɨl-tI-GAr</ta>
            <ta e="T207" id="Seg_3736" s="T206">ere</ta>
            <ta e="T208" id="Seg_3737" s="T207">hɨrɨt-IAK-m</ta>
            <ta e="T209" id="Seg_3738" s="T208">dʼi͡e-BIT</ta>
            <ta e="T210" id="Seg_3739" s="T209">Laːjku</ta>
            <ta e="T211" id="Seg_3740" s="T210">kütür</ta>
            <ta e="T212" id="Seg_3741" s="T211">Laːjku</ta>
            <ta e="T213" id="Seg_3742" s="T212">tɨː-GA</ta>
            <ta e="T214" id="Seg_3743" s="T213">olor-BIT</ta>
            <ta e="T215" id="Seg_3744" s="T214">orgujakaːn</ta>
            <ta e="T216" id="Seg_3745" s="T215">kɨtɨl</ta>
            <ta e="T217" id="Seg_3746" s="T216">üstün</ta>
            <ta e="T218" id="Seg_3747" s="T217">ert-I-n-A</ta>
            <ta e="T219" id="Seg_3748" s="T218">hɨrɨt-I-BIT</ta>
            <ta e="T220" id="Seg_3749" s="T219">ogonnʼor-LAːK</ta>
            <ta e="T221" id="Seg_3750" s="T220">emeːksin</ta>
            <ta e="T222" id="Seg_3751" s="T221">dʼi͡e-GA</ta>
            <ta e="T223" id="Seg_3752" s="T222">kiːr-Ar-LArI-n</ta>
            <ta e="T224" id="Seg_3753" s="T223">kɨtta</ta>
            <ta e="T225" id="Seg_3754" s="T224">ɨraːt-An</ta>
            <ta e="T226" id="Seg_3755" s="T225">da</ta>
            <ta e="T227" id="Seg_3756" s="T226">ɨraːt-An</ta>
            <ta e="T228" id="Seg_3757" s="T227">is-BIT</ta>
            <ta e="T229" id="Seg_3758" s="T228">bu</ta>
            <ta e="T230" id="Seg_3759" s="T229">ogo</ta>
            <ta e="T231" id="Seg_3760" s="T230">ebe</ta>
            <ta e="T232" id="Seg_3761" s="T231">orto-tI-GAr</ta>
            <ta e="T233" id="Seg_3762" s="T232">tij-BIT</ta>
            <ta e="T234" id="Seg_3763" s="T233">daːganɨ</ta>
            <ta e="T235" id="Seg_3764" s="T234">ulakan</ta>
            <ta e="T236" id="Seg_3765" s="T235">kihi</ta>
            <ta e="T237" id="Seg_3766" s="T236">bu͡ol-BIT</ta>
            <ta e="T238" id="Seg_3767" s="T237">ogonnʼor</ta>
            <ta e="T239" id="Seg_3768" s="T238">tagɨs-I-BIT-tA</ta>
            <ta e="T240" id="Seg_3769" s="T239">kör-BIT-tA</ta>
            <ta e="T241" id="Seg_3770" s="T240">u͡ol-tA</ta>
            <ta e="T242" id="Seg_3771" s="T241">ɨraːt-BIT</ta>
            <ta e="T243" id="Seg_3772" s="T242">kaja</ta>
            <ta e="T244" id="Seg_3773" s="T243">kajdi͡ek</ta>
            <ta e="T245" id="Seg_3774" s="T244">bar-A-GIn</ta>
            <ta e="T246" id="Seg_3775" s="T245">ɨraːt-I-BA</ta>
            <ta e="T247" id="Seg_3776" s="T246">di͡e-A-BIn</ta>
            <ta e="T248" id="Seg_3777" s="T247">uː-GA</ta>
            <ta e="T249" id="Seg_3778" s="T248">tüs-TI-ŋ</ta>
            <ta e="T250" id="Seg_3779" s="T249">Laːjku</ta>
            <ta e="T251" id="Seg_3780" s="T250">ebe</ta>
            <ta e="T252" id="Seg_3781" s="T251">orto-tI-GAr</ta>
            <ta e="T253" id="Seg_3782" s="T252">tij-An</ta>
            <ta e="T254" id="Seg_3783" s="T253">baran</ta>
            <ta e="T255" id="Seg_3784" s="T254">ü͡ögüleː-BIT</ta>
            <ta e="T256" id="Seg_3785" s="T255">ha-haː</ta>
            <ta e="T257" id="Seg_3786" s="T256">min</ta>
            <ta e="T258" id="Seg_3787" s="T257">Laːjku-BIn</ta>
            <ta e="T259" id="Seg_3788" s="T258">eːt</ta>
            <ta e="T260" id="Seg_3789" s="T259">ehigi</ta>
            <ta e="T261" id="Seg_3790" s="T260">ol-nI</ta>
            <ta e="T262" id="Seg_3791" s="T261">bil-BAtAK-GIt</ta>
            <ta e="T263" id="Seg_3792" s="T262">Laːjku</ta>
            <ta e="T264" id="Seg_3793" s="T263">küreː-BIT</ta>
            <ta e="T265" id="Seg_3794" s="T264">ogonnʼor-LAːK</ta>
            <ta e="T266" id="Seg_3795" s="T265">emeːksin</ta>
            <ta e="T267" id="Seg_3796" s="T266">ölörös-A</ta>
            <ta e="T268" id="Seg_3797" s="T267">kaːl-BIT-LAr</ta>
            <ta e="T269" id="Seg_3798" s="T268">bu</ta>
            <ta e="T270" id="Seg_3799" s="T269">u͡ol</ta>
            <ta e="T271" id="Seg_3800" s="T270">kaːm-An</ta>
            <ta e="T272" id="Seg_3801" s="T271">is-BIT</ta>
            <ta e="T273" id="Seg_3802" s="T272">töhö</ta>
            <ta e="T274" id="Seg_3803" s="T273">bu͡ol-IAK.[tA]=Ij</ta>
            <ta e="T275" id="Seg_3804" s="T274">kör-BIT-tA</ta>
            <ta e="T276" id="Seg_3805" s="T275">dʼi͡e-LAr</ta>
            <ta e="T277" id="Seg_3806" s="T276">tur-Ar-LAr</ta>
            <ta e="T278" id="Seg_3807" s="T277">hɨrga-LAːK</ta>
            <ta e="T279" id="Seg_3808" s="T278">taba-LAr</ta>
            <ta e="T280" id="Seg_3809" s="T279">baːj-I-LIN-A</ta>
            <ta e="T281" id="Seg_3810" s="T280">hɨt-Ar-LAr</ta>
            <ta e="T282" id="Seg_3811" s="T281">biːr</ta>
            <ta e="T283" id="Seg_3812" s="T282">dʼi͡e-GA</ta>
            <ta e="T284" id="Seg_3813" s="T283">kiːr-BIT-tA</ta>
            <ta e="T285" id="Seg_3814" s="T284">čogotok</ta>
            <ta e="T286" id="Seg_3815" s="T285">dʼaktar</ta>
            <ta e="T287" id="Seg_3816" s="T286">olor-Ar</ta>
            <ta e="T288" id="Seg_3817" s="T287">Laːjku</ta>
            <ta e="T289" id="Seg_3818" s="T288">čaːj-LAː-BIT</ta>
            <ta e="T290" id="Seg_3819" s="T289">bu</ta>
            <ta e="T291" id="Seg_3820" s="T290">tu͡ok=Ij</ta>
            <ta e="T292" id="Seg_3821" s="T291">munnʼak</ta>
            <ta e="T293" id="Seg_3822" s="T292">du͡o</ta>
            <ta e="T294" id="Seg_3823" s="T293">kurum</ta>
            <ta e="T295" id="Seg_3824" s="T294">du͡o</ta>
            <ta e="T296" id="Seg_3825" s="T295">hu͡ok</ta>
            <ta e="T297" id="Seg_3826" s="T296">bihigi</ta>
            <ta e="T298" id="Seg_3827" s="T297">kineːs-BIt</ta>
            <ta e="T299" id="Seg_3828" s="T298">biːr</ta>
            <ta e="T300" id="Seg_3829" s="T299">kɨːs-LAːK</ta>
            <ta e="T301" id="Seg_3830" s="T300">ol</ta>
            <ta e="T302" id="Seg_3831" s="T301">kɨːs-GA</ta>
            <ta e="T303" id="Seg_3832" s="T302">kineːs</ta>
            <ta e="T304" id="Seg_3833" s="T303">taːburun-tI-n</ta>
            <ta e="T305" id="Seg_3834" s="T304">taːj-BIT</ta>
            <ta e="T306" id="Seg_3835" s="T305">kihi</ta>
            <ta e="T307" id="Seg_3836" s="T306">ere</ta>
            <ta e="T308" id="Seg_3837" s="T307">er</ta>
            <ta e="T309" id="Seg_3838" s="T308">bu͡ol-Ar</ta>
            <ta e="T310" id="Seg_3839" s="T309">dʼi͡e-BIT</ta>
            <ta e="T311" id="Seg_3840" s="T310">dʼaktar</ta>
            <ta e="T312" id="Seg_3841" s="T311">Laːjku</ta>
            <ta e="T313" id="Seg_3842" s="T312">tagɨs-I-BIT</ta>
            <ta e="T314" id="Seg_3843" s="T313">da</ta>
            <ta e="T315" id="Seg_3844" s="T314">ebe</ta>
            <ta e="T316" id="Seg_3845" s="T315">kɨrɨː-tI-GAr</ta>
            <ta e="T317" id="Seg_3846" s="T316">kiːr-BIT</ta>
            <ta e="T318" id="Seg_3847" s="T317">ot-I-nAn</ta>
            <ta e="T319" id="Seg_3848" s="T318">hap-n-An</ta>
            <ta e="T320" id="Seg_3849" s="T319">baran</ta>
            <ta e="T321" id="Seg_3850" s="T320">hɨt-BIT</ta>
            <ta e="T322" id="Seg_3851" s="T321">kör-BIT-tA</ta>
            <ta e="T323" id="Seg_3852" s="T322">ikki</ta>
            <ta e="T324" id="Seg_3853" s="T323">kɨːs</ta>
            <ta e="T325" id="Seg_3854" s="T324">uː</ta>
            <ta e="T326" id="Seg_3855" s="T325">bas-A</ta>
            <ta e="T327" id="Seg_3856" s="T326">is-Ar-LAr</ta>
            <ta e="T328" id="Seg_3857" s="T327">orguhu͡ok-LAN-An</ta>
            <ta e="T329" id="Seg_3858" s="T328">ikki</ta>
            <ta e="T330" id="Seg_3859" s="T329">baːg-nI</ta>
            <ta e="T331" id="Seg_3860" s="T330">hannɨ-LArI-GAr</ta>
            <ta e="T332" id="Seg_3861" s="T331">hük-BIT-LAr</ta>
            <ta e="T333" id="Seg_3862" s="T332">u͡ol</ta>
            <ta e="T334" id="Seg_3863" s="T333">atak-tI-ttAn</ta>
            <ta e="T335" id="Seg_3864" s="T334">baːj</ta>
            <ta e="T336" id="Seg_3865" s="T335">kɨːs</ta>
            <ta e="T337" id="Seg_3866" s="T336">iŋin-An</ta>
            <ta e="T338" id="Seg_3867" s="T337">uː-tI-n</ta>
            <ta e="T339" id="Seg_3868" s="T338">dʼalkɨt-BIT</ta>
            <ta e="T340" id="Seg_3869" s="T339">bu</ta>
            <ta e="T341" id="Seg_3870" s="T340">barɨta</ta>
            <ta e="T342" id="Seg_3871" s="T341">aga-m</ta>
            <ta e="T343" id="Seg_3872" s="T342">ihin</ta>
            <ta e="T344" id="Seg_3873" s="T343">er-GA</ta>
            <ta e="T345" id="Seg_3874" s="T344">tagɨs-I-BIT</ta>
            <ta e="T346" id="Seg_3875" s="T345">e-BIT-I-m</ta>
            <ta e="T347" id="Seg_3876" s="T346">bu͡ol-TAR</ta>
            <ta e="T348" id="Seg_3877" s="T347">uː</ta>
            <ta e="T349" id="Seg_3878" s="T348">bas-A</ta>
            <ta e="T350" id="Seg_3879" s="T349">erejden-A</ta>
            <ta e="T351" id="Seg_3880" s="T350">hɨrɨt-IAK</ta>
            <ta e="T352" id="Seg_3881" s="T351">e-TI-m</ta>
            <ta e="T353" id="Seg_3882" s="T352">du͡o</ta>
            <ta e="T354" id="Seg_3883" s="T353">atak-BI-n</ta>
            <ta e="T355" id="Seg_3884" s="T354">ere</ta>
            <ta e="T356" id="Seg_3885" s="T355">ilit-TI-m</ta>
            <ta e="T357" id="Seg_3886" s="T356">dogor</ta>
            <ta e="T358" id="Seg_3887" s="T357">iti</ta>
            <ta e="T359" id="Seg_3888" s="T358">teːte-ŋ</ta>
            <ta e="T360" id="Seg_3889" s="T359">tu͡ok-nI</ta>
            <ta e="T361" id="Seg_3890" s="T360">taːj-TAr-Ar</ta>
            <ta e="T362" id="Seg_3891" s="T361">aga-m</ta>
            <ta e="T363" id="Seg_3892" s="T362">kahan</ta>
            <ta e="T364" id="Seg_3893" s="T363">ere</ta>
            <ta e="T365" id="Seg_3894" s="T364">hette</ta>
            <ta e="T366" id="Seg_3895" s="T365">bɨt</ta>
            <ta e="T367" id="Seg_3896" s="T366">tiriː-tI-nAn</ta>
            <ta e="T368" id="Seg_3897" s="T367">dahɨna</ta>
            <ta e="T369" id="Seg_3898" s="T368">oŋor-BIT</ta>
            <ta e="T370" id="Seg_3899" s="T369">e-TI-tA</ta>
            <ta e="T371" id="Seg_3900" s="T370">iti-nI</ta>
            <ta e="T372" id="Seg_3901" s="T371">gɨn-Ar</ta>
            <ta e="T373" id="Seg_3902" s="T372">e-BIT-tA</ta>
            <ta e="T374" id="Seg_3903" s="T373">du͡o</ta>
            <ta e="T375" id="Seg_3904" s="T374">dʼe</ta>
            <ta e="T376" id="Seg_3905" s="T375">bar-IAk</ta>
            <ta e="T377" id="Seg_3906" s="T376">uː</ta>
            <ta e="T378" id="Seg_3907" s="T377">da</ta>
            <ta e="T379" id="Seg_3908" s="T378">tuluj-BAT</ta>
            <ta e="T380" id="Seg_3909" s="T379">mas</ta>
            <ta e="T381" id="Seg_3910" s="T380">da</ta>
            <ta e="T382" id="Seg_3911" s="T381">tuluj-BAT</ta>
            <ta e="T383" id="Seg_3912" s="T382">bačča-LAːK</ta>
            <ta e="T384" id="Seg_3913" s="T383">ɨ͡aldʼɨt-GA</ta>
            <ta e="T385" id="Seg_3914" s="T384">Laːjku</ta>
            <ta e="T386" id="Seg_3915" s="T385">tönün-An</ta>
            <ta e="T387" id="Seg_3916" s="T386">kel-An</ta>
            <ta e="T388" id="Seg_3917" s="T387">bu</ta>
            <ta e="T389" id="Seg_3918" s="T388">dʼi͡e-tI-GAr</ta>
            <ta e="T390" id="Seg_3919" s="T389">kiːr-BIT</ta>
            <ta e="T391" id="Seg_3920" s="T390">kon-BIT</ta>
            <ta e="T392" id="Seg_3921" s="T391">harsi͡erda</ta>
            <ta e="T393" id="Seg_3922" s="T392">tur-BIT-tA</ta>
            <ta e="T394" id="Seg_3923" s="T393">emi͡e</ta>
            <ta e="T395" id="Seg_3924" s="T394">ɨ͡aldʼɨt</ta>
            <ta e="T396" id="Seg_3925" s="T395">toloru</ta>
            <ta e="T397" id="Seg_3926" s="T396">bu</ta>
            <ta e="T398" id="Seg_3927" s="T397">u͡ol</ta>
            <ta e="T399" id="Seg_3928" s="T398">emi͡e</ta>
            <ta e="T400" id="Seg_3929" s="T399">kineːs-GA</ta>
            <ta e="T401" id="Seg_3930" s="T400">bar-IːhI</ta>
            <ta e="T402" id="Seg_3931" s="T401">ɨ͡aldʼɨt-LAr-nI</ta>
            <ta e="T403" id="Seg_3932" s="T402">kɨtta</ta>
            <ta e="T404" id="Seg_3933" s="T403">biːrge</ta>
            <ta e="T405" id="Seg_3934" s="T404">kiːr-BIT</ta>
            <ta e="T406" id="Seg_3935" s="T405">kör-BIT-tA</ta>
            <ta e="T407" id="Seg_3936" s="T406">dʼi͡e-nI</ta>
            <ta e="T408" id="Seg_3937" s="T407">toloru</ta>
            <ta e="T409" id="Seg_3938" s="T408">er</ta>
            <ta e="T410" id="Seg_3939" s="T409">kihi</ta>
            <ta e="T411" id="Seg_3940" s="T410">o</ta>
            <ta e="T412" id="Seg_3941" s="T411">bügün</ta>
            <ta e="T413" id="Seg_3942" s="T412">haŋa</ta>
            <ta e="T414" id="Seg_3943" s="T413">ɨ͡aldʼɨt</ta>
            <ta e="T415" id="Seg_3944" s="T414">kel-BIT</ta>
            <ta e="T416" id="Seg_3945" s="T415">dʼe</ta>
            <ta e="T417" id="Seg_3946" s="T416">bagar</ta>
            <ta e="T418" id="Seg_3947" s="T417">en</ta>
            <ta e="T419" id="Seg_3948" s="T418">min</ta>
            <ta e="T420" id="Seg_3949" s="T419">taːburun-BI-n</ta>
            <ta e="T421" id="Seg_3950" s="T420">taːj-IAK-ŋ</ta>
            <ta e="T422" id="Seg_3951" s="T421">di͡e-BIT</ta>
            <ta e="T423" id="Seg_3952" s="T422">kineːs</ta>
            <ta e="T424" id="Seg_3953" s="T423">tu͡ok</ta>
            <ta e="T425" id="Seg_3954" s="T424">taːburun-LAːK-GIn=Ij</ta>
            <ta e="T426" id="Seg_3955" s="T425">töhö</ta>
            <ta e="T427" id="Seg_3956" s="T426">öj-I-m</ta>
            <ta e="T428" id="Seg_3957" s="T427">kotor-I-n-An</ta>
            <ta e="T429" id="Seg_3958" s="T428">taːj-A</ta>
            <ta e="T430" id="Seg_3959" s="T429">hataː-IAK</ta>
            <ta e="T431" id="Seg_3960" s="T430">e-TI-m</ta>
            <ta e="T432" id="Seg_3961" s="T431">Laːjku</ta>
            <ta e="T433" id="Seg_3962" s="T432">di͡e-BIT</ta>
            <ta e="T434" id="Seg_3963" s="T433">min</ta>
            <ta e="T435" id="Seg_3964" s="T434">aːt-LAː-A-r-A-BIn</ta>
            <ta e="T436" id="Seg_3965" s="T435">dahɨna</ta>
            <ta e="T437" id="Seg_3966" s="T436">tu͡ok-I-nAn</ta>
            <ta e="T438" id="Seg_3967" s="T437">oŋohulun-I-BIT=Ij</ta>
            <ta e="T439" id="Seg_3968" s="T438">dʼi͡e-An</ta>
            <ta e="T440" id="Seg_3969" s="T439">kineːs</ta>
            <ta e="T441" id="Seg_3970" s="T440">ɨjɨt-BIT</ta>
            <ta e="T442" id="Seg_3971" s="T441">öskötün</ta>
            <ta e="T443" id="Seg_3972" s="T442">min</ta>
            <ta e="T444" id="Seg_3973" s="T443">öj-I-m</ta>
            <ta e="T445" id="Seg_3974" s="T444">bar-Ar</ta>
            <ta e="T446" id="Seg_3975" s="T445">hir-tI-nAn</ta>
            <ta e="T447" id="Seg_3976" s="T446">oŋor-IAK-ŋ</ta>
            <ta e="T448" id="Seg_3977" s="T447">du͡o</ta>
            <ta e="T449" id="Seg_3978" s="T448">tiriː</ta>
            <ta e="T450" id="Seg_3979" s="T449">bu͡ol-TAK-InA</ta>
            <ta e="T451" id="Seg_3980" s="T450">bu͡ol-TIn</ta>
            <ta e="T452" id="Seg_3981" s="T451">min</ta>
            <ta e="T453" id="Seg_3982" s="T452">hanaː-BA-r</ta>
            <ta e="T454" id="Seg_3983" s="T453">hette</ta>
            <ta e="T455" id="Seg_3984" s="T454">bɨt</ta>
            <ta e="T456" id="Seg_3985" s="T455">tiriː-tI-nAn</ta>
            <ta e="T457" id="Seg_3986" s="T456">oŋohulun-I-BIT</ta>
            <ta e="T458" id="Seg_3987" s="T457">bɨhɨːlaːk</ta>
            <ta e="T459" id="Seg_3988" s="T458">Laːjku</ta>
            <ta e="T460" id="Seg_3989" s="T459">haŋar-BIT</ta>
            <ta e="T461" id="Seg_3990" s="T460">o</ta>
            <ta e="T462" id="Seg_3991" s="T461">tu͡ok</ta>
            <ta e="T463" id="Seg_3992" s="T462">öj-LAːK</ta>
            <ta e="T464" id="Seg_3993" s="T463">kihi-tA-GIn=Ij</ta>
            <ta e="T465" id="Seg_3994" s="T464">taːj-TI-ŋ</ta>
            <ta e="T466" id="Seg_3995" s="T465">diː</ta>
            <ta e="T467" id="Seg_3996" s="T466">dʼe</ta>
            <ta e="T468" id="Seg_3997" s="T467">oččogo</ta>
            <ta e="T469" id="Seg_3998" s="T468">min</ta>
            <ta e="T470" id="Seg_3999" s="T469">kɨːs-BI-n</ta>
            <ta e="T471" id="Seg_4000" s="T470">dʼaktar</ta>
            <ta e="T472" id="Seg_4001" s="T471">gɨn-Ar</ta>
            <ta e="T473" id="Seg_4002" s="T472">ɨjaːk-LAːK-GIn</ta>
            <ta e="T474" id="Seg_4003" s="T473">e-BIT</ta>
            <ta e="T475" id="Seg_4004" s="T474">kajdak</ta>
            <ta e="T476" id="Seg_4005" s="T475">gɨn-IAK-ŋ=Ij</ta>
            <ta e="T477" id="Seg_4006" s="T476">harsɨn</ta>
            <ta e="T478" id="Seg_4007" s="T477">kurumnaː-IAK-BIt</ta>
            <ta e="T479" id="Seg_4008" s="T478">Laːjku</ta>
            <ta e="T480" id="Seg_4009" s="T479">baːj</ta>
            <ta e="T481" id="Seg_4010" s="T480">kineːs</ta>
            <ta e="T482" id="Seg_4011" s="T481">bosku͡oj</ta>
            <ta e="T483" id="Seg_4012" s="T482">kɨːs-tI-n</ta>
            <ta e="T484" id="Seg_4013" s="T483">dʼaktar-LAN-An</ta>
            <ta e="T485" id="Seg_4014" s="T484">baːj-An-tot-An</ta>
            <ta e="T486" id="Seg_4015" s="T485">olor-BIT</ta>
            <ta e="T487" id="Seg_4016" s="T486">tu͡ok</ta>
            <ta e="T488" id="Seg_4017" s="T487">da</ta>
            <ta e="T489" id="Seg_4018" s="T488">erej-nI</ta>
            <ta e="T490" id="Seg_4019" s="T489">bil-BAkkA</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_4020" s="T0">lonely</ta>
            <ta e="T2" id="Seg_4021" s="T1">arise-PTCP.PST</ta>
            <ta e="T3" id="Seg_4022" s="T2">mother-3SG-ACC-father-3SG-ACC</ta>
            <ta e="T4" id="Seg_4023" s="T3">remember-NEG.PTCP</ta>
            <ta e="T5" id="Seg_4024" s="T4">Laajku.[NOM]</ta>
            <ta e="T6" id="Seg_4025" s="T5">say-CVB.SEQ</ta>
            <ta e="T7" id="Seg_4026" s="T6">dog.[NOM]</ta>
            <ta e="T8" id="Seg_4027" s="T7">live-PST2.[3SG]</ta>
            <ta e="T9" id="Seg_4028" s="T8">once</ta>
            <ta e="T10" id="Seg_4029" s="T9">walk-CVB.SIM</ta>
            <ta e="T11" id="Seg_4030" s="T10">go-EP-PST2.[3SG]</ta>
            <ta e="T12" id="Seg_4031" s="T11">human.being.[NOM]</ta>
            <ta e="T13" id="Seg_4032" s="T12">search-CVB.SIM</ta>
            <ta e="T14" id="Seg_4033" s="T13">that</ta>
            <ta e="T15" id="Seg_4034" s="T14">go-CVB.SEQ</ta>
            <ta e="T16" id="Seg_4035" s="T15">see-PST2.[3SG]</ta>
            <ta e="T17" id="Seg_4036" s="T16">high</ta>
            <ta e="T18" id="Seg_4037" s="T17">hill-ACC</ta>
            <ta e="T19" id="Seg_4038" s="T18">what.[NOM]</ta>
            <ta e="T20" id="Seg_4039" s="T19">be-FUT.[3SG]=Q</ta>
            <ta e="T21" id="Seg_4040" s="T20">1SG.[NOM]</ta>
            <ta e="T22" id="Seg_4041" s="T21">thither</ta>
            <ta e="T23" id="Seg_4042" s="T22">climb-TEMP-1SG</ta>
            <ta e="T24" id="Seg_4043" s="T23">think-CVB.SIM</ta>
            <ta e="T25" id="Seg_4044" s="T24">think-PST2.[3SG]</ta>
            <ta e="T26" id="Seg_4045" s="T25">hill.[NOM]</ta>
            <ta e="T27" id="Seg_4046" s="T26">upper.part-3SG-DAT/LOC</ta>
            <ta e="T28" id="Seg_4047" s="T27">climb-CVB.SEQ</ta>
            <ta e="T29" id="Seg_4048" s="T28">see-PST2-3SG</ta>
            <ta e="T30" id="Seg_4049" s="T29">Baba.Yaga.[NOM]</ta>
            <ta e="T31" id="Seg_4050" s="T30">go-PRS.[3SG]</ta>
            <ta e="T32" id="Seg_4051" s="T31">one.out.of.two</ta>
            <ta e="T33" id="Seg_4052" s="T32">side.[NOM]</ta>
            <ta e="T34" id="Seg_4053" s="T33">side-3SG-ABL</ta>
            <ta e="T35" id="Seg_4054" s="T34">hill.[NOM]</ta>
            <ta e="T36" id="Seg_4055" s="T35">middle-3SG-DAT/LOC</ta>
            <ta e="T37" id="Seg_4056" s="T36">reach-PST2.[3SG]</ta>
            <ta e="T38" id="Seg_4057" s="T37">and</ta>
            <ta e="T39" id="Seg_4058" s="T38">sit.on.the.hind.legs-NMNZ.[NOM]</ta>
            <ta e="T40" id="Seg_4059" s="T39">make-PST2.[3SG]</ta>
            <ta e="T41" id="Seg_4060" s="T40">Laajku.[NOM]</ta>
            <ta e="T42" id="Seg_4061" s="T41">werebeast.[NOM]</ta>
            <ta e="T43" id="Seg_4062" s="T42">very</ta>
            <ta e="T44" id="Seg_4063" s="T43">be-PST1-3SG</ta>
            <ta e="T45" id="Seg_4064" s="T44">be-PST2.[3SG]</ta>
            <ta e="T46" id="Seg_4065" s="T45">fox.[NOM]</ta>
            <ta e="T47" id="Seg_4066" s="T46">child.[NOM]</ta>
            <ta e="T48" id="Seg_4067" s="T47">become-CVB.SEQ</ta>
            <ta e="T49" id="Seg_4068" s="T48">after</ta>
            <ta e="T50" id="Seg_4069" s="T49">hill.[NOM]</ta>
            <ta e="T51" id="Seg_4070" s="T50">lower.part-3SG-ACC</ta>
            <ta e="T52" id="Seg_4071" s="T51">to</ta>
            <ta e="T53" id="Seg_4072" s="T52">roll-CVB.SEQ</ta>
            <ta e="T54" id="Seg_4073" s="T53">stay-PST2.[3SG]</ta>
            <ta e="T55" id="Seg_4074" s="T54">oh</ta>
            <ta e="T56" id="Seg_4075" s="T55">sin.[NOM]</ta>
            <ta e="T57" id="Seg_4076" s="T56">and</ta>
            <ta e="T58" id="Seg_4077" s="T57">how</ta>
            <ta e="T59" id="Seg_4078" s="T58">child-VBZ-PTCP.PRS-1SG-ACC</ta>
            <ta e="T60" id="Seg_4079" s="T59">notice-NEG.CVB.SIM</ta>
            <ta e="T61" id="Seg_4080" s="T60">stay-PST2-1SG=Q</ta>
            <ta e="T62" id="Seg_4081" s="T61">say-PST2.[3SG]</ta>
            <ta e="T63" id="Seg_4082" s="T62">and</ta>
            <ta e="T64" id="Seg_4083" s="T63">child-ACC</ta>
            <ta e="T65" id="Seg_4084" s="T64">wrap.up-CVB.SIM</ta>
            <ta e="T66" id="Seg_4085" s="T65">grab-CVB.SEQ</ta>
            <ta e="T67" id="Seg_4086" s="T66">after</ta>
            <ta e="T68" id="Seg_4087" s="T67">house-3SG-DAT/LOC</ta>
            <ta e="T69" id="Seg_4088" s="T68">carry-CVB.SIM</ta>
            <ta e="T70" id="Seg_4089" s="T69">go-PST2.[3SG]</ta>
            <ta e="T71" id="Seg_4090" s="T70">Baba.Yaga.[NOM]</ta>
            <ta e="T72" id="Seg_4091" s="T71">old.man-3SG-ACC</ta>
            <ta e="T73" id="Seg_4092" s="T72">with</ta>
            <ta e="T74" id="Seg_4093" s="T73">river.[NOM]</ta>
            <ta e="T75" id="Seg_4094" s="T74">shore-3SG-DAT/LOC</ta>
            <ta e="T76" id="Seg_4095" s="T75">live-PTCP.PRS</ta>
            <ta e="T77" id="Seg_4096" s="T76">be-PST2-3PL</ta>
            <ta e="T78" id="Seg_4097" s="T77">child-POSS</ta>
            <ta e="T79" id="Seg_4098" s="T78">NEG-3PL</ta>
            <ta e="T80" id="Seg_4099" s="T79">be-PST2.[3SG]</ta>
            <ta e="T81" id="Seg_4100" s="T80">lonely</ta>
            <ta e="T82" id="Seg_4101" s="T81">tamed</ta>
            <ta e="T83" id="Seg_4102" s="T82">reindeer-PROPR-3PL</ta>
            <ta e="T84" id="Seg_4103" s="T83">child-3PL.[NOM]</ta>
            <ta e="T85" id="Seg_4104" s="T84">day.by.day</ta>
            <ta e="T86" id="Seg_4105" s="T85">grow-CVB.SEQ</ta>
            <ta e="T87" id="Seg_4106" s="T86">go-PST2.[3SG]</ta>
            <ta e="T88" id="Seg_4107" s="T87">language-VBZ-PST2.[3SG]</ta>
            <ta e="T89" id="Seg_4108" s="T88">mother-3SG.[NOM]</ta>
            <ta e="T90" id="Seg_4109" s="T89">self-3SG-ABL</ta>
            <ta e="T91" id="Seg_4110" s="T90">long.coat-COM</ta>
            <ta e="T92" id="Seg_4111" s="T91">clothes.[NOM]</ta>
            <ta e="T93" id="Seg_4112" s="T92">sew-PST2.[3SG]</ta>
            <ta e="T94" id="Seg_4113" s="T93">this</ta>
            <ta e="T95" id="Seg_4114" s="T94">child.[NOM]</ta>
            <ta e="T96" id="Seg_4115" s="T95">run-CVB.SIM</ta>
            <ta e="T97" id="Seg_4116" s="T96">go-PTCP.PRS</ta>
            <ta e="T98" id="Seg_4117" s="T97">become-PST2.[3SG]</ta>
            <ta e="T99" id="Seg_4118" s="T98">house-3SG-GEN</ta>
            <ta e="T100" id="Seg_4119" s="T99">place.beneath-DAT/LOC</ta>
            <ta e="T101" id="Seg_4120" s="T100">old.man.[NOM]</ta>
            <ta e="T102" id="Seg_4121" s="T101">day.[NOM]</ta>
            <ta e="T103" id="Seg_4122" s="T102">every</ta>
            <ta e="T104" id="Seg_4123" s="T103">net-VBZ-PTCP.PRS</ta>
            <ta e="T105" id="Seg_4124" s="T104">custom-PROPR.[NOM]</ta>
            <ta e="T106" id="Seg_4125" s="T105">be-PST2.[3SG]</ta>
            <ta e="T107" id="Seg_4126" s="T106">this</ta>
            <ta e="T108" id="Seg_4127" s="T107">child-ACC</ta>
            <ta e="T109" id="Seg_4128" s="T108">fish-EP-INSTR</ta>
            <ta e="T110" id="Seg_4129" s="T109">only</ta>
            <ta e="T111" id="Seg_4130" s="T110">eat-EP-CAUS-PTCP.PRS</ta>
            <ta e="T112" id="Seg_4131" s="T111">be-PST2-3PL</ta>
            <ta e="T113" id="Seg_4132" s="T112">Laajku.[NOM]</ta>
            <ta e="T114" id="Seg_4133" s="T113">once</ta>
            <ta e="T115" id="Seg_4134" s="T114">say-PST2.[3SG]</ta>
            <ta e="T116" id="Seg_4135" s="T115">mum.[NOM]</ta>
            <ta e="T117" id="Seg_4136" s="T116">1SG.[NOM]</ta>
            <ta e="T118" id="Seg_4137" s="T117">meat.[NOM]</ta>
            <ta e="T119" id="Seg_4138" s="T118">eat-PTCP.FUT-1SG-ACC</ta>
            <ta e="T120" id="Seg_4139" s="T119">want-PRS-1SG</ta>
            <ta e="T121" id="Seg_4140" s="T120">old.man.[NOM]</ta>
            <ta e="T122" id="Seg_4141" s="T121">well</ta>
            <ta e="T123" id="Seg_4142" s="T122">kill.[IMP.2SG]</ta>
            <ta e="T124" id="Seg_4143" s="T123">infertile.animal-2SG-ACC</ta>
            <ta e="T125" id="Seg_4144" s="T124">child.[NOM]</ta>
            <ta e="T126" id="Seg_4145" s="T125">meat.[NOM]</ta>
            <ta e="T127" id="Seg_4146" s="T126">eat-PTCP.FUT.[3SG]-ACC</ta>
            <ta e="T128" id="Seg_4147" s="T127">want-PST2.[3SG]</ta>
            <ta e="T129" id="Seg_4148" s="T128">old.man.[NOM]</ta>
            <ta e="T130" id="Seg_4149" s="T129">reindeer-3SG-ACC</ta>
            <ta e="T131" id="Seg_4150" s="T130">kill-CVB.SEQ</ta>
            <ta e="T132" id="Seg_4151" s="T131">throw-PST2.[3SG]</ta>
            <ta e="T133" id="Seg_4152" s="T132">Laajku.[NOM]</ta>
            <ta e="T134" id="Seg_4153" s="T133">werebeast.[NOM]</ta>
            <ta e="T135" id="Seg_4154" s="T134">wish-3SG.[NOM]</ta>
            <ta e="T136" id="Seg_4155" s="T135">grow-CVB.SEQ</ta>
            <ta e="T137" id="Seg_4156" s="T136">go-PST2.[3SG]</ta>
            <ta e="T138" id="Seg_4157" s="T137">mum.[NOM]</ta>
            <ta e="T139" id="Seg_4158" s="T138">why</ta>
            <ta e="T140" id="Seg_4159" s="T139">river.[NOM]</ta>
            <ta e="T141" id="Seg_4160" s="T140">to.the.other.shore</ta>
            <ta e="T142" id="Seg_4161" s="T141">carry-EP-REFL-NEG-1PL</ta>
            <ta e="T143" id="Seg_4162" s="T142">there</ta>
            <ta e="T144" id="Seg_4163" s="T143">dry.lowland.[NOM]</ta>
            <ta e="T145" id="Seg_4164" s="T144">very</ta>
            <ta e="T146" id="Seg_4165" s="T145">apparently</ta>
            <ta e="T147" id="Seg_4166" s="T146">say-PST2.[3SG]</ta>
            <ta e="T148" id="Seg_4167" s="T147">well</ta>
            <ta e="T149" id="Seg_4168" s="T148">old.man.[NOM]</ta>
            <ta e="T150" id="Seg_4169" s="T149">hear-PRS-2SG</ta>
            <ta e="T151" id="Seg_4170" s="T150">Q</ta>
            <ta e="T152" id="Seg_4171" s="T151">child.[NOM]</ta>
            <ta e="T153" id="Seg_4172" s="T152">word-3SG-ACC</ta>
            <ta e="T154" id="Seg_4173" s="T153">truth-ACC</ta>
            <ta e="T155" id="Seg_4174" s="T154">make-PRS.[3SG]</ta>
            <ta e="T156" id="Seg_4175" s="T155">what.kind.of-DAT/LOC</ta>
            <ta e="T157" id="Seg_4176" s="T156">until</ta>
            <ta e="T158" id="Seg_4177" s="T157">one</ta>
            <ta e="T159" id="Seg_4178" s="T158">place-DAT/LOC</ta>
            <ta e="T160" id="Seg_4179" s="T159">live-FUT-1PL=Q</ta>
            <ta e="T161" id="Seg_4180" s="T160">start-IMP.1DU</ta>
            <ta e="T162" id="Seg_4181" s="T161">old.man.[NOM]</ta>
            <ta e="T163" id="Seg_4182" s="T162">baulk-NEG.CVB.SIM</ta>
            <ta e="T164" id="Seg_4183" s="T163">human.being-PL-3SG-ACC</ta>
            <ta e="T165" id="Seg_4184" s="T164">to.the.other.shore</ta>
            <ta e="T166" id="Seg_4185" s="T165">take.out-CVB.SEQ</ta>
            <ta e="T167" id="Seg_4186" s="T166">throw-PST2.[3SG]</ta>
            <ta e="T168" id="Seg_4187" s="T167">small.boat-INSTR</ta>
            <ta e="T169" id="Seg_4188" s="T168">Laajku.[NOM]</ta>
            <ta e="T170" id="Seg_4189" s="T169">bark-CVB.SIM-bark-CVB.SIM</ta>
            <ta e="T171" id="Seg_4190" s="T170">run-CVB.SIM</ta>
            <ta e="T172" id="Seg_4191" s="T171">go-EP-PST2.[3SG]</ta>
            <ta e="T173" id="Seg_4192" s="T172">in.the.evening-ADVZ</ta>
            <ta e="T174" id="Seg_4193" s="T173">carry-EP-REFL-CVB.SEQ</ta>
            <ta e="T175" id="Seg_4194" s="T174">stop-PTCP.PRS-3PL-ACC</ta>
            <ta e="T176" id="Seg_4195" s="T175">with</ta>
            <ta e="T177" id="Seg_4196" s="T176">this</ta>
            <ta e="T178" id="Seg_4197" s="T177">child.[NOM]</ta>
            <ta e="T179" id="Seg_4198" s="T178">mum-3SG-DAT/LOC</ta>
            <ta e="T180" id="Seg_4199" s="T179">come-CVB.SEQ</ta>
            <ta e="T181" id="Seg_4200" s="T180">knee-3SG-DAT/LOC</ta>
            <ta e="T182" id="Seg_4201" s="T181">sit.down-PST2.[3SG]</ta>
            <ta e="T183" id="Seg_4202" s="T182">mum.[NOM]</ta>
            <ta e="T184" id="Seg_4203" s="T183">1SG.[NOM]</ta>
            <ta e="T185" id="Seg_4204" s="T184">small.boat-VBZ-PTCP.FUT-1SG-ACC</ta>
            <ta e="T186" id="Seg_4205" s="T185">want-PRS-1SG</ta>
            <ta e="T187" id="Seg_4206" s="T186">that.[NOM]</ta>
            <ta e="T188" id="Seg_4207" s="T187">what=Q</ta>
            <ta e="T189" id="Seg_4208" s="T188">again</ta>
            <ta e="T190" id="Seg_4209" s="T189">old.man.[NOM]</ta>
            <ta e="T191" id="Seg_4210" s="T190">be.angry-PST2.[3SG]</ta>
            <ta e="T192" id="Seg_4211" s="T191">sit.down-CVB.SIM</ta>
            <ta e="T193" id="Seg_4212" s="T192">fall-CVB.SEQ</ta>
            <ta e="T194" id="Seg_4213" s="T193">after</ta>
            <ta e="T195" id="Seg_4214" s="T194">well</ta>
            <ta e="T196" id="Seg_4215" s="T195">better</ta>
            <ta e="T197" id="Seg_4216" s="T196">small.boat-VBZ.[IMP.2SG]</ta>
            <ta e="T198" id="Seg_4217" s="T197">distant</ta>
            <ta e="T199" id="Seg_4218" s="T198">just</ta>
            <ta e="T200" id="Seg_4219" s="T199">go-EP-NEG.[IMP.2SG]</ta>
            <ta e="T201" id="Seg_4220" s="T200">Q</ta>
            <ta e="T202" id="Seg_4221" s="T201">no</ta>
            <ta e="T203" id="Seg_4222" s="T202">go-FUT-1SG</ta>
            <ta e="T204" id="Seg_4223" s="T203">NEG-3SG</ta>
            <ta e="T205" id="Seg_4224" s="T204">river.[NOM]</ta>
            <ta e="T206" id="Seg_4225" s="T205">shore-3SG-DAT/LOC</ta>
            <ta e="T207" id="Seg_4226" s="T206">just</ta>
            <ta e="T208" id="Seg_4227" s="T207">go-FUT-1SG</ta>
            <ta e="T209" id="Seg_4228" s="T208">say-PST2.[3SG]</ta>
            <ta e="T210" id="Seg_4229" s="T209">Laajku.[NOM]</ta>
            <ta e="T211" id="Seg_4230" s="T210">beast.[NOM]</ta>
            <ta e="T212" id="Seg_4231" s="T211">Laajku.[NOM]</ta>
            <ta e="T213" id="Seg_4232" s="T212">small.boat-DAT/LOC</ta>
            <ta e="T214" id="Seg_4233" s="T213">sit.down-PST2.[3SG]</ta>
            <ta e="T215" id="Seg_4234" s="T214">silently</ta>
            <ta e="T216" id="Seg_4235" s="T215">shore.[NOM]</ta>
            <ta e="T217" id="Seg_4236" s="T216">along</ta>
            <ta e="T218" id="Seg_4237" s="T217">row-EP-MED-CVB.SIM</ta>
            <ta e="T219" id="Seg_4238" s="T218">go-EP-PST2.[3SG]</ta>
            <ta e="T220" id="Seg_4239" s="T219">old.man-PROPR</ta>
            <ta e="T221" id="Seg_4240" s="T220">old.woman.[NOM]</ta>
            <ta e="T222" id="Seg_4241" s="T221">house-DAT/LOC</ta>
            <ta e="T223" id="Seg_4242" s="T222">go.in-PTCP.PRS-3PL-ACC</ta>
            <ta e="T224" id="Seg_4243" s="T223">with</ta>
            <ta e="T225" id="Seg_4244" s="T224">move.away-CVB.SEQ</ta>
            <ta e="T226" id="Seg_4245" s="T225">and</ta>
            <ta e="T227" id="Seg_4246" s="T226">move.away-CVB.SEQ</ta>
            <ta e="T228" id="Seg_4247" s="T227">go-PST2.[3SG]</ta>
            <ta e="T229" id="Seg_4248" s="T228">this</ta>
            <ta e="T230" id="Seg_4249" s="T229">child.[NOM]</ta>
            <ta e="T231" id="Seg_4250" s="T230">river.[NOM]</ta>
            <ta e="T232" id="Seg_4251" s="T231">middle-3SG-DAT/LOC</ta>
            <ta e="T233" id="Seg_4252" s="T232">reach-PST2.[3SG]</ta>
            <ta e="T234" id="Seg_4253" s="T233">EMPH</ta>
            <ta e="T235" id="Seg_4254" s="T234">big</ta>
            <ta e="T236" id="Seg_4255" s="T235">human.being.[NOM]</ta>
            <ta e="T237" id="Seg_4256" s="T236">become-PST2.[3SG]</ta>
            <ta e="T238" id="Seg_4257" s="T237">old.man.[NOM]</ta>
            <ta e="T239" id="Seg_4258" s="T238">go.out-EP-PST2-3SG</ta>
            <ta e="T240" id="Seg_4259" s="T239">see-PST2-3SG</ta>
            <ta e="T241" id="Seg_4260" s="T240">son-3SG.[NOM]</ta>
            <ta e="T242" id="Seg_4261" s="T241">move.away-PST2.[3SG]</ta>
            <ta e="T243" id="Seg_4262" s="T242">well</ta>
            <ta e="T244" id="Seg_4263" s="T243">whereto</ta>
            <ta e="T245" id="Seg_4264" s="T244">go-PRS-2SG</ta>
            <ta e="T246" id="Seg_4265" s="T245">move.away-EP-NEG.[IMP.2SG]</ta>
            <ta e="T247" id="Seg_4266" s="T246">say-PRS-1SG</ta>
            <ta e="T248" id="Seg_4267" s="T247">water-DAT/LOC</ta>
            <ta e="T249" id="Seg_4268" s="T248">fall-PST1-2SG</ta>
            <ta e="T250" id="Seg_4269" s="T249">Laajku.[NOM]</ta>
            <ta e="T251" id="Seg_4270" s="T250">river.[NOM]</ta>
            <ta e="T252" id="Seg_4271" s="T251">middle-3SG-DAT/LOC</ta>
            <ta e="T253" id="Seg_4272" s="T252">reach-CVB.SEQ</ta>
            <ta e="T254" id="Seg_4273" s="T253">after</ta>
            <ta e="T255" id="Seg_4274" s="T254">shout-PST2.[3SG]</ta>
            <ta e="T256" id="Seg_4275" s="T255">ha-ha</ta>
            <ta e="T257" id="Seg_4276" s="T256">1SG.[NOM]</ta>
            <ta e="T258" id="Seg_4277" s="T257">Laajku-1SG</ta>
            <ta e="T259" id="Seg_4278" s="T258">EVID</ta>
            <ta e="T260" id="Seg_4279" s="T259">2PL.[NOM]</ta>
            <ta e="T261" id="Seg_4280" s="T260">that-ACC</ta>
            <ta e="T262" id="Seg_4281" s="T261">notice-PST2.NEG-2PL</ta>
            <ta e="T263" id="Seg_4282" s="T262">Laajku.[NOM]</ta>
            <ta e="T264" id="Seg_4283" s="T263">escape-PST2.[3SG]</ta>
            <ta e="T265" id="Seg_4284" s="T264">old.man-PROPR</ta>
            <ta e="T266" id="Seg_4285" s="T265">old.woman.[NOM]</ta>
            <ta e="T267" id="Seg_4286" s="T266">fight-CVB.SIM</ta>
            <ta e="T268" id="Seg_4287" s="T267">stay-PST2-3PL</ta>
            <ta e="T269" id="Seg_4288" s="T268">this</ta>
            <ta e="T270" id="Seg_4289" s="T269">boy.[NOM]</ta>
            <ta e="T271" id="Seg_4290" s="T270">walk-CVB.SEQ</ta>
            <ta e="T272" id="Seg_4291" s="T271">go-PST2.[3SG]</ta>
            <ta e="T273" id="Seg_4292" s="T272">how.much</ta>
            <ta e="T274" id="Seg_4293" s="T273">be-FUT.[3SG]=Q</ta>
            <ta e="T275" id="Seg_4294" s="T274">see-PST2-3SG</ta>
            <ta e="T276" id="Seg_4295" s="T275">house-PL.[NOM]</ta>
            <ta e="T277" id="Seg_4296" s="T276">stand-PRS-3PL</ta>
            <ta e="T278" id="Seg_4297" s="T277">sledge-PROPR</ta>
            <ta e="T279" id="Seg_4298" s="T278">reindeer-PL.[NOM]</ta>
            <ta e="T280" id="Seg_4299" s="T279">tie-EP-PASS/REFL-CVB.SIM</ta>
            <ta e="T281" id="Seg_4300" s="T280">lie-PRS-3PL</ta>
            <ta e="T282" id="Seg_4301" s="T281">one</ta>
            <ta e="T283" id="Seg_4302" s="T282">house-DAT/LOC</ta>
            <ta e="T284" id="Seg_4303" s="T283">go.in-PST2-3SG</ta>
            <ta e="T285" id="Seg_4304" s="T284">lonely</ta>
            <ta e="T286" id="Seg_4305" s="T285">woman.[NOM]</ta>
            <ta e="T287" id="Seg_4306" s="T286">sit-PRS.[3SG]</ta>
            <ta e="T288" id="Seg_4307" s="T287">Laajku.[NOM]</ta>
            <ta e="T289" id="Seg_4308" s="T288">tea-VBZ-PST2.[3SG]</ta>
            <ta e="T290" id="Seg_4309" s="T289">this</ta>
            <ta e="T291" id="Seg_4310" s="T290">what.[NOM]=Q</ta>
            <ta e="T292" id="Seg_4311" s="T291">gathering.[NOM]</ta>
            <ta e="T293" id="Seg_4312" s="T292">Q</ta>
            <ta e="T294" id="Seg_4313" s="T293">wedding.[NOM]</ta>
            <ta e="T295" id="Seg_4314" s="T294">Q</ta>
            <ta e="T296" id="Seg_4315" s="T295">no</ta>
            <ta e="T297" id="Seg_4316" s="T296">1PL.[NOM]</ta>
            <ta e="T298" id="Seg_4317" s="T297">prince-1PL.[NOM]</ta>
            <ta e="T299" id="Seg_4318" s="T298">one</ta>
            <ta e="T300" id="Seg_4319" s="T299">daughter-PROPR.[NOM]</ta>
            <ta e="T301" id="Seg_4320" s="T300">that</ta>
            <ta e="T302" id="Seg_4321" s="T301">daughter-DAT/LOC</ta>
            <ta e="T303" id="Seg_4322" s="T302">prince.[NOM]</ta>
            <ta e="T304" id="Seg_4323" s="T303">riddle-3SG-ACC</ta>
            <ta e="T305" id="Seg_4324" s="T304">guess-PTCP.PST</ta>
            <ta e="T306" id="Seg_4325" s="T305">human.being.[NOM]</ta>
            <ta e="T307" id="Seg_4326" s="T306">just</ta>
            <ta e="T308" id="Seg_4327" s="T307">man.[NOM]</ta>
            <ta e="T309" id="Seg_4328" s="T308">become-PRS.[3SG]</ta>
            <ta e="T310" id="Seg_4329" s="T309">say-PST2.[3SG]</ta>
            <ta e="T311" id="Seg_4330" s="T310">woman.[NOM]</ta>
            <ta e="T312" id="Seg_4331" s="T311">Laajku.[NOM]</ta>
            <ta e="T313" id="Seg_4332" s="T312">go.out-EP-PST2.[3SG]</ta>
            <ta e="T314" id="Seg_4333" s="T313">and</ta>
            <ta e="T315" id="Seg_4334" s="T314">river.[NOM]</ta>
            <ta e="T316" id="Seg_4335" s="T315">edge-3SG-DAT/LOC</ta>
            <ta e="T317" id="Seg_4336" s="T316">go.in-PST2.[3SG]</ta>
            <ta e="T318" id="Seg_4337" s="T317">grass-EP-INSTR</ta>
            <ta e="T319" id="Seg_4338" s="T318">cover-REFL-CVB.SEQ</ta>
            <ta e="T320" id="Seg_4339" s="T319">after</ta>
            <ta e="T321" id="Seg_4340" s="T320">lie.down-PST2.[3SG]</ta>
            <ta e="T322" id="Seg_4341" s="T321">see-PST2-3SG</ta>
            <ta e="T323" id="Seg_4342" s="T322">two</ta>
            <ta e="T324" id="Seg_4343" s="T323">girl.[NOM]</ta>
            <ta e="T325" id="Seg_4344" s="T324">water.[NOM]</ta>
            <ta e="T326" id="Seg_4345" s="T325">scoop-CVB.SIM</ta>
            <ta e="T327" id="Seg_4346" s="T326">go-PRS-3PL</ta>
            <ta e="T328" id="Seg_4347" s="T327">shoulder.yoke-VBZ-CVB.SEQ</ta>
            <ta e="T329" id="Seg_4348" s="T328">two</ta>
            <ta e="T330" id="Seg_4349" s="T329">container-ACC</ta>
            <ta e="T331" id="Seg_4350" s="T330">shoulder-3PL-DAT/LOC</ta>
            <ta e="T332" id="Seg_4351" s="T331">take.on.the.back-PST2-3PL</ta>
            <ta e="T333" id="Seg_4352" s="T332">boy.[NOM]</ta>
            <ta e="T334" id="Seg_4353" s="T333">leg-3SG-ABL</ta>
            <ta e="T335" id="Seg_4354" s="T334">rich</ta>
            <ta e="T336" id="Seg_4355" s="T335">girl.[NOM]</ta>
            <ta e="T337" id="Seg_4356" s="T336">stumble-CVB.SEQ</ta>
            <ta e="T338" id="Seg_4357" s="T337">water-3SG-ACC</ta>
            <ta e="T339" id="Seg_4358" s="T338">spill-PST2.[3SG]</ta>
            <ta e="T340" id="Seg_4359" s="T339">this.[NOM]</ta>
            <ta e="T341" id="Seg_4360" s="T340">completely</ta>
            <ta e="T342" id="Seg_4361" s="T341">father-1SG.[NOM]</ta>
            <ta e="T343" id="Seg_4362" s="T342">because.of</ta>
            <ta e="T344" id="Seg_4363" s="T343">husband-DAT/LOC</ta>
            <ta e="T345" id="Seg_4364" s="T344">go.out-EP-PTCP.PST</ta>
            <ta e="T346" id="Seg_4365" s="T345">be-PST2-EP-1SG</ta>
            <ta e="T347" id="Seg_4366" s="T346">be-COND.[3SG]</ta>
            <ta e="T348" id="Seg_4367" s="T347">water.[NOM]</ta>
            <ta e="T349" id="Seg_4368" s="T348">scoop-CVB.SIM</ta>
            <ta e="T350" id="Seg_4369" s="T349">get.tired-CVB.SIM</ta>
            <ta e="T351" id="Seg_4370" s="T350">go-PTCP.FUT</ta>
            <ta e="T352" id="Seg_4371" s="T351">be-PST1-1SG</ta>
            <ta e="T353" id="Seg_4372" s="T352">Q</ta>
            <ta e="T354" id="Seg_4373" s="T353">leg-1SG-ACC</ta>
            <ta e="T355" id="Seg_4374" s="T354">just</ta>
            <ta e="T356" id="Seg_4375" s="T355">make.wet-PST1-1SG</ta>
            <ta e="T357" id="Seg_4376" s="T356">friend</ta>
            <ta e="T358" id="Seg_4377" s="T357">that.[NOM]</ta>
            <ta e="T359" id="Seg_4378" s="T358">father-2SG.[NOM]</ta>
            <ta e="T360" id="Seg_4379" s="T359">what-ACC</ta>
            <ta e="T361" id="Seg_4380" s="T360">guess-CAUS-PRS.[3SG]</ta>
            <ta e="T362" id="Seg_4381" s="T361">father-1SG.[NOM]</ta>
            <ta e="T363" id="Seg_4382" s="T362">when</ta>
            <ta e="T364" id="Seg_4383" s="T363">INDEF</ta>
            <ta e="T365" id="Seg_4384" s="T364">seven</ta>
            <ta e="T366" id="Seg_4385" s="T365">louse.[NOM]</ta>
            <ta e="T367" id="Seg_4386" s="T366">skin-3SG-INSTR</ta>
            <ta e="T368" id="Seg_4387" s="T367">cover.for.load.[NOM]</ta>
            <ta e="T369" id="Seg_4388" s="T368">make-PTCP.PST</ta>
            <ta e="T370" id="Seg_4389" s="T369">be-PST1-3SG</ta>
            <ta e="T371" id="Seg_4390" s="T370">that-ACC</ta>
            <ta e="T372" id="Seg_4391" s="T371">make-PTCP.PRS</ta>
            <ta e="T373" id="Seg_4392" s="T372">be-PST2-3SG</ta>
            <ta e="T374" id="Seg_4393" s="T373">Q</ta>
            <ta e="T375" id="Seg_4394" s="T374">well</ta>
            <ta e="T376" id="Seg_4395" s="T375">go-IMP.1DU</ta>
            <ta e="T377" id="Seg_4396" s="T376">water.[NOM]</ta>
            <ta e="T378" id="Seg_4397" s="T377">NEG</ta>
            <ta e="T379" id="Seg_4398" s="T378">bear-NEG.[3SG]</ta>
            <ta e="T380" id="Seg_4399" s="T379">wood.[NOM]</ta>
            <ta e="T381" id="Seg_4400" s="T380">NEG</ta>
            <ta e="T382" id="Seg_4401" s="T381">bear-NEG.[3SG]</ta>
            <ta e="T383" id="Seg_4402" s="T382">so.much-PROPR</ta>
            <ta e="T384" id="Seg_4403" s="T383">guest-DAT/LOC</ta>
            <ta e="T385" id="Seg_4404" s="T384">Laajku.[NOM]</ta>
            <ta e="T386" id="Seg_4405" s="T385">come.back-CVB.SEQ</ta>
            <ta e="T387" id="Seg_4406" s="T386">come-CVB.SEQ</ta>
            <ta e="T388" id="Seg_4407" s="T387">this</ta>
            <ta e="T389" id="Seg_4408" s="T388">house-3SG-DAT/LOC</ta>
            <ta e="T390" id="Seg_4409" s="T389">go.in-PST2.[3SG]</ta>
            <ta e="T391" id="Seg_4410" s="T390">overnight-PST2.[3SG]</ta>
            <ta e="T392" id="Seg_4411" s="T391">morning.[NOM]</ta>
            <ta e="T393" id="Seg_4412" s="T392">stand.up-PST2-3SG</ta>
            <ta e="T394" id="Seg_4413" s="T393">again</ta>
            <ta e="T395" id="Seg_4414" s="T394">guest.[NOM]</ta>
            <ta e="T396" id="Seg_4415" s="T395">full.[NOM]</ta>
            <ta e="T397" id="Seg_4416" s="T396">this</ta>
            <ta e="T398" id="Seg_4417" s="T397">boy.[NOM]</ta>
            <ta e="T399" id="Seg_4418" s="T398">also</ta>
            <ta e="T400" id="Seg_4419" s="T399">prince-DAT/LOC</ta>
            <ta e="T401" id="Seg_4420" s="T400">go-CAP.[3SG]</ta>
            <ta e="T402" id="Seg_4421" s="T401">guest-PL-ACC</ta>
            <ta e="T403" id="Seg_4422" s="T402">with</ta>
            <ta e="T404" id="Seg_4423" s="T403">together</ta>
            <ta e="T405" id="Seg_4424" s="T404">go.in-PST2.[3SG]</ta>
            <ta e="T406" id="Seg_4425" s="T405">see-PST2-3SG</ta>
            <ta e="T407" id="Seg_4426" s="T406">house-ACC</ta>
            <ta e="T408" id="Seg_4427" s="T407">full</ta>
            <ta e="T409" id="Seg_4428" s="T408">man.[NOM]</ta>
            <ta e="T410" id="Seg_4429" s="T409">human.being.[NOM]</ta>
            <ta e="T411" id="Seg_4430" s="T410">oh</ta>
            <ta e="T412" id="Seg_4431" s="T411">today</ta>
            <ta e="T413" id="Seg_4432" s="T412">new</ta>
            <ta e="T414" id="Seg_4433" s="T413">guest.[NOM]</ta>
            <ta e="T415" id="Seg_4434" s="T414">come-PST2.[3SG]</ta>
            <ta e="T416" id="Seg_4435" s="T415">well</ta>
            <ta e="T417" id="Seg_4436" s="T416">maybe</ta>
            <ta e="T418" id="Seg_4437" s="T417">2SG.[NOM]</ta>
            <ta e="T419" id="Seg_4438" s="T418">1SG.[NOM]</ta>
            <ta e="T420" id="Seg_4439" s="T419">riddle-1SG-ACC</ta>
            <ta e="T421" id="Seg_4440" s="T420">guess-FUT-2SG</ta>
            <ta e="T422" id="Seg_4441" s="T421">say-PST2.[3SG]</ta>
            <ta e="T423" id="Seg_4442" s="T422">prince.[NOM]</ta>
            <ta e="T424" id="Seg_4443" s="T423">what</ta>
            <ta e="T425" id="Seg_4444" s="T424">riddle-PROPR-2SG=Q</ta>
            <ta e="T426" id="Seg_4445" s="T425">how.much</ta>
            <ta e="T427" id="Seg_4446" s="T426">mind-EP-1SG.[NOM]</ta>
            <ta e="T428" id="Seg_4447" s="T427">take.out-EP-MED-CVB.SEQ</ta>
            <ta e="T429" id="Seg_4448" s="T428">guess-CVB.SIM</ta>
            <ta e="T430" id="Seg_4449" s="T429">can-PTCP.FUT</ta>
            <ta e="T431" id="Seg_4450" s="T430">be-PST1-1SG</ta>
            <ta e="T432" id="Seg_4451" s="T431">Laajku.[NOM]</ta>
            <ta e="T433" id="Seg_4452" s="T432">say-PST2.[3SG]</ta>
            <ta e="T434" id="Seg_4453" s="T433">1SG.[NOM]</ta>
            <ta e="T435" id="Seg_4454" s="T434">name-VBZ-EP-CAUS-PRS-1SG</ta>
            <ta e="T436" id="Seg_4455" s="T435">cover.for.load.[NOM]</ta>
            <ta e="T437" id="Seg_4456" s="T436">what-EP-INSTR</ta>
            <ta e="T438" id="Seg_4457" s="T437">be.made-EP-PTCP.PST.[NOM]=Q</ta>
            <ta e="T439" id="Seg_4458" s="T438">say-CVB.SEQ</ta>
            <ta e="T440" id="Seg_4459" s="T439">prince.[NOM]</ta>
            <ta e="T441" id="Seg_4460" s="T440">ask-PST2.[3SG]</ta>
            <ta e="T442" id="Seg_4461" s="T441">if</ta>
            <ta e="T443" id="Seg_4462" s="T442">1SG.[NOM]</ta>
            <ta e="T444" id="Seg_4463" s="T443">mind-EP-1SG.[NOM]</ta>
            <ta e="T445" id="Seg_4464" s="T444">go-PTCP.PRS</ta>
            <ta e="T446" id="Seg_4465" s="T445">matter-3SG-INSTR</ta>
            <ta e="T447" id="Seg_4466" s="T446">make-FUT-2SG</ta>
            <ta e="T448" id="Seg_4467" s="T447">Q</ta>
            <ta e="T449" id="Seg_4468" s="T448">skin.[NOM]</ta>
            <ta e="T450" id="Seg_4469" s="T449">be-TEMP-3SG</ta>
            <ta e="T451" id="Seg_4470" s="T450">be-IMP.3SG</ta>
            <ta e="T452" id="Seg_4471" s="T451">1SG.[NOM]</ta>
            <ta e="T453" id="Seg_4472" s="T452">thought-1SG-DAT/LOC</ta>
            <ta e="T454" id="Seg_4473" s="T453">seven</ta>
            <ta e="T455" id="Seg_4474" s="T454">louse.[NOM]</ta>
            <ta e="T456" id="Seg_4475" s="T455">skin-3SG-INSTR</ta>
            <ta e="T457" id="Seg_4476" s="T456">be.made-EP-PTCP.PST.[NOM]</ta>
            <ta e="T458" id="Seg_4477" s="T457">apparently</ta>
            <ta e="T459" id="Seg_4478" s="T458">Laajku.[NOM]</ta>
            <ta e="T460" id="Seg_4479" s="T459">say-PST2.[3SG]</ta>
            <ta e="T461" id="Seg_4480" s="T460">oh</ta>
            <ta e="T462" id="Seg_4481" s="T461">what.[NOM]</ta>
            <ta e="T463" id="Seg_4482" s="T462">mind-PROPR</ta>
            <ta e="T464" id="Seg_4483" s="T463">human.being-3SG-2SG=Q</ta>
            <ta e="T465" id="Seg_4484" s="T464">guess-PST1-2SG</ta>
            <ta e="T466" id="Seg_4485" s="T465">EMPH</ta>
            <ta e="T467" id="Seg_4486" s="T466">well</ta>
            <ta e="T468" id="Seg_4487" s="T467">then</ta>
            <ta e="T469" id="Seg_4488" s="T468">1SG.[NOM]</ta>
            <ta e="T470" id="Seg_4489" s="T469">daughter-1SG-ACC</ta>
            <ta e="T471" id="Seg_4490" s="T470">woman.[NOM]</ta>
            <ta e="T472" id="Seg_4491" s="T471">make-PTCP.PRS</ta>
            <ta e="T473" id="Seg_4492" s="T472">destiny-PROPR-2SG</ta>
            <ta e="T474" id="Seg_4493" s="T473">be-PST2.[3SG]</ta>
            <ta e="T475" id="Seg_4494" s="T474">how</ta>
            <ta e="T476" id="Seg_4495" s="T475">make-FUT-2SG=Q</ta>
            <ta e="T477" id="Seg_4496" s="T476">tomorrow</ta>
            <ta e="T478" id="Seg_4497" s="T477">celebrate-FUT-1PL</ta>
            <ta e="T479" id="Seg_4498" s="T478">Laajku.[NOM]</ta>
            <ta e="T480" id="Seg_4499" s="T479">rich</ta>
            <ta e="T481" id="Seg_4500" s="T480">prince.[NOM]</ta>
            <ta e="T482" id="Seg_4501" s="T481">beautiful</ta>
            <ta e="T483" id="Seg_4502" s="T482">daughter-3SG-ACC</ta>
            <ta e="T484" id="Seg_4503" s="T483">woman-VBZ-CVB.SEQ</ta>
            <ta e="T485" id="Seg_4504" s="T484">be.rich-CVB.SEQ-eat.ones.fill-CVB.SEQ</ta>
            <ta e="T486" id="Seg_4505" s="T485">live-PST2.[3SG]</ta>
            <ta e="T487" id="Seg_4506" s="T486">what.[NOM]</ta>
            <ta e="T488" id="Seg_4507" s="T487">NEG</ta>
            <ta e="T489" id="Seg_4508" s="T488">pain-ACC</ta>
            <ta e="T490" id="Seg_4509" s="T489">know-NEG.CVB.SIM</ta>
         </annotation>
         <annotation name="gg" tierref="gg">
            <ta e="T1" id="Seg_4510" s="T0">einsam</ta>
            <ta e="T2" id="Seg_4511" s="T1">entstehen-PTCP.PST</ta>
            <ta e="T3" id="Seg_4512" s="T2">Mutter-3SG-ACC-Vater-3SG-ACC</ta>
            <ta e="T4" id="Seg_4513" s="T3">erinnern-NEG.PTCP</ta>
            <ta e="T5" id="Seg_4514" s="T4">Laajku.[NOM]</ta>
            <ta e="T6" id="Seg_4515" s="T5">sagen-CVB.SEQ</ta>
            <ta e="T7" id="Seg_4516" s="T6">Hund.[NOM]</ta>
            <ta e="T8" id="Seg_4517" s="T7">leben-PST2.[3SG]</ta>
            <ta e="T9" id="Seg_4518" s="T8">einmal</ta>
            <ta e="T10" id="Seg_4519" s="T9">go-CVB.SIM</ta>
            <ta e="T11" id="Seg_4520" s="T10">gehen-EP-PST2.[3SG]</ta>
            <ta e="T12" id="Seg_4521" s="T11">Mensch.[NOM]</ta>
            <ta e="T13" id="Seg_4522" s="T12">suchen-CVB.SIM</ta>
            <ta e="T14" id="Seg_4523" s="T13">jenes</ta>
            <ta e="T15" id="Seg_4524" s="T14">gehen-CVB.SEQ</ta>
            <ta e="T16" id="Seg_4525" s="T15">sehen-PST2.[3SG]</ta>
            <ta e="T17" id="Seg_4526" s="T16">hoch</ta>
            <ta e="T18" id="Seg_4527" s="T17">Hügel-ACC</ta>
            <ta e="T19" id="Seg_4528" s="T18">was.[NOM]</ta>
            <ta e="T20" id="Seg_4529" s="T19">sein-FUT.[3SG]=Q</ta>
            <ta e="T21" id="Seg_4530" s="T20">1SG.[NOM]</ta>
            <ta e="T22" id="Seg_4531" s="T21">dorthin</ta>
            <ta e="T23" id="Seg_4532" s="T22">klettern-TEMP-1SG</ta>
            <ta e="T24" id="Seg_4533" s="T23">denken-CVB.SIM</ta>
            <ta e="T25" id="Seg_4534" s="T24">denken-PST2.[3SG]</ta>
            <ta e="T26" id="Seg_4535" s="T25">Hügel.[NOM]</ta>
            <ta e="T27" id="Seg_4536" s="T26">oberer.Teil-3SG-DAT/LOC</ta>
            <ta e="T28" id="Seg_4537" s="T27">klettern-CVB.SEQ</ta>
            <ta e="T29" id="Seg_4538" s="T28">sehen-PST2-3SG</ta>
            <ta e="T30" id="Seg_4539" s="T29">Baba.Jaga.[NOM]</ta>
            <ta e="T31" id="Seg_4540" s="T30">gehen-PRS.[3SG]</ta>
            <ta e="T32" id="Seg_4541" s="T31">einer.von.zwei</ta>
            <ta e="T33" id="Seg_4542" s="T32">Seite.[NOM]</ta>
            <ta e="T34" id="Seg_4543" s="T33">Seite-3SG-ABL</ta>
            <ta e="T35" id="Seg_4544" s="T34">Hügel.[NOM]</ta>
            <ta e="T36" id="Seg_4545" s="T35">Mitte-3SG-DAT/LOC</ta>
            <ta e="T37" id="Seg_4546" s="T36">ankommen-PST2.[3SG]</ta>
            <ta e="T38" id="Seg_4547" s="T37">und</ta>
            <ta e="T39" id="Seg_4548" s="T38">auf.den.Hinterbeinen.sitzen-NMNZ.[NOM]</ta>
            <ta e="T40" id="Seg_4549" s="T39">machen-PST2.[3SG]</ta>
            <ta e="T41" id="Seg_4550" s="T40">Laajku.[NOM]</ta>
            <ta e="T42" id="Seg_4551" s="T41">Werwesen.[NOM]</ta>
            <ta e="T43" id="Seg_4552" s="T42">sehr</ta>
            <ta e="T44" id="Seg_4553" s="T43">sein-PST1-3SG</ta>
            <ta e="T45" id="Seg_4554" s="T44">sein-PST2.[3SG]</ta>
            <ta e="T46" id="Seg_4555" s="T45">Fuchs.[NOM]</ta>
            <ta e="T47" id="Seg_4556" s="T46">Kind.[NOM]</ta>
            <ta e="T48" id="Seg_4557" s="T47">werden-CVB.SEQ</ta>
            <ta e="T49" id="Seg_4558" s="T48">nachdem</ta>
            <ta e="T50" id="Seg_4559" s="T49">Hügel.[NOM]</ta>
            <ta e="T51" id="Seg_4560" s="T50">Unterteil-3SG-ACC</ta>
            <ta e="T52" id="Seg_4561" s="T51">zu</ta>
            <ta e="T53" id="Seg_4562" s="T52">rollen-CVB.SEQ</ta>
            <ta e="T54" id="Seg_4563" s="T53">bleiben-PST2.[3SG]</ta>
            <ta e="T55" id="Seg_4564" s="T54">oh</ta>
            <ta e="T56" id="Seg_4565" s="T55">Sünde.[NOM]</ta>
            <ta e="T57" id="Seg_4566" s="T56">und</ta>
            <ta e="T58" id="Seg_4567" s="T57">wie</ta>
            <ta e="T59" id="Seg_4568" s="T58">Kind-VBZ-PTCP.PRS-1SG-ACC</ta>
            <ta e="T60" id="Seg_4569" s="T59">bemerken-NEG.CVB.SIM</ta>
            <ta e="T61" id="Seg_4570" s="T60">bleiben-PST2-1SG=Q</ta>
            <ta e="T62" id="Seg_4571" s="T61">sagen-PST2.[3SG]</ta>
            <ta e="T63" id="Seg_4572" s="T62">und</ta>
            <ta e="T64" id="Seg_4573" s="T63">Kind-ACC</ta>
            <ta e="T65" id="Seg_4574" s="T64">einwickeln-CVB.SIM</ta>
            <ta e="T66" id="Seg_4575" s="T65">greifen-CVB.SEQ</ta>
            <ta e="T67" id="Seg_4576" s="T66">nachdem</ta>
            <ta e="T68" id="Seg_4577" s="T67">Haus-3SG-DAT/LOC</ta>
            <ta e="T69" id="Seg_4578" s="T68">tragen-CVB.SIM</ta>
            <ta e="T70" id="Seg_4579" s="T69">gehen-PST2.[3SG]</ta>
            <ta e="T71" id="Seg_4580" s="T70">Baba.Jaga.[NOM]</ta>
            <ta e="T72" id="Seg_4581" s="T71">alter.Mann-3SG-ACC</ta>
            <ta e="T73" id="Seg_4582" s="T72">mit</ta>
            <ta e="T74" id="Seg_4583" s="T73">Fluss.[NOM]</ta>
            <ta e="T75" id="Seg_4584" s="T74">Ufer-3SG-DAT/LOC</ta>
            <ta e="T76" id="Seg_4585" s="T75">leben-PTCP.PRS</ta>
            <ta e="T77" id="Seg_4586" s="T76">sein-PST2-3PL</ta>
            <ta e="T78" id="Seg_4587" s="T77">Kind-POSS</ta>
            <ta e="T79" id="Seg_4588" s="T78">NEG-3PL</ta>
            <ta e="T80" id="Seg_4589" s="T79">sein-PST2.[3SG]</ta>
            <ta e="T81" id="Seg_4590" s="T80">einsam</ta>
            <ta e="T82" id="Seg_4591" s="T81">gezähmt</ta>
            <ta e="T83" id="Seg_4592" s="T82">Rentier-PROPR-3PL</ta>
            <ta e="T84" id="Seg_4593" s="T83">Kind-3PL.[NOM]</ta>
            <ta e="T85" id="Seg_4594" s="T84">mit.jedem.Tag</ta>
            <ta e="T86" id="Seg_4595" s="T85">wachsen-CVB.SEQ</ta>
            <ta e="T87" id="Seg_4596" s="T86">gehen-PST2.[3SG]</ta>
            <ta e="T88" id="Seg_4597" s="T87">Sprache-VBZ-PST2.[3SG]</ta>
            <ta e="T89" id="Seg_4598" s="T88">Mutter-3SG.[NOM]</ta>
            <ta e="T90" id="Seg_4599" s="T89">selbst-3SG-ABL</ta>
            <ta e="T91" id="Seg_4600" s="T90">langer.Mantel-COM</ta>
            <ta e="T92" id="Seg_4601" s="T91">Kleidung.[NOM]</ta>
            <ta e="T93" id="Seg_4602" s="T92">nähen-PST2.[3SG]</ta>
            <ta e="T94" id="Seg_4603" s="T93">dieses</ta>
            <ta e="T95" id="Seg_4604" s="T94">Kind.[NOM]</ta>
            <ta e="T96" id="Seg_4605" s="T95">laufen-CVB.SIM</ta>
            <ta e="T97" id="Seg_4606" s="T96">gehen-PTCP.PRS</ta>
            <ta e="T98" id="Seg_4607" s="T97">werden-PST2.[3SG]</ta>
            <ta e="T99" id="Seg_4608" s="T98">Haus-3SG-GEN</ta>
            <ta e="T100" id="Seg_4609" s="T99">Platz.neben-DAT/LOC</ta>
            <ta e="T101" id="Seg_4610" s="T100">alter.Mann.[NOM]</ta>
            <ta e="T102" id="Seg_4611" s="T101">Tag.[NOM]</ta>
            <ta e="T103" id="Seg_4612" s="T102">jeder</ta>
            <ta e="T104" id="Seg_4613" s="T103">Netz-VBZ-PTCP.PRS</ta>
            <ta e="T105" id="Seg_4614" s="T104">Gewohnheit-PROPR.[NOM]</ta>
            <ta e="T106" id="Seg_4615" s="T105">sein-PST2.[3SG]</ta>
            <ta e="T107" id="Seg_4616" s="T106">dieses</ta>
            <ta e="T108" id="Seg_4617" s="T107">Kind-ACC</ta>
            <ta e="T109" id="Seg_4618" s="T108">Fisch-EP-INSTR</ta>
            <ta e="T110" id="Seg_4619" s="T109">nur</ta>
            <ta e="T111" id="Seg_4620" s="T110">essen-EP-CAUS-PTCP.PRS</ta>
            <ta e="T112" id="Seg_4621" s="T111">sein-PST2-3PL</ta>
            <ta e="T113" id="Seg_4622" s="T112">Laajku.[NOM]</ta>
            <ta e="T114" id="Seg_4623" s="T113">einmal</ta>
            <ta e="T115" id="Seg_4624" s="T114">sagen-PST2.[3SG]</ta>
            <ta e="T116" id="Seg_4625" s="T115">Mama.[NOM]</ta>
            <ta e="T117" id="Seg_4626" s="T116">1SG.[NOM]</ta>
            <ta e="T118" id="Seg_4627" s="T117">Fleisch.[NOM]</ta>
            <ta e="T119" id="Seg_4628" s="T118">essen-PTCP.FUT-1SG-ACC</ta>
            <ta e="T120" id="Seg_4629" s="T119">wollen-PRS-1SG</ta>
            <ta e="T121" id="Seg_4630" s="T120">alter.Mann.[NOM]</ta>
            <ta e="T122" id="Seg_4631" s="T121">doch</ta>
            <ta e="T123" id="Seg_4632" s="T122">töten.[IMP.2SG]</ta>
            <ta e="T124" id="Seg_4633" s="T123">unfruchtbares.Tier-2SG-ACC</ta>
            <ta e="T125" id="Seg_4634" s="T124">Kind.[NOM]</ta>
            <ta e="T126" id="Seg_4635" s="T125">Fleisch.[NOM]</ta>
            <ta e="T127" id="Seg_4636" s="T126">essen-PTCP.FUT.[3SG]-ACC</ta>
            <ta e="T128" id="Seg_4637" s="T127">wollen-PST2.[3SG]</ta>
            <ta e="T129" id="Seg_4638" s="T128">alter.Mann.[NOM]</ta>
            <ta e="T130" id="Seg_4639" s="T129">Rentier-3SG-ACC</ta>
            <ta e="T131" id="Seg_4640" s="T130">töten-CVB.SEQ</ta>
            <ta e="T132" id="Seg_4641" s="T131">werfen-PST2.[3SG]</ta>
            <ta e="T133" id="Seg_4642" s="T132">Laajku.[NOM]</ta>
            <ta e="T134" id="Seg_4643" s="T133">Werwesen.[NOM]</ta>
            <ta e="T135" id="Seg_4644" s="T134">Wunsch-3SG.[NOM]</ta>
            <ta e="T136" id="Seg_4645" s="T135">wachsen-CVB.SEQ</ta>
            <ta e="T137" id="Seg_4646" s="T136">gehen-PST2.[3SG]</ta>
            <ta e="T138" id="Seg_4647" s="T137">Mama.[NOM]</ta>
            <ta e="T139" id="Seg_4648" s="T138">warum</ta>
            <ta e="T140" id="Seg_4649" s="T139">Fluss.[NOM]</ta>
            <ta e="T141" id="Seg_4650" s="T140">ans.andere.Ufer</ta>
            <ta e="T142" id="Seg_4651" s="T141">tragen-EP-REFL-NEG-1PL</ta>
            <ta e="T143" id="Seg_4652" s="T142">dort</ta>
            <ta e="T144" id="Seg_4653" s="T143">trockene.Ebene.[NOM]</ta>
            <ta e="T145" id="Seg_4654" s="T144">sehr</ta>
            <ta e="T146" id="Seg_4655" s="T145">offenbar</ta>
            <ta e="T147" id="Seg_4656" s="T146">sagen-PST2.[3SG]</ta>
            <ta e="T148" id="Seg_4657" s="T147">na</ta>
            <ta e="T149" id="Seg_4658" s="T148">alter.Mann.[NOM]</ta>
            <ta e="T150" id="Seg_4659" s="T149">hören-PRS-2SG</ta>
            <ta e="T151" id="Seg_4660" s="T150">Q</ta>
            <ta e="T152" id="Seg_4661" s="T151">Kind.[NOM]</ta>
            <ta e="T153" id="Seg_4662" s="T152">Wort-3SG-ACC</ta>
            <ta e="T154" id="Seg_4663" s="T153">Wahrheit-ACC</ta>
            <ta e="T155" id="Seg_4664" s="T154">machen-PRS.[3SG]</ta>
            <ta e="T156" id="Seg_4665" s="T155">was.für.ein-DAT/LOC</ta>
            <ta e="T157" id="Seg_4666" s="T156">bis.zu</ta>
            <ta e="T158" id="Seg_4667" s="T157">eins</ta>
            <ta e="T159" id="Seg_4668" s="T158">Ort-DAT/LOC</ta>
            <ta e="T160" id="Seg_4669" s="T159">leben-FUT-1PL=Q</ta>
            <ta e="T161" id="Seg_4670" s="T160">wegfahren-IMP.1DU</ta>
            <ta e="T162" id="Seg_4671" s="T161">alter.Mann.[NOM]</ta>
            <ta e="T163" id="Seg_4672" s="T162">sich.sträuben-NEG.CVB.SIM</ta>
            <ta e="T164" id="Seg_4673" s="T163">Mensch-PL-3SG-ACC</ta>
            <ta e="T165" id="Seg_4674" s="T164">ans.andere.Ufer</ta>
            <ta e="T166" id="Seg_4675" s="T165">herausnehmen-CVB.SEQ</ta>
            <ta e="T167" id="Seg_4676" s="T166">werfen-PST2.[3SG]</ta>
            <ta e="T168" id="Seg_4677" s="T167">kleines.Boot-INSTR</ta>
            <ta e="T169" id="Seg_4678" s="T168">Laajku.[NOM]</ta>
            <ta e="T170" id="Seg_4679" s="T169">bellen-CVB.SIM-bellen-CVB.SIM</ta>
            <ta e="T171" id="Seg_4680" s="T170">laufen-CVB.SIM</ta>
            <ta e="T172" id="Seg_4681" s="T171">gehen-EP-PST2.[3SG]</ta>
            <ta e="T173" id="Seg_4682" s="T172">am.Abend-ADVZ</ta>
            <ta e="T174" id="Seg_4683" s="T173">tragen-EP-REFL-CVB.SEQ</ta>
            <ta e="T175" id="Seg_4684" s="T174">aufhören-PTCP.PRS-3PL-ACC</ta>
            <ta e="T176" id="Seg_4685" s="T175">mit</ta>
            <ta e="T177" id="Seg_4686" s="T176">dieses</ta>
            <ta e="T178" id="Seg_4687" s="T177">Kind.[NOM]</ta>
            <ta e="T179" id="Seg_4688" s="T178">Mama-3SG-DAT/LOC</ta>
            <ta e="T180" id="Seg_4689" s="T179">kommen-CVB.SEQ</ta>
            <ta e="T181" id="Seg_4690" s="T180">Knie-3SG-DAT/LOC</ta>
            <ta e="T182" id="Seg_4691" s="T181">sich.setzen-PST2.[3SG]</ta>
            <ta e="T183" id="Seg_4692" s="T182">Mama.[NOM]</ta>
            <ta e="T184" id="Seg_4693" s="T183">1SG.[NOM]</ta>
            <ta e="T185" id="Seg_4694" s="T184">kleines.Boot-VBZ-PTCP.FUT-1SG-ACC</ta>
            <ta e="T186" id="Seg_4695" s="T185">wollen-PRS-1SG</ta>
            <ta e="T187" id="Seg_4696" s="T186">jenes.[NOM]</ta>
            <ta e="T188" id="Seg_4697" s="T187">was=Q</ta>
            <ta e="T189" id="Seg_4698" s="T188">wieder</ta>
            <ta e="T190" id="Seg_4699" s="T189">alter.Mann.[NOM]</ta>
            <ta e="T191" id="Seg_4700" s="T190">sich.ärgern-PST2.[3SG]</ta>
            <ta e="T192" id="Seg_4701" s="T191">sich.setzen-CVB.SIM</ta>
            <ta e="T193" id="Seg_4702" s="T192">fallen-CVB.SEQ</ta>
            <ta e="T194" id="Seg_4703" s="T193">nachdem</ta>
            <ta e="T195" id="Seg_4704" s="T194">doch</ta>
            <ta e="T196" id="Seg_4705" s="T195">besser</ta>
            <ta e="T197" id="Seg_4706" s="T196">kleines.Boot-VBZ.[IMP.2SG]</ta>
            <ta e="T198" id="Seg_4707" s="T197">fern</ta>
            <ta e="T199" id="Seg_4708" s="T198">nur</ta>
            <ta e="T200" id="Seg_4709" s="T199">gehen-EP-NEG.[IMP.2SG]</ta>
            <ta e="T201" id="Seg_4710" s="T200">Q</ta>
            <ta e="T202" id="Seg_4711" s="T201">nein</ta>
            <ta e="T203" id="Seg_4712" s="T202">gehen-FUT-1SG</ta>
            <ta e="T204" id="Seg_4713" s="T203">NEG-3SG</ta>
            <ta e="T205" id="Seg_4714" s="T204">Fluss.[NOM]</ta>
            <ta e="T206" id="Seg_4715" s="T205">Ufer-3SG-DAT/LOC</ta>
            <ta e="T207" id="Seg_4716" s="T206">nur</ta>
            <ta e="T208" id="Seg_4717" s="T207">gehen-FUT-1SG</ta>
            <ta e="T209" id="Seg_4718" s="T208">sagen-PST2.[3SG]</ta>
            <ta e="T210" id="Seg_4719" s="T209">Laajku.[NOM]</ta>
            <ta e="T211" id="Seg_4720" s="T210">Ungeheuer.[NOM]</ta>
            <ta e="T212" id="Seg_4721" s="T211">Laajku.[NOM]</ta>
            <ta e="T213" id="Seg_4722" s="T212">kleines.Boot-DAT/LOC</ta>
            <ta e="T214" id="Seg_4723" s="T213">sich.setzen-PST2.[3SG]</ta>
            <ta e="T215" id="Seg_4724" s="T214">ganz.leise</ta>
            <ta e="T216" id="Seg_4725" s="T215">Ufer.[NOM]</ta>
            <ta e="T217" id="Seg_4726" s="T216">entlang</ta>
            <ta e="T218" id="Seg_4727" s="T217">rudern-EP-MED-CVB.SIM</ta>
            <ta e="T219" id="Seg_4728" s="T218">gehen-EP-PST2.[3SG]</ta>
            <ta e="T220" id="Seg_4729" s="T219">alter.Mann-PROPR</ta>
            <ta e="T221" id="Seg_4730" s="T220">Alte.[NOM]</ta>
            <ta e="T222" id="Seg_4731" s="T221">Haus-DAT/LOC</ta>
            <ta e="T223" id="Seg_4732" s="T222">hineingehen-PTCP.PRS-3PL-ACC</ta>
            <ta e="T224" id="Seg_4733" s="T223">mit</ta>
            <ta e="T225" id="Seg_4734" s="T224">sich.wegbewegen-CVB.SEQ</ta>
            <ta e="T226" id="Seg_4735" s="T225">und</ta>
            <ta e="T227" id="Seg_4736" s="T226">sich.wegbewegen-CVB.SEQ</ta>
            <ta e="T228" id="Seg_4737" s="T227">gehen-PST2.[3SG]</ta>
            <ta e="T229" id="Seg_4738" s="T228">dieses</ta>
            <ta e="T230" id="Seg_4739" s="T229">Kind.[NOM]</ta>
            <ta e="T231" id="Seg_4740" s="T230">Fluss.[NOM]</ta>
            <ta e="T232" id="Seg_4741" s="T231">Mitte-3SG-DAT/LOC</ta>
            <ta e="T233" id="Seg_4742" s="T232">ankommen-PST2.[3SG]</ta>
            <ta e="T234" id="Seg_4743" s="T233">EMPH</ta>
            <ta e="T235" id="Seg_4744" s="T234">groß</ta>
            <ta e="T236" id="Seg_4745" s="T235">Mensch.[NOM]</ta>
            <ta e="T237" id="Seg_4746" s="T236">werden-PST2.[3SG]</ta>
            <ta e="T238" id="Seg_4747" s="T237">alter.Mann.[NOM]</ta>
            <ta e="T239" id="Seg_4748" s="T238">hinausgehen-EP-PST2-3SG</ta>
            <ta e="T240" id="Seg_4749" s="T239">sehen-PST2-3SG</ta>
            <ta e="T241" id="Seg_4750" s="T240">Sohn-3SG.[NOM]</ta>
            <ta e="T242" id="Seg_4751" s="T241">sich.wegbewegen-PST2.[3SG]</ta>
            <ta e="T243" id="Seg_4752" s="T242">na</ta>
            <ta e="T244" id="Seg_4753" s="T243">wohin</ta>
            <ta e="T245" id="Seg_4754" s="T244">gehen-PRS-2SG</ta>
            <ta e="T246" id="Seg_4755" s="T245">sich.wegbewegen-EP-NEG.[IMP.2SG]</ta>
            <ta e="T247" id="Seg_4756" s="T246">sagen-PRS-1SG</ta>
            <ta e="T248" id="Seg_4757" s="T247">Wasser-DAT/LOC</ta>
            <ta e="T249" id="Seg_4758" s="T248">fallen-PST1-2SG</ta>
            <ta e="T250" id="Seg_4759" s="T249">Laajku.[NOM]</ta>
            <ta e="T251" id="Seg_4760" s="T250">Fluss.[NOM]</ta>
            <ta e="T252" id="Seg_4761" s="T251">Mitte-3SG-DAT/LOC</ta>
            <ta e="T253" id="Seg_4762" s="T252">ankommen-CVB.SEQ</ta>
            <ta e="T254" id="Seg_4763" s="T253">nachdem</ta>
            <ta e="T255" id="Seg_4764" s="T254">schreien-PST2.[3SG]</ta>
            <ta e="T256" id="Seg_4765" s="T255">ha-ha</ta>
            <ta e="T257" id="Seg_4766" s="T256">1SG.[NOM]</ta>
            <ta e="T258" id="Seg_4767" s="T257">Laajku-1SG</ta>
            <ta e="T259" id="Seg_4768" s="T258">EVID</ta>
            <ta e="T260" id="Seg_4769" s="T259">2PL.[NOM]</ta>
            <ta e="T261" id="Seg_4770" s="T260">jenes-ACC</ta>
            <ta e="T262" id="Seg_4771" s="T261">bemerken-PST2.NEG-2PL</ta>
            <ta e="T263" id="Seg_4772" s="T262">Laajku.[NOM]</ta>
            <ta e="T264" id="Seg_4773" s="T263">entfliehen-PST2.[3SG]</ta>
            <ta e="T265" id="Seg_4774" s="T264">alter.Mann-PROPR</ta>
            <ta e="T266" id="Seg_4775" s="T265">Alte.[NOM]</ta>
            <ta e="T267" id="Seg_4776" s="T266">kämpfen-CVB.SIM</ta>
            <ta e="T268" id="Seg_4777" s="T267">bleiben-PST2-3PL</ta>
            <ta e="T269" id="Seg_4778" s="T268">dieses</ta>
            <ta e="T270" id="Seg_4779" s="T269">Junge.[NOM]</ta>
            <ta e="T271" id="Seg_4780" s="T270">go-CVB.SEQ</ta>
            <ta e="T272" id="Seg_4781" s="T271">gehen-PST2.[3SG]</ta>
            <ta e="T273" id="Seg_4782" s="T272">wie.viel</ta>
            <ta e="T274" id="Seg_4783" s="T273">sein-FUT.[3SG]=Q</ta>
            <ta e="T275" id="Seg_4784" s="T274">sehen-PST2-3SG</ta>
            <ta e="T276" id="Seg_4785" s="T275">Haus-PL.[NOM]</ta>
            <ta e="T277" id="Seg_4786" s="T276">stehen-PRS-3PL</ta>
            <ta e="T278" id="Seg_4787" s="T277">Schlitten-PROPR</ta>
            <ta e="T279" id="Seg_4788" s="T278">Rentier-PL.[NOM]</ta>
            <ta e="T280" id="Seg_4789" s="T279">binden-EP-PASS/REFL-CVB.SIM</ta>
            <ta e="T281" id="Seg_4790" s="T280">liegen-PRS-3PL</ta>
            <ta e="T282" id="Seg_4791" s="T281">eins</ta>
            <ta e="T283" id="Seg_4792" s="T282">Haus-DAT/LOC</ta>
            <ta e="T284" id="Seg_4793" s="T283">hineingehen-PST2-3SG</ta>
            <ta e="T285" id="Seg_4794" s="T284">einsam</ta>
            <ta e="T286" id="Seg_4795" s="T285">Frau.[NOM]</ta>
            <ta e="T287" id="Seg_4796" s="T286">sitzen-PRS.[3SG]</ta>
            <ta e="T288" id="Seg_4797" s="T287">Laajku.[NOM]</ta>
            <ta e="T289" id="Seg_4798" s="T288">Tee-VBZ-PST2.[3SG]</ta>
            <ta e="T290" id="Seg_4799" s="T289">dieses</ta>
            <ta e="T291" id="Seg_4800" s="T290">was.[NOM]=Q</ta>
            <ta e="T292" id="Seg_4801" s="T291">Versammlung.[NOM]</ta>
            <ta e="T293" id="Seg_4802" s="T292">Q</ta>
            <ta e="T294" id="Seg_4803" s="T293">Hochzeit.[NOM]</ta>
            <ta e="T295" id="Seg_4804" s="T294">Q</ta>
            <ta e="T296" id="Seg_4805" s="T295">nein</ta>
            <ta e="T297" id="Seg_4806" s="T296">1PL.[NOM]</ta>
            <ta e="T298" id="Seg_4807" s="T297">Fürst-1PL.[NOM]</ta>
            <ta e="T299" id="Seg_4808" s="T298">eins</ta>
            <ta e="T300" id="Seg_4809" s="T299">Tochter-PROPR.[NOM]</ta>
            <ta e="T301" id="Seg_4810" s="T300">jenes</ta>
            <ta e="T302" id="Seg_4811" s="T301">Tochter-DAT/LOC</ta>
            <ta e="T303" id="Seg_4812" s="T302">Fürst.[NOM]</ta>
            <ta e="T304" id="Seg_4813" s="T303">Rätsel-3SG-ACC</ta>
            <ta e="T305" id="Seg_4814" s="T304">erraten-PTCP.PST</ta>
            <ta e="T306" id="Seg_4815" s="T305">Mensch.[NOM]</ta>
            <ta e="T307" id="Seg_4816" s="T306">nur</ta>
            <ta e="T308" id="Seg_4817" s="T307">Mann.[NOM]</ta>
            <ta e="T309" id="Seg_4818" s="T308">werden-PRS.[3SG]</ta>
            <ta e="T310" id="Seg_4819" s="T309">sagen-PST2.[3SG]</ta>
            <ta e="T311" id="Seg_4820" s="T310">Frau.[NOM]</ta>
            <ta e="T312" id="Seg_4821" s="T311">Laajku.[NOM]</ta>
            <ta e="T313" id="Seg_4822" s="T312">hinausgehen-EP-PST2.[3SG]</ta>
            <ta e="T314" id="Seg_4823" s="T313">und</ta>
            <ta e="T315" id="Seg_4824" s="T314">Fluss.[NOM]</ta>
            <ta e="T316" id="Seg_4825" s="T315">Rand-3SG-DAT/LOC</ta>
            <ta e="T317" id="Seg_4826" s="T316">hineingehen-PST2.[3SG]</ta>
            <ta e="T318" id="Seg_4827" s="T317">Gras-EP-INSTR</ta>
            <ta e="T319" id="Seg_4828" s="T318">bedecken-REFL-CVB.SEQ</ta>
            <ta e="T320" id="Seg_4829" s="T319">nachdem</ta>
            <ta e="T321" id="Seg_4830" s="T320">sich.hinlegen-PST2.[3SG]</ta>
            <ta e="T322" id="Seg_4831" s="T321">sehen-PST2-3SG</ta>
            <ta e="T323" id="Seg_4832" s="T322">zwei</ta>
            <ta e="T324" id="Seg_4833" s="T323">Mädchen.[NOM]</ta>
            <ta e="T325" id="Seg_4834" s="T324">Wasser.[NOM]</ta>
            <ta e="T326" id="Seg_4835" s="T325">schöpfen-CVB.SIM</ta>
            <ta e="T327" id="Seg_4836" s="T326">gehen-PRS-3PL</ta>
            <ta e="T328" id="Seg_4837" s="T327">Schulterjoch-VBZ-CVB.SEQ</ta>
            <ta e="T329" id="Seg_4838" s="T328">zwei</ta>
            <ta e="T330" id="Seg_4839" s="T329">Behälter-ACC</ta>
            <ta e="T331" id="Seg_4840" s="T330">Schulter-3PL-DAT/LOC</ta>
            <ta e="T332" id="Seg_4841" s="T331">auf.den.Rücken.nehmen-PST2-3PL</ta>
            <ta e="T333" id="Seg_4842" s="T332">Junge.[NOM]</ta>
            <ta e="T334" id="Seg_4843" s="T333">Bein-3SG-ABL</ta>
            <ta e="T335" id="Seg_4844" s="T334">reich</ta>
            <ta e="T336" id="Seg_4845" s="T335">Mädchen.[NOM]</ta>
            <ta e="T337" id="Seg_4846" s="T336">stolpern-CVB.SEQ</ta>
            <ta e="T338" id="Seg_4847" s="T337">Wasser-3SG-ACC</ta>
            <ta e="T339" id="Seg_4848" s="T338">verschütten-PST2.[3SG]</ta>
            <ta e="T340" id="Seg_4849" s="T339">dieses.[NOM]</ta>
            <ta e="T341" id="Seg_4850" s="T340">ganz</ta>
            <ta e="T342" id="Seg_4851" s="T341">Vater-1SG.[NOM]</ta>
            <ta e="T343" id="Seg_4852" s="T342">wegen</ta>
            <ta e="T344" id="Seg_4853" s="T343">Ehemann-DAT/LOC</ta>
            <ta e="T345" id="Seg_4854" s="T344">hinausgehen-EP-PTCP.PST</ta>
            <ta e="T346" id="Seg_4855" s="T345">sein-PST2-EP-1SG</ta>
            <ta e="T347" id="Seg_4856" s="T346">sein-COND.[3SG]</ta>
            <ta e="T348" id="Seg_4857" s="T347">Wasser.[NOM]</ta>
            <ta e="T349" id="Seg_4858" s="T348">schöpfen-CVB.SIM</ta>
            <ta e="T350" id="Seg_4859" s="T349">müde.werden-CVB.SIM</ta>
            <ta e="T351" id="Seg_4860" s="T350">gehen-PTCP.FUT</ta>
            <ta e="T352" id="Seg_4861" s="T351">sein-PST1-1SG</ta>
            <ta e="T353" id="Seg_4862" s="T352">Q</ta>
            <ta e="T354" id="Seg_4863" s="T353">Bein-1SG-ACC</ta>
            <ta e="T355" id="Seg_4864" s="T354">nur</ta>
            <ta e="T356" id="Seg_4865" s="T355">nass.machen-PST1-1SG</ta>
            <ta e="T357" id="Seg_4866" s="T356">Freund</ta>
            <ta e="T358" id="Seg_4867" s="T357">dieses.[NOM]</ta>
            <ta e="T359" id="Seg_4868" s="T358">Vater-2SG.[NOM]</ta>
            <ta e="T360" id="Seg_4869" s="T359">was-ACC</ta>
            <ta e="T361" id="Seg_4870" s="T360">erraten-CAUS-PRS.[3SG]</ta>
            <ta e="T362" id="Seg_4871" s="T361">Vater-1SG.[NOM]</ta>
            <ta e="T363" id="Seg_4872" s="T362">wann</ta>
            <ta e="T364" id="Seg_4873" s="T363">INDEF</ta>
            <ta e="T365" id="Seg_4874" s="T364">sieben</ta>
            <ta e="T366" id="Seg_4875" s="T365">Laus.[NOM]</ta>
            <ta e="T367" id="Seg_4876" s="T366">Haut-3SG-INSTR</ta>
            <ta e="T368" id="Seg_4877" s="T367">Lastendecke.[NOM]</ta>
            <ta e="T369" id="Seg_4878" s="T368">machen-PTCP.PST</ta>
            <ta e="T370" id="Seg_4879" s="T369">sein-PST1-3SG</ta>
            <ta e="T371" id="Seg_4880" s="T370">dieses-ACC</ta>
            <ta e="T372" id="Seg_4881" s="T371">machen-PTCP.PRS</ta>
            <ta e="T373" id="Seg_4882" s="T372">sein-PST2-3SG</ta>
            <ta e="T374" id="Seg_4883" s="T373">Q</ta>
            <ta e="T375" id="Seg_4884" s="T374">doch</ta>
            <ta e="T376" id="Seg_4885" s="T375">gehen-IMP.1DU</ta>
            <ta e="T377" id="Seg_4886" s="T376">Wasser.[NOM]</ta>
            <ta e="T378" id="Seg_4887" s="T377">NEG</ta>
            <ta e="T379" id="Seg_4888" s="T378">ertragen-NEG.[3SG]</ta>
            <ta e="T380" id="Seg_4889" s="T379">Holz.[NOM]</ta>
            <ta e="T381" id="Seg_4890" s="T380">NEG</ta>
            <ta e="T382" id="Seg_4891" s="T381">ertragen-NEG.[3SG]</ta>
            <ta e="T383" id="Seg_4892" s="T382">so.viel-PROPR</ta>
            <ta e="T384" id="Seg_4893" s="T383">Gast-DAT/LOC</ta>
            <ta e="T385" id="Seg_4894" s="T384">Laajku.[NOM]</ta>
            <ta e="T386" id="Seg_4895" s="T385">zurückkommen-CVB.SEQ</ta>
            <ta e="T387" id="Seg_4896" s="T386">kommen-CVB.SEQ</ta>
            <ta e="T388" id="Seg_4897" s="T387">dieses</ta>
            <ta e="T389" id="Seg_4898" s="T388">Haus-3SG-DAT/LOC</ta>
            <ta e="T390" id="Seg_4899" s="T389">hineingehen-PST2.[3SG]</ta>
            <ta e="T391" id="Seg_4900" s="T390">übernachten-PST2.[3SG]</ta>
            <ta e="T392" id="Seg_4901" s="T391">Morgen.[NOM]</ta>
            <ta e="T393" id="Seg_4902" s="T392">aufstehen-PST2-3SG</ta>
            <ta e="T394" id="Seg_4903" s="T393">wieder</ta>
            <ta e="T395" id="Seg_4904" s="T394">Gast.[NOM]</ta>
            <ta e="T396" id="Seg_4905" s="T395">voll.[NOM]</ta>
            <ta e="T397" id="Seg_4906" s="T396">dieses</ta>
            <ta e="T398" id="Seg_4907" s="T397">Junge.[NOM]</ta>
            <ta e="T399" id="Seg_4908" s="T398">auch</ta>
            <ta e="T400" id="Seg_4909" s="T399">Fürst-DAT/LOC</ta>
            <ta e="T401" id="Seg_4910" s="T400">gehen-CAP.[3SG]</ta>
            <ta e="T402" id="Seg_4911" s="T401">Gast-PL-ACC</ta>
            <ta e="T403" id="Seg_4912" s="T402">mit</ta>
            <ta e="T404" id="Seg_4913" s="T403">zusammen</ta>
            <ta e="T405" id="Seg_4914" s="T404">hineingehen-PST2.[3SG]</ta>
            <ta e="T406" id="Seg_4915" s="T405">sehen-PST2-3SG</ta>
            <ta e="T407" id="Seg_4916" s="T406">Haus-ACC</ta>
            <ta e="T408" id="Seg_4917" s="T407">voll</ta>
            <ta e="T409" id="Seg_4918" s="T408">Mann.[NOM]</ta>
            <ta e="T410" id="Seg_4919" s="T409">Mensch.[NOM]</ta>
            <ta e="T411" id="Seg_4920" s="T410">oh</ta>
            <ta e="T412" id="Seg_4921" s="T411">heute</ta>
            <ta e="T413" id="Seg_4922" s="T412">neu</ta>
            <ta e="T414" id="Seg_4923" s="T413">Gast.[NOM]</ta>
            <ta e="T415" id="Seg_4924" s="T414">kommen-PST2.[3SG]</ta>
            <ta e="T416" id="Seg_4925" s="T415">doch</ta>
            <ta e="T417" id="Seg_4926" s="T416">vielleicht</ta>
            <ta e="T418" id="Seg_4927" s="T417">2SG.[NOM]</ta>
            <ta e="T419" id="Seg_4928" s="T418">1SG.[NOM]</ta>
            <ta e="T420" id="Seg_4929" s="T419">Rätsel-1SG-ACC</ta>
            <ta e="T421" id="Seg_4930" s="T420">erraten-FUT-2SG</ta>
            <ta e="T422" id="Seg_4931" s="T421">sagen-PST2.[3SG]</ta>
            <ta e="T423" id="Seg_4932" s="T422">Fürst.[NOM]</ta>
            <ta e="T424" id="Seg_4933" s="T423">was</ta>
            <ta e="T425" id="Seg_4934" s="T424">Rätsel-PROPR-2SG=Q</ta>
            <ta e="T426" id="Seg_4935" s="T425">wie.viel</ta>
            <ta e="T427" id="Seg_4936" s="T426">Verstand-EP-1SG.[NOM]</ta>
            <ta e="T428" id="Seg_4937" s="T427">herausnehmen-EP-MED-CVB.SEQ</ta>
            <ta e="T429" id="Seg_4938" s="T428">erraten-CVB.SIM</ta>
            <ta e="T430" id="Seg_4939" s="T429">können-PTCP.FUT</ta>
            <ta e="T431" id="Seg_4940" s="T430">sein-PST1-1SG</ta>
            <ta e="T432" id="Seg_4941" s="T431">Laajku.[NOM]</ta>
            <ta e="T433" id="Seg_4942" s="T432">sagen-PST2.[3SG]</ta>
            <ta e="T434" id="Seg_4943" s="T433">1SG.[NOM]</ta>
            <ta e="T435" id="Seg_4944" s="T434">Name-VBZ-EP-CAUS-PRS-1SG</ta>
            <ta e="T436" id="Seg_4945" s="T435">Lastendecke.[NOM]</ta>
            <ta e="T437" id="Seg_4946" s="T436">was-EP-INSTR</ta>
            <ta e="T438" id="Seg_4947" s="T437">gemacht.werden-EP-PTCP.PST.[NOM]=Q</ta>
            <ta e="T439" id="Seg_4948" s="T438">sagen-CVB.SEQ</ta>
            <ta e="T440" id="Seg_4949" s="T439">Fürst.[NOM]</ta>
            <ta e="T441" id="Seg_4950" s="T440">fragen-PST2.[3SG]</ta>
            <ta e="T442" id="Seg_4951" s="T441">falls</ta>
            <ta e="T443" id="Seg_4952" s="T442">1SG.[NOM]</ta>
            <ta e="T444" id="Seg_4953" s="T443">Verstand-EP-1SG.[NOM]</ta>
            <ta e="T445" id="Seg_4954" s="T444">gehen-PTCP.PRS</ta>
            <ta e="T446" id="Seg_4955" s="T445">Sache-3SG-INSTR</ta>
            <ta e="T447" id="Seg_4956" s="T446">machen-FUT-2SG</ta>
            <ta e="T448" id="Seg_4957" s="T447">Q</ta>
            <ta e="T449" id="Seg_4958" s="T448">Haut.[NOM]</ta>
            <ta e="T450" id="Seg_4959" s="T449">sein-TEMP-3SG</ta>
            <ta e="T451" id="Seg_4960" s="T450">sein-IMP.3SG</ta>
            <ta e="T452" id="Seg_4961" s="T451">1SG.[NOM]</ta>
            <ta e="T453" id="Seg_4962" s="T452">Gedanke-1SG-DAT/LOC</ta>
            <ta e="T454" id="Seg_4963" s="T453">sieben</ta>
            <ta e="T455" id="Seg_4964" s="T454">Laus.[NOM]</ta>
            <ta e="T456" id="Seg_4965" s="T455">Haut-3SG-INSTR</ta>
            <ta e="T457" id="Seg_4966" s="T456">gemacht.werden-EP-PTCP.PST.[NOM]</ta>
            <ta e="T458" id="Seg_4967" s="T457">offenbar</ta>
            <ta e="T459" id="Seg_4968" s="T458">Laajku.[NOM]</ta>
            <ta e="T460" id="Seg_4969" s="T459">sagen-PST2.[3SG]</ta>
            <ta e="T461" id="Seg_4970" s="T460">oh</ta>
            <ta e="T462" id="Seg_4971" s="T461">was.[NOM]</ta>
            <ta e="T463" id="Seg_4972" s="T462">Verstand-PROPR</ta>
            <ta e="T464" id="Seg_4973" s="T463">Mensch-3SG-2SG=Q</ta>
            <ta e="T465" id="Seg_4974" s="T464">erraten-PST1-2SG</ta>
            <ta e="T466" id="Seg_4975" s="T465">EMPH</ta>
            <ta e="T467" id="Seg_4976" s="T466">doch</ta>
            <ta e="T468" id="Seg_4977" s="T467">dann</ta>
            <ta e="T469" id="Seg_4978" s="T468">1SG.[NOM]</ta>
            <ta e="T470" id="Seg_4979" s="T469">Tochter-1SG-ACC</ta>
            <ta e="T471" id="Seg_4980" s="T470">Frau.[NOM]</ta>
            <ta e="T472" id="Seg_4981" s="T471">machen-PTCP.PRS</ta>
            <ta e="T473" id="Seg_4982" s="T472">Bestimmung-PROPR-2SG</ta>
            <ta e="T474" id="Seg_4983" s="T473">sein-PST2.[3SG]</ta>
            <ta e="T475" id="Seg_4984" s="T474">wie</ta>
            <ta e="T476" id="Seg_4985" s="T475">machen-FUT-2SG=Q</ta>
            <ta e="T477" id="Seg_4986" s="T476">morgen</ta>
            <ta e="T478" id="Seg_4987" s="T477">feiern-FUT-1PL</ta>
            <ta e="T479" id="Seg_4988" s="T478">Laajku.[NOM]</ta>
            <ta e="T480" id="Seg_4989" s="T479">reich</ta>
            <ta e="T481" id="Seg_4990" s="T480">Fürst.[NOM]</ta>
            <ta e="T482" id="Seg_4991" s="T481">schön</ta>
            <ta e="T483" id="Seg_4992" s="T482">Tochter-3SG-ACC</ta>
            <ta e="T484" id="Seg_4993" s="T483">Frau-VBZ-CVB.SEQ</ta>
            <ta e="T485" id="Seg_4994" s="T484">reich.sein-CVB.SEQ-sich.satt.essen-CVB.SEQ</ta>
            <ta e="T486" id="Seg_4995" s="T485">leben-PST2.[3SG]</ta>
            <ta e="T487" id="Seg_4996" s="T486">was.[NOM]</ta>
            <ta e="T488" id="Seg_4997" s="T487">NEG</ta>
            <ta e="T489" id="Seg_4998" s="T488">Qual-ACC</ta>
            <ta e="T490" id="Seg_4999" s="T489">wissen-NEG.CVB.SIM</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_5000" s="T0">одиноко</ta>
            <ta e="T2" id="Seg_5001" s="T1">создаваться-PTCP.PST</ta>
            <ta e="T3" id="Seg_5002" s="T2">мать-3SG-ACC-отец-3SG-ACC</ta>
            <ta e="T4" id="Seg_5003" s="T3">помнить-NEG.PTCP</ta>
            <ta e="T5" id="Seg_5004" s="T4">Лаайку.[NOM]</ta>
            <ta e="T6" id="Seg_5005" s="T5">говорить-CVB.SEQ</ta>
            <ta e="T7" id="Seg_5006" s="T6">собака.[NOM]</ta>
            <ta e="T8" id="Seg_5007" s="T7">жить-PST2.[3SG]</ta>
            <ta e="T9" id="Seg_5008" s="T8">однажды</ta>
            <ta e="T10" id="Seg_5009" s="T9">идти-CVB.SIM</ta>
            <ta e="T11" id="Seg_5010" s="T10">идти-EP-PST2.[3SG]</ta>
            <ta e="T12" id="Seg_5011" s="T11">человек.[NOM]</ta>
            <ta e="T13" id="Seg_5012" s="T12">искать-CVB.SIM</ta>
            <ta e="T14" id="Seg_5013" s="T13">тот</ta>
            <ta e="T15" id="Seg_5014" s="T14">идти-CVB.SEQ</ta>
            <ta e="T16" id="Seg_5015" s="T15">видеть-PST2.[3SG]</ta>
            <ta e="T17" id="Seg_5016" s="T16">высокий</ta>
            <ta e="T18" id="Seg_5017" s="T17">сопка-ACC</ta>
            <ta e="T19" id="Seg_5018" s="T18">что.[NOM]</ta>
            <ta e="T20" id="Seg_5019" s="T19">быть-FUT.[3SG]=Q</ta>
            <ta e="T21" id="Seg_5020" s="T20">1SG.[NOM]</ta>
            <ta e="T22" id="Seg_5021" s="T21">туда</ta>
            <ta e="T23" id="Seg_5022" s="T22">лезть-TEMP-1SG</ta>
            <ta e="T24" id="Seg_5023" s="T23">думать-CVB.SIM</ta>
            <ta e="T25" id="Seg_5024" s="T24">думать-PST2.[3SG]</ta>
            <ta e="T26" id="Seg_5025" s="T25">сопка.[NOM]</ta>
            <ta e="T27" id="Seg_5026" s="T26">верхняя.часть-3SG-DAT/LOC</ta>
            <ta e="T28" id="Seg_5027" s="T27">лезть-CVB.SEQ</ta>
            <ta e="T29" id="Seg_5028" s="T28">видеть-PST2-3SG</ta>
            <ta e="T30" id="Seg_5029" s="T29">Баба.Яга.[NOM]</ta>
            <ta e="T31" id="Seg_5030" s="T30">идти-PRS.[3SG]</ta>
            <ta e="T32" id="Seg_5031" s="T31">один.из.двух</ta>
            <ta e="T33" id="Seg_5032" s="T32">сторона.[NOM]</ta>
            <ta e="T34" id="Seg_5033" s="T33">сторона-3SG-ABL</ta>
            <ta e="T35" id="Seg_5034" s="T34">сопка.[NOM]</ta>
            <ta e="T36" id="Seg_5035" s="T35">середина-3SG-DAT/LOC</ta>
            <ta e="T37" id="Seg_5036" s="T36">доезжать-PST2.[3SG]</ta>
            <ta e="T38" id="Seg_5037" s="T37">да</ta>
            <ta e="T39" id="Seg_5038" s="T38">сидеть.на.задних.лапах-NMNZ.[NOM]</ta>
            <ta e="T40" id="Seg_5039" s="T39">делать-PST2.[3SG]</ta>
            <ta e="T41" id="Seg_5040" s="T40">Лаайку.[NOM]</ta>
            <ta e="T42" id="Seg_5041" s="T41">оборотень.[NOM]</ta>
            <ta e="T43" id="Seg_5042" s="T42">очень</ta>
            <ta e="T44" id="Seg_5043" s="T43">быть-PST1-3SG</ta>
            <ta e="T45" id="Seg_5044" s="T44">быть-PST2.[3SG]</ta>
            <ta e="T46" id="Seg_5045" s="T45">лиса.[NOM]</ta>
            <ta e="T47" id="Seg_5046" s="T46">ребенок.[NOM]</ta>
            <ta e="T48" id="Seg_5047" s="T47">становиться-CVB.SEQ</ta>
            <ta e="T49" id="Seg_5048" s="T48">после</ta>
            <ta e="T50" id="Seg_5049" s="T49">сопка.[NOM]</ta>
            <ta e="T51" id="Seg_5050" s="T50">нижняя.часть-3SG-ACC</ta>
            <ta e="T52" id="Seg_5051" s="T51">к</ta>
            <ta e="T53" id="Seg_5052" s="T52">кататься-CVB.SEQ</ta>
            <ta e="T54" id="Seg_5053" s="T53">оставаться-PST2.[3SG]</ta>
            <ta e="T55" id="Seg_5054" s="T54">о</ta>
            <ta e="T56" id="Seg_5055" s="T55">грех.[NOM]</ta>
            <ta e="T57" id="Seg_5056" s="T56">да</ta>
            <ta e="T58" id="Seg_5057" s="T57">как</ta>
            <ta e="T59" id="Seg_5058" s="T58">ребенок-VBZ-PTCP.PRS-1SG-ACC</ta>
            <ta e="T60" id="Seg_5059" s="T59">замечать-NEG.CVB.SIM</ta>
            <ta e="T61" id="Seg_5060" s="T60">оставаться-PST2-1SG=Q</ta>
            <ta e="T62" id="Seg_5061" s="T61">говорить-PST2.[3SG]</ta>
            <ta e="T63" id="Seg_5062" s="T62">да</ta>
            <ta e="T64" id="Seg_5063" s="T63">ребенок-ACC</ta>
            <ta e="T65" id="Seg_5064" s="T64">укутать-CVB.SIM</ta>
            <ta e="T66" id="Seg_5065" s="T65">хватать-CVB.SEQ</ta>
            <ta e="T67" id="Seg_5066" s="T66">после</ta>
            <ta e="T68" id="Seg_5067" s="T67">дом-3SG-DAT/LOC</ta>
            <ta e="T69" id="Seg_5068" s="T68">носить-CVB.SIM</ta>
            <ta e="T70" id="Seg_5069" s="T69">идти-PST2.[3SG]</ta>
            <ta e="T71" id="Seg_5070" s="T70">Баба.Яга.[NOM]</ta>
            <ta e="T72" id="Seg_5071" s="T71">старик-3SG-ACC</ta>
            <ta e="T73" id="Seg_5072" s="T72">с</ta>
            <ta e="T74" id="Seg_5073" s="T73">река.[NOM]</ta>
            <ta e="T75" id="Seg_5074" s="T74">берег-3SG-DAT/LOC</ta>
            <ta e="T76" id="Seg_5075" s="T75">жить-PTCP.PRS</ta>
            <ta e="T77" id="Seg_5076" s="T76">быть-PST2-3PL</ta>
            <ta e="T78" id="Seg_5077" s="T77">ребенок-POSS</ta>
            <ta e="T79" id="Seg_5078" s="T78">NEG-3PL</ta>
            <ta e="T80" id="Seg_5079" s="T79">быть-PST2.[3SG]</ta>
            <ta e="T81" id="Seg_5080" s="T80">одинокий</ta>
            <ta e="T82" id="Seg_5081" s="T81">ручной</ta>
            <ta e="T83" id="Seg_5082" s="T82">олень-PROPR-3PL</ta>
            <ta e="T84" id="Seg_5083" s="T83">ребенок-3PL.[NOM]</ta>
            <ta e="T85" id="Seg_5084" s="T84">с.каждым.днем</ta>
            <ta e="T86" id="Seg_5085" s="T85">расти-CVB.SEQ</ta>
            <ta e="T87" id="Seg_5086" s="T86">идти-PST2.[3SG]</ta>
            <ta e="T88" id="Seg_5087" s="T87">язык-VBZ-PST2.[3SG]</ta>
            <ta e="T89" id="Seg_5088" s="T88">мать-3SG.[NOM]</ta>
            <ta e="T90" id="Seg_5089" s="T89">сам-3SG-ABL</ta>
            <ta e="T91" id="Seg_5090" s="T90">сокуй-COM</ta>
            <ta e="T92" id="Seg_5091" s="T91">одежда.[NOM]</ta>
            <ta e="T93" id="Seg_5092" s="T92">шить-PST2.[3SG]</ta>
            <ta e="T94" id="Seg_5093" s="T93">этот</ta>
            <ta e="T95" id="Seg_5094" s="T94">ребенок.[NOM]</ta>
            <ta e="T96" id="Seg_5095" s="T95">бежать-CVB.SIM</ta>
            <ta e="T97" id="Seg_5096" s="T96">идти-PTCP.PRS</ta>
            <ta e="T98" id="Seg_5097" s="T97">становиться-PST2.[3SG]</ta>
            <ta e="T99" id="Seg_5098" s="T98">дом-3SG-GEN</ta>
            <ta e="T100" id="Seg_5099" s="T99">место.около-DAT/LOC</ta>
            <ta e="T101" id="Seg_5100" s="T100">старик.[NOM]</ta>
            <ta e="T102" id="Seg_5101" s="T101">день.[NOM]</ta>
            <ta e="T103" id="Seg_5102" s="T102">каждый</ta>
            <ta e="T104" id="Seg_5103" s="T103">сеть-VBZ-PTCP.PRS</ta>
            <ta e="T105" id="Seg_5104" s="T104">обыкновение-PROPR.[NOM]</ta>
            <ta e="T106" id="Seg_5105" s="T105">быть-PST2.[3SG]</ta>
            <ta e="T107" id="Seg_5106" s="T106">этот</ta>
            <ta e="T108" id="Seg_5107" s="T107">ребенок-ACC</ta>
            <ta e="T109" id="Seg_5108" s="T108">рыба-EP-INSTR</ta>
            <ta e="T110" id="Seg_5109" s="T109">только</ta>
            <ta e="T111" id="Seg_5110" s="T110">есть-EP-CAUS-PTCP.PRS</ta>
            <ta e="T112" id="Seg_5111" s="T111">быть-PST2-3PL</ta>
            <ta e="T113" id="Seg_5112" s="T112">Лаайку.[NOM]</ta>
            <ta e="T114" id="Seg_5113" s="T113">однажды</ta>
            <ta e="T115" id="Seg_5114" s="T114">говорить-PST2.[3SG]</ta>
            <ta e="T116" id="Seg_5115" s="T115">мама.[NOM]</ta>
            <ta e="T117" id="Seg_5116" s="T116">1SG.[NOM]</ta>
            <ta e="T118" id="Seg_5117" s="T117">мясо.[NOM]</ta>
            <ta e="T119" id="Seg_5118" s="T118">есть-PTCP.FUT-1SG-ACC</ta>
            <ta e="T120" id="Seg_5119" s="T119">хотеть-PRS-1SG</ta>
            <ta e="T121" id="Seg_5120" s="T120">старик.[NOM]</ta>
            <ta e="T122" id="Seg_5121" s="T121">вот</ta>
            <ta e="T123" id="Seg_5122" s="T122">убить.[IMP.2SG]</ta>
            <ta e="T124" id="Seg_5123" s="T123">яловое.животное-2SG-ACC</ta>
            <ta e="T125" id="Seg_5124" s="T124">ребенок.[NOM]</ta>
            <ta e="T126" id="Seg_5125" s="T125">мясо.[NOM]</ta>
            <ta e="T127" id="Seg_5126" s="T126">есть-PTCP.FUT.[3SG]-ACC</ta>
            <ta e="T128" id="Seg_5127" s="T127">хотеть-PST2.[3SG]</ta>
            <ta e="T129" id="Seg_5128" s="T128">старик.[NOM]</ta>
            <ta e="T130" id="Seg_5129" s="T129">олень-3SG-ACC</ta>
            <ta e="T131" id="Seg_5130" s="T130">убить-CVB.SEQ</ta>
            <ta e="T132" id="Seg_5131" s="T131">бросать-PST2.[3SG]</ta>
            <ta e="T133" id="Seg_5132" s="T132">Лаайку.[NOM]</ta>
            <ta e="T134" id="Seg_5133" s="T133">оборотень.[NOM]</ta>
            <ta e="T135" id="Seg_5134" s="T134">желание-3SG.[NOM]</ta>
            <ta e="T136" id="Seg_5135" s="T135">расти-CVB.SEQ</ta>
            <ta e="T137" id="Seg_5136" s="T136">идти-PST2.[3SG]</ta>
            <ta e="T138" id="Seg_5137" s="T137">мама.[NOM]</ta>
            <ta e="T139" id="Seg_5138" s="T138">почему</ta>
            <ta e="T140" id="Seg_5139" s="T139">река.[NOM]</ta>
            <ta e="T141" id="Seg_5140" s="T140">на.тот.берег</ta>
            <ta e="T142" id="Seg_5141" s="T141">носить-EP-REFL-NEG-1PL</ta>
            <ta e="T143" id="Seg_5142" s="T142">там</ta>
            <ta e="T144" id="Seg_5143" s="T143">сухая.равнина.[NOM]</ta>
            <ta e="T145" id="Seg_5144" s="T144">очень</ta>
            <ta e="T146" id="Seg_5145" s="T145">наверное</ta>
            <ta e="T147" id="Seg_5146" s="T146">говорить-PST2.[3SG]</ta>
            <ta e="T148" id="Seg_5147" s="T147">эй</ta>
            <ta e="T149" id="Seg_5148" s="T148">старик.[NOM]</ta>
            <ta e="T150" id="Seg_5149" s="T149">слышать-PRS-2SG</ta>
            <ta e="T151" id="Seg_5150" s="T150">Q</ta>
            <ta e="T152" id="Seg_5151" s="T151">ребенок.[NOM]</ta>
            <ta e="T153" id="Seg_5152" s="T152">слово-3SG-ACC</ta>
            <ta e="T154" id="Seg_5153" s="T153">правда-ACC</ta>
            <ta e="T155" id="Seg_5154" s="T154">делать-PRS.[3SG]</ta>
            <ta e="T156" id="Seg_5155" s="T155">какой-DAT/LOC</ta>
            <ta e="T157" id="Seg_5156" s="T156">пока</ta>
            <ta e="T158" id="Seg_5157" s="T157">один</ta>
            <ta e="T159" id="Seg_5158" s="T158">место-DAT/LOC</ta>
            <ta e="T160" id="Seg_5159" s="T159">жить-FUT-1PL=Q</ta>
            <ta e="T161" id="Seg_5160" s="T160">уезжать-IMP.1DU</ta>
            <ta e="T162" id="Seg_5161" s="T161">старик.[NOM]</ta>
            <ta e="T163" id="Seg_5162" s="T162">упрямиться-NEG.CVB.SIM</ta>
            <ta e="T164" id="Seg_5163" s="T163">человек-PL-3SG-ACC</ta>
            <ta e="T165" id="Seg_5164" s="T164">на.тот.берег</ta>
            <ta e="T166" id="Seg_5165" s="T165">вынимать-CVB.SEQ</ta>
            <ta e="T167" id="Seg_5166" s="T166">бросать-PST2.[3SG]</ta>
            <ta e="T168" id="Seg_5167" s="T167">лодочка-INSTR</ta>
            <ta e="T169" id="Seg_5168" s="T168">Лаайку.[NOM]</ta>
            <ta e="T170" id="Seg_5169" s="T169">лаять-CVB.SIM-лаять-CVB.SIM</ta>
            <ta e="T171" id="Seg_5170" s="T170">бегать-CVB.SIM</ta>
            <ta e="T172" id="Seg_5171" s="T171">идти-EP-PST2.[3SG]</ta>
            <ta e="T173" id="Seg_5172" s="T172">вечером-ADVZ</ta>
            <ta e="T174" id="Seg_5173" s="T173">носить-EP-REFL-CVB.SEQ</ta>
            <ta e="T175" id="Seg_5174" s="T174">кончать-PTCP.PRS-3PL-ACC</ta>
            <ta e="T176" id="Seg_5175" s="T175">с</ta>
            <ta e="T177" id="Seg_5176" s="T176">этот</ta>
            <ta e="T178" id="Seg_5177" s="T177">ребенок.[NOM]</ta>
            <ta e="T179" id="Seg_5178" s="T178">мама-3SG-DAT/LOC</ta>
            <ta e="T180" id="Seg_5179" s="T179">приходить-CVB.SEQ</ta>
            <ta e="T181" id="Seg_5180" s="T180">колено-3SG-DAT/LOC</ta>
            <ta e="T182" id="Seg_5181" s="T181">сесть-PST2.[3SG]</ta>
            <ta e="T183" id="Seg_5182" s="T182">мама.[NOM]</ta>
            <ta e="T184" id="Seg_5183" s="T183">1SG.[NOM]</ta>
            <ta e="T185" id="Seg_5184" s="T184">лодочка-VBZ-PTCP.FUT-1SG-ACC</ta>
            <ta e="T186" id="Seg_5185" s="T185">хотеть-PRS-1SG</ta>
            <ta e="T187" id="Seg_5186" s="T186">тот.[NOM]</ta>
            <ta e="T188" id="Seg_5187" s="T187">что=Q</ta>
            <ta e="T189" id="Seg_5188" s="T188">опять</ta>
            <ta e="T190" id="Seg_5189" s="T189">старик.[NOM]</ta>
            <ta e="T191" id="Seg_5190" s="T190">сердиться-PST2.[3SG]</ta>
            <ta e="T192" id="Seg_5191" s="T191">сесть-CVB.SIM</ta>
            <ta e="T193" id="Seg_5192" s="T192">падать-CVB.SEQ</ta>
            <ta e="T194" id="Seg_5193" s="T193">после</ta>
            <ta e="T195" id="Seg_5194" s="T194">вот</ta>
            <ta e="T196" id="Seg_5195" s="T195">лучше</ta>
            <ta e="T197" id="Seg_5196" s="T196">лодочка-VBZ.[IMP.2SG]</ta>
            <ta e="T198" id="Seg_5197" s="T197">далекий</ta>
            <ta e="T199" id="Seg_5198" s="T198">только</ta>
            <ta e="T200" id="Seg_5199" s="T199">идти-EP-NEG.[IMP.2SG]</ta>
            <ta e="T201" id="Seg_5200" s="T200">Q</ta>
            <ta e="T202" id="Seg_5201" s="T201">нет</ta>
            <ta e="T203" id="Seg_5202" s="T202">идти-FUT-1SG</ta>
            <ta e="T204" id="Seg_5203" s="T203">NEG-3SG</ta>
            <ta e="T205" id="Seg_5204" s="T204">река.[NOM]</ta>
            <ta e="T206" id="Seg_5205" s="T205">берег-3SG-DAT/LOC</ta>
            <ta e="T207" id="Seg_5206" s="T206">только</ta>
            <ta e="T208" id="Seg_5207" s="T207">идти-FUT-1SG</ta>
            <ta e="T209" id="Seg_5208" s="T208">говорить-PST2.[3SG]</ta>
            <ta e="T210" id="Seg_5209" s="T209">Лаайку.[NOM]</ta>
            <ta e="T211" id="Seg_5210" s="T210">чудовище.[NOM]</ta>
            <ta e="T212" id="Seg_5211" s="T211">Лаайку.[NOM]</ta>
            <ta e="T213" id="Seg_5212" s="T212">лодочка-DAT/LOC</ta>
            <ta e="T214" id="Seg_5213" s="T213">сесть-PST2.[3SG]</ta>
            <ta e="T215" id="Seg_5214" s="T214">потихоньку</ta>
            <ta e="T216" id="Seg_5215" s="T215">берег.[NOM]</ta>
            <ta e="T217" id="Seg_5216" s="T216">вдоль</ta>
            <ta e="T218" id="Seg_5217" s="T217">грести-EP-MED-CVB.SIM</ta>
            <ta e="T219" id="Seg_5218" s="T218">идти-EP-PST2.[3SG]</ta>
            <ta e="T220" id="Seg_5219" s="T219">старик-PROPR</ta>
            <ta e="T221" id="Seg_5220" s="T220">старуха.[NOM]</ta>
            <ta e="T222" id="Seg_5221" s="T221">дом-DAT/LOC</ta>
            <ta e="T223" id="Seg_5222" s="T222">входить-PTCP.PRS-3PL-ACC</ta>
            <ta e="T224" id="Seg_5223" s="T223">с</ta>
            <ta e="T225" id="Seg_5224" s="T224">удаляться-CVB.SEQ</ta>
            <ta e="T226" id="Seg_5225" s="T225">да</ta>
            <ta e="T227" id="Seg_5226" s="T226">удаляться-CVB.SEQ</ta>
            <ta e="T228" id="Seg_5227" s="T227">идти-PST2.[3SG]</ta>
            <ta e="T229" id="Seg_5228" s="T228">этот</ta>
            <ta e="T230" id="Seg_5229" s="T229">ребенок.[NOM]</ta>
            <ta e="T231" id="Seg_5230" s="T230">река.[NOM]</ta>
            <ta e="T232" id="Seg_5231" s="T231">середина-3SG-DAT/LOC</ta>
            <ta e="T233" id="Seg_5232" s="T232">доезжать-PST2.[3SG]</ta>
            <ta e="T234" id="Seg_5233" s="T233">EMPH</ta>
            <ta e="T235" id="Seg_5234" s="T234">большой</ta>
            <ta e="T236" id="Seg_5235" s="T235">человек.[NOM]</ta>
            <ta e="T237" id="Seg_5236" s="T236">становиться-PST2.[3SG]</ta>
            <ta e="T238" id="Seg_5237" s="T237">старик.[NOM]</ta>
            <ta e="T239" id="Seg_5238" s="T238">выйти-EP-PST2-3SG</ta>
            <ta e="T240" id="Seg_5239" s="T239">видеть-PST2-3SG</ta>
            <ta e="T241" id="Seg_5240" s="T240">сын-3SG.[NOM]</ta>
            <ta e="T242" id="Seg_5241" s="T241">удаляться-PST2.[3SG]</ta>
            <ta e="T243" id="Seg_5242" s="T242">эй</ta>
            <ta e="T244" id="Seg_5243" s="T243">куда</ta>
            <ta e="T245" id="Seg_5244" s="T244">идти-PRS-2SG</ta>
            <ta e="T246" id="Seg_5245" s="T245">удаляться-EP-NEG.[IMP.2SG]</ta>
            <ta e="T247" id="Seg_5246" s="T246">говорить-PRS-1SG</ta>
            <ta e="T248" id="Seg_5247" s="T247">вода-DAT/LOC</ta>
            <ta e="T249" id="Seg_5248" s="T248">падать-PST1-2SG</ta>
            <ta e="T250" id="Seg_5249" s="T249">Лаайку.[NOM]</ta>
            <ta e="T251" id="Seg_5250" s="T250">река.[NOM]</ta>
            <ta e="T252" id="Seg_5251" s="T251">середина-3SG-DAT/LOC</ta>
            <ta e="T253" id="Seg_5252" s="T252">доезжать-CVB.SEQ</ta>
            <ta e="T254" id="Seg_5253" s="T253">после</ta>
            <ta e="T255" id="Seg_5254" s="T254">кричать-PST2.[3SG]</ta>
            <ta e="T256" id="Seg_5255" s="T255">ха-ха</ta>
            <ta e="T257" id="Seg_5256" s="T256">1SG.[NOM]</ta>
            <ta e="T258" id="Seg_5257" s="T257">Лаайку-1SG</ta>
            <ta e="T259" id="Seg_5258" s="T258">EVID</ta>
            <ta e="T260" id="Seg_5259" s="T259">2PL.[NOM]</ta>
            <ta e="T261" id="Seg_5260" s="T260">тот-ACC</ta>
            <ta e="T262" id="Seg_5261" s="T261">замечать-PST2.NEG-2PL</ta>
            <ta e="T263" id="Seg_5262" s="T262">Лаайку.[NOM]</ta>
            <ta e="T264" id="Seg_5263" s="T263">спасаться-PST2.[3SG]</ta>
            <ta e="T265" id="Seg_5264" s="T264">старик-PROPR</ta>
            <ta e="T266" id="Seg_5265" s="T265">старуха.[NOM]</ta>
            <ta e="T267" id="Seg_5266" s="T266">побить-CVB.SIM</ta>
            <ta e="T268" id="Seg_5267" s="T267">оставаться-PST2-3PL</ta>
            <ta e="T269" id="Seg_5268" s="T268">этот</ta>
            <ta e="T270" id="Seg_5269" s="T269">мальчик.[NOM]</ta>
            <ta e="T271" id="Seg_5270" s="T270">идти-CVB.SEQ</ta>
            <ta e="T272" id="Seg_5271" s="T271">идти-PST2.[3SG]</ta>
            <ta e="T273" id="Seg_5272" s="T272">сколько</ta>
            <ta e="T274" id="Seg_5273" s="T273">быть-FUT.[3SG]=Q</ta>
            <ta e="T275" id="Seg_5274" s="T274">видеть-PST2-3SG</ta>
            <ta e="T276" id="Seg_5275" s="T275">дом-PL.[NOM]</ta>
            <ta e="T277" id="Seg_5276" s="T276">стоять-PRS-3PL</ta>
            <ta e="T278" id="Seg_5277" s="T277">сани-PROPR</ta>
            <ta e="T279" id="Seg_5278" s="T278">олень-PL.[NOM]</ta>
            <ta e="T280" id="Seg_5279" s="T279">связывать-EP-PASS/REFL-CVB.SIM</ta>
            <ta e="T281" id="Seg_5280" s="T280">лежать-PRS-3PL</ta>
            <ta e="T282" id="Seg_5281" s="T281">один</ta>
            <ta e="T283" id="Seg_5282" s="T282">дом-DAT/LOC</ta>
            <ta e="T284" id="Seg_5283" s="T283">входить-PST2-3SG</ta>
            <ta e="T285" id="Seg_5284" s="T284">одинаковый</ta>
            <ta e="T286" id="Seg_5285" s="T285">жена.[NOM]</ta>
            <ta e="T287" id="Seg_5286" s="T286">сидеть-PRS.[3SG]</ta>
            <ta e="T288" id="Seg_5287" s="T287">Лаайку.[NOM]</ta>
            <ta e="T289" id="Seg_5288" s="T288">чай-VBZ-PST2.[3SG]</ta>
            <ta e="T290" id="Seg_5289" s="T289">этот</ta>
            <ta e="T291" id="Seg_5290" s="T290">что.[NOM]=Q</ta>
            <ta e="T292" id="Seg_5291" s="T291">сход.[NOM]</ta>
            <ta e="T293" id="Seg_5292" s="T292">Q</ta>
            <ta e="T294" id="Seg_5293" s="T293">свадьба.[NOM]</ta>
            <ta e="T295" id="Seg_5294" s="T294">Q</ta>
            <ta e="T296" id="Seg_5295" s="T295">нет</ta>
            <ta e="T297" id="Seg_5296" s="T296">1PL.[NOM]</ta>
            <ta e="T298" id="Seg_5297" s="T297">князь-1PL.[NOM]</ta>
            <ta e="T299" id="Seg_5298" s="T298">один</ta>
            <ta e="T300" id="Seg_5299" s="T299">дочь-PROPR.[NOM]</ta>
            <ta e="T301" id="Seg_5300" s="T300">тот</ta>
            <ta e="T302" id="Seg_5301" s="T301">дочь-DAT/LOC</ta>
            <ta e="T303" id="Seg_5302" s="T302">князь.[NOM]</ta>
            <ta e="T304" id="Seg_5303" s="T303">загадка-3SG-ACC</ta>
            <ta e="T305" id="Seg_5304" s="T304">догадываться-PTCP.PST</ta>
            <ta e="T306" id="Seg_5305" s="T305">человек.[NOM]</ta>
            <ta e="T307" id="Seg_5306" s="T306">только</ta>
            <ta e="T308" id="Seg_5307" s="T307">мужчина.[NOM]</ta>
            <ta e="T309" id="Seg_5308" s="T308">становиться-PRS.[3SG]</ta>
            <ta e="T310" id="Seg_5309" s="T309">говорить-PST2.[3SG]</ta>
            <ta e="T311" id="Seg_5310" s="T310">жена.[NOM]</ta>
            <ta e="T312" id="Seg_5311" s="T311">Лаайку.[NOM]</ta>
            <ta e="T313" id="Seg_5312" s="T312">выйти-EP-PST2.[3SG]</ta>
            <ta e="T314" id="Seg_5313" s="T313">да</ta>
            <ta e="T315" id="Seg_5314" s="T314">река.[NOM]</ta>
            <ta e="T316" id="Seg_5315" s="T315">край-3SG-DAT/LOC</ta>
            <ta e="T317" id="Seg_5316" s="T316">входить-PST2.[3SG]</ta>
            <ta e="T318" id="Seg_5317" s="T317">трава-EP-INSTR</ta>
            <ta e="T319" id="Seg_5318" s="T318">покрывать-REFL-CVB.SEQ</ta>
            <ta e="T320" id="Seg_5319" s="T319">после</ta>
            <ta e="T321" id="Seg_5320" s="T320">ложиться-PST2.[3SG]</ta>
            <ta e="T322" id="Seg_5321" s="T321">видеть-PST2-3SG</ta>
            <ta e="T323" id="Seg_5322" s="T322">два</ta>
            <ta e="T324" id="Seg_5323" s="T323">девушка.[NOM]</ta>
            <ta e="T325" id="Seg_5324" s="T324">вода.[NOM]</ta>
            <ta e="T326" id="Seg_5325" s="T325">черпать-CVB.SIM</ta>
            <ta e="T327" id="Seg_5326" s="T326">идти-PRS-3PL</ta>
            <ta e="T328" id="Seg_5327" s="T327">коромысло-VBZ-CVB.SEQ</ta>
            <ta e="T329" id="Seg_5328" s="T328">два</ta>
            <ta e="T330" id="Seg_5329" s="T329">бак-ACC</ta>
            <ta e="T331" id="Seg_5330" s="T330">плечо-3PL-DAT/LOC</ta>
            <ta e="T332" id="Seg_5331" s="T331">наложить.на.спину-PST2-3PL</ta>
            <ta e="T333" id="Seg_5332" s="T332">мальчик.[NOM]</ta>
            <ta e="T334" id="Seg_5333" s="T333">нога-3SG-ABL</ta>
            <ta e="T335" id="Seg_5334" s="T334">богатый</ta>
            <ta e="T336" id="Seg_5335" s="T335">девушка.[NOM]</ta>
            <ta e="T337" id="Seg_5336" s="T336">споткнуться-CVB.SEQ</ta>
            <ta e="T338" id="Seg_5337" s="T337">вода-3SG-ACC</ta>
            <ta e="T339" id="Seg_5338" s="T338">расплёскивать-PST2.[3SG]</ta>
            <ta e="T340" id="Seg_5339" s="T339">этот.[NOM]</ta>
            <ta e="T341" id="Seg_5340" s="T340">полностью</ta>
            <ta e="T342" id="Seg_5341" s="T341">отец-1SG.[NOM]</ta>
            <ta e="T343" id="Seg_5342" s="T342">из_за</ta>
            <ta e="T344" id="Seg_5343" s="T343">муж-DAT/LOC</ta>
            <ta e="T345" id="Seg_5344" s="T344">выйти-EP-PTCP.PST</ta>
            <ta e="T346" id="Seg_5345" s="T345">быть-PST2-EP-1SG</ta>
            <ta e="T347" id="Seg_5346" s="T346">быть-COND.[3SG]</ta>
            <ta e="T348" id="Seg_5347" s="T347">вода.[NOM]</ta>
            <ta e="T349" id="Seg_5348" s="T348">черпать-CVB.SIM</ta>
            <ta e="T350" id="Seg_5349" s="T349">устать-CVB.SIM</ta>
            <ta e="T351" id="Seg_5350" s="T350">идти-PTCP.FUT</ta>
            <ta e="T352" id="Seg_5351" s="T351">быть-PST1-1SG</ta>
            <ta e="T353" id="Seg_5352" s="T352">Q</ta>
            <ta e="T354" id="Seg_5353" s="T353">нога-1SG-ACC</ta>
            <ta e="T355" id="Seg_5354" s="T354">только</ta>
            <ta e="T356" id="Seg_5355" s="T355">облить-PST1-1SG</ta>
            <ta e="T357" id="Seg_5356" s="T356">друг</ta>
            <ta e="T358" id="Seg_5357" s="T357">тот.[NOM]</ta>
            <ta e="T359" id="Seg_5358" s="T358">отец-2SG.[NOM]</ta>
            <ta e="T360" id="Seg_5359" s="T359">что-ACC</ta>
            <ta e="T361" id="Seg_5360" s="T360">догадываться-CAUS-PRS.[3SG]</ta>
            <ta e="T362" id="Seg_5361" s="T361">отец-1SG.[NOM]</ta>
            <ta e="T363" id="Seg_5362" s="T362">когда</ta>
            <ta e="T364" id="Seg_5363" s="T363">INDEF</ta>
            <ta e="T365" id="Seg_5364" s="T364">семь</ta>
            <ta e="T366" id="Seg_5365" s="T365">вошь.[NOM]</ta>
            <ta e="T367" id="Seg_5366" s="T366">кожа-3SG-INSTR</ta>
            <ta e="T368" id="Seg_5367" s="T367">покрышка.для.вьюка.[NOM]</ta>
            <ta e="T369" id="Seg_5368" s="T368">делать-PTCP.PST</ta>
            <ta e="T370" id="Seg_5369" s="T369">быть-PST1-3SG</ta>
            <ta e="T371" id="Seg_5370" s="T370">тот-ACC</ta>
            <ta e="T372" id="Seg_5371" s="T371">делать-PTCP.PRS</ta>
            <ta e="T373" id="Seg_5372" s="T372">быть-PST2-3SG</ta>
            <ta e="T374" id="Seg_5373" s="T373">Q</ta>
            <ta e="T375" id="Seg_5374" s="T374">вот</ta>
            <ta e="T376" id="Seg_5375" s="T375">идти-IMP.1DU</ta>
            <ta e="T377" id="Seg_5376" s="T376">вода.[NOM]</ta>
            <ta e="T378" id="Seg_5377" s="T377">NEG</ta>
            <ta e="T379" id="Seg_5378" s="T378">выносить-NEG.[3SG]</ta>
            <ta e="T380" id="Seg_5379" s="T379">дерево.[NOM]</ta>
            <ta e="T381" id="Seg_5380" s="T380">NEG</ta>
            <ta e="T382" id="Seg_5381" s="T381">выносить-NEG.[3SG]</ta>
            <ta e="T383" id="Seg_5382" s="T382">столько-PROPR</ta>
            <ta e="T384" id="Seg_5383" s="T383">гость-DAT/LOC</ta>
            <ta e="T385" id="Seg_5384" s="T384">Лаайку.[NOM]</ta>
            <ta e="T386" id="Seg_5385" s="T385">возвращаться-CVB.SEQ</ta>
            <ta e="T387" id="Seg_5386" s="T386">приходить-CVB.SEQ</ta>
            <ta e="T388" id="Seg_5387" s="T387">этот</ta>
            <ta e="T389" id="Seg_5388" s="T388">дом-3SG-DAT/LOC</ta>
            <ta e="T390" id="Seg_5389" s="T389">входить-PST2.[3SG]</ta>
            <ta e="T391" id="Seg_5390" s="T390">ночевать-PST2.[3SG]</ta>
            <ta e="T392" id="Seg_5391" s="T391">утро.[NOM]</ta>
            <ta e="T393" id="Seg_5392" s="T392">вставать-PST2-3SG</ta>
            <ta e="T394" id="Seg_5393" s="T393">опять</ta>
            <ta e="T395" id="Seg_5394" s="T394">гость.[NOM]</ta>
            <ta e="T396" id="Seg_5395" s="T395">полный.[NOM]</ta>
            <ta e="T397" id="Seg_5396" s="T396">этот</ta>
            <ta e="T398" id="Seg_5397" s="T397">мальчик.[NOM]</ta>
            <ta e="T399" id="Seg_5398" s="T398">тоже</ta>
            <ta e="T400" id="Seg_5399" s="T399">князь-DAT/LOC</ta>
            <ta e="T401" id="Seg_5400" s="T400">идти-CAP.[3SG]</ta>
            <ta e="T402" id="Seg_5401" s="T401">гость-PL-ACC</ta>
            <ta e="T403" id="Seg_5402" s="T402">с</ta>
            <ta e="T404" id="Seg_5403" s="T403">вместе</ta>
            <ta e="T405" id="Seg_5404" s="T404">входить-PST2.[3SG]</ta>
            <ta e="T406" id="Seg_5405" s="T405">видеть-PST2-3SG</ta>
            <ta e="T407" id="Seg_5406" s="T406">дом-ACC</ta>
            <ta e="T408" id="Seg_5407" s="T407">полный</ta>
            <ta e="T409" id="Seg_5408" s="T408">мужчина.[NOM]</ta>
            <ta e="T410" id="Seg_5409" s="T409">человек.[NOM]</ta>
            <ta e="T411" id="Seg_5410" s="T410">о</ta>
            <ta e="T412" id="Seg_5411" s="T411">сегодня</ta>
            <ta e="T413" id="Seg_5412" s="T412">новый</ta>
            <ta e="T414" id="Seg_5413" s="T413">гость.[NOM]</ta>
            <ta e="T415" id="Seg_5414" s="T414">приходить-PST2.[3SG]</ta>
            <ta e="T416" id="Seg_5415" s="T415">вот</ta>
            <ta e="T417" id="Seg_5416" s="T416">можно</ta>
            <ta e="T418" id="Seg_5417" s="T417">2SG.[NOM]</ta>
            <ta e="T419" id="Seg_5418" s="T418">1SG.[NOM]</ta>
            <ta e="T420" id="Seg_5419" s="T419">загадка-1SG-ACC</ta>
            <ta e="T421" id="Seg_5420" s="T420">догадываться-FUT-2SG</ta>
            <ta e="T422" id="Seg_5421" s="T421">говорить-PST2.[3SG]</ta>
            <ta e="T423" id="Seg_5422" s="T422">князь.[NOM]</ta>
            <ta e="T424" id="Seg_5423" s="T423">что</ta>
            <ta e="T425" id="Seg_5424" s="T424">загадка-PROPR-2SG=Q</ta>
            <ta e="T426" id="Seg_5425" s="T425">сколько</ta>
            <ta e="T427" id="Seg_5426" s="T426">ум-EP-1SG.[NOM]</ta>
            <ta e="T428" id="Seg_5427" s="T427">выкладывать-EP-MED-CVB.SEQ</ta>
            <ta e="T429" id="Seg_5428" s="T428">догадываться-CVB.SIM</ta>
            <ta e="T430" id="Seg_5429" s="T429">мочь-PTCP.FUT</ta>
            <ta e="T431" id="Seg_5430" s="T430">быть-PST1-1SG</ta>
            <ta e="T432" id="Seg_5431" s="T431">Лаайку.[NOM]</ta>
            <ta e="T433" id="Seg_5432" s="T432">говорить-PST2.[3SG]</ta>
            <ta e="T434" id="Seg_5433" s="T433">1SG.[NOM]</ta>
            <ta e="T435" id="Seg_5434" s="T434">имя-VBZ-EP-CAUS-PRS-1SG</ta>
            <ta e="T436" id="Seg_5435" s="T435">покрышка.для.вьюка.[NOM]</ta>
            <ta e="T437" id="Seg_5436" s="T436">что-EP-INSTR</ta>
            <ta e="T438" id="Seg_5437" s="T437">делаться-EP-PTCP.PST.[NOM]=Q</ta>
            <ta e="T439" id="Seg_5438" s="T438">говорить-CVB.SEQ</ta>
            <ta e="T440" id="Seg_5439" s="T439">князь.[NOM]</ta>
            <ta e="T441" id="Seg_5440" s="T440">спрашивать-PST2.[3SG]</ta>
            <ta e="T442" id="Seg_5441" s="T441">если</ta>
            <ta e="T443" id="Seg_5442" s="T442">1SG.[NOM]</ta>
            <ta e="T444" id="Seg_5443" s="T443">ум-EP-1SG.[NOM]</ta>
            <ta e="T445" id="Seg_5444" s="T444">идти-PTCP.PRS</ta>
            <ta e="T446" id="Seg_5445" s="T445">дело-3SG-INSTR</ta>
            <ta e="T447" id="Seg_5446" s="T446">делать-FUT-2SG</ta>
            <ta e="T448" id="Seg_5447" s="T447">Q</ta>
            <ta e="T449" id="Seg_5448" s="T448">кожа.[NOM]</ta>
            <ta e="T450" id="Seg_5449" s="T449">быть-TEMP-3SG</ta>
            <ta e="T451" id="Seg_5450" s="T450">быть-IMP.3SG</ta>
            <ta e="T452" id="Seg_5451" s="T451">1SG.[NOM]</ta>
            <ta e="T453" id="Seg_5452" s="T452">мысль-1SG-DAT/LOC</ta>
            <ta e="T454" id="Seg_5453" s="T453">семь</ta>
            <ta e="T455" id="Seg_5454" s="T454">вошь.[NOM]</ta>
            <ta e="T456" id="Seg_5455" s="T455">кожа-3SG-INSTR</ta>
            <ta e="T457" id="Seg_5456" s="T456">делаться-EP-PTCP.PST.[NOM]</ta>
            <ta e="T458" id="Seg_5457" s="T457">наверное</ta>
            <ta e="T459" id="Seg_5458" s="T458">Лаайку.[NOM]</ta>
            <ta e="T460" id="Seg_5459" s="T459">говорить-PST2.[3SG]</ta>
            <ta e="T461" id="Seg_5460" s="T460">о</ta>
            <ta e="T462" id="Seg_5461" s="T461">что.[NOM]</ta>
            <ta e="T463" id="Seg_5462" s="T462">ум-PROPR</ta>
            <ta e="T464" id="Seg_5463" s="T463">человек-3SG-2SG=Q</ta>
            <ta e="T465" id="Seg_5464" s="T464">догадываться-PST1-2SG</ta>
            <ta e="T466" id="Seg_5465" s="T465">EMPH</ta>
            <ta e="T467" id="Seg_5466" s="T466">вот</ta>
            <ta e="T468" id="Seg_5467" s="T467">тогда</ta>
            <ta e="T469" id="Seg_5468" s="T468">1SG.[NOM]</ta>
            <ta e="T470" id="Seg_5469" s="T469">дочь-1SG-ACC</ta>
            <ta e="T471" id="Seg_5470" s="T470">жена.[NOM]</ta>
            <ta e="T472" id="Seg_5471" s="T471">делать-PTCP.PRS</ta>
            <ta e="T473" id="Seg_5472" s="T472">предназначение-PROPR-2SG</ta>
            <ta e="T474" id="Seg_5473" s="T473">быть-PST2.[3SG]</ta>
            <ta e="T475" id="Seg_5474" s="T474">как</ta>
            <ta e="T476" id="Seg_5475" s="T475">делать-FUT-2SG=Q</ta>
            <ta e="T477" id="Seg_5476" s="T476">завтра</ta>
            <ta e="T478" id="Seg_5477" s="T477">праздновать-FUT-1PL</ta>
            <ta e="T479" id="Seg_5478" s="T478">Лаайку.[NOM]</ta>
            <ta e="T480" id="Seg_5479" s="T479">богатый</ta>
            <ta e="T481" id="Seg_5480" s="T480">князь.[NOM]</ta>
            <ta e="T482" id="Seg_5481" s="T481">красивый</ta>
            <ta e="T483" id="Seg_5482" s="T482">дочь-3SG-ACC</ta>
            <ta e="T484" id="Seg_5483" s="T483">жена-VBZ-CVB.SEQ</ta>
            <ta e="T485" id="Seg_5484" s="T484">быть.богатым-CVB.SEQ-наесться-CVB.SEQ</ta>
            <ta e="T486" id="Seg_5485" s="T485">жить-PST2.[3SG]</ta>
            <ta e="T487" id="Seg_5486" s="T486">что.[NOM]</ta>
            <ta e="T488" id="Seg_5487" s="T487">NEG</ta>
            <ta e="T489" id="Seg_5488" s="T488">мука-ACC</ta>
            <ta e="T490" id="Seg_5489" s="T489">знать-NEG.CVB.SIM</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_5490" s="T0">adv</ta>
            <ta e="T2" id="Seg_5491" s="T1">v-v:ptcp</ta>
            <ta e="T3" id="Seg_5492" s="T2">n-n:poss-n:case-n-n:poss-n:case</ta>
            <ta e="T4" id="Seg_5493" s="T3">v-v:ptcp</ta>
            <ta e="T5" id="Seg_5494" s="T4">propr-n:case</ta>
            <ta e="T6" id="Seg_5495" s="T5">v-v:cvb</ta>
            <ta e="T7" id="Seg_5496" s="T6">n-n:case</ta>
            <ta e="T8" id="Seg_5497" s="T7">v-v:tense-v:pred.pn</ta>
            <ta e="T9" id="Seg_5498" s="T8">adv</ta>
            <ta e="T10" id="Seg_5499" s="T9">v-v:cvb</ta>
            <ta e="T11" id="Seg_5500" s="T10">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T12" id="Seg_5501" s="T11">n-n:case</ta>
            <ta e="T13" id="Seg_5502" s="T12">v-v:cvb</ta>
            <ta e="T14" id="Seg_5503" s="T13">dempro</ta>
            <ta e="T15" id="Seg_5504" s="T14">v-v:cvb</ta>
            <ta e="T16" id="Seg_5505" s="T15">v-v:tense-v:pred.pn</ta>
            <ta e="T17" id="Seg_5506" s="T16">adj</ta>
            <ta e="T18" id="Seg_5507" s="T17">n-n:case</ta>
            <ta e="T19" id="Seg_5508" s="T18">que-pro:case</ta>
            <ta e="T20" id="Seg_5509" s="T19">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T21" id="Seg_5510" s="T20">pers-pro:case</ta>
            <ta e="T22" id="Seg_5511" s="T21">adv</ta>
            <ta e="T23" id="Seg_5512" s="T22">v-v:mood-v:temp.pn</ta>
            <ta e="T24" id="Seg_5513" s="T23">v-v:cvb</ta>
            <ta e="T25" id="Seg_5514" s="T24">v-v:tense-v:pred.pn</ta>
            <ta e="T26" id="Seg_5515" s="T25">n-n:case</ta>
            <ta e="T27" id="Seg_5516" s="T26">n-n:poss-n:case</ta>
            <ta e="T28" id="Seg_5517" s="T27">v-v:cvb</ta>
            <ta e="T29" id="Seg_5518" s="T28">v-v:tense-v:poss.pn</ta>
            <ta e="T30" id="Seg_5519" s="T29">propr-n:case</ta>
            <ta e="T31" id="Seg_5520" s="T30">v-v:tense-v:pred.pn</ta>
            <ta e="T32" id="Seg_5521" s="T31">adj</ta>
            <ta e="T33" id="Seg_5522" s="T32">n-n:case</ta>
            <ta e="T34" id="Seg_5523" s="T33">n-n:poss-n:case</ta>
            <ta e="T35" id="Seg_5524" s="T34">n-n:case</ta>
            <ta e="T36" id="Seg_5525" s="T35">n-n:poss-n:case</ta>
            <ta e="T37" id="Seg_5526" s="T36">v-v:tense-v:pred.pn</ta>
            <ta e="T38" id="Seg_5527" s="T37">conj</ta>
            <ta e="T39" id="Seg_5528" s="T38">v-v&gt;n-n:case</ta>
            <ta e="T40" id="Seg_5529" s="T39">v-v:tense-v:pred.pn</ta>
            <ta e="T41" id="Seg_5530" s="T40">propr-n:case</ta>
            <ta e="T42" id="Seg_5531" s="T41">n-n:case</ta>
            <ta e="T43" id="Seg_5532" s="T42">ptcl</ta>
            <ta e="T44" id="Seg_5533" s="T43">v-v:tense-v:poss.pn</ta>
            <ta e="T45" id="Seg_5534" s="T44">v-v:tense-v:pred.pn</ta>
            <ta e="T46" id="Seg_5535" s="T45">n-n:case</ta>
            <ta e="T47" id="Seg_5536" s="T46">n-n:case</ta>
            <ta e="T48" id="Seg_5537" s="T47">v-v:cvb</ta>
            <ta e="T49" id="Seg_5538" s="T48">post</ta>
            <ta e="T50" id="Seg_5539" s="T49">n-n:case</ta>
            <ta e="T51" id="Seg_5540" s="T50">n-n:poss-n:case</ta>
            <ta e="T52" id="Seg_5541" s="T51">post</ta>
            <ta e="T53" id="Seg_5542" s="T52">v-v:cvb</ta>
            <ta e="T54" id="Seg_5543" s="T53">v-v:tense-v:pred.pn</ta>
            <ta e="T55" id="Seg_5544" s="T54">interj</ta>
            <ta e="T56" id="Seg_5545" s="T55">n-n:case</ta>
            <ta e="T57" id="Seg_5546" s="T56">conj</ta>
            <ta e="T58" id="Seg_5547" s="T57">que</ta>
            <ta e="T59" id="Seg_5548" s="T58">n-n&gt;v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T60" id="Seg_5549" s="T59">v-v:cvb</ta>
            <ta e="T61" id="Seg_5550" s="T60">v-v:tense-v:pred.pn-ptcl</ta>
            <ta e="T62" id="Seg_5551" s="T61">v-v:tense-v:pred.pn</ta>
            <ta e="T63" id="Seg_5552" s="T62">conj</ta>
            <ta e="T64" id="Seg_5553" s="T63">n-n:case</ta>
            <ta e="T65" id="Seg_5554" s="T64">v-v:cvb</ta>
            <ta e="T66" id="Seg_5555" s="T65">v-v:cvb</ta>
            <ta e="T67" id="Seg_5556" s="T66">post</ta>
            <ta e="T68" id="Seg_5557" s="T67">n-n:poss-n:case</ta>
            <ta e="T69" id="Seg_5558" s="T68">v-v:cvb</ta>
            <ta e="T70" id="Seg_5559" s="T69">v-v:tense-v:pred.pn</ta>
            <ta e="T71" id="Seg_5560" s="T70">propr-n:case</ta>
            <ta e="T72" id="Seg_5561" s="T71">n-n:poss-n:case</ta>
            <ta e="T73" id="Seg_5562" s="T72">post</ta>
            <ta e="T74" id="Seg_5563" s="T73">n-n:case</ta>
            <ta e="T75" id="Seg_5564" s="T74">n-n:poss-n:case</ta>
            <ta e="T76" id="Seg_5565" s="T75">v-v:ptcp</ta>
            <ta e="T77" id="Seg_5566" s="T76">v-v:tense-v:pred.pn</ta>
            <ta e="T78" id="Seg_5567" s="T77">n-n:(poss)</ta>
            <ta e="T79" id="Seg_5568" s="T78">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T80" id="Seg_5569" s="T79">v-v:tense-v:pred.pn</ta>
            <ta e="T81" id="Seg_5570" s="T80">adj</ta>
            <ta e="T82" id="Seg_5571" s="T81">adj</ta>
            <ta e="T83" id="Seg_5572" s="T82">n-n&gt;adj-n:(pred.pn)</ta>
            <ta e="T84" id="Seg_5573" s="T83">n-n:(poss)-n:case</ta>
            <ta e="T85" id="Seg_5574" s="T84">adv</ta>
            <ta e="T86" id="Seg_5575" s="T85">v-v:cvb</ta>
            <ta e="T87" id="Seg_5576" s="T86">v-v:tense-v:pred.pn</ta>
            <ta e="T88" id="Seg_5577" s="T87">n-n&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T89" id="Seg_5578" s="T88">n-n:(poss)-n:case</ta>
            <ta e="T90" id="Seg_5579" s="T89">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T91" id="Seg_5580" s="T90">n-n:case</ta>
            <ta e="T92" id="Seg_5581" s="T91">n-n:case</ta>
            <ta e="T93" id="Seg_5582" s="T92">v-v:tense-v:pred.pn</ta>
            <ta e="T94" id="Seg_5583" s="T93">dempro</ta>
            <ta e="T95" id="Seg_5584" s="T94">n-n:case</ta>
            <ta e="T96" id="Seg_5585" s="T95">v-v:cvb</ta>
            <ta e="T97" id="Seg_5586" s="T96">v-v:ptcp</ta>
            <ta e="T98" id="Seg_5587" s="T97">v-v:tense-v:pred.pn</ta>
            <ta e="T99" id="Seg_5588" s="T98">n-n:poss-n:case</ta>
            <ta e="T100" id="Seg_5589" s="T99">n-n:case</ta>
            <ta e="T101" id="Seg_5590" s="T100">n-n:case</ta>
            <ta e="T102" id="Seg_5591" s="T101">n-n:case</ta>
            <ta e="T103" id="Seg_5592" s="T102">adj</ta>
            <ta e="T104" id="Seg_5593" s="T103">n-n&gt;v-v:ptcp</ta>
            <ta e="T105" id="Seg_5594" s="T104">n-n&gt;adj-n:case</ta>
            <ta e="T106" id="Seg_5595" s="T105">v-v:tense-v:pred.pn</ta>
            <ta e="T107" id="Seg_5596" s="T106">dempro</ta>
            <ta e="T108" id="Seg_5597" s="T107">n-n:case</ta>
            <ta e="T109" id="Seg_5598" s="T108">n-n:(ins)-n:case</ta>
            <ta e="T110" id="Seg_5599" s="T109">ptcl</ta>
            <ta e="T111" id="Seg_5600" s="T110">v-v:(ins)-v&gt;v-v:ptcp</ta>
            <ta e="T112" id="Seg_5601" s="T111">v-v:tense-v:pred.pn</ta>
            <ta e="T113" id="Seg_5602" s="T112">propr-n:case</ta>
            <ta e="T114" id="Seg_5603" s="T113">adv</ta>
            <ta e="T115" id="Seg_5604" s="T114">v-v:tense-v:pred.pn</ta>
            <ta e="T116" id="Seg_5605" s="T115">n-n:case</ta>
            <ta e="T117" id="Seg_5606" s="T116">pers-pro:case</ta>
            <ta e="T118" id="Seg_5607" s="T117">n-n:case</ta>
            <ta e="T119" id="Seg_5608" s="T118">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T120" id="Seg_5609" s="T119">v-v:tense-v:pred.pn</ta>
            <ta e="T121" id="Seg_5610" s="T120">n-n:case</ta>
            <ta e="T122" id="Seg_5611" s="T121">ptcl</ta>
            <ta e="T123" id="Seg_5612" s="T122">v-v:mood.pn</ta>
            <ta e="T124" id="Seg_5613" s="T123">n-n:poss-n:case</ta>
            <ta e="T125" id="Seg_5614" s="T124">n-n:case</ta>
            <ta e="T126" id="Seg_5615" s="T125">n-n:case</ta>
            <ta e="T127" id="Seg_5616" s="T126">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T128" id="Seg_5617" s="T127">v-v:tense-v:pred.pn</ta>
            <ta e="T129" id="Seg_5618" s="T128">n-n:case</ta>
            <ta e="T130" id="Seg_5619" s="T129">n-n:poss-n:case</ta>
            <ta e="T131" id="Seg_5620" s="T130">v-v:cvb</ta>
            <ta e="T132" id="Seg_5621" s="T131">v-v:tense-v:pred.pn</ta>
            <ta e="T133" id="Seg_5622" s="T132">propr-n:case</ta>
            <ta e="T134" id="Seg_5623" s="T133">n-n:case</ta>
            <ta e="T135" id="Seg_5624" s="T134">n-n:(poss)-n:case</ta>
            <ta e="T136" id="Seg_5625" s="T135">v-v:cvb</ta>
            <ta e="T137" id="Seg_5626" s="T136">v-v:tense-v:pred.pn</ta>
            <ta e="T138" id="Seg_5627" s="T137">n-n:case</ta>
            <ta e="T139" id="Seg_5628" s="T138">que</ta>
            <ta e="T140" id="Seg_5629" s="T139">n-n:case</ta>
            <ta e="T141" id="Seg_5630" s="T140">adv</ta>
            <ta e="T142" id="Seg_5631" s="T141">v-v:(ins)-v&gt;v-v:(neg)-v:pred.pn</ta>
            <ta e="T143" id="Seg_5632" s="T142">adv</ta>
            <ta e="T144" id="Seg_5633" s="T143">n-n:case</ta>
            <ta e="T145" id="Seg_5634" s="T144">ptcl</ta>
            <ta e="T146" id="Seg_5635" s="T145">adv</ta>
            <ta e="T147" id="Seg_5636" s="T146">v-v:tense-v:pred.pn</ta>
            <ta e="T148" id="Seg_5637" s="T147">interj</ta>
            <ta e="T149" id="Seg_5638" s="T148">n-n:case</ta>
            <ta e="T150" id="Seg_5639" s="T149">v-v:tense-v:pred.pn</ta>
            <ta e="T151" id="Seg_5640" s="T150">ptcl</ta>
            <ta e="T152" id="Seg_5641" s="T151">n-n:case</ta>
            <ta e="T153" id="Seg_5642" s="T152">n-n:poss-n:case</ta>
            <ta e="T154" id="Seg_5643" s="T153">n-n:case</ta>
            <ta e="T155" id="Seg_5644" s="T154">v-v:tense-v:pred.pn</ta>
            <ta e="T156" id="Seg_5645" s="T155">que-pro:case</ta>
            <ta e="T157" id="Seg_5646" s="T156">post</ta>
            <ta e="T158" id="Seg_5647" s="T157">cardnum</ta>
            <ta e="T159" id="Seg_5648" s="T158">n-n:case</ta>
            <ta e="T160" id="Seg_5649" s="T159">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T161" id="Seg_5650" s="T160">v-v:mood.pn</ta>
            <ta e="T162" id="Seg_5651" s="T161">n-n:case</ta>
            <ta e="T163" id="Seg_5652" s="T162">v-v:cvb</ta>
            <ta e="T164" id="Seg_5653" s="T163">n-n:(num)-n:poss-n:case</ta>
            <ta e="T165" id="Seg_5654" s="T164">adv</ta>
            <ta e="T166" id="Seg_5655" s="T165">v-v:cvb</ta>
            <ta e="T167" id="Seg_5656" s="T166">v-v:tense-v:pred.pn</ta>
            <ta e="T168" id="Seg_5657" s="T167">n-n:case</ta>
            <ta e="T169" id="Seg_5658" s="T168">propr-n:case</ta>
            <ta e="T170" id="Seg_5659" s="T169">v-v:cvb-v-v:cvb</ta>
            <ta e="T171" id="Seg_5660" s="T170">v-v:cvb</ta>
            <ta e="T172" id="Seg_5661" s="T171">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T173" id="Seg_5662" s="T172">adv-adv&gt;adv</ta>
            <ta e="T174" id="Seg_5663" s="T173">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T175" id="Seg_5664" s="T174">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T176" id="Seg_5665" s="T175">post</ta>
            <ta e="T177" id="Seg_5666" s="T176">dempro</ta>
            <ta e="T178" id="Seg_5667" s="T177">n-n:case</ta>
            <ta e="T179" id="Seg_5668" s="T178">n-n:poss-n:case</ta>
            <ta e="T180" id="Seg_5669" s="T179">v-v:cvb</ta>
            <ta e="T181" id="Seg_5670" s="T180">n-n:poss-n:case</ta>
            <ta e="T182" id="Seg_5671" s="T181">v-v:tense-v:pred.pn</ta>
            <ta e="T183" id="Seg_5672" s="T182">n-n:case</ta>
            <ta e="T184" id="Seg_5673" s="T183">pers-pro:case</ta>
            <ta e="T185" id="Seg_5674" s="T184">n-n&gt;v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T186" id="Seg_5675" s="T185">v-v:tense-v:pred.pn</ta>
            <ta e="T187" id="Seg_5676" s="T186">dempro-pro:case</ta>
            <ta e="T188" id="Seg_5677" s="T187">que-ptcl</ta>
            <ta e="T189" id="Seg_5678" s="T188">ptcl</ta>
            <ta e="T190" id="Seg_5679" s="T189">n-n:case</ta>
            <ta e="T191" id="Seg_5680" s="T190">v-v:tense-v:pred.pn</ta>
            <ta e="T192" id="Seg_5681" s="T191">v-v:cvb</ta>
            <ta e="T193" id="Seg_5682" s="T192">v-v:cvb</ta>
            <ta e="T194" id="Seg_5683" s="T193">post</ta>
            <ta e="T195" id="Seg_5684" s="T194">ptcl</ta>
            <ta e="T196" id="Seg_5685" s="T195">ptcl</ta>
            <ta e="T197" id="Seg_5686" s="T196">n-n&gt;v-v:mood.pn</ta>
            <ta e="T198" id="Seg_5687" s="T197">adj</ta>
            <ta e="T199" id="Seg_5688" s="T198">ptcl</ta>
            <ta e="T200" id="Seg_5689" s="T199">v-v:(ins)-v:(neg)-v:mood.pn</ta>
            <ta e="T201" id="Seg_5690" s="T200">ptcl</ta>
            <ta e="T202" id="Seg_5691" s="T201">ptcl</ta>
            <ta e="T203" id="Seg_5692" s="T202">v-v:tense-v:poss.pn</ta>
            <ta e="T204" id="Seg_5693" s="T203">ptcl-ptcl:(poss.pn)</ta>
            <ta e="T205" id="Seg_5694" s="T204">n-n:case</ta>
            <ta e="T206" id="Seg_5695" s="T205">n-n:poss-n:case</ta>
            <ta e="T207" id="Seg_5696" s="T206">ptcl</ta>
            <ta e="T208" id="Seg_5697" s="T207">v-v:tense-v:poss.pn</ta>
            <ta e="T209" id="Seg_5698" s="T208">v-v:tense-v:pred.pn</ta>
            <ta e="T210" id="Seg_5699" s="T209">propr-n:case</ta>
            <ta e="T211" id="Seg_5700" s="T210">n-n:case</ta>
            <ta e="T212" id="Seg_5701" s="T211">propr-n:case</ta>
            <ta e="T213" id="Seg_5702" s="T212">n-n:case</ta>
            <ta e="T214" id="Seg_5703" s="T213">v-v:tense-v:pred.pn</ta>
            <ta e="T215" id="Seg_5704" s="T214">adv</ta>
            <ta e="T216" id="Seg_5705" s="T215">n-n:case</ta>
            <ta e="T217" id="Seg_5706" s="T216">post</ta>
            <ta e="T218" id="Seg_5707" s="T217">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T219" id="Seg_5708" s="T218">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T220" id="Seg_5709" s="T219">n-n&gt;adj</ta>
            <ta e="T221" id="Seg_5710" s="T220">n-n:case</ta>
            <ta e="T222" id="Seg_5711" s="T221">n-n:case</ta>
            <ta e="T223" id="Seg_5712" s="T222">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T224" id="Seg_5713" s="T223">post</ta>
            <ta e="T225" id="Seg_5714" s="T224">v-v:cvb</ta>
            <ta e="T226" id="Seg_5715" s="T225">conj</ta>
            <ta e="T227" id="Seg_5716" s="T226">v-v:cvb</ta>
            <ta e="T228" id="Seg_5717" s="T227">v-v:tense-v:pred.pn</ta>
            <ta e="T229" id="Seg_5718" s="T228">dempro</ta>
            <ta e="T230" id="Seg_5719" s="T229">n-n:case</ta>
            <ta e="T231" id="Seg_5720" s="T230">n-n:case</ta>
            <ta e="T232" id="Seg_5721" s="T231">n-n:poss-n:case</ta>
            <ta e="T233" id="Seg_5722" s="T232">v-v:tense-v:pred.pn</ta>
            <ta e="T234" id="Seg_5723" s="T233">ptcl</ta>
            <ta e="T235" id="Seg_5724" s="T234">adj</ta>
            <ta e="T236" id="Seg_5725" s="T235">n-n:case</ta>
            <ta e="T237" id="Seg_5726" s="T236">v-v:tense-v:pred.pn</ta>
            <ta e="T238" id="Seg_5727" s="T237">n-n:case</ta>
            <ta e="T239" id="Seg_5728" s="T238">v-v:(ins)-v:tense-v:poss.pn</ta>
            <ta e="T240" id="Seg_5729" s="T239">v-v:tense-v:poss.pn</ta>
            <ta e="T241" id="Seg_5730" s="T240">n-n:(poss)-n:case</ta>
            <ta e="T242" id="Seg_5731" s="T241">v-v:tense-v:pred.pn</ta>
            <ta e="T243" id="Seg_5732" s="T242">interj</ta>
            <ta e="T244" id="Seg_5733" s="T243">que</ta>
            <ta e="T245" id="Seg_5734" s="T244">v-v:tense-v:pred.pn</ta>
            <ta e="T246" id="Seg_5735" s="T245">v-v:(ins)-v:(neg)-v:mood.pn</ta>
            <ta e="T247" id="Seg_5736" s="T246">v-v:tense-v:pred.pn</ta>
            <ta e="T248" id="Seg_5737" s="T247">n-n:case</ta>
            <ta e="T249" id="Seg_5738" s="T248">v-v:tense-v:poss.pn</ta>
            <ta e="T250" id="Seg_5739" s="T249">propr-n:case</ta>
            <ta e="T251" id="Seg_5740" s="T250">n-n:case</ta>
            <ta e="T252" id="Seg_5741" s="T251">n-n:poss-n:case</ta>
            <ta e="T253" id="Seg_5742" s="T252">v-v:cvb</ta>
            <ta e="T254" id="Seg_5743" s="T253">post</ta>
            <ta e="T255" id="Seg_5744" s="T254">v-v:tense-v:pred.pn</ta>
            <ta e="T256" id="Seg_5745" s="T255">interj</ta>
            <ta e="T257" id="Seg_5746" s="T256">pers-pro:case</ta>
            <ta e="T258" id="Seg_5747" s="T257">propr-n:(pred.pn)</ta>
            <ta e="T259" id="Seg_5748" s="T258">ptcl</ta>
            <ta e="T260" id="Seg_5749" s="T259">pers-pro:case</ta>
            <ta e="T261" id="Seg_5750" s="T260">dempro-pro:case</ta>
            <ta e="T262" id="Seg_5751" s="T261">v-v:neg-v:pred.pn</ta>
            <ta e="T263" id="Seg_5752" s="T262">propr-n:case</ta>
            <ta e="T264" id="Seg_5753" s="T263">v-v:tense-v:pred.pn</ta>
            <ta e="T265" id="Seg_5754" s="T264">n-n&gt;adj</ta>
            <ta e="T266" id="Seg_5755" s="T265">n-n:case</ta>
            <ta e="T267" id="Seg_5756" s="T266">v-v:cvb</ta>
            <ta e="T268" id="Seg_5757" s="T267">v-v:tense-v:pred.pn</ta>
            <ta e="T269" id="Seg_5758" s="T268">dempro</ta>
            <ta e="T270" id="Seg_5759" s="T269">n-n:case</ta>
            <ta e="T271" id="Seg_5760" s="T270">v-v:cvb</ta>
            <ta e="T272" id="Seg_5761" s="T271">v-v:tense-v:pred.pn</ta>
            <ta e="T273" id="Seg_5762" s="T272">que</ta>
            <ta e="T274" id="Seg_5763" s="T273">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T275" id="Seg_5764" s="T274">v-v:tense-v:poss.pn</ta>
            <ta e="T276" id="Seg_5765" s="T275">n-n:(num)-n:case</ta>
            <ta e="T277" id="Seg_5766" s="T276">v-v:tense-v:pred.pn</ta>
            <ta e="T278" id="Seg_5767" s="T277">n-n&gt;adj</ta>
            <ta e="T279" id="Seg_5768" s="T278">n-n:(num)-n:case</ta>
            <ta e="T280" id="Seg_5769" s="T279">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T281" id="Seg_5770" s="T280">v-v:tense-v:pred.pn</ta>
            <ta e="T282" id="Seg_5771" s="T281">cardnum</ta>
            <ta e="T283" id="Seg_5772" s="T282">n-n:case</ta>
            <ta e="T284" id="Seg_5773" s="T283">v-v:tense-v:poss.pn</ta>
            <ta e="T285" id="Seg_5774" s="T284">adj</ta>
            <ta e="T286" id="Seg_5775" s="T285">n-n:case</ta>
            <ta e="T287" id="Seg_5776" s="T286">v-v:tense-v:pred.pn</ta>
            <ta e="T288" id="Seg_5777" s="T287">propr-n:case</ta>
            <ta e="T289" id="Seg_5778" s="T288">n-n&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T290" id="Seg_5779" s="T289">dempro</ta>
            <ta e="T291" id="Seg_5780" s="T290">que-pro:case-ptcl</ta>
            <ta e="T292" id="Seg_5781" s="T291">n-n:case</ta>
            <ta e="T293" id="Seg_5782" s="T292">ptcl</ta>
            <ta e="T294" id="Seg_5783" s="T293">n-n:case</ta>
            <ta e="T295" id="Seg_5784" s="T294">ptcl</ta>
            <ta e="T296" id="Seg_5785" s="T295">ptcl</ta>
            <ta e="T297" id="Seg_5786" s="T296">pers-pro:case</ta>
            <ta e="T298" id="Seg_5787" s="T297">n-n:(poss)-n:case</ta>
            <ta e="T299" id="Seg_5788" s="T298">cardnum</ta>
            <ta e="T300" id="Seg_5789" s="T299">n-n&gt;adj-n:case</ta>
            <ta e="T301" id="Seg_5790" s="T300">dempro</ta>
            <ta e="T302" id="Seg_5791" s="T301">n-n:case</ta>
            <ta e="T303" id="Seg_5792" s="T302">n-n:case</ta>
            <ta e="T304" id="Seg_5793" s="T303">n-n:poss-n:case</ta>
            <ta e="T305" id="Seg_5794" s="T304">v-v:ptcp</ta>
            <ta e="T306" id="Seg_5795" s="T305">n-n:case</ta>
            <ta e="T307" id="Seg_5796" s="T306">ptcl</ta>
            <ta e="T308" id="Seg_5797" s="T307">n-n:case</ta>
            <ta e="T309" id="Seg_5798" s="T308">v-v:tense-v:pred.pn</ta>
            <ta e="T310" id="Seg_5799" s="T309">v-v:tense-v:pred.pn</ta>
            <ta e="T311" id="Seg_5800" s="T310">n-n:case</ta>
            <ta e="T312" id="Seg_5801" s="T311">propr-n:case</ta>
            <ta e="T313" id="Seg_5802" s="T312">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T314" id="Seg_5803" s="T313">conj</ta>
            <ta e="T315" id="Seg_5804" s="T314">n-n:case</ta>
            <ta e="T316" id="Seg_5805" s="T315">n-n:poss-n:case</ta>
            <ta e="T317" id="Seg_5806" s="T316">v-v:tense-v:pred.pn</ta>
            <ta e="T318" id="Seg_5807" s="T317">n-n:(ins)-n:case</ta>
            <ta e="T319" id="Seg_5808" s="T318">v-v&gt;v-v:cvb</ta>
            <ta e="T320" id="Seg_5809" s="T319">post</ta>
            <ta e="T321" id="Seg_5810" s="T320">v-v:tense-v:pred.pn</ta>
            <ta e="T322" id="Seg_5811" s="T321">v-v:tense-v:poss.pn</ta>
            <ta e="T323" id="Seg_5812" s="T322">cardnum</ta>
            <ta e="T324" id="Seg_5813" s="T323">n-n:case</ta>
            <ta e="T325" id="Seg_5814" s="T324">n-n:case</ta>
            <ta e="T326" id="Seg_5815" s="T325">v-v:cvb</ta>
            <ta e="T327" id="Seg_5816" s="T326">v-v:tense-v:pred.pn</ta>
            <ta e="T328" id="Seg_5817" s="T327">n-n&gt;v-v:cvb</ta>
            <ta e="T329" id="Seg_5818" s="T328">cardnum</ta>
            <ta e="T330" id="Seg_5819" s="T329">n-n:case</ta>
            <ta e="T331" id="Seg_5820" s="T330">n-n:poss-n:case</ta>
            <ta e="T332" id="Seg_5821" s="T331">v-v:tense-v:pred.pn</ta>
            <ta e="T333" id="Seg_5822" s="T332">n-n:case</ta>
            <ta e="T334" id="Seg_5823" s="T333">n-n:poss-n:case</ta>
            <ta e="T335" id="Seg_5824" s="T334">adj</ta>
            <ta e="T336" id="Seg_5825" s="T335">n-n:case</ta>
            <ta e="T337" id="Seg_5826" s="T336">v-v:cvb</ta>
            <ta e="T338" id="Seg_5827" s="T337">n-n:poss-n:case</ta>
            <ta e="T339" id="Seg_5828" s="T338">v-v:tense-v:pred.pn</ta>
            <ta e="T340" id="Seg_5829" s="T339">dempro-pro:case</ta>
            <ta e="T341" id="Seg_5830" s="T340">adv</ta>
            <ta e="T342" id="Seg_5831" s="T341">n-n:(poss)-n:case</ta>
            <ta e="T343" id="Seg_5832" s="T342">post</ta>
            <ta e="T344" id="Seg_5833" s="T343">n-n:case</ta>
            <ta e="T345" id="Seg_5834" s="T344">v-v:(ins)-v:ptcp</ta>
            <ta e="T346" id="Seg_5835" s="T345">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T347" id="Seg_5836" s="T346">v-v:mood-v:pred.pn</ta>
            <ta e="T348" id="Seg_5837" s="T347">n-n:case</ta>
            <ta e="T349" id="Seg_5838" s="T348">v-v:cvb</ta>
            <ta e="T350" id="Seg_5839" s="T349">v-v:cvb</ta>
            <ta e="T351" id="Seg_5840" s="T350">v-v:ptcp</ta>
            <ta e="T352" id="Seg_5841" s="T351">v-v:tense-v:poss.pn</ta>
            <ta e="T353" id="Seg_5842" s="T352">ptcl</ta>
            <ta e="T354" id="Seg_5843" s="T353">n-n:poss-n:case</ta>
            <ta e="T355" id="Seg_5844" s="T354">ptcl</ta>
            <ta e="T356" id="Seg_5845" s="T355">v-v:tense-v:poss.pn</ta>
            <ta e="T357" id="Seg_5846" s="T356">n</ta>
            <ta e="T358" id="Seg_5847" s="T357">dempro-pro:case</ta>
            <ta e="T359" id="Seg_5848" s="T358">n-n:(poss)-n:case</ta>
            <ta e="T360" id="Seg_5849" s="T359">que-pro:case</ta>
            <ta e="T361" id="Seg_5850" s="T360">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T362" id="Seg_5851" s="T361">n-n:(poss)-n:case</ta>
            <ta e="T363" id="Seg_5852" s="T362">que</ta>
            <ta e="T364" id="Seg_5853" s="T363">ptcl</ta>
            <ta e="T365" id="Seg_5854" s="T364">cardnum</ta>
            <ta e="T366" id="Seg_5855" s="T365">n-n:case</ta>
            <ta e="T367" id="Seg_5856" s="T366">n-n:poss-n:case</ta>
            <ta e="T368" id="Seg_5857" s="T367">n-n:case</ta>
            <ta e="T369" id="Seg_5858" s="T368">v-v:ptcp</ta>
            <ta e="T370" id="Seg_5859" s="T369">v-v:tense-v:poss.pn</ta>
            <ta e="T371" id="Seg_5860" s="T370">dempro-pro:case</ta>
            <ta e="T372" id="Seg_5861" s="T371">v-v:ptcp</ta>
            <ta e="T373" id="Seg_5862" s="T372">v-v:tense-v:poss.pn</ta>
            <ta e="T374" id="Seg_5863" s="T373">ptcl</ta>
            <ta e="T375" id="Seg_5864" s="T374">ptcl</ta>
            <ta e="T376" id="Seg_5865" s="T375">v-v:mood.pn</ta>
            <ta e="T377" id="Seg_5866" s="T376">n-n:case</ta>
            <ta e="T378" id="Seg_5867" s="T377">ptcl</ta>
            <ta e="T379" id="Seg_5868" s="T378">v-v:(neg)-v:pred.pn</ta>
            <ta e="T380" id="Seg_5869" s="T379">n-n:case</ta>
            <ta e="T381" id="Seg_5870" s="T380">ptcl</ta>
            <ta e="T382" id="Seg_5871" s="T381">v-v:(neg)-v:pred.pn</ta>
            <ta e="T383" id="Seg_5872" s="T382">adv-adv&gt;adj</ta>
            <ta e="T384" id="Seg_5873" s="T383">n-n:case</ta>
            <ta e="T385" id="Seg_5874" s="T384">propr-n:case</ta>
            <ta e="T386" id="Seg_5875" s="T385">v-v:cvb</ta>
            <ta e="T387" id="Seg_5876" s="T386">v-v:cvb</ta>
            <ta e="T388" id="Seg_5877" s="T387">dempro</ta>
            <ta e="T389" id="Seg_5878" s="T388">n-n:poss-n:case</ta>
            <ta e="T390" id="Seg_5879" s="T389">v-v:tense-v:pred.pn</ta>
            <ta e="T391" id="Seg_5880" s="T390">v-v:tense-v:pred.pn</ta>
            <ta e="T392" id="Seg_5881" s="T391">n-n:case</ta>
            <ta e="T393" id="Seg_5882" s="T392">v-v:tense-v:poss.pn</ta>
            <ta e="T394" id="Seg_5883" s="T393">ptcl</ta>
            <ta e="T395" id="Seg_5884" s="T394">n-n:case</ta>
            <ta e="T396" id="Seg_5885" s="T395">adj-n:case</ta>
            <ta e="T397" id="Seg_5886" s="T396">dempro</ta>
            <ta e="T398" id="Seg_5887" s="T397">n-n:case</ta>
            <ta e="T399" id="Seg_5888" s="T398">ptcl</ta>
            <ta e="T400" id="Seg_5889" s="T399">n-n:case</ta>
            <ta e="T401" id="Seg_5890" s="T400">v-v:mood-v:pred.pn</ta>
            <ta e="T402" id="Seg_5891" s="T401">n-n:(num)-n:case</ta>
            <ta e="T403" id="Seg_5892" s="T402">post</ta>
            <ta e="T404" id="Seg_5893" s="T403">adv</ta>
            <ta e="T405" id="Seg_5894" s="T404">v-v:tense-v:pred.pn</ta>
            <ta e="T406" id="Seg_5895" s="T405">v-v:tense-v:poss.pn</ta>
            <ta e="T407" id="Seg_5896" s="T406">n-n:case</ta>
            <ta e="T408" id="Seg_5897" s="T407">adj</ta>
            <ta e="T409" id="Seg_5898" s="T408">n-n:case</ta>
            <ta e="T410" id="Seg_5899" s="T409">n-n:case</ta>
            <ta e="T411" id="Seg_5900" s="T410">interj</ta>
            <ta e="T412" id="Seg_5901" s="T411">adv</ta>
            <ta e="T413" id="Seg_5902" s="T412">adj</ta>
            <ta e="T414" id="Seg_5903" s="T413">n-n:case</ta>
            <ta e="T415" id="Seg_5904" s="T414">v-v:tense-v:pred.pn</ta>
            <ta e="T416" id="Seg_5905" s="T415">ptcl</ta>
            <ta e="T417" id="Seg_5906" s="T416">adv</ta>
            <ta e="T418" id="Seg_5907" s="T417">pers-pro:case</ta>
            <ta e="T419" id="Seg_5908" s="T418">pers-pro:case</ta>
            <ta e="T420" id="Seg_5909" s="T419">n-n:poss-n:case</ta>
            <ta e="T421" id="Seg_5910" s="T420">v-v:tense-v:poss.pn</ta>
            <ta e="T422" id="Seg_5911" s="T421">v-v:tense-v:pred.pn</ta>
            <ta e="T423" id="Seg_5912" s="T422">n-n:case</ta>
            <ta e="T424" id="Seg_5913" s="T423">que</ta>
            <ta e="T425" id="Seg_5914" s="T424">n-n&gt;adj-n:(pred.pn)-ptcl</ta>
            <ta e="T426" id="Seg_5915" s="T425">que</ta>
            <ta e="T427" id="Seg_5916" s="T426">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T428" id="Seg_5917" s="T427">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T429" id="Seg_5918" s="T428">v-v:cvb</ta>
            <ta e="T430" id="Seg_5919" s="T429">v-v:ptcp</ta>
            <ta e="T431" id="Seg_5920" s="T430">v-v:tense-v:poss.pn</ta>
            <ta e="T432" id="Seg_5921" s="T431">propr-n:case</ta>
            <ta e="T433" id="Seg_5922" s="T432">v-v:tense-v:pred.pn</ta>
            <ta e="T434" id="Seg_5923" s="T433">pers-pro:case</ta>
            <ta e="T435" id="Seg_5924" s="T434">n-n&gt;v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T436" id="Seg_5925" s="T435">n-n:case</ta>
            <ta e="T437" id="Seg_5926" s="T436">que-pro:(ins)-pro:case</ta>
            <ta e="T438" id="Seg_5927" s="T437">v-v:(ins)-v:ptcp-v:(case)-ptcl</ta>
            <ta e="T439" id="Seg_5928" s="T438">v-v:cvb</ta>
            <ta e="T440" id="Seg_5929" s="T439">n-n:case</ta>
            <ta e="T441" id="Seg_5930" s="T440">v-v:tense-v:pred.pn</ta>
            <ta e="T442" id="Seg_5931" s="T441">conj</ta>
            <ta e="T443" id="Seg_5932" s="T442">pers-pro:case</ta>
            <ta e="T444" id="Seg_5933" s="T443">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T445" id="Seg_5934" s="T444">v-v:ptcp</ta>
            <ta e="T446" id="Seg_5935" s="T445">n-n:poss-n:case</ta>
            <ta e="T447" id="Seg_5936" s="T446">v-v:tense-v:poss.pn</ta>
            <ta e="T448" id="Seg_5937" s="T447">ptcl</ta>
            <ta e="T449" id="Seg_5938" s="T448">n-n:case</ta>
            <ta e="T450" id="Seg_5939" s="T449">v-v:mood-v:temp.pn</ta>
            <ta e="T451" id="Seg_5940" s="T450">v-v:mood.pn</ta>
            <ta e="T452" id="Seg_5941" s="T451">pers-pro:case</ta>
            <ta e="T453" id="Seg_5942" s="T452">n-n:poss-n:case</ta>
            <ta e="T454" id="Seg_5943" s="T453">cardnum</ta>
            <ta e="T455" id="Seg_5944" s="T454">n-n:case</ta>
            <ta e="T456" id="Seg_5945" s="T455">n-n:poss-n:case</ta>
            <ta e="T457" id="Seg_5946" s="T456">v-v:(ins)-v:ptcp-v:(case)</ta>
            <ta e="T458" id="Seg_5947" s="T457">adv</ta>
            <ta e="T459" id="Seg_5948" s="T458">propr-n:case</ta>
            <ta e="T460" id="Seg_5949" s="T459">v-v:tense-v:pred.pn</ta>
            <ta e="T461" id="Seg_5950" s="T460">interj</ta>
            <ta e="T462" id="Seg_5951" s="T461">que-pro:case</ta>
            <ta e="T463" id="Seg_5952" s="T462">n-n&gt;adj</ta>
            <ta e="T464" id="Seg_5953" s="T463">n-n:(poss)-n:(pred.pn)-ptcl</ta>
            <ta e="T465" id="Seg_5954" s="T464">v-v:tense-v:poss.pn</ta>
            <ta e="T466" id="Seg_5955" s="T465">ptcl</ta>
            <ta e="T467" id="Seg_5956" s="T466">ptcl</ta>
            <ta e="T468" id="Seg_5957" s="T467">adv</ta>
            <ta e="T469" id="Seg_5958" s="T468">pers-pro:case</ta>
            <ta e="T470" id="Seg_5959" s="T469">n-n:poss-n:case</ta>
            <ta e="T471" id="Seg_5960" s="T470">n-n:case</ta>
            <ta e="T472" id="Seg_5961" s="T471">v-v:ptcp</ta>
            <ta e="T473" id="Seg_5962" s="T472">n-n&gt;adj-n:(pred.pn)</ta>
            <ta e="T474" id="Seg_5963" s="T473">v-v:tense-v:pred.pn</ta>
            <ta e="T475" id="Seg_5964" s="T474">que</ta>
            <ta e="T476" id="Seg_5965" s="T475">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T477" id="Seg_5966" s="T476">adv</ta>
            <ta e="T478" id="Seg_5967" s="T477">v-v:tense-v:poss.pn</ta>
            <ta e="T479" id="Seg_5968" s="T478">propr-n:case</ta>
            <ta e="T480" id="Seg_5969" s="T479">adj</ta>
            <ta e="T481" id="Seg_5970" s="T480">n-n:case</ta>
            <ta e="T482" id="Seg_5971" s="T481">adj</ta>
            <ta e="T483" id="Seg_5972" s="T482">n-n:poss-n:case</ta>
            <ta e="T484" id="Seg_5973" s="T483">n-n&gt;v-v:cvb</ta>
            <ta e="T485" id="Seg_5974" s="T484">v-v:cvb-v-v:cvb</ta>
            <ta e="T486" id="Seg_5975" s="T485">v-v:tense-v:pred.pn</ta>
            <ta e="T487" id="Seg_5976" s="T486">que-pro:case</ta>
            <ta e="T488" id="Seg_5977" s="T487">ptcl</ta>
            <ta e="T489" id="Seg_5978" s="T488">n-n:case</ta>
            <ta e="T490" id="Seg_5979" s="T489">v-v:cvb</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_5980" s="T0">adv</ta>
            <ta e="T2" id="Seg_5981" s="T1">v</ta>
            <ta e="T3" id="Seg_5982" s="T2">n</ta>
            <ta e="T4" id="Seg_5983" s="T3">v</ta>
            <ta e="T5" id="Seg_5984" s="T4">propr</ta>
            <ta e="T6" id="Seg_5985" s="T5">v</ta>
            <ta e="T7" id="Seg_5986" s="T6">n</ta>
            <ta e="T8" id="Seg_5987" s="T7">v</ta>
            <ta e="T9" id="Seg_5988" s="T8">adv</ta>
            <ta e="T10" id="Seg_5989" s="T9">v</ta>
            <ta e="T11" id="Seg_5990" s="T10">aux</ta>
            <ta e="T12" id="Seg_5991" s="T11">n</ta>
            <ta e="T13" id="Seg_5992" s="T12">v</ta>
            <ta e="T14" id="Seg_5993" s="T13">dempro</ta>
            <ta e="T15" id="Seg_5994" s="T14">v</ta>
            <ta e="T16" id="Seg_5995" s="T15">v</ta>
            <ta e="T17" id="Seg_5996" s="T16">adj</ta>
            <ta e="T18" id="Seg_5997" s="T17">n</ta>
            <ta e="T19" id="Seg_5998" s="T18">que</ta>
            <ta e="T20" id="Seg_5999" s="T19">cop</ta>
            <ta e="T21" id="Seg_6000" s="T20">pers</ta>
            <ta e="T22" id="Seg_6001" s="T21">adv</ta>
            <ta e="T23" id="Seg_6002" s="T22">v</ta>
            <ta e="T24" id="Seg_6003" s="T23">v</ta>
            <ta e="T25" id="Seg_6004" s="T24">v</ta>
            <ta e="T26" id="Seg_6005" s="T25">n</ta>
            <ta e="T27" id="Seg_6006" s="T26">n</ta>
            <ta e="T28" id="Seg_6007" s="T27">v</ta>
            <ta e="T29" id="Seg_6008" s="T28">v</ta>
            <ta e="T30" id="Seg_6009" s="T29">propr</ta>
            <ta e="T31" id="Seg_6010" s="T30">v</ta>
            <ta e="T32" id="Seg_6011" s="T31">adj</ta>
            <ta e="T33" id="Seg_6012" s="T32">n</ta>
            <ta e="T34" id="Seg_6013" s="T33">n</ta>
            <ta e="T35" id="Seg_6014" s="T34">n</ta>
            <ta e="T36" id="Seg_6015" s="T35">n</ta>
            <ta e="T37" id="Seg_6016" s="T36">v</ta>
            <ta e="T38" id="Seg_6017" s="T37">conj</ta>
            <ta e="T39" id="Seg_6018" s="T38">n</ta>
            <ta e="T40" id="Seg_6019" s="T39">v</ta>
            <ta e="T41" id="Seg_6020" s="T40">propr</ta>
            <ta e="T42" id="Seg_6021" s="T41">n</ta>
            <ta e="T43" id="Seg_6022" s="T42">ptcl</ta>
            <ta e="T44" id="Seg_6023" s="T43">cop</ta>
            <ta e="T45" id="Seg_6024" s="T44">aux</ta>
            <ta e="T46" id="Seg_6025" s="T45">n</ta>
            <ta e="T47" id="Seg_6026" s="T46">n</ta>
            <ta e="T48" id="Seg_6027" s="T47">cop</ta>
            <ta e="T49" id="Seg_6028" s="T48">post</ta>
            <ta e="T50" id="Seg_6029" s="T49">n</ta>
            <ta e="T51" id="Seg_6030" s="T50">n</ta>
            <ta e="T52" id="Seg_6031" s="T51">post</ta>
            <ta e="T53" id="Seg_6032" s="T52">v</ta>
            <ta e="T54" id="Seg_6033" s="T53">aux</ta>
            <ta e="T55" id="Seg_6034" s="T54">interj</ta>
            <ta e="T56" id="Seg_6035" s="T55">n</ta>
            <ta e="T57" id="Seg_6036" s="T56">conj</ta>
            <ta e="T58" id="Seg_6037" s="T57">que</ta>
            <ta e="T59" id="Seg_6038" s="T58">v</ta>
            <ta e="T60" id="Seg_6039" s="T59">v</ta>
            <ta e="T61" id="Seg_6040" s="T60">aux</ta>
            <ta e="T62" id="Seg_6041" s="T61">v</ta>
            <ta e="T63" id="Seg_6042" s="T62">conj</ta>
            <ta e="T64" id="Seg_6043" s="T63">n</ta>
            <ta e="T65" id="Seg_6044" s="T64">v</ta>
            <ta e="T66" id="Seg_6045" s="T65">v</ta>
            <ta e="T67" id="Seg_6046" s="T66">post</ta>
            <ta e="T68" id="Seg_6047" s="T67">n</ta>
            <ta e="T69" id="Seg_6048" s="T68">v</ta>
            <ta e="T70" id="Seg_6049" s="T69">aux</ta>
            <ta e="T71" id="Seg_6050" s="T70">propr</ta>
            <ta e="T72" id="Seg_6051" s="T71">n</ta>
            <ta e="T73" id="Seg_6052" s="T72">post</ta>
            <ta e="T74" id="Seg_6053" s="T73">n</ta>
            <ta e="T75" id="Seg_6054" s="T74">n</ta>
            <ta e="T76" id="Seg_6055" s="T75">v</ta>
            <ta e="T77" id="Seg_6056" s="T76">aux</ta>
            <ta e="T78" id="Seg_6057" s="T77">n</ta>
            <ta e="T79" id="Seg_6058" s="T78">ptcl</ta>
            <ta e="T80" id="Seg_6059" s="T79">aux</ta>
            <ta e="T81" id="Seg_6060" s="T80">adj</ta>
            <ta e="T82" id="Seg_6061" s="T81">adj</ta>
            <ta e="T83" id="Seg_6062" s="T82">adj</ta>
            <ta e="T84" id="Seg_6063" s="T83">n</ta>
            <ta e="T85" id="Seg_6064" s="T84">adv</ta>
            <ta e="T86" id="Seg_6065" s="T85">v</ta>
            <ta e="T87" id="Seg_6066" s="T86">aux</ta>
            <ta e="T88" id="Seg_6067" s="T87">v</ta>
            <ta e="T89" id="Seg_6068" s="T88">n</ta>
            <ta e="T90" id="Seg_6069" s="T89">emphpro</ta>
            <ta e="T91" id="Seg_6070" s="T90">n</ta>
            <ta e="T92" id="Seg_6071" s="T91">n</ta>
            <ta e="T93" id="Seg_6072" s="T92">v</ta>
            <ta e="T94" id="Seg_6073" s="T93">dempro</ta>
            <ta e="T95" id="Seg_6074" s="T94">n</ta>
            <ta e="T96" id="Seg_6075" s="T95">v</ta>
            <ta e="T97" id="Seg_6076" s="T96">aux</ta>
            <ta e="T98" id="Seg_6077" s="T97">aux</ta>
            <ta e="T99" id="Seg_6078" s="T98">n</ta>
            <ta e="T100" id="Seg_6079" s="T99">n</ta>
            <ta e="T101" id="Seg_6080" s="T100">n</ta>
            <ta e="T102" id="Seg_6081" s="T101">n</ta>
            <ta e="T103" id="Seg_6082" s="T102">adj</ta>
            <ta e="T104" id="Seg_6083" s="T103">v</ta>
            <ta e="T105" id="Seg_6084" s="T104">adj</ta>
            <ta e="T106" id="Seg_6085" s="T105">cop</ta>
            <ta e="T107" id="Seg_6086" s="T106">dempro</ta>
            <ta e="T108" id="Seg_6087" s="T107">n</ta>
            <ta e="T109" id="Seg_6088" s="T108">n</ta>
            <ta e="T110" id="Seg_6089" s="T109">ptcl</ta>
            <ta e="T111" id="Seg_6090" s="T110">v</ta>
            <ta e="T112" id="Seg_6091" s="T111">aux</ta>
            <ta e="T113" id="Seg_6092" s="T112">propr</ta>
            <ta e="T114" id="Seg_6093" s="T113">adv</ta>
            <ta e="T115" id="Seg_6094" s="T114">v</ta>
            <ta e="T116" id="Seg_6095" s="T115">n</ta>
            <ta e="T117" id="Seg_6096" s="T116">pers</ta>
            <ta e="T118" id="Seg_6097" s="T117">n</ta>
            <ta e="T119" id="Seg_6098" s="T118">v</ta>
            <ta e="T120" id="Seg_6099" s="T119">v</ta>
            <ta e="T121" id="Seg_6100" s="T120">n</ta>
            <ta e="T122" id="Seg_6101" s="T121">ptcl</ta>
            <ta e="T123" id="Seg_6102" s="T122">v</ta>
            <ta e="T124" id="Seg_6103" s="T123">n</ta>
            <ta e="T125" id="Seg_6104" s="T124">n</ta>
            <ta e="T126" id="Seg_6105" s="T125">n</ta>
            <ta e="T127" id="Seg_6106" s="T126">v</ta>
            <ta e="T128" id="Seg_6107" s="T127">v</ta>
            <ta e="T129" id="Seg_6108" s="T128">n</ta>
            <ta e="T130" id="Seg_6109" s="T129">n</ta>
            <ta e="T131" id="Seg_6110" s="T130">v</ta>
            <ta e="T132" id="Seg_6111" s="T131">aux</ta>
            <ta e="T133" id="Seg_6112" s="T132">propr</ta>
            <ta e="T134" id="Seg_6113" s="T133">n</ta>
            <ta e="T135" id="Seg_6114" s="T134">n</ta>
            <ta e="T136" id="Seg_6115" s="T135">v</ta>
            <ta e="T137" id="Seg_6116" s="T136">aux</ta>
            <ta e="T138" id="Seg_6117" s="T137">n</ta>
            <ta e="T139" id="Seg_6118" s="T138">que</ta>
            <ta e="T140" id="Seg_6119" s="T139">n</ta>
            <ta e="T141" id="Seg_6120" s="T140">adv</ta>
            <ta e="T142" id="Seg_6121" s="T141">v</ta>
            <ta e="T143" id="Seg_6122" s="T142">adv</ta>
            <ta e="T144" id="Seg_6123" s="T143">n</ta>
            <ta e="T145" id="Seg_6124" s="T144">ptcl</ta>
            <ta e="T146" id="Seg_6125" s="T145">adv</ta>
            <ta e="T147" id="Seg_6126" s="T146">v</ta>
            <ta e="T148" id="Seg_6127" s="T147">interj</ta>
            <ta e="T149" id="Seg_6128" s="T148">n</ta>
            <ta e="T150" id="Seg_6129" s="T149">v</ta>
            <ta e="T151" id="Seg_6130" s="T150">ptcl</ta>
            <ta e="T152" id="Seg_6131" s="T151">n</ta>
            <ta e="T153" id="Seg_6132" s="T152">n</ta>
            <ta e="T154" id="Seg_6133" s="T153">n</ta>
            <ta e="T155" id="Seg_6134" s="T154">v</ta>
            <ta e="T156" id="Seg_6135" s="T155">que</ta>
            <ta e="T157" id="Seg_6136" s="T156">post</ta>
            <ta e="T158" id="Seg_6137" s="T157">cardnum</ta>
            <ta e="T159" id="Seg_6138" s="T158">n</ta>
            <ta e="T160" id="Seg_6139" s="T159">v</ta>
            <ta e="T161" id="Seg_6140" s="T160">v</ta>
            <ta e="T162" id="Seg_6141" s="T161">n</ta>
            <ta e="T163" id="Seg_6142" s="T162">v</ta>
            <ta e="T164" id="Seg_6143" s="T163">n</ta>
            <ta e="T165" id="Seg_6144" s="T164">adv</ta>
            <ta e="T166" id="Seg_6145" s="T165">v</ta>
            <ta e="T167" id="Seg_6146" s="T166">aux</ta>
            <ta e="T168" id="Seg_6147" s="T167">n</ta>
            <ta e="T169" id="Seg_6148" s="T168">propr</ta>
            <ta e="T170" id="Seg_6149" s="T169">v</ta>
            <ta e="T171" id="Seg_6150" s="T170">v</ta>
            <ta e="T172" id="Seg_6151" s="T171">aux</ta>
            <ta e="T173" id="Seg_6152" s="T172">adv</ta>
            <ta e="T174" id="Seg_6153" s="T173">v</ta>
            <ta e="T175" id="Seg_6154" s="T174">v</ta>
            <ta e="T176" id="Seg_6155" s="T175">post</ta>
            <ta e="T177" id="Seg_6156" s="T176">dempro</ta>
            <ta e="T178" id="Seg_6157" s="T177">n</ta>
            <ta e="T179" id="Seg_6158" s="T178">n</ta>
            <ta e="T180" id="Seg_6159" s="T179">v</ta>
            <ta e="T181" id="Seg_6160" s="T180">n</ta>
            <ta e="T182" id="Seg_6161" s="T181">v</ta>
            <ta e="T183" id="Seg_6162" s="T182">n</ta>
            <ta e="T184" id="Seg_6163" s="T183">pers</ta>
            <ta e="T185" id="Seg_6164" s="T184">v</ta>
            <ta e="T186" id="Seg_6165" s="T185">v</ta>
            <ta e="T187" id="Seg_6166" s="T186">dempro</ta>
            <ta e="T188" id="Seg_6167" s="T187">que</ta>
            <ta e="T189" id="Seg_6168" s="T188">ptcl</ta>
            <ta e="T190" id="Seg_6169" s="T189">n</ta>
            <ta e="T191" id="Seg_6170" s="T190">v</ta>
            <ta e="T192" id="Seg_6171" s="T191">v</ta>
            <ta e="T193" id="Seg_6172" s="T192">aux</ta>
            <ta e="T194" id="Seg_6173" s="T193">post</ta>
            <ta e="T195" id="Seg_6174" s="T194">ptcl</ta>
            <ta e="T196" id="Seg_6175" s="T195">ptcl</ta>
            <ta e="T197" id="Seg_6176" s="T196">v</ta>
            <ta e="T198" id="Seg_6177" s="T197">adv</ta>
            <ta e="T199" id="Seg_6178" s="T198">ptcl</ta>
            <ta e="T200" id="Seg_6179" s="T199">v</ta>
            <ta e="T201" id="Seg_6180" s="T200">ptcl</ta>
            <ta e="T202" id="Seg_6181" s="T201">ptcl</ta>
            <ta e="T203" id="Seg_6182" s="T202">v</ta>
            <ta e="T204" id="Seg_6183" s="T203">ptcl</ta>
            <ta e="T205" id="Seg_6184" s="T204">n</ta>
            <ta e="T206" id="Seg_6185" s="T205">n</ta>
            <ta e="T207" id="Seg_6186" s="T206">ptcl</ta>
            <ta e="T208" id="Seg_6187" s="T207">v</ta>
            <ta e="T209" id="Seg_6188" s="T208">v</ta>
            <ta e="T210" id="Seg_6189" s="T209">propr</ta>
            <ta e="T211" id="Seg_6190" s="T210">n</ta>
            <ta e="T212" id="Seg_6191" s="T211">propr</ta>
            <ta e="T213" id="Seg_6192" s="T212">n</ta>
            <ta e="T214" id="Seg_6193" s="T213">v</ta>
            <ta e="T215" id="Seg_6194" s="T214">adv</ta>
            <ta e="T216" id="Seg_6195" s="T215">n</ta>
            <ta e="T217" id="Seg_6196" s="T216">post</ta>
            <ta e="T218" id="Seg_6197" s="T217">v</ta>
            <ta e="T219" id="Seg_6198" s="T218">aux</ta>
            <ta e="T220" id="Seg_6199" s="T219">adj</ta>
            <ta e="T221" id="Seg_6200" s="T220">n</ta>
            <ta e="T222" id="Seg_6201" s="T221">n</ta>
            <ta e="T223" id="Seg_6202" s="T222">v</ta>
            <ta e="T224" id="Seg_6203" s="T223">post</ta>
            <ta e="T225" id="Seg_6204" s="T224">v</ta>
            <ta e="T226" id="Seg_6205" s="T225">conj</ta>
            <ta e="T227" id="Seg_6206" s="T226">v</ta>
            <ta e="T228" id="Seg_6207" s="T227">aux</ta>
            <ta e="T229" id="Seg_6208" s="T228">dempro</ta>
            <ta e="T230" id="Seg_6209" s="T229">n</ta>
            <ta e="T231" id="Seg_6210" s="T230">n</ta>
            <ta e="T232" id="Seg_6211" s="T231">n</ta>
            <ta e="T233" id="Seg_6212" s="T232">v</ta>
            <ta e="T234" id="Seg_6213" s="T233">ptcl</ta>
            <ta e="T235" id="Seg_6214" s="T234">adj</ta>
            <ta e="T236" id="Seg_6215" s="T235">n</ta>
            <ta e="T237" id="Seg_6216" s="T236">cop</ta>
            <ta e="T238" id="Seg_6217" s="T237">n</ta>
            <ta e="T239" id="Seg_6218" s="T238">v</ta>
            <ta e="T240" id="Seg_6219" s="T239">v</ta>
            <ta e="T241" id="Seg_6220" s="T240">n</ta>
            <ta e="T242" id="Seg_6221" s="T241">v</ta>
            <ta e="T243" id="Seg_6222" s="T242">interj</ta>
            <ta e="T244" id="Seg_6223" s="T243">que</ta>
            <ta e="T245" id="Seg_6224" s="T244">v</ta>
            <ta e="T246" id="Seg_6225" s="T245">v</ta>
            <ta e="T247" id="Seg_6226" s="T246">v</ta>
            <ta e="T248" id="Seg_6227" s="T247">n</ta>
            <ta e="T249" id="Seg_6228" s="T248">v</ta>
            <ta e="T250" id="Seg_6229" s="T249">propr</ta>
            <ta e="T251" id="Seg_6230" s="T250">n</ta>
            <ta e="T252" id="Seg_6231" s="T251">n</ta>
            <ta e="T253" id="Seg_6232" s="T252">v</ta>
            <ta e="T254" id="Seg_6233" s="T253">post</ta>
            <ta e="T255" id="Seg_6234" s="T254">v</ta>
            <ta e="T256" id="Seg_6235" s="T255">interj</ta>
            <ta e="T257" id="Seg_6236" s="T256">pers</ta>
            <ta e="T258" id="Seg_6237" s="T257">propr</ta>
            <ta e="T259" id="Seg_6238" s="T258">ptcl</ta>
            <ta e="T260" id="Seg_6239" s="T259">pers</ta>
            <ta e="T261" id="Seg_6240" s="T260">dempro</ta>
            <ta e="T262" id="Seg_6241" s="T261">v</ta>
            <ta e="T263" id="Seg_6242" s="T262">propr</ta>
            <ta e="T264" id="Seg_6243" s="T263">v</ta>
            <ta e="T265" id="Seg_6244" s="T264">adj</ta>
            <ta e="T266" id="Seg_6245" s="T265">n</ta>
            <ta e="T267" id="Seg_6246" s="T266">v</ta>
            <ta e="T268" id="Seg_6247" s="T267">aux</ta>
            <ta e="T269" id="Seg_6248" s="T268">dempro</ta>
            <ta e="T270" id="Seg_6249" s="T269">n</ta>
            <ta e="T271" id="Seg_6250" s="T270">v</ta>
            <ta e="T272" id="Seg_6251" s="T271">aux</ta>
            <ta e="T273" id="Seg_6252" s="T272">que</ta>
            <ta e="T274" id="Seg_6253" s="T273">cop</ta>
            <ta e="T275" id="Seg_6254" s="T274">v</ta>
            <ta e="T276" id="Seg_6255" s="T275">n</ta>
            <ta e="T277" id="Seg_6256" s="T276">v</ta>
            <ta e="T278" id="Seg_6257" s="T277">adj</ta>
            <ta e="T279" id="Seg_6258" s="T278">n</ta>
            <ta e="T280" id="Seg_6259" s="T279">v</ta>
            <ta e="T281" id="Seg_6260" s="T280">v</ta>
            <ta e="T282" id="Seg_6261" s="T281">cardnum</ta>
            <ta e="T283" id="Seg_6262" s="T282">n</ta>
            <ta e="T284" id="Seg_6263" s="T283">v</ta>
            <ta e="T285" id="Seg_6264" s="T284">adj</ta>
            <ta e="T286" id="Seg_6265" s="T285">n</ta>
            <ta e="T287" id="Seg_6266" s="T286">v</ta>
            <ta e="T288" id="Seg_6267" s="T287">propr</ta>
            <ta e="T289" id="Seg_6268" s="T288">v</ta>
            <ta e="T290" id="Seg_6269" s="T289">dempro</ta>
            <ta e="T291" id="Seg_6270" s="T290">que</ta>
            <ta e="T292" id="Seg_6271" s="T291">n</ta>
            <ta e="T293" id="Seg_6272" s="T292">ptcl</ta>
            <ta e="T294" id="Seg_6273" s="T293">n</ta>
            <ta e="T295" id="Seg_6274" s="T294">ptcl</ta>
            <ta e="T296" id="Seg_6275" s="T295">ptcl</ta>
            <ta e="T297" id="Seg_6276" s="T296">pers</ta>
            <ta e="T298" id="Seg_6277" s="T297">n</ta>
            <ta e="T299" id="Seg_6278" s="T298">cardnum</ta>
            <ta e="T300" id="Seg_6279" s="T299">adj</ta>
            <ta e="T301" id="Seg_6280" s="T300">dempro</ta>
            <ta e="T302" id="Seg_6281" s="T301">n</ta>
            <ta e="T303" id="Seg_6282" s="T302">n</ta>
            <ta e="T304" id="Seg_6283" s="T303">n</ta>
            <ta e="T305" id="Seg_6284" s="T304">v</ta>
            <ta e="T306" id="Seg_6285" s="T305">n</ta>
            <ta e="T307" id="Seg_6286" s="T306">ptcl</ta>
            <ta e="T308" id="Seg_6287" s="T307">n</ta>
            <ta e="T309" id="Seg_6288" s="T308">cop</ta>
            <ta e="T310" id="Seg_6289" s="T309">v</ta>
            <ta e="T311" id="Seg_6290" s="T310">n</ta>
            <ta e="T312" id="Seg_6291" s="T311">propr</ta>
            <ta e="T313" id="Seg_6292" s="T312">v</ta>
            <ta e="T314" id="Seg_6293" s="T313">conj</ta>
            <ta e="T315" id="Seg_6294" s="T314">n</ta>
            <ta e="T316" id="Seg_6295" s="T315">n</ta>
            <ta e="T317" id="Seg_6296" s="T316">v</ta>
            <ta e="T318" id="Seg_6297" s="T317">n</ta>
            <ta e="T319" id="Seg_6298" s="T318">v</ta>
            <ta e="T320" id="Seg_6299" s="T319">post</ta>
            <ta e="T321" id="Seg_6300" s="T320">v</ta>
            <ta e="T322" id="Seg_6301" s="T321">v</ta>
            <ta e="T323" id="Seg_6302" s="T322">cardnum</ta>
            <ta e="T324" id="Seg_6303" s="T323">n</ta>
            <ta e="T325" id="Seg_6304" s="T324">n</ta>
            <ta e="T326" id="Seg_6305" s="T325">v</ta>
            <ta e="T327" id="Seg_6306" s="T326">v</ta>
            <ta e="T328" id="Seg_6307" s="T327">v</ta>
            <ta e="T329" id="Seg_6308" s="T328">cardnum</ta>
            <ta e="T330" id="Seg_6309" s="T329">n</ta>
            <ta e="T331" id="Seg_6310" s="T330">n</ta>
            <ta e="T332" id="Seg_6311" s="T331">v</ta>
            <ta e="T333" id="Seg_6312" s="T332">n</ta>
            <ta e="T334" id="Seg_6313" s="T333">n</ta>
            <ta e="T335" id="Seg_6314" s="T334">adj</ta>
            <ta e="T336" id="Seg_6315" s="T335">n</ta>
            <ta e="T337" id="Seg_6316" s="T336">v</ta>
            <ta e="T338" id="Seg_6317" s="T337">n</ta>
            <ta e="T339" id="Seg_6318" s="T338">v</ta>
            <ta e="T340" id="Seg_6319" s="T339">dempro</ta>
            <ta e="T341" id="Seg_6320" s="T340">adv</ta>
            <ta e="T342" id="Seg_6321" s="T341">n</ta>
            <ta e="T343" id="Seg_6322" s="T342">post</ta>
            <ta e="T344" id="Seg_6323" s="T343">n</ta>
            <ta e="T345" id="Seg_6324" s="T344">v</ta>
            <ta e="T346" id="Seg_6325" s="T345">aux</ta>
            <ta e="T347" id="Seg_6326" s="T346">aux</ta>
            <ta e="T348" id="Seg_6327" s="T347">n</ta>
            <ta e="T349" id="Seg_6328" s="T348">v</ta>
            <ta e="T350" id="Seg_6329" s="T349">v</ta>
            <ta e="T351" id="Seg_6330" s="T350">v</ta>
            <ta e="T352" id="Seg_6331" s="T351">aux</ta>
            <ta e="T353" id="Seg_6332" s="T352">ptcl</ta>
            <ta e="T354" id="Seg_6333" s="T353">n</ta>
            <ta e="T355" id="Seg_6334" s="T354">ptcl</ta>
            <ta e="T356" id="Seg_6335" s="T355">v</ta>
            <ta e="T357" id="Seg_6336" s="T356">n</ta>
            <ta e="T358" id="Seg_6337" s="T357">dempro</ta>
            <ta e="T359" id="Seg_6338" s="T358">n</ta>
            <ta e="T360" id="Seg_6339" s="T359">que</ta>
            <ta e="T361" id="Seg_6340" s="T360">v</ta>
            <ta e="T362" id="Seg_6341" s="T361">n</ta>
            <ta e="T363" id="Seg_6342" s="T362">que</ta>
            <ta e="T364" id="Seg_6343" s="T363">ptcl</ta>
            <ta e="T365" id="Seg_6344" s="T364">cardnum</ta>
            <ta e="T366" id="Seg_6345" s="T365">n</ta>
            <ta e="T367" id="Seg_6346" s="T366">n</ta>
            <ta e="T368" id="Seg_6347" s="T367">n</ta>
            <ta e="T369" id="Seg_6348" s="T368">v</ta>
            <ta e="T370" id="Seg_6349" s="T369">aux</ta>
            <ta e="T371" id="Seg_6350" s="T370">dempro</ta>
            <ta e="T372" id="Seg_6351" s="T371">v</ta>
            <ta e="T373" id="Seg_6352" s="T372">aux</ta>
            <ta e="T374" id="Seg_6353" s="T373">ptcl</ta>
            <ta e="T375" id="Seg_6354" s="T374">ptcl</ta>
            <ta e="T376" id="Seg_6355" s="T375">v</ta>
            <ta e="T377" id="Seg_6356" s="T376">n</ta>
            <ta e="T378" id="Seg_6357" s="T377">ptcl</ta>
            <ta e="T379" id="Seg_6358" s="T378">v</ta>
            <ta e="T380" id="Seg_6359" s="T379">n</ta>
            <ta e="T381" id="Seg_6360" s="T380">ptcl</ta>
            <ta e="T382" id="Seg_6361" s="T381">v</ta>
            <ta e="T383" id="Seg_6362" s="T382">adj</ta>
            <ta e="T384" id="Seg_6363" s="T383">n</ta>
            <ta e="T385" id="Seg_6364" s="T384">propr</ta>
            <ta e="T386" id="Seg_6365" s="T385">v</ta>
            <ta e="T387" id="Seg_6366" s="T386">v</ta>
            <ta e="T388" id="Seg_6367" s="T387">dempro</ta>
            <ta e="T389" id="Seg_6368" s="T388">n</ta>
            <ta e="T390" id="Seg_6369" s="T389">v</ta>
            <ta e="T391" id="Seg_6370" s="T390">v</ta>
            <ta e="T392" id="Seg_6371" s="T391">n</ta>
            <ta e="T393" id="Seg_6372" s="T392">v</ta>
            <ta e="T394" id="Seg_6373" s="T393">ptcl</ta>
            <ta e="T395" id="Seg_6374" s="T394">n</ta>
            <ta e="T396" id="Seg_6375" s="T395">adj</ta>
            <ta e="T397" id="Seg_6376" s="T396">dempro</ta>
            <ta e="T398" id="Seg_6377" s="T397">n</ta>
            <ta e="T399" id="Seg_6378" s="T398">ptcl</ta>
            <ta e="T400" id="Seg_6379" s="T399">n</ta>
            <ta e="T401" id="Seg_6380" s="T400">v</ta>
            <ta e="T402" id="Seg_6381" s="T401">n</ta>
            <ta e="T403" id="Seg_6382" s="T402">post</ta>
            <ta e="T404" id="Seg_6383" s="T403">adv</ta>
            <ta e="T405" id="Seg_6384" s="T404">v</ta>
            <ta e="T406" id="Seg_6385" s="T405">v</ta>
            <ta e="T407" id="Seg_6386" s="T406">n</ta>
            <ta e="T408" id="Seg_6387" s="T407">adj</ta>
            <ta e="T409" id="Seg_6388" s="T408">n</ta>
            <ta e="T410" id="Seg_6389" s="T409">n</ta>
            <ta e="T411" id="Seg_6390" s="T410">interj</ta>
            <ta e="T412" id="Seg_6391" s="T411">adv</ta>
            <ta e="T413" id="Seg_6392" s="T412">adj</ta>
            <ta e="T414" id="Seg_6393" s="T413">n</ta>
            <ta e="T415" id="Seg_6394" s="T414">v</ta>
            <ta e="T416" id="Seg_6395" s="T415">ptcl</ta>
            <ta e="T417" id="Seg_6396" s="T416">adv</ta>
            <ta e="T418" id="Seg_6397" s="T417">pers</ta>
            <ta e="T419" id="Seg_6398" s="T418">pers</ta>
            <ta e="T420" id="Seg_6399" s="T419">n</ta>
            <ta e="T421" id="Seg_6400" s="T420">v</ta>
            <ta e="T422" id="Seg_6401" s="T421">v</ta>
            <ta e="T423" id="Seg_6402" s="T422">n</ta>
            <ta e="T424" id="Seg_6403" s="T423">que</ta>
            <ta e="T425" id="Seg_6404" s="T424">adj</ta>
            <ta e="T426" id="Seg_6405" s="T425">que</ta>
            <ta e="T427" id="Seg_6406" s="T426">n</ta>
            <ta e="T428" id="Seg_6407" s="T427">v</ta>
            <ta e="T429" id="Seg_6408" s="T428">v</ta>
            <ta e="T430" id="Seg_6409" s="T429">v</ta>
            <ta e="T431" id="Seg_6410" s="T430">aux</ta>
            <ta e="T432" id="Seg_6411" s="T431">propr</ta>
            <ta e="T433" id="Seg_6412" s="T432">v</ta>
            <ta e="T434" id="Seg_6413" s="T433">pers</ta>
            <ta e="T435" id="Seg_6414" s="T434">v</ta>
            <ta e="T436" id="Seg_6415" s="T435">n</ta>
            <ta e="T437" id="Seg_6416" s="T436">que</ta>
            <ta e="T438" id="Seg_6417" s="T437">adj</ta>
            <ta e="T439" id="Seg_6418" s="T438">v</ta>
            <ta e="T440" id="Seg_6419" s="T439">n</ta>
            <ta e="T441" id="Seg_6420" s="T440">v</ta>
            <ta e="T442" id="Seg_6421" s="T441">conj</ta>
            <ta e="T443" id="Seg_6422" s="T442">pers</ta>
            <ta e="T444" id="Seg_6423" s="T443">n</ta>
            <ta e="T445" id="Seg_6424" s="T444">v</ta>
            <ta e="T446" id="Seg_6425" s="T445">n</ta>
            <ta e="T447" id="Seg_6426" s="T446">v</ta>
            <ta e="T448" id="Seg_6427" s="T447">ptcl</ta>
            <ta e="T449" id="Seg_6428" s="T448">n</ta>
            <ta e="T450" id="Seg_6429" s="T449">cop</ta>
            <ta e="T451" id="Seg_6430" s="T450">cop</ta>
            <ta e="T452" id="Seg_6431" s="T451">pers</ta>
            <ta e="T453" id="Seg_6432" s="T452">n</ta>
            <ta e="T454" id="Seg_6433" s="T453">cardnum</ta>
            <ta e="T455" id="Seg_6434" s="T454">n</ta>
            <ta e="T456" id="Seg_6435" s="T455">n</ta>
            <ta e="T457" id="Seg_6436" s="T456">v</ta>
            <ta e="T458" id="Seg_6437" s="T457">adv</ta>
            <ta e="T459" id="Seg_6438" s="T458">propr</ta>
            <ta e="T460" id="Seg_6439" s="T459">v</ta>
            <ta e="T461" id="Seg_6440" s="T460">interj</ta>
            <ta e="T462" id="Seg_6441" s="T461">que</ta>
            <ta e="T463" id="Seg_6442" s="T462">adj</ta>
            <ta e="T464" id="Seg_6443" s="T463">n</ta>
            <ta e="T465" id="Seg_6444" s="T464">v</ta>
            <ta e="T466" id="Seg_6445" s="T465">ptcl</ta>
            <ta e="T467" id="Seg_6446" s="T466">ptcl</ta>
            <ta e="T468" id="Seg_6447" s="T467">adv</ta>
            <ta e="T469" id="Seg_6448" s="T468">pers</ta>
            <ta e="T470" id="Seg_6449" s="T469">n</ta>
            <ta e="T471" id="Seg_6450" s="T470">n</ta>
            <ta e="T472" id="Seg_6451" s="T471">v</ta>
            <ta e="T473" id="Seg_6452" s="T472">adj</ta>
            <ta e="T474" id="Seg_6453" s="T473">aux</ta>
            <ta e="T475" id="Seg_6454" s="T474">que</ta>
            <ta e="T476" id="Seg_6455" s="T475">v</ta>
            <ta e="T477" id="Seg_6456" s="T476">adv</ta>
            <ta e="T478" id="Seg_6457" s="T477">v</ta>
            <ta e="T479" id="Seg_6458" s="T478">propr</ta>
            <ta e="T480" id="Seg_6459" s="T479">adj</ta>
            <ta e="T481" id="Seg_6460" s="T480">n</ta>
            <ta e="T482" id="Seg_6461" s="T481">adj</ta>
            <ta e="T483" id="Seg_6462" s="T482">n</ta>
            <ta e="T484" id="Seg_6463" s="T483">v</ta>
            <ta e="T485" id="Seg_6464" s="T484">v</ta>
            <ta e="T486" id="Seg_6465" s="T485">v</ta>
            <ta e="T487" id="Seg_6466" s="T486">que</ta>
            <ta e="T488" id="Seg_6467" s="T487">ptcl</ta>
            <ta e="T489" id="Seg_6468" s="T488">n</ta>
            <ta e="T490" id="Seg_6469" s="T489">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR" />
         <annotation name="SyF" tierref="SyF" />
         <annotation name="IST" tierref="IST" />
         <annotation name="Top" tierref="Top" />
         <annotation name="Foc" tierref="Foc" />
         <annotation name="BOR" tierref="BOR" />
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T8" id="Seg_6470" s="T0">There lived a dog named Laajku, it didn't remember his parents.</ta>
            <ta e="T13" id="Seg_6471" s="T8">Once it went to search for people.</ta>
            <ta e="T18" id="Seg_6472" s="T13">Walking it saw a high hill.</ta>
            <ta e="T25" id="Seg_6473" s="T18">"What will be if I climb there up?", it thought.</ta>
            <ta e="T34" id="Seg_6474" s="T25">Having climbed up the hill it saw the Baba Yaga going from the other side.</ta>
            <ta e="T40" id="Seg_6475" s="T34">It reached the middle of the hill and sat down on its hind legs.</ta>
            <ta e="T45" id="Seg_6476" s="T40">Laajku was apparently a big werebeast.</ta>
            <ta e="T54" id="Seg_6477" s="T45">After it had become a child, it rolled down the hill.</ta>
            <ta e="T57" id="Seg_6478" s="T54">"Oh, what a sin!</ta>
            <ta e="T70" id="Seg_6479" s="T57">How could I not notice that I gave birth to a child?!", she said, wrapped the child up and carried it home.</ta>
            <ta e="T77" id="Seg_6480" s="T70">Baba Yaga lived together with her old man on the river shore.</ta>
            <ta e="T83" id="Seg_6481" s="T77">They didn't have any children, they had a lonely tamed reindeer.</ta>
            <ta e="T88" id="Seg_6482" s="T83">The child was growing day by day, it started to talk.</ta>
            <ta e="T93" id="Seg_6483" s="T88">The mother sewed a long coat and some other clothes for it.</ta>
            <ta e="T100" id="Seg_6484" s="T93">This child began to run around the house.</ta>
            <ta e="T106" id="Seg_6485" s="T100">The old man used to go fishing with a net every day.</ta>
            <ta e="T112" id="Seg_6486" s="T106">Apparently they fed the child only with fish.</ta>
            <ta e="T115" id="Seg_6487" s="T112">Laajku said once: </ta>
            <ta e="T120" id="Seg_6488" s="T115">"Mum, I want to eat meat."</ta>
            <ta e="T128" id="Seg_6489" s="T120">"Old man, kill an infertile [female reindeer], the child wants to eat meat."</ta>
            <ta e="T132" id="Seg_6490" s="T128">The old man killed the reindeer.</ta>
            <ta e="T137" id="Seg_6491" s="T132">Laajku-werebeast's wishes were growing.</ta>
            <ta e="T147" id="Seg_6492" s="T137">"Mum, why don't we move to the other shore of the river, it seems to be dry lowland over there", it said.</ta>
            <ta e="T153" id="Seg_6493" s="T147">"Old man, do you hear what the child is saying?</ta>
            <ta e="T160" id="Seg_6494" s="T153">It says the truth, for how long should we live in one place?!</ta>
            <ta e="T161" id="Seg_6495" s="T160">Let us move!"</ta>
            <ta e="T168" id="Seg_6496" s="T161">The old man didn't baulk and brought his people in a small boat to the other shore.</ta>
            <ta e="T172" id="Seg_6497" s="T168">Laajku ran barking.</ta>
            <ta e="T182" id="Seg_6498" s="T172">In the evening, as they were ready with the move, the child came to his mother and sat down onto her knees.</ta>
            <ta e="T186" id="Seg_6499" s="T182">"Mum, I want to go by boat [myself]."</ta>
            <ta e="T191" id="Seg_6500" s="T186">"What else then!", the old man got angry.</ta>
            <ta e="T201" id="Seg_6501" s="T191">He sat down and [said]: "Well, then go by boat, but just don't go too far away, all right?"</ta>
            <ta e="T211" id="Seg_6502" s="T201">"I won't go [far], I will just go along the shore", said the Laajku beast.</ta>
            <ta e="T219" id="Seg_6503" s="T211">Laajku sat down into the boat and rowed silently along the shore.</ta>
            <ta e="T228" id="Seg_6504" s="T219">As the old man and the old woman had gone into the house, he was moving away.</ta>
            <ta e="T237" id="Seg_6505" s="T228">As this child reached the middle of the river, it became an adult.</ta>
            <ta e="T242" id="Seg_6506" s="T237">The old man came out and saw that his son had moved away: </ta>
            <ta e="T245" id="Seg_6507" s="T242">"Hey, where are you going?</ta>
            <ta e="T247" id="Seg_6508" s="T245">I tell you not to move away.</ta>
            <ta e="T249" id="Seg_6509" s="T247">You will fall into the water!"</ta>
            <ta e="T256" id="Seg_6510" s="T249">Laajku reached the middle of the river and shouted "haha".</ta>
            <ta e="T262" id="Seg_6511" s="T256">"I am Laajku then and you did not notice that!"</ta>
            <ta e="T264" id="Seg_6512" s="T262">Laajku escaped.</ta>
            <ta e="T268" id="Seg_6513" s="T264">The old man and the old woman started to fight.</ta>
            <ta e="T272" id="Seg_6514" s="T268">So this boy went away.</ta>
            <ta e="T281" id="Seg_6515" s="T272">After a while he saw houses standing, reindeers being tied to the sledge.</ta>
            <ta e="T287" id="Seg_6516" s="T281">He went into one house, a lonely woman sits there.</ta>
            <ta e="T289" id="Seg_6517" s="T287">Laajku drank tea.</ta>
            <ta e="T295" id="Seg_6518" s="T289">"What is this, some gathering or a wedding?"</ta>
            <ta e="T296" id="Seg_6519" s="T295">"No.</ta>
            <ta e="T311" id="Seg_6520" s="T296">Our prince has one daughter, the one who guesses the prince's riddle will become her husband", the woman said.</ta>
            <ta e="T321" id="Seg_6521" s="T311">Laajku went out, went to the river shore, lay down and covered himself with grass.</ta>
            <ta e="T332" id="Seg_6522" s="T321">He saw two girls with shoulder yokes going to scoop water and carrying two containers on their backs.</ta>
            <ta e="T339" id="Seg_6523" s="T332">The rich girl stumbled over the boy's legs and spilled her water: </ta>
            <ta e="T353" id="Seg_6524" s="T339">"That's all because of my father, if I had a husband, would I get tired scooping water?!</ta>
            <ta e="T356" id="Seg_6525" s="T353">I just made my legs wet."</ta>
            <ta e="T361" id="Seg_6526" s="T356">"My friend, what riddle does your father give?"</ta>
            <ta e="T370" id="Seg_6527" s="T361">"Once my father had sewn a cover for load out of the skin of seven lice.</ta>
            <ta e="T374" id="Seg_6528" s="T370">This might be the riddle.</ta>
            <ta e="T376" id="Seg_6529" s="T374">Well, let's go.</ta>
            <ta e="T384" id="Seg_6530" s="T376">There is too much water and wood to bear for so many guests."</ta>
            <ta e="T391" id="Seg_6531" s="T384">Laajku came back, went into that house and overnighted there.</ta>
            <ta e="T396" id="Seg_6532" s="T391">The day began and it was again full of guests.</ta>
            <ta e="T405" id="Seg_6533" s="T396">The boy also wanted to go to the prince, together with the guests he went inside.</ta>
            <ta e="T410" id="Seg_6534" s="T405">He saw that the house was full of men.</ta>
            <ta e="T415" id="Seg_6535" s="T410">"Oh, today a new guest has come.</ta>
            <ta e="T423" id="Seg_6536" s="T415">Well, maybe you will guess my riddle?", the prince said.</ta>
            <ta e="T425" id="Seg_6537" s="T423">"What is your riddle?</ta>
            <ta e="T433" id="Seg_6538" s="T425">If I use my mind, I might be able to guess it", said Laajku.</ta>
            <ta e="T441" id="Seg_6539" s="T433">"The riddle is: What is a cover for load made of?", the prince asked. </ta>
            <ta e="T448" id="Seg_6540" s="T441">"Maybe you can guess my thoughts?"</ta>
            <ta e="T451" id="Seg_6541" s="T448">"Skin is skin.</ta>
            <ta e="T460" id="Seg_6542" s="T451">I think it is made out of the skin of seven lice", Laajku said.</ta>
            <ta e="T464" id="Seg_6543" s="T460">"Oh, what a clever man you are?</ta>
            <ta e="T466" id="Seg_6544" s="T464">You've guessed it!</ta>
            <ta e="T474" id="Seg_6545" s="T466">Well, then it is your destiny to marry my daughter.</ta>
            <ta e="T478" id="Seg_6546" s="T474">Whatever you do, tomorrow we will celebrate the wedding."</ta>
            <ta e="T490" id="Seg_6547" s="T478">Laajku married the prince's beautiful daughter, he lived a rich life, ate his fill and didn't know any need.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T8" id="Seg_6548" s="T0">Es lebte einmal ein Hund mit dem Namen Laajku, der einsam war und der seine Eltern nicht erinnerte.</ta>
            <ta e="T13" id="Seg_6549" s="T8">Einmal ging er und suchte nach Menschen.</ta>
            <ta e="T18" id="Seg_6550" s="T13">Er ging und erblickte einen hohen Hügel.</ta>
            <ta e="T25" id="Seg_6551" s="T18">"Was wird sein, wenn ich dort hinaufklettere?", dachte er.</ta>
            <ta e="T34" id="Seg_6552" s="T25">Er kletterte auf den Hügel und sah, dass von der anderen Seite die Baba Jaga kam.</ta>
            <ta e="T40" id="Seg_6553" s="T34">Er kam zur Mitte des Hügels und setzte sich auf die Hinterbeine.</ta>
            <ta e="T45" id="Seg_6554" s="T40">Laajku war offenbar ein großes Werwesen.</ta>
            <ta e="T54" id="Seg_6555" s="T45">Nachdem er ein kleines Kind geworden war, rollte er sich vom Berg hinunter.</ta>
            <ta e="T57" id="Seg_6556" s="T54">"Oh, was für eine Sünde!</ta>
            <ta e="T70" id="Seg_6557" s="T57">Wie konnte ich nicht bemerken, dass ich ein Kind geboren habe?!", sagte sie, wickelte das Kind ein und trug es nach Hause.</ta>
            <ta e="T77" id="Seg_6558" s="T70">Die Baba Jaga lebte mit ihrem alten Mann am Ufer des Flusses.</ta>
            <ta e="T83" id="Seg_6559" s="T77">Sie hatten keine Kinder, sie hatten ein einsames gezähmtes Rentier.</ta>
            <ta e="T88" id="Seg_6560" s="T83">Das Kind wuchs mit jedem Tag, es fing an zu sprechen.</ta>
            <ta e="T93" id="Seg_6561" s="T88">Die Mutter nähte ihm einen Mantel und andere Kleidung.</ta>
            <ta e="T100" id="Seg_6562" s="T93">Dieses Kind fing an, um das Haus herum zu laufen.</ta>
            <ta e="T106" id="Seg_6563" s="T100">Der alte Mann ging offenbar jeden Tag mit dem Netz fischen.</ta>
            <ta e="T112" id="Seg_6564" s="T106">Sie ernährten dieses Kind offenbar nur mit Fisch.</ta>
            <ta e="T115" id="Seg_6565" s="T112">Laajku sagte einmal: </ta>
            <ta e="T120" id="Seg_6566" s="T115">"Mama, ich möchte Fleisch essen."</ta>
            <ta e="T128" id="Seg_6567" s="T120">"Alter Mann, töte eine unfruchtbare [Rentierkuh], das Kind möchte Fleisch essen."</ta>
            <ta e="T132" id="Seg_6568" s="T128">Der alte Mann tötete das Rentier.</ta>
            <ta e="T137" id="Seg_6569" s="T132">Das Verlangen des Laajku-Werwesens wuchs immer mehr.</ta>
            <ta e="T147" id="Seg_6570" s="T137">"Mama, warum fahren wir nicht auf die andere Seite des Flusses, dort ist es doch wohl trocken und eben", sagte es.</ta>
            <ta e="T153" id="Seg_6571" s="T147">"Nun, alter Mann, hörst du, was das Kind sagt?</ta>
            <ta e="T160" id="Seg_6572" s="T153">Es hat Recht, wie lange werden wir an einem Ort leben?!</ta>
            <ta e="T161" id="Seg_6573" s="T160">Fahren wir hinüber!"</ta>
            <ta e="T168" id="Seg_6574" s="T161">Der alte Mann sträubte sich nicht und fuhr die Seinen mit einem kleinen Boot auf die andere Seite.</ta>
            <ta e="T172" id="Seg_6575" s="T168">Laajku lief und bellte.</ta>
            <ta e="T182" id="Seg_6576" s="T172">Am Abend, als sie fertig mit dem Umzug waren, kam dieses Kind zu seiner Mutter und setzte sich ihr auf die Knie.</ta>
            <ta e="T186" id="Seg_6577" s="T182">"Mama, ich möchte [selber] Boot fahren."</ta>
            <ta e="T191" id="Seg_6578" s="T186">"Was soll das denn schon wieder?", ärgerte sich der Alte.</ta>
            <ta e="T201" id="Seg_6579" s="T191">Er setzte sich [und sagte]: "Nun gut, dann fahr halt Boot, nur fahr nicht weit hinaus, ja?"</ta>
            <ta e="T211" id="Seg_6580" s="T201">"Nein, ich fahre nicht weit, ich fahre nur am Ufer des Flusses", sagte das Laajku-Ungeheuer.</ta>
            <ta e="T219" id="Seg_6581" s="T211">Laajku setzte sich ins Boot und ruderte ganz leise am Ufer entlang.</ta>
            <ta e="T228" id="Seg_6582" s="T219">Als der Alte und die Alte ins Haus gegangen waren, entfernte er sich immer weiter.</ta>
            <ta e="T237" id="Seg_6583" s="T228">Als dieses Kind in der Mitte des Flusses angekommen waren, wurde es zu einem erwachsenen Menschen.</ta>
            <ta e="T242" id="Seg_6584" s="T237">Der Alte ging hinaus und sah, dass der Sohn schon weit weg war: </ta>
            <ta e="T245" id="Seg_6585" s="T242">"Hey, wohin fährst du?</ta>
            <ta e="T247" id="Seg_6586" s="T245">Ich sage, dass du nicht weit weg fahren sollst.</ta>
            <ta e="T249" id="Seg_6587" s="T247">Du fällst ins Wasser!"</ta>
            <ta e="T256" id="Seg_6588" s="T249">Laajku, schon in der Mitte des Flusses angekommen, schrie "haha".</ta>
            <ta e="T262" id="Seg_6589" s="T256">"Ich bin wohl Laajku und ihr habt das nicht bemerkt!"</ta>
            <ta e="T264" id="Seg_6590" s="T262">Laajku fuhr davon.</ta>
            <ta e="T268" id="Seg_6591" s="T264">Der Alte und die Alte fingen an zu kämpfen.</ta>
            <ta e="T272" id="Seg_6592" s="T268">Da ging dieser Junge davon.</ta>
            <ta e="T281" id="Seg_6593" s="T272">Nach einiger Zeit sah er, dass dort Häuser standen, dass dort angespannte Rentiere lagen.</ta>
            <ta e="T287" id="Seg_6594" s="T281">Er ging in ein Haus hinein, dort sitzt eine einsame Frau.</ta>
            <ta e="T289" id="Seg_6595" s="T287">Laajku trank Tee.</ta>
            <ta e="T295" id="Seg_6596" s="T289">"Was ist das hier, eine Versammlung oder eine Hochzeit?"</ta>
            <ta e="T296" id="Seg_6597" s="T295">"Nein.</ta>
            <ta e="T311" id="Seg_6598" s="T296">Unser Fürst hat eine Tochter, der Mann dieser Tochter wird nur derjenige, der ein Rätsel des Fürsten gelöst hat", sagte die Frau.</ta>
            <ta e="T321" id="Seg_6599" s="T311">Laajku ging hinaus und ging ans Flussufer, er legte sich hin und bedeckte sich mit Gras.</ta>
            <ta e="T332" id="Seg_6600" s="T321">Er sah, dass zwei Mädchen mit einem Schulterjoch nach Wasser gingen, sie trugen zwei Behälter auf ihrem Rücken.</ta>
            <ta e="T339" id="Seg_6601" s="T332">Ein reiches Mädchen stolperte über die Beine des Jungen und verschüttete ihr Wasser: </ta>
            <ta e="T353" id="Seg_6602" s="T339">"Das ist alles nur wegen meines Vaters, wenn ich verheiratet wäre, müsste ich mich wohl kaum abmühen und Wasser schleppen?!</ta>
            <ta e="T356" id="Seg_6603" s="T353">Ich habe nur meine Beine nass gemacht."</ta>
            <ta e="T361" id="Seg_6604" s="T356">"Meine Freundin, was lässt dein Vater denn erraten?"</ta>
            <ta e="T370" id="Seg_6605" s="T361">"Einmal hatte mein Vater eine Lastendecke aus der Haut von sieben Läusen gemacht.</ta>
            <ta e="T374" id="Seg_6606" s="T370">Das lässt er wohl raten.</ta>
            <ta e="T376" id="Seg_6607" s="T374">Nun, gehen wir.</ta>
            <ta e="T384" id="Seg_6608" s="T376">Weder Wasser noch Holz für so viele Gäste kann man aushalten."</ta>
            <ta e="T391" id="Seg_6609" s="T384">Laajku kehrte zurück und ging in dieses Haus hinein, er übernachtete.</ta>
            <ta e="T396" id="Seg_6610" s="T391">Es wurde Morgen und es war wieder voll von Gästen.</ta>
            <ta e="T405" id="Seg_6611" s="T396">Der Junge wollte auch zum Fürsten gehen, gemeinsam mit den Gästen ging er hinein.</ta>
            <ta e="T410" id="Seg_6612" s="T405">Er sah, dass das Haus voller Männer war.</ta>
            <ta e="T415" id="Seg_6613" s="T410">"Oh, heute ist ein neuer Gast gekommen.</ta>
            <ta e="T423" id="Seg_6614" s="T415">Nun, vielleicht kannst du mein Rätsel lösen?", sagte der Fürst.</ta>
            <ta e="T425" id="Seg_6615" s="T423">"Was für ein Rätsel hast du?</ta>
            <ta e="T433" id="Seg_6616" s="T425">Wenn ich meinen Verstand bemühe, kann ich es vielleicht erraten", sagte Laajku.</ta>
            <ta e="T441" id="Seg_6617" s="T433">"Ich lasse raten: Woraus ist eine Lastendecke gemacht?", fragte der Fürst.</ta>
            <ta e="T448" id="Seg_6618" s="T441">"Vielleicht kannst du den Gang meiner Gedanken nachvollziehen?"</ta>
            <ta e="T451" id="Seg_6619" s="T448">"Haut ist Haut.</ta>
            <ta e="T460" id="Seg_6620" s="T451">Meiner Meinung nach ist sie aus der Haut von sieben Läusen gemacht", sprach Laajku.</ta>
            <ta e="T464" id="Seg_6621" s="T460">"Oh, was bist du für ein kluger Mensch?</ta>
            <ta e="T466" id="Seg_6622" s="T464">Du hast es erraten.</ta>
            <ta e="T474" id="Seg_6623" s="T466">Nun, dann ist es dir wohl beschieden, meine Tochter zu heiraten.</ta>
            <ta e="T478" id="Seg_6624" s="T474">Was du auch machst, morgen werden wir Hochzeit feiern."</ta>
            <ta e="T490" id="Seg_6625" s="T478">Laajku heiratete die schöne Tochter des reichen Fürsten, lebte in Reichtum und Wohlstand und kannte keine Not.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T8" id="Seg_6626" s="T0">Жил одинокий пес по имени Лаайку, родителей своих не помнил.</ta>
            <ta e="T13" id="Seg_6627" s="T8">Однажды ходил искал людей.</ta>
            <ta e="T18" id="Seg_6628" s="T13">Так бродя, увидел высокую сопку.</ta>
            <ta e="T25" id="Seg_6629" s="T18">"Что будет, если взберусь на нее?", подумал.</ta>
            <ta e="T34" id="Seg_6630" s="T25">Взобравшись на сопку, увидел: с той стороны идет Джигэ-баба.</ta>
            <ta e="T40" id="Seg_6631" s="T34">До середины сопки дойдя, присела.</ta>
            <ta e="T45" id="Seg_6632" s="T40">Лаайку был, оказывается, большим оборотнем.</ta>
            <ta e="T54" id="Seg_6633" s="T45">Превратившись в маленького ребенка, скатился вниз с сопки.</ta>
            <ta e="T57" id="Seg_6634" s="T54">"О, грех какой!</ta>
            <ta e="T70" id="Seg_6635" s="T57">Как не заметила, что родила ребенка?!", сказала [Джигэ-баба], укутала ребенка и понесла домой.</ta>
            <ta e="T77" id="Seg_6636" s="T70">Джигэ-баба со своим стариком, оказывается, жили на берегу реки-бабушки.</ta>
            <ta e="T83" id="Seg_6637" s="T77">Детей у них не было, имели единственного ручного оленя.</ta>
            <ta e="T88" id="Seg_6638" s="T83">Ребенок рос с каждым днем, начал говорить.</ta>
            <ta e="T93" id="Seg_6639" s="T88">Мать сшила ему сокуй и другую одежду.</ta>
            <ta e="T100" id="Seg_6640" s="T93">Этот ребенок стал уже бегать около дома.</ta>
            <ta e="T106" id="Seg_6641" s="T100">Старик, оказывается, обычно рыбачил, забрасывая сети.</ta>
            <ta e="T112" id="Seg_6642" s="T106">Кормили этого ребенка только рыбой.</ta>
            <ta e="T115" id="Seg_6643" s="T112">Лаайку однажды сказал: </ta>
            <ta e="T120" id="Seg_6644" s="T115">"Мама, хочу поесть мяса."</ta>
            <ta e="T128" id="Seg_6645" s="T120">"Старик, забивай яловую важенку, ребенок хочет мяса."</ta>
            <ta e="T132" id="Seg_6646" s="T128">Старик забил оленя.</ta>
            <ta e="T137" id="Seg_6647" s="T132">У Лаайку-оборотня желания все росли.</ta>
            <ta e="T147" id="Seg_6648" s="T137">"Мама, почему мы на тот берег реки-бабушки не переезжаем, там, кажется, ровно и сухо", сказал.</ta>
            <ta e="T153" id="Seg_6649" s="T147">"Старик, слышишь, что говорит ребенок?</ta>
            <ta e="T160" id="Seg_6650" s="T153">Правду говорит, сколько будем жить на одном месте?!</ta>
            <ta e="T161" id="Seg_6651" s="T160">Переедем."</ta>
            <ta e="T168" id="Seg_6652" s="T161">Старик, не противясь, на лодке перевез своих на тот берег.</ta>
            <ta e="T172" id="Seg_6653" s="T168">Лаайку с лаем бегал.</ta>
            <ta e="T182" id="Seg_6654" s="T172">К вечеру, когда кончили переправляться, этот ребенок подошел к матери и сел к ней на колени.</ta>
            <ta e="T186" id="Seg_6655" s="T182">"Мама, хочу покататься на лодке."</ta>
            <ta e="T191" id="Seg_6656" s="T186">"Это еще что?", рассердился старик.</ta>
            <ta e="T201" id="Seg_6657" s="T191">Сел [и сказал]: "Ну, ладно, покатайся на лодке, только не отплывай далеко."</ta>
            <ta e="T211" id="Seg_6658" s="T201">"Нет, не отплыву, около берега реки-бабушки покатаюсь", сказал Лаайку-чудище.</ta>
            <ta e="T219" id="Seg_6659" s="T211">Лаайку, сев в лодку, потихоньку греб вдоль берега.</ta>
            <ta e="T228" id="Seg_6660" s="T219">Как только старик со старухой вошли в дом, стал все удаляться и удаляться.</ta>
            <ta e="T237" id="Seg_6661" s="T228">Когда доплыл до середины реки-бабушки, ребенок превратился во взрослого человека.</ta>
            <ta e="T242" id="Seg_6662" s="T237">Вышел старик и увидел, что сын уже далеко: </ta>
            <ta e="T245" id="Seg_6663" s="T242">"Что это, куда плывешь?</ta>
            <ta e="T247" id="Seg_6664" s="T245">Не уплывай далеко, говорю.</ta>
            <ta e="T249" id="Seg_6665" s="T247">Упадешь в воду!"</ta>
            <ta e="T256" id="Seg_6666" s="T249">Лаайку, доплыв до середины реки, закричал "ха-ха".</ta>
            <ta e="T262" id="Seg_6667" s="T256">"Я же Лаайку, а вы об этом не догадались!"</ta>
            <ta e="T264" id="Seg_6668" s="T262">Лаайку убежал.</ta>
            <ta e="T268" id="Seg_6669" s="T264">Старик со старухой остались драться.</ta>
            <ta e="T272" id="Seg_6670" s="T268">Вот ушел этот парень.</ta>
            <ta e="T281" id="Seg_6671" s="T272">Через какое-то время видит: стоят дома, лежат привязанные олени.</ta>
            <ta e="T287" id="Seg_6672" s="T281">Вошел в один дом, сидит одинокая женщина.</ta>
            <ta e="T289" id="Seg_6673" s="T287">Лаайку стал пить чай.</ta>
            <ta e="T295" id="Seg_6674" s="T289">"Что здесь, сход ли, свадьба ли?"</ta>
            <ta e="T296" id="Seg_6675" s="T295">"Нет.</ta>
            <ta e="T311" id="Seg_6676" s="T296">У нашего князька есть дочка, мужем той девушки станет только тот человек, который отгадает загадку князька", сказала женщина.</ta>
            <ta e="T321" id="Seg_6677" s="T311">Лаайку вышел, на берегу реки-бабушки, прикрывшись травой, улегся.</ta>
            <ta e="T332" id="Seg_6678" s="T321">Видел: идут две девушки по воду; с коромыслами, два бачка на плечах несут.</ta>
            <ta e="T339" id="Seg_6679" s="T332">Споткнувшись о ногу парня, богатая девушка расплескала воду: </ta>
            <ta e="T353" id="Seg_6680" s="T339">"Это все из-за отца, если б вышла замуж, разве мучилась бы, таская воду?!</ta>
            <ta e="T356" id="Seg_6681" s="T353">Только ноги облила."</ta>
            <ta e="T361" id="Seg_6682" s="T356">"Подружка, отец твой что же загадывает?"</ta>
            <ta e="T370" id="Seg_6683" s="T361">— Когда-то отец сшил для вьюка покрышку из шкур семи вшей.</ta>
            <ta e="T374" id="Seg_6684" s="T370">Наверное это загадывает.</ta>
            <ta e="T376" id="Seg_6685" s="T374">Ну, пошли.</ta>
            <ta e="T384" id="Seg_6686" s="T376">И воды не напасешься, и дров не напасешься для стольких гостей."</ta>
            <ta e="T391" id="Seg_6687" s="T384">Лаайку возвратился, вошел в тот же дом, заночевал.</ta>
            <ta e="T396" id="Seg_6688" s="T391">Утром встал — опять гостей полно.</ta>
            <ta e="T405" id="Seg_6689" s="T396">Этот парень тоже решил к князьку пойти, с гостями вместе вошел.</ta>
            <ta e="T410" id="Seg_6690" s="T405">Видел: полный дом мужчин.</ta>
            <ta e="T415" id="Seg_6691" s="T410">"О, сегодня новый гость появился.</ta>
            <ta e="T423" id="Seg_6692" s="T415">Ну, может, ты отгадаешь мою загадку?", сказал князек.</ta>
            <ta e="T425" id="Seg_6693" s="T423">"Какая у тебя загадка?</ta>
            <ta e="T433" id="Seg_6694" s="T425">Если разумом одолею, попробую отгадать", сказал Лаайку.</ta>
            <ta e="T441" id="Seg_6695" s="T433">"Я загадываю: из чего сшит чехол для вьюка?", спросил князек.</ta>
            <ta e="T448" id="Seg_6696" s="T441">"Можешь отгадать ход моих мыслей?"</ta>
            <ta e="T451" id="Seg_6697" s="T448">"Шкура есть шкура.</ta>
            <ta e="T460" id="Seg_6698" s="T451">По-моему, видать, из шкур семи вшей", проговорил Лаайку.</ta>
            <ta e="T464" id="Seg_6699" s="T460">"О, какой ты умный человек?</ta>
            <ta e="T466" id="Seg_6700" s="T464">Разгадал же!</ta>
            <ta e="T474" id="Seg_6701" s="T466">Ну, значит тебе суждено жениться на моей дочери.</ta>
            <ta e="T478" id="Seg_6702" s="T474">Что поделаешь, завтра сыграем свадьбу."</ta>
            <ta e="T490" id="Seg_6703" s="T478">Лаайку, женившись на красивой дочери богатого князька, зажил в богатстве и довольстве, не зная нужды.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T8" id="Seg_6704" s="T0">Жил одинокий пес по имени Лаайку, отца-матери не помнил.</ta>
            <ta e="T13" id="Seg_6705" s="T8">Однажды ходил искал людей.</ta>
            <ta e="T18" id="Seg_6706" s="T13">Так бродя, увидел высокую сопку.</ta>
            <ta e="T25" id="Seg_6707" s="T18">"Что будет, если взберусь на нее?" — подумал.</ta>
            <ta e="T34" id="Seg_6708" s="T25">Взобравшись на сопку, увидел: с той стороны идет Джигэ-баба.</ta>
            <ta e="T40" id="Seg_6709" s="T34">До середины сопки дойдя, присела.</ta>
            <ta e="T45" id="Seg_6710" s="T40">Лаайку был, оказывается, большим оборотнем.</ta>
            <ta e="T54" id="Seg_6711" s="T45">Превратившись в ребенка, скатился вниз с сопки.</ta>
            <ta e="T57" id="Seg_6712" s="T54">"О, грех какой!</ta>
            <ta e="T70" id="Seg_6713" s="T57">Как не заметила, что родила ребенка?!" — сказала [Джигэ-баба], укутала ребенка и понесла домой.</ta>
            <ta e="T77" id="Seg_6714" s="T70">Джигэ-баба со своим стариком, оказывается, жили на берегу реки-бабушки.</ta>
            <ta e="T83" id="Seg_6715" s="T77">Детей у них не было, имели единственного ручного оленя.</ta>
            <ta e="T88" id="Seg_6716" s="T83">Мальчик рос с каждым днем. Начал говорить.</ta>
            <ta e="T93" id="Seg_6717" s="T88">Мать сшила ему сокуй с капюшоном.</ta>
            <ta e="T100" id="Seg_6718" s="T93">Этот ребенок стал уже бегать около дома.</ta>
            <ta e="T106" id="Seg_6719" s="T100">Старик, оказывается, обычно рыбачил, забрасывая сети.</ta>
            <ta e="T112" id="Seg_6720" s="T106">Кормили этого ребенка только рыбой.</ta>
            <ta e="T115" id="Seg_6721" s="T112">Лаайку однажды сказал: </ta>
            <ta e="T120" id="Seg_6722" s="T115">— Мама, хочу поесть мяса.</ta>
            <ta e="T128" id="Seg_6723" s="T120">— Старик, давай забьем яловую важенку, ребенок хочет мяса.</ta>
            <ta e="T132" id="Seg_6724" s="T128">Старик забил оленя.</ta>
            <ta e="T137" id="Seg_6725" s="T132">У Лаайку-оборотня желания все росли.</ta>
            <ta e="T147" id="Seg_6726" s="T137">— Мама, почему мы на тот берег реки-бабушки не переезжаем, там, кажется, ровно и сухо, — сказал.</ta>
            <ta e="T153" id="Seg_6727" s="T147">— Старик, слышишь, что говорит ребенок?</ta>
            <ta e="T160" id="Seg_6728" s="T153">Правду говорит, сколько будем жить на одном месте?!</ta>
            <ta e="T161" id="Seg_6729" s="T160">Переедем.</ta>
            <ta e="T168" id="Seg_6730" s="T161">Старик, не противясь, на лодке перевез своих на тот берег.</ta>
            <ta e="T172" id="Seg_6731" s="T168">Лаайку с лаем бегал. </ta>
            <ta e="T182" id="Seg_6732" s="T172">К вечеру, когда кончили переправляться, этот ребенок подошел к матери и сел к ней на колени.</ta>
            <ta e="T186" id="Seg_6733" s="T182">— Мама, хочу покататься на лодке.</ta>
            <ta e="T191" id="Seg_6734" s="T186">— Это еще что? — рассердился старик.</ta>
            <ta e="T201" id="Seg_6735" s="T191">Немного подумав: — Ну, ладно, покатайся на лодке, только не отплывай далеко.</ta>
            <ta e="T211" id="Seg_6736" s="T201">— Нет, не отплыву, около берега реки-бабушки покатаюсь, — сказал Лаайку-чудище.</ta>
            <ta e="T219" id="Seg_6737" s="T211">Лаайку, сев в лодку, потихоньку греб вдоль берега.</ta>
            <ta e="T228" id="Seg_6738" s="T219">Как только старик со старухой вошли в дом, стал все удаляться и удаляться.</ta>
            <ta e="T237" id="Seg_6739" s="T228">Когда доплыл до середины реки-бабушки, ребенок превратился во взрослого человека.</ta>
            <ta e="T242" id="Seg_6740" s="T237">Вышел старик и видит: сын уже далеко. </ta>
            <ta e="T245" id="Seg_6741" s="T242">— Что это, куда плывешь?</ta>
            <ta e="T247" id="Seg_6742" s="T245">Не уплывай далеко, говорю.</ta>
            <ta e="T249" id="Seg_6743" s="T247">Упадешь в воду!</ta>
            <ta e="T256" id="Seg_6744" s="T249">Лаайку, доплыв до середины реки, закричал: — Ха-ха!</ta>
            <ta e="T262" id="Seg_6745" s="T256">Я же Лаайку, а вы об этом не догадались!</ta>
            <ta e="T264" id="Seg_6746" s="T262">Лаайку убежал.</ta>
            <ta e="T268" id="Seg_6747" s="T264">Старик со старухой остались драться.</ta>
            <ta e="T272" id="Seg_6748" s="T268">Вот идет этот парень.</ta>
            <ta e="T281" id="Seg_6749" s="T272">Через какое-то время видит: стоят дома, лежат привязанные олени.</ta>
            <ta e="T287" id="Seg_6750" s="T281">Вошел в один дом, сидит одинокая женщине.</ta>
            <ta e="T289" id="Seg_6751" s="T287">Лаайку стал пить чай.</ta>
            <ta e="T295" id="Seg_6752" s="T289">— Что здесь, сход ли, свадьба ли?</ta>
            <ta e="T296" id="Seg_6753" s="T295">— Нет.</ta>
            <ta e="T311" id="Seg_6754" s="T296">У нашего князька есть единственная дочка, мужем той девушки станет только тот человек, который отгадает загадку князька, — сказала женщина.</ta>
            <ta e="T321" id="Seg_6755" s="T311">Лаайку вышел, на берегу реки-бабушки, прикрывшись травой, улегся.</ta>
            <ta e="T332" id="Seg_6756" s="T321">Видит: идут две девушки по воду; с коромыслами, два бачка на плечах несут.</ta>
            <ta e="T339" id="Seg_6757" s="T332">Споткнувшись о ногу парня, богатая девушка расплескала воду. </ta>
            <ta e="T353" id="Seg_6758" s="T339">— Это все из-за отца, если б вышла замуж, разве мучилась бы, таская воду?!</ta>
            <ta e="T356" id="Seg_6759" s="T353">Только ноги облила.</ta>
            <ta e="T361" id="Seg_6760" s="T356">— Подружка, отец твой что же загадывает?</ta>
            <ta e="T370" id="Seg_6761" s="T361">— Когда-то отец сшил для вьюка покрышку из шкур семи вшей.</ta>
            <ta e="T374" id="Seg_6762" s="T370">Наверное это загадывает.</ta>
            <ta e="T376" id="Seg_6763" s="T374">Ну, пошли.</ta>
            <ta e="T384" id="Seg_6764" s="T376">И воды не напасешься, и дров не напасешься для стольких гостей.</ta>
            <ta e="T391" id="Seg_6765" s="T384">Лаайку возвратился, вошел в тот же дом, заночевал.</ta>
            <ta e="T396" id="Seg_6766" s="T391">Утром встал — опять гостей полно.</ta>
            <ta e="T405" id="Seg_6767" s="T396">Этот парень тоже решил к князьку пойти, с гостями вместе вошел.</ta>
            <ta e="T410" id="Seg_6768" s="T405">Видит: полный дом мужчин.</ta>
            <ta e="T415" id="Seg_6769" s="T410">— О, сегодня новый гость появился.</ta>
            <ta e="T423" id="Seg_6770" s="T415">Ну, может, ты отгадаешь мою загадку? — сказал князек.</ta>
            <ta e="T425" id="Seg_6771" s="T423">— Какая у тебя загадка?</ta>
            <ta e="T433" id="Seg_6772" s="T425">Если разумом одолею, попробую отгадать, — сказал Лаайку.</ta>
            <ta e="T441" id="Seg_6773" s="T433">— Я загадываю: из чего сшит чехол для вьюка? — спросил князек.</ta>
            <ta e="T448" id="Seg_6774" s="T441">— Можешь отгадать ход моих мыслей?</ta>
            <ta e="T451" id="Seg_6775" s="T448">— Шкура есть шкура.</ta>
            <ta e="T460" id="Seg_6776" s="T451">По-моему, видать, из шкур семи вшей, — проговорил Лаайку.</ta>
            <ta e="T464" id="Seg_6777" s="T460">— О, какой ты умный человек?</ta>
            <ta e="T466" id="Seg_6778" s="T464">Разгадал же!</ta>
            <ta e="T474" id="Seg_6779" s="T466">Ну, значит тебе суждено жениться на моей дочери.</ta>
            <ta e="T478" id="Seg_6780" s="T474">Что поделаешь, завтра сыграем свадьбу.</ta>
            <ta e="T490" id="Seg_6781" s="T478">Лаайку, женившись на красивой дочери богатого князька, зажил в богатстве и довольстве, не зная нужды.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T54" id="Seg_6782" s="T45">[DCh]: "hahɨl ogo", lit. 'fox child' means a small child.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg"
                          name="gg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top"
                          name="Top"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc"
                          name="Foc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
