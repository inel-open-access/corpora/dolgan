<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID47FD28BE-C562-B5C3-96F1-2D490B4F89B2">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>BaRD_1930_ChildOfMasterOfMountain_flk</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\flk\BaRD_1930_ChildOfMasterOfMountain_flk\BaRD_1930_ChildOfMasterOfMountain_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">169</ud-information>
            <ud-information attribute-name="# HIAT:w">129</ud-information>
            <ud-information attribute-name="# e">129</ud-information>
            <ud-information attribute-name="# HIAT:u">20</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="BaRD">
            <abbreviation>BaRD</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="4.0" type="appl" />
         <tli id="T1" time="4.5" type="appl" />
         <tli id="T2" time="5.0" type="appl" />
         <tli id="T3" time="5.5" type="appl" />
         <tli id="T4" time="6.0" type="appl" />
         <tli id="T5" time="6.5" type="appl" />
         <tli id="T6" time="7.0" type="appl" />
         <tli id="T7" time="7.5" type="appl" />
         <tli id="T8" time="8.0" type="appl" />
         <tli id="T9" time="8.5" type="appl" />
         <tli id="T10" time="9.0" type="appl" />
         <tli id="T11" time="9.5" type="appl" />
         <tli id="T12" time="10.0" type="appl" />
         <tli id="T13" time="10.5" type="appl" />
         <tli id="T14" time="11.0" type="appl" />
         <tli id="T15" time="11.5" type="appl" />
         <tli id="T16" time="12.0" type="appl" />
         <tli id="T17" time="12.5" type="appl" />
         <tli id="T18" time="13.0" type="appl" />
         <tli id="T19" time="13.5" type="appl" />
         <tli id="T20" time="14.0" type="appl" />
         <tli id="T21" time="14.5" type="appl" />
         <tli id="T22" time="15.0" type="appl" />
         <tli id="T23" time="15.5" type="appl" />
         <tli id="T24" time="16.0" type="appl" />
         <tli id="T25" time="16.5" type="appl" />
         <tli id="T26" time="17.0" type="appl" />
         <tli id="T27" time="17.5" type="appl" />
         <tli id="T28" time="18.0" type="appl" />
         <tli id="T29" time="18.5" type="appl" />
         <tli id="T30" time="19.0" type="appl" />
         <tli id="T31" time="19.5" type="appl" />
         <tli id="T32" time="20.0" type="appl" />
         <tli id="T33" time="20.5" type="appl" />
         <tli id="T34" time="21.0" type="appl" />
         <tli id="T35" time="21.5" type="appl" />
         <tli id="T36" time="22.0" type="appl" />
         <tli id="T37" time="22.5" type="appl" />
         <tli id="T38" time="23.0" type="appl" />
         <tli id="T39" time="23.5" type="appl" />
         <tli id="T40" time="24.0" type="appl" />
         <tli id="T41" time="24.5" type="appl" />
         <tli id="T42" time="25.0" type="appl" />
         <tli id="T43" time="25.5" type="appl" />
         <tli id="T44" time="26.0" type="appl" />
         <tli id="T45" time="26.5" type="appl" />
         <tli id="T46" time="27.0" type="appl" />
         <tli id="T47" time="27.5" type="appl" />
         <tli id="T48" time="28.0" type="appl" />
         <tli id="T49" time="28.5" type="appl" />
         <tli id="T50" time="29.0" type="appl" />
         <tli id="T51" time="29.5" type="appl" />
         <tli id="T52" time="30.0" type="appl" />
         <tli id="T53" time="30.5" type="appl" />
         <tli id="T54" time="31.0" type="appl" />
         <tli id="T55" time="31.5" type="appl" />
         <tli id="T56" time="32.0" type="appl" />
         <tli id="T57" time="32.5" type="appl" />
         <tli id="T58" time="33.0" type="appl" />
         <tli id="T59" time="33.5" type="appl" />
         <tli id="T60" time="34.0" type="appl" />
         <tli id="T61" time="34.5" type="appl" />
         <tli id="T62" time="35.0" type="appl" />
         <tli id="T63" time="35.5" type="appl" />
         <tli id="T64" time="36.0" type="appl" />
         <tli id="T65" time="36.5" type="appl" />
         <tli id="T66" time="37.0" type="appl" />
         <tli id="T67" time="37.5" type="appl" />
         <tli id="T68" time="38.0" type="appl" />
         <tli id="T69" time="38.5" type="appl" />
         <tli id="T70" time="39.0" type="appl" />
         <tli id="T71" time="39.5" type="appl" />
         <tli id="T72" time="40.0" type="appl" />
         <tli id="T73" time="40.5" type="appl" />
         <tli id="T74" time="41.0" type="appl" />
         <tli id="T75" time="41.5" type="appl" />
         <tli id="T76" time="42.0" type="appl" />
         <tli id="T77" time="42.5" type="appl" />
         <tli id="T78" time="43.0" type="appl" />
         <tli id="T79" time="43.5" type="appl" />
         <tli id="T80" time="44.0" type="appl" />
         <tli id="T81" time="44.5" type="appl" />
         <tli id="T82" time="45.0" type="appl" />
         <tli id="T83" time="45.5" type="appl" />
         <tli id="T84" time="46.0" type="appl" />
         <tli id="T85" time="46.5" type="appl" />
         <tli id="T86" time="47.0" type="appl" />
         <tli id="T87" time="47.5" type="appl" />
         <tli id="T88" time="48.0" type="appl" />
         <tli id="T89" time="48.5" type="appl" />
         <tli id="T90" time="49.0" type="appl" />
         <tli id="T91" time="49.5" type="appl" />
         <tli id="T92" time="50.0" type="appl" />
         <tli id="T93" time="50.5" type="appl" />
         <tli id="T94" time="51.0" type="appl" />
         <tli id="T95" time="51.5" type="appl" />
         <tli id="T96" time="52.0" type="appl" />
         <tli id="T97" time="52.5" type="appl" />
         <tli id="T98" time="53.0" type="appl" />
         <tli id="T99" time="53.5" type="appl" />
         <tli id="T100" time="54.0" type="appl" />
         <tli id="T101" time="54.5" type="appl" />
         <tli id="T102" time="55.0" type="appl" />
         <tli id="T103" time="55.5" type="appl" />
         <tli id="T104" time="56.0" type="appl" />
         <tli id="T105" time="56.5" type="appl" />
         <tli id="T106" time="57.0" type="appl" />
         <tli id="T107" time="57.5" type="appl" />
         <tli id="T108" time="58.0" type="appl" />
         <tli id="T109" time="58.5" type="appl" />
         <tli id="T110" time="59.0" type="appl" />
         <tli id="T111" time="59.5" type="appl" />
         <tli id="T112" time="60.0" type="appl" />
         <tli id="T113" time="60.5" type="appl" />
         <tli id="T114" time="61.0" type="appl" />
         <tli id="T115" time="61.5" type="appl" />
         <tli id="T116" time="62.0" type="appl" />
         <tli id="T117" time="62.5" type="appl" />
         <tli id="T118" time="63.0" type="appl" />
         <tli id="T119" time="63.5" type="appl" />
         <tli id="T120" time="64.0" type="appl" />
         <tli id="T121" time="64.5" type="appl" />
         <tli id="T122" time="65.0" type="appl" />
         <tli id="T123" time="65.5" type="appl" />
         <tli id="T124" time="66.0" type="appl" />
         <tli id="T125" time="66.5" type="appl" />
         <tli id="T126" time="67.0" type="appl" />
         <tli id="T127" time="67.5" type="appl" />
         <tli id="T128" time="68.0" type="appl" />
         <tli id="T129" time="68.5" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="BaRD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T129" id="Seg_0" n="sc" s="T0">
               <ts e="T8" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Bɨlɨr</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">ogonnʼordoːk</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">emeːksin</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">hogotokkoːn</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">olorbuttar</ts>
                  <nts id="Seg_17" n="HIAT:ip">,</nts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_20" n="HIAT:w" s="T5">bi͡ek</ts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_23" n="HIAT:w" s="T6">ogoto</ts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_26" n="HIAT:w" s="T7">hu͡oktar</ts>
                  <nts id="Seg_27" n="HIAT:ip">.</nts>
                  <nts id="Seg_28" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T13" id="Seg_30" n="HIAT:u" s="T8">
                  <ts e="T9" id="Seg_32" n="HIAT:w" s="T8">Biːrde</ts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_35" n="HIAT:w" s="T9">turan</ts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_38" n="HIAT:w" s="T10">ogonnʼor</ts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_41" n="HIAT:w" s="T11">eppit</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_44" n="HIAT:w" s="T12">emeːksiniger</ts>
                  <nts id="Seg_45" n="HIAT:ip">:</nts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_48" n="HIAT:u" s="T13">
                  <nts id="Seg_49" n="HIAT:ip">"</nts>
                  <ts e="T14" id="Seg_51" n="HIAT:w" s="T13">Emeːksin</ts>
                  <nts id="Seg_52" n="HIAT:ip">,</nts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_55" n="HIAT:w" s="T14">bihigi</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_58" n="HIAT:w" s="T15">ogolonu͡okputugar</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_61" n="HIAT:w" s="T16">bert</ts>
                  <nts id="Seg_62" n="HIAT:ip">.</nts>
                  <nts id="Seg_63" n="HIAT:ip">"</nts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_66" n="HIAT:u" s="T17">
                  <ts e="T18" id="Seg_68" n="HIAT:w" s="T17">Onuga</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_71" n="HIAT:w" s="T18">emeːksine</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_74" n="HIAT:w" s="T19">eppit</ts>
                  <nts id="Seg_75" n="HIAT:ip">:</nts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T25" id="Seg_78" n="HIAT:u" s="T20">
                  <ts e="T21" id="Seg_80" n="HIAT:w" s="T20">Hiŋilbitiger</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_83" n="HIAT:w" s="T21">ogolommotokput</ts>
                  <nts id="Seg_84" n="HIAT:ip">,</nts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_87" n="HIAT:w" s="T22">kajdak</ts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_90" n="HIAT:w" s="T23">biligin</ts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_93" n="HIAT:w" s="T24">ogolonu͡okputuj</ts>
                  <nts id="Seg_94" n="HIAT:ip">?</nts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T29" id="Seg_97" n="HIAT:u" s="T25">
                  <ts e="T26" id="Seg_99" n="HIAT:w" s="T25">Ol</ts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_102" n="HIAT:w" s="T26">hatanɨ͡a</ts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_105" n="HIAT:w" s="T27">du͡o</ts>
                  <nts id="Seg_106" n="HIAT:ip">"</nts>
                  <nts id="Seg_107" n="HIAT:ip">,</nts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_110" n="HIAT:w" s="T28">di͡ebit</ts>
                  <nts id="Seg_111" n="HIAT:ip">.</nts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T32" id="Seg_114" n="HIAT:u" s="T29">
                  <ts e="T30" id="Seg_116" n="HIAT:w" s="T29">Onu</ts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_119" n="HIAT:w" s="T30">ogonnʼoro</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_122" n="HIAT:w" s="T31">eppit</ts>
                  <nts id="Seg_123" n="HIAT:ip">:</nts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T39" id="Seg_126" n="HIAT:u" s="T32">
                  <nts id="Seg_127" n="HIAT:ip">"</nts>
                  <ts e="T33" id="Seg_129" n="HIAT:w" s="T32">Min</ts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_132" n="HIAT:w" s="T33">kihi</ts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_135" n="HIAT:w" s="T34">bularɨttan</ts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_138" n="HIAT:w" s="T35">atɨnnɨk</ts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_141" n="HIAT:w" s="T36">ogolonoːru</ts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_144" n="HIAT:w" s="T37">gɨnabɨn</ts>
                  <nts id="Seg_145" n="HIAT:ip">"</nts>
                  <nts id="Seg_146" n="HIAT:ip">,</nts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_149" n="HIAT:w" s="T38">di͡ebit</ts>
                  <nts id="Seg_150" n="HIAT:ip">.</nts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T41" id="Seg_153" n="HIAT:u" s="T39">
                  <ts e="T40" id="Seg_155" n="HIAT:w" s="T39">Emeːksine</ts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_158" n="HIAT:w" s="T40">di͡ebit</ts>
                  <nts id="Seg_159" n="HIAT:ip">:</nts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T45" id="Seg_162" n="HIAT:u" s="T41">
                  <nts id="Seg_163" n="HIAT:ip">"</nts>
                  <ts e="T42" id="Seg_165" n="HIAT:w" s="T41">Ogolonorgo</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_168" n="HIAT:w" s="T42">tu͡ok</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_171" n="HIAT:w" s="T43">kuhagana</ts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_174" n="HIAT:w" s="T44">bu͡olu͡oj</ts>
                  <nts id="Seg_175" n="HIAT:ip">?</nts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T49" id="Seg_178" n="HIAT:u" s="T45">
                  <ts e="T46" id="Seg_180" n="HIAT:w" s="T45">Onnuk</ts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_183" n="HIAT:w" s="T46">bu͡ollagɨna</ts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_186" n="HIAT:w" s="T47">terinnegiŋ</ts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_189" n="HIAT:w" s="T48">diː</ts>
                  <nts id="Seg_190" n="HIAT:ip">.</nts>
                  <nts id="Seg_191" n="HIAT:ip">"</nts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T64" id="Seg_194" n="HIAT:u" s="T49">
                  <ts e="T50" id="Seg_196" n="HIAT:w" s="T49">Manɨ͡aga</ts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_199" n="HIAT:w" s="T50">ogonnʼoro</ts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_202" n="HIAT:w" s="T51">ilimi</ts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_205" n="HIAT:w" s="T52">ɨlbɨt</ts>
                  <nts id="Seg_206" n="HIAT:ip">,</nts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_209" n="HIAT:w" s="T53">kabɨjakaːŋŋa</ts>
                  <nts id="Seg_210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_212" n="HIAT:w" s="T54">turu͡orallarɨn</ts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_215" n="HIAT:w" s="T55">kurduk</ts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_218" n="HIAT:w" s="T56">kɨtɨlga</ts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_221" n="HIAT:w" s="T57">tu͡ora</ts>
                  <nts id="Seg_222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_224" n="HIAT:w" s="T58">mastaːn</ts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_227" n="HIAT:w" s="T59">turu͡oran</ts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_230" n="HIAT:w" s="T60">keːspit</ts>
                  <nts id="Seg_231" n="HIAT:ip">,</nts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_234" n="HIAT:w" s="T61">kaja</ts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_237" n="HIAT:w" s="T62">di͡eg</ts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_240" n="HIAT:w" s="T63">annʼan</ts>
                  <nts id="Seg_241" n="HIAT:ip">.</nts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T79" id="Seg_244" n="HIAT:u" s="T64">
                  <ts e="T65" id="Seg_246" n="HIAT:w" s="T64">Bu</ts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_249" n="HIAT:w" s="T65">gɨnan</ts>
                  <nts id="Seg_250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_252" n="HIAT:w" s="T66">baran</ts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_255" n="HIAT:w" s="T67">ilimin</ts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_258" n="HIAT:w" s="T68">ihiger</ts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_261" n="HIAT:w" s="T69">kiːren</ts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_264" n="HIAT:w" s="T70">ekkiriː-ekkiriː</ts>
                  <nts id="Seg_265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_266" n="HIAT:ip">"</nts>
                  <ts e="T72" id="Seg_268" n="HIAT:w" s="T71">ohoː-eheː</ts>
                  <nts id="Seg_269" n="HIAT:ip">"</nts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_272" n="HIAT:w" s="T72">di͡en</ts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_275" n="HIAT:w" s="T73">ü͡ögülüː-ü͡ögülüː</ts>
                  <nts id="Seg_276" n="HIAT:ip">,</nts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_279" n="HIAT:w" s="T74">ogoluː</ts>
                  <nts id="Seg_280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_282" n="HIAT:w" s="T75">harɨlɨː-harɨlɨː</ts>
                  <nts id="Seg_283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_285" n="HIAT:w" s="T76">muŋ</ts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_288" n="HIAT:w" s="T77">eletinen</ts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_291" n="HIAT:w" s="T78">ekkiriːr</ts>
                  <nts id="Seg_292" n="HIAT:ip">.</nts>
                  <nts id="Seg_293" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T83" id="Seg_295" n="HIAT:u" s="T79">
                  <ts e="T80" id="Seg_297" n="HIAT:w" s="T79">Ogurduk</ts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_300" n="HIAT:w" s="T80">üs</ts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_303" n="HIAT:w" s="T81">kün</ts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_306" n="HIAT:w" s="T82">bɨhɨːlanar</ts>
                  <nts id="Seg_307" n="HIAT:ip">.</nts>
                  <nts id="Seg_308" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T92" id="Seg_310" n="HIAT:u" s="T83">
                  <ts e="T84" id="Seg_312" n="HIAT:w" s="T83">Ühüs</ts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_315" n="HIAT:w" s="T84">künüger</ts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_318" n="HIAT:w" s="T85">taksɨbɨta</ts>
                  <nts id="Seg_319" n="HIAT:ip">,</nts>
                  <nts id="Seg_320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_322" n="HIAT:w" s="T86">ilimiger</ts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_325" n="HIAT:w" s="T87">u͡ol</ts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_328" n="HIAT:w" s="T88">ogo</ts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_331" n="HIAT:w" s="T89">tübehen</ts>
                  <nts id="Seg_332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_334" n="HIAT:w" s="T90">ɨtɨː</ts>
                  <nts id="Seg_335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_337" n="HIAT:w" s="T91">hɨtar</ts>
                  <nts id="Seg_338" n="HIAT:ip">.</nts>
                  <nts id="Seg_339" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T96" id="Seg_341" n="HIAT:u" s="T92">
                  <ts e="T93" id="Seg_343" n="HIAT:w" s="T92">Manɨ</ts>
                  <nts id="Seg_344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_346" n="HIAT:w" s="T93">ogonnʼor</ts>
                  <nts id="Seg_347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_349" n="HIAT:w" s="T94">dʼi͡etiger</ts>
                  <nts id="Seg_350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_352" n="HIAT:w" s="T95">egeler</ts>
                  <nts id="Seg_353" n="HIAT:ip">.</nts>
                  <nts id="Seg_354" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T105" id="Seg_356" n="HIAT:u" s="T96">
                  <ts e="T97" id="Seg_358" n="HIAT:w" s="T96">Dʼi͡ege</ts>
                  <nts id="Seg_359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_361" n="HIAT:w" s="T97">kiːrbittere</ts>
                  <nts id="Seg_362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_364" n="HIAT:w" s="T98">bu</ts>
                  <nts id="Seg_365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_367" n="HIAT:w" s="T99">ogoloro</ts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_370" n="HIAT:w" s="T100">čoroːkuta</ts>
                  <nts id="Seg_371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_373" n="HIAT:w" s="T101">da</ts>
                  <nts id="Seg_374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_376" n="HIAT:w" s="T102">emehete</ts>
                  <nts id="Seg_377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_379" n="HIAT:w" s="T103">deː</ts>
                  <nts id="Seg_380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_382" n="HIAT:w" s="T104">bütej</ts>
                  <nts id="Seg_383" n="HIAT:ip">.</nts>
                  <nts id="Seg_384" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T113" id="Seg_386" n="HIAT:u" s="T105">
                  <ts e="T106" id="Seg_388" n="HIAT:w" s="T105">Ü͡örbüt</ts>
                  <nts id="Seg_389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_391" n="HIAT:w" s="T106">dʼon</ts>
                  <nts id="Seg_392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_394" n="HIAT:w" s="T107">manɨ</ts>
                  <nts id="Seg_395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_397" n="HIAT:w" s="T108">dʼe</ts>
                  <nts id="Seg_398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_400" n="HIAT:w" s="T109">mataŋa</ts>
                  <nts id="Seg_401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_403" n="HIAT:w" s="T110">kurduk</ts>
                  <nts id="Seg_404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_406" n="HIAT:w" s="T111">kaːlaːn</ts>
                  <nts id="Seg_407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_409" n="HIAT:w" s="T112">ahatallar</ts>
                  <nts id="Seg_410" n="HIAT:ip">.</nts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T120" id="Seg_413" n="HIAT:u" s="T113">
                  <ts e="T114" id="Seg_415" n="HIAT:w" s="T113">Bütej</ts>
                  <nts id="Seg_416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_418" n="HIAT:w" s="T114">kihi</ts>
                  <nts id="Seg_419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_421" n="HIAT:w" s="T115">kajdak</ts>
                  <nts id="Seg_422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_424" n="HIAT:w" s="T116">bu͡olu͡oj</ts>
                  <nts id="Seg_425" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_426" n="HIAT:ip">—</nts>
                  <nts id="Seg_427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_429" n="HIAT:w" s="T117">honno</ts>
                  <nts id="Seg_430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_432" n="HIAT:w" s="T118">ölön</ts>
                  <nts id="Seg_433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_435" n="HIAT:w" s="T119">kaːlbɨt</ts>
                  <nts id="Seg_436" n="HIAT:ip">.</nts>
                  <nts id="Seg_437" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T129" id="Seg_439" n="HIAT:u" s="T120">
                  <ts e="T121" id="Seg_441" n="HIAT:w" s="T120">Onnuk</ts>
                  <nts id="Seg_442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_444" n="HIAT:w" s="T121">kaja</ts>
                  <nts id="Seg_445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_447" n="HIAT:w" s="T122">iččititten</ts>
                  <nts id="Seg_448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_450" n="HIAT:w" s="T123">ogo</ts>
                  <nts id="Seg_451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_453" n="HIAT:w" s="T124">kelen</ts>
                  <nts id="Seg_454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_456" n="HIAT:w" s="T125">barbɨt</ts>
                  <nts id="Seg_457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_459" n="HIAT:w" s="T126">di͡en</ts>
                  <nts id="Seg_460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_462" n="HIAT:w" s="T127">bɨlɨrgɨlar</ts>
                  <nts id="Seg_463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_465" n="HIAT:w" s="T128">kepsiːller</ts>
                  <nts id="Seg_466" n="HIAT:ip">.</nts>
                  <nts id="Seg_467" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T129" id="Seg_468" n="sc" s="T0">
               <ts e="T1" id="Seg_470" n="e" s="T0">Bɨlɨr </ts>
               <ts e="T2" id="Seg_472" n="e" s="T1">ogonnʼordoːk </ts>
               <ts e="T3" id="Seg_474" n="e" s="T2">emeːksin </ts>
               <ts e="T4" id="Seg_476" n="e" s="T3">hogotokkoːn </ts>
               <ts e="T5" id="Seg_478" n="e" s="T4">olorbuttar, </ts>
               <ts e="T6" id="Seg_480" n="e" s="T5">bi͡ek </ts>
               <ts e="T7" id="Seg_482" n="e" s="T6">ogoto </ts>
               <ts e="T8" id="Seg_484" n="e" s="T7">hu͡oktar. </ts>
               <ts e="T9" id="Seg_486" n="e" s="T8">Biːrde </ts>
               <ts e="T10" id="Seg_488" n="e" s="T9">turan </ts>
               <ts e="T11" id="Seg_490" n="e" s="T10">ogonnʼor </ts>
               <ts e="T12" id="Seg_492" n="e" s="T11">eppit </ts>
               <ts e="T13" id="Seg_494" n="e" s="T12">emeːksiniger: </ts>
               <ts e="T14" id="Seg_496" n="e" s="T13">"Emeːksin, </ts>
               <ts e="T15" id="Seg_498" n="e" s="T14">bihigi </ts>
               <ts e="T16" id="Seg_500" n="e" s="T15">ogolonu͡okputugar </ts>
               <ts e="T17" id="Seg_502" n="e" s="T16">bert." </ts>
               <ts e="T18" id="Seg_504" n="e" s="T17">Onuga </ts>
               <ts e="T19" id="Seg_506" n="e" s="T18">emeːksine </ts>
               <ts e="T20" id="Seg_508" n="e" s="T19">eppit: </ts>
               <ts e="T21" id="Seg_510" n="e" s="T20">Hiŋilbitiger </ts>
               <ts e="T22" id="Seg_512" n="e" s="T21">ogolommotokput, </ts>
               <ts e="T23" id="Seg_514" n="e" s="T22">kajdak </ts>
               <ts e="T24" id="Seg_516" n="e" s="T23">biligin </ts>
               <ts e="T25" id="Seg_518" n="e" s="T24">ogolonu͡okputuj? </ts>
               <ts e="T26" id="Seg_520" n="e" s="T25">Ol </ts>
               <ts e="T27" id="Seg_522" n="e" s="T26">hatanɨ͡a </ts>
               <ts e="T28" id="Seg_524" n="e" s="T27">du͡o", </ts>
               <ts e="T29" id="Seg_526" n="e" s="T28">di͡ebit. </ts>
               <ts e="T30" id="Seg_528" n="e" s="T29">Onu </ts>
               <ts e="T31" id="Seg_530" n="e" s="T30">ogonnʼoro </ts>
               <ts e="T32" id="Seg_532" n="e" s="T31">eppit: </ts>
               <ts e="T33" id="Seg_534" n="e" s="T32">"Min </ts>
               <ts e="T34" id="Seg_536" n="e" s="T33">kihi </ts>
               <ts e="T35" id="Seg_538" n="e" s="T34">bularɨttan </ts>
               <ts e="T36" id="Seg_540" n="e" s="T35">atɨnnɨk </ts>
               <ts e="T37" id="Seg_542" n="e" s="T36">ogolonoːru </ts>
               <ts e="T38" id="Seg_544" n="e" s="T37">gɨnabɨn", </ts>
               <ts e="T39" id="Seg_546" n="e" s="T38">di͡ebit. </ts>
               <ts e="T40" id="Seg_548" n="e" s="T39">Emeːksine </ts>
               <ts e="T41" id="Seg_550" n="e" s="T40">di͡ebit: </ts>
               <ts e="T42" id="Seg_552" n="e" s="T41">"Ogolonorgo </ts>
               <ts e="T43" id="Seg_554" n="e" s="T42">tu͡ok </ts>
               <ts e="T44" id="Seg_556" n="e" s="T43">kuhagana </ts>
               <ts e="T45" id="Seg_558" n="e" s="T44">bu͡olu͡oj? </ts>
               <ts e="T46" id="Seg_560" n="e" s="T45">Onnuk </ts>
               <ts e="T47" id="Seg_562" n="e" s="T46">bu͡ollagɨna </ts>
               <ts e="T48" id="Seg_564" n="e" s="T47">terinnegiŋ </ts>
               <ts e="T49" id="Seg_566" n="e" s="T48">diː." </ts>
               <ts e="T50" id="Seg_568" n="e" s="T49">Manɨ͡aga </ts>
               <ts e="T51" id="Seg_570" n="e" s="T50">ogonnʼoro </ts>
               <ts e="T52" id="Seg_572" n="e" s="T51">ilimi </ts>
               <ts e="T53" id="Seg_574" n="e" s="T52">ɨlbɨt, </ts>
               <ts e="T54" id="Seg_576" n="e" s="T53">kabɨjakaːŋŋa </ts>
               <ts e="T55" id="Seg_578" n="e" s="T54">turu͡orallarɨn </ts>
               <ts e="T56" id="Seg_580" n="e" s="T55">kurduk </ts>
               <ts e="T57" id="Seg_582" n="e" s="T56">kɨtɨlga </ts>
               <ts e="T58" id="Seg_584" n="e" s="T57">tu͡ora </ts>
               <ts e="T59" id="Seg_586" n="e" s="T58">mastaːn </ts>
               <ts e="T60" id="Seg_588" n="e" s="T59">turu͡oran </ts>
               <ts e="T61" id="Seg_590" n="e" s="T60">keːspit, </ts>
               <ts e="T62" id="Seg_592" n="e" s="T61">kaja </ts>
               <ts e="T63" id="Seg_594" n="e" s="T62">di͡eg </ts>
               <ts e="T64" id="Seg_596" n="e" s="T63">annʼan. </ts>
               <ts e="T65" id="Seg_598" n="e" s="T64">Bu </ts>
               <ts e="T66" id="Seg_600" n="e" s="T65">gɨnan </ts>
               <ts e="T67" id="Seg_602" n="e" s="T66">baran </ts>
               <ts e="T68" id="Seg_604" n="e" s="T67">ilimin </ts>
               <ts e="T69" id="Seg_606" n="e" s="T68">ihiger </ts>
               <ts e="T70" id="Seg_608" n="e" s="T69">kiːren </ts>
               <ts e="T71" id="Seg_610" n="e" s="T70">ekkiriː-ekkiriː </ts>
               <ts e="T72" id="Seg_612" n="e" s="T71">"ohoː-eheː" </ts>
               <ts e="T73" id="Seg_614" n="e" s="T72">di͡en </ts>
               <ts e="T74" id="Seg_616" n="e" s="T73">ü͡ögülüː-ü͡ögülüː, </ts>
               <ts e="T75" id="Seg_618" n="e" s="T74">ogoluː </ts>
               <ts e="T76" id="Seg_620" n="e" s="T75">harɨlɨː-harɨlɨː </ts>
               <ts e="T77" id="Seg_622" n="e" s="T76">muŋ </ts>
               <ts e="T78" id="Seg_624" n="e" s="T77">eletinen </ts>
               <ts e="T79" id="Seg_626" n="e" s="T78">ekkiriːr. </ts>
               <ts e="T80" id="Seg_628" n="e" s="T79">Ogurduk </ts>
               <ts e="T81" id="Seg_630" n="e" s="T80">üs </ts>
               <ts e="T82" id="Seg_632" n="e" s="T81">kün </ts>
               <ts e="T83" id="Seg_634" n="e" s="T82">bɨhɨːlanar. </ts>
               <ts e="T84" id="Seg_636" n="e" s="T83">Ühüs </ts>
               <ts e="T85" id="Seg_638" n="e" s="T84">künüger </ts>
               <ts e="T86" id="Seg_640" n="e" s="T85">taksɨbɨta, </ts>
               <ts e="T87" id="Seg_642" n="e" s="T86">ilimiger </ts>
               <ts e="T88" id="Seg_644" n="e" s="T87">u͡ol </ts>
               <ts e="T89" id="Seg_646" n="e" s="T88">ogo </ts>
               <ts e="T90" id="Seg_648" n="e" s="T89">tübehen </ts>
               <ts e="T91" id="Seg_650" n="e" s="T90">ɨtɨː </ts>
               <ts e="T92" id="Seg_652" n="e" s="T91">hɨtar. </ts>
               <ts e="T93" id="Seg_654" n="e" s="T92">Manɨ </ts>
               <ts e="T94" id="Seg_656" n="e" s="T93">ogonnʼor </ts>
               <ts e="T95" id="Seg_658" n="e" s="T94">dʼi͡etiger </ts>
               <ts e="T96" id="Seg_660" n="e" s="T95">egeler. </ts>
               <ts e="T97" id="Seg_662" n="e" s="T96">Dʼi͡ege </ts>
               <ts e="T98" id="Seg_664" n="e" s="T97">kiːrbittere </ts>
               <ts e="T99" id="Seg_666" n="e" s="T98">bu </ts>
               <ts e="T100" id="Seg_668" n="e" s="T99">ogoloro </ts>
               <ts e="T101" id="Seg_670" n="e" s="T100">čoroːkuta </ts>
               <ts e="T102" id="Seg_672" n="e" s="T101">da </ts>
               <ts e="T103" id="Seg_674" n="e" s="T102">emehete </ts>
               <ts e="T104" id="Seg_676" n="e" s="T103">deː </ts>
               <ts e="T105" id="Seg_678" n="e" s="T104">bütej. </ts>
               <ts e="T106" id="Seg_680" n="e" s="T105">Ü͡örbüt </ts>
               <ts e="T107" id="Seg_682" n="e" s="T106">dʼon </ts>
               <ts e="T108" id="Seg_684" n="e" s="T107">manɨ </ts>
               <ts e="T109" id="Seg_686" n="e" s="T108">dʼe </ts>
               <ts e="T110" id="Seg_688" n="e" s="T109">mataŋa </ts>
               <ts e="T111" id="Seg_690" n="e" s="T110">kurduk </ts>
               <ts e="T112" id="Seg_692" n="e" s="T111">kaːlaːn </ts>
               <ts e="T113" id="Seg_694" n="e" s="T112">ahatallar. </ts>
               <ts e="T114" id="Seg_696" n="e" s="T113">Bütej </ts>
               <ts e="T115" id="Seg_698" n="e" s="T114">kihi </ts>
               <ts e="T116" id="Seg_700" n="e" s="T115">kajdak </ts>
               <ts e="T117" id="Seg_702" n="e" s="T116">bu͡olu͡oj — </ts>
               <ts e="T118" id="Seg_704" n="e" s="T117">honno </ts>
               <ts e="T119" id="Seg_706" n="e" s="T118">ölön </ts>
               <ts e="T120" id="Seg_708" n="e" s="T119">kaːlbɨt. </ts>
               <ts e="T121" id="Seg_710" n="e" s="T120">Onnuk </ts>
               <ts e="T122" id="Seg_712" n="e" s="T121">kaja </ts>
               <ts e="T123" id="Seg_714" n="e" s="T122">iččititten </ts>
               <ts e="T124" id="Seg_716" n="e" s="T123">ogo </ts>
               <ts e="T125" id="Seg_718" n="e" s="T124">kelen </ts>
               <ts e="T126" id="Seg_720" n="e" s="T125">barbɨt </ts>
               <ts e="T127" id="Seg_722" n="e" s="T126">di͡en </ts>
               <ts e="T128" id="Seg_724" n="e" s="T127">bɨlɨrgɨlar </ts>
               <ts e="T129" id="Seg_726" n="e" s="T128">kepsiːller. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T8" id="Seg_727" s="T0">BaRD_1930_ChildOfMasterOfMountain_flk.001 (001.001)</ta>
            <ta e="T13" id="Seg_728" s="T8">BaRD_1930_ChildOfMasterOfMountain_flk.002 (001.002)</ta>
            <ta e="T17" id="Seg_729" s="T13">BaRD_1930_ChildOfMasterOfMountain_flk.003 (001.003)</ta>
            <ta e="T20" id="Seg_730" s="T17">BaRD_1930_ChildOfMasterOfMountain_flk.004 (001.004)</ta>
            <ta e="T25" id="Seg_731" s="T20">BaRD_1930_ChildOfMasterOfMountain_flk.005 (001.005)</ta>
            <ta e="T29" id="Seg_732" s="T25">BaRD_1930_ChildOfMasterOfMountain_flk.006 (001.006)</ta>
            <ta e="T32" id="Seg_733" s="T29">BaRD_1930_ChildOfMasterOfMountain_flk.007 (001.008)</ta>
            <ta e="T39" id="Seg_734" s="T32">BaRD_1930_ChildOfMasterOfMountain_flk.008 (001.009)</ta>
            <ta e="T41" id="Seg_735" s="T39">BaRD_1930_ChildOfMasterOfMountain_flk.009 (001.010)</ta>
            <ta e="T45" id="Seg_736" s="T41">BaRD_1930_ChildOfMasterOfMountain_flk.010 (001.011)</ta>
            <ta e="T49" id="Seg_737" s="T45">BaRD_1930_ChildOfMasterOfMountain_flk.011 (001.012)</ta>
            <ta e="T64" id="Seg_738" s="T49">BaRD_1930_ChildOfMasterOfMountain_flk.012 (001.013)</ta>
            <ta e="T79" id="Seg_739" s="T64">BaRD_1930_ChildOfMasterOfMountain_flk.013 (001.014)</ta>
            <ta e="T83" id="Seg_740" s="T79">BaRD_1930_ChildOfMasterOfMountain_flk.014 (001.015)</ta>
            <ta e="T92" id="Seg_741" s="T83">BaRD_1930_ChildOfMasterOfMountain_flk.015 (001.016)</ta>
            <ta e="T96" id="Seg_742" s="T92">BaRD_1930_ChildOfMasterOfMountain_flk.016 (001.017)</ta>
            <ta e="T105" id="Seg_743" s="T96">BaRD_1930_ChildOfMasterOfMountain_flk.017 (001.018)</ta>
            <ta e="T113" id="Seg_744" s="T105">BaRD_1930_ChildOfMasterOfMountain_flk.018 (001.019)</ta>
            <ta e="T120" id="Seg_745" s="T113">BaRD_1930_ChildOfMasterOfMountain_flk.019 (001.020)</ta>
            <ta e="T129" id="Seg_746" s="T120">BaRD_1930_ChildOfMasterOfMountain_flk.020 (001.021)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T8" id="Seg_747" s="T0">Былыр огонньордоок эмээксин һоготоккоон олорбуттар, биэк огото һуоктар.</ta>
            <ta e="T13" id="Seg_748" s="T8">Биирдэ туран огонньор эппит эмээксинигэр:</ta>
            <ta e="T17" id="Seg_749" s="T13">— Эмээксин, биһиги оголонуокпутугар бэрт.</ta>
            <ta e="T20" id="Seg_750" s="T17">Онуга эмээксинэ эппит:</ta>
            <ta e="T25" id="Seg_751" s="T20">— Һиҥилбитигэр оголоммотокпут, кайдак билигин оголонуок-путуй?</ta>
            <ta e="T29" id="Seg_752" s="T25">Ол һатаныа дуо? — диэбит.</ta>
            <ta e="T32" id="Seg_753" s="T29">Ону огонньоро эппит:</ta>
            <ta e="T39" id="Seg_754" s="T32">— Мин киһи буларыттан атыннык оголоноору гынабын, — диэбит.</ta>
            <ta e="T41" id="Seg_755" s="T39">Эмээксинэ диэбит:</ta>
            <ta e="T45" id="Seg_756" s="T41">— Оголонорго туок куһагана буолуой?!</ta>
            <ta e="T49" id="Seg_757" s="T45">Оннук буоллагына тэриннэгиҥ дии.</ta>
            <ta e="T64" id="Seg_758" s="T49">Маныага огонньоро илими ылбыт, кабыйакааҥҥа туруоралларын курдук кытылга туора мастаан туруоран кээспит, кайа диэг анньан.</ta>
            <ta e="T79" id="Seg_759" s="T64">Бу гынан баран: илиминиһигэр киирэн эккирии-эккирии: "Оһоо-эһээ!" — диэн үөгүлүү-үөгүлүү, оголуу һарылыы-һарылыы муҥ элэтинэн эккириир.</ta>
            <ta e="T83" id="Seg_760" s="T79">Огурдук үс күн быһыыланар.</ta>
            <ta e="T92" id="Seg_761" s="T83">Үһүс күнүгэр таксыбыта, илимигэр уол ого түбэһэн ытыы һытар.</ta>
            <ta e="T96" id="Seg_762" s="T92">Маны огонньор дьиэтигэр эгэлэр.</ta>
            <ta e="T105" id="Seg_763" s="T96">Дьиэгэ киирбиттэрэ бу оголоро чороокута да эмэһэтэ дээ бүтэй.</ta>
            <ta e="T113" id="Seg_764" s="T105">Үөрбүт дьон маны дьэ матаҥа курдук каалаан аһаталлар.</ta>
            <ta e="T120" id="Seg_765" s="T113">Бүтэй киһи кайдак буолуой — һонно өлөн каалбыт.</ta>
            <ta e="T129" id="Seg_766" s="T120">Оннук кайа иччититтэн ого кэлэн барбыт диэн былыргылар кэпсииллэр.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T8" id="Seg_767" s="T0">Bɨlɨr ogonnʼordoːk emeːksin hogotokkoːn olorbuttar, bi͡ek ogoto hu͡oktar. </ta>
            <ta e="T13" id="Seg_768" s="T8">Biːrde turan ogonnʼor eppit emeːksiniger:</ta>
            <ta e="T17" id="Seg_769" s="T13">"Emeːksin, bihigi ogolonu͡okputugar bert." </ta>
            <ta e="T20" id="Seg_770" s="T17">Onuga emeːksine eppit:</ta>
            <ta e="T25" id="Seg_771" s="T20">"Hiŋilbitiger ogolommotokput, kajdak biligin ogolonu͡okputuj? </ta>
            <ta e="T29" id="Seg_772" s="T25">Ol hatanɨ͡a du͡o", di͡ebit. </ta>
            <ta e="T32" id="Seg_773" s="T29">Onu ogonnʼoro eppit:</ta>
            <ta e="T39" id="Seg_774" s="T32">"Min kihi bularɨttan atɨnnɨk ogolonoːru gɨnabɨn", di͡ebit. </ta>
            <ta e="T41" id="Seg_775" s="T39">Emeːksine di͡ebit:</ta>
            <ta e="T45" id="Seg_776" s="T41">"Ogolonorgo tu͡ok kuhagana bu͡olu͡oj? </ta>
            <ta e="T49" id="Seg_777" s="T45">Onnuk bu͡ollagɨna terinnegiŋ diː." </ta>
            <ta e="T64" id="Seg_778" s="T49">Manɨ͡aga ogonnʼoro ilimi ɨlbɨt, kabɨjakaːŋŋa turu͡orallarɨn kurduk kɨtɨlga tu͡ora mastaːn turu͡oran keːspit, kaja di͡eg annʼan. </ta>
            <ta e="T79" id="Seg_779" s="T64">Bu gɨnan baran ilimin ihiger kiːren ekkiriː-ekkiriː "ohoː-eheː" di͡en ü͡ögülüː-ü͡ögülüː, ogoluː harɨlɨː-harɨlɨː muŋ eletinen ekkiriːr. </ta>
            <ta e="T83" id="Seg_780" s="T79">Ogurduk üs kün bɨhɨːlanar. </ta>
            <ta e="T92" id="Seg_781" s="T83">Ühüs künüger taksɨbɨta, ilimiger u͡ol ogo tübehen ɨtɨː hɨtar. </ta>
            <ta e="T96" id="Seg_782" s="T92">Manɨ ogonnʼor dʼi͡etiger egeler. </ta>
            <ta e="T105" id="Seg_783" s="T96">Dʼi͡ege kiːrbittere bu ogoloro čoroːkuta da emehete deː bütej. </ta>
            <ta e="T113" id="Seg_784" s="T105">Ü͡örbüt dʼon manɨ dʼe mataŋa kurduk kaːlaːn ahatallar. </ta>
            <ta e="T120" id="Seg_785" s="T113">Bütej kihi kajdak bu͡olu͡oj — honno ölön kaːlbɨt. </ta>
            <ta e="T129" id="Seg_786" s="T120">Onnuk kaja iččititten ogo kelen barbɨt di͡en bɨlɨrgɨlar kepsiːller. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_787" s="T0">bɨlɨr</ta>
            <ta e="T2" id="Seg_788" s="T1">ogonnʼor-doːk</ta>
            <ta e="T3" id="Seg_789" s="T2">emeːksin</ta>
            <ta e="T4" id="Seg_790" s="T3">hogotok-koːn</ta>
            <ta e="T5" id="Seg_791" s="T4">olor-but-tar</ta>
            <ta e="T6" id="Seg_792" s="T5">bi͡ek</ta>
            <ta e="T7" id="Seg_793" s="T6">ogo-to</ta>
            <ta e="T8" id="Seg_794" s="T7">hu͡ok-tar</ta>
            <ta e="T9" id="Seg_795" s="T8">biːrde</ta>
            <ta e="T10" id="Seg_796" s="T9">tur-an</ta>
            <ta e="T11" id="Seg_797" s="T10">ogonnʼor</ta>
            <ta e="T12" id="Seg_798" s="T11">ep-pit</ta>
            <ta e="T13" id="Seg_799" s="T12">emeːksin-i-ger</ta>
            <ta e="T14" id="Seg_800" s="T13">emeːksin</ta>
            <ta e="T15" id="Seg_801" s="T14">bihigi</ta>
            <ta e="T16" id="Seg_802" s="T15">ogo-lon-u͡ok-putu-gar</ta>
            <ta e="T17" id="Seg_803" s="T16">bert</ta>
            <ta e="T18" id="Seg_804" s="T17">onu-ga</ta>
            <ta e="T19" id="Seg_805" s="T18">emeːksin-e</ta>
            <ta e="T20" id="Seg_806" s="T19">ep-pit</ta>
            <ta e="T21" id="Seg_807" s="T20">hiŋil-biti-ger</ta>
            <ta e="T22" id="Seg_808" s="T21">ogo-lom-motok-put</ta>
            <ta e="T23" id="Seg_809" s="T22">kajdak</ta>
            <ta e="T24" id="Seg_810" s="T23">biligin</ta>
            <ta e="T25" id="Seg_811" s="T24">ogo-lon-u͡ok-put=uj</ta>
            <ta e="T26" id="Seg_812" s="T25">ol</ta>
            <ta e="T27" id="Seg_813" s="T26">hatan-ɨ͡a</ta>
            <ta e="T28" id="Seg_814" s="T27">du͡o</ta>
            <ta e="T29" id="Seg_815" s="T28">di͡e-bit</ta>
            <ta e="T30" id="Seg_816" s="T29">o-nu</ta>
            <ta e="T31" id="Seg_817" s="T30">ogonnʼor-o</ta>
            <ta e="T32" id="Seg_818" s="T31">ep-pit</ta>
            <ta e="T33" id="Seg_819" s="T32">min</ta>
            <ta e="T34" id="Seg_820" s="T33">kihi</ta>
            <ta e="T35" id="Seg_821" s="T34">bu-lar-ɨ-ttan</ta>
            <ta e="T36" id="Seg_822" s="T35">atɨnnɨk</ta>
            <ta e="T37" id="Seg_823" s="T36">ogo-lon-oːru</ta>
            <ta e="T38" id="Seg_824" s="T37">gɨn-a-bɨn</ta>
            <ta e="T39" id="Seg_825" s="T38">di͡e-bit</ta>
            <ta e="T40" id="Seg_826" s="T39">emeːksin-e</ta>
            <ta e="T41" id="Seg_827" s="T40">di͡e-bit</ta>
            <ta e="T42" id="Seg_828" s="T41">ogo-lon-or-go</ta>
            <ta e="T43" id="Seg_829" s="T42">tu͡ok</ta>
            <ta e="T44" id="Seg_830" s="T43">kuhagana</ta>
            <ta e="T45" id="Seg_831" s="T44">bu͡ol-u͡o=j</ta>
            <ta e="T46" id="Seg_832" s="T45">onnuk</ta>
            <ta e="T47" id="Seg_833" s="T46">bu͡ollagɨna</ta>
            <ta e="T48" id="Seg_834" s="T47">terin-neg-i-ŋ</ta>
            <ta e="T49" id="Seg_835" s="T48">diː</ta>
            <ta e="T50" id="Seg_836" s="T49">manɨ͡a-ga</ta>
            <ta e="T51" id="Seg_837" s="T50">ogonnʼor-o</ta>
            <ta e="T52" id="Seg_838" s="T51">ilim-i</ta>
            <ta e="T53" id="Seg_839" s="T52">ɨl-bɨt</ta>
            <ta e="T54" id="Seg_840" s="T53">kabɨjakaːŋ-ŋa</ta>
            <ta e="T55" id="Seg_841" s="T54">turu͡or-al-larɨ-n</ta>
            <ta e="T56" id="Seg_842" s="T55">kurduk</ta>
            <ta e="T57" id="Seg_843" s="T56">kɨtɨl-ga</ta>
            <ta e="T58" id="Seg_844" s="T57">tu͡ora</ta>
            <ta e="T59" id="Seg_845" s="T58">mas-taː-n</ta>
            <ta e="T60" id="Seg_846" s="T59">turu͡or-an</ta>
            <ta e="T61" id="Seg_847" s="T60">keːs-pit</ta>
            <ta e="T62" id="Seg_848" s="T61">kaja</ta>
            <ta e="T63" id="Seg_849" s="T62">di͡eg</ta>
            <ta e="T64" id="Seg_850" s="T63">annʼ-an</ta>
            <ta e="T65" id="Seg_851" s="T64">bu</ta>
            <ta e="T66" id="Seg_852" s="T65">gɨn-an</ta>
            <ta e="T67" id="Seg_853" s="T66">baran</ta>
            <ta e="T68" id="Seg_854" s="T67">ilim-i-n</ta>
            <ta e="T69" id="Seg_855" s="T68">ih-i-ger</ta>
            <ta e="T70" id="Seg_856" s="T69">kiːr-en</ta>
            <ta e="T71" id="Seg_857" s="T70">ekkir-iː-ekkir-iː</ta>
            <ta e="T72" id="Seg_858" s="T71">ohoː-eheː</ta>
            <ta e="T73" id="Seg_859" s="T72">di͡e-n</ta>
            <ta e="T74" id="Seg_860" s="T73">ü͡ögül-üː-ü͡ögül-üː</ta>
            <ta e="T75" id="Seg_861" s="T74">ogo-luː</ta>
            <ta e="T76" id="Seg_862" s="T75">harɨl-ɨː-harɨl-ɨː</ta>
            <ta e="T77" id="Seg_863" s="T76">muŋ</ta>
            <ta e="T78" id="Seg_864" s="T77">ele-ti-nen</ta>
            <ta e="T79" id="Seg_865" s="T78">ekkiriː-r</ta>
            <ta e="T80" id="Seg_866" s="T79">ogurduk</ta>
            <ta e="T81" id="Seg_867" s="T80">üs</ta>
            <ta e="T82" id="Seg_868" s="T81">kün</ta>
            <ta e="T83" id="Seg_869" s="T82">bɨhɨː-lan-ar</ta>
            <ta e="T84" id="Seg_870" s="T83">üh-üs</ta>
            <ta e="T85" id="Seg_871" s="T84">kün-ü-ger</ta>
            <ta e="T86" id="Seg_872" s="T85">taks-ɨ-bɨt-a</ta>
            <ta e="T87" id="Seg_873" s="T86">ilim-i-ger</ta>
            <ta e="T88" id="Seg_874" s="T87">u͡ol</ta>
            <ta e="T89" id="Seg_875" s="T88">ogo</ta>
            <ta e="T90" id="Seg_876" s="T89">tübeh-en</ta>
            <ta e="T91" id="Seg_877" s="T90">ɨt-ɨː</ta>
            <ta e="T92" id="Seg_878" s="T91">hɨt-ar</ta>
            <ta e="T93" id="Seg_879" s="T92">ma-nɨ</ta>
            <ta e="T94" id="Seg_880" s="T93">ogonnʼor</ta>
            <ta e="T95" id="Seg_881" s="T94">dʼi͡e-ti-ger</ta>
            <ta e="T96" id="Seg_882" s="T95">egel-er</ta>
            <ta e="T97" id="Seg_883" s="T96">dʼi͡e-ge</ta>
            <ta e="T98" id="Seg_884" s="T97">kiːr-bit-tere</ta>
            <ta e="T99" id="Seg_885" s="T98">bu</ta>
            <ta e="T100" id="Seg_886" s="T99">ogo-loro</ta>
            <ta e="T101" id="Seg_887" s="T100">čoroːkut-a</ta>
            <ta e="T102" id="Seg_888" s="T101">da</ta>
            <ta e="T103" id="Seg_889" s="T102">emehe-te</ta>
            <ta e="T104" id="Seg_890" s="T103">deː</ta>
            <ta e="T105" id="Seg_891" s="T104">bütej</ta>
            <ta e="T106" id="Seg_892" s="T105">ü͡ör-büt</ta>
            <ta e="T107" id="Seg_893" s="T106">dʼon</ta>
            <ta e="T108" id="Seg_894" s="T107">ma-nɨ</ta>
            <ta e="T109" id="Seg_895" s="T108">dʼe</ta>
            <ta e="T110" id="Seg_896" s="T109">mataŋa</ta>
            <ta e="T111" id="Seg_897" s="T110">kurduk</ta>
            <ta e="T112" id="Seg_898" s="T111">kaːlaː-n</ta>
            <ta e="T113" id="Seg_899" s="T112">ah-a-t-al-lar</ta>
            <ta e="T114" id="Seg_900" s="T113">bütej</ta>
            <ta e="T115" id="Seg_901" s="T114">kihi</ta>
            <ta e="T116" id="Seg_902" s="T115">kajdak</ta>
            <ta e="T117" id="Seg_903" s="T116">bu͡ol-u͡o=j</ta>
            <ta e="T118" id="Seg_904" s="T117">h-onno</ta>
            <ta e="T119" id="Seg_905" s="T118">öl-ön</ta>
            <ta e="T120" id="Seg_906" s="T119">kaːl-bɨt</ta>
            <ta e="T121" id="Seg_907" s="T120">onnuk</ta>
            <ta e="T122" id="Seg_908" s="T121">kaja</ta>
            <ta e="T123" id="Seg_909" s="T122">ičči-ti-tten</ta>
            <ta e="T124" id="Seg_910" s="T123">ogo</ta>
            <ta e="T125" id="Seg_911" s="T124">kel-en</ta>
            <ta e="T126" id="Seg_912" s="T125">bar-bɨt</ta>
            <ta e="T127" id="Seg_913" s="T126">di͡e-n</ta>
            <ta e="T128" id="Seg_914" s="T127">bɨlɨrgɨ-lar</ta>
            <ta e="T129" id="Seg_915" s="T128">kepsiː-l-ler</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_916" s="T0">bɨlɨr</ta>
            <ta e="T2" id="Seg_917" s="T1">ogonnʼor-LAːK</ta>
            <ta e="T3" id="Seg_918" s="T2">emeːksin</ta>
            <ta e="T4" id="Seg_919" s="T3">čogotok-kAːN</ta>
            <ta e="T5" id="Seg_920" s="T4">olor-BIT-LAr</ta>
            <ta e="T6" id="Seg_921" s="T5">bi͡ek</ta>
            <ta e="T7" id="Seg_922" s="T6">ogo-tA</ta>
            <ta e="T8" id="Seg_923" s="T7">hu͡ok-LAr</ta>
            <ta e="T9" id="Seg_924" s="T8">biːrde</ta>
            <ta e="T10" id="Seg_925" s="T9">tur-An</ta>
            <ta e="T11" id="Seg_926" s="T10">ogonnʼor</ta>
            <ta e="T12" id="Seg_927" s="T11">et-BIT</ta>
            <ta e="T13" id="Seg_928" s="T12">emeːksin-tI-GAr</ta>
            <ta e="T14" id="Seg_929" s="T13">emeːksin</ta>
            <ta e="T15" id="Seg_930" s="T14">bihigi</ta>
            <ta e="T16" id="Seg_931" s="T15">ogo-LAN-IAK-BItI-GAr</ta>
            <ta e="T17" id="Seg_932" s="T16">bert</ta>
            <ta e="T18" id="Seg_933" s="T17">ol-GA</ta>
            <ta e="T19" id="Seg_934" s="T18">emeːksin-tA</ta>
            <ta e="T20" id="Seg_935" s="T19">et-BIT</ta>
            <ta e="T21" id="Seg_936" s="T20">hiŋil-BItI-GAr</ta>
            <ta e="T22" id="Seg_937" s="T21">ogo-LAN-BAtAK-BIt</ta>
            <ta e="T23" id="Seg_938" s="T22">kajdak</ta>
            <ta e="T24" id="Seg_939" s="T23">biligin</ta>
            <ta e="T25" id="Seg_940" s="T24">ogo-LAN-IAK-BIt=Ij</ta>
            <ta e="T26" id="Seg_941" s="T25">ol</ta>
            <ta e="T27" id="Seg_942" s="T26">hatan-IAK.[tA]</ta>
            <ta e="T28" id="Seg_943" s="T27">du͡o</ta>
            <ta e="T29" id="Seg_944" s="T28">di͡e-BIT</ta>
            <ta e="T30" id="Seg_945" s="T29">ol-nI</ta>
            <ta e="T31" id="Seg_946" s="T30">ogonnʼor-tA</ta>
            <ta e="T32" id="Seg_947" s="T31">et-BIT</ta>
            <ta e="T33" id="Seg_948" s="T32">min</ta>
            <ta e="T34" id="Seg_949" s="T33">kihi</ta>
            <ta e="T35" id="Seg_950" s="T34">bu-LAr-tI-ttAn</ta>
            <ta e="T36" id="Seg_951" s="T35">atɨnnɨk</ta>
            <ta e="T37" id="Seg_952" s="T36">ogo-LAN-AːrI</ta>
            <ta e="T38" id="Seg_953" s="T37">gɨn-A-BIn</ta>
            <ta e="T39" id="Seg_954" s="T38">di͡e-BIT</ta>
            <ta e="T40" id="Seg_955" s="T39">emeːksin-tA</ta>
            <ta e="T41" id="Seg_956" s="T40">di͡e-BIT</ta>
            <ta e="T42" id="Seg_957" s="T41">ogo-LAN-Ar-GA</ta>
            <ta e="T43" id="Seg_958" s="T42">tu͡ok</ta>
            <ta e="T44" id="Seg_959" s="T43">kuhagana</ta>
            <ta e="T45" id="Seg_960" s="T44">bu͡ol-IAK.[tA]=Ij</ta>
            <ta e="T46" id="Seg_961" s="T45">onnuk</ta>
            <ta e="T47" id="Seg_962" s="T46">bu͡ollagɨna</ta>
            <ta e="T48" id="Seg_963" s="T47">terin-TAK-I-ŋ</ta>
            <ta e="T49" id="Seg_964" s="T48">diː</ta>
            <ta e="T50" id="Seg_965" s="T49">bu-GA</ta>
            <ta e="T51" id="Seg_966" s="T50">ogonnʼor-tA</ta>
            <ta e="T52" id="Seg_967" s="T51">ilim-nI</ta>
            <ta e="T53" id="Seg_968" s="T52">ɨl-BIT</ta>
            <ta e="T54" id="Seg_969" s="T53">kabɨjakaːn-GA</ta>
            <ta e="T55" id="Seg_970" s="T54">turu͡or-Ar-LArI-n</ta>
            <ta e="T56" id="Seg_971" s="T55">kurduk</ta>
            <ta e="T57" id="Seg_972" s="T56">kɨtɨl-GA</ta>
            <ta e="T58" id="Seg_973" s="T57">tu͡ora</ta>
            <ta e="T59" id="Seg_974" s="T58">mas-LAː-An</ta>
            <ta e="T60" id="Seg_975" s="T59">turu͡or-An</ta>
            <ta e="T61" id="Seg_976" s="T60">keːs-BIT</ta>
            <ta e="T62" id="Seg_977" s="T61">kaja</ta>
            <ta e="T63" id="Seg_978" s="T62">dek</ta>
            <ta e="T64" id="Seg_979" s="T63">as-An</ta>
            <ta e="T65" id="Seg_980" s="T64">bu</ta>
            <ta e="T66" id="Seg_981" s="T65">gɨn-An</ta>
            <ta e="T67" id="Seg_982" s="T66">baran</ta>
            <ta e="T68" id="Seg_983" s="T67">ilim-tI-n</ta>
            <ta e="T69" id="Seg_984" s="T68">is-tI-GAr</ta>
            <ta e="T70" id="Seg_985" s="T69">kiːr-An</ta>
            <ta e="T71" id="Seg_986" s="T70">ekkireː-A-ekkireː-A</ta>
            <ta e="T72" id="Seg_987" s="T71">ohoː-eheː</ta>
            <ta e="T73" id="Seg_988" s="T72">di͡e-An</ta>
            <ta e="T74" id="Seg_989" s="T73">ü͡ögüleː-A-ü͡ögüleː-A</ta>
            <ta e="T75" id="Seg_990" s="T74">ogo-LIː</ta>
            <ta e="T76" id="Seg_991" s="T75">harɨlaː-A-harɨlaː-A</ta>
            <ta e="T77" id="Seg_992" s="T76">muŋ</ta>
            <ta e="T78" id="Seg_993" s="T77">ele-tI-nAn</ta>
            <ta e="T79" id="Seg_994" s="T78">ekkireː-Ar</ta>
            <ta e="T80" id="Seg_995" s="T79">ogorduk</ta>
            <ta e="T81" id="Seg_996" s="T80">üs</ta>
            <ta e="T82" id="Seg_997" s="T81">kün</ta>
            <ta e="T83" id="Seg_998" s="T82">bɨhɨː-LAN-Ar</ta>
            <ta e="T84" id="Seg_999" s="T83">üs-Is</ta>
            <ta e="T85" id="Seg_1000" s="T84">kün-tI-GAr</ta>
            <ta e="T86" id="Seg_1001" s="T85">tagɨs-I-BIT-tA</ta>
            <ta e="T87" id="Seg_1002" s="T86">ilim-tI-GAr</ta>
            <ta e="T88" id="Seg_1003" s="T87">u͡ol</ta>
            <ta e="T89" id="Seg_1004" s="T88">ogo</ta>
            <ta e="T90" id="Seg_1005" s="T89">tübes-An</ta>
            <ta e="T91" id="Seg_1006" s="T90">ɨtaː-A</ta>
            <ta e="T92" id="Seg_1007" s="T91">hɨt-Ar</ta>
            <ta e="T93" id="Seg_1008" s="T92">bu-nI</ta>
            <ta e="T94" id="Seg_1009" s="T93">ogonnʼor</ta>
            <ta e="T95" id="Seg_1010" s="T94">dʼi͡e-tI-GAr</ta>
            <ta e="T96" id="Seg_1011" s="T95">egel-Ar</ta>
            <ta e="T97" id="Seg_1012" s="T96">dʼi͡e-GA</ta>
            <ta e="T98" id="Seg_1013" s="T97">kiːr-BIT-LArA</ta>
            <ta e="T99" id="Seg_1014" s="T98">bu</ta>
            <ta e="T100" id="Seg_1015" s="T99">ogo-LArA</ta>
            <ta e="T101" id="Seg_1016" s="T100">čoroːkut-tA</ta>
            <ta e="T102" id="Seg_1017" s="T101">da</ta>
            <ta e="T103" id="Seg_1018" s="T102">emehe-tA</ta>
            <ta e="T104" id="Seg_1019" s="T103">da</ta>
            <ta e="T105" id="Seg_1020" s="T104">bütej</ta>
            <ta e="T106" id="Seg_1021" s="T105">ü͡ör-BIT</ta>
            <ta e="T107" id="Seg_1022" s="T106">dʼon</ta>
            <ta e="T108" id="Seg_1023" s="T107">bu-nI</ta>
            <ta e="T109" id="Seg_1024" s="T108">dʼe</ta>
            <ta e="T110" id="Seg_1025" s="T109">mataŋa</ta>
            <ta e="T111" id="Seg_1026" s="T110">kurduk</ta>
            <ta e="T112" id="Seg_1027" s="T111">kaːlaː-An</ta>
            <ta e="T113" id="Seg_1028" s="T112">ahaː-A-t-Ar-LAr</ta>
            <ta e="T114" id="Seg_1029" s="T113">bütej</ta>
            <ta e="T115" id="Seg_1030" s="T114">kihi</ta>
            <ta e="T116" id="Seg_1031" s="T115">kajdak</ta>
            <ta e="T117" id="Seg_1032" s="T116">bu͡ol-IAK.[tA]=Ij</ta>
            <ta e="T118" id="Seg_1033" s="T117">h-onno</ta>
            <ta e="T119" id="Seg_1034" s="T118">öl-An</ta>
            <ta e="T120" id="Seg_1035" s="T119">kaːl-BIT</ta>
            <ta e="T121" id="Seg_1036" s="T120">onnuk</ta>
            <ta e="T122" id="Seg_1037" s="T121">kaja</ta>
            <ta e="T123" id="Seg_1038" s="T122">ičči-tI-ttAn</ta>
            <ta e="T124" id="Seg_1039" s="T123">ogo</ta>
            <ta e="T125" id="Seg_1040" s="T124">kel-An</ta>
            <ta e="T126" id="Seg_1041" s="T125">bar-BIT</ta>
            <ta e="T127" id="Seg_1042" s="T126">di͡e-An</ta>
            <ta e="T128" id="Seg_1043" s="T127">bɨlɨrgɨ-LAr</ta>
            <ta e="T129" id="Seg_1044" s="T128">kepseː-Ar-LAr</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_1045" s="T0">long.ago</ta>
            <ta e="T2" id="Seg_1046" s="T1">old.man-PROPR</ta>
            <ta e="T3" id="Seg_1047" s="T2">old.woman.[NOM]</ta>
            <ta e="T4" id="Seg_1048" s="T3">alone-INTNS</ta>
            <ta e="T5" id="Seg_1049" s="T4">live-PST2-3PL</ta>
            <ta e="T6" id="Seg_1050" s="T5">always</ta>
            <ta e="T7" id="Seg_1051" s="T6">child-POSS</ta>
            <ta e="T8" id="Seg_1052" s="T7">NEG-3PL</ta>
            <ta e="T9" id="Seg_1053" s="T8">once</ta>
            <ta e="T10" id="Seg_1054" s="T9">stand-CVB.SEQ</ta>
            <ta e="T11" id="Seg_1055" s="T10">old.man.[NOM]</ta>
            <ta e="T12" id="Seg_1056" s="T11">speak-PST2.[3SG]</ta>
            <ta e="T13" id="Seg_1057" s="T12">old.woman-3SG-DAT/LOC</ta>
            <ta e="T14" id="Seg_1058" s="T13">old.woman.[NOM]</ta>
            <ta e="T15" id="Seg_1059" s="T14">1PL.[NOM]</ta>
            <ta e="T16" id="Seg_1060" s="T15">child-VBZ-PTCP.FUT-1PL-DAT/LOC</ta>
            <ta e="T17" id="Seg_1061" s="T16">better.[NOM]</ta>
            <ta e="T18" id="Seg_1062" s="T17">that-DAT/LOC</ta>
            <ta e="T19" id="Seg_1063" s="T18">old.woman-3SG.[NOM]</ta>
            <ta e="T20" id="Seg_1064" s="T19">speak-PST2.[3SG]</ta>
            <ta e="T21" id="Seg_1065" s="T20">youth-1PL-DAT/LOC</ta>
            <ta e="T22" id="Seg_1066" s="T21">child-VBZ-PST2.NEG-1PL</ta>
            <ta e="T23" id="Seg_1067" s="T22">how</ta>
            <ta e="T24" id="Seg_1068" s="T23">now</ta>
            <ta e="T25" id="Seg_1069" s="T24">child-VBZ-FUT-1PL=Q</ta>
            <ta e="T26" id="Seg_1070" s="T25">that.[NOM]</ta>
            <ta e="T27" id="Seg_1071" s="T26">be.possible-FUT.[3SG]</ta>
            <ta e="T28" id="Seg_1072" s="T27">Q</ta>
            <ta e="T29" id="Seg_1073" s="T28">say-PST2.[3SG]</ta>
            <ta e="T30" id="Seg_1074" s="T29">that-ACC</ta>
            <ta e="T31" id="Seg_1075" s="T30">old.man-3SG.[NOM]</ta>
            <ta e="T32" id="Seg_1076" s="T31">speak-PST2.[3SG]</ta>
            <ta e="T33" id="Seg_1077" s="T32">1SG.[NOM]</ta>
            <ta e="T34" id="Seg_1078" s="T33">human.being.[NOM]</ta>
            <ta e="T35" id="Seg_1079" s="T34">this-PL-3SG-ABL</ta>
            <ta e="T36" id="Seg_1080" s="T35">different</ta>
            <ta e="T37" id="Seg_1081" s="T36">child-VBZ-CVB.PURP</ta>
            <ta e="T38" id="Seg_1082" s="T37">want-PRS-1SG</ta>
            <ta e="T39" id="Seg_1083" s="T38">say-PST2.[3SG]</ta>
            <ta e="T40" id="Seg_1084" s="T39">old.woman-3SG.[NOM]</ta>
            <ta e="T41" id="Seg_1085" s="T40">say-PST2.[3SG]</ta>
            <ta e="T42" id="Seg_1086" s="T41">child-VBZ-PTCP.PRS-DAT/LOC</ta>
            <ta e="T43" id="Seg_1087" s="T42">what.[NOM]</ta>
            <ta e="T44" id="Seg_1088" s="T43">badly</ta>
            <ta e="T45" id="Seg_1089" s="T44">be-FUT.[3SG]=Q</ta>
            <ta e="T46" id="Seg_1090" s="T45">such.[NOM]</ta>
            <ta e="T47" id="Seg_1091" s="T46">though</ta>
            <ta e="T48" id="Seg_1092" s="T47">prepare-INFER-EP-2SG</ta>
            <ta e="T49" id="Seg_1093" s="T48">EMPH</ta>
            <ta e="T50" id="Seg_1094" s="T49">this-DAT/LOC</ta>
            <ta e="T51" id="Seg_1095" s="T50">old.man-3SG.[NOM]</ta>
            <ta e="T52" id="Seg_1096" s="T51">net-ACC</ta>
            <ta e="T53" id="Seg_1097" s="T52">take-PST2.[3SG]</ta>
            <ta e="T54" id="Seg_1098" s="T53">partridge-DAT/LOC</ta>
            <ta e="T55" id="Seg_1099" s="T54">place-PTCP.PRS-3PL-ACC</ta>
            <ta e="T56" id="Seg_1100" s="T55">like</ta>
            <ta e="T57" id="Seg_1101" s="T56">shore-DAT/LOC</ta>
            <ta e="T58" id="Seg_1102" s="T57">across</ta>
            <ta e="T59" id="Seg_1103" s="T58">wood-VBZ-CVB.SEQ</ta>
            <ta e="T60" id="Seg_1104" s="T59">place-CVB.SEQ</ta>
            <ta e="T61" id="Seg_1105" s="T60">throw-PST2.[3SG]</ta>
            <ta e="T62" id="Seg_1106" s="T61">mountain.[NOM]</ta>
            <ta e="T63" id="Seg_1107" s="T62">to</ta>
            <ta e="T64" id="Seg_1108" s="T63">push-CVB.SEQ</ta>
            <ta e="T65" id="Seg_1109" s="T64">this</ta>
            <ta e="T66" id="Seg_1110" s="T65">make-CVB.SEQ</ta>
            <ta e="T67" id="Seg_1111" s="T66">after</ta>
            <ta e="T68" id="Seg_1112" s="T67">net-3SG-GEN</ta>
            <ta e="T69" id="Seg_1113" s="T68">inside-3SG-DAT/LOC</ta>
            <ta e="T70" id="Seg_1114" s="T69">go.in-CVB.SEQ</ta>
            <ta e="T71" id="Seg_1115" s="T70">jump-CVB.SIM-jump-CVB.SIM</ta>
            <ta e="T72" id="Seg_1116" s="T71">oho-ehee</ta>
            <ta e="T73" id="Seg_1117" s="T72">say-CVB.SEQ</ta>
            <ta e="T74" id="Seg_1118" s="T73">shout-CVB.SIM-shout-CVB.SIM</ta>
            <ta e="T75" id="Seg_1119" s="T74">child-SIM</ta>
            <ta e="T76" id="Seg_1120" s="T75">roar-CVB.SIM-roar-CVB.SIM</ta>
            <ta e="T77" id="Seg_1121" s="T76">most</ta>
            <ta e="T78" id="Seg_1122" s="T77">last-3SG-INSTR</ta>
            <ta e="T79" id="Seg_1123" s="T78">jump-PRS.[3SG]</ta>
            <ta e="T80" id="Seg_1124" s="T79">like.that</ta>
            <ta e="T81" id="Seg_1125" s="T80">three</ta>
            <ta e="T82" id="Seg_1126" s="T81">day.[NOM]</ta>
            <ta e="T83" id="Seg_1127" s="T82">custom-VBZ-PRS.[3SG]</ta>
            <ta e="T84" id="Seg_1128" s="T83">three-ORD</ta>
            <ta e="T85" id="Seg_1129" s="T84">day-3SG-DAT/LOC</ta>
            <ta e="T86" id="Seg_1130" s="T85">go.out-EP-PST2-3SG</ta>
            <ta e="T87" id="Seg_1131" s="T86">net-3SG-DAT/LOC</ta>
            <ta e="T88" id="Seg_1132" s="T87">boy.[NOM]</ta>
            <ta e="T89" id="Seg_1133" s="T88">child.[NOM]</ta>
            <ta e="T90" id="Seg_1134" s="T89">get.into-CVB.SEQ</ta>
            <ta e="T91" id="Seg_1135" s="T90">cry-CVB.SIM</ta>
            <ta e="T92" id="Seg_1136" s="T91">lie-PRS.[3SG]</ta>
            <ta e="T93" id="Seg_1137" s="T92">this-ACC</ta>
            <ta e="T94" id="Seg_1138" s="T93">old.man.[NOM]</ta>
            <ta e="T95" id="Seg_1139" s="T94">house-3SG-DAT/LOC</ta>
            <ta e="T96" id="Seg_1140" s="T95">bring-PRS.[3SG]</ta>
            <ta e="T97" id="Seg_1141" s="T96">house-DAT/LOC</ta>
            <ta e="T98" id="Seg_1142" s="T97">go.in-PST2-3PL</ta>
            <ta e="T99" id="Seg_1143" s="T98">this</ta>
            <ta e="T100" id="Seg_1144" s="T99">child-3PL.[NOM]</ta>
            <ta e="T101" id="Seg_1145" s="T100">penis-3SG.[NOM]</ta>
            <ta e="T102" id="Seg_1146" s="T101">and</ta>
            <ta e="T103" id="Seg_1147" s="T102">anus-3SG.[NOM]</ta>
            <ta e="T104" id="Seg_1148" s="T103">and</ta>
            <ta e="T105" id="Seg_1149" s="T104">complete.[NOM]</ta>
            <ta e="T106" id="Seg_1150" s="T105">be.happy-PTCP.PST</ta>
            <ta e="T107" id="Seg_1151" s="T106">people.[NOM]</ta>
            <ta e="T108" id="Seg_1152" s="T107">this-ACC</ta>
            <ta e="T109" id="Seg_1153" s="T108">well</ta>
            <ta e="T110" id="Seg_1154" s="T109">bag.[NOM]</ta>
            <ta e="T111" id="Seg_1155" s="T110">like</ta>
            <ta e="T112" id="Seg_1156" s="T111">lay-CVB.SEQ</ta>
            <ta e="T113" id="Seg_1157" s="T112">eat-EP-CAUS-PRS-3PL</ta>
            <ta e="T114" id="Seg_1158" s="T113">complete</ta>
            <ta e="T115" id="Seg_1159" s="T114">human.being.[NOM]</ta>
            <ta e="T116" id="Seg_1160" s="T115">how</ta>
            <ta e="T117" id="Seg_1161" s="T116">be-FUT.[3SG]=Q</ta>
            <ta e="T118" id="Seg_1162" s="T117">EMPH-there</ta>
            <ta e="T119" id="Seg_1163" s="T118">die-CVB.SEQ</ta>
            <ta e="T120" id="Seg_1164" s="T119">stay-PST2.[3SG]</ta>
            <ta e="T121" id="Seg_1165" s="T120">such</ta>
            <ta e="T122" id="Seg_1166" s="T121">mountain.[NOM]</ta>
            <ta e="T123" id="Seg_1167" s="T122">master-3SG-ABL</ta>
            <ta e="T124" id="Seg_1168" s="T123">child.[NOM]</ta>
            <ta e="T125" id="Seg_1169" s="T124">come-CVB.SEQ</ta>
            <ta e="T126" id="Seg_1170" s="T125">go-PST2.[3SG]</ta>
            <ta e="T127" id="Seg_1171" s="T126">say-CVB.SEQ</ta>
            <ta e="T128" id="Seg_1172" s="T127">ancestor-PL.[NOM]</ta>
            <ta e="T129" id="Seg_1173" s="T128">tell-PRS-3PL</ta>
         </annotation>
         <annotation name="gg" tierref="gg">
            <ta e="T1" id="Seg_1174" s="T0">vor.langer.Zeit</ta>
            <ta e="T2" id="Seg_1175" s="T1">alter.Mann-PROPR</ta>
            <ta e="T3" id="Seg_1176" s="T2">Alte.[NOM]</ta>
            <ta e="T4" id="Seg_1177" s="T3">alleine-INTNS</ta>
            <ta e="T5" id="Seg_1178" s="T4">leben-PST2-3PL</ta>
            <ta e="T6" id="Seg_1179" s="T5">immer</ta>
            <ta e="T7" id="Seg_1180" s="T6">Kind-POSS</ta>
            <ta e="T8" id="Seg_1181" s="T7">NEG-3PL</ta>
            <ta e="T9" id="Seg_1182" s="T8">einmal</ta>
            <ta e="T10" id="Seg_1183" s="T9">stehen-CVB.SEQ</ta>
            <ta e="T11" id="Seg_1184" s="T10">alter.Mann.[NOM]</ta>
            <ta e="T12" id="Seg_1185" s="T11">sprechen-PST2.[3SG]</ta>
            <ta e="T13" id="Seg_1186" s="T12">Alte-3SG-DAT/LOC</ta>
            <ta e="T14" id="Seg_1187" s="T13">Alte.[NOM]</ta>
            <ta e="T15" id="Seg_1188" s="T14">1PL.[NOM]</ta>
            <ta e="T16" id="Seg_1189" s="T15">Kind-VBZ-PTCP.FUT-1PL-DAT/LOC</ta>
            <ta e="T17" id="Seg_1190" s="T16">besser.[NOM]</ta>
            <ta e="T18" id="Seg_1191" s="T17">jenes-DAT/LOC</ta>
            <ta e="T19" id="Seg_1192" s="T18">Alte-3SG.[NOM]</ta>
            <ta e="T20" id="Seg_1193" s="T19">sprechen-PST2.[3SG]</ta>
            <ta e="T21" id="Seg_1194" s="T20">Jugend-1PL-DAT/LOC</ta>
            <ta e="T22" id="Seg_1195" s="T21">Kind-VBZ-PST2.NEG-1PL</ta>
            <ta e="T23" id="Seg_1196" s="T22">wie</ta>
            <ta e="T24" id="Seg_1197" s="T23">jetzt</ta>
            <ta e="T25" id="Seg_1198" s="T24">Kind-VBZ-FUT-1PL=Q</ta>
            <ta e="T26" id="Seg_1199" s="T25">jenes.[NOM]</ta>
            <ta e="T27" id="Seg_1200" s="T26">möglich.sein-FUT.[3SG]</ta>
            <ta e="T28" id="Seg_1201" s="T27">Q</ta>
            <ta e="T29" id="Seg_1202" s="T28">sagen-PST2.[3SG]</ta>
            <ta e="T30" id="Seg_1203" s="T29">jenes-ACC</ta>
            <ta e="T31" id="Seg_1204" s="T30">alter.Mann-3SG.[NOM]</ta>
            <ta e="T32" id="Seg_1205" s="T31">sprechen-PST2.[3SG]</ta>
            <ta e="T33" id="Seg_1206" s="T32">1SG.[NOM]</ta>
            <ta e="T34" id="Seg_1207" s="T33">Mensch.[NOM]</ta>
            <ta e="T35" id="Seg_1208" s="T34">dieses-PL-3SG-ABL</ta>
            <ta e="T36" id="Seg_1209" s="T35">anders</ta>
            <ta e="T37" id="Seg_1210" s="T36">Kind-VBZ-CVB.PURP</ta>
            <ta e="T38" id="Seg_1211" s="T37">wollen-PRS-1SG</ta>
            <ta e="T39" id="Seg_1212" s="T38">sagen-PST2.[3SG]</ta>
            <ta e="T40" id="Seg_1213" s="T39">Alte-3SG.[NOM]</ta>
            <ta e="T41" id="Seg_1214" s="T40">sagen-PST2.[3SG]</ta>
            <ta e="T42" id="Seg_1215" s="T41">Kind-VBZ-PTCP.PRS-DAT/LOC</ta>
            <ta e="T43" id="Seg_1216" s="T42">was.[NOM]</ta>
            <ta e="T44" id="Seg_1217" s="T43">schlecht</ta>
            <ta e="T45" id="Seg_1218" s="T44">sein-FUT.[3SG]=Q</ta>
            <ta e="T46" id="Seg_1219" s="T45">solch.[NOM]</ta>
            <ta e="T47" id="Seg_1220" s="T46">aber</ta>
            <ta e="T48" id="Seg_1221" s="T47">vorbereiten-INFER-EP-2SG</ta>
            <ta e="T49" id="Seg_1222" s="T48">EMPH</ta>
            <ta e="T50" id="Seg_1223" s="T49">dieses-DAT/LOC</ta>
            <ta e="T51" id="Seg_1224" s="T50">alter.Mann-3SG.[NOM]</ta>
            <ta e="T52" id="Seg_1225" s="T51">Netz-ACC</ta>
            <ta e="T53" id="Seg_1226" s="T52">nehmen-PST2.[3SG]</ta>
            <ta e="T54" id="Seg_1227" s="T53">Rebhuhn-DAT/LOC</ta>
            <ta e="T55" id="Seg_1228" s="T54">stellen-PTCP.PRS-3PL-ACC</ta>
            <ta e="T56" id="Seg_1229" s="T55">wie</ta>
            <ta e="T57" id="Seg_1230" s="T56">Ufer-DAT/LOC</ta>
            <ta e="T58" id="Seg_1231" s="T57">quer.über</ta>
            <ta e="T59" id="Seg_1232" s="T58">Holz-VBZ-CVB.SEQ</ta>
            <ta e="T60" id="Seg_1233" s="T59">stellen-CVB.SEQ</ta>
            <ta e="T61" id="Seg_1234" s="T60">werfen-PST2.[3SG]</ta>
            <ta e="T62" id="Seg_1235" s="T61">Berg.[NOM]</ta>
            <ta e="T63" id="Seg_1236" s="T62">zu</ta>
            <ta e="T64" id="Seg_1237" s="T63">stoßen-CVB.SEQ</ta>
            <ta e="T65" id="Seg_1238" s="T64">dieses</ta>
            <ta e="T66" id="Seg_1239" s="T65">machen-CVB.SEQ</ta>
            <ta e="T67" id="Seg_1240" s="T66">nachdem</ta>
            <ta e="T68" id="Seg_1241" s="T67">Netz-3SG-GEN</ta>
            <ta e="T69" id="Seg_1242" s="T68">Inneres-3SG-DAT/LOC</ta>
            <ta e="T70" id="Seg_1243" s="T69">hineingehen-CVB.SEQ</ta>
            <ta e="T71" id="Seg_1244" s="T70">springen-CVB.SIM-springen-CVB.SIM</ta>
            <ta e="T72" id="Seg_1245" s="T71">oho-ehee</ta>
            <ta e="T73" id="Seg_1246" s="T72">sagen-CVB.SEQ</ta>
            <ta e="T74" id="Seg_1247" s="T73">schreien-CVB.SIM-schreien-CVB.SIM</ta>
            <ta e="T75" id="Seg_1248" s="T74">Kind-SIM</ta>
            <ta e="T76" id="Seg_1249" s="T75">brüllen-CVB.SIM-brüllen-CVB.SIM</ta>
            <ta e="T77" id="Seg_1250" s="T76">meist</ta>
            <ta e="T78" id="Seg_1251" s="T77">letzter-3SG-INSTR</ta>
            <ta e="T79" id="Seg_1252" s="T78">springen-PRS.[3SG]</ta>
            <ta e="T80" id="Seg_1253" s="T79">so</ta>
            <ta e="T81" id="Seg_1254" s="T80">drei</ta>
            <ta e="T82" id="Seg_1255" s="T81">Tag.[NOM]</ta>
            <ta e="T83" id="Seg_1256" s="T82">Sitte-VBZ-PRS.[3SG]</ta>
            <ta e="T84" id="Seg_1257" s="T83">drei-ORD</ta>
            <ta e="T85" id="Seg_1258" s="T84">Tag-3SG-DAT/LOC</ta>
            <ta e="T86" id="Seg_1259" s="T85">hinausgehen-EP-PST2-3SG</ta>
            <ta e="T87" id="Seg_1260" s="T86">Netz-3SG-DAT/LOC</ta>
            <ta e="T88" id="Seg_1261" s="T87">Junge.[NOM]</ta>
            <ta e="T89" id="Seg_1262" s="T88">Kind.[NOM]</ta>
            <ta e="T90" id="Seg_1263" s="T89">geraten-CVB.SEQ</ta>
            <ta e="T91" id="Seg_1264" s="T90">weinen-CVB.SIM</ta>
            <ta e="T92" id="Seg_1265" s="T91">liegen-PRS.[3SG]</ta>
            <ta e="T93" id="Seg_1266" s="T92">dieses-ACC</ta>
            <ta e="T94" id="Seg_1267" s="T93">alter.Mann.[NOM]</ta>
            <ta e="T95" id="Seg_1268" s="T94">Haus-3SG-DAT/LOC</ta>
            <ta e="T96" id="Seg_1269" s="T95">bringen-PRS.[3SG]</ta>
            <ta e="T97" id="Seg_1270" s="T96">Haus-DAT/LOC</ta>
            <ta e="T98" id="Seg_1271" s="T97">hineingehen-PST2-3PL</ta>
            <ta e="T99" id="Seg_1272" s="T98">dieses</ta>
            <ta e="T100" id="Seg_1273" s="T99">Kind-3PL.[NOM]</ta>
            <ta e="T101" id="Seg_1274" s="T100">Penis-3SG.[NOM]</ta>
            <ta e="T102" id="Seg_1275" s="T101">und</ta>
            <ta e="T103" id="Seg_1276" s="T102">After-3SG.[NOM]</ta>
            <ta e="T104" id="Seg_1277" s="T103">und</ta>
            <ta e="T105" id="Seg_1278" s="T104">vollständig.[NOM]</ta>
            <ta e="T106" id="Seg_1279" s="T105">sich.freuen-PTCP.PST</ta>
            <ta e="T107" id="Seg_1280" s="T106">Leute.[NOM]</ta>
            <ta e="T108" id="Seg_1281" s="T107">dieses-ACC</ta>
            <ta e="T109" id="Seg_1282" s="T108">doch</ta>
            <ta e="T110" id="Seg_1283" s="T109">Tasche.[NOM]</ta>
            <ta e="T111" id="Seg_1284" s="T110">wie</ta>
            <ta e="T112" id="Seg_1285" s="T111">legen-CVB.SEQ</ta>
            <ta e="T113" id="Seg_1286" s="T112">essen-EP-CAUS-PRS-3PL</ta>
            <ta e="T114" id="Seg_1287" s="T113">vollständig</ta>
            <ta e="T115" id="Seg_1288" s="T114">Mensch.[NOM]</ta>
            <ta e="T116" id="Seg_1289" s="T115">wie</ta>
            <ta e="T117" id="Seg_1290" s="T116">sein-FUT.[3SG]=Q</ta>
            <ta e="T118" id="Seg_1291" s="T117">EMPH-dort</ta>
            <ta e="T119" id="Seg_1292" s="T118">sterben-CVB.SEQ</ta>
            <ta e="T120" id="Seg_1293" s="T119">bleiben-PST2.[3SG]</ta>
            <ta e="T121" id="Seg_1294" s="T120">solch</ta>
            <ta e="T122" id="Seg_1295" s="T121">Berg.[NOM]</ta>
            <ta e="T123" id="Seg_1296" s="T122">Herr-3SG-ABL</ta>
            <ta e="T124" id="Seg_1297" s="T123">Kind.[NOM]</ta>
            <ta e="T125" id="Seg_1298" s="T124">kommen-CVB.SEQ</ta>
            <ta e="T126" id="Seg_1299" s="T125">gehen-PST2.[3SG]</ta>
            <ta e="T127" id="Seg_1300" s="T126">sagen-CVB.SEQ</ta>
            <ta e="T128" id="Seg_1301" s="T127">Vorfahre-PL.[NOM]</ta>
            <ta e="T129" id="Seg_1302" s="T128">erzählen-PRS-3PL</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_1303" s="T0">давно</ta>
            <ta e="T2" id="Seg_1304" s="T1">старик-PROPR</ta>
            <ta e="T3" id="Seg_1305" s="T2">старуха.[NOM]</ta>
            <ta e="T4" id="Seg_1306" s="T3">одиноко-INTNS</ta>
            <ta e="T5" id="Seg_1307" s="T4">жить-PST2-3PL</ta>
            <ta e="T6" id="Seg_1308" s="T5">всегда</ta>
            <ta e="T7" id="Seg_1309" s="T6">ребенок-POSS</ta>
            <ta e="T8" id="Seg_1310" s="T7">NEG-3PL</ta>
            <ta e="T9" id="Seg_1311" s="T8">однажды</ta>
            <ta e="T10" id="Seg_1312" s="T9">стоять-CVB.SEQ</ta>
            <ta e="T11" id="Seg_1313" s="T10">старик.[NOM]</ta>
            <ta e="T12" id="Seg_1314" s="T11">говорить-PST2.[3SG]</ta>
            <ta e="T13" id="Seg_1315" s="T12">старуха-3SG-DAT/LOC</ta>
            <ta e="T14" id="Seg_1316" s="T13">старуха.[NOM]</ta>
            <ta e="T15" id="Seg_1317" s="T14">1PL.[NOM]</ta>
            <ta e="T16" id="Seg_1318" s="T15">ребенок-VBZ-PTCP.FUT-1PL-DAT/LOC</ta>
            <ta e="T17" id="Seg_1319" s="T16">лучше.[NOM]</ta>
            <ta e="T18" id="Seg_1320" s="T17">тот-DAT/LOC</ta>
            <ta e="T19" id="Seg_1321" s="T18">старуха-3SG.[NOM]</ta>
            <ta e="T20" id="Seg_1322" s="T19">говорить-PST2.[3SG]</ta>
            <ta e="T21" id="Seg_1323" s="T20">молодость-1PL-DAT/LOC</ta>
            <ta e="T22" id="Seg_1324" s="T21">ребенок-VBZ-PST2.NEG-1PL</ta>
            <ta e="T23" id="Seg_1325" s="T22">как</ta>
            <ta e="T24" id="Seg_1326" s="T23">сейчас</ta>
            <ta e="T25" id="Seg_1327" s="T24">ребенок-VBZ-FUT-1PL=Q</ta>
            <ta e="T26" id="Seg_1328" s="T25">тот.[NOM]</ta>
            <ta e="T27" id="Seg_1329" s="T26">быть.возможным-FUT.[3SG]</ta>
            <ta e="T28" id="Seg_1330" s="T27">Q</ta>
            <ta e="T29" id="Seg_1331" s="T28">говорить-PST2.[3SG]</ta>
            <ta e="T30" id="Seg_1332" s="T29">тот-ACC</ta>
            <ta e="T31" id="Seg_1333" s="T30">старик-3SG.[NOM]</ta>
            <ta e="T32" id="Seg_1334" s="T31">говорить-PST2.[3SG]</ta>
            <ta e="T33" id="Seg_1335" s="T32">1SG.[NOM]</ta>
            <ta e="T34" id="Seg_1336" s="T33">человек.[NOM]</ta>
            <ta e="T35" id="Seg_1337" s="T34">этот-PL-3SG-ABL</ta>
            <ta e="T36" id="Seg_1338" s="T35">по_другому</ta>
            <ta e="T37" id="Seg_1339" s="T36">ребенок-VBZ-CVB.PURP</ta>
            <ta e="T38" id="Seg_1340" s="T37">хотеть-PRS-1SG</ta>
            <ta e="T39" id="Seg_1341" s="T38">говорить-PST2.[3SG]</ta>
            <ta e="T40" id="Seg_1342" s="T39">старуха-3SG.[NOM]</ta>
            <ta e="T41" id="Seg_1343" s="T40">говорить-PST2.[3SG]</ta>
            <ta e="T42" id="Seg_1344" s="T41">ребенок-VBZ-PTCP.PRS-DAT/LOC</ta>
            <ta e="T43" id="Seg_1345" s="T42">что.[NOM]</ta>
            <ta e="T44" id="Seg_1346" s="T43">плохо</ta>
            <ta e="T45" id="Seg_1347" s="T44">быть-FUT.[3SG]=Q</ta>
            <ta e="T46" id="Seg_1348" s="T45">такой.[NOM]</ta>
            <ta e="T47" id="Seg_1349" s="T46">однако</ta>
            <ta e="T48" id="Seg_1350" s="T47">готовить-INFER-EP-2SG</ta>
            <ta e="T49" id="Seg_1351" s="T48">EMPH</ta>
            <ta e="T50" id="Seg_1352" s="T49">этот-DAT/LOC</ta>
            <ta e="T51" id="Seg_1353" s="T50">старик-3SG.[NOM]</ta>
            <ta e="T52" id="Seg_1354" s="T51">сеть-ACC</ta>
            <ta e="T53" id="Seg_1355" s="T52">взять-PST2.[3SG]</ta>
            <ta e="T54" id="Seg_1356" s="T53">куропатка-DAT/LOC</ta>
            <ta e="T55" id="Seg_1357" s="T54">ставить-PTCP.PRS-3PL-ACC</ta>
            <ta e="T56" id="Seg_1358" s="T55">как</ta>
            <ta e="T57" id="Seg_1359" s="T56">берег-DAT/LOC</ta>
            <ta e="T58" id="Seg_1360" s="T57">поперек</ta>
            <ta e="T59" id="Seg_1361" s="T58">дерево-VBZ-CVB.SEQ</ta>
            <ta e="T60" id="Seg_1362" s="T59">ставить-CVB.SEQ</ta>
            <ta e="T61" id="Seg_1363" s="T60">бросать-PST2.[3SG]</ta>
            <ta e="T62" id="Seg_1364" s="T61">гора.[NOM]</ta>
            <ta e="T63" id="Seg_1365" s="T62">к</ta>
            <ta e="T64" id="Seg_1366" s="T63">толкать-CVB.SEQ</ta>
            <ta e="T65" id="Seg_1367" s="T64">этот</ta>
            <ta e="T66" id="Seg_1368" s="T65">делать-CVB.SEQ</ta>
            <ta e="T67" id="Seg_1369" s="T66">после</ta>
            <ta e="T68" id="Seg_1370" s="T67">сеть-3SG-GEN</ta>
            <ta e="T69" id="Seg_1371" s="T68">нутро-3SG-DAT/LOC</ta>
            <ta e="T70" id="Seg_1372" s="T69">входить-CVB.SEQ</ta>
            <ta e="T71" id="Seg_1373" s="T70">прыгать-CVB.SIM-прыгать-CVB.SIM</ta>
            <ta e="T72" id="Seg_1374" s="T71">осоо-эсээ</ta>
            <ta e="T73" id="Seg_1375" s="T72">говорить-CVB.SEQ</ta>
            <ta e="T74" id="Seg_1376" s="T73">кричать-CVB.SIM-кричать-CVB.SIM</ta>
            <ta e="T75" id="Seg_1377" s="T74">ребенок-SIM</ta>
            <ta e="T76" id="Seg_1378" s="T75">реветь-CVB.SIM-реветь-CVB.SIM</ta>
            <ta e="T77" id="Seg_1379" s="T76">самый</ta>
            <ta e="T78" id="Seg_1380" s="T77">последний-3SG-INSTR</ta>
            <ta e="T79" id="Seg_1381" s="T78">прыгать-PRS.[3SG]</ta>
            <ta e="T80" id="Seg_1382" s="T79">так</ta>
            <ta e="T81" id="Seg_1383" s="T80">три</ta>
            <ta e="T82" id="Seg_1384" s="T81">день.[NOM]</ta>
            <ta e="T83" id="Seg_1385" s="T82">обычай-VBZ-PRS.[3SG]</ta>
            <ta e="T84" id="Seg_1386" s="T83">три-ORD</ta>
            <ta e="T85" id="Seg_1387" s="T84">день-3SG-DAT/LOC</ta>
            <ta e="T86" id="Seg_1388" s="T85">выйти-EP-PST2-3SG</ta>
            <ta e="T87" id="Seg_1389" s="T86">сеть-3SG-DAT/LOC</ta>
            <ta e="T88" id="Seg_1390" s="T87">мальчик.[NOM]</ta>
            <ta e="T89" id="Seg_1391" s="T88">ребенок.[NOM]</ta>
            <ta e="T90" id="Seg_1392" s="T89">попадать-CVB.SEQ</ta>
            <ta e="T91" id="Seg_1393" s="T90">плакать-CVB.SIM</ta>
            <ta e="T92" id="Seg_1394" s="T91">лежать-PRS.[3SG]</ta>
            <ta e="T93" id="Seg_1395" s="T92">этот-ACC</ta>
            <ta e="T94" id="Seg_1396" s="T93">старик.[NOM]</ta>
            <ta e="T95" id="Seg_1397" s="T94">дом-3SG-DAT/LOC</ta>
            <ta e="T96" id="Seg_1398" s="T95">принести-PRS.[3SG]</ta>
            <ta e="T97" id="Seg_1399" s="T96">дом-DAT/LOC</ta>
            <ta e="T98" id="Seg_1400" s="T97">входить-PST2-3PL</ta>
            <ta e="T99" id="Seg_1401" s="T98">этот</ta>
            <ta e="T100" id="Seg_1402" s="T99">ребенок-3PL.[NOM]</ta>
            <ta e="T101" id="Seg_1403" s="T100">пенис-3SG.[NOM]</ta>
            <ta e="T102" id="Seg_1404" s="T101">да</ta>
            <ta e="T103" id="Seg_1405" s="T102">задний.проход-3SG.[NOM]</ta>
            <ta e="T104" id="Seg_1406" s="T103">да</ta>
            <ta e="T105" id="Seg_1407" s="T104">полный.[NOM]</ta>
            <ta e="T106" id="Seg_1408" s="T105">радоваться-PTCP.PST</ta>
            <ta e="T107" id="Seg_1409" s="T106">люди.[NOM]</ta>
            <ta e="T108" id="Seg_1410" s="T107">этот-ACC</ta>
            <ta e="T109" id="Seg_1411" s="T108">вот</ta>
            <ta e="T110" id="Seg_1412" s="T109">сума.[NOM]</ta>
            <ta e="T111" id="Seg_1413" s="T110">как</ta>
            <ta e="T112" id="Seg_1414" s="T111">класть-CVB.SEQ</ta>
            <ta e="T113" id="Seg_1415" s="T112">есть-EP-CAUS-PRS-3PL</ta>
            <ta e="T114" id="Seg_1416" s="T113">полный</ta>
            <ta e="T115" id="Seg_1417" s="T114">человек.[NOM]</ta>
            <ta e="T116" id="Seg_1418" s="T115">как</ta>
            <ta e="T117" id="Seg_1419" s="T116">быть-FUT.[3SG]=Q</ta>
            <ta e="T118" id="Seg_1420" s="T117">EMPH-там</ta>
            <ta e="T119" id="Seg_1421" s="T118">умирать-CVB.SEQ</ta>
            <ta e="T120" id="Seg_1422" s="T119">оставаться-PST2.[3SG]</ta>
            <ta e="T121" id="Seg_1423" s="T120">такой</ta>
            <ta e="T122" id="Seg_1424" s="T121">гора.[NOM]</ta>
            <ta e="T123" id="Seg_1425" s="T122">господин-3SG-ABL</ta>
            <ta e="T124" id="Seg_1426" s="T123">ребенок.[NOM]</ta>
            <ta e="T125" id="Seg_1427" s="T124">приходить-CVB.SEQ</ta>
            <ta e="T126" id="Seg_1428" s="T125">идти-PST2.[3SG]</ta>
            <ta e="T127" id="Seg_1429" s="T126">говорить-CVB.SEQ</ta>
            <ta e="T128" id="Seg_1430" s="T127">предок-PL.[NOM]</ta>
            <ta e="T129" id="Seg_1431" s="T128">рассказывать-PRS-3PL</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_1432" s="T0">adv</ta>
            <ta e="T2" id="Seg_1433" s="T1">n-n&gt;adj</ta>
            <ta e="T3" id="Seg_1434" s="T2">n-n:case</ta>
            <ta e="T4" id="Seg_1435" s="T3">adv-adv&gt;adv</ta>
            <ta e="T5" id="Seg_1436" s="T4">v-v:tense-v:pred.pn</ta>
            <ta e="T6" id="Seg_1437" s="T5">adv</ta>
            <ta e="T7" id="Seg_1438" s="T6">n-n:(poss)</ta>
            <ta e="T8" id="Seg_1439" s="T7">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T9" id="Seg_1440" s="T8">adv</ta>
            <ta e="T10" id="Seg_1441" s="T9">v-v:cvb</ta>
            <ta e="T11" id="Seg_1442" s="T10">n-n:case</ta>
            <ta e="T12" id="Seg_1443" s="T11">v-v:tense-v:pred.pn</ta>
            <ta e="T13" id="Seg_1444" s="T12">n-n:poss-n:case</ta>
            <ta e="T14" id="Seg_1445" s="T13">n-n:case</ta>
            <ta e="T15" id="Seg_1446" s="T14">pers-pro:case</ta>
            <ta e="T16" id="Seg_1447" s="T15">n-n&gt;v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T17" id="Seg_1448" s="T16">adj-n:case</ta>
            <ta e="T18" id="Seg_1449" s="T17">dempro-pro:case</ta>
            <ta e="T19" id="Seg_1450" s="T18">n-n:(poss)-n:case</ta>
            <ta e="T20" id="Seg_1451" s="T19">v-v:tense-v:pred.pn</ta>
            <ta e="T21" id="Seg_1452" s="T20">n-n:poss-n:case</ta>
            <ta e="T22" id="Seg_1453" s="T21">n-n&gt;v-v:neg-v:pred.pn</ta>
            <ta e="T23" id="Seg_1454" s="T22">que</ta>
            <ta e="T24" id="Seg_1455" s="T23">adv</ta>
            <ta e="T25" id="Seg_1456" s="T24">n-n&gt;v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T26" id="Seg_1457" s="T25">dempro-pro:case</ta>
            <ta e="T27" id="Seg_1458" s="T26">v-v:tense-v:poss.pn</ta>
            <ta e="T28" id="Seg_1459" s="T27">ptcl</ta>
            <ta e="T29" id="Seg_1460" s="T28">v-v:tense-v:pred.pn</ta>
            <ta e="T30" id="Seg_1461" s="T29">dempro-pro:case</ta>
            <ta e="T31" id="Seg_1462" s="T30">n-n:(poss)-n:case</ta>
            <ta e="T32" id="Seg_1463" s="T31">v-v:tense-v:pred.pn</ta>
            <ta e="T33" id="Seg_1464" s="T32">pers-pro:case</ta>
            <ta e="T34" id="Seg_1465" s="T33">n-n:case</ta>
            <ta e="T35" id="Seg_1466" s="T34">dempro-pro:(num)-pro:(poss)-pro:case</ta>
            <ta e="T36" id="Seg_1467" s="T35">adv</ta>
            <ta e="T37" id="Seg_1468" s="T36">n-n&gt;v-v:cvb</ta>
            <ta e="T38" id="Seg_1469" s="T37">v-v:tense-v:pred.pn</ta>
            <ta e="T39" id="Seg_1470" s="T38">v-v:tense-v:pred.pn</ta>
            <ta e="T40" id="Seg_1471" s="T39">n-n:(poss)-n:case</ta>
            <ta e="T41" id="Seg_1472" s="T40">v-v:tense-v:pred.pn</ta>
            <ta e="T42" id="Seg_1473" s="T41">n-n&gt;v-v:ptcp-v:(case)</ta>
            <ta e="T43" id="Seg_1474" s="T42">que-pro:case</ta>
            <ta e="T44" id="Seg_1475" s="T43">adv</ta>
            <ta e="T45" id="Seg_1476" s="T44">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T46" id="Seg_1477" s="T45">dempro-pro:case</ta>
            <ta e="T47" id="Seg_1478" s="T46">ptcl</ta>
            <ta e="T48" id="Seg_1479" s="T47">v-v:mood-v:(ins)-v:poss.pn</ta>
            <ta e="T49" id="Seg_1480" s="T48">ptcl</ta>
            <ta e="T50" id="Seg_1481" s="T49">dempro-pro:case</ta>
            <ta e="T51" id="Seg_1482" s="T50">n-n:(poss)-n:case</ta>
            <ta e="T52" id="Seg_1483" s="T51">n-n:case</ta>
            <ta e="T53" id="Seg_1484" s="T52">v-v:tense-v:pred.pn</ta>
            <ta e="T54" id="Seg_1485" s="T53">n-n:case</ta>
            <ta e="T55" id="Seg_1486" s="T54">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T56" id="Seg_1487" s="T55">post</ta>
            <ta e="T57" id="Seg_1488" s="T56">n-n:case</ta>
            <ta e="T58" id="Seg_1489" s="T57">post</ta>
            <ta e="T59" id="Seg_1490" s="T58">n-n&gt;v-v:cvb</ta>
            <ta e="T60" id="Seg_1491" s="T59">v-v:cvb</ta>
            <ta e="T61" id="Seg_1492" s="T60">v-v:tense-v:pred.pn</ta>
            <ta e="T62" id="Seg_1493" s="T61">n-n:case</ta>
            <ta e="T63" id="Seg_1494" s="T62">post</ta>
            <ta e="T64" id="Seg_1495" s="T63">v-v:cvb</ta>
            <ta e="T65" id="Seg_1496" s="T64">dempro</ta>
            <ta e="T66" id="Seg_1497" s="T65">v-v:cvb</ta>
            <ta e="T67" id="Seg_1498" s="T66">post</ta>
            <ta e="T68" id="Seg_1499" s="T67">n-n:poss-n:case</ta>
            <ta e="T69" id="Seg_1500" s="T68">n-n:poss-n:case</ta>
            <ta e="T70" id="Seg_1501" s="T69">v-v:cvb</ta>
            <ta e="T71" id="Seg_1502" s="T70">v-v:cvb-v-v:cvb</ta>
            <ta e="T72" id="Seg_1503" s="T71">interj-interj</ta>
            <ta e="T73" id="Seg_1504" s="T72">v-v:cvb</ta>
            <ta e="T74" id="Seg_1505" s="T73">v-v:cvb-v-v:cvb</ta>
            <ta e="T75" id="Seg_1506" s="T74">n-n&gt;adv</ta>
            <ta e="T76" id="Seg_1507" s="T75">v-v:cvb-v-v:cvb</ta>
            <ta e="T77" id="Seg_1508" s="T76">ptcl</ta>
            <ta e="T78" id="Seg_1509" s="T77">adj-n:poss-n:case</ta>
            <ta e="T79" id="Seg_1510" s="T78">v-v:tense-v:pred.pn</ta>
            <ta e="T80" id="Seg_1511" s="T79">adv</ta>
            <ta e="T81" id="Seg_1512" s="T80">cardnum</ta>
            <ta e="T82" id="Seg_1513" s="T81">n-n:case</ta>
            <ta e="T83" id="Seg_1514" s="T82">n-n&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T84" id="Seg_1515" s="T83">cardnum-cardnum&gt;ordnum</ta>
            <ta e="T85" id="Seg_1516" s="T84">n-n:poss-n:case</ta>
            <ta e="T86" id="Seg_1517" s="T85">v-v:(ins)-v:tense-v:poss.pn</ta>
            <ta e="T87" id="Seg_1518" s="T86">n-n:poss-n:case</ta>
            <ta e="T88" id="Seg_1519" s="T87">n-n:case</ta>
            <ta e="T89" id="Seg_1520" s="T88">n-n:case</ta>
            <ta e="T90" id="Seg_1521" s="T89">v-v:cvb</ta>
            <ta e="T91" id="Seg_1522" s="T90">v-v:cvb</ta>
            <ta e="T92" id="Seg_1523" s="T91">v-v:tense-v:pred.pn</ta>
            <ta e="T93" id="Seg_1524" s="T92">dempro-pro:case</ta>
            <ta e="T94" id="Seg_1525" s="T93">n-n:case</ta>
            <ta e="T95" id="Seg_1526" s="T94">n-n:poss-n:case</ta>
            <ta e="T96" id="Seg_1527" s="T95">v-v:tense-v:pred.pn</ta>
            <ta e="T97" id="Seg_1528" s="T96">n-n:case</ta>
            <ta e="T98" id="Seg_1529" s="T97">v-v:tense-v:poss.pn</ta>
            <ta e="T99" id="Seg_1530" s="T98">dempro</ta>
            <ta e="T100" id="Seg_1531" s="T99">n-n:(poss)-n:case</ta>
            <ta e="T101" id="Seg_1532" s="T100">n-n:(poss)-n:case</ta>
            <ta e="T102" id="Seg_1533" s="T101">conj</ta>
            <ta e="T103" id="Seg_1534" s="T102">n-n:(poss)-n:case</ta>
            <ta e="T104" id="Seg_1535" s="T103">conj</ta>
            <ta e="T105" id="Seg_1536" s="T104">adj-n:case</ta>
            <ta e="T106" id="Seg_1537" s="T105">v-v:ptcp</ta>
            <ta e="T107" id="Seg_1538" s="T106">n-n:case</ta>
            <ta e="T108" id="Seg_1539" s="T107">dempro-pro:case</ta>
            <ta e="T109" id="Seg_1540" s="T108">ptcl</ta>
            <ta e="T110" id="Seg_1541" s="T109">n-n:case</ta>
            <ta e="T111" id="Seg_1542" s="T110">post</ta>
            <ta e="T112" id="Seg_1543" s="T111">v-v:cvb</ta>
            <ta e="T113" id="Seg_1544" s="T112">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T114" id="Seg_1545" s="T113">adj</ta>
            <ta e="T115" id="Seg_1546" s="T114">n-n:case</ta>
            <ta e="T116" id="Seg_1547" s="T115">que</ta>
            <ta e="T117" id="Seg_1548" s="T116">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T118" id="Seg_1549" s="T117">adv&gt;adv-adv</ta>
            <ta e="T119" id="Seg_1550" s="T118">v-v:cvb</ta>
            <ta e="T120" id="Seg_1551" s="T119">v-v:tense-v:pred.pn</ta>
            <ta e="T121" id="Seg_1552" s="T120">dempro</ta>
            <ta e="T122" id="Seg_1553" s="T121">n-n:case</ta>
            <ta e="T123" id="Seg_1554" s="T122">n-n:poss-n:case</ta>
            <ta e="T124" id="Seg_1555" s="T123">n-n:case</ta>
            <ta e="T125" id="Seg_1556" s="T124">v-v:cvb</ta>
            <ta e="T126" id="Seg_1557" s="T125">v-v:tense-v:pred.pn</ta>
            <ta e="T127" id="Seg_1558" s="T126">v-v:cvb</ta>
            <ta e="T128" id="Seg_1559" s="T127">n-n:(num)-n:case</ta>
            <ta e="T129" id="Seg_1560" s="T128">v-v:tense-v:pred.pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_1561" s="T0">adv</ta>
            <ta e="T2" id="Seg_1562" s="T1">adj</ta>
            <ta e="T3" id="Seg_1563" s="T2">n</ta>
            <ta e="T4" id="Seg_1564" s="T3">adv</ta>
            <ta e="T5" id="Seg_1565" s="T4">v</ta>
            <ta e="T6" id="Seg_1566" s="T5">adv</ta>
            <ta e="T7" id="Seg_1567" s="T6">n</ta>
            <ta e="T8" id="Seg_1568" s="T7">ptcl</ta>
            <ta e="T9" id="Seg_1569" s="T8">adv</ta>
            <ta e="T10" id="Seg_1570" s="T9">v</ta>
            <ta e="T11" id="Seg_1571" s="T10">n</ta>
            <ta e="T12" id="Seg_1572" s="T11">v</ta>
            <ta e="T13" id="Seg_1573" s="T12">n</ta>
            <ta e="T14" id="Seg_1574" s="T13">n</ta>
            <ta e="T15" id="Seg_1575" s="T14">pers</ta>
            <ta e="T16" id="Seg_1576" s="T15">v</ta>
            <ta e="T17" id="Seg_1577" s="T16">adj</ta>
            <ta e="T18" id="Seg_1578" s="T17">dempro</ta>
            <ta e="T19" id="Seg_1579" s="T18">n</ta>
            <ta e="T20" id="Seg_1580" s="T19">v</ta>
            <ta e="T21" id="Seg_1581" s="T20">n</ta>
            <ta e="T22" id="Seg_1582" s="T21">v</ta>
            <ta e="T23" id="Seg_1583" s="T22">que</ta>
            <ta e="T24" id="Seg_1584" s="T23">adv</ta>
            <ta e="T25" id="Seg_1585" s="T24">v</ta>
            <ta e="T26" id="Seg_1586" s="T25">dempro</ta>
            <ta e="T27" id="Seg_1587" s="T26">v</ta>
            <ta e="T28" id="Seg_1588" s="T27">ptcl</ta>
            <ta e="T29" id="Seg_1589" s="T28">v</ta>
            <ta e="T30" id="Seg_1590" s="T29">dempro</ta>
            <ta e="T31" id="Seg_1591" s="T30">n</ta>
            <ta e="T32" id="Seg_1592" s="T31">v</ta>
            <ta e="T33" id="Seg_1593" s="T32">pers</ta>
            <ta e="T34" id="Seg_1594" s="T33">n</ta>
            <ta e="T35" id="Seg_1595" s="T34">dempro</ta>
            <ta e="T36" id="Seg_1596" s="T35">adv</ta>
            <ta e="T37" id="Seg_1597" s="T36">v</ta>
            <ta e="T38" id="Seg_1598" s="T37">v</ta>
            <ta e="T39" id="Seg_1599" s="T38">v</ta>
            <ta e="T40" id="Seg_1600" s="T39">n</ta>
            <ta e="T41" id="Seg_1601" s="T40">v</ta>
            <ta e="T42" id="Seg_1602" s="T41">v</ta>
            <ta e="T43" id="Seg_1603" s="T42">que</ta>
            <ta e="T44" id="Seg_1604" s="T43">adv</ta>
            <ta e="T45" id="Seg_1605" s="T44">cop</ta>
            <ta e="T46" id="Seg_1606" s="T45">dempro</ta>
            <ta e="T47" id="Seg_1607" s="T46">ptcl</ta>
            <ta e="T48" id="Seg_1608" s="T47">v</ta>
            <ta e="T49" id="Seg_1609" s="T48">ptcl</ta>
            <ta e="T50" id="Seg_1610" s="T49">dempro</ta>
            <ta e="T51" id="Seg_1611" s="T50">n</ta>
            <ta e="T52" id="Seg_1612" s="T51">n</ta>
            <ta e="T53" id="Seg_1613" s="T52">v</ta>
            <ta e="T54" id="Seg_1614" s="T53">n</ta>
            <ta e="T55" id="Seg_1615" s="T54">v</ta>
            <ta e="T56" id="Seg_1616" s="T55">post</ta>
            <ta e="T57" id="Seg_1617" s="T56">n</ta>
            <ta e="T58" id="Seg_1618" s="T57">post</ta>
            <ta e="T59" id="Seg_1619" s="T58">v</ta>
            <ta e="T60" id="Seg_1620" s="T59">v</ta>
            <ta e="T61" id="Seg_1621" s="T60">aux</ta>
            <ta e="T62" id="Seg_1622" s="T61">n</ta>
            <ta e="T63" id="Seg_1623" s="T62">post</ta>
            <ta e="T64" id="Seg_1624" s="T63">v</ta>
            <ta e="T65" id="Seg_1625" s="T64">dempro</ta>
            <ta e="T66" id="Seg_1626" s="T65">v</ta>
            <ta e="T67" id="Seg_1627" s="T66">post</ta>
            <ta e="T68" id="Seg_1628" s="T67">n</ta>
            <ta e="T69" id="Seg_1629" s="T68">n</ta>
            <ta e="T70" id="Seg_1630" s="T69">v</ta>
            <ta e="T71" id="Seg_1631" s="T70">v</ta>
            <ta e="T72" id="Seg_1632" s="T71">interj</ta>
            <ta e="T73" id="Seg_1633" s="T72">v</ta>
            <ta e="T74" id="Seg_1634" s="T73">v</ta>
            <ta e="T75" id="Seg_1635" s="T74">adv</ta>
            <ta e="T76" id="Seg_1636" s="T75">v</ta>
            <ta e="T77" id="Seg_1637" s="T76">ptcl</ta>
            <ta e="T78" id="Seg_1638" s="T77">adj</ta>
            <ta e="T79" id="Seg_1639" s="T78">v</ta>
            <ta e="T80" id="Seg_1640" s="T79">adv</ta>
            <ta e="T81" id="Seg_1641" s="T80">cardnum</ta>
            <ta e="T82" id="Seg_1642" s="T81">n</ta>
            <ta e="T83" id="Seg_1643" s="T82">v</ta>
            <ta e="T84" id="Seg_1644" s="T83">ordnum</ta>
            <ta e="T85" id="Seg_1645" s="T84">n</ta>
            <ta e="T86" id="Seg_1646" s="T85">v</ta>
            <ta e="T87" id="Seg_1647" s="T86">n</ta>
            <ta e="T88" id="Seg_1648" s="T87">n</ta>
            <ta e="T89" id="Seg_1649" s="T88">n</ta>
            <ta e="T90" id="Seg_1650" s="T89">v</ta>
            <ta e="T91" id="Seg_1651" s="T90">v</ta>
            <ta e="T92" id="Seg_1652" s="T91">v</ta>
            <ta e="T93" id="Seg_1653" s="T92">dempro</ta>
            <ta e="T94" id="Seg_1654" s="T93">n</ta>
            <ta e="T95" id="Seg_1655" s="T94">n</ta>
            <ta e="T96" id="Seg_1656" s="T95">v</ta>
            <ta e="T97" id="Seg_1657" s="T96">n</ta>
            <ta e="T98" id="Seg_1658" s="T97">v</ta>
            <ta e="T99" id="Seg_1659" s="T98">dempro</ta>
            <ta e="T100" id="Seg_1660" s="T99">n</ta>
            <ta e="T101" id="Seg_1661" s="T100">n</ta>
            <ta e="T102" id="Seg_1662" s="T101">conj</ta>
            <ta e="T103" id="Seg_1663" s="T102">n</ta>
            <ta e="T104" id="Seg_1664" s="T103">conj</ta>
            <ta e="T105" id="Seg_1665" s="T104">adj</ta>
            <ta e="T106" id="Seg_1666" s="T105">v</ta>
            <ta e="T107" id="Seg_1667" s="T106">n</ta>
            <ta e="T108" id="Seg_1668" s="T107">dempro</ta>
            <ta e="T109" id="Seg_1669" s="T108">ptcl</ta>
            <ta e="T110" id="Seg_1670" s="T109">n</ta>
            <ta e="T111" id="Seg_1671" s="T110">post</ta>
            <ta e="T112" id="Seg_1672" s="T111">v</ta>
            <ta e="T113" id="Seg_1673" s="T112">v</ta>
            <ta e="T114" id="Seg_1674" s="T113">adj</ta>
            <ta e="T115" id="Seg_1675" s="T114">n</ta>
            <ta e="T116" id="Seg_1676" s="T115">que</ta>
            <ta e="T117" id="Seg_1677" s="T116">cop</ta>
            <ta e="T118" id="Seg_1678" s="T117">adv</ta>
            <ta e="T119" id="Seg_1679" s="T118">v</ta>
            <ta e="T120" id="Seg_1680" s="T119">aux</ta>
            <ta e="T121" id="Seg_1681" s="T120">dempro</ta>
            <ta e="T122" id="Seg_1682" s="T121">n</ta>
            <ta e="T123" id="Seg_1683" s="T122">n</ta>
            <ta e="T124" id="Seg_1684" s="T123">n</ta>
            <ta e="T125" id="Seg_1685" s="T124">v</ta>
            <ta e="T126" id="Seg_1686" s="T125">v</ta>
            <ta e="T127" id="Seg_1687" s="T126">v</ta>
            <ta e="T128" id="Seg_1688" s="T127">n</ta>
            <ta e="T129" id="Seg_1689" s="T128">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_1690" s="T1">np.h:Th</ta>
            <ta e="T3" id="Seg_1691" s="T2">np.h:Th</ta>
            <ta e="T7" id="Seg_1692" s="T6">np.h:Th</ta>
            <ta e="T8" id="Seg_1693" s="T7">0.3.h:Poss</ta>
            <ta e="T11" id="Seg_1694" s="T10">np.h:A</ta>
            <ta e="T13" id="Seg_1695" s="T12">0.3.h:Poss np.h:R</ta>
            <ta e="T15" id="Seg_1696" s="T14">pro.h:Th</ta>
            <ta e="T19" id="Seg_1697" s="T18">np.h:A</ta>
            <ta e="T21" id="Seg_1698" s="T20">n:Time</ta>
            <ta e="T22" id="Seg_1699" s="T21">0.1.h:Th</ta>
            <ta e="T24" id="Seg_1700" s="T23">adv:Time</ta>
            <ta e="T25" id="Seg_1701" s="T24">0.1.h:Th</ta>
            <ta e="T26" id="Seg_1702" s="T25">pro:Th</ta>
            <ta e="T29" id="Seg_1703" s="T28">0.3.h:A</ta>
            <ta e="T31" id="Seg_1704" s="T30">np.h:A</ta>
            <ta e="T33" id="Seg_1705" s="T32">pro.h:E</ta>
            <ta e="T37" id="Seg_1706" s="T36">0.1.h:Th</ta>
            <ta e="T39" id="Seg_1707" s="T38">0.3.h:A</ta>
            <ta e="T40" id="Seg_1708" s="T39">np.h:A</ta>
            <ta e="T48" id="Seg_1709" s="T47">0.2.h:A</ta>
            <ta e="T51" id="Seg_1710" s="T50">np.h:A</ta>
            <ta e="T52" id="Seg_1711" s="T51">np:Th</ta>
            <ta e="T54" id="Seg_1712" s="T53">np:Th</ta>
            <ta e="T55" id="Seg_1713" s="T54">0.3.h:A</ta>
            <ta e="T57" id="Seg_1714" s="T56">np:L</ta>
            <ta e="T61" id="Seg_1715" s="T60">0.3.h:A</ta>
            <ta e="T64" id="Seg_1716" s="T63">0.3.h:A</ta>
            <ta e="T68" id="Seg_1717" s="T67">np:Poss</ta>
            <ta e="T69" id="Seg_1718" s="T68">np:G</ta>
            <ta e="T70" id="Seg_1719" s="T69">0.3.h:A</ta>
            <ta e="T71" id="Seg_1720" s="T70">0.3.h:A</ta>
            <ta e="T73" id="Seg_1721" s="T72">0.3.h:A</ta>
            <ta e="T74" id="Seg_1722" s="T73">0.3.h:A</ta>
            <ta e="T76" id="Seg_1723" s="T75">0.3.h:A</ta>
            <ta e="T79" id="Seg_1724" s="T78">0.3.h:A</ta>
            <ta e="T82" id="Seg_1725" s="T81">n:Time</ta>
            <ta e="T83" id="Seg_1726" s="T82">0.3.h:A</ta>
            <ta e="T85" id="Seg_1727" s="T84">n:Time</ta>
            <ta e="T86" id="Seg_1728" s="T85">0.3.h:A</ta>
            <ta e="T87" id="Seg_1729" s="T86">np:L</ta>
            <ta e="T89" id="Seg_1730" s="T88">np.h:Th</ta>
            <ta e="T90" id="Seg_1731" s="T89">0.3.h:P</ta>
            <ta e="T91" id="Seg_1732" s="T90">0.3.h:A</ta>
            <ta e="T93" id="Seg_1733" s="T92">pro.h:Th</ta>
            <ta e="T94" id="Seg_1734" s="T93">np.h:A</ta>
            <ta e="T95" id="Seg_1735" s="T94">np:G</ta>
            <ta e="T97" id="Seg_1736" s="T96">np:G</ta>
            <ta e="T98" id="Seg_1737" s="T97">0.3.h:A</ta>
            <ta e="T100" id="Seg_1738" s="T99">np.h:Poss</ta>
            <ta e="T101" id="Seg_1739" s="T100">np:Th</ta>
            <ta e="T103" id="Seg_1740" s="T102">np:Th</ta>
            <ta e="T107" id="Seg_1741" s="T106">np.h:A</ta>
            <ta e="T108" id="Seg_1742" s="T107">pro.h:Th</ta>
            <ta e="T115" id="Seg_1743" s="T114">np.h:Th</ta>
            <ta e="T120" id="Seg_1744" s="T119">0.3.h:P</ta>
            <ta e="T123" id="Seg_1745" s="T122">np:So</ta>
            <ta e="T124" id="Seg_1746" s="T123">np.h:A</ta>
            <ta e="T127" id="Seg_1747" s="T126">0.3.h:A</ta>
            <ta e="T128" id="Seg_1748" s="T127">np.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T3" id="Seg_1749" s="T2">np.h:S</ta>
            <ta e="T5" id="Seg_1750" s="T4">v:pred</ta>
            <ta e="T8" id="Seg_1751" s="T7">0.3.h:S ptcl:pred</ta>
            <ta e="T11" id="Seg_1752" s="T10">np.h:S</ta>
            <ta e="T12" id="Seg_1753" s="T11">v:pred</ta>
            <ta e="T16" id="Seg_1754" s="T14">s:cond</ta>
            <ta e="T17" id="Seg_1755" s="T16">adj:pred</ta>
            <ta e="T19" id="Seg_1756" s="T18">np.h:S</ta>
            <ta e="T20" id="Seg_1757" s="T19">v:pred</ta>
            <ta e="T22" id="Seg_1758" s="T21">0.1.h:S v:pred</ta>
            <ta e="T25" id="Seg_1759" s="T24">0.1.h:S v:pred</ta>
            <ta e="T26" id="Seg_1760" s="T25">pro:S</ta>
            <ta e="T27" id="Seg_1761" s="T26">v:pred</ta>
            <ta e="T29" id="Seg_1762" s="T28">0.3.h:S v:pred</ta>
            <ta e="T31" id="Seg_1763" s="T30">np.h:S</ta>
            <ta e="T32" id="Seg_1764" s="T31">v:pred</ta>
            <ta e="T33" id="Seg_1765" s="T32">pro.h:S</ta>
            <ta e="T37" id="Seg_1766" s="T33">s:comp</ta>
            <ta e="T38" id="Seg_1767" s="T37">v:pred</ta>
            <ta e="T39" id="Seg_1768" s="T38">0.3.h:S v:pred</ta>
            <ta e="T40" id="Seg_1769" s="T39">np.h:S</ta>
            <ta e="T41" id="Seg_1770" s="T40">v:pred</ta>
            <ta e="T45" id="Seg_1771" s="T44">cop</ta>
            <ta e="T48" id="Seg_1772" s="T47">0.2.h:S v:pred</ta>
            <ta e="T51" id="Seg_1773" s="T50">np.h:S</ta>
            <ta e="T52" id="Seg_1774" s="T51">np:O</ta>
            <ta e="T53" id="Seg_1775" s="T52">v:pred</ta>
            <ta e="T56" id="Seg_1776" s="T53">s:adv</ta>
            <ta e="T61" id="Seg_1777" s="T60">0.3.h:S v:pred</ta>
            <ta e="T64" id="Seg_1778" s="T61">s:adv</ta>
            <ta e="T67" id="Seg_1779" s="T64">s:adv</ta>
            <ta e="T70" id="Seg_1780" s="T67">s:adv</ta>
            <ta e="T71" id="Seg_1781" s="T70">s:adv</ta>
            <ta e="T73" id="Seg_1782" s="T71">s:adv</ta>
            <ta e="T74" id="Seg_1783" s="T73">s:adv</ta>
            <ta e="T76" id="Seg_1784" s="T74">s:adv</ta>
            <ta e="T79" id="Seg_1785" s="T78">0.3.h:S v:pred</ta>
            <ta e="T83" id="Seg_1786" s="T82">0.3.h:S v:pred</ta>
            <ta e="T86" id="Seg_1787" s="T85">0.3.h:S v:pred</ta>
            <ta e="T89" id="Seg_1788" s="T88">np.h:S</ta>
            <ta e="T90" id="Seg_1789" s="T89">s:temp</ta>
            <ta e="T91" id="Seg_1790" s="T90">s:adv</ta>
            <ta e="T92" id="Seg_1791" s="T91">v:pred</ta>
            <ta e="T93" id="Seg_1792" s="T92">pro.h:O</ta>
            <ta e="T94" id="Seg_1793" s="T93">np.h:S</ta>
            <ta e="T96" id="Seg_1794" s="T95">v:pred</ta>
            <ta e="T98" id="Seg_1795" s="T97">0.3.h:S v:pred</ta>
            <ta e="T101" id="Seg_1796" s="T100">np:S</ta>
            <ta e="T103" id="Seg_1797" s="T102">np:S</ta>
            <ta e="T105" id="Seg_1798" s="T104">adj:pred</ta>
            <ta e="T107" id="Seg_1799" s="T106">np.h:S</ta>
            <ta e="T108" id="Seg_1800" s="T107">pro.h:O</ta>
            <ta e="T112" id="Seg_1801" s="T109">s:adv</ta>
            <ta e="T113" id="Seg_1802" s="T112">v:pred</ta>
            <ta e="T115" id="Seg_1803" s="T114">np.h:S</ta>
            <ta e="T117" id="Seg_1804" s="T116">cop</ta>
            <ta e="T120" id="Seg_1805" s="T119">0.3.h:S v:pred</ta>
            <ta e="T124" id="Seg_1806" s="T123">np.h:S</ta>
            <ta e="T126" id="Seg_1807" s="T125">v:pred</ta>
            <ta e="T127" id="Seg_1808" s="T126">s:adv</ta>
            <ta e="T128" id="Seg_1809" s="T127">np.h:S</ta>
            <ta e="T129" id="Seg_1810" s="T128">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST">
            <ta e="T2" id="Seg_1811" s="T1">new</ta>
            <ta e="T3" id="Seg_1812" s="T2">new</ta>
            <ta e="T8" id="Seg_1813" s="T7">0.accs-aggr</ta>
            <ta e="T11" id="Seg_1814" s="T10">giv-active</ta>
            <ta e="T12" id="Seg_1815" s="T11">quot-sp</ta>
            <ta e="T13" id="Seg_1816" s="T12">giv-active</ta>
            <ta e="T14" id="Seg_1817" s="T13">giv-active-Q</ta>
            <ta e="T15" id="Seg_1818" s="T14">giv-active-Q</ta>
            <ta e="T19" id="Seg_1819" s="T18">giv-active</ta>
            <ta e="T20" id="Seg_1820" s="T19">quot-sp</ta>
            <ta e="T21" id="Seg_1821" s="T20">accs-inf-Q</ta>
            <ta e="T22" id="Seg_1822" s="T21">0.giv-active-Q</ta>
            <ta e="T25" id="Seg_1823" s="T24">0.giv-active-Q</ta>
            <ta e="T29" id="Seg_1824" s="T28">0.giv-inactive 0.quot-sp</ta>
            <ta e="T31" id="Seg_1825" s="T30">giv-inactive</ta>
            <ta e="T32" id="Seg_1826" s="T31">quot-sp</ta>
            <ta e="T33" id="Seg_1827" s="T32">giv-active-Q</ta>
            <ta e="T37" id="Seg_1828" s="T36">0.giv-active-Q</ta>
            <ta e="T39" id="Seg_1829" s="T38">0.giv-active 0.quot-sp</ta>
            <ta e="T40" id="Seg_1830" s="T39">giv-inactive</ta>
            <ta e="T41" id="Seg_1831" s="T40">quot-sp</ta>
            <ta e="T48" id="Seg_1832" s="T47">0.giv-inactive-Q</ta>
            <ta e="T51" id="Seg_1833" s="T50">giv-active</ta>
            <ta e="T52" id="Seg_1834" s="T51">new</ta>
            <ta e="T57" id="Seg_1835" s="T56">new</ta>
            <ta e="T61" id="Seg_1836" s="T60">0.giv-active</ta>
            <ta e="T62" id="Seg_1837" s="T61">new</ta>
            <ta e="T64" id="Seg_1838" s="T63">0.giv-active</ta>
            <ta e="T68" id="Seg_1839" s="T67">giv-active</ta>
            <ta e="T69" id="Seg_1840" s="T68">accs-inf</ta>
            <ta e="T70" id="Seg_1841" s="T69">0.giv-active</ta>
            <ta e="T71" id="Seg_1842" s="T70">0.giv-active</ta>
            <ta e="T73" id="Seg_1843" s="T72">0.giv-active</ta>
            <ta e="T74" id="Seg_1844" s="T73">0.giv-active</ta>
            <ta e="T76" id="Seg_1845" s="T75">0.giv-active</ta>
            <ta e="T79" id="Seg_1846" s="T78">0.giv-active</ta>
            <ta e="T83" id="Seg_1847" s="T82">0.giv-active</ta>
            <ta e="T86" id="Seg_1848" s="T85">0.giv-active</ta>
            <ta e="T87" id="Seg_1849" s="T86">giv-inactive</ta>
            <ta e="T89" id="Seg_1850" s="T88">new</ta>
            <ta e="T90" id="Seg_1851" s="T89">0.giv-active</ta>
            <ta e="T91" id="Seg_1852" s="T90">0.giv-active</ta>
            <ta e="T93" id="Seg_1853" s="T92">giv-active</ta>
            <ta e="T94" id="Seg_1854" s="T93">giv-active</ta>
            <ta e="T98" id="Seg_1855" s="T97">0.accs-aggr</ta>
            <ta e="T100" id="Seg_1856" s="T99">giv-active</ta>
            <ta e="T101" id="Seg_1857" s="T100">accs-inf</ta>
            <ta e="T103" id="Seg_1858" s="T102">accs-inf</ta>
            <ta e="T107" id="Seg_1859" s="T106">accs-gen</ta>
            <ta e="T108" id="Seg_1860" s="T107">giv-active</ta>
            <ta e="T120" id="Seg_1861" s="T119">0.giv-active</ta>
            <ta e="T123" id="Seg_1862" s="T122">accs-gen-Q</ta>
            <ta e="T124" id="Seg_1863" s="T123">giv-active-Q</ta>
            <ta e="T127" id="Seg_1864" s="T126">0.quot-sp</ta>
            <ta e="T128" id="Seg_1865" s="T127">accs-inf</ta>
         </annotation>
         <annotation name="Top" tierref="Top">
            <ta e="T1" id="Seg_1866" s="T0">top.int.concr</ta>
            <ta e="T8" id="Seg_1867" s="T7">0.top.int.concr</ta>
            <ta e="T12" id="Seg_1868" s="T11">0.top.int.abstr.</ta>
            <ta e="T17" id="Seg_1869" s="T16">0.top.int.abstr</ta>
            <ta e="T18" id="Seg_1870" s="T17">top.int.concr</ta>
            <ta e="T21" id="Seg_1871" s="T20">top.int.concr</ta>
            <ta e="T26" id="Seg_1872" s="T25">top.int.concr</ta>
            <ta e="T32" id="Seg_1873" s="T31">0.top.int.concr</ta>
            <ta e="T33" id="Seg_1874" s="T32">top.int.concr</ta>
            <ta e="T40" id="Seg_1875" s="T39">top.int.concr</ta>
            <ta e="T53" id="Seg_1876" s="T52">0.top.int.abstr.</ta>
            <ta e="T61" id="Seg_1877" s="T60">0.top.int.concr</ta>
            <ta e="T79" id="Seg_1878" s="T78">0.top.int.concr</ta>
            <ta e="T83" id="Seg_1879" s="T82">0.top.int.concr</ta>
            <ta e="T85" id="Seg_1880" s="T83">top.int.concr</ta>
            <ta e="T87" id="Seg_1881" s="T86">top.int.concr</ta>
            <ta e="T93" id="Seg_1882" s="T92">top.int.concr</ta>
            <ta e="T98" id="Seg_1883" s="T97">0.top.int.concr</ta>
            <ta e="T100" id="Seg_1884" s="T98">top.int.concr</ta>
            <ta e="T107" id="Seg_1885" s="T105">top.int.concr</ta>
            <ta e="T120" id="Seg_1886" s="T119">0.top.int.concr</ta>
            <ta e="T128" id="Seg_1887" s="T127">top.int.concr</ta>
         </annotation>
         <annotation name="Foc" tierref="Foc">
            <ta e="T5" id="Seg_1888" s="T0">foc.wid</ta>
            <ta e="T8" id="Seg_1889" s="T5">foc.int</ta>
            <ta e="T13" id="Seg_1890" s="T8">foc.wid</ta>
            <ta e="T17" id="Seg_1891" s="T14">foc.wid</ta>
            <ta e="T19" id="Seg_1892" s="T18">foc.nar</ta>
            <ta e="T22" id="Seg_1893" s="T21">foc.nar</ta>
            <ta e="T23" id="Seg_1894" s="T22">foc.nar</ta>
            <ta e="T28" id="Seg_1895" s="T27">foc.ver</ta>
            <ta e="T31" id="Seg_1896" s="T30">foc.nar</ta>
            <ta e="T36" id="Seg_1897" s="T33">foc.nar</ta>
            <ta e="T41" id="Seg_1898" s="T40">foc.int</ta>
            <ta e="T48" id="Seg_1899" s="T47">foc.int</ta>
            <ta e="T53" id="Seg_1900" s="T51">foc.int</ta>
            <ta e="T61" id="Seg_1901" s="T53">foc.int</ta>
            <ta e="T79" id="Seg_1902" s="T67">foc.int</ta>
            <ta e="T82" id="Seg_1903" s="T80">foc.nar</ta>
            <ta e="T86" id="Seg_1904" s="T85">foc.int</ta>
            <ta e="T92" id="Seg_1905" s="T87">foc.wid</ta>
            <ta e="T96" id="Seg_1906" s="T94">foc.int</ta>
            <ta e="T98" id="Seg_1907" s="T96">foc.int</ta>
            <ta e="T105" id="Seg_1908" s="T100">foc.int</ta>
            <ta e="T113" id="Seg_1909" s="T109">foc.int</ta>
            <ta e="T116" id="Seg_1910" s="T115">foc.nar</ta>
            <ta e="T120" id="Seg_1911" s="T118">foc.nar</ta>
            <ta e="T126" id="Seg_1912" s="T120">foc.wid</ta>
            <ta e="T129" id="Seg_1913" s="T128">foc.int</ta>
         </annotation>
         <annotation name="BOR" tierref="BOR">
            <ta e="T4" id="Seg_1914" s="T3">EV:gram (INTNS)</ta>
            <ta e="T6" id="Seg_1915" s="T5">RUS:core</ta>
            <ta e="T54" id="Seg_1916" s="T53">EV:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon">
            <ta e="T6" id="Seg_1917" s="T5">fortition Vsub</ta>
            <ta e="T54" id="Seg_1918" s="T53">fortition Vsub Vsub</ta>
         </annotation>
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T6" id="Seg_1919" s="T5">dir:bare</ta>
            <ta e="T54" id="Seg_1920" s="T53">dir:infl</ta>
         </annotation>
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T8" id="Seg_1921" s="T0">Long ago an old man and an old woman lived all alone, they never had a child.</ta>
            <ta e="T13" id="Seg_1922" s="T8">Once the old man said to his old wife:</ta>
            <ta e="T17" id="Seg_1923" s="T13">"Old woman, it would be good if we could have a child."</ta>
            <ta e="T20" id="Seg_1924" s="T17">The old woman said on that:</ta>
            <ta e="T25" id="Seg_1925" s="T20">"We didn't have a child, when we were young, how shall we have it now?</ta>
            <ta e="T29" id="Seg_1926" s="T25">Is it possible?", she said.</ta>
            <ta e="T32" id="Seg_1927" s="T29">The old man said:</ta>
            <ta e="T39" id="Seg_1928" s="T32">"I want to have a child not the way people usually have it", he said.</ta>
            <ta e="T41" id="Seg_1929" s="T39">The old woman said:</ta>
            <ta e="T45" id="Seg_1930" s="T41">"Do you want to have a child in a bad way?!</ta>
            <ta e="T49" id="Seg_1931" s="T45">Then you should do it yourself."</ta>
            <ta e="T64" id="Seg_1932" s="T49">There the old man took a net, placed it as for partridges, spread it out from the shore across the river and to the mountain.</ta>
            <ta e="T79" id="Seg_1933" s="T64">Having made that, he goes inside the net and shouts "Oho-ehee", roaring like a child and jumping with all his might.</ta>
            <ta e="T83" id="Seg_1934" s="T79">He spents three days doing that.</ta>
            <ta e="T92" id="Seg_1935" s="T83">On the third day he went out, a child boy got into the net, he is lying there crying.</ta>
            <ta e="T96" id="Seg_1936" s="T92">The old man brings him home.</ta>
            <ta e="T105" id="Seg_1937" s="T96">They entered the house, both penis and after of this child are closed.</ta>
            <ta e="T113" id="Seg_1938" s="T105">The pleased people feed him, they stuff him like a bag.</ta>
            <ta e="T120" id="Seg_1939" s="T113">But how should a human live without any hole? And so he died there.</ta>
            <ta e="T129" id="Seg_1940" s="T120">This is what the ancestors tell [us] about a child that came from the master of the mountain.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T8" id="Seg_1941" s="T0">Vor langer Zeit lebten ein alter Mann und eine alte Frau ganz alleine, nie hatten sie ein Kind.</ta>
            <ta e="T13" id="Seg_1942" s="T8">Einmal sagte der alte Mann zu seiner alten Frau:</ta>
            <ta e="T17" id="Seg_1943" s="T13">"Alte, es wäre gut, wenn wir ein Kind bekämen."</ta>
            <ta e="T20" id="Seg_1944" s="T17">Darauf sagte die alte Frau:</ta>
            <ta e="T25" id="Seg_1945" s="T20">"Wir haben in unserer Jugend kein Kind bekommen, wie sollen wir jetzt eins bekommen?</ta>
            <ta e="T29" id="Seg_1946" s="T25">Ist das möglich?", sagte sie.</ta>
            <ta e="T32" id="Seg_1947" s="T29">Da sagte der alte Mann:</ta>
            <ta e="T39" id="Seg_1948" s="T32">"Ich möchte ein Kind bekommen, auf andere Weise wie die Leute", sagte er.</ta>
            <ta e="T41" id="Seg_1949" s="T39">Die alte Frau sagte:</ta>
            <ta e="T45" id="Seg_1950" s="T41">"Möchtest du etwa auf schlechte Art und Weise ein Kind bekommen?!</ta>
            <ta e="T49" id="Seg_1951" s="T45">Wenn es so ist, dann kümmer du dich darum."</ta>
            <ta e="T64" id="Seg_1952" s="T49">Da nahm der alte Mann ein Netz, stellte es wie für Rebhühner auf, hängte es quer über den Fluss in Richtung der Berge.</ta>
            <ta e="T79" id="Seg_1953" s="T64">Nachdem er das gemacht hat, geht er ins Netz hinein, springt und sagt "Ohoo-Ehee", dabei schreit und brüllt er aus Leibeskräften wie ein Kind.</ta>
            <ta e="T83" id="Seg_1954" s="T79">Das macht er drei Tage lang.</ta>
            <ta e="T92" id="Seg_1955" s="T83">Am dritten Tag ging er hinaus, im Netz hatte sich ein Junge verfangen, er liegt dort und weint.</ta>
            <ta e="T96" id="Seg_1956" s="T92">Diesen nahm der alte Mann mit nach Hause.</ta>
            <ta e="T105" id="Seg_1957" s="T96">Sie gingen ins Haus hinein, sowohl Penis als auch After des Kindes sind verschlossen.</ta>
            <ta e="T113" id="Seg_1958" s="T105">Die erfreuten Leute füttern, sie stopfen ihn wie eine Tasche voll.</ta>
            <ta e="T120" id="Seg_1959" s="T113">Wie soll ein Mensch ohne Öffnungen leben? Und da starb er auch.</ta>
            <ta e="T129" id="Seg_1960" s="T120">So erzählen die Vorfahren, dass ein Kind vom Herren der Berge gekommen ist.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T8" id="Seg_1961" s="T0">В старину жили одинокие старик со старухой, век-то не имели ребенка.</ta>
            <ta e="T13" id="Seg_1962" s="T8">Однажды старик говорит своей старухе:</ta>
            <ta e="T17" id="Seg_1963" s="T13">"Старуха, нам бы хорошо завести своего ребенка."</ta>
            <ta e="T20" id="Seg_1964" s="T17">На это старуха сказала:</ta>
            <ta e="T25" id="Seg_1965" s="T20">"Когда мы были молодыми, не появился у нас ребенок, как сейчас родим его?</ta>
            <ta e="T29" id="Seg_1966" s="T25">Сможем ли мы?", сказала.</ta>
            <ta e="T32" id="Seg_1967" s="T29">Тут старик сказал:</ta>
            <ta e="T39" id="Seg_1968" s="T32">"Я хочу завести ребенка не так, как у людей бывает", сказал.</ta>
            <ta e="T41" id="Seg_1969" s="T39">Старуха сказала:</ta>
            <ta e="T45" id="Seg_1970" s="T41">"Разве худо обзавестись ребенком?!</ta>
            <ta e="T49" id="Seg_1971" s="T45">Раз так, то ты постарайся."</ta>
            <ta e="T64" id="Seg_1972" s="T49">Тогда старик взял сеть, поставил ее как на куропаток, навесив на колья от берега реки в сторону горы.</ta>
            <ta e="T79" id="Seg_1973" s="T64">Сделав так, входит внутрь сети и, подпрыгивая на месте, голосит: "Осоо-эсээ!", при этом вопиет, как малое дитя, и прыгает что есть мочи.</ta>
            <ta e="T83" id="Seg_1974" s="T79">Вот так три дня делает.</ta>
            <ta e="T92" id="Seg_1975" s="T83">На третий день вышел — в сеть мальчик маленький попался, лежит и плачет.</ta>
            <ta e="T96" id="Seg_1976" s="T92">Старик его домой приносит.</ta>
            <ta e="T105" id="Seg_1977" s="T96">Внес в дом, а у этого ребенка ни спереди, ни сзади отверстия нет.</ta>
            <ta e="T113" id="Seg_1978" s="T105">Обрадованные люди кормят его, словно суму [кожаную] набивают.</ta>
            <ta e="T120" id="Seg_1979" s="T113">Как же будет жить цельный человек, не имеющий отверстия? Тут же и умер.</ta>
            <ta e="T129" id="Seg_1980" s="T120">Вот что рассказыбают предки о том, как от духа горы был получен ребенок.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T8" id="Seg_1981" s="T0">В старину жили одинокие старик со старухой, век-то не имели ребенка.</ta>
            <ta e="T13" id="Seg_1982" s="T8">Однажды старик говорит своей старухе:</ta>
            <ta e="T17" id="Seg_1983" s="T13">— Старуха, нам бы хорошо завести своего ребенка.</ta>
            <ta e="T20" id="Seg_1984" s="T17">На это старуха сказала:</ta>
            <ta e="T25" id="Seg_1985" s="T20">— Когда мы были молодыми, не появился у нас ребенок, как сейчас родим его?</ta>
            <ta e="T29" id="Seg_1986" s="T25">Сможем ли мы?</ta>
            <ta e="T32" id="Seg_1987" s="T29">Тут старик сказал:</ta>
            <ta e="T39" id="Seg_1988" s="T32">— Я хочу завести ребенка не так, как у людей бывает.</ta>
            <ta e="T41" id="Seg_1989" s="T39">Старуха сказала:</ta>
            <ta e="T45" id="Seg_1990" s="T41">— Разве худо обзавестись ребенком?!</ta>
            <ta e="T49" id="Seg_1991" s="T45">Раз так, то ты постарайся.</ta>
            <ta e="T64" id="Seg_1992" s="T49">Тогда старик взял сеть, поставил ее как на куропаток, навесив на колья от берега реки в сторону горы.</ta>
            <ta e="T79" id="Seg_1993" s="T64">Сделав так, вошел внутрь сети и, подпрыгивая на месте, заголосил: "Осоо-эсээ!" — при этом вопил, как малое дитя, и прыгал что есть мочи.</ta>
            <ta e="T83" id="Seg_1994" s="T79">Вот так три дня делал.</ta>
            <ta e="T92" id="Seg_1995" s="T83">На третий день вышел — в сеть мальчик маленький попался, лежит и плачет.</ta>
            <ta e="T96" id="Seg_1996" s="T92">Старик его домой принес.</ta>
            <ta e="T105" id="Seg_1997" s="T96">Внес в дом, [посмотрел], а у этого ребенка ни спереди, ни сзади отверстия нет.</ta>
            <ta e="T113" id="Seg_1998" s="T105">Обрадованные люди стали кормить его, словно суму [кожаную] набили.</ta>
            <ta e="T120" id="Seg_1999" s="T113">Как же будет жить цельный человек, не имеющий отверстия? Тут же и умер.</ta>
            <ta e="T129" id="Seg_2000" s="T120">Вот что рассказывали предки о том, как от духа горы был получен ребенок, но умер.</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg"
                          name="gg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top"
                          name="Top"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc"
                          name="Foc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
