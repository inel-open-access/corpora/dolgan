<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID956B2DF3-1046-F1D7-8A67-FED3F30840ED">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>AnZA_19XX_AncestorSwanFragment_flk</transcription-name>
         <referenced-file url="AnZA_19XX_AncestorSwanFragment_flk.wav" />
         <referenced-file url="AnZA_19XX_AncestorSwanFragment_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\flk\AnZA_19XX_AncestorSwanFragment_flk\AnZA_19XX_AncestorSwanFragment_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">46</ud-information>
            <ud-information attribute-name="# HIAT:w">35</ud-information>
            <ud-information attribute-name="# e">35</ud-information>
            <ud-information attribute-name="# HIAT:u">5</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="AnZA">
            <abbreviation>AnZA</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="2.638" type="appl" />
         <tli id="T1" time="2.835" type="appl" />
         <tli id="T2" time="3.032" type="appl" />
         <tli id="T3" time="3.229" type="appl" />
         <tli id="T4" time="3.426" type="appl" />
         <tli id="T5" time="3.623" type="appl" />
         <tli id="T6" time="3.82" type="appl" />
         <tli id="T7" time="4.016" type="appl" />
         <tli id="T8" time="4.213" type="appl" />
         <tli id="T9" time="4.41" type="appl" />
         <tli id="T10" time="4.607" type="appl" />
         <tli id="T11" time="4.804" type="appl" />
         <tli id="T12" time="9.345372761348171" />
         <tli id="T13" time="10.315" type="appl" />
         <tli id="T14" time="11.004" type="appl" />
         <tli id="T15" time="11.694" type="appl" />
         <tli id="T16" time="12.383" type="appl" />
         <tli id="T17" time="13.072" type="appl" />
         <tli id="T18" time="13.762" type="appl" />
         <tli id="T19" time="14.451" type="appl" />
         <tli id="T20" time="15.14" type="appl" />
         <tli id="T21" time="15.639" type="appl" />
         <tli id="T22" time="16.137" type="appl" />
         <tli id="T23" time="16.636" type="appl" />
         <tli id="T24" time="17.135" type="appl" />
         <tli id="T25" time="17.634" type="appl" />
         <tli id="T26" time="18.132" type="appl" />
         <tli id="T27" time="18.631" type="appl" />
         <tli id="T28" time="19.13" type="appl" />
         <tli id="T29" time="19.629" type="appl" />
         <tli id="T30" time="20.127" type="appl" />
         <tli id="T31" time="21.423700467170487" />
         <tli id="T32" time="21.55701534251069" />
         <tli id="T33" time="21.759333333333334" type="intp" />
         <tli id="T34" time="22.326" type="intp" />
         <tli id="T35" time="22.892666666666667" type="intp" />
         <tli id="T36" time="22.956821533582815" />
         <tli id="T37" time="24.026" type="appl" />
         <tli id="T38" time="25.716" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="AnZA"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T37" id="Seg_0" n="sc" s="T0">
               <ts e="T12" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Teːtebiteːk</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">ölördöktörünö</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">bu͡ollagɨna</ts>
                  <nts id="Seg_11" n="HIAT:ip">,</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_14" n="HIAT:w" s="T3">menʼiːtin</ts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_17" n="HIAT:w" s="T4">bu͡ollagɨna</ts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_20" n="HIAT:w" s="T5">ɨraːk</ts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_23" n="HIAT:w" s="T6">bagaj</ts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_26" n="HIAT:w" s="T7">illenner</ts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_28" n="HIAT:ip">(</nts>
                  <ts e="T10" id="Seg_30" n="HIAT:w" s="T8">mask-</ts>
                  <nts id="Seg_31" n="HIAT:ip">)</nts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_34" n="HIAT:w" s="T10">maska</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_37" n="HIAT:w" s="T11">iːleːččiler</ts>
                  <nts id="Seg_38" n="HIAT:ip">.</nts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_41" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_43" n="HIAT:w" s="T12">Onton</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_46" n="HIAT:w" s="T13">bu͡ollagɨna</ts>
                  <nts id="Seg_47" n="HIAT:ip">,</nts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_50" n="HIAT:w" s="T14">tüːtün</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_53" n="HIAT:w" s="T15">bu͡ollagɨna</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_56" n="HIAT:w" s="T16">ɨraːk</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_59" n="HIAT:w" s="T17">bagaj</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_62" n="HIAT:w" s="T18">illen</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_65" n="HIAT:w" s="T19">ülgeːččiler</ts>
                  <nts id="Seg_66" n="HIAT:ip">.</nts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T31" id="Seg_69" n="HIAT:u" s="T20">
                  <ts e="T21" id="Seg_71" n="HIAT:w" s="T20">Ol</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_74" n="HIAT:w" s="T21">bu͡ollagɨna</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_77" n="HIAT:w" s="T22">bɨlɨr</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_80" n="HIAT:w" s="T23">bihi͡ene</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_82" n="HIAT:ip">(</nts>
                  <ts e="T25" id="Seg_84" n="HIAT:w" s="T24">-bit</ts>
                  <nts id="Seg_85" n="HIAT:ip">)</nts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_88" n="HIAT:w" s="T25">gi͡ene</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_91" n="HIAT:w" s="T26">du͡o</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_94" n="HIAT:w" s="T27">taŋara</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_97" n="HIAT:w" s="T28">ete</ts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_100" n="HIAT:w" s="T29">ühü</ts>
                  <nts id="Seg_101" n="HIAT:ip">.</nts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T36" id="Seg_104" n="HIAT:u" s="T31">
                  <ts e="T32" id="Seg_106" n="HIAT:w" s="T31">Ol</ts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_109" n="HIAT:w" s="T32">ihin</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_112" n="HIAT:w" s="T33">ol</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_115" n="HIAT:w" s="T34">kördük</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_118" n="HIAT:w" s="T35">karajallar</ts>
                  <nts id="Seg_119" n="HIAT:ip">.</nts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T37" id="Seg_122" n="HIAT:u" s="T36">
                  <ts e="T37" id="Seg_124" n="HIAT:w" s="T36">Elete</ts>
                  <nts id="Seg_125" n="HIAT:ip">.</nts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T37" id="Seg_127" n="sc" s="T0">
               <ts e="T1" id="Seg_129" n="e" s="T0">Teːtebiteːk </ts>
               <ts e="T2" id="Seg_131" n="e" s="T1">ölördöktörünö </ts>
               <ts e="T3" id="Seg_133" n="e" s="T2">bu͡ollagɨna, </ts>
               <ts e="T4" id="Seg_135" n="e" s="T3">menʼiːtin </ts>
               <ts e="T5" id="Seg_137" n="e" s="T4">bu͡ollagɨna </ts>
               <ts e="T6" id="Seg_139" n="e" s="T5">ɨraːk </ts>
               <ts e="T7" id="Seg_141" n="e" s="T6">bagaj </ts>
               <ts e="T8" id="Seg_143" n="e" s="T7">illenner </ts>
               <ts e="T10" id="Seg_145" n="e" s="T8">(mask-) </ts>
               <ts e="T11" id="Seg_147" n="e" s="T10">maska </ts>
               <ts e="T12" id="Seg_149" n="e" s="T11">iːleːččiler. </ts>
               <ts e="T13" id="Seg_151" n="e" s="T12">Onton </ts>
               <ts e="T14" id="Seg_153" n="e" s="T13">bu͡ollagɨna, </ts>
               <ts e="T15" id="Seg_155" n="e" s="T14">tüːtün </ts>
               <ts e="T16" id="Seg_157" n="e" s="T15">bu͡ollagɨna </ts>
               <ts e="T17" id="Seg_159" n="e" s="T16">ɨraːk </ts>
               <ts e="T18" id="Seg_161" n="e" s="T17">bagaj </ts>
               <ts e="T19" id="Seg_163" n="e" s="T18">illen </ts>
               <ts e="T20" id="Seg_165" n="e" s="T19">ülgeːččiler. </ts>
               <ts e="T21" id="Seg_167" n="e" s="T20">Ol </ts>
               <ts e="T22" id="Seg_169" n="e" s="T21">bu͡ollagɨna </ts>
               <ts e="T23" id="Seg_171" n="e" s="T22">bɨlɨr </ts>
               <ts e="T24" id="Seg_173" n="e" s="T23">bihi͡ene </ts>
               <ts e="T25" id="Seg_175" n="e" s="T24">(-bit) </ts>
               <ts e="T26" id="Seg_177" n="e" s="T25">gi͡ene </ts>
               <ts e="T27" id="Seg_179" n="e" s="T26">du͡o </ts>
               <ts e="T28" id="Seg_181" n="e" s="T27">taŋara </ts>
               <ts e="T29" id="Seg_183" n="e" s="T28">ete </ts>
               <ts e="T31" id="Seg_185" n="e" s="T29">ühü. </ts>
               <ts e="T32" id="Seg_187" n="e" s="T31">Ol </ts>
               <ts e="T33" id="Seg_189" n="e" s="T32">ihin </ts>
               <ts e="T34" id="Seg_191" n="e" s="T33">ol </ts>
               <ts e="T35" id="Seg_193" n="e" s="T34">kördük </ts>
               <ts e="T36" id="Seg_195" n="e" s="T35">karajallar. </ts>
               <ts e="T37" id="Seg_197" n="e" s="T36">Elete. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T12" id="Seg_198" s="T0">AnZA_19XX_AncestorSwanFragment_flk.001 (001.001)</ta>
            <ta e="T20" id="Seg_199" s="T12">AnZA_19XX_AncestorSwanFragment_flk.002 (001.002)</ta>
            <ta e="T31" id="Seg_200" s="T20">AnZA_19XX_AncestorSwanFragment_flk.003 (001.003)</ta>
            <ta e="T36" id="Seg_201" s="T31">AnZA_19XX_AncestorSwanFragment_flk.004 (001.004)</ta>
            <ta e="T37" id="Seg_202" s="T36">AnZA_19XX_AncestorSwanFragment_flk.005 (001.005)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T12" id="Seg_203" s="T0">Тээтэбитээк өлөрдөктөрүнө буоллагына, мэньиитин буоллагына ыраак багай иллэннэр (маск..) маска иилэччилэр.</ta>
            <ta e="T20" id="Seg_204" s="T12">Онтон буоллагынэ, түүтүн буоллагына ыраак багай иллэн үлгээччилэр.</ta>
            <ta e="T31" id="Seg_205" s="T20">Ол буоллагына былыр биһиэнэ өрбүгэбит гиэнэ дуо тангара этэ үһү.</ta>
            <ta e="T36" id="Seg_206" s="T31">Ол иһин ол көрдүк карайаллар. </ta>
            <ta e="T37" id="Seg_207" s="T36">Элэтэ.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T12" id="Seg_208" s="T0">Teːtebiteːk ölördöktörünö bu͡ollagɨna, menʼiːtin bu͡ollagɨna ɨraːk bagaj illenner (mask-) maska iːleːččiler. </ta>
            <ta e="T20" id="Seg_209" s="T12">Onton bu͡ollagɨna, tüːtün bu͡ollagɨna ɨraːk bagaj illen ülgeːččiler. </ta>
            <ta e="T31" id="Seg_210" s="T20">Ol bu͡ollagɨna bɨlɨr bihi͡ene (-bit) gi͡ene du͡o taŋara ete ühü. </ta>
            <ta e="T36" id="Seg_211" s="T31">Ol ihin ol kördük karajallar. </ta>
            <ta e="T37" id="Seg_212" s="T36">Elete. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_213" s="T0">teːte-bi-teːk</ta>
            <ta e="T2" id="Seg_214" s="T1">ölör-dök-törünö</ta>
            <ta e="T3" id="Seg_215" s="T2">bu͡ollagɨna</ta>
            <ta e="T4" id="Seg_216" s="T3">menʼiː-ti-n</ta>
            <ta e="T5" id="Seg_217" s="T4">bu͡ollagɨna</ta>
            <ta e="T6" id="Seg_218" s="T5">ɨraːk</ta>
            <ta e="T7" id="Seg_219" s="T6">bagaj</ta>
            <ta e="T8" id="Seg_220" s="T7">ill-en-ner</ta>
            <ta e="T11" id="Seg_221" s="T10">mas-ka</ta>
            <ta e="T12" id="Seg_222" s="T11">iːl-eːčči-ler</ta>
            <ta e="T13" id="Seg_223" s="T12">onton</ta>
            <ta e="T14" id="Seg_224" s="T13">bu͡ollagɨna</ta>
            <ta e="T15" id="Seg_225" s="T14">tüː-tü-n</ta>
            <ta e="T16" id="Seg_226" s="T15">bu͡ollagɨna</ta>
            <ta e="T17" id="Seg_227" s="T16">ɨraːk</ta>
            <ta e="T18" id="Seg_228" s="T17">bagaj</ta>
            <ta e="T19" id="Seg_229" s="T18">ill-en</ta>
            <ta e="T20" id="Seg_230" s="T19">ülgeː-čči-ler</ta>
            <ta e="T21" id="Seg_231" s="T20">ol</ta>
            <ta e="T22" id="Seg_232" s="T21">bu͡ollagɨna</ta>
            <ta e="T23" id="Seg_233" s="T22">bɨlɨr</ta>
            <ta e="T24" id="Seg_234" s="T23">bihi͡ene</ta>
            <ta e="T26" id="Seg_235" s="T25">gi͡en-e</ta>
            <ta e="T27" id="Seg_236" s="T26">du͡o</ta>
            <ta e="T28" id="Seg_237" s="T27">taŋara</ta>
            <ta e="T29" id="Seg_238" s="T28">e-t-e</ta>
            <ta e="T31" id="Seg_239" s="T29">ühü</ta>
            <ta e="T32" id="Seg_240" s="T31">ol</ta>
            <ta e="T33" id="Seg_241" s="T32">ihin</ta>
            <ta e="T34" id="Seg_242" s="T33">ol</ta>
            <ta e="T35" id="Seg_243" s="T34">kördük</ta>
            <ta e="T36" id="Seg_244" s="T35">karaj-al-lar</ta>
            <ta e="T37" id="Seg_245" s="T36">ele-te</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_246" s="T0">teːte-BI-LAːK</ta>
            <ta e="T2" id="Seg_247" s="T1">ölör-TAK-TArInA</ta>
            <ta e="T3" id="Seg_248" s="T2">bu͡ollagɨna</ta>
            <ta e="T4" id="Seg_249" s="T3">menʼiː-tI-n</ta>
            <ta e="T5" id="Seg_250" s="T4">bu͡ollagɨna</ta>
            <ta e="T6" id="Seg_251" s="T5">ɨraːk</ta>
            <ta e="T7" id="Seg_252" s="T6">bagajɨ</ta>
            <ta e="T8" id="Seg_253" s="T7">ilin-An-LAr</ta>
            <ta e="T11" id="Seg_254" s="T10">mas-GA</ta>
            <ta e="T12" id="Seg_255" s="T11">iːl-AːččI-LAr</ta>
            <ta e="T13" id="Seg_256" s="T12">onton</ta>
            <ta e="T14" id="Seg_257" s="T13">bu͡ollagɨna</ta>
            <ta e="T15" id="Seg_258" s="T14">tüː-tI-n</ta>
            <ta e="T16" id="Seg_259" s="T15">bu͡ollagɨna</ta>
            <ta e="T17" id="Seg_260" s="T16">ɨraːk</ta>
            <ta e="T18" id="Seg_261" s="T17">bagajɨ</ta>
            <ta e="T19" id="Seg_262" s="T18">ilin-An</ta>
            <ta e="T20" id="Seg_263" s="T19">ülgeː-AːččI-LAr</ta>
            <ta e="T21" id="Seg_264" s="T20">ol</ta>
            <ta e="T22" id="Seg_265" s="T21">bu͡ollagɨna</ta>
            <ta e="T23" id="Seg_266" s="T22">bɨlɨr</ta>
            <ta e="T24" id="Seg_267" s="T23">bihi͡ene</ta>
            <ta e="T26" id="Seg_268" s="T25">gi͡en-tA</ta>
            <ta e="T27" id="Seg_269" s="T26">du͡o</ta>
            <ta e="T28" id="Seg_270" s="T27">taŋara</ta>
            <ta e="T29" id="Seg_271" s="T28">e-TI-tA</ta>
            <ta e="T31" id="Seg_272" s="T29">ühü</ta>
            <ta e="T32" id="Seg_273" s="T31">ol</ta>
            <ta e="T33" id="Seg_274" s="T32">ihin</ta>
            <ta e="T34" id="Seg_275" s="T33">ol</ta>
            <ta e="T35" id="Seg_276" s="T34">kördük</ta>
            <ta e="T36" id="Seg_277" s="T35">karaj-Ar-LAr</ta>
            <ta e="T37" id="Seg_278" s="T36">ele-tA</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_279" s="T0">father-1SG-PROPR.[NOM]</ta>
            <ta e="T2" id="Seg_280" s="T1">kill-TEMP-3PL</ta>
            <ta e="T3" id="Seg_281" s="T2">though</ta>
            <ta e="T4" id="Seg_282" s="T3">head-3SG-ACC</ta>
            <ta e="T5" id="Seg_283" s="T4">though</ta>
            <ta e="T6" id="Seg_284" s="T5">far.away</ta>
            <ta e="T7" id="Seg_285" s="T6">very</ta>
            <ta e="T8" id="Seg_286" s="T7">take.away-CVB.SEQ-3PL</ta>
            <ta e="T11" id="Seg_287" s="T10">tree-DAT/LOC</ta>
            <ta e="T12" id="Seg_288" s="T11">put.on-HAB-3PL</ta>
            <ta e="T13" id="Seg_289" s="T12">then</ta>
            <ta e="T14" id="Seg_290" s="T13">though</ta>
            <ta e="T15" id="Seg_291" s="T14">feather-3SG-ACC</ta>
            <ta e="T16" id="Seg_292" s="T15">though</ta>
            <ta e="T17" id="Seg_293" s="T16">far.away</ta>
            <ta e="T18" id="Seg_294" s="T17">very</ta>
            <ta e="T19" id="Seg_295" s="T18">take.away-CVB.SEQ</ta>
            <ta e="T20" id="Seg_296" s="T19">pluck-HAB-3PL</ta>
            <ta e="T21" id="Seg_297" s="T20">that.[NOM]</ta>
            <ta e="T22" id="Seg_298" s="T21">though</ta>
            <ta e="T23" id="Seg_299" s="T22">long.ago</ta>
            <ta e="T24" id="Seg_300" s="T23">our</ta>
            <ta e="T26" id="Seg_301" s="T25">own-3SG</ta>
            <ta e="T27" id="Seg_302" s="T26">MOD</ta>
            <ta e="T28" id="Seg_303" s="T27">god.[NOM]</ta>
            <ta e="T29" id="Seg_304" s="T28">be-PST1-3SG</ta>
            <ta e="T31" id="Seg_305" s="T29">it.is.said</ta>
            <ta e="T32" id="Seg_306" s="T31">that.[NOM]</ta>
            <ta e="T33" id="Seg_307" s="T32">because.of</ta>
            <ta e="T34" id="Seg_308" s="T33">that.[NOM]</ta>
            <ta e="T35" id="Seg_309" s="T34">similar</ta>
            <ta e="T36" id="Seg_310" s="T35">bury-PRS-3PL</ta>
            <ta e="T37" id="Seg_311" s="T36">last-3SG.[NOM]</ta>
         </annotation>
         <annotation name="gg" tierref="gg">
            <ta e="T1" id="Seg_312" s="T0">Vater-1SG-PROPR.[NOM]</ta>
            <ta e="T2" id="Seg_313" s="T1">töten-TEMP-3PL</ta>
            <ta e="T3" id="Seg_314" s="T2">aber</ta>
            <ta e="T4" id="Seg_315" s="T3">Kopf-3SG-ACC</ta>
            <ta e="T5" id="Seg_316" s="T4">aber</ta>
            <ta e="T6" id="Seg_317" s="T5">weit.weg</ta>
            <ta e="T7" id="Seg_318" s="T6">sehr</ta>
            <ta e="T8" id="Seg_319" s="T7">mitnehmen-CVB.SEQ-3PL</ta>
            <ta e="T11" id="Seg_320" s="T10">Baum-DAT/LOC</ta>
            <ta e="T12" id="Seg_321" s="T11">umhängen-HAB-3PL</ta>
            <ta e="T13" id="Seg_322" s="T12">dann</ta>
            <ta e="T14" id="Seg_323" s="T13">aber</ta>
            <ta e="T15" id="Seg_324" s="T14">Feder-3SG-ACC</ta>
            <ta e="T16" id="Seg_325" s="T15">aber</ta>
            <ta e="T17" id="Seg_326" s="T16">weit.weg</ta>
            <ta e="T18" id="Seg_327" s="T17">sehr</ta>
            <ta e="T19" id="Seg_328" s="T18">mitnehmen-CVB.SEQ</ta>
            <ta e="T20" id="Seg_329" s="T19">rupfen-HAB-3PL</ta>
            <ta e="T21" id="Seg_330" s="T20">jenes.[NOM]</ta>
            <ta e="T22" id="Seg_331" s="T21">aber</ta>
            <ta e="T23" id="Seg_332" s="T22">vor.langer.Zeit</ta>
            <ta e="T24" id="Seg_333" s="T23">unser</ta>
            <ta e="T26" id="Seg_334" s="T25">eigen-3SG</ta>
            <ta e="T27" id="Seg_335" s="T26">MOD</ta>
            <ta e="T28" id="Seg_336" s="T27">Gott.[NOM]</ta>
            <ta e="T29" id="Seg_337" s="T28">sein-PST1-3SG</ta>
            <ta e="T31" id="Seg_338" s="T29">man.sagt</ta>
            <ta e="T32" id="Seg_339" s="T31">jenes.[NOM]</ta>
            <ta e="T33" id="Seg_340" s="T32">wegen</ta>
            <ta e="T34" id="Seg_341" s="T33">jenes.[NOM]</ta>
            <ta e="T35" id="Seg_342" s="T34">ähnlich</ta>
            <ta e="T36" id="Seg_343" s="T35">begraben-PRS-3PL</ta>
            <ta e="T37" id="Seg_344" s="T36">letzter-3SG.[NOM]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_345" s="T0">отец-1SG-PROPR.[NOM]</ta>
            <ta e="T2" id="Seg_346" s="T1">убить-TEMP-3PL</ta>
            <ta e="T3" id="Seg_347" s="T2">однако</ta>
            <ta e="T4" id="Seg_348" s="T3">голова-3SG-ACC</ta>
            <ta e="T5" id="Seg_349" s="T4">однако</ta>
            <ta e="T6" id="Seg_350" s="T5">далеко</ta>
            <ta e="T7" id="Seg_351" s="T6">очень</ta>
            <ta e="T8" id="Seg_352" s="T7">уносить-CVB.SEQ-3PL</ta>
            <ta e="T11" id="Seg_353" s="T10">дерево-DAT/LOC</ta>
            <ta e="T12" id="Seg_354" s="T11">накидывать-HAB-3PL</ta>
            <ta e="T13" id="Seg_355" s="T12">потом</ta>
            <ta e="T14" id="Seg_356" s="T13">однако</ta>
            <ta e="T15" id="Seg_357" s="T14">перо-3SG-ACC</ta>
            <ta e="T16" id="Seg_358" s="T15">однако</ta>
            <ta e="T17" id="Seg_359" s="T16">далеко</ta>
            <ta e="T18" id="Seg_360" s="T17">очень</ta>
            <ta e="T19" id="Seg_361" s="T18">уносить-CVB.SEQ</ta>
            <ta e="T20" id="Seg_362" s="T19">выщипывать-HAB-3PL</ta>
            <ta e="T21" id="Seg_363" s="T20">тот.[NOM]</ta>
            <ta e="T22" id="Seg_364" s="T21">однако</ta>
            <ta e="T23" id="Seg_365" s="T22">давно</ta>
            <ta e="T24" id="Seg_366" s="T23">наш</ta>
            <ta e="T26" id="Seg_367" s="T25">собственный-3SG</ta>
            <ta e="T27" id="Seg_368" s="T26">MOD</ta>
            <ta e="T28" id="Seg_369" s="T27">Бог.[NOM]</ta>
            <ta e="T29" id="Seg_370" s="T28">быть-PST1-3SG</ta>
            <ta e="T31" id="Seg_371" s="T29">говорят</ta>
            <ta e="T32" id="Seg_372" s="T31">тот.[NOM]</ta>
            <ta e="T33" id="Seg_373" s="T32">из_за</ta>
            <ta e="T34" id="Seg_374" s="T33">тот.[NOM]</ta>
            <ta e="T35" id="Seg_375" s="T34">подобно</ta>
            <ta e="T36" id="Seg_376" s="T35">хоронить-PRS-3PL</ta>
            <ta e="T37" id="Seg_377" s="T36">последний-3SG.[NOM]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_378" s="T0">n-n:poss-n&gt;adj-n:case</ta>
            <ta e="T2" id="Seg_379" s="T1">v-v:mood-v:temp.pn</ta>
            <ta e="T3" id="Seg_380" s="T2">ptcl</ta>
            <ta e="T4" id="Seg_381" s="T3">n-n:poss-n:case</ta>
            <ta e="T5" id="Seg_382" s="T4">ptcl</ta>
            <ta e="T6" id="Seg_383" s="T5">adv</ta>
            <ta e="T7" id="Seg_384" s="T6">ptcl</ta>
            <ta e="T8" id="Seg_385" s="T7">v-v:cvb-v:pred.pn</ta>
            <ta e="T11" id="Seg_386" s="T10">n-n:case</ta>
            <ta e="T12" id="Seg_387" s="T11">v-v:mood-v:pred.pn</ta>
            <ta e="T13" id="Seg_388" s="T12">adv</ta>
            <ta e="T14" id="Seg_389" s="T13">ptcl</ta>
            <ta e="T15" id="Seg_390" s="T14">n-n:poss-n:case</ta>
            <ta e="T16" id="Seg_391" s="T15">ptcl</ta>
            <ta e="T17" id="Seg_392" s="T16">adv</ta>
            <ta e="T18" id="Seg_393" s="T17">ptcl</ta>
            <ta e="T19" id="Seg_394" s="T18">v-v:cvb</ta>
            <ta e="T20" id="Seg_395" s="T19">v-v:mood-v:pred.pn</ta>
            <ta e="T21" id="Seg_396" s="T20">dempro-pro:case</ta>
            <ta e="T22" id="Seg_397" s="T21">ptcl</ta>
            <ta e="T23" id="Seg_398" s="T22">adv</ta>
            <ta e="T24" id="Seg_399" s="T23">posspr</ta>
            <ta e="T26" id="Seg_400" s="T25">adj-n:(poss)</ta>
            <ta e="T27" id="Seg_401" s="T26">ptcl</ta>
            <ta e="T28" id="Seg_402" s="T27">n-n:case</ta>
            <ta e="T29" id="Seg_403" s="T28">v-v:tense-v:poss.pn</ta>
            <ta e="T31" id="Seg_404" s="T29">ptcl</ta>
            <ta e="T32" id="Seg_405" s="T31">dempro-pro:case</ta>
            <ta e="T33" id="Seg_406" s="T32">post</ta>
            <ta e="T34" id="Seg_407" s="T33">dempro-pro:case</ta>
            <ta e="T35" id="Seg_408" s="T34">post</ta>
            <ta e="T36" id="Seg_409" s="T35">v-v:tense-v:pred.pn</ta>
            <ta e="T37" id="Seg_410" s="T36">adj-n:(poss)-n:case</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_411" s="T0">n</ta>
            <ta e="T2" id="Seg_412" s="T1">v</ta>
            <ta e="T3" id="Seg_413" s="T2">ptcl</ta>
            <ta e="T4" id="Seg_414" s="T3">n</ta>
            <ta e="T5" id="Seg_415" s="T4">ptcl</ta>
            <ta e="T6" id="Seg_416" s="T5">adv</ta>
            <ta e="T7" id="Seg_417" s="T6">ptcl</ta>
            <ta e="T8" id="Seg_418" s="T7">v</ta>
            <ta e="T11" id="Seg_419" s="T10">n</ta>
            <ta e="T12" id="Seg_420" s="T11">v</ta>
            <ta e="T13" id="Seg_421" s="T12">adv</ta>
            <ta e="T14" id="Seg_422" s="T13">ptcl</ta>
            <ta e="T15" id="Seg_423" s="T14">n</ta>
            <ta e="T16" id="Seg_424" s="T15">ptcl</ta>
            <ta e="T17" id="Seg_425" s="T16">adv</ta>
            <ta e="T18" id="Seg_426" s="T17">ptcl</ta>
            <ta e="T19" id="Seg_427" s="T18">v</ta>
            <ta e="T20" id="Seg_428" s="T19">v</ta>
            <ta e="T21" id="Seg_429" s="T20">dempro</ta>
            <ta e="T22" id="Seg_430" s="T21">ptcl</ta>
            <ta e="T23" id="Seg_431" s="T22">adv</ta>
            <ta e="T24" id="Seg_432" s="T23">posspr</ta>
            <ta e="T26" id="Seg_433" s="T25">adj</ta>
            <ta e="T27" id="Seg_434" s="T26">ptcl</ta>
            <ta e="T28" id="Seg_435" s="T27">n</ta>
            <ta e="T29" id="Seg_436" s="T28">cop</ta>
            <ta e="T31" id="Seg_437" s="T29">ptcl</ta>
            <ta e="T32" id="Seg_438" s="T31">dempro</ta>
            <ta e="T33" id="Seg_439" s="T32">post</ta>
            <ta e="T34" id="Seg_440" s="T33">dempro</ta>
            <ta e="T35" id="Seg_441" s="T34">post</ta>
            <ta e="T36" id="Seg_442" s="T35">v</ta>
            <ta e="T37" id="Seg_443" s="T36">adj</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_444" s="T1">0.3.h:A</ta>
            <ta e="T4" id="Seg_445" s="T3">0.3:Poss np:Th</ta>
            <ta e="T8" id="Seg_446" s="T7">0.3.h:A</ta>
            <ta e="T11" id="Seg_447" s="T10">np:G</ta>
            <ta e="T12" id="Seg_448" s="T11">0.3.h:A</ta>
            <ta e="T15" id="Seg_449" s="T14">0.3:Poss np:Th</ta>
            <ta e="T19" id="Seg_450" s="T18">0.3.h:A</ta>
            <ta e="T20" id="Seg_451" s="T19">0.3.h:A</ta>
            <ta e="T21" id="Seg_452" s="T20">pro:Th</ta>
            <ta e="T23" id="Seg_453" s="T22">adv:Time</ta>
            <ta e="T24" id="Seg_454" s="T23">pro.h:Poss</ta>
            <ta e="T33" id="Seg_455" s="T31">pp:Cau</ta>
            <ta e="T36" id="Seg_456" s="T35">0.3.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_457" s="T1">s:temp</ta>
            <ta e="T4" id="Seg_458" s="T3">np:O</ta>
            <ta e="T8" id="Seg_459" s="T7">0.3.h:S v:pred</ta>
            <ta e="T12" id="Seg_460" s="T11">0.3.h:S v:pred</ta>
            <ta e="T15" id="Seg_461" s="T14">np:O</ta>
            <ta e="T19" id="Seg_462" s="T16">s:temp</ta>
            <ta e="T20" id="Seg_463" s="T19">0.3.h:S v:pred</ta>
            <ta e="T21" id="Seg_464" s="T20">pro:S</ta>
            <ta e="T28" id="Seg_465" s="T27">n:pred</ta>
            <ta e="T29" id="Seg_466" s="T28">cop</ta>
            <ta e="T36" id="Seg_467" s="T35">0.3.h:S v:pred</ta>
            <ta e="T37" id="Seg_468" s="T36">n:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST">
            <ta e="T1" id="Seg_469" s="T0">accs-inf</ta>
            <ta e="T2" id="Seg_470" s="T1">0.giv-active</ta>
            <ta e="T4" id="Seg_471" s="T3">accs-inf</ta>
            <ta e="T8" id="Seg_472" s="T7">0.giv-active</ta>
            <ta e="T11" id="Seg_473" s="T10">new</ta>
            <ta e="T12" id="Seg_474" s="T11">0.giv-active</ta>
            <ta e="T15" id="Seg_475" s="T14">accs-inf</ta>
            <ta e="T19" id="Seg_476" s="T18">0.giv-active</ta>
            <ta e="T20" id="Seg_477" s="T19">0.giv-active</ta>
            <ta e="T21" id="Seg_478" s="T20">giv-active</ta>
            <ta e="T24" id="Seg_479" s="T23">accs-inf</ta>
            <ta e="T36" id="Seg_480" s="T35">0.giv-active</ta>
         </annotation>
         <annotation name="Top" tierref="Top">
            <ta e="T3" id="Seg_481" s="T0">top.int.concr</ta>
            <ta e="T22" id="Seg_482" s="T20">top.int.concr</ta>
            <ta e="T36" id="Seg_483" s="T35">0.top.int.abstr</ta>
         </annotation>
         <annotation name="Foc" tierref="Foc">
            <ta e="T12" id="Seg_484" s="T3">foc.int</ta>
            <ta e="T28" id="Seg_485" s="T23">foc.nar</ta>
            <ta e="T36" id="Seg_486" s="T31">foc.wid</ta>
         </annotation>
         <annotation name="BOR" tierref="BOR" />
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T12" id="Seg_487" s="T0">My father and his people, when they killed [a swan], they carried its head far away and hung it up on a tree.</ta>
            <ta e="T20" id="Seg_488" s="T12">And then his feathers, they carried it far away and plucked it.</ta>
            <ta e="T31" id="Seg_489" s="T20">It was long time ago our holy bird, it is said.</ta>
            <ta e="T36" id="Seg_490" s="T31">Therefore one buries [them] like that.</ta>
            <ta e="T37" id="Seg_491" s="T36">That's all.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T12" id="Seg_492" s="T0">Mein Vater und die Seinen, wenn sie [einen Schwan] töteten, dann trugen sie seinen Kopf weit weg und hängten ihn an einen Baum.</ta>
            <ta e="T20" id="Seg_493" s="T12">Und dann seine Federn, sie trugen ihn weit weg und rupften ihn.</ta>
            <ta e="T31" id="Seg_494" s="T20">Der war aber vor langer Zeit unser heiliger Vogel, sagt man.</ta>
            <ta e="T36" id="Seg_495" s="T31">Deshalb beerdigt man [sie] so.</ta>
            <ta e="T37" id="Seg_496" s="T36">Ende.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T12" id="Seg_497" s="T0">Мой отец и другие, когда убивали [лебедя], его голову уносили подальше и подвешивали на дерево.</ta>
            <ta e="T20" id="Seg_498" s="T12">И потом его перья, уносили далеко и там выщипывали.</ta>
            <ta e="T31" id="Seg_499" s="T20">Это давным-давно была наша священная птица, говорят.</ta>
            <ta e="T36" id="Seg_500" s="T31">Поэтому [их] таким образом хоронят. </ta>
            <ta e="T37" id="Seg_501" s="T36">Всё.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T12" id="Seg_502" s="T0">Мой отец и другие, когда его (лебедя) убивали, его голову, унося далеко, подвешивали на дерево.</ta>
            <ta e="T20" id="Seg_503" s="T12">И там его перья, унеся далеко, там выщипывали.</ta>
            <ta e="T31" id="Seg_504" s="T20">Это давным-давно нашего рода-племени была священной птицей (кому покланялись), было, оказывается.</ta>
            <ta e="T36" id="Seg_505" s="T31">Поэтому таким образом хоронят. </ta>
            <ta e="T37" id="Seg_506" s="T36">Всё.</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg"
                          name="gg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top"
                          name="Top"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc"
                          name="Foc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
