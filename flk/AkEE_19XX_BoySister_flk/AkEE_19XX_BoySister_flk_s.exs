<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID092815ED-F1D9-0B79-5725-F8497E45B934">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>AkEE_19XX_BoySister_flk</transcription-name>
         <referenced-file url="AkEE_19XX_BoySister_flk.wav" />
         <referenced-file url="AkEE_19XX_BoySister_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\flk\AkEE_19XX_BoySister_flk\AkEE_19XX_BoySister_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">1333</ud-information>
            <ud-information attribute-name="# HIAT:w">948</ud-information>
            <ud-information attribute-name="# e">948</ud-information>
            <ud-information attribute-name="# HIAT:u">200</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="AkEE">
            <abbreviation>AkEE</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" time="0.0" type="appl" />
         <tli id="T2" time="0.948857142857143" type="appl" />
         <tli id="T3" time="1.897714285714286" type="appl" />
         <tli id="T4" time="2.846571428571429" type="appl" />
         <tli id="T5" time="3.795428571428572" type="appl" />
         <tli id="T6" time="4.744285714285715" type="appl" />
         <tli id="T7" time="5.693142857142858" type="appl" />
         <tli id="T8" time="7.4533192472424314" />
         <tli id="T9" time="7.7136000000000005" type="appl" />
         <tli id="T10" time="8.7852" type="appl" />
         <tli id="T11" time="9.8568" type="appl" />
         <tli id="T12" time="10.9284" type="appl" />
         <tli id="T13" time="12.0" type="appl" />
         <tli id="T14" time="12.8125" type="appl" />
         <tli id="T15" time="13.625" type="appl" />
         <tli id="T16" time="14.4375" type="appl" />
         <tli id="T17" time="15.533303976811148" />
         <tli id="T18" time="16.15" type="appl" />
         <tli id="T19" time="17.05" type="appl" />
         <tli id="T20" time="17.95" type="appl" />
         <tli id="T21" time="18.85" type="appl" />
         <tli id="T22" time="19.803333341742473" />
         <tli id="T23" time="20.365000000000002" type="appl" />
         <tli id="T24" time="20.98" type="appl" />
         <tli id="T25" time="21.595" type="appl" />
         <tli id="T26" time="22.29666684571629" />
         <tli id="T27" time="22.842" type="appl" />
         <tli id="T28" time="23.474" type="appl" />
         <tli id="T29" time="24.106" type="appl" />
         <tli id="T30" time="24.70466685210844" />
         <tli id="T31" time="25.391571428571428" type="appl" />
         <tli id="T32" time="26.045142857142856" type="appl" />
         <tli id="T33" time="26.698714285714285" type="appl" />
         <tli id="T34" time="27.352285714285713" type="appl" />
         <tli id="T35" time="28.005857142857142" type="appl" />
         <tli id="T36" time="28.65942857142857" type="appl" />
         <tli id="T37" time="29.393001090544356" />
         <tli id="T38" time="30.087249999999997" type="appl" />
         <tli id="T39" time="30.8615" type="appl" />
         <tli id="T40" time="31.635749999999998" type="appl" />
         <tli id="T41" time="32.78999987886103" />
         <tli id="T42" time="33.45133333333333" type="appl" />
         <tli id="T43" time="34.492666666666665" type="appl" />
         <tli id="T44" time="35.64066506428152" />
         <tli id="T45" time="36.1732" type="appl" />
         <tli id="T46" time="36.8124" type="appl" />
         <tli id="T47" time="37.4516" type="appl" />
         <tli id="T48" time="38.090799999999994" type="appl" />
         <tli id="T49" time="38.73" type="appl" />
         <tli id="T50" time="39.49333333333333" type="appl" />
         <tli id="T51" time="40.25666666666667" type="appl" />
         <tli id="T52" time="41.02" type="appl" />
         <tli id="T53" time="41.91" type="appl" />
         <tli id="T54" time="42.8" type="appl" />
         <tli id="T55" time="43.632" type="appl" />
         <tli id="T56" time="44.464" type="appl" />
         <tli id="T57" time="45.296" type="appl" />
         <tli id="T58" time="46.128" type="appl" />
         <tli id="T59" time="46.959999999999994" type="appl" />
         <tli id="T60" time="47.791999999999994" type="appl" />
         <tli id="T61" time="48.623999999999995" type="appl" />
         <tli id="T62" time="49.455999999999996" type="appl" />
         <tli id="T63" time="50.288" type="appl" />
         <tli id="T64" time="51.532" type="appl" />
         <tli id="T65" time="52.776" type="appl" />
         <tli id="T66" time="54.9465628226942" />
         <tli id="T67" time="55.594" type="appl" />
         <tli id="T68" time="57.168000000000006" type="appl" />
         <tli id="T69" time="58.742000000000004" type="appl" />
         <tli id="T70" time="60.20933282445747" />
         <tli id="T71" time="61.170857142857145" type="appl" />
         <tli id="T72" time="62.02571428571429" type="appl" />
         <tli id="T73" time="62.88057142857143" type="appl" />
         <tli id="T74" time="63.73542857142857" type="appl" />
         <tli id="T75" time="64.59028571428571" type="appl" />
         <tli id="T76" time="65.44514285714286" type="appl" />
         <tli id="T77" time="66.3" type="appl" />
         <tli id="T78" time="67.073" type="appl" />
         <tli id="T79" time="67.846" type="appl" />
         <tli id="T80" time="68.619" type="appl" />
         <tli id="T81" time="69.392" type="appl" />
         <tli id="T82" time="70.165" type="appl" />
         <tli id="T83" time="70.96466406036625" />
         <tli id="T84" time="71.53383333333333" type="appl" />
         <tli id="T85" time="72.12966666666667" type="appl" />
         <tli id="T86" time="72.72550000000001" type="appl" />
         <tli id="T87" time="73.32133333333334" type="appl" />
         <tli id="T88" time="73.91716666666667" type="appl" />
         <tli id="T89" time="74.2263310731976" />
         <tli id="T90" time="75.4484" type="appl" />
         <tli id="T91" time="76.38380000000001" type="appl" />
         <tli id="T92" time="77.3192" type="appl" />
         <tli id="T93" time="78.2546" type="appl" />
         <tli id="T94" time="79.19" type="appl" />
         <tli id="T95" time="79.89574999999999" type="appl" />
         <tli id="T96" time="80.6015" type="appl" />
         <tli id="T97" time="81.30725000000001" type="appl" />
         <tli id="T98" time="83.47317557667485" />
         <tli id="T99" time="83.6115" type="appl" />
         <tli id="T100" time="85.99317081411459" />
         <tli id="T101" time="86.04954545454545" type="appl" />
         <tli id="T102" time="86.8890909090909" type="appl" />
         <tli id="T103" time="87.72863636363635" type="appl" />
         <tli id="T104" time="88.56818181818181" type="appl" />
         <tli id="T105" time="89.40772727272727" type="appl" />
         <tli id="T106" time="90.24727272727272" type="appl" />
         <tli id="T107" time="91.08681818181817" type="appl" />
         <tli id="T108" time="91.92636363636363" type="appl" />
         <tli id="T109" time="92.76590909090909" type="appl" />
         <tli id="T110" time="93.60545454545453" type="appl" />
         <tli id="T111" time="94.39166405631472" />
         <tli id="T112" time="95.04383333333332" type="appl" />
         <tli id="T113" time="95.64266666666666" type="appl" />
         <tli id="T114" time="96.2415" type="appl" />
         <tli id="T115" time="96.84033333333333" type="appl" />
         <tli id="T116" time="97.43916666666667" type="appl" />
         <tli id="T117" time="98.19799800852542" />
         <tli id="T118" time="98.9275" type="appl" />
         <tli id="T119" time="99.817" type="appl" />
         <tli id="T120" time="100.7065" type="appl" />
         <tli id="T121" time="101.596" type="appl" />
         <tli id="T122" time="102.4855" type="appl" />
         <tli id="T123" time="103.375" type="appl" />
         <tli id="T124" time="104.2" type="appl" />
         <tli id="T125" time="105.025" type="appl" />
         <tli id="T126" time="105.85" type="appl" />
         <tli id="T127" time="106.675" type="appl" />
         <tli id="T128" time="107.77999812858924" />
         <tli id="T129" time="109.094" type="appl" />
         <tli id="T130" time="110.688" type="appl" />
         <tli id="T131" time="111.32900000000001" type="appl" />
         <tli id="T132" time="111.97" type="appl" />
         <tli id="T133" time="112.611" type="appl" />
         <tli id="T134" time="113.252" type="appl" />
         <tli id="T135" time="113.893" type="appl" />
         <tli id="T136" time="114.53399999999999" type="appl" />
         <tli id="T137" time="115.20833695429239" />
         <tli id="T138" time="115.9485" type="appl" />
         <tli id="T139" time="116.722" type="appl" />
         <tli id="T140" time="117.49549999999999" type="appl" />
         <tli id="T141" time="118.269" type="appl" />
         <tli id="T142" time="119.0425" type="appl" />
         <tli id="T143" time="119.816" type="appl" />
         <tli id="T144" time="120.5895" type="appl" />
         <tli id="T145" time="121.15633352562351" />
         <tli id="T146" time="122.143" type="appl" />
         <tli id="T147" time="122.923" type="appl" />
         <tli id="T148" time="123.703" type="appl" />
         <tli id="T149" time="124.483" type="appl" />
         <tli id="T150" time="125.38967187916832" />
         <tli id="T151" time="126.26257142857143" type="appl" />
         <tli id="T152" time="127.26214285714286" type="appl" />
         <tli id="T153" time="128.26171428571428" type="appl" />
         <tli id="T154" time="129.26128571428572" type="appl" />
         <tli id="T155" time="130.26085714285713" type="appl" />
         <tli id="T156" time="131.26042857142858" type="appl" />
         <tli id="T157" time="132.29999736095257" />
         <tli id="T158" time="133.25133333333332" type="appl" />
         <tli id="T159" time="134.24266666666668" type="appl" />
         <tli id="T160" time="135.15400238381108" />
         <tli id="T161" time="136.07128571428572" type="appl" />
         <tli id="T162" time="136.90857142857143" type="appl" />
         <tli id="T163" time="137.74585714285715" type="appl" />
         <tli id="T164" time="138.58314285714286" type="appl" />
         <tli id="T165" time="139.42042857142857" type="appl" />
         <tli id="T166" time="140.2577142857143" type="appl" />
         <tli id="T167" time="141.2216601873268" />
         <tli id="T168" time="141.8075" type="appl" />
         <tli id="T169" time="142.52" type="appl" />
         <tli id="T170" time="143.23250000000002" type="appl" />
         <tli id="T171" time="143.945" type="appl" />
         <tli id="T172" time="144.6575" type="appl" />
         <tli id="T173" time="145.37" type="appl" />
         <tli id="T174" time="145.98833333333334" type="appl" />
         <tli id="T175" time="146.60666666666665" type="appl" />
         <tli id="T176" time="147.25166181200916" />
         <tli id="T177" time="147.982" type="appl" />
         <tli id="T178" time="148.739" type="appl" />
         <tli id="T179" time="149.49599999999998" type="appl" />
         <tli id="T180" time="150.253" type="appl" />
         <tli id="T181" time="150.9366678685185" />
         <tli id="T182" time="151.91562499999998" type="appl" />
         <tli id="T183" time="152.82125" type="appl" />
         <tli id="T184" time="153.726875" type="appl" />
         <tli id="T185" time="154.6325" type="appl" />
         <tli id="T186" time="155.53812499999998" type="appl" />
         <tli id="T187" time="156.44375" type="appl" />
         <tli id="T188" time="157.349375" type="appl" />
         <tli id="T189" time="158.3016669701779" />
         <tli id="T190" time="158.968" type="appl" />
         <tli id="T191" time="159.68099999999998" type="appl" />
         <tli id="T192" time="160.394" type="appl" />
         <tli id="T193" time="161.107" type="appl" />
         <tli id="T194" time="161.86666023266707" />
         <tli id="T195" time="162.462" type="appl" />
         <tli id="T196" time="163.10399999999998" type="appl" />
         <tli id="T197" time="163.746" type="appl" />
         <tli id="T198" time="164.388" type="appl" />
         <tli id="T199" time="165.21666692229454" />
         <tli id="T200" time="165.782" type="appl" />
         <tli id="T201" time="166.534" type="appl" />
         <tli id="T202" time="167.286" type="appl" />
         <tli id="T203" time="168.1046692767345" />
         <tli id="T204" time="168.8904" type="appl" />
         <tli id="T205" time="169.74280000000002" type="appl" />
         <tli id="T206" time="170.5952" type="appl" />
         <tli id="T207" time="171.44760000000002" type="appl" />
         <tli id="T208" time="172.37333308435078" />
         <tli id="T209" time="172.84828571428574" type="appl" />
         <tli id="T210" time="173.39657142857143" type="appl" />
         <tli id="T211" time="173.94485714285716" type="appl" />
         <tli id="T212" time="174.49314285714286" type="appl" />
         <tli id="T213" time="175.04142857142858" type="appl" />
         <tli id="T214" time="175.58971428571428" type="appl" />
         <tli id="T215" time="176.19800033524655" />
         <tli id="T216" time="176.96114285714287" type="appl" />
         <tli id="T217" time="177.78428571428572" type="appl" />
         <tli id="T218" time="178.60742857142858" type="appl" />
         <tli id="T219" time="179.43057142857143" type="appl" />
         <tli id="T220" time="180.2537142857143" type="appl" />
         <tli id="T221" time="181.07685714285714" type="appl" />
         <tli id="T222" time="182.2529888918726" />
         <tli id="T223" time="182.93333333333334" type="appl" />
         <tli id="T224" time="183.96666666666667" type="appl" />
         <tli id="T225" time="185.0" type="appl" />
         <tli id="T226" time="185.55" type="appl" />
         <tli id="T227" time="186.1" type="appl" />
         <tli id="T228" time="186.65" type="appl" />
         <tli id="T229" time="187.2" type="appl" />
         <tli id="T230" time="187.75" type="appl" />
         <tli id="T231" time="188.43666666666667" type="appl" />
         <tli id="T232" time="189.12333333333333" type="appl" />
         <tli id="T233" time="189.81" type="appl" />
         <tli id="T234" time="190.41" type="appl" />
         <tli id="T235" time="191.01000000000002" type="appl" />
         <tli id="T236" time="191.65000238237758" />
         <tli id="T237" time="192.3305" type="appl" />
         <tli id="T238" time="193.0976689172537" />
         <tli id="T239" time="193.79749999999999" type="appl" />
         <tli id="T240" time="194.54399999999998" type="appl" />
         <tli id="T241" time="195.2905" type="appl" />
         <tli id="T242" time="196.037" type="appl" />
         <tli id="T243" time="196.7835" type="appl" />
         <tli id="T244" time="197.44333778767077" />
         <tli id="T245" time="198.38675" type="appl" />
         <tli id="T246" time="199.24349999999998" type="appl" />
         <tli id="T247" time="200.10025" type="appl" />
         <tli id="T248" time="201.14366933604967" />
         <tli id="T249" time="201.75" type="appl" />
         <tli id="T250" time="202.543" type="appl" />
         <tli id="T251" time="203.336" type="appl" />
         <tli id="T252" time="204.1928" type="appl" />
         <tli id="T253" time="205.0496" type="appl" />
         <tli id="T254" time="205.90640000000002" type="appl" />
         <tli id="T255" time="206.7632" type="appl" />
         <tli id="T256" time="207.7066647450893" />
         <tli id="T257" time="208.323" type="appl" />
         <tli id="T258" time="209.026" type="appl" />
         <tli id="T259" time="209.72899999999998" type="appl" />
         <tli id="T260" time="210.432" type="appl" />
         <tli id="T261" time="211.21166666666664" type="appl" />
         <tli id="T262" time="211.99133333333333" type="appl" />
         <tli id="T263" time="212.77100000000002" type="appl" />
         <tli id="T264" time="213.55066666666667" type="appl" />
         <tli id="T265" time="214.33033333333333" type="appl" />
         <tli id="T266" time="215.116663761703" />
         <tli id="T267" time="215.96550000000002" type="appl" />
         <tli id="T268" time="216.85432974850673" />
         <tli id="T269" time="217.43514285714286" type="appl" />
         <tli id="T270" time="218.0492857142857" type="appl" />
         <tli id="T271" time="218.66342857142857" type="appl" />
         <tli id="T272" time="219.27757142857143" type="appl" />
         <tli id="T273" time="219.8917142857143" type="appl" />
         <tli id="T274" time="220.50585714285714" type="appl" />
         <tli id="T275" time="221.12" type="appl" />
         <tli id="T276" time="221.8075" type="appl" />
         <tli id="T277" time="222.495" type="appl" />
         <tli id="T278" time="223.1825" type="appl" />
         <tli id="T279" time="223.87" type="appl" />
         <tli id="T280" time="224.4265" type="appl" />
         <tli id="T281" time="224.983" type="appl" />
         <tli id="T282" time="225.5395" type="appl" />
         <tli id="T283" time="226.096" type="appl" />
         <tli id="T284" time="226.86271428571428" type="appl" />
         <tli id="T285" time="227.62942857142858" type="appl" />
         <tli id="T286" time="228.39614285714285" type="appl" />
         <tli id="T287" time="229.16285714285715" type="appl" />
         <tli id="T288" time="229.92957142857142" type="appl" />
         <tli id="T289" time="230.69628571428572" type="appl" />
         <tli id="T290" time="231.2896670503744" />
         <tli id="T291" time="232.25475" type="appl" />
         <tli id="T292" time="233.04649999999998" type="appl" />
         <tli id="T293" time="233.83825" type="appl" />
         <tli id="T294" time="234.60999931691708" />
         <tli id="T295" time="235.44" type="appl" />
         <tli id="T296" time="236.2566628715404" />
         <tli id="T297" time="236.803" type="appl" />
         <tli id="T298" time="237.356" type="appl" />
         <tli id="T299" time="237.909" type="appl" />
         <tli id="T300" time="238.462" type="appl" />
         <tli id="T301" time="239.50099999999998" type="appl" />
         <tli id="T302" time="240.67998784519455" />
         <tli id="T303" time="241.315" type="appl" />
         <tli id="T304" time="242.08999999999997" type="appl" />
         <tli id="T305" time="242.86499999999998" type="appl" />
         <tli id="T306" time="243.64" type="appl" />
         <tli id="T307" time="244.506" type="appl" />
         <tli id="T308" time="245.372" type="appl" />
         <tli id="T309" time="246.06650000000002" type="appl" />
         <tli id="T310" time="246.76100000000002" type="appl" />
         <tli id="T311" time="247.4555" type="appl" />
         <tli id="T312" time="248.40333262378013" />
         <tli id="T313" time="249.03028571428572" type="appl" />
         <tli id="T314" time="249.91057142857144" type="appl" />
         <tli id="T315" time="250.79085714285716" type="appl" />
         <tli id="T316" time="251.67114285714285" type="appl" />
         <tli id="T317" time="252.55142857142857" type="appl" />
         <tli id="T318" time="253.4317142857143" type="appl" />
         <tli id="T319" time="254.39199318076004" />
         <tli id="T320" time="255.09333333333333" type="appl" />
         <tli id="T321" time="255.87466666666668" type="appl" />
         <tli id="T322" time="256.656" type="appl" />
         <tli id="T323" time="257.43733333333336" type="appl" />
         <tli id="T324" time="258.21866666666665" type="appl" />
         <tli id="T325" time="259.02667192164165" />
         <tli id="T326" time="259.4479" type="appl" />
         <tli id="T327" time="259.8958" type="appl" />
         <tli id="T328" time="260.3437" type="appl" />
         <tli id="T329" time="260.7916" type="appl" />
         <tli id="T330" time="261.2395" type="appl" />
         <tli id="T331" time="261.68739999999997" type="appl" />
         <tli id="T332" time="262.1353" type="appl" />
         <tli id="T333" time="262.5832" type="appl" />
         <tli id="T334" time="263.0311" type="appl" />
         <tli id="T335" time="263.63234030068344" />
         <tli id="T336" time="264.4552" type="appl" />
         <tli id="T337" time="265.4314" type="appl" />
         <tli id="T338" time="266.4076" type="appl" />
         <tli id="T339" time="267.3838" type="appl" />
         <tli id="T340" time="269.0261582318614" />
         <tli id="T341" time="269.32975" type="appl" />
         <tli id="T342" time="270.29949999999997" type="appl" />
         <tli id="T343" time="271.26925" type="appl" />
         <tli id="T344" time="272.41232370731876" />
         <tli id="T345" time="272.85925" type="appl" />
         <tli id="T346" time="273.47950000000003" type="appl" />
         <tli id="T347" time="274.09975000000003" type="appl" />
         <tli id="T348" time="274.72" type="appl" />
         <tli id="T349" time="275.416" type="appl" />
         <tli id="T350" time="276.112" type="appl" />
         <tli id="T351" time="276.808" type="appl" />
         <tli id="T352" time="277.504" type="appl" />
         <tli id="T353" time="278.4194738126884" />
         <tli id="T354" time="278.98449999999997" type="appl" />
         <tli id="T355" time="279.769" type="appl" />
         <tli id="T356" time="280.5535" type="appl" />
         <tli id="T357" time="281.338" type="appl" />
         <tli id="T358" time="282.1225" type="appl" />
         <tli id="T359" time="282.907" type="appl" />
         <tli id="T360" time="283.6915" type="appl" />
         <tli id="T361" time="284.476" type="appl" />
         <tli id="T362" time="285.26050000000004" type="appl" />
         <tli id="T363" time="286.0850062005341" />
         <tli id="T364" time="286.85625000000005" type="appl" />
         <tli id="T365" time="287.6675" type="appl" />
         <tli id="T366" time="288.47875" type="appl" />
         <tli id="T367" time="289.29" type="appl" />
         <tli id="T368" time="290.45225000000005" type="appl" />
         <tli id="T369" time="291.6145" type="appl" />
         <tli id="T370" time="292.77675" type="appl" />
         <tli id="T371" time="294.00566831454137" />
         <tli id="T372" time="295.09933333333333" type="appl" />
         <tli id="T373" time="296.2596666666667" type="appl" />
         <tli id="T374" time="297.42" type="appl" />
         <tli id="T375" time="297.96000000000004" type="appl" />
         <tli id="T376" time="298.5" type="appl" />
         <tli id="T377" time="299.04" type="appl" />
         <tli id="T378" time="299.82175" type="appl" />
         <tli id="T379" time="300.6035" type="appl" />
         <tli id="T380" time="301.38525" type="appl" />
         <tli id="T381" time="302.48700645225546" />
         <tli id="T382" time="303.0213333333333" type="appl" />
         <tli id="T383" time="303.8756666666667" type="appl" />
         <tli id="T384" time="304.73" type="appl" />
         <tli id="T385" time="305.452" type="appl" />
         <tli id="T386" time="306.17400000000004" type="appl" />
         <tli id="T387" time="306.896" type="appl" />
         <tli id="T388" time="307.618" type="appl" />
         <tli id="T389" time="308.34000000000003" type="appl" />
         <tli id="T390" time="309.062" type="appl" />
         <tli id="T391" time="309.9465" type="appl" />
         <tli id="T392" time="310.831" type="appl" />
         <tli id="T393" time="311.7155" type="appl" />
         <tli id="T394" time="312.6200081351435" />
         <tli id="T395" time="313.18240000000003" type="appl" />
         <tli id="T396" time="313.76480000000004" type="appl" />
         <tli id="T397" time="314.3472" type="appl" />
         <tli id="T398" time="314.9296" type="appl" />
         <tli id="T399" time="315.61867434293896" />
         <tli id="T400" time="316.587" type="appl" />
         <tli id="T401" time="317.662" type="appl" />
         <tli id="T402" time="318.737" type="appl" />
         <tli id="T403" time="319.812" type="appl" />
         <tli id="T404" time="320.8203311789505" />
         <tli id="T405" time="321.64025" type="appl" />
         <tli id="T406" time="322.3935" type="appl" />
         <tli id="T407" time="323.14675" type="appl" />
         <tli id="T408" time="323.9" type="appl" />
         <tli id="T409" time="324.78999999999996" type="appl" />
         <tli id="T410" time="325.68" type="appl" />
         <tli id="T411" time="326.57" type="appl" />
         <tli id="T412" time="327.46" type="appl" />
         <tli id="T413" time="328.35" type="appl" />
         <tli id="T414" time="329.3200026153498" />
         <tli id="T415" time="329.9185" type="appl" />
         <tli id="T416" time="330.597" type="appl" />
         <tli id="T417" time="331.2755" type="appl" />
         <tli id="T418" time="331.954" type="appl" />
         <tli id="T419" time="332.6325" type="appl" />
         <tli id="T420" time="333.311" type="appl" />
         <tli id="T421" time="333.98949999999996" type="appl" />
         <tli id="T422" time="334.668" type="appl" />
         <tli id="T423" time="335.3465" type="appl" />
         <tli id="T424" time="336.0449899057396" />
         <tli id="T425" time="336.62375" type="appl" />
         <tli id="T426" time="337.22249999999997" type="appl" />
         <tli id="T427" time="337.82125" type="appl" />
         <tli id="T428" time="338.4400114210349" />
         <tli id="T429" time="339.334625" type="appl" />
         <tli id="T430" time="340.24925" type="appl" />
         <tli id="T431" time="341.163875" type="appl" />
         <tli id="T432" time="342.0785" type="appl" />
         <tli id="T433" time="342.993125" type="appl" />
         <tli id="T434" time="343.90775" type="appl" />
         <tli id="T435" time="344.822375" type="appl" />
         <tli id="T436" time="345.7036695683861" />
         <tli id="T437" time="346.39457142857145" type="appl" />
         <tli id="T438" time="347.0521428571429" type="appl" />
         <tli id="T439" time="347.7097142857143" type="appl" />
         <tli id="T440" time="348.3672857142857" type="appl" />
         <tli id="T441" time="349.0248571428571" type="appl" />
         <tli id="T442" time="349.68242857142855" type="appl" />
         <tli id="T443" time="350.51332193691684" />
         <tli id="T444" time="351.1433333333333" type="appl" />
         <tli id="T445" time="351.94666666666666" type="appl" />
         <tli id="T446" time="352.69667718557537" />
         <tli id="T447" time="354.2" type="appl" />
         <tli id="T448" time="355.53667181824557" />
         <tli id="T449" time="356.32599999999996" type="appl" />
         <tli id="T450" time="357.00199999999995" type="appl" />
         <tli id="T451" time="357.678" type="appl" />
         <tli id="T452" time="358.354" type="appl" />
         <tli id="T453" time="359.0833317820497" />
         <tli id="T454" time="359.81265332036537" />
         <tli id="T455" time="361.52598341566176" />
         <tli id="T456" time="362.59931472049715" />
         <tli id="T0" time="363.8193124148133" />
         <tli id="T949" time="363.97597878539483" type="intp" />
         <tli id="T947" time="364.212645004784" />
         <tli id="T948" time="365.032643455062" />
         <tli id="T457" time="366.539307274272" />
         <tli id="T458" time="367.03930632931963" />
         <tli id="T459" time="367.3095" type="appl" />
         <tli id="T460" time="367.97775" type="appl" />
         <tli id="T461" time="368.80599569877177" />
         <tli id="T462" time="369.288" type="appl" />
         <tli id="T463" time="369.93" type="appl" />
         <tli id="T464" time="370.572" type="appl" />
         <tli id="T465" time="371.214" type="appl" />
         <tli id="T466" time="371.856" type="appl" />
         <tli id="T467" time="372.498" type="appl" />
         <tli id="T468" time="373.1799978489749" />
         <tli id="T469" time="373.89099999999996" type="appl" />
         <tli id="T470" time="374.642" type="appl" />
         <tli id="T471" time="375.393" type="appl" />
         <tli id="T472" time="375.9585555555555" type="appl" />
         <tli id="T473" time="376.5241111111111" type="appl" />
         <tli id="T474" time="377.08966666666663" type="appl" />
         <tli id="T475" time="377.6552222222222" type="appl" />
         <tli id="T476" time="378.22077777777776" type="appl" />
         <tli id="T477" time="378.78633333333335" type="appl" />
         <tli id="T478" time="379.3518888888889" type="appl" />
         <tli id="T479" time="379.91744444444447" type="appl" />
         <tli id="T480" time="380.6430045779089" />
         <tli id="T481" time="381.027625" type="appl" />
         <tli id="T482" time="381.57225" type="appl" />
         <tli id="T483" time="382.116875" type="appl" />
         <tli id="T484" time="382.6615" type="appl" />
         <tli id="T485" time="383.206125" type="appl" />
         <tli id="T486" time="383.75075" type="appl" />
         <tli id="T487" time="384.295375" type="appl" />
         <tli id="T488" time="385.1866678658013" />
         <tli id="T489" time="385.5855714285714" type="appl" />
         <tli id="T490" time="386.3311428571428" type="appl" />
         <tli id="T491" time="387.0767142857143" type="appl" />
         <tli id="T492" time="387.8222857142857" type="appl" />
         <tli id="T493" time="388.5678571428572" type="appl" />
         <tli id="T494" time="389.3134285714286" type="appl" />
         <tli id="T495" time="389.8856694017853" />
         <tli id="T496" time="390.7312" type="appl" />
         <tli id="T497" time="391.40340000000003" type="appl" />
         <tli id="T498" time="392.0756" type="appl" />
         <tli id="T499" time="392.74780000000004" type="appl" />
         <tli id="T500" time="393.5266781456122" />
         <tli id="T501" time="394.37" type="appl" />
         <tli id="T502" time="395.3666746681873" />
         <tli id="T503" time="396.318" type="appl" />
         <tli id="T504" time="397.316" type="appl" />
         <tli id="T505" time="398.31399999999996" type="appl" />
         <tli id="T506" time="399.312" type="appl" />
         <tli id="T507" time="400.31" type="appl" />
         <tli id="T508" time="401.308" type="appl" />
         <tli id="T509" time="402.306" type="appl" />
         <tli id="T510" time="403.41066988243955" />
         <tli id="T511" time="404.15725" type="appl" />
         <tli id="T512" time="405.0105" type="appl" />
         <tli id="T513" time="405.86375" type="appl" />
         <tli id="T514" time="406.717" type="appl" />
         <tli id="T515" time="407.57025" type="appl" />
         <tli id="T516" time="408.4235" type="appl" />
         <tli id="T517" time="409.27675" type="appl" />
         <tli id="T518" time="410.34333907199624" />
         <tli id="T519" time="411.269" type="appl" />
         <tli id="T520" time="412.408" type="appl" />
         <tli id="T521" time="413.6803379737072" />
         <tli id="T522" time="414.12960000000004" type="appl" />
         <tli id="T523" time="414.7122" type="appl" />
         <tli id="T524" time="415.2948" type="appl" />
         <tli id="T525" time="415.87739999999997" type="appl" />
         <tli id="T526" time="416.50666075721597" />
         <tli id="T527" time="417.2" type="appl" />
         <tli id="T528" time="417.94" type="appl" />
         <tli id="T529" time="418.68" type="appl" />
         <tli id="T530" time="419.62665486071285" />
         <tli id="T531" time="420.318" type="appl" />
         <tli id="T532" time="421.216" type="appl" />
         <tli id="T533" time="421.8245" type="appl" />
         <tli id="T534" time="422.433" type="appl" />
         <tli id="T535" time="423.0415" type="appl" />
         <tli id="T536" time="423.82333963769617" />
         <tli id="T537" time="424.245" type="appl" />
         <tli id="T538" time="424.84" type="appl" />
         <tli id="T539" time="425.435" type="appl" />
         <tli id="T540" time="426.03000000000003" type="appl" />
         <tli id="T541" time="426.625" type="appl" />
         <tli id="T542" time="427.2666664634571" />
         <tli id="T543" time="428.262" type="appl" />
         <tli id="T544" time="429.304" type="appl" />
         <tli id="T545" time="430.08779999999996" type="appl" />
         <tli id="T546" time="430.8716" type="appl" />
         <tli id="T547" time="431.6554" type="appl" />
         <tli id="T548" time="432.43919999999997" type="appl" />
         <tli id="T549" time="433.22299999999996" type="appl" />
         <tli id="T550" time="434.0068" type="appl" />
         <tli id="T551" time="434.7906" type="appl" />
         <tli id="T552" time="435.57439999999997" type="appl" />
         <tli id="T553" time="436.3582" type="appl" />
         <tli id="T554" time="437.8325058699881" />
         <tli id="T555" time="438.526" type="appl" />
         <tli id="T556" time="440.1766681064028" />
         <tli id="T557" time="440.88522222222224" type="appl" />
         <tli id="T558" time="441.86044444444445" type="appl" />
         <tli id="T559" time="442.83566666666667" type="appl" />
         <tli id="T560" time="443.8108888888889" type="appl" />
         <tli id="T561" time="444.78611111111115" type="appl" />
         <tli id="T562" time="445.76133333333337" type="appl" />
         <tli id="T563" time="446.7365555555556" type="appl" />
         <tli id="T564" time="447.7117777777778" type="appl" />
         <tli id="T565" time="448.687" type="appl" />
         <tli id="T566" time="449.462" type="appl" />
         <tli id="T567" time="450.237" type="appl" />
         <tli id="T568" time="451.012" type="appl" />
         <tli id="T569" time="451.78700000000003" type="appl" />
         <tli id="T570" time="452.39534293089287" />
         <tli id="T571" time="453.3415" type="appl" />
         <tli id="T572" time="454.121" type="appl" />
         <tli id="T573" time="454.9005" type="appl" />
         <tli id="T574" time="455.89335715329963" />
         <tli id="T575" time="456.5133333333333" type="appl" />
         <tli id="T576" time="457.3466666666667" type="appl" />
         <tli id="T577" time="458.6857997925054" />
         <tli id="T578" time="459.474" type="appl" />
         <tli id="T579" time="460.76800000000003" type="appl" />
         <tli id="T580" time="461.96865817153696" />
         <tli id="T581" time="462.8272727272727" type="appl" />
         <tli id="T582" time="463.5925454545455" type="appl" />
         <tli id="T583" time="464.3578181818182" type="appl" />
         <tli id="T584" time="465.12309090909093" type="appl" />
         <tli id="T585" time="465.88836363636364" type="appl" />
         <tli id="T586" time="466.6536363636364" type="appl" />
         <tli id="T587" time="467.4189090909091" type="appl" />
         <tli id="T588" time="468.18418181818186" type="appl" />
         <tli id="T589" time="468.94945454545456" type="appl" />
         <tli id="T590" time="469.7147272727273" type="appl" />
         <tli id="T591" time="470.48" type="appl" />
         <tli id="T592" time="471.06025" type="appl" />
         <tli id="T593" time="471.6405" type="appl" />
         <tli id="T594" time="472.22075" type="appl" />
         <tli id="T595" time="472.8143147575725" />
         <tli id="T596" time="473.48075" type="appl" />
         <tli id="T597" time="474.16049999999996" type="appl" />
         <tli id="T598" time="474.84024999999997" type="appl" />
         <tli id="T599" time="475.6533198087796" />
         <tli id="T600" time="476.06333333333333" type="appl" />
         <tli id="T601" time="476.6066666666666" type="appl" />
         <tli id="T602" time="477.15" type="appl" />
         <tli id="T603" time="477.6933333333333" type="appl" />
         <tli id="T604" time="478.2366666666666" type="appl" />
         <tli id="T605" time="478.87331372328583" />
         <tli id="T606" time="479.601" type="appl" />
         <tli id="T607" time="480.422" type="appl" />
         <tli id="T608" time="481.243" type="appl" />
         <tli id="T609" time="481.89731842486054" />
         <tli id="T610" time="482.8955" type="appl" />
         <tli id="T611" time="483.727" type="appl" />
         <tli id="T612" time="484.5585" type="appl" />
         <tli id="T613" time="485.3700201951055" />
         <tli id="T614" time="486.1755" type="appl" />
         <tli id="T615" time="486.961" type="appl" />
         <tli id="T616" time="487.89033333333333" type="appl" />
         <tli id="T617" time="488.81966666666665" type="appl" />
         <tli id="T618" time="489.749" type="appl" />
         <tli id="T619" time="490.67833333333334" type="appl" />
         <tli id="T620" time="491.60766666666666" type="appl" />
         <tli id="T621" time="492.84365190395926" />
         <tli id="T622" time="493.128" type="appl" />
         <tli id="T623" time="493.719" type="appl" />
         <tli id="T624" time="494.31" type="appl" />
         <tli id="T625" time="495.111" type="appl" />
         <tli id="T626" time="496.12531236858814" />
         <tli id="T627" time="496.64255555555553" type="appl" />
         <tli id="T628" time="497.3731111111111" type="appl" />
         <tli id="T629" time="498.10366666666664" type="appl" />
         <tli id="T630" time="498.8342222222222" type="appl" />
         <tli id="T631" time="499.5647777777778" type="appl" />
         <tli id="T632" time="500.29533333333336" type="appl" />
         <tli id="T633" time="501.0258888888889" type="appl" />
         <tli id="T634" time="501.75644444444447" type="appl" />
         <tli id="T635" time="502.487" type="appl" />
         <tli id="T636" time="503.1541666666667" type="appl" />
         <tli id="T637" time="503.82133333333337" type="appl" />
         <tli id="T638" time="504.48850000000004" type="appl" />
         <tli id="T639" time="505.15566666666666" type="appl" />
         <tli id="T640" time="505.82283333333334" type="appl" />
         <tli id="T641" time="506.82331298367995" />
         <tli id="T642" time="507.2985" type="appl" />
         <tli id="T643" time="508.10699999999997" type="appl" />
         <tli id="T644" time="508.9155" type="appl" />
         <tli id="T645" time="509.9973174017689" />
         <tli id="T646" time="510.68449999999996" type="appl" />
         <tli id="T647" time="511.645" type="appl" />
         <tli id="T648" time="512.6055" type="appl" />
         <tli id="T649" time="513.566" type="appl" />
         <tli id="T650" time="514.5264999999999" type="appl" />
         <tli id="T651" time="515.5536610674548" />
         <tli id="T652" time="516.1446666666667" type="appl" />
         <tli id="T653" time="516.8023333333333" type="appl" />
         <tli id="T654" time="517.606678020774" />
         <tli id="T655" time="518.645" type="appl" />
         <tli id="T656" time="520.2566730125262" />
         <tli id="T657" time="521.2045" type="appl" />
         <tli id="T658" time="522.6923454926615" />
         <tli id="T659" time="523.19425" type="appl" />
         <tli id="T660" time="523.8095" type="appl" />
         <tli id="T661" time="524.4247499999999" type="appl" />
         <tli id="T662" time="525.2399969278337" />
         <tli id="T663" time="526.4546666666666" type="appl" />
         <tli id="T664" time="527.8693333333333" type="appl" />
         <tli id="T665" time="529.284" type="appl" />
         <tli id="T666" time="530.6986666666667" type="appl" />
         <tli id="T667" time="532.1133333333333" type="appl" />
         <tli id="T668" time="533.528" type="appl" />
         <tli id="T669" time="534.2795" type="appl" />
         <tli id="T670" time="535.0310000000001" type="appl" />
         <tli id="T671" time="535.7825" type="appl" />
         <tli id="T672" time="536.534" type="appl" />
         <tli id="T673" time="537.2855000000001" type="appl" />
         <tli id="T674" time="538.0970038792677" />
         <tli id="T675" time="538.754" type="appl" />
         <tli id="T676" time="539.471" type="appl" />
         <tli id="T677" time="540.188" type="appl" />
         <tli id="T678" time="540.905" type="appl" />
         <tli id="T679" time="541.6220000000001" type="appl" />
         <tli id="T680" time="542.339" type="appl" />
         <tli id="T681" time="543.056" type="appl" />
         <tli id="T682" time="543.773" type="appl" />
         <tli id="T683" time="544.49" type="appl" />
         <tli id="T684" time="545.20475" type="appl" />
         <tli id="T685" time="545.9195" type="appl" />
         <tli id="T686" time="546.6342500000001" type="appl" />
         <tli id="T687" time="547.5356839576933" />
         <tli id="T688" time="548.0193333333334" type="appl" />
         <tli id="T689" time="548.6896666666667" type="appl" />
         <tli id="T690" time="549.4466699294381" />
         <tli id="T691" time="550.0678" type="appl" />
         <tli id="T692" time="550.7756" type="appl" />
         <tli id="T693" time="551.4834" type="appl" />
         <tli id="T694" time="552.1912" type="appl" />
         <tli id="T695" time="552.8256739600965" />
         <tli id="T696" time="553.43425" type="appl" />
         <tli id="T697" time="553.9694999999999" type="appl" />
         <tli id="T698" time="554.50475" type="appl" />
         <tli id="T699" time="555.0799926163074" />
         <tli id="T700" time="555.9448571428571" type="appl" />
         <tli id="T701" time="556.8497142857143" type="appl" />
         <tli id="T702" time="557.7545714285715" type="appl" />
         <tli id="T703" time="558.6594285714285" type="appl" />
         <tli id="T704" time="559.5642857142857" type="appl" />
         <tli id="T705" time="560.4691428571429" type="appl" />
         <tli id="T706" time="561.4539909867008" />
         <tli id="T707" time="561.9916000000001" type="appl" />
         <tli id="T708" time="562.6092" type="appl" />
         <tli id="T709" time="563.2268" type="appl" />
         <tli id="T710" time="563.8444" type="appl" />
         <tli id="T711" time="564.4953394054975" />
         <tli id="T712" time="565.1134999999999" type="appl" />
         <tli id="T713" time="565.765" type="appl" />
         <tli id="T714" time="566.4165" type="appl" />
         <tli id="T715" time="567.068" type="appl" />
         <tli id="T716" time="567.7194999999999" type="appl" />
         <tli id="T717" time="568.371" type="appl" />
         <tli id="T718" time="569.0225" type="appl" />
         <tli id="T719" time="569.7473086464231" />
         <tli id="T720" time="571.1272" type="appl" />
         <tli id="T721" time="572.5804" type="appl" />
         <tli id="T722" time="574.0336" type="appl" />
         <tli id="T723" time="575.4868" type="appl" />
         <tli id="T724" time="577.160002970443" />
         <tli id="T725" time="578.0205000000001" type="appl" />
         <tli id="T726" time="579.101" type="appl" />
         <tli id="T727" time="580.1814999999999" type="appl" />
         <tli id="T728" time="581.2419744225574" />
         <tli id="T729" time="581.9789999999999" type="appl" />
         <tli id="T730" time="582.6959999999999" type="appl" />
         <tli id="T731" time="583.413" type="appl" />
         <tli id="T732" time="584.13" type="appl" />
         <tli id="T733" time="585.3238" type="appl" />
         <tli id="T734" time="586.5176" type="appl" />
         <tli id="T735" time="587.7114" type="appl" />
         <tli id="T736" time="588.9052" type="appl" />
         <tli id="T737" time="590.078988971409" />
         <tli id="T738" time="590.8391666666666" type="appl" />
         <tli id="T739" time="591.5793333333334" type="appl" />
         <tli id="T740" time="592.3195000000001" type="appl" />
         <tli id="T741" time="593.0596666666667" type="appl" />
         <tli id="T742" time="593.7998333333333" type="appl" />
         <tli id="T743" time="594.5200222449295" />
         <tli id="T744" time="595.2431428571429" type="appl" />
         <tli id="T745" time="595.9462857142856" type="appl" />
         <tli id="T746" time="596.6494285714285" type="appl" />
         <tli id="T747" time="597.3525714285714" type="appl" />
         <tli id="T748" time="598.0557142857143" type="appl" />
         <tli id="T749" time="598.7588571428571" type="appl" />
         <tli id="T750" time="599.462" type="appl" />
         <tli id="T751" time="600.2813333333334" type="appl" />
         <tli id="T752" time="601.1006666666666" type="appl" />
         <tli id="T753" time="601.92" type="appl" />
         <tli id="T754" time="602.4553999999999" type="appl" />
         <tli id="T755" time="602.9907999999999" type="appl" />
         <tli id="T756" time="603.5262" type="appl" />
         <tli id="T757" time="604.0616" type="appl" />
         <tli id="T758" time="604.6503155996027" />
         <tli id="T759" time="605.2417777777778" type="appl" />
         <tli id="T760" time="605.8865555555556" type="appl" />
         <tli id="T761" time="606.5313333333334" type="appl" />
         <tli id="T762" time="607.1761111111111" type="appl" />
         <tli id="T763" time="607.8208888888888" type="appl" />
         <tli id="T764" time="608.4656666666666" type="appl" />
         <tli id="T765" time="609.1104444444444" type="appl" />
         <tli id="T766" time="609.7552222222222" type="appl" />
         <tli id="T767" time="610.3199923844326" />
         <tli id="T768" time="610.9233333333333" type="appl" />
         <tli id="T769" time="611.4466666666667" type="appl" />
         <tli id="T770" time="611.9166560335512" />
         <tli id="T771" time="612.58975" type="appl" />
         <tli id="T772" time="613.2094999999999" type="appl" />
         <tli id="T773" time="613.82925" type="appl" />
         <tli id="T774" time="614.449" type="appl" />
         <tli id="T775" time="615.07675" type="appl" />
         <tli id="T776" time="615.7045" type="appl" />
         <tli id="T777" time="616.33225" type="appl" />
         <tli id="T778" time="616.96" type="appl" />
         <tli id="T779" time="617.7896666666667" type="appl" />
         <tli id="T780" time="618.6193333333333" type="appl" />
         <tli id="T781" time="619.449" type="appl" />
         <tli id="T782" time="620.0525" type="appl" />
         <tli id="T783" time="620.656" type="appl" />
         <tli id="T784" time="621.2595" type="appl" />
         <tli id="T785" time="621.863" type="appl" />
         <tli id="T786" time="622.4665" type="appl" />
         <tli id="T787" time="623.2500200311977" />
         <tli id="T788" time="623.7585" type="appl" />
         <tli id="T789" time="624.447" type="appl" />
         <tli id="T790" time="625.1355" type="appl" />
         <tli id="T791" time="625.7773590047584" />
         <tli id="T792" time="626.7783333333333" type="appl" />
         <tli id="T793" time="627.7326666666667" type="appl" />
         <tli id="T794" time="628.6003224196161" />
         <tli id="T795" time="629.351375" type="appl" />
         <tli id="T796" time="630.01575" type="appl" />
         <tli id="T797" time="630.680125" type="appl" />
         <tli id="T798" time="631.3444999999999" type="appl" />
         <tli id="T799" time="632.008875" type="appl" />
         <tli id="T800" time="632.6732499999999" type="appl" />
         <tli id="T801" time="633.337625" type="appl" />
         <tli id="T802" time="634.33880115775" />
         <tli id="T803" time="634.6116666666667" type="appl" />
         <tli id="T804" time="635.2213333333333" type="appl" />
         <tli id="T805" time="635.8309999999999" type="appl" />
         <tli id="T806" time="636.4406666666666" type="appl" />
         <tli id="T807" time="637.0503333333334" type="appl" />
         <tli id="T808" time="637.8066591871493" />
         <tli id="T809" time="638.200875" type="appl" />
         <tli id="T810" time="638.7417499999999" type="appl" />
         <tli id="T811" time="639.2826249999999" type="appl" />
         <tli id="T812" time="639.8235" type="appl" />
         <tli id="T813" time="640.364375" type="appl" />
         <tli id="T814" time="640.90525" type="appl" />
         <tli id="T815" time="641.4461249999999" type="appl" />
         <tli id="T816" time="642.3870151140293" />
         <tli id="T817" time="642.7076" type="appl" />
         <tli id="T818" time="643.4282" type="appl" />
         <tli id="T819" time="644.1488" type="appl" />
         <tli id="T820" time="644.8694" type="appl" />
         <tli id="T821" time="645.59" type="appl" />
         <tli id="T822" time="646.205" type="appl" />
         <tli id="T823" time="646.82" type="appl" />
         <tli id="T824" time="647.435" type="appl" />
         <tli id="T825" time="648.05" type="appl" />
         <tli id="T826" time="648.665" type="appl" />
         <tli id="T827" time="649.6121056259366" />
         <tli id="T828" time="650.13" type="appl" />
         <tli id="T829" time="650.98" type="appl" />
         <tli id="T830" time="651.9166846038266" />
         <tli id="T831" time="652.4895" type="appl" />
         <tli id="T832" time="653.149" type="appl" />
         <tli id="T833" time="653.8085000000001" type="appl" />
         <tli id="T834" time="654.4680000000001" type="appl" />
         <tli id="T835" time="655.1275" type="appl" />
         <tli id="T836" time="655.8203230596407" />
         <tli id="T837" time="656.7245" type="appl" />
         <tli id="T838" time="657.662" type="appl" />
         <tli id="T839" time="658.5995" type="appl" />
         <tli id="T840" time="659.5036494318244" />
         <tli id="T841" time="659.9996" type="appl" />
         <tli id="T842" time="660.4622" type="appl" />
         <tli id="T843" time="660.9248" type="appl" />
         <tli id="T844" time="661.3874000000001" type="appl" />
         <tli id="T845" time="661.85" type="appl" />
         <tli id="T846" time="662.9956666666667" type="appl" />
         <tli id="T847" time="664.1413333333334" type="appl" />
         <tli id="T848" time="665.287" type="appl" />
         <tli id="T849" time="665.9575" type="appl" />
         <tli id="T850" time="666.628" type="appl" />
         <tli id="T851" time="667.2985" type="appl" />
         <tli id="T852" time="667.9689999999999" type="appl" />
         <tli id="T853" time="668.6395" type="appl" />
         <tli id="T854" time="669.31" type="appl" />
         <tli id="T855" time="670.295" type="appl" />
         <tli id="T856" time="672.5387289633844" />
         <tli id="T857" time="673.5" type="appl" />
         <tli id="T858" time="675.7866915750325" />
         <tli id="T859" time="676.2025" type="appl" />
         <tli id="T860" time="676.685" type="appl" />
         <tli id="T861" time="677.1675" type="appl" />
         <tli id="T862" time="677.65" type="appl" />
         <tli id="T863" time="678.1814999999999" type="appl" />
         <tli id="T864" time="678.713" type="appl" />
         <tli id="T865" time="679.2445" type="appl" />
         <tli id="T866" time="680.1653812163765" />
         <tli id="T867" time="680.5168" type="appl" />
         <tli id="T868" time="681.2576" type="appl" />
         <tli id="T869" time="681.9984" type="appl" />
         <tli id="T870" time="682.7392" type="appl" />
         <tli id="T871" time="683.6600100285148" />
         <tli id="T872" time="684.1964" type="appl" />
         <tli id="T873" time="684.9128000000001" type="appl" />
         <tli id="T874" time="685.6292" type="appl" />
         <tli id="T875" time="686.3456" type="appl" />
         <tli id="T876" time="687.1019826901683" />
         <tli id="T877" time="687.928" type="appl" />
         <tli id="T878" time="688.794" type="appl" />
         <tli id="T879" time="689.7066652675568" />
         <tli id="T880" time="690.5606666666666" type="appl" />
         <tli id="T881" time="691.4613333333333" type="appl" />
         <tli id="T882" time="692.4286913731767" />
         <tli id="T883" time="693.184" type="appl" />
         <tli id="T884" time="694.006" type="appl" />
         <tli id="T885" time="694.828" type="appl" />
         <tli id="T886" time="695.6899872929594" />
         <tli id="T887" time="696.3385000000001" type="appl" />
         <tli id="T888" time="697.6386815267723" />
         <tli id="T889" time="697.8907777777778" type="appl" />
         <tli id="T890" time="698.7545555555556" type="appl" />
         <tli id="T891" time="699.6183333333333" type="appl" />
         <tli id="T892" time="700.4821111111112" type="appl" />
         <tli id="T893" time="701.3458888888889" type="appl" />
         <tli id="T894" time="702.2096666666667" type="appl" />
         <tli id="T895" time="703.0734444444445" type="appl" />
         <tli id="T896" time="703.9372222222223" type="appl" />
         <tli id="T897" time="704.9210115138355" />
         <tli id="T898" time="705.68525" type="appl" />
         <tli id="T899" time="706.5695000000001" type="appl" />
         <tli id="T900" time="707.45375" type="appl" />
         <tli id="T901" time="708.3979841093424" />
         <tli id="T902" time="708.9515454545455" type="appl" />
         <tli id="T903" time="709.5650909090908" type="appl" />
         <tli id="T904" time="710.1786363636363" type="appl" />
         <tli id="T905" time="710.7921818181818" type="appl" />
         <tli id="T906" time="711.4057272727273" type="appl" />
         <tli id="T907" time="712.0192727272727" type="appl" />
         <tli id="T908" time="712.6328181818182" type="appl" />
         <tli id="T909" time="713.2463636363636" type="appl" />
         <tli id="T910" time="713.8599090909091" type="appl" />
         <tli id="T911" time="714.4734545454545" type="appl" />
         <tli id="T912" time="715.087" type="appl" />
         <tli id="T913" time="715.97275" type="appl" />
         <tli id="T914" time="716.8585" type="appl" />
         <tli id="T915" time="717.74425" type="appl" />
         <tli id="T916" time="718.76999575719" />
         <tli id="T917" time="719.2362499999999" type="appl" />
         <tli id="T918" time="719.8425" type="appl" />
         <tli id="T919" time="720.44875" type="appl" />
         <tli id="T920" time="721.055" type="appl" />
         <tli id="T921" time="721.677923076923" type="appl" />
         <tli id="T922" time="722.3008461538461" type="appl" />
         <tli id="T923" time="722.9237692307692" type="appl" />
         <tli id="T924" time="723.5466923076923" type="appl" />
         <tli id="T925" time="724.1696153846153" type="appl" />
         <tli id="T926" time="724.7925384615385" type="appl" />
         <tli id="T927" time="725.4154615384615" type="appl" />
         <tli id="T928" time="726.0383846153846" type="appl" />
         <tli id="T929" time="726.6613076923077" type="appl" />
         <tli id="T930" time="727.2842307692308" type="appl" />
         <tli id="T931" time="727.9071538461538" type="appl" />
         <tli id="T932" time="728.530076923077" type="appl" />
         <tli id="T933" time="729.153" type="appl" />
         <tli id="T934" time="729.919" type="appl" />
         <tli id="T935" time="730.6850000000001" type="appl" />
         <tli id="T936" time="731.451" type="appl" />
         <tli id="T937" time="732.217" type="appl" />
         <tli id="T938" time="732.9830000000001" type="appl" />
         <tli id="T939" time="733.8889776003654" />
         <tli id="T940" time="734.44" type="appl" />
         <tli id="T941" time="735.131" type="appl" />
         <tli id="T942" time="735.822" type="appl" />
         <tli id="T943" time="736.513" type="appl" />
         <tli id="T944" time="737.2040000000001" type="appl" />
         <tli id="T945" time="737.895" type="appl" />
         <tli id="T946" time="738.586" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="AkEE"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T946" id="Seg_0" n="sc" s="T1">
               <ts e="T8" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Ihilleːŋ</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">dulgaːnnar</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">oloŋkolorun</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_12" n="HIAT:ip">"</nts>
                  <ts e="T5" id="Seg_14" n="HIAT:w" s="T4">Edʼiːjdeːk</ts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_17" n="HIAT:w" s="T5">u͡ol</ts>
                  <nts id="Seg_18" n="HIAT:ip">"</nts>
                  <nts id="Seg_19" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_21" n="HIAT:w" s="T6">di͡en</ts>
                  <nts id="Seg_22" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_24" n="HIAT:w" s="T7">aːttaːgɨ</ts>
                  <nts id="Seg_25" n="HIAT:ip">.</nts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T13" id="Seg_28" n="HIAT:u" s="T8">
                  <ts e="T9" id="Seg_30" n="HIAT:w" s="T8">U͡ol</ts>
                  <nts id="Seg_31" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_33" n="HIAT:w" s="T9">edʼiːjin</ts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_36" n="HIAT:w" s="T10">kɨtta</ts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_39" n="HIAT:w" s="T11">ikki͡ejkeːn</ts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_42" n="HIAT:w" s="T12">olorbuttara</ts>
                  <nts id="Seg_43" n="HIAT:ip">.</nts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_46" n="HIAT:u" s="T13">
                  <ts e="T14" id="Seg_48" n="HIAT:w" s="T13">Edʼiːje</ts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_51" n="HIAT:w" s="T14">ülehitteːk</ts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_54" n="HIAT:w" s="T15">uːs</ts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_57" n="HIAT:w" s="T16">bagajɨ</ts>
                  <nts id="Seg_58" n="HIAT:ip">.</nts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T22" id="Seg_61" n="HIAT:u" s="T17">
                  <ts e="T18" id="Seg_63" n="HIAT:w" s="T17">Hetiːlere</ts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_66" n="HIAT:w" s="T18">bugdiː</ts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_69" n="HIAT:w" s="T19">taba</ts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_72" n="HIAT:w" s="T20">tiriːlerinen</ts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_75" n="HIAT:w" s="T21">habɨːlaːktar</ts>
                  <nts id="Seg_76" n="HIAT:ip">.</nts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_79" n="HIAT:u" s="T22">
                  <ts e="T23" id="Seg_81" n="HIAT:w" s="T22">Uraha</ts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_84" n="HIAT:w" s="T23">dʼi͡eni</ts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_87" n="HIAT:w" s="T24">bejete</ts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_90" n="HIAT:w" s="T25">tuttaːččɨ</ts>
                  <nts id="Seg_91" n="HIAT:ip">.</nts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T30" id="Seg_94" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_96" n="HIAT:w" s="T26">Tabalarɨn</ts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_99" n="HIAT:w" s="T27">da</ts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_102" n="HIAT:w" s="T28">hajɨn</ts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_105" n="HIAT:w" s="T29">keteːčči</ts>
                  <nts id="Seg_106" n="HIAT:ip">.</nts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T37" id="Seg_109" n="HIAT:u" s="T30">
                  <ts e="T31" id="Seg_111" n="HIAT:w" s="T30">U͡ol</ts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_114" n="HIAT:w" s="T31">baltɨta</ts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_117" n="HIAT:w" s="T32">kühüŋŋütten</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_120" n="HIAT:w" s="T33">utujbut</ts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_123" n="HIAT:w" s="T34">kün</ts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_126" n="HIAT:w" s="T35">taksɨ͡ar</ts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_129" n="HIAT:w" s="T36">di͡eri</ts>
                  <nts id="Seg_130" n="HIAT:ip">.</nts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T41" id="Seg_133" n="HIAT:u" s="T37">
                  <ts e="T38" id="Seg_135" n="HIAT:w" s="T37">Ü͡ördere</ts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_138" n="HIAT:w" s="T38">ɨraːppɨt</ts>
                  <nts id="Seg_139" n="HIAT:ip">,</nts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_142" n="HIAT:w" s="T39">oŋkoloro</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_145" n="HIAT:w" s="T40">keŋeːbit</ts>
                  <nts id="Seg_146" n="HIAT:ip">.</nts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T44" id="Seg_149" n="HIAT:u" s="T41">
                  <ts e="T42" id="Seg_151" n="HIAT:w" s="T41">Kɨːs</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_154" n="HIAT:w" s="T42">oŋko</ts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_157" n="HIAT:w" s="T43">ularɨtɨːhɨ</ts>
                  <nts id="Seg_158" n="HIAT:ip">.</nts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T49" id="Seg_161" n="HIAT:u" s="T44">
                  <ts e="T45" id="Seg_163" n="HIAT:w" s="T44">Ü͡örün</ts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_166" n="HIAT:w" s="T45">egelle</ts>
                  <nts id="Seg_167" n="HIAT:ip">,</nts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_170" n="HIAT:w" s="T46">taba</ts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_173" n="HIAT:w" s="T47">tutunna</ts>
                  <nts id="Seg_174" n="HIAT:ip">,</nts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_177" n="HIAT:w" s="T48">kölünne</ts>
                  <nts id="Seg_178" n="HIAT:ip">.</nts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T52" id="Seg_181" n="HIAT:u" s="T49">
                  <ts e="T50" id="Seg_183" n="HIAT:w" s="T49">Baltɨtɨn</ts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_186" n="HIAT:w" s="T50">uhugunnara</ts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_189" n="HIAT:w" s="T51">hatɨːr</ts>
                  <nts id="Seg_190" n="HIAT:ip">.</nts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T54" id="Seg_193" n="HIAT:u" s="T52">
                  <ts e="T53" id="Seg_195" n="HIAT:w" s="T52">Amattan</ts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_198" n="HIAT:w" s="T53">uhuktubat</ts>
                  <nts id="Seg_199" n="HIAT:ip">.</nts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T63" id="Seg_202" n="HIAT:u" s="T54">
                  <ts e="T55" id="Seg_204" n="HIAT:w" s="T54">Uraha</ts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_207" n="HIAT:w" s="T55">dʼi͡etin</ts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_210" n="HIAT:w" s="T56">kastaːn</ts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_213" n="HIAT:w" s="T57">baraːn</ts>
                  <nts id="Seg_214" n="HIAT:ip">,</nts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_217" n="HIAT:w" s="T58">baltɨtɨn</ts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_220" n="HIAT:w" s="T59">kɨrsa</ts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_223" n="HIAT:w" s="T60">hu͡organɨn</ts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_226" n="HIAT:w" s="T61">tuːra</ts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_229" n="HIAT:w" s="T62">tarta</ts>
                  <nts id="Seg_230" n="HIAT:ip">.</nts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T66" id="Seg_233" n="HIAT:u" s="T63">
                  <ts e="T64" id="Seg_235" n="HIAT:w" s="T63">Örüküjen</ts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_238" n="HIAT:w" s="T64">uhugunna</ts>
                  <nts id="Seg_239" n="HIAT:ip">,</nts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_242" n="HIAT:w" s="T65">taŋɨnna</ts>
                  <nts id="Seg_243" n="HIAT:ip">.</nts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T70" id="Seg_246" n="HIAT:u" s="T66">
                  <ts e="T67" id="Seg_248" n="HIAT:w" s="T66">Hɨrgatɨgar</ts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_251" n="HIAT:w" s="T67">oloroːt</ts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_254" n="HIAT:w" s="T68">emi͡e</ts>
                  <nts id="Seg_255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_257" n="HIAT:w" s="T69">utujda</ts>
                  <nts id="Seg_258" n="HIAT:ip">.</nts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T77" id="Seg_261" n="HIAT:u" s="T70">
                  <ts e="T71" id="Seg_263" n="HIAT:w" s="T70">Mastanarɨgar</ts>
                  <nts id="Seg_264" n="HIAT:ip">,</nts>
                  <nts id="Seg_265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_267" n="HIAT:w" s="T71">buːstanarɨgar</ts>
                  <nts id="Seg_268" n="HIAT:ip">,</nts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_271" n="HIAT:w" s="T72">tabalanarɨgar</ts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_274" n="HIAT:w" s="T73">bu͡olan</ts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_277" n="HIAT:w" s="T74">kɨːs</ts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_280" n="HIAT:w" s="T75">bert</ts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_283" n="HIAT:w" s="T76">hɨlajar</ts>
                  <nts id="Seg_284" n="HIAT:ip">.</nts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T83" id="Seg_287" n="HIAT:u" s="T77">
                  <ts e="T78" id="Seg_289" n="HIAT:w" s="T77">Hɨlajan</ts>
                  <nts id="Seg_290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_292" n="HIAT:w" s="T78">utujbut</ts>
                  <nts id="Seg_293" n="HIAT:ip">,</nts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_296" n="HIAT:w" s="T79">utuja</ts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_299" n="HIAT:w" s="T80">hɨtan</ts>
                  <nts id="Seg_300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_302" n="HIAT:w" s="T81">toŋon</ts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_305" n="HIAT:w" s="T82">kaːlbɨt</ts>
                  <nts id="Seg_306" n="HIAT:ip">.</nts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T89" id="Seg_309" n="HIAT:u" s="T83">
                  <ts e="T84" id="Seg_311" n="HIAT:w" s="T83">U͡ol</ts>
                  <nts id="Seg_312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_314" n="HIAT:w" s="T84">baltɨta</ts>
                  <nts id="Seg_315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_317" n="HIAT:w" s="T85">össü͡ö</ts>
                  <nts id="Seg_318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_320" n="HIAT:w" s="T86">üs</ts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_323" n="HIAT:w" s="T87">künü</ts>
                  <nts id="Seg_324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_326" n="HIAT:w" s="T88">utujbut</ts>
                  <nts id="Seg_327" n="HIAT:ip">.</nts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T94" id="Seg_330" n="HIAT:u" s="T89">
                  <ts e="T90" id="Seg_332" n="HIAT:w" s="T89">Uhuktubuta</ts>
                  <nts id="Seg_333" n="HIAT:ip">,</nts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_336" n="HIAT:w" s="T90">körbüte</ts>
                  <nts id="Seg_337" n="HIAT:ip">,</nts>
                  <nts id="Seg_338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_340" n="HIAT:w" s="T91">edʼiːje</ts>
                  <nts id="Seg_341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_343" n="HIAT:w" s="T92">toŋon</ts>
                  <nts id="Seg_344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_346" n="HIAT:w" s="T93">kaːlbɨt</ts>
                  <nts id="Seg_347" n="HIAT:ip">.</nts>
                  <nts id="Seg_348" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T98" id="Seg_350" n="HIAT:u" s="T94">
                  <ts e="T95" id="Seg_352" n="HIAT:w" s="T94">Ü͡öre</ts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_355" n="HIAT:w" s="T95">barɨta</ts>
                  <nts id="Seg_356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_358" n="HIAT:w" s="T96">toŋon</ts>
                  <nts id="Seg_359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_361" n="HIAT:w" s="T97">ölbüt</ts>
                  <nts id="Seg_362" n="HIAT:ip">.</nts>
                  <nts id="Seg_363" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T100" id="Seg_365" n="HIAT:u" s="T98">
                  <ts e="T99" id="Seg_367" n="HIAT:w" s="T98">Hobu͡oj</ts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_370" n="HIAT:w" s="T99">oksubut</ts>
                  <nts id="Seg_371" n="HIAT:ip">.</nts>
                  <nts id="Seg_372" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T111" id="Seg_374" n="HIAT:u" s="T100">
                  <nts id="Seg_375" n="HIAT:ip">"</nts>
                  <ts e="T101" id="Seg_377" n="HIAT:w" s="T100">U͡ot</ts>
                  <nts id="Seg_378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_380" n="HIAT:w" s="T101">otunnakpɨna</ts>
                  <nts id="Seg_381" n="HIAT:ip">,</nts>
                  <nts id="Seg_382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_384" n="HIAT:w" s="T102">bugaːt</ts>
                  <nts id="Seg_385" n="HIAT:ip">,</nts>
                  <nts id="Seg_386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_388" n="HIAT:w" s="T103">edʼiːjim</ts>
                  <nts id="Seg_389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_391" n="HIAT:w" s="T104">tilli͡e</ts>
                  <nts id="Seg_392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_394" n="HIAT:w" s="T105">da</ts>
                  <nts id="Seg_395" n="HIAT:ip">"</nts>
                  <nts id="Seg_396" n="HIAT:ip">,</nts>
                  <nts id="Seg_397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_399" n="HIAT:w" s="T106">diː</ts>
                  <nts id="Seg_400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_402" n="HIAT:w" s="T107">hanaːbɨt</ts>
                  <nts id="Seg_403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_405" n="HIAT:w" s="T108">u͡ol</ts>
                  <nts id="Seg_406" n="HIAT:ip">,</nts>
                  <nts id="Seg_407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_408" n="HIAT:ip">"</nts>
                  <ts e="T110" id="Seg_410" n="HIAT:w" s="T109">mastana</ts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_413" n="HIAT:w" s="T110">barɨ͡akka</ts>
                  <nts id="Seg_414" n="HIAT:ip">.</nts>
                  <nts id="Seg_415" n="HIAT:ip">"</nts>
                  <nts id="Seg_416" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T117" id="Seg_418" n="HIAT:u" s="T111">
                  <ts e="T112" id="Seg_420" n="HIAT:w" s="T111">Kajalaːk</ts>
                  <nts id="Seg_421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_423" n="HIAT:w" s="T112">ürek</ts>
                  <nts id="Seg_424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_426" n="HIAT:w" s="T113">ürdünen</ts>
                  <nts id="Seg_427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_429" n="HIAT:w" s="T114">mas</ts>
                  <nts id="Seg_430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_432" n="HIAT:w" s="T115">di͡ek</ts>
                  <nts id="Seg_433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_435" n="HIAT:w" s="T116">hüːrbüt</ts>
                  <nts id="Seg_436" n="HIAT:ip">.</nts>
                  <nts id="Seg_437" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T123" id="Seg_439" n="HIAT:u" s="T117">
                  <ts e="T118" id="Seg_441" n="HIAT:w" s="T117">Körbüte</ts>
                  <nts id="Seg_442" n="HIAT:ip">,</nts>
                  <nts id="Seg_443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_445" n="HIAT:w" s="T118">kallaːntan</ts>
                  <nts id="Seg_446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_448" n="HIAT:w" s="T119">tutuktaːk</ts>
                  <nts id="Seg_449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_451" n="HIAT:w" s="T120">otuː</ts>
                  <nts id="Seg_452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_454" n="HIAT:w" s="T121">gɨdaːrɨja</ts>
                  <nts id="Seg_455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_457" n="HIAT:w" s="T122">hɨtar</ts>
                  <nts id="Seg_458" n="HIAT:ip">.</nts>
                  <nts id="Seg_459" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T128" id="Seg_461" n="HIAT:u" s="T123">
                  <ts e="T124" id="Seg_463" n="HIAT:w" s="T123">Abaːhɨ</ts>
                  <nts id="Seg_464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_466" n="HIAT:w" s="T124">üčehege</ts>
                  <nts id="Seg_467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_469" n="HIAT:w" s="T125">et</ts>
                  <nts id="Seg_470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_472" n="HIAT:w" s="T126">ü͡ölene</ts>
                  <nts id="Seg_473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_475" n="HIAT:w" s="T127">oloror</ts>
                  <nts id="Seg_476" n="HIAT:ip">.</nts>
                  <nts id="Seg_477" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T130" id="Seg_479" n="HIAT:u" s="T128">
                  <ts e="T129" id="Seg_481" n="HIAT:w" s="T128">U͡olu</ts>
                  <nts id="Seg_482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_484" n="HIAT:w" s="T129">körböt</ts>
                  <nts id="Seg_485" n="HIAT:ip">.</nts>
                  <nts id="Seg_486" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T137" id="Seg_488" n="HIAT:u" s="T130">
                  <ts e="T131" id="Seg_490" n="HIAT:w" s="T130">Bu</ts>
                  <nts id="Seg_491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_493" n="HIAT:w" s="T131">u͡ol</ts>
                  <nts id="Seg_494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_496" n="HIAT:w" s="T132">hulburuta</ts>
                  <nts id="Seg_497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_499" n="HIAT:w" s="T133">tɨːta-tɨːta</ts>
                  <nts id="Seg_500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_502" n="HIAT:w" s="T134">oŋu͡orguttan</ts>
                  <nts id="Seg_503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_505" n="HIAT:w" s="T135">hiː</ts>
                  <nts id="Seg_506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_508" n="HIAT:w" s="T136">olorbut</ts>
                  <nts id="Seg_509" n="HIAT:ip">.</nts>
                  <nts id="Seg_510" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T145" id="Seg_512" n="HIAT:u" s="T137">
                  <nts id="Seg_513" n="HIAT:ip">"</nts>
                  <ts e="T138" id="Seg_515" n="HIAT:w" s="T137">Kajtak</ts>
                  <nts id="Seg_516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_518" n="HIAT:w" s="T138">tu͡olkulaːbatɨn</ts>
                  <nts id="Seg_519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_521" n="HIAT:w" s="T139">ke</ts>
                  <nts id="Seg_522" n="HIAT:ip">"</nts>
                  <nts id="Seg_523" n="HIAT:ip">,</nts>
                  <nts id="Seg_524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_526" n="HIAT:w" s="T140">diːr</ts>
                  <nts id="Seg_527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_529" n="HIAT:w" s="T141">Abaːhɨ</ts>
                  <nts id="Seg_530" n="HIAT:ip">,</nts>
                  <nts id="Seg_531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_532" n="HIAT:ip">"</nts>
                  <ts e="T143" id="Seg_534" n="HIAT:w" s="T142">etterim</ts>
                  <nts id="Seg_535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_537" n="HIAT:w" s="T143">u͡okka</ts>
                  <nts id="Seg_538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_540" n="HIAT:w" s="T144">tühellerin</ts>
                  <nts id="Seg_541" n="HIAT:ip">?</nts>
                  <nts id="Seg_542" n="HIAT:ip">"</nts>
                  <nts id="Seg_543" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T150" id="Seg_545" n="HIAT:u" s="T145">
                  <ts e="T146" id="Seg_547" n="HIAT:w" s="T145">Abaːhɨ</ts>
                  <nts id="Seg_548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_550" n="HIAT:w" s="T146">u͡olu</ts>
                  <nts id="Seg_551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_553" n="HIAT:w" s="T147">körön</ts>
                  <nts id="Seg_554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_556" n="HIAT:w" s="T148">ölöröːrü</ts>
                  <nts id="Seg_557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_559" n="HIAT:w" s="T149">gɨmmɨt</ts>
                  <nts id="Seg_560" n="HIAT:ip">.</nts>
                  <nts id="Seg_561" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T157" id="Seg_563" n="HIAT:u" s="T150">
                  <ts e="T151" id="Seg_565" n="HIAT:w" s="T150">U͡ol</ts>
                  <nts id="Seg_566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_568" n="HIAT:w" s="T151">uruːkatɨnan</ts>
                  <nts id="Seg_569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_571" n="HIAT:w" s="T152">Abaːhɨ</ts>
                  <nts id="Seg_572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_574" n="HIAT:w" s="T153">agɨs</ts>
                  <nts id="Seg_575" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_577" n="HIAT:w" s="T154">bahɨn</ts>
                  <nts id="Seg_578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_580" n="HIAT:w" s="T155">tuːra</ts>
                  <nts id="Seg_581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_583" n="HIAT:w" s="T156">oksubut</ts>
                  <nts id="Seg_584" n="HIAT:ip">.</nts>
                  <nts id="Seg_585" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T160" id="Seg_587" n="HIAT:u" s="T157">
                  <ts e="T158" id="Seg_589" n="HIAT:w" s="T157">Bastara</ts>
                  <nts id="Seg_590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_592" n="HIAT:w" s="T158">üŋküːlüː</ts>
                  <nts id="Seg_593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_595" n="HIAT:w" s="T159">hɨldʼallar</ts>
                  <nts id="Seg_596" n="HIAT:ip">.</nts>
                  <nts id="Seg_597" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T163" id="Seg_599" n="HIAT:u" s="T160">
                  <ts e="T161" id="Seg_601" n="HIAT:w" s="T160">U͡ol</ts>
                  <nts id="Seg_602" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_604" n="HIAT:w" s="T161">onno</ts>
                  <nts id="Seg_605" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_607" n="HIAT:w" s="T162">časkɨjbɨt</ts>
                  <nts id="Seg_608" n="HIAT:ip">:</nts>
                  <nts id="Seg_609" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T167" id="Seg_611" n="HIAT:u" s="T163">
                  <nts id="Seg_612" n="HIAT:ip">"</nts>
                  <ts e="T164" id="Seg_614" n="HIAT:w" s="T163">Bukatɨːr</ts>
                  <nts id="Seg_615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_617" n="HIAT:w" s="T164">ikkileːččite</ts>
                  <nts id="Seg_618" n="HIAT:ip">,</nts>
                  <nts id="Seg_619" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_621" n="HIAT:w" s="T165">ühüsteːččite</ts>
                  <nts id="Seg_622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_624" n="HIAT:w" s="T166">hu͡ok</ts>
                  <nts id="Seg_625" n="HIAT:ip">!</nts>
                  <nts id="Seg_626" n="HIAT:ip">"</nts>
                  <nts id="Seg_627" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T173" id="Seg_629" n="HIAT:u" s="T167">
                  <ts e="T168" id="Seg_631" n="HIAT:w" s="T167">Abaːhɨ</ts>
                  <nts id="Seg_632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_634" n="HIAT:w" s="T168">tü͡öhün</ts>
                  <nts id="Seg_635" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_637" n="HIAT:w" s="T169">tɨːra</ts>
                  <nts id="Seg_638" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_640" n="HIAT:w" s="T170">tardan</ts>
                  <nts id="Seg_641" n="HIAT:ip">,</nts>
                  <nts id="Seg_642" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_644" n="HIAT:w" s="T171">taːhɨ</ts>
                  <nts id="Seg_645" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_647" n="HIAT:w" s="T172">oroːto</ts>
                  <nts id="Seg_648" n="HIAT:ip">.</nts>
                  <nts id="Seg_649" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T176" id="Seg_651" n="HIAT:u" s="T173">
                  <ts e="T174" id="Seg_653" n="HIAT:w" s="T173">Taːs</ts>
                  <nts id="Seg_654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_656" n="HIAT:w" s="T174">hürekteːk</ts>
                  <nts id="Seg_657" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_659" n="HIAT:w" s="T175">ebit</ts>
                  <nts id="Seg_660" n="HIAT:ip">.</nts>
                  <nts id="Seg_661" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T181" id="Seg_663" n="HIAT:u" s="T176">
                  <ts e="T177" id="Seg_665" n="HIAT:w" s="T176">U͡ol</ts>
                  <nts id="Seg_666" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_668" n="HIAT:w" s="T177">taːhɨ</ts>
                  <nts id="Seg_669" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_671" n="HIAT:w" s="T178">tebe-tebe</ts>
                  <nts id="Seg_672" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_674" n="HIAT:w" s="T179">bara</ts>
                  <nts id="Seg_675" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_677" n="HIAT:w" s="T180">turbut</ts>
                  <nts id="Seg_678" n="HIAT:ip">.</nts>
                  <nts id="Seg_679" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T189" id="Seg_681" n="HIAT:u" s="T181">
                  <ts e="T182" id="Seg_683" n="HIAT:w" s="T181">Innitiger</ts>
                  <nts id="Seg_684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_686" n="HIAT:w" s="T182">hir</ts>
                  <nts id="Seg_687" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_689" n="HIAT:w" s="T183">aŋardaːk</ts>
                  <nts id="Seg_690" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_692" n="HIAT:w" s="T184">haga</ts>
                  <nts id="Seg_693" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_695" n="HIAT:w" s="T185">taːs</ts>
                  <nts id="Seg_696" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_698" n="HIAT:w" s="T186">turbut</ts>
                  <nts id="Seg_699" n="HIAT:ip">,</nts>
                  <nts id="Seg_700" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_702" n="HIAT:w" s="T187">taːs</ts>
                  <nts id="Seg_703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_705" n="HIAT:w" s="T188">kaja</ts>
                  <nts id="Seg_706" n="HIAT:ip">.</nts>
                  <nts id="Seg_707" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T194" id="Seg_709" n="HIAT:u" s="T189">
                  <ts e="T190" id="Seg_711" n="HIAT:w" s="T189">Taːs</ts>
                  <nts id="Seg_712" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_714" n="HIAT:w" s="T190">kajanɨ</ts>
                  <nts id="Seg_715" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_717" n="HIAT:w" s="T191">taːs</ts>
                  <nts id="Seg_718" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_720" n="HIAT:w" s="T192">hüreginen</ts>
                  <nts id="Seg_721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_723" n="HIAT:w" s="T193">teppit</ts>
                  <nts id="Seg_724" n="HIAT:ip">.</nts>
                  <nts id="Seg_725" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T199" id="Seg_727" n="HIAT:u" s="T194">
                  <ts e="T195" id="Seg_729" n="HIAT:w" s="T194">Kajata</ts>
                  <nts id="Seg_730" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_732" n="HIAT:w" s="T195">ikki</ts>
                  <nts id="Seg_733" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_735" n="HIAT:w" s="T196">aŋɨ</ts>
                  <nts id="Seg_736" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_738" n="HIAT:w" s="T197">kajdan</ts>
                  <nts id="Seg_739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_741" n="HIAT:w" s="T198">tüspüt</ts>
                  <nts id="Seg_742" n="HIAT:ip">.</nts>
                  <nts id="Seg_743" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T203" id="Seg_745" n="HIAT:u" s="T199">
                  <ts e="T200" id="Seg_747" n="HIAT:w" s="T199">Oduːlaːn</ts>
                  <nts id="Seg_748" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_750" n="HIAT:w" s="T200">körbüte</ts>
                  <nts id="Seg_751" n="HIAT:ip">,</nts>
                  <nts id="Seg_752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_754" n="HIAT:w" s="T201">hir</ts>
                  <nts id="Seg_755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_757" n="HIAT:w" s="T202">köstör</ts>
                  <nts id="Seg_758" n="HIAT:ip">.</nts>
                  <nts id="Seg_759" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T208" id="Seg_761" n="HIAT:u" s="T203">
                  <ts e="T204" id="Seg_763" n="HIAT:w" s="T203">Onno</ts>
                  <nts id="Seg_764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_766" n="HIAT:w" s="T204">baran</ts>
                  <nts id="Seg_767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_769" n="HIAT:w" s="T205">hɨrgalaːk</ts>
                  <nts id="Seg_770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_772" n="HIAT:w" s="T206">hu͡olun</ts>
                  <nts id="Seg_773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_775" n="HIAT:w" s="T207">körbüt</ts>
                  <nts id="Seg_776" n="HIAT:ip">.</nts>
                  <nts id="Seg_777" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T215" id="Seg_779" n="HIAT:u" s="T208">
                  <ts e="T209" id="Seg_781" n="HIAT:w" s="T208">Paːs</ts>
                  <nts id="Seg_782" n="HIAT:ip">,</nts>
                  <nts id="Seg_783" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_785" n="HIAT:w" s="T209">kapkaːn</ts>
                  <nts id="Seg_786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_788" n="HIAT:w" s="T210">körüne</ts>
                  <nts id="Seg_789" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_791" n="HIAT:w" s="T211">hɨldʼar</ts>
                  <nts id="Seg_792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_794" n="HIAT:w" s="T212">kihi</ts>
                  <nts id="Seg_795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_797" n="HIAT:w" s="T213">ete</ts>
                  <nts id="Seg_798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_800" n="HIAT:w" s="T214">ebit</ts>
                  <nts id="Seg_801" n="HIAT:ip">.</nts>
                  <nts id="Seg_802" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T222" id="Seg_804" n="HIAT:u" s="T215">
                  <ts e="T216" id="Seg_806" n="HIAT:w" s="T215">Töhö</ts>
                  <nts id="Seg_807" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_809" n="HIAT:w" s="T216">da</ts>
                  <nts id="Seg_810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_812" n="HIAT:w" s="T217">bu͡olbakka</ts>
                  <nts id="Seg_813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_815" n="HIAT:w" s="T218">kɨrsahɨt</ts>
                  <nts id="Seg_816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_818" n="HIAT:w" s="T219">utarɨ</ts>
                  <nts id="Seg_819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_821" n="HIAT:w" s="T220">iherin</ts>
                  <nts id="Seg_822" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_824" n="HIAT:w" s="T221">körbüt</ts>
                  <nts id="Seg_825" n="HIAT:ip">.</nts>
                  <nts id="Seg_826" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T225" id="Seg_828" n="HIAT:u" s="T222">
                  <ts e="T223" id="Seg_830" n="HIAT:w" s="T222">Kelen</ts>
                  <nts id="Seg_831" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_833" n="HIAT:w" s="T223">turda</ts>
                  <nts id="Seg_834" n="HIAT:ip">,</nts>
                  <nts id="Seg_835" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_837" n="HIAT:w" s="T224">doroːbolosto</ts>
                  <nts id="Seg_838" n="HIAT:ip">.</nts>
                  <nts id="Seg_839" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T230" id="Seg_841" n="HIAT:u" s="T225">
                  <nts id="Seg_842" n="HIAT:ip">"</nts>
                  <ts e="T226" id="Seg_844" n="HIAT:w" s="T225">Tuːgu</ts>
                  <nts id="Seg_845" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_847" n="HIAT:w" s="T226">gɨna</ts>
                  <nts id="Seg_848" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_850" n="HIAT:w" s="T227">hɨldʼagɨn</ts>
                  <nts id="Seg_851" n="HIAT:ip">"</nts>
                  <nts id="Seg_852" n="HIAT:ip">,</nts>
                  <nts id="Seg_853" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_855" n="HIAT:w" s="T228">ɨjɨppɨt</ts>
                  <nts id="Seg_856" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_858" n="HIAT:w" s="T229">u͡ol</ts>
                  <nts id="Seg_859" n="HIAT:ip">.</nts>
                  <nts id="Seg_860" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T233" id="Seg_862" n="HIAT:u" s="T230">
                  <nts id="Seg_863" n="HIAT:ip">"</nts>
                  <ts e="T231" id="Seg_865" n="HIAT:w" s="T230">Paːs</ts>
                  <nts id="Seg_866" n="HIAT:ip">,</nts>
                  <nts id="Seg_867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_869" n="HIAT:w" s="T231">kapkaːn</ts>
                  <nts id="Seg_870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_872" n="HIAT:w" s="T232">körünebin</ts>
                  <nts id="Seg_873" n="HIAT:ip">.</nts>
                  <nts id="Seg_874" n="HIAT:ip">"</nts>
                  <nts id="Seg_875" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T236" id="Seg_877" n="HIAT:u" s="T233">
                  <nts id="Seg_878" n="HIAT:ip">"</nts>
                  <ts e="T234" id="Seg_880" n="HIAT:w" s="T233">ɨraːk</ts>
                  <nts id="Seg_881" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_883" n="HIAT:w" s="T234">dʼi͡eleːkkit</ts>
                  <nts id="Seg_884" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_886" n="HIAT:w" s="T235">du͡o</ts>
                  <nts id="Seg_887" n="HIAT:ip">?</nts>
                  <nts id="Seg_888" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T238" id="Seg_890" n="HIAT:u" s="T236">
                  <ts e="T237" id="Seg_892" n="HIAT:w" s="T236">Kas</ts>
                  <nts id="Seg_893" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_895" n="HIAT:w" s="T237">kihigitij</ts>
                  <nts id="Seg_896" n="HIAT:ip">?</nts>
                  <nts id="Seg_897" n="HIAT:ip">"</nts>
                  <nts id="Seg_898" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T244" id="Seg_900" n="HIAT:u" s="T238">
                  <nts id="Seg_901" n="HIAT:ip">"</nts>
                  <ts e="T239" id="Seg_903" n="HIAT:w" s="T238">Inʼebin</ts>
                  <nts id="Seg_904" n="HIAT:ip">,</nts>
                  <nts id="Seg_905" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_907" n="HIAT:w" s="T239">agabɨn</ts>
                  <nts id="Seg_908" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_910" n="HIAT:w" s="T240">kɨtta</ts>
                  <nts id="Seg_911" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_913" n="HIAT:w" s="T241">olorobun</ts>
                  <nts id="Seg_914" n="HIAT:ip">"</nts>
                  <nts id="Seg_915" n="HIAT:ip">,</nts>
                  <nts id="Seg_916" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_918" n="HIAT:w" s="T242">di͡ebit</ts>
                  <nts id="Seg_919" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_921" n="HIAT:w" s="T243">u͡ol</ts>
                  <nts id="Seg_922" n="HIAT:ip">.</nts>
                  <nts id="Seg_923" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T248" id="Seg_925" n="HIAT:u" s="T244">
                  <nts id="Seg_926" n="HIAT:ip">"</nts>
                  <ts e="T245" id="Seg_928" n="HIAT:w" s="T244">Meŋehin</ts>
                  <nts id="Seg_929" n="HIAT:ip">,</nts>
                  <nts id="Seg_930" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_932" n="HIAT:w" s="T245">illi͡em</ts>
                  <nts id="Seg_933" n="HIAT:ip">,</nts>
                  <nts id="Seg_934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_936" n="HIAT:w" s="T246">ɨ͡aldʼɨt</ts>
                  <nts id="Seg_937" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_939" n="HIAT:w" s="T247">bu͡ol</ts>
                  <nts id="Seg_940" n="HIAT:ip">.</nts>
                  <nts id="Seg_941" n="HIAT:ip">"</nts>
                  <nts id="Seg_942" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T251" id="Seg_944" n="HIAT:u" s="T248">
                  <ts e="T249" id="Seg_946" n="HIAT:w" s="T248">Uraha</ts>
                  <nts id="Seg_947" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_949" n="HIAT:w" s="T249">dʼi͡etiger</ts>
                  <nts id="Seg_950" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_952" n="HIAT:w" s="T250">egelbit</ts>
                  <nts id="Seg_953" n="HIAT:ip">.</nts>
                  <nts id="Seg_954" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T256" id="Seg_956" n="HIAT:u" s="T251">
                  <ts e="T252" id="Seg_958" n="HIAT:w" s="T251">Ogonnʼordoːk</ts>
                  <nts id="Seg_959" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_961" n="HIAT:w" s="T252">emeːksin</ts>
                  <nts id="Seg_962" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_964" n="HIAT:w" s="T253">utarɨ</ts>
                  <nts id="Seg_965" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_967" n="HIAT:w" s="T254">taksannar</ts>
                  <nts id="Seg_968" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_970" n="HIAT:w" s="T255">körsübütter</ts>
                  <nts id="Seg_971" n="HIAT:ip">.</nts>
                  <nts id="Seg_972" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T260" id="Seg_974" n="HIAT:u" s="T256">
                  <nts id="Seg_975" n="HIAT:ip">"</nts>
                  <ts e="T257" id="Seg_977" n="HIAT:w" s="T256">Kaja</ts>
                  <nts id="Seg_978" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_980" n="HIAT:w" s="T257">hirten</ts>
                  <nts id="Seg_981" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_983" n="HIAT:w" s="T258">kelliŋ</ts>
                  <nts id="Seg_984" n="HIAT:ip">"</nts>
                  <nts id="Seg_985" n="HIAT:ip">,</nts>
                  <nts id="Seg_986" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_988" n="HIAT:w" s="T259">ɨjɨppɨttar</ts>
                  <nts id="Seg_989" n="HIAT:ip">.</nts>
                  <nts id="Seg_990" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T266" id="Seg_992" n="HIAT:u" s="T260">
                  <nts id="Seg_993" n="HIAT:ip">"</nts>
                  <ts e="T261" id="Seg_995" n="HIAT:w" s="T260">Manna</ts>
                  <nts id="Seg_996" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_998" n="HIAT:w" s="T261">tu͡ora</ts>
                  <nts id="Seg_999" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_1001" n="HIAT:w" s="T262">karaktaːk</ts>
                  <nts id="Seg_1002" n="HIAT:ip">,</nts>
                  <nts id="Seg_1003" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_1005" n="HIAT:w" s="T263">u͡ot</ts>
                  <nts id="Seg_1006" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_1008" n="HIAT:w" s="T264">ulluŋnaːk</ts>
                  <nts id="Seg_1009" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_1011" n="HIAT:w" s="T265">üktenelik</ts>
                  <nts id="Seg_1012" n="HIAT:ip">.</nts>
                  <nts id="Seg_1013" n="HIAT:ip">"</nts>
                  <nts id="Seg_1014" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T268" id="Seg_1016" n="HIAT:u" s="T266">
                  <ts e="T267" id="Seg_1018" n="HIAT:w" s="T266">U͡ol</ts>
                  <nts id="Seg_1019" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_1021" n="HIAT:w" s="T267">kepseːbit</ts>
                  <nts id="Seg_1022" n="HIAT:ip">:</nts>
                  <nts id="Seg_1023" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T275" id="Seg_1025" n="HIAT:u" s="T268">
                  <nts id="Seg_1026" n="HIAT:ip">"</nts>
                  <ts e="T269" id="Seg_1028" n="HIAT:w" s="T268">Edʼiːjim</ts>
                  <nts id="Seg_1029" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_1031" n="HIAT:w" s="T269">toŋon</ts>
                  <nts id="Seg_1032" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_1034" n="HIAT:w" s="T270">öllö</ts>
                  <nts id="Seg_1035" n="HIAT:ip">,</nts>
                  <nts id="Seg_1036" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_1038" n="HIAT:w" s="T271">tabalarbɨt</ts>
                  <nts id="Seg_1039" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_1041" n="HIAT:w" s="T272">emi͡e</ts>
                  <nts id="Seg_1042" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_1044" n="HIAT:w" s="T273">toŋon</ts>
                  <nts id="Seg_1045" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_1047" n="HIAT:w" s="T274">öllüler</ts>
                  <nts id="Seg_1048" n="HIAT:ip">.</nts>
                  <nts id="Seg_1049" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T279" id="Seg_1051" n="HIAT:u" s="T275">
                  <ts e="T276" id="Seg_1053" n="HIAT:w" s="T275">Kaːr</ts>
                  <nts id="Seg_1054" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_1056" n="HIAT:w" s="T276">ihiger</ts>
                  <nts id="Seg_1057" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_1059" n="HIAT:w" s="T277">hɨtallar</ts>
                  <nts id="Seg_1060" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_1062" n="HIAT:w" s="T278">tibille</ts>
                  <nts id="Seg_1063" n="HIAT:ip">.</nts>
                  <nts id="Seg_1064" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T283" id="Seg_1066" n="HIAT:u" s="T279">
                  <ts e="T280" id="Seg_1068" n="HIAT:w" s="T279">Kajtak</ts>
                  <nts id="Seg_1069" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_1071" n="HIAT:w" s="T280">kihi</ts>
                  <nts id="Seg_1072" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_1074" n="HIAT:w" s="T281">bu͡olu͡om</ts>
                  <nts id="Seg_1075" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_1077" n="HIAT:w" s="T282">dʼürü</ts>
                  <nts id="Seg_1078" n="HIAT:ip">?</nts>
                  <nts id="Seg_1079" n="HIAT:ip">"</nts>
                  <nts id="Seg_1080" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T290" id="Seg_1082" n="HIAT:u" s="T283">
                  <nts id="Seg_1083" n="HIAT:ip">"</nts>
                  <ts e="T284" id="Seg_1085" n="HIAT:w" s="T283">Maŋnaj</ts>
                  <nts id="Seg_1086" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_1088" n="HIAT:w" s="T284">ɨ͡aldʼɨt</ts>
                  <nts id="Seg_1089" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_1091" n="HIAT:w" s="T285">bu͡ol</ts>
                  <nts id="Seg_1092" n="HIAT:ip">"</nts>
                  <nts id="Seg_1093" n="HIAT:ip">,</nts>
                  <nts id="Seg_1094" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_1096" n="HIAT:w" s="T286">di͡ebit</ts>
                  <nts id="Seg_1097" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_1099" n="HIAT:w" s="T287">ogonnʼor</ts>
                  <nts id="Seg_1100" n="HIAT:ip">,</nts>
                  <nts id="Seg_1101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1102" n="HIAT:ip">"</nts>
                  <ts e="T289" id="Seg_1104" n="HIAT:w" s="T288">onton</ts>
                  <nts id="Seg_1105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_1107" n="HIAT:w" s="T289">hübelehi͡ekpit</ts>
                  <nts id="Seg_1108" n="HIAT:ip">.</nts>
                  <nts id="Seg_1109" n="HIAT:ip">"</nts>
                  <nts id="Seg_1110" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T294" id="Seg_1112" n="HIAT:u" s="T290">
                  <ts e="T291" id="Seg_1114" n="HIAT:w" s="T290">U͡ola</ts>
                  <nts id="Seg_1115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_1117" n="HIAT:w" s="T291">taksan</ts>
                  <nts id="Seg_1118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_1120" n="HIAT:w" s="T292">tɨhɨnɨ</ts>
                  <nts id="Seg_1121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_1123" n="HIAT:w" s="T293">ölörbüt</ts>
                  <nts id="Seg_1124" n="HIAT:ip">.</nts>
                  <nts id="Seg_1125" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T296" id="Seg_1127" n="HIAT:u" s="T294">
                  <ts e="T295" id="Seg_1129" n="HIAT:w" s="T294">Tastaːk</ts>
                  <nts id="Seg_1130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1132" n="HIAT:w" s="T295">tɨhɨnɨ</ts>
                  <nts id="Seg_1133" n="HIAT:ip">.</nts>
                  <nts id="Seg_1134" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T300" id="Seg_1136" n="HIAT:u" s="T296">
                  <ts e="T297" id="Seg_1138" n="HIAT:w" s="T296">ɨ͡aldʼɨt</ts>
                  <nts id="Seg_1139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_1141" n="HIAT:w" s="T297">onu</ts>
                  <nts id="Seg_1142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_1144" n="HIAT:w" s="T298">hogotogun</ts>
                  <nts id="Seg_1145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_1147" n="HIAT:w" s="T299">hi͡ebit</ts>
                  <nts id="Seg_1148" n="HIAT:ip">.</nts>
                  <nts id="Seg_1149" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T302" id="Seg_1151" n="HIAT:u" s="T300">
                  <nts id="Seg_1152" n="HIAT:ip">"</nts>
                  <ts e="T301" id="Seg_1154" n="HIAT:w" s="T300">Tottuŋ</ts>
                  <nts id="Seg_1155" n="HIAT:ip">"</nts>
                  <nts id="Seg_1156" n="HIAT:ip">,</nts>
                  <nts id="Seg_1157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_1159" n="HIAT:w" s="T301">ɨjɨppɨttar</ts>
                  <nts id="Seg_1160" n="HIAT:ip">.</nts>
                  <nts id="Seg_1161" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T306" id="Seg_1163" n="HIAT:u" s="T302">
                  <nts id="Seg_1164" n="HIAT:ip">"</nts>
                  <ts e="T303" id="Seg_1166" n="HIAT:w" s="T302">Tottum</ts>
                  <nts id="Seg_1167" n="HIAT:ip">,</nts>
                  <nts id="Seg_1168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_1170" n="HIAT:w" s="T303">kühüŋŋütten</ts>
                  <nts id="Seg_1171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_1173" n="HIAT:w" s="T304">ahɨː</ts>
                  <nts id="Seg_1174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_1176" n="HIAT:w" s="T305">ilikpin</ts>
                  <nts id="Seg_1177" n="HIAT:ip">.</nts>
                  <nts id="Seg_1178" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T308" id="Seg_1180" n="HIAT:u" s="T306">
                  <ts e="T307" id="Seg_1182" n="HIAT:w" s="T306">Anɨ</ts>
                  <nts id="Seg_1183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_1185" n="HIAT:w" s="T307">hɨnnʼanɨ͡am</ts>
                  <nts id="Seg_1186" n="HIAT:ip">.</nts>
                  <nts id="Seg_1187" n="HIAT:ip">"</nts>
                  <nts id="Seg_1188" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T312" id="Seg_1190" n="HIAT:u" s="T308">
                  <ts e="T309" id="Seg_1192" n="HIAT:w" s="T308">Üs</ts>
                  <nts id="Seg_1193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_1195" n="HIAT:w" s="T309">künü</ts>
                  <nts id="Seg_1196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_1198" n="HIAT:w" s="T310">u͡ol</ts>
                  <nts id="Seg_1199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_1201" n="HIAT:w" s="T311">utujbut</ts>
                  <nts id="Seg_1202" n="HIAT:ip">.</nts>
                  <nts id="Seg_1203" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T319" id="Seg_1205" n="HIAT:u" s="T312">
                  <ts e="T313" id="Seg_1207" n="HIAT:w" s="T312">Uhuktubutugar</ts>
                  <nts id="Seg_1208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_1210" n="HIAT:w" s="T313">ogonnʼor</ts>
                  <nts id="Seg_1211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_1213" n="HIAT:w" s="T314">muŋ</ts>
                  <nts id="Seg_1214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1216" n="HIAT:w" s="T315">emis</ts>
                  <nts id="Seg_1217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_1219" n="HIAT:w" s="T316">tabanɨ</ts>
                  <nts id="Seg_1220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_1222" n="HIAT:w" s="T317">ölörön</ts>
                  <nts id="Seg_1223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1225" n="HIAT:w" s="T318">hi͡ekpit</ts>
                  <nts id="Seg_1226" n="HIAT:ip">.</nts>
                  <nts id="Seg_1227" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T325" id="Seg_1229" n="HIAT:u" s="T319">
                  <ts e="T320" id="Seg_1231" n="HIAT:w" s="T319">Ogonnʼor</ts>
                  <nts id="Seg_1232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1234" n="HIAT:w" s="T320">üs</ts>
                  <nts id="Seg_1235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_1237" n="HIAT:w" s="T321">ɨjdaːgɨ</ts>
                  <nts id="Seg_1238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1240" n="HIAT:w" s="T322">ahɨ</ts>
                  <nts id="Seg_1241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_1243" n="HIAT:w" s="T323">belemneːbit</ts>
                  <nts id="Seg_1244" n="HIAT:ip">,</nts>
                  <nts id="Seg_1245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1247" n="HIAT:w" s="T324">barɨ͡agɨn</ts>
                  <nts id="Seg_1248" n="HIAT:ip">.</nts>
                  <nts id="Seg_1249" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T327" id="Seg_1251" n="HIAT:u" s="T325">
                  <ts e="T326" id="Seg_1253" n="HIAT:w" s="T325">U͡ol</ts>
                  <nts id="Seg_1254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1256" n="HIAT:w" s="T326">di͡ebit</ts>
                  <nts id="Seg_1257" n="HIAT:ip">:</nts>
                  <nts id="Seg_1258" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T335" id="Seg_1260" n="HIAT:u" s="T327">
                  <nts id="Seg_1261" n="HIAT:ip">"</nts>
                  <ts e="T328" id="Seg_1263" n="HIAT:w" s="T327">Kanna</ts>
                  <nts id="Seg_1264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_1266" n="HIAT:w" s="T328">ire</ts>
                  <nts id="Seg_1267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_1269" n="HIAT:w" s="T329">hir</ts>
                  <nts id="Seg_1270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_1272" n="HIAT:w" s="T330">iččite</ts>
                  <nts id="Seg_1273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_1275" n="HIAT:w" s="T331">baːr</ts>
                  <nts id="Seg_1276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_1278" n="HIAT:w" s="T332">ühü</ts>
                  <nts id="Seg_1279" n="HIAT:ip">,</nts>
                  <nts id="Seg_1280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_1282" n="HIAT:w" s="T333">onno</ts>
                  <nts id="Seg_1283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_1285" n="HIAT:w" s="T334">barɨ͡am</ts>
                  <nts id="Seg_1286" n="HIAT:ip">.</nts>
                  <nts id="Seg_1287" n="HIAT:ip">"</nts>
                  <nts id="Seg_1288" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T340" id="Seg_1290" n="HIAT:u" s="T335">
                  <ts e="T336" id="Seg_1292" n="HIAT:w" s="T335">U͡ol</ts>
                  <nts id="Seg_1293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_1295" n="HIAT:w" s="T336">hɨrgatɨgar</ts>
                  <nts id="Seg_1296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_1298" n="HIAT:w" s="T337">taːs</ts>
                  <nts id="Seg_1299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_1301" n="HIAT:w" s="T338">hüregi</ts>
                  <nts id="Seg_1302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_1304" n="HIAT:w" s="T339">uːrda</ts>
                  <nts id="Seg_1305" n="HIAT:ip">.</nts>
                  <nts id="Seg_1306" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T344" id="Seg_1308" n="HIAT:u" s="T340">
                  <ts e="T341" id="Seg_1310" n="HIAT:w" s="T340">Ürek</ts>
                  <nts id="Seg_1311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_1313" n="HIAT:w" s="T341">ihinen</ts>
                  <nts id="Seg_1314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1316" n="HIAT:w" s="T342">kamnaːn</ts>
                  <nts id="Seg_1317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_1319" n="HIAT:w" s="T343">kaːlla</ts>
                  <nts id="Seg_1320" n="HIAT:ip">.</nts>
                  <nts id="Seg_1321" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T348" id="Seg_1323" n="HIAT:u" s="T344">
                  <ts e="T345" id="Seg_1325" n="HIAT:w" s="T344">Töhö</ts>
                  <nts id="Seg_1326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_1328" n="HIAT:w" s="T345">ördük</ts>
                  <nts id="Seg_1329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_1331" n="HIAT:w" s="T346">barbɨta</ts>
                  <nts id="Seg_1332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1334" n="HIAT:w" s="T347">dʼürü</ts>
                  <nts id="Seg_1335" n="HIAT:ip">?</nts>
                  <nts id="Seg_1336" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T353" id="Seg_1338" n="HIAT:u" s="T348">
                  <ts e="T349" id="Seg_1340" n="HIAT:w" s="T348">Araj</ts>
                  <nts id="Seg_1341" n="HIAT:ip">,</nts>
                  <nts id="Seg_1342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_1344" n="HIAT:w" s="T349">čuguːn</ts>
                  <nts id="Seg_1345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_1347" n="HIAT:w" s="T350">uraha</ts>
                  <nts id="Seg_1348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_1350" n="HIAT:w" s="T351">dʼi͡eni</ts>
                  <nts id="Seg_1351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_1353" n="HIAT:w" s="T352">körbüt</ts>
                  <nts id="Seg_1354" n="HIAT:ip">.</nts>
                  <nts id="Seg_1355" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T363" id="Seg_1357" n="HIAT:u" s="T353">
                  <ts e="T354" id="Seg_1359" n="HIAT:w" s="T353">Aːnɨ</ts>
                  <nts id="Seg_1360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_1362" n="HIAT:w" s="T354">arɨjan</ts>
                  <nts id="Seg_1363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_1365" n="HIAT:w" s="T355">kördö</ts>
                  <nts id="Seg_1366" n="HIAT:ip">,</nts>
                  <nts id="Seg_1367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_1369" n="HIAT:w" s="T356">kɨrdʼagas</ts>
                  <nts id="Seg_1370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_1372" n="HIAT:w" s="T357">bagajɨ</ts>
                  <nts id="Seg_1373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_1375" n="HIAT:w" s="T358">emeːksin</ts>
                  <nts id="Seg_1376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_1378" n="HIAT:w" s="T359">oloror</ts>
                  <nts id="Seg_1379" n="HIAT:ip">,</nts>
                  <nts id="Seg_1380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_1382" n="HIAT:w" s="T360">attɨtɨgar</ts>
                  <nts id="Seg_1383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_1385" n="HIAT:w" s="T361">kü͡ört</ts>
                  <nts id="Seg_1386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_1388" n="HIAT:w" s="T362">hɨtar</ts>
                  <nts id="Seg_1389" n="HIAT:ip">.</nts>
                  <nts id="Seg_1390" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T367" id="Seg_1392" n="HIAT:u" s="T363">
                  <ts e="T364" id="Seg_1394" n="HIAT:w" s="T363">Menʼiːtin</ts>
                  <nts id="Seg_1395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_1397" n="HIAT:w" s="T364">ürdütüger</ts>
                  <nts id="Seg_1398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_1400" n="HIAT:w" s="T365">mataga</ts>
                  <nts id="Seg_1401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_1403" n="HIAT:w" s="T366">ɨjaːllɨbɨt</ts>
                  <nts id="Seg_1404" n="HIAT:ip">.</nts>
                  <nts id="Seg_1405" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T371" id="Seg_1407" n="HIAT:u" s="T367">
                  <ts e="T368" id="Seg_1409" n="HIAT:w" s="T367">Mataga</ts>
                  <nts id="Seg_1410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_1412" n="HIAT:w" s="T368">kihi</ts>
                  <nts id="Seg_1413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_1415" n="HIAT:w" s="T369">hɨrajɨn</ts>
                  <nts id="Seg_1416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_1418" n="HIAT:w" s="T370">majgɨlaːk</ts>
                  <nts id="Seg_1419" n="HIAT:ip">.</nts>
                  <nts id="Seg_1420" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T374" id="Seg_1422" n="HIAT:u" s="T371">
                  <ts e="T372" id="Seg_1424" n="HIAT:w" s="T371">Ačaːk</ts>
                  <nts id="Seg_1425" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_1427" n="HIAT:w" s="T372">annɨta</ts>
                  <nts id="Seg_1428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_1430" n="HIAT:w" s="T373">köŋdöj</ts>
                  <nts id="Seg_1431" n="HIAT:ip">.</nts>
                  <nts id="Seg_1432" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T377" id="Seg_1434" n="HIAT:u" s="T374">
                  <ts e="T375" id="Seg_1436" n="HIAT:w" s="T374">Ürek</ts>
                  <nts id="Seg_1437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_1439" n="HIAT:w" s="T375">usta</ts>
                  <nts id="Seg_1440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_1442" n="HIAT:w" s="T376">hɨtar</ts>
                  <nts id="Seg_1443" n="HIAT:ip">.</nts>
                  <nts id="Seg_1444" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T381" id="Seg_1446" n="HIAT:u" s="T377">
                  <ts e="T378" id="Seg_1448" n="HIAT:w" s="T377">Ürekke</ts>
                  <nts id="Seg_1449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_1451" n="HIAT:w" s="T378">tɨːlaːk</ts>
                  <nts id="Seg_1452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_1454" n="HIAT:w" s="T379">ilimnene</ts>
                  <nts id="Seg_1455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_1457" n="HIAT:w" s="T380">hɨldʼar</ts>
                  <nts id="Seg_1458" n="HIAT:ip">.</nts>
                  <nts id="Seg_1459" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T384" id="Seg_1461" n="HIAT:u" s="T381">
                  <ts e="T382" id="Seg_1463" n="HIAT:w" s="T381">Mataga</ts>
                  <nts id="Seg_1464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_1466" n="HIAT:w" s="T382">haŋalaːk</ts>
                  <nts id="Seg_1467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_1469" n="HIAT:w" s="T383">bu͡olbut</ts>
                  <nts id="Seg_1470" n="HIAT:ip">:</nts>
                  <nts id="Seg_1471" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T390" id="Seg_1473" n="HIAT:u" s="T384">
                  <nts id="Seg_1474" n="HIAT:ip">"</nts>
                  <ts e="T385" id="Seg_1476" n="HIAT:w" s="T384">ɨ͡aldʼɨt</ts>
                  <nts id="Seg_1477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T386" id="Seg_1479" n="HIAT:w" s="T385">kelle</ts>
                  <nts id="Seg_1480" n="HIAT:ip">,</nts>
                  <nts id="Seg_1481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_1483" n="HIAT:w" s="T386">maččɨttar</ts>
                  <nts id="Seg_1484" n="HIAT:ip">,</nts>
                  <nts id="Seg_1485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_1487" n="HIAT:w" s="T387">mastaːŋ</ts>
                  <nts id="Seg_1488" n="HIAT:ip">,</nts>
                  <nts id="Seg_1489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_1491" n="HIAT:w" s="T388">u͡otta</ts>
                  <nts id="Seg_1492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_1494" n="HIAT:w" s="T389">ottuŋ</ts>
                  <nts id="Seg_1495" n="HIAT:ip">!</nts>
                  <nts id="Seg_1496" n="HIAT:ip">"</nts>
                  <nts id="Seg_1497" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T394" id="Seg_1499" n="HIAT:u" s="T390">
                  <ts e="T391" id="Seg_1501" n="HIAT:w" s="T390">Mastara</ts>
                  <nts id="Seg_1502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_1504" n="HIAT:w" s="T391">bejelere</ts>
                  <nts id="Seg_1505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T393" id="Seg_1507" n="HIAT:w" s="T392">kiːrdiler</ts>
                  <nts id="Seg_1508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_1510" n="HIAT:w" s="T393">tahaːrattan</ts>
                  <nts id="Seg_1511" n="HIAT:ip">.</nts>
                  <nts id="Seg_1512" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T396" id="Seg_1514" n="HIAT:u" s="T394">
                  <ts e="T395" id="Seg_1516" n="HIAT:w" s="T394">U͡ol</ts>
                  <nts id="Seg_1517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T396" id="Seg_1519" n="HIAT:w" s="T395">tu͡olkulaːta</ts>
                  <nts id="Seg_1520" n="HIAT:ip">:</nts>
                  <nts id="Seg_1521" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T399" id="Seg_1523" n="HIAT:u" s="T396">
                  <nts id="Seg_1524" n="HIAT:ip">"</nts>
                  <ts e="T397" id="Seg_1526" n="HIAT:w" s="T396">Timir</ts>
                  <nts id="Seg_1527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_1529" n="HIAT:w" s="T397">mastar</ts>
                  <nts id="Seg_1530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_1532" n="HIAT:w" s="T398">ebit</ts>
                  <nts id="Seg_1533" n="HIAT:ip">.</nts>
                  <nts id="Seg_1534" n="HIAT:ip">"</nts>
                  <nts id="Seg_1535" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T404" id="Seg_1537" n="HIAT:u" s="T399">
                  <ts e="T400" id="Seg_1539" n="HIAT:w" s="T399">Emeːksin</ts>
                  <nts id="Seg_1540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_1542" n="HIAT:w" s="T400">turan</ts>
                  <nts id="Seg_1543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T402" id="Seg_1545" n="HIAT:w" s="T401">mastarɨ</ts>
                  <nts id="Seg_1546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_1548" n="HIAT:w" s="T402">ačaːgɨgar</ts>
                  <nts id="Seg_1549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T404" id="Seg_1551" n="HIAT:w" s="T403">musta</ts>
                  <nts id="Seg_1552" n="HIAT:ip">.</nts>
                  <nts id="Seg_1553" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T406" id="Seg_1555" n="HIAT:u" s="T404">
                  <ts e="T405" id="Seg_1557" n="HIAT:w" s="T404">Mataga</ts>
                  <nts id="Seg_1558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_1560" n="HIAT:w" s="T405">di͡ete</ts>
                  <nts id="Seg_1561" n="HIAT:ip">:</nts>
                  <nts id="Seg_1562" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T408" id="Seg_1564" n="HIAT:u" s="T406">
                  <nts id="Seg_1565" n="HIAT:ip">"</nts>
                  <ts e="T407" id="Seg_1567" n="HIAT:w" s="T406">Kü͡ört</ts>
                  <nts id="Seg_1568" n="HIAT:ip">,</nts>
                  <nts id="Seg_1569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T408" id="Seg_1571" n="HIAT:w" s="T407">kü͡örteː</ts>
                  <nts id="Seg_1572" n="HIAT:ip">!</nts>
                  <nts id="Seg_1573" n="HIAT:ip">"</nts>
                  <nts id="Seg_1574" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T414" id="Seg_1576" n="HIAT:u" s="T408">
                  <ts e="T409" id="Seg_1578" n="HIAT:w" s="T408">Uraga</ts>
                  <nts id="Seg_1579" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_1581" n="HIAT:w" s="T409">dʼi͡e</ts>
                  <nts id="Seg_1582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T411" id="Seg_1584" n="HIAT:w" s="T410">timir</ts>
                  <nts id="Seg_1585" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T412" id="Seg_1587" n="HIAT:w" s="T411">mu͡ostata</ts>
                  <nts id="Seg_1588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T413" id="Seg_1590" n="HIAT:w" s="T412">kɨtarčɨ</ts>
                  <nts id="Seg_1591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414" id="Seg_1593" n="HIAT:w" s="T413">barda</ts>
                  <nts id="Seg_1594" n="HIAT:ip">.</nts>
                  <nts id="Seg_1595" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T417" id="Seg_1597" n="HIAT:u" s="T414">
                  <ts e="T415" id="Seg_1599" n="HIAT:w" s="T414">Mataga</ts>
                  <nts id="Seg_1600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T416" id="Seg_1602" n="HIAT:w" s="T415">bi͡ek</ts>
                  <nts id="Seg_1603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T417" id="Seg_1605" n="HIAT:w" s="T416">diːr</ts>
                  <nts id="Seg_1606" n="HIAT:ip">:</nts>
                  <nts id="Seg_1607" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T424" id="Seg_1609" n="HIAT:u" s="T417">
                  <nts id="Seg_1610" n="HIAT:ip">"</nts>
                  <ts e="T418" id="Seg_1612" n="HIAT:w" s="T417">Kü͡ört</ts>
                  <nts id="Seg_1613" n="HIAT:ip">,</nts>
                  <nts id="Seg_1614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T419" id="Seg_1616" n="HIAT:w" s="T418">kü͡örteː</ts>
                  <nts id="Seg_1617" n="HIAT:ip">,</nts>
                  <nts id="Seg_1618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T420" id="Seg_1620" n="HIAT:w" s="T419">kü͡örteː</ts>
                  <nts id="Seg_1621" n="HIAT:ip">,</nts>
                  <nts id="Seg_1622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_1624" n="HIAT:w" s="T420">ɨ͡aldʼɨt</ts>
                  <nts id="Seg_1625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T422" id="Seg_1627" n="HIAT:w" s="T421">kelle</ts>
                  <nts id="Seg_1628" n="HIAT:ip">,</nts>
                  <nts id="Seg_1629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T423" id="Seg_1631" n="HIAT:w" s="T422">u͡otta</ts>
                  <nts id="Seg_1632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T424" id="Seg_1634" n="HIAT:w" s="T423">ep</ts>
                  <nts id="Seg_1635" n="HIAT:ip">!</nts>
                  <nts id="Seg_1636" n="HIAT:ip">"</nts>
                  <nts id="Seg_1637" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T428" id="Seg_1639" n="HIAT:u" s="T424">
                  <ts e="T425" id="Seg_1641" n="HIAT:w" s="T424">Uraha</ts>
                  <nts id="Seg_1642" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T426" id="Seg_1644" n="HIAT:w" s="T425">dʼi͡e</ts>
                  <nts id="Seg_1645" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T427" id="Seg_1647" n="HIAT:w" s="T426">barɨta</ts>
                  <nts id="Seg_1648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T428" id="Seg_1650" n="HIAT:w" s="T427">kɨtarda</ts>
                  <nts id="Seg_1651" n="HIAT:ip">.</nts>
                  <nts id="Seg_1652" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T432" id="Seg_1654" n="HIAT:u" s="T428">
                  <ts e="T429" id="Seg_1656" n="HIAT:w" s="T428">Emeːksin</ts>
                  <nts id="Seg_1657" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_1659" n="HIAT:w" s="T429">ɨ͡aldʼɨt</ts>
                  <nts id="Seg_1660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T431" id="Seg_1662" n="HIAT:w" s="T430">u͡olu</ts>
                  <nts id="Seg_1663" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_1665" n="HIAT:w" s="T431">kördöstö</ts>
                  <nts id="Seg_1666" n="HIAT:ip">:</nts>
                  <nts id="Seg_1667" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T436" id="Seg_1669" n="HIAT:u" s="T432">
                  <nts id="Seg_1670" n="HIAT:ip">"</nts>
                  <ts e="T433" id="Seg_1672" n="HIAT:w" s="T432">Ititime</ts>
                  <nts id="Seg_1673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T434" id="Seg_1675" n="HIAT:w" s="T433">olus</ts>
                  <nts id="Seg_1676" n="HIAT:ip">,</nts>
                  <nts id="Seg_1677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T435" id="Seg_1679" n="HIAT:w" s="T434">hojut</ts>
                  <nts id="Seg_1680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_1682" n="HIAT:w" s="T435">dʼe</ts>
                  <nts id="Seg_1683" n="HIAT:ip">.</nts>
                  <nts id="Seg_1684" n="HIAT:ip">"</nts>
                  <nts id="Seg_1685" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T443" id="Seg_1687" n="HIAT:u" s="T436">
                  <ts e="T437" id="Seg_1689" n="HIAT:w" s="T436">U͡ol</ts>
                  <nts id="Seg_1690" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T438" id="Seg_1692" n="HIAT:w" s="T437">taːs</ts>
                  <nts id="Seg_1693" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T439" id="Seg_1695" n="HIAT:w" s="T438">hüregi</ts>
                  <nts id="Seg_1696" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440" id="Seg_1698" n="HIAT:w" s="T439">ɨlan</ts>
                  <nts id="Seg_1699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_1701" n="HIAT:w" s="T440">ačaːk</ts>
                  <nts id="Seg_1702" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T442" id="Seg_1704" n="HIAT:w" s="T441">ortotugar</ts>
                  <nts id="Seg_1705" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T443" id="Seg_1707" n="HIAT:w" s="T442">bɨrakta</ts>
                  <nts id="Seg_1708" n="HIAT:ip">.</nts>
                  <nts id="Seg_1709" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T446" id="Seg_1711" n="HIAT:u" s="T443">
                  <ts e="T444" id="Seg_1713" n="HIAT:w" s="T443">Uraha</ts>
                  <nts id="Seg_1714" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T445" id="Seg_1716" n="HIAT:w" s="T444">dʼi͡e</ts>
                  <nts id="Seg_1717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T446" id="Seg_1719" n="HIAT:w" s="T445">hojdo</ts>
                  <nts id="Seg_1720" n="HIAT:ip">.</nts>
                  <nts id="Seg_1721" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T448" id="Seg_1723" n="HIAT:u" s="T446">
                  <ts e="T447" id="Seg_1725" n="HIAT:w" s="T446">Mu͡ostata</ts>
                  <nts id="Seg_1726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T448" id="Seg_1728" n="HIAT:w" s="T447">kɨtarbata</ts>
                  <nts id="Seg_1729" n="HIAT:ip">.</nts>
                  <nts id="Seg_1730" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T453" id="Seg_1732" n="HIAT:u" s="T448">
                  <ts e="T449" id="Seg_1734" n="HIAT:w" s="T448">Emeːksin</ts>
                  <nts id="Seg_1735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T450" id="Seg_1737" n="HIAT:w" s="T449">emi͡e</ts>
                  <nts id="Seg_1738" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T451" id="Seg_1740" n="HIAT:w" s="T450">kam</ts>
                  <nts id="Seg_1741" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T452" id="Seg_1743" n="HIAT:w" s="T451">kɨrɨ͡a</ts>
                  <nts id="Seg_1744" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T453" id="Seg_1746" n="HIAT:w" s="T452">bu͡olla</ts>
                  <nts id="Seg_1747" n="HIAT:ip">.</nts>
                  <nts id="Seg_1748" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T457" id="Seg_1750" n="HIAT:u" s="T453">
                  <nts id="Seg_1751" n="HIAT:ip">"</nts>
                  <ts e="T454" id="Seg_1753" n="HIAT:w" s="T453">Olus</ts>
                  <nts id="Seg_1754" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T455" id="Seg_1756" n="HIAT:w" s="T454">toŋoruma</ts>
                  <nts id="Seg_1757" n="HIAT:ip">"</nts>
                  <nts id="Seg_1758" n="HIAT:ip">,</nts>
                  <nts id="Seg_1759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T456" id="Seg_1761" n="HIAT:w" s="T455">kördöspüt</ts>
                  <nts id="Seg_1762" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T0" id="Seg_1764" n="HIAT:w" s="T456">mataga</ts>
                  <nts id="Seg_1765" n="HIAT:ip">,</nts>
                  <nts id="Seg_1766" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1767" n="HIAT:ip">"</nts>
                  <ts e="T947" id="Seg_1769" n="HIAT:w" s="T0">barɨ</ts>
                  <nts id="Seg_1770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T948" id="Seg_1772" n="HIAT:w" s="T947">hanaːgɨn</ts>
                  <nts id="Seg_1773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T457" id="Seg_1775" n="HIAT:w" s="T948">toloru͡om</ts>
                  <nts id="Seg_1776" n="HIAT:ip">.</nts>
                  <nts id="Seg_1777" n="HIAT:ip">"</nts>
                  <nts id="Seg_1778" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T461" id="Seg_1780" n="HIAT:u" s="T457">
                  <ts e="T458" id="Seg_1782" n="HIAT:w" s="T457">Mataga</ts>
                  <nts id="Seg_1783" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T459" id="Seg_1785" n="HIAT:w" s="T458">ebit</ts>
                  <nts id="Seg_1786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T460" id="Seg_1788" n="HIAT:w" s="T459">hir</ts>
                  <nts id="Seg_1789" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T461" id="Seg_1791" n="HIAT:w" s="T460">iččite</ts>
                  <nts id="Seg_1792" n="HIAT:ip">.</nts>
                  <nts id="Seg_1793" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T468" id="Seg_1795" n="HIAT:u" s="T461">
                  <ts e="T462" id="Seg_1797" n="HIAT:w" s="T461">U͡ol</ts>
                  <nts id="Seg_1798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T463" id="Seg_1800" n="HIAT:w" s="T462">taːs</ts>
                  <nts id="Seg_1801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T464" id="Seg_1803" n="HIAT:w" s="T463">hüregi</ts>
                  <nts id="Seg_1804" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T465" id="Seg_1806" n="HIAT:w" s="T464">ačaːk</ts>
                  <nts id="Seg_1807" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T466" id="Seg_1809" n="HIAT:w" s="T465">ihitten</ts>
                  <nts id="Seg_1810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T467" id="Seg_1812" n="HIAT:w" s="T466">oroːn</ts>
                  <nts id="Seg_1813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T468" id="Seg_1815" n="HIAT:w" s="T467">ɨlla</ts>
                  <nts id="Seg_1816" n="HIAT:ip">.</nts>
                  <nts id="Seg_1817" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T471" id="Seg_1819" n="HIAT:u" s="T468">
                  <ts e="T469" id="Seg_1821" n="HIAT:w" s="T468">U͡ota</ts>
                  <nts id="Seg_1822" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T470" id="Seg_1824" n="HIAT:w" s="T469">emi͡e</ts>
                  <nts id="Seg_1825" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T471" id="Seg_1827" n="HIAT:w" s="T470">gɨdaːrɨjda</ts>
                  <nts id="Seg_1828" n="HIAT:ip">.</nts>
                  <nts id="Seg_1829" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T474" id="Seg_1831" n="HIAT:u" s="T471">
                  <ts e="T472" id="Seg_1833" n="HIAT:w" s="T471">Mataga</ts>
                  <nts id="Seg_1834" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T473" id="Seg_1836" n="HIAT:w" s="T472">di͡ete</ts>
                  <nts id="Seg_1837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T474" id="Seg_1839" n="HIAT:w" s="T473">u͡olga</ts>
                  <nts id="Seg_1840" n="HIAT:ip">:</nts>
                  <nts id="Seg_1841" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T480" id="Seg_1843" n="HIAT:u" s="T474">
                  <nts id="Seg_1844" n="HIAT:ip">"</nts>
                  <ts e="T475" id="Seg_1846" n="HIAT:w" s="T474">Tahaːra</ts>
                  <nts id="Seg_1847" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T476" id="Seg_1849" n="HIAT:w" s="T475">tagɨs</ts>
                  <nts id="Seg_1850" n="HIAT:ip">,</nts>
                  <nts id="Seg_1851" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T477" id="Seg_1853" n="HIAT:w" s="T476">onno</ts>
                  <nts id="Seg_1854" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T478" id="Seg_1856" n="HIAT:w" s="T477">hɨrgalaːk</ts>
                  <nts id="Seg_1857" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T479" id="Seg_1859" n="HIAT:w" s="T478">taba</ts>
                  <nts id="Seg_1860" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T480" id="Seg_1862" n="HIAT:w" s="T479">turar</ts>
                  <nts id="Seg_1863" n="HIAT:ip">.</nts>
                  <nts id="Seg_1864" n="HIAT:ip">"</nts>
                  <nts id="Seg_1865" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T488" id="Seg_1867" n="HIAT:u" s="T480">
                  <ts e="T481" id="Seg_1869" n="HIAT:w" s="T480">U͡ol</ts>
                  <nts id="Seg_1870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T482" id="Seg_1872" n="HIAT:w" s="T481">tahaːra</ts>
                  <nts id="Seg_1873" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T483" id="Seg_1875" n="HIAT:w" s="T482">taksan</ts>
                  <nts id="Seg_1876" n="HIAT:ip">,</nts>
                  <nts id="Seg_1877" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T484" id="Seg_1879" n="HIAT:w" s="T483">tu͡ok</ts>
                  <nts id="Seg_1880" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T485" id="Seg_1882" n="HIAT:w" s="T484">da</ts>
                  <nts id="Seg_1883" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T486" id="Seg_1885" n="HIAT:w" s="T485">hɨrgalaːk</ts>
                  <nts id="Seg_1886" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T487" id="Seg_1888" n="HIAT:w" s="T486">tabatɨn</ts>
                  <nts id="Seg_1889" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T488" id="Seg_1891" n="HIAT:w" s="T487">körböt</ts>
                  <nts id="Seg_1892" n="HIAT:ip">.</nts>
                  <nts id="Seg_1893" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T495" id="Seg_1895" n="HIAT:u" s="T488">
                  <ts e="T489" id="Seg_1897" n="HIAT:w" s="T488">Kanna</ts>
                  <nts id="Seg_1898" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T490" id="Seg_1900" n="HIAT:w" s="T489">ere</ts>
                  <nts id="Seg_1901" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T491" id="Seg_1903" n="HIAT:w" s="T490">ču͡oraːn</ts>
                  <nts id="Seg_1904" n="HIAT:ip">,</nts>
                  <nts id="Seg_1905" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T492" id="Seg_1907" n="HIAT:w" s="T491">gubaːrka</ts>
                  <nts id="Seg_1908" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T493" id="Seg_1910" n="HIAT:w" s="T492">ere</ts>
                  <nts id="Seg_1911" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T494" id="Seg_1913" n="HIAT:w" s="T493">tɨ͡aha</ts>
                  <nts id="Seg_1914" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T495" id="Seg_1916" n="HIAT:w" s="T494">ihiller</ts>
                  <nts id="Seg_1917" n="HIAT:ip">.</nts>
                  <nts id="Seg_1918" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T500" id="Seg_1920" n="HIAT:u" s="T495">
                  <ts e="T496" id="Seg_1922" n="HIAT:w" s="T495">Ketegen</ts>
                  <nts id="Seg_1923" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T497" id="Seg_1925" n="HIAT:w" s="T496">di͡ek</ts>
                  <nts id="Seg_1926" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T498" id="Seg_1928" n="HIAT:w" s="T497">mataga</ts>
                  <nts id="Seg_1929" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T499" id="Seg_1931" n="HIAT:w" s="T498">haŋata</ts>
                  <nts id="Seg_1932" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T500" id="Seg_1934" n="HIAT:w" s="T499">ihillibit</ts>
                  <nts id="Seg_1935" n="HIAT:ip">:</nts>
                  <nts id="Seg_1936" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T502" id="Seg_1938" n="HIAT:u" s="T500">
                  <nts id="Seg_1939" n="HIAT:ip">"</nts>
                  <ts e="T501" id="Seg_1941" n="HIAT:w" s="T500">Olor</ts>
                  <nts id="Seg_1942" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T502" id="Seg_1944" n="HIAT:w" s="T501">hɨrgagar</ts>
                  <nts id="Seg_1945" n="HIAT:ip">.</nts>
                  <nts id="Seg_1946" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T510" id="Seg_1948" n="HIAT:u" s="T502">
                  <ts e="T503" id="Seg_1950" n="HIAT:w" s="T502">Keŋniŋ</ts>
                  <nts id="Seg_1951" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T504" id="Seg_1953" n="HIAT:w" s="T503">di͡ek</ts>
                  <nts id="Seg_1954" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T505" id="Seg_1956" n="HIAT:w" s="T504">kanʼɨhɨma</ts>
                  <nts id="Seg_1957" n="HIAT:ip">,</nts>
                  <nts id="Seg_1958" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T506" id="Seg_1960" n="HIAT:w" s="T505">hu͡ol</ts>
                  <nts id="Seg_1961" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T507" id="Seg_1963" n="HIAT:w" s="T506">aːra</ts>
                  <nts id="Seg_1964" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T508" id="Seg_1966" n="HIAT:w" s="T507">toktoːmo</ts>
                  <nts id="Seg_1967" n="HIAT:ip">,</nts>
                  <nts id="Seg_1968" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T509" id="Seg_1970" n="HIAT:w" s="T508">tabalargɨn</ts>
                  <nts id="Seg_1971" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T510" id="Seg_1973" n="HIAT:w" s="T509">ahatɨma</ts>
                  <nts id="Seg_1974" n="HIAT:ip">.</nts>
                  <nts id="Seg_1975" n="HIAT:ip">"</nts>
                  <nts id="Seg_1976" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T518" id="Seg_1978" n="HIAT:u" s="T510">
                  <ts e="T511" id="Seg_1980" n="HIAT:w" s="T510">U͡ol</ts>
                  <nts id="Seg_1981" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T512" id="Seg_1983" n="HIAT:w" s="T511">iliːtin</ts>
                  <nts id="Seg_1984" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T513" id="Seg_1986" n="HIAT:w" s="T512">uːnna</ts>
                  <nts id="Seg_1987" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T514" id="Seg_1989" n="HIAT:w" s="T513">nʼuŋuːnu</ts>
                  <nts id="Seg_1990" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T515" id="Seg_1992" n="HIAT:w" s="T514">kapta</ts>
                  <nts id="Seg_1993" n="HIAT:ip">,</nts>
                  <nts id="Seg_1994" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T516" id="Seg_1996" n="HIAT:w" s="T515">köstübet</ts>
                  <nts id="Seg_1997" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T517" id="Seg_1999" n="HIAT:w" s="T516">hɨrgaga</ts>
                  <nts id="Seg_2000" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T518" id="Seg_2002" n="HIAT:w" s="T517">miːnne</ts>
                  <nts id="Seg_2003" n="HIAT:ip">.</nts>
                  <nts id="Seg_2004" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T521" id="Seg_2006" n="HIAT:u" s="T518">
                  <ts e="T519" id="Seg_2008" n="HIAT:w" s="T518">Tɨ͡allaːk</ts>
                  <nts id="Seg_2009" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T520" id="Seg_2011" n="HIAT:w" s="T519">purga</ts>
                  <nts id="Seg_2012" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T521" id="Seg_2014" n="HIAT:w" s="T520">ogusta</ts>
                  <nts id="Seg_2015" n="HIAT:ip">.</nts>
                  <nts id="Seg_2016" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T526" id="Seg_2018" n="HIAT:u" s="T521">
                  <ts e="T522" id="Seg_2020" n="HIAT:w" s="T521">Kaččanɨ</ts>
                  <nts id="Seg_2021" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T523" id="Seg_2023" n="HIAT:w" s="T522">barbɨta</ts>
                  <nts id="Seg_2024" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T524" id="Seg_2026" n="HIAT:w" s="T523">dʼürü</ts>
                  <nts id="Seg_2027" n="HIAT:ip">,</nts>
                  <nts id="Seg_2028" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T525" id="Seg_2030" n="HIAT:w" s="T524">kim</ts>
                  <nts id="Seg_2031" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T526" id="Seg_2033" n="HIAT:w" s="T525">bili͡ej</ts>
                  <nts id="Seg_2034" n="HIAT:ip">?</nts>
                  <nts id="Seg_2035" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T530" id="Seg_2037" n="HIAT:u" s="T526">
                  <ts e="T527" id="Seg_2039" n="HIAT:w" s="T526">Hɨrgalaːk</ts>
                  <nts id="Seg_2040" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T528" id="Seg_2042" n="HIAT:w" s="T527">tabalara</ts>
                  <nts id="Seg_2043" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T529" id="Seg_2045" n="HIAT:w" s="T528">bejelere</ts>
                  <nts id="Seg_2046" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T530" id="Seg_2048" n="HIAT:w" s="T529">illeller</ts>
                  <nts id="Seg_2049" n="HIAT:ip">.</nts>
                  <nts id="Seg_2050" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T532" id="Seg_2052" n="HIAT:u" s="T530">
                  <ts e="T531" id="Seg_2054" n="HIAT:w" s="T530">U͡ol</ts>
                  <nts id="Seg_2055" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T532" id="Seg_2057" n="HIAT:w" s="T531">tommot</ts>
                  <nts id="Seg_2058" n="HIAT:ip">.</nts>
                  <nts id="Seg_2059" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T536" id="Seg_2061" n="HIAT:u" s="T532">
                  <ts e="T533" id="Seg_2063" n="HIAT:w" s="T532">Iliːtin</ts>
                  <nts id="Seg_2064" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T534" id="Seg_2066" n="HIAT:w" s="T533">olbogun</ts>
                  <nts id="Seg_2067" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T535" id="Seg_2069" n="HIAT:w" s="T534">ihiger</ts>
                  <nts id="Seg_2070" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T536" id="Seg_2072" n="HIAT:w" s="T535">ukta</ts>
                  <nts id="Seg_2073" n="HIAT:ip">.</nts>
                  <nts id="Seg_2074" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T542" id="Seg_2076" n="HIAT:u" s="T536">
                  <ts e="T537" id="Seg_2078" n="HIAT:w" s="T536">Onno</ts>
                  <nts id="Seg_2079" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T538" id="Seg_2081" n="HIAT:w" s="T537">itiː</ts>
                  <nts id="Seg_2082" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T539" id="Seg_2084" n="HIAT:w" s="T538">timir</ts>
                  <nts id="Seg_2085" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T540" id="Seg_2087" n="HIAT:w" s="T539">mas</ts>
                  <nts id="Seg_2088" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T541" id="Seg_2090" n="HIAT:w" s="T540">hɨtar</ts>
                  <nts id="Seg_2091" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T542" id="Seg_2093" n="HIAT:w" s="T541">ebit</ts>
                  <nts id="Seg_2094" n="HIAT:ip">.</nts>
                  <nts id="Seg_2095" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T544" id="Seg_2097" n="HIAT:u" s="T542">
                  <ts e="T543" id="Seg_2099" n="HIAT:w" s="T542">Ontuta</ts>
                  <nts id="Seg_2100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T544" id="Seg_2102" n="HIAT:w" s="T543">ititer</ts>
                  <nts id="Seg_2103" n="HIAT:ip">.</nts>
                  <nts id="Seg_2104" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T554" id="Seg_2106" n="HIAT:u" s="T544">
                  <ts e="T545" id="Seg_2108" n="HIAT:w" s="T544">Ebe</ts>
                  <nts id="Seg_2109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T546" id="Seg_2111" n="HIAT:w" s="T545">ürdüger</ts>
                  <nts id="Seg_2112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T547" id="Seg_2114" n="HIAT:w" s="T546">kördö</ts>
                  <nts id="Seg_2115" n="HIAT:ip">,</nts>
                  <nts id="Seg_2116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T548" id="Seg_2118" n="HIAT:w" s="T547">hɨrgaga</ts>
                  <nts id="Seg_2119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T549" id="Seg_2121" n="HIAT:w" s="T548">ölbüt</ts>
                  <nts id="Seg_2122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T550" id="Seg_2124" n="HIAT:w" s="T549">dʼaktar</ts>
                  <nts id="Seg_2125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T551" id="Seg_2127" n="HIAT:w" s="T550">hɨtar</ts>
                  <nts id="Seg_2128" n="HIAT:ip">,</nts>
                  <nts id="Seg_2129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T552" id="Seg_2131" n="HIAT:w" s="T551">kam</ts>
                  <nts id="Seg_2132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T553" id="Seg_2134" n="HIAT:w" s="T552">tibillen</ts>
                  <nts id="Seg_2135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T554" id="Seg_2137" n="HIAT:w" s="T553">baraːn</ts>
                  <nts id="Seg_2138" n="HIAT:ip">.</nts>
                  <nts id="Seg_2139" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T556" id="Seg_2141" n="HIAT:u" s="T554">
                  <ts e="T555" id="Seg_2143" n="HIAT:w" s="T554">Tehijbekke</ts>
                  <nts id="Seg_2144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T556" id="Seg_2146" n="HIAT:w" s="T555">toktoːto</ts>
                  <nts id="Seg_2147" n="HIAT:ip">.</nts>
                  <nts id="Seg_2148" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T565" id="Seg_2150" n="HIAT:u" s="T556">
                  <ts e="T557" id="Seg_2152" n="HIAT:w" s="T556">Toktuːrun</ts>
                  <nts id="Seg_2153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T558" id="Seg_2155" n="HIAT:w" s="T557">kɨtta</ts>
                  <nts id="Seg_2156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T559" id="Seg_2158" n="HIAT:w" s="T558">nʼuŋuhuta</ts>
                  <nts id="Seg_2159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T560" id="Seg_2161" n="HIAT:w" s="T559">hulbu</ts>
                  <nts id="Seg_2162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T561" id="Seg_2164" n="HIAT:w" s="T560">ɨstanan</ts>
                  <nts id="Seg_2165" n="HIAT:ip">,</nts>
                  <nts id="Seg_2166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T562" id="Seg_2168" n="HIAT:w" s="T561">kajdi͡e</ts>
                  <nts id="Seg_2169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T563" id="Seg_2171" n="HIAT:w" s="T562">barbɨta</ts>
                  <nts id="Seg_2172" n="HIAT:ip">,</nts>
                  <nts id="Seg_2173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T564" id="Seg_2175" n="HIAT:w" s="T563">kajdi͡ek</ts>
                  <nts id="Seg_2176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T565" id="Seg_2178" n="HIAT:w" s="T564">kelbite</ts>
                  <nts id="Seg_2179" n="HIAT:ip">?</nts>
                  <nts id="Seg_2180" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T570" id="Seg_2182" n="HIAT:u" s="T565">
                  <ts e="T566" id="Seg_2184" n="HIAT:w" s="T565">Üs</ts>
                  <nts id="Seg_2185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T567" id="Seg_2187" n="HIAT:w" s="T566">koŋnomu͡oj</ts>
                  <nts id="Seg_2188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T568" id="Seg_2190" n="HIAT:w" s="T567">tabalaːk</ts>
                  <nts id="Seg_2191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T569" id="Seg_2193" n="HIAT:w" s="T568">ete</ts>
                  <nts id="Seg_2194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T570" id="Seg_2196" n="HIAT:w" s="T569">ebit</ts>
                  <nts id="Seg_2197" n="HIAT:ip">.</nts>
                  <nts id="Seg_2198" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T574" id="Seg_2200" n="HIAT:u" s="T570">
                  <ts e="T571" id="Seg_2202" n="HIAT:w" s="T570">Nʼuŋuhuta</ts>
                  <nts id="Seg_2203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T572" id="Seg_2205" n="HIAT:w" s="T571">bɨstan</ts>
                  <nts id="Seg_2206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T573" id="Seg_2208" n="HIAT:w" s="T572">baran</ts>
                  <nts id="Seg_2209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T574" id="Seg_2211" n="HIAT:w" s="T573">kaːlbɨt</ts>
                  <nts id="Seg_2212" n="HIAT:ip">.</nts>
                  <nts id="Seg_2213" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T577" id="Seg_2215" n="HIAT:u" s="T574">
                  <ts e="T575" id="Seg_2217" n="HIAT:w" s="T574">Mataga</ts>
                  <nts id="Seg_2218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T576" id="Seg_2220" n="HIAT:w" s="T575">haŋata</ts>
                  <nts id="Seg_2221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T577" id="Seg_2223" n="HIAT:w" s="T576">ihillibit</ts>
                  <nts id="Seg_2224" n="HIAT:ip">:</nts>
                  <nts id="Seg_2225" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T580" id="Seg_2227" n="HIAT:u" s="T577">
                  <nts id="Seg_2228" n="HIAT:ip">"</nts>
                  <ts e="T578" id="Seg_2230" n="HIAT:w" s="T577">Eni͡eke</ts>
                  <nts id="Seg_2231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T579" id="Seg_2233" n="HIAT:w" s="T578">di͡ebitim</ts>
                  <nts id="Seg_2234" n="HIAT:ip">,</nts>
                  <nts id="Seg_2235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T580" id="Seg_2237" n="HIAT:w" s="T579">toktoːmo</ts>
                  <nts id="Seg_2238" n="HIAT:ip">!</nts>
                  <nts id="Seg_2239" n="HIAT:ip">"</nts>
                  <nts id="Seg_2240" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T591" id="Seg_2242" n="HIAT:u" s="T580">
                  <ts e="T581" id="Seg_2244" n="HIAT:w" s="T580">U͡ol</ts>
                  <nts id="Seg_2245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T582" id="Seg_2247" n="HIAT:w" s="T581">hɨrgatɨttan</ts>
                  <nts id="Seg_2248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T583" id="Seg_2250" n="HIAT:w" s="T582">kɨhɨl</ts>
                  <nts id="Seg_2251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T584" id="Seg_2253" n="HIAT:w" s="T583">timir</ts>
                  <nts id="Seg_2254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T585" id="Seg_2256" n="HIAT:w" s="T584">mahɨ</ts>
                  <nts id="Seg_2257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T586" id="Seg_2259" n="HIAT:w" s="T585">ɨlan</ts>
                  <nts id="Seg_2260" n="HIAT:ip">,</nts>
                  <nts id="Seg_2261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T587" id="Seg_2263" n="HIAT:w" s="T586">kɨːs</ts>
                  <nts id="Seg_2264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T588" id="Seg_2266" n="HIAT:w" s="T587">hɨrgatɨn</ts>
                  <nts id="Seg_2267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T589" id="Seg_2269" n="HIAT:w" s="T588">ulkatɨgar</ts>
                  <nts id="Seg_2270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T590" id="Seg_2272" n="HIAT:w" s="T589">batarɨ</ts>
                  <nts id="Seg_2273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T591" id="Seg_2275" n="HIAT:w" s="T590">asta</ts>
                  <nts id="Seg_2276" n="HIAT:ip">.</nts>
                  <nts id="Seg_2277" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T595" id="Seg_2279" n="HIAT:u" s="T591">
                  <ts e="T592" id="Seg_2281" n="HIAT:w" s="T591">Kaːr</ts>
                  <nts id="Seg_2282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T593" id="Seg_2284" n="HIAT:w" s="T592">honno</ts>
                  <nts id="Seg_2285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T594" id="Seg_2287" n="HIAT:w" s="T593">iren</ts>
                  <nts id="Seg_2288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T595" id="Seg_2290" n="HIAT:w" s="T594">kaːlla</ts>
                  <nts id="Seg_2291" n="HIAT:ip">.</nts>
                  <nts id="Seg_2292" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T599" id="Seg_2294" n="HIAT:u" s="T595">
                  <ts e="T596" id="Seg_2296" n="HIAT:w" s="T595">Toŋon</ts>
                  <nts id="Seg_2297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T597" id="Seg_2299" n="HIAT:w" s="T596">ölbüt</ts>
                  <nts id="Seg_2300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T598" id="Seg_2302" n="HIAT:w" s="T597">dʼaktara</ts>
                  <nts id="Seg_2303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T599" id="Seg_2305" n="HIAT:w" s="T598">tilinne</ts>
                  <nts id="Seg_2306" n="HIAT:ip">.</nts>
                  <nts id="Seg_2307" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T605" id="Seg_2309" n="HIAT:u" s="T599">
                  <ts e="T600" id="Seg_2311" n="HIAT:w" s="T599">Körbüte</ts>
                  <nts id="Seg_2312" n="HIAT:ip">,</nts>
                  <nts id="Seg_2313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T601" id="Seg_2315" n="HIAT:w" s="T600">bert</ts>
                  <nts id="Seg_2316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T602" id="Seg_2318" n="HIAT:w" s="T601">basku͡oj</ts>
                  <nts id="Seg_2319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T603" id="Seg_2321" n="HIAT:w" s="T602">bagajɨ</ts>
                  <nts id="Seg_2322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T604" id="Seg_2324" n="HIAT:w" s="T603">kɨːs</ts>
                  <nts id="Seg_2325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T605" id="Seg_2327" n="HIAT:w" s="T604">ebit</ts>
                  <nts id="Seg_2328" n="HIAT:ip">.</nts>
                  <nts id="Seg_2329" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T609" id="Seg_2331" n="HIAT:u" s="T605">
                  <ts e="T606" id="Seg_2333" n="HIAT:w" s="T605">Kɨːs</ts>
                  <nts id="Seg_2334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T607" id="Seg_2336" n="HIAT:w" s="T606">ü͡ören</ts>
                  <nts id="Seg_2337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T608" id="Seg_2339" n="HIAT:w" s="T607">muŋa</ts>
                  <nts id="Seg_2340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T609" id="Seg_2342" n="HIAT:w" s="T608">hu͡ok</ts>
                  <nts id="Seg_2343" n="HIAT:ip">.</nts>
                  <nts id="Seg_2344" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T613" id="Seg_2346" n="HIAT:u" s="T609">
                  <nts id="Seg_2347" n="HIAT:ip">"</nts>
                  <ts e="T610" id="Seg_2349" n="HIAT:w" s="T609">Anɨ</ts>
                  <nts id="Seg_2350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T611" id="Seg_2352" n="HIAT:w" s="T610">bejem</ts>
                  <nts id="Seg_2353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T612" id="Seg_2355" n="HIAT:w" s="T611">dʼi͡eber</ts>
                  <nts id="Seg_2356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T613" id="Seg_2358" n="HIAT:w" s="T612">tiːji͡em</ts>
                  <nts id="Seg_2359" n="HIAT:ip">.</nts>
                  <nts id="Seg_2360" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T615" id="Seg_2362" n="HIAT:u" s="T613">
                  <ts e="T614" id="Seg_2364" n="HIAT:w" s="T613">Barsɨbakkɨn</ts>
                  <nts id="Seg_2365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T615" id="Seg_2367" n="HIAT:w" s="T614">du͡o</ts>
                  <nts id="Seg_2368" n="HIAT:ip">?</nts>
                  <nts id="Seg_2369" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T621" id="Seg_2371" n="HIAT:u" s="T615">
                  <ts e="T616" id="Seg_2373" n="HIAT:w" s="T615">Ahatɨ͡am</ts>
                  <nts id="Seg_2374" n="HIAT:ip">,</nts>
                  <nts id="Seg_2375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T617" id="Seg_2377" n="HIAT:w" s="T616">ičiges</ts>
                  <nts id="Seg_2378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T618" id="Seg_2380" n="HIAT:w" s="T617">tellekke</ts>
                  <nts id="Seg_2381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T619" id="Seg_2383" n="HIAT:w" s="T618">telgi͡em</ts>
                  <nts id="Seg_2384" n="HIAT:ip">"</nts>
                  <nts id="Seg_2385" n="HIAT:ip">,</nts>
                  <nts id="Seg_2386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T620" id="Seg_2388" n="HIAT:w" s="T619">kördöspüt</ts>
                  <nts id="Seg_2389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T621" id="Seg_2391" n="HIAT:w" s="T620">kɨːs</ts>
                  <nts id="Seg_2392" n="HIAT:ip">.</nts>
                  <nts id="Seg_2393" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T624" id="Seg_2395" n="HIAT:u" s="T621">
                  <nts id="Seg_2396" n="HIAT:ip">"</nts>
                  <ts e="T622" id="Seg_2398" n="HIAT:w" s="T621">ɨraːk</ts>
                  <nts id="Seg_2399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T623" id="Seg_2401" n="HIAT:w" s="T622">du͡o</ts>
                  <nts id="Seg_2402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T624" id="Seg_2404" n="HIAT:w" s="T623">dʼi͡eŋ</ts>
                  <nts id="Seg_2405" n="HIAT:ip">?</nts>
                  <nts id="Seg_2406" n="HIAT:ip">"</nts>
                  <nts id="Seg_2407" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T626" id="Seg_2409" n="HIAT:u" s="T624">
                  <nts id="Seg_2410" n="HIAT:ip">"</nts>
                  <ts e="T625" id="Seg_2412" n="HIAT:w" s="T624">Uː</ts>
                  <nts id="Seg_2413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T626" id="Seg_2415" n="HIAT:w" s="T625">ihiger</ts>
                  <nts id="Seg_2416" n="HIAT:ip">.</nts>
                  <nts id="Seg_2417" n="HIAT:ip">"</nts>
                  <nts id="Seg_2418" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T635" id="Seg_2420" n="HIAT:u" s="T626">
                  <ts e="T627" id="Seg_2422" n="HIAT:w" s="T626">U͡ol</ts>
                  <nts id="Seg_2423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T628" id="Seg_2425" n="HIAT:w" s="T627">ügüs</ts>
                  <nts id="Seg_2426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T629" id="Seg_2428" n="HIAT:w" s="T628">oduːnu</ts>
                  <nts id="Seg_2429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T630" id="Seg_2431" n="HIAT:w" s="T629">körbüte</ts>
                  <nts id="Seg_2432" n="HIAT:ip">,</nts>
                  <nts id="Seg_2433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T631" id="Seg_2435" n="HIAT:w" s="T630">ol</ts>
                  <nts id="Seg_2436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T632" id="Seg_2438" n="HIAT:w" s="T631">ihin</ts>
                  <nts id="Seg_2439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T633" id="Seg_2441" n="HIAT:w" s="T632">kɨːs</ts>
                  <nts id="Seg_2442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T634" id="Seg_2444" n="HIAT:w" s="T633">öhün</ts>
                  <nts id="Seg_2445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T635" id="Seg_2447" n="HIAT:w" s="T634">hökpötö</ts>
                  <nts id="Seg_2448" n="HIAT:ip">.</nts>
                  <nts id="Seg_2449" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T637" id="Seg_2451" n="HIAT:u" s="T635">
                  <ts e="T636" id="Seg_2453" n="HIAT:w" s="T635">Diː</ts>
                  <nts id="Seg_2454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T637" id="Seg_2456" n="HIAT:w" s="T636">ebit</ts>
                  <nts id="Seg_2457" n="HIAT:ip">:</nts>
                  <nts id="Seg_2458" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T641" id="Seg_2460" n="HIAT:u" s="T637">
                  <nts id="Seg_2461" n="HIAT:ip">"</nts>
                  <ts e="T638" id="Seg_2463" n="HIAT:w" s="T637">Min</ts>
                  <nts id="Seg_2464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T639" id="Seg_2466" n="HIAT:w" s="T638">edʼiːjbin</ts>
                  <nts id="Seg_2467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T640" id="Seg_2469" n="HIAT:w" s="T639">bulu͡okpun</ts>
                  <nts id="Seg_2470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T641" id="Seg_2472" n="HIAT:w" s="T640">naːda</ts>
                  <nts id="Seg_2473" n="HIAT:ip">.</nts>
                  <nts id="Seg_2474" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T645" id="Seg_2476" n="HIAT:u" s="T641">
                  <ts e="T642" id="Seg_2478" n="HIAT:w" s="T641">Kojut</ts>
                  <nts id="Seg_2479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T643" id="Seg_2481" n="HIAT:w" s="T642">keli͡em</ts>
                  <nts id="Seg_2482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T644" id="Seg_2484" n="HIAT:w" s="T643">eːt</ts>
                  <nts id="Seg_2485" n="HIAT:ip">,</nts>
                  <nts id="Seg_2486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T645" id="Seg_2488" n="HIAT:w" s="T644">küːt</ts>
                  <nts id="Seg_2489" n="HIAT:ip">.</nts>
                  <nts id="Seg_2490" n="HIAT:ip">"</nts>
                  <nts id="Seg_2491" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T651" id="Seg_2493" n="HIAT:u" s="T645">
                  <ts e="T646" id="Seg_2495" n="HIAT:w" s="T645">U͡ol</ts>
                  <nts id="Seg_2496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T647" id="Seg_2498" n="HIAT:w" s="T646">töhönü-kaččanɨ</ts>
                  <nts id="Seg_2499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T648" id="Seg_2501" n="HIAT:w" s="T647">barbɨta</ts>
                  <nts id="Seg_2502" n="HIAT:ip">,</nts>
                  <nts id="Seg_2503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T649" id="Seg_2505" n="HIAT:w" s="T648">kim</ts>
                  <nts id="Seg_2506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T650" id="Seg_2508" n="HIAT:w" s="T649">da</ts>
                  <nts id="Seg_2509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T651" id="Seg_2511" n="HIAT:w" s="T650">bilbet</ts>
                  <nts id="Seg_2512" n="HIAT:ip">.</nts>
                  <nts id="Seg_2513" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T654" id="Seg_2515" n="HIAT:u" s="T651">
                  <ts e="T652" id="Seg_2517" n="HIAT:w" s="T651">Araj</ts>
                  <nts id="Seg_2518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T653" id="Seg_2520" n="HIAT:w" s="T652">kadʼɨrɨk</ts>
                  <nts id="Seg_2521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T654" id="Seg_2523" n="HIAT:w" s="T653">bu͡olbut</ts>
                  <nts id="Seg_2524" n="HIAT:ip">.</nts>
                  <nts id="Seg_2525" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T656" id="Seg_2527" n="HIAT:u" s="T654">
                  <ts e="T655" id="Seg_2529" n="HIAT:w" s="T654">Ahɨːlɨktaːk</ts>
                  <nts id="Seg_2530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T656" id="Seg_2532" n="HIAT:w" s="T655">bagajɨ</ts>
                  <nts id="Seg_2533" n="HIAT:ip">.</nts>
                  <nts id="Seg_2534" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T658" id="Seg_2536" n="HIAT:u" s="T656">
                  <ts e="T657" id="Seg_2538" n="HIAT:w" s="T656">Tabalara</ts>
                  <nts id="Seg_2539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T658" id="Seg_2541" n="HIAT:w" s="T657">aččɨktaːtɨlar</ts>
                  <nts id="Seg_2542" n="HIAT:ip">.</nts>
                  <nts id="Seg_2543" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T662" id="Seg_2545" n="HIAT:u" s="T658">
                  <nts id="Seg_2546" n="HIAT:ip">"</nts>
                  <ts e="T659" id="Seg_2548" n="HIAT:w" s="T658">Ahatɨ͡akka</ts>
                  <nts id="Seg_2549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T660" id="Seg_2551" n="HIAT:w" s="T659">duː</ts>
                  <nts id="Seg_2552" n="HIAT:ip">"</nts>
                  <nts id="Seg_2553" n="HIAT:ip">,</nts>
                  <nts id="Seg_2554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T661" id="Seg_2556" n="HIAT:w" s="T660">diː</ts>
                  <nts id="Seg_2557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T662" id="Seg_2559" n="HIAT:w" s="T661">hanaːbɨt</ts>
                  <nts id="Seg_2560" n="HIAT:ip">.</nts>
                  <nts id="Seg_2561" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T668" id="Seg_2563" n="HIAT:u" s="T662">
                  <ts e="T663" id="Seg_2565" n="HIAT:w" s="T662">Hir</ts>
                  <nts id="Seg_2566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T664" id="Seg_2568" n="HIAT:w" s="T663">iččitin</ts>
                  <nts id="Seg_2569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T665" id="Seg_2571" n="HIAT:w" s="T664">haŋatɨn</ts>
                  <nts id="Seg_2572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T666" id="Seg_2574" n="HIAT:w" s="T665">öjdüː-öjdüː</ts>
                  <nts id="Seg_2575" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T667" id="Seg_2577" n="HIAT:w" s="T666">tabalarɨn</ts>
                  <nts id="Seg_2578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T668" id="Seg_2580" n="HIAT:w" s="T667">toktoːppoto</ts>
                  <nts id="Seg_2581" n="HIAT:ip">.</nts>
                  <nts id="Seg_2582" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T671" id="Seg_2584" n="HIAT:u" s="T668">
                  <ts e="T669" id="Seg_2586" n="HIAT:w" s="T668">Onton</ts>
                  <nts id="Seg_2587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T670" id="Seg_2589" n="HIAT:w" s="T669">diː</ts>
                  <nts id="Seg_2590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T671" id="Seg_2592" n="HIAT:w" s="T670">hanaːta</ts>
                  <nts id="Seg_2593" n="HIAT:ip">:</nts>
                  <nts id="Seg_2594" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T674" id="Seg_2596" n="HIAT:u" s="T671">
                  <nts id="Seg_2597" n="HIAT:ip">"</nts>
                  <ts e="T672" id="Seg_2599" n="HIAT:w" s="T671">Olus</ts>
                  <nts id="Seg_2600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T673" id="Seg_2602" n="HIAT:w" s="T672">aččɨktaːbɨttar</ts>
                  <nts id="Seg_2603" n="HIAT:ip">,</nts>
                  <nts id="Seg_2604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T674" id="Seg_2606" n="HIAT:w" s="T673">toktotu͡okka</ts>
                  <nts id="Seg_2607" n="HIAT:ip">.</nts>
                  <nts id="Seg_2608" n="HIAT:ip">"</nts>
                  <nts id="Seg_2609" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T683" id="Seg_2611" n="HIAT:u" s="T674">
                  <ts e="T675" id="Seg_2613" n="HIAT:w" s="T674">Toktotorun</ts>
                  <nts id="Seg_2614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T676" id="Seg_2616" n="HIAT:w" s="T675">kɨtta</ts>
                  <nts id="Seg_2617" n="HIAT:ip">,</nts>
                  <nts id="Seg_2618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T677" id="Seg_2620" n="HIAT:w" s="T676">ortokuː</ts>
                  <nts id="Seg_2621" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T678" id="Seg_2623" n="HIAT:w" s="T677">kölüːr</ts>
                  <nts id="Seg_2624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T679" id="Seg_2626" n="HIAT:w" s="T678">tabata</ts>
                  <nts id="Seg_2627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T680" id="Seg_2629" n="HIAT:w" s="T679">melis</ts>
                  <nts id="Seg_2630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T681" id="Seg_2632" n="HIAT:w" s="T680">ere</ts>
                  <nts id="Seg_2633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T682" id="Seg_2635" n="HIAT:w" s="T681">gɨnan</ts>
                  <nts id="Seg_2636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T683" id="Seg_2638" n="HIAT:w" s="T682">kaːlbɨt</ts>
                  <nts id="Seg_2639" n="HIAT:ip">.</nts>
                  <nts id="Seg_2640" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T687" id="Seg_2642" n="HIAT:u" s="T683">
                  <ts e="T684" id="Seg_2644" n="HIAT:w" s="T683">Kajdi͡e</ts>
                  <nts id="Seg_2645" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T685" id="Seg_2647" n="HIAT:w" s="T684">barbɨta</ts>
                  <nts id="Seg_2648" n="HIAT:ip">,</nts>
                  <nts id="Seg_2649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T686" id="Seg_2651" n="HIAT:w" s="T685">kajdi͡ek</ts>
                  <nts id="Seg_2652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T687" id="Seg_2654" n="HIAT:w" s="T686">köppüte</ts>
                  <nts id="Seg_2655" n="HIAT:ip">?</nts>
                  <nts id="Seg_2656" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T690" id="Seg_2658" n="HIAT:u" s="T687">
                  <ts e="T688" id="Seg_2660" n="HIAT:w" s="T687">Emi͡e</ts>
                  <nts id="Seg_2661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T689" id="Seg_2663" n="HIAT:w" s="T688">haŋa</ts>
                  <nts id="Seg_2664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T690" id="Seg_2666" n="HIAT:w" s="T689">ihillibit</ts>
                  <nts id="Seg_2667" n="HIAT:ip">:</nts>
                  <nts id="Seg_2668" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T695" id="Seg_2670" n="HIAT:u" s="T690">
                  <nts id="Seg_2671" n="HIAT:ip">"</nts>
                  <ts e="T691" id="Seg_2673" n="HIAT:w" s="T690">Min</ts>
                  <nts id="Seg_2674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T692" id="Seg_2676" n="HIAT:w" s="T691">eni͡eke</ts>
                  <nts id="Seg_2677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T693" id="Seg_2679" n="HIAT:w" s="T692">di͡ebitim</ts>
                  <nts id="Seg_2680" n="HIAT:ip">,</nts>
                  <nts id="Seg_2681" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T694" id="Seg_2683" n="HIAT:w" s="T693">ahatɨma</ts>
                  <nts id="Seg_2684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T695" id="Seg_2686" n="HIAT:w" s="T694">tabalargɨn</ts>
                  <nts id="Seg_2687" n="HIAT:ip">.</nts>
                  <nts id="Seg_2688" n="HIAT:ip">"</nts>
                  <nts id="Seg_2689" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T699" id="Seg_2691" n="HIAT:u" s="T695">
                  <ts e="T696" id="Seg_2693" n="HIAT:w" s="T695">U͡ol</ts>
                  <nts id="Seg_2694" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T697" id="Seg_2696" n="HIAT:w" s="T696">töhönü</ts>
                  <nts id="Seg_2697" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T698" id="Seg_2699" n="HIAT:w" s="T697">barbɨta</ts>
                  <nts id="Seg_2700" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T699" id="Seg_2702" n="HIAT:w" s="T698">dʼürü</ts>
                  <nts id="Seg_2703" n="HIAT:ip">?</nts>
                  <nts id="Seg_2704" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T706" id="Seg_2706" n="HIAT:u" s="T699">
                  <ts e="T700" id="Seg_2708" n="HIAT:w" s="T699">Maː</ts>
                  <nts id="Seg_2709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T701" id="Seg_2711" n="HIAT:w" s="T700">taːs</ts>
                  <nts id="Seg_2712" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T702" id="Seg_2714" n="HIAT:w" s="T701">kajatɨgar</ts>
                  <nts id="Seg_2715" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T703" id="Seg_2717" n="HIAT:w" s="T702">tiːjde</ts>
                  <nts id="Seg_2718" n="HIAT:ip">,</nts>
                  <nts id="Seg_2719" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T704" id="Seg_2721" n="HIAT:w" s="T703">ikki</ts>
                  <nts id="Seg_2722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T705" id="Seg_2724" n="HIAT:w" s="T704">aŋɨ</ts>
                  <nts id="Seg_2725" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T706" id="Seg_2727" n="HIAT:w" s="T705">kajdaːččɨtɨgar</ts>
                  <nts id="Seg_2728" n="HIAT:ip">.</nts>
                  <nts id="Seg_2729" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T711" id="Seg_2731" n="HIAT:u" s="T706">
                  <ts e="T707" id="Seg_2733" n="HIAT:w" s="T706">Araj</ts>
                  <nts id="Seg_2734" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T708" id="Seg_2736" n="HIAT:w" s="T707">bejetin</ts>
                  <nts id="Seg_2737" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T709" id="Seg_2739" n="HIAT:w" s="T708">hiriger</ts>
                  <nts id="Seg_2740" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T710" id="Seg_2742" n="HIAT:w" s="T709">tiːjbit</ts>
                  <nts id="Seg_2743" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T711" id="Seg_2745" n="HIAT:w" s="T710">ebit</ts>
                  <nts id="Seg_2746" n="HIAT:ip">.</nts>
                  <nts id="Seg_2747" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T719" id="Seg_2749" n="HIAT:u" s="T711">
                  <ts e="T712" id="Seg_2751" n="HIAT:w" s="T711">Toŋon</ts>
                  <nts id="Seg_2752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T713" id="Seg_2754" n="HIAT:w" s="T712">ölbüt</ts>
                  <nts id="Seg_2755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T714" id="Seg_2757" n="HIAT:w" s="T713">tabalar</ts>
                  <nts id="Seg_2758" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T715" id="Seg_2760" n="HIAT:w" s="T714">mu͡ostara</ts>
                  <nts id="Seg_2761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T716" id="Seg_2763" n="HIAT:w" s="T715">araččɨ</ts>
                  <nts id="Seg_2764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T717" id="Seg_2766" n="HIAT:w" s="T716">kaːr</ts>
                  <nts id="Seg_2767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T718" id="Seg_2769" n="HIAT:w" s="T717">ihitten</ts>
                  <nts id="Seg_2770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T719" id="Seg_2772" n="HIAT:w" s="T718">čorohollor</ts>
                  <nts id="Seg_2773" n="HIAT:ip">.</nts>
                  <nts id="Seg_2774" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T724" id="Seg_2776" n="HIAT:u" s="T719">
                  <ts e="T720" id="Seg_2778" n="HIAT:w" s="T719">Kaːrɨ</ts>
                  <nts id="Seg_2779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T721" id="Seg_2781" n="HIAT:w" s="T720">kürdʼen</ts>
                  <nts id="Seg_2782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T722" id="Seg_2784" n="HIAT:w" s="T721">edʼiːjin</ts>
                  <nts id="Seg_2785" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T723" id="Seg_2787" n="HIAT:w" s="T722">nʼu͡oŋuhutun</ts>
                  <nts id="Seg_2788" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T724" id="Seg_2790" n="HIAT:w" s="T723">bulla</ts>
                  <nts id="Seg_2791" n="HIAT:ip">.</nts>
                  <nts id="Seg_2792" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T728" id="Seg_2794" n="HIAT:u" s="T724">
                  <ts e="T725" id="Seg_2796" n="HIAT:w" s="T724">Itiː</ts>
                  <nts id="Seg_2797" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T726" id="Seg_2799" n="HIAT:w" s="T725">timir</ts>
                  <nts id="Seg_2800" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T727" id="Seg_2802" n="HIAT:w" s="T726">mahɨnan</ts>
                  <nts id="Seg_2803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T728" id="Seg_2805" n="HIAT:w" s="T727">taːrɨjda</ts>
                  <nts id="Seg_2806" n="HIAT:ip">.</nts>
                  <nts id="Seg_2807" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T732" id="Seg_2809" n="HIAT:u" s="T728">
                  <ts e="T729" id="Seg_2811" n="HIAT:w" s="T728">Tabata</ts>
                  <nts id="Seg_2812" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T730" id="Seg_2814" n="HIAT:w" s="T729">tillen</ts>
                  <nts id="Seg_2815" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T731" id="Seg_2817" n="HIAT:w" s="T730">baraːn</ts>
                  <nts id="Seg_2818" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T732" id="Seg_2820" n="HIAT:w" s="T731">turda</ts>
                  <nts id="Seg_2821" n="HIAT:ip">.</nts>
                  <nts id="Seg_2822" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T737" id="Seg_2824" n="HIAT:u" s="T732">
                  <ts e="T733" id="Seg_2826" n="HIAT:w" s="T732">Onton</ts>
                  <nts id="Seg_2827" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T734" id="Seg_2829" n="HIAT:w" s="T733">mahɨ</ts>
                  <nts id="Seg_2830" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T735" id="Seg_2832" n="HIAT:w" s="T734">ɨlla</ts>
                  <nts id="Seg_2833" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T736" id="Seg_2835" n="HIAT:w" s="T735">kaːrga</ts>
                  <nts id="Seg_2836" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T737" id="Seg_2838" n="HIAT:w" s="T736">asta</ts>
                  <nts id="Seg_2839" n="HIAT:ip">.</nts>
                  <nts id="Seg_2840" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T743" id="Seg_2842" n="HIAT:u" s="T737">
                  <ts e="T738" id="Seg_2844" n="HIAT:w" s="T737">Toŋon</ts>
                  <nts id="Seg_2845" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T739" id="Seg_2847" n="HIAT:w" s="T738">ölbüt</ts>
                  <nts id="Seg_2848" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T740" id="Seg_2850" n="HIAT:w" s="T739">tabalar</ts>
                  <nts id="Seg_2851" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T741" id="Seg_2853" n="HIAT:w" s="T740">honno</ts>
                  <nts id="Seg_2854" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T742" id="Seg_2856" n="HIAT:w" s="T741">barɨlara</ts>
                  <nts id="Seg_2857" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T743" id="Seg_2859" n="HIAT:w" s="T742">tilinniler</ts>
                  <nts id="Seg_2860" n="HIAT:ip">.</nts>
                  <nts id="Seg_2861" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T750" id="Seg_2863" n="HIAT:u" s="T743">
                  <ts e="T744" id="Seg_2865" n="HIAT:w" s="T743">Bütün</ts>
                  <nts id="Seg_2866" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T745" id="Seg_2868" n="HIAT:w" s="T744">ü͡ör</ts>
                  <nts id="Seg_2869" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T746" id="Seg_2871" n="HIAT:w" s="T745">bu͡olan</ts>
                  <nts id="Seg_2872" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T747" id="Seg_2874" n="HIAT:w" s="T746">ahɨː</ts>
                  <nts id="Seg_2875" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T748" id="Seg_2877" n="HIAT:w" s="T747">hɨldʼallar</ts>
                  <nts id="Seg_2878" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T749" id="Seg_2880" n="HIAT:w" s="T748">haŋa</ts>
                  <nts id="Seg_2881" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T750" id="Seg_2883" n="HIAT:w" s="T749">hirinen</ts>
                  <nts id="Seg_2884" n="HIAT:ip">.</nts>
                  <nts id="Seg_2885" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T753" id="Seg_2887" n="HIAT:u" s="T750">
                  <ts e="T751" id="Seg_2889" n="HIAT:w" s="T750">Kanʼɨspɨta</ts>
                  <nts id="Seg_2890" n="HIAT:ip">,</nts>
                  <nts id="Seg_2891" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T752" id="Seg_2893" n="HIAT:w" s="T751">edʼiːje</ts>
                  <nts id="Seg_2894" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T753" id="Seg_2896" n="HIAT:w" s="T752">turar</ts>
                  <nts id="Seg_2897" n="HIAT:ip">.</nts>
                  <nts id="Seg_2898" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T758" id="Seg_2900" n="HIAT:u" s="T753">
                  <ts e="T754" id="Seg_2902" n="HIAT:w" s="T753">U͡ol</ts>
                  <nts id="Seg_2903" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T755" id="Seg_2905" n="HIAT:w" s="T754">edʼiːjin</ts>
                  <nts id="Seg_2906" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T756" id="Seg_2908" n="HIAT:w" s="T755">körön</ts>
                  <nts id="Seg_2909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T757" id="Seg_2911" n="HIAT:w" s="T756">bert</ts>
                  <nts id="Seg_2912" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T758" id="Seg_2914" n="HIAT:w" s="T757">ü͡örde</ts>
                  <nts id="Seg_2915" n="HIAT:ip">.</nts>
                  <nts id="Seg_2916" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T767" id="Seg_2918" n="HIAT:u" s="T758">
                  <nts id="Seg_2919" n="HIAT:ip">"</nts>
                  <ts e="T759" id="Seg_2921" n="HIAT:w" s="T758">Maŋnaj</ts>
                  <nts id="Seg_2922" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T760" id="Seg_2924" n="HIAT:w" s="T759">en</ts>
                  <nts id="Seg_2925" n="HIAT:ip">"</nts>
                  <nts id="Seg_2926" n="HIAT:ip">,</nts>
                  <nts id="Seg_2927" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T761" id="Seg_2929" n="HIAT:w" s="T760">di͡ete</ts>
                  <nts id="Seg_2930" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T762" id="Seg_2932" n="HIAT:w" s="T761">edʼiːje</ts>
                  <nts id="Seg_2933" n="HIAT:ip">,</nts>
                  <nts id="Seg_2934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2935" n="HIAT:ip">"</nts>
                  <ts e="T763" id="Seg_2937" n="HIAT:w" s="T762">ör</ts>
                  <nts id="Seg_2938" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T764" id="Seg_2940" n="HIAT:w" s="T763">bagajdɨk</ts>
                  <nts id="Seg_2941" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T765" id="Seg_2943" n="HIAT:w" s="T764">utujbutuŋ</ts>
                  <nts id="Seg_2944" n="HIAT:ip">,</nts>
                  <nts id="Seg_2945" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T766" id="Seg_2947" n="HIAT:w" s="T765">onton</ts>
                  <nts id="Seg_2948" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T767" id="Seg_2950" n="HIAT:w" s="T766">min</ts>
                  <nts id="Seg_2951" n="HIAT:ip">.</nts>
                  <nts id="Seg_2952" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T770" id="Seg_2954" n="HIAT:u" s="T767">
                  <ts e="T768" id="Seg_2956" n="HIAT:w" s="T767">Haːs</ts>
                  <nts id="Seg_2957" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T769" id="Seg_2959" n="HIAT:w" s="T768">bu͡olbut</ts>
                  <nts id="Seg_2960" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T770" id="Seg_2962" n="HIAT:w" s="T769">ebit</ts>
                  <nts id="Seg_2963" n="HIAT:ip">.</nts>
                  <nts id="Seg_2964" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T774" id="Seg_2966" n="HIAT:u" s="T770">
                  <ts e="T771" id="Seg_2968" n="HIAT:w" s="T770">Kɨhɨnɨ</ts>
                  <nts id="Seg_2969" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T772" id="Seg_2971" n="HIAT:w" s="T771">meldʼi</ts>
                  <nts id="Seg_2972" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T773" id="Seg_2974" n="HIAT:w" s="T772">utujbuppun</ts>
                  <nts id="Seg_2975" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T774" id="Seg_2977" n="HIAT:w" s="T773">ebit</ts>
                  <nts id="Seg_2978" n="HIAT:ip">.</nts>
                  <nts id="Seg_2979" n="HIAT:ip">"</nts>
                  <nts id="Seg_2980" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T778" id="Seg_2982" n="HIAT:u" s="T774">
                  <ts e="T775" id="Seg_2984" n="HIAT:w" s="T774">U͡ol</ts>
                  <nts id="Seg_2985" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T776" id="Seg_2987" n="HIAT:w" s="T775">edʼiːjin</ts>
                  <nts id="Seg_2988" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T777" id="Seg_2990" n="HIAT:w" s="T776">dʼi͡etiger</ts>
                  <nts id="Seg_2991" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T778" id="Seg_2993" n="HIAT:w" s="T777">ilte</ts>
                  <nts id="Seg_2994" n="HIAT:ip">.</nts>
                  <nts id="Seg_2995" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T781" id="Seg_2997" n="HIAT:u" s="T778">
                  <ts e="T779" id="Seg_2999" n="HIAT:w" s="T778">Ü͡örün</ts>
                  <nts id="Seg_3000" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T780" id="Seg_3002" n="HIAT:w" s="T779">üːren</ts>
                  <nts id="Seg_3003" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T781" id="Seg_3005" n="HIAT:w" s="T780">egelle</ts>
                  <nts id="Seg_3006" n="HIAT:ip">.</nts>
                  <nts id="Seg_3007" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T787" id="Seg_3009" n="HIAT:u" s="T781">
                  <ts e="T782" id="Seg_3011" n="HIAT:w" s="T781">Töhö</ts>
                  <nts id="Seg_3012" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T783" id="Seg_3014" n="HIAT:w" s="T782">da</ts>
                  <nts id="Seg_3015" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T784" id="Seg_3017" n="HIAT:w" s="T783">bu͡olbakka</ts>
                  <nts id="Seg_3018" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T785" id="Seg_3020" n="HIAT:w" s="T784">bu</ts>
                  <nts id="Seg_3021" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T786" id="Seg_3023" n="HIAT:w" s="T785">u͡ol</ts>
                  <nts id="Seg_3024" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T787" id="Seg_3026" n="HIAT:w" s="T786">terinne</ts>
                  <nts id="Seg_3027" n="HIAT:ip">.</nts>
                  <nts id="Seg_3028" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T791" id="Seg_3030" n="HIAT:u" s="T787">
                  <nts id="Seg_3031" n="HIAT:ip">"</nts>
                  <ts e="T788" id="Seg_3033" n="HIAT:w" s="T787">Kajdi͡e</ts>
                  <nts id="Seg_3034" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T789" id="Seg_3036" n="HIAT:w" s="T788">baragɨn</ts>
                  <nts id="Seg_3037" n="HIAT:ip">"</nts>
                  <nts id="Seg_3038" n="HIAT:ip">,</nts>
                  <nts id="Seg_3039" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T790" id="Seg_3041" n="HIAT:w" s="T789">ɨjɨppɨt</ts>
                  <nts id="Seg_3042" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T791" id="Seg_3044" n="HIAT:w" s="T790">edʼiːje</ts>
                  <nts id="Seg_3045" n="HIAT:ip">.</nts>
                  <nts id="Seg_3046" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T794" id="Seg_3048" n="HIAT:u" s="T791">
                  <nts id="Seg_3049" n="HIAT:ip">"</nts>
                  <ts e="T792" id="Seg_3051" n="HIAT:w" s="T791">Hotoru</ts>
                  <nts id="Seg_3052" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T793" id="Seg_3054" n="HIAT:w" s="T792">keli͡em</ts>
                  <nts id="Seg_3055" n="HIAT:ip">,</nts>
                  <nts id="Seg_3056" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T794" id="Seg_3058" n="HIAT:w" s="T793">kečehime</ts>
                  <nts id="Seg_3059" n="HIAT:ip">.</nts>
                  <nts id="Seg_3060" n="HIAT:ip">"</nts>
                  <nts id="Seg_3061" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T802" id="Seg_3063" n="HIAT:u" s="T794">
                  <ts e="T795" id="Seg_3065" n="HIAT:w" s="T794">U͡ol</ts>
                  <nts id="Seg_3066" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T796" id="Seg_3068" n="HIAT:w" s="T795">maː</ts>
                  <nts id="Seg_3069" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T797" id="Seg_3071" n="HIAT:w" s="T796">kɨːhɨ</ts>
                  <nts id="Seg_3072" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T798" id="Seg_3074" n="HIAT:w" s="T797">kördüː</ts>
                  <nts id="Seg_3075" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T799" id="Seg_3077" n="HIAT:w" s="T798">barɨːhɨ</ts>
                  <nts id="Seg_3078" n="HIAT:ip">,</nts>
                  <nts id="Seg_3079" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T800" id="Seg_3081" n="HIAT:w" s="T799">uː</ts>
                  <nts id="Seg_3082" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T801" id="Seg_3084" n="HIAT:w" s="T800">ihiger</ts>
                  <nts id="Seg_3085" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T802" id="Seg_3087" n="HIAT:w" s="T801">dʼi͡eleːgi</ts>
                  <nts id="Seg_3088" n="HIAT:ip">.</nts>
                  <nts id="Seg_3089" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T808" id="Seg_3091" n="HIAT:u" s="T802">
                  <ts e="T803" id="Seg_3093" n="HIAT:w" s="T802">Baran</ts>
                  <nts id="Seg_3094" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T804" id="Seg_3096" n="HIAT:w" s="T803">ihen</ts>
                  <nts id="Seg_3097" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T805" id="Seg_3099" n="HIAT:w" s="T804">mɨlaː</ts>
                  <nts id="Seg_3100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T806" id="Seg_3102" n="HIAT:w" s="T805">bagaj</ts>
                  <nts id="Seg_3103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T807" id="Seg_3105" n="HIAT:w" s="T806">purgaː</ts>
                  <nts id="Seg_3106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T808" id="Seg_3108" n="HIAT:w" s="T807">bu͡olbut</ts>
                  <nts id="Seg_3109" n="HIAT:ip">.</nts>
                  <nts id="Seg_3110" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T816" id="Seg_3112" n="HIAT:u" s="T808">
                  <ts e="T809" id="Seg_3114" n="HIAT:w" s="T808">Hirinen</ts>
                  <nts id="Seg_3115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T810" id="Seg_3117" n="HIAT:w" s="T809">da</ts>
                  <nts id="Seg_3118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T811" id="Seg_3120" n="HIAT:w" s="T810">bararɨn</ts>
                  <nts id="Seg_3121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T812" id="Seg_3123" n="HIAT:w" s="T811">bilbet</ts>
                  <nts id="Seg_3124" n="HIAT:ip">,</nts>
                  <nts id="Seg_3125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T813" id="Seg_3127" n="HIAT:w" s="T812">buːhunan</ts>
                  <nts id="Seg_3128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T814" id="Seg_3130" n="HIAT:w" s="T813">da</ts>
                  <nts id="Seg_3131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T815" id="Seg_3133" n="HIAT:w" s="T814">bararɨn</ts>
                  <nts id="Seg_3134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T816" id="Seg_3136" n="HIAT:w" s="T815">bilbet</ts>
                  <nts id="Seg_3137" n="HIAT:ip">.</nts>
                  <nts id="Seg_3138" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T821" id="Seg_3140" n="HIAT:u" s="T816">
                  <nts id="Seg_3141" n="HIAT:ip">(</nts>
                  <ts e="T817" id="Seg_3143" n="HIAT:w" s="T816">Öjüleːn</ts>
                  <nts id="Seg_3144" n="HIAT:ip">)</nts>
                  <nts id="Seg_3145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T818" id="Seg_3147" n="HIAT:w" s="T817">körbüte</ts>
                  <nts id="Seg_3148" n="HIAT:ip">,</nts>
                  <nts id="Seg_3149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T819" id="Seg_3151" n="HIAT:w" s="T818">bajgal</ts>
                  <nts id="Seg_3152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T820" id="Seg_3154" n="HIAT:w" s="T819">buːha</ts>
                  <nts id="Seg_3155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T821" id="Seg_3157" n="HIAT:w" s="T820">ebit</ts>
                  <nts id="Seg_3158" n="HIAT:ip">.</nts>
                  <nts id="Seg_3159" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T827" id="Seg_3161" n="HIAT:u" s="T821">
                  <ts e="T822" id="Seg_3163" n="HIAT:w" s="T821">Haːskɨ</ts>
                  <nts id="Seg_3164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T823" id="Seg_3166" n="HIAT:w" s="T822">buːs</ts>
                  <nts id="Seg_3167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T824" id="Seg_3169" n="HIAT:w" s="T823">iŋnen</ts>
                  <nts id="Seg_3170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T825" id="Seg_3172" n="HIAT:w" s="T824">u͡ol</ts>
                  <nts id="Seg_3173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T826" id="Seg_3175" n="HIAT:w" s="T825">uːga</ts>
                  <nts id="Seg_3176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T827" id="Seg_3178" n="HIAT:w" s="T826">tüspüt</ts>
                  <nts id="Seg_3179" n="HIAT:ip">.</nts>
                  <nts id="Seg_3180" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T830" id="Seg_3182" n="HIAT:u" s="T827">
                  <ts e="T828" id="Seg_3184" n="HIAT:w" s="T827">Tühen</ts>
                  <nts id="Seg_3185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T829" id="Seg_3187" n="HIAT:w" s="T828">hordoŋ</ts>
                  <nts id="Seg_3188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T830" id="Seg_3190" n="HIAT:w" s="T829">bu͡olbut</ts>
                  <nts id="Seg_3191" n="HIAT:ip">.</nts>
                  <nts id="Seg_3192" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T836" id="Seg_3194" n="HIAT:u" s="T830">
                  <ts e="T831" id="Seg_3196" n="HIAT:w" s="T830">Uː</ts>
                  <nts id="Seg_3197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T832" id="Seg_3199" n="HIAT:w" s="T831">ihinen</ts>
                  <nts id="Seg_3200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T833" id="Seg_3202" n="HIAT:w" s="T832">usta</ts>
                  <nts id="Seg_3203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T834" id="Seg_3205" n="HIAT:w" s="T833">hɨldʼan</ts>
                  <nts id="Seg_3206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T835" id="Seg_3208" n="HIAT:w" s="T834">ilimŋe</ts>
                  <nts id="Seg_3209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T836" id="Seg_3211" n="HIAT:w" s="T835">tübespit</ts>
                  <nts id="Seg_3212" n="HIAT:ip">.</nts>
                  <nts id="Seg_3213" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T840" id="Seg_3215" n="HIAT:u" s="T836">
                  <ts e="T837" id="Seg_3217" n="HIAT:w" s="T836">I͡enneːk</ts>
                  <nts id="Seg_3218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T838" id="Seg_3220" n="HIAT:w" s="T837">ilimŋe</ts>
                  <nts id="Seg_3221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T839" id="Seg_3223" n="HIAT:w" s="T838">hordoŋnor</ts>
                  <nts id="Seg_3224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T840" id="Seg_3226" n="HIAT:w" s="T839">agaj</ts>
                  <nts id="Seg_3227" n="HIAT:ip">.</nts>
                  <nts id="Seg_3228" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T845" id="Seg_3230" n="HIAT:u" s="T840">
                  <ts e="T841" id="Seg_3232" n="HIAT:w" s="T840">Töhö</ts>
                  <nts id="Seg_3233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T842" id="Seg_3235" n="HIAT:w" s="T841">da</ts>
                  <nts id="Seg_3236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T843" id="Seg_3238" n="HIAT:w" s="T842">bu͡olbakka</ts>
                  <nts id="Seg_3239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T844" id="Seg_3241" n="HIAT:w" s="T843">kɨːs</ts>
                  <nts id="Seg_3242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T845" id="Seg_3244" n="HIAT:w" s="T844">kelle</ts>
                  <nts id="Seg_3245" n="HIAT:ip">.</nts>
                  <nts id="Seg_3246" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T848" id="Seg_3248" n="HIAT:u" s="T845">
                  <ts e="T846" id="Seg_3250" n="HIAT:w" s="T845">Maː</ts>
                  <nts id="Seg_3251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T847" id="Seg_3253" n="HIAT:w" s="T846">kɨːha</ts>
                  <nts id="Seg_3254" n="HIAT:ip">,</nts>
                  <nts id="Seg_3255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T848" id="Seg_3257" n="HIAT:w" s="T847">tilinnereːččite</ts>
                  <nts id="Seg_3258" n="HIAT:ip">.</nts>
                  <nts id="Seg_3259" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T854" id="Seg_3261" n="HIAT:u" s="T848">
                  <ts e="T849" id="Seg_3263" n="HIAT:w" s="T848">Hordoŋ</ts>
                  <nts id="Seg_3264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T850" id="Seg_3266" n="HIAT:w" s="T849">u͡olu</ts>
                  <nts id="Seg_3267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T851" id="Seg_3269" n="HIAT:w" s="T850">nʼu͡oŋuhut</ts>
                  <nts id="Seg_3270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T852" id="Seg_3272" n="HIAT:w" s="T851">gɨnan</ts>
                  <nts id="Seg_3273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T853" id="Seg_3275" n="HIAT:w" s="T852">baraːn</ts>
                  <nts id="Seg_3276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T854" id="Seg_3278" n="HIAT:w" s="T853">kölünne</ts>
                  <nts id="Seg_3279" n="HIAT:ip">.</nts>
                  <nts id="Seg_3280" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T856" id="Seg_3282" n="HIAT:u" s="T854">
                  <ts e="T855" id="Seg_3284" n="HIAT:w" s="T854">Dʼi͡etiger</ts>
                  <nts id="Seg_3285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T856" id="Seg_3287" n="HIAT:w" s="T855">barɨːhɨ</ts>
                  <nts id="Seg_3288" n="HIAT:ip">.</nts>
                  <nts id="Seg_3289" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T858" id="Seg_3291" n="HIAT:u" s="T856">
                  <ts e="T857" id="Seg_3293" n="HIAT:w" s="T856">Nʼu͡oŋuhut</ts>
                  <nts id="Seg_3294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T858" id="Seg_3296" n="HIAT:w" s="T857">istibet</ts>
                  <nts id="Seg_3297" n="HIAT:ip">.</nts>
                  <nts id="Seg_3298" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T862" id="Seg_3300" n="HIAT:u" s="T858">
                  <ts e="T859" id="Seg_3302" n="HIAT:w" s="T858">Kɨtɨl</ts>
                  <nts id="Seg_3303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T860" id="Seg_3305" n="HIAT:w" s="T859">ere</ts>
                  <nts id="Seg_3306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T861" id="Seg_3308" n="HIAT:w" s="T860">di͡ek</ts>
                  <nts id="Seg_3309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T862" id="Seg_3311" n="HIAT:w" s="T861">talahar</ts>
                  <nts id="Seg_3312" n="HIAT:ip">.</nts>
                  <nts id="Seg_3313" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T866" id="Seg_3315" n="HIAT:u" s="T862">
                  <ts e="T863" id="Seg_3317" n="HIAT:w" s="T862">Kɨːs</ts>
                  <nts id="Seg_3318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T864" id="Seg_3320" n="HIAT:w" s="T863">onu</ts>
                  <nts id="Seg_3321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T865" id="Seg_3323" n="HIAT:w" s="T864">halaja</ts>
                  <nts id="Seg_3324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T866" id="Seg_3326" n="HIAT:w" s="T865">hatɨːr</ts>
                  <nts id="Seg_3327" n="HIAT:ip">.</nts>
                  <nts id="Seg_3328" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T871" id="Seg_3330" n="HIAT:u" s="T866">
                  <ts e="T867" id="Seg_3332" n="HIAT:w" s="T866">Nʼu͡oŋuhuta</ts>
                  <nts id="Seg_3333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T868" id="Seg_3335" n="HIAT:w" s="T867">bɨha</ts>
                  <nts id="Seg_3336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T869" id="Seg_3338" n="HIAT:w" s="T868">mökküjen</ts>
                  <nts id="Seg_3339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T870" id="Seg_3341" n="HIAT:w" s="T869">kɨtɨlga</ts>
                  <nts id="Seg_3342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T871" id="Seg_3344" n="HIAT:w" s="T870">tagɨsta</ts>
                  <nts id="Seg_3345" n="HIAT:ip">.</nts>
                  <nts id="Seg_3346" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T876" id="Seg_3348" n="HIAT:u" s="T871">
                  <ts e="T872" id="Seg_3350" n="HIAT:w" s="T871">Taksaːt</ts>
                  <nts id="Seg_3351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T873" id="Seg_3353" n="HIAT:w" s="T872">u͡ol-hordoŋ</ts>
                  <nts id="Seg_3354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T874" id="Seg_3356" n="HIAT:w" s="T873">ile</ts>
                  <nts id="Seg_3357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T875" id="Seg_3359" n="HIAT:w" s="T874">u͡ol</ts>
                  <nts id="Seg_3360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T876" id="Seg_3362" n="HIAT:w" s="T875">bu͡olbut</ts>
                  <nts id="Seg_3363" n="HIAT:ip">.</nts>
                  <nts id="Seg_3364" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T879" id="Seg_3366" n="HIAT:u" s="T876">
                  <ts e="T877" id="Seg_3368" n="HIAT:w" s="T876">Hordoŋnor</ts>
                  <nts id="Seg_3369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T878" id="Seg_3371" n="HIAT:w" s="T877">taba</ts>
                  <nts id="Seg_3372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T879" id="Seg_3374" n="HIAT:w" s="T878">bu͡ollular</ts>
                  <nts id="Seg_3375" n="HIAT:ip">.</nts>
                  <nts id="Seg_3376" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T882" id="Seg_3378" n="HIAT:u" s="T879">
                  <ts e="T880" id="Seg_3380" n="HIAT:w" s="T879">Kɨːstaːk</ts>
                  <nts id="Seg_3381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T881" id="Seg_3383" n="HIAT:w" s="T880">u͡ol</ts>
                  <nts id="Seg_3384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T882" id="Seg_3386" n="HIAT:w" s="T881">taːjɨstɨlar</ts>
                  <nts id="Seg_3387" n="HIAT:ip">.</nts>
                  <nts id="Seg_3388" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T884" id="Seg_3390" n="HIAT:u" s="T882">
                  <ts e="T883" id="Seg_3392" n="HIAT:w" s="T882">Kɨːs</ts>
                  <nts id="Seg_3393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T884" id="Seg_3395" n="HIAT:w" s="T883">ɨjɨtta</ts>
                  <nts id="Seg_3396" n="HIAT:ip">:</nts>
                  <nts id="Seg_3397" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T886" id="Seg_3399" n="HIAT:u" s="T884">
                  <nts id="Seg_3400" n="HIAT:ip">"</nts>
                  <ts e="T885" id="Seg_3402" n="HIAT:w" s="T884">Edʼiːjgin</ts>
                  <nts id="Seg_3403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T886" id="Seg_3405" n="HIAT:w" s="T885">bulluŋ</ts>
                  <nts id="Seg_3406" n="HIAT:ip">?</nts>
                  <nts id="Seg_3407" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T888" id="Seg_3409" n="HIAT:u" s="T886">
                  <ts e="T887" id="Seg_3411" n="HIAT:w" s="T886">Kajtak</ts>
                  <nts id="Seg_3412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T888" id="Seg_3414" n="HIAT:w" s="T887">olorogun</ts>
                  <nts id="Seg_3415" n="HIAT:ip">?</nts>
                  <nts id="Seg_3416" n="HIAT:ip">"</nts>
                  <nts id="Seg_3417" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T897" id="Seg_3419" n="HIAT:u" s="T888">
                  <nts id="Seg_3420" n="HIAT:ip">"</nts>
                  <ts e="T889" id="Seg_3422" n="HIAT:w" s="T888">Barɨs</ts>
                  <nts id="Seg_3423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T890" id="Seg_3425" n="HIAT:w" s="T889">minigin</ts>
                  <nts id="Seg_3426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T891" id="Seg_3428" n="HIAT:w" s="T890">kɨtta</ts>
                  <nts id="Seg_3429" n="HIAT:ip">"</nts>
                  <nts id="Seg_3430" n="HIAT:ip">,</nts>
                  <nts id="Seg_3431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T892" id="Seg_3433" n="HIAT:w" s="T891">di͡ete</ts>
                  <nts id="Seg_3434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T893" id="Seg_3436" n="HIAT:w" s="T892">u͡ol</ts>
                  <nts id="Seg_3437" n="HIAT:ip">,</nts>
                  <nts id="Seg_3438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3439" n="HIAT:ip">"</nts>
                  <ts e="T894" id="Seg_3441" n="HIAT:w" s="T893">edʼiːjim</ts>
                  <nts id="Seg_3442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T895" id="Seg_3444" n="HIAT:w" s="T894">enigin</ts>
                  <nts id="Seg_3445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T896" id="Seg_3447" n="HIAT:w" s="T895">küːter</ts>
                  <nts id="Seg_3448" n="HIAT:ip">,</nts>
                  <nts id="Seg_3449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T897" id="Seg_3451" n="HIAT:w" s="T896">bilsihiŋ</ts>
                  <nts id="Seg_3452" n="HIAT:ip">.</nts>
                  <nts id="Seg_3453" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T901" id="Seg_3455" n="HIAT:u" s="T897">
                  <ts e="T898" id="Seg_3457" n="HIAT:w" s="T897">Biːrge</ts>
                  <nts id="Seg_3458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T899" id="Seg_3460" n="HIAT:w" s="T898">oloru͡okput</ts>
                  <nts id="Seg_3461" n="HIAT:ip">,</nts>
                  <nts id="Seg_3462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T900" id="Seg_3464" n="HIAT:w" s="T899">ü͡örbütün</ts>
                  <nts id="Seg_3465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T901" id="Seg_3467" n="HIAT:w" s="T900">kolbu͡okput</ts>
                  <nts id="Seg_3468" n="HIAT:ip">.</nts>
                  <nts id="Seg_3469" n="HIAT:ip">"</nts>
                  <nts id="Seg_3470" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T912" id="Seg_3472" n="HIAT:u" s="T901">
                  <ts e="T902" id="Seg_3474" n="HIAT:w" s="T901">U͡olu</ts>
                  <nts id="Seg_3475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T903" id="Seg_3477" n="HIAT:w" s="T902">kɨtta</ts>
                  <nts id="Seg_3478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T904" id="Seg_3480" n="HIAT:w" s="T903">kɨːs</ts>
                  <nts id="Seg_3481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T905" id="Seg_3483" n="HIAT:w" s="T904">hordoŋnoru</ts>
                  <nts id="Seg_3484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T906" id="Seg_3486" n="HIAT:w" s="T905">uː</ts>
                  <nts id="Seg_3487" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T907" id="Seg_3489" n="HIAT:w" s="T906">ihitten</ts>
                  <nts id="Seg_3490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T908" id="Seg_3492" n="HIAT:w" s="T907">oroːtular</ts>
                  <nts id="Seg_3493" n="HIAT:ip">,</nts>
                  <nts id="Seg_3494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T909" id="Seg_3496" n="HIAT:w" s="T908">ontulara</ts>
                  <nts id="Seg_3497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T910" id="Seg_3499" n="HIAT:w" s="T909">bugdʼiː</ts>
                  <nts id="Seg_3500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T911" id="Seg_3502" n="HIAT:w" s="T910">taba</ts>
                  <nts id="Seg_3503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T912" id="Seg_3505" n="HIAT:w" s="T911">bu͡ollular</ts>
                  <nts id="Seg_3506" n="HIAT:ip">.</nts>
                  <nts id="Seg_3507" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T916" id="Seg_3509" n="HIAT:u" s="T912">
                  <ts e="T913" id="Seg_3511" n="HIAT:w" s="T912">U͡ol</ts>
                  <nts id="Seg_3512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T914" id="Seg_3514" n="HIAT:w" s="T913">edʼiːje</ts>
                  <nts id="Seg_3515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T915" id="Seg_3517" n="HIAT:w" s="T914">ü͡ören</ts>
                  <nts id="Seg_3518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T916" id="Seg_3520" n="HIAT:w" s="T915">körüste</ts>
                  <nts id="Seg_3521" n="HIAT:ip">.</nts>
                  <nts id="Seg_3522" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T920" id="Seg_3524" n="HIAT:u" s="T916">
                  <ts e="T917" id="Seg_3526" n="HIAT:w" s="T916">U͡ol</ts>
                  <nts id="Seg_3527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T918" id="Seg_3529" n="HIAT:w" s="T917">üs</ts>
                  <nts id="Seg_3530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T919" id="Seg_3532" n="HIAT:w" s="T918">künü</ts>
                  <nts id="Seg_3533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T920" id="Seg_3535" n="HIAT:w" s="T919">utujda</ts>
                  <nts id="Seg_3536" n="HIAT:ip">.</nts>
                  <nts id="Seg_3537" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T933" id="Seg_3539" n="HIAT:u" s="T920">
                  <ts e="T921" id="Seg_3541" n="HIAT:w" s="T920">Uhuktan</ts>
                  <nts id="Seg_3542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T922" id="Seg_3544" n="HIAT:w" s="T921">körbüte</ts>
                  <nts id="Seg_3545" n="HIAT:ip">,</nts>
                  <nts id="Seg_3546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T923" id="Seg_3548" n="HIAT:w" s="T922">edʼiːje</ts>
                  <nts id="Seg_3549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T924" id="Seg_3551" n="HIAT:w" s="T923">kɨːhɨ</ts>
                  <nts id="Seg_3552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T925" id="Seg_3554" n="HIAT:w" s="T924">gɨtta</ts>
                  <nts id="Seg_3555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T926" id="Seg_3557" n="HIAT:w" s="T925">kepsete-kepsete</ts>
                  <nts id="Seg_3558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T927" id="Seg_3560" n="HIAT:w" s="T926">külse</ts>
                  <nts id="Seg_3561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T928" id="Seg_3563" n="HIAT:w" s="T927">olorollor</ts>
                  <nts id="Seg_3564" n="HIAT:ip">,</nts>
                  <nts id="Seg_3565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T929" id="Seg_3567" n="HIAT:w" s="T928">čaːj</ts>
                  <nts id="Seg_3568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T930" id="Seg_3570" n="HIAT:w" s="T929">iheller</ts>
                  <nts id="Seg_3571" n="HIAT:ip">,</nts>
                  <nts id="Seg_3572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T931" id="Seg_3574" n="HIAT:w" s="T930">busput</ts>
                  <nts id="Seg_3575" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T932" id="Seg_3577" n="HIAT:w" s="T931">eti</ts>
                  <nts id="Seg_3578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T933" id="Seg_3580" n="HIAT:w" s="T932">hiːller</ts>
                  <nts id="Seg_3581" n="HIAT:ip">.</nts>
                  <nts id="Seg_3582" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T939" id="Seg_3584" n="HIAT:u" s="T933">
                  <ts e="T934" id="Seg_3586" n="HIAT:w" s="T933">U͡ol</ts>
                  <nts id="Seg_3587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T935" id="Seg_3589" n="HIAT:w" s="T934">ulkalarɨgar</ts>
                  <nts id="Seg_3590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T936" id="Seg_3592" n="HIAT:w" s="T935">olordo</ts>
                  <nts id="Seg_3593" n="HIAT:ip">,</nts>
                  <nts id="Seg_3594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T937" id="Seg_3596" n="HIAT:w" s="T936">bütün</ts>
                  <nts id="Seg_3597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T938" id="Seg_3599" n="HIAT:w" s="T937">tabanɨ</ts>
                  <nts id="Seg_3600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T939" id="Seg_3602" n="HIAT:w" s="T938">hi͡ete</ts>
                  <nts id="Seg_3603" n="HIAT:ip">.</nts>
                  <nts id="Seg_3604" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T946" id="Seg_3606" n="HIAT:u" s="T939">
                  <ts e="T940" id="Seg_3608" n="HIAT:w" s="T939">U͡ol</ts>
                  <nts id="Seg_3609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T941" id="Seg_3611" n="HIAT:w" s="T940">kɨːhɨ</ts>
                  <nts id="Seg_3612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T942" id="Seg_3614" n="HIAT:w" s="T941">dʼaktar</ts>
                  <nts id="Seg_3615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T943" id="Seg_3617" n="HIAT:w" s="T942">gɨmmɨt</ts>
                  <nts id="Seg_3618" n="HIAT:ip">,</nts>
                  <nts id="Seg_3619" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T944" id="Seg_3621" n="HIAT:w" s="T943">dʼolloːk</ts>
                  <nts id="Seg_3622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T945" id="Seg_3624" n="HIAT:w" s="T944">bagajtɨk</ts>
                  <nts id="Seg_3625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T946" id="Seg_3627" n="HIAT:w" s="T945">olorbuttar</ts>
                  <nts id="Seg_3628" n="HIAT:ip">.</nts>
                  <nts id="Seg_3629" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T946" id="Seg_3630" n="sc" s="T1">
               <ts e="T2" id="Seg_3632" n="e" s="T1">Ihilleːŋ </ts>
               <ts e="T3" id="Seg_3634" n="e" s="T2">dulgaːnnar </ts>
               <ts e="T4" id="Seg_3636" n="e" s="T3">oloŋkolorun </ts>
               <ts e="T5" id="Seg_3638" n="e" s="T4">"Edʼiːjdeːk </ts>
               <ts e="T6" id="Seg_3640" n="e" s="T5">u͡ol" </ts>
               <ts e="T7" id="Seg_3642" n="e" s="T6">di͡en </ts>
               <ts e="T8" id="Seg_3644" n="e" s="T7">aːttaːgɨ. </ts>
               <ts e="T9" id="Seg_3646" n="e" s="T8">U͡ol </ts>
               <ts e="T10" id="Seg_3648" n="e" s="T9">edʼiːjin </ts>
               <ts e="T11" id="Seg_3650" n="e" s="T10">kɨtta </ts>
               <ts e="T12" id="Seg_3652" n="e" s="T11">ikki͡ejkeːn </ts>
               <ts e="T13" id="Seg_3654" n="e" s="T12">olorbuttara. </ts>
               <ts e="T14" id="Seg_3656" n="e" s="T13">Edʼiːje </ts>
               <ts e="T15" id="Seg_3658" n="e" s="T14">ülehitteːk </ts>
               <ts e="T16" id="Seg_3660" n="e" s="T15">uːs </ts>
               <ts e="T17" id="Seg_3662" n="e" s="T16">bagajɨ. </ts>
               <ts e="T18" id="Seg_3664" n="e" s="T17">Hetiːlere </ts>
               <ts e="T19" id="Seg_3666" n="e" s="T18">bugdiː </ts>
               <ts e="T20" id="Seg_3668" n="e" s="T19">taba </ts>
               <ts e="T21" id="Seg_3670" n="e" s="T20">tiriːlerinen </ts>
               <ts e="T22" id="Seg_3672" n="e" s="T21">habɨːlaːktar. </ts>
               <ts e="T23" id="Seg_3674" n="e" s="T22">Uraha </ts>
               <ts e="T24" id="Seg_3676" n="e" s="T23">dʼi͡eni </ts>
               <ts e="T25" id="Seg_3678" n="e" s="T24">bejete </ts>
               <ts e="T26" id="Seg_3680" n="e" s="T25">tuttaːččɨ. </ts>
               <ts e="T27" id="Seg_3682" n="e" s="T26">Tabalarɨn </ts>
               <ts e="T28" id="Seg_3684" n="e" s="T27">da </ts>
               <ts e="T29" id="Seg_3686" n="e" s="T28">hajɨn </ts>
               <ts e="T30" id="Seg_3688" n="e" s="T29">keteːčči. </ts>
               <ts e="T31" id="Seg_3690" n="e" s="T30">U͡ol </ts>
               <ts e="T32" id="Seg_3692" n="e" s="T31">baltɨta </ts>
               <ts e="T33" id="Seg_3694" n="e" s="T32">kühüŋŋütten </ts>
               <ts e="T34" id="Seg_3696" n="e" s="T33">utujbut </ts>
               <ts e="T35" id="Seg_3698" n="e" s="T34">kün </ts>
               <ts e="T36" id="Seg_3700" n="e" s="T35">taksɨ͡ar </ts>
               <ts e="T37" id="Seg_3702" n="e" s="T36">di͡eri. </ts>
               <ts e="T38" id="Seg_3704" n="e" s="T37">Ü͡ördere </ts>
               <ts e="T39" id="Seg_3706" n="e" s="T38">ɨraːppɨt, </ts>
               <ts e="T40" id="Seg_3708" n="e" s="T39">oŋkoloro </ts>
               <ts e="T41" id="Seg_3710" n="e" s="T40">keŋeːbit. </ts>
               <ts e="T42" id="Seg_3712" n="e" s="T41">Kɨːs </ts>
               <ts e="T43" id="Seg_3714" n="e" s="T42">oŋko </ts>
               <ts e="T44" id="Seg_3716" n="e" s="T43">ularɨtɨːhɨ. </ts>
               <ts e="T45" id="Seg_3718" n="e" s="T44">Ü͡örün </ts>
               <ts e="T46" id="Seg_3720" n="e" s="T45">egelle, </ts>
               <ts e="T47" id="Seg_3722" n="e" s="T46">taba </ts>
               <ts e="T48" id="Seg_3724" n="e" s="T47">tutunna, </ts>
               <ts e="T49" id="Seg_3726" n="e" s="T48">kölünne. </ts>
               <ts e="T50" id="Seg_3728" n="e" s="T49">Baltɨtɨn </ts>
               <ts e="T51" id="Seg_3730" n="e" s="T50">uhugunnara </ts>
               <ts e="T52" id="Seg_3732" n="e" s="T51">hatɨːr. </ts>
               <ts e="T53" id="Seg_3734" n="e" s="T52">Amattan </ts>
               <ts e="T54" id="Seg_3736" n="e" s="T53">uhuktubat. </ts>
               <ts e="T55" id="Seg_3738" n="e" s="T54">Uraha </ts>
               <ts e="T56" id="Seg_3740" n="e" s="T55">dʼi͡etin </ts>
               <ts e="T57" id="Seg_3742" n="e" s="T56">kastaːn </ts>
               <ts e="T58" id="Seg_3744" n="e" s="T57">baraːn, </ts>
               <ts e="T59" id="Seg_3746" n="e" s="T58">baltɨtɨn </ts>
               <ts e="T60" id="Seg_3748" n="e" s="T59">kɨrsa </ts>
               <ts e="T61" id="Seg_3750" n="e" s="T60">hu͡organɨn </ts>
               <ts e="T62" id="Seg_3752" n="e" s="T61">tuːra </ts>
               <ts e="T63" id="Seg_3754" n="e" s="T62">tarta. </ts>
               <ts e="T64" id="Seg_3756" n="e" s="T63">Örüküjen </ts>
               <ts e="T65" id="Seg_3758" n="e" s="T64">uhugunna, </ts>
               <ts e="T66" id="Seg_3760" n="e" s="T65">taŋɨnna. </ts>
               <ts e="T67" id="Seg_3762" n="e" s="T66">Hɨrgatɨgar </ts>
               <ts e="T68" id="Seg_3764" n="e" s="T67">oloroːt </ts>
               <ts e="T69" id="Seg_3766" n="e" s="T68">emi͡e </ts>
               <ts e="T70" id="Seg_3768" n="e" s="T69">utujda. </ts>
               <ts e="T71" id="Seg_3770" n="e" s="T70">Mastanarɨgar, </ts>
               <ts e="T72" id="Seg_3772" n="e" s="T71">buːstanarɨgar, </ts>
               <ts e="T73" id="Seg_3774" n="e" s="T72">tabalanarɨgar </ts>
               <ts e="T74" id="Seg_3776" n="e" s="T73">bu͡olan </ts>
               <ts e="T75" id="Seg_3778" n="e" s="T74">kɨːs </ts>
               <ts e="T76" id="Seg_3780" n="e" s="T75">bert </ts>
               <ts e="T77" id="Seg_3782" n="e" s="T76">hɨlajar. </ts>
               <ts e="T78" id="Seg_3784" n="e" s="T77">Hɨlajan </ts>
               <ts e="T79" id="Seg_3786" n="e" s="T78">utujbut, </ts>
               <ts e="T80" id="Seg_3788" n="e" s="T79">utuja </ts>
               <ts e="T81" id="Seg_3790" n="e" s="T80">hɨtan </ts>
               <ts e="T82" id="Seg_3792" n="e" s="T81">toŋon </ts>
               <ts e="T83" id="Seg_3794" n="e" s="T82">kaːlbɨt. </ts>
               <ts e="T84" id="Seg_3796" n="e" s="T83">U͡ol </ts>
               <ts e="T85" id="Seg_3798" n="e" s="T84">baltɨta </ts>
               <ts e="T86" id="Seg_3800" n="e" s="T85">össü͡ö </ts>
               <ts e="T87" id="Seg_3802" n="e" s="T86">üs </ts>
               <ts e="T88" id="Seg_3804" n="e" s="T87">künü </ts>
               <ts e="T89" id="Seg_3806" n="e" s="T88">utujbut. </ts>
               <ts e="T90" id="Seg_3808" n="e" s="T89">Uhuktubuta, </ts>
               <ts e="T91" id="Seg_3810" n="e" s="T90">körbüte, </ts>
               <ts e="T92" id="Seg_3812" n="e" s="T91">edʼiːje </ts>
               <ts e="T93" id="Seg_3814" n="e" s="T92">toŋon </ts>
               <ts e="T94" id="Seg_3816" n="e" s="T93">kaːlbɨt. </ts>
               <ts e="T95" id="Seg_3818" n="e" s="T94">Ü͡öre </ts>
               <ts e="T96" id="Seg_3820" n="e" s="T95">barɨta </ts>
               <ts e="T97" id="Seg_3822" n="e" s="T96">toŋon </ts>
               <ts e="T98" id="Seg_3824" n="e" s="T97">ölbüt. </ts>
               <ts e="T99" id="Seg_3826" n="e" s="T98">Hobu͡oj </ts>
               <ts e="T100" id="Seg_3828" n="e" s="T99">oksubut. </ts>
               <ts e="T101" id="Seg_3830" n="e" s="T100">"U͡ot </ts>
               <ts e="T102" id="Seg_3832" n="e" s="T101">otunnakpɨna, </ts>
               <ts e="T103" id="Seg_3834" n="e" s="T102">bugaːt, </ts>
               <ts e="T104" id="Seg_3836" n="e" s="T103">edʼiːjim </ts>
               <ts e="T105" id="Seg_3838" n="e" s="T104">tilli͡e </ts>
               <ts e="T106" id="Seg_3840" n="e" s="T105">da", </ts>
               <ts e="T107" id="Seg_3842" n="e" s="T106">diː </ts>
               <ts e="T108" id="Seg_3844" n="e" s="T107">hanaːbɨt </ts>
               <ts e="T109" id="Seg_3846" n="e" s="T108">u͡ol, </ts>
               <ts e="T110" id="Seg_3848" n="e" s="T109">"mastana </ts>
               <ts e="T111" id="Seg_3850" n="e" s="T110">barɨ͡akka." </ts>
               <ts e="T112" id="Seg_3852" n="e" s="T111">Kajalaːk </ts>
               <ts e="T113" id="Seg_3854" n="e" s="T112">ürek </ts>
               <ts e="T114" id="Seg_3856" n="e" s="T113">ürdünen </ts>
               <ts e="T115" id="Seg_3858" n="e" s="T114">mas </ts>
               <ts e="T116" id="Seg_3860" n="e" s="T115">di͡ek </ts>
               <ts e="T117" id="Seg_3862" n="e" s="T116">hüːrbüt. </ts>
               <ts e="T118" id="Seg_3864" n="e" s="T117">Körbüte, </ts>
               <ts e="T119" id="Seg_3866" n="e" s="T118">kallaːntan </ts>
               <ts e="T120" id="Seg_3868" n="e" s="T119">tutuktaːk </ts>
               <ts e="T121" id="Seg_3870" n="e" s="T120">otuː </ts>
               <ts e="T122" id="Seg_3872" n="e" s="T121">gɨdaːrɨja </ts>
               <ts e="T123" id="Seg_3874" n="e" s="T122">hɨtar. </ts>
               <ts e="T124" id="Seg_3876" n="e" s="T123">Abaːhɨ </ts>
               <ts e="T125" id="Seg_3878" n="e" s="T124">üčehege </ts>
               <ts e="T126" id="Seg_3880" n="e" s="T125">et </ts>
               <ts e="T127" id="Seg_3882" n="e" s="T126">ü͡ölene </ts>
               <ts e="T128" id="Seg_3884" n="e" s="T127">oloror. </ts>
               <ts e="T129" id="Seg_3886" n="e" s="T128">U͡olu </ts>
               <ts e="T130" id="Seg_3888" n="e" s="T129">körböt. </ts>
               <ts e="T131" id="Seg_3890" n="e" s="T130">Bu </ts>
               <ts e="T132" id="Seg_3892" n="e" s="T131">u͡ol </ts>
               <ts e="T133" id="Seg_3894" n="e" s="T132">hulburuta </ts>
               <ts e="T134" id="Seg_3896" n="e" s="T133">tɨːta-tɨːta </ts>
               <ts e="T135" id="Seg_3898" n="e" s="T134">oŋu͡orguttan </ts>
               <ts e="T136" id="Seg_3900" n="e" s="T135">hiː </ts>
               <ts e="T137" id="Seg_3902" n="e" s="T136">olorbut. </ts>
               <ts e="T138" id="Seg_3904" n="e" s="T137">"Kajtak </ts>
               <ts e="T139" id="Seg_3906" n="e" s="T138">tu͡olkulaːbatɨn </ts>
               <ts e="T140" id="Seg_3908" n="e" s="T139">ke", </ts>
               <ts e="T141" id="Seg_3910" n="e" s="T140">diːr </ts>
               <ts e="T142" id="Seg_3912" n="e" s="T141">Abaːhɨ, </ts>
               <ts e="T143" id="Seg_3914" n="e" s="T142">"etterim </ts>
               <ts e="T144" id="Seg_3916" n="e" s="T143">u͡okka </ts>
               <ts e="T145" id="Seg_3918" n="e" s="T144">tühellerin?" </ts>
               <ts e="T146" id="Seg_3920" n="e" s="T145">Abaːhɨ </ts>
               <ts e="T147" id="Seg_3922" n="e" s="T146">u͡olu </ts>
               <ts e="T148" id="Seg_3924" n="e" s="T147">körön </ts>
               <ts e="T149" id="Seg_3926" n="e" s="T148">ölöröːrü </ts>
               <ts e="T150" id="Seg_3928" n="e" s="T149">gɨmmɨt. </ts>
               <ts e="T151" id="Seg_3930" n="e" s="T150">U͡ol </ts>
               <ts e="T152" id="Seg_3932" n="e" s="T151">uruːkatɨnan </ts>
               <ts e="T153" id="Seg_3934" n="e" s="T152">Abaːhɨ </ts>
               <ts e="T154" id="Seg_3936" n="e" s="T153">agɨs </ts>
               <ts e="T155" id="Seg_3938" n="e" s="T154">bahɨn </ts>
               <ts e="T156" id="Seg_3940" n="e" s="T155">tuːra </ts>
               <ts e="T157" id="Seg_3942" n="e" s="T156">oksubut. </ts>
               <ts e="T158" id="Seg_3944" n="e" s="T157">Bastara </ts>
               <ts e="T159" id="Seg_3946" n="e" s="T158">üŋküːlüː </ts>
               <ts e="T160" id="Seg_3948" n="e" s="T159">hɨldʼallar. </ts>
               <ts e="T161" id="Seg_3950" n="e" s="T160">U͡ol </ts>
               <ts e="T162" id="Seg_3952" n="e" s="T161">onno </ts>
               <ts e="T163" id="Seg_3954" n="e" s="T162">časkɨjbɨt: </ts>
               <ts e="T164" id="Seg_3956" n="e" s="T163">"Bukatɨːr </ts>
               <ts e="T165" id="Seg_3958" n="e" s="T164">ikkileːččite, </ts>
               <ts e="T166" id="Seg_3960" n="e" s="T165">ühüsteːččite </ts>
               <ts e="T167" id="Seg_3962" n="e" s="T166">hu͡ok!" </ts>
               <ts e="T168" id="Seg_3964" n="e" s="T167">Abaːhɨ </ts>
               <ts e="T169" id="Seg_3966" n="e" s="T168">tü͡öhün </ts>
               <ts e="T170" id="Seg_3968" n="e" s="T169">tɨːra </ts>
               <ts e="T171" id="Seg_3970" n="e" s="T170">tardan, </ts>
               <ts e="T172" id="Seg_3972" n="e" s="T171">taːhɨ </ts>
               <ts e="T173" id="Seg_3974" n="e" s="T172">oroːto. </ts>
               <ts e="T174" id="Seg_3976" n="e" s="T173">Taːs </ts>
               <ts e="T175" id="Seg_3978" n="e" s="T174">hürekteːk </ts>
               <ts e="T176" id="Seg_3980" n="e" s="T175">ebit. </ts>
               <ts e="T177" id="Seg_3982" n="e" s="T176">U͡ol </ts>
               <ts e="T178" id="Seg_3984" n="e" s="T177">taːhɨ </ts>
               <ts e="T179" id="Seg_3986" n="e" s="T178">tebe-tebe </ts>
               <ts e="T180" id="Seg_3988" n="e" s="T179">bara </ts>
               <ts e="T181" id="Seg_3990" n="e" s="T180">turbut. </ts>
               <ts e="T182" id="Seg_3992" n="e" s="T181">Innitiger </ts>
               <ts e="T183" id="Seg_3994" n="e" s="T182">hir </ts>
               <ts e="T184" id="Seg_3996" n="e" s="T183">aŋardaːk </ts>
               <ts e="T185" id="Seg_3998" n="e" s="T184">haga </ts>
               <ts e="T186" id="Seg_4000" n="e" s="T185">taːs </ts>
               <ts e="T187" id="Seg_4002" n="e" s="T186">turbut, </ts>
               <ts e="T188" id="Seg_4004" n="e" s="T187">taːs </ts>
               <ts e="T189" id="Seg_4006" n="e" s="T188">kaja. </ts>
               <ts e="T190" id="Seg_4008" n="e" s="T189">Taːs </ts>
               <ts e="T191" id="Seg_4010" n="e" s="T190">kajanɨ </ts>
               <ts e="T192" id="Seg_4012" n="e" s="T191">taːs </ts>
               <ts e="T193" id="Seg_4014" n="e" s="T192">hüreginen </ts>
               <ts e="T194" id="Seg_4016" n="e" s="T193">teppit. </ts>
               <ts e="T195" id="Seg_4018" n="e" s="T194">Kajata </ts>
               <ts e="T196" id="Seg_4020" n="e" s="T195">ikki </ts>
               <ts e="T197" id="Seg_4022" n="e" s="T196">aŋɨ </ts>
               <ts e="T198" id="Seg_4024" n="e" s="T197">kajdan </ts>
               <ts e="T199" id="Seg_4026" n="e" s="T198">tüspüt. </ts>
               <ts e="T200" id="Seg_4028" n="e" s="T199">Oduːlaːn </ts>
               <ts e="T201" id="Seg_4030" n="e" s="T200">körbüte, </ts>
               <ts e="T202" id="Seg_4032" n="e" s="T201">hir </ts>
               <ts e="T203" id="Seg_4034" n="e" s="T202">köstör. </ts>
               <ts e="T204" id="Seg_4036" n="e" s="T203">Onno </ts>
               <ts e="T205" id="Seg_4038" n="e" s="T204">baran </ts>
               <ts e="T206" id="Seg_4040" n="e" s="T205">hɨrgalaːk </ts>
               <ts e="T207" id="Seg_4042" n="e" s="T206">hu͡olun </ts>
               <ts e="T208" id="Seg_4044" n="e" s="T207">körbüt. </ts>
               <ts e="T209" id="Seg_4046" n="e" s="T208">Paːs, </ts>
               <ts e="T210" id="Seg_4048" n="e" s="T209">kapkaːn </ts>
               <ts e="T211" id="Seg_4050" n="e" s="T210">körüne </ts>
               <ts e="T212" id="Seg_4052" n="e" s="T211">hɨldʼar </ts>
               <ts e="T213" id="Seg_4054" n="e" s="T212">kihi </ts>
               <ts e="T214" id="Seg_4056" n="e" s="T213">ete </ts>
               <ts e="T215" id="Seg_4058" n="e" s="T214">ebit. </ts>
               <ts e="T216" id="Seg_4060" n="e" s="T215">Töhö </ts>
               <ts e="T217" id="Seg_4062" n="e" s="T216">da </ts>
               <ts e="T218" id="Seg_4064" n="e" s="T217">bu͡olbakka </ts>
               <ts e="T219" id="Seg_4066" n="e" s="T218">kɨrsahɨt </ts>
               <ts e="T220" id="Seg_4068" n="e" s="T219">utarɨ </ts>
               <ts e="T221" id="Seg_4070" n="e" s="T220">iherin </ts>
               <ts e="T222" id="Seg_4072" n="e" s="T221">körbüt. </ts>
               <ts e="T223" id="Seg_4074" n="e" s="T222">Kelen </ts>
               <ts e="T224" id="Seg_4076" n="e" s="T223">turda, </ts>
               <ts e="T225" id="Seg_4078" n="e" s="T224">doroːbolosto. </ts>
               <ts e="T226" id="Seg_4080" n="e" s="T225">"Tuːgu </ts>
               <ts e="T227" id="Seg_4082" n="e" s="T226">gɨna </ts>
               <ts e="T228" id="Seg_4084" n="e" s="T227">hɨldʼagɨn", </ts>
               <ts e="T229" id="Seg_4086" n="e" s="T228">ɨjɨppɨt </ts>
               <ts e="T230" id="Seg_4088" n="e" s="T229">u͡ol. </ts>
               <ts e="T231" id="Seg_4090" n="e" s="T230">"Paːs, </ts>
               <ts e="T232" id="Seg_4092" n="e" s="T231">kapkaːn </ts>
               <ts e="T233" id="Seg_4094" n="e" s="T232">körünebin." </ts>
               <ts e="T234" id="Seg_4096" n="e" s="T233">"ɨraːk </ts>
               <ts e="T235" id="Seg_4098" n="e" s="T234">dʼi͡eleːkkit </ts>
               <ts e="T236" id="Seg_4100" n="e" s="T235">du͡o? </ts>
               <ts e="T237" id="Seg_4102" n="e" s="T236">Kas </ts>
               <ts e="T238" id="Seg_4104" n="e" s="T237">kihigitij?" </ts>
               <ts e="T239" id="Seg_4106" n="e" s="T238">"Inʼebin, </ts>
               <ts e="T240" id="Seg_4108" n="e" s="T239">agabɨn </ts>
               <ts e="T241" id="Seg_4110" n="e" s="T240">kɨtta </ts>
               <ts e="T242" id="Seg_4112" n="e" s="T241">olorobun", </ts>
               <ts e="T243" id="Seg_4114" n="e" s="T242">di͡ebit </ts>
               <ts e="T244" id="Seg_4116" n="e" s="T243">u͡ol. </ts>
               <ts e="T245" id="Seg_4118" n="e" s="T244">"Meŋehin, </ts>
               <ts e="T246" id="Seg_4120" n="e" s="T245">illi͡em, </ts>
               <ts e="T247" id="Seg_4122" n="e" s="T246">ɨ͡aldʼɨt </ts>
               <ts e="T248" id="Seg_4124" n="e" s="T247">bu͡ol." </ts>
               <ts e="T249" id="Seg_4126" n="e" s="T248">Uraha </ts>
               <ts e="T250" id="Seg_4128" n="e" s="T249">dʼi͡etiger </ts>
               <ts e="T251" id="Seg_4130" n="e" s="T250">egelbit. </ts>
               <ts e="T252" id="Seg_4132" n="e" s="T251">Ogonnʼordoːk </ts>
               <ts e="T253" id="Seg_4134" n="e" s="T252">emeːksin </ts>
               <ts e="T254" id="Seg_4136" n="e" s="T253">utarɨ </ts>
               <ts e="T255" id="Seg_4138" n="e" s="T254">taksannar </ts>
               <ts e="T256" id="Seg_4140" n="e" s="T255">körsübütter. </ts>
               <ts e="T257" id="Seg_4142" n="e" s="T256">"Kaja </ts>
               <ts e="T258" id="Seg_4144" n="e" s="T257">hirten </ts>
               <ts e="T259" id="Seg_4146" n="e" s="T258">kelliŋ", </ts>
               <ts e="T260" id="Seg_4148" n="e" s="T259">ɨjɨppɨttar. </ts>
               <ts e="T261" id="Seg_4150" n="e" s="T260">"Manna </ts>
               <ts e="T262" id="Seg_4152" n="e" s="T261">tu͡ora </ts>
               <ts e="T263" id="Seg_4154" n="e" s="T262">karaktaːk, </ts>
               <ts e="T264" id="Seg_4156" n="e" s="T263">u͡ot </ts>
               <ts e="T265" id="Seg_4158" n="e" s="T264">ulluŋnaːk </ts>
               <ts e="T266" id="Seg_4160" n="e" s="T265">üktenelik." </ts>
               <ts e="T267" id="Seg_4162" n="e" s="T266">U͡ol </ts>
               <ts e="T268" id="Seg_4164" n="e" s="T267">kepseːbit: </ts>
               <ts e="T269" id="Seg_4166" n="e" s="T268">"Edʼiːjim </ts>
               <ts e="T270" id="Seg_4168" n="e" s="T269">toŋon </ts>
               <ts e="T271" id="Seg_4170" n="e" s="T270">öllö, </ts>
               <ts e="T272" id="Seg_4172" n="e" s="T271">tabalarbɨt </ts>
               <ts e="T273" id="Seg_4174" n="e" s="T272">emi͡e </ts>
               <ts e="T274" id="Seg_4176" n="e" s="T273">toŋon </ts>
               <ts e="T275" id="Seg_4178" n="e" s="T274">öllüler. </ts>
               <ts e="T276" id="Seg_4180" n="e" s="T275">Kaːr </ts>
               <ts e="T277" id="Seg_4182" n="e" s="T276">ihiger </ts>
               <ts e="T278" id="Seg_4184" n="e" s="T277">hɨtallar </ts>
               <ts e="T279" id="Seg_4186" n="e" s="T278">tibille. </ts>
               <ts e="T280" id="Seg_4188" n="e" s="T279">Kajtak </ts>
               <ts e="T281" id="Seg_4190" n="e" s="T280">kihi </ts>
               <ts e="T282" id="Seg_4192" n="e" s="T281">bu͡olu͡om </ts>
               <ts e="T283" id="Seg_4194" n="e" s="T282">dʼürü?" </ts>
               <ts e="T284" id="Seg_4196" n="e" s="T283">"Maŋnaj </ts>
               <ts e="T285" id="Seg_4198" n="e" s="T284">ɨ͡aldʼɨt </ts>
               <ts e="T286" id="Seg_4200" n="e" s="T285">bu͡ol", </ts>
               <ts e="T287" id="Seg_4202" n="e" s="T286">di͡ebit </ts>
               <ts e="T288" id="Seg_4204" n="e" s="T287">ogonnʼor, </ts>
               <ts e="T289" id="Seg_4206" n="e" s="T288">"onton </ts>
               <ts e="T290" id="Seg_4208" n="e" s="T289">hübelehi͡ekpit." </ts>
               <ts e="T291" id="Seg_4210" n="e" s="T290">U͡ola </ts>
               <ts e="T292" id="Seg_4212" n="e" s="T291">taksan </ts>
               <ts e="T293" id="Seg_4214" n="e" s="T292">tɨhɨnɨ </ts>
               <ts e="T294" id="Seg_4216" n="e" s="T293">ölörbüt. </ts>
               <ts e="T295" id="Seg_4218" n="e" s="T294">Tastaːk </ts>
               <ts e="T296" id="Seg_4220" n="e" s="T295">tɨhɨnɨ. </ts>
               <ts e="T297" id="Seg_4222" n="e" s="T296">ɨ͡aldʼɨt </ts>
               <ts e="T298" id="Seg_4224" n="e" s="T297">onu </ts>
               <ts e="T299" id="Seg_4226" n="e" s="T298">hogotogun </ts>
               <ts e="T300" id="Seg_4228" n="e" s="T299">hi͡ebit. </ts>
               <ts e="T301" id="Seg_4230" n="e" s="T300">"Tottuŋ", </ts>
               <ts e="T302" id="Seg_4232" n="e" s="T301">ɨjɨppɨttar. </ts>
               <ts e="T303" id="Seg_4234" n="e" s="T302">"Tottum, </ts>
               <ts e="T304" id="Seg_4236" n="e" s="T303">kühüŋŋütten </ts>
               <ts e="T305" id="Seg_4238" n="e" s="T304">ahɨː </ts>
               <ts e="T306" id="Seg_4240" n="e" s="T305">ilikpin. </ts>
               <ts e="T307" id="Seg_4242" n="e" s="T306">Anɨ </ts>
               <ts e="T308" id="Seg_4244" n="e" s="T307">hɨnnʼanɨ͡am." </ts>
               <ts e="T309" id="Seg_4246" n="e" s="T308">Üs </ts>
               <ts e="T310" id="Seg_4248" n="e" s="T309">künü </ts>
               <ts e="T311" id="Seg_4250" n="e" s="T310">u͡ol </ts>
               <ts e="T312" id="Seg_4252" n="e" s="T311">utujbut. </ts>
               <ts e="T313" id="Seg_4254" n="e" s="T312">Uhuktubutugar </ts>
               <ts e="T314" id="Seg_4256" n="e" s="T313">ogonnʼor </ts>
               <ts e="T315" id="Seg_4258" n="e" s="T314">muŋ </ts>
               <ts e="T316" id="Seg_4260" n="e" s="T315">emis </ts>
               <ts e="T317" id="Seg_4262" n="e" s="T316">tabanɨ </ts>
               <ts e="T318" id="Seg_4264" n="e" s="T317">ölörön </ts>
               <ts e="T319" id="Seg_4266" n="e" s="T318">hi͡ekpit. </ts>
               <ts e="T320" id="Seg_4268" n="e" s="T319">Ogonnʼor </ts>
               <ts e="T321" id="Seg_4270" n="e" s="T320">üs </ts>
               <ts e="T322" id="Seg_4272" n="e" s="T321">ɨjdaːgɨ </ts>
               <ts e="T323" id="Seg_4274" n="e" s="T322">ahɨ </ts>
               <ts e="T324" id="Seg_4276" n="e" s="T323">belemneːbit, </ts>
               <ts e="T325" id="Seg_4278" n="e" s="T324">barɨ͡agɨn. </ts>
               <ts e="T326" id="Seg_4280" n="e" s="T325">U͡ol </ts>
               <ts e="T327" id="Seg_4282" n="e" s="T326">di͡ebit: </ts>
               <ts e="T328" id="Seg_4284" n="e" s="T327">"Kanna </ts>
               <ts e="T329" id="Seg_4286" n="e" s="T328">ire </ts>
               <ts e="T330" id="Seg_4288" n="e" s="T329">hir </ts>
               <ts e="T331" id="Seg_4290" n="e" s="T330">iččite </ts>
               <ts e="T332" id="Seg_4292" n="e" s="T331">baːr </ts>
               <ts e="T333" id="Seg_4294" n="e" s="T332">ühü, </ts>
               <ts e="T334" id="Seg_4296" n="e" s="T333">onno </ts>
               <ts e="T335" id="Seg_4298" n="e" s="T334">barɨ͡am." </ts>
               <ts e="T336" id="Seg_4300" n="e" s="T335">U͡ol </ts>
               <ts e="T337" id="Seg_4302" n="e" s="T336">hɨrgatɨgar </ts>
               <ts e="T338" id="Seg_4304" n="e" s="T337">taːs </ts>
               <ts e="T339" id="Seg_4306" n="e" s="T338">hüregi </ts>
               <ts e="T340" id="Seg_4308" n="e" s="T339">uːrda. </ts>
               <ts e="T341" id="Seg_4310" n="e" s="T340">Ürek </ts>
               <ts e="T342" id="Seg_4312" n="e" s="T341">ihinen </ts>
               <ts e="T343" id="Seg_4314" n="e" s="T342">kamnaːn </ts>
               <ts e="T344" id="Seg_4316" n="e" s="T343">kaːlla. </ts>
               <ts e="T345" id="Seg_4318" n="e" s="T344">Töhö </ts>
               <ts e="T346" id="Seg_4320" n="e" s="T345">ördük </ts>
               <ts e="T347" id="Seg_4322" n="e" s="T346">barbɨta </ts>
               <ts e="T348" id="Seg_4324" n="e" s="T347">dʼürü? </ts>
               <ts e="T349" id="Seg_4326" n="e" s="T348">Araj, </ts>
               <ts e="T350" id="Seg_4328" n="e" s="T349">čuguːn </ts>
               <ts e="T351" id="Seg_4330" n="e" s="T350">uraha </ts>
               <ts e="T352" id="Seg_4332" n="e" s="T351">dʼi͡eni </ts>
               <ts e="T353" id="Seg_4334" n="e" s="T352">körbüt. </ts>
               <ts e="T354" id="Seg_4336" n="e" s="T353">Aːnɨ </ts>
               <ts e="T355" id="Seg_4338" n="e" s="T354">arɨjan </ts>
               <ts e="T356" id="Seg_4340" n="e" s="T355">kördö, </ts>
               <ts e="T357" id="Seg_4342" n="e" s="T356">kɨrdʼagas </ts>
               <ts e="T358" id="Seg_4344" n="e" s="T357">bagajɨ </ts>
               <ts e="T359" id="Seg_4346" n="e" s="T358">emeːksin </ts>
               <ts e="T360" id="Seg_4348" n="e" s="T359">oloror, </ts>
               <ts e="T361" id="Seg_4350" n="e" s="T360">attɨtɨgar </ts>
               <ts e="T362" id="Seg_4352" n="e" s="T361">kü͡ört </ts>
               <ts e="T363" id="Seg_4354" n="e" s="T362">hɨtar. </ts>
               <ts e="T364" id="Seg_4356" n="e" s="T363">Menʼiːtin </ts>
               <ts e="T365" id="Seg_4358" n="e" s="T364">ürdütüger </ts>
               <ts e="T366" id="Seg_4360" n="e" s="T365">mataga </ts>
               <ts e="T367" id="Seg_4362" n="e" s="T366">ɨjaːllɨbɨt. </ts>
               <ts e="T368" id="Seg_4364" n="e" s="T367">Mataga </ts>
               <ts e="T369" id="Seg_4366" n="e" s="T368">kihi </ts>
               <ts e="T370" id="Seg_4368" n="e" s="T369">hɨrajɨn </ts>
               <ts e="T371" id="Seg_4370" n="e" s="T370">majgɨlaːk. </ts>
               <ts e="T372" id="Seg_4372" n="e" s="T371">Ačaːk </ts>
               <ts e="T373" id="Seg_4374" n="e" s="T372">annɨta </ts>
               <ts e="T374" id="Seg_4376" n="e" s="T373">köŋdöj. </ts>
               <ts e="T375" id="Seg_4378" n="e" s="T374">Ürek </ts>
               <ts e="T376" id="Seg_4380" n="e" s="T375">usta </ts>
               <ts e="T377" id="Seg_4382" n="e" s="T376">hɨtar. </ts>
               <ts e="T378" id="Seg_4384" n="e" s="T377">Ürekke </ts>
               <ts e="T379" id="Seg_4386" n="e" s="T378">tɨːlaːk </ts>
               <ts e="T380" id="Seg_4388" n="e" s="T379">ilimnene </ts>
               <ts e="T381" id="Seg_4390" n="e" s="T380">hɨldʼar. </ts>
               <ts e="T382" id="Seg_4392" n="e" s="T381">Mataga </ts>
               <ts e="T383" id="Seg_4394" n="e" s="T382">haŋalaːk </ts>
               <ts e="T384" id="Seg_4396" n="e" s="T383">bu͡olbut: </ts>
               <ts e="T385" id="Seg_4398" n="e" s="T384">"ɨ͡aldʼɨt </ts>
               <ts e="T386" id="Seg_4400" n="e" s="T385">kelle, </ts>
               <ts e="T387" id="Seg_4402" n="e" s="T386">maččɨttar, </ts>
               <ts e="T388" id="Seg_4404" n="e" s="T387">mastaːŋ, </ts>
               <ts e="T389" id="Seg_4406" n="e" s="T388">u͡otta </ts>
               <ts e="T390" id="Seg_4408" n="e" s="T389">ottuŋ!" </ts>
               <ts e="T391" id="Seg_4410" n="e" s="T390">Mastara </ts>
               <ts e="T392" id="Seg_4412" n="e" s="T391">bejelere </ts>
               <ts e="T393" id="Seg_4414" n="e" s="T392">kiːrdiler </ts>
               <ts e="T394" id="Seg_4416" n="e" s="T393">tahaːrattan. </ts>
               <ts e="T395" id="Seg_4418" n="e" s="T394">U͡ol </ts>
               <ts e="T396" id="Seg_4420" n="e" s="T395">tu͡olkulaːta: </ts>
               <ts e="T397" id="Seg_4422" n="e" s="T396">"Timir </ts>
               <ts e="T398" id="Seg_4424" n="e" s="T397">mastar </ts>
               <ts e="T399" id="Seg_4426" n="e" s="T398">ebit." </ts>
               <ts e="T400" id="Seg_4428" n="e" s="T399">Emeːksin </ts>
               <ts e="T401" id="Seg_4430" n="e" s="T400">turan </ts>
               <ts e="T402" id="Seg_4432" n="e" s="T401">mastarɨ </ts>
               <ts e="T403" id="Seg_4434" n="e" s="T402">ačaːgɨgar </ts>
               <ts e="T404" id="Seg_4436" n="e" s="T403">musta. </ts>
               <ts e="T405" id="Seg_4438" n="e" s="T404">Mataga </ts>
               <ts e="T406" id="Seg_4440" n="e" s="T405">di͡ete: </ts>
               <ts e="T407" id="Seg_4442" n="e" s="T406">"Kü͡ört, </ts>
               <ts e="T408" id="Seg_4444" n="e" s="T407">kü͡örteː!" </ts>
               <ts e="T409" id="Seg_4446" n="e" s="T408">Uraga </ts>
               <ts e="T410" id="Seg_4448" n="e" s="T409">dʼi͡e </ts>
               <ts e="T411" id="Seg_4450" n="e" s="T410">timir </ts>
               <ts e="T412" id="Seg_4452" n="e" s="T411">mu͡ostata </ts>
               <ts e="T413" id="Seg_4454" n="e" s="T412">kɨtarčɨ </ts>
               <ts e="T414" id="Seg_4456" n="e" s="T413">barda. </ts>
               <ts e="T415" id="Seg_4458" n="e" s="T414">Mataga </ts>
               <ts e="T416" id="Seg_4460" n="e" s="T415">bi͡ek </ts>
               <ts e="T417" id="Seg_4462" n="e" s="T416">diːr: </ts>
               <ts e="T418" id="Seg_4464" n="e" s="T417">"Kü͡ört, </ts>
               <ts e="T419" id="Seg_4466" n="e" s="T418">kü͡örteː, </ts>
               <ts e="T420" id="Seg_4468" n="e" s="T419">kü͡örteː, </ts>
               <ts e="T421" id="Seg_4470" n="e" s="T420">ɨ͡aldʼɨt </ts>
               <ts e="T422" id="Seg_4472" n="e" s="T421">kelle, </ts>
               <ts e="T423" id="Seg_4474" n="e" s="T422">u͡otta </ts>
               <ts e="T424" id="Seg_4476" n="e" s="T423">ep!" </ts>
               <ts e="T425" id="Seg_4478" n="e" s="T424">Uraha </ts>
               <ts e="T426" id="Seg_4480" n="e" s="T425">dʼi͡e </ts>
               <ts e="T427" id="Seg_4482" n="e" s="T426">barɨta </ts>
               <ts e="T428" id="Seg_4484" n="e" s="T427">kɨtarda. </ts>
               <ts e="T429" id="Seg_4486" n="e" s="T428">Emeːksin </ts>
               <ts e="T430" id="Seg_4488" n="e" s="T429">ɨ͡aldʼɨt </ts>
               <ts e="T431" id="Seg_4490" n="e" s="T430">u͡olu </ts>
               <ts e="T432" id="Seg_4492" n="e" s="T431">kördöstö: </ts>
               <ts e="T433" id="Seg_4494" n="e" s="T432">"Ititime </ts>
               <ts e="T434" id="Seg_4496" n="e" s="T433">olus, </ts>
               <ts e="T435" id="Seg_4498" n="e" s="T434">hojut </ts>
               <ts e="T436" id="Seg_4500" n="e" s="T435">dʼe." </ts>
               <ts e="T437" id="Seg_4502" n="e" s="T436">U͡ol </ts>
               <ts e="T438" id="Seg_4504" n="e" s="T437">taːs </ts>
               <ts e="T439" id="Seg_4506" n="e" s="T438">hüregi </ts>
               <ts e="T440" id="Seg_4508" n="e" s="T439">ɨlan </ts>
               <ts e="T441" id="Seg_4510" n="e" s="T440">ačaːk </ts>
               <ts e="T442" id="Seg_4512" n="e" s="T441">ortotugar </ts>
               <ts e="T443" id="Seg_4514" n="e" s="T442">bɨrakta. </ts>
               <ts e="T444" id="Seg_4516" n="e" s="T443">Uraha </ts>
               <ts e="T445" id="Seg_4518" n="e" s="T444">dʼi͡e </ts>
               <ts e="T446" id="Seg_4520" n="e" s="T445">hojdo. </ts>
               <ts e="T447" id="Seg_4522" n="e" s="T446">Mu͡ostata </ts>
               <ts e="T448" id="Seg_4524" n="e" s="T447">kɨtarbata. </ts>
               <ts e="T449" id="Seg_4526" n="e" s="T448">Emeːksin </ts>
               <ts e="T450" id="Seg_4528" n="e" s="T449">emi͡e </ts>
               <ts e="T451" id="Seg_4530" n="e" s="T450">kam </ts>
               <ts e="T452" id="Seg_4532" n="e" s="T451">kɨrɨ͡a </ts>
               <ts e="T453" id="Seg_4534" n="e" s="T452">bu͡olla. </ts>
               <ts e="T454" id="Seg_4536" n="e" s="T453">"Olus </ts>
               <ts e="T455" id="Seg_4538" n="e" s="T454">toŋoruma", </ts>
               <ts e="T456" id="Seg_4540" n="e" s="T455">kördöspüt </ts>
               <ts e="T0" id="Seg_4542" n="e" s="T456">mataga, </ts>
               <ts e="T947" id="Seg_4544" n="e" s="T0">"barɨ </ts>
               <ts e="T948" id="Seg_4546" n="e" s="T947">hanaːgɨn </ts>
               <ts e="T457" id="Seg_4548" n="e" s="T948">toloru͡om." </ts>
               <ts e="T458" id="Seg_4550" n="e" s="T457">Mataga </ts>
               <ts e="T459" id="Seg_4552" n="e" s="T458">ebit </ts>
               <ts e="T460" id="Seg_4554" n="e" s="T459">hir </ts>
               <ts e="T461" id="Seg_4556" n="e" s="T460">iččite. </ts>
               <ts e="T462" id="Seg_4558" n="e" s="T461">U͡ol </ts>
               <ts e="T463" id="Seg_4560" n="e" s="T462">taːs </ts>
               <ts e="T464" id="Seg_4562" n="e" s="T463">hüregi </ts>
               <ts e="T465" id="Seg_4564" n="e" s="T464">ačaːk </ts>
               <ts e="T466" id="Seg_4566" n="e" s="T465">ihitten </ts>
               <ts e="T467" id="Seg_4568" n="e" s="T466">oroːn </ts>
               <ts e="T468" id="Seg_4570" n="e" s="T467">ɨlla. </ts>
               <ts e="T469" id="Seg_4572" n="e" s="T468">U͡ota </ts>
               <ts e="T470" id="Seg_4574" n="e" s="T469">emi͡e </ts>
               <ts e="T471" id="Seg_4576" n="e" s="T470">gɨdaːrɨjda. </ts>
               <ts e="T472" id="Seg_4578" n="e" s="T471">Mataga </ts>
               <ts e="T473" id="Seg_4580" n="e" s="T472">di͡ete </ts>
               <ts e="T474" id="Seg_4582" n="e" s="T473">u͡olga: </ts>
               <ts e="T475" id="Seg_4584" n="e" s="T474">"Tahaːra </ts>
               <ts e="T476" id="Seg_4586" n="e" s="T475">tagɨs, </ts>
               <ts e="T477" id="Seg_4588" n="e" s="T476">onno </ts>
               <ts e="T478" id="Seg_4590" n="e" s="T477">hɨrgalaːk </ts>
               <ts e="T479" id="Seg_4592" n="e" s="T478">taba </ts>
               <ts e="T480" id="Seg_4594" n="e" s="T479">turar." </ts>
               <ts e="T481" id="Seg_4596" n="e" s="T480">U͡ol </ts>
               <ts e="T482" id="Seg_4598" n="e" s="T481">tahaːra </ts>
               <ts e="T483" id="Seg_4600" n="e" s="T482">taksan, </ts>
               <ts e="T484" id="Seg_4602" n="e" s="T483">tu͡ok </ts>
               <ts e="T485" id="Seg_4604" n="e" s="T484">da </ts>
               <ts e="T486" id="Seg_4606" n="e" s="T485">hɨrgalaːk </ts>
               <ts e="T487" id="Seg_4608" n="e" s="T486">tabatɨn </ts>
               <ts e="T488" id="Seg_4610" n="e" s="T487">körböt. </ts>
               <ts e="T489" id="Seg_4612" n="e" s="T488">Kanna </ts>
               <ts e="T490" id="Seg_4614" n="e" s="T489">ere </ts>
               <ts e="T491" id="Seg_4616" n="e" s="T490">ču͡oraːn, </ts>
               <ts e="T492" id="Seg_4618" n="e" s="T491">gubaːrka </ts>
               <ts e="T493" id="Seg_4620" n="e" s="T492">ere </ts>
               <ts e="T494" id="Seg_4622" n="e" s="T493">tɨ͡aha </ts>
               <ts e="T495" id="Seg_4624" n="e" s="T494">ihiller. </ts>
               <ts e="T496" id="Seg_4626" n="e" s="T495">Ketegen </ts>
               <ts e="T497" id="Seg_4628" n="e" s="T496">di͡ek </ts>
               <ts e="T498" id="Seg_4630" n="e" s="T497">mataga </ts>
               <ts e="T499" id="Seg_4632" n="e" s="T498">haŋata </ts>
               <ts e="T500" id="Seg_4634" n="e" s="T499">ihillibit: </ts>
               <ts e="T501" id="Seg_4636" n="e" s="T500">"Olor </ts>
               <ts e="T502" id="Seg_4638" n="e" s="T501">hɨrgagar. </ts>
               <ts e="T503" id="Seg_4640" n="e" s="T502">Keŋniŋ </ts>
               <ts e="T504" id="Seg_4642" n="e" s="T503">di͡ek </ts>
               <ts e="T505" id="Seg_4644" n="e" s="T504">kanʼɨhɨma, </ts>
               <ts e="T506" id="Seg_4646" n="e" s="T505">hu͡ol </ts>
               <ts e="T507" id="Seg_4648" n="e" s="T506">aːra </ts>
               <ts e="T508" id="Seg_4650" n="e" s="T507">toktoːmo, </ts>
               <ts e="T509" id="Seg_4652" n="e" s="T508">tabalargɨn </ts>
               <ts e="T510" id="Seg_4654" n="e" s="T509">ahatɨma." </ts>
               <ts e="T511" id="Seg_4656" n="e" s="T510">U͡ol </ts>
               <ts e="T512" id="Seg_4658" n="e" s="T511">iliːtin </ts>
               <ts e="T513" id="Seg_4660" n="e" s="T512">uːnna </ts>
               <ts e="T514" id="Seg_4662" n="e" s="T513">nʼuŋuːnu </ts>
               <ts e="T515" id="Seg_4664" n="e" s="T514">kapta, </ts>
               <ts e="T516" id="Seg_4666" n="e" s="T515">köstübet </ts>
               <ts e="T517" id="Seg_4668" n="e" s="T516">hɨrgaga </ts>
               <ts e="T518" id="Seg_4670" n="e" s="T517">miːnne. </ts>
               <ts e="T519" id="Seg_4672" n="e" s="T518">Tɨ͡allaːk </ts>
               <ts e="T520" id="Seg_4674" n="e" s="T519">purga </ts>
               <ts e="T521" id="Seg_4676" n="e" s="T520">ogusta. </ts>
               <ts e="T522" id="Seg_4678" n="e" s="T521">Kaččanɨ </ts>
               <ts e="T523" id="Seg_4680" n="e" s="T522">barbɨta </ts>
               <ts e="T524" id="Seg_4682" n="e" s="T523">dʼürü, </ts>
               <ts e="T525" id="Seg_4684" n="e" s="T524">kim </ts>
               <ts e="T526" id="Seg_4686" n="e" s="T525">bili͡ej? </ts>
               <ts e="T527" id="Seg_4688" n="e" s="T526">Hɨrgalaːk </ts>
               <ts e="T528" id="Seg_4690" n="e" s="T527">tabalara </ts>
               <ts e="T529" id="Seg_4692" n="e" s="T528">bejelere </ts>
               <ts e="T530" id="Seg_4694" n="e" s="T529">illeller. </ts>
               <ts e="T531" id="Seg_4696" n="e" s="T530">U͡ol </ts>
               <ts e="T532" id="Seg_4698" n="e" s="T531">tommot. </ts>
               <ts e="T533" id="Seg_4700" n="e" s="T532">Iliːtin </ts>
               <ts e="T534" id="Seg_4702" n="e" s="T533">olbogun </ts>
               <ts e="T535" id="Seg_4704" n="e" s="T534">ihiger </ts>
               <ts e="T536" id="Seg_4706" n="e" s="T535">ukta. </ts>
               <ts e="T537" id="Seg_4708" n="e" s="T536">Onno </ts>
               <ts e="T538" id="Seg_4710" n="e" s="T537">itiː </ts>
               <ts e="T539" id="Seg_4712" n="e" s="T538">timir </ts>
               <ts e="T540" id="Seg_4714" n="e" s="T539">mas </ts>
               <ts e="T541" id="Seg_4716" n="e" s="T540">hɨtar </ts>
               <ts e="T542" id="Seg_4718" n="e" s="T541">ebit. </ts>
               <ts e="T543" id="Seg_4720" n="e" s="T542">Ontuta </ts>
               <ts e="T544" id="Seg_4722" n="e" s="T543">ititer. </ts>
               <ts e="T545" id="Seg_4724" n="e" s="T544">Ebe </ts>
               <ts e="T546" id="Seg_4726" n="e" s="T545">ürdüger </ts>
               <ts e="T547" id="Seg_4728" n="e" s="T546">kördö, </ts>
               <ts e="T548" id="Seg_4730" n="e" s="T547">hɨrgaga </ts>
               <ts e="T549" id="Seg_4732" n="e" s="T548">ölbüt </ts>
               <ts e="T550" id="Seg_4734" n="e" s="T549">dʼaktar </ts>
               <ts e="T551" id="Seg_4736" n="e" s="T550">hɨtar, </ts>
               <ts e="T552" id="Seg_4738" n="e" s="T551">kam </ts>
               <ts e="T553" id="Seg_4740" n="e" s="T552">tibillen </ts>
               <ts e="T554" id="Seg_4742" n="e" s="T553">baraːn. </ts>
               <ts e="T555" id="Seg_4744" n="e" s="T554">Tehijbekke </ts>
               <ts e="T556" id="Seg_4746" n="e" s="T555">toktoːto. </ts>
               <ts e="T557" id="Seg_4748" n="e" s="T556">Toktuːrun </ts>
               <ts e="T558" id="Seg_4750" n="e" s="T557">kɨtta </ts>
               <ts e="T559" id="Seg_4752" n="e" s="T558">nʼuŋuhuta </ts>
               <ts e="T560" id="Seg_4754" n="e" s="T559">hulbu </ts>
               <ts e="T561" id="Seg_4756" n="e" s="T560">ɨstanan, </ts>
               <ts e="T562" id="Seg_4758" n="e" s="T561">kajdi͡e </ts>
               <ts e="T563" id="Seg_4760" n="e" s="T562">barbɨta, </ts>
               <ts e="T564" id="Seg_4762" n="e" s="T563">kajdi͡ek </ts>
               <ts e="T565" id="Seg_4764" n="e" s="T564">kelbite? </ts>
               <ts e="T566" id="Seg_4766" n="e" s="T565">Üs </ts>
               <ts e="T567" id="Seg_4768" n="e" s="T566">koŋnomu͡oj </ts>
               <ts e="T568" id="Seg_4770" n="e" s="T567">tabalaːk </ts>
               <ts e="T569" id="Seg_4772" n="e" s="T568">ete </ts>
               <ts e="T570" id="Seg_4774" n="e" s="T569">ebit. </ts>
               <ts e="T571" id="Seg_4776" n="e" s="T570">Nʼuŋuhuta </ts>
               <ts e="T572" id="Seg_4778" n="e" s="T571">bɨstan </ts>
               <ts e="T573" id="Seg_4780" n="e" s="T572">baran </ts>
               <ts e="T574" id="Seg_4782" n="e" s="T573">kaːlbɨt. </ts>
               <ts e="T575" id="Seg_4784" n="e" s="T574">Mataga </ts>
               <ts e="T576" id="Seg_4786" n="e" s="T575">haŋata </ts>
               <ts e="T577" id="Seg_4788" n="e" s="T576">ihillibit: </ts>
               <ts e="T578" id="Seg_4790" n="e" s="T577">"Eni͡eke </ts>
               <ts e="T579" id="Seg_4792" n="e" s="T578">di͡ebitim, </ts>
               <ts e="T580" id="Seg_4794" n="e" s="T579">toktoːmo!" </ts>
               <ts e="T581" id="Seg_4796" n="e" s="T580">U͡ol </ts>
               <ts e="T582" id="Seg_4798" n="e" s="T581">hɨrgatɨttan </ts>
               <ts e="T583" id="Seg_4800" n="e" s="T582">kɨhɨl </ts>
               <ts e="T584" id="Seg_4802" n="e" s="T583">timir </ts>
               <ts e="T585" id="Seg_4804" n="e" s="T584">mahɨ </ts>
               <ts e="T586" id="Seg_4806" n="e" s="T585">ɨlan, </ts>
               <ts e="T587" id="Seg_4808" n="e" s="T586">kɨːs </ts>
               <ts e="T588" id="Seg_4810" n="e" s="T587">hɨrgatɨn </ts>
               <ts e="T589" id="Seg_4812" n="e" s="T588">ulkatɨgar </ts>
               <ts e="T590" id="Seg_4814" n="e" s="T589">batarɨ </ts>
               <ts e="T591" id="Seg_4816" n="e" s="T590">asta. </ts>
               <ts e="T592" id="Seg_4818" n="e" s="T591">Kaːr </ts>
               <ts e="T593" id="Seg_4820" n="e" s="T592">honno </ts>
               <ts e="T594" id="Seg_4822" n="e" s="T593">iren </ts>
               <ts e="T595" id="Seg_4824" n="e" s="T594">kaːlla. </ts>
               <ts e="T596" id="Seg_4826" n="e" s="T595">Toŋon </ts>
               <ts e="T597" id="Seg_4828" n="e" s="T596">ölbüt </ts>
               <ts e="T598" id="Seg_4830" n="e" s="T597">dʼaktara </ts>
               <ts e="T599" id="Seg_4832" n="e" s="T598">tilinne. </ts>
               <ts e="T600" id="Seg_4834" n="e" s="T599">Körbüte, </ts>
               <ts e="T601" id="Seg_4836" n="e" s="T600">bert </ts>
               <ts e="T602" id="Seg_4838" n="e" s="T601">basku͡oj </ts>
               <ts e="T603" id="Seg_4840" n="e" s="T602">bagajɨ </ts>
               <ts e="T604" id="Seg_4842" n="e" s="T603">kɨːs </ts>
               <ts e="T605" id="Seg_4844" n="e" s="T604">ebit. </ts>
               <ts e="T606" id="Seg_4846" n="e" s="T605">Kɨːs </ts>
               <ts e="T607" id="Seg_4848" n="e" s="T606">ü͡ören </ts>
               <ts e="T608" id="Seg_4850" n="e" s="T607">muŋa </ts>
               <ts e="T609" id="Seg_4852" n="e" s="T608">hu͡ok. </ts>
               <ts e="T610" id="Seg_4854" n="e" s="T609">"Anɨ </ts>
               <ts e="T611" id="Seg_4856" n="e" s="T610">bejem </ts>
               <ts e="T612" id="Seg_4858" n="e" s="T611">dʼi͡eber </ts>
               <ts e="T613" id="Seg_4860" n="e" s="T612">tiːji͡em. </ts>
               <ts e="T614" id="Seg_4862" n="e" s="T613">Barsɨbakkɨn </ts>
               <ts e="T615" id="Seg_4864" n="e" s="T614">du͡o? </ts>
               <ts e="T616" id="Seg_4866" n="e" s="T615">Ahatɨ͡am, </ts>
               <ts e="T617" id="Seg_4868" n="e" s="T616">ičiges </ts>
               <ts e="T618" id="Seg_4870" n="e" s="T617">tellekke </ts>
               <ts e="T619" id="Seg_4872" n="e" s="T618">telgi͡em", </ts>
               <ts e="T620" id="Seg_4874" n="e" s="T619">kördöspüt </ts>
               <ts e="T621" id="Seg_4876" n="e" s="T620">kɨːs. </ts>
               <ts e="T622" id="Seg_4878" n="e" s="T621">"ɨraːk </ts>
               <ts e="T623" id="Seg_4880" n="e" s="T622">du͡o </ts>
               <ts e="T624" id="Seg_4882" n="e" s="T623">dʼi͡eŋ?" </ts>
               <ts e="T625" id="Seg_4884" n="e" s="T624">"Uː </ts>
               <ts e="T626" id="Seg_4886" n="e" s="T625">ihiger." </ts>
               <ts e="T627" id="Seg_4888" n="e" s="T626">U͡ol </ts>
               <ts e="T628" id="Seg_4890" n="e" s="T627">ügüs </ts>
               <ts e="T629" id="Seg_4892" n="e" s="T628">oduːnu </ts>
               <ts e="T630" id="Seg_4894" n="e" s="T629">körbüte, </ts>
               <ts e="T631" id="Seg_4896" n="e" s="T630">ol </ts>
               <ts e="T632" id="Seg_4898" n="e" s="T631">ihin </ts>
               <ts e="T633" id="Seg_4900" n="e" s="T632">kɨːs </ts>
               <ts e="T634" id="Seg_4902" n="e" s="T633">öhün </ts>
               <ts e="T635" id="Seg_4904" n="e" s="T634">hökpötö. </ts>
               <ts e="T636" id="Seg_4906" n="e" s="T635">Diː </ts>
               <ts e="T637" id="Seg_4908" n="e" s="T636">ebit: </ts>
               <ts e="T638" id="Seg_4910" n="e" s="T637">"Min </ts>
               <ts e="T639" id="Seg_4912" n="e" s="T638">edʼiːjbin </ts>
               <ts e="T640" id="Seg_4914" n="e" s="T639">bulu͡okpun </ts>
               <ts e="T641" id="Seg_4916" n="e" s="T640">naːda. </ts>
               <ts e="T642" id="Seg_4918" n="e" s="T641">Kojut </ts>
               <ts e="T643" id="Seg_4920" n="e" s="T642">keli͡em </ts>
               <ts e="T644" id="Seg_4922" n="e" s="T643">eːt, </ts>
               <ts e="T645" id="Seg_4924" n="e" s="T644">küːt." </ts>
               <ts e="T646" id="Seg_4926" n="e" s="T645">U͡ol </ts>
               <ts e="T647" id="Seg_4928" n="e" s="T646">töhönü-kaččanɨ </ts>
               <ts e="T648" id="Seg_4930" n="e" s="T647">barbɨta, </ts>
               <ts e="T649" id="Seg_4932" n="e" s="T648">kim </ts>
               <ts e="T650" id="Seg_4934" n="e" s="T649">da </ts>
               <ts e="T651" id="Seg_4936" n="e" s="T650">bilbet. </ts>
               <ts e="T652" id="Seg_4938" n="e" s="T651">Araj </ts>
               <ts e="T653" id="Seg_4940" n="e" s="T652">kadʼɨrɨk </ts>
               <ts e="T654" id="Seg_4942" n="e" s="T653">bu͡olbut. </ts>
               <ts e="T655" id="Seg_4944" n="e" s="T654">Ahɨːlɨktaːk </ts>
               <ts e="T656" id="Seg_4946" n="e" s="T655">bagajɨ. </ts>
               <ts e="T657" id="Seg_4948" n="e" s="T656">Tabalara </ts>
               <ts e="T658" id="Seg_4950" n="e" s="T657">aččɨktaːtɨlar. </ts>
               <ts e="T659" id="Seg_4952" n="e" s="T658">"Ahatɨ͡akka </ts>
               <ts e="T660" id="Seg_4954" n="e" s="T659">duː", </ts>
               <ts e="T661" id="Seg_4956" n="e" s="T660">diː </ts>
               <ts e="T662" id="Seg_4958" n="e" s="T661">hanaːbɨt. </ts>
               <ts e="T663" id="Seg_4960" n="e" s="T662">Hir </ts>
               <ts e="T664" id="Seg_4962" n="e" s="T663">iččitin </ts>
               <ts e="T665" id="Seg_4964" n="e" s="T664">haŋatɨn </ts>
               <ts e="T666" id="Seg_4966" n="e" s="T665">öjdüː-öjdüː </ts>
               <ts e="T667" id="Seg_4968" n="e" s="T666">tabalarɨn </ts>
               <ts e="T668" id="Seg_4970" n="e" s="T667">toktoːppoto. </ts>
               <ts e="T669" id="Seg_4972" n="e" s="T668">Onton </ts>
               <ts e="T670" id="Seg_4974" n="e" s="T669">diː </ts>
               <ts e="T671" id="Seg_4976" n="e" s="T670">hanaːta: </ts>
               <ts e="T672" id="Seg_4978" n="e" s="T671">"Olus </ts>
               <ts e="T673" id="Seg_4980" n="e" s="T672">aččɨktaːbɨttar, </ts>
               <ts e="T674" id="Seg_4982" n="e" s="T673">toktotu͡okka." </ts>
               <ts e="T675" id="Seg_4984" n="e" s="T674">Toktotorun </ts>
               <ts e="T676" id="Seg_4986" n="e" s="T675">kɨtta, </ts>
               <ts e="T677" id="Seg_4988" n="e" s="T676">ortokuː </ts>
               <ts e="T678" id="Seg_4990" n="e" s="T677">kölüːr </ts>
               <ts e="T679" id="Seg_4992" n="e" s="T678">tabata </ts>
               <ts e="T680" id="Seg_4994" n="e" s="T679">melis </ts>
               <ts e="T681" id="Seg_4996" n="e" s="T680">ere </ts>
               <ts e="T682" id="Seg_4998" n="e" s="T681">gɨnan </ts>
               <ts e="T683" id="Seg_5000" n="e" s="T682">kaːlbɨt. </ts>
               <ts e="T684" id="Seg_5002" n="e" s="T683">Kajdi͡e </ts>
               <ts e="T685" id="Seg_5004" n="e" s="T684">barbɨta, </ts>
               <ts e="T686" id="Seg_5006" n="e" s="T685">kajdi͡ek </ts>
               <ts e="T687" id="Seg_5008" n="e" s="T686">köppüte? </ts>
               <ts e="T688" id="Seg_5010" n="e" s="T687">Emi͡e </ts>
               <ts e="T689" id="Seg_5012" n="e" s="T688">haŋa </ts>
               <ts e="T690" id="Seg_5014" n="e" s="T689">ihillibit: </ts>
               <ts e="T691" id="Seg_5016" n="e" s="T690">"Min </ts>
               <ts e="T692" id="Seg_5018" n="e" s="T691">eni͡eke </ts>
               <ts e="T693" id="Seg_5020" n="e" s="T692">di͡ebitim, </ts>
               <ts e="T694" id="Seg_5022" n="e" s="T693">ahatɨma </ts>
               <ts e="T695" id="Seg_5024" n="e" s="T694">tabalargɨn." </ts>
               <ts e="T696" id="Seg_5026" n="e" s="T695">U͡ol </ts>
               <ts e="T697" id="Seg_5028" n="e" s="T696">töhönü </ts>
               <ts e="T698" id="Seg_5030" n="e" s="T697">barbɨta </ts>
               <ts e="T699" id="Seg_5032" n="e" s="T698">dʼürü? </ts>
               <ts e="T700" id="Seg_5034" n="e" s="T699">Maː </ts>
               <ts e="T701" id="Seg_5036" n="e" s="T700">taːs </ts>
               <ts e="T702" id="Seg_5038" n="e" s="T701">kajatɨgar </ts>
               <ts e="T703" id="Seg_5040" n="e" s="T702">tiːjde, </ts>
               <ts e="T704" id="Seg_5042" n="e" s="T703">ikki </ts>
               <ts e="T705" id="Seg_5044" n="e" s="T704">aŋɨ </ts>
               <ts e="T706" id="Seg_5046" n="e" s="T705">kajdaːččɨtɨgar. </ts>
               <ts e="T707" id="Seg_5048" n="e" s="T706">Araj </ts>
               <ts e="T708" id="Seg_5050" n="e" s="T707">bejetin </ts>
               <ts e="T709" id="Seg_5052" n="e" s="T708">hiriger </ts>
               <ts e="T710" id="Seg_5054" n="e" s="T709">tiːjbit </ts>
               <ts e="T711" id="Seg_5056" n="e" s="T710">ebit. </ts>
               <ts e="T712" id="Seg_5058" n="e" s="T711">Toŋon </ts>
               <ts e="T713" id="Seg_5060" n="e" s="T712">ölbüt </ts>
               <ts e="T714" id="Seg_5062" n="e" s="T713">tabalar </ts>
               <ts e="T715" id="Seg_5064" n="e" s="T714">mu͡ostara </ts>
               <ts e="T716" id="Seg_5066" n="e" s="T715">araččɨ </ts>
               <ts e="T717" id="Seg_5068" n="e" s="T716">kaːr </ts>
               <ts e="T718" id="Seg_5070" n="e" s="T717">ihitten </ts>
               <ts e="T719" id="Seg_5072" n="e" s="T718">čorohollor. </ts>
               <ts e="T720" id="Seg_5074" n="e" s="T719">Kaːrɨ </ts>
               <ts e="T721" id="Seg_5076" n="e" s="T720">kürdʼen </ts>
               <ts e="T722" id="Seg_5078" n="e" s="T721">edʼiːjin </ts>
               <ts e="T723" id="Seg_5080" n="e" s="T722">nʼu͡oŋuhutun </ts>
               <ts e="T724" id="Seg_5082" n="e" s="T723">bulla. </ts>
               <ts e="T725" id="Seg_5084" n="e" s="T724">Itiː </ts>
               <ts e="T726" id="Seg_5086" n="e" s="T725">timir </ts>
               <ts e="T727" id="Seg_5088" n="e" s="T726">mahɨnan </ts>
               <ts e="T728" id="Seg_5090" n="e" s="T727">taːrɨjda. </ts>
               <ts e="T729" id="Seg_5092" n="e" s="T728">Tabata </ts>
               <ts e="T730" id="Seg_5094" n="e" s="T729">tillen </ts>
               <ts e="T731" id="Seg_5096" n="e" s="T730">baraːn </ts>
               <ts e="T732" id="Seg_5098" n="e" s="T731">turda. </ts>
               <ts e="T733" id="Seg_5100" n="e" s="T732">Onton </ts>
               <ts e="T734" id="Seg_5102" n="e" s="T733">mahɨ </ts>
               <ts e="T735" id="Seg_5104" n="e" s="T734">ɨlla </ts>
               <ts e="T736" id="Seg_5106" n="e" s="T735">kaːrga </ts>
               <ts e="T737" id="Seg_5108" n="e" s="T736">asta. </ts>
               <ts e="T738" id="Seg_5110" n="e" s="T737">Toŋon </ts>
               <ts e="T739" id="Seg_5112" n="e" s="T738">ölbüt </ts>
               <ts e="T740" id="Seg_5114" n="e" s="T739">tabalar </ts>
               <ts e="T741" id="Seg_5116" n="e" s="T740">honno </ts>
               <ts e="T742" id="Seg_5118" n="e" s="T741">barɨlara </ts>
               <ts e="T743" id="Seg_5120" n="e" s="T742">tilinniler. </ts>
               <ts e="T744" id="Seg_5122" n="e" s="T743">Bütün </ts>
               <ts e="T745" id="Seg_5124" n="e" s="T744">ü͡ör </ts>
               <ts e="T746" id="Seg_5126" n="e" s="T745">bu͡olan </ts>
               <ts e="T747" id="Seg_5128" n="e" s="T746">ahɨː </ts>
               <ts e="T748" id="Seg_5130" n="e" s="T747">hɨldʼallar </ts>
               <ts e="T749" id="Seg_5132" n="e" s="T748">haŋa </ts>
               <ts e="T750" id="Seg_5134" n="e" s="T749">hirinen. </ts>
               <ts e="T751" id="Seg_5136" n="e" s="T750">Kanʼɨspɨta, </ts>
               <ts e="T752" id="Seg_5138" n="e" s="T751">edʼiːje </ts>
               <ts e="T753" id="Seg_5140" n="e" s="T752">turar. </ts>
               <ts e="T754" id="Seg_5142" n="e" s="T753">U͡ol </ts>
               <ts e="T755" id="Seg_5144" n="e" s="T754">edʼiːjin </ts>
               <ts e="T756" id="Seg_5146" n="e" s="T755">körön </ts>
               <ts e="T757" id="Seg_5148" n="e" s="T756">bert </ts>
               <ts e="T758" id="Seg_5150" n="e" s="T757">ü͡örde. </ts>
               <ts e="T759" id="Seg_5152" n="e" s="T758">"Maŋnaj </ts>
               <ts e="T760" id="Seg_5154" n="e" s="T759">en", </ts>
               <ts e="T761" id="Seg_5156" n="e" s="T760">di͡ete </ts>
               <ts e="T762" id="Seg_5158" n="e" s="T761">edʼiːje, </ts>
               <ts e="T763" id="Seg_5160" n="e" s="T762">"ör </ts>
               <ts e="T764" id="Seg_5162" n="e" s="T763">bagajdɨk </ts>
               <ts e="T765" id="Seg_5164" n="e" s="T764">utujbutuŋ, </ts>
               <ts e="T766" id="Seg_5166" n="e" s="T765">onton </ts>
               <ts e="T767" id="Seg_5168" n="e" s="T766">min. </ts>
               <ts e="T768" id="Seg_5170" n="e" s="T767">Haːs </ts>
               <ts e="T769" id="Seg_5172" n="e" s="T768">bu͡olbut </ts>
               <ts e="T770" id="Seg_5174" n="e" s="T769">ebit. </ts>
               <ts e="T771" id="Seg_5176" n="e" s="T770">Kɨhɨnɨ </ts>
               <ts e="T772" id="Seg_5178" n="e" s="T771">meldʼi </ts>
               <ts e="T773" id="Seg_5180" n="e" s="T772">utujbuppun </ts>
               <ts e="T774" id="Seg_5182" n="e" s="T773">ebit." </ts>
               <ts e="T775" id="Seg_5184" n="e" s="T774">U͡ol </ts>
               <ts e="T776" id="Seg_5186" n="e" s="T775">edʼiːjin </ts>
               <ts e="T777" id="Seg_5188" n="e" s="T776">dʼi͡etiger </ts>
               <ts e="T778" id="Seg_5190" n="e" s="T777">ilte. </ts>
               <ts e="T779" id="Seg_5192" n="e" s="T778">Ü͡örün </ts>
               <ts e="T780" id="Seg_5194" n="e" s="T779">üːren </ts>
               <ts e="T781" id="Seg_5196" n="e" s="T780">egelle. </ts>
               <ts e="T782" id="Seg_5198" n="e" s="T781">Töhö </ts>
               <ts e="T783" id="Seg_5200" n="e" s="T782">da </ts>
               <ts e="T784" id="Seg_5202" n="e" s="T783">bu͡olbakka </ts>
               <ts e="T785" id="Seg_5204" n="e" s="T784">bu </ts>
               <ts e="T786" id="Seg_5206" n="e" s="T785">u͡ol </ts>
               <ts e="T787" id="Seg_5208" n="e" s="T786">terinne. </ts>
               <ts e="T788" id="Seg_5210" n="e" s="T787">"Kajdi͡e </ts>
               <ts e="T789" id="Seg_5212" n="e" s="T788">baragɨn", </ts>
               <ts e="T790" id="Seg_5214" n="e" s="T789">ɨjɨppɨt </ts>
               <ts e="T791" id="Seg_5216" n="e" s="T790">edʼiːje. </ts>
               <ts e="T792" id="Seg_5218" n="e" s="T791">"Hotoru </ts>
               <ts e="T793" id="Seg_5220" n="e" s="T792">keli͡em, </ts>
               <ts e="T794" id="Seg_5222" n="e" s="T793">kečehime." </ts>
               <ts e="T795" id="Seg_5224" n="e" s="T794">U͡ol </ts>
               <ts e="T796" id="Seg_5226" n="e" s="T795">maː </ts>
               <ts e="T797" id="Seg_5228" n="e" s="T796">kɨːhɨ </ts>
               <ts e="T798" id="Seg_5230" n="e" s="T797">kördüː </ts>
               <ts e="T799" id="Seg_5232" n="e" s="T798">barɨːhɨ, </ts>
               <ts e="T800" id="Seg_5234" n="e" s="T799">uː </ts>
               <ts e="T801" id="Seg_5236" n="e" s="T800">ihiger </ts>
               <ts e="T802" id="Seg_5238" n="e" s="T801">dʼi͡eleːgi. </ts>
               <ts e="T803" id="Seg_5240" n="e" s="T802">Baran </ts>
               <ts e="T804" id="Seg_5242" n="e" s="T803">ihen </ts>
               <ts e="T805" id="Seg_5244" n="e" s="T804">mɨlaː </ts>
               <ts e="T806" id="Seg_5246" n="e" s="T805">bagaj </ts>
               <ts e="T807" id="Seg_5248" n="e" s="T806">purgaː </ts>
               <ts e="T808" id="Seg_5250" n="e" s="T807">bu͡olbut. </ts>
               <ts e="T809" id="Seg_5252" n="e" s="T808">Hirinen </ts>
               <ts e="T810" id="Seg_5254" n="e" s="T809">da </ts>
               <ts e="T811" id="Seg_5256" n="e" s="T810">bararɨn </ts>
               <ts e="T812" id="Seg_5258" n="e" s="T811">bilbet, </ts>
               <ts e="T813" id="Seg_5260" n="e" s="T812">buːhunan </ts>
               <ts e="T814" id="Seg_5262" n="e" s="T813">da </ts>
               <ts e="T815" id="Seg_5264" n="e" s="T814">bararɨn </ts>
               <ts e="T816" id="Seg_5266" n="e" s="T815">bilbet. </ts>
               <ts e="T817" id="Seg_5268" n="e" s="T816">(Öjüleːn) </ts>
               <ts e="T818" id="Seg_5270" n="e" s="T817">körbüte, </ts>
               <ts e="T819" id="Seg_5272" n="e" s="T818">bajgal </ts>
               <ts e="T820" id="Seg_5274" n="e" s="T819">buːha </ts>
               <ts e="T821" id="Seg_5276" n="e" s="T820">ebit. </ts>
               <ts e="T822" id="Seg_5278" n="e" s="T821">Haːskɨ </ts>
               <ts e="T823" id="Seg_5280" n="e" s="T822">buːs </ts>
               <ts e="T824" id="Seg_5282" n="e" s="T823">iŋnen </ts>
               <ts e="T825" id="Seg_5284" n="e" s="T824">u͡ol </ts>
               <ts e="T826" id="Seg_5286" n="e" s="T825">uːga </ts>
               <ts e="T827" id="Seg_5288" n="e" s="T826">tüspüt. </ts>
               <ts e="T828" id="Seg_5290" n="e" s="T827">Tühen </ts>
               <ts e="T829" id="Seg_5292" n="e" s="T828">hordoŋ </ts>
               <ts e="T830" id="Seg_5294" n="e" s="T829">bu͡olbut. </ts>
               <ts e="T831" id="Seg_5296" n="e" s="T830">Uː </ts>
               <ts e="T832" id="Seg_5298" n="e" s="T831">ihinen </ts>
               <ts e="T833" id="Seg_5300" n="e" s="T832">usta </ts>
               <ts e="T834" id="Seg_5302" n="e" s="T833">hɨldʼan </ts>
               <ts e="T835" id="Seg_5304" n="e" s="T834">ilimŋe </ts>
               <ts e="T836" id="Seg_5306" n="e" s="T835">tübespit. </ts>
               <ts e="T837" id="Seg_5308" n="e" s="T836">I͡enneːk </ts>
               <ts e="T838" id="Seg_5310" n="e" s="T837">ilimŋe </ts>
               <ts e="T839" id="Seg_5312" n="e" s="T838">hordoŋnor </ts>
               <ts e="T840" id="Seg_5314" n="e" s="T839">agaj. </ts>
               <ts e="T841" id="Seg_5316" n="e" s="T840">Töhö </ts>
               <ts e="T842" id="Seg_5318" n="e" s="T841">da </ts>
               <ts e="T843" id="Seg_5320" n="e" s="T842">bu͡olbakka </ts>
               <ts e="T844" id="Seg_5322" n="e" s="T843">kɨːs </ts>
               <ts e="T845" id="Seg_5324" n="e" s="T844">kelle. </ts>
               <ts e="T846" id="Seg_5326" n="e" s="T845">Maː </ts>
               <ts e="T847" id="Seg_5328" n="e" s="T846">kɨːha, </ts>
               <ts e="T848" id="Seg_5330" n="e" s="T847">tilinnereːččite. </ts>
               <ts e="T849" id="Seg_5332" n="e" s="T848">Hordoŋ </ts>
               <ts e="T850" id="Seg_5334" n="e" s="T849">u͡olu </ts>
               <ts e="T851" id="Seg_5336" n="e" s="T850">nʼu͡oŋuhut </ts>
               <ts e="T852" id="Seg_5338" n="e" s="T851">gɨnan </ts>
               <ts e="T853" id="Seg_5340" n="e" s="T852">baraːn </ts>
               <ts e="T854" id="Seg_5342" n="e" s="T853">kölünne. </ts>
               <ts e="T855" id="Seg_5344" n="e" s="T854">Dʼi͡etiger </ts>
               <ts e="T856" id="Seg_5346" n="e" s="T855">barɨːhɨ. </ts>
               <ts e="T857" id="Seg_5348" n="e" s="T856">Nʼu͡oŋuhut </ts>
               <ts e="T858" id="Seg_5350" n="e" s="T857">istibet. </ts>
               <ts e="T859" id="Seg_5352" n="e" s="T858">Kɨtɨl </ts>
               <ts e="T860" id="Seg_5354" n="e" s="T859">ere </ts>
               <ts e="T861" id="Seg_5356" n="e" s="T860">di͡ek </ts>
               <ts e="T862" id="Seg_5358" n="e" s="T861">talahar. </ts>
               <ts e="T863" id="Seg_5360" n="e" s="T862">Kɨːs </ts>
               <ts e="T864" id="Seg_5362" n="e" s="T863">onu </ts>
               <ts e="T865" id="Seg_5364" n="e" s="T864">halaja </ts>
               <ts e="T866" id="Seg_5366" n="e" s="T865">hatɨːr. </ts>
               <ts e="T867" id="Seg_5368" n="e" s="T866">Nʼu͡oŋuhuta </ts>
               <ts e="T868" id="Seg_5370" n="e" s="T867">bɨha </ts>
               <ts e="T869" id="Seg_5372" n="e" s="T868">mökküjen </ts>
               <ts e="T870" id="Seg_5374" n="e" s="T869">kɨtɨlga </ts>
               <ts e="T871" id="Seg_5376" n="e" s="T870">tagɨsta. </ts>
               <ts e="T872" id="Seg_5378" n="e" s="T871">Taksaːt </ts>
               <ts e="T873" id="Seg_5380" n="e" s="T872">u͡ol-hordoŋ </ts>
               <ts e="T874" id="Seg_5382" n="e" s="T873">ile </ts>
               <ts e="T875" id="Seg_5384" n="e" s="T874">u͡ol </ts>
               <ts e="T876" id="Seg_5386" n="e" s="T875">bu͡olbut. </ts>
               <ts e="T877" id="Seg_5388" n="e" s="T876">Hordoŋnor </ts>
               <ts e="T878" id="Seg_5390" n="e" s="T877">taba </ts>
               <ts e="T879" id="Seg_5392" n="e" s="T878">bu͡ollular. </ts>
               <ts e="T880" id="Seg_5394" n="e" s="T879">Kɨːstaːk </ts>
               <ts e="T881" id="Seg_5396" n="e" s="T880">u͡ol </ts>
               <ts e="T882" id="Seg_5398" n="e" s="T881">taːjɨstɨlar. </ts>
               <ts e="T883" id="Seg_5400" n="e" s="T882">Kɨːs </ts>
               <ts e="T884" id="Seg_5402" n="e" s="T883">ɨjɨtta: </ts>
               <ts e="T885" id="Seg_5404" n="e" s="T884">"Edʼiːjgin </ts>
               <ts e="T886" id="Seg_5406" n="e" s="T885">bulluŋ? </ts>
               <ts e="T887" id="Seg_5408" n="e" s="T886">Kajtak </ts>
               <ts e="T888" id="Seg_5410" n="e" s="T887">olorogun?" </ts>
               <ts e="T889" id="Seg_5412" n="e" s="T888">"Barɨs </ts>
               <ts e="T890" id="Seg_5414" n="e" s="T889">minigin </ts>
               <ts e="T891" id="Seg_5416" n="e" s="T890">kɨtta", </ts>
               <ts e="T892" id="Seg_5418" n="e" s="T891">di͡ete </ts>
               <ts e="T893" id="Seg_5420" n="e" s="T892">u͡ol, </ts>
               <ts e="T894" id="Seg_5422" n="e" s="T893">"edʼiːjim </ts>
               <ts e="T895" id="Seg_5424" n="e" s="T894">enigin </ts>
               <ts e="T896" id="Seg_5426" n="e" s="T895">küːter, </ts>
               <ts e="T897" id="Seg_5428" n="e" s="T896">bilsihiŋ. </ts>
               <ts e="T898" id="Seg_5430" n="e" s="T897">Biːrge </ts>
               <ts e="T899" id="Seg_5432" n="e" s="T898">oloru͡okput, </ts>
               <ts e="T900" id="Seg_5434" n="e" s="T899">ü͡örbütün </ts>
               <ts e="T901" id="Seg_5436" n="e" s="T900">kolbu͡okput." </ts>
               <ts e="T902" id="Seg_5438" n="e" s="T901">U͡olu </ts>
               <ts e="T903" id="Seg_5440" n="e" s="T902">kɨtta </ts>
               <ts e="T904" id="Seg_5442" n="e" s="T903">kɨːs </ts>
               <ts e="T905" id="Seg_5444" n="e" s="T904">hordoŋnoru </ts>
               <ts e="T906" id="Seg_5446" n="e" s="T905">uː </ts>
               <ts e="T907" id="Seg_5448" n="e" s="T906">ihitten </ts>
               <ts e="T908" id="Seg_5450" n="e" s="T907">oroːtular, </ts>
               <ts e="T909" id="Seg_5452" n="e" s="T908">ontulara </ts>
               <ts e="T910" id="Seg_5454" n="e" s="T909">bugdʼiː </ts>
               <ts e="T911" id="Seg_5456" n="e" s="T910">taba </ts>
               <ts e="T912" id="Seg_5458" n="e" s="T911">bu͡ollular. </ts>
               <ts e="T913" id="Seg_5460" n="e" s="T912">U͡ol </ts>
               <ts e="T914" id="Seg_5462" n="e" s="T913">edʼiːje </ts>
               <ts e="T915" id="Seg_5464" n="e" s="T914">ü͡ören </ts>
               <ts e="T916" id="Seg_5466" n="e" s="T915">körüste. </ts>
               <ts e="T917" id="Seg_5468" n="e" s="T916">U͡ol </ts>
               <ts e="T918" id="Seg_5470" n="e" s="T917">üs </ts>
               <ts e="T919" id="Seg_5472" n="e" s="T918">künü </ts>
               <ts e="T920" id="Seg_5474" n="e" s="T919">utujda. </ts>
               <ts e="T921" id="Seg_5476" n="e" s="T920">Uhuktan </ts>
               <ts e="T922" id="Seg_5478" n="e" s="T921">körbüte, </ts>
               <ts e="T923" id="Seg_5480" n="e" s="T922">edʼiːje </ts>
               <ts e="T924" id="Seg_5482" n="e" s="T923">kɨːhɨ </ts>
               <ts e="T925" id="Seg_5484" n="e" s="T924">gɨtta </ts>
               <ts e="T926" id="Seg_5486" n="e" s="T925">kepsete-kepsete </ts>
               <ts e="T927" id="Seg_5488" n="e" s="T926">külse </ts>
               <ts e="T928" id="Seg_5490" n="e" s="T927">olorollor, </ts>
               <ts e="T929" id="Seg_5492" n="e" s="T928">čaːj </ts>
               <ts e="T930" id="Seg_5494" n="e" s="T929">iheller, </ts>
               <ts e="T931" id="Seg_5496" n="e" s="T930">busput </ts>
               <ts e="T932" id="Seg_5498" n="e" s="T931">eti </ts>
               <ts e="T933" id="Seg_5500" n="e" s="T932">hiːller. </ts>
               <ts e="T934" id="Seg_5502" n="e" s="T933">U͡ol </ts>
               <ts e="T935" id="Seg_5504" n="e" s="T934">ulkalarɨgar </ts>
               <ts e="T936" id="Seg_5506" n="e" s="T935">olordo, </ts>
               <ts e="T937" id="Seg_5508" n="e" s="T936">bütün </ts>
               <ts e="T938" id="Seg_5510" n="e" s="T937">tabanɨ </ts>
               <ts e="T939" id="Seg_5512" n="e" s="T938">hi͡ete. </ts>
               <ts e="T940" id="Seg_5514" n="e" s="T939">U͡ol </ts>
               <ts e="T941" id="Seg_5516" n="e" s="T940">kɨːhɨ </ts>
               <ts e="T942" id="Seg_5518" n="e" s="T941">dʼaktar </ts>
               <ts e="T943" id="Seg_5520" n="e" s="T942">gɨmmɨt, </ts>
               <ts e="T944" id="Seg_5522" n="e" s="T943">dʼolloːk </ts>
               <ts e="T945" id="Seg_5524" n="e" s="T944">bagajtɨk </ts>
               <ts e="T946" id="Seg_5526" n="e" s="T945">olorbuttar. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T8" id="Seg_5527" s="T1">AkEE_19XX_BoySister_flk.001 (001.001)</ta>
            <ta e="T13" id="Seg_5528" s="T8">AkEE_19XX_BoySister_flk.002 (001.002)</ta>
            <ta e="T17" id="Seg_5529" s="T13">AkEE_19XX_BoySister_flk.003 (001.003)</ta>
            <ta e="T22" id="Seg_5530" s="T17">AkEE_19XX_BoySister_flk.004 (001.004)</ta>
            <ta e="T26" id="Seg_5531" s="T22">AkEE_19XX_BoySister_flk.005 (001.005)</ta>
            <ta e="T30" id="Seg_5532" s="T26">AkEE_19XX_BoySister_flk.006 (001.006)</ta>
            <ta e="T37" id="Seg_5533" s="T30">AkEE_19XX_BoySister_flk.007 (001.007)</ta>
            <ta e="T41" id="Seg_5534" s="T37">AkEE_19XX_BoySister_flk.008 (001.008)</ta>
            <ta e="T44" id="Seg_5535" s="T41">AkEE_19XX_BoySister_flk.009 (001.009)</ta>
            <ta e="T49" id="Seg_5536" s="T44">AkEE_19XX_BoySister_flk.010 (001.010)</ta>
            <ta e="T52" id="Seg_5537" s="T49">AkEE_19XX_BoySister_flk.011 (001.011)</ta>
            <ta e="T54" id="Seg_5538" s="T52">AkEE_19XX_BoySister_flk.012 (001.012)</ta>
            <ta e="T63" id="Seg_5539" s="T54">AkEE_19XX_BoySister_flk.013 (001.013)</ta>
            <ta e="T66" id="Seg_5540" s="T63">AkEE_19XX_BoySister_flk.014 (001.014)</ta>
            <ta e="T70" id="Seg_5541" s="T66">AkEE_19XX_BoySister_flk.015 (001.015)</ta>
            <ta e="T77" id="Seg_5542" s="T70">AkEE_19XX_BoySister_flk.016 (001.016)</ta>
            <ta e="T83" id="Seg_5543" s="T77">AkEE_19XX_BoySister_flk.017 (001.017)</ta>
            <ta e="T89" id="Seg_5544" s="T83">AkEE_19XX_BoySister_flk.018 (001.018)</ta>
            <ta e="T94" id="Seg_5545" s="T89">AkEE_19XX_BoySister_flk.019 (001.019)</ta>
            <ta e="T98" id="Seg_5546" s="T94">AkEE_19XX_BoySister_flk.020 (001.020)</ta>
            <ta e="T100" id="Seg_5547" s="T98">AkEE_19XX_BoySister_flk.021 (001.021)</ta>
            <ta e="T111" id="Seg_5548" s="T100">AkEE_19XX_BoySister_flk.022 (001.022)</ta>
            <ta e="T117" id="Seg_5549" s="T111">AkEE_19XX_BoySister_flk.023 (001.023)</ta>
            <ta e="T123" id="Seg_5550" s="T117">AkEE_19XX_BoySister_flk.024 (001.024)</ta>
            <ta e="T128" id="Seg_5551" s="T123">AkEE_19XX_BoySister_flk.025 (001.025)</ta>
            <ta e="T130" id="Seg_5552" s="T128">AkEE_19XX_BoySister_flk.026 (001.026)</ta>
            <ta e="T137" id="Seg_5553" s="T130">AkEE_19XX_BoySister_flk.027 (001.027)</ta>
            <ta e="T145" id="Seg_5554" s="T137">AkEE_19XX_BoySister_flk.028 (001.028)</ta>
            <ta e="T150" id="Seg_5555" s="T145">AkEE_19XX_BoySister_flk.029 (001.029)</ta>
            <ta e="T157" id="Seg_5556" s="T150">AkEE_19XX_BoySister_flk.030 (001.030)</ta>
            <ta e="T160" id="Seg_5557" s="T157">AkEE_19XX_BoySister_flk.031 (001.031)</ta>
            <ta e="T163" id="Seg_5558" s="T160">AkEE_19XX_BoySister_flk.032 (001.032)</ta>
            <ta e="T167" id="Seg_5559" s="T163">AkEE_19XX_BoySister_flk.033 (001.032)</ta>
            <ta e="T173" id="Seg_5560" s="T167">AkEE_19XX_BoySister_flk.034 (001.033)</ta>
            <ta e="T176" id="Seg_5561" s="T173">AkEE_19XX_BoySister_flk.035 (001.034)</ta>
            <ta e="T181" id="Seg_5562" s="T176">AkEE_19XX_BoySister_flk.036 (001.035)</ta>
            <ta e="T189" id="Seg_5563" s="T181">AkEE_19XX_BoySister_flk.037 (001.036)</ta>
            <ta e="T194" id="Seg_5564" s="T189">AkEE_19XX_BoySister_flk.038 (001.037)</ta>
            <ta e="T199" id="Seg_5565" s="T194">AkEE_19XX_BoySister_flk.039 (001.038)</ta>
            <ta e="T203" id="Seg_5566" s="T199">AkEE_19XX_BoySister_flk.040 (001.039)</ta>
            <ta e="T208" id="Seg_5567" s="T203">AkEE_19XX_BoySister_flk.041 (001.040)</ta>
            <ta e="T215" id="Seg_5568" s="T208">AkEE_19XX_BoySister_flk.042 (001.041)</ta>
            <ta e="T222" id="Seg_5569" s="T215">AkEE_19XX_BoySister_flk.043 (001.042)</ta>
            <ta e="T225" id="Seg_5570" s="T222">AkEE_19XX_BoySister_flk.044 (001.043)</ta>
            <ta e="T230" id="Seg_5571" s="T225">AkEE_19XX_BoySister_flk.045 (001.044)</ta>
            <ta e="T233" id="Seg_5572" s="T230">AkEE_19XX_BoySister_flk.046 (001.045)</ta>
            <ta e="T236" id="Seg_5573" s="T233">AkEE_19XX_BoySister_flk.047 (001.046)</ta>
            <ta e="T238" id="Seg_5574" s="T236">AkEE_19XX_BoySister_flk.048 (001.047)</ta>
            <ta e="T244" id="Seg_5575" s="T238">AkEE_19XX_BoySister_flk.049 (001.048)</ta>
            <ta e="T248" id="Seg_5576" s="T244">AkEE_19XX_BoySister_flk.050 (001.049)</ta>
            <ta e="T251" id="Seg_5577" s="T248">AkEE_19XX_BoySister_flk.051 (001.050)</ta>
            <ta e="T256" id="Seg_5578" s="T251">AkEE_19XX_BoySister_flk.052 (001.051)</ta>
            <ta e="T260" id="Seg_5579" s="T256">AkEE_19XX_BoySister_flk.053 (001.052)</ta>
            <ta e="T266" id="Seg_5580" s="T260">AkEE_19XX_BoySister_flk.054 (001.053)</ta>
            <ta e="T268" id="Seg_5581" s="T266">AkEE_19XX_BoySister_flk.055 (001.054)</ta>
            <ta e="T275" id="Seg_5582" s="T268">AkEE_19XX_BoySister_flk.056 (001.055)</ta>
            <ta e="T279" id="Seg_5583" s="T275">AkEE_19XX_BoySister_flk.057 (001.056)</ta>
            <ta e="T283" id="Seg_5584" s="T279">AkEE_19XX_BoySister_flk.058 (001.057)</ta>
            <ta e="T290" id="Seg_5585" s="T283">AkEE_19XX_BoySister_flk.059 (001.058)</ta>
            <ta e="T294" id="Seg_5586" s="T290">AkEE_19XX_BoySister_flk.060 (001.059)</ta>
            <ta e="T296" id="Seg_5587" s="T294">AkEE_19XX_BoySister_flk.061 (001.060)</ta>
            <ta e="T300" id="Seg_5588" s="T296">AkEE_19XX_BoySister_flk.062 (001.061)</ta>
            <ta e="T302" id="Seg_5589" s="T300">AkEE_19XX_BoySister_flk.063 (001.062)</ta>
            <ta e="T306" id="Seg_5590" s="T302">AkEE_19XX_BoySister_flk.064 (001.063)</ta>
            <ta e="T308" id="Seg_5591" s="T306">AkEE_19XX_BoySister_flk.065 (001.064)</ta>
            <ta e="T312" id="Seg_5592" s="T308">AkEE_19XX_BoySister_flk.066 (001.065)</ta>
            <ta e="T319" id="Seg_5593" s="T312">AkEE_19XX_BoySister_flk.067 (001.066)</ta>
            <ta e="T325" id="Seg_5594" s="T319">AkEE_19XX_BoySister_flk.068 (001.067)</ta>
            <ta e="T327" id="Seg_5595" s="T325">AkEE_19XX_BoySister_flk.069 (001.068)</ta>
            <ta e="T335" id="Seg_5596" s="T327">AkEE_19XX_BoySister_flk.070 (001.068)</ta>
            <ta e="T340" id="Seg_5597" s="T335">AkEE_19XX_BoySister_flk.071 (001.069)</ta>
            <ta e="T344" id="Seg_5598" s="T340">AkEE_19XX_BoySister_flk.072 (001.070)</ta>
            <ta e="T348" id="Seg_5599" s="T344">AkEE_19XX_BoySister_flk.073 (001.071)</ta>
            <ta e="T353" id="Seg_5600" s="T348">AkEE_19XX_BoySister_flk.074 (001.072)</ta>
            <ta e="T363" id="Seg_5601" s="T353">AkEE_19XX_BoySister_flk.075 (001.073)</ta>
            <ta e="T367" id="Seg_5602" s="T363">AkEE_19XX_BoySister_flk.076 (001.074)</ta>
            <ta e="T371" id="Seg_5603" s="T367">AkEE_19XX_BoySister_flk.077 (001.075)</ta>
            <ta e="T374" id="Seg_5604" s="T371">AkEE_19XX_BoySister_flk.078 (001.076)</ta>
            <ta e="T377" id="Seg_5605" s="T374">AkEE_19XX_BoySister_flk.079 (001.077)</ta>
            <ta e="T381" id="Seg_5606" s="T377">AkEE_19XX_BoySister_flk.080 (001.078)</ta>
            <ta e="T384" id="Seg_5607" s="T381">AkEE_19XX_BoySister_flk.081 (001.079)</ta>
            <ta e="T390" id="Seg_5608" s="T384">AkEE_19XX_BoySister_flk.082 (001.080)</ta>
            <ta e="T394" id="Seg_5609" s="T390">AkEE_19XX_BoySister_flk.083 (001.081)</ta>
            <ta e="T396" id="Seg_5610" s="T394">AkEE_19XX_BoySister_flk.084 (001.082)</ta>
            <ta e="T399" id="Seg_5611" s="T396">AkEE_19XX_BoySister_flk.085 (001.082)</ta>
            <ta e="T404" id="Seg_5612" s="T399">AkEE_19XX_BoySister_flk.086 (001.083)</ta>
            <ta e="T406" id="Seg_5613" s="T404">AkEE_19XX_BoySister_flk.087 (001.084)</ta>
            <ta e="T408" id="Seg_5614" s="T406">AkEE_19XX_BoySister_flk.088 (001.084)</ta>
            <ta e="T414" id="Seg_5615" s="T408">AkEE_19XX_BoySister_flk.089 (001.085)</ta>
            <ta e="T417" id="Seg_5616" s="T414">AkEE_19XX_BoySister_flk.090 (001.086)</ta>
            <ta e="T424" id="Seg_5617" s="T417">AkEE_19XX_BoySister_flk.091 (001.086)</ta>
            <ta e="T428" id="Seg_5618" s="T424">AkEE_19XX_BoySister_flk.092 (001.087)</ta>
            <ta e="T432" id="Seg_5619" s="T428">AkEE_19XX_BoySister_flk.093 (001.088)</ta>
            <ta e="T436" id="Seg_5620" s="T432">AkEE_19XX_BoySister_flk.094 (001.088)</ta>
            <ta e="T443" id="Seg_5621" s="T436">AkEE_19XX_BoySister_flk.095 (001.089)</ta>
            <ta e="T446" id="Seg_5622" s="T443">AkEE_19XX_BoySister_flk.096 (001.090)</ta>
            <ta e="T448" id="Seg_5623" s="T446">AkEE_19XX_BoySister_flk.097 (001.091)</ta>
            <ta e="T453" id="Seg_5624" s="T448">AkEE_19XX_BoySister_flk.098 (001.092)</ta>
            <ta e="T457" id="Seg_5625" s="T453">AkEE_19XX_BoySister_flk.099 (001.093)</ta>
            <ta e="T461" id="Seg_5626" s="T457">AkEE_19XX_BoySister_flk.100 (001.094)</ta>
            <ta e="T468" id="Seg_5627" s="T461">AkEE_19XX_BoySister_flk.101 (001.095)</ta>
            <ta e="T471" id="Seg_5628" s="T468">AkEE_19XX_BoySister_flk.102 (001.096)</ta>
            <ta e="T474" id="Seg_5629" s="T471">AkEE_19XX_BoySister_flk.103 (001.097)</ta>
            <ta e="T480" id="Seg_5630" s="T474">AkEE_19XX_BoySister_flk.104 (001.097)</ta>
            <ta e="T488" id="Seg_5631" s="T480">AkEE_19XX_BoySister_flk.105 (001.098)</ta>
            <ta e="T495" id="Seg_5632" s="T488">AkEE_19XX_BoySister_flk.106 (001.099)</ta>
            <ta e="T500" id="Seg_5633" s="T495">AkEE_19XX_BoySister_flk.107 (001.100)</ta>
            <ta e="T502" id="Seg_5634" s="T500">AkEE_19XX_BoySister_flk.108 (001.101)</ta>
            <ta e="T510" id="Seg_5635" s="T502">AkEE_19XX_BoySister_flk.109 (001.102)</ta>
            <ta e="T518" id="Seg_5636" s="T510">AkEE_19XX_BoySister_flk.110 (001.103)</ta>
            <ta e="T521" id="Seg_5637" s="T518">AkEE_19XX_BoySister_flk.111 (001.104)</ta>
            <ta e="T526" id="Seg_5638" s="T521">AkEE_19XX_BoySister_flk.112 (001.105)</ta>
            <ta e="T530" id="Seg_5639" s="T526">AkEE_19XX_BoySister_flk.113 (001.106)</ta>
            <ta e="T532" id="Seg_5640" s="T530">AkEE_19XX_BoySister_flk.114 (001.107)</ta>
            <ta e="T536" id="Seg_5641" s="T532">AkEE_19XX_BoySister_flk.115 (001.108)</ta>
            <ta e="T542" id="Seg_5642" s="T536">AkEE_19XX_BoySister_flk.116 (001.109)</ta>
            <ta e="T544" id="Seg_5643" s="T542">AkEE_19XX_BoySister_flk.117 (001.110)</ta>
            <ta e="T554" id="Seg_5644" s="T544">AkEE_19XX_BoySister_flk.118 (001.111)</ta>
            <ta e="T556" id="Seg_5645" s="T554">AkEE_19XX_BoySister_flk.119 (001.112)</ta>
            <ta e="T565" id="Seg_5646" s="T556">AkEE_19XX_BoySister_flk.120 (001.113)</ta>
            <ta e="T570" id="Seg_5647" s="T565">AkEE_19XX_BoySister_flk.121 (001.114)</ta>
            <ta e="T574" id="Seg_5648" s="T570">AkEE_19XX_BoySister_flk.122 (001.115)</ta>
            <ta e="T577" id="Seg_5649" s="T574">AkEE_19XX_BoySister_flk.123 (001.116)</ta>
            <ta e="T580" id="Seg_5650" s="T577">AkEE_19XX_BoySister_flk.124 (001.117)</ta>
            <ta e="T591" id="Seg_5651" s="T580">AkEE_19XX_BoySister_flk.125 (001.118)</ta>
            <ta e="T595" id="Seg_5652" s="T591">AkEE_19XX_BoySister_flk.126 (001.119)</ta>
            <ta e="T599" id="Seg_5653" s="T595">AkEE_19XX_BoySister_flk.127 (001.120)</ta>
            <ta e="T605" id="Seg_5654" s="T599">AkEE_19XX_BoySister_flk.128 (001.121)</ta>
            <ta e="T609" id="Seg_5655" s="T605">AkEE_19XX_BoySister_flk.129 (001.122)</ta>
            <ta e="T613" id="Seg_5656" s="T609">AkEE_19XX_BoySister_flk.130 (001.123)</ta>
            <ta e="T615" id="Seg_5657" s="T613">AkEE_19XX_BoySister_flk.131 (001.124)</ta>
            <ta e="T621" id="Seg_5658" s="T615">AkEE_19XX_BoySister_flk.132 (001.125)</ta>
            <ta e="T624" id="Seg_5659" s="T621">AkEE_19XX_BoySister_flk.133 (001.126)</ta>
            <ta e="T626" id="Seg_5660" s="T624">AkEE_19XX_BoySister_flk.134 (001.127)</ta>
            <ta e="T635" id="Seg_5661" s="T626">AkEE_19XX_BoySister_flk.135 (001.128)</ta>
            <ta e="T637" id="Seg_5662" s="T635">AkEE_19XX_BoySister_flk.136 (001.129)</ta>
            <ta e="T641" id="Seg_5663" s="T637">AkEE_19XX_BoySister_flk.137 (001.129)</ta>
            <ta e="T645" id="Seg_5664" s="T641">AkEE_19XX_BoySister_flk.138 (001.130)</ta>
            <ta e="T651" id="Seg_5665" s="T645">AkEE_19XX_BoySister_flk.139 (001.131)</ta>
            <ta e="T654" id="Seg_5666" s="T651">AkEE_19XX_BoySister_flk.140 (001.132)</ta>
            <ta e="T656" id="Seg_5667" s="T654">AkEE_19XX_BoySister_flk.141 (001.133)</ta>
            <ta e="T658" id="Seg_5668" s="T656">AkEE_19XX_BoySister_flk.142 (001.134)</ta>
            <ta e="T662" id="Seg_5669" s="T658">AkEE_19XX_BoySister_flk.143 (001.135)</ta>
            <ta e="T668" id="Seg_5670" s="T662">AkEE_19XX_BoySister_flk.144 (001.136)</ta>
            <ta e="T671" id="Seg_5671" s="T668">AkEE_19XX_BoySister_flk.145 (001.137)</ta>
            <ta e="T674" id="Seg_5672" s="T671">AkEE_19XX_BoySister_flk.146 (001.137)</ta>
            <ta e="T683" id="Seg_5673" s="T674">AkEE_19XX_BoySister_flk.147 (001.138)</ta>
            <ta e="T687" id="Seg_5674" s="T683">AkEE_19XX_BoySister_flk.148 (001.139)</ta>
            <ta e="T690" id="Seg_5675" s="T687">AkEE_19XX_BoySister_flk.149 (001.140)</ta>
            <ta e="T695" id="Seg_5676" s="T690">AkEE_19XX_BoySister_flk.150 (001.141)</ta>
            <ta e="T699" id="Seg_5677" s="T695">AkEE_19XX_BoySister_flk.151 (001.142)</ta>
            <ta e="T706" id="Seg_5678" s="T699">AkEE_19XX_BoySister_flk.152 (001.143)</ta>
            <ta e="T711" id="Seg_5679" s="T706">AkEE_19XX_BoySister_flk.153 (001.144)</ta>
            <ta e="T719" id="Seg_5680" s="T711">AkEE_19XX_BoySister_flk.154 (001.145)</ta>
            <ta e="T724" id="Seg_5681" s="T719">AkEE_19XX_BoySister_flk.155 (001.146)</ta>
            <ta e="T728" id="Seg_5682" s="T724">AkEE_19XX_BoySister_flk.156 (001.147)</ta>
            <ta e="T732" id="Seg_5683" s="T728">AkEE_19XX_BoySister_flk.157 (001.148)</ta>
            <ta e="T737" id="Seg_5684" s="T732">AkEE_19XX_BoySister_flk.158 (001.149)</ta>
            <ta e="T743" id="Seg_5685" s="T737">AkEE_19XX_BoySister_flk.159 (001.150)</ta>
            <ta e="T750" id="Seg_5686" s="T743">AkEE_19XX_BoySister_flk.160 (001.151)</ta>
            <ta e="T753" id="Seg_5687" s="T750">AkEE_19XX_BoySister_flk.161 (001.152)</ta>
            <ta e="T758" id="Seg_5688" s="T753">AkEE_19XX_BoySister_flk.162 (001.153)</ta>
            <ta e="T767" id="Seg_5689" s="T758">AkEE_19XX_BoySister_flk.163 (001.154)</ta>
            <ta e="T770" id="Seg_5690" s="T767">AkEE_19XX_BoySister_flk.164 (001.155)</ta>
            <ta e="T774" id="Seg_5691" s="T770">AkEE_19XX_BoySister_flk.165 (001.156)</ta>
            <ta e="T778" id="Seg_5692" s="T774">AkEE_19XX_BoySister_flk.166 (001.157)</ta>
            <ta e="T781" id="Seg_5693" s="T778">AkEE_19XX_BoySister_flk.167 (001.158)</ta>
            <ta e="T787" id="Seg_5694" s="T781">AkEE_19XX_BoySister_flk.168 (001.159)</ta>
            <ta e="T791" id="Seg_5695" s="T787">AkEE_19XX_BoySister_flk.169 (001.160)</ta>
            <ta e="T794" id="Seg_5696" s="T791">AkEE_19XX_BoySister_flk.170 (001.161)</ta>
            <ta e="T802" id="Seg_5697" s="T794">AkEE_19XX_BoySister_flk.171 (001.162)</ta>
            <ta e="T808" id="Seg_5698" s="T802">AkEE_19XX_BoySister_flk.172 (001.163)</ta>
            <ta e="T816" id="Seg_5699" s="T808">AkEE_19XX_BoySister_flk.173 (001.164)</ta>
            <ta e="T821" id="Seg_5700" s="T816">AkEE_19XX_BoySister_flk.174 (001.165)</ta>
            <ta e="T827" id="Seg_5701" s="T821">AkEE_19XX_BoySister_flk.175 (001.166)</ta>
            <ta e="T830" id="Seg_5702" s="T827">AkEE_19XX_BoySister_flk.176 (001.167)</ta>
            <ta e="T836" id="Seg_5703" s="T830">AkEE_19XX_BoySister_flk.177 (001.168)</ta>
            <ta e="T840" id="Seg_5704" s="T836">AkEE_19XX_BoySister_flk.178 (001.169)</ta>
            <ta e="T845" id="Seg_5705" s="T840">AkEE_19XX_BoySister_flk.179 (001.170)</ta>
            <ta e="T848" id="Seg_5706" s="T845">AkEE_19XX_BoySister_flk.180 (001.171)</ta>
            <ta e="T854" id="Seg_5707" s="T848">AkEE_19XX_BoySister_flk.181 (001.172)</ta>
            <ta e="T856" id="Seg_5708" s="T854">AkEE_19XX_BoySister_flk.182 (001.173)</ta>
            <ta e="T858" id="Seg_5709" s="T856">AkEE_19XX_BoySister_flk.183 (001.174)</ta>
            <ta e="T862" id="Seg_5710" s="T858">AkEE_19XX_BoySister_flk.184 (001.175)</ta>
            <ta e="T866" id="Seg_5711" s="T862">AkEE_19XX_BoySister_flk.185 (001.176)</ta>
            <ta e="T871" id="Seg_5712" s="T866">AkEE_19XX_BoySister_flk.186 (001.177)</ta>
            <ta e="T876" id="Seg_5713" s="T871">AkEE_19XX_BoySister_flk.187 (001.178)</ta>
            <ta e="T879" id="Seg_5714" s="T876">AkEE_19XX_BoySister_flk.188 (001.179)</ta>
            <ta e="T882" id="Seg_5715" s="T879">AkEE_19XX_BoySister_flk.189 (001.180)</ta>
            <ta e="T884" id="Seg_5716" s="T882">AkEE_19XX_BoySister_flk.190 (001.181)</ta>
            <ta e="T886" id="Seg_5717" s="T884">AkEE_19XX_BoySister_flk.191 (001.181)</ta>
            <ta e="T888" id="Seg_5718" s="T886">AkEE_19XX_BoySister_flk.192 (001.182)</ta>
            <ta e="T897" id="Seg_5719" s="T888">AkEE_19XX_BoySister_flk.193 (001.183)</ta>
            <ta e="T901" id="Seg_5720" s="T897">AkEE_19XX_BoySister_flk.194 (001.184)</ta>
            <ta e="T912" id="Seg_5721" s="T901">AkEE_19XX_BoySister_flk.195 (001.185)</ta>
            <ta e="T916" id="Seg_5722" s="T912">AkEE_19XX_BoySister_flk.196 (001.186)</ta>
            <ta e="T920" id="Seg_5723" s="T916">AkEE_19XX_BoySister_flk.197 (001.187)</ta>
            <ta e="T933" id="Seg_5724" s="T920">AkEE_19XX_BoySister_flk.198 (001.188)</ta>
            <ta e="T939" id="Seg_5725" s="T933">AkEE_19XX_BoySister_flk.199 (001.189)</ta>
            <ta e="T946" id="Seg_5726" s="T939">AkEE_19XX_BoySister_flk.200 (001.190)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T8" id="Seg_5727" s="T1">Иһиллээӈ дулгааннар олоӈколорун "Эдьиийдээк уол" диэн ааттаагы.</ta>
            <ta e="T13" id="Seg_5728" s="T8">Уол эдьиийин кытта (гытта) иккиэйккээн олорбуттара. </ta>
            <ta e="T17" id="Seg_5729" s="T13">Эдьиийэ үлэһиттэээк уус багайы.</ta>
            <ta e="T22" id="Seg_5730" s="T17">Һэтиилэрэ бугдьии таба тириилэринэн һабыылаактар.</ta>
            <ta e="T26" id="Seg_5731" s="T22">Ураһа диэни бэйэтэ туттааччы. </ta>
            <ta e="T30" id="Seg_5732" s="T26">Табаларын да һайын кэтээччи.</ta>
            <ta e="T37" id="Seg_5733" s="T30">Уол балтыта күһүӈӈүттэн утуйбут күн таксыар диэри.</ta>
            <ta e="T41" id="Seg_5734" s="T37">Үөрдэрэ ырааппыт, оӈколоро кэӈээбит. </ta>
            <ta e="T44" id="Seg_5735" s="T41">Кыыс оӈко уларыттыыһы.</ta>
            <ta e="T49" id="Seg_5736" s="T44">Үөрүн эгэллэ, таба тутунна, көлүннэ. </ta>
            <ta e="T52" id="Seg_5737" s="T49">Балтытын уһугуннара һатыыр. </ta>
            <ta e="T54" id="Seg_5738" s="T52">Аматтан уһуктубат. </ta>
            <ta e="T63" id="Seg_5739" s="T54">Ураһа дьиэтин кастаан бараан, балтытын кырса һуорганын туура тарта.</ta>
            <ta e="T66" id="Seg_5740" s="T63">Өрүкүйэн уһугунна, таӈынна. </ta>
            <ta e="T70" id="Seg_5741" s="T66">Һыргатыгар олороот эмиэ утуйда.</ta>
            <ta e="T77" id="Seg_5742" s="T70">Мастанарыгар, буустанарыгар, табаланарыгар буолан кыыс бэрт һылайар.</ta>
            <ta e="T83" id="Seg_5743" s="T77">Һылайан утуйбут, утуйа һытан тоӈон каалбыт.</ta>
            <ta e="T89" id="Seg_5744" s="T83">Уол балтыта өссүө үс күнү утуйбут.</ta>
            <ta e="T94" id="Seg_5745" s="T89">Уһуктубута, көрбүтэ, эдьиийэ тоӈон каалбыт. </ta>
            <ta e="T98" id="Seg_5746" s="T94">Үөрэ барыта тоӈон өлбүт.</ta>
            <ta e="T100" id="Seg_5747" s="T98">Һобуой оксубут. </ta>
            <ta e="T111" id="Seg_5748" s="T100">"Уот оттуннакпына, бугаат, эдьиийим тиллиэ да, -- дии һанаабыт уол, --мастана барыакка".</ta>
            <ta e="T117" id="Seg_5749" s="T111">Кайалаак үрэк үрдүнэн мас диэк һүүрбүт.</ta>
            <ta e="T123" id="Seg_5750" s="T117">Көрбүтэ, каллаантан тутуктаак отуу гыдаарыйа һытар.</ta>
            <ta e="T128" id="Seg_5751" s="T123">Абааһы (үчүһэгэ?) эт үөлэнэ олорор. </ta>
            <ta e="T130" id="Seg_5752" s="T128">Уолу көрбөт.</ta>
            <ta e="T137" id="Seg_5753" s="T130">Бу уол һулубурата тыыта-тыыта оӈуоргуттан һии олорбут.</ta>
            <ta e="T145" id="Seg_5754" s="T137">"Кайтак (кайдак) туолкулаабатын киэ, -- диир Абааһы, -- эттэрим уокка түһэллэрин?"</ta>
            <ta e="T150" id="Seg_5755" s="T145">Абааһы уолу көрөн өлөрөөрү гыммыт.</ta>
            <ta e="T157" id="Seg_5756" s="T150">Уол уруукатынан Абааһы агыс баһын туура оксубут. </ta>
            <ta e="T160" id="Seg_5757" s="T157">Бастара үӈкүлүү һылдьаллар.</ta>
            <ta e="T163" id="Seg_5758" s="T160">Уол онно часкыйбыт: </ta>
            <ta e="T167" id="Seg_5759" s="T163">"Букатыыр иккилээччитэ, үһүстээччитэ һуок!"</ta>
            <ta e="T173" id="Seg_5760" s="T167">Абааһы түөһүн тыыра тардан, тааһы ороото. </ta>
            <ta e="T176" id="Seg_5761" s="T173">Таас һүрэктээк эбит.</ta>
            <ta e="T181" id="Seg_5762" s="T176">Уол тааһы тэбэ-тэбэ бара турбут. </ta>
            <ta e="T189" id="Seg_5763" s="T181">Иннитигэр һир аӈардаак һага таас турбут. Таас кайа.</ta>
            <ta e="T194" id="Seg_5764" s="T189">Таас кайаны таас һүрэгинэн тэппит. </ta>
            <ta e="T199" id="Seg_5765" s="T194">Кайата икки аӈы кайдан түспүт. </ta>
            <ta e="T203" id="Seg_5766" s="T199">Одуулаан көрбүтэ, һир көстөр.</ta>
            <ta e="T208" id="Seg_5767" s="T203">Онно баран һыргалаак һуолун көрбүт. </ta>
            <ta e="T215" id="Seg_5768" s="T208">Паас, капкаан көрүнэ һылдьар киһи этэ эбит.</ta>
            <ta e="T222" id="Seg_5769" s="T215">Төһө да буолбакка һырсаһыт утары иһэрин көрбүт. </ta>
            <ta e="T225" id="Seg_5770" s="T222">Кэлэн турда, дорооболосто.</ta>
            <ta e="T230" id="Seg_5771" s="T225">"Туугу гына һылдьагын?" -- ыыппыт уол. </ta>
            <ta e="T233" id="Seg_5772" s="T230">"Паас, капкаан көрүнэбин." </ta>
            <ta e="T236" id="Seg_5773" s="T233">"Ыраак дьиэлээккит дуо? </ta>
            <ta e="T238" id="Seg_5774" s="T236">Кас киһигитий?"</ta>
            <ta e="T244" id="Seg_5775" s="T238">"Иньэбин, агабын кытта (гытта) олоробун,"-- диэбит уол. </ta>
            <ta e="T248" id="Seg_5776" s="T244">"Мэӈэһин, иллиэм. Ыалдьыт буол".</ta>
            <ta e="T251" id="Seg_5777" s="T248">Ураһа дьиэтигэр эгэлбит.</ta>
            <ta e="T256" id="Seg_5778" s="T251">Огонньуордоок эмээксин утары таксаннар көрсүбүттэр. </ta>
            <ta e="T260" id="Seg_5779" s="T256">"Кайа һиртэн кэллиӈ?" -- ыыппыттар. </ta>
            <ta e="T266" id="Seg_5780" s="T260">"Манна туора карактаак, уот уллуӈнаак үктэнэл илик." </ta>
            <ta e="T268" id="Seg_5781" s="T266">Уол кэпсиэбит:</ta>
            <ta e="T275" id="Seg_5782" s="T268">"Эдьиийим тоӈон өллө, табаларбыт эмиэ тоӈон өллүлэр. </ta>
            <ta e="T279" id="Seg_5783" s="T275">Каар иһигэр һыталлар тибиллэ. </ta>
            <ta e="T283" id="Seg_5784" s="T279">Кайтак (кайдак) киһи буолуом дьүрү?"</ta>
            <ta e="T290" id="Seg_5785" s="T283">"Маӈнай ыалдьыт буол, -- диэбит огонньоор, -- онтон һүбэлиһиэппит".</ta>
            <ta e="T294" id="Seg_5786" s="T290">Уола таксан тыһыны өлөрбүт. </ta>
            <ta e="T296" id="Seg_5787" s="T294">Тастаак тыһыыны. </ta>
            <ta e="T300" id="Seg_5788" s="T296">Ыалдьыт ону һоготогун һиэбит.</ta>
            <ta e="T302" id="Seg_5789" s="T300">"Тоттуӈ?" -- ыыппыттар. </ta>
            <ta e="T306" id="Seg_5790" s="T302">"Тоттум, күһүӈӈүттэн аһыы иликпин. </ta>
            <ta e="T308" id="Seg_5791" s="T306">Аны һынньаныам".</ta>
            <ta e="T312" id="Seg_5792" s="T308">Үс күнү уол утуйбут.</ta>
            <ta e="T319" id="Seg_5793" s="T312">Уһуктубутугар огонньор мууӈ эмис табаны өлөрөн һиэппит.</ta>
            <ta e="T325" id="Seg_5794" s="T319">Огонньор үс ыйдаагы аһы бэлэмнээбит, барыагын.</ta>
            <ta e="T327" id="Seg_5795" s="T325">Уол диэбит: </ta>
            <ta e="T335" id="Seg_5796" s="T327">"Канна ирэ һир иччитэ баар үһү. Онно барыам".</ta>
            <ta e="T340" id="Seg_5797" s="T335">Уол һыргатыгар таас һүрэги уурда. </ta>
            <ta e="T344" id="Seg_5798" s="T340">Үрэк иһинэн камнаан каалла.</ta>
            <ta e="T348" id="Seg_5799" s="T344">Төһө өрдүк барбыта дьүрү? </ta>
            <ta e="T353" id="Seg_5800" s="T348">Арай, чугуун ураһа дьиэни көрбүт.</ta>
            <ta e="T363" id="Seg_5801" s="T353">Ааны арыйан көрдө -- кырдьагас багайы эмээксин олорор, аттытыгар күөрт һытар.</ta>
            <ta e="T367" id="Seg_5802" s="T363">Мэньиитин үрдүтүгэр матага (матака) ыйааллыбыт. </ta>
            <ta e="T371" id="Seg_5803" s="T367">Матага киһи һырайын майгылаак.</ta>
            <ta e="T374" id="Seg_5804" s="T371">Ачаак анныта көӈдөй. </ta>
            <ta e="T377" id="Seg_5805" s="T374">Үрэк уста һытар. </ta>
            <ta e="T381" id="Seg_5806" s="T377">Үрэккэ тыылаак илимнэнэ һылдьар.</ta>
            <ta e="T384" id="Seg_5807" s="T381">Матага һаӈалаак буолбут: </ta>
            <ta e="T390" id="Seg_5808" s="T384">"Ыалдьыт кэллэ. Маччыттар, мастааӈ, уотта оттуӈ!"</ta>
            <ta e="T394" id="Seg_5809" s="T390">Мастара бэйэлэрэ киирдилэр таһаараттан. </ta>
            <ta e="T396" id="Seg_5810" s="T394">Уол туолкулаата: </ta>
            <ta e="T399" id="Seg_5811" s="T396">"Тимир мастар эбит".</ta>
            <ta e="T404" id="Seg_5812" s="T399">Эмээксин туран мастары ачаагыгар муста.</ta>
            <ta e="T406" id="Seg_5813" s="T404">Матага диэтэ: </ta>
            <ta e="T408" id="Seg_5814" s="T406">"Күөрт, күөртэ!"</ta>
            <ta e="T414" id="Seg_5815" s="T408">Урага дьиэ тимир муостата кытарчы барда. </ta>
            <ta e="T417" id="Seg_5816" s="T414">Матага биэк диир: </ta>
            <ta e="T424" id="Seg_5817" s="T417">"Күөрт, күөртэ, күөртэ. Ыалдьыт кэллэ, уотта эп!"</ta>
            <ta e="T428" id="Seg_5818" s="T424">Ураһа дьиэ барыта кытарда. </ta>
            <ta e="T432" id="Seg_5819" s="T428">Эмээксин ыалдьыт уолу көрдөстө: </ta>
            <ta e="T436" id="Seg_5820" s="T432">"Ититимэ олус, һойут диэ".</ta>
            <ta e="T443" id="Seg_5821" s="T436">Уол таас һүрэги ылан ачаак ортотугар быракта. </ta>
            <ta e="T446" id="Seg_5822" s="T443">Ураһа дьиэ һойдо. </ta>
            <ta e="T448" id="Seg_5823" s="T446">Муостата кытарбата.</ta>
            <ta e="T453" id="Seg_5824" s="T448">Эмэксин эмиэ кам кырыа буолла. </ta>
            <ta e="T457" id="Seg_5825" s="T453">"Олус тоӈорума!" -- көрдөспүт Матага.</ta>
            <ta e="T461" id="Seg_5826" s="T457">Матага эбит һир иччитэ.</ta>
            <ta e="T468" id="Seg_5827" s="T461">Уол таас һүрэги ачаак иһиттэн ороон ылла. </ta>
            <ta e="T471" id="Seg_5828" s="T468">Уота эмиэ гыдаарыйда.</ta>
            <ta e="T474" id="Seg_5829" s="T471">Матага диэтэ уолга: </ta>
            <ta e="T480" id="Seg_5830" s="T474">"Таһаара тагыс, онно һыргалаак таба турар".</ta>
            <ta e="T488" id="Seg_5831" s="T480">Уол таһаара таксан, туок да һыргалаак табатын көрбөт. </ta>
            <ta e="T495" id="Seg_5832" s="T488">Канна эрэ чуораан, губаарка эрэ тыаһа иһиллэр.</ta>
            <ta e="T500" id="Seg_5833" s="T495">Кэтэгэн диэк Матага һаӈата иһиллибит: </ta>
            <ta e="T502" id="Seg_5834" s="T500">"Олор һыргагар. </ta>
            <ta e="T510" id="Seg_5835" s="T502">Кэӈниӈ (кэнниӈ) диэк каньыһыма, һуол аара токтоома, табаларгын аһатыма". </ta>
            <ta e="T518" id="Seg_5836" s="T510">Уол илиитин уунна ньуӈууну капта, көстүбэт һыргага мииннэ. </ta>
            <ta e="T521" id="Seg_5837" s="T518">Тыаллаак пургаа огуста.</ta>
            <ta e="T526" id="Seg_5838" s="T521">Каччаны барбыта дьүрү, ким билиэй? </ta>
            <ta e="T530" id="Seg_5839" s="T526">Һыргалаак табалара бэйэлэрэ иллэллэр. </ta>
            <ta e="T532" id="Seg_5840" s="T530">Уол томмот.</ta>
            <ta e="T536" id="Seg_5841" s="T532">Илиитин олбогун иһигэр укта. </ta>
            <ta e="T542" id="Seg_5842" s="T536">Онно итии тимир мас һытар эбит. </ta>
            <ta e="T544" id="Seg_5843" s="T542">Онтута ититэр, онтута ититэр. </ta>
            <ta e="T554" id="Seg_5844" s="T544">Эбэ үрдүгэр көрдө, һыргага өлбүт дьактар һытар, кам тибиллэн бараан.</ta>
            <ta e="T556" id="Seg_5845" s="T554">Тэһийбэккэ токтоото. </ta>
            <ta e="T565" id="Seg_5846" s="T556">Токтуурун кытта (гытта) ньуӈууһута һулбу ыстанан, кайдиэк барбыта, кайдиэк кэлбитэ?</ta>
            <ta e="T570" id="Seg_5847" s="T565">Ус комномуой табалаак этэ эбит. </ta>
            <ta e="T574" id="Seg_5848" s="T570">Ньуӈууһута быстан баран каалбыт. </ta>
            <ta e="T577" id="Seg_5849" s="T574">Матага һаӈата иһиллибит: </ta>
            <ta e="T580" id="Seg_5850" s="T577">"Эниэкэ дьиэбитим, токтоомо!"</ta>
            <ta e="T591" id="Seg_5851" s="T580">Уол һыргатыттан кыһыл тимир маһы ылан, кыыс һыргатын улкатыгар (улакаатыгар) батары аста. </ta>
            <ta e="T595" id="Seg_5852" s="T591">Каар һонно ирэн каалла.</ta>
            <ta e="T599" id="Seg_5853" s="T595">Тоӈон өлбүт дьактар тилиннэ. </ta>
            <ta e="T605" id="Seg_5854" s="T599">Көрбүтэ, бэрт боскуой багай кыыс эбит.</ta>
            <ta e="T613" id="Seg_5855" s="T609">"Аны бэйэм дьиэбэр тийиэм. </ta>
            <ta e="T615" id="Seg_5856" s="T613">Барсыбаккын дуо?</ta>
            <ta e="T621" id="Seg_5857" s="T615">Аһатыам, ичигэс тэллэккэ тэлгиэм",-- көрдөспүт кыыс.</ta>
            <ta e="T624" id="Seg_5858" s="T621">"Ыраак дуо дьиэӈ?" </ta>
            <ta e="T626" id="Seg_5859" s="T624">"Уу иһигэр".</ta>
            <ta e="T635" id="Seg_5860" s="T626">Уол үгүс одууну көрбүтэ. Ол иһин кыыс өһүн һөкпөтө.</ta>
            <ta e="T637" id="Seg_5861" s="T635">Дитэбит: </ta>
            <ta e="T641" id="Seg_5862" s="T637">"Мин эдьиийбин булуокпун наада. </ta>
            <ta e="T645" id="Seg_5863" s="T641">Койут кэлиэм ээт. Күүт".</ta>
            <ta e="T651" id="Seg_5864" s="T645">Уол төһөнү-каччаны барбыта, ким да билбэт.</ta>
            <ta e="T654" id="Seg_5865" s="T651">Арай кадьырык буолбут. </ta>
            <ta e="T656" id="Seg_5866" s="T654">Аһыылыктаак багайы. </ta>
            <ta e="T658" id="Seg_5867" s="T656">Табалара аччыктаатылар.</ta>
            <ta e="T662" id="Seg_5868" s="T658">"Аһатыакка дуу?" -- дии һанаабыт. </ta>
            <ta e="T668" id="Seg_5869" s="T662">Һир иччитин һаӈатын өйдүү-өйдүү табаларын токтокпото.</ta>
            <ta e="T671" id="Seg_5870" s="T668">Онтон дии һанаата: </ta>
            <ta e="T674" id="Seg_5871" s="T671">"Олус аччыктаабыттар, токтотуокка".</ta>
            <ta e="T683" id="Seg_5872" s="T674">Токтоторун кытта (гытта), ортокуу көлүүр табата мэлис эрэ гынан каалбыт. </ta>
            <ta e="T687" id="Seg_5873" s="T683">Кайдиэк барбыта, кайдиэк көппүтэ?</ta>
            <ta e="T690" id="Seg_5874" s="T687">Эмиэ һаӈа иһиллибит. </ta>
            <ta e="T695" id="Seg_5875" s="T690">"Мин эниэкэ диэбитим, аһатыма табаларгын".</ta>
            <ta e="T699" id="Seg_5876" s="T695">Уол төһөнү барбыта дьүрү? </ta>
            <ta e="T706" id="Seg_5877" s="T699">Маа (баадьыгын) таас кайатыгар тийдэ, икки аӈы кайдааччытыгар.</ta>
            <ta e="T711" id="Seg_5878" s="T706">Арай бэйэтин һиригэр тийбит эбит.</ta>
            <ta e="T719" id="Seg_5879" s="T711">Тоӈон өлбүт табалар муостара араччы каар иһиттэн чороһоллор.</ta>
            <ta e="T724" id="Seg_5880" s="T719">Каары күрдьэн эдьийин ньуоӈууһутун булла. </ta>
            <ta e="T728" id="Seg_5881" s="T724">Итии тимир маһынан таарыйда.</ta>
            <ta e="T732" id="Seg_5882" s="T728">Табата тиллэн бараан турда. </ta>
            <ta e="T737" id="Seg_5883" s="T732">Онтон маһы ылла каарга аста.</ta>
            <ta e="T743" id="Seg_5884" s="T737">Тоӈон өлбүт табалар һонно барылара тилиннилэр. </ta>
            <ta e="T750" id="Seg_5885" s="T743">Бүтүн үөр буолан аһыы һылдьаллар һаӈа һиринэн.</ta>
            <ta e="T753" id="Seg_5886" s="T750">Каньыспыта, эдьиийэ турар. </ta>
            <ta e="T758" id="Seg_5887" s="T753">Уол эдьиийин көрөн бэрт үөрдэ.</ta>
            <ta e="T767" id="Seg_5888" s="T758">"Маӈнай эн, -- диэтэ эдьиийэ, -- өр багайдык утубутуӈ, онтон мин. </ta>
            <ta e="T770" id="Seg_5889" s="T767">Һаас буолбут эбит. </ta>
            <ta e="T774" id="Seg_5890" s="T770">Кыһыны мэлдьи утуйбуппун эбит".</ta>
            <ta e="T778" id="Seg_5891" s="T774">Уол эдьиийин дьиэтигэр илтэ. </ta>
            <ta e="T781" id="Seg_5892" s="T778">Үөрүн үүрэн эгэллэ.</ta>
            <ta e="T787" id="Seg_5893" s="T781">Төһө да буолбакка бу уол тэриннэ. </ta>
            <ta e="T791" id="Seg_5894" s="T787">"Кайдиэк барагын?" -- ыыппыт эдьиийэ.</ta>
            <ta e="T794" id="Seg_5895" s="T791">"Һотору кэлиэм, кэчэһимэ".</ta>
            <ta e="T802" id="Seg_5896" s="T794">Уол маа (баадьыгы) кыыһы көрдүү барыыһы, уу иһигэр дьиэлээги.</ta>
            <ta e="T808" id="Seg_5897" s="T802">Баран иһэн мылаа багай пургаа буолбут. </ta>
            <ta e="T816" id="Seg_5898" s="T808">Һиринэн да барарын билбэт,бууһунан да барарын билбэт.</ta>
            <ta e="T821" id="Seg_5899" s="T816">Өйүлээн көрбүтэ, байгал бууһа эбит. </ta>
            <ta e="T827" id="Seg_5900" s="T821">Һааскы буус (бууска) иӈнэн уол ууга түспүт.</ta>
            <ta e="T830" id="Seg_5901" s="T827">Түһэн һордоӈ буолбут. </ta>
            <ta e="T836" id="Seg_5902" s="T830">Уу иһинэн уста һылдьан илимӈэ түбэспит.</ta>
            <ta e="T840" id="Seg_5903" s="T836">Иэӈнээк илимӈэ һордоӈнар агай.</ta>
            <ta e="T845" id="Seg_5904" s="T840">Төһө да буолбакка кыыс кэллэ. </ta>
            <ta e="T848" id="Seg_5905" s="T845">Маа (баайдьыгын) кыыһа, тилиннэрээччитэ.</ta>
            <ta e="T854" id="Seg_5906" s="T848">Һордоӈ уолу ньуоӈууһут гынан бараан көлүннэ. </ta>
            <ta e="T856" id="Seg_5907" s="T854">Дьиэтигэр барыыһы.</ta>
            <ta e="T858" id="Seg_5908" s="T856">Ньуоӈууһут истибэт. </ta>
            <ta e="T862" id="Seg_5909" s="T858">Кытыл эрэ диэк талаһар. </ta>
            <ta e="T866" id="Seg_5910" s="T862">Кыыс ону һалайа һатыыр.</ta>
            <ta e="T871" id="Seg_5911" s="T866">Ньуоӈууһута быһа мөккүйэн кытыылга тагыста. </ta>
            <ta e="T876" id="Seg_5912" s="T871">Таксаат уол-һордоӈ илэ уол буолбут.</ta>
            <ta e="T879" id="Seg_5913" s="T876">Һордоӈнор таба буоллулар. </ta>
            <ta e="T882" id="Seg_5914" s="T879">Кыстаак уол тайыстылар.</ta>
            <ta e="T884" id="Seg_5915" s="T882">Кыыс ыытта: </ta>
            <ta e="T886" id="Seg_5916" s="T884">"Эдьиийгин буллуӈ? </ta>
            <ta e="T888" id="Seg_5917" s="T886">Кайтак (кайдак) олорогун?"</ta>
            <ta e="T897" id="Seg_5918" s="T888">"Барыс минигит кытта (гытта), -- диэтэ уол, -- эдьиийим энигин күүтэр, билсиһиӈ".</ta>
            <ta e="T901" id="Seg_5919" s="T897">Бииргэ олоруокпут, үөрбүтүн колбуокпут".</ta>
            <ta e="T912" id="Seg_5920" s="T901">Уолу кытта (гытта) кыыс һордоӈнору уу иһиттэн ороотулар, онтулара бугдьии таба буоллулар.</ta>
            <ta e="T916" id="Seg_5921" s="T912">Уол эдьиийэ үөрэн көрүстэ. </ta>
            <ta e="T920" id="Seg_5922" s="T916">Уол үс күнү утуйда.</ta>
            <ta e="T933" id="Seg_5923" s="T920">Уһуктан көрбүтэ: эдьиийэ кыыһы гытта кэпсэтэ- кэпсэтэ күлсэ олороллор, чаай иһэллэр, буспут эти һииллэр.</ta>
            <ta e="T939" id="Seg_5924" s="T933">Уол улкаларыгар (улакааларыгар) олордо, бүтүн табаны һиэтэ. </ta>
            <ta e="T946" id="Seg_5925" s="T939">Уол кыыһы дьактар гыммыт. Дьоллоок багайтык олорбуттар.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T8" id="Seg_5926" s="T1">Ihilleːŋ dulgaːnnar oloŋkolorun "Edʼiːjdeːk u͡ol" di͡en aːttaːgɨ. </ta>
            <ta e="T13" id="Seg_5927" s="T8">U͡ol edʼiːjin kɨtta ikki͡ejkeːn olorbuttara. </ta>
            <ta e="T17" id="Seg_5928" s="T13">Edʼiːje ülehitteːk uːs bagajɨ. </ta>
            <ta e="T22" id="Seg_5929" s="T17">Hetiːlere bugdiː taba tiriːlerinen habɨːlaːktar. </ta>
            <ta e="T26" id="Seg_5930" s="T22">Uraha dʼi͡eni bejete tuttaːččɨ. </ta>
            <ta e="T30" id="Seg_5931" s="T26">Tabalarɨn da hajɨn keteːčči. </ta>
            <ta e="T37" id="Seg_5932" s="T30">U͡ol baltɨta kühüŋŋütten utujbut kün taksɨ͡ar di͡eri. </ta>
            <ta e="T41" id="Seg_5933" s="T37">Ü͡ördere ɨraːppɨt, oŋkoloro keŋeːbit. </ta>
            <ta e="T44" id="Seg_5934" s="T41">Kɨːs oŋko ularɨtɨːhɨ. </ta>
            <ta e="T49" id="Seg_5935" s="T44">Ü͡örün egelle, taba tutunna, kölünne. </ta>
            <ta e="T52" id="Seg_5936" s="T49">Baltɨtɨn uhugunnara hatɨːr. </ta>
            <ta e="T54" id="Seg_5937" s="T52">Amattan uhuktubat. </ta>
            <ta e="T63" id="Seg_5938" s="T54">Uraha dʼi͡etin kastaːn baraːn, baltɨtɨn kɨrsa hu͡organɨn tuːra tarta. </ta>
            <ta e="T66" id="Seg_5939" s="T63">Örüküjen uhugunna, taŋɨnna. </ta>
            <ta e="T70" id="Seg_5940" s="T66">Hɨrgatɨgar oloroːt emi͡e utujda. </ta>
            <ta e="T77" id="Seg_5941" s="T70">Mastanarɨgar, buːstanarɨgar, tabalanarɨgar bu͡olan kɨːs bert hɨlajar. </ta>
            <ta e="T83" id="Seg_5942" s="T77">Hɨlajan utujbut, utuja hɨtan toŋon kaːlbɨt. </ta>
            <ta e="T89" id="Seg_5943" s="T83">U͡ol baltɨta össü͡ö üs künü utujbut. </ta>
            <ta e="T94" id="Seg_5944" s="T89">Uhuktubuta, körbüte, edʼiːje toŋon kaːlbɨt. </ta>
            <ta e="T98" id="Seg_5945" s="T94">Ü͡öre barɨta toŋon ölbüt. </ta>
            <ta e="T100" id="Seg_5946" s="T98">Hobu͡oj oksubut. </ta>
            <ta e="T111" id="Seg_5947" s="T100">"U͡ot otunnakpɨna, bugaːt, edʼiːjim tilli͡e da", diː hanaːbɨt u͡ol, "mastana barɨ͡akka." </ta>
            <ta e="T117" id="Seg_5948" s="T111">Kajalaːk ürek ürdünen mas di͡ek hüːrbüt. </ta>
            <ta e="T123" id="Seg_5949" s="T117">Körbüte, kallaːntan tutuktaːk otuː gɨdaːrɨja hɨtar. </ta>
            <ta e="T128" id="Seg_5950" s="T123">Abaːhɨ üčehege et ü͡ölene oloror. </ta>
            <ta e="T130" id="Seg_5951" s="T128">U͡olu körböt. </ta>
            <ta e="T137" id="Seg_5952" s="T130">Bu u͡ol hulburuta tɨːta-tɨːta oŋu͡orguttan hiː olorbut. </ta>
            <ta e="T145" id="Seg_5953" s="T137">"Kajtak tu͡olkulaːbatɨn ke", diːr Abaːhɨ, "etterim u͡okka tühellerin?" </ta>
            <ta e="T150" id="Seg_5954" s="T145">Abaːhɨ u͡olu körön ölöröːrü gɨmmɨt. </ta>
            <ta e="T157" id="Seg_5955" s="T150">U͡ol uruːkatɨnan Abaːhɨ agɨs bahɨn tuːra oksubut. </ta>
            <ta e="T160" id="Seg_5956" s="T157">Bastara üŋküːlüː hɨldʼallar. </ta>
            <ta e="T163" id="Seg_5957" s="T160">U͡ol onno časkɨjbɨt: </ta>
            <ta e="T167" id="Seg_5958" s="T163">"Bukatɨːr ikkileːččite, ühüsteːččite hu͡ok!" </ta>
            <ta e="T173" id="Seg_5959" s="T167">Abaːhɨ tü͡öhün tɨːra tardan, taːhɨ oroːto. </ta>
            <ta e="T176" id="Seg_5960" s="T173">Taːs hürekteːk ebit. </ta>
            <ta e="T181" id="Seg_5961" s="T176">U͡ol taːhɨ tebe-tebe bara turbut. </ta>
            <ta e="T189" id="Seg_5962" s="T181">Innitiger hir aŋardaːk haga taːs turbut, taːs kaja. </ta>
            <ta e="T194" id="Seg_5963" s="T189">Taːs kajanɨ taːs hüreginen teppit. </ta>
            <ta e="T199" id="Seg_5964" s="T194">Kajata ikki aŋɨ kajdan tüspüt. </ta>
            <ta e="T203" id="Seg_5965" s="T199">Oduːlaːn körbüte, hir köstör. </ta>
            <ta e="T208" id="Seg_5966" s="T203">Onno baran hɨrgalaːk hu͡olun körbüt. </ta>
            <ta e="T215" id="Seg_5967" s="T208">Paːs, kapkaːn körüne hɨldʼar kihi ete ebit. </ta>
            <ta e="T222" id="Seg_5968" s="T215">Töhö da bu͡olbakka kɨrsahɨt utarɨ iherin körbüt. </ta>
            <ta e="T225" id="Seg_5969" s="T222">Kelen turda, doroːbolosto. </ta>
            <ta e="T230" id="Seg_5970" s="T225">"Tuːgu gɨna hɨldʼagɨn", ɨjɨppɨt u͡ol. </ta>
            <ta e="T233" id="Seg_5971" s="T230">"Paːs, kapkaːn körünebin." </ta>
            <ta e="T236" id="Seg_5972" s="T233">"ɨraːk dʼi͡eleːkkit du͡o? </ta>
            <ta e="T238" id="Seg_5973" s="T236">Kas kihigitij?" </ta>
            <ta e="T244" id="Seg_5974" s="T238">"Inʼebin, agabɨn kɨtta olorobun", di͡ebit u͡ol. </ta>
            <ta e="T248" id="Seg_5975" s="T244">"Meŋehin, illi͡em, ɨ͡aldʼɨt bu͡ol." </ta>
            <ta e="T251" id="Seg_5976" s="T248">Uraha dʼi͡etiger egelbit. </ta>
            <ta e="T256" id="Seg_5977" s="T251">Ogonnʼordoːk emeːksin utarɨ taksannar körsübütter. </ta>
            <ta e="T260" id="Seg_5978" s="T256">"Kaja hirten kelliŋ", ɨjɨppɨttar. </ta>
            <ta e="T266" id="Seg_5979" s="T260">"Manna tu͡ora karaktaːk, u͡ot ulluŋnaːk üktenelik." </ta>
            <ta e="T268" id="Seg_5980" s="T266">U͡ol kepseːbit: </ta>
            <ta e="T275" id="Seg_5981" s="T268">"Edʼiːjim toŋon öllö, tabalarbɨt emi͡e toŋon öllüler. </ta>
            <ta e="T279" id="Seg_5982" s="T275">Kaːr ihiger hɨtallar tibille. </ta>
            <ta e="T283" id="Seg_5983" s="T279">Kajtak kihi bu͡olu͡om dʼürü?" </ta>
            <ta e="T290" id="Seg_5984" s="T283">"Maŋnaj ɨ͡aldʼɨt bu͡ol", di͡ebit ogonnʼor, "onton hübelehi͡ekpit." </ta>
            <ta e="T294" id="Seg_5985" s="T290">U͡ola taksan tɨhɨnɨ ölörbüt. </ta>
            <ta e="T296" id="Seg_5986" s="T294">Tastaːk tɨhɨnɨ. </ta>
            <ta e="T300" id="Seg_5987" s="T296">ɨ͡aldʼɨt onu hogotogun hi͡ebit. </ta>
            <ta e="T302" id="Seg_5988" s="T300">"Tottuŋ", ɨjɨppɨttar. </ta>
            <ta e="T306" id="Seg_5989" s="T302">"Tottum, kühüŋŋütten ahɨː ilikpin. </ta>
            <ta e="T308" id="Seg_5990" s="T306">Anɨ hɨnnʼanɨ͡am." </ta>
            <ta e="T312" id="Seg_5991" s="T308">Üs künü u͡ol utujbut. </ta>
            <ta e="T319" id="Seg_5992" s="T312">Uhuktubutugar ogonnʼor muŋ emis tabanɨ ölörön hi͡ekpit. </ta>
            <ta e="T325" id="Seg_5993" s="T319">Ogonnʼor üs ɨjdaːgɨ ahɨ belemneːbit, barɨ͡agɨn. </ta>
            <ta e="T327" id="Seg_5994" s="T325">U͡ol di͡ebit: </ta>
            <ta e="T335" id="Seg_5995" s="T327">"Kanna ire hir iččite baːr ühü, onno barɨ͡am." </ta>
            <ta e="T340" id="Seg_5996" s="T335">U͡ol hɨrgatɨgar taːs hüregi uːrda. </ta>
            <ta e="T344" id="Seg_5997" s="T340">Ürek ihinen kamnaːn kaːlla. </ta>
            <ta e="T348" id="Seg_5998" s="T344">Töhö ördük barbɨta dʼürü? </ta>
            <ta e="T353" id="Seg_5999" s="T348">Araj, čuguːn uraha dʼi͡eni körbüt. </ta>
            <ta e="T363" id="Seg_6000" s="T353">Aːnɨ arɨjan kördö, kɨrdʼagas bagajɨ emeːksin oloror, attɨtɨgar kü͡ört hɨtar. </ta>
            <ta e="T367" id="Seg_6001" s="T363">Menʼiːtin ürdütüger mataga ɨjaːllɨbɨt. </ta>
            <ta e="T371" id="Seg_6002" s="T367">Mataga kihi hɨrajɨn majgɨlaːk. </ta>
            <ta e="T374" id="Seg_6003" s="T371">Ačaːk annɨta köŋdöj. </ta>
            <ta e="T377" id="Seg_6004" s="T374">Ürek usta hɨtar. </ta>
            <ta e="T381" id="Seg_6005" s="T377">Ürekke tɨːlaːk ilimnene hɨldʼar. </ta>
            <ta e="T384" id="Seg_6006" s="T381">Mataga haŋalaːk bu͡olbut: </ta>
            <ta e="T390" id="Seg_6007" s="T384">"ɨ͡aldʼɨt kelle, maččɨttar, mastaːŋ, u͡otta ottuŋ!" </ta>
            <ta e="T394" id="Seg_6008" s="T390">Mastara bejelere kiːrdiler tahaːrattan. </ta>
            <ta e="T396" id="Seg_6009" s="T394">U͡ol tu͡olkulaːta: </ta>
            <ta e="T399" id="Seg_6010" s="T396">"Timir mastar ebit." </ta>
            <ta e="T404" id="Seg_6011" s="T399">Emeːksin turan mastarɨ ačaːgɨgar musta. </ta>
            <ta e="T406" id="Seg_6012" s="T404">Mataga di͡ete: </ta>
            <ta e="T408" id="Seg_6013" s="T406">"Kü͡ört, kü͡örteː!" </ta>
            <ta e="T414" id="Seg_6014" s="T408">Uraga dʼi͡e timir mu͡ostata kɨtarčɨ barda. </ta>
            <ta e="T417" id="Seg_6015" s="T414">Mataga bi͡ek diːr: </ta>
            <ta e="T424" id="Seg_6016" s="T417">"Kü͡ört, kü͡örteː, kü͡örteː, ɨ͡aldʼɨt kelle, u͡otta ep!" </ta>
            <ta e="T428" id="Seg_6017" s="T424">Uraha dʼi͡e barɨta kɨtarda. </ta>
            <ta e="T432" id="Seg_6018" s="T428">Emeːksin ɨ͡aldʼɨt u͡olu kördöstö: </ta>
            <ta e="T436" id="Seg_6019" s="T432">"Ititime olus, hojut dʼe." </ta>
            <ta e="T443" id="Seg_6020" s="T436">U͡ol taːs hüregi ɨlan ačaːk ortotugar bɨrakta. </ta>
            <ta e="T446" id="Seg_6021" s="T443">Uraha dʼi͡e hojdo. </ta>
            <ta e="T448" id="Seg_6022" s="T446">Mu͡ostata kɨtarbata. </ta>
            <ta e="T453" id="Seg_6023" s="T448">Emeːksin emi͡e kam kɨrɨ͡a bu͡olla. </ta>
            <ta e="T457" id="Seg_6024" s="T453">"Olus toŋoruma", kördöspüt mataga, "barɨ hanaːgɨn toloru͡om."</ta>
            <ta e="T461" id="Seg_6025" s="T457">Mataga ebit hir iččite. </ta>
            <ta e="T468" id="Seg_6026" s="T461">U͡ol taːs hüregi ačaːk ihitten oroːn ɨlla. </ta>
            <ta e="T471" id="Seg_6027" s="T468">U͡ota emi͡e gɨdaːrɨjda. </ta>
            <ta e="T474" id="Seg_6028" s="T471">Mataga di͡ete u͡olga: </ta>
            <ta e="T480" id="Seg_6029" s="T474">"Tahaːra tagɨs, onno hɨrgalaːk taba turar." </ta>
            <ta e="T488" id="Seg_6030" s="T480">U͡ol tahaːra taksan, tu͡ok da hɨrgalaːk tabatɨn körböt. </ta>
            <ta e="T495" id="Seg_6031" s="T488">Kanna ere ču͡oraːn, gubaːrka ere tɨ͡aha ihiller. </ta>
            <ta e="T500" id="Seg_6032" s="T495">Ketegen di͡ek mataga haŋata ihillibit: </ta>
            <ta e="T502" id="Seg_6033" s="T500">"Olor hɨrgagar. </ta>
            <ta e="T510" id="Seg_6034" s="T502">Keŋniŋ di͡ek kanʼɨhɨma, hu͡ol aːra toktoːmo, tabalargɨn ahatɨma." </ta>
            <ta e="T518" id="Seg_6035" s="T510">U͡ol iliːtin uːnna nʼuŋuːnu kapta, köstübet hɨrgaga miːnne. </ta>
            <ta e="T521" id="Seg_6036" s="T518">Tɨ͡allaːk purga ogusta. </ta>
            <ta e="T526" id="Seg_6037" s="T521">Kaččanɨ barbɨta dʼürü, kim bili͡ej? </ta>
            <ta e="T530" id="Seg_6038" s="T526">Hɨrgalaːk tabalara bejelere illeller. </ta>
            <ta e="T532" id="Seg_6039" s="T530">U͡ol tommot. </ta>
            <ta e="T536" id="Seg_6040" s="T532">Iliːtin olbogun ihiger ukta. </ta>
            <ta e="T542" id="Seg_6041" s="T536">Onno itiː timir mas hɨtar ebit. </ta>
            <ta e="T544" id="Seg_6042" s="T542">Ontuta ititer. </ta>
            <ta e="T554" id="Seg_6043" s="T544">Ebe ürdüger kördö, hɨrgaga ölbüt dʼaktar hɨtar, kam tibillen baraːn. </ta>
            <ta e="T556" id="Seg_6044" s="T554">Tehijbekke toktoːto. </ta>
            <ta e="T565" id="Seg_6045" s="T556">Toktuːrun kɨtta nʼuŋuhuta hulbu ɨstanan, kajdi͡e barbɨta, kajdi͡ek kelbite? </ta>
            <ta e="T570" id="Seg_6046" s="T565">Üs koŋnomu͡oj tabalaːk ete ebit. </ta>
            <ta e="T574" id="Seg_6047" s="T570">Nʼuŋuhuta bɨstan baran kaːlbɨt. </ta>
            <ta e="T577" id="Seg_6048" s="T574">Mataga haŋata ihillibit: </ta>
            <ta e="T580" id="Seg_6049" s="T577">"Eni͡eke di͡ebitim, toktoːmo!" </ta>
            <ta e="T591" id="Seg_6050" s="T580">U͡ol hɨrgatɨttan kɨhɨl timir mahɨ ɨlan, kɨːs hɨrgatɨn ulkatɨgar batarɨ asta. </ta>
            <ta e="T595" id="Seg_6051" s="T591">Kaːr honno iren kaːlla. </ta>
            <ta e="T599" id="Seg_6052" s="T595">Toŋon ölbüt dʼaktara tilinne. </ta>
            <ta e="T605" id="Seg_6053" s="T599">Körbüte, bert basku͡oj bagajɨ kɨːs ebit. </ta>
            <ta e="T609" id="Seg_6054" s="T605">Kɨːs ü͡ören muŋa hu͡ok. </ta>
            <ta e="T613" id="Seg_6055" s="T609">"Anɨ bejem dʼi͡eber tiːji͡em. </ta>
            <ta e="T615" id="Seg_6056" s="T613">Barsɨbakkɨn du͡o? </ta>
            <ta e="T621" id="Seg_6057" s="T615">Ahatɨ͡am, ičiges tellekke telgi͡em", kördöspüt kɨːs. </ta>
            <ta e="T624" id="Seg_6058" s="T621">"ɨraːk du͡o dʼi͡eŋ?" </ta>
            <ta e="T626" id="Seg_6059" s="T624">"Uː ihiger." </ta>
            <ta e="T635" id="Seg_6060" s="T626">U͡ol ügüs oduːnu körbüte, ol ihin kɨːs öhün hökpötö. </ta>
            <ta e="T637" id="Seg_6061" s="T635">Diː ebit: </ta>
            <ta e="T641" id="Seg_6062" s="T637">"Min edʼiːjbin bulu͡okpun naːda. </ta>
            <ta e="T645" id="Seg_6063" s="T641">Kojut keli͡em eːt, küːt." </ta>
            <ta e="T651" id="Seg_6064" s="T645">U͡ol töhönü-kaččanɨ barbɨta, kim da bilbet. </ta>
            <ta e="T654" id="Seg_6065" s="T651">Araj kadʼɨrɨk bu͡olbut. </ta>
            <ta e="T656" id="Seg_6066" s="T654">Ahɨːlɨktaːk bagajɨ. </ta>
            <ta e="T658" id="Seg_6067" s="T656">Tabalara aččɨktaːtɨlar. </ta>
            <ta e="T662" id="Seg_6068" s="T658">"Ahatɨ͡akka duː", diː hanaːbɨt. </ta>
            <ta e="T668" id="Seg_6069" s="T662">Hir iččitin haŋatɨn öjdüː-öjdüː tabalarɨn toktoːppoto. </ta>
            <ta e="T671" id="Seg_6070" s="T668">Onton diː hanaːta: </ta>
            <ta e="T674" id="Seg_6071" s="T671">"Olus aččɨktaːbɨttar, toktotu͡okka." </ta>
            <ta e="T683" id="Seg_6072" s="T674">Toktotorun kɨtta, ortokuː kölüːr tabata melis ere gɨnan kaːlbɨt. </ta>
            <ta e="T687" id="Seg_6073" s="T683">Kajdi͡e barbɨta, kajdi͡ek köppüte? </ta>
            <ta e="T690" id="Seg_6074" s="T687">Emi͡e haŋa ihillibit: </ta>
            <ta e="T695" id="Seg_6075" s="T690">"Min eni͡eke di͡ebitim, ahatɨma tabalargɨn." </ta>
            <ta e="T699" id="Seg_6076" s="T695">U͡ol töhönü barbɨta dʼürü? </ta>
            <ta e="T706" id="Seg_6077" s="T699">Maː taːs kajatɨgar tiːjde, ikki aŋɨ kajdaːččɨtɨgar. </ta>
            <ta e="T711" id="Seg_6078" s="T706">Araj bejetin hiriger tiːjbit ebit. </ta>
            <ta e="T719" id="Seg_6079" s="T711">Toŋon ölbüt tabalar mu͡ostara araččɨ kaːr ihitten čorohollor. </ta>
            <ta e="T724" id="Seg_6080" s="T719">Kaːrɨ kürdʼen edʼiːjin nʼu͡oŋuhutun bulla. </ta>
            <ta e="T728" id="Seg_6081" s="T724">Itiː timir mahɨnan taːrɨjda. </ta>
            <ta e="T732" id="Seg_6082" s="T728">Tabata tillen baraːn turda. </ta>
            <ta e="T737" id="Seg_6083" s="T732">Onton mahɨ ɨlla kaːrga asta. </ta>
            <ta e="T743" id="Seg_6084" s="T737">Toŋon ölbüt tabalar honno barɨlara tilinniler. </ta>
            <ta e="T750" id="Seg_6085" s="T743">Bütün ü͡ör bu͡olan ahɨː hɨldʼallar haŋa hirinen. </ta>
            <ta e="T753" id="Seg_6086" s="T750">Kanʼɨspɨta, edʼiːje turar. </ta>
            <ta e="T758" id="Seg_6087" s="T753">U͡ol edʼiːjin körön bert ü͡örde. </ta>
            <ta e="T767" id="Seg_6088" s="T758">"Maŋnaj en", di͡ete edʼiːje, "ör bagajdɨk utujbutuŋ, onton min. </ta>
            <ta e="T770" id="Seg_6089" s="T767">Haːs bu͡olbut ebit. </ta>
            <ta e="T774" id="Seg_6090" s="T770">Kɨhɨnɨ meldʼi utujbuppun ebit." </ta>
            <ta e="T778" id="Seg_6091" s="T774">U͡ol edʼiːjin dʼi͡etiger ilte. </ta>
            <ta e="T781" id="Seg_6092" s="T778">Ü͡örün üːren egelle. </ta>
            <ta e="T787" id="Seg_6093" s="T781">Töhö da bu͡olbakka bu u͡ol terinne. </ta>
            <ta e="T791" id="Seg_6094" s="T787">"Kajdi͡e baragɨn", ɨjɨppɨt edʼiːje. </ta>
            <ta e="T794" id="Seg_6095" s="T791">"Hotoru keli͡em, kečehime." </ta>
            <ta e="T802" id="Seg_6096" s="T794">U͡ol maː kɨːhɨ kördüː barɨːhɨ, uː ihiger dʼi͡eleːgi. </ta>
            <ta e="T808" id="Seg_6097" s="T802">Baran ihen mɨlaː bagaj purgaː bu͡olbut. </ta>
            <ta e="T816" id="Seg_6098" s="T808">Hirinen da bararɨn bilbet, buːhunan da bararɨn bilbet. </ta>
            <ta e="T821" id="Seg_6099" s="T816">(Öjüleːn) körbüte, bajgal buːha ebit. </ta>
            <ta e="T827" id="Seg_6100" s="T821">Haːskɨ buːs iŋnen u͡ol uːga tüspüt. </ta>
            <ta e="T830" id="Seg_6101" s="T827">Tühen hordoŋ bu͡olbut. </ta>
            <ta e="T836" id="Seg_6102" s="T830">Uː ihinen usta hɨldʼan ilimŋe tübespit. </ta>
            <ta e="T840" id="Seg_6103" s="T836">I͡enneːk ilimŋe hordoŋnor agaj. </ta>
            <ta e="T845" id="Seg_6104" s="T840">Töhö da bu͡olbakka kɨːs kelle. </ta>
            <ta e="T848" id="Seg_6105" s="T845">Maː kɨːha, tilinnereːččite. </ta>
            <ta e="T854" id="Seg_6106" s="T848">Hordoŋ u͡olu nʼu͡oŋuhut gɨnan baraːn kölünne. </ta>
            <ta e="T856" id="Seg_6107" s="T854">Dʼi͡etiger barɨːhɨ. </ta>
            <ta e="T858" id="Seg_6108" s="T856">Nʼu͡oŋuhut istibet. </ta>
            <ta e="T862" id="Seg_6109" s="T858">Kɨtɨl ere di͡ek talahar. </ta>
            <ta e="T866" id="Seg_6110" s="T862">Kɨːs onu halaja hatɨːr. </ta>
            <ta e="T871" id="Seg_6111" s="T866">Nʼu͡oŋuhuta bɨha mökküjen kɨtɨlga tagɨsta. </ta>
            <ta e="T876" id="Seg_6112" s="T871">Taksaːt u͡ol-hordoŋ ile u͡ol bu͡olbut. </ta>
            <ta e="T879" id="Seg_6113" s="T876">Hordoŋnor taba bu͡ollular. </ta>
            <ta e="T882" id="Seg_6114" s="T879">Kɨːstaːk u͡ol taːjɨstɨlar. </ta>
            <ta e="T884" id="Seg_6115" s="T882">Kɨːs ɨjɨtta: </ta>
            <ta e="T886" id="Seg_6116" s="T884">"Edʼiːjgin bulluŋ? </ta>
            <ta e="T888" id="Seg_6117" s="T886">Kajtak olorogun?" </ta>
            <ta e="T897" id="Seg_6118" s="T888">"Barɨs minigin kɨtta", di͡ete u͡ol, "edʼiːjim enigin küːter, bilsihiŋ. </ta>
            <ta e="T901" id="Seg_6119" s="T897">Biːrge oloru͡okput, ü͡örbütün kolbu͡okput." </ta>
            <ta e="T912" id="Seg_6120" s="T901">U͡olu kɨtta kɨːs hordoŋnoru uː ihitten oroːtular, ontulara bugdʼiː taba bu͡ollular. </ta>
            <ta e="T916" id="Seg_6121" s="T912">U͡ol edʼiːje ü͡ören körüste. </ta>
            <ta e="T920" id="Seg_6122" s="T916">U͡ol üs künü utujda. </ta>
            <ta e="T933" id="Seg_6123" s="T920">Uhuktan körbüte, edʼiːje kɨːhɨ gɨtta kepsete-kepsete külse olorollor, čaːj iheller, busput eti hiːller. </ta>
            <ta e="T939" id="Seg_6124" s="T933">U͡ol ulkalarɨgar olordo, bütün tabanɨ hi͡ete. </ta>
            <ta e="T946" id="Seg_6125" s="T939">U͡ol kɨːhɨ dʼaktar gɨmmɨt, dʼolloːk bagajtɨk olorbuttar. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_6126" s="T1">ihilleː-ŋ</ta>
            <ta e="T3" id="Seg_6127" s="T2">dulgaːn-nar</ta>
            <ta e="T4" id="Seg_6128" s="T3">oloŋko-loru-n</ta>
            <ta e="T5" id="Seg_6129" s="T4">edʼiːj-deːk</ta>
            <ta e="T6" id="Seg_6130" s="T5">u͡ol</ta>
            <ta e="T7" id="Seg_6131" s="T6">di͡e-n</ta>
            <ta e="T8" id="Seg_6132" s="T7">aːt-taːgɨ</ta>
            <ta e="T9" id="Seg_6133" s="T8">u͡ol</ta>
            <ta e="T10" id="Seg_6134" s="T9">edʼiːj-i-n</ta>
            <ta e="T11" id="Seg_6135" s="T10">kɨtta</ta>
            <ta e="T12" id="Seg_6136" s="T11">ikki͡ejkeːn</ta>
            <ta e="T13" id="Seg_6137" s="T12">olor-but-tara</ta>
            <ta e="T14" id="Seg_6138" s="T13">edʼiːj-e</ta>
            <ta e="T15" id="Seg_6139" s="T14">ülehit-teːk</ta>
            <ta e="T16" id="Seg_6140" s="T15">uːs</ta>
            <ta e="T17" id="Seg_6141" s="T16">bagajɨ</ta>
            <ta e="T18" id="Seg_6142" s="T17">hetiː-ler-e</ta>
            <ta e="T19" id="Seg_6143" s="T18">bugdiː</ta>
            <ta e="T20" id="Seg_6144" s="T19">taba</ta>
            <ta e="T21" id="Seg_6145" s="T20">tiriː-ler-i-nen</ta>
            <ta e="T22" id="Seg_6146" s="T21">habɨː-laːk-tar</ta>
            <ta e="T23" id="Seg_6147" s="T22">uraha</ta>
            <ta e="T24" id="Seg_6148" s="T23">dʼi͡e-ni</ta>
            <ta e="T25" id="Seg_6149" s="T24">beje-te</ta>
            <ta e="T26" id="Seg_6150" s="T25">tut-t-aːččɨ</ta>
            <ta e="T27" id="Seg_6151" s="T26">taba-larɨ-n</ta>
            <ta e="T28" id="Seg_6152" s="T27">da</ta>
            <ta e="T29" id="Seg_6153" s="T28">hajɨn</ta>
            <ta e="T30" id="Seg_6154" s="T29">keteː-čči</ta>
            <ta e="T31" id="Seg_6155" s="T30">u͡ol</ta>
            <ta e="T32" id="Seg_6156" s="T31">balt-ɨ-ta</ta>
            <ta e="T33" id="Seg_6157" s="T32">kühüŋ-ŋü-tten</ta>
            <ta e="T34" id="Seg_6158" s="T33">utuj-but</ta>
            <ta e="T35" id="Seg_6159" s="T34">kün</ta>
            <ta e="T36" id="Seg_6160" s="T35">taks-ɨ͡a-r</ta>
            <ta e="T37" id="Seg_6161" s="T36">di͡eri</ta>
            <ta e="T38" id="Seg_6162" s="T37">ü͡ör-dere</ta>
            <ta e="T39" id="Seg_6163" s="T38">ɨraːp-pɨt</ta>
            <ta e="T40" id="Seg_6164" s="T39">oŋko-loro</ta>
            <ta e="T41" id="Seg_6165" s="T40">keŋeː-bit</ta>
            <ta e="T42" id="Seg_6166" s="T41">kɨːs</ta>
            <ta e="T43" id="Seg_6167" s="T42">oŋko</ta>
            <ta e="T44" id="Seg_6168" s="T43">ularɨt-ɨːhɨ</ta>
            <ta e="T45" id="Seg_6169" s="T44">ü͡ör-ü-n</ta>
            <ta e="T46" id="Seg_6170" s="T45">egel-l-e</ta>
            <ta e="T47" id="Seg_6171" s="T46">taba</ta>
            <ta e="T48" id="Seg_6172" s="T47">tutun-n-a</ta>
            <ta e="T49" id="Seg_6173" s="T48">kölün-n-e</ta>
            <ta e="T50" id="Seg_6174" s="T49">balt-ɨ-tɨ-n</ta>
            <ta e="T51" id="Seg_6175" s="T50">uhugun-nar-a</ta>
            <ta e="T52" id="Seg_6176" s="T51">hatɨː-r</ta>
            <ta e="T53" id="Seg_6177" s="T52">amattan</ta>
            <ta e="T54" id="Seg_6178" s="T53">uhukt-u-bat</ta>
            <ta e="T55" id="Seg_6179" s="T54">uraha</ta>
            <ta e="T56" id="Seg_6180" s="T55">dʼi͡e-ti-n</ta>
            <ta e="T57" id="Seg_6181" s="T56">kastaː-n</ta>
            <ta e="T58" id="Seg_6182" s="T57">baraːn</ta>
            <ta e="T59" id="Seg_6183" s="T58">balt-ɨ-tɨ-n</ta>
            <ta e="T60" id="Seg_6184" s="T59">kɨrsa</ta>
            <ta e="T61" id="Seg_6185" s="T60">hu͡organ-ɨ-n</ta>
            <ta e="T62" id="Seg_6186" s="T61">tuːra</ta>
            <ta e="T63" id="Seg_6187" s="T62">tar-t-a</ta>
            <ta e="T64" id="Seg_6188" s="T63">örüküj-en</ta>
            <ta e="T65" id="Seg_6189" s="T64">uhugun-n-a</ta>
            <ta e="T66" id="Seg_6190" s="T65">taŋɨn-n-a</ta>
            <ta e="T67" id="Seg_6191" s="T66">hɨrga-tɨ-gar</ta>
            <ta e="T68" id="Seg_6192" s="T67">olor-oːt</ta>
            <ta e="T69" id="Seg_6193" s="T68">emi͡e</ta>
            <ta e="T70" id="Seg_6194" s="T69">utuj-d-a</ta>
            <ta e="T71" id="Seg_6195" s="T70">mas-tan-ar-ɨ-gar</ta>
            <ta e="T72" id="Seg_6196" s="T71">buːs-tan-ar-ɨ-gar</ta>
            <ta e="T73" id="Seg_6197" s="T72">taba-lan-ar-ɨ-gar</ta>
            <ta e="T74" id="Seg_6198" s="T73">bu͡ol-an</ta>
            <ta e="T75" id="Seg_6199" s="T74">kɨːs</ta>
            <ta e="T76" id="Seg_6200" s="T75">bert</ta>
            <ta e="T77" id="Seg_6201" s="T76">hɨlaj-ar</ta>
            <ta e="T78" id="Seg_6202" s="T77">hɨlaj-an</ta>
            <ta e="T79" id="Seg_6203" s="T78">utuj-but</ta>
            <ta e="T80" id="Seg_6204" s="T79">utuj-a</ta>
            <ta e="T81" id="Seg_6205" s="T80">hɨt-an</ta>
            <ta e="T82" id="Seg_6206" s="T81">toŋ-on</ta>
            <ta e="T83" id="Seg_6207" s="T82">kaːl-bɨt</ta>
            <ta e="T84" id="Seg_6208" s="T83">u͡ol</ta>
            <ta e="T85" id="Seg_6209" s="T84">balt-ɨ-ta</ta>
            <ta e="T86" id="Seg_6210" s="T85">össü͡ö</ta>
            <ta e="T87" id="Seg_6211" s="T86">üs</ta>
            <ta e="T88" id="Seg_6212" s="T87">kün-ü</ta>
            <ta e="T89" id="Seg_6213" s="T88">utuj-but</ta>
            <ta e="T90" id="Seg_6214" s="T89">uhukt-u-but-a</ta>
            <ta e="T91" id="Seg_6215" s="T90">kör-büt-e</ta>
            <ta e="T92" id="Seg_6216" s="T91">edʼiːj-e</ta>
            <ta e="T93" id="Seg_6217" s="T92">toŋ-on</ta>
            <ta e="T94" id="Seg_6218" s="T93">kaːl-bɨt</ta>
            <ta e="T95" id="Seg_6219" s="T94">ü͡ör-e</ta>
            <ta e="T96" id="Seg_6220" s="T95">barɨ-ta</ta>
            <ta e="T97" id="Seg_6221" s="T96">toŋ-on</ta>
            <ta e="T98" id="Seg_6222" s="T97">öl-büt</ta>
            <ta e="T99" id="Seg_6223" s="T98">hobu͡oj</ta>
            <ta e="T100" id="Seg_6224" s="T99">oks-u-but</ta>
            <ta e="T101" id="Seg_6225" s="T100">u͡ot</ta>
            <ta e="T102" id="Seg_6226" s="T101">otun-nak-pɨna</ta>
            <ta e="T103" id="Seg_6227" s="T102">bugaːt</ta>
            <ta e="T104" id="Seg_6228" s="T103">edʼiːj-i-m</ta>
            <ta e="T105" id="Seg_6229" s="T104">till-i͡e</ta>
            <ta e="T106" id="Seg_6230" s="T105">da</ta>
            <ta e="T107" id="Seg_6231" s="T106">d-iː</ta>
            <ta e="T108" id="Seg_6232" s="T107">hanaː-bɨt</ta>
            <ta e="T109" id="Seg_6233" s="T108">u͡ol</ta>
            <ta e="T110" id="Seg_6234" s="T109">mas-tan-a</ta>
            <ta e="T111" id="Seg_6235" s="T110">bar-ɨ͡ak-ka</ta>
            <ta e="T112" id="Seg_6236" s="T111">kaja-laːk</ta>
            <ta e="T113" id="Seg_6237" s="T112">ürek</ta>
            <ta e="T114" id="Seg_6238" s="T113">ürd-ü-nen</ta>
            <ta e="T115" id="Seg_6239" s="T114">mas</ta>
            <ta e="T116" id="Seg_6240" s="T115">di͡ek</ta>
            <ta e="T117" id="Seg_6241" s="T116">hüːr-büt</ta>
            <ta e="T118" id="Seg_6242" s="T117">kör-büt-e</ta>
            <ta e="T119" id="Seg_6243" s="T118">kallaːn-tan</ta>
            <ta e="T120" id="Seg_6244" s="T119">tutuktaːk</ta>
            <ta e="T121" id="Seg_6245" s="T120">otuː</ta>
            <ta e="T122" id="Seg_6246" s="T121">gɨdaːrɨj-a</ta>
            <ta e="T123" id="Seg_6247" s="T122">hɨt-ar</ta>
            <ta e="T124" id="Seg_6248" s="T123">abaːhɨ</ta>
            <ta e="T125" id="Seg_6249" s="T124">üčehe-ge</ta>
            <ta e="T126" id="Seg_6250" s="T125">et</ta>
            <ta e="T127" id="Seg_6251" s="T126">ü͡öl-e-n-e</ta>
            <ta e="T128" id="Seg_6252" s="T127">olor-or</ta>
            <ta e="T129" id="Seg_6253" s="T128">u͡ol-u</ta>
            <ta e="T130" id="Seg_6254" s="T129">kör-böt</ta>
            <ta e="T131" id="Seg_6255" s="T130">bu</ta>
            <ta e="T132" id="Seg_6256" s="T131">u͡ol</ta>
            <ta e="T133" id="Seg_6257" s="T132">hulburu-t-a</ta>
            <ta e="T134" id="Seg_6258" s="T133">tɨːt-a-tɨːt-a</ta>
            <ta e="T135" id="Seg_6259" s="T134">oŋu͡or-gu-ttan</ta>
            <ta e="T136" id="Seg_6260" s="T135">h-iː</ta>
            <ta e="T137" id="Seg_6261" s="T136">olor-but</ta>
            <ta e="T138" id="Seg_6262" s="T137">kajtak</ta>
            <ta e="T139" id="Seg_6263" s="T138">tu͡olkulaː-bat-ɨ-n</ta>
            <ta e="T140" id="Seg_6264" s="T139">ke</ta>
            <ta e="T141" id="Seg_6265" s="T140">diː-r</ta>
            <ta e="T142" id="Seg_6266" s="T141">abaːhɨ</ta>
            <ta e="T143" id="Seg_6267" s="T142">et-ter-i-m</ta>
            <ta e="T144" id="Seg_6268" s="T143">u͡ok-ka</ta>
            <ta e="T145" id="Seg_6269" s="T144">tüh-el-leri-n</ta>
            <ta e="T146" id="Seg_6270" s="T145">abaːhɨ</ta>
            <ta e="T147" id="Seg_6271" s="T146">u͡ol-u</ta>
            <ta e="T148" id="Seg_6272" s="T147">kör-ön</ta>
            <ta e="T149" id="Seg_6273" s="T148">ölör-öːrü</ta>
            <ta e="T150" id="Seg_6274" s="T149">gɨm-mɨt</ta>
            <ta e="T151" id="Seg_6275" s="T150">u͡ol</ta>
            <ta e="T152" id="Seg_6276" s="T151">uruːka-tɨ-nan</ta>
            <ta e="T153" id="Seg_6277" s="T152">abaːhɨ</ta>
            <ta e="T154" id="Seg_6278" s="T153">agɨs</ta>
            <ta e="T155" id="Seg_6279" s="T154">bah-ɨ-n</ta>
            <ta e="T156" id="Seg_6280" s="T155">tuːra</ta>
            <ta e="T157" id="Seg_6281" s="T156">oks-u-but</ta>
            <ta e="T158" id="Seg_6282" s="T157">bas-tar-a</ta>
            <ta e="T159" id="Seg_6283" s="T158">üŋküːl-üː</ta>
            <ta e="T160" id="Seg_6284" s="T159">hɨldʼ-al-lar</ta>
            <ta e="T161" id="Seg_6285" s="T160">u͡ol</ta>
            <ta e="T162" id="Seg_6286" s="T161">onno</ta>
            <ta e="T163" id="Seg_6287" s="T162">časkɨj-bɨt</ta>
            <ta e="T164" id="Seg_6288" s="T163">bukatɨːr</ta>
            <ta e="T165" id="Seg_6289" s="T164">ikki-leː-čči-te</ta>
            <ta e="T166" id="Seg_6290" s="T165">üh-üs-teː-čči-te</ta>
            <ta e="T167" id="Seg_6291" s="T166">hu͡ok</ta>
            <ta e="T168" id="Seg_6292" s="T167">abaːhɨ</ta>
            <ta e="T169" id="Seg_6293" s="T168">tü͡öh-ü-n</ta>
            <ta e="T170" id="Seg_6294" s="T169">tɨːr-a</ta>
            <ta e="T171" id="Seg_6295" s="T170">tard-an</ta>
            <ta e="T172" id="Seg_6296" s="T171">taːh-ɨ</ta>
            <ta e="T173" id="Seg_6297" s="T172">oroː-t-o</ta>
            <ta e="T174" id="Seg_6298" s="T173">taːs</ta>
            <ta e="T175" id="Seg_6299" s="T174">hürek-teːk</ta>
            <ta e="T176" id="Seg_6300" s="T175">e-bit</ta>
            <ta e="T177" id="Seg_6301" s="T176">u͡ol</ta>
            <ta e="T178" id="Seg_6302" s="T177">taːh-ɨ</ta>
            <ta e="T179" id="Seg_6303" s="T178">teb-e-teb-e</ta>
            <ta e="T180" id="Seg_6304" s="T179">bar-a</ta>
            <ta e="T181" id="Seg_6305" s="T180">tur-but</ta>
            <ta e="T182" id="Seg_6306" s="T181">inni-ti-ger</ta>
            <ta e="T183" id="Seg_6307" s="T182">hir</ta>
            <ta e="T184" id="Seg_6308" s="T183">aŋar-daːk</ta>
            <ta e="T185" id="Seg_6309" s="T184">haga</ta>
            <ta e="T186" id="Seg_6310" s="T185">taːs</ta>
            <ta e="T187" id="Seg_6311" s="T186">tur-but</ta>
            <ta e="T188" id="Seg_6312" s="T187">taːs</ta>
            <ta e="T189" id="Seg_6313" s="T188">kaja</ta>
            <ta e="T190" id="Seg_6314" s="T189">taːs</ta>
            <ta e="T191" id="Seg_6315" s="T190">kaja-nɨ</ta>
            <ta e="T192" id="Seg_6316" s="T191">taːs</ta>
            <ta e="T193" id="Seg_6317" s="T192">hüreg-i-nen</ta>
            <ta e="T194" id="Seg_6318" s="T193">tep-pit</ta>
            <ta e="T195" id="Seg_6319" s="T194">kaja-ta</ta>
            <ta e="T196" id="Seg_6320" s="T195">ikki</ta>
            <ta e="T197" id="Seg_6321" s="T196">aŋɨ</ta>
            <ta e="T198" id="Seg_6322" s="T197">kajd-an</ta>
            <ta e="T199" id="Seg_6323" s="T198">tüs-püt</ta>
            <ta e="T200" id="Seg_6324" s="T199">oduːlaː-n</ta>
            <ta e="T201" id="Seg_6325" s="T200">kör-büt-e</ta>
            <ta e="T202" id="Seg_6326" s="T201">hir</ta>
            <ta e="T203" id="Seg_6327" s="T202">köst-ör</ta>
            <ta e="T204" id="Seg_6328" s="T203">onno</ta>
            <ta e="T205" id="Seg_6329" s="T204">bar-an</ta>
            <ta e="T206" id="Seg_6330" s="T205">hɨrga-laːk</ta>
            <ta e="T207" id="Seg_6331" s="T206">hu͡ol-u-n</ta>
            <ta e="T208" id="Seg_6332" s="T207">kör-büt</ta>
            <ta e="T209" id="Seg_6333" s="T208">paːs</ta>
            <ta e="T210" id="Seg_6334" s="T209">kapkaːn</ta>
            <ta e="T211" id="Seg_6335" s="T210">kör-ü-n-e</ta>
            <ta e="T212" id="Seg_6336" s="T211">hɨldʼ-ar</ta>
            <ta e="T213" id="Seg_6337" s="T212">kihi</ta>
            <ta e="T214" id="Seg_6338" s="T213">e-t-e</ta>
            <ta e="T215" id="Seg_6339" s="T214">e-bit</ta>
            <ta e="T216" id="Seg_6340" s="T215">töhö</ta>
            <ta e="T217" id="Seg_6341" s="T216">da</ta>
            <ta e="T218" id="Seg_6342" s="T217">bu͡ol-bakka</ta>
            <ta e="T219" id="Seg_6343" s="T218">kɨrsa-hɨt</ta>
            <ta e="T220" id="Seg_6344" s="T219">utarɨ</ta>
            <ta e="T221" id="Seg_6345" s="T220">ih-er-i-n</ta>
            <ta e="T222" id="Seg_6346" s="T221">kör-büt</ta>
            <ta e="T223" id="Seg_6347" s="T222">kel-en</ta>
            <ta e="T224" id="Seg_6348" s="T223">tur-d-a</ta>
            <ta e="T225" id="Seg_6349" s="T224">doroːbolos-t-o</ta>
            <ta e="T226" id="Seg_6350" s="T225">tuːg-u</ta>
            <ta e="T227" id="Seg_6351" s="T226">gɨn-a</ta>
            <ta e="T228" id="Seg_6352" s="T227">hɨldʼ-a-gɨn</ta>
            <ta e="T229" id="Seg_6353" s="T228">ɨjɨp-pɨt</ta>
            <ta e="T230" id="Seg_6354" s="T229">u͡ol</ta>
            <ta e="T231" id="Seg_6355" s="T230">paːs</ta>
            <ta e="T232" id="Seg_6356" s="T231">kapkaːn</ta>
            <ta e="T233" id="Seg_6357" s="T232">kör-ü-n-e-bin</ta>
            <ta e="T234" id="Seg_6358" s="T233">ɨraːk</ta>
            <ta e="T235" id="Seg_6359" s="T234">dʼi͡e-leːk-kit</ta>
            <ta e="T236" id="Seg_6360" s="T235">du͡o</ta>
            <ta e="T237" id="Seg_6361" s="T236">kas</ta>
            <ta e="T238" id="Seg_6362" s="T237">kihi-git=ij</ta>
            <ta e="T239" id="Seg_6363" s="T238">inʼe-bi-n</ta>
            <ta e="T240" id="Seg_6364" s="T239">aga-bɨ-n</ta>
            <ta e="T241" id="Seg_6365" s="T240">kɨtta</ta>
            <ta e="T242" id="Seg_6366" s="T241">olor-o-bun</ta>
            <ta e="T243" id="Seg_6367" s="T242">di͡e-bit</ta>
            <ta e="T244" id="Seg_6368" s="T243">u͡ol</ta>
            <ta e="T245" id="Seg_6369" s="T244">meŋehin</ta>
            <ta e="T246" id="Seg_6370" s="T245">ill-i͡e-m</ta>
            <ta e="T247" id="Seg_6371" s="T246">ɨ͡aldʼɨt</ta>
            <ta e="T248" id="Seg_6372" s="T247">bu͡ol</ta>
            <ta e="T249" id="Seg_6373" s="T248">uraha</ta>
            <ta e="T250" id="Seg_6374" s="T249">dʼi͡e-ti-ger</ta>
            <ta e="T251" id="Seg_6375" s="T250">egel-bit</ta>
            <ta e="T252" id="Seg_6376" s="T251">ogonnʼor-doːk</ta>
            <ta e="T253" id="Seg_6377" s="T252">emeːksin</ta>
            <ta e="T254" id="Seg_6378" s="T253">utarɨ</ta>
            <ta e="T255" id="Seg_6379" s="T254">taks-an-nar</ta>
            <ta e="T256" id="Seg_6380" s="T255">körs-ü-büt-ter</ta>
            <ta e="T257" id="Seg_6381" s="T256">kaja</ta>
            <ta e="T258" id="Seg_6382" s="T257">hir-ten</ta>
            <ta e="T259" id="Seg_6383" s="T258">kel-li-ŋ</ta>
            <ta e="T260" id="Seg_6384" s="T259">ɨjɨp-pɨt-tar</ta>
            <ta e="T261" id="Seg_6385" s="T260">manna</ta>
            <ta e="T262" id="Seg_6386" s="T261">tu͡ora</ta>
            <ta e="T263" id="Seg_6387" s="T262">karak-taːk</ta>
            <ta e="T264" id="Seg_6388" s="T263">u͡ot</ta>
            <ta e="T265" id="Seg_6389" s="T264">ulluŋ-naːk</ta>
            <ta e="T266" id="Seg_6390" s="T265">ükt-e-n-e=lik</ta>
            <ta e="T267" id="Seg_6391" s="T266">u͡ol</ta>
            <ta e="T268" id="Seg_6392" s="T267">kepseː-bit</ta>
            <ta e="T269" id="Seg_6393" s="T268">edʼiːj-i-m</ta>
            <ta e="T270" id="Seg_6394" s="T269">toŋ-on</ta>
            <ta e="T271" id="Seg_6395" s="T270">öl-l-ö</ta>
            <ta e="T272" id="Seg_6396" s="T271">taba-lar-bɨt</ta>
            <ta e="T273" id="Seg_6397" s="T272">emi͡e</ta>
            <ta e="T274" id="Seg_6398" s="T273">toŋ-on</ta>
            <ta e="T275" id="Seg_6399" s="T274">öl-lü-ler</ta>
            <ta e="T276" id="Seg_6400" s="T275">kaːr</ta>
            <ta e="T277" id="Seg_6401" s="T276">ih-i-ger</ta>
            <ta e="T278" id="Seg_6402" s="T277">hɨt-al-lar</ta>
            <ta e="T279" id="Seg_6403" s="T278">tib-i-ll-e</ta>
            <ta e="T280" id="Seg_6404" s="T279">kajtak</ta>
            <ta e="T281" id="Seg_6405" s="T280">kihi</ta>
            <ta e="T282" id="Seg_6406" s="T281">bu͡ol-u͡o-m</ta>
            <ta e="T283" id="Seg_6407" s="T282">dʼürü</ta>
            <ta e="T284" id="Seg_6408" s="T283">maŋnaj</ta>
            <ta e="T285" id="Seg_6409" s="T284">ɨ͡aldʼɨt</ta>
            <ta e="T286" id="Seg_6410" s="T285">bu͡ol</ta>
            <ta e="T287" id="Seg_6411" s="T286">di͡e-bit</ta>
            <ta e="T288" id="Seg_6412" s="T287">ogonnʼor</ta>
            <ta e="T289" id="Seg_6413" s="T288">onton</ta>
            <ta e="T290" id="Seg_6414" s="T289">hübel-e-h-i͡ek-pit</ta>
            <ta e="T291" id="Seg_6415" s="T290">u͡ol-a</ta>
            <ta e="T292" id="Seg_6416" s="T291">taks-an</ta>
            <ta e="T293" id="Seg_6417" s="T292">tɨhɨ-nɨ</ta>
            <ta e="T294" id="Seg_6418" s="T293">ölör-büt</ta>
            <ta e="T295" id="Seg_6419" s="T294">tas-taːk</ta>
            <ta e="T296" id="Seg_6420" s="T295">tɨhɨ-nɨ</ta>
            <ta e="T297" id="Seg_6421" s="T296">ɨ͡aldʼɨt</ta>
            <ta e="T298" id="Seg_6422" s="T297">o-nu</ta>
            <ta e="T299" id="Seg_6423" s="T298">hogotogun</ta>
            <ta e="T300" id="Seg_6424" s="T299">hi͡e-bit</ta>
            <ta e="T301" id="Seg_6425" s="T300">tot-tu-ŋ</ta>
            <ta e="T302" id="Seg_6426" s="T301">ɨjɨp-pɨt-tar</ta>
            <ta e="T303" id="Seg_6427" s="T302">tot-tu-m</ta>
            <ta e="T304" id="Seg_6428" s="T303">kühüŋ-ŋü-tten</ta>
            <ta e="T305" id="Seg_6429" s="T304">ah-ɨː</ta>
            <ta e="T306" id="Seg_6430" s="T305">ilik-pin</ta>
            <ta e="T307" id="Seg_6431" s="T306">anɨ</ta>
            <ta e="T308" id="Seg_6432" s="T307">hɨnnʼan-ɨ͡a-m</ta>
            <ta e="T309" id="Seg_6433" s="T308">üs</ta>
            <ta e="T310" id="Seg_6434" s="T309">kün-ü</ta>
            <ta e="T311" id="Seg_6435" s="T310">u͡ol</ta>
            <ta e="T312" id="Seg_6436" s="T311">utuj-but</ta>
            <ta e="T313" id="Seg_6437" s="T312">uhukt-u-but-u-gar</ta>
            <ta e="T314" id="Seg_6438" s="T313">ogonnʼor</ta>
            <ta e="T315" id="Seg_6439" s="T314">muŋ</ta>
            <ta e="T316" id="Seg_6440" s="T315">emis</ta>
            <ta e="T317" id="Seg_6441" s="T316">taba-nɨ</ta>
            <ta e="T318" id="Seg_6442" s="T317">ölör-ön</ta>
            <ta e="T319" id="Seg_6443" s="T318">hi͡e-k-pit</ta>
            <ta e="T320" id="Seg_6444" s="T319">ogonnʼor</ta>
            <ta e="T321" id="Seg_6445" s="T320">üs</ta>
            <ta e="T322" id="Seg_6446" s="T321">ɨj-daːgɨ</ta>
            <ta e="T323" id="Seg_6447" s="T322">ah-ɨ</ta>
            <ta e="T324" id="Seg_6448" s="T323">belem-neː-bit</ta>
            <ta e="T325" id="Seg_6449" s="T324">bar-ɨ͡ag-ɨ-n</ta>
            <ta e="T326" id="Seg_6450" s="T325">u͡ol</ta>
            <ta e="T327" id="Seg_6451" s="T326">di͡e-bit</ta>
            <ta e="T328" id="Seg_6452" s="T327">kanna</ta>
            <ta e="T329" id="Seg_6453" s="T328">ire</ta>
            <ta e="T330" id="Seg_6454" s="T329">hir</ta>
            <ta e="T331" id="Seg_6455" s="T330">ičči-te</ta>
            <ta e="T332" id="Seg_6456" s="T331">baːr</ta>
            <ta e="T333" id="Seg_6457" s="T332">ühü</ta>
            <ta e="T334" id="Seg_6458" s="T333">onno</ta>
            <ta e="T335" id="Seg_6459" s="T334">bar-ɨ͡a-m</ta>
            <ta e="T336" id="Seg_6460" s="T335">u͡ol</ta>
            <ta e="T337" id="Seg_6461" s="T336">hɨrga-tɨ-gar</ta>
            <ta e="T338" id="Seg_6462" s="T337">taːs</ta>
            <ta e="T339" id="Seg_6463" s="T338">hüreg-i</ta>
            <ta e="T340" id="Seg_6464" s="T339">uːr-d-a</ta>
            <ta e="T341" id="Seg_6465" s="T340">ürek</ta>
            <ta e="T342" id="Seg_6466" s="T341">ih-i-nen</ta>
            <ta e="T343" id="Seg_6467" s="T342">kamnaː-n</ta>
            <ta e="T344" id="Seg_6468" s="T343">kaːl-l-a</ta>
            <ta e="T345" id="Seg_6469" s="T344">töhö</ta>
            <ta e="T346" id="Seg_6470" s="T345">ör-dük</ta>
            <ta e="T347" id="Seg_6471" s="T346">bar-bɨt-a</ta>
            <ta e="T348" id="Seg_6472" s="T347">dʼürü</ta>
            <ta e="T349" id="Seg_6473" s="T348">araj</ta>
            <ta e="T350" id="Seg_6474" s="T349">čuguːn</ta>
            <ta e="T351" id="Seg_6475" s="T350">uraha</ta>
            <ta e="T352" id="Seg_6476" s="T351">dʼi͡e-ni</ta>
            <ta e="T353" id="Seg_6477" s="T352">kör-büt</ta>
            <ta e="T354" id="Seg_6478" s="T353">aːn-ɨ</ta>
            <ta e="T355" id="Seg_6479" s="T354">arɨj-an</ta>
            <ta e="T356" id="Seg_6480" s="T355">kör-d-ö</ta>
            <ta e="T357" id="Seg_6481" s="T356">kɨrdʼagas</ta>
            <ta e="T358" id="Seg_6482" s="T357">bagajɨ</ta>
            <ta e="T359" id="Seg_6483" s="T358">emeːksin</ta>
            <ta e="T360" id="Seg_6484" s="T359">olor-or</ta>
            <ta e="T361" id="Seg_6485" s="T360">attɨ-tɨ-gar</ta>
            <ta e="T362" id="Seg_6486" s="T361">kü͡ört</ta>
            <ta e="T363" id="Seg_6487" s="T362">hɨt-ar</ta>
            <ta e="T364" id="Seg_6488" s="T363">menʼiː-ti-n</ta>
            <ta e="T365" id="Seg_6489" s="T364">ürdü-tü-ger</ta>
            <ta e="T366" id="Seg_6490" s="T365">mataga</ta>
            <ta e="T367" id="Seg_6491" s="T366">ɨjaː-ll-ɨ-bɨt</ta>
            <ta e="T368" id="Seg_6492" s="T367">mataga</ta>
            <ta e="T369" id="Seg_6493" s="T368">kihi</ta>
            <ta e="T370" id="Seg_6494" s="T369">hɨraj-ɨ-n</ta>
            <ta e="T371" id="Seg_6495" s="T370">majgɨlaːk</ta>
            <ta e="T372" id="Seg_6496" s="T371">ačaːk</ta>
            <ta e="T373" id="Seg_6497" s="T372">ann-ɨ-ta</ta>
            <ta e="T374" id="Seg_6498" s="T373">köŋdöj</ta>
            <ta e="T375" id="Seg_6499" s="T374">ürek</ta>
            <ta e="T376" id="Seg_6500" s="T375">ust-a</ta>
            <ta e="T377" id="Seg_6501" s="T376">hɨt-ar</ta>
            <ta e="T378" id="Seg_6502" s="T377">ürek-ke</ta>
            <ta e="T379" id="Seg_6503" s="T378">tɨː-laːk</ta>
            <ta e="T380" id="Seg_6504" s="T379">ilim-nen-e</ta>
            <ta e="T381" id="Seg_6505" s="T380">hɨldʼ-ar</ta>
            <ta e="T382" id="Seg_6506" s="T381">mataga</ta>
            <ta e="T383" id="Seg_6507" s="T382">haŋa-laːk</ta>
            <ta e="T384" id="Seg_6508" s="T383">bu͡ol-but</ta>
            <ta e="T385" id="Seg_6509" s="T384">ɨ͡aldʼɨt</ta>
            <ta e="T386" id="Seg_6510" s="T385">kel-l-e</ta>
            <ta e="T387" id="Seg_6511" s="T386">mač-čɨt-tar</ta>
            <ta e="T388" id="Seg_6512" s="T387">mas-taː-ŋ</ta>
            <ta e="T389" id="Seg_6513" s="T388">u͡ot-ta</ta>
            <ta e="T390" id="Seg_6514" s="T389">ott-u-ŋ</ta>
            <ta e="T391" id="Seg_6515" s="T390">mas-tara</ta>
            <ta e="T392" id="Seg_6516" s="T391">beje-lere</ta>
            <ta e="T393" id="Seg_6517" s="T392">kiːr-di-ler</ta>
            <ta e="T394" id="Seg_6518" s="T393">tahaːrattan</ta>
            <ta e="T395" id="Seg_6519" s="T394">u͡ol</ta>
            <ta e="T396" id="Seg_6520" s="T395">tu͡olkulaː-t-a</ta>
            <ta e="T397" id="Seg_6521" s="T396">timir</ta>
            <ta e="T398" id="Seg_6522" s="T397">mas-tar</ta>
            <ta e="T399" id="Seg_6523" s="T398">e-bit</ta>
            <ta e="T400" id="Seg_6524" s="T399">emeːksin</ta>
            <ta e="T401" id="Seg_6525" s="T400">tur-an</ta>
            <ta e="T402" id="Seg_6526" s="T401">mas-tar-ɨ</ta>
            <ta e="T403" id="Seg_6527" s="T402">ačaːg-ɨ-gar</ta>
            <ta e="T404" id="Seg_6528" s="T403">mus-t-a</ta>
            <ta e="T405" id="Seg_6529" s="T404">mataga</ta>
            <ta e="T406" id="Seg_6530" s="T405">di͡e-t-e</ta>
            <ta e="T407" id="Seg_6531" s="T406">kü͡ört</ta>
            <ta e="T408" id="Seg_6532" s="T407">kü͡ör-teː</ta>
            <ta e="T409" id="Seg_6533" s="T408">uraga</ta>
            <ta e="T410" id="Seg_6534" s="T409">dʼi͡e</ta>
            <ta e="T411" id="Seg_6535" s="T410">timir</ta>
            <ta e="T412" id="Seg_6536" s="T411">mu͡osta-ta</ta>
            <ta e="T413" id="Seg_6537" s="T412">kɨtar-čɨ</ta>
            <ta e="T414" id="Seg_6538" s="T413">bar-d-a</ta>
            <ta e="T415" id="Seg_6539" s="T414">mataga</ta>
            <ta e="T416" id="Seg_6540" s="T415">bi͡ek</ta>
            <ta e="T417" id="Seg_6541" s="T416">diː-r</ta>
            <ta e="T418" id="Seg_6542" s="T417">kü͡ört</ta>
            <ta e="T419" id="Seg_6543" s="T418">kü͡ör-teː</ta>
            <ta e="T420" id="Seg_6544" s="T419">kü͡ör-teː</ta>
            <ta e="T421" id="Seg_6545" s="T420">ɨ͡aldʼɨt</ta>
            <ta e="T422" id="Seg_6546" s="T421">kel-l-e</ta>
            <ta e="T423" id="Seg_6547" s="T422">u͡ot-ta</ta>
            <ta e="T424" id="Seg_6548" s="T423">ep</ta>
            <ta e="T425" id="Seg_6549" s="T424">uraha</ta>
            <ta e="T426" id="Seg_6550" s="T425">dʼi͡e</ta>
            <ta e="T427" id="Seg_6551" s="T426">barɨ-ta</ta>
            <ta e="T428" id="Seg_6552" s="T427">kɨtar-d-a</ta>
            <ta e="T429" id="Seg_6553" s="T428">emeːksin</ta>
            <ta e="T430" id="Seg_6554" s="T429">ɨ͡aldʼɨt</ta>
            <ta e="T431" id="Seg_6555" s="T430">u͡ol-u</ta>
            <ta e="T432" id="Seg_6556" s="T431">kördös-t-ö</ta>
            <ta e="T433" id="Seg_6557" s="T432">iti-t-i-me</ta>
            <ta e="T434" id="Seg_6558" s="T433">olus</ta>
            <ta e="T435" id="Seg_6559" s="T434">hoj-u-t</ta>
            <ta e="T436" id="Seg_6560" s="T435">dʼe</ta>
            <ta e="T437" id="Seg_6561" s="T436">u͡ol</ta>
            <ta e="T438" id="Seg_6562" s="T437">taːs</ta>
            <ta e="T439" id="Seg_6563" s="T438">hüreg-i</ta>
            <ta e="T440" id="Seg_6564" s="T439">ɨl-an</ta>
            <ta e="T441" id="Seg_6565" s="T440">ačaːk</ta>
            <ta e="T442" id="Seg_6566" s="T441">orto-tu-gar</ta>
            <ta e="T443" id="Seg_6567" s="T442">bɨrak-t-a</ta>
            <ta e="T444" id="Seg_6568" s="T443">uraha</ta>
            <ta e="T445" id="Seg_6569" s="T444">dʼi͡e</ta>
            <ta e="T446" id="Seg_6570" s="T445">hoj-d-o</ta>
            <ta e="T447" id="Seg_6571" s="T446">mu͡osta-ta</ta>
            <ta e="T448" id="Seg_6572" s="T447">kɨtar-ba-t-a</ta>
            <ta e="T449" id="Seg_6573" s="T448">emeːksin</ta>
            <ta e="T450" id="Seg_6574" s="T449">emi͡e</ta>
            <ta e="T451" id="Seg_6575" s="T450">kam</ta>
            <ta e="T452" id="Seg_6576" s="T451">kɨrɨ͡a</ta>
            <ta e="T453" id="Seg_6577" s="T452">bu͡ol-l-a</ta>
            <ta e="T454" id="Seg_6578" s="T453">olus</ta>
            <ta e="T455" id="Seg_6579" s="T454">toŋ-o-r-u-ma</ta>
            <ta e="T456" id="Seg_6580" s="T455">kördös-püt</ta>
            <ta e="T0" id="Seg_6581" s="T456">mataga</ta>
            <ta e="T947" id="Seg_6582" s="T0">barɨ</ta>
            <ta e="T948" id="Seg_6583" s="T947">hanaː-gɨ-n</ta>
            <ta e="T457" id="Seg_6584" s="T948">tolor-u͡o-m</ta>
            <ta e="T458" id="Seg_6585" s="T457">mataga</ta>
            <ta e="T459" id="Seg_6586" s="T458">e-bit</ta>
            <ta e="T460" id="Seg_6587" s="T459">hir</ta>
            <ta e="T461" id="Seg_6588" s="T460">ičči-te</ta>
            <ta e="T462" id="Seg_6589" s="T461">u͡ol</ta>
            <ta e="T463" id="Seg_6590" s="T462">taːs</ta>
            <ta e="T464" id="Seg_6591" s="T463">hüreg-i</ta>
            <ta e="T465" id="Seg_6592" s="T464">ačaːk</ta>
            <ta e="T466" id="Seg_6593" s="T465">ih-i-tten</ta>
            <ta e="T467" id="Seg_6594" s="T466">oroː-n</ta>
            <ta e="T468" id="Seg_6595" s="T467">ɨl-l-a</ta>
            <ta e="T469" id="Seg_6596" s="T468">u͡ot-a</ta>
            <ta e="T470" id="Seg_6597" s="T469">emi͡e</ta>
            <ta e="T471" id="Seg_6598" s="T470">gɨdaːrɨj-d-a</ta>
            <ta e="T472" id="Seg_6599" s="T471">mataga</ta>
            <ta e="T473" id="Seg_6600" s="T472">di͡e-t-e</ta>
            <ta e="T474" id="Seg_6601" s="T473">u͡ol-ga</ta>
            <ta e="T475" id="Seg_6602" s="T474">tahaːra</ta>
            <ta e="T476" id="Seg_6603" s="T475">tagɨs</ta>
            <ta e="T477" id="Seg_6604" s="T476">onno</ta>
            <ta e="T478" id="Seg_6605" s="T477">hɨrga-laːk</ta>
            <ta e="T479" id="Seg_6606" s="T478">taba</ta>
            <ta e="T480" id="Seg_6607" s="T479">tur-ar</ta>
            <ta e="T481" id="Seg_6608" s="T480">u͡ol</ta>
            <ta e="T482" id="Seg_6609" s="T481">tahaːra</ta>
            <ta e="T483" id="Seg_6610" s="T482">taks-an</ta>
            <ta e="T484" id="Seg_6611" s="T483">tu͡ok</ta>
            <ta e="T485" id="Seg_6612" s="T484">da</ta>
            <ta e="T486" id="Seg_6613" s="T485">hɨrga-laːk</ta>
            <ta e="T487" id="Seg_6614" s="T486">taba-tɨ-n</ta>
            <ta e="T488" id="Seg_6615" s="T487">kör-böt</ta>
            <ta e="T489" id="Seg_6616" s="T488">kanna</ta>
            <ta e="T490" id="Seg_6617" s="T489">ere</ta>
            <ta e="T491" id="Seg_6618" s="T490">ču͡oraːn</ta>
            <ta e="T492" id="Seg_6619" s="T491">gubaːrka</ta>
            <ta e="T493" id="Seg_6620" s="T492">ere</ta>
            <ta e="T494" id="Seg_6621" s="T493">tɨ͡ah-a</ta>
            <ta e="T495" id="Seg_6622" s="T494">ihill-er</ta>
            <ta e="T496" id="Seg_6623" s="T495">keteg-e-n</ta>
            <ta e="T497" id="Seg_6624" s="T496">di͡ek</ta>
            <ta e="T498" id="Seg_6625" s="T497">mataga</ta>
            <ta e="T499" id="Seg_6626" s="T498">haŋa-ta</ta>
            <ta e="T500" id="Seg_6627" s="T499">ihill-i-bit</ta>
            <ta e="T501" id="Seg_6628" s="T500">olor</ta>
            <ta e="T502" id="Seg_6629" s="T501">hɨrga-ga-r</ta>
            <ta e="T503" id="Seg_6630" s="T502">keŋn-i-ŋ</ta>
            <ta e="T504" id="Seg_6631" s="T503">di͡ek</ta>
            <ta e="T505" id="Seg_6632" s="T504">kanʼɨh-ɨ-ma</ta>
            <ta e="T506" id="Seg_6633" s="T505">hu͡ol</ta>
            <ta e="T507" id="Seg_6634" s="T506">aːra</ta>
            <ta e="T508" id="Seg_6635" s="T507">toktoː-mo</ta>
            <ta e="T509" id="Seg_6636" s="T508">taba-lar-gɨ-n</ta>
            <ta e="T510" id="Seg_6637" s="T509">ah-a-t-ɨ-ma</ta>
            <ta e="T511" id="Seg_6638" s="T510">u͡ol</ta>
            <ta e="T512" id="Seg_6639" s="T511">iliː-ti-n</ta>
            <ta e="T513" id="Seg_6640" s="T512">uːn-n-a</ta>
            <ta e="T514" id="Seg_6641" s="T513">nʼuŋuː-nu</ta>
            <ta e="T515" id="Seg_6642" s="T514">kap-t-a</ta>
            <ta e="T516" id="Seg_6643" s="T515">köst-ü-bet</ta>
            <ta e="T517" id="Seg_6644" s="T516">hɨrga-ga</ta>
            <ta e="T518" id="Seg_6645" s="T517">miːn-n-e</ta>
            <ta e="T519" id="Seg_6646" s="T518">tɨ͡al-laːk</ta>
            <ta e="T520" id="Seg_6647" s="T519">purga</ta>
            <ta e="T521" id="Seg_6648" s="T520">ogus-t-a</ta>
            <ta e="T522" id="Seg_6649" s="T521">kačča-nɨ</ta>
            <ta e="T523" id="Seg_6650" s="T522">bar-bɨt-a</ta>
            <ta e="T524" id="Seg_6651" s="T523">dʼürü</ta>
            <ta e="T525" id="Seg_6652" s="T524">kim</ta>
            <ta e="T526" id="Seg_6653" s="T525">bil-i͡e=j</ta>
            <ta e="T527" id="Seg_6654" s="T526">hɨrga-laːk</ta>
            <ta e="T528" id="Seg_6655" s="T527">taba-lara</ta>
            <ta e="T529" id="Seg_6656" s="T528">beje-lere</ta>
            <ta e="T530" id="Seg_6657" s="T529">ill-el-ler</ta>
            <ta e="T531" id="Seg_6658" s="T530">u͡ol</ta>
            <ta e="T532" id="Seg_6659" s="T531">tom-mot</ta>
            <ta e="T533" id="Seg_6660" s="T532">iliː-ti-n</ta>
            <ta e="T534" id="Seg_6661" s="T533">olbog-u-n</ta>
            <ta e="T535" id="Seg_6662" s="T534">ih-i-ger</ta>
            <ta e="T536" id="Seg_6663" s="T535">uk-t-a</ta>
            <ta e="T537" id="Seg_6664" s="T536">onno</ta>
            <ta e="T538" id="Seg_6665" s="T537">itiː</ta>
            <ta e="T539" id="Seg_6666" s="T538">timir</ta>
            <ta e="T540" id="Seg_6667" s="T539">mas</ta>
            <ta e="T541" id="Seg_6668" s="T540">hɨt-ar</ta>
            <ta e="T542" id="Seg_6669" s="T541">e-bit</ta>
            <ta e="T543" id="Seg_6670" s="T542">on-tu-ta</ta>
            <ta e="T544" id="Seg_6671" s="T543">it-i-t-er</ta>
            <ta e="T545" id="Seg_6672" s="T544">ebe</ta>
            <ta e="T546" id="Seg_6673" s="T545">ürd-ü-ger</ta>
            <ta e="T547" id="Seg_6674" s="T546">kör-d-ö</ta>
            <ta e="T548" id="Seg_6675" s="T547">hɨrga-ga</ta>
            <ta e="T549" id="Seg_6676" s="T548">öl-büt</ta>
            <ta e="T550" id="Seg_6677" s="T549">dʼaktar</ta>
            <ta e="T551" id="Seg_6678" s="T550">hɨt-ar</ta>
            <ta e="T552" id="Seg_6679" s="T551">kam</ta>
            <ta e="T553" id="Seg_6680" s="T552">tib-i-ll-en</ta>
            <ta e="T554" id="Seg_6681" s="T553">baraːn</ta>
            <ta e="T555" id="Seg_6682" s="T554">tehij-bekke</ta>
            <ta e="T556" id="Seg_6683" s="T555">toktoː-t-o</ta>
            <ta e="T557" id="Seg_6684" s="T556">toktuː-r-u-n</ta>
            <ta e="T558" id="Seg_6685" s="T557">kɨtta</ta>
            <ta e="T559" id="Seg_6686" s="T558">nʼuŋuhut-a</ta>
            <ta e="T560" id="Seg_6687" s="T559">hulbu</ta>
            <ta e="T561" id="Seg_6688" s="T560">ɨstan-an</ta>
            <ta e="T562" id="Seg_6689" s="T561">kajdi͡e</ta>
            <ta e="T563" id="Seg_6690" s="T562">bar-bɨt-a</ta>
            <ta e="T564" id="Seg_6691" s="T563">kajdi͡ek</ta>
            <ta e="T565" id="Seg_6692" s="T564">kel-bit-e</ta>
            <ta e="T566" id="Seg_6693" s="T565">üs</ta>
            <ta e="T567" id="Seg_6694" s="T566">koŋnomu͡oj</ta>
            <ta e="T568" id="Seg_6695" s="T567">taba-laːk</ta>
            <ta e="T569" id="Seg_6696" s="T568">e-t-e</ta>
            <ta e="T570" id="Seg_6697" s="T569">e-bit</ta>
            <ta e="T571" id="Seg_6698" s="T570">nʼuŋuhut-a</ta>
            <ta e="T572" id="Seg_6699" s="T571">bɨst-an</ta>
            <ta e="T573" id="Seg_6700" s="T572">bar-an</ta>
            <ta e="T574" id="Seg_6701" s="T573">kaːl-bɨt</ta>
            <ta e="T575" id="Seg_6702" s="T574">mataga</ta>
            <ta e="T576" id="Seg_6703" s="T575">haŋa-ta</ta>
            <ta e="T577" id="Seg_6704" s="T576">ihill-i-bit</ta>
            <ta e="T578" id="Seg_6705" s="T577">eni͡e-ke</ta>
            <ta e="T579" id="Seg_6706" s="T578">di͡e-bit-i-m</ta>
            <ta e="T580" id="Seg_6707" s="T579">toktoː-mo</ta>
            <ta e="T581" id="Seg_6708" s="T580">u͡ol</ta>
            <ta e="T582" id="Seg_6709" s="T581">hɨrga-tɨ-ttan</ta>
            <ta e="T583" id="Seg_6710" s="T582">kɨhɨl</ta>
            <ta e="T584" id="Seg_6711" s="T583">timir</ta>
            <ta e="T585" id="Seg_6712" s="T584">mah-ɨ</ta>
            <ta e="T586" id="Seg_6713" s="T585">ɨl-an</ta>
            <ta e="T587" id="Seg_6714" s="T586">kɨːs</ta>
            <ta e="T588" id="Seg_6715" s="T587">hɨrga-tɨ-n</ta>
            <ta e="T589" id="Seg_6716" s="T588">ulka-tɨ-gar</ta>
            <ta e="T590" id="Seg_6717" s="T589">batarɨ</ta>
            <ta e="T591" id="Seg_6718" s="T590">as-t-a</ta>
            <ta e="T592" id="Seg_6719" s="T591">kaːr</ta>
            <ta e="T593" id="Seg_6720" s="T592">honno</ta>
            <ta e="T594" id="Seg_6721" s="T593">ir-en</ta>
            <ta e="T595" id="Seg_6722" s="T594">kaːl-l-a</ta>
            <ta e="T596" id="Seg_6723" s="T595">toŋ-on</ta>
            <ta e="T597" id="Seg_6724" s="T596">öl-büt</ta>
            <ta e="T598" id="Seg_6725" s="T597">dʼaktar-a</ta>
            <ta e="T599" id="Seg_6726" s="T598">tilin-n-e</ta>
            <ta e="T600" id="Seg_6727" s="T599">kör-büt-e</ta>
            <ta e="T601" id="Seg_6728" s="T600">bert</ta>
            <ta e="T602" id="Seg_6729" s="T601">basku͡oj</ta>
            <ta e="T603" id="Seg_6730" s="T602">bagajɨ</ta>
            <ta e="T604" id="Seg_6731" s="T603">kɨːs</ta>
            <ta e="T605" id="Seg_6732" s="T604">e-bit</ta>
            <ta e="T606" id="Seg_6733" s="T605">kɨːs</ta>
            <ta e="T607" id="Seg_6734" s="T606">ü͡ör-en</ta>
            <ta e="T608" id="Seg_6735" s="T607">muŋ-a</ta>
            <ta e="T609" id="Seg_6736" s="T608">hu͡ok</ta>
            <ta e="T610" id="Seg_6737" s="T609">anɨ</ta>
            <ta e="T611" id="Seg_6738" s="T610">beje-m</ta>
            <ta e="T612" id="Seg_6739" s="T611">dʼi͡e-be-r</ta>
            <ta e="T613" id="Seg_6740" s="T612">tiːj-i͡e-m</ta>
            <ta e="T614" id="Seg_6741" s="T613">bars-ɨ-bak-kɨn</ta>
            <ta e="T615" id="Seg_6742" s="T614">du͡o</ta>
            <ta e="T616" id="Seg_6743" s="T615">ah-a-t-ɨ͡a-m</ta>
            <ta e="T617" id="Seg_6744" s="T616">ičiges</ta>
            <ta e="T618" id="Seg_6745" s="T617">tellek-ke</ta>
            <ta e="T619" id="Seg_6746" s="T618">telg-i͡e-m</ta>
            <ta e="T620" id="Seg_6747" s="T619">kördös-püt</ta>
            <ta e="T621" id="Seg_6748" s="T620">kɨːs</ta>
            <ta e="T622" id="Seg_6749" s="T621">ɨraːk</ta>
            <ta e="T623" id="Seg_6750" s="T622">du͡o</ta>
            <ta e="T624" id="Seg_6751" s="T623">dʼi͡e-ŋ</ta>
            <ta e="T625" id="Seg_6752" s="T624">uː</ta>
            <ta e="T626" id="Seg_6753" s="T625">ih-i-ger</ta>
            <ta e="T627" id="Seg_6754" s="T626">u͡ol</ta>
            <ta e="T628" id="Seg_6755" s="T627">ügüs</ta>
            <ta e="T629" id="Seg_6756" s="T628">oduː-nu</ta>
            <ta e="T630" id="Seg_6757" s="T629">kör-büt-e</ta>
            <ta e="T631" id="Seg_6758" s="T630">ol</ta>
            <ta e="T632" id="Seg_6759" s="T631">ihin</ta>
            <ta e="T633" id="Seg_6760" s="T632">kɨːs</ta>
            <ta e="T634" id="Seg_6761" s="T633">öh-ü-n</ta>
            <ta e="T635" id="Seg_6762" s="T634">hök-pö-t-ö</ta>
            <ta e="T636" id="Seg_6763" s="T635">d-iː</ta>
            <ta e="T637" id="Seg_6764" s="T636">e-bit</ta>
            <ta e="T638" id="Seg_6765" s="T637">min</ta>
            <ta e="T639" id="Seg_6766" s="T638">edʼiːj-bi-n</ta>
            <ta e="T640" id="Seg_6767" s="T639">bul-u͡ok-pu-n</ta>
            <ta e="T641" id="Seg_6768" s="T640">naːda</ta>
            <ta e="T642" id="Seg_6769" s="T641">kojut</ta>
            <ta e="T643" id="Seg_6770" s="T642">kel-i͡e-m</ta>
            <ta e="T644" id="Seg_6771" s="T643">eːt</ta>
            <ta e="T645" id="Seg_6772" s="T644">küːt</ta>
            <ta e="T646" id="Seg_6773" s="T645">u͡ol</ta>
            <ta e="T647" id="Seg_6774" s="T646">töhö-nü-kačča-nɨ</ta>
            <ta e="T648" id="Seg_6775" s="T647">bar-bɨt-a</ta>
            <ta e="T649" id="Seg_6776" s="T648">kim</ta>
            <ta e="T650" id="Seg_6777" s="T649">da</ta>
            <ta e="T651" id="Seg_6778" s="T650">bil-bet</ta>
            <ta e="T652" id="Seg_6779" s="T651">araj</ta>
            <ta e="T653" id="Seg_6780" s="T652">kadʼɨrɨk</ta>
            <ta e="T654" id="Seg_6781" s="T653">bu͡ol-but</ta>
            <ta e="T655" id="Seg_6782" s="T654">ahɨːlɨk-taːk</ta>
            <ta e="T656" id="Seg_6783" s="T655">bagajɨ</ta>
            <ta e="T657" id="Seg_6784" s="T656">taba-lar-a</ta>
            <ta e="T658" id="Seg_6785" s="T657">aččɨk-taː-tɨ-lar</ta>
            <ta e="T659" id="Seg_6786" s="T658">ah-a-t-ɨ͡ak-ka</ta>
            <ta e="T660" id="Seg_6787" s="T659">duː</ta>
            <ta e="T661" id="Seg_6788" s="T660">d-iː</ta>
            <ta e="T662" id="Seg_6789" s="T661">hanaː-bɨt</ta>
            <ta e="T663" id="Seg_6790" s="T662">hir</ta>
            <ta e="T664" id="Seg_6791" s="T663">ičči-ti-n</ta>
            <ta e="T665" id="Seg_6792" s="T664">haŋa-tɨ-n</ta>
            <ta e="T666" id="Seg_6793" s="T665">öjd-üː-öjd-üː</ta>
            <ta e="T667" id="Seg_6794" s="T666">taba-lar-ɨ-n</ta>
            <ta e="T668" id="Seg_6795" s="T667">toktoː-p-po-t-o</ta>
            <ta e="T669" id="Seg_6796" s="T668">onton</ta>
            <ta e="T670" id="Seg_6797" s="T669">d-iː</ta>
            <ta e="T671" id="Seg_6798" s="T670">hanaː-t-a</ta>
            <ta e="T672" id="Seg_6799" s="T671">olus</ta>
            <ta e="T673" id="Seg_6800" s="T672">aččɨk-taː-bɨt-tar</ta>
            <ta e="T674" id="Seg_6801" s="T673">tokt-o-t-u͡ok-ka</ta>
            <ta e="T675" id="Seg_6802" s="T674">tokt-o-t-or-u-n</ta>
            <ta e="T676" id="Seg_6803" s="T675">kɨtta</ta>
            <ta e="T677" id="Seg_6804" s="T676">orto-kuː</ta>
            <ta e="T678" id="Seg_6805" s="T677">kölüː-r</ta>
            <ta e="T679" id="Seg_6806" s="T678">taba-ta</ta>
            <ta e="T680" id="Seg_6807" s="T679">meli-s</ta>
            <ta e="T681" id="Seg_6808" s="T680">ere</ta>
            <ta e="T682" id="Seg_6809" s="T681">gɨn-an</ta>
            <ta e="T683" id="Seg_6810" s="T682">kaːl-bɨt</ta>
            <ta e="T684" id="Seg_6811" s="T683">kajdi͡e</ta>
            <ta e="T685" id="Seg_6812" s="T684">bar-bɨt-a</ta>
            <ta e="T686" id="Seg_6813" s="T685">kajdi͡ek</ta>
            <ta e="T687" id="Seg_6814" s="T686">köp-püt-e</ta>
            <ta e="T688" id="Seg_6815" s="T687">emi͡e</ta>
            <ta e="T689" id="Seg_6816" s="T688">haŋa</ta>
            <ta e="T690" id="Seg_6817" s="T689">ihill-i-bit</ta>
            <ta e="T691" id="Seg_6818" s="T690">min</ta>
            <ta e="T692" id="Seg_6819" s="T691">eni͡e-ke</ta>
            <ta e="T693" id="Seg_6820" s="T692">di͡e-bit-i-m</ta>
            <ta e="T694" id="Seg_6821" s="T693">ah-a-t-ɨ-ma</ta>
            <ta e="T695" id="Seg_6822" s="T694">taba-lar-gɨ-n</ta>
            <ta e="T696" id="Seg_6823" s="T695">u͡ol</ta>
            <ta e="T697" id="Seg_6824" s="T696">töhö-nü</ta>
            <ta e="T698" id="Seg_6825" s="T697">bar-bɨt-a</ta>
            <ta e="T699" id="Seg_6826" s="T698">dʼürü</ta>
            <ta e="T700" id="Seg_6827" s="T699">maː</ta>
            <ta e="T701" id="Seg_6828" s="T700">taːs</ta>
            <ta e="T702" id="Seg_6829" s="T701">kaja-tɨ-gar</ta>
            <ta e="T703" id="Seg_6830" s="T702">tiːj-d-e</ta>
            <ta e="T704" id="Seg_6831" s="T703">ikki</ta>
            <ta e="T705" id="Seg_6832" s="T704">aŋɨ</ta>
            <ta e="T706" id="Seg_6833" s="T705">kajd-aːččɨ-tɨ-gar</ta>
            <ta e="T707" id="Seg_6834" s="T706">araj</ta>
            <ta e="T708" id="Seg_6835" s="T707">beje-ti-n</ta>
            <ta e="T709" id="Seg_6836" s="T708">hir-i-ger</ta>
            <ta e="T710" id="Seg_6837" s="T709">tiːj-bit</ta>
            <ta e="T711" id="Seg_6838" s="T710">e-bit</ta>
            <ta e="T712" id="Seg_6839" s="T711">toŋ-on</ta>
            <ta e="T713" id="Seg_6840" s="T712">öl-büt</ta>
            <ta e="T714" id="Seg_6841" s="T713">taba-lar</ta>
            <ta e="T715" id="Seg_6842" s="T714">mu͡os-tara</ta>
            <ta e="T716" id="Seg_6843" s="T715">araččɨ</ta>
            <ta e="T717" id="Seg_6844" s="T716">kaːr</ta>
            <ta e="T718" id="Seg_6845" s="T717">ih-i-tten</ta>
            <ta e="T719" id="Seg_6846" s="T718">čoroh-ol-lor</ta>
            <ta e="T720" id="Seg_6847" s="T719">kaːr-ɨ</ta>
            <ta e="T721" id="Seg_6848" s="T720">kürdʼ-en</ta>
            <ta e="T722" id="Seg_6849" s="T721">edʼiːj-i-n</ta>
            <ta e="T723" id="Seg_6850" s="T722">nʼu͡oŋuhut-u-n</ta>
            <ta e="T724" id="Seg_6851" s="T723">bul-l-a</ta>
            <ta e="T725" id="Seg_6852" s="T724">itiː</ta>
            <ta e="T726" id="Seg_6853" s="T725">timir</ta>
            <ta e="T727" id="Seg_6854" s="T726">mah-ɨ-nan</ta>
            <ta e="T728" id="Seg_6855" s="T727">taːrɨj-d-a</ta>
            <ta e="T729" id="Seg_6856" s="T728">taba-ta</ta>
            <ta e="T730" id="Seg_6857" s="T729">till-en</ta>
            <ta e="T731" id="Seg_6858" s="T730">baraːn</ta>
            <ta e="T732" id="Seg_6859" s="T731">tur-d-a</ta>
            <ta e="T733" id="Seg_6860" s="T732">onton</ta>
            <ta e="T734" id="Seg_6861" s="T733">mah-ɨ</ta>
            <ta e="T735" id="Seg_6862" s="T734">ɨl-l-a</ta>
            <ta e="T736" id="Seg_6863" s="T735">kaːr-ga</ta>
            <ta e="T737" id="Seg_6864" s="T736">as-t-a</ta>
            <ta e="T738" id="Seg_6865" s="T737">toŋ-on</ta>
            <ta e="T739" id="Seg_6866" s="T738">öl-büt</ta>
            <ta e="T740" id="Seg_6867" s="T739">taba-lar</ta>
            <ta e="T741" id="Seg_6868" s="T740">honno</ta>
            <ta e="T742" id="Seg_6869" s="T741">barɨ-lara</ta>
            <ta e="T743" id="Seg_6870" s="T742">tilin-ni-ler</ta>
            <ta e="T744" id="Seg_6871" s="T743">bütün</ta>
            <ta e="T745" id="Seg_6872" s="T744">ü͡ör</ta>
            <ta e="T746" id="Seg_6873" s="T745">bu͡ol-an</ta>
            <ta e="T747" id="Seg_6874" s="T746">ah-ɨː</ta>
            <ta e="T748" id="Seg_6875" s="T747">hɨldʼ-al-lar</ta>
            <ta e="T749" id="Seg_6876" s="T748">haŋa</ta>
            <ta e="T750" id="Seg_6877" s="T749">hir-i-nen</ta>
            <ta e="T751" id="Seg_6878" s="T750">kanʼɨs-pɨt-a</ta>
            <ta e="T752" id="Seg_6879" s="T751">edʼiːj-e</ta>
            <ta e="T753" id="Seg_6880" s="T752">tur-ar</ta>
            <ta e="T754" id="Seg_6881" s="T753">u͡ol</ta>
            <ta e="T755" id="Seg_6882" s="T754">edʼiːj-i-n</ta>
            <ta e="T756" id="Seg_6883" s="T755">kör-ön</ta>
            <ta e="T757" id="Seg_6884" s="T756">bert</ta>
            <ta e="T758" id="Seg_6885" s="T757">ü͡ör-d-e</ta>
            <ta e="T759" id="Seg_6886" s="T758">maŋnaj</ta>
            <ta e="T760" id="Seg_6887" s="T759">en</ta>
            <ta e="T761" id="Seg_6888" s="T760">di͡e-t-e</ta>
            <ta e="T762" id="Seg_6889" s="T761">edʼiːj-e</ta>
            <ta e="T763" id="Seg_6890" s="T762">ör</ta>
            <ta e="T764" id="Seg_6891" s="T763">bagaj-dɨk</ta>
            <ta e="T765" id="Seg_6892" s="T764">utuj-but-u-ŋ</ta>
            <ta e="T766" id="Seg_6893" s="T765">onton</ta>
            <ta e="T767" id="Seg_6894" s="T766">min</ta>
            <ta e="T768" id="Seg_6895" s="T767">haːs</ta>
            <ta e="T769" id="Seg_6896" s="T768">bu͡ol-but</ta>
            <ta e="T770" id="Seg_6897" s="T769">e-bit</ta>
            <ta e="T771" id="Seg_6898" s="T770">kɨhɨn-ɨ</ta>
            <ta e="T772" id="Seg_6899" s="T771">meldʼi</ta>
            <ta e="T773" id="Seg_6900" s="T772">utuj-bup-pun</ta>
            <ta e="T774" id="Seg_6901" s="T773">e-bit</ta>
            <ta e="T775" id="Seg_6902" s="T774">u͡ol</ta>
            <ta e="T776" id="Seg_6903" s="T775">edʼiːj-i-n</ta>
            <ta e="T777" id="Seg_6904" s="T776">dʼi͡e-ti-ger</ta>
            <ta e="T778" id="Seg_6905" s="T777">il-t-e</ta>
            <ta e="T779" id="Seg_6906" s="T778">ü͡ör-ü-n</ta>
            <ta e="T780" id="Seg_6907" s="T779">üːr-en</ta>
            <ta e="T781" id="Seg_6908" s="T780">egel-l-e</ta>
            <ta e="T782" id="Seg_6909" s="T781">töhö</ta>
            <ta e="T783" id="Seg_6910" s="T782">da</ta>
            <ta e="T784" id="Seg_6911" s="T783">bu͡ol-bakka</ta>
            <ta e="T785" id="Seg_6912" s="T784">bu</ta>
            <ta e="T786" id="Seg_6913" s="T785">u͡ol</ta>
            <ta e="T787" id="Seg_6914" s="T786">terin-n-e</ta>
            <ta e="T788" id="Seg_6915" s="T787">kajdi͡e</ta>
            <ta e="T789" id="Seg_6916" s="T788">bar-a-gɨn</ta>
            <ta e="T790" id="Seg_6917" s="T789">ɨjɨp-pɨt</ta>
            <ta e="T791" id="Seg_6918" s="T790">edʼiːj-e</ta>
            <ta e="T792" id="Seg_6919" s="T791">hotoru</ta>
            <ta e="T793" id="Seg_6920" s="T792">kel-i͡e-m</ta>
            <ta e="T794" id="Seg_6921" s="T793">kečeh-i-me</ta>
            <ta e="T795" id="Seg_6922" s="T794">u͡ol</ta>
            <ta e="T796" id="Seg_6923" s="T795">maː</ta>
            <ta e="T797" id="Seg_6924" s="T796">kɨːh-ɨ</ta>
            <ta e="T798" id="Seg_6925" s="T797">körd-üː</ta>
            <ta e="T799" id="Seg_6926" s="T798">bar-ɨːhɨ</ta>
            <ta e="T800" id="Seg_6927" s="T799">uː</ta>
            <ta e="T801" id="Seg_6928" s="T800">ih-i-ger</ta>
            <ta e="T802" id="Seg_6929" s="T801">dʼi͡e-leːg-i</ta>
            <ta e="T803" id="Seg_6930" s="T802">baran</ta>
            <ta e="T804" id="Seg_6931" s="T803">ih-en</ta>
            <ta e="T805" id="Seg_6932" s="T804">mɨlaː</ta>
            <ta e="T806" id="Seg_6933" s="T805">bagaj</ta>
            <ta e="T807" id="Seg_6934" s="T806">purgaː</ta>
            <ta e="T808" id="Seg_6935" s="T807">bu͡ol-but</ta>
            <ta e="T809" id="Seg_6936" s="T808">hir-i-nen</ta>
            <ta e="T810" id="Seg_6937" s="T809">da</ta>
            <ta e="T811" id="Seg_6938" s="T810">bar-ar-ɨ-n</ta>
            <ta e="T812" id="Seg_6939" s="T811">bil-bet</ta>
            <ta e="T813" id="Seg_6940" s="T812">buːh-u-nan</ta>
            <ta e="T814" id="Seg_6941" s="T813">da</ta>
            <ta e="T815" id="Seg_6942" s="T814">bar-ar-ɨ-n</ta>
            <ta e="T816" id="Seg_6943" s="T815">bil-bet</ta>
            <ta e="T817" id="Seg_6944" s="T816">öj-üleː-n</ta>
            <ta e="T818" id="Seg_6945" s="T817">kör-büt-e</ta>
            <ta e="T819" id="Seg_6946" s="T818">bajgal</ta>
            <ta e="T820" id="Seg_6947" s="T819">buːh-a</ta>
            <ta e="T821" id="Seg_6948" s="T820">e-bit</ta>
            <ta e="T822" id="Seg_6949" s="T821">haːs-kɨ</ta>
            <ta e="T823" id="Seg_6950" s="T822">buːs</ta>
            <ta e="T824" id="Seg_6951" s="T823">iŋn-en</ta>
            <ta e="T825" id="Seg_6952" s="T824">u͡ol</ta>
            <ta e="T826" id="Seg_6953" s="T825">uː-ga</ta>
            <ta e="T827" id="Seg_6954" s="T826">tüs-püt</ta>
            <ta e="T828" id="Seg_6955" s="T827">tüh-en</ta>
            <ta e="T829" id="Seg_6956" s="T828">hordoŋ</ta>
            <ta e="T830" id="Seg_6957" s="T829">bu͡ol-but</ta>
            <ta e="T831" id="Seg_6958" s="T830">uː</ta>
            <ta e="T832" id="Seg_6959" s="T831">ih-i-nen</ta>
            <ta e="T833" id="Seg_6960" s="T832">ust-a</ta>
            <ta e="T834" id="Seg_6961" s="T833">hɨldʼ-an</ta>
            <ta e="T835" id="Seg_6962" s="T834">ilim-ŋe</ta>
            <ta e="T836" id="Seg_6963" s="T835">tübes-pit</ta>
            <ta e="T837" id="Seg_6964" s="T836">i͡enneːk</ta>
            <ta e="T838" id="Seg_6965" s="T837">ilim-ŋe</ta>
            <ta e="T839" id="Seg_6966" s="T838">hordoŋ-nor</ta>
            <ta e="T840" id="Seg_6967" s="T839">agaj</ta>
            <ta e="T841" id="Seg_6968" s="T840">töhö</ta>
            <ta e="T842" id="Seg_6969" s="T841">da</ta>
            <ta e="T843" id="Seg_6970" s="T842">bu͡ol-bakka</ta>
            <ta e="T844" id="Seg_6971" s="T843">kɨːs</ta>
            <ta e="T845" id="Seg_6972" s="T844">kel-l-e</ta>
            <ta e="T846" id="Seg_6973" s="T845">maː</ta>
            <ta e="T847" id="Seg_6974" s="T846">kɨːh-a</ta>
            <ta e="T848" id="Seg_6975" s="T847">tilin-ner-eːčči-te</ta>
            <ta e="T849" id="Seg_6976" s="T848">hordoŋ</ta>
            <ta e="T850" id="Seg_6977" s="T849">u͡ol-u</ta>
            <ta e="T851" id="Seg_6978" s="T850">nʼu͡oŋuhut</ta>
            <ta e="T852" id="Seg_6979" s="T851">gɨn-an</ta>
            <ta e="T853" id="Seg_6980" s="T852">baraːn</ta>
            <ta e="T854" id="Seg_6981" s="T853">kölün-n-e</ta>
            <ta e="T855" id="Seg_6982" s="T854">dʼi͡e-ti-ger</ta>
            <ta e="T856" id="Seg_6983" s="T855">bar-ɨːhɨ</ta>
            <ta e="T857" id="Seg_6984" s="T856">nʼu͡oŋuhut</ta>
            <ta e="T858" id="Seg_6985" s="T857">ist-i-bet</ta>
            <ta e="T859" id="Seg_6986" s="T858">kɨtɨl</ta>
            <ta e="T860" id="Seg_6987" s="T859">ere</ta>
            <ta e="T861" id="Seg_6988" s="T860">di͡ek</ta>
            <ta e="T862" id="Seg_6989" s="T861">talah-ar</ta>
            <ta e="T863" id="Seg_6990" s="T862">kɨːs</ta>
            <ta e="T864" id="Seg_6991" s="T863">o-nu</ta>
            <ta e="T865" id="Seg_6992" s="T864">halaj-a</ta>
            <ta e="T866" id="Seg_6993" s="T865">hatɨː-r</ta>
            <ta e="T867" id="Seg_6994" s="T866">nʼu͡oŋuhut-a</ta>
            <ta e="T868" id="Seg_6995" s="T867">bɨh-a</ta>
            <ta e="T869" id="Seg_6996" s="T868">mökküj-en</ta>
            <ta e="T870" id="Seg_6997" s="T869">kɨtɨl-ga</ta>
            <ta e="T871" id="Seg_6998" s="T870">tagɨs-t-a</ta>
            <ta e="T872" id="Seg_6999" s="T871">taks-aːt</ta>
            <ta e="T873" id="Seg_7000" s="T872">u͡ol-hordoŋ</ta>
            <ta e="T874" id="Seg_7001" s="T873">ile</ta>
            <ta e="T875" id="Seg_7002" s="T874">u͡ol</ta>
            <ta e="T876" id="Seg_7003" s="T875">bu͡ol-but</ta>
            <ta e="T877" id="Seg_7004" s="T876">hordoŋ-nor</ta>
            <ta e="T878" id="Seg_7005" s="T877">taba</ta>
            <ta e="T879" id="Seg_7006" s="T878">bu͡ol-lu-lar</ta>
            <ta e="T880" id="Seg_7007" s="T879">kɨːs-taːk</ta>
            <ta e="T881" id="Seg_7008" s="T880">u͡ol</ta>
            <ta e="T882" id="Seg_7009" s="T881">taːj-ɨ-s-tɨ-lar</ta>
            <ta e="T883" id="Seg_7010" s="T882">kɨːs</ta>
            <ta e="T884" id="Seg_7011" s="T883">ɨjɨt-t-a</ta>
            <ta e="T885" id="Seg_7012" s="T884">edʼiːj-gi-n</ta>
            <ta e="T886" id="Seg_7013" s="T885">bul-lu-ŋ</ta>
            <ta e="T887" id="Seg_7014" s="T886">kajtak</ta>
            <ta e="T888" id="Seg_7015" s="T887">olor-o-gun</ta>
            <ta e="T889" id="Seg_7016" s="T888">barɨs</ta>
            <ta e="T890" id="Seg_7017" s="T889">minigi-n</ta>
            <ta e="T891" id="Seg_7018" s="T890">kɨtta</ta>
            <ta e="T892" id="Seg_7019" s="T891">di͡e-t-e</ta>
            <ta e="T893" id="Seg_7020" s="T892">u͡ol</ta>
            <ta e="T894" id="Seg_7021" s="T893">edʼiːj-i-m</ta>
            <ta e="T895" id="Seg_7022" s="T894">enigi-n</ta>
            <ta e="T896" id="Seg_7023" s="T895">küːt-er</ta>
            <ta e="T897" id="Seg_7024" s="T896">bil-s-i-h-i-ŋ</ta>
            <ta e="T898" id="Seg_7025" s="T897">biːrge</ta>
            <ta e="T899" id="Seg_7026" s="T898">olor-u͡ok-put</ta>
            <ta e="T900" id="Seg_7027" s="T899">ü͡ör-bütü-n</ta>
            <ta e="T901" id="Seg_7028" s="T900">kolb-u͡ok-put</ta>
            <ta e="T902" id="Seg_7029" s="T901">u͡ol-u</ta>
            <ta e="T903" id="Seg_7030" s="T902">kɨtta</ta>
            <ta e="T904" id="Seg_7031" s="T903">kɨːs</ta>
            <ta e="T905" id="Seg_7032" s="T904">hordoŋ-nor-u</ta>
            <ta e="T906" id="Seg_7033" s="T905">uː</ta>
            <ta e="T907" id="Seg_7034" s="T906">ih-i-tten</ta>
            <ta e="T908" id="Seg_7035" s="T907">oroː-tu-lar</ta>
            <ta e="T909" id="Seg_7036" s="T908">on-tu-lara</ta>
            <ta e="T910" id="Seg_7037" s="T909">bugdʼiː</ta>
            <ta e="T911" id="Seg_7038" s="T910">taba</ta>
            <ta e="T912" id="Seg_7039" s="T911">bu͡ol-lu-lar</ta>
            <ta e="T913" id="Seg_7040" s="T912">u͡ol</ta>
            <ta e="T914" id="Seg_7041" s="T913">edʼiːj-e</ta>
            <ta e="T915" id="Seg_7042" s="T914">ü͡ör-en</ta>
            <ta e="T916" id="Seg_7043" s="T915">körüs-t-e</ta>
            <ta e="T917" id="Seg_7044" s="T916">u͡ol</ta>
            <ta e="T918" id="Seg_7045" s="T917">üs</ta>
            <ta e="T919" id="Seg_7046" s="T918">kün-ü</ta>
            <ta e="T920" id="Seg_7047" s="T919">utuj-d-a</ta>
            <ta e="T921" id="Seg_7048" s="T920">uhukt-an</ta>
            <ta e="T922" id="Seg_7049" s="T921">kör-büt-e</ta>
            <ta e="T923" id="Seg_7050" s="T922">edʼiːj-e</ta>
            <ta e="T924" id="Seg_7051" s="T923">kɨːh-ɨ</ta>
            <ta e="T925" id="Seg_7052" s="T924">gɨtta</ta>
            <ta e="T926" id="Seg_7053" s="T925">kepset-e-kepset-e</ta>
            <ta e="T927" id="Seg_7054" s="T926">kül-s-e</ta>
            <ta e="T928" id="Seg_7055" s="T927">olor-ol-lor</ta>
            <ta e="T929" id="Seg_7056" s="T928">čaːj</ta>
            <ta e="T930" id="Seg_7057" s="T929">ih-el-ler</ta>
            <ta e="T931" id="Seg_7058" s="T930">bus-put</ta>
            <ta e="T932" id="Seg_7059" s="T931">et-i</ta>
            <ta e="T933" id="Seg_7060" s="T932">hiː-l-ler</ta>
            <ta e="T934" id="Seg_7061" s="T933">u͡ol</ta>
            <ta e="T935" id="Seg_7062" s="T934">ulka-larɨ-gar</ta>
            <ta e="T936" id="Seg_7063" s="T935">olor-d-o</ta>
            <ta e="T937" id="Seg_7064" s="T936">bütün</ta>
            <ta e="T938" id="Seg_7065" s="T937">taba-nɨ</ta>
            <ta e="T939" id="Seg_7066" s="T938">hi͡e-t-e</ta>
            <ta e="T940" id="Seg_7067" s="T939">u͡ol</ta>
            <ta e="T941" id="Seg_7068" s="T940">kɨːh-ɨ</ta>
            <ta e="T942" id="Seg_7069" s="T941">dʼaktar</ta>
            <ta e="T943" id="Seg_7070" s="T942">gɨm-mɨt</ta>
            <ta e="T944" id="Seg_7071" s="T943">dʼolloːk</ta>
            <ta e="T945" id="Seg_7072" s="T944">bagaj-tɨk</ta>
            <ta e="T946" id="Seg_7073" s="T945">olor-but-tar</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_7074" s="T1">ihilleː-ŋ</ta>
            <ta e="T3" id="Seg_7075" s="T2">dulgaːn-LAr</ta>
            <ta e="T4" id="Seg_7076" s="T3">oloŋko-LArI-n</ta>
            <ta e="T5" id="Seg_7077" s="T4">edʼij-LAːK</ta>
            <ta e="T6" id="Seg_7078" s="T5">u͡ol</ta>
            <ta e="T7" id="Seg_7079" s="T6">di͡e-An</ta>
            <ta e="T8" id="Seg_7080" s="T7">aːt-LAːgI</ta>
            <ta e="T9" id="Seg_7081" s="T8">u͡ol</ta>
            <ta e="T10" id="Seg_7082" s="T9">edʼij-tI-n</ta>
            <ta e="T11" id="Seg_7083" s="T10">kɨtta</ta>
            <ta e="T12" id="Seg_7084" s="T11">ikki͡ejegin</ta>
            <ta e="T13" id="Seg_7085" s="T12">olor-BIT-LArA</ta>
            <ta e="T14" id="Seg_7086" s="T13">edʼij-tA</ta>
            <ta e="T15" id="Seg_7087" s="T14">ülehit-LAːK</ta>
            <ta e="T16" id="Seg_7088" s="T15">uːs</ta>
            <ta e="T17" id="Seg_7089" s="T16">bagajɨ</ta>
            <ta e="T18" id="Seg_7090" s="T17">hetiː-LAr-tA</ta>
            <ta e="T19" id="Seg_7091" s="T18">bugdi</ta>
            <ta e="T20" id="Seg_7092" s="T19">taba</ta>
            <ta e="T21" id="Seg_7093" s="T20">tiriː-LAr-I-nAn</ta>
            <ta e="T22" id="Seg_7094" s="T21">habɨː-LAːK-LAr</ta>
            <ta e="T23" id="Seg_7095" s="T22">uraha</ta>
            <ta e="T24" id="Seg_7096" s="T23">dʼi͡e-nI</ta>
            <ta e="T25" id="Seg_7097" s="T24">beje-tA</ta>
            <ta e="T26" id="Seg_7098" s="T25">tut-n-AːččI</ta>
            <ta e="T27" id="Seg_7099" s="T26">taba-LArI-n</ta>
            <ta e="T28" id="Seg_7100" s="T27">da</ta>
            <ta e="T29" id="Seg_7101" s="T28">hajɨn</ta>
            <ta e="T30" id="Seg_7102" s="T29">keteː-AːččI</ta>
            <ta e="T31" id="Seg_7103" s="T30">u͡ol</ta>
            <ta e="T32" id="Seg_7104" s="T31">balɨs-I-tA</ta>
            <ta e="T33" id="Seg_7105" s="T32">kühün-GI-ttAn</ta>
            <ta e="T34" id="Seg_7106" s="T33">utuj-BIT</ta>
            <ta e="T35" id="Seg_7107" s="T34">kün</ta>
            <ta e="T36" id="Seg_7108" s="T35">tagɨs-IAK.[tI]-r</ta>
            <ta e="T37" id="Seg_7109" s="T36">di͡eri</ta>
            <ta e="T38" id="Seg_7110" s="T37">ü͡ör-LArA</ta>
            <ta e="T39" id="Seg_7111" s="T38">ɨraːt-BIT</ta>
            <ta e="T40" id="Seg_7112" s="T39">oŋko-LArA</ta>
            <ta e="T41" id="Seg_7113" s="T40">keŋeː-BIT</ta>
            <ta e="T42" id="Seg_7114" s="T41">kɨːs</ta>
            <ta e="T43" id="Seg_7115" s="T42">oŋko</ta>
            <ta e="T44" id="Seg_7116" s="T43">ularɨt-IːhI</ta>
            <ta e="T45" id="Seg_7117" s="T44">ü͡ör-tI-n</ta>
            <ta e="T46" id="Seg_7118" s="T45">egel-TI-tA</ta>
            <ta e="T47" id="Seg_7119" s="T46">taba</ta>
            <ta e="T48" id="Seg_7120" s="T47">tutun-TI-tA</ta>
            <ta e="T49" id="Seg_7121" s="T48">kölün-TI-tA</ta>
            <ta e="T50" id="Seg_7122" s="T49">balɨs-I-tI-n</ta>
            <ta e="T51" id="Seg_7123" s="T50">uhugun-TAr-A</ta>
            <ta e="T52" id="Seg_7124" s="T51">hataː-Ar</ta>
            <ta e="T53" id="Seg_7125" s="T52">amattan</ta>
            <ta e="T54" id="Seg_7126" s="T53">uhugun-I-BAT</ta>
            <ta e="T55" id="Seg_7127" s="T54">uraha</ta>
            <ta e="T56" id="Seg_7128" s="T55">dʼi͡e-tI-n</ta>
            <ta e="T57" id="Seg_7129" s="T56">kastaː-An</ta>
            <ta e="T58" id="Seg_7130" s="T57">baran</ta>
            <ta e="T59" id="Seg_7131" s="T58">balɨs-I-tI-n</ta>
            <ta e="T60" id="Seg_7132" s="T59">kɨrsa</ta>
            <ta e="T61" id="Seg_7133" s="T60">hu͡organ-tI-n</ta>
            <ta e="T62" id="Seg_7134" s="T61">tuːra</ta>
            <ta e="T63" id="Seg_7135" s="T62">tart-TI-tA</ta>
            <ta e="T64" id="Seg_7136" s="T63">örüküj-An</ta>
            <ta e="T65" id="Seg_7137" s="T64">uhugun-TI-tA</ta>
            <ta e="T66" id="Seg_7138" s="T65">taŋɨn-TI-tA</ta>
            <ta e="T67" id="Seg_7139" s="T66">hɨrga-tI-GAr</ta>
            <ta e="T68" id="Seg_7140" s="T67">olor-AːT</ta>
            <ta e="T69" id="Seg_7141" s="T68">emi͡e</ta>
            <ta e="T70" id="Seg_7142" s="T69">utuj-TI-tA</ta>
            <ta e="T71" id="Seg_7143" s="T70">mas-LAN-Ar-tI-GAr</ta>
            <ta e="T72" id="Seg_7144" s="T71">buːs-LAN-Ar-tI-GAr</ta>
            <ta e="T73" id="Seg_7145" s="T72">taba-LAN-Ar-tI-GAr</ta>
            <ta e="T74" id="Seg_7146" s="T73">bu͡ol-An</ta>
            <ta e="T75" id="Seg_7147" s="T74">kɨːs</ta>
            <ta e="T76" id="Seg_7148" s="T75">bert</ta>
            <ta e="T77" id="Seg_7149" s="T76">hɨlaj-Ar</ta>
            <ta e="T78" id="Seg_7150" s="T77">hɨlaj-An</ta>
            <ta e="T79" id="Seg_7151" s="T78">utuj-BIT</ta>
            <ta e="T80" id="Seg_7152" s="T79">utuj-A</ta>
            <ta e="T81" id="Seg_7153" s="T80">hɨt-An</ta>
            <ta e="T82" id="Seg_7154" s="T81">toŋ-An</ta>
            <ta e="T83" id="Seg_7155" s="T82">kaːl-BIT</ta>
            <ta e="T84" id="Seg_7156" s="T83">u͡ol</ta>
            <ta e="T85" id="Seg_7157" s="T84">balɨs-I-tA</ta>
            <ta e="T86" id="Seg_7158" s="T85">össü͡ö</ta>
            <ta e="T87" id="Seg_7159" s="T86">üs</ta>
            <ta e="T88" id="Seg_7160" s="T87">kün-nI</ta>
            <ta e="T89" id="Seg_7161" s="T88">utuj-BIT</ta>
            <ta e="T90" id="Seg_7162" s="T89">uhugun-I-BIT-tA</ta>
            <ta e="T91" id="Seg_7163" s="T90">kör-BIT-tA</ta>
            <ta e="T92" id="Seg_7164" s="T91">edʼij-tA</ta>
            <ta e="T93" id="Seg_7165" s="T92">toŋ-An</ta>
            <ta e="T94" id="Seg_7166" s="T93">kaːl-BIT</ta>
            <ta e="T95" id="Seg_7167" s="T94">ü͡ör-tA</ta>
            <ta e="T96" id="Seg_7168" s="T95">barɨ-tA</ta>
            <ta e="T97" id="Seg_7169" s="T96">toŋ-An</ta>
            <ta e="T98" id="Seg_7170" s="T97">öl-BIT</ta>
            <ta e="T99" id="Seg_7171" s="T98">hobu͡oj</ta>
            <ta e="T100" id="Seg_7172" s="T99">ogus-I-BIT</ta>
            <ta e="T101" id="Seg_7173" s="T100">u͡ot</ta>
            <ta e="T102" id="Seg_7174" s="T101">otun-TAK-BInA</ta>
            <ta e="T103" id="Seg_7175" s="T102">bɨbaːt</ta>
            <ta e="T104" id="Seg_7176" s="T103">edʼij-I-m</ta>
            <ta e="T105" id="Seg_7177" s="T104">tilin-IAK.[tA]</ta>
            <ta e="T106" id="Seg_7178" s="T105">da</ta>
            <ta e="T107" id="Seg_7179" s="T106">di͡e-A</ta>
            <ta e="T108" id="Seg_7180" s="T107">hanaː-BIT</ta>
            <ta e="T109" id="Seg_7181" s="T108">u͡ol</ta>
            <ta e="T110" id="Seg_7182" s="T109">mas-LAN-A</ta>
            <ta e="T111" id="Seg_7183" s="T110">bar-IAK-GA</ta>
            <ta e="T112" id="Seg_7184" s="T111">kaja-LAːK</ta>
            <ta e="T113" id="Seg_7185" s="T112">ürek</ta>
            <ta e="T114" id="Seg_7186" s="T113">ürüt-tI-nAn</ta>
            <ta e="T115" id="Seg_7187" s="T114">mas</ta>
            <ta e="T116" id="Seg_7188" s="T115">dek</ta>
            <ta e="T117" id="Seg_7189" s="T116">hüːr-BIT</ta>
            <ta e="T118" id="Seg_7190" s="T117">kör-BIT-tA</ta>
            <ta e="T119" id="Seg_7191" s="T118">kallaːn-ttAn</ta>
            <ta e="T120" id="Seg_7192" s="T119">tutuktaːk</ta>
            <ta e="T121" id="Seg_7193" s="T120">otuː</ta>
            <ta e="T122" id="Seg_7194" s="T121">gɨdaːrɨj-A</ta>
            <ta e="T123" id="Seg_7195" s="T122">hɨt-Ar</ta>
            <ta e="T124" id="Seg_7196" s="T123">abaːhɨ</ta>
            <ta e="T125" id="Seg_7197" s="T124">üčehe-GA</ta>
            <ta e="T126" id="Seg_7198" s="T125">et</ta>
            <ta e="T127" id="Seg_7199" s="T126">ü͡öl-A-n-A</ta>
            <ta e="T128" id="Seg_7200" s="T127">olor-Ar</ta>
            <ta e="T129" id="Seg_7201" s="T128">u͡ol-nI</ta>
            <ta e="T130" id="Seg_7202" s="T129">kör-BAT</ta>
            <ta e="T131" id="Seg_7203" s="T130">bu</ta>
            <ta e="T132" id="Seg_7204" s="T131">u͡ol</ta>
            <ta e="T133" id="Seg_7205" s="T132">hulburuj-t-A</ta>
            <ta e="T134" id="Seg_7206" s="T133">tɨːt-A-tɨːt-A</ta>
            <ta e="T135" id="Seg_7207" s="T134">onu͡or-GI-ttAn</ta>
            <ta e="T136" id="Seg_7208" s="T135">hi͡e-A</ta>
            <ta e="T137" id="Seg_7209" s="T136">olor-BIT</ta>
            <ta e="T138" id="Seg_7210" s="T137">kajdak</ta>
            <ta e="T139" id="Seg_7211" s="T138">tu͡olkulaː-BAT-tI-n</ta>
            <ta e="T140" id="Seg_7212" s="T139">ka</ta>
            <ta e="T141" id="Seg_7213" s="T140">di͡e-Ar</ta>
            <ta e="T142" id="Seg_7214" s="T141">abaːhɨ</ta>
            <ta e="T143" id="Seg_7215" s="T142">et-LAr-I-m</ta>
            <ta e="T144" id="Seg_7216" s="T143">u͡ot-GA</ta>
            <ta e="T145" id="Seg_7217" s="T144">tüs-Ar-LArI-n</ta>
            <ta e="T146" id="Seg_7218" s="T145">abaːhɨ</ta>
            <ta e="T147" id="Seg_7219" s="T146">u͡ol-nI</ta>
            <ta e="T148" id="Seg_7220" s="T147">kör-An</ta>
            <ta e="T149" id="Seg_7221" s="T148">ölör-AːrI</ta>
            <ta e="T150" id="Seg_7222" s="T149">gɨn-BIT</ta>
            <ta e="T151" id="Seg_7223" s="T150">u͡ol</ta>
            <ta e="T152" id="Seg_7224" s="T151">uruːka-tI-nAn</ta>
            <ta e="T153" id="Seg_7225" s="T152">abaːhɨ</ta>
            <ta e="T154" id="Seg_7226" s="T153">agɨs</ta>
            <ta e="T155" id="Seg_7227" s="T154">bas-tI-n</ta>
            <ta e="T156" id="Seg_7228" s="T155">tuːra</ta>
            <ta e="T157" id="Seg_7229" s="T156">ogus-I-BIT</ta>
            <ta e="T158" id="Seg_7230" s="T157">bas-LAr-tA</ta>
            <ta e="T159" id="Seg_7231" s="T158">üŋküːleː-A</ta>
            <ta e="T160" id="Seg_7232" s="T159">hɨrɨt-Ar-LAr</ta>
            <ta e="T161" id="Seg_7233" s="T160">u͡ol</ta>
            <ta e="T162" id="Seg_7234" s="T161">onno</ta>
            <ta e="T163" id="Seg_7235" s="T162">časkɨj-BIT</ta>
            <ta e="T164" id="Seg_7236" s="T163">bukatɨːr</ta>
            <ta e="T165" id="Seg_7237" s="T164">ikki-LAː-AːččI-tA</ta>
            <ta e="T166" id="Seg_7238" s="T165">üs-Is-LAː-AːččI-tA</ta>
            <ta e="T167" id="Seg_7239" s="T166">hu͡ok</ta>
            <ta e="T168" id="Seg_7240" s="T167">abaːhɨ</ta>
            <ta e="T169" id="Seg_7241" s="T168">tü͡ös-tI-n</ta>
            <ta e="T170" id="Seg_7242" s="T169">tɨːr-A</ta>
            <ta e="T171" id="Seg_7243" s="T170">tart-An</ta>
            <ta e="T172" id="Seg_7244" s="T171">taːs-nI</ta>
            <ta e="T173" id="Seg_7245" s="T172">oroː-TI-tA</ta>
            <ta e="T174" id="Seg_7246" s="T173">taːs</ta>
            <ta e="T175" id="Seg_7247" s="T174">hürek-LAːK</ta>
            <ta e="T176" id="Seg_7248" s="T175">e-BIT</ta>
            <ta e="T177" id="Seg_7249" s="T176">u͡ol</ta>
            <ta e="T178" id="Seg_7250" s="T177">taːs-nI</ta>
            <ta e="T179" id="Seg_7251" s="T178">tep-A-tep-A</ta>
            <ta e="T180" id="Seg_7252" s="T179">bar-A</ta>
            <ta e="T181" id="Seg_7253" s="T180">tur-BIT</ta>
            <ta e="T182" id="Seg_7254" s="T181">ilin-tI-GAr</ta>
            <ta e="T183" id="Seg_7255" s="T182">hir</ta>
            <ta e="T184" id="Seg_7256" s="T183">aŋar-LAːK</ta>
            <ta e="T185" id="Seg_7257" s="T184">haga</ta>
            <ta e="T186" id="Seg_7258" s="T185">taːs</ta>
            <ta e="T187" id="Seg_7259" s="T186">tur-BIT</ta>
            <ta e="T188" id="Seg_7260" s="T187">taːs</ta>
            <ta e="T189" id="Seg_7261" s="T188">kaja</ta>
            <ta e="T190" id="Seg_7262" s="T189">taːs</ta>
            <ta e="T191" id="Seg_7263" s="T190">kaja-nI</ta>
            <ta e="T192" id="Seg_7264" s="T191">taːs</ta>
            <ta e="T193" id="Seg_7265" s="T192">hürek-tI-nAn</ta>
            <ta e="T194" id="Seg_7266" s="T193">tep-BIT</ta>
            <ta e="T195" id="Seg_7267" s="T194">kaja-tA</ta>
            <ta e="T196" id="Seg_7268" s="T195">ikki</ta>
            <ta e="T197" id="Seg_7269" s="T196">aŋɨ</ta>
            <ta e="T198" id="Seg_7270" s="T197">kajɨt-An</ta>
            <ta e="T199" id="Seg_7271" s="T198">tüs-BIT</ta>
            <ta e="T200" id="Seg_7272" s="T199">oduːlaː-An</ta>
            <ta e="T201" id="Seg_7273" s="T200">kör-BIT-tA</ta>
            <ta e="T202" id="Seg_7274" s="T201">hir</ta>
            <ta e="T203" id="Seg_7275" s="T202">köhün-Ar</ta>
            <ta e="T204" id="Seg_7276" s="T203">onno</ta>
            <ta e="T205" id="Seg_7277" s="T204">bar-An</ta>
            <ta e="T206" id="Seg_7278" s="T205">hɨrga-LAːK</ta>
            <ta e="T207" id="Seg_7279" s="T206">hu͡ol-tI-n</ta>
            <ta e="T208" id="Seg_7280" s="T207">kör-BIT</ta>
            <ta e="T209" id="Seg_7281" s="T208">paːs</ta>
            <ta e="T210" id="Seg_7282" s="T209">kapkaːn</ta>
            <ta e="T211" id="Seg_7283" s="T210">kör-I-n-A</ta>
            <ta e="T212" id="Seg_7284" s="T211">hɨrɨt-Ar</ta>
            <ta e="T213" id="Seg_7285" s="T212">kihi</ta>
            <ta e="T214" id="Seg_7286" s="T213">e-TI-tA</ta>
            <ta e="T215" id="Seg_7287" s="T214">e-BIT</ta>
            <ta e="T216" id="Seg_7288" s="T215">töhö</ta>
            <ta e="T217" id="Seg_7289" s="T216">da</ta>
            <ta e="T218" id="Seg_7290" s="T217">bu͡ol-BAkkA</ta>
            <ta e="T219" id="Seg_7291" s="T218">kɨrsa-ČIt</ta>
            <ta e="T220" id="Seg_7292" s="T219">utarɨ</ta>
            <ta e="T221" id="Seg_7293" s="T220">is-Ar-tI-n</ta>
            <ta e="T222" id="Seg_7294" s="T221">kör-BIT</ta>
            <ta e="T223" id="Seg_7295" s="T222">kel-An</ta>
            <ta e="T224" id="Seg_7296" s="T223">tur-TI-tA</ta>
            <ta e="T225" id="Seg_7297" s="T224">doroːbolos-TI-tA</ta>
            <ta e="T226" id="Seg_7298" s="T225">tu͡ok-nI</ta>
            <ta e="T227" id="Seg_7299" s="T226">gɨn-A</ta>
            <ta e="T228" id="Seg_7300" s="T227">hɨrɨt-A-GIn</ta>
            <ta e="T229" id="Seg_7301" s="T228">ɨjɨt-BIT</ta>
            <ta e="T230" id="Seg_7302" s="T229">u͡ol</ta>
            <ta e="T231" id="Seg_7303" s="T230">paːs</ta>
            <ta e="T232" id="Seg_7304" s="T231">kapkaːn</ta>
            <ta e="T233" id="Seg_7305" s="T232">kör-I-n-A-BIn</ta>
            <ta e="T234" id="Seg_7306" s="T233">ɨraːk</ta>
            <ta e="T235" id="Seg_7307" s="T234">dʼi͡e-LAːK-GIt</ta>
            <ta e="T236" id="Seg_7308" s="T235">du͡o</ta>
            <ta e="T237" id="Seg_7309" s="T236">kas</ta>
            <ta e="T238" id="Seg_7310" s="T237">kihi-GIt=Ij</ta>
            <ta e="T239" id="Seg_7311" s="T238">inʼe-BI-n</ta>
            <ta e="T240" id="Seg_7312" s="T239">aga-BI-n</ta>
            <ta e="T241" id="Seg_7313" s="T240">kɨtta</ta>
            <ta e="T242" id="Seg_7314" s="T241">olor-A-BIn</ta>
            <ta e="T243" id="Seg_7315" s="T242">di͡e-BIT</ta>
            <ta e="T244" id="Seg_7316" s="T243">u͡ol</ta>
            <ta e="T245" id="Seg_7317" s="T244">meŋehin</ta>
            <ta e="T246" id="Seg_7318" s="T245">ilin-IAK-m</ta>
            <ta e="T247" id="Seg_7319" s="T246">ɨ͡aldʼɨt</ta>
            <ta e="T248" id="Seg_7320" s="T247">bu͡ol</ta>
            <ta e="T249" id="Seg_7321" s="T248">uraha</ta>
            <ta e="T250" id="Seg_7322" s="T249">dʼi͡e-tI-GAr</ta>
            <ta e="T251" id="Seg_7323" s="T250">egel-BIT</ta>
            <ta e="T252" id="Seg_7324" s="T251">ogonnʼor-LAːK</ta>
            <ta e="T253" id="Seg_7325" s="T252">emeːksin</ta>
            <ta e="T254" id="Seg_7326" s="T253">utarɨ</ta>
            <ta e="T255" id="Seg_7327" s="T254">tagɨs-An-LAr</ta>
            <ta e="T256" id="Seg_7328" s="T255">körüs-I-BIT-LAr</ta>
            <ta e="T257" id="Seg_7329" s="T256">kaja</ta>
            <ta e="T258" id="Seg_7330" s="T257">hir-ttAn</ta>
            <ta e="T259" id="Seg_7331" s="T258">kel-TI-ŋ</ta>
            <ta e="T260" id="Seg_7332" s="T259">ɨjɨt-BIT-LAr</ta>
            <ta e="T261" id="Seg_7333" s="T260">manna</ta>
            <ta e="T262" id="Seg_7334" s="T261">tu͡ora</ta>
            <ta e="T263" id="Seg_7335" s="T262">karak-LAːK</ta>
            <ta e="T264" id="Seg_7336" s="T263">u͡ot</ta>
            <ta e="T265" id="Seg_7337" s="T264">ulluŋ-LAːK</ta>
            <ta e="T266" id="Seg_7338" s="T265">ükteː-A-n-A=ilik</ta>
            <ta e="T267" id="Seg_7339" s="T266">u͡ol</ta>
            <ta e="T268" id="Seg_7340" s="T267">kepseː-BIT</ta>
            <ta e="T269" id="Seg_7341" s="T268">edʼij-I-m</ta>
            <ta e="T270" id="Seg_7342" s="T269">toŋ-An</ta>
            <ta e="T271" id="Seg_7343" s="T270">öl-TI-tA</ta>
            <ta e="T272" id="Seg_7344" s="T271">taba-LAr-BIt</ta>
            <ta e="T273" id="Seg_7345" s="T272">emi͡e</ta>
            <ta e="T274" id="Seg_7346" s="T273">toŋ-An</ta>
            <ta e="T275" id="Seg_7347" s="T274">öl-TI-LAr</ta>
            <ta e="T276" id="Seg_7348" s="T275">kaːr</ta>
            <ta e="T277" id="Seg_7349" s="T276">is-tI-GAr</ta>
            <ta e="T278" id="Seg_7350" s="T277">hɨt-Ar-LAr</ta>
            <ta e="T279" id="Seg_7351" s="T278">tip-I-LIN-A</ta>
            <ta e="T280" id="Seg_7352" s="T279">kajdak</ta>
            <ta e="T281" id="Seg_7353" s="T280">kihi</ta>
            <ta e="T282" id="Seg_7354" s="T281">bu͡ol-IAK-m</ta>
            <ta e="T283" id="Seg_7355" s="T282">dʼürü</ta>
            <ta e="T284" id="Seg_7356" s="T283">maŋnaj</ta>
            <ta e="T285" id="Seg_7357" s="T284">ɨ͡aldʼɨt</ta>
            <ta e="T286" id="Seg_7358" s="T285">bu͡ol</ta>
            <ta e="T287" id="Seg_7359" s="T286">di͡e-BIT</ta>
            <ta e="T288" id="Seg_7360" s="T287">ogonnʼor</ta>
            <ta e="T289" id="Seg_7361" s="T288">onton</ta>
            <ta e="T290" id="Seg_7362" s="T289">hübeleː-A-s-IAK-BIt</ta>
            <ta e="T291" id="Seg_7363" s="T290">u͡ol-tA</ta>
            <ta e="T292" id="Seg_7364" s="T291">tagɨs-An</ta>
            <ta e="T293" id="Seg_7365" s="T292">tɨhɨ-nI</ta>
            <ta e="T294" id="Seg_7366" s="T293">ölör-BIT</ta>
            <ta e="T295" id="Seg_7367" s="T294">tas-LAːK</ta>
            <ta e="T296" id="Seg_7368" s="T295">tɨhɨ-nI</ta>
            <ta e="T297" id="Seg_7369" s="T296">ɨ͡aldʼɨt</ta>
            <ta e="T298" id="Seg_7370" s="T297">ol-nI</ta>
            <ta e="T299" id="Seg_7371" s="T298">hogotogun</ta>
            <ta e="T300" id="Seg_7372" s="T299">hi͡e-BIT</ta>
            <ta e="T301" id="Seg_7373" s="T300">tot-TI-ŋ</ta>
            <ta e="T302" id="Seg_7374" s="T301">ɨjɨt-BIT-LAr</ta>
            <ta e="T303" id="Seg_7375" s="T302">tot-TI-m</ta>
            <ta e="T304" id="Seg_7376" s="T303">kühün-GI-ttAn</ta>
            <ta e="T305" id="Seg_7377" s="T304">ahaː-A</ta>
            <ta e="T306" id="Seg_7378" s="T305">ilik-BIn</ta>
            <ta e="T307" id="Seg_7379" s="T306">anɨ</ta>
            <ta e="T308" id="Seg_7380" s="T307">hɨnnʼan-IAK-m</ta>
            <ta e="T309" id="Seg_7381" s="T308">üs</ta>
            <ta e="T310" id="Seg_7382" s="T309">kün-nI</ta>
            <ta e="T311" id="Seg_7383" s="T310">u͡ol</ta>
            <ta e="T312" id="Seg_7384" s="T311">utuj-BIT</ta>
            <ta e="T313" id="Seg_7385" s="T312">uhugun-I-BIT-tI-GAr</ta>
            <ta e="T314" id="Seg_7386" s="T313">ogonnʼor</ta>
            <ta e="T315" id="Seg_7387" s="T314">muŋ</ta>
            <ta e="T316" id="Seg_7388" s="T315">emis</ta>
            <ta e="T317" id="Seg_7389" s="T316">taba-nI</ta>
            <ta e="T318" id="Seg_7390" s="T317">ölör-An</ta>
            <ta e="T319" id="Seg_7391" s="T318">hi͡e-k-BIT</ta>
            <ta e="T320" id="Seg_7392" s="T319">ogonnʼor</ta>
            <ta e="T321" id="Seg_7393" s="T320">üs</ta>
            <ta e="T322" id="Seg_7394" s="T321">ɨj-LAːgI</ta>
            <ta e="T323" id="Seg_7395" s="T322">as-nI</ta>
            <ta e="T324" id="Seg_7396" s="T323">belem-LAː-BIT</ta>
            <ta e="T325" id="Seg_7397" s="T324">bar-IAK-tI-n</ta>
            <ta e="T326" id="Seg_7398" s="T325">u͡ol</ta>
            <ta e="T327" id="Seg_7399" s="T326">di͡e-BIT</ta>
            <ta e="T328" id="Seg_7400" s="T327">kanna</ta>
            <ta e="T329" id="Seg_7401" s="T328">ere</ta>
            <ta e="T330" id="Seg_7402" s="T329">hir</ta>
            <ta e="T331" id="Seg_7403" s="T330">ičči-tA</ta>
            <ta e="T332" id="Seg_7404" s="T331">baːr</ta>
            <ta e="T333" id="Seg_7405" s="T332">ühü</ta>
            <ta e="T334" id="Seg_7406" s="T333">onno</ta>
            <ta e="T335" id="Seg_7407" s="T334">bar-IAK-m</ta>
            <ta e="T336" id="Seg_7408" s="T335">u͡ol</ta>
            <ta e="T337" id="Seg_7409" s="T336">hɨrga-tI-GAr</ta>
            <ta e="T338" id="Seg_7410" s="T337">taːs</ta>
            <ta e="T339" id="Seg_7411" s="T338">hürek-nI</ta>
            <ta e="T340" id="Seg_7412" s="T339">uːr-TI-tA</ta>
            <ta e="T341" id="Seg_7413" s="T340">ürek</ta>
            <ta e="T342" id="Seg_7414" s="T341">is-tI-nAn</ta>
            <ta e="T343" id="Seg_7415" s="T342">kamnaː-An</ta>
            <ta e="T344" id="Seg_7416" s="T343">kaːl-TI-tA</ta>
            <ta e="T345" id="Seg_7417" s="T344">töhö</ta>
            <ta e="T346" id="Seg_7418" s="T345">ör-LIk</ta>
            <ta e="T347" id="Seg_7419" s="T346">bar-BIT-tA</ta>
            <ta e="T348" id="Seg_7420" s="T347">dʼürü</ta>
            <ta e="T349" id="Seg_7421" s="T348">agaj</ta>
            <ta e="T350" id="Seg_7422" s="T349">čuguːn</ta>
            <ta e="T351" id="Seg_7423" s="T350">uraha</ta>
            <ta e="T352" id="Seg_7424" s="T351">dʼi͡e-nI</ta>
            <ta e="T353" id="Seg_7425" s="T352">kör-BIT</ta>
            <ta e="T354" id="Seg_7426" s="T353">aːn-nI</ta>
            <ta e="T355" id="Seg_7427" s="T354">arɨj-An</ta>
            <ta e="T356" id="Seg_7428" s="T355">kör-TI-tA</ta>
            <ta e="T357" id="Seg_7429" s="T356">kɨrdʼagas</ta>
            <ta e="T358" id="Seg_7430" s="T357">bagajɨ</ta>
            <ta e="T359" id="Seg_7431" s="T358">emeːksin</ta>
            <ta e="T360" id="Seg_7432" s="T359">olor-Ar</ta>
            <ta e="T361" id="Seg_7433" s="T360">attɨ-tI-GAr</ta>
            <ta e="T362" id="Seg_7434" s="T361">kü͡ört</ta>
            <ta e="T363" id="Seg_7435" s="T362">hɨt-Ar</ta>
            <ta e="T364" id="Seg_7436" s="T363">menʼiː-tI-n</ta>
            <ta e="T365" id="Seg_7437" s="T364">ürüt-tI-GAr</ta>
            <ta e="T366" id="Seg_7438" s="T365">mataŋa</ta>
            <ta e="T367" id="Seg_7439" s="T366">ɨjaː-LIN-I-BIT</ta>
            <ta e="T368" id="Seg_7440" s="T367">mataŋa</ta>
            <ta e="T369" id="Seg_7441" s="T368">kihi</ta>
            <ta e="T370" id="Seg_7442" s="T369">hɨraj-tI-n</ta>
            <ta e="T371" id="Seg_7443" s="T370">majgɨlaːk</ta>
            <ta e="T372" id="Seg_7444" s="T371">ačaːk</ta>
            <ta e="T373" id="Seg_7445" s="T372">alɨn-I-tA</ta>
            <ta e="T374" id="Seg_7446" s="T373">köŋdöj</ta>
            <ta e="T375" id="Seg_7447" s="T374">ürek</ta>
            <ta e="T376" id="Seg_7448" s="T375">uhun-A</ta>
            <ta e="T377" id="Seg_7449" s="T376">hɨt-Ar</ta>
            <ta e="T378" id="Seg_7450" s="T377">ürek-GA</ta>
            <ta e="T379" id="Seg_7451" s="T378">tɨː-LAːK</ta>
            <ta e="T380" id="Seg_7452" s="T379">ilim-LAN-A</ta>
            <ta e="T381" id="Seg_7453" s="T380">hɨrɨt-Ar</ta>
            <ta e="T382" id="Seg_7454" s="T381">mataŋa</ta>
            <ta e="T383" id="Seg_7455" s="T382">haŋa-LAːK</ta>
            <ta e="T384" id="Seg_7456" s="T383">bu͡ol-BIT</ta>
            <ta e="T385" id="Seg_7457" s="T384">ɨ͡aldʼɨt</ta>
            <ta e="T386" id="Seg_7458" s="T385">kel-TI-tA</ta>
            <ta e="T387" id="Seg_7459" s="T386">mas-ČIt-LAr</ta>
            <ta e="T388" id="Seg_7460" s="T387">mas-LAː-ŋ</ta>
            <ta e="T389" id="Seg_7461" s="T388">u͡ot-TA</ta>
            <ta e="T390" id="Seg_7462" s="T389">otut-I-ŋ</ta>
            <ta e="T391" id="Seg_7463" s="T390">mas-LArA</ta>
            <ta e="T392" id="Seg_7464" s="T391">beje-LArA</ta>
            <ta e="T393" id="Seg_7465" s="T392">kiːr-TI-LAr</ta>
            <ta e="T394" id="Seg_7466" s="T393">tahaːrattan</ta>
            <ta e="T395" id="Seg_7467" s="T394">u͡ol</ta>
            <ta e="T396" id="Seg_7468" s="T395">tu͡olkulaː-TI-tA</ta>
            <ta e="T397" id="Seg_7469" s="T396">timir</ta>
            <ta e="T398" id="Seg_7470" s="T397">mas-LAr</ta>
            <ta e="T399" id="Seg_7471" s="T398">e-BIT</ta>
            <ta e="T400" id="Seg_7472" s="T399">emeːksin</ta>
            <ta e="T401" id="Seg_7473" s="T400">tur-An</ta>
            <ta e="T402" id="Seg_7474" s="T401">mas-LAr-nI</ta>
            <ta e="T403" id="Seg_7475" s="T402">ačaːk-tI-GAr</ta>
            <ta e="T404" id="Seg_7476" s="T403">mus-TI-tA</ta>
            <ta e="T405" id="Seg_7477" s="T404">mataŋa</ta>
            <ta e="T406" id="Seg_7478" s="T405">di͡e-TI-tA</ta>
            <ta e="T407" id="Seg_7479" s="T406">kü͡ört</ta>
            <ta e="T408" id="Seg_7480" s="T407">kü͡ört-LAː</ta>
            <ta e="T409" id="Seg_7481" s="T408">uraha</ta>
            <ta e="T410" id="Seg_7482" s="T409">dʼi͡e</ta>
            <ta e="T411" id="Seg_7483" s="T410">timir</ta>
            <ta e="T412" id="Seg_7484" s="T411">mu͡osta-tA</ta>
            <ta e="T413" id="Seg_7485" s="T412">kɨtar-čI</ta>
            <ta e="T414" id="Seg_7486" s="T413">bar-TI-tA</ta>
            <ta e="T415" id="Seg_7487" s="T414">mataŋa</ta>
            <ta e="T416" id="Seg_7488" s="T415">bi͡ek</ta>
            <ta e="T417" id="Seg_7489" s="T416">di͡e-Ar</ta>
            <ta e="T418" id="Seg_7490" s="T417">kü͡ört</ta>
            <ta e="T419" id="Seg_7491" s="T418">kü͡ört-LAː</ta>
            <ta e="T420" id="Seg_7492" s="T419">kü͡ört-LAː</ta>
            <ta e="T421" id="Seg_7493" s="T420">ɨ͡aldʼɨt</ta>
            <ta e="T422" id="Seg_7494" s="T421">kel-TI-tA</ta>
            <ta e="T423" id="Seg_7495" s="T422">u͡ot-TA</ta>
            <ta e="T424" id="Seg_7496" s="T423">ep</ta>
            <ta e="T425" id="Seg_7497" s="T424">uraha</ta>
            <ta e="T426" id="Seg_7498" s="T425">dʼi͡e</ta>
            <ta e="T427" id="Seg_7499" s="T426">barɨ-tA</ta>
            <ta e="T428" id="Seg_7500" s="T427">kɨtar-TI-tA</ta>
            <ta e="T429" id="Seg_7501" s="T428">emeːksin</ta>
            <ta e="T430" id="Seg_7502" s="T429">ɨ͡aldʼɨt</ta>
            <ta e="T431" id="Seg_7503" s="T430">u͡ol-nI</ta>
            <ta e="T432" id="Seg_7504" s="T431">kördös-TI-tA</ta>
            <ta e="T433" id="Seg_7505" s="T432">itij-t-I-m</ta>
            <ta e="T434" id="Seg_7506" s="T433">olus</ta>
            <ta e="T435" id="Seg_7507" s="T434">hoj-I-t</ta>
            <ta e="T436" id="Seg_7508" s="T435">dʼe</ta>
            <ta e="T437" id="Seg_7509" s="T436">u͡ol</ta>
            <ta e="T438" id="Seg_7510" s="T437">taːs</ta>
            <ta e="T439" id="Seg_7511" s="T438">hürek-nI</ta>
            <ta e="T440" id="Seg_7512" s="T439">ɨl-An</ta>
            <ta e="T441" id="Seg_7513" s="T440">ačaːk</ta>
            <ta e="T442" id="Seg_7514" s="T441">orto-tI-GAr</ta>
            <ta e="T443" id="Seg_7515" s="T442">bɨrak-TI-tA</ta>
            <ta e="T444" id="Seg_7516" s="T443">uraha</ta>
            <ta e="T445" id="Seg_7517" s="T444">dʼi͡e</ta>
            <ta e="T446" id="Seg_7518" s="T445">hoj-TI-tA</ta>
            <ta e="T447" id="Seg_7519" s="T446">mu͡osta-tA</ta>
            <ta e="T448" id="Seg_7520" s="T447">kɨtar-BA-TI-tA</ta>
            <ta e="T449" id="Seg_7521" s="T448">emeːksin</ta>
            <ta e="T450" id="Seg_7522" s="T449">emi͡e</ta>
            <ta e="T451" id="Seg_7523" s="T450">kam</ta>
            <ta e="T452" id="Seg_7524" s="T451">kɨrɨ͡a</ta>
            <ta e="T453" id="Seg_7525" s="T452">bu͡ol-TI-tA</ta>
            <ta e="T454" id="Seg_7526" s="T453">olus</ta>
            <ta e="T455" id="Seg_7527" s="T454">toŋ-A-r-I-BA</ta>
            <ta e="T456" id="Seg_7528" s="T455">kördös-BIT</ta>
            <ta e="T0" id="Seg_7529" s="T456">mataŋa</ta>
            <ta e="T947" id="Seg_7530" s="T0">barɨ</ta>
            <ta e="T948" id="Seg_7531" s="T947">hanaː-GI-n</ta>
            <ta e="T457" id="Seg_7532" s="T948">tolor-IAK-m</ta>
            <ta e="T458" id="Seg_7533" s="T457">mataŋa</ta>
            <ta e="T459" id="Seg_7534" s="T458">e-BIT</ta>
            <ta e="T460" id="Seg_7535" s="T459">hir</ta>
            <ta e="T461" id="Seg_7536" s="T460">ičči-tA</ta>
            <ta e="T462" id="Seg_7537" s="T461">u͡ol</ta>
            <ta e="T463" id="Seg_7538" s="T462">taːs</ta>
            <ta e="T464" id="Seg_7539" s="T463">hürek-nI</ta>
            <ta e="T465" id="Seg_7540" s="T464">ačaːk</ta>
            <ta e="T466" id="Seg_7541" s="T465">is-tI-ttAn</ta>
            <ta e="T467" id="Seg_7542" s="T466">oroː-An</ta>
            <ta e="T468" id="Seg_7543" s="T467">ɨl-TI-tA</ta>
            <ta e="T469" id="Seg_7544" s="T468">u͡ot-tA</ta>
            <ta e="T470" id="Seg_7545" s="T469">emi͡e</ta>
            <ta e="T471" id="Seg_7546" s="T470">gɨdaːrɨj-TI-tA</ta>
            <ta e="T472" id="Seg_7547" s="T471">mataŋa</ta>
            <ta e="T473" id="Seg_7548" s="T472">di͡e-TI-tA</ta>
            <ta e="T474" id="Seg_7549" s="T473">u͡ol-GA</ta>
            <ta e="T475" id="Seg_7550" s="T474">tahaːra</ta>
            <ta e="T476" id="Seg_7551" s="T475">tagɨs</ta>
            <ta e="T477" id="Seg_7552" s="T476">onno</ta>
            <ta e="T478" id="Seg_7553" s="T477">hɨrga-LAːK</ta>
            <ta e="T479" id="Seg_7554" s="T478">taba</ta>
            <ta e="T480" id="Seg_7555" s="T479">tur-Ar</ta>
            <ta e="T481" id="Seg_7556" s="T480">u͡ol</ta>
            <ta e="T482" id="Seg_7557" s="T481">tahaːra</ta>
            <ta e="T483" id="Seg_7558" s="T482">tagɨs-An</ta>
            <ta e="T484" id="Seg_7559" s="T483">tu͡ok</ta>
            <ta e="T485" id="Seg_7560" s="T484">da</ta>
            <ta e="T486" id="Seg_7561" s="T485">hɨrga-LAːK</ta>
            <ta e="T487" id="Seg_7562" s="T486">taba-tI-n</ta>
            <ta e="T488" id="Seg_7563" s="T487">kör-BAT</ta>
            <ta e="T489" id="Seg_7564" s="T488">kanna</ta>
            <ta e="T490" id="Seg_7565" s="T489">ere</ta>
            <ta e="T491" id="Seg_7566" s="T490">ču͡oraːn</ta>
            <ta e="T492" id="Seg_7567" s="T491">gugaːrka</ta>
            <ta e="T493" id="Seg_7568" s="T492">ere</ta>
            <ta e="T494" id="Seg_7569" s="T493">tɨ͡as-tA</ta>
            <ta e="T495" id="Seg_7570" s="T494">ihilin-Ar</ta>
            <ta e="T496" id="Seg_7571" s="T495">ketek-tA-n</ta>
            <ta e="T497" id="Seg_7572" s="T496">dek</ta>
            <ta e="T498" id="Seg_7573" s="T497">mataŋa</ta>
            <ta e="T499" id="Seg_7574" s="T498">haŋa-tA</ta>
            <ta e="T500" id="Seg_7575" s="T499">ihilin-I-BIT</ta>
            <ta e="T501" id="Seg_7576" s="T500">olor</ta>
            <ta e="T502" id="Seg_7577" s="T501">hɨrga-GA-r</ta>
            <ta e="T503" id="Seg_7578" s="T502">kelin-I-ŋ</ta>
            <ta e="T504" id="Seg_7579" s="T503">dek</ta>
            <ta e="T505" id="Seg_7580" s="T504">kanʼɨs-I-BA</ta>
            <ta e="T506" id="Seg_7581" s="T505">hu͡ol</ta>
            <ta e="T507" id="Seg_7582" s="T506">aːra</ta>
            <ta e="T508" id="Seg_7583" s="T507">toktoː-BA</ta>
            <ta e="T509" id="Seg_7584" s="T508">taba-LAr-GI-n</ta>
            <ta e="T510" id="Seg_7585" s="T509">ahaː-A-t-I-BA</ta>
            <ta e="T511" id="Seg_7586" s="T510">u͡ol</ta>
            <ta e="T512" id="Seg_7587" s="T511">iliː-tI-n</ta>
            <ta e="T513" id="Seg_7588" s="T512">uːn-TI-tA</ta>
            <ta e="T514" id="Seg_7589" s="T513">nʼu͡oguː-nI</ta>
            <ta e="T515" id="Seg_7590" s="T514">kap-TI-tA</ta>
            <ta e="T516" id="Seg_7591" s="T515">köhün-I-BAT</ta>
            <ta e="T517" id="Seg_7592" s="T516">hɨrga-GA</ta>
            <ta e="T518" id="Seg_7593" s="T517">miːn-TI-tA</ta>
            <ta e="T519" id="Seg_7594" s="T518">tɨ͡al-LAːK</ta>
            <ta e="T520" id="Seg_7595" s="T519">purgaː</ta>
            <ta e="T521" id="Seg_7596" s="T520">ogus-TI-tA</ta>
            <ta e="T522" id="Seg_7597" s="T521">kačča-nI</ta>
            <ta e="T523" id="Seg_7598" s="T522">bar-BIT-tA</ta>
            <ta e="T524" id="Seg_7599" s="T523">dʼürü</ta>
            <ta e="T525" id="Seg_7600" s="T524">kim</ta>
            <ta e="T526" id="Seg_7601" s="T525">bil-IAK.[tA]=Ij</ta>
            <ta e="T527" id="Seg_7602" s="T526">hɨrga-LAːK</ta>
            <ta e="T528" id="Seg_7603" s="T527">taba-LArA</ta>
            <ta e="T529" id="Seg_7604" s="T528">beje-LArA</ta>
            <ta e="T530" id="Seg_7605" s="T529">ilin-Ar-LAr</ta>
            <ta e="T531" id="Seg_7606" s="T530">u͡ol</ta>
            <ta e="T532" id="Seg_7607" s="T531">toŋ-BAT</ta>
            <ta e="T533" id="Seg_7608" s="T532">iliː-tI-n</ta>
            <ta e="T534" id="Seg_7609" s="T533">olbok-tI-n</ta>
            <ta e="T535" id="Seg_7610" s="T534">is-tI-GAr</ta>
            <ta e="T536" id="Seg_7611" s="T535">uk-TI-tA</ta>
            <ta e="T537" id="Seg_7612" s="T536">onno</ta>
            <ta e="T538" id="Seg_7613" s="T537">itiː</ta>
            <ta e="T539" id="Seg_7614" s="T538">timir</ta>
            <ta e="T540" id="Seg_7615" s="T539">mas</ta>
            <ta e="T541" id="Seg_7616" s="T540">hɨt-Ar</ta>
            <ta e="T542" id="Seg_7617" s="T541">e-BIT</ta>
            <ta e="T543" id="Seg_7618" s="T542">ol-tI-tA</ta>
            <ta e="T544" id="Seg_7619" s="T543">itij-I-t-Ar</ta>
            <ta e="T545" id="Seg_7620" s="T544">ebe</ta>
            <ta e="T546" id="Seg_7621" s="T545">ürüt-tI-GAr</ta>
            <ta e="T547" id="Seg_7622" s="T546">kör-TI-tA</ta>
            <ta e="T548" id="Seg_7623" s="T547">hɨrga-GA</ta>
            <ta e="T549" id="Seg_7624" s="T548">öl-BIT</ta>
            <ta e="T550" id="Seg_7625" s="T549">dʼaktar</ta>
            <ta e="T551" id="Seg_7626" s="T550">hɨt-Ar</ta>
            <ta e="T552" id="Seg_7627" s="T551">kam</ta>
            <ta e="T553" id="Seg_7628" s="T552">tip-I-LIN-An</ta>
            <ta e="T554" id="Seg_7629" s="T553">baran</ta>
            <ta e="T555" id="Seg_7630" s="T554">tehij-BAkkA</ta>
            <ta e="T556" id="Seg_7631" s="T555">toktoː-TI-tA</ta>
            <ta e="T557" id="Seg_7632" s="T556">toktoː-Ar-tI-n</ta>
            <ta e="T558" id="Seg_7633" s="T557">kɨtta</ta>
            <ta e="T559" id="Seg_7634" s="T558">nʼu͡oguhut-tA</ta>
            <ta e="T560" id="Seg_7635" s="T559">hulbu</ta>
            <ta e="T561" id="Seg_7636" s="T560">ɨstan-An</ta>
            <ta e="T562" id="Seg_7637" s="T561">kajdi͡ek</ta>
            <ta e="T563" id="Seg_7638" s="T562">bar-BIT-tA</ta>
            <ta e="T564" id="Seg_7639" s="T563">kajdi͡ek</ta>
            <ta e="T565" id="Seg_7640" s="T564">kel-BIT-tA</ta>
            <ta e="T566" id="Seg_7641" s="T565">üs</ta>
            <ta e="T567" id="Seg_7642" s="T566">koŋnomu͡oj</ta>
            <ta e="T568" id="Seg_7643" s="T567">taba-LAːK</ta>
            <ta e="T569" id="Seg_7644" s="T568">e-TI-tA</ta>
            <ta e="T570" id="Seg_7645" s="T569">e-BIT</ta>
            <ta e="T571" id="Seg_7646" s="T570">nʼu͡oguhut-tA</ta>
            <ta e="T572" id="Seg_7647" s="T571">bɨhɨn-An</ta>
            <ta e="T573" id="Seg_7648" s="T572">bar-An</ta>
            <ta e="T574" id="Seg_7649" s="T573">kaːl-BIT</ta>
            <ta e="T575" id="Seg_7650" s="T574">mataŋa</ta>
            <ta e="T576" id="Seg_7651" s="T575">haŋa-tA</ta>
            <ta e="T577" id="Seg_7652" s="T576">ihilin-I-BIT</ta>
            <ta e="T578" id="Seg_7653" s="T577">en-GA</ta>
            <ta e="T579" id="Seg_7654" s="T578">di͡e-BIT-I-m</ta>
            <ta e="T580" id="Seg_7655" s="T579">toktoː-BA</ta>
            <ta e="T581" id="Seg_7656" s="T580">u͡ol</ta>
            <ta e="T582" id="Seg_7657" s="T581">hɨrga-tI-ttAn</ta>
            <ta e="T583" id="Seg_7658" s="T582">kɨhɨl</ta>
            <ta e="T584" id="Seg_7659" s="T583">timir</ta>
            <ta e="T585" id="Seg_7660" s="T584">mas-nI</ta>
            <ta e="T586" id="Seg_7661" s="T585">ɨl-An</ta>
            <ta e="T587" id="Seg_7662" s="T586">kɨːs</ta>
            <ta e="T588" id="Seg_7663" s="T587">hɨrga-tI-n</ta>
            <ta e="T589" id="Seg_7664" s="T588">ulka-tI-GAr</ta>
            <ta e="T590" id="Seg_7665" s="T589">batarɨ</ta>
            <ta e="T591" id="Seg_7666" s="T590">as-TI-tA</ta>
            <ta e="T592" id="Seg_7667" s="T591">kaːr</ta>
            <ta e="T593" id="Seg_7668" s="T592">honno</ta>
            <ta e="T594" id="Seg_7669" s="T593">ir-An</ta>
            <ta e="T595" id="Seg_7670" s="T594">kaːl-TI-tA</ta>
            <ta e="T596" id="Seg_7671" s="T595">toŋ-An</ta>
            <ta e="T597" id="Seg_7672" s="T596">öl-BIT</ta>
            <ta e="T598" id="Seg_7673" s="T597">dʼaktar-tA</ta>
            <ta e="T599" id="Seg_7674" s="T598">tilin-TI-tA</ta>
            <ta e="T600" id="Seg_7675" s="T599">kör-BIT-tA</ta>
            <ta e="T601" id="Seg_7676" s="T600">bert</ta>
            <ta e="T602" id="Seg_7677" s="T601">bosku͡oj</ta>
            <ta e="T603" id="Seg_7678" s="T602">bagajɨ</ta>
            <ta e="T604" id="Seg_7679" s="T603">kɨːs</ta>
            <ta e="T605" id="Seg_7680" s="T604">e-BIT</ta>
            <ta e="T606" id="Seg_7681" s="T605">kɨːs</ta>
            <ta e="T607" id="Seg_7682" s="T606">ü͡ör-An</ta>
            <ta e="T608" id="Seg_7683" s="T607">muŋ-tA</ta>
            <ta e="T609" id="Seg_7684" s="T608">hu͡ok</ta>
            <ta e="T610" id="Seg_7685" s="T609">anɨ</ta>
            <ta e="T611" id="Seg_7686" s="T610">beje-m</ta>
            <ta e="T612" id="Seg_7687" s="T611">dʼi͡e-BA-r</ta>
            <ta e="T613" id="Seg_7688" s="T612">tij-IAK-m</ta>
            <ta e="T614" id="Seg_7689" s="T613">barɨs-I-BAT-GIn</ta>
            <ta e="T615" id="Seg_7690" s="T614">du͡o</ta>
            <ta e="T616" id="Seg_7691" s="T615">ahaː-A-t-IAK-m</ta>
            <ta e="T617" id="Seg_7692" s="T616">ičiges</ta>
            <ta e="T618" id="Seg_7693" s="T617">tellek-GA</ta>
            <ta e="T619" id="Seg_7694" s="T618">telgeː-IAK-m</ta>
            <ta e="T620" id="Seg_7695" s="T619">kördös-BIT</ta>
            <ta e="T621" id="Seg_7696" s="T620">kɨːs</ta>
            <ta e="T622" id="Seg_7697" s="T621">ɨraːk</ta>
            <ta e="T623" id="Seg_7698" s="T622">du͡o</ta>
            <ta e="T624" id="Seg_7699" s="T623">dʼi͡e-ŋ</ta>
            <ta e="T625" id="Seg_7700" s="T624">uː</ta>
            <ta e="T626" id="Seg_7701" s="T625">is-tI-GAr</ta>
            <ta e="T627" id="Seg_7702" s="T626">u͡ol</ta>
            <ta e="T628" id="Seg_7703" s="T627">ügüs</ta>
            <ta e="T629" id="Seg_7704" s="T628">oduː-nI</ta>
            <ta e="T630" id="Seg_7705" s="T629">kör-BIT-tA</ta>
            <ta e="T631" id="Seg_7706" s="T630">ol</ta>
            <ta e="T632" id="Seg_7707" s="T631">ihin</ta>
            <ta e="T633" id="Seg_7708" s="T632">kɨːs</ta>
            <ta e="T634" id="Seg_7709" s="T633">ös-tI-n</ta>
            <ta e="T635" id="Seg_7710" s="T634">hök-BA-TI-tA</ta>
            <ta e="T636" id="Seg_7711" s="T635">di͡e-A</ta>
            <ta e="T637" id="Seg_7712" s="T636">e-BIT</ta>
            <ta e="T638" id="Seg_7713" s="T637">min</ta>
            <ta e="T639" id="Seg_7714" s="T638">edʼij-BI-n</ta>
            <ta e="T640" id="Seg_7715" s="T639">bul-IAK-BI-n</ta>
            <ta e="T641" id="Seg_7716" s="T640">naːda</ta>
            <ta e="T642" id="Seg_7717" s="T641">kojut</ta>
            <ta e="T643" id="Seg_7718" s="T642">kel-IAK-m</ta>
            <ta e="T644" id="Seg_7719" s="T643">eːt</ta>
            <ta e="T645" id="Seg_7720" s="T644">köhüt</ta>
            <ta e="T646" id="Seg_7721" s="T645">u͡ol</ta>
            <ta e="T647" id="Seg_7722" s="T646">töhö-nI-kačča-nI</ta>
            <ta e="T648" id="Seg_7723" s="T647">bar-BIT-tA</ta>
            <ta e="T649" id="Seg_7724" s="T648">kim</ta>
            <ta e="T650" id="Seg_7725" s="T649">da</ta>
            <ta e="T651" id="Seg_7726" s="T650">bil-BAT</ta>
            <ta e="T652" id="Seg_7727" s="T651">agaj</ta>
            <ta e="T653" id="Seg_7728" s="T652">kadʼɨrɨk</ta>
            <ta e="T654" id="Seg_7729" s="T653">bu͡ol-BIT</ta>
            <ta e="T655" id="Seg_7730" s="T654">ahɨlɨk-LAːK</ta>
            <ta e="T656" id="Seg_7731" s="T655">bagajɨ</ta>
            <ta e="T657" id="Seg_7732" s="T656">taba-LAr-tA</ta>
            <ta e="T658" id="Seg_7733" s="T657">aččɨk-LAː-TI-LAr</ta>
            <ta e="T659" id="Seg_7734" s="T658">ahaː-A-t-IAK-GA</ta>
            <ta e="T660" id="Seg_7735" s="T659">du͡o</ta>
            <ta e="T661" id="Seg_7736" s="T660">di͡e-A</ta>
            <ta e="T662" id="Seg_7737" s="T661">hanaː-BIT</ta>
            <ta e="T663" id="Seg_7738" s="T662">hir</ta>
            <ta e="T664" id="Seg_7739" s="T663">ičči-tI-n</ta>
            <ta e="T665" id="Seg_7740" s="T664">haŋa-tI-n</ta>
            <ta e="T666" id="Seg_7741" s="T665">öjdöː-A-öjdöː-A</ta>
            <ta e="T667" id="Seg_7742" s="T666">taba-LAr-tI-n</ta>
            <ta e="T668" id="Seg_7743" s="T667">toktoː-t-BA-TI-tA</ta>
            <ta e="T669" id="Seg_7744" s="T668">onton</ta>
            <ta e="T670" id="Seg_7745" s="T669">di͡e-A</ta>
            <ta e="T671" id="Seg_7746" s="T670">hanaː-TI-tA</ta>
            <ta e="T672" id="Seg_7747" s="T671">olus</ta>
            <ta e="T673" id="Seg_7748" s="T672">aččɨk-LAː-BIT-LAr</ta>
            <ta e="T674" id="Seg_7749" s="T673">toktoː-A-t-IAK-GA</ta>
            <ta e="T675" id="Seg_7750" s="T674">toktoː-A-t-Ar-tI-n</ta>
            <ta e="T676" id="Seg_7751" s="T675">kɨtta</ta>
            <ta e="T677" id="Seg_7752" s="T676">orto-GI</ta>
            <ta e="T678" id="Seg_7753" s="T677">kölüj-Ar</ta>
            <ta e="T679" id="Seg_7754" s="T678">taba-tA</ta>
            <ta e="T680" id="Seg_7755" s="T679">melij-s</ta>
            <ta e="T681" id="Seg_7756" s="T680">ere</ta>
            <ta e="T682" id="Seg_7757" s="T681">gɨn-An</ta>
            <ta e="T683" id="Seg_7758" s="T682">kaːl-BIT</ta>
            <ta e="T684" id="Seg_7759" s="T683">kajdi͡ek</ta>
            <ta e="T685" id="Seg_7760" s="T684">bar-BIT-tA</ta>
            <ta e="T686" id="Seg_7761" s="T685">kajdi͡ek</ta>
            <ta e="T687" id="Seg_7762" s="T686">köt-BIT-tA</ta>
            <ta e="T688" id="Seg_7763" s="T687">emi͡e</ta>
            <ta e="T689" id="Seg_7764" s="T688">haŋa</ta>
            <ta e="T690" id="Seg_7765" s="T689">ihilin-I-BIT</ta>
            <ta e="T691" id="Seg_7766" s="T690">min</ta>
            <ta e="T692" id="Seg_7767" s="T691">en-GA</ta>
            <ta e="T693" id="Seg_7768" s="T692">di͡e-BIT-I-m</ta>
            <ta e="T694" id="Seg_7769" s="T693">ahaː-A-t-I-BA</ta>
            <ta e="T695" id="Seg_7770" s="T694">taba-LAr-GI-n</ta>
            <ta e="T696" id="Seg_7771" s="T695">u͡ol</ta>
            <ta e="T697" id="Seg_7772" s="T696">töhö-nI</ta>
            <ta e="T698" id="Seg_7773" s="T697">bar-BIT-tA</ta>
            <ta e="T699" id="Seg_7774" s="T698">dʼürü</ta>
            <ta e="T700" id="Seg_7775" s="T699">bu</ta>
            <ta e="T701" id="Seg_7776" s="T700">taːs</ta>
            <ta e="T702" id="Seg_7777" s="T701">kaja-tI-GAr</ta>
            <ta e="T703" id="Seg_7778" s="T702">tij-TI-tA</ta>
            <ta e="T704" id="Seg_7779" s="T703">ikki</ta>
            <ta e="T705" id="Seg_7780" s="T704">aŋɨ</ta>
            <ta e="T706" id="Seg_7781" s="T705">kajɨt-AːččI-tI-GAr</ta>
            <ta e="T707" id="Seg_7782" s="T706">agaj</ta>
            <ta e="T708" id="Seg_7783" s="T707">beje-tI-n</ta>
            <ta e="T709" id="Seg_7784" s="T708">hir-tI-GAr</ta>
            <ta e="T710" id="Seg_7785" s="T709">tij-BIT</ta>
            <ta e="T711" id="Seg_7786" s="T710">e-BIT</ta>
            <ta e="T712" id="Seg_7787" s="T711">toŋ-An</ta>
            <ta e="T713" id="Seg_7788" s="T712">öl-BIT</ta>
            <ta e="T714" id="Seg_7789" s="T713">taba-LAr</ta>
            <ta e="T715" id="Seg_7790" s="T714">mu͡os-LArA</ta>
            <ta e="T716" id="Seg_7791" s="T715">arɨːččɨ</ta>
            <ta e="T717" id="Seg_7792" s="T716">kaːr</ta>
            <ta e="T718" id="Seg_7793" s="T717">is-tI-ttAn</ta>
            <ta e="T719" id="Seg_7794" s="T718">čoros-Ar-LAr</ta>
            <ta e="T720" id="Seg_7795" s="T719">kaːr-nI</ta>
            <ta e="T721" id="Seg_7796" s="T720">kürt-An</ta>
            <ta e="T722" id="Seg_7797" s="T721">edʼij-tI-n</ta>
            <ta e="T723" id="Seg_7798" s="T722">nʼu͡oguhut-tI-n</ta>
            <ta e="T724" id="Seg_7799" s="T723">bul-TI-tA</ta>
            <ta e="T725" id="Seg_7800" s="T724">itiː</ta>
            <ta e="T726" id="Seg_7801" s="T725">timir</ta>
            <ta e="T727" id="Seg_7802" s="T726">mas-tI-nAn</ta>
            <ta e="T728" id="Seg_7803" s="T727">taːrɨj-TI-tA</ta>
            <ta e="T729" id="Seg_7804" s="T728">taba-tA</ta>
            <ta e="T730" id="Seg_7805" s="T729">tilin-An</ta>
            <ta e="T731" id="Seg_7806" s="T730">baran</ta>
            <ta e="T732" id="Seg_7807" s="T731">tur-TI-tA</ta>
            <ta e="T733" id="Seg_7808" s="T732">onton</ta>
            <ta e="T734" id="Seg_7809" s="T733">mas-nI</ta>
            <ta e="T735" id="Seg_7810" s="T734">ɨl-TI-tA</ta>
            <ta e="T736" id="Seg_7811" s="T735">kaːr-GA</ta>
            <ta e="T737" id="Seg_7812" s="T736">as-TI-tA</ta>
            <ta e="T738" id="Seg_7813" s="T737">toŋ-An</ta>
            <ta e="T739" id="Seg_7814" s="T738">öl-BIT</ta>
            <ta e="T740" id="Seg_7815" s="T739">taba-LAr</ta>
            <ta e="T741" id="Seg_7816" s="T740">honno</ta>
            <ta e="T742" id="Seg_7817" s="T741">barɨ-LArA</ta>
            <ta e="T743" id="Seg_7818" s="T742">tilin-TI-LAr</ta>
            <ta e="T744" id="Seg_7819" s="T743">bütün</ta>
            <ta e="T745" id="Seg_7820" s="T744">ü͡ör</ta>
            <ta e="T746" id="Seg_7821" s="T745">bu͡ol-An</ta>
            <ta e="T747" id="Seg_7822" s="T746">ahaː-A</ta>
            <ta e="T748" id="Seg_7823" s="T747">hɨrɨt-Ar-LAr</ta>
            <ta e="T749" id="Seg_7824" s="T748">haŋa</ta>
            <ta e="T750" id="Seg_7825" s="T749">hir-I-nAn</ta>
            <ta e="T751" id="Seg_7826" s="T750">kanʼɨs-BIT-tA</ta>
            <ta e="T752" id="Seg_7827" s="T751">edʼij-tA</ta>
            <ta e="T753" id="Seg_7828" s="T752">tur-Ar</ta>
            <ta e="T754" id="Seg_7829" s="T753">u͡ol</ta>
            <ta e="T755" id="Seg_7830" s="T754">edʼij-tI-n</ta>
            <ta e="T756" id="Seg_7831" s="T755">kör-An</ta>
            <ta e="T757" id="Seg_7832" s="T756">bert</ta>
            <ta e="T758" id="Seg_7833" s="T757">ü͡ör-TI-tA</ta>
            <ta e="T759" id="Seg_7834" s="T758">maŋnaj</ta>
            <ta e="T760" id="Seg_7835" s="T759">en</ta>
            <ta e="T761" id="Seg_7836" s="T760">di͡e-TI-tA</ta>
            <ta e="T762" id="Seg_7837" s="T761">edʼij-tA</ta>
            <ta e="T763" id="Seg_7838" s="T762">ör</ta>
            <ta e="T764" id="Seg_7839" s="T763">bagajɨ-LIk</ta>
            <ta e="T765" id="Seg_7840" s="T764">utuj-BIT-I-ŋ</ta>
            <ta e="T766" id="Seg_7841" s="T765">onton</ta>
            <ta e="T767" id="Seg_7842" s="T766">min</ta>
            <ta e="T768" id="Seg_7843" s="T767">haːs</ta>
            <ta e="T769" id="Seg_7844" s="T768">bu͡ol-BIT</ta>
            <ta e="T770" id="Seg_7845" s="T769">e-BIT</ta>
            <ta e="T771" id="Seg_7846" s="T770">kɨhɨn-nI</ta>
            <ta e="T772" id="Seg_7847" s="T771">meldʼi</ta>
            <ta e="T773" id="Seg_7848" s="T772">utuj-BIT-BIn</ta>
            <ta e="T774" id="Seg_7849" s="T773">e-BIT</ta>
            <ta e="T775" id="Seg_7850" s="T774">u͡ol</ta>
            <ta e="T776" id="Seg_7851" s="T775">edʼij-tI-n</ta>
            <ta e="T777" id="Seg_7852" s="T776">dʼi͡e-tI-GAr</ta>
            <ta e="T778" id="Seg_7853" s="T777">ilt-BIT-tA</ta>
            <ta e="T779" id="Seg_7854" s="T778">ü͡ör-tI-n</ta>
            <ta e="T780" id="Seg_7855" s="T779">üːr-An</ta>
            <ta e="T781" id="Seg_7856" s="T780">egel-TI-tA</ta>
            <ta e="T782" id="Seg_7857" s="T781">töhö</ta>
            <ta e="T783" id="Seg_7858" s="T782">da</ta>
            <ta e="T784" id="Seg_7859" s="T783">bu͡ol-BAkkA</ta>
            <ta e="T785" id="Seg_7860" s="T784">bu</ta>
            <ta e="T786" id="Seg_7861" s="T785">u͡ol</ta>
            <ta e="T787" id="Seg_7862" s="T786">terin-TI-tA</ta>
            <ta e="T788" id="Seg_7863" s="T787">kajdi͡ek</ta>
            <ta e="T789" id="Seg_7864" s="T788">bar-A-GIn</ta>
            <ta e="T790" id="Seg_7865" s="T789">ɨjɨt-BIT</ta>
            <ta e="T791" id="Seg_7866" s="T790">edʼij-tA</ta>
            <ta e="T792" id="Seg_7867" s="T791">hotoru</ta>
            <ta e="T793" id="Seg_7868" s="T792">kel-IAK-m</ta>
            <ta e="T794" id="Seg_7869" s="T793">kečes-I-m</ta>
            <ta e="T795" id="Seg_7870" s="T794">u͡ol</ta>
            <ta e="T796" id="Seg_7871" s="T795">bu</ta>
            <ta e="T797" id="Seg_7872" s="T796">kɨːs-nI</ta>
            <ta e="T798" id="Seg_7873" s="T797">kördöː-A</ta>
            <ta e="T799" id="Seg_7874" s="T798">bar-IːhI</ta>
            <ta e="T800" id="Seg_7875" s="T799">uː</ta>
            <ta e="T801" id="Seg_7876" s="T800">is-tI-GAr</ta>
            <ta e="T802" id="Seg_7877" s="T801">dʼi͡e-LAːK-nI</ta>
            <ta e="T803" id="Seg_7878" s="T802">baran</ta>
            <ta e="T804" id="Seg_7879" s="T803">is-An</ta>
            <ta e="T805" id="Seg_7880" s="T804">mɨlaː</ta>
            <ta e="T806" id="Seg_7881" s="T805">bagajɨ</ta>
            <ta e="T807" id="Seg_7882" s="T806">purgaː</ta>
            <ta e="T808" id="Seg_7883" s="T807">bu͡ol-BIT</ta>
            <ta e="T809" id="Seg_7884" s="T808">hir-tI-nAn</ta>
            <ta e="T810" id="Seg_7885" s="T809">da</ta>
            <ta e="T811" id="Seg_7886" s="T810">bar-Ar-tI-n</ta>
            <ta e="T812" id="Seg_7887" s="T811">bil-BAT</ta>
            <ta e="T813" id="Seg_7888" s="T812">buːs-tI-nAn</ta>
            <ta e="T814" id="Seg_7889" s="T813">da</ta>
            <ta e="T815" id="Seg_7890" s="T814">bar-Ar-tI-n</ta>
            <ta e="T816" id="Seg_7891" s="T815">bil-BAT</ta>
            <ta e="T817" id="Seg_7892" s="T816">öj-AlAː-An</ta>
            <ta e="T818" id="Seg_7893" s="T817">kör-BIT-tA</ta>
            <ta e="T819" id="Seg_7894" s="T818">bajgal</ta>
            <ta e="T820" id="Seg_7895" s="T819">buːs-tA</ta>
            <ta e="T821" id="Seg_7896" s="T820">e-BIT</ta>
            <ta e="T822" id="Seg_7897" s="T821">haːs-GI</ta>
            <ta e="T823" id="Seg_7898" s="T822">buːs</ta>
            <ta e="T824" id="Seg_7899" s="T823">iŋin-An</ta>
            <ta e="T825" id="Seg_7900" s="T824">u͡ol</ta>
            <ta e="T826" id="Seg_7901" s="T825">uː-GA</ta>
            <ta e="T827" id="Seg_7902" s="T826">tüs-BIT</ta>
            <ta e="T828" id="Seg_7903" s="T827">tüs-An</ta>
            <ta e="T829" id="Seg_7904" s="T828">hordoŋ</ta>
            <ta e="T830" id="Seg_7905" s="T829">bu͡ol-BIT</ta>
            <ta e="T831" id="Seg_7906" s="T830">uː</ta>
            <ta e="T832" id="Seg_7907" s="T831">is-tI-nAn</ta>
            <ta e="T833" id="Seg_7908" s="T832">uhun-A</ta>
            <ta e="T834" id="Seg_7909" s="T833">hɨrɨt-An</ta>
            <ta e="T835" id="Seg_7910" s="T834">ilim-GA</ta>
            <ta e="T836" id="Seg_7911" s="T835">tübes-BIT</ta>
            <ta e="T837" id="Seg_7912" s="T836">i͡enneːk</ta>
            <ta e="T838" id="Seg_7913" s="T837">ilim-GA</ta>
            <ta e="T839" id="Seg_7914" s="T838">hordoŋ-LAr</ta>
            <ta e="T840" id="Seg_7915" s="T839">agaj</ta>
            <ta e="T841" id="Seg_7916" s="T840">töhö</ta>
            <ta e="T842" id="Seg_7917" s="T841">da</ta>
            <ta e="T843" id="Seg_7918" s="T842">bu͡ol-BAkkA</ta>
            <ta e="T844" id="Seg_7919" s="T843">kɨːs</ta>
            <ta e="T845" id="Seg_7920" s="T844">kel-TI-tA</ta>
            <ta e="T846" id="Seg_7921" s="T845">bu</ta>
            <ta e="T847" id="Seg_7922" s="T846">kɨːs-tA</ta>
            <ta e="T848" id="Seg_7923" s="T847">tilin-TAr-AːččI-tA</ta>
            <ta e="T849" id="Seg_7924" s="T848">hordoŋ</ta>
            <ta e="T850" id="Seg_7925" s="T849">u͡ol-nI</ta>
            <ta e="T851" id="Seg_7926" s="T850">nʼu͡oguhut</ta>
            <ta e="T852" id="Seg_7927" s="T851">gɨn-An</ta>
            <ta e="T853" id="Seg_7928" s="T852">baran</ta>
            <ta e="T854" id="Seg_7929" s="T853">kölün-TI-tA</ta>
            <ta e="T855" id="Seg_7930" s="T854">dʼi͡e-tI-GAr</ta>
            <ta e="T856" id="Seg_7931" s="T855">bar-IːhI</ta>
            <ta e="T857" id="Seg_7932" s="T856">nʼu͡oguhut</ta>
            <ta e="T858" id="Seg_7933" s="T857">ihit-I-BAT</ta>
            <ta e="T859" id="Seg_7934" s="T858">kɨtɨl</ta>
            <ta e="T860" id="Seg_7935" s="T859">ere</ta>
            <ta e="T861" id="Seg_7936" s="T860">dek</ta>
            <ta e="T862" id="Seg_7937" s="T861">talas-Ar</ta>
            <ta e="T863" id="Seg_7938" s="T862">kɨːs</ta>
            <ta e="T864" id="Seg_7939" s="T863">ol-nI</ta>
            <ta e="T865" id="Seg_7940" s="T864">halaj-A</ta>
            <ta e="T866" id="Seg_7941" s="T865">hataː-Ar</ta>
            <ta e="T867" id="Seg_7942" s="T866">nʼu͡oguhut-tA</ta>
            <ta e="T868" id="Seg_7943" s="T867">bɨs-A</ta>
            <ta e="T869" id="Seg_7944" s="T868">mökküj-An</ta>
            <ta e="T870" id="Seg_7945" s="T869">kɨtɨl-GA</ta>
            <ta e="T871" id="Seg_7946" s="T870">tagɨs-TI-tA</ta>
            <ta e="T872" id="Seg_7947" s="T871">tagɨs-AːT</ta>
            <ta e="T873" id="Seg_7948" s="T872">u͡ol-hordoŋ</ta>
            <ta e="T874" id="Seg_7949" s="T873">ile</ta>
            <ta e="T875" id="Seg_7950" s="T874">u͡ol</ta>
            <ta e="T876" id="Seg_7951" s="T875">bu͡ol-BIT</ta>
            <ta e="T877" id="Seg_7952" s="T876">hordoŋ-LAr</ta>
            <ta e="T878" id="Seg_7953" s="T877">taba</ta>
            <ta e="T879" id="Seg_7954" s="T878">bu͡ol-TI-LAr</ta>
            <ta e="T880" id="Seg_7955" s="T879">kɨːs-LAːK</ta>
            <ta e="T881" id="Seg_7956" s="T880">u͡ol</ta>
            <ta e="T882" id="Seg_7957" s="T881">taːj-I-s-TI-LAr</ta>
            <ta e="T883" id="Seg_7958" s="T882">kɨːs</ta>
            <ta e="T884" id="Seg_7959" s="T883">ɨjɨt-TI-tA</ta>
            <ta e="T885" id="Seg_7960" s="T884">edʼij-GI-n</ta>
            <ta e="T886" id="Seg_7961" s="T885">bul-TI-ŋ</ta>
            <ta e="T887" id="Seg_7962" s="T886">kajdak</ta>
            <ta e="T888" id="Seg_7963" s="T887">olor-A-GIn</ta>
            <ta e="T889" id="Seg_7964" s="T888">barɨs</ta>
            <ta e="T890" id="Seg_7965" s="T889">min-n</ta>
            <ta e="T891" id="Seg_7966" s="T890">kɨtta</ta>
            <ta e="T892" id="Seg_7967" s="T891">di͡e-TI-tA</ta>
            <ta e="T893" id="Seg_7968" s="T892">u͡ol</ta>
            <ta e="T894" id="Seg_7969" s="T893">edʼij-I-m</ta>
            <ta e="T895" id="Seg_7970" s="T894">en-n</ta>
            <ta e="T896" id="Seg_7971" s="T895">köhüt-Ar</ta>
            <ta e="T897" id="Seg_7972" s="T896">bil-s-I-s-I-ŋ</ta>
            <ta e="T898" id="Seg_7973" s="T897">biːrge</ta>
            <ta e="T899" id="Seg_7974" s="T898">olor-IAK-BIt</ta>
            <ta e="T900" id="Seg_7975" s="T899">ü͡ör-BItI-n</ta>
            <ta e="T901" id="Seg_7976" s="T900">kolboː-IAK-BIt</ta>
            <ta e="T902" id="Seg_7977" s="T901">u͡ol-nI</ta>
            <ta e="T903" id="Seg_7978" s="T902">kɨtta</ta>
            <ta e="T904" id="Seg_7979" s="T903">kɨːs</ta>
            <ta e="T905" id="Seg_7980" s="T904">hordoŋ-LAr-nI</ta>
            <ta e="T906" id="Seg_7981" s="T905">uː</ta>
            <ta e="T907" id="Seg_7982" s="T906">is-tI-ttAn</ta>
            <ta e="T908" id="Seg_7983" s="T907">oroː-TI-LAr</ta>
            <ta e="T909" id="Seg_7984" s="T908">ol-tI-LArA</ta>
            <ta e="T910" id="Seg_7985" s="T909">bugdi</ta>
            <ta e="T911" id="Seg_7986" s="T910">taba</ta>
            <ta e="T912" id="Seg_7987" s="T911">bu͡ol-TI-LAr</ta>
            <ta e="T913" id="Seg_7988" s="T912">u͡ol</ta>
            <ta e="T914" id="Seg_7989" s="T913">edʼij-tA</ta>
            <ta e="T915" id="Seg_7990" s="T914">ü͡ör-An</ta>
            <ta e="T916" id="Seg_7991" s="T915">körüs-TI-tA</ta>
            <ta e="T917" id="Seg_7992" s="T916">u͡ol</ta>
            <ta e="T918" id="Seg_7993" s="T917">üs</ta>
            <ta e="T919" id="Seg_7994" s="T918">kün-nI</ta>
            <ta e="T920" id="Seg_7995" s="T919">utuj-TI-tA</ta>
            <ta e="T921" id="Seg_7996" s="T920">uhugun-An</ta>
            <ta e="T922" id="Seg_7997" s="T921">kör-BIT-tA</ta>
            <ta e="T923" id="Seg_7998" s="T922">edʼij-tA</ta>
            <ta e="T924" id="Seg_7999" s="T923">kɨːs-nI</ta>
            <ta e="T925" id="Seg_8000" s="T924">kɨtta</ta>
            <ta e="T926" id="Seg_8001" s="T925">kepset-A-kepset-A</ta>
            <ta e="T927" id="Seg_8002" s="T926">kül-s-A</ta>
            <ta e="T928" id="Seg_8003" s="T927">olor-Ar-LAr</ta>
            <ta e="T929" id="Seg_8004" s="T928">čaːj</ta>
            <ta e="T930" id="Seg_8005" s="T929">is-Ar-LAr</ta>
            <ta e="T931" id="Seg_8006" s="T930">bus-BIT</ta>
            <ta e="T932" id="Seg_8007" s="T931">et-nI</ta>
            <ta e="T933" id="Seg_8008" s="T932">hi͡e-Ar-LAr</ta>
            <ta e="T934" id="Seg_8009" s="T933">u͡ol</ta>
            <ta e="T935" id="Seg_8010" s="T934">ulka-LArI-GAr</ta>
            <ta e="T936" id="Seg_8011" s="T935">olor-TI-tA</ta>
            <ta e="T937" id="Seg_8012" s="T936">bütün</ta>
            <ta e="T938" id="Seg_8013" s="T937">taba-nI</ta>
            <ta e="T939" id="Seg_8014" s="T938">hi͡e-TI-tA</ta>
            <ta e="T940" id="Seg_8015" s="T939">u͡ol</ta>
            <ta e="T941" id="Seg_8016" s="T940">kɨːs-nI</ta>
            <ta e="T942" id="Seg_8017" s="T941">dʼaktar</ta>
            <ta e="T943" id="Seg_8018" s="T942">gɨn-BIT</ta>
            <ta e="T944" id="Seg_8019" s="T943">dʼolloːk</ta>
            <ta e="T945" id="Seg_8020" s="T944">bagajɨ-LIk</ta>
            <ta e="T946" id="Seg_8021" s="T945">olor-BIT-LAr</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_8022" s="T1">listen-IMP.2PL</ta>
            <ta e="T3" id="Seg_8023" s="T2">Dolgan-PL.[NOM]</ta>
            <ta e="T4" id="Seg_8024" s="T3">tale-3PL-ACC</ta>
            <ta e="T5" id="Seg_8025" s="T4">older.sister-PROPR.[NOM]</ta>
            <ta e="T6" id="Seg_8026" s="T5">boy.[NOM]</ta>
            <ta e="T7" id="Seg_8027" s="T6">say-CVB.SEQ</ta>
            <ta e="T8" id="Seg_8028" s="T7">name-ADJZ.[NOM]</ta>
            <ta e="T9" id="Seg_8029" s="T8">boy.[NOM]</ta>
            <ta e="T10" id="Seg_8030" s="T9">older.sister-3SG-ACC</ta>
            <ta e="T11" id="Seg_8031" s="T10">with</ta>
            <ta e="T12" id="Seg_8032" s="T11">two.by.two</ta>
            <ta e="T13" id="Seg_8033" s="T12">live-PST2-3PL</ta>
            <ta e="T14" id="Seg_8034" s="T13">older.sister-3SG.[NOM]</ta>
            <ta e="T15" id="Seg_8035" s="T14">worker-PROPR.[NOM]</ta>
            <ta e="T16" id="Seg_8036" s="T15">master.[NOM]</ta>
            <ta e="T17" id="Seg_8037" s="T16">very</ta>
            <ta e="T18" id="Seg_8038" s="T17">sledge-PL-3SG.[NOM]</ta>
            <ta e="T19" id="Seg_8039" s="T18">spotted</ta>
            <ta e="T20" id="Seg_8040" s="T19">reindeer.[NOM]</ta>
            <ta e="T21" id="Seg_8041" s="T20">fur-PL-EP-INSTR</ta>
            <ta e="T22" id="Seg_8042" s="T21">lid-PROPR-3PL</ta>
            <ta e="T23" id="Seg_8043" s="T22">pole.[NOM]</ta>
            <ta e="T24" id="Seg_8044" s="T23">tent-ACC</ta>
            <ta e="T25" id="Seg_8045" s="T24">self-3SG.[NOM]</ta>
            <ta e="T26" id="Seg_8046" s="T25">build-MED-HAB.[3SG]</ta>
            <ta e="T27" id="Seg_8047" s="T26">reindeer-3PL-ACC</ta>
            <ta e="T28" id="Seg_8048" s="T27">EMPH</ta>
            <ta e="T29" id="Seg_8049" s="T28">in.summer</ta>
            <ta e="T30" id="Seg_8050" s="T29">guard-HAB.[3SG]</ta>
            <ta e="T31" id="Seg_8051" s="T30">boy.[NOM]</ta>
            <ta e="T32" id="Seg_8052" s="T31">younger.brother-EP-3SG.[NOM]</ta>
            <ta e="T33" id="Seg_8053" s="T32">autumn-2SG-ABL</ta>
            <ta e="T34" id="Seg_8054" s="T33">sleep-PST2.[3SG]</ta>
            <ta e="T35" id="Seg_8055" s="T34">sun.[NOM]</ta>
            <ta e="T36" id="Seg_8056" s="T35">go.out-PTCP.FUT.[3SG]-DAT/LOC</ta>
            <ta e="T37" id="Seg_8057" s="T36">until</ta>
            <ta e="T38" id="Seg_8058" s="T37">herd-3PL.[NOM]</ta>
            <ta e="T39" id="Seg_8059" s="T38">move.away-PST2.[3SG]</ta>
            <ta e="T40" id="Seg_8060" s="T39">pasture-3PL.[NOM]</ta>
            <ta e="T41" id="Seg_8061" s="T40">get.broader-PST2.[3SG]</ta>
            <ta e="T42" id="Seg_8062" s="T41">girl.[NOM]</ta>
            <ta e="T43" id="Seg_8063" s="T42">pasture.[NOM]</ta>
            <ta e="T44" id="Seg_8064" s="T43">change-CAP.[3SG]</ta>
            <ta e="T45" id="Seg_8065" s="T44">herd-3SG-ACC</ta>
            <ta e="T46" id="Seg_8066" s="T45">bring-PST1-3SG</ta>
            <ta e="T47" id="Seg_8067" s="T46">reindeer.[NOM]</ta>
            <ta e="T48" id="Seg_8068" s="T47">catch-PST1-3SG</ta>
            <ta e="T49" id="Seg_8069" s="T48">harness-PST1-3SG</ta>
            <ta e="T50" id="Seg_8070" s="T49">younger.brother-EP-3SG-ACC</ta>
            <ta e="T51" id="Seg_8071" s="T50">wake.up-CAUS-CVB.SIM</ta>
            <ta e="T52" id="Seg_8072" s="T51">try-PRS.[3SG]</ta>
            <ta e="T53" id="Seg_8073" s="T52">at.all</ta>
            <ta e="T54" id="Seg_8074" s="T53">wake.up-EP-NEG.[3SG]</ta>
            <ta e="T55" id="Seg_8075" s="T54">pole.[NOM]</ta>
            <ta e="T56" id="Seg_8076" s="T55">tent-3SG-ACC</ta>
            <ta e="T57" id="Seg_8077" s="T56">remove-CVB.SEQ</ta>
            <ta e="T58" id="Seg_8078" s="T57">after</ta>
            <ta e="T59" id="Seg_8079" s="T58">younger.brother-EP-3SG-GEN</ta>
            <ta e="T60" id="Seg_8080" s="T59">polar.fox.[NOM]</ta>
            <ta e="T61" id="Seg_8081" s="T60">blanket-3SG-ACC</ta>
            <ta e="T62" id="Seg_8082" s="T61">completely</ta>
            <ta e="T63" id="Seg_8083" s="T62">pull-PST1-3SG</ta>
            <ta e="T64" id="Seg_8084" s="T63">be.excited-CVB.SEQ</ta>
            <ta e="T65" id="Seg_8085" s="T64">wake.up-PST1-3SG</ta>
            <ta e="T66" id="Seg_8086" s="T65">dress-PST1-3SG</ta>
            <ta e="T67" id="Seg_8087" s="T66">sledge-3SG-DAT/LOC</ta>
            <ta e="T68" id="Seg_8088" s="T67">sit.down-CVB.ANT</ta>
            <ta e="T69" id="Seg_8089" s="T68">again</ta>
            <ta e="T70" id="Seg_8090" s="T69">fall.asleep-PST1-3SG</ta>
            <ta e="T71" id="Seg_8091" s="T70">tree-VBZ-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T72" id="Seg_8092" s="T71">ice-VBZ-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T73" id="Seg_8093" s="T72">reindeer-VBZ-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T74" id="Seg_8094" s="T73">be-CVB.SEQ</ta>
            <ta e="T75" id="Seg_8095" s="T74">girl.[NOM]</ta>
            <ta e="T76" id="Seg_8096" s="T75">very</ta>
            <ta e="T77" id="Seg_8097" s="T76">get.tired-PRS.[3SG]</ta>
            <ta e="T78" id="Seg_8098" s="T77">get.tired-CVB.SEQ</ta>
            <ta e="T79" id="Seg_8099" s="T78">fall.asleep-PST2.[3SG]</ta>
            <ta e="T80" id="Seg_8100" s="T79">sleep-CVB.SIM</ta>
            <ta e="T81" id="Seg_8101" s="T80">lie-CVB.SEQ</ta>
            <ta e="T82" id="Seg_8102" s="T81">freeze-CVB.SEQ</ta>
            <ta e="T83" id="Seg_8103" s="T82">stay-PST2.[3SG]</ta>
            <ta e="T84" id="Seg_8104" s="T83">boy.[NOM]</ta>
            <ta e="T85" id="Seg_8105" s="T84">younger.brother-EP-3SG.[NOM]</ta>
            <ta e="T86" id="Seg_8106" s="T85">still</ta>
            <ta e="T87" id="Seg_8107" s="T86">three</ta>
            <ta e="T88" id="Seg_8108" s="T87">day-ACC</ta>
            <ta e="T89" id="Seg_8109" s="T88">sleep-PST2.[3SG]</ta>
            <ta e="T90" id="Seg_8110" s="T89">wake.up-EP-PST2-3SG</ta>
            <ta e="T91" id="Seg_8111" s="T90">see-PST2-3SG</ta>
            <ta e="T92" id="Seg_8112" s="T91">older.sister-3SG.[NOM]</ta>
            <ta e="T93" id="Seg_8113" s="T92">freeze-CVB.SEQ</ta>
            <ta e="T94" id="Seg_8114" s="T93">stay-PST2.[3SG]</ta>
            <ta e="T95" id="Seg_8115" s="T94">herd-3SG.[NOM]</ta>
            <ta e="T96" id="Seg_8116" s="T95">every-3SG.[NOM]</ta>
            <ta e="T97" id="Seg_8117" s="T96">freeze-CVB.SEQ</ta>
            <ta e="T98" id="Seg_8118" s="T97">die-PST2.[3SG]</ta>
            <ta e="T99" id="Seg_8119" s="T98">snowdrift.[NOM]</ta>
            <ta e="T100" id="Seg_8120" s="T99">beat-EP-PST2.[3SG]</ta>
            <ta e="T101" id="Seg_8121" s="T100">fire.[NOM]</ta>
            <ta e="T102" id="Seg_8122" s="T101">heat-TEMP-1SG</ta>
            <ta e="T103" id="Seg_8123" s="T102">maybe</ta>
            <ta e="T104" id="Seg_8124" s="T103">older.sister-EP-1SG.[NOM]</ta>
            <ta e="T105" id="Seg_8125" s="T104">revive-FUT.[3SG]</ta>
            <ta e="T106" id="Seg_8126" s="T105">EMPH</ta>
            <ta e="T107" id="Seg_8127" s="T106">say-CVB.SIM</ta>
            <ta e="T108" id="Seg_8128" s="T107">think-PST2.[3SG]</ta>
            <ta e="T109" id="Seg_8129" s="T108">boy.[NOM]</ta>
            <ta e="T110" id="Seg_8130" s="T109">wood-VBZ-CVB.SIM</ta>
            <ta e="T111" id="Seg_8131" s="T110">go-PTCP.FUT-DAT/LOC</ta>
            <ta e="T112" id="Seg_8132" s="T111">rock-PROPR.[NOM]</ta>
            <ta e="T113" id="Seg_8133" s="T112">river.[NOM]</ta>
            <ta e="T114" id="Seg_8134" s="T113">upper.part-3SG-INSTR</ta>
            <ta e="T115" id="Seg_8135" s="T114">forest.[NOM]</ta>
            <ta e="T116" id="Seg_8136" s="T115">to</ta>
            <ta e="T117" id="Seg_8137" s="T116">run-PST2.[3SG]</ta>
            <ta e="T118" id="Seg_8138" s="T117">see-PST2-3SG</ta>
            <ta e="T119" id="Seg_8139" s="T118">sky-ABL</ta>
            <ta e="T120" id="Seg_8140" s="T119">%%</ta>
            <ta e="T121" id="Seg_8141" s="T120">campfire.[NOM]</ta>
            <ta e="T122" id="Seg_8142" s="T121">flame-CVB.SIM</ta>
            <ta e="T123" id="Seg_8143" s="T122">lie-PRS.[3SG]</ta>
            <ta e="T124" id="Seg_8144" s="T123">evil.spirit.[NOM]</ta>
            <ta e="T125" id="Seg_8145" s="T124">skewer-DAT/LOC</ta>
            <ta e="T126" id="Seg_8146" s="T125">meat.[NOM]</ta>
            <ta e="T127" id="Seg_8147" s="T126">fry-EP-MED-CVB.SIM</ta>
            <ta e="T128" id="Seg_8148" s="T127">sit-PRS.[3SG]</ta>
            <ta e="T129" id="Seg_8149" s="T128">boy-ACC</ta>
            <ta e="T130" id="Seg_8150" s="T129">see-NEG.[3SG]</ta>
            <ta e="T131" id="Seg_8151" s="T130">this</ta>
            <ta e="T132" id="Seg_8152" s="T131">boy.[NOM]</ta>
            <ta e="T133" id="Seg_8153" s="T132">slip.down-CAUS-CVB.SIM</ta>
            <ta e="T134" id="Seg_8154" s="T133">touch-CVB.SIM-touch-CVB.SIM</ta>
            <ta e="T135" id="Seg_8155" s="T134">on.the.other.shore-2SG-ABL</ta>
            <ta e="T136" id="Seg_8156" s="T135">eat-CVB.SIM</ta>
            <ta e="T137" id="Seg_8157" s="T136">sit-PST2.[3SG]</ta>
            <ta e="T138" id="Seg_8158" s="T137">how</ta>
            <ta e="T139" id="Seg_8159" s="T138">collect.oneself-NEG.PTCP-3SG-ACC</ta>
            <ta e="T140" id="Seg_8160" s="T139">well</ta>
            <ta e="T141" id="Seg_8161" s="T140">think-PRS.[3SG]</ta>
            <ta e="T142" id="Seg_8162" s="T141">evil.spirit.[NOM]</ta>
            <ta e="T143" id="Seg_8163" s="T142">meat-PL-EP-1SG.[NOM]</ta>
            <ta e="T144" id="Seg_8164" s="T143">fire-DAT/LOC</ta>
            <ta e="T145" id="Seg_8165" s="T144">fall-PTCP.PRS-3PL-ACC</ta>
            <ta e="T146" id="Seg_8166" s="T145">evil.spirit.[NOM]</ta>
            <ta e="T147" id="Seg_8167" s="T146">boy-ACC</ta>
            <ta e="T148" id="Seg_8168" s="T147">see-CVB.SEQ</ta>
            <ta e="T149" id="Seg_8169" s="T148">kill-CVB.PURP</ta>
            <ta e="T150" id="Seg_8170" s="T149">want-PST2.[3SG]</ta>
            <ta e="T151" id="Seg_8171" s="T150">boy.[NOM]</ta>
            <ta e="T152" id="Seg_8172" s="T151">mitten-3SG-INSTR</ta>
            <ta e="T153" id="Seg_8173" s="T152">evil.spirit.[NOM]</ta>
            <ta e="T154" id="Seg_8174" s="T153">eight</ta>
            <ta e="T155" id="Seg_8175" s="T154">head-3SG-ACC</ta>
            <ta e="T156" id="Seg_8176" s="T155">completely</ta>
            <ta e="T157" id="Seg_8177" s="T156">beat-EP-PST2.[3SG]</ta>
            <ta e="T158" id="Seg_8178" s="T157">head-PL-3SG.[NOM]</ta>
            <ta e="T159" id="Seg_8179" s="T158">dance-CVB.SIM</ta>
            <ta e="T160" id="Seg_8180" s="T159">go-PRS-3PL</ta>
            <ta e="T161" id="Seg_8181" s="T160">boy.[NOM]</ta>
            <ta e="T162" id="Seg_8182" s="T161">there</ta>
            <ta e="T163" id="Seg_8183" s="T162">scream.out-PST2.[3SG]</ta>
            <ta e="T164" id="Seg_8184" s="T163">hero.[NOM]</ta>
            <ta e="T165" id="Seg_8185" s="T164">two-VBZ-PTCP.HAB-3SG</ta>
            <ta e="T166" id="Seg_8186" s="T165">three-ORD-VBZ-PTCP.HAB-3SG</ta>
            <ta e="T167" id="Seg_8187" s="T166">NEG.[3SG]</ta>
            <ta e="T168" id="Seg_8188" s="T167">evil.spirit.[NOM]</ta>
            <ta e="T169" id="Seg_8189" s="T168">breast-3SG-ACC</ta>
            <ta e="T170" id="Seg_8190" s="T169">unstitch-CVB.SIM</ta>
            <ta e="T171" id="Seg_8191" s="T170">pull-CVB.SEQ</ta>
            <ta e="T172" id="Seg_8192" s="T171">stone-ACC</ta>
            <ta e="T173" id="Seg_8193" s="T172">get.out-PST1-3SG</ta>
            <ta e="T174" id="Seg_8194" s="T173">stone.[NOM]</ta>
            <ta e="T175" id="Seg_8195" s="T174">heart-PROPR.[NOM]</ta>
            <ta e="T176" id="Seg_8196" s="T175">be-PST2.[3SG]</ta>
            <ta e="T177" id="Seg_8197" s="T176">boy.[NOM]</ta>
            <ta e="T178" id="Seg_8198" s="T177">stone-ACC</ta>
            <ta e="T179" id="Seg_8199" s="T178">kick-CVB.SIM-kick-CVB.SIM</ta>
            <ta e="T180" id="Seg_8200" s="T179">go-CVB.SIM</ta>
            <ta e="T181" id="Seg_8201" s="T180">stand-PST2.[3SG]</ta>
            <ta e="T182" id="Seg_8202" s="T181">front-3SG-DAT/LOC</ta>
            <ta e="T183" id="Seg_8203" s="T182">earth.[NOM]</ta>
            <ta e="T184" id="Seg_8204" s="T183">half-PROPR.[NOM]</ta>
            <ta e="T185" id="Seg_8205" s="T184">big.as</ta>
            <ta e="T186" id="Seg_8206" s="T185">stone.[NOM]</ta>
            <ta e="T187" id="Seg_8207" s="T186">stand.up-PST2.[3SG]</ta>
            <ta e="T188" id="Seg_8208" s="T187">stone.[NOM]</ta>
            <ta e="T189" id="Seg_8209" s="T188">mountain.[NOM]</ta>
            <ta e="T190" id="Seg_8210" s="T189">stone.[NOM]</ta>
            <ta e="T191" id="Seg_8211" s="T190">mountain-ACC</ta>
            <ta e="T192" id="Seg_8212" s="T191">stone.[NOM]</ta>
            <ta e="T193" id="Seg_8213" s="T192">heart-3SG-INSTR</ta>
            <ta e="T194" id="Seg_8214" s="T193">kick-PST2.[3SG]</ta>
            <ta e="T195" id="Seg_8215" s="T194">mountain-3SG.[NOM]</ta>
            <ta e="T196" id="Seg_8216" s="T195">two</ta>
            <ta e="T197" id="Seg_8217" s="T196">direction.[NOM]</ta>
            <ta e="T198" id="Seg_8218" s="T197">tear-CVB.SEQ</ta>
            <ta e="T199" id="Seg_8219" s="T198">fall-PST2.[3SG]</ta>
            <ta e="T200" id="Seg_8220" s="T199">watch-CVB.SEQ</ta>
            <ta e="T201" id="Seg_8221" s="T200">see-PST2-3SG</ta>
            <ta e="T202" id="Seg_8222" s="T201">earth.[NOM]</ta>
            <ta e="T203" id="Seg_8223" s="T202">to.be.on.view-PRS.[3SG]</ta>
            <ta e="T204" id="Seg_8224" s="T203">thither</ta>
            <ta e="T205" id="Seg_8225" s="T204">go-CVB.SEQ</ta>
            <ta e="T206" id="Seg_8226" s="T205">sledge-PROPR.[NOM]</ta>
            <ta e="T207" id="Seg_8227" s="T206">trace-3SG-ACC</ta>
            <ta e="T208" id="Seg_8228" s="T207">see-PST2.[3SG]</ta>
            <ta e="T209" id="Seg_8229" s="T208">deadfall.[NOM]</ta>
            <ta e="T210" id="Seg_8230" s="T209">gin.trap.[NOM]</ta>
            <ta e="T211" id="Seg_8231" s="T210">see-EP-MED-CVB.SIM</ta>
            <ta e="T212" id="Seg_8232" s="T211">go-PTCP.PRS</ta>
            <ta e="T213" id="Seg_8233" s="T212">human.being.[NOM]</ta>
            <ta e="T214" id="Seg_8234" s="T213">be-PST1-3SG</ta>
            <ta e="T215" id="Seg_8235" s="T214">be-PST2.[3SG]</ta>
            <ta e="T216" id="Seg_8236" s="T215">how.much</ta>
            <ta e="T217" id="Seg_8237" s="T216">NEG</ta>
            <ta e="T218" id="Seg_8238" s="T217">be-NEG.CVB.SIM</ta>
            <ta e="T219" id="Seg_8239" s="T218">polar.fox-AG.[NOM]</ta>
            <ta e="T220" id="Seg_8240" s="T219">towards</ta>
            <ta e="T221" id="Seg_8241" s="T220">go-PTCP.PRS-3SG-ACC</ta>
            <ta e="T222" id="Seg_8242" s="T221">see-PST2.[3SG]</ta>
            <ta e="T223" id="Seg_8243" s="T222">come-CVB.SEQ</ta>
            <ta e="T224" id="Seg_8244" s="T223">stand-PST1-3SG</ta>
            <ta e="T225" id="Seg_8245" s="T224">greet-PST1-3SG</ta>
            <ta e="T226" id="Seg_8246" s="T225">what-ACC</ta>
            <ta e="T227" id="Seg_8247" s="T226">make-CVB.SIM</ta>
            <ta e="T228" id="Seg_8248" s="T227">go-PRS-2SG</ta>
            <ta e="T229" id="Seg_8249" s="T228">ask-PST2.[3SG]</ta>
            <ta e="T230" id="Seg_8250" s="T229">boy.[NOM]</ta>
            <ta e="T231" id="Seg_8251" s="T230">deadfall.[NOM]</ta>
            <ta e="T232" id="Seg_8252" s="T231">gin.trap.[NOM]</ta>
            <ta e="T233" id="Seg_8253" s="T232">see-EP-MED-PRS-1SG</ta>
            <ta e="T234" id="Seg_8254" s="T233">far.away</ta>
            <ta e="T235" id="Seg_8255" s="T234">house-PROPR-2PL</ta>
            <ta e="T236" id="Seg_8256" s="T235">Q</ta>
            <ta e="T237" id="Seg_8257" s="T236">how.much</ta>
            <ta e="T238" id="Seg_8258" s="T237">human.being-2PL=Q</ta>
            <ta e="T239" id="Seg_8259" s="T238">mother-1SG-ACC</ta>
            <ta e="T240" id="Seg_8260" s="T239">father-1SG-ACC</ta>
            <ta e="T241" id="Seg_8261" s="T240">with</ta>
            <ta e="T242" id="Seg_8262" s="T241">live-PRS-1SG</ta>
            <ta e="T243" id="Seg_8263" s="T242">say-PST2.[3SG]</ta>
            <ta e="T244" id="Seg_8264" s="T243">boy.[NOM]</ta>
            <ta e="T245" id="Seg_8265" s="T244">ride.[IMP.2SG]</ta>
            <ta e="T246" id="Seg_8266" s="T245">take.away-FUT-1SG</ta>
            <ta e="T247" id="Seg_8267" s="T246">guest.[NOM]</ta>
            <ta e="T248" id="Seg_8268" s="T247">be.[IMP.2SG]</ta>
            <ta e="T249" id="Seg_8269" s="T248">pole.[NOM]</ta>
            <ta e="T250" id="Seg_8270" s="T249">tent-3SG-DAT/LOC</ta>
            <ta e="T251" id="Seg_8271" s="T250">bring-PST2.[3SG]</ta>
            <ta e="T252" id="Seg_8272" s="T251">old.man-PROPR</ta>
            <ta e="T253" id="Seg_8273" s="T252">old.woman.[NOM]</ta>
            <ta e="T254" id="Seg_8274" s="T253">towards</ta>
            <ta e="T255" id="Seg_8275" s="T254">go.out-CVB.SEQ-3PL</ta>
            <ta e="T256" id="Seg_8276" s="T255">meet-EP-PST2-3PL</ta>
            <ta e="T257" id="Seg_8277" s="T256">well</ta>
            <ta e="T258" id="Seg_8278" s="T257">place-ABL</ta>
            <ta e="T259" id="Seg_8279" s="T258">come-PST1-2SG</ta>
            <ta e="T260" id="Seg_8280" s="T259">ask-PST2-3PL</ta>
            <ta e="T261" id="Seg_8281" s="T260">hither</ta>
            <ta e="T262" id="Seg_8282" s="T261">across</ta>
            <ta e="T263" id="Seg_8283" s="T262">eye-PROPR.[NOM]</ta>
            <ta e="T264" id="Seg_8284" s="T263">fire.[NOM]</ta>
            <ta e="T265" id="Seg_8285" s="T264">sole-PROPR.[NOM]</ta>
            <ta e="T266" id="Seg_8286" s="T265">go-EP-MED-CVB.SIM=not.yet.[3SG]</ta>
            <ta e="T267" id="Seg_8287" s="T266">boy.[NOM]</ta>
            <ta e="T268" id="Seg_8288" s="T267">tell-PST2.[3SG]</ta>
            <ta e="T269" id="Seg_8289" s="T268">older.sister-EP-1SG.[NOM]</ta>
            <ta e="T270" id="Seg_8290" s="T269">freeze-CVB.SEQ</ta>
            <ta e="T271" id="Seg_8291" s="T270">die-PST1-3SG</ta>
            <ta e="T272" id="Seg_8292" s="T271">reindeer-PL-1PL.[NOM]</ta>
            <ta e="T273" id="Seg_8293" s="T272">also</ta>
            <ta e="T274" id="Seg_8294" s="T273">freeze-CVB.SEQ</ta>
            <ta e="T275" id="Seg_8295" s="T274">die-PST1-3PL</ta>
            <ta e="T276" id="Seg_8296" s="T275">snow.[NOM]</ta>
            <ta e="T277" id="Seg_8297" s="T276">inside-3SG-DAT/LOC</ta>
            <ta e="T278" id="Seg_8298" s="T277">lie-PRS-3PL</ta>
            <ta e="T279" id="Seg_8299" s="T278">flurry-EP-PASS/REFL-CVB.SIM</ta>
            <ta e="T280" id="Seg_8300" s="T279">how</ta>
            <ta e="T281" id="Seg_8301" s="T280">human.being.[NOM]</ta>
            <ta e="T282" id="Seg_8302" s="T281">be-FUT-1SG</ta>
            <ta e="T283" id="Seg_8303" s="T282">Q</ta>
            <ta e="T284" id="Seg_8304" s="T283">at.first</ta>
            <ta e="T285" id="Seg_8305" s="T284">guest.[NOM]</ta>
            <ta e="T286" id="Seg_8306" s="T285">be.[IMP.2SG]</ta>
            <ta e="T287" id="Seg_8307" s="T286">say-PST2.[3SG]</ta>
            <ta e="T288" id="Seg_8308" s="T287">old.man.[NOM]</ta>
            <ta e="T289" id="Seg_8309" s="T288">then</ta>
            <ta e="T290" id="Seg_8310" s="T289">advise-EP-RECP/COLL-FUT-1PL</ta>
            <ta e="T291" id="Seg_8311" s="T290">son-3SG.[NOM]</ta>
            <ta e="T292" id="Seg_8312" s="T291">go.out-CVB.SEQ</ta>
            <ta e="T293" id="Seg_8313" s="T292">female.reindeer-ACC</ta>
            <ta e="T294" id="Seg_8314" s="T293">kill-PST2.[3SG]</ta>
            <ta e="T295" id="Seg_8315" s="T294">edge-PROPR.[NOM]</ta>
            <ta e="T296" id="Seg_8316" s="T295">female.reindeer-ACC</ta>
            <ta e="T297" id="Seg_8317" s="T296">guest.[NOM]</ta>
            <ta e="T298" id="Seg_8318" s="T297">that-ACC</ta>
            <ta e="T299" id="Seg_8319" s="T298">lonely</ta>
            <ta e="T300" id="Seg_8320" s="T299">eat-PST2.[3SG]</ta>
            <ta e="T301" id="Seg_8321" s="T300">eat.ones.fill-PST1-2SG</ta>
            <ta e="T302" id="Seg_8322" s="T301">ask-PST2-3PL</ta>
            <ta e="T303" id="Seg_8323" s="T302">eat.ones.fill-PST1-1SG</ta>
            <ta e="T304" id="Seg_8324" s="T303">autumn-2SG-ABL</ta>
            <ta e="T305" id="Seg_8325" s="T304">eat-CVB.SIM</ta>
            <ta e="T306" id="Seg_8326" s="T305">not.yet-1SG</ta>
            <ta e="T307" id="Seg_8327" s="T306">now</ta>
            <ta e="T308" id="Seg_8328" s="T307">relax-FUT-1SG</ta>
            <ta e="T309" id="Seg_8329" s="T308">three</ta>
            <ta e="T310" id="Seg_8330" s="T309">day-ACC</ta>
            <ta e="T311" id="Seg_8331" s="T310">boy.[NOM]</ta>
            <ta e="T312" id="Seg_8332" s="T311">sleep-PST2.[3SG]</ta>
            <ta e="T313" id="Seg_8333" s="T312">wake.up-EP-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T314" id="Seg_8334" s="T313">old.man.[NOM]</ta>
            <ta e="T315" id="Seg_8335" s="T314">most</ta>
            <ta e="T316" id="Seg_8336" s="T315">fat.[NOM]</ta>
            <ta e="T317" id="Seg_8337" s="T316">reindeer-ACC</ta>
            <ta e="T318" id="Seg_8338" s="T317">kill-CVB.SEQ</ta>
            <ta e="T319" id="Seg_8339" s="T318">eat-DRV-PST2.[3SG]</ta>
            <ta e="T320" id="Seg_8340" s="T319">old.man.[NOM]</ta>
            <ta e="T321" id="Seg_8341" s="T320">three</ta>
            <ta e="T322" id="Seg_8342" s="T321">month-ADJZ.[NOM]</ta>
            <ta e="T323" id="Seg_8343" s="T322">food-ACC</ta>
            <ta e="T324" id="Seg_8344" s="T323">ready-VBZ-PST2.[3SG]</ta>
            <ta e="T325" id="Seg_8345" s="T324">go-PTCP.FUT-3SG-ACC</ta>
            <ta e="T326" id="Seg_8346" s="T325">boy.[NOM]</ta>
            <ta e="T327" id="Seg_8347" s="T326">say-PST2.[3SG]</ta>
            <ta e="T328" id="Seg_8348" s="T327">where</ta>
            <ta e="T329" id="Seg_8349" s="T328">INDEF</ta>
            <ta e="T330" id="Seg_8350" s="T329">earth.[NOM]</ta>
            <ta e="T331" id="Seg_8351" s="T330">spirit-3SG.[NOM]</ta>
            <ta e="T332" id="Seg_8352" s="T331">there.is</ta>
            <ta e="T333" id="Seg_8353" s="T332">it.is.said</ta>
            <ta e="T334" id="Seg_8354" s="T333">thither</ta>
            <ta e="T335" id="Seg_8355" s="T334">go-FUT-1SG</ta>
            <ta e="T336" id="Seg_8356" s="T335">boy.[NOM]</ta>
            <ta e="T337" id="Seg_8357" s="T336">sledge-3SG-DAT/LOC</ta>
            <ta e="T338" id="Seg_8358" s="T337">stone.[NOM]</ta>
            <ta e="T339" id="Seg_8359" s="T338">heart-ACC</ta>
            <ta e="T340" id="Seg_8360" s="T339">lay-PST1-3SG</ta>
            <ta e="T341" id="Seg_8361" s="T340">river.[NOM]</ta>
            <ta e="T342" id="Seg_8362" s="T341">inside-3SG-INSTR</ta>
            <ta e="T343" id="Seg_8363" s="T342">hit.the.road-CVB.SEQ</ta>
            <ta e="T344" id="Seg_8364" s="T343">stay-PST1-3SG</ta>
            <ta e="T345" id="Seg_8365" s="T344">how.much</ta>
            <ta e="T346" id="Seg_8366" s="T345">long-ADVZ</ta>
            <ta e="T347" id="Seg_8367" s="T346">go-PST2-3SG</ta>
            <ta e="T348" id="Seg_8368" s="T347">Q</ta>
            <ta e="T349" id="Seg_8369" s="T348">only</ta>
            <ta e="T350" id="Seg_8370" s="T349">cast.iron.[NOM]</ta>
            <ta e="T351" id="Seg_8371" s="T350">pole.[NOM]</ta>
            <ta e="T352" id="Seg_8372" s="T351">tent-ACC</ta>
            <ta e="T353" id="Seg_8373" s="T352">see-PST2.[3SG]</ta>
            <ta e="T354" id="Seg_8374" s="T353">door-ACC</ta>
            <ta e="T355" id="Seg_8375" s="T354">open-CVB.SEQ</ta>
            <ta e="T356" id="Seg_8376" s="T355">see-PST1-3SG</ta>
            <ta e="T357" id="Seg_8377" s="T356">old.[NOM]</ta>
            <ta e="T358" id="Seg_8378" s="T357">very</ta>
            <ta e="T359" id="Seg_8379" s="T358">old.woman.[NOM]</ta>
            <ta e="T360" id="Seg_8380" s="T359">sit-PRS.[3SG]</ta>
            <ta e="T361" id="Seg_8381" s="T360">place.beneath-3SG-DAT/LOC</ta>
            <ta e="T362" id="Seg_8382" s="T361">bellows.[NOM]</ta>
            <ta e="T363" id="Seg_8383" s="T362">lie-PRS.[3SG]</ta>
            <ta e="T364" id="Seg_8384" s="T363">head-3SG-GEN</ta>
            <ta e="T365" id="Seg_8385" s="T364">upper.part-3SG-DAT/LOC</ta>
            <ta e="T366" id="Seg_8386" s="T365">bag.[NOM]</ta>
            <ta e="T367" id="Seg_8387" s="T366">hang.up-PASS/REFL-EP-PST2.[3SG]</ta>
            <ta e="T368" id="Seg_8388" s="T367">bag.[NOM]</ta>
            <ta e="T369" id="Seg_8389" s="T368">human.being.[NOM]</ta>
            <ta e="T370" id="Seg_8390" s="T369">face-3SG-ACC</ta>
            <ta e="T371" id="Seg_8391" s="T370">similar.to</ta>
            <ta e="T372" id="Seg_8392" s="T371">stove.[NOM]</ta>
            <ta e="T373" id="Seg_8393" s="T372">lower.part-EP-3SG.[NOM]</ta>
            <ta e="T374" id="Seg_8394" s="T373">hollow.[NOM]</ta>
            <ta e="T375" id="Seg_8395" s="T374">river.[NOM]</ta>
            <ta e="T376" id="Seg_8396" s="T375">flow-CVB.SIM</ta>
            <ta e="T377" id="Seg_8397" s="T376">lie-PRS.[3SG]</ta>
            <ta e="T378" id="Seg_8398" s="T377">river-DAT/LOC</ta>
            <ta e="T379" id="Seg_8399" s="T378">small.boat-PROPR.[NOM]</ta>
            <ta e="T380" id="Seg_8400" s="T379">net-VBZ-CVB.SIM</ta>
            <ta e="T381" id="Seg_8401" s="T380">go-PRS.[3SG]</ta>
            <ta e="T382" id="Seg_8402" s="T381">bag.[NOM]</ta>
            <ta e="T383" id="Seg_8403" s="T382">language-PROPR.[NOM]</ta>
            <ta e="T384" id="Seg_8404" s="T383">become-PST2.[3SG]</ta>
            <ta e="T385" id="Seg_8405" s="T384">guest.[NOM]</ta>
            <ta e="T386" id="Seg_8406" s="T385">come-PST1-3SG</ta>
            <ta e="T387" id="Seg_8407" s="T386">wood-AG-PL.[NOM]</ta>
            <ta e="T388" id="Seg_8408" s="T387">tree-VBZ-IMP.2PL</ta>
            <ta e="T389" id="Seg_8409" s="T388">fire-PART</ta>
            <ta e="T390" id="Seg_8410" s="T389">light-EP-IMP.2PL</ta>
            <ta e="T391" id="Seg_8411" s="T390">wood-3PL.[NOM]</ta>
            <ta e="T392" id="Seg_8412" s="T391">self-3PL.[NOM]</ta>
            <ta e="T393" id="Seg_8413" s="T392">go.in-PST1-3PL</ta>
            <ta e="T394" id="Seg_8414" s="T393">from.outside</ta>
            <ta e="T395" id="Seg_8415" s="T394">boy.[NOM]</ta>
            <ta e="T396" id="Seg_8416" s="T395">understand-PST1-3SG</ta>
            <ta e="T397" id="Seg_8417" s="T396">iron.[NOM]</ta>
            <ta e="T398" id="Seg_8418" s="T397">wood-PL.[NOM]</ta>
            <ta e="T399" id="Seg_8419" s="T398">be-PST2.[3SG]</ta>
            <ta e="T400" id="Seg_8420" s="T399">old.woman.[NOM]</ta>
            <ta e="T401" id="Seg_8421" s="T400">stand.up-CVB.SEQ</ta>
            <ta e="T402" id="Seg_8422" s="T401">wood-PL-ACC</ta>
            <ta e="T403" id="Seg_8423" s="T402">stove-3SG-DAT/LOC</ta>
            <ta e="T404" id="Seg_8424" s="T403">gather-PST1-3SG</ta>
            <ta e="T405" id="Seg_8425" s="T404">bag.[NOM]</ta>
            <ta e="T406" id="Seg_8426" s="T405">say-PST1-3SG</ta>
            <ta e="T407" id="Seg_8427" s="T406">bellows.[NOM]</ta>
            <ta e="T408" id="Seg_8428" s="T407">bellows-VBZ.[IMP.2SG]</ta>
            <ta e="T409" id="Seg_8429" s="T408">pole.[NOM]</ta>
            <ta e="T410" id="Seg_8430" s="T409">tent.[NOM]</ta>
            <ta e="T411" id="Seg_8431" s="T410">iron</ta>
            <ta e="T412" id="Seg_8432" s="T411">floor-3SG.[NOM]</ta>
            <ta e="T413" id="Seg_8433" s="T412">get.red-INCH</ta>
            <ta e="T414" id="Seg_8434" s="T413">go-PST1-3SG</ta>
            <ta e="T415" id="Seg_8435" s="T414">bag.[NOM]</ta>
            <ta e="T416" id="Seg_8436" s="T415">always</ta>
            <ta e="T417" id="Seg_8437" s="T416">say-PRS.[3SG]</ta>
            <ta e="T418" id="Seg_8438" s="T417">bellows.[NOM]</ta>
            <ta e="T419" id="Seg_8439" s="T418">bellows-VBZ.[IMP.2SG]</ta>
            <ta e="T420" id="Seg_8440" s="T419">bellows-VBZ.[IMP.2SG]</ta>
            <ta e="T421" id="Seg_8441" s="T420">guest.[NOM]</ta>
            <ta e="T422" id="Seg_8442" s="T421">come-PST1-3SG</ta>
            <ta e="T423" id="Seg_8443" s="T422">fire-PART</ta>
            <ta e="T424" id="Seg_8444" s="T423">add.[IMP.2SG]</ta>
            <ta e="T425" id="Seg_8445" s="T424">pole.[NOM]</ta>
            <ta e="T426" id="Seg_8446" s="T425">tent.[NOM]</ta>
            <ta e="T427" id="Seg_8447" s="T426">every-3SG.[NOM]</ta>
            <ta e="T428" id="Seg_8448" s="T427">get.red-PST1-3SG</ta>
            <ta e="T429" id="Seg_8449" s="T428">old.woman.[NOM]</ta>
            <ta e="T430" id="Seg_8450" s="T429">guest.[NOM]</ta>
            <ta e="T431" id="Seg_8451" s="T430">boy-ACC</ta>
            <ta e="T432" id="Seg_8452" s="T431">beg-PST1-3SG</ta>
            <ta e="T433" id="Seg_8453" s="T432">become.warm-CAUS-EP-NEG.[IMP.2SG]</ta>
            <ta e="T434" id="Seg_8454" s="T433">very</ta>
            <ta e="T435" id="Seg_8455" s="T434">cool-EP-CAUS.[IMP.2SG]</ta>
            <ta e="T436" id="Seg_8456" s="T435">well</ta>
            <ta e="T437" id="Seg_8457" s="T436">boy.[NOM]</ta>
            <ta e="T438" id="Seg_8458" s="T437">stone.[NOM]</ta>
            <ta e="T439" id="Seg_8459" s="T438">heart-ACC</ta>
            <ta e="T440" id="Seg_8460" s="T439">take-CVB.SEQ</ta>
            <ta e="T441" id="Seg_8461" s="T440">stove.[NOM]</ta>
            <ta e="T442" id="Seg_8462" s="T441">middle-3SG-DAT/LOC</ta>
            <ta e="T443" id="Seg_8463" s="T442">throw-PST1-3SG</ta>
            <ta e="T444" id="Seg_8464" s="T443">pole.[NOM]</ta>
            <ta e="T445" id="Seg_8465" s="T444">tent.[NOM]</ta>
            <ta e="T446" id="Seg_8466" s="T445">cool-PST1-3SG</ta>
            <ta e="T447" id="Seg_8467" s="T446">floor-3SG.[NOM]</ta>
            <ta e="T448" id="Seg_8468" s="T447">get.red-NEG-PST1-3SG</ta>
            <ta e="T449" id="Seg_8469" s="T448">old.woman.[NOM]</ta>
            <ta e="T450" id="Seg_8470" s="T449">again</ta>
            <ta e="T451" id="Seg_8471" s="T450">strongly</ta>
            <ta e="T452" id="Seg_8472" s="T451">rime.[NOM]</ta>
            <ta e="T453" id="Seg_8473" s="T452">be-PST1-3SG</ta>
            <ta e="T454" id="Seg_8474" s="T453">very</ta>
            <ta e="T455" id="Seg_8475" s="T454">freeze-EP-CAUS-EP-NEG.[IMP.2SG]</ta>
            <ta e="T456" id="Seg_8476" s="T455">beg-PST2.[3SG]</ta>
            <ta e="T0" id="Seg_8477" s="T456">bag.[NOM]</ta>
            <ta e="T947" id="Seg_8478" s="T0">every.[NOM]</ta>
            <ta e="T948" id="Seg_8479" s="T947">wish-2SG-ACC</ta>
            <ta e="T457" id="Seg_8480" s="T948">fill-FUT-1SG</ta>
            <ta e="T458" id="Seg_8481" s="T457">bag.[NOM]</ta>
            <ta e="T459" id="Seg_8482" s="T458">be-PST2.[3SG]</ta>
            <ta e="T460" id="Seg_8483" s="T459">earth.[NOM]</ta>
            <ta e="T461" id="Seg_8484" s="T460">master-3SG.[NOM]</ta>
            <ta e="T462" id="Seg_8485" s="T461">boy.[NOM]</ta>
            <ta e="T463" id="Seg_8486" s="T462">stone.[NOM]</ta>
            <ta e="T464" id="Seg_8487" s="T463">heart-ACC</ta>
            <ta e="T465" id="Seg_8488" s="T464">stove.[NOM]</ta>
            <ta e="T466" id="Seg_8489" s="T465">inside-3SG-ABL</ta>
            <ta e="T467" id="Seg_8490" s="T466">get.out-CVB.SEQ</ta>
            <ta e="T468" id="Seg_8491" s="T467">take-PST1-3SG</ta>
            <ta e="T469" id="Seg_8492" s="T468">fire-3SG.[NOM]</ta>
            <ta e="T470" id="Seg_8493" s="T469">again</ta>
            <ta e="T471" id="Seg_8494" s="T470">flame-PST1-3SG</ta>
            <ta e="T472" id="Seg_8495" s="T471">bag.[NOM]</ta>
            <ta e="T473" id="Seg_8496" s="T472">say-PST1-3SG</ta>
            <ta e="T474" id="Seg_8497" s="T473">boy-DAT/LOC</ta>
            <ta e="T475" id="Seg_8498" s="T474">out</ta>
            <ta e="T476" id="Seg_8499" s="T475">go.out.[IMP.2SG]</ta>
            <ta e="T477" id="Seg_8500" s="T476">there</ta>
            <ta e="T478" id="Seg_8501" s="T477">sledge-PROPR</ta>
            <ta e="T479" id="Seg_8502" s="T478">reindeer.[NOM]</ta>
            <ta e="T480" id="Seg_8503" s="T479">stand-PRS.[3SG]</ta>
            <ta e="T481" id="Seg_8504" s="T480">boy.[NOM]</ta>
            <ta e="T482" id="Seg_8505" s="T481">out</ta>
            <ta e="T483" id="Seg_8506" s="T482">go.out-CVB.SEQ</ta>
            <ta e="T484" id="Seg_8507" s="T483">what.[NOM]</ta>
            <ta e="T485" id="Seg_8508" s="T484">NEG</ta>
            <ta e="T486" id="Seg_8509" s="T485">sledge-PROPR</ta>
            <ta e="T487" id="Seg_8510" s="T486">reindeer-3SG-ACC</ta>
            <ta e="T488" id="Seg_8511" s="T487">see-NEG.[3SG]</ta>
            <ta e="T489" id="Seg_8512" s="T488">where</ta>
            <ta e="T490" id="Seg_8513" s="T489">INDEF</ta>
            <ta e="T491" id="Seg_8514" s="T490">bell.[NOM]</ta>
            <ta e="T492" id="Seg_8515" s="T491">iron.item.at.reindeer.harness.[NOM]</ta>
            <ta e="T493" id="Seg_8516" s="T492">just</ta>
            <ta e="T494" id="Seg_8517" s="T493">sound-3SG.[NOM]</ta>
            <ta e="T495" id="Seg_8518" s="T494">be.heard-PRS.[3SG]</ta>
            <ta e="T496" id="Seg_8519" s="T495">nape-3SG-ACC</ta>
            <ta e="T497" id="Seg_8520" s="T496">to</ta>
            <ta e="T498" id="Seg_8521" s="T497">bag.[NOM]</ta>
            <ta e="T499" id="Seg_8522" s="T498">voice-3SG.[NOM]</ta>
            <ta e="T500" id="Seg_8523" s="T499">be.heard-EP-PST2.[3SG]</ta>
            <ta e="T501" id="Seg_8524" s="T500">sit.down.[IMP.2SG]</ta>
            <ta e="T502" id="Seg_8525" s="T501">sledge-2SG-DAT/LOC</ta>
            <ta e="T503" id="Seg_8526" s="T502">back-EP-2SG.[NOM]</ta>
            <ta e="T504" id="Seg_8527" s="T503">to</ta>
            <ta e="T505" id="Seg_8528" s="T504">look.around-EP-NEG.[IMP.2SG]</ta>
            <ta e="T506" id="Seg_8529" s="T505">way.[NOM]</ta>
            <ta e="T507" id="Seg_8530" s="T506">on.the.way</ta>
            <ta e="T508" id="Seg_8531" s="T507">stop-NEG.[IMP.2SG]</ta>
            <ta e="T509" id="Seg_8532" s="T508">reindeer-PL-2SG-ACC</ta>
            <ta e="T510" id="Seg_8533" s="T509">eat-EP-CAUS-EP-NEG.[IMP.2SG]</ta>
            <ta e="T511" id="Seg_8534" s="T510">boy.[NOM]</ta>
            <ta e="T512" id="Seg_8535" s="T511">hand-3SG-ACC</ta>
            <ta e="T513" id="Seg_8536" s="T512">stretch.out-PST1-3SG</ta>
            <ta e="T514" id="Seg_8537" s="T513">rein-ACC</ta>
            <ta e="T515" id="Seg_8538" s="T514">grab-PST1-3SG</ta>
            <ta e="T516" id="Seg_8539" s="T515">to.be.on.view-EP-NEG.PTCP</ta>
            <ta e="T517" id="Seg_8540" s="T516">sledge-DAT/LOC</ta>
            <ta e="T518" id="Seg_8541" s="T517">mount-PST1-3SG</ta>
            <ta e="T519" id="Seg_8542" s="T518">wind-PROPR.[NOM]</ta>
            <ta e="T520" id="Seg_8543" s="T519">snowstorm.[NOM]</ta>
            <ta e="T521" id="Seg_8544" s="T520">beat-PST1-3SG</ta>
            <ta e="T522" id="Seg_8545" s="T521">how.much-ACC</ta>
            <ta e="T523" id="Seg_8546" s="T522">go-PST2-3SG</ta>
            <ta e="T524" id="Seg_8547" s="T523">Q</ta>
            <ta e="T525" id="Seg_8548" s="T524">who.[NOM]</ta>
            <ta e="T526" id="Seg_8549" s="T525">know-FUT.[3SG]=Q</ta>
            <ta e="T527" id="Seg_8550" s="T526">sledge-PROPR.[NOM]</ta>
            <ta e="T528" id="Seg_8551" s="T527">reindeer-3PL.[NOM]</ta>
            <ta e="T529" id="Seg_8552" s="T528">self-3PL.[NOM]</ta>
            <ta e="T530" id="Seg_8553" s="T529">take.away-PRS-3PL</ta>
            <ta e="T531" id="Seg_8554" s="T530">boy.[NOM]</ta>
            <ta e="T532" id="Seg_8555" s="T531">freeze-NEG.[3SG]</ta>
            <ta e="T533" id="Seg_8556" s="T532">hand-3SG-ACC</ta>
            <ta e="T534" id="Seg_8557" s="T533">seat-3SG-GEN</ta>
            <ta e="T535" id="Seg_8558" s="T534">inside-3SG-DAT/LOC</ta>
            <ta e="T536" id="Seg_8559" s="T535">stick-PST1-3SG</ta>
            <ta e="T537" id="Seg_8560" s="T536">there</ta>
            <ta e="T538" id="Seg_8561" s="T537">warm.[NOM]</ta>
            <ta e="T539" id="Seg_8562" s="T538">iron.[NOM]</ta>
            <ta e="T540" id="Seg_8563" s="T539">wood.[NOM]</ta>
            <ta e="T541" id="Seg_8564" s="T540">lie-PRS.[3SG]</ta>
            <ta e="T542" id="Seg_8565" s="T541">be-PST2.[3SG]</ta>
            <ta e="T543" id="Seg_8566" s="T542">that-3SG-3SG.[NOM]</ta>
            <ta e="T544" id="Seg_8567" s="T543">become.warm-EP-CAUS-PRS.[3SG]</ta>
            <ta e="T545" id="Seg_8568" s="T544">river.[NOM]</ta>
            <ta e="T546" id="Seg_8569" s="T545">upper.part-3SG-DAT/LOC</ta>
            <ta e="T547" id="Seg_8570" s="T546">see-PST1-3SG</ta>
            <ta e="T548" id="Seg_8571" s="T547">sledge-DAT/LOC</ta>
            <ta e="T549" id="Seg_8572" s="T548">die-PTCP.PST</ta>
            <ta e="T550" id="Seg_8573" s="T549">woman.[NOM]</ta>
            <ta e="T551" id="Seg_8574" s="T550">lie-PRS.[3SG]</ta>
            <ta e="T552" id="Seg_8575" s="T551">strongly</ta>
            <ta e="T553" id="Seg_8576" s="T552">flurry-EP-PASS/REFL-CVB.SEQ</ta>
            <ta e="T554" id="Seg_8577" s="T553">after</ta>
            <ta e="T555" id="Seg_8578" s="T554">bear-NEG.CVB.SIM</ta>
            <ta e="T556" id="Seg_8579" s="T555">stop-PST1-3SG</ta>
            <ta e="T557" id="Seg_8580" s="T556">stop-PTCP.PRS-3SG-ACC</ta>
            <ta e="T558" id="Seg_8581" s="T557">with</ta>
            <ta e="T559" id="Seg_8582" s="T558">front.reindeer-3SG.[NOM]</ta>
            <ta e="T560" id="Seg_8583" s="T559">PFV</ta>
            <ta e="T561" id="Seg_8584" s="T560">jump-CVB.SEQ</ta>
            <ta e="T562" id="Seg_8585" s="T561">whereto</ta>
            <ta e="T563" id="Seg_8586" s="T562">go-PST2-3SG</ta>
            <ta e="T564" id="Seg_8587" s="T563">whereto</ta>
            <ta e="T565" id="Seg_8588" s="T564">come-PST2-3SG</ta>
            <ta e="T566" id="Seg_8589" s="T565">three</ta>
            <ta e="T567" id="Seg_8590" s="T566">black.reindeer.[NOM]</ta>
            <ta e="T568" id="Seg_8591" s="T567">reindeer-PROPR.[NOM]</ta>
            <ta e="T569" id="Seg_8592" s="T568">be-PST1-3SG</ta>
            <ta e="T570" id="Seg_8593" s="T569">be-PST2.[3SG]</ta>
            <ta e="T571" id="Seg_8594" s="T570">front.reindeer-3SG.[NOM]</ta>
            <ta e="T572" id="Seg_8595" s="T571">come.to.an.end-CVB.SEQ</ta>
            <ta e="T573" id="Seg_8596" s="T572">go-CVB.SEQ</ta>
            <ta e="T574" id="Seg_8597" s="T573">stay-PST2.[3SG]</ta>
            <ta e="T575" id="Seg_8598" s="T574">bag.[NOM]</ta>
            <ta e="T576" id="Seg_8599" s="T575">voice-3SG.[NOM]</ta>
            <ta e="T577" id="Seg_8600" s="T576">be.heard-EP-PST2.[3SG]</ta>
            <ta e="T578" id="Seg_8601" s="T577">2SG-DAT/LOC</ta>
            <ta e="T579" id="Seg_8602" s="T578">say-PST2-EP-1SG</ta>
            <ta e="T580" id="Seg_8603" s="T579">stop-NEG.[IMP.2SG]</ta>
            <ta e="T581" id="Seg_8604" s="T580">boy.[NOM]</ta>
            <ta e="T582" id="Seg_8605" s="T581">sledge-3SG-ABL</ta>
            <ta e="T583" id="Seg_8606" s="T582">red.[NOM]</ta>
            <ta e="T584" id="Seg_8607" s="T583">iron.[NOM]</ta>
            <ta e="T585" id="Seg_8608" s="T584">wood-ACC</ta>
            <ta e="T586" id="Seg_8609" s="T585">take-CVB.SEQ</ta>
            <ta e="T587" id="Seg_8610" s="T586">girl.[NOM]</ta>
            <ta e="T588" id="Seg_8611" s="T587">sledge-3SG-GEN</ta>
            <ta e="T589" id="Seg_8612" s="T588">neighbourhood-3SG-DAT/LOC</ta>
            <ta e="T590" id="Seg_8613" s="T589">into</ta>
            <ta e="T591" id="Seg_8614" s="T590">push-PST1-3SG</ta>
            <ta e="T592" id="Seg_8615" s="T591">snow.[NOM]</ta>
            <ta e="T593" id="Seg_8616" s="T592">immediately</ta>
            <ta e="T594" id="Seg_8617" s="T593">melt-CVB.SEQ</ta>
            <ta e="T595" id="Seg_8618" s="T594">stay-PST1-3SG</ta>
            <ta e="T596" id="Seg_8619" s="T595">freeze-CVB.SEQ</ta>
            <ta e="T597" id="Seg_8620" s="T596">die-PTCP.PST</ta>
            <ta e="T598" id="Seg_8621" s="T597">woman-3SG.[NOM]</ta>
            <ta e="T599" id="Seg_8622" s="T598">revive-PST1-3SG</ta>
            <ta e="T600" id="Seg_8623" s="T599">see-PST2-3SG</ta>
            <ta e="T601" id="Seg_8624" s="T600">very</ta>
            <ta e="T602" id="Seg_8625" s="T601">beautiful</ta>
            <ta e="T603" id="Seg_8626" s="T602">very</ta>
            <ta e="T604" id="Seg_8627" s="T603">girl.[NOM]</ta>
            <ta e="T605" id="Seg_8628" s="T604">be-PST2.[3SG]</ta>
            <ta e="T606" id="Seg_8629" s="T605">girl.[NOM]</ta>
            <ta e="T607" id="Seg_8630" s="T606">be.happy-CVB.SEQ</ta>
            <ta e="T608" id="Seg_8631" s="T607">misery-POSS</ta>
            <ta e="T609" id="Seg_8632" s="T608">NEG.[3SG]</ta>
            <ta e="T610" id="Seg_8633" s="T609">now</ta>
            <ta e="T611" id="Seg_8634" s="T610">self-1SG.[NOM]</ta>
            <ta e="T612" id="Seg_8635" s="T611">house-1SG-DAT/LOC</ta>
            <ta e="T613" id="Seg_8636" s="T612">reach-FUT-1SG</ta>
            <ta e="T614" id="Seg_8637" s="T613">come.along-EP-NEG-2SG</ta>
            <ta e="T615" id="Seg_8638" s="T614">Q</ta>
            <ta e="T616" id="Seg_8639" s="T615">eat-EP-CAUS-FUT-1SG</ta>
            <ta e="T617" id="Seg_8640" s="T616">warm.[NOM]</ta>
            <ta e="T618" id="Seg_8641" s="T617">bedding-DAT/LOC</ta>
            <ta e="T619" id="Seg_8642" s="T618">spread-FUT-1SG</ta>
            <ta e="T620" id="Seg_8643" s="T619">beg-PST2.[3SG]</ta>
            <ta e="T621" id="Seg_8644" s="T620">girl.[NOM]</ta>
            <ta e="T622" id="Seg_8645" s="T621">distant.[NOM]</ta>
            <ta e="T623" id="Seg_8646" s="T622">Q</ta>
            <ta e="T624" id="Seg_8647" s="T623">house-2SG.[NOM]</ta>
            <ta e="T625" id="Seg_8648" s="T624">water.[NOM]</ta>
            <ta e="T626" id="Seg_8649" s="T625">inside-3SG-DAT/LOC</ta>
            <ta e="T627" id="Seg_8650" s="T626">boy.[NOM]</ta>
            <ta e="T628" id="Seg_8651" s="T627">many</ta>
            <ta e="T629" id="Seg_8652" s="T628">miracle-ACC</ta>
            <ta e="T630" id="Seg_8653" s="T629">see-PST2-3SG</ta>
            <ta e="T631" id="Seg_8654" s="T630">that.[NOM]</ta>
            <ta e="T632" id="Seg_8655" s="T631">because.of</ta>
            <ta e="T633" id="Seg_8656" s="T632">girl.[NOM]</ta>
            <ta e="T634" id="Seg_8657" s="T633">story-3SG-ACC</ta>
            <ta e="T635" id="Seg_8658" s="T634">wonder-NEG-PST1-3SG</ta>
            <ta e="T636" id="Seg_8659" s="T635">say-CVB.SIM</ta>
            <ta e="T637" id="Seg_8660" s="T636">be-PST2.[3SG]</ta>
            <ta e="T638" id="Seg_8661" s="T637">1SG.[NOM]</ta>
            <ta e="T639" id="Seg_8662" s="T638">older.sister-1SG-ACC</ta>
            <ta e="T640" id="Seg_8663" s="T639">find-PTCP.FUT-1SG-ACC</ta>
            <ta e="T641" id="Seg_8664" s="T640">need.to</ta>
            <ta e="T642" id="Seg_8665" s="T641">later</ta>
            <ta e="T643" id="Seg_8666" s="T642">come-FUT-1SG</ta>
            <ta e="T644" id="Seg_8667" s="T643">EVID</ta>
            <ta e="T645" id="Seg_8668" s="T644">wait.[IMP.2SG]</ta>
            <ta e="T646" id="Seg_8669" s="T645">boy.[NOM]</ta>
            <ta e="T647" id="Seg_8670" s="T646">how.much-ACC-how.much-ACC</ta>
            <ta e="T648" id="Seg_8671" s="T647">go-PST2-3SG</ta>
            <ta e="T649" id="Seg_8672" s="T648">who.[NOM]</ta>
            <ta e="T650" id="Seg_8673" s="T649">NEG</ta>
            <ta e="T651" id="Seg_8674" s="T650">know-NEG.[3SG]</ta>
            <ta e="T652" id="Seg_8675" s="T651">suddenly</ta>
            <ta e="T653" id="Seg_8676" s="T652">thawed.patch.[NOM]</ta>
            <ta e="T654" id="Seg_8677" s="T653">become-PST2.[3SG]</ta>
            <ta e="T655" id="Seg_8678" s="T654">reindeer.moss-PROPR.[NOM]</ta>
            <ta e="T656" id="Seg_8679" s="T655">very</ta>
            <ta e="T657" id="Seg_8680" s="T656">reindeer-PL-3SG.[NOM]</ta>
            <ta e="T658" id="Seg_8681" s="T657">hungry-VBZ-PST1-3PL</ta>
            <ta e="T659" id="Seg_8682" s="T658">eat-EP-CAUS-PTCP.FUT-DAT/LOC</ta>
            <ta e="T660" id="Seg_8683" s="T659">Q</ta>
            <ta e="T661" id="Seg_8684" s="T660">say-CVB.SIM</ta>
            <ta e="T662" id="Seg_8685" s="T661">think-PST2.[3SG]</ta>
            <ta e="T663" id="Seg_8686" s="T662">earth.[NOM]</ta>
            <ta e="T664" id="Seg_8687" s="T663">master-3SG-GEN</ta>
            <ta e="T665" id="Seg_8688" s="T664">word-3SG-ACC</ta>
            <ta e="T666" id="Seg_8689" s="T665">remember-CVB.SIM-remember-CVB.SIM</ta>
            <ta e="T667" id="Seg_8690" s="T666">reindeer-PL-3SG-ACC</ta>
            <ta e="T668" id="Seg_8691" s="T667">stop-CAUS-NEG-PST1-3SG</ta>
            <ta e="T669" id="Seg_8692" s="T668">then</ta>
            <ta e="T670" id="Seg_8693" s="T669">say-CVB.SIM</ta>
            <ta e="T671" id="Seg_8694" s="T670">think-PST1-3SG</ta>
            <ta e="T672" id="Seg_8695" s="T671">very</ta>
            <ta e="T673" id="Seg_8696" s="T672">hungry-VBZ-PST2-3PL</ta>
            <ta e="T674" id="Seg_8697" s="T673">stop-EP-CAUS-PTCP.FUT-DAT/LOC</ta>
            <ta e="T675" id="Seg_8698" s="T674">stop-EP-CAUS-PTCP.PRS-3SG-ACC</ta>
            <ta e="T676" id="Seg_8699" s="T675">with</ta>
            <ta e="T677" id="Seg_8700" s="T676">middle-ADJZ</ta>
            <ta e="T678" id="Seg_8701" s="T677">harness-PTCP.PRS</ta>
            <ta e="T679" id="Seg_8702" s="T678">reindeer-3SG.[NOM]</ta>
            <ta e="T680" id="Seg_8703" s="T679">disappear-NMNZ.[NOM]</ta>
            <ta e="T681" id="Seg_8704" s="T680">just</ta>
            <ta e="T682" id="Seg_8705" s="T681">make-CVB.SEQ</ta>
            <ta e="T683" id="Seg_8706" s="T682">stay-PST2.[3SG]</ta>
            <ta e="T684" id="Seg_8707" s="T683">whereto</ta>
            <ta e="T685" id="Seg_8708" s="T684">go-PST2-3SG</ta>
            <ta e="T686" id="Seg_8709" s="T685">whereto</ta>
            <ta e="T687" id="Seg_8710" s="T686">run-PST2-3SG</ta>
            <ta e="T688" id="Seg_8711" s="T687">again</ta>
            <ta e="T689" id="Seg_8712" s="T688">voice.[NOM]</ta>
            <ta e="T690" id="Seg_8713" s="T689">be.heard-EP-PST2.[3SG]</ta>
            <ta e="T691" id="Seg_8714" s="T690">1SG.[NOM]</ta>
            <ta e="T692" id="Seg_8715" s="T691">2SG-DAT/LOC</ta>
            <ta e="T693" id="Seg_8716" s="T692">say-PST2-EP-1SG</ta>
            <ta e="T694" id="Seg_8717" s="T693">eat-EP-CAUS-EP-NEG.[IMP.2SG]</ta>
            <ta e="T695" id="Seg_8718" s="T694">reindeer-PL-2SG-ACC</ta>
            <ta e="T696" id="Seg_8719" s="T695">boy.[NOM]</ta>
            <ta e="T697" id="Seg_8720" s="T696">how.much-ACC</ta>
            <ta e="T698" id="Seg_8721" s="T697">go-PST2-3SG</ta>
            <ta e="T699" id="Seg_8722" s="T698">Q</ta>
            <ta e="T700" id="Seg_8723" s="T699">this</ta>
            <ta e="T701" id="Seg_8724" s="T700">stone.[NOM]</ta>
            <ta e="T702" id="Seg_8725" s="T701">mountain-3SG-DAT/LOC</ta>
            <ta e="T703" id="Seg_8726" s="T702">reach-PST1-3SG</ta>
            <ta e="T704" id="Seg_8727" s="T703">two</ta>
            <ta e="T705" id="Seg_8728" s="T704">direction.[NOM]</ta>
            <ta e="T706" id="Seg_8729" s="T705">tear-PTCP.HAB-3SG-DAT/LOC</ta>
            <ta e="T707" id="Seg_8730" s="T706">only</ta>
            <ta e="T708" id="Seg_8731" s="T707">self-3SG-GEN</ta>
            <ta e="T709" id="Seg_8732" s="T708">place-3SG-DAT/LOC</ta>
            <ta e="T710" id="Seg_8733" s="T709">reach-PST2.[3SG]</ta>
            <ta e="T711" id="Seg_8734" s="T710">be-PST2.[3SG]</ta>
            <ta e="T712" id="Seg_8735" s="T711">freeze-CVB.SEQ</ta>
            <ta e="T713" id="Seg_8736" s="T712">die-PTCP.PST</ta>
            <ta e="T714" id="Seg_8737" s="T713">reindeer-PL.[NOM]</ta>
            <ta e="T715" id="Seg_8738" s="T714">horn-3PL.[NOM]</ta>
            <ta e="T716" id="Seg_8739" s="T715">hardly</ta>
            <ta e="T717" id="Seg_8740" s="T716">snow.[NOM]</ta>
            <ta e="T718" id="Seg_8741" s="T717">inside-3SG-ABL</ta>
            <ta e="T719" id="Seg_8742" s="T718">pull.out-PRS-3PL</ta>
            <ta e="T720" id="Seg_8743" s="T719">snow-ACC</ta>
            <ta e="T721" id="Seg_8744" s="T720">shovel-CVB.SEQ</ta>
            <ta e="T722" id="Seg_8745" s="T721">older.sister-3SG-GEN</ta>
            <ta e="T723" id="Seg_8746" s="T722">front.reindeer-3SG-ACC</ta>
            <ta e="T724" id="Seg_8747" s="T723">find-PST1-3SG</ta>
            <ta e="T725" id="Seg_8748" s="T724">warm.[NOM]</ta>
            <ta e="T726" id="Seg_8749" s="T725">iron.[NOM]</ta>
            <ta e="T727" id="Seg_8750" s="T726">wood-3SG-INSTR</ta>
            <ta e="T728" id="Seg_8751" s="T727">graze-PST1-3SG</ta>
            <ta e="T729" id="Seg_8752" s="T728">reindeer-3SG.[NOM]</ta>
            <ta e="T730" id="Seg_8753" s="T729">revive-CVB.SEQ</ta>
            <ta e="T731" id="Seg_8754" s="T730">after</ta>
            <ta e="T732" id="Seg_8755" s="T731">stand.up-PST1-3SG</ta>
            <ta e="T733" id="Seg_8756" s="T732">then</ta>
            <ta e="T734" id="Seg_8757" s="T733">wood-ACC</ta>
            <ta e="T735" id="Seg_8758" s="T734">take-PST1-3SG</ta>
            <ta e="T736" id="Seg_8759" s="T735">snow-DAT/LOC</ta>
            <ta e="T737" id="Seg_8760" s="T736">push-PST1-3SG</ta>
            <ta e="T738" id="Seg_8761" s="T737">freeze-CVB.SEQ</ta>
            <ta e="T739" id="Seg_8762" s="T738">die-PTCP.PST</ta>
            <ta e="T740" id="Seg_8763" s="T739">reindeer-PL.[NOM]</ta>
            <ta e="T741" id="Seg_8764" s="T740">immediately</ta>
            <ta e="T742" id="Seg_8765" s="T741">every-3PL.[NOM]</ta>
            <ta e="T743" id="Seg_8766" s="T742">revive-PST1-3PL</ta>
            <ta e="T744" id="Seg_8767" s="T743">intact</ta>
            <ta e="T745" id="Seg_8768" s="T744">herd.[NOM]</ta>
            <ta e="T746" id="Seg_8769" s="T745">become-CVB.SEQ</ta>
            <ta e="T747" id="Seg_8770" s="T746">eat-CVB.SIM</ta>
            <ta e="T748" id="Seg_8771" s="T747">go-PRS-3PL</ta>
            <ta e="T749" id="Seg_8772" s="T748">new</ta>
            <ta e="T750" id="Seg_8773" s="T749">earth-EP-INSTR</ta>
            <ta e="T751" id="Seg_8774" s="T750">look.around-PST2-3SG</ta>
            <ta e="T752" id="Seg_8775" s="T751">older.sister-3SG.[NOM]</ta>
            <ta e="T753" id="Seg_8776" s="T752">stand-PRS.[3SG]</ta>
            <ta e="T754" id="Seg_8777" s="T753">boy.[NOM]</ta>
            <ta e="T755" id="Seg_8778" s="T754">older.sister-3SG-ACC</ta>
            <ta e="T756" id="Seg_8779" s="T755">see-CVB.SEQ</ta>
            <ta e="T757" id="Seg_8780" s="T756">very</ta>
            <ta e="T758" id="Seg_8781" s="T757">be.happy-PST1-3SG</ta>
            <ta e="T759" id="Seg_8782" s="T758">at.first</ta>
            <ta e="T760" id="Seg_8783" s="T759">2SG.[NOM]</ta>
            <ta e="T761" id="Seg_8784" s="T760">say-PST1-3SG</ta>
            <ta e="T762" id="Seg_8785" s="T761">older.sister-3SG.[NOM]</ta>
            <ta e="T763" id="Seg_8786" s="T762">long</ta>
            <ta e="T764" id="Seg_8787" s="T763">very-ADVZ</ta>
            <ta e="T765" id="Seg_8788" s="T764">sleep-PST2-EP-2SG</ta>
            <ta e="T766" id="Seg_8789" s="T765">then</ta>
            <ta e="T767" id="Seg_8790" s="T766">1SG.[NOM]</ta>
            <ta e="T768" id="Seg_8791" s="T767">spring.[NOM]</ta>
            <ta e="T769" id="Seg_8792" s="T768">become-PST2.[3SG]</ta>
            <ta e="T770" id="Seg_8793" s="T769">be-PST2.[3SG]</ta>
            <ta e="T771" id="Seg_8794" s="T770">winter-ACC</ta>
            <ta e="T772" id="Seg_8795" s="T771">whole</ta>
            <ta e="T773" id="Seg_8796" s="T772">sleep-PST2-1SG</ta>
            <ta e="T774" id="Seg_8797" s="T773">be-PST2.[3SG]</ta>
            <ta e="T775" id="Seg_8798" s="T774">boy.[NOM]</ta>
            <ta e="T776" id="Seg_8799" s="T775">older.sister-3SG-ACC</ta>
            <ta e="T777" id="Seg_8800" s="T776">house-3SG-DAT/LOC</ta>
            <ta e="T778" id="Seg_8801" s="T777">bring-PST2-3SG</ta>
            <ta e="T779" id="Seg_8802" s="T778">herd-3SG-ACC</ta>
            <ta e="T780" id="Seg_8803" s="T779">drive-CVB.SEQ</ta>
            <ta e="T781" id="Seg_8804" s="T780">bring-PST1-3SG</ta>
            <ta e="T782" id="Seg_8805" s="T781">how.much</ta>
            <ta e="T783" id="Seg_8806" s="T782">NEG</ta>
            <ta e="T784" id="Seg_8807" s="T783">be-NEG.CVB.SIM</ta>
            <ta e="T785" id="Seg_8808" s="T784">this</ta>
            <ta e="T786" id="Seg_8809" s="T785">boy.[NOM]</ta>
            <ta e="T787" id="Seg_8810" s="T786">prepare-PST1-3SG</ta>
            <ta e="T788" id="Seg_8811" s="T787">whereto</ta>
            <ta e="T789" id="Seg_8812" s="T788">go-PRS-2SG</ta>
            <ta e="T790" id="Seg_8813" s="T789">ask-PST2.[3SG]</ta>
            <ta e="T791" id="Seg_8814" s="T790">older.sister-3SG.[NOM]</ta>
            <ta e="T792" id="Seg_8815" s="T791">soon</ta>
            <ta e="T793" id="Seg_8816" s="T792">come-FUT-1SG</ta>
            <ta e="T794" id="Seg_8817" s="T793">fear-EP-NEG.[IMP.2SG]</ta>
            <ta e="T795" id="Seg_8818" s="T794">boy.[NOM]</ta>
            <ta e="T796" id="Seg_8819" s="T795">this</ta>
            <ta e="T797" id="Seg_8820" s="T796">girl-ACC</ta>
            <ta e="T798" id="Seg_8821" s="T797">search-CVB.SIM</ta>
            <ta e="T799" id="Seg_8822" s="T798">go-CAP.[3SG]</ta>
            <ta e="T800" id="Seg_8823" s="T799">water.[NOM]</ta>
            <ta e="T801" id="Seg_8824" s="T800">inside-3SG-DAT/LOC</ta>
            <ta e="T802" id="Seg_8825" s="T801">house-PROPR-ACC</ta>
            <ta e="T803" id="Seg_8826" s="T802">after</ta>
            <ta e="T804" id="Seg_8827" s="T803">go-CVB.SEQ</ta>
            <ta e="T805" id="Seg_8828" s="T804">gloomy.[NOM]</ta>
            <ta e="T806" id="Seg_8829" s="T805">very</ta>
            <ta e="T807" id="Seg_8830" s="T806">snowstorm.[NOM]</ta>
            <ta e="T808" id="Seg_8831" s="T807">become-PST2.[3SG]</ta>
            <ta e="T809" id="Seg_8832" s="T808">earth-3SG-INSTR</ta>
            <ta e="T810" id="Seg_8833" s="T809">EMPH</ta>
            <ta e="T811" id="Seg_8834" s="T810">go-PTCP.PRS-3SG-ACC</ta>
            <ta e="T812" id="Seg_8835" s="T811">know-NEG.[3SG]</ta>
            <ta e="T813" id="Seg_8836" s="T812">ice-3SG-INSTR</ta>
            <ta e="T814" id="Seg_8837" s="T813">EMPH</ta>
            <ta e="T815" id="Seg_8838" s="T814">go-PTCP.PRS-3SG-ACC</ta>
            <ta e="T816" id="Seg_8839" s="T815">know-NEG.[3SG]</ta>
            <ta e="T817" id="Seg_8840" s="T816">think.out-FREQ-CVB.SEQ</ta>
            <ta e="T818" id="Seg_8841" s="T817">see-PST2-3SG</ta>
            <ta e="T819" id="Seg_8842" s="T818">river.[NOM]</ta>
            <ta e="T820" id="Seg_8843" s="T819">ice.floe-3SG.[NOM]</ta>
            <ta e="T821" id="Seg_8844" s="T820">be-PST2.[3SG]</ta>
            <ta e="T822" id="Seg_8845" s="T821">spring-ADJZ.[NOM]</ta>
            <ta e="T823" id="Seg_8846" s="T822">ice.[NOM]</ta>
            <ta e="T824" id="Seg_8847" s="T823">get.caught-CVB.SEQ</ta>
            <ta e="T825" id="Seg_8848" s="T824">boy.[NOM]</ta>
            <ta e="T826" id="Seg_8849" s="T825">water-DAT/LOC</ta>
            <ta e="T827" id="Seg_8850" s="T826">fall-PST2.[3SG]</ta>
            <ta e="T828" id="Seg_8851" s="T827">fall-CVB.SEQ</ta>
            <ta e="T829" id="Seg_8852" s="T828">pike.[NOM]</ta>
            <ta e="T830" id="Seg_8853" s="T829">become-PST2.[3SG]</ta>
            <ta e="T831" id="Seg_8854" s="T830">water.[NOM]</ta>
            <ta e="T832" id="Seg_8855" s="T831">inside-3SG-INSTR</ta>
            <ta e="T833" id="Seg_8856" s="T832">flow-CVB.SIM</ta>
            <ta e="T834" id="Seg_8857" s="T833">go-CVB.SEQ</ta>
            <ta e="T835" id="Seg_8858" s="T834">net-DAT/LOC</ta>
            <ta e="T836" id="Seg_8859" s="T835">get.into-PST2.[3SG]</ta>
            <ta e="T837" id="Seg_8860" s="T836">broad.[NOM]</ta>
            <ta e="T838" id="Seg_8861" s="T837">net-DAT/LOC</ta>
            <ta e="T839" id="Seg_8862" s="T838">pike-PL.[NOM]</ta>
            <ta e="T840" id="Seg_8863" s="T839">only</ta>
            <ta e="T841" id="Seg_8864" s="T840">how.much</ta>
            <ta e="T842" id="Seg_8865" s="T841">NEG</ta>
            <ta e="T843" id="Seg_8866" s="T842">be-NEG.CVB.SIM</ta>
            <ta e="T844" id="Seg_8867" s="T843">girl.[NOM]</ta>
            <ta e="T845" id="Seg_8868" s="T844">come-PST1-3SG</ta>
            <ta e="T846" id="Seg_8869" s="T845">this</ta>
            <ta e="T847" id="Seg_8870" s="T846">girl-3SG.[NOM]</ta>
            <ta e="T848" id="Seg_8871" s="T847">revive-PASS-PTCP.HAB-3SG.[3SG]</ta>
            <ta e="T849" id="Seg_8872" s="T848">pike.[NOM]</ta>
            <ta e="T850" id="Seg_8873" s="T849">boy-ACC</ta>
            <ta e="T851" id="Seg_8874" s="T850">front.reindeer.[NOM]</ta>
            <ta e="T852" id="Seg_8875" s="T851">make-CVB.SEQ</ta>
            <ta e="T853" id="Seg_8876" s="T852">after</ta>
            <ta e="T854" id="Seg_8877" s="T853">harness-PST1-3SG</ta>
            <ta e="T855" id="Seg_8878" s="T854">house-3SG-DAT/LOC</ta>
            <ta e="T856" id="Seg_8879" s="T855">go-CAP.[3SG]</ta>
            <ta e="T857" id="Seg_8880" s="T856">front.reindeer.[NOM]</ta>
            <ta e="T858" id="Seg_8881" s="T857">hear-EP-NEG.[3SG]</ta>
            <ta e="T859" id="Seg_8882" s="T858">shore.[NOM]</ta>
            <ta e="T860" id="Seg_8883" s="T859">just</ta>
            <ta e="T861" id="Seg_8884" s="T860">to</ta>
            <ta e="T862" id="Seg_8885" s="T861">%%-PRS.[3SG]</ta>
            <ta e="T863" id="Seg_8886" s="T862">girl.[NOM]</ta>
            <ta e="T864" id="Seg_8887" s="T863">that-ACC</ta>
            <ta e="T865" id="Seg_8888" s="T864">lead-CVB.SIM</ta>
            <ta e="T866" id="Seg_8889" s="T865">try-PRS.[3SG]</ta>
            <ta e="T867" id="Seg_8890" s="T866">front.reindeer-3SG.[NOM]</ta>
            <ta e="T868" id="Seg_8891" s="T867">cut-CVB.SIM</ta>
            <ta e="T869" id="Seg_8892" s="T868">make.an.effort-CVB.SEQ</ta>
            <ta e="T870" id="Seg_8893" s="T869">shore-DAT/LOC</ta>
            <ta e="T871" id="Seg_8894" s="T870">go.out-PST1-3SG</ta>
            <ta e="T872" id="Seg_8895" s="T871">go.out-CVB.ANT</ta>
            <ta e="T873" id="Seg_8896" s="T872">boy-pike.[NOM]</ta>
            <ta e="T874" id="Seg_8897" s="T873">real.[NOM]</ta>
            <ta e="T875" id="Seg_8898" s="T874">boy.[NOM]</ta>
            <ta e="T876" id="Seg_8899" s="T875">become-PST2.[3SG]</ta>
            <ta e="T877" id="Seg_8900" s="T876">pike-PL.[NOM]</ta>
            <ta e="T878" id="Seg_8901" s="T877">reindeer.[NOM]</ta>
            <ta e="T879" id="Seg_8902" s="T878">become-PST1-3PL</ta>
            <ta e="T880" id="Seg_8903" s="T879">girl-PROPR.[NOM]</ta>
            <ta e="T881" id="Seg_8904" s="T880">boy.[NOM]</ta>
            <ta e="T882" id="Seg_8905" s="T881">realize-EP-RECP/COLL-PST1-3PL</ta>
            <ta e="T883" id="Seg_8906" s="T882">girl.[NOM]</ta>
            <ta e="T884" id="Seg_8907" s="T883">ask-PST1-3SG</ta>
            <ta e="T885" id="Seg_8908" s="T884">older.sister-2SG-ACC</ta>
            <ta e="T886" id="Seg_8909" s="T885">find-PST1-2SG</ta>
            <ta e="T887" id="Seg_8910" s="T886">how</ta>
            <ta e="T888" id="Seg_8911" s="T887">live-PRS-2SG</ta>
            <ta e="T889" id="Seg_8912" s="T888">come.along.[IMP.2SG]</ta>
            <ta e="T890" id="Seg_8913" s="T889">1SG-ACC</ta>
            <ta e="T891" id="Seg_8914" s="T890">with</ta>
            <ta e="T892" id="Seg_8915" s="T891">say-PST1-3SG</ta>
            <ta e="T893" id="Seg_8916" s="T892">boy.[NOM]</ta>
            <ta e="T894" id="Seg_8917" s="T893">older.sister-EP-1SG.[NOM]</ta>
            <ta e="T895" id="Seg_8918" s="T894">2SG-ACC</ta>
            <ta e="T896" id="Seg_8919" s="T895">wait-PRS.[3SG]</ta>
            <ta e="T897" id="Seg_8920" s="T896">know-RECP/COLL-EP-RECP/COLL-EP-IMP.2PL</ta>
            <ta e="T898" id="Seg_8921" s="T897">together</ta>
            <ta e="T899" id="Seg_8922" s="T898">live-FUT-1PL</ta>
            <ta e="T900" id="Seg_8923" s="T899">herd-1PL-ACC</ta>
            <ta e="T901" id="Seg_8924" s="T900">connect-FUT-1PL</ta>
            <ta e="T902" id="Seg_8925" s="T901">boy-ACC</ta>
            <ta e="T903" id="Seg_8926" s="T902">with</ta>
            <ta e="T904" id="Seg_8927" s="T903">girl.[NOM]</ta>
            <ta e="T905" id="Seg_8928" s="T904">pike-PL-ACC</ta>
            <ta e="T906" id="Seg_8929" s="T905">water.[NOM]</ta>
            <ta e="T907" id="Seg_8930" s="T906">inside-3SG-ABL</ta>
            <ta e="T908" id="Seg_8931" s="T907">get.out-PST1-3PL</ta>
            <ta e="T909" id="Seg_8932" s="T908">that-3SG-3PL.[NOM]</ta>
            <ta e="T910" id="Seg_8933" s="T909">spotted.[NOM]</ta>
            <ta e="T911" id="Seg_8934" s="T910">reindeer.[NOM]</ta>
            <ta e="T912" id="Seg_8935" s="T911">become-PST1-3PL</ta>
            <ta e="T913" id="Seg_8936" s="T912">boy.[NOM]</ta>
            <ta e="T914" id="Seg_8937" s="T913">older.sister-3SG.[NOM]</ta>
            <ta e="T915" id="Seg_8938" s="T914">be.happy-CVB.SEQ</ta>
            <ta e="T916" id="Seg_8939" s="T915">meet-PST1-3SG</ta>
            <ta e="T917" id="Seg_8940" s="T916">boy.[NOM]</ta>
            <ta e="T918" id="Seg_8941" s="T917">three</ta>
            <ta e="T919" id="Seg_8942" s="T918">day-ACC</ta>
            <ta e="T920" id="Seg_8943" s="T919">sleep-PST1-3SG</ta>
            <ta e="T921" id="Seg_8944" s="T920">wake.up-CVB.SEQ</ta>
            <ta e="T922" id="Seg_8945" s="T921">see-PST2-3SG</ta>
            <ta e="T923" id="Seg_8946" s="T922">older.sister-3SG.[NOM]</ta>
            <ta e="T924" id="Seg_8947" s="T923">girl-ACC</ta>
            <ta e="T925" id="Seg_8948" s="T924">with</ta>
            <ta e="T926" id="Seg_8949" s="T925">chat-CVB.SIM-chat-CVB.SIM</ta>
            <ta e="T927" id="Seg_8950" s="T926">laugh-RECP/COLL-CVB.SIM</ta>
            <ta e="T928" id="Seg_8951" s="T927">sit-PRS-3PL</ta>
            <ta e="T929" id="Seg_8952" s="T928">tea.[NOM]</ta>
            <ta e="T930" id="Seg_8953" s="T929">drink-PRS-3PL</ta>
            <ta e="T931" id="Seg_8954" s="T930">boil-PTCP.PST</ta>
            <ta e="T932" id="Seg_8955" s="T931">meat-ACC</ta>
            <ta e="T933" id="Seg_8956" s="T932">eat-PRS-3PL</ta>
            <ta e="T934" id="Seg_8957" s="T933">boy.[NOM]</ta>
            <ta e="T935" id="Seg_8958" s="T934">neighbourhood-3PL-DAT/LOC</ta>
            <ta e="T936" id="Seg_8959" s="T935">sit.down-PST1-3SG</ta>
            <ta e="T937" id="Seg_8960" s="T936">intact</ta>
            <ta e="T938" id="Seg_8961" s="T937">reindeer-ACC</ta>
            <ta e="T939" id="Seg_8962" s="T938">eat-PST1-3SG</ta>
            <ta e="T940" id="Seg_8963" s="T939">boy.[NOM]</ta>
            <ta e="T941" id="Seg_8964" s="T940">girl-ACC</ta>
            <ta e="T942" id="Seg_8965" s="T941">woman.[NOM]</ta>
            <ta e="T943" id="Seg_8966" s="T942">make-PST2.[3SG]</ta>
            <ta e="T944" id="Seg_8967" s="T943">happy.[NOM]</ta>
            <ta e="T945" id="Seg_8968" s="T944">very-ADVZ</ta>
            <ta e="T946" id="Seg_8969" s="T945">live-PST2-3PL</ta>
         </annotation>
         <annotation name="gg" tierref="gg">
            <ta e="T2" id="Seg_8970" s="T1">zuhören-IMP.2PL</ta>
            <ta e="T3" id="Seg_8971" s="T2">Dolgane-PL.[NOM]</ta>
            <ta e="T4" id="Seg_8972" s="T3">Märchen-3PL-ACC</ta>
            <ta e="T5" id="Seg_8973" s="T4">ältere.Schwester-PROPR.[NOM]</ta>
            <ta e="T6" id="Seg_8974" s="T5">Junge.[NOM]</ta>
            <ta e="T7" id="Seg_8975" s="T6">sagen-CVB.SEQ</ta>
            <ta e="T8" id="Seg_8976" s="T7">Name-ADJZ.[NOM]</ta>
            <ta e="T9" id="Seg_8977" s="T8">Junge.[NOM]</ta>
            <ta e="T10" id="Seg_8978" s="T9">ältere.Schwester-3SG-ACC</ta>
            <ta e="T11" id="Seg_8979" s="T10">mit</ta>
            <ta e="T12" id="Seg_8980" s="T11">zu.zweit</ta>
            <ta e="T13" id="Seg_8981" s="T12">leben-PST2-3PL</ta>
            <ta e="T14" id="Seg_8982" s="T13">ältere.Schwester-3SG.[NOM]</ta>
            <ta e="T15" id="Seg_8983" s="T14">Arbeiter-PROPR.[NOM]</ta>
            <ta e="T16" id="Seg_8984" s="T15">Meister.[NOM]</ta>
            <ta e="T17" id="Seg_8985" s="T16">sehr</ta>
            <ta e="T18" id="Seg_8986" s="T17">Schlitten-PL-3SG.[NOM]</ta>
            <ta e="T19" id="Seg_8987" s="T18">scheckig</ta>
            <ta e="T20" id="Seg_8988" s="T19">Rentier.[NOM]</ta>
            <ta e="T21" id="Seg_8989" s="T20">Fell-PL-EP-INSTR</ta>
            <ta e="T22" id="Seg_8990" s="T21">Deckel-PROPR-3PL</ta>
            <ta e="T23" id="Seg_8991" s="T22">Stange.[NOM]</ta>
            <ta e="T24" id="Seg_8992" s="T23">Zelt-ACC</ta>
            <ta e="T25" id="Seg_8993" s="T24">selbst-3SG.[NOM]</ta>
            <ta e="T26" id="Seg_8994" s="T25">bauen-MED-HAB.[3SG]</ta>
            <ta e="T27" id="Seg_8995" s="T26">Rentier-3PL-ACC</ta>
            <ta e="T28" id="Seg_8996" s="T27">EMPH</ta>
            <ta e="T29" id="Seg_8997" s="T28">im.Sommer</ta>
            <ta e="T30" id="Seg_8998" s="T29">hüten-HAB.[3SG]</ta>
            <ta e="T31" id="Seg_8999" s="T30">Junge.[NOM]</ta>
            <ta e="T32" id="Seg_9000" s="T31">jüngerer.Bruder-EP-3SG.[NOM]</ta>
            <ta e="T33" id="Seg_9001" s="T32">Herbst-2SG-ABL</ta>
            <ta e="T34" id="Seg_9002" s="T33">schlafen-PST2.[3SG]</ta>
            <ta e="T35" id="Seg_9003" s="T34">Sonne.[NOM]</ta>
            <ta e="T36" id="Seg_9004" s="T35">hinausgehen-PTCP.FUT.[3SG]-DAT/LOC</ta>
            <ta e="T37" id="Seg_9005" s="T36">bis.zu</ta>
            <ta e="T38" id="Seg_9006" s="T37">Herde-3PL.[NOM]</ta>
            <ta e="T39" id="Seg_9007" s="T38">sich.wegbewegen-PST2.[3SG]</ta>
            <ta e="T40" id="Seg_9008" s="T39">Weide-3PL.[NOM]</ta>
            <ta e="T41" id="Seg_9009" s="T40">sich.verbreitern-PST2.[3SG]</ta>
            <ta e="T42" id="Seg_9010" s="T41">Mädchen.[NOM]</ta>
            <ta e="T43" id="Seg_9011" s="T42">Weide.[NOM]</ta>
            <ta e="T44" id="Seg_9012" s="T43">ändern-CAP.[3SG]</ta>
            <ta e="T45" id="Seg_9013" s="T44">Herde-3SG-ACC</ta>
            <ta e="T46" id="Seg_9014" s="T45">bringen-PST1-3SG</ta>
            <ta e="T47" id="Seg_9015" s="T46">Rentier.[NOM]</ta>
            <ta e="T48" id="Seg_9016" s="T47">ergreifen-PST1-3SG</ta>
            <ta e="T49" id="Seg_9017" s="T48">einspannen-PST1-3SG</ta>
            <ta e="T50" id="Seg_9018" s="T49">jüngerer.Bruder-EP-3SG-ACC</ta>
            <ta e="T51" id="Seg_9019" s="T50">aufwachen-CAUS-CVB.SIM</ta>
            <ta e="T52" id="Seg_9020" s="T51">versuchen-PRS.[3SG]</ta>
            <ta e="T53" id="Seg_9021" s="T52">völlig</ta>
            <ta e="T54" id="Seg_9022" s="T53">aufwachen-EP-NEG.[3SG]</ta>
            <ta e="T55" id="Seg_9023" s="T54">Stange.[NOM]</ta>
            <ta e="T56" id="Seg_9024" s="T55">Zelt-3SG-ACC</ta>
            <ta e="T57" id="Seg_9025" s="T56">abziehen-CVB.SEQ</ta>
            <ta e="T58" id="Seg_9026" s="T57">nachdem</ta>
            <ta e="T59" id="Seg_9027" s="T58">jüngerer.Bruder-EP-3SG-GEN</ta>
            <ta e="T60" id="Seg_9028" s="T59">Polarfuchs.[NOM]</ta>
            <ta e="T61" id="Seg_9029" s="T60">Bettdecke-3SG-ACC</ta>
            <ta e="T62" id="Seg_9030" s="T61">völlig</ta>
            <ta e="T63" id="Seg_9031" s="T62">ziehen-PST1-3SG</ta>
            <ta e="T64" id="Seg_9032" s="T63">aufgeregt.sein-CVB.SEQ</ta>
            <ta e="T65" id="Seg_9033" s="T64">aufwachen-PST1-3SG</ta>
            <ta e="T66" id="Seg_9034" s="T65">sich.anziehen-PST1-3SG</ta>
            <ta e="T67" id="Seg_9035" s="T66">Schlitten-3SG-DAT/LOC</ta>
            <ta e="T68" id="Seg_9036" s="T67">sich.setzen-CVB.ANT</ta>
            <ta e="T69" id="Seg_9037" s="T68">wieder</ta>
            <ta e="T70" id="Seg_9038" s="T69">einschlafen-PST1-3SG</ta>
            <ta e="T71" id="Seg_9039" s="T70">Baum-VBZ-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T72" id="Seg_9040" s="T71">Eis-VBZ-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T73" id="Seg_9041" s="T72">Rentier-VBZ-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T74" id="Seg_9042" s="T73">sein-CVB.SEQ</ta>
            <ta e="T75" id="Seg_9043" s="T74">Mädchen.[NOM]</ta>
            <ta e="T76" id="Seg_9044" s="T75">sehr</ta>
            <ta e="T77" id="Seg_9045" s="T76">müde.werden-PRS.[3SG]</ta>
            <ta e="T78" id="Seg_9046" s="T77">müde.werden-CVB.SEQ</ta>
            <ta e="T79" id="Seg_9047" s="T78">einschlafen-PST2.[3SG]</ta>
            <ta e="T80" id="Seg_9048" s="T79">schlafen-CVB.SIM</ta>
            <ta e="T81" id="Seg_9049" s="T80">liegen-CVB.SEQ</ta>
            <ta e="T82" id="Seg_9050" s="T81">frieren-CVB.SEQ</ta>
            <ta e="T83" id="Seg_9051" s="T82">bleiben-PST2.[3SG]</ta>
            <ta e="T84" id="Seg_9052" s="T83">Junge.[NOM]</ta>
            <ta e="T85" id="Seg_9053" s="T84">jüngerer.Bruder-EP-3SG.[NOM]</ta>
            <ta e="T86" id="Seg_9054" s="T85">noch</ta>
            <ta e="T87" id="Seg_9055" s="T86">drei</ta>
            <ta e="T88" id="Seg_9056" s="T87">Tag-ACC</ta>
            <ta e="T89" id="Seg_9057" s="T88">schlafen-PST2.[3SG]</ta>
            <ta e="T90" id="Seg_9058" s="T89">aufwachen-EP-PST2-3SG</ta>
            <ta e="T91" id="Seg_9059" s="T90">sehen-PST2-3SG</ta>
            <ta e="T92" id="Seg_9060" s="T91">ältere.Schwester-3SG.[NOM]</ta>
            <ta e="T93" id="Seg_9061" s="T92">frieren-CVB.SEQ</ta>
            <ta e="T94" id="Seg_9062" s="T93">bleiben-PST2.[3SG]</ta>
            <ta e="T95" id="Seg_9063" s="T94">Herde-3SG.[NOM]</ta>
            <ta e="T96" id="Seg_9064" s="T95">jeder-3SG.[NOM]</ta>
            <ta e="T97" id="Seg_9065" s="T96">frieren-CVB.SEQ</ta>
            <ta e="T98" id="Seg_9066" s="T97">sterben-PST2.[3SG]</ta>
            <ta e="T99" id="Seg_9067" s="T98">Schneewehe.[NOM]</ta>
            <ta e="T100" id="Seg_9068" s="T99">schlagen-EP-PST2.[3SG]</ta>
            <ta e="T101" id="Seg_9069" s="T100">Feuer.[NOM]</ta>
            <ta e="T102" id="Seg_9070" s="T101">heizen-TEMP-1SG</ta>
            <ta e="T103" id="Seg_9071" s="T102">vielleicht</ta>
            <ta e="T104" id="Seg_9072" s="T103">ältere.Schwester-EP-1SG.[NOM]</ta>
            <ta e="T105" id="Seg_9073" s="T104">wieder.aufleben-FUT.[3SG]</ta>
            <ta e="T106" id="Seg_9074" s="T105">EMPH</ta>
            <ta e="T107" id="Seg_9075" s="T106">sagen-CVB.SIM</ta>
            <ta e="T108" id="Seg_9076" s="T107">denken-PST2.[3SG]</ta>
            <ta e="T109" id="Seg_9077" s="T108">Junge.[NOM]</ta>
            <ta e="T110" id="Seg_9078" s="T109">Holz-VBZ-CVB.SIM</ta>
            <ta e="T111" id="Seg_9079" s="T110">gehen-PTCP.FUT-DAT/LOC</ta>
            <ta e="T112" id="Seg_9080" s="T111">Felsen-PROPR.[NOM]</ta>
            <ta e="T113" id="Seg_9081" s="T112">Fluss.[NOM]</ta>
            <ta e="T114" id="Seg_9082" s="T113">oberer.Teil-3SG-INSTR</ta>
            <ta e="T115" id="Seg_9083" s="T114">Wald.[NOM]</ta>
            <ta e="T116" id="Seg_9084" s="T115">zu</ta>
            <ta e="T117" id="Seg_9085" s="T116">laufen-PST2.[3SG]</ta>
            <ta e="T118" id="Seg_9086" s="T117">sehen-PST2-3SG</ta>
            <ta e="T119" id="Seg_9087" s="T118">Himmel-ABL</ta>
            <ta e="T120" id="Seg_9088" s="T119">%%</ta>
            <ta e="T121" id="Seg_9089" s="T120">Lagerfeuer.[NOM]</ta>
            <ta e="T122" id="Seg_9090" s="T121">flammen-CVB.SIM</ta>
            <ta e="T123" id="Seg_9091" s="T122">liegen-PRS.[3SG]</ta>
            <ta e="T124" id="Seg_9092" s="T123">böser.Geist.[NOM]</ta>
            <ta e="T125" id="Seg_9093" s="T124">Bratspieß-DAT/LOC</ta>
            <ta e="T126" id="Seg_9094" s="T125">Fleisch.[NOM]</ta>
            <ta e="T127" id="Seg_9095" s="T126">braten-EP-MED-CVB.SIM</ta>
            <ta e="T128" id="Seg_9096" s="T127">sitzen-PRS.[3SG]</ta>
            <ta e="T129" id="Seg_9097" s="T128">Junge-ACC</ta>
            <ta e="T130" id="Seg_9098" s="T129">sehen-NEG.[3SG]</ta>
            <ta e="T131" id="Seg_9099" s="T130">dieses</ta>
            <ta e="T132" id="Seg_9100" s="T131">Junge.[NOM]</ta>
            <ta e="T133" id="Seg_9101" s="T132">herunterrutschen-CAUS-CVB.SIM</ta>
            <ta e="T134" id="Seg_9102" s="T133">berühren-CVB.SIM-berühren-CVB.SIM</ta>
            <ta e="T135" id="Seg_9103" s="T134">am.anderen.Ufer-2SG-ABL</ta>
            <ta e="T136" id="Seg_9104" s="T135">essen-CVB.SIM</ta>
            <ta e="T137" id="Seg_9105" s="T136">sitzen-PST2.[3SG]</ta>
            <ta e="T138" id="Seg_9106" s="T137">wie</ta>
            <ta e="T139" id="Seg_9107" s="T138">sich.besinnen-NEG.PTCP-3SG-ACC</ta>
            <ta e="T140" id="Seg_9108" s="T139">nun</ta>
            <ta e="T141" id="Seg_9109" s="T140">denken-PRS.[3SG]</ta>
            <ta e="T142" id="Seg_9110" s="T141">böser.Geist.[NOM]</ta>
            <ta e="T143" id="Seg_9111" s="T142">Fleisch-PL-EP-1SG.[NOM]</ta>
            <ta e="T144" id="Seg_9112" s="T143">Feuer-DAT/LOC</ta>
            <ta e="T145" id="Seg_9113" s="T144">fallen-PTCP.PRS-3PL-ACC</ta>
            <ta e="T146" id="Seg_9114" s="T145">böser.Geist.[NOM]</ta>
            <ta e="T147" id="Seg_9115" s="T146">Junge-ACC</ta>
            <ta e="T148" id="Seg_9116" s="T147">sehen-CVB.SEQ</ta>
            <ta e="T149" id="Seg_9117" s="T148">töten-CVB.PURP</ta>
            <ta e="T150" id="Seg_9118" s="T149">wollen-PST2.[3SG]</ta>
            <ta e="T151" id="Seg_9119" s="T150">Junge.[NOM]</ta>
            <ta e="T152" id="Seg_9120" s="T151">Fäustling-3SG-INSTR</ta>
            <ta e="T153" id="Seg_9121" s="T152">böser.Geist.[NOM]</ta>
            <ta e="T154" id="Seg_9122" s="T153">acht</ta>
            <ta e="T155" id="Seg_9123" s="T154">Kopf-3SG-ACC</ta>
            <ta e="T156" id="Seg_9124" s="T155">völlig</ta>
            <ta e="T157" id="Seg_9125" s="T156">schlagen-EP-PST2.[3SG]</ta>
            <ta e="T158" id="Seg_9126" s="T157">Kopf-PL-3SG.[NOM]</ta>
            <ta e="T159" id="Seg_9127" s="T158">tanzen-CVB.SIM</ta>
            <ta e="T160" id="Seg_9128" s="T159">gehen-PRS-3PL</ta>
            <ta e="T161" id="Seg_9129" s="T160">Junge.[NOM]</ta>
            <ta e="T162" id="Seg_9130" s="T161">dort</ta>
            <ta e="T163" id="Seg_9131" s="T162">aufschreien-PST2.[3SG]</ta>
            <ta e="T164" id="Seg_9132" s="T163">Held.[NOM]</ta>
            <ta e="T165" id="Seg_9133" s="T164">zwei-VBZ-PTCP.HAB-3SG</ta>
            <ta e="T166" id="Seg_9134" s="T165">drei-ORD-VBZ-PTCP.HAB-3SG</ta>
            <ta e="T167" id="Seg_9135" s="T166">NEG.[3SG]</ta>
            <ta e="T168" id="Seg_9136" s="T167">böser.Geist.[NOM]</ta>
            <ta e="T169" id="Seg_9137" s="T168">Brust-3SG-ACC</ta>
            <ta e="T170" id="Seg_9138" s="T169">auftrennen-CVB.SIM</ta>
            <ta e="T171" id="Seg_9139" s="T170">ziehen-CVB.SEQ</ta>
            <ta e="T172" id="Seg_9140" s="T171">Stein-ACC</ta>
            <ta e="T173" id="Seg_9141" s="T172">herausholen-PST1-3SG</ta>
            <ta e="T174" id="Seg_9142" s="T173">Stein.[NOM]</ta>
            <ta e="T175" id="Seg_9143" s="T174">Herz-PROPR.[NOM]</ta>
            <ta e="T176" id="Seg_9144" s="T175">sein-PST2.[3SG]</ta>
            <ta e="T177" id="Seg_9145" s="T176">Junge.[NOM]</ta>
            <ta e="T178" id="Seg_9146" s="T177">Stein-ACC</ta>
            <ta e="T179" id="Seg_9147" s="T178">treten-CVB.SIM-treten-CVB.SIM</ta>
            <ta e="T180" id="Seg_9148" s="T179">gehen-CVB.SIM</ta>
            <ta e="T181" id="Seg_9149" s="T180">stehen-PST2.[3SG]</ta>
            <ta e="T182" id="Seg_9150" s="T181">Vorderteil-3SG-DAT/LOC</ta>
            <ta e="T183" id="Seg_9151" s="T182">Erde.[NOM]</ta>
            <ta e="T184" id="Seg_9152" s="T183">Hälfte-PROPR.[NOM]</ta>
            <ta e="T185" id="Seg_9153" s="T184">von.der.Größe.von</ta>
            <ta e="T186" id="Seg_9154" s="T185">Stein.[NOM]</ta>
            <ta e="T187" id="Seg_9155" s="T186">aufstehen-PST2.[3SG]</ta>
            <ta e="T188" id="Seg_9156" s="T187">Stein.[NOM]</ta>
            <ta e="T189" id="Seg_9157" s="T188">Berg.[NOM]</ta>
            <ta e="T190" id="Seg_9158" s="T189">Stein.[NOM]</ta>
            <ta e="T191" id="Seg_9159" s="T190">Berg-ACC</ta>
            <ta e="T192" id="Seg_9160" s="T191">Stein.[NOM]</ta>
            <ta e="T193" id="Seg_9161" s="T192">Herz-3SG-INSTR</ta>
            <ta e="T194" id="Seg_9162" s="T193">treten-PST2.[3SG]</ta>
            <ta e="T195" id="Seg_9163" s="T194">Berg-3SG.[NOM]</ta>
            <ta e="T196" id="Seg_9164" s="T195">zwei</ta>
            <ta e="T197" id="Seg_9165" s="T196">Richtung.[NOM]</ta>
            <ta e="T198" id="Seg_9166" s="T197">zerreißen-CVB.SEQ</ta>
            <ta e="T199" id="Seg_9167" s="T198">fallen-PST2.[3SG]</ta>
            <ta e="T200" id="Seg_9168" s="T199">betrachten-CVB.SEQ</ta>
            <ta e="T201" id="Seg_9169" s="T200">sehen-PST2-3SG</ta>
            <ta e="T202" id="Seg_9170" s="T201">Erde.[NOM]</ta>
            <ta e="T203" id="Seg_9171" s="T202">zu.sehen.sein-PRS.[3SG]</ta>
            <ta e="T204" id="Seg_9172" s="T203">dorthin</ta>
            <ta e="T205" id="Seg_9173" s="T204">gehen-CVB.SEQ</ta>
            <ta e="T206" id="Seg_9174" s="T205">Schlitten-PROPR.[NOM]</ta>
            <ta e="T207" id="Seg_9175" s="T206">Spur-3SG-ACC</ta>
            <ta e="T208" id="Seg_9176" s="T207">sehen-PST2.[3SG]</ta>
            <ta e="T209" id="Seg_9177" s="T208">Totfalle.[NOM]</ta>
            <ta e="T210" id="Seg_9178" s="T209">Fangeisen.[NOM]</ta>
            <ta e="T211" id="Seg_9179" s="T210">sehen-EP-MED-CVB.SIM</ta>
            <ta e="T212" id="Seg_9180" s="T211">gehen-PTCP.PRS</ta>
            <ta e="T213" id="Seg_9181" s="T212">Mensch.[NOM]</ta>
            <ta e="T214" id="Seg_9182" s="T213">sein-PST1-3SG</ta>
            <ta e="T215" id="Seg_9183" s="T214">sein-PST2.[3SG]</ta>
            <ta e="T216" id="Seg_9184" s="T215">wie.viel</ta>
            <ta e="T217" id="Seg_9185" s="T216">NEG</ta>
            <ta e="T218" id="Seg_9186" s="T217">sein-NEG.CVB.SIM</ta>
            <ta e="T219" id="Seg_9187" s="T218">Polarfuchs-AG.[NOM]</ta>
            <ta e="T220" id="Seg_9188" s="T219">entgegen</ta>
            <ta e="T221" id="Seg_9189" s="T220">gehen-PTCP.PRS-3SG-ACC</ta>
            <ta e="T222" id="Seg_9190" s="T221">sehen-PST2.[3SG]</ta>
            <ta e="T223" id="Seg_9191" s="T222">kommen-CVB.SEQ</ta>
            <ta e="T224" id="Seg_9192" s="T223">stehen-PST1-3SG</ta>
            <ta e="T225" id="Seg_9193" s="T224">begrüßen-PST1-3SG</ta>
            <ta e="T226" id="Seg_9194" s="T225">was-ACC</ta>
            <ta e="T227" id="Seg_9195" s="T226">machen-CVB.SIM</ta>
            <ta e="T228" id="Seg_9196" s="T227">gehen-PRS-2SG</ta>
            <ta e="T229" id="Seg_9197" s="T228">fragen-PST2.[3SG]</ta>
            <ta e="T230" id="Seg_9198" s="T229">Junge.[NOM]</ta>
            <ta e="T231" id="Seg_9199" s="T230">Totfalle.[NOM]</ta>
            <ta e="T232" id="Seg_9200" s="T231">Fangeisen.[NOM]</ta>
            <ta e="T233" id="Seg_9201" s="T232">sehen-EP-MED-PRS-1SG</ta>
            <ta e="T234" id="Seg_9202" s="T233">weit.weg</ta>
            <ta e="T235" id="Seg_9203" s="T234">Haus-PROPR-2PL</ta>
            <ta e="T236" id="Seg_9204" s="T235">Q</ta>
            <ta e="T237" id="Seg_9205" s="T236">wie.viel</ta>
            <ta e="T238" id="Seg_9206" s="T237">Mensch-2PL=Q</ta>
            <ta e="T239" id="Seg_9207" s="T238">Mutter-1SG-ACC</ta>
            <ta e="T240" id="Seg_9208" s="T239">Vater-1SG-ACC</ta>
            <ta e="T241" id="Seg_9209" s="T240">mit</ta>
            <ta e="T242" id="Seg_9210" s="T241">leben-PRS-1SG</ta>
            <ta e="T243" id="Seg_9211" s="T242">sagen-PST2.[3SG]</ta>
            <ta e="T244" id="Seg_9212" s="T243">Junge.[NOM]</ta>
            <ta e="T245" id="Seg_9213" s="T244">reiten.[IMP.2SG]</ta>
            <ta e="T246" id="Seg_9214" s="T245">mitnehmen-FUT-1SG</ta>
            <ta e="T247" id="Seg_9215" s="T246">Gast.[NOM]</ta>
            <ta e="T248" id="Seg_9216" s="T247">sein.[IMP.2SG]</ta>
            <ta e="T249" id="Seg_9217" s="T248">Stange.[NOM]</ta>
            <ta e="T250" id="Seg_9218" s="T249">Zelt-3SG-DAT/LOC</ta>
            <ta e="T251" id="Seg_9219" s="T250">bringen-PST2.[3SG]</ta>
            <ta e="T252" id="Seg_9220" s="T251">alter.Mann-PROPR</ta>
            <ta e="T253" id="Seg_9221" s="T252">Alte.[NOM]</ta>
            <ta e="T254" id="Seg_9222" s="T253">entgegen</ta>
            <ta e="T255" id="Seg_9223" s="T254">hinausgehen-CVB.SEQ-3PL</ta>
            <ta e="T256" id="Seg_9224" s="T255">treffen-EP-PST2-3PL</ta>
            <ta e="T257" id="Seg_9225" s="T256">na</ta>
            <ta e="T258" id="Seg_9226" s="T257">Ort-ABL</ta>
            <ta e="T259" id="Seg_9227" s="T258">kommen-PST1-2SG</ta>
            <ta e="T260" id="Seg_9228" s="T259">fragen-PST2-3PL</ta>
            <ta e="T261" id="Seg_9229" s="T260">hierher</ta>
            <ta e="T262" id="Seg_9230" s="T261">quer.über</ta>
            <ta e="T263" id="Seg_9231" s="T262">Auge-PROPR.[NOM]</ta>
            <ta e="T264" id="Seg_9232" s="T263">Feuer.[NOM]</ta>
            <ta e="T265" id="Seg_9233" s="T264">Sohle-PROPR.[NOM]</ta>
            <ta e="T266" id="Seg_9234" s="T265">gehen-EP-MED-CVB.SIM=noch.nicht.[3SG]</ta>
            <ta e="T267" id="Seg_9235" s="T266">Junge.[NOM]</ta>
            <ta e="T268" id="Seg_9236" s="T267">erzählen-PST2.[3SG]</ta>
            <ta e="T269" id="Seg_9237" s="T268">ältere.Schwester-EP-1SG.[NOM]</ta>
            <ta e="T270" id="Seg_9238" s="T269">frieren-CVB.SEQ</ta>
            <ta e="T271" id="Seg_9239" s="T270">sterben-PST1-3SG</ta>
            <ta e="T272" id="Seg_9240" s="T271">Rentier-PL-1PL.[NOM]</ta>
            <ta e="T273" id="Seg_9241" s="T272">auch</ta>
            <ta e="T274" id="Seg_9242" s="T273">frieren-CVB.SEQ</ta>
            <ta e="T275" id="Seg_9243" s="T274">sterben-PST1-3PL</ta>
            <ta e="T276" id="Seg_9244" s="T275">Schnee.[NOM]</ta>
            <ta e="T277" id="Seg_9245" s="T276">Inneres-3SG-DAT/LOC</ta>
            <ta e="T278" id="Seg_9246" s="T277">liegen-PRS-3PL</ta>
            <ta e="T279" id="Seg_9247" s="T278">stöbern-EP-PASS/REFL-CVB.SIM</ta>
            <ta e="T280" id="Seg_9248" s="T279">wie</ta>
            <ta e="T281" id="Seg_9249" s="T280">Mensch.[NOM]</ta>
            <ta e="T282" id="Seg_9250" s="T281">sein-FUT-1SG</ta>
            <ta e="T283" id="Seg_9251" s="T282">Q</ta>
            <ta e="T284" id="Seg_9252" s="T283">zuerst</ta>
            <ta e="T285" id="Seg_9253" s="T284">Gast.[NOM]</ta>
            <ta e="T286" id="Seg_9254" s="T285">sein.[IMP.2SG]</ta>
            <ta e="T287" id="Seg_9255" s="T286">sagen-PST2.[3SG]</ta>
            <ta e="T288" id="Seg_9256" s="T287">alter.Mann.[NOM]</ta>
            <ta e="T289" id="Seg_9257" s="T288">dann</ta>
            <ta e="T290" id="Seg_9258" s="T289">raten-EP-RECP/COLL-FUT-1PL</ta>
            <ta e="T291" id="Seg_9259" s="T290">Sohn-3SG.[NOM]</ta>
            <ta e="T292" id="Seg_9260" s="T291">hinausgehen-CVB.SEQ</ta>
            <ta e="T293" id="Seg_9261" s="T292">Rentierkuh-ACC</ta>
            <ta e="T294" id="Seg_9262" s="T293">töten-PST2.[3SG]</ta>
            <ta e="T295" id="Seg_9263" s="T294">Rand-PROPR.[NOM]</ta>
            <ta e="T296" id="Seg_9264" s="T295">Rentierkuh-ACC</ta>
            <ta e="T297" id="Seg_9265" s="T296">Gast.[NOM]</ta>
            <ta e="T298" id="Seg_9266" s="T297">jenes-ACC</ta>
            <ta e="T299" id="Seg_9267" s="T298">einsam</ta>
            <ta e="T300" id="Seg_9268" s="T299">essen-PST2.[3SG]</ta>
            <ta e="T301" id="Seg_9269" s="T300">sich.satt.essen-PST1-2SG</ta>
            <ta e="T302" id="Seg_9270" s="T301">fragen-PST2-3PL</ta>
            <ta e="T303" id="Seg_9271" s="T302">sich.satt.essen-PST1-1SG</ta>
            <ta e="T304" id="Seg_9272" s="T303">Herbst-2SG-ABL</ta>
            <ta e="T305" id="Seg_9273" s="T304">essen-CVB.SIM</ta>
            <ta e="T306" id="Seg_9274" s="T305">noch.nicht-1SG</ta>
            <ta e="T307" id="Seg_9275" s="T306">jetzt</ta>
            <ta e="T308" id="Seg_9276" s="T307">sich.ausruhen-FUT-1SG</ta>
            <ta e="T309" id="Seg_9277" s="T308">drei</ta>
            <ta e="T310" id="Seg_9278" s="T309">Tag-ACC</ta>
            <ta e="T311" id="Seg_9279" s="T310">Junge.[NOM]</ta>
            <ta e="T312" id="Seg_9280" s="T311">schlafen-PST2.[3SG]</ta>
            <ta e="T313" id="Seg_9281" s="T312">aufwachen-EP-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T314" id="Seg_9282" s="T313">alter.Mann.[NOM]</ta>
            <ta e="T315" id="Seg_9283" s="T314">meist</ta>
            <ta e="T316" id="Seg_9284" s="T315">fett.[NOM]</ta>
            <ta e="T317" id="Seg_9285" s="T316">Rentier-ACC</ta>
            <ta e="T318" id="Seg_9286" s="T317">töten-CVB.SEQ</ta>
            <ta e="T319" id="Seg_9287" s="T318">essen-DRV-PST2.[3SG]</ta>
            <ta e="T320" id="Seg_9288" s="T319">alter.Mann.[NOM]</ta>
            <ta e="T321" id="Seg_9289" s="T320">drei</ta>
            <ta e="T322" id="Seg_9290" s="T321">Monat-ADJZ.[NOM]</ta>
            <ta e="T323" id="Seg_9291" s="T322">Nahrung-ACC</ta>
            <ta e="T324" id="Seg_9292" s="T323">fertig-VBZ-PST2.[3SG]</ta>
            <ta e="T325" id="Seg_9293" s="T324">gehen-PTCP.FUT-3SG-ACC</ta>
            <ta e="T326" id="Seg_9294" s="T325">Junge.[NOM]</ta>
            <ta e="T327" id="Seg_9295" s="T326">sagen-PST2.[3SG]</ta>
            <ta e="T328" id="Seg_9296" s="T327">wo</ta>
            <ta e="T329" id="Seg_9297" s="T328">INDEF</ta>
            <ta e="T330" id="Seg_9298" s="T329">Erde.[NOM]</ta>
            <ta e="T331" id="Seg_9299" s="T330">Geist-3SG.[NOM]</ta>
            <ta e="T332" id="Seg_9300" s="T331">es.gibt</ta>
            <ta e="T333" id="Seg_9301" s="T332">man.sagt</ta>
            <ta e="T334" id="Seg_9302" s="T333">dorthin</ta>
            <ta e="T335" id="Seg_9303" s="T334">gehen-FUT-1SG</ta>
            <ta e="T336" id="Seg_9304" s="T335">Junge.[NOM]</ta>
            <ta e="T337" id="Seg_9305" s="T336">Schlitten-3SG-DAT/LOC</ta>
            <ta e="T338" id="Seg_9306" s="T337">Stein.[NOM]</ta>
            <ta e="T339" id="Seg_9307" s="T338">Herz-ACC</ta>
            <ta e="T340" id="Seg_9308" s="T339">legen-PST1-3SG</ta>
            <ta e="T341" id="Seg_9309" s="T340">Fluss.[NOM]</ta>
            <ta e="T342" id="Seg_9310" s="T341">Inneres-3SG-INSTR</ta>
            <ta e="T343" id="Seg_9311" s="T342">sich.auf.den.Weg.machen-CVB.SEQ</ta>
            <ta e="T344" id="Seg_9312" s="T343">bleiben-PST1-3SG</ta>
            <ta e="T345" id="Seg_9313" s="T344">wie.viel</ta>
            <ta e="T346" id="Seg_9314" s="T345">lange-ADVZ</ta>
            <ta e="T347" id="Seg_9315" s="T346">gehen-PST2-3SG</ta>
            <ta e="T348" id="Seg_9316" s="T347">Q</ta>
            <ta e="T349" id="Seg_9317" s="T348">nur</ta>
            <ta e="T350" id="Seg_9318" s="T349">Gusseisen.[NOM]</ta>
            <ta e="T351" id="Seg_9319" s="T350">Stange.[NOM]</ta>
            <ta e="T352" id="Seg_9320" s="T351">Zelt-ACC</ta>
            <ta e="T353" id="Seg_9321" s="T352">sehen-PST2.[3SG]</ta>
            <ta e="T354" id="Seg_9322" s="T353">Tür-ACC</ta>
            <ta e="T355" id="Seg_9323" s="T354">öffnen-CVB.SEQ</ta>
            <ta e="T356" id="Seg_9324" s="T355">sehen-PST1-3SG</ta>
            <ta e="T357" id="Seg_9325" s="T356">alt.[NOM]</ta>
            <ta e="T358" id="Seg_9326" s="T357">sehr</ta>
            <ta e="T359" id="Seg_9327" s="T358">Alte.[NOM]</ta>
            <ta e="T360" id="Seg_9328" s="T359">sitzen-PRS.[3SG]</ta>
            <ta e="T361" id="Seg_9329" s="T360">Platz.neben-3SG-DAT/LOC</ta>
            <ta e="T362" id="Seg_9330" s="T361">Blasebalg.[NOM]</ta>
            <ta e="T363" id="Seg_9331" s="T362">liegen-PRS.[3SG]</ta>
            <ta e="T364" id="Seg_9332" s="T363">Kopf-3SG-GEN</ta>
            <ta e="T365" id="Seg_9333" s="T364">oberer.Teil-3SG-DAT/LOC</ta>
            <ta e="T366" id="Seg_9334" s="T365">Tasche.[NOM]</ta>
            <ta e="T367" id="Seg_9335" s="T366">aufhängen-PASS/REFL-EP-PST2.[3SG]</ta>
            <ta e="T368" id="Seg_9336" s="T367">Tasche.[NOM]</ta>
            <ta e="T369" id="Seg_9337" s="T368">Mensch.[NOM]</ta>
            <ta e="T370" id="Seg_9338" s="T369">Gesicht-3SG-ACC</ta>
            <ta e="T371" id="Seg_9339" s="T370">ähnlich.wie</ta>
            <ta e="T372" id="Seg_9340" s="T371">Herd.[NOM]</ta>
            <ta e="T373" id="Seg_9341" s="T372">Unterteil-EP-3SG.[NOM]</ta>
            <ta e="T374" id="Seg_9342" s="T373">hohl.[NOM]</ta>
            <ta e="T375" id="Seg_9343" s="T374">Fluss.[NOM]</ta>
            <ta e="T376" id="Seg_9344" s="T375">fließen-CVB.SIM</ta>
            <ta e="T377" id="Seg_9345" s="T376">liegen-PRS.[3SG]</ta>
            <ta e="T378" id="Seg_9346" s="T377">Fluss-DAT/LOC</ta>
            <ta e="T379" id="Seg_9347" s="T378">kleines.Boot-PROPR.[NOM]</ta>
            <ta e="T380" id="Seg_9348" s="T379">Netz-VBZ-CVB.SIM</ta>
            <ta e="T381" id="Seg_9349" s="T380">gehen-PRS.[3SG]</ta>
            <ta e="T382" id="Seg_9350" s="T381">Tasche.[NOM]</ta>
            <ta e="T383" id="Seg_9351" s="T382">Sprache-PROPR.[NOM]</ta>
            <ta e="T384" id="Seg_9352" s="T383">werden-PST2.[3SG]</ta>
            <ta e="T385" id="Seg_9353" s="T384">Gast.[NOM]</ta>
            <ta e="T386" id="Seg_9354" s="T385">kommen-PST1-3SG</ta>
            <ta e="T387" id="Seg_9355" s="T386">Holz-AG-PL.[NOM]</ta>
            <ta e="T388" id="Seg_9356" s="T387">Baum-VBZ-IMP.2PL</ta>
            <ta e="T389" id="Seg_9357" s="T388">Feuer-PART</ta>
            <ta e="T390" id="Seg_9358" s="T389">anzünden-EP-IMP.2PL</ta>
            <ta e="T391" id="Seg_9359" s="T390">Holz-3PL.[NOM]</ta>
            <ta e="T392" id="Seg_9360" s="T391">selbst-3PL.[NOM]</ta>
            <ta e="T393" id="Seg_9361" s="T392">hineingehen-PST1-3PL</ta>
            <ta e="T394" id="Seg_9362" s="T393">von.draußen</ta>
            <ta e="T395" id="Seg_9363" s="T394">Junge.[NOM]</ta>
            <ta e="T396" id="Seg_9364" s="T395">verstehen-PST1-3SG</ta>
            <ta e="T397" id="Seg_9365" s="T396">eisern.[NOM]</ta>
            <ta e="T398" id="Seg_9366" s="T397">Holz-PL.[NOM]</ta>
            <ta e="T399" id="Seg_9367" s="T398">sein-PST2.[3SG]</ta>
            <ta e="T400" id="Seg_9368" s="T399">Alte.[NOM]</ta>
            <ta e="T401" id="Seg_9369" s="T400">aufstehen-CVB.SEQ</ta>
            <ta e="T402" id="Seg_9370" s="T401">Holz-PL-ACC</ta>
            <ta e="T403" id="Seg_9371" s="T402">Herd-3SG-DAT/LOC</ta>
            <ta e="T404" id="Seg_9372" s="T403">sammeln-PST1-3SG</ta>
            <ta e="T405" id="Seg_9373" s="T404">Tasche.[NOM]</ta>
            <ta e="T406" id="Seg_9374" s="T405">sagen-PST1-3SG</ta>
            <ta e="T407" id="Seg_9375" s="T406">Blasebalg.[NOM]</ta>
            <ta e="T408" id="Seg_9376" s="T407">Blasebalg-VBZ.[IMP.2SG]</ta>
            <ta e="T409" id="Seg_9377" s="T408">Stange.[NOM]</ta>
            <ta e="T410" id="Seg_9378" s="T409">Zelt.[NOM]</ta>
            <ta e="T411" id="Seg_9379" s="T410">eisern</ta>
            <ta e="T412" id="Seg_9380" s="T411">Boden-3SG.[NOM]</ta>
            <ta e="T413" id="Seg_9381" s="T412">rot.werden-INCH</ta>
            <ta e="T414" id="Seg_9382" s="T413">gehen-PST1-3SG</ta>
            <ta e="T415" id="Seg_9383" s="T414">Tasche.[NOM]</ta>
            <ta e="T416" id="Seg_9384" s="T415">immer</ta>
            <ta e="T417" id="Seg_9385" s="T416">sagen-PRS.[3SG]</ta>
            <ta e="T418" id="Seg_9386" s="T417">Blasebalg.[NOM]</ta>
            <ta e="T419" id="Seg_9387" s="T418">Blasebalg-VBZ.[IMP.2SG]</ta>
            <ta e="T420" id="Seg_9388" s="T419">Blasebalg-VBZ.[IMP.2SG]</ta>
            <ta e="T421" id="Seg_9389" s="T420">Gast.[NOM]</ta>
            <ta e="T422" id="Seg_9390" s="T421">kommen-PST1-3SG</ta>
            <ta e="T423" id="Seg_9391" s="T422">Feuer-PART</ta>
            <ta e="T424" id="Seg_9392" s="T423">hinzufügen.[IMP.2SG]</ta>
            <ta e="T425" id="Seg_9393" s="T424">Stange.[NOM]</ta>
            <ta e="T426" id="Seg_9394" s="T425">Zelt.[NOM]</ta>
            <ta e="T427" id="Seg_9395" s="T426">jeder-3SG.[NOM]</ta>
            <ta e="T428" id="Seg_9396" s="T427">rot.werden-PST1-3SG</ta>
            <ta e="T429" id="Seg_9397" s="T428">Alte.[NOM]</ta>
            <ta e="T430" id="Seg_9398" s="T429">Gast.[NOM]</ta>
            <ta e="T431" id="Seg_9399" s="T430">Junge-ACC</ta>
            <ta e="T432" id="Seg_9400" s="T431">bitten-PST1-3SG</ta>
            <ta e="T433" id="Seg_9401" s="T432">warm.werden-CAUS-EP-NEG.[IMP.2SG]</ta>
            <ta e="T434" id="Seg_9402" s="T433">sehr</ta>
            <ta e="T435" id="Seg_9403" s="T434">abkühlen-EP-CAUS.[IMP.2SG]</ta>
            <ta e="T436" id="Seg_9404" s="T435">doch</ta>
            <ta e="T437" id="Seg_9405" s="T436">Junge.[NOM]</ta>
            <ta e="T438" id="Seg_9406" s="T437">Stein.[NOM]</ta>
            <ta e="T439" id="Seg_9407" s="T438">Herz-ACC</ta>
            <ta e="T440" id="Seg_9408" s="T439">nehmen-CVB.SEQ</ta>
            <ta e="T441" id="Seg_9409" s="T440">Herd.[NOM]</ta>
            <ta e="T442" id="Seg_9410" s="T441">Mitte-3SG-DAT/LOC</ta>
            <ta e="T443" id="Seg_9411" s="T442">werfen-PST1-3SG</ta>
            <ta e="T444" id="Seg_9412" s="T443">Stange.[NOM]</ta>
            <ta e="T445" id="Seg_9413" s="T444">Zelt.[NOM]</ta>
            <ta e="T446" id="Seg_9414" s="T445">abkühlen-PST1-3SG</ta>
            <ta e="T447" id="Seg_9415" s="T446">Boden-3SG.[NOM]</ta>
            <ta e="T448" id="Seg_9416" s="T447">rot.werden-NEG-PST1-3SG</ta>
            <ta e="T449" id="Seg_9417" s="T448">Alte.[NOM]</ta>
            <ta e="T450" id="Seg_9418" s="T449">wieder</ta>
            <ta e="T451" id="Seg_9419" s="T450">heftig</ta>
            <ta e="T452" id="Seg_9420" s="T451">Reif.[NOM]</ta>
            <ta e="T453" id="Seg_9421" s="T452">sein-PST1-3SG</ta>
            <ta e="T454" id="Seg_9422" s="T453">sehr</ta>
            <ta e="T455" id="Seg_9423" s="T454">frieren-EP-CAUS-EP-NEG.[IMP.2SG]</ta>
            <ta e="T456" id="Seg_9424" s="T455">bitten-PST2.[3SG]</ta>
            <ta e="T0" id="Seg_9425" s="T456">Tasche.[NOM]</ta>
            <ta e="T947" id="Seg_9426" s="T0">jeder.[NOM]</ta>
            <ta e="T948" id="Seg_9427" s="T947">Wunsch-2SG-ACC</ta>
            <ta e="T457" id="Seg_9428" s="T948">füllen-FUT-1SG</ta>
            <ta e="T458" id="Seg_9429" s="T457">Tasche.[NOM]</ta>
            <ta e="T459" id="Seg_9430" s="T458">sein-PST2.[3SG]</ta>
            <ta e="T460" id="Seg_9431" s="T459">Erde.[NOM]</ta>
            <ta e="T461" id="Seg_9432" s="T460">Herr-3SG.[NOM]</ta>
            <ta e="T462" id="Seg_9433" s="T461">Junge.[NOM]</ta>
            <ta e="T463" id="Seg_9434" s="T462">Stein.[NOM]</ta>
            <ta e="T464" id="Seg_9435" s="T463">Herz-ACC</ta>
            <ta e="T465" id="Seg_9436" s="T464">Herd.[NOM]</ta>
            <ta e="T466" id="Seg_9437" s="T465">Inneres-3SG-ABL</ta>
            <ta e="T467" id="Seg_9438" s="T466">herausholen-CVB.SEQ</ta>
            <ta e="T468" id="Seg_9439" s="T467">nehmen-PST1-3SG</ta>
            <ta e="T469" id="Seg_9440" s="T468">Feuer-3SG.[NOM]</ta>
            <ta e="T470" id="Seg_9441" s="T469">wieder</ta>
            <ta e="T471" id="Seg_9442" s="T470">flammen-PST1-3SG</ta>
            <ta e="T472" id="Seg_9443" s="T471">Tasche.[NOM]</ta>
            <ta e="T473" id="Seg_9444" s="T472">sagen-PST1-3SG</ta>
            <ta e="T474" id="Seg_9445" s="T473">Junge-DAT/LOC</ta>
            <ta e="T475" id="Seg_9446" s="T474">nach.draußen</ta>
            <ta e="T476" id="Seg_9447" s="T475">hinausgehen.[IMP.2SG]</ta>
            <ta e="T477" id="Seg_9448" s="T476">dort</ta>
            <ta e="T478" id="Seg_9449" s="T477">Schlitten-PROPR</ta>
            <ta e="T479" id="Seg_9450" s="T478">Rentier.[NOM]</ta>
            <ta e="T480" id="Seg_9451" s="T479">stehen-PRS.[3SG]</ta>
            <ta e="T481" id="Seg_9452" s="T480">Junge.[NOM]</ta>
            <ta e="T482" id="Seg_9453" s="T481">nach.draußen</ta>
            <ta e="T483" id="Seg_9454" s="T482">hinausgehen-CVB.SEQ</ta>
            <ta e="T484" id="Seg_9455" s="T483">was.[NOM]</ta>
            <ta e="T485" id="Seg_9456" s="T484">NEG</ta>
            <ta e="T486" id="Seg_9457" s="T485">Schlitten-PROPR</ta>
            <ta e="T487" id="Seg_9458" s="T486">Rentier-3SG-ACC</ta>
            <ta e="T488" id="Seg_9459" s="T487">sehen-NEG.[3SG]</ta>
            <ta e="T489" id="Seg_9460" s="T488">wo</ta>
            <ta e="T490" id="Seg_9461" s="T489">INDEF</ta>
            <ta e="T491" id="Seg_9462" s="T490">Klingel.[NOM]</ta>
            <ta e="T492" id="Seg_9463" s="T491">Eisenteil.am.Rentiergeschirr.[NOM]</ta>
            <ta e="T493" id="Seg_9464" s="T492">nur</ta>
            <ta e="T494" id="Seg_9465" s="T493">Geräusch-3SG.[NOM]</ta>
            <ta e="T495" id="Seg_9466" s="T494">gehört.werden-PRS.[3SG]</ta>
            <ta e="T496" id="Seg_9467" s="T495">Nacken-3SG-ACC</ta>
            <ta e="T497" id="Seg_9468" s="T496">zu</ta>
            <ta e="T498" id="Seg_9469" s="T497">Tasche.[NOM]</ta>
            <ta e="T499" id="Seg_9470" s="T498">Stimme-3SG.[NOM]</ta>
            <ta e="T500" id="Seg_9471" s="T499">gehört.werden-EP-PST2.[3SG]</ta>
            <ta e="T501" id="Seg_9472" s="T500">sich.setzen.[IMP.2SG]</ta>
            <ta e="T502" id="Seg_9473" s="T501">Schlitten-2SG-DAT/LOC</ta>
            <ta e="T503" id="Seg_9474" s="T502">Hinterteil-EP-2SG.[NOM]</ta>
            <ta e="T504" id="Seg_9475" s="T503">zu</ta>
            <ta e="T505" id="Seg_9476" s="T504">sich.umsehen-EP-NEG.[IMP.2SG]</ta>
            <ta e="T506" id="Seg_9477" s="T505">Weg.[NOM]</ta>
            <ta e="T507" id="Seg_9478" s="T506">auf.dem.Weg</ta>
            <ta e="T508" id="Seg_9479" s="T507">halten-NEG.[IMP.2SG]</ta>
            <ta e="T509" id="Seg_9480" s="T508">Rentier-PL-2SG-ACC</ta>
            <ta e="T510" id="Seg_9481" s="T509">essen-EP-CAUS-EP-NEG.[IMP.2SG]</ta>
            <ta e="T511" id="Seg_9482" s="T510">Junge.[NOM]</ta>
            <ta e="T512" id="Seg_9483" s="T511">Hand-3SG-ACC</ta>
            <ta e="T513" id="Seg_9484" s="T512">ausstrecken-PST1-3SG</ta>
            <ta e="T514" id="Seg_9485" s="T513">Zügel-ACC</ta>
            <ta e="T515" id="Seg_9486" s="T514">greifen-PST1-3SG</ta>
            <ta e="T516" id="Seg_9487" s="T515">zu.sehen.sein-EP-NEG.PTCP</ta>
            <ta e="T517" id="Seg_9488" s="T516">Schlitten-DAT/LOC</ta>
            <ta e="T518" id="Seg_9489" s="T517">besteigen-PST1-3SG</ta>
            <ta e="T519" id="Seg_9490" s="T518">Wind-PROPR.[NOM]</ta>
            <ta e="T520" id="Seg_9491" s="T519">Schneesturm.[NOM]</ta>
            <ta e="T521" id="Seg_9492" s="T520">schlagen-PST1-3SG</ta>
            <ta e="T522" id="Seg_9493" s="T521">wie.viel-ACC</ta>
            <ta e="T523" id="Seg_9494" s="T522">gehen-PST2-3SG</ta>
            <ta e="T524" id="Seg_9495" s="T523">Q</ta>
            <ta e="T525" id="Seg_9496" s="T524">wer.[NOM]</ta>
            <ta e="T526" id="Seg_9497" s="T525">wissen-FUT.[3SG]=Q</ta>
            <ta e="T527" id="Seg_9498" s="T526">Schlitten-PROPR.[NOM]</ta>
            <ta e="T528" id="Seg_9499" s="T527">Rentier-3PL.[NOM]</ta>
            <ta e="T529" id="Seg_9500" s="T528">selbst-3PL.[NOM]</ta>
            <ta e="T530" id="Seg_9501" s="T529">mitnehmen-PRS-3PL</ta>
            <ta e="T531" id="Seg_9502" s="T530">Junge.[NOM]</ta>
            <ta e="T532" id="Seg_9503" s="T531">frieren-NEG.[3SG]</ta>
            <ta e="T533" id="Seg_9504" s="T532">Hand-3SG-ACC</ta>
            <ta e="T534" id="Seg_9505" s="T533">Sitz-3SG-GEN</ta>
            <ta e="T535" id="Seg_9506" s="T534">Inneres-3SG-DAT/LOC</ta>
            <ta e="T536" id="Seg_9507" s="T535">stecken-PST1-3SG</ta>
            <ta e="T537" id="Seg_9508" s="T536">dort</ta>
            <ta e="T538" id="Seg_9509" s="T537">warm.[NOM]</ta>
            <ta e="T539" id="Seg_9510" s="T538">eisern.[NOM]</ta>
            <ta e="T540" id="Seg_9511" s="T539">Holz.[NOM]</ta>
            <ta e="T541" id="Seg_9512" s="T540">liegen-PRS.[3SG]</ta>
            <ta e="T542" id="Seg_9513" s="T541">sein-PST2.[3SG]</ta>
            <ta e="T543" id="Seg_9514" s="T542">jenes-3SG-3SG.[NOM]</ta>
            <ta e="T544" id="Seg_9515" s="T543">warm.werden-EP-CAUS-PRS.[3SG]</ta>
            <ta e="T545" id="Seg_9516" s="T544">Fluss.[NOM]</ta>
            <ta e="T546" id="Seg_9517" s="T545">oberer.Teil-3SG-DAT/LOC</ta>
            <ta e="T547" id="Seg_9518" s="T546">sehen-PST1-3SG</ta>
            <ta e="T548" id="Seg_9519" s="T547">Schlitten-DAT/LOC</ta>
            <ta e="T549" id="Seg_9520" s="T548">sterben-PTCP.PST</ta>
            <ta e="T550" id="Seg_9521" s="T549">Frau.[NOM]</ta>
            <ta e="T551" id="Seg_9522" s="T550">liegen-PRS.[3SG]</ta>
            <ta e="T552" id="Seg_9523" s="T551">heftig</ta>
            <ta e="T553" id="Seg_9524" s="T552">stöbern-EP-PASS/REFL-CVB.SEQ</ta>
            <ta e="T554" id="Seg_9525" s="T553">nachdem</ta>
            <ta e="T555" id="Seg_9526" s="T554">aushalten-NEG.CVB.SIM</ta>
            <ta e="T556" id="Seg_9527" s="T555">halten-PST1-3SG</ta>
            <ta e="T557" id="Seg_9528" s="T556">halten-PTCP.PRS-3SG-ACC</ta>
            <ta e="T558" id="Seg_9529" s="T557">mit</ta>
            <ta e="T559" id="Seg_9530" s="T558">vorderstes.Rentier-3SG.[NOM]</ta>
            <ta e="T560" id="Seg_9531" s="T559">PFV</ta>
            <ta e="T561" id="Seg_9532" s="T560">springen-CVB.SEQ</ta>
            <ta e="T562" id="Seg_9533" s="T561">wohin</ta>
            <ta e="T563" id="Seg_9534" s="T562">gehen-PST2-3SG</ta>
            <ta e="T564" id="Seg_9535" s="T563">wohin</ta>
            <ta e="T565" id="Seg_9536" s="T564">kommen-PST2-3SG</ta>
            <ta e="T566" id="Seg_9537" s="T565">drei</ta>
            <ta e="T567" id="Seg_9538" s="T566">schwarzes.Rentier.[NOM]</ta>
            <ta e="T568" id="Seg_9539" s="T567">Rentier-PROPR.[NOM]</ta>
            <ta e="T569" id="Seg_9540" s="T568">sein-PST1-3SG</ta>
            <ta e="T570" id="Seg_9541" s="T569">sein-PST2.[3SG]</ta>
            <ta e="T571" id="Seg_9542" s="T570">vorderstes.Rentier-3SG.[NOM]</ta>
            <ta e="T572" id="Seg_9543" s="T571">zu.Ende.gehen-CVB.SEQ</ta>
            <ta e="T573" id="Seg_9544" s="T572">gehen-CVB.SEQ</ta>
            <ta e="T574" id="Seg_9545" s="T573">bleiben-PST2.[3SG]</ta>
            <ta e="T575" id="Seg_9546" s="T574">Tasche.[NOM]</ta>
            <ta e="T576" id="Seg_9547" s="T575">Stimme-3SG.[NOM]</ta>
            <ta e="T577" id="Seg_9548" s="T576">gehört.werden-EP-PST2.[3SG]</ta>
            <ta e="T578" id="Seg_9549" s="T577">2SG-DAT/LOC</ta>
            <ta e="T579" id="Seg_9550" s="T578">sagen-PST2-EP-1SG</ta>
            <ta e="T580" id="Seg_9551" s="T579">halten-NEG.[IMP.2SG]</ta>
            <ta e="T581" id="Seg_9552" s="T580">Junge.[NOM]</ta>
            <ta e="T582" id="Seg_9553" s="T581">Schlitten-3SG-ABL</ta>
            <ta e="T583" id="Seg_9554" s="T582">rot.[NOM]</ta>
            <ta e="T584" id="Seg_9555" s="T583">eisern.[NOM]</ta>
            <ta e="T585" id="Seg_9556" s="T584">Holz-ACC</ta>
            <ta e="T586" id="Seg_9557" s="T585">nehmen-CVB.SEQ</ta>
            <ta e="T587" id="Seg_9558" s="T586">Mädchen.[NOM]</ta>
            <ta e="T588" id="Seg_9559" s="T587">Schlitten-3SG-GEN</ta>
            <ta e="T589" id="Seg_9560" s="T588">Nachbarschaft-3SG-DAT/LOC</ta>
            <ta e="T590" id="Seg_9561" s="T589">nach.innen</ta>
            <ta e="T591" id="Seg_9562" s="T590">stoßen-PST1-3SG</ta>
            <ta e="T592" id="Seg_9563" s="T591">Schnee.[NOM]</ta>
            <ta e="T593" id="Seg_9564" s="T592">sofort</ta>
            <ta e="T594" id="Seg_9565" s="T593">schmelzen-CVB.SEQ</ta>
            <ta e="T595" id="Seg_9566" s="T594">bleiben-PST1-3SG</ta>
            <ta e="T596" id="Seg_9567" s="T595">frieren-CVB.SEQ</ta>
            <ta e="T597" id="Seg_9568" s="T596">sterben-PTCP.PST</ta>
            <ta e="T598" id="Seg_9569" s="T597">Frau-3SG.[NOM]</ta>
            <ta e="T599" id="Seg_9570" s="T598">wieder.aufleben-PST1-3SG</ta>
            <ta e="T600" id="Seg_9571" s="T599">sehen-PST2-3SG</ta>
            <ta e="T601" id="Seg_9572" s="T600">sehr</ta>
            <ta e="T602" id="Seg_9573" s="T601">schön</ta>
            <ta e="T603" id="Seg_9574" s="T602">sehr</ta>
            <ta e="T604" id="Seg_9575" s="T603">Mädchen.[NOM]</ta>
            <ta e="T605" id="Seg_9576" s="T604">sein-PST2.[3SG]</ta>
            <ta e="T606" id="Seg_9577" s="T605">Mädchen.[NOM]</ta>
            <ta e="T607" id="Seg_9578" s="T606">sich.freuen-CVB.SEQ</ta>
            <ta e="T608" id="Seg_9579" s="T607">Unglück-POSS</ta>
            <ta e="T609" id="Seg_9580" s="T608">NEG.[3SG]</ta>
            <ta e="T610" id="Seg_9581" s="T609">jetzt</ta>
            <ta e="T611" id="Seg_9582" s="T610">selbst-1SG.[NOM]</ta>
            <ta e="T612" id="Seg_9583" s="T611">Haus-1SG-DAT/LOC</ta>
            <ta e="T613" id="Seg_9584" s="T612">ankommen-FUT-1SG</ta>
            <ta e="T614" id="Seg_9585" s="T613">mitkommen-EP-NEG-2SG</ta>
            <ta e="T615" id="Seg_9586" s="T614">Q</ta>
            <ta e="T616" id="Seg_9587" s="T615">essen-EP-CAUS-FUT-1SG</ta>
            <ta e="T617" id="Seg_9588" s="T616">warm.[NOM]</ta>
            <ta e="T618" id="Seg_9589" s="T617">Bettwäsche-DAT/LOC</ta>
            <ta e="T619" id="Seg_9590" s="T618">ausbreiten-FUT-1SG</ta>
            <ta e="T620" id="Seg_9591" s="T619">bitten-PST2.[3SG]</ta>
            <ta e="T621" id="Seg_9592" s="T620">Mädchen.[NOM]</ta>
            <ta e="T622" id="Seg_9593" s="T621">fern.[NOM]</ta>
            <ta e="T623" id="Seg_9594" s="T622">Q</ta>
            <ta e="T624" id="Seg_9595" s="T623">Haus-2SG.[NOM]</ta>
            <ta e="T625" id="Seg_9596" s="T624">Wasser.[NOM]</ta>
            <ta e="T626" id="Seg_9597" s="T625">Inneres-3SG-DAT/LOC</ta>
            <ta e="T627" id="Seg_9598" s="T626">Junge.[NOM]</ta>
            <ta e="T628" id="Seg_9599" s="T627">viel</ta>
            <ta e="T629" id="Seg_9600" s="T628">Wunder-ACC</ta>
            <ta e="T630" id="Seg_9601" s="T629">sehen-PST2-3SG</ta>
            <ta e="T631" id="Seg_9602" s="T630">jenes.[NOM]</ta>
            <ta e="T632" id="Seg_9603" s="T631">wegen</ta>
            <ta e="T633" id="Seg_9604" s="T632">Mädchen.[NOM]</ta>
            <ta e="T634" id="Seg_9605" s="T633">Erzählung-3SG-ACC</ta>
            <ta e="T635" id="Seg_9606" s="T634">sich.wundern-NEG-PST1-3SG</ta>
            <ta e="T636" id="Seg_9607" s="T635">sagen-CVB.SIM</ta>
            <ta e="T637" id="Seg_9608" s="T636">sein-PST2.[3SG]</ta>
            <ta e="T638" id="Seg_9609" s="T637">1SG.[NOM]</ta>
            <ta e="T639" id="Seg_9610" s="T638">ältere.Schwester-1SG-ACC</ta>
            <ta e="T640" id="Seg_9611" s="T639">finden-PTCP.FUT-1SG-ACC</ta>
            <ta e="T641" id="Seg_9612" s="T640">man.muss</ta>
            <ta e="T642" id="Seg_9613" s="T641">später</ta>
            <ta e="T643" id="Seg_9614" s="T642">kommen-FUT-1SG</ta>
            <ta e="T644" id="Seg_9615" s="T643">EVID</ta>
            <ta e="T645" id="Seg_9616" s="T644">warten.[IMP.2SG]</ta>
            <ta e="T646" id="Seg_9617" s="T645">Junge.[NOM]</ta>
            <ta e="T647" id="Seg_9618" s="T646">wie.viel-ACC-wie.viel-ACC</ta>
            <ta e="T648" id="Seg_9619" s="T647">gehen-PST2-3SG</ta>
            <ta e="T649" id="Seg_9620" s="T648">wer.[NOM]</ta>
            <ta e="T650" id="Seg_9621" s="T649">NEG</ta>
            <ta e="T651" id="Seg_9622" s="T650">wissen-NEG.[3SG]</ta>
            <ta e="T652" id="Seg_9623" s="T651">plötzlich</ta>
            <ta e="T653" id="Seg_9624" s="T652">Loch.in.der.Schneedecke.[NOM]</ta>
            <ta e="T654" id="Seg_9625" s="T653">werden-PST2.[3SG]</ta>
            <ta e="T655" id="Seg_9626" s="T654">Rentierflechte-PROPR.[NOM]</ta>
            <ta e="T656" id="Seg_9627" s="T655">sehr</ta>
            <ta e="T657" id="Seg_9628" s="T656">Rentier-PL-3SG.[NOM]</ta>
            <ta e="T658" id="Seg_9629" s="T657">hungrig-VBZ-PST1-3PL</ta>
            <ta e="T659" id="Seg_9630" s="T658">essen-EP-CAUS-PTCP.FUT-DAT/LOC</ta>
            <ta e="T660" id="Seg_9631" s="T659">Q</ta>
            <ta e="T661" id="Seg_9632" s="T660">sagen-CVB.SIM</ta>
            <ta e="T662" id="Seg_9633" s="T661">denken-PST2.[3SG]</ta>
            <ta e="T663" id="Seg_9634" s="T662">Erde.[NOM]</ta>
            <ta e="T664" id="Seg_9635" s="T663">Herr-3SG-GEN</ta>
            <ta e="T665" id="Seg_9636" s="T664">Wort-3SG-ACC</ta>
            <ta e="T666" id="Seg_9637" s="T665">erinnern-CVB.SIM-erinnern-CVB.SIM</ta>
            <ta e="T667" id="Seg_9638" s="T666">Rentier-PL-3SG-ACC</ta>
            <ta e="T668" id="Seg_9639" s="T667">halten-CAUS-NEG-PST1-3SG</ta>
            <ta e="T669" id="Seg_9640" s="T668">dann</ta>
            <ta e="T670" id="Seg_9641" s="T669">sagen-CVB.SIM</ta>
            <ta e="T671" id="Seg_9642" s="T670">denken-PST1-3SG</ta>
            <ta e="T672" id="Seg_9643" s="T671">sehr</ta>
            <ta e="T673" id="Seg_9644" s="T672">hungrig-VBZ-PST2-3PL</ta>
            <ta e="T674" id="Seg_9645" s="T673">halten-EP-CAUS-PTCP.FUT-DAT/LOC</ta>
            <ta e="T675" id="Seg_9646" s="T674">halten-EP-CAUS-PTCP.PRS-3SG-ACC</ta>
            <ta e="T676" id="Seg_9647" s="T675">mit</ta>
            <ta e="T677" id="Seg_9648" s="T676">Mitte-ADJZ</ta>
            <ta e="T678" id="Seg_9649" s="T677">einspannen-PTCP.PRS</ta>
            <ta e="T679" id="Seg_9650" s="T678">Rentier-3SG.[NOM]</ta>
            <ta e="T680" id="Seg_9651" s="T679">verschwinden-NMNZ.[NOM]</ta>
            <ta e="T681" id="Seg_9652" s="T680">nur</ta>
            <ta e="T682" id="Seg_9653" s="T681">machen-CVB.SEQ</ta>
            <ta e="T683" id="Seg_9654" s="T682">bleiben-PST2.[3SG]</ta>
            <ta e="T684" id="Seg_9655" s="T683">wohin</ta>
            <ta e="T685" id="Seg_9656" s="T684">gehen-PST2-3SG</ta>
            <ta e="T686" id="Seg_9657" s="T685">wohin</ta>
            <ta e="T687" id="Seg_9658" s="T686">rennen-PST2-3SG</ta>
            <ta e="T688" id="Seg_9659" s="T687">wieder</ta>
            <ta e="T689" id="Seg_9660" s="T688">Stimme.[NOM]</ta>
            <ta e="T690" id="Seg_9661" s="T689">gehört.werden-EP-PST2.[3SG]</ta>
            <ta e="T691" id="Seg_9662" s="T690">1SG.[NOM]</ta>
            <ta e="T692" id="Seg_9663" s="T691">2SG-DAT/LOC</ta>
            <ta e="T693" id="Seg_9664" s="T692">sagen-PST2-EP-1SG</ta>
            <ta e="T694" id="Seg_9665" s="T693">essen-EP-CAUS-EP-NEG.[IMP.2SG]</ta>
            <ta e="T695" id="Seg_9666" s="T694">Rentier-PL-2SG-ACC</ta>
            <ta e="T696" id="Seg_9667" s="T695">Junge.[NOM]</ta>
            <ta e="T697" id="Seg_9668" s="T696">wie.viel-ACC</ta>
            <ta e="T698" id="Seg_9669" s="T697">gehen-PST2-3SG</ta>
            <ta e="T699" id="Seg_9670" s="T698">Q</ta>
            <ta e="T700" id="Seg_9671" s="T699">dieses</ta>
            <ta e="T701" id="Seg_9672" s="T700">Stein.[NOM]</ta>
            <ta e="T702" id="Seg_9673" s="T701">Berg-3SG-DAT/LOC</ta>
            <ta e="T703" id="Seg_9674" s="T702">ankommen-PST1-3SG</ta>
            <ta e="T704" id="Seg_9675" s="T703">zwei</ta>
            <ta e="T705" id="Seg_9676" s="T704">Richtung.[NOM]</ta>
            <ta e="T706" id="Seg_9677" s="T705">zerreißen-PTCP.HAB-3SG-DAT/LOC</ta>
            <ta e="T707" id="Seg_9678" s="T706">nur</ta>
            <ta e="T708" id="Seg_9679" s="T707">selbst-3SG-GEN</ta>
            <ta e="T709" id="Seg_9680" s="T708">Ort-3SG-DAT/LOC</ta>
            <ta e="T710" id="Seg_9681" s="T709">ankommen-PST2.[3SG]</ta>
            <ta e="T711" id="Seg_9682" s="T710">sein-PST2.[3SG]</ta>
            <ta e="T712" id="Seg_9683" s="T711">frieren-CVB.SEQ</ta>
            <ta e="T713" id="Seg_9684" s="T712">sterben-PTCP.PST</ta>
            <ta e="T714" id="Seg_9685" s="T713">Rentier-PL.[NOM]</ta>
            <ta e="T715" id="Seg_9686" s="T714">Horn-3PL.[NOM]</ta>
            <ta e="T716" id="Seg_9687" s="T715">kaum</ta>
            <ta e="T717" id="Seg_9688" s="T716">Schnee.[NOM]</ta>
            <ta e="T718" id="Seg_9689" s="T717">Inneres-3SG-ABL</ta>
            <ta e="T719" id="Seg_9690" s="T718">herausziehen-PRS-3PL</ta>
            <ta e="T720" id="Seg_9691" s="T719">Schnee-ACC</ta>
            <ta e="T721" id="Seg_9692" s="T720">schaufeln-CVB.SEQ</ta>
            <ta e="T722" id="Seg_9693" s="T721">ältere.Schwester-3SG-GEN</ta>
            <ta e="T723" id="Seg_9694" s="T722">vorderstes.Rentier-3SG-ACC</ta>
            <ta e="T724" id="Seg_9695" s="T723">finden-PST1-3SG</ta>
            <ta e="T725" id="Seg_9696" s="T724">warm.[NOM]</ta>
            <ta e="T726" id="Seg_9697" s="T725">eisern.[NOM]</ta>
            <ta e="T727" id="Seg_9698" s="T726">Holz-3SG-INSTR</ta>
            <ta e="T728" id="Seg_9699" s="T727">streifen-PST1-3SG</ta>
            <ta e="T729" id="Seg_9700" s="T728">Rentier-3SG.[NOM]</ta>
            <ta e="T730" id="Seg_9701" s="T729">wieder.aufleben-CVB.SEQ</ta>
            <ta e="T731" id="Seg_9702" s="T730">nachdem</ta>
            <ta e="T732" id="Seg_9703" s="T731">aufstehen-PST1-3SG</ta>
            <ta e="T733" id="Seg_9704" s="T732">dann</ta>
            <ta e="T734" id="Seg_9705" s="T733">Holz-ACC</ta>
            <ta e="T735" id="Seg_9706" s="T734">nehmen-PST1-3SG</ta>
            <ta e="T736" id="Seg_9707" s="T735">Schnee-DAT/LOC</ta>
            <ta e="T737" id="Seg_9708" s="T736">stoßen-PST1-3SG</ta>
            <ta e="T738" id="Seg_9709" s="T737">frieren-CVB.SEQ</ta>
            <ta e="T739" id="Seg_9710" s="T738">sterben-PTCP.PST</ta>
            <ta e="T740" id="Seg_9711" s="T739">Rentier-PL.[NOM]</ta>
            <ta e="T741" id="Seg_9712" s="T740">sofort</ta>
            <ta e="T742" id="Seg_9713" s="T741">jeder-3PL.[NOM]</ta>
            <ta e="T743" id="Seg_9714" s="T742">wieder.aufleben-PST1-3PL</ta>
            <ta e="T744" id="Seg_9715" s="T743">ganz</ta>
            <ta e="T745" id="Seg_9716" s="T744">Herde.[NOM]</ta>
            <ta e="T746" id="Seg_9717" s="T745">werden-CVB.SEQ</ta>
            <ta e="T747" id="Seg_9718" s="T746">essen-CVB.SIM</ta>
            <ta e="T748" id="Seg_9719" s="T747">gehen-PRS-3PL</ta>
            <ta e="T749" id="Seg_9720" s="T748">neu</ta>
            <ta e="T750" id="Seg_9721" s="T749">Erde-EP-INSTR</ta>
            <ta e="T751" id="Seg_9722" s="T750">sich.umsehen-PST2-3SG</ta>
            <ta e="T752" id="Seg_9723" s="T751">ältere.Schwester-3SG.[NOM]</ta>
            <ta e="T753" id="Seg_9724" s="T752">stehen-PRS.[3SG]</ta>
            <ta e="T754" id="Seg_9725" s="T753">Junge.[NOM]</ta>
            <ta e="T755" id="Seg_9726" s="T754">ältere.Schwester-3SG-ACC</ta>
            <ta e="T756" id="Seg_9727" s="T755">sehen-CVB.SEQ</ta>
            <ta e="T757" id="Seg_9728" s="T756">sehr</ta>
            <ta e="T758" id="Seg_9729" s="T757">sich.freuen-PST1-3SG</ta>
            <ta e="T759" id="Seg_9730" s="T758">zuerst</ta>
            <ta e="T760" id="Seg_9731" s="T759">2SG.[NOM]</ta>
            <ta e="T761" id="Seg_9732" s="T760">sagen-PST1-3SG</ta>
            <ta e="T762" id="Seg_9733" s="T761">ältere.Schwester-3SG.[NOM]</ta>
            <ta e="T763" id="Seg_9734" s="T762">lange</ta>
            <ta e="T764" id="Seg_9735" s="T763">sehr-ADVZ</ta>
            <ta e="T765" id="Seg_9736" s="T764">schlafen-PST2-EP-2SG</ta>
            <ta e="T766" id="Seg_9737" s="T765">dann</ta>
            <ta e="T767" id="Seg_9738" s="T766">1SG.[NOM]</ta>
            <ta e="T768" id="Seg_9739" s="T767">Frühling.[NOM]</ta>
            <ta e="T769" id="Seg_9740" s="T768">werden-PST2.[3SG]</ta>
            <ta e="T770" id="Seg_9741" s="T769">sein-PST2.[3SG]</ta>
            <ta e="T771" id="Seg_9742" s="T770">Winter-ACC</ta>
            <ta e="T772" id="Seg_9743" s="T771">ganz</ta>
            <ta e="T773" id="Seg_9744" s="T772">schlafen-PST2-1SG</ta>
            <ta e="T774" id="Seg_9745" s="T773">sein-PST2.[3SG]</ta>
            <ta e="T775" id="Seg_9746" s="T774">Junge.[NOM]</ta>
            <ta e="T776" id="Seg_9747" s="T775">ältere.Schwester-3SG-ACC</ta>
            <ta e="T777" id="Seg_9748" s="T776">Haus-3SG-DAT/LOC</ta>
            <ta e="T778" id="Seg_9749" s="T777">bringen-PST2-3SG</ta>
            <ta e="T779" id="Seg_9750" s="T778">Herde-3SG-ACC</ta>
            <ta e="T780" id="Seg_9751" s="T779">treiben-CVB.SEQ</ta>
            <ta e="T781" id="Seg_9752" s="T780">bringen-PST1-3SG</ta>
            <ta e="T782" id="Seg_9753" s="T781">wie.viel</ta>
            <ta e="T783" id="Seg_9754" s="T782">NEG</ta>
            <ta e="T784" id="Seg_9755" s="T783">sein-NEG.CVB.SIM</ta>
            <ta e="T785" id="Seg_9756" s="T784">dieses</ta>
            <ta e="T786" id="Seg_9757" s="T785">Junge.[NOM]</ta>
            <ta e="T787" id="Seg_9758" s="T786">vorbereiten-PST1-3SG</ta>
            <ta e="T788" id="Seg_9759" s="T787">wohin</ta>
            <ta e="T789" id="Seg_9760" s="T788">gehen-PRS-2SG</ta>
            <ta e="T790" id="Seg_9761" s="T789">fragen-PST2.[3SG]</ta>
            <ta e="T791" id="Seg_9762" s="T790">ältere.Schwester-3SG.[NOM]</ta>
            <ta e="T792" id="Seg_9763" s="T791">bald</ta>
            <ta e="T793" id="Seg_9764" s="T792">kommen-FUT-1SG</ta>
            <ta e="T794" id="Seg_9765" s="T793">fürchten-EP-NEG.[IMP.2SG]</ta>
            <ta e="T795" id="Seg_9766" s="T794">Junge.[NOM]</ta>
            <ta e="T796" id="Seg_9767" s="T795">dieses</ta>
            <ta e="T797" id="Seg_9768" s="T796">Mädchen-ACC</ta>
            <ta e="T798" id="Seg_9769" s="T797">suchen-CVB.SIM</ta>
            <ta e="T799" id="Seg_9770" s="T798">gehen-CAP.[3SG]</ta>
            <ta e="T800" id="Seg_9771" s="T799">Wasser.[NOM]</ta>
            <ta e="T801" id="Seg_9772" s="T800">Inneres-3SG-DAT/LOC</ta>
            <ta e="T802" id="Seg_9773" s="T801">Haus-PROPR-ACC</ta>
            <ta e="T803" id="Seg_9774" s="T802">nachdem</ta>
            <ta e="T804" id="Seg_9775" s="T803">gehen-CVB.SEQ</ta>
            <ta e="T805" id="Seg_9776" s="T804">düster.[NOM]</ta>
            <ta e="T806" id="Seg_9777" s="T805">sehr</ta>
            <ta e="T807" id="Seg_9778" s="T806">Schneesturm.[NOM]</ta>
            <ta e="T808" id="Seg_9779" s="T807">werden-PST2.[3SG]</ta>
            <ta e="T809" id="Seg_9780" s="T808">Erde-3SG-INSTR</ta>
            <ta e="T810" id="Seg_9781" s="T809">EMPH</ta>
            <ta e="T811" id="Seg_9782" s="T810">gehen-PTCP.PRS-3SG-ACC</ta>
            <ta e="T812" id="Seg_9783" s="T811">wissen-NEG.[3SG]</ta>
            <ta e="T813" id="Seg_9784" s="T812">Eis-3SG-INSTR</ta>
            <ta e="T814" id="Seg_9785" s="T813">EMPH</ta>
            <ta e="T815" id="Seg_9786" s="T814">gehen-PTCP.PRS-3SG-ACC</ta>
            <ta e="T816" id="Seg_9787" s="T815">wissen-NEG.[3SG]</ta>
            <ta e="T817" id="Seg_9788" s="T816">ausdenken-FREQ-CVB.SEQ</ta>
            <ta e="T818" id="Seg_9789" s="T817">sehen-PST2-3SG</ta>
            <ta e="T819" id="Seg_9790" s="T818">Fluss.[NOM]</ta>
            <ta e="T820" id="Seg_9791" s="T819">Eisscholle-3SG.[NOM]</ta>
            <ta e="T821" id="Seg_9792" s="T820">sein-PST2.[3SG]</ta>
            <ta e="T822" id="Seg_9793" s="T821">Frühling-ADJZ.[NOM]</ta>
            <ta e="T823" id="Seg_9794" s="T822">Eis.[NOM]</ta>
            <ta e="T824" id="Seg_9795" s="T823">hängen.bleiben-CVB.SEQ</ta>
            <ta e="T825" id="Seg_9796" s="T824">Junge.[NOM]</ta>
            <ta e="T826" id="Seg_9797" s="T825">Wasser-DAT/LOC</ta>
            <ta e="T827" id="Seg_9798" s="T826">fallen-PST2.[3SG]</ta>
            <ta e="T828" id="Seg_9799" s="T827">fallen-CVB.SEQ</ta>
            <ta e="T829" id="Seg_9800" s="T828">Hecht.[NOM]</ta>
            <ta e="T830" id="Seg_9801" s="T829">werden-PST2.[3SG]</ta>
            <ta e="T831" id="Seg_9802" s="T830">Wasser.[NOM]</ta>
            <ta e="T832" id="Seg_9803" s="T831">Inneres-3SG-INSTR</ta>
            <ta e="T833" id="Seg_9804" s="T832">fließen-CVB.SIM</ta>
            <ta e="T834" id="Seg_9805" s="T833">gehen-CVB.SEQ</ta>
            <ta e="T835" id="Seg_9806" s="T834">Netz-DAT/LOC</ta>
            <ta e="T836" id="Seg_9807" s="T835">geraten-PST2.[3SG]</ta>
            <ta e="T837" id="Seg_9808" s="T836">breit.[NOM]</ta>
            <ta e="T838" id="Seg_9809" s="T837">Netz-DAT/LOC</ta>
            <ta e="T839" id="Seg_9810" s="T838">Hecht-PL.[NOM]</ta>
            <ta e="T840" id="Seg_9811" s="T839">nur</ta>
            <ta e="T841" id="Seg_9812" s="T840">wie.viel</ta>
            <ta e="T842" id="Seg_9813" s="T841">NEG</ta>
            <ta e="T843" id="Seg_9814" s="T842">sein-NEG.CVB.SIM</ta>
            <ta e="T844" id="Seg_9815" s="T843">Mädchen.[NOM]</ta>
            <ta e="T845" id="Seg_9816" s="T844">kommen-PST1-3SG</ta>
            <ta e="T846" id="Seg_9817" s="T845">dieses</ta>
            <ta e="T847" id="Seg_9818" s="T846">Mädchen-3SG.[NOM]</ta>
            <ta e="T848" id="Seg_9819" s="T847">wieder.aufleben-PASS-PTCP.HAB-3SG.[3SG]</ta>
            <ta e="T849" id="Seg_9820" s="T848">Hecht.[NOM]</ta>
            <ta e="T850" id="Seg_9821" s="T849">Junge-ACC</ta>
            <ta e="T851" id="Seg_9822" s="T850">vorderstes.Rentier.[NOM]</ta>
            <ta e="T852" id="Seg_9823" s="T851">machen-CVB.SEQ</ta>
            <ta e="T853" id="Seg_9824" s="T852">nachdem</ta>
            <ta e="T854" id="Seg_9825" s="T853">einspannen-PST1-3SG</ta>
            <ta e="T855" id="Seg_9826" s="T854">Haus-3SG-DAT/LOC</ta>
            <ta e="T856" id="Seg_9827" s="T855">gehen-CAP.[3SG]</ta>
            <ta e="T857" id="Seg_9828" s="T856">vorderstes.Rentier.[NOM]</ta>
            <ta e="T858" id="Seg_9829" s="T857">hören-EP-NEG.[3SG]</ta>
            <ta e="T859" id="Seg_9830" s="T858">Ufer.[NOM]</ta>
            <ta e="T860" id="Seg_9831" s="T859">nur</ta>
            <ta e="T861" id="Seg_9832" s="T860">zu</ta>
            <ta e="T862" id="Seg_9833" s="T861">%%-PRS.[3SG]</ta>
            <ta e="T863" id="Seg_9834" s="T862">Mädchen.[NOM]</ta>
            <ta e="T864" id="Seg_9835" s="T863">jenes-ACC</ta>
            <ta e="T865" id="Seg_9836" s="T864">führen-CVB.SIM</ta>
            <ta e="T866" id="Seg_9837" s="T865">versuchen-PRS.[3SG]</ta>
            <ta e="T867" id="Seg_9838" s="T866">vorderstes.Rentier-3SG.[NOM]</ta>
            <ta e="T868" id="Seg_9839" s="T867">schneiden-CVB.SIM</ta>
            <ta e="T869" id="Seg_9840" s="T868">sich.bemühen-CVB.SEQ</ta>
            <ta e="T870" id="Seg_9841" s="T869">Ufer-DAT/LOC</ta>
            <ta e="T871" id="Seg_9842" s="T870">hinausgehen-PST1-3SG</ta>
            <ta e="T872" id="Seg_9843" s="T871">hinausgehen-CVB.ANT</ta>
            <ta e="T873" id="Seg_9844" s="T872">Junge-Hecht.[NOM]</ta>
            <ta e="T874" id="Seg_9845" s="T873">echt.[NOM]</ta>
            <ta e="T875" id="Seg_9846" s="T874">Junge.[NOM]</ta>
            <ta e="T876" id="Seg_9847" s="T875">werden-PST2.[3SG]</ta>
            <ta e="T877" id="Seg_9848" s="T876">Hecht-PL.[NOM]</ta>
            <ta e="T878" id="Seg_9849" s="T877">Rentier.[NOM]</ta>
            <ta e="T879" id="Seg_9850" s="T878">werden-PST1-3PL</ta>
            <ta e="T880" id="Seg_9851" s="T879">Mädchen-PROPR.[NOM]</ta>
            <ta e="T881" id="Seg_9852" s="T880">Junge.[NOM]</ta>
            <ta e="T882" id="Seg_9853" s="T881">erkennen-EP-RECP/COLL-PST1-3PL</ta>
            <ta e="T883" id="Seg_9854" s="T882">Mädchen.[NOM]</ta>
            <ta e="T884" id="Seg_9855" s="T883">fragen-PST1-3SG</ta>
            <ta e="T885" id="Seg_9856" s="T884">ältere.Schwester-2SG-ACC</ta>
            <ta e="T886" id="Seg_9857" s="T885">finden-PST1-2SG</ta>
            <ta e="T887" id="Seg_9858" s="T886">wie</ta>
            <ta e="T888" id="Seg_9859" s="T887">leben-PRS-2SG</ta>
            <ta e="T889" id="Seg_9860" s="T888">mitkommen.[IMP.2SG]</ta>
            <ta e="T890" id="Seg_9861" s="T889">1SG-ACC</ta>
            <ta e="T891" id="Seg_9862" s="T890">mit</ta>
            <ta e="T892" id="Seg_9863" s="T891">sagen-PST1-3SG</ta>
            <ta e="T893" id="Seg_9864" s="T892">Junge.[NOM]</ta>
            <ta e="T894" id="Seg_9865" s="T893">ältere.Schwester-EP-1SG.[NOM]</ta>
            <ta e="T895" id="Seg_9866" s="T894">2SG-ACC</ta>
            <ta e="T896" id="Seg_9867" s="T895">warten-PRS.[3SG]</ta>
            <ta e="T897" id="Seg_9868" s="T896">wissen-RECP/COLL-EP-RECP/COLL-EP-IMP.2PL</ta>
            <ta e="T898" id="Seg_9869" s="T897">zusammen</ta>
            <ta e="T899" id="Seg_9870" s="T898">leben-FUT-1PL</ta>
            <ta e="T900" id="Seg_9871" s="T899">Herde-1PL-ACC</ta>
            <ta e="T901" id="Seg_9872" s="T900">verbinden-FUT-1PL</ta>
            <ta e="T902" id="Seg_9873" s="T901">Junge-ACC</ta>
            <ta e="T903" id="Seg_9874" s="T902">mit</ta>
            <ta e="T904" id="Seg_9875" s="T903">Mädchen.[NOM]</ta>
            <ta e="T905" id="Seg_9876" s="T904">Hecht-PL-ACC</ta>
            <ta e="T906" id="Seg_9877" s="T905">Wasser.[NOM]</ta>
            <ta e="T907" id="Seg_9878" s="T906">Inneres-3SG-ABL</ta>
            <ta e="T908" id="Seg_9879" s="T907">herausholen-PST1-3PL</ta>
            <ta e="T909" id="Seg_9880" s="T908">jenes-3SG-3PL.[NOM]</ta>
            <ta e="T910" id="Seg_9881" s="T909">scheckig.[NOM]</ta>
            <ta e="T911" id="Seg_9882" s="T910">Rentier.[NOM]</ta>
            <ta e="T912" id="Seg_9883" s="T911">werden-PST1-3PL</ta>
            <ta e="T913" id="Seg_9884" s="T912">Junge.[NOM]</ta>
            <ta e="T914" id="Seg_9885" s="T913">ältere.Schwester-3SG.[NOM]</ta>
            <ta e="T915" id="Seg_9886" s="T914">sich.freuen-CVB.SEQ</ta>
            <ta e="T916" id="Seg_9887" s="T915">treffen-PST1-3SG</ta>
            <ta e="T917" id="Seg_9888" s="T916">Junge.[NOM]</ta>
            <ta e="T918" id="Seg_9889" s="T917">drei</ta>
            <ta e="T919" id="Seg_9890" s="T918">Tag-ACC</ta>
            <ta e="T920" id="Seg_9891" s="T919">schlafen-PST1-3SG</ta>
            <ta e="T921" id="Seg_9892" s="T920">aufwachen-CVB.SEQ</ta>
            <ta e="T922" id="Seg_9893" s="T921">sehen-PST2-3SG</ta>
            <ta e="T923" id="Seg_9894" s="T922">ältere.Schwester-3SG.[NOM]</ta>
            <ta e="T924" id="Seg_9895" s="T923">Mädchen-ACC</ta>
            <ta e="T925" id="Seg_9896" s="T924">mit</ta>
            <ta e="T926" id="Seg_9897" s="T925">sich.unterhalten-CVB.SIM-sich.unterhalten-CVB.SIM</ta>
            <ta e="T927" id="Seg_9898" s="T926">lachen-RECP/COLL-CVB.SIM</ta>
            <ta e="T928" id="Seg_9899" s="T927">sitzen-PRS-3PL</ta>
            <ta e="T929" id="Seg_9900" s="T928">Tee.[NOM]</ta>
            <ta e="T930" id="Seg_9901" s="T929">trinken-PRS-3PL</ta>
            <ta e="T931" id="Seg_9902" s="T930">kochen-PTCP.PST</ta>
            <ta e="T932" id="Seg_9903" s="T931">Fleisch-ACC</ta>
            <ta e="T933" id="Seg_9904" s="T932">essen-PRS-3PL</ta>
            <ta e="T934" id="Seg_9905" s="T933">Junge.[NOM]</ta>
            <ta e="T935" id="Seg_9906" s="T934">Nachbarschaft-3PL-DAT/LOC</ta>
            <ta e="T936" id="Seg_9907" s="T935">sich.setzen-PST1-3SG</ta>
            <ta e="T937" id="Seg_9908" s="T936">ganz</ta>
            <ta e="T938" id="Seg_9909" s="T937">Rentier-ACC</ta>
            <ta e="T939" id="Seg_9910" s="T938">essen-PST1-3SG</ta>
            <ta e="T940" id="Seg_9911" s="T939">Junge.[NOM]</ta>
            <ta e="T941" id="Seg_9912" s="T940">Mädchen-ACC</ta>
            <ta e="T942" id="Seg_9913" s="T941">Frau.[NOM]</ta>
            <ta e="T943" id="Seg_9914" s="T942">machen-PST2.[3SG]</ta>
            <ta e="T944" id="Seg_9915" s="T943">glücklich.[NOM]</ta>
            <ta e="T945" id="Seg_9916" s="T944">sehr-ADVZ</ta>
            <ta e="T946" id="Seg_9917" s="T945">leben-PST2-3PL</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_9918" s="T1">слушать-IMP.2PL</ta>
            <ta e="T3" id="Seg_9919" s="T2">долган-PL.[NOM]</ta>
            <ta e="T4" id="Seg_9920" s="T3">сказка-3PL-ACC</ta>
            <ta e="T5" id="Seg_9921" s="T4">старшая.сестра-PROPR.[NOM]</ta>
            <ta e="T6" id="Seg_9922" s="T5">мальчик.[NOM]</ta>
            <ta e="T7" id="Seg_9923" s="T6">говорить-CVB.SEQ</ta>
            <ta e="T8" id="Seg_9924" s="T7">имя-ADJZ.[NOM]</ta>
            <ta e="T9" id="Seg_9925" s="T8">мальчик.[NOM]</ta>
            <ta e="T10" id="Seg_9926" s="T9">старшая.сестра-3SG-ACC</ta>
            <ta e="T11" id="Seg_9927" s="T10">с</ta>
            <ta e="T12" id="Seg_9928" s="T11">вдвоем</ta>
            <ta e="T13" id="Seg_9929" s="T12">жить-PST2-3PL</ta>
            <ta e="T14" id="Seg_9930" s="T13">старшая.сестра-3SG.[NOM]</ta>
            <ta e="T15" id="Seg_9931" s="T14">работник-PROPR.[NOM]</ta>
            <ta e="T16" id="Seg_9932" s="T15">мастер.[NOM]</ta>
            <ta e="T17" id="Seg_9933" s="T16">очень</ta>
            <ta e="T18" id="Seg_9934" s="T17">нарта-PL-3SG.[NOM]</ta>
            <ta e="T19" id="Seg_9935" s="T18">пятнистый</ta>
            <ta e="T20" id="Seg_9936" s="T19">олень.[NOM]</ta>
            <ta e="T21" id="Seg_9937" s="T20">шкура-PL-EP-INSTR</ta>
            <ta e="T22" id="Seg_9938" s="T21">крышка-PROPR-3PL</ta>
            <ta e="T23" id="Seg_9939" s="T22">шест.[NOM]</ta>
            <ta e="T24" id="Seg_9940" s="T23">чум-ACC</ta>
            <ta e="T25" id="Seg_9941" s="T24">сам-3SG.[NOM]</ta>
            <ta e="T26" id="Seg_9942" s="T25">строить-MED-HAB.[3SG]</ta>
            <ta e="T27" id="Seg_9943" s="T26">олень-3PL-ACC</ta>
            <ta e="T28" id="Seg_9944" s="T27">EMPH</ta>
            <ta e="T29" id="Seg_9945" s="T28">летом</ta>
            <ta e="T30" id="Seg_9946" s="T29">охранять-HAB.[3SG]</ta>
            <ta e="T31" id="Seg_9947" s="T30">мальчик.[NOM]</ta>
            <ta e="T32" id="Seg_9948" s="T31">младший.брат-EP-3SG.[NOM]</ta>
            <ta e="T33" id="Seg_9949" s="T32">осень-2SG-ABL</ta>
            <ta e="T34" id="Seg_9950" s="T33">спать-PST2.[3SG]</ta>
            <ta e="T35" id="Seg_9951" s="T34">солнце.[NOM]</ta>
            <ta e="T36" id="Seg_9952" s="T35">выйти-PTCP.FUT.[3SG]-DAT/LOC</ta>
            <ta e="T37" id="Seg_9953" s="T36">пока</ta>
            <ta e="T38" id="Seg_9954" s="T37">стадо-3PL.[NOM]</ta>
            <ta e="T39" id="Seg_9955" s="T38">удаляться-PST2.[3SG]</ta>
            <ta e="T40" id="Seg_9956" s="T39">пастбище-3PL.[NOM]</ta>
            <ta e="T41" id="Seg_9957" s="T40">расширяться-PST2.[3SG]</ta>
            <ta e="T42" id="Seg_9958" s="T41">девушка.[NOM]</ta>
            <ta e="T43" id="Seg_9959" s="T42">пастбище.[NOM]</ta>
            <ta e="T44" id="Seg_9960" s="T43">сменить-CAP.[3SG]</ta>
            <ta e="T45" id="Seg_9961" s="T44">стадо-3SG-ACC</ta>
            <ta e="T46" id="Seg_9962" s="T45">принести-PST1-3SG</ta>
            <ta e="T47" id="Seg_9963" s="T46">олень.[NOM]</ta>
            <ta e="T48" id="Seg_9964" s="T47">поймать-PST1-3SG</ta>
            <ta e="T49" id="Seg_9965" s="T48">запрячь-PST1-3SG</ta>
            <ta e="T50" id="Seg_9966" s="T49">младший.брат-EP-3SG-ACC</ta>
            <ta e="T51" id="Seg_9967" s="T50">просыпаться-CAUS-CVB.SIM</ta>
            <ta e="T52" id="Seg_9968" s="T51">пытаться-PRS.[3SG]</ta>
            <ta e="T53" id="Seg_9969" s="T52">вообще</ta>
            <ta e="T54" id="Seg_9970" s="T53">просыпаться-EP-NEG.[3SG]</ta>
            <ta e="T55" id="Seg_9971" s="T54">шест.[NOM]</ta>
            <ta e="T56" id="Seg_9972" s="T55">чум-3SG-ACC</ta>
            <ta e="T57" id="Seg_9973" s="T56">стягивать-CVB.SEQ</ta>
            <ta e="T58" id="Seg_9974" s="T57">после</ta>
            <ta e="T59" id="Seg_9975" s="T58">младший.брат-EP-3SG-GEN</ta>
            <ta e="T60" id="Seg_9976" s="T59">песец.[NOM]</ta>
            <ta e="T61" id="Seg_9977" s="T60">одеяло-3SG-ACC</ta>
            <ta e="T62" id="Seg_9978" s="T61">совсем</ta>
            <ta e="T63" id="Seg_9979" s="T62">тянуть-PST1-3SG</ta>
            <ta e="T64" id="Seg_9980" s="T63">возбуждаться-CVB.SEQ</ta>
            <ta e="T65" id="Seg_9981" s="T64">просыпаться-PST1-3SG</ta>
            <ta e="T66" id="Seg_9982" s="T65">одеваться-PST1-3SG</ta>
            <ta e="T67" id="Seg_9983" s="T66">сани-3SG-DAT/LOC</ta>
            <ta e="T68" id="Seg_9984" s="T67">сесть-CVB.ANT</ta>
            <ta e="T69" id="Seg_9985" s="T68">опять</ta>
            <ta e="T70" id="Seg_9986" s="T69">уснуть-PST1-3SG</ta>
            <ta e="T71" id="Seg_9987" s="T70">дерево-VBZ-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T72" id="Seg_9988" s="T71">лед-VBZ-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T73" id="Seg_9989" s="T72">олень-VBZ-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T74" id="Seg_9990" s="T73">быть-CVB.SEQ</ta>
            <ta e="T75" id="Seg_9991" s="T74">девушка.[NOM]</ta>
            <ta e="T76" id="Seg_9992" s="T75">очень</ta>
            <ta e="T77" id="Seg_9993" s="T76">устать-PRS.[3SG]</ta>
            <ta e="T78" id="Seg_9994" s="T77">устать-CVB.SEQ</ta>
            <ta e="T79" id="Seg_9995" s="T78">уснуть-PST2.[3SG]</ta>
            <ta e="T80" id="Seg_9996" s="T79">спать-CVB.SIM</ta>
            <ta e="T81" id="Seg_9997" s="T80">лежать-CVB.SEQ</ta>
            <ta e="T82" id="Seg_9998" s="T81">мерзнуть-CVB.SEQ</ta>
            <ta e="T83" id="Seg_9999" s="T82">оставаться-PST2.[3SG]</ta>
            <ta e="T84" id="Seg_10000" s="T83">мальчик.[NOM]</ta>
            <ta e="T85" id="Seg_10001" s="T84">младший.брат-EP-3SG.[NOM]</ta>
            <ta e="T86" id="Seg_10002" s="T85">еще</ta>
            <ta e="T87" id="Seg_10003" s="T86">три</ta>
            <ta e="T88" id="Seg_10004" s="T87">день-ACC</ta>
            <ta e="T89" id="Seg_10005" s="T88">спать-PST2.[3SG]</ta>
            <ta e="T90" id="Seg_10006" s="T89">просыпаться-EP-PST2-3SG</ta>
            <ta e="T91" id="Seg_10007" s="T90">видеть-PST2-3SG</ta>
            <ta e="T92" id="Seg_10008" s="T91">старшая.сестра-3SG.[NOM]</ta>
            <ta e="T93" id="Seg_10009" s="T92">мерзнуть-CVB.SEQ</ta>
            <ta e="T94" id="Seg_10010" s="T93">оставаться-PST2.[3SG]</ta>
            <ta e="T95" id="Seg_10011" s="T94">стадо-3SG.[NOM]</ta>
            <ta e="T96" id="Seg_10012" s="T95">каждый-3SG.[NOM]</ta>
            <ta e="T97" id="Seg_10013" s="T96">мерзнуть-CVB.SEQ</ta>
            <ta e="T98" id="Seg_10014" s="T97">умирать-PST2.[3SG]</ta>
            <ta e="T99" id="Seg_10015" s="T98">сугроб.[NOM]</ta>
            <ta e="T100" id="Seg_10016" s="T99">бить-EP-PST2.[3SG]</ta>
            <ta e="T101" id="Seg_10017" s="T100">огонь.[NOM]</ta>
            <ta e="T102" id="Seg_10018" s="T101">топить-TEMP-1SG</ta>
            <ta e="T103" id="Seg_10019" s="T102">может</ta>
            <ta e="T104" id="Seg_10020" s="T103">старшая.сестра-EP-1SG.[NOM]</ta>
            <ta e="T105" id="Seg_10021" s="T104">оживать-FUT.[3SG]</ta>
            <ta e="T106" id="Seg_10022" s="T105">EMPH</ta>
            <ta e="T107" id="Seg_10023" s="T106">говорить-CVB.SIM</ta>
            <ta e="T108" id="Seg_10024" s="T107">думать-PST2.[3SG]</ta>
            <ta e="T109" id="Seg_10025" s="T108">мальчик.[NOM]</ta>
            <ta e="T110" id="Seg_10026" s="T109">дерево-VBZ-CVB.SIM</ta>
            <ta e="T111" id="Seg_10027" s="T110">идти-PTCP.FUT-DAT/LOC</ta>
            <ta e="T112" id="Seg_10028" s="T111">скала-PROPR.[NOM]</ta>
            <ta e="T113" id="Seg_10029" s="T112">река.[NOM]</ta>
            <ta e="T114" id="Seg_10030" s="T113">верхняя.часть-3SG-INSTR</ta>
            <ta e="T115" id="Seg_10031" s="T114">лес.[NOM]</ta>
            <ta e="T116" id="Seg_10032" s="T115">к</ta>
            <ta e="T117" id="Seg_10033" s="T116">бегать-PST2.[3SG]</ta>
            <ta e="T118" id="Seg_10034" s="T117">видеть-PST2-3SG</ta>
            <ta e="T119" id="Seg_10035" s="T118">небо-ABL</ta>
            <ta e="T120" id="Seg_10036" s="T119">%%</ta>
            <ta e="T121" id="Seg_10037" s="T120">костер.[NOM]</ta>
            <ta e="T122" id="Seg_10038" s="T121">пламенеть-CVB.SIM</ta>
            <ta e="T123" id="Seg_10039" s="T122">лежать-PRS.[3SG]</ta>
            <ta e="T124" id="Seg_10040" s="T123">злой.дух.[NOM]</ta>
            <ta e="T125" id="Seg_10041" s="T124">вертел-DAT/LOC</ta>
            <ta e="T126" id="Seg_10042" s="T125">мясо.[NOM]</ta>
            <ta e="T127" id="Seg_10043" s="T126">жарить-EP-MED-CVB.SIM</ta>
            <ta e="T128" id="Seg_10044" s="T127">сидеть-PRS.[3SG]</ta>
            <ta e="T129" id="Seg_10045" s="T128">мальчик-ACC</ta>
            <ta e="T130" id="Seg_10046" s="T129">видеть-NEG.[3SG]</ta>
            <ta e="T131" id="Seg_10047" s="T130">этот</ta>
            <ta e="T132" id="Seg_10048" s="T131">мальчик.[NOM]</ta>
            <ta e="T133" id="Seg_10049" s="T132">сползать-CAUS-CVB.SIM</ta>
            <ta e="T134" id="Seg_10050" s="T133">трогать-CVB.SIM-трогать-CVB.SIM</ta>
            <ta e="T135" id="Seg_10051" s="T134">на.том.берегу-2SG-ABL</ta>
            <ta e="T136" id="Seg_10052" s="T135">есть-CVB.SIM</ta>
            <ta e="T137" id="Seg_10053" s="T136">сидеть-PST2.[3SG]</ta>
            <ta e="T138" id="Seg_10054" s="T137">как</ta>
            <ta e="T139" id="Seg_10055" s="T138">опомниться-NEG.PTCP-3SG-ACC</ta>
            <ta e="T140" id="Seg_10056" s="T139">вот</ta>
            <ta e="T141" id="Seg_10057" s="T140">думать-PRS.[3SG]</ta>
            <ta e="T142" id="Seg_10058" s="T141">злой.дух.[NOM]</ta>
            <ta e="T143" id="Seg_10059" s="T142">мясо-PL-EP-1SG.[NOM]</ta>
            <ta e="T144" id="Seg_10060" s="T143">огонь-DAT/LOC</ta>
            <ta e="T145" id="Seg_10061" s="T144">падать-PTCP.PRS-3PL-ACC</ta>
            <ta e="T146" id="Seg_10062" s="T145">злой.дух.[NOM]</ta>
            <ta e="T147" id="Seg_10063" s="T146">мальчик-ACC</ta>
            <ta e="T148" id="Seg_10064" s="T147">видеть-CVB.SEQ</ta>
            <ta e="T149" id="Seg_10065" s="T148">убить-CVB.PURP</ta>
            <ta e="T150" id="Seg_10066" s="T149">хотеть-PST2.[3SG]</ta>
            <ta e="T151" id="Seg_10067" s="T150">мальчик.[NOM]</ta>
            <ta e="T152" id="Seg_10068" s="T151">рукавица-3SG-INSTR</ta>
            <ta e="T153" id="Seg_10069" s="T152">злой.дух.[NOM]</ta>
            <ta e="T154" id="Seg_10070" s="T153">восемь</ta>
            <ta e="T155" id="Seg_10071" s="T154">голова-3SG-ACC</ta>
            <ta e="T156" id="Seg_10072" s="T155">совсем</ta>
            <ta e="T157" id="Seg_10073" s="T156">бить-EP-PST2.[3SG]</ta>
            <ta e="T158" id="Seg_10074" s="T157">голова-PL-3SG.[NOM]</ta>
            <ta e="T159" id="Seg_10075" s="T158">плясать-CVB.SIM</ta>
            <ta e="T160" id="Seg_10076" s="T159">идти-PRS-3PL</ta>
            <ta e="T161" id="Seg_10077" s="T160">мальчик.[NOM]</ta>
            <ta e="T162" id="Seg_10078" s="T161">там</ta>
            <ta e="T163" id="Seg_10079" s="T162">вскрикивать-PST2.[3SG]</ta>
            <ta e="T164" id="Seg_10080" s="T163">богатырь.[NOM]</ta>
            <ta e="T165" id="Seg_10081" s="T164">два-VBZ-PTCP.HAB-3SG</ta>
            <ta e="T166" id="Seg_10082" s="T165">три-ORD-VBZ-PTCP.HAB-3SG</ta>
            <ta e="T167" id="Seg_10083" s="T166">NEG.[3SG]</ta>
            <ta e="T168" id="Seg_10084" s="T167">злой.дух.[NOM]</ta>
            <ta e="T169" id="Seg_10085" s="T168">грудь-3SG-ACC</ta>
            <ta e="T170" id="Seg_10086" s="T169">пороть-CVB.SIM</ta>
            <ta e="T171" id="Seg_10087" s="T170">тянуть-CVB.SEQ</ta>
            <ta e="T172" id="Seg_10088" s="T171">камень-ACC</ta>
            <ta e="T173" id="Seg_10089" s="T172">вытащить-PST1-3SG</ta>
            <ta e="T174" id="Seg_10090" s="T173">камень.[NOM]</ta>
            <ta e="T175" id="Seg_10091" s="T174">сердце-PROPR.[NOM]</ta>
            <ta e="T176" id="Seg_10092" s="T175">быть-PST2.[3SG]</ta>
            <ta e="T177" id="Seg_10093" s="T176">мальчик.[NOM]</ta>
            <ta e="T178" id="Seg_10094" s="T177">камень-ACC</ta>
            <ta e="T179" id="Seg_10095" s="T178">пнуть-CVB.SIM-пнуть-CVB.SIM</ta>
            <ta e="T180" id="Seg_10096" s="T179">идти-CVB.SIM</ta>
            <ta e="T181" id="Seg_10097" s="T180">стоять-PST2.[3SG]</ta>
            <ta e="T182" id="Seg_10098" s="T181">передняя.часть-3SG-DAT/LOC</ta>
            <ta e="T183" id="Seg_10099" s="T182">земля.[NOM]</ta>
            <ta e="T184" id="Seg_10100" s="T183">половина-PROPR.[NOM]</ta>
            <ta e="T185" id="Seg_10101" s="T184">ростом.с</ta>
            <ta e="T186" id="Seg_10102" s="T185">камень.[NOM]</ta>
            <ta e="T187" id="Seg_10103" s="T186">вставать-PST2.[3SG]</ta>
            <ta e="T188" id="Seg_10104" s="T187">камень.[NOM]</ta>
            <ta e="T189" id="Seg_10105" s="T188">гора.[NOM]</ta>
            <ta e="T190" id="Seg_10106" s="T189">камень.[NOM]</ta>
            <ta e="T191" id="Seg_10107" s="T190">гора-ACC</ta>
            <ta e="T192" id="Seg_10108" s="T191">камень.[NOM]</ta>
            <ta e="T193" id="Seg_10109" s="T192">сердце-3SG-INSTR</ta>
            <ta e="T194" id="Seg_10110" s="T193">пнуть-PST2.[3SG]</ta>
            <ta e="T195" id="Seg_10111" s="T194">гора-3SG.[NOM]</ta>
            <ta e="T196" id="Seg_10112" s="T195">два</ta>
            <ta e="T197" id="Seg_10113" s="T196">сторона.[NOM]</ta>
            <ta e="T198" id="Seg_10114" s="T197">разорвать-CVB.SEQ</ta>
            <ta e="T199" id="Seg_10115" s="T198">падать-PST2.[3SG]</ta>
            <ta e="T200" id="Seg_10116" s="T199">смотреть-CVB.SEQ</ta>
            <ta e="T201" id="Seg_10117" s="T200">видеть-PST2-3SG</ta>
            <ta e="T202" id="Seg_10118" s="T201">земля.[NOM]</ta>
            <ta e="T203" id="Seg_10119" s="T202">быть.видно-PRS.[3SG]</ta>
            <ta e="T204" id="Seg_10120" s="T203">туда</ta>
            <ta e="T205" id="Seg_10121" s="T204">идти-CVB.SEQ</ta>
            <ta e="T206" id="Seg_10122" s="T205">сани-PROPR.[NOM]</ta>
            <ta e="T207" id="Seg_10123" s="T206">след-3SG-ACC</ta>
            <ta e="T208" id="Seg_10124" s="T207">видеть-PST2.[3SG]</ta>
            <ta e="T209" id="Seg_10125" s="T208">пасть.[NOM]</ta>
            <ta e="T210" id="Seg_10126" s="T209">капкан.[NOM]</ta>
            <ta e="T211" id="Seg_10127" s="T210">видеть-EP-MED-CVB.SIM</ta>
            <ta e="T212" id="Seg_10128" s="T211">идти-PTCP.PRS</ta>
            <ta e="T213" id="Seg_10129" s="T212">человек.[NOM]</ta>
            <ta e="T214" id="Seg_10130" s="T213">быть-PST1-3SG</ta>
            <ta e="T215" id="Seg_10131" s="T214">быть-PST2.[3SG]</ta>
            <ta e="T216" id="Seg_10132" s="T215">сколько</ta>
            <ta e="T217" id="Seg_10133" s="T216">NEG</ta>
            <ta e="T218" id="Seg_10134" s="T217">быть-NEG.CVB.SIM</ta>
            <ta e="T219" id="Seg_10135" s="T218">песец-AG.[NOM]</ta>
            <ta e="T220" id="Seg_10136" s="T219">навстречу</ta>
            <ta e="T221" id="Seg_10137" s="T220">идти-PTCP.PRS-3SG-ACC</ta>
            <ta e="T222" id="Seg_10138" s="T221">видеть-PST2.[3SG]</ta>
            <ta e="T223" id="Seg_10139" s="T222">приходить-CVB.SEQ</ta>
            <ta e="T224" id="Seg_10140" s="T223">стоять-PST1-3SG</ta>
            <ta e="T225" id="Seg_10141" s="T224">здороваться-PST1-3SG</ta>
            <ta e="T226" id="Seg_10142" s="T225">что-ACC</ta>
            <ta e="T227" id="Seg_10143" s="T226">делать-CVB.SIM</ta>
            <ta e="T228" id="Seg_10144" s="T227">идти-PRS-2SG</ta>
            <ta e="T229" id="Seg_10145" s="T228">спрашивать-PST2.[3SG]</ta>
            <ta e="T230" id="Seg_10146" s="T229">мальчик.[NOM]</ta>
            <ta e="T231" id="Seg_10147" s="T230">пасть.[NOM]</ta>
            <ta e="T232" id="Seg_10148" s="T231">капкан.[NOM]</ta>
            <ta e="T233" id="Seg_10149" s="T232">видеть-EP-MED-PRS-1SG</ta>
            <ta e="T234" id="Seg_10150" s="T233">далеко</ta>
            <ta e="T235" id="Seg_10151" s="T234">дом-PROPR-2PL</ta>
            <ta e="T236" id="Seg_10152" s="T235">Q</ta>
            <ta e="T237" id="Seg_10153" s="T236">сколько</ta>
            <ta e="T238" id="Seg_10154" s="T237">человек-2PL=Q</ta>
            <ta e="T239" id="Seg_10155" s="T238">мать-1SG-ACC</ta>
            <ta e="T240" id="Seg_10156" s="T239">отец-1SG-ACC</ta>
            <ta e="T241" id="Seg_10157" s="T240">с</ta>
            <ta e="T242" id="Seg_10158" s="T241">жить-PRS-1SG</ta>
            <ta e="T243" id="Seg_10159" s="T242">говорить-PST2.[3SG]</ta>
            <ta e="T244" id="Seg_10160" s="T243">мальчик.[NOM]</ta>
            <ta e="T245" id="Seg_10161" s="T244">скакать.[IMP.2SG]</ta>
            <ta e="T246" id="Seg_10162" s="T245">уносить-FUT-1SG</ta>
            <ta e="T247" id="Seg_10163" s="T246">гость.[NOM]</ta>
            <ta e="T248" id="Seg_10164" s="T247">быть.[IMP.2SG]</ta>
            <ta e="T249" id="Seg_10165" s="T248">шест.[NOM]</ta>
            <ta e="T250" id="Seg_10166" s="T249">чум-3SG-DAT/LOC</ta>
            <ta e="T251" id="Seg_10167" s="T250">принести-PST2.[3SG]</ta>
            <ta e="T252" id="Seg_10168" s="T251">старик-PROPR</ta>
            <ta e="T253" id="Seg_10169" s="T252">старуха.[NOM]</ta>
            <ta e="T254" id="Seg_10170" s="T253">навстречу</ta>
            <ta e="T255" id="Seg_10171" s="T254">выйти-CVB.SEQ-3PL</ta>
            <ta e="T256" id="Seg_10172" s="T255">встречать-EP-PST2-3PL</ta>
            <ta e="T257" id="Seg_10173" s="T256">эй</ta>
            <ta e="T258" id="Seg_10174" s="T257">место-ABL</ta>
            <ta e="T259" id="Seg_10175" s="T258">приходить-PST1-2SG</ta>
            <ta e="T260" id="Seg_10176" s="T259">спрашивать-PST2-3PL</ta>
            <ta e="T261" id="Seg_10177" s="T260">сюда</ta>
            <ta e="T262" id="Seg_10178" s="T261">поперек</ta>
            <ta e="T263" id="Seg_10179" s="T262">глаз-PROPR.[NOM]</ta>
            <ta e="T264" id="Seg_10180" s="T263">огонь.[NOM]</ta>
            <ta e="T265" id="Seg_10181" s="T264">подошва-PROPR.[NOM]</ta>
            <ta e="T266" id="Seg_10182" s="T265">идти-EP-MED-CVB.SIM=еще.не.[3SG]</ta>
            <ta e="T267" id="Seg_10183" s="T266">мальчик.[NOM]</ta>
            <ta e="T268" id="Seg_10184" s="T267">рассказывать-PST2.[3SG]</ta>
            <ta e="T269" id="Seg_10185" s="T268">старшая.сестра-EP-1SG.[NOM]</ta>
            <ta e="T270" id="Seg_10186" s="T269">мерзнуть-CVB.SEQ</ta>
            <ta e="T271" id="Seg_10187" s="T270">умирать-PST1-3SG</ta>
            <ta e="T272" id="Seg_10188" s="T271">олень-PL-1PL.[NOM]</ta>
            <ta e="T273" id="Seg_10189" s="T272">тоже</ta>
            <ta e="T274" id="Seg_10190" s="T273">мерзнуть-CVB.SEQ</ta>
            <ta e="T275" id="Seg_10191" s="T274">умирать-PST1-3PL</ta>
            <ta e="T276" id="Seg_10192" s="T275">снег.[NOM]</ta>
            <ta e="T277" id="Seg_10193" s="T276">нутро-3SG-DAT/LOC</ta>
            <ta e="T278" id="Seg_10194" s="T277">лежать-PRS-3PL</ta>
            <ta e="T279" id="Seg_10195" s="T278">замести-EP-PASS/REFL-CVB.SIM</ta>
            <ta e="T280" id="Seg_10196" s="T279">как</ta>
            <ta e="T281" id="Seg_10197" s="T280">человек.[NOM]</ta>
            <ta e="T282" id="Seg_10198" s="T281">быть-FUT-1SG</ta>
            <ta e="T283" id="Seg_10199" s="T282">Q</ta>
            <ta e="T284" id="Seg_10200" s="T283">сначала</ta>
            <ta e="T285" id="Seg_10201" s="T284">гость.[NOM]</ta>
            <ta e="T286" id="Seg_10202" s="T285">быть.[IMP.2SG]</ta>
            <ta e="T287" id="Seg_10203" s="T286">говорить-PST2.[3SG]</ta>
            <ta e="T288" id="Seg_10204" s="T287">старик.[NOM]</ta>
            <ta e="T289" id="Seg_10205" s="T288">потом</ta>
            <ta e="T290" id="Seg_10206" s="T289">советовать-EP-RECP/COLL-FUT-1PL</ta>
            <ta e="T291" id="Seg_10207" s="T290">сын-3SG.[NOM]</ta>
            <ta e="T292" id="Seg_10208" s="T291">выйти-CVB.SEQ</ta>
            <ta e="T293" id="Seg_10209" s="T292">важенка-ACC</ta>
            <ta e="T294" id="Seg_10210" s="T293">убить-PST2.[3SG]</ta>
            <ta e="T295" id="Seg_10211" s="T294">край-PROPR.[NOM]</ta>
            <ta e="T296" id="Seg_10212" s="T295">важенка-ACC</ta>
            <ta e="T297" id="Seg_10213" s="T296">гость.[NOM]</ta>
            <ta e="T298" id="Seg_10214" s="T297">тот-ACC</ta>
            <ta e="T299" id="Seg_10215" s="T298">одиноко</ta>
            <ta e="T300" id="Seg_10216" s="T299">есть-PST2.[3SG]</ta>
            <ta e="T301" id="Seg_10217" s="T300">наесться-PST1-2SG</ta>
            <ta e="T302" id="Seg_10218" s="T301">спрашивать-PST2-3PL</ta>
            <ta e="T303" id="Seg_10219" s="T302">наесться-PST1-1SG</ta>
            <ta e="T304" id="Seg_10220" s="T303">осень-2SG-ABL</ta>
            <ta e="T305" id="Seg_10221" s="T304">есть-CVB.SIM</ta>
            <ta e="T306" id="Seg_10222" s="T305">еще.не-1SG</ta>
            <ta e="T307" id="Seg_10223" s="T306">теперь</ta>
            <ta e="T308" id="Seg_10224" s="T307">отдыхать-FUT-1SG</ta>
            <ta e="T309" id="Seg_10225" s="T308">три</ta>
            <ta e="T310" id="Seg_10226" s="T309">день-ACC</ta>
            <ta e="T311" id="Seg_10227" s="T310">мальчик.[NOM]</ta>
            <ta e="T312" id="Seg_10228" s="T311">спать-PST2.[3SG]</ta>
            <ta e="T313" id="Seg_10229" s="T312">просыпаться-EP-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T314" id="Seg_10230" s="T313">старик.[NOM]</ta>
            <ta e="T315" id="Seg_10231" s="T314">самый</ta>
            <ta e="T316" id="Seg_10232" s="T315">жирный.[NOM]</ta>
            <ta e="T317" id="Seg_10233" s="T316">олень-ACC</ta>
            <ta e="T318" id="Seg_10234" s="T317">убить-CVB.SEQ</ta>
            <ta e="T319" id="Seg_10235" s="T318">есть-DRV-PST2.[3SG]</ta>
            <ta e="T320" id="Seg_10236" s="T319">старик.[NOM]</ta>
            <ta e="T321" id="Seg_10237" s="T320">три</ta>
            <ta e="T322" id="Seg_10238" s="T321">месяц-ADJZ.[NOM]</ta>
            <ta e="T323" id="Seg_10239" s="T322">пища-ACC</ta>
            <ta e="T324" id="Seg_10240" s="T323">готовый-VBZ-PST2.[3SG]</ta>
            <ta e="T325" id="Seg_10241" s="T324">идти-PTCP.FUT-3SG-ACC</ta>
            <ta e="T326" id="Seg_10242" s="T325">мальчик.[NOM]</ta>
            <ta e="T327" id="Seg_10243" s="T326">говорить-PST2.[3SG]</ta>
            <ta e="T328" id="Seg_10244" s="T327">где</ta>
            <ta e="T329" id="Seg_10245" s="T328">INDEF</ta>
            <ta e="T330" id="Seg_10246" s="T329">земля.[NOM]</ta>
            <ta e="T331" id="Seg_10247" s="T330">дух-3SG.[NOM]</ta>
            <ta e="T332" id="Seg_10248" s="T331">есть</ta>
            <ta e="T333" id="Seg_10249" s="T332">говорят</ta>
            <ta e="T334" id="Seg_10250" s="T333">туда</ta>
            <ta e="T335" id="Seg_10251" s="T334">идти-FUT-1SG</ta>
            <ta e="T336" id="Seg_10252" s="T335">мальчик.[NOM]</ta>
            <ta e="T337" id="Seg_10253" s="T336">сани-3SG-DAT/LOC</ta>
            <ta e="T338" id="Seg_10254" s="T337">камень.[NOM]</ta>
            <ta e="T339" id="Seg_10255" s="T338">сердце-ACC</ta>
            <ta e="T340" id="Seg_10256" s="T339">класть-PST1-3SG</ta>
            <ta e="T341" id="Seg_10257" s="T340">река.[NOM]</ta>
            <ta e="T342" id="Seg_10258" s="T341">нутро-3SG-INSTR</ta>
            <ta e="T343" id="Seg_10259" s="T342">отправляться-CVB.SEQ</ta>
            <ta e="T344" id="Seg_10260" s="T343">оставаться-PST1-3SG</ta>
            <ta e="T345" id="Seg_10261" s="T344">сколько</ta>
            <ta e="T346" id="Seg_10262" s="T345">долго-ADVZ</ta>
            <ta e="T347" id="Seg_10263" s="T346">идти-PST2-3SG</ta>
            <ta e="T348" id="Seg_10264" s="T347">Q</ta>
            <ta e="T349" id="Seg_10265" s="T348">только</ta>
            <ta e="T350" id="Seg_10266" s="T349">чугун.[NOM]</ta>
            <ta e="T351" id="Seg_10267" s="T350">шест.[NOM]</ta>
            <ta e="T352" id="Seg_10268" s="T351">чум-ACC</ta>
            <ta e="T353" id="Seg_10269" s="T352">видеть-PST2.[3SG]</ta>
            <ta e="T354" id="Seg_10270" s="T353">дверь-ACC</ta>
            <ta e="T355" id="Seg_10271" s="T354">открывать-CVB.SEQ</ta>
            <ta e="T356" id="Seg_10272" s="T355">видеть-PST1-3SG</ta>
            <ta e="T357" id="Seg_10273" s="T356">старый.[NOM]</ta>
            <ta e="T358" id="Seg_10274" s="T357">очень</ta>
            <ta e="T359" id="Seg_10275" s="T358">старуха.[NOM]</ta>
            <ta e="T360" id="Seg_10276" s="T359">сидеть-PRS.[3SG]</ta>
            <ta e="T361" id="Seg_10277" s="T360">место.около-3SG-DAT/LOC</ta>
            <ta e="T362" id="Seg_10278" s="T361">мех.[NOM]</ta>
            <ta e="T363" id="Seg_10279" s="T362">лежать-PRS.[3SG]</ta>
            <ta e="T364" id="Seg_10280" s="T363">голова-3SG-GEN</ta>
            <ta e="T365" id="Seg_10281" s="T364">верхняя.часть-3SG-DAT/LOC</ta>
            <ta e="T366" id="Seg_10282" s="T365">сума.[NOM]</ta>
            <ta e="T367" id="Seg_10283" s="T366">повесить-PASS/REFL-EP-PST2.[3SG]</ta>
            <ta e="T368" id="Seg_10284" s="T367">сума.[NOM]</ta>
            <ta e="T369" id="Seg_10285" s="T368">человек.[NOM]</ta>
            <ta e="T370" id="Seg_10286" s="T369">лицо-3SG-ACC</ta>
            <ta e="T371" id="Seg_10287" s="T370">похожий.на</ta>
            <ta e="T372" id="Seg_10288" s="T371">очаг.[NOM]</ta>
            <ta e="T373" id="Seg_10289" s="T372">нижняя.часть-EP-3SG.[NOM]</ta>
            <ta e="T374" id="Seg_10290" s="T373">полый.[NOM]</ta>
            <ta e="T375" id="Seg_10291" s="T374">река.[NOM]</ta>
            <ta e="T376" id="Seg_10292" s="T375">течь-CVB.SIM</ta>
            <ta e="T377" id="Seg_10293" s="T376">лежать-PRS.[3SG]</ta>
            <ta e="T378" id="Seg_10294" s="T377">река-DAT/LOC</ta>
            <ta e="T379" id="Seg_10295" s="T378">лодочка-PROPR.[NOM]</ta>
            <ta e="T380" id="Seg_10296" s="T379">сеть-VBZ-CVB.SIM</ta>
            <ta e="T381" id="Seg_10297" s="T380">идти-PRS.[3SG]</ta>
            <ta e="T382" id="Seg_10298" s="T381">сума.[NOM]</ta>
            <ta e="T383" id="Seg_10299" s="T382">язык-PROPR.[NOM]</ta>
            <ta e="T384" id="Seg_10300" s="T383">становиться-PST2.[3SG]</ta>
            <ta e="T385" id="Seg_10301" s="T384">гость.[NOM]</ta>
            <ta e="T386" id="Seg_10302" s="T385">приходить-PST1-3SG</ta>
            <ta e="T387" id="Seg_10303" s="T386">дерево-AG-PL.[NOM]</ta>
            <ta e="T388" id="Seg_10304" s="T387">дерево-VBZ-IMP.2PL</ta>
            <ta e="T389" id="Seg_10305" s="T388">огонь-PART</ta>
            <ta e="T390" id="Seg_10306" s="T389">зажигать-EP-IMP.2PL</ta>
            <ta e="T391" id="Seg_10307" s="T390">дерево-3PL.[NOM]</ta>
            <ta e="T392" id="Seg_10308" s="T391">сам-3PL.[NOM]</ta>
            <ta e="T393" id="Seg_10309" s="T392">входить-PST1-3PL</ta>
            <ta e="T394" id="Seg_10310" s="T393">с.улицы</ta>
            <ta e="T395" id="Seg_10311" s="T394">мальчик.[NOM]</ta>
            <ta e="T396" id="Seg_10312" s="T395">понимать-PST1-3SG</ta>
            <ta e="T397" id="Seg_10313" s="T396">железный.[NOM]</ta>
            <ta e="T398" id="Seg_10314" s="T397">дерево-PL.[NOM]</ta>
            <ta e="T399" id="Seg_10315" s="T398">быть-PST2.[3SG]</ta>
            <ta e="T400" id="Seg_10316" s="T399">старуха.[NOM]</ta>
            <ta e="T401" id="Seg_10317" s="T400">вставать-CVB.SEQ</ta>
            <ta e="T402" id="Seg_10318" s="T401">дерево-PL-ACC</ta>
            <ta e="T403" id="Seg_10319" s="T402">очаг-3SG-DAT/LOC</ta>
            <ta e="T404" id="Seg_10320" s="T403">собирать-PST1-3SG</ta>
            <ta e="T405" id="Seg_10321" s="T404">сума.[NOM]</ta>
            <ta e="T406" id="Seg_10322" s="T405">говорить-PST1-3SG</ta>
            <ta e="T407" id="Seg_10323" s="T406">мех.[NOM]</ta>
            <ta e="T408" id="Seg_10324" s="T407">мех-VBZ.[IMP.2SG]</ta>
            <ta e="T409" id="Seg_10325" s="T408">шест.[NOM]</ta>
            <ta e="T410" id="Seg_10326" s="T409">чум.[NOM]</ta>
            <ta e="T411" id="Seg_10327" s="T410">железный</ta>
            <ta e="T412" id="Seg_10328" s="T411">пол-3SG.[NOM]</ta>
            <ta e="T413" id="Seg_10329" s="T412">краснеть-INCH</ta>
            <ta e="T414" id="Seg_10330" s="T413">идти-PST1-3SG</ta>
            <ta e="T415" id="Seg_10331" s="T414">сума.[NOM]</ta>
            <ta e="T416" id="Seg_10332" s="T415">всегда</ta>
            <ta e="T417" id="Seg_10333" s="T416">говорить-PRS.[3SG]</ta>
            <ta e="T418" id="Seg_10334" s="T417">мех.[NOM]</ta>
            <ta e="T419" id="Seg_10335" s="T418">мех-VBZ.[IMP.2SG]</ta>
            <ta e="T420" id="Seg_10336" s="T419">мех-VBZ.[IMP.2SG]</ta>
            <ta e="T421" id="Seg_10337" s="T420">гость.[NOM]</ta>
            <ta e="T422" id="Seg_10338" s="T421">приходить-PST1-3SG</ta>
            <ta e="T423" id="Seg_10339" s="T422">огонь-PART</ta>
            <ta e="T424" id="Seg_10340" s="T423">добавлять.[IMP.2SG]</ta>
            <ta e="T425" id="Seg_10341" s="T424">шест.[NOM]</ta>
            <ta e="T426" id="Seg_10342" s="T425">чум.[NOM]</ta>
            <ta e="T427" id="Seg_10343" s="T426">каждый-3SG.[NOM]</ta>
            <ta e="T428" id="Seg_10344" s="T427">краснеть-PST1-3SG</ta>
            <ta e="T429" id="Seg_10345" s="T428">старуха.[NOM]</ta>
            <ta e="T430" id="Seg_10346" s="T429">гость.[NOM]</ta>
            <ta e="T431" id="Seg_10347" s="T430">мальчик-ACC</ta>
            <ta e="T432" id="Seg_10348" s="T431">просить-PST1-3SG</ta>
            <ta e="T433" id="Seg_10349" s="T432">согреться-CAUS-EP-NEG.[IMP.2SG]</ta>
            <ta e="T434" id="Seg_10350" s="T433">очень</ta>
            <ta e="T435" id="Seg_10351" s="T434">охлаждать-EP-CAUS.[IMP.2SG]</ta>
            <ta e="T436" id="Seg_10352" s="T435">вот</ta>
            <ta e="T437" id="Seg_10353" s="T436">мальчик.[NOM]</ta>
            <ta e="T438" id="Seg_10354" s="T437">камень.[NOM]</ta>
            <ta e="T439" id="Seg_10355" s="T438">сердце-ACC</ta>
            <ta e="T440" id="Seg_10356" s="T439">взять-CVB.SEQ</ta>
            <ta e="T441" id="Seg_10357" s="T440">очаг.[NOM]</ta>
            <ta e="T442" id="Seg_10358" s="T441">середина-3SG-DAT/LOC</ta>
            <ta e="T443" id="Seg_10359" s="T442">бросать-PST1-3SG</ta>
            <ta e="T444" id="Seg_10360" s="T443">шест.[NOM]</ta>
            <ta e="T445" id="Seg_10361" s="T444">чум.[NOM]</ta>
            <ta e="T446" id="Seg_10362" s="T445">охлаждать-PST1-3SG</ta>
            <ta e="T447" id="Seg_10363" s="T446">пол-3SG.[NOM]</ta>
            <ta e="T448" id="Seg_10364" s="T447">краснеть-NEG-PST1-3SG</ta>
            <ta e="T449" id="Seg_10365" s="T448">старуха.[NOM]</ta>
            <ta e="T450" id="Seg_10366" s="T449">опять</ta>
            <ta e="T451" id="Seg_10367" s="T450">крепко</ta>
            <ta e="T452" id="Seg_10368" s="T451">иней.[NOM]</ta>
            <ta e="T453" id="Seg_10369" s="T452">быть-PST1-3SG</ta>
            <ta e="T454" id="Seg_10370" s="T453">очень</ta>
            <ta e="T455" id="Seg_10371" s="T454">мерзнуть-EP-CAUS-EP-NEG.[IMP.2SG]</ta>
            <ta e="T456" id="Seg_10372" s="T455">просить-PST2.[3SG]</ta>
            <ta e="T0" id="Seg_10373" s="T456">сума.[NOM]</ta>
            <ta e="T947" id="Seg_10374" s="T0">каждый.[NOM]</ta>
            <ta e="T948" id="Seg_10375" s="T947">желание-2SG-ACC</ta>
            <ta e="T457" id="Seg_10376" s="T948">наполнять-FUT-1SG</ta>
            <ta e="T458" id="Seg_10377" s="T457">сума.[NOM]</ta>
            <ta e="T459" id="Seg_10378" s="T458">быть-PST2.[3SG]</ta>
            <ta e="T460" id="Seg_10379" s="T459">земля.[NOM]</ta>
            <ta e="T461" id="Seg_10380" s="T460">господин-3SG.[NOM]</ta>
            <ta e="T462" id="Seg_10381" s="T461">мальчик.[NOM]</ta>
            <ta e="T463" id="Seg_10382" s="T462">камень.[NOM]</ta>
            <ta e="T464" id="Seg_10383" s="T463">сердце-ACC</ta>
            <ta e="T465" id="Seg_10384" s="T464">очаг.[NOM]</ta>
            <ta e="T466" id="Seg_10385" s="T465">нутро-3SG-ABL</ta>
            <ta e="T467" id="Seg_10386" s="T466">вытащить-CVB.SEQ</ta>
            <ta e="T468" id="Seg_10387" s="T467">взять-PST1-3SG</ta>
            <ta e="T469" id="Seg_10388" s="T468">огонь-3SG.[NOM]</ta>
            <ta e="T470" id="Seg_10389" s="T469">опять</ta>
            <ta e="T471" id="Seg_10390" s="T470">пламенеть-PST1-3SG</ta>
            <ta e="T472" id="Seg_10391" s="T471">сума.[NOM]</ta>
            <ta e="T473" id="Seg_10392" s="T472">говорить-PST1-3SG</ta>
            <ta e="T474" id="Seg_10393" s="T473">мальчик-DAT/LOC</ta>
            <ta e="T475" id="Seg_10394" s="T474">на.улицу</ta>
            <ta e="T476" id="Seg_10395" s="T475">выйти.[IMP.2SG]</ta>
            <ta e="T477" id="Seg_10396" s="T476">там</ta>
            <ta e="T478" id="Seg_10397" s="T477">сани-PROPR</ta>
            <ta e="T479" id="Seg_10398" s="T478">олень.[NOM]</ta>
            <ta e="T480" id="Seg_10399" s="T479">стоять-PRS.[3SG]</ta>
            <ta e="T481" id="Seg_10400" s="T480">мальчик.[NOM]</ta>
            <ta e="T482" id="Seg_10401" s="T481">на.улицу</ta>
            <ta e="T483" id="Seg_10402" s="T482">выйти-CVB.SEQ</ta>
            <ta e="T484" id="Seg_10403" s="T483">что.[NOM]</ta>
            <ta e="T485" id="Seg_10404" s="T484">NEG</ta>
            <ta e="T486" id="Seg_10405" s="T485">сани-PROPR</ta>
            <ta e="T487" id="Seg_10406" s="T486">олень-3SG-ACC</ta>
            <ta e="T488" id="Seg_10407" s="T487">видеть-NEG.[3SG]</ta>
            <ta e="T489" id="Seg_10408" s="T488">где</ta>
            <ta e="T490" id="Seg_10409" s="T489">INDEF</ta>
            <ta e="T491" id="Seg_10410" s="T490">бубенчик.[NOM]</ta>
            <ta e="T492" id="Seg_10411" s="T491">железная.подвеска.к.упряжке.[NOM]</ta>
            <ta e="T493" id="Seg_10412" s="T492">только</ta>
            <ta e="T494" id="Seg_10413" s="T493">звук-3SG.[NOM]</ta>
            <ta e="T495" id="Seg_10414" s="T494">слышаться-PRS.[3SG]</ta>
            <ta e="T496" id="Seg_10415" s="T495">затылок-3SG-ACC</ta>
            <ta e="T497" id="Seg_10416" s="T496">к</ta>
            <ta e="T498" id="Seg_10417" s="T497">сума.[NOM]</ta>
            <ta e="T499" id="Seg_10418" s="T498">голос-3SG.[NOM]</ta>
            <ta e="T500" id="Seg_10419" s="T499">слышаться-EP-PST2.[3SG]</ta>
            <ta e="T501" id="Seg_10420" s="T500">сесть.[IMP.2SG]</ta>
            <ta e="T502" id="Seg_10421" s="T501">сани-2SG-DAT/LOC</ta>
            <ta e="T503" id="Seg_10422" s="T502">задняя.часть-EP-2SG.[NOM]</ta>
            <ta e="T504" id="Seg_10423" s="T503">к</ta>
            <ta e="T505" id="Seg_10424" s="T504">осмотреться-EP-NEG.[IMP.2SG]</ta>
            <ta e="T506" id="Seg_10425" s="T505">дорога.[NOM]</ta>
            <ta e="T507" id="Seg_10426" s="T506">по.пути</ta>
            <ta e="T508" id="Seg_10427" s="T507">останавливаться-NEG.[IMP.2SG]</ta>
            <ta e="T509" id="Seg_10428" s="T508">олень-PL-2SG-ACC</ta>
            <ta e="T510" id="Seg_10429" s="T509">есть-EP-CAUS-EP-NEG.[IMP.2SG]</ta>
            <ta e="T511" id="Seg_10430" s="T510">мальчик.[NOM]</ta>
            <ta e="T512" id="Seg_10431" s="T511">рука-3SG-ACC</ta>
            <ta e="T513" id="Seg_10432" s="T512">протягивать-PST1-3SG</ta>
            <ta e="T514" id="Seg_10433" s="T513">повод-ACC</ta>
            <ta e="T515" id="Seg_10434" s="T514">хватать-PST1-3SG</ta>
            <ta e="T516" id="Seg_10435" s="T515">быть.видно-EP-NEG.PTCP</ta>
            <ta e="T517" id="Seg_10436" s="T516">сани-DAT/LOC</ta>
            <ta e="T518" id="Seg_10437" s="T517">садиться-PST1-3SG</ta>
            <ta e="T519" id="Seg_10438" s="T518">ветер-PROPR.[NOM]</ta>
            <ta e="T520" id="Seg_10439" s="T519">пурга.[NOM]</ta>
            <ta e="T521" id="Seg_10440" s="T520">бить-PST1-3SG</ta>
            <ta e="T522" id="Seg_10441" s="T521">сколько-ACC</ta>
            <ta e="T523" id="Seg_10442" s="T522">идти-PST2-3SG</ta>
            <ta e="T524" id="Seg_10443" s="T523">Q</ta>
            <ta e="T525" id="Seg_10444" s="T524">кто.[NOM]</ta>
            <ta e="T526" id="Seg_10445" s="T525">знать-FUT.[3SG]=Q</ta>
            <ta e="T527" id="Seg_10446" s="T526">сани-PROPR.[NOM]</ta>
            <ta e="T528" id="Seg_10447" s="T527">олень-3PL.[NOM]</ta>
            <ta e="T529" id="Seg_10448" s="T528">сам-3PL.[NOM]</ta>
            <ta e="T530" id="Seg_10449" s="T529">уносить-PRS-3PL</ta>
            <ta e="T531" id="Seg_10450" s="T530">мальчик.[NOM]</ta>
            <ta e="T532" id="Seg_10451" s="T531">мерзнуть-NEG.[3SG]</ta>
            <ta e="T533" id="Seg_10452" s="T532">рука-3SG-ACC</ta>
            <ta e="T534" id="Seg_10453" s="T533">сидушка-3SG-GEN</ta>
            <ta e="T535" id="Seg_10454" s="T534">нутро-3SG-DAT/LOC</ta>
            <ta e="T536" id="Seg_10455" s="T535">вставлять-PST1-3SG</ta>
            <ta e="T537" id="Seg_10456" s="T536">там</ta>
            <ta e="T538" id="Seg_10457" s="T537">теплый.[NOM]</ta>
            <ta e="T539" id="Seg_10458" s="T538">железный.[NOM]</ta>
            <ta e="T540" id="Seg_10459" s="T539">дерево.[NOM]</ta>
            <ta e="T541" id="Seg_10460" s="T540">лежать-PRS.[3SG]</ta>
            <ta e="T542" id="Seg_10461" s="T541">быть-PST2.[3SG]</ta>
            <ta e="T543" id="Seg_10462" s="T542">тот-3SG-3SG.[NOM]</ta>
            <ta e="T544" id="Seg_10463" s="T543">согреться-EP-CAUS-PRS.[3SG]</ta>
            <ta e="T545" id="Seg_10464" s="T544">река.[NOM]</ta>
            <ta e="T546" id="Seg_10465" s="T545">верхняя.часть-3SG-DAT/LOC</ta>
            <ta e="T547" id="Seg_10466" s="T546">видеть-PST1-3SG</ta>
            <ta e="T548" id="Seg_10467" s="T547">сани-DAT/LOC</ta>
            <ta e="T549" id="Seg_10468" s="T548">умирать-PTCP.PST</ta>
            <ta e="T550" id="Seg_10469" s="T549">жена.[NOM]</ta>
            <ta e="T551" id="Seg_10470" s="T550">лежать-PRS.[3SG]</ta>
            <ta e="T552" id="Seg_10471" s="T551">крепко</ta>
            <ta e="T553" id="Seg_10472" s="T552">замести-EP-PASS/REFL-CVB.SEQ</ta>
            <ta e="T554" id="Seg_10473" s="T553">после</ta>
            <ta e="T555" id="Seg_10474" s="T554">выносить-NEG.CVB.SIM</ta>
            <ta e="T556" id="Seg_10475" s="T555">останавливаться-PST1-3SG</ta>
            <ta e="T557" id="Seg_10476" s="T556">останавливаться-PTCP.PRS-3SG-ACC</ta>
            <ta e="T558" id="Seg_10477" s="T557">с</ta>
            <ta e="T559" id="Seg_10478" s="T558">передовой.олень-3SG.[NOM]</ta>
            <ta e="T560" id="Seg_10479" s="T559">PFV</ta>
            <ta e="T561" id="Seg_10480" s="T560">прыгать-CVB.SEQ</ta>
            <ta e="T562" id="Seg_10481" s="T561">куда</ta>
            <ta e="T563" id="Seg_10482" s="T562">идти-PST2-3SG</ta>
            <ta e="T564" id="Seg_10483" s="T563">куда</ta>
            <ta e="T565" id="Seg_10484" s="T564">приходить-PST2-3SG</ta>
            <ta e="T566" id="Seg_10485" s="T565">три</ta>
            <ta e="T567" id="Seg_10486" s="T566">черный.олень.[NOM]</ta>
            <ta e="T568" id="Seg_10487" s="T567">олень-PROPR.[NOM]</ta>
            <ta e="T569" id="Seg_10488" s="T568">быть-PST1-3SG</ta>
            <ta e="T570" id="Seg_10489" s="T569">быть-PST2.[3SG]</ta>
            <ta e="T571" id="Seg_10490" s="T570">передовой.олень-3SG.[NOM]</ta>
            <ta e="T572" id="Seg_10491" s="T571">кончаться-CVB.SEQ</ta>
            <ta e="T573" id="Seg_10492" s="T572">идти-CVB.SEQ</ta>
            <ta e="T574" id="Seg_10493" s="T573">оставаться-PST2.[3SG]</ta>
            <ta e="T575" id="Seg_10494" s="T574">сума.[NOM]</ta>
            <ta e="T576" id="Seg_10495" s="T575">голос-3SG.[NOM]</ta>
            <ta e="T577" id="Seg_10496" s="T576">слышаться-EP-PST2.[3SG]</ta>
            <ta e="T578" id="Seg_10497" s="T577">2SG-DAT/LOC</ta>
            <ta e="T579" id="Seg_10498" s="T578">говорить-PST2-EP-1SG</ta>
            <ta e="T580" id="Seg_10499" s="T579">останавливаться-NEG.[IMP.2SG]</ta>
            <ta e="T581" id="Seg_10500" s="T580">мальчик.[NOM]</ta>
            <ta e="T582" id="Seg_10501" s="T581">сани-3SG-ABL</ta>
            <ta e="T583" id="Seg_10502" s="T582">красный.[NOM]</ta>
            <ta e="T584" id="Seg_10503" s="T583">железный.[NOM]</ta>
            <ta e="T585" id="Seg_10504" s="T584">дерево-ACC</ta>
            <ta e="T586" id="Seg_10505" s="T585">взять-CVB.SEQ</ta>
            <ta e="T587" id="Seg_10506" s="T586">девушка.[NOM]</ta>
            <ta e="T588" id="Seg_10507" s="T587">сани-3SG-GEN</ta>
            <ta e="T589" id="Seg_10508" s="T588">соседтсво-3SG-DAT/LOC</ta>
            <ta e="T590" id="Seg_10509" s="T589">внутрь</ta>
            <ta e="T591" id="Seg_10510" s="T590">толкать-PST1-3SG</ta>
            <ta e="T592" id="Seg_10511" s="T591">снег.[NOM]</ta>
            <ta e="T593" id="Seg_10512" s="T592">сразу</ta>
            <ta e="T594" id="Seg_10513" s="T593">таять-CVB.SEQ</ta>
            <ta e="T595" id="Seg_10514" s="T594">оставаться-PST1-3SG</ta>
            <ta e="T596" id="Seg_10515" s="T595">мерзнуть-CVB.SEQ</ta>
            <ta e="T597" id="Seg_10516" s="T596">умирать-PTCP.PST</ta>
            <ta e="T598" id="Seg_10517" s="T597">жена-3SG.[NOM]</ta>
            <ta e="T599" id="Seg_10518" s="T598">оживать-PST1-3SG</ta>
            <ta e="T600" id="Seg_10519" s="T599">видеть-PST2-3SG</ta>
            <ta e="T601" id="Seg_10520" s="T600">очень</ta>
            <ta e="T602" id="Seg_10521" s="T601">красивый</ta>
            <ta e="T603" id="Seg_10522" s="T602">очень</ta>
            <ta e="T604" id="Seg_10523" s="T603">девушка.[NOM]</ta>
            <ta e="T605" id="Seg_10524" s="T604">быть-PST2.[3SG]</ta>
            <ta e="T606" id="Seg_10525" s="T605">девушка.[NOM]</ta>
            <ta e="T607" id="Seg_10526" s="T606">радоваться-CVB.SEQ</ta>
            <ta e="T608" id="Seg_10527" s="T607">беда-POSS</ta>
            <ta e="T609" id="Seg_10528" s="T608">NEG.[3SG]</ta>
            <ta e="T610" id="Seg_10529" s="T609">теперь</ta>
            <ta e="T611" id="Seg_10530" s="T610">сам-1SG.[NOM]</ta>
            <ta e="T612" id="Seg_10531" s="T611">дом-1SG-DAT/LOC</ta>
            <ta e="T613" id="Seg_10532" s="T612">доезжать-FUT-1SG</ta>
            <ta e="T614" id="Seg_10533" s="T613">сопровождать-EP-NEG-2SG</ta>
            <ta e="T615" id="Seg_10534" s="T614">Q</ta>
            <ta e="T616" id="Seg_10535" s="T615">есть-EP-CAUS-FUT-1SG</ta>
            <ta e="T617" id="Seg_10536" s="T616">теплый.[NOM]</ta>
            <ta e="T618" id="Seg_10537" s="T617">постельное.белье-DAT/LOC</ta>
            <ta e="T619" id="Seg_10538" s="T618">постелить-FUT-1SG</ta>
            <ta e="T620" id="Seg_10539" s="T619">просить-PST2.[3SG]</ta>
            <ta e="T621" id="Seg_10540" s="T620">девушка.[NOM]</ta>
            <ta e="T622" id="Seg_10541" s="T621">далекий.[NOM]</ta>
            <ta e="T623" id="Seg_10542" s="T622">Q</ta>
            <ta e="T624" id="Seg_10543" s="T623">дом-2SG.[NOM]</ta>
            <ta e="T625" id="Seg_10544" s="T624">вода.[NOM]</ta>
            <ta e="T626" id="Seg_10545" s="T625">нутро-3SG-DAT/LOC</ta>
            <ta e="T627" id="Seg_10546" s="T626">мальчик.[NOM]</ta>
            <ta e="T628" id="Seg_10547" s="T627">много</ta>
            <ta e="T629" id="Seg_10548" s="T628">чудо-ACC</ta>
            <ta e="T630" id="Seg_10549" s="T629">видеть-PST2-3SG</ta>
            <ta e="T631" id="Seg_10550" s="T630">тот.[NOM]</ta>
            <ta e="T632" id="Seg_10551" s="T631">из_за</ta>
            <ta e="T633" id="Seg_10552" s="T632">девушка.[NOM]</ta>
            <ta e="T634" id="Seg_10553" s="T633">рассказ-3SG-ACC</ta>
            <ta e="T635" id="Seg_10554" s="T634">удивляться-NEG-PST1-3SG</ta>
            <ta e="T636" id="Seg_10555" s="T635">говорить-CVB.SIM</ta>
            <ta e="T637" id="Seg_10556" s="T636">быть-PST2.[3SG]</ta>
            <ta e="T638" id="Seg_10557" s="T637">1SG.[NOM]</ta>
            <ta e="T639" id="Seg_10558" s="T638">старшая.сестра-1SG-ACC</ta>
            <ta e="T640" id="Seg_10559" s="T639">найти-PTCP.FUT-1SG-ACC</ta>
            <ta e="T641" id="Seg_10560" s="T640">надо</ta>
            <ta e="T642" id="Seg_10561" s="T641">позже</ta>
            <ta e="T643" id="Seg_10562" s="T642">приходить-FUT-1SG</ta>
            <ta e="T644" id="Seg_10563" s="T643">EVID</ta>
            <ta e="T645" id="Seg_10564" s="T644">ждать.[IMP.2SG]</ta>
            <ta e="T646" id="Seg_10565" s="T645">мальчик.[NOM]</ta>
            <ta e="T647" id="Seg_10566" s="T646">сколько-ACC-сколько-ACC</ta>
            <ta e="T648" id="Seg_10567" s="T647">идти-PST2-3SG</ta>
            <ta e="T649" id="Seg_10568" s="T648">кто.[NOM]</ta>
            <ta e="T650" id="Seg_10569" s="T649">NEG</ta>
            <ta e="T651" id="Seg_10570" s="T650">знать-NEG.[3SG]</ta>
            <ta e="T652" id="Seg_10571" s="T651">вдруг</ta>
            <ta e="T653" id="Seg_10572" s="T652">проталина.[NOM]</ta>
            <ta e="T654" id="Seg_10573" s="T653">становиться-PST2.[3SG]</ta>
            <ta e="T655" id="Seg_10574" s="T654">ягель-PROPR.[NOM]</ta>
            <ta e="T656" id="Seg_10575" s="T655">очень</ta>
            <ta e="T657" id="Seg_10576" s="T656">олень-PL-3SG.[NOM]</ta>
            <ta e="T658" id="Seg_10577" s="T657">голодный-VBZ-PST1-3PL</ta>
            <ta e="T659" id="Seg_10578" s="T658">есть-EP-CAUS-PTCP.FUT-DAT/LOC</ta>
            <ta e="T660" id="Seg_10579" s="T659">Q</ta>
            <ta e="T661" id="Seg_10580" s="T660">говорить-CVB.SIM</ta>
            <ta e="T662" id="Seg_10581" s="T661">думать-PST2.[3SG]</ta>
            <ta e="T663" id="Seg_10582" s="T662">земля.[NOM]</ta>
            <ta e="T664" id="Seg_10583" s="T663">господин-3SG-GEN</ta>
            <ta e="T665" id="Seg_10584" s="T664">слово-3SG-ACC</ta>
            <ta e="T666" id="Seg_10585" s="T665">помнить-CVB.SIM-помнить-CVB.SIM</ta>
            <ta e="T667" id="Seg_10586" s="T666">олень-PL-3SG-ACC</ta>
            <ta e="T668" id="Seg_10587" s="T667">останавливаться-CAUS-NEG-PST1-3SG</ta>
            <ta e="T669" id="Seg_10588" s="T668">потом</ta>
            <ta e="T670" id="Seg_10589" s="T669">говорить-CVB.SIM</ta>
            <ta e="T671" id="Seg_10590" s="T670">думать-PST1-3SG</ta>
            <ta e="T672" id="Seg_10591" s="T671">очень</ta>
            <ta e="T673" id="Seg_10592" s="T672">голодный-VBZ-PST2-3PL</ta>
            <ta e="T674" id="Seg_10593" s="T673">останавливаться-EP-CAUS-PTCP.FUT-DAT/LOC</ta>
            <ta e="T675" id="Seg_10594" s="T674">останавливаться-EP-CAUS-PTCP.PRS-3SG-ACC</ta>
            <ta e="T676" id="Seg_10595" s="T675">с</ta>
            <ta e="T677" id="Seg_10596" s="T676">середина-ADJZ</ta>
            <ta e="T678" id="Seg_10597" s="T677">запрячь-PTCP.PRS</ta>
            <ta e="T679" id="Seg_10598" s="T678">олень-3SG.[NOM]</ta>
            <ta e="T680" id="Seg_10599" s="T679">исчезать-NMNZ.[NOM]</ta>
            <ta e="T681" id="Seg_10600" s="T680">только</ta>
            <ta e="T682" id="Seg_10601" s="T681">делать-CVB.SEQ</ta>
            <ta e="T683" id="Seg_10602" s="T682">оставаться-PST2.[3SG]</ta>
            <ta e="T684" id="Seg_10603" s="T683">куда</ta>
            <ta e="T685" id="Seg_10604" s="T684">идти-PST2-3SG</ta>
            <ta e="T686" id="Seg_10605" s="T685">куда</ta>
            <ta e="T687" id="Seg_10606" s="T686">бежать-PST2-3SG</ta>
            <ta e="T688" id="Seg_10607" s="T687">опять</ta>
            <ta e="T689" id="Seg_10608" s="T688">голос.[NOM]</ta>
            <ta e="T690" id="Seg_10609" s="T689">слышаться-EP-PST2.[3SG]</ta>
            <ta e="T691" id="Seg_10610" s="T690">1SG.[NOM]</ta>
            <ta e="T692" id="Seg_10611" s="T691">2SG-DAT/LOC</ta>
            <ta e="T693" id="Seg_10612" s="T692">говорить-PST2-EP-1SG</ta>
            <ta e="T694" id="Seg_10613" s="T693">есть-EP-CAUS-EP-NEG.[IMP.2SG]</ta>
            <ta e="T695" id="Seg_10614" s="T694">олень-PL-2SG-ACC</ta>
            <ta e="T696" id="Seg_10615" s="T695">мальчик.[NOM]</ta>
            <ta e="T697" id="Seg_10616" s="T696">сколько-ACC</ta>
            <ta e="T698" id="Seg_10617" s="T697">идти-PST2-3SG</ta>
            <ta e="T699" id="Seg_10618" s="T698">Q</ta>
            <ta e="T700" id="Seg_10619" s="T699">этот</ta>
            <ta e="T701" id="Seg_10620" s="T700">камень.[NOM]</ta>
            <ta e="T702" id="Seg_10621" s="T701">гора-3SG-DAT/LOC</ta>
            <ta e="T703" id="Seg_10622" s="T702">доезжать-PST1-3SG</ta>
            <ta e="T704" id="Seg_10623" s="T703">два</ta>
            <ta e="T705" id="Seg_10624" s="T704">сторона.[NOM]</ta>
            <ta e="T706" id="Seg_10625" s="T705">разорвать-PTCP.HAB-3SG-DAT/LOC</ta>
            <ta e="T707" id="Seg_10626" s="T706">только</ta>
            <ta e="T708" id="Seg_10627" s="T707">сам-3SG-GEN</ta>
            <ta e="T709" id="Seg_10628" s="T708">место-3SG-DAT/LOC</ta>
            <ta e="T710" id="Seg_10629" s="T709">доезжать-PST2.[3SG]</ta>
            <ta e="T711" id="Seg_10630" s="T710">быть-PST2.[3SG]</ta>
            <ta e="T712" id="Seg_10631" s="T711">мерзнуть-CVB.SEQ</ta>
            <ta e="T713" id="Seg_10632" s="T712">умирать-PTCP.PST</ta>
            <ta e="T714" id="Seg_10633" s="T713">олень-PL.[NOM]</ta>
            <ta e="T715" id="Seg_10634" s="T714">рог-3PL.[NOM]</ta>
            <ta e="T716" id="Seg_10635" s="T715">еле</ta>
            <ta e="T717" id="Seg_10636" s="T716">снег.[NOM]</ta>
            <ta e="T718" id="Seg_10637" s="T717">нутро-3SG-ABL</ta>
            <ta e="T719" id="Seg_10638" s="T718">вытянуть-PRS-3PL</ta>
            <ta e="T720" id="Seg_10639" s="T719">снег-ACC</ta>
            <ta e="T721" id="Seg_10640" s="T720">копать-CVB.SEQ</ta>
            <ta e="T722" id="Seg_10641" s="T721">старшая.сестра-3SG-GEN</ta>
            <ta e="T723" id="Seg_10642" s="T722">передовой.олень-3SG-ACC</ta>
            <ta e="T724" id="Seg_10643" s="T723">найти-PST1-3SG</ta>
            <ta e="T725" id="Seg_10644" s="T724">теплый.[NOM]</ta>
            <ta e="T726" id="Seg_10645" s="T725">железный.[NOM]</ta>
            <ta e="T727" id="Seg_10646" s="T726">дерево-3SG-INSTR</ta>
            <ta e="T728" id="Seg_10647" s="T727">задеть-PST1-3SG</ta>
            <ta e="T729" id="Seg_10648" s="T728">олень-3SG.[NOM]</ta>
            <ta e="T730" id="Seg_10649" s="T729">оживать-CVB.SEQ</ta>
            <ta e="T731" id="Seg_10650" s="T730">после</ta>
            <ta e="T732" id="Seg_10651" s="T731">вставать-PST1-3SG</ta>
            <ta e="T733" id="Seg_10652" s="T732">потом</ta>
            <ta e="T734" id="Seg_10653" s="T733">дерево-ACC</ta>
            <ta e="T735" id="Seg_10654" s="T734">взять-PST1-3SG</ta>
            <ta e="T736" id="Seg_10655" s="T735">снег-DAT/LOC</ta>
            <ta e="T737" id="Seg_10656" s="T736">толкать-PST1-3SG</ta>
            <ta e="T738" id="Seg_10657" s="T737">мерзнуть-CVB.SEQ</ta>
            <ta e="T739" id="Seg_10658" s="T738">умирать-PTCP.PST</ta>
            <ta e="T740" id="Seg_10659" s="T739">олень-PL.[NOM]</ta>
            <ta e="T741" id="Seg_10660" s="T740">сразу</ta>
            <ta e="T742" id="Seg_10661" s="T741">каждый-3PL.[NOM]</ta>
            <ta e="T743" id="Seg_10662" s="T742">оживать-PST1-3PL</ta>
            <ta e="T744" id="Seg_10663" s="T743">целый</ta>
            <ta e="T745" id="Seg_10664" s="T744">стадо.[NOM]</ta>
            <ta e="T746" id="Seg_10665" s="T745">становиться-CVB.SEQ</ta>
            <ta e="T747" id="Seg_10666" s="T746">есть-CVB.SIM</ta>
            <ta e="T748" id="Seg_10667" s="T747">идти-PRS-3PL</ta>
            <ta e="T749" id="Seg_10668" s="T748">новый</ta>
            <ta e="T750" id="Seg_10669" s="T749">земля-EP-INSTR</ta>
            <ta e="T751" id="Seg_10670" s="T750">осмотреться-PST2-3SG</ta>
            <ta e="T752" id="Seg_10671" s="T751">старшая.сестра-3SG.[NOM]</ta>
            <ta e="T753" id="Seg_10672" s="T752">стоять-PRS.[3SG]</ta>
            <ta e="T754" id="Seg_10673" s="T753">мальчик.[NOM]</ta>
            <ta e="T755" id="Seg_10674" s="T754">старшая.сестра-3SG-ACC</ta>
            <ta e="T756" id="Seg_10675" s="T755">видеть-CVB.SEQ</ta>
            <ta e="T757" id="Seg_10676" s="T756">очень</ta>
            <ta e="T758" id="Seg_10677" s="T757">радоваться-PST1-3SG</ta>
            <ta e="T759" id="Seg_10678" s="T758">сначала</ta>
            <ta e="T760" id="Seg_10679" s="T759">2SG.[NOM]</ta>
            <ta e="T761" id="Seg_10680" s="T760">говорить-PST1-3SG</ta>
            <ta e="T762" id="Seg_10681" s="T761">старшая.сестра-3SG.[NOM]</ta>
            <ta e="T763" id="Seg_10682" s="T762">долго</ta>
            <ta e="T764" id="Seg_10683" s="T763">очень-ADVZ</ta>
            <ta e="T765" id="Seg_10684" s="T764">спать-PST2-EP-2SG</ta>
            <ta e="T766" id="Seg_10685" s="T765">потом</ta>
            <ta e="T767" id="Seg_10686" s="T766">1SG.[NOM]</ta>
            <ta e="T768" id="Seg_10687" s="T767">весна.[NOM]</ta>
            <ta e="T769" id="Seg_10688" s="T768">становиться-PST2.[3SG]</ta>
            <ta e="T770" id="Seg_10689" s="T769">быть-PST2.[3SG]</ta>
            <ta e="T771" id="Seg_10690" s="T770">зима-ACC</ta>
            <ta e="T772" id="Seg_10691" s="T771">целый</ta>
            <ta e="T773" id="Seg_10692" s="T772">спать-PST2-1SG</ta>
            <ta e="T774" id="Seg_10693" s="T773">быть-PST2.[3SG]</ta>
            <ta e="T775" id="Seg_10694" s="T774">мальчик.[NOM]</ta>
            <ta e="T776" id="Seg_10695" s="T775">старшая.сестра-3SG-ACC</ta>
            <ta e="T777" id="Seg_10696" s="T776">дом-3SG-DAT/LOC</ta>
            <ta e="T778" id="Seg_10697" s="T777">приносить-PST2-3SG</ta>
            <ta e="T779" id="Seg_10698" s="T778">стадо-3SG-ACC</ta>
            <ta e="T780" id="Seg_10699" s="T779">погонять-CVB.SEQ</ta>
            <ta e="T781" id="Seg_10700" s="T780">принести-PST1-3SG</ta>
            <ta e="T782" id="Seg_10701" s="T781">сколько</ta>
            <ta e="T783" id="Seg_10702" s="T782">NEG</ta>
            <ta e="T784" id="Seg_10703" s="T783">быть-NEG.CVB.SIM</ta>
            <ta e="T785" id="Seg_10704" s="T784">этот</ta>
            <ta e="T786" id="Seg_10705" s="T785">мальчик.[NOM]</ta>
            <ta e="T787" id="Seg_10706" s="T786">готовить-PST1-3SG</ta>
            <ta e="T788" id="Seg_10707" s="T787">куда</ta>
            <ta e="T789" id="Seg_10708" s="T788">идти-PRS-2SG</ta>
            <ta e="T790" id="Seg_10709" s="T789">спрашивать-PST2.[3SG]</ta>
            <ta e="T791" id="Seg_10710" s="T790">старшая.сестра-3SG.[NOM]</ta>
            <ta e="T792" id="Seg_10711" s="T791">скоро</ta>
            <ta e="T793" id="Seg_10712" s="T792">приходить-FUT-1SG</ta>
            <ta e="T794" id="Seg_10713" s="T793">опасаться-EP-NEG.[IMP.2SG]</ta>
            <ta e="T795" id="Seg_10714" s="T794">мальчик.[NOM]</ta>
            <ta e="T796" id="Seg_10715" s="T795">этот</ta>
            <ta e="T797" id="Seg_10716" s="T796">девушка-ACC</ta>
            <ta e="T798" id="Seg_10717" s="T797">искать-CVB.SIM</ta>
            <ta e="T799" id="Seg_10718" s="T798">идти-CAP.[3SG]</ta>
            <ta e="T800" id="Seg_10719" s="T799">вода.[NOM]</ta>
            <ta e="T801" id="Seg_10720" s="T800">нутро-3SG-DAT/LOC</ta>
            <ta e="T802" id="Seg_10721" s="T801">дом-PROPR-ACC</ta>
            <ta e="T803" id="Seg_10722" s="T802">после</ta>
            <ta e="T804" id="Seg_10723" s="T803">идти-CVB.SEQ</ta>
            <ta e="T805" id="Seg_10724" s="T804">пасмурный.[NOM]</ta>
            <ta e="T806" id="Seg_10725" s="T805">очень</ta>
            <ta e="T807" id="Seg_10726" s="T806">пурга.[NOM]</ta>
            <ta e="T808" id="Seg_10727" s="T807">становиться-PST2.[3SG]</ta>
            <ta e="T809" id="Seg_10728" s="T808">земля-3SG-INSTR</ta>
            <ta e="T810" id="Seg_10729" s="T809">EMPH</ta>
            <ta e="T811" id="Seg_10730" s="T810">идти-PTCP.PRS-3SG-ACC</ta>
            <ta e="T812" id="Seg_10731" s="T811">знать-NEG.[3SG]</ta>
            <ta e="T813" id="Seg_10732" s="T812">лед-3SG-INSTR</ta>
            <ta e="T814" id="Seg_10733" s="T813">EMPH</ta>
            <ta e="T815" id="Seg_10734" s="T814">идти-PTCP.PRS-3SG-ACC</ta>
            <ta e="T816" id="Seg_10735" s="T815">знать-NEG.[3SG]</ta>
            <ta e="T817" id="Seg_10736" s="T816">придумать-FREQ-CVB.SEQ</ta>
            <ta e="T818" id="Seg_10737" s="T817">видеть-PST2-3SG</ta>
            <ta e="T819" id="Seg_10738" s="T818">река.[NOM]</ta>
            <ta e="T820" id="Seg_10739" s="T819">льдина-3SG.[NOM]</ta>
            <ta e="T821" id="Seg_10740" s="T820">быть-PST2.[3SG]</ta>
            <ta e="T822" id="Seg_10741" s="T821">весна-ADJZ.[NOM]</ta>
            <ta e="T823" id="Seg_10742" s="T822">лед.[NOM]</ta>
            <ta e="T824" id="Seg_10743" s="T823">зацепляться-CVB.SEQ</ta>
            <ta e="T825" id="Seg_10744" s="T824">мальчик.[NOM]</ta>
            <ta e="T826" id="Seg_10745" s="T825">вода-DAT/LOC</ta>
            <ta e="T827" id="Seg_10746" s="T826">падать-PST2.[3SG]</ta>
            <ta e="T828" id="Seg_10747" s="T827">падать-CVB.SEQ</ta>
            <ta e="T829" id="Seg_10748" s="T828">щука.[NOM]</ta>
            <ta e="T830" id="Seg_10749" s="T829">становиться-PST2.[3SG]</ta>
            <ta e="T831" id="Seg_10750" s="T830">вода.[NOM]</ta>
            <ta e="T832" id="Seg_10751" s="T831">нутро-3SG-INSTR</ta>
            <ta e="T833" id="Seg_10752" s="T832">течь-CVB.SIM</ta>
            <ta e="T834" id="Seg_10753" s="T833">идти-CVB.SEQ</ta>
            <ta e="T835" id="Seg_10754" s="T834">сеть-DAT/LOC</ta>
            <ta e="T836" id="Seg_10755" s="T835">попадать-PST2.[3SG]</ta>
            <ta e="T837" id="Seg_10756" s="T836">ширкоий.[NOM]</ta>
            <ta e="T838" id="Seg_10757" s="T837">сеть-DAT/LOC</ta>
            <ta e="T839" id="Seg_10758" s="T838">щука-PL.[NOM]</ta>
            <ta e="T840" id="Seg_10759" s="T839">только</ta>
            <ta e="T841" id="Seg_10760" s="T840">сколько</ta>
            <ta e="T842" id="Seg_10761" s="T841">NEG</ta>
            <ta e="T843" id="Seg_10762" s="T842">быть-NEG.CVB.SIM</ta>
            <ta e="T844" id="Seg_10763" s="T843">девушка.[NOM]</ta>
            <ta e="T845" id="Seg_10764" s="T844">приходить-PST1-3SG</ta>
            <ta e="T846" id="Seg_10765" s="T845">этот</ta>
            <ta e="T847" id="Seg_10766" s="T846">девушка-3SG.[NOM]</ta>
            <ta e="T848" id="Seg_10767" s="T847">оживать-PASS-PTCP.HAB-3SG.[3SG]</ta>
            <ta e="T849" id="Seg_10768" s="T848">щука.[NOM]</ta>
            <ta e="T850" id="Seg_10769" s="T849">мальчик-ACC</ta>
            <ta e="T851" id="Seg_10770" s="T850">передовой.олень.[NOM]</ta>
            <ta e="T852" id="Seg_10771" s="T851">делать-CVB.SEQ</ta>
            <ta e="T853" id="Seg_10772" s="T852">после</ta>
            <ta e="T854" id="Seg_10773" s="T853">запрячь-PST1-3SG</ta>
            <ta e="T855" id="Seg_10774" s="T854">дом-3SG-DAT/LOC</ta>
            <ta e="T856" id="Seg_10775" s="T855">идти-CAP.[3SG]</ta>
            <ta e="T857" id="Seg_10776" s="T856">передовой.олень.[NOM]</ta>
            <ta e="T858" id="Seg_10777" s="T857">слышать-EP-NEG.[3SG]</ta>
            <ta e="T859" id="Seg_10778" s="T858">берег.[NOM]</ta>
            <ta e="T860" id="Seg_10779" s="T859">только</ta>
            <ta e="T861" id="Seg_10780" s="T860">к</ta>
            <ta e="T862" id="Seg_10781" s="T861">%%-PRS.[3SG]</ta>
            <ta e="T863" id="Seg_10782" s="T862">девушка.[NOM]</ta>
            <ta e="T864" id="Seg_10783" s="T863">тот-ACC</ta>
            <ta e="T865" id="Seg_10784" s="T864">руководить-CVB.SIM</ta>
            <ta e="T866" id="Seg_10785" s="T865">пытаться-PRS.[3SG]</ta>
            <ta e="T867" id="Seg_10786" s="T866">передовой.олень-3SG.[NOM]</ta>
            <ta e="T868" id="Seg_10787" s="T867">резать-CVB.SIM</ta>
            <ta e="T869" id="Seg_10788" s="T868">стараться-CVB.SEQ</ta>
            <ta e="T870" id="Seg_10789" s="T869">берег-DAT/LOC</ta>
            <ta e="T871" id="Seg_10790" s="T870">выйти-PST1-3SG</ta>
            <ta e="T872" id="Seg_10791" s="T871">выйти-CVB.ANT</ta>
            <ta e="T873" id="Seg_10792" s="T872">мальчик-щука.[NOM]</ta>
            <ta e="T874" id="Seg_10793" s="T873">настоящий.[NOM]</ta>
            <ta e="T875" id="Seg_10794" s="T874">мальчик.[NOM]</ta>
            <ta e="T876" id="Seg_10795" s="T875">становиться-PST2.[3SG]</ta>
            <ta e="T877" id="Seg_10796" s="T876">щука-PL.[NOM]</ta>
            <ta e="T878" id="Seg_10797" s="T877">олень.[NOM]</ta>
            <ta e="T879" id="Seg_10798" s="T878">становиться-PST1-3PL</ta>
            <ta e="T880" id="Seg_10799" s="T879">девушка-PROPR.[NOM]</ta>
            <ta e="T881" id="Seg_10800" s="T880">мальчик.[NOM]</ta>
            <ta e="T882" id="Seg_10801" s="T881">узнавать-EP-RECP/COLL-PST1-3PL</ta>
            <ta e="T883" id="Seg_10802" s="T882">девушка.[NOM]</ta>
            <ta e="T884" id="Seg_10803" s="T883">спрашивать-PST1-3SG</ta>
            <ta e="T885" id="Seg_10804" s="T884">старшая.сестра-2SG-ACC</ta>
            <ta e="T886" id="Seg_10805" s="T885">найти-PST1-2SG</ta>
            <ta e="T887" id="Seg_10806" s="T886">как</ta>
            <ta e="T888" id="Seg_10807" s="T887">жить-PRS-2SG</ta>
            <ta e="T889" id="Seg_10808" s="T888">сопровождать.[IMP.2SG]</ta>
            <ta e="T890" id="Seg_10809" s="T889">1SG-ACC</ta>
            <ta e="T891" id="Seg_10810" s="T890">с</ta>
            <ta e="T892" id="Seg_10811" s="T891">говорить-PST1-3SG</ta>
            <ta e="T893" id="Seg_10812" s="T892">мальчик.[NOM]</ta>
            <ta e="T894" id="Seg_10813" s="T893">старшая.сестра-EP-1SG.[NOM]</ta>
            <ta e="T895" id="Seg_10814" s="T894">2SG-ACC</ta>
            <ta e="T896" id="Seg_10815" s="T895">ждать-PRS.[3SG]</ta>
            <ta e="T897" id="Seg_10816" s="T896">знать-RECP/COLL-EP-RECP/COLL-EP-IMP.2PL</ta>
            <ta e="T898" id="Seg_10817" s="T897">вместе</ta>
            <ta e="T899" id="Seg_10818" s="T898">жить-FUT-1PL</ta>
            <ta e="T900" id="Seg_10819" s="T899">стадо-1PL-ACC</ta>
            <ta e="T901" id="Seg_10820" s="T900">связывать-FUT-1PL</ta>
            <ta e="T902" id="Seg_10821" s="T901">мальчик-ACC</ta>
            <ta e="T903" id="Seg_10822" s="T902">с</ta>
            <ta e="T904" id="Seg_10823" s="T903">девушка.[NOM]</ta>
            <ta e="T905" id="Seg_10824" s="T904">щука-PL-ACC</ta>
            <ta e="T906" id="Seg_10825" s="T905">вода.[NOM]</ta>
            <ta e="T907" id="Seg_10826" s="T906">нутро-3SG-ABL</ta>
            <ta e="T908" id="Seg_10827" s="T907">вытащить-PST1-3PL</ta>
            <ta e="T909" id="Seg_10828" s="T908">тот-3SG-3PL.[NOM]</ta>
            <ta e="T910" id="Seg_10829" s="T909">пятнистый.[NOM]</ta>
            <ta e="T911" id="Seg_10830" s="T910">олень.[NOM]</ta>
            <ta e="T912" id="Seg_10831" s="T911">становиться-PST1-3PL</ta>
            <ta e="T913" id="Seg_10832" s="T912">мальчик.[NOM]</ta>
            <ta e="T914" id="Seg_10833" s="T913">старшая.сестра-3SG.[NOM]</ta>
            <ta e="T915" id="Seg_10834" s="T914">радоваться-CVB.SEQ</ta>
            <ta e="T916" id="Seg_10835" s="T915">встречать-PST1-3SG</ta>
            <ta e="T917" id="Seg_10836" s="T916">мальчик.[NOM]</ta>
            <ta e="T918" id="Seg_10837" s="T917">три</ta>
            <ta e="T919" id="Seg_10838" s="T918">день-ACC</ta>
            <ta e="T920" id="Seg_10839" s="T919">спать-PST1-3SG</ta>
            <ta e="T921" id="Seg_10840" s="T920">просыпаться-CVB.SEQ</ta>
            <ta e="T922" id="Seg_10841" s="T921">видеть-PST2-3SG</ta>
            <ta e="T923" id="Seg_10842" s="T922">старшая.сестра-3SG.[NOM]</ta>
            <ta e="T924" id="Seg_10843" s="T923">девушка-ACC</ta>
            <ta e="T925" id="Seg_10844" s="T924">с</ta>
            <ta e="T926" id="Seg_10845" s="T925">разговаривать-CVB.SIM-разговаривать-CVB.SIM</ta>
            <ta e="T927" id="Seg_10846" s="T926">смеяться-RECP/COLL-CVB.SIM</ta>
            <ta e="T928" id="Seg_10847" s="T927">сидеть-PRS-3PL</ta>
            <ta e="T929" id="Seg_10848" s="T928">чай.[NOM]</ta>
            <ta e="T930" id="Seg_10849" s="T929">пить-PRS-3PL</ta>
            <ta e="T931" id="Seg_10850" s="T930">вариться-PTCP.PST</ta>
            <ta e="T932" id="Seg_10851" s="T931">мясо-ACC</ta>
            <ta e="T933" id="Seg_10852" s="T932">есть-PRS-3PL</ta>
            <ta e="T934" id="Seg_10853" s="T933">мальчик.[NOM]</ta>
            <ta e="T935" id="Seg_10854" s="T934">соседтсво-3PL-DAT/LOC</ta>
            <ta e="T936" id="Seg_10855" s="T935">сесть-PST1-3SG</ta>
            <ta e="T937" id="Seg_10856" s="T936">целый</ta>
            <ta e="T938" id="Seg_10857" s="T937">олень-ACC</ta>
            <ta e="T939" id="Seg_10858" s="T938">есть-PST1-3SG</ta>
            <ta e="T940" id="Seg_10859" s="T939">мальчик.[NOM]</ta>
            <ta e="T941" id="Seg_10860" s="T940">девушка-ACC</ta>
            <ta e="T942" id="Seg_10861" s="T941">жена.[NOM]</ta>
            <ta e="T943" id="Seg_10862" s="T942">делать-PST2.[3SG]</ta>
            <ta e="T944" id="Seg_10863" s="T943">счастливый.[NOM]</ta>
            <ta e="T945" id="Seg_10864" s="T944">очень-ADVZ</ta>
            <ta e="T946" id="Seg_10865" s="T945">жить-PST2-3PL</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_10866" s="T1">v-v:mood.pn</ta>
            <ta e="T3" id="Seg_10867" s="T2">n-n:(num).[n:case]</ta>
            <ta e="T4" id="Seg_10868" s="T3">n-n:poss-n:case</ta>
            <ta e="T5" id="Seg_10869" s="T4">n-n&gt;adj.[n:case]</ta>
            <ta e="T6" id="Seg_10870" s="T5">n.[n:case]</ta>
            <ta e="T7" id="Seg_10871" s="T6">v-v:cvb</ta>
            <ta e="T8" id="Seg_10872" s="T7">n-n&gt;adj.[n:case]</ta>
            <ta e="T9" id="Seg_10873" s="T8">n.[n:case]</ta>
            <ta e="T10" id="Seg_10874" s="T9">n-n:poss-n:case</ta>
            <ta e="T11" id="Seg_10875" s="T10">post</ta>
            <ta e="T12" id="Seg_10876" s="T11">adv</ta>
            <ta e="T13" id="Seg_10877" s="T12">v-v:tense-v:poss.pn</ta>
            <ta e="T14" id="Seg_10878" s="T13">n-n:(poss).[n:case]</ta>
            <ta e="T15" id="Seg_10879" s="T14">n-n&gt;adj.[n:case]</ta>
            <ta e="T16" id="Seg_10880" s="T15">n.[n:case]</ta>
            <ta e="T17" id="Seg_10881" s="T16">ptcl</ta>
            <ta e="T18" id="Seg_10882" s="T17">n-n:(num)-n:(poss).[n:case]</ta>
            <ta e="T19" id="Seg_10883" s="T18">adj</ta>
            <ta e="T20" id="Seg_10884" s="T19">n.[n:case]</ta>
            <ta e="T21" id="Seg_10885" s="T20">n-n:(num)-n:(ins)-n:case</ta>
            <ta e="T22" id="Seg_10886" s="T21">n-n&gt;adj-n:(pred.pn)</ta>
            <ta e="T23" id="Seg_10887" s="T22">n.[n:case]</ta>
            <ta e="T24" id="Seg_10888" s="T23">n-n:case</ta>
            <ta e="T25" id="Seg_10889" s="T24">emphpro-pro:(poss).[pro:case]</ta>
            <ta e="T26" id="Seg_10890" s="T25">v-v&gt;v-v:mood.[v:pred.pn]</ta>
            <ta e="T27" id="Seg_10891" s="T26">n-n:poss-n:case</ta>
            <ta e="T28" id="Seg_10892" s="T27">ptcl</ta>
            <ta e="T29" id="Seg_10893" s="T28">adv</ta>
            <ta e="T30" id="Seg_10894" s="T29">v-v:mood.[v:pred.pn]</ta>
            <ta e="T31" id="Seg_10895" s="T30">n.[n:case]</ta>
            <ta e="T32" id="Seg_10896" s="T31">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T33" id="Seg_10897" s="T32">n-n:poss-n:case</ta>
            <ta e="T34" id="Seg_10898" s="T33">v-v:tense.[v:pred.pn]</ta>
            <ta e="T35" id="Seg_10899" s="T34">n.[n:case]</ta>
            <ta e="T36" id="Seg_10900" s="T35">v-v:ptcp-v:poss.pn-v:(case)</ta>
            <ta e="T37" id="Seg_10901" s="T36">post</ta>
            <ta e="T38" id="Seg_10902" s="T37">n-n:(poss).[n:case]</ta>
            <ta e="T39" id="Seg_10903" s="T38">v-v:tense.[v:pred.pn]</ta>
            <ta e="T40" id="Seg_10904" s="T39">n-n:(poss).[n:case]</ta>
            <ta e="T41" id="Seg_10905" s="T40">v-v:tense.[v:pred.pn]</ta>
            <ta e="T42" id="Seg_10906" s="T41">n.[n:case]</ta>
            <ta e="T43" id="Seg_10907" s="T42">n.[n:case]</ta>
            <ta e="T44" id="Seg_10908" s="T43">v-v:mood.[v:pred.pn]</ta>
            <ta e="T45" id="Seg_10909" s="T44">n-n:poss-n:case</ta>
            <ta e="T46" id="Seg_10910" s="T45">v-v:tense-v:poss.pn</ta>
            <ta e="T47" id="Seg_10911" s="T46">n.[n:case]</ta>
            <ta e="T48" id="Seg_10912" s="T47">v-v:tense-v:poss.pn</ta>
            <ta e="T49" id="Seg_10913" s="T48">v-v:tense-v:poss.pn</ta>
            <ta e="T50" id="Seg_10914" s="T49">n-n:(ins)-n:poss-n:case</ta>
            <ta e="T51" id="Seg_10915" s="T50">v-v&gt;v-v:cvb</ta>
            <ta e="T52" id="Seg_10916" s="T51">v-v:tense.[v:pred.pn]</ta>
            <ta e="T53" id="Seg_10917" s="T52">adv</ta>
            <ta e="T54" id="Seg_10918" s="T53">v-v:(ins)-v:(neg).[v:pred.pn]</ta>
            <ta e="T55" id="Seg_10919" s="T54">n.[n:case]</ta>
            <ta e="T56" id="Seg_10920" s="T55">n-n:poss-n:case</ta>
            <ta e="T57" id="Seg_10921" s="T56">v-v:cvb</ta>
            <ta e="T58" id="Seg_10922" s="T57">post</ta>
            <ta e="T59" id="Seg_10923" s="T58">n-n:(ins)-n:poss-n:case</ta>
            <ta e="T60" id="Seg_10924" s="T59">n.[n:case]</ta>
            <ta e="T61" id="Seg_10925" s="T60">n-n:poss-n:case</ta>
            <ta e="T62" id="Seg_10926" s="T61">ptcl</ta>
            <ta e="T63" id="Seg_10927" s="T62">v-v:tense-v:poss.pn</ta>
            <ta e="T64" id="Seg_10928" s="T63">v-v:cvb</ta>
            <ta e="T65" id="Seg_10929" s="T64">v-v:tense-v:poss.pn</ta>
            <ta e="T66" id="Seg_10930" s="T65">v-v:tense-v:poss.pn</ta>
            <ta e="T67" id="Seg_10931" s="T66">n-n:poss-n:case</ta>
            <ta e="T68" id="Seg_10932" s="T67">v-v:cvb</ta>
            <ta e="T69" id="Seg_10933" s="T68">ptcl</ta>
            <ta e="T70" id="Seg_10934" s="T69">v-v:tense-v:poss.pn</ta>
            <ta e="T71" id="Seg_10935" s="T70">n-n&gt;v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T72" id="Seg_10936" s="T71">n-n&gt;v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T73" id="Seg_10937" s="T72">n-n&gt;v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T74" id="Seg_10938" s="T73">v-v:cvb</ta>
            <ta e="T75" id="Seg_10939" s="T74">n.[n:case]</ta>
            <ta e="T76" id="Seg_10940" s="T75">adv</ta>
            <ta e="T77" id="Seg_10941" s="T76">v-v:tense.[v:pred.pn]</ta>
            <ta e="T78" id="Seg_10942" s="T77">v-v:cvb</ta>
            <ta e="T79" id="Seg_10943" s="T78">v-v:tense.[v:pred.pn]</ta>
            <ta e="T80" id="Seg_10944" s="T79">v-v:cvb</ta>
            <ta e="T81" id="Seg_10945" s="T80">v-v:cvb</ta>
            <ta e="T82" id="Seg_10946" s="T81">v-v:cvb</ta>
            <ta e="T83" id="Seg_10947" s="T82">v-v:tense.[v:pred.pn]</ta>
            <ta e="T84" id="Seg_10948" s="T83">n.[n:case]</ta>
            <ta e="T85" id="Seg_10949" s="T84">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T86" id="Seg_10950" s="T85">adv</ta>
            <ta e="T87" id="Seg_10951" s="T86">cardnum</ta>
            <ta e="T88" id="Seg_10952" s="T87">n-n:case</ta>
            <ta e="T89" id="Seg_10953" s="T88">v-v:tense.[v:pred.pn]</ta>
            <ta e="T90" id="Seg_10954" s="T89">v-v:(ins)-v:tense-v:poss.pn</ta>
            <ta e="T91" id="Seg_10955" s="T90">v-v:tense-v:poss.pn</ta>
            <ta e="T92" id="Seg_10956" s="T91">n-n:(poss).[n:case]</ta>
            <ta e="T93" id="Seg_10957" s="T92">v-v:cvb</ta>
            <ta e="T94" id="Seg_10958" s="T93">v-v:tense.[v:pred.pn]</ta>
            <ta e="T95" id="Seg_10959" s="T94">n-n:(poss).[n:case]</ta>
            <ta e="T96" id="Seg_10960" s="T95">adj-n:(poss).[n:case]</ta>
            <ta e="T97" id="Seg_10961" s="T96">v-v:cvb</ta>
            <ta e="T98" id="Seg_10962" s="T97">v-v:tense.[v:pred.pn]</ta>
            <ta e="T99" id="Seg_10963" s="T98">n.[n:case]</ta>
            <ta e="T100" id="Seg_10964" s="T99">v-v:(ins)-v:tense.[v:pred.pn]</ta>
            <ta e="T101" id="Seg_10965" s="T100">n.[n:case]</ta>
            <ta e="T102" id="Seg_10966" s="T101">v-v:mood-v:temp.pn</ta>
            <ta e="T103" id="Seg_10967" s="T102">ptcl</ta>
            <ta e="T104" id="Seg_10968" s="T103">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T105" id="Seg_10969" s="T104">v-v:tense-v:poss.pn</ta>
            <ta e="T106" id="Seg_10970" s="T105">ptcl</ta>
            <ta e="T107" id="Seg_10971" s="T106">v-v:cvb</ta>
            <ta e="T108" id="Seg_10972" s="T107">v-v:tense.[v:pred.pn]</ta>
            <ta e="T109" id="Seg_10973" s="T108">n.[n:case]</ta>
            <ta e="T110" id="Seg_10974" s="T109">n-n&gt;v-v:cvb</ta>
            <ta e="T111" id="Seg_10975" s="T110">v-v:ptcp-v:(case)</ta>
            <ta e="T112" id="Seg_10976" s="T111">n-n&gt;adj.[n:case]</ta>
            <ta e="T113" id="Seg_10977" s="T112">n.[n:case]</ta>
            <ta e="T114" id="Seg_10978" s="T113">n-n:poss-n:case</ta>
            <ta e="T115" id="Seg_10979" s="T114">n.[n:case]</ta>
            <ta e="T116" id="Seg_10980" s="T115">post</ta>
            <ta e="T117" id="Seg_10981" s="T116">v-v:tense.[v:pred.pn]</ta>
            <ta e="T118" id="Seg_10982" s="T117">v-v:tense-v:poss.pn</ta>
            <ta e="T119" id="Seg_10983" s="T118">n-n:case</ta>
            <ta e="T120" id="Seg_10984" s="T119">adj</ta>
            <ta e="T121" id="Seg_10985" s="T120">n.[n:case]</ta>
            <ta e="T122" id="Seg_10986" s="T121">v-v:cvb</ta>
            <ta e="T123" id="Seg_10987" s="T122">v-v:tense.[v:pred.pn]</ta>
            <ta e="T124" id="Seg_10988" s="T123">n.[n:case]</ta>
            <ta e="T125" id="Seg_10989" s="T124">n-n:case</ta>
            <ta e="T126" id="Seg_10990" s="T125">n.[n:case]</ta>
            <ta e="T127" id="Seg_10991" s="T126">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T128" id="Seg_10992" s="T127">v-v:tense.[v:pred.pn]</ta>
            <ta e="T129" id="Seg_10993" s="T128">n-n:case</ta>
            <ta e="T130" id="Seg_10994" s="T129">v-v:(neg).[v:pred.pn]</ta>
            <ta e="T131" id="Seg_10995" s="T130">dempro</ta>
            <ta e="T132" id="Seg_10996" s="T131">n.[n:case]</ta>
            <ta e="T133" id="Seg_10997" s="T132">v-v&gt;v-v:cvb</ta>
            <ta e="T134" id="Seg_10998" s="T133">v-v:cvb-v-v:cvb</ta>
            <ta e="T135" id="Seg_10999" s="T134">adv-n:poss-n:case</ta>
            <ta e="T136" id="Seg_11000" s="T135">v-v:cvb</ta>
            <ta e="T137" id="Seg_11001" s="T136">v-v:tense.[v:pred.pn]</ta>
            <ta e="T138" id="Seg_11002" s="T137">que</ta>
            <ta e="T139" id="Seg_11003" s="T138">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T140" id="Seg_11004" s="T139">ptcl</ta>
            <ta e="T141" id="Seg_11005" s="T140">v-v:tense.[v:pred.pn]</ta>
            <ta e="T142" id="Seg_11006" s="T141">n.[n:case]</ta>
            <ta e="T143" id="Seg_11007" s="T142">n-n:(num)-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T144" id="Seg_11008" s="T143">n-n:case</ta>
            <ta e="T145" id="Seg_11009" s="T144">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T146" id="Seg_11010" s="T145">n.[n:case]</ta>
            <ta e="T147" id="Seg_11011" s="T146">n-n:case</ta>
            <ta e="T148" id="Seg_11012" s="T147">v-v:cvb</ta>
            <ta e="T149" id="Seg_11013" s="T148">v-v:cvb</ta>
            <ta e="T150" id="Seg_11014" s="T149">v-v:tense.[v:pred.pn]</ta>
            <ta e="T151" id="Seg_11015" s="T150">n.[n:case]</ta>
            <ta e="T152" id="Seg_11016" s="T151">n-n:poss-n:case</ta>
            <ta e="T153" id="Seg_11017" s="T152">n.[n:case]</ta>
            <ta e="T154" id="Seg_11018" s="T153">cardnum</ta>
            <ta e="T155" id="Seg_11019" s="T154">n-n:poss-n:case</ta>
            <ta e="T156" id="Seg_11020" s="T155">ptcl</ta>
            <ta e="T157" id="Seg_11021" s="T156">v-v:(ins)-v:tense.[v:pred.pn]</ta>
            <ta e="T158" id="Seg_11022" s="T157">n-n:(num)-n:(poss).[n:case]</ta>
            <ta e="T159" id="Seg_11023" s="T158">v-v:cvb</ta>
            <ta e="T160" id="Seg_11024" s="T159">v-v:tense-v:pred.pn</ta>
            <ta e="T161" id="Seg_11025" s="T160">n.[n:case]</ta>
            <ta e="T162" id="Seg_11026" s="T161">adv</ta>
            <ta e="T163" id="Seg_11027" s="T162">v-v:tense.[v:pred.pn]</ta>
            <ta e="T164" id="Seg_11028" s="T163">n.[n:case]</ta>
            <ta e="T165" id="Seg_11029" s="T164">cardnum-n&gt;v-v:ptcp.[v:poss]</ta>
            <ta e="T166" id="Seg_11030" s="T165">cardnum-cardnum&gt;ordnum-n&gt;v-v:ptcp.[v:poss]</ta>
            <ta e="T167" id="Seg_11031" s="T166">ptcl.[ptcl:pred.pn]</ta>
            <ta e="T168" id="Seg_11032" s="T167">n.[n:case]</ta>
            <ta e="T169" id="Seg_11033" s="T168">n-n:poss-n:case</ta>
            <ta e="T170" id="Seg_11034" s="T169">v-v:cvb</ta>
            <ta e="T171" id="Seg_11035" s="T170">v-v:cvb</ta>
            <ta e="T172" id="Seg_11036" s="T171">n-n:case</ta>
            <ta e="T173" id="Seg_11037" s="T172">v-v:tense-v:poss.pn</ta>
            <ta e="T174" id="Seg_11038" s="T173">n.[n:case]</ta>
            <ta e="T175" id="Seg_11039" s="T174">n-n&gt;adj.[n:case]</ta>
            <ta e="T176" id="Seg_11040" s="T175">v-v:tense.[v:pred.pn]</ta>
            <ta e="T177" id="Seg_11041" s="T176">n.[n:case]</ta>
            <ta e="T178" id="Seg_11042" s="T177">n-n:case</ta>
            <ta e="T179" id="Seg_11043" s="T178">v-v:cvb-v-v:cvb</ta>
            <ta e="T180" id="Seg_11044" s="T179">v-v:cvb</ta>
            <ta e="T181" id="Seg_11045" s="T180">v-v:tense.[v:pred.pn]</ta>
            <ta e="T182" id="Seg_11046" s="T181">n-n:poss-n:case</ta>
            <ta e="T183" id="Seg_11047" s="T182">n.[n:case]</ta>
            <ta e="T184" id="Seg_11048" s="T183">n-n&gt;adj.[n:case]</ta>
            <ta e="T185" id="Seg_11049" s="T184">post</ta>
            <ta e="T186" id="Seg_11050" s="T185">n.[n:case]</ta>
            <ta e="T187" id="Seg_11051" s="T186">v-v:tense.[v:pred.pn]</ta>
            <ta e="T188" id="Seg_11052" s="T187">n.[n:case]</ta>
            <ta e="T189" id="Seg_11053" s="T188">n.[n:case]</ta>
            <ta e="T190" id="Seg_11054" s="T189">n.[n:case]</ta>
            <ta e="T191" id="Seg_11055" s="T190">n-n:case</ta>
            <ta e="T192" id="Seg_11056" s="T191">n.[n:case]</ta>
            <ta e="T193" id="Seg_11057" s="T192">n-n:poss-n:case</ta>
            <ta e="T194" id="Seg_11058" s="T193">v-v:tense.[v:pred.pn]</ta>
            <ta e="T195" id="Seg_11059" s="T194">n-n:(poss).[n:case]</ta>
            <ta e="T196" id="Seg_11060" s="T195">cardnum</ta>
            <ta e="T197" id="Seg_11061" s="T196">n.[n:case]</ta>
            <ta e="T198" id="Seg_11062" s="T197">v-v:cvb</ta>
            <ta e="T199" id="Seg_11063" s="T198">v-v:tense.[v:pred.pn]</ta>
            <ta e="T200" id="Seg_11064" s="T199">v-v:cvb</ta>
            <ta e="T201" id="Seg_11065" s="T200">v-v:tense-v:poss.pn</ta>
            <ta e="T202" id="Seg_11066" s="T201">n.[n:case]</ta>
            <ta e="T203" id="Seg_11067" s="T202">v-v:tense.[v:pred.pn]</ta>
            <ta e="T204" id="Seg_11068" s="T203">adv</ta>
            <ta e="T205" id="Seg_11069" s="T204">v-v:cvb</ta>
            <ta e="T206" id="Seg_11070" s="T205">n-n&gt;adj.[n:case]</ta>
            <ta e="T207" id="Seg_11071" s="T206">n-n:poss-n:case</ta>
            <ta e="T208" id="Seg_11072" s="T207">v-v:tense.[v:pred.pn]</ta>
            <ta e="T209" id="Seg_11073" s="T208">n.[n:case]</ta>
            <ta e="T210" id="Seg_11074" s="T209">n.[n:case]</ta>
            <ta e="T211" id="Seg_11075" s="T210">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T212" id="Seg_11076" s="T211">v-v:ptcp</ta>
            <ta e="T213" id="Seg_11077" s="T212">n.[n:case]</ta>
            <ta e="T214" id="Seg_11078" s="T213">v-v:tense-v:poss.pn</ta>
            <ta e="T215" id="Seg_11079" s="T214">v-v:tense.[v:pred.pn]</ta>
            <ta e="T216" id="Seg_11080" s="T215">que</ta>
            <ta e="T217" id="Seg_11081" s="T216">ptcl</ta>
            <ta e="T218" id="Seg_11082" s="T217">v-v:cvb</ta>
            <ta e="T219" id="Seg_11083" s="T218">n-n&gt;n.[n:case]</ta>
            <ta e="T220" id="Seg_11084" s="T219">adv</ta>
            <ta e="T221" id="Seg_11085" s="T220">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T222" id="Seg_11086" s="T221">v-v:tense.[v:pred.pn]</ta>
            <ta e="T223" id="Seg_11087" s="T222">v-v:cvb</ta>
            <ta e="T224" id="Seg_11088" s="T223">v-v:tense-v:poss.pn</ta>
            <ta e="T225" id="Seg_11089" s="T224">v-v:tense-v:poss.pn</ta>
            <ta e="T226" id="Seg_11090" s="T225">que-pro:case</ta>
            <ta e="T227" id="Seg_11091" s="T226">v-v:cvb</ta>
            <ta e="T228" id="Seg_11092" s="T227">v-v:tense-v:pred.pn</ta>
            <ta e="T229" id="Seg_11093" s="T228">v-v:tense.[v:pred.pn]</ta>
            <ta e="T230" id="Seg_11094" s="T229">n.[n:case]</ta>
            <ta e="T231" id="Seg_11095" s="T230">n.[n:case]</ta>
            <ta e="T232" id="Seg_11096" s="T231">n.[n:case]</ta>
            <ta e="T233" id="Seg_11097" s="T232">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T234" id="Seg_11098" s="T233">adv</ta>
            <ta e="T235" id="Seg_11099" s="T234">n-n&gt;adj-n:(pred.pn)</ta>
            <ta e="T236" id="Seg_11100" s="T235">ptcl</ta>
            <ta e="T237" id="Seg_11101" s="T236">que</ta>
            <ta e="T238" id="Seg_11102" s="T237">n-n:(pred.pn)=ptcl</ta>
            <ta e="T239" id="Seg_11103" s="T238">n-n:poss-n:case</ta>
            <ta e="T240" id="Seg_11104" s="T239">n-n:poss-n:case</ta>
            <ta e="T241" id="Seg_11105" s="T240">post</ta>
            <ta e="T242" id="Seg_11106" s="T241">v-v:tense-v:pred.pn</ta>
            <ta e="T243" id="Seg_11107" s="T242">v-v:tense.[v:pred.pn]</ta>
            <ta e="T244" id="Seg_11108" s="T243">n.[n:case]</ta>
            <ta e="T245" id="Seg_11109" s="T244">v.[v:mood.pn]</ta>
            <ta e="T246" id="Seg_11110" s="T245">v-v:tense-v:poss.pn</ta>
            <ta e="T247" id="Seg_11111" s="T246">n.[n:case]</ta>
            <ta e="T248" id="Seg_11112" s="T247">v.[v:mood.pn]</ta>
            <ta e="T249" id="Seg_11113" s="T248">n.[n:case]</ta>
            <ta e="T250" id="Seg_11114" s="T249">n-n:poss-n:case</ta>
            <ta e="T251" id="Seg_11115" s="T250">v-v:tense.[v:pred.pn]</ta>
            <ta e="T252" id="Seg_11116" s="T251">n-n&gt;adj</ta>
            <ta e="T253" id="Seg_11117" s="T252">n.[n:case]</ta>
            <ta e="T254" id="Seg_11118" s="T253">adv</ta>
            <ta e="T255" id="Seg_11119" s="T254">v-v:cvb-v:pred.pn</ta>
            <ta e="T256" id="Seg_11120" s="T255">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T257" id="Seg_11121" s="T256">interj</ta>
            <ta e="T258" id="Seg_11122" s="T257">n-n:case</ta>
            <ta e="T259" id="Seg_11123" s="T258">v-v:tense-v:poss.pn</ta>
            <ta e="T260" id="Seg_11124" s="T259">v-v:tense-v:pred.pn</ta>
            <ta e="T261" id="Seg_11125" s="T260">adv</ta>
            <ta e="T262" id="Seg_11126" s="T261">adv</ta>
            <ta e="T263" id="Seg_11127" s="T262">n-n&gt;adj.[n:case]</ta>
            <ta e="T264" id="Seg_11128" s="T263">n.[n:case]</ta>
            <ta e="T265" id="Seg_11129" s="T264">n-n&gt;adj.[n:case]</ta>
            <ta e="T266" id="Seg_11130" s="T265">v-v:(ins)-v&gt;v-v:cvb=ptcl.[ptcl:pred.pn]</ta>
            <ta e="T267" id="Seg_11131" s="T266">n.[n:case]</ta>
            <ta e="T268" id="Seg_11132" s="T267">v-v:tense.[v:pred.pn]</ta>
            <ta e="T269" id="Seg_11133" s="T268">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T270" id="Seg_11134" s="T269">v-v:cvb</ta>
            <ta e="T271" id="Seg_11135" s="T270">v-v:tense-v:poss.pn</ta>
            <ta e="T272" id="Seg_11136" s="T271">n-n:(num)-n:(poss).[n:case]</ta>
            <ta e="T273" id="Seg_11137" s="T272">ptcl</ta>
            <ta e="T274" id="Seg_11138" s="T273">v-v:cvb</ta>
            <ta e="T275" id="Seg_11139" s="T274">v-v:tense-v:pred.pn</ta>
            <ta e="T276" id="Seg_11140" s="T275">n.[n:case]</ta>
            <ta e="T277" id="Seg_11141" s="T276">n-n:poss-n:case</ta>
            <ta e="T278" id="Seg_11142" s="T277">v-v:tense-v:pred.pn</ta>
            <ta e="T279" id="Seg_11143" s="T278">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T280" id="Seg_11144" s="T279">que</ta>
            <ta e="T281" id="Seg_11145" s="T280">n.[n:case]</ta>
            <ta e="T282" id="Seg_11146" s="T281">v-v:tense-v:poss.pn</ta>
            <ta e="T283" id="Seg_11147" s="T282">ptcl</ta>
            <ta e="T284" id="Seg_11148" s="T283">adv</ta>
            <ta e="T285" id="Seg_11149" s="T284">n.[n:case]</ta>
            <ta e="T286" id="Seg_11150" s="T285">v.[v:mood.pn]</ta>
            <ta e="T287" id="Seg_11151" s="T286">v-v:tense.[v:pred.pn]</ta>
            <ta e="T288" id="Seg_11152" s="T287">n.[n:case]</ta>
            <ta e="T289" id="Seg_11153" s="T288">adv</ta>
            <ta e="T290" id="Seg_11154" s="T289">v-v:(ins)-v&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T291" id="Seg_11155" s="T290">n-n:(poss).[n:case]</ta>
            <ta e="T292" id="Seg_11156" s="T291">v-v:cvb</ta>
            <ta e="T293" id="Seg_11157" s="T292">n-n:case</ta>
            <ta e="T294" id="Seg_11158" s="T293">v-v:tense.[v:pred.pn]</ta>
            <ta e="T295" id="Seg_11159" s="T294">n-n&gt;adj.[n:case]</ta>
            <ta e="T296" id="Seg_11160" s="T295">n-n:case</ta>
            <ta e="T297" id="Seg_11161" s="T296">n.[n:case]</ta>
            <ta e="T298" id="Seg_11162" s="T297">dempro-pro:case</ta>
            <ta e="T299" id="Seg_11163" s="T298">adv</ta>
            <ta e="T300" id="Seg_11164" s="T299">v-v:tense.[v:pred.pn]</ta>
            <ta e="T301" id="Seg_11165" s="T300">v-v:tense-v:poss.pn</ta>
            <ta e="T302" id="Seg_11166" s="T301">v-v:tense-v:pred.pn</ta>
            <ta e="T303" id="Seg_11167" s="T302">v-v:tense-v:poss.pn</ta>
            <ta e="T304" id="Seg_11168" s="T303">n-n:poss-n:case</ta>
            <ta e="T305" id="Seg_11169" s="T304">v-v:cvb</ta>
            <ta e="T306" id="Seg_11170" s="T305">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T307" id="Seg_11171" s="T306">adv</ta>
            <ta e="T308" id="Seg_11172" s="T307">v-v:tense-v:poss.pn</ta>
            <ta e="T309" id="Seg_11173" s="T308">cardnum</ta>
            <ta e="T310" id="Seg_11174" s="T309">n-n:case</ta>
            <ta e="T311" id="Seg_11175" s="T310">n.[n:case]</ta>
            <ta e="T312" id="Seg_11176" s="T311">v-v:tense.[v:pred.pn]</ta>
            <ta e="T313" id="Seg_11177" s="T312">v-v:(ins)-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T314" id="Seg_11178" s="T313">n.[n:case]</ta>
            <ta e="T315" id="Seg_11179" s="T314">ptcl</ta>
            <ta e="T316" id="Seg_11180" s="T315">adj.[n:case]</ta>
            <ta e="T317" id="Seg_11181" s="T316">n-n:case</ta>
            <ta e="T318" id="Seg_11182" s="T317">v-v:cvb</ta>
            <ta e="T319" id="Seg_11183" s="T318">v-v&gt;v-v:tense.[v:pred.pn]</ta>
            <ta e="T320" id="Seg_11184" s="T319">n.[n:case]</ta>
            <ta e="T321" id="Seg_11185" s="T320">cardnum</ta>
            <ta e="T322" id="Seg_11186" s="T321">n-n&gt;adj.[n:case]</ta>
            <ta e="T323" id="Seg_11187" s="T322">n-n:case</ta>
            <ta e="T324" id="Seg_11188" s="T323">adj-adj&gt;v-v:tense.[v:pred.pn]</ta>
            <ta e="T325" id="Seg_11189" s="T324">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T326" id="Seg_11190" s="T325">n.[n:case]</ta>
            <ta e="T327" id="Seg_11191" s="T326">v-v:tense.[v:pred.pn]</ta>
            <ta e="T328" id="Seg_11192" s="T327">que</ta>
            <ta e="T329" id="Seg_11193" s="T328">ptcl</ta>
            <ta e="T330" id="Seg_11194" s="T329">n.[n:case]</ta>
            <ta e="T331" id="Seg_11195" s="T330">n-n:(poss).[n:case]</ta>
            <ta e="T332" id="Seg_11196" s="T331">ptcl</ta>
            <ta e="T333" id="Seg_11197" s="T332">ptcl</ta>
            <ta e="T334" id="Seg_11198" s="T333">adv</ta>
            <ta e="T335" id="Seg_11199" s="T334">v-v:tense-v:poss.pn</ta>
            <ta e="T336" id="Seg_11200" s="T335">n.[n:case]</ta>
            <ta e="T337" id="Seg_11201" s="T336">n-n:poss-n:case</ta>
            <ta e="T338" id="Seg_11202" s="T337">n.[n:case]</ta>
            <ta e="T339" id="Seg_11203" s="T338">n-n:case</ta>
            <ta e="T340" id="Seg_11204" s="T339">v-v:tense-v:poss.pn</ta>
            <ta e="T341" id="Seg_11205" s="T340">n.[n:case]</ta>
            <ta e="T342" id="Seg_11206" s="T341">n-n:poss-n:case</ta>
            <ta e="T343" id="Seg_11207" s="T342">v-v:cvb</ta>
            <ta e="T344" id="Seg_11208" s="T343">v-v:tense-v:poss.pn</ta>
            <ta e="T345" id="Seg_11209" s="T344">que</ta>
            <ta e="T346" id="Seg_11210" s="T345">adv-adv&gt;adv</ta>
            <ta e="T347" id="Seg_11211" s="T346">v-v:tense-v:poss.pn</ta>
            <ta e="T348" id="Seg_11212" s="T347">ptcl</ta>
            <ta e="T349" id="Seg_11213" s="T348">ptcl</ta>
            <ta e="T350" id="Seg_11214" s="T349">n.[n:case]</ta>
            <ta e="T351" id="Seg_11215" s="T350">n.[n:case]</ta>
            <ta e="T352" id="Seg_11216" s="T351">n-n:case</ta>
            <ta e="T353" id="Seg_11217" s="T352">v-v:tense.[v:pred.pn]</ta>
            <ta e="T354" id="Seg_11218" s="T353">n-n:case</ta>
            <ta e="T355" id="Seg_11219" s="T354">v-v:cvb</ta>
            <ta e="T356" id="Seg_11220" s="T355">v-v:tense-v:poss.pn</ta>
            <ta e="T357" id="Seg_11221" s="T356">adj.[n:case]</ta>
            <ta e="T358" id="Seg_11222" s="T357">ptcl</ta>
            <ta e="T359" id="Seg_11223" s="T358">n.[n:case]</ta>
            <ta e="T360" id="Seg_11224" s="T359">v-v:tense.[v:pred.pn]</ta>
            <ta e="T361" id="Seg_11225" s="T360">n-n:poss-n:case</ta>
            <ta e="T362" id="Seg_11226" s="T361">n.[n:case]</ta>
            <ta e="T363" id="Seg_11227" s="T362">v-v:tense.[v:pred.pn]</ta>
            <ta e="T364" id="Seg_11228" s="T363">n-n:poss-n:case</ta>
            <ta e="T365" id="Seg_11229" s="T364">n-n:poss-n:case</ta>
            <ta e="T366" id="Seg_11230" s="T365">n.[n:case]</ta>
            <ta e="T367" id="Seg_11231" s="T366">v-v&gt;v-v:(ins)-v:tense.[v:pred.pn]</ta>
            <ta e="T368" id="Seg_11232" s="T367">n.[n:case]</ta>
            <ta e="T369" id="Seg_11233" s="T368">n.[n:case]</ta>
            <ta e="T370" id="Seg_11234" s="T369">n-n:poss-n:case</ta>
            <ta e="T371" id="Seg_11235" s="T370">post</ta>
            <ta e="T372" id="Seg_11236" s="T371">n.[n:case]</ta>
            <ta e="T373" id="Seg_11237" s="T372">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T374" id="Seg_11238" s="T373">adj.[n:case]</ta>
            <ta e="T375" id="Seg_11239" s="T374">n.[n:case]</ta>
            <ta e="T376" id="Seg_11240" s="T375">v-v:cvb</ta>
            <ta e="T377" id="Seg_11241" s="T376">v-v:tense.[v:pred.pn]</ta>
            <ta e="T378" id="Seg_11242" s="T377">n-n:case</ta>
            <ta e="T379" id="Seg_11243" s="T378">n-n&gt;adj.[n:case]</ta>
            <ta e="T380" id="Seg_11244" s="T379">n-n&gt;v-v:cvb</ta>
            <ta e="T381" id="Seg_11245" s="T380">v-v:tense.[v:pred.pn]</ta>
            <ta e="T382" id="Seg_11246" s="T381">n.[n:case]</ta>
            <ta e="T383" id="Seg_11247" s="T382">n-n&gt;adj.[n:case]</ta>
            <ta e="T384" id="Seg_11248" s="T383">v-v:tense.[v:pred.pn]</ta>
            <ta e="T385" id="Seg_11249" s="T384">n.[n:case]</ta>
            <ta e="T386" id="Seg_11250" s="T385">v-v:tense-v:poss.pn</ta>
            <ta e="T387" id="Seg_11251" s="T386">n-n&gt;n-n:(num).[n:case]</ta>
            <ta e="T388" id="Seg_11252" s="T387">n-n&gt;v-v:mood.pn</ta>
            <ta e="T389" id="Seg_11253" s="T388">n-n:case</ta>
            <ta e="T390" id="Seg_11254" s="T389">v-v:(ins)-v:mood.pn</ta>
            <ta e="T391" id="Seg_11255" s="T390">n-n:(poss).[n:case]</ta>
            <ta e="T392" id="Seg_11256" s="T391">emphpro-pro:(poss).[pro:case]</ta>
            <ta e="T393" id="Seg_11257" s="T392">v-v:tense-v:pred.pn</ta>
            <ta e="T394" id="Seg_11258" s="T393">adv</ta>
            <ta e="T395" id="Seg_11259" s="T394">n.[n:case]</ta>
            <ta e="T396" id="Seg_11260" s="T395">v-v:tense-v:poss.pn</ta>
            <ta e="T397" id="Seg_11261" s="T396">adj.[n:case]</ta>
            <ta e="T398" id="Seg_11262" s="T397">n-n:(num).[n:case]</ta>
            <ta e="T399" id="Seg_11263" s="T398">v-v:tense.[v:pred.pn]</ta>
            <ta e="T400" id="Seg_11264" s="T399">n.[n:case]</ta>
            <ta e="T401" id="Seg_11265" s="T400">v-v:cvb</ta>
            <ta e="T402" id="Seg_11266" s="T401">n-n:(num)-n:case</ta>
            <ta e="T403" id="Seg_11267" s="T402">n-n:poss-n:case</ta>
            <ta e="T404" id="Seg_11268" s="T403">v-v:tense-v:poss.pn</ta>
            <ta e="T405" id="Seg_11269" s="T404">n.[n:case]</ta>
            <ta e="T406" id="Seg_11270" s="T405">v-v:tense-v:poss.pn</ta>
            <ta e="T407" id="Seg_11271" s="T406">n.[n:case]</ta>
            <ta e="T408" id="Seg_11272" s="T407">n-n&gt;v.[v:mood.pn]</ta>
            <ta e="T409" id="Seg_11273" s="T408">n.[n:case]</ta>
            <ta e="T410" id="Seg_11274" s="T409">n.[n:case]</ta>
            <ta e="T411" id="Seg_11275" s="T410">adj</ta>
            <ta e="T412" id="Seg_11276" s="T411">n-n:(poss).[n:case]</ta>
            <ta e="T413" id="Seg_11277" s="T412">v-v&gt;adv</ta>
            <ta e="T414" id="Seg_11278" s="T413">v-v:tense-v:poss.pn</ta>
            <ta e="T415" id="Seg_11279" s="T414">n.[n:case]</ta>
            <ta e="T416" id="Seg_11280" s="T415">adv</ta>
            <ta e="T417" id="Seg_11281" s="T416">v-v:tense.[v:pred.pn]</ta>
            <ta e="T418" id="Seg_11282" s="T417">n.[n:case]</ta>
            <ta e="T419" id="Seg_11283" s="T418">n-n&gt;v.[v:mood.pn]</ta>
            <ta e="T420" id="Seg_11284" s="T419">n-n&gt;v.[v:mood.pn]</ta>
            <ta e="T421" id="Seg_11285" s="T420">n.[n:case]</ta>
            <ta e="T422" id="Seg_11286" s="T421">v-v:tense-v:poss.pn</ta>
            <ta e="T423" id="Seg_11287" s="T422">n-n:case</ta>
            <ta e="T424" id="Seg_11288" s="T423">v.[v:mood.pn]</ta>
            <ta e="T425" id="Seg_11289" s="T424">n.[n:case]</ta>
            <ta e="T426" id="Seg_11290" s="T425">n.[n:case]</ta>
            <ta e="T427" id="Seg_11291" s="T426">adj-n:(poss).[n:case]</ta>
            <ta e="T428" id="Seg_11292" s="T427">v-v:tense-v:poss.pn</ta>
            <ta e="T429" id="Seg_11293" s="T428">n.[n:case]</ta>
            <ta e="T430" id="Seg_11294" s="T429">n.[n:case]</ta>
            <ta e="T431" id="Seg_11295" s="T430">n-n:case</ta>
            <ta e="T432" id="Seg_11296" s="T431">v-v:tense-v:poss.pn</ta>
            <ta e="T433" id="Seg_11297" s="T432">v-v&gt;v-v:(ins)-v:(neg).[v:mood.pn]</ta>
            <ta e="T434" id="Seg_11298" s="T433">adv</ta>
            <ta e="T435" id="Seg_11299" s="T434">v-v:(ins)-v&gt;v.[v:mood.pn]</ta>
            <ta e="T436" id="Seg_11300" s="T435">ptcl</ta>
            <ta e="T437" id="Seg_11301" s="T436">n.[n:case]</ta>
            <ta e="T438" id="Seg_11302" s="T437">n.[n:case]</ta>
            <ta e="T439" id="Seg_11303" s="T438">n-n:case</ta>
            <ta e="T440" id="Seg_11304" s="T439">v-v:cvb</ta>
            <ta e="T441" id="Seg_11305" s="T440">n.[n:case]</ta>
            <ta e="T442" id="Seg_11306" s="T441">n-n:poss-n:case</ta>
            <ta e="T443" id="Seg_11307" s="T442">v-v:tense-v:poss.pn</ta>
            <ta e="T444" id="Seg_11308" s="T443">n.[n:case]</ta>
            <ta e="T445" id="Seg_11309" s="T444">n.[n:case]</ta>
            <ta e="T446" id="Seg_11310" s="T445">v-v:tense-v:poss.pn</ta>
            <ta e="T447" id="Seg_11311" s="T446">n-n:(poss).[n:case]</ta>
            <ta e="T448" id="Seg_11312" s="T447">v-v:(neg)-v:tense-v:poss.pn</ta>
            <ta e="T449" id="Seg_11313" s="T448">n.[n:case]</ta>
            <ta e="T450" id="Seg_11314" s="T449">ptcl</ta>
            <ta e="T451" id="Seg_11315" s="T450">adv</ta>
            <ta e="T452" id="Seg_11316" s="T451">n.[n:case]</ta>
            <ta e="T453" id="Seg_11317" s="T452">v-v:tense-v:poss.pn</ta>
            <ta e="T454" id="Seg_11318" s="T453">adv</ta>
            <ta e="T455" id="Seg_11319" s="T454">v-v:(ins)-v&gt;v-v:(ins)-v:(neg).[v:mood.pn]</ta>
            <ta e="T456" id="Seg_11320" s="T455">v-v:tense.[v:pred.pn]</ta>
            <ta e="T0" id="Seg_11321" s="T456">n.[n:case]</ta>
            <ta e="T947" id="Seg_11322" s="T0">adj.[n:case]</ta>
            <ta e="T948" id="Seg_11323" s="T947">n-n:(poss)-n:case</ta>
            <ta e="T457" id="Seg_11324" s="T948">v-v:tense-v:poss.pn</ta>
            <ta e="T458" id="Seg_11325" s="T457">n.[n:case]</ta>
            <ta e="T459" id="Seg_11326" s="T458">v-v:tense.[v:pred.pn]</ta>
            <ta e="T460" id="Seg_11327" s="T459">n.[n:case]</ta>
            <ta e="T461" id="Seg_11328" s="T460">n-n:(poss).[n:case]</ta>
            <ta e="T462" id="Seg_11329" s="T461">n.[n:case]</ta>
            <ta e="T463" id="Seg_11330" s="T462">n.[n:case]</ta>
            <ta e="T464" id="Seg_11331" s="T463">n-n:case</ta>
            <ta e="T465" id="Seg_11332" s="T464">n.[n:case]</ta>
            <ta e="T466" id="Seg_11333" s="T465">n-n:poss-n:case</ta>
            <ta e="T467" id="Seg_11334" s="T466">v-v:cvb</ta>
            <ta e="T468" id="Seg_11335" s="T467">v-v:tense-v:poss.pn</ta>
            <ta e="T469" id="Seg_11336" s="T468">n-n:(poss).[n:case]</ta>
            <ta e="T470" id="Seg_11337" s="T469">ptcl</ta>
            <ta e="T471" id="Seg_11338" s="T470">v-v:tense-v:poss.pn</ta>
            <ta e="T472" id="Seg_11339" s="T471">n.[n:case]</ta>
            <ta e="T473" id="Seg_11340" s="T472">v-v:tense-v:poss.pn</ta>
            <ta e="T474" id="Seg_11341" s="T473">n-n:case</ta>
            <ta e="T475" id="Seg_11342" s="T474">adv</ta>
            <ta e="T476" id="Seg_11343" s="T475">v.[v:mood.pn]</ta>
            <ta e="T477" id="Seg_11344" s="T476">adv</ta>
            <ta e="T478" id="Seg_11345" s="T477">n-n&gt;adj</ta>
            <ta e="T479" id="Seg_11346" s="T478">n.[n:case]</ta>
            <ta e="T480" id="Seg_11347" s="T479">v-v:tense.[v:pred.pn]</ta>
            <ta e="T481" id="Seg_11348" s="T480">n.[n:case]</ta>
            <ta e="T482" id="Seg_11349" s="T481">adv</ta>
            <ta e="T483" id="Seg_11350" s="T482">v-v:cvb</ta>
            <ta e="T484" id="Seg_11351" s="T483">que.[pro:case]</ta>
            <ta e="T485" id="Seg_11352" s="T484">ptcl</ta>
            <ta e="T486" id="Seg_11353" s="T485">n-n&gt;adj</ta>
            <ta e="T487" id="Seg_11354" s="T486">n-n:poss-n:case</ta>
            <ta e="T488" id="Seg_11355" s="T487">v-v:(neg).[v:pred.pn]</ta>
            <ta e="T489" id="Seg_11356" s="T488">que</ta>
            <ta e="T490" id="Seg_11357" s="T489">ptcl</ta>
            <ta e="T491" id="Seg_11358" s="T490">n.[n:case]</ta>
            <ta e="T492" id="Seg_11359" s="T491">n.[n:case]</ta>
            <ta e="T493" id="Seg_11360" s="T492">ptcl</ta>
            <ta e="T494" id="Seg_11361" s="T493">n-n:(poss).[n:case]</ta>
            <ta e="T495" id="Seg_11362" s="T494">v-v:tense.[v:pred.pn]</ta>
            <ta e="T496" id="Seg_11363" s="T495">n-n:(poss)-n:case</ta>
            <ta e="T497" id="Seg_11364" s="T496">post</ta>
            <ta e="T498" id="Seg_11365" s="T497">n.[n:case]</ta>
            <ta e="T499" id="Seg_11366" s="T498">n-n:(poss).[n:case]</ta>
            <ta e="T500" id="Seg_11367" s="T499">v-v:(ins)-v:tense.[v:pred.pn]</ta>
            <ta e="T501" id="Seg_11368" s="T500">v.[v:mood.pn]</ta>
            <ta e="T502" id="Seg_11369" s="T501">n-n:poss-n:case</ta>
            <ta e="T503" id="Seg_11370" s="T502">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T504" id="Seg_11371" s="T503">post</ta>
            <ta e="T505" id="Seg_11372" s="T504">v-v:(ins)-v:(neg).[v:mood.pn]</ta>
            <ta e="T506" id="Seg_11373" s="T505">n.[n:case]</ta>
            <ta e="T507" id="Seg_11374" s="T506">adv</ta>
            <ta e="T508" id="Seg_11375" s="T507">v-v:(neg).[v:mood.pn]</ta>
            <ta e="T509" id="Seg_11376" s="T508">n-n:(num)-n:poss-n:case</ta>
            <ta e="T510" id="Seg_11377" s="T509">v-v:(ins)-v&gt;v-v:(ins)-v:(neg).[v:mood.pn]</ta>
            <ta e="T511" id="Seg_11378" s="T510">n.[n:case]</ta>
            <ta e="T512" id="Seg_11379" s="T511">n-n:poss-n:case</ta>
            <ta e="T513" id="Seg_11380" s="T512">v-v:tense-v:poss.pn</ta>
            <ta e="T514" id="Seg_11381" s="T513">n-n:case</ta>
            <ta e="T515" id="Seg_11382" s="T514">v-v:tense-v:poss.pn</ta>
            <ta e="T516" id="Seg_11383" s="T515">v-v:(ins)-v:ptcp</ta>
            <ta e="T517" id="Seg_11384" s="T516">n-n:case</ta>
            <ta e="T518" id="Seg_11385" s="T517">v-v:tense-v:poss.pn</ta>
            <ta e="T519" id="Seg_11386" s="T518">n-n&gt;adj.[n:case]</ta>
            <ta e="T520" id="Seg_11387" s="T519">n.[n:case]</ta>
            <ta e="T521" id="Seg_11388" s="T520">v-v:tense-v:poss.pn</ta>
            <ta e="T522" id="Seg_11389" s="T521">que-pro:case</ta>
            <ta e="T523" id="Seg_11390" s="T522">v-v:tense-v:poss.pn</ta>
            <ta e="T524" id="Seg_11391" s="T523">ptcl</ta>
            <ta e="T525" id="Seg_11392" s="T524">que.[pro:case]</ta>
            <ta e="T526" id="Seg_11393" s="T525">v-v:tense-v:poss.pn=ptcl</ta>
            <ta e="T527" id="Seg_11394" s="T526">n-n&gt;adj.[n:case]</ta>
            <ta e="T528" id="Seg_11395" s="T527">n-n:(poss).[n:case]</ta>
            <ta e="T529" id="Seg_11396" s="T528">emphpro-pro:(poss).[pro:case]</ta>
            <ta e="T530" id="Seg_11397" s="T529">v-v:tense-v:pred.pn</ta>
            <ta e="T531" id="Seg_11398" s="T530">n.[n:case]</ta>
            <ta e="T532" id="Seg_11399" s="T531">v-v:(neg).[v:pred.pn]</ta>
            <ta e="T533" id="Seg_11400" s="T532">n-n:poss-n:case</ta>
            <ta e="T534" id="Seg_11401" s="T533">n-n:poss-n:case</ta>
            <ta e="T535" id="Seg_11402" s="T534">n-n:poss-n:case</ta>
            <ta e="T536" id="Seg_11403" s="T535">v-v:tense-v:poss.pn</ta>
            <ta e="T537" id="Seg_11404" s="T536">adv</ta>
            <ta e="T538" id="Seg_11405" s="T537">adj.[n:case]</ta>
            <ta e="T539" id="Seg_11406" s="T538">adj.[n:case]</ta>
            <ta e="T540" id="Seg_11407" s="T539">n.[n:case]</ta>
            <ta e="T541" id="Seg_11408" s="T540">v-v:tense.[v:pred.pn]</ta>
            <ta e="T542" id="Seg_11409" s="T541">v-v:tense.[v:pred.pn]</ta>
            <ta e="T543" id="Seg_11410" s="T542">dempro-pro:(poss)-pro:(poss).[pro:case]</ta>
            <ta e="T544" id="Seg_11411" s="T543">v-v:(ins)-v&gt;v-v:tense.[v:pred.pn]</ta>
            <ta e="T545" id="Seg_11412" s="T544">n.[n:case]</ta>
            <ta e="T546" id="Seg_11413" s="T545">n-n:poss-n:case</ta>
            <ta e="T547" id="Seg_11414" s="T546">v-v:tense-v:poss.pn</ta>
            <ta e="T548" id="Seg_11415" s="T547">n-n:case</ta>
            <ta e="T549" id="Seg_11416" s="T548">v-v:ptcp</ta>
            <ta e="T550" id="Seg_11417" s="T549">n.[n:case]</ta>
            <ta e="T551" id="Seg_11418" s="T550">v-v:tense.[v:pred.pn]</ta>
            <ta e="T552" id="Seg_11419" s="T551">adv</ta>
            <ta e="T553" id="Seg_11420" s="T552">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T554" id="Seg_11421" s="T553">post</ta>
            <ta e="T555" id="Seg_11422" s="T554">v-v:cvb</ta>
            <ta e="T556" id="Seg_11423" s="T555">v-v:tense-v:poss.pn</ta>
            <ta e="T557" id="Seg_11424" s="T556">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T558" id="Seg_11425" s="T557">post</ta>
            <ta e="T559" id="Seg_11426" s="T558">n-n:(poss).[n:case]</ta>
            <ta e="T560" id="Seg_11427" s="T559">ptcl</ta>
            <ta e="T561" id="Seg_11428" s="T560">v-v:cvb</ta>
            <ta e="T562" id="Seg_11429" s="T561">que</ta>
            <ta e="T563" id="Seg_11430" s="T562">v-v:tense-v:poss.pn</ta>
            <ta e="T564" id="Seg_11431" s="T563">que</ta>
            <ta e="T565" id="Seg_11432" s="T564">v-v:tense-v:poss.pn</ta>
            <ta e="T566" id="Seg_11433" s="T565">cardnum</ta>
            <ta e="T567" id="Seg_11434" s="T566">n.[n:case]</ta>
            <ta e="T568" id="Seg_11435" s="T567">n-n&gt;adj.[n:case]</ta>
            <ta e="T569" id="Seg_11436" s="T568">v-v:tense-v:poss.pn</ta>
            <ta e="T570" id="Seg_11437" s="T569">v-v:tense.[v:pred.pn]</ta>
            <ta e="T571" id="Seg_11438" s="T570">n-n:(poss).[n:case]</ta>
            <ta e="T572" id="Seg_11439" s="T571">v-v:cvb</ta>
            <ta e="T573" id="Seg_11440" s="T572">v-v:cvb</ta>
            <ta e="T574" id="Seg_11441" s="T573">v-v:tense.[v:pred.pn]</ta>
            <ta e="T575" id="Seg_11442" s="T574">n.[n:case]</ta>
            <ta e="T576" id="Seg_11443" s="T575">n-n:(poss).[n:case]</ta>
            <ta e="T577" id="Seg_11444" s="T576">v-v:(ins)-v:tense.[v:pred.pn]</ta>
            <ta e="T578" id="Seg_11445" s="T577">pers-pro:case</ta>
            <ta e="T579" id="Seg_11446" s="T578">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T580" id="Seg_11447" s="T579">v-v:(neg).[v:mood.pn]</ta>
            <ta e="T581" id="Seg_11448" s="T580">n.[n:case]</ta>
            <ta e="T582" id="Seg_11449" s="T581">n-n:poss-n:case</ta>
            <ta e="T583" id="Seg_11450" s="T582">adj.[n:case]</ta>
            <ta e="T584" id="Seg_11451" s="T583">adj.[n:case]</ta>
            <ta e="T585" id="Seg_11452" s="T584">n-n:case</ta>
            <ta e="T586" id="Seg_11453" s="T585">v-v:cvb</ta>
            <ta e="T587" id="Seg_11454" s="T586">n.[n:case]</ta>
            <ta e="T588" id="Seg_11455" s="T587">n-n:poss-n:case</ta>
            <ta e="T589" id="Seg_11456" s="T588">n-n:poss-n:case</ta>
            <ta e="T590" id="Seg_11457" s="T589">adv</ta>
            <ta e="T591" id="Seg_11458" s="T590">v-v:tense-v:poss.pn</ta>
            <ta e="T592" id="Seg_11459" s="T591">n.[n:case]</ta>
            <ta e="T593" id="Seg_11460" s="T592">adv</ta>
            <ta e="T594" id="Seg_11461" s="T593">v-v:cvb</ta>
            <ta e="T595" id="Seg_11462" s="T594">v-v:tense-v:poss.pn</ta>
            <ta e="T596" id="Seg_11463" s="T595">v-v:cvb</ta>
            <ta e="T597" id="Seg_11464" s="T596">v-v:ptcp</ta>
            <ta e="T598" id="Seg_11465" s="T597">n-n:(poss).[n:case]</ta>
            <ta e="T599" id="Seg_11466" s="T598">v-v:tense-v:poss.pn</ta>
            <ta e="T600" id="Seg_11467" s="T599">v-v:tense-v:poss.pn</ta>
            <ta e="T601" id="Seg_11468" s="T600">adv</ta>
            <ta e="T602" id="Seg_11469" s="T601">adj</ta>
            <ta e="T603" id="Seg_11470" s="T602">ptcl</ta>
            <ta e="T604" id="Seg_11471" s="T603">n.[n:case]</ta>
            <ta e="T605" id="Seg_11472" s="T604">v-v:tense.[v:pred.pn]</ta>
            <ta e="T606" id="Seg_11473" s="T605">n.[n:case]</ta>
            <ta e="T607" id="Seg_11474" s="T606">v-v:cvb</ta>
            <ta e="T608" id="Seg_11475" s="T607">n-n:(poss)</ta>
            <ta e="T609" id="Seg_11476" s="T608">ptcl.[ptcl:pred.pn]</ta>
            <ta e="T610" id="Seg_11477" s="T609">adv</ta>
            <ta e="T611" id="Seg_11478" s="T610">emphpro-pro:(poss).[pro:case]</ta>
            <ta e="T612" id="Seg_11479" s="T611">n-n:poss-n:case</ta>
            <ta e="T613" id="Seg_11480" s="T612">v-v:tense-v:poss.pn</ta>
            <ta e="T614" id="Seg_11481" s="T613">v-v:(ins)-v:(neg)-v:pred.pn</ta>
            <ta e="T615" id="Seg_11482" s="T614">ptcl</ta>
            <ta e="T616" id="Seg_11483" s="T615">v-v:(ins)-v&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T617" id="Seg_11484" s="T616">adj.[n:case]</ta>
            <ta e="T618" id="Seg_11485" s="T617">n-n:case</ta>
            <ta e="T619" id="Seg_11486" s="T618">v-v:tense-v:poss.pn</ta>
            <ta e="T620" id="Seg_11487" s="T619">v-v:tense.[v:pred.pn]</ta>
            <ta e="T621" id="Seg_11488" s="T620">n.[n:case]</ta>
            <ta e="T622" id="Seg_11489" s="T621">adj.[n:case]</ta>
            <ta e="T623" id="Seg_11490" s="T622">ptcl</ta>
            <ta e="T624" id="Seg_11491" s="T623">n-n:(poss).[n:case]</ta>
            <ta e="T625" id="Seg_11492" s="T624">n.[n:case]</ta>
            <ta e="T626" id="Seg_11493" s="T625">n-n:poss-n:case</ta>
            <ta e="T627" id="Seg_11494" s="T626">n.[n:case]</ta>
            <ta e="T628" id="Seg_11495" s="T627">quant</ta>
            <ta e="T629" id="Seg_11496" s="T628">n-n:case</ta>
            <ta e="T630" id="Seg_11497" s="T629">v-v:tense-v:poss.pn</ta>
            <ta e="T631" id="Seg_11498" s="T630">dempro.[pro:case]</ta>
            <ta e="T632" id="Seg_11499" s="T631">post</ta>
            <ta e="T633" id="Seg_11500" s="T632">n.[n:case]</ta>
            <ta e="T634" id="Seg_11501" s="T633">n-n:poss-n:case</ta>
            <ta e="T635" id="Seg_11502" s="T634">v-v:(neg)-v:tense-v:poss.pn</ta>
            <ta e="T636" id="Seg_11503" s="T635">v-v:cvb</ta>
            <ta e="T637" id="Seg_11504" s="T636">v-v:tense.[v:pred.pn]</ta>
            <ta e="T638" id="Seg_11505" s="T637">pers.[pro:case]</ta>
            <ta e="T639" id="Seg_11506" s="T638">n-n:poss-n:case</ta>
            <ta e="T640" id="Seg_11507" s="T639">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T641" id="Seg_11508" s="T640">ptcl</ta>
            <ta e="T642" id="Seg_11509" s="T641">adv</ta>
            <ta e="T643" id="Seg_11510" s="T642">v-v:tense-v:poss.pn</ta>
            <ta e="T644" id="Seg_11511" s="T643">ptcl</ta>
            <ta e="T645" id="Seg_11512" s="T644">v.[v:mood.pn]</ta>
            <ta e="T646" id="Seg_11513" s="T645">n.[n:case]</ta>
            <ta e="T647" id="Seg_11514" s="T646">que-pro:case-que-pro:case</ta>
            <ta e="T648" id="Seg_11515" s="T647">v-v:tense-v:poss.pn</ta>
            <ta e="T649" id="Seg_11516" s="T648">que.[pro:case]</ta>
            <ta e="T650" id="Seg_11517" s="T649">ptcl</ta>
            <ta e="T651" id="Seg_11518" s="T650">v-v:(neg).[v:pred.pn]</ta>
            <ta e="T652" id="Seg_11519" s="T651">adv</ta>
            <ta e="T653" id="Seg_11520" s="T652">n.[n:case]</ta>
            <ta e="T654" id="Seg_11521" s="T653">v-v:tense.[v:pred.pn]</ta>
            <ta e="T655" id="Seg_11522" s="T654">n-n&gt;adj.[n:case]</ta>
            <ta e="T656" id="Seg_11523" s="T655">ptcl</ta>
            <ta e="T657" id="Seg_11524" s="T656">n-n:(num)-n:(poss).[n:case]</ta>
            <ta e="T658" id="Seg_11525" s="T657">adj-adj&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T659" id="Seg_11526" s="T658">v-v:(ins)-v&gt;v-v:ptcp-v:(case)</ta>
            <ta e="T660" id="Seg_11527" s="T659">ptcl</ta>
            <ta e="T661" id="Seg_11528" s="T660">v-v:cvb</ta>
            <ta e="T662" id="Seg_11529" s="T661">v-v:tense.[v:pred.pn]</ta>
            <ta e="T663" id="Seg_11530" s="T662">n.[n:case]</ta>
            <ta e="T664" id="Seg_11531" s="T663">n-n:poss-n:case</ta>
            <ta e="T665" id="Seg_11532" s="T664">n-n:poss-n:case</ta>
            <ta e="T666" id="Seg_11533" s="T665">v-v:cvb-v-v:cvb</ta>
            <ta e="T667" id="Seg_11534" s="T666">n-n:(num)-n:poss-n:case</ta>
            <ta e="T668" id="Seg_11535" s="T667">v-v&gt;v-v:(neg)-v:tense-v:poss.pn</ta>
            <ta e="T669" id="Seg_11536" s="T668">adv</ta>
            <ta e="T670" id="Seg_11537" s="T669">v-v:cvb</ta>
            <ta e="T671" id="Seg_11538" s="T670">v-v:tense-v:poss.pn</ta>
            <ta e="T672" id="Seg_11539" s="T671">adv</ta>
            <ta e="T673" id="Seg_11540" s="T672">adj-adj&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T674" id="Seg_11541" s="T673">v-v:(ins)-v&gt;v-v:ptcp-v:(case)</ta>
            <ta e="T675" id="Seg_11542" s="T674">v-v:(ins)-v&gt;v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T676" id="Seg_11543" s="T675">post</ta>
            <ta e="T677" id="Seg_11544" s="T676">n-n&gt;adj</ta>
            <ta e="T678" id="Seg_11545" s="T677">v-v:ptcp</ta>
            <ta e="T679" id="Seg_11546" s="T678">n-n:(poss).[n:case]</ta>
            <ta e="T680" id="Seg_11547" s="T679">v-v&gt;n.[n:case]</ta>
            <ta e="T681" id="Seg_11548" s="T680">ptcl</ta>
            <ta e="T682" id="Seg_11549" s="T681">v-v:cvb</ta>
            <ta e="T683" id="Seg_11550" s="T682">v-v:tense.[v:pred.pn]</ta>
            <ta e="T684" id="Seg_11551" s="T683">que</ta>
            <ta e="T685" id="Seg_11552" s="T684">v-v:tense-v:poss.pn</ta>
            <ta e="T686" id="Seg_11553" s="T685">que</ta>
            <ta e="T687" id="Seg_11554" s="T686">v-v:tense-v:poss.pn</ta>
            <ta e="T688" id="Seg_11555" s="T687">ptcl</ta>
            <ta e="T689" id="Seg_11556" s="T688">n.[n:case]</ta>
            <ta e="T690" id="Seg_11557" s="T689">v-v:(ins)-v:tense.[v:pred.pn]</ta>
            <ta e="T691" id="Seg_11558" s="T690">pers.[pro:case]</ta>
            <ta e="T692" id="Seg_11559" s="T691">pers-pro:case</ta>
            <ta e="T693" id="Seg_11560" s="T692">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T694" id="Seg_11561" s="T693">v-v:(ins)-v&gt;v-v:(ins)-v:(neg).[v:mood.pn]</ta>
            <ta e="T695" id="Seg_11562" s="T694">n-n:(num)-n:poss-n:case</ta>
            <ta e="T696" id="Seg_11563" s="T695">n.[n:case]</ta>
            <ta e="T697" id="Seg_11564" s="T696">que-pro:case</ta>
            <ta e="T698" id="Seg_11565" s="T697">v-v:tense-v:poss.pn</ta>
            <ta e="T699" id="Seg_11566" s="T698">ptcl</ta>
            <ta e="T700" id="Seg_11567" s="T699">dempro</ta>
            <ta e="T701" id="Seg_11568" s="T700">n.[n:case]</ta>
            <ta e="T702" id="Seg_11569" s="T701">n-n:poss-n:case</ta>
            <ta e="T703" id="Seg_11570" s="T702">v-v:tense-v:poss.pn</ta>
            <ta e="T704" id="Seg_11571" s="T703">cardnum</ta>
            <ta e="T705" id="Seg_11572" s="T704">n.[n:case]</ta>
            <ta e="T706" id="Seg_11573" s="T705">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T707" id="Seg_11574" s="T706">ptcl</ta>
            <ta e="T708" id="Seg_11575" s="T707">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T709" id="Seg_11576" s="T708">n-n:poss-n:case</ta>
            <ta e="T710" id="Seg_11577" s="T709">v-v:tense.[v:pred.pn]</ta>
            <ta e="T711" id="Seg_11578" s="T710">v-v:tense.[v:pred.pn]</ta>
            <ta e="T712" id="Seg_11579" s="T711">v-v:cvb</ta>
            <ta e="T713" id="Seg_11580" s="T712">v-v:ptcp</ta>
            <ta e="T714" id="Seg_11581" s="T713">n-n:(num).[n:case]</ta>
            <ta e="T715" id="Seg_11582" s="T714">n-n:(poss).[n:case]</ta>
            <ta e="T716" id="Seg_11583" s="T715">adv</ta>
            <ta e="T717" id="Seg_11584" s="T716">n.[n:case]</ta>
            <ta e="T718" id="Seg_11585" s="T717">n-n:poss-n:case</ta>
            <ta e="T719" id="Seg_11586" s="T718">v-v:tense-v:pred.pn</ta>
            <ta e="T720" id="Seg_11587" s="T719">n-n:case</ta>
            <ta e="T721" id="Seg_11588" s="T720">v-v:cvb</ta>
            <ta e="T722" id="Seg_11589" s="T721">n-n:poss-n:case</ta>
            <ta e="T723" id="Seg_11590" s="T722">n-n:poss-n:case</ta>
            <ta e="T724" id="Seg_11591" s="T723">v-v:tense-v:poss.pn</ta>
            <ta e="T725" id="Seg_11592" s="T724">adj.[n:case]</ta>
            <ta e="T726" id="Seg_11593" s="T725">adj.[n:case]</ta>
            <ta e="T727" id="Seg_11594" s="T726">n-n:poss-n:case</ta>
            <ta e="T728" id="Seg_11595" s="T727">v-v:tense-v:poss.pn</ta>
            <ta e="T729" id="Seg_11596" s="T728">n-n:(poss).[n:case]</ta>
            <ta e="T730" id="Seg_11597" s="T729">v-v:cvb</ta>
            <ta e="T731" id="Seg_11598" s="T730">post</ta>
            <ta e="T732" id="Seg_11599" s="T731">v-v:tense-v:poss.pn</ta>
            <ta e="T733" id="Seg_11600" s="T732">adv</ta>
            <ta e="T734" id="Seg_11601" s="T733">n-n:case</ta>
            <ta e="T735" id="Seg_11602" s="T734">v-v:tense-v:poss.pn</ta>
            <ta e="T736" id="Seg_11603" s="T735">n-n:case</ta>
            <ta e="T737" id="Seg_11604" s="T736">v-v:tense-v:poss.pn</ta>
            <ta e="T738" id="Seg_11605" s="T737">v-v:cvb</ta>
            <ta e="T739" id="Seg_11606" s="T738">v-v:ptcp</ta>
            <ta e="T740" id="Seg_11607" s="T739">n-n:(num).[n:case]</ta>
            <ta e="T741" id="Seg_11608" s="T740">adv</ta>
            <ta e="T742" id="Seg_11609" s="T741">adj-n:(poss).[n:case]</ta>
            <ta e="T743" id="Seg_11610" s="T742">v-v:tense-v:pred.pn</ta>
            <ta e="T744" id="Seg_11611" s="T743">adj</ta>
            <ta e="T745" id="Seg_11612" s="T744">n.[n:case]</ta>
            <ta e="T746" id="Seg_11613" s="T745">v-v:cvb</ta>
            <ta e="T747" id="Seg_11614" s="T746">v-v:cvb</ta>
            <ta e="T748" id="Seg_11615" s="T747">v-v:tense-v:pred.pn</ta>
            <ta e="T749" id="Seg_11616" s="T748">adj</ta>
            <ta e="T750" id="Seg_11617" s="T749">n-n:(ins)-n:case</ta>
            <ta e="T751" id="Seg_11618" s="T750">v-v:tense-v:poss.pn</ta>
            <ta e="T752" id="Seg_11619" s="T751">n-n:(poss).[n:case]</ta>
            <ta e="T753" id="Seg_11620" s="T752">v-v:tense.[v:pred.pn]</ta>
            <ta e="T754" id="Seg_11621" s="T753">n.[n:case]</ta>
            <ta e="T755" id="Seg_11622" s="T754">n-n:poss-n:case</ta>
            <ta e="T756" id="Seg_11623" s="T755">v-v:cvb</ta>
            <ta e="T757" id="Seg_11624" s="T756">adv</ta>
            <ta e="T758" id="Seg_11625" s="T757">v-v:tense-v:poss.pn</ta>
            <ta e="T759" id="Seg_11626" s="T758">adv</ta>
            <ta e="T760" id="Seg_11627" s="T759">pers.[pro:case]</ta>
            <ta e="T761" id="Seg_11628" s="T760">v-v:tense-v:poss.pn</ta>
            <ta e="T762" id="Seg_11629" s="T761">n-n:(poss).[n:case]</ta>
            <ta e="T763" id="Seg_11630" s="T762">adv</ta>
            <ta e="T764" id="Seg_11631" s="T763">ptcl-ptcl&gt;adv</ta>
            <ta e="T765" id="Seg_11632" s="T764">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T766" id="Seg_11633" s="T765">adv</ta>
            <ta e="T767" id="Seg_11634" s="T766">pers.[pro:case]</ta>
            <ta e="T768" id="Seg_11635" s="T767">n.[n:case]</ta>
            <ta e="T769" id="Seg_11636" s="T768">v-v:tense.[v:pred.pn]</ta>
            <ta e="T770" id="Seg_11637" s="T769">v-v:tense.[v:pred.pn]</ta>
            <ta e="T771" id="Seg_11638" s="T770">n-n:case</ta>
            <ta e="T772" id="Seg_11639" s="T771">post</ta>
            <ta e="T773" id="Seg_11640" s="T772">v-v:tense-v:pred.pn</ta>
            <ta e="T774" id="Seg_11641" s="T773">v-v:tense.[v:pred.pn]</ta>
            <ta e="T775" id="Seg_11642" s="T774">n.[n:case]</ta>
            <ta e="T776" id="Seg_11643" s="T775">n-n:poss-n:case</ta>
            <ta e="T777" id="Seg_11644" s="T776">n-n:poss-n:case</ta>
            <ta e="T778" id="Seg_11645" s="T777">v-v:tense-v:poss.pn</ta>
            <ta e="T779" id="Seg_11646" s="T778">n-n:poss-n:case</ta>
            <ta e="T780" id="Seg_11647" s="T779">v-v:cvb</ta>
            <ta e="T781" id="Seg_11648" s="T780">v-v:tense-v:poss.pn</ta>
            <ta e="T782" id="Seg_11649" s="T781">que</ta>
            <ta e="T783" id="Seg_11650" s="T782">ptcl</ta>
            <ta e="T784" id="Seg_11651" s="T783">v-v:cvb</ta>
            <ta e="T785" id="Seg_11652" s="T784">dempro</ta>
            <ta e="T786" id="Seg_11653" s="T785">n.[n:case]</ta>
            <ta e="T787" id="Seg_11654" s="T786">v-v:tense-v:poss.pn</ta>
            <ta e="T788" id="Seg_11655" s="T787">que</ta>
            <ta e="T789" id="Seg_11656" s="T788">v-v:tense-v:pred.pn</ta>
            <ta e="T790" id="Seg_11657" s="T789">v-v:tense.[v:pred.pn]</ta>
            <ta e="T791" id="Seg_11658" s="T790">n-n:(poss).[n:case]</ta>
            <ta e="T792" id="Seg_11659" s="T791">adv</ta>
            <ta e="T793" id="Seg_11660" s="T792">v-v:tense-v:poss.pn</ta>
            <ta e="T794" id="Seg_11661" s="T793">v-v:(ins)-v:(neg).[v:mood.pn]</ta>
            <ta e="T795" id="Seg_11662" s="T794">n.[n:case]</ta>
            <ta e="T796" id="Seg_11663" s="T795">dempro</ta>
            <ta e="T797" id="Seg_11664" s="T796">n-n:case</ta>
            <ta e="T798" id="Seg_11665" s="T797">v-v:cvb</ta>
            <ta e="T799" id="Seg_11666" s="T798">v-v:mood.[v:pred.pn]</ta>
            <ta e="T800" id="Seg_11667" s="T799">n.[n:case]</ta>
            <ta e="T801" id="Seg_11668" s="T800">n-n:poss-n:case</ta>
            <ta e="T802" id="Seg_11669" s="T801">n-n&gt;adj-n:case</ta>
            <ta e="T803" id="Seg_11670" s="T802">post</ta>
            <ta e="T804" id="Seg_11671" s="T803">v-v:cvb</ta>
            <ta e="T805" id="Seg_11672" s="T804">adj.[n:case]</ta>
            <ta e="T806" id="Seg_11673" s="T805">ptcl</ta>
            <ta e="T807" id="Seg_11674" s="T806">n.[n:case]</ta>
            <ta e="T808" id="Seg_11675" s="T807">v-v:tense.[v:pred.pn]</ta>
            <ta e="T809" id="Seg_11676" s="T808">n-n:poss-n:case</ta>
            <ta e="T810" id="Seg_11677" s="T809">ptcl</ta>
            <ta e="T811" id="Seg_11678" s="T810">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T812" id="Seg_11679" s="T811">v-v:(neg).[v:pred.pn]</ta>
            <ta e="T813" id="Seg_11680" s="T812">n-n:poss-n:case</ta>
            <ta e="T814" id="Seg_11681" s="T813">ptcl</ta>
            <ta e="T815" id="Seg_11682" s="T814">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T816" id="Seg_11683" s="T815">v-v:(neg).[v:pred.pn]</ta>
            <ta e="T817" id="Seg_11684" s="T816">v-v&gt;v-v:cvb</ta>
            <ta e="T818" id="Seg_11685" s="T817">v-v:tense-v:poss.pn</ta>
            <ta e="T819" id="Seg_11686" s="T818">n.[n:case]</ta>
            <ta e="T820" id="Seg_11687" s="T819">n-n:(poss).[n:case]</ta>
            <ta e="T821" id="Seg_11688" s="T820">v-v:tense.[v:pred.pn]</ta>
            <ta e="T822" id="Seg_11689" s="T821">n-n&gt;adj.[n:case]</ta>
            <ta e="T823" id="Seg_11690" s="T822">n.[n:case]</ta>
            <ta e="T824" id="Seg_11691" s="T823">v-v:cvb</ta>
            <ta e="T825" id="Seg_11692" s="T824">n.[n:case]</ta>
            <ta e="T826" id="Seg_11693" s="T825">n-n:case</ta>
            <ta e="T827" id="Seg_11694" s="T826">v-v:tense.[v:pred.pn]</ta>
            <ta e="T828" id="Seg_11695" s="T827">v-v:cvb</ta>
            <ta e="T829" id="Seg_11696" s="T828">n.[n:case]</ta>
            <ta e="T830" id="Seg_11697" s="T829">v-v:tense.[v:pred.pn]</ta>
            <ta e="T831" id="Seg_11698" s="T830">n.[n:case]</ta>
            <ta e="T832" id="Seg_11699" s="T831">n-n:poss-n:case</ta>
            <ta e="T833" id="Seg_11700" s="T832">v-v:cvb</ta>
            <ta e="T834" id="Seg_11701" s="T833">v-v:cvb</ta>
            <ta e="T835" id="Seg_11702" s="T834">n-n:case</ta>
            <ta e="T836" id="Seg_11703" s="T835">v-v:tense.[v:pred.pn]</ta>
            <ta e="T837" id="Seg_11704" s="T836">adj.[n:case]</ta>
            <ta e="T838" id="Seg_11705" s="T837">n-n:case</ta>
            <ta e="T839" id="Seg_11706" s="T838">n-n:(num).[n:case]</ta>
            <ta e="T840" id="Seg_11707" s="T839">ptcl</ta>
            <ta e="T841" id="Seg_11708" s="T840">que</ta>
            <ta e="T842" id="Seg_11709" s="T841">ptcl</ta>
            <ta e="T843" id="Seg_11710" s="T842">v-v:cvb</ta>
            <ta e="T844" id="Seg_11711" s="T843">n.[n:case]</ta>
            <ta e="T845" id="Seg_11712" s="T844">v-v:tense-v:poss.pn</ta>
            <ta e="T846" id="Seg_11713" s="T845">dempro</ta>
            <ta e="T847" id="Seg_11714" s="T846">n-n:(poss).[n:case]</ta>
            <ta e="T848" id="Seg_11715" s="T847">v-v&gt;v-v:ptcp-v:(poss).[v:pred.pn]</ta>
            <ta e="T849" id="Seg_11716" s="T848">n.[n:case]</ta>
            <ta e="T850" id="Seg_11717" s="T849">n-n:case</ta>
            <ta e="T851" id="Seg_11718" s="T850">n.[n:case]</ta>
            <ta e="T852" id="Seg_11719" s="T851">v-v:cvb</ta>
            <ta e="T853" id="Seg_11720" s="T852">post</ta>
            <ta e="T854" id="Seg_11721" s="T853">v-v:tense-v:poss.pn</ta>
            <ta e="T855" id="Seg_11722" s="T854">n-n:poss-n:case</ta>
            <ta e="T856" id="Seg_11723" s="T855">v-v:mood.[v:pred.pn]</ta>
            <ta e="T857" id="Seg_11724" s="T856">n.[n:case]</ta>
            <ta e="T858" id="Seg_11725" s="T857">v-v:(ins)-v:(neg).[v:pred.pn]</ta>
            <ta e="T859" id="Seg_11726" s="T858">n.[n:case]</ta>
            <ta e="T860" id="Seg_11727" s="T859">ptcl</ta>
            <ta e="T861" id="Seg_11728" s="T860">post</ta>
            <ta e="T862" id="Seg_11729" s="T861">v-v:tense.[v:pred.pn]</ta>
            <ta e="T863" id="Seg_11730" s="T862">n.[n:case]</ta>
            <ta e="T864" id="Seg_11731" s="T863">dempro-pro:case</ta>
            <ta e="T865" id="Seg_11732" s="T864">v-v:cvb</ta>
            <ta e="T866" id="Seg_11733" s="T865">v-v:tense.[v:pred.pn]</ta>
            <ta e="T867" id="Seg_11734" s="T866">n-n:(poss).[n:case]</ta>
            <ta e="T868" id="Seg_11735" s="T867">v-v:cvb</ta>
            <ta e="T869" id="Seg_11736" s="T868">v-v:cvb</ta>
            <ta e="T870" id="Seg_11737" s="T869">n-n:case</ta>
            <ta e="T871" id="Seg_11738" s="T870">v-v:tense-v:poss.pn</ta>
            <ta e="T872" id="Seg_11739" s="T871">v-v:cvb</ta>
            <ta e="T873" id="Seg_11740" s="T872">n-n.[n:case]</ta>
            <ta e="T874" id="Seg_11741" s="T873">adj.[n:case]</ta>
            <ta e="T875" id="Seg_11742" s="T874">n.[n:case]</ta>
            <ta e="T876" id="Seg_11743" s="T875">v-v:tense.[v:pred.pn]</ta>
            <ta e="T877" id="Seg_11744" s="T876">n-n:(num).[n:case]</ta>
            <ta e="T878" id="Seg_11745" s="T877">n.[n:case]</ta>
            <ta e="T879" id="Seg_11746" s="T878">v-v:tense-v:pred.pn</ta>
            <ta e="T880" id="Seg_11747" s="T879">n-n&gt;adj.[n:case]</ta>
            <ta e="T881" id="Seg_11748" s="T880">n.[n:case]</ta>
            <ta e="T882" id="Seg_11749" s="T881">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T883" id="Seg_11750" s="T882">n.[n:case]</ta>
            <ta e="T884" id="Seg_11751" s="T883">v-v:tense-v:poss.pn</ta>
            <ta e="T885" id="Seg_11752" s="T884">n-n:poss-n:case</ta>
            <ta e="T886" id="Seg_11753" s="T885">v-v:tense-v:poss.pn</ta>
            <ta e="T887" id="Seg_11754" s="T886">que</ta>
            <ta e="T888" id="Seg_11755" s="T887">v-v:tense-v:pred.pn</ta>
            <ta e="T889" id="Seg_11756" s="T888">v.[v:mood.pn]</ta>
            <ta e="T890" id="Seg_11757" s="T889">pers-pro:case</ta>
            <ta e="T891" id="Seg_11758" s="T890">post</ta>
            <ta e="T892" id="Seg_11759" s="T891">v-v:tense-v:poss.pn</ta>
            <ta e="T893" id="Seg_11760" s="T892">n.[n:case]</ta>
            <ta e="T894" id="Seg_11761" s="T893">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T895" id="Seg_11762" s="T894">pers-pro:case</ta>
            <ta e="T896" id="Seg_11763" s="T895">v-v:tense.[v:pred.pn]</ta>
            <ta e="T897" id="Seg_11764" s="T896">v-v&gt;v-v:(ins)-v&gt;v-v:(ins)-v:mood.pn</ta>
            <ta e="T898" id="Seg_11765" s="T897">adv</ta>
            <ta e="T899" id="Seg_11766" s="T898">v-v:tense-v:poss.pn</ta>
            <ta e="T900" id="Seg_11767" s="T899">n-n:poss-n:case</ta>
            <ta e="T901" id="Seg_11768" s="T900">v-v:tense-v:poss.pn</ta>
            <ta e="T902" id="Seg_11769" s="T901">n-n:case</ta>
            <ta e="T903" id="Seg_11770" s="T902">post</ta>
            <ta e="T904" id="Seg_11771" s="T903">n.[n:case]</ta>
            <ta e="T905" id="Seg_11772" s="T904">n-n:(num)-n:case</ta>
            <ta e="T906" id="Seg_11773" s="T905">n.[n:case]</ta>
            <ta e="T907" id="Seg_11774" s="T906">n-n:poss-n:case</ta>
            <ta e="T908" id="Seg_11775" s="T907">v-v:tense-v:pred.pn</ta>
            <ta e="T909" id="Seg_11776" s="T908">dempro-pro:(poss)-pro:(poss).[pro:case]</ta>
            <ta e="T910" id="Seg_11777" s="T909">adj.[n:case]</ta>
            <ta e="T911" id="Seg_11778" s="T910">n.[n:case]</ta>
            <ta e="T912" id="Seg_11779" s="T911">v-v:tense-v:pred.pn</ta>
            <ta e="T913" id="Seg_11780" s="T912">n.[n:case]</ta>
            <ta e="T914" id="Seg_11781" s="T913">n-n:(poss).[n:case]</ta>
            <ta e="T915" id="Seg_11782" s="T914">v-v:cvb</ta>
            <ta e="T916" id="Seg_11783" s="T915">v-v:tense-v:poss.pn</ta>
            <ta e="T917" id="Seg_11784" s="T916">n.[n:case]</ta>
            <ta e="T918" id="Seg_11785" s="T917">cardnum</ta>
            <ta e="T919" id="Seg_11786" s="T918">n-n:case</ta>
            <ta e="T920" id="Seg_11787" s="T919">v-v:tense-v:poss.pn</ta>
            <ta e="T921" id="Seg_11788" s="T920">v-v:cvb</ta>
            <ta e="T922" id="Seg_11789" s="T921">v-v:tense-v:poss.pn</ta>
            <ta e="T923" id="Seg_11790" s="T922">n-n:(poss).[n:case]</ta>
            <ta e="T924" id="Seg_11791" s="T923">n-n:case</ta>
            <ta e="T925" id="Seg_11792" s="T924">post</ta>
            <ta e="T926" id="Seg_11793" s="T925">v-v:cvb-v-v:cvb</ta>
            <ta e="T927" id="Seg_11794" s="T926">v-v&gt;v-v:cvb</ta>
            <ta e="T928" id="Seg_11795" s="T927">v-v:tense-v:pred.pn</ta>
            <ta e="T929" id="Seg_11796" s="T928">n.[n:case]</ta>
            <ta e="T930" id="Seg_11797" s="T929">v-v:tense-v:pred.pn</ta>
            <ta e="T931" id="Seg_11798" s="T930">v-v:ptcp</ta>
            <ta e="T932" id="Seg_11799" s="T931">n-n:case</ta>
            <ta e="T933" id="Seg_11800" s="T932">v-v:tense-v:pred.pn</ta>
            <ta e="T934" id="Seg_11801" s="T933">n.[n:case]</ta>
            <ta e="T935" id="Seg_11802" s="T934">n-n:poss-n:case</ta>
            <ta e="T936" id="Seg_11803" s="T935">v-v:tense-v:poss.pn</ta>
            <ta e="T937" id="Seg_11804" s="T936">adj</ta>
            <ta e="T938" id="Seg_11805" s="T937">n-n:case</ta>
            <ta e="T939" id="Seg_11806" s="T938">v-v:tense-v:poss.pn</ta>
            <ta e="T940" id="Seg_11807" s="T939">n.[n:case]</ta>
            <ta e="T941" id="Seg_11808" s="T940">n-n:case</ta>
            <ta e="T942" id="Seg_11809" s="T941">n.[n:case]</ta>
            <ta e="T943" id="Seg_11810" s="T942">v-v:tense.[v:pred.pn]</ta>
            <ta e="T944" id="Seg_11811" s="T943">adj.[n:case]</ta>
            <ta e="T945" id="Seg_11812" s="T944">ptcl-ptcl&gt;adv</ta>
            <ta e="T946" id="Seg_11813" s="T945">v-v:tense-v:pred.pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_11814" s="T1">v</ta>
            <ta e="T3" id="Seg_11815" s="T2">n</ta>
            <ta e="T4" id="Seg_11816" s="T3">n</ta>
            <ta e="T5" id="Seg_11817" s="T4">adj</ta>
            <ta e="T6" id="Seg_11818" s="T5">n</ta>
            <ta e="T7" id="Seg_11819" s="T6">v</ta>
            <ta e="T8" id="Seg_11820" s="T7">adj</ta>
            <ta e="T9" id="Seg_11821" s="T8">n</ta>
            <ta e="T10" id="Seg_11822" s="T9">n</ta>
            <ta e="T11" id="Seg_11823" s="T10">post</ta>
            <ta e="T12" id="Seg_11824" s="T11">adv</ta>
            <ta e="T13" id="Seg_11825" s="T12">v</ta>
            <ta e="T14" id="Seg_11826" s="T13">n</ta>
            <ta e="T15" id="Seg_11827" s="T14">adj</ta>
            <ta e="T16" id="Seg_11828" s="T15">n</ta>
            <ta e="T17" id="Seg_11829" s="T16">ptcl</ta>
            <ta e="T18" id="Seg_11830" s="T17">n</ta>
            <ta e="T19" id="Seg_11831" s="T18">adj</ta>
            <ta e="T20" id="Seg_11832" s="T19">n</ta>
            <ta e="T21" id="Seg_11833" s="T20">n</ta>
            <ta e="T22" id="Seg_11834" s="T21">adj</ta>
            <ta e="T23" id="Seg_11835" s="T22">n</ta>
            <ta e="T24" id="Seg_11836" s="T23">n</ta>
            <ta e="T25" id="Seg_11837" s="T24">emphpro</ta>
            <ta e="T26" id="Seg_11838" s="T25">v</ta>
            <ta e="T27" id="Seg_11839" s="T26">n</ta>
            <ta e="T28" id="Seg_11840" s="T27">ptcl</ta>
            <ta e="T29" id="Seg_11841" s="T28">adv</ta>
            <ta e="T30" id="Seg_11842" s="T29">v</ta>
            <ta e="T31" id="Seg_11843" s="T30">n</ta>
            <ta e="T32" id="Seg_11844" s="T31">n</ta>
            <ta e="T33" id="Seg_11845" s="T32">n</ta>
            <ta e="T34" id="Seg_11846" s="T33">v</ta>
            <ta e="T35" id="Seg_11847" s="T34">n</ta>
            <ta e="T36" id="Seg_11848" s="T35">v</ta>
            <ta e="T37" id="Seg_11849" s="T36">post</ta>
            <ta e="T38" id="Seg_11850" s="T37">n</ta>
            <ta e="T39" id="Seg_11851" s="T38">v</ta>
            <ta e="T40" id="Seg_11852" s="T39">n</ta>
            <ta e="T41" id="Seg_11853" s="T40">v</ta>
            <ta e="T42" id="Seg_11854" s="T41">n</ta>
            <ta e="T43" id="Seg_11855" s="T42">n</ta>
            <ta e="T44" id="Seg_11856" s="T43">v</ta>
            <ta e="T45" id="Seg_11857" s="T44">n</ta>
            <ta e="T46" id="Seg_11858" s="T45">v</ta>
            <ta e="T47" id="Seg_11859" s="T46">n</ta>
            <ta e="T48" id="Seg_11860" s="T47">v</ta>
            <ta e="T49" id="Seg_11861" s="T48">v</ta>
            <ta e="T50" id="Seg_11862" s="T49">n</ta>
            <ta e="T51" id="Seg_11863" s="T50">v</ta>
            <ta e="T52" id="Seg_11864" s="T51">v</ta>
            <ta e="T53" id="Seg_11865" s="T52">adv</ta>
            <ta e="T54" id="Seg_11866" s="T53">v</ta>
            <ta e="T55" id="Seg_11867" s="T54">n</ta>
            <ta e="T56" id="Seg_11868" s="T55">n</ta>
            <ta e="T57" id="Seg_11869" s="T56">v</ta>
            <ta e="T58" id="Seg_11870" s="T57">post</ta>
            <ta e="T59" id="Seg_11871" s="T58">n</ta>
            <ta e="T60" id="Seg_11872" s="T59">n</ta>
            <ta e="T61" id="Seg_11873" s="T60">n</ta>
            <ta e="T62" id="Seg_11874" s="T61">ptcl</ta>
            <ta e="T63" id="Seg_11875" s="T62">v</ta>
            <ta e="T64" id="Seg_11876" s="T63">v</ta>
            <ta e="T65" id="Seg_11877" s="T64">v</ta>
            <ta e="T66" id="Seg_11878" s="T65">v</ta>
            <ta e="T67" id="Seg_11879" s="T66">n</ta>
            <ta e="T68" id="Seg_11880" s="T67">v</ta>
            <ta e="T69" id="Seg_11881" s="T68">ptcl</ta>
            <ta e="T70" id="Seg_11882" s="T69">v</ta>
            <ta e="T71" id="Seg_11883" s="T70">v</ta>
            <ta e="T72" id="Seg_11884" s="T71">v</ta>
            <ta e="T73" id="Seg_11885" s="T72">v</ta>
            <ta e="T74" id="Seg_11886" s="T73">cop</ta>
            <ta e="T75" id="Seg_11887" s="T74">n</ta>
            <ta e="T76" id="Seg_11888" s="T75">adv</ta>
            <ta e="T77" id="Seg_11889" s="T76">v</ta>
            <ta e="T78" id="Seg_11890" s="T77">v</ta>
            <ta e="T79" id="Seg_11891" s="T78">v</ta>
            <ta e="T80" id="Seg_11892" s="T79">v</ta>
            <ta e="T81" id="Seg_11893" s="T80">aux</ta>
            <ta e="T82" id="Seg_11894" s="T81">v</ta>
            <ta e="T83" id="Seg_11895" s="T82">aux</ta>
            <ta e="T84" id="Seg_11896" s="T83">n</ta>
            <ta e="T85" id="Seg_11897" s="T84">n</ta>
            <ta e="T86" id="Seg_11898" s="T85">adv</ta>
            <ta e="T87" id="Seg_11899" s="T86">cardnum</ta>
            <ta e="T88" id="Seg_11900" s="T87">n</ta>
            <ta e="T89" id="Seg_11901" s="T88">v</ta>
            <ta e="T90" id="Seg_11902" s="T89">v</ta>
            <ta e="T91" id="Seg_11903" s="T90">v</ta>
            <ta e="T92" id="Seg_11904" s="T91">n</ta>
            <ta e="T93" id="Seg_11905" s="T92">v</ta>
            <ta e="T94" id="Seg_11906" s="T93">aux</ta>
            <ta e="T95" id="Seg_11907" s="T94">n</ta>
            <ta e="T96" id="Seg_11908" s="T95">adj</ta>
            <ta e="T97" id="Seg_11909" s="T96">v</ta>
            <ta e="T98" id="Seg_11910" s="T97">v</ta>
            <ta e="T99" id="Seg_11911" s="T98">n</ta>
            <ta e="T100" id="Seg_11912" s="T99">v</ta>
            <ta e="T101" id="Seg_11913" s="T100">n</ta>
            <ta e="T102" id="Seg_11914" s="T101">v</ta>
            <ta e="T103" id="Seg_11915" s="T102">ptcl</ta>
            <ta e="T104" id="Seg_11916" s="T103">n</ta>
            <ta e="T105" id="Seg_11917" s="T104">v</ta>
            <ta e="T106" id="Seg_11918" s="T105">ptcl</ta>
            <ta e="T107" id="Seg_11919" s="T106">v</ta>
            <ta e="T108" id="Seg_11920" s="T107">v</ta>
            <ta e="T109" id="Seg_11921" s="T108">n</ta>
            <ta e="T110" id="Seg_11922" s="T109">v</ta>
            <ta e="T111" id="Seg_11923" s="T110">v</ta>
            <ta e="T112" id="Seg_11924" s="T111">adj</ta>
            <ta e="T113" id="Seg_11925" s="T112">n</ta>
            <ta e="T114" id="Seg_11926" s="T113">n</ta>
            <ta e="T115" id="Seg_11927" s="T114">n</ta>
            <ta e="T116" id="Seg_11928" s="T115">post</ta>
            <ta e="T117" id="Seg_11929" s="T116">v</ta>
            <ta e="T118" id="Seg_11930" s="T117">v</ta>
            <ta e="T119" id="Seg_11931" s="T118">n</ta>
            <ta e="T120" id="Seg_11932" s="T119">adj</ta>
            <ta e="T121" id="Seg_11933" s="T120">n</ta>
            <ta e="T122" id="Seg_11934" s="T121">v</ta>
            <ta e="T123" id="Seg_11935" s="T122">aux</ta>
            <ta e="T124" id="Seg_11936" s="T123">n</ta>
            <ta e="T125" id="Seg_11937" s="T124">n</ta>
            <ta e="T126" id="Seg_11938" s="T125">n</ta>
            <ta e="T127" id="Seg_11939" s="T126">v</ta>
            <ta e="T128" id="Seg_11940" s="T127">v</ta>
            <ta e="T129" id="Seg_11941" s="T128">n</ta>
            <ta e="T130" id="Seg_11942" s="T129">v</ta>
            <ta e="T131" id="Seg_11943" s="T130">dempro</ta>
            <ta e="T132" id="Seg_11944" s="T131">n</ta>
            <ta e="T133" id="Seg_11945" s="T132">v</ta>
            <ta e="T134" id="Seg_11946" s="T133">v</ta>
            <ta e="T135" id="Seg_11947" s="T134">adv</ta>
            <ta e="T136" id="Seg_11948" s="T135">v</ta>
            <ta e="T137" id="Seg_11949" s="T136">aux</ta>
            <ta e="T138" id="Seg_11950" s="T137">que</ta>
            <ta e="T139" id="Seg_11951" s="T138">v</ta>
            <ta e="T140" id="Seg_11952" s="T139">ptcl</ta>
            <ta e="T141" id="Seg_11953" s="T140">v</ta>
            <ta e="T142" id="Seg_11954" s="T141">n</ta>
            <ta e="T143" id="Seg_11955" s="T142">n</ta>
            <ta e="T144" id="Seg_11956" s="T143">n</ta>
            <ta e="T145" id="Seg_11957" s="T144">v</ta>
            <ta e="T146" id="Seg_11958" s="T145">n</ta>
            <ta e="T147" id="Seg_11959" s="T146">n</ta>
            <ta e="T148" id="Seg_11960" s="T147">v</ta>
            <ta e="T149" id="Seg_11961" s="T148">v</ta>
            <ta e="T150" id="Seg_11962" s="T149">v</ta>
            <ta e="T151" id="Seg_11963" s="T150">n</ta>
            <ta e="T152" id="Seg_11964" s="T151">n</ta>
            <ta e="T153" id="Seg_11965" s="T152">n</ta>
            <ta e="T154" id="Seg_11966" s="T153">cardnum</ta>
            <ta e="T155" id="Seg_11967" s="T154">n</ta>
            <ta e="T156" id="Seg_11968" s="T155">ptcl</ta>
            <ta e="T157" id="Seg_11969" s="T156">v</ta>
            <ta e="T158" id="Seg_11970" s="T157">n</ta>
            <ta e="T159" id="Seg_11971" s="T158">v</ta>
            <ta e="T160" id="Seg_11972" s="T159">aux</ta>
            <ta e="T161" id="Seg_11973" s="T160">n</ta>
            <ta e="T162" id="Seg_11974" s="T161">adv</ta>
            <ta e="T163" id="Seg_11975" s="T162">v</ta>
            <ta e="T164" id="Seg_11976" s="T163">n</ta>
            <ta e="T165" id="Seg_11977" s="T164">v</ta>
            <ta e="T166" id="Seg_11978" s="T165">v</ta>
            <ta e="T167" id="Seg_11979" s="T166">ptcl</ta>
            <ta e="T168" id="Seg_11980" s="T167">n</ta>
            <ta e="T169" id="Seg_11981" s="T168">n</ta>
            <ta e="T170" id="Seg_11982" s="T169">v</ta>
            <ta e="T171" id="Seg_11983" s="T170">v</ta>
            <ta e="T172" id="Seg_11984" s="T171">n</ta>
            <ta e="T173" id="Seg_11985" s="T172">v</ta>
            <ta e="T174" id="Seg_11986" s="T173">n</ta>
            <ta e="T175" id="Seg_11987" s="T174">adj</ta>
            <ta e="T176" id="Seg_11988" s="T175">cop</ta>
            <ta e="T177" id="Seg_11989" s="T176">n</ta>
            <ta e="T178" id="Seg_11990" s="T177">n</ta>
            <ta e="T179" id="Seg_11991" s="T178">v</ta>
            <ta e="T180" id="Seg_11992" s="T179">v</ta>
            <ta e="T181" id="Seg_11993" s="T180">aux</ta>
            <ta e="T182" id="Seg_11994" s="T181">n</ta>
            <ta e="T183" id="Seg_11995" s="T182">n</ta>
            <ta e="T184" id="Seg_11996" s="T183">adj</ta>
            <ta e="T185" id="Seg_11997" s="T184">post</ta>
            <ta e="T186" id="Seg_11998" s="T185">n</ta>
            <ta e="T187" id="Seg_11999" s="T186">v</ta>
            <ta e="T188" id="Seg_12000" s="T187">n</ta>
            <ta e="T189" id="Seg_12001" s="T188">n</ta>
            <ta e="T190" id="Seg_12002" s="T189">n</ta>
            <ta e="T191" id="Seg_12003" s="T190">n</ta>
            <ta e="T192" id="Seg_12004" s="T191">n</ta>
            <ta e="T193" id="Seg_12005" s="T192">n</ta>
            <ta e="T194" id="Seg_12006" s="T193">v</ta>
            <ta e="T195" id="Seg_12007" s="T194">n</ta>
            <ta e="T196" id="Seg_12008" s="T195">cardnum</ta>
            <ta e="T197" id="Seg_12009" s="T196">n</ta>
            <ta e="T198" id="Seg_12010" s="T197">v</ta>
            <ta e="T199" id="Seg_12011" s="T198">aux</ta>
            <ta e="T200" id="Seg_12012" s="T199">v</ta>
            <ta e="T201" id="Seg_12013" s="T200">v</ta>
            <ta e="T202" id="Seg_12014" s="T201">n</ta>
            <ta e="T203" id="Seg_12015" s="T202">v</ta>
            <ta e="T204" id="Seg_12016" s="T203">adv</ta>
            <ta e="T205" id="Seg_12017" s="T204">v</ta>
            <ta e="T206" id="Seg_12018" s="T205">n</ta>
            <ta e="T207" id="Seg_12019" s="T206">n</ta>
            <ta e="T208" id="Seg_12020" s="T207">v</ta>
            <ta e="T209" id="Seg_12021" s="T208">n</ta>
            <ta e="T210" id="Seg_12022" s="T209">n</ta>
            <ta e="T211" id="Seg_12023" s="T210">v</ta>
            <ta e="T212" id="Seg_12024" s="T211">v</ta>
            <ta e="T213" id="Seg_12025" s="T212">n</ta>
            <ta e="T214" id="Seg_12026" s="T213">cop</ta>
            <ta e="T215" id="Seg_12027" s="T214">aux</ta>
            <ta e="T216" id="Seg_12028" s="T215">que</ta>
            <ta e="T217" id="Seg_12029" s="T216">ptcl</ta>
            <ta e="T218" id="Seg_12030" s="T217">cop</ta>
            <ta e="T219" id="Seg_12031" s="T218">n</ta>
            <ta e="T220" id="Seg_12032" s="T219">adv</ta>
            <ta e="T221" id="Seg_12033" s="T220">v</ta>
            <ta e="T222" id="Seg_12034" s="T221">v</ta>
            <ta e="T223" id="Seg_12035" s="T222">v</ta>
            <ta e="T224" id="Seg_12036" s="T223">aux</ta>
            <ta e="T225" id="Seg_12037" s="T224">v</ta>
            <ta e="T226" id="Seg_12038" s="T225">que</ta>
            <ta e="T227" id="Seg_12039" s="T226">v</ta>
            <ta e="T228" id="Seg_12040" s="T227">aux</ta>
            <ta e="T229" id="Seg_12041" s="T228">v</ta>
            <ta e="T230" id="Seg_12042" s="T229">n</ta>
            <ta e="T231" id="Seg_12043" s="T230">n</ta>
            <ta e="T232" id="Seg_12044" s="T231">n</ta>
            <ta e="T233" id="Seg_12045" s="T232">v</ta>
            <ta e="T234" id="Seg_12046" s="T233">adv</ta>
            <ta e="T235" id="Seg_12047" s="T234">adj</ta>
            <ta e="T236" id="Seg_12048" s="T235">ptcl</ta>
            <ta e="T237" id="Seg_12049" s="T236">que</ta>
            <ta e="T238" id="Seg_12050" s="T237">n</ta>
            <ta e="T239" id="Seg_12051" s="T238">n</ta>
            <ta e="T240" id="Seg_12052" s="T239">n</ta>
            <ta e="T241" id="Seg_12053" s="T240">post</ta>
            <ta e="T242" id="Seg_12054" s="T241">v</ta>
            <ta e="T243" id="Seg_12055" s="T242">v</ta>
            <ta e="T244" id="Seg_12056" s="T243">n</ta>
            <ta e="T245" id="Seg_12057" s="T244">v</ta>
            <ta e="T246" id="Seg_12058" s="T245">v</ta>
            <ta e="T247" id="Seg_12059" s="T246">n</ta>
            <ta e="T248" id="Seg_12060" s="T247">cop</ta>
            <ta e="T249" id="Seg_12061" s="T248">n</ta>
            <ta e="T250" id="Seg_12062" s="T249">n</ta>
            <ta e="T251" id="Seg_12063" s="T250">v</ta>
            <ta e="T252" id="Seg_12064" s="T251">adj</ta>
            <ta e="T253" id="Seg_12065" s="T252">n</ta>
            <ta e="T254" id="Seg_12066" s="T253">adv</ta>
            <ta e="T255" id="Seg_12067" s="T254">v</ta>
            <ta e="T256" id="Seg_12068" s="T255">v</ta>
            <ta e="T257" id="Seg_12069" s="T256">interj</ta>
            <ta e="T258" id="Seg_12070" s="T257">n</ta>
            <ta e="T259" id="Seg_12071" s="T258">v</ta>
            <ta e="T260" id="Seg_12072" s="T259">v</ta>
            <ta e="T261" id="Seg_12073" s="T260">adv</ta>
            <ta e="T262" id="Seg_12074" s="T261">adv</ta>
            <ta e="T263" id="Seg_12075" s="T262">n</ta>
            <ta e="T264" id="Seg_12076" s="T263">n</ta>
            <ta e="T265" id="Seg_12077" s="T264">n</ta>
            <ta e="T266" id="Seg_12078" s="T265">v</ta>
            <ta e="T267" id="Seg_12079" s="T266">n</ta>
            <ta e="T268" id="Seg_12080" s="T267">v</ta>
            <ta e="T269" id="Seg_12081" s="T268">n</ta>
            <ta e="T270" id="Seg_12082" s="T269">v</ta>
            <ta e="T271" id="Seg_12083" s="T270">v</ta>
            <ta e="T272" id="Seg_12084" s="T271">n</ta>
            <ta e="T273" id="Seg_12085" s="T272">ptcl</ta>
            <ta e="T274" id="Seg_12086" s="T273">v</ta>
            <ta e="T275" id="Seg_12087" s="T274">v</ta>
            <ta e="T276" id="Seg_12088" s="T275">n</ta>
            <ta e="T277" id="Seg_12089" s="T276">n</ta>
            <ta e="T278" id="Seg_12090" s="T277">v</ta>
            <ta e="T279" id="Seg_12091" s="T278">v</ta>
            <ta e="T280" id="Seg_12092" s="T279">que</ta>
            <ta e="T281" id="Seg_12093" s="T280">n</ta>
            <ta e="T282" id="Seg_12094" s="T281">cop</ta>
            <ta e="T283" id="Seg_12095" s="T282">ptcl</ta>
            <ta e="T284" id="Seg_12096" s="T283">adv</ta>
            <ta e="T285" id="Seg_12097" s="T284">n</ta>
            <ta e="T286" id="Seg_12098" s="T285">cop</ta>
            <ta e="T287" id="Seg_12099" s="T286">v</ta>
            <ta e="T288" id="Seg_12100" s="T287">n</ta>
            <ta e="T289" id="Seg_12101" s="T288">adv</ta>
            <ta e="T290" id="Seg_12102" s="T289">v</ta>
            <ta e="T291" id="Seg_12103" s="T290">n</ta>
            <ta e="T292" id="Seg_12104" s="T291">v</ta>
            <ta e="T293" id="Seg_12105" s="T292">n</ta>
            <ta e="T294" id="Seg_12106" s="T293">v</ta>
            <ta e="T295" id="Seg_12107" s="T294">adj</ta>
            <ta e="T296" id="Seg_12108" s="T295">n</ta>
            <ta e="T297" id="Seg_12109" s="T296">n</ta>
            <ta e="T298" id="Seg_12110" s="T297">dempro</ta>
            <ta e="T299" id="Seg_12111" s="T298">adv</ta>
            <ta e="T300" id="Seg_12112" s="T299">v</ta>
            <ta e="T301" id="Seg_12113" s="T300">v</ta>
            <ta e="T302" id="Seg_12114" s="T301">v</ta>
            <ta e="T303" id="Seg_12115" s="T302">v</ta>
            <ta e="T304" id="Seg_12116" s="T303">n</ta>
            <ta e="T305" id="Seg_12117" s="T304">v</ta>
            <ta e="T306" id="Seg_12118" s="T305">ptcl</ta>
            <ta e="T307" id="Seg_12119" s="T306">adv</ta>
            <ta e="T308" id="Seg_12120" s="T307">v</ta>
            <ta e="T309" id="Seg_12121" s="T308">cardnum</ta>
            <ta e="T310" id="Seg_12122" s="T309">n</ta>
            <ta e="T311" id="Seg_12123" s="T310">n</ta>
            <ta e="T312" id="Seg_12124" s="T311">v</ta>
            <ta e="T313" id="Seg_12125" s="T312">v</ta>
            <ta e="T314" id="Seg_12126" s="T313">n</ta>
            <ta e="T315" id="Seg_12127" s="T314">ptcl</ta>
            <ta e="T316" id="Seg_12128" s="T315">adj</ta>
            <ta e="T317" id="Seg_12129" s="T316">n</ta>
            <ta e="T318" id="Seg_12130" s="T317">v</ta>
            <ta e="T319" id="Seg_12131" s="T318">v</ta>
            <ta e="T320" id="Seg_12132" s="T319">n</ta>
            <ta e="T321" id="Seg_12133" s="T320">cardnum</ta>
            <ta e="T322" id="Seg_12134" s="T321">adj</ta>
            <ta e="T323" id="Seg_12135" s="T322">n</ta>
            <ta e="T324" id="Seg_12136" s="T323">v</ta>
            <ta e="T325" id="Seg_12137" s="T324">v</ta>
            <ta e="T326" id="Seg_12138" s="T325">n</ta>
            <ta e="T327" id="Seg_12139" s="T326">v</ta>
            <ta e="T328" id="Seg_12140" s="T327">que</ta>
            <ta e="T329" id="Seg_12141" s="T328">ptcl</ta>
            <ta e="T330" id="Seg_12142" s="T329">n</ta>
            <ta e="T331" id="Seg_12143" s="T330">n</ta>
            <ta e="T332" id="Seg_12144" s="T331">ptcl</ta>
            <ta e="T333" id="Seg_12145" s="T332">ptcl</ta>
            <ta e="T334" id="Seg_12146" s="T333">adv</ta>
            <ta e="T335" id="Seg_12147" s="T334">v</ta>
            <ta e="T336" id="Seg_12148" s="T335">n</ta>
            <ta e="T337" id="Seg_12149" s="T336">n</ta>
            <ta e="T338" id="Seg_12150" s="T337">n</ta>
            <ta e="T339" id="Seg_12151" s="T338">n</ta>
            <ta e="T340" id="Seg_12152" s="T339">v</ta>
            <ta e="T341" id="Seg_12153" s="T340">n</ta>
            <ta e="T342" id="Seg_12154" s="T341">n</ta>
            <ta e="T343" id="Seg_12155" s="T342">v</ta>
            <ta e="T344" id="Seg_12156" s="T343">aux</ta>
            <ta e="T345" id="Seg_12157" s="T344">que</ta>
            <ta e="T346" id="Seg_12158" s="T345">adv</ta>
            <ta e="T347" id="Seg_12159" s="T346">v</ta>
            <ta e="T348" id="Seg_12160" s="T347">ptcl</ta>
            <ta e="T349" id="Seg_12161" s="T348">ptcl</ta>
            <ta e="T350" id="Seg_12162" s="T349">n</ta>
            <ta e="T351" id="Seg_12163" s="T350">n</ta>
            <ta e="T352" id="Seg_12164" s="T351">n</ta>
            <ta e="T353" id="Seg_12165" s="T352">v</ta>
            <ta e="T354" id="Seg_12166" s="T353">n</ta>
            <ta e="T355" id="Seg_12167" s="T354">v</ta>
            <ta e="T356" id="Seg_12168" s="T355">v</ta>
            <ta e="T357" id="Seg_12169" s="T356">adj</ta>
            <ta e="T358" id="Seg_12170" s="T357">ptcl</ta>
            <ta e="T359" id="Seg_12171" s="T358">n</ta>
            <ta e="T360" id="Seg_12172" s="T359">v</ta>
            <ta e="T361" id="Seg_12173" s="T360">n</ta>
            <ta e="T362" id="Seg_12174" s="T361">n</ta>
            <ta e="T363" id="Seg_12175" s="T362">v</ta>
            <ta e="T364" id="Seg_12176" s="T363">n</ta>
            <ta e="T365" id="Seg_12177" s="T364">n</ta>
            <ta e="T366" id="Seg_12178" s="T365">n</ta>
            <ta e="T367" id="Seg_12179" s="T366">v</ta>
            <ta e="T368" id="Seg_12180" s="T367">n</ta>
            <ta e="T369" id="Seg_12181" s="T368">n</ta>
            <ta e="T370" id="Seg_12182" s="T369">n</ta>
            <ta e="T371" id="Seg_12183" s="T370">post</ta>
            <ta e="T372" id="Seg_12184" s="T371">n</ta>
            <ta e="T373" id="Seg_12185" s="T372">n</ta>
            <ta e="T374" id="Seg_12186" s="T373">adj</ta>
            <ta e="T375" id="Seg_12187" s="T374">n</ta>
            <ta e="T376" id="Seg_12188" s="T375">v</ta>
            <ta e="T377" id="Seg_12189" s="T376">aux</ta>
            <ta e="T378" id="Seg_12190" s="T377">n</ta>
            <ta e="T379" id="Seg_12191" s="T378">n</ta>
            <ta e="T380" id="Seg_12192" s="T379">v</ta>
            <ta e="T381" id="Seg_12193" s="T380">aux</ta>
            <ta e="T382" id="Seg_12194" s="T381">n</ta>
            <ta e="T383" id="Seg_12195" s="T382">adj</ta>
            <ta e="T384" id="Seg_12196" s="T383">cop</ta>
            <ta e="T385" id="Seg_12197" s="T384">n</ta>
            <ta e="T386" id="Seg_12198" s="T385">v</ta>
            <ta e="T387" id="Seg_12199" s="T386">n</ta>
            <ta e="T388" id="Seg_12200" s="T387">v</ta>
            <ta e="T389" id="Seg_12201" s="T388">n</ta>
            <ta e="T390" id="Seg_12202" s="T389">v</ta>
            <ta e="T391" id="Seg_12203" s="T390">n</ta>
            <ta e="T392" id="Seg_12204" s="T391">emphpro</ta>
            <ta e="T393" id="Seg_12205" s="T392">v</ta>
            <ta e="T394" id="Seg_12206" s="T393">adv</ta>
            <ta e="T395" id="Seg_12207" s="T394">n</ta>
            <ta e="T396" id="Seg_12208" s="T395">v</ta>
            <ta e="T397" id="Seg_12209" s="T396">adj</ta>
            <ta e="T398" id="Seg_12210" s="T397">n</ta>
            <ta e="T399" id="Seg_12211" s="T398">cop</ta>
            <ta e="T400" id="Seg_12212" s="T399">n</ta>
            <ta e="T401" id="Seg_12213" s="T400">v</ta>
            <ta e="T402" id="Seg_12214" s="T401">n</ta>
            <ta e="T403" id="Seg_12215" s="T402">n</ta>
            <ta e="T404" id="Seg_12216" s="T403">v</ta>
            <ta e="T405" id="Seg_12217" s="T404">n</ta>
            <ta e="T406" id="Seg_12218" s="T405">v</ta>
            <ta e="T407" id="Seg_12219" s="T406">n</ta>
            <ta e="T408" id="Seg_12220" s="T407">v</ta>
            <ta e="T409" id="Seg_12221" s="T408">n</ta>
            <ta e="T410" id="Seg_12222" s="T409">n</ta>
            <ta e="T411" id="Seg_12223" s="T410">adj</ta>
            <ta e="T412" id="Seg_12224" s="T411">n</ta>
            <ta e="T413" id="Seg_12225" s="T412">adv</ta>
            <ta e="T414" id="Seg_12226" s="T413">v</ta>
            <ta e="T415" id="Seg_12227" s="T414">n</ta>
            <ta e="T416" id="Seg_12228" s="T415">adv</ta>
            <ta e="T417" id="Seg_12229" s="T416">v</ta>
            <ta e="T418" id="Seg_12230" s="T417">n</ta>
            <ta e="T419" id="Seg_12231" s="T418">v</ta>
            <ta e="T420" id="Seg_12232" s="T419">v</ta>
            <ta e="T421" id="Seg_12233" s="T420">n</ta>
            <ta e="T422" id="Seg_12234" s="T421">v</ta>
            <ta e="T423" id="Seg_12235" s="T422">n</ta>
            <ta e="T424" id="Seg_12236" s="T423">v</ta>
            <ta e="T425" id="Seg_12237" s="T424">n</ta>
            <ta e="T426" id="Seg_12238" s="T425">n</ta>
            <ta e="T427" id="Seg_12239" s="T426">adj</ta>
            <ta e="T428" id="Seg_12240" s="T427">v</ta>
            <ta e="T429" id="Seg_12241" s="T428">n</ta>
            <ta e="T430" id="Seg_12242" s="T429">n</ta>
            <ta e="T431" id="Seg_12243" s="T430">n</ta>
            <ta e="T432" id="Seg_12244" s="T431">v</ta>
            <ta e="T433" id="Seg_12245" s="T432">v</ta>
            <ta e="T434" id="Seg_12246" s="T433">adv</ta>
            <ta e="T435" id="Seg_12247" s="T434">v</ta>
            <ta e="T436" id="Seg_12248" s="T435">ptcl</ta>
            <ta e="T437" id="Seg_12249" s="T436">n</ta>
            <ta e="T438" id="Seg_12250" s="T437">n</ta>
            <ta e="T439" id="Seg_12251" s="T438">n</ta>
            <ta e="T440" id="Seg_12252" s="T439">v</ta>
            <ta e="T441" id="Seg_12253" s="T440">n</ta>
            <ta e="T442" id="Seg_12254" s="T441">n</ta>
            <ta e="T443" id="Seg_12255" s="T442">v</ta>
            <ta e="T444" id="Seg_12256" s="T443">n</ta>
            <ta e="T445" id="Seg_12257" s="T444">n</ta>
            <ta e="T446" id="Seg_12258" s="T445">v</ta>
            <ta e="T447" id="Seg_12259" s="T446">n</ta>
            <ta e="T448" id="Seg_12260" s="T447">v</ta>
            <ta e="T449" id="Seg_12261" s="T448">n</ta>
            <ta e="T450" id="Seg_12262" s="T449">ptcl</ta>
            <ta e="T451" id="Seg_12263" s="T450">adv</ta>
            <ta e="T452" id="Seg_12264" s="T451">n</ta>
            <ta e="T453" id="Seg_12265" s="T452">cop</ta>
            <ta e="T454" id="Seg_12266" s="T453">adv</ta>
            <ta e="T455" id="Seg_12267" s="T454">v</ta>
            <ta e="T456" id="Seg_12268" s="T455">v</ta>
            <ta e="T0" id="Seg_12269" s="T456">n</ta>
            <ta e="T947" id="Seg_12270" s="T0">adj</ta>
            <ta e="T948" id="Seg_12271" s="T947">n</ta>
            <ta e="T457" id="Seg_12272" s="T948">v</ta>
            <ta e="T458" id="Seg_12273" s="T457">n</ta>
            <ta e="T459" id="Seg_12274" s="T458">cop</ta>
            <ta e="T460" id="Seg_12275" s="T459">n</ta>
            <ta e="T461" id="Seg_12276" s="T460">n</ta>
            <ta e="T462" id="Seg_12277" s="T461">n</ta>
            <ta e="T463" id="Seg_12278" s="T462">n</ta>
            <ta e="T464" id="Seg_12279" s="T463">n</ta>
            <ta e="T465" id="Seg_12280" s="T464">n</ta>
            <ta e="T466" id="Seg_12281" s="T465">n</ta>
            <ta e="T467" id="Seg_12282" s="T466">v</ta>
            <ta e="T468" id="Seg_12283" s="T467">aux</ta>
            <ta e="T469" id="Seg_12284" s="T468">n</ta>
            <ta e="T470" id="Seg_12285" s="T469">ptcl</ta>
            <ta e="T471" id="Seg_12286" s="T470">v</ta>
            <ta e="T472" id="Seg_12287" s="T471">n</ta>
            <ta e="T473" id="Seg_12288" s="T472">v</ta>
            <ta e="T474" id="Seg_12289" s="T473">n</ta>
            <ta e="T475" id="Seg_12290" s="T474">adv</ta>
            <ta e="T476" id="Seg_12291" s="T475">v</ta>
            <ta e="T477" id="Seg_12292" s="T476">adv</ta>
            <ta e="T478" id="Seg_12293" s="T477">adj</ta>
            <ta e="T479" id="Seg_12294" s="T478">n</ta>
            <ta e="T480" id="Seg_12295" s="T479">v</ta>
            <ta e="T481" id="Seg_12296" s="T480">n</ta>
            <ta e="T482" id="Seg_12297" s="T481">adv</ta>
            <ta e="T483" id="Seg_12298" s="T482">v</ta>
            <ta e="T484" id="Seg_12299" s="T483">que</ta>
            <ta e="T485" id="Seg_12300" s="T484">ptcl</ta>
            <ta e="T486" id="Seg_12301" s="T485">adj</ta>
            <ta e="T487" id="Seg_12302" s="T486">n</ta>
            <ta e="T488" id="Seg_12303" s="T487">v</ta>
            <ta e="T489" id="Seg_12304" s="T488">que</ta>
            <ta e="T490" id="Seg_12305" s="T489">ptcl</ta>
            <ta e="T491" id="Seg_12306" s="T490">n</ta>
            <ta e="T492" id="Seg_12307" s="T491">n</ta>
            <ta e="T493" id="Seg_12308" s="T492">ptcl</ta>
            <ta e="T494" id="Seg_12309" s="T493">n</ta>
            <ta e="T495" id="Seg_12310" s="T494">v</ta>
            <ta e="T496" id="Seg_12311" s="T495">n</ta>
            <ta e="T497" id="Seg_12312" s="T496">post</ta>
            <ta e="T498" id="Seg_12313" s="T497">n</ta>
            <ta e="T499" id="Seg_12314" s="T498">n</ta>
            <ta e="T500" id="Seg_12315" s="T499">v</ta>
            <ta e="T501" id="Seg_12316" s="T500">v</ta>
            <ta e="T502" id="Seg_12317" s="T501">n</ta>
            <ta e="T503" id="Seg_12318" s="T502">n</ta>
            <ta e="T504" id="Seg_12319" s="T503">post</ta>
            <ta e="T505" id="Seg_12320" s="T504">v</ta>
            <ta e="T506" id="Seg_12321" s="T505">n</ta>
            <ta e="T507" id="Seg_12322" s="T506">adv</ta>
            <ta e="T508" id="Seg_12323" s="T507">v</ta>
            <ta e="T509" id="Seg_12324" s="T508">n</ta>
            <ta e="T510" id="Seg_12325" s="T509">v</ta>
            <ta e="T511" id="Seg_12326" s="T510">n</ta>
            <ta e="T512" id="Seg_12327" s="T511">n</ta>
            <ta e="T513" id="Seg_12328" s="T512">v</ta>
            <ta e="T514" id="Seg_12329" s="T513">n</ta>
            <ta e="T515" id="Seg_12330" s="T514">v</ta>
            <ta e="T516" id="Seg_12331" s="T515">v</ta>
            <ta e="T517" id="Seg_12332" s="T516">n</ta>
            <ta e="T518" id="Seg_12333" s="T517">v</ta>
            <ta e="T519" id="Seg_12334" s="T518">adj</ta>
            <ta e="T520" id="Seg_12335" s="T519">n</ta>
            <ta e="T521" id="Seg_12336" s="T520">v</ta>
            <ta e="T522" id="Seg_12337" s="T521">que</ta>
            <ta e="T523" id="Seg_12338" s="T522">v</ta>
            <ta e="T524" id="Seg_12339" s="T523">ptcl</ta>
            <ta e="T525" id="Seg_12340" s="T524">que</ta>
            <ta e="T526" id="Seg_12341" s="T525">v</ta>
            <ta e="T527" id="Seg_12342" s="T526">adj</ta>
            <ta e="T528" id="Seg_12343" s="T527">n</ta>
            <ta e="T529" id="Seg_12344" s="T528">emphpro</ta>
            <ta e="T530" id="Seg_12345" s="T529">v</ta>
            <ta e="T531" id="Seg_12346" s="T530">n</ta>
            <ta e="T532" id="Seg_12347" s="T531">v</ta>
            <ta e="T533" id="Seg_12348" s="T532">n</ta>
            <ta e="T534" id="Seg_12349" s="T533">n</ta>
            <ta e="T535" id="Seg_12350" s="T534">n</ta>
            <ta e="T536" id="Seg_12351" s="T535">v</ta>
            <ta e="T537" id="Seg_12352" s="T536">adv</ta>
            <ta e="T538" id="Seg_12353" s="T537">adj</ta>
            <ta e="T539" id="Seg_12354" s="T538">adj</ta>
            <ta e="T540" id="Seg_12355" s="T539">n</ta>
            <ta e="T541" id="Seg_12356" s="T540">v</ta>
            <ta e="T542" id="Seg_12357" s="T541">aux</ta>
            <ta e="T543" id="Seg_12358" s="T542">dempro</ta>
            <ta e="T544" id="Seg_12359" s="T543">v</ta>
            <ta e="T545" id="Seg_12360" s="T544">n</ta>
            <ta e="T546" id="Seg_12361" s="T545">n</ta>
            <ta e="T547" id="Seg_12362" s="T546">v</ta>
            <ta e="T548" id="Seg_12363" s="T547">n</ta>
            <ta e="T549" id="Seg_12364" s="T548">v</ta>
            <ta e="T550" id="Seg_12365" s="T549">n</ta>
            <ta e="T551" id="Seg_12366" s="T550">v</ta>
            <ta e="T552" id="Seg_12367" s="T551">adv</ta>
            <ta e="T553" id="Seg_12368" s="T552">v</ta>
            <ta e="T554" id="Seg_12369" s="T553">post</ta>
            <ta e="T555" id="Seg_12370" s="T554">v</ta>
            <ta e="T556" id="Seg_12371" s="T555">v</ta>
            <ta e="T557" id="Seg_12372" s="T556">v</ta>
            <ta e="T558" id="Seg_12373" s="T557">post</ta>
            <ta e="T559" id="Seg_12374" s="T558">n</ta>
            <ta e="T560" id="Seg_12375" s="T559">ptcl</ta>
            <ta e="T561" id="Seg_12376" s="T560">v</ta>
            <ta e="T562" id="Seg_12377" s="T561">que</ta>
            <ta e="T563" id="Seg_12378" s="T562">v</ta>
            <ta e="T564" id="Seg_12379" s="T563">que</ta>
            <ta e="T565" id="Seg_12380" s="T564">v</ta>
            <ta e="T566" id="Seg_12381" s="T565">cardnum</ta>
            <ta e="T567" id="Seg_12382" s="T566">n</ta>
            <ta e="T568" id="Seg_12383" s="T567">adj</ta>
            <ta e="T569" id="Seg_12384" s="T568">cop</ta>
            <ta e="T570" id="Seg_12385" s="T569">aux</ta>
            <ta e="T571" id="Seg_12386" s="T570">n</ta>
            <ta e="T572" id="Seg_12387" s="T571">v</ta>
            <ta e="T573" id="Seg_12388" s="T572">aux</ta>
            <ta e="T574" id="Seg_12389" s="T573">aux</ta>
            <ta e="T575" id="Seg_12390" s="T574">n</ta>
            <ta e="T576" id="Seg_12391" s="T575">n</ta>
            <ta e="T577" id="Seg_12392" s="T576">v</ta>
            <ta e="T578" id="Seg_12393" s="T577">pers</ta>
            <ta e="T579" id="Seg_12394" s="T578">v</ta>
            <ta e="T580" id="Seg_12395" s="T579">v</ta>
            <ta e="T581" id="Seg_12396" s="T580">n</ta>
            <ta e="T582" id="Seg_12397" s="T581">n</ta>
            <ta e="T583" id="Seg_12398" s="T582">adj</ta>
            <ta e="T584" id="Seg_12399" s="T583">adj</ta>
            <ta e="T585" id="Seg_12400" s="T584">n</ta>
            <ta e="T586" id="Seg_12401" s="T585">v</ta>
            <ta e="T587" id="Seg_12402" s="T586">n</ta>
            <ta e="T588" id="Seg_12403" s="T587">n</ta>
            <ta e="T589" id="Seg_12404" s="T588">n</ta>
            <ta e="T590" id="Seg_12405" s="T589">adv</ta>
            <ta e="T591" id="Seg_12406" s="T590">v</ta>
            <ta e="T592" id="Seg_12407" s="T591">n</ta>
            <ta e="T593" id="Seg_12408" s="T592">adv</ta>
            <ta e="T594" id="Seg_12409" s="T593">v</ta>
            <ta e="T595" id="Seg_12410" s="T594">aux</ta>
            <ta e="T596" id="Seg_12411" s="T595">v</ta>
            <ta e="T597" id="Seg_12412" s="T596">v</ta>
            <ta e="T598" id="Seg_12413" s="T597">n</ta>
            <ta e="T599" id="Seg_12414" s="T598">v</ta>
            <ta e="T600" id="Seg_12415" s="T599">v</ta>
            <ta e="T601" id="Seg_12416" s="T600">adv</ta>
            <ta e="T602" id="Seg_12417" s="T601">adj</ta>
            <ta e="T603" id="Seg_12418" s="T602">ptcl</ta>
            <ta e="T604" id="Seg_12419" s="T603">n</ta>
            <ta e="T605" id="Seg_12420" s="T604">cop</ta>
            <ta e="T606" id="Seg_12421" s="T605">n</ta>
            <ta e="T607" id="Seg_12422" s="T606">v</ta>
            <ta e="T608" id="Seg_12423" s="T607">n</ta>
            <ta e="T609" id="Seg_12424" s="T608">ptcl</ta>
            <ta e="T610" id="Seg_12425" s="T609">adv</ta>
            <ta e="T611" id="Seg_12426" s="T610">emphpro</ta>
            <ta e="T612" id="Seg_12427" s="T611">n</ta>
            <ta e="T613" id="Seg_12428" s="T612">v</ta>
            <ta e="T614" id="Seg_12429" s="T613">v</ta>
            <ta e="T615" id="Seg_12430" s="T614">ptcl</ta>
            <ta e="T616" id="Seg_12431" s="T615">v</ta>
            <ta e="T617" id="Seg_12432" s="T616">adj</ta>
            <ta e="T618" id="Seg_12433" s="T617">n</ta>
            <ta e="T619" id="Seg_12434" s="T618">v</ta>
            <ta e="T620" id="Seg_12435" s="T619">v</ta>
            <ta e="T621" id="Seg_12436" s="T620">n</ta>
            <ta e="T622" id="Seg_12437" s="T621">adj</ta>
            <ta e="T623" id="Seg_12438" s="T622">ptcl</ta>
            <ta e="T624" id="Seg_12439" s="T623">n</ta>
            <ta e="T625" id="Seg_12440" s="T624">n</ta>
            <ta e="T626" id="Seg_12441" s="T625">n</ta>
            <ta e="T627" id="Seg_12442" s="T626">n</ta>
            <ta e="T628" id="Seg_12443" s="T627">quant</ta>
            <ta e="T629" id="Seg_12444" s="T628">n</ta>
            <ta e="T630" id="Seg_12445" s="T629">v</ta>
            <ta e="T631" id="Seg_12446" s="T630">dempro</ta>
            <ta e="T632" id="Seg_12447" s="T631">post</ta>
            <ta e="T633" id="Seg_12448" s="T632">n</ta>
            <ta e="T634" id="Seg_12449" s="T633">n</ta>
            <ta e="T635" id="Seg_12450" s="T634">v</ta>
            <ta e="T636" id="Seg_12451" s="T635">v</ta>
            <ta e="T637" id="Seg_12452" s="T636">aux</ta>
            <ta e="T638" id="Seg_12453" s="T637">pers</ta>
            <ta e="T639" id="Seg_12454" s="T638">n</ta>
            <ta e="T640" id="Seg_12455" s="T639">v</ta>
            <ta e="T641" id="Seg_12456" s="T640">ptcl</ta>
            <ta e="T642" id="Seg_12457" s="T641">adv</ta>
            <ta e="T643" id="Seg_12458" s="T642">v</ta>
            <ta e="T644" id="Seg_12459" s="T643">ptcl</ta>
            <ta e="T645" id="Seg_12460" s="T644">v</ta>
            <ta e="T646" id="Seg_12461" s="T645">n</ta>
            <ta e="T647" id="Seg_12462" s="T646">que</ta>
            <ta e="T648" id="Seg_12463" s="T647">v</ta>
            <ta e="T649" id="Seg_12464" s="T648">que</ta>
            <ta e="T650" id="Seg_12465" s="T649">ptcl</ta>
            <ta e="T651" id="Seg_12466" s="T650">v</ta>
            <ta e="T652" id="Seg_12467" s="T651">adv</ta>
            <ta e="T653" id="Seg_12468" s="T652">n</ta>
            <ta e="T654" id="Seg_12469" s="T653">v</ta>
            <ta e="T655" id="Seg_12470" s="T654">adj</ta>
            <ta e="T656" id="Seg_12471" s="T655">ptcl</ta>
            <ta e="T657" id="Seg_12472" s="T656">n</ta>
            <ta e="T658" id="Seg_12473" s="T657">v</ta>
            <ta e="T659" id="Seg_12474" s="T658">v</ta>
            <ta e="T660" id="Seg_12475" s="T659">ptcl</ta>
            <ta e="T661" id="Seg_12476" s="T660">v</ta>
            <ta e="T662" id="Seg_12477" s="T661">v</ta>
            <ta e="T663" id="Seg_12478" s="T662">n</ta>
            <ta e="T664" id="Seg_12479" s="T663">n</ta>
            <ta e="T665" id="Seg_12480" s="T664">n</ta>
            <ta e="T666" id="Seg_12481" s="T665">v</ta>
            <ta e="T667" id="Seg_12482" s="T666">n</ta>
            <ta e="T668" id="Seg_12483" s="T667">v</ta>
            <ta e="T669" id="Seg_12484" s="T668">adv</ta>
            <ta e="T670" id="Seg_12485" s="T669">v</ta>
            <ta e="T671" id="Seg_12486" s="T670">v</ta>
            <ta e="T672" id="Seg_12487" s="T671">adv</ta>
            <ta e="T673" id="Seg_12488" s="T672">v</ta>
            <ta e="T674" id="Seg_12489" s="T673">v</ta>
            <ta e="T675" id="Seg_12490" s="T674">v</ta>
            <ta e="T676" id="Seg_12491" s="T675">post</ta>
            <ta e="T677" id="Seg_12492" s="T676">adj</ta>
            <ta e="T678" id="Seg_12493" s="T677">v</ta>
            <ta e="T679" id="Seg_12494" s="T678">n</ta>
            <ta e="T680" id="Seg_12495" s="T679">n</ta>
            <ta e="T681" id="Seg_12496" s="T680">ptcl</ta>
            <ta e="T682" id="Seg_12497" s="T681">v</ta>
            <ta e="T683" id="Seg_12498" s="T682">aux</ta>
            <ta e="T684" id="Seg_12499" s="T683">que</ta>
            <ta e="T685" id="Seg_12500" s="T684">v</ta>
            <ta e="T686" id="Seg_12501" s="T685">que</ta>
            <ta e="T687" id="Seg_12502" s="T686">v</ta>
            <ta e="T688" id="Seg_12503" s="T687">ptcl</ta>
            <ta e="T689" id="Seg_12504" s="T688">n</ta>
            <ta e="T690" id="Seg_12505" s="T689">v</ta>
            <ta e="T691" id="Seg_12506" s="T690">pers</ta>
            <ta e="T692" id="Seg_12507" s="T691">pers</ta>
            <ta e="T693" id="Seg_12508" s="T692">v</ta>
            <ta e="T694" id="Seg_12509" s="T693">v</ta>
            <ta e="T695" id="Seg_12510" s="T694">n</ta>
            <ta e="T696" id="Seg_12511" s="T695">n</ta>
            <ta e="T697" id="Seg_12512" s="T696">que</ta>
            <ta e="T698" id="Seg_12513" s="T697">v</ta>
            <ta e="T699" id="Seg_12514" s="T698">ptcl</ta>
            <ta e="T700" id="Seg_12515" s="T699">dempro</ta>
            <ta e="T701" id="Seg_12516" s="T700">n</ta>
            <ta e="T702" id="Seg_12517" s="T701">n</ta>
            <ta e="T703" id="Seg_12518" s="T702">v</ta>
            <ta e="T704" id="Seg_12519" s="T703">cardnum</ta>
            <ta e="T705" id="Seg_12520" s="T704">n</ta>
            <ta e="T706" id="Seg_12521" s="T705">v</ta>
            <ta e="T707" id="Seg_12522" s="T706">ptcl</ta>
            <ta e="T708" id="Seg_12523" s="T707">emphpro</ta>
            <ta e="T709" id="Seg_12524" s="T708">n</ta>
            <ta e="T710" id="Seg_12525" s="T709">v</ta>
            <ta e="T711" id="Seg_12526" s="T710">aux</ta>
            <ta e="T712" id="Seg_12527" s="T711">v</ta>
            <ta e="T713" id="Seg_12528" s="T712">v</ta>
            <ta e="T714" id="Seg_12529" s="T713">n</ta>
            <ta e="T715" id="Seg_12530" s="T714">n</ta>
            <ta e="T716" id="Seg_12531" s="T715">adv</ta>
            <ta e="T717" id="Seg_12532" s="T716">n</ta>
            <ta e="T718" id="Seg_12533" s="T717">n</ta>
            <ta e="T719" id="Seg_12534" s="T718">v</ta>
            <ta e="T720" id="Seg_12535" s="T719">n</ta>
            <ta e="T721" id="Seg_12536" s="T720">v</ta>
            <ta e="T722" id="Seg_12537" s="T721">n</ta>
            <ta e="T723" id="Seg_12538" s="T722">n</ta>
            <ta e="T724" id="Seg_12539" s="T723">v</ta>
            <ta e="T725" id="Seg_12540" s="T724">adj</ta>
            <ta e="T726" id="Seg_12541" s="T725">adj</ta>
            <ta e="T727" id="Seg_12542" s="T726">n</ta>
            <ta e="T728" id="Seg_12543" s="T727">v</ta>
            <ta e="T729" id="Seg_12544" s="T728">n</ta>
            <ta e="T730" id="Seg_12545" s="T729">v</ta>
            <ta e="T731" id="Seg_12546" s="T730">post</ta>
            <ta e="T732" id="Seg_12547" s="T731">v</ta>
            <ta e="T733" id="Seg_12548" s="T732">adv</ta>
            <ta e="T734" id="Seg_12549" s="T733">n</ta>
            <ta e="T735" id="Seg_12550" s="T734">v</ta>
            <ta e="T736" id="Seg_12551" s="T735">n</ta>
            <ta e="T737" id="Seg_12552" s="T736">v</ta>
            <ta e="T738" id="Seg_12553" s="T737">v</ta>
            <ta e="T739" id="Seg_12554" s="T738">v</ta>
            <ta e="T740" id="Seg_12555" s="T739">n</ta>
            <ta e="T741" id="Seg_12556" s="T740">adv</ta>
            <ta e="T742" id="Seg_12557" s="T741">adj</ta>
            <ta e="T743" id="Seg_12558" s="T742">v</ta>
            <ta e="T744" id="Seg_12559" s="T743">adj</ta>
            <ta e="T745" id="Seg_12560" s="T744">n</ta>
            <ta e="T746" id="Seg_12561" s="T745">cop</ta>
            <ta e="T747" id="Seg_12562" s="T746">v</ta>
            <ta e="T748" id="Seg_12563" s="T747">aux</ta>
            <ta e="T749" id="Seg_12564" s="T748">adj</ta>
            <ta e="T750" id="Seg_12565" s="T749">n</ta>
            <ta e="T751" id="Seg_12566" s="T750">v</ta>
            <ta e="T752" id="Seg_12567" s="T751">n</ta>
            <ta e="T753" id="Seg_12568" s="T752">v</ta>
            <ta e="T754" id="Seg_12569" s="T753">n</ta>
            <ta e="T755" id="Seg_12570" s="T754">n</ta>
            <ta e="T756" id="Seg_12571" s="T755">v</ta>
            <ta e="T757" id="Seg_12572" s="T756">adv</ta>
            <ta e="T758" id="Seg_12573" s="T757">v</ta>
            <ta e="T759" id="Seg_12574" s="T758">adv</ta>
            <ta e="T760" id="Seg_12575" s="T759">pers</ta>
            <ta e="T761" id="Seg_12576" s="T760">v</ta>
            <ta e="T762" id="Seg_12577" s="T761">n</ta>
            <ta e="T763" id="Seg_12578" s="T762">adv</ta>
            <ta e="T764" id="Seg_12579" s="T763">adv</ta>
            <ta e="T765" id="Seg_12580" s="T764">v</ta>
            <ta e="T766" id="Seg_12581" s="T765">adv</ta>
            <ta e="T767" id="Seg_12582" s="T766">pers</ta>
            <ta e="T768" id="Seg_12583" s="T767">n</ta>
            <ta e="T769" id="Seg_12584" s="T768">v</ta>
            <ta e="T770" id="Seg_12585" s="T769">aux</ta>
            <ta e="T771" id="Seg_12586" s="T770">n</ta>
            <ta e="T772" id="Seg_12587" s="T771">post</ta>
            <ta e="T773" id="Seg_12588" s="T772">v</ta>
            <ta e="T774" id="Seg_12589" s="T773">aux</ta>
            <ta e="T775" id="Seg_12590" s="T774">n</ta>
            <ta e="T776" id="Seg_12591" s="T775">n</ta>
            <ta e="T777" id="Seg_12592" s="T776">n</ta>
            <ta e="T778" id="Seg_12593" s="T777">v</ta>
            <ta e="T779" id="Seg_12594" s="T778">n</ta>
            <ta e="T780" id="Seg_12595" s="T779">v</ta>
            <ta e="T781" id="Seg_12596" s="T780">v</ta>
            <ta e="T782" id="Seg_12597" s="T781">que</ta>
            <ta e="T783" id="Seg_12598" s="T782">ptcl</ta>
            <ta e="T784" id="Seg_12599" s="T783">cop</ta>
            <ta e="T785" id="Seg_12600" s="T784">dempro</ta>
            <ta e="T786" id="Seg_12601" s="T785">n</ta>
            <ta e="T787" id="Seg_12602" s="T786">v</ta>
            <ta e="T788" id="Seg_12603" s="T787">que</ta>
            <ta e="T789" id="Seg_12604" s="T788">v</ta>
            <ta e="T790" id="Seg_12605" s="T789">v</ta>
            <ta e="T791" id="Seg_12606" s="T790">n</ta>
            <ta e="T792" id="Seg_12607" s="T791">adv</ta>
            <ta e="T793" id="Seg_12608" s="T792">v</ta>
            <ta e="T794" id="Seg_12609" s="T793">v</ta>
            <ta e="T795" id="Seg_12610" s="T794">n</ta>
            <ta e="T796" id="Seg_12611" s="T795">dempro</ta>
            <ta e="T797" id="Seg_12612" s="T796">n</ta>
            <ta e="T798" id="Seg_12613" s="T797">v</ta>
            <ta e="T799" id="Seg_12614" s="T798">v</ta>
            <ta e="T800" id="Seg_12615" s="T799">n</ta>
            <ta e="T801" id="Seg_12616" s="T800">n</ta>
            <ta e="T802" id="Seg_12617" s="T801">n</ta>
            <ta e="T803" id="Seg_12618" s="T802">post</ta>
            <ta e="T804" id="Seg_12619" s="T803">aux</ta>
            <ta e="T805" id="Seg_12620" s="T804">adj</ta>
            <ta e="T806" id="Seg_12621" s="T805">ptcl</ta>
            <ta e="T807" id="Seg_12622" s="T806">n</ta>
            <ta e="T808" id="Seg_12623" s="T807">v</ta>
            <ta e="T809" id="Seg_12624" s="T808">n</ta>
            <ta e="T810" id="Seg_12625" s="T809">ptcl</ta>
            <ta e="T811" id="Seg_12626" s="T810">v</ta>
            <ta e="T812" id="Seg_12627" s="T811">v</ta>
            <ta e="T813" id="Seg_12628" s="T812">n</ta>
            <ta e="T814" id="Seg_12629" s="T813">ptcl</ta>
            <ta e="T815" id="Seg_12630" s="T814">v</ta>
            <ta e="T816" id="Seg_12631" s="T815">v</ta>
            <ta e="T817" id="Seg_12632" s="T816">v</ta>
            <ta e="T818" id="Seg_12633" s="T817">v</ta>
            <ta e="T819" id="Seg_12634" s="T818">n</ta>
            <ta e="T820" id="Seg_12635" s="T819">n</ta>
            <ta e="T821" id="Seg_12636" s="T820">cop</ta>
            <ta e="T822" id="Seg_12637" s="T821">adj</ta>
            <ta e="T823" id="Seg_12638" s="T822">n</ta>
            <ta e="T824" id="Seg_12639" s="T823">v</ta>
            <ta e="T825" id="Seg_12640" s="T824">n</ta>
            <ta e="T826" id="Seg_12641" s="T825">n</ta>
            <ta e="T827" id="Seg_12642" s="T826">v</ta>
            <ta e="T828" id="Seg_12643" s="T827">v</ta>
            <ta e="T829" id="Seg_12644" s="T828">n</ta>
            <ta e="T830" id="Seg_12645" s="T829">cop</ta>
            <ta e="T831" id="Seg_12646" s="T830">n</ta>
            <ta e="T832" id="Seg_12647" s="T831">n</ta>
            <ta e="T833" id="Seg_12648" s="T832">v</ta>
            <ta e="T834" id="Seg_12649" s="T833">aux</ta>
            <ta e="T835" id="Seg_12650" s="T834">n</ta>
            <ta e="T836" id="Seg_12651" s="T835">v</ta>
            <ta e="T837" id="Seg_12652" s="T836">adj</ta>
            <ta e="T838" id="Seg_12653" s="T837">n</ta>
            <ta e="T839" id="Seg_12654" s="T838">n</ta>
            <ta e="T840" id="Seg_12655" s="T839">ptcl</ta>
            <ta e="T841" id="Seg_12656" s="T840">que</ta>
            <ta e="T842" id="Seg_12657" s="T841">ptcl</ta>
            <ta e="T843" id="Seg_12658" s="T842">cop</ta>
            <ta e="T844" id="Seg_12659" s="T843">n</ta>
            <ta e="T845" id="Seg_12660" s="T844">v</ta>
            <ta e="T846" id="Seg_12661" s="T845">dempro</ta>
            <ta e="T847" id="Seg_12662" s="T846">n</ta>
            <ta e="T848" id="Seg_12663" s="T847">v</ta>
            <ta e="T849" id="Seg_12664" s="T848">n</ta>
            <ta e="T850" id="Seg_12665" s="T849">n</ta>
            <ta e="T851" id="Seg_12666" s="T850">n</ta>
            <ta e="T852" id="Seg_12667" s="T851">v</ta>
            <ta e="T853" id="Seg_12668" s="T852">post</ta>
            <ta e="T854" id="Seg_12669" s="T853">v</ta>
            <ta e="T855" id="Seg_12670" s="T854">n</ta>
            <ta e="T856" id="Seg_12671" s="T855">v</ta>
            <ta e="T857" id="Seg_12672" s="T856">n</ta>
            <ta e="T858" id="Seg_12673" s="T857">v</ta>
            <ta e="T859" id="Seg_12674" s="T858">n</ta>
            <ta e="T860" id="Seg_12675" s="T859">ptcl</ta>
            <ta e="T861" id="Seg_12676" s="T860">post</ta>
            <ta e="T862" id="Seg_12677" s="T861">v</ta>
            <ta e="T863" id="Seg_12678" s="T862">n</ta>
            <ta e="T864" id="Seg_12679" s="T863">dempro</ta>
            <ta e="T865" id="Seg_12680" s="T864">v</ta>
            <ta e="T866" id="Seg_12681" s="T865">v</ta>
            <ta e="T867" id="Seg_12682" s="T866">n</ta>
            <ta e="T868" id="Seg_12683" s="T867">v</ta>
            <ta e="T869" id="Seg_12684" s="T868">v</ta>
            <ta e="T870" id="Seg_12685" s="T869">n</ta>
            <ta e="T871" id="Seg_12686" s="T870">v</ta>
            <ta e="T872" id="Seg_12687" s="T871">v</ta>
            <ta e="T873" id="Seg_12688" s="T872">n</ta>
            <ta e="T874" id="Seg_12689" s="T873">adj</ta>
            <ta e="T875" id="Seg_12690" s="T874">n</ta>
            <ta e="T876" id="Seg_12691" s="T875">cop</ta>
            <ta e="T877" id="Seg_12692" s="T876">n</ta>
            <ta e="T878" id="Seg_12693" s="T877">n</ta>
            <ta e="T879" id="Seg_12694" s="T878">cop</ta>
            <ta e="T880" id="Seg_12695" s="T879">adj</ta>
            <ta e="T881" id="Seg_12696" s="T880">n</ta>
            <ta e="T882" id="Seg_12697" s="T881">v</ta>
            <ta e="T883" id="Seg_12698" s="T882">n</ta>
            <ta e="T884" id="Seg_12699" s="T883">v</ta>
            <ta e="T885" id="Seg_12700" s="T884">n</ta>
            <ta e="T886" id="Seg_12701" s="T885">v</ta>
            <ta e="T887" id="Seg_12702" s="T886">que</ta>
            <ta e="T888" id="Seg_12703" s="T887">v</ta>
            <ta e="T889" id="Seg_12704" s="T888">v</ta>
            <ta e="T890" id="Seg_12705" s="T889">pers</ta>
            <ta e="T891" id="Seg_12706" s="T890">post</ta>
            <ta e="T892" id="Seg_12707" s="T891">v</ta>
            <ta e="T893" id="Seg_12708" s="T892">n</ta>
            <ta e="T894" id="Seg_12709" s="T893">n</ta>
            <ta e="T895" id="Seg_12710" s="T894">pers</ta>
            <ta e="T896" id="Seg_12711" s="T895">v</ta>
            <ta e="T897" id="Seg_12712" s="T896">v</ta>
            <ta e="T898" id="Seg_12713" s="T897">adv</ta>
            <ta e="T899" id="Seg_12714" s="T898">v</ta>
            <ta e="T900" id="Seg_12715" s="T899">n</ta>
            <ta e="T901" id="Seg_12716" s="T900">v</ta>
            <ta e="T902" id="Seg_12717" s="T901">n</ta>
            <ta e="T903" id="Seg_12718" s="T902">post</ta>
            <ta e="T904" id="Seg_12719" s="T903">n</ta>
            <ta e="T905" id="Seg_12720" s="T904">n</ta>
            <ta e="T906" id="Seg_12721" s="T905">n</ta>
            <ta e="T907" id="Seg_12722" s="T906">n</ta>
            <ta e="T908" id="Seg_12723" s="T907">v</ta>
            <ta e="T909" id="Seg_12724" s="T908">dempro</ta>
            <ta e="T910" id="Seg_12725" s="T909">adj</ta>
            <ta e="T911" id="Seg_12726" s="T910">n</ta>
            <ta e="T912" id="Seg_12727" s="T911">cop</ta>
            <ta e="T913" id="Seg_12728" s="T912">n</ta>
            <ta e="T914" id="Seg_12729" s="T913">n</ta>
            <ta e="T915" id="Seg_12730" s="T914">v</ta>
            <ta e="T916" id="Seg_12731" s="T915">v</ta>
            <ta e="T917" id="Seg_12732" s="T916">n</ta>
            <ta e="T918" id="Seg_12733" s="T917">cardnum</ta>
            <ta e="T919" id="Seg_12734" s="T918">n</ta>
            <ta e="T920" id="Seg_12735" s="T919">v</ta>
            <ta e="T921" id="Seg_12736" s="T920">v</ta>
            <ta e="T922" id="Seg_12737" s="T921">v</ta>
            <ta e="T923" id="Seg_12738" s="T922">n</ta>
            <ta e="T924" id="Seg_12739" s="T923">n</ta>
            <ta e="T925" id="Seg_12740" s="T924">post</ta>
            <ta e="T926" id="Seg_12741" s="T925">v</ta>
            <ta e="T927" id="Seg_12742" s="T926">v</ta>
            <ta e="T928" id="Seg_12743" s="T927">aux</ta>
            <ta e="T929" id="Seg_12744" s="T928">n</ta>
            <ta e="T930" id="Seg_12745" s="T929">v</ta>
            <ta e="T931" id="Seg_12746" s="T930">v</ta>
            <ta e="T932" id="Seg_12747" s="T931">n</ta>
            <ta e="T933" id="Seg_12748" s="T932">v</ta>
            <ta e="T934" id="Seg_12749" s="T933">n</ta>
            <ta e="T935" id="Seg_12750" s="T934">n</ta>
            <ta e="T936" id="Seg_12751" s="T935">v</ta>
            <ta e="T937" id="Seg_12752" s="T936">adj</ta>
            <ta e="T938" id="Seg_12753" s="T937">n</ta>
            <ta e="T939" id="Seg_12754" s="T938">v</ta>
            <ta e="T940" id="Seg_12755" s="T939">n</ta>
            <ta e="T941" id="Seg_12756" s="T940">n</ta>
            <ta e="T942" id="Seg_12757" s="T941">n</ta>
            <ta e="T943" id="Seg_12758" s="T942">v</ta>
            <ta e="T944" id="Seg_12759" s="T943">adj</ta>
            <ta e="T945" id="Seg_12760" s="T944">adv</ta>
            <ta e="T946" id="Seg_12761" s="T945">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_12762" s="T1">0.2.h:A</ta>
            <ta e="T3" id="Seg_12763" s="T2">np.h:Poss</ta>
            <ta e="T4" id="Seg_12764" s="T3">np:Th</ta>
            <ta e="T5" id="Seg_12765" s="T4">np:So</ta>
            <ta e="T9" id="Seg_12766" s="T8">np.h:Th</ta>
            <ta e="T11" id="Seg_12767" s="T9">pp:Com</ta>
            <ta e="T14" id="Seg_12768" s="T13">0.3.h:Poss np.h:Th</ta>
            <ta e="T18" id="Seg_12769" s="T17">0.3.h:Poss np:Poss</ta>
            <ta e="T22" id="Seg_12770" s="T21">np:Th</ta>
            <ta e="T24" id="Seg_12771" s="T23">np:P</ta>
            <ta e="T25" id="Seg_12772" s="T24">pro.h:A</ta>
            <ta e="T27" id="Seg_12773" s="T26">0.3.h:Poss np:Th</ta>
            <ta e="T29" id="Seg_12774" s="T28">adv:Time</ta>
            <ta e="T30" id="Seg_12775" s="T29">0.3.h:A</ta>
            <ta e="T32" id="Seg_12776" s="T31">0.3.h:Poss np.h:Th</ta>
            <ta e="T33" id="Seg_12777" s="T32">n:Time</ta>
            <ta e="T38" id="Seg_12778" s="T37">0.3.h:Poss np:A</ta>
            <ta e="T40" id="Seg_12779" s="T39">0.3.h:Th np:Th</ta>
            <ta e="T42" id="Seg_12780" s="T41">np.h:E</ta>
            <ta e="T43" id="Seg_12781" s="T42">np:Th</ta>
            <ta e="T45" id="Seg_12782" s="T44">np:Th</ta>
            <ta e="T46" id="Seg_12783" s="T45">0.3.h:A</ta>
            <ta e="T47" id="Seg_12784" s="T46">np:Th</ta>
            <ta e="T48" id="Seg_12785" s="T47">0.3.h:A</ta>
            <ta e="T49" id="Seg_12786" s="T48">0.3.h:A</ta>
            <ta e="T50" id="Seg_12787" s="T49">0.3.h:Poss np.h:P</ta>
            <ta e="T52" id="Seg_12788" s="T51">0.3.h:A</ta>
            <ta e="T54" id="Seg_12789" s="T53">0.3.h:P</ta>
            <ta e="T59" id="Seg_12790" s="T58">0.3.h:Poss np.h:Poss</ta>
            <ta e="T61" id="Seg_12791" s="T60">np:Th</ta>
            <ta e="T63" id="Seg_12792" s="T62">0.3.h:A</ta>
            <ta e="T65" id="Seg_12793" s="T64">0.3.h:P</ta>
            <ta e="T66" id="Seg_12794" s="T65">0.3.h:A</ta>
            <ta e="T67" id="Seg_12795" s="T66">np:G</ta>
            <ta e="T70" id="Seg_12796" s="T69">0.3.h:P</ta>
            <ta e="T75" id="Seg_12797" s="T74">np.h:Th</ta>
            <ta e="T79" id="Seg_12798" s="T78">0.3.h:P</ta>
            <ta e="T83" id="Seg_12799" s="T81">0.3.h:P</ta>
            <ta e="T85" id="Seg_12800" s="T84">0.3.h:Poss np.h:Th</ta>
            <ta e="T88" id="Seg_12801" s="T87">n:Time</ta>
            <ta e="T90" id="Seg_12802" s="T89">0.3.h:P</ta>
            <ta e="T91" id="Seg_12803" s="T90">0.3.h:E</ta>
            <ta e="T92" id="Seg_12804" s="T91">0.3.h:Poss np.h:P</ta>
            <ta e="T95" id="Seg_12805" s="T94">np:P</ta>
            <ta e="T99" id="Seg_12806" s="T98">np:Cau</ta>
            <ta e="T101" id="Seg_12807" s="T100">np:P</ta>
            <ta e="T102" id="Seg_12808" s="T101">0.1.h:A</ta>
            <ta e="T104" id="Seg_12809" s="T103">0.1.h:Poss np.h:Th</ta>
            <ta e="T109" id="Seg_12810" s="T108">np.h:E</ta>
            <ta e="T114" id="Seg_12811" s="T113">np:Path</ta>
            <ta e="T116" id="Seg_12812" s="T114">pp:G</ta>
            <ta e="T117" id="Seg_12813" s="T116">0.3.h:A</ta>
            <ta e="T118" id="Seg_12814" s="T117">0.3.h:E</ta>
            <ta e="T119" id="Seg_12815" s="T118">np:So</ta>
            <ta e="T121" id="Seg_12816" s="T120">np:Th</ta>
            <ta e="T124" id="Seg_12817" s="T123">np.h:A</ta>
            <ta e="T125" id="Seg_12818" s="T124">np:L</ta>
            <ta e="T126" id="Seg_12819" s="T125">np:P</ta>
            <ta e="T129" id="Seg_12820" s="T128">np.h:St</ta>
            <ta e="T130" id="Seg_12821" s="T129">0.3.h:E</ta>
            <ta e="T132" id="Seg_12822" s="T131">np.h:A</ta>
            <ta e="T142" id="Seg_12823" s="T141">np.h:E</ta>
            <ta e="T146" id="Seg_12824" s="T145">np.h:E</ta>
            <ta e="T147" id="Seg_12825" s="T146">np.h:St</ta>
            <ta e="T151" id="Seg_12826" s="T150">np.h:A</ta>
            <ta e="T152" id="Seg_12827" s="T151">0.3.h:Poss np:Ins</ta>
            <ta e="T153" id="Seg_12828" s="T152">np.h:Poss</ta>
            <ta e="T155" id="Seg_12829" s="T154">np:P</ta>
            <ta e="T158" id="Seg_12830" s="T157">np:Th</ta>
            <ta e="T161" id="Seg_12831" s="T160">np.h:A</ta>
            <ta e="T164" id="Seg_12832" s="T163">np.h:A</ta>
            <ta e="T168" id="Seg_12833" s="T167">np.h:Poss</ta>
            <ta e="T169" id="Seg_12834" s="T168">np:P</ta>
            <ta e="T172" id="Seg_12835" s="T171">np:Th</ta>
            <ta e="T173" id="Seg_12836" s="T172">0.3.h:A</ta>
            <ta e="T175" id="Seg_12837" s="T174">np:Th</ta>
            <ta e="T176" id="Seg_12838" s="T175">0.3.h:Poss</ta>
            <ta e="T177" id="Seg_12839" s="T176">np.h:A</ta>
            <ta e="T178" id="Seg_12840" s="T177">np:P</ta>
            <ta e="T182" id="Seg_12841" s="T181">np:L</ta>
            <ta e="T186" id="Seg_12842" s="T185">np:Th</ta>
            <ta e="T191" id="Seg_12843" s="T190">np:P</ta>
            <ta e="T193" id="Seg_12844" s="T192">np:Ins</ta>
            <ta e="T194" id="Seg_12845" s="T193">0.3.h:A</ta>
            <ta e="T195" id="Seg_12846" s="T194">np:P</ta>
            <ta e="T201" id="Seg_12847" s="T200">0.3.h:A</ta>
            <ta e="T202" id="Seg_12848" s="T201">np:Th</ta>
            <ta e="T206" id="Seg_12849" s="T205">np.h:Poss</ta>
            <ta e="T207" id="Seg_12850" s="T206">np:St</ta>
            <ta e="T208" id="Seg_12851" s="T207">0.3.h:E</ta>
            <ta e="T214" id="Seg_12852" s="T213">0.3.h:Th</ta>
            <ta e="T219" id="Seg_12853" s="T218">np.h:A</ta>
            <ta e="T222" id="Seg_12854" s="T221">0.3.h:E</ta>
            <ta e="T224" id="Seg_12855" s="T222">0.3.h:A</ta>
            <ta e="T225" id="Seg_12856" s="T224">0.3.h:A</ta>
            <ta e="T226" id="Seg_12857" s="T225">pro:P</ta>
            <ta e="T228" id="Seg_12858" s="T226">0.2.h:A</ta>
            <ta e="T230" id="Seg_12859" s="T229">np.h:A</ta>
            <ta e="T231" id="Seg_12860" s="T230">np:Th</ta>
            <ta e="T232" id="Seg_12861" s="T231">np:Th</ta>
            <ta e="T233" id="Seg_12862" s="T232">0.1.h:A</ta>
            <ta e="T235" id="Seg_12863" s="T234">0.2.h:Th</ta>
            <ta e="T238" id="Seg_12864" s="T237">0.2.h:Th</ta>
            <ta e="T241" id="Seg_12865" s="T238">0.1.h:Poss pp:Com</ta>
            <ta e="T242" id="Seg_12866" s="T241">0.1.h:Th</ta>
            <ta e="T244" id="Seg_12867" s="T243">np.h:A</ta>
            <ta e="T245" id="Seg_12868" s="T244">0.2.h:A</ta>
            <ta e="T246" id="Seg_12869" s="T245">0.1.h:A</ta>
            <ta e="T248" id="Seg_12870" s="T247">0.2.h:Th</ta>
            <ta e="T250" id="Seg_12871" s="T249">0.3.h:Poss np:G</ta>
            <ta e="T251" id="Seg_12872" s="T250">0.3.h:A</ta>
            <ta e="T252" id="Seg_12873" s="T251">np:Com</ta>
            <ta e="T253" id="Seg_12874" s="T252">np.h:A</ta>
            <ta e="T258" id="Seg_12875" s="T257">np:So</ta>
            <ta e="T259" id="Seg_12876" s="T258">0.2.h:A</ta>
            <ta e="T260" id="Seg_12877" s="T259">0.3.h:A</ta>
            <ta e="T261" id="Seg_12878" s="T260">adv:G</ta>
            <ta e="T263" id="Seg_12879" s="T262">np.h:A</ta>
            <ta e="T265" id="Seg_12880" s="T264">np.h:A</ta>
            <ta e="T267" id="Seg_12881" s="T266">np.h:A</ta>
            <ta e="T269" id="Seg_12882" s="T268">0.1.h:Poss np.h:P</ta>
            <ta e="T272" id="Seg_12883" s="T271">0.1.h:Poss np:P</ta>
            <ta e="T277" id="Seg_12884" s="T276">np:L</ta>
            <ta e="T278" id="Seg_12885" s="T277">0.3:Th</ta>
            <ta e="T282" id="Seg_12886" s="T281">0.1.h:Th</ta>
            <ta e="T286" id="Seg_12887" s="T285">0.2.h:Th</ta>
            <ta e="T288" id="Seg_12888" s="T287">np.h:A</ta>
            <ta e="T289" id="Seg_12889" s="T288">adv:Time</ta>
            <ta e="T290" id="Seg_12890" s="T289">0.1.h:A</ta>
            <ta e="T291" id="Seg_12891" s="T290">np.h:A</ta>
            <ta e="T293" id="Seg_12892" s="T292">np:P</ta>
            <ta e="T297" id="Seg_12893" s="T296">np.h:A</ta>
            <ta e="T298" id="Seg_12894" s="T297">pro:P</ta>
            <ta e="T301" id="Seg_12895" s="T300">0.2.h:Th</ta>
            <ta e="T302" id="Seg_12896" s="T301">0.3.h:A</ta>
            <ta e="T303" id="Seg_12897" s="T302">0.1.h:Th</ta>
            <ta e="T304" id="Seg_12898" s="T303">n:Time</ta>
            <ta e="T306" id="Seg_12899" s="T304">0.1.h:A</ta>
            <ta e="T307" id="Seg_12900" s="T306">adv:Time</ta>
            <ta e="T308" id="Seg_12901" s="T307">0.1.h:A</ta>
            <ta e="T310" id="Seg_12902" s="T309">n:Time</ta>
            <ta e="T311" id="Seg_12903" s="T310">np.h:Th</ta>
            <ta e="T314" id="Seg_12904" s="T313">np.h:A</ta>
            <ta e="T317" id="Seg_12905" s="T316">np:P</ta>
            <ta e="T320" id="Seg_12906" s="T319">np.h:A</ta>
            <ta e="T323" id="Seg_12907" s="T322">np:P</ta>
            <ta e="T326" id="Seg_12908" s="T325">np.h:A</ta>
            <ta e="T328" id="Seg_12909" s="T327">pro:L</ta>
            <ta e="T331" id="Seg_12910" s="T330">np.h:Th</ta>
            <ta e="T334" id="Seg_12911" s="T333">adv:G</ta>
            <ta e="T335" id="Seg_12912" s="T334">0.1.h:A</ta>
            <ta e="T336" id="Seg_12913" s="T335">np.h:A</ta>
            <ta e="T337" id="Seg_12914" s="T336">np:G</ta>
            <ta e="T339" id="Seg_12915" s="T338">np:Th</ta>
            <ta e="T342" id="Seg_12916" s="T341">np:Path</ta>
            <ta e="T344" id="Seg_12917" s="T342">0.3.h:A</ta>
            <ta e="T347" id="Seg_12918" s="T346">0.3.h:A</ta>
            <ta e="T352" id="Seg_12919" s="T351">np:St</ta>
            <ta e="T353" id="Seg_12920" s="T352">0.3.h:E</ta>
            <ta e="T354" id="Seg_12921" s="T353">np:P</ta>
            <ta e="T356" id="Seg_12922" s="T355">0.3.h:E</ta>
            <ta e="T359" id="Seg_12923" s="T358">np.h:Th</ta>
            <ta e="T361" id="Seg_12924" s="T360">np:L</ta>
            <ta e="T362" id="Seg_12925" s="T361">np:Th</ta>
            <ta e="T364" id="Seg_12926" s="T363">0.3.h:Poss</ta>
            <ta e="T365" id="Seg_12927" s="T364">np:L</ta>
            <ta e="T366" id="Seg_12928" s="T365">np:Th</ta>
            <ta e="T368" id="Seg_12929" s="T367">np:Th</ta>
            <ta e="T373" id="Seg_12930" s="T372">np:Th</ta>
            <ta e="T375" id="Seg_12931" s="T374">np:Th</ta>
            <ta e="T378" id="Seg_12932" s="T377">np:L</ta>
            <ta e="T379" id="Seg_12933" s="T378">np.h:A</ta>
            <ta e="T382" id="Seg_12934" s="T381">np:A</ta>
            <ta e="T385" id="Seg_12935" s="T384">np.h:A</ta>
            <ta e="T388" id="Seg_12936" s="T387">0.2.h:A</ta>
            <ta e="T389" id="Seg_12937" s="T388">np:P</ta>
            <ta e="T390" id="Seg_12938" s="T389">0.2.h:A</ta>
            <ta e="T391" id="Seg_12939" s="T390">np:A</ta>
            <ta e="T394" id="Seg_12940" s="T393">adv:So</ta>
            <ta e="T395" id="Seg_12941" s="T394">np.h:E</ta>
            <ta e="T399" id="Seg_12942" s="T398">0.3:Th</ta>
            <ta e="T400" id="Seg_12943" s="T399">np.h:A</ta>
            <ta e="T402" id="Seg_12944" s="T401">np:Th</ta>
            <ta e="T403" id="Seg_12945" s="T402">np:G</ta>
            <ta e="T405" id="Seg_12946" s="T404">np:A</ta>
            <ta e="T408" id="Seg_12947" s="T407">0.2:A</ta>
            <ta e="T410" id="Seg_12948" s="T409">np:Poss</ta>
            <ta e="T412" id="Seg_12949" s="T411">np:Th</ta>
            <ta e="T415" id="Seg_12950" s="T414">np:A</ta>
            <ta e="T419" id="Seg_12951" s="T418">0.2:A</ta>
            <ta e="T420" id="Seg_12952" s="T419">0.2:A</ta>
            <ta e="T421" id="Seg_12953" s="T420">np.h:A</ta>
            <ta e="T423" id="Seg_12954" s="T422">np:P</ta>
            <ta e="T424" id="Seg_12955" s="T423">0.2:A</ta>
            <ta e="T426" id="Seg_12956" s="T425">np:Th</ta>
            <ta e="T429" id="Seg_12957" s="T428">np.h:A</ta>
            <ta e="T431" id="Seg_12958" s="T430">np.h:R</ta>
            <ta e="T433" id="Seg_12959" s="T432">0.2.h:A</ta>
            <ta e="T435" id="Seg_12960" s="T434">0.2.h:A</ta>
            <ta e="T437" id="Seg_12961" s="T436">np.h:A</ta>
            <ta e="T439" id="Seg_12962" s="T438">np:Th</ta>
            <ta e="T442" id="Seg_12963" s="T441">np:G</ta>
            <ta e="T445" id="Seg_12964" s="T444">np:Th</ta>
            <ta e="T447" id="Seg_12965" s="T446">np:Th</ta>
            <ta e="T449" id="Seg_12966" s="T448">np.h:Th</ta>
            <ta e="T455" id="Seg_12967" s="T454">0.2.h:A</ta>
            <ta e="T0" id="Seg_12968" s="T456">np:A</ta>
            <ta e="T948" id="Seg_12969" s="T947">np:Th</ta>
            <ta e="T457" id="Seg_12970" s="T948">0.2.h:A</ta>
            <ta e="T458" id="Seg_12971" s="T457">np:Th</ta>
            <ta e="T462" id="Seg_12972" s="T461">np.h:A</ta>
            <ta e="T464" id="Seg_12973" s="T463">np:Th</ta>
            <ta e="T466" id="Seg_12974" s="T465">np:So</ta>
            <ta e="T469" id="Seg_12975" s="T468">np:Th</ta>
            <ta e="T472" id="Seg_12976" s="T471">np:A</ta>
            <ta e="T474" id="Seg_12977" s="T473">np.h:R</ta>
            <ta e="T475" id="Seg_12978" s="T474">adv:G</ta>
            <ta e="T476" id="Seg_12979" s="T475">0.2.h:A</ta>
            <ta e="T477" id="Seg_12980" s="T476">adv:L</ta>
            <ta e="T479" id="Seg_12981" s="T478">np:Th</ta>
            <ta e="T481" id="Seg_12982" s="T480">np.h:E</ta>
            <ta e="T487" id="Seg_12983" s="T486">np:St</ta>
            <ta e="T489" id="Seg_12984" s="T488">pro:L</ta>
            <ta e="T494" id="Seg_12985" s="T493">np:St</ta>
            <ta e="T497" id="Seg_12986" s="T495">pp:L</ta>
            <ta e="T498" id="Seg_12987" s="T497">np:Poss</ta>
            <ta e="T499" id="Seg_12988" s="T498">np:St</ta>
            <ta e="T501" id="Seg_12989" s="T500">0.2.h:A</ta>
            <ta e="T502" id="Seg_12990" s="T501">np:G</ta>
            <ta e="T505" id="Seg_12991" s="T504">0.2.h:A</ta>
            <ta e="T508" id="Seg_12992" s="T507">0.2.h:A</ta>
            <ta e="T509" id="Seg_12993" s="T508">np:P</ta>
            <ta e="T510" id="Seg_12994" s="T509">0.2.h:A</ta>
            <ta e="T511" id="Seg_12995" s="T510">np.h:A</ta>
            <ta e="T512" id="Seg_12996" s="T511">0.3.h:Poss np:Th</ta>
            <ta e="T514" id="Seg_12997" s="T513">np:Th</ta>
            <ta e="T515" id="Seg_12998" s="T514">0.3.h:A</ta>
            <ta e="T517" id="Seg_12999" s="T516">np:G</ta>
            <ta e="T518" id="Seg_13000" s="T517">0.3.h:A</ta>
            <ta e="T520" id="Seg_13001" s="T519">np:Th</ta>
            <ta e="T523" id="Seg_13002" s="T522">0.3.h:A</ta>
            <ta e="T525" id="Seg_13003" s="T524">pro.h:E</ta>
            <ta e="T528" id="Seg_13004" s="T527">np:A</ta>
            <ta e="T531" id="Seg_13005" s="T530">np.h:E</ta>
            <ta e="T533" id="Seg_13006" s="T532">0.3.h:Poss np:Th</ta>
            <ta e="T535" id="Seg_13007" s="T534">np:G</ta>
            <ta e="T536" id="Seg_13008" s="T535">0.3.h:A</ta>
            <ta e="T537" id="Seg_13009" s="T536">adv:L</ta>
            <ta e="T540" id="Seg_13010" s="T539">np:Th</ta>
            <ta e="T543" id="Seg_13011" s="T542">pro:Cau</ta>
            <ta e="T547" id="Seg_13012" s="T546">0.3.h:A</ta>
            <ta e="T548" id="Seg_13013" s="T547">np:L</ta>
            <ta e="T550" id="Seg_13014" s="T549">np.h:Th</ta>
            <ta e="T556" id="Seg_13015" s="T555">0.3.h:A</ta>
            <ta e="T557" id="Seg_13016" s="T556">0.3.h:A</ta>
            <ta e="T559" id="Seg_13017" s="T558">np:A</ta>
            <ta e="T562" id="Seg_13018" s="T561">pro:G</ta>
            <ta e="T563" id="Seg_13019" s="T562">0.3:A</ta>
            <ta e="T564" id="Seg_13020" s="T563">pro:G</ta>
            <ta e="T565" id="Seg_13021" s="T564">0.3:A</ta>
            <ta e="T568" id="Seg_13022" s="T567">np:Th</ta>
            <ta e="T570" id="Seg_13023" s="T568">0.3.h:Poss</ta>
            <ta e="T571" id="Seg_13024" s="T570">0.3.h:Poss np:P</ta>
            <ta e="T575" id="Seg_13025" s="T574">np:Poss</ta>
            <ta e="T576" id="Seg_13026" s="T575">np:St</ta>
            <ta e="T578" id="Seg_13027" s="T577">pro.h:R</ta>
            <ta e="T579" id="Seg_13028" s="T578">0.1:A</ta>
            <ta e="T580" id="Seg_13029" s="T579">0.2.h:A</ta>
            <ta e="T581" id="Seg_13030" s="T580">np.h:A</ta>
            <ta e="T582" id="Seg_13031" s="T581">np:So</ta>
            <ta e="T585" id="Seg_13032" s="T584">np:Th</ta>
            <ta e="T589" id="Seg_13033" s="T588">np:G</ta>
            <ta e="T590" id="Seg_13034" s="T589">adv:G</ta>
            <ta e="T592" id="Seg_13035" s="T591">np:P</ta>
            <ta e="T598" id="Seg_13036" s="T597">np.h:P</ta>
            <ta e="T600" id="Seg_13037" s="T599">0.3.h:E</ta>
            <ta e="T605" id="Seg_13038" s="T604">0.3.h:Th</ta>
            <ta e="T606" id="Seg_13039" s="T605">np.h:E</ta>
            <ta e="T610" id="Seg_13040" s="T609">adv:Time</ta>
            <ta e="T611" id="Seg_13041" s="T610">pro.h:Poss</ta>
            <ta e="T612" id="Seg_13042" s="T611">np:G</ta>
            <ta e="T613" id="Seg_13043" s="T612">0.1.h:A</ta>
            <ta e="T614" id="Seg_13044" s="T613">0.2.h:A</ta>
            <ta e="T616" id="Seg_13045" s="T615">0.1.h:A</ta>
            <ta e="T619" id="Seg_13046" s="T618">0.1.h:A</ta>
            <ta e="T621" id="Seg_13047" s="T620">np.h:A</ta>
            <ta e="T624" id="Seg_13048" s="T623">0.2.h:Poss np:Th</ta>
            <ta e="T626" id="Seg_13049" s="T625">np:L</ta>
            <ta e="T627" id="Seg_13050" s="T626">np.h:E</ta>
            <ta e="T629" id="Seg_13051" s="T628">np:St</ta>
            <ta e="T633" id="Seg_13052" s="T632">np.h:Poss</ta>
            <ta e="T634" id="Seg_13053" s="T633">np:St</ta>
            <ta e="T635" id="Seg_13054" s="T634">0.3.h:E</ta>
            <ta e="T637" id="Seg_13055" s="T635">0.3.h:A</ta>
            <ta e="T638" id="Seg_13056" s="T637">pro.h:E</ta>
            <ta e="T639" id="Seg_13057" s="T638">0.1.h:Poss np.h:Th</ta>
            <ta e="T642" id="Seg_13058" s="T641">adv:Time</ta>
            <ta e="T643" id="Seg_13059" s="T642">0.1.h:A</ta>
            <ta e="T645" id="Seg_13060" s="T644">0.2.h:A</ta>
            <ta e="T646" id="Seg_13061" s="T645">np.h:A</ta>
            <ta e="T649" id="Seg_13062" s="T648">pro.h:E</ta>
            <ta e="T653" id="Seg_13063" s="T652">np:Th</ta>
            <ta e="T657" id="Seg_13064" s="T656">0.3.h:Poss np:E</ta>
            <ta e="T662" id="Seg_13065" s="T661">0.3.h:E</ta>
            <ta e="T664" id="Seg_13066" s="T663">np.h:Poss</ta>
            <ta e="T665" id="Seg_13067" s="T664">np:St</ta>
            <ta e="T667" id="Seg_13068" s="T666">0.3.h:Poss np:Th</ta>
            <ta e="T668" id="Seg_13069" s="T667">0.3.h:A</ta>
            <ta e="T669" id="Seg_13070" s="T668">adv:Time</ta>
            <ta e="T671" id="Seg_13071" s="T670">0.3.h:E</ta>
            <ta e="T673" id="Seg_13072" s="T672">0.3:E</ta>
            <ta e="T675" id="Seg_13073" s="T674">0.3.h:A</ta>
            <ta e="T679" id="Seg_13074" s="T678">np:P</ta>
            <ta e="T684" id="Seg_13075" s="T683">pro:G</ta>
            <ta e="T685" id="Seg_13076" s="T684">0.3:A</ta>
            <ta e="T686" id="Seg_13077" s="T685">pro:G</ta>
            <ta e="T687" id="Seg_13078" s="T686">0.3:A</ta>
            <ta e="T689" id="Seg_13079" s="T688">np:St</ta>
            <ta e="T691" id="Seg_13080" s="T690">pro:A</ta>
            <ta e="T692" id="Seg_13081" s="T691">pro.h:R</ta>
            <ta e="T694" id="Seg_13082" s="T693">0.2.h:A</ta>
            <ta e="T695" id="Seg_13083" s="T694">np:P</ta>
            <ta e="T696" id="Seg_13084" s="T695">np.h:A</ta>
            <ta e="T702" id="Seg_13085" s="T701">np:G</ta>
            <ta e="T703" id="Seg_13086" s="T702">0.3.h:Th</ta>
            <ta e="T708" id="Seg_13087" s="T707">pro.h:Poss</ta>
            <ta e="T709" id="Seg_13088" s="T708">np:G</ta>
            <ta e="T711" id="Seg_13089" s="T709">0.3.h:Th</ta>
            <ta e="T714" id="Seg_13090" s="T713">np:Poss</ta>
            <ta e="T715" id="Seg_13091" s="T714">np:Th</ta>
            <ta e="T718" id="Seg_13092" s="T717">np:So</ta>
            <ta e="T720" id="Seg_13093" s="T719">np:Th</ta>
            <ta e="T722" id="Seg_13094" s="T721">0.3.h:Poss np.h:Poss</ta>
            <ta e="T723" id="Seg_13095" s="T722">np:Th</ta>
            <ta e="T724" id="Seg_13096" s="T723">0.3.h:E</ta>
            <ta e="T727" id="Seg_13097" s="T726">np:Ins</ta>
            <ta e="T728" id="Seg_13098" s="T727">0.3.h:A</ta>
            <ta e="T729" id="Seg_13099" s="T728">np.h:A</ta>
            <ta e="T734" id="Seg_13100" s="T733">np:Th</ta>
            <ta e="T735" id="Seg_13101" s="T734">0.3.h:A</ta>
            <ta e="T736" id="Seg_13102" s="T735">np:G</ta>
            <ta e="T737" id="Seg_13103" s="T736">0.3.h:A</ta>
            <ta e="T740" id="Seg_13104" s="T739">np:P</ta>
            <ta e="T748" id="Seg_13105" s="T746">0.3.h:A</ta>
            <ta e="T750" id="Seg_13106" s="T749">np:L</ta>
            <ta e="T751" id="Seg_13107" s="T750">0.3.h:A</ta>
            <ta e="T752" id="Seg_13108" s="T751">0.3.h:Poss np.h:Th</ta>
            <ta e="T754" id="Seg_13109" s="T753">np.h:E</ta>
            <ta e="T755" id="Seg_13110" s="T754">0.3.h:Poss np.h:St</ta>
            <ta e="T760" id="Seg_13111" s="T759">pro.h:Th</ta>
            <ta e="T762" id="Seg_13112" s="T761">np.h:Th</ta>
            <ta e="T768" id="Seg_13113" s="T767">np:Th</ta>
            <ta e="T772" id="Seg_13114" s="T770">pp:Time</ta>
            <ta e="T773" id="Seg_13115" s="T772">0.1.h:Th</ta>
            <ta e="T775" id="Seg_13116" s="T774">np.h:A</ta>
            <ta e="T776" id="Seg_13117" s="T775">0.3.h:Poss np.h:Th</ta>
            <ta e="T777" id="Seg_13118" s="T776">np:G</ta>
            <ta e="T779" id="Seg_13119" s="T778">np:Th</ta>
            <ta e="T781" id="Seg_13120" s="T780">0.3.h:A</ta>
            <ta e="T786" id="Seg_13121" s="T785">np.h:A</ta>
            <ta e="T789" id="Seg_13122" s="T788">0.2.h:A</ta>
            <ta e="T791" id="Seg_13123" s="T790">0.3.h:Poss np.h:A</ta>
            <ta e="T792" id="Seg_13124" s="T791">adv:Time</ta>
            <ta e="T793" id="Seg_13125" s="T792">0.1.h:A</ta>
            <ta e="T794" id="Seg_13126" s="T793">0.2.h:E</ta>
            <ta e="T795" id="Seg_13127" s="T794">np.h:A</ta>
            <ta e="T797" id="Seg_13128" s="T796">np.h:Th</ta>
            <ta e="T801" id="Seg_13129" s="T800">np:L</ta>
            <ta e="T807" id="Seg_13130" s="T806">np:Th</ta>
            <ta e="T809" id="Seg_13131" s="T808">np:Path</ta>
            <ta e="T811" id="Seg_13132" s="T810">0.3.h:A</ta>
            <ta e="T812" id="Seg_13133" s="T811">0.3.h:E</ta>
            <ta e="T813" id="Seg_13134" s="T812">np:Path</ta>
            <ta e="T815" id="Seg_13135" s="T814">0.3.h:A</ta>
            <ta e="T816" id="Seg_13136" s="T815">0.3.h:E</ta>
            <ta e="T819" id="Seg_13137" s="T818">np:Th</ta>
            <ta e="T825" id="Seg_13138" s="T824">np.h:P</ta>
            <ta e="T826" id="Seg_13139" s="T825">np:G</ta>
            <ta e="T830" id="Seg_13140" s="T829">0.3.h:Th</ta>
            <ta e="T832" id="Seg_13141" s="T831">np:Path</ta>
            <ta e="T835" id="Seg_13142" s="T834">np:G</ta>
            <ta e="T836" id="Seg_13143" s="T835">0.3.h:P</ta>
            <ta e="T838" id="Seg_13144" s="T837">np:L</ta>
            <ta e="T839" id="Seg_13145" s="T838">np:Th</ta>
            <ta e="T844" id="Seg_13146" s="T843">np.h:A</ta>
            <ta e="T850" id="Seg_13147" s="T849">np.h:P</ta>
            <ta e="T854" id="Seg_13148" s="T853">0.3.h:A</ta>
            <ta e="T855" id="Seg_13149" s="T854">np:G</ta>
            <ta e="T856" id="Seg_13150" s="T855">0.3.h:A</ta>
            <ta e="T857" id="Seg_13151" s="T856">np:A</ta>
            <ta e="T861" id="Seg_13152" s="T858">pp:G</ta>
            <ta e="T863" id="Seg_13153" s="T862">np.h:A</ta>
            <ta e="T864" id="Seg_13154" s="T863">np:Th</ta>
            <ta e="T867" id="Seg_13155" s="T866">np:A</ta>
            <ta e="T870" id="Seg_13156" s="T869">np:G</ta>
            <ta e="T873" id="Seg_13157" s="T872">np.h:Th</ta>
            <ta e="T877" id="Seg_13158" s="T876">np:Th</ta>
            <ta e="T880" id="Seg_13159" s="T879">np:Com</ta>
            <ta e="T881" id="Seg_13160" s="T880">np.h:E</ta>
            <ta e="T883" id="Seg_13161" s="T882">np.h:A</ta>
            <ta e="T885" id="Seg_13162" s="T884">0.2.h:Poss np.h:Th</ta>
            <ta e="T886" id="Seg_13163" s="T885">0.2.h:E</ta>
            <ta e="T888" id="Seg_13164" s="T887">0.2.h:Th</ta>
            <ta e="T889" id="Seg_13165" s="T888">0.2.h:A</ta>
            <ta e="T891" id="Seg_13166" s="T889">pp:Com</ta>
            <ta e="T893" id="Seg_13167" s="T892">np.h:A</ta>
            <ta e="T894" id="Seg_13168" s="T893">0.1.h:Poss np.h:A</ta>
            <ta e="T895" id="Seg_13169" s="T894">pro.h:Th</ta>
            <ta e="T897" id="Seg_13170" s="T896">0.2.h:A</ta>
            <ta e="T899" id="Seg_13171" s="T898">0.1.h:Th</ta>
            <ta e="T900" id="Seg_13172" s="T899">0.1.h:Poss np:Th</ta>
            <ta e="T901" id="Seg_13173" s="T900">0.1.h:A</ta>
            <ta e="T903" id="Seg_13174" s="T901">pp:Com</ta>
            <ta e="T904" id="Seg_13175" s="T903">np.h:A</ta>
            <ta e="T905" id="Seg_13176" s="T904">np:Th</ta>
            <ta e="T907" id="Seg_13177" s="T906">np:So</ta>
            <ta e="T909" id="Seg_13178" s="T908">pro:Th</ta>
            <ta e="T913" id="Seg_13179" s="T912">np.h:Poss</ta>
            <ta e="T914" id="Seg_13180" s="T913">np.h:A</ta>
            <ta e="T917" id="Seg_13181" s="T916">np.h:Th</ta>
            <ta e="T922" id="Seg_13182" s="T921">0.3.h:E</ta>
            <ta e="T923" id="Seg_13183" s="T922">0.3.h:Poss np.h:A</ta>
            <ta e="T925" id="Seg_13184" s="T923">pp:Com</ta>
            <ta e="T929" id="Seg_13185" s="T928">np:P</ta>
            <ta e="T930" id="Seg_13186" s="T929">0.3.h:A</ta>
            <ta e="T932" id="Seg_13187" s="T931">np:P</ta>
            <ta e="T933" id="Seg_13188" s="T932">0.3.h:A</ta>
            <ta e="T934" id="Seg_13189" s="T933">np.h:A</ta>
            <ta e="T935" id="Seg_13190" s="T934">np:G</ta>
            <ta e="T938" id="Seg_13191" s="T937">np:P</ta>
            <ta e="T939" id="Seg_13192" s="T938">0.3.h:A</ta>
            <ta e="T940" id="Seg_13193" s="T939">np.h:A</ta>
            <ta e="T941" id="Seg_13194" s="T940">np.h:P</ta>
            <ta e="T946" id="Seg_13195" s="T945">0.3.h:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_13196" s="T1">0.2.h:S v:pred</ta>
            <ta e="T4" id="Seg_13197" s="T3">np:O</ta>
            <ta e="T9" id="Seg_13198" s="T8">np.h:S</ta>
            <ta e="T13" id="Seg_13199" s="T12">v:pred</ta>
            <ta e="T14" id="Seg_13200" s="T13">np.h:S</ta>
            <ta e="T16" id="Seg_13201" s="T15">n:pred</ta>
            <ta e="T18" id="Seg_13202" s="T17">np.h:S</ta>
            <ta e="T22" id="Seg_13203" s="T21">adj:pred</ta>
            <ta e="T24" id="Seg_13204" s="T23">np:O</ta>
            <ta e="T25" id="Seg_13205" s="T24">pro.h:S</ta>
            <ta e="T26" id="Seg_13206" s="T25">v:pred</ta>
            <ta e="T27" id="Seg_13207" s="T26">np:O</ta>
            <ta e="T30" id="Seg_13208" s="T29">0.3.h:S v:pred</ta>
            <ta e="T32" id="Seg_13209" s="T31">np.h:S</ta>
            <ta e="T34" id="Seg_13210" s="T33">v:pred</ta>
            <ta e="T37" id="Seg_13211" s="T34">s:temp</ta>
            <ta e="T38" id="Seg_13212" s="T37">np:S</ta>
            <ta e="T39" id="Seg_13213" s="T38">v:pred</ta>
            <ta e="T40" id="Seg_13214" s="T39">np:S</ta>
            <ta e="T41" id="Seg_13215" s="T40">v:pred</ta>
            <ta e="T42" id="Seg_13216" s="T41">np.h:S</ta>
            <ta e="T43" id="Seg_13217" s="T42">np:O</ta>
            <ta e="T44" id="Seg_13218" s="T43">v:pred</ta>
            <ta e="T45" id="Seg_13219" s="T44">np:O</ta>
            <ta e="T46" id="Seg_13220" s="T45">0.3.h:S v:pred</ta>
            <ta e="T47" id="Seg_13221" s="T46">np:O</ta>
            <ta e="T48" id="Seg_13222" s="T47">0.3.h:S v:pred</ta>
            <ta e="T49" id="Seg_13223" s="T48">0.3.h:S v:pred</ta>
            <ta e="T51" id="Seg_13224" s="T49">s:comp</ta>
            <ta e="T52" id="Seg_13225" s="T51">0.3.h:S v:pred</ta>
            <ta e="T54" id="Seg_13226" s="T53">0.3.h:S v:pred</ta>
            <ta e="T58" id="Seg_13227" s="T54">s:temp</ta>
            <ta e="T61" id="Seg_13228" s="T60">np:O</ta>
            <ta e="T63" id="Seg_13229" s="T62">0.3.h:S v:pred</ta>
            <ta e="T64" id="Seg_13230" s="T63">s:adv</ta>
            <ta e="T65" id="Seg_13231" s="T64">0.3.h:S v:pred</ta>
            <ta e="T66" id="Seg_13232" s="T65">0.3.h:S v:pred</ta>
            <ta e="T68" id="Seg_13233" s="T66">s:temp</ta>
            <ta e="T70" id="Seg_13234" s="T69">0.3.h:S v:pred</ta>
            <ta e="T71" id="Seg_13235" s="T70">s:adv</ta>
            <ta e="T72" id="Seg_13236" s="T71">s:adv</ta>
            <ta e="T73" id="Seg_13237" s="T72">s:adv</ta>
            <ta e="T75" id="Seg_13238" s="T74">np.h:S</ta>
            <ta e="T77" id="Seg_13239" s="T76">v:pred</ta>
            <ta e="T78" id="Seg_13240" s="T77">s:temp</ta>
            <ta e="T79" id="Seg_13241" s="T78">0.3.h:S v:pred</ta>
            <ta e="T81" id="Seg_13242" s="T79">s:temp</ta>
            <ta e="T83" id="Seg_13243" s="T81">0.3.h:S v:pred</ta>
            <ta e="T85" id="Seg_13244" s="T84">np.h:S</ta>
            <ta e="T88" id="Seg_13245" s="T87">np:O</ta>
            <ta e="T89" id="Seg_13246" s="T88">v:pred</ta>
            <ta e="T90" id="Seg_13247" s="T89">0.3.h:S v:pred</ta>
            <ta e="T91" id="Seg_13248" s="T90">0.3.h:S v:pred</ta>
            <ta e="T92" id="Seg_13249" s="T91">np.h:S</ta>
            <ta e="T94" id="Seg_13250" s="T92">v:pred</ta>
            <ta e="T95" id="Seg_13251" s="T94">np:S</ta>
            <ta e="T98" id="Seg_13252" s="T97">v:pred</ta>
            <ta e="T99" id="Seg_13253" s="T98">np:S</ta>
            <ta e="T100" id="Seg_13254" s="T99">v:pred</ta>
            <ta e="T102" id="Seg_13255" s="T100">s:cond</ta>
            <ta e="T104" id="Seg_13256" s="T103">np.h:S</ta>
            <ta e="T105" id="Seg_13257" s="T104">v:pred</ta>
            <ta e="T108" id="Seg_13258" s="T107">v:pred</ta>
            <ta e="T109" id="Seg_13259" s="T108">np.h:S</ta>
            <ta e="T117" id="Seg_13260" s="T116">0.3.h:S v:pred</ta>
            <ta e="T118" id="Seg_13261" s="T117">0.3.h:S v:pred</ta>
            <ta e="T121" id="Seg_13262" s="T120">np:S</ta>
            <ta e="T123" id="Seg_13263" s="T121">v:pred</ta>
            <ta e="T124" id="Seg_13264" s="T123">np.h:S</ta>
            <ta e="T127" id="Seg_13265" s="T124">s:adv</ta>
            <ta e="T128" id="Seg_13266" s="T127">v:pred</ta>
            <ta e="T129" id="Seg_13267" s="T128">np.h:O</ta>
            <ta e="T130" id="Seg_13268" s="T129">0.3.h:S v:pred</ta>
            <ta e="T132" id="Seg_13269" s="T131">np.h:S</ta>
            <ta e="T137" id="Seg_13270" s="T135">v:pred</ta>
            <ta e="T141" id="Seg_13271" s="T140">v:pred</ta>
            <ta e="T142" id="Seg_13272" s="T141">np.h:S</ta>
            <ta e="T146" id="Seg_13273" s="T145">np.h:S</ta>
            <ta e="T148" id="Seg_13274" s="T146">s:temp</ta>
            <ta e="T149" id="Seg_13275" s="T148">s:purp</ta>
            <ta e="T150" id="Seg_13276" s="T149">v:pred</ta>
            <ta e="T151" id="Seg_13277" s="T150">np.h:S</ta>
            <ta e="T155" id="Seg_13278" s="T154">np:O</ta>
            <ta e="T157" id="Seg_13279" s="T156">v:pred</ta>
            <ta e="T158" id="Seg_13280" s="T157">np:S</ta>
            <ta e="T160" id="Seg_13281" s="T158">v:pred</ta>
            <ta e="T161" id="Seg_13282" s="T160">np.h:S</ta>
            <ta e="T163" id="Seg_13283" s="T162">v:pred</ta>
            <ta e="T164" id="Seg_13284" s="T163">np.h:S</ta>
            <ta e="T167" id="Seg_13285" s="T165">v:pred</ta>
            <ta e="T171" id="Seg_13286" s="T167">s:temp</ta>
            <ta e="T172" id="Seg_13287" s="T171">np:O</ta>
            <ta e="T173" id="Seg_13288" s="T172">0.3.h:S v:pred</ta>
            <ta e="T175" id="Seg_13289" s="T174">adj:pred</ta>
            <ta e="T176" id="Seg_13290" s="T175">0.3.h:S cop</ta>
            <ta e="T177" id="Seg_13291" s="T176">np.h:S</ta>
            <ta e="T178" id="Seg_13292" s="T177">np:O</ta>
            <ta e="T179" id="Seg_13293" s="T178">s:adv</ta>
            <ta e="T181" id="Seg_13294" s="T179">v:pred</ta>
            <ta e="T186" id="Seg_13295" s="T185">np:S</ta>
            <ta e="T187" id="Seg_13296" s="T186">v:pred</ta>
            <ta e="T191" id="Seg_13297" s="T190">np:O</ta>
            <ta e="T194" id="Seg_13298" s="T193">0.3.h:S v:pred</ta>
            <ta e="T195" id="Seg_13299" s="T194">np:S</ta>
            <ta e="T199" id="Seg_13300" s="T197">v:pred</ta>
            <ta e="T201" id="Seg_13301" s="T200">0.3.h:S v:pred</ta>
            <ta e="T202" id="Seg_13302" s="T201">np:S</ta>
            <ta e="T203" id="Seg_13303" s="T202">v:pred</ta>
            <ta e="T205" id="Seg_13304" s="T203">s:temp</ta>
            <ta e="T207" id="Seg_13305" s="T206">np:O</ta>
            <ta e="T208" id="Seg_13306" s="T207">0.3.h:S v:pred</ta>
            <ta e="T212" id="Seg_13307" s="T208">s:rel</ta>
            <ta e="T213" id="Seg_13308" s="T212">n:pred</ta>
            <ta e="T214" id="Seg_13309" s="T213">0.3.h:S cop</ta>
            <ta e="T218" id="Seg_13310" s="T215">s:adv</ta>
            <ta e="T221" id="Seg_13311" s="T218">s:comp</ta>
            <ta e="T222" id="Seg_13312" s="T221">0.3.h:S v:pred</ta>
            <ta e="T224" id="Seg_13313" s="T222">0.3.h:S v:pred</ta>
            <ta e="T225" id="Seg_13314" s="T224">0.3.h:S v:pred</ta>
            <ta e="T226" id="Seg_13315" s="T225">pro:O</ta>
            <ta e="T228" id="Seg_13316" s="T226">0.2.h:S v:pred</ta>
            <ta e="T229" id="Seg_13317" s="T228">v:pred</ta>
            <ta e="T230" id="Seg_13318" s="T229">np.h:S</ta>
            <ta e="T231" id="Seg_13319" s="T230">np:O</ta>
            <ta e="T232" id="Seg_13320" s="T231">np:O</ta>
            <ta e="T233" id="Seg_13321" s="T232">0.1.h:S v:pred</ta>
            <ta e="T235" id="Seg_13322" s="T234">0.2.h:S adj:pred</ta>
            <ta e="T238" id="Seg_13323" s="T237">0.2.h:S n:pred</ta>
            <ta e="T242" id="Seg_13324" s="T241">0.1.h:S v:pred</ta>
            <ta e="T243" id="Seg_13325" s="T242">v:pred</ta>
            <ta e="T244" id="Seg_13326" s="T243">np.h:S</ta>
            <ta e="T245" id="Seg_13327" s="T244">0.2.h:S v:pred</ta>
            <ta e="T246" id="Seg_13328" s="T245">0.1.h:S v:pred</ta>
            <ta e="T247" id="Seg_13329" s="T246">n:pred</ta>
            <ta e="T248" id="Seg_13330" s="T247">0.2.h:S cop</ta>
            <ta e="T251" id="Seg_13331" s="T250">0.3.h:S v:pred</ta>
            <ta e="T253" id="Seg_13332" s="T252">np.h:S</ta>
            <ta e="T255" id="Seg_13333" s="T253">s:temp</ta>
            <ta e="T256" id="Seg_13334" s="T255">v:pred</ta>
            <ta e="T259" id="Seg_13335" s="T258">0.2.h:S v:pred</ta>
            <ta e="T260" id="Seg_13336" s="T259">0.3.h:S v:pred</ta>
            <ta e="T263" id="Seg_13337" s="T262">np.h:S</ta>
            <ta e="T265" id="Seg_13338" s="T264">np.h:S</ta>
            <ta e="T266" id="Seg_13339" s="T265">v:pred</ta>
            <ta e="T267" id="Seg_13340" s="T266">np.h:S</ta>
            <ta e="T268" id="Seg_13341" s="T267">v:pred</ta>
            <ta e="T269" id="Seg_13342" s="T268">np.h:S</ta>
            <ta e="T271" id="Seg_13343" s="T270">v:pred</ta>
            <ta e="T272" id="Seg_13344" s="T271">np:S</ta>
            <ta e="T275" id="Seg_13345" s="T274">v:pred</ta>
            <ta e="T278" id="Seg_13346" s="T277">0.3:S v:pred</ta>
            <ta e="T281" id="Seg_13347" s="T280">n:pred</ta>
            <ta e="T282" id="Seg_13348" s="T281">0.1.h:S cop</ta>
            <ta e="T285" id="Seg_13349" s="T284">n:pred</ta>
            <ta e="T286" id="Seg_13350" s="T285">0.2.h:S cop</ta>
            <ta e="T287" id="Seg_13351" s="T286">v:pred</ta>
            <ta e="T288" id="Seg_13352" s="T287">np.h:S</ta>
            <ta e="T290" id="Seg_13353" s="T289">0.1.h:S v:pred</ta>
            <ta e="T291" id="Seg_13354" s="T290">np.h:S</ta>
            <ta e="T292" id="Seg_13355" s="T291">s:temp</ta>
            <ta e="T293" id="Seg_13356" s="T292">np:O</ta>
            <ta e="T294" id="Seg_13357" s="T293">v:pred</ta>
            <ta e="T297" id="Seg_13358" s="T296">np.h:S</ta>
            <ta e="T298" id="Seg_13359" s="T297">pro:O</ta>
            <ta e="T300" id="Seg_13360" s="T299">v:pred</ta>
            <ta e="T301" id="Seg_13361" s="T300">0.2.h:S v:pred</ta>
            <ta e="T302" id="Seg_13362" s="T301">0.3.h:S v:pred</ta>
            <ta e="T303" id="Seg_13363" s="T302">0.1.h:S v:pred</ta>
            <ta e="T306" id="Seg_13364" s="T304">0.1.h:S v:pred</ta>
            <ta e="T308" id="Seg_13365" s="T307">0.1.h:S v:pred</ta>
            <ta e="T310" id="Seg_13366" s="T309">np:O</ta>
            <ta e="T311" id="Seg_13367" s="T310">np.h:S</ta>
            <ta e="T312" id="Seg_13368" s="T311">v:pred</ta>
            <ta e="T313" id="Seg_13369" s="T312">s:temp</ta>
            <ta e="T314" id="Seg_13370" s="T313">np.h:S</ta>
            <ta e="T317" id="Seg_13371" s="T316">np:O</ta>
            <ta e="T319" id="Seg_13372" s="T318">v:pred</ta>
            <ta e="T320" id="Seg_13373" s="T319">np.h:S</ta>
            <ta e="T323" id="Seg_13374" s="T322">np:O</ta>
            <ta e="T324" id="Seg_13375" s="T323">v:pred</ta>
            <ta e="T325" id="Seg_13376" s="T324">s:purp</ta>
            <ta e="T326" id="Seg_13377" s="T325">np.h:S</ta>
            <ta e="T327" id="Seg_13378" s="T326">v:pred</ta>
            <ta e="T331" id="Seg_13379" s="T330">np.h:S</ta>
            <ta e="T332" id="Seg_13380" s="T331">ptcl:pred</ta>
            <ta e="T335" id="Seg_13381" s="T334">0.1.h:S v:pred</ta>
            <ta e="T336" id="Seg_13382" s="T335">np.h:S</ta>
            <ta e="T339" id="Seg_13383" s="T338">np:O</ta>
            <ta e="T340" id="Seg_13384" s="T339">v:pred</ta>
            <ta e="T344" id="Seg_13385" s="T342">0.3.h:S v:pred</ta>
            <ta e="T347" id="Seg_13386" s="T346">0.3.h:S v:pred</ta>
            <ta e="T352" id="Seg_13387" s="T351">np:O</ta>
            <ta e="T353" id="Seg_13388" s="T352">0.3.h:S v:pred</ta>
            <ta e="T355" id="Seg_13389" s="T353">s:temp</ta>
            <ta e="T356" id="Seg_13390" s="T355">0.3.h:S v:pred</ta>
            <ta e="T359" id="Seg_13391" s="T358">np.h:S</ta>
            <ta e="T360" id="Seg_13392" s="T359">v:pred</ta>
            <ta e="T362" id="Seg_13393" s="T361">np:S</ta>
            <ta e="T363" id="Seg_13394" s="T362">v:pred</ta>
            <ta e="T366" id="Seg_13395" s="T365">np:S</ta>
            <ta e="T367" id="Seg_13396" s="T366">v:pred</ta>
            <ta e="T368" id="Seg_13397" s="T367">np:S</ta>
            <ta e="T370" id="Seg_13398" s="T369">n:pred</ta>
            <ta e="T373" id="Seg_13399" s="T372">np:S</ta>
            <ta e="T374" id="Seg_13400" s="T373">adj:pred</ta>
            <ta e="T375" id="Seg_13401" s="T374">np:S</ta>
            <ta e="T377" id="Seg_13402" s="T375">v:pred</ta>
            <ta e="T379" id="Seg_13403" s="T378">np.h:S</ta>
            <ta e="T381" id="Seg_13404" s="T379">v:pred</ta>
            <ta e="T382" id="Seg_13405" s="T381">np:S</ta>
            <ta e="T383" id="Seg_13406" s="T382">adj:pred</ta>
            <ta e="T384" id="Seg_13407" s="T383">cop</ta>
            <ta e="T385" id="Seg_13408" s="T384">np.h:S</ta>
            <ta e="T386" id="Seg_13409" s="T385">v:pred</ta>
            <ta e="T388" id="Seg_13410" s="T387">0.2.h:S v:pred</ta>
            <ta e="T389" id="Seg_13411" s="T388">np:O</ta>
            <ta e="T390" id="Seg_13412" s="T389">0.2.h:S v:pred</ta>
            <ta e="T391" id="Seg_13413" s="T390">np:S</ta>
            <ta e="T393" id="Seg_13414" s="T392">v:pred</ta>
            <ta e="T395" id="Seg_13415" s="T394">np.h:S</ta>
            <ta e="T396" id="Seg_13416" s="T395">v:pred</ta>
            <ta e="T398" id="Seg_13417" s="T397">n:pred</ta>
            <ta e="T399" id="Seg_13418" s="T398">0.3:S cop</ta>
            <ta e="T400" id="Seg_13419" s="T399">np.h:S</ta>
            <ta e="T401" id="Seg_13420" s="T400">s:temp</ta>
            <ta e="T402" id="Seg_13421" s="T401">np:O</ta>
            <ta e="T404" id="Seg_13422" s="T403">v:pred</ta>
            <ta e="T405" id="Seg_13423" s="T404">np:S</ta>
            <ta e="T406" id="Seg_13424" s="T405">v:pred</ta>
            <ta e="T408" id="Seg_13425" s="T407">0.2:S v:pred</ta>
            <ta e="T412" id="Seg_13426" s="T411">np:S</ta>
            <ta e="T414" id="Seg_13427" s="T413">v:pred</ta>
            <ta e="T415" id="Seg_13428" s="T414">np:S</ta>
            <ta e="T417" id="Seg_13429" s="T416">v:pred</ta>
            <ta e="T419" id="Seg_13430" s="T418">0.2:S v:pred</ta>
            <ta e="T420" id="Seg_13431" s="T419">0.2:S v:pred</ta>
            <ta e="T421" id="Seg_13432" s="T420">np.h:S</ta>
            <ta e="T422" id="Seg_13433" s="T421">v:pred</ta>
            <ta e="T423" id="Seg_13434" s="T422">np:O</ta>
            <ta e="T424" id="Seg_13435" s="T423">0.2:S v:pred</ta>
            <ta e="T426" id="Seg_13436" s="T425">np:S</ta>
            <ta e="T428" id="Seg_13437" s="T427">v:pred</ta>
            <ta e="T429" id="Seg_13438" s="T428">np.h:S</ta>
            <ta e="T431" id="Seg_13439" s="T430">np.h:O</ta>
            <ta e="T432" id="Seg_13440" s="T431">v:pred</ta>
            <ta e="T433" id="Seg_13441" s="T432">0.2.h:S v:pred</ta>
            <ta e="T435" id="Seg_13442" s="T434">0.2.h:S v:pred</ta>
            <ta e="T437" id="Seg_13443" s="T436">np.h:S</ta>
            <ta e="T440" id="Seg_13444" s="T437">s:temp</ta>
            <ta e="T443" id="Seg_13445" s="T442">v:pred</ta>
            <ta e="T445" id="Seg_13446" s="T444">np:S</ta>
            <ta e="T446" id="Seg_13447" s="T445">v:pred</ta>
            <ta e="T447" id="Seg_13448" s="T446">np:S</ta>
            <ta e="T448" id="Seg_13449" s="T447">v:pred</ta>
            <ta e="T449" id="Seg_13450" s="T448">np.h:S</ta>
            <ta e="T452" id="Seg_13451" s="T451">n:pred</ta>
            <ta e="T453" id="Seg_13452" s="T452">cop</ta>
            <ta e="T455" id="Seg_13453" s="T454">0.2.h:S v:pred</ta>
            <ta e="T456" id="Seg_13454" s="T455">v:pred</ta>
            <ta e="T0" id="Seg_13455" s="T456">np:S</ta>
            <ta e="T948" id="Seg_13456" s="T947">np:O</ta>
            <ta e="T457" id="Seg_13457" s="T948">0.2.h:S v:pred</ta>
            <ta e="T458" id="Seg_13458" s="T457">np:S</ta>
            <ta e="T459" id="Seg_13459" s="T458">cop</ta>
            <ta e="T461" id="Seg_13460" s="T460">n:pred</ta>
            <ta e="T462" id="Seg_13461" s="T461">np.h:S</ta>
            <ta e="T464" id="Seg_13462" s="T463">np:O</ta>
            <ta e="T468" id="Seg_13463" s="T466">v:pred</ta>
            <ta e="T469" id="Seg_13464" s="T468">np:S</ta>
            <ta e="T471" id="Seg_13465" s="T470">v:pred</ta>
            <ta e="T472" id="Seg_13466" s="T471">np:S</ta>
            <ta e="T473" id="Seg_13467" s="T472">v:pred</ta>
            <ta e="T476" id="Seg_13468" s="T475">0.2.h:S v:pred</ta>
            <ta e="T479" id="Seg_13469" s="T478">np:S</ta>
            <ta e="T480" id="Seg_13470" s="T479">v:pred</ta>
            <ta e="T481" id="Seg_13471" s="T480">np.h:S</ta>
            <ta e="T483" id="Seg_13472" s="T481">s:temp</ta>
            <ta e="T487" id="Seg_13473" s="T486">np:O</ta>
            <ta e="T488" id="Seg_13474" s="T487">v:pred</ta>
            <ta e="T494" id="Seg_13475" s="T493">np:S</ta>
            <ta e="T495" id="Seg_13476" s="T494">v:pred</ta>
            <ta e="T499" id="Seg_13477" s="T498">np:S</ta>
            <ta e="T500" id="Seg_13478" s="T499">v:pred</ta>
            <ta e="T501" id="Seg_13479" s="T500">0.2.h:S v:pred</ta>
            <ta e="T505" id="Seg_13480" s="T504">0.2.h:S v:pred</ta>
            <ta e="T508" id="Seg_13481" s="T507">0.2.h:S v:pred</ta>
            <ta e="T509" id="Seg_13482" s="T508">np:O</ta>
            <ta e="T510" id="Seg_13483" s="T509">0.2.h:S v:pred</ta>
            <ta e="T511" id="Seg_13484" s="T510">np.h:S</ta>
            <ta e="T512" id="Seg_13485" s="T511">np:O</ta>
            <ta e="T513" id="Seg_13486" s="T512">v:pred</ta>
            <ta e="T514" id="Seg_13487" s="T513">np:O</ta>
            <ta e="T515" id="Seg_13488" s="T514">0.3.h:S v:pred</ta>
            <ta e="T518" id="Seg_13489" s="T517">0.3.h:S v:pred</ta>
            <ta e="T520" id="Seg_13490" s="T519">np:S</ta>
            <ta e="T521" id="Seg_13491" s="T520">v:pred</ta>
            <ta e="T522" id="Seg_13492" s="T521">pro:O</ta>
            <ta e="T523" id="Seg_13493" s="T522">0.3.h:S v:pred</ta>
            <ta e="T525" id="Seg_13494" s="T524">pro.h:S</ta>
            <ta e="T526" id="Seg_13495" s="T525">v:pred</ta>
            <ta e="T528" id="Seg_13496" s="T527">np:S</ta>
            <ta e="T530" id="Seg_13497" s="T529">v:pred</ta>
            <ta e="T531" id="Seg_13498" s="T530">np.h:S</ta>
            <ta e="T532" id="Seg_13499" s="T531">v:pred</ta>
            <ta e="T533" id="Seg_13500" s="T532">np:O</ta>
            <ta e="T536" id="Seg_13501" s="T535">0.3.h:S v:pred</ta>
            <ta e="T540" id="Seg_13502" s="T539">np:S</ta>
            <ta e="T542" id="Seg_13503" s="T540">v:pred</ta>
            <ta e="T543" id="Seg_13504" s="T542">pro:S</ta>
            <ta e="T544" id="Seg_13505" s="T543">v:pred</ta>
            <ta e="T547" id="Seg_13506" s="T546">0.3.h:S v:pred</ta>
            <ta e="T550" id="Seg_13507" s="T549">np.h:S</ta>
            <ta e="T551" id="Seg_13508" s="T550">v:pred</ta>
            <ta e="T554" id="Seg_13509" s="T551">s:adv</ta>
            <ta e="T555" id="Seg_13510" s="T554">s:adv</ta>
            <ta e="T556" id="Seg_13511" s="T555">0.3.h:S v:pred</ta>
            <ta e="T558" id="Seg_13512" s="T556">s:temp</ta>
            <ta e="T561" id="Seg_13513" s="T558">s:adv</ta>
            <ta e="T563" id="Seg_13514" s="T562">0.3:S v:pred</ta>
            <ta e="T565" id="Seg_13515" s="T564">0.3:S v:pred</ta>
            <ta e="T568" id="Seg_13516" s="T567">adj:pred</ta>
            <ta e="T570" id="Seg_13517" s="T568">0.3.h:S cop</ta>
            <ta e="T571" id="Seg_13518" s="T570">np:S</ta>
            <ta e="T574" id="Seg_13519" s="T571">v:pred</ta>
            <ta e="T576" id="Seg_13520" s="T575">np:S</ta>
            <ta e="T577" id="Seg_13521" s="T576">v:pred</ta>
            <ta e="T579" id="Seg_13522" s="T578">0.1:S v:pred</ta>
            <ta e="T580" id="Seg_13523" s="T579">0.2.h:S v:pred</ta>
            <ta e="T581" id="Seg_13524" s="T580">np.h:S</ta>
            <ta e="T586" id="Seg_13525" s="T581">s:temp</ta>
            <ta e="T591" id="Seg_13526" s="T590">v:pred</ta>
            <ta e="T592" id="Seg_13527" s="T591">np:S</ta>
            <ta e="T595" id="Seg_13528" s="T593">v:pred</ta>
            <ta e="T598" id="Seg_13529" s="T597">np.h:S</ta>
            <ta e="T599" id="Seg_13530" s="T598">v:pred</ta>
            <ta e="T600" id="Seg_13531" s="T599">0.3.h:S v:pred</ta>
            <ta e="T604" id="Seg_13532" s="T603">n:pred</ta>
            <ta e="T605" id="Seg_13533" s="T604">0.3.h:S cop</ta>
            <ta e="T606" id="Seg_13534" s="T605">np.h:S</ta>
            <ta e="T607" id="Seg_13535" s="T606">s:adv</ta>
            <ta e="T609" id="Seg_13536" s="T608">ptcl:pred</ta>
            <ta e="T613" id="Seg_13537" s="T612">0.1.h:S v:pred</ta>
            <ta e="T614" id="Seg_13538" s="T613">0.2.h:S v:pred</ta>
            <ta e="T616" id="Seg_13539" s="T615">0.1.h:S v:pred</ta>
            <ta e="T619" id="Seg_13540" s="T618">0.1.h:S v:pred</ta>
            <ta e="T620" id="Seg_13541" s="T619">v:pred</ta>
            <ta e="T621" id="Seg_13542" s="T620">np.h:S</ta>
            <ta e="T622" id="Seg_13543" s="T621">adj:pred</ta>
            <ta e="T624" id="Seg_13544" s="T623">np:S</ta>
            <ta e="T627" id="Seg_13545" s="T626">np.h:S</ta>
            <ta e="T629" id="Seg_13546" s="T628">np:O</ta>
            <ta e="T630" id="Seg_13547" s="T629">v:pred</ta>
            <ta e="T634" id="Seg_13548" s="T633">np:O</ta>
            <ta e="T635" id="Seg_13549" s="T634">0.3.h:S v:pred</ta>
            <ta e="T637" id="Seg_13550" s="T635">0.3.h:S v:pred</ta>
            <ta e="T640" id="Seg_13551" s="T638">s:comp</ta>
            <ta e="T643" id="Seg_13552" s="T642">0.1.h:S v:pred</ta>
            <ta e="T645" id="Seg_13553" s="T644">0.2.h:S v:pred</ta>
            <ta e="T646" id="Seg_13554" s="T645">np.h:S</ta>
            <ta e="T648" id="Seg_13555" s="T647">v:pred</ta>
            <ta e="T649" id="Seg_13556" s="T648">pro.h:S</ta>
            <ta e="T651" id="Seg_13557" s="T650">v:pred</ta>
            <ta e="T653" id="Seg_13558" s="T652">np:S</ta>
            <ta e="T654" id="Seg_13559" s="T653">v:pred</ta>
            <ta e="T655" id="Seg_13560" s="T654">adj:pred</ta>
            <ta e="T657" id="Seg_13561" s="T656">np:S</ta>
            <ta e="T658" id="Seg_13562" s="T657">v:pred</ta>
            <ta e="T662" id="Seg_13563" s="T661">0.3.h:S v:pred</ta>
            <ta e="T666" id="Seg_13564" s="T662">s:adv</ta>
            <ta e="T667" id="Seg_13565" s="T666">np:O</ta>
            <ta e="T668" id="Seg_13566" s="T667">0.3.h:S v:pred</ta>
            <ta e="T671" id="Seg_13567" s="T670">0.3.h:S v:pred</ta>
            <ta e="T673" id="Seg_13568" s="T672">0.3.h:S v:pred</ta>
            <ta e="T676" id="Seg_13569" s="T674">s:temp</ta>
            <ta e="T679" id="Seg_13570" s="T678">np:S</ta>
            <ta e="T680" id="Seg_13571" s="T679">np:O</ta>
            <ta e="T683" id="Seg_13572" s="T681">v:pred</ta>
            <ta e="T685" id="Seg_13573" s="T684">0.3:S v:pred</ta>
            <ta e="T687" id="Seg_13574" s="T686">0.3:S v:pred</ta>
            <ta e="T689" id="Seg_13575" s="T688">np:S</ta>
            <ta e="T690" id="Seg_13576" s="T689">v:pred</ta>
            <ta e="T691" id="Seg_13577" s="T690">pro:S</ta>
            <ta e="T693" id="Seg_13578" s="T692">v:pred</ta>
            <ta e="T694" id="Seg_13579" s="T693">0.2.h:S v:pred</ta>
            <ta e="T695" id="Seg_13580" s="T694">np:O</ta>
            <ta e="T696" id="Seg_13581" s="T695">np.h:S</ta>
            <ta e="T698" id="Seg_13582" s="T697">v:pred</ta>
            <ta e="T703" id="Seg_13583" s="T702">0.3.h:S v:pred</ta>
            <ta e="T706" id="Seg_13584" s="T703">s:rel</ta>
            <ta e="T711" id="Seg_13585" s="T709">0.3.h:S v:pred</ta>
            <ta e="T713" id="Seg_13586" s="T711">s:rel</ta>
            <ta e="T715" id="Seg_13587" s="T714">np:S</ta>
            <ta e="T719" id="Seg_13588" s="T718">v:pred</ta>
            <ta e="T721" id="Seg_13589" s="T719">s:temp</ta>
            <ta e="T723" id="Seg_13590" s="T722">np:O</ta>
            <ta e="T724" id="Seg_13591" s="T723">0.3.h:S v:pred</ta>
            <ta e="T728" id="Seg_13592" s="T727">0.3.h:S v:pred</ta>
            <ta e="T729" id="Seg_13593" s="T728">np.h:S</ta>
            <ta e="T731" id="Seg_13594" s="T729">s:temp</ta>
            <ta e="T732" id="Seg_13595" s="T731">v:pred</ta>
            <ta e="T734" id="Seg_13596" s="T733">np:O</ta>
            <ta e="T735" id="Seg_13597" s="T734">0.3.h:S v:pred</ta>
            <ta e="T737" id="Seg_13598" s="T736">0.3.h:S v:pred</ta>
            <ta e="T739" id="Seg_13599" s="T737">s:rel</ta>
            <ta e="T740" id="Seg_13600" s="T739">np:S</ta>
            <ta e="T743" id="Seg_13601" s="T742">v:pred</ta>
            <ta e="T746" id="Seg_13602" s="T743">s:temp</ta>
            <ta e="T748" id="Seg_13603" s="T746">0.3:S v:pred</ta>
            <ta e="T751" id="Seg_13604" s="T750">0.3.h:S v:pred</ta>
            <ta e="T752" id="Seg_13605" s="T751">np.h:S</ta>
            <ta e="T753" id="Seg_13606" s="T752">v:pred</ta>
            <ta e="T754" id="Seg_13607" s="T753">np.h:S</ta>
            <ta e="T756" id="Seg_13608" s="T754">s:adv</ta>
            <ta e="T758" id="Seg_13609" s="T757">v:pred</ta>
            <ta e="T760" id="Seg_13610" s="T759">pro.h:S</ta>
            <ta e="T761" id="Seg_13611" s="T760">v:pred</ta>
            <ta e="T762" id="Seg_13612" s="T761">np.h:S</ta>
            <ta e="T765" id="Seg_13613" s="T764">v:pred</ta>
            <ta e="T768" id="Seg_13614" s="T767">np:S</ta>
            <ta e="T769" id="Seg_13615" s="T768">v:pred</ta>
            <ta e="T773" id="Seg_13616" s="T772">0.1.h:S v:pred</ta>
            <ta e="T775" id="Seg_13617" s="T774">np.h:S</ta>
            <ta e="T776" id="Seg_13618" s="T775">np.h:O</ta>
            <ta e="T778" id="Seg_13619" s="T777">v:pred</ta>
            <ta e="T779" id="Seg_13620" s="T778">np:O</ta>
            <ta e="T781" id="Seg_13621" s="T780">0.3.h:S v:pred</ta>
            <ta e="T784" id="Seg_13622" s="T781">s:temp</ta>
            <ta e="T786" id="Seg_13623" s="T785">np.h:S</ta>
            <ta e="T787" id="Seg_13624" s="T786">v:pred</ta>
            <ta e="T789" id="Seg_13625" s="T788">0.2.h:S v:pred</ta>
            <ta e="T790" id="Seg_13626" s="T789">v:pred</ta>
            <ta e="T791" id="Seg_13627" s="T790">np.h:S</ta>
            <ta e="T793" id="Seg_13628" s="T792">0.1.h:S v:pred</ta>
            <ta e="T794" id="Seg_13629" s="T793">0.2.h:S v:pred</ta>
            <ta e="T795" id="Seg_13630" s="T794">np.h:S</ta>
            <ta e="T798" id="Seg_13631" s="T795">s:purp</ta>
            <ta e="T799" id="Seg_13632" s="T798">v:pred</ta>
            <ta e="T802" id="Seg_13633" s="T799">s:rel</ta>
            <ta e="T804" id="Seg_13634" s="T802">s:temp</ta>
            <ta e="T807" id="Seg_13635" s="T806">np:S</ta>
            <ta e="T808" id="Seg_13636" s="T807">v:pred</ta>
            <ta e="T811" id="Seg_13637" s="T808">s:comp</ta>
            <ta e="T812" id="Seg_13638" s="T811">0.3.h:S v:pred</ta>
            <ta e="T815" id="Seg_13639" s="T812">s:comp</ta>
            <ta e="T816" id="Seg_13640" s="T815">0.3.h:S v:pred</ta>
            <ta e="T819" id="Seg_13641" s="T818">np:S</ta>
            <ta e="T820" id="Seg_13642" s="T819">n:pred</ta>
            <ta e="T821" id="Seg_13643" s="T820">cop</ta>
            <ta e="T824" id="Seg_13644" s="T821">s:temp</ta>
            <ta e="T825" id="Seg_13645" s="T824">np.h:S</ta>
            <ta e="T827" id="Seg_13646" s="T826">v:pred</ta>
            <ta e="T828" id="Seg_13647" s="T827">s:temp</ta>
            <ta e="T829" id="Seg_13648" s="T828">n:pred</ta>
            <ta e="T830" id="Seg_13649" s="T829">0.3.h:S cop</ta>
            <ta e="T834" id="Seg_13650" s="T830">s:temp</ta>
            <ta e="T836" id="Seg_13651" s="T835">0.3.h:S v:pred</ta>
            <ta e="T839" id="Seg_13652" s="T838">np:S</ta>
            <ta e="T843" id="Seg_13653" s="T840">s:temp</ta>
            <ta e="T844" id="Seg_13654" s="T843">np.h:S</ta>
            <ta e="T845" id="Seg_13655" s="T844">v:pred</ta>
            <ta e="T848" id="Seg_13656" s="T847">s:rel</ta>
            <ta e="T850" id="Seg_13657" s="T849">np.h:O</ta>
            <ta e="T853" id="Seg_13658" s="T850">s:temp</ta>
            <ta e="T854" id="Seg_13659" s="T853">0.3.h:S v:pred</ta>
            <ta e="T856" id="Seg_13660" s="T855">0.3.h:S v:pred</ta>
            <ta e="T857" id="Seg_13661" s="T856">np:S</ta>
            <ta e="T858" id="Seg_13662" s="T857">v:pred</ta>
            <ta e="T862" id="Seg_13663" s="T861">0.3:S v:pred</ta>
            <ta e="T863" id="Seg_13664" s="T862">np.h:S</ta>
            <ta e="T864" id="Seg_13665" s="T863">np:O</ta>
            <ta e="T866" id="Seg_13666" s="T865">v:pred</ta>
            <ta e="T867" id="Seg_13667" s="T866">np:S</ta>
            <ta e="T869" id="Seg_13668" s="T867">s:temp</ta>
            <ta e="T871" id="Seg_13669" s="T870">v:pred</ta>
            <ta e="T872" id="Seg_13670" s="T871">s:temp</ta>
            <ta e="T873" id="Seg_13671" s="T872">np.h:S</ta>
            <ta e="T875" id="Seg_13672" s="T874">n:pred</ta>
            <ta e="T876" id="Seg_13673" s="T875">cop</ta>
            <ta e="T877" id="Seg_13674" s="T876">np:S</ta>
            <ta e="T878" id="Seg_13675" s="T877">n:pred</ta>
            <ta e="T879" id="Seg_13676" s="T878">cop</ta>
            <ta e="T881" id="Seg_13677" s="T880">np.h:S</ta>
            <ta e="T882" id="Seg_13678" s="T881">v:pred</ta>
            <ta e="T883" id="Seg_13679" s="T882">np.h:S</ta>
            <ta e="T884" id="Seg_13680" s="T883">v:pred</ta>
            <ta e="T885" id="Seg_13681" s="T884">np.h:O</ta>
            <ta e="T886" id="Seg_13682" s="T885">0.2.h:S v:pred</ta>
            <ta e="T888" id="Seg_13683" s="T887">0.2.h:S v:pred</ta>
            <ta e="T889" id="Seg_13684" s="T888">0.2.h:S v:pred</ta>
            <ta e="T892" id="Seg_13685" s="T891">v:pred</ta>
            <ta e="T893" id="Seg_13686" s="T892">np.h:S</ta>
            <ta e="T894" id="Seg_13687" s="T893">np.h:S</ta>
            <ta e="T895" id="Seg_13688" s="T894">pro.h:O</ta>
            <ta e="T896" id="Seg_13689" s="T895">v:pred</ta>
            <ta e="T897" id="Seg_13690" s="T896">0.2.h:S v:pred</ta>
            <ta e="T899" id="Seg_13691" s="T898">0.1.h:S v:pred</ta>
            <ta e="T900" id="Seg_13692" s="T899">np:O</ta>
            <ta e="T901" id="Seg_13693" s="T900">0.1.h:S v:pred</ta>
            <ta e="T904" id="Seg_13694" s="T903">np.h:S</ta>
            <ta e="T905" id="Seg_13695" s="T904">np:O</ta>
            <ta e="T908" id="Seg_13696" s="T907">v:pred</ta>
            <ta e="T909" id="Seg_13697" s="T908">pro:S</ta>
            <ta e="T911" id="Seg_13698" s="T910">n:pred</ta>
            <ta e="T912" id="Seg_13699" s="T911">cop</ta>
            <ta e="T914" id="Seg_13700" s="T913">np.h:S</ta>
            <ta e="T915" id="Seg_13701" s="T914">s:adv</ta>
            <ta e="T916" id="Seg_13702" s="T915">v:pred</ta>
            <ta e="T917" id="Seg_13703" s="T916">np.h:S</ta>
            <ta e="T919" id="Seg_13704" s="T918">np:O</ta>
            <ta e="T920" id="Seg_13705" s="T919">v:pred</ta>
            <ta e="T921" id="Seg_13706" s="T920">s:temp</ta>
            <ta e="T922" id="Seg_13707" s="T921">0.3.h:S v:pred</ta>
            <ta e="T923" id="Seg_13708" s="T922">np.h:S</ta>
            <ta e="T926" id="Seg_13709" s="T925">s:adv</ta>
            <ta e="T928" id="Seg_13710" s="T926">v:pred</ta>
            <ta e="T929" id="Seg_13711" s="T928">np:O</ta>
            <ta e="T930" id="Seg_13712" s="T929">0.3.h:S v:pred</ta>
            <ta e="T932" id="Seg_13713" s="T931">np:O</ta>
            <ta e="T933" id="Seg_13714" s="T932">0.3.h:S v:pred</ta>
            <ta e="T934" id="Seg_13715" s="T933">np.h:S</ta>
            <ta e="T936" id="Seg_13716" s="T935">v:pred</ta>
            <ta e="T938" id="Seg_13717" s="T937">np:O</ta>
            <ta e="T939" id="Seg_13718" s="T938">0.3.h:S v:pred</ta>
            <ta e="T940" id="Seg_13719" s="T939">np.h:S</ta>
            <ta e="T941" id="Seg_13720" s="T940">np.h:O</ta>
            <ta e="T943" id="Seg_13721" s="T942">v:pred</ta>
            <ta e="T946" id="Seg_13722" s="T945">0.3.h:S v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="Top" tierref="Top" />
         <annotation name="Foc" tierref="Foc" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T86" id="Seg_13723" s="T85">RUS:mod</ta>
            <ta e="T99" id="Seg_13724" s="T98">RUS:cult</ta>
            <ta e="T103" id="Seg_13725" s="T102">RUS:mod</ta>
            <ta e="T139" id="Seg_13726" s="T138">RUS:cult</ta>
            <ta e="T152" id="Seg_13727" s="T151">RUS:cult</ta>
            <ta e="T164" id="Seg_13728" s="T163">RUS:cult</ta>
            <ta e="T209" id="Seg_13729" s="T208">RUS:cult</ta>
            <ta e="T210" id="Seg_13730" s="T209">RUS:cult</ta>
            <ta e="T225" id="Seg_13731" s="T224">RUS:cult</ta>
            <ta e="T231" id="Seg_13732" s="T230">RUS:cult</ta>
            <ta e="T232" id="Seg_13733" s="T231">RUS:cult</ta>
            <ta e="T350" id="Seg_13734" s="T349">RUS:cult</ta>
            <ta e="T372" id="Seg_13735" s="T371">RUS:cult</ta>
            <ta e="T396" id="Seg_13736" s="T395">RUS:cult</ta>
            <ta e="T403" id="Seg_13737" s="T402">RUS:cult</ta>
            <ta e="T416" id="Seg_13738" s="T415">RUS:core</ta>
            <ta e="T441" id="Seg_13739" s="T440">RUS:cult</ta>
            <ta e="T465" id="Seg_13740" s="T464">RUS:cult</ta>
            <ta e="T492" id="Seg_13741" s="T491">EV:cult</ta>
            <ta e="T520" id="Seg_13742" s="T519">RUS:core</ta>
            <ta e="T559" id="Seg_13743" s="T558">EV:cult</ta>
            <ta e="T567" id="Seg_13744" s="T566">EV:cult</ta>
            <ta e="T571" id="Seg_13745" s="T570">EV:cult</ta>
            <ta e="T602" id="Seg_13746" s="T601">RUS:core</ta>
            <ta e="T641" id="Seg_13747" s="T640">RUS:mod</ta>
            <ta e="T723" id="Seg_13748" s="T722">EV:cult</ta>
            <ta e="T807" id="Seg_13749" s="T806">RUS:core</ta>
            <ta e="T851" id="Seg_13750" s="T850">EV:cult</ta>
            <ta e="T857" id="Seg_13751" s="T856">EV:cult</ta>
            <ta e="T867" id="Seg_13752" s="T866">EV:cult</ta>
            <ta e="T929" id="Seg_13753" s="T928">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T8" id="Seg_13754" s="T1">Listen to the Dolgan tale named "The boy and the sister".</ta>
            <ta e="T13" id="Seg_13755" s="T8">There lived a boy and his older sister.</ta>
            <ta e="T17" id="Seg_13756" s="T13">His older sister is very hardworking and a master.</ta>
            <ta e="T22" id="Seg_13757" s="T17">Their sledges have coverings made of spotted reindeer fur.</ta>
            <ta e="T26" id="Seg_13758" s="T22">She builds up the chum herself.</ta>
            <ta e="T30" id="Seg_13759" s="T26">She guards even their reindeer in summer.</ta>
            <ta e="T37" id="Seg_13760" s="T30">Her small brother slept since autumn until the sun is going out again.</ta>
            <ta e="T41" id="Seg_13761" s="T37">The herd moved away, the pasture got broader. </ta>
            <ta e="T44" id="Seg_13762" s="T41">The girl wants to change the pasture. </ta>
            <ta e="T49" id="Seg_13763" s="T44">She brought the herd, caught the reindeers and harnessed [them].</ta>
            <ta e="T52" id="Seg_13764" s="T49">Sie tries to wake up her brother.</ta>
            <ta e="T54" id="Seg_13765" s="T52">He doesn't wake up at all.</ta>
            <ta e="T63" id="Seg_13766" s="T54">Having removed the [cover of the] chum, she pulled off her brother's fox blanket.</ta>
            <ta e="T66" id="Seg_13767" s="T63">Being frightened he woke up and dressed. </ta>
            <ta e="T70" id="Seg_13768" s="T66">Having sat down onto the sledge, he again fell asleep.</ta>
            <ta e="T77" id="Seg_13769" s="T70">Gathering wood, getting ice and guarding reindeers the girl becomes very tired.</ta>
            <ta e="T83" id="Seg_13770" s="T77">She became tired and fell asleep, sleeping she froze to death.</ta>
            <ta e="T89" id="Seg_13771" s="T83">Her little brother slept three more days.</ta>
            <ta e="T94" id="Seg_13772" s="T89">He woke up and saw, her sister has frozen to death.</ta>
            <ta e="T98" id="Seg_13773" s="T94">The whole herd froze to death.</ta>
            <ta e="T100" id="Seg_13774" s="T98">A snowdrift covered it.</ta>
            <ta e="T111" id="Seg_13775" s="T100">"If I light up a fire, my sister maybe revives", the boy thought, "one has to go to gather wood."</ta>
            <ta e="T117" id="Seg_13776" s="T111">Above a rocky river he ran to the forest.</ta>
            <ta e="T123" id="Seg_13777" s="T117">He saw, from the sky a (…) is flaming.</ta>
            <ta e="T128" id="Seg_13778" s="T123">A devil is sitting there and frying meat on a skewer.</ta>
            <ta e="T130" id="Seg_13779" s="T128">He doesn't see the boy.</ta>
            <ta e="T137" id="Seg_13780" s="T130">This boy pulled(?) [him?] from the other shore and ate (?).</ta>
            <ta e="T145" id="Seg_13781" s="T137">"How does he not understand", the devil thinks, "the my meat is falling into the fire?"</ta>
            <ta e="T150" id="Seg_13782" s="T145">The devil saw the boy and wanted to kill him.</ta>
            <ta e="T157" id="Seg_13783" s="T150">The boy beat off the devil's eight heads with his mitten.</ta>
            <ta e="T160" id="Seg_13784" s="T157">The heads are dancing.</ta>
            <ta e="T163" id="Seg_13785" s="T160">The boy screamed out: </ta>
            <ta e="T167" id="Seg_13786" s="T163">"The hero doesn't do it for a second or a third time!"</ta>
            <ta e="T173" id="Seg_13787" s="T167">He unstitched the devil's breast and got out a stone.</ta>
            <ta e="T176" id="Seg_13788" s="T173">He apparently has a stony heart.</ta>
            <ta e="T181" id="Seg_13789" s="T176">The boy was going and kicking the stone.</ta>
            <ta e="T189" id="Seg_13790" s="T181">In front of him a stone big as half of the earth appeared, a stone mountain. </ta>
            <ta e="T194" id="Seg_13791" s="T189">He kicked the stone mountain with the stone heart.</ta>
            <ta e="T199" id="Seg_13792" s="T194">The mountain broke up into two directions.</ta>
            <ta e="T203" id="Seg_13793" s="T199">He watched, the earth was visible.</ta>
            <ta e="T208" id="Seg_13794" s="T203">He went there and saw the trace of a sledge driver.</ta>
            <ta e="T215" id="Seg_13795" s="T208">It was apparently a human who checks his deadfall traps and gin traps.</ta>
            <ta e="T222" id="Seg_13796" s="T215">It was not long and he saw that the polar fox hunter was coming towards him.</ta>
            <ta e="T225" id="Seg_13797" s="T222">He came there and greeted.</ta>
            <ta e="T230" id="Seg_13798" s="T225">"What do you do?", the boy asked.</ta>
            <ta e="T233" id="Seg_13799" s="T230">"I'm checking my deadfall traps and gin traps."</ta>
            <ta e="T236" id="Seg_13800" s="T233">"Do you live far away?</ta>
            <ta e="T238" id="Seg_13801" s="T236">How many are you?"</ta>
            <ta e="T244" id="Seg_13802" s="T238">"I'm living with my mother and my father", the boy said.</ta>
            <ta e="T248" id="Seg_13803" s="T244">"Mount, I'll take you along, be [my] guest."</ta>
            <ta e="T251" id="Seg_13804" s="T248">He brought [him] to his chum.</ta>
            <ta e="T256" id="Seg_13805" s="T251">An old man and an old women came out and met them.</ta>
            <ta e="T260" id="Seg_13806" s="T256">"Where do you come from?", they asked.</ta>
            <ta e="T266" id="Seg_13807" s="T260">"No cross-eyed person, no fire-soled person came here yet."</ta>
            <ta e="T268" id="Seg_13808" s="T266">The boy told:</ta>
            <ta e="T275" id="Seg_13809" s="T268">"My sister froze to death, our reindeers also froze to death.</ta>
            <ta e="T279" id="Seg_13810" s="T275">They are lying in the snow, drifted over.</ta>
            <ta e="T283" id="Seg_13811" s="T279">How shall I survive?"</ta>
            <ta e="T290" id="Seg_13812" s="T283">"Be [our] guest at first", the old man said, "then we'll think about it."</ta>
            <ta e="T294" id="Seg_13813" s="T290">The boy went out and killed a reindeer cow.</ta>
            <ta e="T296" id="Seg_13814" s="T294">A reindeer cow with outer fur.</ta>
            <ta e="T300" id="Seg_13815" s="T296">The guest ate it alone.</ta>
            <ta e="T302" id="Seg_13816" s="T300">"Did you eat your fill?", they asked.</ta>
            <ta e="T306" id="Seg_13817" s="T302">"I ate my fill, since autumn I hadn't eaten yet.</ta>
            <ta e="T308" id="Seg_13818" s="T306">Now I'll relax.</ta>
            <ta e="T312" id="Seg_13819" s="T308">The boy slept for three days.</ta>
            <ta e="T319" id="Seg_13820" s="T312">When he woke up, the old man killed the fattest reindeer and gave it to him to eat.</ta>
            <ta e="T325" id="Seg_13821" s="T319">The old man prepared food for three months, for that he goes.</ta>
            <ta e="T327" id="Seg_13822" s="T325">The boy said: </ta>
            <ta e="T335" id="Seg_13823" s="T327">"Somewhere there is the spirit of the earth, there I will go."</ta>
            <ta e="T340" id="Seg_13824" s="T335">The boy laid the stony heart onto his sledge.</ta>
            <ta e="T344" id="Seg_13825" s="T340">He hit the road on the river.</ta>
            <ta e="T348" id="Seg_13826" s="T344">How long he went, though?</ta>
            <ta e="T353" id="Seg_13827" s="T348">Suddenly he saw a chum made of cast iron.</ta>
            <ta e="T363" id="Seg_13828" s="T353">He opened the door and saw a very old woman sitting there, besides her there are lying bellows.</ta>
            <ta e="T367" id="Seg_13829" s="T363">Above her head a bag is hung up.</ta>
            <ta e="T371" id="Seg_13830" s="T367">The bag is like a human face.</ta>
            <ta e="T374" id="Seg_13831" s="T371">The lower part of the oven is hollow.</ta>
            <ta e="T377" id="Seg_13832" s="T374">A river is flowing [through].</ta>
            <ta e="T381" id="Seg_13833" s="T377">On the river a fisherman(?) is fishing.</ta>
            <ta e="T384" id="Seg_13834" s="T381">The bag said:</ta>
            <ta e="T390" id="Seg_13835" s="T384">"A guest has come; woodcutters, get wood and light up a fire!"</ta>
            <ta e="T394" id="Seg_13836" s="T390">The wood came in itself from outside.</ta>
            <ta e="T396" id="Seg_13837" s="T394">The boy understood: </ta>
            <ta e="T399" id="Seg_13838" s="T396">"It's apparently iron woods."</ta>
            <ta e="T404" id="Seg_13839" s="T399">The old woman stood up and put the woods into the oven.</ta>
            <ta e="T406" id="Seg_13840" s="T404">The bag said: </ta>
            <ta e="T408" id="Seg_13841" s="T406">"Bellows, blow!"</ta>
            <ta e="T414" id="Seg_13842" s="T408">The iron floor of the chums became red.</ta>
            <ta e="T417" id="Seg_13843" s="T414">The bag all the time says: </ta>
            <ta e="T424" id="Seg_13844" s="T417">"Bellows, blow, blow, a guest has come, add fire!"</ta>
            <ta e="T428" id="Seg_13845" s="T424">The whole chum became red.</ta>
            <ta e="T432" id="Seg_13846" s="T428">The old woman begged the guest, the boy: </ta>
            <ta e="T436" id="Seg_13847" s="T432">"Don't make it too warm, cool it down."</ta>
            <ta e="T443" id="Seg_13848" s="T436">The boy took the stony heart and threw it in the middle of the oven.</ta>
            <ta e="T446" id="Seg_13849" s="T443">The chum cooled down. </ta>
            <ta e="T448" id="Seg_13850" s="T446">The floor wasn't red [anymore].</ta>
            <ta e="T453" id="Seg_13851" s="T448">The old woman was again heavily covered with rime.</ta>
            <ta e="T457" id="Seg_13852" s="T453">"Don't freeze it too much!", the bag asked, "I'll fulfil every wish of yours."</ta>
            <ta e="T461" id="Seg_13853" s="T457">The bag apparently was the spirit of the earth.</ta>
            <ta e="T468" id="Seg_13854" s="T461">The boy got out the stony heart out of the oven.</ta>
            <ta e="T471" id="Seg_13855" s="T468">The fire flamed up again.</ta>
            <ta e="T474" id="Seg_13856" s="T471">The bag said to the boy: </ta>
            <ta e="T480" id="Seg_13857" s="T474">"Go out, there is a reindeer team standing."</ta>
            <ta e="T488" id="Seg_13858" s="T480">The boy goes out, he doesn't see any reindeer team.</ta>
            <ta e="T495" id="Seg_13859" s="T488">Somewhere only the sound of a bell, of an iron item at the reindeer harness can be heard.</ta>
            <ta e="T500" id="Seg_13860" s="T495">Behind him the voice of the bag can be heard:</ta>
            <ta e="T502" id="Seg_13861" s="T500">"Sit down onto the sledge.</ta>
            <ta e="T510" id="Seg_13862" s="T502">Don't turn back, don't stop on the way, don't feed your reindeer."</ta>
            <ta e="T518" id="Seg_13863" s="T510">The boy stretched out his hand, grabbed the rein and mounted the invisible sledge.</ta>
            <ta e="T521" id="Seg_13864" s="T518">A snowstrom came up.</ta>
            <ta e="T526" id="Seg_13865" s="T521">Who knows how far he went?</ta>
            <ta e="T530" id="Seg_13866" s="T526">The reindeer team takes him along by itself.</ta>
            <ta e="T532" id="Seg_13867" s="T530">The boy doesn't freeze.</ta>
            <ta e="T536" id="Seg_13868" s="T532">He stuck his hands under the seat.</ta>
            <ta e="T542" id="Seg_13869" s="T536">There is lying a warm iron piece wood, apparently.</ta>
            <ta e="T544" id="Seg_13870" s="T542">That one warms him up.</ta>
            <ta e="T554" id="Seg_13871" s="T544">He looked over the river, on the sledge the dead woman is lying, heavily drifted over with snow.</ta>
            <ta e="T556" id="Seg_13872" s="T554">Not bearing it he stopped.</ta>
            <ta e="T565" id="Seg_13873" s="T556">When he stopped, his front reindeer jumped up, and where did it go, where did it come to?</ta>
            <ta e="T570" id="Seg_13874" s="T565">He apparently had three black reindeers.</ta>
            <ta e="T574" id="Seg_13875" s="T570">His front reindeer perished.</ta>
            <ta e="T577" id="Seg_13876" s="T574">The bag's voice was heard:</ta>
            <ta e="T580" id="Seg_13877" s="T577">"I said it to you, don't stop!"</ta>
            <ta e="T591" id="Seg_13878" s="T580">The boy took the red iron piece of wood from his sledge and pushed it into the girl's sledge.</ta>
            <ta e="T595" id="Seg_13879" s="T591">The snow melted immediately.</ta>
            <ta e="T599" id="Seg_13880" s="T595">The frozen woman revived.</ta>
            <ta e="T605" id="Seg_13881" s="T599">He saw that she was a very beautiful girl.</ta>
            <ta e="T609" id="Seg_13882" s="T605">The girl is happy and has no sorrow.</ta>
            <ta e="T613" id="Seg_13883" s="T609">"Now I'll go to my home.</ta>
            <ta e="T615" id="Seg_13884" s="T613">Won't you come along?</ta>
            <ta e="T621" id="Seg_13885" s="T615">I'll feed you, I'll spread a warm bedding", the girl asked.</ta>
            <ta e="T624" id="Seg_13886" s="T621">"Is your house far away?"</ta>
            <ta e="T626" id="Seg_13887" s="T624">"In the water."</ta>
            <ta e="T635" id="Seg_13888" s="T626">The boy had seen already many miracles, therefore he didn't wonder at the girl's story.</ta>
            <ta e="T637" id="Seg_13889" s="T635">He said: </ta>
            <ta e="T641" id="Seg_13890" s="T637">"I have to find my sister.</ta>
            <ta e="T645" id="Seg_13891" s="T641">Later I'll come apparently, wait."</ta>
            <ta e="T651" id="Seg_13892" s="T645">Nobody knows how far the boy went.</ta>
            <ta e="T654" id="Seg_13893" s="T651">Suddenly there were thawed patches.</ta>
            <ta e="T656" id="Seg_13894" s="T654">There is a lot of reindeer moss.</ta>
            <ta e="T658" id="Seg_13895" s="T656">His reindeers were hungry.</ta>
            <ta e="T662" id="Seg_13896" s="T658">"Shall I feed let them eat?", he thought.</ta>
            <ta e="T668" id="Seg_13897" s="T662">He remembered the words of the spirit of the earth and didn't stop the reindeers.</ta>
            <ta e="T671" id="Seg_13898" s="T668">Then he thought: </ta>
            <ta e="T674" id="Seg_13899" s="T671">"They are so hungry, one has to stop them."</ta>
            <ta e="T683" id="Seg_13900" s="T674">When he just stopped them, the reindeer, which was harnessed in the middle, vanished.</ta>
            <ta e="T687" id="Seg_13901" s="T683">Where did it go, where did it run?</ta>
            <ta e="T690" id="Seg_13902" s="T687">Again the voice was heard:</ta>
            <ta e="T695" id="Seg_13903" s="T690">"I said it to you, don't feed your reindeers."</ta>
            <ta e="T699" id="Seg_13904" s="T695">How far did the boy go?</ta>
            <ta e="T706" id="Seg_13905" s="T699">He came to that stony mountain which was torn apart.</ta>
            <ta e="T711" id="Seg_13906" s="T706">He had arrived at his place, apparently.</ta>
            <ta e="T719" id="Seg_13907" s="T711">The antlers of the frozen reindeers hardly stick out of the snow.</ta>
            <ta e="T724" id="Seg_13908" s="T719">He shoveled the snow and found the front reindeer of his sister.</ta>
            <ta e="T728" id="Seg_13909" s="T724">He grazed it with the warm iron piece of wood.</ta>
            <ta e="T732" id="Seg_13910" s="T728">The reindeer revived and stood up.</ta>
            <ta e="T737" id="Seg_13911" s="T732">Then he took the piece of wood and pushed it into the snow.</ta>
            <ta e="T743" id="Seg_13912" s="T737">All the frozen reindeers immediately revived.</ta>
            <ta e="T750" id="Seg_13913" s="T743">They became an intact herd and were eating on the new land.</ta>
            <ta e="T753" id="Seg_13914" s="T750">He looked around, his sister is standing there.</ta>
            <ta e="T758" id="Seg_13915" s="T753">The boy saw his sister and was very happy.</ta>
            <ta e="T767" id="Seg_13916" s="T758">"At first you", the sister said, "slept for a long time, then I.</ta>
            <ta e="T770" id="Seg_13917" s="T767">Spring has come apparently.</ta>
            <ta e="T774" id="Seg_13918" s="T770">I apparently slept during the whole winter."</ta>
            <ta e="T778" id="Seg_13919" s="T774">The boy brought his sister home.</ta>
            <ta e="T781" id="Seg_13920" s="T778">He drove and brought the herd.</ta>
            <ta e="T787" id="Seg_13921" s="T781">After some time the boy prepared himself.</ta>
            <ta e="T791" id="Seg_13922" s="T787">"Where do you go?", his sister asked.</ta>
            <ta e="T794" id="Seg_13923" s="T791">"I'll come soon, don't be afraid."</ta>
            <ta e="T802" id="Seg_13924" s="T794">The boy goes looking for that girl which has her home under water.</ta>
            <ta e="T808" id="Seg_13925" s="T802">One the way a very gloomy snowstorm came up.</ta>
            <ta e="T816" id="Seg_13926" s="T808">He doesn't know whether he goes on the earth, he doesn't know whether he goes on the ice.</ta>
            <ta e="T821" id="Seg_13927" s="T816">He noticed(?), the river was apparently frozen.</ta>
            <ta e="T827" id="Seg_13928" s="T821">The boy got caught on the spring ice and fell into the water.</ta>
            <ta e="T830" id="Seg_13929" s="T827">He fell and became a pike.</ta>
            <ta e="T836" id="Seg_13930" s="T830">He swam in the water and got into a net.</ta>
            <ta e="T840" id="Seg_13931" s="T836">In the broad net there are only pikes. </ta>
            <ta e="T845" id="Seg_13932" s="T840">After some time the girl came.</ta>
            <ta e="T848" id="Seg_13933" s="T845">That girl which had revived.</ta>
            <ta e="T854" id="Seg_13934" s="T848">She turned the pike boy into a front reindeer and harnessed him.</ta>
            <ta e="T856" id="Seg_13935" s="T854">She hit the road home.</ta>
            <ta e="T858" id="Seg_13936" s="T856">The front reindeer doesn't listen.</ta>
            <ta e="T862" id="Seg_13937" s="T858">It pulls(?) only towards the shore.</ta>
            <ta e="T866" id="Seg_13938" s="T862">The girl tries to lead it in vain.</ta>
            <ta e="T871" id="Seg_13939" s="T866">The front reindeer made an effort and went out onto the shore.</ta>
            <ta e="T876" id="Seg_13940" s="T871">After it had gone out, the pike boy became a real boy.</ta>
            <ta e="T879" id="Seg_13941" s="T876">The pikes became reindeer.</ta>
            <ta e="T882" id="Seg_13942" s="T879">The girl and the boy recognized each other.</ta>
            <ta e="T884" id="Seg_13943" s="T882">The girl asked: </ta>
            <ta e="T886" id="Seg_13944" s="T884">"Did you find your sister?</ta>
            <ta e="T888" id="Seg_13945" s="T886">How are you?"</ta>
            <ta e="T897" id="Seg_13946" s="T888">"Come with me", the boy said, "my sister is waiting for you, get to know each other.</ta>
            <ta e="T901" id="Seg_13947" s="T897">We'll live together and unite our herds."</ta>
            <ta e="T912" id="Seg_13948" s="T901">The boy and the girl got the pikes out of the water, they became spotted reindeers.</ta>
            <ta e="T916" id="Seg_13949" s="T912">The boy's sister met them out of joy.</ta>
            <ta e="T920" id="Seg_13950" s="T916">The boy slept for three days.</ta>
            <ta e="T933" id="Seg_13951" s="T920">He woke up and saw: Her sister and the girl are chatting and laughing with each other, they are drinking tea, they are eating boiled meat.</ta>
            <ta e="T939" id="Seg_13952" s="T933">The boy sat down beneath them and ate a whole reindeer.</ta>
            <ta e="T946" id="Seg_13953" s="T939">The boy married the girl, they lived very happily.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T8" id="Seg_13954" s="T1">Hört das dolganische Märchen mit dem Namen "Der Junge und die Schwester".</ta>
            <ta e="T13" id="Seg_13955" s="T8">Es lebten ein Junge und seine ältere Schwester.</ta>
            <ta e="T17" id="Seg_13956" s="T13">Seine ältere Schwester ist sehr arbeitssam und eine Meisterin.</ta>
            <ta e="T22" id="Seg_13957" s="T17">Ihre Schlitten haben Abdeckungen aus scheckigem Rentierfell.</ta>
            <ta e="T26" id="Seg_13958" s="T22">Das Tschum baut sie selber auf.</ta>
            <ta e="T30" id="Seg_13959" s="T26">Sogar ihre Rentiere hütet sie im Sommer.</ta>
            <ta e="T37" id="Seg_13960" s="T30">Ihr kleiner Bruder schlief vom Herbst bis die Sonne wieder herauskommt.</ta>
            <ta e="T41" id="Seg_13961" s="T37">Die Herde bewegte sich weg, die Weide wurde breiter.</ta>
            <ta e="T44" id="Seg_13962" s="T41">Das Mädchen möchte die Weide wechseln.</ta>
            <ta e="T49" id="Seg_13963" s="T44">Sie brachte die Herde, fing die Rentiere und spannte an.</ta>
            <ta e="T52" id="Seg_13964" s="T49">Sie versucht ihren Bruder zu wecken.</ta>
            <ta e="T54" id="Seg_13965" s="T52">Er wacht überhaupt nicht auf.</ta>
            <ta e="T63" id="Seg_13966" s="T54">Nachdem sie das Tschum abgezogen hatte, zog sie die Fuchsdecke ihres Bruder weg.</ta>
            <ta e="T66" id="Seg_13967" s="T63">Erschrocken wachte er auf und zog sich an.</ta>
            <ta e="T70" id="Seg_13968" s="T66">Nachdem er sich auf den Schlitten gesetzt hatte, schlief er wieder ein.</ta>
            <ta e="T77" id="Seg_13969" s="T70">Vom Holzsammeln, Eisholen und Rentierehüten wird das Mädchen sehr müde.</ta>
            <ta e="T83" id="Seg_13970" s="T77">Sie wurde müde und schlief ein, schlafend erfror sie.</ta>
            <ta e="T89" id="Seg_13971" s="T83">Ihr kleiner Bruder schlief noch drei Tage.</ta>
            <ta e="T94" id="Seg_13972" s="T89">Er wachte auf und sah, seine Schwester ist erfrozen.</ta>
            <ta e="T98" id="Seg_13973" s="T94">Die ganze Herde erfror.</ta>
            <ta e="T100" id="Seg_13974" s="T98">Eine Schneewehe bedeckte sie.</ta>
            <ta e="T111" id="Seg_13975" s="T100">"Wenn ich ein Feuer entzünde, erwacht meine Schwester vielleicht wieder zum Leben", dachte er Junge, "man muss Holz sammeln gehen."</ta>
            <ta e="T117" id="Seg_13976" s="T111">Oberhalb eines felsigen Flusses lief er zum Wald.</ta>
            <ta e="T123" id="Seg_13977" s="T117">Er sah, vom Himmel flammt ein (…) Feuer. </ta>
            <ta e="T128" id="Seg_13978" s="T123">Ein Teufel sitzt und brät Fleisch auf einem Spieß.</ta>
            <ta e="T130" id="Seg_13979" s="T128">Er sieht den Jungen nicht.</ta>
            <ta e="T137" id="Seg_13980" s="T130">Dieser Junge zog(?) [ihn?] vom anderen Ufer und aß (?).</ta>
            <ta e="T145" id="Seg_13981" s="T137">"Wie versteht er nicht", denkt der Teufel, "dass mein Fleisch ins Feuer fällt?"</ta>
            <ta e="T150" id="Seg_13982" s="T145">Der Teufel sah den Jungen und wollte ihn töten.</ta>
            <ta e="T157" id="Seg_13983" s="T150">Der Junge schlug dem Teufel mit seinem Fäustling seine acht Köpfe ab.</ta>
            <ta e="T160" id="Seg_13984" s="T157">Die Köpfe tanzen.</ta>
            <ta e="T163" id="Seg_13985" s="T160">Der Junge schrie auf: </ta>
            <ta e="T167" id="Seg_13986" s="T163">"Das macht der Held kein zweites und kein drittes Mal!"</ta>
            <ta e="T173" id="Seg_13987" s="T167">Er trennte die Brust des Teufels auf und holte einen Stein heraus.</ta>
            <ta e="T176" id="Seg_13988" s="T173">Er hat offenbar ein steinernes Herz.</ta>
            <ta e="T181" id="Seg_13989" s="T176">Der Junge trat den Stein und ging.</ta>
            <ta e="T189" id="Seg_13990" s="T181">Vor ihm tauchte ein Stein von der Größe der halben Erde auf, ein Steinberg.</ta>
            <ta e="T194" id="Seg_13991" s="T189">Er trat mit dem Steinherz gegen den Steinberg.</ta>
            <ta e="T199" id="Seg_13992" s="T194">Der Berg brach in zwei Richtungen auf.</ta>
            <ta e="T203" id="Seg_13993" s="T199">Er schaute, die Erde ist zu sehen.</ta>
            <ta e="T208" id="Seg_13994" s="T203">Er ging dorthin und sah die Spur eines Schlittenführers.</ta>
            <ta e="T215" id="Seg_13995" s="T208">Es war wohl ein Mensch, der seine Totfallen und Fangeisen kontrolliert.</ta>
            <ta e="T222" id="Seg_13996" s="T215">Es war nicht lange und er sah, dass ihm der Polarfuchsjäger entgegen kommt.</ta>
            <ta e="T225" id="Seg_13997" s="T222">Er kam und grüßte.</ta>
            <ta e="T230" id="Seg_13998" s="T225">"Was machst du?", fragte der Junge.</ta>
            <ta e="T233" id="Seg_13999" s="T230">"Ich kontrolliere meine Totfallen und Fangeisen."</ta>
            <ta e="T236" id="Seg_14000" s="T233">"Wohnt ihr weit weg?</ta>
            <ta e="T238" id="Seg_14001" s="T236">Wie viele seid ihr?"</ta>
            <ta e="T244" id="Seg_14002" s="T238">"Ich lebe mit meiner Mutter und meinem Vater", sagte der Junge.</ta>
            <ta e="T248" id="Seg_14003" s="T244">"Steig auf, ich nehme dich mit, sei [mein] Gast."</ta>
            <ta e="T251" id="Seg_14004" s="T248">Er brachte [ihn] zu seinem Tschum.</ta>
            <ta e="T256" id="Seg_14005" s="T251">Ein alter Mann und eine alter Frau kamen heraus und trafen sie.</ta>
            <ta e="T260" id="Seg_14006" s="T256">"Wo kommst du her?", fragten sie.</ta>
            <ta e="T266" id="Seg_14007" s="T260">"Hierher ist noch kein Queräugiger, kein Feuersohliger gegangen."</ta>
            <ta e="T268" id="Seg_14008" s="T266">Der Junge erzählte:</ta>
            <ta e="T275" id="Seg_14009" s="T268">"Meine Schwester ist erfroren, unsere Rentiere sind auch erfroren.</ta>
            <ta e="T279" id="Seg_14010" s="T275">Sie liegen unter dem Schnee, zugeweht.</ta>
            <ta e="T283" id="Seg_14011" s="T279">Wie soll ich überleben?"</ta>
            <ta e="T290" id="Seg_14012" s="T283">"Sei zuerst [unser] Gast", sagte der alte Mann, "dann überlegen wir."</ta>
            <ta e="T294" id="Seg_14013" s="T290">Der Junge ging hinaus und tötete eine Rentierkuh.</ta>
            <ta e="T296" id="Seg_14014" s="T294">Eine Rentierkuh mit äußerem Fell.</ta>
            <ta e="T300" id="Seg_14015" s="T296">Der Gast aß sie alleine.</ta>
            <ta e="T302" id="Seg_14016" s="T300">"Bist du satt?", fragten sie.</ta>
            <ta e="T306" id="Seg_14017" s="T302">"Ich bin satt, seit Herbst habe ich nichts gegessen.</ta>
            <ta e="T308" id="Seg_14018" s="T306">Jetzt ruhe ich mich aus."</ta>
            <ta e="T312" id="Seg_14019" s="T308">Der Junge schlief drei Tage lang.</ta>
            <ta e="T319" id="Seg_14020" s="T312">Als er aufwachte, tötete der alte Mann das fetteste Rentier und gab es ihm zu essen.</ta>
            <ta e="T325" id="Seg_14021" s="T319">Der alte Mann bereitete Proviant für drei Monate vor, damit er geht.</ta>
            <ta e="T327" id="Seg_14022" s="T325">Der Junge sagte: </ta>
            <ta e="T335" id="Seg_14023" s="T327">"Irgendwo gibt es den Geist der Erde, sagt man, dort gehe ich hin."</ta>
            <ta e="T340" id="Seg_14024" s="T335">Der Junge legte das steinerne Herz auf seinen Schlitten.</ta>
            <ta e="T344" id="Seg_14025" s="T340">Er machte sich auf den Weg auf dem Fluss.</ta>
            <ta e="T348" id="Seg_14026" s="T344">Wie lange er wohl fuhr?</ta>
            <ta e="T353" id="Seg_14027" s="T348">Plötzlich sah er ein Tschum aus Gusseisen.</ta>
            <ta e="T363" id="Seg_14028" s="T353">Er öffnete die Tür und sah, dass eine sehr alte Frau dort sitzt, neben ihr liegt ein Blasebalg.</ta>
            <ta e="T367" id="Seg_14029" s="T363">Über ihrem Kopf ist eine Tasche aufgehängt.</ta>
            <ta e="T371" id="Seg_14030" s="T367">Die Tasche ist wie ein menschliches Gesicht.</ta>
            <ta e="T374" id="Seg_14031" s="T371">Das Unterteil des Ofens ist hohl.</ta>
            <ta e="T377" id="Seg_14032" s="T374">Ein Fluss fließt [hindurch].</ta>
            <ta e="T381" id="Seg_14033" s="T377">Auf dem Fluss fischt ein Fischer(?).</ta>
            <ta e="T384" id="Seg_14034" s="T381">Die Tasche sagte:</ta>
            <ta e="T390" id="Seg_14035" s="T384">"Ein Gast ist gekommen; Holzhacker, holt Holz und macht ein Feuer!"</ta>
            <ta e="T394" id="Seg_14036" s="T390">Das Holz kam von selbst von draußen herein.</ta>
            <ta e="T396" id="Seg_14037" s="T394">Der Junge verstand: </ta>
            <ta e="T399" id="Seg_14038" s="T396">"Es sind offenbar eisernen Hölzer."</ta>
            <ta e="T404" id="Seg_14039" s="T399">Die alte Frau stand auf und legte die Hölzer in den Ofen.</ta>
            <ta e="T406" id="Seg_14040" s="T404">Die Tasche sagte: </ta>
            <ta e="T408" id="Seg_14041" s="T406">"Blaseblag, blase!"</ta>
            <ta e="T414" id="Seg_14042" s="T408">Der eisernen Boden des Tschums wurde rot.</ta>
            <ta e="T417" id="Seg_14043" s="T414">Die Tasche sagt die ganze Zeit: </ta>
            <ta e="T424" id="Seg_14044" s="T417">"Blasebalg, blase, blase, ein Gast ist gekommen, füge Feuer hinzu!"</ta>
            <ta e="T428" id="Seg_14045" s="T424">Das ganze Tschum wurde rot.</ta>
            <ta e="T432" id="Seg_14046" s="T428">Die alte Frau bat den Gast, den Jungen: </ta>
            <ta e="T436" id="Seg_14047" s="T432">"Mach es nicht zu warm, kühl es ab."</ta>
            <ta e="T443" id="Seg_14048" s="T436">Der Junge nahm das steinerne Herz und warf es mitten in den Ofen.</ta>
            <ta e="T446" id="Seg_14049" s="T443">Das Tschum kühlte ab.</ta>
            <ta e="T448" id="Seg_14050" s="T446">Der Boden war nicht [mehr] rot.</ta>
            <ta e="T453" id="Seg_14051" s="T448">Die alte Frau wurde wieder heftig mit Reif überzogen.</ta>
            <ta e="T457" id="Seg_14052" s="T453">"Frier es nicht zu sehr ein!", bat die Tasche, "ich erfülle alle deine Wünsche."</ta>
            <ta e="T461" id="Seg_14053" s="T457">Die Tasche war offenbar der Geist der Erde.</ta>
            <ta e="T468" id="Seg_14054" s="T461">Der Junge holte das steinerne Herz aus dem Ofen heraus.</ta>
            <ta e="T471" id="Seg_14055" s="T468">Das Feuer flammte wieder auf.</ta>
            <ta e="T474" id="Seg_14056" s="T471">Die Tasche sagte zum Jungen: </ta>
            <ta e="T480" id="Seg_14057" s="T474">"Geh nach draußen, dort steht ein Rentiergespann."</ta>
            <ta e="T488" id="Seg_14058" s="T480">Der Junge geht nach draußen, er sieht kein Rentiergespann.</ta>
            <ta e="T495" id="Seg_14059" s="T488">Irgendwo ist nur das Geräusch einer Klingel, eines Eisenteils am Rentiergeschirr zu hören.</ta>
            <ta e="T500" id="Seg_14060" s="T495">Hinter ihm ist die Stimme der Tasche zu hören:</ta>
            <ta e="T502" id="Seg_14061" s="T500">"Setz dich auf den Schlitten.</ta>
            <ta e="T510" id="Seg_14062" s="T502">Dreh dich nicht um, halt unterwegs nicht an, füttere nicht deine Rentiere."</ta>
            <ta e="T518" id="Seg_14063" s="T510">Der Junge streckte seine Hand aus, griff die Zügel und stieg auf den unsichtbaren Schlitten.</ta>
            <ta e="T521" id="Seg_14064" s="T518">Ein Schneesturm kam auf.</ta>
            <ta e="T526" id="Seg_14065" s="T521">Wieviel er gefahren ist, wer weiß es?</ta>
            <ta e="T530" id="Seg_14066" s="T526">Das Rentiergespann nimmt ihn von selbst mit.</ta>
            <ta e="T532" id="Seg_14067" s="T530">Der Junge friert nicht.</ta>
            <ta e="T536" id="Seg_14068" s="T532">Seine Hände hat er unter den Sitz gesteckt.</ta>
            <ta e="T542" id="Seg_14069" s="T536">Dort liegt offenbar ein warmes eisernes Holz.</ta>
            <ta e="T544" id="Seg_14070" s="T542">Das wärmt ihn.</ta>
            <ta e="T554" id="Seg_14071" s="T544">Er schaute über den Fluss, auf dem Schlitten liegt die gestorbene Frau, heftig von Schnee zugeweht.</ta>
            <ta e="T556" id="Seg_14072" s="T554">Er hielt es nicht aus und hielt an.</ta>
            <ta e="T565" id="Seg_14073" s="T556">Als er anhielt, sprang sein vorderstes Rentier auf und wohin lief es, wohin kam es?</ta>
            <ta e="T570" id="Seg_14074" s="T565">Er hatte offenbar drei schwarze Rentiere.</ta>
            <ta e="T574" id="Seg_14075" s="T570">Sein vorderstes Rentier verendete.</ta>
            <ta e="T577" id="Seg_14076" s="T574">Die Stimme der Tasche war zu hören:</ta>
            <ta e="T580" id="Seg_14077" s="T577">"Ich habe es dir gesagt, halte nicht an!"</ta>
            <ta e="T591" id="Seg_14078" s="T580">Der Junge nahm das rote, eiserne Stück Holz von seinem Schlitten und steckte es in den Schlitten des Mädchens.</ta>
            <ta e="T595" id="Seg_14079" s="T591">Der Schnee schmolz sofort.</ta>
            <ta e="T599" id="Seg_14080" s="T595">Die erfrorene Frau erwachte zum Leben.</ta>
            <ta e="T605" id="Seg_14081" s="T599">Er sah, dass sie ein sehr schönes Mädchen war.</ta>
            <ta e="T609" id="Seg_14082" s="T605">Das Mädchen freut sich und hat keine Not.</ta>
            <ta e="T613" id="Seg_14083" s="T609">"Jetzt fahre ich zu mir nach Hause.</ta>
            <ta e="T615" id="Seg_14084" s="T613">Willst du nicht mitkommen?</ta>
            <ta e="T621" id="Seg_14085" s="T615">Ich gebe dir zu essen, ich breite warme Bettwäsche aus", bat das Mädchen.</ta>
            <ta e="T624" id="Seg_14086" s="T621">"Ist dein Haus weit?"</ta>
            <ta e="T626" id="Seg_14087" s="T624">"Im Wasser."</ta>
            <ta e="T635" id="Seg_14088" s="T626">Der Junge hatte schon viele Wunder gesehen, deshalb wunderte er sich nicht über die Erzählung des Mädchens.</ta>
            <ta e="T637" id="Seg_14089" s="T635">Er sagte: </ta>
            <ta e="T641" id="Seg_14090" s="T637">"Ich muss meine Schwester finden.</ta>
            <ta e="T645" id="Seg_14091" s="T641">Später komme ich wohl, warte."</ta>
            <ta e="T651" id="Seg_14092" s="T645">Wie weit der Junge ging, weiß keiner.</ta>
            <ta e="T654" id="Seg_14093" s="T651">Plötzlich brach die Schneedecke auf.</ta>
            <ta e="T656" id="Seg_14094" s="T654">Es gibt viele Rentierflechten.</ta>
            <ta e="T658" id="Seg_14095" s="T656">Seine Rentiere waren hungrig.</ta>
            <ta e="T662" id="Seg_14096" s="T658">"Soll ich sie essen lassen?", dachte er.</ta>
            <ta e="T668" id="Seg_14097" s="T662">Er erinnerte sich an die Worte des Geists der Erde und hielt die Rentiere nicht an.</ta>
            <ta e="T671" id="Seg_14098" s="T668">Dann dachte er: </ta>
            <ta e="T674" id="Seg_14099" s="T671">"Sie sind so ausgehungert, man muss sie anhalten."</ta>
            <ta e="T683" id="Seg_14100" s="T674">Als er sie gerade anhielt, da verschwand das in der Mitte angespannte Rentier.</ta>
            <ta e="T687" id="Seg_14101" s="T683">Wo ging es hin, wo rannte es hin?</ta>
            <ta e="T690" id="Seg_14102" s="T687">Wieder war die Stimme zu hören:</ta>
            <ta e="T695" id="Seg_14103" s="T690">"Ich habe es dir gesagt, füttere deine Rentiere nicht."</ta>
            <ta e="T699" id="Seg_14104" s="T695">Wie weit ging der Junge?</ta>
            <ta e="T706" id="Seg_14105" s="T699">Er kam zu diesem Steinberg, der auseinandergerissen war.</ta>
            <ta e="T711" id="Seg_14106" s="T706">Er war offenbar an seinem Ort angekommen.</ta>
            <ta e="T719" id="Seg_14107" s="T711">Die Geweihe der erfrorenen Rentiere ragen kaum aus dem Schnee heraus.</ta>
            <ta e="T724" id="Seg_14108" s="T719">Er grub den Schnee weg und fand das vorderste Rentier seiner Schwester.</ta>
            <ta e="T728" id="Seg_14109" s="T724">Er streifte es mit dem warmen eisernen Holz.</ta>
            <ta e="T732" id="Seg_14110" s="T728">Das Rentier erwachte zum Leben und stand auf.</ta>
            <ta e="T737" id="Seg_14111" s="T732">Dann nahm er das Holz und stieß es in den Schnee.</ta>
            <ta e="T743" id="Seg_14112" s="T737">Die erfrorenen Rentiere erwachten alle sofort wieder zum Leben.</ta>
            <ta e="T750" id="Seg_14113" s="T743">Sie wurden zu einer intakten Herde und fraßen auf dem neuen Land.</ta>
            <ta e="T753" id="Seg_14114" s="T750">Er schaute sich um, seine Schwester steht dort.</ta>
            <ta e="T758" id="Seg_14115" s="T753">Der Junge sah seine Schwester und freute sich sehr.</ta>
            <ta e="T767" id="Seg_14116" s="T758">"Zuerst hast du", sagte die Schwester, "sehr lange geschlafen, dann ich.</ta>
            <ta e="T770" id="Seg_14117" s="T767">Es ist offenbar Frühling geworden.</ta>
            <ta e="T774" id="Seg_14118" s="T770">Ich habe wohl den ganzen Winter geschlafen."</ta>
            <ta e="T778" id="Seg_14119" s="T774">Der Junge brachte seine Schwester nach Hause.</ta>
            <ta e="T781" id="Seg_14120" s="T778">Er trieb und brachte die Herde.</ta>
            <ta e="T787" id="Seg_14121" s="T781">Nach einiger Zeit machte sich der Junge fertig.</ta>
            <ta e="T791" id="Seg_14122" s="T787">"Wo gehst du hin?", fragte seine Schwester.</ta>
            <ta e="T794" id="Seg_14123" s="T791">"Ich komme bald, hab keine Angst."</ta>
            <ta e="T802" id="Seg_14124" s="T794">Der Junge geht dieses Mädchen suchen, das unter Wasser zuhause ist.</ta>
            <ta e="T808" id="Seg_14125" s="T802">Auf dem Weg kam ein sehr düsterer Schneesturm auf.</ta>
            <ta e="T816" id="Seg_14126" s="T808">Er weiß nicht, ob er über die Erde geht, er weiß nicht, ob er über das Eis geht.</ta>
            <ta e="T821" id="Seg_14127" s="T816">Er bemerkte(?), der Fluss war offenbar vereist.</ta>
            <ta e="T827" id="Seg_14128" s="T821">Der Junge blieb am Frühlingseis hängen und fiel ins Wasser.</ta>
            <ta e="T830" id="Seg_14129" s="T827">Er fiel und wurde zu einem Hecht.</ta>
            <ta e="T836" id="Seg_14130" s="T830">Er schwamm im Wasser und geriet in ein Netz.</ta>
            <ta e="T840" id="Seg_14131" s="T836">In dem breiten Netz sind nur Hechte.</ta>
            <ta e="T845" id="Seg_14132" s="T840">Nach einiger Zeit kam das Mädchen.</ta>
            <ta e="T848" id="Seg_14133" s="T845">Dieses Mädchen, das wieder zum Leben erwacht war.</ta>
            <ta e="T854" id="Seg_14134" s="T848">Sie machte den Hechtjungen zum vordersten Rentier und spannte ihn an.</ta>
            <ta e="T856" id="Seg_14135" s="T854">Sie machte sich auf den Weg nach Hause.</ta>
            <ta e="T858" id="Seg_14136" s="T856">Das vorderste Rentier hört nicht.</ta>
            <ta e="T862" id="Seg_14137" s="T858">Es zieht(?) nur zum Ufer.</ta>
            <ta e="T866" id="Seg_14138" s="T862">Das Mädchen versucht vergebens es zu führen.</ta>
            <ta e="T871" id="Seg_14139" s="T866">Das vorderste Rentier strengte sich an und lief aufs Ufer.</ta>
            <ta e="T876" id="Seg_14140" s="T871">Nachdem es herausgegangen war, wurde der Hechtjunge zu einem richtigen Jungen.</ta>
            <ta e="T879" id="Seg_14141" s="T876">Die Hechte wurden zu Rentieren.</ta>
            <ta e="T882" id="Seg_14142" s="T879">Das Mädchen und der Junge erkannten einander.</ta>
            <ta e="T884" id="Seg_14143" s="T882">Das Mädchen fragte: </ta>
            <ta e="T886" id="Seg_14144" s="T884">"Hast du deine Schwester gefunden?</ta>
            <ta e="T888" id="Seg_14145" s="T886">Wie geht es dir?"</ta>
            <ta e="T897" id="Seg_14146" s="T888">"Komm mit mir", sagte der Junge, "meine Schwester wartet auf dich, lernt euch kennen.</ta>
            <ta e="T901" id="Seg_14147" s="T897">Wir werden zusammen leben und unsere Herden vereinigen."</ta>
            <ta e="T912" id="Seg_14148" s="T901">Der Junge und das Mädchen holten die Hechte aus dem Wasser, jene wurden zu scheckigen Rentieren.</ta>
            <ta e="T916" id="Seg_14149" s="T912">Die Schwester des Jungen traf sie vor Freude.</ta>
            <ta e="T920" id="Seg_14150" s="T916">Der Junge schlief drei Tage lang.</ta>
            <ta e="T933" id="Seg_14151" s="T920">Er wachte auf und sah: Seine Schwester und das Mädchen unterhalten sich und lachen miteinander, sie trinken Tee, sie essen gekochtes Fleisch.</ta>
            <ta e="T939" id="Seg_14152" s="T933">Der Junge setzte sich neben sie und aß ein ganzes Rentier.</ta>
            <ta e="T946" id="Seg_14153" s="T939">Der Junge heiratete das Mädchen, sie lebten sehr glücklich.</ta>
         </annotation>
         <annotation name="fr" tierref="fr" />
         <annotation name="ltr" tierref="ltr">
            <ta e="T8" id="Seg_14154" s="T1">Прослушайте долганскую сказку "Со старшей сестрой юноша" под названием.</ta>
            <ta e="T13" id="Seg_14155" s="T8">Юноша со старшей сестрой вдвоём жили. </ta>
            <ta e="T17" id="Seg_14156" s="T13">Сестра рабочащая и рукодельница.</ta>
            <ta e="T22" id="Seg_14157" s="T17">Грузовые санки от пятнистого оленя шкурой покрытие имеют.</ta>
            <ta e="T26" id="Seg_14158" s="T22">Чум сама устанавливает. </ta>
            <ta e="T30" id="Seg_14159" s="T26">Оленей даже летом сторожит.</ta>
            <ta e="T37" id="Seg_14160" s="T30">Братишка с осени заснул до появления солнца.</ta>
            <ta e="T41" id="Seg_14161" s="T37">Стадо отдалилось, пастбище расширилось. </ta>
            <ta e="T44" id="Seg_14162" s="T41">Девушка пастбище поменять хочет.</ta>
            <ta e="T49" id="Seg_14163" s="T44">Стадо пригнала, оленей поймала, запрягла. </ta>
            <ta e="T52" id="Seg_14164" s="T49">Братика пытается разбудить. </ta>
            <ta e="T54" id="Seg_14165" s="T52">Никак не просыпается.</ta>
            <ta e="T63" id="Seg_14166" s="T54">С чума нюки сняв, с брата песцовое одеяло стянула. </ta>
            <ta e="T66" id="Seg_14167" s="T63">Испугавшись проснулся, оделся. </ta>
            <ta e="T70" id="Seg_14168" s="T66">На сани сев, опять заснул.</ta>
            <ta e="T77" id="Seg_14169" s="T70">Собирая дрова, добывая лёд, охраняя оленей девушка сильно уставала.</ta>
            <ta e="T83" id="Seg_14170" s="T77">Устав заснула, во сне замёрзла.</ta>
            <ta e="T89" id="Seg_14171" s="T83">Братишка ещё три дня проспал.</ta>
            <ta e="T94" id="Seg_14172" s="T89">Проснулся, увидел, сестра замёрзла. </ta>
            <ta e="T98" id="Seg_14173" s="T94">Стадо всё от холода померло.</ta>
            <ta e="T100" id="Seg_14174" s="T98">Сугробом накрыло. </ta>
            <ta e="T111" id="Seg_14175" s="T100">"Огонь если разожгу, может быть, сестра оживёт, -- подумал парень, -- за дровами пойти надо".</ta>
            <ta e="T117" id="Seg_14176" s="T111">По гористой реки сверху к лесу побежал.</ta>
            <ta e="T123" id="Seg_14177" s="T117">Видит, от неба начинающийся костёр пламенеет лежит.</ta>
            <ta e="T128" id="Seg_14178" s="T123">Дьявол (у костра?) мясо вялит сидит. </ta>
            <ta e="T130" id="Seg_14179" s="T128">Мальчика не видит.</ta>
            <ta e="T137" id="Seg_14180" s="T130">Этот парень, вытягивая, с другого берега кушал сидел.</ta>
            <ta e="T145" id="Seg_14181" s="T137">"Как не понимает-то, -- думает Дьявол, -- Что куски мяса на костёр падают?"</ta>
            <ta e="T150" id="Seg_14182" s="T145">Дьявол мальчика увидев, убить захотел. </ta>
            <ta e="T157" id="Seg_14183" s="T150">Парень рукавицами Дьявола восемь голов напрочь отсёк. </ta>
            <ta e="T160" id="Seg_14184" s="T157">Головы танцуют ходят.</ta>
            <ta e="T163" id="Seg_14185" s="T160">Парень тут вскрикнул: </ta>
            <ta e="T167" id="Seg_14186" s="T163">"Богатырь ни на второй раз, ни в третий раз не повторяет!"</ta>
            <ta e="T173" id="Seg_14187" s="T167">Дьявола грудь распоров, камень вынул. </ta>
            <ta e="T176" id="Seg_14188" s="T173">Каменное сердце у него, оказывается.</ta>
            <ta e="T181" id="Seg_14189" s="T176">Юноша камень пиная пошёл. </ta>
            <ta e="T189" id="Seg_14190" s="T181">Перед ним с пол земли размером камень появился. Каменная гора. </ta>
            <ta e="T194" id="Seg_14191" s="T189">Каменную гору каменным сердцем подопнул. </ta>
            <ta e="T199" id="Seg_14192" s="T194">Гора его, пополам расколовшись, упала. </ta>
            <ta e="T203" id="Seg_14193" s="T199">Присмотрелся было, землю видно.</ta>
            <ta e="T208" id="Seg_14194" s="T203">Туда подойдя, оленевода след увидел. </ta>
            <ta e="T215" id="Seg_14195" s="T208">Пасти, капканы проверял который человек, оказывается.</ta>
            <ta e="T222" id="Seg_14196" s="T215">Немного погодя охотник, как навстречу ему идёт, видит. </ta>
            <ta e="T225" id="Seg_14197" s="T222">Как подошёл, встал и поздоровался.</ta>
            <ta e="T230" id="Seg_14198" s="T225">"Что делаешь ходишь?" -- спросил парень.</ta>
            <ta e="T233" id="Seg_14199" s="T230">"Пасти, капкан проверяю". </ta>
            <ta e="T236" id="Seg_14200" s="T233">"Далеко ваш дом? </ta>
            <ta e="T238" id="Seg_14201" s="T236">Сколько вас?"</ta>
            <ta e="T244" id="Seg_14202" s="T238">"С матерью да с отцом живу", -- ответил парень. </ta>
            <ta e="T248" id="Seg_14203" s="T244">"Подсаживайся ко мне, отвезу. Гостем будь".</ta>
            <ta e="T251" id="Seg_14204" s="T248">В чум свой привёз.</ta>
            <ta e="T256" id="Seg_14205" s="T251">Дед с бабкой навстречу к ним вышли. </ta>
            <ta e="T260" id="Seg_14206" s="T256">"Откуда приехал?" -- спросили.</ta>
            <ta e="T266" id="Seg_14207" s="T260">"Сюда с поперечными глазами, с огненными стопами не ступала нога."</ta>
            <ta e="T268" id="Seg_14208" s="T266">Парень рассказал:</ta>
            <ta e="T275" id="Seg_14209" s="T268">"Сестра замёрнув умерла, олени тоже замёрли и умерли. </ta>
            <ta e="T279" id="Seg_14210" s="T275">Под снегом лежат, под перемётом. </ta>
            <ta e="T283" id="Seg_14211" s="T279">Как выживу, не знаю?"</ta>
            <ta e="T290" id="Seg_14212" s="T283">"Сначала погости, -- промолвил дед, -- потом посоветуемся".</ta>
            <ta e="T294" id="Seg_14213" s="T290">Юноша выйдя, важенку зарубил. </ta>
            <ta e="T296" id="Seg_14214" s="T294">С ннаружной шкурой важенку. </ta>
            <ta e="T300" id="Seg_14215" s="T296">Гость ту один съел.</ta>
            <ta e="T302" id="Seg_14216" s="T300">"Наелся?" -- спросили. </ta>
            <ta e="T306" id="Seg_14217" s="T302">"Наелся, с осени не ел. </ta>
            <ta e="T308" id="Seg_14218" s="T306">Сейчас отдохну".</ta>
            <ta e="T312" id="Seg_14219" s="T308">Три дня парень спал.</ta>
            <ta e="T319" id="Seg_14220" s="T312">Как проснулся, старик, самого жирного оленя зарезав, дал покушать.</ta>
            <ta e="T325" id="Seg_14221" s="T319">Старик трёхмесячную провизию подготовил, чтобы в путь отправился.</ta>
            <ta e="T327" id="Seg_14222" s="T325">Парень сказал: </ta>
            <ta e="T335" id="Seg_14223" s="T327">"Где-то земли дух есть, говорят. Туда поеду".</ta>
            <ta e="T340" id="Seg_14224" s="T335">Парень на сани свои каменное сердце положил. </ta>
            <ta e="T344" id="Seg_14225" s="T340">По реке тронулся.</ta>
            <ta e="T348" id="Seg_14226" s="T344">Сколько ехал не известно? </ta>
            <ta e="T353" id="Seg_14227" s="T348">Боже, из чугуна чум увидел.</ta>
            <ta e="T363" id="Seg_14228" s="T353">Дверь приоткрыв видит -- старая очень старуха сидит, а рядом меха для раздувания огня лежат. </ta>
            <ta e="T367" id="Seg_14229" s="T363">Над головой котомка висит котомка (меховая сумка) висит. </ta>
            <ta e="T371" id="Seg_14230" s="T367">Котомка на человеческое лицо похоже.</ta>
            <ta e="T374" id="Seg_14231" s="T371">Под очагом пустота. </ta>
            <ta e="T377" id="Seg_14232" s="T374">Река протекает. </ta>
            <ta e="T381" id="Seg_14233" s="T377">На реке рыбак рыбачит.</ta>
            <ta e="T384" id="Seg_14234" s="T381">Котамка с речью стала: </ta>
            <ta e="T390" id="Seg_14235" s="T384">"Гость пришёл. Дровосеки, натоскайте дров, огонь разожгите!"</ta>
            <ta e="T394" id="Seg_14236" s="T390">Дрова сами зашли с улицы. </ta>
            <ta e="T396" id="Seg_14237" s="T394">Парень понял: </ta>
            <ta e="T399" id="Seg_14238" s="T396">"Железные дрова, оказыается".</ta>
            <ta e="T404" id="Seg_14239" s="T399">Старуха встав, дрова в огонь сложила.</ta>
            <ta e="T406" id="Seg_14240" s="T404">Котомка сказала: </ta>
            <ta e="T408" id="Seg_14241" s="T406">"Меха, раздувай!" </ta>
            <ta e="T414" id="Seg_14242" s="T408">У чума железные полы покраснели. </ta>
            <ta e="T417" id="Seg_14243" s="T414">Котомка всё приговаривает: </ta>
            <ta e="T424" id="Seg_14244" s="T417">"Меха, раздувай, раздувай! Гость пришёл, огня добавь!" </ta>
            <ta e="T428" id="Seg_14245" s="T424">Чум весь раскалился. </ta>
            <ta e="T432" id="Seg_14246" s="T428">Старуха гостя парня попросила: </ta>
            <ta e="T436" id="Seg_14247" s="T432">"Не грей сильно, остуди--ка".</ta>
            <ta e="T443" id="Seg_14248" s="T436">Мальчик, каменное сердце схватив, очага в середину кинул. </ta>
            <ta e="T446" id="Seg_14249" s="T443">Чум остыл. </ta>
            <ta e="T448" id="Seg_14250" s="T446">Полы не покраснели.</ta>
            <ta e="T453" id="Seg_14251" s="T448">Старуха опять инеем покрылась. </ta>
            <ta e="T457" id="Seg_14252" s="T453">"Сильно не замораживай!" -- попросила Котомка.</ta>
            <ta e="T461" id="Seg_14253" s="T457">Котомка, оказывается, земли дух.</ta>
            <ta e="T468" id="Seg_14254" s="T461">Мальчик каменное сердце из очага вынул. </ta>
            <ta e="T471" id="Seg_14255" s="T468">Пламя опять разгорелось. </ta>
            <ta e="T474" id="Seg_14256" s="T471">Котомка сказала парню: </ta>
            <ta e="T480" id="Seg_14257" s="T474">"На улицу выйди, там упряжка оленья стоит".</ta>
            <ta e="T488" id="Seg_14258" s="T480">Парень на улицу выйдя, никакой упряжки не увидел. </ta>
            <ta e="T495" id="Seg_14259" s="T488">Где-то колокольчика, гугаарки только (жележная повдеска к упряжи) звон слышен.</ta>
            <ta e="T500" id="Seg_14260" s="T495">Позади Котомки голос слышится: </ta>
            <ta e="T502" id="Seg_14261" s="T500">"Садись в свои сани. </ta>
            <ta e="T510" id="Seg_14262" s="T502">Назад не оборачивайся, по пути не останавливайся, оленей не корми". </ta>
            <ta e="T518" id="Seg_14263" s="T510">Парень руку протянул за возжи схватил, на невидимые сани сел. </ta>
            <ta e="T521" id="Seg_14264" s="T518">С ветром пурга поднялась.</ta>
            <ta e="T526" id="Seg_14265" s="T521">Сколько ехал интересно, кто его знает? </ta>
            <ta e="T530" id="Seg_14266" s="T526">Олени в упряжке сами везут. </ta>
            <ta e="T532" id="Seg_14267" s="T530">Парень не мёрзнет.</ta>
            <ta e="T536" id="Seg_14268" s="T532">Руку под сидушку просунул. </ta>
            <ta e="T542" id="Seg_14269" s="T536">Там горячая железная деревяшка лежит, оказывается, </ta>
            <ta e="T544" id="Seg_14270" s="T542">она и греет.</ta>
            <ta e="T554" id="Seg_14271" s="T544">Над тундрой посмотрел, на санях мёртвая женщина лежит, сильно заметённая снегом.</ta>
            <ta e="T556" id="Seg_14272" s="T554">Не выдержав, остановился. </ta>
            <ta e="T565" id="Seg_14273" s="T556">Как только остановился, передовой олень, оторвавшись, куда побежал, куда пришёл?</ta>
            <ta e="T570" id="Seg_14274" s="T565">Три запряжённых оленя у него было, оказывается.</ta>
            <ta e="T574" id="Seg_14275" s="T570">Возжи оборвавши убежали. </ta>
            <ta e="T577" id="Seg_14276" s="T574">Котомки голос послышался: </ta>
            <ta e="T580" id="Seg_14277" s="T577">"Тебе говорила, не останавливайся!"</ta>
            <ta e="T591" id="Seg_14278" s="T580">Парень из саней красную железную деревяшку взяв, девушкиных саней в угол насквозь проткнул. </ta>
            <ta e="T595" id="Seg_14279" s="T591">Снег тут же расстаял.</ta>
            <ta e="T599" id="Seg_14280" s="T595">От окоченения умершая женщина ожила. </ta>
            <ta e="T605" id="Seg_14281" s="T599">Видит, невероятно красивая очень девушка, оказывается.</ta>
            <ta e="T613" id="Seg_14282" s="T609">"Сейчас в свой дом доберусь. </ta>
            <ta e="T615" id="Seg_14283" s="T613">Не поедешь со мной ?</ta>
            <ta e="T621" id="Seg_14284" s="T615">Накормлю, тёплую постель застелю", -- предложила девушка.</ta>
            <ta e="T624" id="Seg_14285" s="T621">"Далеко твой дом?" </ta>
            <ta e="T626" id="Seg_14286" s="T624">"Под водой".</ta>
            <ta e="T635" id="Seg_14287" s="T626">Парень много дива повидал. Поэтому девичьему рассказу не удивился.</ta>
            <ta e="T637" id="Seg_14288" s="T635">Промолвил: </ta>
            <ta e="T641" id="Seg_14289" s="T637">"Я сестру найти должен. </ta>
            <ta e="T645" id="Seg_14290" s="T641">Потом приеду. Жди".</ta>
            <ta e="T651" id="Seg_14291" s="T645">Парень сколько долго шёл, никто не знает.</ta>
            <ta e="T654" id="Seg_14292" s="T651">Тут почернела земля. </ta>
            <ta e="T656" id="Seg_14293" s="T654">Ягеля много очень. </ta>
            <ta e="T658" id="Seg_14294" s="T656">Олени проголодались.</ta>
            <ta e="T662" id="Seg_14295" s="T658">"Покормить что-ли?" -- подумал. </ta>
            <ta e="T668" id="Seg_14296" s="T662">Земли духа наказ вспоминая, оленей не остановил.</ta>
            <ta e="T671" id="Seg_14297" s="T668">Потом подумал: </ta>
            <ta e="T674" id="Seg_14298" s="T671">"Силно проголодались, остановить надо".</ta>
            <ta e="T683" id="Seg_14299" s="T674">Как только остановил, среднего запряжённого олененя и след простыл. </ta>
            <ta e="T687" id="Seg_14300" s="T683">Куда девался, куда поскакал?</ta>
            <ta e="T690" id="Seg_14301" s="T687">Опять голос послышался: </ta>
            <ta e="T695" id="Seg_14302" s="T690">"Я тебе говорила, не корми оленей".</ta>
            <ta e="T699" id="Seg_14303" s="T695">Парень сколько шёл? </ta>
            <ta e="T706" id="Seg_14304" s="T699">До той каменной горы дошёл, пополам расколовшейся.</ta>
            <ta e="T711" id="Seg_14305" s="T706">Ведь на свою землю вернулся, оказывается.</ta>
            <ta e="T719" id="Seg_14306" s="T711">От окоченения умерших оленей рога еле из-под снега торчат.</ta>
            <ta e="T724" id="Seg_14307" s="T719">Снег откапывая, сестры передавого оленя нашёл. </ta>
            <ta e="T728" id="Seg_14308" s="T724">Горячей железной палкой поскребла.</ta>
            <ta e="T732" id="Seg_14309" s="T728">Олень оживши встал. </ta>
            <ta e="T737" id="Seg_14310" s="T732">Затем палку взял и в снег воткнул.</ta>
            <ta e="T743" id="Seg_14311" s="T737">От океченения умершие олени тут же все ожили. </ta>
            <ta e="T750" id="Seg_14312" s="T743">Целое стадо образовав, кушают ходят по новой земле. </ta>
            <ta e="T753" id="Seg_14313" s="T750">Повернулс, сестра стоит. </ta>
            <ta e="T758" id="Seg_14314" s="T753">Мальчик сестру увидев, очень обрадовался.</ta>
            <ta e="T767" id="Seg_14315" s="T758">"Сначала ты, -- говорит сестра, -- долго очень проспал, потом я. </ta>
            <ta e="T770" id="Seg_14316" s="T767">Весна наступила, оказывается. </ta>
            <ta e="T774" id="Seg_14317" s="T770">Всю зиму проспал, оказывается."</ta>
            <ta e="T778" id="Seg_14318" s="T774">Парень сестру домой отвёл. </ta>
            <ta e="T781" id="Seg_14319" s="T778">Стадо собрав, подогнал.</ta>
            <ta e="T787" id="Seg_14320" s="T781">Через некоторое время этот парень засобирался. </ta>
            <ta e="T791" id="Seg_14321" s="T787">"Куда собрался?" -- спросила сестра.</ta>
            <ta e="T794" id="Seg_14322" s="T791">"Скоро вернусь, не переживай".</ta>
            <ta e="T802" id="Seg_14323" s="T794">Парень ту девушку искать пойдёт, втидимо, под водой что живущую.</ta>
            <ta e="T808" id="Seg_14324" s="T802">В пути порошистая пурга наступила. </ta>
            <ta e="T816" id="Seg_14325" s="T808">По земле ли едет, не знает, по льду ли идёт, не знает.</ta>
            <ta e="T821" id="Seg_14326" s="T816">Опомнился, моря лёд, оказывается. </ta>
            <ta e="T827" id="Seg_14327" s="T821">О весенний лёд подскользнувшись, парень в воду упал.</ta>
            <ta e="T830" id="Seg_14328" s="T827">Упав в щуку превратился. </ta>
            <ta e="T836" id="Seg_14329" s="T830">В воде проплывая, в сети попал.</ta>
            <ta e="T840" id="Seg_14330" s="T836">В широких сетях только щуки.</ta>
            <ta e="T845" id="Seg_14331" s="T840">Немного погодя девушка пришла. </ta>
            <ta e="T848" id="Seg_14332" s="T845">Та девушка, которая оживила.</ta>
            <ta e="T854" id="Seg_14333" s="T848">Щуку-парня сделав передовым оленем, запрягла. </ta>
            <ta e="T856" id="Seg_14334" s="T854">Домой засобралась.</ta>
            <ta e="T858" id="Seg_14335" s="T856">Передовой олень не слушается. </ta>
            <ta e="T862" id="Seg_14336" s="T858">К берегу только тянется. </ta>
            <ta e="T866" id="Seg_14337" s="T862">Девушка его пытается повернуть.</ta>
            <ta e="T871" id="Seg_14338" s="T866">Передовой, с силой помчавшись, на берег вышел. </ta>
            <ta e="T876" id="Seg_14339" s="T871">Как вышел парень - щука в настоящего парня превратился.</ta>
            <ta e="T879" id="Seg_14340" s="T876">Щуки в оленей превратились. </ta>
            <ta e="T882" id="Seg_14341" s="T879">Девушка с парнем узнали друг друга.</ta>
            <ta e="T884" id="Seg_14342" s="T882">Девушка спросила: </ta>
            <ta e="T886" id="Seg_14343" s="T884">"Сестру нашла? </ta>
            <ta e="T888" id="Seg_14344" s="T886">Как поживаешь?"</ta>
            <ta e="T897" id="Seg_14345" s="T888">"Поедем со мной," -- сказал парень, -- сестра тебя ждёт, познакомьтесь.</ta>
            <ta e="T901" id="Seg_14346" s="T897">Вместе будем жить, стадо объединим".</ta>
            <ta e="T912" id="Seg_14347" s="T901">Парень с девушкой щук из воды вынули, и те в пятнистых оленей превратились.</ta>
            <ta e="T916" id="Seg_14348" s="T912">Парня сестра от радости познакомилась. </ta>
            <ta e="T920" id="Seg_14349" s="T916">Парень три дня проспал.</ta>
            <ta e="T933" id="Seg_14350" s="T920">Проснулся: сестра с девушкой разговаривая смеются сидят, чай пьют, варёное мясо едят.</ta>
            <ta e="T939" id="Seg_14351" s="T933">Парень в углу сел, целого оленя съел.</ta>
            <ta e="T946" id="Seg_14352" s="T939">Парень девушку в жёны взял. Очень счастливо зажили.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T554" id="Seg_14353" s="T544">[DCh]: Not clear why in the Russian literal translation "ebe" is translated with 'tundra'.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T0" />
            <conversion-tli id="T949" />
            <conversion-tli id="T947" />
            <conversion-tli id="T948" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T559" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
            <conversion-tli id="T571" />
            <conversion-tli id="T572" />
            <conversion-tli id="T573" />
            <conversion-tli id="T574" />
            <conversion-tli id="T575" />
            <conversion-tli id="T576" />
            <conversion-tli id="T577" />
            <conversion-tli id="T578" />
            <conversion-tli id="T579" />
            <conversion-tli id="T580" />
            <conversion-tli id="T581" />
            <conversion-tli id="T582" />
            <conversion-tli id="T583" />
            <conversion-tli id="T584" />
            <conversion-tli id="T585" />
            <conversion-tli id="T586" />
            <conversion-tli id="T587" />
            <conversion-tli id="T588" />
            <conversion-tli id="T589" />
            <conversion-tli id="T590" />
            <conversion-tli id="T591" />
            <conversion-tli id="T592" />
            <conversion-tli id="T593" />
            <conversion-tli id="T594" />
            <conversion-tli id="T595" />
            <conversion-tli id="T596" />
            <conversion-tli id="T597" />
            <conversion-tli id="T598" />
            <conversion-tli id="T599" />
            <conversion-tli id="T600" />
            <conversion-tli id="T601" />
            <conversion-tli id="T602" />
            <conversion-tli id="T603" />
            <conversion-tli id="T604" />
            <conversion-tli id="T605" />
            <conversion-tli id="T606" />
            <conversion-tli id="T607" />
            <conversion-tli id="T608" />
            <conversion-tli id="T609" />
            <conversion-tli id="T610" />
            <conversion-tli id="T611" />
            <conversion-tli id="T612" />
            <conversion-tli id="T613" />
            <conversion-tli id="T614" />
            <conversion-tli id="T615" />
            <conversion-tli id="T616" />
            <conversion-tli id="T617" />
            <conversion-tli id="T618" />
            <conversion-tli id="T619" />
            <conversion-tli id="T620" />
            <conversion-tli id="T621" />
            <conversion-tli id="T622" />
            <conversion-tli id="T623" />
            <conversion-tli id="T624" />
            <conversion-tli id="T625" />
            <conversion-tli id="T626" />
            <conversion-tli id="T627" />
            <conversion-tli id="T628" />
            <conversion-tli id="T629" />
            <conversion-tli id="T630" />
            <conversion-tli id="T631" />
            <conversion-tli id="T632" />
            <conversion-tli id="T633" />
            <conversion-tli id="T634" />
            <conversion-tli id="T635" />
            <conversion-tli id="T636" />
            <conversion-tli id="T637" />
            <conversion-tli id="T638" />
            <conversion-tli id="T639" />
            <conversion-tli id="T640" />
            <conversion-tli id="T641" />
            <conversion-tli id="T642" />
            <conversion-tli id="T643" />
            <conversion-tli id="T644" />
            <conversion-tli id="T645" />
            <conversion-tli id="T646" />
            <conversion-tli id="T647" />
            <conversion-tli id="T648" />
            <conversion-tli id="T649" />
            <conversion-tli id="T650" />
            <conversion-tli id="T651" />
            <conversion-tli id="T652" />
            <conversion-tli id="T653" />
            <conversion-tli id="T654" />
            <conversion-tli id="T655" />
            <conversion-tli id="T656" />
            <conversion-tli id="T657" />
            <conversion-tli id="T658" />
            <conversion-tli id="T659" />
            <conversion-tli id="T660" />
            <conversion-tli id="T661" />
            <conversion-tli id="T662" />
            <conversion-tli id="T663" />
            <conversion-tli id="T664" />
            <conversion-tli id="T665" />
            <conversion-tli id="T666" />
            <conversion-tli id="T667" />
            <conversion-tli id="T668" />
            <conversion-tli id="T669" />
            <conversion-tli id="T670" />
            <conversion-tli id="T671" />
            <conversion-tli id="T672" />
            <conversion-tli id="T673" />
            <conversion-tli id="T674" />
            <conversion-tli id="T675" />
            <conversion-tli id="T676" />
            <conversion-tli id="T677" />
            <conversion-tli id="T678" />
            <conversion-tli id="T679" />
            <conversion-tli id="T680" />
            <conversion-tli id="T681" />
            <conversion-tli id="T682" />
            <conversion-tli id="T683" />
            <conversion-tli id="T684" />
            <conversion-tli id="T685" />
            <conversion-tli id="T686" />
            <conversion-tli id="T687" />
            <conversion-tli id="T688" />
            <conversion-tli id="T689" />
            <conversion-tli id="T690" />
            <conversion-tli id="T691" />
            <conversion-tli id="T692" />
            <conversion-tli id="T693" />
            <conversion-tli id="T694" />
            <conversion-tli id="T695" />
            <conversion-tli id="T696" />
            <conversion-tli id="T697" />
            <conversion-tli id="T698" />
            <conversion-tli id="T699" />
            <conversion-tli id="T700" />
            <conversion-tli id="T701" />
            <conversion-tli id="T702" />
            <conversion-tli id="T703" />
            <conversion-tli id="T704" />
            <conversion-tli id="T705" />
            <conversion-tli id="T706" />
            <conversion-tli id="T707" />
            <conversion-tli id="T708" />
            <conversion-tli id="T709" />
            <conversion-tli id="T710" />
            <conversion-tli id="T711" />
            <conversion-tli id="T712" />
            <conversion-tli id="T713" />
            <conversion-tli id="T714" />
            <conversion-tli id="T715" />
            <conversion-tli id="T716" />
            <conversion-tli id="T717" />
            <conversion-tli id="T718" />
            <conversion-tli id="T719" />
            <conversion-tli id="T720" />
            <conversion-tli id="T721" />
            <conversion-tli id="T722" />
            <conversion-tli id="T723" />
            <conversion-tli id="T724" />
            <conversion-tli id="T725" />
            <conversion-tli id="T726" />
            <conversion-tli id="T727" />
            <conversion-tli id="T728" />
            <conversion-tli id="T729" />
            <conversion-tli id="T730" />
            <conversion-tli id="T731" />
            <conversion-tli id="T732" />
            <conversion-tli id="T733" />
            <conversion-tli id="T734" />
            <conversion-tli id="T735" />
            <conversion-tli id="T736" />
            <conversion-tli id="T737" />
            <conversion-tli id="T738" />
            <conversion-tli id="T739" />
            <conversion-tli id="T740" />
            <conversion-tli id="T741" />
            <conversion-tli id="T742" />
            <conversion-tli id="T743" />
            <conversion-tli id="T744" />
            <conversion-tli id="T745" />
            <conversion-tli id="T746" />
            <conversion-tli id="T747" />
            <conversion-tli id="T748" />
            <conversion-tli id="T749" />
            <conversion-tli id="T750" />
            <conversion-tli id="T751" />
            <conversion-tli id="T752" />
            <conversion-tli id="T753" />
            <conversion-tli id="T754" />
            <conversion-tli id="T755" />
            <conversion-tli id="T756" />
            <conversion-tli id="T757" />
            <conversion-tli id="T758" />
            <conversion-tli id="T759" />
            <conversion-tli id="T760" />
            <conversion-tli id="T761" />
            <conversion-tli id="T762" />
            <conversion-tli id="T763" />
            <conversion-tli id="T764" />
            <conversion-tli id="T765" />
            <conversion-tli id="T766" />
            <conversion-tli id="T767" />
            <conversion-tli id="T768" />
            <conversion-tli id="T769" />
            <conversion-tli id="T770" />
            <conversion-tli id="T771" />
            <conversion-tli id="T772" />
            <conversion-tli id="T773" />
            <conversion-tli id="T774" />
            <conversion-tli id="T775" />
            <conversion-tli id="T776" />
            <conversion-tli id="T777" />
            <conversion-tli id="T778" />
            <conversion-tli id="T779" />
            <conversion-tli id="T780" />
            <conversion-tli id="T781" />
            <conversion-tli id="T782" />
            <conversion-tli id="T783" />
            <conversion-tli id="T784" />
            <conversion-tli id="T785" />
            <conversion-tli id="T786" />
            <conversion-tli id="T787" />
            <conversion-tli id="T788" />
            <conversion-tli id="T789" />
            <conversion-tli id="T790" />
            <conversion-tli id="T791" />
            <conversion-tli id="T792" />
            <conversion-tli id="T793" />
            <conversion-tli id="T794" />
            <conversion-tli id="T795" />
            <conversion-tli id="T796" />
            <conversion-tli id="T797" />
            <conversion-tli id="T798" />
            <conversion-tli id="T799" />
            <conversion-tli id="T800" />
            <conversion-tli id="T801" />
            <conversion-tli id="T802" />
            <conversion-tli id="T803" />
            <conversion-tli id="T804" />
            <conversion-tli id="T805" />
            <conversion-tli id="T806" />
            <conversion-tli id="T807" />
            <conversion-tli id="T808" />
            <conversion-tli id="T809" />
            <conversion-tli id="T810" />
            <conversion-tli id="T811" />
            <conversion-tli id="T812" />
            <conversion-tli id="T813" />
            <conversion-tli id="T814" />
            <conversion-tli id="T815" />
            <conversion-tli id="T816" />
            <conversion-tli id="T817" />
            <conversion-tli id="T818" />
            <conversion-tli id="T819" />
            <conversion-tli id="T820" />
            <conversion-tli id="T821" />
            <conversion-tli id="T822" />
            <conversion-tli id="T823" />
            <conversion-tli id="T824" />
            <conversion-tli id="T825" />
            <conversion-tli id="T826" />
            <conversion-tli id="T827" />
            <conversion-tli id="T828" />
            <conversion-tli id="T829" />
            <conversion-tli id="T830" />
            <conversion-tli id="T831" />
            <conversion-tli id="T832" />
            <conversion-tli id="T833" />
            <conversion-tli id="T834" />
            <conversion-tli id="T835" />
            <conversion-tli id="T836" />
            <conversion-tli id="T837" />
            <conversion-tli id="T838" />
            <conversion-tli id="T839" />
            <conversion-tli id="T840" />
            <conversion-tli id="T841" />
            <conversion-tli id="T842" />
            <conversion-tli id="T843" />
            <conversion-tli id="T844" />
            <conversion-tli id="T845" />
            <conversion-tli id="T846" />
            <conversion-tli id="T847" />
            <conversion-tli id="T848" />
            <conversion-tli id="T849" />
            <conversion-tli id="T850" />
            <conversion-tli id="T851" />
            <conversion-tli id="T852" />
            <conversion-tli id="T853" />
            <conversion-tli id="T854" />
            <conversion-tli id="T855" />
            <conversion-tli id="T856" />
            <conversion-tli id="T857" />
            <conversion-tli id="T858" />
            <conversion-tli id="T859" />
            <conversion-tli id="T860" />
            <conversion-tli id="T861" />
            <conversion-tli id="T862" />
            <conversion-tli id="T863" />
            <conversion-tli id="T864" />
            <conversion-tli id="T865" />
            <conversion-tli id="T866" />
            <conversion-tli id="T867" />
            <conversion-tli id="T868" />
            <conversion-tli id="T869" />
            <conversion-tli id="T870" />
            <conversion-tli id="T871" />
            <conversion-tli id="T872" />
            <conversion-tli id="T873" />
            <conversion-tli id="T874" />
            <conversion-tli id="T875" />
            <conversion-tli id="T876" />
            <conversion-tli id="T877" />
            <conversion-tli id="T878" />
            <conversion-tli id="T879" />
            <conversion-tli id="T880" />
            <conversion-tli id="T881" />
            <conversion-tli id="T882" />
            <conversion-tli id="T883" />
            <conversion-tli id="T884" />
            <conversion-tli id="T885" />
            <conversion-tli id="T886" />
            <conversion-tli id="T887" />
            <conversion-tli id="T888" />
            <conversion-tli id="T889" />
            <conversion-tli id="T890" />
            <conversion-tli id="T891" />
            <conversion-tli id="T892" />
            <conversion-tli id="T893" />
            <conversion-tli id="T894" />
            <conversion-tli id="T895" />
            <conversion-tli id="T896" />
            <conversion-tli id="T897" />
            <conversion-tli id="T898" />
            <conversion-tli id="T899" />
            <conversion-tli id="T900" />
            <conversion-tli id="T901" />
            <conversion-tli id="T902" />
            <conversion-tli id="T903" />
            <conversion-tli id="T904" />
            <conversion-tli id="T905" />
            <conversion-tli id="T906" />
            <conversion-tli id="T907" />
            <conversion-tli id="T908" />
            <conversion-tli id="T909" />
            <conversion-tli id="T910" />
            <conversion-tli id="T911" />
            <conversion-tli id="T912" />
            <conversion-tli id="T913" />
            <conversion-tli id="T914" />
            <conversion-tli id="T915" />
            <conversion-tli id="T916" />
            <conversion-tli id="T917" />
            <conversion-tli id="T918" />
            <conversion-tli id="T919" />
            <conversion-tli id="T920" />
            <conversion-tli id="T921" />
            <conversion-tli id="T922" />
            <conversion-tli id="T923" />
            <conversion-tli id="T924" />
            <conversion-tli id="T925" />
            <conversion-tli id="T926" />
            <conversion-tli id="T927" />
            <conversion-tli id="T928" />
            <conversion-tli id="T929" />
            <conversion-tli id="T930" />
            <conversion-tli id="T931" />
            <conversion-tli id="T932" />
            <conversion-tli id="T933" />
            <conversion-tli id="T934" />
            <conversion-tli id="T935" />
            <conversion-tli id="T936" />
            <conversion-tli id="T937" />
            <conversion-tli id="T938" />
            <conversion-tli id="T939" />
            <conversion-tli id="T940" />
            <conversion-tli id="T941" />
            <conversion-tli id="T942" />
            <conversion-tli id="T943" />
            <conversion-tli id="T944" />
            <conversion-tli id="T945" />
            <conversion-tli id="T946" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg"
                          name="gg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top"
                          name="Top"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc"
                          name="Foc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
