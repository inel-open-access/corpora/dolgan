<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID701B8C3E-592A-5A53-9612-879B3E292907">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>YeSV_1964_WarPartridgesPikes_flk</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\flk\ErSV_1964_WarPartridgesPikes_flk\ErSV_1964_WarPartridgesPikes_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">466</ud-information>
            <ud-information attribute-name="# HIAT:w">346</ud-information>
            <ud-information attribute-name="# e">346</ud-information>
            <ud-information attribute-name="# HIAT:u">59</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="YeSV">
            <abbreviation>ErSV</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="4.0" type="appl" />
         <tli id="T1" time="4.5" type="appl" />
         <tli id="T2" time="5.0" type="appl" />
         <tli id="T3" time="5.5" type="appl" />
         <tli id="T4" time="6.0" type="appl" />
         <tli id="T5" time="6.5" type="appl" />
         <tli id="T6" time="7.0" type="appl" />
         <tli id="T7" time="7.5" type="appl" />
         <tli id="T8" time="8.0" type="appl" />
         <tli id="T9" time="8.5" type="appl" />
         <tli id="T10" time="9.0" type="appl" />
         <tli id="T11" time="9.5" type="appl" />
         <tli id="T12" time="10.0" type="appl" />
         <tli id="T13" time="10.5" type="appl" />
         <tli id="T14" time="11.0" type="appl" />
         <tli id="T15" time="11.5" type="appl" />
         <tli id="T16" time="12.0" type="appl" />
         <tli id="T17" time="12.5" type="appl" />
         <tli id="T18" time="13.0" type="appl" />
         <tli id="T19" time="13.5" type="appl" />
         <tli id="T20" time="14.0" type="appl" />
         <tli id="T21" time="14.5" type="appl" />
         <tli id="T22" time="15.0" type="appl" />
         <tli id="T23" time="15.5" type="appl" />
         <tli id="T24" time="16.0" type="appl" />
         <tli id="T25" time="16.5" type="appl" />
         <tli id="T26" time="17.0" type="appl" />
         <tli id="T27" time="17.5" type="appl" />
         <tli id="T28" time="18.0" type="appl" />
         <tli id="T29" time="18.5" type="appl" />
         <tli id="T30" time="19.0" type="appl" />
         <tli id="T31" time="19.5" type="appl" />
         <tli id="T32" time="20.0" type="appl" />
         <tli id="T33" time="20.5" type="appl" />
         <tli id="T34" time="21.0" type="appl" />
         <tli id="T35" time="21.5" type="appl" />
         <tli id="T36" time="22.0" type="appl" />
         <tli id="T37" time="22.5" type="appl" />
         <tli id="T38" time="23.0" type="appl" />
         <tli id="T39" time="23.5" type="appl" />
         <tli id="T40" time="24.0" type="appl" />
         <tli id="T41" time="24.5" type="appl" />
         <tli id="T42" time="25.0" type="appl" />
         <tli id="T43" time="25.5" type="appl" />
         <tli id="T44" time="26.0" type="appl" />
         <tli id="T45" time="26.5" type="appl" />
         <tli id="T46" time="27.0" type="appl" />
         <tli id="T47" time="27.5" type="appl" />
         <tli id="T48" time="28.0" type="appl" />
         <tli id="T49" time="28.5" type="appl" />
         <tli id="T50" time="29.0" type="appl" />
         <tli id="T51" time="29.5" type="appl" />
         <tli id="T52" time="30.0" type="appl" />
         <tli id="T53" time="30.5" type="appl" />
         <tli id="T54" time="31.0" type="appl" />
         <tli id="T55" time="31.5" type="appl" />
         <tli id="T56" time="32.0" type="appl" />
         <tli id="T57" time="32.5" type="appl" />
         <tli id="T58" time="33.0" type="appl" />
         <tli id="T59" time="33.5" type="appl" />
         <tli id="T60" time="34.0" type="appl" />
         <tli id="T61" time="34.5" type="appl" />
         <tli id="T62" time="35.0" type="appl" />
         <tli id="T63" time="35.5" type="appl" />
         <tli id="T64" time="36.0" type="appl" />
         <tli id="T65" time="36.5" type="appl" />
         <tli id="T66" time="37.0" type="appl" />
         <tli id="T67" time="37.5" type="appl" />
         <tli id="T68" time="38.0" type="appl" />
         <tli id="T69" time="38.5" type="appl" />
         <tli id="T70" time="39.0" type="appl" />
         <tli id="T71" time="39.5" type="appl" />
         <tli id="T72" time="40.0" type="appl" />
         <tli id="T73" time="40.5" type="appl" />
         <tli id="T74" time="41.0" type="appl" />
         <tli id="T75" time="41.5" type="appl" />
         <tli id="T76" time="42.0" type="appl" />
         <tli id="T77" time="42.5" type="appl" />
         <tli id="T78" time="43.0" type="appl" />
         <tli id="T79" time="43.5" type="appl" />
         <tli id="T80" time="44.0" type="appl" />
         <tli id="T81" time="44.5" type="appl" />
         <tli id="T82" time="45.0" type="appl" />
         <tli id="T83" time="45.5" type="appl" />
         <tli id="T84" time="46.0" type="appl" />
         <tli id="T85" time="46.5" type="appl" />
         <tli id="T86" time="47.0" type="appl" />
         <tli id="T87" time="47.5" type="appl" />
         <tli id="T88" time="48.0" type="appl" />
         <tli id="T89" time="48.5" type="appl" />
         <tli id="T90" time="49.0" type="appl" />
         <tli id="T91" time="49.5" type="appl" />
         <tli id="T92" time="50.0" type="appl" />
         <tli id="T93" time="50.5" type="appl" />
         <tli id="T94" time="51.0" type="appl" />
         <tli id="T95" time="51.5" type="appl" />
         <tli id="T96" time="52.0" type="appl" />
         <tli id="T97" time="52.5" type="appl" />
         <tli id="T98" time="53.0" type="appl" />
         <tli id="T99" time="53.5" type="appl" />
         <tli id="T100" time="54.0" type="appl" />
         <tli id="T101" time="54.5" type="appl" />
         <tli id="T102" time="55.0" type="appl" />
         <tli id="T103" time="55.5" type="appl" />
         <tli id="T104" time="56.0" type="appl" />
         <tli id="T105" time="56.5" type="appl" />
         <tli id="T106" time="57.0" type="appl" />
         <tli id="T107" time="57.5" type="appl" />
         <tli id="T108" time="58.0" type="appl" />
         <tli id="T109" time="58.5" type="appl" />
         <tli id="T110" time="59.0" type="appl" />
         <tli id="T111" time="59.5" type="appl" />
         <tli id="T112" time="60.0" type="appl" />
         <tli id="T113" time="60.5" type="appl" />
         <tli id="T114" time="61.0" type="appl" />
         <tli id="T115" time="61.5" type="appl" />
         <tli id="T116" time="62.0" type="appl" />
         <tli id="T117" time="62.5" type="appl" />
         <tli id="T118" time="63.0" type="appl" />
         <tli id="T119" time="63.5" type="appl" />
         <tli id="T120" time="64.0" type="appl" />
         <tli id="T121" time="64.5" type="appl" />
         <tli id="T122" time="65.0" type="appl" />
         <tli id="T123" time="65.5" type="appl" />
         <tli id="T124" time="66.0" type="appl" />
         <tli id="T125" time="66.5" type="appl" />
         <tli id="T126" time="67.0" type="appl" />
         <tli id="T127" time="67.5" type="appl" />
         <tli id="T128" time="68.0" type="appl" />
         <tli id="T129" time="68.5" type="appl" />
         <tli id="T130" time="69.0" type="appl" />
         <tli id="T131" time="69.5" type="appl" />
         <tli id="T132" time="70.0" type="appl" />
         <tli id="T133" time="70.5" type="appl" />
         <tli id="T134" time="71.0" type="appl" />
         <tli id="T135" time="71.5" type="appl" />
         <tli id="T136" time="72.0" type="appl" />
         <tli id="T137" time="72.5" type="appl" />
         <tli id="T138" time="73.0" type="appl" />
         <tli id="T139" time="73.5" type="appl" />
         <tli id="T140" time="74.0" type="appl" />
         <tli id="T141" time="74.5" type="appl" />
         <tli id="T142" time="75.0" type="appl" />
         <tli id="T143" time="75.5" type="appl" />
         <tli id="T144" time="76.0" type="appl" />
         <tli id="T145" time="76.5" type="appl" />
         <tli id="T146" time="77.0" type="appl" />
         <tli id="T147" time="77.5" type="appl" />
         <tli id="T148" time="78.0" type="appl" />
         <tli id="T149" time="78.5" type="appl" />
         <tli id="T150" time="79.0" type="appl" />
         <tli id="T151" time="79.5" type="appl" />
         <tli id="T152" time="80.0" type="appl" />
         <tli id="T153" time="80.5" type="appl" />
         <tli id="T154" time="81.0" type="appl" />
         <tli id="T155" time="81.5" type="appl" />
         <tli id="T156" time="82.0" type="appl" />
         <tli id="T157" time="82.5" type="appl" />
         <tli id="T158" time="83.0" type="appl" />
         <tli id="T159" time="83.5" type="appl" />
         <tli id="T160" time="84.0" type="appl" />
         <tli id="T161" time="84.5" type="appl" />
         <tli id="T162" time="85.0" type="appl" />
         <tli id="T163" time="85.5" type="appl" />
         <tli id="T164" time="86.0" type="appl" />
         <tli id="T165" time="86.5" type="appl" />
         <tli id="T166" time="87.0" type="appl" />
         <tli id="T167" time="87.5" type="appl" />
         <tli id="T168" time="88.0" type="appl" />
         <tli id="T169" time="88.5" type="appl" />
         <tli id="T170" time="89.0" type="appl" />
         <tli id="T171" time="89.5" type="appl" />
         <tli id="T172" time="90.0" type="appl" />
         <tli id="T173" time="90.5" type="appl" />
         <tli id="T174" time="91.0" type="appl" />
         <tli id="T175" time="91.5" type="appl" />
         <tli id="T176" time="92.0" type="appl" />
         <tli id="T177" time="92.5" type="appl" />
         <tli id="T178" time="93.0" type="appl" />
         <tli id="T179" time="93.5" type="appl" />
         <tli id="T180" time="94.0" type="appl" />
         <tli id="T181" time="94.5" type="appl" />
         <tli id="T182" time="95.0" type="appl" />
         <tli id="T183" time="95.5" type="appl" />
         <tli id="T184" time="96.0" type="appl" />
         <tli id="T185" time="96.5" type="appl" />
         <tli id="T186" time="97.0" type="appl" />
         <tli id="T187" time="97.5" type="appl" />
         <tli id="T188" time="98.0" type="appl" />
         <tli id="T189" time="98.5" type="appl" />
         <tli id="T190" time="99.0" type="appl" />
         <tli id="T191" time="99.5" type="appl" />
         <tli id="T192" time="100.0" type="appl" />
         <tli id="T193" time="100.5" type="appl" />
         <tli id="T194" time="101.0" type="appl" />
         <tli id="T195" time="101.5" type="appl" />
         <tli id="T196" time="102.0" type="appl" />
         <tli id="T197" time="102.5" type="appl" />
         <tli id="T198" time="103.0" type="appl" />
         <tli id="T199" time="103.5" type="appl" />
         <tli id="T200" time="104.0" type="appl" />
         <tli id="T201" time="104.5" type="appl" />
         <tli id="T202" time="105.0" type="appl" />
         <tli id="T203" time="105.5" type="appl" />
         <tli id="T204" time="106.0" type="appl" />
         <tli id="T205" time="106.5" type="appl" />
         <tli id="T206" time="107.0" type="appl" />
         <tli id="T207" time="107.5" type="appl" />
         <tli id="T208" time="108.0" type="appl" />
         <tli id="T209" time="108.5" type="appl" />
         <tli id="T210" time="109.0" type="appl" />
         <tli id="T211" time="109.5" type="appl" />
         <tli id="T212" time="110.0" type="appl" />
         <tli id="T213" time="110.5" type="appl" />
         <tli id="T214" time="111.0" type="appl" />
         <tli id="T215" time="111.5" type="appl" />
         <tli id="T216" time="112.0" type="appl" />
         <tli id="T217" time="112.5" type="appl" />
         <tli id="T218" time="113.0" type="appl" />
         <tli id="T219" time="113.5" type="appl" />
         <tli id="T220" time="114.0" type="appl" />
         <tli id="T221" time="114.5" type="appl" />
         <tli id="T222" time="115.0" type="appl" />
         <tli id="T223" time="115.5" type="appl" />
         <tli id="T224" time="116.0" type="appl" />
         <tli id="T225" time="116.5" type="appl" />
         <tli id="T226" time="117.0" type="appl" />
         <tli id="T227" time="117.5" type="appl" />
         <tli id="T228" time="118.0" type="appl" />
         <tli id="T229" time="118.5" type="appl" />
         <tli id="T230" time="119.0" type="appl" />
         <tli id="T231" time="119.5" type="appl" />
         <tli id="T232" time="120.0" type="appl" />
         <tli id="T233" time="120.5" type="appl" />
         <tli id="T234" time="121.0" type="appl" />
         <tli id="T235" time="121.5" type="appl" />
         <tli id="T236" time="122.0" type="appl" />
         <tli id="T237" time="122.5" type="appl" />
         <tli id="T238" time="123.0" type="appl" />
         <tli id="T239" time="123.5" type="appl" />
         <tli id="T240" time="124.0" type="appl" />
         <tli id="T241" time="124.5" type="appl" />
         <tli id="T242" time="125.0" type="appl" />
         <tli id="T243" time="125.5" type="appl" />
         <tli id="T244" time="126.0" type="appl" />
         <tli id="T245" time="126.5" type="appl" />
         <tli id="T246" time="127.0" type="appl" />
         <tli id="T247" time="127.5" type="appl" />
         <tli id="T248" time="128.0" type="appl" />
         <tli id="T249" time="128.5" type="appl" />
         <tli id="T250" time="129.0" type="appl" />
         <tli id="T251" time="129.5" type="appl" />
         <tli id="T252" time="130.0" type="appl" />
         <tli id="T253" time="130.5" type="appl" />
         <tli id="T254" time="131.0" type="appl" />
         <tli id="T255" time="131.5" type="appl" />
         <tli id="T256" time="132.0" type="appl" />
         <tli id="T257" time="132.5" type="appl" />
         <tli id="T258" time="133.0" type="appl" />
         <tli id="T259" time="133.5" type="appl" />
         <tli id="T260" time="134.0" type="appl" />
         <tli id="T261" time="134.5" type="appl" />
         <tli id="T262" time="135.0" type="appl" />
         <tli id="T263" time="135.5" type="appl" />
         <tli id="T264" time="136.0" type="appl" />
         <tli id="T265" time="136.5" type="appl" />
         <tli id="T266" time="137.0" type="appl" />
         <tli id="T267" time="137.5" type="appl" />
         <tli id="T268" time="138.0" type="appl" />
         <tli id="T269" time="138.5" type="appl" />
         <tli id="T270" time="139.0" type="appl" />
         <tli id="T271" time="139.5" type="appl" />
         <tli id="T272" time="140.0" type="appl" />
         <tli id="T273" time="140.5" type="appl" />
         <tli id="T274" time="141.0" type="appl" />
         <tli id="T275" time="141.5" type="appl" />
         <tli id="T276" time="142.0" type="appl" />
         <tli id="T277" time="142.5" type="appl" />
         <tli id="T278" time="143.0" type="appl" />
         <tli id="T279" time="143.5" type="appl" />
         <tli id="T280" time="144.0" type="appl" />
         <tli id="T281" time="144.5" type="appl" />
         <tli id="T282" time="145.0" type="appl" />
         <tli id="T283" time="145.5" type="appl" />
         <tli id="T284" time="146.0" type="appl" />
         <tli id="T285" time="146.5" type="appl" />
         <tli id="T286" time="147.0" type="appl" />
         <tli id="T287" time="147.5" type="appl" />
         <tli id="T288" time="148.0" type="appl" />
         <tli id="T289" time="148.5" type="appl" />
         <tli id="T290" time="149.0" type="appl" />
         <tli id="T291" time="149.5" type="appl" />
         <tli id="T292" time="150.0" type="appl" />
         <tli id="T293" time="150.5" type="appl" />
         <tli id="T294" time="151.0" type="appl" />
         <tli id="T295" time="151.5" type="appl" />
         <tli id="T296" time="152.0" type="appl" />
         <tli id="T297" time="152.5" type="appl" />
         <tli id="T346" time="152.675" type="intp" />
         <tli id="T298" time="153.0" type="appl" />
         <tli id="T299" time="153.5" type="appl" />
         <tli id="T300" time="154.0" type="appl" />
         <tli id="T301" time="154.5" type="appl" />
         <tli id="T302" time="155.0" type="appl" />
         <tli id="T303" time="155.5" type="appl" />
         <tli id="T304" time="156.0" type="appl" />
         <tli id="T305" time="156.5" type="appl" />
         <tli id="T306" time="157.0" type="appl" />
         <tli id="T307" time="157.5" type="appl" />
         <tli id="T308" time="158.0" type="appl" />
         <tli id="T309" time="158.5" type="appl" />
         <tli id="T310" time="159.0" type="appl" />
         <tli id="T311" time="159.5" type="appl" />
         <tli id="T312" time="160.0" type="appl" />
         <tli id="T313" time="160.5" type="appl" />
         <tli id="T314" time="161.0" type="appl" />
         <tli id="T315" time="161.5" type="appl" />
         <tli id="T316" time="162.0" type="appl" />
         <tli id="T317" time="162.5" type="appl" />
         <tli id="T318" time="163.0" type="appl" />
         <tli id="T319" time="163.5" type="appl" />
         <tli id="T320" time="164.0" type="appl" />
         <tli id="T321" time="164.5" type="appl" />
         <tli id="T322" time="165.0" type="appl" />
         <tli id="T323" time="165.5" type="appl" />
         <tli id="T324" time="166.0" type="appl" />
         <tli id="T325" time="166.5" type="appl" />
         <tli id="T326" time="167.0" type="appl" />
         <tli id="T327" time="167.5" type="appl" />
         <tli id="T328" time="168.0" type="appl" />
         <tli id="T329" time="168.5" type="appl" />
         <tli id="T330" time="169.0" type="appl" />
         <tli id="T331" time="169.5" type="appl" />
         <tli id="T332" time="170.0" type="appl" />
         <tli id="T333" time="170.5" type="appl" />
         <tli id="T334" time="171.0" type="appl" />
         <tli id="T335" time="171.5" type="appl" />
         <tli id="T336" time="172.0" type="appl" />
         <tli id="T337" time="172.5" type="appl" />
         <tli id="T338" time="173.0" type="appl" />
         <tli id="T339" time="173.5" type="appl" />
         <tli id="T340" time="174.0" type="appl" />
         <tli id="T341" time="174.5" type="appl" />
         <tli id="T342" time="175.0" type="appl" />
         <tli id="T343" time="175.5" type="appl" />
         <tli id="T344" time="176.0" type="appl" />
         <tli id="T345" time="176.5" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="YeSV"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T345" id="Seg_0" n="sc" s="T0">
               <ts e="T8" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Dʼe</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">bɨlɨr</ts>
                  <nts id="Seg_8" n="HIAT:ip">,</nts>
                  <nts id="Seg_9" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_11" n="HIAT:w" s="T2">hir-taŋara</ts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_14" n="HIAT:w" s="T3">ü͡ösküːrüger</ts>
                  <nts id="Seg_15" n="HIAT:ip">,</nts>
                  <nts id="Seg_16" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_18" n="HIAT:w" s="T4">kurpaːskɨ</ts>
                  <nts id="Seg_19" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_21" n="HIAT:w" s="T5">mu͡ora</ts>
                  <nts id="Seg_22" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_24" n="HIAT:w" s="T6">hirdeːge</ts>
                  <nts id="Seg_25" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_27" n="HIAT:w" s="T7">ühü</ts>
                  <nts id="Seg_28" n="HIAT:ip">.</nts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T12" id="Seg_31" n="HIAT:u" s="T8">
                  <ts e="T9" id="Seg_33" n="HIAT:w" s="T8">Biːr</ts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_36" n="HIAT:w" s="T9">dʼɨl</ts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_39" n="HIAT:w" s="T10">purgaːlaːk</ts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_42" n="HIAT:w" s="T11">bu͡olbut</ts>
                  <nts id="Seg_43" n="HIAT:ip">.</nts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T18" id="Seg_46" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_48" n="HIAT:w" s="T12">Kɨhɨn</ts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_51" n="HIAT:w" s="T13">hamɨːrdaːbɨt</ts>
                  <nts id="Seg_52" n="HIAT:ip">,</nts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_55" n="HIAT:w" s="T14">kaːra</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_58" n="HIAT:w" s="T15">barɨta</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_61" n="HIAT:w" s="T16">buːs</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_64" n="HIAT:w" s="T17">bu͡olbut</ts>
                  <nts id="Seg_65" n="HIAT:ip">.</nts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T23" id="Seg_68" n="HIAT:u" s="T18">
                  <ts e="T19" id="Seg_70" n="HIAT:w" s="T18">Biːr</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_73" n="HIAT:w" s="T19">da</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_76" n="HIAT:w" s="T20">kadʼɨrɨk</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_79" n="HIAT:w" s="T21">öttüte</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_82" n="HIAT:w" s="T22">hu͡ok</ts>
                  <nts id="Seg_83" n="HIAT:ip">.</nts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T31" id="Seg_86" n="HIAT:u" s="T23">
                  <ts e="T24" id="Seg_88" n="HIAT:w" s="T23">Kurpaːskɨlar</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_91" n="HIAT:w" s="T24">korgujbuttar</ts>
                  <nts id="Seg_92" n="HIAT:ip">,</nts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_95" n="HIAT:w" s="T25">giniler</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_98" n="HIAT:w" s="T26">as</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_101" n="HIAT:w" s="T27">kördönö</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_104" n="HIAT:w" s="T28">mas</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_107" n="HIAT:w" s="T29">di͡ekki</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_110" n="HIAT:w" s="T30">köppütter</ts>
                  <nts id="Seg_111" n="HIAT:ip">.</nts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T36" id="Seg_114" n="HIAT:u" s="T31">
                  <ts e="T32" id="Seg_116" n="HIAT:w" s="T31">Biːr</ts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_119" n="HIAT:w" s="T32">muŋ</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_122" n="HIAT:w" s="T33">kɨrdʼagas</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_125" n="HIAT:w" s="T34">hübehit</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_128" n="HIAT:w" s="T35">kurpaːskɨlaːktar</ts>
                  <nts id="Seg_129" n="HIAT:ip">.</nts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T51" id="Seg_132" n="HIAT:u" s="T36">
                  <ts e="T37" id="Seg_134" n="HIAT:w" s="T36">Maska</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_137" n="HIAT:w" s="T37">tijbitter</ts>
                  <nts id="Seg_138" n="HIAT:ip">,</nts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_141" n="HIAT:w" s="T38">kanna</ts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_144" n="HIAT:w" s="T39">da</ts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_147" n="HIAT:w" s="T40">kadʼɨrɨk</ts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_150" n="HIAT:w" s="T41">köstübet</ts>
                  <nts id="Seg_151" n="HIAT:ip">,</nts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_154" n="HIAT:w" s="T42">talak</ts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_157" n="HIAT:w" s="T43">töbötö</ts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_160" n="HIAT:w" s="T44">da</ts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_163" n="HIAT:w" s="T45">billibet</ts>
                  <nts id="Seg_164" n="HIAT:ip">,</nts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_167" n="HIAT:w" s="T46">tiben</ts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_170" n="HIAT:w" s="T47">keːspit</ts>
                  <nts id="Seg_171" n="HIAT:ip">,</nts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_174" n="HIAT:w" s="T48">mas</ts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_177" n="HIAT:w" s="T49">agaj</ts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_180" n="HIAT:w" s="T50">karaːrar</ts>
                  <nts id="Seg_181" n="HIAT:ip">.</nts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T53" id="Seg_184" n="HIAT:u" s="T51">
                  <ts e="T52" id="Seg_186" n="HIAT:w" s="T51">Hübehittere</ts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_189" n="HIAT:w" s="T52">hübeliːr</ts>
                  <nts id="Seg_190" n="HIAT:ip">:</nts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T57" id="Seg_193" n="HIAT:u" s="T53">
                  <nts id="Seg_194" n="HIAT:ip">"</nts>
                  <ts e="T54" id="Seg_196" n="HIAT:w" s="T53">Dʼe</ts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_199" n="HIAT:w" s="T54">anɨ</ts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_202" n="HIAT:w" s="T55">kihitijbetibit</ts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_205" n="HIAT:w" s="T56">bihigi</ts>
                  <nts id="Seg_206" n="HIAT:ip">.</nts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T61" id="Seg_209" n="HIAT:u" s="T57">
                  <ts e="T58" id="Seg_211" n="HIAT:w" s="T57">Kanna</ts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_214" n="HIAT:w" s="T58">baran</ts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_217" n="HIAT:w" s="T59">öl-ü͡ökpütüj</ts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_220" n="HIAT:w" s="T60">bihigi</ts>
                  <nts id="Seg_221" n="HIAT:ip">?</nts>
                  <nts id="Seg_222" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T63" id="Seg_224" n="HIAT:u" s="T61">
                  <ts e="T62" id="Seg_226" n="HIAT:w" s="T61">Bugurduk</ts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_229" n="HIAT:w" s="T62">duːmalɨːbɨn</ts>
                  <nts id="Seg_230" n="HIAT:ip">.</nts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T73" id="Seg_233" n="HIAT:u" s="T63">
                  <ts e="T64" id="Seg_235" n="HIAT:w" s="T63">Kannɨk</ts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_238" n="HIAT:w" s="T64">ere</ts>
                  <nts id="Seg_239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_241" n="HIAT:w" s="T65">üjege</ts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_244" n="HIAT:w" s="T66">biːr</ts>
                  <nts id="Seg_245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_247" n="HIAT:w" s="T67">hiri</ts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_250" n="HIAT:w" s="T68">körbütteːkpin</ts>
                  <nts id="Seg_251" n="HIAT:ip">,</nts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_254" n="HIAT:w" s="T69">biːr</ts>
                  <nts id="Seg_255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_257" n="HIAT:w" s="T70">ulagan</ts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_260" n="HIAT:w" s="T71">ürek</ts>
                  <nts id="Seg_261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_263" n="HIAT:w" s="T72">baːr</ts>
                  <nts id="Seg_264" n="HIAT:ip">.</nts>
                  <nts id="Seg_265" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T79" id="Seg_267" n="HIAT:u" s="T73">
                  <ts e="T74" id="Seg_269" n="HIAT:w" s="T73">Ol</ts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_272" n="HIAT:w" s="T74">üregiŋ</ts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_275" n="HIAT:w" s="T75">kajdaktaːk</ts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_278" n="HIAT:w" s="T76">purgaːga</ts>
                  <nts id="Seg_279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_281" n="HIAT:w" s="T77">tibilleːččite</ts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_284" n="HIAT:w" s="T78">hu͡ok</ts>
                  <nts id="Seg_285" n="HIAT:ip">.</nts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T85" id="Seg_288" n="HIAT:u" s="T79">
                  <ts e="T80" id="Seg_290" n="HIAT:w" s="T79">Horok</ts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_293" n="HIAT:w" s="T80">hire</ts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_296" n="HIAT:w" s="T81">kɨhɨnɨ</ts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_299" n="HIAT:w" s="T82">bɨha</ts>
                  <nts id="Seg_300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_302" n="HIAT:w" s="T83">hiːkej</ts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_305" n="HIAT:w" s="T84">bu͡olar</ts>
                  <nts id="Seg_306" n="HIAT:ip">.</nts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T92" id="Seg_309" n="HIAT:u" s="T85">
                  <ts e="T86" id="Seg_311" n="HIAT:w" s="T85">Kɨtɨlɨttan</ts>
                  <nts id="Seg_312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_314" n="HIAT:w" s="T86">talaktaːk</ts>
                  <nts id="Seg_315" n="HIAT:ip">,</nts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_318" n="HIAT:w" s="T87">kajata</ts>
                  <nts id="Seg_319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_321" n="HIAT:w" s="T88">barɨta</ts>
                  <nts id="Seg_322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_324" n="HIAT:w" s="T89">heppereːkteːk</ts>
                  <nts id="Seg_325" n="HIAT:ip">,</nts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_328" n="HIAT:w" s="T90">hugunnaːk</ts>
                  <nts id="Seg_329" n="HIAT:ip">,</nts>
                  <nts id="Seg_330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_332" n="HIAT:w" s="T91">ottoːk</ts>
                  <nts id="Seg_333" n="HIAT:ip">.</nts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T97" id="Seg_336" n="HIAT:u" s="T92">
                  <ts e="T93" id="Seg_338" n="HIAT:w" s="T92">Ol</ts>
                  <nts id="Seg_339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_341" n="HIAT:w" s="T93">ürekke</ts>
                  <nts id="Seg_342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_344" n="HIAT:w" s="T94">ku͡okaː</ts>
                  <nts id="Seg_345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_347" n="HIAT:w" s="T95">gi͡ene</ts>
                  <nts id="Seg_348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_350" n="HIAT:w" s="T96">ologo</ts>
                  <nts id="Seg_351" n="HIAT:ip">.</nts>
                  <nts id="Seg_352" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T103" id="Seg_354" n="HIAT:u" s="T97">
                  <ts e="T98" id="Seg_356" n="HIAT:w" s="T97">Hɨččak</ts>
                  <nts id="Seg_357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_359" n="HIAT:w" s="T98">ku͡okaːlar</ts>
                  <nts id="Seg_360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_362" n="HIAT:w" s="T99">agaj</ts>
                  <nts id="Seg_363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_365" n="HIAT:w" s="T100">bihigini</ts>
                  <nts id="Seg_366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_368" n="HIAT:w" s="T101">čugahatɨ͡aktara</ts>
                  <nts id="Seg_369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_371" n="HIAT:w" s="T102">hu͡oga</ts>
                  <nts id="Seg_372" n="HIAT:ip">.</nts>
                  <nts id="Seg_373" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T118" id="Seg_375" n="HIAT:u" s="T103">
                  <ts e="T104" id="Seg_377" n="HIAT:w" s="T103">Bɨlɨr</ts>
                  <nts id="Seg_378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_380" n="HIAT:w" s="T104">üjege</ts>
                  <nts id="Seg_381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_383" n="HIAT:w" s="T105">andagaj</ts>
                  <nts id="Seg_384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_386" n="HIAT:w" s="T106">tɨllaːktar</ts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_389" n="HIAT:w" s="T107">kurpaːskanɨ</ts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_392" n="HIAT:w" s="T108">kɨtta</ts>
                  <nts id="Seg_393" n="HIAT:ip">,</nts>
                  <nts id="Seg_394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_396" n="HIAT:w" s="T109">kurpaːskɨ</ts>
                  <nts id="Seg_397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_399" n="HIAT:w" s="T110">ku͡okaː</ts>
                  <nts id="Seg_400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_402" n="HIAT:w" s="T111">dojdutugar</ts>
                  <nts id="Seg_403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_405" n="HIAT:w" s="T112">barɨ͡a</ts>
                  <nts id="Seg_406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_408" n="HIAT:w" s="T113">hu͡oga</ts>
                  <nts id="Seg_409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_411" n="HIAT:w" s="T114">di͡en</ts>
                  <nts id="Seg_412" n="HIAT:ip">,</nts>
                  <nts id="Seg_413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_415" n="HIAT:w" s="T115">ku͡okaː</ts>
                  <nts id="Seg_416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_417" n="HIAT:ip">—</nts>
                  <nts id="Seg_418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_420" n="HIAT:w" s="T116">kurpaːskɨ</ts>
                  <nts id="Seg_421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_423" n="HIAT:w" s="T117">dojdutugar</ts>
                  <nts id="Seg_424" n="HIAT:ip">.</nts>
                  <nts id="Seg_425" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T123" id="Seg_427" n="HIAT:u" s="T118">
                  <ts e="T119" id="Seg_429" n="HIAT:w" s="T118">Öjdüːr</ts>
                  <nts id="Seg_430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_432" n="HIAT:w" s="T119">bu͡ollaktɨrɨna</ts>
                  <nts id="Seg_433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_435" n="HIAT:w" s="T120">ütü͡önnen</ts>
                  <nts id="Seg_436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_438" n="HIAT:w" s="T121">bi͡eri͡ektere</ts>
                  <nts id="Seg_439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_441" n="HIAT:w" s="T122">hu͡oga</ts>
                  <nts id="Seg_442" n="HIAT:ip">.</nts>
                  <nts id="Seg_443" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T128" id="Seg_445" n="HIAT:u" s="T123">
                  <ts e="T124" id="Seg_447" n="HIAT:w" s="T123">Bihigi</ts>
                  <nts id="Seg_448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_450" n="HIAT:w" s="T124">oččogo</ts>
                  <nts id="Seg_451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_453" n="HIAT:w" s="T125">heriːnnen</ts>
                  <nts id="Seg_454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_456" n="HIAT:w" s="T126">agaj</ts>
                  <nts id="Seg_457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_459" n="HIAT:w" s="T127">kiːrini͡ekpit</ts>
                  <nts id="Seg_460" n="HIAT:ip">.</nts>
                  <nts id="Seg_461" n="HIAT:ip">"</nts>
                  <nts id="Seg_462" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T129" id="Seg_464" n="HIAT:u" s="T128">
                  <ts e="T129" id="Seg_466" n="HIAT:w" s="T128">Kurpaːskɨlar</ts>
                  <nts id="Seg_467" n="HIAT:ip">:</nts>
                  <nts id="Seg_468" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T130" id="Seg_470" n="HIAT:u" s="T129">
                  <nts id="Seg_471" n="HIAT:ip">"</nts>
                  <ts e="T130" id="Seg_473" n="HIAT:w" s="T129">Kirdik</ts>
                  <nts id="Seg_474" n="HIAT:ip">.</nts>
                  <nts id="Seg_475" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T134" id="Seg_477" n="HIAT:u" s="T130">
                  <ts e="T131" id="Seg_479" n="HIAT:w" s="T130">Manna</ts>
                  <nts id="Seg_480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_482" n="HIAT:w" s="T131">olorbutunan</ts>
                  <nts id="Seg_483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_485" n="HIAT:w" s="T132">ölü͡ökpüt</ts>
                  <nts id="Seg_486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_488" n="HIAT:w" s="T133">du͡o</ts>
                  <nts id="Seg_489" n="HIAT:ip">?</nts>
                  <nts id="Seg_490" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T137" id="Seg_492" n="HIAT:u" s="T134">
                  <ts e="T135" id="Seg_494" n="HIAT:w" s="T134">Ol</ts>
                  <nts id="Seg_495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_497" n="HIAT:w" s="T135">keri͡etin</ts>
                  <nts id="Seg_498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_500" n="HIAT:w" s="T136">heriːlehi͡ekpit</ts>
                  <nts id="Seg_501" n="HIAT:ip">.</nts>
                  <nts id="Seg_502" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T139" id="Seg_504" n="HIAT:u" s="T137">
                  <ts e="T138" id="Seg_506" n="HIAT:w" s="T137">Barɨ͡agɨŋ</ts>
                  <nts id="Seg_507" n="HIAT:ip">"</nts>
                  <nts id="Seg_508" n="HIAT:ip">,</nts>
                  <nts id="Seg_509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_511" n="HIAT:w" s="T138">di͡etiler</ts>
                  <nts id="Seg_512" n="HIAT:ip">.</nts>
                  <nts id="Seg_513" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T143" id="Seg_515" n="HIAT:u" s="T139">
                  <ts e="T140" id="Seg_517" n="HIAT:w" s="T139">Kurpaːskɨlar</ts>
                  <nts id="Seg_518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_520" n="HIAT:w" s="T140">kötöllör</ts>
                  <nts id="Seg_521" n="HIAT:ip">,</nts>
                  <nts id="Seg_522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_524" n="HIAT:w" s="T141">ürekke</ts>
                  <nts id="Seg_525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_527" n="HIAT:w" s="T142">tijeller</ts>
                  <nts id="Seg_528" n="HIAT:ip">.</nts>
                  <nts id="Seg_529" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T148" id="Seg_531" n="HIAT:u" s="T143">
                  <ts e="T144" id="Seg_533" n="HIAT:w" s="T143">As</ts>
                  <nts id="Seg_534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_536" n="HIAT:w" s="T144">neleje</ts>
                  <nts id="Seg_537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_539" n="HIAT:w" s="T145">hɨtar</ts>
                  <nts id="Seg_540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_542" n="HIAT:w" s="T146">hire</ts>
                  <nts id="Seg_543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_545" n="HIAT:w" s="T147">ebit</ts>
                  <nts id="Seg_546" n="HIAT:ip">.</nts>
                  <nts id="Seg_547" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T150" id="Seg_549" n="HIAT:u" s="T148">
                  <ts e="T149" id="Seg_551" n="HIAT:w" s="T148">Ahɨː</ts>
                  <nts id="Seg_552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_554" n="HIAT:w" s="T149">olorbuttar</ts>
                  <nts id="Seg_555" n="HIAT:ip">.</nts>
                  <nts id="Seg_556" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T157" id="Seg_558" n="HIAT:u" s="T150">
                  <ts e="T151" id="Seg_560" n="HIAT:w" s="T150">Hiːkej</ts>
                  <nts id="Seg_561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_563" n="HIAT:w" s="T151">ustun</ts>
                  <nts id="Seg_564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_566" n="HIAT:w" s="T152">ku͡okaː</ts>
                  <nts id="Seg_567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_569" n="HIAT:w" s="T153">hübehitin</ts>
                  <nts id="Seg_570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_572" n="HIAT:w" s="T154">mejiːte</ts>
                  <nts id="Seg_573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_575" n="HIAT:w" s="T155">bɨltas</ts>
                  <nts id="Seg_576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_578" n="HIAT:w" s="T156">gɨmmɨt</ts>
                  <nts id="Seg_579" n="HIAT:ip">:</nts>
                  <nts id="Seg_580" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T162" id="Seg_582" n="HIAT:u" s="T157">
                  <nts id="Seg_583" n="HIAT:ip">"</nts>
                  <ts e="T158" id="Seg_585" n="HIAT:w" s="T157">Kaja-keː</ts>
                  <nts id="Seg_586" n="HIAT:ip">,</nts>
                  <nts id="Seg_587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_589" n="HIAT:w" s="T158">kurpaːskɨlar</ts>
                  <nts id="Seg_590" n="HIAT:ip">,</nts>
                  <nts id="Seg_591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_593" n="HIAT:w" s="T159">togo</ts>
                  <nts id="Seg_594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_596" n="HIAT:w" s="T160">hirge</ts>
                  <nts id="Seg_597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_599" n="HIAT:w" s="T161">kelbikkitij</ts>
                  <nts id="Seg_600" n="HIAT:ip">?</nts>
                  <nts id="Seg_601" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T174" id="Seg_603" n="HIAT:u" s="T162">
                  <ts e="T163" id="Seg_605" n="HIAT:w" s="T162">Bɨlɨr</ts>
                  <nts id="Seg_606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_608" n="HIAT:w" s="T163">üjege</ts>
                  <nts id="Seg_609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_611" n="HIAT:w" s="T164">andagaj</ts>
                  <nts id="Seg_612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_614" n="HIAT:w" s="T165">tɨllaːk</ts>
                  <nts id="Seg_615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_617" n="HIAT:w" s="T166">etibit</ts>
                  <nts id="Seg_618" n="HIAT:ip">,</nts>
                  <nts id="Seg_619" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_621" n="HIAT:w" s="T167">ehigi</ts>
                  <nts id="Seg_622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_624" n="HIAT:w" s="T168">hirgitiger</ts>
                  <nts id="Seg_625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_627" n="HIAT:w" s="T169">bihigi</ts>
                  <nts id="Seg_628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_630" n="HIAT:w" s="T170">barbappɨt</ts>
                  <nts id="Seg_631" n="HIAT:ip">,</nts>
                  <nts id="Seg_632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_634" n="HIAT:w" s="T171">ehigi</ts>
                  <nts id="Seg_635" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_636" n="HIAT:ip">—</nts>
                  <nts id="Seg_637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_639" n="HIAT:w" s="T172">bihigi</ts>
                  <nts id="Seg_640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_642" n="HIAT:w" s="T173">hirbitiger</ts>
                  <nts id="Seg_643" n="HIAT:ip">.</nts>
                  <nts id="Seg_644" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T180" id="Seg_646" n="HIAT:u" s="T174">
                  <ts e="T175" id="Seg_648" n="HIAT:w" s="T174">Togo</ts>
                  <nts id="Seg_649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_651" n="HIAT:w" s="T175">kelligit</ts>
                  <nts id="Seg_652" n="HIAT:ip">,</nts>
                  <nts id="Seg_653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_655" n="HIAT:w" s="T176">köstümeŋ</ts>
                  <nts id="Seg_656" n="HIAT:ip">,</nts>
                  <nts id="Seg_657" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_659" n="HIAT:w" s="T177">hanɨkaːn</ts>
                  <nts id="Seg_660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_662" n="HIAT:w" s="T178">barɨŋ</ts>
                  <nts id="Seg_663" n="HIAT:ip">,</nts>
                  <nts id="Seg_664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_666" n="HIAT:w" s="T179">mantan</ts>
                  <nts id="Seg_667" n="HIAT:ip">!</nts>
                  <nts id="Seg_668" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T196" id="Seg_670" n="HIAT:u" s="T180">
                  <ts e="T181" id="Seg_672" n="HIAT:w" s="T180">Talak</ts>
                  <nts id="Seg_673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_675" n="HIAT:w" s="T181">gi͡enin</ts>
                  <nts id="Seg_676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_678" n="HIAT:w" s="T182">hebirdegin</ts>
                  <nts id="Seg_679" n="HIAT:ip">,</nts>
                  <nts id="Seg_680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_682" n="HIAT:w" s="T183">heppereːk</ts>
                  <nts id="Seg_683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_685" n="HIAT:w" s="T184">gi͡enin</ts>
                  <nts id="Seg_686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_688" n="HIAT:w" s="T185">töbötün</ts>
                  <nts id="Seg_689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_691" n="HIAT:w" s="T186">da</ts>
                  <nts id="Seg_692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_694" n="HIAT:w" s="T187">hi͡eti͡em</ts>
                  <nts id="Seg_695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_697" n="HIAT:w" s="T188">hu͡oga</ts>
                  <nts id="Seg_698" n="HIAT:ip">,</nts>
                  <nts id="Seg_699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_701" n="HIAT:w" s="T189">oppun</ts>
                  <nts id="Seg_702" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_704" n="HIAT:w" s="T190">da</ts>
                  <nts id="Seg_705" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_707" n="HIAT:w" s="T191">hi͡eppeppin</ts>
                  <nts id="Seg_708" n="HIAT:ip">,</nts>
                  <nts id="Seg_709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_711" n="HIAT:w" s="T192">čajbɨn</ts>
                  <nts id="Seg_712" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_714" n="HIAT:w" s="T193">da</ts>
                  <nts id="Seg_715" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_717" n="HIAT:w" s="T194">kastarɨ͡am</ts>
                  <nts id="Seg_718" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_720" n="HIAT:w" s="T195">hu͡oga</ts>
                  <nts id="Seg_721" n="HIAT:ip">!</nts>
                  <nts id="Seg_722" n="HIAT:ip">"</nts>
                  <nts id="Seg_723" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T200" id="Seg_725" n="HIAT:u" s="T196">
                  <ts e="T197" id="Seg_727" n="HIAT:w" s="T196">Onuga</ts>
                  <nts id="Seg_728" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_730" n="HIAT:w" s="T197">kurpaːsku</ts>
                  <nts id="Seg_731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_733" n="HIAT:w" s="T198">hübehite</ts>
                  <nts id="Seg_734" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_736" n="HIAT:w" s="T199">diːr</ts>
                  <nts id="Seg_737" n="HIAT:ip">:</nts>
                  <nts id="Seg_738" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T203" id="Seg_740" n="HIAT:u" s="T200">
                  <nts id="Seg_741" n="HIAT:ip">"</nts>
                  <ts e="T201" id="Seg_743" n="HIAT:w" s="T200">Bihigi</ts>
                  <nts id="Seg_744" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_746" n="HIAT:w" s="T201">ütü͡önnen</ts>
                  <nts id="Seg_747" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_749" n="HIAT:w" s="T202">barbappɨt</ts>
                  <nts id="Seg_750" n="HIAT:ip">.</nts>
                  <nts id="Seg_751" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T208" id="Seg_753" n="HIAT:u" s="T203">
                  <ts e="T204" id="Seg_755" n="HIAT:w" s="T203">Heriːnnen</ts>
                  <nts id="Seg_756" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_758" n="HIAT:w" s="T204">daːganɨ</ts>
                  <nts id="Seg_759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_761" n="HIAT:w" s="T205">bu</ts>
                  <nts id="Seg_762" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_764" n="HIAT:w" s="T206">hiri</ts>
                  <nts id="Seg_765" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_767" n="HIAT:w" s="T207">ɨlɨ͡akpɨt</ts>
                  <nts id="Seg_768" n="HIAT:ip">!</nts>
                  <nts id="Seg_769" n="HIAT:ip">"</nts>
                  <nts id="Seg_770" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T210" id="Seg_772" n="HIAT:u" s="T208">
                  <ts e="T209" id="Seg_774" n="HIAT:w" s="T208">Ku͡okaː</ts>
                  <nts id="Seg_775" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_777" n="HIAT:w" s="T209">hübehite</ts>
                  <nts id="Seg_778" n="HIAT:ip">:</nts>
                  <nts id="Seg_779" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T213" id="Seg_781" n="HIAT:u" s="T210">
                  <nts id="Seg_782" n="HIAT:ip">"</nts>
                  <ts e="T211" id="Seg_784" n="HIAT:w" s="T210">Min</ts>
                  <nts id="Seg_785" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_787" n="HIAT:w" s="T211">ütü͡önnen</ts>
                  <nts id="Seg_788" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_790" n="HIAT:w" s="T212">bi͡erbeppin</ts>
                  <nts id="Seg_791" n="HIAT:ip">.</nts>
                  <nts id="Seg_792" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T217" id="Seg_794" n="HIAT:u" s="T213">
                  <ts e="T214" id="Seg_796" n="HIAT:w" s="T213">Ku͡okaːnɨ</ts>
                  <nts id="Seg_797" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_799" n="HIAT:w" s="T214">imičči</ts>
                  <nts id="Seg_800" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_802" n="HIAT:w" s="T215">ölördökkütüne</ts>
                  <nts id="Seg_803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_805" n="HIAT:w" s="T216">ɨlɨ͡akkɨt</ts>
                  <nts id="Seg_806" n="HIAT:ip">.</nts>
                  <nts id="Seg_807" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T218" id="Seg_809" n="HIAT:u" s="T217">
                  <ts e="T218" id="Seg_811" n="HIAT:w" s="T217">Heriːlehi͡ekpit</ts>
                  <nts id="Seg_812" n="HIAT:ip">.</nts>
                  <nts id="Seg_813" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T223" id="Seg_815" n="HIAT:u" s="T218">
                  <ts e="T219" id="Seg_817" n="HIAT:w" s="T218">Toktoːŋ</ts>
                  <nts id="Seg_818" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_819" n="HIAT:ip">—</nts>
                  <nts id="Seg_820" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_822" n="HIAT:w" s="T219">min</ts>
                  <nts id="Seg_823" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_825" n="HIAT:w" s="T220">dʼommun</ts>
                  <nts id="Seg_826" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_828" n="HIAT:w" s="T221">komunu͡om</ts>
                  <nts id="Seg_829" n="HIAT:ip">"</nts>
                  <nts id="Seg_830" n="HIAT:ip">,</nts>
                  <nts id="Seg_831" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_833" n="HIAT:w" s="T222">diːr</ts>
                  <nts id="Seg_834" n="HIAT:ip">.</nts>
                  <nts id="Seg_835" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T233" id="Seg_837" n="HIAT:u" s="T223">
                  <ts e="T224" id="Seg_839" n="HIAT:w" s="T223">Ku͡okaː</ts>
                  <nts id="Seg_840" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_842" n="HIAT:w" s="T224">hübehite</ts>
                  <nts id="Seg_843" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_845" n="HIAT:w" s="T225">ergiller</ts>
                  <nts id="Seg_846" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_848" n="HIAT:w" s="T226">da</ts>
                  <nts id="Seg_849" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_851" n="HIAT:w" s="T227">kuturugunan</ts>
                  <nts id="Seg_852" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_853" n="HIAT:ip">"</nts>
                  <ts e="T229" id="Seg_855" n="HIAT:w" s="T228">bar</ts>
                  <nts id="Seg_856" n="HIAT:ip">"</nts>
                  <nts id="Seg_857" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_859" n="HIAT:w" s="T229">gɨmmɨt</ts>
                  <nts id="Seg_860" n="HIAT:ip">,</nts>
                  <nts id="Seg_861" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_863" n="HIAT:w" s="T230">barbɨt</ts>
                  <nts id="Seg_864" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_866" n="HIAT:w" s="T231">dʼon</ts>
                  <nts id="Seg_867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_869" n="HIAT:w" s="T232">komuna</ts>
                  <nts id="Seg_870" n="HIAT:ip">.</nts>
                  <nts id="Seg_871" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T240" id="Seg_873" n="HIAT:u" s="T233">
                  <ts e="T234" id="Seg_875" n="HIAT:w" s="T233">Ku͡okaː</ts>
                  <nts id="Seg_876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_878" n="HIAT:w" s="T234">ü͡örün</ts>
                  <nts id="Seg_879" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_881" n="HIAT:w" s="T235">barɨtɨn</ts>
                  <nts id="Seg_882" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_884" n="HIAT:w" s="T236">egelen</ts>
                  <nts id="Seg_885" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_887" n="HIAT:w" s="T237">hiːkeji</ts>
                  <nts id="Seg_888" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_890" n="HIAT:w" s="T238">karaːččɨ</ts>
                  <nts id="Seg_891" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_893" n="HIAT:w" s="T239">oŋordo</ts>
                  <nts id="Seg_894" n="HIAT:ip">.</nts>
                  <nts id="Seg_895" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T244" id="Seg_897" n="HIAT:u" s="T240">
                  <ts e="T241" id="Seg_899" n="HIAT:w" s="T240">Kurpaːskɨlar</ts>
                  <nts id="Seg_900" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_902" n="HIAT:w" s="T241">belem</ts>
                  <nts id="Seg_903" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_905" n="HIAT:w" s="T242">kɨtɨlga</ts>
                  <nts id="Seg_906" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_908" n="HIAT:w" s="T243">turallar</ts>
                  <nts id="Seg_909" n="HIAT:ip">.</nts>
                  <nts id="Seg_910" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T248" id="Seg_912" n="HIAT:u" s="T244">
                  <ts e="T245" id="Seg_914" n="HIAT:w" s="T244">Kurpaːskɨ</ts>
                  <nts id="Seg_915" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_917" n="HIAT:w" s="T245">hübehite</ts>
                  <nts id="Seg_918" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_920" n="HIAT:w" s="T246">dʼonugar</ts>
                  <nts id="Seg_921" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_923" n="HIAT:w" s="T247">eter</ts>
                  <nts id="Seg_924" n="HIAT:ip">:</nts>
                  <nts id="Seg_925" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T254" id="Seg_927" n="HIAT:u" s="T248">
                  <nts id="Seg_928" n="HIAT:ip">"</nts>
                  <ts e="T249" id="Seg_930" n="HIAT:w" s="T248">Ku͡okaːnɨ</ts>
                  <nts id="Seg_931" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_933" n="HIAT:w" s="T249">baska</ts>
                  <nts id="Seg_934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_936" n="HIAT:w" s="T250">ɨtaːrɨŋ</ts>
                  <nts id="Seg_937" n="HIAT:ip">,</nts>
                  <nts id="Seg_938" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_940" n="HIAT:w" s="T251">agdatɨgar</ts>
                  <nts id="Seg_941" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_943" n="HIAT:w" s="T252">ölöːččüte</ts>
                  <nts id="Seg_944" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_946" n="HIAT:w" s="T253">hu͡ok</ts>
                  <nts id="Seg_947" n="HIAT:ip">!</nts>
                  <nts id="Seg_948" n="HIAT:ip">"</nts>
                  <nts id="Seg_949" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T258" id="Seg_951" n="HIAT:u" s="T254">
                  <ts e="T255" id="Seg_953" n="HIAT:w" s="T254">Kurpaːskɨ</ts>
                  <nts id="Seg_954" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_956" n="HIAT:w" s="T255">heriːte</ts>
                  <nts id="Seg_957" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_959" n="HIAT:w" s="T256">oktorun</ts>
                  <nts id="Seg_960" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_962" n="HIAT:w" s="T257">ɨːppɨt</ts>
                  <nts id="Seg_963" n="HIAT:ip">.</nts>
                  <nts id="Seg_964" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T276" id="Seg_966" n="HIAT:u" s="T258">
                  <ts e="T259" id="Seg_968" n="HIAT:w" s="T258">Alaŋaː</ts>
                  <nts id="Seg_969" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_971" n="HIAT:w" s="T259">kirsitin</ts>
                  <nts id="Seg_972" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_974" n="HIAT:w" s="T260">tɨ͡ahɨn</ts>
                  <nts id="Seg_975" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_977" n="HIAT:w" s="T261">ihilleːt</ts>
                  <nts id="Seg_978" n="HIAT:ip">,</nts>
                  <nts id="Seg_979" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_981" n="HIAT:w" s="T262">ku͡okaːlar</ts>
                  <nts id="Seg_982" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_984" n="HIAT:w" s="T263">ü͡ös</ts>
                  <nts id="Seg_985" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_987" n="HIAT:w" s="T264">di͡eki</ts>
                  <nts id="Seg_988" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_990" n="HIAT:w" s="T265">ergille</ts>
                  <nts id="Seg_991" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_993" n="HIAT:w" s="T266">bi͡eren</ts>
                  <nts id="Seg_994" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_996" n="HIAT:w" s="T267">aharbɨttar</ts>
                  <nts id="Seg_997" n="HIAT:ip">,</nts>
                  <nts id="Seg_998" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_1000" n="HIAT:w" s="T268">kurpaːskɨlar</ts>
                  <nts id="Seg_1001" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_1003" n="HIAT:w" s="T269">oktoro</ts>
                  <nts id="Seg_1004" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_1006" n="HIAT:w" s="T270">ku͡okaː</ts>
                  <nts id="Seg_1007" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_1009" n="HIAT:w" s="T271">higdetiger</ts>
                  <nts id="Seg_1010" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_1012" n="HIAT:w" s="T272">da</ts>
                  <nts id="Seg_1013" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_1015" n="HIAT:w" s="T273">higdetiger</ts>
                  <nts id="Seg_1016" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_1018" n="HIAT:w" s="T274">bi͡eren</ts>
                  <nts id="Seg_1019" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_1021" n="HIAT:w" s="T275">ispit</ts>
                  <nts id="Seg_1022" n="HIAT:ip">.</nts>
                  <nts id="Seg_1023" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T279" id="Seg_1025" n="HIAT:u" s="T276">
                  <ts e="T277" id="Seg_1027" n="HIAT:w" s="T276">Ku͡okaːlar</ts>
                  <nts id="Seg_1028" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_1030" n="HIAT:w" s="T277">ergillen</ts>
                  <nts id="Seg_1031" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_1033" n="HIAT:w" s="T278">kelbitter</ts>
                  <nts id="Seg_1034" n="HIAT:ip">.</nts>
                  <nts id="Seg_1035" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T288" id="Seg_1037" n="HIAT:u" s="T279">
                  <ts e="T280" id="Seg_1039" n="HIAT:w" s="T279">Kurpaːskɨlar</ts>
                  <nts id="Seg_1040" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_1042" n="HIAT:w" s="T280">ok</ts>
                  <nts id="Seg_1043" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_1045" n="HIAT:w" s="T281">ɨlɨnɨ͡aktarɨgar</ts>
                  <nts id="Seg_1046" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_1048" n="HIAT:w" s="T282">di͡eri</ts>
                  <nts id="Seg_1049" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_1051" n="HIAT:w" s="T283">ku͡okaːlar</ts>
                  <nts id="Seg_1052" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_1054" n="HIAT:w" s="T284">ɨppɨttar</ts>
                  <nts id="Seg_1055" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_1057" n="HIAT:w" s="T285">kurpaːskɨlar</ts>
                  <nts id="Seg_1058" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_1060" n="HIAT:w" s="T286">hürekterin</ts>
                  <nts id="Seg_1061" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_1063" n="HIAT:w" s="T287">kɨŋaːn</ts>
                  <nts id="Seg_1064" n="HIAT:ip">.</nts>
                  <nts id="Seg_1065" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T303" id="Seg_1067" n="HIAT:u" s="T288">
                  <ts e="T289" id="Seg_1069" n="HIAT:w" s="T288">Kurpaːskɨlar</ts>
                  <nts id="Seg_1070" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_1072" n="HIAT:w" s="T289">aharan</ts>
                  <nts id="Seg_1073" n="HIAT:ip">,</nts>
                  <nts id="Seg_1074" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_1076" n="HIAT:w" s="T290">kötö</ts>
                  <nts id="Seg_1077" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_1079" n="HIAT:w" s="T291">gɨna</ts>
                  <nts id="Seg_1080" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_1082" n="HIAT:w" s="T292">ü͡öhe</ts>
                  <nts id="Seg_1083" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_1085" n="HIAT:w" s="T293">di͡eki</ts>
                  <nts id="Seg_1086" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1088" n="HIAT:w" s="T294">ojon</ts>
                  <nts id="Seg_1089" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1091" n="HIAT:w" s="T295">bi͡erbitter</ts>
                  <nts id="Seg_1092" n="HIAT:ip">,</nts>
                  <nts id="Seg_1093" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_1095" n="HIAT:w" s="T296">ku͡okaːlar</ts>
                  <nts id="Seg_1096" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_1098" n="HIAT:w" s="T297">oktoro</ts>
                  <nts id="Seg_1099" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1100" n="HIAT:ip">—</nts>
                  <nts id="Seg_1101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_1103" n="HIAT:w" s="T346">kurpaːskɨlar</ts>
                  <nts id="Seg_1104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_1106" n="HIAT:w" s="T298">gi͡ennerin</ts>
                  <nts id="Seg_1107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_1109" n="HIAT:w" s="T299">bɨlčɨŋŋa</ts>
                  <nts id="Seg_1110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_1112" n="HIAT:w" s="T300">da</ts>
                  <nts id="Seg_1113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_1115" n="HIAT:w" s="T301">bɨlčɨŋŋa</ts>
                  <nts id="Seg_1116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1118" n="HIAT:w" s="T302">ataktarɨgar</ts>
                  <nts id="Seg_1119" n="HIAT:ip">.</nts>
                  <nts id="Seg_1120" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T305" id="Seg_1122" n="HIAT:u" s="T303">
                  <ts e="T304" id="Seg_1124" n="HIAT:w" s="T303">Ogorduk</ts>
                  <nts id="Seg_1125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_1127" n="HIAT:w" s="T304">heriːlespitter</ts>
                  <nts id="Seg_1128" n="HIAT:ip">.</nts>
                  <nts id="Seg_1129" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T309" id="Seg_1131" n="HIAT:u" s="T305">
                  <ts e="T306" id="Seg_1133" n="HIAT:w" s="T305">Kajalara</ts>
                  <nts id="Seg_1134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_1136" n="HIAT:w" s="T306">da</ts>
                  <nts id="Seg_1137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_1139" n="HIAT:w" s="T307">ölörsübet</ts>
                  <nts id="Seg_1140" n="HIAT:ip">,</nts>
                  <nts id="Seg_1141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1143" n="HIAT:w" s="T308">kɨ͡ajsɨbat</ts>
                  <nts id="Seg_1144" n="HIAT:ip">.</nts>
                  <nts id="Seg_1145" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T314" id="Seg_1147" n="HIAT:u" s="T309">
                  <ts e="T310" id="Seg_1149" n="HIAT:w" s="T309">Oktoro</ts>
                  <nts id="Seg_1150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_1152" n="HIAT:w" s="T310">barɨta</ts>
                  <nts id="Seg_1153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_1155" n="HIAT:w" s="T311">baranan</ts>
                  <nts id="Seg_1156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_1158" n="HIAT:w" s="T312">heriːlehen</ts>
                  <nts id="Seg_1159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_1161" n="HIAT:w" s="T313">büppütter</ts>
                  <nts id="Seg_1162" n="HIAT:ip">.</nts>
                  <nts id="Seg_1163" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T316" id="Seg_1165" n="HIAT:u" s="T314">
                  <ts e="T315" id="Seg_1167" n="HIAT:w" s="T314">Ku͡oka</ts>
                  <nts id="Seg_1168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1170" n="HIAT:w" s="T315">hübehite</ts>
                  <nts id="Seg_1171" n="HIAT:ip">:</nts>
                  <nts id="Seg_1172" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T319" id="Seg_1174" n="HIAT:u" s="T316">
                  <nts id="Seg_1175" n="HIAT:ip">"</nts>
                  <ts e="T317" id="Seg_1177" n="HIAT:w" s="T316">Anɨ</ts>
                  <nts id="Seg_1178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_1180" n="HIAT:w" s="T317">heriːlehen</ts>
                  <nts id="Seg_1181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1183" n="HIAT:w" s="T318">bütü͡ögüŋ</ts>
                  <nts id="Seg_1184" n="HIAT:ip">.</nts>
                  <nts id="Seg_1185" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T328" id="Seg_1187" n="HIAT:u" s="T319">
                  <ts e="T320" id="Seg_1189" n="HIAT:w" s="T319">Ehigi</ts>
                  <nts id="Seg_1190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1192" n="HIAT:w" s="T320">kanna</ts>
                  <nts id="Seg_1193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_1195" n="HIAT:w" s="T321">bagarar</ts>
                  <nts id="Seg_1196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1198" n="HIAT:w" s="T322">kötö</ts>
                  <nts id="Seg_1199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_1201" n="HIAT:w" s="T323">hɨldʼɨŋ</ts>
                  <nts id="Seg_1202" n="HIAT:ip">,</nts>
                  <nts id="Seg_1203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1205" n="HIAT:w" s="T324">kanna</ts>
                  <nts id="Seg_1206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_1208" n="HIAT:w" s="T325">bagarar</ts>
                  <nts id="Seg_1209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1211" n="HIAT:w" s="T326">ahaːŋ</ts>
                  <nts id="Seg_1212" n="HIAT:ip">"</nts>
                  <nts id="Seg_1213" n="HIAT:ip">,</nts>
                  <nts id="Seg_1214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_1216" n="HIAT:w" s="T327">diːr</ts>
                  <nts id="Seg_1217" n="HIAT:ip">.</nts>
                  <nts id="Seg_1218" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T344" id="Seg_1220" n="HIAT:u" s="T328">
                  <ts e="T329" id="Seg_1222" n="HIAT:w" s="T328">Hoččotton</ts>
                  <nts id="Seg_1223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_1225" n="HIAT:w" s="T329">kurpaːskɨlar</ts>
                  <nts id="Seg_1226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_1228" n="HIAT:w" s="T330">oktoro</ts>
                  <nts id="Seg_1229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_1231" n="HIAT:w" s="T331">ku͡okaː</ts>
                  <nts id="Seg_1232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_1234" n="HIAT:w" s="T332">higdetiger</ts>
                  <nts id="Seg_1235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_1237" n="HIAT:w" s="T333">atɨrdʼak</ts>
                  <nts id="Seg_1238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_1240" n="HIAT:w" s="T334">oŋu͡ok</ts>
                  <nts id="Seg_1241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_1243" n="HIAT:w" s="T335">bu͡olbuttar</ts>
                  <nts id="Seg_1244" n="HIAT:ip">,</nts>
                  <nts id="Seg_1245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_1247" n="HIAT:w" s="T336">ku͡okaːlar</ts>
                  <nts id="Seg_1248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_1250" n="HIAT:w" s="T337">oktoro</ts>
                  <nts id="Seg_1251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_1253" n="HIAT:w" s="T338">kurpaːskɨ</ts>
                  <nts id="Seg_1254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_1256" n="HIAT:w" s="T339">atagɨn</ts>
                  <nts id="Seg_1257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_1259" n="HIAT:w" s="T340">bɨlčɨŋɨgar</ts>
                  <nts id="Seg_1260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_1262" n="HIAT:w" s="T341">iŋiːr</ts>
                  <nts id="Seg_1263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1265" n="HIAT:w" s="T342">oŋu͡ok</ts>
                  <nts id="Seg_1266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_1268" n="HIAT:w" s="T343">bu͡olbuttar</ts>
                  <nts id="Seg_1269" n="HIAT:ip">.</nts>
                  <nts id="Seg_1270" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T345" id="Seg_1272" n="HIAT:u" s="T344">
                  <ts e="T345" id="Seg_1274" n="HIAT:w" s="T344">Elete</ts>
                  <nts id="Seg_1275" n="HIAT:ip">.</nts>
                  <nts id="Seg_1276" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T345" id="Seg_1277" n="sc" s="T0">
               <ts e="T1" id="Seg_1279" n="e" s="T0">Dʼe </ts>
               <ts e="T2" id="Seg_1281" n="e" s="T1">bɨlɨr, </ts>
               <ts e="T3" id="Seg_1283" n="e" s="T2">hir-taŋara </ts>
               <ts e="T4" id="Seg_1285" n="e" s="T3">ü͡ösküːrüger, </ts>
               <ts e="T5" id="Seg_1287" n="e" s="T4">kurpaːskɨ </ts>
               <ts e="T6" id="Seg_1289" n="e" s="T5">mu͡ora </ts>
               <ts e="T7" id="Seg_1291" n="e" s="T6">hirdeːge </ts>
               <ts e="T8" id="Seg_1293" n="e" s="T7">ühü. </ts>
               <ts e="T9" id="Seg_1295" n="e" s="T8">Biːr </ts>
               <ts e="T10" id="Seg_1297" n="e" s="T9">dʼɨl </ts>
               <ts e="T11" id="Seg_1299" n="e" s="T10">purgaːlaːk </ts>
               <ts e="T12" id="Seg_1301" n="e" s="T11">bu͡olbut. </ts>
               <ts e="T13" id="Seg_1303" n="e" s="T12">Kɨhɨn </ts>
               <ts e="T14" id="Seg_1305" n="e" s="T13">hamɨːrdaːbɨt, </ts>
               <ts e="T15" id="Seg_1307" n="e" s="T14">kaːra </ts>
               <ts e="T16" id="Seg_1309" n="e" s="T15">barɨta </ts>
               <ts e="T17" id="Seg_1311" n="e" s="T16">buːs </ts>
               <ts e="T18" id="Seg_1313" n="e" s="T17">bu͡olbut. </ts>
               <ts e="T19" id="Seg_1315" n="e" s="T18">Biːr </ts>
               <ts e="T20" id="Seg_1317" n="e" s="T19">da </ts>
               <ts e="T21" id="Seg_1319" n="e" s="T20">kadʼɨrɨk </ts>
               <ts e="T22" id="Seg_1321" n="e" s="T21">öttüte </ts>
               <ts e="T23" id="Seg_1323" n="e" s="T22">hu͡ok. </ts>
               <ts e="T24" id="Seg_1325" n="e" s="T23">Kurpaːskɨlar </ts>
               <ts e="T25" id="Seg_1327" n="e" s="T24">korgujbuttar, </ts>
               <ts e="T26" id="Seg_1329" n="e" s="T25">giniler </ts>
               <ts e="T27" id="Seg_1331" n="e" s="T26">as </ts>
               <ts e="T28" id="Seg_1333" n="e" s="T27">kördönö </ts>
               <ts e="T29" id="Seg_1335" n="e" s="T28">mas </ts>
               <ts e="T30" id="Seg_1337" n="e" s="T29">di͡ekki </ts>
               <ts e="T31" id="Seg_1339" n="e" s="T30">köppütter. </ts>
               <ts e="T32" id="Seg_1341" n="e" s="T31">Biːr </ts>
               <ts e="T33" id="Seg_1343" n="e" s="T32">muŋ </ts>
               <ts e="T34" id="Seg_1345" n="e" s="T33">kɨrdʼagas </ts>
               <ts e="T35" id="Seg_1347" n="e" s="T34">hübehit </ts>
               <ts e="T36" id="Seg_1349" n="e" s="T35">kurpaːskɨlaːktar. </ts>
               <ts e="T37" id="Seg_1351" n="e" s="T36">Maska </ts>
               <ts e="T38" id="Seg_1353" n="e" s="T37">tijbitter, </ts>
               <ts e="T39" id="Seg_1355" n="e" s="T38">kanna </ts>
               <ts e="T40" id="Seg_1357" n="e" s="T39">da </ts>
               <ts e="T41" id="Seg_1359" n="e" s="T40">kadʼɨrɨk </ts>
               <ts e="T42" id="Seg_1361" n="e" s="T41">köstübet, </ts>
               <ts e="T43" id="Seg_1363" n="e" s="T42">talak </ts>
               <ts e="T44" id="Seg_1365" n="e" s="T43">töbötö </ts>
               <ts e="T45" id="Seg_1367" n="e" s="T44">da </ts>
               <ts e="T46" id="Seg_1369" n="e" s="T45">billibet, </ts>
               <ts e="T47" id="Seg_1371" n="e" s="T46">tiben </ts>
               <ts e="T48" id="Seg_1373" n="e" s="T47">keːspit, </ts>
               <ts e="T49" id="Seg_1375" n="e" s="T48">mas </ts>
               <ts e="T50" id="Seg_1377" n="e" s="T49">agaj </ts>
               <ts e="T51" id="Seg_1379" n="e" s="T50">karaːrar. </ts>
               <ts e="T52" id="Seg_1381" n="e" s="T51">Hübehittere </ts>
               <ts e="T53" id="Seg_1383" n="e" s="T52">hübeliːr: </ts>
               <ts e="T54" id="Seg_1385" n="e" s="T53">"Dʼe </ts>
               <ts e="T55" id="Seg_1387" n="e" s="T54">anɨ </ts>
               <ts e="T56" id="Seg_1389" n="e" s="T55">kihitijbetibit </ts>
               <ts e="T57" id="Seg_1391" n="e" s="T56">bihigi. </ts>
               <ts e="T58" id="Seg_1393" n="e" s="T57">Kanna </ts>
               <ts e="T59" id="Seg_1395" n="e" s="T58">baran </ts>
               <ts e="T60" id="Seg_1397" n="e" s="T59">öl-ü͡ökpütüj </ts>
               <ts e="T61" id="Seg_1399" n="e" s="T60">bihigi? </ts>
               <ts e="T62" id="Seg_1401" n="e" s="T61">Bugurduk </ts>
               <ts e="T63" id="Seg_1403" n="e" s="T62">duːmalɨːbɨn. </ts>
               <ts e="T64" id="Seg_1405" n="e" s="T63">Kannɨk </ts>
               <ts e="T65" id="Seg_1407" n="e" s="T64">ere </ts>
               <ts e="T66" id="Seg_1409" n="e" s="T65">üjege </ts>
               <ts e="T67" id="Seg_1411" n="e" s="T66">biːr </ts>
               <ts e="T68" id="Seg_1413" n="e" s="T67">hiri </ts>
               <ts e="T69" id="Seg_1415" n="e" s="T68">körbütteːkpin, </ts>
               <ts e="T70" id="Seg_1417" n="e" s="T69">biːr </ts>
               <ts e="T71" id="Seg_1419" n="e" s="T70">ulagan </ts>
               <ts e="T72" id="Seg_1421" n="e" s="T71">ürek </ts>
               <ts e="T73" id="Seg_1423" n="e" s="T72">baːr. </ts>
               <ts e="T74" id="Seg_1425" n="e" s="T73">Ol </ts>
               <ts e="T75" id="Seg_1427" n="e" s="T74">üregiŋ </ts>
               <ts e="T76" id="Seg_1429" n="e" s="T75">kajdaktaːk </ts>
               <ts e="T77" id="Seg_1431" n="e" s="T76">purgaːga </ts>
               <ts e="T78" id="Seg_1433" n="e" s="T77">tibilleːččite </ts>
               <ts e="T79" id="Seg_1435" n="e" s="T78">hu͡ok. </ts>
               <ts e="T80" id="Seg_1437" n="e" s="T79">Horok </ts>
               <ts e="T81" id="Seg_1439" n="e" s="T80">hire </ts>
               <ts e="T82" id="Seg_1441" n="e" s="T81">kɨhɨnɨ </ts>
               <ts e="T83" id="Seg_1443" n="e" s="T82">bɨha </ts>
               <ts e="T84" id="Seg_1445" n="e" s="T83">hiːkej </ts>
               <ts e="T85" id="Seg_1447" n="e" s="T84">bu͡olar. </ts>
               <ts e="T86" id="Seg_1449" n="e" s="T85">Kɨtɨlɨttan </ts>
               <ts e="T87" id="Seg_1451" n="e" s="T86">talaktaːk, </ts>
               <ts e="T88" id="Seg_1453" n="e" s="T87">kajata </ts>
               <ts e="T89" id="Seg_1455" n="e" s="T88">barɨta </ts>
               <ts e="T90" id="Seg_1457" n="e" s="T89">heppereːkteːk, </ts>
               <ts e="T91" id="Seg_1459" n="e" s="T90">hugunnaːk, </ts>
               <ts e="T92" id="Seg_1461" n="e" s="T91">ottoːk. </ts>
               <ts e="T93" id="Seg_1463" n="e" s="T92">Ol </ts>
               <ts e="T94" id="Seg_1465" n="e" s="T93">ürekke </ts>
               <ts e="T95" id="Seg_1467" n="e" s="T94">ku͡okaː </ts>
               <ts e="T96" id="Seg_1469" n="e" s="T95">gi͡ene </ts>
               <ts e="T97" id="Seg_1471" n="e" s="T96">ologo. </ts>
               <ts e="T98" id="Seg_1473" n="e" s="T97">Hɨččak </ts>
               <ts e="T99" id="Seg_1475" n="e" s="T98">ku͡okaːlar </ts>
               <ts e="T100" id="Seg_1477" n="e" s="T99">agaj </ts>
               <ts e="T101" id="Seg_1479" n="e" s="T100">bihigini </ts>
               <ts e="T102" id="Seg_1481" n="e" s="T101">čugahatɨ͡aktara </ts>
               <ts e="T103" id="Seg_1483" n="e" s="T102">hu͡oga. </ts>
               <ts e="T104" id="Seg_1485" n="e" s="T103">Bɨlɨr </ts>
               <ts e="T105" id="Seg_1487" n="e" s="T104">üjege </ts>
               <ts e="T106" id="Seg_1489" n="e" s="T105">andagaj </ts>
               <ts e="T107" id="Seg_1491" n="e" s="T106">tɨllaːktar </ts>
               <ts e="T108" id="Seg_1493" n="e" s="T107">kurpaːskanɨ </ts>
               <ts e="T109" id="Seg_1495" n="e" s="T108">kɨtta, </ts>
               <ts e="T110" id="Seg_1497" n="e" s="T109">kurpaːskɨ </ts>
               <ts e="T111" id="Seg_1499" n="e" s="T110">ku͡okaː </ts>
               <ts e="T112" id="Seg_1501" n="e" s="T111">dojdutugar </ts>
               <ts e="T113" id="Seg_1503" n="e" s="T112">barɨ͡a </ts>
               <ts e="T114" id="Seg_1505" n="e" s="T113">hu͡oga </ts>
               <ts e="T115" id="Seg_1507" n="e" s="T114">di͡en, </ts>
               <ts e="T116" id="Seg_1509" n="e" s="T115">ku͡okaː — </ts>
               <ts e="T117" id="Seg_1511" n="e" s="T116">kurpaːskɨ </ts>
               <ts e="T118" id="Seg_1513" n="e" s="T117">dojdutugar. </ts>
               <ts e="T119" id="Seg_1515" n="e" s="T118">Öjdüːr </ts>
               <ts e="T120" id="Seg_1517" n="e" s="T119">bu͡ollaktɨrɨna </ts>
               <ts e="T121" id="Seg_1519" n="e" s="T120">ütü͡önnen </ts>
               <ts e="T122" id="Seg_1521" n="e" s="T121">bi͡eri͡ektere </ts>
               <ts e="T123" id="Seg_1523" n="e" s="T122">hu͡oga. </ts>
               <ts e="T124" id="Seg_1525" n="e" s="T123">Bihigi </ts>
               <ts e="T125" id="Seg_1527" n="e" s="T124">oččogo </ts>
               <ts e="T126" id="Seg_1529" n="e" s="T125">heriːnnen </ts>
               <ts e="T127" id="Seg_1531" n="e" s="T126">agaj </ts>
               <ts e="T128" id="Seg_1533" n="e" s="T127">kiːrini͡ekpit." </ts>
               <ts e="T129" id="Seg_1535" n="e" s="T128">Kurpaːskɨlar: </ts>
               <ts e="T130" id="Seg_1537" n="e" s="T129">"Kirdik. </ts>
               <ts e="T131" id="Seg_1539" n="e" s="T130">Manna </ts>
               <ts e="T132" id="Seg_1541" n="e" s="T131">olorbutunan </ts>
               <ts e="T133" id="Seg_1543" n="e" s="T132">ölü͡ökpüt </ts>
               <ts e="T134" id="Seg_1545" n="e" s="T133">du͡o? </ts>
               <ts e="T135" id="Seg_1547" n="e" s="T134">Ol </ts>
               <ts e="T136" id="Seg_1549" n="e" s="T135">keri͡etin </ts>
               <ts e="T137" id="Seg_1551" n="e" s="T136">heriːlehi͡ekpit. </ts>
               <ts e="T138" id="Seg_1553" n="e" s="T137">Barɨ͡agɨŋ", </ts>
               <ts e="T139" id="Seg_1555" n="e" s="T138">di͡etiler. </ts>
               <ts e="T140" id="Seg_1557" n="e" s="T139">Kurpaːskɨlar </ts>
               <ts e="T141" id="Seg_1559" n="e" s="T140">kötöllör, </ts>
               <ts e="T142" id="Seg_1561" n="e" s="T141">ürekke </ts>
               <ts e="T143" id="Seg_1563" n="e" s="T142">tijeller. </ts>
               <ts e="T144" id="Seg_1565" n="e" s="T143">As </ts>
               <ts e="T145" id="Seg_1567" n="e" s="T144">neleje </ts>
               <ts e="T146" id="Seg_1569" n="e" s="T145">hɨtar </ts>
               <ts e="T147" id="Seg_1571" n="e" s="T146">hire </ts>
               <ts e="T148" id="Seg_1573" n="e" s="T147">ebit. </ts>
               <ts e="T149" id="Seg_1575" n="e" s="T148">Ahɨː </ts>
               <ts e="T150" id="Seg_1577" n="e" s="T149">olorbuttar. </ts>
               <ts e="T151" id="Seg_1579" n="e" s="T150">Hiːkej </ts>
               <ts e="T152" id="Seg_1581" n="e" s="T151">ustun </ts>
               <ts e="T153" id="Seg_1583" n="e" s="T152">ku͡okaː </ts>
               <ts e="T154" id="Seg_1585" n="e" s="T153">hübehitin </ts>
               <ts e="T155" id="Seg_1587" n="e" s="T154">mejiːte </ts>
               <ts e="T156" id="Seg_1589" n="e" s="T155">bɨltas </ts>
               <ts e="T157" id="Seg_1591" n="e" s="T156">gɨmmɨt: </ts>
               <ts e="T158" id="Seg_1593" n="e" s="T157">"Kaja-keː, </ts>
               <ts e="T159" id="Seg_1595" n="e" s="T158">kurpaːskɨlar, </ts>
               <ts e="T160" id="Seg_1597" n="e" s="T159">togo </ts>
               <ts e="T161" id="Seg_1599" n="e" s="T160">hirge </ts>
               <ts e="T162" id="Seg_1601" n="e" s="T161">kelbikkitij? </ts>
               <ts e="T163" id="Seg_1603" n="e" s="T162">Bɨlɨr </ts>
               <ts e="T164" id="Seg_1605" n="e" s="T163">üjege </ts>
               <ts e="T165" id="Seg_1607" n="e" s="T164">andagaj </ts>
               <ts e="T166" id="Seg_1609" n="e" s="T165">tɨllaːk </ts>
               <ts e="T167" id="Seg_1611" n="e" s="T166">etibit, </ts>
               <ts e="T168" id="Seg_1613" n="e" s="T167">ehigi </ts>
               <ts e="T169" id="Seg_1615" n="e" s="T168">hirgitiger </ts>
               <ts e="T170" id="Seg_1617" n="e" s="T169">bihigi </ts>
               <ts e="T171" id="Seg_1619" n="e" s="T170">barbappɨt, </ts>
               <ts e="T172" id="Seg_1621" n="e" s="T171">ehigi — </ts>
               <ts e="T173" id="Seg_1623" n="e" s="T172">bihigi </ts>
               <ts e="T174" id="Seg_1625" n="e" s="T173">hirbitiger. </ts>
               <ts e="T175" id="Seg_1627" n="e" s="T174">Togo </ts>
               <ts e="T176" id="Seg_1629" n="e" s="T175">kelligit, </ts>
               <ts e="T177" id="Seg_1631" n="e" s="T176">köstümeŋ, </ts>
               <ts e="T178" id="Seg_1633" n="e" s="T177">hanɨkaːn </ts>
               <ts e="T179" id="Seg_1635" n="e" s="T178">barɨŋ, </ts>
               <ts e="T180" id="Seg_1637" n="e" s="T179">mantan! </ts>
               <ts e="T181" id="Seg_1639" n="e" s="T180">Talak </ts>
               <ts e="T182" id="Seg_1641" n="e" s="T181">gi͡enin </ts>
               <ts e="T183" id="Seg_1643" n="e" s="T182">hebirdegin, </ts>
               <ts e="T184" id="Seg_1645" n="e" s="T183">heppereːk </ts>
               <ts e="T185" id="Seg_1647" n="e" s="T184">gi͡enin </ts>
               <ts e="T186" id="Seg_1649" n="e" s="T185">töbötün </ts>
               <ts e="T187" id="Seg_1651" n="e" s="T186">da </ts>
               <ts e="T188" id="Seg_1653" n="e" s="T187">hi͡eti͡em </ts>
               <ts e="T189" id="Seg_1655" n="e" s="T188">hu͡oga, </ts>
               <ts e="T190" id="Seg_1657" n="e" s="T189">oppun </ts>
               <ts e="T191" id="Seg_1659" n="e" s="T190">da </ts>
               <ts e="T192" id="Seg_1661" n="e" s="T191">hi͡eppeppin, </ts>
               <ts e="T193" id="Seg_1663" n="e" s="T192">čajbɨn </ts>
               <ts e="T194" id="Seg_1665" n="e" s="T193">da </ts>
               <ts e="T195" id="Seg_1667" n="e" s="T194">kastarɨ͡am </ts>
               <ts e="T196" id="Seg_1669" n="e" s="T195">hu͡oga!" </ts>
               <ts e="T197" id="Seg_1671" n="e" s="T196">Onuga </ts>
               <ts e="T198" id="Seg_1673" n="e" s="T197">kurpaːsku </ts>
               <ts e="T199" id="Seg_1675" n="e" s="T198">hübehite </ts>
               <ts e="T200" id="Seg_1677" n="e" s="T199">diːr: </ts>
               <ts e="T201" id="Seg_1679" n="e" s="T200">"Bihigi </ts>
               <ts e="T202" id="Seg_1681" n="e" s="T201">ütü͡önnen </ts>
               <ts e="T203" id="Seg_1683" n="e" s="T202">barbappɨt. </ts>
               <ts e="T204" id="Seg_1685" n="e" s="T203">Heriːnnen </ts>
               <ts e="T205" id="Seg_1687" n="e" s="T204">daːganɨ </ts>
               <ts e="T206" id="Seg_1689" n="e" s="T205">bu </ts>
               <ts e="T207" id="Seg_1691" n="e" s="T206">hiri </ts>
               <ts e="T208" id="Seg_1693" n="e" s="T207">ɨlɨ͡akpɨt!" </ts>
               <ts e="T209" id="Seg_1695" n="e" s="T208">Ku͡okaː </ts>
               <ts e="T210" id="Seg_1697" n="e" s="T209">hübehite: </ts>
               <ts e="T211" id="Seg_1699" n="e" s="T210">"Min </ts>
               <ts e="T212" id="Seg_1701" n="e" s="T211">ütü͡önnen </ts>
               <ts e="T213" id="Seg_1703" n="e" s="T212">bi͡erbeppin. </ts>
               <ts e="T214" id="Seg_1705" n="e" s="T213">Ku͡okaːnɨ </ts>
               <ts e="T215" id="Seg_1707" n="e" s="T214">imičči </ts>
               <ts e="T216" id="Seg_1709" n="e" s="T215">ölördökkütüne </ts>
               <ts e="T217" id="Seg_1711" n="e" s="T216">ɨlɨ͡akkɨt. </ts>
               <ts e="T218" id="Seg_1713" n="e" s="T217">Heriːlehi͡ekpit. </ts>
               <ts e="T219" id="Seg_1715" n="e" s="T218">Toktoːŋ — </ts>
               <ts e="T220" id="Seg_1717" n="e" s="T219">min </ts>
               <ts e="T221" id="Seg_1719" n="e" s="T220">dʼommun </ts>
               <ts e="T222" id="Seg_1721" n="e" s="T221">komunu͡om", </ts>
               <ts e="T223" id="Seg_1723" n="e" s="T222">diːr. </ts>
               <ts e="T224" id="Seg_1725" n="e" s="T223">Ku͡okaː </ts>
               <ts e="T225" id="Seg_1727" n="e" s="T224">hübehite </ts>
               <ts e="T226" id="Seg_1729" n="e" s="T225">ergiller </ts>
               <ts e="T227" id="Seg_1731" n="e" s="T226">da </ts>
               <ts e="T228" id="Seg_1733" n="e" s="T227">kuturugunan </ts>
               <ts e="T229" id="Seg_1735" n="e" s="T228">"bar" </ts>
               <ts e="T230" id="Seg_1737" n="e" s="T229">gɨmmɨt, </ts>
               <ts e="T231" id="Seg_1739" n="e" s="T230">barbɨt </ts>
               <ts e="T232" id="Seg_1741" n="e" s="T231">dʼon </ts>
               <ts e="T233" id="Seg_1743" n="e" s="T232">komuna. </ts>
               <ts e="T234" id="Seg_1745" n="e" s="T233">Ku͡okaː </ts>
               <ts e="T235" id="Seg_1747" n="e" s="T234">ü͡örün </ts>
               <ts e="T236" id="Seg_1749" n="e" s="T235">barɨtɨn </ts>
               <ts e="T237" id="Seg_1751" n="e" s="T236">egelen </ts>
               <ts e="T238" id="Seg_1753" n="e" s="T237">hiːkeji </ts>
               <ts e="T239" id="Seg_1755" n="e" s="T238">karaːččɨ </ts>
               <ts e="T240" id="Seg_1757" n="e" s="T239">oŋordo. </ts>
               <ts e="T241" id="Seg_1759" n="e" s="T240">Kurpaːskɨlar </ts>
               <ts e="T242" id="Seg_1761" n="e" s="T241">belem </ts>
               <ts e="T243" id="Seg_1763" n="e" s="T242">kɨtɨlga </ts>
               <ts e="T244" id="Seg_1765" n="e" s="T243">turallar. </ts>
               <ts e="T245" id="Seg_1767" n="e" s="T244">Kurpaːskɨ </ts>
               <ts e="T246" id="Seg_1769" n="e" s="T245">hübehite </ts>
               <ts e="T247" id="Seg_1771" n="e" s="T246">dʼonugar </ts>
               <ts e="T248" id="Seg_1773" n="e" s="T247">eter: </ts>
               <ts e="T249" id="Seg_1775" n="e" s="T248">"Ku͡okaːnɨ </ts>
               <ts e="T250" id="Seg_1777" n="e" s="T249">baska </ts>
               <ts e="T251" id="Seg_1779" n="e" s="T250">ɨtaːrɨŋ, </ts>
               <ts e="T252" id="Seg_1781" n="e" s="T251">agdatɨgar </ts>
               <ts e="T253" id="Seg_1783" n="e" s="T252">ölöːččüte </ts>
               <ts e="T254" id="Seg_1785" n="e" s="T253">hu͡ok!" </ts>
               <ts e="T255" id="Seg_1787" n="e" s="T254">Kurpaːskɨ </ts>
               <ts e="T256" id="Seg_1789" n="e" s="T255">heriːte </ts>
               <ts e="T257" id="Seg_1791" n="e" s="T256">oktorun </ts>
               <ts e="T258" id="Seg_1793" n="e" s="T257">ɨːppɨt. </ts>
               <ts e="T259" id="Seg_1795" n="e" s="T258">Alaŋaː </ts>
               <ts e="T260" id="Seg_1797" n="e" s="T259">kirsitin </ts>
               <ts e="T261" id="Seg_1799" n="e" s="T260">tɨ͡ahɨn </ts>
               <ts e="T262" id="Seg_1801" n="e" s="T261">ihilleːt, </ts>
               <ts e="T263" id="Seg_1803" n="e" s="T262">ku͡okaːlar </ts>
               <ts e="T264" id="Seg_1805" n="e" s="T263">ü͡ös </ts>
               <ts e="T265" id="Seg_1807" n="e" s="T264">di͡eki </ts>
               <ts e="T266" id="Seg_1809" n="e" s="T265">ergille </ts>
               <ts e="T267" id="Seg_1811" n="e" s="T266">bi͡eren </ts>
               <ts e="T268" id="Seg_1813" n="e" s="T267">aharbɨttar, </ts>
               <ts e="T269" id="Seg_1815" n="e" s="T268">kurpaːskɨlar </ts>
               <ts e="T270" id="Seg_1817" n="e" s="T269">oktoro </ts>
               <ts e="T271" id="Seg_1819" n="e" s="T270">ku͡okaː </ts>
               <ts e="T272" id="Seg_1821" n="e" s="T271">higdetiger </ts>
               <ts e="T273" id="Seg_1823" n="e" s="T272">da </ts>
               <ts e="T274" id="Seg_1825" n="e" s="T273">higdetiger </ts>
               <ts e="T275" id="Seg_1827" n="e" s="T274">bi͡eren </ts>
               <ts e="T276" id="Seg_1829" n="e" s="T275">ispit. </ts>
               <ts e="T277" id="Seg_1831" n="e" s="T276">Ku͡okaːlar </ts>
               <ts e="T278" id="Seg_1833" n="e" s="T277">ergillen </ts>
               <ts e="T279" id="Seg_1835" n="e" s="T278">kelbitter. </ts>
               <ts e="T280" id="Seg_1837" n="e" s="T279">Kurpaːskɨlar </ts>
               <ts e="T281" id="Seg_1839" n="e" s="T280">ok </ts>
               <ts e="T282" id="Seg_1841" n="e" s="T281">ɨlɨnɨ͡aktarɨgar </ts>
               <ts e="T283" id="Seg_1843" n="e" s="T282">di͡eri </ts>
               <ts e="T284" id="Seg_1845" n="e" s="T283">ku͡okaːlar </ts>
               <ts e="T285" id="Seg_1847" n="e" s="T284">ɨppɨttar </ts>
               <ts e="T286" id="Seg_1849" n="e" s="T285">kurpaːskɨlar </ts>
               <ts e="T287" id="Seg_1851" n="e" s="T286">hürekterin </ts>
               <ts e="T288" id="Seg_1853" n="e" s="T287">kɨŋaːn. </ts>
               <ts e="T289" id="Seg_1855" n="e" s="T288">Kurpaːskɨlar </ts>
               <ts e="T290" id="Seg_1857" n="e" s="T289">aharan, </ts>
               <ts e="T291" id="Seg_1859" n="e" s="T290">kötö </ts>
               <ts e="T292" id="Seg_1861" n="e" s="T291">gɨna </ts>
               <ts e="T293" id="Seg_1863" n="e" s="T292">ü͡öhe </ts>
               <ts e="T294" id="Seg_1865" n="e" s="T293">di͡eki </ts>
               <ts e="T295" id="Seg_1867" n="e" s="T294">ojon </ts>
               <ts e="T296" id="Seg_1869" n="e" s="T295">bi͡erbitter, </ts>
               <ts e="T297" id="Seg_1871" n="e" s="T296">ku͡okaːlar </ts>
               <ts e="T346" id="Seg_1873" n="e" s="T297">oktoro — </ts>
               <ts e="T298" id="Seg_1875" n="e" s="T346">kurpaːskɨlar </ts>
               <ts e="T299" id="Seg_1877" n="e" s="T298">gi͡ennerin </ts>
               <ts e="T300" id="Seg_1879" n="e" s="T299">bɨlčɨŋŋa </ts>
               <ts e="T301" id="Seg_1881" n="e" s="T300">da </ts>
               <ts e="T302" id="Seg_1883" n="e" s="T301">bɨlčɨŋŋa </ts>
               <ts e="T303" id="Seg_1885" n="e" s="T302">ataktarɨgar. </ts>
               <ts e="T304" id="Seg_1887" n="e" s="T303">Ogorduk </ts>
               <ts e="T305" id="Seg_1889" n="e" s="T304">heriːlespitter. </ts>
               <ts e="T306" id="Seg_1891" n="e" s="T305">Kajalara </ts>
               <ts e="T307" id="Seg_1893" n="e" s="T306">da </ts>
               <ts e="T308" id="Seg_1895" n="e" s="T307">ölörsübet, </ts>
               <ts e="T309" id="Seg_1897" n="e" s="T308">kɨ͡ajsɨbat. </ts>
               <ts e="T310" id="Seg_1899" n="e" s="T309">Oktoro </ts>
               <ts e="T311" id="Seg_1901" n="e" s="T310">barɨta </ts>
               <ts e="T312" id="Seg_1903" n="e" s="T311">baranan </ts>
               <ts e="T313" id="Seg_1905" n="e" s="T312">heriːlehen </ts>
               <ts e="T314" id="Seg_1907" n="e" s="T313">büppütter. </ts>
               <ts e="T315" id="Seg_1909" n="e" s="T314">Ku͡oka </ts>
               <ts e="T316" id="Seg_1911" n="e" s="T315">hübehite: </ts>
               <ts e="T317" id="Seg_1913" n="e" s="T316">"Anɨ </ts>
               <ts e="T318" id="Seg_1915" n="e" s="T317">heriːlehen </ts>
               <ts e="T319" id="Seg_1917" n="e" s="T318">bütü͡ögüŋ. </ts>
               <ts e="T320" id="Seg_1919" n="e" s="T319">Ehigi </ts>
               <ts e="T321" id="Seg_1921" n="e" s="T320">kanna </ts>
               <ts e="T322" id="Seg_1923" n="e" s="T321">bagarar </ts>
               <ts e="T323" id="Seg_1925" n="e" s="T322">kötö </ts>
               <ts e="T324" id="Seg_1927" n="e" s="T323">hɨldʼɨŋ, </ts>
               <ts e="T325" id="Seg_1929" n="e" s="T324">kanna </ts>
               <ts e="T326" id="Seg_1931" n="e" s="T325">bagarar </ts>
               <ts e="T327" id="Seg_1933" n="e" s="T326">ahaːŋ", </ts>
               <ts e="T328" id="Seg_1935" n="e" s="T327">diːr. </ts>
               <ts e="T329" id="Seg_1937" n="e" s="T328">Hoččotton </ts>
               <ts e="T330" id="Seg_1939" n="e" s="T329">kurpaːskɨlar </ts>
               <ts e="T331" id="Seg_1941" n="e" s="T330">oktoro </ts>
               <ts e="T332" id="Seg_1943" n="e" s="T331">ku͡okaː </ts>
               <ts e="T333" id="Seg_1945" n="e" s="T332">higdetiger </ts>
               <ts e="T334" id="Seg_1947" n="e" s="T333">atɨrdʼak </ts>
               <ts e="T335" id="Seg_1949" n="e" s="T334">oŋu͡ok </ts>
               <ts e="T336" id="Seg_1951" n="e" s="T335">bu͡olbuttar, </ts>
               <ts e="T337" id="Seg_1953" n="e" s="T336">ku͡okaːlar </ts>
               <ts e="T338" id="Seg_1955" n="e" s="T337">oktoro </ts>
               <ts e="T339" id="Seg_1957" n="e" s="T338">kurpaːskɨ </ts>
               <ts e="T340" id="Seg_1959" n="e" s="T339">atagɨn </ts>
               <ts e="T341" id="Seg_1961" n="e" s="T340">bɨlčɨŋɨgar </ts>
               <ts e="T342" id="Seg_1963" n="e" s="T341">iŋiːr </ts>
               <ts e="T343" id="Seg_1965" n="e" s="T342">oŋu͡ok </ts>
               <ts e="T344" id="Seg_1967" n="e" s="T343">bu͡olbuttar. </ts>
               <ts e="T345" id="Seg_1969" n="e" s="T344">Elete. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T8" id="Seg_1970" s="T0">ErSV_1964_WarPartridgesPikes_flk.001</ta>
            <ta e="T12" id="Seg_1971" s="T8">ErSV_1964_WarPartridgesPikes_flk.002</ta>
            <ta e="T18" id="Seg_1972" s="T12">ErSV_1964_WarPartridgesPikes_flk.003</ta>
            <ta e="T23" id="Seg_1973" s="T18">ErSV_1964_WarPartridgesPikes_flk.004</ta>
            <ta e="T31" id="Seg_1974" s="T23">ErSV_1964_WarPartridgesPikes_flk.005</ta>
            <ta e="T36" id="Seg_1975" s="T31">ErSV_1964_WarPartridgesPikes_flk.006</ta>
            <ta e="T51" id="Seg_1976" s="T36">ErSV_1964_WarPartridgesPikes_flk.007</ta>
            <ta e="T53" id="Seg_1977" s="T51">ErSV_1964_WarPartridgesPikes_flk.008</ta>
            <ta e="T57" id="Seg_1978" s="T53">ErSV_1964_WarPartridgesPikes_flk.009</ta>
            <ta e="T61" id="Seg_1979" s="T57">ErSV_1964_WarPartridgesPikes_flk.010</ta>
            <ta e="T63" id="Seg_1980" s="T61">ErSV_1964_WarPartridgesPikes_flk.011</ta>
            <ta e="T73" id="Seg_1981" s="T63">ErSV_1964_WarPartridgesPikes_flk.012</ta>
            <ta e="T79" id="Seg_1982" s="T73">ErSV_1964_WarPartridgesPikes_flk.013</ta>
            <ta e="T85" id="Seg_1983" s="T79">ErSV_1964_WarPartridgesPikes_flk.014</ta>
            <ta e="T92" id="Seg_1984" s="T85">ErSV_1964_WarPartridgesPikes_flk.015</ta>
            <ta e="T97" id="Seg_1985" s="T92">ErSV_1964_WarPartridgesPikes_flk.016</ta>
            <ta e="T103" id="Seg_1986" s="T97">ErSV_1964_WarPartridgesPikes_flk.017</ta>
            <ta e="T118" id="Seg_1987" s="T103">ErSV_1964_WarPartridgesPikes_flk.018</ta>
            <ta e="T123" id="Seg_1988" s="T118">ErSV_1964_WarPartridgesPikes_flk.019</ta>
            <ta e="T128" id="Seg_1989" s="T123">ErSV_1964_WarPartridgesPikes_flk.020</ta>
            <ta e="T129" id="Seg_1990" s="T128">ErSV_1964_WarPartridgesPikes_flk.021</ta>
            <ta e="T130" id="Seg_1991" s="T129">ErSV_1964_WarPartridgesPikes_flk.022</ta>
            <ta e="T134" id="Seg_1992" s="T130">ErSV_1964_WarPartridgesPikes_flk.023</ta>
            <ta e="T137" id="Seg_1993" s="T134">ErSV_1964_WarPartridgesPikes_flk.024</ta>
            <ta e="T139" id="Seg_1994" s="T137">ErSV_1964_WarPartridgesPikes_flk.025</ta>
            <ta e="T143" id="Seg_1995" s="T139">ErSV_1964_WarPartridgesPikes_flk.026</ta>
            <ta e="T148" id="Seg_1996" s="T143">ErSV_1964_WarPartridgesPikes_flk.027</ta>
            <ta e="T150" id="Seg_1997" s="T148">ErSV_1964_WarPartridgesPikes_flk.028</ta>
            <ta e="T157" id="Seg_1998" s="T150">ErSV_1964_WarPartridgesPikes_flk.029</ta>
            <ta e="T162" id="Seg_1999" s="T157">ErSV_1964_WarPartridgesPikes_flk.030</ta>
            <ta e="T174" id="Seg_2000" s="T162">ErSV_1964_WarPartridgesPikes_flk.031</ta>
            <ta e="T180" id="Seg_2001" s="T174">ErSV_1964_WarPartridgesPikes_flk.032</ta>
            <ta e="T196" id="Seg_2002" s="T180">ErSV_1964_WarPartridgesPikes_flk.033</ta>
            <ta e="T200" id="Seg_2003" s="T196">ErSV_1964_WarPartridgesPikes_flk.034</ta>
            <ta e="T203" id="Seg_2004" s="T200">ErSV_1964_WarPartridgesPikes_flk.035</ta>
            <ta e="T208" id="Seg_2005" s="T203">ErSV_1964_WarPartridgesPikes_flk.036</ta>
            <ta e="T210" id="Seg_2006" s="T208">ErSV_1964_WarPartridgesPikes_flk.037</ta>
            <ta e="T213" id="Seg_2007" s="T210">ErSV_1964_WarPartridgesPikes_flk.038</ta>
            <ta e="T217" id="Seg_2008" s="T213">ErSV_1964_WarPartridgesPikes_flk.039</ta>
            <ta e="T218" id="Seg_2009" s="T217">ErSV_1964_WarPartridgesPikes_flk.040</ta>
            <ta e="T223" id="Seg_2010" s="T218">ErSV_1964_WarPartridgesPikes_flk.041</ta>
            <ta e="T233" id="Seg_2011" s="T223">ErSV_1964_WarPartridgesPikes_flk.042</ta>
            <ta e="T240" id="Seg_2012" s="T233">ErSV_1964_WarPartridgesPikes_flk.043</ta>
            <ta e="T244" id="Seg_2013" s="T240">ErSV_1964_WarPartridgesPikes_flk.044</ta>
            <ta e="T248" id="Seg_2014" s="T244">ErSV_1964_WarPartridgesPikes_flk.045</ta>
            <ta e="T254" id="Seg_2015" s="T248">ErSV_1964_WarPartridgesPikes_flk.046</ta>
            <ta e="T258" id="Seg_2016" s="T254">ErSV_1964_WarPartridgesPikes_flk.047</ta>
            <ta e="T276" id="Seg_2017" s="T258">ErSV_1964_WarPartridgesPikes_flk.048</ta>
            <ta e="T279" id="Seg_2018" s="T276">ErSV_1964_WarPartridgesPikes_flk.049</ta>
            <ta e="T288" id="Seg_2019" s="T279">ErSV_1964_WarPartridgesPikes_flk.050</ta>
            <ta e="T303" id="Seg_2020" s="T288">ErSV_1964_WarPartridgesPikes_flk.051</ta>
            <ta e="T305" id="Seg_2021" s="T303">ErSV_1964_WarPartridgesPikes_flk.052</ta>
            <ta e="T309" id="Seg_2022" s="T305">ErSV_1964_WarPartridgesPikes_flk.053</ta>
            <ta e="T314" id="Seg_2023" s="T309">ErSV_1964_WarPartridgesPikes_flk.054</ta>
            <ta e="T316" id="Seg_2024" s="T314">ErSV_1964_WarPartridgesPikes_flk.055</ta>
            <ta e="T319" id="Seg_2025" s="T316">ErSV_1964_WarPartridgesPikes_flk.056</ta>
            <ta e="T328" id="Seg_2026" s="T319">ErSV_1964_WarPartridgesPikes_flk.057</ta>
            <ta e="T344" id="Seg_2027" s="T328">ErSV_1964_WarPartridgesPikes_flk.058</ta>
            <ta e="T345" id="Seg_2028" s="T344">ErSV_1964_WarPartridgesPikes_flk.059</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T8" id="Seg_2029" s="T0">Дьэ былыр, һир-таҥара үөскүүрүгэр, курпааскы муора һирдээгэ үһү.</ta>
            <ta e="T12" id="Seg_2030" s="T8">Биир дьыл пургаалаак буолбут.</ta>
            <ta e="T18" id="Seg_2031" s="T12">Кыһын һамыырдаабыт, каара барыта буус буолбут.</ta>
            <ta e="T23" id="Seg_2032" s="T18">Биир да кадьырык өттүтэ һуок.</ta>
            <ta e="T31" id="Seg_2033" s="T23">Курпааскылар коргуйбуттар, гинилэр ас көрдөнө мас диэкки көппүттэр.</ta>
            <ta e="T36" id="Seg_2034" s="T31">Биир муҥ кырдьагас һүбэһит курпааскылаактар.</ta>
            <ta e="T51" id="Seg_2035" s="T36">Маска тийбиттэр, канна да кадьырык көстүбэт, талак төбөтө да биллибэт, тибэн кээспит, мас агай караарар.</ta>
            <ta e="T53" id="Seg_2036" s="T51">һүбэһиттэрэ һүбэлиир:</ta>
            <ta e="T57" id="Seg_2037" s="T53">— Дьэ аны киһитийбэтибит биһиги.</ta>
            <ta e="T61" id="Seg_2038" s="T57">Канна баран өлүөкпүтүй биһиги?!</ta>
            <ta e="T63" id="Seg_2039" s="T61">Бугурдук дуумалыыбын.</ta>
            <ta e="T73" id="Seg_2040" s="T63">Каннык эрэ үйэгэ биир һири көрбүттээкпин, биир улаган үрэк баар.</ta>
            <ta e="T79" id="Seg_2041" s="T73">Ол үрэгиҥ кайдактаах пургаага тибиллээччитэ һуок.</ta>
            <ta e="T85" id="Seg_2042" s="T79">һорок һирэ кыһыны быһа һиикэй буолар.</ta>
            <ta e="T92" id="Seg_2043" s="T85">Кытылыттан талактаак, кайата барыта һэппэрээктээк, һугуннаак, оттоок.</ta>
            <ta e="T97" id="Seg_2044" s="T92">Ол үрэккэ куокаа гиэнэ олого.</ta>
            <ta e="T103" id="Seg_2045" s="T97">һыччак куокаалар агай биһигини чугаһатыактара һуога.</ta>
            <ta e="T118" id="Seg_2046" s="T103">Былыр үйэгэ андагай тыллаактар курпаасканы кытта: курпааскы куокаа дойдутугар барыа һуога диэн, куокаа — курпааскы дойдутугар.</ta>
            <ta e="T123" id="Seg_2047" s="T118">Өйдүүр буоллактырына үтүөннэн биэриэктэрэ һуога.</ta>
            <ta e="T128" id="Seg_2048" s="T123">Биһиги оччого һэрииннэн агай киириниэкпит.</ta>
            <ta e="T129" id="Seg_2049" s="T128">Курпааскылар:</ta>
            <ta e="T130" id="Seg_2050" s="T129">— Кирдик.</ta>
            <ta e="T134" id="Seg_2051" s="T130">Манна олорбутунан өлүөкпүт дуо?!</ta>
            <ta e="T137" id="Seg_2052" s="T134">Ол кэриэтин һэриилэһиэкпит.</ta>
            <ta e="T139" id="Seg_2053" s="T137">Барыагыҥ!— диэтилэр.</ta>
            <ta e="T143" id="Seg_2054" s="T139">Курпааскылар көтөллөр, үрэккэ тийэллэр.</ta>
            <ta e="T148" id="Seg_2055" s="T143">Ас нэлэйэ һытар һирэ эбит.</ta>
            <ta e="T150" id="Seg_2056" s="T148">Аһыы олорбуттар. </ta>
            <ta e="T157" id="Seg_2057" s="T150">һиикэй устун куокаа һүбэһитин мэйиитэ былтас гыммыт:</ta>
            <ta e="T162" id="Seg_2058" s="T157">— Кайа-кээ, курпааскылар, того һиргэ кэлбиккитий?</ta>
            <ta e="T174" id="Seg_2059" s="T162">Былыр үйэгэ андагай тыллаак этибит: эһиги һиргитигэр биһиги барбаппыт, эһиги — биһиги һирбитигэр.</ta>
            <ta e="T180" id="Seg_2060" s="T174">Того кэллигит, көстүмэҥ, һаныкаан барыҥ, мантан!</ta>
            <ta e="T196" id="Seg_2061" s="T180">Талак гиэнин һэбирдэгин, һэппэрээк гиэнин төбөтүн да һиэтиэм һуога, оппун да һиэппэппин, чайбын да кастарыам һуога!</ta>
            <ta e="T200" id="Seg_2062" s="T196">Онуга курпааску һүбэһитэ диир:</ta>
            <ta e="T203" id="Seg_2063" s="T200">— Биһиги үтүөннэн барбаппыт.</ta>
            <ta e="T208" id="Seg_2064" s="T203">һэрииннэн дааганы бу һири ылыакпыт!</ta>
            <ta e="T210" id="Seg_2065" s="T208">Куокаа һүбэһитэ:</ta>
            <ta e="T213" id="Seg_2066" s="T210">— Мин үтүөннэн биэрбэппин.</ta>
            <ta e="T217" id="Seg_2067" s="T213">Куокааыны имиччи өлөрдөккүтүнэ ылыаккыт.</ta>
            <ta e="T218" id="Seg_2068" s="T217">һэриилэһиэкпит.</ta>
            <ta e="T223" id="Seg_2069" s="T218">Токтооҥ — мин дьоммун комунуом!— диир.</ta>
            <ta e="T233" id="Seg_2070" s="T223">Куокаа һүбэһитэ эргиллэр да кутуругунан "бар" гыммыт, барбыт дьон комуна.</ta>
            <ta e="T240" id="Seg_2071" s="T233">Куокаа үөрүн барытын эгэлэн һиикэйи карааччы оҥордо.</ta>
            <ta e="T244" id="Seg_2072" s="T240">Курпааскылар бэлэм кытылга тураллар.</ta>
            <ta e="T248" id="Seg_2073" s="T244">Курпааскы һүбэһитэ дьонугар этэр:</ta>
            <ta e="T254" id="Seg_2074" s="T248">— Куокааны баска ытаарыҥ, агдатыгар өлөөччүтэ һуок!</ta>
            <ta e="T258" id="Seg_2075" s="T254">Курпааскы һэриитэ окторун ыыппыт.</ta>
            <ta e="T276" id="Seg_2076" s="T258">Алаҥаа кирситин тыаһын иһиллээт, куокаалар үөс диэки эргиллэ биэрэн аһарбыттар, курпааскылар окторо куокаа һигдэтигэр да һигдэтигэр биэрэн испит.</ta>
            <ta e="T279" id="Seg_2077" s="T276">Куокаалар эргиллэн кэлбиттэр.</ta>
            <ta e="T288" id="Seg_2078" s="T279">Курпааскылар ок ылыныактарыгар диэри куокаалар ыппыттар курпааскылар һүрэктэрин кыҥаан.</ta>
            <ta e="T303" id="Seg_2079" s="T288">Курпааскылар аһаран, көтө гына үөһэ диэки ойон биэрбиттэр, куокаалар окторо - курпааскылар гиэннэрин былчыҥҥа да былчыҥҥа (атактарыгар).</ta>
            <ta e="T305" id="Seg_2080" s="T303">Огордук һэриилэспиттэр.</ta>
            <ta e="T309" id="Seg_2081" s="T305">Кайалара да өлөрсүбэт, кыайсыбат.</ta>
            <ta e="T314" id="Seg_2082" s="T309">Окторо барыта баранан һэриилэһэн бүппүттэр.</ta>
            <ta e="T316" id="Seg_2083" s="T314">Куока һүбэһитэ:</ta>
            <ta e="T319" id="Seg_2084" s="T316">— Аны һэриилэһэн бүтүөгүҥ.</ta>
            <ta e="T328" id="Seg_2085" s="T319">Эһиги [канна] багарар көтө һылдьыҥ, канна багарар аһааҥ! — диир.</ta>
            <ta e="T344" id="Seg_2086" s="T328">һоччоттон курпааскылар окторо куокаа һигдэтигэр атырдьак оҥуок буолбуттар, куокаалар окторо курпааскы атагын былчыҥыгар иҥиир оҥуок буолбуттар.</ta>
            <ta e="T345" id="Seg_2087" s="T344">Элэтэ.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T8" id="Seg_2088" s="T0">Dʼe bɨlɨr, hir-taŋara ü͡ösküːrüger, kurpaːskɨ mu͡ora hirdeːge ühü. </ta>
            <ta e="T12" id="Seg_2089" s="T8">Biːr dʼɨl purgaːlaːk bu͡olbut. </ta>
            <ta e="T18" id="Seg_2090" s="T12">Kɨhɨn hamɨːrdaːbɨt, kaːra barɨta buːs bu͡olbut. </ta>
            <ta e="T23" id="Seg_2091" s="T18">Biːr da kadʼɨrɨk öttüte hu͡ok. </ta>
            <ta e="T31" id="Seg_2092" s="T23">Kurpaːskɨlar korgujbuttar, giniler as kördönö mas di͡ekki köppütter. </ta>
            <ta e="T36" id="Seg_2093" s="T31">Biːr muŋ kɨrdʼagas hübehit kurpaːskɨlaːktar. </ta>
            <ta e="T51" id="Seg_2094" s="T36">Maska tijbitter, kanna da kadʼɨrɨk köstübet, talak töbötö da billibet, tiben keːspit, mas agaj karaːrar. </ta>
            <ta e="T53" id="Seg_2095" s="T51">Hübehittere hübeliːr:</ta>
            <ta e="T57" id="Seg_2096" s="T53">"Dʼe anɨ kihitijbetibit bihigi. </ta>
            <ta e="T61" id="Seg_2097" s="T57">Kanna baran ölü͡ökpütüj bihigi? </ta>
            <ta e="T63" id="Seg_2098" s="T61">Bugurduk duːmalɨːbɨn. </ta>
            <ta e="T73" id="Seg_2099" s="T63">Kannɨk ere üjege biːr hiri körbütteːkpin, biːr ulagan ürek baːr. </ta>
            <ta e="T79" id="Seg_2100" s="T73">Ol üregiŋ kajdaktaːk purgaːga tibilleːččite hu͡ok. </ta>
            <ta e="T85" id="Seg_2101" s="T79">Horok hire kɨhɨnɨ bɨha hiːkej bu͡olar. </ta>
            <ta e="T92" id="Seg_2102" s="T85">Kɨtɨlɨttan talaktaːk, kajata barɨta heppereːkteːk, hugunnaːk, ottoːk. </ta>
            <ta e="T97" id="Seg_2103" s="T92">Ol ürekke ku͡okaː gi͡ene ologo. </ta>
            <ta e="T103" id="Seg_2104" s="T97">Hɨččak ku͡okaːlar agaj bihigini čugahatɨ͡aktara hu͡oga. </ta>
            <ta e="T118" id="Seg_2105" s="T103">Bɨlɨr üjege andagaj tɨllaːktar kurpaːskanɨ kɨtta, kurpaːskɨ ku͡okaː dojdutugar barɨ͡a hu͡oga di͡en, ku͡okaː — kurpaːskɨ dojdutugar. </ta>
            <ta e="T123" id="Seg_2106" s="T118">Öjdüːr bu͡ollaktɨrɨna ütü͡önnen bi͡eri͡ektere hu͡oga. </ta>
            <ta e="T128" id="Seg_2107" s="T123">Bihigi oččogo heriːnnen agaj kiːrini͡ekpit." </ta>
            <ta e="T129" id="Seg_2108" s="T128">Kurpaːskɨlar:</ta>
            <ta e="T130" id="Seg_2109" s="T129">"Kirdik. </ta>
            <ta e="T134" id="Seg_2110" s="T130">Manna olorbutunan ölü͡ökpüt du͡o? </ta>
            <ta e="T137" id="Seg_2111" s="T134">Ol keri͡etin heriːlehi͡ekpit. </ta>
            <ta e="T139" id="Seg_2112" s="T137">Barɨ͡agɨŋ", di͡etiler. </ta>
            <ta e="T143" id="Seg_2113" s="T139">Kurpaːskɨlar kötöllör, ürekke tijeller. </ta>
            <ta e="T148" id="Seg_2114" s="T143">As neleje hɨtar hire ebit. </ta>
            <ta e="T150" id="Seg_2115" s="T148">Ahɨː olorbuttar. </ta>
            <ta e="T157" id="Seg_2116" s="T150">Hiːkej ustun ku͡okaː hübehitin mejiːte bɨltas gɨmmɨt:</ta>
            <ta e="T162" id="Seg_2117" s="T157">"Kaja-keː, kurpaːskɨlar, togo hirge kelbikkitij? </ta>
            <ta e="T174" id="Seg_2118" s="T162">Bɨlɨr üjege andagaj tɨllaːk etibit, ehigi hirgitiger bihigi barbappɨt, ehigi — bihigi hirbitiger. </ta>
            <ta e="T180" id="Seg_2119" s="T174">Togo kelligit, köstümeŋ, hanɨkaːn barɨŋ, mantan! </ta>
            <ta e="T196" id="Seg_2120" s="T180">Talak gi͡enin hebirdegin, heppereːk gi͡enin töbötün da hi͡eti͡em hu͡oga, oppun da hi͡eppeppin, čajbɨn da kastarɨ͡am hu͡oga!" </ta>
            <ta e="T200" id="Seg_2121" s="T196">Onuga kurpaːsku hübehite diːr:</ta>
            <ta e="T203" id="Seg_2122" s="T200">"Bihigi ütü͡önnen barbappɨt. </ta>
            <ta e="T208" id="Seg_2123" s="T203">Heriːnnen daːganɨ bu hiri ɨlɨ͡akpɨt!" </ta>
            <ta e="T210" id="Seg_2124" s="T208">Ku͡okaː hübehite:</ta>
            <ta e="T213" id="Seg_2125" s="T210">"Min ütü͡önnen bi͡erbeppin. </ta>
            <ta e="T217" id="Seg_2126" s="T213">Ku͡okaːnɨ imičči ölördökkütüne ɨlɨ͡akkɨt. </ta>
            <ta e="T218" id="Seg_2127" s="T217">Heriːlehi͡ekpit. </ta>
            <ta e="T223" id="Seg_2128" s="T218">Toktoːŋ — min dʼommun komunu͡om", diːr. </ta>
            <ta e="T233" id="Seg_2129" s="T223">Ku͡okaː hübehite ergiller da kuturugunan "bar" gɨmmɨt, barbɨt dʼon komuna. </ta>
            <ta e="T240" id="Seg_2130" s="T233">Ku͡okaː ü͡örün barɨtɨn egelen hiːkeji karaːččɨ oŋordo. </ta>
            <ta e="T244" id="Seg_2131" s="T240">Kurpaːskɨlar belem kɨtɨlga turallar. </ta>
            <ta e="T248" id="Seg_2132" s="T244">Kurpaːskɨ hübehite dʼonugar eter:</ta>
            <ta e="T254" id="Seg_2133" s="T248">"Ku͡okaːnɨ baska ɨtaːrɨŋ, agdatɨgar ölöːččüte hu͡ok!" </ta>
            <ta e="T258" id="Seg_2134" s="T254">Kurpaːskɨ heriːte oktorun ɨːppɨt. </ta>
            <ta e="T276" id="Seg_2135" s="T258">Alaŋaː kirsitin tɨ͡ahɨn ihilleːt, ku͡okaːlar ü͡ös di͡eki ergille bi͡eren aharbɨttar, kurpaːskɨlar oktoro ku͡okaː higdetiger da higdetiger bi͡eren ispit. </ta>
            <ta e="T279" id="Seg_2136" s="T276">Ku͡okaːlar ergillen kelbitter. </ta>
            <ta e="T288" id="Seg_2137" s="T279">Kurpaːskɨlar ok ɨlɨnɨ͡aktarɨgar di͡eri ku͡okaːlar ɨppɨttar kurpaːskɨlar hürekterin kɨŋaːn. </ta>
            <ta e="T303" id="Seg_2138" s="T288">Kurpaːskɨlar aharan, kötö gɨna ü͡öhe di͡eki ojon bi͡erbitter, ku͡okaːlar oktoro — kurpaːskɨlar gi͡ennerin bɨlčɨŋŋa da bɨlčɨŋŋa ataktarɨgar. </ta>
            <ta e="T305" id="Seg_2139" s="T303">Ogorduk heriːlespitter. </ta>
            <ta e="T309" id="Seg_2140" s="T305">Kajalara da ölörsübet, kɨ͡ajsɨbat. </ta>
            <ta e="T314" id="Seg_2141" s="T309">Oktoro barɨta baranan heriːlehen büppütter. </ta>
            <ta e="T316" id="Seg_2142" s="T314">Ku͡oka hübehite:</ta>
            <ta e="T319" id="Seg_2143" s="T316">"Anɨ heriːlehen bütü͡ögüŋ. </ta>
            <ta e="T328" id="Seg_2144" s="T319">Ehigi kanna bagarar kötö hɨldʼɨŋ, kanna bagarar ahaːŋ", diːr. </ta>
            <ta e="T344" id="Seg_2145" s="T328">Hoččotton kurpaːskɨlar oktoro ku͡okaː higdetiger atɨrdʼak oŋu͡ok bu͡olbuttar, ku͡okaːlar oktoro kurpaːskɨ atagɨn bɨlčɨŋɨgar iŋiːr oŋu͡ok bu͡olbuttar. </ta>
            <ta e="T345" id="Seg_2146" s="T344">Elete. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_2147" s="T0">dʼe</ta>
            <ta e="T2" id="Seg_2148" s="T1">bɨlɨr</ta>
            <ta e="T3" id="Seg_2149" s="T2">hir-taŋara</ta>
            <ta e="T4" id="Seg_2150" s="T3">ü͡ösküː-r-ü-ger</ta>
            <ta e="T5" id="Seg_2151" s="T4">kurpaːskɨ</ta>
            <ta e="T6" id="Seg_2152" s="T5">mu͡ora</ta>
            <ta e="T7" id="Seg_2153" s="T6">hir-deːge</ta>
            <ta e="T8" id="Seg_2154" s="T7">ühü</ta>
            <ta e="T9" id="Seg_2155" s="T8">biːr</ta>
            <ta e="T10" id="Seg_2156" s="T9">dʼɨl</ta>
            <ta e="T11" id="Seg_2157" s="T10">purgaː-laːk</ta>
            <ta e="T12" id="Seg_2158" s="T11">bu͡ol-but</ta>
            <ta e="T13" id="Seg_2159" s="T12">kɨhɨn</ta>
            <ta e="T14" id="Seg_2160" s="T13">hamɨːrdaː-bɨt</ta>
            <ta e="T15" id="Seg_2161" s="T14">kaːr-a</ta>
            <ta e="T16" id="Seg_2162" s="T15">barɨ-ta</ta>
            <ta e="T17" id="Seg_2163" s="T16">buːs</ta>
            <ta e="T18" id="Seg_2164" s="T17">bu͡ol-but</ta>
            <ta e="T19" id="Seg_2165" s="T18">biːr</ta>
            <ta e="T20" id="Seg_2166" s="T19">da</ta>
            <ta e="T21" id="Seg_2167" s="T20">kadʼɨrɨk</ta>
            <ta e="T22" id="Seg_2168" s="T21">öttü-te</ta>
            <ta e="T23" id="Seg_2169" s="T22">hu͡ok</ta>
            <ta e="T24" id="Seg_2170" s="T23">kurpaːskɨ-lar</ta>
            <ta e="T25" id="Seg_2171" s="T24">korguj-but-tar</ta>
            <ta e="T26" id="Seg_2172" s="T25">giniler</ta>
            <ta e="T27" id="Seg_2173" s="T26">as</ta>
            <ta e="T28" id="Seg_2174" s="T27">kördön-ö</ta>
            <ta e="T29" id="Seg_2175" s="T28">mas</ta>
            <ta e="T30" id="Seg_2176" s="T29">di͡ekki</ta>
            <ta e="T31" id="Seg_2177" s="T30">köp-püt-ter</ta>
            <ta e="T32" id="Seg_2178" s="T31">biːr</ta>
            <ta e="T33" id="Seg_2179" s="T32">muŋ</ta>
            <ta e="T34" id="Seg_2180" s="T33">kɨrdʼagas</ta>
            <ta e="T35" id="Seg_2181" s="T34">hübehit</ta>
            <ta e="T36" id="Seg_2182" s="T35">kurpaːskɨ-laːk-tar</ta>
            <ta e="T37" id="Seg_2183" s="T36">mas-ka</ta>
            <ta e="T38" id="Seg_2184" s="T37">tij-bit-ter</ta>
            <ta e="T39" id="Seg_2185" s="T38">kanna</ta>
            <ta e="T40" id="Seg_2186" s="T39">da</ta>
            <ta e="T41" id="Seg_2187" s="T40">kadʼɨrɨk</ta>
            <ta e="T42" id="Seg_2188" s="T41">köst-ü-bet</ta>
            <ta e="T43" id="Seg_2189" s="T42">talak</ta>
            <ta e="T44" id="Seg_2190" s="T43">töbö-tö</ta>
            <ta e="T45" id="Seg_2191" s="T44">da</ta>
            <ta e="T46" id="Seg_2192" s="T45">bil-l-i-bet</ta>
            <ta e="T47" id="Seg_2193" s="T46">tib-en</ta>
            <ta e="T48" id="Seg_2194" s="T47">keːs-pit</ta>
            <ta e="T49" id="Seg_2195" s="T48">mas</ta>
            <ta e="T50" id="Seg_2196" s="T49">agaj</ta>
            <ta e="T51" id="Seg_2197" s="T50">karaːr-ar</ta>
            <ta e="T52" id="Seg_2198" s="T51">hübehit-tere</ta>
            <ta e="T53" id="Seg_2199" s="T52">hübeliː-r</ta>
            <ta e="T54" id="Seg_2200" s="T53">dʼe</ta>
            <ta e="T55" id="Seg_2201" s="T54">anɨ</ta>
            <ta e="T56" id="Seg_2202" s="T55">kihitij-be-ti-bit</ta>
            <ta e="T57" id="Seg_2203" s="T56">bihigi</ta>
            <ta e="T58" id="Seg_2204" s="T57">kanna</ta>
            <ta e="T59" id="Seg_2205" s="T58">bar-an</ta>
            <ta e="T60" id="Seg_2206" s="T59">öl-ü͡ök-püt=üj</ta>
            <ta e="T61" id="Seg_2207" s="T60">bihigi</ta>
            <ta e="T62" id="Seg_2208" s="T61">bugurduk</ta>
            <ta e="T63" id="Seg_2209" s="T62">duːmal-ɨː-bɨn</ta>
            <ta e="T64" id="Seg_2210" s="T63">kannɨk</ta>
            <ta e="T65" id="Seg_2211" s="T64">ere</ta>
            <ta e="T66" id="Seg_2212" s="T65">üje-ge</ta>
            <ta e="T67" id="Seg_2213" s="T66">biːr</ta>
            <ta e="T68" id="Seg_2214" s="T67">hir-i</ta>
            <ta e="T69" id="Seg_2215" s="T68">kör-büt-teːk-pin</ta>
            <ta e="T70" id="Seg_2216" s="T69">biːr</ta>
            <ta e="T71" id="Seg_2217" s="T70">ulagan</ta>
            <ta e="T72" id="Seg_2218" s="T71">ürek</ta>
            <ta e="T73" id="Seg_2219" s="T72">baːr</ta>
            <ta e="T74" id="Seg_2220" s="T73">ol</ta>
            <ta e="T75" id="Seg_2221" s="T74">üreg-i-ŋ</ta>
            <ta e="T76" id="Seg_2222" s="T75">kajdak-taːk</ta>
            <ta e="T77" id="Seg_2223" s="T76">purgaː-ga</ta>
            <ta e="T78" id="Seg_2224" s="T77">tib-i-ll-eːčči-te</ta>
            <ta e="T79" id="Seg_2225" s="T78">hu͡ok</ta>
            <ta e="T80" id="Seg_2226" s="T79">horok</ta>
            <ta e="T81" id="Seg_2227" s="T80">hir-e</ta>
            <ta e="T82" id="Seg_2228" s="T81">kɨhɨn-ɨ</ta>
            <ta e="T83" id="Seg_2229" s="T82">bɨha</ta>
            <ta e="T84" id="Seg_2230" s="T83">hiːkej</ta>
            <ta e="T85" id="Seg_2231" s="T84">bu͡ol-ar</ta>
            <ta e="T86" id="Seg_2232" s="T85">kɨtɨl-ɨ-ttan</ta>
            <ta e="T87" id="Seg_2233" s="T86">talak-taːk</ta>
            <ta e="T88" id="Seg_2234" s="T87">kaja-ta</ta>
            <ta e="T89" id="Seg_2235" s="T88">barɨta</ta>
            <ta e="T90" id="Seg_2236" s="T89">heppereːk-teːk</ta>
            <ta e="T91" id="Seg_2237" s="T90">hugun-naːk</ta>
            <ta e="T92" id="Seg_2238" s="T91">ot-toːk</ta>
            <ta e="T93" id="Seg_2239" s="T92">ol</ta>
            <ta e="T94" id="Seg_2240" s="T93">ürek-ke</ta>
            <ta e="T95" id="Seg_2241" s="T94">ku͡okaː</ta>
            <ta e="T96" id="Seg_2242" s="T95">gi͡en-e</ta>
            <ta e="T97" id="Seg_2243" s="T96">olog-o</ta>
            <ta e="T98" id="Seg_2244" s="T97">hɨččak</ta>
            <ta e="T99" id="Seg_2245" s="T98">ku͡okaː-lar</ta>
            <ta e="T100" id="Seg_2246" s="T99">agaj</ta>
            <ta e="T101" id="Seg_2247" s="T100">bihigi-ni</ta>
            <ta e="T102" id="Seg_2248" s="T101">čugah-a-t-ɨ͡ak-tara</ta>
            <ta e="T103" id="Seg_2249" s="T102">hu͡og-a</ta>
            <ta e="T104" id="Seg_2250" s="T103">bɨlɨr</ta>
            <ta e="T105" id="Seg_2251" s="T104">üje-ge</ta>
            <ta e="T106" id="Seg_2252" s="T105">andagaj</ta>
            <ta e="T107" id="Seg_2253" s="T106">tɨl-laːk-tar</ta>
            <ta e="T108" id="Seg_2254" s="T107">kurpaːska-nɨ</ta>
            <ta e="T109" id="Seg_2255" s="T108">kɨtta</ta>
            <ta e="T110" id="Seg_2256" s="T109">kurpaːskɨ</ta>
            <ta e="T111" id="Seg_2257" s="T110">ku͡okaː</ta>
            <ta e="T112" id="Seg_2258" s="T111">dojdu-tu-gar</ta>
            <ta e="T113" id="Seg_2259" s="T112">bar-ɨ͡a</ta>
            <ta e="T114" id="Seg_2260" s="T113">hu͡og-a</ta>
            <ta e="T115" id="Seg_2261" s="T114">di͡e-n</ta>
            <ta e="T116" id="Seg_2262" s="T115">ku͡okaː</ta>
            <ta e="T117" id="Seg_2263" s="T116">kurpaːskɨ</ta>
            <ta e="T118" id="Seg_2264" s="T117">dojdu-tu-gar</ta>
            <ta e="T119" id="Seg_2265" s="T118">öjdüː-r</ta>
            <ta e="T120" id="Seg_2266" s="T119">bu͡ol-lak-tɨrɨna</ta>
            <ta e="T121" id="Seg_2267" s="T120">ütü͡ö-nnen</ta>
            <ta e="T122" id="Seg_2268" s="T121">bi͡er-i͡ek-tere</ta>
            <ta e="T123" id="Seg_2269" s="T122">hu͡og-a</ta>
            <ta e="T124" id="Seg_2270" s="T123">bihigi</ta>
            <ta e="T125" id="Seg_2271" s="T124">oččogo</ta>
            <ta e="T126" id="Seg_2272" s="T125">heriː-nnen</ta>
            <ta e="T127" id="Seg_2273" s="T126">agaj</ta>
            <ta e="T128" id="Seg_2274" s="T127">kiːr-i-n-i͡ek-pit</ta>
            <ta e="T129" id="Seg_2275" s="T128">kurpaːskɨ-lar</ta>
            <ta e="T130" id="Seg_2276" s="T129">kirdik</ta>
            <ta e="T131" id="Seg_2277" s="T130">manna</ta>
            <ta e="T132" id="Seg_2278" s="T131">olor-but-u-nan</ta>
            <ta e="T133" id="Seg_2279" s="T132">öl-ü͡ök-püt</ta>
            <ta e="T134" id="Seg_2280" s="T133">du͡o</ta>
            <ta e="T135" id="Seg_2281" s="T134">ol</ta>
            <ta e="T136" id="Seg_2282" s="T135">keri͡etin</ta>
            <ta e="T137" id="Seg_2283" s="T136">heriːleh-i͡ek-pit</ta>
            <ta e="T138" id="Seg_2284" s="T137">bar-ɨ͡agɨŋ</ta>
            <ta e="T139" id="Seg_2285" s="T138">di͡e-ti-ler</ta>
            <ta e="T140" id="Seg_2286" s="T139">kurpaːskɨ-lar</ta>
            <ta e="T141" id="Seg_2287" s="T140">köt-öl-lör</ta>
            <ta e="T142" id="Seg_2288" s="T141">ürek-ke</ta>
            <ta e="T143" id="Seg_2289" s="T142">tij-el-ler</ta>
            <ta e="T144" id="Seg_2290" s="T143">as</ta>
            <ta e="T145" id="Seg_2291" s="T144">nelej-e</ta>
            <ta e="T146" id="Seg_2292" s="T145">hɨt-ar</ta>
            <ta e="T147" id="Seg_2293" s="T146">hir-e</ta>
            <ta e="T148" id="Seg_2294" s="T147">e-bit</ta>
            <ta e="T149" id="Seg_2295" s="T148">ah-ɨː</ta>
            <ta e="T150" id="Seg_2296" s="T149">olor-but-tar</ta>
            <ta e="T151" id="Seg_2297" s="T150">hiːkej</ta>
            <ta e="T152" id="Seg_2298" s="T151">ustun</ta>
            <ta e="T153" id="Seg_2299" s="T152">ku͡okaː</ta>
            <ta e="T154" id="Seg_2300" s="T153">hübehit-i-n</ta>
            <ta e="T155" id="Seg_2301" s="T154">mejiː-te</ta>
            <ta e="T156" id="Seg_2302" s="T155">bɨlta-s</ta>
            <ta e="T157" id="Seg_2303" s="T156">gɨm-mɨt</ta>
            <ta e="T158" id="Seg_2304" s="T157">kaja=keː</ta>
            <ta e="T159" id="Seg_2305" s="T158">kurpaːskɨ-lar</ta>
            <ta e="T160" id="Seg_2306" s="T159">togo</ta>
            <ta e="T161" id="Seg_2307" s="T160">hir-ge</ta>
            <ta e="T162" id="Seg_2308" s="T161">kel-bik-kit=ij</ta>
            <ta e="T163" id="Seg_2309" s="T162">bɨlɨr</ta>
            <ta e="T164" id="Seg_2310" s="T163">üje-ge</ta>
            <ta e="T165" id="Seg_2311" s="T164">andagaj</ta>
            <ta e="T166" id="Seg_2312" s="T165">tɨl-laːk</ta>
            <ta e="T167" id="Seg_2313" s="T166">e-ti-bit</ta>
            <ta e="T168" id="Seg_2314" s="T167">ehigi</ta>
            <ta e="T169" id="Seg_2315" s="T168">hir-giti-ger</ta>
            <ta e="T170" id="Seg_2316" s="T169">bihigi</ta>
            <ta e="T171" id="Seg_2317" s="T170">bar-bap-pɨt</ta>
            <ta e="T172" id="Seg_2318" s="T171">ehigi</ta>
            <ta e="T173" id="Seg_2319" s="T172">bihigi</ta>
            <ta e="T174" id="Seg_2320" s="T173">hir-biti-ger</ta>
            <ta e="T175" id="Seg_2321" s="T174">togo</ta>
            <ta e="T176" id="Seg_2322" s="T175">kel-li-git</ta>
            <ta e="T177" id="Seg_2323" s="T176">köst-ü-me-ŋ</ta>
            <ta e="T178" id="Seg_2324" s="T177">hanɨkaːn</ta>
            <ta e="T179" id="Seg_2325" s="T178">bar-ɨ-ŋ</ta>
            <ta e="T180" id="Seg_2326" s="T179">mantan</ta>
            <ta e="T181" id="Seg_2327" s="T180">talak</ta>
            <ta e="T182" id="Seg_2328" s="T181">gi͡en-i-n</ta>
            <ta e="T183" id="Seg_2329" s="T182">hebirdeg-i-n</ta>
            <ta e="T184" id="Seg_2330" s="T183">heppereːk</ta>
            <ta e="T185" id="Seg_2331" s="T184">gi͡en-i-n</ta>
            <ta e="T186" id="Seg_2332" s="T185">töbö-tü-n</ta>
            <ta e="T187" id="Seg_2333" s="T186">da</ta>
            <ta e="T188" id="Seg_2334" s="T187">hi͡e-t-i͡e-m</ta>
            <ta e="T189" id="Seg_2335" s="T188">hu͡og-a</ta>
            <ta e="T190" id="Seg_2336" s="T189">op-pu-n</ta>
            <ta e="T191" id="Seg_2337" s="T190">da</ta>
            <ta e="T192" id="Seg_2338" s="T191">hi͡e-p-pep-pin</ta>
            <ta e="T193" id="Seg_2339" s="T192">čaj-bɨ-n</ta>
            <ta e="T194" id="Seg_2340" s="T193">da</ta>
            <ta e="T195" id="Seg_2341" s="T194">kas-tar-ɨ͡a-m</ta>
            <ta e="T196" id="Seg_2342" s="T195">hu͡og-a</ta>
            <ta e="T197" id="Seg_2343" s="T196">onu-ga</ta>
            <ta e="T198" id="Seg_2344" s="T197">kurpaːsku</ta>
            <ta e="T199" id="Seg_2345" s="T198">hübehit-e</ta>
            <ta e="T200" id="Seg_2346" s="T199">diː-r</ta>
            <ta e="T201" id="Seg_2347" s="T200">bihigi</ta>
            <ta e="T202" id="Seg_2348" s="T201">ütü͡ö-nnen</ta>
            <ta e="T203" id="Seg_2349" s="T202">bar-bap-pɨt</ta>
            <ta e="T204" id="Seg_2350" s="T203">heriː-nnen</ta>
            <ta e="T205" id="Seg_2351" s="T204">daːganɨ</ta>
            <ta e="T206" id="Seg_2352" s="T205">bu</ta>
            <ta e="T207" id="Seg_2353" s="T206">hir-i</ta>
            <ta e="T208" id="Seg_2354" s="T207">ɨl-ɨ͡ak-pɨt</ta>
            <ta e="T209" id="Seg_2355" s="T208">ku͡okaː</ta>
            <ta e="T210" id="Seg_2356" s="T209">hübehit-e</ta>
            <ta e="T211" id="Seg_2357" s="T210">min</ta>
            <ta e="T212" id="Seg_2358" s="T211">ütü͡ö-nnen</ta>
            <ta e="T213" id="Seg_2359" s="T212">bi͡er-bep-pin</ta>
            <ta e="T214" id="Seg_2360" s="T213">ku͡okaː-nɨ</ta>
            <ta e="T215" id="Seg_2361" s="T214">imičči</ta>
            <ta e="T216" id="Seg_2362" s="T215">ölör-dök-kütüne</ta>
            <ta e="T217" id="Seg_2363" s="T216">ɨl-ɨ͡ak-kɨt</ta>
            <ta e="T218" id="Seg_2364" s="T217">heriːleh-i͡ek-pit</ta>
            <ta e="T219" id="Seg_2365" s="T218">toktoː-ŋ</ta>
            <ta e="T220" id="Seg_2366" s="T219">min</ta>
            <ta e="T221" id="Seg_2367" s="T220">dʼom-mu-n</ta>
            <ta e="T222" id="Seg_2368" s="T221">komu-n-u͡o-m</ta>
            <ta e="T223" id="Seg_2369" s="T222">diː-r</ta>
            <ta e="T224" id="Seg_2370" s="T223">ku͡okaː</ta>
            <ta e="T225" id="Seg_2371" s="T224">hübehit-e</ta>
            <ta e="T226" id="Seg_2372" s="T225">ergill-er</ta>
            <ta e="T227" id="Seg_2373" s="T226">da</ta>
            <ta e="T228" id="Seg_2374" s="T227">kuturug-u-nan</ta>
            <ta e="T229" id="Seg_2375" s="T228">bar</ta>
            <ta e="T230" id="Seg_2376" s="T229">gɨm-mɨt</ta>
            <ta e="T231" id="Seg_2377" s="T230">bar-bɨt</ta>
            <ta e="T232" id="Seg_2378" s="T231">dʼon</ta>
            <ta e="T233" id="Seg_2379" s="T232">komu-n-a</ta>
            <ta e="T234" id="Seg_2380" s="T233">ku͡okaː</ta>
            <ta e="T235" id="Seg_2381" s="T234">ü͡ör-ü-n</ta>
            <ta e="T236" id="Seg_2382" s="T235">barɨ-tɨ-n</ta>
            <ta e="T237" id="Seg_2383" s="T236">egel-en</ta>
            <ta e="T238" id="Seg_2384" s="T237">hiːkej-i</ta>
            <ta e="T239" id="Seg_2385" s="T238">karaː-ččɨ</ta>
            <ta e="T240" id="Seg_2386" s="T239">oŋor-d-o</ta>
            <ta e="T241" id="Seg_2387" s="T240">kurpaːskɨ-lar</ta>
            <ta e="T242" id="Seg_2388" s="T241">belem</ta>
            <ta e="T243" id="Seg_2389" s="T242">kɨtɨl-ga</ta>
            <ta e="T244" id="Seg_2390" s="T243">tur-al-lar</ta>
            <ta e="T245" id="Seg_2391" s="T244">kurpaːskɨ</ta>
            <ta e="T246" id="Seg_2392" s="T245">hübehit-e</ta>
            <ta e="T247" id="Seg_2393" s="T246">dʼon-u-gar</ta>
            <ta e="T248" id="Seg_2394" s="T247">et-er</ta>
            <ta e="T249" id="Seg_2395" s="T248">ku͡okaː-nɨ</ta>
            <ta e="T250" id="Seg_2396" s="T249">bas-ka</ta>
            <ta e="T251" id="Seg_2397" s="T250">ɨt-aːr-ɨ-ŋ</ta>
            <ta e="T252" id="Seg_2398" s="T251">agda-tɨ-gar</ta>
            <ta e="T253" id="Seg_2399" s="T252">öl-öːččü-te</ta>
            <ta e="T254" id="Seg_2400" s="T253">hu͡ok</ta>
            <ta e="T255" id="Seg_2401" s="T254">kurpaːskɨ</ta>
            <ta e="T256" id="Seg_2402" s="T255">heriː-te</ta>
            <ta e="T257" id="Seg_2403" s="T256">ok-tor-u-n</ta>
            <ta e="T258" id="Seg_2404" s="T257">ɨːp-pɨt</ta>
            <ta e="T259" id="Seg_2405" s="T258">alaŋaː</ta>
            <ta e="T260" id="Seg_2406" s="T259">kirs-i-ti-n</ta>
            <ta e="T261" id="Seg_2407" s="T260">tɨ͡ah-ɨ-n</ta>
            <ta e="T262" id="Seg_2408" s="T261">ihill-eːt</ta>
            <ta e="T263" id="Seg_2409" s="T262">ku͡okaː-lar</ta>
            <ta e="T264" id="Seg_2410" s="T263">ü͡ös</ta>
            <ta e="T265" id="Seg_2411" s="T264">di͡eki</ta>
            <ta e="T266" id="Seg_2412" s="T265">ergill-e</ta>
            <ta e="T267" id="Seg_2413" s="T266">bi͡er-en</ta>
            <ta e="T268" id="Seg_2414" s="T267">ahar-bɨt-tar</ta>
            <ta e="T269" id="Seg_2415" s="T268">kurpaːskɨ-lar</ta>
            <ta e="T270" id="Seg_2416" s="T269">ok-toro</ta>
            <ta e="T271" id="Seg_2417" s="T270">ku͡okaː</ta>
            <ta e="T272" id="Seg_2418" s="T271">higde-ti-ger</ta>
            <ta e="T273" id="Seg_2419" s="T272">da</ta>
            <ta e="T274" id="Seg_2420" s="T273">higde-ti-ger</ta>
            <ta e="T275" id="Seg_2421" s="T274">bi͡er-en</ta>
            <ta e="T276" id="Seg_2422" s="T275">is-pit</ta>
            <ta e="T277" id="Seg_2423" s="T276">ku͡okaː-lar</ta>
            <ta e="T278" id="Seg_2424" s="T277">ergill-en</ta>
            <ta e="T279" id="Seg_2425" s="T278">kel-bit-ter</ta>
            <ta e="T280" id="Seg_2426" s="T279">kurpaːskɨ-lar</ta>
            <ta e="T281" id="Seg_2427" s="T280">ok</ta>
            <ta e="T282" id="Seg_2428" s="T281">ɨlɨn-ɨ͡ak-tarɨ-gar</ta>
            <ta e="T283" id="Seg_2429" s="T282">di͡eri</ta>
            <ta e="T284" id="Seg_2430" s="T283">ku͡okaː-lar</ta>
            <ta e="T285" id="Seg_2431" s="T284">ɨp-pɨt-tar</ta>
            <ta e="T286" id="Seg_2432" s="T285">kurpaːskɨ-lar</ta>
            <ta e="T287" id="Seg_2433" s="T286">hürek-teri-n</ta>
            <ta e="T288" id="Seg_2434" s="T287">kɨŋaː-n</ta>
            <ta e="T289" id="Seg_2435" s="T288">kurpaːskɨ-lar</ta>
            <ta e="T290" id="Seg_2436" s="T289">ahar-an</ta>
            <ta e="T291" id="Seg_2437" s="T290">köt-ö</ta>
            <ta e="T292" id="Seg_2438" s="T291">gɨn-a</ta>
            <ta e="T293" id="Seg_2439" s="T292">ü͡öh-e</ta>
            <ta e="T294" id="Seg_2440" s="T293">di͡eki</ta>
            <ta e="T295" id="Seg_2441" s="T294">oj-on</ta>
            <ta e="T296" id="Seg_2442" s="T295">bi͡er-bit-ter</ta>
            <ta e="T297" id="Seg_2443" s="T296">ku͡okaː-lar</ta>
            <ta e="T346" id="Seg_2444" s="T297">ok-toro</ta>
            <ta e="T298" id="Seg_2445" s="T346">kurpaːskɨ-lar</ta>
            <ta e="T299" id="Seg_2446" s="T298">gi͡en-neri-n</ta>
            <ta e="T300" id="Seg_2447" s="T299">bɨlčɨŋ-ŋa</ta>
            <ta e="T301" id="Seg_2448" s="T300">da</ta>
            <ta e="T302" id="Seg_2449" s="T301">bɨlčɨŋ-ŋa</ta>
            <ta e="T303" id="Seg_2450" s="T302">atak-tarɨ-gar</ta>
            <ta e="T304" id="Seg_2451" s="T303">ogorduk</ta>
            <ta e="T305" id="Seg_2452" s="T304">heriːles-pit-ter</ta>
            <ta e="T306" id="Seg_2453" s="T305">kaja-lara</ta>
            <ta e="T307" id="Seg_2454" s="T306">da</ta>
            <ta e="T308" id="Seg_2455" s="T307">ölör-s-ü-bet</ta>
            <ta e="T309" id="Seg_2456" s="T308">kɨ͡aj-s-ɨ-bat</ta>
            <ta e="T310" id="Seg_2457" s="T309">ok-tor-o</ta>
            <ta e="T311" id="Seg_2458" s="T310">barɨta</ta>
            <ta e="T312" id="Seg_2459" s="T311">baran-an</ta>
            <ta e="T313" id="Seg_2460" s="T312">heriːleh-en</ta>
            <ta e="T314" id="Seg_2461" s="T313">büp-püt-ter</ta>
            <ta e="T315" id="Seg_2462" s="T314">ku͡oka</ta>
            <ta e="T316" id="Seg_2463" s="T315">hübehit-e</ta>
            <ta e="T317" id="Seg_2464" s="T316">anɨ</ta>
            <ta e="T318" id="Seg_2465" s="T317">heriːleh-en</ta>
            <ta e="T319" id="Seg_2466" s="T318">büt-ü͡ögüŋ</ta>
            <ta e="T320" id="Seg_2467" s="T319">ehigi</ta>
            <ta e="T321" id="Seg_2468" s="T320">kanna</ta>
            <ta e="T322" id="Seg_2469" s="T321">bagar-ar</ta>
            <ta e="T323" id="Seg_2470" s="T322">köt-ö</ta>
            <ta e="T324" id="Seg_2471" s="T323">hɨldʼ-ɨ-ŋ</ta>
            <ta e="T325" id="Seg_2472" s="T324">kanna</ta>
            <ta e="T326" id="Seg_2473" s="T325">bagar-ar</ta>
            <ta e="T327" id="Seg_2474" s="T326">ahaː-ŋ</ta>
            <ta e="T328" id="Seg_2475" s="T327">diː-r</ta>
            <ta e="T329" id="Seg_2476" s="T328">hoččotton</ta>
            <ta e="T330" id="Seg_2477" s="T329">kurpaːskɨ-lar</ta>
            <ta e="T331" id="Seg_2478" s="T330">ok-toro</ta>
            <ta e="T332" id="Seg_2479" s="T331">ku͡okaː</ta>
            <ta e="T333" id="Seg_2480" s="T332">higde-ti-ger</ta>
            <ta e="T334" id="Seg_2481" s="T333">atɨrdʼak</ta>
            <ta e="T335" id="Seg_2482" s="T334">oŋu͡ok</ta>
            <ta e="T336" id="Seg_2483" s="T335">bu͡ol-but-tar</ta>
            <ta e="T337" id="Seg_2484" s="T336">ku͡okaː-lar</ta>
            <ta e="T338" id="Seg_2485" s="T337">ok-toro</ta>
            <ta e="T339" id="Seg_2486" s="T338">kurpaːskɨ</ta>
            <ta e="T340" id="Seg_2487" s="T339">atag-ɨ-n</ta>
            <ta e="T341" id="Seg_2488" s="T340">bɨlčɨŋ-ɨ-gar</ta>
            <ta e="T342" id="Seg_2489" s="T341">iŋiːr</ta>
            <ta e="T343" id="Seg_2490" s="T342">oŋu͡ok</ta>
            <ta e="T344" id="Seg_2491" s="T343">bu͡ol-but-tar</ta>
            <ta e="T345" id="Seg_2492" s="T344">ele-te</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_2493" s="T0">dʼe</ta>
            <ta e="T2" id="Seg_2494" s="T1">bɨlɨr</ta>
            <ta e="T3" id="Seg_2495" s="T2">hir-taŋara</ta>
            <ta e="T4" id="Seg_2496" s="T3">ü͡öskeː-Ar-tI-GAr</ta>
            <ta e="T5" id="Seg_2497" s="T4">kurpaːskɨ</ta>
            <ta e="T6" id="Seg_2498" s="T5">mu͡ora</ta>
            <ta e="T7" id="Seg_2499" s="T6">hir-LAːgI</ta>
            <ta e="T8" id="Seg_2500" s="T7">ühü</ta>
            <ta e="T9" id="Seg_2501" s="T8">biːr</ta>
            <ta e="T10" id="Seg_2502" s="T9">dʼɨl</ta>
            <ta e="T11" id="Seg_2503" s="T10">purgaː-LAːK</ta>
            <ta e="T12" id="Seg_2504" s="T11">bu͡ol-BIT</ta>
            <ta e="T13" id="Seg_2505" s="T12">kɨhɨn</ta>
            <ta e="T14" id="Seg_2506" s="T13">hamɨːrdaː-BIT</ta>
            <ta e="T15" id="Seg_2507" s="T14">kaːr-tA</ta>
            <ta e="T16" id="Seg_2508" s="T15">barɨ-tA</ta>
            <ta e="T17" id="Seg_2509" s="T16">buːs</ta>
            <ta e="T18" id="Seg_2510" s="T17">bu͡ol-BIT</ta>
            <ta e="T19" id="Seg_2511" s="T18">biːr</ta>
            <ta e="T20" id="Seg_2512" s="T19">da</ta>
            <ta e="T21" id="Seg_2513" s="T20">kadʼɨrɨk</ta>
            <ta e="T22" id="Seg_2514" s="T21">öttü-tA</ta>
            <ta e="T23" id="Seg_2515" s="T22">hu͡ok</ta>
            <ta e="T24" id="Seg_2516" s="T23">kurpaːskɨ-LAr</ta>
            <ta e="T25" id="Seg_2517" s="T24">korguj-BIT-LAr</ta>
            <ta e="T26" id="Seg_2518" s="T25">giniler</ta>
            <ta e="T27" id="Seg_2519" s="T26">as</ta>
            <ta e="T28" id="Seg_2520" s="T27">kördön-A</ta>
            <ta e="T29" id="Seg_2521" s="T28">mas</ta>
            <ta e="T30" id="Seg_2522" s="T29">di͡ekki</ta>
            <ta e="T31" id="Seg_2523" s="T30">köt-BIT-LAr</ta>
            <ta e="T32" id="Seg_2524" s="T31">biːr</ta>
            <ta e="T33" id="Seg_2525" s="T32">muŋ</ta>
            <ta e="T34" id="Seg_2526" s="T33">kɨrdʼagas</ta>
            <ta e="T35" id="Seg_2527" s="T34">hübehit</ta>
            <ta e="T36" id="Seg_2528" s="T35">kurpaːskɨ-LAːK-LAr</ta>
            <ta e="T37" id="Seg_2529" s="T36">mas-GA</ta>
            <ta e="T38" id="Seg_2530" s="T37">tij-BIT-LAr</ta>
            <ta e="T39" id="Seg_2531" s="T38">kanna</ta>
            <ta e="T40" id="Seg_2532" s="T39">da</ta>
            <ta e="T41" id="Seg_2533" s="T40">kadʼɨrɨk</ta>
            <ta e="T42" id="Seg_2534" s="T41">köhün-I-BAT</ta>
            <ta e="T43" id="Seg_2535" s="T42">talak</ta>
            <ta e="T44" id="Seg_2536" s="T43">töbö-tA</ta>
            <ta e="T45" id="Seg_2537" s="T44">da</ta>
            <ta e="T46" id="Seg_2538" s="T45">bil-LIN-I-BAT</ta>
            <ta e="T47" id="Seg_2539" s="T46">tip-An</ta>
            <ta e="T48" id="Seg_2540" s="T47">keːs-BIT</ta>
            <ta e="T49" id="Seg_2541" s="T48">mas</ta>
            <ta e="T50" id="Seg_2542" s="T49">agaj</ta>
            <ta e="T51" id="Seg_2543" s="T50">karaːr-Ar</ta>
            <ta e="T52" id="Seg_2544" s="T51">hübehit-LArA</ta>
            <ta e="T53" id="Seg_2545" s="T52">hübeleː-Ar</ta>
            <ta e="T54" id="Seg_2546" s="T53">dʼe</ta>
            <ta e="T55" id="Seg_2547" s="T54">anɨ</ta>
            <ta e="T56" id="Seg_2548" s="T55">kihitij-BA-TI-BIt</ta>
            <ta e="T57" id="Seg_2549" s="T56">bihigi</ta>
            <ta e="T58" id="Seg_2550" s="T57">kanna</ta>
            <ta e="T59" id="Seg_2551" s="T58">bar-An</ta>
            <ta e="T60" id="Seg_2552" s="T59">öl-IAK-BIt=Ij</ta>
            <ta e="T61" id="Seg_2553" s="T60">bihigi</ta>
            <ta e="T62" id="Seg_2554" s="T61">bugurduk</ta>
            <ta e="T63" id="Seg_2555" s="T62">duːmalaː-A-BIn</ta>
            <ta e="T64" id="Seg_2556" s="T63">kannɨk</ta>
            <ta e="T65" id="Seg_2557" s="T64">ere</ta>
            <ta e="T66" id="Seg_2558" s="T65">üje-GA</ta>
            <ta e="T67" id="Seg_2559" s="T66">biːr</ta>
            <ta e="T68" id="Seg_2560" s="T67">hir-nI</ta>
            <ta e="T69" id="Seg_2561" s="T68">kör-BIT-LAːK-BIn</ta>
            <ta e="T70" id="Seg_2562" s="T69">biːr</ta>
            <ta e="T71" id="Seg_2563" s="T70">ulakan</ta>
            <ta e="T72" id="Seg_2564" s="T71">ürek</ta>
            <ta e="T73" id="Seg_2565" s="T72">baːr</ta>
            <ta e="T74" id="Seg_2566" s="T73">ol</ta>
            <ta e="T75" id="Seg_2567" s="T74">ürek-I-ŋ</ta>
            <ta e="T76" id="Seg_2568" s="T75">kajdak-LAːK</ta>
            <ta e="T77" id="Seg_2569" s="T76">purgaː-GA</ta>
            <ta e="T78" id="Seg_2570" s="T77">tip-I-LIN-AːččI-tA</ta>
            <ta e="T79" id="Seg_2571" s="T78">hu͡ok</ta>
            <ta e="T80" id="Seg_2572" s="T79">horok</ta>
            <ta e="T81" id="Seg_2573" s="T80">hir-tA</ta>
            <ta e="T82" id="Seg_2574" s="T81">kɨhɨn-nI</ta>
            <ta e="T83" id="Seg_2575" s="T82">bɨha</ta>
            <ta e="T84" id="Seg_2576" s="T83">hiːkej</ta>
            <ta e="T85" id="Seg_2577" s="T84">bu͡ol-Ar</ta>
            <ta e="T86" id="Seg_2578" s="T85">kɨtɨl-tI-ttAn</ta>
            <ta e="T87" id="Seg_2579" s="T86">talak-LAːK</ta>
            <ta e="T88" id="Seg_2580" s="T87">kaja-tA</ta>
            <ta e="T89" id="Seg_2581" s="T88">barɨta</ta>
            <ta e="T90" id="Seg_2582" s="T89">heppereːk-LAːK</ta>
            <ta e="T91" id="Seg_2583" s="T90">hugun-LAːK</ta>
            <ta e="T92" id="Seg_2584" s="T91">ot-LAːK</ta>
            <ta e="T93" id="Seg_2585" s="T92">ol</ta>
            <ta e="T94" id="Seg_2586" s="T93">ürek-GA</ta>
            <ta e="T95" id="Seg_2587" s="T94">ku͡okaː</ta>
            <ta e="T96" id="Seg_2588" s="T95">gi͡en-tA</ta>
            <ta e="T97" id="Seg_2589" s="T96">olok-tA</ta>
            <ta e="T98" id="Seg_2590" s="T97">hɨččak</ta>
            <ta e="T99" id="Seg_2591" s="T98">ku͡okaː-LAr</ta>
            <ta e="T100" id="Seg_2592" s="T99">agaj</ta>
            <ta e="T101" id="Seg_2593" s="T100">bihigi-nI</ta>
            <ta e="T102" id="Seg_2594" s="T101">čugas-A-t-IAK-LArA</ta>
            <ta e="T103" id="Seg_2595" s="T102">hu͡ok-tA</ta>
            <ta e="T104" id="Seg_2596" s="T103">bɨlɨr</ta>
            <ta e="T105" id="Seg_2597" s="T104">üje-GA</ta>
            <ta e="T106" id="Seg_2598" s="T105">andagaj</ta>
            <ta e="T107" id="Seg_2599" s="T106">tɨl-LAːK-LAr</ta>
            <ta e="T108" id="Seg_2600" s="T107">kurpaːskɨ-nI</ta>
            <ta e="T109" id="Seg_2601" s="T108">kɨtta</ta>
            <ta e="T110" id="Seg_2602" s="T109">kurpaːskɨ</ta>
            <ta e="T111" id="Seg_2603" s="T110">ku͡okaː</ta>
            <ta e="T112" id="Seg_2604" s="T111">dojdu-tI-GAr</ta>
            <ta e="T113" id="Seg_2605" s="T112">bar-IAK.[tA]</ta>
            <ta e="T114" id="Seg_2606" s="T113">hu͡ok-tA</ta>
            <ta e="T115" id="Seg_2607" s="T114">di͡e-An</ta>
            <ta e="T116" id="Seg_2608" s="T115">ku͡okaː</ta>
            <ta e="T117" id="Seg_2609" s="T116">kurpaːskɨ</ta>
            <ta e="T118" id="Seg_2610" s="T117">dojdu-tI-GAr</ta>
            <ta e="T119" id="Seg_2611" s="T118">öjdöː-Ar</ta>
            <ta e="T120" id="Seg_2612" s="T119">bu͡ol-TAK-TArInA</ta>
            <ta e="T121" id="Seg_2613" s="T120">ötü͡ö-nAn</ta>
            <ta e="T122" id="Seg_2614" s="T121">bi͡er-IAK-LArA</ta>
            <ta e="T123" id="Seg_2615" s="T122">hu͡ok-tA</ta>
            <ta e="T124" id="Seg_2616" s="T123">bihigi</ta>
            <ta e="T125" id="Seg_2617" s="T124">oččogo</ta>
            <ta e="T126" id="Seg_2618" s="T125">heriː-nAn</ta>
            <ta e="T127" id="Seg_2619" s="T126">agaj</ta>
            <ta e="T128" id="Seg_2620" s="T127">kiːr-I-n-IAK-BIt</ta>
            <ta e="T129" id="Seg_2621" s="T128">kurpaːskɨ-LAr</ta>
            <ta e="T130" id="Seg_2622" s="T129">kirdik</ta>
            <ta e="T131" id="Seg_2623" s="T130">manna</ta>
            <ta e="T132" id="Seg_2624" s="T131">olor-BIT-I-nAn</ta>
            <ta e="T133" id="Seg_2625" s="T132">öl-IAK-BIt</ta>
            <ta e="T134" id="Seg_2626" s="T133">du͡o</ta>
            <ta e="T135" id="Seg_2627" s="T134">ol</ta>
            <ta e="T136" id="Seg_2628" s="T135">keri͡etin</ta>
            <ta e="T137" id="Seg_2629" s="T136">heriːles-IAK-BIt</ta>
            <ta e="T138" id="Seg_2630" s="T137">bar-IAgIŋ</ta>
            <ta e="T139" id="Seg_2631" s="T138">di͡e-TI-LAr</ta>
            <ta e="T140" id="Seg_2632" s="T139">kurpaːskɨ-LAr</ta>
            <ta e="T141" id="Seg_2633" s="T140">köt-Ar-LAr</ta>
            <ta e="T142" id="Seg_2634" s="T141">ürek-GA</ta>
            <ta e="T143" id="Seg_2635" s="T142">tij-Ar-LAr</ta>
            <ta e="T144" id="Seg_2636" s="T143">as</ta>
            <ta e="T145" id="Seg_2637" s="T144">nelej-A</ta>
            <ta e="T146" id="Seg_2638" s="T145">hɨt-Ar</ta>
            <ta e="T147" id="Seg_2639" s="T146">hir-tA</ta>
            <ta e="T148" id="Seg_2640" s="T147">e-BIT</ta>
            <ta e="T149" id="Seg_2641" s="T148">ahaː-A</ta>
            <ta e="T150" id="Seg_2642" s="T149">olor-BIT-LAr</ta>
            <ta e="T151" id="Seg_2643" s="T150">hiːkej</ta>
            <ta e="T152" id="Seg_2644" s="T151">üstün</ta>
            <ta e="T153" id="Seg_2645" s="T152">ku͡okaː</ta>
            <ta e="T154" id="Seg_2646" s="T153">hübehit-tI-n</ta>
            <ta e="T155" id="Seg_2647" s="T154">menʼiː-tA</ta>
            <ta e="T156" id="Seg_2648" s="T155">bɨltaj-s</ta>
            <ta e="T157" id="Seg_2649" s="T156">gɨn-BIT</ta>
            <ta e="T158" id="Seg_2650" s="T157">kaja=keː</ta>
            <ta e="T159" id="Seg_2651" s="T158">kurpaːskɨ-LAr</ta>
            <ta e="T160" id="Seg_2652" s="T159">togo</ta>
            <ta e="T161" id="Seg_2653" s="T160">hir-GA</ta>
            <ta e="T162" id="Seg_2654" s="T161">kel-BIT-GIt=Ij</ta>
            <ta e="T163" id="Seg_2655" s="T162">bɨlɨr</ta>
            <ta e="T164" id="Seg_2656" s="T163">üje-GA</ta>
            <ta e="T165" id="Seg_2657" s="T164">andagaj</ta>
            <ta e="T166" id="Seg_2658" s="T165">tɨl-LAːK</ta>
            <ta e="T167" id="Seg_2659" s="T166">e-TI-BIt</ta>
            <ta e="T168" id="Seg_2660" s="T167">ehigi</ta>
            <ta e="T169" id="Seg_2661" s="T168">hir-GItI-GAr</ta>
            <ta e="T170" id="Seg_2662" s="T169">bihigi</ta>
            <ta e="T171" id="Seg_2663" s="T170">bar-BAT-BIt</ta>
            <ta e="T172" id="Seg_2664" s="T171">ehigi</ta>
            <ta e="T173" id="Seg_2665" s="T172">bihigi</ta>
            <ta e="T174" id="Seg_2666" s="T173">hir-BItI-GAr</ta>
            <ta e="T175" id="Seg_2667" s="T174">togo</ta>
            <ta e="T176" id="Seg_2668" s="T175">kel-TI-GIt</ta>
            <ta e="T177" id="Seg_2669" s="T176">köhüt-I-m-ŋ</ta>
            <ta e="T178" id="Seg_2670" s="T177">hanɨkaːn</ta>
            <ta e="T179" id="Seg_2671" s="T178">bar-I-ŋ</ta>
            <ta e="T180" id="Seg_2672" s="T179">mantan</ta>
            <ta e="T181" id="Seg_2673" s="T180">talak</ta>
            <ta e="T182" id="Seg_2674" s="T181">gi͡en-tI-n</ta>
            <ta e="T183" id="Seg_2675" s="T182">hebirdek-I-n</ta>
            <ta e="T184" id="Seg_2676" s="T183">heppereːk</ta>
            <ta e="T185" id="Seg_2677" s="T184">gi͡en-tI-n</ta>
            <ta e="T186" id="Seg_2678" s="T185">töbö-tI-n</ta>
            <ta e="T187" id="Seg_2679" s="T186">da</ta>
            <ta e="T188" id="Seg_2680" s="T187">hi͡e-t-IAK-m</ta>
            <ta e="T189" id="Seg_2681" s="T188">hu͡ok-tA</ta>
            <ta e="T190" id="Seg_2682" s="T189">ot-BI-n</ta>
            <ta e="T191" id="Seg_2683" s="T190">da</ta>
            <ta e="T192" id="Seg_2684" s="T191">hi͡e-t-BAT-BIn</ta>
            <ta e="T193" id="Seg_2685" s="T192">čaj-BI-n</ta>
            <ta e="T194" id="Seg_2686" s="T193">da</ta>
            <ta e="T195" id="Seg_2687" s="T194">kas-TAr-IAK-m</ta>
            <ta e="T196" id="Seg_2688" s="T195">hu͡ok-tA</ta>
            <ta e="T197" id="Seg_2689" s="T196">ol-GA</ta>
            <ta e="T198" id="Seg_2690" s="T197">kurpaːskɨ</ta>
            <ta e="T199" id="Seg_2691" s="T198">hübehit-tA</ta>
            <ta e="T200" id="Seg_2692" s="T199">di͡e-Ar</ta>
            <ta e="T201" id="Seg_2693" s="T200">bihigi</ta>
            <ta e="T202" id="Seg_2694" s="T201">ötü͡ö-nAn</ta>
            <ta e="T203" id="Seg_2695" s="T202">bar-BAT-BIt</ta>
            <ta e="T204" id="Seg_2696" s="T203">heriː-nAn</ta>
            <ta e="T205" id="Seg_2697" s="T204">daːganɨ</ta>
            <ta e="T206" id="Seg_2698" s="T205">bu</ta>
            <ta e="T207" id="Seg_2699" s="T206">hir-nI</ta>
            <ta e="T208" id="Seg_2700" s="T207">ɨl-IAK-BIt</ta>
            <ta e="T209" id="Seg_2701" s="T208">ku͡okaː</ta>
            <ta e="T210" id="Seg_2702" s="T209">hübehit-tA</ta>
            <ta e="T211" id="Seg_2703" s="T210">min</ta>
            <ta e="T212" id="Seg_2704" s="T211">ötü͡ö-nAn</ta>
            <ta e="T213" id="Seg_2705" s="T212">bi͡er-BAT-BIn</ta>
            <ta e="T214" id="Seg_2706" s="T213">ku͡okaː-nI</ta>
            <ta e="T215" id="Seg_2707" s="T214">imičči</ta>
            <ta e="T216" id="Seg_2708" s="T215">ölör-TAK-GItInA</ta>
            <ta e="T217" id="Seg_2709" s="T216">ɨl-IAK-GIt</ta>
            <ta e="T218" id="Seg_2710" s="T217">heriːles-IAK-BIt</ta>
            <ta e="T219" id="Seg_2711" s="T218">toktoː-ŋ</ta>
            <ta e="T220" id="Seg_2712" s="T219">min</ta>
            <ta e="T221" id="Seg_2713" s="T220">dʼon-BI-n</ta>
            <ta e="T222" id="Seg_2714" s="T221">komuj-n-IAK-m</ta>
            <ta e="T223" id="Seg_2715" s="T222">di͡e-Ar</ta>
            <ta e="T224" id="Seg_2716" s="T223">ku͡okaː</ta>
            <ta e="T225" id="Seg_2717" s="T224">hübehit-tA</ta>
            <ta e="T226" id="Seg_2718" s="T225">ergilin-Ar</ta>
            <ta e="T227" id="Seg_2719" s="T226">da</ta>
            <ta e="T228" id="Seg_2720" s="T227">kuturuk-tI-nAn</ta>
            <ta e="T229" id="Seg_2721" s="T228">bar</ta>
            <ta e="T230" id="Seg_2722" s="T229">gɨn-BIT</ta>
            <ta e="T231" id="Seg_2723" s="T230">bar-BIT</ta>
            <ta e="T232" id="Seg_2724" s="T231">dʼon</ta>
            <ta e="T233" id="Seg_2725" s="T232">komuj-n-A</ta>
            <ta e="T234" id="Seg_2726" s="T233">ku͡okaː</ta>
            <ta e="T235" id="Seg_2727" s="T234">ü͡ör-tI-n</ta>
            <ta e="T236" id="Seg_2728" s="T235">barɨ-tI-n</ta>
            <ta e="T237" id="Seg_2729" s="T236">egel-An</ta>
            <ta e="T238" id="Seg_2730" s="T237">hiːkej-nI</ta>
            <ta e="T239" id="Seg_2731" s="T238">kara-ččI</ta>
            <ta e="T240" id="Seg_2732" s="T239">oŋor-TI-tA</ta>
            <ta e="T241" id="Seg_2733" s="T240">kurpaːskɨ-LAr</ta>
            <ta e="T242" id="Seg_2734" s="T241">belem</ta>
            <ta e="T243" id="Seg_2735" s="T242">kɨtɨl-GA</ta>
            <ta e="T244" id="Seg_2736" s="T243">tur-Ar-LAr</ta>
            <ta e="T245" id="Seg_2737" s="T244">kurpaːskɨ</ta>
            <ta e="T246" id="Seg_2738" s="T245">hübehit-tA</ta>
            <ta e="T247" id="Seg_2739" s="T246">dʼon-tI-GAr</ta>
            <ta e="T248" id="Seg_2740" s="T247">et-Ar</ta>
            <ta e="T249" id="Seg_2741" s="T248">ku͡okaː-nI</ta>
            <ta e="T250" id="Seg_2742" s="T249">bas-GA</ta>
            <ta e="T251" id="Seg_2743" s="T250">ɨt-Aːr-I-ŋ</ta>
            <ta e="T252" id="Seg_2744" s="T251">higde-tI-GAr</ta>
            <ta e="T253" id="Seg_2745" s="T252">öl-AːččI-tA</ta>
            <ta e="T254" id="Seg_2746" s="T253">hu͡ok</ta>
            <ta e="T255" id="Seg_2747" s="T254">kurpaːskɨ</ta>
            <ta e="T256" id="Seg_2748" s="T255">heriː-tA</ta>
            <ta e="T257" id="Seg_2749" s="T256">ok-LAr-tI-n</ta>
            <ta e="T258" id="Seg_2750" s="T257">ɨːt-BIT</ta>
            <ta e="T259" id="Seg_2751" s="T258">alaŋaː</ta>
            <ta e="T260" id="Seg_2752" s="T259">kiris-I-tI-n</ta>
            <ta e="T261" id="Seg_2753" s="T260">tɨ͡as-tI-n</ta>
            <ta e="T262" id="Seg_2754" s="T261">ihilin-AːT</ta>
            <ta e="T263" id="Seg_2755" s="T262">ku͡okaː-LAr</ta>
            <ta e="T264" id="Seg_2756" s="T263">ü͡ös</ta>
            <ta e="T265" id="Seg_2757" s="T264">di͡ekki</ta>
            <ta e="T266" id="Seg_2758" s="T265">ergilin-A</ta>
            <ta e="T267" id="Seg_2759" s="T266">bi͡er-An</ta>
            <ta e="T268" id="Seg_2760" s="T267">ahar-BIT-LAr</ta>
            <ta e="T269" id="Seg_2761" s="T268">kurpaːskɨ-LAr</ta>
            <ta e="T270" id="Seg_2762" s="T269">ok-LArA</ta>
            <ta e="T271" id="Seg_2763" s="T270">ku͡okaː</ta>
            <ta e="T272" id="Seg_2764" s="T271">higde-tI-GAr</ta>
            <ta e="T273" id="Seg_2765" s="T272">da</ta>
            <ta e="T274" id="Seg_2766" s="T273">higde-tI-GAr</ta>
            <ta e="T275" id="Seg_2767" s="T274">bi͡er-An</ta>
            <ta e="T276" id="Seg_2768" s="T275">is-BIT</ta>
            <ta e="T277" id="Seg_2769" s="T276">ku͡okaː-LAr</ta>
            <ta e="T278" id="Seg_2770" s="T277">ergilin-An</ta>
            <ta e="T279" id="Seg_2771" s="T278">kel-BIT-LAr</ta>
            <ta e="T280" id="Seg_2772" s="T279">kurpaːskɨ-LAr</ta>
            <ta e="T281" id="Seg_2773" s="T280">ok</ta>
            <ta e="T282" id="Seg_2774" s="T281">ɨlɨn-IAK-LArI-GAr</ta>
            <ta e="T283" id="Seg_2775" s="T282">di͡eri</ta>
            <ta e="T284" id="Seg_2776" s="T283">ku͡okaː-LAr</ta>
            <ta e="T285" id="Seg_2777" s="T284">ɨt-BIT-LAr</ta>
            <ta e="T286" id="Seg_2778" s="T285">kurpaːskɨ-LAr</ta>
            <ta e="T287" id="Seg_2779" s="T286">hürek-LArI-n</ta>
            <ta e="T288" id="Seg_2780" s="T287">kɨŋaː-An</ta>
            <ta e="T289" id="Seg_2781" s="T288">kurpaːskɨ-LAr</ta>
            <ta e="T290" id="Seg_2782" s="T289">ahar-An</ta>
            <ta e="T291" id="Seg_2783" s="T290">köt-A</ta>
            <ta e="T292" id="Seg_2784" s="T291">gɨn-A</ta>
            <ta e="T293" id="Seg_2785" s="T292">ü͡ös-tA</ta>
            <ta e="T294" id="Seg_2786" s="T293">di͡ekki</ta>
            <ta e="T295" id="Seg_2787" s="T294">oj-An</ta>
            <ta e="T296" id="Seg_2788" s="T295">bi͡er-BIT-LAr</ta>
            <ta e="T297" id="Seg_2789" s="T296">ku͡okaː-LAr</ta>
            <ta e="T346" id="Seg_2790" s="T297">ok-LArA</ta>
            <ta e="T298" id="Seg_2791" s="T346">kurpaːskɨ-LAr</ta>
            <ta e="T299" id="Seg_2792" s="T298">gi͡en-LArI-n</ta>
            <ta e="T300" id="Seg_2793" s="T299">bɨlčɨŋ-GA</ta>
            <ta e="T301" id="Seg_2794" s="T300">da</ta>
            <ta e="T302" id="Seg_2795" s="T301">bɨlčɨŋ-GA</ta>
            <ta e="T303" id="Seg_2796" s="T302">atak-LArI-GAr</ta>
            <ta e="T304" id="Seg_2797" s="T303">ogorduk</ta>
            <ta e="T305" id="Seg_2798" s="T304">heriːles-BIT-LAr</ta>
            <ta e="T306" id="Seg_2799" s="T305">kaja-LArA</ta>
            <ta e="T307" id="Seg_2800" s="T306">da</ta>
            <ta e="T308" id="Seg_2801" s="T307">ölör-s-I-BAT</ta>
            <ta e="T309" id="Seg_2802" s="T308">kɨ͡aj-s-I-BAT</ta>
            <ta e="T310" id="Seg_2803" s="T309">ok-LAr-tA</ta>
            <ta e="T311" id="Seg_2804" s="T310">barɨta</ta>
            <ta e="T312" id="Seg_2805" s="T311">baran-An</ta>
            <ta e="T313" id="Seg_2806" s="T312">heriːles-An</ta>
            <ta e="T314" id="Seg_2807" s="T313">büt-BIT-LAr</ta>
            <ta e="T315" id="Seg_2808" s="T314">ku͡okaː</ta>
            <ta e="T316" id="Seg_2809" s="T315">hübehit-tA</ta>
            <ta e="T317" id="Seg_2810" s="T316">anɨ</ta>
            <ta e="T318" id="Seg_2811" s="T317">heriːles-An</ta>
            <ta e="T319" id="Seg_2812" s="T318">büt-IAgIŋ</ta>
            <ta e="T320" id="Seg_2813" s="T319">ehigi</ta>
            <ta e="T321" id="Seg_2814" s="T320">kanna</ta>
            <ta e="T322" id="Seg_2815" s="T321">bagar-Ar</ta>
            <ta e="T323" id="Seg_2816" s="T322">köt-A</ta>
            <ta e="T324" id="Seg_2817" s="T323">hɨrɨt-I-ŋ</ta>
            <ta e="T325" id="Seg_2818" s="T324">kanna</ta>
            <ta e="T326" id="Seg_2819" s="T325">bagar-Ar</ta>
            <ta e="T327" id="Seg_2820" s="T326">ahaː-ŋ</ta>
            <ta e="T328" id="Seg_2821" s="T327">di͡e-Ar</ta>
            <ta e="T329" id="Seg_2822" s="T328">hoččotton</ta>
            <ta e="T330" id="Seg_2823" s="T329">kurpaːskɨ-LAr</ta>
            <ta e="T331" id="Seg_2824" s="T330">ok-LArA</ta>
            <ta e="T332" id="Seg_2825" s="T331">ku͡okaː</ta>
            <ta e="T333" id="Seg_2826" s="T332">higde-tI-GAr</ta>
            <ta e="T334" id="Seg_2827" s="T333">atɨrdʼak</ta>
            <ta e="T335" id="Seg_2828" s="T334">oŋu͡ok</ta>
            <ta e="T336" id="Seg_2829" s="T335">bu͡ol-BIT-LAr</ta>
            <ta e="T337" id="Seg_2830" s="T336">ku͡okaː-LAr</ta>
            <ta e="T338" id="Seg_2831" s="T337">ok-LArA</ta>
            <ta e="T339" id="Seg_2832" s="T338">kurpaːskɨ</ta>
            <ta e="T340" id="Seg_2833" s="T339">atak-tI-n</ta>
            <ta e="T341" id="Seg_2834" s="T340">bɨlčɨŋ-tI-GAr</ta>
            <ta e="T342" id="Seg_2835" s="T341">iŋiːr</ta>
            <ta e="T343" id="Seg_2836" s="T342">oŋu͡ok</ta>
            <ta e="T344" id="Seg_2837" s="T343">bu͡ol-BIT-LAr</ta>
            <ta e="T345" id="Seg_2838" s="T344">ele-tA</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_2839" s="T0">well</ta>
            <ta e="T2" id="Seg_2840" s="T1">long.ago</ta>
            <ta e="T3" id="Seg_2841" s="T2">earth.[NOM]-sky.[NOM]</ta>
            <ta e="T4" id="Seg_2842" s="T3">arise-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T5" id="Seg_2843" s="T4">partridge.[NOM]</ta>
            <ta e="T6" id="Seg_2844" s="T5">tundra.[NOM]</ta>
            <ta e="T7" id="Seg_2845" s="T6">earth-ADJZ.[NOM]</ta>
            <ta e="T8" id="Seg_2846" s="T7">it.is.said</ta>
            <ta e="T9" id="Seg_2847" s="T8">one</ta>
            <ta e="T10" id="Seg_2848" s="T9">year.[NOM]</ta>
            <ta e="T11" id="Seg_2849" s="T10">snowstorm-PROPR.[NOM]</ta>
            <ta e="T12" id="Seg_2850" s="T11">become-PST2.[3SG]</ta>
            <ta e="T13" id="Seg_2851" s="T12">in.winter</ta>
            <ta e="T14" id="Seg_2852" s="T13">rain-PST2.[3SG]</ta>
            <ta e="T15" id="Seg_2853" s="T14">snow-3SG.[NOM]</ta>
            <ta e="T16" id="Seg_2854" s="T15">whole-3SG.[NOM]</ta>
            <ta e="T17" id="Seg_2855" s="T16">ice.[NOM]</ta>
            <ta e="T18" id="Seg_2856" s="T17">become-PST2.[3SG]</ta>
            <ta e="T19" id="Seg_2857" s="T18">one</ta>
            <ta e="T20" id="Seg_2858" s="T19">NEG</ta>
            <ta e="T21" id="Seg_2859" s="T20">thawed.patch.[NOM]</ta>
            <ta e="T22" id="Seg_2860" s="T21">side-3SG.[NOM]</ta>
            <ta e="T23" id="Seg_2861" s="T22">NEG.EX</ta>
            <ta e="T24" id="Seg_2862" s="T23">partridge-PL.[NOM]</ta>
            <ta e="T25" id="Seg_2863" s="T24">hunger-PST2-3PL</ta>
            <ta e="T26" id="Seg_2864" s="T25">3PL.[NOM]</ta>
            <ta e="T27" id="Seg_2865" s="T26">food.[NOM]</ta>
            <ta e="T28" id="Seg_2866" s="T27">search.for-CVB.SIM</ta>
            <ta e="T29" id="Seg_2867" s="T28">forest.[NOM]</ta>
            <ta e="T30" id="Seg_2868" s="T29">in.the.direction</ta>
            <ta e="T31" id="Seg_2869" s="T30">fly-PST2-3PL</ta>
            <ta e="T32" id="Seg_2870" s="T31">one</ta>
            <ta e="T33" id="Seg_2871" s="T32">most</ta>
            <ta e="T34" id="Seg_2872" s="T33">old</ta>
            <ta e="T35" id="Seg_2873" s="T34">leader.[NOM]</ta>
            <ta e="T36" id="Seg_2874" s="T35">partridge-PROPR-3PL</ta>
            <ta e="T37" id="Seg_2875" s="T36">forest-DAT/LOC</ta>
            <ta e="T38" id="Seg_2876" s="T37">reach-PST2-3PL</ta>
            <ta e="T39" id="Seg_2877" s="T38">where</ta>
            <ta e="T40" id="Seg_2878" s="T39">NEG</ta>
            <ta e="T41" id="Seg_2879" s="T40">thawed.patch.[NOM]</ta>
            <ta e="T42" id="Seg_2880" s="T41">to.be.on.view-EP-NEG.[3SG]</ta>
            <ta e="T43" id="Seg_2881" s="T42">bush.[NOM]</ta>
            <ta e="T44" id="Seg_2882" s="T43">treetop-3SG.[NOM]</ta>
            <ta e="T45" id="Seg_2883" s="T44">NEG</ta>
            <ta e="T46" id="Seg_2884" s="T45">notice-PASS/REFL-EP-NEG.[3SG]</ta>
            <ta e="T47" id="Seg_2885" s="T46">flurry-CVB.SEQ</ta>
            <ta e="T48" id="Seg_2886" s="T47">throw-PST2.[3SG]</ta>
            <ta e="T49" id="Seg_2887" s="T48">tree.[NOM]</ta>
            <ta e="T50" id="Seg_2888" s="T49">only</ta>
            <ta e="T51" id="Seg_2889" s="T50">be.black-PRS.[3SG]</ta>
            <ta e="T52" id="Seg_2890" s="T51">leader-3PL.[NOM]</ta>
            <ta e="T53" id="Seg_2891" s="T52">advise-PRS.[3SG]</ta>
            <ta e="T54" id="Seg_2892" s="T53">well</ta>
            <ta e="T55" id="Seg_2893" s="T54">now</ta>
            <ta e="T56" id="Seg_2894" s="T55">save.oneself-NEG-PST1-1PL</ta>
            <ta e="T57" id="Seg_2895" s="T56">1PL.[NOM]</ta>
            <ta e="T58" id="Seg_2896" s="T57">whereto</ta>
            <ta e="T59" id="Seg_2897" s="T58">go-CVB.SEQ</ta>
            <ta e="T60" id="Seg_2898" s="T59">die-FUT-1PL=Q</ta>
            <ta e="T61" id="Seg_2899" s="T60">1PL.[NOM]</ta>
            <ta e="T62" id="Seg_2900" s="T61">like.this</ta>
            <ta e="T63" id="Seg_2901" s="T62">think-PRS-1SG</ta>
            <ta e="T64" id="Seg_2902" s="T63">what.kind.of</ta>
            <ta e="T65" id="Seg_2903" s="T64">INDEF</ta>
            <ta e="T66" id="Seg_2904" s="T65">time-DAT/LOC</ta>
            <ta e="T67" id="Seg_2905" s="T66">one</ta>
            <ta e="T68" id="Seg_2906" s="T67">place-ACC</ta>
            <ta e="T69" id="Seg_2907" s="T68">see-PTCP.PST-PROPR-1SG</ta>
            <ta e="T70" id="Seg_2908" s="T69">one</ta>
            <ta e="T71" id="Seg_2909" s="T70">big</ta>
            <ta e="T72" id="Seg_2910" s="T71">river.[NOM]</ta>
            <ta e="T73" id="Seg_2911" s="T72">there.is</ta>
            <ta e="T74" id="Seg_2912" s="T73">that</ta>
            <ta e="T75" id="Seg_2913" s="T74">river-EP-2SG.[NOM]</ta>
            <ta e="T76" id="Seg_2914" s="T75">how-PROPR</ta>
            <ta e="T77" id="Seg_2915" s="T76">snowstorm-DAT/LOC</ta>
            <ta e="T78" id="Seg_2916" s="T77">flurry-EP-PASS/REFL-PTCP.HAB-3SG</ta>
            <ta e="T79" id="Seg_2917" s="T78">NEG.[3SG]</ta>
            <ta e="T80" id="Seg_2918" s="T79">some</ta>
            <ta e="T81" id="Seg_2919" s="T80">place-3SG.[NOM]</ta>
            <ta e="T82" id="Seg_2920" s="T81">winter-ACC</ta>
            <ta e="T83" id="Seg_2921" s="T82">during</ta>
            <ta e="T84" id="Seg_2922" s="T83">icehole.[NOM]</ta>
            <ta e="T85" id="Seg_2923" s="T84">be-PRS.[3SG]</ta>
            <ta e="T86" id="Seg_2924" s="T85">shore-3SG-ABL</ta>
            <ta e="T87" id="Seg_2925" s="T86">willow.bush-PROPR.[NOM]</ta>
            <ta e="T88" id="Seg_2926" s="T87">rock-3SG.[NOM]</ta>
            <ta e="T89" id="Seg_2927" s="T88">completely</ta>
            <ta e="T90" id="Seg_2928" s="T89">bug_rosemary-PROPR.[NOM]</ta>
            <ta e="T91" id="Seg_2929" s="T90">berry-PROPR.[NOM]</ta>
            <ta e="T92" id="Seg_2930" s="T91">grass-PROPR.[NOM]</ta>
            <ta e="T93" id="Seg_2931" s="T92">that</ta>
            <ta e="T94" id="Seg_2932" s="T93">river-DAT/LOC</ta>
            <ta e="T95" id="Seg_2933" s="T94">pike.[NOM]</ta>
            <ta e="T96" id="Seg_2934" s="T95">own-3SG</ta>
            <ta e="T97" id="Seg_2935" s="T96">place.of.living-3SG.[NOM]</ta>
            <ta e="T98" id="Seg_2936" s="T97">just</ta>
            <ta e="T99" id="Seg_2937" s="T98">pike-PL.[NOM]</ta>
            <ta e="T100" id="Seg_2938" s="T99">only</ta>
            <ta e="T101" id="Seg_2939" s="T100">1PL-ACC</ta>
            <ta e="T102" id="Seg_2940" s="T101">get.to-EP-CAUS-FUT-3PL</ta>
            <ta e="T103" id="Seg_2941" s="T102">NEG-3SG</ta>
            <ta e="T104" id="Seg_2942" s="T103">long.ago</ta>
            <ta e="T105" id="Seg_2943" s="T104">time-DAT/LOC</ta>
            <ta e="T106" id="Seg_2944" s="T105">sworn</ta>
            <ta e="T107" id="Seg_2945" s="T106">word-PROPR-3PL</ta>
            <ta e="T108" id="Seg_2946" s="T107">partridge-ACC</ta>
            <ta e="T109" id="Seg_2947" s="T108">with</ta>
            <ta e="T110" id="Seg_2948" s="T109">partridge.[NOM]</ta>
            <ta e="T111" id="Seg_2949" s="T110">pike.[NOM]</ta>
            <ta e="T112" id="Seg_2950" s="T111">country-3SG-DAT/LOC</ta>
            <ta e="T113" id="Seg_2951" s="T112">go-FUT.[3SG]</ta>
            <ta e="T114" id="Seg_2952" s="T113">NEG-3SG</ta>
            <ta e="T115" id="Seg_2953" s="T114">say-CVB.SEQ</ta>
            <ta e="T116" id="Seg_2954" s="T115">pike.[NOM]</ta>
            <ta e="T117" id="Seg_2955" s="T116">partridge.[NOM]</ta>
            <ta e="T118" id="Seg_2956" s="T117">country-3SG-DAT/LOC</ta>
            <ta e="T119" id="Seg_2957" s="T118">remember-PTCP.PRS</ta>
            <ta e="T120" id="Seg_2958" s="T119">be-TEMP-3PL</ta>
            <ta e="T121" id="Seg_2959" s="T120">good-INSTR</ta>
            <ta e="T122" id="Seg_2960" s="T121">give-FUT-3PL</ta>
            <ta e="T123" id="Seg_2961" s="T122">NEG-3SG</ta>
            <ta e="T124" id="Seg_2962" s="T123">1PL.[NOM]</ta>
            <ta e="T125" id="Seg_2963" s="T124">then</ta>
            <ta e="T126" id="Seg_2964" s="T125">war-INSTR</ta>
            <ta e="T127" id="Seg_2965" s="T126">only</ta>
            <ta e="T128" id="Seg_2966" s="T127">go.in-EP-MED-FUT-1PL</ta>
            <ta e="T129" id="Seg_2967" s="T128">partridge-PL.[NOM]</ta>
            <ta e="T130" id="Seg_2968" s="T129">truth.[NOM]</ta>
            <ta e="T131" id="Seg_2969" s="T130">here</ta>
            <ta e="T132" id="Seg_2970" s="T131">live-PTCP.PST-EP-INSTR</ta>
            <ta e="T133" id="Seg_2971" s="T132">die-FUT-1PL</ta>
            <ta e="T134" id="Seg_2972" s="T133">Q</ta>
            <ta e="T135" id="Seg_2973" s="T134">that.[NOM]</ta>
            <ta e="T136" id="Seg_2974" s="T135">instead.of</ta>
            <ta e="T137" id="Seg_2975" s="T136">fight-FUT-1PL</ta>
            <ta e="T138" id="Seg_2976" s="T137">go-IMP.1PL</ta>
            <ta e="T139" id="Seg_2977" s="T138">say-PST1-3PL</ta>
            <ta e="T140" id="Seg_2978" s="T139">partridge-PL.[NOM]</ta>
            <ta e="T141" id="Seg_2979" s="T140">fly-PRS-3PL</ta>
            <ta e="T142" id="Seg_2980" s="T141">river-DAT/LOC</ta>
            <ta e="T143" id="Seg_2981" s="T142">reach-PRS-3PL</ta>
            <ta e="T144" id="Seg_2982" s="T143">food.[NOM]</ta>
            <ta e="T145" id="Seg_2983" s="T144">spread.out-CVB.SIM</ta>
            <ta e="T146" id="Seg_2984" s="T145">lie-PTCP.PRS</ta>
            <ta e="T147" id="Seg_2985" s="T146">place-3SG.[NOM]</ta>
            <ta e="T148" id="Seg_2986" s="T147">be-PST2.[3SG]</ta>
            <ta e="T149" id="Seg_2987" s="T148">eat-CVB.SIM</ta>
            <ta e="T150" id="Seg_2988" s="T149">sit.down-PST2-3PL</ta>
            <ta e="T151" id="Seg_2989" s="T150">icehole.[NOM]</ta>
            <ta e="T152" id="Seg_2990" s="T151">through</ta>
            <ta e="T153" id="Seg_2991" s="T152">pike.[NOM]</ta>
            <ta e="T154" id="Seg_2992" s="T153">leader-3SG-GEN</ta>
            <ta e="T155" id="Seg_2993" s="T154">head-3SG.[NOM]</ta>
            <ta e="T156" id="Seg_2994" s="T155">lean.out-NMNZ.[NOM]</ta>
            <ta e="T157" id="Seg_2995" s="T156">make-PST2.[3SG]</ta>
            <ta e="T158" id="Seg_2996" s="T157">well=EMPH</ta>
            <ta e="T159" id="Seg_2997" s="T158">partridge-PL.[NOM]</ta>
            <ta e="T160" id="Seg_2998" s="T159">why</ta>
            <ta e="T161" id="Seg_2999" s="T160">place-DAT/LOC</ta>
            <ta e="T162" id="Seg_3000" s="T161">come-PST2-2PL=Q</ta>
            <ta e="T163" id="Seg_3001" s="T162">long.ago</ta>
            <ta e="T164" id="Seg_3002" s="T163">time-DAT/LOC</ta>
            <ta e="T165" id="Seg_3003" s="T164">sworn</ta>
            <ta e="T166" id="Seg_3004" s="T165">word-PROPR.[NOM]</ta>
            <ta e="T167" id="Seg_3005" s="T166">be-PST1-1PL</ta>
            <ta e="T168" id="Seg_3006" s="T167">2PL.[NOM]</ta>
            <ta e="T169" id="Seg_3007" s="T168">place-2PL-DAT/LOC</ta>
            <ta e="T170" id="Seg_3008" s="T169">1PL.[NOM]</ta>
            <ta e="T171" id="Seg_3009" s="T170">go-NEG-1PL</ta>
            <ta e="T172" id="Seg_3010" s="T171">2PL.[NOM]</ta>
            <ta e="T173" id="Seg_3011" s="T172">1PL.[NOM]</ta>
            <ta e="T174" id="Seg_3012" s="T173">place-1PL-DAT/LOC</ta>
            <ta e="T175" id="Seg_3013" s="T174">why</ta>
            <ta e="T176" id="Seg_3014" s="T175">come-PST1-2PL</ta>
            <ta e="T177" id="Seg_3015" s="T176">wait-EP-NEG-IMP.2PL</ta>
            <ta e="T178" id="Seg_3016" s="T177">at.once</ta>
            <ta e="T179" id="Seg_3017" s="T178">go-EP-IMP.2PL</ta>
            <ta e="T180" id="Seg_3018" s="T179">from.here</ta>
            <ta e="T181" id="Seg_3019" s="T180">bush.[NOM]</ta>
            <ta e="T182" id="Seg_3020" s="T181">own-3SG-GEN</ta>
            <ta e="T183" id="Seg_3021" s="T182">leaf-3SG-ACC</ta>
            <ta e="T184" id="Seg_3022" s="T183">bug_rosemary.[NOM]</ta>
            <ta e="T185" id="Seg_3023" s="T184">own-3SG-GEN</ta>
            <ta e="T186" id="Seg_3024" s="T185">top-3SG-ACC</ta>
            <ta e="T187" id="Seg_3025" s="T186">NEG</ta>
            <ta e="T188" id="Seg_3026" s="T187">eat-CAUS-FUT-1SG</ta>
            <ta e="T189" id="Seg_3027" s="T188">NEG-3SG</ta>
            <ta e="T190" id="Seg_3028" s="T189">grass-1SG-ACC</ta>
            <ta e="T191" id="Seg_3029" s="T190">NEG</ta>
            <ta e="T192" id="Seg_3030" s="T191">eat-CAUS-NEG-1SG</ta>
            <ta e="T193" id="Seg_3031" s="T192">gravel-1SG-ACC</ta>
            <ta e="T194" id="Seg_3032" s="T193">NEG</ta>
            <ta e="T195" id="Seg_3033" s="T194">dig-CAUS-FUT-1SG</ta>
            <ta e="T196" id="Seg_3034" s="T195">NEG-3SG</ta>
            <ta e="T197" id="Seg_3035" s="T196">that-DAT/LOC</ta>
            <ta e="T198" id="Seg_3036" s="T197">partridge.[NOM]</ta>
            <ta e="T199" id="Seg_3037" s="T198">leader-3SG.[NOM]</ta>
            <ta e="T200" id="Seg_3038" s="T199">say-PRS.[3SG]</ta>
            <ta e="T201" id="Seg_3039" s="T200">1PL.[NOM]</ta>
            <ta e="T202" id="Seg_3040" s="T201">good-INSTR</ta>
            <ta e="T203" id="Seg_3041" s="T202">go-NEG-1PL</ta>
            <ta e="T204" id="Seg_3042" s="T203">war-INSTR</ta>
            <ta e="T205" id="Seg_3043" s="T204">even.if</ta>
            <ta e="T206" id="Seg_3044" s="T205">this</ta>
            <ta e="T207" id="Seg_3045" s="T206">place-ACC</ta>
            <ta e="T208" id="Seg_3046" s="T207">take-FUT-1PL</ta>
            <ta e="T209" id="Seg_3047" s="T208">pike.[NOM]</ta>
            <ta e="T210" id="Seg_3048" s="T209">leader-3SG.[NOM]</ta>
            <ta e="T211" id="Seg_3049" s="T210">1SG.[NOM]</ta>
            <ta e="T212" id="Seg_3050" s="T211">good-INSTR</ta>
            <ta e="T213" id="Seg_3051" s="T212">give-NEG-1SG</ta>
            <ta e="T214" id="Seg_3052" s="T213">pike-ACC</ta>
            <ta e="T215" id="Seg_3053" s="T214">completely</ta>
            <ta e="T216" id="Seg_3054" s="T215">kill-TEMP-2PL</ta>
            <ta e="T217" id="Seg_3055" s="T216">take-FUT-2PL</ta>
            <ta e="T218" id="Seg_3056" s="T217">fight-FUT-1PL</ta>
            <ta e="T219" id="Seg_3057" s="T218">stop-IMP.2PL</ta>
            <ta e="T220" id="Seg_3058" s="T219">1SG.[NOM]</ta>
            <ta e="T221" id="Seg_3059" s="T220">people-1SG-ACC</ta>
            <ta e="T222" id="Seg_3060" s="T221">gather-MED-FUT-1SG</ta>
            <ta e="T223" id="Seg_3061" s="T222">say-PRS.[3SG]</ta>
            <ta e="T224" id="Seg_3062" s="T223">pike.[NOM]</ta>
            <ta e="T225" id="Seg_3063" s="T224">leader-3SG.[NOM]</ta>
            <ta e="T226" id="Seg_3064" s="T225">turn-PRS.[3SG]</ta>
            <ta e="T227" id="Seg_3065" s="T226">and</ta>
            <ta e="T228" id="Seg_3066" s="T227">tail-3SG-INSTR</ta>
            <ta e="T229" id="Seg_3067" s="T228">splat</ta>
            <ta e="T230" id="Seg_3068" s="T229">make-PST2.[3SG]</ta>
            <ta e="T231" id="Seg_3069" s="T230">go-PST2.[3SG]</ta>
            <ta e="T232" id="Seg_3070" s="T231">people.[NOM]</ta>
            <ta e="T233" id="Seg_3071" s="T232">gather-MED-CVB.SIM</ta>
            <ta e="T234" id="Seg_3072" s="T233">pike.[NOM]</ta>
            <ta e="T235" id="Seg_3073" s="T234">swarm-3SG-ACC</ta>
            <ta e="T236" id="Seg_3074" s="T235">whole-3SG-ACC</ta>
            <ta e="T237" id="Seg_3075" s="T236">get-CVB.SEQ</ta>
            <ta e="T238" id="Seg_3076" s="T237">icehole-ACC</ta>
            <ta e="T239" id="Seg_3077" s="T238">black-ADVZ</ta>
            <ta e="T240" id="Seg_3078" s="T239">make-PST1-3SG</ta>
            <ta e="T241" id="Seg_3079" s="T240">partridge-PL.[NOM]</ta>
            <ta e="T242" id="Seg_3080" s="T241">ready</ta>
            <ta e="T243" id="Seg_3081" s="T242">shore-DAT/LOC</ta>
            <ta e="T244" id="Seg_3082" s="T243">stand-PRS-3PL</ta>
            <ta e="T245" id="Seg_3083" s="T244">partridge.[NOM]</ta>
            <ta e="T246" id="Seg_3084" s="T245">leader-3SG.[NOM]</ta>
            <ta e="T247" id="Seg_3085" s="T246">people-3SG-DAT/LOC</ta>
            <ta e="T248" id="Seg_3086" s="T247">speak-PRS.[3SG]</ta>
            <ta e="T249" id="Seg_3087" s="T248">pike-ACC</ta>
            <ta e="T250" id="Seg_3088" s="T249">head-DAT/LOC</ta>
            <ta e="T251" id="Seg_3089" s="T250">shoot-FUT-EP-IMP.2PL</ta>
            <ta e="T252" id="Seg_3090" s="T251">back.part.of.a.fish-3SG-DAT/LOC</ta>
            <ta e="T253" id="Seg_3091" s="T252">die-PTCP.HAB-3SG</ta>
            <ta e="T254" id="Seg_3092" s="T253">NEG.[3SG]</ta>
            <ta e="T255" id="Seg_3093" s="T254">partridge.[NOM]</ta>
            <ta e="T256" id="Seg_3094" s="T255">army-3SG.[NOM]</ta>
            <ta e="T257" id="Seg_3095" s="T256">arrow-PL-3SG-ACC</ta>
            <ta e="T258" id="Seg_3096" s="T257">release-PST2.[3SG]</ta>
            <ta e="T259" id="Seg_3097" s="T258">bow.[NOM]</ta>
            <ta e="T260" id="Seg_3098" s="T259">bowstring-EP-3SG-GEN</ta>
            <ta e="T261" id="Seg_3099" s="T260">sound-3SG-ACC</ta>
            <ta e="T262" id="Seg_3100" s="T261">be.heard-CVB.ANT</ta>
            <ta e="T263" id="Seg_3101" s="T262">pike-PL.[NOM]</ta>
            <ta e="T264" id="Seg_3102" s="T263">middle.[NOM]</ta>
            <ta e="T265" id="Seg_3103" s="T264">in.the.direction</ta>
            <ta e="T266" id="Seg_3104" s="T265">turn-CVB.SIM</ta>
            <ta e="T267" id="Seg_3105" s="T266">give-CVB.SEQ</ta>
            <ta e="T268" id="Seg_3106" s="T267">turn-PST2-3PL</ta>
            <ta e="T269" id="Seg_3107" s="T268">partridge-PL.[NOM]</ta>
            <ta e="T270" id="Seg_3108" s="T269">arrow-3PL.[NOM]</ta>
            <ta e="T271" id="Seg_3109" s="T270">pike.[NOM]</ta>
            <ta e="T272" id="Seg_3110" s="T271">back.part.of.a.fish-3SG-DAT/LOC</ta>
            <ta e="T273" id="Seg_3111" s="T272">NEG</ta>
            <ta e="T274" id="Seg_3112" s="T273">back.part.of.a.fish-3SG-DAT/LOC</ta>
            <ta e="T275" id="Seg_3113" s="T274">give-CVB.SEQ</ta>
            <ta e="T276" id="Seg_3114" s="T275">go-PST2.[3SG]</ta>
            <ta e="T277" id="Seg_3115" s="T276">pike-PL.[NOM]</ta>
            <ta e="T278" id="Seg_3116" s="T277">turn-CVB.SEQ</ta>
            <ta e="T279" id="Seg_3117" s="T278">come-PST2-3PL</ta>
            <ta e="T280" id="Seg_3118" s="T279">partridge-PL.[NOM]</ta>
            <ta e="T281" id="Seg_3119" s="T280">arrow.[NOM]</ta>
            <ta e="T282" id="Seg_3120" s="T281">get-PTCP.FUT-3PL-DAT/LOC</ta>
            <ta e="T283" id="Seg_3121" s="T282">until</ta>
            <ta e="T284" id="Seg_3122" s="T283">pike-PL.[NOM]</ta>
            <ta e="T285" id="Seg_3123" s="T284">shoot-PST2-3PL</ta>
            <ta e="T286" id="Seg_3124" s="T285">partridge-PL.[NOM]</ta>
            <ta e="T287" id="Seg_3125" s="T286">heart-3PL-ACC</ta>
            <ta e="T288" id="Seg_3126" s="T287">target-CVB.SEQ</ta>
            <ta e="T289" id="Seg_3127" s="T288">partridge-PL.[NOM]</ta>
            <ta e="T290" id="Seg_3128" s="T289">turn-CVB.SEQ</ta>
            <ta e="T291" id="Seg_3129" s="T290">fly-CVB.SIM</ta>
            <ta e="T292" id="Seg_3130" s="T291">make-CVB.SIM</ta>
            <ta e="T293" id="Seg_3131" s="T292">middle-3SG.[NOM]</ta>
            <ta e="T294" id="Seg_3132" s="T293">in.the.direction</ta>
            <ta e="T295" id="Seg_3133" s="T294">jump-CVB.SEQ</ta>
            <ta e="T296" id="Seg_3134" s="T295">give-PST2-3PL</ta>
            <ta e="T297" id="Seg_3135" s="T296">pike-PL.[NOM]</ta>
            <ta e="T346" id="Seg_3136" s="T297">arrow-3PL.[NOM]</ta>
            <ta e="T298" id="Seg_3137" s="T346">partridge-PL.[NOM]</ta>
            <ta e="T299" id="Seg_3138" s="T298">own-3PL-GEN</ta>
            <ta e="T300" id="Seg_3139" s="T299">muscle-DAT/LOC</ta>
            <ta e="T301" id="Seg_3140" s="T300">NEG</ta>
            <ta e="T302" id="Seg_3141" s="T301">muscle-DAT/LOC</ta>
            <ta e="T303" id="Seg_3142" s="T302">leg-3PL-DAT/LOC</ta>
            <ta e="T304" id="Seg_3143" s="T303">like.that</ta>
            <ta e="T305" id="Seg_3144" s="T304">fight-PST2-3PL</ta>
            <ta e="T306" id="Seg_3145" s="T305">what.kind.of-3PL.[NOM]</ta>
            <ta e="T307" id="Seg_3146" s="T306">NEG</ta>
            <ta e="T308" id="Seg_3147" s="T307">kill-RECP/COLL-EP-NEG.[3SG]</ta>
            <ta e="T309" id="Seg_3148" s="T308">win-RECP/COLL-EP-NEG.[3SG]</ta>
            <ta e="T310" id="Seg_3149" s="T309">arrow-PL-3SG.[NOM]</ta>
            <ta e="T311" id="Seg_3150" s="T310">completely</ta>
            <ta e="T312" id="Seg_3151" s="T311">run.out-CVB.SEQ</ta>
            <ta e="T313" id="Seg_3152" s="T312">fight-CVB.SEQ</ta>
            <ta e="T314" id="Seg_3153" s="T313">stop-PST2-3PL</ta>
            <ta e="T315" id="Seg_3154" s="T314">pike.[NOM]</ta>
            <ta e="T316" id="Seg_3155" s="T315">leader-3SG.[NOM]</ta>
            <ta e="T317" id="Seg_3156" s="T316">now</ta>
            <ta e="T318" id="Seg_3157" s="T317">fight-CVB.SEQ</ta>
            <ta e="T319" id="Seg_3158" s="T318">stop-IMP.1PL</ta>
            <ta e="T320" id="Seg_3159" s="T319">2PL.[NOM]</ta>
            <ta e="T321" id="Seg_3160" s="T320">whereto</ta>
            <ta e="T322" id="Seg_3161" s="T321">want-PTCP.PRS</ta>
            <ta e="T323" id="Seg_3162" s="T322">fly-CVB.SIM</ta>
            <ta e="T324" id="Seg_3163" s="T323">go-EP-IMP.2PL</ta>
            <ta e="T325" id="Seg_3164" s="T324">where</ta>
            <ta e="T326" id="Seg_3165" s="T325">want-PTCP.PRS</ta>
            <ta e="T327" id="Seg_3166" s="T326">eat-IMP.2PL</ta>
            <ta e="T328" id="Seg_3167" s="T327">say-PRS.[3SG]</ta>
            <ta e="T329" id="Seg_3168" s="T328">from.then.on</ta>
            <ta e="T330" id="Seg_3169" s="T329">partridge-PL.[NOM]</ta>
            <ta e="T331" id="Seg_3170" s="T330">arrow-3PL.[NOM]</ta>
            <ta e="T332" id="Seg_3171" s="T331">pike.[NOM]</ta>
            <ta e="T333" id="Seg_3172" s="T332">back.part.of.a.fish-3SG-DAT/LOC</ta>
            <ta e="T334" id="Seg_3173" s="T333">forked</ta>
            <ta e="T335" id="Seg_3174" s="T334">bone.[NOM]</ta>
            <ta e="T336" id="Seg_3175" s="T335">become-PST2-3PL</ta>
            <ta e="T337" id="Seg_3176" s="T336">pike-PL.[NOM]</ta>
            <ta e="T338" id="Seg_3177" s="T337">arrow-3PL.[NOM]</ta>
            <ta e="T339" id="Seg_3178" s="T338">partridge.[NOM]</ta>
            <ta e="T340" id="Seg_3179" s="T339">leg-3SG-GEN</ta>
            <ta e="T341" id="Seg_3180" s="T340">muscle-3SG-DAT/LOC</ta>
            <ta e="T342" id="Seg_3181" s="T341">sinew.[NOM]</ta>
            <ta e="T343" id="Seg_3182" s="T342">bone.[NOM]</ta>
            <ta e="T344" id="Seg_3183" s="T343">become-PST2-3PL</ta>
            <ta e="T345" id="Seg_3184" s="T344">last-3SG.[NOM]</ta>
         </annotation>
         <annotation name="gg" tierref="gd">
            <ta e="T1" id="Seg_3185" s="T0">doch</ta>
            <ta e="T2" id="Seg_3186" s="T1">vor.langer.Zeit</ta>
            <ta e="T3" id="Seg_3187" s="T2">Erde.[NOM]-Himmel.[NOM]</ta>
            <ta e="T4" id="Seg_3188" s="T3">entstehen-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T5" id="Seg_3189" s="T4">Rebhuhn.[NOM]</ta>
            <ta e="T6" id="Seg_3190" s="T5">Tundra.[NOM]</ta>
            <ta e="T7" id="Seg_3191" s="T6">Erde-ADJZ.[NOM]</ta>
            <ta e="T8" id="Seg_3192" s="T7">man.sagt</ta>
            <ta e="T9" id="Seg_3193" s="T8">eins</ta>
            <ta e="T10" id="Seg_3194" s="T9">Jahr.[NOM]</ta>
            <ta e="T11" id="Seg_3195" s="T10">Schneesturm-PROPR.[NOM]</ta>
            <ta e="T12" id="Seg_3196" s="T11">werden-PST2.[3SG]</ta>
            <ta e="T13" id="Seg_3197" s="T12">im.Winter</ta>
            <ta e="T14" id="Seg_3198" s="T13">regnen-PST2.[3SG]</ta>
            <ta e="T15" id="Seg_3199" s="T14">Schnee-3SG.[NOM]</ta>
            <ta e="T16" id="Seg_3200" s="T15">ganz-3SG.[NOM]</ta>
            <ta e="T17" id="Seg_3201" s="T16">Eis.[NOM]</ta>
            <ta e="T18" id="Seg_3202" s="T17">werden-PST2.[3SG]</ta>
            <ta e="T19" id="Seg_3203" s="T18">eins</ta>
            <ta e="T20" id="Seg_3204" s="T19">NEG</ta>
            <ta e="T21" id="Seg_3205" s="T20">Loch.in.der.Schneedecke.[NOM]</ta>
            <ta e="T22" id="Seg_3206" s="T21">Seite-3SG.[NOM]</ta>
            <ta e="T23" id="Seg_3207" s="T22">NEG.EX</ta>
            <ta e="T24" id="Seg_3208" s="T23">Rebhuhn-PL.[NOM]</ta>
            <ta e="T25" id="Seg_3209" s="T24">hungern-PST2-3PL</ta>
            <ta e="T26" id="Seg_3210" s="T25">3PL.[NOM]</ta>
            <ta e="T27" id="Seg_3211" s="T26">Nahrung.[NOM]</ta>
            <ta e="T28" id="Seg_3212" s="T27">suchen-CVB.SIM</ta>
            <ta e="T29" id="Seg_3213" s="T28">Wald.[NOM]</ta>
            <ta e="T30" id="Seg_3214" s="T29">in.Richtung</ta>
            <ta e="T31" id="Seg_3215" s="T30">fliegen-PST2-3PL</ta>
            <ta e="T32" id="Seg_3216" s="T31">eins</ta>
            <ta e="T33" id="Seg_3217" s="T32">meist</ta>
            <ta e="T34" id="Seg_3218" s="T33">alt</ta>
            <ta e="T35" id="Seg_3219" s="T34">Anführer.[NOM]</ta>
            <ta e="T36" id="Seg_3220" s="T35">Rebhuhn-PROPR-3PL</ta>
            <ta e="T37" id="Seg_3221" s="T36">Wald-DAT/LOC</ta>
            <ta e="T38" id="Seg_3222" s="T37">ankommen-PST2-3PL</ta>
            <ta e="T39" id="Seg_3223" s="T38">wo</ta>
            <ta e="T40" id="Seg_3224" s="T39">NEG</ta>
            <ta e="T41" id="Seg_3225" s="T40">Loch.in.der.Schneedecke.[NOM]</ta>
            <ta e="T42" id="Seg_3226" s="T41">zu.sehen.sein-EP-NEG.[3SG]</ta>
            <ta e="T43" id="Seg_3227" s="T42">Strauch.[NOM]</ta>
            <ta e="T44" id="Seg_3228" s="T43">Wipfel-3SG.[NOM]</ta>
            <ta e="T45" id="Seg_3229" s="T44">NEG</ta>
            <ta e="T46" id="Seg_3230" s="T45">bemerken-PASS/REFL-EP-NEG.[3SG]</ta>
            <ta e="T47" id="Seg_3231" s="T46">stöbern-CVB.SEQ</ta>
            <ta e="T48" id="Seg_3232" s="T47">werfen-PST2.[3SG]</ta>
            <ta e="T49" id="Seg_3233" s="T48">Baum.[NOM]</ta>
            <ta e="T50" id="Seg_3234" s="T49">nur</ta>
            <ta e="T51" id="Seg_3235" s="T50">schwarz.sein-PRS.[3SG]</ta>
            <ta e="T52" id="Seg_3236" s="T51">Anführer-3PL.[NOM]</ta>
            <ta e="T53" id="Seg_3237" s="T52">raten-PRS.[3SG]</ta>
            <ta e="T54" id="Seg_3238" s="T53">doch</ta>
            <ta e="T55" id="Seg_3239" s="T54">jetzt</ta>
            <ta e="T56" id="Seg_3240" s="T55">sich.retten-NEG-PST1-1PL</ta>
            <ta e="T57" id="Seg_3241" s="T56">1PL.[NOM]</ta>
            <ta e="T58" id="Seg_3242" s="T57">wo</ta>
            <ta e="T59" id="Seg_3243" s="T58">gehen-CVB.SEQ</ta>
            <ta e="T60" id="Seg_3244" s="T59">sterben-FUT-1PL=Q</ta>
            <ta e="T61" id="Seg_3245" s="T60">1PL.[NOM]</ta>
            <ta e="T62" id="Seg_3246" s="T61">so</ta>
            <ta e="T63" id="Seg_3247" s="T62">denken-PRS-1SG</ta>
            <ta e="T64" id="Seg_3248" s="T63">was.für.ein</ta>
            <ta e="T65" id="Seg_3249" s="T64">INDEF</ta>
            <ta e="T66" id="Seg_3250" s="T65">Zeit-DAT/LOC</ta>
            <ta e="T67" id="Seg_3251" s="T66">eins</ta>
            <ta e="T68" id="Seg_3252" s="T67">Ort-ACC</ta>
            <ta e="T69" id="Seg_3253" s="T68">sehen-PTCP.PST-PROPR-1SG</ta>
            <ta e="T70" id="Seg_3254" s="T69">eins</ta>
            <ta e="T71" id="Seg_3255" s="T70">groß</ta>
            <ta e="T72" id="Seg_3256" s="T71">Fluss.[NOM]</ta>
            <ta e="T73" id="Seg_3257" s="T72">es.gibt</ta>
            <ta e="T74" id="Seg_3258" s="T73">jenes</ta>
            <ta e="T75" id="Seg_3259" s="T74">Fluss-EP-2SG.[NOM]</ta>
            <ta e="T76" id="Seg_3260" s="T75">wie-PROPR</ta>
            <ta e="T77" id="Seg_3261" s="T76">Schneesturm-DAT/LOC</ta>
            <ta e="T78" id="Seg_3262" s="T77">stöbern-EP-PASS/REFL-PTCP.HAB-3SG</ta>
            <ta e="T79" id="Seg_3263" s="T78">NEG.[3SG]</ta>
            <ta e="T80" id="Seg_3264" s="T79">mancher</ta>
            <ta e="T81" id="Seg_3265" s="T80">Ort-3SG.[NOM]</ta>
            <ta e="T82" id="Seg_3266" s="T81">Winter-ACC</ta>
            <ta e="T83" id="Seg_3267" s="T82">während</ta>
            <ta e="T84" id="Seg_3268" s="T83">Eisloch.[NOM]</ta>
            <ta e="T85" id="Seg_3269" s="T84">sein-PRS.[3SG]</ta>
            <ta e="T86" id="Seg_3270" s="T85">Ufer-3SG-ABL</ta>
            <ta e="T87" id="Seg_3271" s="T86">Weidenstrauch-PROPR.[NOM]</ta>
            <ta e="T88" id="Seg_3272" s="T87">Felsen-3SG.[NOM]</ta>
            <ta e="T89" id="Seg_3273" s="T88">ganz</ta>
            <ta e="T90" id="Seg_3274" s="T89">Rosmarinheide-PROPR.[NOM]</ta>
            <ta e="T91" id="Seg_3275" s="T90">Beere-PROPR.[NOM]</ta>
            <ta e="T92" id="Seg_3276" s="T91">Gras-PROPR.[NOM]</ta>
            <ta e="T93" id="Seg_3277" s="T92">jenes</ta>
            <ta e="T94" id="Seg_3278" s="T93">Fluss-DAT/LOC</ta>
            <ta e="T95" id="Seg_3279" s="T94">Hecht.[NOM]</ta>
            <ta e="T96" id="Seg_3280" s="T95">eigen-3SG</ta>
            <ta e="T97" id="Seg_3281" s="T96">Aufenthaltsort-3SG.[NOM]</ta>
            <ta e="T98" id="Seg_3282" s="T97">lediglich</ta>
            <ta e="T99" id="Seg_3283" s="T98">Hecht-PL.[NOM]</ta>
            <ta e="T100" id="Seg_3284" s="T99">nur</ta>
            <ta e="T101" id="Seg_3285" s="T100">1PL-ACC</ta>
            <ta e="T102" id="Seg_3286" s="T101">gelangen-EP-CAUS-FUT-3PL</ta>
            <ta e="T103" id="Seg_3287" s="T102">NEG-3SG</ta>
            <ta e="T104" id="Seg_3288" s="T103">vor.langer.Zeit</ta>
            <ta e="T105" id="Seg_3289" s="T104">Zeit-DAT/LOC</ta>
            <ta e="T106" id="Seg_3290" s="T105">geschworen</ta>
            <ta e="T107" id="Seg_3291" s="T106">Wort-PROPR-3PL</ta>
            <ta e="T108" id="Seg_3292" s="T107">Rebhuhn-ACC</ta>
            <ta e="T109" id="Seg_3293" s="T108">mit</ta>
            <ta e="T110" id="Seg_3294" s="T109">Rebhuhn.[NOM]</ta>
            <ta e="T111" id="Seg_3295" s="T110">Hecht.[NOM]</ta>
            <ta e="T112" id="Seg_3296" s="T111">Land-3SG-DAT/LOC</ta>
            <ta e="T113" id="Seg_3297" s="T112">gehen-FUT.[3SG]</ta>
            <ta e="T114" id="Seg_3298" s="T113">NEG-3SG</ta>
            <ta e="T115" id="Seg_3299" s="T114">sagen-CVB.SEQ</ta>
            <ta e="T116" id="Seg_3300" s="T115">Hecht.[NOM]</ta>
            <ta e="T117" id="Seg_3301" s="T116">Rebhuhn.[NOM]</ta>
            <ta e="T118" id="Seg_3302" s="T117">Land-3SG-DAT/LOC</ta>
            <ta e="T119" id="Seg_3303" s="T118">erinnern-PTCP.PRS</ta>
            <ta e="T120" id="Seg_3304" s="T119">sein-TEMP-3PL</ta>
            <ta e="T121" id="Seg_3305" s="T120">gut-INSTR</ta>
            <ta e="T122" id="Seg_3306" s="T121">geben-FUT-3PL</ta>
            <ta e="T123" id="Seg_3307" s="T122">NEG-3SG</ta>
            <ta e="T124" id="Seg_3308" s="T123">1PL.[NOM]</ta>
            <ta e="T125" id="Seg_3309" s="T124">dann</ta>
            <ta e="T126" id="Seg_3310" s="T125">Krieg-INSTR</ta>
            <ta e="T127" id="Seg_3311" s="T126">nur</ta>
            <ta e="T128" id="Seg_3312" s="T127">hineingehen-EP-MED-FUT-1PL</ta>
            <ta e="T129" id="Seg_3313" s="T128">Rebhuhn-PL.[NOM]</ta>
            <ta e="T130" id="Seg_3314" s="T129">Wahrheit.[NOM]</ta>
            <ta e="T131" id="Seg_3315" s="T130">hier</ta>
            <ta e="T132" id="Seg_3316" s="T131">jenes-PTCP.PST-EP-INSTR</ta>
            <ta e="T133" id="Seg_3317" s="T132">sterben-FUT-1PL</ta>
            <ta e="T134" id="Seg_3318" s="T133">Q</ta>
            <ta e="T135" id="Seg_3319" s="T134">jenes.[NOM]</ta>
            <ta e="T136" id="Seg_3320" s="T135">anstatt</ta>
            <ta e="T137" id="Seg_3321" s="T136">kämpfen-FUT-1PL</ta>
            <ta e="T138" id="Seg_3322" s="T137">gehen-IMP.1PL</ta>
            <ta e="T139" id="Seg_3323" s="T138">sagen-PST1-3PL</ta>
            <ta e="T140" id="Seg_3324" s="T139">Rebhuhn-PL.[NOM]</ta>
            <ta e="T141" id="Seg_3325" s="T140">fliegen-PRS-3PL</ta>
            <ta e="T142" id="Seg_3326" s="T141">Fluss-DAT/LOC</ta>
            <ta e="T143" id="Seg_3327" s="T142">ankommen-PRS-3PL</ta>
            <ta e="T144" id="Seg_3328" s="T143">Nahrung.[NOM]</ta>
            <ta e="T145" id="Seg_3329" s="T144">sich.ausbreiten-CVB.SIM</ta>
            <ta e="T146" id="Seg_3330" s="T145">liegen-PTCP.PRS</ta>
            <ta e="T147" id="Seg_3331" s="T146">Ort-3SG.[NOM]</ta>
            <ta e="T148" id="Seg_3332" s="T147">sein-PST2.[3SG]</ta>
            <ta e="T149" id="Seg_3333" s="T148">essen-CVB.SIM</ta>
            <ta e="T150" id="Seg_3334" s="T149">sich.setzen-PST2-3PL</ta>
            <ta e="T151" id="Seg_3335" s="T150">Eisloch.[NOM]</ta>
            <ta e="T152" id="Seg_3336" s="T151">durch</ta>
            <ta e="T153" id="Seg_3337" s="T152">Hecht.[NOM]</ta>
            <ta e="T154" id="Seg_3338" s="T153">Anführer-3SG-GEN</ta>
            <ta e="T155" id="Seg_3339" s="T154">Kopf-3SG.[NOM]</ta>
            <ta e="T156" id="Seg_3340" s="T155">sich.hinauslehnen-NMNZ.[NOM]</ta>
            <ta e="T157" id="Seg_3341" s="T156">machen-PST2.[3SG]</ta>
            <ta e="T158" id="Seg_3342" s="T157">na=EMPH</ta>
            <ta e="T159" id="Seg_3343" s="T158">Rebhuhn-PL.[NOM]</ta>
            <ta e="T160" id="Seg_3344" s="T159">warum</ta>
            <ta e="T161" id="Seg_3345" s="T160">Ort-DAT/LOC</ta>
            <ta e="T162" id="Seg_3346" s="T161">kommen-PST2-2PL=Q</ta>
            <ta e="T163" id="Seg_3347" s="T162">vor.langer.Zeit</ta>
            <ta e="T164" id="Seg_3348" s="T163">Zeit-DAT/LOC</ta>
            <ta e="T165" id="Seg_3349" s="T164">geschworen</ta>
            <ta e="T166" id="Seg_3350" s="T165">Wort-PROPR.[NOM]</ta>
            <ta e="T167" id="Seg_3351" s="T166">sein-PST1-1PL</ta>
            <ta e="T168" id="Seg_3352" s="T167">2PL.[NOM]</ta>
            <ta e="T169" id="Seg_3353" s="T168">Ort-2PL-DAT/LOC</ta>
            <ta e="T170" id="Seg_3354" s="T169">1PL.[NOM]</ta>
            <ta e="T171" id="Seg_3355" s="T170">gehen-NEG-1PL</ta>
            <ta e="T172" id="Seg_3356" s="T171">2PL.[NOM]</ta>
            <ta e="T173" id="Seg_3357" s="T172">1PL.[NOM]</ta>
            <ta e="T174" id="Seg_3358" s="T173">Ort-1PL-DAT/LOC</ta>
            <ta e="T175" id="Seg_3359" s="T174">warum</ta>
            <ta e="T176" id="Seg_3360" s="T175">kommen-PST1-2PL</ta>
            <ta e="T177" id="Seg_3361" s="T176">warten-EP-NEG-IMP.2PL</ta>
            <ta e="T178" id="Seg_3362" s="T177">sofort</ta>
            <ta e="T179" id="Seg_3363" s="T178">gehen-EP-IMP.2PL</ta>
            <ta e="T180" id="Seg_3364" s="T179">von.hier</ta>
            <ta e="T181" id="Seg_3365" s="T180">Strauch.[NOM]</ta>
            <ta e="T182" id="Seg_3366" s="T181">eigen-3SG-GEN</ta>
            <ta e="T183" id="Seg_3367" s="T182">Blatt-3SG-ACC</ta>
            <ta e="T184" id="Seg_3368" s="T183">Rosmarinheide.[NOM]</ta>
            <ta e="T185" id="Seg_3369" s="T184">eigen-3SG-GEN</ta>
            <ta e="T186" id="Seg_3370" s="T185">Spitze-3SG-ACC</ta>
            <ta e="T187" id="Seg_3371" s="T186">NEG</ta>
            <ta e="T188" id="Seg_3372" s="T187">essen-CAUS-FUT-1SG</ta>
            <ta e="T189" id="Seg_3373" s="T188">NEG-3SG</ta>
            <ta e="T190" id="Seg_3374" s="T189">Gras-1SG-ACC</ta>
            <ta e="T191" id="Seg_3375" s="T190">NEG</ta>
            <ta e="T192" id="Seg_3376" s="T191">essen-CAUS-NEG-1SG</ta>
            <ta e="T193" id="Seg_3377" s="T192">Kies-1SG-ACC</ta>
            <ta e="T194" id="Seg_3378" s="T193">NEG</ta>
            <ta e="T195" id="Seg_3379" s="T194">graben-CAUS-FUT-1SG</ta>
            <ta e="T196" id="Seg_3380" s="T195">NEG-3SG</ta>
            <ta e="T197" id="Seg_3381" s="T196">jenes-DAT/LOC</ta>
            <ta e="T198" id="Seg_3382" s="T197">Rebhuhn.[NOM]</ta>
            <ta e="T199" id="Seg_3383" s="T198">Anführer-3SG.[NOM]</ta>
            <ta e="T200" id="Seg_3384" s="T199">sagen-PRS.[3SG]</ta>
            <ta e="T201" id="Seg_3385" s="T200">1PL.[NOM]</ta>
            <ta e="T202" id="Seg_3386" s="T201">gut-INSTR</ta>
            <ta e="T203" id="Seg_3387" s="T202">gehen-NEG-1PL</ta>
            <ta e="T204" id="Seg_3388" s="T203">Krieg-INSTR</ta>
            <ta e="T205" id="Seg_3389" s="T204">wenn.auch</ta>
            <ta e="T206" id="Seg_3390" s="T205">dieses</ta>
            <ta e="T207" id="Seg_3391" s="T206">Ort-ACC</ta>
            <ta e="T208" id="Seg_3392" s="T207">nehmen-FUT-1PL</ta>
            <ta e="T209" id="Seg_3393" s="T208">Hecht.[NOM]</ta>
            <ta e="T210" id="Seg_3394" s="T209">Anführer-3SG.[NOM]</ta>
            <ta e="T211" id="Seg_3395" s="T210">1SG.[NOM]</ta>
            <ta e="T212" id="Seg_3396" s="T211">gut-INSTR</ta>
            <ta e="T213" id="Seg_3397" s="T212">geben-NEG-1SG</ta>
            <ta e="T214" id="Seg_3398" s="T213">Hecht-ACC</ta>
            <ta e="T215" id="Seg_3399" s="T214">vollständig</ta>
            <ta e="T216" id="Seg_3400" s="T215">töten-TEMP-2PL</ta>
            <ta e="T217" id="Seg_3401" s="T216">nehmen-FUT-2PL</ta>
            <ta e="T218" id="Seg_3402" s="T217">kämpfen-FUT-1PL</ta>
            <ta e="T219" id="Seg_3403" s="T218">halten-IMP.2PL</ta>
            <ta e="T220" id="Seg_3404" s="T219">1SG.[NOM]</ta>
            <ta e="T221" id="Seg_3405" s="T220">Leute-1SG-ACC</ta>
            <ta e="T222" id="Seg_3406" s="T221">sammeln-MED-FUT-1SG</ta>
            <ta e="T223" id="Seg_3407" s="T222">sagen-PRS.[3SG]</ta>
            <ta e="T224" id="Seg_3408" s="T223">Hecht.[NOM]</ta>
            <ta e="T225" id="Seg_3409" s="T224">Anführer-3SG.[NOM]</ta>
            <ta e="T226" id="Seg_3410" s="T225">sich.drehen-PRS.[3SG]</ta>
            <ta e="T227" id="Seg_3411" s="T226">und</ta>
            <ta e="T228" id="Seg_3412" s="T227">Schwanz-3SG-INSTR</ta>
            <ta e="T229" id="Seg_3413" s="T228">platsch</ta>
            <ta e="T230" id="Seg_3414" s="T229">machen-PST2.[3SG]</ta>
            <ta e="T231" id="Seg_3415" s="T230">gehen-PST2.[3SG]</ta>
            <ta e="T232" id="Seg_3416" s="T231">Leute.[NOM]</ta>
            <ta e="T233" id="Seg_3417" s="T232">sammeln-MED-CVB.SIM</ta>
            <ta e="T234" id="Seg_3418" s="T233">Hecht.[NOM]</ta>
            <ta e="T235" id="Seg_3419" s="T234">Schwarm-3SG-ACC</ta>
            <ta e="T236" id="Seg_3420" s="T235">ganz-3SG-ACC</ta>
            <ta e="T237" id="Seg_3421" s="T236">holen-CVB.SEQ</ta>
            <ta e="T238" id="Seg_3422" s="T237">Eisloch-ACC</ta>
            <ta e="T239" id="Seg_3423" s="T238">schwarz-ADVZ</ta>
            <ta e="T240" id="Seg_3424" s="T239">machen-PST1-3SG</ta>
            <ta e="T241" id="Seg_3425" s="T240">Rebhuhn-PL.[NOM]</ta>
            <ta e="T242" id="Seg_3426" s="T241">fertig</ta>
            <ta e="T243" id="Seg_3427" s="T242">Ufer-DAT/LOC</ta>
            <ta e="T244" id="Seg_3428" s="T243">stehen-PRS-3PL</ta>
            <ta e="T245" id="Seg_3429" s="T244">Rebhuhn.[NOM]</ta>
            <ta e="T246" id="Seg_3430" s="T245">Anführer-3SG.[NOM]</ta>
            <ta e="T247" id="Seg_3431" s="T246">Leute-3SG-DAT/LOC</ta>
            <ta e="T248" id="Seg_3432" s="T247">sprechen-PRS.[3SG]</ta>
            <ta e="T249" id="Seg_3433" s="T248">Hecht-ACC</ta>
            <ta e="T250" id="Seg_3434" s="T249">Kopf-DAT/LOC</ta>
            <ta e="T251" id="Seg_3435" s="T250">schießen-FUT-EP-IMP.2PL</ta>
            <ta e="T252" id="Seg_3436" s="T251">Rückenteil.eines.Fisches-3SG-DAT/LOC</ta>
            <ta e="T253" id="Seg_3437" s="T252">sterben-PTCP.HAB-3SG</ta>
            <ta e="T254" id="Seg_3438" s="T253">NEG.[3SG]</ta>
            <ta e="T255" id="Seg_3439" s="T254">Rebhuhn.[NOM]</ta>
            <ta e="T256" id="Seg_3440" s="T255">Heer-3SG.[NOM]</ta>
            <ta e="T257" id="Seg_3441" s="T256">Pfeil-PL-3SG-ACC</ta>
            <ta e="T258" id="Seg_3442" s="T257">(los)lassen-PST2.[3SG]</ta>
            <ta e="T259" id="Seg_3443" s="T258">Bogen.[NOM]</ta>
            <ta e="T260" id="Seg_3444" s="T259">Bogensehne-EP-3SG-GEN</ta>
            <ta e="T261" id="Seg_3445" s="T260">Geräusch-3SG-ACC</ta>
            <ta e="T262" id="Seg_3446" s="T261">gehört.werden-CVB.ANT</ta>
            <ta e="T263" id="Seg_3447" s="T262">Hecht-PL.[NOM]</ta>
            <ta e="T264" id="Seg_3448" s="T263">Mitte.[NOM]</ta>
            <ta e="T265" id="Seg_3449" s="T264">in.Richtung</ta>
            <ta e="T266" id="Seg_3450" s="T265">sich.drehen-CVB.SIM</ta>
            <ta e="T267" id="Seg_3451" s="T266">geben-CVB.SEQ</ta>
            <ta e="T268" id="Seg_3452" s="T267">sich.drehen-PST2-3PL</ta>
            <ta e="T269" id="Seg_3453" s="T268">Rebhuhn-PL.[NOM]</ta>
            <ta e="T270" id="Seg_3454" s="T269">Pfeil-3PL.[NOM]</ta>
            <ta e="T271" id="Seg_3455" s="T270">Hecht.[NOM]</ta>
            <ta e="T272" id="Seg_3456" s="T271">Rückenteil.eines.Fisches-3SG-DAT/LOC</ta>
            <ta e="T273" id="Seg_3457" s="T272">NEG</ta>
            <ta e="T274" id="Seg_3458" s="T273">Rückenteil.eines.Fisches-3SG-DAT/LOC</ta>
            <ta e="T275" id="Seg_3459" s="T274">geben-CVB.SEQ</ta>
            <ta e="T276" id="Seg_3460" s="T275">gehen-PST2.[3SG]</ta>
            <ta e="T277" id="Seg_3461" s="T276">Hecht-PL.[NOM]</ta>
            <ta e="T278" id="Seg_3462" s="T277">sich.drehen-CVB.SEQ</ta>
            <ta e="T279" id="Seg_3463" s="T278">kommen-PST2-3PL</ta>
            <ta e="T280" id="Seg_3464" s="T279">Rebhuhn-PL.[NOM]</ta>
            <ta e="T281" id="Seg_3465" s="T280">Pfeil.[NOM]</ta>
            <ta e="T282" id="Seg_3466" s="T281">besorgen-PTCP.FUT-3PL-DAT/LOC</ta>
            <ta e="T283" id="Seg_3467" s="T282">bis.zu</ta>
            <ta e="T284" id="Seg_3468" s="T283">Hecht-PL.[NOM]</ta>
            <ta e="T285" id="Seg_3469" s="T284">schießen-PST2-3PL</ta>
            <ta e="T286" id="Seg_3470" s="T285">Rebhuhn-PL.[NOM]</ta>
            <ta e="T287" id="Seg_3471" s="T286">Herz-3PL-ACC</ta>
            <ta e="T288" id="Seg_3472" s="T287">zielen-CVB.SEQ</ta>
            <ta e="T289" id="Seg_3473" s="T288">Rebhuhn-PL.[NOM]</ta>
            <ta e="T290" id="Seg_3474" s="T289">sich.drehen-CVB.SEQ</ta>
            <ta e="T291" id="Seg_3475" s="T290">fliegen-CVB.SIM</ta>
            <ta e="T292" id="Seg_3476" s="T291">machen-CVB.SIM</ta>
            <ta e="T293" id="Seg_3477" s="T292">Mitte-3SG.[NOM]</ta>
            <ta e="T294" id="Seg_3478" s="T293">in.Richtung</ta>
            <ta e="T295" id="Seg_3479" s="T294">springen-CVB.SEQ</ta>
            <ta e="T296" id="Seg_3480" s="T295">geben-PST2-3PL</ta>
            <ta e="T297" id="Seg_3481" s="T296">Hecht-PL.[NOM]</ta>
            <ta e="T346" id="Seg_3482" s="T297">Pfeil-3PL.[NOM]</ta>
            <ta e="T298" id="Seg_3483" s="T346">Rebhuhn-PL.[NOM]</ta>
            <ta e="T299" id="Seg_3484" s="T298">eigen-3PL-GEN</ta>
            <ta e="T300" id="Seg_3485" s="T299">Muskel-DAT/LOC</ta>
            <ta e="T301" id="Seg_3486" s="T300">NEG</ta>
            <ta e="T302" id="Seg_3487" s="T301">Muskel-DAT/LOC</ta>
            <ta e="T303" id="Seg_3488" s="T302">Bein-3PL-DAT/LOC</ta>
            <ta e="T304" id="Seg_3489" s="T303">so</ta>
            <ta e="T305" id="Seg_3490" s="T304">kämpfen-PST2-3PL</ta>
            <ta e="T306" id="Seg_3491" s="T305">was.für.ein-3PL.[NOM]</ta>
            <ta e="T307" id="Seg_3492" s="T306">NEG</ta>
            <ta e="T308" id="Seg_3493" s="T307">töten-RECP/COLL-EP-NEG.[3SG]</ta>
            <ta e="T309" id="Seg_3494" s="T308">siegen-RECP/COLL-EP-NEG.[3SG]</ta>
            <ta e="T310" id="Seg_3495" s="T309">Pfeil-PL-3SG.[NOM]</ta>
            <ta e="T311" id="Seg_3496" s="T310">ganz</ta>
            <ta e="T312" id="Seg_3497" s="T311">zu.Ende.gehen-CVB.SEQ</ta>
            <ta e="T313" id="Seg_3498" s="T312">kämpfen-CVB.SEQ</ta>
            <ta e="T314" id="Seg_3499" s="T313">aufhören-PST2-3PL</ta>
            <ta e="T315" id="Seg_3500" s="T314">Hecht.[NOM]</ta>
            <ta e="T316" id="Seg_3501" s="T315">Anführer-3SG.[NOM]</ta>
            <ta e="T317" id="Seg_3502" s="T316">jetzt</ta>
            <ta e="T318" id="Seg_3503" s="T317">kämpfen-CVB.SEQ</ta>
            <ta e="T319" id="Seg_3504" s="T318">aufhören-IMP.1PL</ta>
            <ta e="T320" id="Seg_3505" s="T319">2PL.[NOM]</ta>
            <ta e="T321" id="Seg_3506" s="T320">wohin</ta>
            <ta e="T322" id="Seg_3507" s="T321">wollen-PTCP.PRS</ta>
            <ta e="T323" id="Seg_3508" s="T322">fliegen-CVB.SIM</ta>
            <ta e="T324" id="Seg_3509" s="T323">gehen-EP-IMP.2PL</ta>
            <ta e="T325" id="Seg_3510" s="T324">wo</ta>
            <ta e="T326" id="Seg_3511" s="T325">wollen-PTCP.PRS</ta>
            <ta e="T327" id="Seg_3512" s="T326">essen-IMP.2PL</ta>
            <ta e="T328" id="Seg_3513" s="T327">sagen-PRS.[3SG]</ta>
            <ta e="T329" id="Seg_3514" s="T328">von.da.an</ta>
            <ta e="T330" id="Seg_3515" s="T329">Rebhuhn-PL.[NOM]</ta>
            <ta e="T331" id="Seg_3516" s="T330">Pfeil-3PL.[NOM]</ta>
            <ta e="T332" id="Seg_3517" s="T331">Hecht.[NOM]</ta>
            <ta e="T333" id="Seg_3518" s="T332">Rückenteil.eines.Fisches-3SG-DAT/LOC</ta>
            <ta e="T334" id="Seg_3519" s="T333">gespalten</ta>
            <ta e="T335" id="Seg_3520" s="T334">Knochen.[NOM]</ta>
            <ta e="T336" id="Seg_3521" s="T335">werden-PST2-3PL</ta>
            <ta e="T337" id="Seg_3522" s="T336">Hecht-PL.[NOM]</ta>
            <ta e="T338" id="Seg_3523" s="T337">Pfeil-3PL.[NOM]</ta>
            <ta e="T339" id="Seg_3524" s="T338">Rebhuhn.[NOM]</ta>
            <ta e="T340" id="Seg_3525" s="T339">Bein-3SG-GEN</ta>
            <ta e="T341" id="Seg_3526" s="T340">Muskel-3SG-DAT/LOC</ta>
            <ta e="T342" id="Seg_3527" s="T341">Sehne.[NOM]</ta>
            <ta e="T343" id="Seg_3528" s="T342">Knochen.[NOM]</ta>
            <ta e="T344" id="Seg_3529" s="T343">werden-PST2-3PL</ta>
            <ta e="T345" id="Seg_3530" s="T344">letzter-3SG.[NOM]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_3531" s="T0">вот</ta>
            <ta e="T2" id="Seg_3532" s="T1">давно</ta>
            <ta e="T3" id="Seg_3533" s="T2">земля.[NOM]-небо.[NOM]</ta>
            <ta e="T4" id="Seg_3534" s="T3">создаваться-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T5" id="Seg_3535" s="T4">куропатка.[NOM]</ta>
            <ta e="T6" id="Seg_3536" s="T5">тундра.[NOM]</ta>
            <ta e="T7" id="Seg_3537" s="T6">земля-ADJZ.[NOM]</ta>
            <ta e="T8" id="Seg_3538" s="T7">говорят</ta>
            <ta e="T9" id="Seg_3539" s="T8">один</ta>
            <ta e="T10" id="Seg_3540" s="T9">год.[NOM]</ta>
            <ta e="T11" id="Seg_3541" s="T10">пурга-PROPR.[NOM]</ta>
            <ta e="T12" id="Seg_3542" s="T11">становиться-PST2.[3SG]</ta>
            <ta e="T13" id="Seg_3543" s="T12">зимой</ta>
            <ta e="T14" id="Seg_3544" s="T13">дождить-PST2.[3SG]</ta>
            <ta e="T15" id="Seg_3545" s="T14">снег-3SG.[NOM]</ta>
            <ta e="T16" id="Seg_3546" s="T15">целый-3SG.[NOM]</ta>
            <ta e="T17" id="Seg_3547" s="T16">лед.[NOM]</ta>
            <ta e="T18" id="Seg_3548" s="T17">становиться-PST2.[3SG]</ta>
            <ta e="T19" id="Seg_3549" s="T18">один</ta>
            <ta e="T20" id="Seg_3550" s="T19">NEG</ta>
            <ta e="T21" id="Seg_3551" s="T20">проталина.[NOM]</ta>
            <ta e="T22" id="Seg_3552" s="T21">сторона-3SG.[NOM]</ta>
            <ta e="T23" id="Seg_3553" s="T22">NEG.EX</ta>
            <ta e="T24" id="Seg_3554" s="T23">куропатка-PL.[NOM]</ta>
            <ta e="T25" id="Seg_3555" s="T24">голодать-PST2-3PL</ta>
            <ta e="T26" id="Seg_3556" s="T25">3PL.[NOM]</ta>
            <ta e="T27" id="Seg_3557" s="T26">пища.[NOM]</ta>
            <ta e="T28" id="Seg_3558" s="T27">искать-CVB.SIM</ta>
            <ta e="T29" id="Seg_3559" s="T28">лес.[NOM]</ta>
            <ta e="T30" id="Seg_3560" s="T29">в.сторону</ta>
            <ta e="T31" id="Seg_3561" s="T30">летать-PST2-3PL</ta>
            <ta e="T32" id="Seg_3562" s="T31">один</ta>
            <ta e="T33" id="Seg_3563" s="T32">самый</ta>
            <ta e="T34" id="Seg_3564" s="T33">старый</ta>
            <ta e="T35" id="Seg_3565" s="T34">руководитель.[NOM]</ta>
            <ta e="T36" id="Seg_3566" s="T35">куропатка-PROPR-3PL</ta>
            <ta e="T37" id="Seg_3567" s="T36">лес-DAT/LOC</ta>
            <ta e="T38" id="Seg_3568" s="T37">доезжать-PST2-3PL</ta>
            <ta e="T39" id="Seg_3569" s="T38">где</ta>
            <ta e="T40" id="Seg_3570" s="T39">NEG</ta>
            <ta e="T41" id="Seg_3571" s="T40">проталина.[NOM]</ta>
            <ta e="T42" id="Seg_3572" s="T41">быть.видно-EP-NEG.[3SG]</ta>
            <ta e="T43" id="Seg_3573" s="T42">куст.[NOM]</ta>
            <ta e="T44" id="Seg_3574" s="T43">верхушка-3SG.[NOM]</ta>
            <ta e="T45" id="Seg_3575" s="T44">NEG</ta>
            <ta e="T46" id="Seg_3576" s="T45">замечать-PASS/REFL-EP-NEG.[3SG]</ta>
            <ta e="T47" id="Seg_3577" s="T46">замести-CVB.SEQ</ta>
            <ta e="T48" id="Seg_3578" s="T47">бросать-PST2.[3SG]</ta>
            <ta e="T49" id="Seg_3579" s="T48">дерево.[NOM]</ta>
            <ta e="T50" id="Seg_3580" s="T49">только</ta>
            <ta e="T51" id="Seg_3581" s="T50">чернеть-PRS.[3SG]</ta>
            <ta e="T52" id="Seg_3582" s="T51">руководитель-3PL.[NOM]</ta>
            <ta e="T53" id="Seg_3583" s="T52">советовать-PRS.[3SG]</ta>
            <ta e="T54" id="Seg_3584" s="T53">вот</ta>
            <ta e="T55" id="Seg_3585" s="T54">теперь</ta>
            <ta e="T56" id="Seg_3586" s="T55">спастись-NEG-PST1-1PL</ta>
            <ta e="T57" id="Seg_3587" s="T56">1PL.[NOM]</ta>
            <ta e="T58" id="Seg_3588" s="T57">куда</ta>
            <ta e="T59" id="Seg_3589" s="T58">идти-CVB.SEQ</ta>
            <ta e="T60" id="Seg_3590" s="T59">умирать-FUT-1PL=Q</ta>
            <ta e="T61" id="Seg_3591" s="T60">1PL.[NOM]</ta>
            <ta e="T62" id="Seg_3592" s="T61">так</ta>
            <ta e="T63" id="Seg_3593" s="T62">думать-PRS-1SG</ta>
            <ta e="T64" id="Seg_3594" s="T63">какой</ta>
            <ta e="T65" id="Seg_3595" s="T64">INDEF</ta>
            <ta e="T66" id="Seg_3596" s="T65">время-DAT/LOC</ta>
            <ta e="T67" id="Seg_3597" s="T66">один</ta>
            <ta e="T68" id="Seg_3598" s="T67">место-ACC</ta>
            <ta e="T69" id="Seg_3599" s="T68">видеть-PTCP.PST-PROPR-1SG</ta>
            <ta e="T70" id="Seg_3600" s="T69">один</ta>
            <ta e="T71" id="Seg_3601" s="T70">большой</ta>
            <ta e="T72" id="Seg_3602" s="T71">река.[NOM]</ta>
            <ta e="T73" id="Seg_3603" s="T72">есть</ta>
            <ta e="T74" id="Seg_3604" s="T73">тот</ta>
            <ta e="T75" id="Seg_3605" s="T74">река-EP-2SG.[NOM]</ta>
            <ta e="T76" id="Seg_3606" s="T75">как-PROPR</ta>
            <ta e="T77" id="Seg_3607" s="T76">пурга-DAT/LOC</ta>
            <ta e="T78" id="Seg_3608" s="T77">замести-EP-PASS/REFL-PTCP.HAB-3SG</ta>
            <ta e="T79" id="Seg_3609" s="T78">NEG.[3SG]</ta>
            <ta e="T80" id="Seg_3610" s="T79">некоторый</ta>
            <ta e="T81" id="Seg_3611" s="T80">место-3SG.[NOM]</ta>
            <ta e="T82" id="Seg_3612" s="T81">зима-ACC</ta>
            <ta e="T83" id="Seg_3613" s="T82">в.течение</ta>
            <ta e="T84" id="Seg_3614" s="T83">полынья.[NOM]</ta>
            <ta e="T85" id="Seg_3615" s="T84">быть-PRS.[3SG]</ta>
            <ta e="T86" id="Seg_3616" s="T85">берег-3SG-ABL</ta>
            <ta e="T87" id="Seg_3617" s="T86">тальник-PROPR.[NOM]</ta>
            <ta e="T88" id="Seg_3618" s="T87">скала-3SG.[NOM]</ta>
            <ta e="T89" id="Seg_3619" s="T88">полностью</ta>
            <ta e="T90" id="Seg_3620" s="T89">багульник-PROPR.[NOM]</ta>
            <ta e="T91" id="Seg_3621" s="T90">ягода-PROPR.[NOM]</ta>
            <ta e="T92" id="Seg_3622" s="T91">трава-PROPR.[NOM]</ta>
            <ta e="T93" id="Seg_3623" s="T92">тот</ta>
            <ta e="T94" id="Seg_3624" s="T93">река-DAT/LOC</ta>
            <ta e="T95" id="Seg_3625" s="T94">щука.[NOM]</ta>
            <ta e="T96" id="Seg_3626" s="T95">собственный-3SG</ta>
            <ta e="T97" id="Seg_3627" s="T96">место.жизни-3SG.[NOM]</ta>
            <ta e="T98" id="Seg_3628" s="T97">только</ta>
            <ta e="T99" id="Seg_3629" s="T98">щука-PL.[NOM]</ta>
            <ta e="T100" id="Seg_3630" s="T99">только</ta>
            <ta e="T101" id="Seg_3631" s="T100">1PL-ACC</ta>
            <ta e="T102" id="Seg_3632" s="T101">попадать-EP-CAUS-FUT-3PL</ta>
            <ta e="T103" id="Seg_3633" s="T102">NEG-3SG</ta>
            <ta e="T104" id="Seg_3634" s="T103">давно</ta>
            <ta e="T105" id="Seg_3635" s="T104">время-DAT/LOC</ta>
            <ta e="T106" id="Seg_3636" s="T105">клятвенный</ta>
            <ta e="T107" id="Seg_3637" s="T106">слово-PROPR-3PL</ta>
            <ta e="T108" id="Seg_3638" s="T107">куропатка-ACC</ta>
            <ta e="T109" id="Seg_3639" s="T108">с</ta>
            <ta e="T110" id="Seg_3640" s="T109">куропатка.[NOM]</ta>
            <ta e="T111" id="Seg_3641" s="T110">щука.[NOM]</ta>
            <ta e="T112" id="Seg_3642" s="T111">страна-3SG-DAT/LOC</ta>
            <ta e="T113" id="Seg_3643" s="T112">идти-FUT.[3SG]</ta>
            <ta e="T114" id="Seg_3644" s="T113">NEG-3SG</ta>
            <ta e="T115" id="Seg_3645" s="T114">говорить-CVB.SEQ</ta>
            <ta e="T116" id="Seg_3646" s="T115">щука.[NOM]</ta>
            <ta e="T117" id="Seg_3647" s="T116">куропатка.[NOM]</ta>
            <ta e="T118" id="Seg_3648" s="T117">страна-3SG-DAT/LOC</ta>
            <ta e="T119" id="Seg_3649" s="T118">помнить-PTCP.PRS</ta>
            <ta e="T120" id="Seg_3650" s="T119">быть-TEMP-3PL</ta>
            <ta e="T121" id="Seg_3651" s="T120">добрый-INSTR</ta>
            <ta e="T122" id="Seg_3652" s="T121">давать-FUT-3PL</ta>
            <ta e="T123" id="Seg_3653" s="T122">NEG-3SG</ta>
            <ta e="T124" id="Seg_3654" s="T123">1PL.[NOM]</ta>
            <ta e="T125" id="Seg_3655" s="T124">тогда</ta>
            <ta e="T126" id="Seg_3656" s="T125">война-INSTR</ta>
            <ta e="T127" id="Seg_3657" s="T126">только</ta>
            <ta e="T128" id="Seg_3658" s="T127">входить-EP-MED-FUT-1PL</ta>
            <ta e="T129" id="Seg_3659" s="T128">куропатка-PL.[NOM]</ta>
            <ta e="T130" id="Seg_3660" s="T129">правда.[NOM]</ta>
            <ta e="T131" id="Seg_3661" s="T130">здесь</ta>
            <ta e="T132" id="Seg_3662" s="T131">жить-PTCP.PST-EP-INSTR</ta>
            <ta e="T133" id="Seg_3663" s="T132">умирать-FUT-1PL</ta>
            <ta e="T134" id="Seg_3664" s="T133">Q</ta>
            <ta e="T135" id="Seg_3665" s="T134">тот.[NOM]</ta>
            <ta e="T136" id="Seg_3666" s="T135">вместо</ta>
            <ta e="T137" id="Seg_3667" s="T136">воевать-FUT-1PL</ta>
            <ta e="T138" id="Seg_3668" s="T137">идти-IMP.1PL</ta>
            <ta e="T139" id="Seg_3669" s="T138">говорить-PST1-3PL</ta>
            <ta e="T140" id="Seg_3670" s="T139">куропатка-PL.[NOM]</ta>
            <ta e="T141" id="Seg_3671" s="T140">летать-PRS-3PL</ta>
            <ta e="T142" id="Seg_3672" s="T141">река-DAT/LOC</ta>
            <ta e="T143" id="Seg_3673" s="T142">доезжать-PRS-3PL</ta>
            <ta e="T144" id="Seg_3674" s="T143">пища.[NOM]</ta>
            <ta e="T145" id="Seg_3675" s="T144">распространяться-CVB.SIM</ta>
            <ta e="T146" id="Seg_3676" s="T145">лежать-PTCP.PRS</ta>
            <ta e="T147" id="Seg_3677" s="T146">место-3SG.[NOM]</ta>
            <ta e="T148" id="Seg_3678" s="T147">быть-PST2.[3SG]</ta>
            <ta e="T149" id="Seg_3679" s="T148">есть-CVB.SIM</ta>
            <ta e="T150" id="Seg_3680" s="T149">сесть-PST2-3PL</ta>
            <ta e="T151" id="Seg_3681" s="T150">полынья.[NOM]</ta>
            <ta e="T152" id="Seg_3682" s="T151">через</ta>
            <ta e="T153" id="Seg_3683" s="T152">щука.[NOM]</ta>
            <ta e="T154" id="Seg_3684" s="T153">руководитель-3SG-GEN</ta>
            <ta e="T155" id="Seg_3685" s="T154">голова-3SG.[NOM]</ta>
            <ta e="T156" id="Seg_3686" s="T155">высовываться-NMNZ.[NOM]</ta>
            <ta e="T157" id="Seg_3687" s="T156">делать-PST2.[3SG]</ta>
            <ta e="T158" id="Seg_3688" s="T157">эй=EMPH</ta>
            <ta e="T159" id="Seg_3689" s="T158">куропатка-PL.[NOM]</ta>
            <ta e="T160" id="Seg_3690" s="T159">почему</ta>
            <ta e="T161" id="Seg_3691" s="T160">место-DAT/LOC</ta>
            <ta e="T162" id="Seg_3692" s="T161">приходить-PST2-2PL=Q</ta>
            <ta e="T163" id="Seg_3693" s="T162">давно</ta>
            <ta e="T164" id="Seg_3694" s="T163">время-DAT/LOC</ta>
            <ta e="T165" id="Seg_3695" s="T164">клятвенный</ta>
            <ta e="T166" id="Seg_3696" s="T165">слово-PROPR.[NOM]</ta>
            <ta e="T167" id="Seg_3697" s="T166">быть-PST1-1PL</ta>
            <ta e="T168" id="Seg_3698" s="T167">2PL.[NOM]</ta>
            <ta e="T169" id="Seg_3699" s="T168">место-2PL-DAT/LOC</ta>
            <ta e="T170" id="Seg_3700" s="T169">1PL.[NOM]</ta>
            <ta e="T171" id="Seg_3701" s="T170">идти-NEG-1PL</ta>
            <ta e="T172" id="Seg_3702" s="T171">2PL.[NOM]</ta>
            <ta e="T173" id="Seg_3703" s="T172">1PL.[NOM]</ta>
            <ta e="T174" id="Seg_3704" s="T173">место-1PL-DAT/LOC</ta>
            <ta e="T175" id="Seg_3705" s="T174">почему</ta>
            <ta e="T176" id="Seg_3706" s="T175">приходить-PST1-2PL</ta>
            <ta e="T177" id="Seg_3707" s="T176">ждать-EP-NEG-IMP.2PL</ta>
            <ta e="T178" id="Seg_3708" s="T177">немедленно</ta>
            <ta e="T179" id="Seg_3709" s="T178">идти-EP-IMP.2PL</ta>
            <ta e="T180" id="Seg_3710" s="T179">отсюда</ta>
            <ta e="T181" id="Seg_3711" s="T180">куст.[NOM]</ta>
            <ta e="T182" id="Seg_3712" s="T181">собственный-3SG-GEN</ta>
            <ta e="T183" id="Seg_3713" s="T182">лист-3SG-ACC</ta>
            <ta e="T184" id="Seg_3714" s="T183">багульник.[NOM]</ta>
            <ta e="T185" id="Seg_3715" s="T184">собственный-3SG-GEN</ta>
            <ta e="T186" id="Seg_3716" s="T185">верхушка-3SG-ACC</ta>
            <ta e="T187" id="Seg_3717" s="T186">NEG</ta>
            <ta e="T188" id="Seg_3718" s="T187">есть-CAUS-FUT-1SG</ta>
            <ta e="T189" id="Seg_3719" s="T188">NEG-3SG</ta>
            <ta e="T190" id="Seg_3720" s="T189">трава-1SG-ACC</ta>
            <ta e="T191" id="Seg_3721" s="T190">NEG</ta>
            <ta e="T192" id="Seg_3722" s="T191">есть-CAUS-NEG-1SG</ta>
            <ta e="T193" id="Seg_3723" s="T192">галька-1SG-ACC</ta>
            <ta e="T194" id="Seg_3724" s="T193">NEG</ta>
            <ta e="T195" id="Seg_3725" s="T194">копать-CAUS-FUT-1SG</ta>
            <ta e="T196" id="Seg_3726" s="T195">NEG-3SG</ta>
            <ta e="T197" id="Seg_3727" s="T196">тот-DAT/LOC</ta>
            <ta e="T198" id="Seg_3728" s="T197">куропатка.[NOM]</ta>
            <ta e="T199" id="Seg_3729" s="T198">руководитель-3SG.[NOM]</ta>
            <ta e="T200" id="Seg_3730" s="T199">говорить-PRS.[3SG]</ta>
            <ta e="T201" id="Seg_3731" s="T200">1PL.[NOM]</ta>
            <ta e="T202" id="Seg_3732" s="T201">добрый-INSTR</ta>
            <ta e="T203" id="Seg_3733" s="T202">идти-NEG-1PL</ta>
            <ta e="T204" id="Seg_3734" s="T203">война-INSTR</ta>
            <ta e="T205" id="Seg_3735" s="T204">хоть</ta>
            <ta e="T206" id="Seg_3736" s="T205">этот</ta>
            <ta e="T207" id="Seg_3737" s="T206">место-ACC</ta>
            <ta e="T208" id="Seg_3738" s="T207">взять-FUT-1PL</ta>
            <ta e="T209" id="Seg_3739" s="T208">щука.[NOM]</ta>
            <ta e="T210" id="Seg_3740" s="T209">руководитель-3SG.[NOM]</ta>
            <ta e="T211" id="Seg_3741" s="T210">1SG.[NOM]</ta>
            <ta e="T212" id="Seg_3742" s="T211">добрый-INSTR</ta>
            <ta e="T213" id="Seg_3743" s="T212">давать-NEG-1SG</ta>
            <ta e="T214" id="Seg_3744" s="T213">щука-ACC</ta>
            <ta e="T215" id="Seg_3745" s="T214">совсем</ta>
            <ta e="T216" id="Seg_3746" s="T215">убить-TEMP-2PL</ta>
            <ta e="T217" id="Seg_3747" s="T216">взять-FUT-2PL</ta>
            <ta e="T218" id="Seg_3748" s="T217">воевать-FUT-1PL</ta>
            <ta e="T219" id="Seg_3749" s="T218">останавливаться-IMP.2PL</ta>
            <ta e="T220" id="Seg_3750" s="T219">1SG.[NOM]</ta>
            <ta e="T221" id="Seg_3751" s="T220">люди-1SG-ACC</ta>
            <ta e="T222" id="Seg_3752" s="T221">собирать-MED-FUT-1SG</ta>
            <ta e="T223" id="Seg_3753" s="T222">говорить-PRS.[3SG]</ta>
            <ta e="T224" id="Seg_3754" s="T223">щука.[NOM]</ta>
            <ta e="T225" id="Seg_3755" s="T224">руководитель-3SG.[NOM]</ta>
            <ta e="T226" id="Seg_3756" s="T225">вертеться-PRS.[3SG]</ta>
            <ta e="T227" id="Seg_3757" s="T226">да</ta>
            <ta e="T228" id="Seg_3758" s="T227">хвост-3SG-INSTR</ta>
            <ta e="T229" id="Seg_3759" s="T228">плюх</ta>
            <ta e="T230" id="Seg_3760" s="T229">делать-PST2.[3SG]</ta>
            <ta e="T231" id="Seg_3761" s="T230">идти-PST2.[3SG]</ta>
            <ta e="T232" id="Seg_3762" s="T231">люди.[NOM]</ta>
            <ta e="T233" id="Seg_3763" s="T232">собирать-MED-CVB.SIM</ta>
            <ta e="T234" id="Seg_3764" s="T233">щука.[NOM]</ta>
            <ta e="T235" id="Seg_3765" s="T234">косяк-3SG-ACC</ta>
            <ta e="T236" id="Seg_3766" s="T235">целый-3SG-ACC</ta>
            <ta e="T237" id="Seg_3767" s="T236">приносить-CVB.SEQ</ta>
            <ta e="T238" id="Seg_3768" s="T237">полынья-ACC</ta>
            <ta e="T239" id="Seg_3769" s="T238">черный-ADVZ</ta>
            <ta e="T240" id="Seg_3770" s="T239">делать-PST1-3SG</ta>
            <ta e="T241" id="Seg_3771" s="T240">куропатка-PL.[NOM]</ta>
            <ta e="T242" id="Seg_3772" s="T241">готов</ta>
            <ta e="T243" id="Seg_3773" s="T242">берег-DAT/LOC</ta>
            <ta e="T244" id="Seg_3774" s="T243">стоять-PRS-3PL</ta>
            <ta e="T245" id="Seg_3775" s="T244">куропатка.[NOM]</ta>
            <ta e="T246" id="Seg_3776" s="T245">руководитель-3SG.[NOM]</ta>
            <ta e="T247" id="Seg_3777" s="T246">люди-3SG-DAT/LOC</ta>
            <ta e="T248" id="Seg_3778" s="T247">говорить-PRS.[3SG]</ta>
            <ta e="T249" id="Seg_3779" s="T248">щука-ACC</ta>
            <ta e="T250" id="Seg_3780" s="T249">голова-DAT/LOC</ta>
            <ta e="T251" id="Seg_3781" s="T250">стрелять-FUT-EP-IMP.2PL</ta>
            <ta e="T252" id="Seg_3782" s="T251">спина.рыбы-3SG-DAT/LOC</ta>
            <ta e="T253" id="Seg_3783" s="T252">умирать-PTCP.HAB-3SG</ta>
            <ta e="T254" id="Seg_3784" s="T253">NEG.[3SG]</ta>
            <ta e="T255" id="Seg_3785" s="T254">куропатка.[NOM]</ta>
            <ta e="T256" id="Seg_3786" s="T255">войско-3SG.[NOM]</ta>
            <ta e="T257" id="Seg_3787" s="T256">стрела-PL-3SG-ACC</ta>
            <ta e="T258" id="Seg_3788" s="T257">пустить-PST2.[3SG]</ta>
            <ta e="T259" id="Seg_3789" s="T258">лук.[NOM]</ta>
            <ta e="T260" id="Seg_3790" s="T259">тетива-EP-3SG-GEN</ta>
            <ta e="T261" id="Seg_3791" s="T260">звук-3SG-ACC</ta>
            <ta e="T262" id="Seg_3792" s="T261">слышаться-CVB.ANT</ta>
            <ta e="T263" id="Seg_3793" s="T262">щука-PL.[NOM]</ta>
            <ta e="T264" id="Seg_3794" s="T263">середина.[NOM]</ta>
            <ta e="T265" id="Seg_3795" s="T264">в.сторону</ta>
            <ta e="T266" id="Seg_3796" s="T265">вертеться-CVB.SIM</ta>
            <ta e="T267" id="Seg_3797" s="T266">давать-CVB.SEQ</ta>
            <ta e="T268" id="Seg_3798" s="T267">вертеться-PST2-3PL</ta>
            <ta e="T269" id="Seg_3799" s="T268">куропатка-PL.[NOM]</ta>
            <ta e="T270" id="Seg_3800" s="T269">стрела-3PL.[NOM]</ta>
            <ta e="T271" id="Seg_3801" s="T270">щука.[NOM]</ta>
            <ta e="T272" id="Seg_3802" s="T271">спина.рыбы-3SG-DAT/LOC</ta>
            <ta e="T273" id="Seg_3803" s="T272">NEG</ta>
            <ta e="T274" id="Seg_3804" s="T273">спина.рыбы-3SG-DAT/LOC</ta>
            <ta e="T275" id="Seg_3805" s="T274">давать-CVB.SEQ</ta>
            <ta e="T276" id="Seg_3806" s="T275">идти-PST2.[3SG]</ta>
            <ta e="T277" id="Seg_3807" s="T276">щука-PL.[NOM]</ta>
            <ta e="T278" id="Seg_3808" s="T277">вертеться-CVB.SEQ</ta>
            <ta e="T279" id="Seg_3809" s="T278">приходить-PST2-3PL</ta>
            <ta e="T280" id="Seg_3810" s="T279">куропатка-PL.[NOM]</ta>
            <ta e="T281" id="Seg_3811" s="T280">стрела.[NOM]</ta>
            <ta e="T282" id="Seg_3812" s="T281">доставать-PTCP.FUT-3PL-DAT/LOC</ta>
            <ta e="T283" id="Seg_3813" s="T282">пока</ta>
            <ta e="T284" id="Seg_3814" s="T283">щука-PL.[NOM]</ta>
            <ta e="T285" id="Seg_3815" s="T284">стрелять-PST2-3PL</ta>
            <ta e="T286" id="Seg_3816" s="T285">куропатка-PL.[NOM]</ta>
            <ta e="T287" id="Seg_3817" s="T286">сердце-3PL-ACC</ta>
            <ta e="T288" id="Seg_3818" s="T287">целить-CVB.SEQ</ta>
            <ta e="T289" id="Seg_3819" s="T288">куропатка-PL.[NOM]</ta>
            <ta e="T290" id="Seg_3820" s="T289">вертеться-CVB.SEQ</ta>
            <ta e="T291" id="Seg_3821" s="T290">летать-CVB.SIM</ta>
            <ta e="T292" id="Seg_3822" s="T291">делать-CVB.SIM</ta>
            <ta e="T293" id="Seg_3823" s="T292">середина-3SG.[NOM]</ta>
            <ta e="T294" id="Seg_3824" s="T293">в.сторону</ta>
            <ta e="T295" id="Seg_3825" s="T294">прыгать-CVB.SEQ</ta>
            <ta e="T296" id="Seg_3826" s="T295">давать-PST2-3PL</ta>
            <ta e="T297" id="Seg_3827" s="T296">щука-PL.[NOM]</ta>
            <ta e="T346" id="Seg_3828" s="T297">стрела-3PL.[NOM]</ta>
            <ta e="T298" id="Seg_3829" s="T346">куропатка-PL.[NOM]</ta>
            <ta e="T299" id="Seg_3830" s="T298">собственный-3PL-GEN</ta>
            <ta e="T300" id="Seg_3831" s="T299">мышца-DAT/LOC</ta>
            <ta e="T301" id="Seg_3832" s="T300">NEG</ta>
            <ta e="T302" id="Seg_3833" s="T301">мышца-DAT/LOC</ta>
            <ta e="T303" id="Seg_3834" s="T302">нога-3PL-DAT/LOC</ta>
            <ta e="T304" id="Seg_3835" s="T303">так</ta>
            <ta e="T305" id="Seg_3836" s="T304">воевать-PST2-3PL</ta>
            <ta e="T306" id="Seg_3837" s="T305">какой-3PL.[NOM]</ta>
            <ta e="T307" id="Seg_3838" s="T306">NEG</ta>
            <ta e="T308" id="Seg_3839" s="T307">убить-RECP/COLL-EP-NEG.[3SG]</ta>
            <ta e="T309" id="Seg_3840" s="T308">побеждать-RECP/COLL-EP-NEG.[3SG]</ta>
            <ta e="T310" id="Seg_3841" s="T309">стрела-PL-3SG.[NOM]</ta>
            <ta e="T311" id="Seg_3842" s="T310">полностью</ta>
            <ta e="T312" id="Seg_3843" s="T311">кончаться-CVB.SEQ</ta>
            <ta e="T313" id="Seg_3844" s="T312">воевать-CVB.SEQ</ta>
            <ta e="T314" id="Seg_3845" s="T313">кончать-PST2-3PL</ta>
            <ta e="T315" id="Seg_3846" s="T314">щука.[NOM]</ta>
            <ta e="T316" id="Seg_3847" s="T315">руководитель-3SG.[NOM]</ta>
            <ta e="T317" id="Seg_3848" s="T316">теперь</ta>
            <ta e="T318" id="Seg_3849" s="T317">воевать-CVB.SEQ</ta>
            <ta e="T319" id="Seg_3850" s="T318">кончать-IMP.1PL</ta>
            <ta e="T320" id="Seg_3851" s="T319">2PL.[NOM]</ta>
            <ta e="T321" id="Seg_3852" s="T320">куда</ta>
            <ta e="T322" id="Seg_3853" s="T321">хотеть-PTCP.PRS</ta>
            <ta e="T323" id="Seg_3854" s="T322">летать-CVB.SIM</ta>
            <ta e="T324" id="Seg_3855" s="T323">идти-EP-IMP.2PL</ta>
            <ta e="T325" id="Seg_3856" s="T324">где</ta>
            <ta e="T326" id="Seg_3857" s="T325">хотеть-PTCP.PRS</ta>
            <ta e="T327" id="Seg_3858" s="T326">есть-IMP.2PL</ta>
            <ta e="T328" id="Seg_3859" s="T327">говорить-PRS.[3SG]</ta>
            <ta e="T329" id="Seg_3860" s="T328">с.тех.пор</ta>
            <ta e="T330" id="Seg_3861" s="T329">куропатка-PL.[NOM]</ta>
            <ta e="T331" id="Seg_3862" s="T330">стрела-3PL.[NOM]</ta>
            <ta e="T332" id="Seg_3863" s="T331">щука.[NOM]</ta>
            <ta e="T333" id="Seg_3864" s="T332">спина.рыбы-3SG-DAT/LOC</ta>
            <ta e="T334" id="Seg_3865" s="T333">вилообразный</ta>
            <ta e="T335" id="Seg_3866" s="T334">кость.[NOM]</ta>
            <ta e="T336" id="Seg_3867" s="T335">становиться-PST2-3PL</ta>
            <ta e="T337" id="Seg_3868" s="T336">щука-PL.[NOM]</ta>
            <ta e="T338" id="Seg_3869" s="T337">стрела-3PL.[NOM]</ta>
            <ta e="T339" id="Seg_3870" s="T338">куропатка.[NOM]</ta>
            <ta e="T340" id="Seg_3871" s="T339">нога-3SG-GEN</ta>
            <ta e="T341" id="Seg_3872" s="T340">мышца-3SG-DAT/LOC</ta>
            <ta e="T342" id="Seg_3873" s="T341">сухожилие.[NOM]</ta>
            <ta e="T343" id="Seg_3874" s="T342">кость.[NOM]</ta>
            <ta e="T344" id="Seg_3875" s="T343">становиться-PST2-3PL</ta>
            <ta e="T345" id="Seg_3876" s="T344">последний-3SG.[NOM]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_3877" s="T0">ptcl</ta>
            <ta e="T2" id="Seg_3878" s="T1">adv</ta>
            <ta e="T3" id="Seg_3879" s="T2">n-n:case-n-n:case</ta>
            <ta e="T4" id="Seg_3880" s="T3">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T5" id="Seg_3881" s="T4">n-n:case</ta>
            <ta e="T6" id="Seg_3882" s="T5">n-n:case</ta>
            <ta e="T7" id="Seg_3883" s="T6">n-n&gt;adj-n:case</ta>
            <ta e="T8" id="Seg_3884" s="T7">ptcl</ta>
            <ta e="T9" id="Seg_3885" s="T8">cardnum</ta>
            <ta e="T10" id="Seg_3886" s="T9">n-n:case</ta>
            <ta e="T11" id="Seg_3887" s="T10">n-n&gt;adj-n:case</ta>
            <ta e="T12" id="Seg_3888" s="T11">v-v:tense-v:pred.pn</ta>
            <ta e="T13" id="Seg_3889" s="T12">adv</ta>
            <ta e="T14" id="Seg_3890" s="T13">v-v:tense-v:pred.pn</ta>
            <ta e="T15" id="Seg_3891" s="T14">n-n:(poss)-n:case</ta>
            <ta e="T16" id="Seg_3892" s="T15">adj-n:(poss)-n:case</ta>
            <ta e="T17" id="Seg_3893" s="T16">n-n:case</ta>
            <ta e="T18" id="Seg_3894" s="T17">v-v:tense-v:pred.pn</ta>
            <ta e="T19" id="Seg_3895" s="T18">cardnum</ta>
            <ta e="T20" id="Seg_3896" s="T19">ptcl</ta>
            <ta e="T21" id="Seg_3897" s="T20">n-n:case</ta>
            <ta e="T22" id="Seg_3898" s="T21">n-n:(poss)-n:case</ta>
            <ta e="T23" id="Seg_3899" s="T22">ptcl</ta>
            <ta e="T24" id="Seg_3900" s="T23">n-n:(num)-n:case</ta>
            <ta e="T25" id="Seg_3901" s="T24">v-v:tense-v:pred.pn</ta>
            <ta e="T26" id="Seg_3902" s="T25">pers-pro:case</ta>
            <ta e="T27" id="Seg_3903" s="T26">n-n:case</ta>
            <ta e="T28" id="Seg_3904" s="T27">v-v:cvb</ta>
            <ta e="T29" id="Seg_3905" s="T28">n-n:case</ta>
            <ta e="T30" id="Seg_3906" s="T29">post</ta>
            <ta e="T31" id="Seg_3907" s="T30">v-v:tense-v:pred.pn</ta>
            <ta e="T32" id="Seg_3908" s="T31">cardnum</ta>
            <ta e="T33" id="Seg_3909" s="T32">ptcl</ta>
            <ta e="T34" id="Seg_3910" s="T33">adj</ta>
            <ta e="T35" id="Seg_3911" s="T34">n-n:case</ta>
            <ta e="T36" id="Seg_3912" s="T35">n-n&gt;adj-n:(pred.pn)</ta>
            <ta e="T37" id="Seg_3913" s="T36">n-n:case</ta>
            <ta e="T38" id="Seg_3914" s="T37">v-v:tense-v:pred.pn</ta>
            <ta e="T39" id="Seg_3915" s="T38">que</ta>
            <ta e="T40" id="Seg_3916" s="T39">ptcl</ta>
            <ta e="T41" id="Seg_3917" s="T40">n-n:case</ta>
            <ta e="T42" id="Seg_3918" s="T41">v-v:(ins)-v:(neg)-v:pred.pn</ta>
            <ta e="T43" id="Seg_3919" s="T42">n-n:case</ta>
            <ta e="T44" id="Seg_3920" s="T43">n-n:(poss)-n:case</ta>
            <ta e="T45" id="Seg_3921" s="T44">ptcl</ta>
            <ta e="T46" id="Seg_3922" s="T45">v-v&gt;v-v:(ins)-v:(neg)-v:pred.pn</ta>
            <ta e="T47" id="Seg_3923" s="T46">v-v:cvb</ta>
            <ta e="T48" id="Seg_3924" s="T47">v-v:tense-v:pred.pn</ta>
            <ta e="T49" id="Seg_3925" s="T48">n-n:case</ta>
            <ta e="T50" id="Seg_3926" s="T49">ptcl</ta>
            <ta e="T51" id="Seg_3927" s="T50">v-v:tense-v:pred.pn</ta>
            <ta e="T52" id="Seg_3928" s="T51">n-n:(poss)-n:case</ta>
            <ta e="T53" id="Seg_3929" s="T52">v-v:tense-v:pred.pn</ta>
            <ta e="T54" id="Seg_3930" s="T53">ptcl</ta>
            <ta e="T55" id="Seg_3931" s="T54">adv</ta>
            <ta e="T56" id="Seg_3932" s="T55">v-v:(neg)-v:tense-v:poss.pn</ta>
            <ta e="T57" id="Seg_3933" s="T56">pers-pro:case</ta>
            <ta e="T58" id="Seg_3934" s="T57">que</ta>
            <ta e="T59" id="Seg_3935" s="T58">v-v:cvb</ta>
            <ta e="T60" id="Seg_3936" s="T59">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T61" id="Seg_3937" s="T60">pers-pro:case</ta>
            <ta e="T62" id="Seg_3938" s="T61">adv</ta>
            <ta e="T63" id="Seg_3939" s="T62">v-v:tense-v:pred.pn</ta>
            <ta e="T64" id="Seg_3940" s="T63">que</ta>
            <ta e="T65" id="Seg_3941" s="T64">ptcl</ta>
            <ta e="T66" id="Seg_3942" s="T65">n-n:case</ta>
            <ta e="T67" id="Seg_3943" s="T66">cardnum</ta>
            <ta e="T68" id="Seg_3944" s="T67">n-n:case</ta>
            <ta e="T69" id="Seg_3945" s="T68">v-v:ptcp-v&gt;adj-n:(pred.pn)</ta>
            <ta e="T70" id="Seg_3946" s="T69">cardnum</ta>
            <ta e="T71" id="Seg_3947" s="T70">adj</ta>
            <ta e="T72" id="Seg_3948" s="T71">n-n:case</ta>
            <ta e="T73" id="Seg_3949" s="T72">ptcl</ta>
            <ta e="T74" id="Seg_3950" s="T73">dempro</ta>
            <ta e="T75" id="Seg_3951" s="T74">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T76" id="Seg_3952" s="T75">que-que&gt;adj</ta>
            <ta e="T77" id="Seg_3953" s="T76">n-n:case</ta>
            <ta e="T78" id="Seg_3954" s="T77">v-v:(ins)-v&gt;v-v:ptcp-v:(poss)</ta>
            <ta e="T79" id="Seg_3955" s="T78">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T80" id="Seg_3956" s="T79">indfpro</ta>
            <ta e="T81" id="Seg_3957" s="T80">n-n:(poss)-n:case</ta>
            <ta e="T82" id="Seg_3958" s="T81">n-n:case</ta>
            <ta e="T83" id="Seg_3959" s="T82">post</ta>
            <ta e="T84" id="Seg_3960" s="T83">n-n:case</ta>
            <ta e="T85" id="Seg_3961" s="T84">v-v:tense-v:pred.pn</ta>
            <ta e="T86" id="Seg_3962" s="T85">n-n:poss-n:case</ta>
            <ta e="T87" id="Seg_3963" s="T86">n-n&gt;adj-n:case</ta>
            <ta e="T88" id="Seg_3964" s="T87">n-n:(poss)-n:case</ta>
            <ta e="T89" id="Seg_3965" s="T88">adv</ta>
            <ta e="T90" id="Seg_3966" s="T89">n-n&gt;adj-n:case</ta>
            <ta e="T91" id="Seg_3967" s="T90">n-n&gt;adj-n:case</ta>
            <ta e="T92" id="Seg_3968" s="T91">n-n&gt;adj-n:case</ta>
            <ta e="T93" id="Seg_3969" s="T92">dempro</ta>
            <ta e="T94" id="Seg_3970" s="T93">n-n:case</ta>
            <ta e="T95" id="Seg_3971" s="T94">n-n:case</ta>
            <ta e="T96" id="Seg_3972" s="T95">adj-n:(poss)</ta>
            <ta e="T97" id="Seg_3973" s="T96">n-n:(poss)-n:case</ta>
            <ta e="T98" id="Seg_3974" s="T97">adv</ta>
            <ta e="T99" id="Seg_3975" s="T98">n-n:(num)-n:case</ta>
            <ta e="T100" id="Seg_3976" s="T99">ptcl</ta>
            <ta e="T101" id="Seg_3977" s="T100">pers-pro:case</ta>
            <ta e="T102" id="Seg_3978" s="T101">v-v:(ins)-v&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T103" id="Seg_3979" s="T102">ptcl-ptcl:(poss.pn)</ta>
            <ta e="T104" id="Seg_3980" s="T103">adv</ta>
            <ta e="T105" id="Seg_3981" s="T104">n-n:case</ta>
            <ta e="T106" id="Seg_3982" s="T105">adj</ta>
            <ta e="T107" id="Seg_3983" s="T106">n-n&gt;adj-n:(pred.pn)</ta>
            <ta e="T108" id="Seg_3984" s="T107">n-n:case</ta>
            <ta e="T109" id="Seg_3985" s="T108">post</ta>
            <ta e="T110" id="Seg_3986" s="T109">n-n:case</ta>
            <ta e="T111" id="Seg_3987" s="T110">n-n:case</ta>
            <ta e="T112" id="Seg_3988" s="T111">n-n:poss-n:case</ta>
            <ta e="T113" id="Seg_3989" s="T112">v-v:tense-v:poss.pn</ta>
            <ta e="T114" id="Seg_3990" s="T113">ptcl-ptcl:(poss.pn)</ta>
            <ta e="T115" id="Seg_3991" s="T114">v-v:cvb</ta>
            <ta e="T116" id="Seg_3992" s="T115">n-n:case</ta>
            <ta e="T117" id="Seg_3993" s="T116">n-n:case</ta>
            <ta e="T118" id="Seg_3994" s="T117">n-n:poss-n:case</ta>
            <ta e="T119" id="Seg_3995" s="T118">v-v:ptcp</ta>
            <ta e="T120" id="Seg_3996" s="T119">v-v:mood-v:temp.pn</ta>
            <ta e="T121" id="Seg_3997" s="T120">adj-n:case</ta>
            <ta e="T122" id="Seg_3998" s="T121">v-v:tense-v:poss.pn</ta>
            <ta e="T123" id="Seg_3999" s="T122">ptcl-ptcl:(poss.pn)</ta>
            <ta e="T124" id="Seg_4000" s="T123">pers-pro:case</ta>
            <ta e="T125" id="Seg_4001" s="T124">adv</ta>
            <ta e="T126" id="Seg_4002" s="T125">n-n:case</ta>
            <ta e="T127" id="Seg_4003" s="T126">ptcl</ta>
            <ta e="T128" id="Seg_4004" s="T127">v-v:(ins)-v&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T129" id="Seg_4005" s="T128">n-n:(num)-n:case</ta>
            <ta e="T130" id="Seg_4006" s="T129">n-n:case</ta>
            <ta e="T131" id="Seg_4007" s="T130">adv</ta>
            <ta e="T132" id="Seg_4008" s="T131">v-v:ptcp-v:(ins)-v:(case)</ta>
            <ta e="T133" id="Seg_4009" s="T132">v-v:tense-v:poss.pn</ta>
            <ta e="T134" id="Seg_4010" s="T133">ptcl</ta>
            <ta e="T135" id="Seg_4011" s="T134">dempro-pro:case</ta>
            <ta e="T136" id="Seg_4012" s="T135">post</ta>
            <ta e="T137" id="Seg_4013" s="T136">v-v:tense-v:poss.pn</ta>
            <ta e="T138" id="Seg_4014" s="T137">v-v:mood.pn</ta>
            <ta e="T139" id="Seg_4015" s="T138">v-v:tense-v:pred.pn</ta>
            <ta e="T140" id="Seg_4016" s="T139">n-n:(num)-n:case</ta>
            <ta e="T141" id="Seg_4017" s="T140">v-v:tense-v:pred.pn</ta>
            <ta e="T142" id="Seg_4018" s="T141">n-n:case</ta>
            <ta e="T143" id="Seg_4019" s="T142">v-v:tense-v:pred.pn</ta>
            <ta e="T144" id="Seg_4020" s="T143">n-n:case</ta>
            <ta e="T145" id="Seg_4021" s="T144">v-v:cvb</ta>
            <ta e="T146" id="Seg_4022" s="T145">v-v:ptcp</ta>
            <ta e="T147" id="Seg_4023" s="T146">n-n:(poss)-n:case</ta>
            <ta e="T148" id="Seg_4024" s="T147">v-v:tense-v:pred.pn</ta>
            <ta e="T149" id="Seg_4025" s="T148">v-v:cvb</ta>
            <ta e="T150" id="Seg_4026" s="T149">v-v:tense-v:pred.pn</ta>
            <ta e="T151" id="Seg_4027" s="T150">n-n:case</ta>
            <ta e="T152" id="Seg_4028" s="T151">post</ta>
            <ta e="T153" id="Seg_4029" s="T152">n-n:case</ta>
            <ta e="T154" id="Seg_4030" s="T153">n-n:poss-n:case</ta>
            <ta e="T155" id="Seg_4031" s="T154">n-n:(poss)-n:case</ta>
            <ta e="T156" id="Seg_4032" s="T155">v-v&gt;n-n:case</ta>
            <ta e="T157" id="Seg_4033" s="T156">v-v:tense-v:pred.pn</ta>
            <ta e="T158" id="Seg_4034" s="T157">interj-ptcl</ta>
            <ta e="T159" id="Seg_4035" s="T158">n-n:(num)-n:case</ta>
            <ta e="T160" id="Seg_4036" s="T159">que</ta>
            <ta e="T161" id="Seg_4037" s="T160">n-n:case</ta>
            <ta e="T162" id="Seg_4038" s="T161">v-v:tense-v:pred.pn-ptcl</ta>
            <ta e="T163" id="Seg_4039" s="T162">adv</ta>
            <ta e="T164" id="Seg_4040" s="T163">n-n:case</ta>
            <ta e="T165" id="Seg_4041" s="T164">adj</ta>
            <ta e="T166" id="Seg_4042" s="T165">n-n&gt;adj-n:case</ta>
            <ta e="T167" id="Seg_4043" s="T166">v-v:tense-v:poss.pn</ta>
            <ta e="T168" id="Seg_4044" s="T167">pers-pro:case</ta>
            <ta e="T169" id="Seg_4045" s="T168">n-n:poss-n:case</ta>
            <ta e="T170" id="Seg_4046" s="T169">pers-pro:case</ta>
            <ta e="T171" id="Seg_4047" s="T170">v-v:(neg)-v:pred.pn</ta>
            <ta e="T172" id="Seg_4048" s="T171">pers-pro:case</ta>
            <ta e="T173" id="Seg_4049" s="T172">pers-pro:case</ta>
            <ta e="T174" id="Seg_4050" s="T173">n-n:poss-n:case</ta>
            <ta e="T175" id="Seg_4051" s="T174">que</ta>
            <ta e="T176" id="Seg_4052" s="T175">v-v:tense-v:poss.pn</ta>
            <ta e="T177" id="Seg_4053" s="T176">v-v:(ins)-v:(neg)-v:mood.pn</ta>
            <ta e="T178" id="Seg_4054" s="T177">adv</ta>
            <ta e="T179" id="Seg_4055" s="T178">v-v:(ins)-v:mood.pn</ta>
            <ta e="T180" id="Seg_4056" s="T179">adv</ta>
            <ta e="T181" id="Seg_4057" s="T180">n-n:case</ta>
            <ta e="T182" id="Seg_4058" s="T181">adj-n:poss-n:case</ta>
            <ta e="T183" id="Seg_4059" s="T182">n-n:(poss)-n:case</ta>
            <ta e="T184" id="Seg_4060" s="T183">n-n:case</ta>
            <ta e="T185" id="Seg_4061" s="T184">adj-n:poss-n:case</ta>
            <ta e="T186" id="Seg_4062" s="T185">n-n:poss-n:case</ta>
            <ta e="T187" id="Seg_4063" s="T186">ptcl</ta>
            <ta e="T188" id="Seg_4064" s="T187">v-v&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T189" id="Seg_4065" s="T188">ptcl-ptcl:(poss.pn)</ta>
            <ta e="T190" id="Seg_4066" s="T189">n-n:poss-n:case</ta>
            <ta e="T191" id="Seg_4067" s="T190">ptcl</ta>
            <ta e="T192" id="Seg_4068" s="T191">v-v&gt;v-v:(neg)-v:pred.pn</ta>
            <ta e="T193" id="Seg_4069" s="T192">n-n:poss-n:case</ta>
            <ta e="T194" id="Seg_4070" s="T193">ptcl</ta>
            <ta e="T195" id="Seg_4071" s="T194">v-v&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T196" id="Seg_4072" s="T195">ptcl-ptcl:(poss.pn)</ta>
            <ta e="T197" id="Seg_4073" s="T196">dempro-pro:case</ta>
            <ta e="T198" id="Seg_4074" s="T197">n-n:case</ta>
            <ta e="T199" id="Seg_4075" s="T198">n-n:(poss)-n:case</ta>
            <ta e="T200" id="Seg_4076" s="T199">v-v:tense-v:pred.pn</ta>
            <ta e="T201" id="Seg_4077" s="T200">pers-pro:case</ta>
            <ta e="T202" id="Seg_4078" s="T201">adj-n:case</ta>
            <ta e="T203" id="Seg_4079" s="T202">v-v:(neg)-v:pred.pn</ta>
            <ta e="T204" id="Seg_4080" s="T203">n-n:case</ta>
            <ta e="T205" id="Seg_4081" s="T204">ptcl</ta>
            <ta e="T206" id="Seg_4082" s="T205">dempro</ta>
            <ta e="T207" id="Seg_4083" s="T206">n-n:case</ta>
            <ta e="T208" id="Seg_4084" s="T207">v-v:tense-v:poss.pn</ta>
            <ta e="T209" id="Seg_4085" s="T208">n-n:case</ta>
            <ta e="T210" id="Seg_4086" s="T209">n-n:(poss)-n:case</ta>
            <ta e="T211" id="Seg_4087" s="T210">pers-pro:case</ta>
            <ta e="T212" id="Seg_4088" s="T211">adj-n:case</ta>
            <ta e="T213" id="Seg_4089" s="T212">v-v:(neg)-v:pred.pn</ta>
            <ta e="T214" id="Seg_4090" s="T213">n-n:case</ta>
            <ta e="T215" id="Seg_4091" s="T214">adv</ta>
            <ta e="T216" id="Seg_4092" s="T215">v-v:mood-v:temp.pn</ta>
            <ta e="T217" id="Seg_4093" s="T216">v-v:tense-v:poss.pn</ta>
            <ta e="T218" id="Seg_4094" s="T217">v-v:tense-v:poss.pn</ta>
            <ta e="T219" id="Seg_4095" s="T218">v-v:mood.pn</ta>
            <ta e="T220" id="Seg_4096" s="T219">pers-pro:case</ta>
            <ta e="T221" id="Seg_4097" s="T220">n-n:poss-n:case</ta>
            <ta e="T222" id="Seg_4098" s="T221">v-v&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T223" id="Seg_4099" s="T222">v-v:tense-v:pred.pn</ta>
            <ta e="T224" id="Seg_4100" s="T223">n-n:case</ta>
            <ta e="T225" id="Seg_4101" s="T224">n-n:(poss)-n:case</ta>
            <ta e="T226" id="Seg_4102" s="T225">v-v:tense-v:pred.pn</ta>
            <ta e="T227" id="Seg_4103" s="T226">conj</ta>
            <ta e="T228" id="Seg_4104" s="T227">n-n:poss-n:case</ta>
            <ta e="T229" id="Seg_4105" s="T228">interj</ta>
            <ta e="T230" id="Seg_4106" s="T229">v-v:tense-v:pred.pn</ta>
            <ta e="T231" id="Seg_4107" s="T230">v-v:tense-v:pred.pn</ta>
            <ta e="T232" id="Seg_4108" s="T231">n-n:case</ta>
            <ta e="T233" id="Seg_4109" s="T232">v-v&gt;v-v:cvb</ta>
            <ta e="T234" id="Seg_4110" s="T233">n-n:case</ta>
            <ta e="T235" id="Seg_4111" s="T234">n-n:poss-n:case</ta>
            <ta e="T236" id="Seg_4112" s="T235">adj-n:poss-n:case</ta>
            <ta e="T237" id="Seg_4113" s="T236">v-v:cvb</ta>
            <ta e="T238" id="Seg_4114" s="T237">n-n:case</ta>
            <ta e="T239" id="Seg_4115" s="T238">adj-adj&gt;adv</ta>
            <ta e="T240" id="Seg_4116" s="T239">v-v:tense-v:poss.pn</ta>
            <ta e="T241" id="Seg_4117" s="T240">n-n:(num)-n:case</ta>
            <ta e="T242" id="Seg_4118" s="T241">adv</ta>
            <ta e="T243" id="Seg_4119" s="T242">n-n:case</ta>
            <ta e="T244" id="Seg_4120" s="T243">v-v:tense-v:pred.pn</ta>
            <ta e="T245" id="Seg_4121" s="T244">n-n:case</ta>
            <ta e="T246" id="Seg_4122" s="T245">n-n:(poss)-n:case</ta>
            <ta e="T247" id="Seg_4123" s="T246">n-n:poss-n:case</ta>
            <ta e="T248" id="Seg_4124" s="T247">v-v:tense-v:pred.pn</ta>
            <ta e="T249" id="Seg_4125" s="T248">n-n:case</ta>
            <ta e="T250" id="Seg_4126" s="T249">n-n:case</ta>
            <ta e="T251" id="Seg_4127" s="T250">v-v:(tense)-v:(ins)-v:mood.pn</ta>
            <ta e="T252" id="Seg_4128" s="T251">n-n:poss-n:case</ta>
            <ta e="T253" id="Seg_4129" s="T252">v-v:ptcp-v:(poss)</ta>
            <ta e="T254" id="Seg_4130" s="T253">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T255" id="Seg_4131" s="T254">n-n:case</ta>
            <ta e="T256" id="Seg_4132" s="T255">n-n:(poss)-n:case</ta>
            <ta e="T257" id="Seg_4133" s="T256">n-n:(num)-n:poss-n:case</ta>
            <ta e="T258" id="Seg_4134" s="T257">v-v:tense-v:pred.pn</ta>
            <ta e="T259" id="Seg_4135" s="T258">n-n:case</ta>
            <ta e="T260" id="Seg_4136" s="T259">n-n:(ins)-n:poss-n:case</ta>
            <ta e="T261" id="Seg_4137" s="T260">n-n:poss-n:case</ta>
            <ta e="T262" id="Seg_4138" s="T261">v-v:cvb</ta>
            <ta e="T263" id="Seg_4139" s="T262">n-n:(num)-n:case</ta>
            <ta e="T264" id="Seg_4140" s="T263">n-n:case</ta>
            <ta e="T265" id="Seg_4141" s="T264">post</ta>
            <ta e="T266" id="Seg_4142" s="T265">v-v:cvb</ta>
            <ta e="T267" id="Seg_4143" s="T266">v-v:cvb</ta>
            <ta e="T268" id="Seg_4144" s="T267">v-v:tense-v:pred.pn</ta>
            <ta e="T269" id="Seg_4145" s="T268">n-n:(num)-n:case</ta>
            <ta e="T270" id="Seg_4146" s="T269">n-n:(poss)-n:case</ta>
            <ta e="T271" id="Seg_4147" s="T270">n-n:case</ta>
            <ta e="T272" id="Seg_4148" s="T271">n-n:poss-n:case</ta>
            <ta e="T273" id="Seg_4149" s="T272">ptcl</ta>
            <ta e="T274" id="Seg_4150" s="T273">n-n:poss-n:case</ta>
            <ta e="T275" id="Seg_4151" s="T274">v-v:cvb</ta>
            <ta e="T276" id="Seg_4152" s="T275">v-v:tense-v:pred.pn</ta>
            <ta e="T277" id="Seg_4153" s="T276">n-n:(num)-n:case</ta>
            <ta e="T278" id="Seg_4154" s="T277">v-v:cvb</ta>
            <ta e="T279" id="Seg_4155" s="T278">v-v:tense-v:pred.pn</ta>
            <ta e="T280" id="Seg_4156" s="T279">n-n:(num)-n:case</ta>
            <ta e="T281" id="Seg_4157" s="T280">n-n:case</ta>
            <ta e="T282" id="Seg_4158" s="T281">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T283" id="Seg_4159" s="T282">post</ta>
            <ta e="T284" id="Seg_4160" s="T283">n-n:(num)-n:case</ta>
            <ta e="T285" id="Seg_4161" s="T284">v-v:tense-v:pred.pn</ta>
            <ta e="T286" id="Seg_4162" s="T285">n-n:(num)-n:case</ta>
            <ta e="T287" id="Seg_4163" s="T286">n-n:poss-n:case</ta>
            <ta e="T288" id="Seg_4164" s="T287">v-v:cvb</ta>
            <ta e="T289" id="Seg_4165" s="T288">n-n:(num)-n:case</ta>
            <ta e="T290" id="Seg_4166" s="T289">v-v:cvb</ta>
            <ta e="T291" id="Seg_4167" s="T290">v-v:cvb</ta>
            <ta e="T292" id="Seg_4168" s="T291">v-v:cvb</ta>
            <ta e="T293" id="Seg_4169" s="T292">n-n:(poss)-n:case</ta>
            <ta e="T294" id="Seg_4170" s="T293">post</ta>
            <ta e="T295" id="Seg_4171" s="T294">v-v:cvb</ta>
            <ta e="T296" id="Seg_4172" s="T295">v-v:tense-v:pred.pn</ta>
            <ta e="T297" id="Seg_4173" s="T296">n-n:(num)-n:case</ta>
            <ta e="T346" id="Seg_4174" s="T297">n-n:(poss)-n:case</ta>
            <ta e="T298" id="Seg_4175" s="T346">n-n:(num)-n:case</ta>
            <ta e="T299" id="Seg_4176" s="T298">adj-n:poss-n:case</ta>
            <ta e="T300" id="Seg_4177" s="T299">n-n:case</ta>
            <ta e="T301" id="Seg_4178" s="T300">ptcl</ta>
            <ta e="T302" id="Seg_4179" s="T301">n-n:case</ta>
            <ta e="T303" id="Seg_4180" s="T302">n-n:poss-n:case</ta>
            <ta e="T304" id="Seg_4181" s="T303">adv</ta>
            <ta e="T305" id="Seg_4182" s="T304">v-v:tense-v:pred.pn</ta>
            <ta e="T306" id="Seg_4183" s="T305">que-pro:(poss)-pro:case</ta>
            <ta e="T307" id="Seg_4184" s="T306">ptcl</ta>
            <ta e="T308" id="Seg_4185" s="T307">v-v&gt;v-v:(ins)-v:(neg)-v:pred.pn</ta>
            <ta e="T309" id="Seg_4186" s="T308">v-v&gt;v-v:(ins)-v:(neg)-v:pred.pn</ta>
            <ta e="T310" id="Seg_4187" s="T309">n-n:(num)-n:(poss)-n:case</ta>
            <ta e="T311" id="Seg_4188" s="T310">adv</ta>
            <ta e="T312" id="Seg_4189" s="T311">v-v:cvb</ta>
            <ta e="T313" id="Seg_4190" s="T312">v-v:cvb</ta>
            <ta e="T314" id="Seg_4191" s="T313">v-v:tense-v:pred.pn</ta>
            <ta e="T315" id="Seg_4192" s="T314">n-n:case</ta>
            <ta e="T316" id="Seg_4193" s="T315">n-n:(poss)-n:case</ta>
            <ta e="T317" id="Seg_4194" s="T316">adv</ta>
            <ta e="T318" id="Seg_4195" s="T317">v-v:cvb</ta>
            <ta e="T319" id="Seg_4196" s="T318">v-v:mood.pn</ta>
            <ta e="T320" id="Seg_4197" s="T319">pers-pro:case</ta>
            <ta e="T321" id="Seg_4198" s="T320">que</ta>
            <ta e="T322" id="Seg_4199" s="T321">v-v:ptcp</ta>
            <ta e="T323" id="Seg_4200" s="T322">v-v:cvb</ta>
            <ta e="T324" id="Seg_4201" s="T323">v-v:(ins)-v:mood.pn</ta>
            <ta e="T325" id="Seg_4202" s="T324">que</ta>
            <ta e="T326" id="Seg_4203" s="T325">v-v:ptcp</ta>
            <ta e="T327" id="Seg_4204" s="T326">v-v:mood.pn</ta>
            <ta e="T328" id="Seg_4205" s="T327">v-v:tense-v:pred.pn</ta>
            <ta e="T329" id="Seg_4206" s="T328">adv</ta>
            <ta e="T330" id="Seg_4207" s="T329">n-n:(num)-n:case</ta>
            <ta e="T331" id="Seg_4208" s="T330">n-n:(poss)-n:case</ta>
            <ta e="T332" id="Seg_4209" s="T331">n-n:case</ta>
            <ta e="T333" id="Seg_4210" s="T332">n-n:poss-n:case</ta>
            <ta e="T334" id="Seg_4211" s="T333">adj</ta>
            <ta e="T335" id="Seg_4212" s="T334">n-n:case</ta>
            <ta e="T336" id="Seg_4213" s="T335">v-v:tense-v:pred.pn</ta>
            <ta e="T337" id="Seg_4214" s="T336">n-n:(num)-n:case</ta>
            <ta e="T338" id="Seg_4215" s="T337">n-n:(poss)-n:case</ta>
            <ta e="T339" id="Seg_4216" s="T338">n-n:case</ta>
            <ta e="T340" id="Seg_4217" s="T339">n-n:poss-n:case</ta>
            <ta e="T341" id="Seg_4218" s="T340">n-n:poss-case</ta>
            <ta e="T342" id="Seg_4219" s="T341">n-n:case</ta>
            <ta e="T343" id="Seg_4220" s="T342">n-n:case</ta>
            <ta e="T344" id="Seg_4221" s="T343">v-v:tense-v:pred.pn</ta>
            <ta e="T345" id="Seg_4222" s="T344">adj-n:(poss)-n:case</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_4223" s="T0">ptcl</ta>
            <ta e="T2" id="Seg_4224" s="T1">adv</ta>
            <ta e="T3" id="Seg_4225" s="T2">n</ta>
            <ta e="T4" id="Seg_4226" s="T3">v</ta>
            <ta e="T5" id="Seg_4227" s="T4">n</ta>
            <ta e="T6" id="Seg_4228" s="T5">n</ta>
            <ta e="T7" id="Seg_4229" s="T6">adj</ta>
            <ta e="T8" id="Seg_4230" s="T7">ptcl</ta>
            <ta e="T9" id="Seg_4231" s="T8">cardnum</ta>
            <ta e="T10" id="Seg_4232" s="T9">n</ta>
            <ta e="T11" id="Seg_4233" s="T10">adj</ta>
            <ta e="T12" id="Seg_4234" s="T11">cop</ta>
            <ta e="T13" id="Seg_4235" s="T12">adv</ta>
            <ta e="T14" id="Seg_4236" s="T13">v</ta>
            <ta e="T15" id="Seg_4237" s="T14">n</ta>
            <ta e="T16" id="Seg_4238" s="T15">adj</ta>
            <ta e="T17" id="Seg_4239" s="T16">n</ta>
            <ta e="T18" id="Seg_4240" s="T17">cop</ta>
            <ta e="T19" id="Seg_4241" s="T18">cardnum</ta>
            <ta e="T20" id="Seg_4242" s="T19">ptcl</ta>
            <ta e="T21" id="Seg_4243" s="T20">n</ta>
            <ta e="T22" id="Seg_4244" s="T21">n</ta>
            <ta e="T23" id="Seg_4245" s="T22">ptcl</ta>
            <ta e="T24" id="Seg_4246" s="T23">n</ta>
            <ta e="T25" id="Seg_4247" s="T24">v</ta>
            <ta e="T26" id="Seg_4248" s="T25">pers</ta>
            <ta e="T27" id="Seg_4249" s="T26">n</ta>
            <ta e="T28" id="Seg_4250" s="T27">v</ta>
            <ta e="T29" id="Seg_4251" s="T28">n</ta>
            <ta e="T30" id="Seg_4252" s="T29">post</ta>
            <ta e="T31" id="Seg_4253" s="T30">v</ta>
            <ta e="T32" id="Seg_4254" s="T31">cardnum</ta>
            <ta e="T33" id="Seg_4255" s="T32">ptcl</ta>
            <ta e="T34" id="Seg_4256" s="T33">adj</ta>
            <ta e="T35" id="Seg_4257" s="T34">n</ta>
            <ta e="T36" id="Seg_4258" s="T35">adj</ta>
            <ta e="T37" id="Seg_4259" s="T36">n</ta>
            <ta e="T38" id="Seg_4260" s="T37">v</ta>
            <ta e="T39" id="Seg_4261" s="T38">que</ta>
            <ta e="T40" id="Seg_4262" s="T39">ptcl</ta>
            <ta e="T41" id="Seg_4263" s="T40">n</ta>
            <ta e="T42" id="Seg_4264" s="T41">v</ta>
            <ta e="T43" id="Seg_4265" s="T42">n</ta>
            <ta e="T44" id="Seg_4266" s="T43">n</ta>
            <ta e="T45" id="Seg_4267" s="T44">ptcl</ta>
            <ta e="T46" id="Seg_4268" s="T45">v</ta>
            <ta e="T47" id="Seg_4269" s="T46">v</ta>
            <ta e="T48" id="Seg_4270" s="T47">aux</ta>
            <ta e="T49" id="Seg_4271" s="T48">n</ta>
            <ta e="T50" id="Seg_4272" s="T49">ptcl</ta>
            <ta e="T51" id="Seg_4273" s="T50">v</ta>
            <ta e="T52" id="Seg_4274" s="T51">n</ta>
            <ta e="T53" id="Seg_4275" s="T52">v</ta>
            <ta e="T54" id="Seg_4276" s="T53">ptcl</ta>
            <ta e="T55" id="Seg_4277" s="T54">adv</ta>
            <ta e="T56" id="Seg_4278" s="T55">v</ta>
            <ta e="T57" id="Seg_4279" s="T56">pers</ta>
            <ta e="T58" id="Seg_4280" s="T57">que</ta>
            <ta e="T59" id="Seg_4281" s="T58">v</ta>
            <ta e="T60" id="Seg_4282" s="T59">v</ta>
            <ta e="T61" id="Seg_4283" s="T60">pers</ta>
            <ta e="T62" id="Seg_4284" s="T61">adv</ta>
            <ta e="T63" id="Seg_4285" s="T62">v</ta>
            <ta e="T64" id="Seg_4286" s="T63">que</ta>
            <ta e="T65" id="Seg_4287" s="T64">ptcl</ta>
            <ta e="T66" id="Seg_4288" s="T65">n</ta>
            <ta e="T67" id="Seg_4289" s="T66">cardnum</ta>
            <ta e="T68" id="Seg_4290" s="T67">n</ta>
            <ta e="T69" id="Seg_4291" s="T68">adj</ta>
            <ta e="T70" id="Seg_4292" s="T69">cardnum</ta>
            <ta e="T71" id="Seg_4293" s="T70">adj</ta>
            <ta e="T72" id="Seg_4294" s="T71">n</ta>
            <ta e="T73" id="Seg_4295" s="T72">ptcl</ta>
            <ta e="T74" id="Seg_4296" s="T73">dempro</ta>
            <ta e="T75" id="Seg_4297" s="T74">n</ta>
            <ta e="T76" id="Seg_4298" s="T75">adj</ta>
            <ta e="T77" id="Seg_4299" s="T76">n</ta>
            <ta e="T78" id="Seg_4300" s="T77">v</ta>
            <ta e="T79" id="Seg_4301" s="T78">ptcl</ta>
            <ta e="T80" id="Seg_4302" s="T79">indfpro</ta>
            <ta e="T81" id="Seg_4303" s="T80">n</ta>
            <ta e="T82" id="Seg_4304" s="T81">n</ta>
            <ta e="T83" id="Seg_4305" s="T82">post</ta>
            <ta e="T84" id="Seg_4306" s="T83">n</ta>
            <ta e="T85" id="Seg_4307" s="T84">cop</ta>
            <ta e="T86" id="Seg_4308" s="T85">n</ta>
            <ta e="T87" id="Seg_4309" s="T86">adj</ta>
            <ta e="T88" id="Seg_4310" s="T87">n</ta>
            <ta e="T89" id="Seg_4311" s="T88">adv</ta>
            <ta e="T90" id="Seg_4312" s="T89">adj</ta>
            <ta e="T91" id="Seg_4313" s="T90">adj</ta>
            <ta e="T92" id="Seg_4314" s="T91">adj</ta>
            <ta e="T93" id="Seg_4315" s="T92">dempro</ta>
            <ta e="T94" id="Seg_4316" s="T93">n</ta>
            <ta e="T95" id="Seg_4317" s="T94">n</ta>
            <ta e="T96" id="Seg_4318" s="T95">adj</ta>
            <ta e="T97" id="Seg_4319" s="T96">n</ta>
            <ta e="T98" id="Seg_4320" s="T97">adv</ta>
            <ta e="T99" id="Seg_4321" s="T98">n</ta>
            <ta e="T100" id="Seg_4322" s="T99">ptcl</ta>
            <ta e="T101" id="Seg_4323" s="T100">pers</ta>
            <ta e="T102" id="Seg_4324" s="T101">v</ta>
            <ta e="T103" id="Seg_4325" s="T102">ptcl</ta>
            <ta e="T104" id="Seg_4326" s="T103">adv</ta>
            <ta e="T105" id="Seg_4327" s="T104">n</ta>
            <ta e="T106" id="Seg_4328" s="T105">adj</ta>
            <ta e="T107" id="Seg_4329" s="T106">adj</ta>
            <ta e="T108" id="Seg_4330" s="T107">n</ta>
            <ta e="T109" id="Seg_4331" s="T108">post</ta>
            <ta e="T110" id="Seg_4332" s="T109">n</ta>
            <ta e="T111" id="Seg_4333" s="T110">n</ta>
            <ta e="T112" id="Seg_4334" s="T111">n</ta>
            <ta e="T113" id="Seg_4335" s="T112">v</ta>
            <ta e="T114" id="Seg_4336" s="T113">ptcl</ta>
            <ta e="T115" id="Seg_4337" s="T114">v</ta>
            <ta e="T116" id="Seg_4338" s="T115">n</ta>
            <ta e="T117" id="Seg_4339" s="T116">n</ta>
            <ta e="T118" id="Seg_4340" s="T117">n</ta>
            <ta e="T119" id="Seg_4341" s="T118">v</ta>
            <ta e="T120" id="Seg_4342" s="T119">aux</ta>
            <ta e="T121" id="Seg_4343" s="T120">adj</ta>
            <ta e="T122" id="Seg_4344" s="T121">v</ta>
            <ta e="T123" id="Seg_4345" s="T122">ptcl</ta>
            <ta e="T124" id="Seg_4346" s="T123">pers</ta>
            <ta e="T125" id="Seg_4347" s="T124">adv</ta>
            <ta e="T126" id="Seg_4348" s="T125">n</ta>
            <ta e="T127" id="Seg_4349" s="T126">ptcl</ta>
            <ta e="T128" id="Seg_4350" s="T127">v</ta>
            <ta e="T129" id="Seg_4351" s="T128">n</ta>
            <ta e="T130" id="Seg_4352" s="T129">n</ta>
            <ta e="T131" id="Seg_4353" s="T130">adv</ta>
            <ta e="T132" id="Seg_4354" s="T131">v</ta>
            <ta e="T133" id="Seg_4355" s="T132">v</ta>
            <ta e="T134" id="Seg_4356" s="T133">ptcl</ta>
            <ta e="T135" id="Seg_4357" s="T134">dempro</ta>
            <ta e="T136" id="Seg_4358" s="T135">post</ta>
            <ta e="T137" id="Seg_4359" s="T136">v</ta>
            <ta e="T138" id="Seg_4360" s="T137">v</ta>
            <ta e="T139" id="Seg_4361" s="T138">v</ta>
            <ta e="T140" id="Seg_4362" s="T139">n</ta>
            <ta e="T141" id="Seg_4363" s="T140">v</ta>
            <ta e="T142" id="Seg_4364" s="T141">n</ta>
            <ta e="T143" id="Seg_4365" s="T142">v</ta>
            <ta e="T144" id="Seg_4366" s="T143">n</ta>
            <ta e="T145" id="Seg_4367" s="T144">v</ta>
            <ta e="T146" id="Seg_4368" s="T145">aux</ta>
            <ta e="T147" id="Seg_4369" s="T146">n</ta>
            <ta e="T148" id="Seg_4370" s="T147">cop</ta>
            <ta e="T149" id="Seg_4371" s="T148">v</ta>
            <ta e="T150" id="Seg_4372" s="T149">v</ta>
            <ta e="T151" id="Seg_4373" s="T150">n</ta>
            <ta e="T152" id="Seg_4374" s="T151">post</ta>
            <ta e="T153" id="Seg_4375" s="T152">n</ta>
            <ta e="T154" id="Seg_4376" s="T153">n</ta>
            <ta e="T155" id="Seg_4377" s="T154">n</ta>
            <ta e="T156" id="Seg_4378" s="T155">n</ta>
            <ta e="T157" id="Seg_4379" s="T156">v</ta>
            <ta e="T158" id="Seg_4380" s="T157">interj</ta>
            <ta e="T159" id="Seg_4381" s="T158">n</ta>
            <ta e="T160" id="Seg_4382" s="T159">que</ta>
            <ta e="T161" id="Seg_4383" s="T160">n</ta>
            <ta e="T162" id="Seg_4384" s="T161">v</ta>
            <ta e="T163" id="Seg_4385" s="T162">adv</ta>
            <ta e="T164" id="Seg_4386" s="T163">n</ta>
            <ta e="T165" id="Seg_4387" s="T164">adj</ta>
            <ta e="T166" id="Seg_4388" s="T165">adj</ta>
            <ta e="T167" id="Seg_4389" s="T166">cop</ta>
            <ta e="T168" id="Seg_4390" s="T167">pers</ta>
            <ta e="T169" id="Seg_4391" s="T168">n</ta>
            <ta e="T170" id="Seg_4392" s="T169">pers</ta>
            <ta e="T171" id="Seg_4393" s="T170">v</ta>
            <ta e="T172" id="Seg_4394" s="T171">pers</ta>
            <ta e="T173" id="Seg_4395" s="T172">pers</ta>
            <ta e="T174" id="Seg_4396" s="T173">n</ta>
            <ta e="T175" id="Seg_4397" s="T174">que</ta>
            <ta e="T176" id="Seg_4398" s="T175">v</ta>
            <ta e="T177" id="Seg_4399" s="T176">v</ta>
            <ta e="T178" id="Seg_4400" s="T177">adv</ta>
            <ta e="T179" id="Seg_4401" s="T178">v</ta>
            <ta e="T180" id="Seg_4402" s="T179">adv</ta>
            <ta e="T181" id="Seg_4403" s="T180">n</ta>
            <ta e="T182" id="Seg_4404" s="T181">adj</ta>
            <ta e="T183" id="Seg_4405" s="T182">n</ta>
            <ta e="T184" id="Seg_4406" s="T183">n</ta>
            <ta e="T185" id="Seg_4407" s="T184">adj</ta>
            <ta e="T186" id="Seg_4408" s="T185">n</ta>
            <ta e="T187" id="Seg_4409" s="T186">ptcl</ta>
            <ta e="T188" id="Seg_4410" s="T187">v</ta>
            <ta e="T189" id="Seg_4411" s="T188">ptcl</ta>
            <ta e="T190" id="Seg_4412" s="T189">n</ta>
            <ta e="T191" id="Seg_4413" s="T190">ptcl</ta>
            <ta e="T192" id="Seg_4414" s="T191">v</ta>
            <ta e="T193" id="Seg_4415" s="T192">n</ta>
            <ta e="T194" id="Seg_4416" s="T193">ptcl</ta>
            <ta e="T195" id="Seg_4417" s="T194">v</ta>
            <ta e="T196" id="Seg_4418" s="T195">ptcl</ta>
            <ta e="T197" id="Seg_4419" s="T196">dempro</ta>
            <ta e="T198" id="Seg_4420" s="T197">n</ta>
            <ta e="T199" id="Seg_4421" s="T198">n</ta>
            <ta e="T200" id="Seg_4422" s="T199">v</ta>
            <ta e="T201" id="Seg_4423" s="T200">pers</ta>
            <ta e="T202" id="Seg_4424" s="T201">adj</ta>
            <ta e="T203" id="Seg_4425" s="T202">v</ta>
            <ta e="T204" id="Seg_4426" s="T203">n</ta>
            <ta e="T205" id="Seg_4427" s="T204">ptcl</ta>
            <ta e="T206" id="Seg_4428" s="T205">dempro</ta>
            <ta e="T207" id="Seg_4429" s="T206">n</ta>
            <ta e="T208" id="Seg_4430" s="T207">v</ta>
            <ta e="T209" id="Seg_4431" s="T208">n</ta>
            <ta e="T210" id="Seg_4432" s="T209">n</ta>
            <ta e="T211" id="Seg_4433" s="T210">pers</ta>
            <ta e="T212" id="Seg_4434" s="T211">adj</ta>
            <ta e="T213" id="Seg_4435" s="T212">v</ta>
            <ta e="T214" id="Seg_4436" s="T213">n</ta>
            <ta e="T215" id="Seg_4437" s="T214">adv</ta>
            <ta e="T216" id="Seg_4438" s="T215">v</ta>
            <ta e="T217" id="Seg_4439" s="T216">v</ta>
            <ta e="T218" id="Seg_4440" s="T217">v</ta>
            <ta e="T219" id="Seg_4441" s="T218">v</ta>
            <ta e="T220" id="Seg_4442" s="T219">pers</ta>
            <ta e="T221" id="Seg_4443" s="T220">n</ta>
            <ta e="T222" id="Seg_4444" s="T221">v</ta>
            <ta e="T223" id="Seg_4445" s="T222">v</ta>
            <ta e="T224" id="Seg_4446" s="T223">n</ta>
            <ta e="T225" id="Seg_4447" s="T224">n</ta>
            <ta e="T226" id="Seg_4448" s="T225">v</ta>
            <ta e="T227" id="Seg_4449" s="T226">conj</ta>
            <ta e="T228" id="Seg_4450" s="T227">n</ta>
            <ta e="T229" id="Seg_4451" s="T228">interj</ta>
            <ta e="T230" id="Seg_4452" s="T229">v</ta>
            <ta e="T231" id="Seg_4453" s="T230">v</ta>
            <ta e="T232" id="Seg_4454" s="T231">n</ta>
            <ta e="T233" id="Seg_4455" s="T232">v</ta>
            <ta e="T234" id="Seg_4456" s="T233">n</ta>
            <ta e="T235" id="Seg_4457" s="T234">n</ta>
            <ta e="T236" id="Seg_4458" s="T235">adj</ta>
            <ta e="T237" id="Seg_4459" s="T236">v</ta>
            <ta e="T238" id="Seg_4460" s="T237">n</ta>
            <ta e="T239" id="Seg_4461" s="T238">adv</ta>
            <ta e="T240" id="Seg_4462" s="T239">v</ta>
            <ta e="T241" id="Seg_4463" s="T240">n</ta>
            <ta e="T242" id="Seg_4464" s="T241">adv</ta>
            <ta e="T243" id="Seg_4465" s="T242">n</ta>
            <ta e="T244" id="Seg_4466" s="T243">v</ta>
            <ta e="T245" id="Seg_4467" s="T244">n</ta>
            <ta e="T246" id="Seg_4468" s="T245">n</ta>
            <ta e="T247" id="Seg_4469" s="T246">n</ta>
            <ta e="T248" id="Seg_4470" s="T247">v</ta>
            <ta e="T249" id="Seg_4471" s="T248">n</ta>
            <ta e="T250" id="Seg_4472" s="T249">n</ta>
            <ta e="T251" id="Seg_4473" s="T250">v</ta>
            <ta e="T252" id="Seg_4474" s="T251">n</ta>
            <ta e="T253" id="Seg_4475" s="T252">v</ta>
            <ta e="T254" id="Seg_4476" s="T253">ptcl</ta>
            <ta e="T255" id="Seg_4477" s="T254">n</ta>
            <ta e="T256" id="Seg_4478" s="T255">n</ta>
            <ta e="T257" id="Seg_4479" s="T256">n</ta>
            <ta e="T258" id="Seg_4480" s="T257">v</ta>
            <ta e="T259" id="Seg_4481" s="T258">n</ta>
            <ta e="T260" id="Seg_4482" s="T259">n</ta>
            <ta e="T261" id="Seg_4483" s="T260">n</ta>
            <ta e="T262" id="Seg_4484" s="T261">v</ta>
            <ta e="T263" id="Seg_4485" s="T262">n</ta>
            <ta e="T264" id="Seg_4486" s="T263">n</ta>
            <ta e="T265" id="Seg_4487" s="T264">post</ta>
            <ta e="T266" id="Seg_4488" s="T265">v</ta>
            <ta e="T267" id="Seg_4489" s="T266">aux</ta>
            <ta e="T268" id="Seg_4490" s="T267">v</ta>
            <ta e="T269" id="Seg_4491" s="T268">n</ta>
            <ta e="T270" id="Seg_4492" s="T269">n</ta>
            <ta e="T271" id="Seg_4493" s="T270">n</ta>
            <ta e="T272" id="Seg_4494" s="T271">n</ta>
            <ta e="T273" id="Seg_4495" s="T272">post</ta>
            <ta e="T274" id="Seg_4496" s="T273">n</ta>
            <ta e="T275" id="Seg_4497" s="T274">v</ta>
            <ta e="T276" id="Seg_4498" s="T275">aux</ta>
            <ta e="T277" id="Seg_4499" s="T276">n</ta>
            <ta e="T278" id="Seg_4500" s="T277">v</ta>
            <ta e="T279" id="Seg_4501" s="T278">v</ta>
            <ta e="T280" id="Seg_4502" s="T279">n</ta>
            <ta e="T281" id="Seg_4503" s="T280">n</ta>
            <ta e="T282" id="Seg_4504" s="T281">v</ta>
            <ta e="T283" id="Seg_4505" s="T282">post</ta>
            <ta e="T284" id="Seg_4506" s="T283">n</ta>
            <ta e="T285" id="Seg_4507" s="T284">v</ta>
            <ta e="T286" id="Seg_4508" s="T285">n</ta>
            <ta e="T287" id="Seg_4509" s="T286">n</ta>
            <ta e="T288" id="Seg_4510" s="T287">v</ta>
            <ta e="T289" id="Seg_4511" s="T288">n</ta>
            <ta e="T290" id="Seg_4512" s="T289">v</ta>
            <ta e="T291" id="Seg_4513" s="T290">v</ta>
            <ta e="T292" id="Seg_4514" s="T291">aux</ta>
            <ta e="T293" id="Seg_4515" s="T292">n</ta>
            <ta e="T294" id="Seg_4516" s="T293">post</ta>
            <ta e="T295" id="Seg_4517" s="T294">v</ta>
            <ta e="T296" id="Seg_4518" s="T295">aux</ta>
            <ta e="T297" id="Seg_4519" s="T296">n</ta>
            <ta e="T346" id="Seg_4520" s="T297">n</ta>
            <ta e="T298" id="Seg_4521" s="T346">n</ta>
            <ta e="T299" id="Seg_4522" s="T298">adj</ta>
            <ta e="T300" id="Seg_4523" s="T299">n</ta>
            <ta e="T301" id="Seg_4524" s="T300">ptcl</ta>
            <ta e="T302" id="Seg_4525" s="T301">n</ta>
            <ta e="T303" id="Seg_4526" s="T302">n</ta>
            <ta e="T304" id="Seg_4527" s="T303">adv</ta>
            <ta e="T305" id="Seg_4528" s="T304">v</ta>
            <ta e="T306" id="Seg_4529" s="T305">que</ta>
            <ta e="T307" id="Seg_4530" s="T306">ptcl</ta>
            <ta e="T308" id="Seg_4531" s="T307">v</ta>
            <ta e="T309" id="Seg_4532" s="T308">v</ta>
            <ta e="T310" id="Seg_4533" s="T309">n</ta>
            <ta e="T311" id="Seg_4534" s="T310">adv</ta>
            <ta e="T312" id="Seg_4535" s="T311">v</ta>
            <ta e="T313" id="Seg_4536" s="T312">v</ta>
            <ta e="T314" id="Seg_4537" s="T313">v</ta>
            <ta e="T315" id="Seg_4538" s="T314">n</ta>
            <ta e="T316" id="Seg_4539" s="T315">n</ta>
            <ta e="T317" id="Seg_4540" s="T316">adv</ta>
            <ta e="T318" id="Seg_4541" s="T317">v</ta>
            <ta e="T319" id="Seg_4542" s="T318">v</ta>
            <ta e="T320" id="Seg_4543" s="T319">pers</ta>
            <ta e="T321" id="Seg_4544" s="T320">que</ta>
            <ta e="T322" id="Seg_4545" s="T321">v</ta>
            <ta e="T323" id="Seg_4546" s="T322">v</ta>
            <ta e="T324" id="Seg_4547" s="T323">aux</ta>
            <ta e="T325" id="Seg_4548" s="T324">que</ta>
            <ta e="T326" id="Seg_4549" s="T325">v</ta>
            <ta e="T327" id="Seg_4550" s="T326">v</ta>
            <ta e="T328" id="Seg_4551" s="T327">v</ta>
            <ta e="T329" id="Seg_4552" s="T328">adv</ta>
            <ta e="T330" id="Seg_4553" s="T329">n</ta>
            <ta e="T331" id="Seg_4554" s="T330">n</ta>
            <ta e="T332" id="Seg_4555" s="T331">n</ta>
            <ta e="T333" id="Seg_4556" s="T332">n</ta>
            <ta e="T334" id="Seg_4557" s="T333">adj</ta>
            <ta e="T335" id="Seg_4558" s="T334">n</ta>
            <ta e="T336" id="Seg_4559" s="T335">cop</ta>
            <ta e="T337" id="Seg_4560" s="T336">n</ta>
            <ta e="T338" id="Seg_4561" s="T337">n</ta>
            <ta e="T339" id="Seg_4562" s="T338">n</ta>
            <ta e="T340" id="Seg_4563" s="T339">n</ta>
            <ta e="T341" id="Seg_4564" s="T340">n</ta>
            <ta e="T342" id="Seg_4565" s="T341">n</ta>
            <ta e="T343" id="Seg_4566" s="T342">n</ta>
            <ta e="T344" id="Seg_4567" s="T343">cop</ta>
            <ta e="T345" id="Seg_4568" s="T344">adj</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_4569" s="T1">adv:Time</ta>
            <ta e="T3" id="Seg_4570" s="T2">np:Th</ta>
            <ta e="T5" id="Seg_4571" s="T4">np.h:Th</ta>
            <ta e="T10" id="Seg_4572" s="T9">np:Th</ta>
            <ta e="T13" id="Seg_4573" s="T12">adv:Time</ta>
            <ta e="T15" id="Seg_4574" s="T14">np:Th</ta>
            <ta e="T21" id="Seg_4575" s="T20">np:Poss</ta>
            <ta e="T22" id="Seg_4576" s="T21">np:Th</ta>
            <ta e="T24" id="Seg_4577" s="T23">np.h:E</ta>
            <ta e="T26" id="Seg_4578" s="T25">pro.h:A</ta>
            <ta e="T27" id="Seg_4579" s="T26">np:Th</ta>
            <ta e="T30" id="Seg_4580" s="T28">pp:G</ta>
            <ta e="T36" id="Seg_4581" s="T35">0.3.h:Th</ta>
            <ta e="T37" id="Seg_4582" s="T36">np:G</ta>
            <ta e="T38" id="Seg_4583" s="T37">0.3.h:Th</ta>
            <ta e="T40" id="Seg_4584" s="T38">pro:L</ta>
            <ta e="T41" id="Seg_4585" s="T40">np:Th</ta>
            <ta e="T43" id="Seg_4586" s="T42">np:Poss</ta>
            <ta e="T44" id="Seg_4587" s="T43">np:Th</ta>
            <ta e="T48" id="Seg_4588" s="T47">0.3:Th</ta>
            <ta e="T49" id="Seg_4589" s="T48">np:Th</ta>
            <ta e="T52" id="Seg_4590" s="T51">0.3.h:Poss np.h:A</ta>
            <ta e="T55" id="Seg_4591" s="T54">adv:Time</ta>
            <ta e="T57" id="Seg_4592" s="T56">pro.h:E</ta>
            <ta e="T58" id="Seg_4593" s="T57">adv:L</ta>
            <ta e="T61" id="Seg_4594" s="T60">pro.h:Th</ta>
            <ta e="T63" id="Seg_4595" s="T62">0.1.h:E</ta>
            <ta e="T66" id="Seg_4596" s="T65">n:Time</ta>
            <ta e="T68" id="Seg_4597" s="T67">np:P</ta>
            <ta e="T69" id="Seg_4598" s="T68">0.1.h:E</ta>
            <ta e="T72" id="Seg_4599" s="T71">np:Th</ta>
            <ta e="T75" id="Seg_4600" s="T74">np:P</ta>
            <ta e="T77" id="Seg_4601" s="T76">np:L</ta>
            <ta e="T81" id="Seg_4602" s="T80">np:L</ta>
            <ta e="T83" id="Seg_4603" s="T81">pp:Time</ta>
            <ta e="T84" id="Seg_4604" s="T83">np:Th</ta>
            <ta e="T86" id="Seg_4605" s="T85">0.3:Poss np:L</ta>
            <ta e="T87" id="Seg_4606" s="T86">0.3:Th</ta>
            <ta e="T88" id="Seg_4607" s="T87">np:Th</ta>
            <ta e="T94" id="Seg_4608" s="T93">np:L</ta>
            <ta e="T95" id="Seg_4609" s="T94">np.h:Poss</ta>
            <ta e="T97" id="Seg_4610" s="T96">np:Th</ta>
            <ta e="T99" id="Seg_4611" s="T98">np.h:A</ta>
            <ta e="T101" id="Seg_4612" s="T100">pro.h:P</ta>
            <ta e="T104" id="Seg_4613" s="T103">adv:Time</ta>
            <ta e="T105" id="Seg_4614" s="T104">n:Time</ta>
            <ta e="T107" id="Seg_4615" s="T106">0.3.h:Th</ta>
            <ta e="T109" id="Seg_4616" s="T107">pp:Com</ta>
            <ta e="T110" id="Seg_4617" s="T109">np.h:A</ta>
            <ta e="T111" id="Seg_4618" s="T110">np.h:poss</ta>
            <ta e="T112" id="Seg_4619" s="T111">np:G</ta>
            <ta e="T116" id="Seg_4620" s="T115">np.h:A</ta>
            <ta e="T117" id="Seg_4621" s="T116">np.h:Poss</ta>
            <ta e="T118" id="Seg_4622" s="T117">np:G</ta>
            <ta e="T120" id="Seg_4623" s="T118">0.3.h:E</ta>
            <ta e="T121" id="Seg_4624" s="T120">np:Ins</ta>
            <ta e="T123" id="Seg_4625" s="T121">0.3.h:A</ta>
            <ta e="T124" id="Seg_4626" s="T123">pro.h:A</ta>
            <ta e="T125" id="Seg_4627" s="T124">adv:Time</ta>
            <ta e="T126" id="Seg_4628" s="T125">np:Ins</ta>
            <ta e="T129" id="Seg_4629" s="T128">np.h:A</ta>
            <ta e="T130" id="Seg_4630" s="T129">0.3:Th</ta>
            <ta e="T131" id="Seg_4631" s="T130">adv:L</ta>
            <ta e="T133" id="Seg_4632" s="T132">0.1.h:P</ta>
            <ta e="T137" id="Seg_4633" s="T136">0.1.h:A</ta>
            <ta e="T138" id="Seg_4634" s="T137">0.1.h:A</ta>
            <ta e="T139" id="Seg_4635" s="T138">0.3.h:A</ta>
            <ta e="T140" id="Seg_4636" s="T139">np.h:A</ta>
            <ta e="T142" id="Seg_4637" s="T141">np:G</ta>
            <ta e="T143" id="Seg_4638" s="T142">0.3.h:Th</ta>
            <ta e="T144" id="Seg_4639" s="T143">np:Th</ta>
            <ta e="T147" id="Seg_4640" s="T146">0.3:Th</ta>
            <ta e="T150" id="Seg_4641" s="T149">0.3.h:A</ta>
            <ta e="T152" id="Seg_4642" s="T150">pp:Path</ta>
            <ta e="T153" id="Seg_4643" s="T152">np.h:Poss</ta>
            <ta e="T154" id="Seg_4644" s="T153">np.h:Poss</ta>
            <ta e="T155" id="Seg_4645" s="T154">np:Th</ta>
            <ta e="T156" id="Seg_4646" s="T155">np:P</ta>
            <ta e="T161" id="Seg_4647" s="T160">np:G</ta>
            <ta e="T162" id="Seg_4648" s="T161">0.2.h:A</ta>
            <ta e="T163" id="Seg_4649" s="T162">adv:Time</ta>
            <ta e="T164" id="Seg_4650" s="T163">n:Time</ta>
            <ta e="T167" id="Seg_4651" s="T166">0.1.h:Th</ta>
            <ta e="T168" id="Seg_4652" s="T167">pro.h:Poss</ta>
            <ta e="T169" id="Seg_4653" s="T168">np:G</ta>
            <ta e="T170" id="Seg_4654" s="T169">pro.h:A</ta>
            <ta e="T172" id="Seg_4655" s="T171">pro.h:A</ta>
            <ta e="T173" id="Seg_4656" s="T172">pro.h:Poss</ta>
            <ta e="T174" id="Seg_4657" s="T173">np:G</ta>
            <ta e="T176" id="Seg_4658" s="T175">0.2.h:A</ta>
            <ta e="T177" id="Seg_4659" s="T176">0.2.h:A</ta>
            <ta e="T178" id="Seg_4660" s="T177">adv:Time</ta>
            <ta e="T179" id="Seg_4661" s="T178">0.2.h:A</ta>
            <ta e="T180" id="Seg_4662" s="T179">adv:L</ta>
            <ta e="T181" id="Seg_4663" s="T180">np:Poss</ta>
            <ta e="T183" id="Seg_4664" s="T182">np:P</ta>
            <ta e="T184" id="Seg_4665" s="T183">np:Poss</ta>
            <ta e="T186" id="Seg_4666" s="T185">np:P</ta>
            <ta e="T189" id="Seg_4667" s="T187">0.1.h:A</ta>
            <ta e="T190" id="Seg_4668" s="T189">np:P</ta>
            <ta e="T192" id="Seg_4669" s="T191">0.1.h:A</ta>
            <ta e="T193" id="Seg_4670" s="T192">np:P</ta>
            <ta e="T196" id="Seg_4671" s="T194">0.1.h:A</ta>
            <ta e="T197" id="Seg_4672" s="T196">pro:Time</ta>
            <ta e="T198" id="Seg_4673" s="T197">np.h:Poss</ta>
            <ta e="T199" id="Seg_4674" s="T198">np.h:A</ta>
            <ta e="T201" id="Seg_4675" s="T200">pro.h:A</ta>
            <ta e="T202" id="Seg_4676" s="T201">np:Ins</ta>
            <ta e="T204" id="Seg_4677" s="T203">np:Ins</ta>
            <ta e="T207" id="Seg_4678" s="T206">np:P</ta>
            <ta e="T208" id="Seg_4679" s="T207">0.1.h:A</ta>
            <ta e="T209" id="Seg_4680" s="T208">np.h:Poss</ta>
            <ta e="T210" id="Seg_4681" s="T209">np.h:A</ta>
            <ta e="T211" id="Seg_4682" s="T210">pro.h:A</ta>
            <ta e="T212" id="Seg_4683" s="T211">np:Ins</ta>
            <ta e="T214" id="Seg_4684" s="T213">np.h:P</ta>
            <ta e="T216" id="Seg_4685" s="T215">0.2.h:A</ta>
            <ta e="T217" id="Seg_4686" s="T216">0.2.h:A</ta>
            <ta e="T218" id="Seg_4687" s="T217">0.1.h:A</ta>
            <ta e="T219" id="Seg_4688" s="T218">0.2.h:A</ta>
            <ta e="T220" id="Seg_4689" s="T219">pro.h:A</ta>
            <ta e="T221" id="Seg_4690" s="T220">np.h:Th</ta>
            <ta e="T223" id="Seg_4691" s="T222">0.3.h:A</ta>
            <ta e="T224" id="Seg_4692" s="T223">np.h:Poss</ta>
            <ta e="T225" id="Seg_4693" s="T224">np.h:A</ta>
            <ta e="T228" id="Seg_4694" s="T227">np:Ins</ta>
            <ta e="T231" id="Seg_4695" s="T230">0.3.h:A</ta>
            <ta e="T232" id="Seg_4696" s="T231">np:Th</ta>
            <ta e="T234" id="Seg_4697" s="T233">np.h:A</ta>
            <ta e="T235" id="Seg_4698" s="T234">0.3.h:Poss np:Th</ta>
            <ta e="T238" id="Seg_4699" s="T237">np:P</ta>
            <ta e="T241" id="Seg_4700" s="T240">np.h:Th</ta>
            <ta e="T243" id="Seg_4701" s="T242">np:L</ta>
            <ta e="T245" id="Seg_4702" s="T244">np.h:Poss</ta>
            <ta e="T246" id="Seg_4703" s="T245">np.h:A</ta>
            <ta e="T247" id="Seg_4704" s="T246">np.h:R</ta>
            <ta e="T249" id="Seg_4705" s="T248">np.h:P</ta>
            <ta e="T250" id="Seg_4706" s="T249">np:G</ta>
            <ta e="T251" id="Seg_4707" s="T250">0.2.h:A</ta>
            <ta e="T252" id="Seg_4708" s="T251">np:G</ta>
            <ta e="T254" id="Seg_4709" s="T252">0.3.h:P</ta>
            <ta e="T255" id="Seg_4710" s="T254">np.h:Poss</ta>
            <ta e="T256" id="Seg_4711" s="T255">np:A</ta>
            <ta e="T257" id="Seg_4712" s="T256">np:Th</ta>
            <ta e="T259" id="Seg_4713" s="T258">np:Poss</ta>
            <ta e="T260" id="Seg_4714" s="T259">np:Poss</ta>
            <ta e="T261" id="Seg_4715" s="T260">np:Th</ta>
            <ta e="T263" id="Seg_4716" s="T262">np.h:A</ta>
            <ta e="T265" id="Seg_4717" s="T263">pp:G</ta>
            <ta e="T269" id="Seg_4718" s="T268">np.h:Poss</ta>
            <ta e="T270" id="Seg_4719" s="T269">np:Th</ta>
            <ta e="T271" id="Seg_4720" s="T270">np.h:Poss</ta>
            <ta e="T272" id="Seg_4721" s="T271">np:G</ta>
            <ta e="T274" id="Seg_4722" s="T273">np:G</ta>
            <ta e="T277" id="Seg_4723" s="T276">np.h:A</ta>
            <ta e="T280" id="Seg_4724" s="T279">np.h:A</ta>
            <ta e="T281" id="Seg_4725" s="T280">np:Th</ta>
            <ta e="T284" id="Seg_4726" s="T283">np.h:A</ta>
            <ta e="T286" id="Seg_4727" s="T285">np.h:Poss</ta>
            <ta e="T287" id="Seg_4728" s="T286">np:G</ta>
            <ta e="T289" id="Seg_4729" s="T288">np.h:A</ta>
            <ta e="T294" id="Seg_4730" s="T292">pp:G</ta>
            <ta e="T296" id="Seg_4731" s="T294">0.3.h:A</ta>
            <ta e="T297" id="Seg_4732" s="T296">np.h:Poss</ta>
            <ta e="T346" id="Seg_4733" s="T297">np:Th</ta>
            <ta e="T298" id="Seg_4734" s="T346">np.h:Poss</ta>
            <ta e="T300" id="Seg_4735" s="T299">np:G</ta>
            <ta e="T302" id="Seg_4736" s="T301">np:G</ta>
            <ta e="T303" id="Seg_4737" s="T302">np:G</ta>
            <ta e="T305" id="Seg_4738" s="T304">0.3.h:A</ta>
            <ta e="T307" id="Seg_4739" s="T305">pro.h:A</ta>
            <ta e="T309" id="Seg_4740" s="T308">0.3.h:Th</ta>
            <ta e="T310" id="Seg_4741" s="T309">np:Th</ta>
            <ta e="T314" id="Seg_4742" s="T312">0.3.h:A</ta>
            <ta e="T315" id="Seg_4743" s="T314">np.h:Poss</ta>
            <ta e="T316" id="Seg_4744" s="T315">np.h:A</ta>
            <ta e="T317" id="Seg_4745" s="T316">adv:Time</ta>
            <ta e="T319" id="Seg_4746" s="T317">0.1.h:A</ta>
            <ta e="T320" id="Seg_4747" s="T319">pro.h:A</ta>
            <ta e="T321" id="Seg_4748" s="T320">pro:G</ta>
            <ta e="T325" id="Seg_4749" s="T324">pro:L</ta>
            <ta e="T327" id="Seg_4750" s="T326">0.2.h:A</ta>
            <ta e="T328" id="Seg_4751" s="T327">0.3.h:A</ta>
            <ta e="T329" id="Seg_4752" s="T328">adv:Time</ta>
            <ta e="T330" id="Seg_4753" s="T329">np.h:Poss</ta>
            <ta e="T331" id="Seg_4754" s="T330">np:Th</ta>
            <ta e="T332" id="Seg_4755" s="T331">np.h:Poss</ta>
            <ta e="T333" id="Seg_4756" s="T332">np:L</ta>
            <ta e="T337" id="Seg_4757" s="T336">np.h:Poss</ta>
            <ta e="T338" id="Seg_4758" s="T337">np:Th</ta>
            <ta e="T339" id="Seg_4759" s="T338">np.h:Poss</ta>
            <ta e="T340" id="Seg_4760" s="T339">np:Poss</ta>
            <ta e="T341" id="Seg_4761" s="T340">np:L</ta>
            <ta e="T345" id="Seg_4762" s="T344">0.3:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T4" id="Seg_4763" s="T2">s:temp</ta>
            <ta e="T5" id="Seg_4764" s="T4">np.h:S</ta>
            <ta e="T7" id="Seg_4765" s="T6">adj:pred</ta>
            <ta e="T10" id="Seg_4766" s="T9">np:S</ta>
            <ta e="T11" id="Seg_4767" s="T10">adj:pred</ta>
            <ta e="T12" id="Seg_4768" s="T11">cop</ta>
            <ta e="T14" id="Seg_4769" s="T13">v:pred</ta>
            <ta e="T15" id="Seg_4770" s="T14">np:S</ta>
            <ta e="T17" id="Seg_4771" s="T16">n:pred</ta>
            <ta e="T18" id="Seg_4772" s="T17">cop</ta>
            <ta e="T22" id="Seg_4773" s="T21">np:S</ta>
            <ta e="T23" id="Seg_4774" s="T22">ptcl:pred</ta>
            <ta e="T24" id="Seg_4775" s="T23">np.h:S</ta>
            <ta e="T25" id="Seg_4776" s="T24">v:pred</ta>
            <ta e="T26" id="Seg_4777" s="T25">pro.h:S</ta>
            <ta e="T28" id="Seg_4778" s="T26">s:purp</ta>
            <ta e="T31" id="Seg_4779" s="T30">v:pred</ta>
            <ta e="T36" id="Seg_4780" s="T35">0.3.h:S adj:pred</ta>
            <ta e="T38" id="Seg_4781" s="T37">0.3.h:S v:pred</ta>
            <ta e="T41" id="Seg_4782" s="T40">np:S</ta>
            <ta e="T42" id="Seg_4783" s="T41">v:pred</ta>
            <ta e="T44" id="Seg_4784" s="T43">np:S</ta>
            <ta e="T46" id="Seg_4785" s="T45">adj:pred</ta>
            <ta e="T48" id="Seg_4786" s="T47">0.3:S v:pred</ta>
            <ta e="T49" id="Seg_4787" s="T48">np:S</ta>
            <ta e="T51" id="Seg_4788" s="T50">v:pred</ta>
            <ta e="T52" id="Seg_4789" s="T51">np.h:S</ta>
            <ta e="T53" id="Seg_4790" s="T52">v:pred</ta>
            <ta e="T56" id="Seg_4791" s="T55">v:pred</ta>
            <ta e="T57" id="Seg_4792" s="T56">pro.h:S</ta>
            <ta e="T60" id="Seg_4793" s="T58">v:pred</ta>
            <ta e="T61" id="Seg_4794" s="T60">pro.h:S</ta>
            <ta e="T63" id="Seg_4795" s="T62">0.1.h:S v:pred</ta>
            <ta e="T68" id="Seg_4796" s="T67">np:O</ta>
            <ta e="T69" id="Seg_4797" s="T68">0.1.h:S v:pred</ta>
            <ta e="T72" id="Seg_4798" s="T71">np:S</ta>
            <ta e="T73" id="Seg_4799" s="T72">ptcl:pred</ta>
            <ta e="T75" id="Seg_4800" s="T74">np:S</ta>
            <ta e="T79" id="Seg_4801" s="T77">v:pred</ta>
            <ta e="T84" id="Seg_4802" s="T83">np:S</ta>
            <ta e="T85" id="Seg_4803" s="T84">cop</ta>
            <ta e="T87" id="Seg_4804" s="T86">0.3:S adj:pred</ta>
            <ta e="T88" id="Seg_4805" s="T87">np:S</ta>
            <ta e="T90" id="Seg_4806" s="T89">adj:pred</ta>
            <ta e="T91" id="Seg_4807" s="T90">adj:pred</ta>
            <ta e="T92" id="Seg_4808" s="T91">adj:pred</ta>
            <ta e="T97" id="Seg_4809" s="T96">np:S</ta>
            <ta e="T99" id="Seg_4810" s="T98">np.h:S</ta>
            <ta e="T101" id="Seg_4811" s="T100">pro.h:O</ta>
            <ta e="T103" id="Seg_4812" s="T101">v:pred</ta>
            <ta e="T107" id="Seg_4813" s="T106">0.3.h:S adj:pred</ta>
            <ta e="T110" id="Seg_4814" s="T109">np.h:S</ta>
            <ta e="T114" id="Seg_4815" s="T112">v:pred</ta>
            <ta e="T116" id="Seg_4816" s="T115">np.h:S</ta>
            <ta e="T120" id="Seg_4817" s="T118">s:temp</ta>
            <ta e="T123" id="Seg_4818" s="T121">0.3.h:S v:pred</ta>
            <ta e="T124" id="Seg_4819" s="T123">pro.h:S</ta>
            <ta e="T128" id="Seg_4820" s="T127">v:pred</ta>
            <ta e="T129" id="Seg_4821" s="T128">np.h:S</ta>
            <ta e="T130" id="Seg_4822" s="T129">0.3:S n:pred</ta>
            <ta e="T133" id="Seg_4823" s="T132">0.1.h:S v:pred</ta>
            <ta e="T137" id="Seg_4824" s="T136">0.1.h:S v:pred</ta>
            <ta e="T138" id="Seg_4825" s="T137">0.1.h:S v:pred</ta>
            <ta e="T139" id="Seg_4826" s="T138">0.3.h:S v:pred</ta>
            <ta e="T140" id="Seg_4827" s="T139">np.h:S</ta>
            <ta e="T141" id="Seg_4828" s="T140">v:pred</ta>
            <ta e="T143" id="Seg_4829" s="T142">0.3.h:S v:pred</ta>
            <ta e="T146" id="Seg_4830" s="T143">s:rel</ta>
            <ta e="T147" id="Seg_4831" s="T146">n:pred</ta>
            <ta e="T149" id="Seg_4832" s="T148">s:adv</ta>
            <ta e="T150" id="Seg_4833" s="T149">0.3.h:S v:pred</ta>
            <ta e="T155" id="Seg_4834" s="T154">np:S</ta>
            <ta e="T156" id="Seg_4835" s="T155">np:O</ta>
            <ta e="T157" id="Seg_4836" s="T156">v:pred</ta>
            <ta e="T162" id="Seg_4837" s="T161">0.2.h:S v:pred</ta>
            <ta e="T166" id="Seg_4838" s="T165">adj:pred</ta>
            <ta e="T167" id="Seg_4839" s="T166">0.1.h:S cop</ta>
            <ta e="T170" id="Seg_4840" s="T169">pro.h:S</ta>
            <ta e="T171" id="Seg_4841" s="T170">v:pred</ta>
            <ta e="T172" id="Seg_4842" s="T171">pro.h:S</ta>
            <ta e="T176" id="Seg_4843" s="T175">0.2.h:S v:pred</ta>
            <ta e="T177" id="Seg_4844" s="T176">0.2.h:S v:pred</ta>
            <ta e="T179" id="Seg_4845" s="T178">0.2.h:S v:pred</ta>
            <ta e="T183" id="Seg_4846" s="T182">np:O</ta>
            <ta e="T186" id="Seg_4847" s="T185">np:O</ta>
            <ta e="T189" id="Seg_4848" s="T187">0.1.h:S v:pred</ta>
            <ta e="T190" id="Seg_4849" s="T189">np:O</ta>
            <ta e="T192" id="Seg_4850" s="T191">0.1.h:S v:pred</ta>
            <ta e="T193" id="Seg_4851" s="T192">np:O</ta>
            <ta e="T196" id="Seg_4852" s="T194">0.1.h:S v:pred</ta>
            <ta e="T199" id="Seg_4853" s="T198">np.h:S</ta>
            <ta e="T200" id="Seg_4854" s="T199">v:pred</ta>
            <ta e="T201" id="Seg_4855" s="T200">pro.h:S</ta>
            <ta e="T203" id="Seg_4856" s="T202">v:pred</ta>
            <ta e="T207" id="Seg_4857" s="T206">np:O</ta>
            <ta e="T208" id="Seg_4858" s="T207">0.1.h:S v:pred</ta>
            <ta e="T210" id="Seg_4859" s="T209">np.h:S</ta>
            <ta e="T211" id="Seg_4860" s="T210">pro.h:S</ta>
            <ta e="T213" id="Seg_4861" s="T212">v:pred</ta>
            <ta e="T216" id="Seg_4862" s="T213">s:cond</ta>
            <ta e="T217" id="Seg_4863" s="T216">0.2.h:S v:pred</ta>
            <ta e="T218" id="Seg_4864" s="T217">0.1.h:S v:pred</ta>
            <ta e="T219" id="Seg_4865" s="T218">0.2.h:S v:pred</ta>
            <ta e="T220" id="Seg_4866" s="T219">pro.h:S</ta>
            <ta e="T221" id="Seg_4867" s="T220">np.h:O</ta>
            <ta e="T222" id="Seg_4868" s="T221">v:pred</ta>
            <ta e="T223" id="Seg_4869" s="T222">0.3.h:S v:pred</ta>
            <ta e="T225" id="Seg_4870" s="T224">np.h:S</ta>
            <ta e="T226" id="Seg_4871" s="T225">v:pred</ta>
            <ta e="T230" id="Seg_4872" s="T229">0.3.h:S v:pred</ta>
            <ta e="T231" id="Seg_4873" s="T230">0.3.h:S v:pred</ta>
            <ta e="T233" id="Seg_4874" s="T231">s:purp</ta>
            <ta e="T237" id="Seg_4875" s="T233">s:adv</ta>
            <ta e="T238" id="Seg_4876" s="T237">np:O</ta>
            <ta e="T240" id="Seg_4877" s="T239">0.3.h:S v:pred</ta>
            <ta e="T241" id="Seg_4878" s="T240">np.h:S</ta>
            <ta e="T244" id="Seg_4879" s="T243">v:pred</ta>
            <ta e="T246" id="Seg_4880" s="T245">np.h:S</ta>
            <ta e="T248" id="Seg_4881" s="T247">v:pred</ta>
            <ta e="T251" id="Seg_4882" s="T250">0.2.h:S v:pred</ta>
            <ta e="T254" id="Seg_4883" s="T252">0.3.h:S v:pred</ta>
            <ta e="T256" id="Seg_4884" s="T255">np.h:S</ta>
            <ta e="T257" id="Seg_4885" s="T256">np:O</ta>
            <ta e="T258" id="Seg_4886" s="T257">v:pred</ta>
            <ta e="T262" id="Seg_4887" s="T258">s:temp</ta>
            <ta e="T263" id="Seg_4888" s="T262">np.h:S</ta>
            <ta e="T268" id="Seg_4889" s="T265">v:pred</ta>
            <ta e="T270" id="Seg_4890" s="T269">np:S</ta>
            <ta e="T276" id="Seg_4891" s="T274">v:pred</ta>
            <ta e="T277" id="Seg_4892" s="T276">np.h:S</ta>
            <ta e="T279" id="Seg_4893" s="T277">v:pred</ta>
            <ta e="T283" id="Seg_4894" s="T279">s:temp</ta>
            <ta e="T284" id="Seg_4895" s="T283">np.h:S</ta>
            <ta e="T285" id="Seg_4896" s="T284">v:pred</ta>
            <ta e="T288" id="Seg_4897" s="T285">s:adv</ta>
            <ta e="T290" id="Seg_4898" s="T288">s:adv</ta>
            <ta e="T292" id="Seg_4899" s="T290">s:adv</ta>
            <ta e="T296" id="Seg_4900" s="T294">0.3.h:S v:pred</ta>
            <ta e="T346" id="Seg_4901" s="T297">np:S</ta>
            <ta e="T305" id="Seg_4902" s="T304">0.3.h:S v:pred</ta>
            <ta e="T307" id="Seg_4903" s="T305">pro.h:S</ta>
            <ta e="T308" id="Seg_4904" s="T307">v:pred</ta>
            <ta e="T309" id="Seg_4905" s="T308">0.3.h:S v:pred</ta>
            <ta e="T312" id="Seg_4906" s="T309">s:temp</ta>
            <ta e="T314" id="Seg_4907" s="T312">0.3.h:S v:pred</ta>
            <ta e="T316" id="Seg_4908" s="T315">np.h:S</ta>
            <ta e="T319" id="Seg_4909" s="T317">0.1.h:S v:pred</ta>
            <ta e="T320" id="Seg_4910" s="T319">pro.h:S</ta>
            <ta e="T322" id="Seg_4911" s="T320">s:rel</ta>
            <ta e="T324" id="Seg_4912" s="T322">v:pred</ta>
            <ta e="T326" id="Seg_4913" s="T324">s:rel</ta>
            <ta e="T327" id="Seg_4914" s="T326">v:pred</ta>
            <ta e="T328" id="Seg_4915" s="T327">0.3.h:S v:pred</ta>
            <ta e="T331" id="Seg_4916" s="T330">np:S</ta>
            <ta e="T335" id="Seg_4917" s="T334">n:pred</ta>
            <ta e="T336" id="Seg_4918" s="T335">cop</ta>
            <ta e="T338" id="Seg_4919" s="T337">np:S</ta>
            <ta e="T343" id="Seg_4920" s="T342">n:pred</ta>
            <ta e="T344" id="Seg_4921" s="T343">cop</ta>
            <ta e="T345" id="Seg_4922" s="T344">0.3:S adj:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST">
            <ta e="T3" id="Seg_4923" s="T2">accs-gen</ta>
            <ta e="T5" id="Seg_4924" s="T4">accs-gen</ta>
            <ta e="T6" id="Seg_4925" s="T5">accs-gen</ta>
            <ta e="T10" id="Seg_4926" s="T9">new</ta>
            <ta e="T15" id="Seg_4927" s="T14">accs-sit</ta>
            <ta e="T17" id="Seg_4928" s="T16">accs-sit</ta>
            <ta e="T21" id="Seg_4929" s="T20">accs-gen</ta>
            <ta e="T22" id="Seg_4930" s="T21">accs-inf</ta>
            <ta e="T24" id="Seg_4931" s="T23">giv-inactive</ta>
            <ta e="T26" id="Seg_4932" s="T25">giv-active</ta>
            <ta e="T27" id="Seg_4933" s="T26">new</ta>
            <ta e="T29" id="Seg_4934" s="T28">new</ta>
            <ta e="T36" id="Seg_4935" s="T35">0.giv-active accs-inf</ta>
            <ta e="T37" id="Seg_4936" s="T36">giv-inactive</ta>
            <ta e="T38" id="Seg_4937" s="T37">0.giv-active</ta>
            <ta e="T41" id="Seg_4938" s="T40">accs-sit</ta>
            <ta e="T44" id="Seg_4939" s="T43">accs-inf</ta>
            <ta e="T49" id="Seg_4940" s="T48">accs-inf</ta>
            <ta e="T52" id="Seg_4941" s="T51">giv-inactive</ta>
            <ta e="T53" id="Seg_4942" s="T52">quot-sp</ta>
            <ta e="T57" id="Seg_4943" s="T56">giv-inactive-Q</ta>
            <ta e="T61" id="Seg_4944" s="T60">giv-active-Q</ta>
            <ta e="T63" id="Seg_4945" s="T62">0.giv-inactive-Q</ta>
            <ta e="T66" id="Seg_4946" s="T65">new-Q</ta>
            <ta e="T68" id="Seg_4947" s="T67">new-Q</ta>
            <ta e="T69" id="Seg_4948" s="T68">0.giv-active-Q</ta>
            <ta e="T72" id="Seg_4949" s="T71">new-Q</ta>
            <ta e="T75" id="Seg_4950" s="T74">giv-active-Q</ta>
            <ta e="T77" id="Seg_4951" s="T76">accs-gen-Q</ta>
            <ta e="T81" id="Seg_4952" s="T80">accs-inf-Q</ta>
            <ta e="T82" id="Seg_4953" s="T81">accs-gen-Q</ta>
            <ta e="T84" id="Seg_4954" s="T83">accs-gen-Q</ta>
            <ta e="T86" id="Seg_4955" s="T85">accs-inf-Q</ta>
            <ta e="T88" id="Seg_4956" s="T87">accs-inf-Q</ta>
            <ta e="T94" id="Seg_4957" s="T93">giv-inactive-Q</ta>
            <ta e="T95" id="Seg_4958" s="T94">new-Q</ta>
            <ta e="T97" id="Seg_4959" s="T96">accs-inf-Q</ta>
            <ta e="T99" id="Seg_4960" s="T98">giv-active-Q</ta>
            <ta e="T101" id="Seg_4961" s="T100">giv-inactive-Q</ta>
            <ta e="T105" id="Seg_4962" s="T104">new-Q</ta>
            <ta e="T107" id="Seg_4963" s="T106">0.giv-active-Q new-Q</ta>
            <ta e="T108" id="Seg_4964" s="T107">giv-inactive-Q</ta>
            <ta e="T110" id="Seg_4965" s="T109">giv-active-Q</ta>
            <ta e="T111" id="Seg_4966" s="T110">giv-active-Q</ta>
            <ta e="T112" id="Seg_4967" s="T111">accs-inf-Q</ta>
            <ta e="T116" id="Seg_4968" s="T115">giv-active-Q</ta>
            <ta e="T117" id="Seg_4969" s="T116">giv-active-Q</ta>
            <ta e="T118" id="Seg_4970" s="T117">accs-inf-Q</ta>
            <ta e="T120" id="Seg_4971" s="T118">0.giv-active-Q</ta>
            <ta e="T123" id="Seg_4972" s="T121">0.giv-active-Q</ta>
            <ta e="T124" id="Seg_4973" s="T123">giv-inactive-Q</ta>
            <ta e="T129" id="Seg_4974" s="T128">giv-active</ta>
            <ta e="T133" id="Seg_4975" s="T132">0.giv-inactive-Q</ta>
            <ta e="T135" id="Seg_4976" s="T134">giv-active-Q</ta>
            <ta e="T137" id="Seg_4977" s="T136">0.giv-active-Q</ta>
            <ta e="T138" id="Seg_4978" s="T137">0.giv-active-Q</ta>
            <ta e="T139" id="Seg_4979" s="T138">0.quot-sp</ta>
            <ta e="T140" id="Seg_4980" s="T139">giv-active</ta>
            <ta e="T142" id="Seg_4981" s="T141">giv-inactive</ta>
            <ta e="T144" id="Seg_4982" s="T143">giv-inactive</ta>
            <ta e="T147" id="Seg_4983" s="T146">giv-inactive</ta>
            <ta e="T150" id="Seg_4984" s="T148">0.giv-inactive</ta>
            <ta e="T151" id="Seg_4985" s="T150">accs-inf</ta>
            <ta e="T153" id="Seg_4986" s="T152">giv-inactive</ta>
            <ta e="T154" id="Seg_4987" s="T153">accs-inf</ta>
            <ta e="T155" id="Seg_4988" s="T154">accs-inf</ta>
            <ta e="T159" id="Seg_4989" s="T158">giv-inactive-Q</ta>
            <ta e="T161" id="Seg_4990" s="T160">giv-inactive-Q</ta>
            <ta e="T162" id="Seg_4991" s="T161">0.giv-active-Q</ta>
            <ta e="T166" id="Seg_4992" s="T165">giv-inactive-Q</ta>
            <ta e="T167" id="Seg_4993" s="T166">0.giv-inactive-Q</ta>
            <ta e="T168" id="Seg_4994" s="T167">giv-inactive-Q</ta>
            <ta e="T169" id="Seg_4995" s="T168">giv-inactive-Q</ta>
            <ta e="T170" id="Seg_4996" s="T169">giv-active-Q</ta>
            <ta e="T172" id="Seg_4997" s="T171">giv-active-Q</ta>
            <ta e="T173" id="Seg_4998" s="T172">giv-active-Q</ta>
            <ta e="T174" id="Seg_4999" s="T173">giv-active-Q</ta>
            <ta e="T176" id="Seg_5000" s="T175">0.giv-active-Q</ta>
            <ta e="T177" id="Seg_5001" s="T176">0.giv-active-Q</ta>
            <ta e="T179" id="Seg_5002" s="T178">0.giv-active-Q</ta>
            <ta e="T181" id="Seg_5003" s="T180">accs-sit-Q</ta>
            <ta e="T183" id="Seg_5004" s="T182">accs-inf-Q</ta>
            <ta e="T184" id="Seg_5005" s="T183">accs-sit-Q</ta>
            <ta e="T186" id="Seg_5006" s="T185">accs-inf-Q</ta>
            <ta e="T189" id="Seg_5007" s="T187">0.giv-active-Q</ta>
            <ta e="T190" id="Seg_5008" s="T189">accs-sit-Q</ta>
            <ta e="T192" id="Seg_5009" s="T191">0.giv-active-Q</ta>
            <ta e="T193" id="Seg_5010" s="T192">accs-sit-Q</ta>
            <ta e="T196" id="Seg_5011" s="T194">0.giv-active-Q</ta>
            <ta e="T198" id="Seg_5012" s="T197">giv-inactive</ta>
            <ta e="T199" id="Seg_5013" s="T198">giv-inactive</ta>
            <ta e="T200" id="Seg_5014" s="T199">quot-sp</ta>
            <ta e="T201" id="Seg_5015" s="T200">giv-inactive-Q</ta>
            <ta e="T207" id="Seg_5016" s="T206">giv-inactive-Q</ta>
            <ta e="T208" id="Seg_5017" s="T207">0.giv-active-Q</ta>
            <ta e="T209" id="Seg_5018" s="T208">giv-inactive</ta>
            <ta e="T210" id="Seg_5019" s="T209">giv-inactive</ta>
            <ta e="T211" id="Seg_5020" s="T210">giv-active-Q</ta>
            <ta e="T214" id="Seg_5021" s="T213">giv-inactive-Q</ta>
            <ta e="T216" id="Seg_5022" s="T215">0.giv-inactive-Q</ta>
            <ta e="T217" id="Seg_5023" s="T216">0.giv-active-Q</ta>
            <ta e="T218" id="Seg_5024" s="T217">0.giv-active-Q</ta>
            <ta e="T219" id="Seg_5025" s="T218">0.giv-inactive-Q</ta>
            <ta e="T220" id="Seg_5026" s="T219">giv-inactive-Q</ta>
            <ta e="T221" id="Seg_5027" s="T220">accs-inf-Q</ta>
            <ta e="T223" id="Seg_5028" s="T222">0.quot-sp</ta>
            <ta e="T224" id="Seg_5029" s="T223">giv-active</ta>
            <ta e="T225" id="Seg_5030" s="T224">giv-active</ta>
            <ta e="T228" id="Seg_5031" s="T227">accs-inf</ta>
            <ta e="T229" id="Seg_5032" s="T228">new</ta>
            <ta e="T231" id="Seg_5033" s="T230">0.giv-active</ta>
            <ta e="T232" id="Seg_5034" s="T231">giv-inactive</ta>
            <ta e="T234" id="Seg_5035" s="T233">giv-active</ta>
            <ta e="T235" id="Seg_5036" s="T234">accs-inf</ta>
            <ta e="T238" id="Seg_5037" s="T237">giv-inactive</ta>
            <ta e="T241" id="Seg_5038" s="T240">giv-inactive</ta>
            <ta e="T243" id="Seg_5039" s="T242">giv-inactive</ta>
            <ta e="T245" id="Seg_5040" s="T244">giv-active</ta>
            <ta e="T246" id="Seg_5041" s="T245">giv-inactive</ta>
            <ta e="T247" id="Seg_5042" s="T246">accs-inf</ta>
            <ta e="T249" id="Seg_5043" s="T248">giv-inactive</ta>
            <ta e="T250" id="Seg_5044" s="T249">accs-inf</ta>
            <ta e="T251" id="Seg_5045" s="T250">0.giv-active-Q</ta>
            <ta e="T252" id="Seg_5046" s="T251">accs-inf</ta>
            <ta e="T254" id="Seg_5047" s="T252">0.giv-active-Q</ta>
            <ta e="T255" id="Seg_5048" s="T254">giv-active</ta>
            <ta e="T256" id="Seg_5049" s="T255">accs-aggr</ta>
            <ta e="T257" id="Seg_5050" s="T256">new</ta>
            <ta e="T259" id="Seg_5051" s="T258">accs-inf</ta>
            <ta e="T260" id="Seg_5052" s="T259">accs-inf</ta>
            <ta e="T261" id="Seg_5053" s="T260">accs-gen</ta>
            <ta e="T263" id="Seg_5054" s="T262">giv-inactive</ta>
            <ta e="T264" id="Seg_5055" s="T263">accs-inf</ta>
            <ta e="T269" id="Seg_5056" s="T268">giv-inactive</ta>
            <ta e="T270" id="Seg_5057" s="T269">giv-inactive</ta>
            <ta e="T271" id="Seg_5058" s="T270">giv-active</ta>
            <ta e="T272" id="Seg_5059" s="T271">giv-inactive</ta>
            <ta e="T274" id="Seg_5060" s="T273">giv-inactive</ta>
            <ta e="T277" id="Seg_5061" s="T276">giv-active</ta>
            <ta e="T280" id="Seg_5062" s="T279">giv-inactive</ta>
            <ta e="T281" id="Seg_5063" s="T280">giv-inactive</ta>
            <ta e="T284" id="Seg_5064" s="T283">giv-active</ta>
            <ta e="T286" id="Seg_5065" s="T285">giv-active</ta>
            <ta e="T287" id="Seg_5066" s="T286">accs-inf</ta>
            <ta e="T289" id="Seg_5067" s="T288">giv-active</ta>
            <ta e="T293" id="Seg_5068" s="T292">giv-inactive</ta>
            <ta e="T297" id="Seg_5069" s="T296">giv-inactive</ta>
            <ta e="T346" id="Seg_5070" s="T297">giv-inactive</ta>
            <ta e="T298" id="Seg_5071" s="T346">giv-active</ta>
            <ta e="T300" id="Seg_5072" s="T299">accs-inf</ta>
            <ta e="T302" id="Seg_5073" s="T301">accs-inf</ta>
            <ta e="T303" id="Seg_5074" s="T302">accs-inf</ta>
            <ta e="T305" id="Seg_5075" s="T304">0.accs-aggr</ta>
            <ta e="T307" id="Seg_5076" s="T305">giv-active</ta>
            <ta e="T309" id="Seg_5077" s="T308">0.giv-active</ta>
            <ta e="T310" id="Seg_5078" s="T309">giv-inactive</ta>
            <ta e="T314" id="Seg_5079" s="T313">0.giv-inactive</ta>
            <ta e="T315" id="Seg_5080" s="T314">giv-inactive</ta>
            <ta e="T316" id="Seg_5081" s="T315">giv-inactive</ta>
            <ta e="T319" id="Seg_5082" s="T317">0.giv-inactive-Q</ta>
            <ta e="T320" id="Seg_5083" s="T319">giv-inactive-Q</ta>
            <ta e="T327" id="Seg_5084" s="T326">0.giv-active-Q</ta>
            <ta e="T328" id="Seg_5085" s="T327">0.quot-sp</ta>
            <ta e="T330" id="Seg_5086" s="T329">giv-inactive</ta>
            <ta e="T331" id="Seg_5087" s="T330">giv-inactive</ta>
            <ta e="T332" id="Seg_5088" s="T331">giv-inactive</ta>
            <ta e="T333" id="Seg_5089" s="T332">giv-inactive</ta>
            <ta e="T335" id="Seg_5090" s="T334">accs-inf</ta>
            <ta e="T337" id="Seg_5091" s="T336">giv-active</ta>
            <ta e="T338" id="Seg_5092" s="T337">giv-inactive</ta>
            <ta e="T339" id="Seg_5093" s="T338">giv-active</ta>
            <ta e="T340" id="Seg_5094" s="T339">giv-inactive</ta>
            <ta e="T341" id="Seg_5095" s="T340">giv-inactive</ta>
            <ta e="T343" id="Seg_5096" s="T342">accs-inf</ta>
         </annotation>
         <annotation name="Top" tierref="Top">
            <ta e="T4" id="Seg_5097" s="T0">top.int.concr.</ta>
            <ta e="T10" id="Seg_5098" s="T8">top.int.concr</ta>
            <ta e="T13" id="Seg_5099" s="T12">top.int.concr.</ta>
            <ta e="T15" id="Seg_5100" s="T14">top.int.concr</ta>
            <ta e="T23" id="Seg_5101" s="T22">0.top.int.abstr.</ta>
            <ta e="T24" id="Seg_5102" s="T23">top.int.concr</ta>
            <ta e="T26" id="Seg_5103" s="T25">top.int.concr</ta>
            <ta e="T36" id="Seg_5104" s="T35">0.top.int.concr</ta>
            <ta e="T38" id="Seg_5105" s="T37">0.top.int.concr</ta>
            <ta e="T40" id="Seg_5106" s="T38">top.int.concr</ta>
            <ta e="T44" id="Seg_5107" s="T42">top.int.concr</ta>
            <ta e="T48" id="Seg_5108" s="T47">0.top.int.concr</ta>
            <ta e="T49" id="Seg_5109" s="T48">top.int.concr</ta>
            <ta e="T52" id="Seg_5110" s="T51">top.int.concr</ta>
            <ta e="T55" id="Seg_5111" s="T54">top.int.concr.</ta>
            <ta e="T63" id="Seg_5112" s="T62">0.top.int.concr</ta>
            <ta e="T66" id="Seg_5113" s="T63">top.int.concr.</ta>
            <ta e="T73" id="Seg_5114" s="T72">0.top.int.abstr.</ta>
            <ta e="T75" id="Seg_5115" s="T73">top.int.concr</ta>
            <ta e="T81" id="Seg_5116" s="T79">top.int.concr</ta>
            <ta e="T86" id="Seg_5117" s="T85">top.int.concr</ta>
            <ta e="T88" id="Seg_5118" s="T87">top.int.concr</ta>
            <ta e="T94" id="Seg_5119" s="T92">top.int.concr</ta>
            <ta e="T99" id="Seg_5120" s="T98">top.int.concr</ta>
            <ta e="T105" id="Seg_5121" s="T103">top.int.concr.</ta>
            <ta e="T110" id="Seg_5122" s="T109">top.int.concr.contr.</ta>
            <ta e="T116" id="Seg_5123" s="T115">top.int.concr.contr.</ta>
            <ta e="T120" id="Seg_5124" s="T118">0.top.int.concr</ta>
            <ta e="T123" id="Seg_5125" s="T121">0.top.int.concr</ta>
            <ta e="T124" id="Seg_5126" s="T123">top.int.concr</ta>
            <ta e="T131" id="Seg_5127" s="T130">top.int.concr</ta>
            <ta e="T137" id="Seg_5128" s="T136">0.top.int.concr</ta>
            <ta e="T138" id="Seg_5129" s="T137">0.top.int.concr</ta>
            <ta e="T140" id="Seg_5130" s="T139">top.int.concr</ta>
            <ta e="T143" id="Seg_5131" s="T142">0.top.int.concr</ta>
            <ta e="T148" id="Seg_5132" s="T147">0.top.int.abstr.</ta>
            <ta e="T150" id="Seg_5133" s="T148">0.top.int.concr</ta>
            <ta e="T152" id="Seg_5134" s="T150">top.int.concr</ta>
            <ta e="T162" id="Seg_5135" s="T161">0.top.int.concr</ta>
            <ta e="T164" id="Seg_5136" s="T162">top.int.concr.</ta>
            <ta e="T169" id="Seg_5137" s="T167">top.int.concr.contr.</ta>
            <ta e="T172" id="Seg_5138" s="T171">top.int.concr.contr.</ta>
            <ta e="T176" id="Seg_5139" s="T175">0.top.int.concr</ta>
            <ta e="T177" id="Seg_5140" s="T176">0.top.int.concr</ta>
            <ta e="T179" id="Seg_5141" s="T178">0.top.int.concr</ta>
            <ta e="T189" id="Seg_5142" s="T187">0.top.int.concr</ta>
            <ta e="T192" id="Seg_5143" s="T191">0.top.int.concr</ta>
            <ta e="T196" id="Seg_5144" s="T194">0.top.int.concr</ta>
            <ta e="T197" id="Seg_5145" s="T196">top.int.concr</ta>
            <ta e="T201" id="Seg_5146" s="T200">top.int.concr</ta>
            <ta e="T205" id="Seg_5147" s="T203">top.int.concr</ta>
            <ta e="T208" id="Seg_5148" s="T207">0.top.int.concr</ta>
            <ta e="T211" id="Seg_5149" s="T210">top.int.concr</ta>
            <ta e="T216" id="Seg_5150" s="T215">0.top.int.concr</ta>
            <ta e="T217" id="Seg_5151" s="T216">0.top.int.concr</ta>
            <ta e="T218" id="Seg_5152" s="T217">0.top.int.concr</ta>
            <ta e="T219" id="Seg_5153" s="T218">0.top.int.concr</ta>
            <ta e="T220" id="Seg_5154" s="T219">top.int.concr</ta>
            <ta e="T225" id="Seg_5155" s="T223">top.int.concr</ta>
            <ta e="T230" id="Seg_5156" s="T229">0.top.int.concr</ta>
            <ta e="T231" id="Seg_5157" s="T230">0.top.int.concr</ta>
            <ta e="T234" id="Seg_5158" s="T233">top.int.concr</ta>
            <ta e="T241" id="Seg_5159" s="T240">top.int.concr.contr.</ta>
            <ta e="T246" id="Seg_5160" s="T244">top.int.concr</ta>
            <ta e="T251" id="Seg_5161" s="T250">0.top.int.concr</ta>
            <ta e="T252" id="Seg_5162" s="T251">top.int.concr.contr.</ta>
            <ta e="T256" id="Seg_5163" s="T254">top.int.concr</ta>
            <ta e="T261" id="Seg_5164" s="T258">top.int.concr</ta>
            <ta e="T263" id="Seg_5165" s="T262">top.int.concr</ta>
            <ta e="T270" id="Seg_5166" s="T268">top.int.concr</ta>
            <ta e="T277" id="Seg_5167" s="T276">top.int.concr</ta>
            <ta e="T282" id="Seg_5168" s="T279">top.int.concr</ta>
            <ta e="T289" id="Seg_5169" s="T288">top.int.concr</ta>
            <ta e="T346" id="Seg_5170" s="T296">top.int.concr</ta>
            <ta e="T305" id="Seg_5171" s="T304">0.top.int.concr</ta>
            <ta e="T307" id="Seg_5172" s="T305">top.int.concr</ta>
            <ta e="T309" id="Seg_5173" s="T308">0.top.int.concr</ta>
            <ta e="T310" id="Seg_5174" s="T309">top.int.concr</ta>
            <ta e="T314" id="Seg_5175" s="T313">0.top.int.concr</ta>
            <ta e="T319" id="Seg_5176" s="T317">0.top.int.concr</ta>
            <ta e="T320" id="Seg_5177" s="T319">top.int.concr</ta>
            <ta e="T327" id="Seg_5178" s="T326">0.top.int.concr</ta>
            <ta e="T329" id="Seg_5179" s="T328">top.int.concr.</ta>
            <ta e="T338" id="Seg_5180" s="T336">top.int.concr.contr.</ta>
         </annotation>
         <annotation name="Foc" tierref="Foc">
            <ta e="T8" id="Seg_5181" s="T0">foc.wid</ta>
            <ta e="T11" id="Seg_5182" s="T10">foc.nar</ta>
            <ta e="T14" id="Seg_5183" s="T12">foc.wid</ta>
            <ta e="T18" id="Seg_5184" s="T14">foc.wid</ta>
            <ta e="T23" id="Seg_5185" s="T18">foc.wid</ta>
            <ta e="T25" id="Seg_5186" s="T24">foc.int</ta>
            <ta e="T31" id="Seg_5187" s="T26">foc.int</ta>
            <ta e="T36" id="Seg_5188" s="T31">foc.wid</ta>
            <ta e="T38" id="Seg_5189" s="T36">foc.int</ta>
            <ta e="T41" id="Seg_5190" s="T40">foc.nar</ta>
            <ta e="T45" id="Seg_5191" s="T42">foc.nar</ta>
            <ta e="T48" id="Seg_5192" s="T46">foc.int</ta>
            <ta e="T50" id="Seg_5193" s="T48">foc.nar</ta>
            <ta e="T53" id="Seg_5194" s="T51">foc.wid</ta>
            <ta e="T56" id="Seg_5195" s="T55">foc.nar</ta>
            <ta e="T58" id="Seg_5196" s="T57">foc.nar</ta>
            <ta e="T62" id="Seg_5197" s="T61">foc.nar</ta>
            <ta e="T68" id="Seg_5198" s="T66">foc.nar</ta>
            <ta e="T73" id="Seg_5199" s="T69">foc.wid</ta>
            <ta e="T79" id="Seg_5200" s="T75">foc.int</ta>
            <ta e="T84" id="Seg_5201" s="T83">foc.nar</ta>
            <ta e="T87" id="Seg_5202" s="T86">foc.int</ta>
            <ta e="T92" id="Seg_5203" s="T88">foc.int</ta>
            <ta e="T95" id="Seg_5204" s="T94">foc.nar</ta>
            <ta e="T103" id="Seg_5205" s="T99">foc.int</ta>
            <ta e="T109" id="Seg_5206" s="T103">foc.wid</ta>
            <ta e="T112" id="Seg_5207" s="T110">foc.contr</ta>
            <ta e="T118" id="Seg_5208" s="T116">foc.contr</ta>
            <ta e="T120" id="Seg_5209" s="T118">foc.ver</ta>
            <ta e="T121" id="Seg_5210" s="T120">foc.nar</ta>
            <ta e="T126" id="Seg_5211" s="T125">foc.nar</ta>
            <ta e="T134" id="Seg_5212" s="T130">foc.wid</ta>
            <ta e="T137" id="Seg_5213" s="T136">foc.contr</ta>
            <ta e="T138" id="Seg_5214" s="T137">foc.int</ta>
            <ta e="T141" id="Seg_5215" s="T140">foc.int</ta>
            <ta e="T142" id="Seg_5216" s="T141">foc.nar</ta>
            <ta e="T148" id="Seg_5217" s="T143">foc.wid</ta>
            <ta e="T150" id="Seg_5218" s="T148">foc.wid</ta>
            <ta e="T157" id="Seg_5219" s="T150">foc.wid</ta>
            <ta e="T160" id="Seg_5220" s="T159">foc.nar</ta>
            <ta e="T167" id="Seg_5221" s="T162">foc.wid</ta>
            <ta e="T170" id="Seg_5222" s="T169">foc.contr</ta>
            <ta e="T174" id="Seg_5223" s="T172">foc.contr</ta>
            <ta e="T175" id="Seg_5224" s="T174">foc.nar</ta>
            <ta e="T177" id="Seg_5225" s="T176">foc.int</ta>
            <ta e="T178" id="Seg_5226" s="T177">foc.nar</ta>
            <ta e="T183" id="Seg_5227" s="T180">foc.nar</ta>
            <ta e="T186" id="Seg_5228" s="T183">foc.nar</ta>
            <ta e="T190" id="Seg_5229" s="T189">foc.nar</ta>
            <ta e="T193" id="Seg_5230" s="T192">foc.nar</ta>
            <ta e="T200" id="Seg_5231" s="T196">foc.wid</ta>
            <ta e="T202" id="Seg_5232" s="T201">foc.nar</ta>
            <ta e="T208" id="Seg_5233" s="T205">foc.ver</ta>
            <ta e="T212" id="Seg_5234" s="T211">foc.nar</ta>
            <ta e="T215" id="Seg_5235" s="T214">foc.nar</ta>
            <ta e="T217" id="Seg_5236" s="T216">foc.int</ta>
            <ta e="T218" id="Seg_5237" s="T217">foc.ver</ta>
            <ta e="T219" id="Seg_5238" s="T218">foc.int</ta>
            <ta e="T222" id="Seg_5239" s="T220">foc.int</ta>
            <ta e="T226" id="Seg_5240" s="T225">foc.int</ta>
            <ta e="T230" id="Seg_5241" s="T227">foc.int</ta>
            <ta e="T233" id="Seg_5242" s="T230">foc.int</ta>
            <ta e="T240" id="Seg_5243" s="T234">foc.int</ta>
            <ta e="T244" id="Seg_5244" s="T240">foc.wid</ta>
            <ta e="T248" id="Seg_5245" s="T246">foc.int</ta>
            <ta e="T250" id="Seg_5246" s="T249">foc.contr</ta>
            <ta e="T252" id="Seg_5247" s="T251">foc.contr</ta>
            <ta e="T258" id="Seg_5248" s="T254">foc.wid</ta>
            <ta e="T262" id="Seg_5249" s="T261">foc.int</ta>
            <ta e="T268" id="Seg_5250" s="T263">foc.int</ta>
            <ta e="T274" id="Seg_5251" s="T270">foc.nar</ta>
            <ta e="T279" id="Seg_5252" s="T277">foc.int</ta>
            <ta e="T288" id="Seg_5253" s="T283">foc.wid</ta>
            <ta e="T296" id="Seg_5254" s="T289">foc.int</ta>
            <ta e="T303" id="Seg_5255" s="T346">foc.nar</ta>
            <ta e="T305" id="Seg_5256" s="T303">foc.wid</ta>
            <ta e="T308" id="Seg_5257" s="T307">foc.int</ta>
            <ta e="T309" id="Seg_5258" s="T308">foc.int</ta>
            <ta e="T312" id="Seg_5259" s="T310">foc.int</ta>
            <ta e="T314" id="Seg_5260" s="T312">foc.int</ta>
            <ta e="T319" id="Seg_5261" s="T316">foc.int</ta>
            <ta e="T324" id="Seg_5262" s="T322">foc.int</ta>
            <ta e="T327" id="Seg_5263" s="T326">foc.int</ta>
            <ta e="T335" id="Seg_5264" s="T333">foc.nar</ta>
            <ta e="T343" id="Seg_5265" s="T341">foc.nar</ta>
         </annotation>
         <annotation name="BOR" tierref="TIE0" />
         <annotation name="BOR-Phon" tierref="TIE2" />
         <annotation name="BOR-Morph" tierref="TIE3" />
         <annotation name="CS" tierref="TIE4" />
         <annotation name="fe" tierref="fe">
            <ta e="T8" id="Seg_5266" s="T0">A long time ago, it is said, when the earth and the sky were created, the partridge lived in the tundra.</ta>
            <ta e="T12" id="Seg_5267" s="T8">One year turned out to be very blizzardy and stormy.</ta>
            <ta e="T18" id="Seg_5268" s="T12">In winter it rained, the whole snow turned to ice. </ta>
            <ta e="T23" id="Seg_5269" s="T18">There was not a single thaw hole.</ta>
            <ta e="T31" id="Seg_5270" s="T23">The partridges were starving, they flew in the direction of the forest to search for some food.</ta>
            <ta e="T36" id="Seg_5271" s="T31">The eldest partridge was their leader.</ta>
            <ta e="T51" id="Seg_5272" s="T36">They reached the forest, there were no thawed patches to find, not even the treetops of the willow bushes were visible, everything was covered with snow, only the trees were black. </ta>
            <ta e="T53" id="Seg_5273" s="T51">Their leader advises:</ta>
            <ta e="T57" id="Seg_5274" s="T53">"Well, now we are not saved.</ta>
            <ta e="T61" id="Seg_5275" s="T57">Does it even matter where we die?</ta>
            <ta e="T63" id="Seg_5276" s="T61">That is how I think:</ta>
            <ta e="T73" id="Seg_5277" s="T63">Once I saw a place, there was a big river there.</ta>
            <ta e="T79" id="Seg_5278" s="T73">That river never gets covered by snow during a snowstorm.</ta>
            <ta e="T85" id="Seg_5279" s="T79">On some spots there are iceholes during the whole winter. </ta>
            <ta e="T92" id="Seg_5280" s="T85">Along the river willow bushes grow, the rocks are covered with bog-rosemary, berries and grass. </ta>
            <ta e="T97" id="Seg_5281" s="T92">The pikes live in that river.</ta>
            <ta e="T103" id="Seg_5282" s="T97">But the pikes won't let us there.</ta>
            <ta e="T118" id="Seg_5283" s="T103">A long time ago they had an agreement with the partridges: The partridges would not come to the land of the pikes and the pikes [would not come] to the land of the partridges. </ta>
            <ta e="T123" id="Seg_5284" s="T118">If they remember, they won't give us [the land] amicably.</ta>
            <ta e="T128" id="Seg_5285" s="T123">Then we can go there only in war."</ta>
            <ta e="T129" id="Seg_5286" s="T128">The partridges:</ta>
            <ta e="T130" id="Seg_5287" s="T129">"That's right.</ta>
            <ta e="T134" id="Seg_5288" s="T130">Do we have to die here?!</ta>
            <ta e="T137" id="Seg_5289" s="T134">We better fight.</ta>
            <ta e="T139" id="Seg_5290" s="T137">Let's go!", they said.</ta>
            <ta e="T143" id="Seg_5291" s="T139">The partridges fly, they reach the river. </ta>
            <ta e="T148" id="Seg_5292" s="T143">There is apparently enough food available.</ta>
            <ta e="T150" id="Seg_5293" s="T148">They sat down and ate. </ta>
            <ta e="T157" id="Seg_5294" s="T150">Out of an icehole the head of the pikes' leader came out:</ta>
            <ta e="T162" id="Seg_5295" s="T157">"Hey, partridges, why did you come to this place?</ta>
            <ta e="T174" id="Seg_5296" s="T162">A long time ago we had an agreement: we don't come to your land, neither you [come] to our.</ta>
            <ta e="T180" id="Seg_5297" s="T174">Why did you come, don't wait, go away immediately!</ta>
            <ta e="T196" id="Seg_5298" s="T180">I won't let you eat a leave of the willow bush, no top of the bug-rosemary, I won't leat you grass, I even won't let you dig in the gravel."</ta>
            <ta e="T200" id="Seg_5299" s="T196">The leader of the partridges replies on that: </ta>
            <ta e="T203" id="Seg_5300" s="T200">"We won't go away amicably.</ta>
            <ta e="T208" id="Seg_5301" s="T203">Even if we have to take this place in war!"</ta>
            <ta e="T210" id="Seg_5302" s="T208">The leader of the pikes: </ta>
            <ta e="T213" id="Seg_5303" s="T210">"I won't give you this place amicably. </ta>
            <ta e="T217" id="Seg_5304" s="T213">You'll get it only if you kill all the pikes!</ta>
            <ta e="T218" id="Seg_5305" s="T217">We will fight.</ta>
            <ta e="T223" id="Seg_5306" s="T218">Wait — I'll gather my people!", he says. </ta>
            <ta e="T233" id="Seg_5307" s="T223">The pikes' leader turned away, made "splat" with his tail and went to gather his people. </ta>
            <ta e="T240" id="Seg_5308" s="T233">The pike brought his whole swarm, so that it blackened the icehole completely.</ta>
            <ta e="T244" id="Seg_5309" s="T240">The partridges are standing on the shore ready to start. </ta>
            <ta e="T248" id="Seg_5310" s="T244">The leader of the partridges says to his people: </ta>
            <ta e="T254" id="Seg_5311" s="T248">"Shoot in the pikes' heads, if [you shoot] in their backs, they don't die!"</ta>
            <ta e="T258" id="Seg_5312" s="T254">The partridge army shot its arrows.</ta>
            <ta e="T276" id="Seg_5313" s="T258">Having heard the sound of the bowstrings, the pikes swam into the middle of the river, and the partridges' arrows stroke just into the pikes' backs.</ta>
            <ta e="T279" id="Seg_5314" s="T276">The pikes turned back.</ta>
            <ta e="T288" id="Seg_5315" s="T279">Until the partridges got new arrows, the pikes were shooting, targeting the partridges' hearts.</ta>
            <ta e="T303" id="Seg_5316" s="T288">The partridges, dodging, flew to the middle and the pikes' arrows just stroke into the muscles of their legs. </ta>
            <ta e="T305" id="Seg_5317" s="T303">So they fought.</ta>
            <ta e="T309" id="Seg_5318" s="T305">No one of them could kill the other one, no one could win. </ta>
            <ta e="T314" id="Seg_5319" s="T309">As their arrows ran out, they stopped fighting.</ta>
            <ta e="T316" id="Seg_5320" s="T314">The pikes' leader:</ta>
            <ta e="T319" id="Seg_5321" s="T316">"Let us stop fighting. </ta>
            <ta e="T328" id="Seg_5322" s="T319">Fly, wherever you want, eat, whatever you want!", he says.</ta>
            <ta e="T344" id="Seg_5323" s="T328">Since then the partridges' arrows in the backs of the pikes turned to forked bones, and the pikes' arrows in the legs of the partridges turned to sinewy bones.</ta>
            <ta e="T345" id="Seg_5324" s="T344">The end.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T8" id="Seg_5325" s="T0">Vor langer Zeit, sagt man, als Erde und Himmel entstanden, lebte das Rebhuhn in der Tundra.</ta>
            <ta e="T12" id="Seg_5326" s="T8">Eines Jahres wurde es schneestürmisch. </ta>
            <ta e="T18" id="Seg_5327" s="T12">Im Winter regnete es, der ganze Schnee wurde zu Eis.</ta>
            <ta e="T23" id="Seg_5328" s="T18">Es gab nicht ein Loch in der Schneedecke.</ta>
            <ta e="T31" id="Seg_5329" s="T23">Die Rebhühner hungerten, sie flogen in Richtung des Waldes, um nach Nahrung zu suchen.</ta>
            <ta e="T36" id="Seg_5330" s="T31">Das älteste Rebhuhn war ihr Anführer.</ta>
            <ta e="T51" id="Seg_5331" s="T36">Sie kamen zum Wald, nirgendwo war ein Loch in der Schneedecke zu sehen, nicht einmal die Wipfel der Weidensträuche waren zu bemerken, alles war zugeweht, nur die Bäume waren schwarz.</ta>
            <ta e="T53" id="Seg_5332" s="T51">Ihr Anführer rät:</ta>
            <ta e="T57" id="Seg_5333" s="T53">"Jetzt sind wir doch nicht gerettet.</ta>
            <ta e="T61" id="Seg_5334" s="T57">Ist es nicht ganz egal, wo wir sterben?</ta>
            <ta e="T63" id="Seg_5335" s="T61">Ich denke so:</ta>
            <ta e="T73" id="Seg_5336" s="T63">Irgendwann einmal sah ich einen Ort, dort gibt es einen großen Fluss.</ta>
            <ta e="T79" id="Seg_5337" s="T73">Dieser Fluss wird in keinem Schneesturm zugeweht.</ta>
            <ta e="T85" id="Seg_5338" s="T79">An einigen Stellen sind während des Winters Eislöcher.</ta>
            <ta e="T92" id="Seg_5339" s="T85">An seinem Ufer wachsen Weiden, die Felsen sind ganz mit Rosmarinheide, Beeren und Gras bedeckt.</ta>
            <ta e="T97" id="Seg_5340" s="T92">In jenem Fluss wohnen die Hechte.</ta>
            <ta e="T103" id="Seg_5341" s="T97">Die Hechte werden uns nur nicht dorthin lassen.</ta>
            <ta e="T118" id="Seg_5342" s="T103">Vor langer Zeit hatten sie einen Vertrag mit den Rebhühnern: Die Rebhühner kommen nicht in das Land der Hechte und die Hechte nicht in das Land der Rebhühner.</ta>
            <ta e="T123" id="Seg_5343" s="T118">Wenn sie sich erinnern, werden sie [das Land] nicht im Guten geben.</ta>
            <ta e="T128" id="Seg_5344" s="T123">Dann können wir nur im Krieg dorthin gehen."</ta>
            <ta e="T129" id="Seg_5345" s="T128">Die Rebhühner:</ta>
            <ta e="T130" id="Seg_5346" s="T129">"Das stimmt.</ta>
            <ta e="T134" id="Seg_5347" s="T130">Sollen wir hier so sterben?!</ta>
            <ta e="T137" id="Seg_5348" s="T134">Stattdessen kämpfen wir.</ta>
            <ta e="T139" id="Seg_5349" s="T137">Gehen wir!", sagten sie.</ta>
            <ta e="T143" id="Seg_5350" s="T139">Die Rebhühner fliegen los, sie kommen an den Fluss.</ta>
            <ta e="T148" id="Seg_5351" s="T143">Es ist offensichtlich genug Nahrung vorhanden.</ta>
            <ta e="T150" id="Seg_5352" s="T148">Sie setzten sich und fraßen.</ta>
            <ta e="T157" id="Seg_5353" s="T150">Aus einem Eisloch kam der Kopf des Anführers der Hechte:</ta>
            <ta e="T162" id="Seg_5354" s="T157">"Na, Rebhühner, warum seid ihr an diesen Ort gekommen?</ta>
            <ta e="T174" id="Seg_5355" s="T162">Vor langer Zeit hatten wir einen Vertrag: In euer Land kommen wir nicht — und ihr nicht in unseres.</ta>
            <ta e="T180" id="Seg_5356" s="T174">Warum kamt ihr, wartet nicht, geht sofort weg von hier!</ta>
            <ta e="T196" id="Seg_5357" s="T180">Ich werde euch kein Blatt vom Weidenstrauch, keine Spitze von der Rosmarinheide essen lassen, ich lasse euch kein Gras essen, ich werde euch nicht einmal im Kies graben lassen!"</ta>
            <ta e="T200" id="Seg_5358" s="T196">Darauf sagt der Anführer der Rebhühner:</ta>
            <ta e="T203" id="Seg_5359" s="T200">"Wir gehen nicht im Guten.</ta>
            <ta e="T208" id="Seg_5360" s="T203">Und wenn wir auch diesen Platz im Krieg nehmen!"</ta>
            <ta e="T210" id="Seg_5361" s="T208">Der Anführer der Hechte:</ta>
            <ta e="T213" id="Seg_5362" s="T210">"Ich gebe ihn [den Platz] nicht im Guten her.</ta>
            <ta e="T217" id="Seg_5363" s="T213">Nur wenn ihr die Hechte ganz getötet habt, könnt ihr ihn [den Platz] nehmen.</ta>
            <ta e="T218" id="Seg_5364" s="T217">Wir werden kämpfen.</ta>
            <ta e="T223" id="Seg_5365" s="T218">Warte — ich sammele meine Leute!", sagt er.</ta>
            <ta e="T233" id="Seg_5366" s="T223">Der Anführer der Hechte dreht sich um, machte "platsch" mit seinem Schwanz und ging seine Leute sammeln.</ta>
            <ta e="T240" id="Seg_5367" s="T233">Der Hecht holte seinen ganzen Schwarm und machte das ganze Eisloch schwarz.</ta>
            <ta e="T244" id="Seg_5368" s="T240">Die Rebhühner stehen fertig auf dem Ufer.</ta>
            <ta e="T248" id="Seg_5369" s="T244">Der Anführer der Rebhühner sagt zu seinen Leuten:</ta>
            <ta e="T254" id="Seg_5370" s="T248">"Schießt den Hechten in den Kopf — in den Rücken, dann sterben sie nicht!"</ta>
            <ta e="T258" id="Seg_5371" s="T254">Das Heer der Rebhühner ließ seine Pfeile los.</ta>
            <ta e="T276" id="Seg_5372" s="T258">Als das Geräusch der Bogensehnen zu hören war, drehten sich die Hechte um und schwammen in die Mitte, und die Pfeile der Rebhühner trafen nur die Rückenteile der Hechte.</ta>
            <ta e="T279" id="Seg_5373" s="T276">Die Hechte drehten sich um und kamen zurück.</ta>
            <ta e="T288" id="Seg_5374" s="T279">Bis die Rebhühner Pfeile besorgt hatten, schossen die Hechte und zielten auf die Herzen der Rebhühner.</ta>
            <ta e="T303" id="Seg_5375" s="T288">Die Rebhühner drehten sich um und flogen in die Mitte, die Pfeile der Hechte trafen nur die (Bein-)Muskeln der Rebhühner.</ta>
            <ta e="T305" id="Seg_5376" s="T303">So kämpften sie.</ta>
            <ta e="T309" id="Seg_5377" s="T305">Keiner kann den anderen töten, den anderen besiegen.</ta>
            <ta e="T314" id="Seg_5378" s="T309">Als die Pfeile zur Neige gingen, hörten sie auf zu kämpfen.</ta>
            <ta e="T316" id="Seg_5379" s="T314">Der Anführer der Hechte:</ta>
            <ta e="T319" id="Seg_5380" s="T316">"Lasst uns jetzt aufhören zu kämpfen.</ta>
            <ta e="T328" id="Seg_5381" s="T319">Fliegt, wohin ihr wollt, esst, wo ihr wollt!", sagt er.</ta>
            <ta e="T344" id="Seg_5382" s="T328">Von da an wurden die Pfeile der Rebhühner in den Rücken der Hechte zu gespaltenen Knochen, und die Pfeile der Hechte in den Muskeln der Rebhühner wurden zu sehnigen Knochen.</ta>
            <ta e="T345" id="Seg_5383" s="T344">Ende.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T8" id="Seg_5384" s="T0">Вот в давние времена, когда только создавались земля и небо, куропатка в тундре жила, говорят.</ta>
            <ta e="T12" id="Seg_5385" s="T8">Один год выдался вьюжный, пуржистый.</ta>
            <ta e="T18" id="Seg_5386" s="T12">Зимой пошел дождь, весь снег заледенел.</ta>
            <ta e="T23" id="Seg_5387" s="T18">Ни одной проталины не было.</ta>
            <ta e="T31" id="Seg_5388" s="T23">Куропатки проголодались, полетели они искать пищу в сторону леса.</ta>
            <ta e="T36" id="Seg_5389" s="T31">Самая старая куропатка была у них вожаком.</ta>
            <ta e="T51" id="Seg_5390" s="T36">Долетели до леса, и там нигде проталин не видно, даже верхушки тальника не проглядывают, все замело, только деревья чернеют.</ta>
            <ta e="T53" id="Seg_5391" s="T51">Их вожак советует:</ta>
            <ta e="T57" id="Seg_5392" s="T53">"Теперь нам не спастись.</ta>
            <ta e="T61" id="Seg_5393" s="T57">Не все ли равно, где пропадать?</ta>
            <ta e="T63" id="Seg_5394" s="T61">Вот как думаю.</ta>
            <ta e="T73" id="Seg_5395" s="T63">Когда-то заприметил я одно место, там есть большая река.</ta>
            <ta e="T79" id="Seg_5396" s="T73">Ту реку ни в какую пургу не заносит.</ta>
            <ta e="T85" id="Seg_5397" s="T79">В некоторых местах всю зиму сохраняются полыньи.</ta>
            <ta e="T92" id="Seg_5398" s="T85">По берегам растет ивняк, скалы покрыты багульником, ягодами и травой.</ta>
            <ta e="T97" id="Seg_5399" s="T92">В той реке живут щуки.</ta>
            <ta e="T103" id="Seg_5400" s="T97">Только щуки нас туда не подпустят.</ta>
            <ta e="T118" id="Seg_5401" s="T103">С давних времен есть у них клятвенный уговор с куропатками: куропатки не пойдут в края щук, а щуки — на земли куропаток.</ta>
            <ta e="T123" id="Seg_5402" s="T118">Если помнят, добром не уступят.</ta>
            <ta e="T128" id="Seg_5403" s="T123">Остается нам тогда пойти войной."</ta>
            <ta e="T129" id="Seg_5404" s="T128">Куропатки:</ta>
            <ta e="T130" id="Seg_5405" s="T129">"Правда.</ta>
            <ta e="T134" id="Seg_5406" s="T130">Не пропадать же нам тут?!</ta>
            <ta e="T137" id="Seg_5407" s="T134">Лучше повоюем.</ta>
            <ta e="T139" id="Seg_5408" s="T137">Полетели!", сказали</ta>
            <ta e="T143" id="Seg_5409" s="T139">Полетели куропатки, достигли реки.</ta>
            <ta e="T148" id="Seg_5410" s="T143">Тут корма вдоволь, оказывается.</ta>
            <ta e="T150" id="Seg_5411" s="T148">Сели, кормятся.</ta>
            <ta e="T157" id="Seg_5412" s="T150">Из полыньи высунулась голова вожака щук:</ta>
            <ta e="T162" id="Seg_5413" s="T157">"Эй, куропатки, почему пришли в эти края?</ta>
            <ta e="T174" id="Seg_5414" s="T162">В давние времена был у нас клятвенный уговор: мы не занимаем ваши края, а вы — наши земли.</ta>
            <ta e="T180" id="Seg_5415" s="T174">Зачем пришли, убирайтесь, сейчас же уходите отсюда!</ta>
            <ta e="T196" id="Seg_5416" s="T180">Не дам вам отведать ни листьев ивняка, ни верхушек кустарника, ни травы, не дам копаться и в галечнике!"</ta>
            <ta e="T200" id="Seg_5417" s="T196">На это вожак куропаток говорит:</ta>
            <ta e="T203" id="Seg_5418" s="T200">"Мы добром не уйдем.</ta>
            <ta e="T208" id="Seg_5419" s="T203">Хоть войной, а возьмем этот край!"</ta>
            <ta e="T210" id="Seg_5420" s="T208">Вожак щук:</ta>
            <ta e="T213" id="Seg_5421" s="T210">"Я добром не уступлю.</ta>
            <ta e="T217" id="Seg_5422" s="T213">Только перебив всех щук, возьмете.</ta>
            <ta e="T218" id="Seg_5423" s="T217">Будем воевать.</ta>
            <ta e="T223" id="Seg_5424" s="T218">Обождите — я соберу своих людей!", говорит.</ta>
            <ta e="T233" id="Seg_5425" s="T223">Вожак щук повернулся, "плюх!" — ударил хвостом и уплыл собирать своих.</ta>
            <ta e="T240" id="Seg_5426" s="T233">Щука привела весь свой косяк, так что вся полынья почернела.</ta>
            <ta e="T244" id="Seg_5427" s="T240">Куропатки наготове на берегу стоят.</ta>
            <ta e="T248" id="Seg_5428" s="T244">Вожак куропаток своим говорит:</ta>
            <ta e="T254" id="Seg_5429" s="T248">"Стрелять надо только в головы щук, если в спины — они не гибнут!"</ta>
            <ta e="T258" id="Seg_5430" s="T254">Войско куропаток пустило стрелы.</ta>
            <ta e="T276" id="Seg_5431" s="T258">Услышав щелчок тетивы лука, щуки, увертываясь, устремились к середине реки, и стрелы куропаток вонзались только в спины да в спины щук.</ta>
            <ta e="T279" id="Seg_5432" s="T276">Щуки повернулись и поплыли обратно.</ta>
            <ta e="T288" id="Seg_5433" s="T279">Пока куропатки доставали стрелы, щуки стреляли, целясь в сердца куропаток.</ta>
            <ta e="T303" id="Seg_5434" s="T288">Куропатки, увертываясь, вспархивали над землей, и стрелы щук вонзались только в мускулы ног куропаток.</ta>
            <ta e="T305" id="Seg_5435" s="T303">Так воевали.</ta>
            <ta e="T309" id="Seg_5436" s="T305">Никто из них не мог побить, одолеть другого.</ta>
            <ta e="T314" id="Seg_5437" s="T309">Истратив все стрелы, кончили воевать.</ta>
            <ta e="T316" id="Seg_5438" s="T314">Вожак щук:</ta>
            <ta e="T319" id="Seg_5439" s="T316">"Давайте кончим воевать.</ta>
            <ta e="T328" id="Seg_5440" s="T319">Вы летайте — куда угодно, кормитесь — где угодно!", говорит.</ta>
            <ta e="T344" id="Seg_5441" s="T328">С тех пор стрелы куропаток превратились в вилообразные кости на спинах щук, а стрелы щук в мускулах ног куропаток превратились в сросшиеся жилистые кости.</ta>
            <ta e="T345" id="Seg_5442" s="T344">Конец.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T8" id="Seg_5443" s="T0">Вот в давние времена, когда только создавались земля и небо, куропатка в тундре жила, говорят.</ta>
            <ta e="T12" id="Seg_5444" s="T8">Один год выдался вьюжный, пуржистый.</ta>
            <ta e="T18" id="Seg_5445" s="T12">Зимой пошел дождь, весь снег заледенел.</ta>
            <ta e="T23" id="Seg_5446" s="T18">Ни одной проталины нет.</ta>
            <ta e="T31" id="Seg_5447" s="T23">Куропатки проголодались, полетели они искать пищу в сторону леса.</ta>
            <ta e="T36" id="Seg_5448" s="T31">Самая старая куропатка была у них вожаком.</ta>
            <ta e="T51" id="Seg_5449" s="T36">Долетели до леса, и там нигде проталин не видно, даже верхушки тальника не проглядывают, все замело, только деревья чернеют.</ta>
            <ta e="T53" id="Seg_5450" s="T51">Вожак советует.</ta>
            <ta e="T57" id="Seg_5451" s="T53">— Теперь нам не спастись.</ta>
            <ta e="T61" id="Seg_5452" s="T57">Не все ли равно, где пропадать?</ta>
            <ta e="T63" id="Seg_5453" s="T61">Вот как думаю.</ta>
            <ta e="T73" id="Seg_5454" s="T63">[Когда-то] заприметил я одно место, там есть большая река.</ta>
            <ta e="T79" id="Seg_5455" s="T73">Ту реку ни в какую пургу не заносит.</ta>
            <ta e="T85" id="Seg_5456" s="T79">В некоторых местах всю зиму сохраняются полыньи.</ta>
            <ta e="T92" id="Seg_5457" s="T85">По берегам растет ивняк, горы покрыты кустарником и багульником, голубикой и травой.</ta>
            <ta e="T97" id="Seg_5458" s="T92">В той реке живут щуки.</ta>
            <ta e="T103" id="Seg_5459" s="T97">Только щуки нас туда не подпустят.</ta>
            <ta e="T118" id="Seg_5460" s="T103">С давних времен есть у них клятвенный уговор с куропатками: куропатки не пойдут в края щук, а щуки — на земли куропаток.</ta>
            <ta e="T123" id="Seg_5461" s="T118">Если помнят уговор, добром не уступят.</ta>
            <ta e="T128" id="Seg_5462" s="T123">Остается нам тогда пойти войной.</ta>
            <ta e="T129" id="Seg_5463" s="T128">Куропатки сказали:</ta>
            <ta e="T130" id="Seg_5464" s="T129">— Правда.</ta>
            <ta e="T134" id="Seg_5465" s="T130">Не пропадать же нам тут?!</ta>
            <ta e="T137" id="Seg_5466" s="T134">Лучше повоюем.</ta>
            <ta e="T139" id="Seg_5467" s="T137">Полетели! — сказали</ta>
            <ta e="T143" id="Seg_5468" s="T139">Полетели куропатки, достигли реки.</ta>
            <ta e="T148" id="Seg_5469" s="T143">Тут корма вдоволь, оказывается.</ta>
            <ta e="T150" id="Seg_5470" s="T148">Сели, кормятся.</ta>
            <ta e="T157" id="Seg_5471" s="T150">Из полыньи высунулась голова вожака щук:</ta>
            <ta e="T162" id="Seg_5472" s="T157">— Эй, куропатки, почему пришли в эти края?</ta>
            <ta e="T174" id="Seg_5473" s="T162">В давние времена был у нас клятвенный уговор: мы не занимаем ваши края, а вы — наши земли.</ta>
            <ta e="T180" id="Seg_5474" s="T174">Зачем пришли, убирайтесь, сейчас же уходите отсюда!</ta>
            <ta e="T196" id="Seg_5475" s="T180">Не дам вам отведать ни листьев ивняка, ни верхушек кустарника, ни травы, не дам копаться и в галечнике!</ta>
            <ta e="T200" id="Seg_5476" s="T196">На это вожак куропаток говорит.</ta>
            <ta e="T203" id="Seg_5477" s="T200">— Мы добром не уйдем.</ta>
            <ta e="T208" id="Seg_5478" s="T203">Хоть войной, а возьмем этот край!</ta>
            <ta e="T210" id="Seg_5479" s="T208">Вожак щук:</ta>
            <ta e="T213" id="Seg_5480" s="T210">— Я добром не уступлю.</ta>
            <ta e="T217" id="Seg_5481" s="T213">Только перебив всех щук, возьмете.</ta>
            <ta e="T218" id="Seg_5482" s="T217">Будем воевать.</ta>
            <ta e="T223" id="Seg_5483" s="T218">Обождите — я соберу своих людей? — говорит.</ta>
            <ta e="T233" id="Seg_5484" s="T223">Вожак щук повернулся, "плюх!" — ударил хвостом и уплыл собирать своих.</ta>
            <ta e="T240" id="Seg_5485" s="T233">Вожак щук привел весь свой косяк, так что вся полынья почернела.</ta>
            <ta e="T244" id="Seg_5486" s="T240">Куропатки наготове на берегу стоят.</ta>
            <ta e="T248" id="Seg_5487" s="T244">Вожак куропаток своим говорит:</ta>
            <ta e="T254" id="Seg_5488" s="T248">— Стрелять надо только в головы щук, если в спины — они не гибнут!</ta>
            <ta e="T258" id="Seg_5489" s="T254">Войско куропаток пустило стрелы.</ta>
            <ta e="T276" id="Seg_5490" s="T258">Услышав щелчок тетивы лука, щуки, увертываясь, устремились к середине реки, и стрелы куропаток вонзались только в спины да в спины щук.</ta>
            <ta e="T279" id="Seg_5491" s="T276">Щуки подплыли обратно к берегу.</ta>
            <ta e="T288" id="Seg_5492" s="T279">Пока куропатки доставали стрелы, щуки стреляли, целясь в сердца куропаток.</ta>
            <ta e="T303" id="Seg_5493" s="T288">Куропатки, увертываясь, вспархивали над землей, и стрелы щук вонзались только в мускулы ног куропаток.</ta>
            <ta e="T305" id="Seg_5494" s="T303">Так воевали.</ta>
            <ta e="T309" id="Seg_5495" s="T305">Никто из них не мог побить, одолеть другого.</ta>
            <ta e="T314" id="Seg_5496" s="T309">Истратив все стрелы, кончили воевать.</ta>
            <ta e="T316" id="Seg_5497" s="T314">Вожак щук:</ta>
            <ta e="T319" id="Seg_5498" s="T316">— Давайте кончим воевать.</ta>
            <ta e="T328" id="Seg_5499" s="T319">Вы летайте — куда угодно, кормитесь — где угодно! — сказал.</ta>
            <ta e="T344" id="Seg_5500" s="T328">С тех пор стрелы куропаток превратились в вилообразные кости на спинах щук, а стрелы щук в мускулах ног куропаток превратились в сросшиеся жилистые кости.</ta>
            <ta e="T345" id="Seg_5501" s="T344">Конец.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T217" id="Seg_5502" s="T213">[DCh]: "Ku͡okaːɨnɨ" is probably some kind of mistake and shall mean "Ku͡okaːnɨ".</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T346" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg"
                          name="gg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top"
                          name="Top"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc"
                          name="Foc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
