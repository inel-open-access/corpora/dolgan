<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDACF0B538-EAA7-4C73-B032-B3D20B6587C2">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>AkEE_19900810_ReindeerMouse_flk</transcription-name>
         <referenced-file url="AkEE_19900810_ReindeerMouse_flk.wav" />
         <referenced-file url="AkEE_19900810_ReindeerMouse_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\flk\AkEE_19900810_ReindeerMouse_flk\AkEE_19900810_ReindeerMouse_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">131</ud-information>
            <ud-information attribute-name="# HIAT:w">88</ud-information>
            <ud-information attribute-name="# e">88</ud-information>
            <ud-information attribute-name="# HIAT:u">20</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="AkEE">
            <abbreviation>AkEE</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" time="0.0" type="appl" />
         <tli id="T2" time="0.981" type="appl" />
         <tli id="T3" time="1.962" type="appl" />
         <tli id="T4" time="2.943" type="appl" />
         <tli id="T5" time="3.5784000000000002" type="appl" />
         <tli id="T6" time="4.2138" type="appl" />
         <tli id="T7" time="4.8492" type="appl" />
         <tli id="T8" time="5.4846" type="appl" />
         <tli id="T9" time="6.119999999999999" type="appl" />
         <tli id="T10" time="6.12" type="appl" />
         <tli id="T11" time="6.878333333333333" type="appl" />
         <tli id="T12" time="7.636666666666667" type="appl" />
         <tli id="T13" time="8.899964089108739" />
         <tli id="T14" time="9.31" type="appl" />
         <tli id="T15" time="10.225" type="appl" />
         <tli id="T16" time="11.253332685837018" />
         <tli id="T17" time="11.824" type="appl" />
         <tli id="T18" time="12.508000000000001" type="appl" />
         <tli id="T19" time="13.192" type="appl" />
         <tli id="T20" time="13.876000000000001" type="appl" />
         <tli id="T21" time="14.56" type="appl" />
         <tli id="T22" time="15.31" type="appl" />
         <tli id="T23" time="16.06" type="appl" />
         <tli id="T24" time="17.493262748929837" />
         <tli id="T25" time="18.613999999999997" type="appl" />
         <tli id="T26" time="20.793249433655546" />
         <tli id="T27" time="21.175" type="appl" />
         <tli id="T28" time="21.932" type="appl" />
         <tli id="T29" time="22.689" type="appl" />
         <tli id="T30" time="23.446" type="appl" />
         <tli id="T31" time="24.203" type="appl" />
         <tli id="T32" time="25.519897028545504" />
         <tli id="T33" time="25.736666666666668" type="appl" />
         <tli id="T34" time="26.513333333333335" type="appl" />
         <tli id="T35" time="27.29" type="appl" />
         <tli id="T36" time="28.066666666666666" type="appl" />
         <tli id="T37" time="28.843333333333334" type="appl" />
         <tli id="T38" time="29.62" type="appl" />
         <tli id="T39" time="30.304666666666666" type="appl" />
         <tli id="T40" time="30.989333333333335" type="appl" />
         <tli id="T41" time="31.82066587610491" />
         <tli id="T42" time="32.88333333333333" type="appl" />
         <tli id="T43" time="34.092666666666666" type="appl" />
         <tli id="T44" time="35.302" type="appl" />
         <tli id="T45" time="36.51133333333333" type="appl" />
         <tli id="T46" time="37.720666666666666" type="appl" />
         <tli id="T47" time="38.93" type="appl" />
         <tli id="T48" time="39.5875" type="appl" />
         <tli id="T49" time="40.245000000000005" type="appl" />
         <tli id="T50" time="40.9025" type="appl" />
         <tli id="T51" time="41.56" type="appl" />
         <tli id="T52" time="42.38333333333333" type="appl" />
         <tli id="T53" time="43.20666666666667" type="appl" />
         <tli id="T54" time="44.16333417303993" />
         <tli id="T55" time="44.62885714285714" type="appl" />
         <tli id="T56" time="45.227714285714285" type="appl" />
         <tli id="T57" time="45.82657142857143" type="appl" />
         <tli id="T58" time="46.425428571428576" type="appl" />
         <tli id="T59" time="47.02428571428572" type="appl" />
         <tli id="T60" time="47.62314285714286" type="appl" />
         <tli id="T61" time="48.57313734325563" />
         <tli id="T62" time="49.11533333333333" type="appl" />
         <tli id="T63" time="50.00866666666667" type="appl" />
         <tli id="T64" time="50.902" type="appl" />
         <tli id="T65" time="52.21" type="appl" />
         <tli id="T66" time="52.898" type="appl" />
         <tli id="T67" time="53.586" type="appl" />
         <tli id="T68" time="54.274" type="appl" />
         <tli id="T69" time="54.961999999999996" type="appl" />
         <tli id="T70" time="55.61666361049423" />
         <tli id="T71" time="56.16" type="appl" />
         <tli id="T72" time="56.67" type="appl" />
         <tli id="T73" time="57.18" type="appl" />
         <tli id="T74" time="57.97714285714286" type="appl" />
         <tli id="T75" time="58.77428571428572" type="appl" />
         <tli id="T76" time="59.57142857142857" type="appl" />
         <tli id="T77" time="60.36857142857143" type="appl" />
         <tli id="T78" time="61.16571428571429" type="appl" />
         <tli id="T79" time="61.962857142857146" type="appl" />
         <tli id="T80" time="62.760000000000005" type="appl" />
         <tli id="T81" time="63.55714285714286" type="appl" />
         <tli id="T82" time="64.35428571428571" type="appl" />
         <tli id="T83" time="65.15142857142857" type="appl" />
         <tli id="T84" time="65.94857142857143" type="appl" />
         <tli id="T85" time="66.74571428571429" type="appl" />
         <tli id="T86" time="67.54285714285714" type="appl" />
         <tli id="T87" time="68.34" type="appl" />
         <tli id="T88" time="69.44466666666666" type="appl" />
         <tli id="T89" time="70.54933333333334" type="appl" />
         <tli id="T90" time="71.654" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="AkEE"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T90" id="Seg_0" n="sc" s="T1">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Kutujagɨ</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">kɨtta</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">taba</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_14" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">Biːrde</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">kutujak</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">tabanɨ</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">körön</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_28" n="HIAT:w" s="T8">di͡ebit</ts>
                  <nts id="Seg_29" n="HIAT:ip">:</nts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T13" id="Seg_32" n="HIAT:u" s="T10">
                  <nts id="Seg_33" n="HIAT:ip">"</nts>
                  <ts e="T11" id="Seg_35" n="HIAT:w" s="T10">Taba</ts>
                  <nts id="Seg_36" n="HIAT:ip">,</nts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_39" n="HIAT:w" s="T11">kistenele</ts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_42" n="HIAT:w" s="T12">oːnnʼu͡ok</ts>
                  <nts id="Seg_43" n="HIAT:ip">.</nts>
                  <nts id="Seg_44" n="HIAT:ip">"</nts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_47" n="HIAT:u" s="T13">
                  <nts id="Seg_48" n="HIAT:ip">"</nts>
                  <ts e="T14" id="Seg_50" n="HIAT:w" s="T13">Oːnnʼu͡ok</ts>
                  <nts id="Seg_51" n="HIAT:ip">"</nts>
                  <nts id="Seg_52" n="HIAT:ip">,</nts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_55" n="HIAT:w" s="T14">ü͡örbüt</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_58" n="HIAT:w" s="T15">taba</ts>
                  <nts id="Seg_59" n="HIAT:ip">.</nts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_62" n="HIAT:u" s="T16">
                  <nts id="Seg_63" n="HIAT:ip">"</nts>
                  <ts e="T17" id="Seg_65" n="HIAT:w" s="T16">Maŋnaj</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_68" n="HIAT:w" s="T17">en</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_71" n="HIAT:w" s="T18">kisten</ts>
                  <nts id="Seg_72" n="HIAT:ip">"</nts>
                  <nts id="Seg_73" n="HIAT:ip">,</nts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_76" n="HIAT:w" s="T19">hübeleːbit</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_79" n="HIAT:w" s="T20">taba</ts>
                  <nts id="Seg_80" n="HIAT:ip">.</nts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T24" id="Seg_83" n="HIAT:u" s="T21">
                  <nts id="Seg_84" n="HIAT:ip">"</nts>
                  <ts e="T22" id="Seg_86" n="HIAT:w" s="T21">Min</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_89" n="HIAT:w" s="T22">enigin</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_92" n="HIAT:w" s="T23">kördü͡öm</ts>
                  <nts id="Seg_93" n="HIAT:ip">.</nts>
                  <nts id="Seg_94" n="HIAT:ip">"</nts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_97" n="HIAT:u" s="T24">
                  <ts e="T25" id="Seg_99" n="HIAT:w" s="T24">Taba</ts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_102" n="HIAT:w" s="T25">kistenne</ts>
                  <nts id="Seg_103" n="HIAT:ip">.</nts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T32" id="Seg_106" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_108" n="HIAT:w" s="T26">Töhö</ts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_111" n="HIAT:w" s="T27">da</ts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_114" n="HIAT:w" s="T28">bu͡olbakka</ts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_117" n="HIAT:w" s="T29">kutujak</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_120" n="HIAT:w" s="T30">kördüː</ts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_123" n="HIAT:w" s="T31">barbɨt</ts>
                  <nts id="Seg_124" n="HIAT:ip">.</nts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T38" id="Seg_127" n="HIAT:u" s="T32">
                  <ts e="T33" id="Seg_129" n="HIAT:w" s="T32">Hiri</ts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_132" n="HIAT:w" s="T33">barɨtɨn</ts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_135" n="HIAT:w" s="T34">tegelijde</ts>
                  <nts id="Seg_136" n="HIAT:ip">,</nts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_139" n="HIAT:w" s="T35">kanna</ts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_142" n="HIAT:w" s="T36">da</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_145" n="HIAT:w" s="T37">bulbat</ts>
                  <nts id="Seg_146" n="HIAT:ip">.</nts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T41" id="Seg_149" n="HIAT:u" s="T38">
                  <ts e="T39" id="Seg_151" n="HIAT:w" s="T38">Talaktaːk</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_154" n="HIAT:w" s="T39">hirge</ts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_157" n="HIAT:w" s="T40">tiːjde</ts>
                  <nts id="Seg_158" n="HIAT:ip">.</nts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T47" id="Seg_161" n="HIAT:u" s="T41">
                  <ts e="T42" id="Seg_163" n="HIAT:w" s="T41">Talaktar</ts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_166" n="HIAT:w" s="T42">ejmeŋniːller</ts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_169" n="HIAT:w" s="T43">tɨ͡altan</ts>
                  <nts id="Seg_170" n="HIAT:ip">,</nts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_173" n="HIAT:w" s="T44">ikki</ts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_176" n="HIAT:w" s="T45">talak</ts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_179" n="HIAT:w" s="T46">kamnaːbat</ts>
                  <nts id="Seg_180" n="HIAT:ip">.</nts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T51" id="Seg_183" n="HIAT:u" s="T47">
                  <ts e="T48" id="Seg_185" n="HIAT:w" s="T47">Kutujak</ts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_188" n="HIAT:w" s="T48">ol</ts>
                  <nts id="Seg_189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_191" n="HIAT:w" s="T49">talaktarga</ts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_194" n="HIAT:w" s="T50">hüːrde</ts>
                  <nts id="Seg_195" n="HIAT:ip">.</nts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T54" id="Seg_198" n="HIAT:u" s="T51">
                  <ts e="T52" id="Seg_200" n="HIAT:w" s="T51">Körbüte</ts>
                  <nts id="Seg_201" n="HIAT:ip">,</nts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_204" n="HIAT:w" s="T52">taba</ts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_207" n="HIAT:w" s="T53">mu͡ostara</ts>
                  <nts id="Seg_208" n="HIAT:ip">.</nts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T61" id="Seg_211" n="HIAT:u" s="T54">
                  <nts id="Seg_212" n="HIAT:ip">"</nts>
                  <ts e="T55" id="Seg_214" n="HIAT:w" s="T54">Anɨ</ts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_217" n="HIAT:w" s="T55">min</ts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_220" n="HIAT:w" s="T56">kisteni͡em</ts>
                  <nts id="Seg_221" n="HIAT:ip">"</nts>
                  <nts id="Seg_222" n="HIAT:ip">,</nts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_225" n="HIAT:w" s="T57">di͡ebit</ts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_228" n="HIAT:w" s="T58">kutujak</ts>
                  <nts id="Seg_229" n="HIAT:ip">,</nts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_231" n="HIAT:ip">"</nts>
                  <ts e="T60" id="Seg_233" n="HIAT:w" s="T59">en</ts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_236" n="HIAT:w" s="T60">kördöː</ts>
                  <nts id="Seg_237" n="HIAT:ip">"</nts>
                  <nts id="Seg_238" n="HIAT:ip">.</nts>
                  <nts id="Seg_239" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T64" id="Seg_241" n="HIAT:u" s="T61">
                  <ts e="T62" id="Seg_243" n="HIAT:w" s="T61">Kutujak</ts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_246" n="HIAT:w" s="T62">hüːren</ts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_249" n="HIAT:w" s="T63">kaːlla</ts>
                  <nts id="Seg_250" n="HIAT:ip">.</nts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T65" id="Seg_253" n="HIAT:u" s="T64">
                  <ts e="T65" id="Seg_255" n="HIAT:w" s="T64">Kistenne</ts>
                  <nts id="Seg_256" n="HIAT:ip">.</nts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T70" id="Seg_259" n="HIAT:u" s="T65">
                  <ts e="T66" id="Seg_261" n="HIAT:w" s="T65">Taba</ts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_264" n="HIAT:w" s="T66">kördüːr</ts>
                  <nts id="Seg_265" n="HIAT:ip">,</nts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_268" n="HIAT:w" s="T67">kördüːr</ts>
                  <nts id="Seg_269" n="HIAT:ip">,</nts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_272" n="HIAT:w" s="T68">amattan</ts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_275" n="HIAT:w" s="T69">bulbat</ts>
                  <nts id="Seg_276" n="HIAT:ip">.</nts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T73" id="Seg_279" n="HIAT:u" s="T70">
                  <ts e="T71" id="Seg_281" n="HIAT:w" s="T70">Heni͡e</ts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_284" n="HIAT:w" s="T71">ke</ts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_287" n="HIAT:w" s="T72">baranna</ts>
                  <nts id="Seg_288" n="HIAT:ip">.</nts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T87" id="Seg_291" n="HIAT:u" s="T73">
                  <ts e="T74" id="Seg_293" n="HIAT:w" s="T73">Ol</ts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_296" n="HIAT:w" s="T74">ihin</ts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_299" n="HIAT:w" s="T75">taba</ts>
                  <nts id="Seg_300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_302" n="HIAT:w" s="T76">anɨga</ts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_305" n="HIAT:w" s="T77">di͡eri</ts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_308" n="HIAT:w" s="T78">hiri</ts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_311" n="HIAT:w" s="T79">hɨllɨːr-hɨllɨːr</ts>
                  <nts id="Seg_312" n="HIAT:ip">,</nts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_315" n="HIAT:w" s="T80">honon</ts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_318" n="HIAT:w" s="T81">bu</ts>
                  <nts id="Seg_319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_321" n="HIAT:w" s="T82">küŋŋe</ts>
                  <nts id="Seg_322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_324" n="HIAT:w" s="T83">di͡eri</ts>
                  <nts id="Seg_325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_327" n="HIAT:w" s="T84">taba</ts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_330" n="HIAT:w" s="T85">kutujagɨ</ts>
                  <nts id="Seg_331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_333" n="HIAT:w" s="T86">bulbat</ts>
                  <nts id="Seg_334" n="HIAT:ip">.</nts>
                  <nts id="Seg_335" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T90" id="Seg_337" n="HIAT:u" s="T87">
                  <ts e="T88" id="Seg_339" n="HIAT:w" s="T87">Oloŋkoloːbuto</ts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_342" n="HIAT:w" s="T88">Ogdu͡o</ts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_345" n="HIAT:w" s="T89">Aksʼonova</ts>
                  <nts id="Seg_346" n="HIAT:ip">.</nts>
                  <nts id="Seg_347" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T90" id="Seg_348" n="sc" s="T1">
               <ts e="T2" id="Seg_350" n="e" s="T1">Kutujagɨ </ts>
               <ts e="T3" id="Seg_352" n="e" s="T2">kɨtta </ts>
               <ts e="T4" id="Seg_354" n="e" s="T3">taba. </ts>
               <ts e="T5" id="Seg_356" n="e" s="T4">Biːrde </ts>
               <ts e="T6" id="Seg_358" n="e" s="T5">kutujak </ts>
               <ts e="T7" id="Seg_360" n="e" s="T6">tabanɨ </ts>
               <ts e="T8" id="Seg_362" n="e" s="T7">körön </ts>
               <ts e="T10" id="Seg_364" n="e" s="T8">di͡ebit: </ts>
               <ts e="T11" id="Seg_366" n="e" s="T10">"Taba, </ts>
               <ts e="T12" id="Seg_368" n="e" s="T11">kistenele </ts>
               <ts e="T13" id="Seg_370" n="e" s="T12">oːnnʼu͡ok." </ts>
               <ts e="T14" id="Seg_372" n="e" s="T13">"Oːnnʼu͡ok", </ts>
               <ts e="T15" id="Seg_374" n="e" s="T14">ü͡örbüt </ts>
               <ts e="T16" id="Seg_376" n="e" s="T15">taba. </ts>
               <ts e="T17" id="Seg_378" n="e" s="T16">"Maŋnaj </ts>
               <ts e="T18" id="Seg_380" n="e" s="T17">en </ts>
               <ts e="T19" id="Seg_382" n="e" s="T18">kisten", </ts>
               <ts e="T20" id="Seg_384" n="e" s="T19">hübeleːbit </ts>
               <ts e="T21" id="Seg_386" n="e" s="T20">taba. </ts>
               <ts e="T22" id="Seg_388" n="e" s="T21">"Min </ts>
               <ts e="T23" id="Seg_390" n="e" s="T22">enigin </ts>
               <ts e="T24" id="Seg_392" n="e" s="T23">kördü͡öm." </ts>
               <ts e="T25" id="Seg_394" n="e" s="T24">Taba </ts>
               <ts e="T26" id="Seg_396" n="e" s="T25">kistenne. </ts>
               <ts e="T27" id="Seg_398" n="e" s="T26">Töhö </ts>
               <ts e="T28" id="Seg_400" n="e" s="T27">da </ts>
               <ts e="T29" id="Seg_402" n="e" s="T28">bu͡olbakka </ts>
               <ts e="T30" id="Seg_404" n="e" s="T29">kutujak </ts>
               <ts e="T31" id="Seg_406" n="e" s="T30">kördüː </ts>
               <ts e="T32" id="Seg_408" n="e" s="T31">barbɨt. </ts>
               <ts e="T33" id="Seg_410" n="e" s="T32">Hiri </ts>
               <ts e="T34" id="Seg_412" n="e" s="T33">barɨtɨn </ts>
               <ts e="T35" id="Seg_414" n="e" s="T34">tegelijde, </ts>
               <ts e="T36" id="Seg_416" n="e" s="T35">kanna </ts>
               <ts e="T37" id="Seg_418" n="e" s="T36">da </ts>
               <ts e="T38" id="Seg_420" n="e" s="T37">bulbat. </ts>
               <ts e="T39" id="Seg_422" n="e" s="T38">Talaktaːk </ts>
               <ts e="T40" id="Seg_424" n="e" s="T39">hirge </ts>
               <ts e="T41" id="Seg_426" n="e" s="T40">tiːjde. </ts>
               <ts e="T42" id="Seg_428" n="e" s="T41">Talaktar </ts>
               <ts e="T43" id="Seg_430" n="e" s="T42">ejmeŋniːller </ts>
               <ts e="T44" id="Seg_432" n="e" s="T43">tɨ͡altan, </ts>
               <ts e="T45" id="Seg_434" n="e" s="T44">ikki </ts>
               <ts e="T46" id="Seg_436" n="e" s="T45">talak </ts>
               <ts e="T47" id="Seg_438" n="e" s="T46">kamnaːbat. </ts>
               <ts e="T48" id="Seg_440" n="e" s="T47">Kutujak </ts>
               <ts e="T49" id="Seg_442" n="e" s="T48">ol </ts>
               <ts e="T50" id="Seg_444" n="e" s="T49">talaktarga </ts>
               <ts e="T51" id="Seg_446" n="e" s="T50">hüːrde. </ts>
               <ts e="T52" id="Seg_448" n="e" s="T51">Körbüte, </ts>
               <ts e="T53" id="Seg_450" n="e" s="T52">taba </ts>
               <ts e="T54" id="Seg_452" n="e" s="T53">mu͡ostara. </ts>
               <ts e="T55" id="Seg_454" n="e" s="T54">"Anɨ </ts>
               <ts e="T56" id="Seg_456" n="e" s="T55">min </ts>
               <ts e="T57" id="Seg_458" n="e" s="T56">kisteni͡em", </ts>
               <ts e="T58" id="Seg_460" n="e" s="T57">di͡ebit </ts>
               <ts e="T59" id="Seg_462" n="e" s="T58">kutujak, </ts>
               <ts e="T60" id="Seg_464" n="e" s="T59">"en </ts>
               <ts e="T61" id="Seg_466" n="e" s="T60">kördöː". </ts>
               <ts e="T62" id="Seg_468" n="e" s="T61">Kutujak </ts>
               <ts e="T63" id="Seg_470" n="e" s="T62">hüːren </ts>
               <ts e="T64" id="Seg_472" n="e" s="T63">kaːlla. </ts>
               <ts e="T65" id="Seg_474" n="e" s="T64">Kistenne. </ts>
               <ts e="T66" id="Seg_476" n="e" s="T65">Taba </ts>
               <ts e="T67" id="Seg_478" n="e" s="T66">kördüːr, </ts>
               <ts e="T68" id="Seg_480" n="e" s="T67">kördüːr, </ts>
               <ts e="T69" id="Seg_482" n="e" s="T68">amattan </ts>
               <ts e="T70" id="Seg_484" n="e" s="T69">bulbat. </ts>
               <ts e="T71" id="Seg_486" n="e" s="T70">Heni͡e </ts>
               <ts e="T72" id="Seg_488" n="e" s="T71">ke </ts>
               <ts e="T73" id="Seg_490" n="e" s="T72">baranna. </ts>
               <ts e="T74" id="Seg_492" n="e" s="T73">Ol </ts>
               <ts e="T75" id="Seg_494" n="e" s="T74">ihin </ts>
               <ts e="T76" id="Seg_496" n="e" s="T75">taba </ts>
               <ts e="T77" id="Seg_498" n="e" s="T76">anɨga </ts>
               <ts e="T78" id="Seg_500" n="e" s="T77">di͡eri </ts>
               <ts e="T79" id="Seg_502" n="e" s="T78">hiri </ts>
               <ts e="T80" id="Seg_504" n="e" s="T79">hɨllɨːr-hɨllɨːr, </ts>
               <ts e="T81" id="Seg_506" n="e" s="T80">honon </ts>
               <ts e="T82" id="Seg_508" n="e" s="T81">bu </ts>
               <ts e="T83" id="Seg_510" n="e" s="T82">küŋŋe </ts>
               <ts e="T84" id="Seg_512" n="e" s="T83">di͡eri </ts>
               <ts e="T85" id="Seg_514" n="e" s="T84">taba </ts>
               <ts e="T86" id="Seg_516" n="e" s="T85">kutujagɨ </ts>
               <ts e="T87" id="Seg_518" n="e" s="T86">bulbat. </ts>
               <ts e="T88" id="Seg_520" n="e" s="T87">Oloŋkoloːbuto </ts>
               <ts e="T89" id="Seg_522" n="e" s="T88">Ogdu͡o </ts>
               <ts e="T90" id="Seg_524" n="e" s="T89">Aksʼonova. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_525" s="T1">AkEE_19900810_ReindeerMouse_flk.001 (001.001)</ta>
            <ta e="T10" id="Seg_526" s="T4">AkEE_19900810_ReindeerMouse_flk.002 (001.002)</ta>
            <ta e="T13" id="Seg_527" s="T10">AkEE_19900810_ReindeerMouse_flk.003 (001.003)</ta>
            <ta e="T16" id="Seg_528" s="T13">AkEE_19900810_ReindeerMouse_flk.004 (001.004)</ta>
            <ta e="T21" id="Seg_529" s="T16">AkEE_19900810_ReindeerMouse_flk.005 (001.005)</ta>
            <ta e="T24" id="Seg_530" s="T21">AkEE_19900810_ReindeerMouse_flk.006 (001.006)</ta>
            <ta e="T26" id="Seg_531" s="T24">AkEE_19900810_ReindeerMouse_flk.007 (001.007)</ta>
            <ta e="T32" id="Seg_532" s="T26">AkEE_19900810_ReindeerMouse_flk.008 (001.008)</ta>
            <ta e="T38" id="Seg_533" s="T32">AkEE_19900810_ReindeerMouse_flk.009 (001.009)</ta>
            <ta e="T41" id="Seg_534" s="T38">AkEE_19900810_ReindeerMouse_flk.010 (001.010)</ta>
            <ta e="T47" id="Seg_535" s="T41">AkEE_19900810_ReindeerMouse_flk.011 (001.011)</ta>
            <ta e="T51" id="Seg_536" s="T47">AkEE_19900810_ReindeerMouse_flk.012 (001.012)</ta>
            <ta e="T54" id="Seg_537" s="T51">AkEE_19900810_ReindeerMouse_flk.013 (001.013)</ta>
            <ta e="T61" id="Seg_538" s="T54">AkEE_19900810_ReindeerMouse_flk.014 (001.014)</ta>
            <ta e="T64" id="Seg_539" s="T61">AkEE_19900810_ReindeerMouse_flk.015 (001.015)</ta>
            <ta e="T65" id="Seg_540" s="T64">AkEE_19900810_ReindeerMouse_flk.016 (001.016)</ta>
            <ta e="T70" id="Seg_541" s="T65">AkEE_19900810_ReindeerMouse_flk.017 (001.017)</ta>
            <ta e="T73" id="Seg_542" s="T70">AkEE_19900810_ReindeerMouse_flk.018 (001.018)</ta>
            <ta e="T87" id="Seg_543" s="T73">AkEE_19900810_ReindeerMouse_flk.019 (001.019)</ta>
            <ta e="T90" id="Seg_544" s="T87">AkEE_19900810_ReindeerMouse_flk.020 (001.020)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T4" id="Seg_545" s="T1">Кутуйагы кытта таба.</ta>
            <ta e="T10" id="Seg_546" s="T4">Биирдэ кутуйак табаны көрөн диэбит: </ta>
            <ta e="T13" id="Seg_547" s="T10">"Таба, кистэӈэлэ оонньуок."</ta>
            <ta e="T16" id="Seg_548" s="T13">"Оонньуок, – үөрбүт таба. </ta>
            <ta e="T21" id="Seg_549" s="T16">Маӈнай эн кистэн", – һүбэлээбит таба.</ta>
            <ta e="T24" id="Seg_550" s="T21">– мин энигин күөрдөм.</ta>
            <ta e="T26" id="Seg_551" s="T24">Таба кистэннэ.</ta>
            <ta e="T32" id="Seg_552" s="T26">Төһө да болбакка кутуйак көрдүү барбыт.</ta>
            <ta e="T38" id="Seg_553" s="T32">һири барытын тэгэлийдэ, канна да булбат. </ta>
            <ta e="T41" id="Seg_554" s="T38">Таллактаак һиргэ тийдэ.</ta>
            <ta e="T47" id="Seg_555" s="T41">Таллактар ээймэӈниллэр тыалтан, икки талак камнаабат.</ta>
            <ta e="T51" id="Seg_556" s="T47">Кутуйак ол талактарга һүүрдэ. </ta>
            <ta e="T54" id="Seg_557" s="T51">Көрбүтэ, таба муостара. </ta>
            <ta e="T61" id="Seg_558" s="T54">"Аны мин кистэнэм, – диэбит кутуйак, – эн көрдөө".</ta>
            <ta e="T64" id="Seg_559" s="T61">Кутуйак һүүрэн калла. </ta>
            <ta e="T65" id="Seg_560" s="T64">Кистэннэ. </ta>
            <ta e="T70" id="Seg_561" s="T65">Таба көрдүүр, көрдүүр, аматтан булбат. </ta>
            <ta e="T73" id="Seg_562" s="T70">Һэниэкэ баранна. </ta>
            <ta e="T87" id="Seg_563" s="T73">Ол иһин таба аныга диэри һири һыллыыр-һыллыыр, һонон бу күнӈэ диэри таба кутуйагы булбат. </ta>
            <ta e="T90" id="Seg_564" s="T87">Олоӈколообута Огдуо Аксёнова.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_565" s="T1">Kutujagɨ kɨtta taba. </ta>
            <ta e="T10" id="Seg_566" s="T4">Biːrde kutujak tabanɨ körön di͡ebit: </ta>
            <ta e="T13" id="Seg_567" s="T10">"Taba, kistenele oːnnʼu͡ok." </ta>
            <ta e="T16" id="Seg_568" s="T13">"Oːnnʼu͡ok", ü͡örbüt taba. </ta>
            <ta e="T21" id="Seg_569" s="T16">"Maŋnaj en kisten", hübeleːbit taba. </ta>
            <ta e="T24" id="Seg_570" s="T21">"Min enigin kördü͡öm." </ta>
            <ta e="T26" id="Seg_571" s="T24">Taba kistenne. </ta>
            <ta e="T32" id="Seg_572" s="T26">Töhö da bu͡olbakka kutujak kördüː barbɨt. </ta>
            <ta e="T38" id="Seg_573" s="T32">Hiri barɨtɨn tegelijde, kanna da bulbat. </ta>
            <ta e="T41" id="Seg_574" s="T38">Talaktaːk hirge tiːjde. </ta>
            <ta e="T47" id="Seg_575" s="T41">Talaktar ejmeŋniːller tɨ͡altan, ikki talak kamnaːbat. </ta>
            <ta e="T51" id="Seg_576" s="T47">Kutujak ol talaktarga hüːrde. </ta>
            <ta e="T54" id="Seg_577" s="T51">Körbüte, taba mu͡ostara. </ta>
            <ta e="T61" id="Seg_578" s="T54">"Anɨ min kisteni͡em", di͡ebit kutujak, "en kördöː". </ta>
            <ta e="T64" id="Seg_579" s="T61">Kutujak hüːren kaːlla. </ta>
            <ta e="T65" id="Seg_580" s="T64">Kistenne. </ta>
            <ta e="T70" id="Seg_581" s="T65">Taba kördüːr, kördüːr, amattan bulbat. </ta>
            <ta e="T73" id="Seg_582" s="T70">Heni͡e ke baranna. </ta>
            <ta e="T87" id="Seg_583" s="T73">Ol ihin taba anɨga di͡eri hiri hɨllɨːr-hɨllɨːr, honon bu küŋŋe di͡eri taba kutujagɨ bulbat. </ta>
            <ta e="T90" id="Seg_584" s="T87">Oloŋkoloːbuto Ogdu͡o Aksʼonova. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_585" s="T1">kutujag-ɨ</ta>
            <ta e="T3" id="Seg_586" s="T2">kɨtta</ta>
            <ta e="T4" id="Seg_587" s="T3">taba</ta>
            <ta e="T5" id="Seg_588" s="T4">biːrde</ta>
            <ta e="T6" id="Seg_589" s="T5">kutujak</ta>
            <ta e="T7" id="Seg_590" s="T6">taba-nɨ</ta>
            <ta e="T8" id="Seg_591" s="T7">kör-ön</ta>
            <ta e="T10" id="Seg_592" s="T8">di͡e-bit</ta>
            <ta e="T11" id="Seg_593" s="T10">taba</ta>
            <ta e="T12" id="Seg_594" s="T11">kisten-el-e</ta>
            <ta e="T13" id="Seg_595" s="T12">oːnnʼ-u͡ok</ta>
            <ta e="T14" id="Seg_596" s="T13">oːnnʼ-u͡ok</ta>
            <ta e="T15" id="Seg_597" s="T14">ü͡ör-büt</ta>
            <ta e="T16" id="Seg_598" s="T15">taba</ta>
            <ta e="T17" id="Seg_599" s="T16">maŋnaj</ta>
            <ta e="T18" id="Seg_600" s="T17">en</ta>
            <ta e="T19" id="Seg_601" s="T18">kisten</ta>
            <ta e="T20" id="Seg_602" s="T19">hübeleː-bit</ta>
            <ta e="T21" id="Seg_603" s="T20">taba</ta>
            <ta e="T22" id="Seg_604" s="T21">min</ta>
            <ta e="T23" id="Seg_605" s="T22">enigi-n</ta>
            <ta e="T24" id="Seg_606" s="T23">körd-ü͡ö-m</ta>
            <ta e="T25" id="Seg_607" s="T24">taba</ta>
            <ta e="T26" id="Seg_608" s="T25">kisten-n-e</ta>
            <ta e="T27" id="Seg_609" s="T26">töhö</ta>
            <ta e="T28" id="Seg_610" s="T27">da</ta>
            <ta e="T29" id="Seg_611" s="T28">bu͡ol-bakka</ta>
            <ta e="T30" id="Seg_612" s="T29">kutujak</ta>
            <ta e="T31" id="Seg_613" s="T30">körd-üː</ta>
            <ta e="T32" id="Seg_614" s="T31">bar-bɨt</ta>
            <ta e="T33" id="Seg_615" s="T32">hir-i</ta>
            <ta e="T34" id="Seg_616" s="T33">barɨ-tɨ-n</ta>
            <ta e="T35" id="Seg_617" s="T34">tegelij-d-e</ta>
            <ta e="T36" id="Seg_618" s="T35">kanna</ta>
            <ta e="T37" id="Seg_619" s="T36">da</ta>
            <ta e="T38" id="Seg_620" s="T37">bul-bat</ta>
            <ta e="T39" id="Seg_621" s="T38">talak-taːk</ta>
            <ta e="T40" id="Seg_622" s="T39">hir-ge</ta>
            <ta e="T41" id="Seg_623" s="T40">tiːj-d-e</ta>
            <ta e="T42" id="Seg_624" s="T41">talak-tar</ta>
            <ta e="T43" id="Seg_625" s="T42">ejmeŋniː-l-ler</ta>
            <ta e="T44" id="Seg_626" s="T43">tɨ͡al-tan</ta>
            <ta e="T45" id="Seg_627" s="T44">ikki</ta>
            <ta e="T46" id="Seg_628" s="T45">talak</ta>
            <ta e="T47" id="Seg_629" s="T46">kamnaː-bat</ta>
            <ta e="T48" id="Seg_630" s="T47">kutujak</ta>
            <ta e="T49" id="Seg_631" s="T48">ol</ta>
            <ta e="T50" id="Seg_632" s="T49">talak-tar-ga</ta>
            <ta e="T51" id="Seg_633" s="T50">hüːr-d-e</ta>
            <ta e="T52" id="Seg_634" s="T51">kör-büt-e</ta>
            <ta e="T53" id="Seg_635" s="T52">taba</ta>
            <ta e="T54" id="Seg_636" s="T53">mu͡os-tar-a</ta>
            <ta e="T55" id="Seg_637" s="T54">anɨ</ta>
            <ta e="T56" id="Seg_638" s="T55">min</ta>
            <ta e="T57" id="Seg_639" s="T56">kisten-i͡e-m</ta>
            <ta e="T58" id="Seg_640" s="T57">di͡e-bit</ta>
            <ta e="T59" id="Seg_641" s="T58">kutujak</ta>
            <ta e="T60" id="Seg_642" s="T59">en</ta>
            <ta e="T61" id="Seg_643" s="T60">kördöː</ta>
            <ta e="T62" id="Seg_644" s="T61">kutujak</ta>
            <ta e="T63" id="Seg_645" s="T62">hüːr-en</ta>
            <ta e="T64" id="Seg_646" s="T63">kaːl-l-a</ta>
            <ta e="T65" id="Seg_647" s="T64">kisten-n-e</ta>
            <ta e="T66" id="Seg_648" s="T65">taba</ta>
            <ta e="T67" id="Seg_649" s="T66">kördüː-r</ta>
            <ta e="T68" id="Seg_650" s="T67">kördüː-r</ta>
            <ta e="T69" id="Seg_651" s="T68">amattan</ta>
            <ta e="T70" id="Seg_652" s="T69">bul-bat</ta>
            <ta e="T71" id="Seg_653" s="T70">heni͡e</ta>
            <ta e="T72" id="Seg_654" s="T71">ke</ta>
            <ta e="T73" id="Seg_655" s="T72">baran-n-a</ta>
            <ta e="T74" id="Seg_656" s="T73">ol</ta>
            <ta e="T75" id="Seg_657" s="T74">ihin</ta>
            <ta e="T76" id="Seg_658" s="T75">taba</ta>
            <ta e="T77" id="Seg_659" s="T76">anɨ-ga</ta>
            <ta e="T78" id="Seg_660" s="T77">di͡eri</ta>
            <ta e="T79" id="Seg_661" s="T78">hir-i</ta>
            <ta e="T80" id="Seg_662" s="T79">hɨllɨː-r-hɨllɨː-r</ta>
            <ta e="T81" id="Seg_663" s="T80">h-onon</ta>
            <ta e="T82" id="Seg_664" s="T81">bu</ta>
            <ta e="T83" id="Seg_665" s="T82">küŋ-ŋe</ta>
            <ta e="T84" id="Seg_666" s="T83">di͡eri</ta>
            <ta e="T85" id="Seg_667" s="T84">taba</ta>
            <ta e="T86" id="Seg_668" s="T85">kutujag-ɨ</ta>
            <ta e="T87" id="Seg_669" s="T86">bul-bat</ta>
            <ta e="T88" id="Seg_670" s="T87">oloŋko-loː-but-o</ta>
            <ta e="T89" id="Seg_671" s="T88">Ogdu͡o</ta>
            <ta e="T90" id="Seg_672" s="T89">Aksʼonova</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_673" s="T1">kutujak-nI</ta>
            <ta e="T3" id="Seg_674" s="T2">kɨtta</ta>
            <ta e="T4" id="Seg_675" s="T3">taba</ta>
            <ta e="T5" id="Seg_676" s="T4">biːrde</ta>
            <ta e="T6" id="Seg_677" s="T5">kutujak</ta>
            <ta e="T7" id="Seg_678" s="T6">taba-nI</ta>
            <ta e="T8" id="Seg_679" s="T7">kör-An</ta>
            <ta e="T10" id="Seg_680" s="T8">di͡e-BIT</ta>
            <ta e="T11" id="Seg_681" s="T10">taba</ta>
            <ta e="T12" id="Seg_682" s="T11">kisten-AlAː-A</ta>
            <ta e="T13" id="Seg_683" s="T12">oːnnʼoː-IAk</ta>
            <ta e="T14" id="Seg_684" s="T13">oːnnʼoː-IAk</ta>
            <ta e="T15" id="Seg_685" s="T14">ü͡ör-BIT</ta>
            <ta e="T16" id="Seg_686" s="T15">taba</ta>
            <ta e="T17" id="Seg_687" s="T16">maŋnaj</ta>
            <ta e="T18" id="Seg_688" s="T17">en</ta>
            <ta e="T19" id="Seg_689" s="T18">kisten</ta>
            <ta e="T20" id="Seg_690" s="T19">hübeleː-BIT</ta>
            <ta e="T21" id="Seg_691" s="T20">taba</ta>
            <ta e="T22" id="Seg_692" s="T21">min</ta>
            <ta e="T23" id="Seg_693" s="T22">en-n</ta>
            <ta e="T24" id="Seg_694" s="T23">kördöː-IAK-m</ta>
            <ta e="T25" id="Seg_695" s="T24">taba</ta>
            <ta e="T26" id="Seg_696" s="T25">kisten-TI-tA</ta>
            <ta e="T27" id="Seg_697" s="T26">töhö</ta>
            <ta e="T28" id="Seg_698" s="T27">da</ta>
            <ta e="T29" id="Seg_699" s="T28">bu͡ol-BAkkA</ta>
            <ta e="T30" id="Seg_700" s="T29">kutujak</ta>
            <ta e="T31" id="Seg_701" s="T30">kördöː-A</ta>
            <ta e="T32" id="Seg_702" s="T31">bar-BIT</ta>
            <ta e="T33" id="Seg_703" s="T32">hir-nI</ta>
            <ta e="T34" id="Seg_704" s="T33">barɨ-tI-n</ta>
            <ta e="T35" id="Seg_705" s="T34">tegelij-TI-tA</ta>
            <ta e="T36" id="Seg_706" s="T35">kanna</ta>
            <ta e="T37" id="Seg_707" s="T36">da</ta>
            <ta e="T38" id="Seg_708" s="T37">bul-BAT</ta>
            <ta e="T39" id="Seg_709" s="T38">talak-LAːK</ta>
            <ta e="T40" id="Seg_710" s="T39">hir-GA</ta>
            <ta e="T41" id="Seg_711" s="T40">tij-TI-tA</ta>
            <ta e="T42" id="Seg_712" s="T41">talak-LAr</ta>
            <ta e="T43" id="Seg_713" s="T42">ejmeŋneː-Ar-LAr</ta>
            <ta e="T44" id="Seg_714" s="T43">tɨ͡al-ttAn</ta>
            <ta e="T45" id="Seg_715" s="T44">ikki</ta>
            <ta e="T46" id="Seg_716" s="T45">talak</ta>
            <ta e="T47" id="Seg_717" s="T46">kamnaː-BAT</ta>
            <ta e="T48" id="Seg_718" s="T47">kutujak</ta>
            <ta e="T49" id="Seg_719" s="T48">ol</ta>
            <ta e="T50" id="Seg_720" s="T49">talak-LAr-GA</ta>
            <ta e="T51" id="Seg_721" s="T50">hüːr-TI-tA</ta>
            <ta e="T52" id="Seg_722" s="T51">kör-BIT-tA</ta>
            <ta e="T53" id="Seg_723" s="T52">taba</ta>
            <ta e="T54" id="Seg_724" s="T53">mu͡os-LAr-tA</ta>
            <ta e="T55" id="Seg_725" s="T54">anɨ</ta>
            <ta e="T56" id="Seg_726" s="T55">min</ta>
            <ta e="T57" id="Seg_727" s="T56">kisten-IAK-m</ta>
            <ta e="T58" id="Seg_728" s="T57">di͡e-BIT</ta>
            <ta e="T59" id="Seg_729" s="T58">kutujak</ta>
            <ta e="T60" id="Seg_730" s="T59">en</ta>
            <ta e="T61" id="Seg_731" s="T60">kördöː</ta>
            <ta e="T62" id="Seg_732" s="T61">kutujak</ta>
            <ta e="T63" id="Seg_733" s="T62">hüːr-An</ta>
            <ta e="T64" id="Seg_734" s="T63">kaːl-TI-tA</ta>
            <ta e="T65" id="Seg_735" s="T64">kisten-TI-tA</ta>
            <ta e="T66" id="Seg_736" s="T65">taba</ta>
            <ta e="T67" id="Seg_737" s="T66">kördöː-Ar</ta>
            <ta e="T68" id="Seg_738" s="T67">kördöː-Ar</ta>
            <ta e="T69" id="Seg_739" s="T68">amattan</ta>
            <ta e="T70" id="Seg_740" s="T69">bul-BAT</ta>
            <ta e="T71" id="Seg_741" s="T70">heni͡e</ta>
            <ta e="T72" id="Seg_742" s="T71">ka</ta>
            <ta e="T73" id="Seg_743" s="T72">baran-TI-tA</ta>
            <ta e="T74" id="Seg_744" s="T73">ol</ta>
            <ta e="T75" id="Seg_745" s="T74">ihin</ta>
            <ta e="T76" id="Seg_746" s="T75">taba</ta>
            <ta e="T77" id="Seg_747" s="T76">anɨ-GA</ta>
            <ta e="T78" id="Seg_748" s="T77">di͡eri</ta>
            <ta e="T79" id="Seg_749" s="T78">hir-nI</ta>
            <ta e="T80" id="Seg_750" s="T79">hɨllaː-Ar-hɨllaː-Ar</ta>
            <ta e="T81" id="Seg_751" s="T80">h-onon</ta>
            <ta e="T82" id="Seg_752" s="T81">bu</ta>
            <ta e="T83" id="Seg_753" s="T82">kün-GA</ta>
            <ta e="T84" id="Seg_754" s="T83">di͡eri</ta>
            <ta e="T85" id="Seg_755" s="T84">taba</ta>
            <ta e="T86" id="Seg_756" s="T85">kutujak-nI</ta>
            <ta e="T87" id="Seg_757" s="T86">bul-BAT</ta>
            <ta e="T88" id="Seg_758" s="T87">oloŋko-LAː-BIT-tA</ta>
            <ta e="T89" id="Seg_759" s="T88">Ogdu͡o</ta>
            <ta e="T90" id="Seg_760" s="T89">Aksʼonova</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_761" s="T1">mouse-ACC</ta>
            <ta e="T3" id="Seg_762" s="T2">with</ta>
            <ta e="T4" id="Seg_763" s="T3">reindeer.[NOM]</ta>
            <ta e="T5" id="Seg_764" s="T4">once</ta>
            <ta e="T6" id="Seg_765" s="T5">mouse.[NOM]</ta>
            <ta e="T7" id="Seg_766" s="T6">reindeer-ACC</ta>
            <ta e="T8" id="Seg_767" s="T7">see-CVB.SEQ</ta>
            <ta e="T10" id="Seg_768" s="T8">say-PST2.[3SG]</ta>
            <ta e="T11" id="Seg_769" s="T10">reindeer.[NOM]</ta>
            <ta e="T12" id="Seg_770" s="T11">hide-FREQ-CVB.SIM</ta>
            <ta e="T13" id="Seg_771" s="T12">play-IMP.1DU</ta>
            <ta e="T14" id="Seg_772" s="T13">play-IMP.1DU</ta>
            <ta e="T15" id="Seg_773" s="T14">be.happy-PST2.[3SG]</ta>
            <ta e="T16" id="Seg_774" s="T15">reindeer.[NOM]</ta>
            <ta e="T17" id="Seg_775" s="T16">at.first</ta>
            <ta e="T18" id="Seg_776" s="T17">2SG.[NOM]</ta>
            <ta e="T19" id="Seg_777" s="T18">hide.[IMP.2SG]</ta>
            <ta e="T20" id="Seg_778" s="T19">advise-PST2.[3SG]</ta>
            <ta e="T21" id="Seg_779" s="T20">reindeer.[NOM]</ta>
            <ta e="T22" id="Seg_780" s="T21">1SG.[NOM]</ta>
            <ta e="T23" id="Seg_781" s="T22">2SG-ACC</ta>
            <ta e="T24" id="Seg_782" s="T23">search-FUT-1SG</ta>
            <ta e="T25" id="Seg_783" s="T24">reindeer.[NOM]</ta>
            <ta e="T26" id="Seg_784" s="T25">hide-PST1-3SG</ta>
            <ta e="T27" id="Seg_785" s="T26">how.much</ta>
            <ta e="T28" id="Seg_786" s="T27">NEG</ta>
            <ta e="T29" id="Seg_787" s="T28">be-NEG.CVB.SIM</ta>
            <ta e="T30" id="Seg_788" s="T29">mouse.[NOM]</ta>
            <ta e="T31" id="Seg_789" s="T30">search-CVB.SIM</ta>
            <ta e="T32" id="Seg_790" s="T31">go-PST2.[3SG]</ta>
            <ta e="T33" id="Seg_791" s="T32">earth-ACC</ta>
            <ta e="T34" id="Seg_792" s="T33">whole-3SG-ACC</ta>
            <ta e="T35" id="Seg_793" s="T34">%%-PST1-3SG</ta>
            <ta e="T36" id="Seg_794" s="T35">where</ta>
            <ta e="T37" id="Seg_795" s="T36">NEG</ta>
            <ta e="T38" id="Seg_796" s="T37">find-NEG.[3SG]</ta>
            <ta e="T39" id="Seg_797" s="T38">bush-PROPR</ta>
            <ta e="T40" id="Seg_798" s="T39">place-DAT/LOC</ta>
            <ta e="T41" id="Seg_799" s="T40">reach-PST1-3SG</ta>
            <ta e="T42" id="Seg_800" s="T41">bush-PL.[NOM]</ta>
            <ta e="T43" id="Seg_801" s="T42">move-PRS-3PL</ta>
            <ta e="T44" id="Seg_802" s="T43">wind-ABL</ta>
            <ta e="T45" id="Seg_803" s="T44">two</ta>
            <ta e="T46" id="Seg_804" s="T45">bush.[NOM]</ta>
            <ta e="T47" id="Seg_805" s="T46">move-NEG.[3SG]</ta>
            <ta e="T48" id="Seg_806" s="T47">mouse.[NOM]</ta>
            <ta e="T49" id="Seg_807" s="T48">that</ta>
            <ta e="T50" id="Seg_808" s="T49">bush-PL-DAT/LOC</ta>
            <ta e="T51" id="Seg_809" s="T50">run-PST1-3SG</ta>
            <ta e="T52" id="Seg_810" s="T51">see-PST2-3SG</ta>
            <ta e="T53" id="Seg_811" s="T52">reindeer.[NOM]</ta>
            <ta e="T54" id="Seg_812" s="T53">horn-PL-3SG.[NOM]</ta>
            <ta e="T55" id="Seg_813" s="T54">now</ta>
            <ta e="T56" id="Seg_814" s="T55">1SG.[NOM]</ta>
            <ta e="T57" id="Seg_815" s="T56">hide-FUT-1SG</ta>
            <ta e="T58" id="Seg_816" s="T57">say-PST2.[3SG]</ta>
            <ta e="T59" id="Seg_817" s="T58">mouse.[NOM]</ta>
            <ta e="T60" id="Seg_818" s="T59">2SG.[NOM]</ta>
            <ta e="T61" id="Seg_819" s="T60">search.[IMP.2SG]</ta>
            <ta e="T62" id="Seg_820" s="T61">mouse.[NOM]</ta>
            <ta e="T63" id="Seg_821" s="T62">run-CVB.SEQ</ta>
            <ta e="T64" id="Seg_822" s="T63">stay-PST1-3SG</ta>
            <ta e="T65" id="Seg_823" s="T64">hide-PST1-3SG</ta>
            <ta e="T66" id="Seg_824" s="T65">reindeer.[NOM]</ta>
            <ta e="T67" id="Seg_825" s="T66">search-PRS.[3SG]</ta>
            <ta e="T68" id="Seg_826" s="T67">search-PRS.[3SG]</ta>
            <ta e="T69" id="Seg_827" s="T68">at.all</ta>
            <ta e="T70" id="Seg_828" s="T69">find-NEG.[3SG]</ta>
            <ta e="T71" id="Seg_829" s="T70">power.[NOM]</ta>
            <ta e="T72" id="Seg_830" s="T71">well</ta>
            <ta e="T73" id="Seg_831" s="T72">end-PST1-3SG</ta>
            <ta e="T74" id="Seg_832" s="T73">that.[NOM]</ta>
            <ta e="T75" id="Seg_833" s="T74">because.of</ta>
            <ta e="T76" id="Seg_834" s="T75">reindeer.[NOM]</ta>
            <ta e="T77" id="Seg_835" s="T76">now-DAT/LOC</ta>
            <ta e="T78" id="Seg_836" s="T77">until</ta>
            <ta e="T79" id="Seg_837" s="T78">earth-ACC</ta>
            <ta e="T80" id="Seg_838" s="T79">sniff-PRS.[3SG]-sniff-PRS.[3SG]</ta>
            <ta e="T81" id="Seg_839" s="T80">EMPH-then</ta>
            <ta e="T82" id="Seg_840" s="T81">this</ta>
            <ta e="T83" id="Seg_841" s="T82">day-DAT/LOC</ta>
            <ta e="T84" id="Seg_842" s="T83">until</ta>
            <ta e="T85" id="Seg_843" s="T84">reindeer.[NOM]</ta>
            <ta e="T86" id="Seg_844" s="T85">mouse-ACC</ta>
            <ta e="T87" id="Seg_845" s="T86">find-NEG.[3SG]</ta>
            <ta e="T88" id="Seg_846" s="T87">tale-VBZ-PST2-3SG</ta>
            <ta e="T89" id="Seg_847" s="T88">Ogdo</ta>
            <ta e="T90" id="Seg_848" s="T89">Aksyonova.[NOM]</ta>
         </annotation>
         <annotation name="gg" tierref="gg">
            <ta e="T2" id="Seg_849" s="T1">Maus-ACC</ta>
            <ta e="T3" id="Seg_850" s="T2">mit</ta>
            <ta e="T4" id="Seg_851" s="T3">Rentier.[NOM]</ta>
            <ta e="T5" id="Seg_852" s="T4">einmal</ta>
            <ta e="T6" id="Seg_853" s="T5">Maus.[NOM]</ta>
            <ta e="T7" id="Seg_854" s="T6">Rentier-ACC</ta>
            <ta e="T8" id="Seg_855" s="T7">sehen-CVB.SEQ</ta>
            <ta e="T10" id="Seg_856" s="T8">sagen-PST2.[3SG]</ta>
            <ta e="T11" id="Seg_857" s="T10">Rentier.[NOM]</ta>
            <ta e="T12" id="Seg_858" s="T11">sich.verstecken-FREQ-CVB.SIM</ta>
            <ta e="T13" id="Seg_859" s="T12">spielen-IMP.1DU</ta>
            <ta e="T14" id="Seg_860" s="T13">spielen-IMP.1DU</ta>
            <ta e="T15" id="Seg_861" s="T14">sich.freuen-PST2.[3SG]</ta>
            <ta e="T16" id="Seg_862" s="T15">Rentier.[NOM]</ta>
            <ta e="T17" id="Seg_863" s="T16">zuerst</ta>
            <ta e="T18" id="Seg_864" s="T17">2SG.[NOM]</ta>
            <ta e="T19" id="Seg_865" s="T18">sich.verstecken.[IMP.2SG]</ta>
            <ta e="T20" id="Seg_866" s="T19">raten-PST2.[3SG]</ta>
            <ta e="T21" id="Seg_867" s="T20">Rentier.[NOM]</ta>
            <ta e="T22" id="Seg_868" s="T21">1SG.[NOM]</ta>
            <ta e="T23" id="Seg_869" s="T22">2SG-ACC</ta>
            <ta e="T24" id="Seg_870" s="T23">suchen-FUT-1SG</ta>
            <ta e="T25" id="Seg_871" s="T24">Rentier.[NOM]</ta>
            <ta e="T26" id="Seg_872" s="T25">sich.verstecken-PST1-3SG</ta>
            <ta e="T27" id="Seg_873" s="T26">wie.viel</ta>
            <ta e="T28" id="Seg_874" s="T27">NEG</ta>
            <ta e="T29" id="Seg_875" s="T28">sein-NEG.CVB.SIM</ta>
            <ta e="T30" id="Seg_876" s="T29">Maus.[NOM]</ta>
            <ta e="T31" id="Seg_877" s="T30">suchen-CVB.SIM</ta>
            <ta e="T32" id="Seg_878" s="T31">gehen-PST2.[3SG]</ta>
            <ta e="T33" id="Seg_879" s="T32">Erde-ACC</ta>
            <ta e="T34" id="Seg_880" s="T33">ganz-3SG-ACC</ta>
            <ta e="T35" id="Seg_881" s="T34">%%-PST1-3SG</ta>
            <ta e="T36" id="Seg_882" s="T35">wo</ta>
            <ta e="T37" id="Seg_883" s="T36">NEG</ta>
            <ta e="T38" id="Seg_884" s="T37">finden-NEG.[3SG]</ta>
            <ta e="T39" id="Seg_885" s="T38">Strauch-PROPR</ta>
            <ta e="T40" id="Seg_886" s="T39">Ort-DAT/LOC</ta>
            <ta e="T41" id="Seg_887" s="T40">ankommen-PST1-3SG</ta>
            <ta e="T42" id="Seg_888" s="T41">Strauch-PL.[NOM]</ta>
            <ta e="T43" id="Seg_889" s="T42">sich.bewegen-PRS-3PL</ta>
            <ta e="T44" id="Seg_890" s="T43">Wind-ABL</ta>
            <ta e="T45" id="Seg_891" s="T44">zwei</ta>
            <ta e="T46" id="Seg_892" s="T45">Strauch.[NOM]</ta>
            <ta e="T47" id="Seg_893" s="T46">sich.bewegen-NEG.[3SG]</ta>
            <ta e="T48" id="Seg_894" s="T47">Maus.[NOM]</ta>
            <ta e="T49" id="Seg_895" s="T48">jenes</ta>
            <ta e="T50" id="Seg_896" s="T49">Strauch-PL-DAT/LOC</ta>
            <ta e="T51" id="Seg_897" s="T50">laufen-PST1-3SG</ta>
            <ta e="T52" id="Seg_898" s="T51">sehen-PST2-3SG</ta>
            <ta e="T53" id="Seg_899" s="T52">Rentier.[NOM]</ta>
            <ta e="T54" id="Seg_900" s="T53">Horn-PL-3SG.[NOM]</ta>
            <ta e="T55" id="Seg_901" s="T54">jetzt</ta>
            <ta e="T56" id="Seg_902" s="T55">1SG.[NOM]</ta>
            <ta e="T57" id="Seg_903" s="T56">sich.verstecken-FUT-1SG</ta>
            <ta e="T58" id="Seg_904" s="T57">sagen-PST2.[3SG]</ta>
            <ta e="T59" id="Seg_905" s="T58">Maus.[NOM]</ta>
            <ta e="T60" id="Seg_906" s="T59">2SG.[NOM]</ta>
            <ta e="T61" id="Seg_907" s="T60">suchen.[IMP.2SG]</ta>
            <ta e="T62" id="Seg_908" s="T61">Maus.[NOM]</ta>
            <ta e="T63" id="Seg_909" s="T62">laufen-CVB.SEQ</ta>
            <ta e="T64" id="Seg_910" s="T63">bleiben-PST1-3SG</ta>
            <ta e="T65" id="Seg_911" s="T64">sich.verstecken-PST1-3SG</ta>
            <ta e="T66" id="Seg_912" s="T65">Rentier.[NOM]</ta>
            <ta e="T67" id="Seg_913" s="T66">suchen-PRS.[3SG]</ta>
            <ta e="T68" id="Seg_914" s="T67">suchen-PRS.[3SG]</ta>
            <ta e="T69" id="Seg_915" s="T68">völlig</ta>
            <ta e="T70" id="Seg_916" s="T69">finden-NEG.[3SG]</ta>
            <ta e="T71" id="Seg_917" s="T70">Kraft.[NOM]</ta>
            <ta e="T72" id="Seg_918" s="T71">nun</ta>
            <ta e="T73" id="Seg_919" s="T72">enden-PST1-3SG</ta>
            <ta e="T74" id="Seg_920" s="T73">jenes.[NOM]</ta>
            <ta e="T75" id="Seg_921" s="T74">wegen</ta>
            <ta e="T76" id="Seg_922" s="T75">Rentier.[NOM]</ta>
            <ta e="T77" id="Seg_923" s="T76">jetzt-DAT/LOC</ta>
            <ta e="T78" id="Seg_924" s="T77">bis.zu</ta>
            <ta e="T79" id="Seg_925" s="T78">Erde-ACC</ta>
            <ta e="T80" id="Seg_926" s="T79">schnuppern-PRS.[3SG]-schnuppern-PRS.[3SG]</ta>
            <ta e="T81" id="Seg_927" s="T80">EMPH-dann</ta>
            <ta e="T82" id="Seg_928" s="T81">dieses</ta>
            <ta e="T83" id="Seg_929" s="T82">Tag-DAT/LOC</ta>
            <ta e="T84" id="Seg_930" s="T83">bis.zu</ta>
            <ta e="T85" id="Seg_931" s="T84">Rentier.[NOM]</ta>
            <ta e="T86" id="Seg_932" s="T85">Maus-ACC</ta>
            <ta e="T87" id="Seg_933" s="T86">finden-NEG.[3SG]</ta>
            <ta e="T88" id="Seg_934" s="T87">Märchen-VBZ-PST2-3SG</ta>
            <ta e="T89" id="Seg_935" s="T88">Ogdo</ta>
            <ta e="T90" id="Seg_936" s="T89">Aksjonova.[NOM]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_937" s="T1">мышь-ACC</ta>
            <ta e="T3" id="Seg_938" s="T2">с</ta>
            <ta e="T4" id="Seg_939" s="T3">олень.[NOM]</ta>
            <ta e="T5" id="Seg_940" s="T4">однажды</ta>
            <ta e="T6" id="Seg_941" s="T5">мышь.[NOM]</ta>
            <ta e="T7" id="Seg_942" s="T6">олень-ACC</ta>
            <ta e="T8" id="Seg_943" s="T7">видеть-CVB.SEQ</ta>
            <ta e="T10" id="Seg_944" s="T8">говорить-PST2.[3SG]</ta>
            <ta e="T11" id="Seg_945" s="T10">олень.[NOM]</ta>
            <ta e="T12" id="Seg_946" s="T11">прятаться-FREQ-CVB.SIM</ta>
            <ta e="T13" id="Seg_947" s="T12">играть-IMP.1DU</ta>
            <ta e="T14" id="Seg_948" s="T13">играть-IMP.1DU</ta>
            <ta e="T15" id="Seg_949" s="T14">радоваться-PST2.[3SG]</ta>
            <ta e="T16" id="Seg_950" s="T15">олень.[NOM]</ta>
            <ta e="T17" id="Seg_951" s="T16">сначала</ta>
            <ta e="T18" id="Seg_952" s="T17">2SG.[NOM]</ta>
            <ta e="T19" id="Seg_953" s="T18">прятаться.[IMP.2SG]</ta>
            <ta e="T20" id="Seg_954" s="T19">советовать-PST2.[3SG]</ta>
            <ta e="T21" id="Seg_955" s="T20">олень.[NOM]</ta>
            <ta e="T22" id="Seg_956" s="T21">1SG.[NOM]</ta>
            <ta e="T23" id="Seg_957" s="T22">2SG-ACC</ta>
            <ta e="T24" id="Seg_958" s="T23">искать-FUT-1SG</ta>
            <ta e="T25" id="Seg_959" s="T24">олень.[NOM]</ta>
            <ta e="T26" id="Seg_960" s="T25">прятаться-PST1-3SG</ta>
            <ta e="T27" id="Seg_961" s="T26">сколько</ta>
            <ta e="T28" id="Seg_962" s="T27">NEG</ta>
            <ta e="T29" id="Seg_963" s="T28">быть-NEG.CVB.SIM</ta>
            <ta e="T30" id="Seg_964" s="T29">мышь.[NOM]</ta>
            <ta e="T31" id="Seg_965" s="T30">искать-CVB.SIM</ta>
            <ta e="T32" id="Seg_966" s="T31">идти-PST2.[3SG]</ta>
            <ta e="T33" id="Seg_967" s="T32">земля-ACC</ta>
            <ta e="T34" id="Seg_968" s="T33">целый-3SG-ACC</ta>
            <ta e="T35" id="Seg_969" s="T34">%%-PST1-3SG</ta>
            <ta e="T36" id="Seg_970" s="T35">где</ta>
            <ta e="T37" id="Seg_971" s="T36">NEG</ta>
            <ta e="T38" id="Seg_972" s="T37">найти-NEG.[3SG]</ta>
            <ta e="T39" id="Seg_973" s="T38">куст-PROPR</ta>
            <ta e="T40" id="Seg_974" s="T39">место-DAT/LOC</ta>
            <ta e="T41" id="Seg_975" s="T40">доезжать-PST1-3SG</ta>
            <ta e="T42" id="Seg_976" s="T41">куст-PL.[NOM]</ta>
            <ta e="T43" id="Seg_977" s="T42">двигаться-PRS-3PL</ta>
            <ta e="T44" id="Seg_978" s="T43">ветер-ABL</ta>
            <ta e="T45" id="Seg_979" s="T44">два</ta>
            <ta e="T46" id="Seg_980" s="T45">куст.[NOM]</ta>
            <ta e="T47" id="Seg_981" s="T46">двигаться-NEG.[3SG]</ta>
            <ta e="T48" id="Seg_982" s="T47">мышь.[NOM]</ta>
            <ta e="T49" id="Seg_983" s="T48">тот</ta>
            <ta e="T50" id="Seg_984" s="T49">куст-PL-DAT/LOC</ta>
            <ta e="T51" id="Seg_985" s="T50">бегать-PST1-3SG</ta>
            <ta e="T52" id="Seg_986" s="T51">видеть-PST2-3SG</ta>
            <ta e="T53" id="Seg_987" s="T52">олень.[NOM]</ta>
            <ta e="T54" id="Seg_988" s="T53">рог-PL-3SG.[NOM]</ta>
            <ta e="T55" id="Seg_989" s="T54">теперь</ta>
            <ta e="T56" id="Seg_990" s="T55">1SG.[NOM]</ta>
            <ta e="T57" id="Seg_991" s="T56">прятаться-FUT-1SG</ta>
            <ta e="T58" id="Seg_992" s="T57">говорить-PST2.[3SG]</ta>
            <ta e="T59" id="Seg_993" s="T58">мышь.[NOM]</ta>
            <ta e="T60" id="Seg_994" s="T59">2SG.[NOM]</ta>
            <ta e="T61" id="Seg_995" s="T60">искать.[IMP.2SG]</ta>
            <ta e="T62" id="Seg_996" s="T61">мышь.[NOM]</ta>
            <ta e="T63" id="Seg_997" s="T62">бегать-CVB.SEQ</ta>
            <ta e="T64" id="Seg_998" s="T63">оставаться-PST1-3SG</ta>
            <ta e="T65" id="Seg_999" s="T64">прятаться-PST1-3SG</ta>
            <ta e="T66" id="Seg_1000" s="T65">олень.[NOM]</ta>
            <ta e="T67" id="Seg_1001" s="T66">искать-PRS.[3SG]</ta>
            <ta e="T68" id="Seg_1002" s="T67">искать-PRS.[3SG]</ta>
            <ta e="T69" id="Seg_1003" s="T68">вообще</ta>
            <ta e="T70" id="Seg_1004" s="T69">найти-NEG.[3SG]</ta>
            <ta e="T71" id="Seg_1005" s="T70">сила.[NOM]</ta>
            <ta e="T72" id="Seg_1006" s="T71">вот</ta>
            <ta e="T73" id="Seg_1007" s="T72">кончаться-PST1-3SG</ta>
            <ta e="T74" id="Seg_1008" s="T73">тот.[NOM]</ta>
            <ta e="T75" id="Seg_1009" s="T74">из_за</ta>
            <ta e="T76" id="Seg_1010" s="T75">олень.[NOM]</ta>
            <ta e="T77" id="Seg_1011" s="T76">теперь-DAT/LOC</ta>
            <ta e="T78" id="Seg_1012" s="T77">пока</ta>
            <ta e="T79" id="Seg_1013" s="T78">земля-ACC</ta>
            <ta e="T80" id="Seg_1014" s="T79">нюхать-PRS.[3SG]-нюхать-PRS.[3SG]</ta>
            <ta e="T81" id="Seg_1015" s="T80">EMPH-вот</ta>
            <ta e="T82" id="Seg_1016" s="T81">этот</ta>
            <ta e="T83" id="Seg_1017" s="T82">день-DAT/LOC</ta>
            <ta e="T84" id="Seg_1018" s="T83">пока</ta>
            <ta e="T85" id="Seg_1019" s="T84">олень.[NOM]</ta>
            <ta e="T86" id="Seg_1020" s="T85">мышь-ACC</ta>
            <ta e="T87" id="Seg_1021" s="T86">найти-NEG.[3SG]</ta>
            <ta e="T88" id="Seg_1022" s="T87">сказка-VBZ-PST2-3SG</ta>
            <ta e="T89" id="Seg_1023" s="T88">Огдуо</ta>
            <ta e="T90" id="Seg_1024" s="T89">Аксенова.[NOM]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_1025" s="T1">n-n:case</ta>
            <ta e="T3" id="Seg_1026" s="T2">post</ta>
            <ta e="T4" id="Seg_1027" s="T3">n.[n:case]</ta>
            <ta e="T5" id="Seg_1028" s="T4">adv</ta>
            <ta e="T6" id="Seg_1029" s="T5">n.[n:case]</ta>
            <ta e="T7" id="Seg_1030" s="T6">n-n:case</ta>
            <ta e="T8" id="Seg_1031" s="T7">v-v:cvb</ta>
            <ta e="T10" id="Seg_1032" s="T8">v-v:tense.[v:pred.pn]</ta>
            <ta e="T11" id="Seg_1033" s="T10">n.[n:case]</ta>
            <ta e="T12" id="Seg_1034" s="T11">v-v&gt;v-v:cvb</ta>
            <ta e="T13" id="Seg_1035" s="T12">v-v:mood.pn</ta>
            <ta e="T14" id="Seg_1036" s="T13">v-v:mood.pn</ta>
            <ta e="T15" id="Seg_1037" s="T14">v-v:tense.[v:pred.pn]</ta>
            <ta e="T16" id="Seg_1038" s="T15">n.[n:case]</ta>
            <ta e="T17" id="Seg_1039" s="T16">adv</ta>
            <ta e="T18" id="Seg_1040" s="T17">pers.[pro:case]</ta>
            <ta e="T19" id="Seg_1041" s="T18">v.[v:mood.pn]</ta>
            <ta e="T20" id="Seg_1042" s="T19">v-v:tense.[v:pred.pn]</ta>
            <ta e="T21" id="Seg_1043" s="T20">n.[n:case]</ta>
            <ta e="T22" id="Seg_1044" s="T21">pers.[pro:case]</ta>
            <ta e="T23" id="Seg_1045" s="T22">pers-pro:case</ta>
            <ta e="T24" id="Seg_1046" s="T23">v-v:tense-v:poss.pn</ta>
            <ta e="T25" id="Seg_1047" s="T24">n.[n:case]</ta>
            <ta e="T26" id="Seg_1048" s="T25">v-v:tense-v:poss.pn</ta>
            <ta e="T27" id="Seg_1049" s="T26">que</ta>
            <ta e="T28" id="Seg_1050" s="T27">ptcl</ta>
            <ta e="T29" id="Seg_1051" s="T28">v-v:cvb</ta>
            <ta e="T30" id="Seg_1052" s="T29">n.[n:case]</ta>
            <ta e="T31" id="Seg_1053" s="T30">v-v:cvb</ta>
            <ta e="T32" id="Seg_1054" s="T31">v-v:tense.[v:pred.pn]</ta>
            <ta e="T33" id="Seg_1055" s="T32">n-n:case</ta>
            <ta e="T34" id="Seg_1056" s="T33">adj-n:poss-n:case</ta>
            <ta e="T35" id="Seg_1057" s="T34">v-v:tense-v:poss.pn</ta>
            <ta e="T36" id="Seg_1058" s="T35">que</ta>
            <ta e="T37" id="Seg_1059" s="T36">ptcl</ta>
            <ta e="T38" id="Seg_1060" s="T37">v-v:(neg).[v:pred.pn]</ta>
            <ta e="T39" id="Seg_1061" s="T38">n-n&gt;adj</ta>
            <ta e="T40" id="Seg_1062" s="T39">n-n:case</ta>
            <ta e="T41" id="Seg_1063" s="T40">v-v:tense-v:poss.pn</ta>
            <ta e="T42" id="Seg_1064" s="T41">n-n:(num).[n:case]</ta>
            <ta e="T43" id="Seg_1065" s="T42">v-v:tense-v:pred.pn</ta>
            <ta e="T44" id="Seg_1066" s="T43">n-n:case</ta>
            <ta e="T45" id="Seg_1067" s="T44">cardnum</ta>
            <ta e="T46" id="Seg_1068" s="T45">n.[n:case]</ta>
            <ta e="T47" id="Seg_1069" s="T46">v-v:(neg).[v:pred.pn]</ta>
            <ta e="T48" id="Seg_1070" s="T47">n.[n:case]</ta>
            <ta e="T49" id="Seg_1071" s="T48">dempro</ta>
            <ta e="T50" id="Seg_1072" s="T49">n-n:(num)-n:case</ta>
            <ta e="T51" id="Seg_1073" s="T50">v-v:tense-v:poss.pn</ta>
            <ta e="T52" id="Seg_1074" s="T51">v-v:tense-v:poss.pn</ta>
            <ta e="T53" id="Seg_1075" s="T52">n.[n:case]</ta>
            <ta e="T54" id="Seg_1076" s="T53">n-n:(num)-n:(poss).[n:case]</ta>
            <ta e="T55" id="Seg_1077" s="T54">adv</ta>
            <ta e="T56" id="Seg_1078" s="T55">pers.[pro:case]</ta>
            <ta e="T57" id="Seg_1079" s="T56">v-v:tense-v:poss.pn</ta>
            <ta e="T58" id="Seg_1080" s="T57">v-v:tense.[v:pred.pn]</ta>
            <ta e="T59" id="Seg_1081" s="T58">n.[n:case]</ta>
            <ta e="T60" id="Seg_1082" s="T59">pers.[pro:case]</ta>
            <ta e="T61" id="Seg_1083" s="T60">v.[v:mood.pn]</ta>
            <ta e="T62" id="Seg_1084" s="T61">n.[n:case]</ta>
            <ta e="T63" id="Seg_1085" s="T62">v-v:cvb</ta>
            <ta e="T64" id="Seg_1086" s="T63">v-v:tense-v:poss.pn</ta>
            <ta e="T65" id="Seg_1087" s="T64">v-v:tense-v:poss.pn</ta>
            <ta e="T66" id="Seg_1088" s="T65">n.[n:case]</ta>
            <ta e="T67" id="Seg_1089" s="T66">v-v:tense.[v:pred.pn]</ta>
            <ta e="T68" id="Seg_1090" s="T67">v-v:tense.[v:pred.pn]</ta>
            <ta e="T69" id="Seg_1091" s="T68">adv</ta>
            <ta e="T70" id="Seg_1092" s="T69">v-v:(neg).[v:pred.pn]</ta>
            <ta e="T71" id="Seg_1093" s="T70">n.[n:case]</ta>
            <ta e="T72" id="Seg_1094" s="T71">ptcl</ta>
            <ta e="T73" id="Seg_1095" s="T72">v-v:tense-v:poss.pn</ta>
            <ta e="T74" id="Seg_1096" s="T73">dempro.[pro:case]</ta>
            <ta e="T75" id="Seg_1097" s="T74">post</ta>
            <ta e="T76" id="Seg_1098" s="T75">n.[n:case]</ta>
            <ta e="T77" id="Seg_1099" s="T76">adv-n:case</ta>
            <ta e="T78" id="Seg_1100" s="T77">post</ta>
            <ta e="T79" id="Seg_1101" s="T78">n-n:case</ta>
            <ta e="T80" id="Seg_1102" s="T79">v-v:tense.[v:pred.pn]-v-v:tense.[v:pred.pn]</ta>
            <ta e="T81" id="Seg_1103" s="T80">adv&gt;adv-adv</ta>
            <ta e="T82" id="Seg_1104" s="T81">dempro</ta>
            <ta e="T83" id="Seg_1105" s="T82">n-n:case</ta>
            <ta e="T84" id="Seg_1106" s="T83">post</ta>
            <ta e="T85" id="Seg_1107" s="T84">n.[n:case]</ta>
            <ta e="T86" id="Seg_1108" s="T85">n-n:case</ta>
            <ta e="T87" id="Seg_1109" s="T86">v-v:(neg).[v:pred.pn]</ta>
            <ta e="T88" id="Seg_1110" s="T87">n-n&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T89" id="Seg_1111" s="T88">propr</ta>
            <ta e="T90" id="Seg_1112" s="T89">propr.[n:case]</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_1113" s="T1">n</ta>
            <ta e="T3" id="Seg_1114" s="T2">post</ta>
            <ta e="T4" id="Seg_1115" s="T3">n</ta>
            <ta e="T5" id="Seg_1116" s="T4">adv</ta>
            <ta e="T6" id="Seg_1117" s="T5">n</ta>
            <ta e="T7" id="Seg_1118" s="T6">n</ta>
            <ta e="T8" id="Seg_1119" s="T7">v</ta>
            <ta e="T10" id="Seg_1120" s="T8">v</ta>
            <ta e="T11" id="Seg_1121" s="T10">n</ta>
            <ta e="T12" id="Seg_1122" s="T11">v</ta>
            <ta e="T13" id="Seg_1123" s="T12">v</ta>
            <ta e="T14" id="Seg_1124" s="T13">v</ta>
            <ta e="T15" id="Seg_1125" s="T14">v</ta>
            <ta e="T16" id="Seg_1126" s="T15">n</ta>
            <ta e="T17" id="Seg_1127" s="T16">adv</ta>
            <ta e="T18" id="Seg_1128" s="T17">pers</ta>
            <ta e="T19" id="Seg_1129" s="T18">v</ta>
            <ta e="T20" id="Seg_1130" s="T19">v</ta>
            <ta e="T21" id="Seg_1131" s="T20">n</ta>
            <ta e="T22" id="Seg_1132" s="T21">pers</ta>
            <ta e="T23" id="Seg_1133" s="T22">pers</ta>
            <ta e="T24" id="Seg_1134" s="T23">v</ta>
            <ta e="T25" id="Seg_1135" s="T24">n</ta>
            <ta e="T26" id="Seg_1136" s="T25">v</ta>
            <ta e="T27" id="Seg_1137" s="T26">que</ta>
            <ta e="T28" id="Seg_1138" s="T27">ptcl</ta>
            <ta e="T29" id="Seg_1139" s="T28">cop</ta>
            <ta e="T30" id="Seg_1140" s="T29">n</ta>
            <ta e="T31" id="Seg_1141" s="T30">v</ta>
            <ta e="T32" id="Seg_1142" s="T31">v</ta>
            <ta e="T33" id="Seg_1143" s="T32">n</ta>
            <ta e="T34" id="Seg_1144" s="T33">adj</ta>
            <ta e="T35" id="Seg_1145" s="T34">v</ta>
            <ta e="T36" id="Seg_1146" s="T35">que</ta>
            <ta e="T37" id="Seg_1147" s="T36">ptcl</ta>
            <ta e="T38" id="Seg_1148" s="T37">v</ta>
            <ta e="T39" id="Seg_1149" s="T38">adj</ta>
            <ta e="T40" id="Seg_1150" s="T39">n</ta>
            <ta e="T41" id="Seg_1151" s="T40">v</ta>
            <ta e="T42" id="Seg_1152" s="T41">n</ta>
            <ta e="T43" id="Seg_1153" s="T42">v</ta>
            <ta e="T44" id="Seg_1154" s="T43">n</ta>
            <ta e="T45" id="Seg_1155" s="T44">cardnum</ta>
            <ta e="T46" id="Seg_1156" s="T45">n</ta>
            <ta e="T47" id="Seg_1157" s="T46">v</ta>
            <ta e="T48" id="Seg_1158" s="T47">n</ta>
            <ta e="T49" id="Seg_1159" s="T48">dempro</ta>
            <ta e="T50" id="Seg_1160" s="T49">n</ta>
            <ta e="T51" id="Seg_1161" s="T50">v</ta>
            <ta e="T52" id="Seg_1162" s="T51">v</ta>
            <ta e="T53" id="Seg_1163" s="T52">n</ta>
            <ta e="T54" id="Seg_1164" s="T53">n</ta>
            <ta e="T55" id="Seg_1165" s="T54">adv</ta>
            <ta e="T56" id="Seg_1166" s="T55">pers</ta>
            <ta e="T57" id="Seg_1167" s="T56">v</ta>
            <ta e="T58" id="Seg_1168" s="T57">v</ta>
            <ta e="T59" id="Seg_1169" s="T58">n</ta>
            <ta e="T60" id="Seg_1170" s="T59">pers</ta>
            <ta e="T61" id="Seg_1171" s="T60">v</ta>
            <ta e="T62" id="Seg_1172" s="T61">n</ta>
            <ta e="T63" id="Seg_1173" s="T62">v</ta>
            <ta e="T64" id="Seg_1174" s="T63">aux</ta>
            <ta e="T65" id="Seg_1175" s="T64">v</ta>
            <ta e="T66" id="Seg_1176" s="T65">n</ta>
            <ta e="T67" id="Seg_1177" s="T66">v</ta>
            <ta e="T68" id="Seg_1178" s="T67">v</ta>
            <ta e="T69" id="Seg_1179" s="T68">adv</ta>
            <ta e="T70" id="Seg_1180" s="T69">v</ta>
            <ta e="T71" id="Seg_1181" s="T70">n</ta>
            <ta e="T72" id="Seg_1182" s="T71">ptcl</ta>
            <ta e="T73" id="Seg_1183" s="T72">v</ta>
            <ta e="T74" id="Seg_1184" s="T73">dempro</ta>
            <ta e="T75" id="Seg_1185" s="T74">post</ta>
            <ta e="T76" id="Seg_1186" s="T75">n</ta>
            <ta e="T77" id="Seg_1187" s="T76">adv</ta>
            <ta e="T78" id="Seg_1188" s="T77">post</ta>
            <ta e="T79" id="Seg_1189" s="T78">n</ta>
            <ta e="T80" id="Seg_1190" s="T79">v</ta>
            <ta e="T81" id="Seg_1191" s="T80">adv</ta>
            <ta e="T82" id="Seg_1192" s="T81">dempro</ta>
            <ta e="T83" id="Seg_1193" s="T82">n</ta>
            <ta e="T84" id="Seg_1194" s="T83">post</ta>
            <ta e="T85" id="Seg_1195" s="T84">n</ta>
            <ta e="T86" id="Seg_1196" s="T85">n</ta>
            <ta e="T87" id="Seg_1197" s="T86">v</ta>
            <ta e="T88" id="Seg_1198" s="T87">v</ta>
            <ta e="T89" id="Seg_1199" s="T88">propr</ta>
            <ta e="T90" id="Seg_1200" s="T89">propr</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T3" id="Seg_1201" s="T1">pp:Com</ta>
            <ta e="T5" id="Seg_1202" s="T4">adv:Time</ta>
            <ta e="T6" id="Seg_1203" s="T5">np.h:A</ta>
            <ta e="T13" id="Seg_1204" s="T12">0.1.h:A</ta>
            <ta e="T14" id="Seg_1205" s="T13">0.1.h:A</ta>
            <ta e="T16" id="Seg_1206" s="T15">np.h:E</ta>
            <ta e="T18" id="Seg_1207" s="T17">pro.h:A</ta>
            <ta e="T21" id="Seg_1208" s="T20">np.h:A</ta>
            <ta e="T22" id="Seg_1209" s="T21">pro.h:A</ta>
            <ta e="T23" id="Seg_1210" s="T22">pro.h:Th</ta>
            <ta e="T25" id="Seg_1211" s="T24">np.h:A</ta>
            <ta e="T30" id="Seg_1212" s="T29">np.h:A</ta>
            <ta e="T33" id="Seg_1213" s="T32">np:Path</ta>
            <ta e="T35" id="Seg_1214" s="T34">0.3.h:A</ta>
            <ta e="T36" id="Seg_1215" s="T35">pro:L</ta>
            <ta e="T38" id="Seg_1216" s="T37">0.3.h:E</ta>
            <ta e="T40" id="Seg_1217" s="T39">np:G</ta>
            <ta e="T41" id="Seg_1218" s="T40">0.3.h:Th</ta>
            <ta e="T42" id="Seg_1219" s="T41">np:Th</ta>
            <ta e="T44" id="Seg_1220" s="T43">np:Cau</ta>
            <ta e="T46" id="Seg_1221" s="T45">np:Th</ta>
            <ta e="T48" id="Seg_1222" s="T47">np.h:A</ta>
            <ta e="T50" id="Seg_1223" s="T49">np:G</ta>
            <ta e="T52" id="Seg_1224" s="T51">0.3.h:A</ta>
            <ta e="T53" id="Seg_1225" s="T52">np.h:Poss</ta>
            <ta e="T55" id="Seg_1226" s="T54">adv:Time</ta>
            <ta e="T56" id="Seg_1227" s="T55">pro.h:A</ta>
            <ta e="T59" id="Seg_1228" s="T58">np.h:A</ta>
            <ta e="T60" id="Seg_1229" s="T59">pro.h:A</ta>
            <ta e="T62" id="Seg_1230" s="T61">np.h:A</ta>
            <ta e="T65" id="Seg_1231" s="T64">0.3.h:A</ta>
            <ta e="T66" id="Seg_1232" s="T65">np.h:A</ta>
            <ta e="T70" id="Seg_1233" s="T69">0.3.h:E</ta>
            <ta e="T71" id="Seg_1234" s="T70">np:Th</ta>
            <ta e="T76" id="Seg_1235" s="T75">np.h:A</ta>
            <ta e="T78" id="Seg_1236" s="T76">pp:Time</ta>
            <ta e="T79" id="Seg_1237" s="T78">np:Th</ta>
            <ta e="T84" id="Seg_1238" s="T82">pp:Time</ta>
            <ta e="T85" id="Seg_1239" s="T84">np.h:E</ta>
            <ta e="T86" id="Seg_1240" s="T85">np.h:Th</ta>
            <ta e="T90" id="Seg_1241" s="T89">np.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T6" id="Seg_1242" s="T5">np.h:S</ta>
            <ta e="T8" id="Seg_1243" s="T6">s:temp</ta>
            <ta e="T10" id="Seg_1244" s="T8">v:pred</ta>
            <ta e="T13" id="Seg_1245" s="T12">0.1.h:S v:pred</ta>
            <ta e="T14" id="Seg_1246" s="T13">0.1.h:S v:pred</ta>
            <ta e="T15" id="Seg_1247" s="T14">v:pred</ta>
            <ta e="T16" id="Seg_1248" s="T15">np.h:S</ta>
            <ta e="T18" id="Seg_1249" s="T17">pro.h:S</ta>
            <ta e="T19" id="Seg_1250" s="T18">v:pred</ta>
            <ta e="T20" id="Seg_1251" s="T19">v:pred</ta>
            <ta e="T21" id="Seg_1252" s="T20">np.h:S</ta>
            <ta e="T22" id="Seg_1253" s="T21">pro.h:S</ta>
            <ta e="T23" id="Seg_1254" s="T22">pro.h:O</ta>
            <ta e="T24" id="Seg_1255" s="T23">v:pred</ta>
            <ta e="T25" id="Seg_1256" s="T24">np.h:S</ta>
            <ta e="T26" id="Seg_1257" s="T25">v:pred</ta>
            <ta e="T29" id="Seg_1258" s="T26">s:adv</ta>
            <ta e="T30" id="Seg_1259" s="T29">np.h:S</ta>
            <ta e="T31" id="Seg_1260" s="T30">s:purp</ta>
            <ta e="T32" id="Seg_1261" s="T31">v:pred</ta>
            <ta e="T33" id="Seg_1262" s="T32">np:O</ta>
            <ta e="T35" id="Seg_1263" s="T34">0.3.h:S v:pred</ta>
            <ta e="T38" id="Seg_1264" s="T37">0.3.h:S v:pred</ta>
            <ta e="T41" id="Seg_1265" s="T40">0.3.h:S v:pred</ta>
            <ta e="T42" id="Seg_1266" s="T41">np:S</ta>
            <ta e="T43" id="Seg_1267" s="T42">v:pred</ta>
            <ta e="T46" id="Seg_1268" s="T45">np:S</ta>
            <ta e="T47" id="Seg_1269" s="T46">v:pred</ta>
            <ta e="T48" id="Seg_1270" s="T47">np.h:S</ta>
            <ta e="T51" id="Seg_1271" s="T50">v:pred</ta>
            <ta e="T52" id="Seg_1272" s="T51">0.3.h:S v:pred</ta>
            <ta e="T56" id="Seg_1273" s="T55">pro.h:S</ta>
            <ta e="T57" id="Seg_1274" s="T56">v:pred</ta>
            <ta e="T58" id="Seg_1275" s="T57">v:pred</ta>
            <ta e="T59" id="Seg_1276" s="T58">np.h:S</ta>
            <ta e="T60" id="Seg_1277" s="T59">pro.h:S</ta>
            <ta e="T61" id="Seg_1278" s="T60">v:pred</ta>
            <ta e="T62" id="Seg_1279" s="T61">np.h:S</ta>
            <ta e="T64" id="Seg_1280" s="T62">v:pred</ta>
            <ta e="T65" id="Seg_1281" s="T64">0.3.h:S v:pred</ta>
            <ta e="T66" id="Seg_1282" s="T65">np.h:S</ta>
            <ta e="T67" id="Seg_1283" s="T66">v:pred</ta>
            <ta e="T68" id="Seg_1284" s="T67">v:pred</ta>
            <ta e="T70" id="Seg_1285" s="T69">0.3.h:S v:pred</ta>
            <ta e="T71" id="Seg_1286" s="T70">np:S</ta>
            <ta e="T73" id="Seg_1287" s="T72">v:pred</ta>
            <ta e="T76" id="Seg_1288" s="T75">np.h:S</ta>
            <ta e="T79" id="Seg_1289" s="T78">np:O</ta>
            <ta e="T80" id="Seg_1290" s="T79">v:pred</ta>
            <ta e="T85" id="Seg_1291" s="T84">np.h:S</ta>
            <ta e="T86" id="Seg_1292" s="T85">np.h:O</ta>
            <ta e="T87" id="Seg_1293" s="T86">v:pred</ta>
            <ta e="T88" id="Seg_1294" s="T87">v:pred</ta>
            <ta e="T90" id="Seg_1295" s="T89">np.h:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST">
            <ta e="T6" id="Seg_1296" s="T5">new</ta>
            <ta e="T7" id="Seg_1297" s="T6">new</ta>
            <ta e="T10" id="Seg_1298" s="T8">quot-sp</ta>
            <ta e="T11" id="Seg_1299" s="T10">giv-active-Q</ta>
            <ta e="T13" id="Seg_1300" s="T12">0.giv-active-Q</ta>
            <ta e="T14" id="Seg_1301" s="T13">0.giv-active-Q</ta>
            <ta e="T16" id="Seg_1302" s="T15">giv-active</ta>
            <ta e="T18" id="Seg_1303" s="T17">giv-active-Q</ta>
            <ta e="T20" id="Seg_1304" s="T19">quot-sp</ta>
            <ta e="T21" id="Seg_1305" s="T20">giv-active</ta>
            <ta e="T22" id="Seg_1306" s="T21">giv-active-Q</ta>
            <ta e="T23" id="Seg_1307" s="T22">giv-active-Q</ta>
            <ta e="T25" id="Seg_1308" s="T24">giv-active</ta>
            <ta e="T30" id="Seg_1309" s="T29">giv-inactive</ta>
            <ta e="T33" id="Seg_1310" s="T32">accs-inf</ta>
            <ta e="T35" id="Seg_1311" s="T34">0.giv-active</ta>
            <ta e="T38" id="Seg_1312" s="T37">0.giv-active</ta>
            <ta e="T39" id="Seg_1313" s="T38">new</ta>
            <ta e="T40" id="Seg_1314" s="T39">new</ta>
            <ta e="T41" id="Seg_1315" s="T40">0.giv-active</ta>
            <ta e="T42" id="Seg_1316" s="T41">giv-active</ta>
            <ta e="T44" id="Seg_1317" s="T43">accs-inf</ta>
            <ta e="T46" id="Seg_1318" s="T45">accs-inf</ta>
            <ta e="T48" id="Seg_1319" s="T47">giv-inactive</ta>
            <ta e="T50" id="Seg_1320" s="T49">giv-active</ta>
            <ta e="T52" id="Seg_1321" s="T51">0.giv-active</ta>
            <ta e="T53" id="Seg_1322" s="T52">giv-inactive</ta>
            <ta e="T54" id="Seg_1323" s="T53">accs-inf</ta>
            <ta e="T56" id="Seg_1324" s="T55">giv-active-Q</ta>
            <ta e="T58" id="Seg_1325" s="T57">quot-sp</ta>
            <ta e="T59" id="Seg_1326" s="T58">giv-active</ta>
            <ta e="T60" id="Seg_1327" s="T59">giv-active-Q</ta>
            <ta e="T62" id="Seg_1328" s="T61">giv-active</ta>
            <ta e="T65" id="Seg_1329" s="T64">0.giv-active</ta>
            <ta e="T66" id="Seg_1330" s="T65">giv-inactive</ta>
            <ta e="T70" id="Seg_1331" s="T69">0.giv-active</ta>
            <ta e="T71" id="Seg_1332" s="T70">accs-inf</ta>
            <ta e="T76" id="Seg_1333" s="T75">giv-inactive</ta>
            <ta e="T79" id="Seg_1334" s="T78">giv-inactive</ta>
            <ta e="T85" id="Seg_1335" s="T84">giv-active</ta>
            <ta e="T86" id="Seg_1336" s="T85">giv-inactive</ta>
            <ta e="T90" id="Seg_1337" s="T89">new</ta>
         </annotation>
         <annotation name="Top" tierref="Top" />
         <annotation name="Foc" tierref="Foc" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T90" id="Seg_1338" s="T89">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T90" id="Seg_1339" s="T89">dir:bare</ta>
         </annotation>
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_1340" s="T1">The mouse and the reindeer.</ta>
            <ta e="T10" id="Seg_1341" s="T4">Once the mouse met the reindeer and said:</ta>
            <ta e="T13" id="Seg_1342" s="T10">"Reindeer, let us play hide and seek."</ta>
            <ta e="T16" id="Seg_1343" s="T13">"Let's play", the reindeer was happy.</ta>
            <ta e="T21" id="Seg_1344" s="T16">"At first you hide", the reindeer advised.</ta>
            <ta e="T24" id="Seg_1345" s="T21">"I will search you."</ta>
            <ta e="T26" id="Seg_1346" s="T24">The reindeer hid.</ta>
            <ta e="T32" id="Seg_1347" s="T26">After a short time the mouse went off to search.</ta>
            <ta e="T38" id="Seg_1348" s="T32">The mouse went(?) through the whole land, it found [it] nowhere.</ta>
            <ta e="T41" id="Seg_1349" s="T38">It came to a bushy place.</ta>
            <ta e="T47" id="Seg_1350" s="T41">The bushes are moving because of the wind, two bushes are not moving.</ta>
            <ta e="T51" id="Seg_1351" s="T47">The mouse ran towards those two bushes.</ta>
            <ta e="T54" id="Seg_1352" s="T51">It looked, the reindeer's antlers.</ta>
            <ta e="T61" id="Seg_1353" s="T54">"Now I will hide", the mouse said, "you search."</ta>
            <ta e="T64" id="Seg_1354" s="T61">The mouse ran off.</ta>
            <ta e="T65" id="Seg_1355" s="T64">It hid.</ta>
            <ta e="T70" id="Seg_1356" s="T65">The reindeer searches and searches, it doesn't find [the mouse] at all.</ta>
            <ta e="T73" id="Seg_1357" s="T70">[Its] power came to an end.</ta>
            <ta e="T87" id="Seg_1358" s="T73">Therefore the reindeer is sniffing the earth until now, and so it doesn't find [it] until now.</ta>
            <ta e="T90" id="Seg_1359" s="T87">The tale was told by Ogduo Aksyonova.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_1360" s="T1">Die Maus und das Rentier.</ta>
            <ta e="T10" id="Seg_1361" s="T4">Einmal traf die Maus das Rentier und sagte:</ta>
            <ta e="T13" id="Seg_1362" s="T10">"Rentier, lass uns verstecken spielen."</ta>
            <ta e="T16" id="Seg_1363" s="T13">"Lass uns spielen", freute sich das Rentier.</ta>
            <ta e="T21" id="Seg_1364" s="T16">"Zuerst versteck du dich", riet das Rentier.</ta>
            <ta e="T24" id="Seg_1365" s="T21">"Ich werde dich suchen."</ta>
            <ta e="T26" id="Seg_1366" s="T24">Das Rentier versteckte sich.</ta>
            <ta e="T32" id="Seg_1367" s="T26">Nach kurzer Zeit ging die Maus los um zu suchen.</ta>
            <ta e="T38" id="Seg_1368" s="T32">Die Maus lief(?) durchs ganze Land, sie findet [es] nirgendwo.</ta>
            <ta e="T41" id="Seg_1369" s="T38">Sie kam zu einem Ort mit Büschen.</ta>
            <ta e="T47" id="Seg_1370" s="T41">Die Büsche schwanken vom Wind, zwei Büsche bewegen sich nicht.</ta>
            <ta e="T51" id="Seg_1371" s="T47">Die Maus lief zu jenen Büschen hin.</ta>
            <ta e="T54" id="Seg_1372" s="T51">Sie schaute, das Geweih des Rentiers.</ta>
            <ta e="T61" id="Seg_1373" s="T54">"Jetzt verstecke ich mich", sagte die Maus, "such du."</ta>
            <ta e="T64" id="Seg_1374" s="T61">Die Maus lief los.</ta>
            <ta e="T65" id="Seg_1375" s="T64">Sie versteckte sich.</ta>
            <ta e="T70" id="Seg_1376" s="T65">Das Rentier sucht und sucht, es findet [sie] überhaupt nicht.</ta>
            <ta e="T73" id="Seg_1377" s="T70">[Seine] Kräfte gingen zuende.</ta>
            <ta e="T87" id="Seg_1378" s="T73">Deshalb schnuppert und schnuppert das Rentier bis jetzt an der Erde, und so findet es [sie] bis heute nicht.</ta>
            <ta e="T90" id="Seg_1379" s="T87">Das Märchen erzählte Ogduo Aksjonova.</ta>
         </annotation>
         <annotation name="fr" tierref="fr" />
         <annotation name="ltr" tierref="ltr">
            <ta e="T4" id="Seg_1380" s="T1">Мышка и олень.</ta>
            <ta e="T10" id="Seg_1381" s="T4">Однажды мышка, оленя встретив, сказал: </ta>
            <ta e="T13" id="Seg_1382" s="T10">"Олень, в прятки давай поиграем."</ta>
            <ta e="T16" id="Seg_1383" s="T13">"Поиграем, – обрадовался олень, </ta>
            <ta e="T21" id="Seg_1384" s="T16">– начала ты спячься" – посоветовал олень.</ta>
            <ta e="T24" id="Seg_1385" s="T21">– а я тебя поищу.</ta>
            <ta e="T26" id="Seg_1386" s="T24">Олень спрятался.</ta>
            <ta e="T32" id="Seg_1387" s="T26">Через короткое время мышка искать пошла.</ta>
            <ta e="T38" id="Seg_1388" s="T32">землю всю быстро побежала, нигде не находит. </ta>
            <ta e="T41" id="Seg_1389" s="T38">До с кустами места дошла.</ta>
            <ta e="T47" id="Seg_1390" s="T41">Кусты шевелятся от ветра, два куста стоят не шевелясь.</ta>
            <ta e="T51" id="Seg_1391" s="T47">Мышка к тем кустам побежала. </ta>
            <ta e="T54" id="Seg_1392" s="T51">Смотрит, оленьии рога. </ta>
            <ta e="T61" id="Seg_1393" s="T54">"Сейчас я спрячусь, – сказала мышка, – ты ищи".</ta>
            <ta e="T64" id="Seg_1394" s="T61">Мышка убежала.</ta>
            <ta e="T65" id="Seg_1395" s="T64">Спряталась. </ta>
            <ta e="T70" id="Seg_1396" s="T65">Олень ищет, ищет, никак не может найти. </ta>
            <ta e="T73" id="Seg_1397" s="T70">Силы иссякли. </ta>
            <ta e="T87" id="Seg_1398" s="T73">Поэтому олень до сих пор землю нюхает-нюхает, и так до этого дня олень мышку не находит.</ta>
            <ta e="T90" id="Seg_1399" s="T87">Сказку рассказала Огдуо Аксёнова.</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg"
                          name="gg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top"
                          name="Top"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc"
                          name="Foc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
