<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDED646240-8449-4971-8DA1-AAA0FDAE2E49">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>PoS_PrG_1964_Kaamyylaak_flk</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\flk\PoS_PrG_1964_Kaamyylaak_flk\PoS_PrG_1964_Kaamyylaak_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">507</ud-information>
            <ud-information attribute-name="# HIAT:w">353</ud-information>
            <ud-information attribute-name="# e">353</ud-information>
            <ud-information attribute-name="# HIAT:u">76</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PoS">
            <abbreviation>PoS</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="4.0" type="appl" />
         <tli id="T1" time="4.5" type="appl" />
         <tli id="T2" time="5.0" type="appl" />
         <tli id="T3" time="5.5" type="appl" />
         <tli id="T4" time="6.0" type="appl" />
         <tli id="T5" time="6.5" type="appl" />
         <tli id="T6" time="7.0" type="appl" />
         <tli id="T7" time="7.5" type="appl" />
         <tli id="T8" time="8.0" type="appl" />
         <tli id="T9" time="8.5" type="appl" />
         <tli id="T10" time="9.0" type="appl" />
         <tli id="T11" time="9.5" type="appl" />
         <tli id="T12" time="10.0" type="appl" />
         <tli id="T13" time="10.5" type="appl" />
         <tli id="T14" time="11.0" type="appl" />
         <tli id="T15" time="11.5" type="appl" />
         <tli id="T16" time="12.0" type="appl" />
         <tli id="T17" time="12.5" type="appl" />
         <tli id="T18" time="13.0" type="appl" />
         <tli id="T19" time="13.5" type="appl" />
         <tli id="T20" time="14.0" type="appl" />
         <tli id="T21" time="14.5" type="appl" />
         <tli id="T22" time="15.0" type="appl" />
         <tli id="T23" time="15.5" type="appl" />
         <tli id="T24" time="16.0" type="appl" />
         <tli id="T25" time="16.5" type="appl" />
         <tli id="T26" time="17.0" type="appl" />
         <tli id="T27" time="17.5" type="appl" />
         <tli id="T28" time="18.0" type="appl" />
         <tli id="T29" time="18.5" type="appl" />
         <tli id="T30" time="19.0" type="appl" />
         <tli id="T31" time="19.5" type="appl" />
         <tli id="T32" time="20.0" type="appl" />
         <tli id="T33" time="20.5" type="appl" />
         <tli id="T34" time="21.0" type="appl" />
         <tli id="T35" time="21.5" type="appl" />
         <tli id="T36" time="22.0" type="appl" />
         <tli id="T37" time="22.5" type="appl" />
         <tli id="T38" time="23.0" type="appl" />
         <tli id="T39" time="23.5" type="appl" />
         <tli id="T40" time="24.0" type="appl" />
         <tli id="T41" time="24.5" type="appl" />
         <tli id="T42" time="25.0" type="appl" />
         <tli id="T43" time="25.5" type="appl" />
         <tli id="T44" time="26.0" type="appl" />
         <tli id="T45" time="26.5" type="appl" />
         <tli id="T46" time="27.0" type="appl" />
         <tli id="T47" time="27.5" type="appl" />
         <tli id="T48" time="28.0" type="appl" />
         <tli id="T49" time="28.5" type="appl" />
         <tli id="T50" time="29.0" type="appl" />
         <tli id="T51" time="29.5" type="appl" />
         <tli id="T52" time="30.0" type="appl" />
         <tli id="T53" time="30.5" type="appl" />
         <tli id="T54" time="31.0" type="appl" />
         <tli id="T55" time="31.5" type="appl" />
         <tli id="T56" time="32.0" type="appl" />
         <tli id="T57" time="32.5" type="appl" />
         <tli id="T58" time="33.0" type="appl" />
         <tli id="T59" time="33.5" type="appl" />
         <tli id="T60" time="34.0" type="appl" />
         <tli id="T61" time="34.5" type="appl" />
         <tli id="T62" time="35.0" type="appl" />
         <tli id="T63" time="35.5" type="appl" />
         <tli id="T64" time="36.0" type="appl" />
         <tli id="T65" time="36.5" type="appl" />
         <tli id="T66" time="37.0" type="appl" />
         <tli id="T67" time="37.5" type="appl" />
         <tli id="T68" time="38.0" type="appl" />
         <tli id="T69" time="38.5" type="appl" />
         <tli id="T70" time="39.0" type="appl" />
         <tli id="T71" time="39.5" type="appl" />
         <tli id="T72" time="40.0" type="appl" />
         <tli id="T73" time="40.5" type="appl" />
         <tli id="T74" time="41.0" type="appl" />
         <tli id="T75" time="41.5" type="appl" />
         <tli id="T76" time="42.0" type="appl" />
         <tli id="T77" time="42.5" type="appl" />
         <tli id="T78" time="43.0" type="appl" />
         <tli id="T79" time="43.5" type="appl" />
         <tli id="T80" time="44.0" type="appl" />
         <tli id="T81" time="44.5" type="appl" />
         <tli id="T82" time="45.0" type="appl" />
         <tli id="T83" time="45.5" type="appl" />
         <tli id="T84" time="46.0" type="appl" />
         <tli id="T85" time="46.5" type="appl" />
         <tli id="T86" time="47.0" type="appl" />
         <tli id="T87" time="47.5" type="appl" />
         <tli id="T88" time="48.0" type="appl" />
         <tli id="T89" time="48.5" type="appl" />
         <tli id="T90" time="49.0" type="appl" />
         <tli id="T91" time="49.5" type="appl" />
         <tli id="T92" time="50.0" type="appl" />
         <tli id="T93" time="50.5" type="appl" />
         <tli id="T94" time="51.0" type="appl" />
         <tli id="T95" time="51.5" type="appl" />
         <tli id="T96" time="52.0" type="appl" />
         <tli id="T97" time="52.5" type="appl" />
         <tli id="T98" time="53.0" type="appl" />
         <tli id="T99" time="53.5" type="appl" />
         <tli id="T100" time="54.0" type="appl" />
         <tli id="T101" time="54.5" type="appl" />
         <tli id="T102" time="55.0" type="appl" />
         <tli id="T103" time="55.5" type="appl" />
         <tli id="T104" time="56.0" type="appl" />
         <tli id="T105" time="56.5" type="appl" />
         <tli id="T106" time="57.0" type="appl" />
         <tli id="T107" time="57.5" type="appl" />
         <tli id="T108" time="58.0" type="appl" />
         <tli id="T109" time="58.5" type="appl" />
         <tli id="T110" time="59.0" type="appl" />
         <tli id="T111" time="59.5" type="appl" />
         <tli id="T112" time="60.0" type="appl" />
         <tli id="T113" time="60.5" type="appl" />
         <tli id="T114" time="61.0" type="appl" />
         <tli id="T115" time="61.5" type="appl" />
         <tli id="T116" time="62.0" type="appl" />
         <tli id="T117" time="62.5" type="appl" />
         <tli id="T118" time="63.0" type="appl" />
         <tli id="T119" time="63.5" type="appl" />
         <tli id="T120" time="64.0" type="appl" />
         <tli id="T121" time="64.5" type="appl" />
         <tli id="T122" time="65.0" type="appl" />
         <tli id="T123" time="65.5" type="appl" />
         <tli id="T124" time="66.0" type="appl" />
         <tli id="T125" time="66.5" type="appl" />
         <tli id="T126" time="67.0" type="appl" />
         <tli id="T127" time="67.5" type="appl" />
         <tli id="T128" time="68.0" type="appl" />
         <tli id="T129" time="68.5" type="appl" />
         <tli id="T130" time="69.0" type="appl" />
         <tli id="T131" time="69.5" type="appl" />
         <tli id="T132" time="70.0" type="appl" />
         <tli id="T133" time="70.5" type="appl" />
         <tli id="T134" time="71.0" type="appl" />
         <tli id="T135" time="71.5" type="appl" />
         <tli id="T136" time="72.0" type="appl" />
         <tli id="T137" time="72.5" type="appl" />
         <tli id="T138" time="73.0" type="appl" />
         <tli id="T139" time="73.5" type="appl" />
         <tli id="T140" time="74.0" type="appl" />
         <tli id="T141" time="74.5" type="appl" />
         <tli id="T142" time="75.0" type="appl" />
         <tli id="T143" time="75.5" type="appl" />
         <tli id="T144" time="76.0" type="appl" />
         <tli id="T145" time="76.5" type="appl" />
         <tli id="T146" time="77.0" type="appl" />
         <tli id="T147" time="77.5" type="appl" />
         <tli id="T148" time="78.0" type="appl" />
         <tli id="T149" time="78.5" type="appl" />
         <tli id="T150" time="79.0" type="appl" />
         <tli id="T151" time="79.5" type="appl" />
         <tli id="T152" time="80.0" type="appl" />
         <tli id="T153" time="80.5" type="appl" />
         <tli id="T154" time="81.0" type="appl" />
         <tli id="T155" time="81.5" type="appl" />
         <tli id="T156" time="82.0" type="appl" />
         <tli id="T157" time="82.5" type="appl" />
         <tli id="T158" time="83.0" type="appl" />
         <tli id="T159" time="83.5" type="appl" />
         <tli id="T160" time="84.0" type="appl" />
         <tli id="T161" time="84.5" type="appl" />
         <tli id="T162" time="85.0" type="appl" />
         <tli id="T163" time="85.5" type="appl" />
         <tli id="T164" time="86.0" type="appl" />
         <tli id="T165" time="86.5" type="appl" />
         <tli id="T166" time="87.0" type="appl" />
         <tli id="T167" time="87.5" type="appl" />
         <tli id="T168" time="88.0" type="appl" />
         <tli id="T169" time="88.5" type="appl" />
         <tli id="T170" time="89.0" type="appl" />
         <tli id="T171" time="89.5" type="appl" />
         <tli id="T172" time="90.0" type="appl" />
         <tli id="T173" time="90.5" type="appl" />
         <tli id="T174" time="91.0" type="appl" />
         <tli id="T175" time="91.5" type="appl" />
         <tli id="T176" time="92.0" type="appl" />
         <tli id="T177" time="92.5" type="appl" />
         <tli id="T178" time="93.0" type="appl" />
         <tli id="T179" time="93.5" type="appl" />
         <tli id="T180" time="94.0" type="appl" />
         <tli id="T181" time="94.5" type="appl" />
         <tli id="T182" time="95.0" type="appl" />
         <tli id="T183" time="95.5" type="appl" />
         <tli id="T184" time="96.0" type="appl" />
         <tli id="T185" time="96.5" type="appl" />
         <tli id="T186" time="97.0" type="appl" />
         <tli id="T187" time="97.5" type="appl" />
         <tli id="T188" time="98.0" type="appl" />
         <tli id="T189" time="98.5" type="appl" />
         <tli id="T190" time="99.0" type="appl" />
         <tli id="T191" time="99.5" type="appl" />
         <tli id="T192" time="100.0" type="appl" />
         <tli id="T193" time="100.5" type="appl" />
         <tli id="T194" time="101.0" type="appl" />
         <tli id="T195" time="101.5" type="appl" />
         <tli id="T196" time="102.0" type="appl" />
         <tli id="T197" time="102.5" type="appl" />
         <tli id="T198" time="103.0" type="appl" />
         <tli id="T199" time="103.5" type="appl" />
         <tli id="T200" time="104.0" type="appl" />
         <tli id="T201" time="104.5" type="appl" />
         <tli id="T202" time="105.0" type="appl" />
         <tli id="T203" time="105.5" type="appl" />
         <tli id="T204" time="106.0" type="appl" />
         <tli id="T205" time="106.5" type="appl" />
         <tli id="T206" time="107.0" type="appl" />
         <tli id="T207" time="107.5" type="appl" />
         <tli id="T208" time="108.0" type="appl" />
         <tli id="T209" time="108.5" type="appl" />
         <tli id="T210" time="109.0" type="appl" />
         <tli id="T211" time="109.5" type="appl" />
         <tli id="T212" time="110.0" type="appl" />
         <tli id="T213" time="110.5" type="appl" />
         <tli id="T214" time="111.0" type="appl" />
         <tli id="T215" time="111.5" type="appl" />
         <tli id="T216" time="112.0" type="appl" />
         <tli id="T217" time="112.5" type="appl" />
         <tli id="T218" time="113.0" type="appl" />
         <tli id="T219" time="113.5" type="appl" />
         <tli id="T220" time="114.0" type="appl" />
         <tli id="T221" time="114.5" type="appl" />
         <tli id="T222" time="115.0" type="appl" />
         <tli id="T223" time="115.5" type="appl" />
         <tli id="T224" time="116.0" type="appl" />
         <tli id="T225" time="116.5" type="appl" />
         <tli id="T226" time="117.0" type="appl" />
         <tli id="T227" time="117.5" type="appl" />
         <tli id="T228" time="118.0" type="appl" />
         <tli id="T229" time="118.5" type="appl" />
         <tli id="T230" time="119.0" type="appl" />
         <tli id="T231" time="119.5" type="appl" />
         <tli id="T232" time="120.0" type="appl" />
         <tli id="T233" time="120.5" type="appl" />
         <tli id="T234" time="121.0" type="appl" />
         <tli id="T235" time="121.5" type="appl" />
         <tli id="T236" time="122.0" type="appl" />
         <tli id="T237" time="122.5" type="appl" />
         <tli id="T238" time="123.0" type="appl" />
         <tli id="T239" time="123.5" type="appl" />
         <tli id="T240" time="124.0" type="appl" />
         <tli id="T241" time="124.5" type="appl" />
         <tli id="T242" time="125.0" type="appl" />
         <tli id="T243" time="125.5" type="appl" />
         <tli id="T244" time="126.0" type="appl" />
         <tli id="T245" time="126.5" type="appl" />
         <tli id="T246" time="127.0" type="appl" />
         <tli id="T247" time="127.5" type="appl" />
         <tli id="T248" time="128.0" type="appl" />
         <tli id="T249" time="128.5" type="appl" />
         <tli id="T250" time="129.0" type="appl" />
         <tli id="T251" time="129.5" type="appl" />
         <tli id="T252" time="130.0" type="appl" />
         <tli id="T253" time="130.5" type="appl" />
         <tli id="T254" time="131.0" type="appl" />
         <tli id="T255" time="131.5" type="appl" />
         <tli id="T256" time="132.0" type="appl" />
         <tli id="T257" time="132.5" type="appl" />
         <tli id="T258" time="133.0" type="appl" />
         <tli id="T259" time="133.5" type="appl" />
         <tli id="T260" time="134.0" type="appl" />
         <tli id="T261" time="134.5" type="appl" />
         <tli id="T262" time="135.0" type="appl" />
         <tli id="T263" time="135.5" type="appl" />
         <tli id="T264" time="136.0" type="appl" />
         <tli id="T265" time="136.5" type="appl" />
         <tli id="T266" time="137.0" type="appl" />
         <tli id="T267" time="137.5" type="appl" />
         <tli id="T268" time="138.0" type="appl" />
         <tli id="T269" time="138.5" type="appl" />
         <tli id="T270" time="139.0" type="appl" />
         <tli id="T271" time="139.5" type="appl" />
         <tli id="T272" time="140.0" type="appl" />
         <tli id="T273" time="140.5" type="appl" />
         <tli id="T274" time="141.0" type="appl" />
         <tli id="T275" time="141.5" type="appl" />
         <tli id="T276" time="142.0" type="appl" />
         <tli id="T277" time="142.5" type="appl" />
         <tli id="T278" time="143.0" type="appl" />
         <tli id="T279" time="143.5" type="appl" />
         <tli id="T280" time="144.0" type="appl" />
         <tli id="T354" time="144.051" type="intp" />
         <tli id="T281" time="144.5" type="appl" />
         <tli id="T282" time="145.0" type="appl" />
         <tli id="T283" time="145.5" type="appl" />
         <tli id="T284" time="146.0" type="appl" />
         <tli id="T285" time="146.5" type="appl" />
         <tli id="T286" time="147.0" type="appl" />
         <tli id="T287" time="147.5" type="appl" />
         <tli id="T288" time="148.0" type="appl" />
         <tli id="T289" time="148.5" type="appl" />
         <tli id="T290" time="149.0" type="appl" />
         <tli id="T291" time="149.5" type="appl" />
         <tli id="T292" time="150.0" type="appl" />
         <tli id="T293" time="150.5" type="appl" />
         <tli id="T294" time="151.0" type="appl" />
         <tli id="T295" time="151.5" type="appl" />
         <tli id="T296" time="152.0" type="appl" />
         <tli id="T297" time="152.5" type="appl" />
         <tli id="T298" time="153.0" type="appl" />
         <tli id="T299" time="153.5" type="appl" />
         <tli id="T300" time="154.0" type="appl" />
         <tli id="T301" time="154.5" type="appl" />
         <tli id="T302" time="155.0" type="appl" />
         <tli id="T303" time="155.5" type="appl" />
         <tli id="T304" time="156.0" type="appl" />
         <tli id="T305" time="156.5" type="appl" />
         <tli id="T306" time="157.0" type="appl" />
         <tli id="T307" time="157.5" type="appl" />
         <tli id="T355" time="157.949" type="intp" />
         <tli id="T308" time="158.0" type="appl" />
         <tli id="T309" time="158.5" type="appl" />
         <tli id="T310" time="159.0" type="appl" />
         <tli id="T311" time="159.5" type="appl" />
         <tli id="T312" time="160.0" type="appl" />
         <tli id="T313" time="160.5" type="appl" />
         <tli id="T314" time="161.0" type="appl" />
         <tli id="T315" time="161.5" type="appl" />
         <tli id="T316" time="162.0" type="appl" />
         <tli id="T317" time="162.5" type="appl" />
         <tli id="T318" time="163.0" type="appl" />
         <tli id="T319" time="163.5" type="appl" />
         <tli id="T320" time="164.0" type="appl" />
         <tli id="T321" time="164.5" type="appl" />
         <tli id="T322" time="165.0" type="appl" />
         <tli id="T323" time="165.5" type="appl" />
         <tli id="T324" time="166.0" type="appl" />
         <tli id="T325" time="166.5" type="appl" />
         <tli id="T326" time="167.0" type="appl" />
         <tli id="T327" time="167.5" type="appl" />
         <tli id="T328" time="168.0" type="appl" />
         <tli id="T329" time="168.5" type="appl" />
         <tli id="T330" time="169.0" type="appl" />
         <tli id="T331" time="169.5" type="appl" />
         <tli id="T332" time="170.0" type="appl" />
         <tli id="T333" time="170.5" type="appl" />
         <tli id="T334" time="171.0" type="appl" />
         <tli id="T335" time="171.5" type="appl" />
         <tli id="T336" time="172.0" type="appl" />
         <tli id="T337" time="172.5" type="appl" />
         <tli id="T338" time="173.0" type="appl" />
         <tli id="T339" time="173.5" type="appl" />
         <tli id="T340" time="174.0" type="appl" />
         <tli id="T341" time="174.5" type="appl" />
         <tli id="T342" time="175.0" type="appl" />
         <tli id="T343" time="175.5" type="appl" />
         <tli id="T344" time="176.0" type="appl" />
         <tli id="T345" time="176.5" type="appl" />
         <tli id="T346" time="177.0" type="appl" />
         <tli id="T347" time="177.5" type="appl" />
         <tli id="T348" time="178.0" type="appl" />
         <tli id="T349" time="178.5" type="appl" />
         <tli id="T350" time="179.0" type="appl" />
         <tli id="T351" time="179.5" type="appl" />
         <tli id="T352" time="180.0" type="appl" />
         <tli id="T353" time="180.5" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PoS"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T353" id="Seg_0" n="sc" s="T0">
               <ts e="T3" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Kaːmɨːlaːk</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">kaːman</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">ispit</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T7" id="Seg_14" n="HIAT:u" s="T3">
                  <ts e="T4" id="Seg_16" n="HIAT:w" s="T3">Kaːman</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">ihen-ihen</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">huːrka</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">kelbit</ts>
                  <nts id="Seg_26" n="HIAT:ip">.</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T11" id="Seg_29" n="HIAT:u" s="T7">
                  <ts e="T8" id="Seg_31" n="HIAT:w" s="T7">Buːska</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_34" n="HIAT:w" s="T8">toŋmut</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_37" n="HIAT:w" s="T9">kaːnɨ</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_40" n="HIAT:w" s="T10">körbüt</ts>
                  <nts id="Seg_41" n="HIAT:ip">.</nts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T18" id="Seg_44" n="HIAT:u" s="T11">
                  <nts id="Seg_45" n="HIAT:ip">"</nts>
                  <ts e="T12" id="Seg_47" n="HIAT:w" s="T11">Taba</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_50" n="HIAT:w" s="T12">kolun</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_53" n="HIAT:w" s="T13">bullum</ts>
                  <nts id="Seg_54" n="HIAT:ip">"</nts>
                  <nts id="Seg_55" n="HIAT:ip">,</nts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_58" n="HIAT:w" s="T14">diː-diː</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_61" n="HIAT:w" s="T15">huːmkatɨgar</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_64" n="HIAT:w" s="T16">uktan</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_67" n="HIAT:w" s="T17">keːspit</ts>
                  <nts id="Seg_68" n="HIAT:ip">.</nts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T23" id="Seg_71" n="HIAT:u" s="T18">
                  <ts e="T19" id="Seg_73" n="HIAT:w" s="T18">Innʼe</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_76" n="HIAT:w" s="T19">gɨnan</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_79" n="HIAT:w" s="T20">baraːn</ts>
                  <nts id="Seg_80" n="HIAT:ip">,</nts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_83" n="HIAT:w" s="T21">kaːman</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_86" n="HIAT:w" s="T22">ispit</ts>
                  <nts id="Seg_87" n="HIAT:ip">.</nts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_90" n="HIAT:u" s="T23">
                  <ts e="T24" id="Seg_92" n="HIAT:w" s="T23">Ihen-ihen</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_95" n="HIAT:w" s="T24">ɨ͡allarga</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_98" n="HIAT:w" s="T25">kelbit</ts>
                  <nts id="Seg_99" n="HIAT:ip">.</nts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T30" id="Seg_102" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_104" n="HIAT:w" s="T26">ɨ͡allarga</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_107" n="HIAT:w" s="T27">kü͡östeːri</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_110" n="HIAT:w" s="T28">etterin</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_113" n="HIAT:w" s="T29">etteːbitter</ts>
                  <nts id="Seg_114" n="HIAT:ip">.</nts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T36" id="Seg_117" n="HIAT:u" s="T30">
                  <nts id="Seg_118" n="HIAT:ip">"</nts>
                  <ts e="T31" id="Seg_120" n="HIAT:w" s="T30">Kaja</ts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_123" n="HIAT:w" s="T31">ɨ͡aldʼɨppɨtɨgar</ts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_126" n="HIAT:w" s="T32">et</ts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_129" n="HIAT:w" s="T33">tijbet</ts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_132" n="HIAT:w" s="T34">ebit</ts>
                  <nts id="Seg_133" n="HIAT:ip">"</nts>
                  <nts id="Seg_134" n="HIAT:ip">,</nts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_137" n="HIAT:w" s="T35">despitter</ts>
                  <nts id="Seg_138" n="HIAT:ip">.</nts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T39" id="Seg_141" n="HIAT:u" s="T36">
                  <ts e="T37" id="Seg_143" n="HIAT:w" s="T36">Onuga</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_146" n="HIAT:w" s="T37">Kaːmɨːlaːk</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_149" n="HIAT:w" s="T38">di͡ebit</ts>
                  <nts id="Seg_150" n="HIAT:ip">:</nts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T50" id="Seg_153" n="HIAT:u" s="T39">
                  <nts id="Seg_154" n="HIAT:ip">"</nts>
                  <ts e="T40" id="Seg_156" n="HIAT:w" s="T39">Kaja</ts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_159" n="HIAT:w" s="T40">tu͡ok</ts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_162" n="HIAT:w" s="T41">bu͡olagɨt</ts>
                  <nts id="Seg_163" n="HIAT:ip">,</nts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_166" n="HIAT:w" s="T42">min</ts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_169" n="HIAT:w" s="T43">huːmkabar</ts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_172" n="HIAT:w" s="T44">et</ts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_175" n="HIAT:w" s="T45">baːr</ts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_178" n="HIAT:w" s="T46">ete</ts>
                  <nts id="Seg_179" n="HIAT:ip">,</nts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_182" n="HIAT:w" s="T47">onu</ts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_185" n="HIAT:w" s="T48">killeren</ts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_188" n="HIAT:w" s="T49">kü͡östeːŋ</ts>
                  <nts id="Seg_189" n="HIAT:ip">!</nts>
                  <nts id="Seg_190" n="HIAT:ip">"</nts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T58" id="Seg_193" n="HIAT:u" s="T50">
                  <ts e="T51" id="Seg_195" n="HIAT:w" s="T50">Dʼe</ts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_198" n="HIAT:w" s="T51">onton</ts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_201" n="HIAT:w" s="T52">ɨ͡allar</ts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_204" n="HIAT:w" s="T53">kü͡österiger</ts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_207" n="HIAT:w" s="T54">Kaːmɨːlaːk</ts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_209" n="HIAT:ip">"</nts>
                  <ts e="T56" id="Seg_211" n="HIAT:w" s="T55">etin</ts>
                  <nts id="Seg_212" n="HIAT:ip">"</nts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_215" n="HIAT:w" s="T56">ugan</ts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_218" n="HIAT:w" s="T57">keːspitter</ts>
                  <nts id="Seg_219" n="HIAT:ip">.</nts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T62" id="Seg_222" n="HIAT:u" s="T58">
                  <ts e="T59" id="Seg_224" n="HIAT:w" s="T58">Ki͡ehennen</ts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_227" n="HIAT:w" s="T59">dʼe</ts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_230" n="HIAT:w" s="T60">etterin</ts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_233" n="HIAT:w" s="T61">kotorollor</ts>
                  <nts id="Seg_234" n="HIAT:ip">.</nts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T67" id="Seg_237" n="HIAT:u" s="T62">
                  <ts e="T63" id="Seg_239" n="HIAT:w" s="T62">Kotorbuttarɨgar</ts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_242" n="HIAT:w" s="T63">ɨ͡aldʼɨttarɨgar</ts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_245" n="HIAT:w" s="T64">ettere</ts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_248" n="HIAT:w" s="T65">emi͡e</ts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_251" n="HIAT:w" s="T66">tijbetek</ts>
                  <nts id="Seg_252" n="HIAT:ip">.</nts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T73" id="Seg_255" n="HIAT:u" s="T67">
                  <ts e="T68" id="Seg_257" n="HIAT:w" s="T67">Kaːmɨːlaːk</ts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_259" n="HIAT:ip">"</nts>
                  <ts e="T69" id="Seg_261" n="HIAT:w" s="T68">kaja</ts>
                  <nts id="Seg_262" n="HIAT:ip">,</nts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_265" n="HIAT:w" s="T69">kannanɨj</ts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_268" n="HIAT:w" s="T70">min</ts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_271" n="HIAT:w" s="T71">etim</ts>
                  <nts id="Seg_272" n="HIAT:ip">"</nts>
                  <nts id="Seg_273" n="HIAT:ip">,</nts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_276" n="HIAT:w" s="T72">di͡ebit</ts>
                  <nts id="Seg_277" n="HIAT:ip">.</nts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T74" id="Seg_280" n="HIAT:u" s="T73">
                  <nts id="Seg_281" n="HIAT:ip">"</nts>
                  <ts e="T74" id="Seg_283" n="HIAT:w" s="T73">Öllüm-öllüm</ts>
                  <nts id="Seg_284" n="HIAT:ip">.</nts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T77" id="Seg_287" n="HIAT:u" s="T74">
                  <ts e="T75" id="Seg_289" n="HIAT:w" s="T74">Kajdak</ts>
                  <nts id="Seg_290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_292" n="HIAT:w" s="T75">ahaːbakka</ts>
                  <nts id="Seg_293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_295" n="HIAT:w" s="T76">utuju͡omuj</ts>
                  <nts id="Seg_296" n="HIAT:ip">?</nts>
                  <nts id="Seg_297" n="HIAT:ip">"</nts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T80" id="Seg_300" n="HIAT:u" s="T77">
                  <nts id="Seg_301" n="HIAT:ip">"</nts>
                  <ts e="T78" id="Seg_303" n="HIAT:w" s="T77">Kajaː</ts>
                  <nts id="Seg_304" n="HIAT:ip">,</nts>
                  <nts id="Seg_305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_307" n="HIAT:w" s="T78">ölüme</ts>
                  <nts id="Seg_308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_310" n="HIAT:w" s="T79">agaj</ts>
                  <nts id="Seg_311" n="HIAT:ip">.</nts>
                  <nts id="Seg_312" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T87" id="Seg_314" n="HIAT:u" s="T80">
                  <ts e="T81" id="Seg_316" n="HIAT:w" s="T80">Bihigi</ts>
                  <nts id="Seg_317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_319" n="HIAT:w" s="T81">eji͡eke</ts>
                  <nts id="Seg_320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_322" n="HIAT:w" s="T82">bütün</ts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_325" n="HIAT:w" s="T83">tugutta</ts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_328" n="HIAT:w" s="T84">bi͡erebit</ts>
                  <nts id="Seg_329" n="HIAT:ip">"</nts>
                  <nts id="Seg_330" n="HIAT:ip">,</nts>
                  <nts id="Seg_331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_333" n="HIAT:w" s="T85">di͡ebitter</ts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_336" n="HIAT:w" s="T86">ɨ͡allar</ts>
                  <nts id="Seg_337" n="HIAT:ip">.</nts>
                  <nts id="Seg_338" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T93" id="Seg_340" n="HIAT:u" s="T87">
                  <ts e="T88" id="Seg_342" n="HIAT:w" s="T87">Ahaːn</ts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_345" n="HIAT:w" s="T88">baran</ts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_348" n="HIAT:w" s="T89">utujbuttar</ts>
                  <nts id="Seg_349" n="HIAT:ip">,</nts>
                  <nts id="Seg_350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_352" n="HIAT:w" s="T90">tuguttarɨn</ts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_355" n="HIAT:w" s="T91">köldörbütter</ts>
                  <nts id="Seg_356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_358" n="HIAT:w" s="T92">Kaːmɨːlaːkka</ts>
                  <nts id="Seg_359" n="HIAT:ip">.</nts>
                  <nts id="Seg_360" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T110" id="Seg_362" n="HIAT:u" s="T93">
                  <ts e="T94" id="Seg_364" n="HIAT:w" s="T93">Kaːmɨːlaːk</ts>
                  <nts id="Seg_365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_367" n="HIAT:w" s="T94">tüːn</ts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_370" n="HIAT:w" s="T95">taksan</ts>
                  <nts id="Seg_371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_373" n="HIAT:w" s="T96">tuguta</ts>
                  <nts id="Seg_374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_376" n="HIAT:w" s="T97">iːkke</ts>
                  <nts id="Seg_377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_379" n="HIAT:w" s="T98">kelbitin</ts>
                  <nts id="Seg_380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_382" n="HIAT:w" s="T99">tutan</ts>
                  <nts id="Seg_383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_385" n="HIAT:w" s="T100">ɨlbɨt</ts>
                  <nts id="Seg_386" n="HIAT:ip">,</nts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_389" n="HIAT:w" s="T101">innʼe</ts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_392" n="HIAT:w" s="T102">gɨnan</ts>
                  <nts id="Seg_393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_395" n="HIAT:w" s="T103">baraːn</ts>
                  <nts id="Seg_396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_398" n="HIAT:w" s="T104">ikki</ts>
                  <nts id="Seg_399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_401" n="HIAT:w" s="T105">bastɨŋ</ts>
                  <nts id="Seg_402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_404" n="HIAT:w" s="T106">atɨːr</ts>
                  <nts id="Seg_405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_407" n="HIAT:w" s="T107">mu͡ostarɨgar</ts>
                  <nts id="Seg_408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_410" n="HIAT:w" s="T108">iːlen</ts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_413" n="HIAT:w" s="T109">keːspit</ts>
                  <nts id="Seg_414" n="HIAT:ip">.</nts>
                  <nts id="Seg_415" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T113" id="Seg_417" n="HIAT:u" s="T110">
                  <ts e="T111" id="Seg_419" n="HIAT:w" s="T110">Kiːren</ts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_422" n="HIAT:w" s="T111">utujan</ts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_425" n="HIAT:w" s="T112">kaːlbɨt</ts>
                  <nts id="Seg_426" n="HIAT:ip">.</nts>
                  <nts id="Seg_427" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T124" id="Seg_429" n="HIAT:u" s="T113">
                  <ts e="T114" id="Seg_431" n="HIAT:w" s="T113">Harsi͡erda</ts>
                  <nts id="Seg_432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_434" n="HIAT:w" s="T114">ɨ͡allar</ts>
                  <nts id="Seg_435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_437" n="HIAT:w" s="T115">körbüttere</ts>
                  <nts id="Seg_438" n="HIAT:ip">,</nts>
                  <nts id="Seg_439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_441" n="HIAT:w" s="T116">Kaːmɨːlaːk</ts>
                  <nts id="Seg_442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_444" n="HIAT:w" s="T117">tugutun</ts>
                  <nts id="Seg_445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_447" n="HIAT:w" s="T118">ikki</ts>
                  <nts id="Seg_448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_450" n="HIAT:w" s="T119">bastɨŋ</ts>
                  <nts id="Seg_451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_453" n="HIAT:w" s="T120">atɨːrdara</ts>
                  <nts id="Seg_454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_456" n="HIAT:w" s="T121">mu͡ostarɨgar</ts>
                  <nts id="Seg_457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_459" n="HIAT:w" s="T122">iːle</ts>
                  <nts id="Seg_460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_462" n="HIAT:w" s="T123">hɨldʼallar</ts>
                  <nts id="Seg_463" n="HIAT:ip">.</nts>
                  <nts id="Seg_464" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T128" id="Seg_466" n="HIAT:u" s="T124">
                  <ts e="T125" id="Seg_468" n="HIAT:w" s="T124">Kaːmɨːlaːk</ts>
                  <nts id="Seg_469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_471" n="HIAT:w" s="T125">onu</ts>
                  <nts id="Seg_472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_474" n="HIAT:w" s="T126">ihitte</ts>
                  <nts id="Seg_475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_477" n="HIAT:w" s="T127">daːganɨ</ts>
                  <nts id="Seg_478" n="HIAT:ip">:</nts>
                  <nts id="Seg_479" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T132" id="Seg_481" n="HIAT:u" s="T128">
                  <nts id="Seg_482" n="HIAT:ip">"</nts>
                  <ts e="T129" id="Seg_484" n="HIAT:w" s="T128">Dʼe</ts>
                  <nts id="Seg_485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_487" n="HIAT:w" s="T129">ile</ts>
                  <nts id="Seg_488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_490" n="HIAT:w" s="T130">ölöbün</ts>
                  <nts id="Seg_491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_493" n="HIAT:w" s="T131">ebit</ts>
                  <nts id="Seg_494" n="HIAT:ip">!</nts>
                  <nts id="Seg_495" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T137" id="Seg_497" n="HIAT:u" s="T132">
                  <ts e="T133" id="Seg_499" n="HIAT:w" s="T132">Tuguta</ts>
                  <nts id="Seg_500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_502" n="HIAT:w" s="T133">hu͡ok</ts>
                  <nts id="Seg_503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_505" n="HIAT:w" s="T134">kaːllɨm</ts>
                  <nts id="Seg_506" n="HIAT:ip">"</nts>
                  <nts id="Seg_507" n="HIAT:ip">,</nts>
                  <nts id="Seg_508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_510" n="HIAT:w" s="T135">di͡en</ts>
                  <nts id="Seg_511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_513" n="HIAT:w" s="T136">üːgüleːbit</ts>
                  <nts id="Seg_514" n="HIAT:ip">.</nts>
                  <nts id="Seg_515" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T140" id="Seg_517" n="HIAT:u" s="T137">
                  <nts id="Seg_518" n="HIAT:ip">"</nts>
                  <ts e="T138" id="Seg_520" n="HIAT:w" s="T137">Kaja</ts>
                  <nts id="Seg_521" n="HIAT:ip">,</nts>
                  <nts id="Seg_522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_524" n="HIAT:w" s="T138">ölüme</ts>
                  <nts id="Seg_525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_527" n="HIAT:w" s="T139">agaj</ts>
                  <nts id="Seg_528" n="HIAT:ip">!</nts>
                  <nts id="Seg_529" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T148" id="Seg_531" n="HIAT:u" s="T140">
                  <ts e="T141" id="Seg_533" n="HIAT:w" s="T140">Bihigi</ts>
                  <nts id="Seg_534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_536" n="HIAT:w" s="T141">eji͡eke</ts>
                  <nts id="Seg_537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_539" n="HIAT:w" s="T142">ol</ts>
                  <nts id="Seg_540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_542" n="HIAT:w" s="T143">ikki</ts>
                  <nts id="Seg_543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_545" n="HIAT:w" s="T144">atɨːrɨ</ts>
                  <nts id="Seg_546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_548" n="HIAT:w" s="T145">bi͡eri͡ekpit</ts>
                  <nts id="Seg_549" n="HIAT:ip">"</nts>
                  <nts id="Seg_550" n="HIAT:ip">,</nts>
                  <nts id="Seg_551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_553" n="HIAT:w" s="T146">di͡ebitter</ts>
                  <nts id="Seg_554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_556" n="HIAT:w" s="T147">dʼi͡eteːgilere</ts>
                  <nts id="Seg_557" n="HIAT:ip">.</nts>
                  <nts id="Seg_558" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T151" id="Seg_560" n="HIAT:u" s="T148">
                  <ts e="T149" id="Seg_562" n="HIAT:w" s="T148">Kaːmɨːlaːk</ts>
                  <nts id="Seg_563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_565" n="HIAT:w" s="T149">ikki</ts>
                  <nts id="Seg_566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_568" n="HIAT:w" s="T150">atɨːrdammɨt</ts>
                  <nts id="Seg_569" n="HIAT:ip">.</nts>
                  <nts id="Seg_570" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T158" id="Seg_572" n="HIAT:u" s="T151">
                  <ts e="T152" id="Seg_574" n="HIAT:w" s="T151">Ikki</ts>
                  <nts id="Seg_575" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_577" n="HIAT:w" s="T152">atɨːrɨn</ts>
                  <nts id="Seg_578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_580" n="HIAT:w" s="T153">kölünen</ts>
                  <nts id="Seg_581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_583" n="HIAT:w" s="T154">baran</ts>
                  <nts id="Seg_584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_586" n="HIAT:w" s="T155">Kaːmɨːlaːk</ts>
                  <nts id="Seg_587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_589" n="HIAT:w" s="T156">hɨrgalana</ts>
                  <nts id="Seg_590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_592" n="HIAT:w" s="T157">barbɨt</ts>
                  <nts id="Seg_593" n="HIAT:ip">.</nts>
                  <nts id="Seg_594" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T161" id="Seg_596" n="HIAT:u" s="T158">
                  <ts e="T159" id="Seg_598" n="HIAT:w" s="T158">Ihen-ihen</ts>
                  <nts id="Seg_599" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_601" n="HIAT:w" s="T159">oŋu͡oktarga</ts>
                  <nts id="Seg_602" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_604" n="HIAT:w" s="T160">keppit</ts>
                  <nts id="Seg_605" n="HIAT:ip">.</nts>
                  <nts id="Seg_606" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T171" id="Seg_608" n="HIAT:u" s="T161">
                  <ts e="T162" id="Seg_610" n="HIAT:w" s="T161">Onno</ts>
                  <nts id="Seg_611" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_613" n="HIAT:w" s="T162">biːr</ts>
                  <nts id="Seg_614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_616" n="HIAT:w" s="T163">emeːksini</ts>
                  <nts id="Seg_617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_619" n="HIAT:w" s="T164">kolbotun</ts>
                  <nts id="Seg_620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_622" n="HIAT:w" s="T165">arɨjan</ts>
                  <nts id="Seg_623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_625" n="HIAT:w" s="T166">kostoːn</ts>
                  <nts id="Seg_626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_628" n="HIAT:w" s="T167">ɨlbɨt</ts>
                  <nts id="Seg_629" n="HIAT:ip">,</nts>
                  <nts id="Seg_630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_632" n="HIAT:w" s="T168">hɨrgatɨgar</ts>
                  <nts id="Seg_633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_635" n="HIAT:w" s="T169">uːran</ts>
                  <nts id="Seg_636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_638" n="HIAT:w" s="T170">keːspit</ts>
                  <nts id="Seg_639" n="HIAT:ip">.</nts>
                  <nts id="Seg_640" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T173" id="Seg_642" n="HIAT:u" s="T171">
                  <ts e="T172" id="Seg_644" n="HIAT:w" s="T171">Baran</ts>
                  <nts id="Seg_645" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_647" n="HIAT:w" s="T172">ispit</ts>
                  <nts id="Seg_648" n="HIAT:ip">.</nts>
                  <nts id="Seg_649" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T177" id="Seg_651" n="HIAT:u" s="T173">
                  <ts e="T174" id="Seg_653" n="HIAT:w" s="T173">Ihen-ihen</ts>
                  <nts id="Seg_654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_656" n="HIAT:w" s="T174">biːr</ts>
                  <nts id="Seg_657" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_659" n="HIAT:w" s="T175">ɨ͡alga</ts>
                  <nts id="Seg_660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_662" n="HIAT:w" s="T176">tijbit</ts>
                  <nts id="Seg_663" n="HIAT:ip">.</nts>
                  <nts id="Seg_664" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T181" id="Seg_666" n="HIAT:u" s="T177">
                  <ts e="T178" id="Seg_668" n="HIAT:w" s="T177">Hɨrgatɨn</ts>
                  <nts id="Seg_669" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_671" n="HIAT:w" s="T178">ɨ͡allartan</ts>
                  <nts id="Seg_672" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_674" n="HIAT:w" s="T179">ɨraːkkaːn</ts>
                  <nts id="Seg_675" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_677" n="HIAT:w" s="T180">keːspit</ts>
                  <nts id="Seg_678" n="HIAT:ip">.</nts>
                  <nts id="Seg_679" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T185" id="Seg_681" n="HIAT:u" s="T181">
                  <ts e="T182" id="Seg_683" n="HIAT:w" s="T181">Kaːmɨːlaːk</ts>
                  <nts id="Seg_684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_686" n="HIAT:w" s="T182">tahaːraː</ts>
                  <nts id="Seg_687" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_689" n="HIAT:w" s="T183">ogoloru</ts>
                  <nts id="Seg_690" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_692" n="HIAT:w" s="T184">körsübut</ts>
                  <nts id="Seg_693" n="HIAT:ip">:</nts>
                  <nts id="Seg_694" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T189" id="Seg_696" n="HIAT:u" s="T185">
                  <nts id="Seg_697" n="HIAT:ip">"</nts>
                  <ts e="T186" id="Seg_699" n="HIAT:w" s="T185">Ogoloːr</ts>
                  <nts id="Seg_700" n="HIAT:ip">,</nts>
                  <nts id="Seg_701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_703" n="HIAT:w" s="T186">min</ts>
                  <nts id="Seg_704" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_706" n="HIAT:w" s="T187">hɨrgabar</ts>
                  <nts id="Seg_707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_709" n="HIAT:w" s="T188">čugahaːjagɨt</ts>
                  <nts id="Seg_710" n="HIAT:ip">!</nts>
                  <nts id="Seg_711" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T192" id="Seg_713" n="HIAT:u" s="T189">
                  <ts e="T190" id="Seg_715" n="HIAT:w" s="T189">Hɨrgabar</ts>
                  <nts id="Seg_716" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_718" n="HIAT:w" s="T190">emeːksinim</ts>
                  <nts id="Seg_719" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_721" n="HIAT:w" s="T191">baːr</ts>
                  <nts id="Seg_722" n="HIAT:ip">.</nts>
                  <nts id="Seg_723" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T199" id="Seg_725" n="HIAT:u" s="T192">
                  <ts e="T193" id="Seg_727" n="HIAT:w" s="T192">Kihi</ts>
                  <nts id="Seg_728" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_730" n="HIAT:w" s="T193">čugahaːta</ts>
                  <nts id="Seg_731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_733" n="HIAT:w" s="T194">da</ts>
                  <nts id="Seg_734" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_736" n="HIAT:w" s="T195">hohujan</ts>
                  <nts id="Seg_737" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_739" n="HIAT:w" s="T196">ölön</ts>
                  <nts id="Seg_740" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_742" n="HIAT:w" s="T197">kaːlɨ͡aga</ts>
                  <nts id="Seg_743" n="HIAT:ip">"</nts>
                  <nts id="Seg_744" n="HIAT:ip">,</nts>
                  <nts id="Seg_745" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_747" n="HIAT:w" s="T198">di͡ebit</ts>
                  <nts id="Seg_748" n="HIAT:ip">.</nts>
                  <nts id="Seg_749" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T206" id="Seg_751" n="HIAT:u" s="T199">
                  <ts e="T200" id="Seg_753" n="HIAT:w" s="T199">Kaːmɨːlaːk</ts>
                  <nts id="Seg_754" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_756" n="HIAT:w" s="T200">čaːj</ts>
                  <nts id="Seg_757" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_759" n="HIAT:w" s="T201">ihe</ts>
                  <nts id="Seg_760" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_762" n="HIAT:w" s="T202">olordoguna</ts>
                  <nts id="Seg_763" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_765" n="HIAT:w" s="T203">ogolor</ts>
                  <nts id="Seg_766" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_768" n="HIAT:w" s="T204">hüːren</ts>
                  <nts id="Seg_769" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_771" n="HIAT:w" s="T205">kiːrbitter</ts>
                  <nts id="Seg_772" n="HIAT:ip">:</nts>
                  <nts id="Seg_773" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T211" id="Seg_775" n="HIAT:u" s="T206">
                  <ts e="T207" id="Seg_777" n="HIAT:w" s="T206">Kaːmɨːlaːk</ts>
                  <nts id="Seg_778" n="HIAT:ip">,</nts>
                  <nts id="Seg_779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_781" n="HIAT:w" s="T207">emeːksiniŋ</ts>
                  <nts id="Seg_782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_784" n="HIAT:w" s="T208">ölön</ts>
                  <nts id="Seg_785" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_787" n="HIAT:w" s="T209">kaːlbɨt</ts>
                  <nts id="Seg_788" n="HIAT:ip">"</nts>
                  <nts id="Seg_789" n="HIAT:ip">,</nts>
                  <nts id="Seg_790" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_792" n="HIAT:w" s="T210">di͡ebitter</ts>
                  <nts id="Seg_793" n="HIAT:ip">.</nts>
                  <nts id="Seg_794" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T212" id="Seg_796" n="HIAT:u" s="T211">
                  <nts id="Seg_797" n="HIAT:ip">"</nts>
                  <ts e="T212" id="Seg_799" n="HIAT:w" s="T211">Araː</ts>
                  <nts id="Seg_800" n="HIAT:ip">!</nts>
                  <nts id="Seg_801" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T217" id="Seg_803" n="HIAT:u" s="T212">
                  <ts e="T213" id="Seg_805" n="HIAT:w" s="T212">Anɨ</ts>
                  <nts id="Seg_806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_808" n="HIAT:w" s="T213">bagas</ts>
                  <nts id="Seg_809" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_811" n="HIAT:w" s="T214">ile</ts>
                  <nts id="Seg_812" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_814" n="HIAT:w" s="T215">ölöbün</ts>
                  <nts id="Seg_815" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_817" n="HIAT:w" s="T216">ebit</ts>
                  <nts id="Seg_818" n="HIAT:ip">!</nts>
                  <nts id="Seg_819" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T220" id="Seg_821" n="HIAT:u" s="T217">
                  <ts e="T218" id="Seg_823" n="HIAT:w" s="T217">Emeːksine</ts>
                  <nts id="Seg_824" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_826" n="HIAT:w" s="T218">hu͡ok</ts>
                  <nts id="Seg_827" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_829" n="HIAT:w" s="T219">kaːllɨm</ts>
                  <nts id="Seg_830" n="HIAT:ip">!</nts>
                  <nts id="Seg_831" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T226" id="Seg_833" n="HIAT:u" s="T220">
                  <ts e="T221" id="Seg_835" n="HIAT:w" s="T220">Iti</ts>
                  <nts id="Seg_836" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_838" n="HIAT:w" s="T221">ogolor</ts>
                  <nts id="Seg_839" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_841" n="HIAT:w" s="T222">hɨrgaga</ts>
                  <nts id="Seg_842" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_844" n="HIAT:w" s="T223">čugahaːnnar</ts>
                  <nts id="Seg_845" n="HIAT:ip">"</nts>
                  <nts id="Seg_846" n="HIAT:ip">,</nts>
                  <nts id="Seg_847" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_849" n="HIAT:w" s="T224">Kaːmɨːlaːk</ts>
                  <nts id="Seg_850" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_852" n="HIAT:w" s="T225">üːgüleːbit</ts>
                  <nts id="Seg_853" n="HIAT:ip">.</nts>
                  <nts id="Seg_854" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T230" id="Seg_856" n="HIAT:u" s="T226">
                  <nts id="Seg_857" n="HIAT:ip">"</nts>
                  <ts e="T227" id="Seg_859" n="HIAT:w" s="T226">Togo</ts>
                  <nts id="Seg_860" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_862" n="HIAT:w" s="T227">ölöːrü</ts>
                  <nts id="Seg_863" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_865" n="HIAT:w" s="T228">gɨnagɨn</ts>
                  <nts id="Seg_866" n="HIAT:ip">,</nts>
                  <nts id="Seg_867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_869" n="HIAT:w" s="T229">kaja</ts>
                  <nts id="Seg_870" n="HIAT:ip">!</nts>
                  <nts id="Seg_871" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T236" id="Seg_873" n="HIAT:u" s="T230">
                  <ts e="T231" id="Seg_875" n="HIAT:w" s="T230">ɨl</ts>
                  <nts id="Seg_876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_878" n="HIAT:w" s="T231">iti</ts>
                  <nts id="Seg_879" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_881" n="HIAT:w" s="T232">ikki</ts>
                  <nts id="Seg_882" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_884" n="HIAT:w" s="T233">kɨːspɨtɨn</ts>
                  <nts id="Seg_885" n="HIAT:ip">"</nts>
                  <nts id="Seg_886" n="HIAT:ip">,</nts>
                  <nts id="Seg_887" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_889" n="HIAT:w" s="T234">despitter</ts>
                  <nts id="Seg_890" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_892" n="HIAT:w" s="T235">dʼi͡eleːkter</ts>
                  <nts id="Seg_893" n="HIAT:ip">.</nts>
                  <nts id="Seg_894" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T240" id="Seg_896" n="HIAT:u" s="T236">
                  <ts e="T237" id="Seg_898" n="HIAT:w" s="T236">Kaːmɨːlaːk</ts>
                  <nts id="Seg_899" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_901" n="HIAT:w" s="T237">onon</ts>
                  <nts id="Seg_902" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_904" n="HIAT:w" s="T238">ikki</ts>
                  <nts id="Seg_905" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_907" n="HIAT:w" s="T239">kɨːstammɨt</ts>
                  <nts id="Seg_908" n="HIAT:ip">.</nts>
                  <nts id="Seg_909" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T247" id="Seg_911" n="HIAT:u" s="T240">
                  <ts e="T241" id="Seg_913" n="HIAT:w" s="T240">Kɨrgɨttarɨn</ts>
                  <nts id="Seg_914" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_916" n="HIAT:w" s="T241">ti͡ejen</ts>
                  <nts id="Seg_917" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_919" n="HIAT:w" s="T242">baran</ts>
                  <nts id="Seg_920" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_922" n="HIAT:w" s="T243">Kaːmɨːlaːk</ts>
                  <nts id="Seg_923" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_925" n="HIAT:w" s="T244">baran</ts>
                  <nts id="Seg_926" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_928" n="HIAT:w" s="T245">ispit</ts>
                  <nts id="Seg_929" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_931" n="HIAT:w" s="T246">dʼe</ts>
                  <nts id="Seg_932" n="HIAT:ip">.</nts>
                  <nts id="Seg_933" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T253" id="Seg_935" n="HIAT:u" s="T247">
                  <ts e="T248" id="Seg_937" n="HIAT:w" s="T247">Baran</ts>
                  <nts id="Seg_938" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_940" n="HIAT:w" s="T248">ihen-ihen</ts>
                  <nts id="Seg_941" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_943" n="HIAT:w" s="T249">biːr</ts>
                  <nts id="Seg_944" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_946" n="HIAT:w" s="T250">baːj</ts>
                  <nts id="Seg_947" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_949" n="HIAT:w" s="T251">kirili͡ehin</ts>
                  <nts id="Seg_950" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_952" n="HIAT:w" s="T252">körbüt</ts>
                  <nts id="Seg_953" n="HIAT:ip">.</nts>
                  <nts id="Seg_954" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T258" id="Seg_956" n="HIAT:u" s="T253">
                  <ts e="T254" id="Seg_958" n="HIAT:w" s="T253">Onu</ts>
                  <nts id="Seg_959" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_961" n="HIAT:w" s="T254">körön</ts>
                  <nts id="Seg_962" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_964" n="HIAT:w" s="T255">Kaːmɨːlaːk</ts>
                  <nts id="Seg_965" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_967" n="HIAT:w" s="T256">kɨrgɨttarɨgar</ts>
                  <nts id="Seg_968" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_970" n="HIAT:w" s="T257">hanarbɨt</ts>
                  <nts id="Seg_971" n="HIAT:ip">:</nts>
                  <nts id="Seg_972" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T264" id="Seg_974" n="HIAT:u" s="T258">
                  <nts id="Seg_975" n="HIAT:ip">"</nts>
                  <ts e="T259" id="Seg_977" n="HIAT:w" s="T258">Kaja</ts>
                  <nts id="Seg_978" n="HIAT:ip">,</nts>
                  <nts id="Seg_979" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_981" n="HIAT:w" s="T259">kɨrgɨttar</ts>
                  <nts id="Seg_982" n="HIAT:ip">,</nts>
                  <nts id="Seg_983" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_985" n="HIAT:w" s="T260">barɨŋ</ts>
                  <nts id="Seg_986" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_988" n="HIAT:w" s="T261">taksɨŋ</ts>
                  <nts id="Seg_989" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_991" n="HIAT:w" s="T262">tiːt</ts>
                  <nts id="Seg_992" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_994" n="HIAT:w" s="T263">maska</ts>
                  <nts id="Seg_995" n="HIAT:ip">!</nts>
                  <nts id="Seg_996" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T266" id="Seg_998" n="HIAT:u" s="T264">
                  <ts e="T265" id="Seg_1000" n="HIAT:w" s="T264">Kistenen</ts>
                  <nts id="Seg_1001" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_1003" n="HIAT:w" s="T265">oloruŋ</ts>
                  <nts id="Seg_1004" n="HIAT:ip">.</nts>
                  <nts id="Seg_1005" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T269" id="Seg_1007" n="HIAT:u" s="T266">
                  <ts e="T267" id="Seg_1009" n="HIAT:w" s="T266">Min</ts>
                  <nts id="Seg_1010" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_1012" n="HIAT:w" s="T267">baːjga</ts>
                  <nts id="Seg_1013" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_1015" n="HIAT:w" s="T268">barɨ͡am</ts>
                  <nts id="Seg_1016" n="HIAT:ip">.</nts>
                  <nts id="Seg_1017" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T272" id="Seg_1019" n="HIAT:u" s="T269">
                  <ts e="T270" id="Seg_1021" n="HIAT:w" s="T269">Ginini</ts>
                  <nts id="Seg_1022" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_1024" n="HIAT:w" s="T270">munna</ts>
                  <nts id="Seg_1025" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_1027" n="HIAT:w" s="T271">egeli͡em</ts>
                  <nts id="Seg_1028" n="HIAT:ip">.</nts>
                  <nts id="Seg_1029" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T277" id="Seg_1031" n="HIAT:u" s="T272">
                  <ts e="T273" id="Seg_1033" n="HIAT:w" s="T272">Baːj</ts>
                  <nts id="Seg_1034" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1035" n="HIAT:ip">"</nts>
                  <ts e="T274" id="Seg_1037" n="HIAT:w" s="T273">kimi͡ene</ts>
                  <nts id="Seg_1038" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_1040" n="HIAT:w" s="T274">baːjaj</ts>
                  <nts id="Seg_1041" n="HIAT:ip">"</nts>
                  <nts id="Seg_1042" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_1044" n="HIAT:w" s="T275">diː-diː</ts>
                  <nts id="Seg_1045" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_1047" n="HIAT:w" s="T276">ɨjɨtɨ͡a</ts>
                  <nts id="Seg_1048" n="HIAT:ip">.</nts>
                  <nts id="Seg_1049" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T280" id="Seg_1051" n="HIAT:u" s="T277">
                  <ts e="T278" id="Seg_1053" n="HIAT:w" s="T277">Onuga</ts>
                  <nts id="Seg_1054" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_1056" n="HIAT:w" s="T278">ehigi</ts>
                  <nts id="Seg_1057" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_1059" n="HIAT:w" s="T279">di͡eriŋ</ts>
                  <nts id="Seg_1060" n="HIAT:ip">:</nts>
                  <nts id="Seg_1061" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T282" id="Seg_1063" n="HIAT:u" s="T280">
                  <nts id="Seg_1064" n="HIAT:ip">"</nts>
                  <ts e="T281" id="Seg_1066" n="HIAT:w" s="T280">Kaːmɨːlaːk</ts>
                  <nts id="Seg_1067" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_1069" n="HIAT:w" s="T281">baːja</ts>
                  <nts id="Seg_1070" n="HIAT:ip">!</nts>
                  <nts id="Seg_1071" n="HIAT:ip">"</nts>
                  <nts id="Seg_1072" n="HIAT:ip">"</nts>
                  <nts id="Seg_1073" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T288" id="Seg_1075" n="HIAT:u" s="T282">
                  <ts e="T283" id="Seg_1077" n="HIAT:w" s="T282">Onu</ts>
                  <nts id="Seg_1078" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_1080" n="HIAT:w" s="T283">haŋaran</ts>
                  <nts id="Seg_1081" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_1083" n="HIAT:w" s="T284">baran</ts>
                  <nts id="Seg_1084" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_1086" n="HIAT:w" s="T285">Kaːmɨːlaːk</ts>
                  <nts id="Seg_1087" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_1089" n="HIAT:w" s="T286">baːjga</ts>
                  <nts id="Seg_1090" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_1092" n="HIAT:w" s="T287">tijbit</ts>
                  <nts id="Seg_1093" n="HIAT:ip">.</nts>
                  <nts id="Seg_1094" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T292" id="Seg_1096" n="HIAT:u" s="T288">
                  <ts e="T289" id="Seg_1098" n="HIAT:w" s="T288">Čaːj</ts>
                  <nts id="Seg_1099" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_1101" n="HIAT:w" s="T289">ihen</ts>
                  <nts id="Seg_1102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_1104" n="HIAT:w" s="T290">büten</ts>
                  <nts id="Seg_1105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_1107" n="HIAT:w" s="T291">di͡ebit</ts>
                  <nts id="Seg_1108" n="HIAT:ip">:</nts>
                  <nts id="Seg_1109" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T297" id="Seg_1111" n="HIAT:u" s="T292">
                  <nts id="Seg_1112" n="HIAT:ip">"</nts>
                  <ts e="T293" id="Seg_1114" n="HIAT:w" s="T292">Kaja</ts>
                  <nts id="Seg_1115" n="HIAT:ip">,</nts>
                  <nts id="Seg_1116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_1118" n="HIAT:w" s="T293">dogoː</ts>
                  <nts id="Seg_1119" n="HIAT:ip">,</nts>
                  <nts id="Seg_1120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1122" n="HIAT:w" s="T294">kimmit</ts>
                  <nts id="Seg_1123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1125" n="HIAT:w" s="T295">baːjaj</ts>
                  <nts id="Seg_1126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_1128" n="HIAT:w" s="T296">bu</ts>
                  <nts id="Seg_1129" n="HIAT:ip">?</nts>
                  <nts id="Seg_1130" n="HIAT:ip">"</nts>
                  <nts id="Seg_1131" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T303" id="Seg_1133" n="HIAT:u" s="T297">
                  <ts e="T298" id="Seg_1135" n="HIAT:w" s="T297">Onno</ts>
                  <nts id="Seg_1136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_1138" n="HIAT:w" s="T298">baːj</ts>
                  <nts id="Seg_1139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_1141" n="HIAT:w" s="T299">hohuja</ts>
                  <nts id="Seg_1142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_1144" n="HIAT:w" s="T300">hɨhan</ts>
                  <nts id="Seg_1145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_1147" n="HIAT:w" s="T301">baran</ts>
                  <nts id="Seg_1148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1150" n="HIAT:w" s="T302">eppit</ts>
                  <nts id="Seg_1151" n="HIAT:ip">:</nts>
                  <nts id="Seg_1152" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T305" id="Seg_1154" n="HIAT:u" s="T303">
                  <nts id="Seg_1155" n="HIAT:ip">"</nts>
                  <ts e="T304" id="Seg_1157" n="HIAT:w" s="T303">Kimi͡ene</ts>
                  <nts id="Seg_1158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_1160" n="HIAT:w" s="T304">bu͡olu͡oj</ts>
                  <nts id="Seg_1161" n="HIAT:ip">?</nts>
                  <nts id="Seg_1162" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T307" id="Seg_1164" n="HIAT:u" s="T305">
                  <ts e="T306" id="Seg_1166" n="HIAT:w" s="T305">Mi͡ene</ts>
                  <nts id="Seg_1167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_1169" n="HIAT:w" s="T306">bu͡olumuja</ts>
                  <nts id="Seg_1170" n="HIAT:ip">!</nts>
                  <nts id="Seg_1171" n="HIAT:ip">"</nts>
                  <nts id="Seg_1172" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T308" id="Seg_1174" n="HIAT:u" s="T307">
                  <ts e="T308" id="Seg_1176" n="HIAT:w" s="T307">Kaːmɨːlaːk</ts>
                  <nts id="Seg_1177" n="HIAT:ip">:</nts>
                  <nts id="Seg_1178" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T309" id="Seg_1180" n="HIAT:u" s="T308">
                  <nts id="Seg_1181" n="HIAT:ip">"</nts>
                  <ts e="T309" id="Seg_1183" n="HIAT:w" s="T308">Hu͡ok</ts>
                  <nts id="Seg_1184" n="HIAT:ip">!</nts>
                  <nts id="Seg_1185" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T310" id="Seg_1187" n="HIAT:u" s="T309">
                  <ts e="T310" id="Seg_1189" n="HIAT:w" s="T309">Mi͡ene</ts>
                  <nts id="Seg_1190" n="HIAT:ip">!</nts>
                  <nts id="Seg_1191" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T316" id="Seg_1193" n="HIAT:u" s="T310">
                  <ts e="T311" id="Seg_1195" n="HIAT:w" s="T310">Itegejbet</ts>
                  <nts id="Seg_1196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_1198" n="HIAT:w" s="T311">bu͡ollakkɨna</ts>
                  <nts id="Seg_1199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_1201" n="HIAT:w" s="T312">barɨ͡ak</ts>
                  <nts id="Seg_1202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_1204" n="HIAT:w" s="T313">taŋarattan</ts>
                  <nts id="Seg_1205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_1207" n="HIAT:w" s="T314">ɨjɨta</ts>
                  <nts id="Seg_1208" n="HIAT:ip">"</nts>
                  <nts id="Seg_1209" n="HIAT:ip">,</nts>
                  <nts id="Seg_1210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1212" n="HIAT:w" s="T315">di͡ebit</ts>
                  <nts id="Seg_1213" n="HIAT:ip">.</nts>
                  <nts id="Seg_1214" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T317" id="Seg_1216" n="HIAT:u" s="T316">
                  <ts e="T317" id="Seg_1218" n="HIAT:w" s="T316">Barbɨttar</ts>
                  <nts id="Seg_1219" n="HIAT:ip">.</nts>
                  <nts id="Seg_1220" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T324" id="Seg_1222" n="HIAT:u" s="T317">
                  <ts e="T318" id="Seg_1224" n="HIAT:w" s="T317">Kɨrgɨttar</ts>
                  <nts id="Seg_1225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1227" n="HIAT:w" s="T318">hɨtar</ts>
                  <nts id="Seg_1228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_1230" n="HIAT:w" s="T319">hirderiger</ts>
                  <nts id="Seg_1231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1233" n="HIAT:w" s="T320">kelen</ts>
                  <nts id="Seg_1234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_1236" n="HIAT:w" s="T321">baran</ts>
                  <nts id="Seg_1237" n="HIAT:ip">,</nts>
                  <nts id="Seg_1238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1240" n="HIAT:w" s="T322">Kaːmɨːlaːk</ts>
                  <nts id="Seg_1241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_1243" n="HIAT:w" s="T323">üːgüleːbit</ts>
                  <nts id="Seg_1244" n="HIAT:ip">:</nts>
                  <nts id="Seg_1245" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T327" id="Seg_1247" n="HIAT:u" s="T324">
                  <ts e="T325" id="Seg_1249" n="HIAT:w" s="T324">Bu</ts>
                  <nts id="Seg_1250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_1252" n="HIAT:w" s="T325">kim</ts>
                  <nts id="Seg_1253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1255" n="HIAT:w" s="T326">baːjaj</ts>
                  <nts id="Seg_1256" n="HIAT:ip">?</nts>
                  <nts id="Seg_1257" n="HIAT:ip">"</nts>
                  <nts id="Seg_1258" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T330" id="Seg_1260" n="HIAT:u" s="T327">
                  <ts e="T328" id="Seg_1262" n="HIAT:w" s="T327">Üːhetten</ts>
                  <nts id="Seg_1263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_1265" n="HIAT:w" s="T328">kɨrgɨttar</ts>
                  <nts id="Seg_1266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_1268" n="HIAT:w" s="T329">üːgüleːbitter</ts>
                  <nts id="Seg_1269" n="HIAT:ip">:</nts>
                  <nts id="Seg_1270" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T334" id="Seg_1272" n="HIAT:u" s="T330">
                  <nts id="Seg_1273" n="HIAT:ip">"</nts>
                  <ts e="T331" id="Seg_1275" n="HIAT:w" s="T330">Kaːmɨːlaːk</ts>
                  <nts id="Seg_1276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_1278" n="HIAT:w" s="T331">baːja</ts>
                  <nts id="Seg_1279" n="HIAT:ip">,</nts>
                  <nts id="Seg_1280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_1282" n="HIAT:w" s="T332">Kaːmɨːlaːk</ts>
                  <nts id="Seg_1283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_1285" n="HIAT:w" s="T333">baːja</ts>
                  <nts id="Seg_1286" n="HIAT:ip">!</nts>
                  <nts id="Seg_1287" n="HIAT:ip">"</nts>
                  <nts id="Seg_1288" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T341" id="Seg_1290" n="HIAT:u" s="T334">
                  <nts id="Seg_1291" n="HIAT:ip">"</nts>
                  <ts e="T335" id="Seg_1293" n="HIAT:w" s="T334">Ihittiŋ</ts>
                  <nts id="Seg_1294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_1296" n="HIAT:w" s="T335">du͡o</ts>
                  <nts id="Seg_1297" n="HIAT:ip">"</nts>
                  <nts id="Seg_1298" n="HIAT:ip">,</nts>
                  <nts id="Seg_1299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_1301" n="HIAT:w" s="T336">di͡ebit</ts>
                  <nts id="Seg_1302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_1304" n="HIAT:w" s="T337">Kaːmɨːlaːk</ts>
                  <nts id="Seg_1305" n="HIAT:ip">,</nts>
                  <nts id="Seg_1306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1307" n="HIAT:ip">"</nts>
                  <ts e="T339" id="Seg_1309" n="HIAT:w" s="T338">kim</ts>
                  <nts id="Seg_1310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_1312" n="HIAT:w" s="T339">baːja</ts>
                  <nts id="Seg_1313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_1315" n="HIAT:w" s="T340">ebitij</ts>
                  <nts id="Seg_1316" n="HIAT:ip">?</nts>
                  <nts id="Seg_1317" n="HIAT:ip">"</nts>
                  <nts id="Seg_1318" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T349" id="Seg_1320" n="HIAT:u" s="T341">
                  <ts e="T342" id="Seg_1322" n="HIAT:w" s="T341">Kaːmɨːlaːk</ts>
                  <nts id="Seg_1323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1325" n="HIAT:w" s="T342">iti</ts>
                  <nts id="Seg_1326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_1328" n="HIAT:w" s="T343">baːjɨ</ts>
                  <nts id="Seg_1329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_1331" n="HIAT:w" s="T344">batan</ts>
                  <nts id="Seg_1332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_1334" n="HIAT:w" s="T345">ɨːppɨt</ts>
                  <nts id="Seg_1335" n="HIAT:ip">,</nts>
                  <nts id="Seg_1336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_1338" n="HIAT:w" s="T346">baːjɨn</ts>
                  <nts id="Seg_1339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1341" n="HIAT:w" s="T347">baːj</ts>
                  <nts id="Seg_1342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_1344" n="HIAT:w" s="T348">gɨmmɨt</ts>
                  <nts id="Seg_1345" n="HIAT:ip">.</nts>
                  <nts id="Seg_1346" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T351" id="Seg_1348" n="HIAT:u" s="T349">
                  <ts e="T350" id="Seg_1350" n="HIAT:w" s="T349">Bajan-toton</ts>
                  <nts id="Seg_1351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_1353" n="HIAT:w" s="T350">olorbut</ts>
                  <nts id="Seg_1354" n="HIAT:ip">.</nts>
                  <nts id="Seg_1355" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T353" id="Seg_1357" n="HIAT:u" s="T351">
                  <ts e="T352" id="Seg_1359" n="HIAT:w" s="T351">Elete</ts>
                  <nts id="Seg_1360" n="HIAT:ip">,</nts>
                  <nts id="Seg_1361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_1363" n="HIAT:w" s="T352">bütte</ts>
                  <nts id="Seg_1364" n="HIAT:ip">.</nts>
                  <nts id="Seg_1365" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T353" id="Seg_1366" n="sc" s="T0">
               <ts e="T1" id="Seg_1368" n="e" s="T0">Kaːmɨːlaːk </ts>
               <ts e="T2" id="Seg_1370" n="e" s="T1">kaːman </ts>
               <ts e="T3" id="Seg_1372" n="e" s="T2">ispit. </ts>
               <ts e="T4" id="Seg_1374" n="e" s="T3">Kaːman </ts>
               <ts e="T5" id="Seg_1376" n="e" s="T4">ihen-ihen </ts>
               <ts e="T6" id="Seg_1378" n="e" s="T5">huːrka </ts>
               <ts e="T7" id="Seg_1380" n="e" s="T6">kelbit. </ts>
               <ts e="T8" id="Seg_1382" n="e" s="T7">Buːska </ts>
               <ts e="T9" id="Seg_1384" n="e" s="T8">toŋmut </ts>
               <ts e="T10" id="Seg_1386" n="e" s="T9">kaːnɨ </ts>
               <ts e="T11" id="Seg_1388" n="e" s="T10">körbüt. </ts>
               <ts e="T12" id="Seg_1390" n="e" s="T11">"Taba </ts>
               <ts e="T13" id="Seg_1392" n="e" s="T12">kolun </ts>
               <ts e="T14" id="Seg_1394" n="e" s="T13">bullum", </ts>
               <ts e="T15" id="Seg_1396" n="e" s="T14">diː-diː </ts>
               <ts e="T16" id="Seg_1398" n="e" s="T15">huːmkatɨgar </ts>
               <ts e="T17" id="Seg_1400" n="e" s="T16">uktan </ts>
               <ts e="T18" id="Seg_1402" n="e" s="T17">keːspit. </ts>
               <ts e="T19" id="Seg_1404" n="e" s="T18">Innʼe </ts>
               <ts e="T20" id="Seg_1406" n="e" s="T19">gɨnan </ts>
               <ts e="T21" id="Seg_1408" n="e" s="T20">baraːn, </ts>
               <ts e="T22" id="Seg_1410" n="e" s="T21">kaːman </ts>
               <ts e="T23" id="Seg_1412" n="e" s="T22">ispit. </ts>
               <ts e="T24" id="Seg_1414" n="e" s="T23">Ihen-ihen </ts>
               <ts e="T25" id="Seg_1416" n="e" s="T24">ɨ͡allarga </ts>
               <ts e="T26" id="Seg_1418" n="e" s="T25">kelbit. </ts>
               <ts e="T27" id="Seg_1420" n="e" s="T26">ɨ͡allarga </ts>
               <ts e="T28" id="Seg_1422" n="e" s="T27">kü͡östeːri </ts>
               <ts e="T29" id="Seg_1424" n="e" s="T28">etterin </ts>
               <ts e="T30" id="Seg_1426" n="e" s="T29">etteːbitter. </ts>
               <ts e="T31" id="Seg_1428" n="e" s="T30">"Kaja </ts>
               <ts e="T32" id="Seg_1430" n="e" s="T31">ɨ͡aldʼɨppɨtɨgar </ts>
               <ts e="T33" id="Seg_1432" n="e" s="T32">et </ts>
               <ts e="T34" id="Seg_1434" n="e" s="T33">tijbet </ts>
               <ts e="T35" id="Seg_1436" n="e" s="T34">ebit", </ts>
               <ts e="T36" id="Seg_1438" n="e" s="T35">despitter. </ts>
               <ts e="T37" id="Seg_1440" n="e" s="T36">Onuga </ts>
               <ts e="T38" id="Seg_1442" n="e" s="T37">Kaːmɨːlaːk </ts>
               <ts e="T39" id="Seg_1444" n="e" s="T38">di͡ebit: </ts>
               <ts e="T40" id="Seg_1446" n="e" s="T39">"Kaja </ts>
               <ts e="T41" id="Seg_1448" n="e" s="T40">tu͡ok </ts>
               <ts e="T42" id="Seg_1450" n="e" s="T41">bu͡olagɨt, </ts>
               <ts e="T43" id="Seg_1452" n="e" s="T42">min </ts>
               <ts e="T44" id="Seg_1454" n="e" s="T43">huːmkabar </ts>
               <ts e="T45" id="Seg_1456" n="e" s="T44">et </ts>
               <ts e="T46" id="Seg_1458" n="e" s="T45">baːr </ts>
               <ts e="T47" id="Seg_1460" n="e" s="T46">ete, </ts>
               <ts e="T48" id="Seg_1462" n="e" s="T47">onu </ts>
               <ts e="T49" id="Seg_1464" n="e" s="T48">killeren </ts>
               <ts e="T50" id="Seg_1466" n="e" s="T49">kü͡östeːŋ!" </ts>
               <ts e="T51" id="Seg_1468" n="e" s="T50">Dʼe </ts>
               <ts e="T52" id="Seg_1470" n="e" s="T51">onton </ts>
               <ts e="T53" id="Seg_1472" n="e" s="T52">ɨ͡allar </ts>
               <ts e="T54" id="Seg_1474" n="e" s="T53">kü͡österiger </ts>
               <ts e="T55" id="Seg_1476" n="e" s="T54">Kaːmɨːlaːk </ts>
               <ts e="T56" id="Seg_1478" n="e" s="T55">"etin" </ts>
               <ts e="T57" id="Seg_1480" n="e" s="T56">ugan </ts>
               <ts e="T58" id="Seg_1482" n="e" s="T57">keːspitter. </ts>
               <ts e="T59" id="Seg_1484" n="e" s="T58">Ki͡ehennen </ts>
               <ts e="T60" id="Seg_1486" n="e" s="T59">dʼe </ts>
               <ts e="T61" id="Seg_1488" n="e" s="T60">etterin </ts>
               <ts e="T62" id="Seg_1490" n="e" s="T61">kotorollor. </ts>
               <ts e="T63" id="Seg_1492" n="e" s="T62">Kotorbuttarɨgar </ts>
               <ts e="T64" id="Seg_1494" n="e" s="T63">ɨ͡aldʼɨttarɨgar </ts>
               <ts e="T65" id="Seg_1496" n="e" s="T64">ettere </ts>
               <ts e="T66" id="Seg_1498" n="e" s="T65">emi͡e </ts>
               <ts e="T67" id="Seg_1500" n="e" s="T66">tijbetek. </ts>
               <ts e="T68" id="Seg_1502" n="e" s="T67">Kaːmɨːlaːk </ts>
               <ts e="T69" id="Seg_1504" n="e" s="T68">"kaja, </ts>
               <ts e="T70" id="Seg_1506" n="e" s="T69">kannanɨj </ts>
               <ts e="T71" id="Seg_1508" n="e" s="T70">min </ts>
               <ts e="T72" id="Seg_1510" n="e" s="T71">etim", </ts>
               <ts e="T73" id="Seg_1512" n="e" s="T72">di͡ebit. </ts>
               <ts e="T74" id="Seg_1514" n="e" s="T73">"Öllüm-öllüm. </ts>
               <ts e="T75" id="Seg_1516" n="e" s="T74">Kajdak </ts>
               <ts e="T76" id="Seg_1518" n="e" s="T75">ahaːbakka </ts>
               <ts e="T77" id="Seg_1520" n="e" s="T76">utuju͡omuj?" </ts>
               <ts e="T78" id="Seg_1522" n="e" s="T77">"Kajaː, </ts>
               <ts e="T79" id="Seg_1524" n="e" s="T78">ölüme </ts>
               <ts e="T80" id="Seg_1526" n="e" s="T79">agaj. </ts>
               <ts e="T81" id="Seg_1528" n="e" s="T80">Bihigi </ts>
               <ts e="T82" id="Seg_1530" n="e" s="T81">eji͡eke </ts>
               <ts e="T83" id="Seg_1532" n="e" s="T82">bütün </ts>
               <ts e="T84" id="Seg_1534" n="e" s="T83">tugutta </ts>
               <ts e="T85" id="Seg_1536" n="e" s="T84">bi͡erebit", </ts>
               <ts e="T86" id="Seg_1538" n="e" s="T85">di͡ebitter </ts>
               <ts e="T87" id="Seg_1540" n="e" s="T86">ɨ͡allar. </ts>
               <ts e="T88" id="Seg_1542" n="e" s="T87">Ahaːn </ts>
               <ts e="T89" id="Seg_1544" n="e" s="T88">baran </ts>
               <ts e="T90" id="Seg_1546" n="e" s="T89">utujbuttar, </ts>
               <ts e="T91" id="Seg_1548" n="e" s="T90">tuguttarɨn </ts>
               <ts e="T92" id="Seg_1550" n="e" s="T91">köldörbütter </ts>
               <ts e="T93" id="Seg_1552" n="e" s="T92">Kaːmɨːlaːkka. </ts>
               <ts e="T94" id="Seg_1554" n="e" s="T93">Kaːmɨːlaːk </ts>
               <ts e="T95" id="Seg_1556" n="e" s="T94">tüːn </ts>
               <ts e="T96" id="Seg_1558" n="e" s="T95">taksan </ts>
               <ts e="T97" id="Seg_1560" n="e" s="T96">tuguta </ts>
               <ts e="T98" id="Seg_1562" n="e" s="T97">iːkke </ts>
               <ts e="T99" id="Seg_1564" n="e" s="T98">kelbitin </ts>
               <ts e="T100" id="Seg_1566" n="e" s="T99">tutan </ts>
               <ts e="T101" id="Seg_1568" n="e" s="T100">ɨlbɨt, </ts>
               <ts e="T102" id="Seg_1570" n="e" s="T101">innʼe </ts>
               <ts e="T103" id="Seg_1572" n="e" s="T102">gɨnan </ts>
               <ts e="T104" id="Seg_1574" n="e" s="T103">baraːn </ts>
               <ts e="T105" id="Seg_1576" n="e" s="T104">ikki </ts>
               <ts e="T106" id="Seg_1578" n="e" s="T105">bastɨŋ </ts>
               <ts e="T107" id="Seg_1580" n="e" s="T106">atɨːr </ts>
               <ts e="T108" id="Seg_1582" n="e" s="T107">mu͡ostarɨgar </ts>
               <ts e="T109" id="Seg_1584" n="e" s="T108">iːlen </ts>
               <ts e="T110" id="Seg_1586" n="e" s="T109">keːspit. </ts>
               <ts e="T111" id="Seg_1588" n="e" s="T110">Kiːren </ts>
               <ts e="T112" id="Seg_1590" n="e" s="T111">utujan </ts>
               <ts e="T113" id="Seg_1592" n="e" s="T112">kaːlbɨt. </ts>
               <ts e="T114" id="Seg_1594" n="e" s="T113">Harsi͡erda </ts>
               <ts e="T115" id="Seg_1596" n="e" s="T114">ɨ͡allar </ts>
               <ts e="T116" id="Seg_1598" n="e" s="T115">körbüttere, </ts>
               <ts e="T117" id="Seg_1600" n="e" s="T116">Kaːmɨːlaːk </ts>
               <ts e="T118" id="Seg_1602" n="e" s="T117">tugutun </ts>
               <ts e="T119" id="Seg_1604" n="e" s="T118">ikki </ts>
               <ts e="T120" id="Seg_1606" n="e" s="T119">bastɨŋ </ts>
               <ts e="T121" id="Seg_1608" n="e" s="T120">atɨːrdara </ts>
               <ts e="T122" id="Seg_1610" n="e" s="T121">mu͡ostarɨgar </ts>
               <ts e="T123" id="Seg_1612" n="e" s="T122">iːle </ts>
               <ts e="T124" id="Seg_1614" n="e" s="T123">hɨldʼallar. </ts>
               <ts e="T125" id="Seg_1616" n="e" s="T124">Kaːmɨːlaːk </ts>
               <ts e="T126" id="Seg_1618" n="e" s="T125">onu </ts>
               <ts e="T127" id="Seg_1620" n="e" s="T126">ihitte </ts>
               <ts e="T128" id="Seg_1622" n="e" s="T127">daːganɨ: </ts>
               <ts e="T129" id="Seg_1624" n="e" s="T128">"Dʼe </ts>
               <ts e="T130" id="Seg_1626" n="e" s="T129">ile </ts>
               <ts e="T131" id="Seg_1628" n="e" s="T130">ölöbün </ts>
               <ts e="T132" id="Seg_1630" n="e" s="T131">ebit! </ts>
               <ts e="T133" id="Seg_1632" n="e" s="T132">Tuguta </ts>
               <ts e="T134" id="Seg_1634" n="e" s="T133">hu͡ok </ts>
               <ts e="T135" id="Seg_1636" n="e" s="T134">kaːllɨm", </ts>
               <ts e="T136" id="Seg_1638" n="e" s="T135">di͡en </ts>
               <ts e="T137" id="Seg_1640" n="e" s="T136">üːgüleːbit. </ts>
               <ts e="T138" id="Seg_1642" n="e" s="T137">"Kaja, </ts>
               <ts e="T139" id="Seg_1644" n="e" s="T138">ölüme </ts>
               <ts e="T140" id="Seg_1646" n="e" s="T139">agaj! </ts>
               <ts e="T141" id="Seg_1648" n="e" s="T140">Bihigi </ts>
               <ts e="T142" id="Seg_1650" n="e" s="T141">eji͡eke </ts>
               <ts e="T143" id="Seg_1652" n="e" s="T142">ol </ts>
               <ts e="T144" id="Seg_1654" n="e" s="T143">ikki </ts>
               <ts e="T145" id="Seg_1656" n="e" s="T144">atɨːrɨ </ts>
               <ts e="T146" id="Seg_1658" n="e" s="T145">bi͡eri͡ekpit", </ts>
               <ts e="T147" id="Seg_1660" n="e" s="T146">di͡ebitter </ts>
               <ts e="T148" id="Seg_1662" n="e" s="T147">dʼi͡eteːgilere. </ts>
               <ts e="T149" id="Seg_1664" n="e" s="T148">Kaːmɨːlaːk </ts>
               <ts e="T150" id="Seg_1666" n="e" s="T149">ikki </ts>
               <ts e="T151" id="Seg_1668" n="e" s="T150">atɨːrdammɨt. </ts>
               <ts e="T152" id="Seg_1670" n="e" s="T151">Ikki </ts>
               <ts e="T153" id="Seg_1672" n="e" s="T152">atɨːrɨn </ts>
               <ts e="T154" id="Seg_1674" n="e" s="T153">kölünen </ts>
               <ts e="T155" id="Seg_1676" n="e" s="T154">baran </ts>
               <ts e="T156" id="Seg_1678" n="e" s="T155">Kaːmɨːlaːk </ts>
               <ts e="T157" id="Seg_1680" n="e" s="T156">hɨrgalana </ts>
               <ts e="T158" id="Seg_1682" n="e" s="T157">barbɨt. </ts>
               <ts e="T159" id="Seg_1684" n="e" s="T158">Ihen-ihen </ts>
               <ts e="T160" id="Seg_1686" n="e" s="T159">oŋu͡oktarga </ts>
               <ts e="T161" id="Seg_1688" n="e" s="T160">keppit. </ts>
               <ts e="T162" id="Seg_1690" n="e" s="T161">Onno </ts>
               <ts e="T163" id="Seg_1692" n="e" s="T162">biːr </ts>
               <ts e="T164" id="Seg_1694" n="e" s="T163">emeːksini </ts>
               <ts e="T165" id="Seg_1696" n="e" s="T164">kolbotun </ts>
               <ts e="T166" id="Seg_1698" n="e" s="T165">arɨjan </ts>
               <ts e="T167" id="Seg_1700" n="e" s="T166">kostoːn </ts>
               <ts e="T168" id="Seg_1702" n="e" s="T167">ɨlbɨt, </ts>
               <ts e="T169" id="Seg_1704" n="e" s="T168">hɨrgatɨgar </ts>
               <ts e="T170" id="Seg_1706" n="e" s="T169">uːran </ts>
               <ts e="T171" id="Seg_1708" n="e" s="T170">keːspit. </ts>
               <ts e="T172" id="Seg_1710" n="e" s="T171">Baran </ts>
               <ts e="T173" id="Seg_1712" n="e" s="T172">ispit. </ts>
               <ts e="T174" id="Seg_1714" n="e" s="T173">Ihen-ihen </ts>
               <ts e="T175" id="Seg_1716" n="e" s="T174">biːr </ts>
               <ts e="T176" id="Seg_1718" n="e" s="T175">ɨ͡alga </ts>
               <ts e="T177" id="Seg_1720" n="e" s="T176">tijbit. </ts>
               <ts e="T178" id="Seg_1722" n="e" s="T177">Hɨrgatɨn </ts>
               <ts e="T179" id="Seg_1724" n="e" s="T178">ɨ͡allartan </ts>
               <ts e="T180" id="Seg_1726" n="e" s="T179">ɨraːkkaːn </ts>
               <ts e="T181" id="Seg_1728" n="e" s="T180">keːspit. </ts>
               <ts e="T182" id="Seg_1730" n="e" s="T181">Kaːmɨːlaːk </ts>
               <ts e="T183" id="Seg_1732" n="e" s="T182">tahaːraː </ts>
               <ts e="T184" id="Seg_1734" n="e" s="T183">ogoloru </ts>
               <ts e="T185" id="Seg_1736" n="e" s="T184">körsübut: </ts>
               <ts e="T186" id="Seg_1738" n="e" s="T185">"Ogoloːr, </ts>
               <ts e="T187" id="Seg_1740" n="e" s="T186">min </ts>
               <ts e="T188" id="Seg_1742" n="e" s="T187">hɨrgabar </ts>
               <ts e="T189" id="Seg_1744" n="e" s="T188">čugahaːjagɨt! </ts>
               <ts e="T190" id="Seg_1746" n="e" s="T189">Hɨrgabar </ts>
               <ts e="T191" id="Seg_1748" n="e" s="T190">emeːksinim </ts>
               <ts e="T192" id="Seg_1750" n="e" s="T191">baːr. </ts>
               <ts e="T193" id="Seg_1752" n="e" s="T192">Kihi </ts>
               <ts e="T194" id="Seg_1754" n="e" s="T193">čugahaːta </ts>
               <ts e="T195" id="Seg_1756" n="e" s="T194">da </ts>
               <ts e="T196" id="Seg_1758" n="e" s="T195">hohujan </ts>
               <ts e="T197" id="Seg_1760" n="e" s="T196">ölön </ts>
               <ts e="T198" id="Seg_1762" n="e" s="T197">kaːlɨ͡aga", </ts>
               <ts e="T199" id="Seg_1764" n="e" s="T198">di͡ebit. </ts>
               <ts e="T200" id="Seg_1766" n="e" s="T199">Kaːmɨːlaːk </ts>
               <ts e="T201" id="Seg_1768" n="e" s="T200">čaːj </ts>
               <ts e="T202" id="Seg_1770" n="e" s="T201">ihe </ts>
               <ts e="T203" id="Seg_1772" n="e" s="T202">olordoguna </ts>
               <ts e="T204" id="Seg_1774" n="e" s="T203">ogolor </ts>
               <ts e="T205" id="Seg_1776" n="e" s="T204">hüːren </ts>
               <ts e="T206" id="Seg_1778" n="e" s="T205">kiːrbitter: </ts>
               <ts e="T207" id="Seg_1780" n="e" s="T206">Kaːmɨːlaːk, </ts>
               <ts e="T208" id="Seg_1782" n="e" s="T207">emeːksiniŋ </ts>
               <ts e="T209" id="Seg_1784" n="e" s="T208">ölön </ts>
               <ts e="T210" id="Seg_1786" n="e" s="T209">kaːlbɨt", </ts>
               <ts e="T211" id="Seg_1788" n="e" s="T210">di͡ebitter. </ts>
               <ts e="T212" id="Seg_1790" n="e" s="T211">"Araː! </ts>
               <ts e="T213" id="Seg_1792" n="e" s="T212">Anɨ </ts>
               <ts e="T214" id="Seg_1794" n="e" s="T213">bagas </ts>
               <ts e="T215" id="Seg_1796" n="e" s="T214">ile </ts>
               <ts e="T216" id="Seg_1798" n="e" s="T215">ölöbün </ts>
               <ts e="T217" id="Seg_1800" n="e" s="T216">ebit! </ts>
               <ts e="T218" id="Seg_1802" n="e" s="T217">Emeːksine </ts>
               <ts e="T219" id="Seg_1804" n="e" s="T218">hu͡ok </ts>
               <ts e="T220" id="Seg_1806" n="e" s="T219">kaːllɨm! </ts>
               <ts e="T221" id="Seg_1808" n="e" s="T220">Iti </ts>
               <ts e="T222" id="Seg_1810" n="e" s="T221">ogolor </ts>
               <ts e="T223" id="Seg_1812" n="e" s="T222">hɨrgaga </ts>
               <ts e="T224" id="Seg_1814" n="e" s="T223">čugahaːnnar", </ts>
               <ts e="T225" id="Seg_1816" n="e" s="T224">Kaːmɨːlaːk </ts>
               <ts e="T226" id="Seg_1818" n="e" s="T225">üːgüleːbit. </ts>
               <ts e="T227" id="Seg_1820" n="e" s="T226">"Togo </ts>
               <ts e="T228" id="Seg_1822" n="e" s="T227">ölöːrü </ts>
               <ts e="T229" id="Seg_1824" n="e" s="T228">gɨnagɨn, </ts>
               <ts e="T230" id="Seg_1826" n="e" s="T229">kaja! </ts>
               <ts e="T231" id="Seg_1828" n="e" s="T230">ɨl </ts>
               <ts e="T232" id="Seg_1830" n="e" s="T231">iti </ts>
               <ts e="T233" id="Seg_1832" n="e" s="T232">ikki </ts>
               <ts e="T234" id="Seg_1834" n="e" s="T233">kɨːspɨtɨn", </ts>
               <ts e="T235" id="Seg_1836" n="e" s="T234">despitter </ts>
               <ts e="T236" id="Seg_1838" n="e" s="T235">dʼi͡eleːkter. </ts>
               <ts e="T237" id="Seg_1840" n="e" s="T236">Kaːmɨːlaːk </ts>
               <ts e="T238" id="Seg_1842" n="e" s="T237">onon </ts>
               <ts e="T239" id="Seg_1844" n="e" s="T238">ikki </ts>
               <ts e="T240" id="Seg_1846" n="e" s="T239">kɨːstammɨt. </ts>
               <ts e="T241" id="Seg_1848" n="e" s="T240">Kɨrgɨttarɨn </ts>
               <ts e="T242" id="Seg_1850" n="e" s="T241">ti͡ejen </ts>
               <ts e="T243" id="Seg_1852" n="e" s="T242">baran </ts>
               <ts e="T244" id="Seg_1854" n="e" s="T243">Kaːmɨːlaːk </ts>
               <ts e="T245" id="Seg_1856" n="e" s="T244">baran </ts>
               <ts e="T246" id="Seg_1858" n="e" s="T245">ispit </ts>
               <ts e="T247" id="Seg_1860" n="e" s="T246">dʼe. </ts>
               <ts e="T248" id="Seg_1862" n="e" s="T247">Baran </ts>
               <ts e="T249" id="Seg_1864" n="e" s="T248">ihen-ihen </ts>
               <ts e="T250" id="Seg_1866" n="e" s="T249">biːr </ts>
               <ts e="T251" id="Seg_1868" n="e" s="T250">baːj </ts>
               <ts e="T252" id="Seg_1870" n="e" s="T251">kirili͡ehin </ts>
               <ts e="T253" id="Seg_1872" n="e" s="T252">körbüt. </ts>
               <ts e="T254" id="Seg_1874" n="e" s="T253">Onu </ts>
               <ts e="T255" id="Seg_1876" n="e" s="T254">körön </ts>
               <ts e="T256" id="Seg_1878" n="e" s="T255">Kaːmɨːlaːk </ts>
               <ts e="T257" id="Seg_1880" n="e" s="T256">kɨrgɨttarɨgar </ts>
               <ts e="T258" id="Seg_1882" n="e" s="T257">hanarbɨt: </ts>
               <ts e="T259" id="Seg_1884" n="e" s="T258">"Kaja, </ts>
               <ts e="T260" id="Seg_1886" n="e" s="T259">kɨrgɨttar, </ts>
               <ts e="T261" id="Seg_1888" n="e" s="T260">barɨŋ </ts>
               <ts e="T262" id="Seg_1890" n="e" s="T261">taksɨŋ </ts>
               <ts e="T263" id="Seg_1892" n="e" s="T262">tiːt </ts>
               <ts e="T264" id="Seg_1894" n="e" s="T263">maska! </ts>
               <ts e="T265" id="Seg_1896" n="e" s="T264">Kistenen </ts>
               <ts e="T266" id="Seg_1898" n="e" s="T265">oloruŋ. </ts>
               <ts e="T267" id="Seg_1900" n="e" s="T266">Min </ts>
               <ts e="T268" id="Seg_1902" n="e" s="T267">baːjga </ts>
               <ts e="T269" id="Seg_1904" n="e" s="T268">barɨ͡am. </ts>
               <ts e="T270" id="Seg_1906" n="e" s="T269">Ginini </ts>
               <ts e="T271" id="Seg_1908" n="e" s="T270">munna </ts>
               <ts e="T272" id="Seg_1910" n="e" s="T271">egeli͡em. </ts>
               <ts e="T273" id="Seg_1912" n="e" s="T272">Baːj </ts>
               <ts e="T274" id="Seg_1914" n="e" s="T273">"kimi͡ene </ts>
               <ts e="T275" id="Seg_1916" n="e" s="T274">baːjaj" </ts>
               <ts e="T276" id="Seg_1918" n="e" s="T275">diː-diː </ts>
               <ts e="T277" id="Seg_1920" n="e" s="T276">ɨjɨtɨ͡a. </ts>
               <ts e="T278" id="Seg_1922" n="e" s="T277">Onuga </ts>
               <ts e="T279" id="Seg_1924" n="e" s="T278">ehigi </ts>
               <ts e="T280" id="Seg_1926" n="e" s="T279">di͡eriŋ: </ts>
               <ts e="T281" id="Seg_1928" n="e" s="T280">"Kaːmɨːlaːk </ts>
               <ts e="T282" id="Seg_1930" n="e" s="T281">baːja!"" </ts>
               <ts e="T283" id="Seg_1932" n="e" s="T282">Onu </ts>
               <ts e="T284" id="Seg_1934" n="e" s="T283">haŋaran </ts>
               <ts e="T285" id="Seg_1936" n="e" s="T284">baran </ts>
               <ts e="T286" id="Seg_1938" n="e" s="T285">Kaːmɨːlaːk </ts>
               <ts e="T287" id="Seg_1940" n="e" s="T286">baːjga </ts>
               <ts e="T288" id="Seg_1942" n="e" s="T287">tijbit. </ts>
               <ts e="T289" id="Seg_1944" n="e" s="T288">Čaːj </ts>
               <ts e="T290" id="Seg_1946" n="e" s="T289">ihen </ts>
               <ts e="T291" id="Seg_1948" n="e" s="T290">büten </ts>
               <ts e="T292" id="Seg_1950" n="e" s="T291">di͡ebit: </ts>
               <ts e="T293" id="Seg_1952" n="e" s="T292">"Kaja, </ts>
               <ts e="T294" id="Seg_1954" n="e" s="T293">dogoː, </ts>
               <ts e="T295" id="Seg_1956" n="e" s="T294">kimmit </ts>
               <ts e="T296" id="Seg_1958" n="e" s="T295">baːjaj </ts>
               <ts e="T297" id="Seg_1960" n="e" s="T296">bu?" </ts>
               <ts e="T298" id="Seg_1962" n="e" s="T297">Onno </ts>
               <ts e="T299" id="Seg_1964" n="e" s="T298">baːj </ts>
               <ts e="T300" id="Seg_1966" n="e" s="T299">hohuja </ts>
               <ts e="T301" id="Seg_1968" n="e" s="T300">hɨhan </ts>
               <ts e="T302" id="Seg_1970" n="e" s="T301">baran </ts>
               <ts e="T303" id="Seg_1972" n="e" s="T302">eppit: </ts>
               <ts e="T304" id="Seg_1974" n="e" s="T303">"Kimi͡ene </ts>
               <ts e="T305" id="Seg_1976" n="e" s="T304">bu͡olu͡oj? </ts>
               <ts e="T306" id="Seg_1978" n="e" s="T305">Mi͡ene </ts>
               <ts e="T307" id="Seg_1980" n="e" s="T306">bu͡olumuja!" </ts>
               <ts e="T308" id="Seg_1982" n="e" s="T307">Kaːmɨːlaːk: </ts>
               <ts e="T309" id="Seg_1984" n="e" s="T308">"Hu͡ok! </ts>
               <ts e="T310" id="Seg_1986" n="e" s="T309">Mi͡ene! </ts>
               <ts e="T311" id="Seg_1988" n="e" s="T310">Itegejbet </ts>
               <ts e="T312" id="Seg_1990" n="e" s="T311">bu͡ollakkɨna </ts>
               <ts e="T313" id="Seg_1992" n="e" s="T312">barɨ͡ak </ts>
               <ts e="T314" id="Seg_1994" n="e" s="T313">taŋarattan </ts>
               <ts e="T315" id="Seg_1996" n="e" s="T314">ɨjɨta", </ts>
               <ts e="T316" id="Seg_1998" n="e" s="T315">di͡ebit. </ts>
               <ts e="T317" id="Seg_2000" n="e" s="T316">Barbɨttar. </ts>
               <ts e="T318" id="Seg_2002" n="e" s="T317">Kɨrgɨttar </ts>
               <ts e="T319" id="Seg_2004" n="e" s="T318">hɨtar </ts>
               <ts e="T320" id="Seg_2006" n="e" s="T319">hirderiger </ts>
               <ts e="T321" id="Seg_2008" n="e" s="T320">kelen </ts>
               <ts e="T322" id="Seg_2010" n="e" s="T321">baran, </ts>
               <ts e="T323" id="Seg_2012" n="e" s="T322">Kaːmɨːlaːk </ts>
               <ts e="T324" id="Seg_2014" n="e" s="T323">üːgüleːbit: </ts>
               <ts e="T325" id="Seg_2016" n="e" s="T324">Bu </ts>
               <ts e="T326" id="Seg_2018" n="e" s="T325">kim </ts>
               <ts e="T327" id="Seg_2020" n="e" s="T326">baːjaj?" </ts>
               <ts e="T328" id="Seg_2022" n="e" s="T327">Üːhetten </ts>
               <ts e="T329" id="Seg_2024" n="e" s="T328">kɨrgɨttar </ts>
               <ts e="T330" id="Seg_2026" n="e" s="T329">üːgüleːbitter: </ts>
               <ts e="T331" id="Seg_2028" n="e" s="T330">"Kaːmɨːlaːk </ts>
               <ts e="T332" id="Seg_2030" n="e" s="T331">baːja, </ts>
               <ts e="T333" id="Seg_2032" n="e" s="T332">Kaːmɨːlaːk </ts>
               <ts e="T334" id="Seg_2034" n="e" s="T333">baːja!" </ts>
               <ts e="T335" id="Seg_2036" n="e" s="T334">"Ihittiŋ </ts>
               <ts e="T336" id="Seg_2038" n="e" s="T335">du͡o", </ts>
               <ts e="T337" id="Seg_2040" n="e" s="T336">di͡ebit </ts>
               <ts e="T338" id="Seg_2042" n="e" s="T337">Kaːmɨːlaːk, </ts>
               <ts e="T339" id="Seg_2044" n="e" s="T338">"kim </ts>
               <ts e="T340" id="Seg_2046" n="e" s="T339">baːja </ts>
               <ts e="T341" id="Seg_2048" n="e" s="T340">ebitij?" </ts>
               <ts e="T342" id="Seg_2050" n="e" s="T341">Kaːmɨːlaːk </ts>
               <ts e="T343" id="Seg_2052" n="e" s="T342">iti </ts>
               <ts e="T344" id="Seg_2054" n="e" s="T343">baːjɨ </ts>
               <ts e="T345" id="Seg_2056" n="e" s="T344">batan </ts>
               <ts e="T346" id="Seg_2058" n="e" s="T345">ɨːppɨt, </ts>
               <ts e="T347" id="Seg_2060" n="e" s="T346">baːjɨn </ts>
               <ts e="T348" id="Seg_2062" n="e" s="T347">baːj </ts>
               <ts e="T349" id="Seg_2064" n="e" s="T348">gɨmmɨt. </ts>
               <ts e="T350" id="Seg_2066" n="e" s="T349">Bajan-toton </ts>
               <ts e="T351" id="Seg_2068" n="e" s="T350">olorbut. </ts>
               <ts e="T352" id="Seg_2070" n="e" s="T351">Elete, </ts>
               <ts e="T353" id="Seg_2072" n="e" s="T352">bütte. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T3" id="Seg_2073" s="T0">PoS_PrG_1964_Kaamyylaak_flk.001 (001.001)</ta>
            <ta e="T7" id="Seg_2074" s="T3">PoS_PrG_1964_Kaamyylaak_flk.002 (001.002)</ta>
            <ta e="T11" id="Seg_2075" s="T7">PoS_PrG_1964_Kaamyylaak_flk.003 (001.003)</ta>
            <ta e="T18" id="Seg_2076" s="T11">PoS_PrG_1964_Kaamyylaak_flk.004 (001.004)</ta>
            <ta e="T23" id="Seg_2077" s="T18">PoS_PrG_1964_Kaamyylaak_flk.005 (001.005)</ta>
            <ta e="T26" id="Seg_2078" s="T23">PoS_PrG_1964_Kaamyylaak_flk.006 (001.006)</ta>
            <ta e="T30" id="Seg_2079" s="T26">PoS_PrG_1964_Kaamyylaak_flk.007 (001.007)</ta>
            <ta e="T36" id="Seg_2080" s="T30">PoS_PrG_1964_Kaamyylaak_flk.008 (001.008)</ta>
            <ta e="T39" id="Seg_2081" s="T36">PoS_PrG_1964_Kaamyylaak_flk.009 (001.009)</ta>
            <ta e="T50" id="Seg_2082" s="T39">PoS_PrG_1964_Kaamyylaak_flk.010 (001.009)</ta>
            <ta e="T58" id="Seg_2083" s="T50">PoS_PrG_1964_Kaamyylaak_flk.011 (001.010)</ta>
            <ta e="T62" id="Seg_2084" s="T58">PoS_PrG_1964_Kaamyylaak_flk.012 (001.011)</ta>
            <ta e="T67" id="Seg_2085" s="T62">PoS_PrG_1964_Kaamyylaak_flk.013 (001.012)</ta>
            <ta e="T73" id="Seg_2086" s="T67">PoS_PrG_1964_Kaamyylaak_flk.014 (001.013)</ta>
            <ta e="T74" id="Seg_2087" s="T73">PoS_PrG_1964_Kaamyylaak_flk.015 (001.015)</ta>
            <ta e="T77" id="Seg_2088" s="T74">PoS_PrG_1964_Kaamyylaak_flk.016 (001.016)</ta>
            <ta e="T80" id="Seg_2089" s="T77">PoS_PrG_1964_Kaamyylaak_flk.017 (001.017)</ta>
            <ta e="T87" id="Seg_2090" s="T80">PoS_PrG_1964_Kaamyylaak_flk.018 (001.018)</ta>
            <ta e="T93" id="Seg_2091" s="T87">PoS_PrG_1964_Kaamyylaak_flk.019 (001.019)</ta>
            <ta e="T110" id="Seg_2092" s="T93">PoS_PrG_1964_Kaamyylaak_flk.020 (001.020)</ta>
            <ta e="T113" id="Seg_2093" s="T110">PoS_PrG_1964_Kaamyylaak_flk.021 (001.021)</ta>
            <ta e="T124" id="Seg_2094" s="T113">PoS_PrG_1964_Kaamyylaak_flk.022 (001.022)</ta>
            <ta e="T128" id="Seg_2095" s="T124">PoS_PrG_1964_Kaamyylaak_flk.023 (001.023)</ta>
            <ta e="T132" id="Seg_2096" s="T128">PoS_PrG_1964_Kaamyylaak_flk.024 (001.023)</ta>
            <ta e="T137" id="Seg_2097" s="T132">PoS_PrG_1964_Kaamyylaak_flk.025 (001.024)</ta>
            <ta e="T140" id="Seg_2098" s="T137">PoS_PrG_1964_Kaamyylaak_flk.026 (001.026)</ta>
            <ta e="T148" id="Seg_2099" s="T140">PoS_PrG_1964_Kaamyylaak_flk.027 (001.027)</ta>
            <ta e="T151" id="Seg_2100" s="T148">PoS_PrG_1964_Kaamyylaak_flk.028 (001.029)</ta>
            <ta e="T158" id="Seg_2101" s="T151">PoS_PrG_1964_Kaamyylaak_flk.029 (001.030)</ta>
            <ta e="T161" id="Seg_2102" s="T158">PoS_PrG_1964_Kaamyylaak_flk.030 (001.031)</ta>
            <ta e="T171" id="Seg_2103" s="T161">PoS_PrG_1964_Kaamyylaak_flk.031 (001.032)</ta>
            <ta e="T173" id="Seg_2104" s="T171">PoS_PrG_1964_Kaamyylaak_flk.032 (001.033)</ta>
            <ta e="T177" id="Seg_2105" s="T173">PoS_PrG_1964_Kaamyylaak_flk.033 (001.034)</ta>
            <ta e="T181" id="Seg_2106" s="T177">PoS_PrG_1964_Kaamyylaak_flk.034 (001.035)</ta>
            <ta e="T185" id="Seg_2107" s="T181">PoS_PrG_1964_Kaamyylaak_flk.035 (001.036)</ta>
            <ta e="T189" id="Seg_2108" s="T185">PoS_PrG_1964_Kaamyylaak_flk.036 (001.036)</ta>
            <ta e="T192" id="Seg_2109" s="T189">PoS_PrG_1964_Kaamyylaak_flk.037 (001.037)</ta>
            <ta e="T199" id="Seg_2110" s="T192">PoS_PrG_1964_Kaamyylaak_flk.038 (001.038)</ta>
            <ta e="T206" id="Seg_2111" s="T199">PoS_PrG_1964_Kaamyylaak_flk.039 (001.040)</ta>
            <ta e="T211" id="Seg_2112" s="T206">PoS_PrG_1964_Kaamyylaak_flk.040 (001.041)</ta>
            <ta e="T212" id="Seg_2113" s="T211">PoS_PrG_1964_Kaamyylaak_flk.041 (001.042)</ta>
            <ta e="T217" id="Seg_2114" s="T212">PoS_PrG_1964_Kaamyylaak_flk.042 (001.043)</ta>
            <ta e="T220" id="Seg_2115" s="T217">PoS_PrG_1964_Kaamyylaak_flk.043 (001.044)</ta>
            <ta e="T226" id="Seg_2116" s="T220">PoS_PrG_1964_Kaamyylaak_flk.044 (001.045)</ta>
            <ta e="T230" id="Seg_2117" s="T226">PoS_PrG_1964_Kaamyylaak_flk.045 (001.047)</ta>
            <ta e="T236" id="Seg_2118" s="T230">PoS_PrG_1964_Kaamyylaak_flk.046 (001.048)</ta>
            <ta e="T240" id="Seg_2119" s="T236">PoS_PrG_1964_Kaamyylaak_flk.047 (001.050)</ta>
            <ta e="T247" id="Seg_2120" s="T240">PoS_PrG_1964_Kaamyylaak_flk.048 (001.051)</ta>
            <ta e="T253" id="Seg_2121" s="T247">PoS_PrG_1964_Kaamyylaak_flk.049 (001.052)</ta>
            <ta e="T258" id="Seg_2122" s="T253">PoS_PrG_1964_Kaamyylaak_flk.050 (001.053)</ta>
            <ta e="T264" id="Seg_2123" s="T258">PoS_PrG_1964_Kaamyylaak_flk.051 (001.053)</ta>
            <ta e="T266" id="Seg_2124" s="T264">PoS_PrG_1964_Kaamyylaak_flk.052 (001.054)</ta>
            <ta e="T269" id="Seg_2125" s="T266">PoS_PrG_1964_Kaamyylaak_flk.053 (001.055)</ta>
            <ta e="T272" id="Seg_2126" s="T269">PoS_PrG_1964_Kaamyylaak_flk.054 (001.056)</ta>
            <ta e="T277" id="Seg_2127" s="T272">PoS_PrG_1964_Kaamyylaak_flk.055 (001.057)</ta>
            <ta e="T280" id="Seg_2128" s="T277">PoS_PrG_1964_Kaamyylaak_flk.056 (001.059)</ta>
            <ta e="T282" id="Seg_2129" s="T280">PoS_PrG_1964_Kaamyylaak_flk.057 (001.059)</ta>
            <ta e="T288" id="Seg_2130" s="T282">PoS_PrG_1964_Kaamyylaak_flk.058 (001.060)</ta>
            <ta e="T292" id="Seg_2131" s="T288">PoS_PrG_1964_Kaamyylaak_flk.059 (001.061)</ta>
            <ta e="T297" id="Seg_2132" s="T292">PoS_PrG_1964_Kaamyylaak_flk.060 (001.061)</ta>
            <ta e="T303" id="Seg_2133" s="T297">PoS_PrG_1964_Kaamyylaak_flk.061 (001.062)</ta>
            <ta e="T305" id="Seg_2134" s="T303">PoS_PrG_1964_Kaamyylaak_flk.062 (001.062)</ta>
            <ta e="T307" id="Seg_2135" s="T305">PoS_PrG_1964_Kaamyylaak_flk.063 (001.063)</ta>
            <ta e="T308" id="Seg_2136" s="T307">PoS_PrG_1964_Kaamyylaak_flk.064 (001.064)</ta>
            <ta e="T309" id="Seg_2137" s="T308">PoS_PrG_1964_Kaamyylaak_flk.065 (001.062)</ta>
            <ta e="T310" id="Seg_2138" s="T309">PoS_PrG_1964_Kaamyylaak_flk.066 (001.065)</ta>
            <ta e="T316" id="Seg_2139" s="T310">PoS_PrG_1964_Kaamyylaak_flk.067 (001.066)</ta>
            <ta e="T317" id="Seg_2140" s="T316">PoS_PrG_1964_Kaamyylaak_flk.068 (001.068)</ta>
            <ta e="T324" id="Seg_2141" s="T317">PoS_PrG_1964_Kaamyylaak_flk.069 (001.069)</ta>
            <ta e="T327" id="Seg_2142" s="T324">PoS_PrG_1964_Kaamyylaak_flk.070 (001.069)</ta>
            <ta e="T330" id="Seg_2143" s="T327">PoS_PrG_1964_Kaamyylaak_flk.071 (001.070)</ta>
            <ta e="T334" id="Seg_2144" s="T330">PoS_PrG_1964_Kaamyylaak_flk.072 (001.070)</ta>
            <ta e="T341" id="Seg_2145" s="T334">PoS_PrG_1964_Kaamyylaak_flk.073 (001.071)</ta>
            <ta e="T349" id="Seg_2146" s="T341">PoS_PrG_1964_Kaamyylaak_flk.074 (001.073)</ta>
            <ta e="T351" id="Seg_2147" s="T349">PoS_PrG_1964_Kaamyylaak_flk.075 (001.074)</ta>
            <ta e="T353" id="Seg_2148" s="T351">PoS_PrG_1964_Kaamyylaak_flk.076 (001.075)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T3" id="Seg_2149" s="T0">Каамыылаак кааман испит.</ta>
            <ta e="T7" id="Seg_2150" s="T3">Кааман иһэн-иһэн һуурка кэлбит.</ta>
            <ta e="T11" id="Seg_2151" s="T7">Бууска тоҥмут кааны көрбүт.</ta>
            <ta e="T18" id="Seg_2152" s="T11">"Таба колун буллум", — дии-дии һуумкатыгар уктан кээспит.</ta>
            <ta e="T23" id="Seg_2153" s="T18">Инньэ гынан бараан, кааман испит.</ta>
            <ta e="T26" id="Seg_2154" s="T23">Иһэн-иһэн ыалларга кэлбит.</ta>
            <ta e="T30" id="Seg_2155" s="T26">Ыалларга күөстээри эттэрин эттээбиттэр.</ta>
            <ta e="T36" id="Seg_2156" s="T30">— Кайа ыалдьыппытыгар эт тийбэт эбит, — дэспиттэр.</ta>
            <ta e="T39" id="Seg_2157" s="T36">Онуга Каамыылаак диэбит: </ta>
            <ta e="T50" id="Seg_2158" s="T39">Кайа туок буолагыт, мин һуумкабар эт баар этэ, ону киллэрэн күөстээҥ!</ta>
            <ta e="T58" id="Seg_2159" s="T50">Дьэ онтон ыаллар күөстэригэр Каамыылаак "этин" уган кээспиттэр.</ta>
            <ta e="T62" id="Seg_2160" s="T58">Киэһэннэн дьэ эттэрин котороллор.</ta>
            <ta e="T67" id="Seg_2161" s="T62">Которбуттарыгар ыалдьыттарыгар эттэрэ эмиэ тийбэтэк.</ta>
            <ta e="T73" id="Seg_2162" s="T67">Каамыылаак: — Кайа, каннаный мин этим? — диэбит.</ta>
            <ta e="T74" id="Seg_2163" s="T73">— Өллүм-өллүм.</ta>
            <ta e="T77" id="Seg_2164" s="T74">Кай-дак аһаабакка утуйуомуй?!</ta>
            <ta e="T80" id="Seg_2165" s="T77">— Кайаа, өлүмэ агай.</ta>
            <ta e="T87" id="Seg_2166" s="T80">Биһиги эйиэкэ бүтүн тугутта биэрэбит, — диэбиттэр ыаллар.</ta>
            <ta e="T93" id="Seg_2167" s="T87">Аһаан баран утуйбуттар, тугуттарын көлдөрбүттэр Каамыылаакка.</ta>
            <ta e="T110" id="Seg_2168" s="T93">Каамыылаак түүн таксан тугута ииккэ кэлбитин тутан ылбыт, инньэ гынан бараан икки бастыҥ атыыр муостарыгар иилэн кээспит.</ta>
            <ta e="T113" id="Seg_2169" s="T110">Киирэн утуйан каалбыт.</ta>
            <ta e="T124" id="Seg_2170" s="T113">һарсиэрда ыаллар көрбүттэрэ: Каамыылаак тугутун икки бастыҥ атыырдара муостарыгар иилэ һылдьаллар.</ta>
            <ta e="T128" id="Seg_2171" s="T124">Каамыылаак ону иһиттэ дааганы: </ta>
            <ta e="T132" id="Seg_2172" s="T128">— Дьэ илэ өлөбүн эбит!</ta>
            <ta e="T137" id="Seg_2173" s="T132">Тугута һуок кааллым! — диэн үүгүлээбит.</ta>
            <ta e="T140" id="Seg_2174" s="T137">— Кайа, өлүмэ агай!</ta>
            <ta e="T148" id="Seg_2175" s="T140">Биһиги эйиэкэ ол икки атыыры биэриэкпит! — диэбиттэр дьиэтээгилэрэ.</ta>
            <ta e="T151" id="Seg_2176" s="T148">Каамыылаак икки атыырдаммыт.</ta>
            <ta e="T158" id="Seg_2177" s="T151">Икки атыырын көлүнэн баран Каамыылаак һыргалана барбыт.</ta>
            <ta e="T161" id="Seg_2178" s="T158">Иһэн-иһэн оҥуоктарга кэппит.</ta>
            <ta e="T171" id="Seg_2179" s="T161">Онно биир эмээксини колботун арыйан костоон ылбыт, һыргатыгар ууран кээспит.</ta>
            <ta e="T173" id="Seg_2180" s="T171">Баран испит.</ta>
            <ta e="T177" id="Seg_2181" s="T173">Иһэн-иһэн биир ыалга тийбит,</ta>
            <ta e="T181" id="Seg_2182" s="T177">һыргатын ыаллартан ырааккаан кээспит.</ta>
            <ta e="T185" id="Seg_2183" s="T181">Каамыылаак таһаараа оголору көрсүбут: </ta>
            <ta e="T189" id="Seg_2184" s="T185">— Оголоор, мин һыргабар чугаһаайагыт!</ta>
            <ta e="T192" id="Seg_2185" s="T189">һыргабар эмээксиним баар.</ta>
            <ta e="T199" id="Seg_2186" s="T192">Киһи чугаһаата да һоһуйан өлөн каалыага! — диэбит.</ta>
            <ta e="T206" id="Seg_2187" s="T199">Каамыылаак чаай иһэ олордогуна оголор һүүрэн киирбиттэр: </ta>
            <ta e="T211" id="Seg_2188" s="T206">— Каамыылаак, эмээксиниҥ өлөн каалбыт! — диэбиттэр.</ta>
            <ta e="T212" id="Seg_2189" s="T211">— Араа!</ta>
            <ta e="T217" id="Seg_2190" s="T212">Аны багас илэ өлөбүн эбит!</ta>
            <ta e="T220" id="Seg_2191" s="T217">Эмээксинэ һуок кааллым!</ta>
            <ta e="T226" id="Seg_2192" s="T220">Ити оголор һыргага чугаһааннар! — Каамыылаак үүгүлээбит.</ta>
            <ta e="T230" id="Seg_2193" s="T226">— Того өлөөрү гынагын, кайа!</ta>
            <ta e="T236" id="Seg_2194" s="T230">Ыл ити икки кыыспытын! — дэспиттэр дьиэлээктэр.</ta>
            <ta e="T240" id="Seg_2195" s="T236">Каамыылаак онон икки кыыстаммыт.</ta>
            <ta e="T247" id="Seg_2196" s="T240">Кыргыттарын тиэйэн баран Каамыылаак баран испит дьэ.</ta>
            <ta e="T253" id="Seg_2197" s="T247">Баран иһэн-иһэн биир баай кирилиэһин көрбүт.</ta>
            <ta e="T258" id="Seg_2198" s="T253">Ону көрөн Каамыылаак кыргыттарыгар һанарбыт: </ta>
            <ta e="T264" id="Seg_2199" s="T258">— Кайа, кыргыттар, барыҥ таксыҥ тиит маска!</ta>
            <ta e="T266" id="Seg_2200" s="T264">Кистэнэн олоруҥ.</ta>
            <ta e="T269" id="Seg_2201" s="T266">Мин баайга барыам.</ta>
            <ta e="T272" id="Seg_2202" s="T269">Гинини мунна эгэлиэм.</ta>
            <ta e="T277" id="Seg_2203" s="T272">Баай: "Кимиэнэ баайай!" — дии-дии ыйытыа.</ta>
            <ta e="T280" id="Seg_2204" s="T277">Онуга эһиги диэриҥ: </ta>
            <ta e="T282" id="Seg_2205" s="T280">"Каамыылаак баайа!"</ta>
            <ta e="T288" id="Seg_2206" s="T282">Ону һаҥаран баран Каамыылаак баайга тийбит.</ta>
            <ta e="T292" id="Seg_2207" s="T288">Чаай иһэн бүтэн диэбит: </ta>
            <ta e="T297" id="Seg_2208" s="T292">— Кайа, догоо, киммит баайай бу?</ta>
            <ta e="T303" id="Seg_2209" s="T297">Онно баай һоһуйа һыһан баран эппит: </ta>
            <ta e="T305" id="Seg_2210" s="T303">— Кимиэнэ буолуой?!</ta>
            <ta e="T307" id="Seg_2211" s="T305">Миэнэ буолумуйа!</ta>
            <ta e="T308" id="Seg_2212" s="T307">Каамыылаак: </ta>
            <ta e="T309" id="Seg_2213" s="T308">— Һуок!</ta>
            <ta e="T310" id="Seg_2214" s="T309">Миэнэ!</ta>
            <ta e="T316" id="Seg_2215" s="T310">Итэгэйбэт буоллаккына барыак таҥараттан ыйыта! — диэбит.</ta>
            <ta e="T317" id="Seg_2216" s="T316">Барбыттар.</ta>
            <ta e="T324" id="Seg_2217" s="T317">Кыргыттар һытар һирдэригэр кэлэн баран, Каамыылаак үүгүлээбит: </ta>
            <ta e="T327" id="Seg_2218" s="T324">— Бу ким баайай?</ta>
            <ta e="T330" id="Seg_2219" s="T327">Үүһэттэн кыргыттар үүгүлээбиттэр: </ta>
            <ta e="T334" id="Seg_2220" s="T330">— Каамыылаак баайа, Каамыылаак баайа!</ta>
            <ta e="T341" id="Seg_2221" s="T334">— Иһиттиҥ дуо? — диэбит Каамыылаак — Ким баайа эбитий?!</ta>
            <ta e="T349" id="Seg_2222" s="T341">Каамыылаак ити баайы батан ыыппыт, баайын баай гыммыт.</ta>
            <ta e="T351" id="Seg_2223" s="T349">Байан-тотон олорбут.</ta>
            <ta e="T353" id="Seg_2224" s="T351">Элэтэ, бүттэ.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T3" id="Seg_2225" s="T0">Kaːmɨːlaːk kaːman ispit. </ta>
            <ta e="T7" id="Seg_2226" s="T3">Kaːman ihen-ihen huːrka kelbit. </ta>
            <ta e="T11" id="Seg_2227" s="T7">Buːska toŋmut kaːnɨ körbüt. </ta>
            <ta e="T18" id="Seg_2228" s="T11">"Taba kolun bullum", diː-diː huːmkatɨgar uktan keːspit. </ta>
            <ta e="T23" id="Seg_2229" s="T18">Innʼe gɨnan baraːn, kaːman ispit. </ta>
            <ta e="T26" id="Seg_2230" s="T23">Ihen-ihen ɨ͡allarga kelbit. </ta>
            <ta e="T30" id="Seg_2231" s="T26">ɨ͡allarga kü͡östeːri etterin etteːbitter. </ta>
            <ta e="T36" id="Seg_2232" s="T30">"Kaja ɨ͡aldʼɨppɨtɨgar et tijbet ebit", despitter. </ta>
            <ta e="T39" id="Seg_2233" s="T36">Onuga Kaːmɨːlaːk di͡ebit: </ta>
            <ta e="T50" id="Seg_2234" s="T39">"Kaja tu͡ok bu͡olagɨt, min huːmkabar et baːr ete, onu killeren kü͡östeːŋ!" </ta>
            <ta e="T58" id="Seg_2235" s="T50">Dʼe onton ɨ͡allar kü͡österiger Kaːmɨːlaːk "etin" ugan keːspitter. </ta>
            <ta e="T62" id="Seg_2236" s="T58">Ki͡ehennen dʼe etterin kotorollor. </ta>
            <ta e="T67" id="Seg_2237" s="T62">Kotorbuttarɨgar ɨ͡aldʼɨttarɨgar ettere emi͡e tijbetek. </ta>
            <ta e="T73" id="Seg_2238" s="T67">Kaːmɨːlaːk "kaja, kannanɨj min etim", di͡ebit. </ta>
            <ta e="T74" id="Seg_2239" s="T73">"Öllüm-öllüm. </ta>
            <ta e="T77" id="Seg_2240" s="T74">Kajdak ahaːbakka utuju͡omuj?" </ta>
            <ta e="T80" id="Seg_2241" s="T77">"Kajaː, ölüme agaj. </ta>
            <ta e="T87" id="Seg_2242" s="T80">Bihigi eji͡eke bütün tugutta bi͡erebit", di͡ebitter ɨ͡allar. </ta>
            <ta e="T93" id="Seg_2243" s="T87">Ahaːn baran utujbuttar, tuguttarɨn köldörbütter Kaːmɨːlaːkka. </ta>
            <ta e="T110" id="Seg_2244" s="T93">Kaːmɨːlaːk tüːn taksan tuguta iːkke kelbitin tutan ɨlbɨt, innʼe gɨnan baraːn ikki bastɨŋ atɨːr mu͡ostarɨgar iːlen keːspit. </ta>
            <ta e="T113" id="Seg_2245" s="T110">Kiːren utujan kaːlbɨt. </ta>
            <ta e="T124" id="Seg_2246" s="T113">Harsi͡erda ɨ͡allar körbüttere, Kaːmɨːlaːk tugutun ikki bastɨŋ atɨːrdara mu͡ostarɨgar iːle hɨldʼallar. </ta>
            <ta e="T128" id="Seg_2247" s="T124">Kaːmɨːlaːk onu ihitte daːganɨ: </ta>
            <ta e="T132" id="Seg_2248" s="T128">"Dʼe ile ölöbün ebit! </ta>
            <ta e="T137" id="Seg_2249" s="T132">Tuguta hu͡ok kaːllɨm", di͡en üːgüleːbit. </ta>
            <ta e="T140" id="Seg_2250" s="T137">"Kaja, ölüme agaj! </ta>
            <ta e="T148" id="Seg_2251" s="T140">Bihigi eji͡eke ol ikki atɨːrɨ bi͡eri͡ekpit", di͡ebitter dʼi͡eteːgilere. </ta>
            <ta e="T151" id="Seg_2252" s="T148">Kaːmɨːlaːk ikki atɨːrdammɨt. </ta>
            <ta e="T158" id="Seg_2253" s="T151">Ikki atɨːrɨn kölünen baran Kaːmɨːlaːk hɨrgalana barbɨt. </ta>
            <ta e="T161" id="Seg_2254" s="T158">Ihen-ihen oŋu͡oktarga keppit. </ta>
            <ta e="T171" id="Seg_2255" s="T161">Onno biːr emeːksini kolbotun arɨjan kostoːn ɨlbɨt, hɨrgatɨgar uːran keːspit. </ta>
            <ta e="T173" id="Seg_2256" s="T171">Baran ispit. </ta>
            <ta e="T177" id="Seg_2257" s="T173">Ihen-ihen biːr ɨ͡alga tijbit. </ta>
            <ta e="T181" id="Seg_2258" s="T177">Hɨrgatɨn ɨ͡allartan ɨraːkkaːn keːspit. </ta>
            <ta e="T185" id="Seg_2259" s="T181">Kaːmɨːlaːk tahaːraː ogoloru körsübut: </ta>
            <ta e="T189" id="Seg_2260" s="T185">"Ogoloːr, min hɨrgabar čugahaːjagɨt! </ta>
            <ta e="T192" id="Seg_2261" s="T189">Hɨrgabar emeːksinim baːr. </ta>
            <ta e="T199" id="Seg_2262" s="T192">Kihi čugahaːta da hohujan ölön kaːlɨ͡aga", di͡ebit. </ta>
            <ta e="T206" id="Seg_2263" s="T199">Kaːmɨːlaːk čaːj ihe olordoguna ogolor hüːren kiːrbitter: </ta>
            <ta e="T211" id="Seg_2264" s="T206">"Kaːmɨːlaːk, emeːksiniŋ ölön kaːlbɨt", di͡ebitter. </ta>
            <ta e="T212" id="Seg_2265" s="T211">"Araː! </ta>
            <ta e="T217" id="Seg_2266" s="T212">Anɨ bagas ile ölöbün ebit! </ta>
            <ta e="T220" id="Seg_2267" s="T217">Emeːksine hu͡ok kaːllɨm! </ta>
            <ta e="T226" id="Seg_2268" s="T220">Iti ogolor hɨrgaga čugahaːnnar", Kaːmɨːlaːk üːgüleːbit. </ta>
            <ta e="T230" id="Seg_2269" s="T226">"Togo ölöːrü gɨnagɨn, kaja! </ta>
            <ta e="T236" id="Seg_2270" s="T230">ɨl iti ikki kɨːspɨtɨn", despitter dʼi͡eleːkter. </ta>
            <ta e="T240" id="Seg_2271" s="T236">Kaːmɨːlaːk onon ikki kɨːstammɨt. </ta>
            <ta e="T247" id="Seg_2272" s="T240">Kɨrgɨttarɨn ti͡ejen baran Kaːmɨːlaːk baran ispit dʼe. </ta>
            <ta e="T253" id="Seg_2273" s="T247">Baran ihen-ihen biːr baːj kirili͡ehin körbüt. </ta>
            <ta e="T258" id="Seg_2274" s="T253">Onu körön Kaːmɨːlaːk kɨrgɨttarɨgar hanarbɨt: </ta>
            <ta e="T264" id="Seg_2275" s="T258">"Kaja, kɨrgɨttar, barɨŋ taksɨŋ tiːt maska! </ta>
            <ta e="T266" id="Seg_2276" s="T264">Kistenen oloruŋ. </ta>
            <ta e="T269" id="Seg_2277" s="T266">Min baːjga barɨ͡am. </ta>
            <ta e="T272" id="Seg_2278" s="T269">Ginini munna egeli͡em. </ta>
            <ta e="T277" id="Seg_2279" s="T272">Baːj "kimi͡ene baːjaj" diː-diː ɨjɨtɨ͡a. </ta>
            <ta e="T280" id="Seg_2280" s="T277">Onuga ehigi di͡eriŋ: </ta>
            <ta e="T282" id="Seg_2281" s="T280">"Kaːmɨːlaːk baːja!"" </ta>
            <ta e="T288" id="Seg_2282" s="T282">Onu haŋaran baran Kaːmɨːlaːk baːjga tijbit. </ta>
            <ta e="T292" id="Seg_2283" s="T288">Čaːj ihen büten di͡ebit: </ta>
            <ta e="T297" id="Seg_2284" s="T292">"Kaja, dogoː, kimmit baːjaj bu?" </ta>
            <ta e="T303" id="Seg_2285" s="T297">Onno baːj hohuja hɨhan baran eppit: </ta>
            <ta e="T305" id="Seg_2286" s="T303">"Kimi͡ene bu͡olu͡oj? </ta>
            <ta e="T307" id="Seg_2287" s="T305">Mi͡ene bu͡olumuja!" </ta>
            <ta e="T308" id="Seg_2288" s="T307">Kaːmɨːlaːk: </ta>
            <ta e="T309" id="Seg_2289" s="T308">"Hu͡ok! </ta>
            <ta e="T310" id="Seg_2290" s="T309">Mi͡ene! </ta>
            <ta e="T316" id="Seg_2291" s="T310">Itegejbet bu͡ollakkɨna barɨ͡ak taŋarattan ɨjɨta", di͡ebit. </ta>
            <ta e="T317" id="Seg_2292" s="T316">Barbɨttar. </ta>
            <ta e="T324" id="Seg_2293" s="T317">Kɨrgɨttar hɨtar hirderiger kelen baran, Kaːmɨːlaːk üːgüleːbit: </ta>
            <ta e="T327" id="Seg_2294" s="T324">"Bu kim baːjaj?" </ta>
            <ta e="T330" id="Seg_2295" s="T327">Üːhetten kɨrgɨttar üːgüleːbitter: </ta>
            <ta e="T334" id="Seg_2296" s="T330">"Kaːmɨːlaːk baːja, Kaːmɨːlaːk baːja!" </ta>
            <ta e="T341" id="Seg_2297" s="T334">"Ihittiŋ du͡o", di͡ebit Kaːmɨːlaːk, "kim baːja ebitij?" </ta>
            <ta e="T349" id="Seg_2298" s="T341">Kaːmɨːlaːk iti baːjɨ batan ɨːppɨt, baːjɨn baːj gɨmmɨt. </ta>
            <ta e="T351" id="Seg_2299" s="T349">Bajan-toton olorbut. </ta>
            <ta e="T353" id="Seg_2300" s="T351">Elete, bütte. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_2301" s="T0">Kaːmɨːlaːk</ta>
            <ta e="T2" id="Seg_2302" s="T1">kaːm-an</ta>
            <ta e="T3" id="Seg_2303" s="T2">is-pit</ta>
            <ta e="T4" id="Seg_2304" s="T3">kaːm-an</ta>
            <ta e="T5" id="Seg_2305" s="T4">ih-en-ih-en</ta>
            <ta e="T6" id="Seg_2306" s="T5">huːr-ka</ta>
            <ta e="T7" id="Seg_2307" s="T6">kel-bit</ta>
            <ta e="T8" id="Seg_2308" s="T7">buːs-ka</ta>
            <ta e="T9" id="Seg_2309" s="T8">toŋ-mut</ta>
            <ta e="T10" id="Seg_2310" s="T9">kaːn-ɨ</ta>
            <ta e="T11" id="Seg_2311" s="T10">kör-büt</ta>
            <ta e="T12" id="Seg_2312" s="T11">taba</ta>
            <ta e="T13" id="Seg_2313" s="T12">kol-u-n</ta>
            <ta e="T14" id="Seg_2314" s="T13">bul-lu-m</ta>
            <ta e="T15" id="Seg_2315" s="T14">d-iː-d-iː</ta>
            <ta e="T16" id="Seg_2316" s="T15">huːmka-tɨ-gar</ta>
            <ta e="T17" id="Seg_2317" s="T16">ukt-an</ta>
            <ta e="T18" id="Seg_2318" s="T17">keːs-pit</ta>
            <ta e="T19" id="Seg_2319" s="T18">innʼe</ta>
            <ta e="T20" id="Seg_2320" s="T19">gɨn-an</ta>
            <ta e="T21" id="Seg_2321" s="T20">baraːn</ta>
            <ta e="T22" id="Seg_2322" s="T21">kaːm-an</ta>
            <ta e="T23" id="Seg_2323" s="T22">is-pit</ta>
            <ta e="T24" id="Seg_2324" s="T23">ih-en-ih-en</ta>
            <ta e="T25" id="Seg_2325" s="T24">ɨ͡al-lar-ga</ta>
            <ta e="T26" id="Seg_2326" s="T25">kel-bit</ta>
            <ta e="T27" id="Seg_2327" s="T26">ɨ͡al-lar-ga</ta>
            <ta e="T28" id="Seg_2328" s="T27">kü͡öst-eːri</ta>
            <ta e="T29" id="Seg_2329" s="T28">et-teri-n</ta>
            <ta e="T30" id="Seg_2330" s="T29">et-teː-bit-ter</ta>
            <ta e="T31" id="Seg_2331" s="T30">kaja</ta>
            <ta e="T32" id="Seg_2332" s="T31">ɨ͡aldʼɨp-pɨtɨ-gar</ta>
            <ta e="T33" id="Seg_2333" s="T32">et</ta>
            <ta e="T34" id="Seg_2334" s="T33">tij-bet</ta>
            <ta e="T35" id="Seg_2335" s="T34">e-bit</ta>
            <ta e="T36" id="Seg_2336" s="T35">d-e-s-pit-ter</ta>
            <ta e="T37" id="Seg_2337" s="T36">onu-ga</ta>
            <ta e="T38" id="Seg_2338" s="T37">Kaːmɨːlaːk</ta>
            <ta e="T39" id="Seg_2339" s="T38">di͡e-bit</ta>
            <ta e="T40" id="Seg_2340" s="T39">kaja</ta>
            <ta e="T41" id="Seg_2341" s="T40">tu͡ok</ta>
            <ta e="T42" id="Seg_2342" s="T41">bu͡ol-a-gɨt</ta>
            <ta e="T43" id="Seg_2343" s="T42">min</ta>
            <ta e="T44" id="Seg_2344" s="T43">huːmka-ba-r</ta>
            <ta e="T45" id="Seg_2345" s="T44">et</ta>
            <ta e="T46" id="Seg_2346" s="T45">baːr</ta>
            <ta e="T47" id="Seg_2347" s="T46">e-t-e</ta>
            <ta e="T48" id="Seg_2348" s="T47">o-nu</ta>
            <ta e="T49" id="Seg_2349" s="T48">killer-en</ta>
            <ta e="T50" id="Seg_2350" s="T49">kü͡östeː-ŋ</ta>
            <ta e="T51" id="Seg_2351" s="T50">dʼe</ta>
            <ta e="T52" id="Seg_2352" s="T51">onton</ta>
            <ta e="T53" id="Seg_2353" s="T52">ɨ͡al-lar</ta>
            <ta e="T54" id="Seg_2354" s="T53">kü͡ös-teri-ger</ta>
            <ta e="T55" id="Seg_2355" s="T54">Kaːmɨːlaːk</ta>
            <ta e="T56" id="Seg_2356" s="T55">et-i-n</ta>
            <ta e="T57" id="Seg_2357" s="T56">ug-an</ta>
            <ta e="T58" id="Seg_2358" s="T57">keːs-pit-ter</ta>
            <ta e="T59" id="Seg_2359" s="T58">ki͡ehe-nnen</ta>
            <ta e="T60" id="Seg_2360" s="T59">dʼe</ta>
            <ta e="T61" id="Seg_2361" s="T60">et-teri-n</ta>
            <ta e="T62" id="Seg_2362" s="T61">kotor-ol-lor</ta>
            <ta e="T63" id="Seg_2363" s="T62">kotor-but-tarɨ-gar</ta>
            <ta e="T64" id="Seg_2364" s="T63">ɨ͡aldʼɨt-tarɨ-gar</ta>
            <ta e="T65" id="Seg_2365" s="T64">et-tere</ta>
            <ta e="T66" id="Seg_2366" s="T65">emi͡e</ta>
            <ta e="T67" id="Seg_2367" s="T66">tij-betek</ta>
            <ta e="T68" id="Seg_2368" s="T67">Kaːmɨːlaːk</ta>
            <ta e="T69" id="Seg_2369" s="T68">kaja</ta>
            <ta e="T70" id="Seg_2370" s="T69">kannan=ɨj</ta>
            <ta e="T71" id="Seg_2371" s="T70">min</ta>
            <ta e="T72" id="Seg_2372" s="T71">et-i-m</ta>
            <ta e="T73" id="Seg_2373" s="T72">di͡e-bit</ta>
            <ta e="T74" id="Seg_2374" s="T73">öl-lü-m-öl-lü-m</ta>
            <ta e="T75" id="Seg_2375" s="T74">kajdak</ta>
            <ta e="T76" id="Seg_2376" s="T75">ahaː-bakka</ta>
            <ta e="T77" id="Seg_2377" s="T76">utuj-u͡o-m=uj</ta>
            <ta e="T78" id="Seg_2378" s="T77">kajaː</ta>
            <ta e="T79" id="Seg_2379" s="T78">öl-ü-me</ta>
            <ta e="T80" id="Seg_2380" s="T79">agaj</ta>
            <ta e="T81" id="Seg_2381" s="T80">bihigi</ta>
            <ta e="T82" id="Seg_2382" s="T81">eji͡e-ke</ta>
            <ta e="T83" id="Seg_2383" s="T82">bütün</ta>
            <ta e="T84" id="Seg_2384" s="T83">tugut-ta</ta>
            <ta e="T85" id="Seg_2385" s="T84">bi͡er-e-bit</ta>
            <ta e="T86" id="Seg_2386" s="T85">di͡e-bit-ter</ta>
            <ta e="T87" id="Seg_2387" s="T86">ɨ͡al-lar</ta>
            <ta e="T88" id="Seg_2388" s="T87">ahaː-n</ta>
            <ta e="T89" id="Seg_2389" s="T88">baran</ta>
            <ta e="T90" id="Seg_2390" s="T89">utuj-but-tar</ta>
            <ta e="T91" id="Seg_2391" s="T90">tugut-tarɨ-n</ta>
            <ta e="T92" id="Seg_2392" s="T91">köl-dör-büt-ter</ta>
            <ta e="T93" id="Seg_2393" s="T92">Kaːmɨːlaːk-ka</ta>
            <ta e="T94" id="Seg_2394" s="T93">Kaːmɨːlaːk</ta>
            <ta e="T95" id="Seg_2395" s="T94">tüːn</ta>
            <ta e="T96" id="Seg_2396" s="T95">taks-an</ta>
            <ta e="T97" id="Seg_2397" s="T96">tugut-a</ta>
            <ta e="T98" id="Seg_2398" s="T97">iːkke</ta>
            <ta e="T99" id="Seg_2399" s="T98">kel-bit-i-n</ta>
            <ta e="T100" id="Seg_2400" s="T99">tut-an</ta>
            <ta e="T101" id="Seg_2401" s="T100">ɨl-bɨt</ta>
            <ta e="T102" id="Seg_2402" s="T101">innʼe</ta>
            <ta e="T103" id="Seg_2403" s="T102">gɨn-an</ta>
            <ta e="T104" id="Seg_2404" s="T103">baraːn</ta>
            <ta e="T105" id="Seg_2405" s="T104">ikki</ta>
            <ta e="T106" id="Seg_2406" s="T105">bastɨŋ</ta>
            <ta e="T107" id="Seg_2407" s="T106">atɨːr</ta>
            <ta e="T108" id="Seg_2408" s="T107">mu͡os-tarɨ-gar</ta>
            <ta e="T109" id="Seg_2409" s="T108">iːl-en</ta>
            <ta e="T110" id="Seg_2410" s="T109">keːs-pit</ta>
            <ta e="T111" id="Seg_2411" s="T110">kiːr-en</ta>
            <ta e="T112" id="Seg_2412" s="T111">utuj-an</ta>
            <ta e="T113" id="Seg_2413" s="T112">kaːl-bɨt</ta>
            <ta e="T114" id="Seg_2414" s="T113">harsi͡erda</ta>
            <ta e="T115" id="Seg_2415" s="T114">ɨ͡al-lar</ta>
            <ta e="T116" id="Seg_2416" s="T115">kör-büt-tere</ta>
            <ta e="T117" id="Seg_2417" s="T116">Kaːmɨːlaːk</ta>
            <ta e="T118" id="Seg_2418" s="T117">tugut-u-n</ta>
            <ta e="T119" id="Seg_2419" s="T118">ikki</ta>
            <ta e="T120" id="Seg_2420" s="T119">bastɨŋ</ta>
            <ta e="T121" id="Seg_2421" s="T120">atɨːr-dara</ta>
            <ta e="T122" id="Seg_2422" s="T121">mu͡os-tarɨ-gar</ta>
            <ta e="T123" id="Seg_2423" s="T122">iːl-e</ta>
            <ta e="T124" id="Seg_2424" s="T123">hɨldʼ-al-lar</ta>
            <ta e="T125" id="Seg_2425" s="T124">Kaːmɨːlaːk</ta>
            <ta e="T126" id="Seg_2426" s="T125">o-nu</ta>
            <ta e="T127" id="Seg_2427" s="T126">ihit-t-e</ta>
            <ta e="T128" id="Seg_2428" s="T127">daːganɨ</ta>
            <ta e="T129" id="Seg_2429" s="T128">dʼe</ta>
            <ta e="T130" id="Seg_2430" s="T129">ile</ta>
            <ta e="T131" id="Seg_2431" s="T130">öl-ö-bün</ta>
            <ta e="T132" id="Seg_2432" s="T131">e-bit</ta>
            <ta e="T133" id="Seg_2433" s="T132">tugut-a</ta>
            <ta e="T134" id="Seg_2434" s="T133">hu͡ok</ta>
            <ta e="T135" id="Seg_2435" s="T134">kaːl-lɨ-m</ta>
            <ta e="T136" id="Seg_2436" s="T135">di͡e-n</ta>
            <ta e="T137" id="Seg_2437" s="T136">üːgüleː-bit</ta>
            <ta e="T138" id="Seg_2438" s="T137">kaja</ta>
            <ta e="T139" id="Seg_2439" s="T138">öl-ü-me</ta>
            <ta e="T140" id="Seg_2440" s="T139">agaj</ta>
            <ta e="T141" id="Seg_2441" s="T140">bihigi</ta>
            <ta e="T142" id="Seg_2442" s="T141">eji͡e-ke</ta>
            <ta e="T143" id="Seg_2443" s="T142">ol</ta>
            <ta e="T144" id="Seg_2444" s="T143">ikki</ta>
            <ta e="T145" id="Seg_2445" s="T144">atɨːr-ɨ</ta>
            <ta e="T146" id="Seg_2446" s="T145">bi͡er-i͡ek-pit</ta>
            <ta e="T147" id="Seg_2447" s="T146">di͡e-bit-ter</ta>
            <ta e="T148" id="Seg_2448" s="T147">dʼi͡e-teːgi-lere</ta>
            <ta e="T149" id="Seg_2449" s="T148">Kaːmɨːlaːk</ta>
            <ta e="T150" id="Seg_2450" s="T149">ikki</ta>
            <ta e="T151" id="Seg_2451" s="T150">atɨːr-dam-mɨt</ta>
            <ta e="T152" id="Seg_2452" s="T151">ikki</ta>
            <ta e="T153" id="Seg_2453" s="T152">atɨːr-ɨ-n</ta>
            <ta e="T154" id="Seg_2454" s="T153">kölün-en</ta>
            <ta e="T155" id="Seg_2455" s="T154">baran</ta>
            <ta e="T156" id="Seg_2456" s="T155">Kaːmɨːlaːk</ta>
            <ta e="T157" id="Seg_2457" s="T156">hɨrga-lan-a</ta>
            <ta e="T158" id="Seg_2458" s="T157">bar-bɨt</ta>
            <ta e="T159" id="Seg_2459" s="T158">ih-en-ih-en</ta>
            <ta e="T160" id="Seg_2460" s="T159">oŋu͡ok-tar-ga</ta>
            <ta e="T161" id="Seg_2461" s="T160">kep-pit</ta>
            <ta e="T162" id="Seg_2462" s="T161">onno</ta>
            <ta e="T163" id="Seg_2463" s="T162">biːr</ta>
            <ta e="T164" id="Seg_2464" s="T163">emeːksin-i</ta>
            <ta e="T165" id="Seg_2465" s="T164">kolbo-tu-n</ta>
            <ta e="T166" id="Seg_2466" s="T165">arɨj-an</ta>
            <ta e="T167" id="Seg_2467" s="T166">kostoː-n</ta>
            <ta e="T168" id="Seg_2468" s="T167">ɨl-bɨt</ta>
            <ta e="T169" id="Seg_2469" s="T168">hɨrga-tɨ-gar</ta>
            <ta e="T170" id="Seg_2470" s="T169">uːr-an</ta>
            <ta e="T171" id="Seg_2471" s="T170">keːs-pit</ta>
            <ta e="T172" id="Seg_2472" s="T171">bar-an</ta>
            <ta e="T173" id="Seg_2473" s="T172">is-pit</ta>
            <ta e="T174" id="Seg_2474" s="T173">ih-en-ih-en</ta>
            <ta e="T175" id="Seg_2475" s="T174">biːr</ta>
            <ta e="T176" id="Seg_2476" s="T175">ɨ͡al-ga</ta>
            <ta e="T177" id="Seg_2477" s="T176">tij-bit</ta>
            <ta e="T178" id="Seg_2478" s="T177">hɨrga-tɨ-n</ta>
            <ta e="T179" id="Seg_2479" s="T178">ɨ͡al-lar-tan</ta>
            <ta e="T180" id="Seg_2480" s="T179">ɨraːk-kaːn</ta>
            <ta e="T181" id="Seg_2481" s="T180">keːs-pit</ta>
            <ta e="T182" id="Seg_2482" s="T181">Kaːmɨːlaːk</ta>
            <ta e="T183" id="Seg_2483" s="T182">tahaːraː</ta>
            <ta e="T184" id="Seg_2484" s="T183">ogo-lor-u</ta>
            <ta e="T185" id="Seg_2485" s="T184">körs-ü-but</ta>
            <ta e="T186" id="Seg_2486" s="T185">ogo-loːr</ta>
            <ta e="T187" id="Seg_2487" s="T186">min</ta>
            <ta e="T188" id="Seg_2488" s="T187">hɨrga-ba-r</ta>
            <ta e="T189" id="Seg_2489" s="T188">čugah-aːja-gɨt</ta>
            <ta e="T190" id="Seg_2490" s="T189">hɨrga-ba-r</ta>
            <ta e="T191" id="Seg_2491" s="T190">emeːksin-i-m</ta>
            <ta e="T192" id="Seg_2492" s="T191">baːr</ta>
            <ta e="T193" id="Seg_2493" s="T192">kihi</ta>
            <ta e="T194" id="Seg_2494" s="T193">čugahaː-t-a</ta>
            <ta e="T195" id="Seg_2495" s="T194">da</ta>
            <ta e="T196" id="Seg_2496" s="T195">hohuj-an</ta>
            <ta e="T197" id="Seg_2497" s="T196">öl-ön</ta>
            <ta e="T198" id="Seg_2498" s="T197">kaːl-ɨ͡ag-a</ta>
            <ta e="T199" id="Seg_2499" s="T198">di͡e-bit</ta>
            <ta e="T200" id="Seg_2500" s="T199">Kaːmɨːlaːk</ta>
            <ta e="T201" id="Seg_2501" s="T200">čaːj</ta>
            <ta e="T202" id="Seg_2502" s="T201">ih-e</ta>
            <ta e="T203" id="Seg_2503" s="T202">olor-dog-una</ta>
            <ta e="T204" id="Seg_2504" s="T203">ogo-lor</ta>
            <ta e="T205" id="Seg_2505" s="T204">hüːr-en</ta>
            <ta e="T206" id="Seg_2506" s="T205">kiːr-bit-ter</ta>
            <ta e="T207" id="Seg_2507" s="T206">Kaːmɨːlaːk</ta>
            <ta e="T208" id="Seg_2508" s="T207">emeːksin-i-ŋ</ta>
            <ta e="T209" id="Seg_2509" s="T208">öl-ön</ta>
            <ta e="T210" id="Seg_2510" s="T209">kaːl-bɨt</ta>
            <ta e="T211" id="Seg_2511" s="T210">di͡e-bit-ter</ta>
            <ta e="T212" id="Seg_2512" s="T211">araː</ta>
            <ta e="T213" id="Seg_2513" s="T212">anɨ</ta>
            <ta e="T214" id="Seg_2514" s="T213">bagas</ta>
            <ta e="T215" id="Seg_2515" s="T214">ile</ta>
            <ta e="T216" id="Seg_2516" s="T215">öl-ö-bün</ta>
            <ta e="T217" id="Seg_2517" s="T216">e-bit</ta>
            <ta e="T218" id="Seg_2518" s="T217">emeːksin-e</ta>
            <ta e="T219" id="Seg_2519" s="T218">hu͡ok</ta>
            <ta e="T220" id="Seg_2520" s="T219">kaːl-lɨ-m</ta>
            <ta e="T221" id="Seg_2521" s="T220">iti</ta>
            <ta e="T222" id="Seg_2522" s="T221">ogo-lor</ta>
            <ta e="T223" id="Seg_2523" s="T222">hɨrga-ga</ta>
            <ta e="T224" id="Seg_2524" s="T223">čugahaː-n-nar</ta>
            <ta e="T225" id="Seg_2525" s="T224">Kaːmɨːlaːk</ta>
            <ta e="T226" id="Seg_2526" s="T225">üːgüleː-bit</ta>
            <ta e="T227" id="Seg_2527" s="T226">togo</ta>
            <ta e="T228" id="Seg_2528" s="T227">öl-öːrü</ta>
            <ta e="T229" id="Seg_2529" s="T228">gɨn-a-gɨn</ta>
            <ta e="T230" id="Seg_2530" s="T229">kaja</ta>
            <ta e="T231" id="Seg_2531" s="T230">ɨl</ta>
            <ta e="T232" id="Seg_2532" s="T231">iti</ta>
            <ta e="T233" id="Seg_2533" s="T232">ikki</ta>
            <ta e="T234" id="Seg_2534" s="T233">kɨːs-pɨtɨ-n</ta>
            <ta e="T235" id="Seg_2535" s="T234">d-e-s-pit-ter</ta>
            <ta e="T236" id="Seg_2536" s="T235">dʼi͡e-leːk-ter</ta>
            <ta e="T237" id="Seg_2537" s="T236">Kaːmɨːlaːk</ta>
            <ta e="T238" id="Seg_2538" s="T237">onon</ta>
            <ta e="T239" id="Seg_2539" s="T238">ikki</ta>
            <ta e="T240" id="Seg_2540" s="T239">kɨːs-tam-mɨt</ta>
            <ta e="T241" id="Seg_2541" s="T240">kɨrgɨt-tar-ɨ-n</ta>
            <ta e="T242" id="Seg_2542" s="T241">ti͡ej-en</ta>
            <ta e="T243" id="Seg_2543" s="T242">baran</ta>
            <ta e="T244" id="Seg_2544" s="T243">Kaːmɨːlaːk</ta>
            <ta e="T245" id="Seg_2545" s="T244">bar-an</ta>
            <ta e="T246" id="Seg_2546" s="T245">is-pit</ta>
            <ta e="T247" id="Seg_2547" s="T246">dʼe</ta>
            <ta e="T248" id="Seg_2548" s="T247">bar-an</ta>
            <ta e="T249" id="Seg_2549" s="T248">ih-en-ih-en</ta>
            <ta e="T250" id="Seg_2550" s="T249">biːr</ta>
            <ta e="T251" id="Seg_2551" s="T250">baːj</ta>
            <ta e="T252" id="Seg_2552" s="T251">kirili͡eh-i-n</ta>
            <ta e="T253" id="Seg_2553" s="T252">kör-büt</ta>
            <ta e="T254" id="Seg_2554" s="T253">o-nu</ta>
            <ta e="T255" id="Seg_2555" s="T254">kör-ön</ta>
            <ta e="T256" id="Seg_2556" s="T255">Kaːmɨːlaːk</ta>
            <ta e="T257" id="Seg_2557" s="T256">kɨrgɨt-tar-ɨ-gar</ta>
            <ta e="T258" id="Seg_2558" s="T257">hanar-bɨt</ta>
            <ta e="T259" id="Seg_2559" s="T258">kaja</ta>
            <ta e="T260" id="Seg_2560" s="T259">kɨrgɨt-tar</ta>
            <ta e="T261" id="Seg_2561" s="T260">bar-ɨ-ŋ</ta>
            <ta e="T262" id="Seg_2562" s="T261">taks-ɨ-ŋ</ta>
            <ta e="T263" id="Seg_2563" s="T262">tiːt</ta>
            <ta e="T264" id="Seg_2564" s="T263">mas-ka</ta>
            <ta e="T265" id="Seg_2565" s="T264">kisten-en</ta>
            <ta e="T266" id="Seg_2566" s="T265">olor-u-ŋ</ta>
            <ta e="T267" id="Seg_2567" s="T266">min</ta>
            <ta e="T268" id="Seg_2568" s="T267">baːj-ga</ta>
            <ta e="T269" id="Seg_2569" s="T268">bar-ɨ͡a-m</ta>
            <ta e="T270" id="Seg_2570" s="T269">gini-ni</ta>
            <ta e="T271" id="Seg_2571" s="T270">munna</ta>
            <ta e="T272" id="Seg_2572" s="T271">egel-i͡e-m</ta>
            <ta e="T273" id="Seg_2573" s="T272">baːj</ta>
            <ta e="T274" id="Seg_2574" s="T273">kimi͡ene</ta>
            <ta e="T275" id="Seg_2575" s="T274">baːj-a=j</ta>
            <ta e="T276" id="Seg_2576" s="T275">d-iː-d-iː</ta>
            <ta e="T277" id="Seg_2577" s="T276">ɨjɨt-ɨ͡a</ta>
            <ta e="T278" id="Seg_2578" s="T277">onu-ga</ta>
            <ta e="T279" id="Seg_2579" s="T278">ehigi</ta>
            <ta e="T280" id="Seg_2580" s="T279">di͡e-r-i-ŋ</ta>
            <ta e="T281" id="Seg_2581" s="T280">Kaːmɨːlaːk</ta>
            <ta e="T282" id="Seg_2582" s="T281">baːj-a</ta>
            <ta e="T283" id="Seg_2583" s="T282">o-nu</ta>
            <ta e="T284" id="Seg_2584" s="T283">haŋar-an</ta>
            <ta e="T285" id="Seg_2585" s="T284">baran</ta>
            <ta e="T286" id="Seg_2586" s="T285">Kaːmɨːlaːk</ta>
            <ta e="T287" id="Seg_2587" s="T286">baːj-ga</ta>
            <ta e="T288" id="Seg_2588" s="T287">tij-bit</ta>
            <ta e="T289" id="Seg_2589" s="T288">čaːj</ta>
            <ta e="T290" id="Seg_2590" s="T289">ih-en</ta>
            <ta e="T291" id="Seg_2591" s="T290">büt-en</ta>
            <ta e="T292" id="Seg_2592" s="T291">di͡e-bit</ta>
            <ta e="T293" id="Seg_2593" s="T292">kaja</ta>
            <ta e="T294" id="Seg_2594" s="T293">dogoː</ta>
            <ta e="T295" id="Seg_2595" s="T294">kim-mit</ta>
            <ta e="T296" id="Seg_2596" s="T295">baːj-a=j</ta>
            <ta e="T297" id="Seg_2597" s="T296">bu</ta>
            <ta e="T298" id="Seg_2598" s="T297">onno</ta>
            <ta e="T299" id="Seg_2599" s="T298">baːj</ta>
            <ta e="T300" id="Seg_2600" s="T299">hohuj-a</ta>
            <ta e="T301" id="Seg_2601" s="T300">hɨh-an</ta>
            <ta e="T302" id="Seg_2602" s="T301">baran</ta>
            <ta e="T303" id="Seg_2603" s="T302">ep-pit</ta>
            <ta e="T304" id="Seg_2604" s="T303">kimi͡ene</ta>
            <ta e="T305" id="Seg_2605" s="T304">bu͡ol-u͡o=j</ta>
            <ta e="T306" id="Seg_2606" s="T305">mi͡ene</ta>
            <ta e="T307" id="Seg_2607" s="T306">bu͡ol-u-muja</ta>
            <ta e="T308" id="Seg_2608" s="T307">Kaːmɨːlaːk</ta>
            <ta e="T309" id="Seg_2609" s="T308">hu͡ok</ta>
            <ta e="T310" id="Seg_2610" s="T309">mi͡ene</ta>
            <ta e="T311" id="Seg_2611" s="T310">itegej-bet</ta>
            <ta e="T312" id="Seg_2612" s="T311">bu͡ol-lak-kɨna</ta>
            <ta e="T313" id="Seg_2613" s="T312">bar-ɨ͡ak</ta>
            <ta e="T314" id="Seg_2614" s="T313">taŋara-ttan</ta>
            <ta e="T315" id="Seg_2615" s="T314">ɨjɨt-a</ta>
            <ta e="T316" id="Seg_2616" s="T315">di͡e-bit</ta>
            <ta e="T317" id="Seg_2617" s="T316">bar-bɨt-tar</ta>
            <ta e="T318" id="Seg_2618" s="T317">kɨrgɨt-tar</ta>
            <ta e="T319" id="Seg_2619" s="T318">hɨt-ar</ta>
            <ta e="T320" id="Seg_2620" s="T319">hir-deri-ger</ta>
            <ta e="T321" id="Seg_2621" s="T320">kel-en</ta>
            <ta e="T322" id="Seg_2622" s="T321">baran</ta>
            <ta e="T323" id="Seg_2623" s="T322">Kaːmɨːlaːk</ta>
            <ta e="T324" id="Seg_2624" s="T323">üːgüleː-bit</ta>
            <ta e="T325" id="Seg_2625" s="T324">bu</ta>
            <ta e="T326" id="Seg_2626" s="T325">kim</ta>
            <ta e="T327" id="Seg_2627" s="T326">baːj-a=j</ta>
            <ta e="T328" id="Seg_2628" s="T327">üːhe-tten</ta>
            <ta e="T329" id="Seg_2629" s="T328">kɨrgɨt-tar</ta>
            <ta e="T330" id="Seg_2630" s="T329">üːgüleː-bit-ter</ta>
            <ta e="T331" id="Seg_2631" s="T330">Kaːmɨːlaːk</ta>
            <ta e="T332" id="Seg_2632" s="T331">baːj-a</ta>
            <ta e="T333" id="Seg_2633" s="T332">Kaːmɨːlaːk</ta>
            <ta e="T334" id="Seg_2634" s="T333">baːj-a</ta>
            <ta e="T335" id="Seg_2635" s="T334">ihit-ti-ŋ</ta>
            <ta e="T336" id="Seg_2636" s="T335">du͡o</ta>
            <ta e="T337" id="Seg_2637" s="T336">di͡e-bit</ta>
            <ta e="T338" id="Seg_2638" s="T337">Kaːmɨːlaːk</ta>
            <ta e="T339" id="Seg_2639" s="T338">kim</ta>
            <ta e="T340" id="Seg_2640" s="T339">baːj-a</ta>
            <ta e="T341" id="Seg_2641" s="T340">e-bit=ij</ta>
            <ta e="T342" id="Seg_2642" s="T341">Kaːmɨːlaːk</ta>
            <ta e="T343" id="Seg_2643" s="T342">iti</ta>
            <ta e="T344" id="Seg_2644" s="T343">baːj-ɨ</ta>
            <ta e="T345" id="Seg_2645" s="T344">bat-an</ta>
            <ta e="T346" id="Seg_2646" s="T345">ɨːp-pɨt</ta>
            <ta e="T347" id="Seg_2647" s="T346">baːj-ɨ-n</ta>
            <ta e="T348" id="Seg_2648" s="T347">baːj</ta>
            <ta e="T349" id="Seg_2649" s="T348">gɨm-mɨt</ta>
            <ta e="T350" id="Seg_2650" s="T349">baj-an-tot-on</ta>
            <ta e="T351" id="Seg_2651" s="T350">olor-but</ta>
            <ta e="T352" id="Seg_2652" s="T351">ele-te</ta>
            <ta e="T353" id="Seg_2653" s="T352">büt-t-e</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_2654" s="T0">Kaːmɨːlaːk</ta>
            <ta e="T2" id="Seg_2655" s="T1">kaːm-An</ta>
            <ta e="T3" id="Seg_2656" s="T2">is-BIT</ta>
            <ta e="T4" id="Seg_2657" s="T3">kaːm-An</ta>
            <ta e="T5" id="Seg_2658" s="T4">is-An-is-An</ta>
            <ta e="T6" id="Seg_2659" s="T5">huːrt-GA</ta>
            <ta e="T7" id="Seg_2660" s="T6">kel-BIT</ta>
            <ta e="T8" id="Seg_2661" s="T7">buːs-GA</ta>
            <ta e="T9" id="Seg_2662" s="T8">toŋ-BIT</ta>
            <ta e="T10" id="Seg_2663" s="T9">kaːn-nI</ta>
            <ta e="T11" id="Seg_2664" s="T10">kör-BIT</ta>
            <ta e="T12" id="Seg_2665" s="T11">taba</ta>
            <ta e="T13" id="Seg_2666" s="T12">kol-tI-n</ta>
            <ta e="T14" id="Seg_2667" s="T13">bul-TI-m</ta>
            <ta e="T15" id="Seg_2668" s="T14">di͡e-A-di͡e-A</ta>
            <ta e="T16" id="Seg_2669" s="T15">huːmka-tI-GAr</ta>
            <ta e="T17" id="Seg_2670" s="T16">ugun-An</ta>
            <ta e="T18" id="Seg_2671" s="T17">keːs-BIT</ta>
            <ta e="T19" id="Seg_2672" s="T18">innʼe</ta>
            <ta e="T20" id="Seg_2673" s="T19">gɨn-An</ta>
            <ta e="T21" id="Seg_2674" s="T20">baran</ta>
            <ta e="T22" id="Seg_2675" s="T21">kaːm-An</ta>
            <ta e="T23" id="Seg_2676" s="T22">is-BIT</ta>
            <ta e="T24" id="Seg_2677" s="T23">is-An-is-An</ta>
            <ta e="T25" id="Seg_2678" s="T24">ɨ͡al-LAr-GA</ta>
            <ta e="T26" id="Seg_2679" s="T25">kel-BIT</ta>
            <ta e="T27" id="Seg_2680" s="T26">ɨ͡al-LAr-GA</ta>
            <ta e="T28" id="Seg_2681" s="T27">kü͡östeː-AːrI</ta>
            <ta e="T29" id="Seg_2682" s="T28">et-LArI-n</ta>
            <ta e="T30" id="Seg_2683" s="T29">et-LAː-BIT-LAr</ta>
            <ta e="T31" id="Seg_2684" s="T30">kaja</ta>
            <ta e="T32" id="Seg_2685" s="T31">ɨ͡aldʼɨt-BItI-GAr</ta>
            <ta e="T33" id="Seg_2686" s="T32">et</ta>
            <ta e="T34" id="Seg_2687" s="T33">tij-BAT</ta>
            <ta e="T35" id="Seg_2688" s="T34">e-BIT</ta>
            <ta e="T36" id="Seg_2689" s="T35">di͡e-A-s-BIT-LAr</ta>
            <ta e="T37" id="Seg_2690" s="T36">ol-GA</ta>
            <ta e="T38" id="Seg_2691" s="T37">Kaːmɨːlaːk</ta>
            <ta e="T39" id="Seg_2692" s="T38">di͡e-BIT</ta>
            <ta e="T40" id="Seg_2693" s="T39">kaja</ta>
            <ta e="T41" id="Seg_2694" s="T40">tu͡ok</ta>
            <ta e="T42" id="Seg_2695" s="T41">bu͡ol-A-GIt</ta>
            <ta e="T43" id="Seg_2696" s="T42">min</ta>
            <ta e="T44" id="Seg_2697" s="T43">huːmka-BA-r</ta>
            <ta e="T45" id="Seg_2698" s="T44">et</ta>
            <ta e="T46" id="Seg_2699" s="T45">baːr</ta>
            <ta e="T47" id="Seg_2700" s="T46">e-TI-tA</ta>
            <ta e="T48" id="Seg_2701" s="T47">ol-nI</ta>
            <ta e="T49" id="Seg_2702" s="T48">killer-An</ta>
            <ta e="T50" id="Seg_2703" s="T49">kü͡östeː-ŋ</ta>
            <ta e="T51" id="Seg_2704" s="T50">dʼe</ta>
            <ta e="T52" id="Seg_2705" s="T51">onton</ta>
            <ta e="T53" id="Seg_2706" s="T52">ɨ͡al-LAr</ta>
            <ta e="T54" id="Seg_2707" s="T53">kü͡ös-LArI-GAr</ta>
            <ta e="T55" id="Seg_2708" s="T54">Kaːmɨːlaːk</ta>
            <ta e="T56" id="Seg_2709" s="T55">et-tI-n</ta>
            <ta e="T57" id="Seg_2710" s="T56">uk-An</ta>
            <ta e="T58" id="Seg_2711" s="T57">keːs-BIT-LAr</ta>
            <ta e="T59" id="Seg_2712" s="T58">ki͡ehe-nAn</ta>
            <ta e="T60" id="Seg_2713" s="T59">dʼe</ta>
            <ta e="T61" id="Seg_2714" s="T60">et-LArI-n</ta>
            <ta e="T62" id="Seg_2715" s="T61">kotor-Ar-LAr</ta>
            <ta e="T63" id="Seg_2716" s="T62">kotor-BIT-LArI-GAr</ta>
            <ta e="T64" id="Seg_2717" s="T63">ɨ͡aldʼɨt-LArI-GAr</ta>
            <ta e="T65" id="Seg_2718" s="T64">et-LArA</ta>
            <ta e="T66" id="Seg_2719" s="T65">emi͡e</ta>
            <ta e="T67" id="Seg_2720" s="T66">tij-BAtAK</ta>
            <ta e="T68" id="Seg_2721" s="T67">Kaːmɨːlaːk</ta>
            <ta e="T69" id="Seg_2722" s="T68">kaja</ta>
            <ta e="T70" id="Seg_2723" s="T69">kanna=Ij</ta>
            <ta e="T71" id="Seg_2724" s="T70">min</ta>
            <ta e="T72" id="Seg_2725" s="T71">et-I-m</ta>
            <ta e="T73" id="Seg_2726" s="T72">di͡e-BIT</ta>
            <ta e="T74" id="Seg_2727" s="T73">öl-TI-m-öl-TI-m</ta>
            <ta e="T75" id="Seg_2728" s="T74">kajdak</ta>
            <ta e="T76" id="Seg_2729" s="T75">ahaː-BAkkA</ta>
            <ta e="T77" id="Seg_2730" s="T76">utuj-IAK-m=Ij</ta>
            <ta e="T78" id="Seg_2731" s="T77">kajaː</ta>
            <ta e="T79" id="Seg_2732" s="T78">öl-I-m</ta>
            <ta e="T80" id="Seg_2733" s="T79">agaj</ta>
            <ta e="T81" id="Seg_2734" s="T80">bihigi</ta>
            <ta e="T82" id="Seg_2735" s="T81">en-GA</ta>
            <ta e="T83" id="Seg_2736" s="T82">bütün</ta>
            <ta e="T84" id="Seg_2737" s="T83">tugut-TA</ta>
            <ta e="T85" id="Seg_2738" s="T84">bi͡er-A-BIt</ta>
            <ta e="T86" id="Seg_2739" s="T85">di͡e-BIT-LAr</ta>
            <ta e="T87" id="Seg_2740" s="T86">ɨ͡al-LAr</ta>
            <ta e="T88" id="Seg_2741" s="T87">ahaː-An</ta>
            <ta e="T89" id="Seg_2742" s="T88">baran</ta>
            <ta e="T90" id="Seg_2743" s="T89">utuj-BIT-LAr</ta>
            <ta e="T91" id="Seg_2744" s="T90">tugut-LArI-n</ta>
            <ta e="T92" id="Seg_2745" s="T91">kör-TAr-BIT-LAr</ta>
            <ta e="T93" id="Seg_2746" s="T92">Kaːmɨːlaːk-GA</ta>
            <ta e="T94" id="Seg_2747" s="T93">Kaːmɨːlaːk</ta>
            <ta e="T95" id="Seg_2748" s="T94">tüːn</ta>
            <ta e="T96" id="Seg_2749" s="T95">tagɨs-An</ta>
            <ta e="T97" id="Seg_2750" s="T96">tugut-tA</ta>
            <ta e="T98" id="Seg_2751" s="T97">itte</ta>
            <ta e="T99" id="Seg_2752" s="T98">kel-BIT-tI-n</ta>
            <ta e="T100" id="Seg_2753" s="T99">tut-An</ta>
            <ta e="T101" id="Seg_2754" s="T100">ɨl-BIT</ta>
            <ta e="T102" id="Seg_2755" s="T101">innʼe</ta>
            <ta e="T103" id="Seg_2756" s="T102">gɨn-An</ta>
            <ta e="T104" id="Seg_2757" s="T103">baran</ta>
            <ta e="T105" id="Seg_2758" s="T104">ikki</ta>
            <ta e="T106" id="Seg_2759" s="T105">bastɨŋ</ta>
            <ta e="T107" id="Seg_2760" s="T106">atɨːr</ta>
            <ta e="T108" id="Seg_2761" s="T107">mu͡os-LArI-GAr</ta>
            <ta e="T109" id="Seg_2762" s="T108">iːl-An</ta>
            <ta e="T110" id="Seg_2763" s="T109">keːs-BIT</ta>
            <ta e="T111" id="Seg_2764" s="T110">kiːr-An</ta>
            <ta e="T112" id="Seg_2765" s="T111">utuj-An</ta>
            <ta e="T113" id="Seg_2766" s="T112">kaːl-BIT</ta>
            <ta e="T114" id="Seg_2767" s="T113">harsi͡erda</ta>
            <ta e="T115" id="Seg_2768" s="T114">ɨ͡al-LAr</ta>
            <ta e="T116" id="Seg_2769" s="T115">kör-BIT-LArA</ta>
            <ta e="T117" id="Seg_2770" s="T116">Kaːmɨːlaːk</ta>
            <ta e="T118" id="Seg_2771" s="T117">tugut-tI-n</ta>
            <ta e="T119" id="Seg_2772" s="T118">ikki</ta>
            <ta e="T120" id="Seg_2773" s="T119">bastɨŋ</ta>
            <ta e="T121" id="Seg_2774" s="T120">atɨːr-LArA</ta>
            <ta e="T122" id="Seg_2775" s="T121">mu͡os-LArI-GAr</ta>
            <ta e="T123" id="Seg_2776" s="T122">iːl-A</ta>
            <ta e="T124" id="Seg_2777" s="T123">hɨrɨt-Ar-LAr</ta>
            <ta e="T125" id="Seg_2778" s="T124">Kaːmɨːlaːk</ta>
            <ta e="T126" id="Seg_2779" s="T125">ol-nI</ta>
            <ta e="T127" id="Seg_2780" s="T126">ihit-TI-tA</ta>
            <ta e="T128" id="Seg_2781" s="T127">daːganɨ</ta>
            <ta e="T129" id="Seg_2782" s="T128">dʼe</ta>
            <ta e="T130" id="Seg_2783" s="T129">ile</ta>
            <ta e="T131" id="Seg_2784" s="T130">öl-A-BIn</ta>
            <ta e="T132" id="Seg_2785" s="T131">e-BIT</ta>
            <ta e="T133" id="Seg_2786" s="T132">tugut-tA</ta>
            <ta e="T134" id="Seg_2787" s="T133">hu͡ok</ta>
            <ta e="T135" id="Seg_2788" s="T134">kaːl-TI-m</ta>
            <ta e="T136" id="Seg_2789" s="T135">di͡e-An</ta>
            <ta e="T137" id="Seg_2790" s="T136">ü͡ögüleː-BIT</ta>
            <ta e="T138" id="Seg_2791" s="T137">kaja</ta>
            <ta e="T139" id="Seg_2792" s="T138">öl-I-m</ta>
            <ta e="T140" id="Seg_2793" s="T139">agaj</ta>
            <ta e="T141" id="Seg_2794" s="T140">bihigi</ta>
            <ta e="T142" id="Seg_2795" s="T141">en-GA</ta>
            <ta e="T143" id="Seg_2796" s="T142">ol</ta>
            <ta e="T144" id="Seg_2797" s="T143">ikki</ta>
            <ta e="T145" id="Seg_2798" s="T144">atɨːr-nI</ta>
            <ta e="T146" id="Seg_2799" s="T145">bi͡er-IAK-BIt</ta>
            <ta e="T147" id="Seg_2800" s="T146">di͡e-BIT-LAr</ta>
            <ta e="T148" id="Seg_2801" s="T147">dʼi͡e-LAːgI-LArA</ta>
            <ta e="T149" id="Seg_2802" s="T148">Kaːmɨːlaːk</ta>
            <ta e="T150" id="Seg_2803" s="T149">ikki</ta>
            <ta e="T151" id="Seg_2804" s="T150">atɨːr-LAN-BIT</ta>
            <ta e="T152" id="Seg_2805" s="T151">ikki</ta>
            <ta e="T153" id="Seg_2806" s="T152">atɨːr-tI-n</ta>
            <ta e="T154" id="Seg_2807" s="T153">kölün-An</ta>
            <ta e="T155" id="Seg_2808" s="T154">baran</ta>
            <ta e="T156" id="Seg_2809" s="T155">Kaːmɨːlaːk</ta>
            <ta e="T157" id="Seg_2810" s="T156">hɨrga-LAN-A</ta>
            <ta e="T158" id="Seg_2811" s="T157">bar-BIT</ta>
            <ta e="T159" id="Seg_2812" s="T158">is-An-is-An</ta>
            <ta e="T160" id="Seg_2813" s="T159">oŋu͡ok-LAr-GA</ta>
            <ta e="T161" id="Seg_2814" s="T160">kep-BIT</ta>
            <ta e="T162" id="Seg_2815" s="T161">onno</ta>
            <ta e="T163" id="Seg_2816" s="T162">biːr</ta>
            <ta e="T164" id="Seg_2817" s="T163">emeːksin-nI</ta>
            <ta e="T165" id="Seg_2818" s="T164">kolbo-tI-n</ta>
            <ta e="T166" id="Seg_2819" s="T165">arɨj-An</ta>
            <ta e="T167" id="Seg_2820" s="T166">kostoː-An</ta>
            <ta e="T168" id="Seg_2821" s="T167">ɨl-BIT</ta>
            <ta e="T169" id="Seg_2822" s="T168">hɨrga-tI-GAr</ta>
            <ta e="T170" id="Seg_2823" s="T169">uːr-An</ta>
            <ta e="T171" id="Seg_2824" s="T170">keːs-BIT</ta>
            <ta e="T172" id="Seg_2825" s="T171">bar-An</ta>
            <ta e="T173" id="Seg_2826" s="T172">is-BIT</ta>
            <ta e="T174" id="Seg_2827" s="T173">is-An-is-An</ta>
            <ta e="T175" id="Seg_2828" s="T174">biːr</ta>
            <ta e="T176" id="Seg_2829" s="T175">ɨ͡al-GA</ta>
            <ta e="T177" id="Seg_2830" s="T176">tij-BIT</ta>
            <ta e="T178" id="Seg_2831" s="T177">hɨrga-tI-n</ta>
            <ta e="T179" id="Seg_2832" s="T178">ɨ͡al-LAr-ttAn</ta>
            <ta e="T180" id="Seg_2833" s="T179">ɨraːk-kAːN</ta>
            <ta e="T181" id="Seg_2834" s="T180">keːs-BIT</ta>
            <ta e="T182" id="Seg_2835" s="T181">Kaːmɨːlaːk</ta>
            <ta e="T183" id="Seg_2836" s="T182">tahaːra</ta>
            <ta e="T184" id="Seg_2837" s="T183">ogo-LAr-nI</ta>
            <ta e="T185" id="Seg_2838" s="T184">körüs-I-BIT</ta>
            <ta e="T186" id="Seg_2839" s="T185">ogo-LAr</ta>
            <ta e="T187" id="Seg_2840" s="T186">min</ta>
            <ta e="T188" id="Seg_2841" s="T187">hɨrga-BA-r</ta>
            <ta e="T189" id="Seg_2842" s="T188">čugahaː-AːjA-GIt</ta>
            <ta e="T190" id="Seg_2843" s="T189">hɨrga-BA-r</ta>
            <ta e="T191" id="Seg_2844" s="T190">emeːksin-I-m</ta>
            <ta e="T192" id="Seg_2845" s="T191">baːr</ta>
            <ta e="T193" id="Seg_2846" s="T192">kihi</ta>
            <ta e="T194" id="Seg_2847" s="T193">čugahaː-TI-tA</ta>
            <ta e="T195" id="Seg_2848" s="T194">da</ta>
            <ta e="T196" id="Seg_2849" s="T195">hohuj-An</ta>
            <ta e="T197" id="Seg_2850" s="T196">öl-An</ta>
            <ta e="T198" id="Seg_2851" s="T197">kaːl-IAK-tA</ta>
            <ta e="T199" id="Seg_2852" s="T198">di͡e-BIT</ta>
            <ta e="T200" id="Seg_2853" s="T199">Kaːmɨːlaːk</ta>
            <ta e="T201" id="Seg_2854" s="T200">čaːj</ta>
            <ta e="T202" id="Seg_2855" s="T201">is-A</ta>
            <ta e="T203" id="Seg_2856" s="T202">olor-TAK-InA</ta>
            <ta e="T204" id="Seg_2857" s="T203">ogo-LAr</ta>
            <ta e="T205" id="Seg_2858" s="T204">hüːr-An</ta>
            <ta e="T206" id="Seg_2859" s="T205">kiːr-BIT-LAr</ta>
            <ta e="T207" id="Seg_2860" s="T206">Kaːmɨːlaːk</ta>
            <ta e="T208" id="Seg_2861" s="T207">emeːksin-I-ŋ</ta>
            <ta e="T209" id="Seg_2862" s="T208">öl-An</ta>
            <ta e="T210" id="Seg_2863" s="T209">kaːl-BIT</ta>
            <ta e="T211" id="Seg_2864" s="T210">di͡e-BIT-LAr</ta>
            <ta e="T212" id="Seg_2865" s="T211">araː</ta>
            <ta e="T213" id="Seg_2866" s="T212">anɨ</ta>
            <ta e="T214" id="Seg_2867" s="T213">bagas</ta>
            <ta e="T215" id="Seg_2868" s="T214">ile</ta>
            <ta e="T216" id="Seg_2869" s="T215">öl-A-BIn</ta>
            <ta e="T217" id="Seg_2870" s="T216">e-BIT</ta>
            <ta e="T218" id="Seg_2871" s="T217">emeːksin-tA</ta>
            <ta e="T219" id="Seg_2872" s="T218">hu͡ok</ta>
            <ta e="T220" id="Seg_2873" s="T219">kaːl-TI-m</ta>
            <ta e="T221" id="Seg_2874" s="T220">iti</ta>
            <ta e="T222" id="Seg_2875" s="T221">ogo-LAr</ta>
            <ta e="T223" id="Seg_2876" s="T222">hɨrga-GA</ta>
            <ta e="T224" id="Seg_2877" s="T223">čugahaː-An-LAr</ta>
            <ta e="T225" id="Seg_2878" s="T224">Kaːmɨːlaːk</ta>
            <ta e="T226" id="Seg_2879" s="T225">ü͡ögüleː-BIT</ta>
            <ta e="T227" id="Seg_2880" s="T226">togo</ta>
            <ta e="T228" id="Seg_2881" s="T227">öl-AːrI</ta>
            <ta e="T229" id="Seg_2882" s="T228">gɨn-A-GIn</ta>
            <ta e="T230" id="Seg_2883" s="T229">kaja</ta>
            <ta e="T231" id="Seg_2884" s="T230">ɨl</ta>
            <ta e="T232" id="Seg_2885" s="T231">iti</ta>
            <ta e="T233" id="Seg_2886" s="T232">ikki</ta>
            <ta e="T234" id="Seg_2887" s="T233">kɨːs-BItI-n</ta>
            <ta e="T235" id="Seg_2888" s="T234">di͡e-A-s-BIT-LAr</ta>
            <ta e="T236" id="Seg_2889" s="T235">dʼi͡e-LAːK-LAr</ta>
            <ta e="T237" id="Seg_2890" s="T236">Kaːmɨːlaːk</ta>
            <ta e="T238" id="Seg_2891" s="T237">onon</ta>
            <ta e="T239" id="Seg_2892" s="T238">ikki</ta>
            <ta e="T240" id="Seg_2893" s="T239">kɨːs-LAN-BIT</ta>
            <ta e="T241" id="Seg_2894" s="T240">kɨːs-LAr-tI-n</ta>
            <ta e="T242" id="Seg_2895" s="T241">ti͡ej-An</ta>
            <ta e="T243" id="Seg_2896" s="T242">baran</ta>
            <ta e="T244" id="Seg_2897" s="T243">Kaːmɨːlaːk</ta>
            <ta e="T245" id="Seg_2898" s="T244">bar-An</ta>
            <ta e="T246" id="Seg_2899" s="T245">is-BIT</ta>
            <ta e="T247" id="Seg_2900" s="T246">dʼe</ta>
            <ta e="T248" id="Seg_2901" s="T247">bar-An</ta>
            <ta e="T249" id="Seg_2902" s="T248">is-An-is-An</ta>
            <ta e="T250" id="Seg_2903" s="T249">biːr</ta>
            <ta e="T251" id="Seg_2904" s="T250">baːj</ta>
            <ta e="T252" id="Seg_2905" s="T251">kirili͡es-tI-n</ta>
            <ta e="T253" id="Seg_2906" s="T252">kör-BIT</ta>
            <ta e="T254" id="Seg_2907" s="T253">ol-nI</ta>
            <ta e="T255" id="Seg_2908" s="T254">kör-An</ta>
            <ta e="T256" id="Seg_2909" s="T255">Kaːmɨːlaːk</ta>
            <ta e="T257" id="Seg_2910" s="T256">kɨːs-LAr-tI-GAr</ta>
            <ta e="T258" id="Seg_2911" s="T257">haŋar-BIT</ta>
            <ta e="T259" id="Seg_2912" s="T258">kaja</ta>
            <ta e="T260" id="Seg_2913" s="T259">kɨːs-LAr</ta>
            <ta e="T261" id="Seg_2914" s="T260">bar-I-ŋ</ta>
            <ta e="T262" id="Seg_2915" s="T261">tagɨs-I-ŋ</ta>
            <ta e="T263" id="Seg_2916" s="T262">tiːt</ta>
            <ta e="T264" id="Seg_2917" s="T263">mas-GA</ta>
            <ta e="T265" id="Seg_2918" s="T264">kisten-An</ta>
            <ta e="T266" id="Seg_2919" s="T265">olor-I-ŋ</ta>
            <ta e="T267" id="Seg_2920" s="T266">min</ta>
            <ta e="T268" id="Seg_2921" s="T267">baːj-GA</ta>
            <ta e="T269" id="Seg_2922" s="T268">bar-IAK-m</ta>
            <ta e="T270" id="Seg_2923" s="T269">gini-nI</ta>
            <ta e="T271" id="Seg_2924" s="T270">manna</ta>
            <ta e="T272" id="Seg_2925" s="T271">egel-IAK-m</ta>
            <ta e="T273" id="Seg_2926" s="T272">baːj</ta>
            <ta e="T274" id="Seg_2927" s="T273">kimi͡ene</ta>
            <ta e="T275" id="Seg_2928" s="T274">baːj-tA=Ij</ta>
            <ta e="T276" id="Seg_2929" s="T275">di͡e-A-di͡e-A</ta>
            <ta e="T277" id="Seg_2930" s="T276">ɨjɨt-IAK.[tA]</ta>
            <ta e="T278" id="Seg_2931" s="T277">ol-GA</ta>
            <ta e="T279" id="Seg_2932" s="T278">ehigi</ta>
            <ta e="T280" id="Seg_2933" s="T279">di͡e-Aːr-I-ŋ</ta>
            <ta e="T281" id="Seg_2934" s="T280">Kaːmɨːlaːk</ta>
            <ta e="T282" id="Seg_2935" s="T281">baːj-tA</ta>
            <ta e="T283" id="Seg_2936" s="T282">ol-nI</ta>
            <ta e="T284" id="Seg_2937" s="T283">haŋar-An</ta>
            <ta e="T285" id="Seg_2938" s="T284">baran</ta>
            <ta e="T286" id="Seg_2939" s="T285">Kaːmɨːlaːk</ta>
            <ta e="T287" id="Seg_2940" s="T286">baːj-GA</ta>
            <ta e="T288" id="Seg_2941" s="T287">tij-BIT</ta>
            <ta e="T289" id="Seg_2942" s="T288">čaːj</ta>
            <ta e="T290" id="Seg_2943" s="T289">is-An</ta>
            <ta e="T291" id="Seg_2944" s="T290">büt-An</ta>
            <ta e="T292" id="Seg_2945" s="T291">di͡e-BIT</ta>
            <ta e="T293" id="Seg_2946" s="T292">kaja</ta>
            <ta e="T294" id="Seg_2947" s="T293">dogor</ta>
            <ta e="T295" id="Seg_2948" s="T294">kim-BIt</ta>
            <ta e="T296" id="Seg_2949" s="T295">baːj-tA=Ij</ta>
            <ta e="T297" id="Seg_2950" s="T296">bu</ta>
            <ta e="T298" id="Seg_2951" s="T297">onno</ta>
            <ta e="T299" id="Seg_2952" s="T298">baːj</ta>
            <ta e="T300" id="Seg_2953" s="T299">hohuj-A</ta>
            <ta e="T301" id="Seg_2954" s="T300">hɨs-An</ta>
            <ta e="T302" id="Seg_2955" s="T301">baran</ta>
            <ta e="T303" id="Seg_2956" s="T302">et-BIT</ta>
            <ta e="T304" id="Seg_2957" s="T303">kimi͡ene</ta>
            <ta e="T305" id="Seg_2958" s="T304">bu͡ol-IAK.[tA]=Ij</ta>
            <ta e="T306" id="Seg_2959" s="T305">mini͡ene</ta>
            <ta e="T307" id="Seg_2960" s="T306">bu͡ol-I-mInA</ta>
            <ta e="T308" id="Seg_2961" s="T307">Kaːmɨːlaːk</ta>
            <ta e="T309" id="Seg_2962" s="T308">hu͡ok</ta>
            <ta e="T310" id="Seg_2963" s="T309">mini͡ene</ta>
            <ta e="T311" id="Seg_2964" s="T310">itegej-BAT</ta>
            <ta e="T312" id="Seg_2965" s="T311">bu͡ol-TAK-GInA</ta>
            <ta e="T313" id="Seg_2966" s="T312">bar-IAk</ta>
            <ta e="T314" id="Seg_2967" s="T313">taŋara-ttAn</ta>
            <ta e="T315" id="Seg_2968" s="T314">ɨjɨt-A</ta>
            <ta e="T316" id="Seg_2969" s="T315">di͡e-BIT</ta>
            <ta e="T317" id="Seg_2970" s="T316">bar-BIT-LAr</ta>
            <ta e="T318" id="Seg_2971" s="T317">kɨːs-LAr</ta>
            <ta e="T319" id="Seg_2972" s="T318">hɨt-Ar</ta>
            <ta e="T320" id="Seg_2973" s="T319">hir-LArI-GAr</ta>
            <ta e="T321" id="Seg_2974" s="T320">kel-An</ta>
            <ta e="T322" id="Seg_2975" s="T321">baran</ta>
            <ta e="T323" id="Seg_2976" s="T322">Kaːmɨːlaːk</ta>
            <ta e="T324" id="Seg_2977" s="T323">ü͡ögüleː-BIT</ta>
            <ta e="T325" id="Seg_2978" s="T324">bu</ta>
            <ta e="T326" id="Seg_2979" s="T325">kim</ta>
            <ta e="T327" id="Seg_2980" s="T326">baːj-tA=Ij</ta>
            <ta e="T328" id="Seg_2981" s="T327">üːhe-ttAn</ta>
            <ta e="T329" id="Seg_2982" s="T328">kɨːs-LAr</ta>
            <ta e="T330" id="Seg_2983" s="T329">ü͡ögüleː-BIT-LAr</ta>
            <ta e="T331" id="Seg_2984" s="T330">Kaːmɨːlaːk</ta>
            <ta e="T332" id="Seg_2985" s="T331">baːj-tA</ta>
            <ta e="T333" id="Seg_2986" s="T332">Kaːmɨːlaːk</ta>
            <ta e="T334" id="Seg_2987" s="T333">baːj-tA</ta>
            <ta e="T335" id="Seg_2988" s="T334">ihit-TI-ŋ</ta>
            <ta e="T336" id="Seg_2989" s="T335">du͡o</ta>
            <ta e="T337" id="Seg_2990" s="T336">di͡e-BIT</ta>
            <ta e="T338" id="Seg_2991" s="T337">Kaːmɨːlaːk</ta>
            <ta e="T339" id="Seg_2992" s="T338">kim</ta>
            <ta e="T340" id="Seg_2993" s="T339">baːj-tA</ta>
            <ta e="T341" id="Seg_2994" s="T340">e-BIT=Ij</ta>
            <ta e="T342" id="Seg_2995" s="T341">Kaːmɨːlaːk</ta>
            <ta e="T343" id="Seg_2996" s="T342">iti</ta>
            <ta e="T344" id="Seg_2997" s="T343">baːj-nI</ta>
            <ta e="T345" id="Seg_2998" s="T344">bat-An</ta>
            <ta e="T346" id="Seg_2999" s="T345">ɨːt-BIT</ta>
            <ta e="T347" id="Seg_3000" s="T346">baːj-tI-n</ta>
            <ta e="T348" id="Seg_3001" s="T347">baːj</ta>
            <ta e="T349" id="Seg_3002" s="T348">gɨn-BIT</ta>
            <ta e="T350" id="Seg_3003" s="T349">baːj-An-tot-An</ta>
            <ta e="T351" id="Seg_3004" s="T350">olor-BIT</ta>
            <ta e="T352" id="Seg_3005" s="T351">ele-tA</ta>
            <ta e="T353" id="Seg_3006" s="T352">büt-TI-tA</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_3007" s="T0">Kaamyylaak.[NOM]</ta>
            <ta e="T2" id="Seg_3008" s="T1">walk-CVB.SEQ</ta>
            <ta e="T3" id="Seg_3009" s="T2">go-PST2.[3SG]</ta>
            <ta e="T4" id="Seg_3010" s="T3">walk-CVB.SEQ</ta>
            <ta e="T5" id="Seg_3011" s="T4">go-CVB.SEQ-go-CVB.SEQ</ta>
            <ta e="T6" id="Seg_3012" s="T5">temporary.settlement-DAT/LOC</ta>
            <ta e="T7" id="Seg_3013" s="T6">come-PST2.[3SG]</ta>
            <ta e="T8" id="Seg_3014" s="T7">ice-DAT/LOC</ta>
            <ta e="T9" id="Seg_3015" s="T8">freeze-PTCP.PST</ta>
            <ta e="T10" id="Seg_3016" s="T9">blood-ACC</ta>
            <ta e="T11" id="Seg_3017" s="T10">see-PST2.[3SG]</ta>
            <ta e="T12" id="Seg_3018" s="T11">reindeer.[NOM]</ta>
            <ta e="T13" id="Seg_3019" s="T12">foreleg-3SG-ACC</ta>
            <ta e="T14" id="Seg_3020" s="T13">find-PST1-1SG</ta>
            <ta e="T15" id="Seg_3021" s="T14">say-CVB.SIM-say-CVB.SIM</ta>
            <ta e="T16" id="Seg_3022" s="T15">pocket-3SG-DAT/LOC</ta>
            <ta e="T17" id="Seg_3023" s="T16">put.in-CVB.SEQ</ta>
            <ta e="T18" id="Seg_3024" s="T17">throw-PST2.[3SG]</ta>
            <ta e="T19" id="Seg_3025" s="T18">so</ta>
            <ta e="T20" id="Seg_3026" s="T19">make-CVB.SEQ</ta>
            <ta e="T21" id="Seg_3027" s="T20">after</ta>
            <ta e="T22" id="Seg_3028" s="T21">walk-CVB.SEQ</ta>
            <ta e="T23" id="Seg_3029" s="T22">go-PST2.[3SG]</ta>
            <ta e="T24" id="Seg_3030" s="T23">go-CVB.SEQ-go-CVB.SEQ</ta>
            <ta e="T25" id="Seg_3031" s="T24">inhabitant-PL-DAT/LOC</ta>
            <ta e="T26" id="Seg_3032" s="T25">come-PST2.[3SG]</ta>
            <ta e="T27" id="Seg_3033" s="T26">inhabitant-PL-DAT/LOC</ta>
            <ta e="T28" id="Seg_3034" s="T27">cook-CVB.PURP</ta>
            <ta e="T29" id="Seg_3035" s="T28">meat-3PL-ACC</ta>
            <ta e="T30" id="Seg_3036" s="T29">meat-VBZ-PST2-3PL</ta>
            <ta e="T31" id="Seg_3037" s="T30">well</ta>
            <ta e="T32" id="Seg_3038" s="T31">guest-1PL-DAT/LOC</ta>
            <ta e="T33" id="Seg_3039" s="T32">meat.[NOM]</ta>
            <ta e="T34" id="Seg_3040" s="T33">be.enough-NEG.PTCP</ta>
            <ta e="T35" id="Seg_3041" s="T34">be-PST2.[3SG]</ta>
            <ta e="T36" id="Seg_3042" s="T35">say-EP-RECP/COLL-PST2-3PL</ta>
            <ta e="T37" id="Seg_3043" s="T36">that-DAT/LOC</ta>
            <ta e="T38" id="Seg_3044" s="T37">Kaamyylaak.[NOM]</ta>
            <ta e="T39" id="Seg_3045" s="T38">say-PST2.[3SG]</ta>
            <ta e="T40" id="Seg_3046" s="T39">well</ta>
            <ta e="T41" id="Seg_3047" s="T40">what.[NOM]</ta>
            <ta e="T42" id="Seg_3048" s="T41">be-PRS-2PL</ta>
            <ta e="T43" id="Seg_3049" s="T42">1SG.[NOM]</ta>
            <ta e="T44" id="Seg_3050" s="T43">pocket-1SG-DAT/LOC</ta>
            <ta e="T45" id="Seg_3051" s="T44">meat.[NOM]</ta>
            <ta e="T46" id="Seg_3052" s="T45">there.is</ta>
            <ta e="T47" id="Seg_3053" s="T46">be-PST1-3SG</ta>
            <ta e="T48" id="Seg_3054" s="T47">that-ACC</ta>
            <ta e="T49" id="Seg_3055" s="T48">bring.in-CVB.SEQ</ta>
            <ta e="T50" id="Seg_3056" s="T49">cook-IMP.2PL</ta>
            <ta e="T51" id="Seg_3057" s="T50">well</ta>
            <ta e="T52" id="Seg_3058" s="T51">then</ta>
            <ta e="T53" id="Seg_3059" s="T52">inhabitant-PL.[NOM]</ta>
            <ta e="T54" id="Seg_3060" s="T53">kettle-3PL-DAT/LOC</ta>
            <ta e="T55" id="Seg_3061" s="T54">Kaamyylaak.[NOM]</ta>
            <ta e="T56" id="Seg_3062" s="T55">meat-3SG-ACC</ta>
            <ta e="T57" id="Seg_3063" s="T56">stick-CVB.SEQ</ta>
            <ta e="T58" id="Seg_3064" s="T57">throw-PST2-3PL</ta>
            <ta e="T59" id="Seg_3065" s="T58">evening-INSTR</ta>
            <ta e="T60" id="Seg_3066" s="T59">well</ta>
            <ta e="T61" id="Seg_3067" s="T60">meat-3PL-ACC</ta>
            <ta e="T62" id="Seg_3068" s="T61">take.out-PRS-3PL</ta>
            <ta e="T63" id="Seg_3069" s="T62">take.out-PTCP.PST-3PL-DAT/LOC</ta>
            <ta e="T64" id="Seg_3070" s="T63">guest-3PL-DAT/LOC</ta>
            <ta e="T65" id="Seg_3071" s="T64">meat-3PL.[NOM]</ta>
            <ta e="T66" id="Seg_3072" s="T65">again</ta>
            <ta e="T67" id="Seg_3073" s="T66">be.enough-PST2.NEG.[3SG]</ta>
            <ta e="T68" id="Seg_3074" s="T67">Kaamyylaak.[NOM]</ta>
            <ta e="T69" id="Seg_3075" s="T68">well</ta>
            <ta e="T70" id="Seg_3076" s="T69">where=Q</ta>
            <ta e="T71" id="Seg_3077" s="T70">1SG.[NOM]</ta>
            <ta e="T72" id="Seg_3078" s="T71">meat-EP-1SG.[NOM]</ta>
            <ta e="T73" id="Seg_3079" s="T72">say-PST2.[3SG]</ta>
            <ta e="T74" id="Seg_3080" s="T73">die-PST1-1SG-die-PST1-1SG</ta>
            <ta e="T75" id="Seg_3081" s="T74">how</ta>
            <ta e="T76" id="Seg_3082" s="T75">eat-NEG.CVB.SIM</ta>
            <ta e="T77" id="Seg_3083" s="T76">fall.asleep-FUT-1SG=Q</ta>
            <ta e="T78" id="Seg_3084" s="T77">hey</ta>
            <ta e="T79" id="Seg_3085" s="T78">die-EP-NEG.[IMP.2SG]</ta>
            <ta e="T80" id="Seg_3086" s="T79">only</ta>
            <ta e="T81" id="Seg_3087" s="T80">1PL.[NOM]</ta>
            <ta e="T82" id="Seg_3088" s="T81">2SG-DAT/LOC</ta>
            <ta e="T83" id="Seg_3089" s="T82">intact</ta>
            <ta e="T84" id="Seg_3090" s="T83">calf-PART</ta>
            <ta e="T85" id="Seg_3091" s="T84">give-PRS-1PL</ta>
            <ta e="T86" id="Seg_3092" s="T85">say-PST2-3PL</ta>
            <ta e="T87" id="Seg_3093" s="T86">inhabitant-PL.[NOM]</ta>
            <ta e="T88" id="Seg_3094" s="T87">eat-CVB.SEQ</ta>
            <ta e="T89" id="Seg_3095" s="T88">after</ta>
            <ta e="T90" id="Seg_3096" s="T89">fall.asleep-PST2-3PL</ta>
            <ta e="T91" id="Seg_3097" s="T90">reindeer.calf-3PL-ACC</ta>
            <ta e="T92" id="Seg_3098" s="T91">see-CAUS-PST2-3PL</ta>
            <ta e="T93" id="Seg_3099" s="T92">Kaamyylaak-DAT/LOC</ta>
            <ta e="T94" id="Seg_3100" s="T93">Kaamyylaak.[NOM]</ta>
            <ta e="T95" id="Seg_3101" s="T94">at.night</ta>
            <ta e="T96" id="Seg_3102" s="T95">go.out-CVB.SEQ</ta>
            <ta e="T97" id="Seg_3103" s="T96">reindeer.calf-3SG.[NOM]</ta>
            <ta e="T98" id="Seg_3104" s="T97">EMPH</ta>
            <ta e="T99" id="Seg_3105" s="T98">come-PTCP.PST-3SG-ACC</ta>
            <ta e="T100" id="Seg_3106" s="T99">grab-CVB.SEQ</ta>
            <ta e="T101" id="Seg_3107" s="T100">take-PST2.[3SG]</ta>
            <ta e="T102" id="Seg_3108" s="T101">so</ta>
            <ta e="T103" id="Seg_3109" s="T102">make-CVB.SEQ</ta>
            <ta e="T104" id="Seg_3110" s="T103">after</ta>
            <ta e="T105" id="Seg_3111" s="T104">two</ta>
            <ta e="T106" id="Seg_3112" s="T105">best</ta>
            <ta e="T107" id="Seg_3113" s="T106">reindeer.bull.[NOM]</ta>
            <ta e="T108" id="Seg_3114" s="T107">horn-3PL-DAT/LOC</ta>
            <ta e="T109" id="Seg_3115" s="T108">hang.up-CVB.SEQ</ta>
            <ta e="T110" id="Seg_3116" s="T109">throw-PST2.[3SG]</ta>
            <ta e="T111" id="Seg_3117" s="T110">go.in-CVB.SEQ</ta>
            <ta e="T112" id="Seg_3118" s="T111">fall.asleep-CVB.SEQ</ta>
            <ta e="T113" id="Seg_3119" s="T112">stay-PST2.[3SG]</ta>
            <ta e="T114" id="Seg_3120" s="T113">in.the.morning</ta>
            <ta e="T115" id="Seg_3121" s="T114">neighbour-PL.[NOM]</ta>
            <ta e="T116" id="Seg_3122" s="T115">see-PST2-3PL</ta>
            <ta e="T117" id="Seg_3123" s="T116">Kaamyylaak.[NOM]</ta>
            <ta e="T118" id="Seg_3124" s="T117">reindeer.calf-3SG-ACC</ta>
            <ta e="T119" id="Seg_3125" s="T118">two</ta>
            <ta e="T120" id="Seg_3126" s="T119">best</ta>
            <ta e="T121" id="Seg_3127" s="T120">reindeer.bull-3PL.[NOM]</ta>
            <ta e="T122" id="Seg_3128" s="T121">horn-3PL-DAT/LOC</ta>
            <ta e="T123" id="Seg_3129" s="T122">put.on-CVB.SIM</ta>
            <ta e="T124" id="Seg_3130" s="T123">go-PRS-3PL</ta>
            <ta e="T125" id="Seg_3131" s="T124">Kaamyylaak.[NOM]</ta>
            <ta e="T126" id="Seg_3132" s="T125">that-ACC</ta>
            <ta e="T127" id="Seg_3133" s="T126">hear-PST1-3SG</ta>
            <ta e="T128" id="Seg_3134" s="T127">EMPH</ta>
            <ta e="T129" id="Seg_3135" s="T128">well</ta>
            <ta e="T130" id="Seg_3136" s="T129">indeed</ta>
            <ta e="T131" id="Seg_3137" s="T130">die-PRS-1SG</ta>
            <ta e="T132" id="Seg_3138" s="T131">be-PST2.[3SG]</ta>
            <ta e="T133" id="Seg_3139" s="T132">reindeer.calf-POSS</ta>
            <ta e="T134" id="Seg_3140" s="T133">NEG</ta>
            <ta e="T135" id="Seg_3141" s="T134">stay-PST1-1SG</ta>
            <ta e="T136" id="Seg_3142" s="T135">say-CVB.SEQ</ta>
            <ta e="T137" id="Seg_3143" s="T136">shout-PST2.[3SG]</ta>
            <ta e="T138" id="Seg_3144" s="T137">well</ta>
            <ta e="T139" id="Seg_3145" s="T138">die-EP-NEG.[IMP.2SG]</ta>
            <ta e="T140" id="Seg_3146" s="T139">only</ta>
            <ta e="T141" id="Seg_3147" s="T140">1PL.[NOM]</ta>
            <ta e="T142" id="Seg_3148" s="T141">2SG-DAT/LOC</ta>
            <ta e="T143" id="Seg_3149" s="T142">that</ta>
            <ta e="T144" id="Seg_3150" s="T143">two</ta>
            <ta e="T145" id="Seg_3151" s="T144">reindeer.bull-ACC</ta>
            <ta e="T146" id="Seg_3152" s="T145">give-FUT-1PL</ta>
            <ta e="T147" id="Seg_3153" s="T146">say-PST2-3PL</ta>
            <ta e="T148" id="Seg_3154" s="T147">house-ADJZ-3PL.[NOM]</ta>
            <ta e="T149" id="Seg_3155" s="T148">Kaamyylaak.[NOM]</ta>
            <ta e="T150" id="Seg_3156" s="T149">two</ta>
            <ta e="T151" id="Seg_3157" s="T150">reindeer.bull-VBZ-PST2.[3SG]</ta>
            <ta e="T152" id="Seg_3158" s="T151">two</ta>
            <ta e="T153" id="Seg_3159" s="T152">reindeer.bull-3SG-ACC</ta>
            <ta e="T154" id="Seg_3160" s="T153">harness-CVB.SEQ</ta>
            <ta e="T155" id="Seg_3161" s="T154">after</ta>
            <ta e="T156" id="Seg_3162" s="T155">Kaamyylaak.[NOM]</ta>
            <ta e="T157" id="Seg_3163" s="T156">sledge-VBZ-CVB.SIM</ta>
            <ta e="T158" id="Seg_3164" s="T157">go-PST2.[3SG]</ta>
            <ta e="T159" id="Seg_3165" s="T158">drive-CVB.SEQ-drive-CVB.SEQ</ta>
            <ta e="T160" id="Seg_3166" s="T159">tomb-PL-DAT/LOC</ta>
            <ta e="T161" id="Seg_3167" s="T160">push-PST2.[3SG]</ta>
            <ta e="T162" id="Seg_3168" s="T161">there</ta>
            <ta e="T163" id="Seg_3169" s="T162">one</ta>
            <ta e="T164" id="Seg_3170" s="T163">old.woman-ACC</ta>
            <ta e="T165" id="Seg_3171" s="T164">wooden.box-3SG-ACC</ta>
            <ta e="T166" id="Seg_3172" s="T165">open-CVB.SEQ</ta>
            <ta e="T167" id="Seg_3173" s="T166">take.out-CVB.SEQ</ta>
            <ta e="T168" id="Seg_3174" s="T167">take-PST2.[3SG]</ta>
            <ta e="T169" id="Seg_3175" s="T168">sledge-3SG-DAT/LOC</ta>
            <ta e="T170" id="Seg_3176" s="T169">lay-CVB.SEQ</ta>
            <ta e="T171" id="Seg_3177" s="T170">throw-PST2.[3SG]</ta>
            <ta e="T172" id="Seg_3178" s="T171">go-CVB.SEQ</ta>
            <ta e="T173" id="Seg_3179" s="T172">go-PST2.[3SG]</ta>
            <ta e="T174" id="Seg_3180" s="T173">go-CVB.SEQ-go-CVB.SEQ</ta>
            <ta e="T175" id="Seg_3181" s="T174">one</ta>
            <ta e="T176" id="Seg_3182" s="T175">inhabitant-DAT/LOC</ta>
            <ta e="T177" id="Seg_3183" s="T176">reach-PST2.[3SG]</ta>
            <ta e="T178" id="Seg_3184" s="T177">sledge-3SG-ACC</ta>
            <ta e="T179" id="Seg_3185" s="T178">inhabitant-PL-ABL</ta>
            <ta e="T180" id="Seg_3186" s="T179">far.away-INTNS</ta>
            <ta e="T181" id="Seg_3187" s="T180">let-PST2.[3SG]</ta>
            <ta e="T182" id="Seg_3188" s="T181">Kaamyylaak.[NOM]</ta>
            <ta e="T183" id="Seg_3189" s="T182">outside</ta>
            <ta e="T184" id="Seg_3190" s="T183">child-PL-ACC</ta>
            <ta e="T185" id="Seg_3191" s="T184">meet-EP-PST2.[3SG]</ta>
            <ta e="T186" id="Seg_3192" s="T185">child-PL.[NOM]</ta>
            <ta e="T187" id="Seg_3193" s="T186">1SG.[NOM]</ta>
            <ta e="T188" id="Seg_3194" s="T187">sledge-1SG-DAT/LOC</ta>
            <ta e="T189" id="Seg_3195" s="T188">come.closer-APPR-2PL</ta>
            <ta e="T190" id="Seg_3196" s="T189">sledge-1SG-DAT/LOC</ta>
            <ta e="T191" id="Seg_3197" s="T190">old.woman-EP-1SG.[NOM]</ta>
            <ta e="T192" id="Seg_3198" s="T191">there.is</ta>
            <ta e="T193" id="Seg_3199" s="T192">human.being.[NOM]</ta>
            <ta e="T194" id="Seg_3200" s="T193">come.closer-PST1-3SG</ta>
            <ta e="T195" id="Seg_3201" s="T194">and</ta>
            <ta e="T196" id="Seg_3202" s="T195">startle-CVB.SEQ</ta>
            <ta e="T197" id="Seg_3203" s="T196">die-CVB.SEQ</ta>
            <ta e="T198" id="Seg_3204" s="T197">stay-FUT-3SG</ta>
            <ta e="T199" id="Seg_3205" s="T198">say-PST2.[3SG]</ta>
            <ta e="T200" id="Seg_3206" s="T199">Kaamyylaak.[NOM]</ta>
            <ta e="T201" id="Seg_3207" s="T200">tea.[NOM]</ta>
            <ta e="T202" id="Seg_3208" s="T201">drink-CVB.SIM</ta>
            <ta e="T203" id="Seg_3209" s="T202">sit-TEMP-3SG</ta>
            <ta e="T204" id="Seg_3210" s="T203">child-PL.[NOM]</ta>
            <ta e="T205" id="Seg_3211" s="T204">run-CVB.SEQ</ta>
            <ta e="T206" id="Seg_3212" s="T205">go.in-PST2-3PL</ta>
            <ta e="T207" id="Seg_3213" s="T206">Kaamyylaak.[NOM]</ta>
            <ta e="T208" id="Seg_3214" s="T207">old.woman-EP-2SG.[NOM]</ta>
            <ta e="T209" id="Seg_3215" s="T208">die-CVB.SEQ</ta>
            <ta e="T210" id="Seg_3216" s="T209">stay-PST2.[3SG]</ta>
            <ta e="T211" id="Seg_3217" s="T210">say-PST2-3PL</ta>
            <ta e="T212" id="Seg_3218" s="T211">oh.dear</ta>
            <ta e="T213" id="Seg_3219" s="T212">now</ta>
            <ta e="T214" id="Seg_3220" s="T213">EMPH</ta>
            <ta e="T215" id="Seg_3221" s="T214">indeed</ta>
            <ta e="T216" id="Seg_3222" s="T215">die-CVB.SIM-1SG</ta>
            <ta e="T217" id="Seg_3223" s="T216">be-PST2.[3SG]</ta>
            <ta e="T218" id="Seg_3224" s="T217">old.woman-POSS</ta>
            <ta e="T219" id="Seg_3225" s="T218">NEG</ta>
            <ta e="T220" id="Seg_3226" s="T219">stay-PST1-1SG</ta>
            <ta e="T221" id="Seg_3227" s="T220">that</ta>
            <ta e="T222" id="Seg_3228" s="T221">child-PL.[NOM]</ta>
            <ta e="T223" id="Seg_3229" s="T222">sledge-DAT/LOC</ta>
            <ta e="T224" id="Seg_3230" s="T223">come.closer-CVB.SEQ-3PL</ta>
            <ta e="T225" id="Seg_3231" s="T224">Kaamyylaak.[NOM]</ta>
            <ta e="T226" id="Seg_3232" s="T225">shout-PST2.[3SG]</ta>
            <ta e="T227" id="Seg_3233" s="T226">why</ta>
            <ta e="T228" id="Seg_3234" s="T227">die-CVB.PURP</ta>
            <ta e="T229" id="Seg_3235" s="T228">make-PRS-2SG</ta>
            <ta e="T230" id="Seg_3236" s="T229">well</ta>
            <ta e="T231" id="Seg_3237" s="T230">take.[IMP.2SG]</ta>
            <ta e="T232" id="Seg_3238" s="T231">that</ta>
            <ta e="T233" id="Seg_3239" s="T232">two</ta>
            <ta e="T234" id="Seg_3240" s="T233">girl-1PL-ACC</ta>
            <ta e="T235" id="Seg_3241" s="T234">say-EP-RECP/COLL-PST2-3PL</ta>
            <ta e="T236" id="Seg_3242" s="T235">house-PROPR-PL.[NOM]</ta>
            <ta e="T237" id="Seg_3243" s="T236">Kaamyylaak.[NOM]</ta>
            <ta e="T238" id="Seg_3244" s="T237">so</ta>
            <ta e="T239" id="Seg_3245" s="T238">two</ta>
            <ta e="T240" id="Seg_3246" s="T239">girl-VBZ-PST2.[3SG]</ta>
            <ta e="T241" id="Seg_3247" s="T240">girl-PL-3SG-ACC</ta>
            <ta e="T242" id="Seg_3248" s="T241">load-CVB.SEQ</ta>
            <ta e="T243" id="Seg_3249" s="T242">after</ta>
            <ta e="T244" id="Seg_3250" s="T243">Kaamyylaak.[NOM]</ta>
            <ta e="T245" id="Seg_3251" s="T244">go-CVB.SEQ</ta>
            <ta e="T246" id="Seg_3252" s="T245">go-PST2.[3SG]</ta>
            <ta e="T247" id="Seg_3253" s="T246">well</ta>
            <ta e="T248" id="Seg_3254" s="T247">go-CVB.SEQ</ta>
            <ta e="T249" id="Seg_3255" s="T248">drive-CVB.SEQ-drive-CVB.SEQ</ta>
            <ta e="T250" id="Seg_3256" s="T249">one</ta>
            <ta e="T251" id="Seg_3257" s="T250">rich.[NOM]</ta>
            <ta e="T252" id="Seg_3258" s="T251">porch-3SG-ACC</ta>
            <ta e="T253" id="Seg_3259" s="T252">see-PST2.[3SG]</ta>
            <ta e="T254" id="Seg_3260" s="T253">that-ACC</ta>
            <ta e="T255" id="Seg_3261" s="T254">see-CVB.SEQ</ta>
            <ta e="T256" id="Seg_3262" s="T255">Kaamyylaak.[NOM]</ta>
            <ta e="T257" id="Seg_3263" s="T256">girl-PL-3SG-DAT/LOC</ta>
            <ta e="T258" id="Seg_3264" s="T257">speak-PST2.[3SG]</ta>
            <ta e="T259" id="Seg_3265" s="T258">well</ta>
            <ta e="T260" id="Seg_3266" s="T259">girl-PL.[NOM]</ta>
            <ta e="T261" id="Seg_3267" s="T260">go-EP-IMP.2PL</ta>
            <ta e="T262" id="Seg_3268" s="T261">go.out-EP-IMP.2PL</ta>
            <ta e="T263" id="Seg_3269" s="T262">larch.[NOM]</ta>
            <ta e="T264" id="Seg_3270" s="T263">tree-DAT/LOC</ta>
            <ta e="T265" id="Seg_3271" s="T264">hide-CVB.SEQ</ta>
            <ta e="T266" id="Seg_3272" s="T265">sit-EP-IMP.2PL</ta>
            <ta e="T267" id="Seg_3273" s="T266">1SG.[NOM]</ta>
            <ta e="T268" id="Seg_3274" s="T267">rich-DAT/LOC</ta>
            <ta e="T269" id="Seg_3275" s="T268">go-FUT-1SG</ta>
            <ta e="T270" id="Seg_3276" s="T269">3SG-ACC</ta>
            <ta e="T271" id="Seg_3277" s="T270">hither</ta>
            <ta e="T272" id="Seg_3278" s="T271">bring-FUT-1SG</ta>
            <ta e="T273" id="Seg_3279" s="T272">rich.[NOM]</ta>
            <ta e="T274" id="Seg_3280" s="T273">whose</ta>
            <ta e="T275" id="Seg_3281" s="T274">wealth-3SG.[NOM]=Q</ta>
            <ta e="T276" id="Seg_3282" s="T275">say-CVB.SIM-say-CVB.SIM</ta>
            <ta e="T277" id="Seg_3283" s="T276">ask-FUT.[3SG]</ta>
            <ta e="T278" id="Seg_3284" s="T277">that-DAT/LOC</ta>
            <ta e="T279" id="Seg_3285" s="T278">2PL.[NOM]</ta>
            <ta e="T280" id="Seg_3286" s="T279">say-FUT-EP-IMP.2PL</ta>
            <ta e="T281" id="Seg_3287" s="T280">Kaamyylaak.[NOM]</ta>
            <ta e="T282" id="Seg_3288" s="T281">wealth-3SG.[NOM]</ta>
            <ta e="T283" id="Seg_3289" s="T282">that-ACC</ta>
            <ta e="T284" id="Seg_3290" s="T283">speak-CVB.SEQ</ta>
            <ta e="T285" id="Seg_3291" s="T284">after</ta>
            <ta e="T286" id="Seg_3292" s="T285">Kaamyylaak.[NOM]</ta>
            <ta e="T287" id="Seg_3293" s="T286">rich-DAT/LOC</ta>
            <ta e="T288" id="Seg_3294" s="T287">reach-PST2.[3SG]</ta>
            <ta e="T289" id="Seg_3295" s="T288">tea.[NOM]</ta>
            <ta e="T290" id="Seg_3296" s="T289">go-CVB.SEQ</ta>
            <ta e="T291" id="Seg_3297" s="T290">stop-CVB.SEQ</ta>
            <ta e="T292" id="Seg_3298" s="T291">say-PST2.[3SG]</ta>
            <ta e="T293" id="Seg_3299" s="T292">well</ta>
            <ta e="T294" id="Seg_3300" s="T293">friend</ta>
            <ta e="T295" id="Seg_3301" s="T294">who-1PL.[NOM]</ta>
            <ta e="T296" id="Seg_3302" s="T295">wealth-3SG.[NOM]=Q</ta>
            <ta e="T297" id="Seg_3303" s="T296">this</ta>
            <ta e="T298" id="Seg_3304" s="T297">there</ta>
            <ta e="T299" id="Seg_3305" s="T298">rich.[NOM]</ta>
            <ta e="T300" id="Seg_3306" s="T299">startle-CVB.SIM</ta>
            <ta e="T301" id="Seg_3307" s="T300">beat-CVB.SEQ</ta>
            <ta e="T302" id="Seg_3308" s="T301">after</ta>
            <ta e="T303" id="Seg_3309" s="T302">speak-PST2.[3SG]</ta>
            <ta e="T304" id="Seg_3310" s="T303">whose</ta>
            <ta e="T305" id="Seg_3311" s="T304">be-FUT.[3SG]=Q</ta>
            <ta e="T306" id="Seg_3312" s="T305">my</ta>
            <ta e="T307" id="Seg_3313" s="T306">be-EP-NEG.CVB</ta>
            <ta e="T308" id="Seg_3314" s="T307">Kaamyylaak.[NOM]</ta>
            <ta e="T309" id="Seg_3315" s="T308">no</ta>
            <ta e="T310" id="Seg_3316" s="T309">my</ta>
            <ta e="T311" id="Seg_3317" s="T310">believe-NEG.PTCP</ta>
            <ta e="T312" id="Seg_3318" s="T311">be-TEMP-2SG</ta>
            <ta e="T313" id="Seg_3319" s="T312">go-IMP.1DU</ta>
            <ta e="T314" id="Seg_3320" s="T313">sky-ABL</ta>
            <ta e="T315" id="Seg_3321" s="T314">ask-CVB.SIM</ta>
            <ta e="T316" id="Seg_3322" s="T315">say-PST2.[3SG]</ta>
            <ta e="T317" id="Seg_3323" s="T316">go-PST2-3PL</ta>
            <ta e="T318" id="Seg_3324" s="T317">girl-PL.[NOM]</ta>
            <ta e="T319" id="Seg_3325" s="T318">lie-PTCP.PRS</ta>
            <ta e="T320" id="Seg_3326" s="T319">place-3PL-DAT/LOC</ta>
            <ta e="T321" id="Seg_3327" s="T320">come-CVB.SEQ</ta>
            <ta e="T322" id="Seg_3328" s="T321">after</ta>
            <ta e="T323" id="Seg_3329" s="T322">Kaamyylaak.[NOM]</ta>
            <ta e="T324" id="Seg_3330" s="T323">shout-PST2.[3SG]</ta>
            <ta e="T325" id="Seg_3331" s="T324">this.[NOM]</ta>
            <ta e="T326" id="Seg_3332" s="T325">who.[NOM]</ta>
            <ta e="T327" id="Seg_3333" s="T326">wealth-3SG.[NOM]=Q</ta>
            <ta e="T328" id="Seg_3334" s="T327">up-ABL</ta>
            <ta e="T329" id="Seg_3335" s="T328">girl-PL.[NOM]</ta>
            <ta e="T330" id="Seg_3336" s="T329">shout-PST2-3PL</ta>
            <ta e="T331" id="Seg_3337" s="T330">Kaamyylaak.[NOM]</ta>
            <ta e="T332" id="Seg_3338" s="T331">wealth-3SG.[NOM]</ta>
            <ta e="T333" id="Seg_3339" s="T332">Kaamyylaak.[NOM]</ta>
            <ta e="T334" id="Seg_3340" s="T333">wealth-3SG.[NOM]</ta>
            <ta e="T335" id="Seg_3341" s="T334">hear-PST1-2SG</ta>
            <ta e="T336" id="Seg_3342" s="T335">Q</ta>
            <ta e="T337" id="Seg_3343" s="T336">say-PST2.[3SG]</ta>
            <ta e="T338" id="Seg_3344" s="T337">Kaamyylaak.[NOM]</ta>
            <ta e="T339" id="Seg_3345" s="T338">who.[NOM]</ta>
            <ta e="T340" id="Seg_3346" s="T339">wealth-3SG.[NOM]</ta>
            <ta e="T341" id="Seg_3347" s="T340">be-PST2.[3SG]=Q</ta>
            <ta e="T342" id="Seg_3348" s="T341">Kaamyylaak.[NOM]</ta>
            <ta e="T343" id="Seg_3349" s="T342">that</ta>
            <ta e="T344" id="Seg_3350" s="T343">rich-ACC</ta>
            <ta e="T345" id="Seg_3351" s="T344">follow-CVB.SEQ</ta>
            <ta e="T346" id="Seg_3352" s="T345">send-PST2.[3SG]</ta>
            <ta e="T347" id="Seg_3353" s="T346">wealth-3SG-ACC</ta>
            <ta e="T348" id="Seg_3354" s="T347">wealth.[NOM]</ta>
            <ta e="T349" id="Seg_3355" s="T348">make-PST2.[3SG]</ta>
            <ta e="T350" id="Seg_3356" s="T349">be.rich-CVB.SEQ-eat.ones.fill-CVB.SEQ</ta>
            <ta e="T351" id="Seg_3357" s="T350">live-PST2.[3SG]</ta>
            <ta e="T352" id="Seg_3358" s="T351">last-3SG.[NOM]</ta>
            <ta e="T353" id="Seg_3359" s="T352">stop-PST1-3SG</ta>
         </annotation>
         <annotation name="gg" tierref="gg">
            <ta e="T1" id="Seg_3360" s="T0">Kaamyylaak.[NOM]</ta>
            <ta e="T2" id="Seg_3361" s="T1">go-CVB.SEQ</ta>
            <ta e="T3" id="Seg_3362" s="T2">gehen-PST2.[3SG]</ta>
            <ta e="T4" id="Seg_3363" s="T3">go-CVB.SEQ</ta>
            <ta e="T5" id="Seg_3364" s="T4">gehen-CVB.SEQ-gehen-CVB.SEQ</ta>
            <ta e="T6" id="Seg_3365" s="T5">vorübergehende.Siedlung-DAT/LOC</ta>
            <ta e="T7" id="Seg_3366" s="T6">kommen-PST2.[3SG]</ta>
            <ta e="T8" id="Seg_3367" s="T7">Eis-DAT/LOC</ta>
            <ta e="T9" id="Seg_3368" s="T8">frieren-PTCP.PST</ta>
            <ta e="T10" id="Seg_3369" s="T9">Blut-ACC</ta>
            <ta e="T11" id="Seg_3370" s="T10">sehen-PST2.[3SG]</ta>
            <ta e="T12" id="Seg_3371" s="T11">Rentier.[NOM]</ta>
            <ta e="T13" id="Seg_3372" s="T12">Vorderbein-3SG-ACC</ta>
            <ta e="T14" id="Seg_3373" s="T13">finden-PST1-1SG</ta>
            <ta e="T15" id="Seg_3374" s="T14">sagen-CVB.SIM-sagen-CVB.SIM</ta>
            <ta e="T16" id="Seg_3375" s="T15">Tasche-3SG-DAT/LOC</ta>
            <ta e="T17" id="Seg_3376" s="T16">hineintun-CVB.SEQ</ta>
            <ta e="T18" id="Seg_3377" s="T17">werfen-PST2.[3SG]</ta>
            <ta e="T19" id="Seg_3378" s="T18">so</ta>
            <ta e="T20" id="Seg_3379" s="T19">machen-CVB.SEQ</ta>
            <ta e="T21" id="Seg_3380" s="T20">nachdem</ta>
            <ta e="T22" id="Seg_3381" s="T21">go-CVB.SEQ</ta>
            <ta e="T23" id="Seg_3382" s="T22">gehen-PST2.[3SG]</ta>
            <ta e="T24" id="Seg_3383" s="T23">gehen-CVB.SEQ-gehen-CVB.SEQ</ta>
            <ta e="T25" id="Seg_3384" s="T24">Bewohner-PL-DAT/LOC</ta>
            <ta e="T26" id="Seg_3385" s="T25">kommen-PST2.[3SG]</ta>
            <ta e="T27" id="Seg_3386" s="T26">Bewohner-PL-DAT/LOC</ta>
            <ta e="T28" id="Seg_3387" s="T27">kochen-CVB.PURP</ta>
            <ta e="T29" id="Seg_3388" s="T28">Fleisch-3PL-ACC</ta>
            <ta e="T30" id="Seg_3389" s="T29">Fleisch-VBZ-PST2-3PL</ta>
            <ta e="T31" id="Seg_3390" s="T30">na</ta>
            <ta e="T32" id="Seg_3391" s="T31">Gast-1PL-DAT/LOC</ta>
            <ta e="T33" id="Seg_3392" s="T32">Fleisch.[NOM]</ta>
            <ta e="T34" id="Seg_3393" s="T33">reichen-NEG.PTCP</ta>
            <ta e="T35" id="Seg_3394" s="T34">sein-PST2.[3SG]</ta>
            <ta e="T36" id="Seg_3395" s="T35">sagen-EP-RECP/COLL-PST2-3PL</ta>
            <ta e="T37" id="Seg_3396" s="T36">jenes-DAT/LOC</ta>
            <ta e="T38" id="Seg_3397" s="T37">Kaamyylaak.[NOM]</ta>
            <ta e="T39" id="Seg_3398" s="T38">sagen-PST2.[3SG]</ta>
            <ta e="T40" id="Seg_3399" s="T39">na</ta>
            <ta e="T41" id="Seg_3400" s="T40">was.[NOM]</ta>
            <ta e="T42" id="Seg_3401" s="T41">sein-PRS-2PL</ta>
            <ta e="T43" id="Seg_3402" s="T42">1SG.[NOM]</ta>
            <ta e="T44" id="Seg_3403" s="T43">Tasche-1SG-DAT/LOC</ta>
            <ta e="T45" id="Seg_3404" s="T44">Fleisch.[NOM]</ta>
            <ta e="T46" id="Seg_3405" s="T45">es.gibt</ta>
            <ta e="T47" id="Seg_3406" s="T46">sein-PST1-3SG</ta>
            <ta e="T48" id="Seg_3407" s="T47">jenes-ACC</ta>
            <ta e="T49" id="Seg_3408" s="T48">hereinbringen-CVB.SEQ</ta>
            <ta e="T50" id="Seg_3409" s="T49">kochen-IMP.2PL</ta>
            <ta e="T51" id="Seg_3410" s="T50">doch</ta>
            <ta e="T52" id="Seg_3411" s="T51">dann</ta>
            <ta e="T53" id="Seg_3412" s="T52">Bewohner-PL.[NOM]</ta>
            <ta e="T54" id="Seg_3413" s="T53">Kessel-3PL-DAT/LOC</ta>
            <ta e="T55" id="Seg_3414" s="T54">Kaamyylaak.[NOM]</ta>
            <ta e="T56" id="Seg_3415" s="T55">Fleisch-3SG-ACC</ta>
            <ta e="T57" id="Seg_3416" s="T56">stecken-CVB.SEQ</ta>
            <ta e="T58" id="Seg_3417" s="T57">werfen-PST2-3PL</ta>
            <ta e="T59" id="Seg_3418" s="T58">Abend-INSTR</ta>
            <ta e="T60" id="Seg_3419" s="T59">doch</ta>
            <ta e="T61" id="Seg_3420" s="T60">Fleisch-3PL-ACC</ta>
            <ta e="T62" id="Seg_3421" s="T61">herausnehmen-PRS-3PL</ta>
            <ta e="T63" id="Seg_3422" s="T62">herausnehmen-PTCP.PST-3PL-DAT/LOC</ta>
            <ta e="T64" id="Seg_3423" s="T63">Gast-3PL-DAT/LOC</ta>
            <ta e="T65" id="Seg_3424" s="T64">Fleisch-3PL.[NOM]</ta>
            <ta e="T66" id="Seg_3425" s="T65">wieder</ta>
            <ta e="T67" id="Seg_3426" s="T66">reichen-PST2.NEG.[3SG]</ta>
            <ta e="T68" id="Seg_3427" s="T67">Kaamyylaak.[NOM]</ta>
            <ta e="T69" id="Seg_3428" s="T68">na</ta>
            <ta e="T70" id="Seg_3429" s="T69">wo=Q</ta>
            <ta e="T71" id="Seg_3430" s="T70">1SG.[NOM]</ta>
            <ta e="T72" id="Seg_3431" s="T71">Fleisch-EP-1SG.[NOM]</ta>
            <ta e="T73" id="Seg_3432" s="T72">sagen-PST2.[3SG]</ta>
            <ta e="T74" id="Seg_3433" s="T73">sterben-PST1-1SG-sterben-PST1-1SG</ta>
            <ta e="T75" id="Seg_3434" s="T74">wie</ta>
            <ta e="T76" id="Seg_3435" s="T75">essen-NEG.CVB.SIM</ta>
            <ta e="T77" id="Seg_3436" s="T76">einschlafen-FUT-1SG=Q</ta>
            <ta e="T78" id="Seg_3437" s="T77">hey</ta>
            <ta e="T79" id="Seg_3438" s="T78">sterben-EP-NEG.[IMP.2SG]</ta>
            <ta e="T80" id="Seg_3439" s="T79">nur</ta>
            <ta e="T81" id="Seg_3440" s="T80">1PL.[NOM]</ta>
            <ta e="T82" id="Seg_3441" s="T81">2SG-DAT/LOC</ta>
            <ta e="T83" id="Seg_3442" s="T82">ganz</ta>
            <ta e="T84" id="Seg_3443" s="T83">Kalb-PART</ta>
            <ta e="T85" id="Seg_3444" s="T84">geben-PRS-1PL</ta>
            <ta e="T86" id="Seg_3445" s="T85">sagen-PST2-3PL</ta>
            <ta e="T87" id="Seg_3446" s="T86">Bewohner-PL.[NOM]</ta>
            <ta e="T88" id="Seg_3447" s="T87">essen-CVB.SEQ</ta>
            <ta e="T89" id="Seg_3448" s="T88">nachdem</ta>
            <ta e="T90" id="Seg_3449" s="T89">einschlafen-PST2-3PL</ta>
            <ta e="T91" id="Seg_3450" s="T90">Rentierkalb-3PL-ACC</ta>
            <ta e="T92" id="Seg_3451" s="T91">sehen-CAUS-PST2-3PL</ta>
            <ta e="T93" id="Seg_3452" s="T92">Kaamyylaak-DAT/LOC</ta>
            <ta e="T94" id="Seg_3453" s="T93">Kaamyylaak.[NOM]</ta>
            <ta e="T95" id="Seg_3454" s="T94">nachts</ta>
            <ta e="T96" id="Seg_3455" s="T95">hinausgehen-CVB.SEQ</ta>
            <ta e="T97" id="Seg_3456" s="T96">Rentierkalb-3SG.[NOM]</ta>
            <ta e="T98" id="Seg_3457" s="T97">EMPH</ta>
            <ta e="T99" id="Seg_3458" s="T98">kommen-PTCP.PST-3SG-ACC</ta>
            <ta e="T100" id="Seg_3459" s="T99">greifen-CVB.SEQ</ta>
            <ta e="T101" id="Seg_3460" s="T100">nehmen-PST2.[3SG]</ta>
            <ta e="T102" id="Seg_3461" s="T101">so</ta>
            <ta e="T103" id="Seg_3462" s="T102">machen-CVB.SEQ</ta>
            <ta e="T104" id="Seg_3463" s="T103">nachdem</ta>
            <ta e="T105" id="Seg_3464" s="T104">zwei</ta>
            <ta e="T106" id="Seg_3465" s="T105">bester</ta>
            <ta e="T107" id="Seg_3466" s="T106">Rentierbulle.[NOM]</ta>
            <ta e="T108" id="Seg_3467" s="T107">Horn-3PL-DAT/LOC</ta>
            <ta e="T109" id="Seg_3468" s="T108">aufhängen-CVB.SEQ</ta>
            <ta e="T110" id="Seg_3469" s="T109">werfen-PST2.[3SG]</ta>
            <ta e="T111" id="Seg_3470" s="T110">hineingehen-CVB.SEQ</ta>
            <ta e="T112" id="Seg_3471" s="T111">einschlafen-CVB.SEQ</ta>
            <ta e="T113" id="Seg_3472" s="T112">bleiben-PST2.[3SG]</ta>
            <ta e="T114" id="Seg_3473" s="T113">am.Morgen</ta>
            <ta e="T115" id="Seg_3474" s="T114">Nachbar-PL.[NOM]</ta>
            <ta e="T116" id="Seg_3475" s="T115">sehen-PST2-3PL</ta>
            <ta e="T117" id="Seg_3476" s="T116">Kaamyylaak.[NOM]</ta>
            <ta e="T118" id="Seg_3477" s="T117">Rentierkalb-3SG-ACC</ta>
            <ta e="T119" id="Seg_3478" s="T118">zwei</ta>
            <ta e="T120" id="Seg_3479" s="T119">bester</ta>
            <ta e="T121" id="Seg_3480" s="T120">Rentierbulle-3PL.[NOM]</ta>
            <ta e="T122" id="Seg_3481" s="T121">Horn-3PL-DAT/LOC</ta>
            <ta e="T123" id="Seg_3482" s="T122">umhängen-CVB.SIM</ta>
            <ta e="T124" id="Seg_3483" s="T123">gehen-PRS-3PL</ta>
            <ta e="T125" id="Seg_3484" s="T124">Kaamyylaak.[NOM]</ta>
            <ta e="T126" id="Seg_3485" s="T125">jenes-ACC</ta>
            <ta e="T127" id="Seg_3486" s="T126">hören-PST1-3SG</ta>
            <ta e="T128" id="Seg_3487" s="T127">EMPH</ta>
            <ta e="T129" id="Seg_3488" s="T128">doch</ta>
            <ta e="T130" id="Seg_3489" s="T129">tatsächlich</ta>
            <ta e="T131" id="Seg_3490" s="T130">sterben-PRS-1SG</ta>
            <ta e="T132" id="Seg_3491" s="T131">sein-PST2.[3SG]</ta>
            <ta e="T133" id="Seg_3492" s="T132">Rentierkalb-POSS</ta>
            <ta e="T134" id="Seg_3493" s="T133">NEG</ta>
            <ta e="T135" id="Seg_3494" s="T134">bleiben-PST1-1SG</ta>
            <ta e="T136" id="Seg_3495" s="T135">sagen-CVB.SEQ</ta>
            <ta e="T137" id="Seg_3496" s="T136">schreien-PST2.[3SG]</ta>
            <ta e="T138" id="Seg_3497" s="T137">na</ta>
            <ta e="T139" id="Seg_3498" s="T138">sterben-EP-NEG.[IMP.2SG]</ta>
            <ta e="T140" id="Seg_3499" s="T139">nur</ta>
            <ta e="T141" id="Seg_3500" s="T140">1PL.[NOM]</ta>
            <ta e="T142" id="Seg_3501" s="T141">2SG-DAT/LOC</ta>
            <ta e="T143" id="Seg_3502" s="T142">jenes</ta>
            <ta e="T144" id="Seg_3503" s="T143">zwei</ta>
            <ta e="T145" id="Seg_3504" s="T144">Rentierbulle-ACC</ta>
            <ta e="T146" id="Seg_3505" s="T145">geben-FUT-1PL</ta>
            <ta e="T147" id="Seg_3506" s="T146">sagen-PST2-3PL</ta>
            <ta e="T148" id="Seg_3507" s="T147">Haus-ADJZ-3PL.[NOM]</ta>
            <ta e="T149" id="Seg_3508" s="T148">Kaamyylaak.[NOM]</ta>
            <ta e="T150" id="Seg_3509" s="T149">zwei</ta>
            <ta e="T151" id="Seg_3510" s="T150">Rentierbulle-VBZ-PST2.[3SG]</ta>
            <ta e="T152" id="Seg_3511" s="T151">zwei</ta>
            <ta e="T153" id="Seg_3512" s="T152">Rentierbulle-3SG-ACC</ta>
            <ta e="T154" id="Seg_3513" s="T153">einspannen-CVB.SEQ</ta>
            <ta e="T155" id="Seg_3514" s="T154">nachdem</ta>
            <ta e="T156" id="Seg_3515" s="T155">Kaamyylaak.[NOM]</ta>
            <ta e="T157" id="Seg_3516" s="T156">Schlitten-VBZ-CVB.SIM</ta>
            <ta e="T158" id="Seg_3517" s="T157">gehen-PST2.[3SG]</ta>
            <ta e="T159" id="Seg_3518" s="T158">fahren-CVB.SEQ-fahren-CVB.SEQ</ta>
            <ta e="T160" id="Seg_3519" s="T159">Grab-PL-DAT/LOC</ta>
            <ta e="T161" id="Seg_3520" s="T160">stoßen-PST2.[3SG]</ta>
            <ta e="T162" id="Seg_3521" s="T161">dort</ta>
            <ta e="T163" id="Seg_3522" s="T162">eins</ta>
            <ta e="T164" id="Seg_3523" s="T163">Alte-ACC</ta>
            <ta e="T165" id="Seg_3524" s="T164">Holzkiste-3SG-ACC</ta>
            <ta e="T166" id="Seg_3525" s="T165">öffnen-CVB.SEQ</ta>
            <ta e="T167" id="Seg_3526" s="T166">herausnehmen-CVB.SEQ</ta>
            <ta e="T168" id="Seg_3527" s="T167">nehmen-PST2.[3SG]</ta>
            <ta e="T169" id="Seg_3528" s="T168">Schlitten-3SG-DAT/LOC</ta>
            <ta e="T170" id="Seg_3529" s="T169">legen-CVB.SEQ</ta>
            <ta e="T171" id="Seg_3530" s="T170">werfen-PST2.[3SG]</ta>
            <ta e="T172" id="Seg_3531" s="T171">gehen-CVB.SEQ</ta>
            <ta e="T173" id="Seg_3532" s="T172">gehen-PST2.[3SG]</ta>
            <ta e="T174" id="Seg_3533" s="T173">gehen-CVB.SEQ-gehen-CVB.SEQ</ta>
            <ta e="T175" id="Seg_3534" s="T174">eins</ta>
            <ta e="T176" id="Seg_3535" s="T175">Bewohner-DAT/LOC</ta>
            <ta e="T177" id="Seg_3536" s="T176">ankommen-PST2.[3SG]</ta>
            <ta e="T178" id="Seg_3537" s="T177">Schlitten-3SG-ACC</ta>
            <ta e="T179" id="Seg_3538" s="T178">Bewohner-PL-ABL</ta>
            <ta e="T180" id="Seg_3539" s="T179">weit.weg-INTNS</ta>
            <ta e="T181" id="Seg_3540" s="T180">lassen-PST2.[3SG]</ta>
            <ta e="T182" id="Seg_3541" s="T181">Kaamyylaak.[NOM]</ta>
            <ta e="T183" id="Seg_3542" s="T182">draußen</ta>
            <ta e="T184" id="Seg_3543" s="T183">Kind-PL-ACC</ta>
            <ta e="T185" id="Seg_3544" s="T184">treffen-EP-PST2.[3SG]</ta>
            <ta e="T186" id="Seg_3545" s="T185">Kind-PL.[NOM]</ta>
            <ta e="T187" id="Seg_3546" s="T186">1SG.[NOM]</ta>
            <ta e="T188" id="Seg_3547" s="T187">Schlitten-1SG-DAT/LOC</ta>
            <ta e="T189" id="Seg_3548" s="T188">sich.nähern-APPR-2PL</ta>
            <ta e="T190" id="Seg_3549" s="T189">Schlitten-1SG-DAT/LOC</ta>
            <ta e="T191" id="Seg_3550" s="T190">Alte-EP-1SG.[NOM]</ta>
            <ta e="T192" id="Seg_3551" s="T191">es.gibt</ta>
            <ta e="T193" id="Seg_3552" s="T192">Mensch.[NOM]</ta>
            <ta e="T194" id="Seg_3553" s="T193">sich.nähern-PST1-3SG</ta>
            <ta e="T195" id="Seg_3554" s="T194">und</ta>
            <ta e="T196" id="Seg_3555" s="T195">erschrecken-CVB.SEQ</ta>
            <ta e="T197" id="Seg_3556" s="T196">sterben-CVB.SEQ</ta>
            <ta e="T198" id="Seg_3557" s="T197">bleiben-FUT-3SG</ta>
            <ta e="T199" id="Seg_3558" s="T198">sagen-PST2.[3SG]</ta>
            <ta e="T200" id="Seg_3559" s="T199">Kaamyylaak.[NOM]</ta>
            <ta e="T201" id="Seg_3560" s="T200">Tee.[NOM]</ta>
            <ta e="T202" id="Seg_3561" s="T201">trinken-CVB.SIM</ta>
            <ta e="T203" id="Seg_3562" s="T202">sitzen-TEMP-3SG</ta>
            <ta e="T204" id="Seg_3563" s="T203">Kind-PL.[NOM]</ta>
            <ta e="T205" id="Seg_3564" s="T204">laufen-CVB.SEQ</ta>
            <ta e="T206" id="Seg_3565" s="T205">hineingehen-PST2-3PL</ta>
            <ta e="T207" id="Seg_3566" s="T206">Kaamyylaak.[NOM]</ta>
            <ta e="T208" id="Seg_3567" s="T207">Alte-EP-2SG.[NOM]</ta>
            <ta e="T209" id="Seg_3568" s="T208">sterben-CVB.SEQ</ta>
            <ta e="T210" id="Seg_3569" s="T209">bleiben-PST2.[3SG]</ta>
            <ta e="T211" id="Seg_3570" s="T210">sagen-PST2-3PL</ta>
            <ta e="T212" id="Seg_3571" s="T211">oh.nein</ta>
            <ta e="T213" id="Seg_3572" s="T212">jetzt</ta>
            <ta e="T214" id="Seg_3573" s="T213">EMPH</ta>
            <ta e="T215" id="Seg_3574" s="T214">tatsächlich</ta>
            <ta e="T216" id="Seg_3575" s="T215">sterben-CVB.SIM-1SG</ta>
            <ta e="T217" id="Seg_3576" s="T216">sein-PST2.[3SG]</ta>
            <ta e="T218" id="Seg_3577" s="T217">Alte-POSS</ta>
            <ta e="T219" id="Seg_3578" s="T218">NEG</ta>
            <ta e="T220" id="Seg_3579" s="T219">bleiben-PST1-1SG</ta>
            <ta e="T221" id="Seg_3580" s="T220">dieses</ta>
            <ta e="T222" id="Seg_3581" s="T221">Kind-PL.[NOM]</ta>
            <ta e="T223" id="Seg_3582" s="T222">Schlitten-DAT/LOC</ta>
            <ta e="T224" id="Seg_3583" s="T223">sich.nähern-CVB.SEQ-3PL</ta>
            <ta e="T225" id="Seg_3584" s="T224">Kaamyylaak.[NOM]</ta>
            <ta e="T226" id="Seg_3585" s="T225">schreien-PST2.[3SG]</ta>
            <ta e="T227" id="Seg_3586" s="T226">warum</ta>
            <ta e="T228" id="Seg_3587" s="T227">sterben-CVB.PURP</ta>
            <ta e="T229" id="Seg_3588" s="T228">machen-PRS-2SG</ta>
            <ta e="T230" id="Seg_3589" s="T229">na</ta>
            <ta e="T231" id="Seg_3590" s="T230">nehmen.[IMP.2SG]</ta>
            <ta e="T232" id="Seg_3591" s="T231">dieses</ta>
            <ta e="T233" id="Seg_3592" s="T232">zwei</ta>
            <ta e="T234" id="Seg_3593" s="T233">Mädchen-1PL-ACC</ta>
            <ta e="T235" id="Seg_3594" s="T234">sagen-EP-RECP/COLL-PST2-3PL</ta>
            <ta e="T236" id="Seg_3595" s="T235">Haus-PROPR-PL.[NOM]</ta>
            <ta e="T237" id="Seg_3596" s="T236">Kaamyylaak.[NOM]</ta>
            <ta e="T238" id="Seg_3597" s="T237">so</ta>
            <ta e="T239" id="Seg_3598" s="T238">zwei</ta>
            <ta e="T240" id="Seg_3599" s="T239">Mädchen-VBZ-PST2.[3SG]</ta>
            <ta e="T241" id="Seg_3600" s="T240">Mädchen-PL-3SG-ACC</ta>
            <ta e="T242" id="Seg_3601" s="T241">einladen-CVB.SEQ</ta>
            <ta e="T243" id="Seg_3602" s="T242">nachdem</ta>
            <ta e="T244" id="Seg_3603" s="T243">Kaamyylaak.[NOM]</ta>
            <ta e="T245" id="Seg_3604" s="T244">gehen-CVB.SEQ</ta>
            <ta e="T246" id="Seg_3605" s="T245">gehen-PST2.[3SG]</ta>
            <ta e="T247" id="Seg_3606" s="T246">doch</ta>
            <ta e="T248" id="Seg_3607" s="T247">gehen-CVB.SEQ</ta>
            <ta e="T249" id="Seg_3608" s="T248">fahren-CVB.SEQ-fahren-CVB.SEQ</ta>
            <ta e="T250" id="Seg_3609" s="T249">eins</ta>
            <ta e="T251" id="Seg_3610" s="T250">reich.[NOM]</ta>
            <ta e="T252" id="Seg_3611" s="T251">Veranda-3SG-ACC</ta>
            <ta e="T253" id="Seg_3612" s="T252">sehen-PST2.[3SG]</ta>
            <ta e="T254" id="Seg_3613" s="T253">jenes-ACC</ta>
            <ta e="T255" id="Seg_3614" s="T254">sehen-CVB.SEQ</ta>
            <ta e="T256" id="Seg_3615" s="T255">Kaamyylaak.[NOM]</ta>
            <ta e="T257" id="Seg_3616" s="T256">Mädchen-PL-3SG-DAT/LOC</ta>
            <ta e="T258" id="Seg_3617" s="T257">sprechen-PST2.[3SG]</ta>
            <ta e="T259" id="Seg_3618" s="T258">na</ta>
            <ta e="T260" id="Seg_3619" s="T259">Mädchen-PL.[NOM]</ta>
            <ta e="T261" id="Seg_3620" s="T260">gehen-EP-IMP.2PL</ta>
            <ta e="T262" id="Seg_3621" s="T261">hinausgehen-EP-IMP.2PL</ta>
            <ta e="T263" id="Seg_3622" s="T262">Lärche.[NOM]</ta>
            <ta e="T264" id="Seg_3623" s="T263">Baum-DAT/LOC</ta>
            <ta e="T265" id="Seg_3624" s="T264">sich.verstecken-CVB.SEQ</ta>
            <ta e="T266" id="Seg_3625" s="T265">sitzen-EP-IMP.2PL</ta>
            <ta e="T267" id="Seg_3626" s="T266">1SG.[NOM]</ta>
            <ta e="T268" id="Seg_3627" s="T267">reich-DAT/LOC</ta>
            <ta e="T269" id="Seg_3628" s="T268">gehen-FUT-1SG</ta>
            <ta e="T270" id="Seg_3629" s="T269">3SG-ACC</ta>
            <ta e="T271" id="Seg_3630" s="T270">hierher</ta>
            <ta e="T272" id="Seg_3631" s="T271">bringen-FUT-1SG</ta>
            <ta e="T273" id="Seg_3632" s="T272">reich.[NOM]</ta>
            <ta e="T274" id="Seg_3633" s="T273">wessen</ta>
            <ta e="T275" id="Seg_3634" s="T274">Reichtum-3SG.[NOM]=Q</ta>
            <ta e="T276" id="Seg_3635" s="T275">sagen-CVB.SIM-sagen-CVB.SIM</ta>
            <ta e="T277" id="Seg_3636" s="T276">fragen-FUT.[3SG]</ta>
            <ta e="T278" id="Seg_3637" s="T277">jenes-DAT/LOC</ta>
            <ta e="T279" id="Seg_3638" s="T278">2PL.[NOM]</ta>
            <ta e="T280" id="Seg_3639" s="T279">sagen-FUT-EP-IMP.2PL</ta>
            <ta e="T281" id="Seg_3640" s="T280">Kaamyylaak.[NOM]</ta>
            <ta e="T282" id="Seg_3641" s="T281">Reichtum-3SG.[NOM]</ta>
            <ta e="T283" id="Seg_3642" s="T282">jenes-ACC</ta>
            <ta e="T284" id="Seg_3643" s="T283">sprechen-CVB.SEQ</ta>
            <ta e="T285" id="Seg_3644" s="T284">nachdem</ta>
            <ta e="T286" id="Seg_3645" s="T285">Kaamyylaak.[NOM]</ta>
            <ta e="T287" id="Seg_3646" s="T286">reich-DAT/LOC</ta>
            <ta e="T288" id="Seg_3647" s="T287">ankommen-PST2.[3SG]</ta>
            <ta e="T289" id="Seg_3648" s="T288">Tee.[NOM]</ta>
            <ta e="T290" id="Seg_3649" s="T289">gehen-CVB.SEQ</ta>
            <ta e="T291" id="Seg_3650" s="T290">aufhören-CVB.SEQ</ta>
            <ta e="T292" id="Seg_3651" s="T291">sagen-PST2.[3SG]</ta>
            <ta e="T293" id="Seg_3652" s="T292">na</ta>
            <ta e="T294" id="Seg_3653" s="T293">Freund</ta>
            <ta e="T295" id="Seg_3654" s="T294">wer-1PL.[NOM]</ta>
            <ta e="T296" id="Seg_3655" s="T295">Reichtum-3SG.[NOM]=Q</ta>
            <ta e="T297" id="Seg_3656" s="T296">dieses</ta>
            <ta e="T298" id="Seg_3657" s="T297">dort</ta>
            <ta e="T299" id="Seg_3658" s="T298">reich.[NOM]</ta>
            <ta e="T300" id="Seg_3659" s="T299">erschrecken-CVB.SIM</ta>
            <ta e="T301" id="Seg_3660" s="T300">schlagen-CVB.SEQ</ta>
            <ta e="T302" id="Seg_3661" s="T301">nachdem</ta>
            <ta e="T303" id="Seg_3662" s="T302">sprechen-PST2.[3SG]</ta>
            <ta e="T304" id="Seg_3663" s="T303">wessen</ta>
            <ta e="T305" id="Seg_3664" s="T304">sein-FUT.[3SG]=Q</ta>
            <ta e="T306" id="Seg_3665" s="T305">mein</ta>
            <ta e="T307" id="Seg_3666" s="T306">sein-EP-NEG.CVB</ta>
            <ta e="T308" id="Seg_3667" s="T307">Kaamyylaak.[NOM]</ta>
            <ta e="T309" id="Seg_3668" s="T308">nein</ta>
            <ta e="T310" id="Seg_3669" s="T309">mein</ta>
            <ta e="T311" id="Seg_3670" s="T310">glauben-NEG.PTCP</ta>
            <ta e="T312" id="Seg_3671" s="T311">sein-TEMP-2SG</ta>
            <ta e="T313" id="Seg_3672" s="T312">gehen-IMP.1DU</ta>
            <ta e="T314" id="Seg_3673" s="T313">Himmel-ABL</ta>
            <ta e="T315" id="Seg_3674" s="T314">fragen-CVB.SIM</ta>
            <ta e="T316" id="Seg_3675" s="T315">sagen-PST2.[3SG]</ta>
            <ta e="T317" id="Seg_3676" s="T316">gehen-PST2-3PL</ta>
            <ta e="T318" id="Seg_3677" s="T317">Mädchen-PL.[NOM]</ta>
            <ta e="T319" id="Seg_3678" s="T318">liegen-PTCP.PRS</ta>
            <ta e="T320" id="Seg_3679" s="T319">Ort-3PL-DAT/LOC</ta>
            <ta e="T321" id="Seg_3680" s="T320">kommen-CVB.SEQ</ta>
            <ta e="T322" id="Seg_3681" s="T321">nachdem</ta>
            <ta e="T323" id="Seg_3682" s="T322">Kaamyylaak.[NOM]</ta>
            <ta e="T324" id="Seg_3683" s="T323">schreien-PST2.[3SG]</ta>
            <ta e="T325" id="Seg_3684" s="T324">dieses.[NOM]</ta>
            <ta e="T326" id="Seg_3685" s="T325">wer.[NOM]</ta>
            <ta e="T327" id="Seg_3686" s="T326">Reichtum-3SG.[NOM]=Q</ta>
            <ta e="T328" id="Seg_3687" s="T327">oben-ABL</ta>
            <ta e="T329" id="Seg_3688" s="T328">Mädchen-PL.[NOM]</ta>
            <ta e="T330" id="Seg_3689" s="T329">schreien-PST2-3PL</ta>
            <ta e="T331" id="Seg_3690" s="T330">Kaamyylaak.[NOM]</ta>
            <ta e="T332" id="Seg_3691" s="T331">Reichtum-3SG.[NOM]</ta>
            <ta e="T333" id="Seg_3692" s="T332">Kaamyylaak.[NOM]</ta>
            <ta e="T334" id="Seg_3693" s="T333">Reichtum-3SG.[NOM]</ta>
            <ta e="T335" id="Seg_3694" s="T334">hören-PST1-2SG</ta>
            <ta e="T336" id="Seg_3695" s="T335">Q</ta>
            <ta e="T337" id="Seg_3696" s="T336">sagen-PST2.[3SG]</ta>
            <ta e="T338" id="Seg_3697" s="T337">Kaamyylaak.[NOM]</ta>
            <ta e="T339" id="Seg_3698" s="T338">wer.[NOM]</ta>
            <ta e="T340" id="Seg_3699" s="T339">Reichtum-3SG.[NOM]</ta>
            <ta e="T341" id="Seg_3700" s="T340">sein-PST2.[3SG]=Q</ta>
            <ta e="T342" id="Seg_3701" s="T341">Kaamyylaak.[NOM]</ta>
            <ta e="T343" id="Seg_3702" s="T342">dieses</ta>
            <ta e="T344" id="Seg_3703" s="T343">reich-ACC</ta>
            <ta e="T345" id="Seg_3704" s="T344">folgen-CVB.SEQ</ta>
            <ta e="T346" id="Seg_3705" s="T345">schicken-PST2.[3SG]</ta>
            <ta e="T347" id="Seg_3706" s="T346">Reichtum-3SG-ACC</ta>
            <ta e="T348" id="Seg_3707" s="T347">Reichtum.[NOM]</ta>
            <ta e="T349" id="Seg_3708" s="T348">machen-PST2.[3SG]</ta>
            <ta e="T350" id="Seg_3709" s="T349">reich.sein-CVB.SEQ-sich.satt.essen-CVB.SEQ</ta>
            <ta e="T351" id="Seg_3710" s="T350">leben-PST2.[3SG]</ta>
            <ta e="T352" id="Seg_3711" s="T351">letzter-3SG.[NOM]</ta>
            <ta e="T353" id="Seg_3712" s="T352">aufhören-PST1-3SG</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_3713" s="T0">Каамыылаак.[NOM]</ta>
            <ta e="T2" id="Seg_3714" s="T1">идти-CVB.SEQ</ta>
            <ta e="T3" id="Seg_3715" s="T2">идти-PST2.[3SG]</ta>
            <ta e="T4" id="Seg_3716" s="T3">идти-CVB.SEQ</ta>
            <ta e="T5" id="Seg_3717" s="T4">идти-CVB.SEQ-идти-CVB.SEQ</ta>
            <ta e="T6" id="Seg_3718" s="T5">стоянка-DAT/LOC</ta>
            <ta e="T7" id="Seg_3719" s="T6">приходить-PST2.[3SG]</ta>
            <ta e="T8" id="Seg_3720" s="T7">лед-DAT/LOC</ta>
            <ta e="T9" id="Seg_3721" s="T8">мерзнуть-PTCP.PST</ta>
            <ta e="T10" id="Seg_3722" s="T9">кровь-ACC</ta>
            <ta e="T11" id="Seg_3723" s="T10">видеть-PST2.[3SG]</ta>
            <ta e="T12" id="Seg_3724" s="T11">олень.[NOM]</ta>
            <ta e="T13" id="Seg_3725" s="T12">передняя.нога-3SG-ACC</ta>
            <ta e="T14" id="Seg_3726" s="T13">найти-PST1-1SG</ta>
            <ta e="T15" id="Seg_3727" s="T14">говорить-CVB.SIM-говорить-CVB.SIM</ta>
            <ta e="T16" id="Seg_3728" s="T15">сумка-3SG-DAT/LOC</ta>
            <ta e="T17" id="Seg_3729" s="T16">вкладывать-CVB.SEQ</ta>
            <ta e="T18" id="Seg_3730" s="T17">бросать-PST2.[3SG]</ta>
            <ta e="T19" id="Seg_3731" s="T18">так</ta>
            <ta e="T20" id="Seg_3732" s="T19">делать-CVB.SEQ</ta>
            <ta e="T21" id="Seg_3733" s="T20">после</ta>
            <ta e="T22" id="Seg_3734" s="T21">идти-CVB.SEQ</ta>
            <ta e="T23" id="Seg_3735" s="T22">идти-PST2.[3SG]</ta>
            <ta e="T24" id="Seg_3736" s="T23">идти-CVB.SEQ-идти-CVB.SEQ</ta>
            <ta e="T25" id="Seg_3737" s="T24">житель-PL-DAT/LOC</ta>
            <ta e="T26" id="Seg_3738" s="T25">приходить-PST2.[3SG]</ta>
            <ta e="T27" id="Seg_3739" s="T26">житель-PL-DAT/LOC</ta>
            <ta e="T28" id="Seg_3740" s="T27">варить-CVB.PURP</ta>
            <ta e="T29" id="Seg_3741" s="T28">мясо-3PL-ACC</ta>
            <ta e="T30" id="Seg_3742" s="T29">мясо-VBZ-PST2-3PL</ta>
            <ta e="T31" id="Seg_3743" s="T30">эй</ta>
            <ta e="T32" id="Seg_3744" s="T31">гость-1PL-DAT/LOC</ta>
            <ta e="T33" id="Seg_3745" s="T32">мясо.[NOM]</ta>
            <ta e="T34" id="Seg_3746" s="T33">хватать-NEG.PTCP</ta>
            <ta e="T35" id="Seg_3747" s="T34">быть-PST2.[3SG]</ta>
            <ta e="T36" id="Seg_3748" s="T35">говорить-EP-RECP/COLL-PST2-3PL</ta>
            <ta e="T37" id="Seg_3749" s="T36">тот-DAT/LOC</ta>
            <ta e="T38" id="Seg_3750" s="T37">Каамыылаак.[NOM]</ta>
            <ta e="T39" id="Seg_3751" s="T38">говорить-PST2.[3SG]</ta>
            <ta e="T40" id="Seg_3752" s="T39">эй</ta>
            <ta e="T41" id="Seg_3753" s="T40">что.[NOM]</ta>
            <ta e="T42" id="Seg_3754" s="T41">быть-PRS-2PL</ta>
            <ta e="T43" id="Seg_3755" s="T42">1SG.[NOM]</ta>
            <ta e="T44" id="Seg_3756" s="T43">сумка-1SG-DAT/LOC</ta>
            <ta e="T45" id="Seg_3757" s="T44">мясо.[NOM]</ta>
            <ta e="T46" id="Seg_3758" s="T45">есть</ta>
            <ta e="T47" id="Seg_3759" s="T46">быть-PST1-3SG</ta>
            <ta e="T48" id="Seg_3760" s="T47">тот-ACC</ta>
            <ta e="T49" id="Seg_3761" s="T48">внести-CVB.SEQ</ta>
            <ta e="T50" id="Seg_3762" s="T49">варить-IMP.2PL</ta>
            <ta e="T51" id="Seg_3763" s="T50">вот</ta>
            <ta e="T52" id="Seg_3764" s="T51">потом</ta>
            <ta e="T53" id="Seg_3765" s="T52">житель-PL.[NOM]</ta>
            <ta e="T54" id="Seg_3766" s="T53">котел-3PL-DAT/LOC</ta>
            <ta e="T55" id="Seg_3767" s="T54">Каамыылаак.[NOM]</ta>
            <ta e="T56" id="Seg_3768" s="T55">мясо-3SG-ACC</ta>
            <ta e="T57" id="Seg_3769" s="T56">вставлять-CVB.SEQ</ta>
            <ta e="T58" id="Seg_3770" s="T57">бросать-PST2-3PL</ta>
            <ta e="T59" id="Seg_3771" s="T58">вечер-INSTR</ta>
            <ta e="T60" id="Seg_3772" s="T59">вот</ta>
            <ta e="T61" id="Seg_3773" s="T60">мясо-3PL-ACC</ta>
            <ta e="T62" id="Seg_3774" s="T61">выкладывать-PRS-3PL</ta>
            <ta e="T63" id="Seg_3775" s="T62">выкладывать-PTCP.PST-3PL-DAT/LOC</ta>
            <ta e="T64" id="Seg_3776" s="T63">гость-3PL-DAT/LOC</ta>
            <ta e="T65" id="Seg_3777" s="T64">мясо-3PL.[NOM]</ta>
            <ta e="T66" id="Seg_3778" s="T65">опять</ta>
            <ta e="T67" id="Seg_3779" s="T66">хватать-PST2.NEG.[3SG]</ta>
            <ta e="T68" id="Seg_3780" s="T67">Каамыылаак.[NOM]</ta>
            <ta e="T69" id="Seg_3781" s="T68">эй</ta>
            <ta e="T70" id="Seg_3782" s="T69">где=Q</ta>
            <ta e="T71" id="Seg_3783" s="T70">1SG.[NOM]</ta>
            <ta e="T72" id="Seg_3784" s="T71">мясо-EP-1SG.[NOM]</ta>
            <ta e="T73" id="Seg_3785" s="T72">говорить-PST2.[3SG]</ta>
            <ta e="T74" id="Seg_3786" s="T73">умирать-PST1-1SG-умирать-PST1-1SG</ta>
            <ta e="T75" id="Seg_3787" s="T74">как</ta>
            <ta e="T76" id="Seg_3788" s="T75">есть-NEG.CVB.SIM</ta>
            <ta e="T77" id="Seg_3789" s="T76">уснуть-FUT-1SG=Q</ta>
            <ta e="T78" id="Seg_3790" s="T77">ой</ta>
            <ta e="T79" id="Seg_3791" s="T78">умирать-EP-NEG.[IMP.2SG]</ta>
            <ta e="T80" id="Seg_3792" s="T79">только</ta>
            <ta e="T81" id="Seg_3793" s="T80">1PL.[NOM]</ta>
            <ta e="T82" id="Seg_3794" s="T81">2SG-DAT/LOC</ta>
            <ta e="T83" id="Seg_3795" s="T82">целый</ta>
            <ta e="T84" id="Seg_3796" s="T83">теленок-PART</ta>
            <ta e="T85" id="Seg_3797" s="T84">давать-PRS-1PL</ta>
            <ta e="T86" id="Seg_3798" s="T85">говорить-PST2-3PL</ta>
            <ta e="T87" id="Seg_3799" s="T86">житель-PL.[NOM]</ta>
            <ta e="T88" id="Seg_3800" s="T87">есть-CVB.SEQ</ta>
            <ta e="T89" id="Seg_3801" s="T88">после</ta>
            <ta e="T90" id="Seg_3802" s="T89">утуй-PST2-3PL</ta>
            <ta e="T91" id="Seg_3803" s="T90">олененок-3PL-ACC</ta>
            <ta e="T92" id="Seg_3804" s="T91">видеть-CAUS-PST2-3PL</ta>
            <ta e="T93" id="Seg_3805" s="T92">Каамыылаак-DAT/LOC</ta>
            <ta e="T94" id="Seg_3806" s="T93">Каамыылаак.[NOM]</ta>
            <ta e="T95" id="Seg_3807" s="T94">ночью</ta>
            <ta e="T96" id="Seg_3808" s="T95">выйти-CVB.SEQ</ta>
            <ta e="T97" id="Seg_3809" s="T96">олененок-3SG.[NOM]</ta>
            <ta e="T98" id="Seg_3810" s="T97">EMPH</ta>
            <ta e="T99" id="Seg_3811" s="T98">приходить-PTCP.PST-3SG-ACC</ta>
            <ta e="T100" id="Seg_3812" s="T99">хватать-CVB.SEQ</ta>
            <ta e="T101" id="Seg_3813" s="T100">взять-PST2.[3SG]</ta>
            <ta e="T102" id="Seg_3814" s="T101">так</ta>
            <ta e="T103" id="Seg_3815" s="T102">делать-CVB.SEQ</ta>
            <ta e="T104" id="Seg_3816" s="T103">после</ta>
            <ta e="T105" id="Seg_3817" s="T104">два</ta>
            <ta e="T106" id="Seg_3818" s="T105">лучший</ta>
            <ta e="T107" id="Seg_3819" s="T106">олений.бык.[NOM]</ta>
            <ta e="T108" id="Seg_3820" s="T107">рог-3PL-DAT/LOC</ta>
            <ta e="T109" id="Seg_3821" s="T108">навешивать-CVB.SEQ</ta>
            <ta e="T110" id="Seg_3822" s="T109">бросать-PST2.[3SG]</ta>
            <ta e="T111" id="Seg_3823" s="T110">входить-CVB.SEQ</ta>
            <ta e="T112" id="Seg_3824" s="T111">уснуть-CVB.SEQ</ta>
            <ta e="T113" id="Seg_3825" s="T112">оставаться-PST2.[3SG]</ta>
            <ta e="T114" id="Seg_3826" s="T113">утром</ta>
            <ta e="T115" id="Seg_3827" s="T114">сосед-PL.[NOM]</ta>
            <ta e="T116" id="Seg_3828" s="T115">видеть-PST2-3PL</ta>
            <ta e="T117" id="Seg_3829" s="T116">Каамыылаак.[NOM]</ta>
            <ta e="T118" id="Seg_3830" s="T117">олененок-3SG-ACC</ta>
            <ta e="T119" id="Seg_3831" s="T118">два</ta>
            <ta e="T120" id="Seg_3832" s="T119">лучший</ta>
            <ta e="T121" id="Seg_3833" s="T120">олений.бык-3PL.[NOM]</ta>
            <ta e="T122" id="Seg_3834" s="T121">рог-3PL-DAT/LOC</ta>
            <ta e="T123" id="Seg_3835" s="T122">накидывать-CVB.SIM</ta>
            <ta e="T124" id="Seg_3836" s="T123">идти-PRS-3PL</ta>
            <ta e="T125" id="Seg_3837" s="T124">Каамыылаак.[NOM]</ta>
            <ta e="T126" id="Seg_3838" s="T125">тот-ACC</ta>
            <ta e="T127" id="Seg_3839" s="T126">слышать-PST1-3SG</ta>
            <ta e="T128" id="Seg_3840" s="T127">EMPH</ta>
            <ta e="T129" id="Seg_3841" s="T128">вот</ta>
            <ta e="T130" id="Seg_3842" s="T129">вправду</ta>
            <ta e="T131" id="Seg_3843" s="T130">умирать-PRS-1SG</ta>
            <ta e="T132" id="Seg_3844" s="T131">быть-PST2.[3SG]</ta>
            <ta e="T133" id="Seg_3845" s="T132">олененок-POSS</ta>
            <ta e="T134" id="Seg_3846" s="T133">NEG</ta>
            <ta e="T135" id="Seg_3847" s="T134">оставаться-PST1-1SG</ta>
            <ta e="T136" id="Seg_3848" s="T135">говорить-CVB.SEQ</ta>
            <ta e="T137" id="Seg_3849" s="T136">кричать-PST2.[3SG]</ta>
            <ta e="T138" id="Seg_3850" s="T137">эй</ta>
            <ta e="T139" id="Seg_3851" s="T138">умирать-EP-NEG.[IMP.2SG]</ta>
            <ta e="T140" id="Seg_3852" s="T139">только</ta>
            <ta e="T141" id="Seg_3853" s="T140">1PL.[NOM]</ta>
            <ta e="T142" id="Seg_3854" s="T141">2SG-DAT/LOC</ta>
            <ta e="T143" id="Seg_3855" s="T142">тот</ta>
            <ta e="T144" id="Seg_3856" s="T143">два</ta>
            <ta e="T145" id="Seg_3857" s="T144">олений.бык-ACC</ta>
            <ta e="T146" id="Seg_3858" s="T145">давать-FUT-1PL</ta>
            <ta e="T147" id="Seg_3859" s="T146">говорить-PST2-3PL</ta>
            <ta e="T148" id="Seg_3860" s="T147">дом-ADJZ-3PL.[NOM]</ta>
            <ta e="T149" id="Seg_3861" s="T148">Каамыылаак.[NOM]</ta>
            <ta e="T150" id="Seg_3862" s="T149">два</ta>
            <ta e="T151" id="Seg_3863" s="T150">олений.бык-VBZ-PST2.[3SG]</ta>
            <ta e="T152" id="Seg_3864" s="T151">два</ta>
            <ta e="T153" id="Seg_3865" s="T152">олений.бык-3SG-ACC</ta>
            <ta e="T154" id="Seg_3866" s="T153">запрячь-CVB.SEQ</ta>
            <ta e="T155" id="Seg_3867" s="T154">после</ta>
            <ta e="T156" id="Seg_3868" s="T155">Каамыылаак.[NOM]</ta>
            <ta e="T157" id="Seg_3869" s="T156">сани-VBZ-CVB.SIM</ta>
            <ta e="T158" id="Seg_3870" s="T157">идти-PST2.[3SG]</ta>
            <ta e="T159" id="Seg_3871" s="T158">ехать-CVB.SEQ-ехать-CVB.SEQ</ta>
            <ta e="T160" id="Seg_3872" s="T159">могила-PL-DAT/LOC</ta>
            <ta e="T161" id="Seg_3873" s="T160">толкать-PST2.[3SG]</ta>
            <ta e="T162" id="Seg_3874" s="T161">там</ta>
            <ta e="T163" id="Seg_3875" s="T162">один</ta>
            <ta e="T164" id="Seg_3876" s="T163">старуха-ACC</ta>
            <ta e="T165" id="Seg_3877" s="T164">деревянный.ящик-3SG-ACC</ta>
            <ta e="T166" id="Seg_3878" s="T165">открывать-CVB.SEQ</ta>
            <ta e="T167" id="Seg_3879" s="T166">вынимать-CVB.SEQ</ta>
            <ta e="T168" id="Seg_3880" s="T167">взять-PST2.[3SG]</ta>
            <ta e="T169" id="Seg_3881" s="T168">сани-3SG-DAT/LOC</ta>
            <ta e="T170" id="Seg_3882" s="T169">класть-CVB.SEQ</ta>
            <ta e="T171" id="Seg_3883" s="T170">бросать-PST2.[3SG]</ta>
            <ta e="T172" id="Seg_3884" s="T171">идти-CVB.SEQ</ta>
            <ta e="T173" id="Seg_3885" s="T172">идти-PST2.[3SG]</ta>
            <ta e="T174" id="Seg_3886" s="T173">идти-CVB.SEQ-идти-CVB.SEQ</ta>
            <ta e="T175" id="Seg_3887" s="T174">один</ta>
            <ta e="T176" id="Seg_3888" s="T175">житель-DAT/LOC</ta>
            <ta e="T177" id="Seg_3889" s="T176">доезжать-PST2.[3SG]</ta>
            <ta e="T178" id="Seg_3890" s="T177">сани-3SG-ACC</ta>
            <ta e="T179" id="Seg_3891" s="T178">житель-PL-ABL</ta>
            <ta e="T180" id="Seg_3892" s="T179">далеко-INTNS</ta>
            <ta e="T181" id="Seg_3893" s="T180">оставлять-PST2.[3SG]</ta>
            <ta e="T182" id="Seg_3894" s="T181">Каамыылаак.[NOM]</ta>
            <ta e="T183" id="Seg_3895" s="T182">на.улице</ta>
            <ta e="T184" id="Seg_3896" s="T183">ребенок-PL-ACC</ta>
            <ta e="T185" id="Seg_3897" s="T184">встречать-EP-PST2.[3SG]</ta>
            <ta e="T186" id="Seg_3898" s="T185">ребенок-PL.[NOM]</ta>
            <ta e="T187" id="Seg_3899" s="T186">1SG.[NOM]</ta>
            <ta e="T188" id="Seg_3900" s="T187">сани-1SG-DAT/LOC</ta>
            <ta e="T189" id="Seg_3901" s="T188">приближаться-APPR-2PL</ta>
            <ta e="T190" id="Seg_3902" s="T189">сани-1SG-DAT/LOC</ta>
            <ta e="T191" id="Seg_3903" s="T190">старуха-EP-1SG.[NOM]</ta>
            <ta e="T192" id="Seg_3904" s="T191">есть</ta>
            <ta e="T193" id="Seg_3905" s="T192">человек.[NOM]</ta>
            <ta e="T194" id="Seg_3906" s="T193">приближаться-PST1-3SG</ta>
            <ta e="T195" id="Seg_3907" s="T194">да</ta>
            <ta e="T196" id="Seg_3908" s="T195">испугаться-CVB.SEQ</ta>
            <ta e="T197" id="Seg_3909" s="T196">умирать-CVB.SEQ</ta>
            <ta e="T198" id="Seg_3910" s="T197">оставаться-FUT-3SG</ta>
            <ta e="T199" id="Seg_3911" s="T198">говорить-PST2.[3SG]</ta>
            <ta e="T200" id="Seg_3912" s="T199">Каамыылаак.[NOM]</ta>
            <ta e="T201" id="Seg_3913" s="T200">чай.[NOM]</ta>
            <ta e="T202" id="Seg_3914" s="T201">пить-CVB.SIM</ta>
            <ta e="T203" id="Seg_3915" s="T202">сидеть-TEMP-3SG</ta>
            <ta e="T204" id="Seg_3916" s="T203">ребенок-PL.[NOM]</ta>
            <ta e="T205" id="Seg_3917" s="T204">бегать-CVB.SEQ</ta>
            <ta e="T206" id="Seg_3918" s="T205">входить-PST2-3PL</ta>
            <ta e="T207" id="Seg_3919" s="T206">Каамыылаак.[NOM]</ta>
            <ta e="T208" id="Seg_3920" s="T207">старуха-EP-2SG.[NOM]</ta>
            <ta e="T209" id="Seg_3921" s="T208">умирать-CVB.SEQ</ta>
            <ta e="T210" id="Seg_3922" s="T209">оставаться-PST2.[3SG]</ta>
            <ta e="T211" id="Seg_3923" s="T210">говорить-PST2-3PL</ta>
            <ta e="T212" id="Seg_3924" s="T211">вот.беда</ta>
            <ta e="T213" id="Seg_3925" s="T212">теперь</ta>
            <ta e="T214" id="Seg_3926" s="T213">EMPH</ta>
            <ta e="T215" id="Seg_3927" s="T214">вправду</ta>
            <ta e="T216" id="Seg_3928" s="T215">умирать-CVB.SIM-1SG</ta>
            <ta e="T217" id="Seg_3929" s="T216">быть-PST2.[3SG]</ta>
            <ta e="T218" id="Seg_3930" s="T217">старуха-POSS</ta>
            <ta e="T219" id="Seg_3931" s="T218">NEG</ta>
            <ta e="T220" id="Seg_3932" s="T219">оставаться-PST1-1SG</ta>
            <ta e="T221" id="Seg_3933" s="T220">тот</ta>
            <ta e="T222" id="Seg_3934" s="T221">ребенок-PL.[NOM]</ta>
            <ta e="T223" id="Seg_3935" s="T222">сани-DAT/LOC</ta>
            <ta e="T224" id="Seg_3936" s="T223">приближаться-CVB.SEQ-3PL</ta>
            <ta e="T225" id="Seg_3937" s="T224">Каамыылаак.[NOM]</ta>
            <ta e="T226" id="Seg_3938" s="T225">кричать-PST2.[3SG]</ta>
            <ta e="T227" id="Seg_3939" s="T226">почему</ta>
            <ta e="T228" id="Seg_3940" s="T227">умирать-CVB.PURP</ta>
            <ta e="T229" id="Seg_3941" s="T228">делать-PRS-2SG</ta>
            <ta e="T230" id="Seg_3942" s="T229">эй</ta>
            <ta e="T231" id="Seg_3943" s="T230">взять.[IMP.2SG]</ta>
            <ta e="T232" id="Seg_3944" s="T231">тот</ta>
            <ta e="T233" id="Seg_3945" s="T232">два</ta>
            <ta e="T234" id="Seg_3946" s="T233">девушка-1PL-ACC</ta>
            <ta e="T235" id="Seg_3947" s="T234">говорить-EP-RECP/COLL-PST2-3PL</ta>
            <ta e="T236" id="Seg_3948" s="T235">дом-PROPR-PL.[NOM]</ta>
            <ta e="T237" id="Seg_3949" s="T236">Каамыылаак.[NOM]</ta>
            <ta e="T238" id="Seg_3950" s="T237">так</ta>
            <ta e="T239" id="Seg_3951" s="T238">два</ta>
            <ta e="T240" id="Seg_3952" s="T239">девушка-VBZ-PST2.[3SG]</ta>
            <ta e="T241" id="Seg_3953" s="T240">девушка-PL-3SG-ACC</ta>
            <ta e="T242" id="Seg_3954" s="T241">грузить-CVB.SEQ</ta>
            <ta e="T243" id="Seg_3955" s="T242">после</ta>
            <ta e="T244" id="Seg_3956" s="T243">Каамыылаак.[NOM]</ta>
            <ta e="T245" id="Seg_3957" s="T244">идти-CVB.SEQ</ta>
            <ta e="T246" id="Seg_3958" s="T245">идти-PST2.[3SG]</ta>
            <ta e="T247" id="Seg_3959" s="T246">вот</ta>
            <ta e="T248" id="Seg_3960" s="T247">идти-CVB.SEQ</ta>
            <ta e="T249" id="Seg_3961" s="T248">ехать-CVB.SEQ-ехать-CVB.SEQ</ta>
            <ta e="T250" id="Seg_3962" s="T249">один</ta>
            <ta e="T251" id="Seg_3963" s="T250">богатый.[NOM]</ta>
            <ta e="T252" id="Seg_3964" s="T251">крыльцо-3SG-ACC</ta>
            <ta e="T253" id="Seg_3965" s="T252">видеть-PST2.[3SG]</ta>
            <ta e="T254" id="Seg_3966" s="T253">тот-ACC</ta>
            <ta e="T255" id="Seg_3967" s="T254">видеть-CVB.SEQ</ta>
            <ta e="T256" id="Seg_3968" s="T255">Каамыылаак.[NOM]</ta>
            <ta e="T257" id="Seg_3969" s="T256">девушка-PL-3SG-DAT/LOC</ta>
            <ta e="T258" id="Seg_3970" s="T257">говорить-PST2.[3SG]</ta>
            <ta e="T259" id="Seg_3971" s="T258">эй</ta>
            <ta e="T260" id="Seg_3972" s="T259">девушка-PL.[NOM]</ta>
            <ta e="T261" id="Seg_3973" s="T260">идти-EP-IMP.2PL</ta>
            <ta e="T262" id="Seg_3974" s="T261">выйти-EP-IMP.2PL</ta>
            <ta e="T263" id="Seg_3975" s="T262">лиственница.[NOM]</ta>
            <ta e="T264" id="Seg_3976" s="T263">дерево-DAT/LOC</ta>
            <ta e="T265" id="Seg_3977" s="T264">прятаться-CVB.SEQ</ta>
            <ta e="T266" id="Seg_3978" s="T265">сидеть-EP-IMP.2PL</ta>
            <ta e="T267" id="Seg_3979" s="T266">1SG.[NOM]</ta>
            <ta e="T268" id="Seg_3980" s="T267">богатый-DAT/LOC</ta>
            <ta e="T269" id="Seg_3981" s="T268">идти-FUT-1SG</ta>
            <ta e="T270" id="Seg_3982" s="T269">3SG-ACC</ta>
            <ta e="T271" id="Seg_3983" s="T270">сюда</ta>
            <ta e="T272" id="Seg_3984" s="T271">принести-FUT-1SG</ta>
            <ta e="T273" id="Seg_3985" s="T272">богатый.[NOM]</ta>
            <ta e="T274" id="Seg_3986" s="T273">чей</ta>
            <ta e="T275" id="Seg_3987" s="T274">богатство-3SG.[NOM]=Q</ta>
            <ta e="T276" id="Seg_3988" s="T275">говорить-CVB.SIM-говорить-CVB.SIM</ta>
            <ta e="T277" id="Seg_3989" s="T276">спрашивать-FUT.[3SG]</ta>
            <ta e="T278" id="Seg_3990" s="T277">тот-DAT/LOC</ta>
            <ta e="T279" id="Seg_3991" s="T278">2PL.[NOM]</ta>
            <ta e="T280" id="Seg_3992" s="T279">говорить-FUT-EP-IMP.2PL</ta>
            <ta e="T281" id="Seg_3993" s="T280">Каамыылаак.[NOM]</ta>
            <ta e="T282" id="Seg_3994" s="T281">богатство-3SG.[NOM]</ta>
            <ta e="T283" id="Seg_3995" s="T282">тот-ACC</ta>
            <ta e="T284" id="Seg_3996" s="T283">говорить-CVB.SEQ</ta>
            <ta e="T285" id="Seg_3997" s="T284">после</ta>
            <ta e="T286" id="Seg_3998" s="T285">Каамыылаак.[NOM]</ta>
            <ta e="T287" id="Seg_3999" s="T286">богатый-DAT/LOC</ta>
            <ta e="T288" id="Seg_4000" s="T287">доезжать-PST2.[3SG]</ta>
            <ta e="T289" id="Seg_4001" s="T288">чай.[NOM]</ta>
            <ta e="T290" id="Seg_4002" s="T289">идти-CVB.SEQ</ta>
            <ta e="T291" id="Seg_4003" s="T290">кончать-CVB.SEQ</ta>
            <ta e="T292" id="Seg_4004" s="T291">говорить-PST2.[3SG]</ta>
            <ta e="T293" id="Seg_4005" s="T292">эй</ta>
            <ta e="T294" id="Seg_4006" s="T293">друг</ta>
            <ta e="T295" id="Seg_4007" s="T294">кто-1PL.[NOM]</ta>
            <ta e="T296" id="Seg_4008" s="T295">богатство-3SG.[NOM]=Q</ta>
            <ta e="T297" id="Seg_4009" s="T296">этот</ta>
            <ta e="T298" id="Seg_4010" s="T297">там</ta>
            <ta e="T299" id="Seg_4011" s="T298">богатый.[NOM]</ta>
            <ta e="T300" id="Seg_4012" s="T299">испугаться-CVB.SIM</ta>
            <ta e="T301" id="Seg_4013" s="T300">бить-CVB.SEQ</ta>
            <ta e="T302" id="Seg_4014" s="T301">после</ta>
            <ta e="T303" id="Seg_4015" s="T302">говорить-PST2.[3SG]</ta>
            <ta e="T304" id="Seg_4016" s="T303">чей</ta>
            <ta e="T305" id="Seg_4017" s="T304">быть-FUT.[3SG]=Q</ta>
            <ta e="T306" id="Seg_4018" s="T305">мой</ta>
            <ta e="T307" id="Seg_4019" s="T306">быть-EP-NEG.CVB</ta>
            <ta e="T308" id="Seg_4020" s="T307">Каамыылаак.[NOM]</ta>
            <ta e="T309" id="Seg_4021" s="T308">нет</ta>
            <ta e="T310" id="Seg_4022" s="T309">мой</ta>
            <ta e="T311" id="Seg_4023" s="T310">верить-NEG.PTCP</ta>
            <ta e="T312" id="Seg_4024" s="T311">быть-TEMP-2SG</ta>
            <ta e="T313" id="Seg_4025" s="T312">идти-IMP.1DU</ta>
            <ta e="T314" id="Seg_4026" s="T313">небо-ABL</ta>
            <ta e="T315" id="Seg_4027" s="T314">спрашивать-CVB.SIM</ta>
            <ta e="T316" id="Seg_4028" s="T315">говорить-PST2.[3SG]</ta>
            <ta e="T317" id="Seg_4029" s="T316">идти-PST2-3PL</ta>
            <ta e="T318" id="Seg_4030" s="T317">девушка-PL.[NOM]</ta>
            <ta e="T319" id="Seg_4031" s="T318">лежать-PTCP.PRS</ta>
            <ta e="T320" id="Seg_4032" s="T319">место-3PL-DAT/LOC</ta>
            <ta e="T321" id="Seg_4033" s="T320">приходить-CVB.SEQ</ta>
            <ta e="T322" id="Seg_4034" s="T321">после</ta>
            <ta e="T323" id="Seg_4035" s="T322">Каамыылаак.[NOM]</ta>
            <ta e="T324" id="Seg_4036" s="T323">кричать-PST2.[3SG]</ta>
            <ta e="T325" id="Seg_4037" s="T324">этот.[NOM]</ta>
            <ta e="T326" id="Seg_4038" s="T325">кто.[NOM]</ta>
            <ta e="T327" id="Seg_4039" s="T326">богатство-3SG.[NOM]=Q</ta>
            <ta e="T328" id="Seg_4040" s="T327">наверху-ABL</ta>
            <ta e="T329" id="Seg_4041" s="T328">девушка-PL.[NOM]</ta>
            <ta e="T330" id="Seg_4042" s="T329">кричать-PST2-3PL</ta>
            <ta e="T331" id="Seg_4043" s="T330">Каамыылаак.[NOM]</ta>
            <ta e="T332" id="Seg_4044" s="T331">богатство-3SG.[NOM]</ta>
            <ta e="T333" id="Seg_4045" s="T332">Каамыылаак.[NOM]</ta>
            <ta e="T334" id="Seg_4046" s="T333">богатство-3SG.[NOM]</ta>
            <ta e="T335" id="Seg_4047" s="T334">слышать-PST1-2SG</ta>
            <ta e="T336" id="Seg_4048" s="T335">Q</ta>
            <ta e="T337" id="Seg_4049" s="T336">говорить-PST2.[3SG]</ta>
            <ta e="T338" id="Seg_4050" s="T337">Каамыылаак.[NOM]</ta>
            <ta e="T339" id="Seg_4051" s="T338">кто.[NOM]</ta>
            <ta e="T340" id="Seg_4052" s="T339">богатство-3SG.[NOM]</ta>
            <ta e="T341" id="Seg_4053" s="T340">быть-PST2.[3SG]=Q</ta>
            <ta e="T342" id="Seg_4054" s="T341">Каамыылаак.[NOM]</ta>
            <ta e="T343" id="Seg_4055" s="T342">тот</ta>
            <ta e="T344" id="Seg_4056" s="T343">богатый-ACC</ta>
            <ta e="T345" id="Seg_4057" s="T344">следовать-CVB.SEQ</ta>
            <ta e="T346" id="Seg_4058" s="T345">послать-PST2.[3SG]</ta>
            <ta e="T347" id="Seg_4059" s="T346">богатство-3SG-ACC</ta>
            <ta e="T348" id="Seg_4060" s="T347">богатство.[NOM]</ta>
            <ta e="T349" id="Seg_4061" s="T348">делать-PST2.[3SG]</ta>
            <ta e="T350" id="Seg_4062" s="T349">быть.богатым-CVB.SEQ-наесться-CVB.SEQ</ta>
            <ta e="T351" id="Seg_4063" s="T350">жить-PST2.[3SG]</ta>
            <ta e="T352" id="Seg_4064" s="T351">последний-3SG.[NOM]</ta>
            <ta e="T353" id="Seg_4065" s="T352">кончать-PST1-3SG</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_4066" s="T0">propr-n:case</ta>
            <ta e="T2" id="Seg_4067" s="T1">v-v:cvb</ta>
            <ta e="T3" id="Seg_4068" s="T2">v-v:tense-v:pred.pn</ta>
            <ta e="T4" id="Seg_4069" s="T3">v-v:cvb</ta>
            <ta e="T5" id="Seg_4070" s="T4">v-v:cvb-v-v:cvb</ta>
            <ta e="T6" id="Seg_4071" s="T5">n-n:case</ta>
            <ta e="T7" id="Seg_4072" s="T6">v-v:tense-v:pred.pn</ta>
            <ta e="T8" id="Seg_4073" s="T7">n-n:case</ta>
            <ta e="T9" id="Seg_4074" s="T8">v-v:ptcp</ta>
            <ta e="T10" id="Seg_4075" s="T9">n-n:case</ta>
            <ta e="T11" id="Seg_4076" s="T10">v-v:tense-v:pred.pn</ta>
            <ta e="T12" id="Seg_4077" s="T11">n-n:case</ta>
            <ta e="T13" id="Seg_4078" s="T12">n-n:poss-n:case</ta>
            <ta e="T14" id="Seg_4079" s="T13">v-v:tense-v:poss.pn</ta>
            <ta e="T15" id="Seg_4080" s="T14">v-v:cvb-v-v:cvb</ta>
            <ta e="T16" id="Seg_4081" s="T15">n-n:poss-n:case</ta>
            <ta e="T17" id="Seg_4082" s="T16">v-v:cvb</ta>
            <ta e="T18" id="Seg_4083" s="T17">v-v:tense-v:pred.pn</ta>
            <ta e="T19" id="Seg_4084" s="T18">adv</ta>
            <ta e="T20" id="Seg_4085" s="T19">v-v:cvb</ta>
            <ta e="T21" id="Seg_4086" s="T20">post</ta>
            <ta e="T22" id="Seg_4087" s="T21">v-v:cvb</ta>
            <ta e="T23" id="Seg_4088" s="T22">v-v:tense-v:pred.pn</ta>
            <ta e="T24" id="Seg_4089" s="T23">v-v:cvb-v-v:cvb</ta>
            <ta e="T25" id="Seg_4090" s="T24">n-n:(num)-n:case</ta>
            <ta e="T26" id="Seg_4091" s="T25">v-v:tense-v:pred.pn</ta>
            <ta e="T27" id="Seg_4092" s="T26">n-n:(num)-n:case</ta>
            <ta e="T28" id="Seg_4093" s="T27">v-v:cvb</ta>
            <ta e="T29" id="Seg_4094" s="T28">n-n:poss-n:case</ta>
            <ta e="T30" id="Seg_4095" s="T29">n-n&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T31" id="Seg_4096" s="T30">interj</ta>
            <ta e="T32" id="Seg_4097" s="T31">n-n:poss-n:case</ta>
            <ta e="T33" id="Seg_4098" s="T32">n-n:case</ta>
            <ta e="T34" id="Seg_4099" s="T33">v-v:ptcp</ta>
            <ta e="T35" id="Seg_4100" s="T34">v-v:tense-v:pred.pn</ta>
            <ta e="T36" id="Seg_4101" s="T35">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T37" id="Seg_4102" s="T36">dempro-pro:case</ta>
            <ta e="T38" id="Seg_4103" s="T37">propr-n:case</ta>
            <ta e="T39" id="Seg_4104" s="T38">v-v:tense-v:pred.pn</ta>
            <ta e="T40" id="Seg_4105" s="T39">interj</ta>
            <ta e="T41" id="Seg_4106" s="T40">que-pro:case</ta>
            <ta e="T42" id="Seg_4107" s="T41">v-v:tense-v:pred.pn</ta>
            <ta e="T43" id="Seg_4108" s="T42">pers-pro:case</ta>
            <ta e="T44" id="Seg_4109" s="T43">n-n:poss-n:case</ta>
            <ta e="T45" id="Seg_4110" s="T44">n-n:case</ta>
            <ta e="T46" id="Seg_4111" s="T45">ptcl</ta>
            <ta e="T47" id="Seg_4112" s="T46">v-v:tense-v:poss.pn</ta>
            <ta e="T48" id="Seg_4113" s="T47">dempro-pro:case</ta>
            <ta e="T49" id="Seg_4114" s="T48">v-v:cvb</ta>
            <ta e="T50" id="Seg_4115" s="T49">v-v:mood.pn</ta>
            <ta e="T51" id="Seg_4116" s="T50">ptcl</ta>
            <ta e="T52" id="Seg_4117" s="T51">adv</ta>
            <ta e="T53" id="Seg_4118" s="T52">n-n:(num)-n:case</ta>
            <ta e="T54" id="Seg_4119" s="T53">n-n:poss-n:case</ta>
            <ta e="T55" id="Seg_4120" s="T54">propr-n:case</ta>
            <ta e="T56" id="Seg_4121" s="T55">n-n:poss-n:case</ta>
            <ta e="T57" id="Seg_4122" s="T56">v-v:cvb</ta>
            <ta e="T58" id="Seg_4123" s="T57">v-v:tense-v:pred.pn</ta>
            <ta e="T59" id="Seg_4124" s="T58">n-n:case</ta>
            <ta e="T60" id="Seg_4125" s="T59">ptcl</ta>
            <ta e="T61" id="Seg_4126" s="T60">n-n:poss-n:case</ta>
            <ta e="T62" id="Seg_4127" s="T61">v-v:tense-v:pred.pn</ta>
            <ta e="T63" id="Seg_4128" s="T62">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T64" id="Seg_4129" s="T63">n-n:poss-n:case</ta>
            <ta e="T65" id="Seg_4130" s="T64">n-n:(poss)-n:case</ta>
            <ta e="T66" id="Seg_4131" s="T65">ptcl</ta>
            <ta e="T67" id="Seg_4132" s="T66">v-v:neg-v:pred.pn</ta>
            <ta e="T68" id="Seg_4133" s="T67">propr-n:case</ta>
            <ta e="T69" id="Seg_4134" s="T68">interj</ta>
            <ta e="T70" id="Seg_4135" s="T69">que-ptcl</ta>
            <ta e="T71" id="Seg_4136" s="T70">pers-pro:case</ta>
            <ta e="T72" id="Seg_4137" s="T71">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T73" id="Seg_4138" s="T72">v-v:tense-v:pred.pn</ta>
            <ta e="T74" id="Seg_4139" s="T73">v-v:tense-v:poss.pn-v-v:tense-v:poss.pn</ta>
            <ta e="T75" id="Seg_4140" s="T74">que</ta>
            <ta e="T76" id="Seg_4141" s="T75">v-v:cvb</ta>
            <ta e="T77" id="Seg_4142" s="T76">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T78" id="Seg_4143" s="T77">interj</ta>
            <ta e="T79" id="Seg_4144" s="T78">v-v:(ins)-v:(neg)-v:mood.pn</ta>
            <ta e="T80" id="Seg_4145" s="T79">ptcl</ta>
            <ta e="T81" id="Seg_4146" s="T80">pers-pro:case</ta>
            <ta e="T82" id="Seg_4147" s="T81">pers-pro:case</ta>
            <ta e="T83" id="Seg_4148" s="T82">adj</ta>
            <ta e="T84" id="Seg_4149" s="T83">n-n:case</ta>
            <ta e="T85" id="Seg_4150" s="T84">v-v:tense-v:pred.pn</ta>
            <ta e="T86" id="Seg_4151" s="T85">v-v:tense-v:pred.pn</ta>
            <ta e="T87" id="Seg_4152" s="T86">n-n:(num)-n:case</ta>
            <ta e="T88" id="Seg_4153" s="T87">v-v:cvb</ta>
            <ta e="T89" id="Seg_4154" s="T88">post</ta>
            <ta e="T90" id="Seg_4155" s="T89">v-v:tense-v:pred.pn</ta>
            <ta e="T91" id="Seg_4156" s="T90">n-n:poss-n:case</ta>
            <ta e="T92" id="Seg_4157" s="T91">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T93" id="Seg_4158" s="T92">propr-n:case</ta>
            <ta e="T94" id="Seg_4159" s="T93">propr-n:case</ta>
            <ta e="T95" id="Seg_4160" s="T94">adv</ta>
            <ta e="T96" id="Seg_4161" s="T95">v-v:cvb</ta>
            <ta e="T97" id="Seg_4162" s="T96">n-n:(poss)-n:case</ta>
            <ta e="T98" id="Seg_4163" s="T97">ptcl</ta>
            <ta e="T99" id="Seg_4164" s="T98">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T100" id="Seg_4165" s="T99">v-v:cvb</ta>
            <ta e="T101" id="Seg_4166" s="T100">v-v:tense-v:pred.pn</ta>
            <ta e="T102" id="Seg_4167" s="T101">adv</ta>
            <ta e="T103" id="Seg_4168" s="T102">v-v:cvb</ta>
            <ta e="T104" id="Seg_4169" s="T103">post</ta>
            <ta e="T105" id="Seg_4170" s="T104">cardnum</ta>
            <ta e="T106" id="Seg_4171" s="T105">adj</ta>
            <ta e="T107" id="Seg_4172" s="T106">n-n:case</ta>
            <ta e="T108" id="Seg_4173" s="T107">n-n:poss-n:case</ta>
            <ta e="T109" id="Seg_4174" s="T108">v-v:cvb</ta>
            <ta e="T110" id="Seg_4175" s="T109">v-v:tense-v:pred.pn</ta>
            <ta e="T111" id="Seg_4176" s="T110">v-v:cvb</ta>
            <ta e="T112" id="Seg_4177" s="T111">v-v:cvb</ta>
            <ta e="T113" id="Seg_4178" s="T112">v-v:tense-v:pred.pn</ta>
            <ta e="T114" id="Seg_4179" s="T113">adv</ta>
            <ta e="T115" id="Seg_4180" s="T114">n-n:(num)-n:case</ta>
            <ta e="T116" id="Seg_4181" s="T115">v-v:tense-v:poss.pn</ta>
            <ta e="T117" id="Seg_4182" s="T116">propr-n:case</ta>
            <ta e="T118" id="Seg_4183" s="T117">n-n:poss-n:case</ta>
            <ta e="T119" id="Seg_4184" s="T118">cardnum</ta>
            <ta e="T120" id="Seg_4185" s="T119">adj</ta>
            <ta e="T121" id="Seg_4186" s="T120">n-n:(poss)-n:case</ta>
            <ta e="T122" id="Seg_4187" s="T121">n-n:poss-n:case</ta>
            <ta e="T123" id="Seg_4188" s="T122">v-v:cvb</ta>
            <ta e="T124" id="Seg_4189" s="T123">v-v:tense-v:pred.pn</ta>
            <ta e="T125" id="Seg_4190" s="T124">propr-n:case</ta>
            <ta e="T126" id="Seg_4191" s="T125">dempro-pro:case</ta>
            <ta e="T127" id="Seg_4192" s="T126">v-v:tense-v:poss.pn</ta>
            <ta e="T128" id="Seg_4193" s="T127">ptcl</ta>
            <ta e="T129" id="Seg_4194" s="T128">ptcl</ta>
            <ta e="T130" id="Seg_4195" s="T129">adv</ta>
            <ta e="T131" id="Seg_4196" s="T130">v-v:tense-v:pred.pn</ta>
            <ta e="T132" id="Seg_4197" s="T131">v-v:tense-v:pred.pn</ta>
            <ta e="T133" id="Seg_4198" s="T132">n-n:(poss)</ta>
            <ta e="T134" id="Seg_4199" s="T133">ptcl</ta>
            <ta e="T135" id="Seg_4200" s="T134">v-v:tense-v:poss.pn</ta>
            <ta e="T136" id="Seg_4201" s="T135">v-v:cvb</ta>
            <ta e="T137" id="Seg_4202" s="T136">v-v:tense-v:pred.pn</ta>
            <ta e="T138" id="Seg_4203" s="T137">interj</ta>
            <ta e="T139" id="Seg_4204" s="T138">v-v:(ins)-v:(neg)-v:mood.pn</ta>
            <ta e="T140" id="Seg_4205" s="T139">ptcl</ta>
            <ta e="T141" id="Seg_4206" s="T140">pers-pro:case</ta>
            <ta e="T142" id="Seg_4207" s="T141">pers-pro:case</ta>
            <ta e="T143" id="Seg_4208" s="T142">dempro</ta>
            <ta e="T144" id="Seg_4209" s="T143">cardnum</ta>
            <ta e="T145" id="Seg_4210" s="T144">n-n:case</ta>
            <ta e="T146" id="Seg_4211" s="T145">v-v:tense-v:poss.pn</ta>
            <ta e="T147" id="Seg_4212" s="T146">v-v:tense-v:pred.pn</ta>
            <ta e="T148" id="Seg_4213" s="T147">n-n&gt;adj-n:(poss)-n:case</ta>
            <ta e="T149" id="Seg_4214" s="T148">propr-n:case</ta>
            <ta e="T150" id="Seg_4215" s="T149">cardnum</ta>
            <ta e="T151" id="Seg_4216" s="T150">n-n&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T152" id="Seg_4217" s="T151">cardnum</ta>
            <ta e="T153" id="Seg_4218" s="T152">n-n:poss-n:case</ta>
            <ta e="T154" id="Seg_4219" s="T153">v-v:cvb</ta>
            <ta e="T155" id="Seg_4220" s="T154">post</ta>
            <ta e="T156" id="Seg_4221" s="T155">propr-n:case</ta>
            <ta e="T157" id="Seg_4222" s="T156">n-n&gt;v-v:cvb</ta>
            <ta e="T158" id="Seg_4223" s="T157">v-v:tense-v:pred.pn</ta>
            <ta e="T159" id="Seg_4224" s="T158">v-v:cvb-v-v:cvb</ta>
            <ta e="T160" id="Seg_4225" s="T159">n-n:(num)-n:case</ta>
            <ta e="T161" id="Seg_4226" s="T160">v-v:tense-v:pred.pn</ta>
            <ta e="T162" id="Seg_4227" s="T161">adv</ta>
            <ta e="T163" id="Seg_4228" s="T162">cardnum</ta>
            <ta e="T164" id="Seg_4229" s="T163">n-n:case</ta>
            <ta e="T165" id="Seg_4230" s="T164">n-n:poss-n:case</ta>
            <ta e="T166" id="Seg_4231" s="T165">v-v:cvb</ta>
            <ta e="T167" id="Seg_4232" s="T166">v-v:cvb</ta>
            <ta e="T168" id="Seg_4233" s="T167">v-v:tense-v:pred.pn</ta>
            <ta e="T169" id="Seg_4234" s="T168">n-n:poss-n:case</ta>
            <ta e="T170" id="Seg_4235" s="T169">v-v:cvb</ta>
            <ta e="T171" id="Seg_4236" s="T170">v-v:tense-v:pred.pn</ta>
            <ta e="T172" id="Seg_4237" s="T171">v-v:cvb</ta>
            <ta e="T173" id="Seg_4238" s="T172">v-v:tense-v:pred.pn</ta>
            <ta e="T174" id="Seg_4239" s="T173">v-v:cvb-v-v:cvb</ta>
            <ta e="T175" id="Seg_4240" s="T174">cardnum</ta>
            <ta e="T176" id="Seg_4241" s="T175">n-n:case</ta>
            <ta e="T177" id="Seg_4242" s="T176">v-v:tense-v:pred.pn</ta>
            <ta e="T178" id="Seg_4243" s="T177">n-n:poss-n:case</ta>
            <ta e="T179" id="Seg_4244" s="T178">n-n:(num)-n:case</ta>
            <ta e="T180" id="Seg_4245" s="T179">adv-adv&gt;adv</ta>
            <ta e="T181" id="Seg_4246" s="T180">v-v:tense-v:pred.pn</ta>
            <ta e="T182" id="Seg_4247" s="T181">propr-n:case</ta>
            <ta e="T183" id="Seg_4248" s="T182">adv</ta>
            <ta e="T184" id="Seg_4249" s="T183">n-n:(num)-n:case</ta>
            <ta e="T185" id="Seg_4250" s="T184">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T186" id="Seg_4251" s="T185">n-n:(num)-n:case</ta>
            <ta e="T187" id="Seg_4252" s="T186">pers-pro:case</ta>
            <ta e="T188" id="Seg_4253" s="T187">n-n:poss-n:case</ta>
            <ta e="T189" id="Seg_4254" s="T188">v-v:mood-v:pred.pn</ta>
            <ta e="T190" id="Seg_4255" s="T189">n-n:poss-n:case</ta>
            <ta e="T191" id="Seg_4256" s="T190">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T192" id="Seg_4257" s="T191">ptcl</ta>
            <ta e="T193" id="Seg_4258" s="T192">n-n:case</ta>
            <ta e="T194" id="Seg_4259" s="T193">v-v:tense-v:poss.pn</ta>
            <ta e="T195" id="Seg_4260" s="T194">conj</ta>
            <ta e="T196" id="Seg_4261" s="T195">v-v:cvb</ta>
            <ta e="T197" id="Seg_4262" s="T196">v-v:cvb</ta>
            <ta e="T198" id="Seg_4263" s="T197">v-v:tense-v:poss.pn</ta>
            <ta e="T199" id="Seg_4264" s="T198">v-v:tense-v:pred.pn</ta>
            <ta e="T200" id="Seg_4265" s="T199">propr-n:case</ta>
            <ta e="T201" id="Seg_4266" s="T200">n-n:case</ta>
            <ta e="T202" id="Seg_4267" s="T201">v-v:cvb</ta>
            <ta e="T203" id="Seg_4268" s="T202">v-v:mood-v:temp.pn</ta>
            <ta e="T204" id="Seg_4269" s="T203">n-n:(num)-n:case</ta>
            <ta e="T205" id="Seg_4270" s="T204">v-v:cvb</ta>
            <ta e="T206" id="Seg_4271" s="T205">v-v:tense-v:pred.pn</ta>
            <ta e="T207" id="Seg_4272" s="T206">propr-n:case</ta>
            <ta e="T208" id="Seg_4273" s="T207">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T209" id="Seg_4274" s="T208">v-v:cvb</ta>
            <ta e="T210" id="Seg_4275" s="T209">v-v:tense-v:pred.pn</ta>
            <ta e="T211" id="Seg_4276" s="T210">v-v:tense-v:pred.pn</ta>
            <ta e="T212" id="Seg_4277" s="T211">interj</ta>
            <ta e="T213" id="Seg_4278" s="T212">adv</ta>
            <ta e="T214" id="Seg_4279" s="T213">ptcl</ta>
            <ta e="T215" id="Seg_4280" s="T214">adv</ta>
            <ta e="T216" id="Seg_4281" s="T215">v-v:cvb-v:pred.pn</ta>
            <ta e="T217" id="Seg_4282" s="T216">v-v:tense-v:pred.pn</ta>
            <ta e="T218" id="Seg_4283" s="T217">n-n:(poss)</ta>
            <ta e="T219" id="Seg_4284" s="T218">ptcl</ta>
            <ta e="T220" id="Seg_4285" s="T219">v-v:tense-v:poss.pn</ta>
            <ta e="T221" id="Seg_4286" s="T220">dempro</ta>
            <ta e="T222" id="Seg_4287" s="T221">n-n:(num)-n:case</ta>
            <ta e="T223" id="Seg_4288" s="T222">n-n:case</ta>
            <ta e="T224" id="Seg_4289" s="T223">v-v:cvb-v:pred.pn</ta>
            <ta e="T225" id="Seg_4290" s="T224">propr-n:case</ta>
            <ta e="T226" id="Seg_4291" s="T225">v-v:tense-v:pred.pn</ta>
            <ta e="T227" id="Seg_4292" s="T226">que</ta>
            <ta e="T228" id="Seg_4293" s="T227">v-v:cvb</ta>
            <ta e="T229" id="Seg_4294" s="T228">v-v:tense-v:pred.pn</ta>
            <ta e="T230" id="Seg_4295" s="T229">interj</ta>
            <ta e="T231" id="Seg_4296" s="T230">v-v:mood.pn</ta>
            <ta e="T232" id="Seg_4297" s="T231">dempro</ta>
            <ta e="T233" id="Seg_4298" s="T232">cardnum</ta>
            <ta e="T234" id="Seg_4299" s="T233">n-n:poss-n:case</ta>
            <ta e="T235" id="Seg_4300" s="T234">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T236" id="Seg_4301" s="T235">n-n&gt;adj-n:(num)-n:case</ta>
            <ta e="T237" id="Seg_4302" s="T236">propr-n:case</ta>
            <ta e="T238" id="Seg_4303" s="T237">adv</ta>
            <ta e="T239" id="Seg_4304" s="T238">cardnum</ta>
            <ta e="T240" id="Seg_4305" s="T239">n-n&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T241" id="Seg_4306" s="T240">n-n:(num)-n:poss-n:case</ta>
            <ta e="T242" id="Seg_4307" s="T241">v-v:cvb</ta>
            <ta e="T243" id="Seg_4308" s="T242">post</ta>
            <ta e="T244" id="Seg_4309" s="T243">propr-n:case</ta>
            <ta e="T245" id="Seg_4310" s="T244">v-v:cvb</ta>
            <ta e="T246" id="Seg_4311" s="T245">v-v:tense-v:pred.pn</ta>
            <ta e="T247" id="Seg_4312" s="T246">ptcl</ta>
            <ta e="T248" id="Seg_4313" s="T247">v-v:cvb</ta>
            <ta e="T249" id="Seg_4314" s="T248">v-v:cvb-v-v:cvb</ta>
            <ta e="T250" id="Seg_4315" s="T249">cardnum</ta>
            <ta e="T251" id="Seg_4316" s="T250">adj-n:case</ta>
            <ta e="T252" id="Seg_4317" s="T251">n-n:poss-n:case</ta>
            <ta e="T253" id="Seg_4318" s="T252">v-v:tense-v:pred.pn</ta>
            <ta e="T254" id="Seg_4319" s="T253">dempro-pro:case</ta>
            <ta e="T255" id="Seg_4320" s="T254">v-v:cvb</ta>
            <ta e="T256" id="Seg_4321" s="T255">propr-n:case</ta>
            <ta e="T257" id="Seg_4322" s="T256">n-n:(num)-n:poss-n:case</ta>
            <ta e="T258" id="Seg_4323" s="T257">v-v:tense-v:pred.pn</ta>
            <ta e="T259" id="Seg_4324" s="T258">interj</ta>
            <ta e="T260" id="Seg_4325" s="T259">n-n:(num)-n:case</ta>
            <ta e="T261" id="Seg_4326" s="T260">v-v:(ins)-v:mood.pn</ta>
            <ta e="T262" id="Seg_4327" s="T261">v-v:(ins)-v:mood.pn</ta>
            <ta e="T263" id="Seg_4328" s="T262">n-n:case</ta>
            <ta e="T264" id="Seg_4329" s="T263">n-n:case</ta>
            <ta e="T265" id="Seg_4330" s="T264">v-v:cvb</ta>
            <ta e="T266" id="Seg_4331" s="T265">v-v:(ins)-v:mood.pn</ta>
            <ta e="T267" id="Seg_4332" s="T266">pers-pro:case</ta>
            <ta e="T268" id="Seg_4333" s="T267">adj-n:case</ta>
            <ta e="T269" id="Seg_4334" s="T268">v-v:tense-v:poss.pn</ta>
            <ta e="T270" id="Seg_4335" s="T269">pers-pro:case</ta>
            <ta e="T271" id="Seg_4336" s="T270">adv</ta>
            <ta e="T272" id="Seg_4337" s="T271">v-v:tense-v:poss.pn</ta>
            <ta e="T273" id="Seg_4338" s="T272">adj-n:case</ta>
            <ta e="T274" id="Seg_4339" s="T273">que</ta>
            <ta e="T275" id="Seg_4340" s="T274">n-n:(poss)-n:case-ptcl</ta>
            <ta e="T276" id="Seg_4341" s="T275">v-v:cvb-v-v:cvb</ta>
            <ta e="T277" id="Seg_4342" s="T276">v-v:tense-v:poss.pn</ta>
            <ta e="T278" id="Seg_4343" s="T277">dempro-pro:case</ta>
            <ta e="T279" id="Seg_4344" s="T278">pers-pro:case</ta>
            <ta e="T280" id="Seg_4345" s="T279">v-v:(tense)-v:(ins)-v:mood.pn</ta>
            <ta e="T281" id="Seg_4346" s="T280">propr-n:case</ta>
            <ta e="T282" id="Seg_4347" s="T281">n-n:(poss)-n:case</ta>
            <ta e="T283" id="Seg_4348" s="T282">dempro-pro:case</ta>
            <ta e="T284" id="Seg_4349" s="T283">v-v:cvb</ta>
            <ta e="T285" id="Seg_4350" s="T284">post</ta>
            <ta e="T286" id="Seg_4351" s="T285">propr-n:case</ta>
            <ta e="T287" id="Seg_4352" s="T286">adj-n:case</ta>
            <ta e="T288" id="Seg_4353" s="T287">v-v:tense-v:pred.pn</ta>
            <ta e="T289" id="Seg_4354" s="T288">n-n:case</ta>
            <ta e="T290" id="Seg_4355" s="T289">v-v:cvb</ta>
            <ta e="T291" id="Seg_4356" s="T290">v-v:cvb</ta>
            <ta e="T292" id="Seg_4357" s="T291">v-v:tense-v:pred.pn</ta>
            <ta e="T293" id="Seg_4358" s="T292">interj</ta>
            <ta e="T294" id="Seg_4359" s="T293">n</ta>
            <ta e="T295" id="Seg_4360" s="T294">que-pro:(poss)-pro:case</ta>
            <ta e="T296" id="Seg_4361" s="T295">n-n:(poss)-n:case-ptcl</ta>
            <ta e="T297" id="Seg_4362" s="T296">dempro</ta>
            <ta e="T298" id="Seg_4363" s="T297">adv</ta>
            <ta e="T299" id="Seg_4364" s="T298">adj-n:case</ta>
            <ta e="T300" id="Seg_4365" s="T299">v-v:cvb</ta>
            <ta e="T301" id="Seg_4366" s="T300">v-v:cvb</ta>
            <ta e="T302" id="Seg_4367" s="T301">post</ta>
            <ta e="T303" id="Seg_4368" s="T302">v-v:tense-v:pred.pn</ta>
            <ta e="T304" id="Seg_4369" s="T303">que</ta>
            <ta e="T305" id="Seg_4370" s="T304">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T306" id="Seg_4371" s="T305">posspr</ta>
            <ta e="T307" id="Seg_4372" s="T306">v-n:(ins)-v:cvb</ta>
            <ta e="T308" id="Seg_4373" s="T307">propr-n:case</ta>
            <ta e="T309" id="Seg_4374" s="T308">ptcl</ta>
            <ta e="T310" id="Seg_4375" s="T309">posspr</ta>
            <ta e="T311" id="Seg_4376" s="T310">v-v:ptcp</ta>
            <ta e="T312" id="Seg_4377" s="T311">v-v:mood-v:temp.pn</ta>
            <ta e="T313" id="Seg_4378" s="T312">v-v:mood.pn</ta>
            <ta e="T314" id="Seg_4379" s="T313">n-n:case</ta>
            <ta e="T315" id="Seg_4380" s="T314">v-v:cvb</ta>
            <ta e="T316" id="Seg_4381" s="T315">v-v:tense-v:pred.pn</ta>
            <ta e="T317" id="Seg_4382" s="T316">v-v:tense-v:pred.pn</ta>
            <ta e="T318" id="Seg_4383" s="T317">n-n:(num)-n:case</ta>
            <ta e="T319" id="Seg_4384" s="T318">v-v:ptcp</ta>
            <ta e="T320" id="Seg_4385" s="T319">n-n:poss-n:case</ta>
            <ta e="T321" id="Seg_4386" s="T320">v-v:cvb</ta>
            <ta e="T322" id="Seg_4387" s="T321">post</ta>
            <ta e="T323" id="Seg_4388" s="T322">propr-n:case</ta>
            <ta e="T324" id="Seg_4389" s="T323">v-v:tense-v:pred.pn</ta>
            <ta e="T325" id="Seg_4390" s="T324">dempro-pro:case</ta>
            <ta e="T326" id="Seg_4391" s="T325">que-pro:case</ta>
            <ta e="T327" id="Seg_4392" s="T326">n-n:(poss)-n:case-ptcl</ta>
            <ta e="T328" id="Seg_4393" s="T327">adv-n:case</ta>
            <ta e="T329" id="Seg_4394" s="T328">n-n:(num)-n:case</ta>
            <ta e="T330" id="Seg_4395" s="T329">v-v:tense-v:pred.pn</ta>
            <ta e="T331" id="Seg_4396" s="T330">propr-n:case</ta>
            <ta e="T332" id="Seg_4397" s="T331">n-n:(poss)-n:case</ta>
            <ta e="T333" id="Seg_4398" s="T332">propr-n:case</ta>
            <ta e="T334" id="Seg_4399" s="T333">n-n:(poss)-n:case</ta>
            <ta e="T335" id="Seg_4400" s="T334">v-v:tense-v:poss.pn</ta>
            <ta e="T336" id="Seg_4401" s="T335">ptcl</ta>
            <ta e="T337" id="Seg_4402" s="T336">v-v:tense-v:pred.pn</ta>
            <ta e="T338" id="Seg_4403" s="T337">propr-n:case</ta>
            <ta e="T339" id="Seg_4404" s="T338">que-pro:case</ta>
            <ta e="T340" id="Seg_4405" s="T339">n-n:(poss)-n:case</ta>
            <ta e="T341" id="Seg_4406" s="T340">v-v:tense-v:pred.pn-ptcl</ta>
            <ta e="T342" id="Seg_4407" s="T341">propr-n:case</ta>
            <ta e="T343" id="Seg_4408" s="T342">dempro</ta>
            <ta e="T344" id="Seg_4409" s="T343">adj-n:case</ta>
            <ta e="T345" id="Seg_4410" s="T344">v-v:cvb</ta>
            <ta e="T346" id="Seg_4411" s="T345">v-v:tense-v:pred.pn</ta>
            <ta e="T347" id="Seg_4412" s="T346">n-n:poss-n:case</ta>
            <ta e="T348" id="Seg_4413" s="T347">n-n:case</ta>
            <ta e="T349" id="Seg_4414" s="T348">v-v:tense-v:pred.pn</ta>
            <ta e="T350" id="Seg_4415" s="T349">v-v:cvb-v-v:cvb</ta>
            <ta e="T351" id="Seg_4416" s="T350">v-v:tense-v:pred.pn</ta>
            <ta e="T352" id="Seg_4417" s="T351">adj-n:(poss)-n:case</ta>
            <ta e="T353" id="Seg_4418" s="T352">v-v:tense-v:poss.pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_4419" s="T0">propr</ta>
            <ta e="T2" id="Seg_4420" s="T1">v</ta>
            <ta e="T3" id="Seg_4421" s="T2">aux</ta>
            <ta e="T4" id="Seg_4422" s="T3">v</ta>
            <ta e="T5" id="Seg_4423" s="T4">aux</ta>
            <ta e="T6" id="Seg_4424" s="T5">n</ta>
            <ta e="T7" id="Seg_4425" s="T6">v</ta>
            <ta e="T8" id="Seg_4426" s="T7">n</ta>
            <ta e="T9" id="Seg_4427" s="T8">v</ta>
            <ta e="T10" id="Seg_4428" s="T9">n</ta>
            <ta e="T11" id="Seg_4429" s="T10">v</ta>
            <ta e="T12" id="Seg_4430" s="T11">n</ta>
            <ta e="T13" id="Seg_4431" s="T12">n</ta>
            <ta e="T14" id="Seg_4432" s="T13">v</ta>
            <ta e="T15" id="Seg_4433" s="T14">v</ta>
            <ta e="T16" id="Seg_4434" s="T15">n</ta>
            <ta e="T17" id="Seg_4435" s="T16">v</ta>
            <ta e="T18" id="Seg_4436" s="T17">aux</ta>
            <ta e="T19" id="Seg_4437" s="T18">adv</ta>
            <ta e="T20" id="Seg_4438" s="T19">v</ta>
            <ta e="T21" id="Seg_4439" s="T20">post</ta>
            <ta e="T22" id="Seg_4440" s="T21">v</ta>
            <ta e="T23" id="Seg_4441" s="T22">aux</ta>
            <ta e="T24" id="Seg_4442" s="T23">v</ta>
            <ta e="T25" id="Seg_4443" s="T24">n</ta>
            <ta e="T26" id="Seg_4444" s="T25">v</ta>
            <ta e="T27" id="Seg_4445" s="T26">n</ta>
            <ta e="T28" id="Seg_4446" s="T27">v</ta>
            <ta e="T29" id="Seg_4447" s="T28">n</ta>
            <ta e="T30" id="Seg_4448" s="T29">v</ta>
            <ta e="T31" id="Seg_4449" s="T30">interj</ta>
            <ta e="T32" id="Seg_4450" s="T31">n</ta>
            <ta e="T33" id="Seg_4451" s="T32">n</ta>
            <ta e="T34" id="Seg_4452" s="T33">v</ta>
            <ta e="T35" id="Seg_4453" s="T34">aux</ta>
            <ta e="T36" id="Seg_4454" s="T35">v</ta>
            <ta e="T37" id="Seg_4455" s="T36">dempro</ta>
            <ta e="T38" id="Seg_4456" s="T37">propr</ta>
            <ta e="T39" id="Seg_4457" s="T38">v</ta>
            <ta e="T40" id="Seg_4458" s="T39">interj</ta>
            <ta e="T41" id="Seg_4459" s="T40">que</ta>
            <ta e="T42" id="Seg_4460" s="T41">cop</ta>
            <ta e="T43" id="Seg_4461" s="T42">pers</ta>
            <ta e="T44" id="Seg_4462" s="T43">n</ta>
            <ta e="T45" id="Seg_4463" s="T44">n</ta>
            <ta e="T46" id="Seg_4464" s="T45">ptcl</ta>
            <ta e="T47" id="Seg_4465" s="T46">cop</ta>
            <ta e="T48" id="Seg_4466" s="T47">dempro</ta>
            <ta e="T49" id="Seg_4467" s="T48">v</ta>
            <ta e="T50" id="Seg_4468" s="T49">v</ta>
            <ta e="T51" id="Seg_4469" s="T50">ptcl</ta>
            <ta e="T52" id="Seg_4470" s="T51">adv</ta>
            <ta e="T53" id="Seg_4471" s="T52">n</ta>
            <ta e="T54" id="Seg_4472" s="T53">n</ta>
            <ta e="T55" id="Seg_4473" s="T54">propr</ta>
            <ta e="T56" id="Seg_4474" s="T55">n</ta>
            <ta e="T57" id="Seg_4475" s="T56">v</ta>
            <ta e="T58" id="Seg_4476" s="T57">v</ta>
            <ta e="T59" id="Seg_4477" s="T58">n</ta>
            <ta e="T60" id="Seg_4478" s="T59">ptcl</ta>
            <ta e="T61" id="Seg_4479" s="T60">n</ta>
            <ta e="T62" id="Seg_4480" s="T61">v</ta>
            <ta e="T63" id="Seg_4481" s="T62">v</ta>
            <ta e="T64" id="Seg_4482" s="T63">n</ta>
            <ta e="T65" id="Seg_4483" s="T64">n</ta>
            <ta e="T66" id="Seg_4484" s="T65">ptcl</ta>
            <ta e="T67" id="Seg_4485" s="T66">v</ta>
            <ta e="T68" id="Seg_4486" s="T67">propr</ta>
            <ta e="T69" id="Seg_4487" s="T68">interj</ta>
            <ta e="T70" id="Seg_4488" s="T69">que</ta>
            <ta e="T71" id="Seg_4489" s="T70">pers</ta>
            <ta e="T72" id="Seg_4490" s="T71">n</ta>
            <ta e="T73" id="Seg_4491" s="T72">v</ta>
            <ta e="T74" id="Seg_4492" s="T73">v</ta>
            <ta e="T75" id="Seg_4493" s="T74">que</ta>
            <ta e="T76" id="Seg_4494" s="T75">v</ta>
            <ta e="T77" id="Seg_4495" s="T76">v</ta>
            <ta e="T78" id="Seg_4496" s="T77">interj</ta>
            <ta e="T79" id="Seg_4497" s="T78">v</ta>
            <ta e="T80" id="Seg_4498" s="T79">ptcl</ta>
            <ta e="T81" id="Seg_4499" s="T80">pers</ta>
            <ta e="T82" id="Seg_4500" s="T81">pers</ta>
            <ta e="T83" id="Seg_4501" s="T82">adj</ta>
            <ta e="T84" id="Seg_4502" s="T83">n</ta>
            <ta e="T85" id="Seg_4503" s="T84">v</ta>
            <ta e="T86" id="Seg_4504" s="T85">v</ta>
            <ta e="T87" id="Seg_4505" s="T86">n</ta>
            <ta e="T88" id="Seg_4506" s="T87">v</ta>
            <ta e="T89" id="Seg_4507" s="T88">post</ta>
            <ta e="T90" id="Seg_4508" s="T89">v</ta>
            <ta e="T91" id="Seg_4509" s="T90">n</ta>
            <ta e="T92" id="Seg_4510" s="T91">v</ta>
            <ta e="T93" id="Seg_4511" s="T92">propr</ta>
            <ta e="T94" id="Seg_4512" s="T93">propr</ta>
            <ta e="T95" id="Seg_4513" s="T94">adv</ta>
            <ta e="T96" id="Seg_4514" s="T95">v</ta>
            <ta e="T97" id="Seg_4515" s="T96">n</ta>
            <ta e="T98" id="Seg_4516" s="T97">ptcl</ta>
            <ta e="T99" id="Seg_4517" s="T98">v</ta>
            <ta e="T100" id="Seg_4518" s="T99">v</ta>
            <ta e="T101" id="Seg_4519" s="T100">aux</ta>
            <ta e="T102" id="Seg_4520" s="T101">adv</ta>
            <ta e="T103" id="Seg_4521" s="T102">v</ta>
            <ta e="T104" id="Seg_4522" s="T103">post</ta>
            <ta e="T105" id="Seg_4523" s="T104">cardnum</ta>
            <ta e="T106" id="Seg_4524" s="T105">adj</ta>
            <ta e="T107" id="Seg_4525" s="T106">n</ta>
            <ta e="T108" id="Seg_4526" s="T107">n</ta>
            <ta e="T109" id="Seg_4527" s="T108">v</ta>
            <ta e="T110" id="Seg_4528" s="T109">aux</ta>
            <ta e="T111" id="Seg_4529" s="T110">v</ta>
            <ta e="T112" id="Seg_4530" s="T111">v</ta>
            <ta e="T113" id="Seg_4531" s="T112">aux</ta>
            <ta e="T114" id="Seg_4532" s="T113">adv</ta>
            <ta e="T115" id="Seg_4533" s="T114">n</ta>
            <ta e="T116" id="Seg_4534" s="T115">v</ta>
            <ta e="T117" id="Seg_4535" s="T116">propr</ta>
            <ta e="T118" id="Seg_4536" s="T117">n</ta>
            <ta e="T119" id="Seg_4537" s="T118">cardnum</ta>
            <ta e="T120" id="Seg_4538" s="T119">adj</ta>
            <ta e="T121" id="Seg_4539" s="T120">n</ta>
            <ta e="T122" id="Seg_4540" s="T121">n</ta>
            <ta e="T123" id="Seg_4541" s="T122">v</ta>
            <ta e="T124" id="Seg_4542" s="T123">aux</ta>
            <ta e="T125" id="Seg_4543" s="T124">propr</ta>
            <ta e="T126" id="Seg_4544" s="T125">dempro</ta>
            <ta e="T127" id="Seg_4545" s="T126">v</ta>
            <ta e="T128" id="Seg_4546" s="T127">ptcl</ta>
            <ta e="T129" id="Seg_4547" s="T128">ptcl</ta>
            <ta e="T130" id="Seg_4548" s="T129">adv</ta>
            <ta e="T131" id="Seg_4549" s="T130">v</ta>
            <ta e="T132" id="Seg_4550" s="T131">aux</ta>
            <ta e="T133" id="Seg_4551" s="T132">n</ta>
            <ta e="T134" id="Seg_4552" s="T133">ptcl</ta>
            <ta e="T135" id="Seg_4553" s="T134">v</ta>
            <ta e="T136" id="Seg_4554" s="T135">v</ta>
            <ta e="T137" id="Seg_4555" s="T136">v</ta>
            <ta e="T138" id="Seg_4556" s="T137">interj</ta>
            <ta e="T139" id="Seg_4557" s="T138">v</ta>
            <ta e="T140" id="Seg_4558" s="T139">ptcl</ta>
            <ta e="T141" id="Seg_4559" s="T140">pers</ta>
            <ta e="T142" id="Seg_4560" s="T141">pers</ta>
            <ta e="T143" id="Seg_4561" s="T142">dempro</ta>
            <ta e="T144" id="Seg_4562" s="T143">cardnum</ta>
            <ta e="T145" id="Seg_4563" s="T144">n</ta>
            <ta e="T146" id="Seg_4564" s="T145">v</ta>
            <ta e="T147" id="Seg_4565" s="T146">v</ta>
            <ta e="T148" id="Seg_4566" s="T147">n</ta>
            <ta e="T149" id="Seg_4567" s="T148">propr</ta>
            <ta e="T150" id="Seg_4568" s="T149">cardnum</ta>
            <ta e="T151" id="Seg_4569" s="T150">v</ta>
            <ta e="T152" id="Seg_4570" s="T151">cardnum</ta>
            <ta e="T153" id="Seg_4571" s="T152">n</ta>
            <ta e="T154" id="Seg_4572" s="T153">v</ta>
            <ta e="T155" id="Seg_4573" s="T154">post</ta>
            <ta e="T156" id="Seg_4574" s="T155">propr</ta>
            <ta e="T157" id="Seg_4575" s="T156">v</ta>
            <ta e="T158" id="Seg_4576" s="T157">v</ta>
            <ta e="T159" id="Seg_4577" s="T158">v</ta>
            <ta e="T160" id="Seg_4578" s="T159">n</ta>
            <ta e="T161" id="Seg_4579" s="T160">v</ta>
            <ta e="T162" id="Seg_4580" s="T161">adv</ta>
            <ta e="T163" id="Seg_4581" s="T162">cardnum</ta>
            <ta e="T164" id="Seg_4582" s="T163">n</ta>
            <ta e="T165" id="Seg_4583" s="T164">n</ta>
            <ta e="T166" id="Seg_4584" s="T165">v</ta>
            <ta e="T167" id="Seg_4585" s="T166">v</ta>
            <ta e="T168" id="Seg_4586" s="T167">aux</ta>
            <ta e="T169" id="Seg_4587" s="T168">n</ta>
            <ta e="T170" id="Seg_4588" s="T169">v</ta>
            <ta e="T171" id="Seg_4589" s="T170">aux</ta>
            <ta e="T172" id="Seg_4590" s="T171">v</ta>
            <ta e="T173" id="Seg_4591" s="T172">aux</ta>
            <ta e="T174" id="Seg_4592" s="T173">v</ta>
            <ta e="T175" id="Seg_4593" s="T174">cardnum</ta>
            <ta e="T176" id="Seg_4594" s="T175">n</ta>
            <ta e="T177" id="Seg_4595" s="T176">v</ta>
            <ta e="T178" id="Seg_4596" s="T177">n</ta>
            <ta e="T179" id="Seg_4597" s="T178">n</ta>
            <ta e="T180" id="Seg_4598" s="T179">adv</ta>
            <ta e="T181" id="Seg_4599" s="T180">v</ta>
            <ta e="T182" id="Seg_4600" s="T181">propr</ta>
            <ta e="T183" id="Seg_4601" s="T182">adv</ta>
            <ta e="T184" id="Seg_4602" s="T183">n</ta>
            <ta e="T185" id="Seg_4603" s="T184">v</ta>
            <ta e="T186" id="Seg_4604" s="T185">n</ta>
            <ta e="T187" id="Seg_4605" s="T186">pers</ta>
            <ta e="T188" id="Seg_4606" s="T187">n</ta>
            <ta e="T189" id="Seg_4607" s="T188">v</ta>
            <ta e="T190" id="Seg_4608" s="T189">n</ta>
            <ta e="T191" id="Seg_4609" s="T190">n</ta>
            <ta e="T192" id="Seg_4610" s="T191">ptcl</ta>
            <ta e="T193" id="Seg_4611" s="T192">n</ta>
            <ta e="T194" id="Seg_4612" s="T193">v</ta>
            <ta e="T195" id="Seg_4613" s="T194">conj</ta>
            <ta e="T196" id="Seg_4614" s="T195">v</ta>
            <ta e="T197" id="Seg_4615" s="T196">v</ta>
            <ta e="T198" id="Seg_4616" s="T197">aux</ta>
            <ta e="T199" id="Seg_4617" s="T198">v</ta>
            <ta e="T200" id="Seg_4618" s="T199">propr</ta>
            <ta e="T201" id="Seg_4619" s="T200">n</ta>
            <ta e="T202" id="Seg_4620" s="T201">v</ta>
            <ta e="T203" id="Seg_4621" s="T202">aux</ta>
            <ta e="T204" id="Seg_4622" s="T203">n</ta>
            <ta e="T205" id="Seg_4623" s="T204">v</ta>
            <ta e="T206" id="Seg_4624" s="T205">v</ta>
            <ta e="T207" id="Seg_4625" s="T206">propr</ta>
            <ta e="T208" id="Seg_4626" s="T207">n</ta>
            <ta e="T209" id="Seg_4627" s="T208">v</ta>
            <ta e="T210" id="Seg_4628" s="T209">aux</ta>
            <ta e="T211" id="Seg_4629" s="T210">v</ta>
            <ta e="T212" id="Seg_4630" s="T211">interj</ta>
            <ta e="T213" id="Seg_4631" s="T212">adv</ta>
            <ta e="T214" id="Seg_4632" s="T213">ptcl</ta>
            <ta e="T215" id="Seg_4633" s="T214">adv</ta>
            <ta e="T216" id="Seg_4634" s="T215">v</ta>
            <ta e="T217" id="Seg_4635" s="T216">aux</ta>
            <ta e="T218" id="Seg_4636" s="T217">n</ta>
            <ta e="T219" id="Seg_4637" s="T218">ptcl</ta>
            <ta e="T220" id="Seg_4638" s="T219">v</ta>
            <ta e="T221" id="Seg_4639" s="T220">dempro</ta>
            <ta e="T222" id="Seg_4640" s="T221">n</ta>
            <ta e="T223" id="Seg_4641" s="T222">n</ta>
            <ta e="T224" id="Seg_4642" s="T223">v</ta>
            <ta e="T225" id="Seg_4643" s="T224">propr</ta>
            <ta e="T226" id="Seg_4644" s="T225">v</ta>
            <ta e="T227" id="Seg_4645" s="T226">que</ta>
            <ta e="T228" id="Seg_4646" s="T227">v</ta>
            <ta e="T229" id="Seg_4647" s="T228">aux</ta>
            <ta e="T230" id="Seg_4648" s="T229">interj</ta>
            <ta e="T231" id="Seg_4649" s="T230">v</ta>
            <ta e="T232" id="Seg_4650" s="T231">dempro</ta>
            <ta e="T233" id="Seg_4651" s="T232">cardnum</ta>
            <ta e="T234" id="Seg_4652" s="T233">n</ta>
            <ta e="T235" id="Seg_4653" s="T234">v</ta>
            <ta e="T236" id="Seg_4654" s="T235">adj</ta>
            <ta e="T237" id="Seg_4655" s="T236">propr</ta>
            <ta e="T238" id="Seg_4656" s="T237">adv</ta>
            <ta e="T239" id="Seg_4657" s="T238">cardnum</ta>
            <ta e="T240" id="Seg_4658" s="T239">v</ta>
            <ta e="T241" id="Seg_4659" s="T240">n</ta>
            <ta e="T242" id="Seg_4660" s="T241">v</ta>
            <ta e="T243" id="Seg_4661" s="T242">post</ta>
            <ta e="T244" id="Seg_4662" s="T243">propr</ta>
            <ta e="T245" id="Seg_4663" s="T244">v</ta>
            <ta e="T246" id="Seg_4664" s="T245">aux</ta>
            <ta e="T247" id="Seg_4665" s="T246">ptcl</ta>
            <ta e="T248" id="Seg_4666" s="T247">v</ta>
            <ta e="T249" id="Seg_4667" s="T248">v</ta>
            <ta e="T250" id="Seg_4668" s="T249">cardnum</ta>
            <ta e="T251" id="Seg_4669" s="T250">n</ta>
            <ta e="T252" id="Seg_4670" s="T251">n</ta>
            <ta e="T253" id="Seg_4671" s="T252">v</ta>
            <ta e="T254" id="Seg_4672" s="T253">dempro</ta>
            <ta e="T255" id="Seg_4673" s="T254">v</ta>
            <ta e="T256" id="Seg_4674" s="T255">propr</ta>
            <ta e="T257" id="Seg_4675" s="T256">n</ta>
            <ta e="T258" id="Seg_4676" s="T257">v</ta>
            <ta e="T259" id="Seg_4677" s="T258">interj</ta>
            <ta e="T260" id="Seg_4678" s="T259">n</ta>
            <ta e="T261" id="Seg_4679" s="T260">v</ta>
            <ta e="T262" id="Seg_4680" s="T261">v</ta>
            <ta e="T263" id="Seg_4681" s="T262">n</ta>
            <ta e="T264" id="Seg_4682" s="T263">n</ta>
            <ta e="T265" id="Seg_4683" s="T264">v</ta>
            <ta e="T266" id="Seg_4684" s="T265">aux</ta>
            <ta e="T267" id="Seg_4685" s="T266">pers</ta>
            <ta e="T268" id="Seg_4686" s="T267">n</ta>
            <ta e="T269" id="Seg_4687" s="T268">v</ta>
            <ta e="T270" id="Seg_4688" s="T269">pers</ta>
            <ta e="T271" id="Seg_4689" s="T270">adv</ta>
            <ta e="T272" id="Seg_4690" s="T271">v</ta>
            <ta e="T273" id="Seg_4691" s="T272">n</ta>
            <ta e="T274" id="Seg_4692" s="T273">que</ta>
            <ta e="T275" id="Seg_4693" s="T274">n</ta>
            <ta e="T276" id="Seg_4694" s="T275">v</ta>
            <ta e="T277" id="Seg_4695" s="T276">v</ta>
            <ta e="T278" id="Seg_4696" s="T277">dempro</ta>
            <ta e="T279" id="Seg_4697" s="T278">pers</ta>
            <ta e="T280" id="Seg_4698" s="T279">v</ta>
            <ta e="T281" id="Seg_4699" s="T280">propr</ta>
            <ta e="T282" id="Seg_4700" s="T281">n</ta>
            <ta e="T283" id="Seg_4701" s="T282">dempro</ta>
            <ta e="T284" id="Seg_4702" s="T283">v</ta>
            <ta e="T285" id="Seg_4703" s="T284">post</ta>
            <ta e="T286" id="Seg_4704" s="T285">propr</ta>
            <ta e="T287" id="Seg_4705" s="T286">n</ta>
            <ta e="T288" id="Seg_4706" s="T287">v</ta>
            <ta e="T289" id="Seg_4707" s="T288">n</ta>
            <ta e="T290" id="Seg_4708" s="T289">v</ta>
            <ta e="T291" id="Seg_4709" s="T290">v</ta>
            <ta e="T292" id="Seg_4710" s="T291">v</ta>
            <ta e="T293" id="Seg_4711" s="T292">interj</ta>
            <ta e="T294" id="Seg_4712" s="T293">n</ta>
            <ta e="T295" id="Seg_4713" s="T294">que</ta>
            <ta e="T296" id="Seg_4714" s="T295">n</ta>
            <ta e="T297" id="Seg_4715" s="T296">dempro</ta>
            <ta e="T298" id="Seg_4716" s="T297">adv</ta>
            <ta e="T299" id="Seg_4717" s="T298">n</ta>
            <ta e="T300" id="Seg_4718" s="T299">v</ta>
            <ta e="T301" id="Seg_4719" s="T300">aux</ta>
            <ta e="T302" id="Seg_4720" s="T301">post</ta>
            <ta e="T303" id="Seg_4721" s="T302">v</ta>
            <ta e="T304" id="Seg_4722" s="T303">que</ta>
            <ta e="T305" id="Seg_4723" s="T304">cop</ta>
            <ta e="T306" id="Seg_4724" s="T305">posspr</ta>
            <ta e="T307" id="Seg_4725" s="T306">cop</ta>
            <ta e="T308" id="Seg_4726" s="T307">propr</ta>
            <ta e="T309" id="Seg_4727" s="T308">ptcl</ta>
            <ta e="T310" id="Seg_4728" s="T309">posspr</ta>
            <ta e="T311" id="Seg_4729" s="T310">v</ta>
            <ta e="T312" id="Seg_4730" s="T311">aux</ta>
            <ta e="T313" id="Seg_4731" s="T312">v</ta>
            <ta e="T314" id="Seg_4732" s="T313">n</ta>
            <ta e="T315" id="Seg_4733" s="T314">v</ta>
            <ta e="T316" id="Seg_4734" s="T315">v</ta>
            <ta e="T317" id="Seg_4735" s="T316">v</ta>
            <ta e="T318" id="Seg_4736" s="T317">n</ta>
            <ta e="T319" id="Seg_4737" s="T318">v</ta>
            <ta e="T320" id="Seg_4738" s="T319">n</ta>
            <ta e="T321" id="Seg_4739" s="T320">v</ta>
            <ta e="T322" id="Seg_4740" s="T321">post</ta>
            <ta e="T323" id="Seg_4741" s="T322">propr</ta>
            <ta e="T324" id="Seg_4742" s="T323">v</ta>
            <ta e="T325" id="Seg_4743" s="T324">dempro</ta>
            <ta e="T326" id="Seg_4744" s="T325">que</ta>
            <ta e="T327" id="Seg_4745" s="T326">n</ta>
            <ta e="T328" id="Seg_4746" s="T327">adv</ta>
            <ta e="T329" id="Seg_4747" s="T328">n</ta>
            <ta e="T330" id="Seg_4748" s="T329">v</ta>
            <ta e="T331" id="Seg_4749" s="T330">propr</ta>
            <ta e="T332" id="Seg_4750" s="T331">n</ta>
            <ta e="T333" id="Seg_4751" s="T332">propr</ta>
            <ta e="T334" id="Seg_4752" s="T333">n</ta>
            <ta e="T335" id="Seg_4753" s="T334">v</ta>
            <ta e="T336" id="Seg_4754" s="T335">ptcl</ta>
            <ta e="T337" id="Seg_4755" s="T336">v</ta>
            <ta e="T338" id="Seg_4756" s="T337">propr</ta>
            <ta e="T339" id="Seg_4757" s="T338">que</ta>
            <ta e="T340" id="Seg_4758" s="T339">n</ta>
            <ta e="T341" id="Seg_4759" s="T340">cop</ta>
            <ta e="T342" id="Seg_4760" s="T341">propr</ta>
            <ta e="T343" id="Seg_4761" s="T342">dempro</ta>
            <ta e="T344" id="Seg_4762" s="T343">n</ta>
            <ta e="T345" id="Seg_4763" s="T344">v</ta>
            <ta e="T346" id="Seg_4764" s="T345">aux</ta>
            <ta e="T347" id="Seg_4765" s="T346">n</ta>
            <ta e="T348" id="Seg_4766" s="T347">n</ta>
            <ta e="T349" id="Seg_4767" s="T348">v</ta>
            <ta e="T350" id="Seg_4768" s="T349">v</ta>
            <ta e="T351" id="Seg_4769" s="T350">v</ta>
            <ta e="T352" id="Seg_4770" s="T351">adj</ta>
            <ta e="T353" id="Seg_4771" s="T352">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR" />
         <annotation name="SyF" tierref="SyF" />
         <annotation name="IST" tierref="IST" />
         <annotation name="Top" tierref="Top" />
         <annotation name="Foc" tierref="Foc" />
         <annotation name="BOR" tierref="BOR" />
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T3" id="Seg_4772" s="T0">Kaamyylaak was walking around.</ta>
            <ta e="T7" id="Seg_4773" s="T3">Walking so he came to one temporary settlement.</ta>
            <ta e="T11" id="Seg_4774" s="T7">He saw frozen blood on the ice.</ta>
            <ta e="T18" id="Seg_4775" s="T11">"I've found a foreleg of a reindeer", he said and put it into his pocket.</ta>
            <ta e="T23" id="Seg_4776" s="T18">After that he went on.</ta>
            <ta e="T26" id="Seg_4777" s="T23">He walked on and on and came to a settlement.</ta>
            <ta e="T30" id="Seg_4778" s="T26">The inhabitants were preparing meat for cooking.</ta>
            <ta e="T36" id="Seg_4779" s="T30">"The meat will be not enough for our guest, apparently", they said.</ta>
            <ta e="T39" id="Seg_4780" s="T36">Kaamyylaak said on this: </ta>
            <ta e="T50" id="Seg_4781" s="T39">"Well, what of it, there is meat in my pocket, bring it in and cook it!"</ta>
            <ta e="T58" id="Seg_4782" s="T50">The inhabitants threw the "meat" of Kaamyylaak into their kettle.</ta>
            <ta e="T62" id="Seg_4783" s="T58">In the evening they took the meat out.</ta>
            <ta e="T67" id="Seg_4784" s="T62">After they had taken it out, it was again not enough for their guest. </ta>
            <ta e="T73" id="Seg_4785" s="T67">Kaamyylaak: "Where is my meat?", he said.</ta>
            <ta e="T74" id="Seg_4786" s="T73">"I will die, I will die.</ta>
            <ta e="T77" id="Seg_4787" s="T74">How should I fall asleep without having eaten?!"</ta>
            <ta e="T80" id="Seg_4788" s="T77">"Hey, just don't die.</ta>
            <ta e="T87" id="Seg_4789" s="T80">We will give you the whole reindeer calf", said the inhabitants.</ta>
            <ta e="T93" id="Seg_4790" s="T87">They ate and fell asleep, before that they showed the reindeer calf to Kaamyylaak.</ta>
            <ta e="T110" id="Seg_4791" s="T93">At night Kaamyylaak went out, caught the reindeer calf, took it apart and threw the pieces on the two best reindeer bulls' horns.</ta>
            <ta e="T113" id="Seg_4792" s="T110">He came back inside and fel asleep.</ta>
            <ta e="T124" id="Seg_4793" s="T113">In the morning the inhabitants saw, that the pieces of the Kaamyylaak's reindeer calf hung on the horns of their best reindeer bulls.</ta>
            <ta e="T128" id="Seg_4794" s="T124">Kaamyylaak heard that and strated to scream: </ta>
            <ta e="T132" id="Seg_4795" s="T128">"I will die indeed!</ta>
            <ta e="T137" id="Seg_4796" s="T132">I stayed without the reindeer calf!", he shouted.</ta>
            <ta e="T140" id="Seg_4797" s="T137">"Well, just don't die!</ta>
            <ta e="T148" id="Seg_4798" s="T140">We will give you these two reindeer bulls!", said the inhabitants.</ta>
            <ta e="T151" id="Seg_4799" s="T148">I will die became two reindeer bulls.</ta>
            <ta e="T158" id="Seg_4800" s="T151">After Kaamyylaak harnessed both reindeer bulls, he went off in a sledge.</ta>
            <ta e="T161" id="Seg_4801" s="T158">He drove on and on and stumbled over a cemetery.</ta>
            <ta e="T171" id="Seg_4802" s="T161">He opened one grave(?), took an old woman out of there and put her on his sledge.</ta>
            <ta e="T173" id="Seg_4803" s="T171">He drove on.</ta>
            <ta e="T177" id="Seg_4804" s="T173">He drove on and on and came to a settlement.</ta>
            <ta e="T181" id="Seg_4805" s="T177">He left his sledge far from the settlement.</ta>
            <ta e="T185" id="Seg_4806" s="T181">Kaamyylaak met children outside: </ta>
            <ta e="T189" id="Seg_4807" s="T185">"Children, do not go to my sledge!</ta>
            <ta e="T192" id="Seg_4808" s="T189">There is my old woman in the sledge.</ta>
            <ta e="T199" id="Seg_4809" s="T192">If somebody goes there, she will die from fear!", he said.</ta>
            <ta e="T206" id="Seg_4810" s="T199">As Kaamyylaak was sitting and drinking tea, the children ran inside screaming: </ta>
            <ta e="T211" id="Seg_4811" s="T206">"Kaamyylaak, your old woman died!", they said.</ta>
            <ta e="T212" id="Seg_4812" s="T211">"Oh dear!</ta>
            <ta e="T217" id="Seg_4813" s="T212">Now I will die indeed!</ta>
            <ta e="T220" id="Seg_4814" s="T217">I've lost my old woman!</ta>
            <ta e="T226" id="Seg_4815" s="T220">These children came closer to the sledge!", shouted Kaamyylaak.</ta>
            <ta e="T230" id="Seg_4816" s="T226">"Why should you die!</ta>
            <ta e="T236" id="Seg_4817" s="T230">Take these two our daughters!", said the inhabitants.</ta>
            <ta e="T240" id="Seg_4818" s="T236">Kaamyylaak got two girls so.</ta>
            <ta e="T247" id="Seg_4819" s="T240">After he loaded the girls, Kaamyylaak drove on.</ta>
            <ta e="T253" id="Seg_4820" s="T247">He drove on and on and saw a porch of one rich man.</ta>
            <ta e="T258" id="Seg_4821" s="T253">Kaamyylaak saw it and said to the girls: </ta>
            <ta e="T264" id="Seg_4822" s="T258">"Now, girls, go and climb on the larch tree!</ta>
            <ta e="T266" id="Seg_4823" s="T264">Hide yourselves there!</ta>
            <ta e="T269" id="Seg_4824" s="T266">I will go to the rich man.</ta>
            <ta e="T272" id="Seg_4825" s="T269">I will bring him hither.</ta>
            <ta e="T277" id="Seg_4826" s="T272">The rich man will ask: "Whose wealth is it?" </ta>
            <ta e="T280" id="Seg_4827" s="T277">Say on that: </ta>
            <ta e="T282" id="Seg_4828" s="T280">"This is Kaamyylaak's wealth!""</ta>
            <ta e="T288" id="Seg_4829" s="T282">After he had said this, Kaamyylaak went to the rich man.</ta>
            <ta e="T292" id="Seg_4830" s="T288">After having druck tea, he said: </ta>
            <ta e="T297" id="Seg_4831" s="T292">"Well, my friend, whose wealth is that?"</ta>
            <ta e="T303" id="Seg_4832" s="T297">The rich man was startled by this and said: </ta>
            <ta e="T305" id="Seg_4833" s="T303">"Whose else?!</ta>
            <ta e="T307" id="Seg_4834" s="T305">Mine of course!"</ta>
            <ta e="T308" id="Seg_4835" s="T307">Kaamyylaak: </ta>
            <ta e="T309" id="Seg_4836" s="T308">"No!</ta>
            <ta e="T310" id="Seg_4837" s="T309">It's mine!</ta>
            <ta e="T316" id="Seg_4838" s="T310">If you do not believe, let'S go and ask heaven!", he said.</ta>
            <ta e="T317" id="Seg_4839" s="T316">The went.</ta>
            <ta e="T324" id="Seg_4840" s="T317">They came to that place, where the girls were, Kaamyylaak shouted: </ta>
            <ta e="T327" id="Seg_4841" s="T324">"Whose wealth is this?</ta>
            <ta e="T330" id="Seg_4842" s="T327">The girls shouted from above: </ta>
            <ta e="T334" id="Seg_4843" s="T330">"It's Kaamyylaak's wealth, It's Kaamyylaak's wealth!"</ta>
            <ta e="T341" id="Seg_4844" s="T334">"Did you hear that?", said Kaamyylaak, "so whose wealth is it?!"</ta>
            <ta e="T349" id="Seg_4845" s="T341">Kaamyylaak chased that rich man away and made his wealth his own.</ta>
            <ta e="T351" id="Seg_4846" s="T349">He lived a rich life and ate his fill.</ta>
            <ta e="T353" id="Seg_4847" s="T351">That's the end.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T3" id="Seg_4848" s="T0">Kaamyylaak ging umher.</ta>
            <ta e="T7" id="Seg_4849" s="T3">So gehend kam er an eine vorübergehende Siedlung.</ta>
            <ta e="T11" id="Seg_4850" s="T7">Auf dem Eis sah er gefrorenes Blut.</ta>
            <ta e="T18" id="Seg_4851" s="T11">"Ich habe das Vorderbein eines Rentiers gefunden", sagte er und legte es in seine Tasche.</ta>
            <ta e="T23" id="Seg_4852" s="T18">Danach ging er weiter.</ta>
            <ta e="T26" id="Seg_4853" s="T23">Er ging und ging und kam zu einer Siedlung.</ta>
            <ta e="T30" id="Seg_4854" s="T26">Bei der Siedlung bereiteten sie Fleisch vor, um es zu kochen.</ta>
            <ta e="T36" id="Seg_4855" s="T30">"Das Fleisch reicht wohl nicht für unseren Gast", sagten sie.</ta>
            <ta e="T39" id="Seg_4856" s="T36">Darauf sagte Kaamyylaak: </ta>
            <ta e="T50" id="Seg_4857" s="T39">"Nun, was seid ihr, in meiner Tasche sollte Fleisch sein, bringt es herein und kocht es!"</ta>
            <ta e="T58" id="Seg_4858" s="T50">Darauf warfen die Leute das "Fleisch" von Kaamyylaak in ihren Kessel.</ta>
            <ta e="T62" id="Seg_4859" s="T58">Am Abend dann nahmen sie das Fleisch heraus.</ta>
            <ta e="T67" id="Seg_4860" s="T62">Nachdem sie es herausgenommen hatten, reichte es wieder nicht für ihren Gast.</ta>
            <ta e="T73" id="Seg_4861" s="T67">Kaamyylaak: "Wo ist mein Fleisch?", sagte er.</ta>
            <ta e="T74" id="Seg_4862" s="T73">"Ich sterbe, ich sterbe.</ta>
            <ta e="T77" id="Seg_4863" s="T74">Wie soll ich einschlafen, ohne zu essen?!"</ta>
            <ta e="T80" id="Seg_4864" s="T77">"Nun, stirb nur nicht.</ta>
            <ta e="T87" id="Seg_4865" s="T80">Wir geben dir ein ganzes Rentierkalb", sagten die Bewohner.</ta>
            <ta e="T93" id="Seg_4866" s="T87">Sie aßen und schliefen ein, [vorher] zeigten sie Kaamyylaak das Rentierkalb.</ta>
            <ta e="T110" id="Seg_4867" s="T93">In der Nacht ging Kaamyylaak hinaus, griff das Rentierkalb, danach hängte er es auf die Geweihe der beiden besten Rentierbullen.</ta>
            <ta e="T113" id="Seg_4868" s="T110">Er ging hinein und schlief.</ta>
            <ta e="T124" id="Seg_4869" s="T113">Am Morgen sahen die Bewohner, dass das Rentierkalb von Kaamyylaak auf den Geweihen ihrer zwei besten Rentierbullen hängt.</ta>
            <ta e="T128" id="Seg_4870" s="T124">Kaamyylaak hörte das doch: </ta>
            <ta e="T132" id="Seg_4871" s="T128">"Und ich sterbe wirklich!</ta>
            <ta e="T137" id="Seg_4872" s="T132">Ich bin ohne Rentierkalb geblieben!", schrie er.</ta>
            <ta e="T140" id="Seg_4873" s="T137">"Nun, stirb nur nicht!</ta>
            <ta e="T148" id="Seg_4874" s="T140">Wir geben dir diese beiden Rentierbullen!", sagten die Bewohner.</ta>
            <ta e="T151" id="Seg_4875" s="T148">Kaamyylaak bekam zwei Rentierbullen.</ta>
            <ta e="T158" id="Seg_4876" s="T151">Nachdem er die beiden Rentierbullen angespannt hatte, fuhr Kaamyylaak mit dem Schlitten davon.</ta>
            <ta e="T161" id="Seg_4877" s="T158">Er fuhr und fuhr und kam zu einem Friedhof.</ta>
            <ta e="T171" id="Seg_4878" s="T161">Er öffnete ein Grab(?), nahm eine alte Frau heraus und legte sie auf seinen Schlitten.</ta>
            <ta e="T173" id="Seg_4879" s="T171">Er fuhr weiter.</ta>
            <ta e="T177" id="Seg_4880" s="T173">Er fuhr und fuhr und kam zu einer Siedlung.</ta>
            <ta e="T181" id="Seg_4881" s="T177">Seinen Schlitten ließ er weit entfernt stehen.</ta>
            <ta e="T185" id="Seg_4882" s="T181">Draußen traf Kaamyylaak Kinder: </ta>
            <ta e="T189" id="Seg_4883" s="T185">"Kinder, geht nicht zu meinem Schlitten!</ta>
            <ta e="T192" id="Seg_4884" s="T189">Auf dem Schlitten liegt meine alte Frau.</ta>
            <ta e="T199" id="Seg_4885" s="T192">Wenn ein Mensch dahin geht, stirbt sie vor Schreck!", sagte er.</ta>
            <ta e="T206" id="Seg_4886" s="T199">Als Kaamyylaak saß und Tee trank, liefen die Kinder herein: </ta>
            <ta e="T211" id="Seg_4887" s="T206">"Kaamyylaak, deine alte Frau ist gestorben!", sagten sie.</ta>
            <ta e="T212" id="Seg_4888" s="T211">"Oh nein!</ta>
            <ta e="T217" id="Seg_4889" s="T212">Jetzt sterbe ich bestimmt wirklich!</ta>
            <ta e="T220" id="Seg_4890" s="T217">Ich habe meine alte Frau verloren!</ta>
            <ta e="T226" id="Seg_4891" s="T220">Diese Kinder haben sich dem Schlitten genähert!", schrie Kaamyylaak.</ta>
            <ta e="T230" id="Seg_4892" s="T226">"Warum sollst du denn sterben!</ta>
            <ta e="T236" id="Seg_4893" s="T230">Nimm diese beiden unserer Töchter!", sagten die Bewohner.</ta>
            <ta e="T240" id="Seg_4894" s="T236">So bekam Kaamyylaak zwei Mädchen.</ta>
            <ta e="T247" id="Seg_4895" s="T240">Als er die Mädchen eingeladen hatte, fuhr Kaamyylaak weiter.</ta>
            <ta e="T253" id="Seg_4896" s="T247">Er fuhr und fuhr und sah die Veranda eines Reichen.</ta>
            <ta e="T258" id="Seg_4897" s="T253">Kaamyylaak sah sie und sagte zu den Mädchen: </ta>
            <ta e="T264" id="Seg_4898" s="T258">"Nun, Mädchen, geht zur Lärche!</ta>
            <ta e="T266" id="Seg_4899" s="T264">Versteckt euch dort.</ta>
            <ta e="T269" id="Seg_4900" s="T266">Ich gehe zu dem Reichen.</ta>
            <ta e="T272" id="Seg_4901" s="T269">Ich bringe ihn hierher.</ta>
            <ta e="T277" id="Seg_4902" s="T272">Der Reiche wird fragen: "Wessen Reichtum ist das?"</ta>
            <ta e="T280" id="Seg_4903" s="T277">Sagt darauf: </ta>
            <ta e="T282" id="Seg_4904" s="T280">"Das ist der Reichtum von Kaamyylaak!""</ta>
            <ta e="T288" id="Seg_4905" s="T282">Nachdem er das gesagt hatte, kam Kaamyylaak zu dem Reichen.</ta>
            <ta e="T292" id="Seg_4906" s="T288">Er hörte auf Tee zu trinken und sagte: </ta>
            <ta e="T297" id="Seg_4907" s="T292">"Nun, mein Freund, was ist das hier für ein Reichtum?"</ta>
            <ta e="T303" id="Seg_4908" s="T297">Da erschrak der Reiche und sagte: </ta>
            <ta e="T305" id="Seg_4909" s="T303">"Wie wessen?!</ta>
            <ta e="T307" id="Seg_4910" s="T305">Meiner natürlich!"</ta>
            <ta e="T308" id="Seg_4911" s="T307">Kaamyylaak: </ta>
            <ta e="T309" id="Seg_4912" s="T308">"Nein!</ta>
            <ta e="T310" id="Seg_4913" s="T309">Meiner!</ta>
            <ta e="T316" id="Seg_4914" s="T310">Wenn du es nicht glaubst, dann lass uns gehen und den Himmel fragen!", sagte er.</ta>
            <ta e="T317" id="Seg_4915" s="T316">Sie gingen.</ta>
            <ta e="T324" id="Seg_4916" s="T317">Sie kamen zu dem Platz, wo die Mädchen waren, Kaamyylaak schrie: </ta>
            <ta e="T327" id="Seg_4917" s="T324">"Wessen Reichtum ist das?"</ta>
            <ta e="T330" id="Seg_4918" s="T327">Die Mädchen schrieen von oben: </ta>
            <ta e="T334" id="Seg_4919" s="T330">"Der Reichtum von Kaamyylaak, der Reichtum von Kaamyylaak!"</ta>
            <ta e="T341" id="Seg_4920" s="T334">"Hast du das gehört?", sagte Kaamyylaak, "wessen Reichtum ist das also wohl?!"</ta>
            <ta e="T349" id="Seg_4921" s="T341">Kaamyylaak verjagte diesen Reichen und machte dessen Reichtum zu seinem.</ta>
            <ta e="T351" id="Seg_4922" s="T349">Er lebte reich und satt.</ta>
            <ta e="T353" id="Seg_4923" s="T351">Ende, hier hört es auf.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T3" id="Seg_4924" s="T0">Вот шел Каамыылаак.</ta>
            <ta e="T7" id="Seg_4925" s="T3">По пути на старое стойбище набрел.</ta>
            <ta e="T11" id="Seg_4926" s="T7">На льду он увидел замерзшую кровь.</ta>
            <ta e="T18" id="Seg_4927" s="T11">"Оленью переднюю ногу нашел", — сказал и положил в сумку.</ta>
            <ta e="T23" id="Seg_4928" s="T18">Потом зашагал дальше.</ta>
            <ta e="T26" id="Seg_4929" s="T23">Шел-шел, пришел к жилью.</ta>
            <ta e="T30" id="Seg_4930" s="T26">Хозяева мясо рубили, чтоб сварить.</ta>
            <ta e="T36" id="Seg_4931" s="T30">— Мяса-то для госта не хватает, оказывается, — сказали.</ta>
            <ta e="T39" id="Seg_4932" s="T36">На это Каамыылаак говорит: </ta>
            <ta e="T50" id="Seg_4933" s="T39">— Ну и что, в моей сумке должно быть мясо, внесите и сварите!</ta>
            <ta e="T58" id="Seg_4934" s="T50">Хозяева опустили в свой котел "мясо" Каамыылаака.</ta>
            <ta e="T62" id="Seg_4935" s="T58">Вот к вечеру выкладывают мясо.</ta>
            <ta e="T67" id="Seg_4936" s="T62">Разделили было, а гостю все равно мяса не досталось.</ta>
            <ta e="T73" id="Seg_4937" s="T67">Каамыылаак: — А где мое мясо? — кричит.</ta>
            <ta e="T74" id="Seg_4938" s="T73">— Умираю, умираю!</ta>
            <ta e="T77" id="Seg_4939" s="T74">Как усну, не поевши?!</ta>
            <ta e="T80" id="Seg_4940" s="T77">— Что ты, что ты, только не умирай.</ta>
            <ta e="T87" id="Seg_4941" s="T80">Мы тебе дадим целого тугута — сказали хозяева.</ta>
            <ta e="T93" id="Seg_4942" s="T87">Поев, уснули, перед этим Каамыылааку показали того тугута.</ta>
            <ta e="T110" id="Seg_4943" s="T93">Ночью Каамыылаак вышел во двор, поймал тугута, разорвал его и куски накинул на рога двух лучших оленей-самцов.</ta>
            <ta e="T113" id="Seg_4944" s="T110">Вернулся и уснул.</ta>
            <ta e="T124" id="Seg_4945" s="T113">Утром хозяева увидели: куски тугута Каамыылаака висят на рогах двух лучших оленей-самцов.</ta>
            <ta e="T128" id="Seg_4946" s="T124">Как услышал это Каамыылаак, стал кричать: </ta>
            <ta e="T132" id="Seg_4947" s="T128">— И вправду умираю!</ta>
            <ta e="T137" id="Seg_4948" s="T132">Остался без тугута! — закричал.</ta>
            <ta e="T140" id="Seg_4949" s="T137">— Что ты, что ты! Только не умирай! </ta>
            <ta e="T148" id="Seg_4950" s="T140">Мы тебе тех двух оленей-самцов отдадим! — сказали хозяева.</ta>
            <ta e="T151" id="Seg_4951" s="T148">Каамыылаак заимел двух оленей-самцов.</ta>
            <ta e="T158" id="Seg_4952" s="T151">Каамыылаак запряг двух своих оленей-самцов и поехал.</ta>
            <ta e="T161" id="Seg_4953" s="T158">Ехал-ехал и наехал на кладбище.</ta>
            <ta e="T171" id="Seg_4954" s="T161">Раскрыв одну колоду, достал труп старухи и положил на свои нарты.</ta>
            <ta e="T173" id="Seg_4955" s="T171">Дальше поехал.</ta>
            <ta e="T177" id="Seg_4956" s="T173">Доехал до одного стойбища.</ta>
            <ta e="T181" id="Seg_4957" s="T177">Нарты оставил подальше от жилья.</ta>
            <ta e="T185" id="Seg_4958" s="T181">Во дворе Каамыылаак встретил детей: </ta>
            <ta e="T189" id="Seg_4959" s="T185">— Дети, не подходите близко к моим нартам!</ta>
            <ta e="T192" id="Seg_4960" s="T189">На нартах моя старуха лежит.</ta>
            <ta e="T199" id="Seg_4961" s="T192">Если приблизится человек, она умрет от испуга! — сказал.</ta>
            <ta e="T206" id="Seg_4962" s="T199">Когда Каамыылаак пил чай, вбежали дети с криком: </ta>
            <ta e="T211" id="Seg_4963" s="T206">— Каамыылаак, старуха твоя умерла! — сказали.</ta>
            <ta e="T212" id="Seg_4964" s="T211">— Вот беда!</ta>
            <ta e="T217" id="Seg_4965" s="T212">Теперь-то я наверняка умру!</ta>
            <ta e="T220" id="Seg_4966" s="T217">Без старухи остался!</ta>
            <ta e="T226" id="Seg_4967" s="T220">Эти дети приближались к нартам! — завопил Каамыылаак.</ta>
            <ta e="T230" id="Seg_4968" s="T226">— Зачем тебе умирать, не надо!</ta>
            <ta e="T236" id="Seg_4969" s="T230">Возьми двух наших дочерей! — сказали хозяева.</ta>
            <ta e="T240" id="Seg_4970" s="T236">Так Каамыылаак заимел двух девушек.</ta>
            <ta e="T247" id="Seg_4971" s="T240">Посадив девушек на нарты, Каамыылаак поехал дальше.</ta>
            <ta e="T253" id="Seg_4972" s="T247">Ехал-ехал и увидел крыльцо одного богача.</ta>
            <ta e="T258" id="Seg_4973" s="T253">Заметив это, Каамыылаак говорит девушкам: </ta>
            <ta e="T264" id="Seg_4974" s="T258">— Ну, девушки, идите взберитесь на лиственницу!</ta>
            <ta e="T266" id="Seg_4975" s="T264">Спрячьтесь там.</ta>
            <ta e="T269" id="Seg_4976" s="T266">А я пойду к богачу.</ta>
            <ta e="T272" id="Seg_4977" s="T269">Его сюда приведу.</ta>
            <ta e="T277" id="Seg_4978" s="T272">Богач: "Чье это богатство?" — спросит.</ta>
            <ta e="T280" id="Seg_4979" s="T277">На это вы ответите: </ta>
            <ta e="T282" id="Seg_4980" s="T280">"Богатство Каамыылаака".</ta>
            <ta e="T288" id="Seg_4981" s="T282">Сказав так, Каамыылаак пришел к богачу.</ta>
            <ta e="T292" id="Seg_4982" s="T288">Кончив пить чай, говорит: </ta>
            <ta e="T297" id="Seg_4983" s="T292">— Ну, друг, чье все это богатство?</ta>
            <ta e="T303" id="Seg_4984" s="T297">На это богач удивился: </ta>
            <ta e="T305" id="Seg_4985" s="T303">— Как чье?!</ta>
            <ta e="T307" id="Seg_4986" s="T305">Конечно мое!</ta>
            <ta e="T308" id="Seg_4987" s="T307">А Каамыылаак сказал: </ta>
            <ta e="T309" id="Seg_4988" s="T308">— Нет!</ta>
            <ta e="T310" id="Seg_4989" s="T309">Мое!</ta>
            <ta e="T316" id="Seg_4990" s="T310">Если не веришь, пойдем спросим у тангара! — сказал.</ta>
            <ta e="T317" id="Seg_4991" s="T316">Пошли.</ta>
            <ta e="T324" id="Seg_4992" s="T317">Подойдя к месту, где прятались девушки, Каамыылаак закричал: </ta>
            <ta e="T327" id="Seg_4993" s="T324">— Чье все это богатство?</ta>
            <ta e="T330" id="Seg_4994" s="T327">Девушки сверху кричат: </ta>
            <ta e="T334" id="Seg_4995" s="T330">— Богатство Каамыылаака, богатство Каамыылаака!</ta>
            <ta e="T341" id="Seg_4996" s="T334">— Слышал? — сказал Каамыылаак, — так чье же это богатство?</ta>
            <ta e="T349" id="Seg_4997" s="T341">Каамыылаак прогнал того богача прочь, сделав его богатство своим.</ta>
            <ta e="T351" id="Seg_4998" s="T349">Стал жить богато и сытно.</ta>
            <ta e="T353" id="Seg_4999" s="T351">Все, конец.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T3" id="Seg_5000" s="T0">Вот шел Каамыылаак.</ta>
            <ta e="T7" id="Seg_5001" s="T3">По пути на старое стойбище набрел.</ta>
            <ta e="T11" id="Seg_5002" s="T7">На льду он увидел замерзшую кровь.</ta>
            <ta e="T18" id="Seg_5003" s="T11">"Оленью переднюю ногу нашел", — сказал и положил в сумку.</ta>
            <ta e="T23" id="Seg_5004" s="T18">Потом зашагал дальше.</ta>
            <ta e="T26" id="Seg_5005" s="T23">Шел-шел, пришел к жилью.</ta>
            <ta e="T30" id="Seg_5006" s="T26">Хозяева мясо рубили, чтоб сварить.</ta>
            <ta e="T36" id="Seg_5007" s="T30">— Мяса-то для госта не хватает, оказывается, — сказали.</ta>
            <ta e="T39" id="Seg_5008" s="T36">На это Каамыылаак говорит: </ta>
            <ta e="T50" id="Seg_5009" s="T39">— Ну и что, в моей сумке должно быть мясо, внесите и сварите!</ta>
            <ta e="T58" id="Seg_5010" s="T50">Хозяева опустили в свой котел "мясо" Каамыылаака.</ta>
            <ta e="T62" id="Seg_5011" s="T58">Вот к вечеру выкладывают мясо.</ta>
            <ta e="T67" id="Seg_5012" s="T62">Разделили было, а гостю все равно мяса не досталось.</ta>
            <ta e="T73" id="Seg_5013" s="T67">Каамыылаак: — А где мое мясо? — кричит.</ta>
            <ta e="T74" id="Seg_5014" s="T73">— Умираю, умираю!</ta>
            <ta e="T77" id="Seg_5015" s="T74">Как усну, не поевши?!</ta>
            <ta e="T80" id="Seg_5016" s="T77">— Что ты, что ты, только не умирай.</ta>
            <ta e="T87" id="Seg_5017" s="T80">Мы тебе дадим целого тугута — сказали хозяева.</ta>
            <ta e="T93" id="Seg_5018" s="T87">Поев, уснули, перед этим Каамыылааку показали того тугута.</ta>
            <ta e="T110" id="Seg_5019" s="T93">Ночью Каамыылаак вышел во двор, поймал тугута, разорвал его и куски накинул на рога двух лучших оленей-самцов.</ta>
            <ta e="T113" id="Seg_5020" s="T110">Вернулся и уснул.</ta>
            <ta e="T124" id="Seg_5021" s="T113">Утром хозяева увидели: куски тугута Каамыылаака висят на рогах двух лучших оленей-самцов.</ta>
            <ta e="T128" id="Seg_5022" s="T124">Как услышал это Каамыылаак, стал кричать: </ta>
            <ta e="T132" id="Seg_5023" s="T128">— И вправду умираю!</ta>
            <ta e="T137" id="Seg_5024" s="T132">Остался без тугута!</ta>
            <ta e="T140" id="Seg_5025" s="T137">— Что ты, что ты! Только не умирай! </ta>
            <ta e="T148" id="Seg_5026" s="T140">Мы тебе тех двух оленей-самцов отдадим! — сказали хозяева.</ta>
            <ta e="T151" id="Seg_5027" s="T148">Каамыылаак заимел двух оленей-самцов.</ta>
            <ta e="T158" id="Seg_5028" s="T151">Каамыылаак запряг двух своих оленей-самцов и поехал.</ta>
            <ta e="T161" id="Seg_5029" s="T158">Ехал-ехал и наехал на кладбище.</ta>
            <ta e="T171" id="Seg_5030" s="T161">Раскрыв одну колоду, достал труп старухи и положил на свои нарты.</ta>
            <ta e="T173" id="Seg_5031" s="T171">Дальше поехал.</ta>
            <ta e="T177" id="Seg_5032" s="T173">Доехал до одного стойбища.</ta>
            <ta e="T181" id="Seg_5033" s="T177">Нарты оставил подальше от жилья.</ta>
            <ta e="T185" id="Seg_5034" s="T181">Во дворе Каамыылаак встретил детей: </ta>
            <ta e="T189" id="Seg_5035" s="T185">— Дети, не подходите близко к моим нартам!</ta>
            <ta e="T192" id="Seg_5036" s="T189">На нартах моя старуха лежит.</ta>
            <ta e="T199" id="Seg_5037" s="T192">Если приблизится человек, она умрет от испуга! — сказал.</ta>
            <ta e="T206" id="Seg_5038" s="T199">Когда Каамыылаак пил чай, вбежали дети с криком: </ta>
            <ta e="T211" id="Seg_5039" s="T206">— Каамыылаак, старуха твоя умерла!</ta>
            <ta e="T212" id="Seg_5040" s="T211">— Вот беда!</ta>
            <ta e="T217" id="Seg_5041" s="T212">Теперь-то я наверняка умру!</ta>
            <ta e="T220" id="Seg_5042" s="T217">Без старухи остался!</ta>
            <ta e="T226" id="Seg_5043" s="T220">Говорил же детям не приближаться к нартам! — завопил Каамыылаак.</ta>
            <ta e="T230" id="Seg_5044" s="T226">— Зачем тебе умирать, не надо!</ta>
            <ta e="T236" id="Seg_5045" s="T230">Возьми двух наших дочерей! — сказали хозяева.</ta>
            <ta e="T240" id="Seg_5046" s="T236">Так Каамыылаак заимел двух девушек.</ta>
            <ta e="T247" id="Seg_5047" s="T240">Посадив девушек на нарты, Каамыылаак поехал дальше.</ta>
            <ta e="T253" id="Seg_5048" s="T247">Ехал-ехал и увидел крыльцо одного богача.</ta>
            <ta e="T258" id="Seg_5049" s="T253">Заметив это, Каамыылаак говорит девушкам: </ta>
            <ta e="T264" id="Seg_5050" s="T258">— Ну, девушки, идите взберитесь на лиственницу!</ta>
            <ta e="T266" id="Seg_5051" s="T264">Спрячьтесь там.</ta>
            <ta e="T269" id="Seg_5052" s="T266">А я пойду к богачу.</ta>
            <ta e="T272" id="Seg_5053" s="T269">Его сюда приведу.</ta>
            <ta e="T277" id="Seg_5054" s="T272">Богач спросит. "Чье это богатство?" </ta>
            <ta e="T280" id="Seg_5055" s="T277">На это вы ответите: </ta>
            <ta e="T282" id="Seg_5056" s="T280">"Богатство Каамыылаака".</ta>
            <ta e="T288" id="Seg_5057" s="T282">Сказав так, Каамыылаак пришел к богачу.</ta>
            <ta e="T292" id="Seg_5058" s="T288">Кончив пить чай, говорит: </ta>
            <ta e="T297" id="Seg_5059" s="T292">— Ну, друг, чье все это богатство?</ta>
            <ta e="T303" id="Seg_5060" s="T297">На это богач удивился: </ta>
            <ta e="T305" id="Seg_5061" s="T303">— Как чье?!</ta>
            <ta e="T307" id="Seg_5062" s="T305">Конечно мое!</ta>
            <ta e="T308" id="Seg_5063" s="T307">А Каамыылаак сказал: </ta>
            <ta e="T309" id="Seg_5064" s="T308">— Нет!</ta>
            <ta e="T310" id="Seg_5065" s="T309">Мое!</ta>
            <ta e="T316" id="Seg_5066" s="T310">Если не веришь, пойдем спросим у тангара!</ta>
            <ta e="T317" id="Seg_5067" s="T316">Пошли.</ta>
            <ta e="T324" id="Seg_5068" s="T317">Подойдя к месту, где прятались девушки, Каамыылаак закричал: </ta>
            <ta e="T327" id="Seg_5069" s="T324">— Чье все это богатство?</ta>
            <ta e="T330" id="Seg_5070" s="T327">Девушки сверху кричат: </ta>
            <ta e="T334" id="Seg_5071" s="T330">— Богатство Каамыылаака, богатство Каамыылаака!</ta>
            <ta e="T341" id="Seg_5072" s="T334">— Слышал? — сказал Каамыылаак, — так чье же это богатство?</ta>
            <ta e="T349" id="Seg_5073" s="T341">Каамыылаак прогнал того богача прочь, сделав его богатство своим.</ta>
            <ta e="T351" id="Seg_5074" s="T349">Стал жить богато и сытно.</ta>
            <ta e="T353" id="Seg_5075" s="T351">Все, конец.</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T354" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T355" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg"
                          name="gg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top"
                          name="Top"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc"
                          name="Foc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
