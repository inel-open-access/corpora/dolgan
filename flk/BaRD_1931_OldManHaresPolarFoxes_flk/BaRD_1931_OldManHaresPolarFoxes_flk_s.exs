<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID4F6E59DD-9C2A-301C-7457-ACB684A7F58E">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>BaRD_1931_OldManHaresPolarFoxes_flk</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="creation-date">2017-01-03T11:40:19.024+01:00</ud-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\flk\BaRD_1931_OldManHaresPolarFoxes_flk\BaRD_1931_OldManHaresPolarFoxes_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">266</ud-information>
            <ud-information attribute-name="# HIAT:w">191</ud-information>
            <ud-information attribute-name="# e">191</ud-information>
            <ud-information attribute-name="# HIAT:u">29</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="BaRD">
            <abbreviation>BaRD</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="4.0" type="appl" />
         <tli id="T1" time="4.5" type="appl" />
         <tli id="T2" time="5.0" type="appl" />
         <tli id="T3" time="5.5" type="appl" />
         <tli id="T4" time="6.0" type="appl" />
         <tli id="T5" time="6.5" type="appl" />
         <tli id="T6" time="7.0" type="appl" />
         <tli id="T7" time="7.5" type="appl" />
         <tli id="T8" time="8.0" type="appl" />
         <tli id="T9" time="8.5" type="appl" />
         <tli id="T10" time="9.0" type="appl" />
         <tli id="T11" time="9.5" type="appl" />
         <tli id="T12" time="10.0" type="appl" />
         <tli id="T13" time="10.5" type="appl" />
         <tli id="T14" time="11.0" type="appl" />
         <tli id="T15" time="11.5" type="appl" />
         <tli id="T16" time="12.0" type="appl" />
         <tli id="T17" time="12.5" type="appl" />
         <tli id="T18" time="13.0" type="appl" />
         <tli id="T19" time="13.5" type="appl" />
         <tli id="T20" time="14.0" type="appl" />
         <tli id="T21" time="14.5" type="appl" />
         <tli id="T22" time="15.0" type="appl" />
         <tli id="T23" time="15.5" type="appl" />
         <tli id="T24" time="16.0" type="appl" />
         <tli id="T25" time="16.5" type="appl" />
         <tli id="T26" time="17.0" type="appl" />
         <tli id="T27" time="17.5" type="appl" />
         <tli id="T28" time="18.0" type="appl" />
         <tli id="T29" time="18.5" type="appl" />
         <tli id="T30" time="19.0" type="appl" />
         <tli id="T31" time="19.5" type="appl" />
         <tli id="T32" time="20.0" type="appl" />
         <tli id="T33" time="20.5" type="appl" />
         <tli id="T34" time="21.0" type="appl" />
         <tli id="T35" time="21.5" type="appl" />
         <tli id="T36" time="22.0" type="appl" />
         <tli id="T37" time="22.5" type="appl" />
         <tli id="T38" time="23.0" type="appl" />
         <tli id="T39" time="23.5" type="appl" />
         <tli id="T40" time="24.0" type="appl" />
         <tli id="T41" time="24.5" type="appl" />
         <tli id="T42" time="25.0" type="appl" />
         <tli id="T43" time="25.5" type="appl" />
         <tli id="T44" time="26.0" type="appl" />
         <tli id="T45" time="26.5" type="appl" />
         <tli id="T46" time="27.0" type="appl" />
         <tli id="T47" time="27.5" type="appl" />
         <tli id="T48" time="28.0" type="appl" />
         <tli id="T49" time="28.5" type="appl" />
         <tli id="T50" time="29.0" type="appl" />
         <tli id="T51" time="29.5" type="appl" />
         <tli id="T52" time="30.0" type="appl" />
         <tli id="T53" time="30.5" type="appl" />
         <tli id="T54" time="31.0" type="appl" />
         <tli id="T55" time="31.5" type="appl" />
         <tli id="T56" time="32.0" type="appl" />
         <tli id="T57" time="32.5" type="appl" />
         <tli id="T58" time="33.0" type="appl" />
         <tli id="T59" time="33.5" type="appl" />
         <tli id="T60" time="34.0" type="appl" />
         <tli id="T61" time="34.5" type="appl" />
         <tli id="T62" time="35.0" type="appl" />
         <tli id="T63" time="35.5" type="appl" />
         <tli id="T64" time="36.0" type="appl" />
         <tli id="T65" time="36.5" type="appl" />
         <tli id="T66" time="37.0" type="appl" />
         <tli id="T67" time="37.5" type="appl" />
         <tli id="T68" time="38.0" type="appl" />
         <tli id="T69" time="38.5" type="appl" />
         <tli id="T70" time="39.0" type="appl" />
         <tli id="T71" time="39.5" type="appl" />
         <tli id="T72" time="40.0" type="appl" />
         <tli id="T73" time="40.5" type="appl" />
         <tli id="T74" time="41.0" type="appl" />
         <tli id="T75" time="41.5" type="appl" />
         <tli id="T76" time="42.0" type="appl" />
         <tli id="T77" time="42.5" type="appl" />
         <tli id="T78" time="43.0" type="appl" />
         <tli id="T79" time="43.5" type="appl" />
         <tli id="T80" time="44.0" type="appl" />
         <tli id="T81" time="44.5" type="appl" />
         <tli id="T82" time="45.0" type="appl" />
         <tli id="T83" time="45.5" type="appl" />
         <tli id="T84" time="46.0" type="appl" />
         <tli id="T85" time="46.5" type="appl" />
         <tli id="T86" time="47.0" type="appl" />
         <tli id="T87" time="47.5" type="appl" />
         <tli id="T88" time="48.0" type="appl" />
         <tli id="T89" time="48.5" type="appl" />
         <tli id="T90" time="49.0" type="appl" />
         <tli id="T91" time="49.5" type="appl" />
         <tli id="T92" time="50.0" type="appl" />
         <tli id="T93" time="50.5" type="appl" />
         <tli id="T94" time="51.0" type="appl" />
         <tli id="T95" time="51.5" type="appl" />
         <tli id="T96" time="52.0" type="appl" />
         <tli id="T97" time="52.5" type="appl" />
         <tli id="T98" time="53.0" type="appl" />
         <tli id="T99" time="53.5" type="appl" />
         <tli id="T100" time="54.0" type="appl" />
         <tli id="T101" time="54.5" type="appl" />
         <tli id="T102" time="55.0" type="appl" />
         <tli id="T103" time="55.5" type="appl" />
         <tli id="T104" time="56.0" type="appl" />
         <tli id="T105" time="56.5" type="appl" />
         <tli id="T106" time="57.0" type="appl" />
         <tli id="T107" time="57.5" type="appl" />
         <tli id="T108" time="58.0" type="appl" />
         <tli id="T109" time="58.5" type="appl" />
         <tli id="T110" time="59.0" type="appl" />
         <tli id="T111" time="59.5" type="appl" />
         <tli id="T112" time="60.0" type="appl" />
         <tli id="T113" time="60.5" type="appl" />
         <tli id="T114" time="61.0" type="appl" />
         <tli id="T115" time="61.5" type="appl" />
         <tli id="T116" time="62.0" type="appl" />
         <tli id="T117" time="62.5" type="appl" />
         <tli id="T118" time="63.0" type="appl" />
         <tli id="T119" time="63.5" type="appl" />
         <tli id="T120" time="64.0" type="appl" />
         <tli id="T121" time="64.5" type="appl" />
         <tli id="T122" time="65.0" type="appl" />
         <tli id="T123" time="65.5" type="appl" />
         <tli id="T124" time="66.0" type="appl" />
         <tli id="T125" time="66.5" type="appl" />
         <tli id="T126" time="67.0" type="appl" />
         <tli id="T127" time="67.5" type="appl" />
         <tli id="T128" time="68.0" type="appl" />
         <tli id="T129" time="68.5" type="appl" />
         <tli id="T130" time="69.0" type="appl" />
         <tli id="T131" time="69.5" type="appl" />
         <tli id="T132" time="70.0" type="appl" />
         <tli id="T133" time="70.5" type="appl" />
         <tli id="T134" time="71.0" type="appl" />
         <tli id="T135" time="71.5" type="appl" />
         <tli id="T136" time="72.0" type="appl" />
         <tli id="T137" time="72.5" type="appl" />
         <tli id="T138" time="73.0" type="appl" />
         <tli id="T139" time="73.5" type="appl" />
         <tli id="T140" time="74.0" type="appl" />
         <tli id="T141" time="74.5" type="appl" />
         <tli id="T142" time="75.0" type="appl" />
         <tli id="T143" time="75.5" type="appl" />
         <tli id="T144" time="76.0" type="appl" />
         <tli id="T145" time="76.5" type="appl" />
         <tli id="T146" time="77.0" type="appl" />
         <tli id="T147" time="77.5" type="appl" />
         <tli id="T148" time="78.0" type="appl" />
         <tli id="T149" time="78.5" type="appl" />
         <tli id="T150" time="79.0" type="appl" />
         <tli id="T151" time="79.5" type="appl" />
         <tli id="T152" time="80.0" type="appl" />
         <tli id="T153" time="80.5" type="appl" />
         <tli id="T154" time="81.0" type="appl" />
         <tli id="T155" time="81.5" type="appl" />
         <tli id="T156" time="82.0" type="appl" />
         <tli id="T157" time="82.5" type="appl" />
         <tli id="T158" time="83.0" type="appl" />
         <tli id="T159" time="83.5" type="appl" />
         <tli id="T160" time="84.0" type="appl" />
         <tli id="T161" time="84.5" type="appl" />
         <tli id="T162" time="85.0" type="appl" />
         <tli id="T163" time="85.5" type="appl" />
         <tli id="T164" time="86.0" type="appl" />
         <tli id="T165" time="86.5" type="appl" />
         <tli id="T166" time="87.0" type="appl" />
         <tli id="T167" time="87.5" type="appl" />
         <tli id="T168" time="88.0" type="appl" />
         <tli id="T169" time="88.5" type="appl" />
         <tli id="T170" time="89.0" type="appl" />
         <tli id="T171" time="89.5" type="appl" />
         <tli id="T172" time="90.0" type="appl" />
         <tli id="T173" time="90.5" type="appl" />
         <tli id="T174" time="91.0" type="appl" />
         <tli id="T175" time="91.5" type="appl" />
         <tli id="T176" time="92.0" type="appl" />
         <tli id="T177" time="92.5" type="appl" />
         <tli id="T178" time="93.0" type="appl" />
         <tli id="T179" time="93.5" type="appl" />
         <tli id="T180" time="94.0" type="appl" />
         <tli id="T181" time="94.5" type="appl" />
         <tli id="T182" time="95.0" type="appl" />
         <tli id="T183" time="95.5" type="appl" />
         <tli id="T184" time="96.0" type="appl" />
         <tli id="T185" time="96.5" type="appl" />
         <tli id="T186" time="97.0" type="appl" />
         <tli id="T187" time="97.5" type="appl" />
         <tli id="T188" time="98.0" type="appl" />
         <tli id="T189" time="98.5" type="appl" />
         <tli id="T190" time="99.0" type="appl" />
         <tli id="T191" time="99.5" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="BaRD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T191" id="Seg_0" n="sc" s="T0">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Bɨlɨr</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">biːr</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">kihi</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">olorbut</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T7" id="Seg_17" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">Bu</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">kihi</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">hanaːbɨt</ts>
                  <nts id="Seg_26" n="HIAT:ip">:</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_29" n="HIAT:u" s="T7">
                  <nts id="Seg_30" n="HIAT:ip">"</nts>
                  <ts e="T8" id="Seg_32" n="HIAT:w" s="T7">Kɨrsanɨ</ts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_35" n="HIAT:w" s="T8">barɨtɨn</ts>
                  <nts id="Seg_36" n="HIAT:ip">,</nts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_39" n="HIAT:w" s="T9">uskaːnɨ</ts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_42" n="HIAT:w" s="T10">barɨtɨn</ts>
                  <nts id="Seg_43" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_45" n="HIAT:w" s="T11">ölörön</ts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_48" n="HIAT:w" s="T12">keːhi͡ekke</ts>
                  <nts id="Seg_49" n="HIAT:ip">,</nts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_52" n="HIAT:w" s="T13">biːri</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_55" n="HIAT:w" s="T14">da</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_58" n="HIAT:w" s="T15">ordorumaː</ts>
                  <nts id="Seg_59" n="HIAT:ip">"</nts>
                  <nts id="Seg_60" n="HIAT:ip">,</nts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_63" n="HIAT:w" s="T16">di͡ebit</ts>
                  <nts id="Seg_64" n="HIAT:ip">.</nts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T19" id="Seg_67" n="HIAT:u" s="T17">
                  <ts e="T18" id="Seg_69" n="HIAT:w" s="T17">U͡olugar</ts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_72" n="HIAT:w" s="T18">eppit</ts>
                  <nts id="Seg_73" n="HIAT:ip">:</nts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T27" id="Seg_76" n="HIAT:u" s="T19">
                  <nts id="Seg_77" n="HIAT:ip">"</nts>
                  <ts e="T20" id="Seg_79" n="HIAT:w" s="T19">Min</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_82" n="HIAT:w" s="T20">ɨ͡aldʼɨbɨt</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_85" n="HIAT:w" s="T21">bu͡oluːm</ts>
                  <nts id="Seg_86" n="HIAT:ip">,</nts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_89" n="HIAT:w" s="T22">on</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_92" n="HIAT:w" s="T23">baraŋŋɨn</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_95" n="HIAT:w" s="T24">uskaːn</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_98" n="HIAT:w" s="T25">ojunun</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_101" n="HIAT:w" s="T26">egelʼ</ts>
                  <nts id="Seg_102" n="HIAT:ip">.</nts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T34" id="Seg_105" n="HIAT:u" s="T27">
                  <ts e="T28" id="Seg_107" n="HIAT:w" s="T27">Onton</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_110" n="HIAT:w" s="T28">kini͡eke</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_113" n="HIAT:w" s="T29">löčü͡ögünen</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_116" n="HIAT:w" s="T30">barɨ</ts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_119" n="HIAT:w" s="T31">uskaːnɨ</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_122" n="HIAT:w" s="T32">üːren</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_125" n="HIAT:w" s="T33">egel</ts>
                  <nts id="Seg_126" n="HIAT:ip">.</nts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T39" id="Seg_129" n="HIAT:u" s="T34">
                  <ts e="T35" id="Seg_131" n="HIAT:w" s="T34">U͡ola</ts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_134" n="HIAT:w" s="T35">barbɨta</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_137" n="HIAT:w" s="T36">biːr</ts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_140" n="HIAT:w" s="T37">uskaːn</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_143" n="HIAT:w" s="T38">oloror</ts>
                  <nts id="Seg_144" n="HIAT:ip">.</nts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T44" id="Seg_147" n="HIAT:u" s="T39">
                  <nts id="Seg_148" n="HIAT:ip">"</nts>
                  <ts e="T40" id="Seg_150" n="HIAT:w" s="T39">Dʼe</ts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_153" n="HIAT:w" s="T40">kaja</ts>
                  <nts id="Seg_154" n="HIAT:ip">,</nts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_157" n="HIAT:w" s="T41">ojuŋŋut</ts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_160" n="HIAT:w" s="T42">kannanɨj</ts>
                  <nts id="Seg_161" n="HIAT:ip">"</nts>
                  <nts id="Seg_162" n="HIAT:ip">,</nts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_165" n="HIAT:w" s="T43">diːr</ts>
                  <nts id="Seg_166" n="HIAT:ip">.</nts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T49" id="Seg_169" n="HIAT:u" s="T44">
                  <nts id="Seg_170" n="HIAT:ip">"</nts>
                  <ts e="T45" id="Seg_172" n="HIAT:w" s="T44">Onno</ts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_175" n="HIAT:w" s="T45">baːr</ts>
                  <nts id="Seg_176" n="HIAT:ip">"</nts>
                  <nts id="Seg_177" n="HIAT:ip">,</nts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_180" n="HIAT:w" s="T46">di͡en</ts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_183" n="HIAT:w" s="T47">ɨjan</ts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_186" n="HIAT:w" s="T48">bi͡erer</ts>
                  <nts id="Seg_187" n="HIAT:ip">.</nts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T50" id="Seg_190" n="HIAT:u" s="T49">
                  <ts e="T50" id="Seg_192" n="HIAT:w" s="T49">Baran</ts>
                  <nts id="Seg_193" n="HIAT:ip">:</nts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T59" id="Seg_196" n="HIAT:u" s="T50">
                  <nts id="Seg_197" n="HIAT:ip">"</nts>
                  <ts e="T51" id="Seg_199" n="HIAT:w" s="T50">Dʼe</ts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_202" n="HIAT:w" s="T51">abɨraː</ts>
                  <nts id="Seg_203" n="HIAT:ip">"</nts>
                  <nts id="Seg_204" n="HIAT:ip">,</nts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_207" n="HIAT:w" s="T52">diːr</ts>
                  <nts id="Seg_208" n="HIAT:ip">,</nts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_210" n="HIAT:ip">"</nts>
                  <ts e="T54" id="Seg_212" n="HIAT:w" s="T53">agam</ts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_215" n="HIAT:w" s="T54">ölöːrü</ts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_218" n="HIAT:w" s="T55">ɨŋɨrtarda</ts>
                  <nts id="Seg_219" n="HIAT:ip">,</nts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_222" n="HIAT:w" s="T56">kömölöhön</ts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_225" n="HIAT:w" s="T57">kör</ts>
                  <nts id="Seg_226" n="HIAT:ip">"</nts>
                  <nts id="Seg_227" n="HIAT:ip">,</nts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_230" n="HIAT:w" s="T58">diːr</ts>
                  <nts id="Seg_231" n="HIAT:ip">.</nts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T65" id="Seg_234" n="HIAT:u" s="T59">
                  <ts e="T60" id="Seg_236" n="HIAT:w" s="T59">Bu</ts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_239" n="HIAT:w" s="T60">biri͡emege</ts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_242" n="HIAT:w" s="T61">eter</ts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_245" n="HIAT:w" s="T62">kaːlaːččɨ</ts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_248" n="HIAT:w" s="T63">kihi</ts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_251" n="HIAT:w" s="T64">emeːksiniger</ts>
                  <nts id="Seg_252" n="HIAT:ip">:</nts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T71" id="Seg_255" n="HIAT:u" s="T65">
                  <nts id="Seg_256" n="HIAT:ip">"</nts>
                  <ts e="T66" id="Seg_258" n="HIAT:w" s="T65">Munnʼuhunnaktarɨna</ts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_261" n="HIAT:w" s="T66">en</ts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_264" n="HIAT:w" s="T67">aːŋŋɨn</ts>
                  <nts id="Seg_265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_267" n="HIAT:w" s="T68">battatan</ts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_270" n="HIAT:w" s="T69">keːheːr</ts>
                  <nts id="Seg_271" n="HIAT:ip">"</nts>
                  <nts id="Seg_272" n="HIAT:ip">,</nts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_275" n="HIAT:w" s="T70">diːr</ts>
                  <nts id="Seg_276" n="HIAT:ip">.</nts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T77" id="Seg_279" n="HIAT:u" s="T71">
                  <ts e="T72" id="Seg_281" n="HIAT:w" s="T71">Ojunnara</ts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_284" n="HIAT:w" s="T72">kelen</ts>
                  <nts id="Seg_285" n="HIAT:ip">,</nts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_288" n="HIAT:w" s="T73">uskaːn</ts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_291" n="HIAT:w" s="T74">bögönü</ts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_294" n="HIAT:w" s="T75">löčü͡ögünen</ts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_297" n="HIAT:w" s="T76">egeler</ts>
                  <nts id="Seg_298" n="HIAT:ip">.</nts>
                  <nts id="Seg_299" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T86" id="Seg_301" n="HIAT:u" s="T77">
                  <ts e="T78" id="Seg_303" n="HIAT:w" s="T77">Bu</ts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_306" n="HIAT:w" s="T78">egelen</ts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_309" n="HIAT:w" s="T79">kɨːrardagɨna</ts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_312" n="HIAT:w" s="T80">emeːksin</ts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_315" n="HIAT:w" s="T81">aːnɨ</ts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_318" n="HIAT:w" s="T82">battatar</ts>
                  <nts id="Seg_319" n="HIAT:ip">,</nts>
                  <nts id="Seg_320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_322" n="HIAT:w" s="T83">onton</ts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_325" n="HIAT:w" s="T84">ɨ͡aldʼaːččɨ</ts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_328" n="HIAT:w" s="T85">bu͡olbut</ts>
                  <nts id="Seg_329" n="HIAT:ip">:</nts>
                  <nts id="Seg_330" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T88" id="Seg_332" n="HIAT:u" s="T86">
                  <nts id="Seg_333" n="HIAT:ip">"</nts>
                  <ts e="T87" id="Seg_335" n="HIAT:w" s="T86">Oldoːnno</ts>
                  <nts id="Seg_336" n="HIAT:ip">"</nts>
                  <nts id="Seg_337" n="HIAT:ip">,</nts>
                  <nts id="Seg_338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_340" n="HIAT:w" s="T87">di͡ebit</ts>
                  <nts id="Seg_341" n="HIAT:ip">.</nts>
                  <nts id="Seg_342" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T91" id="Seg_344" n="HIAT:u" s="T88">
                  <ts e="T89" id="Seg_346" n="HIAT:w" s="T88">Emeːksin</ts>
                  <nts id="Seg_347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_349" n="HIAT:w" s="T89">oldoːnu</ts>
                  <nts id="Seg_350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_352" n="HIAT:w" s="T90">bi͡erbit</ts>
                  <nts id="Seg_353" n="HIAT:ip">.</nts>
                  <nts id="Seg_354" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T100" id="Seg_356" n="HIAT:u" s="T91">
                  <ts e="T92" id="Seg_358" n="HIAT:w" s="T91">Dʼe</ts>
                  <nts id="Seg_359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_361" n="HIAT:w" s="T92">onon</ts>
                  <nts id="Seg_362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_364" n="HIAT:w" s="T93">tura</ts>
                  <nts id="Seg_365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_367" n="HIAT:w" s="T94">ekkireːn</ts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_370" n="HIAT:w" s="T95">ogonnʼor</ts>
                  <nts id="Seg_371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_373" n="HIAT:w" s="T96">dʼe</ts>
                  <nts id="Seg_374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_376" n="HIAT:w" s="T97">uskaːnnarɨ</ts>
                  <nts id="Seg_377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_379" n="HIAT:w" s="T98">ölörtöːbüt</ts>
                  <nts id="Seg_380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_382" n="HIAT:w" s="T99">eːt</ts>
                  <nts id="Seg_383" n="HIAT:ip">.</nts>
                  <nts id="Seg_384" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T111" id="Seg_386" n="HIAT:u" s="T100">
                  <ts e="T101" id="Seg_388" n="HIAT:w" s="T100">Ojunnarɨn</ts>
                  <nts id="Seg_389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_391" n="HIAT:w" s="T101">oksoːru</ts>
                  <nts id="Seg_392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_394" n="HIAT:w" s="T102">gɨmmɨta</ts>
                  <nts id="Seg_395" n="HIAT:ip">,</nts>
                  <nts id="Seg_396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_398" n="HIAT:w" s="T103">ü͡ölehinen</ts>
                  <nts id="Seg_399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_401" n="HIAT:w" s="T104">ojon</ts>
                  <nts id="Seg_402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_404" n="HIAT:w" s="T105">ku͡opput</ts>
                  <nts id="Seg_405" n="HIAT:ip">,</nts>
                  <nts id="Seg_406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_408" n="HIAT:w" s="T106">oldoːnunan</ts>
                  <nts id="Seg_409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_411" n="HIAT:w" s="T107">kulgaːgɨn</ts>
                  <nts id="Seg_412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_414" n="HIAT:w" s="T108">töbötün</ts>
                  <nts id="Seg_415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_417" n="HIAT:w" s="T109">ere</ts>
                  <nts id="Seg_418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_420" n="HIAT:w" s="T110">oksubut</ts>
                  <nts id="Seg_421" n="HIAT:ip">.</nts>
                  <nts id="Seg_422" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T118" id="Seg_424" n="HIAT:u" s="T111">
                  <ts e="T112" id="Seg_426" n="HIAT:w" s="T111">Ol</ts>
                  <nts id="Seg_427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_429" n="HIAT:w" s="T112">koru͡ota</ts>
                  <nts id="Seg_430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_432" n="HIAT:w" s="T113">bihillibititten</ts>
                  <nts id="Seg_433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_435" n="HIAT:w" s="T114">uskaːn</ts>
                  <nts id="Seg_436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_438" n="HIAT:w" s="T115">kulgaːgɨn</ts>
                  <nts id="Seg_439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_441" n="HIAT:w" s="T116">töbötö</ts>
                  <nts id="Seg_442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_444" n="HIAT:w" s="T117">kara</ts>
                  <nts id="Seg_445" n="HIAT:ip">.</nts>
                  <nts id="Seg_446" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T125" id="Seg_448" n="HIAT:u" s="T118">
                  <ts e="T119" id="Seg_450" n="HIAT:w" s="T118">Onton</ts>
                  <nts id="Seg_451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_453" n="HIAT:w" s="T119">emi͡e</ts>
                  <nts id="Seg_454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_456" n="HIAT:w" s="T120">ɨ͡aldʼɨbɨta</ts>
                  <nts id="Seg_457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_459" n="HIAT:w" s="T121">bu͡olan</ts>
                  <nts id="Seg_460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_462" n="HIAT:w" s="T122">kɨrsa</ts>
                  <nts id="Seg_463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_465" n="HIAT:w" s="T123">ojunun</ts>
                  <nts id="Seg_466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_468" n="HIAT:w" s="T124">ɨŋɨttarbɨt</ts>
                  <nts id="Seg_469" n="HIAT:ip">.</nts>
                  <nts id="Seg_470" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T131" id="Seg_472" n="HIAT:u" s="T125">
                  <ts e="T126" id="Seg_474" n="HIAT:w" s="T125">Elbek</ts>
                  <nts id="Seg_475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_477" n="HIAT:w" s="T126">kɨrsanɨ</ts>
                  <nts id="Seg_478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_480" n="HIAT:w" s="T127">ɨŋɨrɨ͡agɨn</ts>
                  <nts id="Seg_481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_483" n="HIAT:w" s="T128">kuttammɨt</ts>
                  <nts id="Seg_484" n="HIAT:ip">,</nts>
                  <nts id="Seg_485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_487" n="HIAT:w" s="T129">tiːsteːkter</ts>
                  <nts id="Seg_488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_490" n="HIAT:w" s="T130">di͡en</ts>
                  <nts id="Seg_491" n="HIAT:ip">.</nts>
                  <nts id="Seg_492" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T140" id="Seg_494" n="HIAT:u" s="T131">
                  <nts id="Seg_495" n="HIAT:ip">"</nts>
                  <ts e="T132" id="Seg_497" n="HIAT:w" s="T131">Ojunnarɨn</ts>
                  <nts id="Seg_498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_500" n="HIAT:w" s="T132">ölördökpüne</ts>
                  <nts id="Seg_501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_503" n="HIAT:w" s="T133">bejelere</ts>
                  <nts id="Seg_504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_506" n="HIAT:w" s="T134">da</ts>
                  <nts id="Seg_507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_509" n="HIAT:w" s="T135">kömüsküːr</ts>
                  <nts id="Seg_510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_512" n="HIAT:w" s="T136">kihite</ts>
                  <nts id="Seg_513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_515" n="HIAT:w" s="T137">hu͡ok</ts>
                  <nts id="Seg_516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_518" n="HIAT:w" s="T138">ölü͡öktere</ts>
                  <nts id="Seg_519" n="HIAT:ip">"</nts>
                  <nts id="Seg_520" n="HIAT:ip">,</nts>
                  <nts id="Seg_521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_523" n="HIAT:w" s="T139">di͡ebit</ts>
                  <nts id="Seg_524" n="HIAT:ip">.</nts>
                  <nts id="Seg_525" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T148" id="Seg_527" n="HIAT:u" s="T140">
                  <ts e="T141" id="Seg_529" n="HIAT:w" s="T140">Kɨrsatɨn</ts>
                  <nts id="Seg_530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_532" n="HIAT:w" s="T141">ojunun</ts>
                  <nts id="Seg_533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_535" n="HIAT:w" s="T142">oldoːnunan</ts>
                  <nts id="Seg_536" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_538" n="HIAT:w" s="T143">okson</ts>
                  <nts id="Seg_539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_541" n="HIAT:w" s="T144">kuturugun</ts>
                  <nts id="Seg_542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_544" n="HIAT:w" s="T145">töbötün</ts>
                  <nts id="Seg_545" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_547" n="HIAT:w" s="T146">ere</ts>
                  <nts id="Seg_548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_550" n="HIAT:w" s="T147">tappɨt</ts>
                  <nts id="Seg_551" n="HIAT:ip">.</nts>
                  <nts id="Seg_552" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T157" id="Seg_554" n="HIAT:u" s="T148">
                  <ts e="T149" id="Seg_556" n="HIAT:w" s="T148">Ol</ts>
                  <nts id="Seg_557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_559" n="HIAT:w" s="T149">ihin</ts>
                  <nts id="Seg_560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_562" n="HIAT:w" s="T150">oldoːn</ts>
                  <nts id="Seg_563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_565" n="HIAT:w" s="T151">koru͡otuttan</ts>
                  <nts id="Seg_566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_568" n="HIAT:w" s="T152">kɨrsa</ts>
                  <nts id="Seg_569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_571" n="HIAT:w" s="T153">koboloːk</ts>
                  <nts id="Seg_572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_574" n="HIAT:w" s="T154">bu͡olbut</ts>
                  <nts id="Seg_575" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_577" n="HIAT:w" s="T155">kuturugun</ts>
                  <nts id="Seg_578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_580" n="HIAT:w" s="T156">töbötüger</ts>
                  <nts id="Seg_581" n="HIAT:ip">.</nts>
                  <nts id="Seg_582" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T164" id="Seg_584" n="HIAT:u" s="T157">
                  <ts e="T158" id="Seg_586" n="HIAT:w" s="T157">Bugurduk</ts>
                  <nts id="Seg_587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_589" n="HIAT:w" s="T158">bu͡olbutun</ts>
                  <nts id="Seg_590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_592" n="HIAT:w" s="T159">genne</ts>
                  <nts id="Seg_593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_595" n="HIAT:w" s="T160">taŋara</ts>
                  <nts id="Seg_596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_598" n="HIAT:w" s="T161">körsübüt</ts>
                  <nts id="Seg_599" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_601" n="HIAT:w" s="T162">ol</ts>
                  <nts id="Seg_602" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_604" n="HIAT:w" s="T163">kihini</ts>
                  <nts id="Seg_605" n="HIAT:ip">.</nts>
                  <nts id="Seg_606" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T175" id="Seg_608" n="HIAT:u" s="T164">
                  <nts id="Seg_609" n="HIAT:ip">"</nts>
                  <ts e="T165" id="Seg_611" n="HIAT:w" s="T164">Itini</ts>
                  <nts id="Seg_612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_614" n="HIAT:w" s="T165">oŋostubut</ts>
                  <nts id="Seg_615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_617" n="HIAT:w" s="T166">ajɨːgar</ts>
                  <nts id="Seg_618" n="HIAT:ip">"</nts>
                  <nts id="Seg_619" n="HIAT:ip">,</nts>
                  <nts id="Seg_620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_622" n="HIAT:w" s="T167">di͡ebit</ts>
                  <nts id="Seg_623" n="HIAT:ip">,</nts>
                  <nts id="Seg_624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_625" n="HIAT:ip">"</nts>
                  <ts e="T169" id="Seg_627" n="HIAT:w" s="T168">en</ts>
                  <nts id="Seg_628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_630" n="HIAT:w" s="T169">bɨhɨːŋ</ts>
                  <nts id="Seg_631" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_633" n="HIAT:w" s="T170">oloŋko</ts>
                  <nts id="Seg_634" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_636" n="HIAT:w" s="T171">kurduk</ts>
                  <nts id="Seg_637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_639" n="HIAT:w" s="T172">ös</ts>
                  <nts id="Seg_640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_642" n="HIAT:w" s="T173">bu͡ollun</ts>
                  <nts id="Seg_643" n="HIAT:ip">"</nts>
                  <nts id="Seg_644" n="HIAT:ip">,</nts>
                  <nts id="Seg_645" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_647" n="HIAT:w" s="T174">di͡ebit</ts>
                  <nts id="Seg_648" n="HIAT:ip">.</nts>
                  <nts id="Seg_649" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T184" id="Seg_651" n="HIAT:u" s="T175">
                  <nts id="Seg_652" n="HIAT:ip">"</nts>
                  <ts e="T176" id="Seg_654" n="HIAT:w" s="T175">Bejeŋ</ts>
                  <nts id="Seg_655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_657" n="HIAT:w" s="T176">bu͡ollagɨna</ts>
                  <nts id="Seg_658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_660" n="HIAT:w" s="T177">kukaːkɨ</ts>
                  <nts id="Seg_661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_663" n="HIAT:w" s="T178">kötör</ts>
                  <nts id="Seg_664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_666" n="HIAT:w" s="T179">bu͡ola</ts>
                  <nts id="Seg_667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_669" n="HIAT:w" s="T180">hɨldʼan</ts>
                  <nts id="Seg_670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_672" n="HIAT:w" s="T181">ahɨlɨktanar</ts>
                  <nts id="Seg_673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_675" n="HIAT:w" s="T182">bu͡ol</ts>
                  <nts id="Seg_676" n="HIAT:ip">"</nts>
                  <nts id="Seg_677" n="HIAT:ip">,</nts>
                  <nts id="Seg_678" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_680" n="HIAT:w" s="T183">di͡ebit</ts>
                  <nts id="Seg_681" n="HIAT:ip">.</nts>
                  <nts id="Seg_682" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T191" id="Seg_684" n="HIAT:u" s="T184">
                  <ts e="T185" id="Seg_686" n="HIAT:w" s="T184">Ol</ts>
                  <nts id="Seg_687" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_689" n="HIAT:w" s="T185">ihin</ts>
                  <nts id="Seg_690" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_692" n="HIAT:w" s="T186">kukaːkɨ</ts>
                  <nts id="Seg_693" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_695" n="HIAT:w" s="T187">paːs</ts>
                  <nts id="Seg_696" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_698" n="HIAT:w" s="T188">meŋi͡etin</ts>
                  <nts id="Seg_699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_701" n="HIAT:w" s="T189">hiː</ts>
                  <nts id="Seg_702" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_704" n="HIAT:w" s="T190">hɨldʼar</ts>
                  <nts id="Seg_705" n="HIAT:ip">.</nts>
                  <nts id="Seg_706" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T191" id="Seg_707" n="sc" s="T0">
               <ts e="T1" id="Seg_709" n="e" s="T0">Bɨlɨr </ts>
               <ts e="T2" id="Seg_711" n="e" s="T1">biːr </ts>
               <ts e="T3" id="Seg_713" n="e" s="T2">kihi </ts>
               <ts e="T4" id="Seg_715" n="e" s="T3">olorbut. </ts>
               <ts e="T5" id="Seg_717" n="e" s="T4">Bu </ts>
               <ts e="T6" id="Seg_719" n="e" s="T5">kihi </ts>
               <ts e="T7" id="Seg_721" n="e" s="T6">hanaːbɨt: </ts>
               <ts e="T8" id="Seg_723" n="e" s="T7">"Kɨrsanɨ </ts>
               <ts e="T9" id="Seg_725" n="e" s="T8">barɨtɨn, </ts>
               <ts e="T10" id="Seg_727" n="e" s="T9">uskaːnɨ </ts>
               <ts e="T11" id="Seg_729" n="e" s="T10">barɨtɨn </ts>
               <ts e="T12" id="Seg_731" n="e" s="T11">ölörön </ts>
               <ts e="T13" id="Seg_733" n="e" s="T12">keːhi͡ekke, </ts>
               <ts e="T14" id="Seg_735" n="e" s="T13">biːri </ts>
               <ts e="T15" id="Seg_737" n="e" s="T14">da </ts>
               <ts e="T16" id="Seg_739" n="e" s="T15">ordorumaː", </ts>
               <ts e="T17" id="Seg_741" n="e" s="T16">di͡ebit. </ts>
               <ts e="T18" id="Seg_743" n="e" s="T17">U͡olugar </ts>
               <ts e="T19" id="Seg_745" n="e" s="T18">eppit: </ts>
               <ts e="T20" id="Seg_747" n="e" s="T19">"Min </ts>
               <ts e="T21" id="Seg_749" n="e" s="T20">ɨ͡aldʼɨbɨt </ts>
               <ts e="T22" id="Seg_751" n="e" s="T21">bu͡oluːm, </ts>
               <ts e="T23" id="Seg_753" n="e" s="T22">on </ts>
               <ts e="T24" id="Seg_755" n="e" s="T23">baraŋŋɨn </ts>
               <ts e="T25" id="Seg_757" n="e" s="T24">uskaːn </ts>
               <ts e="T26" id="Seg_759" n="e" s="T25">ojunun </ts>
               <ts e="T27" id="Seg_761" n="e" s="T26">egelʼ. </ts>
               <ts e="T28" id="Seg_763" n="e" s="T27">Onton </ts>
               <ts e="T29" id="Seg_765" n="e" s="T28">kini͡eke </ts>
               <ts e="T30" id="Seg_767" n="e" s="T29">löčü͡ögünen </ts>
               <ts e="T31" id="Seg_769" n="e" s="T30">barɨ </ts>
               <ts e="T32" id="Seg_771" n="e" s="T31">uskaːnɨ </ts>
               <ts e="T33" id="Seg_773" n="e" s="T32">üːren </ts>
               <ts e="T34" id="Seg_775" n="e" s="T33">egel. </ts>
               <ts e="T35" id="Seg_777" n="e" s="T34">U͡ola </ts>
               <ts e="T36" id="Seg_779" n="e" s="T35">barbɨta </ts>
               <ts e="T37" id="Seg_781" n="e" s="T36">biːr </ts>
               <ts e="T38" id="Seg_783" n="e" s="T37">uskaːn </ts>
               <ts e="T39" id="Seg_785" n="e" s="T38">oloror. </ts>
               <ts e="T40" id="Seg_787" n="e" s="T39">"Dʼe </ts>
               <ts e="T41" id="Seg_789" n="e" s="T40">kaja, </ts>
               <ts e="T42" id="Seg_791" n="e" s="T41">ojuŋŋut </ts>
               <ts e="T43" id="Seg_793" n="e" s="T42">kannanɨj", </ts>
               <ts e="T44" id="Seg_795" n="e" s="T43">diːr. </ts>
               <ts e="T45" id="Seg_797" n="e" s="T44">"Onno </ts>
               <ts e="T46" id="Seg_799" n="e" s="T45">baːr", </ts>
               <ts e="T47" id="Seg_801" n="e" s="T46">di͡en </ts>
               <ts e="T48" id="Seg_803" n="e" s="T47">ɨjan </ts>
               <ts e="T49" id="Seg_805" n="e" s="T48">bi͡erer. </ts>
               <ts e="T50" id="Seg_807" n="e" s="T49">Baran: </ts>
               <ts e="T51" id="Seg_809" n="e" s="T50">"Dʼe </ts>
               <ts e="T52" id="Seg_811" n="e" s="T51">abɨraː", </ts>
               <ts e="T53" id="Seg_813" n="e" s="T52">diːr, </ts>
               <ts e="T54" id="Seg_815" n="e" s="T53">"agam </ts>
               <ts e="T55" id="Seg_817" n="e" s="T54">ölöːrü </ts>
               <ts e="T56" id="Seg_819" n="e" s="T55">ɨŋɨrtarda, </ts>
               <ts e="T57" id="Seg_821" n="e" s="T56">kömölöhön </ts>
               <ts e="T58" id="Seg_823" n="e" s="T57">kör", </ts>
               <ts e="T59" id="Seg_825" n="e" s="T58">diːr. </ts>
               <ts e="T60" id="Seg_827" n="e" s="T59">Bu </ts>
               <ts e="T61" id="Seg_829" n="e" s="T60">biri͡emege </ts>
               <ts e="T62" id="Seg_831" n="e" s="T61">eter </ts>
               <ts e="T63" id="Seg_833" n="e" s="T62">kaːlaːččɨ </ts>
               <ts e="T64" id="Seg_835" n="e" s="T63">kihi </ts>
               <ts e="T65" id="Seg_837" n="e" s="T64">emeːksiniger: </ts>
               <ts e="T66" id="Seg_839" n="e" s="T65">"Munnʼuhunnaktarɨna </ts>
               <ts e="T67" id="Seg_841" n="e" s="T66">en </ts>
               <ts e="T68" id="Seg_843" n="e" s="T67">aːŋŋɨn </ts>
               <ts e="T69" id="Seg_845" n="e" s="T68">battatan </ts>
               <ts e="T70" id="Seg_847" n="e" s="T69">keːheːr", </ts>
               <ts e="T71" id="Seg_849" n="e" s="T70">diːr. </ts>
               <ts e="T72" id="Seg_851" n="e" s="T71">Ojunnara </ts>
               <ts e="T73" id="Seg_853" n="e" s="T72">kelen, </ts>
               <ts e="T74" id="Seg_855" n="e" s="T73">uskaːn </ts>
               <ts e="T75" id="Seg_857" n="e" s="T74">bögönü </ts>
               <ts e="T76" id="Seg_859" n="e" s="T75">löčü͡ögünen </ts>
               <ts e="T77" id="Seg_861" n="e" s="T76">egeler. </ts>
               <ts e="T78" id="Seg_863" n="e" s="T77">Bu </ts>
               <ts e="T79" id="Seg_865" n="e" s="T78">egelen </ts>
               <ts e="T80" id="Seg_867" n="e" s="T79">kɨːrardagɨna </ts>
               <ts e="T81" id="Seg_869" n="e" s="T80">emeːksin </ts>
               <ts e="T82" id="Seg_871" n="e" s="T81">aːnɨ </ts>
               <ts e="T83" id="Seg_873" n="e" s="T82">battatar, </ts>
               <ts e="T84" id="Seg_875" n="e" s="T83">onton </ts>
               <ts e="T85" id="Seg_877" n="e" s="T84">ɨ͡aldʼaːččɨ </ts>
               <ts e="T86" id="Seg_879" n="e" s="T85">bu͡olbut: </ts>
               <ts e="T87" id="Seg_881" n="e" s="T86">"Oldoːnno", </ts>
               <ts e="T88" id="Seg_883" n="e" s="T87">di͡ebit. </ts>
               <ts e="T89" id="Seg_885" n="e" s="T88">Emeːksin </ts>
               <ts e="T90" id="Seg_887" n="e" s="T89">oldoːnu </ts>
               <ts e="T91" id="Seg_889" n="e" s="T90">bi͡erbit. </ts>
               <ts e="T92" id="Seg_891" n="e" s="T91">Dʼe </ts>
               <ts e="T93" id="Seg_893" n="e" s="T92">onon </ts>
               <ts e="T94" id="Seg_895" n="e" s="T93">tura </ts>
               <ts e="T95" id="Seg_897" n="e" s="T94">ekkireːn </ts>
               <ts e="T96" id="Seg_899" n="e" s="T95">ogonnʼor </ts>
               <ts e="T97" id="Seg_901" n="e" s="T96">dʼe </ts>
               <ts e="T98" id="Seg_903" n="e" s="T97">uskaːnnarɨ </ts>
               <ts e="T99" id="Seg_905" n="e" s="T98">ölörtöːbüt </ts>
               <ts e="T100" id="Seg_907" n="e" s="T99">eːt. </ts>
               <ts e="T101" id="Seg_909" n="e" s="T100">Ojunnarɨn </ts>
               <ts e="T102" id="Seg_911" n="e" s="T101">oksoːru </ts>
               <ts e="T103" id="Seg_913" n="e" s="T102">gɨmmɨta, </ts>
               <ts e="T104" id="Seg_915" n="e" s="T103">ü͡ölehinen </ts>
               <ts e="T105" id="Seg_917" n="e" s="T104">ojon </ts>
               <ts e="T106" id="Seg_919" n="e" s="T105">ku͡opput, </ts>
               <ts e="T107" id="Seg_921" n="e" s="T106">oldoːnunan </ts>
               <ts e="T108" id="Seg_923" n="e" s="T107">kulgaːgɨn </ts>
               <ts e="T109" id="Seg_925" n="e" s="T108">töbötün </ts>
               <ts e="T110" id="Seg_927" n="e" s="T109">ere </ts>
               <ts e="T111" id="Seg_929" n="e" s="T110">oksubut. </ts>
               <ts e="T112" id="Seg_931" n="e" s="T111">Ol </ts>
               <ts e="T113" id="Seg_933" n="e" s="T112">koru͡ota </ts>
               <ts e="T114" id="Seg_935" n="e" s="T113">bihillibititten </ts>
               <ts e="T115" id="Seg_937" n="e" s="T114">uskaːn </ts>
               <ts e="T116" id="Seg_939" n="e" s="T115">kulgaːgɨn </ts>
               <ts e="T117" id="Seg_941" n="e" s="T116">töbötö </ts>
               <ts e="T118" id="Seg_943" n="e" s="T117">kara. </ts>
               <ts e="T119" id="Seg_945" n="e" s="T118">Onton </ts>
               <ts e="T120" id="Seg_947" n="e" s="T119">emi͡e </ts>
               <ts e="T121" id="Seg_949" n="e" s="T120">ɨ͡aldʼɨbɨta </ts>
               <ts e="T122" id="Seg_951" n="e" s="T121">bu͡olan </ts>
               <ts e="T123" id="Seg_953" n="e" s="T122">kɨrsa </ts>
               <ts e="T124" id="Seg_955" n="e" s="T123">ojunun </ts>
               <ts e="T125" id="Seg_957" n="e" s="T124">ɨŋɨttarbɨt. </ts>
               <ts e="T126" id="Seg_959" n="e" s="T125">Elbek </ts>
               <ts e="T127" id="Seg_961" n="e" s="T126">kɨrsanɨ </ts>
               <ts e="T128" id="Seg_963" n="e" s="T127">ɨŋɨrɨ͡agɨn </ts>
               <ts e="T129" id="Seg_965" n="e" s="T128">kuttammɨt, </ts>
               <ts e="T130" id="Seg_967" n="e" s="T129">tiːsteːkter </ts>
               <ts e="T131" id="Seg_969" n="e" s="T130">di͡en. </ts>
               <ts e="T132" id="Seg_971" n="e" s="T131">"Ojunnarɨn </ts>
               <ts e="T133" id="Seg_973" n="e" s="T132">ölördökpüne </ts>
               <ts e="T134" id="Seg_975" n="e" s="T133">bejelere </ts>
               <ts e="T135" id="Seg_977" n="e" s="T134">da </ts>
               <ts e="T136" id="Seg_979" n="e" s="T135">kömüsküːr </ts>
               <ts e="T137" id="Seg_981" n="e" s="T136">kihite </ts>
               <ts e="T138" id="Seg_983" n="e" s="T137">hu͡ok </ts>
               <ts e="T139" id="Seg_985" n="e" s="T138">ölü͡öktere", </ts>
               <ts e="T140" id="Seg_987" n="e" s="T139">di͡ebit. </ts>
               <ts e="T141" id="Seg_989" n="e" s="T140">Kɨrsatɨn </ts>
               <ts e="T142" id="Seg_991" n="e" s="T141">ojunun </ts>
               <ts e="T143" id="Seg_993" n="e" s="T142">oldoːnunan </ts>
               <ts e="T144" id="Seg_995" n="e" s="T143">okson </ts>
               <ts e="T145" id="Seg_997" n="e" s="T144">kuturugun </ts>
               <ts e="T146" id="Seg_999" n="e" s="T145">töbötün </ts>
               <ts e="T147" id="Seg_1001" n="e" s="T146">ere </ts>
               <ts e="T148" id="Seg_1003" n="e" s="T147">tappɨt. </ts>
               <ts e="T149" id="Seg_1005" n="e" s="T148">Ol </ts>
               <ts e="T150" id="Seg_1007" n="e" s="T149">ihin </ts>
               <ts e="T151" id="Seg_1009" n="e" s="T150">oldoːn </ts>
               <ts e="T152" id="Seg_1011" n="e" s="T151">koru͡otuttan </ts>
               <ts e="T153" id="Seg_1013" n="e" s="T152">kɨrsa </ts>
               <ts e="T154" id="Seg_1015" n="e" s="T153">koboloːk </ts>
               <ts e="T155" id="Seg_1017" n="e" s="T154">bu͡olbut </ts>
               <ts e="T156" id="Seg_1019" n="e" s="T155">kuturugun </ts>
               <ts e="T157" id="Seg_1021" n="e" s="T156">töbötüger. </ts>
               <ts e="T158" id="Seg_1023" n="e" s="T157">Bugurduk </ts>
               <ts e="T159" id="Seg_1025" n="e" s="T158">bu͡olbutun </ts>
               <ts e="T160" id="Seg_1027" n="e" s="T159">genne </ts>
               <ts e="T161" id="Seg_1029" n="e" s="T160">taŋara </ts>
               <ts e="T162" id="Seg_1031" n="e" s="T161">körsübüt </ts>
               <ts e="T163" id="Seg_1033" n="e" s="T162">ol </ts>
               <ts e="T164" id="Seg_1035" n="e" s="T163">kihini. </ts>
               <ts e="T165" id="Seg_1037" n="e" s="T164">"Itini </ts>
               <ts e="T166" id="Seg_1039" n="e" s="T165">oŋostubut </ts>
               <ts e="T167" id="Seg_1041" n="e" s="T166">ajɨːgar", </ts>
               <ts e="T168" id="Seg_1043" n="e" s="T167">di͡ebit, </ts>
               <ts e="T169" id="Seg_1045" n="e" s="T168">"en </ts>
               <ts e="T170" id="Seg_1047" n="e" s="T169">bɨhɨːŋ </ts>
               <ts e="T171" id="Seg_1049" n="e" s="T170">oloŋko </ts>
               <ts e="T172" id="Seg_1051" n="e" s="T171">kurduk </ts>
               <ts e="T173" id="Seg_1053" n="e" s="T172">ös </ts>
               <ts e="T174" id="Seg_1055" n="e" s="T173">bu͡ollun", </ts>
               <ts e="T175" id="Seg_1057" n="e" s="T174">di͡ebit. </ts>
               <ts e="T176" id="Seg_1059" n="e" s="T175">"Bejeŋ </ts>
               <ts e="T177" id="Seg_1061" n="e" s="T176">bu͡ollagɨna </ts>
               <ts e="T178" id="Seg_1063" n="e" s="T177">kukaːkɨ </ts>
               <ts e="T179" id="Seg_1065" n="e" s="T178">kötör </ts>
               <ts e="T180" id="Seg_1067" n="e" s="T179">bu͡ola </ts>
               <ts e="T181" id="Seg_1069" n="e" s="T180">hɨldʼan </ts>
               <ts e="T182" id="Seg_1071" n="e" s="T181">ahɨlɨktanar </ts>
               <ts e="T183" id="Seg_1073" n="e" s="T182">bu͡ol", </ts>
               <ts e="T184" id="Seg_1075" n="e" s="T183">di͡ebit. </ts>
               <ts e="T185" id="Seg_1077" n="e" s="T184">Ol </ts>
               <ts e="T186" id="Seg_1079" n="e" s="T185">ihin </ts>
               <ts e="T187" id="Seg_1081" n="e" s="T186">kukaːkɨ </ts>
               <ts e="T188" id="Seg_1083" n="e" s="T187">paːs </ts>
               <ts e="T189" id="Seg_1085" n="e" s="T188">meŋi͡etin </ts>
               <ts e="T190" id="Seg_1087" n="e" s="T189">hiː </ts>
               <ts e="T191" id="Seg_1089" n="e" s="T190">hɨldʼar. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_1090" s="T0">BaRD_1931_OldManHaresPolarFoxes_flk.001</ta>
            <ta e="T7" id="Seg_1091" s="T4">BaRD_1931_OldManHaresPolarFoxes_flk.002</ta>
            <ta e="T17" id="Seg_1092" s="T7">BaRD_1931_OldManHaresPolarFoxes_flk.003</ta>
            <ta e="T19" id="Seg_1093" s="T17">BaRD_1931_OldManHaresPolarFoxes_flk.004</ta>
            <ta e="T27" id="Seg_1094" s="T19">BaRD_1931_OldManHaresPolarFoxes_flk.005</ta>
            <ta e="T34" id="Seg_1095" s="T27">BaRD_1931_OldManHaresPolarFoxes_flk.006</ta>
            <ta e="T39" id="Seg_1096" s="T34">BaRD_1931_OldManHaresPolarFoxes_flk.007</ta>
            <ta e="T44" id="Seg_1097" s="T39">BaRD_1931_OldManHaresPolarFoxes_flk.008</ta>
            <ta e="T49" id="Seg_1098" s="T44">BaRD_1931_OldManHaresPolarFoxes_flk.009</ta>
            <ta e="T50" id="Seg_1099" s="T49">BaRD_1931_OldManHaresPolarFoxes_flk.010</ta>
            <ta e="T59" id="Seg_1100" s="T50">BaRD_1931_OldManHaresPolarFoxes_flk.011</ta>
            <ta e="T65" id="Seg_1101" s="T59">BaRD_1931_OldManHaresPolarFoxes_flk.012</ta>
            <ta e="T71" id="Seg_1102" s="T65">BaRD_1931_OldManHaresPolarFoxes_flk.013</ta>
            <ta e="T77" id="Seg_1103" s="T71">BaRD_1931_OldManHaresPolarFoxes_flk.014</ta>
            <ta e="T86" id="Seg_1104" s="T77">BaRD_1931_OldManHaresPolarFoxes_flk.015</ta>
            <ta e="T88" id="Seg_1105" s="T86">BaRD_1931_OldManHaresPolarFoxes_flk.016</ta>
            <ta e="T91" id="Seg_1106" s="T88">BaRD_1931_OldManHaresPolarFoxes_flk.017</ta>
            <ta e="T100" id="Seg_1107" s="T91">BaRD_1931_OldManHaresPolarFoxes_flk.018</ta>
            <ta e="T111" id="Seg_1108" s="T100">BaRD_1931_OldManHaresPolarFoxes_flk.019</ta>
            <ta e="T118" id="Seg_1109" s="T111">BaRD_1931_OldManHaresPolarFoxes_flk.020</ta>
            <ta e="T125" id="Seg_1110" s="T118">BaRD_1931_OldManHaresPolarFoxes_flk.021</ta>
            <ta e="T131" id="Seg_1111" s="T125">BaRD_1931_OldManHaresPolarFoxes_flk.022</ta>
            <ta e="T140" id="Seg_1112" s="T131">BaRD_1931_OldManHaresPolarFoxes_flk.023</ta>
            <ta e="T148" id="Seg_1113" s="T140">BaRD_1931_OldManHaresPolarFoxes_flk.024</ta>
            <ta e="T157" id="Seg_1114" s="T148">BaRD_1931_OldManHaresPolarFoxes_flk.025</ta>
            <ta e="T164" id="Seg_1115" s="T157">BaRD_1931_OldManHaresPolarFoxes_flk.026</ta>
            <ta e="T175" id="Seg_1116" s="T164">BaRD_1931_OldManHaresPolarFoxes_flk.027</ta>
            <ta e="T184" id="Seg_1117" s="T175">BaRD_1931_OldManHaresPolarFoxes_flk.028</ta>
            <ta e="T191" id="Seg_1118" s="T184">BaRD_1931_OldManHaresPolarFoxes_flk.029</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T4" id="Seg_1119" s="T0">Былыр биир киһи олорбут.</ta>
            <ta e="T7" id="Seg_1120" s="T4">Бу киһи һанаабыт: </ta>
            <ta e="T17" id="Seg_1121" s="T7">"Кырсаны барытын, ускааны барытын өлөрөн кээһиэккэ, биири да ордорумаа", — диэбит.</ta>
            <ta e="T19" id="Seg_1122" s="T17">Уолугар эппит: </ta>
            <ta e="T27" id="Seg_1123" s="T19">"Мин ыалдьыбыт буолуум, он бараҥҥын ускаан ойунун эгэль.</ta>
            <ta e="T34" id="Seg_1124" s="T27">Онтон киниэхэ лөчүөгүнэн бары ускааны үүрэн эгэл".</ta>
            <ta e="T39" id="Seg_1125" s="T34">Уола барбыта биир ускаан олорор.</ta>
            <ta e="T44" id="Seg_1126" s="T39">— Дьэ кайа, ойуҥҥут каннаный? — диир.</ta>
            <ta e="T49" id="Seg_1127" s="T44">— Онно баар, — диэн ыйан биэрэр.</ta>
            <ta e="T50" id="Seg_1128" s="T49">Баран:</ta>
            <ta e="T59" id="Seg_1129" s="T50">— Дьэ абыраа, — диир, — агам өлөөрү ыҥыртарда, көмөлөһөн көр, — диир.</ta>
            <ta e="T65" id="Seg_1130" s="T59">Бу бириэмэгэ этэр каалааччы киһи эмээксинигэр:</ta>
            <ta e="T71" id="Seg_1131" s="T65">— Мунньуһуннактарына эн ааҥҥын баттатан кээһээр, — диир.</ta>
            <ta e="T77" id="Seg_1132" s="T71">Ойуннара кэлэн, ускаан бөгөнү лөчүөгүнэн эгэлэр.</ta>
            <ta e="T86" id="Seg_1133" s="T77">Бу эгэлэн кыырардагына эмээксин ааны баттатар, онтон ыалдьааччы буолбут: </ta>
            <ta e="T88" id="Seg_1134" s="T86">"Олдоонно!" — диэбит.</ta>
            <ta e="T91" id="Seg_1135" s="T88">Эмээксин олдоону биэрбит.</ta>
            <ta e="T100" id="Seg_1136" s="T91">Дьэ онон тура эккирээн огонньор дьэ ускааннары өлөртөөбүт ээт.</ta>
            <ta e="T111" id="Seg_1137" s="T100">Ойуннарын оксоору гыммыта, үөлэһинэн ойон куоппут, олдоонунан кулгаагын төбөтүн эрэ оксубут.</ta>
            <ta e="T118" id="Seg_1138" s="T111">Ол коруота биһиллибититтэн ускаан кулгаагын төбөтө кара.</ta>
            <ta e="T125" id="Seg_1139" s="T118">Онтон эмиэ ыалдьыбыта буолан кырса ойунун ыҥыттарбыт.</ta>
            <ta e="T131" id="Seg_1140" s="T125">Элбэк кырсаны ыҥырыагын куттаммыт, тиистээктэр диэн.</ta>
            <ta e="T140" id="Seg_1141" s="T131">"Ойуннарын өлөрдөкпүнэ бэйэлэрэ да көмүскүүр киһитэ һуох өлүөктэрэ", — диэбит.</ta>
            <ta e="T148" id="Seg_1142" s="T140">Кырсатын ойунун олдоонунан оксон кутуругун төбөтүн эрэ таппыт.</ta>
            <ta e="T157" id="Seg_1143" s="T148">Ол иһин олдоон коруотуттан кырса коболоок буолбут кутуругун төбөтүгэр.</ta>
            <ta e="T164" id="Seg_1144" s="T157">Бугурдук буолбутун гэннэ таҥара көрсүбүт ол киһини.</ta>
            <ta e="T175" id="Seg_1145" s="T164">"Итини оҥостубут айыыгар, — диэбит, — эн быһыыҥ олоҥко курдук өс буоллун, — диэбит.</ta>
            <ta e="T184" id="Seg_1146" s="T175">— Бэйэҥ буоллагына кукаакы көтөр буола һылдьан аһылыктанар буол", — диэбит.</ta>
            <ta e="T191" id="Seg_1147" s="T184">Ол иһин кукаакы паас мэҥиэтин һии һылдьар.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_1148" s="T0">Bɨlɨr biːr kihi olorbut. </ta>
            <ta e="T7" id="Seg_1149" s="T4">Bu kihi hanaːbɨt: </ta>
            <ta e="T17" id="Seg_1150" s="T7">"Kɨrsanɨ barɨtɨn, uskaːnɨ barɨtɨn ölörön keːhi͡ekke, biːri da ordorumaː", di͡ebit. </ta>
            <ta e="T19" id="Seg_1151" s="T17">U͡olugar eppit: </ta>
            <ta e="T27" id="Seg_1152" s="T19">"Min ɨ͡aldʼɨbɨt bu͡oluːm, on baraŋŋɨn uskaːn ojunun egelʼ. </ta>
            <ta e="T34" id="Seg_1153" s="T27">Onton kini͡eke löčü͡ögünen barɨ uskaːnɨ üːren egel." </ta>
            <ta e="T39" id="Seg_1154" s="T34">U͡ola barbɨta biːr uskaːn oloror. </ta>
            <ta e="T44" id="Seg_1155" s="T39">"Dʼe kaja, ojuŋŋut kannanɨj", diːr. </ta>
            <ta e="T49" id="Seg_1156" s="T44">"Onno baːr", di͡en ɨjan bi͡erer. </ta>
            <ta e="T50" id="Seg_1157" s="T49">Baran:</ta>
            <ta e="T59" id="Seg_1158" s="T50">"Dʼe abɨraː", diːr, "agam ölöːrü ɨŋɨrtarda, kömölöhön kör", diːr. </ta>
            <ta e="T65" id="Seg_1159" s="T59">Bu biri͡emege eter kaːlaːččɨ kihi emeːksiniger:</ta>
            <ta e="T71" id="Seg_1160" s="T65">"Munnʼuhunnaktarɨna en aːŋŋɨn battatan keːheːr", diːr. </ta>
            <ta e="T77" id="Seg_1161" s="T71">Ojunnara kelen, uskaːn bögönü löčü͡ögünen egeler. </ta>
            <ta e="T86" id="Seg_1162" s="T77">Bu egelen kɨːrardagɨna emeːksin aːnɨ battatar, onton ɨ͡aldʼaːččɨ bu͡olbut: </ta>
            <ta e="T88" id="Seg_1163" s="T86">"Oldoːnno", di͡ebit. </ta>
            <ta e="T91" id="Seg_1164" s="T88">Emeːksin oldoːnu bi͡erbit. </ta>
            <ta e="T100" id="Seg_1165" s="T91">Dʼe onon tura ekkireːn ogonnʼor dʼe uskaːnnarɨ ölörtöːbüt eːt. </ta>
            <ta e="T111" id="Seg_1166" s="T100">Ojunnarɨn oksoːru gɨmmɨta, ü͡ölehinen ojon ku͡opput, oldoːnunan kulgaːgɨn töbötün ere oksubut. </ta>
            <ta e="T118" id="Seg_1167" s="T111">Ol koru͡ota bihillibititten uskaːn kulgaːgɨn töbötö kara. </ta>
            <ta e="T125" id="Seg_1168" s="T118">Onton emi͡e ɨ͡aldʼɨbɨta bu͡olan kɨrsa ojunun ɨŋɨttarbɨt. </ta>
            <ta e="T131" id="Seg_1169" s="T125">Elbek kɨrsanɨ ɨŋɨrɨ͡agɨn kuttammɨt, tiːsteːkter di͡en. </ta>
            <ta e="T140" id="Seg_1170" s="T131">"Ojunnarɨn ölördökpüne bejelere da kömüsküːr kihite hu͡ok ölü͡öktere", di͡ebit. </ta>
            <ta e="T148" id="Seg_1171" s="T140">Kɨrsatɨn ojunun oldoːnunan okson kuturugun töbötün ere tappɨt. </ta>
            <ta e="T157" id="Seg_1172" s="T148">Ol ihin oldoːn koru͡otuttan kɨrsa koboloːk bu͡olbut kuturugun töbötüger. </ta>
            <ta e="T164" id="Seg_1173" s="T157">Bugurduk bu͡olbutun genne taŋara körsübüt ol kihini. </ta>
            <ta e="T175" id="Seg_1174" s="T164">"Itini oŋostubut ajɨːgar", di͡ebit, "en bɨhɨːŋ oloŋko kurduk ös bu͡ollun", di͡ebit. </ta>
            <ta e="T184" id="Seg_1175" s="T175">"Bejeŋ bu͡ollagɨna kukaːkɨ kötör bu͡ola hɨldʼan ahɨlɨktanar bu͡ol", di͡ebit. </ta>
            <ta e="T191" id="Seg_1176" s="T184">Ol ihin kukaːkɨ paːs meŋi͡etin hiː hɨldʼar. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_1177" s="T0">bɨlɨr</ta>
            <ta e="T2" id="Seg_1178" s="T1">biːr</ta>
            <ta e="T3" id="Seg_1179" s="T2">kihi</ta>
            <ta e="T4" id="Seg_1180" s="T3">olor-but</ta>
            <ta e="T5" id="Seg_1181" s="T4">bu</ta>
            <ta e="T6" id="Seg_1182" s="T5">kihi</ta>
            <ta e="T7" id="Seg_1183" s="T6">hanaː-bɨt</ta>
            <ta e="T8" id="Seg_1184" s="T7">kɨrsa-nɨ</ta>
            <ta e="T9" id="Seg_1185" s="T8">barɨ-tɨ-n</ta>
            <ta e="T10" id="Seg_1186" s="T9">uskaːn-ɨ</ta>
            <ta e="T11" id="Seg_1187" s="T10">barɨ-tɨ-n</ta>
            <ta e="T12" id="Seg_1188" s="T11">ölör-ön</ta>
            <ta e="T13" id="Seg_1189" s="T12">keːh-i͡ek-ke</ta>
            <ta e="T14" id="Seg_1190" s="T13">biːr-i</ta>
            <ta e="T15" id="Seg_1191" s="T14">da</ta>
            <ta e="T16" id="Seg_1192" s="T15">ord-o-r-u-maː</ta>
            <ta e="T17" id="Seg_1193" s="T16">di͡e-bit</ta>
            <ta e="T18" id="Seg_1194" s="T17">u͡ol-u-gar</ta>
            <ta e="T19" id="Seg_1195" s="T18">ep-pit</ta>
            <ta e="T20" id="Seg_1196" s="T19">min</ta>
            <ta e="T21" id="Seg_1197" s="T20">ɨ͡aldʼ-ɨ-bɨt</ta>
            <ta e="T22" id="Seg_1198" s="T21">bu͡ol-uːm</ta>
            <ta e="T23" id="Seg_1199" s="T22">on</ta>
            <ta e="T24" id="Seg_1200" s="T23">bar-aŋ-ŋɨn</ta>
            <ta e="T25" id="Seg_1201" s="T24">uskaːn</ta>
            <ta e="T26" id="Seg_1202" s="T25">ojun-u-n</ta>
            <ta e="T27" id="Seg_1203" s="T26">egelʼ</ta>
            <ta e="T28" id="Seg_1204" s="T27">onton</ta>
            <ta e="T29" id="Seg_1205" s="T28">kini͡e-ke</ta>
            <ta e="T30" id="Seg_1206" s="T29">löčü͡ög-ü-nen</ta>
            <ta e="T31" id="Seg_1207" s="T30">barɨ</ta>
            <ta e="T32" id="Seg_1208" s="T31">uskaːn-ɨ</ta>
            <ta e="T33" id="Seg_1209" s="T32">üːr-en</ta>
            <ta e="T34" id="Seg_1210" s="T33">egel</ta>
            <ta e="T35" id="Seg_1211" s="T34">u͡ol-a</ta>
            <ta e="T36" id="Seg_1212" s="T35">bar-bɨt-a</ta>
            <ta e="T37" id="Seg_1213" s="T36">biːr</ta>
            <ta e="T38" id="Seg_1214" s="T37">uskaːn</ta>
            <ta e="T39" id="Seg_1215" s="T38">olor-or</ta>
            <ta e="T40" id="Seg_1216" s="T39">dʼe</ta>
            <ta e="T41" id="Seg_1217" s="T40">kaja</ta>
            <ta e="T42" id="Seg_1218" s="T41">ojuŋ-ŋut</ta>
            <ta e="T43" id="Seg_1219" s="T42">kannan=ɨj</ta>
            <ta e="T44" id="Seg_1220" s="T43">diː-r</ta>
            <ta e="T45" id="Seg_1221" s="T44">onno</ta>
            <ta e="T46" id="Seg_1222" s="T45">baːr</ta>
            <ta e="T47" id="Seg_1223" s="T46">di͡e-n</ta>
            <ta e="T48" id="Seg_1224" s="T47">ɨj-an</ta>
            <ta e="T49" id="Seg_1225" s="T48">bi͡er-er</ta>
            <ta e="T50" id="Seg_1226" s="T49">bar-an</ta>
            <ta e="T51" id="Seg_1227" s="T50">dʼe</ta>
            <ta e="T52" id="Seg_1228" s="T51">abɨraː</ta>
            <ta e="T53" id="Seg_1229" s="T52">diː-r</ta>
            <ta e="T54" id="Seg_1230" s="T53">aga-m</ta>
            <ta e="T55" id="Seg_1231" s="T54">öl-öːrü</ta>
            <ta e="T56" id="Seg_1232" s="T55">ɨŋɨr-tar-d-a</ta>
            <ta e="T57" id="Seg_1233" s="T56">kömölöh-ön</ta>
            <ta e="T58" id="Seg_1234" s="T57">kör</ta>
            <ta e="T59" id="Seg_1235" s="T58">diː-r</ta>
            <ta e="T60" id="Seg_1236" s="T59">bu</ta>
            <ta e="T61" id="Seg_1237" s="T60">biri͡eme-ge</ta>
            <ta e="T62" id="Seg_1238" s="T61">et-er</ta>
            <ta e="T63" id="Seg_1239" s="T62">kaːl-aːččɨ</ta>
            <ta e="T64" id="Seg_1240" s="T63">kihi</ta>
            <ta e="T65" id="Seg_1241" s="T64">emeːksin-i-ger</ta>
            <ta e="T66" id="Seg_1242" s="T65">munnʼuh-u-n-nak-tarɨna</ta>
            <ta e="T67" id="Seg_1243" s="T66">en</ta>
            <ta e="T68" id="Seg_1244" s="T67">aːŋ-ŋɨ-n</ta>
            <ta e="T69" id="Seg_1245" s="T68">batt-a-t-an</ta>
            <ta e="T70" id="Seg_1246" s="T69">keːh-eːr</ta>
            <ta e="T71" id="Seg_1247" s="T70">diː-r</ta>
            <ta e="T72" id="Seg_1248" s="T71">ojun-nara</ta>
            <ta e="T73" id="Seg_1249" s="T72">kel-en</ta>
            <ta e="T74" id="Seg_1250" s="T73">uskaːn</ta>
            <ta e="T75" id="Seg_1251" s="T74">bögö-nü</ta>
            <ta e="T76" id="Seg_1252" s="T75">löčü͡ög-ü-nen</ta>
            <ta e="T77" id="Seg_1253" s="T76">egel-er</ta>
            <ta e="T78" id="Seg_1254" s="T77">bu</ta>
            <ta e="T79" id="Seg_1255" s="T78">egel-en</ta>
            <ta e="T80" id="Seg_1256" s="T79">kɨːr-a-r-dag-ɨna</ta>
            <ta e="T81" id="Seg_1257" s="T80">emeːksin</ta>
            <ta e="T82" id="Seg_1258" s="T81">aːn-ɨ</ta>
            <ta e="T83" id="Seg_1259" s="T82">batt-a-t-ar</ta>
            <ta e="T84" id="Seg_1260" s="T83">onton</ta>
            <ta e="T85" id="Seg_1261" s="T84">ɨ͡aldʼ-aːččɨ</ta>
            <ta e="T86" id="Seg_1262" s="T85">bu͡ol-but</ta>
            <ta e="T87" id="Seg_1263" s="T86">oldoːn-no</ta>
            <ta e="T88" id="Seg_1264" s="T87">di͡e-bit</ta>
            <ta e="T89" id="Seg_1265" s="T88">emeːksin</ta>
            <ta e="T90" id="Seg_1266" s="T89">oldoːn-u</ta>
            <ta e="T91" id="Seg_1267" s="T90">bi͡er-bit</ta>
            <ta e="T92" id="Seg_1268" s="T91">dʼe</ta>
            <ta e="T93" id="Seg_1269" s="T92">onon</ta>
            <ta e="T94" id="Seg_1270" s="T93">tur-a</ta>
            <ta e="T95" id="Seg_1271" s="T94">ekkireː-n</ta>
            <ta e="T96" id="Seg_1272" s="T95">ogonnʼor</ta>
            <ta e="T97" id="Seg_1273" s="T96">dʼe</ta>
            <ta e="T98" id="Seg_1274" s="T97">uskaːn-nar-ɨ</ta>
            <ta e="T99" id="Seg_1275" s="T98">ölör-töː-büt</ta>
            <ta e="T100" id="Seg_1276" s="T99">eːt</ta>
            <ta e="T101" id="Seg_1277" s="T100">ojun-narɨ-n</ta>
            <ta e="T102" id="Seg_1278" s="T101">oks-oːru</ta>
            <ta e="T103" id="Seg_1279" s="T102">gɨm-mɨt-a</ta>
            <ta e="T104" id="Seg_1280" s="T103">ü͡öleh-i-nen</ta>
            <ta e="T105" id="Seg_1281" s="T104">oj-on</ta>
            <ta e="T106" id="Seg_1282" s="T105">ku͡op-put</ta>
            <ta e="T107" id="Seg_1283" s="T106">oldoːn-u-nan</ta>
            <ta e="T108" id="Seg_1284" s="T107">kulgaːg-ɨ-n</ta>
            <ta e="T109" id="Seg_1285" s="T108">töbö-tü-n</ta>
            <ta e="T110" id="Seg_1286" s="T109">ere</ta>
            <ta e="T111" id="Seg_1287" s="T110">oks-u-but</ta>
            <ta e="T112" id="Seg_1288" s="T111">ol</ta>
            <ta e="T113" id="Seg_1289" s="T112">koru͡o-ta</ta>
            <ta e="T114" id="Seg_1290" s="T113">bihill-i-bit-i-tten</ta>
            <ta e="T115" id="Seg_1291" s="T114">uskaːn</ta>
            <ta e="T116" id="Seg_1292" s="T115">kulgaːg-ɨ-n</ta>
            <ta e="T117" id="Seg_1293" s="T116">töbö-tö</ta>
            <ta e="T118" id="Seg_1294" s="T117">kara</ta>
            <ta e="T119" id="Seg_1295" s="T118">onton</ta>
            <ta e="T120" id="Seg_1296" s="T119">emi͡e</ta>
            <ta e="T121" id="Seg_1297" s="T120">ɨ͡aldʼ-ɨ-bɨt-a</ta>
            <ta e="T122" id="Seg_1298" s="T121">bu͡ol-an</ta>
            <ta e="T123" id="Seg_1299" s="T122">kɨrsa</ta>
            <ta e="T124" id="Seg_1300" s="T123">ojun-u-n</ta>
            <ta e="T125" id="Seg_1301" s="T124">ɨŋɨt-tar-bɨt</ta>
            <ta e="T126" id="Seg_1302" s="T125">elbek</ta>
            <ta e="T127" id="Seg_1303" s="T126">kɨrsa-nɨ</ta>
            <ta e="T128" id="Seg_1304" s="T127">ɨŋɨr-ɨ͡ag-ɨ-n</ta>
            <ta e="T129" id="Seg_1305" s="T128">kuttam-mɨt</ta>
            <ta e="T130" id="Seg_1306" s="T129">tiːs-teːk-ter</ta>
            <ta e="T131" id="Seg_1307" s="T130">di͡e-n</ta>
            <ta e="T132" id="Seg_1308" s="T131">ojun-narɨ-n</ta>
            <ta e="T133" id="Seg_1309" s="T132">ölör-dök-püne</ta>
            <ta e="T134" id="Seg_1310" s="T133">beje-lere</ta>
            <ta e="T135" id="Seg_1311" s="T134">da</ta>
            <ta e="T136" id="Seg_1312" s="T135">kömüsküː-r</ta>
            <ta e="T137" id="Seg_1313" s="T136">kihi-te</ta>
            <ta e="T138" id="Seg_1314" s="T137">hu͡ok</ta>
            <ta e="T139" id="Seg_1315" s="T138">öl-ü͡ök-tere</ta>
            <ta e="T140" id="Seg_1316" s="T139">di͡e-bit</ta>
            <ta e="T141" id="Seg_1317" s="T140">kɨrsa-tɨ-n</ta>
            <ta e="T142" id="Seg_1318" s="T141">ojun-u-n</ta>
            <ta e="T143" id="Seg_1319" s="T142">oldoːn-u-nan</ta>
            <ta e="T144" id="Seg_1320" s="T143">oks-on</ta>
            <ta e="T145" id="Seg_1321" s="T144">kuturug-u-n</ta>
            <ta e="T146" id="Seg_1322" s="T145">töbö-tü-n</ta>
            <ta e="T147" id="Seg_1323" s="T146">ere</ta>
            <ta e="T148" id="Seg_1324" s="T147">tap-pɨt</ta>
            <ta e="T149" id="Seg_1325" s="T148">ol</ta>
            <ta e="T150" id="Seg_1326" s="T149">ihin</ta>
            <ta e="T151" id="Seg_1327" s="T150">oldoːn</ta>
            <ta e="T152" id="Seg_1328" s="T151">koru͡o-tu-ttan</ta>
            <ta e="T153" id="Seg_1329" s="T152">kɨrsa</ta>
            <ta e="T154" id="Seg_1330" s="T153">kobo-loːk</ta>
            <ta e="T155" id="Seg_1331" s="T154">bu͡ol-but</ta>
            <ta e="T156" id="Seg_1332" s="T155">kuturug-u-n</ta>
            <ta e="T157" id="Seg_1333" s="T156">töbö-tü-ger</ta>
            <ta e="T158" id="Seg_1334" s="T157">bugurduk</ta>
            <ta e="T159" id="Seg_1335" s="T158">bu͡ol-but-u-n</ta>
            <ta e="T160" id="Seg_1336" s="T159">genne</ta>
            <ta e="T161" id="Seg_1337" s="T160">taŋara</ta>
            <ta e="T162" id="Seg_1338" s="T161">körs-ü-büt</ta>
            <ta e="T163" id="Seg_1339" s="T162">ol</ta>
            <ta e="T164" id="Seg_1340" s="T163">kihi-ni</ta>
            <ta e="T165" id="Seg_1341" s="T164">iti-ni</ta>
            <ta e="T166" id="Seg_1342" s="T165">oŋostu-but</ta>
            <ta e="T167" id="Seg_1343" s="T166">ajɨː-ga-r</ta>
            <ta e="T168" id="Seg_1344" s="T167">di͡e-bit</ta>
            <ta e="T169" id="Seg_1345" s="T168">en</ta>
            <ta e="T170" id="Seg_1346" s="T169">bɨhɨː-ŋ</ta>
            <ta e="T171" id="Seg_1347" s="T170">oloŋko</ta>
            <ta e="T172" id="Seg_1348" s="T171">kurduk</ta>
            <ta e="T173" id="Seg_1349" s="T172">ös</ta>
            <ta e="T174" id="Seg_1350" s="T173">bu͡ol-lun</ta>
            <ta e="T175" id="Seg_1351" s="T174">di͡e-bit</ta>
            <ta e="T176" id="Seg_1352" s="T175">beje-ŋ</ta>
            <ta e="T177" id="Seg_1353" s="T176">bu͡ollagɨna</ta>
            <ta e="T178" id="Seg_1354" s="T177">kukaːkɨ</ta>
            <ta e="T179" id="Seg_1355" s="T178">kötör</ta>
            <ta e="T180" id="Seg_1356" s="T179">bu͡ol-a</ta>
            <ta e="T181" id="Seg_1357" s="T180">hɨldʼ-an</ta>
            <ta e="T182" id="Seg_1358" s="T181">ahɨlɨktan-ar</ta>
            <ta e="T183" id="Seg_1359" s="T182">bu͡ol</ta>
            <ta e="T184" id="Seg_1360" s="T183">di͡e-bit</ta>
            <ta e="T185" id="Seg_1361" s="T184">ol</ta>
            <ta e="T186" id="Seg_1362" s="T185">ihin</ta>
            <ta e="T187" id="Seg_1363" s="T186">kukaːkɨ</ta>
            <ta e="T188" id="Seg_1364" s="T187">paːs</ta>
            <ta e="T189" id="Seg_1365" s="T188">meŋi͡e-ti-n</ta>
            <ta e="T190" id="Seg_1366" s="T189">h-iː</ta>
            <ta e="T191" id="Seg_1367" s="T190">hɨldʼ-ar</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_1368" s="T0">bɨlɨr</ta>
            <ta e="T2" id="Seg_1369" s="T1">biːr</ta>
            <ta e="T3" id="Seg_1370" s="T2">kihi</ta>
            <ta e="T4" id="Seg_1371" s="T3">olor-BIT</ta>
            <ta e="T5" id="Seg_1372" s="T4">bu</ta>
            <ta e="T6" id="Seg_1373" s="T5">kihi</ta>
            <ta e="T7" id="Seg_1374" s="T6">hanaː-BIT</ta>
            <ta e="T8" id="Seg_1375" s="T7">kɨrsa-nI</ta>
            <ta e="T9" id="Seg_1376" s="T8">barɨ-tI-n</ta>
            <ta e="T10" id="Seg_1377" s="T9">uskaːn-nI</ta>
            <ta e="T11" id="Seg_1378" s="T10">barɨ-tI-n</ta>
            <ta e="T12" id="Seg_1379" s="T11">ölör-An</ta>
            <ta e="T13" id="Seg_1380" s="T12">keːs-IAK-GA</ta>
            <ta e="T14" id="Seg_1381" s="T13">biːr-nI</ta>
            <ta e="T15" id="Seg_1382" s="T14">da</ta>
            <ta e="T16" id="Seg_1383" s="T15">ort-A-r-I-m</ta>
            <ta e="T17" id="Seg_1384" s="T16">di͡e-BIT</ta>
            <ta e="T18" id="Seg_1385" s="T17">u͡ol-tI-GAr</ta>
            <ta e="T19" id="Seg_1386" s="T18">et-BIT</ta>
            <ta e="T20" id="Seg_1387" s="T19">min</ta>
            <ta e="T21" id="Seg_1388" s="T20">ɨ͡arɨj-I-BIT</ta>
            <ta e="T22" id="Seg_1389" s="T21">bu͡ol-Iːm</ta>
            <ta e="T23" id="Seg_1390" s="T22">en</ta>
            <ta e="T24" id="Seg_1391" s="T23">bar-An-GIn</ta>
            <ta e="T25" id="Seg_1392" s="T24">uskaːn</ta>
            <ta e="T26" id="Seg_1393" s="T25">ojun-tI-n</ta>
            <ta e="T27" id="Seg_1394" s="T26">egel</ta>
            <ta e="T28" id="Seg_1395" s="T27">onton</ta>
            <ta e="T29" id="Seg_1396" s="T28">gini-GA</ta>
            <ta e="T30" id="Seg_1397" s="T29">löčü͡ök-tI-nAn</ta>
            <ta e="T31" id="Seg_1398" s="T30">barɨ</ta>
            <ta e="T32" id="Seg_1399" s="T31">uskaːn-nI</ta>
            <ta e="T33" id="Seg_1400" s="T32">üːr-An</ta>
            <ta e="T34" id="Seg_1401" s="T33">egel</ta>
            <ta e="T35" id="Seg_1402" s="T34">u͡ol-tA</ta>
            <ta e="T36" id="Seg_1403" s="T35">bar-BIT-tA</ta>
            <ta e="T37" id="Seg_1404" s="T36">biːr</ta>
            <ta e="T38" id="Seg_1405" s="T37">uskaːn</ta>
            <ta e="T39" id="Seg_1406" s="T38">olor-Ar</ta>
            <ta e="T40" id="Seg_1407" s="T39">dʼe</ta>
            <ta e="T41" id="Seg_1408" s="T40">kaja</ta>
            <ta e="T42" id="Seg_1409" s="T41">ojun-GIt</ta>
            <ta e="T43" id="Seg_1410" s="T42">kanna=Ij</ta>
            <ta e="T44" id="Seg_1411" s="T43">di͡e-Ar</ta>
            <ta e="T45" id="Seg_1412" s="T44">onno</ta>
            <ta e="T46" id="Seg_1413" s="T45">baːr</ta>
            <ta e="T47" id="Seg_1414" s="T46">di͡e-An</ta>
            <ta e="T48" id="Seg_1415" s="T47">ɨj-An</ta>
            <ta e="T49" id="Seg_1416" s="T48">bi͡er-Ar</ta>
            <ta e="T50" id="Seg_1417" s="T49">bar-An</ta>
            <ta e="T51" id="Seg_1418" s="T50">dʼe</ta>
            <ta e="T52" id="Seg_1419" s="T51">abɨraː</ta>
            <ta e="T53" id="Seg_1420" s="T52">di͡e-Ar</ta>
            <ta e="T54" id="Seg_1421" s="T53">aga-m</ta>
            <ta e="T55" id="Seg_1422" s="T54">öl-AːrI</ta>
            <ta e="T56" id="Seg_1423" s="T55">ɨŋɨr-TAr-TI-tA</ta>
            <ta e="T57" id="Seg_1424" s="T56">kömölös-An</ta>
            <ta e="T58" id="Seg_1425" s="T57">kör</ta>
            <ta e="T59" id="Seg_1426" s="T58">di͡e-Ar</ta>
            <ta e="T60" id="Seg_1427" s="T59">bu</ta>
            <ta e="T61" id="Seg_1428" s="T60">biri͡eme-GA</ta>
            <ta e="T62" id="Seg_1429" s="T61">et-Ar</ta>
            <ta e="T63" id="Seg_1430" s="T62">kaːl-AːččI</ta>
            <ta e="T64" id="Seg_1431" s="T63">kihi</ta>
            <ta e="T65" id="Seg_1432" s="T64">emeːksin-tI-GAr</ta>
            <ta e="T66" id="Seg_1433" s="T65">munnʼus-I-n-TAK-TArInA</ta>
            <ta e="T67" id="Seg_1434" s="T66">en</ta>
            <ta e="T68" id="Seg_1435" s="T67">aːn-GI-n</ta>
            <ta e="T69" id="Seg_1436" s="T68">battaː-A-t-An</ta>
            <ta e="T70" id="Seg_1437" s="T69">keːs-Aːr</ta>
            <ta e="T71" id="Seg_1438" s="T70">di͡e-Ar</ta>
            <ta e="T72" id="Seg_1439" s="T71">ojun-LArA</ta>
            <ta e="T73" id="Seg_1440" s="T72">kel-An</ta>
            <ta e="T74" id="Seg_1441" s="T73">uskaːn</ta>
            <ta e="T75" id="Seg_1442" s="T74">bögö-nI</ta>
            <ta e="T76" id="Seg_1443" s="T75">löčü͡ök-tI-nAn</ta>
            <ta e="T77" id="Seg_1444" s="T76">egel-Ar</ta>
            <ta e="T78" id="Seg_1445" s="T77">bu</ta>
            <ta e="T79" id="Seg_1446" s="T78">egel-An</ta>
            <ta e="T80" id="Seg_1447" s="T79">kɨːr-A-r-TAK-InA</ta>
            <ta e="T81" id="Seg_1448" s="T80">emeːksin</ta>
            <ta e="T82" id="Seg_1449" s="T81">aːn-nI</ta>
            <ta e="T83" id="Seg_1450" s="T82">battaː-A-t-Ar</ta>
            <ta e="T84" id="Seg_1451" s="T83">onton</ta>
            <ta e="T85" id="Seg_1452" s="T84">ɨ͡arɨj-AːččI</ta>
            <ta e="T86" id="Seg_1453" s="T85">bu͡ol-BIT</ta>
            <ta e="T87" id="Seg_1454" s="T86">oldoːn-TA</ta>
            <ta e="T88" id="Seg_1455" s="T87">di͡e-BIT</ta>
            <ta e="T89" id="Seg_1456" s="T88">emeːksin</ta>
            <ta e="T90" id="Seg_1457" s="T89">oldoːn-nI</ta>
            <ta e="T91" id="Seg_1458" s="T90">bi͡er-BIT</ta>
            <ta e="T92" id="Seg_1459" s="T91">dʼe</ta>
            <ta e="T93" id="Seg_1460" s="T92">onon</ta>
            <ta e="T94" id="Seg_1461" s="T93">tur-A</ta>
            <ta e="T95" id="Seg_1462" s="T94">ekkireː-An</ta>
            <ta e="T96" id="Seg_1463" s="T95">ogonnʼor</ta>
            <ta e="T97" id="Seg_1464" s="T96">dʼe</ta>
            <ta e="T98" id="Seg_1465" s="T97">uskaːn-LAr-nI</ta>
            <ta e="T99" id="Seg_1466" s="T98">ölör-TAː-BIT</ta>
            <ta e="T100" id="Seg_1467" s="T99">eːt</ta>
            <ta e="T101" id="Seg_1468" s="T100">ojun-LArI-n</ta>
            <ta e="T102" id="Seg_1469" s="T101">ogus-AːrI</ta>
            <ta e="T103" id="Seg_1470" s="T102">gɨn-BIT-tA</ta>
            <ta e="T104" id="Seg_1471" s="T103">ü͡öles-tI-nAn</ta>
            <ta e="T105" id="Seg_1472" s="T104">oj-An</ta>
            <ta e="T106" id="Seg_1473" s="T105">ku͡ot-BIT</ta>
            <ta e="T107" id="Seg_1474" s="T106">oldoːn-tI-nAn</ta>
            <ta e="T108" id="Seg_1475" s="T107">kulgaːk-tI-n</ta>
            <ta e="T109" id="Seg_1476" s="T108">töbö-tI-n</ta>
            <ta e="T110" id="Seg_1477" s="T109">ere</ta>
            <ta e="T111" id="Seg_1478" s="T110">ogus-I-BIT</ta>
            <ta e="T112" id="Seg_1479" s="T111">ol</ta>
            <ta e="T113" id="Seg_1480" s="T112">koru͡o-tA</ta>
            <ta e="T114" id="Seg_1481" s="T113">bihilin-I-BIT-I-ttAn</ta>
            <ta e="T115" id="Seg_1482" s="T114">uskaːn</ta>
            <ta e="T116" id="Seg_1483" s="T115">kulgaːk-tI-n</ta>
            <ta e="T117" id="Seg_1484" s="T116">töbö-tA</ta>
            <ta e="T118" id="Seg_1485" s="T117">kara</ta>
            <ta e="T119" id="Seg_1486" s="T118">onton</ta>
            <ta e="T120" id="Seg_1487" s="T119">emi͡e</ta>
            <ta e="T121" id="Seg_1488" s="T120">ɨ͡arɨj-I-BIT-tA</ta>
            <ta e="T122" id="Seg_1489" s="T121">bu͡ol-An</ta>
            <ta e="T123" id="Seg_1490" s="T122">kɨrsa</ta>
            <ta e="T124" id="Seg_1491" s="T123">ojun-tI-n</ta>
            <ta e="T125" id="Seg_1492" s="T124">ɨŋɨr-TAr-BIT</ta>
            <ta e="T126" id="Seg_1493" s="T125">elbek</ta>
            <ta e="T127" id="Seg_1494" s="T126">kɨrsa-nI</ta>
            <ta e="T128" id="Seg_1495" s="T127">ɨŋɨr-IAK-tI-n</ta>
            <ta e="T129" id="Seg_1496" s="T128">kuttan-BIT</ta>
            <ta e="T130" id="Seg_1497" s="T129">tiːs-LAːK-LAr</ta>
            <ta e="T131" id="Seg_1498" s="T130">di͡e-An</ta>
            <ta e="T132" id="Seg_1499" s="T131">ojun-LArI-n</ta>
            <ta e="T133" id="Seg_1500" s="T132">ölör-TAK-BInA</ta>
            <ta e="T134" id="Seg_1501" s="T133">beje-LArA</ta>
            <ta e="T135" id="Seg_1502" s="T134">da</ta>
            <ta e="T136" id="Seg_1503" s="T135">kömüskeː-Ar</ta>
            <ta e="T137" id="Seg_1504" s="T136">kihi-tA</ta>
            <ta e="T138" id="Seg_1505" s="T137">hu͡ok</ta>
            <ta e="T139" id="Seg_1506" s="T138">öl-IAK-LArA</ta>
            <ta e="T140" id="Seg_1507" s="T139">di͡e-BIT</ta>
            <ta e="T141" id="Seg_1508" s="T140">kɨrsa-tI-n</ta>
            <ta e="T142" id="Seg_1509" s="T141">ojun-tI-n</ta>
            <ta e="T143" id="Seg_1510" s="T142">oldoːn-tI-nAn</ta>
            <ta e="T144" id="Seg_1511" s="T143">ogus-An</ta>
            <ta e="T145" id="Seg_1512" s="T144">kuturuk-tI-n</ta>
            <ta e="T146" id="Seg_1513" s="T145">töbö-tI-n</ta>
            <ta e="T147" id="Seg_1514" s="T146">ere</ta>
            <ta e="T148" id="Seg_1515" s="T147">tap-BIT</ta>
            <ta e="T149" id="Seg_1516" s="T148">ol</ta>
            <ta e="T150" id="Seg_1517" s="T149">ihin</ta>
            <ta e="T151" id="Seg_1518" s="T150">oldoːn</ta>
            <ta e="T152" id="Seg_1519" s="T151">koru͡o-tI-ttAn</ta>
            <ta e="T153" id="Seg_1520" s="T152">kɨrsa</ta>
            <ta e="T154" id="Seg_1521" s="T153">kobo-LAːK</ta>
            <ta e="T155" id="Seg_1522" s="T154">bu͡ol-BIT</ta>
            <ta e="T156" id="Seg_1523" s="T155">kuturuk-tI-n</ta>
            <ta e="T157" id="Seg_1524" s="T156">töbö-tI-GAr</ta>
            <ta e="T158" id="Seg_1525" s="T157">bugurduk</ta>
            <ta e="T159" id="Seg_1526" s="T158">bu͡ol-BIT-tI-n</ta>
            <ta e="T160" id="Seg_1527" s="T159">genne</ta>
            <ta e="T161" id="Seg_1528" s="T160">taŋara</ta>
            <ta e="T162" id="Seg_1529" s="T161">körüs-I-BIT</ta>
            <ta e="T163" id="Seg_1530" s="T162">ol</ta>
            <ta e="T164" id="Seg_1531" s="T163">kihi-nI</ta>
            <ta e="T165" id="Seg_1532" s="T164">iti-nI</ta>
            <ta e="T166" id="Seg_1533" s="T165">oŋohun-BIT</ta>
            <ta e="T167" id="Seg_1534" s="T166">anʼɨː-GA-r</ta>
            <ta e="T168" id="Seg_1535" s="T167">di͡e-BIT</ta>
            <ta e="T169" id="Seg_1536" s="T168">en</ta>
            <ta e="T170" id="Seg_1537" s="T169">bɨhɨː-ŋ</ta>
            <ta e="T171" id="Seg_1538" s="T170">oloŋko</ta>
            <ta e="T172" id="Seg_1539" s="T171">kurduk</ta>
            <ta e="T173" id="Seg_1540" s="T172">ös</ta>
            <ta e="T174" id="Seg_1541" s="T173">bu͡ol-TIn</ta>
            <ta e="T175" id="Seg_1542" s="T174">di͡e-BIT</ta>
            <ta e="T176" id="Seg_1543" s="T175">beje-ŋ</ta>
            <ta e="T177" id="Seg_1544" s="T176">bu͡ollagɨna</ta>
            <ta e="T178" id="Seg_1545" s="T177">kukaːkɨ</ta>
            <ta e="T179" id="Seg_1546" s="T178">kötör</ta>
            <ta e="T180" id="Seg_1547" s="T179">bu͡ol-A</ta>
            <ta e="T181" id="Seg_1548" s="T180">hɨrɨt-An</ta>
            <ta e="T182" id="Seg_1549" s="T181">ahɨlɨktan-Ar</ta>
            <ta e="T183" id="Seg_1550" s="T182">bu͡ol</ta>
            <ta e="T184" id="Seg_1551" s="T183">di͡e-BIT</ta>
            <ta e="T185" id="Seg_1552" s="T184">ol</ta>
            <ta e="T186" id="Seg_1553" s="T185">ihin</ta>
            <ta e="T187" id="Seg_1554" s="T186">kukaːkɨ</ta>
            <ta e="T188" id="Seg_1555" s="T187">paːs</ta>
            <ta e="T189" id="Seg_1556" s="T188">meŋi͡e-tI-n</ta>
            <ta e="T190" id="Seg_1557" s="T189">hi͡e-A</ta>
            <ta e="T191" id="Seg_1558" s="T190">hɨrɨt-Ar</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_1559" s="T0">long.ago</ta>
            <ta e="T2" id="Seg_1560" s="T1">one</ta>
            <ta e="T3" id="Seg_1561" s="T2">human.being.[NOM]</ta>
            <ta e="T4" id="Seg_1562" s="T3">live-PST2.[3SG]</ta>
            <ta e="T5" id="Seg_1563" s="T4">this</ta>
            <ta e="T6" id="Seg_1564" s="T5">human.being.[NOM]</ta>
            <ta e="T7" id="Seg_1565" s="T6">think-PST2.[3SG]</ta>
            <ta e="T8" id="Seg_1566" s="T7">polar.fox-ACC</ta>
            <ta e="T9" id="Seg_1567" s="T8">every-3SG-ACC</ta>
            <ta e="T10" id="Seg_1568" s="T9">hare-ACC</ta>
            <ta e="T11" id="Seg_1569" s="T10">every-3SG-ACC</ta>
            <ta e="T12" id="Seg_1570" s="T11">kill-CVB.SEQ</ta>
            <ta e="T13" id="Seg_1571" s="T12">throw-PTCP.FUT-DAT/LOC</ta>
            <ta e="T14" id="Seg_1572" s="T13">one-ACC</ta>
            <ta e="T15" id="Seg_1573" s="T14">NEG</ta>
            <ta e="T16" id="Seg_1574" s="T15">remain-EP-CAUS-EP-NEG.[IMP.2SG]</ta>
            <ta e="T17" id="Seg_1575" s="T16">say-PST2.[3SG]</ta>
            <ta e="T18" id="Seg_1576" s="T17">son-3SG-DAT/LOC</ta>
            <ta e="T19" id="Seg_1577" s="T18">speak-PST2.[3SG]</ta>
            <ta e="T20" id="Seg_1578" s="T19">1SG.[NOM]</ta>
            <ta e="T21" id="Seg_1579" s="T20">be.sick-EP-PTCP.PST</ta>
            <ta e="T22" id="Seg_1580" s="T21">be-IMP.1SG</ta>
            <ta e="T23" id="Seg_1581" s="T22">2SG.[NOM]</ta>
            <ta e="T24" id="Seg_1582" s="T23">go-CVB.SEQ-2SG</ta>
            <ta e="T25" id="Seg_1583" s="T24">hare.[NOM]</ta>
            <ta e="T26" id="Seg_1584" s="T25">shaman-3SG-ACC</ta>
            <ta e="T27" id="Seg_1585" s="T26">get.[IMP.2SG]</ta>
            <ta e="T28" id="Seg_1586" s="T27">then</ta>
            <ta e="T29" id="Seg_1587" s="T28">3SG-DAT/LOC</ta>
            <ta e="T30" id="Seg_1588" s="T29">deacon-3SG-INSTR</ta>
            <ta e="T31" id="Seg_1589" s="T30">every</ta>
            <ta e="T32" id="Seg_1590" s="T31">hare-ACC</ta>
            <ta e="T33" id="Seg_1591" s="T32">hunt-CVB.SEQ</ta>
            <ta e="T34" id="Seg_1592" s="T33">bring.[IMP.2SG]</ta>
            <ta e="T35" id="Seg_1593" s="T34">son-3SG.[NOM]</ta>
            <ta e="T36" id="Seg_1594" s="T35">go-PST2-3SG</ta>
            <ta e="T37" id="Seg_1595" s="T36">one</ta>
            <ta e="T38" id="Seg_1596" s="T37">hare.[NOM]</ta>
            <ta e="T39" id="Seg_1597" s="T38">sit-PRS.[3SG]</ta>
            <ta e="T40" id="Seg_1598" s="T39">well</ta>
            <ta e="T41" id="Seg_1599" s="T40">well</ta>
            <ta e="T42" id="Seg_1600" s="T41">shaman-2PL.[NOM]</ta>
            <ta e="T43" id="Seg_1601" s="T42">where=Q</ta>
            <ta e="T44" id="Seg_1602" s="T43">say-PRS.[3SG]</ta>
            <ta e="T45" id="Seg_1603" s="T44">there</ta>
            <ta e="T46" id="Seg_1604" s="T45">there.is</ta>
            <ta e="T47" id="Seg_1605" s="T46">say-CVB.SEQ</ta>
            <ta e="T48" id="Seg_1606" s="T47">show-CVB.SEQ</ta>
            <ta e="T49" id="Seg_1607" s="T48">give-PRS.[3SG]</ta>
            <ta e="T50" id="Seg_1608" s="T49">go-CVB.SEQ</ta>
            <ta e="T51" id="Seg_1609" s="T50">well</ta>
            <ta e="T52" id="Seg_1610" s="T51">come.to.aid.[IMP.2SG]</ta>
            <ta e="T53" id="Seg_1611" s="T52">say-PRS.[3SG]</ta>
            <ta e="T54" id="Seg_1612" s="T53">father-1SG.[NOM]</ta>
            <ta e="T55" id="Seg_1613" s="T54">die-CVB.PURP</ta>
            <ta e="T56" id="Seg_1614" s="T55">call-PASS-PST1-3SG</ta>
            <ta e="T57" id="Seg_1615" s="T56">help-CVB.SEQ</ta>
            <ta e="T58" id="Seg_1616" s="T57">try.[IMP.2SG]</ta>
            <ta e="T59" id="Seg_1617" s="T58">say-PRS.[3SG]</ta>
            <ta e="T60" id="Seg_1618" s="T59">this</ta>
            <ta e="T61" id="Seg_1619" s="T60">time-DAT/LOC</ta>
            <ta e="T62" id="Seg_1620" s="T61">speak-PRS.[3SG]</ta>
            <ta e="T63" id="Seg_1621" s="T62">stay-PTCP.HAB</ta>
            <ta e="T64" id="Seg_1622" s="T63">human.being.[NOM]</ta>
            <ta e="T65" id="Seg_1623" s="T64">old.woman-3SG-DAT/LOC</ta>
            <ta e="T66" id="Seg_1624" s="T65">gather.oneself-EP-REFL-TEMP-3PL</ta>
            <ta e="T67" id="Seg_1625" s="T66">2SG.[NOM]</ta>
            <ta e="T68" id="Seg_1626" s="T67">door-2SG-ACC</ta>
            <ta e="T69" id="Seg_1627" s="T68">pull.down-EP-CAUS-CVB.SEQ</ta>
            <ta e="T70" id="Seg_1628" s="T69">throw-FUT.[IMP.2SG]</ta>
            <ta e="T71" id="Seg_1629" s="T70">say-PRS.[3SG]</ta>
            <ta e="T72" id="Seg_1630" s="T71">shaman-3PL.[NOM]</ta>
            <ta e="T73" id="Seg_1631" s="T72">come-CVB.SEQ</ta>
            <ta e="T74" id="Seg_1632" s="T73">hare.[NOM]</ta>
            <ta e="T75" id="Seg_1633" s="T74">a.lot-ACC</ta>
            <ta e="T76" id="Seg_1634" s="T75">deacon-3SG-INSTR</ta>
            <ta e="T77" id="Seg_1635" s="T76">bring-PRS.[3SG]</ta>
            <ta e="T78" id="Seg_1636" s="T77">this</ta>
            <ta e="T79" id="Seg_1637" s="T78">get-CVB.SEQ</ta>
            <ta e="T80" id="Seg_1638" s="T79">shamanize-EP-CAUS-TEMP-3SG</ta>
            <ta e="T81" id="Seg_1639" s="T80">old.woman.[NOM]</ta>
            <ta e="T82" id="Seg_1640" s="T81">door-ACC</ta>
            <ta e="T83" id="Seg_1641" s="T82">pull.down-EP-CAUS-PRS.[3SG]</ta>
            <ta e="T84" id="Seg_1642" s="T83">then</ta>
            <ta e="T85" id="Seg_1643" s="T84">be.sick-PTCP.HAB</ta>
            <ta e="T86" id="Seg_1644" s="T85">become-PTCP.PST.[NOM]</ta>
            <ta e="T87" id="Seg_1645" s="T86">hook.for.the.kettle-PART</ta>
            <ta e="T88" id="Seg_1646" s="T87">say-PST2.[3SG]</ta>
            <ta e="T89" id="Seg_1647" s="T88">old.woman.[NOM]</ta>
            <ta e="T90" id="Seg_1648" s="T89">hook.for.the.kettle-ACC</ta>
            <ta e="T91" id="Seg_1649" s="T90">give-PST2.[3SG]</ta>
            <ta e="T92" id="Seg_1650" s="T91">well</ta>
            <ta e="T93" id="Seg_1651" s="T92">then</ta>
            <ta e="T94" id="Seg_1652" s="T93">stand.up-CVB.SIM</ta>
            <ta e="T95" id="Seg_1653" s="T94">jump-CVB.SEQ</ta>
            <ta e="T96" id="Seg_1654" s="T95">old.man.[NOM]</ta>
            <ta e="T97" id="Seg_1655" s="T96">well</ta>
            <ta e="T98" id="Seg_1656" s="T97">hare-PL-ACC</ta>
            <ta e="T99" id="Seg_1657" s="T98">kill-ITER-PST2.[3SG]</ta>
            <ta e="T100" id="Seg_1658" s="T99">EVID</ta>
            <ta e="T101" id="Seg_1659" s="T100">shaman-3PL-ACC</ta>
            <ta e="T102" id="Seg_1660" s="T101">beat-CVB.PURP</ta>
            <ta e="T103" id="Seg_1661" s="T102">want-PST2-3SG</ta>
            <ta e="T104" id="Seg_1662" s="T103">chimney-3SG-INSTR</ta>
            <ta e="T105" id="Seg_1663" s="T104">jump-CVB.SEQ</ta>
            <ta e="T106" id="Seg_1664" s="T105">go.away-PST2.[3SG]</ta>
            <ta e="T107" id="Seg_1665" s="T106">hook.for.the.kettle-3SG-INSTR</ta>
            <ta e="T108" id="Seg_1666" s="T107">ear-3SG-GEN</ta>
            <ta e="T109" id="Seg_1667" s="T108">top-3SG-ACC</ta>
            <ta e="T110" id="Seg_1668" s="T109">just</ta>
            <ta e="T111" id="Seg_1669" s="T110">beat-EP-PST2.[3SG]</ta>
            <ta e="T112" id="Seg_1670" s="T111">that</ta>
            <ta e="T113" id="Seg_1671" s="T112">soot-3SG.[NOM]</ta>
            <ta e="T114" id="Seg_1672" s="T113">get.dirty-EP-PTCP.PST-EP-ABL</ta>
            <ta e="T115" id="Seg_1673" s="T114">hare.[NOM]</ta>
            <ta e="T116" id="Seg_1674" s="T115">ear-3SG-GEN</ta>
            <ta e="T117" id="Seg_1675" s="T116">top-3SG.[NOM]</ta>
            <ta e="T118" id="Seg_1676" s="T117">black.[NOM]</ta>
            <ta e="T119" id="Seg_1677" s="T118">then</ta>
            <ta e="T120" id="Seg_1678" s="T119">again</ta>
            <ta e="T121" id="Seg_1679" s="T120">be.sick-EP-PST2-3SG</ta>
            <ta e="T122" id="Seg_1680" s="T121">be-CVB.SEQ</ta>
            <ta e="T123" id="Seg_1681" s="T122">polar.fox.[NOM]</ta>
            <ta e="T124" id="Seg_1682" s="T123">shaman-3SG-ACC</ta>
            <ta e="T125" id="Seg_1683" s="T124">call-CAUS-PST2.[3SG]</ta>
            <ta e="T126" id="Seg_1684" s="T125">many</ta>
            <ta e="T127" id="Seg_1685" s="T126">polar.fox-ACC</ta>
            <ta e="T128" id="Seg_1686" s="T127">call-PTCP.FUT-3SG-ACC</ta>
            <ta e="T129" id="Seg_1687" s="T128">be.afraid-PST2.[3SG]</ta>
            <ta e="T130" id="Seg_1688" s="T129">tooth-PROPR-3PL</ta>
            <ta e="T131" id="Seg_1689" s="T130">say-CVB.SEQ</ta>
            <ta e="T132" id="Seg_1690" s="T131">shaman-3PL-ACC</ta>
            <ta e="T133" id="Seg_1691" s="T132">kill-TEMP-1SG</ta>
            <ta e="T134" id="Seg_1692" s="T133">self-3PL.[NOM]</ta>
            <ta e="T135" id="Seg_1693" s="T134">and</ta>
            <ta e="T136" id="Seg_1694" s="T135">protect-PTCP.PRS</ta>
            <ta e="T137" id="Seg_1695" s="T136">human.being-POSS</ta>
            <ta e="T138" id="Seg_1696" s="T137">NEG</ta>
            <ta e="T139" id="Seg_1697" s="T138">die-FUT-3PL</ta>
            <ta e="T140" id="Seg_1698" s="T139">say-PST2.[3SG]</ta>
            <ta e="T141" id="Seg_1699" s="T140">polar.fox-3SG-GEN</ta>
            <ta e="T142" id="Seg_1700" s="T141">shaman-3SG-ACC</ta>
            <ta e="T143" id="Seg_1701" s="T142">hook.for.the.kettle-3SG-INSTR</ta>
            <ta e="T144" id="Seg_1702" s="T143">beat-CVB.SEQ</ta>
            <ta e="T145" id="Seg_1703" s="T144">tail-3SG-GEN</ta>
            <ta e="T146" id="Seg_1704" s="T145">top-3SG-ACC</ta>
            <ta e="T147" id="Seg_1705" s="T146">just</ta>
            <ta e="T148" id="Seg_1706" s="T147">strike-PST2.[3SG]</ta>
            <ta e="T149" id="Seg_1707" s="T148">that.[NOM]</ta>
            <ta e="T150" id="Seg_1708" s="T149">for</ta>
            <ta e="T151" id="Seg_1709" s="T150">hook.for.the.kettle.[NOM]</ta>
            <ta e="T152" id="Seg_1710" s="T151">soot-3SG-ABL</ta>
            <ta e="T153" id="Seg_1711" s="T152">polar.fox.[NOM]</ta>
            <ta e="T154" id="Seg_1712" s="T153">tuft-PROPR.[NOM]</ta>
            <ta e="T155" id="Seg_1713" s="T154">become-PST2.[3SG]</ta>
            <ta e="T156" id="Seg_1714" s="T155">tail-3SG-GEN</ta>
            <ta e="T157" id="Seg_1715" s="T156">top-3SG-DAT/LOC</ta>
            <ta e="T158" id="Seg_1716" s="T157">like.this</ta>
            <ta e="T159" id="Seg_1717" s="T158">be-PTCP.PST-3SG-ACC</ta>
            <ta e="T160" id="Seg_1718" s="T159">after</ta>
            <ta e="T161" id="Seg_1719" s="T160">god.[NOM]</ta>
            <ta e="T162" id="Seg_1720" s="T161">meet-EP-PST2.[3SG]</ta>
            <ta e="T163" id="Seg_1721" s="T162">that</ta>
            <ta e="T164" id="Seg_1722" s="T163">human.being-ACC</ta>
            <ta e="T165" id="Seg_1723" s="T164">that-ACC</ta>
            <ta e="T166" id="Seg_1724" s="T165">commit-PTCP.PST</ta>
            <ta e="T167" id="Seg_1725" s="T166">sin-2SG-DAT/LOC</ta>
            <ta e="T168" id="Seg_1726" s="T167">say-PST2.[3SG]</ta>
            <ta e="T169" id="Seg_1727" s="T168">2SG.[NOM]</ta>
            <ta e="T170" id="Seg_1728" s="T169">offence-2SG.[NOM]</ta>
            <ta e="T171" id="Seg_1729" s="T170">tale.[NOM]</ta>
            <ta e="T172" id="Seg_1730" s="T171">like</ta>
            <ta e="T173" id="Seg_1731" s="T172">parable.[NOM]</ta>
            <ta e="T174" id="Seg_1732" s="T173">be-IMP.3SG</ta>
            <ta e="T175" id="Seg_1733" s="T174">say-PST2.[3SG]</ta>
            <ta e="T176" id="Seg_1734" s="T175">self-2SG.[NOM]</ta>
            <ta e="T177" id="Seg_1735" s="T176">though</ta>
            <ta e="T178" id="Seg_1736" s="T177">jay.[NOM]</ta>
            <ta e="T179" id="Seg_1737" s="T178">bird.[NOM]</ta>
            <ta e="T180" id="Seg_1738" s="T179">be-CVB.SIM</ta>
            <ta e="T181" id="Seg_1739" s="T180">rot-CVB.SEQ</ta>
            <ta e="T182" id="Seg_1740" s="T181">feed.oneself-PTCP.PRS</ta>
            <ta e="T183" id="Seg_1741" s="T182">be.[IMP.2SG]</ta>
            <ta e="T184" id="Seg_1742" s="T183">say-PST2.[3SG]</ta>
            <ta e="T185" id="Seg_1743" s="T184">that.[NOM]</ta>
            <ta e="T186" id="Seg_1744" s="T185">for</ta>
            <ta e="T187" id="Seg_1745" s="T186">jay.[NOM]</ta>
            <ta e="T188" id="Seg_1746" s="T187">deadfall.[NOM]</ta>
            <ta e="T189" id="Seg_1747" s="T188">bait-3SG-ACC</ta>
            <ta e="T190" id="Seg_1748" s="T189">eat-CVB.SIM</ta>
            <ta e="T191" id="Seg_1749" s="T190">go-PRS.[3SG]</ta>
         </annotation>
         <annotation name="gg" tierref="gd">
            <ta e="T1" id="Seg_1750" s="T0">vor.langer.Zeit</ta>
            <ta e="T2" id="Seg_1751" s="T1">eins</ta>
            <ta e="T3" id="Seg_1752" s="T2">Mensch.[NOM]</ta>
            <ta e="T4" id="Seg_1753" s="T3">leben-PST2.[3SG]</ta>
            <ta e="T5" id="Seg_1754" s="T4">dieses</ta>
            <ta e="T6" id="Seg_1755" s="T5">Mensch.[NOM]</ta>
            <ta e="T7" id="Seg_1756" s="T6">denken-PST2.[3SG]</ta>
            <ta e="T8" id="Seg_1757" s="T7">Polarfuchs-ACC</ta>
            <ta e="T9" id="Seg_1758" s="T8">ganz-3SG-ACC</ta>
            <ta e="T10" id="Seg_1759" s="T9">Hase-ACC</ta>
            <ta e="T11" id="Seg_1760" s="T10">ganz-3SG-ACC</ta>
            <ta e="T12" id="Seg_1761" s="T11">töten-CVB.SEQ</ta>
            <ta e="T13" id="Seg_1762" s="T12">werfen-PTCP.FUT-DAT/LOC</ta>
            <ta e="T14" id="Seg_1763" s="T13">eins-ACC</ta>
            <ta e="T15" id="Seg_1764" s="T14">NEG</ta>
            <ta e="T16" id="Seg_1765" s="T15">übrig.bleiben-EP-CAUS-EP-NEG.[IMP.2SG]</ta>
            <ta e="T17" id="Seg_1766" s="T16">sagen-PST2.[3SG]</ta>
            <ta e="T18" id="Seg_1767" s="T17">Sohn-3SG-DAT/LOC</ta>
            <ta e="T19" id="Seg_1768" s="T18">sprechen-PST2.[3SG]</ta>
            <ta e="T20" id="Seg_1769" s="T19">1SG.[NOM]</ta>
            <ta e="T21" id="Seg_1770" s="T20">krank.sein-EP-PTCP.PST</ta>
            <ta e="T22" id="Seg_1771" s="T21">sein-IMP.1SG</ta>
            <ta e="T23" id="Seg_1772" s="T22">2SG.[NOM]</ta>
            <ta e="T24" id="Seg_1773" s="T23">gehen-CVB.SEQ-2SG</ta>
            <ta e="T25" id="Seg_1774" s="T24">Hase.[NOM]</ta>
            <ta e="T26" id="Seg_1775" s="T25">Schamane-3SG-ACC</ta>
            <ta e="T27" id="Seg_1776" s="T26">holen.[IMP.2SG]</ta>
            <ta e="T28" id="Seg_1777" s="T27">dann</ta>
            <ta e="T29" id="Seg_1778" s="T28">3SG-DAT/LOC</ta>
            <ta e="T30" id="Seg_1779" s="T29">Diakon-3SG-INSTR</ta>
            <ta e="T31" id="Seg_1780" s="T30">jeder</ta>
            <ta e="T32" id="Seg_1781" s="T31">Hase-ACC</ta>
            <ta e="T33" id="Seg_1782" s="T32">jagen-CVB.SEQ</ta>
            <ta e="T34" id="Seg_1783" s="T33">bringen.[IMP.2SG]</ta>
            <ta e="T35" id="Seg_1784" s="T34">Sohn-3SG.[NOM]</ta>
            <ta e="T36" id="Seg_1785" s="T35">gehen-PST2-3SG</ta>
            <ta e="T37" id="Seg_1786" s="T36">eins</ta>
            <ta e="T38" id="Seg_1787" s="T37">Hase.[NOM]</ta>
            <ta e="T39" id="Seg_1788" s="T38">sitzen-PRS.[3SG]</ta>
            <ta e="T40" id="Seg_1789" s="T39">doch</ta>
            <ta e="T41" id="Seg_1790" s="T40">na</ta>
            <ta e="T42" id="Seg_1791" s="T41">Schamane-2PL.[NOM]</ta>
            <ta e="T43" id="Seg_1792" s="T42">wo=Q</ta>
            <ta e="T44" id="Seg_1793" s="T43">sagen-PRS.[3SG]</ta>
            <ta e="T45" id="Seg_1794" s="T44">dort</ta>
            <ta e="T46" id="Seg_1795" s="T45">es.gibt</ta>
            <ta e="T47" id="Seg_1796" s="T46">sagen-CVB.SEQ</ta>
            <ta e="T48" id="Seg_1797" s="T47">zeigen-CVB.SEQ</ta>
            <ta e="T49" id="Seg_1798" s="T48">geben-PRS.[3SG]</ta>
            <ta e="T50" id="Seg_1799" s="T49">gehen-CVB.SEQ</ta>
            <ta e="T51" id="Seg_1800" s="T50">doch</ta>
            <ta e="T52" id="Seg_1801" s="T51">zu.Hilfe.kommen.[IMP.2SG]</ta>
            <ta e="T53" id="Seg_1802" s="T52">sagen-PRS.[3SG]</ta>
            <ta e="T54" id="Seg_1803" s="T53">Vater-1SG.[NOM]</ta>
            <ta e="T55" id="Seg_1804" s="T54">sterben-CVB.PURP</ta>
            <ta e="T56" id="Seg_1805" s="T55">rufen-PASS-PST1-3SG</ta>
            <ta e="T57" id="Seg_1806" s="T56">helfen-CVB.SEQ</ta>
            <ta e="T58" id="Seg_1807" s="T57">versuchen.[IMP.2SG]</ta>
            <ta e="T59" id="Seg_1808" s="T58">sagen-PRS.[3SG]</ta>
            <ta e="T60" id="Seg_1809" s="T59">dieses</ta>
            <ta e="T61" id="Seg_1810" s="T60">Zeit-DAT/LOC</ta>
            <ta e="T62" id="Seg_1811" s="T61">sprechen-PRS.[3SG]</ta>
            <ta e="T63" id="Seg_1812" s="T62">bleiben-PTCP.HAB</ta>
            <ta e="T64" id="Seg_1813" s="T63">Mensch.[NOM]</ta>
            <ta e="T65" id="Seg_1814" s="T64">Alte-3SG-DAT/LOC</ta>
            <ta e="T66" id="Seg_1815" s="T65">sich.sammeln-EP-REFL-TEMP-3PL</ta>
            <ta e="T67" id="Seg_1816" s="T66">2SG.[NOM]</ta>
            <ta e="T68" id="Seg_1817" s="T67">Tür-2SG-ACC</ta>
            <ta e="T69" id="Seg_1818" s="T68">herunterziehen-EP-CAUS-CVB.SEQ</ta>
            <ta e="T70" id="Seg_1819" s="T69">werfen-FUT.[IMP.2SG]</ta>
            <ta e="T71" id="Seg_1820" s="T70">sagen-PRS.[3SG]</ta>
            <ta e="T72" id="Seg_1821" s="T71">Schamane-3PL.[NOM]</ta>
            <ta e="T73" id="Seg_1822" s="T72">kommen-CVB.SEQ</ta>
            <ta e="T74" id="Seg_1823" s="T73">Hase.[NOM]</ta>
            <ta e="T75" id="Seg_1824" s="T74">sehr.viel-ACC</ta>
            <ta e="T76" id="Seg_1825" s="T75">Diakon-3SG-INSTR</ta>
            <ta e="T77" id="Seg_1826" s="T76">bringen-PRS.[3SG]</ta>
            <ta e="T78" id="Seg_1827" s="T77">dieses</ta>
            <ta e="T79" id="Seg_1828" s="T78">holen-CVB.SEQ</ta>
            <ta e="T80" id="Seg_1829" s="T79">schamanisieren-EP-CAUS-TEMP-3SG</ta>
            <ta e="T81" id="Seg_1830" s="T80">Alte.[NOM]</ta>
            <ta e="T82" id="Seg_1831" s="T81">Tür-ACC</ta>
            <ta e="T83" id="Seg_1832" s="T82">herunterziehen-EP-CAUS-PRS.[3SG]</ta>
            <ta e="T84" id="Seg_1833" s="T83">dann</ta>
            <ta e="T85" id="Seg_1834" s="T84">krank.sein-PTCP.HAB</ta>
            <ta e="T86" id="Seg_1835" s="T85">werden-PTCP.PST.[NOM]</ta>
            <ta e="T87" id="Seg_1836" s="T86">Haken.für.den.Kessel-PART</ta>
            <ta e="T88" id="Seg_1837" s="T87">sagen-PST2.[3SG]</ta>
            <ta e="T89" id="Seg_1838" s="T88">Alte.[NOM]</ta>
            <ta e="T90" id="Seg_1839" s="T89">Haken.für.den.Kessel-ACC</ta>
            <ta e="T91" id="Seg_1840" s="T90">geben-PST2.[3SG]</ta>
            <ta e="T92" id="Seg_1841" s="T91">doch</ta>
            <ta e="T93" id="Seg_1842" s="T92">dann</ta>
            <ta e="T94" id="Seg_1843" s="T93">aufstehen-CVB.SIM</ta>
            <ta e="T95" id="Seg_1844" s="T94">springen-CVB.SEQ</ta>
            <ta e="T96" id="Seg_1845" s="T95">alter.Mann.[NOM]</ta>
            <ta e="T97" id="Seg_1846" s="T96">doch</ta>
            <ta e="T98" id="Seg_1847" s="T97">Hase-PL-ACC</ta>
            <ta e="T99" id="Seg_1848" s="T98">töten-ITER-PST2.[3SG]</ta>
            <ta e="T100" id="Seg_1849" s="T99">EVID</ta>
            <ta e="T101" id="Seg_1850" s="T100">Schamane-3PL-ACC</ta>
            <ta e="T102" id="Seg_1851" s="T101">schlagen-CVB.PURP</ta>
            <ta e="T103" id="Seg_1852" s="T102">wollen-PST2-3SG</ta>
            <ta e="T104" id="Seg_1853" s="T103">Rauchloch-3SG-INSTR</ta>
            <ta e="T105" id="Seg_1854" s="T104">springen-CVB.SEQ</ta>
            <ta e="T106" id="Seg_1855" s="T105">weggehen-PST2.[3SG]</ta>
            <ta e="T107" id="Seg_1856" s="T106">Haken.für.den.Kessel-3SG-INSTR</ta>
            <ta e="T108" id="Seg_1857" s="T107">Ohr-3SG-GEN</ta>
            <ta e="T109" id="Seg_1858" s="T108">Spitze-3SG-ACC</ta>
            <ta e="T110" id="Seg_1859" s="T109">nur</ta>
            <ta e="T111" id="Seg_1860" s="T110">schlagen-EP-PST2.[3SG]</ta>
            <ta e="T112" id="Seg_1861" s="T111">jenes</ta>
            <ta e="T113" id="Seg_1862" s="T112">Ruß-3SG.[NOM]</ta>
            <ta e="T114" id="Seg_1863" s="T113">schmutzig.werden-EP-PTCP.PST-EP-ABL</ta>
            <ta e="T115" id="Seg_1864" s="T114">Hase.[NOM]</ta>
            <ta e="T116" id="Seg_1865" s="T115">Ohr-3SG-GEN</ta>
            <ta e="T117" id="Seg_1866" s="T116">Spitze-3SG.[NOM]</ta>
            <ta e="T118" id="Seg_1867" s="T117">schwarz.[NOM]</ta>
            <ta e="T119" id="Seg_1868" s="T118">dann</ta>
            <ta e="T120" id="Seg_1869" s="T119">wieder</ta>
            <ta e="T121" id="Seg_1870" s="T120">krank.sein-EP-PST2-3SG</ta>
            <ta e="T122" id="Seg_1871" s="T121">sein-CVB.SEQ</ta>
            <ta e="T123" id="Seg_1872" s="T122">Polarfuchs.[NOM]</ta>
            <ta e="T124" id="Seg_1873" s="T123">Schamane-3SG-ACC</ta>
            <ta e="T125" id="Seg_1874" s="T124">rufen-CAUS-PST2.[3SG]</ta>
            <ta e="T126" id="Seg_1875" s="T125">viel</ta>
            <ta e="T127" id="Seg_1876" s="T126">Polarfuchs-ACC</ta>
            <ta e="T128" id="Seg_1877" s="T127">rufen-PTCP.FUT-3SG-ACC</ta>
            <ta e="T129" id="Seg_1878" s="T128">Angst.haben-PST2.[3SG]</ta>
            <ta e="T130" id="Seg_1879" s="T129">Zahn-PROPR-3PL</ta>
            <ta e="T131" id="Seg_1880" s="T130">sagen-CVB.SEQ</ta>
            <ta e="T132" id="Seg_1881" s="T131">Schamane-3PL-ACC</ta>
            <ta e="T133" id="Seg_1882" s="T132">töten-TEMP-1SG</ta>
            <ta e="T134" id="Seg_1883" s="T133">selbst-3PL.[NOM]</ta>
            <ta e="T135" id="Seg_1884" s="T134">und</ta>
            <ta e="T136" id="Seg_1885" s="T135">schützen-PTCP.PRS</ta>
            <ta e="T137" id="Seg_1886" s="T136">Mensch-POSS</ta>
            <ta e="T138" id="Seg_1887" s="T137">NEG</ta>
            <ta e="T139" id="Seg_1888" s="T138">sterben-FUT-3PL</ta>
            <ta e="T140" id="Seg_1889" s="T139">sagen-PST2.[3SG]</ta>
            <ta e="T141" id="Seg_1890" s="T140">Polarfuchs-3SG-GEN</ta>
            <ta e="T142" id="Seg_1891" s="T141">Schamane-3SG-ACC</ta>
            <ta e="T143" id="Seg_1892" s="T142">Haken.für.den.Kessel-3SG-INSTR</ta>
            <ta e="T144" id="Seg_1893" s="T143">schlagen-CVB.SEQ</ta>
            <ta e="T145" id="Seg_1894" s="T144">Schwanz-3SG-GEN</ta>
            <ta e="T146" id="Seg_1895" s="T145">Spitze-3SG-ACC</ta>
            <ta e="T147" id="Seg_1896" s="T146">nur</ta>
            <ta e="T148" id="Seg_1897" s="T147">treffen-PST2.[3SG]</ta>
            <ta e="T149" id="Seg_1898" s="T148">jenes.[NOM]</ta>
            <ta e="T150" id="Seg_1899" s="T149">für</ta>
            <ta e="T151" id="Seg_1900" s="T150">Haken.für.den.Kessel.[NOM]</ta>
            <ta e="T152" id="Seg_1901" s="T151">Ruß-3SG-ABL</ta>
            <ta e="T153" id="Seg_1902" s="T152">Polarfuchs.[NOM]</ta>
            <ta e="T154" id="Seg_1903" s="T153">Puschel-PROPR.[NOM]</ta>
            <ta e="T155" id="Seg_1904" s="T154">werden-PST2.[3SG]</ta>
            <ta e="T156" id="Seg_1905" s="T155">Schwanz-3SG-GEN</ta>
            <ta e="T157" id="Seg_1906" s="T156">Spitze-3SG-DAT/LOC</ta>
            <ta e="T158" id="Seg_1907" s="T157">so</ta>
            <ta e="T159" id="Seg_1908" s="T158">sein-PTCP.PST-3SG-ACC</ta>
            <ta e="T160" id="Seg_1909" s="T159">nachdem</ta>
            <ta e="T161" id="Seg_1910" s="T160">Gott.[NOM]</ta>
            <ta e="T162" id="Seg_1911" s="T161">treffen-EP-PST2.[3SG]</ta>
            <ta e="T163" id="Seg_1912" s="T162">jenes</ta>
            <ta e="T164" id="Seg_1913" s="T163">Mensch-ACC</ta>
            <ta e="T165" id="Seg_1914" s="T164">dieses-ACC</ta>
            <ta e="T166" id="Seg_1915" s="T165">begehen-PTCP.PST</ta>
            <ta e="T167" id="Seg_1916" s="T166">Sünde-2SG-DAT/LOC</ta>
            <ta e="T168" id="Seg_1917" s="T167">sagen-PST2.[3SG]</ta>
            <ta e="T169" id="Seg_1918" s="T168">2SG.[NOM]</ta>
            <ta e="T170" id="Seg_1919" s="T169">Verstoß-2SG.[NOM]</ta>
            <ta e="T171" id="Seg_1920" s="T170">Märchen.[NOM]</ta>
            <ta e="T172" id="Seg_1921" s="T171">wie</ta>
            <ta e="T173" id="Seg_1922" s="T172">Gleichnis.[NOM]</ta>
            <ta e="T174" id="Seg_1923" s="T173">sein-IMP.3SG</ta>
            <ta e="T175" id="Seg_1924" s="T174">sagen-PST2.[3SG]</ta>
            <ta e="T176" id="Seg_1925" s="T175">selbst-2SG.[NOM]</ta>
            <ta e="T177" id="Seg_1926" s="T176">aber</ta>
            <ta e="T178" id="Seg_1927" s="T177">Eichelhäher.[NOM]</ta>
            <ta e="T179" id="Seg_1928" s="T178">Vogel.[NOM]</ta>
            <ta e="T180" id="Seg_1929" s="T179">sein-CVB.SIM</ta>
            <ta e="T181" id="Seg_1930" s="T180">faulen-CVB.SEQ</ta>
            <ta e="T182" id="Seg_1931" s="T181">sich.ernähren-PTCP.PRS</ta>
            <ta e="T183" id="Seg_1932" s="T182">sein.[IMP.2SG]</ta>
            <ta e="T184" id="Seg_1933" s="T183">sagen-PST2.[3SG]</ta>
            <ta e="T185" id="Seg_1934" s="T184">jenes.[NOM]</ta>
            <ta e="T186" id="Seg_1935" s="T185">für</ta>
            <ta e="T187" id="Seg_1936" s="T186">Eichelhäher.[NOM]</ta>
            <ta e="T188" id="Seg_1937" s="T187">Totfalle.[NOM]</ta>
            <ta e="T189" id="Seg_1938" s="T188">Köder-3SG-ACC</ta>
            <ta e="T190" id="Seg_1939" s="T189">essen-CVB.SIM</ta>
            <ta e="T191" id="Seg_1940" s="T190">gehen-PRS.[3SG]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_1941" s="T0">давно</ta>
            <ta e="T2" id="Seg_1942" s="T1">один</ta>
            <ta e="T3" id="Seg_1943" s="T2">человек.[NOM]</ta>
            <ta e="T4" id="Seg_1944" s="T3">жить-PST2.[3SG]</ta>
            <ta e="T5" id="Seg_1945" s="T4">этот</ta>
            <ta e="T6" id="Seg_1946" s="T5">человек.[NOM]</ta>
            <ta e="T7" id="Seg_1947" s="T6">думать-PST2.[3SG]</ta>
            <ta e="T8" id="Seg_1948" s="T7">песец-ACC</ta>
            <ta e="T9" id="Seg_1949" s="T8">каждый-3SG-ACC</ta>
            <ta e="T10" id="Seg_1950" s="T9">заяц-ACC</ta>
            <ta e="T11" id="Seg_1951" s="T10">каждый-3SG-ACC</ta>
            <ta e="T12" id="Seg_1952" s="T11">убить-CVB.SEQ</ta>
            <ta e="T13" id="Seg_1953" s="T12">бросать-PTCP.FUT-DAT/LOC</ta>
            <ta e="T14" id="Seg_1954" s="T13">один-ACC</ta>
            <ta e="T15" id="Seg_1955" s="T14">NEG</ta>
            <ta e="T16" id="Seg_1956" s="T15">быть.лишным-EP-CAUS-EP-NEG.[IMP.2SG]</ta>
            <ta e="T17" id="Seg_1957" s="T16">говорить-PST2.[3SG]</ta>
            <ta e="T18" id="Seg_1958" s="T17">сын-3SG-DAT/LOC</ta>
            <ta e="T19" id="Seg_1959" s="T18">говорить-PST2.[3SG]</ta>
            <ta e="T20" id="Seg_1960" s="T19">1SG.[NOM]</ta>
            <ta e="T21" id="Seg_1961" s="T20">быть.больным-EP-PTCP.PST</ta>
            <ta e="T22" id="Seg_1962" s="T21">быть-IMP.1SG</ta>
            <ta e="T23" id="Seg_1963" s="T22">2SG.[NOM]</ta>
            <ta e="T24" id="Seg_1964" s="T23">идти-CVB.SEQ-2SG</ta>
            <ta e="T25" id="Seg_1965" s="T24">заяц.[NOM]</ta>
            <ta e="T26" id="Seg_1966" s="T25">шаман-3SG-ACC</ta>
            <ta e="T27" id="Seg_1967" s="T26">приносить.[IMP.2SG]</ta>
            <ta e="T28" id="Seg_1968" s="T27">потом</ta>
            <ta e="T29" id="Seg_1969" s="T28">3SG-DAT/LOC</ta>
            <ta e="T30" id="Seg_1970" s="T29">дьячок-3SG-INSTR</ta>
            <ta e="T31" id="Seg_1971" s="T30">каждый</ta>
            <ta e="T32" id="Seg_1972" s="T31">заяц-ACC</ta>
            <ta e="T33" id="Seg_1973" s="T32">гнать-CVB.SEQ</ta>
            <ta e="T34" id="Seg_1974" s="T33">принести.[IMP.2SG]</ta>
            <ta e="T35" id="Seg_1975" s="T34">сын-3SG.[NOM]</ta>
            <ta e="T36" id="Seg_1976" s="T35">идти-PST2-3SG</ta>
            <ta e="T37" id="Seg_1977" s="T36">один</ta>
            <ta e="T38" id="Seg_1978" s="T37">заяц.[NOM]</ta>
            <ta e="T39" id="Seg_1979" s="T38">сидеть-PRS.[3SG]</ta>
            <ta e="T40" id="Seg_1980" s="T39">вот</ta>
            <ta e="T41" id="Seg_1981" s="T40">эй</ta>
            <ta e="T42" id="Seg_1982" s="T41">шаман-2PL.[NOM]</ta>
            <ta e="T43" id="Seg_1983" s="T42">где=Q</ta>
            <ta e="T44" id="Seg_1984" s="T43">говорить-PRS.[3SG]</ta>
            <ta e="T45" id="Seg_1985" s="T44">там</ta>
            <ta e="T46" id="Seg_1986" s="T45">есть</ta>
            <ta e="T47" id="Seg_1987" s="T46">говорить-CVB.SEQ</ta>
            <ta e="T48" id="Seg_1988" s="T47">показывать-CVB.SEQ</ta>
            <ta e="T49" id="Seg_1989" s="T48">давать-PRS.[3SG]</ta>
            <ta e="T50" id="Seg_1990" s="T49">идти-CVB.SEQ</ta>
            <ta e="T51" id="Seg_1991" s="T50">вот</ta>
            <ta e="T52" id="Seg_1992" s="T51">выручать.[IMP.2SG]</ta>
            <ta e="T53" id="Seg_1993" s="T52">говорить-PRS.[3SG]</ta>
            <ta e="T54" id="Seg_1994" s="T53">отец-1SG.[NOM]</ta>
            <ta e="T55" id="Seg_1995" s="T54">умирать-CVB.PURP</ta>
            <ta e="T56" id="Seg_1996" s="T55">звать-PASS-PST1-3SG</ta>
            <ta e="T57" id="Seg_1997" s="T56">помогать-CVB.SEQ</ta>
            <ta e="T58" id="Seg_1998" s="T57">попробовать.[IMP.2SG]</ta>
            <ta e="T59" id="Seg_1999" s="T58">говорить-PRS.[3SG]</ta>
            <ta e="T60" id="Seg_2000" s="T59">этот</ta>
            <ta e="T61" id="Seg_2001" s="T60">время-DAT/LOC</ta>
            <ta e="T62" id="Seg_2002" s="T61">говорить-PRS.[3SG]</ta>
            <ta e="T63" id="Seg_2003" s="T62">оставаться-PTCP.HAB</ta>
            <ta e="T64" id="Seg_2004" s="T63">человек.[NOM]</ta>
            <ta e="T65" id="Seg_2005" s="T64">старуха-3SG-DAT/LOC</ta>
            <ta e="T66" id="Seg_2006" s="T65">собираться-EP-REFL-TEMP-3PL</ta>
            <ta e="T67" id="Seg_2007" s="T66">2SG.[NOM]</ta>
            <ta e="T68" id="Seg_2008" s="T67">дверь-2SG-ACC</ta>
            <ta e="T69" id="Seg_2009" s="T68">перевешивать-EP-CAUS-CVB.SEQ</ta>
            <ta e="T70" id="Seg_2010" s="T69">бросать-FUT.[IMP.2SG]</ta>
            <ta e="T71" id="Seg_2011" s="T70">говорить-PRS.[3SG]</ta>
            <ta e="T72" id="Seg_2012" s="T71">шаман-3PL.[NOM]</ta>
            <ta e="T73" id="Seg_2013" s="T72">приходить-CVB.SEQ</ta>
            <ta e="T74" id="Seg_2014" s="T73">заяц.[NOM]</ta>
            <ta e="T75" id="Seg_2015" s="T74">очень.много-ACC</ta>
            <ta e="T76" id="Seg_2016" s="T75">дьячок-3SG-INSTR</ta>
            <ta e="T77" id="Seg_2017" s="T76">принести-PRS.[3SG]</ta>
            <ta e="T78" id="Seg_2018" s="T77">этот</ta>
            <ta e="T79" id="Seg_2019" s="T78">приносить-CVB.SEQ</ta>
            <ta e="T80" id="Seg_2020" s="T79">камлать-EP-CAUS-TEMP-3SG</ta>
            <ta e="T81" id="Seg_2021" s="T80">старуха.[NOM]</ta>
            <ta e="T82" id="Seg_2022" s="T81">дверь-ACC</ta>
            <ta e="T83" id="Seg_2023" s="T82">перевешивать-EP-CAUS-PRS.[3SG]</ta>
            <ta e="T84" id="Seg_2024" s="T83">потом</ta>
            <ta e="T85" id="Seg_2025" s="T84">быть.больным-PTCP.HAB</ta>
            <ta e="T86" id="Seg_2026" s="T85">становиться-PTCP.PST.[NOM]</ta>
            <ta e="T87" id="Seg_2027" s="T86">крюк.для.котла-PART</ta>
            <ta e="T88" id="Seg_2028" s="T87">говорить-PST2.[3SG]</ta>
            <ta e="T89" id="Seg_2029" s="T88">старуха.[NOM]</ta>
            <ta e="T90" id="Seg_2030" s="T89">крюк.для.котла-ACC</ta>
            <ta e="T91" id="Seg_2031" s="T90">давать-PST2.[3SG]</ta>
            <ta e="T92" id="Seg_2032" s="T91">вот</ta>
            <ta e="T93" id="Seg_2033" s="T92">вот</ta>
            <ta e="T94" id="Seg_2034" s="T93">вставать-CVB.SIM</ta>
            <ta e="T95" id="Seg_2035" s="T94">прыгать-CVB.SEQ</ta>
            <ta e="T96" id="Seg_2036" s="T95">старик.[NOM]</ta>
            <ta e="T97" id="Seg_2037" s="T96">вот</ta>
            <ta e="T98" id="Seg_2038" s="T97">заяц-PL-ACC</ta>
            <ta e="T99" id="Seg_2039" s="T98">убить-ITER-PST2.[3SG]</ta>
            <ta e="T100" id="Seg_2040" s="T99">EVID</ta>
            <ta e="T101" id="Seg_2041" s="T100">шаман-3PL-ACC</ta>
            <ta e="T102" id="Seg_2042" s="T101">бить-CVB.PURP</ta>
            <ta e="T103" id="Seg_2043" s="T102">хотеть-PST2-3SG</ta>
            <ta e="T104" id="Seg_2044" s="T103">дымоход-3SG-INSTR</ta>
            <ta e="T105" id="Seg_2045" s="T104">прыгать-CVB.SEQ</ta>
            <ta e="T106" id="Seg_2046" s="T105">уходить-PST2.[3SG]</ta>
            <ta e="T107" id="Seg_2047" s="T106">крюк.для.котла-3SG-INSTR</ta>
            <ta e="T108" id="Seg_2048" s="T107">ухо-3SG-GEN</ta>
            <ta e="T109" id="Seg_2049" s="T108">верхушка-3SG-ACC</ta>
            <ta e="T110" id="Seg_2050" s="T109">только</ta>
            <ta e="T111" id="Seg_2051" s="T110">бить-EP-PST2.[3SG]</ta>
            <ta e="T112" id="Seg_2052" s="T111">тот</ta>
            <ta e="T113" id="Seg_2053" s="T112">копоть-3SG.[NOM]</ta>
            <ta e="T114" id="Seg_2054" s="T113">пачкаться-EP-PTCP.PST-EP-ABL</ta>
            <ta e="T115" id="Seg_2055" s="T114">заяц.[NOM]</ta>
            <ta e="T116" id="Seg_2056" s="T115">ухо-3SG-GEN</ta>
            <ta e="T117" id="Seg_2057" s="T116">верхушка-3SG.[NOM]</ta>
            <ta e="T118" id="Seg_2058" s="T117">черный.[NOM]</ta>
            <ta e="T119" id="Seg_2059" s="T118">потом</ta>
            <ta e="T120" id="Seg_2060" s="T119">опять</ta>
            <ta e="T121" id="Seg_2061" s="T120">быть.больным-EP-PST2-3SG</ta>
            <ta e="T122" id="Seg_2062" s="T121">быть-CVB.SEQ</ta>
            <ta e="T123" id="Seg_2063" s="T122">песец.[NOM]</ta>
            <ta e="T124" id="Seg_2064" s="T123">шаман-3SG-ACC</ta>
            <ta e="T125" id="Seg_2065" s="T124">звать-CAUS-PST2.[3SG]</ta>
            <ta e="T126" id="Seg_2066" s="T125">много</ta>
            <ta e="T127" id="Seg_2067" s="T126">песец-ACC</ta>
            <ta e="T128" id="Seg_2068" s="T127">звать-PTCP.FUT-3SG-ACC</ta>
            <ta e="T129" id="Seg_2069" s="T128">бояться-PST2.[3SG]</ta>
            <ta e="T130" id="Seg_2070" s="T129">зуб-PROPR-3PL</ta>
            <ta e="T131" id="Seg_2071" s="T130">говорить-CVB.SEQ</ta>
            <ta e="T132" id="Seg_2072" s="T131">шаман-3PL-ACC</ta>
            <ta e="T133" id="Seg_2073" s="T132">убить-TEMP-1SG</ta>
            <ta e="T134" id="Seg_2074" s="T133">сам-3PL.[NOM]</ta>
            <ta e="T135" id="Seg_2075" s="T134">да</ta>
            <ta e="T136" id="Seg_2076" s="T135">защищать-PTCP.PRS</ta>
            <ta e="T137" id="Seg_2077" s="T136">человек-POSS</ta>
            <ta e="T138" id="Seg_2078" s="T137">NEG</ta>
            <ta e="T139" id="Seg_2079" s="T138">умирать-FUT-3PL</ta>
            <ta e="T140" id="Seg_2080" s="T139">говорить-PST2.[3SG]</ta>
            <ta e="T141" id="Seg_2081" s="T140">песец-3SG-GEN</ta>
            <ta e="T142" id="Seg_2082" s="T141">шаман-3SG-ACC</ta>
            <ta e="T143" id="Seg_2083" s="T142">крюк.для.котла-3SG-INSTR</ta>
            <ta e="T144" id="Seg_2084" s="T143">бить-CVB.SEQ</ta>
            <ta e="T145" id="Seg_2085" s="T144">хвост-3SG-GEN</ta>
            <ta e="T146" id="Seg_2086" s="T145">верхушка-3SG-ACC</ta>
            <ta e="T147" id="Seg_2087" s="T146">только</ta>
            <ta e="T148" id="Seg_2088" s="T147">попадать.в-PST2.[3SG]</ta>
            <ta e="T149" id="Seg_2089" s="T148">тот.[NOM]</ta>
            <ta e="T150" id="Seg_2090" s="T149">для</ta>
            <ta e="T151" id="Seg_2091" s="T150">крюк.для.котла.[NOM]</ta>
            <ta e="T152" id="Seg_2092" s="T151">копоть-3SG-ABL</ta>
            <ta e="T153" id="Seg_2093" s="T152">песец.[NOM]</ta>
            <ta e="T154" id="Seg_2094" s="T153">кисточка-PROPR.[NOM]</ta>
            <ta e="T155" id="Seg_2095" s="T154">становиться-PST2.[3SG]</ta>
            <ta e="T156" id="Seg_2096" s="T155">хвост-3SG-GEN</ta>
            <ta e="T157" id="Seg_2097" s="T156">верхушка-3SG-DAT/LOC</ta>
            <ta e="T158" id="Seg_2098" s="T157">так</ta>
            <ta e="T159" id="Seg_2099" s="T158">быть-PTCP.PST-3SG-ACC</ta>
            <ta e="T160" id="Seg_2100" s="T159">после.того</ta>
            <ta e="T161" id="Seg_2101" s="T160">Бог.[NOM]</ta>
            <ta e="T162" id="Seg_2102" s="T161">встречать-EP-PST2.[3SG]</ta>
            <ta e="T163" id="Seg_2103" s="T162">тот</ta>
            <ta e="T164" id="Seg_2104" s="T163">человек-ACC</ta>
            <ta e="T165" id="Seg_2105" s="T164">тот-ACC</ta>
            <ta e="T166" id="Seg_2106" s="T165">совершать-PTCP.PST</ta>
            <ta e="T167" id="Seg_2107" s="T166">грех-2SG-DAT/LOC</ta>
            <ta e="T168" id="Seg_2108" s="T167">говорить-PST2.[3SG]</ta>
            <ta e="T169" id="Seg_2109" s="T168">2SG.[NOM]</ta>
            <ta e="T170" id="Seg_2110" s="T169">проступок-2SG.[NOM]</ta>
            <ta e="T171" id="Seg_2111" s="T170">сказка.[NOM]</ta>
            <ta e="T172" id="Seg_2112" s="T171">как</ta>
            <ta e="T173" id="Seg_2113" s="T172">притча.[NOM]</ta>
            <ta e="T174" id="Seg_2114" s="T173">быть-IMP.3SG</ta>
            <ta e="T175" id="Seg_2115" s="T174">говорить-PST2.[3SG]</ta>
            <ta e="T176" id="Seg_2116" s="T175">сам-2SG.[NOM]</ta>
            <ta e="T177" id="Seg_2117" s="T176">однако</ta>
            <ta e="T178" id="Seg_2118" s="T177">сойка.[NOM]</ta>
            <ta e="T179" id="Seg_2119" s="T178">птица.[NOM]</ta>
            <ta e="T180" id="Seg_2120" s="T179">быть-CVB.SIM</ta>
            <ta e="T181" id="Seg_2121" s="T180">гнить-CVB.SEQ</ta>
            <ta e="T182" id="Seg_2122" s="T181">кормиться-PTCP.PRS</ta>
            <ta e="T183" id="Seg_2123" s="T182">быть.[IMP.2SG]</ta>
            <ta e="T184" id="Seg_2124" s="T183">говорить-PST2.[3SG]</ta>
            <ta e="T185" id="Seg_2125" s="T184">тот.[NOM]</ta>
            <ta e="T186" id="Seg_2126" s="T185">для</ta>
            <ta e="T187" id="Seg_2127" s="T186">сойка.[NOM]</ta>
            <ta e="T188" id="Seg_2128" s="T187">пасть.[NOM]</ta>
            <ta e="T189" id="Seg_2129" s="T188">приманка-3SG-ACC</ta>
            <ta e="T190" id="Seg_2130" s="T189">есть-CVB.SIM</ta>
            <ta e="T191" id="Seg_2131" s="T190">идти-PRS.[3SG]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_2132" s="T0">adv</ta>
            <ta e="T2" id="Seg_2133" s="T1">cardnum</ta>
            <ta e="T3" id="Seg_2134" s="T2">n-n:case</ta>
            <ta e="T4" id="Seg_2135" s="T3">v-v:tense-v:pred.pn</ta>
            <ta e="T5" id="Seg_2136" s="T4">dempro</ta>
            <ta e="T6" id="Seg_2137" s="T5">n-n:case</ta>
            <ta e="T7" id="Seg_2138" s="T6">v-v:tense-v:pred.pn</ta>
            <ta e="T8" id="Seg_2139" s="T7">n-n:case</ta>
            <ta e="T9" id="Seg_2140" s="T8">adj-n:poss-n:case</ta>
            <ta e="T10" id="Seg_2141" s="T9">n-n:case</ta>
            <ta e="T11" id="Seg_2142" s="T10">adj-n:poss-n:case</ta>
            <ta e="T12" id="Seg_2143" s="T11">v-v:cvb</ta>
            <ta e="T13" id="Seg_2144" s="T12">v-v:ptcp-v:(case)</ta>
            <ta e="T14" id="Seg_2145" s="T13">cardnum-cardnum:case</ta>
            <ta e="T15" id="Seg_2146" s="T14">ptcl</ta>
            <ta e="T16" id="Seg_2147" s="T15">v-v:(ins)-v&gt;v-v:(ins)-v:(neg)-v:mood.pn</ta>
            <ta e="T17" id="Seg_2148" s="T16">v-v:tense-v:pred.pn</ta>
            <ta e="T18" id="Seg_2149" s="T17">n-n:poss-n:case</ta>
            <ta e="T19" id="Seg_2150" s="T18">v-v:tense-v:pred.pn</ta>
            <ta e="T20" id="Seg_2151" s="T19">pers-pro:case</ta>
            <ta e="T21" id="Seg_2152" s="T20">v-v:(ins)-v:ptcp</ta>
            <ta e="T22" id="Seg_2153" s="T21">v-v:mood.pn</ta>
            <ta e="T23" id="Seg_2154" s="T22">pers-pro:case</ta>
            <ta e="T24" id="Seg_2155" s="T23">v-v:cvb-v:pred.pn</ta>
            <ta e="T25" id="Seg_2156" s="T24">n-n:case</ta>
            <ta e="T26" id="Seg_2157" s="T25">n-n:poss-n:case</ta>
            <ta e="T27" id="Seg_2158" s="T26">v-v:mood.pn</ta>
            <ta e="T28" id="Seg_2159" s="T27">adv</ta>
            <ta e="T29" id="Seg_2160" s="T28">pers-pro:case</ta>
            <ta e="T30" id="Seg_2161" s="T29">n-n:poss-n:case</ta>
            <ta e="T31" id="Seg_2162" s="T30">adj</ta>
            <ta e="T32" id="Seg_2163" s="T31">n-n:case</ta>
            <ta e="T33" id="Seg_2164" s="T32">v-v:cvb</ta>
            <ta e="T34" id="Seg_2165" s="T33">v-v:mood.pn</ta>
            <ta e="T35" id="Seg_2166" s="T34">n-n:(poss)-n:case</ta>
            <ta e="T36" id="Seg_2167" s="T35">v-v:tense-v:poss.pn</ta>
            <ta e="T37" id="Seg_2168" s="T36">cardnum</ta>
            <ta e="T38" id="Seg_2169" s="T37">n-n:case</ta>
            <ta e="T39" id="Seg_2170" s="T38">v-v:tense-v:pred.pn</ta>
            <ta e="T40" id="Seg_2171" s="T39">ptcl</ta>
            <ta e="T41" id="Seg_2172" s="T40">interj</ta>
            <ta e="T42" id="Seg_2173" s="T41">n-n:(poss)-n:case</ta>
            <ta e="T43" id="Seg_2174" s="T42">que-ptcl</ta>
            <ta e="T44" id="Seg_2175" s="T43">v-v:tense-v:pred.pn</ta>
            <ta e="T45" id="Seg_2176" s="T44">adv</ta>
            <ta e="T46" id="Seg_2177" s="T45">ptcl</ta>
            <ta e="T47" id="Seg_2178" s="T46">v-v:cvb</ta>
            <ta e="T48" id="Seg_2179" s="T47">v-v:cvb</ta>
            <ta e="T49" id="Seg_2180" s="T48">v-v:tense-v:pred.pn</ta>
            <ta e="T50" id="Seg_2181" s="T49">v-v:cvb</ta>
            <ta e="T51" id="Seg_2182" s="T50">ptcl</ta>
            <ta e="T52" id="Seg_2183" s="T51">v-v:mood.pn</ta>
            <ta e="T53" id="Seg_2184" s="T52">v-v:tense-v:pred.pn</ta>
            <ta e="T54" id="Seg_2185" s="T53">n-n:(poss)-n:case</ta>
            <ta e="T55" id="Seg_2186" s="T54">v-v:cvb</ta>
            <ta e="T56" id="Seg_2187" s="T55">v-v&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T57" id="Seg_2188" s="T56">v-v:cvb</ta>
            <ta e="T58" id="Seg_2189" s="T57">v-v:mood.pn</ta>
            <ta e="T59" id="Seg_2190" s="T58">v-v:tense-v:pred.pn</ta>
            <ta e="T60" id="Seg_2191" s="T59">dempro</ta>
            <ta e="T61" id="Seg_2192" s="T60">n-n:case</ta>
            <ta e="T62" id="Seg_2193" s="T61">v-v:tense-v:pred.pn</ta>
            <ta e="T63" id="Seg_2194" s="T62">v-v:ptcp</ta>
            <ta e="T64" id="Seg_2195" s="T63">n-n:case</ta>
            <ta e="T65" id="Seg_2196" s="T64">n-n:poss-n:case</ta>
            <ta e="T66" id="Seg_2197" s="T65">v-v:(ins)-v&gt;v-v:mood-v:temp.pn</ta>
            <ta e="T67" id="Seg_2198" s="T66">pers-pro:case</ta>
            <ta e="T68" id="Seg_2199" s="T67">n-n:poss-n:case</ta>
            <ta e="T69" id="Seg_2200" s="T68">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T70" id="Seg_2201" s="T69">v-v:(tense)-v:mood.pn</ta>
            <ta e="T71" id="Seg_2202" s="T70">v-v:tense-v:pred.pn</ta>
            <ta e="T72" id="Seg_2203" s="T71">n-n:(poss)-n:case</ta>
            <ta e="T73" id="Seg_2204" s="T72">v-v:cvb</ta>
            <ta e="T74" id="Seg_2205" s="T73">n-n:case</ta>
            <ta e="T75" id="Seg_2206" s="T74">ptcl-n:case</ta>
            <ta e="T76" id="Seg_2207" s="T75">n-n:poss-n:case</ta>
            <ta e="T77" id="Seg_2208" s="T76">v-v:tense-v:pred.pn</ta>
            <ta e="T78" id="Seg_2209" s="T77">dempro</ta>
            <ta e="T79" id="Seg_2210" s="T78">v-v:cvb</ta>
            <ta e="T80" id="Seg_2211" s="T79">v-v:(ins)-v&gt;v-v:mood-v:temp.pn</ta>
            <ta e="T81" id="Seg_2212" s="T80">n-n:case</ta>
            <ta e="T82" id="Seg_2213" s="T81">n-n:case</ta>
            <ta e="T83" id="Seg_2214" s="T82">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T84" id="Seg_2215" s="T83">adv</ta>
            <ta e="T85" id="Seg_2216" s="T84">v-v:ptcp</ta>
            <ta e="T86" id="Seg_2217" s="T85">v-v:ptcp-v:(case)</ta>
            <ta e="T87" id="Seg_2218" s="T86">n-n:case</ta>
            <ta e="T88" id="Seg_2219" s="T87">v-v:tense-v:pred.pn</ta>
            <ta e="T89" id="Seg_2220" s="T88">n-n:case</ta>
            <ta e="T90" id="Seg_2221" s="T89">n-n:case</ta>
            <ta e="T91" id="Seg_2222" s="T90">v-v:tense-v:pred.pn</ta>
            <ta e="T92" id="Seg_2223" s="T91">ptcl</ta>
            <ta e="T93" id="Seg_2224" s="T92">adv</ta>
            <ta e="T94" id="Seg_2225" s="T93">v-v:cvb</ta>
            <ta e="T95" id="Seg_2226" s="T94">v-v:cvb</ta>
            <ta e="T96" id="Seg_2227" s="T95">n-n:case</ta>
            <ta e="T97" id="Seg_2228" s="T96">ptcl</ta>
            <ta e="T98" id="Seg_2229" s="T97">n-n:(num)-n:case</ta>
            <ta e="T99" id="Seg_2230" s="T98">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T100" id="Seg_2231" s="T99">ptcl</ta>
            <ta e="T101" id="Seg_2232" s="T100">n-n:poss-n:case</ta>
            <ta e="T102" id="Seg_2233" s="T101">v-v:cvb</ta>
            <ta e="T103" id="Seg_2234" s="T102">v-v:tense-v:poss.pn</ta>
            <ta e="T104" id="Seg_2235" s="T103">n-n:poss-n:case</ta>
            <ta e="T105" id="Seg_2236" s="T104">v-v:cvb</ta>
            <ta e="T106" id="Seg_2237" s="T105">v-v:tense-v:pred.pn</ta>
            <ta e="T107" id="Seg_2238" s="T106">n-n:poss-n:case</ta>
            <ta e="T108" id="Seg_2239" s="T107">n-n:poss-n:case</ta>
            <ta e="T109" id="Seg_2240" s="T108">n-n:poss-n:case</ta>
            <ta e="T110" id="Seg_2241" s="T109">ptcl</ta>
            <ta e="T111" id="Seg_2242" s="T110">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T112" id="Seg_2243" s="T111">dempro</ta>
            <ta e="T113" id="Seg_2244" s="T112">n-n:(poss)-n:case</ta>
            <ta e="T114" id="Seg_2245" s="T113">v-v:(ins)-v:ptcp-v:(ins)-v:(case)</ta>
            <ta e="T115" id="Seg_2246" s="T114">n-n:case</ta>
            <ta e="T116" id="Seg_2247" s="T115">n-n:poss-n:case</ta>
            <ta e="T117" id="Seg_2248" s="T116">n-n:(poss)-n:case</ta>
            <ta e="T118" id="Seg_2249" s="T117">adj-n:case</ta>
            <ta e="T119" id="Seg_2250" s="T118">adv</ta>
            <ta e="T120" id="Seg_2251" s="T119">ptcl</ta>
            <ta e="T121" id="Seg_2252" s="T120">v-v:(ins)-v:tense-v:poss.pn</ta>
            <ta e="T122" id="Seg_2253" s="T121">v-v:cvb</ta>
            <ta e="T123" id="Seg_2254" s="T122">n-n:case</ta>
            <ta e="T124" id="Seg_2255" s="T123">n-n:poss-n:case</ta>
            <ta e="T125" id="Seg_2256" s="T124">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T126" id="Seg_2257" s="T125">quant</ta>
            <ta e="T127" id="Seg_2258" s="T126">n-n:case</ta>
            <ta e="T128" id="Seg_2259" s="T127">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T129" id="Seg_2260" s="T128">v-v:tense-v:pred.pn</ta>
            <ta e="T130" id="Seg_2261" s="T129">n-n&gt;adj-n:(pred.pn)</ta>
            <ta e="T131" id="Seg_2262" s="T130">v-v:cvb</ta>
            <ta e="T132" id="Seg_2263" s="T131">n-n:poss-n:case</ta>
            <ta e="T133" id="Seg_2264" s="T132">v-v:mood-v:temp.pn</ta>
            <ta e="T134" id="Seg_2265" s="T133">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T135" id="Seg_2266" s="T134">conj</ta>
            <ta e="T136" id="Seg_2267" s="T135">v-v:ptcp</ta>
            <ta e="T137" id="Seg_2268" s="T136">n-n:(poss)</ta>
            <ta e="T138" id="Seg_2269" s="T137">ptcl</ta>
            <ta e="T139" id="Seg_2270" s="T138">v-v:tense-v:poss.pn</ta>
            <ta e="T140" id="Seg_2271" s="T139">v-v:tense-v:pred.pn</ta>
            <ta e="T141" id="Seg_2272" s="T140">n-n:poss-n:case</ta>
            <ta e="T142" id="Seg_2273" s="T141">n-n:poss-n:case</ta>
            <ta e="T143" id="Seg_2274" s="T142">n-n:poss-n:case</ta>
            <ta e="T144" id="Seg_2275" s="T143">v-v:cvb</ta>
            <ta e="T145" id="Seg_2276" s="T144">n-n:poss-n:case</ta>
            <ta e="T146" id="Seg_2277" s="T145">n-n:poss-n:case</ta>
            <ta e="T147" id="Seg_2278" s="T146">ptcl</ta>
            <ta e="T148" id="Seg_2279" s="T147">v-v:tense-v:pred.pn</ta>
            <ta e="T149" id="Seg_2280" s="T148">dempro-pro:case</ta>
            <ta e="T150" id="Seg_2281" s="T149">post</ta>
            <ta e="T151" id="Seg_2282" s="T150">n-n:case</ta>
            <ta e="T152" id="Seg_2283" s="T151">n-n:poss-n:case</ta>
            <ta e="T153" id="Seg_2284" s="T152">n-n:case</ta>
            <ta e="T154" id="Seg_2285" s="T153">n-n&gt;adj-n:case</ta>
            <ta e="T155" id="Seg_2286" s="T154">v-v:tense-v:pred.pn</ta>
            <ta e="T156" id="Seg_2287" s="T155">n-n:poss-n:case</ta>
            <ta e="T157" id="Seg_2288" s="T156">n-n:poss-n:case</ta>
            <ta e="T158" id="Seg_2289" s="T157">adv</ta>
            <ta e="T159" id="Seg_2290" s="T158">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T160" id="Seg_2291" s="T159">post</ta>
            <ta e="T161" id="Seg_2292" s="T160">n-n:case</ta>
            <ta e="T162" id="Seg_2293" s="T161">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T163" id="Seg_2294" s="T162">dempro</ta>
            <ta e="T164" id="Seg_2295" s="T163">n-n:case</ta>
            <ta e="T165" id="Seg_2296" s="T164">dempro-pro:case</ta>
            <ta e="T166" id="Seg_2297" s="T165">v-v:ptcp</ta>
            <ta e="T167" id="Seg_2298" s="T166">n-n:poss-n:case</ta>
            <ta e="T168" id="Seg_2299" s="T167">v-v:tense-v:pred.pn</ta>
            <ta e="T169" id="Seg_2300" s="T168">pers-pro:case</ta>
            <ta e="T170" id="Seg_2301" s="T169">n-n:(poss)-n:case</ta>
            <ta e="T171" id="Seg_2302" s="T170">n-n:case</ta>
            <ta e="T172" id="Seg_2303" s="T171">post</ta>
            <ta e="T173" id="Seg_2304" s="T172">n-n:case</ta>
            <ta e="T174" id="Seg_2305" s="T173">v-v:mood.pn</ta>
            <ta e="T175" id="Seg_2306" s="T174">v-v:tense-v:pred.pn</ta>
            <ta e="T176" id="Seg_2307" s="T175">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T177" id="Seg_2308" s="T176">ptcl</ta>
            <ta e="T178" id="Seg_2309" s="T177">n-n:case</ta>
            <ta e="T179" id="Seg_2310" s="T178">n-n:case</ta>
            <ta e="T180" id="Seg_2311" s="T179">v-v:cvb</ta>
            <ta e="T181" id="Seg_2312" s="T180">v-v:cvb</ta>
            <ta e="T182" id="Seg_2313" s="T181">v-v:ptcp</ta>
            <ta e="T183" id="Seg_2314" s="T182">v-v:mood.pn</ta>
            <ta e="T184" id="Seg_2315" s="T183">v-v:tense-v:pred.pn</ta>
            <ta e="T185" id="Seg_2316" s="T184">dempro-pro:case</ta>
            <ta e="T186" id="Seg_2317" s="T185">post</ta>
            <ta e="T187" id="Seg_2318" s="T186">n-n:case</ta>
            <ta e="T188" id="Seg_2319" s="T187">n-n:case</ta>
            <ta e="T189" id="Seg_2320" s="T188">n-n:poss-n:case</ta>
            <ta e="T190" id="Seg_2321" s="T189">v-v:cvb</ta>
            <ta e="T191" id="Seg_2322" s="T190">v-v:tense-v:pred.pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_2323" s="T0">adv</ta>
            <ta e="T2" id="Seg_2324" s="T1">cardnum</ta>
            <ta e="T3" id="Seg_2325" s="T2">n</ta>
            <ta e="T4" id="Seg_2326" s="T3">v</ta>
            <ta e="T5" id="Seg_2327" s="T4">dempro</ta>
            <ta e="T6" id="Seg_2328" s="T5">n</ta>
            <ta e="T7" id="Seg_2329" s="T6">v</ta>
            <ta e="T8" id="Seg_2330" s="T7">n</ta>
            <ta e="T9" id="Seg_2331" s="T8">adj</ta>
            <ta e="T10" id="Seg_2332" s="T9">n</ta>
            <ta e="T11" id="Seg_2333" s="T10">adj</ta>
            <ta e="T12" id="Seg_2334" s="T11">v</ta>
            <ta e="T13" id="Seg_2335" s="T12">aux</ta>
            <ta e="T14" id="Seg_2336" s="T13">cardnum</ta>
            <ta e="T15" id="Seg_2337" s="T14">ptcl</ta>
            <ta e="T16" id="Seg_2338" s="T15">v</ta>
            <ta e="T17" id="Seg_2339" s="T16">v</ta>
            <ta e="T18" id="Seg_2340" s="T17">n</ta>
            <ta e="T19" id="Seg_2341" s="T18">v</ta>
            <ta e="T20" id="Seg_2342" s="T19">pers</ta>
            <ta e="T21" id="Seg_2343" s="T20">v</ta>
            <ta e="T22" id="Seg_2344" s="T21">aux</ta>
            <ta e="T23" id="Seg_2345" s="T22">pers</ta>
            <ta e="T24" id="Seg_2346" s="T23">v</ta>
            <ta e="T25" id="Seg_2347" s="T24">n</ta>
            <ta e="T26" id="Seg_2348" s="T25">n</ta>
            <ta e="T27" id="Seg_2349" s="T26">v</ta>
            <ta e="T28" id="Seg_2350" s="T27">adv</ta>
            <ta e="T29" id="Seg_2351" s="T28">pers</ta>
            <ta e="T30" id="Seg_2352" s="T29">n</ta>
            <ta e="T31" id="Seg_2353" s="T30">adj</ta>
            <ta e="T32" id="Seg_2354" s="T31">n</ta>
            <ta e="T33" id="Seg_2355" s="T32">v</ta>
            <ta e="T34" id="Seg_2356" s="T33">v</ta>
            <ta e="T35" id="Seg_2357" s="T34">n</ta>
            <ta e="T36" id="Seg_2358" s="T35">v</ta>
            <ta e="T37" id="Seg_2359" s="T36">cardnum</ta>
            <ta e="T38" id="Seg_2360" s="T37">n</ta>
            <ta e="T39" id="Seg_2361" s="T38">v</ta>
            <ta e="T40" id="Seg_2362" s="T39">ptcl</ta>
            <ta e="T41" id="Seg_2363" s="T40">interj</ta>
            <ta e="T42" id="Seg_2364" s="T41">n</ta>
            <ta e="T43" id="Seg_2365" s="T42">que</ta>
            <ta e="T44" id="Seg_2366" s="T43">v</ta>
            <ta e="T45" id="Seg_2367" s="T44">adv</ta>
            <ta e="T46" id="Seg_2368" s="T45">ptcl</ta>
            <ta e="T47" id="Seg_2369" s="T46">v</ta>
            <ta e="T48" id="Seg_2370" s="T47">v</ta>
            <ta e="T49" id="Seg_2371" s="T48">aux</ta>
            <ta e="T50" id="Seg_2372" s="T49">v</ta>
            <ta e="T51" id="Seg_2373" s="T50">ptcl</ta>
            <ta e="T52" id="Seg_2374" s="T51">v</ta>
            <ta e="T53" id="Seg_2375" s="T52">v</ta>
            <ta e="T54" id="Seg_2376" s="T53">n</ta>
            <ta e="T55" id="Seg_2377" s="T54">v</ta>
            <ta e="T56" id="Seg_2378" s="T55">v</ta>
            <ta e="T57" id="Seg_2379" s="T56">v</ta>
            <ta e="T58" id="Seg_2380" s="T57">v</ta>
            <ta e="T59" id="Seg_2381" s="T58">v</ta>
            <ta e="T60" id="Seg_2382" s="T59">dempro</ta>
            <ta e="T61" id="Seg_2383" s="T60">n</ta>
            <ta e="T62" id="Seg_2384" s="T61">v</ta>
            <ta e="T63" id="Seg_2385" s="T62">v</ta>
            <ta e="T64" id="Seg_2386" s="T63">n</ta>
            <ta e="T65" id="Seg_2387" s="T64">n</ta>
            <ta e="T66" id="Seg_2388" s="T65">v</ta>
            <ta e="T67" id="Seg_2389" s="T66">pers</ta>
            <ta e="T68" id="Seg_2390" s="T67">n</ta>
            <ta e="T69" id="Seg_2391" s="T68">v</ta>
            <ta e="T70" id="Seg_2392" s="T69">aux</ta>
            <ta e="T71" id="Seg_2393" s="T70">v</ta>
            <ta e="T72" id="Seg_2394" s="T71">n</ta>
            <ta e="T73" id="Seg_2395" s="T72">v</ta>
            <ta e="T74" id="Seg_2396" s="T73">n</ta>
            <ta e="T75" id="Seg_2397" s="T74">ptcl</ta>
            <ta e="T76" id="Seg_2398" s="T75">n</ta>
            <ta e="T77" id="Seg_2399" s="T76">v</ta>
            <ta e="T78" id="Seg_2400" s="T77">dempro</ta>
            <ta e="T79" id="Seg_2401" s="T78">v</ta>
            <ta e="T80" id="Seg_2402" s="T79">v</ta>
            <ta e="T81" id="Seg_2403" s="T80">n</ta>
            <ta e="T82" id="Seg_2404" s="T81">n</ta>
            <ta e="T83" id="Seg_2405" s="T82">v</ta>
            <ta e="T84" id="Seg_2406" s="T83">adv</ta>
            <ta e="T85" id="Seg_2407" s="T84">v</ta>
            <ta e="T86" id="Seg_2408" s="T85">cop</ta>
            <ta e="T87" id="Seg_2409" s="T86">n</ta>
            <ta e="T88" id="Seg_2410" s="T87">v</ta>
            <ta e="T89" id="Seg_2411" s="T88">n</ta>
            <ta e="T90" id="Seg_2412" s="T89">n</ta>
            <ta e="T91" id="Seg_2413" s="T90">v</ta>
            <ta e="T92" id="Seg_2414" s="T91">ptcl</ta>
            <ta e="T93" id="Seg_2415" s="T92">adv</ta>
            <ta e="T94" id="Seg_2416" s="T93">v</ta>
            <ta e="T95" id="Seg_2417" s="T94">v</ta>
            <ta e="T96" id="Seg_2418" s="T95">n</ta>
            <ta e="T97" id="Seg_2419" s="T96">ptcl</ta>
            <ta e="T98" id="Seg_2420" s="T97">n</ta>
            <ta e="T99" id="Seg_2421" s="T98">v</ta>
            <ta e="T100" id="Seg_2422" s="T99">ptcl</ta>
            <ta e="T101" id="Seg_2423" s="T100">n</ta>
            <ta e="T102" id="Seg_2424" s="T101">v</ta>
            <ta e="T103" id="Seg_2425" s="T102">v</ta>
            <ta e="T104" id="Seg_2426" s="T103">n</ta>
            <ta e="T105" id="Seg_2427" s="T104">v</ta>
            <ta e="T106" id="Seg_2428" s="T105">v</ta>
            <ta e="T107" id="Seg_2429" s="T106">n</ta>
            <ta e="T108" id="Seg_2430" s="T107">n</ta>
            <ta e="T109" id="Seg_2431" s="T108">n</ta>
            <ta e="T110" id="Seg_2432" s="T109">ptcl</ta>
            <ta e="T111" id="Seg_2433" s="T110">v</ta>
            <ta e="T112" id="Seg_2434" s="T111">dempro</ta>
            <ta e="T113" id="Seg_2435" s="T112">n</ta>
            <ta e="T114" id="Seg_2436" s="T113">v</ta>
            <ta e="T115" id="Seg_2437" s="T114">n</ta>
            <ta e="T116" id="Seg_2438" s="T115">n</ta>
            <ta e="T117" id="Seg_2439" s="T116">n</ta>
            <ta e="T118" id="Seg_2440" s="T117">adj</ta>
            <ta e="T119" id="Seg_2441" s="T118">adv</ta>
            <ta e="T120" id="Seg_2442" s="T119">ptcl</ta>
            <ta e="T121" id="Seg_2443" s="T120">v</ta>
            <ta e="T122" id="Seg_2444" s="T121">cop</ta>
            <ta e="T123" id="Seg_2445" s="T122">n</ta>
            <ta e="T124" id="Seg_2446" s="T123">n</ta>
            <ta e="T125" id="Seg_2447" s="T124">v</ta>
            <ta e="T126" id="Seg_2448" s="T125">quant</ta>
            <ta e="T127" id="Seg_2449" s="T126">n</ta>
            <ta e="T128" id="Seg_2450" s="T127">v</ta>
            <ta e="T129" id="Seg_2451" s="T128">v</ta>
            <ta e="T130" id="Seg_2452" s="T129">adj</ta>
            <ta e="T131" id="Seg_2453" s="T130">v</ta>
            <ta e="T132" id="Seg_2454" s="T131">n</ta>
            <ta e="T133" id="Seg_2455" s="T132">v</ta>
            <ta e="T134" id="Seg_2456" s="T133">emphpro</ta>
            <ta e="T135" id="Seg_2457" s="T134">conj</ta>
            <ta e="T136" id="Seg_2458" s="T135">v</ta>
            <ta e="T137" id="Seg_2459" s="T136">n</ta>
            <ta e="T138" id="Seg_2460" s="T137">ptcl</ta>
            <ta e="T139" id="Seg_2461" s="T138">v</ta>
            <ta e="T140" id="Seg_2462" s="T139">v</ta>
            <ta e="T141" id="Seg_2463" s="T140">n</ta>
            <ta e="T142" id="Seg_2464" s="T141">n</ta>
            <ta e="T143" id="Seg_2465" s="T142">n</ta>
            <ta e="T144" id="Seg_2466" s="T143">v</ta>
            <ta e="T145" id="Seg_2467" s="T144">n</ta>
            <ta e="T146" id="Seg_2468" s="T145">n</ta>
            <ta e="T147" id="Seg_2469" s="T146">ptcl</ta>
            <ta e="T148" id="Seg_2470" s="T147">v</ta>
            <ta e="T149" id="Seg_2471" s="T148">dempro</ta>
            <ta e="T150" id="Seg_2472" s="T149">post</ta>
            <ta e="T151" id="Seg_2473" s="T150">n</ta>
            <ta e="T152" id="Seg_2474" s="T151">n</ta>
            <ta e="T153" id="Seg_2475" s="T152">n</ta>
            <ta e="T154" id="Seg_2476" s="T153">adj</ta>
            <ta e="T155" id="Seg_2477" s="T154">cop</ta>
            <ta e="T156" id="Seg_2478" s="T155">n</ta>
            <ta e="T157" id="Seg_2479" s="T156">n</ta>
            <ta e="T158" id="Seg_2480" s="T157">adv</ta>
            <ta e="T159" id="Seg_2481" s="T158">cop</ta>
            <ta e="T160" id="Seg_2482" s="T159">post</ta>
            <ta e="T161" id="Seg_2483" s="T160">n</ta>
            <ta e="T162" id="Seg_2484" s="T161">v</ta>
            <ta e="T163" id="Seg_2485" s="T162">dempro</ta>
            <ta e="T164" id="Seg_2486" s="T163">n</ta>
            <ta e="T165" id="Seg_2487" s="T164">dempro</ta>
            <ta e="T166" id="Seg_2488" s="T165">v</ta>
            <ta e="T167" id="Seg_2489" s="T166">n</ta>
            <ta e="T168" id="Seg_2490" s="T167">v</ta>
            <ta e="T169" id="Seg_2491" s="T168">pers</ta>
            <ta e="T170" id="Seg_2492" s="T169">n</ta>
            <ta e="T171" id="Seg_2493" s="T170">n</ta>
            <ta e="T172" id="Seg_2494" s="T171">post</ta>
            <ta e="T173" id="Seg_2495" s="T172">n</ta>
            <ta e="T174" id="Seg_2496" s="T173">cop</ta>
            <ta e="T175" id="Seg_2497" s="T174">v</ta>
            <ta e="T176" id="Seg_2498" s="T175">emphpro</ta>
            <ta e="T177" id="Seg_2499" s="T176">ptcl</ta>
            <ta e="T178" id="Seg_2500" s="T177">n</ta>
            <ta e="T179" id="Seg_2501" s="T178">n</ta>
            <ta e="T180" id="Seg_2502" s="T179">cop</ta>
            <ta e="T181" id="Seg_2503" s="T180">v</ta>
            <ta e="T182" id="Seg_2504" s="T181">v</ta>
            <ta e="T183" id="Seg_2505" s="T182">aux</ta>
            <ta e="T184" id="Seg_2506" s="T183">v</ta>
            <ta e="T185" id="Seg_2507" s="T184">dempro</ta>
            <ta e="T186" id="Seg_2508" s="T185">post</ta>
            <ta e="T187" id="Seg_2509" s="T186">n</ta>
            <ta e="T188" id="Seg_2510" s="T187">n</ta>
            <ta e="T189" id="Seg_2511" s="T188">n</ta>
            <ta e="T190" id="Seg_2512" s="T189">v</ta>
            <ta e="T191" id="Seg_2513" s="T190">aux</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_2514" s="T0">adv:Time</ta>
            <ta e="T3" id="Seg_2515" s="T2">np.h:Th</ta>
            <ta e="T6" id="Seg_2516" s="T5">np.h:E</ta>
            <ta e="T8" id="Seg_2517" s="T7">np:P</ta>
            <ta e="T10" id="Seg_2518" s="T9">np:P</ta>
            <ta e="T13" id="Seg_2519" s="T11">0.1.h:A</ta>
            <ta e="T14" id="Seg_2520" s="T13">np:P</ta>
            <ta e="T17" id="Seg_2521" s="T16">0.3.h:A</ta>
            <ta e="T18" id="Seg_2522" s="T17">np.h:R</ta>
            <ta e="T19" id="Seg_2523" s="T18">0.3.h:A</ta>
            <ta e="T20" id="Seg_2524" s="T19">pro.h:Th</ta>
            <ta e="T23" id="Seg_2525" s="T22">np.h:A</ta>
            <ta e="T25" id="Seg_2526" s="T24">np:Poss</ta>
            <ta e="T26" id="Seg_2527" s="T25">np.h:Th</ta>
            <ta e="T28" id="Seg_2528" s="T27">pro:Time</ta>
            <ta e="T29" id="Seg_2529" s="T28">pro:G</ta>
            <ta e="T30" id="Seg_2530" s="T29">np:Ins</ta>
            <ta e="T32" id="Seg_2531" s="T31">np:Th</ta>
            <ta e="T34" id="Seg_2532" s="T32">0.2.h:A</ta>
            <ta e="T35" id="Seg_2533" s="T34">np.h:A</ta>
            <ta e="T38" id="Seg_2534" s="T37">np.h:Th</ta>
            <ta e="T42" id="Seg_2535" s="T41">0.2.h:Poss np.h:Th</ta>
            <ta e="T44" id="Seg_2536" s="T43">0.3.h:A</ta>
            <ta e="T45" id="Seg_2537" s="T44">adv:L</ta>
            <ta e="T46" id="Seg_2538" s="T45">0.3.h:Th</ta>
            <ta e="T49" id="Seg_2539" s="T48">0.3.h:A</ta>
            <ta e="T52" id="Seg_2540" s="T51">0.2.h:A</ta>
            <ta e="T53" id="Seg_2541" s="T52">0.3.h:A</ta>
            <ta e="T54" id="Seg_2542" s="T53">0.1.h:Poss np.h:Th</ta>
            <ta e="T58" id="Seg_2543" s="T56">0.2.h:A</ta>
            <ta e="T59" id="Seg_2544" s="T58">0.3.h:A</ta>
            <ta e="T61" id="Seg_2545" s="T60">n:Time</ta>
            <ta e="T64" id="Seg_2546" s="T63">np.h:A</ta>
            <ta e="T65" id="Seg_2547" s="T64">np.h:R</ta>
            <ta e="T66" id="Seg_2548" s="T65">0.3.h:A</ta>
            <ta e="T67" id="Seg_2549" s="T66">pro.h:A</ta>
            <ta e="T68" id="Seg_2550" s="T67">np:P</ta>
            <ta e="T71" id="Seg_2551" s="T70">0.3.h:A</ta>
            <ta e="T72" id="Seg_2552" s="T71">np.h:A</ta>
            <ta e="T74" id="Seg_2553" s="T73">np:Th</ta>
            <ta e="T76" id="Seg_2554" s="T75">np:Ins</ta>
            <ta e="T80" id="Seg_2555" s="T79">0.3.h:A</ta>
            <ta e="T81" id="Seg_2556" s="T80">np.h:A</ta>
            <ta e="T82" id="Seg_2557" s="T81">np:P</ta>
            <ta e="T84" id="Seg_2558" s="T83">pro:Com</ta>
            <ta e="T86" id="Seg_2559" s="T85">np.h:A</ta>
            <ta e="T87" id="Seg_2560" s="T86">np:Th</ta>
            <ta e="T88" id="Seg_2561" s="T87">0.3.h:A</ta>
            <ta e="T89" id="Seg_2562" s="T88">np.h:A</ta>
            <ta e="T90" id="Seg_2563" s="T89">np:Th</ta>
            <ta e="T93" id="Seg_2564" s="T92">adv:Time</ta>
            <ta e="T96" id="Seg_2565" s="T95">np.h:A</ta>
            <ta e="T98" id="Seg_2566" s="T97">np:P</ta>
            <ta e="T101" id="Seg_2567" s="T100">np.h:P</ta>
            <ta e="T103" id="Seg_2568" s="T102">0.3.h:E</ta>
            <ta e="T104" id="Seg_2569" s="T103">np:Path</ta>
            <ta e="T106" id="Seg_2570" s="T104">0.3.h:A</ta>
            <ta e="T107" id="Seg_2571" s="T106">np:Ins</ta>
            <ta e="T108" id="Seg_2572" s="T107">0.3.h:Poss np.h:Poss</ta>
            <ta e="T109" id="Seg_2573" s="T108">np:P</ta>
            <ta e="T111" id="Seg_2574" s="T110">0.3.h:A</ta>
            <ta e="T113" id="Seg_2575" s="T112">np:Cau</ta>
            <ta e="T115" id="Seg_2576" s="T114">np:Poss</ta>
            <ta e="T116" id="Seg_2577" s="T115">np:Poss</ta>
            <ta e="T117" id="Seg_2578" s="T116">np:Th</ta>
            <ta e="T119" id="Seg_2579" s="T118">pro:Time</ta>
            <ta e="T123" id="Seg_2580" s="T122">np:Poss</ta>
            <ta e="T124" id="Seg_2581" s="T123">np.h:Th</ta>
            <ta e="T125" id="Seg_2582" s="T124">0.3.h:A</ta>
            <ta e="T127" id="Seg_2583" s="T126">np:Th</ta>
            <ta e="T129" id="Seg_2584" s="T128">0.3.h:E</ta>
            <ta e="T130" id="Seg_2585" s="T129">0.3:Th</ta>
            <ta e="T132" id="Seg_2586" s="T131">np.h:P</ta>
            <ta e="T133" id="Seg_2587" s="T132">0.1.h:A</ta>
            <ta e="T134" id="Seg_2588" s="T133">pro:P</ta>
            <ta e="T140" id="Seg_2589" s="T139">0.3.h:A </ta>
            <ta e="T141" id="Seg_2590" s="T140">np:Poss</ta>
            <ta e="T142" id="Seg_2591" s="T141">np.h:P</ta>
            <ta e="T143" id="Seg_2592" s="T142">np:Ins</ta>
            <ta e="T145" id="Seg_2593" s="T144">0.3.h:Poss np:Poss</ta>
            <ta e="T146" id="Seg_2594" s="T145">np:Th</ta>
            <ta e="T148" id="Seg_2595" s="T147">0.3.h:E</ta>
            <ta e="T151" id="Seg_2596" s="T150">np:Poss</ta>
            <ta e="T152" id="Seg_2597" s="T151">np:Cau</ta>
            <ta e="T153" id="Seg_2598" s="T152">np:Th</ta>
            <ta e="T156" id="Seg_2599" s="T155">0.3:Poss np:Poss</ta>
            <ta e="T157" id="Seg_2600" s="T156">np:L</ta>
            <ta e="T161" id="Seg_2601" s="T160">np.h:E</ta>
            <ta e="T164" id="Seg_2602" s="T163">np.h:Th</ta>
            <ta e="T167" id="Seg_2603" s="T166">np:Cau</ta>
            <ta e="T168" id="Seg_2604" s="T167">0.3.h:A</ta>
            <ta e="T169" id="Seg_2605" s="T168">pro.h:Poss</ta>
            <ta e="T170" id="Seg_2606" s="T169">np:Th</ta>
            <ta e="T175" id="Seg_2607" s="T174">0.3.h:A</ta>
            <ta e="T176" id="Seg_2608" s="T175">pro.h:Th</ta>
            <ta e="T184" id="Seg_2609" s="T183">0.3.h:A</ta>
            <ta e="T186" id="Seg_2610" s="T184">pro:Cau</ta>
            <ta e="T187" id="Seg_2611" s="T186">np:A</ta>
            <ta e="T188" id="Seg_2612" s="T187">np:Poss</ta>
            <ta e="T189" id="Seg_2613" s="T188">np:P</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T3" id="Seg_2614" s="T2">np.h:S</ta>
            <ta e="T4" id="Seg_2615" s="T3">v:pred</ta>
            <ta e="T6" id="Seg_2616" s="T5">np.h:S</ta>
            <ta e="T7" id="Seg_2617" s="T6">v:pred</ta>
            <ta e="T8" id="Seg_2618" s="T7">np:O</ta>
            <ta e="T10" id="Seg_2619" s="T9">np:O</ta>
            <ta e="T13" id="Seg_2620" s="T11">0.1.h:S</ta>
            <ta e="T14" id="Seg_2621" s="T13">np:O</ta>
            <ta e="T17" id="Seg_2622" s="T16">0.3.h:S v:pred</ta>
            <ta e="T19" id="Seg_2623" s="T18">0.3.h:S v:pred</ta>
            <ta e="T20" id="Seg_2624" s="T19">pro.h:S</ta>
            <ta e="T21" id="Seg_2625" s="T20">adj:pred</ta>
            <ta e="T22" id="Seg_2626" s="T21">cop</ta>
            <ta e="T23" id="Seg_2627" s="T22">pro.h:S</ta>
            <ta e="T26" id="Seg_2628" s="T25">np.h:O</ta>
            <ta e="T27" id="Seg_2629" s="T26">v:pred</ta>
            <ta e="T32" id="Seg_2630" s="T31">np:O</ta>
            <ta e="T34" id="Seg_2631" s="T32">0.2.h:S</ta>
            <ta e="T35" id="Seg_2632" s="T34">np.h:S</ta>
            <ta e="T36" id="Seg_2633" s="T35">v:pred</ta>
            <ta e="T38" id="Seg_2634" s="T37">np.h:S</ta>
            <ta e="T39" id="Seg_2635" s="T38">v:pred</ta>
            <ta e="T42" id="Seg_2636" s="T41">np.h:S</ta>
            <ta e="T43" id="Seg_2637" s="T42">pro:pred</ta>
            <ta e="T44" id="Seg_2638" s="T43">0.3.h:S v:pred</ta>
            <ta e="T46" id="Seg_2639" s="T45">ptcl:pred</ta>
            <ta e="T49" id="Seg_2640" s="T48">0.3.h:S v:pred</ta>
            <ta e="T50" id="Seg_2641" s="T49">s:temp</ta>
            <ta e="T52" id="Seg_2642" s="T51">0.2.h:S v:pred</ta>
            <ta e="T53" id="Seg_2643" s="T52">0.3.h:S v:pred</ta>
            <ta e="T54" id="Seg_2644" s="T53">np.h:S</ta>
            <ta e="T55" id="Seg_2645" s="T54">s:purp</ta>
            <ta e="T56" id="Seg_2646" s="T55">v:pred</ta>
            <ta e="T58" id="Seg_2647" s="T56">0.2.h:S v:pred</ta>
            <ta e="T59" id="Seg_2648" s="T58">0.3.h:S v:pred</ta>
            <ta e="T62" id="Seg_2649" s="T61">v:pred</ta>
            <ta e="T64" id="Seg_2650" s="T63">np.h:S</ta>
            <ta e="T66" id="Seg_2651" s="T65">s:temp</ta>
            <ta e="T67" id="Seg_2652" s="T66">pro.h:S</ta>
            <ta e="T68" id="Seg_2653" s="T67">np:O</ta>
            <ta e="T70" id="Seg_2654" s="T68">v:pred</ta>
            <ta e="T71" id="Seg_2655" s="T70">0.3.h:S v:pred</ta>
            <ta e="T72" id="Seg_2656" s="T71">np.h:S</ta>
            <ta e="T74" id="Seg_2657" s="T73">np:O</ta>
            <ta e="T77" id="Seg_2658" s="T76">v:pred</ta>
            <ta e="T80" id="Seg_2659" s="T77">s:temp</ta>
            <ta e="T81" id="Seg_2660" s="T80">np.h:S</ta>
            <ta e="T82" id="Seg_2661" s="T81">np:O</ta>
            <ta e="T83" id="Seg_2662" s="T82">v:pred</ta>
            <ta e="T86" id="Seg_2663" s="T85">np.h:S</ta>
            <ta e="T87" id="Seg_2664" s="T86">np:O</ta>
            <ta e="T88" id="Seg_2665" s="T87">0.3.h:S v:pred</ta>
            <ta e="T89" id="Seg_2666" s="T88">np.h:S</ta>
            <ta e="T90" id="Seg_2667" s="T89">np:O</ta>
            <ta e="T91" id="Seg_2668" s="T90">v:pred</ta>
            <ta e="T95" id="Seg_2669" s="T93">s:adv</ta>
            <ta e="T96" id="Seg_2670" s="T95">np.h:S</ta>
            <ta e="T98" id="Seg_2671" s="T97">np:O</ta>
            <ta e="T100" id="Seg_2672" s="T98">ptcl:pred</ta>
            <ta e="T102" id="Seg_2673" s="T100">s:purp</ta>
            <ta e="T106" id="Seg_2674" s="T104">0.3.h:S</ta>
            <ta e="T109" id="Seg_2675" s="T108">np:O</ta>
            <ta e="T111" id="Seg_2676" s="T110">0.3.h:S v:pred</ta>
            <ta e="T117" id="Seg_2677" s="T116">np:S</ta>
            <ta e="T118" id="Seg_2678" s="T117">adj:pred</ta>
            <ta e="T122" id="Seg_2679" s="T119">s:adv</ta>
            <ta e="T124" id="Seg_2680" s="T123">np.h:O</ta>
            <ta e="T125" id="Seg_2681" s="T124">0.3.h:S v:pred</ta>
            <ta e="T128" id="Seg_2682" s="T125">s:comp</ta>
            <ta e="T129" id="Seg_2683" s="T128">0.3.h:S v:pred</ta>
            <ta e="T130" id="Seg_2684" s="T129">0.3:S adj:pred</ta>
            <ta e="T133" id="Seg_2685" s="T131">s:temp</ta>
            <ta e="T134" id="Seg_2686" s="T133">pro:S</ta>
            <ta e="T139" id="Seg_2687" s="T138">v:pred</ta>
            <ta e="T140" id="Seg_2688" s="T139">0.3.h:S v:pred</ta>
            <ta e="T144" id="Seg_2689" s="T140">s:adv</ta>
            <ta e="T146" id="Seg_2690" s="T145">np:O</ta>
            <ta e="T148" id="Seg_2691" s="T147">0.3.h:S v:pred</ta>
            <ta e="T153" id="Seg_2692" s="T152">np:S</ta>
            <ta e="T154" id="Seg_2693" s="T153">adj:pred</ta>
            <ta e="T155" id="Seg_2694" s="T154">cop</ta>
            <ta e="T160" id="Seg_2695" s="T157">s:temp</ta>
            <ta e="T161" id="Seg_2696" s="T160">np.h:S</ta>
            <ta e="T162" id="Seg_2697" s="T161">v:pred</ta>
            <ta e="T164" id="Seg_2698" s="T163">pro.h:O</ta>
            <ta e="T168" id="Seg_2699" s="T167">0.3.h:S v:pred</ta>
            <ta e="T170" id="Seg_2700" s="T169">np:S</ta>
            <ta e="T173" id="Seg_2701" s="T172">n:pred</ta>
            <ta e="T174" id="Seg_2702" s="T173">cop</ta>
            <ta e="T175" id="Seg_2703" s="T174">0.3.h:S v:pred</ta>
            <ta e="T176" id="Seg_2704" s="T175">pro.h:S</ta>
            <ta e="T180" id="Seg_2705" s="T177">s:adv</ta>
            <ta e="T182" id="Seg_2706" s="T180">adj:pred</ta>
            <ta e="T183" id="Seg_2707" s="T182">cop</ta>
            <ta e="T184" id="Seg_2708" s="T183">0.3.h:S v:pred</ta>
            <ta e="T187" id="Seg_2709" s="T186">np:S</ta>
            <ta e="T189" id="Seg_2710" s="T188">np:O</ta>
            <ta e="T191" id="Seg_2711" s="T189">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST">
            <ta e="T3" id="Seg_2712" s="T2">new</ta>
            <ta e="T6" id="Seg_2713" s="T5">giv-active</ta>
            <ta e="T7" id="Seg_2714" s="T6">quot-th</ta>
            <ta e="T8" id="Seg_2715" s="T7">new-Q</ta>
            <ta e="T10" id="Seg_2716" s="T9">new-Q</ta>
            <ta e="T14" id="Seg_2717" s="T13">accs-aggr-Q</ta>
            <ta e="T17" id="Seg_2718" s="T16">0.quot-th</ta>
            <ta e="T18" id="Seg_2719" s="T17">accs-inf</ta>
            <ta e="T19" id="Seg_2720" s="T18">0.quot-sp</ta>
            <ta e="T20" id="Seg_2721" s="T19">giv-inactive-Q</ta>
            <ta e="T23" id="Seg_2722" s="T22">giv-inactive-Q</ta>
            <ta e="T25" id="Seg_2723" s="T24">giv-inactive-Q</ta>
            <ta e="T26" id="Seg_2724" s="T25">accs-inf-Q</ta>
            <ta e="T29" id="Seg_2725" s="T28">giv-active-Q</ta>
            <ta e="T32" id="Seg_2726" s="T31">accs-aggr-Q</ta>
            <ta e="T34" id="Seg_2727" s="T32">0.giv-active-Q</ta>
            <ta e="T35" id="Seg_2728" s="T34">giv-active</ta>
            <ta e="T38" id="Seg_2729" s="T37">accs-inf</ta>
            <ta e="T42" id="Seg_2730" s="T41">giv-inactive-Q</ta>
            <ta e="T44" id="Seg_2731" s="T43">0.quot-sp</ta>
            <ta e="T47" id="Seg_2732" s="T46">0.quot-sp</ta>
            <ta e="T49" id="Seg_2733" s="T48">0.giv-inactive</ta>
            <ta e="T52" id="Seg_2734" s="T51">0.giv-inactive-Q</ta>
            <ta e="T53" id="Seg_2735" s="T52">0.quot-sp</ta>
            <ta e="T54" id="Seg_2736" s="T53">0.giv-inactive-Q</ta>
            <ta e="T58" id="Seg_2737" s="T57">0.giv-active-Q</ta>
            <ta e="T59" id="Seg_2738" s="T58">0.quot-sp</ta>
            <ta e="T61" id="Seg_2739" s="T60">accs-sit</ta>
            <ta e="T62" id="Seg_2740" s="T61">quot-sp</ta>
            <ta e="T64" id="Seg_2741" s="T63">giv-inactive</ta>
            <ta e="T65" id="Seg_2742" s="T64">accs-inf</ta>
            <ta e="T66" id="Seg_2743" s="T65">0.giv-inactive-Q</ta>
            <ta e="T67" id="Seg_2744" s="T66">giv-active-Q</ta>
            <ta e="T68" id="Seg_2745" s="T67">new-Q</ta>
            <ta e="T71" id="Seg_2746" s="T70">0.quot-sp</ta>
            <ta e="T72" id="Seg_2747" s="T71">giv-inactive</ta>
            <ta e="T74" id="Seg_2748" s="T73">accs-inf</ta>
            <ta e="T80" id="Seg_2749" s="T79">0.giv-active</ta>
            <ta e="T81" id="Seg_2750" s="T80">giv-inactive</ta>
            <ta e="T82" id="Seg_2751" s="T81">giv-inactive</ta>
            <ta e="T85" id="Seg_2752" s="T84">giv-active</ta>
            <ta e="T87" id="Seg_2753" s="T86">new-Q</ta>
            <ta e="T88" id="Seg_2754" s="T87">0.quot-sp</ta>
            <ta e="T89" id="Seg_2755" s="T88">giv-inactive</ta>
            <ta e="T90" id="Seg_2756" s="T89">giv-inactive</ta>
            <ta e="T96" id="Seg_2757" s="T95">giv-inactive</ta>
            <ta e="T98" id="Seg_2758" s="T97">giv-inactive</ta>
            <ta e="T101" id="Seg_2759" s="T100">giv-inactive</ta>
            <ta e="T103" id="Seg_2760" s="T102">0.giv-active</ta>
            <ta e="T104" id="Seg_2761" s="T103">new</ta>
            <ta e="T106" id="Seg_2762" s="T104">0.giv-active</ta>
            <ta e="T107" id="Seg_2763" s="T106">giv-inactive</ta>
            <ta e="T108" id="Seg_2764" s="T107">accs-inf</ta>
            <ta e="T109" id="Seg_2765" s="T108">accs-inf</ta>
            <ta e="T111" id="Seg_2766" s="T110">0.giv-inactive</ta>
            <ta e="T113" id="Seg_2767" s="T112">new</ta>
            <ta e="T115" id="Seg_2768" s="T114">giv-active</ta>
            <ta e="T116" id="Seg_2769" s="T115">giv-active</ta>
            <ta e="T117" id="Seg_2770" s="T116">giv-active</ta>
            <ta e="T123" id="Seg_2771" s="T122">giv-inactive</ta>
            <ta e="T124" id="Seg_2772" s="T123">new</ta>
            <ta e="T125" id="Seg_2773" s="T124">0.giv-inactive</ta>
            <ta e="T127" id="Seg_2774" s="T126">accs-inf</ta>
            <ta e="T129" id="Seg_2775" s="T128">0.giv-active</ta>
            <ta e="T130" id="Seg_2776" s="T129">0.giv-active</ta>
            <ta e="T132" id="Seg_2777" s="T131">giv-inactive-Q</ta>
            <ta e="T133" id="Seg_2778" s="T132">0.giv-inactive-Q</ta>
            <ta e="T134" id="Seg_2779" s="T133">giv-active-Q</ta>
            <ta e="T137" id="Seg_2780" s="T136">giv-active-Q</ta>
            <ta e="T140" id="Seg_2781" s="T139">0.quot-sp</ta>
            <ta e="T141" id="Seg_2782" s="T140">giv-active</ta>
            <ta e="T142" id="Seg_2783" s="T141">giv-active</ta>
            <ta e="T143" id="Seg_2784" s="T142">giv-inactive</ta>
            <ta e="T145" id="Seg_2785" s="T144">accs-inf</ta>
            <ta e="T146" id="Seg_2786" s="T145">accs-inf</ta>
            <ta e="T148" id="Seg_2787" s="T147">0.giv-active</ta>
            <ta e="T151" id="Seg_2788" s="T150">giv-active</ta>
            <ta e="T152" id="Seg_2789" s="T151">giv-inactive</ta>
            <ta e="T153" id="Seg_2790" s="T152">giv-active</ta>
            <ta e="T154" id="Seg_2791" s="T153">new</ta>
            <ta e="T156" id="Seg_2792" s="T155">giv-active</ta>
            <ta e="T157" id="Seg_2793" s="T156">giv-active</ta>
            <ta e="T161" id="Seg_2794" s="T160">accs-gen</ta>
            <ta e="T164" id="Seg_2795" s="T163">giv-active</ta>
            <ta e="T167" id="Seg_2796" s="T166">accs-inf-Q</ta>
            <ta e="T168" id="Seg_2797" s="T167">0.quot-sp</ta>
            <ta e="T169" id="Seg_2798" s="T168">giv-inactive-Q</ta>
            <ta e="T170" id="Seg_2799" s="T169">giv-active-Q</ta>
            <ta e="T171" id="Seg_2800" s="T170">accs-gen-Q</ta>
            <ta e="T173" id="Seg_2801" s="T172">accs-gen-Q</ta>
            <ta e="T175" id="Seg_2802" s="T174">0.quot-sp</ta>
            <ta e="T176" id="Seg_2803" s="T175">giv-inactive-Q</ta>
            <ta e="T179" id="Seg_2804" s="T177">new-Q</ta>
            <ta e="T184" id="Seg_2805" s="T183">0.quot-sp</ta>
            <ta e="T187" id="Seg_2806" s="T186">accs-inf</ta>
            <ta e="T188" id="Seg_2807" s="T187">new</ta>
            <ta e="T189" id="Seg_2808" s="T188">accs-inf</ta>
         </annotation>
         <annotation name="Top" tierref="Top">
            <ta e="T1" id="Seg_2809" s="T0">top.int.concr</ta>
            <ta e="T6" id="Seg_2810" s="T5">top.int.concr</ta>
            <ta e="T19" id="Seg_2811" s="T18">0.top.int.concr</ta>
            <ta e="T20" id="Seg_2812" s="T19">top.int.concr</ta>
            <ta e="T35" id="Seg_2813" s="T34">top.int.concr</ta>
            <ta e="T38" id="Seg_2814" s="T37">top.int.concr</ta>
            <ta e="T42" id="Seg_2815" s="T41">top.int.concr</ta>
            <ta e="T49" id="Seg_2816" s="T48">0.top.int.concr</ta>
            <ta e="T54" id="Seg_2817" s="T53">top.int.concr</ta>
            <ta e="T61" id="Seg_2818" s="T60">top.int.concr</ta>
            <ta e="T66" id="Seg_2819" s="T65">top.int.concr</ta>
            <ta e="T72" id="Seg_2820" s="T71">top.int.concr</ta>
            <ta e="T80" id="Seg_2821" s="T77">top.int.concr</ta>
            <ta e="T84" id="Seg_2822" s="T83">top.int.concr</ta>
            <ta e="T89" id="Seg_2823" s="T88">top.int.concr</ta>
            <ta e="T96" id="Seg_2824" s="T95">top.int.concr</ta>
            <ta e="T103" id="Seg_2825" s="T102">0.top.int.concr</ta>
            <ta e="T106" id="Seg_2826" s="T104">0.top.int.concr</ta>
            <ta e="T111" id="Seg_2827" s="T110">0.top.int.concr</ta>
            <ta e="T117" id="Seg_2828" s="T116">top.int.concr</ta>
            <ta e="T119" id="Seg_2829" s="T118">top.int.concr</ta>
            <ta e="T129" id="Seg_2830" s="T128">0.top.int.concr</ta>
            <ta e="T130" id="Seg_2831" s="T129">0.top.int.concr</ta>
            <ta e="T133" id="Seg_2832" s="T131">top.int.concr</ta>
            <ta e="T144" id="Seg_2833" s="T140">top.int.concr</ta>
            <ta e="T153" id="Seg_2834" s="T152">top.int.concr</ta>
            <ta e="T160" id="Seg_2835" s="T157">top.int.concr</ta>
            <ta e="T170" id="Seg_2836" s="T169">top.int.concr</ta>
            <ta e="T176" id="Seg_2837" s="T175">top.int.concr.contr.</ta>
            <ta e="T187" id="Seg_2838" s="T186">top.int.concr</ta>
         </annotation>
         <annotation name="Foc" tierref="Foc">
            <ta e="T4" id="Seg_2839" s="T0">foc.wid</ta>
            <ta e="T7" id="Seg_2840" s="T6">foc.int</ta>
            <ta e="T13" id="Seg_2841" s="T7">foc.wid</ta>
            <ta e="T19" id="Seg_2842" s="T17">foc.int</ta>
            <ta e="T21" id="Seg_2843" s="T20">foc.nar</ta>
            <ta e="T27" id="Seg_2844" s="T23">foc.int</ta>
            <ta e="T34" id="Seg_2845" s="T28">foc.int</ta>
            <ta e="T36" id="Seg_2846" s="T35">foc.int</ta>
            <ta e="T39" id="Seg_2847" s="T38">foc.int</ta>
            <ta e="T43" id="Seg_2848" s="T42">foc.nar</ta>
            <ta e="T45" id="Seg_2849" s="T44">foc.nar</ta>
            <ta e="T49" id="Seg_2850" s="T46">foc.int</ta>
            <ta e="T52" id="Seg_2851" s="T51">foc.int</ta>
            <ta e="T56" id="Seg_2852" s="T54">foc.int</ta>
            <ta e="T58" id="Seg_2853" s="T56">foc.int</ta>
            <ta e="T65" id="Seg_2854" s="T59">foc.wid</ta>
            <ta e="T70" id="Seg_2855" s="T66">foc.int</ta>
            <ta e="T77" id="Seg_2856" s="T72">foc.int</ta>
            <ta e="T83" id="Seg_2857" s="T80">foc.wid</ta>
            <ta e="T86" id="Seg_2858" s="T83">foc.int</ta>
            <ta e="T87" id="Seg_2859" s="T86">foc.nar</ta>
            <ta e="T91" id="Seg_2860" s="T89">foc.int</ta>
            <ta e="T99" id="Seg_2861" s="T97">foc.int</ta>
            <ta e="T101" id="Seg_2862" s="T100">foc.nar</ta>
            <ta e="T106" id="Seg_2863" s="T103">foc.int</ta>
            <ta e="T110" id="Seg_2864" s="T107">foc.nar</ta>
            <ta e="T118" id="Seg_2865" s="T117">foc.int</ta>
            <ta e="T125" id="Seg_2866" s="T119">foc.int</ta>
            <ta e="T127" id="Seg_2867" s="T125">foc.nar</ta>
            <ta e="T130" id="Seg_2868" s="T129">foc.nar</ta>
            <ta e="T139" id="Seg_2869" s="T133">foc.wid</ta>
            <ta e="T147" id="Seg_2870" s="T144">foc.nar</ta>
            <ta e="T152" id="Seg_2871" s="T148">foc.nar</ta>
            <ta e="T164" id="Seg_2872" s="T160">foc.wid</ta>
            <ta e="T174" id="Seg_2873" s="T172">foc.int</ta>
            <ta e="T183" id="Seg_2874" s="T177">foc.int</ta>
            <ta e="T191" id="Seg_2875" s="T184">foc.wid</ta>
         </annotation>
         <annotation name="BOR" tierref="TIE0" />
         <annotation name="BOR-Phon" tierref="TIE2" />
         <annotation name="BOR-Morph" tierref="TIE3" />
         <annotation name="CS" tierref="TIE4" />
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_2876" s="T0">A long time ago there lived one man.</ta>
            <ta e="T7" id="Seg_2877" s="T4">This man thought: </ta>
            <ta e="T17" id="Seg_2878" s="T7">"I will kill all polar foxes and all hares, I will not let anyone of them alive", he said</ta>
            <ta e="T19" id="Seg_2879" s="T17">He told his son: </ta>
            <ta e="T27" id="Seg_2880" s="T19">"I will sham sick, go and bring the hare shaman to me.</ta>
            <ta e="T34" id="Seg_2881" s="T27">Then chase all the hares to him as his assistants."</ta>
            <ta e="T39" id="Seg_2882" s="T34">The boy went and [saw] one hare sitting.</ta>
            <ta e="T44" id="Seg_2883" s="T39">"Well well, where is your shaman?", he asked.</ta>
            <ta e="T49" id="Seg_2884" s="T44">"There he is", he said showing to him.</ta>
            <ta e="T50" id="Seg_2885" s="T49">The boy went to him.</ta>
            <ta e="T59" id="Seg_2886" s="T50">"Well, come and help us", he said, "my father is dying. Try to help him", he said.</ta>
            <ta e="T65" id="Seg_2887" s="T59">Meanwhile the man who was staying [at home] says to his old woman:</ta>
            <ta e="T71" id="Seg_2888" s="T65">"When they all gather, close up the door", he said.</ta>
            <ta e="T77" id="Seg_2889" s="T71">The shaman comes and brings a lot of hares as assistants.</ta>
            <ta e="T86" id="Seg_2890" s="T77">As he brought them and started shamanizing, the old woman locked up the door, the one shamed sick [said]: </ta>
            <ta e="T88" id="Seg_2891" s="T86">"The hook!", he said.</ta>
            <ta e="T91" id="Seg_2892" s="T88">The old woman gave him the hook.</ta>
            <ta e="T100" id="Seg_2893" s="T91">Then the old man jumped on his feet and killed the hares.</ta>
            <ta e="T111" id="Seg_2894" s="T100">He also wanted to beat the shaman, but [the last one] escaped through the chimney, so that the hook hit only the tips of his ears.</ta>
            <ta e="T118" id="Seg_2895" s="T111">From the soot on the hook the tips of his ears turned black.</ta>
            <ta e="T125" id="Seg_2896" s="T118">After that the old man shamed sick again, called the shaman of polar foxes.</ta>
            <ta e="T131" id="Seg_2897" s="T125">He didn't dare to call many polar foxes, they are toothy after all. </ta>
            <ta e="T140" id="Seg_2898" s="T131">"If I kill their shaman, they will die without their protector on their own", he said.</ta>
            <ta e="T148" id="Seg_2899" s="T140">As he beat the shaman of polar foxes with the hook, it hit only the tip of his tail.</ta>
            <ta e="T157" id="Seg_2900" s="T148">Because of the hook's soot the tail of the polar fox has a [black] tuft on it's tip.</ta>
            <ta e="T164" id="Seg_2901" s="T157">After all this happened God met that man.</ta>
            <ta e="T175" id="Seg_2902" s="T164">"For this commited sin", he said, "your offence will be parable, as in olonxo", he said.</ta>
            <ta e="T184" id="Seg_2903" s="T175">"And you yourself be a jay bird and eat only rotten [meat]", he said.</ta>
            <ta e="T191" id="Seg_2904" s="T184">That is why the jay eats bait from the deadfall traps [for polar foxes].</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_2905" s="T0">Vor langer Zeit lebte ein Mensch.</ta>
            <ta e="T7" id="Seg_2906" s="T4">Dieser Mensch dachte: </ta>
            <ta e="T17" id="Seg_2907" s="T7">"Ich bringe alle Polarfüchse und Hasen um, ich lasse keinen übrig", sagte er.</ta>
            <ta e="T19" id="Seg_2908" s="T17">Er sprach zu seinem Sohn: </ta>
            <ta e="T27" id="Seg_2909" s="T19">"Ich tue so, als ob ich krank bin, geh du und hol den Schamanen der Hasen.</ta>
            <ta e="T34" id="Seg_2910" s="T27">Dann jag alle Hasen zu ihm als seine Helfer."</ta>
            <ta e="T39" id="Seg_2911" s="T34">Der Junge ging [und sah], dass [dort] ein Hase sitzt.</ta>
            <ta e="T44" id="Seg_2912" s="T39">"Hey, wo ist euer Schamane?", sagt er.</ta>
            <ta e="T49" id="Seg_2913" s="T44">"Dort ist er", sagt er und zeigt [es].</ta>
            <ta e="T50" id="Seg_2914" s="T49">Er ging [zu jenem].</ta>
            <ta e="T59" id="Seg_2915" s="T50">"Nun, komm zu Hilfe", sagt er, "mein Vater wurde gerufen zu sterben, versuch zu helfen", sagt er.</ta>
            <ta e="T65" id="Seg_2916" s="T59">Zu der Zeit sagt der [zuhause] gebliebene Mensch zu seiner Alten:</ta>
            <ta e="T71" id="Seg_2917" s="T65">"Wenn sie sich versammeln, dann versperr die Tür", sagt er.</ta>
            <ta e="T77" id="Seg_2918" s="T71">Der Schamane kommt und bringt sehr viele Hasen als seine Helfer.</ta>
            <ta e="T86" id="Seg_2919" s="T77">Als er sie brachte und anfing zu schamanisieren, blockierte die Alte die Tür, danach [sagte] der krank gewordene: </ta>
            <ta e="T88" id="Seg_2920" s="T86">"Den Haken!", sagte er.</ta>
            <ta e="T91" id="Seg_2921" s="T88">Die Alte gab den Haken.</ta>
            <ta e="T100" id="Seg_2922" s="T91">Der alte Mann sprang auf und fing an die Hasen zu töten.</ta>
            <ta e="T111" id="Seg_2923" s="T100">Er wollte auch den Schamanen schlagen, [jener] entkam durch das Rauchloch, mit dem Haken traf er nur die Spitze der Ohren.</ta>
            <ta e="T118" id="Seg_2924" s="T111">Vom Ruß beschmutzt sind die Spitzen der Ohren des Hasen schwarz.</ta>
            <ta e="T125" id="Seg_2925" s="T118">Danach tat er [der Alte] wieder krank und rief den Schamanen der Polarfüchse.</ta>
            <ta e="T131" id="Seg_2926" s="T125">Viele Polarfüchse zu rufen wagte er nicht, sie haben schließlich Zähne.</ta>
            <ta e="T140" id="Seg_2927" s="T131">"Wenn ich ihren Schamanen töte, dann sterben sie ohne ihren Beschützer von selbst", sagte er.</ta>
            <ta e="T148" id="Seg_2928" s="T140">Als er mit dem Haken nach dem Schamanen der Polarfüchse schlug, er traf nur die Spitze seines Schwanzes.</ta>
            <ta e="T157" id="Seg_2929" s="T148">Deshalb hat der Fuchs vom Ruß des Hakens einen [schwarzen] Puschel an der Schwanzspitze.</ta>
            <ta e="T164" id="Seg_2930" s="T157">Nachdem das alles passiert war, traf Gott diesen Menschen.</ta>
            <ta e="T175" id="Seg_2931" s="T164">"Für diese begangene Sünde", sagte er, "soll dein Verstoß wie das Olonxo ein Gleichnis werden", sagte er.</ta>
            <ta e="T184" id="Seg_2932" s="T175">"Aber du selbst werde zu einem Eichelhäher und ernähre dich von faulendem [Fleisch]", sagte er.</ta>
            <ta e="T191" id="Seg_2933" s="T184">Deshalb frisst der Eichelhäher den Köder aus der Totfalle [für Polarfüchse].</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_2934" s="T0">Жил в старину один человек.</ta>
            <ta e="T7" id="Seg_2935" s="T4">Этот человек задумал: </ta>
            <ta e="T17" id="Seg_2936" s="T7">"Всех песцов, всех зайцев перебью, ни одного не оставлю", сказал.</ta>
            <ta e="T19" id="Seg_2937" s="T17">Сыну сказал: </ta>
            <ta e="T27" id="Seg_2938" s="T19">"Я притворюсь больным, а ты пойди приведи заячьего шамана.</ta>
            <ta e="T34" id="Seg_2939" s="T27">А к нему в качестве дьячков пригони всех зайцев".</ta>
            <ta e="T39" id="Seg_2940" s="T34">Парень пошел, [видит:] один заяц сидит.</ta>
            <ta e="T44" id="Seg_2941" s="T39">"Ну-ка, где ваш шаман?", говорит.</ta>
            <ta e="T49" id="Seg_2942" s="T44">"Там он," говорит заяц и показывает.</ta>
            <ta e="T50" id="Seg_2943" s="T49">Идёт [к нему].</ta>
            <ta e="T59" id="Seg_2944" s="T50">"Ну, выручай," говорит, "отец умирает, зовет, попробуй помочь," говорит.</ta>
            <ta e="T65" id="Seg_2945" s="T59">В это время оставшийся [дома] человек говорит своей старухе:</ta>
            <ta e="T71" id="Seg_2946" s="T65">"Как соберутся, ты дверь заложи," говорит.</ta>
            <ta e="T77" id="Seg_2947" s="T71">Приходит шаман, с собой приводит великое множество зайцев в качестве помощников.</ta>
            <ta e="T86" id="Seg_2948" s="T77">Когда он, приведя их, начал камлать, старуха заложила дверь, а притворившийся больным [сказал]: </ta>
            <ta e="T88" id="Seg_2949" s="T86">"Крюк подай!", сказал.</ta>
            <ta e="T91" id="Seg_2950" s="T88">Старуха подала крюк.</ta>
            <ta e="T100" id="Seg_2951" s="T91">Тут же вскочив, старик начал убивать зайцев.</ta>
            <ta e="T111" id="Seg_2952" s="T100">Хотел ударить и шамана, но тот, выскочив в дымоход, убежал, крюком только кончики ушей задело.</ta>
            <ta e="T118" id="Seg_2953" s="T111">От копоти на крюке кончики ушей у зайца черные.</ta>
            <ta e="T125" id="Seg_2954" s="T118">После этого [старик], опять притворившись больным, шамана песцов позвал.</ta>
            <ta e="T131" id="Seg_2955" s="T125">Много песцов позвать побоялся: мол, все зубастые.</ta>
            <ta e="T140" id="Seg_2956" s="T131">"Если убью их шамана, они без защитника сами пропадут", сказал.</ta>
            <ta e="T148" id="Seg_2957" s="T140">Ударив шамана песцов крюком, только кончик хвоста задел.</ta>
            <ta e="T157" id="Seg_2958" s="T148">Потому от копоти на крюке у песца на кончике хвоста появилась [черная] кисточка.</ta>
            <ta e="T164" id="Seg_2959" s="T157">После того как все это случилось, бог встретил того человека.</ta>
            <ta e="T175" id="Seg_2960" s="T164">"За содеянный грех," сказал, "пусть твой проступок станет притчей, как [в] олонгко", — сказал.</ta>
            <ta e="T184" id="Seg_2961" s="T175">"А сам ты, превратившись в сойку, только тухлым [мясом] питайся", сказал.</ta>
            <ta e="T191" id="Seg_2962" s="T184">Поэтому сойка поедает приманку из пастей [для песцов].</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T4" id="Seg_2963" s="T0">Жил в старину один человек.</ta>
            <ta e="T7" id="Seg_2964" s="T4">Этот человек задумал: </ta>
            <ta e="T17" id="Seg_2965" s="T7">"Всех песцов, всех зайцев перебью, ни одного не оставлю".</ta>
            <ta e="T19" id="Seg_2966" s="T17">Сыну сказал: </ta>
            <ta e="T27" id="Seg_2967" s="T19">"Я притворюсь больным, а ты пойди приведи заячьего шамана.</ta>
            <ta e="T34" id="Seg_2968" s="T27">А к нему дьячками пригони всех зайцев".</ta>
            <ta e="T39" id="Seg_2969" s="T34">Парень пошел, [видит:] один заяц сидит.</ta>
            <ta e="T44" id="Seg_2970" s="T39">— Ну-ка, где ваш шаман? — спрашивает.</ta>
            <ta e="T49" id="Seg_2971" s="T44">— Там он, — говорит, указывая, [заяц].</ta>
            <ta e="T50" id="Seg_2972" s="T49">Придя [к тому]:</ta>
            <ta e="T59" id="Seg_2973" s="T50">— Ну, выручай, отец умирает, зовет, попробуй помочь, — говорит.</ta>
            <ta e="T65" id="Seg_2974" s="T59">В это время оставшийся [дома] старик говорит своей старухе:</ta>
            <ta e="T71" id="Seg_2975" s="T65">— Как соберутся, ты заложи дверь.</ta>
            <ta e="T77" id="Seg_2976" s="T71">Приходит шаман, с собой приводит великое множество зайцев, дьячков [-помощников].</ta>
            <ta e="T86" id="Seg_2977" s="T77">Когда, [их] приведя, начал камлать, старуха заложила дверь, а притворившийся больным </ta>
            <ta e="T88" id="Seg_2978" s="T86">"Олдоон подай!" — говорит.</ta>
            <ta e="T91" id="Seg_2979" s="T88">Старуха подала олдоон.</ta>
            <ta e="T100" id="Seg_2980" s="T91">Тут же вскочив, старик начал убивать зайцев.</ta>
            <ta e="T111" id="Seg_2981" s="T100">Хотел ударить и шамана, но [тот], выскочив в дымоход, убежал, олдоон только кончики ушей задел.</ta>
            <ta e="T118" id="Seg_2982" s="T111">От копоти [на олдооне] у зайца кончики ушей черные.</ta>
            <ta e="T125" id="Seg_2983" s="T118">После этого, опять притворившись больным, [старик] шамана песцов велел позвать.</ta>
            <ta e="T131" id="Seg_2984" s="T125">Много песцов позвать побоялся: мол, все зубастые.</ta>
            <ta e="T140" id="Seg_2985" s="T131">"Если убью их шамана, они без защитника сами пропадут", — задумал.</ta>
            <ta e="T148" id="Seg_2986" s="T140">Ударив шамана песцов олдооном, только кончик хвоста задел.</ta>
            <ta e="T157" id="Seg_2987" s="T148">Потому от копоти на олдооне на кончике хвоста у песца черная кисточка.</ta>
            <ta e="T164" id="Seg_2988" s="T157">После того как все это случилось, бог встретил того человека.</ta>
            <ta e="T175" id="Seg_2989" s="T164">"За содеянный грех пусть твой проступок станет притчей, как в олонгко, — сказал.</ta>
            <ta e="T184" id="Seg_2990" s="T175">— А сам ты, превратившись в сойку-птицу, только [тухлым] мясом питайся", — сказал.</ta>
            <ta e="T191" id="Seg_2991" s="T184">Поэтому сойка поедает приманку из пастей [для песцов].</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T34" id="Seg_2992" s="T27">[AAV]: Rus. "дьячок" denotes usually a (layman) reader who assists the priest at a liturgy.</ta>
            <ta e="T88" id="Seg_2993" s="T86">[AAV]: Ewenki "oldoon" denotes a hook used for hanging up a kettle or a cauldron above the fireplace.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg"
                          name="gg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top"
                          name="Top"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc"
                          name="Foc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
