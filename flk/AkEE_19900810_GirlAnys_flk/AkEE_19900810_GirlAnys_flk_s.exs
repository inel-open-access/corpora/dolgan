<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDFD8EAED4-0840-2D1C-B9D7-33E5EB551D26">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>AkEE_19900810_GirlAnys_flk</transcription-name>
         <referenced-file url="AkEE_19900810_GirlAnys_flk.wav" />
         <referenced-file url="AkEE_19900810_GirlAnys_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\flk\AkEE_19900810_GirlAnys_flk\AkEE_19900810_GirlAnys_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">256</ud-information>
            <ud-information attribute-name="# HIAT:w">196</ud-information>
            <ud-information attribute-name="# e">196</ud-information>
            <ud-information attribute-name="# HIAT:u">30</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="AkEE">
            <abbreviation>AkEE</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" time="0.0" type="appl" />
         <tli id="T2" time="1.275" type="appl" />
         <tli id="T3" time="2.55" type="appl" />
         <tli id="T4" time="3.343083333333333" type="appl" />
         <tli id="T5" time="4.136166666666666" type="appl" />
         <tli id="T6" time="4.92925" type="appl" />
         <tli id="T7" time="5.722333333333333" type="appl" />
         <tli id="T8" time="6.515416666666666" type="appl" />
         <tli id="T9" time="7.3084999999999996" type="appl" />
         <tli id="T10" time="8.101583333333332" type="appl" />
         <tli id="T11" time="8.894666666666666" type="appl" />
         <tli id="T12" time="9.68775" type="appl" />
         <tli id="T13" time="10.480833333333333" type="appl" />
         <tli id="T14" time="11.273916666666665" type="appl" />
         <tli id="T15" time="12.067" type="appl" />
         <tli id="T16" time="12.8716" type="appl" />
         <tli id="T17" time="13.6762" type="appl" />
         <tli id="T18" time="14.4808" type="appl" />
         <tli id="T19" time="15.2854" type="appl" />
         <tli id="T20" time="16.28333155466653" />
         <tli id="T21" time="16.696625" type="appl" />
         <tli id="T22" time="17.30325" type="appl" />
         <tli id="T23" time="17.909875" type="appl" />
         <tli id="T24" time="18.5165" type="appl" />
         <tli id="T25" time="19.123125" type="appl" />
         <tli id="T26" time="19.729750000000003" type="appl" />
         <tli id="T27" time="20.336375" type="appl" />
         <tli id="T28" time="21.12966424524205" />
         <tli id="T29" time="21.71516666666667" type="appl" />
         <tli id="T30" time="22.487333333333336" type="appl" />
         <tli id="T31" time="23.259500000000003" type="appl" />
         <tli id="T32" time="24.031666666666666" type="appl" />
         <tli id="T33" time="24.803833333333333" type="appl" />
         <tli id="T34" time="25.709331256174323" />
         <tli id="T35" time="26.3195" type="appl" />
         <tli id="T36" time="27.063000000000002" type="appl" />
         <tli id="T37" time="27.8065" type="appl" />
         <tli id="T38" time="28.68333204434109" />
         <tli id="T39" time="29.208000000000002" type="appl" />
         <tli id="T40" time="29.866" type="appl" />
         <tli id="T41" time="30.524" type="appl" />
         <tli id="T42" time="31.182000000000002" type="appl" />
         <tli id="T43" time="31.84" type="appl" />
         <tli id="T44" time="32.498000000000005" type="appl" />
         <tli id="T45" time="33.156" type="appl" />
         <tli id="T46" time="33.814" type="appl" />
         <tli id="T47" time="34.88632461009941" />
         <tli id="T48" time="35.46666666666667" type="appl" />
         <tli id="T49" time="36.461333333333336" type="appl" />
         <tli id="T50" time="37.456" type="appl" />
         <tli id="T51" time="38.36716666666667" type="appl" />
         <tli id="T52" time="39.278333333333336" type="appl" />
         <tli id="T53" time="40.1895" type="appl" />
         <tli id="T54" time="41.10066666666667" type="appl" />
         <tli id="T55" time="42.011833333333335" type="appl" />
         <tli id="T56" time="42.923" type="appl" />
         <tli id="T57" time="43.62114285714286" type="appl" />
         <tli id="T58" time="44.31928571428572" type="appl" />
         <tli id="T59" time="45.017428571428574" type="appl" />
         <tli id="T60" time="45.71557142857143" type="appl" />
         <tli id="T61" time="46.413714285714285" type="appl" />
         <tli id="T62" time="47.11185714285715" type="appl" />
         <tli id="T63" time="47.796666775617595" />
         <tli id="T64" time="48.351" type="appl" />
         <tli id="T65" time="48.892" type="appl" />
         <tli id="T66" time="49.433" type="appl" />
         <tli id="T67" time="49.974000000000004" type="appl" />
         <tli id="T68" time="51.006166556826024" />
         <tli id="T69" time="51.239375" type="appl" />
         <tli id="T70" time="51.963750000000005" type="appl" />
         <tli id="T71" time="52.688125" type="appl" />
         <tli id="T72" time="53.4125" type="appl" />
         <tli id="T73" time="54.136875" type="appl" />
         <tli id="T74" time="54.86125" type="appl" />
         <tli id="T75" time="55.585625" type="appl" />
         <tli id="T76" time="56.583331925417696" />
         <tli id="T77" time="57.185500000000005" type="appl" />
         <tli id="T78" time="58.061" type="appl" />
         <tli id="T79" time="58.9365" type="appl" />
         <tli id="T80" time="59.812000000000005" type="appl" />
         <tli id="T81" time="60.6875" type="appl" />
         <tli id="T82" time="61.563" type="appl" />
         <tli id="T83" time="62.21971428571429" type="appl" />
         <tli id="T84" time="62.87642857142857" type="appl" />
         <tli id="T85" time="63.533142857142856" type="appl" />
         <tli id="T86" time="64.18985714285714" type="appl" />
         <tli id="T87" time="64.84657142857142" type="appl" />
         <tli id="T88" time="65.50328571428571" type="appl" />
         <tli id="T89" time="66.77267863457972" />
         <tli id="T90" time="67.06285714285714" type="appl" />
         <tli id="T91" time="67.96571428571428" type="appl" />
         <tli id="T92" time="68.86857142857143" type="appl" />
         <tli id="T93" time="69.77142857142857" type="appl" />
         <tli id="T94" time="70.67428571428572" type="appl" />
         <tli id="T95" time="71.57714285714286" type="appl" />
         <tli id="T96" time="72.52666518612584" />
         <tli id="T97" time="73.53066666666668" type="appl" />
         <tli id="T98" time="74.58133333333333" type="appl" />
         <tli id="T99" time="75.56533330898652" />
         <tli id="T100" time="76.37650000000001" type="appl" />
         <tli id="T101" time="77.12100000000001" type="appl" />
         <tli id="T102" time="77.8655" type="appl" />
         <tli id="T103" time="78.51666504858748" />
         <tli id="T104" time="79.34928571428571" type="appl" />
         <tli id="T105" time="80.08857142857143" type="appl" />
         <tli id="T106" time="80.82785714285714" type="appl" />
         <tli id="T107" time="81.56714285714285" type="appl" />
         <tli id="T108" time="82.30642857142857" type="appl" />
         <tli id="T109" time="83.04571428571428" type="appl" />
         <tli id="T110" time="83.785" type="appl" />
         <tli id="T111" time="84.52428571428571" type="appl" />
         <tli id="T112" time="85.26357142857142" type="appl" />
         <tli id="T113" time="86.00285714285714" type="appl" />
         <tli id="T114" time="86.74214285714285" type="appl" />
         <tli id="T115" time="87.48142857142857" type="appl" />
         <tli id="T116" time="88.22071428571428" type="appl" />
         <tli id="T117" time="89.14666498927649" />
         <tli id="T118" time="90.01079999999999" type="appl" />
         <tli id="T119" time="91.0616" type="appl" />
         <tli id="T120" time="92.1124" type="appl" />
         <tli id="T121" time="93.1632" type="appl" />
         <tli id="T122" time="94.214" type="appl" />
         <tli id="T123" time="95.2648" type="appl" />
         <tli id="T124" time="96.3156" type="appl" />
         <tli id="T125" time="97.3664" type="appl" />
         <tli id="T126" time="98.41720000000001" type="appl" />
         <tli id="T127" time="99.84133226465939" />
         <tli id="T128" time="100.1122" type="appl" />
         <tli id="T129" time="100.7564" type="appl" />
         <tli id="T130" time="101.4006" type="appl" />
         <tli id="T131" time="102.0448" type="appl" />
         <tli id="T132" time="102.689" type="appl" />
         <tli id="T133" time="103.27175" type="appl" />
         <tli id="T134" time="103.8545" type="appl" />
         <tli id="T135" time="104.43724999999999" type="appl" />
         <tli id="T136" time="105.15999756364909" />
         <tli id="T137" time="105.705" type="appl" />
         <tli id="T138" time="106.39" type="appl" />
         <tli id="T139" time="107.075" type="appl" />
         <tli id="T140" time="107.75999999999999" type="appl" />
         <tli id="T141" time="108.445" type="appl" />
         <tli id="T142" time="109.13" type="appl" />
         <tli id="T143" time="109.815" type="appl" />
         <tli id="T144" time="110.52666369407925" />
         <tli id="T145" time="111.23307692307692" type="appl" />
         <tli id="T146" time="111.96615384615384" type="appl" />
         <tli id="T147" time="112.69923076923077" type="appl" />
         <tli id="T148" time="113.43230769230769" type="appl" />
         <tli id="T149" time="114.16538461538461" type="appl" />
         <tli id="T150" time="114.89846153846153" type="appl" />
         <tli id="T151" time="115.63153846153847" type="appl" />
         <tli id="T152" time="116.36461538461539" type="appl" />
         <tli id="T153" time="117.09769230769231" type="appl" />
         <tli id="T154" time="117.83076923076923" type="appl" />
         <tli id="T155" time="118.56384615384616" type="appl" />
         <tli id="T156" time="119.29692307692308" type="appl" />
         <tli id="T157" time="120.03" type="appl" />
         <tli id="T158" time="120.7625" type="appl" />
         <tli id="T159" time="121.495" type="appl" />
         <tli id="T160" time="122.22749999999999" type="appl" />
         <tli id="T161" time="123.213336177654" />
         <tli id="T162" time="123.75399999999999" type="appl" />
         <tli id="T163" time="124.548" type="appl" />
         <tli id="T164" time="125.342" type="appl" />
         <tli id="T165" time="126.13600000000001" type="appl" />
         <tli id="T166" time="127.29666853686365" />
         <tli id="T167" time="128.22533333333334" type="appl" />
         <tli id="T168" time="129.52066666666667" type="appl" />
         <tli id="T169" time="130.816" type="appl" />
         <tli id="T170" time="132.11133333333333" type="appl" />
         <tli id="T171" time="133.40666666666667" type="appl" />
         <tli id="T172" time="134.60200055472183" />
         <tli id="T173" time="135.33816666666667" type="appl" />
         <tli id="T174" time="135.97433333333333" type="appl" />
         <tli id="T175" time="136.6105" type="appl" />
         <tli id="T176" time="137.24666666666667" type="appl" />
         <tli id="T177" time="137.88283333333334" type="appl" />
         <tli id="T178" time="138.4790068119362" />
         <tli id="T179" time="139.21777777777777" type="appl" />
         <tli id="T180" time="139.91655555555556" type="appl" />
         <tli id="T181" time="140.61533333333333" type="appl" />
         <tli id="T182" time="141.31411111111112" type="appl" />
         <tli id="T183" time="142.01288888888888" type="appl" />
         <tli id="T184" time="142.71166666666667" type="appl" />
         <tli id="T185" time="143.41044444444444" type="appl" />
         <tli id="T186" time="144.10922222222223" type="appl" />
         <tli id="T187" time="144.8413272425857" />
         <tli id="T188" time="145.71542857142856" type="appl" />
         <tli id="T189" time="146.62285714285713" type="appl" />
         <tli id="T190" time="147.5302857142857" type="appl" />
         <tli id="T191" time="148.4377142857143" type="appl" />
         <tli id="T192" time="149.34514285714286" type="appl" />
         <tli id="T193" time="150.25257142857143" type="appl" />
         <tli id="T194" time="151.16" type="appl" />
         <tli id="T195" time="152.30433333333332" type="appl" />
         <tli id="T196" time="153.44866666666667" type="appl" />
         <tli id="T197" time="154.593" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="AkEE"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T197" id="Seg_0" n="sc" s="T1">
               <ts e="T3" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Aːnɨs</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">kɨːs</ts>
                  <nts id="Seg_8" n="HIAT:ip">.</nts>
                  <nts id="Seg_9" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T15" id="Seg_11" n="HIAT:u" s="T3">
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">Bu</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">orto</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">hirge</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">tulaːjak</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">Aːnɨs</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_28" n="HIAT:w" s="T8">kɨːs</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_31" n="HIAT:w" s="T9">ogo</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_34" n="HIAT:w" s="T10">olorbuttaːk</ts>
                  <nts id="Seg_35" n="HIAT:ip">,</nts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_38" n="HIAT:w" s="T11">baːjdarga</ts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_41" n="HIAT:w" s="T12">kulut</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_44" n="HIAT:w" s="T13">bu͡ola</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_47" n="HIAT:w" s="T14">hɨldʼɨbɨt</ts>
                  <nts id="Seg_48" n="HIAT:ip">.</nts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_51" n="HIAT:u" s="T15">
                  <ts e="T16" id="Seg_53" n="HIAT:w" s="T15">Ontulara</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_56" n="HIAT:w" s="T16">bert</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_59" n="HIAT:w" s="T17">atagastɨːr</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_62" n="HIAT:w" s="T18">etiler</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_65" n="HIAT:w" s="T19">ebit</ts>
                  <nts id="Seg_66" n="HIAT:ip">.</nts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T28" id="Seg_69" n="HIAT:u" s="T20">
                  <ts e="T21" id="Seg_71" n="HIAT:w" s="T20">Kara</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_74" n="HIAT:w" s="T21">harsi͡erdattan</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_77" n="HIAT:w" s="T22">ki͡ehege</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_80" n="HIAT:w" s="T23">di͡eri</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_83" n="HIAT:w" s="T24">ikki</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_86" n="HIAT:w" s="T25">holuːrdaːk</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_89" n="HIAT:w" s="T26">uː</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_92" n="HIAT:w" s="T27">bahar</ts>
                  <nts id="Seg_93" n="HIAT:ip">.</nts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T34" id="Seg_96" n="HIAT:u" s="T28">
                  <ts e="T29" id="Seg_98" n="HIAT:w" s="T28">Hajɨn</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_101" n="HIAT:w" s="T29">u͡oktalanɨ</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_104" n="HIAT:w" s="T30">ülgeːn</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_107" n="HIAT:w" s="T31">baraːn</ts>
                  <nts id="Seg_108" n="HIAT:ip">,</nts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_111" n="HIAT:w" s="T32">hügen</ts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_114" n="HIAT:w" s="T33">egeler</ts>
                  <nts id="Seg_115" n="HIAT:ip">.</nts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T38" id="Seg_118" n="HIAT:u" s="T34">
                  <ts e="T35" id="Seg_120" n="HIAT:w" s="T34">Kɨhɨn</ts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_123" n="HIAT:w" s="T35">mastɨːr</ts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_126" n="HIAT:w" s="T36">onton</ts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_129" n="HIAT:w" s="T37">abɨratar</ts>
                  <nts id="Seg_130" n="HIAT:ip">.</nts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T47" id="Seg_133" n="HIAT:u" s="T38">
                  <ts e="T39" id="Seg_135" n="HIAT:w" s="T38">Kɨhɨn</ts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_138" n="HIAT:w" s="T39">daː</ts>
                  <nts id="Seg_139" n="HIAT:ip">,</nts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_142" n="HIAT:w" s="T40">hajɨn</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_145" n="HIAT:w" s="T41">daː</ts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_148" n="HIAT:w" s="T42">uː</ts>
                  <nts id="Seg_149" n="HIAT:ip">,</nts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_152" n="HIAT:w" s="T43">mas</ts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_155" n="HIAT:w" s="T44">tulujbat</ts>
                  <nts id="Seg_156" n="HIAT:ip">,</nts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_159" n="HIAT:w" s="T45">honno</ts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_162" n="HIAT:w" s="T46">baranar</ts>
                  <nts id="Seg_163" n="HIAT:ip">.</nts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T50" id="Seg_166" n="HIAT:u" s="T47">
                  <ts e="T48" id="Seg_168" n="HIAT:w" s="T47">Kɨhɨn</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_171" n="HIAT:w" s="T48">mu͡oraga</ts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_174" n="HIAT:w" s="T49">karaŋa</ts>
                  <nts id="Seg_175" n="HIAT:ip">.</nts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T56" id="Seg_178" n="HIAT:u" s="T50">
                  <ts e="T51" id="Seg_180" n="HIAT:w" s="T50">Kün</ts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_183" n="HIAT:w" s="T51">taksɨbat</ts>
                  <nts id="Seg_184" n="HIAT:ip">,</nts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_187" n="HIAT:w" s="T52">ɨjdɨŋ</ts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_190" n="HIAT:w" s="T53">ere</ts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_193" n="HIAT:w" s="T54">kaːrɨ</ts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_196" n="HIAT:w" s="T55">hɨrdatar</ts>
                  <nts id="Seg_197" n="HIAT:ip">.</nts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T63" id="Seg_200" n="HIAT:u" s="T56">
                  <ts e="T57" id="Seg_202" n="HIAT:w" s="T56">ɨj</ts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_205" n="HIAT:w" s="T57">kallaːŋŋa</ts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_208" n="HIAT:w" s="T58">buːs</ts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_211" n="HIAT:w" s="T59">kördük</ts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_214" n="HIAT:w" s="T60">tɨmnɨː</ts>
                  <nts id="Seg_215" n="HIAT:ip">,</nts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_218" n="HIAT:w" s="T61">hiri</ts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_221" n="HIAT:w" s="T62">itippet</ts>
                  <nts id="Seg_222" n="HIAT:ip">.</nts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T68" id="Seg_225" n="HIAT:u" s="T63">
                  <ts e="T64" id="Seg_227" n="HIAT:w" s="T63">Gini</ts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_230" n="HIAT:w" s="T64">hɨrga</ts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_233" n="HIAT:w" s="T65">dʼi͡eni</ts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_236" n="HIAT:w" s="T66">ere</ts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_239" n="HIAT:w" s="T67">hɨrdatar</ts>
                  <nts id="Seg_240" n="HIAT:ip">.</nts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T76" id="Seg_243" n="HIAT:u" s="T68">
                  <ts e="T69" id="Seg_245" n="HIAT:w" s="T68">Hɨrga</ts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_248" n="HIAT:w" s="T69">dʼi͡e</ts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_251" n="HIAT:w" s="T70">ihiger</ts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_254" n="HIAT:w" s="T71">künüs</ts>
                  <nts id="Seg_255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_257" n="HIAT:w" s="T72">ičiges</ts>
                  <nts id="Seg_258" n="HIAT:ip">,</nts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_261" n="HIAT:w" s="T73">ohok</ts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_264" n="HIAT:w" s="T74">ottular</ts>
                  <nts id="Seg_265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_267" n="HIAT:w" s="T75">bu͡olan</ts>
                  <nts id="Seg_268" n="HIAT:ip">.</nts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T82" id="Seg_271" n="HIAT:u" s="T76">
                  <ts e="T77" id="Seg_273" n="HIAT:w" s="T76">Hɨrga</ts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_276" n="HIAT:w" s="T77">dʼi͡e</ts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_279" n="HIAT:w" s="T78">öldüːne</ts>
                  <nts id="Seg_280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_282" n="HIAT:w" s="T79">taba</ts>
                  <nts id="Seg_283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_285" n="HIAT:w" s="T80">tiriːtinen</ts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_288" n="HIAT:w" s="T81">oŋohullaːččɨ</ts>
                  <nts id="Seg_289" n="HIAT:ip">.</nts>
                  <nts id="Seg_290" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T89" id="Seg_292" n="HIAT:u" s="T82">
                  <ts e="T83" id="Seg_294" n="HIAT:w" s="T82">Aːnɨs</ts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_297" n="HIAT:w" s="T83">ere</ts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_300" n="HIAT:w" s="T84">toŋor</ts>
                  <nts id="Seg_301" n="HIAT:ip">,</nts>
                  <nts id="Seg_302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_304" n="HIAT:w" s="T85">gini</ts>
                  <nts id="Seg_305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_307" n="HIAT:w" s="T86">hirge</ts>
                  <nts id="Seg_308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_310" n="HIAT:w" s="T87">taŋastɨːn</ts>
                  <nts id="Seg_311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_313" n="HIAT:w" s="T88">utujar</ts>
                  <nts id="Seg_314" n="HIAT:ip">.</nts>
                  <nts id="Seg_315" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T96" id="Seg_317" n="HIAT:u" s="T89">
                  <ts e="T90" id="Seg_319" n="HIAT:w" s="T89">Ki͡ehennen</ts>
                  <nts id="Seg_320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_322" n="HIAT:w" s="T90">Aːnɨs</ts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_325" n="HIAT:w" s="T91">holuːrdarɨn</ts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_328" n="HIAT:w" s="T92">ɨlan</ts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_331" n="HIAT:w" s="T93">baraːn</ts>
                  <nts id="Seg_332" n="HIAT:ip">,</nts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_335" n="HIAT:w" s="T94">ojbonugar</ts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_338" n="HIAT:w" s="T95">barda</ts>
                  <nts id="Seg_339" n="HIAT:ip">.</nts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T99" id="Seg_342" n="HIAT:u" s="T96">
                  <ts e="T97" id="Seg_344" n="HIAT:w" s="T96">Türgennik</ts>
                  <nts id="Seg_345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_347" n="HIAT:w" s="T97">kaːmtagɨna</ts>
                  <nts id="Seg_348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_350" n="HIAT:w" s="T98">toŋmot</ts>
                  <nts id="Seg_351" n="HIAT:ip">.</nts>
                  <nts id="Seg_352" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T103" id="Seg_354" n="HIAT:u" s="T99">
                  <ts e="T100" id="Seg_356" n="HIAT:w" s="T99">Ojboŋŋo</ts>
                  <nts id="Seg_357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_359" n="HIAT:w" s="T100">uː</ts>
                  <nts id="Seg_360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_362" n="HIAT:w" s="T101">bahaːrɨ</ts>
                  <nts id="Seg_363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_365" n="HIAT:w" s="T102">öŋöjdö</ts>
                  <nts id="Seg_366" n="HIAT:ip">.</nts>
                  <nts id="Seg_367" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T117" id="Seg_369" n="HIAT:u" s="T103">
                  <ts e="T104" id="Seg_371" n="HIAT:w" s="T103">Körbüte</ts>
                  <nts id="Seg_372" n="HIAT:ip">,</nts>
                  <nts id="Seg_373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_375" n="HIAT:w" s="T104">ɨj</ts>
                  <nts id="Seg_376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_378" n="HIAT:w" s="T105">külüːge</ts>
                  <nts id="Seg_379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_381" n="HIAT:w" s="T106">bert</ts>
                  <nts id="Seg_382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_384" n="HIAT:w" s="T107">hugaskaːn</ts>
                  <nts id="Seg_385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_387" n="HIAT:w" s="T108">köstör</ts>
                  <nts id="Seg_388" n="HIAT:ip">,</nts>
                  <nts id="Seg_389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_391" n="HIAT:w" s="T109">ojbonu</ts>
                  <nts id="Seg_392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_394" n="HIAT:w" s="T110">toloru</ts>
                  <nts id="Seg_395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_397" n="HIAT:w" s="T111">bu͡olla</ts>
                  <nts id="Seg_398" n="HIAT:ip">,</nts>
                  <nts id="Seg_399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_401" n="HIAT:w" s="T112">kihi</ts>
                  <nts id="Seg_402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_404" n="HIAT:w" s="T113">da</ts>
                  <nts id="Seg_405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_407" n="HIAT:w" s="T114">haŋatɨn</ts>
                  <nts id="Seg_408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_410" n="HIAT:w" s="T115">ister</ts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_413" n="HIAT:w" s="T116">kördük</ts>
                  <nts id="Seg_414" n="HIAT:ip">.</nts>
                  <nts id="Seg_415" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T127" id="Seg_417" n="HIAT:u" s="T117">
                  <ts e="T118" id="Seg_419" n="HIAT:w" s="T117">Aːnɨs</ts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_422" n="HIAT:w" s="T118">ɨjdɨŋanɨ</ts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_425" n="HIAT:w" s="T119">körön</ts>
                  <nts id="Seg_426" n="HIAT:ip">,</nts>
                  <nts id="Seg_427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_429" n="HIAT:w" s="T120">is</ts>
                  <nts id="Seg_430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_432" n="HIAT:w" s="T121">ihitten</ts>
                  <nts id="Seg_433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_435" n="HIAT:w" s="T122">tü͡öre</ts>
                  <nts id="Seg_436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_438" n="HIAT:w" s="T123">ɨtaːta</ts>
                  <nts id="Seg_439" n="HIAT:ip">,</nts>
                  <nts id="Seg_440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_442" n="HIAT:w" s="T124">ɨjɨ</ts>
                  <nts id="Seg_443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_445" n="HIAT:w" s="T125">kɨtta</ts>
                  <nts id="Seg_446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_448" n="HIAT:w" s="T126">kepsete-kepsete</ts>
                  <nts id="Seg_449" n="HIAT:ip">.</nts>
                  <nts id="Seg_450" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T132" id="Seg_452" n="HIAT:u" s="T127">
                  <nts id="Seg_453" n="HIAT:ip">"</nts>
                  <ts e="T128" id="Seg_455" n="HIAT:w" s="T127">ɨttaːgar</ts>
                  <nts id="Seg_456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_458" n="HIAT:w" s="T128">da</ts>
                  <nts id="Seg_459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_461" n="HIAT:w" s="T129">kuhagannɨk</ts>
                  <nts id="Seg_462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_464" n="HIAT:w" s="T130">olorobun</ts>
                  <nts id="Seg_465" n="HIAT:ip">"</nts>
                  <nts id="Seg_466" n="HIAT:ip">,</nts>
                  <nts id="Seg_467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_469" n="HIAT:w" s="T131">diːr</ts>
                  <nts id="Seg_470" n="HIAT:ip">.</nts>
                  <nts id="Seg_471" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T136" id="Seg_473" n="HIAT:u" s="T132">
                  <ts e="T133" id="Seg_475" n="HIAT:w" s="T132">Kihi</ts>
                  <nts id="Seg_476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_478" n="HIAT:w" s="T133">barɨta</ts>
                  <nts id="Seg_479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_481" n="HIAT:w" s="T134">minigin</ts>
                  <nts id="Seg_482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_484" n="HIAT:w" s="T135">atagastɨːr</ts>
                  <nts id="Seg_485" n="HIAT:ip">.</nts>
                  <nts id="Seg_486" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T144" id="Seg_488" n="HIAT:u" s="T136">
                  <ts e="T137" id="Seg_490" n="HIAT:w" s="T136">Ajɨːlɨː</ts>
                  <nts id="Seg_491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_493" n="HIAT:w" s="T137">da</ts>
                  <nts id="Seg_494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_496" n="HIAT:w" s="T138">ahappattar</ts>
                  <nts id="Seg_497" n="HIAT:ip">,</nts>
                  <nts id="Seg_498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_500" n="HIAT:w" s="T139">ütü͡ö</ts>
                  <nts id="Seg_501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_503" n="HIAT:w" s="T140">taŋaha</ts>
                  <nts id="Seg_504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_506" n="HIAT:w" s="T141">hu͡okpun</ts>
                  <nts id="Seg_507" n="HIAT:ip">,</nts>
                  <nts id="Seg_508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_510" n="HIAT:w" s="T142">barɨta</ts>
                  <nts id="Seg_511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_513" n="HIAT:w" s="T143">tɨːtɨrkaj</ts>
                  <nts id="Seg_514" n="HIAT:ip">.</nts>
                  <nts id="Seg_515" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T157" id="Seg_517" n="HIAT:u" s="T144">
                  <ts e="T145" id="Seg_519" n="HIAT:w" s="T144">Bert</ts>
                  <nts id="Seg_520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_522" n="HIAT:w" s="T145">toŋobun</ts>
                  <nts id="Seg_523" n="HIAT:ip">,</nts>
                  <nts id="Seg_524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_526" n="HIAT:w" s="T146">hu͡organa</ts>
                  <nts id="Seg_527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_529" n="HIAT:w" s="T147">da</ts>
                  <nts id="Seg_530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_532" n="HIAT:w" s="T148">hu͡okpun</ts>
                  <nts id="Seg_533" n="HIAT:ip">,</nts>
                  <nts id="Seg_534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_536" n="HIAT:w" s="T149">taŋastɨːn</ts>
                  <nts id="Seg_537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_539" n="HIAT:w" s="T150">utujabɨn</ts>
                  <nts id="Seg_540" n="HIAT:ip">,</nts>
                  <nts id="Seg_541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_543" n="HIAT:w" s="T151">kihitten</ts>
                  <nts id="Seg_544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_546" n="HIAT:w" s="T152">kahan</ts>
                  <nts id="Seg_547" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_549" n="HIAT:w" s="T153">da</ts>
                  <nts id="Seg_550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_552" n="HIAT:w" s="T154">ütü͡önü</ts>
                  <nts id="Seg_553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_555" n="HIAT:w" s="T155">körbök</ts>
                  <nts id="Seg_556" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_558" n="HIAT:w" s="T156">kördükpün</ts>
                  <nts id="Seg_559" n="HIAT:ip">.</nts>
                  <nts id="Seg_560" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T161" id="Seg_562" n="HIAT:u" s="T157">
                  <ts e="T158" id="Seg_564" n="HIAT:w" s="T157">ɨl</ts>
                  <nts id="Seg_565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_567" n="HIAT:w" s="T158">minigin</ts>
                  <nts id="Seg_568" n="HIAT:ip">,</nts>
                  <nts id="Seg_569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_571" n="HIAT:w" s="T159">ɨj</ts>
                  <nts id="Seg_572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_574" n="HIAT:w" s="T160">baraksan</ts>
                  <nts id="Seg_575" n="HIAT:ip">!</nts>
                  <nts id="Seg_576" n="HIAT:ip">"</nts>
                  <nts id="Seg_577" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T166" id="Seg_579" n="HIAT:u" s="T161">
                  <ts e="T162" id="Seg_581" n="HIAT:w" s="T161">ɨj</ts>
                  <nts id="Seg_582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_584" n="HIAT:w" s="T162">kɨːs</ts>
                  <nts id="Seg_585" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_587" n="HIAT:w" s="T163">ogonu</ts>
                  <nts id="Seg_588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_590" n="HIAT:w" s="T164">ahɨnan</ts>
                  <nts id="Seg_591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_593" n="HIAT:w" s="T165">ɨlbɨt</ts>
                  <nts id="Seg_594" n="HIAT:ip">.</nts>
                  <nts id="Seg_595" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T172" id="Seg_597" n="HIAT:u" s="T166">
                  <ts e="T167" id="Seg_599" n="HIAT:w" s="T166">Hol</ts>
                  <nts id="Seg_600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_602" n="HIAT:w" s="T167">kemten</ts>
                  <nts id="Seg_603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_605" n="HIAT:w" s="T168">ɨjɨ</ts>
                  <nts id="Seg_606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_608" n="HIAT:w" s="T169">kɨŋaːtakka</ts>
                  <nts id="Seg_609" n="HIAT:ip">,</nts>
                  <nts id="Seg_610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_612" n="HIAT:w" s="T170">kihi</ts>
                  <nts id="Seg_613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_615" n="HIAT:w" s="T171">körör</ts>
                  <nts id="Seg_616" n="HIAT:ip">:</nts>
                  <nts id="Seg_617" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T178" id="Seg_619" n="HIAT:u" s="T172">
                  <ts e="T173" id="Seg_621" n="HIAT:w" s="T172">Kɨːs</ts>
                  <nts id="Seg_622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_624" n="HIAT:w" s="T173">ikki</ts>
                  <nts id="Seg_625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_627" n="HIAT:w" s="T174">holuːrdaːk</ts>
                  <nts id="Seg_628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_630" n="HIAT:w" s="T175">uː</ts>
                  <nts id="Seg_631" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_633" n="HIAT:w" s="T176">bahan</ts>
                  <nts id="Seg_634" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_636" n="HIAT:w" s="T177">turar</ts>
                  <nts id="Seg_637" n="HIAT:ip">.</nts>
                  <nts id="Seg_638" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T187" id="Seg_640" n="HIAT:u" s="T178">
                  <ts e="T179" id="Seg_642" n="HIAT:w" s="T178">Tüːn</ts>
                  <nts id="Seg_643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_645" n="HIAT:w" s="T179">ɨjdɨŋa</ts>
                  <nts id="Seg_646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_648" n="HIAT:w" s="T180">bu͡ollagɨna</ts>
                  <nts id="Seg_649" n="HIAT:ip">,</nts>
                  <nts id="Seg_650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_652" n="HIAT:w" s="T181">ɨttar</ts>
                  <nts id="Seg_653" n="HIAT:ip">,</nts>
                  <nts id="Seg_654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_656" n="HIAT:w" s="T182">ɨj</ts>
                  <nts id="Seg_657" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_659" n="HIAT:w" s="T183">di͡ek</ts>
                  <nts id="Seg_660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_662" n="HIAT:w" s="T184">kantajan</ts>
                  <nts id="Seg_663" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_665" n="HIAT:w" s="T185">baraːn</ts>
                  <nts id="Seg_666" n="HIAT:ip">,</nts>
                  <nts id="Seg_667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_669" n="HIAT:w" s="T186">ulujallar</ts>
                  <nts id="Seg_670" n="HIAT:ip">.</nts>
                  <nts id="Seg_671" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T194" id="Seg_673" n="HIAT:u" s="T187">
                  <ts e="T188" id="Seg_675" n="HIAT:w" s="T187">Ginner</ts>
                  <nts id="Seg_676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_678" n="HIAT:w" s="T188">taːjallar</ts>
                  <nts id="Seg_679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_681" n="HIAT:w" s="T189">Aːnɨhɨ</ts>
                  <nts id="Seg_682" n="HIAT:ip">,</nts>
                  <nts id="Seg_683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_685" n="HIAT:w" s="T190">anɨga</ts>
                  <nts id="Seg_686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_688" n="HIAT:w" s="T191">di͡eri</ts>
                  <nts id="Seg_689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_691" n="HIAT:w" s="T192">bi͡ek</ts>
                  <nts id="Seg_692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_694" n="HIAT:w" s="T193">aktallar</ts>
                  <nts id="Seg_695" n="HIAT:ip">.</nts>
                  <nts id="Seg_696" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T197" id="Seg_698" n="HIAT:u" s="T194">
                  <ts e="T195" id="Seg_700" n="HIAT:w" s="T194">Oloŋkoloːbuta</ts>
                  <nts id="Seg_701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_703" n="HIAT:w" s="T195">Ogdu͡o</ts>
                  <nts id="Seg_704" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_706" n="HIAT:w" s="T196">Aksʼonova</ts>
                  <nts id="Seg_707" n="HIAT:ip">.</nts>
                  <nts id="Seg_708" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T197" id="Seg_709" n="sc" s="T1">
               <ts e="T2" id="Seg_711" n="e" s="T1">Aːnɨs </ts>
               <ts e="T3" id="Seg_713" n="e" s="T2">kɨːs. </ts>
               <ts e="T4" id="Seg_715" n="e" s="T3">Bu </ts>
               <ts e="T5" id="Seg_717" n="e" s="T4">orto </ts>
               <ts e="T6" id="Seg_719" n="e" s="T5">hirge </ts>
               <ts e="T7" id="Seg_721" n="e" s="T6">tulaːjak </ts>
               <ts e="T8" id="Seg_723" n="e" s="T7">Aːnɨs </ts>
               <ts e="T9" id="Seg_725" n="e" s="T8">kɨːs </ts>
               <ts e="T10" id="Seg_727" n="e" s="T9">ogo </ts>
               <ts e="T11" id="Seg_729" n="e" s="T10">olorbuttaːk, </ts>
               <ts e="T12" id="Seg_731" n="e" s="T11">baːjdarga </ts>
               <ts e="T13" id="Seg_733" n="e" s="T12">kulut </ts>
               <ts e="T14" id="Seg_735" n="e" s="T13">bu͡ola </ts>
               <ts e="T15" id="Seg_737" n="e" s="T14">hɨldʼɨbɨt. </ts>
               <ts e="T16" id="Seg_739" n="e" s="T15">Ontulara </ts>
               <ts e="T17" id="Seg_741" n="e" s="T16">bert </ts>
               <ts e="T18" id="Seg_743" n="e" s="T17">atagastɨːr </ts>
               <ts e="T19" id="Seg_745" n="e" s="T18">etiler </ts>
               <ts e="T20" id="Seg_747" n="e" s="T19">ebit. </ts>
               <ts e="T21" id="Seg_749" n="e" s="T20">Kara </ts>
               <ts e="T22" id="Seg_751" n="e" s="T21">harsi͡erdattan </ts>
               <ts e="T23" id="Seg_753" n="e" s="T22">ki͡ehege </ts>
               <ts e="T24" id="Seg_755" n="e" s="T23">di͡eri </ts>
               <ts e="T25" id="Seg_757" n="e" s="T24">ikki </ts>
               <ts e="T26" id="Seg_759" n="e" s="T25">holuːrdaːk </ts>
               <ts e="T27" id="Seg_761" n="e" s="T26">uː </ts>
               <ts e="T28" id="Seg_763" n="e" s="T27">bahar. </ts>
               <ts e="T29" id="Seg_765" n="e" s="T28">Hajɨn </ts>
               <ts e="T30" id="Seg_767" n="e" s="T29">u͡oktalanɨ </ts>
               <ts e="T31" id="Seg_769" n="e" s="T30">ülgeːn </ts>
               <ts e="T32" id="Seg_771" n="e" s="T31">baraːn, </ts>
               <ts e="T33" id="Seg_773" n="e" s="T32">hügen </ts>
               <ts e="T34" id="Seg_775" n="e" s="T33">egeler. </ts>
               <ts e="T35" id="Seg_777" n="e" s="T34">Kɨhɨn </ts>
               <ts e="T36" id="Seg_779" n="e" s="T35">mastɨːr </ts>
               <ts e="T37" id="Seg_781" n="e" s="T36">onton </ts>
               <ts e="T38" id="Seg_783" n="e" s="T37">abɨratar. </ts>
               <ts e="T39" id="Seg_785" n="e" s="T38">Kɨhɨn </ts>
               <ts e="T40" id="Seg_787" n="e" s="T39">daː, </ts>
               <ts e="T41" id="Seg_789" n="e" s="T40">hajɨn </ts>
               <ts e="T42" id="Seg_791" n="e" s="T41">daː </ts>
               <ts e="T43" id="Seg_793" n="e" s="T42">uː, </ts>
               <ts e="T44" id="Seg_795" n="e" s="T43">mas </ts>
               <ts e="T45" id="Seg_797" n="e" s="T44">tulujbat, </ts>
               <ts e="T46" id="Seg_799" n="e" s="T45">honno </ts>
               <ts e="T47" id="Seg_801" n="e" s="T46">baranar. </ts>
               <ts e="T48" id="Seg_803" n="e" s="T47">Kɨhɨn </ts>
               <ts e="T49" id="Seg_805" n="e" s="T48">mu͡oraga </ts>
               <ts e="T50" id="Seg_807" n="e" s="T49">karaŋa. </ts>
               <ts e="T51" id="Seg_809" n="e" s="T50">Kün </ts>
               <ts e="T52" id="Seg_811" n="e" s="T51">taksɨbat, </ts>
               <ts e="T53" id="Seg_813" n="e" s="T52">ɨjdɨŋ </ts>
               <ts e="T54" id="Seg_815" n="e" s="T53">ere </ts>
               <ts e="T55" id="Seg_817" n="e" s="T54">kaːrɨ </ts>
               <ts e="T56" id="Seg_819" n="e" s="T55">hɨrdatar. </ts>
               <ts e="T57" id="Seg_821" n="e" s="T56">ɨj </ts>
               <ts e="T58" id="Seg_823" n="e" s="T57">kallaːŋŋa </ts>
               <ts e="T59" id="Seg_825" n="e" s="T58">buːs </ts>
               <ts e="T60" id="Seg_827" n="e" s="T59">kördük </ts>
               <ts e="T61" id="Seg_829" n="e" s="T60">tɨmnɨː, </ts>
               <ts e="T62" id="Seg_831" n="e" s="T61">hiri </ts>
               <ts e="T63" id="Seg_833" n="e" s="T62">itippet. </ts>
               <ts e="T64" id="Seg_835" n="e" s="T63">Gini </ts>
               <ts e="T65" id="Seg_837" n="e" s="T64">hɨrga </ts>
               <ts e="T66" id="Seg_839" n="e" s="T65">dʼi͡eni </ts>
               <ts e="T67" id="Seg_841" n="e" s="T66">ere </ts>
               <ts e="T68" id="Seg_843" n="e" s="T67">hɨrdatar. </ts>
               <ts e="T69" id="Seg_845" n="e" s="T68">Hɨrga </ts>
               <ts e="T70" id="Seg_847" n="e" s="T69">dʼi͡e </ts>
               <ts e="T71" id="Seg_849" n="e" s="T70">ihiger </ts>
               <ts e="T72" id="Seg_851" n="e" s="T71">künüs </ts>
               <ts e="T73" id="Seg_853" n="e" s="T72">ičiges, </ts>
               <ts e="T74" id="Seg_855" n="e" s="T73">ohok </ts>
               <ts e="T75" id="Seg_857" n="e" s="T74">ottular </ts>
               <ts e="T76" id="Seg_859" n="e" s="T75">bu͡olan. </ts>
               <ts e="T77" id="Seg_861" n="e" s="T76">Hɨrga </ts>
               <ts e="T78" id="Seg_863" n="e" s="T77">dʼi͡e </ts>
               <ts e="T79" id="Seg_865" n="e" s="T78">öldüːne </ts>
               <ts e="T80" id="Seg_867" n="e" s="T79">taba </ts>
               <ts e="T81" id="Seg_869" n="e" s="T80">tiriːtinen </ts>
               <ts e="T82" id="Seg_871" n="e" s="T81">oŋohullaːččɨ. </ts>
               <ts e="T83" id="Seg_873" n="e" s="T82">Aːnɨs </ts>
               <ts e="T84" id="Seg_875" n="e" s="T83">ere </ts>
               <ts e="T85" id="Seg_877" n="e" s="T84">toŋor, </ts>
               <ts e="T86" id="Seg_879" n="e" s="T85">gini </ts>
               <ts e="T87" id="Seg_881" n="e" s="T86">hirge </ts>
               <ts e="T88" id="Seg_883" n="e" s="T87">taŋastɨːn </ts>
               <ts e="T89" id="Seg_885" n="e" s="T88">utujar. </ts>
               <ts e="T90" id="Seg_887" n="e" s="T89">Ki͡ehennen </ts>
               <ts e="T91" id="Seg_889" n="e" s="T90">Aːnɨs </ts>
               <ts e="T92" id="Seg_891" n="e" s="T91">holuːrdarɨn </ts>
               <ts e="T93" id="Seg_893" n="e" s="T92">ɨlan </ts>
               <ts e="T94" id="Seg_895" n="e" s="T93">baraːn, </ts>
               <ts e="T95" id="Seg_897" n="e" s="T94">ojbonugar </ts>
               <ts e="T96" id="Seg_899" n="e" s="T95">barda. </ts>
               <ts e="T97" id="Seg_901" n="e" s="T96">Türgennik </ts>
               <ts e="T98" id="Seg_903" n="e" s="T97">kaːmtagɨna </ts>
               <ts e="T99" id="Seg_905" n="e" s="T98">toŋmot. </ts>
               <ts e="T100" id="Seg_907" n="e" s="T99">Ojboŋŋo </ts>
               <ts e="T101" id="Seg_909" n="e" s="T100">uː </ts>
               <ts e="T102" id="Seg_911" n="e" s="T101">bahaːrɨ </ts>
               <ts e="T103" id="Seg_913" n="e" s="T102">öŋöjdö. </ts>
               <ts e="T104" id="Seg_915" n="e" s="T103">Körbüte, </ts>
               <ts e="T105" id="Seg_917" n="e" s="T104">ɨj </ts>
               <ts e="T106" id="Seg_919" n="e" s="T105">külüːge </ts>
               <ts e="T107" id="Seg_921" n="e" s="T106">bert </ts>
               <ts e="T108" id="Seg_923" n="e" s="T107">hugaskaːn </ts>
               <ts e="T109" id="Seg_925" n="e" s="T108">köstör, </ts>
               <ts e="T110" id="Seg_927" n="e" s="T109">ojbonu </ts>
               <ts e="T111" id="Seg_929" n="e" s="T110">toloru </ts>
               <ts e="T112" id="Seg_931" n="e" s="T111">bu͡olla, </ts>
               <ts e="T113" id="Seg_933" n="e" s="T112">kihi </ts>
               <ts e="T114" id="Seg_935" n="e" s="T113">da </ts>
               <ts e="T115" id="Seg_937" n="e" s="T114">haŋatɨn </ts>
               <ts e="T116" id="Seg_939" n="e" s="T115">ister </ts>
               <ts e="T117" id="Seg_941" n="e" s="T116">kördük. </ts>
               <ts e="T118" id="Seg_943" n="e" s="T117">Aːnɨs </ts>
               <ts e="T119" id="Seg_945" n="e" s="T118">ɨjdɨŋanɨ </ts>
               <ts e="T120" id="Seg_947" n="e" s="T119">körön, </ts>
               <ts e="T121" id="Seg_949" n="e" s="T120">is </ts>
               <ts e="T122" id="Seg_951" n="e" s="T121">ihitten </ts>
               <ts e="T123" id="Seg_953" n="e" s="T122">tü͡öre </ts>
               <ts e="T124" id="Seg_955" n="e" s="T123">ɨtaːta, </ts>
               <ts e="T125" id="Seg_957" n="e" s="T124">ɨjɨ </ts>
               <ts e="T126" id="Seg_959" n="e" s="T125">kɨtta </ts>
               <ts e="T127" id="Seg_961" n="e" s="T126">kepsete-kepsete. </ts>
               <ts e="T128" id="Seg_963" n="e" s="T127">"ɨttaːgar </ts>
               <ts e="T129" id="Seg_965" n="e" s="T128">da </ts>
               <ts e="T130" id="Seg_967" n="e" s="T129">kuhagannɨk </ts>
               <ts e="T131" id="Seg_969" n="e" s="T130">olorobun", </ts>
               <ts e="T132" id="Seg_971" n="e" s="T131">diːr. </ts>
               <ts e="T133" id="Seg_973" n="e" s="T132">Kihi </ts>
               <ts e="T134" id="Seg_975" n="e" s="T133">barɨta </ts>
               <ts e="T135" id="Seg_977" n="e" s="T134">minigin </ts>
               <ts e="T136" id="Seg_979" n="e" s="T135">atagastɨːr. </ts>
               <ts e="T137" id="Seg_981" n="e" s="T136">Ajɨːlɨː </ts>
               <ts e="T138" id="Seg_983" n="e" s="T137">da </ts>
               <ts e="T139" id="Seg_985" n="e" s="T138">ahappattar, </ts>
               <ts e="T140" id="Seg_987" n="e" s="T139">ütü͡ö </ts>
               <ts e="T141" id="Seg_989" n="e" s="T140">taŋaha </ts>
               <ts e="T142" id="Seg_991" n="e" s="T141">hu͡okpun, </ts>
               <ts e="T143" id="Seg_993" n="e" s="T142">barɨta </ts>
               <ts e="T144" id="Seg_995" n="e" s="T143">tɨːtɨrkaj. </ts>
               <ts e="T145" id="Seg_997" n="e" s="T144">Bert </ts>
               <ts e="T146" id="Seg_999" n="e" s="T145">toŋobun, </ts>
               <ts e="T147" id="Seg_1001" n="e" s="T146">hu͡organa </ts>
               <ts e="T148" id="Seg_1003" n="e" s="T147">da </ts>
               <ts e="T149" id="Seg_1005" n="e" s="T148">hu͡okpun, </ts>
               <ts e="T150" id="Seg_1007" n="e" s="T149">taŋastɨːn </ts>
               <ts e="T151" id="Seg_1009" n="e" s="T150">utujabɨn, </ts>
               <ts e="T152" id="Seg_1011" n="e" s="T151">kihitten </ts>
               <ts e="T153" id="Seg_1013" n="e" s="T152">kahan </ts>
               <ts e="T154" id="Seg_1015" n="e" s="T153">da </ts>
               <ts e="T155" id="Seg_1017" n="e" s="T154">ütü͡önü </ts>
               <ts e="T156" id="Seg_1019" n="e" s="T155">körbök </ts>
               <ts e="T157" id="Seg_1021" n="e" s="T156">kördükpün. </ts>
               <ts e="T158" id="Seg_1023" n="e" s="T157">ɨl </ts>
               <ts e="T159" id="Seg_1025" n="e" s="T158">minigin, </ts>
               <ts e="T160" id="Seg_1027" n="e" s="T159">ɨj </ts>
               <ts e="T161" id="Seg_1029" n="e" s="T160">baraksan!" </ts>
               <ts e="T162" id="Seg_1031" n="e" s="T161">ɨj </ts>
               <ts e="T163" id="Seg_1033" n="e" s="T162">kɨːs </ts>
               <ts e="T164" id="Seg_1035" n="e" s="T163">ogonu </ts>
               <ts e="T165" id="Seg_1037" n="e" s="T164">ahɨnan </ts>
               <ts e="T166" id="Seg_1039" n="e" s="T165">ɨlbɨt. </ts>
               <ts e="T167" id="Seg_1041" n="e" s="T166">Hol </ts>
               <ts e="T168" id="Seg_1043" n="e" s="T167">kemten </ts>
               <ts e="T169" id="Seg_1045" n="e" s="T168">ɨjɨ </ts>
               <ts e="T170" id="Seg_1047" n="e" s="T169">kɨŋaːtakka, </ts>
               <ts e="T171" id="Seg_1049" n="e" s="T170">kihi </ts>
               <ts e="T172" id="Seg_1051" n="e" s="T171">körör: </ts>
               <ts e="T173" id="Seg_1053" n="e" s="T172">Kɨːs </ts>
               <ts e="T174" id="Seg_1055" n="e" s="T173">ikki </ts>
               <ts e="T175" id="Seg_1057" n="e" s="T174">holuːrdaːk </ts>
               <ts e="T176" id="Seg_1059" n="e" s="T175">uː </ts>
               <ts e="T177" id="Seg_1061" n="e" s="T176">bahan </ts>
               <ts e="T178" id="Seg_1063" n="e" s="T177">turar. </ts>
               <ts e="T179" id="Seg_1065" n="e" s="T178">Tüːn </ts>
               <ts e="T180" id="Seg_1067" n="e" s="T179">ɨjdɨŋa </ts>
               <ts e="T181" id="Seg_1069" n="e" s="T180">bu͡ollagɨna, </ts>
               <ts e="T182" id="Seg_1071" n="e" s="T181">ɨttar, </ts>
               <ts e="T183" id="Seg_1073" n="e" s="T182">ɨj </ts>
               <ts e="T184" id="Seg_1075" n="e" s="T183">di͡ek </ts>
               <ts e="T185" id="Seg_1077" n="e" s="T184">kantajan </ts>
               <ts e="T186" id="Seg_1079" n="e" s="T185">baraːn, </ts>
               <ts e="T187" id="Seg_1081" n="e" s="T186">ulujallar. </ts>
               <ts e="T188" id="Seg_1083" n="e" s="T187">Ginner </ts>
               <ts e="T189" id="Seg_1085" n="e" s="T188">taːjallar </ts>
               <ts e="T190" id="Seg_1087" n="e" s="T189">Aːnɨhɨ, </ts>
               <ts e="T191" id="Seg_1089" n="e" s="T190">anɨga </ts>
               <ts e="T192" id="Seg_1091" n="e" s="T191">di͡eri </ts>
               <ts e="T193" id="Seg_1093" n="e" s="T192">bi͡ek </ts>
               <ts e="T194" id="Seg_1095" n="e" s="T193">aktallar. </ts>
               <ts e="T195" id="Seg_1097" n="e" s="T194">Oloŋkoloːbuta </ts>
               <ts e="T196" id="Seg_1099" n="e" s="T195">Ogdu͡o </ts>
               <ts e="T197" id="Seg_1101" n="e" s="T196">Aksʼonova. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T3" id="Seg_1102" s="T1">AkEE_19900810_GirlAnys_flk.001 (001.001)</ta>
            <ta e="T15" id="Seg_1103" s="T3">AkEE_19900810_GirlAnys_flk.002 (001.002)</ta>
            <ta e="T20" id="Seg_1104" s="T15">AkEE_19900810_GirlAnys_flk.003 (001.003)</ta>
            <ta e="T28" id="Seg_1105" s="T20">AkEE_19900810_GirlAnys_flk.004 (001.004)</ta>
            <ta e="T34" id="Seg_1106" s="T28">AkEE_19900810_GirlAnys_flk.005 (001.005)</ta>
            <ta e="T38" id="Seg_1107" s="T34">AkEE_19900810_GirlAnys_flk.006 (001.006)</ta>
            <ta e="T47" id="Seg_1108" s="T38">AkEE_19900810_GirlAnys_flk.007 (001.007)</ta>
            <ta e="T50" id="Seg_1109" s="T47">AkEE_19900810_GirlAnys_flk.008 (001.008)</ta>
            <ta e="T56" id="Seg_1110" s="T50">AkEE_19900810_GirlAnys_flk.009 (001.009)</ta>
            <ta e="T63" id="Seg_1111" s="T56">AkEE_19900810_GirlAnys_flk.010 (001.010)</ta>
            <ta e="T68" id="Seg_1112" s="T63">AkEE_19900810_GirlAnys_flk.011 (001.011)</ta>
            <ta e="T76" id="Seg_1113" s="T68">AkEE_19900810_GirlAnys_flk.012 (001.012)</ta>
            <ta e="T82" id="Seg_1114" s="T76">AkEE_19900810_GirlAnys_flk.013 (001.013)</ta>
            <ta e="T89" id="Seg_1115" s="T82">AkEE_19900810_GirlAnys_flk.014 (001.014)</ta>
            <ta e="T96" id="Seg_1116" s="T89">AkEE_19900810_GirlAnys_flk.015 (001.015)</ta>
            <ta e="T99" id="Seg_1117" s="T96">AkEE_19900810_GirlAnys_flk.016 (001.016)</ta>
            <ta e="T103" id="Seg_1118" s="T99">AkEE_19900810_GirlAnys_flk.017 (001.017)</ta>
            <ta e="T117" id="Seg_1119" s="T103">AkEE_19900810_GirlAnys_flk.018 (001.018)</ta>
            <ta e="T127" id="Seg_1120" s="T117">AkEE_19900810_GirlAnys_flk.019 (001.019)</ta>
            <ta e="T132" id="Seg_1121" s="T127">AkEE_19900810_GirlAnys_flk.020 (001.020)</ta>
            <ta e="T136" id="Seg_1122" s="T132">AkEE_19900810_GirlAnys_flk.021 (001.021)</ta>
            <ta e="T144" id="Seg_1123" s="T136">AkEE_19900810_GirlAnys_flk.022 (001.022)</ta>
            <ta e="T157" id="Seg_1124" s="T144">AkEE_19900810_GirlAnys_flk.023 (001.023)</ta>
            <ta e="T161" id="Seg_1125" s="T157">AkEE_19900810_GirlAnys_flk.024 (001.024)</ta>
            <ta e="T166" id="Seg_1126" s="T161">AkEE_19900810_GirlAnys_flk.025 (001.025)</ta>
            <ta e="T172" id="Seg_1127" s="T166">AkEE_19900810_GirlAnys_flk.026 (001.026)</ta>
            <ta e="T178" id="Seg_1128" s="T172">AkEE_19900810_GirlAnys_flk.027 (001.027)</ta>
            <ta e="T187" id="Seg_1129" s="T178">AkEE_19900810_GirlAnys_flk.028 (001.028)</ta>
            <ta e="T194" id="Seg_1130" s="T187">AkEE_19900810_GirlAnys_flk.029 (001.029)</ta>
            <ta e="T197" id="Seg_1131" s="T194">AkEE_19900810_GirlAnys_flk.030 (001.030)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T3" id="Seg_1132" s="T1">Ааныс кыыс.</ta>
            <ta e="T15" id="Seg_1133" s="T3">Бу орто һиргэ тулаайак Ааныс кыыс ого олорбуттаак, баайдарга кулут буола һыльдыбыт.</ta>
            <ta e="T20" id="Seg_1134" s="T15">Онтулара бэрт атагастыыр, этилэр эбит. </ta>
            <ta e="T28" id="Seg_1135" s="T20">Кара һарсиэрдаттан киэһэгэ диэри икки һолуурдак уу баһар.</ta>
            <ta e="T34" id="Seg_1136" s="T28">Һайын уокталаны үлгээн бараан, һүгэн эгэлэр.</ta>
            <ta e="T38" id="Seg_1137" s="T34">Кыһын мастыыр онтон абратар (абыратар). </ta>
            <ta e="T47" id="Seg_1138" s="T38">Кыһын да, һайын да уу, мас тулуйбат, һонно баранар.</ta>
            <ta e="T50" id="Seg_1139" s="T47">Кыһын муорага караӈа.</ta>
            <ta e="T56" id="Seg_1140" s="T50">Күн таксыбат, ыйдыӈ эрэ каары һырдатар.</ta>
            <ta e="T63" id="Seg_1141" s="T56">Ый каллааӈӈа буус көрдүк тымныы, һири итиппэт. </ta>
            <ta e="T68" id="Seg_1142" s="T63">Гини һырга дьиэни эрэ һырдатар.</ta>
            <ta e="T76" id="Seg_1143" s="T68">Һырга дьиэ иһигэр күнүс ичигэс, оһок оттулар буолан. </ta>
            <ta e="T82" id="Seg_1144" s="T76">Һырга дьиэ өлдүүнэ таба тириитинэн оӈоһуллааччы.</ta>
            <ta e="T89" id="Seg_1145" s="T82">Ааныс эрэ тоӈор, гини һиргэ таӈастыын утуйар. </ta>
            <ta e="T96" id="Seg_1146" s="T89">Киэһэннэн Ааныс һолуурдарын ылан бараан, ойбонугар барда. </ta>
            <ta e="T99" id="Seg_1147" s="T96">Түргэнник каамтагынан тоӈмот.</ta>
            <ta e="T103" id="Seg_1148" s="T99">Ойбоӈно уу баһаары өӈөйдө. </ta>
            <ta e="T117" id="Seg_1149" s="T103">Көрбүтэ, ый күлүүгэ бэрт һугаскаан көстөр, ойбону толору буолла, киһи да һанатын истэр көрдүк. </ta>
            <ta e="T127" id="Seg_1150" s="T117">Ааныс ыйдыӈаны көрөн, ис иһиттэн түөрэ ытаата, ыйы кытта кэпсэтэ-кэпсэтэ. </ta>
            <ta e="T132" id="Seg_1151" s="T127">"Ыттаагар да куһаганык олоробун", – диир. –</ta>
            <ta e="T136" id="Seg_1152" s="T132">Киһи барыта минигин атагастыыр.</ta>
            <ta e="T144" id="Seg_1153" s="T136">айылыы да аһаппатар, үтүө таӈаһа һуокпун, барыта тыытыркай.</ta>
            <ta e="T157" id="Seg_1154" s="T144">бэрт тоӈобун, һуоргана да һуокпун – таӈастыын утуйабын, киһиттэн каһан да үтөнү көрбөт көрдүкпүн. </ta>
            <ta e="T161" id="Seg_1155" s="T157">Ыл минигин, ый бараксан!" </ta>
            <ta e="T166" id="Seg_1156" s="T161">Ый кыыс огону аһынан ылбыт. </ta>
            <ta e="T172" id="Seg_1157" s="T166">Һол кэмтэн ыйы кыӈаатакка, киһи көрөр: </ta>
            <ta e="T178" id="Seg_1158" s="T172">кыыс икки һолуурдаак уу баһа турара.</ta>
            <ta e="T187" id="Seg_1159" s="T178">Түүн ыйдыӈа буоллагына, ыттар, ый диэк кантайан бараан, улуйаллар.</ta>
            <ta e="T194" id="Seg_1160" s="T187">Гиннэр таайаллар Ааныһы, аныга диэри биэк актталлар. </ta>
            <ta e="T197" id="Seg_1161" s="T194">Олоӈколообута Огдуо Аксёнова. </ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T3" id="Seg_1162" s="T1">Aːnɨs kɨːs. </ta>
            <ta e="T15" id="Seg_1163" s="T3">Bu orto hirge tulaːjak Aːnɨs kɨːs ogo olorbuttaːk, baːjdarga kulut bu͡ola hɨldʼɨbɨt. </ta>
            <ta e="T20" id="Seg_1164" s="T15">Ontulara bert atagastɨːr etiler ebit. </ta>
            <ta e="T28" id="Seg_1165" s="T20">Kara harsi͡erdattan ki͡ehege di͡eri ikki holuːrdaːk uː bahar. </ta>
            <ta e="T34" id="Seg_1166" s="T28">Hajɨn u͡oktalanɨ ülgeːn baraːn, hügen egeler. </ta>
            <ta e="T38" id="Seg_1167" s="T34">Kɨhɨn mastɨːr onton abɨratar. </ta>
            <ta e="T47" id="Seg_1168" s="T38">Kɨhɨn daː, hajɨn daː uː, mas tulujbat, honno baranar. </ta>
            <ta e="T50" id="Seg_1169" s="T47">Kɨhɨn mu͡oraga karaŋa. </ta>
            <ta e="T56" id="Seg_1170" s="T50">Kün taksɨbat, ɨjdɨŋ ere kaːrɨ hɨrdatar. </ta>
            <ta e="T63" id="Seg_1171" s="T56">ɨj kallaːŋŋa buːs kördük tɨmnɨː, hiri itippet. </ta>
            <ta e="T68" id="Seg_1172" s="T63">Gini hɨrga dʼi͡eni ere hɨrdatar. </ta>
            <ta e="T76" id="Seg_1173" s="T68">Hɨrga dʼi͡e ihiger künüs ičiges, ohok ottular bu͡olan. </ta>
            <ta e="T82" id="Seg_1174" s="T76">Hɨrga dʼi͡e öldüːne taba tiriːtinen oŋohullaːččɨ. </ta>
            <ta e="T89" id="Seg_1175" s="T82">Aːnɨs ere toŋor, gini hirge taŋastɨːn utujar. </ta>
            <ta e="T96" id="Seg_1176" s="T89">Ki͡ehennen Aːnɨs holuːrdarɨn ɨlan baraːn, ojbonugar barda. </ta>
            <ta e="T99" id="Seg_1177" s="T96">Türgennik kaːmtagɨna toŋmot. </ta>
            <ta e="T103" id="Seg_1178" s="T99">Ojboŋŋo uː bahaːrɨ öŋöjdö. </ta>
            <ta e="T117" id="Seg_1179" s="T103">Körbüte, ɨj külüːge bert hugaskaːn köstör, ojbonu toloru bu͡olla, kihi da haŋatɨn ister kördük. </ta>
            <ta e="T127" id="Seg_1180" s="T117">Aːnɨs ɨjdɨŋanɨ körön, is ihitten tü͡öre ɨtaːta, ɨjɨ kɨtta kepsete-kepsete. </ta>
            <ta e="T132" id="Seg_1181" s="T127">"ɨttaːgar da kuhagannɨk olorobun", diːr. </ta>
            <ta e="T136" id="Seg_1182" s="T132">Kihi barɨta minigin atagastɨːr. </ta>
            <ta e="T144" id="Seg_1183" s="T136">Ajɨːlɨː da ahappattar, ütü͡ö taŋaha hu͡okpun, barɨta tɨːtɨrkaj. </ta>
            <ta e="T157" id="Seg_1184" s="T144">Bert toŋobun, hu͡organa da hu͡okpun, taŋastɨːn utujabɨn, kihitten kahan da ütü͡önü körbök kördükpün. </ta>
            <ta e="T161" id="Seg_1185" s="T157">ɨl minigin, ɨj baraksan!" </ta>
            <ta e="T166" id="Seg_1186" s="T161">ɨj kɨːs ogonu ahɨnan ɨlbɨt. </ta>
            <ta e="T172" id="Seg_1187" s="T166">Hol kemten ɨjɨ kɨŋaːtakka, kihi körör: </ta>
            <ta e="T178" id="Seg_1188" s="T172">Kɨːs ikki holuːrdaːk uː bahan turar. </ta>
            <ta e="T187" id="Seg_1189" s="T178">Tüːn ɨjdɨŋa bu͡ollagɨna, ɨttar, ɨj di͡ek kantajan baraːn, ulujallar. </ta>
            <ta e="T194" id="Seg_1190" s="T187">Ginner taːjallar Aːnɨhɨ, anɨga di͡eri bi͡ek aktallar. </ta>
            <ta e="T197" id="Seg_1191" s="T194">Oloŋkoloːbuta Ogdu͡o Aksʼonova. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_1192" s="T1">Aːnɨs</ta>
            <ta e="T3" id="Seg_1193" s="T2">kɨːs</ta>
            <ta e="T4" id="Seg_1194" s="T3">bu</ta>
            <ta e="T5" id="Seg_1195" s="T4">orto</ta>
            <ta e="T6" id="Seg_1196" s="T5">hir-ge</ta>
            <ta e="T7" id="Seg_1197" s="T6">tulaːjak</ta>
            <ta e="T8" id="Seg_1198" s="T7">Aːnɨs</ta>
            <ta e="T9" id="Seg_1199" s="T8">kɨːs</ta>
            <ta e="T10" id="Seg_1200" s="T9">ogo</ta>
            <ta e="T11" id="Seg_1201" s="T10">olor-but-taːk</ta>
            <ta e="T12" id="Seg_1202" s="T11">baːj-dar-ga</ta>
            <ta e="T13" id="Seg_1203" s="T12">kulut</ta>
            <ta e="T14" id="Seg_1204" s="T13">bu͡ol-a</ta>
            <ta e="T15" id="Seg_1205" s="T14">hɨldʼ-ɨ-bɨt</ta>
            <ta e="T16" id="Seg_1206" s="T15">on-tu-lara</ta>
            <ta e="T17" id="Seg_1207" s="T16">bert</ta>
            <ta e="T18" id="Seg_1208" s="T17">atagastɨː-r</ta>
            <ta e="T19" id="Seg_1209" s="T18">e-ti-ler</ta>
            <ta e="T20" id="Seg_1210" s="T19">e-bit</ta>
            <ta e="T21" id="Seg_1211" s="T20">kara</ta>
            <ta e="T22" id="Seg_1212" s="T21">harsi͡erda-ttan</ta>
            <ta e="T23" id="Seg_1213" s="T22">ki͡ehe-ge</ta>
            <ta e="T24" id="Seg_1214" s="T23">di͡eri</ta>
            <ta e="T25" id="Seg_1215" s="T24">ikki</ta>
            <ta e="T26" id="Seg_1216" s="T25">holuːr-daːk</ta>
            <ta e="T27" id="Seg_1217" s="T26">uː</ta>
            <ta e="T28" id="Seg_1218" s="T27">bah-ar</ta>
            <ta e="T29" id="Seg_1219" s="T28">hajɨn</ta>
            <ta e="T30" id="Seg_1220" s="T29">u͡oktala-nɨ</ta>
            <ta e="T31" id="Seg_1221" s="T30">ülgeː-n</ta>
            <ta e="T32" id="Seg_1222" s="T31">baraːn</ta>
            <ta e="T33" id="Seg_1223" s="T32">hüg-en</ta>
            <ta e="T34" id="Seg_1224" s="T33">egel-er</ta>
            <ta e="T35" id="Seg_1225" s="T34">kɨhɨn</ta>
            <ta e="T36" id="Seg_1226" s="T35">mas-tɨː-r</ta>
            <ta e="T37" id="Seg_1227" s="T36">onton</ta>
            <ta e="T38" id="Seg_1228" s="T37">abɨrat-ar</ta>
            <ta e="T39" id="Seg_1229" s="T38">kɨhɨn</ta>
            <ta e="T40" id="Seg_1230" s="T39">daː</ta>
            <ta e="T41" id="Seg_1231" s="T40">hajɨn</ta>
            <ta e="T42" id="Seg_1232" s="T41">daː</ta>
            <ta e="T43" id="Seg_1233" s="T42">uː</ta>
            <ta e="T44" id="Seg_1234" s="T43">mas</ta>
            <ta e="T45" id="Seg_1235" s="T44">tuluj-bat</ta>
            <ta e="T46" id="Seg_1236" s="T45">honno</ta>
            <ta e="T47" id="Seg_1237" s="T46">baran-ar</ta>
            <ta e="T48" id="Seg_1238" s="T47">kɨhɨn</ta>
            <ta e="T49" id="Seg_1239" s="T48">mu͡ora-ga</ta>
            <ta e="T50" id="Seg_1240" s="T49">karaŋa</ta>
            <ta e="T51" id="Seg_1241" s="T50">kün</ta>
            <ta e="T52" id="Seg_1242" s="T51">taks-ɨ-bat</ta>
            <ta e="T53" id="Seg_1243" s="T52">ɨjdɨŋ</ta>
            <ta e="T54" id="Seg_1244" s="T53">ere</ta>
            <ta e="T55" id="Seg_1245" s="T54">kaːr-ɨ</ta>
            <ta e="T56" id="Seg_1246" s="T55">hɨrd-a-t-ar</ta>
            <ta e="T57" id="Seg_1247" s="T56">ɨj</ta>
            <ta e="T58" id="Seg_1248" s="T57">kallaːŋ-ŋa</ta>
            <ta e="T59" id="Seg_1249" s="T58">buːs</ta>
            <ta e="T60" id="Seg_1250" s="T59">kördük</ta>
            <ta e="T61" id="Seg_1251" s="T60">tɨmnɨː</ta>
            <ta e="T62" id="Seg_1252" s="T61">hir-i</ta>
            <ta e="T63" id="Seg_1253" s="T62">iti-p-pet</ta>
            <ta e="T64" id="Seg_1254" s="T63">gini</ta>
            <ta e="T65" id="Seg_1255" s="T64">hɨrga</ta>
            <ta e="T66" id="Seg_1256" s="T65">dʼi͡e-ni</ta>
            <ta e="T67" id="Seg_1257" s="T66">ere</ta>
            <ta e="T68" id="Seg_1258" s="T67">hɨrd-a-t-ar</ta>
            <ta e="T69" id="Seg_1259" s="T68">hɨrga</ta>
            <ta e="T70" id="Seg_1260" s="T69">dʼi͡e</ta>
            <ta e="T71" id="Seg_1261" s="T70">ih-i-ger</ta>
            <ta e="T72" id="Seg_1262" s="T71">künüs</ta>
            <ta e="T73" id="Seg_1263" s="T72">ičiges</ta>
            <ta e="T74" id="Seg_1264" s="T73">ohok</ta>
            <ta e="T75" id="Seg_1265" s="T74">ott-u-l-ar</ta>
            <ta e="T76" id="Seg_1266" s="T75">bu͡ol-an</ta>
            <ta e="T77" id="Seg_1267" s="T76">hɨrga</ta>
            <ta e="T78" id="Seg_1268" s="T77">dʼi͡e</ta>
            <ta e="T79" id="Seg_1269" s="T78">öldüːn-e</ta>
            <ta e="T80" id="Seg_1270" s="T79">taba</ta>
            <ta e="T81" id="Seg_1271" s="T80">tiriː-ti-nen</ta>
            <ta e="T82" id="Seg_1272" s="T81">oŋohu-ll-aːččɨ</ta>
            <ta e="T83" id="Seg_1273" s="T82">Aːnɨs</ta>
            <ta e="T84" id="Seg_1274" s="T83">ere</ta>
            <ta e="T85" id="Seg_1275" s="T84">toŋ-or</ta>
            <ta e="T86" id="Seg_1276" s="T85">gini</ta>
            <ta e="T87" id="Seg_1277" s="T86">hir-ge</ta>
            <ta e="T88" id="Seg_1278" s="T87">taŋas-tɨːn</ta>
            <ta e="T89" id="Seg_1279" s="T88">utuj-ar</ta>
            <ta e="T90" id="Seg_1280" s="T89">ki͡ehe-nnen</ta>
            <ta e="T91" id="Seg_1281" s="T90">Aːnɨs</ta>
            <ta e="T92" id="Seg_1282" s="T91">holuːr-dar-ɨ-n</ta>
            <ta e="T93" id="Seg_1283" s="T92">ɨl-an</ta>
            <ta e="T94" id="Seg_1284" s="T93">baraːn</ta>
            <ta e="T95" id="Seg_1285" s="T94">ojbon-u-gar</ta>
            <ta e="T96" id="Seg_1286" s="T95">bar-d-a</ta>
            <ta e="T97" id="Seg_1287" s="T96">türgen-nik</ta>
            <ta e="T98" id="Seg_1288" s="T97">kaːm-tag-ɨna</ta>
            <ta e="T99" id="Seg_1289" s="T98">toŋ-mot</ta>
            <ta e="T100" id="Seg_1290" s="T99">ojboŋ-ŋo</ta>
            <ta e="T101" id="Seg_1291" s="T100">uː</ta>
            <ta e="T102" id="Seg_1292" s="T101">bah-aːrɨ</ta>
            <ta e="T103" id="Seg_1293" s="T102">öŋöj-d-ö</ta>
            <ta e="T104" id="Seg_1294" s="T103">kör-büt-e</ta>
            <ta e="T105" id="Seg_1295" s="T104">ɨj</ta>
            <ta e="T106" id="Seg_1296" s="T105">külüːg-e</ta>
            <ta e="T107" id="Seg_1297" s="T106">bert</ta>
            <ta e="T108" id="Seg_1298" s="T107">hugas-kaːn</ta>
            <ta e="T109" id="Seg_1299" s="T108">köst-ör</ta>
            <ta e="T110" id="Seg_1300" s="T109">ojbon-u</ta>
            <ta e="T111" id="Seg_1301" s="T110">toloru</ta>
            <ta e="T112" id="Seg_1302" s="T111">bu͡olla</ta>
            <ta e="T113" id="Seg_1303" s="T112">kihi</ta>
            <ta e="T114" id="Seg_1304" s="T113">da</ta>
            <ta e="T115" id="Seg_1305" s="T114">haŋa-tɨ-n</ta>
            <ta e="T116" id="Seg_1306" s="T115">ist-er</ta>
            <ta e="T117" id="Seg_1307" s="T116">kördük</ta>
            <ta e="T118" id="Seg_1308" s="T117">Aːnɨs</ta>
            <ta e="T119" id="Seg_1309" s="T118">ɨjdɨŋa-nɨ</ta>
            <ta e="T120" id="Seg_1310" s="T119">kör-ön</ta>
            <ta e="T121" id="Seg_1311" s="T120">is</ta>
            <ta e="T122" id="Seg_1312" s="T121">ih-i-tten</ta>
            <ta e="T123" id="Seg_1313" s="T122">tü͡ör-e</ta>
            <ta e="T124" id="Seg_1314" s="T123">ɨtaː-t-a</ta>
            <ta e="T125" id="Seg_1315" s="T124">ɨj-ɨ</ta>
            <ta e="T126" id="Seg_1316" s="T125">kɨtta</ta>
            <ta e="T127" id="Seg_1317" s="T126">kepset-e-kepset-e</ta>
            <ta e="T128" id="Seg_1318" s="T127">ɨt-taːgar</ta>
            <ta e="T129" id="Seg_1319" s="T128">da</ta>
            <ta e="T130" id="Seg_1320" s="T129">kuhagan-nɨk</ta>
            <ta e="T131" id="Seg_1321" s="T130">olor-o-bun</ta>
            <ta e="T132" id="Seg_1322" s="T131">diː-r</ta>
            <ta e="T133" id="Seg_1323" s="T132">kihi</ta>
            <ta e="T134" id="Seg_1324" s="T133">barɨ-ta</ta>
            <ta e="T135" id="Seg_1325" s="T134">minigi-n</ta>
            <ta e="T136" id="Seg_1326" s="T135">atagastɨː-r</ta>
            <ta e="T137" id="Seg_1327" s="T136">ajɨː-lɨː</ta>
            <ta e="T138" id="Seg_1328" s="T137">da</ta>
            <ta e="T139" id="Seg_1329" s="T138">ah-a-p-pat-tar</ta>
            <ta e="T140" id="Seg_1330" s="T139">ütü͡ö</ta>
            <ta e="T141" id="Seg_1331" s="T140">taŋah-a</ta>
            <ta e="T142" id="Seg_1332" s="T141">hu͡ok-pun</ta>
            <ta e="T143" id="Seg_1333" s="T142">barɨ-ta</ta>
            <ta e="T144" id="Seg_1334" s="T143">tɨːtɨrkaj</ta>
            <ta e="T145" id="Seg_1335" s="T144">bert</ta>
            <ta e="T146" id="Seg_1336" s="T145">toŋ-o-bun</ta>
            <ta e="T147" id="Seg_1337" s="T146">hu͡organ-a</ta>
            <ta e="T148" id="Seg_1338" s="T147">da</ta>
            <ta e="T149" id="Seg_1339" s="T148">hu͡ok-pun</ta>
            <ta e="T150" id="Seg_1340" s="T149">taŋas-tɨːn</ta>
            <ta e="T151" id="Seg_1341" s="T150">utuj-a-bɨn</ta>
            <ta e="T152" id="Seg_1342" s="T151">kihi-tten</ta>
            <ta e="T153" id="Seg_1343" s="T152">kahan</ta>
            <ta e="T154" id="Seg_1344" s="T153">da</ta>
            <ta e="T155" id="Seg_1345" s="T154">ütü͡ö-nü</ta>
            <ta e="T156" id="Seg_1346" s="T155">kör-bök</ta>
            <ta e="T157" id="Seg_1347" s="T156">kördük-pün</ta>
            <ta e="T158" id="Seg_1348" s="T157">ɨl</ta>
            <ta e="T159" id="Seg_1349" s="T158">minigi-n</ta>
            <ta e="T160" id="Seg_1350" s="T159">ɨj</ta>
            <ta e="T161" id="Seg_1351" s="T160">baraksan</ta>
            <ta e="T162" id="Seg_1352" s="T161">ɨj</ta>
            <ta e="T163" id="Seg_1353" s="T162">kɨːs</ta>
            <ta e="T164" id="Seg_1354" s="T163">ogo-nu</ta>
            <ta e="T165" id="Seg_1355" s="T164">ahɨn-an</ta>
            <ta e="T166" id="Seg_1356" s="T165">ɨl-bɨt</ta>
            <ta e="T167" id="Seg_1357" s="T166">hol</ta>
            <ta e="T168" id="Seg_1358" s="T167">kem-ten</ta>
            <ta e="T169" id="Seg_1359" s="T168">ɨj-ɨ</ta>
            <ta e="T170" id="Seg_1360" s="T169">kɨŋaː-tak-ka</ta>
            <ta e="T171" id="Seg_1361" s="T170">kihi</ta>
            <ta e="T172" id="Seg_1362" s="T171">kör-ör</ta>
            <ta e="T173" id="Seg_1363" s="T172">kɨːs</ta>
            <ta e="T174" id="Seg_1364" s="T173">ikki</ta>
            <ta e="T175" id="Seg_1365" s="T174">holuːr-daːk</ta>
            <ta e="T176" id="Seg_1366" s="T175">uː</ta>
            <ta e="T177" id="Seg_1367" s="T176">bah-an</ta>
            <ta e="T178" id="Seg_1368" s="T177">tur-ar</ta>
            <ta e="T179" id="Seg_1369" s="T178">tüːn</ta>
            <ta e="T180" id="Seg_1370" s="T179">ɨjdɨŋa</ta>
            <ta e="T181" id="Seg_1371" s="T180">bu͡ol-lag-ɨna</ta>
            <ta e="T182" id="Seg_1372" s="T181">ɨt-tar</ta>
            <ta e="T183" id="Seg_1373" s="T182">ɨj</ta>
            <ta e="T184" id="Seg_1374" s="T183">di͡ek</ta>
            <ta e="T185" id="Seg_1375" s="T184">kantaj-an</ta>
            <ta e="T186" id="Seg_1376" s="T185">baraːn</ta>
            <ta e="T187" id="Seg_1377" s="T186">uluj-al-lar</ta>
            <ta e="T188" id="Seg_1378" s="T187">ginner</ta>
            <ta e="T189" id="Seg_1379" s="T188">taːj-al-lar</ta>
            <ta e="T190" id="Seg_1380" s="T189">Aːnɨh-ɨ</ta>
            <ta e="T191" id="Seg_1381" s="T190">anɨ-ga</ta>
            <ta e="T192" id="Seg_1382" s="T191">di͡eri</ta>
            <ta e="T193" id="Seg_1383" s="T192">bi͡ek</ta>
            <ta e="T194" id="Seg_1384" s="T193">akt-al-lar</ta>
            <ta e="T195" id="Seg_1385" s="T194">oloŋko-loː-but-a</ta>
            <ta e="T196" id="Seg_1386" s="T195">Ogdu͡o</ta>
            <ta e="T197" id="Seg_1387" s="T196">Aksʼonova</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_1388" s="T1">Aːnɨs</ta>
            <ta e="T3" id="Seg_1389" s="T2">kɨːs</ta>
            <ta e="T4" id="Seg_1390" s="T3">bu</ta>
            <ta e="T5" id="Seg_1391" s="T4">orto</ta>
            <ta e="T6" id="Seg_1392" s="T5">hir-GA</ta>
            <ta e="T7" id="Seg_1393" s="T6">tulaːjak</ta>
            <ta e="T8" id="Seg_1394" s="T7">Aːnɨs</ta>
            <ta e="T9" id="Seg_1395" s="T8">kɨːs</ta>
            <ta e="T10" id="Seg_1396" s="T9">ogo</ta>
            <ta e="T11" id="Seg_1397" s="T10">olor-BIT-LAːK</ta>
            <ta e="T12" id="Seg_1398" s="T11">baːj-LAr-GA</ta>
            <ta e="T13" id="Seg_1399" s="T12">kulut</ta>
            <ta e="T14" id="Seg_1400" s="T13">bu͡ol-A</ta>
            <ta e="T15" id="Seg_1401" s="T14">hɨrɨt-I-BIT</ta>
            <ta e="T16" id="Seg_1402" s="T15">ol-tI-LArA</ta>
            <ta e="T17" id="Seg_1403" s="T16">bert</ta>
            <ta e="T18" id="Seg_1404" s="T17">atagastaː-Ar</ta>
            <ta e="T19" id="Seg_1405" s="T18">e-TI-LAr</ta>
            <ta e="T20" id="Seg_1406" s="T19">e-BIT</ta>
            <ta e="T21" id="Seg_1407" s="T20">kara</ta>
            <ta e="T22" id="Seg_1408" s="T21">harsi͡erda-ttAn</ta>
            <ta e="T23" id="Seg_1409" s="T22">ki͡ehe-GA</ta>
            <ta e="T24" id="Seg_1410" s="T23">di͡eri</ta>
            <ta e="T25" id="Seg_1411" s="T24">ikki</ta>
            <ta e="T26" id="Seg_1412" s="T25">holuːr-LAːK</ta>
            <ta e="T27" id="Seg_1413" s="T26">uː</ta>
            <ta e="T28" id="Seg_1414" s="T27">bas-Ar</ta>
            <ta e="T29" id="Seg_1415" s="T28">hajɨn</ta>
            <ta e="T30" id="Seg_1416" s="T29">u͡oktala-nI</ta>
            <ta e="T31" id="Seg_1417" s="T30">ülgeː-An</ta>
            <ta e="T32" id="Seg_1418" s="T31">baran</ta>
            <ta e="T33" id="Seg_1419" s="T32">hük-An</ta>
            <ta e="T34" id="Seg_1420" s="T33">egel-Ar</ta>
            <ta e="T35" id="Seg_1421" s="T34">kɨhɨn</ta>
            <ta e="T36" id="Seg_1422" s="T35">mas-LAː-Ar</ta>
            <ta e="T37" id="Seg_1423" s="T36">onton</ta>
            <ta e="T38" id="Seg_1424" s="T37">abɨrat-Ar</ta>
            <ta e="T39" id="Seg_1425" s="T38">kɨhɨn</ta>
            <ta e="T40" id="Seg_1426" s="T39">da</ta>
            <ta e="T41" id="Seg_1427" s="T40">hajɨn</ta>
            <ta e="T42" id="Seg_1428" s="T41">da</ta>
            <ta e="T43" id="Seg_1429" s="T42">uː</ta>
            <ta e="T44" id="Seg_1430" s="T43">mas</ta>
            <ta e="T45" id="Seg_1431" s="T44">tuluj-BAT</ta>
            <ta e="T46" id="Seg_1432" s="T45">honno</ta>
            <ta e="T47" id="Seg_1433" s="T46">baran-Ar</ta>
            <ta e="T48" id="Seg_1434" s="T47">kɨhɨn</ta>
            <ta e="T49" id="Seg_1435" s="T48">mu͡ora-GA</ta>
            <ta e="T50" id="Seg_1436" s="T49">karaŋa</ta>
            <ta e="T51" id="Seg_1437" s="T50">kün</ta>
            <ta e="T52" id="Seg_1438" s="T51">tagɨs-I-BAT</ta>
            <ta e="T53" id="Seg_1439" s="T52">ɨjdɨŋ</ta>
            <ta e="T54" id="Seg_1440" s="T53">ere</ta>
            <ta e="T55" id="Seg_1441" s="T54">kaːr-nI</ta>
            <ta e="T56" id="Seg_1442" s="T55">hɨrdaː-A-t-Ar</ta>
            <ta e="T57" id="Seg_1443" s="T56">ɨj</ta>
            <ta e="T58" id="Seg_1444" s="T57">kallaːn-GA</ta>
            <ta e="T59" id="Seg_1445" s="T58">buːs</ta>
            <ta e="T60" id="Seg_1446" s="T59">kördük</ta>
            <ta e="T61" id="Seg_1447" s="T60">tɨmnɨː</ta>
            <ta e="T62" id="Seg_1448" s="T61">hir-nI</ta>
            <ta e="T63" id="Seg_1449" s="T62">itij-t-BAT</ta>
            <ta e="T64" id="Seg_1450" s="T63">gini</ta>
            <ta e="T65" id="Seg_1451" s="T64">hɨrga</ta>
            <ta e="T66" id="Seg_1452" s="T65">dʼi͡e-nI</ta>
            <ta e="T67" id="Seg_1453" s="T66">ere</ta>
            <ta e="T68" id="Seg_1454" s="T67">hɨrdaː-A-t-Ar</ta>
            <ta e="T69" id="Seg_1455" s="T68">hɨrga</ta>
            <ta e="T70" id="Seg_1456" s="T69">dʼi͡e</ta>
            <ta e="T71" id="Seg_1457" s="T70">is-tI-GAr</ta>
            <ta e="T72" id="Seg_1458" s="T71">künüs</ta>
            <ta e="T73" id="Seg_1459" s="T72">ičiges</ta>
            <ta e="T74" id="Seg_1460" s="T73">ačaːk</ta>
            <ta e="T75" id="Seg_1461" s="T74">otut-I-n-Ar</ta>
            <ta e="T76" id="Seg_1462" s="T75">bu͡ol-An</ta>
            <ta e="T77" id="Seg_1463" s="T76">hɨrga</ta>
            <ta e="T78" id="Seg_1464" s="T77">dʼi͡e</ta>
            <ta e="T79" id="Seg_1465" s="T78">öldüːn-tA</ta>
            <ta e="T80" id="Seg_1466" s="T79">taba</ta>
            <ta e="T81" id="Seg_1467" s="T80">tiriː-tI-nAn</ta>
            <ta e="T82" id="Seg_1468" s="T81">oŋohun-LIN-AːččI</ta>
            <ta e="T83" id="Seg_1469" s="T82">Aːnɨs</ta>
            <ta e="T84" id="Seg_1470" s="T83">ere</ta>
            <ta e="T85" id="Seg_1471" s="T84">toŋ-Ar</ta>
            <ta e="T86" id="Seg_1472" s="T85">gini</ta>
            <ta e="T87" id="Seg_1473" s="T86">hir-GA</ta>
            <ta e="T88" id="Seg_1474" s="T87">taŋas-LIːN</ta>
            <ta e="T89" id="Seg_1475" s="T88">utuj-Ar</ta>
            <ta e="T90" id="Seg_1476" s="T89">ki͡ehe-nAn</ta>
            <ta e="T91" id="Seg_1477" s="T90">Aːnɨs</ta>
            <ta e="T92" id="Seg_1478" s="T91">holuːr-LAr-tI-n</ta>
            <ta e="T93" id="Seg_1479" s="T92">ɨl-An</ta>
            <ta e="T94" id="Seg_1480" s="T93">baran</ta>
            <ta e="T95" id="Seg_1481" s="T94">ojbon-tI-GAr</ta>
            <ta e="T96" id="Seg_1482" s="T95">bar-TI-tA</ta>
            <ta e="T97" id="Seg_1483" s="T96">türgen-LIk</ta>
            <ta e="T98" id="Seg_1484" s="T97">kaːm-TAK-InA</ta>
            <ta e="T99" id="Seg_1485" s="T98">toŋ-BAT</ta>
            <ta e="T100" id="Seg_1486" s="T99">ojbon-GA</ta>
            <ta e="T101" id="Seg_1487" s="T100">uː</ta>
            <ta e="T102" id="Seg_1488" s="T101">bas-AːrI</ta>
            <ta e="T103" id="Seg_1489" s="T102">öŋöj-TI-tA</ta>
            <ta e="T104" id="Seg_1490" s="T103">kör-BIT-tA</ta>
            <ta e="T105" id="Seg_1491" s="T104">ɨj</ta>
            <ta e="T106" id="Seg_1492" s="T105">külük-tA</ta>
            <ta e="T107" id="Seg_1493" s="T106">bert</ta>
            <ta e="T108" id="Seg_1494" s="T107">hugas-kAːN</ta>
            <ta e="T109" id="Seg_1495" s="T108">köhün-Ar</ta>
            <ta e="T110" id="Seg_1496" s="T109">ojbon-nI</ta>
            <ta e="T111" id="Seg_1497" s="T110">toloru</ta>
            <ta e="T112" id="Seg_1498" s="T111">bu͡olla</ta>
            <ta e="T113" id="Seg_1499" s="T112">kihi</ta>
            <ta e="T114" id="Seg_1500" s="T113">da</ta>
            <ta e="T115" id="Seg_1501" s="T114">haŋa-tI-n</ta>
            <ta e="T116" id="Seg_1502" s="T115">ihit-Ar</ta>
            <ta e="T117" id="Seg_1503" s="T116">kördük</ta>
            <ta e="T118" id="Seg_1504" s="T117">Aːnɨs</ta>
            <ta e="T119" id="Seg_1505" s="T118">ɨjdɨŋ-nI</ta>
            <ta e="T120" id="Seg_1506" s="T119">kör-An</ta>
            <ta e="T121" id="Seg_1507" s="T120">is</ta>
            <ta e="T122" id="Seg_1508" s="T121">is-tI-ttAn</ta>
            <ta e="T123" id="Seg_1509" s="T122">tü͡ör-A</ta>
            <ta e="T124" id="Seg_1510" s="T123">ɨtaː-TI-tA</ta>
            <ta e="T125" id="Seg_1511" s="T124">ɨj-nI</ta>
            <ta e="T126" id="Seg_1512" s="T125">kɨtta</ta>
            <ta e="T127" id="Seg_1513" s="T126">kepset-A-kepset-A</ta>
            <ta e="T128" id="Seg_1514" s="T127">ɨt-TAːgAr</ta>
            <ta e="T129" id="Seg_1515" s="T128">da</ta>
            <ta e="T130" id="Seg_1516" s="T129">kuhagan-LIk</ta>
            <ta e="T131" id="Seg_1517" s="T130">olor-A-BIn</ta>
            <ta e="T132" id="Seg_1518" s="T131">di͡e-Ar</ta>
            <ta e="T133" id="Seg_1519" s="T132">kihi</ta>
            <ta e="T134" id="Seg_1520" s="T133">barɨ-tA</ta>
            <ta e="T135" id="Seg_1521" s="T134">min-n</ta>
            <ta e="T136" id="Seg_1522" s="T135">atagastaː-Ar</ta>
            <ta e="T137" id="Seg_1523" s="T136">ajɨː-LIː</ta>
            <ta e="T138" id="Seg_1524" s="T137">da</ta>
            <ta e="T139" id="Seg_1525" s="T138">ahaː-A-t-BAT-LAr</ta>
            <ta e="T140" id="Seg_1526" s="T139">ötü͡ö</ta>
            <ta e="T141" id="Seg_1527" s="T140">taŋas-tA</ta>
            <ta e="T142" id="Seg_1528" s="T141">hu͡ok-BIn</ta>
            <ta e="T143" id="Seg_1529" s="T142">barɨ-tA</ta>
            <ta e="T144" id="Seg_1530" s="T143">tɨːtɨrkaj</ta>
            <ta e="T145" id="Seg_1531" s="T144">bert</ta>
            <ta e="T146" id="Seg_1532" s="T145">toŋ-A-BIn</ta>
            <ta e="T147" id="Seg_1533" s="T146">hu͡organ-tA</ta>
            <ta e="T148" id="Seg_1534" s="T147">da</ta>
            <ta e="T149" id="Seg_1535" s="T148">hu͡ok-BIn</ta>
            <ta e="T150" id="Seg_1536" s="T149">taŋas-LIːN</ta>
            <ta e="T151" id="Seg_1537" s="T150">utuj-A-BIn</ta>
            <ta e="T152" id="Seg_1538" s="T151">kihi-ttAn</ta>
            <ta e="T153" id="Seg_1539" s="T152">kahan</ta>
            <ta e="T154" id="Seg_1540" s="T153">da</ta>
            <ta e="T155" id="Seg_1541" s="T154">ötü͡ö-nI</ta>
            <ta e="T156" id="Seg_1542" s="T155">kör-BAT</ta>
            <ta e="T157" id="Seg_1543" s="T156">kördük-BIn</ta>
            <ta e="T158" id="Seg_1544" s="T157">ɨl</ta>
            <ta e="T159" id="Seg_1545" s="T158">min-n</ta>
            <ta e="T160" id="Seg_1546" s="T159">ɨj</ta>
            <ta e="T161" id="Seg_1547" s="T160">baraksan</ta>
            <ta e="T162" id="Seg_1548" s="T161">ɨj</ta>
            <ta e="T163" id="Seg_1549" s="T162">kɨːs</ta>
            <ta e="T164" id="Seg_1550" s="T163">ogo-nI</ta>
            <ta e="T165" id="Seg_1551" s="T164">ahɨn-An</ta>
            <ta e="T166" id="Seg_1552" s="T165">ɨl-BIT</ta>
            <ta e="T167" id="Seg_1553" s="T166">hol</ta>
            <ta e="T168" id="Seg_1554" s="T167">kem-ttAn</ta>
            <ta e="T169" id="Seg_1555" s="T168">ɨj-nI</ta>
            <ta e="T170" id="Seg_1556" s="T169">kɨŋaː-TAK-GA</ta>
            <ta e="T171" id="Seg_1557" s="T170">kihi</ta>
            <ta e="T172" id="Seg_1558" s="T171">kör-Ar</ta>
            <ta e="T173" id="Seg_1559" s="T172">kɨːs</ta>
            <ta e="T174" id="Seg_1560" s="T173">ikki</ta>
            <ta e="T175" id="Seg_1561" s="T174">holuːr-LAːK</ta>
            <ta e="T176" id="Seg_1562" s="T175">uː</ta>
            <ta e="T177" id="Seg_1563" s="T176">bas-An</ta>
            <ta e="T178" id="Seg_1564" s="T177">tur-Ar</ta>
            <ta e="T179" id="Seg_1565" s="T178">tüːn</ta>
            <ta e="T180" id="Seg_1566" s="T179">ɨjdɨŋ</ta>
            <ta e="T181" id="Seg_1567" s="T180">bu͡ol-TAK-InA</ta>
            <ta e="T182" id="Seg_1568" s="T181">ɨt-LAr</ta>
            <ta e="T183" id="Seg_1569" s="T182">ɨj</ta>
            <ta e="T184" id="Seg_1570" s="T183">dek</ta>
            <ta e="T185" id="Seg_1571" s="T184">kantaj-An</ta>
            <ta e="T186" id="Seg_1572" s="T185">baran</ta>
            <ta e="T187" id="Seg_1573" s="T186">uluj-Ar-LAr</ta>
            <ta e="T188" id="Seg_1574" s="T187">giniler</ta>
            <ta e="T189" id="Seg_1575" s="T188">taːj-Ar-LAr</ta>
            <ta e="T190" id="Seg_1576" s="T189">Aːnɨs-nI</ta>
            <ta e="T191" id="Seg_1577" s="T190">anɨ-GA</ta>
            <ta e="T192" id="Seg_1578" s="T191">di͡eri</ta>
            <ta e="T193" id="Seg_1579" s="T192">bi͡ek</ta>
            <ta e="T194" id="Seg_1580" s="T193">agɨn-Ar-LAr</ta>
            <ta e="T195" id="Seg_1581" s="T194">oloŋko-LAː-BIT-tA</ta>
            <ta e="T196" id="Seg_1582" s="T195">Ogdu͡o</ta>
            <ta e="T197" id="Seg_1583" s="T196">Aksʼonova</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_1584" s="T1">Anys</ta>
            <ta e="T3" id="Seg_1585" s="T2">girl.[NOM]</ta>
            <ta e="T4" id="Seg_1586" s="T3">this</ta>
            <ta e="T5" id="Seg_1587" s="T4">middle</ta>
            <ta e="T6" id="Seg_1588" s="T5">earth-DAT/LOC</ta>
            <ta e="T7" id="Seg_1589" s="T6">orphan.[NOM]</ta>
            <ta e="T8" id="Seg_1590" s="T7">Anys</ta>
            <ta e="T9" id="Seg_1591" s="T8">girl.[NOM]</ta>
            <ta e="T10" id="Seg_1592" s="T9">child.[NOM]</ta>
            <ta e="T11" id="Seg_1593" s="T10">live-PTCP.PST-PROPR.[NOM]</ta>
            <ta e="T12" id="Seg_1594" s="T11">rich-PL-DAT/LOC</ta>
            <ta e="T13" id="Seg_1595" s="T12">slave.[NOM]</ta>
            <ta e="T14" id="Seg_1596" s="T13">be-CVB.SIM</ta>
            <ta e="T15" id="Seg_1597" s="T14">go-EP-PST2.[3SG]</ta>
            <ta e="T16" id="Seg_1598" s="T15">that-3SG-3PL.[NOM]</ta>
            <ta e="T17" id="Seg_1599" s="T16">very</ta>
            <ta e="T18" id="Seg_1600" s="T17">offend-PTCP.PRS</ta>
            <ta e="T19" id="Seg_1601" s="T18">be-PST1-3PL</ta>
            <ta e="T20" id="Seg_1602" s="T19">be-PST2.[3SG]</ta>
            <ta e="T21" id="Seg_1603" s="T20">black</ta>
            <ta e="T22" id="Seg_1604" s="T21">morning-ABL</ta>
            <ta e="T23" id="Seg_1605" s="T22">evening-DAT/LOC</ta>
            <ta e="T24" id="Seg_1606" s="T23">until</ta>
            <ta e="T25" id="Seg_1607" s="T24">two</ta>
            <ta e="T26" id="Seg_1608" s="T25">bucket-PROPR</ta>
            <ta e="T27" id="Seg_1609" s="T26">water.[NOM]</ta>
            <ta e="T28" id="Seg_1610" s="T27">scoop-PRS.[3SG]</ta>
            <ta e="T29" id="Seg_1611" s="T28">in.summer</ta>
            <ta e="T30" id="Seg_1612" s="T29">dwarf.birch-ACC</ta>
            <ta e="T31" id="Seg_1613" s="T30">pluck-CVB.SEQ</ta>
            <ta e="T32" id="Seg_1614" s="T31">after</ta>
            <ta e="T33" id="Seg_1615" s="T32">take.on.the.back-CVB.SEQ</ta>
            <ta e="T34" id="Seg_1616" s="T33">bring-PRS.[3SG]</ta>
            <ta e="T35" id="Seg_1617" s="T34">in.winter</ta>
            <ta e="T36" id="Seg_1618" s="T35">wood-VBZ-PRS.[3SG]</ta>
            <ta e="T37" id="Seg_1619" s="T36">then</ta>
            <ta e="T38" id="Seg_1620" s="T37">chop.wood-PRS.[3SG]</ta>
            <ta e="T39" id="Seg_1621" s="T38">in.winter</ta>
            <ta e="T40" id="Seg_1622" s="T39">and</ta>
            <ta e="T41" id="Seg_1623" s="T40">in.summer</ta>
            <ta e="T42" id="Seg_1624" s="T41">and</ta>
            <ta e="T43" id="Seg_1625" s="T42">water.[NOM]</ta>
            <ta e="T44" id="Seg_1626" s="T43">wood.[NOM]</ta>
            <ta e="T45" id="Seg_1627" s="T44">bear-NEG.[3SG]</ta>
            <ta e="T46" id="Seg_1628" s="T45">immediately</ta>
            <ta e="T47" id="Seg_1629" s="T46">run.out-PRS.[3SG]</ta>
            <ta e="T48" id="Seg_1630" s="T47">in.winter</ta>
            <ta e="T49" id="Seg_1631" s="T48">tundra-DAT/LOC</ta>
            <ta e="T50" id="Seg_1632" s="T49">dark.[NOM]</ta>
            <ta e="T51" id="Seg_1633" s="T50">sun.[NOM]</ta>
            <ta e="T52" id="Seg_1634" s="T51">go.out-EP-NEG.[3SG]</ta>
            <ta e="T53" id="Seg_1635" s="T52">full.moon.[NOM]</ta>
            <ta e="T54" id="Seg_1636" s="T53">just</ta>
            <ta e="T55" id="Seg_1637" s="T54">snow-ACC</ta>
            <ta e="T56" id="Seg_1638" s="T55">shine-EP-CAUS-PRS.[3SG]</ta>
            <ta e="T57" id="Seg_1639" s="T56">moon.[NOM]</ta>
            <ta e="T58" id="Seg_1640" s="T57">sky-DAT/LOC</ta>
            <ta e="T59" id="Seg_1641" s="T58">ice.[NOM]</ta>
            <ta e="T60" id="Seg_1642" s="T59">similar</ta>
            <ta e="T61" id="Seg_1643" s="T60">cold.[NOM]</ta>
            <ta e="T62" id="Seg_1644" s="T61">earth-ACC</ta>
            <ta e="T63" id="Seg_1645" s="T62">become.warm-CAUS-NEG.[3SG]</ta>
            <ta e="T64" id="Seg_1646" s="T63">3SG.[NOM]</ta>
            <ta e="T65" id="Seg_1647" s="T64">sledge.[NOM]</ta>
            <ta e="T66" id="Seg_1648" s="T65">house-ACC</ta>
            <ta e="T67" id="Seg_1649" s="T66">just</ta>
            <ta e="T68" id="Seg_1650" s="T67">shine-EP-CAUS-PRS.[3SG]</ta>
            <ta e="T69" id="Seg_1651" s="T68">sledge.[NOM]</ta>
            <ta e="T70" id="Seg_1652" s="T69">house.[NOM]</ta>
            <ta e="T71" id="Seg_1653" s="T70">inside-3SG-DAT/LOC</ta>
            <ta e="T72" id="Seg_1654" s="T71">by.day</ta>
            <ta e="T73" id="Seg_1655" s="T72">warm.[NOM]</ta>
            <ta e="T74" id="Seg_1656" s="T73">stove.[NOM]</ta>
            <ta e="T75" id="Seg_1657" s="T74">heat-EP-REFL-PTCP.PRS</ta>
            <ta e="T76" id="Seg_1658" s="T75">be-CVB.SEQ</ta>
            <ta e="T77" id="Seg_1659" s="T76">sledge.[NOM]</ta>
            <ta e="T78" id="Seg_1660" s="T77">house.[NOM]</ta>
            <ta e="T79" id="Seg_1661" s="T78">leather.cover.for.a.tent-3SG.[NOM]</ta>
            <ta e="T80" id="Seg_1662" s="T79">reindeer.[NOM]</ta>
            <ta e="T81" id="Seg_1663" s="T80">fur-3SG-INSTR</ta>
            <ta e="T82" id="Seg_1664" s="T81">make-PASS/REFL-HAB.[3SG]</ta>
            <ta e="T83" id="Seg_1665" s="T82">Anys.[NOM]</ta>
            <ta e="T84" id="Seg_1666" s="T83">just</ta>
            <ta e="T85" id="Seg_1667" s="T84">freeze-PRS.[3SG]</ta>
            <ta e="T86" id="Seg_1668" s="T85">3SG.[NOM]</ta>
            <ta e="T87" id="Seg_1669" s="T86">earth-DAT/LOC</ta>
            <ta e="T88" id="Seg_1670" s="T87">clothes-COM</ta>
            <ta e="T89" id="Seg_1671" s="T88">sleep-PRS.[3SG]</ta>
            <ta e="T90" id="Seg_1672" s="T89">evening-INSTR</ta>
            <ta e="T91" id="Seg_1673" s="T90">Anys.[NOM]</ta>
            <ta e="T92" id="Seg_1674" s="T91">bucket-PL-3SG-ACC</ta>
            <ta e="T93" id="Seg_1675" s="T92">take-CVB.SEQ</ta>
            <ta e="T94" id="Seg_1676" s="T93">after</ta>
            <ta e="T95" id="Seg_1677" s="T94">icehole-3SG-DAT/LOC</ta>
            <ta e="T96" id="Seg_1678" s="T95">go-PST1-3SG</ta>
            <ta e="T97" id="Seg_1679" s="T96">fast-ADVZ</ta>
            <ta e="T98" id="Seg_1680" s="T97">walk-TEMP-3SG</ta>
            <ta e="T99" id="Seg_1681" s="T98">freeze-NEG.[3SG]</ta>
            <ta e="T100" id="Seg_1682" s="T99">icehole-DAT/LOC</ta>
            <ta e="T101" id="Seg_1683" s="T100">water.[NOM]</ta>
            <ta e="T102" id="Seg_1684" s="T101">scoop-CVB.PURP</ta>
            <ta e="T103" id="Seg_1685" s="T102">look.into-PST1-3SG</ta>
            <ta e="T104" id="Seg_1686" s="T103">see-PST2-3SG</ta>
            <ta e="T105" id="Seg_1687" s="T104">moon.[NOM]</ta>
            <ta e="T106" id="Seg_1688" s="T105">shadow-3SG.[NOM]</ta>
            <ta e="T107" id="Seg_1689" s="T106">very</ta>
            <ta e="T108" id="Seg_1690" s="T107">close-INTNS.[NOM]</ta>
            <ta e="T109" id="Seg_1691" s="T108">to.be.on.view-PRS.[3SG]</ta>
            <ta e="T110" id="Seg_1692" s="T109">icehole-ACC</ta>
            <ta e="T111" id="Seg_1693" s="T110">full</ta>
            <ta e="T112" id="Seg_1694" s="T111">MOD</ta>
            <ta e="T113" id="Seg_1695" s="T112">human.being.[NOM]</ta>
            <ta e="T114" id="Seg_1696" s="T113">EMPH</ta>
            <ta e="T115" id="Seg_1697" s="T114">word-3SG-ACC</ta>
            <ta e="T116" id="Seg_1698" s="T115">hear-PTCP.PRS.[NOM]</ta>
            <ta e="T117" id="Seg_1699" s="T116">similar</ta>
            <ta e="T118" id="Seg_1700" s="T117">Anys</ta>
            <ta e="T119" id="Seg_1701" s="T118">full.moon-ACC</ta>
            <ta e="T120" id="Seg_1702" s="T119">see-CVB.SEQ</ta>
            <ta e="T121" id="Seg_1703" s="T120">soul.[NOM]</ta>
            <ta e="T122" id="Seg_1704" s="T121">inside-3SG-ABL</ta>
            <ta e="T123" id="Seg_1705" s="T122">pull.out-CVB.SIM</ta>
            <ta e="T124" id="Seg_1706" s="T123">cry-PST1-3SG</ta>
            <ta e="T125" id="Seg_1707" s="T124">moon-ACC</ta>
            <ta e="T126" id="Seg_1708" s="T125">with</ta>
            <ta e="T127" id="Seg_1709" s="T126">chat-CVB.SIM-chat-CVB.SIM</ta>
            <ta e="T128" id="Seg_1710" s="T127">dog-COMP</ta>
            <ta e="T129" id="Seg_1711" s="T128">EMPH</ta>
            <ta e="T130" id="Seg_1712" s="T129">bad-ADVZ</ta>
            <ta e="T131" id="Seg_1713" s="T130">live-PRS-1SG</ta>
            <ta e="T132" id="Seg_1714" s="T131">say-PRS.[3SG]</ta>
            <ta e="T133" id="Seg_1715" s="T132">human.being.[NOM]</ta>
            <ta e="T134" id="Seg_1716" s="T133">every-3SG.[NOM]</ta>
            <ta e="T135" id="Seg_1717" s="T134">1SG-ACC</ta>
            <ta e="T136" id="Seg_1718" s="T135">offend-PRS.[3SG]</ta>
            <ta e="T137" id="Seg_1719" s="T136">good-SIM</ta>
            <ta e="T138" id="Seg_1720" s="T137">NEG</ta>
            <ta e="T139" id="Seg_1721" s="T138">eat-EP-CAUS-NEG-3PL</ta>
            <ta e="T140" id="Seg_1722" s="T139">good</ta>
            <ta e="T141" id="Seg_1723" s="T140">clothes-POSS</ta>
            <ta e="T142" id="Seg_1724" s="T141">NEG-1SG</ta>
            <ta e="T143" id="Seg_1725" s="T142">every-3SG.[NOM]</ta>
            <ta e="T144" id="Seg_1726" s="T143">small.piece.[NOM]</ta>
            <ta e="T145" id="Seg_1727" s="T144">very</ta>
            <ta e="T146" id="Seg_1728" s="T145">freeze-PRS-1SG</ta>
            <ta e="T147" id="Seg_1729" s="T146">blanket-POSS</ta>
            <ta e="T148" id="Seg_1730" s="T147">NEG</ta>
            <ta e="T149" id="Seg_1731" s="T148">NEG-1SG</ta>
            <ta e="T150" id="Seg_1732" s="T149">clothes-COM</ta>
            <ta e="T151" id="Seg_1733" s="T150">sleep-PRS-1SG</ta>
            <ta e="T152" id="Seg_1734" s="T151">human.being-ABL</ta>
            <ta e="T153" id="Seg_1735" s="T152">when</ta>
            <ta e="T154" id="Seg_1736" s="T153">NEG</ta>
            <ta e="T155" id="Seg_1737" s="T154">good-ACC</ta>
            <ta e="T156" id="Seg_1738" s="T155">see-NEG.PTCP.[NOM]</ta>
            <ta e="T157" id="Seg_1739" s="T156">like-1SG</ta>
            <ta e="T158" id="Seg_1740" s="T157">take.[IMP.2SG]</ta>
            <ta e="T159" id="Seg_1741" s="T158">1SG-ACC</ta>
            <ta e="T160" id="Seg_1742" s="T159">moon.[NOM]</ta>
            <ta e="T161" id="Seg_1743" s="T160">MOD</ta>
            <ta e="T162" id="Seg_1744" s="T161">moon.[NOM]</ta>
            <ta e="T163" id="Seg_1745" s="T162">girl.[NOM]</ta>
            <ta e="T164" id="Seg_1746" s="T163">child-ACC</ta>
            <ta e="T165" id="Seg_1747" s="T164">feel.sorry-CVB.SEQ</ta>
            <ta e="T166" id="Seg_1748" s="T165">take-PST2.[3SG]</ta>
            <ta e="T167" id="Seg_1749" s="T166">that.EMPH.[NOM]</ta>
            <ta e="T168" id="Seg_1750" s="T167">time-ABL</ta>
            <ta e="T169" id="Seg_1751" s="T168">month-ACC</ta>
            <ta e="T170" id="Seg_1752" s="T169">watch-PTCP.COND-DAT/LOC</ta>
            <ta e="T171" id="Seg_1753" s="T170">human.being.[NOM]</ta>
            <ta e="T172" id="Seg_1754" s="T171">see-PRS.[3SG]</ta>
            <ta e="T173" id="Seg_1755" s="T172">girl.[NOM]</ta>
            <ta e="T174" id="Seg_1756" s="T173">two</ta>
            <ta e="T175" id="Seg_1757" s="T174">bucket-PROPR</ta>
            <ta e="T176" id="Seg_1758" s="T175">water.[NOM]</ta>
            <ta e="T177" id="Seg_1759" s="T176">scoop-CVB.SEQ</ta>
            <ta e="T178" id="Seg_1760" s="T177">stand-PRS.[3SG]</ta>
            <ta e="T179" id="Seg_1761" s="T178">at.night</ta>
            <ta e="T180" id="Seg_1762" s="T179">full.moon.[NOM]</ta>
            <ta e="T181" id="Seg_1763" s="T180">be-TEMP-3SG</ta>
            <ta e="T182" id="Seg_1764" s="T181">dog-PL.[NOM]</ta>
            <ta e="T183" id="Seg_1765" s="T182">moon.[NOM]</ta>
            <ta e="T184" id="Seg_1766" s="T183">to</ta>
            <ta e="T185" id="Seg_1767" s="T184">raise.the.head-CVB.SEQ</ta>
            <ta e="T186" id="Seg_1768" s="T185">after</ta>
            <ta e="T187" id="Seg_1769" s="T186">howl-PRS-3PL</ta>
            <ta e="T188" id="Seg_1770" s="T187">3PL.[NOM]</ta>
            <ta e="T189" id="Seg_1771" s="T188">realize-PRS-3PL</ta>
            <ta e="T190" id="Seg_1772" s="T189">Anys-ACC</ta>
            <ta e="T191" id="Seg_1773" s="T190">now-DAT/LOC</ta>
            <ta e="T192" id="Seg_1774" s="T191">until</ta>
            <ta e="T193" id="Seg_1775" s="T192">always</ta>
            <ta e="T194" id="Seg_1776" s="T193">miss-PRS-3PL</ta>
            <ta e="T195" id="Seg_1777" s="T194">tale-VBZ-PST2-3SG</ta>
            <ta e="T196" id="Seg_1778" s="T195">Ogdo</ta>
            <ta e="T197" id="Seg_1779" s="T196">Aksyonova.[NOM]</ta>
         </annotation>
         <annotation name="gg" tierref="gg">
            <ta e="T2" id="Seg_1780" s="T1">Anys</ta>
            <ta e="T3" id="Seg_1781" s="T2">Mädchen.[NOM]</ta>
            <ta e="T4" id="Seg_1782" s="T3">dieses</ta>
            <ta e="T5" id="Seg_1783" s="T4">mittlerer</ta>
            <ta e="T6" id="Seg_1784" s="T5">Erde-DAT/LOC</ta>
            <ta e="T7" id="Seg_1785" s="T6">Waise.[NOM]</ta>
            <ta e="T8" id="Seg_1786" s="T7">Anys</ta>
            <ta e="T9" id="Seg_1787" s="T8">Mädchen.[NOM]</ta>
            <ta e="T10" id="Seg_1788" s="T9">Kind.[NOM]</ta>
            <ta e="T11" id="Seg_1789" s="T10">leben-PTCP.PST-PROPR.[NOM]</ta>
            <ta e="T12" id="Seg_1790" s="T11">reich-PL-DAT/LOC</ta>
            <ta e="T13" id="Seg_1791" s="T12">Sklave.[NOM]</ta>
            <ta e="T14" id="Seg_1792" s="T13">sein-CVB.SIM</ta>
            <ta e="T15" id="Seg_1793" s="T14">gehen-EP-PST2.[3SG]</ta>
            <ta e="T16" id="Seg_1794" s="T15">jenes-3SG-3PL.[NOM]</ta>
            <ta e="T17" id="Seg_1795" s="T16">sehr</ta>
            <ta e="T18" id="Seg_1796" s="T17">kränken-PTCP.PRS</ta>
            <ta e="T19" id="Seg_1797" s="T18">sein-PST1-3PL</ta>
            <ta e="T20" id="Seg_1798" s="T19">sein-PST2.[3SG]</ta>
            <ta e="T21" id="Seg_1799" s="T20">schwarz</ta>
            <ta e="T22" id="Seg_1800" s="T21">Morgen-ABL</ta>
            <ta e="T23" id="Seg_1801" s="T22">Abend-DAT/LOC</ta>
            <ta e="T24" id="Seg_1802" s="T23">bis.zu</ta>
            <ta e="T25" id="Seg_1803" s="T24">zwei</ta>
            <ta e="T26" id="Seg_1804" s="T25">Eimer-PROPR</ta>
            <ta e="T27" id="Seg_1805" s="T26">Wasser.[NOM]</ta>
            <ta e="T28" id="Seg_1806" s="T27">schöpfen-PRS.[3SG]</ta>
            <ta e="T29" id="Seg_1807" s="T28">im.Sommer</ta>
            <ta e="T30" id="Seg_1808" s="T29">Zwergbirke-ACC</ta>
            <ta e="T31" id="Seg_1809" s="T30">rupfen-CVB.SEQ</ta>
            <ta e="T32" id="Seg_1810" s="T31">nachdem</ta>
            <ta e="T33" id="Seg_1811" s="T32">auf.den.Rücken.nehmen-CVB.SEQ</ta>
            <ta e="T34" id="Seg_1812" s="T33">bringen-PRS.[3SG]</ta>
            <ta e="T35" id="Seg_1813" s="T34">im.Winter</ta>
            <ta e="T36" id="Seg_1814" s="T35">Holz-VBZ-PRS.[3SG]</ta>
            <ta e="T37" id="Seg_1815" s="T36">dann</ta>
            <ta e="T38" id="Seg_1816" s="T37">Holz.hacken-PRS.[3SG]</ta>
            <ta e="T39" id="Seg_1817" s="T38">im.Winter</ta>
            <ta e="T40" id="Seg_1818" s="T39">und</ta>
            <ta e="T41" id="Seg_1819" s="T40">im.Sommer</ta>
            <ta e="T42" id="Seg_1820" s="T41">und</ta>
            <ta e="T43" id="Seg_1821" s="T42">Wasser.[NOM]</ta>
            <ta e="T44" id="Seg_1822" s="T43">Holz.[NOM]</ta>
            <ta e="T45" id="Seg_1823" s="T44">ertragen-NEG.[3SG]</ta>
            <ta e="T46" id="Seg_1824" s="T45">sofort</ta>
            <ta e="T47" id="Seg_1825" s="T46">zu.Ende.gehen-PRS.[3SG]</ta>
            <ta e="T48" id="Seg_1826" s="T47">im.Winter</ta>
            <ta e="T49" id="Seg_1827" s="T48">Tundra-DAT/LOC</ta>
            <ta e="T50" id="Seg_1828" s="T49">dunkel.[NOM]</ta>
            <ta e="T51" id="Seg_1829" s="T50">Sonne.[NOM]</ta>
            <ta e="T52" id="Seg_1830" s="T51">hinausgehen-EP-NEG.[3SG]</ta>
            <ta e="T53" id="Seg_1831" s="T52">Vollmond.[NOM]</ta>
            <ta e="T54" id="Seg_1832" s="T53">nur</ta>
            <ta e="T55" id="Seg_1833" s="T54">Schnee-ACC</ta>
            <ta e="T56" id="Seg_1834" s="T55">scheinen-EP-CAUS-PRS.[3SG]</ta>
            <ta e="T57" id="Seg_1835" s="T56">Mond.[NOM]</ta>
            <ta e="T58" id="Seg_1836" s="T57">Himmel-DAT/LOC</ta>
            <ta e="T59" id="Seg_1837" s="T58">Eis.[NOM]</ta>
            <ta e="T60" id="Seg_1838" s="T59">ähnlich</ta>
            <ta e="T61" id="Seg_1839" s="T60">kalt.[NOM]</ta>
            <ta e="T62" id="Seg_1840" s="T61">Erde-ACC</ta>
            <ta e="T63" id="Seg_1841" s="T62">warm.werden-CAUS-NEG.[3SG]</ta>
            <ta e="T64" id="Seg_1842" s="T63">3SG.[NOM]</ta>
            <ta e="T65" id="Seg_1843" s="T64">Schlitten.[NOM]</ta>
            <ta e="T66" id="Seg_1844" s="T65">Haus-ACC</ta>
            <ta e="T67" id="Seg_1845" s="T66">nur</ta>
            <ta e="T68" id="Seg_1846" s="T67">scheinen-EP-CAUS-PRS.[3SG]</ta>
            <ta e="T69" id="Seg_1847" s="T68">Schlitten.[NOM]</ta>
            <ta e="T70" id="Seg_1848" s="T69">Haus.[NOM]</ta>
            <ta e="T71" id="Seg_1849" s="T70">Inneres-3SG-DAT/LOC</ta>
            <ta e="T72" id="Seg_1850" s="T71">am.Tag</ta>
            <ta e="T73" id="Seg_1851" s="T72">warm.[NOM]</ta>
            <ta e="T74" id="Seg_1852" s="T73">Herd.[NOM]</ta>
            <ta e="T75" id="Seg_1853" s="T74">heizen-EP-REFL-PTCP.PRS</ta>
            <ta e="T76" id="Seg_1854" s="T75">sein-CVB.SEQ</ta>
            <ta e="T77" id="Seg_1855" s="T76">Schlitten.[NOM]</ta>
            <ta e="T78" id="Seg_1856" s="T77">Haus.[NOM]</ta>
            <ta e="T79" id="Seg_1857" s="T78">Lederdecke.eines.Zeltes-3SG.[NOM]</ta>
            <ta e="T80" id="Seg_1858" s="T79">Rentier.[NOM]</ta>
            <ta e="T81" id="Seg_1859" s="T80">Fell-3SG-INSTR</ta>
            <ta e="T82" id="Seg_1860" s="T81">machen-PASS/REFL-HAB.[3SG]</ta>
            <ta e="T83" id="Seg_1861" s="T82">Anys.[NOM]</ta>
            <ta e="T84" id="Seg_1862" s="T83">nur</ta>
            <ta e="T85" id="Seg_1863" s="T84">frieren-PRS.[3SG]</ta>
            <ta e="T86" id="Seg_1864" s="T85">3SG.[NOM]</ta>
            <ta e="T87" id="Seg_1865" s="T86">Erde-DAT/LOC</ta>
            <ta e="T88" id="Seg_1866" s="T87">Kleidung-COM</ta>
            <ta e="T89" id="Seg_1867" s="T88">schlafen-PRS.[3SG]</ta>
            <ta e="T90" id="Seg_1868" s="T89">Abend-INSTR</ta>
            <ta e="T91" id="Seg_1869" s="T90">Anys.[NOM]</ta>
            <ta e="T92" id="Seg_1870" s="T91">Eimer-PL-3SG-ACC</ta>
            <ta e="T93" id="Seg_1871" s="T92">nehmen-CVB.SEQ</ta>
            <ta e="T94" id="Seg_1872" s="T93">nachdem</ta>
            <ta e="T95" id="Seg_1873" s="T94">Eisloch-3SG-DAT/LOC</ta>
            <ta e="T96" id="Seg_1874" s="T95">gehen-PST1-3SG</ta>
            <ta e="T97" id="Seg_1875" s="T96">schnell-ADVZ</ta>
            <ta e="T98" id="Seg_1876" s="T97">go-TEMP-3SG</ta>
            <ta e="T99" id="Seg_1877" s="T98">frieren-NEG.[3SG]</ta>
            <ta e="T100" id="Seg_1878" s="T99">Eisloch-DAT/LOC</ta>
            <ta e="T101" id="Seg_1879" s="T100">Wasser.[NOM]</ta>
            <ta e="T102" id="Seg_1880" s="T101">schöpfen-CVB.PURP</ta>
            <ta e="T103" id="Seg_1881" s="T102">hineinschauen-PST1-3SG</ta>
            <ta e="T104" id="Seg_1882" s="T103">sehen-PST2-3SG</ta>
            <ta e="T105" id="Seg_1883" s="T104">Mond.[NOM]</ta>
            <ta e="T106" id="Seg_1884" s="T105">Schatten-3SG.[NOM]</ta>
            <ta e="T107" id="Seg_1885" s="T106">sehr</ta>
            <ta e="T108" id="Seg_1886" s="T107">nah-INTNS.[NOM]</ta>
            <ta e="T109" id="Seg_1887" s="T108">zu.sehen.sein-PRS.[3SG]</ta>
            <ta e="T110" id="Seg_1888" s="T109">Eisloch-ACC</ta>
            <ta e="T111" id="Seg_1889" s="T110">voll</ta>
            <ta e="T112" id="Seg_1890" s="T111">MOD</ta>
            <ta e="T113" id="Seg_1891" s="T112">Mensch.[NOM]</ta>
            <ta e="T114" id="Seg_1892" s="T113">EMPH</ta>
            <ta e="T115" id="Seg_1893" s="T114">Wort-3SG-ACC</ta>
            <ta e="T116" id="Seg_1894" s="T115">hören-PTCP.PRS.[NOM]</ta>
            <ta e="T117" id="Seg_1895" s="T116">ähnlich</ta>
            <ta e="T118" id="Seg_1896" s="T117">Anys</ta>
            <ta e="T119" id="Seg_1897" s="T118">Vollmond-ACC</ta>
            <ta e="T120" id="Seg_1898" s="T119">sehen-CVB.SEQ</ta>
            <ta e="T121" id="Seg_1899" s="T120">Seele.[NOM]</ta>
            <ta e="T122" id="Seg_1900" s="T121">Inneres-3SG-ABL</ta>
            <ta e="T123" id="Seg_1901" s="T122">ausreißen-CVB.SIM</ta>
            <ta e="T124" id="Seg_1902" s="T123">weinen-PST1-3SG</ta>
            <ta e="T125" id="Seg_1903" s="T124">Mond-ACC</ta>
            <ta e="T126" id="Seg_1904" s="T125">mit</ta>
            <ta e="T127" id="Seg_1905" s="T126">sich.unterhalten-CVB.SIM-sich.unterhalten-CVB.SIM</ta>
            <ta e="T128" id="Seg_1906" s="T127">Hund-COMP</ta>
            <ta e="T129" id="Seg_1907" s="T128">EMPH</ta>
            <ta e="T130" id="Seg_1908" s="T129">schlecht-ADVZ</ta>
            <ta e="T131" id="Seg_1909" s="T130">leben-PRS-1SG</ta>
            <ta e="T132" id="Seg_1910" s="T131">sagen-PRS.[3SG]</ta>
            <ta e="T133" id="Seg_1911" s="T132">Mensch.[NOM]</ta>
            <ta e="T134" id="Seg_1912" s="T133">jeder-3SG.[NOM]</ta>
            <ta e="T135" id="Seg_1913" s="T134">1SG-ACC</ta>
            <ta e="T136" id="Seg_1914" s="T135">kränken-PRS.[3SG]</ta>
            <ta e="T137" id="Seg_1915" s="T136">gut-SIM</ta>
            <ta e="T138" id="Seg_1916" s="T137">NEG</ta>
            <ta e="T139" id="Seg_1917" s="T138">essen-EP-CAUS-NEG-3PL</ta>
            <ta e="T140" id="Seg_1918" s="T139">gut</ta>
            <ta e="T141" id="Seg_1919" s="T140">Kleidung-POSS</ta>
            <ta e="T142" id="Seg_1920" s="T141">NEG-1SG</ta>
            <ta e="T143" id="Seg_1921" s="T142">jeder-3SG.[NOM]</ta>
            <ta e="T144" id="Seg_1922" s="T143">kleines.Stück.[NOM]</ta>
            <ta e="T145" id="Seg_1923" s="T144">sehr</ta>
            <ta e="T146" id="Seg_1924" s="T145">frieren-PRS-1SG</ta>
            <ta e="T147" id="Seg_1925" s="T146">Bettdecke-POSS</ta>
            <ta e="T148" id="Seg_1926" s="T147">NEG</ta>
            <ta e="T149" id="Seg_1927" s="T148">NEG-1SG</ta>
            <ta e="T150" id="Seg_1928" s="T149">Kleidung-COM</ta>
            <ta e="T151" id="Seg_1929" s="T150">schlafen-PRS-1SG</ta>
            <ta e="T152" id="Seg_1930" s="T151">Mensch-ABL</ta>
            <ta e="T153" id="Seg_1931" s="T152">wann</ta>
            <ta e="T154" id="Seg_1932" s="T153">NEG</ta>
            <ta e="T155" id="Seg_1933" s="T154">gut-ACC</ta>
            <ta e="T156" id="Seg_1934" s="T155">sehen-NEG.PTCP.[NOM]</ta>
            <ta e="T157" id="Seg_1935" s="T156">wie-1SG</ta>
            <ta e="T158" id="Seg_1936" s="T157">nehmen.[IMP.2SG]</ta>
            <ta e="T159" id="Seg_1937" s="T158">1SG-ACC</ta>
            <ta e="T160" id="Seg_1938" s="T159">Mond.[NOM]</ta>
            <ta e="T161" id="Seg_1939" s="T160">MOD</ta>
            <ta e="T162" id="Seg_1940" s="T161">Mond.[NOM]</ta>
            <ta e="T163" id="Seg_1941" s="T162">Mädchen.[NOM]</ta>
            <ta e="T164" id="Seg_1942" s="T163">Kind-ACC</ta>
            <ta e="T165" id="Seg_1943" s="T164">bemitleiden-CVB.SEQ</ta>
            <ta e="T166" id="Seg_1944" s="T165">nehmen-PST2.[3SG]</ta>
            <ta e="T167" id="Seg_1945" s="T166">jenes.EMPH.[NOM]</ta>
            <ta e="T168" id="Seg_1946" s="T167">Zeit-ABL</ta>
            <ta e="T169" id="Seg_1947" s="T168">Monat-ACC</ta>
            <ta e="T170" id="Seg_1948" s="T169">beobachten-PTCP.COND-DAT/LOC</ta>
            <ta e="T171" id="Seg_1949" s="T170">Mensch.[NOM]</ta>
            <ta e="T172" id="Seg_1950" s="T171">sehen-PRS.[3SG]</ta>
            <ta e="T173" id="Seg_1951" s="T172">Mädchen.[NOM]</ta>
            <ta e="T174" id="Seg_1952" s="T173">zwei</ta>
            <ta e="T175" id="Seg_1953" s="T174">Eimer-PROPR</ta>
            <ta e="T176" id="Seg_1954" s="T175">Wasser.[NOM]</ta>
            <ta e="T177" id="Seg_1955" s="T176">schöpfen-CVB.SEQ</ta>
            <ta e="T178" id="Seg_1956" s="T177">stehen-PRS.[3SG]</ta>
            <ta e="T179" id="Seg_1957" s="T178">nachts</ta>
            <ta e="T180" id="Seg_1958" s="T179">Vollmond.[NOM]</ta>
            <ta e="T181" id="Seg_1959" s="T180">sein-TEMP-3SG</ta>
            <ta e="T182" id="Seg_1960" s="T181">Hund-PL.[NOM]</ta>
            <ta e="T183" id="Seg_1961" s="T182">Mond.[NOM]</ta>
            <ta e="T184" id="Seg_1962" s="T183">zu</ta>
            <ta e="T185" id="Seg_1963" s="T184">den.Kopf.heben-CVB.SEQ</ta>
            <ta e="T186" id="Seg_1964" s="T185">nachdem</ta>
            <ta e="T187" id="Seg_1965" s="T186">heulen-PRS-3PL</ta>
            <ta e="T188" id="Seg_1966" s="T187">3PL.[NOM]</ta>
            <ta e="T189" id="Seg_1967" s="T188">erkennen-PRS-3PL</ta>
            <ta e="T190" id="Seg_1968" s="T189">Anys-ACC</ta>
            <ta e="T191" id="Seg_1969" s="T190">jetzt-DAT/LOC</ta>
            <ta e="T192" id="Seg_1970" s="T191">bis.zu</ta>
            <ta e="T193" id="Seg_1971" s="T192">immer</ta>
            <ta e="T194" id="Seg_1972" s="T193">vermissen-PRS-3PL</ta>
            <ta e="T195" id="Seg_1973" s="T194">Märchen-VBZ-PST2-3SG</ta>
            <ta e="T196" id="Seg_1974" s="T195">Ogdo</ta>
            <ta e="T197" id="Seg_1975" s="T196">Aksjonova.[NOM]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_1976" s="T1">Аныс</ta>
            <ta e="T3" id="Seg_1977" s="T2">девушка.[NOM]</ta>
            <ta e="T4" id="Seg_1978" s="T3">этот</ta>
            <ta e="T5" id="Seg_1979" s="T4">средний</ta>
            <ta e="T6" id="Seg_1980" s="T5">земля-DAT/LOC</ta>
            <ta e="T7" id="Seg_1981" s="T6">сирота.[NOM]</ta>
            <ta e="T8" id="Seg_1982" s="T7">Аныс</ta>
            <ta e="T9" id="Seg_1983" s="T8">девушка.[NOM]</ta>
            <ta e="T10" id="Seg_1984" s="T9">ребенок.[NOM]</ta>
            <ta e="T11" id="Seg_1985" s="T10">жить-PTCP.PST-PROPR.[NOM]</ta>
            <ta e="T12" id="Seg_1986" s="T11">богатый-PL-DAT/LOC</ta>
            <ta e="T13" id="Seg_1987" s="T12">слуга.[NOM]</ta>
            <ta e="T14" id="Seg_1988" s="T13">быть-CVB.SIM</ta>
            <ta e="T15" id="Seg_1989" s="T14">идти-EP-PST2.[3SG]</ta>
            <ta e="T16" id="Seg_1990" s="T15">тот-3SG-3PL.[NOM]</ta>
            <ta e="T17" id="Seg_1991" s="T16">очень</ta>
            <ta e="T18" id="Seg_1992" s="T17">обижать-PTCP.PRS</ta>
            <ta e="T19" id="Seg_1993" s="T18">быть-PST1-3PL</ta>
            <ta e="T20" id="Seg_1994" s="T19">быть-PST2.[3SG]</ta>
            <ta e="T21" id="Seg_1995" s="T20">черный</ta>
            <ta e="T22" id="Seg_1996" s="T21">утро-ABL</ta>
            <ta e="T23" id="Seg_1997" s="T22">вечер-DAT/LOC</ta>
            <ta e="T24" id="Seg_1998" s="T23">пока</ta>
            <ta e="T25" id="Seg_1999" s="T24">два</ta>
            <ta e="T26" id="Seg_2000" s="T25">ведро-PROPR</ta>
            <ta e="T27" id="Seg_2001" s="T26">вода.[NOM]</ta>
            <ta e="T28" id="Seg_2002" s="T27">черпать-PRS.[3SG]</ta>
            <ta e="T29" id="Seg_2003" s="T28">летом</ta>
            <ta e="T30" id="Seg_2004" s="T29">карликовая.береза-ACC</ta>
            <ta e="T31" id="Seg_2005" s="T30">выщипывать-CVB.SEQ</ta>
            <ta e="T32" id="Seg_2006" s="T31">после</ta>
            <ta e="T33" id="Seg_2007" s="T32">наложить.на.спину-CVB.SEQ</ta>
            <ta e="T34" id="Seg_2008" s="T33">принести-PRS.[3SG]</ta>
            <ta e="T35" id="Seg_2009" s="T34">зимой</ta>
            <ta e="T36" id="Seg_2010" s="T35">дерево-VBZ-PRS.[3SG]</ta>
            <ta e="T37" id="Seg_2011" s="T36">потом</ta>
            <ta e="T38" id="Seg_2012" s="T37">рубить.дрова-PRS.[3SG]</ta>
            <ta e="T39" id="Seg_2013" s="T38">зимой</ta>
            <ta e="T40" id="Seg_2014" s="T39">да</ta>
            <ta e="T41" id="Seg_2015" s="T40">летом</ta>
            <ta e="T42" id="Seg_2016" s="T41">да</ta>
            <ta e="T43" id="Seg_2017" s="T42">вода.[NOM]</ta>
            <ta e="T44" id="Seg_2018" s="T43">дерево.[NOM]</ta>
            <ta e="T45" id="Seg_2019" s="T44">выносить-NEG.[3SG]</ta>
            <ta e="T46" id="Seg_2020" s="T45">сразу</ta>
            <ta e="T47" id="Seg_2021" s="T46">кончаться-PRS.[3SG]</ta>
            <ta e="T48" id="Seg_2022" s="T47">зимой</ta>
            <ta e="T49" id="Seg_2023" s="T48">тундра-DAT/LOC</ta>
            <ta e="T50" id="Seg_2024" s="T49">темный.[NOM]</ta>
            <ta e="T51" id="Seg_2025" s="T50">солнце.[NOM]</ta>
            <ta e="T52" id="Seg_2026" s="T51">выйти-EP-NEG.[3SG]</ta>
            <ta e="T53" id="Seg_2027" s="T52">полнолуние.[NOM]</ta>
            <ta e="T54" id="Seg_2028" s="T53">только</ta>
            <ta e="T55" id="Seg_2029" s="T54">снег-ACC</ta>
            <ta e="T56" id="Seg_2030" s="T55">светить-EP-CAUS-PRS.[3SG]</ta>
            <ta e="T57" id="Seg_2031" s="T56">луна.[NOM]</ta>
            <ta e="T58" id="Seg_2032" s="T57">небо-DAT/LOC</ta>
            <ta e="T59" id="Seg_2033" s="T58">лед.[NOM]</ta>
            <ta e="T60" id="Seg_2034" s="T59">подобно</ta>
            <ta e="T61" id="Seg_2035" s="T60">холодний.[NOM]</ta>
            <ta e="T62" id="Seg_2036" s="T61">земля-ACC</ta>
            <ta e="T63" id="Seg_2037" s="T62">согреться-CAUS-NEG.[3SG]</ta>
            <ta e="T64" id="Seg_2038" s="T63">3SG.[NOM]</ta>
            <ta e="T65" id="Seg_2039" s="T64">сани.[NOM]</ta>
            <ta e="T66" id="Seg_2040" s="T65">дом-ACC</ta>
            <ta e="T67" id="Seg_2041" s="T66">только</ta>
            <ta e="T68" id="Seg_2042" s="T67">светить-EP-CAUS-PRS.[3SG]</ta>
            <ta e="T69" id="Seg_2043" s="T68">сани.[NOM]</ta>
            <ta e="T70" id="Seg_2044" s="T69">дом.[NOM]</ta>
            <ta e="T71" id="Seg_2045" s="T70">нутро-3SG-DAT/LOC</ta>
            <ta e="T72" id="Seg_2046" s="T71">днем</ta>
            <ta e="T73" id="Seg_2047" s="T72">теплый.[NOM]</ta>
            <ta e="T74" id="Seg_2048" s="T73">очаг.[NOM]</ta>
            <ta e="T75" id="Seg_2049" s="T74">отапливать-EP-REFL-PTCP.PRS</ta>
            <ta e="T76" id="Seg_2050" s="T75">быть-CVB.SEQ</ta>
            <ta e="T77" id="Seg_2051" s="T76">сани.[NOM]</ta>
            <ta e="T78" id="Seg_2052" s="T77">дом.[NOM]</ta>
            <ta e="T79" id="Seg_2053" s="T78">кожаная.покрышка.чума-3SG.[NOM]</ta>
            <ta e="T80" id="Seg_2054" s="T79">олень.[NOM]</ta>
            <ta e="T81" id="Seg_2055" s="T80">шкура-3SG-INSTR</ta>
            <ta e="T82" id="Seg_2056" s="T81">делать-PASS/REFL-HAB.[3SG]</ta>
            <ta e="T83" id="Seg_2057" s="T82">Аныс.[NOM]</ta>
            <ta e="T84" id="Seg_2058" s="T83">только</ta>
            <ta e="T85" id="Seg_2059" s="T84">мерзнуть-PRS.[3SG]</ta>
            <ta e="T86" id="Seg_2060" s="T85">3SG.[NOM]</ta>
            <ta e="T87" id="Seg_2061" s="T86">земля-DAT/LOC</ta>
            <ta e="T88" id="Seg_2062" s="T87">одежда-COM</ta>
            <ta e="T89" id="Seg_2063" s="T88">спать-PRS.[3SG]</ta>
            <ta e="T90" id="Seg_2064" s="T89">вечер-INSTR</ta>
            <ta e="T91" id="Seg_2065" s="T90">Аныс.[NOM]</ta>
            <ta e="T92" id="Seg_2066" s="T91">ведро-PL-3SG-ACC</ta>
            <ta e="T93" id="Seg_2067" s="T92">взять-CVB.SEQ</ta>
            <ta e="T94" id="Seg_2068" s="T93">после</ta>
            <ta e="T95" id="Seg_2069" s="T94">прорубь-3SG-DAT/LOC</ta>
            <ta e="T96" id="Seg_2070" s="T95">идти-PST1-3SG</ta>
            <ta e="T97" id="Seg_2071" s="T96">быстрый-ADVZ</ta>
            <ta e="T98" id="Seg_2072" s="T97">идти-TEMP-3SG</ta>
            <ta e="T99" id="Seg_2073" s="T98">мерзнуть-NEG.[3SG]</ta>
            <ta e="T100" id="Seg_2074" s="T99">прорубь-DAT/LOC</ta>
            <ta e="T101" id="Seg_2075" s="T100">вода.[NOM]</ta>
            <ta e="T102" id="Seg_2076" s="T101">черпать-CVB.PURP</ta>
            <ta e="T103" id="Seg_2077" s="T102">загладывать-PST1-3SG</ta>
            <ta e="T104" id="Seg_2078" s="T103">видеть-PST2-3SG</ta>
            <ta e="T105" id="Seg_2079" s="T104">луна.[NOM]</ta>
            <ta e="T106" id="Seg_2080" s="T105">тень-3SG.[NOM]</ta>
            <ta e="T107" id="Seg_2081" s="T106">очень</ta>
            <ta e="T108" id="Seg_2082" s="T107">близкий-INTNS.[NOM]</ta>
            <ta e="T109" id="Seg_2083" s="T108">быть.видно-PRS.[3SG]</ta>
            <ta e="T110" id="Seg_2084" s="T109">прорубь-ACC</ta>
            <ta e="T111" id="Seg_2085" s="T110">полный</ta>
            <ta e="T112" id="Seg_2086" s="T111">MOD</ta>
            <ta e="T113" id="Seg_2087" s="T112">человек.[NOM]</ta>
            <ta e="T114" id="Seg_2088" s="T113">EMPH</ta>
            <ta e="T115" id="Seg_2089" s="T114">слово-3SG-ACC</ta>
            <ta e="T116" id="Seg_2090" s="T115">слышать-PTCP.PRS.[NOM]</ta>
            <ta e="T117" id="Seg_2091" s="T116">подобно</ta>
            <ta e="T118" id="Seg_2092" s="T117">Аныс</ta>
            <ta e="T119" id="Seg_2093" s="T118">полнолуние-ACC</ta>
            <ta e="T120" id="Seg_2094" s="T119">видеть-CVB.SEQ</ta>
            <ta e="T121" id="Seg_2095" s="T120">душа.[NOM]</ta>
            <ta e="T122" id="Seg_2096" s="T121">нутро-3SG-ABL</ta>
            <ta e="T123" id="Seg_2097" s="T122">вырывать-CVB.SIM</ta>
            <ta e="T124" id="Seg_2098" s="T123">плакать-PST1-3SG</ta>
            <ta e="T125" id="Seg_2099" s="T124">луна-ACC</ta>
            <ta e="T126" id="Seg_2100" s="T125">с</ta>
            <ta e="T127" id="Seg_2101" s="T126">разговаривать-CVB.SIM-разговаривать-CVB.SIM</ta>
            <ta e="T128" id="Seg_2102" s="T127">собака-COMP</ta>
            <ta e="T129" id="Seg_2103" s="T128">EMPH</ta>
            <ta e="T130" id="Seg_2104" s="T129">плохой-ADVZ</ta>
            <ta e="T131" id="Seg_2105" s="T130">жить-PRS-1SG</ta>
            <ta e="T132" id="Seg_2106" s="T131">говорить-PRS.[3SG]</ta>
            <ta e="T133" id="Seg_2107" s="T132">человек.[NOM]</ta>
            <ta e="T134" id="Seg_2108" s="T133">каждый-3SG.[NOM]</ta>
            <ta e="T135" id="Seg_2109" s="T134">1SG-ACC</ta>
            <ta e="T136" id="Seg_2110" s="T135">обижать-PRS.[3SG]</ta>
            <ta e="T137" id="Seg_2111" s="T136">нормальный-SIM</ta>
            <ta e="T138" id="Seg_2112" s="T137">NEG</ta>
            <ta e="T139" id="Seg_2113" s="T138">есть-EP-CAUS-NEG-3PL</ta>
            <ta e="T140" id="Seg_2114" s="T139">добрый</ta>
            <ta e="T141" id="Seg_2115" s="T140">одежда-POSS</ta>
            <ta e="T142" id="Seg_2116" s="T141">NEG-1SG</ta>
            <ta e="T143" id="Seg_2117" s="T142">каждый-3SG.[NOM]</ta>
            <ta e="T144" id="Seg_2118" s="T143">кусок.[NOM]</ta>
            <ta e="T145" id="Seg_2119" s="T144">очень</ta>
            <ta e="T146" id="Seg_2120" s="T145">мерзнуть-PRS-1SG</ta>
            <ta e="T147" id="Seg_2121" s="T146">одеяло-POSS</ta>
            <ta e="T148" id="Seg_2122" s="T147">NEG</ta>
            <ta e="T149" id="Seg_2123" s="T148">NEG-1SG</ta>
            <ta e="T150" id="Seg_2124" s="T149">одежда-COM</ta>
            <ta e="T151" id="Seg_2125" s="T150">спать-PRS-1SG</ta>
            <ta e="T152" id="Seg_2126" s="T151">человек-ABL</ta>
            <ta e="T153" id="Seg_2127" s="T152">когда</ta>
            <ta e="T154" id="Seg_2128" s="T153">NEG</ta>
            <ta e="T155" id="Seg_2129" s="T154">добрый-ACC</ta>
            <ta e="T156" id="Seg_2130" s="T155">видеть-NEG.PTCP.[NOM]</ta>
            <ta e="T157" id="Seg_2131" s="T156">как-1SG</ta>
            <ta e="T158" id="Seg_2132" s="T157">взять.[IMP.2SG]</ta>
            <ta e="T159" id="Seg_2133" s="T158">1SG-ACC</ta>
            <ta e="T160" id="Seg_2134" s="T159">луна.[NOM]</ta>
            <ta e="T161" id="Seg_2135" s="T160">MOD</ta>
            <ta e="T162" id="Seg_2136" s="T161">луна.[NOM]</ta>
            <ta e="T163" id="Seg_2137" s="T162">девушка.[NOM]</ta>
            <ta e="T164" id="Seg_2138" s="T163">ребенок-ACC</ta>
            <ta e="T165" id="Seg_2139" s="T164">жалеть-CVB.SEQ</ta>
            <ta e="T166" id="Seg_2140" s="T165">взять-PST2.[3SG]</ta>
            <ta e="T167" id="Seg_2141" s="T166">тот.EMPH.[NOM]</ta>
            <ta e="T168" id="Seg_2142" s="T167">час-ABL</ta>
            <ta e="T169" id="Seg_2143" s="T168">месяц-ACC</ta>
            <ta e="T170" id="Seg_2144" s="T169">наблюдать-PTCP.COND-DAT/LOC</ta>
            <ta e="T171" id="Seg_2145" s="T170">человек.[NOM]</ta>
            <ta e="T172" id="Seg_2146" s="T171">видеть-PRS.[3SG]</ta>
            <ta e="T173" id="Seg_2147" s="T172">девушка.[NOM]</ta>
            <ta e="T174" id="Seg_2148" s="T173">два</ta>
            <ta e="T175" id="Seg_2149" s="T174">ведро-PROPR</ta>
            <ta e="T176" id="Seg_2150" s="T175">вода.[NOM]</ta>
            <ta e="T177" id="Seg_2151" s="T176">черпать-CVB.SEQ</ta>
            <ta e="T178" id="Seg_2152" s="T177">стоять-PRS.[3SG]</ta>
            <ta e="T179" id="Seg_2153" s="T178">ночью</ta>
            <ta e="T180" id="Seg_2154" s="T179">полнолуние.[NOM]</ta>
            <ta e="T181" id="Seg_2155" s="T180">быть-TEMP-3SG</ta>
            <ta e="T182" id="Seg_2156" s="T181">собака-PL.[NOM]</ta>
            <ta e="T183" id="Seg_2157" s="T182">луна.[NOM]</ta>
            <ta e="T184" id="Seg_2158" s="T183">к</ta>
            <ta e="T185" id="Seg_2159" s="T184">поднимать.голову-CVB.SEQ</ta>
            <ta e="T186" id="Seg_2160" s="T185">после</ta>
            <ta e="T187" id="Seg_2161" s="T186">выть-PRS-3PL</ta>
            <ta e="T188" id="Seg_2162" s="T187">3PL.[NOM]</ta>
            <ta e="T189" id="Seg_2163" s="T188">узнавать-PRS-3PL</ta>
            <ta e="T190" id="Seg_2164" s="T189">Аныс-ACC</ta>
            <ta e="T191" id="Seg_2165" s="T190">теперь-DAT/LOC</ta>
            <ta e="T192" id="Seg_2166" s="T191">пока</ta>
            <ta e="T193" id="Seg_2167" s="T192">всегда</ta>
            <ta e="T194" id="Seg_2168" s="T193">скучать-PRS-3PL</ta>
            <ta e="T195" id="Seg_2169" s="T194">сказка-VBZ-PST2-3SG</ta>
            <ta e="T196" id="Seg_2170" s="T195">Огдуо</ta>
            <ta e="T197" id="Seg_2171" s="T196">Аксенова.[NOM]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_2172" s="T1">propr</ta>
            <ta e="T3" id="Seg_2173" s="T2">n.[n:case]</ta>
            <ta e="T4" id="Seg_2174" s="T3">dempro</ta>
            <ta e="T5" id="Seg_2175" s="T4">adj</ta>
            <ta e="T6" id="Seg_2176" s="T5">n-n:case</ta>
            <ta e="T7" id="Seg_2177" s="T6">n.[n:case]</ta>
            <ta e="T8" id="Seg_2178" s="T7">propr</ta>
            <ta e="T9" id="Seg_2179" s="T8">n.[n:case]</ta>
            <ta e="T10" id="Seg_2180" s="T9">n.[n:case]</ta>
            <ta e="T11" id="Seg_2181" s="T10">v-v:ptcp-v&gt;adj.[n:case]</ta>
            <ta e="T12" id="Seg_2182" s="T11">adj-n:(num)-n:case</ta>
            <ta e="T13" id="Seg_2183" s="T12">n.[n:case]</ta>
            <ta e="T14" id="Seg_2184" s="T13">v-v:cvb</ta>
            <ta e="T15" id="Seg_2185" s="T14">v-v:(ins)-v:tense.[v:pred.pn]</ta>
            <ta e="T16" id="Seg_2186" s="T15">dempro-pro:(poss)-pro:(poss).[pro:case]</ta>
            <ta e="T17" id="Seg_2187" s="T16">adv</ta>
            <ta e="T18" id="Seg_2188" s="T17">v-v:ptcp</ta>
            <ta e="T19" id="Seg_2189" s="T18">v-v:tense-v:pred.pn</ta>
            <ta e="T20" id="Seg_2190" s="T19">v-v:tense.[v:pred.pn]</ta>
            <ta e="T21" id="Seg_2191" s="T20">adj</ta>
            <ta e="T22" id="Seg_2192" s="T21">n-n:case</ta>
            <ta e="T23" id="Seg_2193" s="T22">n-n:case</ta>
            <ta e="T24" id="Seg_2194" s="T23">post</ta>
            <ta e="T25" id="Seg_2195" s="T24">cardnum</ta>
            <ta e="T26" id="Seg_2196" s="T25">n-n&gt;adj</ta>
            <ta e="T27" id="Seg_2197" s="T26">n.[n:case]</ta>
            <ta e="T28" id="Seg_2198" s="T27">v-v:tense.[v:pred.pn]</ta>
            <ta e="T29" id="Seg_2199" s="T28">adv</ta>
            <ta e="T30" id="Seg_2200" s="T29">n-n:case</ta>
            <ta e="T31" id="Seg_2201" s="T30">v-v:cvb</ta>
            <ta e="T32" id="Seg_2202" s="T31">post</ta>
            <ta e="T33" id="Seg_2203" s="T32">v-v:cvb</ta>
            <ta e="T34" id="Seg_2204" s="T33">v-v:tense.[v:pred.pn]</ta>
            <ta e="T35" id="Seg_2205" s="T34">adv</ta>
            <ta e="T36" id="Seg_2206" s="T35">n-n&gt;v-v:tense.[v:pred.pn]</ta>
            <ta e="T37" id="Seg_2207" s="T36">adv</ta>
            <ta e="T38" id="Seg_2208" s="T37">v-v:tense.[v:pred.pn]</ta>
            <ta e="T39" id="Seg_2209" s="T38">adv</ta>
            <ta e="T40" id="Seg_2210" s="T39">conj</ta>
            <ta e="T41" id="Seg_2211" s="T40">adv</ta>
            <ta e="T42" id="Seg_2212" s="T41">conj</ta>
            <ta e="T43" id="Seg_2213" s="T42">n.[n:case]</ta>
            <ta e="T44" id="Seg_2214" s="T43">n.[n:case]</ta>
            <ta e="T45" id="Seg_2215" s="T44">v-v:(neg).[v:pred.pn]</ta>
            <ta e="T46" id="Seg_2216" s="T45">adv</ta>
            <ta e="T47" id="Seg_2217" s="T46">v-v:tense.[v:pred.pn]</ta>
            <ta e="T48" id="Seg_2218" s="T47">adv</ta>
            <ta e="T49" id="Seg_2219" s="T48">n-n:case</ta>
            <ta e="T50" id="Seg_2220" s="T49">adj.[n:case]</ta>
            <ta e="T51" id="Seg_2221" s="T50">n.[n:case]</ta>
            <ta e="T52" id="Seg_2222" s="T51">v-v:(ins)-v:(neg).[v:pred.pn]</ta>
            <ta e="T53" id="Seg_2223" s="T52">n.[n:case]</ta>
            <ta e="T54" id="Seg_2224" s="T53">ptcl</ta>
            <ta e="T55" id="Seg_2225" s="T54">n-n:case</ta>
            <ta e="T56" id="Seg_2226" s="T55">v-v:(ins)-v&gt;v-v:tense.[v:pred.pn]</ta>
            <ta e="T57" id="Seg_2227" s="T56">n.[n:case]</ta>
            <ta e="T58" id="Seg_2228" s="T57">n-n:case</ta>
            <ta e="T59" id="Seg_2229" s="T58">n.[n:case]</ta>
            <ta e="T60" id="Seg_2230" s="T59">post</ta>
            <ta e="T61" id="Seg_2231" s="T60">adj.[n:case]</ta>
            <ta e="T62" id="Seg_2232" s="T61">n-n:case</ta>
            <ta e="T63" id="Seg_2233" s="T62">v-v&gt;v-v:(neg).[v:pred.pn]</ta>
            <ta e="T64" id="Seg_2234" s="T63">pers.[pro:case]</ta>
            <ta e="T65" id="Seg_2235" s="T64">n.[n:case]</ta>
            <ta e="T66" id="Seg_2236" s="T65">n-n:case</ta>
            <ta e="T67" id="Seg_2237" s="T66">ptcl</ta>
            <ta e="T68" id="Seg_2238" s="T67">v-v:(ins)-v&gt;v-v:tense.[v:pred.pn]</ta>
            <ta e="T69" id="Seg_2239" s="T68">n.[n:case]</ta>
            <ta e="T70" id="Seg_2240" s="T69">n.[n:case]</ta>
            <ta e="T71" id="Seg_2241" s="T70">n-n:poss-n:case</ta>
            <ta e="T72" id="Seg_2242" s="T71">adv</ta>
            <ta e="T73" id="Seg_2243" s="T72">adj.[n:case]</ta>
            <ta e="T74" id="Seg_2244" s="T73">n.[n:case]</ta>
            <ta e="T75" id="Seg_2245" s="T74">v-v:(ins)-v&gt;v-v:ptcp</ta>
            <ta e="T76" id="Seg_2246" s="T75">v-v:cvb</ta>
            <ta e="T77" id="Seg_2247" s="T76">n.[n:case]</ta>
            <ta e="T78" id="Seg_2248" s="T77">n.[n:case]</ta>
            <ta e="T79" id="Seg_2249" s="T78">n-n:(poss).[n:case]</ta>
            <ta e="T80" id="Seg_2250" s="T79">n.[n:case]</ta>
            <ta e="T81" id="Seg_2251" s="T80">n-n:poss-n:case</ta>
            <ta e="T82" id="Seg_2252" s="T81">v-v&gt;v-v:mood.[v:pred.pn]</ta>
            <ta e="T83" id="Seg_2253" s="T82">propr.[n:case]</ta>
            <ta e="T84" id="Seg_2254" s="T83">ptcl</ta>
            <ta e="T85" id="Seg_2255" s="T84">v-v:tense.[v:pred.pn]</ta>
            <ta e="T86" id="Seg_2256" s="T85">pers.[pro:case]</ta>
            <ta e="T87" id="Seg_2257" s="T86">n-n:case</ta>
            <ta e="T88" id="Seg_2258" s="T87">n-n:case</ta>
            <ta e="T89" id="Seg_2259" s="T88">v-v:tense.[v:pred.pn]</ta>
            <ta e="T90" id="Seg_2260" s="T89">n-n:case</ta>
            <ta e="T91" id="Seg_2261" s="T90">propr.[n:case]</ta>
            <ta e="T92" id="Seg_2262" s="T91">n-n:(num)-n:poss-n:case</ta>
            <ta e="T93" id="Seg_2263" s="T92">v-v:cvb</ta>
            <ta e="T94" id="Seg_2264" s="T93">post</ta>
            <ta e="T95" id="Seg_2265" s="T94">n-n:poss-n:case</ta>
            <ta e="T96" id="Seg_2266" s="T95">v-v:tense-v:poss.pn</ta>
            <ta e="T97" id="Seg_2267" s="T96">adj-adj&gt;adv</ta>
            <ta e="T98" id="Seg_2268" s="T97">v-v:mood-v:temp.pn</ta>
            <ta e="T99" id="Seg_2269" s="T98">v-v:(neg).[v:pred.pn]</ta>
            <ta e="T100" id="Seg_2270" s="T99">n-n:case</ta>
            <ta e="T101" id="Seg_2271" s="T100">n.[n:case]</ta>
            <ta e="T102" id="Seg_2272" s="T101">v-v:cvb</ta>
            <ta e="T103" id="Seg_2273" s="T102">v-v:tense-v:poss.pn</ta>
            <ta e="T104" id="Seg_2274" s="T103">v-v:tense-v:poss.pn</ta>
            <ta e="T105" id="Seg_2275" s="T104">n.[n:case]</ta>
            <ta e="T106" id="Seg_2276" s="T105">n-n:(poss).[n:case]</ta>
            <ta e="T107" id="Seg_2277" s="T106">adv</ta>
            <ta e="T108" id="Seg_2278" s="T107">adj-adj&gt;adj.[n:case]</ta>
            <ta e="T109" id="Seg_2279" s="T108">v-v:tense.[v:pred.pn]</ta>
            <ta e="T110" id="Seg_2280" s="T109">n-n:case</ta>
            <ta e="T111" id="Seg_2281" s="T110">adj</ta>
            <ta e="T112" id="Seg_2282" s="T111">ptcl</ta>
            <ta e="T113" id="Seg_2283" s="T112">n.[n:case]</ta>
            <ta e="T114" id="Seg_2284" s="T113">ptcl</ta>
            <ta e="T115" id="Seg_2285" s="T114">n-n:poss-n:case</ta>
            <ta e="T116" id="Seg_2286" s="T115">v-v:ptcp.[v:(case)]</ta>
            <ta e="T117" id="Seg_2287" s="T116">post</ta>
            <ta e="T118" id="Seg_2288" s="T117">propr</ta>
            <ta e="T119" id="Seg_2289" s="T118">n-n:case</ta>
            <ta e="T120" id="Seg_2290" s="T119">v-v:cvb</ta>
            <ta e="T121" id="Seg_2291" s="T120">n.[n:case]</ta>
            <ta e="T122" id="Seg_2292" s="T121">n-n:poss-n:case</ta>
            <ta e="T123" id="Seg_2293" s="T122">v-v:cvb</ta>
            <ta e="T124" id="Seg_2294" s="T123">v-v:tense-v:poss.pn</ta>
            <ta e="T125" id="Seg_2295" s="T124">n-n:case</ta>
            <ta e="T126" id="Seg_2296" s="T125">post</ta>
            <ta e="T127" id="Seg_2297" s="T126">v-v:cvb-v-v:cvb</ta>
            <ta e="T128" id="Seg_2298" s="T127">n-n:case</ta>
            <ta e="T129" id="Seg_2299" s="T128">ptcl</ta>
            <ta e="T130" id="Seg_2300" s="T129">adj-adj&gt;adv</ta>
            <ta e="T131" id="Seg_2301" s="T130">v-v:tense-v:pred.pn</ta>
            <ta e="T132" id="Seg_2302" s="T131">v-v:tense.[v:pred.pn]</ta>
            <ta e="T133" id="Seg_2303" s="T132">n.[n:case]</ta>
            <ta e="T134" id="Seg_2304" s="T133">adj-n:(poss).[n:case]</ta>
            <ta e="T135" id="Seg_2305" s="T134">pers-pro:case</ta>
            <ta e="T136" id="Seg_2306" s="T135">v-v:tense.[v:pred.pn]</ta>
            <ta e="T137" id="Seg_2307" s="T136">adj-adj&gt;adv</ta>
            <ta e="T138" id="Seg_2308" s="T137">ptcl</ta>
            <ta e="T139" id="Seg_2309" s="T138">v-v:(ins)-v&gt;v-v:(neg)-v:pred.pn</ta>
            <ta e="T140" id="Seg_2310" s="T139">adj</ta>
            <ta e="T141" id="Seg_2311" s="T140">n-n:(poss)</ta>
            <ta e="T142" id="Seg_2312" s="T141">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T143" id="Seg_2313" s="T142">adj-n:(poss).[n:case]</ta>
            <ta e="T144" id="Seg_2314" s="T143">n.[n:case]</ta>
            <ta e="T145" id="Seg_2315" s="T144">adv</ta>
            <ta e="T146" id="Seg_2316" s="T145">v-v:tense-v:pred.pn</ta>
            <ta e="T147" id="Seg_2317" s="T146">n-n:(poss)</ta>
            <ta e="T148" id="Seg_2318" s="T147">ptcl</ta>
            <ta e="T149" id="Seg_2319" s="T148">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T150" id="Seg_2320" s="T149">n-n:case</ta>
            <ta e="T151" id="Seg_2321" s="T150">v-v:tense-v:pred.pn</ta>
            <ta e="T152" id="Seg_2322" s="T151">n-n:case</ta>
            <ta e="T153" id="Seg_2323" s="T152">que</ta>
            <ta e="T154" id="Seg_2324" s="T153">ptcl</ta>
            <ta e="T155" id="Seg_2325" s="T154">adj-n:case</ta>
            <ta e="T156" id="Seg_2326" s="T155">v-v:ptcp.[v:(case)]</ta>
            <ta e="T157" id="Seg_2327" s="T156">post-n:(pred.pn)</ta>
            <ta e="T158" id="Seg_2328" s="T157">v.[v:mood.pn]</ta>
            <ta e="T159" id="Seg_2329" s="T158">pers-pro:case</ta>
            <ta e="T160" id="Seg_2330" s="T159">n.[n:case]</ta>
            <ta e="T161" id="Seg_2331" s="T160">ptcl</ta>
            <ta e="T162" id="Seg_2332" s="T161">n.[n:case]</ta>
            <ta e="T163" id="Seg_2333" s="T162">n.[n:case]</ta>
            <ta e="T164" id="Seg_2334" s="T163">n-n:case</ta>
            <ta e="T165" id="Seg_2335" s="T164">v-v:cvb</ta>
            <ta e="T166" id="Seg_2336" s="T165">v-v:tense.[v:pred.pn]</ta>
            <ta e="T167" id="Seg_2337" s="T166">dempro.[pro:case]</ta>
            <ta e="T168" id="Seg_2338" s="T167">n-n:case</ta>
            <ta e="T169" id="Seg_2339" s="T168">n-n:case</ta>
            <ta e="T170" id="Seg_2340" s="T169">v-v:ptcp-v:(case)</ta>
            <ta e="T171" id="Seg_2341" s="T170">n.[n:case]</ta>
            <ta e="T172" id="Seg_2342" s="T171">v-v:tense.[v:pred.pn]</ta>
            <ta e="T173" id="Seg_2343" s="T172">n.[n:case]</ta>
            <ta e="T174" id="Seg_2344" s="T173">cardnum</ta>
            <ta e="T175" id="Seg_2345" s="T174">n-n&gt;adj</ta>
            <ta e="T176" id="Seg_2346" s="T175">n.[n:case]</ta>
            <ta e="T177" id="Seg_2347" s="T176">v-v:cvb</ta>
            <ta e="T178" id="Seg_2348" s="T177">v-v:tense.[v:pred.pn]</ta>
            <ta e="T179" id="Seg_2349" s="T178">adv</ta>
            <ta e="T180" id="Seg_2350" s="T179">n.[n:case]</ta>
            <ta e="T181" id="Seg_2351" s="T180">v-v:mood-v:temp.pn</ta>
            <ta e="T182" id="Seg_2352" s="T181">n-n:(num).[n:case]</ta>
            <ta e="T183" id="Seg_2353" s="T182">n.[n:case]</ta>
            <ta e="T184" id="Seg_2354" s="T183">post</ta>
            <ta e="T185" id="Seg_2355" s="T184">v-v:cvb</ta>
            <ta e="T186" id="Seg_2356" s="T185">post</ta>
            <ta e="T187" id="Seg_2357" s="T186">v-v:tense-v:pred.pn</ta>
            <ta e="T188" id="Seg_2358" s="T187">pers.[pro:case]</ta>
            <ta e="T189" id="Seg_2359" s="T188">v-v:tense-v:pred.pn</ta>
            <ta e="T190" id="Seg_2360" s="T189">propr-n:case</ta>
            <ta e="T191" id="Seg_2361" s="T190">adv-n:case</ta>
            <ta e="T192" id="Seg_2362" s="T191">post</ta>
            <ta e="T193" id="Seg_2363" s="T192">adv</ta>
            <ta e="T194" id="Seg_2364" s="T193">v-v:tense-v:pred.pn</ta>
            <ta e="T195" id="Seg_2365" s="T194">n-n&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T196" id="Seg_2366" s="T195">propr</ta>
            <ta e="T197" id="Seg_2367" s="T196">propr.[n:case]</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_2368" s="T1">propr</ta>
            <ta e="T3" id="Seg_2369" s="T2">n</ta>
            <ta e="T4" id="Seg_2370" s="T3">dempro</ta>
            <ta e="T5" id="Seg_2371" s="T4">adj</ta>
            <ta e="T6" id="Seg_2372" s="T5">n</ta>
            <ta e="T7" id="Seg_2373" s="T6">n</ta>
            <ta e="T8" id="Seg_2374" s="T7">propr</ta>
            <ta e="T9" id="Seg_2375" s="T8">n</ta>
            <ta e="T10" id="Seg_2376" s="T9">n</ta>
            <ta e="T11" id="Seg_2377" s="T10">adj</ta>
            <ta e="T12" id="Seg_2378" s="T11">n</ta>
            <ta e="T13" id="Seg_2379" s="T12">n</ta>
            <ta e="T14" id="Seg_2380" s="T13">cop</ta>
            <ta e="T15" id="Seg_2381" s="T14">aux</ta>
            <ta e="T16" id="Seg_2382" s="T15">dempro</ta>
            <ta e="T17" id="Seg_2383" s="T16">adv</ta>
            <ta e="T18" id="Seg_2384" s="T17">v</ta>
            <ta e="T19" id="Seg_2385" s="T18">aux</ta>
            <ta e="T20" id="Seg_2386" s="T19">aux</ta>
            <ta e="T21" id="Seg_2387" s="T20">adj</ta>
            <ta e="T22" id="Seg_2388" s="T21">n</ta>
            <ta e="T23" id="Seg_2389" s="T22">n</ta>
            <ta e="T24" id="Seg_2390" s="T23">post</ta>
            <ta e="T25" id="Seg_2391" s="T24">cardnum</ta>
            <ta e="T26" id="Seg_2392" s="T25">adj</ta>
            <ta e="T27" id="Seg_2393" s="T26">n</ta>
            <ta e="T28" id="Seg_2394" s="T27">v</ta>
            <ta e="T29" id="Seg_2395" s="T28">adv</ta>
            <ta e="T30" id="Seg_2396" s="T29">n</ta>
            <ta e="T31" id="Seg_2397" s="T30">v</ta>
            <ta e="T32" id="Seg_2398" s="T31">post</ta>
            <ta e="T33" id="Seg_2399" s="T32">v</ta>
            <ta e="T34" id="Seg_2400" s="T33">v</ta>
            <ta e="T35" id="Seg_2401" s="T34">adv</ta>
            <ta e="T36" id="Seg_2402" s="T35">v</ta>
            <ta e="T37" id="Seg_2403" s="T36">adv</ta>
            <ta e="T38" id="Seg_2404" s="T37">v</ta>
            <ta e="T39" id="Seg_2405" s="T38">adv</ta>
            <ta e="T40" id="Seg_2406" s="T39">conj</ta>
            <ta e="T41" id="Seg_2407" s="T40">adv</ta>
            <ta e="T42" id="Seg_2408" s="T41">conj</ta>
            <ta e="T43" id="Seg_2409" s="T42">n</ta>
            <ta e="T44" id="Seg_2410" s="T43">n</ta>
            <ta e="T45" id="Seg_2411" s="T44">v</ta>
            <ta e="T46" id="Seg_2412" s="T45">adv</ta>
            <ta e="T47" id="Seg_2413" s="T46">v</ta>
            <ta e="T48" id="Seg_2414" s="T47">adv</ta>
            <ta e="T49" id="Seg_2415" s="T48">n</ta>
            <ta e="T50" id="Seg_2416" s="T49">adj</ta>
            <ta e="T51" id="Seg_2417" s="T50">n</ta>
            <ta e="T52" id="Seg_2418" s="T51">v</ta>
            <ta e="T53" id="Seg_2419" s="T52">n</ta>
            <ta e="T54" id="Seg_2420" s="T53">ptcl</ta>
            <ta e="T55" id="Seg_2421" s="T54">n</ta>
            <ta e="T56" id="Seg_2422" s="T55">v</ta>
            <ta e="T57" id="Seg_2423" s="T56">n</ta>
            <ta e="T58" id="Seg_2424" s="T57">n</ta>
            <ta e="T59" id="Seg_2425" s="T58">n</ta>
            <ta e="T60" id="Seg_2426" s="T59">post</ta>
            <ta e="T61" id="Seg_2427" s="T60">adj</ta>
            <ta e="T62" id="Seg_2428" s="T61">n</ta>
            <ta e="T63" id="Seg_2429" s="T62">v</ta>
            <ta e="T64" id="Seg_2430" s="T63">pers</ta>
            <ta e="T65" id="Seg_2431" s="T64">n</ta>
            <ta e="T66" id="Seg_2432" s="T65">n</ta>
            <ta e="T67" id="Seg_2433" s="T66">ptcl</ta>
            <ta e="T68" id="Seg_2434" s="T67">v</ta>
            <ta e="T69" id="Seg_2435" s="T68">n</ta>
            <ta e="T70" id="Seg_2436" s="T69">n</ta>
            <ta e="T71" id="Seg_2437" s="T70">n</ta>
            <ta e="T72" id="Seg_2438" s="T71">adv</ta>
            <ta e="T73" id="Seg_2439" s="T72">adj</ta>
            <ta e="T74" id="Seg_2440" s="T73">n</ta>
            <ta e="T75" id="Seg_2441" s="T74">v</ta>
            <ta e="T76" id="Seg_2442" s="T75">aux</ta>
            <ta e="T77" id="Seg_2443" s="T76">n</ta>
            <ta e="T78" id="Seg_2444" s="T77">n</ta>
            <ta e="T79" id="Seg_2445" s="T78">n</ta>
            <ta e="T80" id="Seg_2446" s="T79">n</ta>
            <ta e="T81" id="Seg_2447" s="T80">n</ta>
            <ta e="T82" id="Seg_2448" s="T81">v</ta>
            <ta e="T83" id="Seg_2449" s="T82">propr</ta>
            <ta e="T84" id="Seg_2450" s="T83">ptcl</ta>
            <ta e="T85" id="Seg_2451" s="T84">v</ta>
            <ta e="T86" id="Seg_2452" s="T85">pers</ta>
            <ta e="T87" id="Seg_2453" s="T86">n</ta>
            <ta e="T88" id="Seg_2454" s="T87">n</ta>
            <ta e="T89" id="Seg_2455" s="T88">v</ta>
            <ta e="T90" id="Seg_2456" s="T89">n</ta>
            <ta e="T91" id="Seg_2457" s="T90">propr</ta>
            <ta e="T92" id="Seg_2458" s="T91">n</ta>
            <ta e="T93" id="Seg_2459" s="T92">v</ta>
            <ta e="T94" id="Seg_2460" s="T93">post</ta>
            <ta e="T95" id="Seg_2461" s="T94">n</ta>
            <ta e="T96" id="Seg_2462" s="T95">v</ta>
            <ta e="T97" id="Seg_2463" s="T96">adv</ta>
            <ta e="T98" id="Seg_2464" s="T97">v</ta>
            <ta e="T99" id="Seg_2465" s="T98">v</ta>
            <ta e="T100" id="Seg_2466" s="T99">n</ta>
            <ta e="T101" id="Seg_2467" s="T100">n</ta>
            <ta e="T102" id="Seg_2468" s="T101">v</ta>
            <ta e="T103" id="Seg_2469" s="T102">v</ta>
            <ta e="T104" id="Seg_2470" s="T103">v</ta>
            <ta e="T105" id="Seg_2471" s="T104">n</ta>
            <ta e="T106" id="Seg_2472" s="T105">n</ta>
            <ta e="T107" id="Seg_2473" s="T106">adv</ta>
            <ta e="T108" id="Seg_2474" s="T107">adj</ta>
            <ta e="T109" id="Seg_2475" s="T108">v</ta>
            <ta e="T110" id="Seg_2476" s="T109">n</ta>
            <ta e="T111" id="Seg_2477" s="T110">adj</ta>
            <ta e="T112" id="Seg_2478" s="T111">ptcl</ta>
            <ta e="T113" id="Seg_2479" s="T112">n</ta>
            <ta e="T114" id="Seg_2480" s="T113">ptcl</ta>
            <ta e="T115" id="Seg_2481" s="T114">n</ta>
            <ta e="T116" id="Seg_2482" s="T115">v</ta>
            <ta e="T117" id="Seg_2483" s="T116">post</ta>
            <ta e="T118" id="Seg_2484" s="T117">propr</ta>
            <ta e="T119" id="Seg_2485" s="T118">n</ta>
            <ta e="T120" id="Seg_2486" s="T119">v</ta>
            <ta e="T121" id="Seg_2487" s="T120">n</ta>
            <ta e="T122" id="Seg_2488" s="T121">n</ta>
            <ta e="T123" id="Seg_2489" s="T122">v</ta>
            <ta e="T124" id="Seg_2490" s="T123">v</ta>
            <ta e="T125" id="Seg_2491" s="T124">n</ta>
            <ta e="T126" id="Seg_2492" s="T125">post</ta>
            <ta e="T127" id="Seg_2493" s="T126">v</ta>
            <ta e="T128" id="Seg_2494" s="T127">n</ta>
            <ta e="T129" id="Seg_2495" s="T128">ptcl</ta>
            <ta e="T130" id="Seg_2496" s="T129">adv</ta>
            <ta e="T131" id="Seg_2497" s="T130">v</ta>
            <ta e="T132" id="Seg_2498" s="T131">v</ta>
            <ta e="T133" id="Seg_2499" s="T132">n</ta>
            <ta e="T134" id="Seg_2500" s="T133">adj</ta>
            <ta e="T135" id="Seg_2501" s="T134">pers</ta>
            <ta e="T136" id="Seg_2502" s="T135">v</ta>
            <ta e="T137" id="Seg_2503" s="T136">adv</ta>
            <ta e="T138" id="Seg_2504" s="T137">ptcl</ta>
            <ta e="T139" id="Seg_2505" s="T138">v</ta>
            <ta e="T140" id="Seg_2506" s="T139">adj</ta>
            <ta e="T141" id="Seg_2507" s="T140">n</ta>
            <ta e="T142" id="Seg_2508" s="T141">ptcl</ta>
            <ta e="T143" id="Seg_2509" s="T142">n</ta>
            <ta e="T144" id="Seg_2510" s="T143">n</ta>
            <ta e="T145" id="Seg_2511" s="T144">adv</ta>
            <ta e="T146" id="Seg_2512" s="T145">v</ta>
            <ta e="T147" id="Seg_2513" s="T146">n</ta>
            <ta e="T148" id="Seg_2514" s="T147">ptcl</ta>
            <ta e="T149" id="Seg_2515" s="T148">ptcl</ta>
            <ta e="T150" id="Seg_2516" s="T149">n</ta>
            <ta e="T151" id="Seg_2517" s="T150">v</ta>
            <ta e="T152" id="Seg_2518" s="T151">n</ta>
            <ta e="T153" id="Seg_2519" s="T152">que</ta>
            <ta e="T154" id="Seg_2520" s="T153">ptcl</ta>
            <ta e="T155" id="Seg_2521" s="T154">n</ta>
            <ta e="T156" id="Seg_2522" s="T155">v</ta>
            <ta e="T157" id="Seg_2523" s="T156">post</ta>
            <ta e="T158" id="Seg_2524" s="T157">v</ta>
            <ta e="T159" id="Seg_2525" s="T158">pers</ta>
            <ta e="T160" id="Seg_2526" s="T159">n</ta>
            <ta e="T161" id="Seg_2527" s="T160">ptcl</ta>
            <ta e="T162" id="Seg_2528" s="T161">n</ta>
            <ta e="T163" id="Seg_2529" s="T162">n</ta>
            <ta e="T164" id="Seg_2530" s="T163">n</ta>
            <ta e="T165" id="Seg_2531" s="T164">v</ta>
            <ta e="T166" id="Seg_2532" s="T165">v</ta>
            <ta e="T167" id="Seg_2533" s="T166">dempro</ta>
            <ta e="T168" id="Seg_2534" s="T167">n</ta>
            <ta e="T169" id="Seg_2535" s="T168">n</ta>
            <ta e="T170" id="Seg_2536" s="T169">v</ta>
            <ta e="T171" id="Seg_2537" s="T170">n</ta>
            <ta e="T172" id="Seg_2538" s="T171">v</ta>
            <ta e="T173" id="Seg_2539" s="T172">n</ta>
            <ta e="T174" id="Seg_2540" s="T173">cardnum</ta>
            <ta e="T175" id="Seg_2541" s="T174">adj</ta>
            <ta e="T176" id="Seg_2542" s="T175">n</ta>
            <ta e="T177" id="Seg_2543" s="T176">v</ta>
            <ta e="T178" id="Seg_2544" s="T177">aux</ta>
            <ta e="T179" id="Seg_2545" s="T178">adv</ta>
            <ta e="T180" id="Seg_2546" s="T179">n</ta>
            <ta e="T181" id="Seg_2547" s="T180">cop</ta>
            <ta e="T182" id="Seg_2548" s="T181">n</ta>
            <ta e="T183" id="Seg_2549" s="T182">n</ta>
            <ta e="T184" id="Seg_2550" s="T183">post</ta>
            <ta e="T185" id="Seg_2551" s="T184">v</ta>
            <ta e="T186" id="Seg_2552" s="T185">post</ta>
            <ta e="T187" id="Seg_2553" s="T186">v</ta>
            <ta e="T188" id="Seg_2554" s="T187">pers</ta>
            <ta e="T189" id="Seg_2555" s="T188">v</ta>
            <ta e="T190" id="Seg_2556" s="T189">propr</ta>
            <ta e="T191" id="Seg_2557" s="T190">adv</ta>
            <ta e="T192" id="Seg_2558" s="T191">post</ta>
            <ta e="T193" id="Seg_2559" s="T192">adv</ta>
            <ta e="T194" id="Seg_2560" s="T193">v</ta>
            <ta e="T195" id="Seg_2561" s="T194">v</ta>
            <ta e="T196" id="Seg_2562" s="T195">propr</ta>
            <ta e="T197" id="Seg_2563" s="T196">propr</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T6" id="Seg_2564" s="T5">np:L</ta>
            <ta e="T10" id="Seg_2565" s="T9">np.h:Th</ta>
            <ta e="T12" id="Seg_2566" s="T11">np:L</ta>
            <ta e="T15" id="Seg_2567" s="T13">0.3.h:Th</ta>
            <ta e="T16" id="Seg_2568" s="T15">pro.h:A</ta>
            <ta e="T22" id="Seg_2569" s="T21">n:Time</ta>
            <ta e="T24" id="Seg_2570" s="T22">pp:Time</ta>
            <ta e="T27" id="Seg_2571" s="T26">np:Th</ta>
            <ta e="T28" id="Seg_2572" s="T27">0.3.h:A</ta>
            <ta e="T29" id="Seg_2573" s="T28">adv:Time</ta>
            <ta e="T30" id="Seg_2574" s="T29">np:P</ta>
            <ta e="T34" id="Seg_2575" s="T33">0.3.h:A</ta>
            <ta e="T35" id="Seg_2576" s="T34">adv:Time</ta>
            <ta e="T36" id="Seg_2577" s="T35">0.3.h:A</ta>
            <ta e="T37" id="Seg_2578" s="T36">adv:Time</ta>
            <ta e="T38" id="Seg_2579" s="T37">0.3.h:A</ta>
            <ta e="T39" id="Seg_2580" s="T38">adv:Time</ta>
            <ta e="T41" id="Seg_2581" s="T40">adv:Time</ta>
            <ta e="T45" id="Seg_2582" s="T44">0.3.h:A</ta>
            <ta e="T46" id="Seg_2583" s="T45">adv:Time</ta>
            <ta e="T47" id="Seg_2584" s="T46">0.3:Th</ta>
            <ta e="T48" id="Seg_2585" s="T47">adv:Time</ta>
            <ta e="T49" id="Seg_2586" s="T48">np:L</ta>
            <ta e="T51" id="Seg_2587" s="T50">np:Th</ta>
            <ta e="T53" id="Seg_2588" s="T52">np:Cau</ta>
            <ta e="T55" id="Seg_2589" s="T54">np:Th</ta>
            <ta e="T57" id="Seg_2590" s="T56">np:Th</ta>
            <ta e="T58" id="Seg_2591" s="T57">np:L</ta>
            <ta e="T62" id="Seg_2592" s="T61">np:P</ta>
            <ta e="T63" id="Seg_2593" s="T62">0.3:Cau</ta>
            <ta e="T64" id="Seg_2594" s="T63">pro:Cau</ta>
            <ta e="T66" id="Seg_2595" s="T65">np:Th</ta>
            <ta e="T71" id="Seg_2596" s="T70">np:L</ta>
            <ta e="T72" id="Seg_2597" s="T71">adv:Time</ta>
            <ta e="T74" id="Seg_2598" s="T73">np:P</ta>
            <ta e="T78" id="Seg_2599" s="T77">np:Poss</ta>
            <ta e="T79" id="Seg_2600" s="T78">np:Th</ta>
            <ta e="T83" id="Seg_2601" s="T82">np.h:E</ta>
            <ta e="T86" id="Seg_2602" s="T85">pro.h:Th</ta>
            <ta e="T87" id="Seg_2603" s="T86">np:L</ta>
            <ta e="T90" id="Seg_2604" s="T89">n:Time</ta>
            <ta e="T91" id="Seg_2605" s="T90">np.h:A</ta>
            <ta e="T92" id="Seg_2606" s="T91">0.3.h:Poss np:Th</ta>
            <ta e="T95" id="Seg_2607" s="T94">np:G</ta>
            <ta e="T98" id="Seg_2608" s="T97">0.3.h:A</ta>
            <ta e="T99" id="Seg_2609" s="T98">0.3.h:E</ta>
            <ta e="T100" id="Seg_2610" s="T99">np:G</ta>
            <ta e="T101" id="Seg_2611" s="T100">np:Th</ta>
            <ta e="T103" id="Seg_2612" s="T102">0.3.h:A</ta>
            <ta e="T104" id="Seg_2613" s="T103">0.3.h:A</ta>
            <ta e="T106" id="Seg_2614" s="T105">np:Th</ta>
            <ta e="T118" id="Seg_2615" s="T117">np.h:A</ta>
            <ta e="T119" id="Seg_2616" s="T118">np:Th</ta>
            <ta e="T126" id="Seg_2617" s="T124">pp:Com</ta>
            <ta e="T131" id="Seg_2618" s="T130">0.1.h:Th</ta>
            <ta e="T132" id="Seg_2619" s="T131">0.3.h:A</ta>
            <ta e="T133" id="Seg_2620" s="T132">np.h:A</ta>
            <ta e="T135" id="Seg_2621" s="T134">pro.h:P</ta>
            <ta e="T139" id="Seg_2622" s="T138">0.3.h:A</ta>
            <ta e="T141" id="Seg_2623" s="T140">np:Th</ta>
            <ta e="T142" id="Seg_2624" s="T141">0.1.h:Poss</ta>
            <ta e="T143" id="Seg_2625" s="T142">np:Th</ta>
            <ta e="T146" id="Seg_2626" s="T145">0.1.h:E</ta>
            <ta e="T147" id="Seg_2627" s="T146">np:Th</ta>
            <ta e="T149" id="Seg_2628" s="T148">0.1.h:Poss</ta>
            <ta e="T151" id="Seg_2629" s="T150">0.1.h:Th</ta>
            <ta e="T155" id="Seg_2630" s="T154">np:St</ta>
            <ta e="T157" id="Seg_2631" s="T155">0.1.h:E</ta>
            <ta e="T158" id="Seg_2632" s="T157">0.2.h:A</ta>
            <ta e="T159" id="Seg_2633" s="T158">pro.h:Th</ta>
            <ta e="T162" id="Seg_2634" s="T161">np.h:A</ta>
            <ta e="T164" id="Seg_2635" s="T163">np.h:Th</ta>
            <ta e="T168" id="Seg_2636" s="T167">n:Time</ta>
            <ta e="T169" id="Seg_2637" s="T168">np:Th</ta>
            <ta e="T171" id="Seg_2638" s="T170">np.h:E</ta>
            <ta e="T173" id="Seg_2639" s="T172">np.h:A</ta>
            <ta e="T176" id="Seg_2640" s="T175">np:Th</ta>
            <ta e="T179" id="Seg_2641" s="T178">adv:Time</ta>
            <ta e="T180" id="Seg_2642" s="T179">np:Th</ta>
            <ta e="T182" id="Seg_2643" s="T181">np:A</ta>
            <ta e="T184" id="Seg_2644" s="T182">pp:G</ta>
            <ta e="T188" id="Seg_2645" s="T187">pro:E</ta>
            <ta e="T190" id="Seg_2646" s="T189">np.h:Th</ta>
            <ta e="T192" id="Seg_2647" s="T190">pp:Time</ta>
            <ta e="T193" id="Seg_2648" s="T192">adv:Time</ta>
            <ta e="T194" id="Seg_2649" s="T193">0.3.h:E</ta>
            <ta e="T197" id="Seg_2650" s="T196">np.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T13" id="Seg_2651" s="T12">n:pred</ta>
            <ta e="T15" id="Seg_2652" s="T13">0.3.h:S cop</ta>
            <ta e="T16" id="Seg_2653" s="T15">pro.h:S</ta>
            <ta e="T19" id="Seg_2654" s="T17">v:pred</ta>
            <ta e="T27" id="Seg_2655" s="T26">np:O</ta>
            <ta e="T28" id="Seg_2656" s="T27">0.3.h:S v:pred</ta>
            <ta e="T32" id="Seg_2657" s="T29">s:temp</ta>
            <ta e="T34" id="Seg_2658" s="T33">0.3.h:S v:pred</ta>
            <ta e="T36" id="Seg_2659" s="T35">0.3.h:S v:pred</ta>
            <ta e="T38" id="Seg_2660" s="T37">0.3.h:S v:pred</ta>
            <ta e="T45" id="Seg_2661" s="T44">0.3.h:S v:pred</ta>
            <ta e="T47" id="Seg_2662" s="T46">0.3:S v:pred</ta>
            <ta e="T50" id="Seg_2663" s="T49">adj:pred</ta>
            <ta e="T51" id="Seg_2664" s="T50">np:S</ta>
            <ta e="T52" id="Seg_2665" s="T51">v:pred</ta>
            <ta e="T53" id="Seg_2666" s="T52">np:S</ta>
            <ta e="T55" id="Seg_2667" s="T54">np:O</ta>
            <ta e="T56" id="Seg_2668" s="T55">v:pred</ta>
            <ta e="T57" id="Seg_2669" s="T56">np:S</ta>
            <ta e="T61" id="Seg_2670" s="T60">adj:pred</ta>
            <ta e="T62" id="Seg_2671" s="T61">np:O</ta>
            <ta e="T63" id="Seg_2672" s="T62">0.3:S v:pred</ta>
            <ta e="T64" id="Seg_2673" s="T63">pro:S</ta>
            <ta e="T66" id="Seg_2674" s="T65">np:O</ta>
            <ta e="T68" id="Seg_2675" s="T67">v:pred</ta>
            <ta e="T73" id="Seg_2676" s="T72">adj:pred</ta>
            <ta e="T76" id="Seg_2677" s="T73">s:adv</ta>
            <ta e="T79" id="Seg_2678" s="T78">np:S</ta>
            <ta e="T82" id="Seg_2679" s="T81">v:pred</ta>
            <ta e="T83" id="Seg_2680" s="T82">np.h:S</ta>
            <ta e="T85" id="Seg_2681" s="T84">v:pred</ta>
            <ta e="T86" id="Seg_2682" s="T85">pro.h:S</ta>
            <ta e="T89" id="Seg_2683" s="T88">v:pred</ta>
            <ta e="T91" id="Seg_2684" s="T90">np.h:S</ta>
            <ta e="T94" id="Seg_2685" s="T91">s:temp</ta>
            <ta e="T96" id="Seg_2686" s="T95">v:pred</ta>
            <ta e="T98" id="Seg_2687" s="T96">s:cond</ta>
            <ta e="T99" id="Seg_2688" s="T98">0.3.h:S v:pred</ta>
            <ta e="T102" id="Seg_2689" s="T100">s:purp</ta>
            <ta e="T103" id="Seg_2690" s="T102">0.3.h:S v:pred</ta>
            <ta e="T104" id="Seg_2691" s="T103">0.3.h:S v:pred</ta>
            <ta e="T106" id="Seg_2692" s="T105">np:S</ta>
            <ta e="T109" id="Seg_2693" s="T108">v:pred</ta>
            <ta e="T118" id="Seg_2694" s="T117">np.h:S</ta>
            <ta e="T120" id="Seg_2695" s="T118">s:temp</ta>
            <ta e="T124" id="Seg_2696" s="T123">v:pred</ta>
            <ta e="T127" id="Seg_2697" s="T124">s:adv</ta>
            <ta e="T131" id="Seg_2698" s="T130">0.1.h:S v:pred</ta>
            <ta e="T132" id="Seg_2699" s="T131">0.3.h:S v:pred</ta>
            <ta e="T133" id="Seg_2700" s="T132">np.h:S</ta>
            <ta e="T135" id="Seg_2701" s="T134">pro.h:O</ta>
            <ta e="T136" id="Seg_2702" s="T135">v:pred</ta>
            <ta e="T139" id="Seg_2703" s="T138">0.3.h:S v:pred</ta>
            <ta e="T142" id="Seg_2704" s="T141">0.1.h:S ptcl:pred</ta>
            <ta e="T143" id="Seg_2705" s="T142">np:S</ta>
            <ta e="T144" id="Seg_2706" s="T143">n:pred</ta>
            <ta e="T146" id="Seg_2707" s="T145">0.1.h:S v:pred</ta>
            <ta e="T149" id="Seg_2708" s="T148">0.1.h:S ptcl:pred</ta>
            <ta e="T151" id="Seg_2709" s="T150">0.1.h:S v:pred</ta>
            <ta e="T155" id="Seg_2710" s="T154">np:O</ta>
            <ta e="T157" id="Seg_2711" s="T155">0.1.h:S v:pred</ta>
            <ta e="T158" id="Seg_2712" s="T157">0.2.h:S v:pred</ta>
            <ta e="T159" id="Seg_2713" s="T158">pro.h:O</ta>
            <ta e="T162" id="Seg_2714" s="T161">np.h:S</ta>
            <ta e="T165" id="Seg_2715" s="T162">s:temp</ta>
            <ta e="T166" id="Seg_2716" s="T165">v:pred</ta>
            <ta e="T170" id="Seg_2717" s="T166">s:temp</ta>
            <ta e="T171" id="Seg_2718" s="T170">np.h:S</ta>
            <ta e="T172" id="Seg_2719" s="T171">v:pred</ta>
            <ta e="T173" id="Seg_2720" s="T172">np.h:S</ta>
            <ta e="T176" id="Seg_2721" s="T175">np:O</ta>
            <ta e="T178" id="Seg_2722" s="T176">v:pred</ta>
            <ta e="T181" id="Seg_2723" s="T179">s:temp</ta>
            <ta e="T182" id="Seg_2724" s="T181">np:S</ta>
            <ta e="T186" id="Seg_2725" s="T182">s:temp</ta>
            <ta e="T187" id="Seg_2726" s="T186">v:pred</ta>
            <ta e="T188" id="Seg_2727" s="T187">pro:S</ta>
            <ta e="T189" id="Seg_2728" s="T188">v:pred</ta>
            <ta e="T190" id="Seg_2729" s="T189">np.h:O</ta>
            <ta e="T194" id="Seg_2730" s="T193">0.3.h:S v:pred</ta>
            <ta e="T195" id="Seg_2731" s="T194">v:pred</ta>
            <ta e="T197" id="Seg_2732" s="T196">np.h:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="Top" tierref="Top" />
         <annotation name="Foc" tierref="Foc" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T30" id="Seg_2733" s="T29">EV:cult</ta>
            <ta e="T40" id="Seg_2734" s="T39">RUS:gram</ta>
            <ta e="T42" id="Seg_2735" s="T41">RUS:gram</ta>
            <ta e="T74" id="Seg_2736" s="T73">RUS:cult</ta>
            <ta e="T79" id="Seg_2737" s="T78">EV:cult</ta>
            <ta e="T108" id="Seg_2738" s="T107">EV:gram (DIM)</ta>
            <ta e="T193" id="Seg_2739" s="T192">RUS:core</ta>
            <ta e="T197" id="Seg_2740" s="T196">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T3" id="Seg_2741" s="T1">The girl Anys.</ta>
            <ta e="T15" id="Seg_2742" s="T3">In the Middle World, there lived the orphan girl Anys, she was a slave of rich people.</ta>
            <ta e="T20" id="Seg_2743" s="T15">Those ones apparently treated it very badly.</ta>
            <ta e="T28" id="Seg_2744" s="T20">From early [lit. black] morning until the evening she scoops water with two buckets.</ta>
            <ta e="T34" id="Seg_2745" s="T28">In summer she plucks dwarf birches and carries them on her back here.</ta>
            <ta e="T38" id="Seg_2746" s="T34">In winter she gathers wood and chops it.</ta>
            <ta e="T47" id="Seg_2747" s="T38">If she doesn't struggle with water and wood in both winter and summer, it runs immediately out.</ta>
            <ta e="T50" id="Seg_2748" s="T47">In winter it is dark in the tundra.</ta>
            <ta e="T56" id="Seg_2749" s="T50">The sun doesn't come out, only the full moon lights the snow.</ta>
            <ta e="T63" id="Seg_2750" s="T56">The moon in the sky is cold as ice, it doesn't warm up the earth.</ta>
            <ta e="T68" id="Seg_2751" s="T63">It only lights the sledge house.</ta>
            <ta e="T76" id="Seg_2752" s="T68">Inside the sledge house it is warm by day, because the stove is heated.</ta>
            <ta e="T82" id="Seg_2753" s="T76">The cover of the sledge house is made of reindeer fur.</ta>
            <ta e="T89" id="Seg_2754" s="T82">Only Anys is freezing, she sleeps on the floor with cloths on.</ta>
            <ta e="T96" id="Seg_2755" s="T89">In the evening Anys took her buckets and went to the ice hole.</ta>
            <ta e="T99" id="Seg_2756" s="T96">If she goes quickly, she doesn't freeze.</ta>
            <ta e="T103" id="Seg_2757" s="T99">She looked into the icehole in order to scoop water.</ta>
            <ta e="T117" id="Seg_2758" s="T103">She looked, the shadow of the moon is visible very closely, it fills(?) the icehole, as if a human voice was heard.</ta>
            <ta e="T127" id="Seg_2759" s="T117">Anys looked at the full moon, she cried, weeped herself out, talking with the moon.</ta>
            <ta e="T132" id="Seg_2760" s="T127">"I'm living even worse than a dog", she says.</ta>
            <ta e="T136" id="Seg_2761" s="T132">All people are offending me.</ta>
            <ta e="T144" id="Seg_2762" s="T136">"They don't feed [me] well, I don't have good clothes, everything is torn.</ta>
            <ta e="T157" id="Seg_2763" s="T144">I'm freezing heavily, I don't even have a blanket; I sleep with cloths on, I don't see anything good from the people.</ta>
            <ta e="T161" id="Seg_2764" s="T157">Take me, dear moon!"</ta>
            <ta e="T166" id="Seg_2765" s="T161">The moon felt sorry for the girl and took it.</ta>
            <ta e="T172" id="Seg_2766" s="T166">Since then, when a human watches the moon, he sees:</ta>
            <ta e="T178" id="Seg_2767" s="T172">A girl with two buckets is scooping water.</ta>
            <ta e="T187" id="Seg_2768" s="T178">In the night, when there is a full moon, dogs raise their head towards the moon and howl.</ta>
            <ta e="T194" id="Seg_2769" s="T187">They recognize Anys, they miss [her] until today.</ta>
            <ta e="T197" id="Seg_2770" s="T194">The tale was told by Ogduo Aksyonova.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T3" id="Seg_2771" s="T1">Das Mädchen Anys.</ta>
            <ta e="T15" id="Seg_2772" s="T3">In der Mittleren Welt lebte das Waisenmädchen Anys, es war eine Sklavin bei reichen Leuten.</ta>
            <ta e="T20" id="Seg_2773" s="T15">Jene behandelten es offenbar sehr schlecht.</ta>
            <ta e="T28" id="Seg_2774" s="T20">Vom frühen [wörtl. schwarzen] Morgen bis zum Abend schöpft es mit zwei Eimern Wasser.</ta>
            <ta e="T34" id="Seg_2775" s="T28">Im Sommer reißt sie Zwergbirken aus und trägt sie auf dem Rücken her.</ta>
            <ta e="T38" id="Seg_2776" s="T34">Im Winter sammelt sie Holz und hackt es.</ta>
            <ta e="T47" id="Seg_2777" s="T38">Wenn sie sich winters wie sommers nicht mit Wasser und Holz quält, geht es sofort zuende.</ta>
            <ta e="T50" id="Seg_2778" s="T47">Im Winter ist es in der Tundra dunkel.</ta>
            <ta e="T56" id="Seg_2779" s="T50">Die Sonne kommt nicht heraus, nur der Vollmond erleuchtet den Schnee.</ta>
            <ta e="T63" id="Seg_2780" s="T56">Der Mond am Himmel ist kalt wie Eis, er erwärmt die Erde nicht.</ta>
            <ta e="T68" id="Seg_2781" s="T63">Er erleuchtet nur das Schlittenhaus.</ta>
            <ta e="T76" id="Seg_2782" s="T68">Im Schlittenhaus ist es am Tag warm, weil der Ofen geheizt wird.</ta>
            <ta e="T82" id="Seg_2783" s="T76">Die Abdeckung des Schlittenhauses ist aus Rentierfell gemacht.</ta>
            <ta e="T89" id="Seg_2784" s="T82">Nur Anys friert, sie schläft in Kleidung auf dem Boden.</ta>
            <ta e="T96" id="Seg_2785" s="T89">Am Abend nahm Anys ihre Eimer und ging zum Eisloch.</ta>
            <ta e="T99" id="Seg_2786" s="T96">Wenn sie schnell geht, friert sie nicht.</ta>
            <ta e="T103" id="Seg_2787" s="T99">Sie blickte ins Eisloch, um Wasser zu schöpfen.</ta>
            <ta e="T117" id="Seg_2788" s="T103">Sie schaute, der Schatten des Mondes ist sehr nahe zu sehen, er füllt(?) das Eisloch, als ob eine menschliche Stimme zu hören sei.</ta>
            <ta e="T127" id="Seg_2789" s="T117">Anys schaute den Vollmond an, weinte sich die Seele aus, wobei sie sich mit dem Mond unterhielt.</ta>
            <ta e="T132" id="Seg_2790" s="T127">"Ich lebe sogar schlechter als ein Hund", sagt sie.</ta>
            <ta e="T136" id="Seg_2791" s="T132">Alle Leute beleidigen mich.</ta>
            <ta e="T144" id="Seg_2792" s="T136">"Sie geben [mir] nicht gut zu essen, ich habe keine gute Kleidung, es ist alles zerrissen.</ta>
            <ta e="T157" id="Seg_2793" s="T144">Ich friere sehr, ich habe nicht einmal eine Bettdecke; ich schlafe mit Kleidung, von den Menschen sehe ich nichts Gutes.</ta>
            <ta e="T161" id="Seg_2794" s="T157">Nimm mich, lieber Mond!"</ta>
            <ta e="T166" id="Seg_2795" s="T161">Dem Mond tat das Mädchen leid und er nahm es.</ta>
            <ta e="T172" id="Seg_2796" s="T166">Wenn ein Mensch seitdem den Mond anguckt, sieht er:</ta>
            <ta e="T178" id="Seg_2797" s="T172">Ein Mädchen mit zwei Eimern schöpft Wasser.</ta>
            <ta e="T187" id="Seg_2798" s="T178">Nachts, wenn Vollmond ist, heben die Hunde ihren Kopf zum Mond und heulen.</ta>
            <ta e="T194" id="Seg_2799" s="T187">Sie erkennen Anys, bis heute vermissen sie [sie].</ta>
            <ta e="T197" id="Seg_2800" s="T194">Das Märchen erzählte Ogduo Aksjonova.</ta>
         </annotation>
         <annotation name="fr" tierref="fr" />
         <annotation name="ltr" tierref="ltr">
            <ta e="T3" id="Seg_2801" s="T1">Аныс девочка.</ta>
            <ta e="T15" id="Seg_2802" s="T3">На этой средней земле сиротка Аныс девочка жила, богатым слугой была.</ta>
            <ta e="T20" id="Seg_2803" s="T15">А те очень обижали, оказывается. </ta>
            <ta e="T28" id="Seg_2804" s="T20">С ранего утра до вечера с двумя ведрами воду набирает.</ta>
            <ta e="T34" id="Seg_2805" s="T28">Летом карликовую берёзу собрав, на спине приносит.</ta>
            <ta e="T38" id="Seg_2806" s="T34">Зимой дрова собирает, потом рубит. </ta>
            <ta e="T47" id="Seg_2807" s="T38">И зимой, и летом воды и дров не напасёшься, сразу заканчивается.</ta>
            <ta e="T50" id="Seg_2808" s="T47">Зимой в тундре темно.</ta>
            <ta e="T56" id="Seg_2809" s="T50">Солнце не выходит, луна только снег светлее делает.</ta>
            <ta e="T63" id="Seg_2810" s="T56">Луна на небе как лёд холодная, землю не согревает. </ta>
            <ta e="T68" id="Seg_2811" s="T63">Она болок только освещает.</ta>
            <ta e="T76" id="Seg_2812" s="T68">Внутри болка днём тепло, печка топится потому что. </ta>
            <ta e="T82" id="Seg_2813" s="T76">В балке нюки (покрытие) из шкуры оленя делается. </ta>
            <ta e="T89" id="Seg_2814" s="T82">Аныс только мёрзнет, она на полу в одежде спит. </ta>
            <ta e="T96" id="Seg_2815" s="T89">Вечером Аныс вёдра забрав, на лунку пошла, </ta>
            <ta e="T99" id="Seg_2816" s="T96">быстро когда идёт, не мёрзнет.</ta>
            <ta e="T103" id="Seg_2817" s="T99">Из лунки воду чтобы набрать пристально вгляделась. </ta>
            <ta e="T117" id="Seg_2818" s="T103">Посмотрела, тень луны была очень близко виднеется, лунку всю наполнила, человека слова слышать может. </ta>
            <ta e="T127" id="Seg_2819" s="T117">Аныс на полнолуние глядя, вывернув всю душу, плакала, с луной разговаривая.</ta>
            <ta e="T132" id="Seg_2820" s="T127">"Даже хуже чем собака живу", – говорит, –</ta>
            <ta e="T136" id="Seg_2821" s="T132">Все люди меня обижают.</ta>
            <ta e="T144" id="Seg_2822" s="T136">и даже хорошо не кормят, хорошей одежды нет, всё рваное.</ta>
            <ta e="T157" id="Seg_2823" s="T144">очень мёрзну, одеяла даже нету – в одежде сплю, от человека никогда хорошего не могу увидеть, </ta>
            <ta e="T161" id="Seg_2824" s="T157">забери меня, милая луна!"</ta>
            <ta e="T166" id="Seg_2825" s="T161">Луна девочку пожелев взяла. </ta>
            <ta e="T172" id="Seg_2826" s="T166">После этого луну разглядывая, человек видит: </ta>
            <ta e="T178" id="Seg_2827" s="T172">девочка с двумя вёдрами воду набирает.</ta>
            <ta e="T187" id="Seg_2828" s="T178">Ночью, когда полнолуние, собаки, на луну смотря, воют.</ta>
            <ta e="T194" id="Seg_2829" s="T187">Гни узнают Аныс, до сих пор скучают. </ta>
            <ta e="T197" id="Seg_2830" s="T194">Сказку рассказала Огдуо Аксёнова.</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg"
                          name="gg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top"
                          name="Top"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc"
                          name="Foc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
