<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID90827484-D014-05A7-A69D-D0C4ACD2619C">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>SaSS_1964_NganasanBraveBoy_flk</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\flk\SaSS_1964_NganasanBraveBoy_flk\SaSS_1964_NganasanBraveBoy_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">1189</ud-information>
            <ud-information attribute-name="# HIAT:w">849</ud-information>
            <ud-information attribute-name="# e">849</ud-information>
            <ud-information attribute-name="# HIAT:u">170</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="SaSS">
            <abbreviation>SaSS</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="4.0" type="appl" />
         <tli id="T1" time="4.5" type="appl" />
         <tli id="T2" time="5.0" type="appl" />
         <tli id="T3" time="5.5" type="appl" />
         <tli id="T4" time="6.0" type="appl" />
         <tli id="T5" time="6.5" type="appl" />
         <tli id="T6" time="7.0" type="appl" />
         <tli id="T7" time="7.5" type="appl" />
         <tli id="T8" time="8.0" type="appl" />
         <tli id="T9" time="8.5" type="appl" />
         <tli id="T10" time="9.0" type="appl" />
         <tli id="T11" time="9.5" type="appl" />
         <tli id="T12" time="10.0" type="appl" />
         <tli id="T13" time="10.5" type="appl" />
         <tli id="T14" time="11.0" type="appl" />
         <tli id="T15" time="11.5" type="appl" />
         <tli id="T16" time="12.0" type="appl" />
         <tli id="T17" time="12.5" type="appl" />
         <tli id="T18" time="13.0" type="appl" />
         <tli id="T19" time="13.5" type="appl" />
         <tli id="T20" time="14.0" type="appl" />
         <tli id="T21" time="14.5" type="appl" />
         <tli id="T22" time="15.0" type="appl" />
         <tli id="T23" time="15.5" type="appl" />
         <tli id="T24" time="16.0" type="appl" />
         <tli id="T25" time="16.5" type="appl" />
         <tli id="T26" time="17.0" type="appl" />
         <tli id="T27" time="17.5" type="appl" />
         <tli id="T28" time="18.0" type="appl" />
         <tli id="T29" time="18.5" type="appl" />
         <tli id="T30" time="19.0" type="appl" />
         <tli id="T31" time="19.5" type="appl" />
         <tli id="T32" time="20.0" type="appl" />
         <tli id="T33" time="20.5" type="appl" />
         <tli id="T34" time="21.0" type="appl" />
         <tli id="T35" time="21.5" type="appl" />
         <tli id="T36" time="22.0" type="appl" />
         <tli id="T37" time="22.5" type="appl" />
         <tli id="T38" time="23.0" type="appl" />
         <tli id="T39" time="23.5" type="appl" />
         <tli id="T40" time="24.0" type="appl" />
         <tli id="T41" time="24.5" type="appl" />
         <tli id="T42" time="25.0" type="appl" />
         <tli id="T43" time="25.5" type="appl" />
         <tli id="T44" time="26.0" type="appl" />
         <tli id="T45" time="26.5" type="appl" />
         <tli id="T46" time="27.0" type="appl" />
         <tli id="T47" time="27.5" type="appl" />
         <tli id="T48" time="28.0" type="appl" />
         <tli id="T49" time="28.5" type="appl" />
         <tli id="T50" time="29.0" type="appl" />
         <tli id="T51" time="29.5" type="appl" />
         <tli id="T52" time="30.0" type="appl" />
         <tli id="T53" time="30.5" type="appl" />
         <tli id="T54" time="31.0" type="appl" />
         <tli id="T55" time="31.5" type="appl" />
         <tli id="T56" time="32.0" type="appl" />
         <tli id="T57" time="32.5" type="appl" />
         <tli id="T58" time="33.0" type="appl" />
         <tli id="T59" time="33.5" type="appl" />
         <tli id="T60" time="34.0" type="appl" />
         <tli id="T61" time="34.5" type="appl" />
         <tli id="T62" time="35.0" type="appl" />
         <tli id="T63" time="35.5" type="appl" />
         <tli id="T64" time="36.0" type="appl" />
         <tli id="T65" time="36.5" type="appl" />
         <tli id="T66" time="37.0" type="appl" />
         <tli id="T67" time="37.5" type="appl" />
         <tli id="T68" time="38.0" type="appl" />
         <tli id="T69" time="38.5" type="appl" />
         <tli id="T70" time="39.0" type="appl" />
         <tli id="T71" time="39.5" type="appl" />
         <tli id="T72" time="40.0" type="appl" />
         <tli id="T73" time="40.5" type="appl" />
         <tli id="T74" time="41.0" type="appl" />
         <tli id="T75" time="41.5" type="appl" />
         <tli id="T76" time="42.0" type="appl" />
         <tli id="T77" time="42.5" type="appl" />
         <tli id="T78" time="43.0" type="appl" />
         <tli id="T79" time="43.5" type="appl" />
         <tli id="T80" time="44.0" type="appl" />
         <tli id="T81" time="44.5" type="appl" />
         <tli id="T82" time="45.0" type="appl" />
         <tli id="T83" time="45.5" type="appl" />
         <tli id="T84" time="46.0" type="appl" />
         <tli id="T85" time="46.5" type="appl" />
         <tli id="T86" time="47.0" type="appl" />
         <tli id="T87" time="47.5" type="appl" />
         <tli id="T88" time="48.0" type="appl" />
         <tli id="T89" time="48.5" type="appl" />
         <tli id="T90" time="49.0" type="appl" />
         <tli id="T91" time="49.5" type="appl" />
         <tli id="T92" time="50.0" type="appl" />
         <tli id="T93" time="50.5" type="appl" />
         <tli id="T94" time="51.0" type="appl" />
         <tli id="T95" time="51.5" type="appl" />
         <tli id="T96" time="52.0" type="appl" />
         <tli id="T97" time="52.5" type="appl" />
         <tli id="T98" time="53.0" type="appl" />
         <tli id="T99" time="53.5" type="appl" />
         <tli id="T100" time="54.0" type="appl" />
         <tli id="T101" time="54.5" type="appl" />
         <tli id="T102" time="55.0" type="appl" />
         <tli id="T103" time="55.5" type="appl" />
         <tli id="T104" time="56.0" type="appl" />
         <tli id="T105" time="56.5" type="appl" />
         <tli id="T106" time="57.0" type="appl" />
         <tli id="T107" time="57.5" type="appl" />
         <tli id="T108" time="58.0" type="appl" />
         <tli id="T109" time="58.5" type="appl" />
         <tli id="T110" time="59.0" type="appl" />
         <tli id="T111" time="59.5" type="appl" />
         <tli id="T112" time="60.0" type="appl" />
         <tli id="T113" time="60.5" type="appl" />
         <tli id="T114" time="61.0" type="appl" />
         <tli id="T115" time="61.5" type="appl" />
         <tli id="T116" time="62.0" type="appl" />
         <tli id="T117" time="62.5" type="appl" />
         <tli id="T118" time="63.0" type="appl" />
         <tli id="T119" time="63.5" type="appl" />
         <tli id="T120" time="64.0" type="appl" />
         <tli id="T121" time="64.5" type="appl" />
         <tli id="T122" time="65.0" type="appl" />
         <tli id="T123" time="65.5" type="appl" />
         <tli id="T124" time="66.0" type="appl" />
         <tli id="T125" time="66.5" type="appl" />
         <tli id="T126" time="67.0" type="appl" />
         <tli id="T127" time="67.5" type="appl" />
         <tli id="T128" time="68.0" type="appl" />
         <tli id="T129" time="68.5" type="appl" />
         <tli id="T130" time="69.0" type="appl" />
         <tli id="T131" time="69.5" type="appl" />
         <tli id="T132" time="70.0" type="appl" />
         <tli id="T133" time="70.5" type="appl" />
         <tli id="T134" time="71.0" type="appl" />
         <tli id="T135" time="71.5" type="appl" />
         <tli id="T136" time="72.0" type="appl" />
         <tli id="T137" time="72.5" type="appl" />
         <tli id="T138" time="73.0" type="appl" />
         <tli id="T139" time="73.5" type="appl" />
         <tli id="T140" time="74.0" type="appl" />
         <tli id="T141" time="74.5" type="appl" />
         <tli id="T142" time="75.0" type="appl" />
         <tli id="T143" time="75.5" type="appl" />
         <tli id="T144" time="76.0" type="appl" />
         <tli id="T145" time="76.5" type="appl" />
         <tli id="T146" time="77.0" type="appl" />
         <tli id="T147" time="77.5" type="appl" />
         <tli id="T148" time="78.0" type="appl" />
         <tli id="T149" time="78.5" type="appl" />
         <tli id="T150" time="79.0" type="appl" />
         <tli id="T151" time="79.5" type="appl" />
         <tli id="T152" time="80.0" type="appl" />
         <tli id="T153" time="80.5" type="appl" />
         <tli id="T154" time="81.0" type="appl" />
         <tli id="T155" time="81.5" type="appl" />
         <tli id="T156" time="82.0" type="appl" />
         <tli id="T157" time="82.5" type="appl" />
         <tli id="T158" time="83.0" type="appl" />
         <tli id="T159" time="83.5" type="appl" />
         <tli id="T160" time="84.0" type="appl" />
         <tli id="T161" time="84.5" type="appl" />
         <tli id="T162" time="85.0" type="appl" />
         <tli id="T163" time="85.5" type="appl" />
         <tli id="T164" time="86.0" type="appl" />
         <tli id="T165" time="86.5" type="appl" />
         <tli id="T166" time="87.0" type="appl" />
         <tli id="T167" time="87.5" type="appl" />
         <tli id="T168" time="88.0" type="appl" />
         <tli id="T169" time="88.5" type="appl" />
         <tli id="T170" time="89.0" type="appl" />
         <tli id="T171" time="89.5" type="appl" />
         <tli id="T172" time="90.0" type="appl" />
         <tli id="T173" time="90.5" type="appl" />
         <tli id="T174" time="91.0" type="appl" />
         <tli id="T175" time="91.5" type="appl" />
         <tli id="T176" time="92.0" type="appl" />
         <tli id="T177" time="92.5" type="appl" />
         <tli id="T178" time="93.0" type="appl" />
         <tli id="T179" time="93.5" type="appl" />
         <tli id="T180" time="94.0" type="appl" />
         <tli id="T181" time="94.5" type="appl" />
         <tli id="T182" time="95.0" type="appl" />
         <tli id="T183" time="95.5" type="appl" />
         <tli id="T184" time="96.0" type="appl" />
         <tli id="T185" time="96.5" type="appl" />
         <tli id="T186" time="97.0" type="appl" />
         <tli id="T187" time="97.5" type="appl" />
         <tli id="T188" time="98.0" type="appl" />
         <tli id="T189" time="98.5" type="appl" />
         <tli id="T190" time="99.0" type="appl" />
         <tli id="T191" time="99.5" type="appl" />
         <tli id="T192" time="100.0" type="appl" />
         <tli id="T193" time="100.5" type="appl" />
         <tli id="T194" time="101.0" type="appl" />
         <tli id="T195" time="101.5" type="appl" />
         <tli id="T196" time="102.0" type="appl" />
         <tli id="T197" time="102.5" type="appl" />
         <tli id="T198" time="103.0" type="appl" />
         <tli id="T199" time="103.5" type="appl" />
         <tli id="T200" time="104.0" type="appl" />
         <tli id="T201" time="104.5" type="appl" />
         <tli id="T202" time="105.0" type="appl" />
         <tli id="T203" time="105.5" type="appl" />
         <tli id="T204" time="106.0" type="appl" />
         <tli id="T205" time="106.5" type="appl" />
         <tli id="T206" time="107.0" type="appl" />
         <tli id="T207" time="107.5" type="appl" />
         <tli id="T208" time="108.0" type="appl" />
         <tli id="T209" time="108.5" type="appl" />
         <tli id="T210" time="109.0" type="appl" />
         <tli id="T211" time="109.5" type="appl" />
         <tli id="T212" time="110.0" type="appl" />
         <tli id="T213" time="110.5" type="appl" />
         <tli id="T214" time="111.0" type="appl" />
         <tli id="T215" time="111.5" type="appl" />
         <tli id="T216" time="112.0" type="appl" />
         <tli id="T217" time="112.5" type="appl" />
         <tli id="T218" time="113.0" type="appl" />
         <tli id="T219" time="113.5" type="appl" />
         <tli id="T220" time="114.0" type="appl" />
         <tli id="T221" time="114.5" type="appl" />
         <tli id="T222" time="115.0" type="appl" />
         <tli id="T223" time="115.5" type="appl" />
         <tli id="T224" time="116.0" type="appl" />
         <tli id="T225" time="116.5" type="appl" />
         <tli id="T226" time="117.0" type="appl" />
         <tli id="T227" time="117.5" type="appl" />
         <tli id="T228" time="118.0" type="appl" />
         <tli id="T229" time="118.5" type="appl" />
         <tli id="T230" time="119.0" type="appl" />
         <tli id="T231" time="119.5" type="appl" />
         <tli id="T232" time="120.0" type="appl" />
         <tli id="T233" time="120.5" type="appl" />
         <tli id="T234" time="121.0" type="appl" />
         <tli id="T235" time="121.5" type="appl" />
         <tli id="T236" time="122.0" type="appl" />
         <tli id="T237" time="122.5" type="appl" />
         <tli id="T238" time="123.0" type="appl" />
         <tli id="T239" time="123.5" type="appl" />
         <tli id="T240" time="124.0" type="appl" />
         <tli id="T241" time="124.5" type="appl" />
         <tli id="T242" time="125.0" type="appl" />
         <tli id="T243" time="125.5" type="appl" />
         <tli id="T244" time="126.0" type="appl" />
         <tli id="T245" time="126.5" type="appl" />
         <tli id="T246" time="127.0" type="appl" />
         <tli id="T247" time="127.5" type="appl" />
         <tli id="T248" time="128.0" type="appl" />
         <tli id="T249" time="128.5" type="appl" />
         <tli id="T250" time="129.0" type="appl" />
         <tli id="T251" time="129.5" type="appl" />
         <tli id="T252" time="130.0" type="appl" />
         <tli id="T253" time="130.5" type="appl" />
         <tli id="T254" time="131.0" type="appl" />
         <tli id="T255" time="131.5" type="appl" />
         <tli id="T256" time="132.0" type="appl" />
         <tli id="T257" time="132.5" type="appl" />
         <tli id="T258" time="133.0" type="appl" />
         <tli id="T259" time="133.5" type="appl" />
         <tli id="T260" time="134.0" type="appl" />
         <tli id="T261" time="134.5" type="appl" />
         <tli id="T262" time="135.0" type="appl" />
         <tli id="T263" time="135.5" type="appl" />
         <tli id="T264" time="136.0" type="appl" />
         <tli id="T265" time="136.5" type="appl" />
         <tli id="T266" time="137.0" type="appl" />
         <tli id="T267" time="137.5" type="appl" />
         <tli id="T268" time="138.0" type="appl" />
         <tli id="T269" time="138.5" type="appl" />
         <tli id="T270" time="139.0" type="appl" />
         <tli id="T271" time="139.5" type="appl" />
         <tli id="T272" time="140.0" type="appl" />
         <tli id="T273" time="140.5" type="appl" />
         <tli id="T274" time="141.0" type="appl" />
         <tli id="T275" time="141.5" type="appl" />
         <tli id="T276" time="142.0" type="appl" />
         <tli id="T277" time="142.5" type="appl" />
         <tli id="T278" time="143.0" type="appl" />
         <tli id="T279" time="143.5" type="appl" />
         <tli id="T280" time="144.0" type="appl" />
         <tli id="T281" time="144.5" type="appl" />
         <tli id="T282" time="145.0" type="appl" />
         <tli id="T283" time="145.5" type="appl" />
         <tli id="T284" time="146.0" type="appl" />
         <tli id="T285" time="146.5" type="appl" />
         <tli id="T286" time="147.0" type="appl" />
         <tli id="T287" time="147.5" type="appl" />
         <tli id="T288" time="148.0" type="appl" />
         <tli id="T289" time="148.5" type="appl" />
         <tli id="T290" time="149.0" type="appl" />
         <tli id="T291" time="149.5" type="appl" />
         <tli id="T292" time="150.0" type="appl" />
         <tli id="T293" time="150.5" type="appl" />
         <tli id="T294" time="151.0" type="appl" />
         <tli id="T295" time="151.5" type="appl" />
         <tli id="T296" time="152.0" type="appl" />
         <tli id="T297" time="152.5" type="appl" />
         <tli id="T298" time="153.0" type="appl" />
         <tli id="T299" time="153.5" type="appl" />
         <tli id="T300" time="154.0" type="appl" />
         <tli id="T301" time="154.5" type="appl" />
         <tli id="T302" time="155.0" type="appl" />
         <tli id="T303" time="155.5" type="appl" />
         <tli id="T304" time="156.0" type="appl" />
         <tli id="T305" time="156.5" type="appl" />
         <tli id="T306" time="157.0" type="appl" />
         <tli id="T307" time="157.5" type="appl" />
         <tli id="T308" time="158.0" type="appl" />
         <tli id="T309" time="158.5" type="appl" />
         <tli id="T310" time="159.0" type="appl" />
         <tli id="T311" time="159.5" type="appl" />
         <tli id="T312" time="160.0" type="appl" />
         <tli id="T313" time="160.5" type="appl" />
         <tli id="T314" time="161.0" type="appl" />
         <tli id="T315" time="161.5" type="appl" />
         <tli id="T316" time="162.0" type="appl" />
         <tli id="T317" time="162.5" type="appl" />
         <tli id="T318" time="163.0" type="appl" />
         <tli id="T319" time="163.5" type="appl" />
         <tli id="T320" time="164.0" type="appl" />
         <tli id="T321" time="164.5" type="appl" />
         <tli id="T322" time="165.0" type="appl" />
         <tli id="T323" time="165.5" type="appl" />
         <tli id="T324" time="166.0" type="appl" />
         <tli id="T325" time="166.5" type="appl" />
         <tli id="T326" time="167.0" type="appl" />
         <tli id="T327" time="167.5" type="appl" />
         <tli id="T328" time="168.0" type="appl" />
         <tli id="T329" time="168.5" type="appl" />
         <tli id="T330" time="169.0" type="appl" />
         <tli id="T331" time="169.5" type="appl" />
         <tli id="T332" time="170.0" type="appl" />
         <tli id="T333" time="170.5" type="appl" />
         <tli id="T334" time="171.0" type="appl" />
         <tli id="T335" time="171.5" type="appl" />
         <tli id="T336" time="172.0" type="appl" />
         <tli id="T337" time="172.5" type="appl" />
         <tli id="T338" time="173.0" type="appl" />
         <tli id="T339" time="173.5" type="appl" />
         <tli id="T340" time="174.0" type="appl" />
         <tli id="T341" time="174.5" type="appl" />
         <tli id="T342" time="175.0" type="appl" />
         <tli id="T343" time="175.5" type="appl" />
         <tli id="T344" time="176.0" type="appl" />
         <tli id="T345" time="176.5" type="appl" />
         <tli id="T346" time="177.0" type="appl" />
         <tli id="T347" time="177.5" type="appl" />
         <tli id="T348" time="178.0" type="appl" />
         <tli id="T349" time="178.5" type="appl" />
         <tli id="T350" time="179.0" type="appl" />
         <tli id="T351" time="179.5" type="appl" />
         <tli id="T352" time="180.0" type="appl" />
         <tli id="T353" time="180.5" type="appl" />
         <tli id="T354" time="181.0" type="appl" />
         <tli id="T355" time="181.5" type="appl" />
         <tli id="T356" time="182.0" type="appl" />
         <tli id="T357" time="182.5" type="appl" />
         <tli id="T358" time="183.0" type="appl" />
         <tli id="T359" time="183.5" type="appl" />
         <tli id="T360" time="184.0" type="appl" />
         <tli id="T361" time="184.5" type="appl" />
         <tli id="T362" time="185.0" type="appl" />
         <tli id="T363" time="185.5" type="appl" />
         <tli id="T364" time="186.0" type="appl" />
         <tli id="T365" time="186.5" type="appl" />
         <tli id="T366" time="187.0" type="appl" />
         <tli id="T367" time="187.5" type="appl" />
         <tli id="T368" time="188.0" type="appl" />
         <tli id="T369" time="188.5" type="appl" />
         <tli id="T370" time="189.0" type="appl" />
         <tli id="T371" time="189.5" type="appl" />
         <tli id="T372" time="190.0" type="appl" />
         <tli id="T373" time="190.5" type="appl" />
         <tli id="T374" time="191.0" type="appl" />
         <tli id="T375" time="191.5" type="appl" />
         <tli id="T376" time="192.0" type="appl" />
         <tli id="T377" time="192.5" type="appl" />
         <tli id="T378" time="193.0" type="appl" />
         <tli id="T379" time="193.5" type="appl" />
         <tli id="T380" time="194.0" type="appl" />
         <tli id="T381" time="194.5" type="appl" />
         <tli id="T382" time="195.0" type="appl" />
         <tli id="T383" time="195.5" type="appl" />
         <tli id="T384" time="196.0" type="appl" />
         <tli id="T385" time="196.5" type="appl" />
         <tli id="T386" time="197.0" type="appl" />
         <tli id="T387" time="197.5" type="appl" />
         <tli id="T388" time="198.0" type="appl" />
         <tli id="T389" time="198.5" type="appl" />
         <tli id="T390" time="199.0" type="appl" />
         <tli id="T391" time="199.5" type="appl" />
         <tli id="T392" time="200.0" type="appl" />
         <tli id="T393" time="200.5" type="appl" />
         <tli id="T394" time="201.0" type="appl" />
         <tli id="T395" time="201.5" type="appl" />
         <tli id="T396" time="202.0" type="appl" />
         <tli id="T397" time="202.5" type="appl" />
         <tli id="T398" time="203.0" type="appl" />
         <tli id="T399" time="203.5" type="appl" />
         <tli id="T400" time="204.0" type="appl" />
         <tli id="T401" time="204.5" type="appl" />
         <tli id="T402" time="205.0" type="appl" />
         <tli id="T403" time="205.5" type="appl" />
         <tli id="T404" time="206.0" type="appl" />
         <tli id="T405" time="206.5" type="appl" />
         <tli id="T406" time="207.0" type="appl" />
         <tli id="T407" time="207.5" type="appl" />
         <tli id="T408" time="208.0" type="appl" />
         <tli id="T409" time="208.5" type="appl" />
         <tli id="T410" time="209.0" type="appl" />
         <tli id="T411" time="209.5" type="appl" />
         <tli id="T412" time="210.0" type="appl" />
         <tli id="T413" time="210.5" type="appl" />
         <tli id="T414" time="211.0" type="appl" />
         <tli id="T415" time="211.5" type="appl" />
         <tli id="T416" time="212.0" type="appl" />
         <tli id="T417" time="212.5" type="appl" />
         <tli id="T418" time="213.0" type="appl" />
         <tli id="T419" time="213.5" type="appl" />
         <tli id="T420" time="214.0" type="appl" />
         <tli id="T421" time="214.5" type="appl" />
         <tli id="T422" time="215.0" type="appl" />
         <tli id="T423" time="215.5" type="appl" />
         <tli id="T424" time="216.0" type="appl" />
         <tli id="T425" time="216.5" type="appl" />
         <tli id="T426" time="217.0" type="appl" />
         <tli id="T427" time="217.5" type="appl" />
         <tli id="T428" time="218.0" type="appl" />
         <tli id="T429" time="218.5" type="appl" />
         <tli id="T430" time="219.0" type="appl" />
         <tli id="T431" time="219.5" type="appl" />
         <tli id="T432" time="220.0" type="appl" />
         <tli id="T433" time="220.5" type="appl" />
         <tli id="T434" time="221.0" type="appl" />
         <tli id="T435" time="221.5" type="appl" />
         <tli id="T436" time="222.0" type="appl" />
         <tli id="T437" time="222.5" type="appl" />
         <tli id="T438" time="223.0" type="appl" />
         <tli id="T439" time="223.5" type="appl" />
         <tli id="T440" time="224.0" type="appl" />
         <tli id="T441" time="224.5" type="appl" />
         <tli id="T442" time="225.0" type="appl" />
         <tli id="T443" time="225.5" type="appl" />
         <tli id="T444" time="226.0" type="appl" />
         <tli id="T445" time="226.5" type="appl" />
         <tli id="T446" time="227.0" type="appl" />
         <tli id="T447" time="227.5" type="appl" />
         <tli id="T448" time="228.0" type="appl" />
         <tli id="T449" time="228.5" type="appl" />
         <tli id="T450" time="229.0" type="appl" />
         <tli id="T451" time="229.5" type="appl" />
         <tli id="T452" time="230.0" type="appl" />
         <tli id="T453" time="230.5" type="appl" />
         <tli id="T454" time="231.0" type="appl" />
         <tli id="T455" time="231.5" type="appl" />
         <tli id="T456" time="232.0" type="appl" />
         <tli id="T457" time="232.5" type="appl" />
         <tli id="T458" time="233.0" type="appl" />
         <tli id="T459" time="233.5" type="appl" />
         <tli id="T460" time="234.0" type="appl" />
         <tli id="T461" time="234.5" type="appl" />
         <tli id="T462" time="235.0" type="appl" />
         <tli id="T463" time="235.5" type="appl" />
         <tli id="T464" time="236.0" type="appl" />
         <tli id="T465" time="236.5" type="appl" />
         <tli id="T466" time="237.0" type="appl" />
         <tli id="T467" time="237.5" type="appl" />
         <tli id="T468" time="238.0" type="appl" />
         <tli id="T469" time="238.5" type="appl" />
         <tli id="T470" time="239.0" type="appl" />
         <tli id="T471" time="239.5" type="appl" />
         <tli id="T472" time="240.0" type="appl" />
         <tli id="T473" time="240.5" type="appl" />
         <tli id="T474" time="241.0" type="appl" />
         <tli id="T475" time="241.5" type="appl" />
         <tli id="T476" time="242.0" type="appl" />
         <tli id="T477" time="242.5" type="appl" />
         <tli id="T478" time="243.0" type="appl" />
         <tli id="T479" time="243.5" type="appl" />
         <tli id="T480" time="244.0" type="appl" />
         <tli id="T481" time="244.5" type="appl" />
         <tli id="T482" time="245.0" type="appl" />
         <tli id="T483" time="245.5" type="appl" />
         <tli id="T484" time="246.0" type="appl" />
         <tli id="T485" time="246.5" type="appl" />
         <tli id="T486" time="247.0" type="appl" />
         <tli id="T487" time="247.5" type="appl" />
         <tli id="T488" time="248.0" type="appl" />
         <tli id="T489" time="248.5" type="appl" />
         <tli id="T490" time="249.0" type="appl" />
         <tli id="T491" time="249.5" type="appl" />
         <tli id="T492" time="250.0" type="appl" />
         <tli id="T493" time="250.5" type="appl" />
         <tli id="T494" time="251.0" type="appl" />
         <tli id="T495" time="251.5" type="appl" />
         <tli id="T496" time="252.0" type="appl" />
         <tli id="T497" time="252.5" type="appl" />
         <tli id="T498" time="253.0" type="appl" />
         <tli id="T499" time="253.5" type="appl" />
         <tli id="T500" time="254.0" type="appl" />
         <tli id="T501" time="254.5" type="appl" />
         <tli id="T502" time="255.0" type="appl" />
         <tli id="T503" time="255.5" type="appl" />
         <tli id="T504" time="256.0" type="appl" />
         <tli id="T505" time="256.5" type="appl" />
         <tli id="T506" time="257.0" type="appl" />
         <tli id="T507" time="257.5" type="appl" />
         <tli id="T508" time="258.0" type="appl" />
         <tli id="T509" time="258.5" type="appl" />
         <tli id="T510" time="259.0" type="appl" />
         <tli id="T511" time="259.5" type="appl" />
         <tli id="T512" time="260.0" type="appl" />
         <tli id="T513" time="260.5" type="appl" />
         <tli id="T514" time="261.0" type="appl" />
         <tli id="T515" time="261.5" type="appl" />
         <tli id="T516" time="262.0" type="appl" />
         <tli id="T517" time="262.5" type="appl" />
         <tli id="T518" time="263.0" type="appl" />
         <tli id="T519" time="263.5" type="appl" />
         <tli id="T520" time="264.0" type="appl" />
         <tli id="T521" time="264.5" type="appl" />
         <tli id="T522" time="265.0" type="appl" />
         <tli id="T523" time="265.5" type="appl" />
         <tli id="T524" time="266.0" type="appl" />
         <tli id="T525" time="266.5" type="appl" />
         <tli id="T526" time="267.0" type="appl" />
         <tli id="T527" time="267.5" type="appl" />
         <tli id="T528" time="268.0" type="appl" />
         <tli id="T529" time="268.5" type="appl" />
         <tli id="T530" time="269.0" type="appl" />
         <tli id="T531" time="269.5" type="appl" />
         <tli id="T532" time="270.0" type="appl" />
         <tli id="T533" time="270.5" type="appl" />
         <tli id="T534" time="271.0" type="appl" />
         <tli id="T535" time="271.5" type="appl" />
         <tli id="T536" time="272.0" type="appl" />
         <tli id="T537" time="272.5" type="appl" />
         <tli id="T538" time="273.0" type="appl" />
         <tli id="T539" time="273.5" type="appl" />
         <tli id="T540" time="274.0" type="appl" />
         <tli id="T541" time="274.5" type="appl" />
         <tli id="T542" time="275.0" type="appl" />
         <tli id="T543" time="275.5" type="appl" />
         <tli id="T544" time="276.0" type="appl" />
         <tli id="T545" time="276.5" type="appl" />
         <tli id="T546" time="277.0" type="appl" />
         <tli id="T547" time="277.5" type="appl" />
         <tli id="T548" time="278.0" type="appl" />
         <tli id="T549" time="278.5" type="appl" />
         <tli id="T550" time="279.0" type="appl" />
         <tli id="T551" time="279.5" type="appl" />
         <tli id="T552" time="280.0" type="appl" />
         <tli id="T553" time="280.5" type="appl" />
         <tli id="T554" time="281.0" type="appl" />
         <tli id="T555" time="281.5" type="appl" />
         <tli id="T556" time="282.0" type="appl" />
         <tli id="T557" time="282.5" type="appl" />
         <tli id="T558" time="283.0" type="appl" />
         <tli id="T559" time="283.5" type="appl" />
         <tli id="T560" time="284.0" type="appl" />
         <tli id="T561" time="284.5" type="appl" />
         <tli id="T562" time="285.0" type="appl" />
         <tli id="T563" time="285.5" type="appl" />
         <tli id="T564" time="286.0" type="appl" />
         <tli id="T565" time="286.5" type="appl" />
         <tli id="T566" time="287.0" type="appl" />
         <tli id="T567" time="287.5" type="appl" />
         <tli id="T568" time="288.0" type="appl" />
         <tli id="T569" time="288.5" type="appl" />
         <tli id="T570" time="289.0" type="appl" />
         <tli id="T571" time="289.5" type="appl" />
         <tli id="T572" time="290.0" type="appl" />
         <tli id="T573" time="290.5" type="appl" />
         <tli id="T574" time="291.0" type="appl" />
         <tli id="T575" time="291.5" type="appl" />
         <tli id="T576" time="292.0" type="appl" />
         <tli id="T577" time="292.5" type="appl" />
         <tli id="T578" time="293.0" type="appl" />
         <tli id="T579" time="293.5" type="appl" />
         <tli id="T580" time="294.0" type="appl" />
         <tli id="T581" time="294.5" type="appl" />
         <tli id="T582" time="295.0" type="appl" />
         <tli id="T583" time="295.5" type="appl" />
         <tli id="T584" time="296.0" type="appl" />
         <tli id="T585" time="296.5" type="appl" />
         <tli id="T586" time="297.0" type="appl" />
         <tli id="T587" time="297.5" type="appl" />
         <tli id="T588" time="298.0" type="appl" />
         <tli id="T589" time="298.5" type="appl" />
         <tli id="T590" time="299.0" type="appl" />
         <tli id="T591" time="299.5" type="appl" />
         <tli id="T592" time="300.0" type="appl" />
         <tli id="T593" time="300.5" type="appl" />
         <tli id="T594" time="301.0" type="appl" />
         <tli id="T595" time="301.5" type="appl" />
         <tli id="T596" time="302.0" type="appl" />
         <tli id="T597" time="302.5" type="appl" />
         <tli id="T598" time="303.0" type="appl" />
         <tli id="T599" time="303.5" type="appl" />
         <tli id="T600" time="304.0" type="appl" />
         <tli id="T601" time="304.5" type="appl" />
         <tli id="T602" time="305.0" type="appl" />
         <tli id="T603" time="305.5" type="appl" />
         <tli id="T604" time="306.0" type="appl" />
         <tli id="T605" time="306.5" type="appl" />
         <tli id="T606" time="307.0" type="appl" />
         <tli id="T607" time="307.5" type="appl" />
         <tli id="T608" time="308.0" type="appl" />
         <tli id="T609" time="308.5" type="appl" />
         <tli id="T610" time="309.0" type="appl" />
         <tli id="T611" time="309.5" type="appl" />
         <tli id="T612" time="310.0" type="appl" />
         <tli id="T613" time="310.5" type="appl" />
         <tli id="T614" time="311.0" type="appl" />
         <tli id="T615" time="311.5" type="appl" />
         <tli id="T616" time="312.0" type="appl" />
         <tli id="T617" time="312.5" type="appl" />
         <tli id="T618" time="313.0" type="appl" />
         <tli id="T619" time="313.5" type="appl" />
         <tli id="T620" time="314.0" type="appl" />
         <tli id="T621" time="314.5" type="appl" />
         <tli id="T622" time="315.0" type="appl" />
         <tli id="T623" time="315.5" type="appl" />
         <tli id="T624" time="316.0" type="appl" />
         <tli id="T625" time="316.5" type="appl" />
         <tli id="T626" time="317.0" type="appl" />
         <tli id="T627" time="317.5" type="appl" />
         <tli id="T628" time="318.0" type="appl" />
         <tli id="T629" time="318.5" type="appl" />
         <tli id="T630" time="319.0" type="appl" />
         <tli id="T631" time="319.5" type="appl" />
         <tli id="T632" time="320.0" type="appl" />
         <tli id="T633" time="320.5" type="appl" />
         <tli id="T634" time="321.0" type="appl" />
         <tli id="T635" time="321.5" type="appl" />
         <tli id="T636" time="322.0" type="appl" />
         <tli id="T637" time="322.5" type="appl" />
         <tli id="T638" time="323.0" type="appl" />
         <tli id="T639" time="323.5" type="appl" />
         <tli id="T640" time="324.0" type="appl" />
         <tli id="T641" time="324.5" type="appl" />
         <tli id="T642" time="325.0" type="appl" />
         <tli id="T643" time="325.5" type="appl" />
         <tli id="T644" time="326.0" type="appl" />
         <tli id="T645" time="326.5" type="appl" />
         <tli id="T646" time="327.0" type="appl" />
         <tli id="T647" time="327.5" type="appl" />
         <tli id="T648" time="328.0" type="appl" />
         <tli id="T649" time="328.5" type="appl" />
         <tli id="T650" time="329.0" type="appl" />
         <tli id="T651" time="329.5" type="appl" />
         <tli id="T652" time="330.0" type="appl" />
         <tli id="T653" time="330.5" type="appl" />
         <tli id="T654" time="331.0" type="appl" />
         <tli id="T655" time="331.5" type="appl" />
         <tli id="T656" time="332.0" type="appl" />
         <tli id="T657" time="332.5" type="appl" />
         <tli id="T658" time="333.0" type="appl" />
         <tli id="T659" time="333.5" type="appl" />
         <tli id="T660" time="334.0" type="appl" />
         <tli id="T661" time="334.5" type="appl" />
         <tli id="T662" time="335.0" type="appl" />
         <tli id="T663" time="335.5" type="appl" />
         <tli id="T664" time="336.0" type="appl" />
         <tli id="T665" time="336.5" type="appl" />
         <tli id="T666" time="337.0" type="appl" />
         <tli id="T667" time="337.5" type="appl" />
         <tli id="T668" time="338.0" type="appl" />
         <tli id="T669" time="338.5" type="appl" />
         <tli id="T670" time="339.0" type="appl" />
         <tli id="T671" time="339.5" type="appl" />
         <tli id="T672" time="340.0" type="appl" />
         <tli id="T673" time="340.5" type="appl" />
         <tli id="T674" time="341.0" type="appl" />
         <tli id="T675" time="341.5" type="appl" />
         <tli id="T676" time="342.0" type="appl" />
         <tli id="T677" time="342.5" type="appl" />
         <tli id="T678" time="343.0" type="appl" />
         <tli id="T679" time="343.5" type="appl" />
         <tli id="T680" time="344.0" type="appl" />
         <tli id="T681" time="344.5" type="appl" />
         <tli id="T682" time="345.0" type="appl" />
         <tli id="T683" time="345.5" type="appl" />
         <tli id="T684" time="346.0" type="appl" />
         <tli id="T685" time="346.5" type="appl" />
         <tli id="T686" time="347.0" type="appl" />
         <tli id="T687" time="347.5" type="appl" />
         <tli id="T688" time="348.0" type="appl" />
         <tli id="T689" time="348.5" type="appl" />
         <tli id="T690" time="349.0" type="appl" />
         <tli id="T691" time="349.5" type="appl" />
         <tli id="T692" time="350.0" type="appl" />
         <tli id="T693" time="350.5" type="appl" />
         <tli id="T694" time="351.0" type="appl" />
         <tli id="T695" time="351.5" type="appl" />
         <tli id="T696" time="352.0" type="appl" />
         <tli id="T697" time="352.5" type="appl" />
         <tli id="T698" time="353.0" type="appl" />
         <tli id="T699" time="353.5" type="appl" />
         <tli id="T700" time="354.0" type="appl" />
         <tli id="T701" time="354.5" type="appl" />
         <tli id="T702" time="355.0" type="appl" />
         <tli id="T703" time="355.5" type="appl" />
         <tli id="T704" time="356.0" type="appl" />
         <tli id="T705" time="356.5" type="appl" />
         <tli id="T706" time="357.0" type="appl" />
         <tli id="T707" time="357.5" type="appl" />
         <tli id="T708" time="358.0" type="appl" />
         <tli id="T709" time="358.5" type="appl" />
         <tli id="T710" time="359.0" type="appl" />
         <tli id="T711" time="359.5" type="appl" />
         <tli id="T712" time="360.0" type="appl" />
         <tli id="T713" time="360.5" type="appl" />
         <tli id="T714" time="361.0" type="appl" />
         <tli id="T715" time="361.5" type="appl" />
         <tli id="T716" time="362.0" type="appl" />
         <tli id="T717" time="362.5" type="appl" />
         <tli id="T718" time="363.0" type="appl" />
         <tli id="T719" time="363.5" type="appl" />
         <tli id="T720" time="364.0" type="appl" />
         <tli id="T721" time="364.5" type="appl" />
         <tli id="T722" time="365.0" type="appl" />
         <tli id="T723" time="365.5" type="appl" />
         <tli id="T724" time="366.0" type="appl" />
         <tli id="T725" time="366.5" type="appl" />
         <tli id="T726" time="367.0" type="appl" />
         <tli id="T727" time="367.5" type="appl" />
         <tli id="T728" time="368.0" type="appl" />
         <tli id="T729" time="368.5" type="appl" />
         <tli id="T730" time="369.0" type="appl" />
         <tli id="T731" time="369.5" type="appl" />
         <tli id="T732" time="370.0" type="appl" />
         <tli id="T733" time="370.5" type="appl" />
         <tli id="T734" time="371.0" type="appl" />
         <tli id="T735" time="371.5" type="appl" />
         <tli id="T736" time="372.0" type="appl" />
         <tli id="T737" time="372.5" type="appl" />
         <tli id="T738" time="373.0" type="appl" />
         <tli id="T739" time="373.5" type="appl" />
         <tli id="T740" time="374.0" type="appl" />
         <tli id="T741" time="374.5" type="appl" />
         <tli id="T742" time="375.0" type="appl" />
         <tli id="T743" time="375.5" type="appl" />
         <tli id="T744" time="376.0" type="appl" />
         <tli id="T745" time="376.5" type="appl" />
         <tli id="T746" time="377.0" type="appl" />
         <tli id="T747" time="377.5" type="appl" />
         <tli id="T748" time="378.0" type="appl" />
         <tli id="T749" time="378.5" type="appl" />
         <tli id="T750" time="379.0" type="appl" />
         <tli id="T751" time="379.5" type="appl" />
         <tli id="T752" time="380.0" type="appl" />
         <tli id="T753" time="380.5" type="appl" />
         <tli id="T754" time="381.0" type="appl" />
         <tli id="T755" time="381.5" type="appl" />
         <tli id="T756" time="382.0" type="appl" />
         <tli id="T757" time="382.5" type="appl" />
         <tli id="T758" time="383.0" type="appl" />
         <tli id="T759" time="383.5" type="appl" />
         <tli id="T760" time="384.0" type="appl" />
         <tli id="T761" time="384.5" type="appl" />
         <tli id="T762" time="385.0" type="appl" />
         <tli id="T763" time="385.5" type="appl" />
         <tli id="T764" time="386.0" type="appl" />
         <tli id="T765" time="386.5" type="appl" />
         <tli id="T766" time="387.0" type="appl" />
         <tli id="T767" time="387.5" type="appl" />
         <tli id="T768" time="388.0" type="appl" />
         <tli id="T769" time="388.5" type="appl" />
         <tli id="T770" time="389.0" type="appl" />
         <tli id="T771" time="389.5" type="appl" />
         <tli id="T772" time="390.0" type="appl" />
         <tli id="T773" time="390.5" type="appl" />
         <tli id="T774" time="391.0" type="appl" />
         <tli id="T775" time="391.5" type="appl" />
         <tli id="T776" time="392.0" type="appl" />
         <tli id="T777" time="392.5" type="appl" />
         <tli id="T778" time="393.0" type="appl" />
         <tli id="T779" time="393.5" type="appl" />
         <tli id="T780" time="394.0" type="appl" />
         <tli id="T781" time="394.5" type="appl" />
         <tli id="T782" time="395.0" type="appl" />
         <tli id="T783" time="395.5" type="appl" />
         <tli id="T784" time="396.0" type="appl" />
         <tli id="T785" time="396.5" type="appl" />
         <tli id="T786" time="397.0" type="appl" />
         <tli id="T787" time="397.5" type="appl" />
         <tli id="T788" time="398.0" type="appl" />
         <tli id="T789" time="398.5" type="appl" />
         <tli id="T790" time="399.0" type="appl" />
         <tli id="T791" time="399.5" type="appl" />
         <tli id="T792" time="400.0" type="appl" />
         <tli id="T793" time="400.5" type="appl" />
         <tli id="T794" time="401.0" type="appl" />
         <tli id="T795" time="401.5" type="appl" />
         <tli id="T796" time="402.0" type="appl" />
         <tli id="T797" time="402.5" type="appl" />
         <tli id="T798" time="403.0" type="appl" />
         <tli id="T799" time="403.5" type="appl" />
         <tli id="T800" time="404.0" type="appl" />
         <tli id="T801" time="404.5" type="appl" />
         <tli id="T802" time="405.0" type="appl" />
         <tli id="T803" time="405.5" type="appl" />
         <tli id="T804" time="406.0" type="appl" />
         <tli id="T805" time="406.5" type="appl" />
         <tli id="T806" time="407.0" type="appl" />
         <tli id="T807" time="407.5" type="appl" />
         <tli id="T808" time="408.0" type="appl" />
         <tli id="T809" time="408.5" type="appl" />
         <tli id="T810" time="409.0" type="appl" />
         <tli id="T811" time="409.5" type="appl" />
         <tli id="T812" time="410.0" type="appl" />
         <tli id="T813" time="410.5" type="appl" />
         <tli id="T814" time="411.0" type="appl" />
         <tli id="T815" time="411.5" type="appl" />
         <tli id="T816" time="412.0" type="appl" />
         <tli id="T817" time="412.5" type="appl" />
         <tli id="T818" time="413.0" type="appl" />
         <tli id="T819" time="413.5" type="appl" />
         <tli id="T820" time="414.0" type="appl" />
         <tli id="T821" time="414.5" type="appl" />
         <tli id="T822" time="415.0" type="appl" />
         <tli id="T823" time="415.5" type="appl" />
         <tli id="T824" time="416.0" type="appl" />
         <tli id="T825" time="416.5" type="appl" />
         <tli id="T826" time="417.0" type="appl" />
         <tli id="T827" time="417.5" type="appl" />
         <tli id="T828" time="418.0" type="appl" />
         <tli id="T829" time="418.5" type="appl" />
         <tli id="T830" time="419.0" type="appl" />
         <tli id="T831" time="419.5" type="appl" />
         <tli id="T832" time="420.0" type="appl" />
         <tli id="T833" time="420.5" type="appl" />
         <tli id="T834" time="421.0" type="appl" />
         <tli id="T835" time="421.5" type="appl" />
         <tli id="T836" time="422.0" type="appl" />
         <tli id="T837" time="422.5" type="appl" />
         <tli id="T838" time="423.0" type="appl" />
         <tli id="T839" time="423.5" type="appl" />
         <tli id="T840" time="424.0" type="appl" />
         <tli id="T841" time="424.5" type="appl" />
         <tli id="T842" time="425.0" type="appl" />
         <tli id="T843" time="425.5" type="appl" />
         <tli id="T844" time="426.0" type="appl" />
         <tli id="T845" time="426.5" type="appl" />
         <tli id="T846" time="427.0" type="appl" />
         <tli id="T847" time="427.5" type="appl" />
         <tli id="T848" time="428.0" type="appl" />
         <tli id="T849" time="428.5" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="SaSS"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T849" id="Seg_0" n="sc" s="T0">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">ɨraːktaːgɨlaːktar</ts>
                  <nts id="Seg_5" n="HIAT:ip">,</nts>
                  <nts id="Seg_6" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_8" n="HIAT:w" s="T1">küseːjinneːkter</ts>
                  <nts id="Seg_9" n="HIAT:ip">,</nts>
                  <nts id="Seg_10" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_12" n="HIAT:w" s="T2">kineːsteːkter</ts>
                  <nts id="Seg_13" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_15" n="HIAT:w" s="T3">bu͡ollaga</ts>
                  <nts id="Seg_16" n="HIAT:ip">.</nts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_19" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_21" n="HIAT:w" s="T4">Bu</ts>
                  <nts id="Seg_22" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_24" n="HIAT:w" s="T5">kihi</ts>
                  <nts id="Seg_25" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_27" n="HIAT:w" s="T6">tuspa</ts>
                  <nts id="Seg_28" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_30" n="HIAT:w" s="T7">oloror</ts>
                  <nts id="Seg_31" n="HIAT:ip">,</nts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_34" n="HIAT:w" s="T8">kohuːn</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_37" n="HIAT:w" s="T9">agaj</ts>
                  <nts id="Seg_38" n="HIAT:ip">.</nts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T14" id="Seg_41" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_43" n="HIAT:w" s="T10">Biːrkeːn</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_46" n="HIAT:w" s="T11">da</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_49" n="HIAT:w" s="T12">ogoto</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_52" n="HIAT:w" s="T13">hu͡ok</ts>
                  <nts id="Seg_53" n="HIAT:ip">.</nts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T18" id="Seg_56" n="HIAT:u" s="T14">
                  <ts e="T15" id="Seg_58" n="HIAT:w" s="T14">Jɨlga</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_61" n="HIAT:w" s="T15">biːrde</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_64" n="HIAT:w" s="T16">tölüːr</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_67" n="HIAT:w" s="T17">nolu͡ogun</ts>
                  <nts id="Seg_68" n="HIAT:ip">.</nts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_71" n="HIAT:u" s="T18">
                  <ts e="T19" id="Seg_73" n="HIAT:w" s="T18">Bajgal</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_76" n="HIAT:w" s="T19">kɨrɨːtɨgar</ts>
                  <nts id="Seg_77" n="HIAT:ip">,</nts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_80" n="HIAT:w" s="T20">ile</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_83" n="HIAT:w" s="T21">mu͡oraga</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_86" n="HIAT:w" s="T22">oloror</ts>
                  <nts id="Seg_87" n="HIAT:ip">,</nts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_90" n="HIAT:w" s="T23">bajgal</ts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_93" n="HIAT:w" s="T24">hɨlbagɨn</ts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_96" n="HIAT:w" s="T25">ottunar</ts>
                  <nts id="Seg_97" n="HIAT:ip">.</nts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T29" id="Seg_100" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_102" n="HIAT:w" s="T26">Ör</ts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_105" n="HIAT:w" s="T27">olorbut</ts>
                  <nts id="Seg_106" n="HIAT:ip">,</nts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_109" n="HIAT:w" s="T28">kɨrdʼɨbɨt</ts>
                  <nts id="Seg_110" n="HIAT:ip">.</nts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T32" id="Seg_113" n="HIAT:u" s="T29">
                  <ts e="T30" id="Seg_115" n="HIAT:w" s="T29">Dʼɨla</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_118" n="HIAT:w" s="T30">haːstɨjan</ts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_121" n="HIAT:w" s="T31">barar</ts>
                  <nts id="Seg_122" n="HIAT:ip">.</nts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T34" id="Seg_125" n="HIAT:u" s="T32">
                  <ts e="T33" id="Seg_127" n="HIAT:w" s="T32">Emeːksiniger</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_130" n="HIAT:w" s="T33">eter</ts>
                  <nts id="Seg_131" n="HIAT:ip">:</nts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T37" id="Seg_134" n="HIAT:u" s="T34">
                  <nts id="Seg_135" n="HIAT:ip">"</nts>
                  <ts e="T35" id="Seg_137" n="HIAT:w" s="T34">Dʼɨlbɨt</ts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_140" n="HIAT:w" s="T35">irde</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_143" n="HIAT:w" s="T36">diː</ts>
                  <nts id="Seg_144" n="HIAT:ip">.</nts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T41" id="Seg_147" n="HIAT:u" s="T37">
                  <ts e="T38" id="Seg_149" n="HIAT:w" s="T37">Mas</ts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_152" n="HIAT:w" s="T38">tardɨna</ts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_155" n="HIAT:w" s="T39">barɨ͡am</ts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_158" n="HIAT:w" s="T40">ehiːlgige</ts>
                  <nts id="Seg_159" n="HIAT:ip">.</nts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T44" id="Seg_162" n="HIAT:u" s="T41">
                  <ts e="T42" id="Seg_164" n="HIAT:w" s="T41">Otut</ts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_167" n="HIAT:w" s="T42">hɨrgata</ts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_170" n="HIAT:w" s="T43">hü͡ökkeː</ts>
                  <nts id="Seg_171" n="HIAT:ip">.</nts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T50" id="Seg_174" n="HIAT:u" s="T44">
                  <ts e="T45" id="Seg_176" n="HIAT:w" s="T44">Otut</ts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_179" n="HIAT:w" s="T45">hɨrgannan</ts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_182" n="HIAT:w" s="T46">bajgalga</ts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_185" n="HIAT:w" s="T47">baran</ts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_188" n="HIAT:w" s="T48">kaːlar</ts>
                  <nts id="Seg_189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_191" n="HIAT:w" s="T49">hobuč-hogotogun</ts>
                  <nts id="Seg_192" n="HIAT:ip">.</nts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T54" id="Seg_195" n="HIAT:u" s="T50">
                  <ts e="T51" id="Seg_197" n="HIAT:w" s="T50">Bajgalga</ts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_200" n="HIAT:w" s="T51">tumus</ts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_203" n="HIAT:w" s="T52">bagajɨnan</ts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_206" n="HIAT:w" s="T53">kiːrbit</ts>
                  <nts id="Seg_207" n="HIAT:ip">.</nts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T63" id="Seg_210" n="HIAT:u" s="T54">
                  <ts e="T55" id="Seg_212" n="HIAT:w" s="T54">Hogotok</ts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_215" n="HIAT:w" s="T55">hirten</ts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_218" n="HIAT:w" s="T56">ti͡emmit</ts>
                  <nts id="Seg_219" n="HIAT:ip">,</nts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_222" n="HIAT:w" s="T57">hɨːrɨn</ts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_225" n="HIAT:w" s="T58">ürdüger</ts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_228" n="HIAT:w" s="T59">biːr</ts>
                  <nts id="Seg_229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_231" n="HIAT:w" s="T60">halaː</ts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_234" n="HIAT:w" s="T61">ustun</ts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_237" n="HIAT:w" s="T62">tönnübüt</ts>
                  <nts id="Seg_238" n="HIAT:ip">.</nts>
                  <nts id="Seg_239" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T70" id="Seg_241" n="HIAT:u" s="T63">
                  <ts e="T64" id="Seg_243" n="HIAT:w" s="T63">Bu</ts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_246" n="HIAT:w" s="T64">taksan</ts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_249" n="HIAT:w" s="T65">kajatɨn</ts>
                  <nts id="Seg_250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_252" n="HIAT:w" s="T66">ürdüger</ts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_255" n="HIAT:w" s="T67">tabaːk</ts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_258" n="HIAT:w" s="T68">tarda</ts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_261" n="HIAT:w" s="T69">olorbut</ts>
                  <nts id="Seg_262" n="HIAT:ip">.</nts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T81" id="Seg_265" n="HIAT:u" s="T70">
                  <ts e="T71" id="Seg_267" n="HIAT:w" s="T70">Ebe</ts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_270" n="HIAT:w" s="T71">di͡ek</ts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_273" n="HIAT:w" s="T72">körbüte</ts>
                  <nts id="Seg_274" n="HIAT:ip">,</nts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_277" n="HIAT:w" s="T73">uːga</ts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_280" n="HIAT:w" s="T74">ogo</ts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_283" n="HIAT:w" s="T75">hɨldʼar</ts>
                  <nts id="Seg_284" n="HIAT:ip">,</nts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_287" n="HIAT:w" s="T76">irbit</ts>
                  <nts id="Seg_288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_290" n="HIAT:w" s="T77">uːga</ts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_293" n="HIAT:w" s="T78">küːgetterinen</ts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_296" n="HIAT:w" s="T79">oːnnʼuː</ts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_299" n="HIAT:w" s="T80">hɨldʼar</ts>
                  <nts id="Seg_300" n="HIAT:ip">.</nts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T84" id="Seg_303" n="HIAT:u" s="T81">
                  <ts e="T82" id="Seg_305" n="HIAT:w" s="T81">U͡ol</ts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_308" n="HIAT:w" s="T82">ogo</ts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_311" n="HIAT:w" s="T83">bɨhɨːlaːk</ts>
                  <nts id="Seg_312" n="HIAT:ip">.</nts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T88" id="Seg_315" n="HIAT:u" s="T84">
                  <ts e="T85" id="Seg_317" n="HIAT:w" s="T84">Kantan</ts>
                  <nts id="Seg_318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_320" n="HIAT:w" s="T85">da</ts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_323" n="HIAT:w" s="T86">kelbite</ts>
                  <nts id="Seg_324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_326" n="HIAT:w" s="T87">billibet</ts>
                  <nts id="Seg_327" n="HIAT:ip">.</nts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T89" id="Seg_330" n="HIAT:u" s="T88">
                  <ts e="T89" id="Seg_332" n="HIAT:w" s="T88">Oduːrgaːbɨt</ts>
                  <nts id="Seg_333" n="HIAT:ip">.</nts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T93" id="Seg_336" n="HIAT:u" s="T89">
                  <nts id="Seg_337" n="HIAT:ip">"</nts>
                  <ts e="T90" id="Seg_339" n="HIAT:w" s="T89">Min</ts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_342" n="HIAT:w" s="T90">ogoto</ts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_345" n="HIAT:w" s="T91">hu͡okpun</ts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_348" n="HIAT:w" s="T92">dʼiː</ts>
                  <nts id="Seg_349" n="HIAT:ip">!</nts>
                  <nts id="Seg_350" n="HIAT:ip">"</nts>
                  <nts id="Seg_351" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T97" id="Seg_353" n="HIAT:u" s="T93">
                  <ts e="T94" id="Seg_355" n="HIAT:w" s="T93">ɨlbɨt</ts>
                  <nts id="Seg_356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_358" n="HIAT:w" s="T94">kihi</ts>
                  <nts id="Seg_359" n="HIAT:ip">"</nts>
                  <nts id="Seg_360" n="HIAT:ip">,</nts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_363" n="HIAT:w" s="T95">diː</ts>
                  <nts id="Seg_364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_366" n="HIAT:w" s="T96">hanaːbɨt</ts>
                  <nts id="Seg_367" n="HIAT:ip">.</nts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T106" id="Seg_370" n="HIAT:u" s="T97">
                  <ts e="T98" id="Seg_372" n="HIAT:w" s="T97">Ogoto</ts>
                  <nts id="Seg_373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_375" n="HIAT:w" s="T98">arɨː</ts>
                  <nts id="Seg_376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_378" n="HIAT:w" s="T99">kumagɨgar</ts>
                  <nts id="Seg_379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_381" n="HIAT:w" s="T100">hɨldʼar</ts>
                  <nts id="Seg_382" n="HIAT:ip">,</nts>
                  <nts id="Seg_383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_385" n="HIAT:w" s="T101">ökögör</ts>
                  <nts id="Seg_386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_388" n="HIAT:w" s="T102">mas</ts>
                  <nts id="Seg_389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_391" n="HIAT:w" s="T103">baːr</ts>
                  <nts id="Seg_392" n="HIAT:ip">,</nts>
                  <nts id="Seg_393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_395" n="HIAT:w" s="T104">onon</ts>
                  <nts id="Seg_396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_398" n="HIAT:w" s="T105">bardaga</ts>
                  <nts id="Seg_399" n="HIAT:ip">.</nts>
                  <nts id="Seg_400" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T117" id="Seg_402" n="HIAT:u" s="T106">
                  <ts e="T107" id="Seg_404" n="HIAT:w" s="T106">ɨstanan</ts>
                  <nts id="Seg_405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_407" n="HIAT:w" s="T107">tijer</ts>
                  <nts id="Seg_408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_410" n="HIAT:w" s="T108">ogonnʼor</ts>
                  <nts id="Seg_411" n="HIAT:ip">,</nts>
                  <nts id="Seg_412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_414" n="HIAT:w" s="T109">argaːtɨttan</ts>
                  <nts id="Seg_415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_417" n="HIAT:w" s="T110">kaban</ts>
                  <nts id="Seg_418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_420" n="HIAT:w" s="T111">ɨlan</ts>
                  <nts id="Seg_421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_423" n="HIAT:w" s="T112">hukujun</ts>
                  <nts id="Seg_424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_426" n="HIAT:w" s="T113">ihiger</ts>
                  <nts id="Seg_427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_429" n="HIAT:w" s="T114">ukput</ts>
                  <nts id="Seg_430" n="HIAT:ip">,</nts>
                  <nts id="Seg_431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_433" n="HIAT:w" s="T115">kurdanan</ts>
                  <nts id="Seg_434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_436" n="HIAT:w" s="T116">keːspit</ts>
                  <nts id="Seg_437" n="HIAT:ip">.</nts>
                  <nts id="Seg_438" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T120" id="Seg_440" n="HIAT:u" s="T117">
                  <ts e="T118" id="Seg_442" n="HIAT:w" s="T117">Dʼe</ts>
                  <nts id="Seg_443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_445" n="HIAT:w" s="T118">araj</ts>
                  <nts id="Seg_446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_448" n="HIAT:w" s="T119">kamnɨːhɨ</ts>
                  <nts id="Seg_449" n="HIAT:ip">.</nts>
                  <nts id="Seg_450" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T123" id="Seg_452" n="HIAT:u" s="T120">
                  <ts e="T121" id="Seg_454" n="HIAT:w" s="T120">Tɨ͡as</ts>
                  <nts id="Seg_455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_457" n="HIAT:w" s="T121">koːkuna</ts>
                  <nts id="Seg_458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_460" n="HIAT:w" s="T122">bu͡olar</ts>
                  <nts id="Seg_461" n="HIAT:ip">.</nts>
                  <nts id="Seg_462" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T124" id="Seg_464" n="HIAT:u" s="T123">
                  <ts e="T124" id="Seg_466" n="HIAT:w" s="T123">Turbut</ts>
                  <nts id="Seg_467" n="HIAT:ip">.</nts>
                  <nts id="Seg_468" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T131" id="Seg_470" n="HIAT:u" s="T124">
                  <ts e="T125" id="Seg_472" n="HIAT:w" s="T124">Körbüte</ts>
                  <nts id="Seg_473" n="HIAT:ip">,</nts>
                  <nts id="Seg_474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_476" n="HIAT:w" s="T125">bütün</ts>
                  <nts id="Seg_477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_479" n="HIAT:w" s="T126">munnʼak</ts>
                  <nts id="Seg_480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_482" n="HIAT:w" s="T127">kihite</ts>
                  <nts id="Seg_483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_485" n="HIAT:w" s="T128">ölbüt</ts>
                  <nts id="Seg_486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_488" n="HIAT:w" s="T129">hire</ts>
                  <nts id="Seg_489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_491" n="HIAT:w" s="T130">ebit</ts>
                  <nts id="Seg_492" n="HIAT:ip">.</nts>
                  <nts id="Seg_493" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T137" id="Seg_495" n="HIAT:u" s="T131">
                  <nts id="Seg_496" n="HIAT:ip">"</nts>
                  <ts e="T132" id="Seg_498" n="HIAT:w" s="T131">Eː</ts>
                  <nts id="Seg_499" n="HIAT:ip">,</nts>
                  <nts id="Seg_500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_502" n="HIAT:w" s="T132">balartan</ts>
                  <nts id="Seg_503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_505" n="HIAT:w" s="T133">ordon</ts>
                  <nts id="Seg_506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_508" n="HIAT:w" s="T134">kaːlbɨt</ts>
                  <nts id="Seg_509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_511" n="HIAT:w" s="T135">ogo</ts>
                  <nts id="Seg_512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_514" n="HIAT:w" s="T136">bu͡ollaga</ts>
                  <nts id="Seg_515" n="HIAT:ip">"</nts>
                  <nts id="Seg_516" n="HIAT:ip">.</nts>
                  <nts id="Seg_517" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T143" id="Seg_519" n="HIAT:u" s="T137">
                  <ts e="T138" id="Seg_521" n="HIAT:w" s="T137">Biːr</ts>
                  <nts id="Seg_522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_524" n="HIAT:w" s="T138">argaːbɨttaːk</ts>
                  <nts id="Seg_525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_527" n="HIAT:w" s="T139">nʼu͡oguhutun</ts>
                  <nts id="Seg_528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_530" n="HIAT:w" s="T140">egelen</ts>
                  <nts id="Seg_531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_533" n="HIAT:w" s="T141">ölörön</ts>
                  <nts id="Seg_534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_536" n="HIAT:w" s="T142">keːher</ts>
                  <nts id="Seg_537" n="HIAT:ip">:</nts>
                  <nts id="Seg_538" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T152" id="Seg_540" n="HIAT:u" s="T143">
                  <nts id="Seg_541" n="HIAT:ip">"</nts>
                  <ts e="T144" id="Seg_543" n="HIAT:w" s="T143">Ogogutun</ts>
                  <nts id="Seg_544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_546" n="HIAT:w" s="T144">iltim</ts>
                  <nts id="Seg_547" n="HIAT:ip">,</nts>
                  <nts id="Seg_548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_550" n="HIAT:w" s="T145">bu</ts>
                  <nts id="Seg_551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_553" n="HIAT:w" s="T146">ogogut</ts>
                  <nts id="Seg_554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_556" n="HIAT:w" s="T147">oŋkuta</ts>
                  <nts id="Seg_557" n="HIAT:ip">,</nts>
                  <nts id="Seg_558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_560" n="HIAT:w" s="T148">biːr</ts>
                  <nts id="Seg_561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_563" n="HIAT:w" s="T149">tabanɨ</ts>
                  <nts id="Seg_564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_566" n="HIAT:w" s="T150">keːstim</ts>
                  <nts id="Seg_567" n="HIAT:ip">"</nts>
                  <nts id="Seg_568" n="HIAT:ip">,</nts>
                  <nts id="Seg_569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_571" n="HIAT:w" s="T151">diːr</ts>
                  <nts id="Seg_572" n="HIAT:ip">.</nts>
                  <nts id="Seg_573" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T156" id="Seg_575" n="HIAT:u" s="T152">
                  <ts e="T153" id="Seg_577" n="HIAT:w" s="T152">Egelle</ts>
                  <nts id="Seg_578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_580" n="HIAT:w" s="T153">dʼi͡etiger</ts>
                  <nts id="Seg_581" n="HIAT:ip">,</nts>
                  <nts id="Seg_582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_584" n="HIAT:w" s="T154">emeːksiniger</ts>
                  <nts id="Seg_585" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_587" n="HIAT:w" s="T155">kepsiːr</ts>
                  <nts id="Seg_588" n="HIAT:ip">.</nts>
                  <nts id="Seg_589" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T165" id="Seg_591" n="HIAT:u" s="T156">
                  <ts e="T157" id="Seg_593" n="HIAT:w" s="T156">Üs</ts>
                  <nts id="Seg_594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_596" n="HIAT:w" s="T157">kün</ts>
                  <nts id="Seg_597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_599" n="HIAT:w" s="T158">turkarɨ</ts>
                  <nts id="Seg_600" n="HIAT:ip">,</nts>
                  <nts id="Seg_601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_603" n="HIAT:w" s="T159">tabalarɨn</ts>
                  <nts id="Seg_604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_606" n="HIAT:w" s="T160">da</ts>
                  <nts id="Seg_607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_609" n="HIAT:w" s="T161">ɨːppakka</ts>
                  <nts id="Seg_610" n="HIAT:ip">,</nts>
                  <nts id="Seg_611" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_613" n="HIAT:w" s="T162">bu</ts>
                  <nts id="Seg_614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_616" n="HIAT:w" s="T163">ogonnon</ts>
                  <nts id="Seg_617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_619" n="HIAT:w" s="T164">oːnʼuːllar</ts>
                  <nts id="Seg_620" n="HIAT:ip">.</nts>
                  <nts id="Seg_621" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T170" id="Seg_623" n="HIAT:u" s="T165">
                  <ts e="T166" id="Seg_625" n="HIAT:w" s="T165">Ölgöbüːne</ts>
                  <nts id="Seg_626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_628" n="HIAT:w" s="T166">barɨta</ts>
                  <nts id="Seg_629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_631" n="HIAT:w" s="T167">kölülle</ts>
                  <nts id="Seg_632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_634" n="HIAT:w" s="T168">hɨtar</ts>
                  <nts id="Seg_635" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_637" n="HIAT:w" s="T169">ebit</ts>
                  <nts id="Seg_638" n="HIAT:ip">.</nts>
                  <nts id="Seg_639" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T173" id="Seg_641" n="HIAT:u" s="T170">
                  <ts e="T171" id="Seg_643" n="HIAT:w" s="T170">Tu͡ok</ts>
                  <nts id="Seg_644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_646" n="HIAT:w" s="T171">da</ts>
                  <nts id="Seg_647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_649" n="HIAT:w" s="T172">billibet</ts>
                  <nts id="Seg_650" n="HIAT:ip">.</nts>
                  <nts id="Seg_651" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T178" id="Seg_653" n="HIAT:u" s="T173">
                  <ts e="T174" id="Seg_655" n="HIAT:w" s="T173">Ogoto</ts>
                  <nts id="Seg_656" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_658" n="HIAT:w" s="T174">hüːreliː</ts>
                  <nts id="Seg_659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_661" n="HIAT:w" s="T175">hɨldʼar</ts>
                  <nts id="Seg_662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_664" n="HIAT:w" s="T176">bu͡olbut</ts>
                  <nts id="Seg_665" n="HIAT:ip">,</nts>
                  <nts id="Seg_666" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_668" n="HIAT:w" s="T177">ulaːppɨt</ts>
                  <nts id="Seg_669" n="HIAT:ip">.</nts>
                  <nts id="Seg_670" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T188" id="Seg_672" n="HIAT:u" s="T178">
                  <ts e="T179" id="Seg_674" n="HIAT:w" s="T178">Oduːluː</ts>
                  <nts id="Seg_675" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_677" n="HIAT:w" s="T179">olordoguna</ts>
                  <nts id="Seg_678" n="HIAT:ip">,</nts>
                  <nts id="Seg_679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_681" n="HIAT:w" s="T180">hɨrgaga</ts>
                  <nts id="Seg_682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_684" n="HIAT:w" s="T181">bi͡es</ts>
                  <nts id="Seg_685" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_687" n="HIAT:w" s="T182">čeːlkeː</ts>
                  <nts id="Seg_688" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_690" n="HIAT:w" s="T183">buːrdaːk</ts>
                  <nts id="Seg_691" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_693" n="HIAT:w" s="T184">čebis-čeːlkeː</ts>
                  <nts id="Seg_694" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_696" n="HIAT:w" s="T185">taŋastaːk</ts>
                  <nts id="Seg_697" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_699" n="HIAT:w" s="T186">kihi</ts>
                  <nts id="Seg_700" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_702" n="HIAT:w" s="T187">iher</ts>
                  <nts id="Seg_703" n="HIAT:ip">.</nts>
                  <nts id="Seg_704" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T190" id="Seg_706" n="HIAT:u" s="T188">
                  <ts e="T189" id="Seg_708" n="HIAT:w" s="T188">Kineːs</ts>
                  <nts id="Seg_709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_711" n="HIAT:w" s="T189">ebit</ts>
                  <nts id="Seg_712" n="HIAT:ip">.</nts>
                  <nts id="Seg_713" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T195" id="Seg_715" n="HIAT:u" s="T190">
                  <nts id="Seg_716" n="HIAT:ip">"</nts>
                  <ts e="T191" id="Seg_718" n="HIAT:w" s="T190">Dʼe</ts>
                  <nts id="Seg_719" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_721" n="HIAT:w" s="T191">tu͡okpun</ts>
                  <nts id="Seg_722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_724" n="HIAT:w" s="T192">bi͡eri͡emij</ts>
                  <nts id="Seg_725" n="HIAT:ip">"</nts>
                  <nts id="Seg_726" n="HIAT:ip">,</nts>
                  <nts id="Seg_727" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_729" n="HIAT:w" s="T193">diː</ts>
                  <nts id="Seg_730" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_732" n="HIAT:w" s="T194">hanɨːr</ts>
                  <nts id="Seg_733" n="HIAT:ip">.</nts>
                  <nts id="Seg_734" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T198" id="Seg_736" n="HIAT:u" s="T195">
                  <nts id="Seg_737" n="HIAT:ip">"</nts>
                  <ts e="T196" id="Seg_739" n="HIAT:w" s="T195">Ogonnʼor</ts>
                  <nts id="Seg_740" n="HIAT:ip">,</nts>
                  <nts id="Seg_741" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_743" n="HIAT:w" s="T196">kajdak</ts>
                  <nts id="Seg_744" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_746" n="HIAT:w" s="T197">olorduŋ</ts>
                  <nts id="Seg_747" n="HIAT:ip">?</nts>
                  <nts id="Seg_748" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T206" id="Seg_750" n="HIAT:u" s="T198">
                  <ts e="T199" id="Seg_752" n="HIAT:w" s="T198">Kajdak</ts>
                  <nts id="Seg_753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_755" n="HIAT:w" s="T199">mannɨk</ts>
                  <nts id="Seg_756" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_758" n="HIAT:w" s="T200">ör</ts>
                  <nts id="Seg_759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_761" n="HIAT:w" s="T201">bu͡ollum</ts>
                  <nts id="Seg_762" n="HIAT:ip">,</nts>
                  <nts id="Seg_763" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_765" n="HIAT:w" s="T202">ogolommukkun</ts>
                  <nts id="Seg_766" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_768" n="HIAT:w" s="T203">duː</ts>
                  <nts id="Seg_769" n="HIAT:ip">"</nts>
                  <nts id="Seg_770" n="HIAT:ip">,</nts>
                  <nts id="Seg_771" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_773" n="HIAT:w" s="T204">diːr</ts>
                  <nts id="Seg_774" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_776" n="HIAT:w" s="T205">kineːs</ts>
                  <nts id="Seg_777" n="HIAT:ip">.</nts>
                  <nts id="Seg_778" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T211" id="Seg_780" n="HIAT:u" s="T206">
                  <nts id="Seg_781" n="HIAT:ip">"</nts>
                  <ts e="T207" id="Seg_783" n="HIAT:w" s="T206">Kɨrdʼar</ts>
                  <nts id="Seg_784" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_786" n="HIAT:w" s="T207">haːspɨtɨgar</ts>
                  <nts id="Seg_787" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_789" n="HIAT:w" s="T208">ogolonnubut</ts>
                  <nts id="Seg_790" n="HIAT:ip">"</nts>
                  <nts id="Seg_791" n="HIAT:ip">,</nts>
                  <nts id="Seg_792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_794" n="HIAT:w" s="T209">diːr</ts>
                  <nts id="Seg_795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_797" n="HIAT:w" s="T210">ogonnʼor</ts>
                  <nts id="Seg_798" n="HIAT:ip">.</nts>
                  <nts id="Seg_799" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T213" id="Seg_801" n="HIAT:u" s="T211">
                  <nts id="Seg_802" n="HIAT:ip">"</nts>
                  <ts e="T212" id="Seg_804" n="HIAT:w" s="T211">Dʼe</ts>
                  <nts id="Seg_805" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_807" n="HIAT:w" s="T212">üčügej</ts>
                  <nts id="Seg_808" n="HIAT:ip">.</nts>
                  <nts id="Seg_809" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T218" id="Seg_811" n="HIAT:u" s="T213">
                  <ts e="T214" id="Seg_813" n="HIAT:w" s="T213">Nolu͡ok</ts>
                  <nts id="Seg_814" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_816" n="HIAT:w" s="T214">komuja</ts>
                  <nts id="Seg_817" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_819" n="HIAT:w" s="T215">kellim</ts>
                  <nts id="Seg_820" n="HIAT:ip">"</nts>
                  <nts id="Seg_821" n="HIAT:ip">,</nts>
                  <nts id="Seg_822" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_824" n="HIAT:w" s="T216">diːr</ts>
                  <nts id="Seg_825" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_827" n="HIAT:w" s="T217">kineːs</ts>
                  <nts id="Seg_828" n="HIAT:ip">.</nts>
                  <nts id="Seg_829" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T224" id="Seg_831" n="HIAT:u" s="T218">
                  <ts e="T219" id="Seg_833" n="HIAT:w" s="T218">Ogonnʼor</ts>
                  <nts id="Seg_834" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_836" n="HIAT:w" s="T219">töhö</ts>
                  <nts id="Seg_837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_839" n="HIAT:w" s="T220">eme</ts>
                  <nts id="Seg_840" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_842" n="HIAT:w" s="T221">küːl</ts>
                  <nts id="Seg_843" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_845" n="HIAT:w" s="T222">kɨrsanɨ</ts>
                  <nts id="Seg_846" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_848" n="HIAT:w" s="T223">bi͡erer</ts>
                  <nts id="Seg_849" n="HIAT:ip">.</nts>
                  <nts id="Seg_850" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T230" id="Seg_852" n="HIAT:u" s="T224">
                  <ts e="T225" id="Seg_854" n="HIAT:w" s="T224">Kineːhe</ts>
                  <nts id="Seg_855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_857" n="HIAT:w" s="T225">ü͡örer</ts>
                  <nts id="Seg_858" n="HIAT:ip">,</nts>
                  <nts id="Seg_859" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_860" n="HIAT:ip">"</nts>
                  <ts e="T227" id="Seg_862" n="HIAT:w" s="T226">ordugun</ts>
                  <nts id="Seg_863" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_865" n="HIAT:w" s="T227">karčiː</ts>
                  <nts id="Seg_866" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_868" n="HIAT:w" s="T228">ɨːtɨ͡am</ts>
                  <nts id="Seg_869" n="HIAT:ip">"</nts>
                  <nts id="Seg_870" n="HIAT:ip">,</nts>
                  <nts id="Seg_871" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_873" n="HIAT:w" s="T229">diːr</ts>
                  <nts id="Seg_874" n="HIAT:ip">.</nts>
                  <nts id="Seg_875" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T232" id="Seg_877" n="HIAT:u" s="T230">
                  <ts e="T231" id="Seg_879" n="HIAT:w" s="T230">Kineːhe</ts>
                  <nts id="Seg_880" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_882" n="HIAT:w" s="T231">kamnɨːhɨ</ts>
                  <nts id="Seg_883" n="HIAT:ip">.</nts>
                  <nts id="Seg_884" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T238" id="Seg_886" n="HIAT:u" s="T232">
                  <ts e="T233" id="Seg_888" n="HIAT:w" s="T232">Kanna</ts>
                  <nts id="Seg_889" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_891" n="HIAT:w" s="T233">ere</ts>
                  <nts id="Seg_892" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_894" n="HIAT:w" s="T234">dʼuraːk</ts>
                  <nts id="Seg_895" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_897" n="HIAT:w" s="T235">kineːhiger</ts>
                  <nts id="Seg_898" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_900" n="HIAT:w" s="T236">kötütte</ts>
                  <nts id="Seg_901" n="HIAT:ip">,</nts>
                  <nts id="Seg_902" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_904" n="HIAT:w" s="T237">dogorugar</ts>
                  <nts id="Seg_905" n="HIAT:ip">.</nts>
                  <nts id="Seg_906" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T241" id="Seg_908" n="HIAT:u" s="T238">
                  <ts e="T239" id="Seg_910" n="HIAT:w" s="T238">Kineːs</ts>
                  <nts id="Seg_911" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_913" n="HIAT:w" s="T239">kineːhiger</ts>
                  <nts id="Seg_914" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_916" n="HIAT:w" s="T240">kelbit</ts>
                  <nts id="Seg_917" n="HIAT:ip">.</nts>
                  <nts id="Seg_918" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T243" id="Seg_920" n="HIAT:u" s="T241">
                  <ts e="T242" id="Seg_922" n="HIAT:w" s="T241">Argilɨː</ts>
                  <nts id="Seg_923" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_925" n="HIAT:w" s="T242">hɨtallar</ts>
                  <nts id="Seg_926" n="HIAT:ip">.</nts>
                  <nts id="Seg_927" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T247" id="Seg_929" n="HIAT:u" s="T243">
                  <ts e="T244" id="Seg_931" n="HIAT:w" s="T243">Kas</ts>
                  <nts id="Seg_932" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_934" n="HIAT:w" s="T244">kün</ts>
                  <nts id="Seg_935" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_937" n="HIAT:w" s="T245">argilɨː</ts>
                  <nts id="Seg_938" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_940" n="HIAT:w" s="T246">hɨppɨttar</ts>
                  <nts id="Seg_941" n="HIAT:ip">.</nts>
                  <nts id="Seg_942" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T252" id="Seg_944" n="HIAT:u" s="T247">
                  <ts e="T248" id="Seg_946" n="HIAT:w" s="T247">Ol</ts>
                  <nts id="Seg_947" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_949" n="HIAT:w" s="T248">oloron</ts>
                  <nts id="Seg_950" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_952" n="HIAT:w" s="T249">ös</ts>
                  <nts id="Seg_953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_955" n="HIAT:w" s="T250">kepseːbit</ts>
                  <nts id="Seg_956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_958" n="HIAT:w" s="T251">dogorugar</ts>
                  <nts id="Seg_959" n="HIAT:ip">:</nts>
                  <nts id="Seg_960" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T257" id="Seg_962" n="HIAT:u" s="T252">
                  <nts id="Seg_963" n="HIAT:ip">"</nts>
                  <ts e="T253" id="Seg_965" n="HIAT:w" s="T252">Dʼe-dʼe</ts>
                  <nts id="Seg_966" n="HIAT:ip">,</nts>
                  <nts id="Seg_967" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_969" n="HIAT:w" s="T253">min</ts>
                  <nts id="Seg_970" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_972" n="HIAT:w" s="T254">kihi</ts>
                  <nts id="Seg_973" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_975" n="HIAT:w" s="T255">kuttanar</ts>
                  <nts id="Seg_976" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_978" n="HIAT:w" s="T256">kihileːkpin</ts>
                  <nts id="Seg_979" n="HIAT:ip">.</nts>
                  <nts id="Seg_980" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T262" id="Seg_982" n="HIAT:u" s="T257">
                  <ts e="T258" id="Seg_984" n="HIAT:w" s="T257">Nolu͡ogun</ts>
                  <nts id="Seg_985" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_987" n="HIAT:w" s="T258">ol</ts>
                  <nts id="Seg_988" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_990" n="HIAT:w" s="T259">ereːri</ts>
                  <nts id="Seg_991" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_993" n="HIAT:w" s="T260">aharɨ</ts>
                  <nts id="Seg_994" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_996" n="HIAT:w" s="T261">tölüːr</ts>
                  <nts id="Seg_997" n="HIAT:ip">.</nts>
                  <nts id="Seg_998" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T264" id="Seg_1000" n="HIAT:u" s="T262">
                  <ts e="T263" id="Seg_1002" n="HIAT:w" s="T262">Onton</ts>
                  <nts id="Seg_1003" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_1005" n="HIAT:w" s="T263">kellim</ts>
                  <nts id="Seg_1006" n="HIAT:ip">.</nts>
                  <nts id="Seg_1007" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T268" id="Seg_1009" n="HIAT:u" s="T264">
                  <ts e="T265" id="Seg_1011" n="HIAT:w" s="T264">Oduːrguːbun</ts>
                  <nts id="Seg_1012" n="HIAT:ip">—</nts>
                  <nts id="Seg_1013" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_1015" n="HIAT:w" s="T265">hüːre</ts>
                  <nts id="Seg_1016" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_1018" n="HIAT:w" s="T266">hɨldʼar</ts>
                  <nts id="Seg_1019" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_1021" n="HIAT:w" s="T267">ogolommut</ts>
                  <nts id="Seg_1022" n="HIAT:ip">.</nts>
                  <nts id="Seg_1023" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T272" id="Seg_1025" n="HIAT:u" s="T268">
                  <ts e="T269" id="Seg_1027" n="HIAT:w" s="T268">Bejete</ts>
                  <nts id="Seg_1028" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_1030" n="HIAT:w" s="T269">töröːbüt</ts>
                  <nts id="Seg_1031" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_1033" n="HIAT:w" s="T270">ogoto</ts>
                  <nts id="Seg_1034" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_1036" n="HIAT:w" s="T271">bu͡olbatak</ts>
                  <nts id="Seg_1037" n="HIAT:ip">.</nts>
                  <nts id="Seg_1038" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T275" id="Seg_1040" n="HIAT:u" s="T272">
                  <ts e="T273" id="Seg_1042" n="HIAT:w" s="T272">Kantan</ts>
                  <nts id="Seg_1043" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_1045" n="HIAT:w" s="T273">ɨlbɨta</ts>
                  <nts id="Seg_1046" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_1048" n="HIAT:w" s="T274">bu͡olu͡oj</ts>
                  <nts id="Seg_1049" n="HIAT:ip">?</nts>
                  <nts id="Seg_1050" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T280" id="Seg_1052" n="HIAT:u" s="T275">
                  <ts e="T276" id="Seg_1054" n="HIAT:w" s="T275">Kimi</ts>
                  <nts id="Seg_1055" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_1057" n="HIAT:w" s="T276">ere</ts>
                  <nts id="Seg_1058" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_1060" n="HIAT:w" s="T277">dʼürü</ts>
                  <nts id="Seg_1061" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_1063" n="HIAT:w" s="T278">ölörön</ts>
                  <nts id="Seg_1064" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_1066" n="HIAT:w" s="T279">ɨllaga</ts>
                  <nts id="Seg_1067" n="HIAT:ip">?</nts>
                  <nts id="Seg_1068" n="HIAT:ip">"</nts>
                  <nts id="Seg_1069" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T290" id="Seg_1071" n="HIAT:u" s="T280">
                  <nts id="Seg_1072" n="HIAT:ip">"</nts>
                  <ts e="T281" id="Seg_1074" n="HIAT:w" s="T280">Eːk</ts>
                  <nts id="Seg_1075" n="HIAT:ip">"</nts>
                  <nts id="Seg_1076" n="HIAT:ip">,</nts>
                  <nts id="Seg_1077" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_1079" n="HIAT:w" s="T281">diːr</ts>
                  <nts id="Seg_1080" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_1082" n="HIAT:w" s="T282">dʼuraːk</ts>
                  <nts id="Seg_1083" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_1085" n="HIAT:w" s="T283">kineːhe</ts>
                  <nts id="Seg_1086" n="HIAT:ip">,</nts>
                  <nts id="Seg_1087" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1088" n="HIAT:ip">"</nts>
                  <ts e="T285" id="Seg_1090" n="HIAT:w" s="T284">bɨlɨr</ts>
                  <nts id="Seg_1091" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_1093" n="HIAT:w" s="T285">min</ts>
                  <nts id="Seg_1094" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_1096" n="HIAT:w" s="T286">biːr</ts>
                  <nts id="Seg_1097" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_1099" n="HIAT:w" s="T287">munʼak</ts>
                  <nts id="Seg_1100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_1102" n="HIAT:w" s="T288">kihim</ts>
                  <nts id="Seg_1103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_1105" n="HIAT:w" s="T289">ölbüte</ts>
                  <nts id="Seg_1106" n="HIAT:ip">.</nts>
                  <nts id="Seg_1107" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T296" id="Seg_1109" n="HIAT:u" s="T290">
                  <ts e="T291" id="Seg_1111" n="HIAT:w" s="T290">Onton</ts>
                  <nts id="Seg_1112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_1114" n="HIAT:w" s="T291">biːr</ts>
                  <nts id="Seg_1115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_1117" n="HIAT:w" s="T292">bihikteːk</ts>
                  <nts id="Seg_1118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_1120" n="HIAT:w" s="T293">ogo</ts>
                  <nts id="Seg_1121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1123" n="HIAT:w" s="T294">kaːlbɨt</ts>
                  <nts id="Seg_1124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1126" n="HIAT:w" s="T295">huraktaːga</ts>
                  <nts id="Seg_1127" n="HIAT:ip">.</nts>
                  <nts id="Seg_1128" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T301" id="Seg_1130" n="HIAT:u" s="T296">
                  <ts e="T297" id="Seg_1132" n="HIAT:w" s="T296">Onu</ts>
                  <nts id="Seg_1133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_1135" n="HIAT:w" s="T297">ɨllarbatagɨm</ts>
                  <nts id="Seg_1136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_1138" n="HIAT:w" s="T298">ɨ͡arɨː</ts>
                  <nts id="Seg_1139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_1141" n="HIAT:w" s="T299">batɨhɨ͡a</ts>
                  <nts id="Seg_1142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_1144" n="HIAT:w" s="T300">di͡emmin</ts>
                  <nts id="Seg_1145" n="HIAT:ip">.</nts>
                  <nts id="Seg_1146" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T303" id="Seg_1148" n="HIAT:u" s="T301">
                  <ts e="T302" id="Seg_1150" n="HIAT:w" s="T301">Onu</ts>
                  <nts id="Seg_1151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1153" n="HIAT:w" s="T302">ɨllaga</ts>
                  <nts id="Seg_1154" n="HIAT:ip">.</nts>
                  <nts id="Seg_1155" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T307" id="Seg_1157" n="HIAT:u" s="T303">
                  <ts e="T304" id="Seg_1159" n="HIAT:w" s="T303">Ol</ts>
                  <nts id="Seg_1160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_1162" n="HIAT:w" s="T304">ogonu</ts>
                  <nts id="Seg_1163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_1165" n="HIAT:w" s="T305">kisti͡ebit</ts>
                  <nts id="Seg_1166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_1168" n="HIAT:w" s="T306">ebit</ts>
                  <nts id="Seg_1169" n="HIAT:ip">.</nts>
                  <nts id="Seg_1170" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T310" id="Seg_1172" n="HIAT:u" s="T307">
                  <nts id="Seg_1173" n="HIAT:ip">"</nts>
                  <ts e="T308" id="Seg_1175" n="HIAT:w" s="T307">Miːgin</ts>
                  <nts id="Seg_1176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1178" n="HIAT:w" s="T308">ölörü͡öktere</ts>
                  <nts id="Seg_1179" n="HIAT:ip">"</nts>
                  <nts id="Seg_1180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_1182" n="HIAT:w" s="T309">di͡en</ts>
                  <nts id="Seg_1183" n="HIAT:ip">.</nts>
                  <nts id="Seg_1184" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T312" id="Seg_1186" n="HIAT:u" s="T310">
                  <ts e="T311" id="Seg_1188" n="HIAT:w" s="T310">Dʼe</ts>
                  <nts id="Seg_1189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_1191" n="HIAT:w" s="T311">bugurduk</ts>
                  <nts id="Seg_1192" n="HIAT:ip">.</nts>
                  <nts id="Seg_1193" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T316" id="Seg_1195" n="HIAT:u" s="T312">
                  <ts e="T313" id="Seg_1197" n="HIAT:w" s="T312">Hübeliːbin</ts>
                  <nts id="Seg_1198" n="HIAT:ip">,</nts>
                  <nts id="Seg_1199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_1201" n="HIAT:w" s="T313">en</ts>
                  <nts id="Seg_1202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_1204" n="HIAT:w" s="T314">kihigitten</ts>
                  <nts id="Seg_1205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1207" n="HIAT:w" s="T315">kuttanɨma</ts>
                  <nts id="Seg_1208" n="HIAT:ip">.</nts>
                  <nts id="Seg_1209" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T329" id="Seg_1211" n="HIAT:u" s="T316">
                  <ts e="T317" id="Seg_1213" n="HIAT:w" s="T316">Ol</ts>
                  <nts id="Seg_1214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_1216" n="HIAT:w" s="T317">munnʼak</ts>
                  <nts id="Seg_1217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1219" n="HIAT:w" s="T318">dʼonun</ts>
                  <nts id="Seg_1220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_1222" n="HIAT:w" s="T319">kɨrgan</ts>
                  <nts id="Seg_1223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1225" n="HIAT:w" s="T320">baran</ts>
                  <nts id="Seg_1226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_1228" n="HIAT:w" s="T321">ogonu</ts>
                  <nts id="Seg_1229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1231" n="HIAT:w" s="T322">ɨlbɨt</ts>
                  <nts id="Seg_1232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_1234" n="HIAT:w" s="T323">di͡en</ts>
                  <nts id="Seg_1235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1237" n="HIAT:w" s="T324">ikki</ts>
                  <nts id="Seg_1238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_1240" n="HIAT:w" s="T325">kineːs</ts>
                  <nts id="Seg_1241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1243" n="HIAT:w" s="T326">ɨraːktaːgɨta</ts>
                  <nts id="Seg_1244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_1246" n="HIAT:w" s="T327">üŋsen</ts>
                  <nts id="Seg_1247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_1249" n="HIAT:w" s="T328">huruju͡ok</ts>
                  <nts id="Seg_1250" n="HIAT:ip">.</nts>
                  <nts id="Seg_1251" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T346" id="Seg_1253" n="HIAT:u" s="T329">
                  <ts e="T330" id="Seg_1255" n="HIAT:w" s="T329">Oččogo</ts>
                  <nts id="Seg_1256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_1258" n="HIAT:w" s="T330">bihigi</ts>
                  <nts id="Seg_1259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_1261" n="HIAT:w" s="T331">tɨlbɨtɨn</ts>
                  <nts id="Seg_1262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_1264" n="HIAT:w" s="T332">ɨraːktaːgɨ</ts>
                  <nts id="Seg_1265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_1267" n="HIAT:w" s="T333">bɨhɨ͡a</ts>
                  <nts id="Seg_1268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_1270" n="HIAT:w" s="T334">hu͡oga</ts>
                  <nts id="Seg_1271" n="HIAT:ip">,</nts>
                  <nts id="Seg_1272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_1274" n="HIAT:w" s="T335">kinini</ts>
                  <nts id="Seg_1275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_1277" n="HIAT:w" s="T336">huːttatɨ͡a</ts>
                  <nts id="Seg_1278" n="HIAT:ip">,</nts>
                  <nts id="Seg_1279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_1281" n="HIAT:w" s="T337">kaːjɨːga</ts>
                  <nts id="Seg_1282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_1284" n="HIAT:w" s="T338">ɨːtɨ͡aga</ts>
                  <nts id="Seg_1285" n="HIAT:ip">,</nts>
                  <nts id="Seg_1286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_1288" n="HIAT:w" s="T339">bihigi</ts>
                  <nts id="Seg_1289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_1291" n="HIAT:w" s="T340">kini</ts>
                  <nts id="Seg_1292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_1294" n="HIAT:w" s="T341">baːjɨn</ts>
                  <nts id="Seg_1295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1297" n="HIAT:w" s="T342">ɨlɨ͡akpɨt</ts>
                  <nts id="Seg_1298" n="HIAT:ip">"</nts>
                  <nts id="Seg_1299" n="HIAT:ip">,</nts>
                  <nts id="Seg_1300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_1302" n="HIAT:w" s="T343">dʼi͡ebit</ts>
                  <nts id="Seg_1303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_1305" n="HIAT:w" s="T344">dʼuraːk</ts>
                  <nts id="Seg_1306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_1308" n="HIAT:w" s="T345">kini͡ehe</ts>
                  <nts id="Seg_1309" n="HIAT:ip">.</nts>
                  <nts id="Seg_1310" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T353" id="Seg_1312" n="HIAT:u" s="T346">
                  <nts id="Seg_1313" n="HIAT:ip">"</nts>
                  <ts e="T347" id="Seg_1315" n="HIAT:w" s="T346">Kihi͡eke</ts>
                  <nts id="Seg_1316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1318" n="HIAT:w" s="T347">kütüreːtekke</ts>
                  <nts id="Seg_1319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_1321" n="HIAT:w" s="T348">kuhagan</ts>
                  <nts id="Seg_1322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_1324" n="HIAT:w" s="T349">bu͡olaːraj</ts>
                  <nts id="Seg_1325" n="HIAT:ip">"</nts>
                  <nts id="Seg_1326" n="HIAT:ip">,</nts>
                  <nts id="Seg_1327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_1329" n="HIAT:w" s="T350">diːr</ts>
                  <nts id="Seg_1330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_1332" n="HIAT:w" s="T351">haːmaj</ts>
                  <nts id="Seg_1333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_1335" n="HIAT:w" s="T352">kineːhe</ts>
                  <nts id="Seg_1336" n="HIAT:ip">.</nts>
                  <nts id="Seg_1337" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T362" id="Seg_1339" n="HIAT:u" s="T353">
                  <nts id="Seg_1340" n="HIAT:ip">"</nts>
                  <ts e="T354" id="Seg_1342" n="HIAT:w" s="T353">Tɨ͡a</ts>
                  <nts id="Seg_1343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_1345" n="HIAT:w" s="T354">kineːhin</ts>
                  <nts id="Seg_1346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_1348" n="HIAT:w" s="T355">ɨgɨrbɨt</ts>
                  <nts id="Seg_1349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_1351" n="HIAT:w" s="T356">bu͡ollar</ts>
                  <nts id="Seg_1352" n="HIAT:ip">,</nts>
                  <nts id="Seg_1353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_1355" n="HIAT:w" s="T357">üs</ts>
                  <nts id="Seg_1356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_1358" n="HIAT:w" s="T358">kineːs</ts>
                  <nts id="Seg_1359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_1361" n="HIAT:w" s="T359">itegelleːk</ts>
                  <nts id="Seg_1362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_1364" n="HIAT:w" s="T360">bu͡olu͡ok</ts>
                  <nts id="Seg_1365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_1367" n="HIAT:w" s="T361">ete</ts>
                  <nts id="Seg_1368" n="HIAT:ip">.</nts>
                  <nts id="Seg_1369" n="HIAT:ip">"</nts>
                  <nts id="Seg_1370" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T364" id="Seg_1372" n="HIAT:u" s="T362">
                  <ts e="T363" id="Seg_1374" n="HIAT:w" s="T362">Dʼuraːk</ts>
                  <nts id="Seg_1375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_1377" n="HIAT:w" s="T363">kineːhe</ts>
                  <nts id="Seg_1378" n="HIAT:ip">:</nts>
                  <nts id="Seg_1379" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T371" id="Seg_1381" n="HIAT:u" s="T364">
                  <nts id="Seg_1382" n="HIAT:ip">"</nts>
                  <ts e="T365" id="Seg_1384" n="HIAT:w" s="T364">Kihite</ts>
                  <nts id="Seg_1385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_1387" n="HIAT:w" s="T365">ɨːtɨ͡am</ts>
                  <nts id="Seg_1388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_1390" n="HIAT:w" s="T366">tɨ͡alar</ts>
                  <nts id="Seg_1391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_1393" n="HIAT:w" s="T367">kineːsteriger</ts>
                  <nts id="Seg_1394" n="HIAT:ip">,</nts>
                  <nts id="Seg_1395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_1397" n="HIAT:w" s="T368">üs</ts>
                  <nts id="Seg_1398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_1400" n="HIAT:w" s="T369">konugunan</ts>
                  <nts id="Seg_1401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_1403" n="HIAT:w" s="T370">keli͡e</ts>
                  <nts id="Seg_1404" n="HIAT:ip">.</nts>
                  <nts id="Seg_1405" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T374" id="Seg_1407" n="HIAT:u" s="T371">
                  <ts e="T372" id="Seg_1409" n="HIAT:w" s="T371">Höbülemmetegine</ts>
                  <nts id="Seg_1410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_1412" n="HIAT:w" s="T372">bejetin</ts>
                  <nts id="Seg_1413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_1415" n="HIAT:w" s="T373">ölörüllü͡ö</ts>
                  <nts id="Seg_1416" n="HIAT:ip">.</nts>
                  <nts id="Seg_1417" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T377" id="Seg_1419" n="HIAT:u" s="T374">
                  <ts e="T375" id="Seg_1421" n="HIAT:w" s="T374">Utarsɨ͡a</ts>
                  <nts id="Seg_1422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_1424" n="HIAT:w" s="T375">hu͡oga</ts>
                  <nts id="Seg_1425" n="HIAT:ip">"</nts>
                  <nts id="Seg_1426" n="HIAT:ip">,</nts>
                  <nts id="Seg_1427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_1429" n="HIAT:w" s="T376">diːr</ts>
                  <nts id="Seg_1430" n="HIAT:ip">.</nts>
                  <nts id="Seg_1431" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T383" id="Seg_1433" n="HIAT:u" s="T377">
                  <ts e="T378" id="Seg_1435" n="HIAT:w" s="T377">Hɨrgalaːk</ts>
                  <nts id="Seg_1436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_1438" n="HIAT:w" s="T378">kihini</ts>
                  <nts id="Seg_1439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_1441" n="HIAT:w" s="T379">ɨːtta</ts>
                  <nts id="Seg_1442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_1444" n="HIAT:w" s="T380">tɨ͡a</ts>
                  <nts id="Seg_1445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T382" id="Seg_1447" n="HIAT:w" s="T381">kineːhin</ts>
                  <nts id="Seg_1448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_1450" n="HIAT:w" s="T382">ɨgɨrtara</ts>
                  <nts id="Seg_1451" n="HIAT:ip">.</nts>
                  <nts id="Seg_1452" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T390" id="Seg_1454" n="HIAT:u" s="T383">
                  <ts e="T384" id="Seg_1456" n="HIAT:w" s="T383">Ühüs</ts>
                  <nts id="Seg_1457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_1459" n="HIAT:w" s="T384">künüger</ts>
                  <nts id="Seg_1460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T386" id="Seg_1462" n="HIAT:w" s="T385">tɨ͡a</ts>
                  <nts id="Seg_1463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_1465" n="HIAT:w" s="T386">kineːhe</ts>
                  <nts id="Seg_1466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_1468" n="HIAT:w" s="T387">keler</ts>
                  <nts id="Seg_1469" n="HIAT:ip">,</nts>
                  <nts id="Seg_1470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_1472" n="HIAT:w" s="T388">oguru͡o</ts>
                  <nts id="Seg_1473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_1475" n="HIAT:w" s="T389">taŋastaːk</ts>
                  <nts id="Seg_1476" n="HIAT:ip">.</nts>
                  <nts id="Seg_1477" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T394" id="Seg_1479" n="HIAT:u" s="T390">
                  <ts e="T391" id="Seg_1481" n="HIAT:w" s="T390">Üs</ts>
                  <nts id="Seg_1482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_1484" n="HIAT:w" s="T391">kas</ts>
                  <nts id="Seg_1485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T393" id="Seg_1487" n="HIAT:w" s="T392">kün</ts>
                  <nts id="Seg_1488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_1490" n="HIAT:w" s="T393">argilɨːllar</ts>
                  <nts id="Seg_1491" n="HIAT:ip">.</nts>
                  <nts id="Seg_1492" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T397" id="Seg_1494" n="HIAT:u" s="T394">
                  <ts e="T395" id="Seg_1496" n="HIAT:w" s="T394">Dʼuraːk</ts>
                  <nts id="Seg_1497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T396" id="Seg_1499" n="HIAT:w" s="T395">kineːhe</ts>
                  <nts id="Seg_1500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_1502" n="HIAT:w" s="T396">kepsiːr</ts>
                  <nts id="Seg_1503" n="HIAT:ip">:</nts>
                  <nts id="Seg_1504" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T405" id="Seg_1506" n="HIAT:u" s="T397">
                  <nts id="Seg_1507" n="HIAT:ip">"</nts>
                  <ts e="T398" id="Seg_1509" n="HIAT:w" s="T397">Haːmaj</ts>
                  <nts id="Seg_1510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_1512" n="HIAT:w" s="T398">kihite</ts>
                  <nts id="Seg_1513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T400" id="Seg_1515" n="HIAT:w" s="T399">ɨ͡arɨːttan</ts>
                  <nts id="Seg_1516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_1518" n="HIAT:w" s="T400">ölbüt</ts>
                  <nts id="Seg_1519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T402" id="Seg_1521" n="HIAT:w" s="T401">dʼonton</ts>
                  <nts id="Seg_1522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_1524" n="HIAT:w" s="T402">biːr</ts>
                  <nts id="Seg_1525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T404" id="Seg_1527" n="HIAT:w" s="T403">ogonu</ts>
                  <nts id="Seg_1528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T405" id="Seg_1530" n="HIAT:w" s="T404">ɨlbɨt</ts>
                  <nts id="Seg_1531" n="HIAT:ip">.</nts>
                  <nts id="Seg_1532" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T408" id="Seg_1534" n="HIAT:u" s="T405">
                  <ts e="T406" id="Seg_1536" n="HIAT:w" s="T405">Onu</ts>
                  <nts id="Seg_1537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T407" id="Seg_1539" n="HIAT:w" s="T406">ɨ͡arɨː</ts>
                  <nts id="Seg_1540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T408" id="Seg_1542" n="HIAT:w" s="T407">batɨhɨ͡a</ts>
                  <nts id="Seg_1543" n="HIAT:ip">.</nts>
                  <nts id="Seg_1544" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T413" id="Seg_1546" n="HIAT:u" s="T408">
                  <ts e="T409" id="Seg_1548" n="HIAT:w" s="T408">Bihigi</ts>
                  <nts id="Seg_1549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_1551" n="HIAT:w" s="T409">ɨraːktaːgɨga</ts>
                  <nts id="Seg_1552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T411" id="Seg_1554" n="HIAT:w" s="T410">üŋsü͡ögüŋ</ts>
                  <nts id="Seg_1555" n="HIAT:ip">,</nts>
                  <nts id="Seg_1556" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T412" id="Seg_1558" n="HIAT:w" s="T411">ühü͡ön</ts>
                  <nts id="Seg_1559" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T413" id="Seg_1561" n="HIAT:w" s="T412">huruju͡oguŋ</ts>
                  <nts id="Seg_1562" n="HIAT:ip">.</nts>
                  <nts id="Seg_1563" n="HIAT:ip">"</nts>
                  <nts id="Seg_1564" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T417" id="Seg_1566" n="HIAT:u" s="T413">
                  <ts e="T414" id="Seg_1568" n="HIAT:w" s="T413">Tɨ͡a</ts>
                  <nts id="Seg_1569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T415" id="Seg_1571" n="HIAT:w" s="T414">kineːhe</ts>
                  <nts id="Seg_1572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T416" id="Seg_1574" n="HIAT:w" s="T415">duːmajdaːbɨt</ts>
                  <nts id="Seg_1575" n="HIAT:ip">,</nts>
                  <nts id="Seg_1576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T417" id="Seg_1578" n="HIAT:w" s="T416">dʼi͡ebit</ts>
                  <nts id="Seg_1579" n="HIAT:ip">:</nts>
                  <nts id="Seg_1580" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T419" id="Seg_1582" n="HIAT:u" s="T417">
                  <nts id="Seg_1583" n="HIAT:ip">"</nts>
                  <ts e="T418" id="Seg_1585" n="HIAT:w" s="T417">Huːttuːrun</ts>
                  <nts id="Seg_1586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T419" id="Seg_1588" n="HIAT:w" s="T418">huːttaːmɨja</ts>
                  <nts id="Seg_1589" n="HIAT:ip">.</nts>
                  <nts id="Seg_1590" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T427" id="Seg_1592" n="HIAT:u" s="T419">
                  <ts e="T420" id="Seg_1594" n="HIAT:w" s="T419">Ol</ts>
                  <nts id="Seg_1595" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_1597" n="HIAT:w" s="T420">ereːri</ts>
                  <nts id="Seg_1598" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T422" id="Seg_1600" n="HIAT:w" s="T421">kütürüːr</ts>
                  <nts id="Seg_1601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T423" id="Seg_1603" n="HIAT:w" s="T422">hüre</ts>
                  <nts id="Seg_1604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T424" id="Seg_1606" n="HIAT:w" s="T423">bert</ts>
                  <nts id="Seg_1607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T425" id="Seg_1609" n="HIAT:w" s="T424">dʼonu</ts>
                  <nts id="Seg_1610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T426" id="Seg_1612" n="HIAT:w" s="T425">kejbit</ts>
                  <nts id="Seg_1613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T427" id="Seg_1615" n="HIAT:w" s="T426">di͡en</ts>
                  <nts id="Seg_1616" n="HIAT:ip">.</nts>
                  <nts id="Seg_1617" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T434" id="Seg_1619" n="HIAT:u" s="T427">
                  <ts e="T428" id="Seg_1621" n="HIAT:w" s="T427">Tulaːjagi</ts>
                  <nts id="Seg_1622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T429" id="Seg_1624" n="HIAT:w" s="T428">ɨlbɨta</ts>
                  <nts id="Seg_1625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_1627" n="HIAT:w" s="T429">tu͡ok</ts>
                  <nts id="Seg_1628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T431" id="Seg_1630" n="HIAT:w" s="T430">burujaj</ts>
                  <nts id="Seg_1631" n="HIAT:ip">,</nts>
                  <nts id="Seg_1632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_1634" n="HIAT:w" s="T431">huːkka</ts>
                  <nts id="Seg_1635" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T433" id="Seg_1637" n="HIAT:w" s="T432">bi͡eri͡ekpitin</ts>
                  <nts id="Seg_1638" n="HIAT:ip">"</nts>
                  <nts id="Seg_1639" n="HIAT:ip">,</nts>
                  <nts id="Seg_1640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T434" id="Seg_1642" n="HIAT:w" s="T433">dʼi͡ebit</ts>
                  <nts id="Seg_1643" n="HIAT:ip">.</nts>
                  <nts id="Seg_1644" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T436" id="Seg_1646" n="HIAT:u" s="T434">
                  <ts e="T435" id="Seg_1648" n="HIAT:w" s="T434">Dʼuraːk</ts>
                  <nts id="Seg_1649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_1651" n="HIAT:w" s="T435">kineːhe</ts>
                  <nts id="Seg_1652" n="HIAT:ip">:</nts>
                  <nts id="Seg_1653" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T441" id="Seg_1655" n="HIAT:u" s="T436">
                  <nts id="Seg_1656" n="HIAT:ip">"</nts>
                  <ts e="T437" id="Seg_1658" n="HIAT:w" s="T436">En</ts>
                  <nts id="Seg_1659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T438" id="Seg_1661" n="HIAT:w" s="T437">kömüsküːgün</ts>
                  <nts id="Seg_1662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T439" id="Seg_1664" n="HIAT:w" s="T438">du͡o</ts>
                  <nts id="Seg_1665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440" id="Seg_1667" n="HIAT:w" s="T439">ol</ts>
                  <nts id="Seg_1668" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_1670" n="HIAT:w" s="T440">kihini</ts>
                  <nts id="Seg_1671" n="HIAT:ip">?</nts>
                  <nts id="Seg_1672" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T445" id="Seg_1674" n="HIAT:u" s="T441">
                  <ts e="T442" id="Seg_1676" n="HIAT:w" s="T441">Oččogo</ts>
                  <nts id="Seg_1677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T443" id="Seg_1679" n="HIAT:w" s="T442">bihigi</ts>
                  <nts id="Seg_1680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T444" id="Seg_1682" n="HIAT:w" s="T443">enʼigin</ts>
                  <nts id="Seg_1683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T445" id="Seg_1685" n="HIAT:w" s="T444">kejebit</ts>
                  <nts id="Seg_1686" n="HIAT:ip">.</nts>
                  <nts id="Seg_1687" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T448" id="Seg_1689" n="HIAT:u" s="T445">
                  <ts e="T446" id="Seg_1691" n="HIAT:w" s="T445">Baːjgɨn</ts>
                  <nts id="Seg_1692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T447" id="Seg_1694" n="HIAT:w" s="T446">ɨlɨ͡akpɨt</ts>
                  <nts id="Seg_1695" n="HIAT:ip">"</nts>
                  <nts id="Seg_1696" n="HIAT:ip">,</nts>
                  <nts id="Seg_1697" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T448" id="Seg_1699" n="HIAT:w" s="T447">diːr</ts>
                  <nts id="Seg_1700" n="HIAT:ip">.</nts>
                  <nts id="Seg_1701" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T451" id="Seg_1703" n="HIAT:u" s="T448">
                  <ts e="T449" id="Seg_1705" n="HIAT:w" s="T448">Tɨ͡a</ts>
                  <nts id="Seg_1706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T450" id="Seg_1708" n="HIAT:w" s="T449">kineːhe</ts>
                  <nts id="Seg_1709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T451" id="Seg_1711" n="HIAT:w" s="T450">kuttammɨt</ts>
                  <nts id="Seg_1712" n="HIAT:ip">.</nts>
                  <nts id="Seg_1713" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T454" id="Seg_1715" n="HIAT:u" s="T451">
                  <ts e="T452" id="Seg_1717" n="HIAT:w" s="T451">Ölörü͡öksüt</ts>
                  <nts id="Seg_1718" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T453" id="Seg_1720" n="HIAT:w" s="T452">dʼon</ts>
                  <nts id="Seg_1721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T454" id="Seg_1723" n="HIAT:w" s="T453">ikki͡en</ts>
                  <nts id="Seg_1724" n="HIAT:ip">.</nts>
                  <nts id="Seg_1725" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T458" id="Seg_1727" n="HIAT:u" s="T454">
                  <nts id="Seg_1728" n="HIAT:ip">"</nts>
                  <ts e="T455" id="Seg_1730" n="HIAT:w" s="T454">Dʼe</ts>
                  <nts id="Seg_1731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T456" id="Seg_1733" n="HIAT:w" s="T455">kajtak</ts>
                  <nts id="Seg_1734" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T457" id="Seg_1736" n="HIAT:w" s="T456">hübeliːgit</ts>
                  <nts id="Seg_1737" n="HIAT:ip">"</nts>
                  <nts id="Seg_1738" n="HIAT:ip">,</nts>
                  <nts id="Seg_1739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T458" id="Seg_1741" n="HIAT:w" s="T457">dʼi͡ebit</ts>
                  <nts id="Seg_1742" n="HIAT:ip">.</nts>
                  <nts id="Seg_1743" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T460" id="Seg_1745" n="HIAT:u" s="T458">
                  <ts e="T459" id="Seg_1747" n="HIAT:w" s="T458">Dʼuraːk</ts>
                  <nts id="Seg_1748" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T460" id="Seg_1750" n="HIAT:w" s="T459">kineːhe</ts>
                  <nts id="Seg_1751" n="HIAT:ip">:</nts>
                  <nts id="Seg_1752" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T464" id="Seg_1754" n="HIAT:u" s="T460">
                  <nts id="Seg_1755" n="HIAT:ip">"</nts>
                  <ts e="T461" id="Seg_1757" n="HIAT:w" s="T460">Min</ts>
                  <nts id="Seg_1758" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T462" id="Seg_1760" n="HIAT:w" s="T461">tojon</ts>
                  <nts id="Seg_1761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T463" id="Seg_1763" n="HIAT:w" s="T462">hübehit</ts>
                  <nts id="Seg_1764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T464" id="Seg_1766" n="HIAT:w" s="T463">bu͡olabɨn</ts>
                  <nts id="Seg_1767" n="HIAT:ip">.</nts>
                  <nts id="Seg_1768" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T476" id="Seg_1770" n="HIAT:u" s="T464">
                  <ts e="T465" id="Seg_1772" n="HIAT:w" s="T464">Üŋsü͡ögüŋ</ts>
                  <nts id="Seg_1773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T466" id="Seg_1775" n="HIAT:w" s="T465">ɨraːktaːgɨga</ts>
                  <nts id="Seg_1776" n="HIAT:ip">,</nts>
                  <nts id="Seg_1777" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T467" id="Seg_1779" n="HIAT:w" s="T466">bu</ts>
                  <nts id="Seg_1780" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T468" id="Seg_1782" n="HIAT:w" s="T467">kohuːn</ts>
                  <nts id="Seg_1783" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T469" id="Seg_1785" n="HIAT:w" s="T468">haːmaj</ts>
                  <nts id="Seg_1786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T470" id="Seg_1788" n="HIAT:w" s="T469">munnʼagɨn</ts>
                  <nts id="Seg_1789" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T471" id="Seg_1791" n="HIAT:w" s="T470">kejen</ts>
                  <nts id="Seg_1792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T472" id="Seg_1794" n="HIAT:w" s="T471">baran</ts>
                  <nts id="Seg_1795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T473" id="Seg_1797" n="HIAT:w" s="T472">ogonu</ts>
                  <nts id="Seg_1798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T474" id="Seg_1800" n="HIAT:w" s="T473">ɨlbɨt</ts>
                  <nts id="Seg_1801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T475" id="Seg_1803" n="HIAT:w" s="T474">di͡en</ts>
                  <nts id="Seg_1804" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T476" id="Seg_1806" n="HIAT:w" s="T475">huruju͡okput</ts>
                  <nts id="Seg_1807" n="HIAT:ip">.</nts>
                  <nts id="Seg_1808" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T487" id="Seg_1810" n="HIAT:u" s="T476">
                  <ts e="T477" id="Seg_1812" n="HIAT:w" s="T476">Min</ts>
                  <nts id="Seg_1813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T478" id="Seg_1815" n="HIAT:w" s="T477">kihilerim</ts>
                  <nts id="Seg_1816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T479" id="Seg_1818" n="HIAT:w" s="T478">baran</ts>
                  <nts id="Seg_1819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T480" id="Seg_1821" n="HIAT:w" s="T479">ölbüt</ts>
                  <nts id="Seg_1822" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T481" id="Seg_1824" n="HIAT:w" s="T480">dʼonu</ts>
                  <nts id="Seg_1825" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T482" id="Seg_1827" n="HIAT:w" s="T481">ɨtɨ͡alɨ͡aktara</ts>
                  <nts id="Seg_1828" n="HIAT:ip">,</nts>
                  <nts id="Seg_1829" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T483" id="Seg_1831" n="HIAT:w" s="T482">komissija</ts>
                  <nts id="Seg_1832" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T484" id="Seg_1834" n="HIAT:w" s="T483">kellegine</ts>
                  <nts id="Seg_1835" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T485" id="Seg_1837" n="HIAT:w" s="T484">bulan</ts>
                  <nts id="Seg_1838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T486" id="Seg_1840" n="HIAT:w" s="T485">ɨlɨ͡a</ts>
                  <nts id="Seg_1841" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T487" id="Seg_1843" n="HIAT:w" s="T486">hu͡oga</ts>
                  <nts id="Seg_1844" n="HIAT:ip">.</nts>
                  <nts id="Seg_1845" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T490" id="Seg_1847" n="HIAT:u" s="T487">
                  <ts e="T488" id="Seg_1849" n="HIAT:w" s="T487">Kim</ts>
                  <nts id="Seg_1850" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T489" id="Seg_1852" n="HIAT:w" s="T488">taːjɨ͡aj</ts>
                  <nts id="Seg_1853" n="HIAT:ip">"</nts>
                  <nts id="Seg_1854" n="HIAT:ip">,</nts>
                  <nts id="Seg_1855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T490" id="Seg_1857" n="HIAT:w" s="T489">diːr</ts>
                  <nts id="Seg_1858" n="HIAT:ip">.</nts>
                  <nts id="Seg_1859" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T497" id="Seg_1861" n="HIAT:u" s="T490">
                  <ts e="T491" id="Seg_1863" n="HIAT:w" s="T490">U͡onča</ts>
                  <nts id="Seg_1864" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T492" id="Seg_1866" n="HIAT:w" s="T491">kihi</ts>
                  <nts id="Seg_1867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T493" id="Seg_1869" n="HIAT:w" s="T492">baran</ts>
                  <nts id="Seg_1870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T494" id="Seg_1872" n="HIAT:w" s="T493">bɨlɨr</ts>
                  <nts id="Seg_1873" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T495" id="Seg_1875" n="HIAT:w" s="T494">ölbüt</ts>
                  <nts id="Seg_1876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T496" id="Seg_1878" n="HIAT:w" s="T495">dʼonu</ts>
                  <nts id="Seg_1879" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T497" id="Seg_1881" n="HIAT:w" s="T496">ɨtɨ͡alɨːllar</ts>
                  <nts id="Seg_1882" n="HIAT:ip">.</nts>
                  <nts id="Seg_1883" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T500" id="Seg_1885" n="HIAT:u" s="T497">
                  <ts e="T498" id="Seg_1887" n="HIAT:w" s="T497">Hurujan</ts>
                  <nts id="Seg_1888" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T499" id="Seg_1890" n="HIAT:w" s="T498">keːstiler</ts>
                  <nts id="Seg_1891" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T500" id="Seg_1893" n="HIAT:w" s="T499">ogorduk</ts>
                  <nts id="Seg_1894" n="HIAT:ip">.</nts>
                  <nts id="Seg_1895" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T505" id="Seg_1897" n="HIAT:u" s="T500">
                  <ts e="T501" id="Seg_1899" n="HIAT:w" s="T500">Haːmaj</ts>
                  <nts id="Seg_1900" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T502" id="Seg_1902" n="HIAT:w" s="T501">kineːhin</ts>
                  <nts id="Seg_1903" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T503" id="Seg_1905" n="HIAT:w" s="T502">ɨːtallar</ts>
                  <nts id="Seg_1906" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T504" id="Seg_1908" n="HIAT:w" s="T503">dʼuraːk</ts>
                  <nts id="Seg_1909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T505" id="Seg_1911" n="HIAT:w" s="T504">kineːhiniːn</ts>
                  <nts id="Seg_1912" n="HIAT:ip">.</nts>
                  <nts id="Seg_1913" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T515" id="Seg_1915" n="HIAT:u" s="T505">
                  <ts e="T506" id="Seg_1917" n="HIAT:w" s="T505">ɨraːktaːgɨ</ts>
                  <nts id="Seg_1918" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T507" id="Seg_1920" n="HIAT:w" s="T506">aːgan</ts>
                  <nts id="Seg_1921" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T508" id="Seg_1923" n="HIAT:w" s="T507">körör</ts>
                  <nts id="Seg_1924" n="HIAT:ip">,</nts>
                  <nts id="Seg_1925" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T509" id="Seg_1927" n="HIAT:w" s="T508">u͡onča</ts>
                  <nts id="Seg_1928" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T510" id="Seg_1930" n="HIAT:w" s="T509">dʼi͡e</ts>
                  <nts id="Seg_1931" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T511" id="Seg_1933" n="HIAT:w" s="T510">dʼonu</ts>
                  <nts id="Seg_1934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T512" id="Seg_1936" n="HIAT:w" s="T511">kejbit</ts>
                  <nts id="Seg_1937" n="HIAT:ip">,</nts>
                  <nts id="Seg_1938" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T513" id="Seg_1940" n="HIAT:w" s="T512">ogonu</ts>
                  <nts id="Seg_1941" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T514" id="Seg_1943" n="HIAT:w" s="T513">u͡orbut</ts>
                  <nts id="Seg_1944" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T515" id="Seg_1946" n="HIAT:w" s="T514">kihi</ts>
                  <nts id="Seg_1947" n="HIAT:ip">.</nts>
                  <nts id="Seg_1948" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T518" id="Seg_1950" n="HIAT:u" s="T515">
                  <ts e="T516" id="Seg_1952" n="HIAT:w" s="T515">ɨraːktaːgɨ</ts>
                  <nts id="Seg_1953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T517" id="Seg_1955" n="HIAT:w" s="T516">barɨlarɨttan</ts>
                  <nts id="Seg_1956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T518" id="Seg_1958" n="HIAT:w" s="T517">ɨjɨtar</ts>
                  <nts id="Seg_1959" n="HIAT:ip">:</nts>
                  <nts id="Seg_1960" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T520" id="Seg_1962" n="HIAT:u" s="T518">
                  <ts e="T519" id="Seg_1964" n="HIAT:w" s="T518">Kirdik</ts>
                  <nts id="Seg_1965" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T520" id="Seg_1967" n="HIAT:w" s="T519">du͡o</ts>
                  <nts id="Seg_1968" n="HIAT:ip">?</nts>
                  <nts id="Seg_1969" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T525" id="Seg_1971" n="HIAT:u" s="T520">
                  <ts e="T521" id="Seg_1973" n="HIAT:w" s="T520">Min</ts>
                  <nts id="Seg_1974" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T522" id="Seg_1976" n="HIAT:w" s="T521">kinini</ts>
                  <nts id="Seg_1977" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T523" id="Seg_1979" n="HIAT:w" s="T522">huːttu͡om</ts>
                  <nts id="Seg_1980" n="HIAT:ip">,</nts>
                  <nts id="Seg_1981" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T524" id="Seg_1983" n="HIAT:w" s="T523">ehigi</ts>
                  <nts id="Seg_1984" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T525" id="Seg_1986" n="HIAT:w" s="T524">istergitiger</ts>
                  <nts id="Seg_1987" n="HIAT:ip">.</nts>
                  <nts id="Seg_1988" n="HIAT:ip">"</nts>
                  <nts id="Seg_1989" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T533" id="Seg_1991" n="HIAT:u" s="T525">
                  <ts e="T526" id="Seg_1993" n="HIAT:w" s="T525">Haːmaj</ts>
                  <nts id="Seg_1994" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T527" id="Seg_1996" n="HIAT:w" s="T526">kineːhe</ts>
                  <nts id="Seg_1997" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1998" n="HIAT:ip">"</nts>
                  <ts e="T528" id="Seg_2000" n="HIAT:w" s="T527">kihi͡eke</ts>
                  <nts id="Seg_2001" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T529" id="Seg_2003" n="HIAT:w" s="T528">hugahaːbat</ts>
                  <nts id="Seg_2004" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T530" id="Seg_2006" n="HIAT:w" s="T529">kihi</ts>
                  <nts id="Seg_2007" n="HIAT:ip">,</nts>
                  <nts id="Seg_2008" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T531" id="Seg_2010" n="HIAT:w" s="T530">oŋorto</ts>
                  <nts id="Seg_2011" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T532" id="Seg_2013" n="HIAT:w" s="T531">bu͡olu͡o</ts>
                  <nts id="Seg_2014" n="HIAT:ip">"</nts>
                  <nts id="Seg_2015" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T533" id="Seg_2017" n="HIAT:w" s="T532">diːr</ts>
                  <nts id="Seg_2018" n="HIAT:ip">.</nts>
                  <nts id="Seg_2019" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T537" id="Seg_2021" n="HIAT:u" s="T533">
                  <ts e="T534" id="Seg_2023" n="HIAT:w" s="T533">Ogonnʼorgo</ts>
                  <nts id="Seg_2024" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T535" id="Seg_2026" n="HIAT:w" s="T534">albun</ts>
                  <nts id="Seg_2027" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T536" id="Seg_2029" n="HIAT:w" s="T535">huruk</ts>
                  <nts id="Seg_2030" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T537" id="Seg_2032" n="HIAT:w" s="T536">ɨːtallar</ts>
                  <nts id="Seg_2033" n="HIAT:ip">.</nts>
                  <nts id="Seg_2034" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T548" id="Seg_2036" n="HIAT:u" s="T537">
                  <ts e="T538" id="Seg_2038" n="HIAT:w" s="T537">Kineːhe</ts>
                  <nts id="Seg_2039" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T539" id="Seg_2041" n="HIAT:w" s="T538">če</ts>
                  <nts id="Seg_2042" n="HIAT:ip">,</nts>
                  <nts id="Seg_2043" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T540" id="Seg_2045" n="HIAT:w" s="T539">nolu͡ogun</ts>
                  <nts id="Seg_2046" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T541" id="Seg_2048" n="HIAT:w" s="T540">karčɨtɨn</ts>
                  <nts id="Seg_2049" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T542" id="Seg_2051" n="HIAT:w" s="T541">ɨla</ts>
                  <nts id="Seg_2052" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T543" id="Seg_2054" n="HIAT:w" s="T542">kelbekteːtin</ts>
                  <nts id="Seg_2055" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T544" id="Seg_2057" n="HIAT:w" s="T543">bert</ts>
                  <nts id="Seg_2058" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T545" id="Seg_2060" n="HIAT:w" s="T544">muŋ</ts>
                  <nts id="Seg_2061" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T546" id="Seg_2063" n="HIAT:w" s="T545">türgennik</ts>
                  <nts id="Seg_2064" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T547" id="Seg_2066" n="HIAT:w" s="T546">di͡en</ts>
                  <nts id="Seg_2067" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T548" id="Seg_2069" n="HIAT:w" s="T547">hurujar</ts>
                  <nts id="Seg_2070" n="HIAT:ip">.</nts>
                  <nts id="Seg_2071" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T552" id="Seg_2073" n="HIAT:u" s="T548">
                  <ts e="T549" id="Seg_2075" n="HIAT:w" s="T548">Ildʼit</ts>
                  <nts id="Seg_2076" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T550" id="Seg_2078" n="HIAT:w" s="T549">kihi</ts>
                  <nts id="Seg_2079" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T551" id="Seg_2081" n="HIAT:w" s="T550">ti͡erder</ts>
                  <nts id="Seg_2082" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T552" id="Seg_2084" n="HIAT:w" s="T551">hurugu</ts>
                  <nts id="Seg_2085" n="HIAT:ip">.</nts>
                  <nts id="Seg_2086" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T558" id="Seg_2088" n="HIAT:u" s="T552">
                  <ts e="T553" id="Seg_2090" n="HIAT:w" s="T552">Ogonnʼor</ts>
                  <nts id="Seg_2091" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T554" id="Seg_2093" n="HIAT:w" s="T553">muŋ</ts>
                  <nts id="Seg_2094" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T555" id="Seg_2096" n="HIAT:w" s="T554">ütü͡ö</ts>
                  <nts id="Seg_2097" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T556" id="Seg_2099" n="HIAT:w" s="T555">tabalarɨnan</ts>
                  <nts id="Seg_2100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T557" id="Seg_2102" n="HIAT:w" s="T556">kötüten</ts>
                  <nts id="Seg_2103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T558" id="Seg_2105" n="HIAT:w" s="T557">keler</ts>
                  <nts id="Seg_2106" n="HIAT:ip">.</nts>
                  <nts id="Seg_2107" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T563" id="Seg_2109" n="HIAT:u" s="T558">
                  <ts e="T559" id="Seg_2111" n="HIAT:w" s="T558">Tabatɨn</ts>
                  <nts id="Seg_2112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T560" id="Seg_2114" n="HIAT:w" s="T559">baːjbɨt</ts>
                  <nts id="Seg_2115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T561" id="Seg_2117" n="HIAT:w" s="T560">da</ts>
                  <nts id="Seg_2118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T562" id="Seg_2120" n="HIAT:w" s="T561">kötön</ts>
                  <nts id="Seg_2121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T563" id="Seg_2123" n="HIAT:w" s="T562">tüher</ts>
                  <nts id="Seg_2124" n="HIAT:ip">.</nts>
                  <nts id="Seg_2125" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T567" id="Seg_2127" n="HIAT:u" s="T563">
                  <ts e="T564" id="Seg_2129" n="HIAT:w" s="T563">Ostoːlu</ts>
                  <nts id="Seg_2130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T565" id="Seg_2132" n="HIAT:w" s="T564">tögürüččü</ts>
                  <nts id="Seg_2133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T566" id="Seg_2135" n="HIAT:w" s="T565">tojon</ts>
                  <nts id="Seg_2136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T567" id="Seg_2138" n="HIAT:w" s="T566">bögö</ts>
                  <nts id="Seg_2139" n="HIAT:ip">.</nts>
                  <nts id="Seg_2140" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T575" id="Seg_2142" n="HIAT:u" s="T567">
                  <ts e="T568" id="Seg_2144" n="HIAT:w" s="T567">ɨraːktaːgɨ</ts>
                  <nts id="Seg_2145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T569" id="Seg_2147" n="HIAT:w" s="T568">čaːj</ts>
                  <nts id="Seg_2148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T570" id="Seg_2150" n="HIAT:w" s="T569">da</ts>
                  <nts id="Seg_2151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T571" id="Seg_2153" n="HIAT:w" s="T570">iherpetek</ts>
                  <nts id="Seg_2154" n="HIAT:ip">,</nts>
                  <nts id="Seg_2155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T572" id="Seg_2157" n="HIAT:w" s="T571">kaːna</ts>
                  <nts id="Seg_2158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T573" id="Seg_2160" n="HIAT:w" s="T572">kuhagan</ts>
                  <nts id="Seg_2161" n="HIAT:ip">,</nts>
                  <nts id="Seg_2162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T574" id="Seg_2164" n="HIAT:w" s="T573">čipčik</ts>
                  <nts id="Seg_2165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T575" id="Seg_2167" n="HIAT:w" s="T574">atɨn</ts>
                  <nts id="Seg_2168" n="HIAT:ip">.</nts>
                  <nts id="Seg_2169" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T578" id="Seg_2171" n="HIAT:u" s="T575">
                  <nts id="Seg_2172" n="HIAT:ip">"</nts>
                  <ts e="T576" id="Seg_2174" n="HIAT:w" s="T575">Dʼe</ts>
                  <nts id="Seg_2175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T577" id="Seg_2177" n="HIAT:w" s="T576">kepseː</ts>
                  <nts id="Seg_2178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T578" id="Seg_2180" n="HIAT:w" s="T577">öskün</ts>
                  <nts id="Seg_2181" n="HIAT:ip">.</nts>
                  <nts id="Seg_2182" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T582" id="Seg_2184" n="HIAT:u" s="T578">
                  <ts e="T579" id="Seg_2186" n="HIAT:w" s="T578">Munnʼak</ts>
                  <nts id="Seg_2187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T580" id="Seg_2189" n="HIAT:w" s="T579">dʼonu</ts>
                  <nts id="Seg_2190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T581" id="Seg_2192" n="HIAT:w" s="T580">togo</ts>
                  <nts id="Seg_2193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T582" id="Seg_2195" n="HIAT:w" s="T581">kɨrtɨŋ</ts>
                  <nts id="Seg_2196" n="HIAT:ip">?</nts>
                  <nts id="Seg_2197" n="HIAT:ip">"</nts>
                  <nts id="Seg_2198" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T590" id="Seg_2200" n="HIAT:u" s="T582">
                  <nts id="Seg_2201" n="HIAT:ip">"</nts>
                  <ts e="T583" id="Seg_2203" n="HIAT:w" s="T582">Tu͡ok</ts>
                  <nts id="Seg_2204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T584" id="Seg_2206" n="HIAT:w" s="T583">munnʼagɨn</ts>
                  <nts id="Seg_2207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T585" id="Seg_2209" n="HIAT:w" s="T584">haptaragɨn</ts>
                  <nts id="Seg_2210" n="HIAT:ip">,</nts>
                  <nts id="Seg_2211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T586" id="Seg_2213" n="HIAT:w" s="T585">kihini</ts>
                  <nts id="Seg_2214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T587" id="Seg_2216" n="HIAT:w" s="T586">kɨtta</ts>
                  <nts id="Seg_2217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T588" id="Seg_2219" n="HIAT:w" s="T587">mökkü͡önüm</ts>
                  <nts id="Seg_2220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T589" id="Seg_2222" n="HIAT:w" s="T588">da</ts>
                  <nts id="Seg_2223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T590" id="Seg_2225" n="HIAT:w" s="T589">hu͡ok</ts>
                  <nts id="Seg_2226" n="HIAT:ip">.</nts>
                  <nts id="Seg_2227" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T594" id="Seg_2229" n="HIAT:u" s="T590">
                  <ts e="T591" id="Seg_2231" n="HIAT:w" s="T590">ɨraːktaːgɨ</ts>
                  <nts id="Seg_2232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T592" id="Seg_2234" n="HIAT:w" s="T591">kineːster</ts>
                  <nts id="Seg_2235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T593" id="Seg_2237" n="HIAT:w" s="T592">huruktarɨn</ts>
                  <nts id="Seg_2238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T594" id="Seg_2240" n="HIAT:w" s="T593">kördörör</ts>
                  <nts id="Seg_2241" n="HIAT:ip">:</nts>
                  <nts id="Seg_2242" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T604" id="Seg_2244" n="HIAT:u" s="T594">
                  <nts id="Seg_2245" n="HIAT:ip">"</nts>
                  <ts e="T595" id="Seg_2247" n="HIAT:w" s="T594">Ehiːlgi</ts>
                  <nts id="Seg_2248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T596" id="Seg_2250" n="HIAT:w" s="T595">uː</ts>
                  <nts id="Seg_2251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T597" id="Seg_2253" n="HIAT:w" s="T596">keli͡er</ts>
                  <nts id="Seg_2254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T598" id="Seg_2256" n="HIAT:w" s="T597">di͡eri</ts>
                  <nts id="Seg_2257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T599" id="Seg_2259" n="HIAT:w" s="T598">kaːjɨːga</ts>
                  <nts id="Seg_2260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T600" id="Seg_2262" n="HIAT:w" s="T599">hɨtɨ͡aŋ</ts>
                  <nts id="Seg_2263" n="HIAT:ip">,</nts>
                  <nts id="Seg_2264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T601" id="Seg_2266" n="HIAT:w" s="T600">miːginneːger</ts>
                  <nts id="Seg_2267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T602" id="Seg_2269" n="HIAT:w" s="T601">ulakan</ts>
                  <nts id="Seg_2270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T603" id="Seg_2272" n="HIAT:w" s="T602">ɨraːktaːgɨga</ts>
                  <nts id="Seg_2273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T604" id="Seg_2275" n="HIAT:w" s="T603">ɨːtɨ͡am</ts>
                  <nts id="Seg_2276" n="HIAT:ip">.</nts>
                  <nts id="Seg_2277" n="HIAT:ip">"</nts>
                  <nts id="Seg_2278" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T608" id="Seg_2280" n="HIAT:u" s="T604">
                  <ts e="T605" id="Seg_2282" n="HIAT:w" s="T604">Ogonnʼoru</ts>
                  <nts id="Seg_2283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T606" id="Seg_2285" n="HIAT:w" s="T605">türmege</ts>
                  <nts id="Seg_2286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T607" id="Seg_2288" n="HIAT:w" s="T606">kaːjan</ts>
                  <nts id="Seg_2289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T608" id="Seg_2291" n="HIAT:w" s="T607">keːheller</ts>
                  <nts id="Seg_2292" n="HIAT:ip">.</nts>
                  <nts id="Seg_2293" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T612" id="Seg_2295" n="HIAT:u" s="T608">
                  <ts e="T609" id="Seg_2297" n="HIAT:w" s="T608">Üs</ts>
                  <nts id="Seg_2298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T610" id="Seg_2300" n="HIAT:w" s="T609">kineːs</ts>
                  <nts id="Seg_2301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T611" id="Seg_2303" n="HIAT:w" s="T610">baːjɨn</ts>
                  <nts id="Seg_2304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T612" id="Seg_2306" n="HIAT:w" s="T611">üllesti͡ektere</ts>
                  <nts id="Seg_2307" n="HIAT:ip">.</nts>
                  <nts id="Seg_2308" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T621" id="Seg_2310" n="HIAT:u" s="T612">
                  <ts e="T613" id="Seg_2312" n="HIAT:w" s="T612">Emeːksin</ts>
                  <nts id="Seg_2313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T614" id="Seg_2315" n="HIAT:w" s="T613">köhön</ts>
                  <nts id="Seg_2316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T615" id="Seg_2318" n="HIAT:w" s="T614">keler</ts>
                  <nts id="Seg_2319" n="HIAT:ip">,</nts>
                  <nts id="Seg_2320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T616" id="Seg_2322" n="HIAT:w" s="T615">ogonnʼoro</ts>
                  <nts id="Seg_2323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T617" id="Seg_2325" n="HIAT:w" s="T616">kaččaga</ts>
                  <nts id="Seg_2326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T618" id="Seg_2328" n="HIAT:w" s="T617">da</ts>
                  <nts id="Seg_2329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T619" id="Seg_2331" n="HIAT:w" s="T618">hu͡ok</ts>
                  <nts id="Seg_2332" n="HIAT:ip">,</nts>
                  <nts id="Seg_2333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T620" id="Seg_2335" n="HIAT:w" s="T619">türmege</ts>
                  <nts id="Seg_2336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T621" id="Seg_2338" n="HIAT:w" s="T620">hɨtar</ts>
                  <nts id="Seg_2339" n="HIAT:ip">.</nts>
                  <nts id="Seg_2340" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T625" id="Seg_2342" n="HIAT:u" s="T621">
                  <ts e="T622" id="Seg_2344" n="HIAT:w" s="T621">ɨraːktaːgɨ</ts>
                  <nts id="Seg_2345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T623" id="Seg_2347" n="HIAT:w" s="T622">emeːksini</ts>
                  <nts id="Seg_2348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T624" id="Seg_2350" n="HIAT:w" s="T623">üːren</ts>
                  <nts id="Seg_2351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T625" id="Seg_2353" n="HIAT:w" s="T624">tahaːrar</ts>
                  <nts id="Seg_2354" n="HIAT:ip">.</nts>
                  <nts id="Seg_2355" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T635" id="Seg_2357" n="HIAT:u" s="T625">
                  <ts e="T626" id="Seg_2359" n="HIAT:w" s="T625">Üs</ts>
                  <nts id="Seg_2360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T627" id="Seg_2362" n="HIAT:w" s="T626">kineːs</ts>
                  <nts id="Seg_2363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T628" id="Seg_2365" n="HIAT:w" s="T627">baːjɨn</ts>
                  <nts id="Seg_2366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T629" id="Seg_2368" n="HIAT:w" s="T628">haba</ts>
                  <nts id="Seg_2369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T630" id="Seg_2371" n="HIAT:w" s="T629">tühen</ts>
                  <nts id="Seg_2372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T631" id="Seg_2374" n="HIAT:w" s="T630">üllesten</ts>
                  <nts id="Seg_2375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T632" id="Seg_2377" n="HIAT:w" s="T631">ɨlallar</ts>
                  <nts id="Seg_2378" n="HIAT:ip">,</nts>
                  <nts id="Seg_2379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T633" id="Seg_2381" n="HIAT:w" s="T632">biːr</ts>
                  <nts id="Seg_2382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T634" id="Seg_2384" n="HIAT:w" s="T633">öldʼüːnü</ts>
                  <nts id="Seg_2385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T635" id="Seg_2387" n="HIAT:w" s="T634">kaːllarallar</ts>
                  <nts id="Seg_2388" n="HIAT:ip">.</nts>
                  <nts id="Seg_2389" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T640" id="Seg_2391" n="HIAT:u" s="T635">
                  <ts e="T636" id="Seg_2393" n="HIAT:w" s="T635">Kɨhɨna</ts>
                  <nts id="Seg_2394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T637" id="Seg_2396" n="HIAT:w" s="T636">baranan</ts>
                  <nts id="Seg_2397" n="HIAT:ip">,</nts>
                  <nts id="Seg_2398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T638" id="Seg_2400" n="HIAT:w" s="T637">poroku͡ot</ts>
                  <nts id="Seg_2401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T639" id="Seg_2403" n="HIAT:w" s="T638">keleːkteːbit</ts>
                  <nts id="Seg_2404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T640" id="Seg_2406" n="HIAT:w" s="T639">üːhetten</ts>
                  <nts id="Seg_2407" n="HIAT:ip">.</nts>
                  <nts id="Seg_2408" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T644" id="Seg_2410" n="HIAT:u" s="T640">
                  <nts id="Seg_2411" n="HIAT:ip">"</nts>
                  <ts e="T641" id="Seg_2413" n="HIAT:w" s="T640">Dʼe</ts>
                  <nts id="Seg_2414" n="HIAT:ip">,</nts>
                  <nts id="Seg_2415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T642" id="Seg_2417" n="HIAT:w" s="T641">kör</ts>
                  <nts id="Seg_2418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T643" id="Seg_2420" n="HIAT:w" s="T642">ergin</ts>
                  <nts id="Seg_2421" n="HIAT:ip">"</nts>
                  <nts id="Seg_2422" n="HIAT:ip">,</nts>
                  <nts id="Seg_2423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T644" id="Seg_2425" n="HIAT:w" s="T643">diːller</ts>
                  <nts id="Seg_2426" n="HIAT:ip">.</nts>
                  <nts id="Seg_2427" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T652" id="Seg_2429" n="HIAT:u" s="T644">
                  <ts e="T645" id="Seg_2431" n="HIAT:w" s="T644">Ogonnʼor</ts>
                  <nts id="Seg_2432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T646" id="Seg_2434" n="HIAT:w" s="T645">karaga</ts>
                  <nts id="Seg_2435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T647" id="Seg_2437" n="HIAT:w" s="T646">iːn</ts>
                  <nts id="Seg_2438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T648" id="Seg_2440" n="HIAT:w" s="T647">ihiger</ts>
                  <nts id="Seg_2441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T649" id="Seg_2443" n="HIAT:w" s="T648">kiːrbit</ts>
                  <nts id="Seg_2444" n="HIAT:ip">,</nts>
                  <nts id="Seg_2445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T650" id="Seg_2447" n="HIAT:w" s="T649">ahappataktar</ts>
                  <nts id="Seg_2448" n="HIAT:ip">,</nts>
                  <nts id="Seg_2449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T651" id="Seg_2451" n="HIAT:w" s="T650">okton</ts>
                  <nts id="Seg_2452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T652" id="Seg_2454" n="HIAT:w" s="T651">kaːlbɨt</ts>
                  <nts id="Seg_2455" n="HIAT:ip">.</nts>
                  <nts id="Seg_2456" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T655" id="Seg_2458" n="HIAT:u" s="T652">
                  <ts e="T653" id="Seg_2460" n="HIAT:w" s="T652">Poroku͡ot</ts>
                  <nts id="Seg_2461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T654" id="Seg_2463" n="HIAT:w" s="T653">ihiger</ts>
                  <nts id="Seg_2464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T655" id="Seg_2466" n="HIAT:w" s="T654">bɨrakpɨttar</ts>
                  <nts id="Seg_2467" n="HIAT:ip">.</nts>
                  <nts id="Seg_2468" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T663" id="Seg_2470" n="HIAT:u" s="T655">
                  <ts e="T656" id="Seg_2472" n="HIAT:w" s="T655">Nöŋü͡ö</ts>
                  <nts id="Seg_2473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T657" id="Seg_2475" n="HIAT:w" s="T656">ɨraːktaːgɨga</ts>
                  <nts id="Seg_2476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T658" id="Seg_2478" n="HIAT:w" s="T657">illʼeller</ts>
                  <nts id="Seg_2479" n="HIAT:ip">,</nts>
                  <nts id="Seg_2480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T659" id="Seg_2482" n="HIAT:w" s="T658">nosilkannan</ts>
                  <nts id="Seg_2483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T660" id="Seg_2485" n="HIAT:w" s="T659">killereller</ts>
                  <nts id="Seg_2486" n="HIAT:ip">,</nts>
                  <nts id="Seg_2487" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T661" id="Seg_2489" n="HIAT:w" s="T660">ölön</ts>
                  <nts id="Seg_2490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T662" id="Seg_2492" n="HIAT:w" s="T661">erer</ts>
                  <nts id="Seg_2493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2494" n="HIAT:ip">[</nts>
                  <ts e="T663" id="Seg_2496" n="HIAT:w" s="T662">kihini</ts>
                  <nts id="Seg_2497" n="HIAT:ip">]</nts>
                  <nts id="Seg_2498" n="HIAT:ip">.</nts>
                  <nts id="Seg_2499" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T668" id="Seg_2501" n="HIAT:u" s="T663">
                  <nts id="Seg_2502" n="HIAT:ip">"</nts>
                  <ts e="T664" id="Seg_2504" n="HIAT:w" s="T663">Kaja</ts>
                  <nts id="Seg_2505" n="HIAT:ip">,</nts>
                  <nts id="Seg_2506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T665" id="Seg_2508" n="HIAT:w" s="T664">kajtak</ts>
                  <nts id="Seg_2509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T666" id="Seg_2511" n="HIAT:w" s="T665">huːttu͡omuj</ts>
                  <nts id="Seg_2512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T667" id="Seg_2514" n="HIAT:w" s="T666">ölbüt</ts>
                  <nts id="Seg_2515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T668" id="Seg_2517" n="HIAT:w" s="T667">kihini</ts>
                  <nts id="Seg_2518" n="HIAT:ip">?</nts>
                  <nts id="Seg_2519" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T672" id="Seg_2521" n="HIAT:u" s="T668">
                  <ts e="T669" id="Seg_2523" n="HIAT:w" s="T668">Ildʼiŋ</ts>
                  <nts id="Seg_2524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T670" id="Seg_2526" n="HIAT:w" s="T669">bolʼnicaga</ts>
                  <nts id="Seg_2527" n="HIAT:ip">,</nts>
                  <nts id="Seg_2528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T671" id="Seg_2530" n="HIAT:w" s="T670">ahattɨnnar</ts>
                  <nts id="Seg_2531" n="HIAT:ip">,</nts>
                  <nts id="Seg_2532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T672" id="Seg_2534" n="HIAT:w" s="T671">kördünner</ts>
                  <nts id="Seg_2535" n="HIAT:ip">.</nts>
                  <nts id="Seg_2536" n="HIAT:ip">"</nts>
                  <nts id="Seg_2537" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T677" id="Seg_2539" n="HIAT:u" s="T672">
                  <ts e="T673" id="Seg_2541" n="HIAT:w" s="T672">Du͡oktuːrdar</ts>
                  <nts id="Seg_2542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T674" id="Seg_2544" n="HIAT:w" s="T673">emtiːller</ts>
                  <nts id="Seg_2545" n="HIAT:ip">,</nts>
                  <nts id="Seg_2546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T675" id="Seg_2548" n="HIAT:w" s="T674">erge</ts>
                  <nts id="Seg_2549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T676" id="Seg_2551" n="HIAT:w" s="T675">bejete</ts>
                  <nts id="Seg_2552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T677" id="Seg_2554" n="HIAT:w" s="T676">bu͡olar</ts>
                  <nts id="Seg_2555" n="HIAT:ip">.</nts>
                  <nts id="Seg_2556" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T679" id="Seg_2558" n="HIAT:u" s="T677">
                  <ts e="T678" id="Seg_2560" n="HIAT:w" s="T677">Du͡oktuːrdar</ts>
                  <nts id="Seg_2561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T679" id="Seg_2563" n="HIAT:w" s="T678">ɨjɨtallar</ts>
                  <nts id="Seg_2564" n="HIAT:ip">:</nts>
                  <nts id="Seg_2565" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T683" id="Seg_2567" n="HIAT:u" s="T679">
                  <nts id="Seg_2568" n="HIAT:ip">"</nts>
                  <ts e="T680" id="Seg_2570" n="HIAT:w" s="T679">Burujuŋ</ts>
                  <nts id="Seg_2571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T681" id="Seg_2573" n="HIAT:w" s="T680">duː</ts>
                  <nts id="Seg_2574" n="HIAT:ip">,</nts>
                  <nts id="Seg_2575" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T682" id="Seg_2577" n="HIAT:w" s="T681">kütürüːller</ts>
                  <nts id="Seg_2578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T683" id="Seg_2580" n="HIAT:w" s="T682">duː</ts>
                  <nts id="Seg_2581" n="HIAT:ip">?</nts>
                  <nts id="Seg_2582" n="HIAT:ip">"</nts>
                  <nts id="Seg_2583" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T686" id="Seg_2585" n="HIAT:u" s="T683">
                  <ts e="T684" id="Seg_2587" n="HIAT:w" s="T683">Ogonnʼor</ts>
                  <nts id="Seg_2588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T685" id="Seg_2590" n="HIAT:w" s="T684">kepsiːr</ts>
                  <nts id="Seg_2591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T686" id="Seg_2593" n="HIAT:w" s="T685">barɨtɨn</ts>
                  <nts id="Seg_2594" n="HIAT:ip">.</nts>
                  <nts id="Seg_2595" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T689" id="Seg_2597" n="HIAT:u" s="T686">
                  <ts e="T687" id="Seg_2599" n="HIAT:w" s="T686">Du͡oktuːrdar</ts>
                  <nts id="Seg_2600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T688" id="Seg_2602" n="HIAT:w" s="T687">kaːn</ts>
                  <nts id="Seg_2603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T689" id="Seg_2605" n="HIAT:w" s="T688">ɨlallar</ts>
                  <nts id="Seg_2606" n="HIAT:ip">:</nts>
                  <nts id="Seg_2607" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T695" id="Seg_2609" n="HIAT:u" s="T689">
                  <nts id="Seg_2610" n="HIAT:ip">"</nts>
                  <ts e="T690" id="Seg_2612" n="HIAT:w" s="T689">Burujdaːk</ts>
                  <nts id="Seg_2613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T691" id="Seg_2615" n="HIAT:w" s="T690">bu͡ollakkɨna</ts>
                  <nts id="Seg_2616" n="HIAT:ip">,</nts>
                  <nts id="Seg_2617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T692" id="Seg_2619" n="HIAT:w" s="T691">kaːnɨŋ</ts>
                  <nts id="Seg_2620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T693" id="Seg_2622" n="HIAT:w" s="T692">atɨn</ts>
                  <nts id="Seg_2623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T694" id="Seg_2625" n="HIAT:w" s="T693">bu͡olu͡o</ts>
                  <nts id="Seg_2626" n="HIAT:ip">"</nts>
                  <nts id="Seg_2627" n="HIAT:ip">,</nts>
                  <nts id="Seg_2628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T695" id="Seg_2630" n="HIAT:w" s="T694">diːller</ts>
                  <nts id="Seg_2631" n="HIAT:ip">.</nts>
                  <nts id="Seg_2632" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T701" id="Seg_2634" n="HIAT:u" s="T695">
                  <ts e="T696" id="Seg_2636" n="HIAT:w" s="T695">Biːrkeːn</ts>
                  <nts id="Seg_2637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T697" id="Seg_2639" n="HIAT:w" s="T696">da</ts>
                  <nts id="Seg_2640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T698" id="Seg_2642" n="HIAT:w" s="T697">kaːpelʼka</ts>
                  <nts id="Seg_2643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T699" id="Seg_2645" n="HIAT:w" s="T698">buruja</ts>
                  <nts id="Seg_2646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T700" id="Seg_2648" n="HIAT:w" s="T699">hu͡ok</ts>
                  <nts id="Seg_2649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T701" id="Seg_2651" n="HIAT:w" s="T700">ebit</ts>
                  <nts id="Seg_2652" n="HIAT:ip">.</nts>
                  <nts id="Seg_2653" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T703" id="Seg_2655" n="HIAT:u" s="T701">
                  <ts e="T702" id="Seg_2657" n="HIAT:w" s="T701">Huruk</ts>
                  <nts id="Seg_2658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T703" id="Seg_2660" n="HIAT:w" s="T702">bi͡ereller</ts>
                  <nts id="Seg_2661" n="HIAT:ip">:</nts>
                  <nts id="Seg_2662" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T707" id="Seg_2664" n="HIAT:u" s="T703">
                  <nts id="Seg_2665" n="HIAT:ip">"</nts>
                  <ts e="T704" id="Seg_2667" n="HIAT:w" s="T703">Manɨ</ts>
                  <nts id="Seg_2668" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T705" id="Seg_2670" n="HIAT:w" s="T704">huːkka</ts>
                  <nts id="Seg_2671" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T706" id="Seg_2673" n="HIAT:w" s="T705">pireːme</ts>
                  <nts id="Seg_2674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T707" id="Seg_2676" n="HIAT:w" s="T706">bi͡ereːr</ts>
                  <nts id="Seg_2677" n="HIAT:ip">.</nts>
                  <nts id="Seg_2678" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T712" id="Seg_2680" n="HIAT:u" s="T707">
                  <ts e="T708" id="Seg_2682" n="HIAT:w" s="T707">Kineːster</ts>
                  <nts id="Seg_2683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T709" id="Seg_2685" n="HIAT:w" s="T708">kaːnnarɨn</ts>
                  <nts id="Seg_2686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T710" id="Seg_2688" n="HIAT:w" s="T709">emi͡e</ts>
                  <nts id="Seg_2689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T711" id="Seg_2691" n="HIAT:w" s="T710">ɨlɨ͡akpɨt</ts>
                  <nts id="Seg_2692" n="HIAT:ip">"</nts>
                  <nts id="Seg_2693" n="HIAT:ip">,</nts>
                  <nts id="Seg_2694" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T712" id="Seg_2696" n="HIAT:w" s="T711">diːller</ts>
                  <nts id="Seg_2697" n="HIAT:ip">.</nts>
                  <nts id="Seg_2698" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T714" id="Seg_2700" n="HIAT:u" s="T712">
                  <ts e="T713" id="Seg_2702" n="HIAT:w" s="T712">Huːt</ts>
                  <nts id="Seg_2703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T714" id="Seg_2705" n="HIAT:w" s="T713">bu͡olar</ts>
                  <nts id="Seg_2706" n="HIAT:ip">.</nts>
                  <nts id="Seg_2707" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T720" id="Seg_2709" n="HIAT:u" s="T714">
                  <ts e="T715" id="Seg_2711" n="HIAT:w" s="T714">Kineːster</ts>
                  <nts id="Seg_2712" n="HIAT:ip">,</nts>
                  <nts id="Seg_2713" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T716" id="Seg_2715" n="HIAT:w" s="T715">maŋnajgɨ</ts>
                  <nts id="Seg_2716" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T717" id="Seg_2718" n="HIAT:w" s="T716">ɨraːktaːgɨ</ts>
                  <nts id="Seg_2719" n="HIAT:ip">,</nts>
                  <nts id="Seg_2720" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T718" id="Seg_2722" n="HIAT:w" s="T717">elbek</ts>
                  <nts id="Seg_2723" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T719" id="Seg_2725" n="HIAT:w" s="T718">dʼon</ts>
                  <nts id="Seg_2726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T720" id="Seg_2728" n="HIAT:w" s="T719">munnʼustubut</ts>
                  <nts id="Seg_2729" n="HIAT:ip">.</nts>
                  <nts id="Seg_2730" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T722" id="Seg_2732" n="HIAT:u" s="T720">
                  <ts e="T721" id="Seg_2734" n="HIAT:w" s="T720">ɨraːktaːgɨ</ts>
                  <nts id="Seg_2735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T722" id="Seg_2737" n="HIAT:w" s="T721">eter</ts>
                  <nts id="Seg_2738" n="HIAT:ip">:</nts>
                  <nts id="Seg_2739" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T725" id="Seg_2741" n="HIAT:u" s="T722">
                  <nts id="Seg_2742" n="HIAT:ip">"</nts>
                  <ts e="T723" id="Seg_2744" n="HIAT:w" s="T722">Öhün-tɨlɨn</ts>
                  <nts id="Seg_2745" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T724" id="Seg_2747" n="HIAT:w" s="T723">isti͡ekke</ts>
                  <nts id="Seg_2748" n="HIAT:ip">"</nts>
                  <nts id="Seg_2749" n="HIAT:ip">,</nts>
                  <nts id="Seg_2750" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T725" id="Seg_2752" n="HIAT:w" s="T724">diːr</ts>
                  <nts id="Seg_2753" n="HIAT:ip">.</nts>
                  <nts id="Seg_2754" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T727" id="Seg_2756" n="HIAT:u" s="T725">
                  <ts e="T726" id="Seg_2758" n="HIAT:w" s="T725">Ogonnʼor</ts>
                  <nts id="Seg_2759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T727" id="Seg_2761" n="HIAT:w" s="T726">kepsiːr</ts>
                  <nts id="Seg_2762" n="HIAT:ip">:</nts>
                  <nts id="Seg_2763" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T733" id="Seg_2765" n="HIAT:u" s="T727">
                  <nts id="Seg_2766" n="HIAT:ip">"</nts>
                  <ts e="T728" id="Seg_2768" n="HIAT:w" s="T727">Min</ts>
                  <nts id="Seg_2769" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T729" id="Seg_2771" n="HIAT:w" s="T728">daːse</ts>
                  <nts id="Seg_2772" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T730" id="Seg_2774" n="HIAT:w" s="T729">kihini</ts>
                  <nts id="Seg_2775" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T731" id="Seg_2777" n="HIAT:w" s="T730">kɨtta</ts>
                  <nts id="Seg_2778" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T732" id="Seg_2780" n="HIAT:w" s="T731">ölössübetek</ts>
                  <nts id="Seg_2781" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T733" id="Seg_2783" n="HIAT:w" s="T732">kihibin</ts>
                  <nts id="Seg_2784" n="HIAT:ip">.</nts>
                  <nts id="Seg_2785" n="HIAT:ip">"</nts>
                  <nts id="Seg_2786" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T741" id="Seg_2788" n="HIAT:u" s="T733">
                  <ts e="T734" id="Seg_2790" n="HIAT:w" s="T733">ɨsparaːpkatɨn</ts>
                  <nts id="Seg_2791" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T735" id="Seg_2793" n="HIAT:w" s="T734">bi͡erer</ts>
                  <nts id="Seg_2794" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2795" n="HIAT:ip">"</nts>
                  <ts e="T736" id="Seg_2797" n="HIAT:w" s="T735">bu</ts>
                  <nts id="Seg_2798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T737" id="Seg_2800" n="HIAT:w" s="T736">kihi</ts>
                  <nts id="Seg_2801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T738" id="Seg_2803" n="HIAT:w" s="T737">kaːpelʼka</ts>
                  <nts id="Seg_2804" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T739" id="Seg_2806" n="HIAT:w" s="T738">da</ts>
                  <nts id="Seg_2807" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T740" id="Seg_2809" n="HIAT:w" s="T739">buruja</ts>
                  <nts id="Seg_2810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T741" id="Seg_2812" n="HIAT:w" s="T740">hu͡ok</ts>
                  <nts id="Seg_2813" n="HIAT:ip">"</nts>
                  <nts id="Seg_2814" n="HIAT:ip">.</nts>
                  <nts id="Seg_2815" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T744" id="Seg_2817" n="HIAT:u" s="T741">
                  <ts e="T742" id="Seg_2819" n="HIAT:w" s="T741">Kineːster</ts>
                  <nts id="Seg_2820" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T743" id="Seg_2822" n="HIAT:w" s="T742">kaːnnarɨn</ts>
                  <nts id="Seg_2823" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T744" id="Seg_2825" n="HIAT:w" s="T743">ɨlallar</ts>
                  <nts id="Seg_2826" n="HIAT:ip">.</nts>
                  <nts id="Seg_2827" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T753" id="Seg_2829" n="HIAT:u" s="T744">
                  <ts e="T745" id="Seg_2831" n="HIAT:w" s="T744">Haːmaj</ts>
                  <nts id="Seg_2832" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T746" id="Seg_2834" n="HIAT:w" s="T745">kohuːnun</ts>
                  <nts id="Seg_2835" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T747" id="Seg_2837" n="HIAT:w" s="T746">kaːna</ts>
                  <nts id="Seg_2838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T748" id="Seg_2840" n="HIAT:w" s="T747">ile</ts>
                  <nts id="Seg_2841" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T749" id="Seg_2843" n="HIAT:w" s="T748">atɨn</ts>
                  <nts id="Seg_2844" n="HIAT:ip">,</nts>
                  <nts id="Seg_2845" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T750" id="Seg_2847" n="HIAT:w" s="T749">kineːster</ts>
                  <nts id="Seg_2848" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T751" id="Seg_2850" n="HIAT:w" s="T750">gi͡ettere</ts>
                  <nts id="Seg_2851" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T752" id="Seg_2853" n="HIAT:w" s="T751">čiŋke</ts>
                  <nts id="Seg_2854" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T753" id="Seg_2856" n="HIAT:w" s="T752">atɨn</ts>
                  <nts id="Seg_2857" n="HIAT:ip">.</nts>
                  <nts id="Seg_2858" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T756" id="Seg_2860" n="HIAT:u" s="T753">
                  <ts e="T754" id="Seg_2862" n="HIAT:w" s="T753">ɨraːktaːgɨ</ts>
                  <nts id="Seg_2863" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T755" id="Seg_2865" n="HIAT:w" s="T754">dʼe</ts>
                  <nts id="Seg_2866" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T756" id="Seg_2868" n="HIAT:w" s="T755">ɨgajar</ts>
                  <nts id="Seg_2869" n="HIAT:ip">:</nts>
                  <nts id="Seg_2870" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T758" id="Seg_2872" n="HIAT:u" s="T756">
                  <nts id="Seg_2873" n="HIAT:ip">"</nts>
                  <ts e="T757" id="Seg_2875" n="HIAT:w" s="T756">Kim</ts>
                  <nts id="Seg_2876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T758" id="Seg_2878" n="HIAT:w" s="T757">hübeleːbitej</ts>
                  <nts id="Seg_2879" n="HIAT:ip">?</nts>
                  <nts id="Seg_2880" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T759" id="Seg_2882" n="HIAT:u" s="T758">
                  <ts e="T759" id="Seg_2884" n="HIAT:w" s="T758">Kepseːŋ</ts>
                  <nts id="Seg_2885" n="HIAT:ip">!</nts>
                  <nts id="Seg_2886" n="HIAT:ip">"</nts>
                  <nts id="Seg_2887" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T762" id="Seg_2889" n="HIAT:u" s="T759">
                  <ts e="T760" id="Seg_2891" n="HIAT:w" s="T759">Tɨ͡a</ts>
                  <nts id="Seg_2892" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T761" id="Seg_2894" n="HIAT:w" s="T760">kineːhe</ts>
                  <nts id="Seg_2895" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T762" id="Seg_2897" n="HIAT:w" s="T761">kepsiːr</ts>
                  <nts id="Seg_2898" n="HIAT:ip">:</nts>
                  <nts id="Seg_2899" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T766" id="Seg_2901" n="HIAT:u" s="T762">
                  <nts id="Seg_2902" n="HIAT:ip">"</nts>
                  <ts e="T763" id="Seg_2904" n="HIAT:w" s="T762">Dʼe</ts>
                  <nts id="Seg_2905" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T764" id="Seg_2907" n="HIAT:w" s="T763">kirdige</ts>
                  <nts id="Seg_2908" n="HIAT:ip">,</nts>
                  <nts id="Seg_2909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T765" id="Seg_2911" n="HIAT:w" s="T764">bihigi</ts>
                  <nts id="Seg_2912" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T766" id="Seg_2914" n="HIAT:w" s="T765">burujdaːkpɨt</ts>
                  <nts id="Seg_2915" n="HIAT:ip">.</nts>
                  <nts id="Seg_2916" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T771" id="Seg_2918" n="HIAT:u" s="T766">
                  <ts e="T767" id="Seg_2920" n="HIAT:w" s="T766">Haːmaj</ts>
                  <nts id="Seg_2921" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T768" id="Seg_2923" n="HIAT:w" s="T767">ulakan</ts>
                  <nts id="Seg_2924" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T769" id="Seg_2926" n="HIAT:w" s="T768">burujdaːk</ts>
                  <nts id="Seg_2927" n="HIAT:ip">—</nts>
                  <nts id="Seg_2928" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T770" id="Seg_2930" n="HIAT:w" s="T769">dʼuraːk</ts>
                  <nts id="Seg_2931" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T771" id="Seg_2933" n="HIAT:w" s="T770">kineːhe</ts>
                  <nts id="Seg_2934" n="HIAT:ip">.</nts>
                  <nts id="Seg_2935" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T777" id="Seg_2937" n="HIAT:u" s="T771">
                  <ts e="T772" id="Seg_2939" n="HIAT:w" s="T771">Minʼigin</ts>
                  <nts id="Seg_2940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T773" id="Seg_2942" n="HIAT:w" s="T772">keji͡ek</ts>
                  <nts id="Seg_2943" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T774" id="Seg_2945" n="HIAT:w" s="T773">baːllara</ts>
                  <nts id="Seg_2946" n="HIAT:ip">,</nts>
                  <nts id="Seg_2947" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T775" id="Seg_2949" n="HIAT:w" s="T774">kuttanan</ts>
                  <nts id="Seg_2950" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T776" id="Seg_2952" n="HIAT:w" s="T775">podpisʼ</ts>
                  <nts id="Seg_2953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T777" id="Seg_2955" n="HIAT:w" s="T776">bi͡erbitim</ts>
                  <nts id="Seg_2956" n="HIAT:ip">.</nts>
                  <nts id="Seg_2957" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T778" id="Seg_2959" n="HIAT:u" s="T777">
                  <ts e="T778" id="Seg_2961" n="HIAT:w" s="T777">ɨraːktaːgɨ</ts>
                  <nts id="Seg_2962" n="HIAT:ip">:</nts>
                  <nts id="Seg_2963" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T784" id="Seg_2965" n="HIAT:u" s="T778">
                  <nts id="Seg_2966" n="HIAT:ip">"</nts>
                  <ts e="T779" id="Seg_2968" n="HIAT:w" s="T778">Dʼe</ts>
                  <nts id="Seg_2969" n="HIAT:ip">,</nts>
                  <nts id="Seg_2970" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T780" id="Seg_2972" n="HIAT:w" s="T779">ogonnʼor</ts>
                  <nts id="Seg_2973" n="HIAT:ip">,</nts>
                  <nts id="Seg_2974" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T781" id="Seg_2976" n="HIAT:w" s="T780">en</ts>
                  <nts id="Seg_2977" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T782" id="Seg_2979" n="HIAT:w" s="T781">burujun</ts>
                  <nts id="Seg_2980" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T783" id="Seg_2982" n="HIAT:w" s="T782">hu͡ok</ts>
                  <nts id="Seg_2983" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T784" id="Seg_2985" n="HIAT:w" s="T783">ebit</ts>
                  <nts id="Seg_2986" n="HIAT:ip">.</nts>
                  <nts id="Seg_2987" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T789" id="Seg_2989" n="HIAT:u" s="T784">
                  <ts e="T785" id="Seg_2991" n="HIAT:w" s="T784">Kaːjɨːga</ts>
                  <nts id="Seg_2992" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T786" id="Seg_2994" n="HIAT:w" s="T785">ejigin</ts>
                  <nts id="Seg_2995" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T787" id="Seg_2997" n="HIAT:w" s="T786">huːttaːbɨt</ts>
                  <nts id="Seg_2998" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T788" id="Seg_3000" n="HIAT:w" s="T787">ɨraːktaːgɨnɨ</ts>
                  <nts id="Seg_3001" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T789" id="Seg_3003" n="HIAT:w" s="T788">olordu͡om</ts>
                  <nts id="Seg_3004" n="HIAT:ip">.</nts>
                  <nts id="Seg_3005" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T799" id="Seg_3007" n="HIAT:u" s="T789">
                  <ts e="T790" id="Seg_3009" n="HIAT:w" s="T789">En</ts>
                  <nts id="Seg_3010" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T791" id="Seg_3012" n="HIAT:w" s="T790">gini</ts>
                  <nts id="Seg_3013" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T792" id="Seg_3015" n="HIAT:w" s="T791">onnugar</ts>
                  <nts id="Seg_3016" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T793" id="Seg_3018" n="HIAT:w" s="T792">ɨraːktaːgɨnnan</ts>
                  <nts id="Seg_3019" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T794" id="Seg_3021" n="HIAT:w" s="T793">barɨ͡aŋ</ts>
                  <nts id="Seg_3022" n="HIAT:ip">,</nts>
                  <nts id="Seg_3023" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T795" id="Seg_3025" n="HIAT:w" s="T794">kineːsteri</ts>
                  <nts id="Seg_3026" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T796" id="Seg_3028" n="HIAT:w" s="T795">onno</ts>
                  <nts id="Seg_3029" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T797" id="Seg_3031" n="HIAT:w" s="T796">tijen</ts>
                  <nts id="Seg_3032" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T798" id="Seg_3034" n="HIAT:w" s="T797">huːttaːr</ts>
                  <nts id="Seg_3035" n="HIAT:ip">"</nts>
                  <nts id="Seg_3036" n="HIAT:ip">,</nts>
                  <nts id="Seg_3037" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T799" id="Seg_3039" n="HIAT:w" s="T798">di͡ebit</ts>
                  <nts id="Seg_3040" n="HIAT:ip">.</nts>
                  <nts id="Seg_3041" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T802" id="Seg_3043" n="HIAT:u" s="T799">
                  <ts e="T800" id="Seg_3045" n="HIAT:w" s="T799">Kamnaːn</ts>
                  <nts id="Seg_3046" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T801" id="Seg_3048" n="HIAT:w" s="T800">kaːlallar</ts>
                  <nts id="Seg_3049" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T802" id="Seg_3051" n="HIAT:w" s="T801">töttörü</ts>
                  <nts id="Seg_3052" n="HIAT:ip">.</nts>
                  <nts id="Seg_3053" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T807" id="Seg_3055" n="HIAT:u" s="T802">
                  <ts e="T803" id="Seg_3057" n="HIAT:w" s="T802">Ogonnʼor</ts>
                  <nts id="Seg_3058" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T804" id="Seg_3060" n="HIAT:w" s="T803">ologugar</ts>
                  <nts id="Seg_3061" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T805" id="Seg_3063" n="HIAT:w" s="T804">kelen</ts>
                  <nts id="Seg_3064" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T806" id="Seg_3066" n="HIAT:w" s="T805">kineːs</ts>
                  <nts id="Seg_3067" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T807" id="Seg_3069" n="HIAT:w" s="T806">bu͡olar</ts>
                  <nts id="Seg_3070" n="HIAT:ip">.</nts>
                  <nts id="Seg_3071" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T812" id="Seg_3073" n="HIAT:u" s="T807">
                  <ts e="T808" id="Seg_3075" n="HIAT:w" s="T807">U͡ola</ts>
                  <nts id="Seg_3076" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T809" id="Seg_3078" n="HIAT:w" s="T808">ulaːtan</ts>
                  <nts id="Seg_3079" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T810" id="Seg_3081" n="HIAT:w" s="T809">pireːme-pireːme</ts>
                  <nts id="Seg_3082" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T811" id="Seg_3084" n="HIAT:w" s="T810">bukatɨːr</ts>
                  <nts id="Seg_3085" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T812" id="Seg_3087" n="HIAT:w" s="T811">bu͡olbut</ts>
                  <nts id="Seg_3088" n="HIAT:ip">.</nts>
                  <nts id="Seg_3089" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T820" id="Seg_3091" n="HIAT:u" s="T812">
                  <ts e="T813" id="Seg_3093" n="HIAT:w" s="T812">Kini</ts>
                  <nts id="Seg_3094" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T814" id="Seg_3096" n="HIAT:w" s="T813">hu͡oguna</ts>
                  <nts id="Seg_3097" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T815" id="Seg_3099" n="HIAT:w" s="T814">ölöröːrü</ts>
                  <nts id="Seg_3100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T816" id="Seg_3102" n="HIAT:w" s="T815">gɨmmɨttar</ts>
                  <nts id="Seg_3103" n="HIAT:ip">,</nts>
                  <nts id="Seg_3104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T817" id="Seg_3106" n="HIAT:w" s="T816">agatɨgar</ts>
                  <nts id="Seg_3107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T818" id="Seg_3109" n="HIAT:w" s="T817">talɨ</ts>
                  <nts id="Seg_3110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T819" id="Seg_3112" n="HIAT:w" s="T818">bu͡olu͡o</ts>
                  <nts id="Seg_3113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T820" id="Seg_3115" n="HIAT:w" s="T819">di͡en</ts>
                  <nts id="Seg_3116" n="HIAT:ip">.</nts>
                  <nts id="Seg_3117" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T828" id="Seg_3119" n="HIAT:u" s="T820">
                  <ts e="T821" id="Seg_3121" n="HIAT:w" s="T820">U͡ol</ts>
                  <nts id="Seg_3122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T822" id="Seg_3124" n="HIAT:w" s="T821">biːr</ts>
                  <nts id="Seg_3125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T823" id="Seg_3127" n="HIAT:w" s="T822">kihini</ts>
                  <nts id="Seg_3128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T824" id="Seg_3130" n="HIAT:w" s="T823">tüŋneri</ts>
                  <nts id="Seg_3131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T825" id="Seg_3133" n="HIAT:w" s="T824">oksubut</ts>
                  <nts id="Seg_3134" n="HIAT:ip">,</nts>
                  <nts id="Seg_3135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T826" id="Seg_3137" n="HIAT:w" s="T825">onu</ts>
                  <nts id="Seg_3138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T827" id="Seg_3140" n="HIAT:w" s="T826">kaːjɨːga</ts>
                  <nts id="Seg_3141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T828" id="Seg_3143" n="HIAT:w" s="T827">olorputtar</ts>
                  <nts id="Seg_3144" n="HIAT:ip">.</nts>
                  <nts id="Seg_3145" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T830" id="Seg_3147" n="HIAT:u" s="T828">
                  <ts e="T829" id="Seg_3149" n="HIAT:w" s="T828">Kineːsterin</ts>
                  <nts id="Seg_3150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T830" id="Seg_3152" n="HIAT:w" s="T829">dʼüːllüːr</ts>
                  <nts id="Seg_3153" n="HIAT:ip">.</nts>
                  <nts id="Seg_3154" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T836" id="Seg_3156" n="HIAT:u" s="T830">
                  <ts e="T831" id="Seg_3158" n="HIAT:w" s="T830">Haːmaj</ts>
                  <nts id="Seg_3159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T832" id="Seg_3161" n="HIAT:w" s="T831">kineːhin</ts>
                  <nts id="Seg_3162" n="HIAT:ip">,</nts>
                  <nts id="Seg_3163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T833" id="Seg_3165" n="HIAT:w" s="T832">dʼuraːk</ts>
                  <nts id="Seg_3166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T834" id="Seg_3168" n="HIAT:w" s="T833">kineːhiniːn</ts>
                  <nts id="Seg_3169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T835" id="Seg_3171" n="HIAT:w" s="T834">türmege</ts>
                  <nts id="Seg_3172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T836" id="Seg_3174" n="HIAT:w" s="T835">olordor</ts>
                  <nts id="Seg_3175" n="HIAT:ip">.</nts>
                  <nts id="Seg_3176" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T840" id="Seg_3178" n="HIAT:u" s="T836">
                  <ts e="T837" id="Seg_3180" n="HIAT:w" s="T836">Tɨ͡a</ts>
                  <nts id="Seg_3181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T838" id="Seg_3183" n="HIAT:w" s="T837">kineːhin</ts>
                  <nts id="Seg_3184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T839" id="Seg_3186" n="HIAT:w" s="T838">kineːhitten</ts>
                  <nts id="Seg_3187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T840" id="Seg_3189" n="HIAT:w" s="T839">uhular</ts>
                  <nts id="Seg_3190" n="HIAT:ip">.</nts>
                  <nts id="Seg_3191" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T844" id="Seg_3193" n="HIAT:u" s="T840">
                  <ts e="T841" id="Seg_3195" n="HIAT:w" s="T840">U͡olun</ts>
                  <nts id="Seg_3196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T842" id="Seg_3198" n="HIAT:w" s="T841">haːmaj</ts>
                  <nts id="Seg_3199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T843" id="Seg_3201" n="HIAT:w" s="T842">kineːhe</ts>
                  <nts id="Seg_3202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T844" id="Seg_3204" n="HIAT:w" s="T843">oŋoror</ts>
                  <nts id="Seg_3205" n="HIAT:ip">.</nts>
                  <nts id="Seg_3206" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T848" id="Seg_3208" n="HIAT:u" s="T844">
                  <ts e="T845" id="Seg_3210" n="HIAT:w" s="T844">Ogonnʼor</ts>
                  <nts id="Seg_3211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T846" id="Seg_3213" n="HIAT:w" s="T845">itinnik</ts>
                  <nts id="Seg_3214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T847" id="Seg_3216" n="HIAT:w" s="T846">kirdige</ts>
                  <nts id="Seg_3217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T848" id="Seg_3219" n="HIAT:w" s="T847">taksɨbɨta</ts>
                  <nts id="Seg_3220" n="HIAT:ip">.</nts>
                  <nts id="Seg_3221" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T849" id="Seg_3223" n="HIAT:u" s="T848">
                  <ts e="T849" id="Seg_3225" n="HIAT:w" s="T848">Elete</ts>
                  <nts id="Seg_3226" n="HIAT:ip">.</nts>
                  <nts id="Seg_3227" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T849" id="Seg_3228" n="sc" s="T0">
               <ts e="T1" id="Seg_3230" n="e" s="T0">ɨraːktaːgɨlaːktar, </ts>
               <ts e="T2" id="Seg_3232" n="e" s="T1">küseːjinneːkter, </ts>
               <ts e="T3" id="Seg_3234" n="e" s="T2">kineːsteːkter </ts>
               <ts e="T4" id="Seg_3236" n="e" s="T3">bu͡ollaga. </ts>
               <ts e="T5" id="Seg_3238" n="e" s="T4">Bu </ts>
               <ts e="T6" id="Seg_3240" n="e" s="T5">kihi </ts>
               <ts e="T7" id="Seg_3242" n="e" s="T6">tuspa </ts>
               <ts e="T8" id="Seg_3244" n="e" s="T7">oloror, </ts>
               <ts e="T9" id="Seg_3246" n="e" s="T8">kohuːn </ts>
               <ts e="T10" id="Seg_3248" n="e" s="T9">agaj. </ts>
               <ts e="T11" id="Seg_3250" n="e" s="T10">Biːrkeːn </ts>
               <ts e="T12" id="Seg_3252" n="e" s="T11">da </ts>
               <ts e="T13" id="Seg_3254" n="e" s="T12">ogoto </ts>
               <ts e="T14" id="Seg_3256" n="e" s="T13">hu͡ok. </ts>
               <ts e="T15" id="Seg_3258" n="e" s="T14">Jɨlga </ts>
               <ts e="T16" id="Seg_3260" n="e" s="T15">biːrde </ts>
               <ts e="T17" id="Seg_3262" n="e" s="T16">tölüːr </ts>
               <ts e="T18" id="Seg_3264" n="e" s="T17">nolu͡ogun. </ts>
               <ts e="T19" id="Seg_3266" n="e" s="T18">Bajgal </ts>
               <ts e="T20" id="Seg_3268" n="e" s="T19">kɨrɨːtɨgar, </ts>
               <ts e="T21" id="Seg_3270" n="e" s="T20">ile </ts>
               <ts e="T22" id="Seg_3272" n="e" s="T21">mu͡oraga </ts>
               <ts e="T23" id="Seg_3274" n="e" s="T22">oloror, </ts>
               <ts e="T24" id="Seg_3276" n="e" s="T23">bajgal </ts>
               <ts e="T25" id="Seg_3278" n="e" s="T24">hɨlbagɨn </ts>
               <ts e="T26" id="Seg_3280" n="e" s="T25">ottunar. </ts>
               <ts e="T27" id="Seg_3282" n="e" s="T26">Ör </ts>
               <ts e="T28" id="Seg_3284" n="e" s="T27">olorbut, </ts>
               <ts e="T29" id="Seg_3286" n="e" s="T28">kɨrdʼɨbɨt. </ts>
               <ts e="T30" id="Seg_3288" n="e" s="T29">Dʼɨla </ts>
               <ts e="T31" id="Seg_3290" n="e" s="T30">haːstɨjan </ts>
               <ts e="T32" id="Seg_3292" n="e" s="T31">barar. </ts>
               <ts e="T33" id="Seg_3294" n="e" s="T32">Emeːksiniger </ts>
               <ts e="T34" id="Seg_3296" n="e" s="T33">eter: </ts>
               <ts e="T35" id="Seg_3298" n="e" s="T34">"Dʼɨlbɨt </ts>
               <ts e="T36" id="Seg_3300" n="e" s="T35">irde </ts>
               <ts e="T37" id="Seg_3302" n="e" s="T36">diː. </ts>
               <ts e="T38" id="Seg_3304" n="e" s="T37">Mas </ts>
               <ts e="T39" id="Seg_3306" n="e" s="T38">tardɨna </ts>
               <ts e="T40" id="Seg_3308" n="e" s="T39">barɨ͡am </ts>
               <ts e="T41" id="Seg_3310" n="e" s="T40">ehiːlgige. </ts>
               <ts e="T42" id="Seg_3312" n="e" s="T41">Otut </ts>
               <ts e="T43" id="Seg_3314" n="e" s="T42">hɨrgata </ts>
               <ts e="T44" id="Seg_3316" n="e" s="T43">hü͡ökkeː. </ts>
               <ts e="T45" id="Seg_3318" n="e" s="T44">Otut </ts>
               <ts e="T46" id="Seg_3320" n="e" s="T45">hɨrgannan </ts>
               <ts e="T47" id="Seg_3322" n="e" s="T46">bajgalga </ts>
               <ts e="T48" id="Seg_3324" n="e" s="T47">baran </ts>
               <ts e="T49" id="Seg_3326" n="e" s="T48">kaːlar </ts>
               <ts e="T50" id="Seg_3328" n="e" s="T49">hobuč-hogotogun. </ts>
               <ts e="T51" id="Seg_3330" n="e" s="T50">Bajgalga </ts>
               <ts e="T52" id="Seg_3332" n="e" s="T51">tumus </ts>
               <ts e="T53" id="Seg_3334" n="e" s="T52">bagajɨnan </ts>
               <ts e="T54" id="Seg_3336" n="e" s="T53">kiːrbit. </ts>
               <ts e="T55" id="Seg_3338" n="e" s="T54">Hogotok </ts>
               <ts e="T56" id="Seg_3340" n="e" s="T55">hirten </ts>
               <ts e="T57" id="Seg_3342" n="e" s="T56">ti͡emmit, </ts>
               <ts e="T58" id="Seg_3344" n="e" s="T57">hɨːrɨn </ts>
               <ts e="T59" id="Seg_3346" n="e" s="T58">ürdüger </ts>
               <ts e="T60" id="Seg_3348" n="e" s="T59">biːr </ts>
               <ts e="T61" id="Seg_3350" n="e" s="T60">halaː </ts>
               <ts e="T62" id="Seg_3352" n="e" s="T61">ustun </ts>
               <ts e="T63" id="Seg_3354" n="e" s="T62">tönnübüt. </ts>
               <ts e="T64" id="Seg_3356" n="e" s="T63">Bu </ts>
               <ts e="T65" id="Seg_3358" n="e" s="T64">taksan </ts>
               <ts e="T66" id="Seg_3360" n="e" s="T65">kajatɨn </ts>
               <ts e="T67" id="Seg_3362" n="e" s="T66">ürdüger </ts>
               <ts e="T68" id="Seg_3364" n="e" s="T67">tabaːk </ts>
               <ts e="T69" id="Seg_3366" n="e" s="T68">tarda </ts>
               <ts e="T70" id="Seg_3368" n="e" s="T69">olorbut. </ts>
               <ts e="T71" id="Seg_3370" n="e" s="T70">Ebe </ts>
               <ts e="T72" id="Seg_3372" n="e" s="T71">di͡ek </ts>
               <ts e="T73" id="Seg_3374" n="e" s="T72">körbüte, </ts>
               <ts e="T74" id="Seg_3376" n="e" s="T73">uːga </ts>
               <ts e="T75" id="Seg_3378" n="e" s="T74">ogo </ts>
               <ts e="T76" id="Seg_3380" n="e" s="T75">hɨldʼar, </ts>
               <ts e="T77" id="Seg_3382" n="e" s="T76">irbit </ts>
               <ts e="T78" id="Seg_3384" n="e" s="T77">uːga </ts>
               <ts e="T79" id="Seg_3386" n="e" s="T78">küːgetterinen </ts>
               <ts e="T80" id="Seg_3388" n="e" s="T79">oːnnʼuː </ts>
               <ts e="T81" id="Seg_3390" n="e" s="T80">hɨldʼar. </ts>
               <ts e="T82" id="Seg_3392" n="e" s="T81">U͡ol </ts>
               <ts e="T83" id="Seg_3394" n="e" s="T82">ogo </ts>
               <ts e="T84" id="Seg_3396" n="e" s="T83">bɨhɨːlaːk. </ts>
               <ts e="T85" id="Seg_3398" n="e" s="T84">Kantan </ts>
               <ts e="T86" id="Seg_3400" n="e" s="T85">da </ts>
               <ts e="T87" id="Seg_3402" n="e" s="T86">kelbite </ts>
               <ts e="T88" id="Seg_3404" n="e" s="T87">billibet. </ts>
               <ts e="T89" id="Seg_3406" n="e" s="T88">Oduːrgaːbɨt. </ts>
               <ts e="T90" id="Seg_3408" n="e" s="T89">"Min </ts>
               <ts e="T91" id="Seg_3410" n="e" s="T90">ogoto </ts>
               <ts e="T92" id="Seg_3412" n="e" s="T91">hu͡okpun </ts>
               <ts e="T93" id="Seg_3414" n="e" s="T92">dʼiː!" </ts>
               <ts e="T94" id="Seg_3416" n="e" s="T93">ɨlbɨt </ts>
               <ts e="T95" id="Seg_3418" n="e" s="T94">kihi", </ts>
               <ts e="T96" id="Seg_3420" n="e" s="T95">diː </ts>
               <ts e="T97" id="Seg_3422" n="e" s="T96">hanaːbɨt. </ts>
               <ts e="T98" id="Seg_3424" n="e" s="T97">Ogoto </ts>
               <ts e="T99" id="Seg_3426" n="e" s="T98">arɨː </ts>
               <ts e="T100" id="Seg_3428" n="e" s="T99">kumagɨgar </ts>
               <ts e="T101" id="Seg_3430" n="e" s="T100">hɨldʼar, </ts>
               <ts e="T102" id="Seg_3432" n="e" s="T101">ökögör </ts>
               <ts e="T103" id="Seg_3434" n="e" s="T102">mas </ts>
               <ts e="T104" id="Seg_3436" n="e" s="T103">baːr, </ts>
               <ts e="T105" id="Seg_3438" n="e" s="T104">onon </ts>
               <ts e="T106" id="Seg_3440" n="e" s="T105">bardaga. </ts>
               <ts e="T107" id="Seg_3442" n="e" s="T106">ɨstanan </ts>
               <ts e="T108" id="Seg_3444" n="e" s="T107">tijer </ts>
               <ts e="T109" id="Seg_3446" n="e" s="T108">ogonnʼor, </ts>
               <ts e="T110" id="Seg_3448" n="e" s="T109">argaːtɨttan </ts>
               <ts e="T111" id="Seg_3450" n="e" s="T110">kaban </ts>
               <ts e="T112" id="Seg_3452" n="e" s="T111">ɨlan </ts>
               <ts e="T113" id="Seg_3454" n="e" s="T112">hukujun </ts>
               <ts e="T114" id="Seg_3456" n="e" s="T113">ihiger </ts>
               <ts e="T115" id="Seg_3458" n="e" s="T114">ukput, </ts>
               <ts e="T116" id="Seg_3460" n="e" s="T115">kurdanan </ts>
               <ts e="T117" id="Seg_3462" n="e" s="T116">keːspit. </ts>
               <ts e="T118" id="Seg_3464" n="e" s="T117">Dʼe </ts>
               <ts e="T119" id="Seg_3466" n="e" s="T118">araj </ts>
               <ts e="T120" id="Seg_3468" n="e" s="T119">kamnɨːhɨ. </ts>
               <ts e="T121" id="Seg_3470" n="e" s="T120">Tɨ͡as </ts>
               <ts e="T122" id="Seg_3472" n="e" s="T121">koːkuna </ts>
               <ts e="T123" id="Seg_3474" n="e" s="T122">bu͡olar. </ts>
               <ts e="T124" id="Seg_3476" n="e" s="T123">Turbut. </ts>
               <ts e="T125" id="Seg_3478" n="e" s="T124">Körbüte, </ts>
               <ts e="T126" id="Seg_3480" n="e" s="T125">bütün </ts>
               <ts e="T127" id="Seg_3482" n="e" s="T126">munnʼak </ts>
               <ts e="T128" id="Seg_3484" n="e" s="T127">kihite </ts>
               <ts e="T129" id="Seg_3486" n="e" s="T128">ölbüt </ts>
               <ts e="T130" id="Seg_3488" n="e" s="T129">hire </ts>
               <ts e="T131" id="Seg_3490" n="e" s="T130">ebit. </ts>
               <ts e="T132" id="Seg_3492" n="e" s="T131">"Eː, </ts>
               <ts e="T133" id="Seg_3494" n="e" s="T132">balartan </ts>
               <ts e="T134" id="Seg_3496" n="e" s="T133">ordon </ts>
               <ts e="T135" id="Seg_3498" n="e" s="T134">kaːlbɨt </ts>
               <ts e="T136" id="Seg_3500" n="e" s="T135">ogo </ts>
               <ts e="T137" id="Seg_3502" n="e" s="T136">bu͡ollaga". </ts>
               <ts e="T138" id="Seg_3504" n="e" s="T137">Biːr </ts>
               <ts e="T139" id="Seg_3506" n="e" s="T138">argaːbɨttaːk </ts>
               <ts e="T140" id="Seg_3508" n="e" s="T139">nʼu͡oguhutun </ts>
               <ts e="T141" id="Seg_3510" n="e" s="T140">egelen </ts>
               <ts e="T142" id="Seg_3512" n="e" s="T141">ölörön </ts>
               <ts e="T143" id="Seg_3514" n="e" s="T142">keːher: </ts>
               <ts e="T144" id="Seg_3516" n="e" s="T143">"Ogogutun </ts>
               <ts e="T145" id="Seg_3518" n="e" s="T144">iltim, </ts>
               <ts e="T146" id="Seg_3520" n="e" s="T145">bu </ts>
               <ts e="T147" id="Seg_3522" n="e" s="T146">ogogut </ts>
               <ts e="T148" id="Seg_3524" n="e" s="T147">oŋkuta, </ts>
               <ts e="T149" id="Seg_3526" n="e" s="T148">biːr </ts>
               <ts e="T150" id="Seg_3528" n="e" s="T149">tabanɨ </ts>
               <ts e="T151" id="Seg_3530" n="e" s="T150">keːstim", </ts>
               <ts e="T152" id="Seg_3532" n="e" s="T151">diːr. </ts>
               <ts e="T153" id="Seg_3534" n="e" s="T152">Egelle </ts>
               <ts e="T154" id="Seg_3536" n="e" s="T153">dʼi͡etiger, </ts>
               <ts e="T155" id="Seg_3538" n="e" s="T154">emeːksiniger </ts>
               <ts e="T156" id="Seg_3540" n="e" s="T155">kepsiːr. </ts>
               <ts e="T157" id="Seg_3542" n="e" s="T156">Üs </ts>
               <ts e="T158" id="Seg_3544" n="e" s="T157">kün </ts>
               <ts e="T159" id="Seg_3546" n="e" s="T158">turkarɨ, </ts>
               <ts e="T160" id="Seg_3548" n="e" s="T159">tabalarɨn </ts>
               <ts e="T161" id="Seg_3550" n="e" s="T160">da </ts>
               <ts e="T162" id="Seg_3552" n="e" s="T161">ɨːppakka, </ts>
               <ts e="T163" id="Seg_3554" n="e" s="T162">bu </ts>
               <ts e="T164" id="Seg_3556" n="e" s="T163">ogonnon </ts>
               <ts e="T165" id="Seg_3558" n="e" s="T164">oːnʼuːllar. </ts>
               <ts e="T166" id="Seg_3560" n="e" s="T165">Ölgöbüːne </ts>
               <ts e="T167" id="Seg_3562" n="e" s="T166">barɨta </ts>
               <ts e="T168" id="Seg_3564" n="e" s="T167">kölülle </ts>
               <ts e="T169" id="Seg_3566" n="e" s="T168">hɨtar </ts>
               <ts e="T170" id="Seg_3568" n="e" s="T169">ebit. </ts>
               <ts e="T171" id="Seg_3570" n="e" s="T170">Tu͡ok </ts>
               <ts e="T172" id="Seg_3572" n="e" s="T171">da </ts>
               <ts e="T173" id="Seg_3574" n="e" s="T172">billibet. </ts>
               <ts e="T174" id="Seg_3576" n="e" s="T173">Ogoto </ts>
               <ts e="T175" id="Seg_3578" n="e" s="T174">hüːreliː </ts>
               <ts e="T176" id="Seg_3580" n="e" s="T175">hɨldʼar </ts>
               <ts e="T177" id="Seg_3582" n="e" s="T176">bu͡olbut, </ts>
               <ts e="T178" id="Seg_3584" n="e" s="T177">ulaːppɨt. </ts>
               <ts e="T179" id="Seg_3586" n="e" s="T178">Oduːluː </ts>
               <ts e="T180" id="Seg_3588" n="e" s="T179">olordoguna, </ts>
               <ts e="T181" id="Seg_3590" n="e" s="T180">hɨrgaga </ts>
               <ts e="T182" id="Seg_3592" n="e" s="T181">bi͡es </ts>
               <ts e="T183" id="Seg_3594" n="e" s="T182">čeːlkeː </ts>
               <ts e="T184" id="Seg_3596" n="e" s="T183">buːrdaːk </ts>
               <ts e="T185" id="Seg_3598" n="e" s="T184">čebis-čeːlkeː </ts>
               <ts e="T186" id="Seg_3600" n="e" s="T185">taŋastaːk </ts>
               <ts e="T187" id="Seg_3602" n="e" s="T186">kihi </ts>
               <ts e="T188" id="Seg_3604" n="e" s="T187">iher. </ts>
               <ts e="T189" id="Seg_3606" n="e" s="T188">Kineːs </ts>
               <ts e="T190" id="Seg_3608" n="e" s="T189">ebit. </ts>
               <ts e="T191" id="Seg_3610" n="e" s="T190">"Dʼe </ts>
               <ts e="T192" id="Seg_3612" n="e" s="T191">tu͡okpun </ts>
               <ts e="T193" id="Seg_3614" n="e" s="T192">bi͡eri͡emij", </ts>
               <ts e="T194" id="Seg_3616" n="e" s="T193">diː </ts>
               <ts e="T195" id="Seg_3618" n="e" s="T194">hanɨːr. </ts>
               <ts e="T196" id="Seg_3620" n="e" s="T195">"Ogonnʼor, </ts>
               <ts e="T197" id="Seg_3622" n="e" s="T196">kajdak </ts>
               <ts e="T198" id="Seg_3624" n="e" s="T197">olorduŋ? </ts>
               <ts e="T199" id="Seg_3626" n="e" s="T198">Kajdak </ts>
               <ts e="T200" id="Seg_3628" n="e" s="T199">mannɨk </ts>
               <ts e="T201" id="Seg_3630" n="e" s="T200">ör </ts>
               <ts e="T202" id="Seg_3632" n="e" s="T201">bu͡ollum, </ts>
               <ts e="T203" id="Seg_3634" n="e" s="T202">ogolommukkun </ts>
               <ts e="T204" id="Seg_3636" n="e" s="T203">duː", </ts>
               <ts e="T205" id="Seg_3638" n="e" s="T204">diːr </ts>
               <ts e="T206" id="Seg_3640" n="e" s="T205">kineːs. </ts>
               <ts e="T207" id="Seg_3642" n="e" s="T206">"Kɨrdʼar </ts>
               <ts e="T208" id="Seg_3644" n="e" s="T207">haːspɨtɨgar </ts>
               <ts e="T209" id="Seg_3646" n="e" s="T208">ogolonnubut", </ts>
               <ts e="T210" id="Seg_3648" n="e" s="T209">diːr </ts>
               <ts e="T211" id="Seg_3650" n="e" s="T210">ogonnʼor. </ts>
               <ts e="T212" id="Seg_3652" n="e" s="T211">"Dʼe </ts>
               <ts e="T213" id="Seg_3654" n="e" s="T212">üčügej. </ts>
               <ts e="T214" id="Seg_3656" n="e" s="T213">Nolu͡ok </ts>
               <ts e="T215" id="Seg_3658" n="e" s="T214">komuja </ts>
               <ts e="T216" id="Seg_3660" n="e" s="T215">kellim", </ts>
               <ts e="T217" id="Seg_3662" n="e" s="T216">diːr </ts>
               <ts e="T218" id="Seg_3664" n="e" s="T217">kineːs. </ts>
               <ts e="T219" id="Seg_3666" n="e" s="T218">Ogonnʼor </ts>
               <ts e="T220" id="Seg_3668" n="e" s="T219">töhö </ts>
               <ts e="T221" id="Seg_3670" n="e" s="T220">eme </ts>
               <ts e="T222" id="Seg_3672" n="e" s="T221">küːl </ts>
               <ts e="T223" id="Seg_3674" n="e" s="T222">kɨrsanɨ </ts>
               <ts e="T224" id="Seg_3676" n="e" s="T223">bi͡erer. </ts>
               <ts e="T225" id="Seg_3678" n="e" s="T224">Kineːhe </ts>
               <ts e="T226" id="Seg_3680" n="e" s="T225">ü͡örer, </ts>
               <ts e="T227" id="Seg_3682" n="e" s="T226">"ordugun </ts>
               <ts e="T228" id="Seg_3684" n="e" s="T227">karčiː </ts>
               <ts e="T229" id="Seg_3686" n="e" s="T228">ɨːtɨ͡am", </ts>
               <ts e="T230" id="Seg_3688" n="e" s="T229">diːr. </ts>
               <ts e="T231" id="Seg_3690" n="e" s="T230">Kineːhe </ts>
               <ts e="T232" id="Seg_3692" n="e" s="T231">kamnɨːhɨ. </ts>
               <ts e="T233" id="Seg_3694" n="e" s="T232">Kanna </ts>
               <ts e="T234" id="Seg_3696" n="e" s="T233">ere </ts>
               <ts e="T235" id="Seg_3698" n="e" s="T234">dʼuraːk </ts>
               <ts e="T236" id="Seg_3700" n="e" s="T235">kineːhiger </ts>
               <ts e="T237" id="Seg_3702" n="e" s="T236">kötütte, </ts>
               <ts e="T238" id="Seg_3704" n="e" s="T237">dogorugar. </ts>
               <ts e="T239" id="Seg_3706" n="e" s="T238">Kineːs </ts>
               <ts e="T240" id="Seg_3708" n="e" s="T239">kineːhiger </ts>
               <ts e="T241" id="Seg_3710" n="e" s="T240">kelbit. </ts>
               <ts e="T242" id="Seg_3712" n="e" s="T241">Argilɨː </ts>
               <ts e="T243" id="Seg_3714" n="e" s="T242">hɨtallar. </ts>
               <ts e="T244" id="Seg_3716" n="e" s="T243">Kas </ts>
               <ts e="T245" id="Seg_3718" n="e" s="T244">kün </ts>
               <ts e="T246" id="Seg_3720" n="e" s="T245">argilɨː </ts>
               <ts e="T247" id="Seg_3722" n="e" s="T246">hɨppɨttar. </ts>
               <ts e="T248" id="Seg_3724" n="e" s="T247">Ol </ts>
               <ts e="T249" id="Seg_3726" n="e" s="T248">oloron </ts>
               <ts e="T250" id="Seg_3728" n="e" s="T249">ös </ts>
               <ts e="T251" id="Seg_3730" n="e" s="T250">kepseːbit </ts>
               <ts e="T252" id="Seg_3732" n="e" s="T251">dogorugar: </ts>
               <ts e="T253" id="Seg_3734" n="e" s="T252">"Dʼe-dʼe, </ts>
               <ts e="T254" id="Seg_3736" n="e" s="T253">min </ts>
               <ts e="T255" id="Seg_3738" n="e" s="T254">kihi </ts>
               <ts e="T256" id="Seg_3740" n="e" s="T255">kuttanar </ts>
               <ts e="T257" id="Seg_3742" n="e" s="T256">kihileːkpin. </ts>
               <ts e="T258" id="Seg_3744" n="e" s="T257">Nolu͡ogun </ts>
               <ts e="T259" id="Seg_3746" n="e" s="T258">ol </ts>
               <ts e="T260" id="Seg_3748" n="e" s="T259">ereːri </ts>
               <ts e="T261" id="Seg_3750" n="e" s="T260">aharɨ </ts>
               <ts e="T262" id="Seg_3752" n="e" s="T261">tölüːr. </ts>
               <ts e="T263" id="Seg_3754" n="e" s="T262">Onton </ts>
               <ts e="T264" id="Seg_3756" n="e" s="T263">kellim. </ts>
               <ts e="T265" id="Seg_3758" n="e" s="T264">Oduːrguːbun— </ts>
               <ts e="T266" id="Seg_3760" n="e" s="T265">hüːre </ts>
               <ts e="T267" id="Seg_3762" n="e" s="T266">hɨldʼar </ts>
               <ts e="T268" id="Seg_3764" n="e" s="T267">ogolommut. </ts>
               <ts e="T269" id="Seg_3766" n="e" s="T268">Bejete </ts>
               <ts e="T270" id="Seg_3768" n="e" s="T269">töröːbüt </ts>
               <ts e="T271" id="Seg_3770" n="e" s="T270">ogoto </ts>
               <ts e="T272" id="Seg_3772" n="e" s="T271">bu͡olbatak. </ts>
               <ts e="T273" id="Seg_3774" n="e" s="T272">Kantan </ts>
               <ts e="T274" id="Seg_3776" n="e" s="T273">ɨlbɨta </ts>
               <ts e="T275" id="Seg_3778" n="e" s="T274">bu͡olu͡oj? </ts>
               <ts e="T276" id="Seg_3780" n="e" s="T275">Kimi </ts>
               <ts e="T277" id="Seg_3782" n="e" s="T276">ere </ts>
               <ts e="T278" id="Seg_3784" n="e" s="T277">dʼürü </ts>
               <ts e="T279" id="Seg_3786" n="e" s="T278">ölörön </ts>
               <ts e="T280" id="Seg_3788" n="e" s="T279">ɨllaga?" </ts>
               <ts e="T281" id="Seg_3790" n="e" s="T280">"Eːk", </ts>
               <ts e="T282" id="Seg_3792" n="e" s="T281">diːr </ts>
               <ts e="T283" id="Seg_3794" n="e" s="T282">dʼuraːk </ts>
               <ts e="T284" id="Seg_3796" n="e" s="T283">kineːhe, </ts>
               <ts e="T285" id="Seg_3798" n="e" s="T284">"bɨlɨr </ts>
               <ts e="T286" id="Seg_3800" n="e" s="T285">min </ts>
               <ts e="T287" id="Seg_3802" n="e" s="T286">biːr </ts>
               <ts e="T288" id="Seg_3804" n="e" s="T287">munʼak </ts>
               <ts e="T289" id="Seg_3806" n="e" s="T288">kihim </ts>
               <ts e="T290" id="Seg_3808" n="e" s="T289">ölbüte. </ts>
               <ts e="T291" id="Seg_3810" n="e" s="T290">Onton </ts>
               <ts e="T292" id="Seg_3812" n="e" s="T291">biːr </ts>
               <ts e="T293" id="Seg_3814" n="e" s="T292">bihikteːk </ts>
               <ts e="T294" id="Seg_3816" n="e" s="T293">ogo </ts>
               <ts e="T295" id="Seg_3818" n="e" s="T294">kaːlbɨt </ts>
               <ts e="T296" id="Seg_3820" n="e" s="T295">huraktaːga. </ts>
               <ts e="T297" id="Seg_3822" n="e" s="T296">Onu </ts>
               <ts e="T298" id="Seg_3824" n="e" s="T297">ɨllarbatagɨm </ts>
               <ts e="T299" id="Seg_3826" n="e" s="T298">ɨ͡arɨː </ts>
               <ts e="T300" id="Seg_3828" n="e" s="T299">batɨhɨ͡a </ts>
               <ts e="T301" id="Seg_3830" n="e" s="T300">di͡emmin. </ts>
               <ts e="T302" id="Seg_3832" n="e" s="T301">Onu </ts>
               <ts e="T303" id="Seg_3834" n="e" s="T302">ɨllaga. </ts>
               <ts e="T304" id="Seg_3836" n="e" s="T303">Ol </ts>
               <ts e="T305" id="Seg_3838" n="e" s="T304">ogonu </ts>
               <ts e="T306" id="Seg_3840" n="e" s="T305">kisti͡ebit </ts>
               <ts e="T307" id="Seg_3842" n="e" s="T306">ebit. </ts>
               <ts e="T308" id="Seg_3844" n="e" s="T307">"Miːgin </ts>
               <ts e="T309" id="Seg_3846" n="e" s="T308">ölörü͡öktere" </ts>
               <ts e="T310" id="Seg_3848" n="e" s="T309">di͡en. </ts>
               <ts e="T311" id="Seg_3850" n="e" s="T310">Dʼe </ts>
               <ts e="T312" id="Seg_3852" n="e" s="T311">bugurduk. </ts>
               <ts e="T313" id="Seg_3854" n="e" s="T312">Hübeliːbin, </ts>
               <ts e="T314" id="Seg_3856" n="e" s="T313">en </ts>
               <ts e="T315" id="Seg_3858" n="e" s="T314">kihigitten </ts>
               <ts e="T316" id="Seg_3860" n="e" s="T315">kuttanɨma. </ts>
               <ts e="T317" id="Seg_3862" n="e" s="T316">Ol </ts>
               <ts e="T318" id="Seg_3864" n="e" s="T317">munnʼak </ts>
               <ts e="T319" id="Seg_3866" n="e" s="T318">dʼonun </ts>
               <ts e="T320" id="Seg_3868" n="e" s="T319">kɨrgan </ts>
               <ts e="T321" id="Seg_3870" n="e" s="T320">baran </ts>
               <ts e="T322" id="Seg_3872" n="e" s="T321">ogonu </ts>
               <ts e="T323" id="Seg_3874" n="e" s="T322">ɨlbɨt </ts>
               <ts e="T324" id="Seg_3876" n="e" s="T323">di͡en </ts>
               <ts e="T325" id="Seg_3878" n="e" s="T324">ikki </ts>
               <ts e="T326" id="Seg_3880" n="e" s="T325">kineːs </ts>
               <ts e="T327" id="Seg_3882" n="e" s="T326">ɨraːktaːgɨta </ts>
               <ts e="T328" id="Seg_3884" n="e" s="T327">üŋsen </ts>
               <ts e="T329" id="Seg_3886" n="e" s="T328">huruju͡ok. </ts>
               <ts e="T330" id="Seg_3888" n="e" s="T329">Oččogo </ts>
               <ts e="T331" id="Seg_3890" n="e" s="T330">bihigi </ts>
               <ts e="T332" id="Seg_3892" n="e" s="T331">tɨlbɨtɨn </ts>
               <ts e="T333" id="Seg_3894" n="e" s="T332">ɨraːktaːgɨ </ts>
               <ts e="T334" id="Seg_3896" n="e" s="T333">bɨhɨ͡a </ts>
               <ts e="T335" id="Seg_3898" n="e" s="T334">hu͡oga, </ts>
               <ts e="T336" id="Seg_3900" n="e" s="T335">kinini </ts>
               <ts e="T337" id="Seg_3902" n="e" s="T336">huːttatɨ͡a, </ts>
               <ts e="T338" id="Seg_3904" n="e" s="T337">kaːjɨːga </ts>
               <ts e="T339" id="Seg_3906" n="e" s="T338">ɨːtɨ͡aga, </ts>
               <ts e="T340" id="Seg_3908" n="e" s="T339">bihigi </ts>
               <ts e="T341" id="Seg_3910" n="e" s="T340">kini </ts>
               <ts e="T342" id="Seg_3912" n="e" s="T341">baːjɨn </ts>
               <ts e="T343" id="Seg_3914" n="e" s="T342">ɨlɨ͡akpɨt", </ts>
               <ts e="T344" id="Seg_3916" n="e" s="T343">dʼi͡ebit </ts>
               <ts e="T345" id="Seg_3918" n="e" s="T344">dʼuraːk </ts>
               <ts e="T346" id="Seg_3920" n="e" s="T345">kini͡ehe. </ts>
               <ts e="T347" id="Seg_3922" n="e" s="T346">"Kihi͡eke </ts>
               <ts e="T348" id="Seg_3924" n="e" s="T347">kütüreːtekke </ts>
               <ts e="T349" id="Seg_3926" n="e" s="T348">kuhagan </ts>
               <ts e="T350" id="Seg_3928" n="e" s="T349">bu͡olaːraj", </ts>
               <ts e="T351" id="Seg_3930" n="e" s="T350">diːr </ts>
               <ts e="T352" id="Seg_3932" n="e" s="T351">haːmaj </ts>
               <ts e="T353" id="Seg_3934" n="e" s="T352">kineːhe. </ts>
               <ts e="T354" id="Seg_3936" n="e" s="T353">"Tɨ͡a </ts>
               <ts e="T355" id="Seg_3938" n="e" s="T354">kineːhin </ts>
               <ts e="T356" id="Seg_3940" n="e" s="T355">ɨgɨrbɨt </ts>
               <ts e="T357" id="Seg_3942" n="e" s="T356">bu͡ollar, </ts>
               <ts e="T358" id="Seg_3944" n="e" s="T357">üs </ts>
               <ts e="T359" id="Seg_3946" n="e" s="T358">kineːs </ts>
               <ts e="T360" id="Seg_3948" n="e" s="T359">itegelleːk </ts>
               <ts e="T361" id="Seg_3950" n="e" s="T360">bu͡olu͡ok </ts>
               <ts e="T362" id="Seg_3952" n="e" s="T361">ete." </ts>
               <ts e="T363" id="Seg_3954" n="e" s="T362">Dʼuraːk </ts>
               <ts e="T364" id="Seg_3956" n="e" s="T363">kineːhe: </ts>
               <ts e="T365" id="Seg_3958" n="e" s="T364">"Kihite </ts>
               <ts e="T366" id="Seg_3960" n="e" s="T365">ɨːtɨ͡am </ts>
               <ts e="T367" id="Seg_3962" n="e" s="T366">tɨ͡alar </ts>
               <ts e="T368" id="Seg_3964" n="e" s="T367">kineːsteriger, </ts>
               <ts e="T369" id="Seg_3966" n="e" s="T368">üs </ts>
               <ts e="T370" id="Seg_3968" n="e" s="T369">konugunan </ts>
               <ts e="T371" id="Seg_3970" n="e" s="T370">keli͡e. </ts>
               <ts e="T372" id="Seg_3972" n="e" s="T371">Höbülemmetegine </ts>
               <ts e="T373" id="Seg_3974" n="e" s="T372">bejetin </ts>
               <ts e="T374" id="Seg_3976" n="e" s="T373">ölörüllü͡ö. </ts>
               <ts e="T375" id="Seg_3978" n="e" s="T374">Utarsɨ͡a </ts>
               <ts e="T376" id="Seg_3980" n="e" s="T375">hu͡oga", </ts>
               <ts e="T377" id="Seg_3982" n="e" s="T376">diːr. </ts>
               <ts e="T378" id="Seg_3984" n="e" s="T377">Hɨrgalaːk </ts>
               <ts e="T379" id="Seg_3986" n="e" s="T378">kihini </ts>
               <ts e="T380" id="Seg_3988" n="e" s="T379">ɨːtta </ts>
               <ts e="T381" id="Seg_3990" n="e" s="T380">tɨ͡a </ts>
               <ts e="T382" id="Seg_3992" n="e" s="T381">kineːhin </ts>
               <ts e="T383" id="Seg_3994" n="e" s="T382">ɨgɨrtara. </ts>
               <ts e="T384" id="Seg_3996" n="e" s="T383">Ühüs </ts>
               <ts e="T385" id="Seg_3998" n="e" s="T384">künüger </ts>
               <ts e="T386" id="Seg_4000" n="e" s="T385">tɨ͡a </ts>
               <ts e="T387" id="Seg_4002" n="e" s="T386">kineːhe </ts>
               <ts e="T388" id="Seg_4004" n="e" s="T387">keler, </ts>
               <ts e="T389" id="Seg_4006" n="e" s="T388">oguru͡o </ts>
               <ts e="T390" id="Seg_4008" n="e" s="T389">taŋastaːk. </ts>
               <ts e="T391" id="Seg_4010" n="e" s="T390">Üs </ts>
               <ts e="T392" id="Seg_4012" n="e" s="T391">kas </ts>
               <ts e="T393" id="Seg_4014" n="e" s="T392">kün </ts>
               <ts e="T394" id="Seg_4016" n="e" s="T393">argilɨːllar. </ts>
               <ts e="T395" id="Seg_4018" n="e" s="T394">Dʼuraːk </ts>
               <ts e="T396" id="Seg_4020" n="e" s="T395">kineːhe </ts>
               <ts e="T397" id="Seg_4022" n="e" s="T396">kepsiːr: </ts>
               <ts e="T398" id="Seg_4024" n="e" s="T397">"Haːmaj </ts>
               <ts e="T399" id="Seg_4026" n="e" s="T398">kihite </ts>
               <ts e="T400" id="Seg_4028" n="e" s="T399">ɨ͡arɨːttan </ts>
               <ts e="T401" id="Seg_4030" n="e" s="T400">ölbüt </ts>
               <ts e="T402" id="Seg_4032" n="e" s="T401">dʼonton </ts>
               <ts e="T403" id="Seg_4034" n="e" s="T402">biːr </ts>
               <ts e="T404" id="Seg_4036" n="e" s="T403">ogonu </ts>
               <ts e="T405" id="Seg_4038" n="e" s="T404">ɨlbɨt. </ts>
               <ts e="T406" id="Seg_4040" n="e" s="T405">Onu </ts>
               <ts e="T407" id="Seg_4042" n="e" s="T406">ɨ͡arɨː </ts>
               <ts e="T408" id="Seg_4044" n="e" s="T407">batɨhɨ͡a. </ts>
               <ts e="T409" id="Seg_4046" n="e" s="T408">Bihigi </ts>
               <ts e="T410" id="Seg_4048" n="e" s="T409">ɨraːktaːgɨga </ts>
               <ts e="T411" id="Seg_4050" n="e" s="T410">üŋsü͡ögüŋ, </ts>
               <ts e="T412" id="Seg_4052" n="e" s="T411">ühü͡ön </ts>
               <ts e="T413" id="Seg_4054" n="e" s="T412">huruju͡oguŋ." </ts>
               <ts e="T414" id="Seg_4056" n="e" s="T413">Tɨ͡a </ts>
               <ts e="T415" id="Seg_4058" n="e" s="T414">kineːhe </ts>
               <ts e="T416" id="Seg_4060" n="e" s="T415">duːmajdaːbɨt, </ts>
               <ts e="T417" id="Seg_4062" n="e" s="T416">dʼi͡ebit: </ts>
               <ts e="T418" id="Seg_4064" n="e" s="T417">"Huːttuːrun </ts>
               <ts e="T419" id="Seg_4066" n="e" s="T418">huːttaːmɨja. </ts>
               <ts e="T420" id="Seg_4068" n="e" s="T419">Ol </ts>
               <ts e="T421" id="Seg_4070" n="e" s="T420">ereːri </ts>
               <ts e="T422" id="Seg_4072" n="e" s="T421">kütürüːr </ts>
               <ts e="T423" id="Seg_4074" n="e" s="T422">hüre </ts>
               <ts e="T424" id="Seg_4076" n="e" s="T423">bert </ts>
               <ts e="T425" id="Seg_4078" n="e" s="T424">dʼonu </ts>
               <ts e="T426" id="Seg_4080" n="e" s="T425">kejbit </ts>
               <ts e="T427" id="Seg_4082" n="e" s="T426">di͡en. </ts>
               <ts e="T428" id="Seg_4084" n="e" s="T427">Tulaːjagi </ts>
               <ts e="T429" id="Seg_4086" n="e" s="T428">ɨlbɨta </ts>
               <ts e="T430" id="Seg_4088" n="e" s="T429">tu͡ok </ts>
               <ts e="T431" id="Seg_4090" n="e" s="T430">burujaj, </ts>
               <ts e="T432" id="Seg_4092" n="e" s="T431">huːkka </ts>
               <ts e="T433" id="Seg_4094" n="e" s="T432">bi͡eri͡ekpitin", </ts>
               <ts e="T434" id="Seg_4096" n="e" s="T433">dʼi͡ebit. </ts>
               <ts e="T435" id="Seg_4098" n="e" s="T434">Dʼuraːk </ts>
               <ts e="T436" id="Seg_4100" n="e" s="T435">kineːhe: </ts>
               <ts e="T437" id="Seg_4102" n="e" s="T436">"En </ts>
               <ts e="T438" id="Seg_4104" n="e" s="T437">kömüsküːgün </ts>
               <ts e="T439" id="Seg_4106" n="e" s="T438">du͡o </ts>
               <ts e="T440" id="Seg_4108" n="e" s="T439">ol </ts>
               <ts e="T441" id="Seg_4110" n="e" s="T440">kihini? </ts>
               <ts e="T442" id="Seg_4112" n="e" s="T441">Oččogo </ts>
               <ts e="T443" id="Seg_4114" n="e" s="T442">bihigi </ts>
               <ts e="T444" id="Seg_4116" n="e" s="T443">enʼigin </ts>
               <ts e="T445" id="Seg_4118" n="e" s="T444">kejebit. </ts>
               <ts e="T446" id="Seg_4120" n="e" s="T445">Baːjgɨn </ts>
               <ts e="T447" id="Seg_4122" n="e" s="T446">ɨlɨ͡akpɨt", </ts>
               <ts e="T448" id="Seg_4124" n="e" s="T447">diːr. </ts>
               <ts e="T449" id="Seg_4126" n="e" s="T448">Tɨ͡a </ts>
               <ts e="T450" id="Seg_4128" n="e" s="T449">kineːhe </ts>
               <ts e="T451" id="Seg_4130" n="e" s="T450">kuttammɨt. </ts>
               <ts e="T452" id="Seg_4132" n="e" s="T451">Ölörü͡öksüt </ts>
               <ts e="T453" id="Seg_4134" n="e" s="T452">dʼon </ts>
               <ts e="T454" id="Seg_4136" n="e" s="T453">ikki͡en. </ts>
               <ts e="T455" id="Seg_4138" n="e" s="T454">"Dʼe </ts>
               <ts e="T456" id="Seg_4140" n="e" s="T455">kajtak </ts>
               <ts e="T457" id="Seg_4142" n="e" s="T456">hübeliːgit", </ts>
               <ts e="T458" id="Seg_4144" n="e" s="T457">dʼi͡ebit. </ts>
               <ts e="T459" id="Seg_4146" n="e" s="T458">Dʼuraːk </ts>
               <ts e="T460" id="Seg_4148" n="e" s="T459">kineːhe: </ts>
               <ts e="T461" id="Seg_4150" n="e" s="T460">"Min </ts>
               <ts e="T462" id="Seg_4152" n="e" s="T461">tojon </ts>
               <ts e="T463" id="Seg_4154" n="e" s="T462">hübehit </ts>
               <ts e="T464" id="Seg_4156" n="e" s="T463">bu͡olabɨn. </ts>
               <ts e="T465" id="Seg_4158" n="e" s="T464">Üŋsü͡ögüŋ </ts>
               <ts e="T466" id="Seg_4160" n="e" s="T465">ɨraːktaːgɨga, </ts>
               <ts e="T467" id="Seg_4162" n="e" s="T466">bu </ts>
               <ts e="T468" id="Seg_4164" n="e" s="T467">kohuːn </ts>
               <ts e="T469" id="Seg_4166" n="e" s="T468">haːmaj </ts>
               <ts e="T470" id="Seg_4168" n="e" s="T469">munnʼagɨn </ts>
               <ts e="T471" id="Seg_4170" n="e" s="T470">kejen </ts>
               <ts e="T472" id="Seg_4172" n="e" s="T471">baran </ts>
               <ts e="T473" id="Seg_4174" n="e" s="T472">ogonu </ts>
               <ts e="T474" id="Seg_4176" n="e" s="T473">ɨlbɨt </ts>
               <ts e="T475" id="Seg_4178" n="e" s="T474">di͡en </ts>
               <ts e="T476" id="Seg_4180" n="e" s="T475">huruju͡okput. </ts>
               <ts e="T477" id="Seg_4182" n="e" s="T476">Min </ts>
               <ts e="T478" id="Seg_4184" n="e" s="T477">kihilerim </ts>
               <ts e="T479" id="Seg_4186" n="e" s="T478">baran </ts>
               <ts e="T480" id="Seg_4188" n="e" s="T479">ölbüt </ts>
               <ts e="T481" id="Seg_4190" n="e" s="T480">dʼonu </ts>
               <ts e="T482" id="Seg_4192" n="e" s="T481">ɨtɨ͡alɨ͡aktara, </ts>
               <ts e="T483" id="Seg_4194" n="e" s="T482">komissija </ts>
               <ts e="T484" id="Seg_4196" n="e" s="T483">kellegine </ts>
               <ts e="T485" id="Seg_4198" n="e" s="T484">bulan </ts>
               <ts e="T486" id="Seg_4200" n="e" s="T485">ɨlɨ͡a </ts>
               <ts e="T487" id="Seg_4202" n="e" s="T486">hu͡oga. </ts>
               <ts e="T488" id="Seg_4204" n="e" s="T487">Kim </ts>
               <ts e="T489" id="Seg_4206" n="e" s="T488">taːjɨ͡aj", </ts>
               <ts e="T490" id="Seg_4208" n="e" s="T489">diːr. </ts>
               <ts e="T491" id="Seg_4210" n="e" s="T490">U͡onča </ts>
               <ts e="T492" id="Seg_4212" n="e" s="T491">kihi </ts>
               <ts e="T493" id="Seg_4214" n="e" s="T492">baran </ts>
               <ts e="T494" id="Seg_4216" n="e" s="T493">bɨlɨr </ts>
               <ts e="T495" id="Seg_4218" n="e" s="T494">ölbüt </ts>
               <ts e="T496" id="Seg_4220" n="e" s="T495">dʼonu </ts>
               <ts e="T497" id="Seg_4222" n="e" s="T496">ɨtɨ͡alɨːllar. </ts>
               <ts e="T498" id="Seg_4224" n="e" s="T497">Hurujan </ts>
               <ts e="T499" id="Seg_4226" n="e" s="T498">keːstiler </ts>
               <ts e="T500" id="Seg_4228" n="e" s="T499">ogorduk. </ts>
               <ts e="T501" id="Seg_4230" n="e" s="T500">Haːmaj </ts>
               <ts e="T502" id="Seg_4232" n="e" s="T501">kineːhin </ts>
               <ts e="T503" id="Seg_4234" n="e" s="T502">ɨːtallar </ts>
               <ts e="T504" id="Seg_4236" n="e" s="T503">dʼuraːk </ts>
               <ts e="T505" id="Seg_4238" n="e" s="T504">kineːhiniːn. </ts>
               <ts e="T506" id="Seg_4240" n="e" s="T505">ɨraːktaːgɨ </ts>
               <ts e="T507" id="Seg_4242" n="e" s="T506">aːgan </ts>
               <ts e="T508" id="Seg_4244" n="e" s="T507">körör, </ts>
               <ts e="T509" id="Seg_4246" n="e" s="T508">u͡onča </ts>
               <ts e="T510" id="Seg_4248" n="e" s="T509">dʼi͡e </ts>
               <ts e="T511" id="Seg_4250" n="e" s="T510">dʼonu </ts>
               <ts e="T512" id="Seg_4252" n="e" s="T511">kejbit, </ts>
               <ts e="T513" id="Seg_4254" n="e" s="T512">ogonu </ts>
               <ts e="T514" id="Seg_4256" n="e" s="T513">u͡orbut </ts>
               <ts e="T515" id="Seg_4258" n="e" s="T514">kihi. </ts>
               <ts e="T516" id="Seg_4260" n="e" s="T515">ɨraːktaːgɨ </ts>
               <ts e="T517" id="Seg_4262" n="e" s="T516">barɨlarɨttan </ts>
               <ts e="T518" id="Seg_4264" n="e" s="T517">ɨjɨtar: </ts>
               <ts e="T519" id="Seg_4266" n="e" s="T518">Kirdik </ts>
               <ts e="T520" id="Seg_4268" n="e" s="T519">du͡o? </ts>
               <ts e="T521" id="Seg_4270" n="e" s="T520">Min </ts>
               <ts e="T522" id="Seg_4272" n="e" s="T521">kinini </ts>
               <ts e="T523" id="Seg_4274" n="e" s="T522">huːttu͡om, </ts>
               <ts e="T524" id="Seg_4276" n="e" s="T523">ehigi </ts>
               <ts e="T525" id="Seg_4278" n="e" s="T524">istergitiger." </ts>
               <ts e="T526" id="Seg_4280" n="e" s="T525">Haːmaj </ts>
               <ts e="T527" id="Seg_4282" n="e" s="T526">kineːhe </ts>
               <ts e="T528" id="Seg_4284" n="e" s="T527">"kihi͡eke </ts>
               <ts e="T529" id="Seg_4286" n="e" s="T528">hugahaːbat </ts>
               <ts e="T530" id="Seg_4288" n="e" s="T529">kihi, </ts>
               <ts e="T531" id="Seg_4290" n="e" s="T530">oŋorto </ts>
               <ts e="T532" id="Seg_4292" n="e" s="T531">bu͡olu͡o" </ts>
               <ts e="T533" id="Seg_4294" n="e" s="T532">diːr. </ts>
               <ts e="T534" id="Seg_4296" n="e" s="T533">Ogonnʼorgo </ts>
               <ts e="T535" id="Seg_4298" n="e" s="T534">albun </ts>
               <ts e="T536" id="Seg_4300" n="e" s="T535">huruk </ts>
               <ts e="T537" id="Seg_4302" n="e" s="T536">ɨːtallar. </ts>
               <ts e="T538" id="Seg_4304" n="e" s="T537">Kineːhe </ts>
               <ts e="T539" id="Seg_4306" n="e" s="T538">če, </ts>
               <ts e="T540" id="Seg_4308" n="e" s="T539">nolu͡ogun </ts>
               <ts e="T541" id="Seg_4310" n="e" s="T540">karčɨtɨn </ts>
               <ts e="T542" id="Seg_4312" n="e" s="T541">ɨla </ts>
               <ts e="T543" id="Seg_4314" n="e" s="T542">kelbekteːtin </ts>
               <ts e="T544" id="Seg_4316" n="e" s="T543">bert </ts>
               <ts e="T545" id="Seg_4318" n="e" s="T544">muŋ </ts>
               <ts e="T546" id="Seg_4320" n="e" s="T545">türgennik </ts>
               <ts e="T547" id="Seg_4322" n="e" s="T546">di͡en </ts>
               <ts e="T548" id="Seg_4324" n="e" s="T547">hurujar. </ts>
               <ts e="T549" id="Seg_4326" n="e" s="T548">Ildʼit </ts>
               <ts e="T550" id="Seg_4328" n="e" s="T549">kihi </ts>
               <ts e="T551" id="Seg_4330" n="e" s="T550">ti͡erder </ts>
               <ts e="T552" id="Seg_4332" n="e" s="T551">hurugu. </ts>
               <ts e="T553" id="Seg_4334" n="e" s="T552">Ogonnʼor </ts>
               <ts e="T554" id="Seg_4336" n="e" s="T553">muŋ </ts>
               <ts e="T555" id="Seg_4338" n="e" s="T554">ütü͡ö </ts>
               <ts e="T556" id="Seg_4340" n="e" s="T555">tabalarɨnan </ts>
               <ts e="T557" id="Seg_4342" n="e" s="T556">kötüten </ts>
               <ts e="T558" id="Seg_4344" n="e" s="T557">keler. </ts>
               <ts e="T559" id="Seg_4346" n="e" s="T558">Tabatɨn </ts>
               <ts e="T560" id="Seg_4348" n="e" s="T559">baːjbɨt </ts>
               <ts e="T561" id="Seg_4350" n="e" s="T560">da </ts>
               <ts e="T562" id="Seg_4352" n="e" s="T561">kötön </ts>
               <ts e="T563" id="Seg_4354" n="e" s="T562">tüher. </ts>
               <ts e="T564" id="Seg_4356" n="e" s="T563">Ostoːlu </ts>
               <ts e="T565" id="Seg_4358" n="e" s="T564">tögürüččü </ts>
               <ts e="T566" id="Seg_4360" n="e" s="T565">tojon </ts>
               <ts e="T567" id="Seg_4362" n="e" s="T566">bögö. </ts>
               <ts e="T568" id="Seg_4364" n="e" s="T567">ɨraːktaːgɨ </ts>
               <ts e="T569" id="Seg_4366" n="e" s="T568">čaːj </ts>
               <ts e="T570" id="Seg_4368" n="e" s="T569">da </ts>
               <ts e="T571" id="Seg_4370" n="e" s="T570">iherpetek, </ts>
               <ts e="T572" id="Seg_4372" n="e" s="T571">kaːna </ts>
               <ts e="T573" id="Seg_4374" n="e" s="T572">kuhagan, </ts>
               <ts e="T574" id="Seg_4376" n="e" s="T573">čipčik </ts>
               <ts e="T575" id="Seg_4378" n="e" s="T574">atɨn. </ts>
               <ts e="T576" id="Seg_4380" n="e" s="T575">"Dʼe </ts>
               <ts e="T577" id="Seg_4382" n="e" s="T576">kepseː </ts>
               <ts e="T578" id="Seg_4384" n="e" s="T577">öskün. </ts>
               <ts e="T579" id="Seg_4386" n="e" s="T578">Munnʼak </ts>
               <ts e="T580" id="Seg_4388" n="e" s="T579">dʼonu </ts>
               <ts e="T581" id="Seg_4390" n="e" s="T580">togo </ts>
               <ts e="T582" id="Seg_4392" n="e" s="T581">kɨrtɨŋ?" </ts>
               <ts e="T583" id="Seg_4394" n="e" s="T582">"Tu͡ok </ts>
               <ts e="T584" id="Seg_4396" n="e" s="T583">munnʼagɨn </ts>
               <ts e="T585" id="Seg_4398" n="e" s="T584">haptaragɨn, </ts>
               <ts e="T586" id="Seg_4400" n="e" s="T585">kihini </ts>
               <ts e="T587" id="Seg_4402" n="e" s="T586">kɨtta </ts>
               <ts e="T588" id="Seg_4404" n="e" s="T587">mökkü͡önüm </ts>
               <ts e="T589" id="Seg_4406" n="e" s="T588">da </ts>
               <ts e="T590" id="Seg_4408" n="e" s="T589">hu͡ok. </ts>
               <ts e="T591" id="Seg_4410" n="e" s="T590">ɨraːktaːgɨ </ts>
               <ts e="T592" id="Seg_4412" n="e" s="T591">kineːster </ts>
               <ts e="T593" id="Seg_4414" n="e" s="T592">huruktarɨn </ts>
               <ts e="T594" id="Seg_4416" n="e" s="T593">kördörör: </ts>
               <ts e="T595" id="Seg_4418" n="e" s="T594">"Ehiːlgi </ts>
               <ts e="T596" id="Seg_4420" n="e" s="T595">uː </ts>
               <ts e="T597" id="Seg_4422" n="e" s="T596">keli͡er </ts>
               <ts e="T598" id="Seg_4424" n="e" s="T597">di͡eri </ts>
               <ts e="T599" id="Seg_4426" n="e" s="T598">kaːjɨːga </ts>
               <ts e="T600" id="Seg_4428" n="e" s="T599">hɨtɨ͡aŋ, </ts>
               <ts e="T601" id="Seg_4430" n="e" s="T600">miːginneːger </ts>
               <ts e="T602" id="Seg_4432" n="e" s="T601">ulakan </ts>
               <ts e="T603" id="Seg_4434" n="e" s="T602">ɨraːktaːgɨga </ts>
               <ts e="T604" id="Seg_4436" n="e" s="T603">ɨːtɨ͡am." </ts>
               <ts e="T605" id="Seg_4438" n="e" s="T604">Ogonnʼoru </ts>
               <ts e="T606" id="Seg_4440" n="e" s="T605">türmege </ts>
               <ts e="T607" id="Seg_4442" n="e" s="T606">kaːjan </ts>
               <ts e="T608" id="Seg_4444" n="e" s="T607">keːheller. </ts>
               <ts e="T609" id="Seg_4446" n="e" s="T608">Üs </ts>
               <ts e="T610" id="Seg_4448" n="e" s="T609">kineːs </ts>
               <ts e="T611" id="Seg_4450" n="e" s="T610">baːjɨn </ts>
               <ts e="T612" id="Seg_4452" n="e" s="T611">üllesti͡ektere. </ts>
               <ts e="T613" id="Seg_4454" n="e" s="T612">Emeːksin </ts>
               <ts e="T614" id="Seg_4456" n="e" s="T613">köhön </ts>
               <ts e="T615" id="Seg_4458" n="e" s="T614">keler, </ts>
               <ts e="T616" id="Seg_4460" n="e" s="T615">ogonnʼoro </ts>
               <ts e="T617" id="Seg_4462" n="e" s="T616">kaččaga </ts>
               <ts e="T618" id="Seg_4464" n="e" s="T617">da </ts>
               <ts e="T619" id="Seg_4466" n="e" s="T618">hu͡ok, </ts>
               <ts e="T620" id="Seg_4468" n="e" s="T619">türmege </ts>
               <ts e="T621" id="Seg_4470" n="e" s="T620">hɨtar. </ts>
               <ts e="T622" id="Seg_4472" n="e" s="T621">ɨraːktaːgɨ </ts>
               <ts e="T623" id="Seg_4474" n="e" s="T622">emeːksini </ts>
               <ts e="T624" id="Seg_4476" n="e" s="T623">üːren </ts>
               <ts e="T625" id="Seg_4478" n="e" s="T624">tahaːrar. </ts>
               <ts e="T626" id="Seg_4480" n="e" s="T625">Üs </ts>
               <ts e="T627" id="Seg_4482" n="e" s="T626">kineːs </ts>
               <ts e="T628" id="Seg_4484" n="e" s="T627">baːjɨn </ts>
               <ts e="T629" id="Seg_4486" n="e" s="T628">haba </ts>
               <ts e="T630" id="Seg_4488" n="e" s="T629">tühen </ts>
               <ts e="T631" id="Seg_4490" n="e" s="T630">üllesten </ts>
               <ts e="T632" id="Seg_4492" n="e" s="T631">ɨlallar, </ts>
               <ts e="T633" id="Seg_4494" n="e" s="T632">biːr </ts>
               <ts e="T634" id="Seg_4496" n="e" s="T633">öldʼüːnü </ts>
               <ts e="T635" id="Seg_4498" n="e" s="T634">kaːllarallar. </ts>
               <ts e="T636" id="Seg_4500" n="e" s="T635">Kɨhɨna </ts>
               <ts e="T637" id="Seg_4502" n="e" s="T636">baranan, </ts>
               <ts e="T638" id="Seg_4504" n="e" s="T637">poroku͡ot </ts>
               <ts e="T639" id="Seg_4506" n="e" s="T638">keleːkteːbit </ts>
               <ts e="T640" id="Seg_4508" n="e" s="T639">üːhetten. </ts>
               <ts e="T641" id="Seg_4510" n="e" s="T640">"Dʼe, </ts>
               <ts e="T642" id="Seg_4512" n="e" s="T641">kör </ts>
               <ts e="T643" id="Seg_4514" n="e" s="T642">ergin", </ts>
               <ts e="T644" id="Seg_4516" n="e" s="T643">diːller. </ts>
               <ts e="T645" id="Seg_4518" n="e" s="T644">Ogonnʼor </ts>
               <ts e="T646" id="Seg_4520" n="e" s="T645">karaga </ts>
               <ts e="T647" id="Seg_4522" n="e" s="T646">iːn </ts>
               <ts e="T648" id="Seg_4524" n="e" s="T647">ihiger </ts>
               <ts e="T649" id="Seg_4526" n="e" s="T648">kiːrbit, </ts>
               <ts e="T650" id="Seg_4528" n="e" s="T649">ahappataktar, </ts>
               <ts e="T651" id="Seg_4530" n="e" s="T650">okton </ts>
               <ts e="T652" id="Seg_4532" n="e" s="T651">kaːlbɨt. </ts>
               <ts e="T653" id="Seg_4534" n="e" s="T652">Poroku͡ot </ts>
               <ts e="T654" id="Seg_4536" n="e" s="T653">ihiger </ts>
               <ts e="T655" id="Seg_4538" n="e" s="T654">bɨrakpɨttar. </ts>
               <ts e="T656" id="Seg_4540" n="e" s="T655">Nöŋü͡ö </ts>
               <ts e="T657" id="Seg_4542" n="e" s="T656">ɨraːktaːgɨga </ts>
               <ts e="T658" id="Seg_4544" n="e" s="T657">illʼeller, </ts>
               <ts e="T659" id="Seg_4546" n="e" s="T658">nosilkannan </ts>
               <ts e="T660" id="Seg_4548" n="e" s="T659">killereller, </ts>
               <ts e="T661" id="Seg_4550" n="e" s="T660">ölön </ts>
               <ts e="T662" id="Seg_4552" n="e" s="T661">erer </ts>
               <ts e="T663" id="Seg_4554" n="e" s="T662">[kihini]. </ts>
               <ts e="T664" id="Seg_4556" n="e" s="T663">"Kaja, </ts>
               <ts e="T665" id="Seg_4558" n="e" s="T664">kajtak </ts>
               <ts e="T666" id="Seg_4560" n="e" s="T665">huːttu͡omuj </ts>
               <ts e="T667" id="Seg_4562" n="e" s="T666">ölbüt </ts>
               <ts e="T668" id="Seg_4564" n="e" s="T667">kihini? </ts>
               <ts e="T669" id="Seg_4566" n="e" s="T668">Ildʼiŋ </ts>
               <ts e="T670" id="Seg_4568" n="e" s="T669">bolʼnicaga, </ts>
               <ts e="T671" id="Seg_4570" n="e" s="T670">ahattɨnnar, </ts>
               <ts e="T672" id="Seg_4572" n="e" s="T671">kördünner." </ts>
               <ts e="T673" id="Seg_4574" n="e" s="T672">Du͡oktuːrdar </ts>
               <ts e="T674" id="Seg_4576" n="e" s="T673">emtiːller, </ts>
               <ts e="T675" id="Seg_4578" n="e" s="T674">erge </ts>
               <ts e="T676" id="Seg_4580" n="e" s="T675">bejete </ts>
               <ts e="T677" id="Seg_4582" n="e" s="T676">bu͡olar. </ts>
               <ts e="T678" id="Seg_4584" n="e" s="T677">Du͡oktuːrdar </ts>
               <ts e="T679" id="Seg_4586" n="e" s="T678">ɨjɨtallar: </ts>
               <ts e="T680" id="Seg_4588" n="e" s="T679">"Burujuŋ </ts>
               <ts e="T681" id="Seg_4590" n="e" s="T680">duː, </ts>
               <ts e="T682" id="Seg_4592" n="e" s="T681">kütürüːller </ts>
               <ts e="T683" id="Seg_4594" n="e" s="T682">duː?" </ts>
               <ts e="T684" id="Seg_4596" n="e" s="T683">Ogonnʼor </ts>
               <ts e="T685" id="Seg_4598" n="e" s="T684">kepsiːr </ts>
               <ts e="T686" id="Seg_4600" n="e" s="T685">barɨtɨn. </ts>
               <ts e="T687" id="Seg_4602" n="e" s="T686">Du͡oktuːrdar </ts>
               <ts e="T688" id="Seg_4604" n="e" s="T687">kaːn </ts>
               <ts e="T689" id="Seg_4606" n="e" s="T688">ɨlallar: </ts>
               <ts e="T690" id="Seg_4608" n="e" s="T689">"Burujdaːk </ts>
               <ts e="T691" id="Seg_4610" n="e" s="T690">bu͡ollakkɨna, </ts>
               <ts e="T692" id="Seg_4612" n="e" s="T691">kaːnɨŋ </ts>
               <ts e="T693" id="Seg_4614" n="e" s="T692">atɨn </ts>
               <ts e="T694" id="Seg_4616" n="e" s="T693">bu͡olu͡o", </ts>
               <ts e="T695" id="Seg_4618" n="e" s="T694">diːller. </ts>
               <ts e="T696" id="Seg_4620" n="e" s="T695">Biːrkeːn </ts>
               <ts e="T697" id="Seg_4622" n="e" s="T696">da </ts>
               <ts e="T698" id="Seg_4624" n="e" s="T697">kaːpelʼka </ts>
               <ts e="T699" id="Seg_4626" n="e" s="T698">buruja </ts>
               <ts e="T700" id="Seg_4628" n="e" s="T699">hu͡ok </ts>
               <ts e="T701" id="Seg_4630" n="e" s="T700">ebit. </ts>
               <ts e="T702" id="Seg_4632" n="e" s="T701">Huruk </ts>
               <ts e="T703" id="Seg_4634" n="e" s="T702">bi͡ereller: </ts>
               <ts e="T704" id="Seg_4636" n="e" s="T703">"Manɨ </ts>
               <ts e="T705" id="Seg_4638" n="e" s="T704">huːkka </ts>
               <ts e="T706" id="Seg_4640" n="e" s="T705">pireːme </ts>
               <ts e="T707" id="Seg_4642" n="e" s="T706">bi͡ereːr. </ts>
               <ts e="T708" id="Seg_4644" n="e" s="T707">Kineːster </ts>
               <ts e="T709" id="Seg_4646" n="e" s="T708">kaːnnarɨn </ts>
               <ts e="T710" id="Seg_4648" n="e" s="T709">emi͡e </ts>
               <ts e="T711" id="Seg_4650" n="e" s="T710">ɨlɨ͡akpɨt", </ts>
               <ts e="T712" id="Seg_4652" n="e" s="T711">diːller. </ts>
               <ts e="T713" id="Seg_4654" n="e" s="T712">Huːt </ts>
               <ts e="T714" id="Seg_4656" n="e" s="T713">bu͡olar. </ts>
               <ts e="T715" id="Seg_4658" n="e" s="T714">Kineːster, </ts>
               <ts e="T716" id="Seg_4660" n="e" s="T715">maŋnajgɨ </ts>
               <ts e="T717" id="Seg_4662" n="e" s="T716">ɨraːktaːgɨ, </ts>
               <ts e="T718" id="Seg_4664" n="e" s="T717">elbek </ts>
               <ts e="T719" id="Seg_4666" n="e" s="T718">dʼon </ts>
               <ts e="T720" id="Seg_4668" n="e" s="T719">munnʼustubut. </ts>
               <ts e="T721" id="Seg_4670" n="e" s="T720">ɨraːktaːgɨ </ts>
               <ts e="T722" id="Seg_4672" n="e" s="T721">eter: </ts>
               <ts e="T723" id="Seg_4674" n="e" s="T722">"Öhün-tɨlɨn </ts>
               <ts e="T724" id="Seg_4676" n="e" s="T723">isti͡ekke", </ts>
               <ts e="T725" id="Seg_4678" n="e" s="T724">diːr. </ts>
               <ts e="T726" id="Seg_4680" n="e" s="T725">Ogonnʼor </ts>
               <ts e="T727" id="Seg_4682" n="e" s="T726">kepsiːr: </ts>
               <ts e="T728" id="Seg_4684" n="e" s="T727">"Min </ts>
               <ts e="T729" id="Seg_4686" n="e" s="T728">daːse </ts>
               <ts e="T730" id="Seg_4688" n="e" s="T729">kihini </ts>
               <ts e="T731" id="Seg_4690" n="e" s="T730">kɨtta </ts>
               <ts e="T732" id="Seg_4692" n="e" s="T731">ölössübetek </ts>
               <ts e="T733" id="Seg_4694" n="e" s="T732">kihibin." </ts>
               <ts e="T734" id="Seg_4696" n="e" s="T733">ɨsparaːpkatɨn </ts>
               <ts e="T735" id="Seg_4698" n="e" s="T734">bi͡erer </ts>
               <ts e="T736" id="Seg_4700" n="e" s="T735">"bu </ts>
               <ts e="T737" id="Seg_4702" n="e" s="T736">kihi </ts>
               <ts e="T738" id="Seg_4704" n="e" s="T737">kaːpelʼka </ts>
               <ts e="T739" id="Seg_4706" n="e" s="T738">da </ts>
               <ts e="T740" id="Seg_4708" n="e" s="T739">buruja </ts>
               <ts e="T741" id="Seg_4710" n="e" s="T740">hu͡ok". </ts>
               <ts e="T742" id="Seg_4712" n="e" s="T741">Kineːster </ts>
               <ts e="T743" id="Seg_4714" n="e" s="T742">kaːnnarɨn </ts>
               <ts e="T744" id="Seg_4716" n="e" s="T743">ɨlallar. </ts>
               <ts e="T745" id="Seg_4718" n="e" s="T744">Haːmaj </ts>
               <ts e="T746" id="Seg_4720" n="e" s="T745">kohuːnun </ts>
               <ts e="T747" id="Seg_4722" n="e" s="T746">kaːna </ts>
               <ts e="T748" id="Seg_4724" n="e" s="T747">ile </ts>
               <ts e="T749" id="Seg_4726" n="e" s="T748">atɨn, </ts>
               <ts e="T750" id="Seg_4728" n="e" s="T749">kineːster </ts>
               <ts e="T751" id="Seg_4730" n="e" s="T750">gi͡ettere </ts>
               <ts e="T752" id="Seg_4732" n="e" s="T751">čiŋke </ts>
               <ts e="T753" id="Seg_4734" n="e" s="T752">atɨn. </ts>
               <ts e="T754" id="Seg_4736" n="e" s="T753">ɨraːktaːgɨ </ts>
               <ts e="T755" id="Seg_4738" n="e" s="T754">dʼe </ts>
               <ts e="T756" id="Seg_4740" n="e" s="T755">ɨgajar: </ts>
               <ts e="T757" id="Seg_4742" n="e" s="T756">"Kim </ts>
               <ts e="T758" id="Seg_4744" n="e" s="T757">hübeleːbitej? </ts>
               <ts e="T759" id="Seg_4746" n="e" s="T758">Kepseːŋ!" </ts>
               <ts e="T760" id="Seg_4748" n="e" s="T759">Tɨ͡a </ts>
               <ts e="T761" id="Seg_4750" n="e" s="T760">kineːhe </ts>
               <ts e="T762" id="Seg_4752" n="e" s="T761">kepsiːr: </ts>
               <ts e="T763" id="Seg_4754" n="e" s="T762">"Dʼe </ts>
               <ts e="T764" id="Seg_4756" n="e" s="T763">kirdige, </ts>
               <ts e="T765" id="Seg_4758" n="e" s="T764">bihigi </ts>
               <ts e="T766" id="Seg_4760" n="e" s="T765">burujdaːkpɨt. </ts>
               <ts e="T767" id="Seg_4762" n="e" s="T766">Haːmaj </ts>
               <ts e="T768" id="Seg_4764" n="e" s="T767">ulakan </ts>
               <ts e="T769" id="Seg_4766" n="e" s="T768">burujdaːk— </ts>
               <ts e="T770" id="Seg_4768" n="e" s="T769">dʼuraːk </ts>
               <ts e="T771" id="Seg_4770" n="e" s="T770">kineːhe. </ts>
               <ts e="T772" id="Seg_4772" n="e" s="T771">Minʼigin </ts>
               <ts e="T773" id="Seg_4774" n="e" s="T772">keji͡ek </ts>
               <ts e="T774" id="Seg_4776" n="e" s="T773">baːllara, </ts>
               <ts e="T775" id="Seg_4778" n="e" s="T774">kuttanan </ts>
               <ts e="T776" id="Seg_4780" n="e" s="T775">podpisʼ </ts>
               <ts e="T777" id="Seg_4782" n="e" s="T776">bi͡erbitim. </ts>
               <ts e="T778" id="Seg_4784" n="e" s="T777">ɨraːktaːgɨ: </ts>
               <ts e="T779" id="Seg_4786" n="e" s="T778">"Dʼe, </ts>
               <ts e="T780" id="Seg_4788" n="e" s="T779">ogonnʼor, </ts>
               <ts e="T781" id="Seg_4790" n="e" s="T780">en </ts>
               <ts e="T782" id="Seg_4792" n="e" s="T781">burujun </ts>
               <ts e="T783" id="Seg_4794" n="e" s="T782">hu͡ok </ts>
               <ts e="T784" id="Seg_4796" n="e" s="T783">ebit. </ts>
               <ts e="T785" id="Seg_4798" n="e" s="T784">Kaːjɨːga </ts>
               <ts e="T786" id="Seg_4800" n="e" s="T785">ejigin </ts>
               <ts e="T787" id="Seg_4802" n="e" s="T786">huːttaːbɨt </ts>
               <ts e="T788" id="Seg_4804" n="e" s="T787">ɨraːktaːgɨnɨ </ts>
               <ts e="T789" id="Seg_4806" n="e" s="T788">olordu͡om. </ts>
               <ts e="T790" id="Seg_4808" n="e" s="T789">En </ts>
               <ts e="T791" id="Seg_4810" n="e" s="T790">gini </ts>
               <ts e="T792" id="Seg_4812" n="e" s="T791">onnugar </ts>
               <ts e="T793" id="Seg_4814" n="e" s="T792">ɨraːktaːgɨnnan </ts>
               <ts e="T794" id="Seg_4816" n="e" s="T793">barɨ͡aŋ, </ts>
               <ts e="T795" id="Seg_4818" n="e" s="T794">kineːsteri </ts>
               <ts e="T796" id="Seg_4820" n="e" s="T795">onno </ts>
               <ts e="T797" id="Seg_4822" n="e" s="T796">tijen </ts>
               <ts e="T798" id="Seg_4824" n="e" s="T797">huːttaːr", </ts>
               <ts e="T799" id="Seg_4826" n="e" s="T798">di͡ebit. </ts>
               <ts e="T800" id="Seg_4828" n="e" s="T799">Kamnaːn </ts>
               <ts e="T801" id="Seg_4830" n="e" s="T800">kaːlallar </ts>
               <ts e="T802" id="Seg_4832" n="e" s="T801">töttörü. </ts>
               <ts e="T803" id="Seg_4834" n="e" s="T802">Ogonnʼor </ts>
               <ts e="T804" id="Seg_4836" n="e" s="T803">ologugar </ts>
               <ts e="T805" id="Seg_4838" n="e" s="T804">kelen </ts>
               <ts e="T806" id="Seg_4840" n="e" s="T805">kineːs </ts>
               <ts e="T807" id="Seg_4842" n="e" s="T806">bu͡olar. </ts>
               <ts e="T808" id="Seg_4844" n="e" s="T807">U͡ola </ts>
               <ts e="T809" id="Seg_4846" n="e" s="T808">ulaːtan </ts>
               <ts e="T810" id="Seg_4848" n="e" s="T809">pireːme-pireːme </ts>
               <ts e="T811" id="Seg_4850" n="e" s="T810">bukatɨːr </ts>
               <ts e="T812" id="Seg_4852" n="e" s="T811">bu͡olbut. </ts>
               <ts e="T813" id="Seg_4854" n="e" s="T812">Kini </ts>
               <ts e="T814" id="Seg_4856" n="e" s="T813">hu͡oguna </ts>
               <ts e="T815" id="Seg_4858" n="e" s="T814">ölöröːrü </ts>
               <ts e="T816" id="Seg_4860" n="e" s="T815">gɨmmɨttar, </ts>
               <ts e="T817" id="Seg_4862" n="e" s="T816">agatɨgar </ts>
               <ts e="T818" id="Seg_4864" n="e" s="T817">talɨ </ts>
               <ts e="T819" id="Seg_4866" n="e" s="T818">bu͡olu͡o </ts>
               <ts e="T820" id="Seg_4868" n="e" s="T819">di͡en. </ts>
               <ts e="T821" id="Seg_4870" n="e" s="T820">U͡ol </ts>
               <ts e="T822" id="Seg_4872" n="e" s="T821">biːr </ts>
               <ts e="T823" id="Seg_4874" n="e" s="T822">kihini </ts>
               <ts e="T824" id="Seg_4876" n="e" s="T823">tüŋneri </ts>
               <ts e="T825" id="Seg_4878" n="e" s="T824">oksubut, </ts>
               <ts e="T826" id="Seg_4880" n="e" s="T825">onu </ts>
               <ts e="T827" id="Seg_4882" n="e" s="T826">kaːjɨːga </ts>
               <ts e="T828" id="Seg_4884" n="e" s="T827">olorputtar. </ts>
               <ts e="T829" id="Seg_4886" n="e" s="T828">Kineːsterin </ts>
               <ts e="T830" id="Seg_4888" n="e" s="T829">dʼüːllüːr. </ts>
               <ts e="T831" id="Seg_4890" n="e" s="T830">Haːmaj </ts>
               <ts e="T832" id="Seg_4892" n="e" s="T831">kineːhin, </ts>
               <ts e="T833" id="Seg_4894" n="e" s="T832">dʼuraːk </ts>
               <ts e="T834" id="Seg_4896" n="e" s="T833">kineːhiniːn </ts>
               <ts e="T835" id="Seg_4898" n="e" s="T834">türmege </ts>
               <ts e="T836" id="Seg_4900" n="e" s="T835">olordor. </ts>
               <ts e="T837" id="Seg_4902" n="e" s="T836">Tɨ͡a </ts>
               <ts e="T838" id="Seg_4904" n="e" s="T837">kineːhin </ts>
               <ts e="T839" id="Seg_4906" n="e" s="T838">kineːhitten </ts>
               <ts e="T840" id="Seg_4908" n="e" s="T839">uhular. </ts>
               <ts e="T841" id="Seg_4910" n="e" s="T840">U͡olun </ts>
               <ts e="T842" id="Seg_4912" n="e" s="T841">haːmaj </ts>
               <ts e="T843" id="Seg_4914" n="e" s="T842">kineːhe </ts>
               <ts e="T844" id="Seg_4916" n="e" s="T843">oŋoror. </ts>
               <ts e="T845" id="Seg_4918" n="e" s="T844">Ogonnʼor </ts>
               <ts e="T846" id="Seg_4920" n="e" s="T845">itinnik </ts>
               <ts e="T847" id="Seg_4922" n="e" s="T846">kirdige </ts>
               <ts e="T848" id="Seg_4924" n="e" s="T847">taksɨbɨta. </ts>
               <ts e="T849" id="Seg_4926" n="e" s="T848">Elete. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_4927" s="T0">SaSS_1964_NganasanBraveBoy_flk.001 (001.001)</ta>
            <ta e="T10" id="Seg_4928" s="T4">SaSS_1964_NganasanBraveBoy_flk.002 (001.002)</ta>
            <ta e="T14" id="Seg_4929" s="T10">SaSS_1964_NganasanBraveBoy_flk.003 (001.003)</ta>
            <ta e="T18" id="Seg_4930" s="T14">SaSS_1964_NganasanBraveBoy_flk.004 (001.004)</ta>
            <ta e="T26" id="Seg_4931" s="T18">SaSS_1964_NganasanBraveBoy_flk.005 (001.005)</ta>
            <ta e="T29" id="Seg_4932" s="T26">SaSS_1964_NganasanBraveBoy_flk.006 (001.006)</ta>
            <ta e="T32" id="Seg_4933" s="T29">SaSS_1964_NganasanBraveBoy_flk.007 (001.007)</ta>
            <ta e="T34" id="Seg_4934" s="T32">SaSS_1964_NganasanBraveBoy_flk.008 (001.008)</ta>
            <ta e="T37" id="Seg_4935" s="T34">SaSS_1964_NganasanBraveBoy_flk.009 (001.008)</ta>
            <ta e="T41" id="Seg_4936" s="T37">SaSS_1964_NganasanBraveBoy_flk.010 (001.009)</ta>
            <ta e="T44" id="Seg_4937" s="T41">SaSS_1964_NganasanBraveBoy_flk.011 (001.010)</ta>
            <ta e="T50" id="Seg_4938" s="T44">SaSS_1964_NganasanBraveBoy_flk.012 (001.011)</ta>
            <ta e="T54" id="Seg_4939" s="T50">SaSS_1964_NganasanBraveBoy_flk.013 (001.012)</ta>
            <ta e="T63" id="Seg_4940" s="T54">SaSS_1964_NganasanBraveBoy_flk.014 (001.013)</ta>
            <ta e="T70" id="Seg_4941" s="T63">SaSS_1964_NganasanBraveBoy_flk.015 (001.014)</ta>
            <ta e="T81" id="Seg_4942" s="T70">SaSS_1964_NganasanBraveBoy_flk.016 (001.015)</ta>
            <ta e="T84" id="Seg_4943" s="T81">SaSS_1964_NganasanBraveBoy_flk.017 (001.016)</ta>
            <ta e="T88" id="Seg_4944" s="T84">SaSS_1964_NganasanBraveBoy_flk.018 (001.017)</ta>
            <ta e="T89" id="Seg_4945" s="T88">SaSS_1964_NganasanBraveBoy_flk.019 (001.018)</ta>
            <ta e="T93" id="Seg_4946" s="T89">SaSS_1964_NganasanBraveBoy_flk.020 (001.019)</ta>
            <ta e="T97" id="Seg_4947" s="T93">SaSS_1964_NganasanBraveBoy_flk.021 (001.020)</ta>
            <ta e="T106" id="Seg_4948" s="T97">SaSS_1964_NganasanBraveBoy_flk.022 (001.021)</ta>
            <ta e="T117" id="Seg_4949" s="T106">SaSS_1964_NganasanBraveBoy_flk.023 (001.022)</ta>
            <ta e="T120" id="Seg_4950" s="T117">SaSS_1964_NganasanBraveBoy_flk.024 (001.023)</ta>
            <ta e="T123" id="Seg_4951" s="T120">SaSS_1964_NganasanBraveBoy_flk.025 (001.024)</ta>
            <ta e="T124" id="Seg_4952" s="T123">SaSS_1964_NganasanBraveBoy_flk.026 (001.025)</ta>
            <ta e="T131" id="Seg_4953" s="T124">SaSS_1964_NganasanBraveBoy_flk.027 (001.026)</ta>
            <ta e="T137" id="Seg_4954" s="T131">SaSS_1964_NganasanBraveBoy_flk.028 (001.027)</ta>
            <ta e="T143" id="Seg_4955" s="T137">SaSS_1964_NganasanBraveBoy_flk.029 (001.028)</ta>
            <ta e="T152" id="Seg_4956" s="T143">SaSS_1964_NganasanBraveBoy_flk.030 (001.028)</ta>
            <ta e="T156" id="Seg_4957" s="T152">SaSS_1964_NganasanBraveBoy_flk.031 (001.029)</ta>
            <ta e="T165" id="Seg_4958" s="T156">SaSS_1964_NganasanBraveBoy_flk.032 (001.030)</ta>
            <ta e="T170" id="Seg_4959" s="T165">SaSS_1964_NganasanBraveBoy_flk.033 (001.031)</ta>
            <ta e="T173" id="Seg_4960" s="T170">SaSS_1964_NganasanBraveBoy_flk.034 (001.032)</ta>
            <ta e="T178" id="Seg_4961" s="T173">SaSS_1964_NganasanBraveBoy_flk.035 (001.033)</ta>
            <ta e="T188" id="Seg_4962" s="T178">SaSS_1964_NganasanBraveBoy_flk.036 (001.034)</ta>
            <ta e="T190" id="Seg_4963" s="T188">SaSS_1964_NganasanBraveBoy_flk.037 (001.035)</ta>
            <ta e="T195" id="Seg_4964" s="T190">SaSS_1964_NganasanBraveBoy_flk.038 (001.036)</ta>
            <ta e="T198" id="Seg_4965" s="T195">SaSS_1964_NganasanBraveBoy_flk.039 (001.038)</ta>
            <ta e="T206" id="Seg_4966" s="T198">SaSS_1964_NganasanBraveBoy_flk.040 (001.039)</ta>
            <ta e="T211" id="Seg_4967" s="T206">SaSS_1964_NganasanBraveBoy_flk.041 (001.041)</ta>
            <ta e="T213" id="Seg_4968" s="T211">SaSS_1964_NganasanBraveBoy_flk.042 (001.043)</ta>
            <ta e="T218" id="Seg_4969" s="T213">SaSS_1964_NganasanBraveBoy_flk.043 (001.044)</ta>
            <ta e="T224" id="Seg_4970" s="T218">SaSS_1964_NganasanBraveBoy_flk.044 (001.045)</ta>
            <ta e="T230" id="Seg_4971" s="T224">SaSS_1964_NganasanBraveBoy_flk.045 (001.046)</ta>
            <ta e="T232" id="Seg_4972" s="T230">SaSS_1964_NganasanBraveBoy_flk.046 (001.047)</ta>
            <ta e="T238" id="Seg_4973" s="T232">SaSS_1964_NganasanBraveBoy_flk.047 (001.048)</ta>
            <ta e="T241" id="Seg_4974" s="T238">SaSS_1964_NganasanBraveBoy_flk.048 (001.049)</ta>
            <ta e="T243" id="Seg_4975" s="T241">SaSS_1964_NganasanBraveBoy_flk.049 (001.050)</ta>
            <ta e="T247" id="Seg_4976" s="T243">SaSS_1964_NganasanBraveBoy_flk.050 (001.051)</ta>
            <ta e="T252" id="Seg_4977" s="T247">SaSS_1964_NganasanBraveBoy_flk.051 (001.052)</ta>
            <ta e="T257" id="Seg_4978" s="T252">SaSS_1964_NganasanBraveBoy_flk.052 (001.052)</ta>
            <ta e="T262" id="Seg_4979" s="T257">SaSS_1964_NganasanBraveBoy_flk.053 (001.053)</ta>
            <ta e="T264" id="Seg_4980" s="T262">SaSS_1964_NganasanBraveBoy_flk.054 (001.054)</ta>
            <ta e="T268" id="Seg_4981" s="T264">SaSS_1964_NganasanBraveBoy_flk.055 (001.055)</ta>
            <ta e="T272" id="Seg_4982" s="T268">SaSS_1964_NganasanBraveBoy_flk.056 (001.056)</ta>
            <ta e="T275" id="Seg_4983" s="T272">SaSS_1964_NganasanBraveBoy_flk.057 (001.057)</ta>
            <ta e="T280" id="Seg_4984" s="T275">SaSS_1964_NganasanBraveBoy_flk.058 (001.058)</ta>
            <ta e="T290" id="Seg_4985" s="T280">SaSS_1964_NganasanBraveBoy_flk.059 (001.059)</ta>
            <ta e="T296" id="Seg_4986" s="T290">SaSS_1964_NganasanBraveBoy_flk.060 (001.060)</ta>
            <ta e="T301" id="Seg_4987" s="T296">SaSS_1964_NganasanBraveBoy_flk.061 (001.061)</ta>
            <ta e="T303" id="Seg_4988" s="T301">SaSS_1964_NganasanBraveBoy_flk.062 (001.062)</ta>
            <ta e="T307" id="Seg_4989" s="T303">SaSS_1964_NganasanBraveBoy_flk.063 (001.063)</ta>
            <ta e="T310" id="Seg_4990" s="T307">SaSS_1964_NganasanBraveBoy_flk.064 (001.064)</ta>
            <ta e="T312" id="Seg_4991" s="T310">SaSS_1964_NganasanBraveBoy_flk.065 (001.065)</ta>
            <ta e="T316" id="Seg_4992" s="T312">SaSS_1964_NganasanBraveBoy_flk.066 (001.066)</ta>
            <ta e="T329" id="Seg_4993" s="T316">SaSS_1964_NganasanBraveBoy_flk.067 (001.067)</ta>
            <ta e="T346" id="Seg_4994" s="T329">SaSS_1964_NganasanBraveBoy_flk.068 (001.068)</ta>
            <ta e="T353" id="Seg_4995" s="T346">SaSS_1964_NganasanBraveBoy_flk.069 (001.069)</ta>
            <ta e="T362" id="Seg_4996" s="T353">SaSS_1964_NganasanBraveBoy_flk.070 (001.071)</ta>
            <ta e="T364" id="Seg_4997" s="T362">SaSS_1964_NganasanBraveBoy_flk.071 (001.072)</ta>
            <ta e="T371" id="Seg_4998" s="T364">SaSS_1964_NganasanBraveBoy_flk.072 (001.072)</ta>
            <ta e="T374" id="Seg_4999" s="T371">SaSS_1964_NganasanBraveBoy_flk.073 (001.073)</ta>
            <ta e="T377" id="Seg_5000" s="T374">SaSS_1964_NganasanBraveBoy_flk.074 (001.074)</ta>
            <ta e="T383" id="Seg_5001" s="T377">SaSS_1964_NganasanBraveBoy_flk.075 (001.075)</ta>
            <ta e="T390" id="Seg_5002" s="T383">SaSS_1964_NganasanBraveBoy_flk.076 (001.076)</ta>
            <ta e="T394" id="Seg_5003" s="T390">SaSS_1964_NganasanBraveBoy_flk.077 (001.077)</ta>
            <ta e="T397" id="Seg_5004" s="T394">SaSS_1964_NganasanBraveBoy_flk.078 (001.078)</ta>
            <ta e="T405" id="Seg_5005" s="T397">SaSS_1964_NganasanBraveBoy_flk.079 (001.078)</ta>
            <ta e="T408" id="Seg_5006" s="T405">SaSS_1964_NganasanBraveBoy_flk.080 (001.079)</ta>
            <ta e="T413" id="Seg_5007" s="T408">SaSS_1964_NganasanBraveBoy_flk.081 (001.080)</ta>
            <ta e="T417" id="Seg_5008" s="T413">SaSS_1964_NganasanBraveBoy_flk.082 (001.081)</ta>
            <ta e="T419" id="Seg_5009" s="T417">SaSS_1964_NganasanBraveBoy_flk.083 (001.081)</ta>
            <ta e="T427" id="Seg_5010" s="T419">SaSS_1964_NganasanBraveBoy_flk.084 (001.082)</ta>
            <ta e="T434" id="Seg_5011" s="T427">SaSS_1964_NganasanBraveBoy_flk.085 (001.083)</ta>
            <ta e="T436" id="Seg_5012" s="T434">SaSS_1964_NganasanBraveBoy_flk.086 (001.084)</ta>
            <ta e="T441" id="Seg_5013" s="T436">SaSS_1964_NganasanBraveBoy_flk.087 (001.084)</ta>
            <ta e="T445" id="Seg_5014" s="T441">SaSS_1964_NganasanBraveBoy_flk.088 (001.085)</ta>
            <ta e="T448" id="Seg_5015" s="T445">SaSS_1964_NganasanBraveBoy_flk.089 (001.086)</ta>
            <ta e="T451" id="Seg_5016" s="T448">SaSS_1964_NganasanBraveBoy_flk.090 (001.087)</ta>
            <ta e="T454" id="Seg_5017" s="T451">SaSS_1964_NganasanBraveBoy_flk.091 (001.088)</ta>
            <ta e="T458" id="Seg_5018" s="T454">SaSS_1964_NganasanBraveBoy_flk.092 (001.089)</ta>
            <ta e="T460" id="Seg_5019" s="T458">SaSS_1964_NganasanBraveBoy_flk.093 (001.091)</ta>
            <ta e="T464" id="Seg_5020" s="T460">SaSS_1964_NganasanBraveBoy_flk.094 (001.091)</ta>
            <ta e="T476" id="Seg_5021" s="T464">SaSS_1964_NganasanBraveBoy_flk.095 (001.092)</ta>
            <ta e="T487" id="Seg_5022" s="T476">SaSS_1964_NganasanBraveBoy_flk.096 (001.093)</ta>
            <ta e="T490" id="Seg_5023" s="T487">SaSS_1964_NganasanBraveBoy_flk.097 (001.094)</ta>
            <ta e="T497" id="Seg_5024" s="T490">SaSS_1964_NganasanBraveBoy_flk.098 (001.095)</ta>
            <ta e="T500" id="Seg_5025" s="T497">SaSS_1964_NganasanBraveBoy_flk.099 (001.096)</ta>
            <ta e="T505" id="Seg_5026" s="T500">SaSS_1964_NganasanBraveBoy_flk.100 (001.097)</ta>
            <ta e="T515" id="Seg_5027" s="T505">SaSS_1964_NganasanBraveBoy_flk.101 (001.098)</ta>
            <ta e="T518" id="Seg_5028" s="T515">SaSS_1964_NganasanBraveBoy_flk.102 (001.099)</ta>
            <ta e="T520" id="Seg_5029" s="T518">SaSS_1964_NganasanBraveBoy_flk.103 (001.099)</ta>
            <ta e="T525" id="Seg_5030" s="T520">SaSS_1964_NganasanBraveBoy_flk.104 (001.100)</ta>
            <ta e="T533" id="Seg_5031" s="T525">SaSS_1964_NganasanBraveBoy_flk.105 (001.101)</ta>
            <ta e="T537" id="Seg_5032" s="T533">SaSS_1964_NganasanBraveBoy_flk.106 (001.102)</ta>
            <ta e="T548" id="Seg_5033" s="T537">SaSS_1964_NganasanBraveBoy_flk.107 (001.103)</ta>
            <ta e="T552" id="Seg_5034" s="T548">SaSS_1964_NganasanBraveBoy_flk.108 (001.104)</ta>
            <ta e="T558" id="Seg_5035" s="T552">SaSS_1964_NganasanBraveBoy_flk.109 (001.105)</ta>
            <ta e="T563" id="Seg_5036" s="T558">SaSS_1964_NganasanBraveBoy_flk.110 (001.106)</ta>
            <ta e="T567" id="Seg_5037" s="T563">SaSS_1964_NganasanBraveBoy_flk.111 (001.107)</ta>
            <ta e="T575" id="Seg_5038" s="T567">SaSS_1964_NganasanBraveBoy_flk.112 (001.108)</ta>
            <ta e="T578" id="Seg_5039" s="T575">SaSS_1964_NganasanBraveBoy_flk.113 (001.109)</ta>
            <ta e="T582" id="Seg_5040" s="T578">SaSS_1964_NganasanBraveBoy_flk.114 (001.110)</ta>
            <ta e="T590" id="Seg_5041" s="T582">SaSS_1964_NganasanBraveBoy_flk.115 (001.111)</ta>
            <ta e="T594" id="Seg_5042" s="T590">SaSS_1964_NganasanBraveBoy_flk.116 (001.112)</ta>
            <ta e="T604" id="Seg_5043" s="T594">SaSS_1964_NganasanBraveBoy_flk.117 (001.112)</ta>
            <ta e="T608" id="Seg_5044" s="T604">SaSS_1964_NganasanBraveBoy_flk.118 (001.113)</ta>
            <ta e="T612" id="Seg_5045" s="T608">SaSS_1964_NganasanBraveBoy_flk.119 (001.114)</ta>
            <ta e="T621" id="Seg_5046" s="T612">SaSS_1964_NganasanBraveBoy_flk.120 (001.115)</ta>
            <ta e="T625" id="Seg_5047" s="T621">SaSS_1964_NganasanBraveBoy_flk.121 (001.116)</ta>
            <ta e="T635" id="Seg_5048" s="T625">SaSS_1964_NganasanBraveBoy_flk.122 (001.117)</ta>
            <ta e="T640" id="Seg_5049" s="T635">SaSS_1964_NganasanBraveBoy_flk.123 (001.118)</ta>
            <ta e="T644" id="Seg_5050" s="T640">SaSS_1964_NganasanBraveBoy_flk.124 (001.119)</ta>
            <ta e="T652" id="Seg_5051" s="T644">SaSS_1964_NganasanBraveBoy_flk.125 (001.120)</ta>
            <ta e="T655" id="Seg_5052" s="T652">SaSS_1964_NganasanBraveBoy_flk.126 (001.121)</ta>
            <ta e="T663" id="Seg_5053" s="T655">SaSS_1964_NganasanBraveBoy_flk.127 (001.122)</ta>
            <ta e="T668" id="Seg_5054" s="T663">SaSS_1964_NganasanBraveBoy_flk.128 (001.123)</ta>
            <ta e="T672" id="Seg_5055" s="T668">SaSS_1964_NganasanBraveBoy_flk.129 (001.124)</ta>
            <ta e="T677" id="Seg_5056" s="T672">SaSS_1964_NganasanBraveBoy_flk.130 (001.125)</ta>
            <ta e="T679" id="Seg_5057" s="T677">SaSS_1964_NganasanBraveBoy_flk.131 (001.126)</ta>
            <ta e="T683" id="Seg_5058" s="T679">SaSS_1964_NganasanBraveBoy_flk.132 (001.126)</ta>
            <ta e="T686" id="Seg_5059" s="T683">SaSS_1964_NganasanBraveBoy_flk.133 (001.127)</ta>
            <ta e="T689" id="Seg_5060" s="T686">SaSS_1964_NganasanBraveBoy_flk.134 (001.128)</ta>
            <ta e="T695" id="Seg_5061" s="T689">SaSS_1964_NganasanBraveBoy_flk.135 (001.128)</ta>
            <ta e="T701" id="Seg_5062" s="T695">SaSS_1964_NganasanBraveBoy_flk.136 (001.129)</ta>
            <ta e="T703" id="Seg_5063" s="T701">SaSS_1964_NganasanBraveBoy_flk.137 (001.130)</ta>
            <ta e="T707" id="Seg_5064" s="T703">SaSS_1964_NganasanBraveBoy_flk.138 (001.130)</ta>
            <ta e="T712" id="Seg_5065" s="T707">SaSS_1964_NganasanBraveBoy_flk.139 (001.131)</ta>
            <ta e="T714" id="Seg_5066" s="T712">SaSS_1964_NganasanBraveBoy_flk.140 (001.132)</ta>
            <ta e="T720" id="Seg_5067" s="T714">SaSS_1964_NganasanBraveBoy_flk.141 (001.133)</ta>
            <ta e="T722" id="Seg_5068" s="T720">SaSS_1964_NganasanBraveBoy_flk.142 (001.134)</ta>
            <ta e="T725" id="Seg_5069" s="T722">SaSS_1964_NganasanBraveBoy_flk.143 (001.134)</ta>
            <ta e="T727" id="Seg_5070" s="T725">SaSS_1964_NganasanBraveBoy_flk.144 (001.135)</ta>
            <ta e="T733" id="Seg_5071" s="T727">SaSS_1964_NganasanBraveBoy_flk.145 (001.135)</ta>
            <ta e="T741" id="Seg_5072" s="T733">SaSS_1964_NganasanBraveBoy_flk.146 (001.135)</ta>
            <ta e="T744" id="Seg_5073" s="T741">SaSS_1964_NganasanBraveBoy_flk.147 (001.136)</ta>
            <ta e="T753" id="Seg_5074" s="T744">SaSS_1964_NganasanBraveBoy_flk.148 (001.137)</ta>
            <ta e="T756" id="Seg_5075" s="T753">SaSS_1964_NganasanBraveBoy_flk.149 (001.138)</ta>
            <ta e="T758" id="Seg_5076" s="T756">SaSS_1964_NganasanBraveBoy_flk.150 (001.138)</ta>
            <ta e="T759" id="Seg_5077" s="T758">SaSS_1964_NganasanBraveBoy_flk.151 (001.139)</ta>
            <ta e="T762" id="Seg_5078" s="T759">SaSS_1964_NganasanBraveBoy_flk.152 (001.140)</ta>
            <ta e="T766" id="Seg_5079" s="T762">SaSS_1964_NganasanBraveBoy_flk.153 (001.140)</ta>
            <ta e="T771" id="Seg_5080" s="T766">SaSS_1964_NganasanBraveBoy_flk.154 (001.141)</ta>
            <ta e="T777" id="Seg_5081" s="T771">SaSS_1964_NganasanBraveBoy_flk.155 (001.142)</ta>
            <ta e="T778" id="Seg_5082" s="T777">SaSS_1964_NganasanBraveBoy_flk.156 (001.143)</ta>
            <ta e="T784" id="Seg_5083" s="T778">SaSS_1964_NganasanBraveBoy_flk.157 (001.143)</ta>
            <ta e="T789" id="Seg_5084" s="T784">SaSS_1964_NganasanBraveBoy_flk.158 (001.144)</ta>
            <ta e="T799" id="Seg_5085" s="T789">SaSS_1964_NganasanBraveBoy_flk.159 (001.145)</ta>
            <ta e="T802" id="Seg_5086" s="T799">SaSS_1964_NganasanBraveBoy_flk.160 (001.146)</ta>
            <ta e="T807" id="Seg_5087" s="T802">SaSS_1964_NganasanBraveBoy_flk.161 (001.147)</ta>
            <ta e="T812" id="Seg_5088" s="T807">SaSS_1964_NganasanBraveBoy_flk.162 (001.148)</ta>
            <ta e="T820" id="Seg_5089" s="T812">SaSS_1964_NganasanBraveBoy_flk.163 (001.149)</ta>
            <ta e="T828" id="Seg_5090" s="T820">SaSS_1964_NganasanBraveBoy_flk.164 (001.150)</ta>
            <ta e="T830" id="Seg_5091" s="T828">SaSS_1964_NganasanBraveBoy_flk.165 (001.151)</ta>
            <ta e="T836" id="Seg_5092" s="T830">SaSS_1964_NganasanBraveBoy_flk.166 (001.152)</ta>
            <ta e="T840" id="Seg_5093" s="T836">SaSS_1964_NganasanBraveBoy_flk.167 (001.153)</ta>
            <ta e="T844" id="Seg_5094" s="T840">SaSS_1964_NganasanBraveBoy_flk.168 (001.154)</ta>
            <ta e="T848" id="Seg_5095" s="T844">SaSS_1964_NganasanBraveBoy_flk.169 (001.155)</ta>
            <ta e="T849" id="Seg_5096" s="T848">SaSS_1964_NganasanBraveBoy_flk.170 (001.156)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T4" id="Seg_5097" s="T0">Ыраактаагылаактар, күсээйиннээктэр, кинээстээктэр буоллага.</ta>
            <ta e="T10" id="Seg_5098" s="T4">Бу киһи туспа олорор, коһуун агай.</ta>
            <ta e="T14" id="Seg_5099" s="T10">Бииркээн да огото һуок.</ta>
            <ta e="T18" id="Seg_5100" s="T14">Йылга биирдэ төлүүр нолуогун.</ta>
            <ta e="T26" id="Seg_5101" s="T18">Байгал кырыытыгар, илэ муорага олорор, байгал һылбагын оттунар.</ta>
            <ta e="T29" id="Seg_5102" s="T26">Өр олорбут, кырдьыбыт.</ta>
            <ta e="T32" id="Seg_5103" s="T29">Дьыла һаастыйан барар.</ta>
            <ta e="T34" id="Seg_5104" s="T32">Эмээксинигэр этэр: </ta>
            <ta e="T37" id="Seg_5105" s="T34">— Дьылбыт ирдэ дии.</ta>
            <ta e="T41" id="Seg_5106" s="T37">Мас тардына барыам эһиилгигэ.</ta>
            <ta e="T44" id="Seg_5107" s="T41">Отут һыргата һүөккээ.</ta>
            <ta e="T50" id="Seg_5108" s="T44">Отут һырганнан байгалга баран каалар һобуч-һоготогун.</ta>
            <ta e="T54" id="Seg_5109" s="T50">Байгалга тумус багайынан киирбит.</ta>
            <ta e="T63" id="Seg_5110" s="T54">һоготок һиртэн тиэммит, һыырын үрдүгэр биир һалаа устун төннүбүт.</ta>
            <ta e="T70" id="Seg_5111" s="T63">Бу таксан кайатын үрдүгэр табаак тарда олорбут.</ta>
            <ta e="T81" id="Seg_5112" s="T70">Эбэ диэк көрбүтэ: ууга ого һылдьар, ирбит ууга күүгэттэринэн оонньуу һылдьар.</ta>
            <ta e="T84" id="Seg_5113" s="T81">Уол ого быһыылаак.</ta>
            <ta e="T88" id="Seg_5114" s="T84">Кантан да кэлбитэ биллибэт.</ta>
            <ta e="T89" id="Seg_5115" s="T88">Одуургаабыт.</ta>
            <ta e="T93" id="Seg_5116" s="T89">— Мин огото һуокпун дьии!</ta>
            <ta e="T97" id="Seg_5117" s="T93">Ылбыт киһи, — дии һанаабыт.</ta>
            <ta e="T106" id="Seg_5118" s="T97">Огото арыы кумагыгар һылдьар, өкөгөр мас баар, онон бардага.</ta>
            <ta e="T117" id="Seg_5119" s="T106">Ыстанан тийэр огонньор, аргаатыттан кабан ылан һукуйун иһигэр укпут, курданан кээспит.</ta>
            <ta e="T120" id="Seg_5120" s="T117">Дьэ арай камныыһы.</ta>
            <ta e="T123" id="Seg_5121" s="T120">Тыас коокуна буолар.</ta>
            <ta e="T124" id="Seg_5122" s="T123">Турбут.</ta>
            <ta e="T131" id="Seg_5123" s="T124">Көрбүтэ: бүтүн мунньак киһитэ өлбүт һирэ эбит.</ta>
            <ta e="T137" id="Seg_5124" s="T131">"Ээ, балартан ордон каалбыт ого буоллага".</ta>
            <ta e="T143" id="Seg_5125" s="T137">Биир аргаабыттаак ньуогуһутун эгэлэн өлөрөн кээһэр: </ta>
            <ta e="T152" id="Seg_5126" s="T143">— Огогутун илтим, бу огогут оҥкута, биир табаны кээстим, — диир.</ta>
            <ta e="T156" id="Seg_5127" s="T152">Эгэллэ дьиэтигэр, эмээксинигэр кэпсиир.</ta>
            <ta e="T165" id="Seg_5128" s="T156">Үс күн туркары, табаларын да ыыппакка, бу огоннон ооньууллар.</ta>
            <ta e="T170" id="Seg_5129" s="T165">Өлгөбүүнэ барыта көлүллэ һытар эбит.</ta>
            <ta e="T173" id="Seg_5130" s="T170">Туок да биллибэт.</ta>
            <ta e="T178" id="Seg_5131" s="T173">Огото һүүрэлии һылдьар буолбут, улааппыт.</ta>
            <ta e="T188" id="Seg_5132" s="T178">Одуулуу олордогуна, һыргага биэс чээлкээ буурдаак чэбис-чээлкээ таҥастаак киһи иһэр.</ta>
            <ta e="T190" id="Seg_5133" s="T188">Кинээс эбит.</ta>
            <ta e="T195" id="Seg_5134" s="T190">— дии һаныыр.</ta>
            <ta e="T198" id="Seg_5135" s="T195">— Огонньор, кайдак олордуҥ?</ta>
            <ta e="T206" id="Seg_5136" s="T198">Кайдак маннык өр буоллум, оголоммуккун дуу? — диир кинээс.</ta>
            <ta e="T211" id="Seg_5137" s="T206">— Кырдьар һааспытыгар оголоннубут!— диир огонньор.</ta>
            <ta e="T213" id="Seg_5138" s="T211">— Дьэ үчүгэй.</ta>
            <ta e="T218" id="Seg_5139" s="T213">Нолуок комуйа кэллим, — диир кинээс.</ta>
            <ta e="T224" id="Seg_5140" s="T218">Огонньор төһө эмэ күүл кырсаны биэрэр.</ta>
            <ta e="T230" id="Seg_5141" s="T224">Кинээһэ үөрэр, ордугун карчии ыытыам — диир.</ta>
            <ta e="T232" id="Seg_5142" s="T230">Кинээһэ камныыһы.</ta>
            <ta e="T238" id="Seg_5143" s="T232">Канна эрэ дьураак кинээһигэр көтүттэ, догоругар.</ta>
            <ta e="T241" id="Seg_5144" s="T238">Кинээс кинээһигэр кэлбит.</ta>
            <ta e="T243" id="Seg_5145" s="T241">Аргилыы һыталлар.</ta>
            <ta e="T247" id="Seg_5146" s="T243">Кас күн аргилыы һыппыттар.</ta>
            <ta e="T252" id="Seg_5147" s="T247">Ол олорон өс кэпсээбит догоругар: </ta>
            <ta e="T257" id="Seg_5148" s="T252">— Дьэ-дьэ, мин киһи куттанар киһилээкпин.</ta>
            <ta e="T262" id="Seg_5149" s="T257">Нолуогун ол эрээри аһары төлүүр.</ta>
            <ta e="T264" id="Seg_5150" s="T262">Онтон кэллим.</ta>
            <ta e="T268" id="Seg_5151" s="T264">Одуургуубун — һүүрэ һылдьар оголоммут.</ta>
            <ta e="T272" id="Seg_5152" s="T268">Бэйэтэ төрөөбүт огото буолбатак.</ta>
            <ta e="T275" id="Seg_5153" s="T272">Кантан ылбыта буолуой?</ta>
            <ta e="T280" id="Seg_5154" s="T275">Кими эрэ дьүрү өлөрөн ыллага?!</ta>
            <ta e="T290" id="Seg_5155" s="T280">— Ээк, — диир дьураак кинээһэ, — былыр мин биир муньак киһим өлбүтэ.</ta>
            <ta e="T296" id="Seg_5156" s="T290">Онтон биир биһиктээк ого каалбыт һурактаага.</ta>
            <ta e="T301" id="Seg_5157" s="T296">Ону ылларбатагым ыарыы батыһыа диэммин.</ta>
            <ta e="T303" id="Seg_5158" s="T301">Ону ыллага.</ta>
            <ta e="T307" id="Seg_5159" s="T303">Ол огону кистиэбит эбит.</ta>
            <ta e="T310" id="Seg_5160" s="T307">Миигин өлөрүөктэрэ диэн.</ta>
            <ta e="T312" id="Seg_5161" s="T310">Дьэ бугурдук.</ta>
            <ta e="T316" id="Seg_5162" s="T312">һүбэлиибин: эн киһигиттэн куттаныма.</ta>
            <ta e="T329" id="Seg_5163" s="T316">Ол мунньак дьонун кырган баран огону ылбыт диэн икки кинээс ыраактаагыта үҥсэн һуруйуок.</ta>
            <ta e="T346" id="Seg_5164" s="T329">Оччого биһиги тылбытын ыраактаагы быһыа һуога, кинини һууттатыа, каайыыга ыытыага, биһиги кини баайын ылыакпыт, — дьиэбит дьураак киниэһэ.</ta>
            <ta e="T353" id="Seg_5165" s="T346">— Киһиэкэ күтүрээтэккэ куһаган буолаарай? — диир һаамай кинээһэ.</ta>
            <ta e="T362" id="Seg_5166" s="T353">— Тыа кинээһин ыгырбыт буоллар, үс кинээс итэгэллээк буолуок этэ.</ta>
            <ta e="T364" id="Seg_5167" s="T362">Дьураак кинээһэ: </ta>
            <ta e="T371" id="Seg_5168" s="T364">— Киһитэ ыытыам тыалар кинээстэригэр, үс конугунан кэлиэ.</ta>
            <ta e="T374" id="Seg_5169" s="T371">һөбүлэммэтэгинэ бэйэтин өлөрүллүө.</ta>
            <ta e="T377" id="Seg_5170" s="T374">Утарсыа һуога, — диир.</ta>
            <ta e="T383" id="Seg_5171" s="T377">һыргалаак киһини ыытта тыа кинээһин ыгыртара.</ta>
            <ta e="T390" id="Seg_5172" s="T383">Үһүс күнүгэр тыа кинээһэ кэлэр, огуруо таҥастаак.</ta>
            <ta e="T394" id="Seg_5173" s="T390">Үс кас күн аргилыыллар.</ta>
            <ta e="T397" id="Seg_5174" s="T394">Дьураак кинээһэ кэпсиир: </ta>
            <ta e="T405" id="Seg_5175" s="T397">— һаамай киһитэ ыарыыттан өлбүт дьонтон биир огону ылбыт.</ta>
            <ta e="T408" id="Seg_5176" s="T405">Ону ыарыы батыһыа.</ta>
            <ta e="T413" id="Seg_5177" s="T408">Биһиги ыраактаагыга үҥсүөгүҥ, үһүөн һуруйуогуҥ.</ta>
            <ta e="T417" id="Seg_5178" s="T413">Тыа кинээһэ дуумайдаабыт, дьиэбит: </ta>
            <ta e="T419" id="Seg_5179" s="T417">— Һууттуурун һууттаамыйа.</ta>
            <ta e="T427" id="Seg_5180" s="T419">Ол эрээри күтүрүүр һүрэ бэрт дьону кэйбит диэн.</ta>
            <ta e="T434" id="Seg_5181" s="T427">Тулаайаги ылбыта туок буруйай, һуукка биэриэкпитин, — дьиэбит.</ta>
            <ta e="T436" id="Seg_5182" s="T434">Дьураак кинээһэ: </ta>
            <ta e="T441" id="Seg_5183" s="T436">— Эн көмүскүүгүн дуо ол киһини?</ta>
            <ta e="T445" id="Seg_5184" s="T441">Оччого биһиги эньигин кэйэбит.</ta>
            <ta e="T448" id="Seg_5185" s="T445">Баайгын ылыакпыт, — диир.</ta>
            <ta e="T451" id="Seg_5186" s="T448">Тыа кинээһэ куттаммыт. </ta>
            <ta e="T454" id="Seg_5187" s="T451">Өлөрүөксүт дьон иккиэн.</ta>
            <ta e="T458" id="Seg_5188" s="T454">— Дьэ кайтак һүбэлиигит?— дьиэбит.</ta>
            <ta e="T460" id="Seg_5189" s="T458">Дьураак кинээһэ: </ta>
            <ta e="T464" id="Seg_5190" s="T460">— Мин тойон һүбэһит буолабын.</ta>
            <ta e="T476" id="Seg_5191" s="T464">Үҥсүөгүҥ ыраактаагыга: бу коһуун һаамай мунньагын кэйэн баран огону ылбыт диэн һуруйуокпут.</ta>
            <ta e="T487" id="Seg_5192" s="T476">Мин киһилэрим баран өлбүт дьону ытыалыактара, комиссия кэллэгинэ булан ылыа һуога.</ta>
            <ta e="T490" id="Seg_5193" s="T487">Ким таайыай, — диир.</ta>
            <ta e="T497" id="Seg_5194" s="T490">Уонча киһи баран былыр өлбүт дьону ытыалыыллар.</ta>
            <ta e="T500" id="Seg_5195" s="T497">һуруйан кээстилэр огордук.</ta>
            <ta e="T505" id="Seg_5196" s="T500">һаамай кинээһин ыыталлар дьураак кинээһиниин.</ta>
            <ta e="T515" id="Seg_5197" s="T505">Ыраактаагы ааган көрөр, уонча дьиэ дьону кэйбит, огону уорбут киһи.</ta>
            <ta e="T518" id="Seg_5198" s="T515">Ыраактаагы барыларыттан ыйытар: </ta>
            <ta e="T520" id="Seg_5199" s="T518">— Кирдик дуо?</ta>
            <ta e="T525" id="Seg_5200" s="T520">Мин кинини һууттуом, эһиги истэргитигэр.</ta>
            <ta e="T533" id="Seg_5201" s="T525">һаамай кинээһэ: — Киһиэкэ һугаһаабат киһи, оҥорто буолуо, — диир.</ta>
            <ta e="T537" id="Seg_5202" s="T533">Огонньорго албун һурук ыыталлар.</ta>
            <ta e="T548" id="Seg_5203" s="T537">Кинээһэ: "Чэ, нолуогун карчытын ыла кэлбэктээтин бэрт муҥ түргэнник", — диэн һуруйар.</ta>
            <ta e="T552" id="Seg_5204" s="T548">Илдьит киһи тиэрдэр һуругу.</ta>
            <ta e="T558" id="Seg_5205" s="T552">Огонньор муҥ үтүө табаларынан көтүтэн кэлэр.</ta>
            <ta e="T563" id="Seg_5206" s="T558">Табатын баайбыт да көтөн түһэр.</ta>
            <ta e="T567" id="Seg_5207" s="T563">Остоолу төгүрүччү тойон бөгө.</ta>
            <ta e="T575" id="Seg_5208" s="T567">Ыраактаагы чаай да иһэрпэтэк, каана куһаган, чипчик атын.</ta>
            <ta e="T578" id="Seg_5209" s="T575">— Дьэ кэпсээ өскүн.</ta>
            <ta e="T582" id="Seg_5210" s="T578">Мунньак дьону того кыртыҥ?</ta>
            <ta e="T590" id="Seg_5211" s="T582">— Туок мунньагын һаптарагын, киһини кытта мөккүөнүм да һуок.</ta>
            <ta e="T594" id="Seg_5212" s="T590">Ыраактаагы кинээстэр һуруктарын көрдөрөр: </ta>
            <ta e="T604" id="Seg_5213" s="T594">— Эһиилги уу кэлиэр диэри каайыыга һытыаҥ, миигиннээгэр улакан ыраактаагыга ыытыам.</ta>
            <ta e="T608" id="Seg_5214" s="T604">Огонньору түрмэгэ каайан кээһэллэр.</ta>
            <ta e="T612" id="Seg_5215" s="T608">Үс кинээс баайын үллэстиэктэрэ.</ta>
            <ta e="T621" id="Seg_5216" s="T612">Эмээксин көһөн кэлэр, огонньоро каччага да һуок, түрмэгэ һытар.</ta>
            <ta e="T625" id="Seg_5217" s="T621">Ыраактаагы эмээксини үүрэн таһаарар.</ta>
            <ta e="T635" id="Seg_5218" s="T625">Үс кинээс баайын һаба түһэн үллэстэн ылаллар, биир өлдьүүнү кааллараллар.</ta>
            <ta e="T640" id="Seg_5219" s="T635">Кыһына баранан, порокуот кэлээктээбит үүһэттэн.</ta>
            <ta e="T644" id="Seg_5220" s="T640">— Дьэ, көр эргин, — дииллэр.</ta>
            <ta e="T652" id="Seg_5221" s="T644">Огонньор карага иин иһигэр киирбит, аһаппатактар, октон каалбыт.</ta>
            <ta e="T655" id="Seg_5222" s="T652">Порокуот иһигэр быракпыттар.</ta>
            <ta e="T663" id="Seg_5223" s="T655">Нөҥүө ыраактаагыга ильльэллэр, носилканнан киллэрэллэр, өлөн эрэр [киһини].</ta>
            <ta e="T668" id="Seg_5224" s="T663">— Кайа, кайтак һууттуомуй өлбүт киһини?</ta>
            <ta e="T672" id="Seg_5225" s="T668">Илдьиҥ больницага, аһаттыннар, көрдүннэр.</ta>
            <ta e="T677" id="Seg_5226" s="T672">Дуоктуурдар эмтииллэр, эргэ бэйэтэ буолар.</ta>
            <ta e="T679" id="Seg_5227" s="T677">Дуоктуурдар ыйыталлар: </ta>
            <ta e="T683" id="Seg_5228" s="T679">— Буруйуҥ дуу, күтүрүүллэр дуу?</ta>
            <ta e="T686" id="Seg_5229" s="T683">Огонньор кэпсиир барытын.</ta>
            <ta e="T689" id="Seg_5230" s="T686">Дуоктуурдар каан ылаллар: </ta>
            <ta e="T695" id="Seg_5231" s="T689">— Буруйдаак буоллаккына, кааныҥ атын буолуо, — дииллэр.</ta>
            <ta e="T701" id="Seg_5232" s="T695">Бииркээн да каапелька буруйа һуок эбит.</ta>
            <ta e="T703" id="Seg_5233" s="T701">һурук биэрэллэр: </ta>
            <ta e="T707" id="Seg_5234" s="T703">— Маны һуукка пирээмэ биэрээр.</ta>
            <ta e="T712" id="Seg_5235" s="T707">Кинээстэр кааннарын эмиэ ылыакпыт, — дииллэр.</ta>
            <ta e="T714" id="Seg_5236" s="T712">һуут буолар.</ta>
            <ta e="T720" id="Seg_5237" s="T714">Кинээстэр, маҥнайгы ыраактаагы, элбэк дьон мунньустубут.</ta>
            <ta e="T722" id="Seg_5238" s="T720">Ыраактаагы этэр: </ta>
            <ta e="T725" id="Seg_5239" s="T722">— Өһүн-тылын истиэккэ, — диир.</ta>
            <ta e="T727" id="Seg_5240" s="T725">Огонньор кэпсиир: </ta>
            <ta e="T733" id="Seg_5241" s="T727">— Мин даасе киһини кытта өлөссүбэтэк киһибин, </ta>
            <ta e="T741" id="Seg_5242" s="T733">— ыспараап-катын биэрэр: "Бу киһи каапелька да буруйа һуок".</ta>
            <ta e="T744" id="Seg_5243" s="T741">Кинээстэр кааннарын ылаллар.</ta>
            <ta e="T753" id="Seg_5244" s="T744">һаамай коһуунун каана илэ атын, кинээстэр гиэттэрэ чиҥкэ атын.</ta>
            <ta e="T756" id="Seg_5245" s="T753">Ыраактаагы дьэ ыгайар: </ta>
            <ta e="T758" id="Seg_5246" s="T756">— Ким һүбэлээбитэй?</ta>
            <ta e="T759" id="Seg_5247" s="T758">Кэпсээҥ!</ta>
            <ta e="T762" id="Seg_5248" s="T759">Тыа кинээһэ кэпсиир: </ta>
            <ta e="T766" id="Seg_5249" s="T762">— Дьэ кирдигэ, биһиги буруйдаакпыт.</ta>
            <ta e="T771" id="Seg_5250" s="T766">Һаамай улакан буруй-даак — дьураак кинээһэ.</ta>
            <ta e="T777" id="Seg_5251" s="T771">Миньигин кэйиэк бааллара, куттанан подпись биэрбитим.</ta>
            <ta e="T778" id="Seg_5252" s="T777">Ыраактаагы: </ta>
            <ta e="T784" id="Seg_5253" s="T778">— Дьэ, огонньор, эн буруйун һуок эбит.</ta>
            <ta e="T789" id="Seg_5254" s="T784">Каайыыга эйигин һууттаабыт ыраактаагыны олордуом.</ta>
            <ta e="T799" id="Seg_5255" s="T789">Эн гини оннугар ыраактаагыннан барыаҥ, кинээстэри онно тийэн һууттаар, — диэбит.</ta>
            <ta e="T802" id="Seg_5256" s="T799">Камнаан каалаллар төттөрү.</ta>
            <ta e="T807" id="Seg_5257" s="T802">Огонньор ологугар кэлэн кинээс буолар.</ta>
            <ta e="T812" id="Seg_5258" s="T807">Уола улаатан пирээмэ-пирээмэ букатыыр буолбут.</ta>
            <ta e="T820" id="Seg_5259" s="T812">Кини һуогуна өлөрөөрү гыммыттар, агатыгар талы буолуо диэн.</ta>
            <ta e="T828" id="Seg_5260" s="T820">Уол биир киһини түҥнэри оксубут, ону каайыыга олорпуттар.</ta>
            <ta e="T830" id="Seg_5261" s="T828">Кинээстэрин дьүүллүүр.</ta>
            <ta e="T836" id="Seg_5262" s="T830">һаамай кинээһин, дьураак кинээһиниин түрмэгэ олордор.</ta>
            <ta e="T840" id="Seg_5263" s="T836">Тыа кинээһин кинээһиттэн уһулар.</ta>
            <ta e="T844" id="Seg_5264" s="T840">Уолун һаамай кинээһэ оҥорор.</ta>
            <ta e="T848" id="Seg_5265" s="T844">Огонньор итинник кирдигэ таксыбыта.</ta>
            <ta e="T849" id="Seg_5266" s="T848">Элэтэ.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_5267" s="T0">ɨraːktaːgɨlaːktar, küseːjinneːkter, kineːsteːkter bu͡ollaga. </ta>
            <ta e="T10" id="Seg_5268" s="T4">Bu kihi tuspa oloror, kohuːn agaj. </ta>
            <ta e="T14" id="Seg_5269" s="T10">Biːrkeːn da ogoto hu͡ok. </ta>
            <ta e="T18" id="Seg_5270" s="T14">Jɨlga biːrde tölüːr nolu͡ogun. </ta>
            <ta e="T26" id="Seg_5271" s="T18">Bajgal kɨrɨːtɨgar, ile mu͡oraga oloror, bajgal hɨlbagɨn ottunar. </ta>
            <ta e="T29" id="Seg_5272" s="T26">Ör olorbut, kɨrdʼɨbɨt. </ta>
            <ta e="T32" id="Seg_5273" s="T29">Dʼɨla haːstɨjan barar. </ta>
            <ta e="T34" id="Seg_5274" s="T32">Emeːksiniger eter: </ta>
            <ta e="T37" id="Seg_5275" s="T34">"Dʼɨlbɨt irde diː. </ta>
            <ta e="T41" id="Seg_5276" s="T37">Mas tardɨna barɨ͡am ehiːlgige. </ta>
            <ta e="T44" id="Seg_5277" s="T41">Otut hɨrgata hü͡ökkeː. </ta>
            <ta e="T50" id="Seg_5278" s="T44">Otut hɨrgannan bajgalga baran kaːlar hobuč-hogotogun. </ta>
            <ta e="T54" id="Seg_5279" s="T50">Bajgalga tumus bagajɨnan kiːrbit. </ta>
            <ta e="T63" id="Seg_5280" s="T54">Hogotok hirten ti͡emmit, hɨːrɨn ürdüger biːr halaː ustun tönnübüt. </ta>
            <ta e="T70" id="Seg_5281" s="T63">Bu taksan kajatɨn ürdüger tabaːk tarda olorbut. </ta>
            <ta e="T81" id="Seg_5282" s="T70">Ebe di͡ek körbüte, uːga ogo hɨldʼar, irbit uːga küːgetterinen oːnnʼuː hɨldʼar. </ta>
            <ta e="T84" id="Seg_5283" s="T81">U͡ol ogo bɨhɨːlaːk. </ta>
            <ta e="T88" id="Seg_5284" s="T84">Kantan da kelbite billibet. </ta>
            <ta e="T89" id="Seg_5285" s="T88">Oduːrgaːbɨt. </ta>
            <ta e="T93" id="Seg_5286" s="T89">"Min ogoto hu͡okpun dʼiː!" </ta>
            <ta e="T97" id="Seg_5287" s="T93">ɨlbɨt kihi", diː hanaːbɨt. </ta>
            <ta e="T106" id="Seg_5288" s="T97">Ogoto arɨː kumagɨgar hɨldʼar, ökögör mas baːr, onon bardaga. </ta>
            <ta e="T117" id="Seg_5289" s="T106">ɨstanan tijer ogonnʼor, argaːtɨttan kaban ɨlan hukujun ihiger ukput, kurdanan keːspit. </ta>
            <ta e="T120" id="Seg_5290" s="T117">Dʼe araj kamnɨːhɨ. </ta>
            <ta e="T123" id="Seg_5291" s="T120">Tɨ͡as koːkuna bu͡olar. </ta>
            <ta e="T124" id="Seg_5292" s="T123">Turbut. </ta>
            <ta e="T131" id="Seg_5293" s="T124">Körbüte, bütün munnʼak kihite ölbüt hire ebit. </ta>
            <ta e="T137" id="Seg_5294" s="T131">"Eː, balartan ordon kaːlbɨt ogo bu͡ollaga". </ta>
            <ta e="T143" id="Seg_5295" s="T137">Biːr argaːbɨttaːk nʼu͡oguhutun egelen ölörön keːher: </ta>
            <ta e="T152" id="Seg_5296" s="T143">"Ogogutun iltim, bu ogogut oŋkuta, biːr tabanɨ keːstim", diːr. </ta>
            <ta e="T156" id="Seg_5297" s="T152">Egelle dʼi͡etiger, emeːksiniger kepsiːr. </ta>
            <ta e="T165" id="Seg_5298" s="T156">Üs kün turkarɨ, tabalarɨn da ɨːppakka, bu ogonnon oːnʼuːllar. </ta>
            <ta e="T170" id="Seg_5299" s="T165">Ölgöbüːne barɨta kölülle hɨtar ebit. </ta>
            <ta e="T173" id="Seg_5300" s="T170">Tu͡ok da billibet. </ta>
            <ta e="T178" id="Seg_5301" s="T173">Ogoto hüːreliː hɨldʼar bu͡olbut, ulaːppɨt. </ta>
            <ta e="T188" id="Seg_5302" s="T178">Oduːluː olordoguna, hɨrgaga bi͡es čeːlkeː buːrdaːk čebis-čeːlkeː taŋastaːk kihi iher. </ta>
            <ta e="T190" id="Seg_5303" s="T188">Kineːs ebit. </ta>
            <ta e="T195" id="Seg_5304" s="T190">"Dʼe tu͡okpun bi͡eri͡emij" —diː hanɨːr. </ta>
            <ta e="T198" id="Seg_5305" s="T195">"Ogonnʼor, kajdak olorduŋ? </ta>
            <ta e="T206" id="Seg_5306" s="T198">Kajdak mannɨk ör bu͡ollum, ogolommukkun duː", diːr kineːs. </ta>
            <ta e="T211" id="Seg_5307" s="T206">"Kɨrdʼar haːspɨtɨgar ogolonnubut", diːr ogonnʼor. </ta>
            <ta e="T213" id="Seg_5308" s="T211">"Dʼe üčügej. </ta>
            <ta e="T218" id="Seg_5309" s="T213">Nolu͡ok komuja kellim", diːr kineːs. </ta>
            <ta e="T224" id="Seg_5310" s="T218">Ogonnʼor töhö eme küːl kɨrsanɨ bi͡erer. </ta>
            <ta e="T230" id="Seg_5311" s="T224">Kineːhe ü͡örer, "ordugun karčiː ɨːtɨ͡am", diːr. </ta>
            <ta e="T232" id="Seg_5312" s="T230">Kineːhe kamnɨːhɨ. </ta>
            <ta e="T238" id="Seg_5313" s="T232">Kanna ere dʼuraːk kineːhiger kötütte, dogorugar. </ta>
            <ta e="T241" id="Seg_5314" s="T238">Kineːs kineːhiger kelbit. </ta>
            <ta e="T243" id="Seg_5315" s="T241">Argilɨː hɨtallar. </ta>
            <ta e="T247" id="Seg_5316" s="T243">Kas kün argilɨː hɨppɨttar. </ta>
            <ta e="T252" id="Seg_5317" s="T247">Ol oloron ös kepseːbit dogorugar: </ta>
            <ta e="T257" id="Seg_5318" s="T252">"Dʼe-dʼe, min kihi kuttanar kihileːkpin. </ta>
            <ta e="T262" id="Seg_5319" s="T257">Nolu͡ogun ol ereːri aharɨ tölüːr. </ta>
            <ta e="T264" id="Seg_5320" s="T262">Onton kellim. </ta>
            <ta e="T268" id="Seg_5321" s="T264">Oduːrguːbun— hüːre hɨldʼar ogolommut. </ta>
            <ta e="T272" id="Seg_5322" s="T268">Bejete töröːbüt ogoto bu͡olbatak. </ta>
            <ta e="T275" id="Seg_5323" s="T272">Kantan ɨlbɨta bu͡olu͡oj? </ta>
            <ta e="T280" id="Seg_5324" s="T275">Kimi ere dʼürü ölörön ɨllaga?" </ta>
            <ta e="T290" id="Seg_5325" s="T280">"Eːk", diːr dʼuraːk kineːhe, "bɨlɨr min biːr munʼak kihim ölbüte. </ta>
            <ta e="T296" id="Seg_5326" s="T290">Onton biːr bihikteːk ogo kaːlbɨt huraktaːga. </ta>
            <ta e="T301" id="Seg_5327" s="T296">Onu ɨllarbatagɨm ɨ͡arɨː batɨhɨ͡a di͡emmin. </ta>
            <ta e="T303" id="Seg_5328" s="T301">Onu ɨllaga. </ta>
            <ta e="T307" id="Seg_5329" s="T303">Ol ogonu kisti͡ebit ebit. </ta>
            <ta e="T310" id="Seg_5330" s="T307">"Miːgin ölörü͡öktere" di͡en. </ta>
            <ta e="T312" id="Seg_5331" s="T310">Dʼe bugurduk. </ta>
            <ta e="T316" id="Seg_5332" s="T312">Hübeliːbin, en kihigitten kuttanɨma. </ta>
            <ta e="T329" id="Seg_5333" s="T316">Ol munnʼak dʼonun kɨrgan baran ogonu ɨlbɨt di͡en ikki kineːs ɨraːktaːgɨta üŋsen huruju͡ok. </ta>
            <ta e="T346" id="Seg_5334" s="T329">Oččogo bihigi tɨlbɨtɨn ɨraːktaːgɨ bɨhɨ͡a hu͡oga, kinini huːttatɨ͡a, kaːjɨːga ɨːtɨ͡aga, bihigi kini baːjɨn ɨlɨ͡akpɨt", dʼi͡ebit dʼuraːk kini͡ehe. </ta>
            <ta e="T353" id="Seg_5335" s="T346">"Kihi͡eke kütüreːtekke kuhagan bu͡olaːraj", diːr haːmaj kineːhe. </ta>
            <ta e="T362" id="Seg_5336" s="T353">"Tɨ͡a kineːhin ɨgɨrbɨt bu͡ollar, üs kineːs itegelleːk bu͡olu͡ok ete." </ta>
            <ta e="T364" id="Seg_5337" s="T362">Dʼuraːk kineːhe: </ta>
            <ta e="T371" id="Seg_5338" s="T364">"Kihite ɨːtɨ͡am tɨ͡alar kineːsteriger, üs konugunan keli͡e. </ta>
            <ta e="T374" id="Seg_5339" s="T371">Höbülemmetegine bejetin ölörüllü͡ö. </ta>
            <ta e="T377" id="Seg_5340" s="T374">Utarsɨ͡a hu͡oga", diːr. </ta>
            <ta e="T383" id="Seg_5341" s="T377">Hɨrgalaːk kihini ɨːtta tɨ͡a kineːhin ɨgɨrtara. </ta>
            <ta e="T390" id="Seg_5342" s="T383">Ühüs künüger tɨ͡a kineːhe keler, oguru͡o taŋastaːk. </ta>
            <ta e="T394" id="Seg_5343" s="T390">Üs kas kün argilɨːllar. </ta>
            <ta e="T397" id="Seg_5344" s="T394">Dʼuraːk kineːhe kepsiːr: </ta>
            <ta e="T405" id="Seg_5345" s="T397">"Haːmaj kihite ɨ͡arɨːttan ölbüt dʼonton biːr ogonu ɨlbɨt. </ta>
            <ta e="T408" id="Seg_5346" s="T405">Onu ɨ͡arɨː batɨhɨ͡a. </ta>
            <ta e="T413" id="Seg_5347" s="T408">Bihigi ɨraːktaːgɨga üŋsü͡ögüŋ, ühü͡ön huruju͡oguŋ." </ta>
            <ta e="T417" id="Seg_5348" s="T413">Tɨ͡a kineːhe duːmajdaːbɨt, dʼi͡ebit: </ta>
            <ta e="T419" id="Seg_5349" s="T417">"Huːttuːrun huːttaːmɨja. </ta>
            <ta e="T427" id="Seg_5350" s="T419">Ol ereːri kütürüːr hüre bert dʼonu kejbit di͡en. </ta>
            <ta e="T434" id="Seg_5351" s="T427">Tulaːjagi ɨlbɨta tu͡ok burujaj, huːkka bi͡eri͡ekpitin", dʼi͡ebit. </ta>
            <ta e="T436" id="Seg_5352" s="T434">Dʼuraːk kineːhe: </ta>
            <ta e="T441" id="Seg_5353" s="T436">"En kömüsküːgün du͡o ol kihini? </ta>
            <ta e="T445" id="Seg_5354" s="T441">Oččogo bihigi enʼigin kejebit. </ta>
            <ta e="T448" id="Seg_5355" s="T445">Baːjgɨn ɨlɨ͡akpɨt", diːr. </ta>
            <ta e="T451" id="Seg_5356" s="T448">Tɨ͡a kineːhe kuttammɨt. </ta>
            <ta e="T454" id="Seg_5357" s="T451">Ölörü͡öksüt dʼon ikki͡en. </ta>
            <ta e="T458" id="Seg_5358" s="T454">"Dʼe kajtak hübeliːgit", dʼi͡ebit. </ta>
            <ta e="T460" id="Seg_5359" s="T458">Dʼuraːk kineːhe: </ta>
            <ta e="T464" id="Seg_5360" s="T460">"Min tojon hübehit bu͡olabɨn. </ta>
            <ta e="T476" id="Seg_5361" s="T464">Üŋsü͡ögüŋ ɨraːktaːgɨga, bu kohuːn haːmaj munnʼagɨn kejen baran ogonu ɨlbɨt di͡en huruju͡okput. </ta>
            <ta e="T487" id="Seg_5362" s="T476">Min kihilerim baran ölbüt dʼonu ɨtɨ͡alɨ͡aktara, komissija kellegine bulan ɨlɨ͡a hu͡oga. </ta>
            <ta e="T490" id="Seg_5363" s="T487">Kim taːjɨ͡aj", diːr. </ta>
            <ta e="T497" id="Seg_5364" s="T490">U͡onča kihi baran bɨlɨr ölbüt dʼonu ɨtɨ͡alɨːllar. </ta>
            <ta e="T500" id="Seg_5365" s="T497">Hurujan keːstiler ogorduk. </ta>
            <ta e="T505" id="Seg_5366" s="T500">Haːmaj kineːhin ɨːtallar dʼuraːk kineːhiniːn. </ta>
            <ta e="T515" id="Seg_5367" s="T505">ɨraːktaːgɨ aːgan körör, u͡onča dʼi͡e dʼonu kejbit, ogonu u͡orbut kihi. </ta>
            <ta e="T518" id="Seg_5368" s="T515">ɨraːktaːgɨ barɨlarɨttan ɨjɨtar: </ta>
            <ta e="T520" id="Seg_5369" s="T518">"Kirdik du͡o? </ta>
            <ta e="T525" id="Seg_5370" s="T520">Min kinini huːttu͡om, ehigi istergitiger." </ta>
            <ta e="T533" id="Seg_5371" s="T525">Haːmaj kineːhe "kihi͡eke hugahaːbat kihi, oŋorto bu͡olu͡o" diːr. </ta>
            <ta e="T537" id="Seg_5372" s="T533">Ogonnʼorgo albun huruk ɨːtallar. </ta>
            <ta e="T548" id="Seg_5373" s="T537">Kineːhe če, nolu͡ogun karčɨtɨn ɨla kelbekteːtin bert muŋ türgennik di͡en hurujar. </ta>
            <ta e="T552" id="Seg_5374" s="T548">Ildʼit kihi ti͡erder hurugu. </ta>
            <ta e="T558" id="Seg_5375" s="T552">Ogonnʼor muŋ ütü͡ö tabalarɨnan kötüten keler. </ta>
            <ta e="T563" id="Seg_5376" s="T558">Tabatɨn baːjbɨt da kötön tüher. </ta>
            <ta e="T567" id="Seg_5377" s="T563">Ostoːlu tögürüččü tojon bögö. </ta>
            <ta e="T575" id="Seg_5378" s="T567">ɨraːktaːgɨ čaːj da iherpetek, kaːna kuhagan, čipčik atɨn. </ta>
            <ta e="T578" id="Seg_5379" s="T575">"Dʼe kepseː öskün. </ta>
            <ta e="T582" id="Seg_5380" s="T578">Munnʼak dʼonu togo kɨrtɨŋ?" </ta>
            <ta e="T590" id="Seg_5381" s="T582">"Tu͡ok munnʼagɨn haptaragɨn, kihini kɨtta mökkü͡önüm da hu͡ok. </ta>
            <ta e="T594" id="Seg_5382" s="T590">ɨraːktaːgɨ kineːster huruktarɨn kördörör: </ta>
            <ta e="T604" id="Seg_5383" s="T594">"Ehiːlgi uː keli͡er di͡eri kaːjɨːga hɨtɨ͡aŋ, miːginneːger ulakan ɨraːktaːgɨga ɨːtɨ͡am." </ta>
            <ta e="T608" id="Seg_5384" s="T604">Ogonnʼoru türmege kaːjan keːheller. </ta>
            <ta e="T612" id="Seg_5385" s="T608">Üs kineːs baːjɨn üllesti͡ektere. </ta>
            <ta e="T621" id="Seg_5386" s="T612">Emeːksin köhön keler, ogonnʼoro kaččaga da hu͡ok, türmege hɨtar. </ta>
            <ta e="T625" id="Seg_5387" s="T621">ɨraːktaːgɨ emeːksini üːren tahaːrar. </ta>
            <ta e="T635" id="Seg_5388" s="T625">Üs kineːs baːjɨn haba tühen üllesten ɨlallar, biːr öldʼüːnü kaːllarallar. </ta>
            <ta e="T640" id="Seg_5389" s="T635">Kɨhɨna baranan, poroku͡ot keleːkteːbit üːhetten. </ta>
            <ta e="T644" id="Seg_5390" s="T640">"Dʼe, kör ergin", diːller. </ta>
            <ta e="T652" id="Seg_5391" s="T644">Ogonnʼor karaga iːn ihiger kiːrbit, ahappataktar, okton kaːlbɨt. </ta>
            <ta e="T655" id="Seg_5392" s="T652">Poroku͡ot ihiger bɨrakpɨttar. </ta>
            <ta e="T663" id="Seg_5393" s="T655">Nöŋü͡ö ɨraːktaːgɨga illʼeller, nosilkannan killereller, ölön erer [kihini]. </ta>
            <ta e="T668" id="Seg_5394" s="T663">"Kaja, kajtak huːttu͡omuj ölbüt kihini? </ta>
            <ta e="T672" id="Seg_5395" s="T668">Ildʼiŋ bolʼnicaga, ahattɨnnar, kördünner." </ta>
            <ta e="T677" id="Seg_5396" s="T672">Du͡oktuːrdar emtiːller, erge bejete bu͡olar. </ta>
            <ta e="T679" id="Seg_5397" s="T677">Du͡oktuːrdar ɨjɨtallar: </ta>
            <ta e="T683" id="Seg_5398" s="T679">"Burujuŋ duː, kütürüːller duː?" </ta>
            <ta e="T686" id="Seg_5399" s="T683">Ogonnʼor kepsiːr barɨtɨn. </ta>
            <ta e="T689" id="Seg_5400" s="T686">Du͡oktuːrdar kaːn ɨlallar: </ta>
            <ta e="T695" id="Seg_5401" s="T689">"Burujdaːk bu͡ollakkɨna, kaːnɨŋ atɨn bu͡olu͡o", diːller. </ta>
            <ta e="T701" id="Seg_5402" s="T695">Biːrkeːn da kaːpelʼka buruja hu͡ok ebit. </ta>
            <ta e="T703" id="Seg_5403" s="T701">Huruk bi͡ereller: </ta>
            <ta e="T707" id="Seg_5404" s="T703">"Manɨ huːkka pireːme bi͡ereːr. </ta>
            <ta e="T712" id="Seg_5405" s="T707">Kineːster kaːnnarɨn emi͡e ɨlɨ͡akpɨt", diːller. </ta>
            <ta e="T714" id="Seg_5406" s="T712">Huːt bu͡olar. </ta>
            <ta e="T720" id="Seg_5407" s="T714">Kineːster, maŋnajgɨ ɨraːktaːgɨ, elbek dʼon munnʼustubut. </ta>
            <ta e="T722" id="Seg_5408" s="T720">ɨraːktaːgɨ eter: </ta>
            <ta e="T725" id="Seg_5409" s="T722">"Öhün-tɨlɨn isti͡ekke", diːr. </ta>
            <ta e="T727" id="Seg_5410" s="T725">Ogonnʼor kepsiːr: </ta>
            <ta e="T733" id="Seg_5411" s="T727">"Min daːse kihini kɨtta ölössübetek kihibin."</ta>
            <ta e="T741" id="Seg_5412" s="T733">ɨsparaːpkatɨn bi͡erer "bu kihi kaːpelʼka da buruja hu͡ok". </ta>
            <ta e="T744" id="Seg_5413" s="T741">Kineːster kaːnnarɨn ɨlallar. </ta>
            <ta e="T753" id="Seg_5414" s="T744">Haːmaj kohuːnun kaːna ile atɨn, kineːster gi͡ettere čiŋke atɨn. </ta>
            <ta e="T756" id="Seg_5415" s="T753">ɨraːktaːgɨ dʼe ɨgajar: </ta>
            <ta e="T758" id="Seg_5416" s="T756">"Kim hübeleːbitej? </ta>
            <ta e="T759" id="Seg_5417" s="T758">Kepseːŋ!" </ta>
            <ta e="T762" id="Seg_5418" s="T759">Tɨ͡a kineːhe kepsiːr: </ta>
            <ta e="T766" id="Seg_5419" s="T762">"Dʼe kirdige, bihigi burujdaːkpɨt. </ta>
            <ta e="T771" id="Seg_5420" s="T766">Haːmaj ulakan burujdaːk— dʼuraːk kineːhe. </ta>
            <ta e="T777" id="Seg_5421" s="T771">Minʼigin keji͡ek baːllara, kuttanan podpisʼ bi͡erbitim. </ta>
            <ta e="T778" id="Seg_5422" s="T777">ɨraːktaːgɨ: </ta>
            <ta e="T784" id="Seg_5423" s="T778">"Dʼe, ogonnʼor, en burujun hu͡ok ebit. </ta>
            <ta e="T789" id="Seg_5424" s="T784">Kaːjɨːga ejigin huːttaːbɨt ɨraːktaːgɨnɨ olordu͡om. </ta>
            <ta e="T799" id="Seg_5425" s="T789">En gini onnugar ɨraːktaːgɨnnan barɨ͡aŋ, kineːsteri onno tijen huːttaːr", di͡ebit. </ta>
            <ta e="T802" id="Seg_5426" s="T799">Kamnaːn kaːlallar töttörü. </ta>
            <ta e="T807" id="Seg_5427" s="T802">Ogonnʼor ologugar kelen kineːs bu͡olar. </ta>
            <ta e="T812" id="Seg_5428" s="T807">U͡ola ulaːtan pireːme-pireːme bukatɨːr bu͡olbut. </ta>
            <ta e="T820" id="Seg_5429" s="T812">Kini hu͡oguna ölöröːrü gɨmmɨttar, agatɨgar talɨ bu͡olu͡o di͡en. </ta>
            <ta e="T828" id="Seg_5430" s="T820">U͡ol biːr kihini tüŋneri oksubut, onu kaːjɨːga olorputtar. </ta>
            <ta e="T830" id="Seg_5431" s="T828">Kineːsterin dʼüːllüːr. </ta>
            <ta e="T836" id="Seg_5432" s="T830">Haːmaj kineːhin, dʼuraːk kineːhiniːn türmege olordor. </ta>
            <ta e="T840" id="Seg_5433" s="T836">Tɨ͡a kineːhin kineːhitten uhular. </ta>
            <ta e="T844" id="Seg_5434" s="T840">U͡olun haːmaj kineːhe oŋoror. </ta>
            <ta e="T848" id="Seg_5435" s="T844">Ogonnʼor itinnik kirdige taksɨbɨta. </ta>
            <ta e="T849" id="Seg_5436" s="T848">Elete. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_5437" s="T0">ɨraːktaːgɨ-laːk-tar</ta>
            <ta e="T2" id="Seg_5438" s="T1">küseːjin-neːk-ter</ta>
            <ta e="T3" id="Seg_5439" s="T2">kineːs-teːk-ter</ta>
            <ta e="T4" id="Seg_5440" s="T3">bu͡ol-lag-a</ta>
            <ta e="T5" id="Seg_5441" s="T4">bu</ta>
            <ta e="T6" id="Seg_5442" s="T5">kihi</ta>
            <ta e="T7" id="Seg_5443" s="T6">tuspa</ta>
            <ta e="T8" id="Seg_5444" s="T7">olor-or</ta>
            <ta e="T9" id="Seg_5445" s="T8">kohuːn</ta>
            <ta e="T10" id="Seg_5446" s="T9">agaj</ta>
            <ta e="T11" id="Seg_5447" s="T10">biːr-keːn</ta>
            <ta e="T12" id="Seg_5448" s="T11">da</ta>
            <ta e="T13" id="Seg_5449" s="T12">ogo-to</ta>
            <ta e="T14" id="Seg_5450" s="T13">hu͡ok</ta>
            <ta e="T15" id="Seg_5451" s="T14">jɨl-ga</ta>
            <ta e="T16" id="Seg_5452" s="T15">biːrde</ta>
            <ta e="T17" id="Seg_5453" s="T16">tölüː-r</ta>
            <ta e="T18" id="Seg_5454" s="T17">nolu͡og-u-n</ta>
            <ta e="T19" id="Seg_5455" s="T18">bajgal</ta>
            <ta e="T20" id="Seg_5456" s="T19">kɨrɨː-tɨ-gar</ta>
            <ta e="T21" id="Seg_5457" s="T20">ile</ta>
            <ta e="T22" id="Seg_5458" s="T21">mu͡ora-ga</ta>
            <ta e="T23" id="Seg_5459" s="T22">olor-or</ta>
            <ta e="T24" id="Seg_5460" s="T23">bajgal</ta>
            <ta e="T25" id="Seg_5461" s="T24">hɨlbag-ɨ-n</ta>
            <ta e="T26" id="Seg_5462" s="T25">ott-u-n-ar</ta>
            <ta e="T27" id="Seg_5463" s="T26">ör</ta>
            <ta e="T28" id="Seg_5464" s="T27">olor-but</ta>
            <ta e="T29" id="Seg_5465" s="T28">kɨrdʼ-ɨ-bɨt</ta>
            <ta e="T30" id="Seg_5466" s="T29">dʼɨl-a</ta>
            <ta e="T31" id="Seg_5467" s="T30">haːs-tɨj-an</ta>
            <ta e="T32" id="Seg_5468" s="T31">bar-ar</ta>
            <ta e="T33" id="Seg_5469" s="T32">emeːksin-i-ger</ta>
            <ta e="T34" id="Seg_5470" s="T33">et-er</ta>
            <ta e="T35" id="Seg_5471" s="T34">dʼɨl-bɨt</ta>
            <ta e="T36" id="Seg_5472" s="T35">ir-d-e</ta>
            <ta e="T37" id="Seg_5473" s="T36">diː</ta>
            <ta e="T38" id="Seg_5474" s="T37">mas</ta>
            <ta e="T39" id="Seg_5475" s="T38">tard-ɨ-n-a</ta>
            <ta e="T40" id="Seg_5476" s="T39">bar-ɨ͡a-m</ta>
            <ta e="T41" id="Seg_5477" s="T40">ehiːl-gi-ge</ta>
            <ta e="T42" id="Seg_5478" s="T41">otut</ta>
            <ta e="T43" id="Seg_5479" s="T42">hɨrga-ta</ta>
            <ta e="T44" id="Seg_5480" s="T43">hü͡ökkeː</ta>
            <ta e="T45" id="Seg_5481" s="T44">otut</ta>
            <ta e="T46" id="Seg_5482" s="T45">hɨrga-nnan</ta>
            <ta e="T47" id="Seg_5483" s="T46">bajgal-ga</ta>
            <ta e="T48" id="Seg_5484" s="T47">bar-an</ta>
            <ta e="T49" id="Seg_5485" s="T48">kaːl-ar</ta>
            <ta e="T50" id="Seg_5486" s="T49">hobuč-hogotogun</ta>
            <ta e="T51" id="Seg_5487" s="T50">bajgal-ga</ta>
            <ta e="T52" id="Seg_5488" s="T51">tumus</ta>
            <ta e="T53" id="Seg_5489" s="T52">bagajɨ-nan</ta>
            <ta e="T54" id="Seg_5490" s="T53">kiːr-bit</ta>
            <ta e="T55" id="Seg_5491" s="T54">hogotok</ta>
            <ta e="T56" id="Seg_5492" s="T55">hir-ten</ta>
            <ta e="T57" id="Seg_5493" s="T56">ti͡e-m-mit</ta>
            <ta e="T58" id="Seg_5494" s="T57">hɨːr-ɨ-n</ta>
            <ta e="T59" id="Seg_5495" s="T58">ürd-ü-ger</ta>
            <ta e="T60" id="Seg_5496" s="T59">biːr</ta>
            <ta e="T61" id="Seg_5497" s="T60">halaː</ta>
            <ta e="T62" id="Seg_5498" s="T61">ustun</ta>
            <ta e="T63" id="Seg_5499" s="T62">tönn-ü-büt</ta>
            <ta e="T64" id="Seg_5500" s="T63">bu</ta>
            <ta e="T65" id="Seg_5501" s="T64">taks-an</ta>
            <ta e="T66" id="Seg_5502" s="T65">kaja-tɨ-n</ta>
            <ta e="T67" id="Seg_5503" s="T66">ürd-ü-ger</ta>
            <ta e="T68" id="Seg_5504" s="T67">tabaːk</ta>
            <ta e="T69" id="Seg_5505" s="T68">tard-a</ta>
            <ta e="T70" id="Seg_5506" s="T69">olor-but</ta>
            <ta e="T71" id="Seg_5507" s="T70">ebe</ta>
            <ta e="T72" id="Seg_5508" s="T71">di͡ek</ta>
            <ta e="T73" id="Seg_5509" s="T72">kör-büt-e</ta>
            <ta e="T74" id="Seg_5510" s="T73">uː-ga</ta>
            <ta e="T75" id="Seg_5511" s="T74">ogo</ta>
            <ta e="T76" id="Seg_5512" s="T75">hɨldʼ-ar</ta>
            <ta e="T77" id="Seg_5513" s="T76">ir-bit</ta>
            <ta e="T78" id="Seg_5514" s="T77">uː-ga</ta>
            <ta e="T79" id="Seg_5515" s="T78">küːget-teri-nen</ta>
            <ta e="T80" id="Seg_5516" s="T79">oːnnʼ-uː</ta>
            <ta e="T81" id="Seg_5517" s="T80">hɨldʼ-ar</ta>
            <ta e="T82" id="Seg_5518" s="T81">u͡ol</ta>
            <ta e="T83" id="Seg_5519" s="T82">ogo</ta>
            <ta e="T84" id="Seg_5520" s="T83">bɨhɨːlaːk</ta>
            <ta e="T85" id="Seg_5521" s="T84">kantan</ta>
            <ta e="T86" id="Seg_5522" s="T85">da</ta>
            <ta e="T87" id="Seg_5523" s="T86">kel-bit-e</ta>
            <ta e="T88" id="Seg_5524" s="T87">bil-l-i-bet</ta>
            <ta e="T89" id="Seg_5525" s="T88">oduːrgaː-bɨt</ta>
            <ta e="T90" id="Seg_5526" s="T89">min</ta>
            <ta e="T91" id="Seg_5527" s="T90">ogo-to</ta>
            <ta e="T92" id="Seg_5528" s="T91">hu͡ok-pun</ta>
            <ta e="T93" id="Seg_5529" s="T92">dʼiː</ta>
            <ta e="T94" id="Seg_5530" s="T93">ɨl-bɨt</ta>
            <ta e="T95" id="Seg_5531" s="T94">kihi</ta>
            <ta e="T96" id="Seg_5532" s="T95">d-iː</ta>
            <ta e="T97" id="Seg_5533" s="T96">hanaː-bɨt</ta>
            <ta e="T98" id="Seg_5534" s="T97">ogo-to</ta>
            <ta e="T99" id="Seg_5535" s="T98">arɨː</ta>
            <ta e="T100" id="Seg_5536" s="T99">kumag-ɨ-gar</ta>
            <ta e="T101" id="Seg_5537" s="T100">hɨldʼ-ar</ta>
            <ta e="T102" id="Seg_5538" s="T101">ökögör</ta>
            <ta e="T103" id="Seg_5539" s="T102">mas</ta>
            <ta e="T104" id="Seg_5540" s="T103">baːr</ta>
            <ta e="T105" id="Seg_5541" s="T104">o-non</ta>
            <ta e="T106" id="Seg_5542" s="T105">bar-dag-a</ta>
            <ta e="T107" id="Seg_5543" s="T106">ɨstan-an</ta>
            <ta e="T108" id="Seg_5544" s="T107">tij-er</ta>
            <ta e="T109" id="Seg_5545" s="T108">ogonnʼor</ta>
            <ta e="T110" id="Seg_5546" s="T109">argaː-tɨ-ttan</ta>
            <ta e="T111" id="Seg_5547" s="T110">kab-an</ta>
            <ta e="T112" id="Seg_5548" s="T111">ɨl-an</ta>
            <ta e="T113" id="Seg_5549" s="T112">hukuj-u-n</ta>
            <ta e="T114" id="Seg_5550" s="T113">ih-i-ger</ta>
            <ta e="T115" id="Seg_5551" s="T114">uk-put</ta>
            <ta e="T116" id="Seg_5552" s="T115">kurd-a-n-an</ta>
            <ta e="T117" id="Seg_5553" s="T116">keːs-pit</ta>
            <ta e="T118" id="Seg_5554" s="T117">dʼe</ta>
            <ta e="T119" id="Seg_5555" s="T118">araj</ta>
            <ta e="T120" id="Seg_5556" s="T119">kamn-ɨːhɨ</ta>
            <ta e="T121" id="Seg_5557" s="T120">tɨ͡as</ta>
            <ta e="T122" id="Seg_5558" s="T121">koːkun-a</ta>
            <ta e="T123" id="Seg_5559" s="T122">bu͡ol-ar</ta>
            <ta e="T124" id="Seg_5560" s="T123">tur-but</ta>
            <ta e="T125" id="Seg_5561" s="T124">kör-büt-e</ta>
            <ta e="T126" id="Seg_5562" s="T125">bütün</ta>
            <ta e="T127" id="Seg_5563" s="T126">munnʼak</ta>
            <ta e="T128" id="Seg_5564" s="T127">kihi-te</ta>
            <ta e="T129" id="Seg_5565" s="T128">öl-büt</ta>
            <ta e="T130" id="Seg_5566" s="T129">hir-e</ta>
            <ta e="T131" id="Seg_5567" s="T130">e-bit</ta>
            <ta e="T132" id="Seg_5568" s="T131">eː</ta>
            <ta e="T133" id="Seg_5569" s="T132">ba-lar-tan</ta>
            <ta e="T134" id="Seg_5570" s="T133">ord-on</ta>
            <ta e="T135" id="Seg_5571" s="T134">kaːl-bɨt</ta>
            <ta e="T136" id="Seg_5572" s="T135">ogo</ta>
            <ta e="T137" id="Seg_5573" s="T136">bu͡ollaga</ta>
            <ta e="T138" id="Seg_5574" s="T137">biːr</ta>
            <ta e="T139" id="Seg_5575" s="T138">argaː-bɨt-taːk</ta>
            <ta e="T140" id="Seg_5576" s="T139">nʼu͡oguhut-u-n</ta>
            <ta e="T141" id="Seg_5577" s="T140">egel-en</ta>
            <ta e="T142" id="Seg_5578" s="T141">ölör-ön</ta>
            <ta e="T143" id="Seg_5579" s="T142">keːh-er</ta>
            <ta e="T144" id="Seg_5580" s="T143">ogo-gutu-n</ta>
            <ta e="T145" id="Seg_5581" s="T144">il-ti-m</ta>
            <ta e="T146" id="Seg_5582" s="T145">bu</ta>
            <ta e="T147" id="Seg_5583" s="T146">ogo-gut</ta>
            <ta e="T148" id="Seg_5584" s="T147">oŋku-ta</ta>
            <ta e="T149" id="Seg_5585" s="T148">biːr</ta>
            <ta e="T150" id="Seg_5586" s="T149">taba-nɨ</ta>
            <ta e="T151" id="Seg_5587" s="T150">keːs-ti-m</ta>
            <ta e="T152" id="Seg_5588" s="T151">diː-r</ta>
            <ta e="T153" id="Seg_5589" s="T152">egel-l-e</ta>
            <ta e="T154" id="Seg_5590" s="T153">dʼi͡e-ti-ger</ta>
            <ta e="T155" id="Seg_5591" s="T154">emeːksin-i-ger</ta>
            <ta e="T156" id="Seg_5592" s="T155">kepsiː-r</ta>
            <ta e="T157" id="Seg_5593" s="T156">üs</ta>
            <ta e="T158" id="Seg_5594" s="T157">kün</ta>
            <ta e="T159" id="Seg_5595" s="T158">turkarɨ</ta>
            <ta e="T160" id="Seg_5596" s="T159">taba-larɨ-n</ta>
            <ta e="T161" id="Seg_5597" s="T160">da</ta>
            <ta e="T162" id="Seg_5598" s="T161">ɨːp-pakka</ta>
            <ta e="T163" id="Seg_5599" s="T162">bu</ta>
            <ta e="T164" id="Seg_5600" s="T163">ogo-nnon</ta>
            <ta e="T165" id="Seg_5601" s="T164">oːnʼuː-l-lar</ta>
            <ta e="T166" id="Seg_5602" s="T165">ölgöbüːn-e</ta>
            <ta e="T167" id="Seg_5603" s="T166">barɨ-ta</ta>
            <ta e="T168" id="Seg_5604" s="T167">kölü-ll-e</ta>
            <ta e="T169" id="Seg_5605" s="T168">hɨt-ar</ta>
            <ta e="T170" id="Seg_5606" s="T169">e-bit</ta>
            <ta e="T171" id="Seg_5607" s="T170">tu͡ok</ta>
            <ta e="T172" id="Seg_5608" s="T171">da</ta>
            <ta e="T173" id="Seg_5609" s="T172">bil-l-i-bet</ta>
            <ta e="T174" id="Seg_5610" s="T173">ogo-to</ta>
            <ta e="T175" id="Seg_5611" s="T174">hüːrel-iː</ta>
            <ta e="T176" id="Seg_5612" s="T175">hɨldʼ-ar</ta>
            <ta e="T177" id="Seg_5613" s="T176">bu͡ol-but</ta>
            <ta e="T178" id="Seg_5614" s="T177">ulaːp-pɨt</ta>
            <ta e="T179" id="Seg_5615" s="T178">oduːl-uː</ta>
            <ta e="T180" id="Seg_5616" s="T179">olor-dog-una</ta>
            <ta e="T181" id="Seg_5617" s="T180">hɨrga-ga</ta>
            <ta e="T182" id="Seg_5618" s="T181">bi͡es</ta>
            <ta e="T183" id="Seg_5619" s="T182">čeːlkeː</ta>
            <ta e="T184" id="Seg_5620" s="T183">buːr-daːk</ta>
            <ta e="T185" id="Seg_5621" s="T184">čebis-čeːlkeː</ta>
            <ta e="T186" id="Seg_5622" s="T185">taŋas-taːk</ta>
            <ta e="T187" id="Seg_5623" s="T186">kihi</ta>
            <ta e="T188" id="Seg_5624" s="T187">ih-er</ta>
            <ta e="T189" id="Seg_5625" s="T188">kineːs</ta>
            <ta e="T190" id="Seg_5626" s="T189">e-bit</ta>
            <ta e="T191" id="Seg_5627" s="T190">dʼe</ta>
            <ta e="T192" id="Seg_5628" s="T191">tu͡ok-pu-n</ta>
            <ta e="T193" id="Seg_5629" s="T192">bi͡er-i͡e-m=ij</ta>
            <ta e="T194" id="Seg_5630" s="T193">d-iː</ta>
            <ta e="T195" id="Seg_5631" s="T194">hanɨː-r</ta>
            <ta e="T196" id="Seg_5632" s="T195">ogonnʼor</ta>
            <ta e="T197" id="Seg_5633" s="T196">kajdak</ta>
            <ta e="T198" id="Seg_5634" s="T197">olor-du-ŋ</ta>
            <ta e="T199" id="Seg_5635" s="T198">kajdak</ta>
            <ta e="T200" id="Seg_5636" s="T199">mannɨk</ta>
            <ta e="T201" id="Seg_5637" s="T200">ör</ta>
            <ta e="T202" id="Seg_5638" s="T201">bu͡ol-lu-m</ta>
            <ta e="T203" id="Seg_5639" s="T202">ogo-lom-muk-kun</ta>
            <ta e="T204" id="Seg_5640" s="T203">duː</ta>
            <ta e="T205" id="Seg_5641" s="T204">diː-r</ta>
            <ta e="T206" id="Seg_5642" s="T205">kineːs</ta>
            <ta e="T207" id="Seg_5643" s="T206">kɨrdʼ-ar</ta>
            <ta e="T208" id="Seg_5644" s="T207">haːs-pɨtɨ-gar</ta>
            <ta e="T209" id="Seg_5645" s="T208">ogo-lon-nu-but</ta>
            <ta e="T210" id="Seg_5646" s="T209">diː-r</ta>
            <ta e="T211" id="Seg_5647" s="T210">ogonnʼor</ta>
            <ta e="T212" id="Seg_5648" s="T211">dʼe</ta>
            <ta e="T213" id="Seg_5649" s="T212">üčügej</ta>
            <ta e="T214" id="Seg_5650" s="T213">nolu͡ok</ta>
            <ta e="T215" id="Seg_5651" s="T214">komuj-a</ta>
            <ta e="T216" id="Seg_5652" s="T215">kel-li-m</ta>
            <ta e="T217" id="Seg_5653" s="T216">diː-r</ta>
            <ta e="T218" id="Seg_5654" s="T217">kineːs</ta>
            <ta e="T219" id="Seg_5655" s="T218">ogonnʼor</ta>
            <ta e="T220" id="Seg_5656" s="T219">töhö</ta>
            <ta e="T221" id="Seg_5657" s="T220">eme</ta>
            <ta e="T222" id="Seg_5658" s="T221">küːl</ta>
            <ta e="T223" id="Seg_5659" s="T222">kɨrsa-nɨ</ta>
            <ta e="T224" id="Seg_5660" s="T223">bi͡er-er</ta>
            <ta e="T225" id="Seg_5661" s="T224">kineːh-e</ta>
            <ta e="T226" id="Seg_5662" s="T225">ü͡ör-er</ta>
            <ta e="T227" id="Seg_5663" s="T226">ordug-u-n</ta>
            <ta e="T228" id="Seg_5664" s="T227">karčiː</ta>
            <ta e="T229" id="Seg_5665" s="T228">ɨːt-ɨ͡a-m</ta>
            <ta e="T230" id="Seg_5666" s="T229">diː-r</ta>
            <ta e="T231" id="Seg_5667" s="T230">kineːh-e</ta>
            <ta e="T232" id="Seg_5668" s="T231">kamn-ɨːhɨ</ta>
            <ta e="T233" id="Seg_5669" s="T232">kanna</ta>
            <ta e="T234" id="Seg_5670" s="T233">ere</ta>
            <ta e="T235" id="Seg_5671" s="T234">dʼuraːk</ta>
            <ta e="T236" id="Seg_5672" s="T235">kineːh-i-ger</ta>
            <ta e="T237" id="Seg_5673" s="T236">kötüt-t-e</ta>
            <ta e="T238" id="Seg_5674" s="T237">dogor-u-gar</ta>
            <ta e="T239" id="Seg_5675" s="T238">kineːs</ta>
            <ta e="T240" id="Seg_5676" s="T239">kineːh-i-ger</ta>
            <ta e="T241" id="Seg_5677" s="T240">kel-bit</ta>
            <ta e="T242" id="Seg_5678" s="T241">argi-l-ɨː</ta>
            <ta e="T243" id="Seg_5679" s="T242">hɨt-al-lar</ta>
            <ta e="T244" id="Seg_5680" s="T243">kas</ta>
            <ta e="T245" id="Seg_5681" s="T244">kün</ta>
            <ta e="T246" id="Seg_5682" s="T245">argi-l-ɨː</ta>
            <ta e="T247" id="Seg_5683" s="T246">hɨp-pɨt-tar</ta>
            <ta e="T248" id="Seg_5684" s="T247">ol</ta>
            <ta e="T249" id="Seg_5685" s="T248">olor-on</ta>
            <ta e="T250" id="Seg_5686" s="T249">ös</ta>
            <ta e="T251" id="Seg_5687" s="T250">kepseː-bit</ta>
            <ta e="T252" id="Seg_5688" s="T251">dogor-u-gar</ta>
            <ta e="T253" id="Seg_5689" s="T252">dʼe-dʼe</ta>
            <ta e="T254" id="Seg_5690" s="T253">min</ta>
            <ta e="T255" id="Seg_5691" s="T254">kihi</ta>
            <ta e="T256" id="Seg_5692" s="T255">kuttan-ar</ta>
            <ta e="T257" id="Seg_5693" s="T256">kihi-leːk-pin</ta>
            <ta e="T258" id="Seg_5694" s="T257">nolu͡og-u-n</ta>
            <ta e="T259" id="Seg_5695" s="T258">ol</ta>
            <ta e="T260" id="Seg_5696" s="T259">ereːri</ta>
            <ta e="T261" id="Seg_5697" s="T260">aharɨ</ta>
            <ta e="T262" id="Seg_5698" s="T261">tölüː-r</ta>
            <ta e="T263" id="Seg_5699" s="T262">on-ton</ta>
            <ta e="T264" id="Seg_5700" s="T263">kel-li-m</ta>
            <ta e="T265" id="Seg_5701" s="T264">oduːrg-uː-bun</ta>
            <ta e="T266" id="Seg_5702" s="T265">hüːr-e</ta>
            <ta e="T267" id="Seg_5703" s="T266">hɨldʼ-ar</ta>
            <ta e="T268" id="Seg_5704" s="T267">ogo-lom-mut</ta>
            <ta e="T269" id="Seg_5705" s="T268">beje-te</ta>
            <ta e="T270" id="Seg_5706" s="T269">töröː-büt</ta>
            <ta e="T271" id="Seg_5707" s="T270">ogo-to</ta>
            <ta e="T272" id="Seg_5708" s="T271">bu͡ol-batak</ta>
            <ta e="T273" id="Seg_5709" s="T272">kantan</ta>
            <ta e="T274" id="Seg_5710" s="T273">ɨl-bɨt-a</ta>
            <ta e="T275" id="Seg_5711" s="T274">bu͡ol-u͡o=j</ta>
            <ta e="T276" id="Seg_5712" s="T275">kim-i</ta>
            <ta e="T277" id="Seg_5713" s="T276">ere</ta>
            <ta e="T278" id="Seg_5714" s="T277">dʼürü</ta>
            <ta e="T279" id="Seg_5715" s="T278">ölör-ön</ta>
            <ta e="T280" id="Seg_5716" s="T279">ɨl-lag-a</ta>
            <ta e="T281" id="Seg_5717" s="T280">eːk</ta>
            <ta e="T282" id="Seg_5718" s="T281">diː-r</ta>
            <ta e="T283" id="Seg_5719" s="T282">dʼuraːk</ta>
            <ta e="T284" id="Seg_5720" s="T283">kineːh-e</ta>
            <ta e="T285" id="Seg_5721" s="T284">bɨlɨr</ta>
            <ta e="T286" id="Seg_5722" s="T285">min</ta>
            <ta e="T287" id="Seg_5723" s="T286">biːr</ta>
            <ta e="T288" id="Seg_5724" s="T287">munʼak</ta>
            <ta e="T289" id="Seg_5725" s="T288">kihi-m</ta>
            <ta e="T290" id="Seg_5726" s="T289">öl-büt-e</ta>
            <ta e="T291" id="Seg_5727" s="T290">on-ton</ta>
            <ta e="T292" id="Seg_5728" s="T291">biːr</ta>
            <ta e="T293" id="Seg_5729" s="T292">bihik-teːk</ta>
            <ta e="T294" id="Seg_5730" s="T293">ogo</ta>
            <ta e="T295" id="Seg_5731" s="T294">kaːl-bɨt</ta>
            <ta e="T296" id="Seg_5732" s="T295">hurak-taːg-a</ta>
            <ta e="T297" id="Seg_5733" s="T296">o-nu</ta>
            <ta e="T298" id="Seg_5734" s="T297">ɨl-lar-batag-ɨ-m</ta>
            <ta e="T299" id="Seg_5735" s="T298">ɨ͡arɨː</ta>
            <ta e="T300" id="Seg_5736" s="T299">batɨh-ɨ͡a</ta>
            <ta e="T301" id="Seg_5737" s="T300">di͡e-m-min</ta>
            <ta e="T302" id="Seg_5738" s="T301">o-nu</ta>
            <ta e="T303" id="Seg_5739" s="T302">ɨl-lag-a</ta>
            <ta e="T304" id="Seg_5740" s="T303">ol</ta>
            <ta e="T305" id="Seg_5741" s="T304">ogo-nu</ta>
            <ta e="T306" id="Seg_5742" s="T305">kisti͡e-bit</ta>
            <ta e="T307" id="Seg_5743" s="T306">e-bit</ta>
            <ta e="T308" id="Seg_5744" s="T307">miːgi-n</ta>
            <ta e="T309" id="Seg_5745" s="T308">ölör-ü͡ök-tere</ta>
            <ta e="T310" id="Seg_5746" s="T309">di͡e-n</ta>
            <ta e="T311" id="Seg_5747" s="T310">dʼe</ta>
            <ta e="T312" id="Seg_5748" s="T311">bugurduk</ta>
            <ta e="T313" id="Seg_5749" s="T312">hübel-iː-bin</ta>
            <ta e="T314" id="Seg_5750" s="T313">en</ta>
            <ta e="T315" id="Seg_5751" s="T314">kihi-gi-tten</ta>
            <ta e="T316" id="Seg_5752" s="T315">kuttan-ɨ-ma</ta>
            <ta e="T317" id="Seg_5753" s="T316">ol</ta>
            <ta e="T318" id="Seg_5754" s="T317">munnʼak</ta>
            <ta e="T319" id="Seg_5755" s="T318">dʼon-u-n</ta>
            <ta e="T320" id="Seg_5756" s="T319">kɨrg-an</ta>
            <ta e="T321" id="Seg_5757" s="T320">baran</ta>
            <ta e="T322" id="Seg_5758" s="T321">ogo-nu</ta>
            <ta e="T323" id="Seg_5759" s="T322">ɨl-bɨt</ta>
            <ta e="T324" id="Seg_5760" s="T323">di͡e-n</ta>
            <ta e="T325" id="Seg_5761" s="T324">ikki</ta>
            <ta e="T326" id="Seg_5762" s="T325">kineːs</ta>
            <ta e="T327" id="Seg_5763" s="T326">ɨraːktaːgɨ-ta</ta>
            <ta e="T328" id="Seg_5764" s="T327">üŋ-s-en</ta>
            <ta e="T329" id="Seg_5765" s="T328">huruj-u͡ok</ta>
            <ta e="T330" id="Seg_5766" s="T329">oččogo</ta>
            <ta e="T331" id="Seg_5767" s="T330">bihigi</ta>
            <ta e="T332" id="Seg_5768" s="T331">tɨl-bɨtɨ-n</ta>
            <ta e="T333" id="Seg_5769" s="T332">ɨraːktaːgɨ</ta>
            <ta e="T334" id="Seg_5770" s="T333">bɨh-ɨ͡a</ta>
            <ta e="T335" id="Seg_5771" s="T334">hu͡og-a</ta>
            <ta e="T336" id="Seg_5772" s="T335">kini-ni</ta>
            <ta e="T337" id="Seg_5773" s="T336">huːt-t-a-t-ɨ͡a</ta>
            <ta e="T338" id="Seg_5774" s="T337">kaːjɨː-ga</ta>
            <ta e="T339" id="Seg_5775" s="T338">ɨːt-ɨ͡ag-a</ta>
            <ta e="T340" id="Seg_5776" s="T339">bihigi</ta>
            <ta e="T341" id="Seg_5777" s="T340">kini</ta>
            <ta e="T342" id="Seg_5778" s="T341">baːj-ɨ-n</ta>
            <ta e="T343" id="Seg_5779" s="T342">ɨl-ɨ͡ak-pɨt</ta>
            <ta e="T344" id="Seg_5780" s="T343">dʼi͡e-bit</ta>
            <ta e="T345" id="Seg_5781" s="T344">dʼuraːk</ta>
            <ta e="T346" id="Seg_5782" s="T345">kini͡eh-e</ta>
            <ta e="T347" id="Seg_5783" s="T346">kihi͡e-ke</ta>
            <ta e="T348" id="Seg_5784" s="T347">kütüreː-tek-ke</ta>
            <ta e="T349" id="Seg_5785" s="T348">kuhagan</ta>
            <ta e="T350" id="Seg_5786" s="T349">bu͡ol-aːraj</ta>
            <ta e="T351" id="Seg_5787" s="T350">diː-r</ta>
            <ta e="T352" id="Seg_5788" s="T351">haːmaj</ta>
            <ta e="T353" id="Seg_5789" s="T352">kineːh-e</ta>
            <ta e="T354" id="Seg_5790" s="T353">tɨ͡a</ta>
            <ta e="T355" id="Seg_5791" s="T354">kineːh-i-n</ta>
            <ta e="T356" id="Seg_5792" s="T355">ɨgɨr-bɨt</ta>
            <ta e="T357" id="Seg_5793" s="T356">bu͡ol-lar</ta>
            <ta e="T358" id="Seg_5794" s="T357">üs</ta>
            <ta e="T359" id="Seg_5795" s="T358">kineːs</ta>
            <ta e="T360" id="Seg_5796" s="T359">itegel-leːk</ta>
            <ta e="T361" id="Seg_5797" s="T360">bu͡ol-u͡ok</ta>
            <ta e="T362" id="Seg_5798" s="T361">e-t-e</ta>
            <ta e="T363" id="Seg_5799" s="T362">dʼuraːk</ta>
            <ta e="T364" id="Seg_5800" s="T363">kineːh-e</ta>
            <ta e="T365" id="Seg_5801" s="T364">kihi-te</ta>
            <ta e="T366" id="Seg_5802" s="T365">ɨːt-ɨ͡a-m</ta>
            <ta e="T367" id="Seg_5803" s="T366">tɨ͡a-lar</ta>
            <ta e="T368" id="Seg_5804" s="T367">kineːs-teri-ger</ta>
            <ta e="T369" id="Seg_5805" s="T368">üs</ta>
            <ta e="T370" id="Seg_5806" s="T369">konug-u-nan</ta>
            <ta e="T371" id="Seg_5807" s="T370">kel-i͡e</ta>
            <ta e="T372" id="Seg_5808" s="T371">höbül-e-m-me-teg-ine</ta>
            <ta e="T373" id="Seg_5809" s="T372">beje-ti-n</ta>
            <ta e="T374" id="Seg_5810" s="T373">ölör-ü-ll-ü͡ö</ta>
            <ta e="T375" id="Seg_5811" s="T374">utar-s-ɨ͡a</ta>
            <ta e="T376" id="Seg_5812" s="T375">hu͡og-a</ta>
            <ta e="T377" id="Seg_5813" s="T376">diː-r</ta>
            <ta e="T378" id="Seg_5814" s="T377">hɨrga-laːk</ta>
            <ta e="T379" id="Seg_5815" s="T378">kihi-ni</ta>
            <ta e="T380" id="Seg_5816" s="T379">ɨːt-t-a</ta>
            <ta e="T381" id="Seg_5817" s="T380">tɨ͡a</ta>
            <ta e="T382" id="Seg_5818" s="T381">kineːh-i-n</ta>
            <ta e="T383" id="Seg_5819" s="T382">ɨgɨr-tar-a</ta>
            <ta e="T384" id="Seg_5820" s="T383">üh-üs</ta>
            <ta e="T385" id="Seg_5821" s="T384">kün-ü-ger</ta>
            <ta e="T386" id="Seg_5822" s="T385">tɨ͡a</ta>
            <ta e="T387" id="Seg_5823" s="T386">kineːh-e</ta>
            <ta e="T388" id="Seg_5824" s="T387">kel-er</ta>
            <ta e="T389" id="Seg_5825" s="T388">oguru͡o</ta>
            <ta e="T390" id="Seg_5826" s="T389">taŋas-taːk</ta>
            <ta e="T391" id="Seg_5827" s="T390">üs</ta>
            <ta e="T392" id="Seg_5828" s="T391">kas</ta>
            <ta e="T393" id="Seg_5829" s="T392">kün</ta>
            <ta e="T394" id="Seg_5830" s="T393">argi-lɨː-l-lar</ta>
            <ta e="T395" id="Seg_5831" s="T394">dʼuraːk</ta>
            <ta e="T396" id="Seg_5832" s="T395">kineːh-e</ta>
            <ta e="T397" id="Seg_5833" s="T396">kepsiː-r</ta>
            <ta e="T398" id="Seg_5834" s="T397">haːmaj</ta>
            <ta e="T399" id="Seg_5835" s="T398">kihi-te</ta>
            <ta e="T400" id="Seg_5836" s="T399">ɨ͡arɨː-ttan</ta>
            <ta e="T401" id="Seg_5837" s="T400">öl-büt</ta>
            <ta e="T402" id="Seg_5838" s="T401">dʼon-ton</ta>
            <ta e="T403" id="Seg_5839" s="T402">biːr</ta>
            <ta e="T404" id="Seg_5840" s="T403">ogo-nu</ta>
            <ta e="T405" id="Seg_5841" s="T404">ɨl-bɨt</ta>
            <ta e="T406" id="Seg_5842" s="T405">o-nu</ta>
            <ta e="T407" id="Seg_5843" s="T406">ɨ͡arɨː</ta>
            <ta e="T408" id="Seg_5844" s="T407">batɨh-ɨ͡a</ta>
            <ta e="T409" id="Seg_5845" s="T408">bihigi</ta>
            <ta e="T410" id="Seg_5846" s="T409">ɨraːktaːgɨ-ga</ta>
            <ta e="T411" id="Seg_5847" s="T410">üŋ-s-ü͡ögüŋ</ta>
            <ta e="T412" id="Seg_5848" s="T411">üh-ü͡ön</ta>
            <ta e="T413" id="Seg_5849" s="T412">huruj-u͡oguŋ</ta>
            <ta e="T414" id="Seg_5850" s="T413">tɨ͡a</ta>
            <ta e="T415" id="Seg_5851" s="T414">kineːh-e</ta>
            <ta e="T416" id="Seg_5852" s="T415">duːmaj-daː-bɨt</ta>
            <ta e="T417" id="Seg_5853" s="T416">dʼi͡e-bit</ta>
            <ta e="T418" id="Seg_5854" s="T417">huːt-tuː-r-u-n</ta>
            <ta e="T419" id="Seg_5855" s="T418">huːt-taː-mɨja</ta>
            <ta e="T420" id="Seg_5856" s="T419">ol</ta>
            <ta e="T421" id="Seg_5857" s="T420">ereːri</ta>
            <ta e="T422" id="Seg_5858" s="T421">kütürüː-r</ta>
            <ta e="T423" id="Seg_5859" s="T422">hür-e</ta>
            <ta e="T424" id="Seg_5860" s="T423">bert</ta>
            <ta e="T425" id="Seg_5861" s="T424">dʼon-u</ta>
            <ta e="T426" id="Seg_5862" s="T425">kej-bit</ta>
            <ta e="T427" id="Seg_5863" s="T426">di͡e-n</ta>
            <ta e="T428" id="Seg_5864" s="T427">tulaːjag-i</ta>
            <ta e="T429" id="Seg_5865" s="T428">ɨl-bɨt-a</ta>
            <ta e="T430" id="Seg_5866" s="T429">tu͡ok</ta>
            <ta e="T431" id="Seg_5867" s="T430">buruj-a=j</ta>
            <ta e="T432" id="Seg_5868" s="T431">huːk-ka</ta>
            <ta e="T433" id="Seg_5869" s="T432">bi͡er-i͡ek-piti-n</ta>
            <ta e="T434" id="Seg_5870" s="T433">dʼi͡e-bit</ta>
            <ta e="T435" id="Seg_5871" s="T434">dʼuraːk</ta>
            <ta e="T436" id="Seg_5872" s="T435">kineːh-e</ta>
            <ta e="T437" id="Seg_5873" s="T436">en</ta>
            <ta e="T438" id="Seg_5874" s="T437">kömüsk-üː-gün</ta>
            <ta e="T439" id="Seg_5875" s="T438">du͡o</ta>
            <ta e="T440" id="Seg_5876" s="T439">ol</ta>
            <ta e="T441" id="Seg_5877" s="T440">kihi-ni</ta>
            <ta e="T442" id="Seg_5878" s="T441">oččogo</ta>
            <ta e="T443" id="Seg_5879" s="T442">bihigi</ta>
            <ta e="T444" id="Seg_5880" s="T443">enʼigi-n</ta>
            <ta e="T445" id="Seg_5881" s="T444">kej-e-bit</ta>
            <ta e="T446" id="Seg_5882" s="T445">baːj-gɨ-n</ta>
            <ta e="T447" id="Seg_5883" s="T446">ɨl-ɨ͡ak-pɨt</ta>
            <ta e="T448" id="Seg_5884" s="T447">diː-r</ta>
            <ta e="T449" id="Seg_5885" s="T448">tɨ͡a</ta>
            <ta e="T450" id="Seg_5886" s="T449">kineːh-e</ta>
            <ta e="T451" id="Seg_5887" s="T450">kuttam-mɨt</ta>
            <ta e="T452" id="Seg_5888" s="T451">ölör-ü͡ök-süt</ta>
            <ta e="T453" id="Seg_5889" s="T452">dʼon</ta>
            <ta e="T454" id="Seg_5890" s="T453">ikk-i͡en</ta>
            <ta e="T455" id="Seg_5891" s="T454">dʼe</ta>
            <ta e="T456" id="Seg_5892" s="T455">kajtak</ta>
            <ta e="T457" id="Seg_5893" s="T456">hübel-iː-git</ta>
            <ta e="T458" id="Seg_5894" s="T457">dʼi͡e-bit</ta>
            <ta e="T459" id="Seg_5895" s="T458">dʼuraːk</ta>
            <ta e="T460" id="Seg_5896" s="T459">kineːh-e</ta>
            <ta e="T461" id="Seg_5897" s="T460">min</ta>
            <ta e="T462" id="Seg_5898" s="T461">tojon</ta>
            <ta e="T463" id="Seg_5899" s="T462">hübehit</ta>
            <ta e="T464" id="Seg_5900" s="T463">bu͡ol-a-bɨn</ta>
            <ta e="T465" id="Seg_5901" s="T464">üŋ-s-ü͡ögüŋ</ta>
            <ta e="T466" id="Seg_5902" s="T465">ɨraːktaːgɨ-ga</ta>
            <ta e="T467" id="Seg_5903" s="T466">bu</ta>
            <ta e="T468" id="Seg_5904" s="T467">kohuːn</ta>
            <ta e="T469" id="Seg_5905" s="T468">haːmaj</ta>
            <ta e="T470" id="Seg_5906" s="T469">munnʼag-ɨ-n</ta>
            <ta e="T471" id="Seg_5907" s="T470">kej-en</ta>
            <ta e="T472" id="Seg_5908" s="T471">baran</ta>
            <ta e="T473" id="Seg_5909" s="T472">ogo-nu</ta>
            <ta e="T474" id="Seg_5910" s="T473">ɨl-bɨt</ta>
            <ta e="T475" id="Seg_5911" s="T474">di͡e-n</ta>
            <ta e="T476" id="Seg_5912" s="T475">huruj-u͡ok-put</ta>
            <ta e="T477" id="Seg_5913" s="T476">min</ta>
            <ta e="T478" id="Seg_5914" s="T477">kihi-ler-i-m</ta>
            <ta e="T479" id="Seg_5915" s="T478">bar-an</ta>
            <ta e="T480" id="Seg_5916" s="T479">öl-büt</ta>
            <ta e="T481" id="Seg_5917" s="T480">dʼon-u</ta>
            <ta e="T482" id="Seg_5918" s="T481">ɨt-ɨ͡al-ɨ͡ak-tara</ta>
            <ta e="T483" id="Seg_5919" s="T482">komissija</ta>
            <ta e="T484" id="Seg_5920" s="T483">kel-leg-ine</ta>
            <ta e="T485" id="Seg_5921" s="T484">bul-an</ta>
            <ta e="T486" id="Seg_5922" s="T485">ɨl-ɨ͡a</ta>
            <ta e="T487" id="Seg_5923" s="T486">hu͡og-a</ta>
            <ta e="T488" id="Seg_5924" s="T487">kim</ta>
            <ta e="T489" id="Seg_5925" s="T488">taːj-ɨ͡a=j</ta>
            <ta e="T490" id="Seg_5926" s="T489">diː-r</ta>
            <ta e="T491" id="Seg_5927" s="T490">u͡on-ča</ta>
            <ta e="T492" id="Seg_5928" s="T491">kihi</ta>
            <ta e="T493" id="Seg_5929" s="T492">bar-an</ta>
            <ta e="T494" id="Seg_5930" s="T493">bɨlɨr</ta>
            <ta e="T495" id="Seg_5931" s="T494">öl-büt</ta>
            <ta e="T496" id="Seg_5932" s="T495">dʼon-u</ta>
            <ta e="T497" id="Seg_5933" s="T496">ɨt-ɨ͡alɨː-l-lar</ta>
            <ta e="T498" id="Seg_5934" s="T497">huruj-an</ta>
            <ta e="T499" id="Seg_5935" s="T498">keːs-ti-ler</ta>
            <ta e="T500" id="Seg_5936" s="T499">ogorduk</ta>
            <ta e="T501" id="Seg_5937" s="T500">haːmaj</ta>
            <ta e="T502" id="Seg_5938" s="T501">kineːh-i-n</ta>
            <ta e="T503" id="Seg_5939" s="T502">ɨːt-al-lar</ta>
            <ta e="T504" id="Seg_5940" s="T503">dʼuraːk</ta>
            <ta e="T505" id="Seg_5941" s="T504">kineːh-i-niːn</ta>
            <ta e="T506" id="Seg_5942" s="T505">ɨraːktaːgɨ</ta>
            <ta e="T507" id="Seg_5943" s="T506">aːg-an</ta>
            <ta e="T508" id="Seg_5944" s="T507">kör-ör</ta>
            <ta e="T509" id="Seg_5945" s="T508">u͡on-ča</ta>
            <ta e="T510" id="Seg_5946" s="T509">dʼi͡e</ta>
            <ta e="T511" id="Seg_5947" s="T510">dʼon-u</ta>
            <ta e="T512" id="Seg_5948" s="T511">kej-bit</ta>
            <ta e="T513" id="Seg_5949" s="T512">ogo-nu</ta>
            <ta e="T514" id="Seg_5950" s="T513">u͡or-but</ta>
            <ta e="T515" id="Seg_5951" s="T514">kihi</ta>
            <ta e="T516" id="Seg_5952" s="T515">ɨraːktaːgɨ</ta>
            <ta e="T517" id="Seg_5953" s="T516">barɨ-larɨ-ttan</ta>
            <ta e="T518" id="Seg_5954" s="T517">ɨjɨt-ar</ta>
            <ta e="T519" id="Seg_5955" s="T518">kirdik</ta>
            <ta e="T520" id="Seg_5956" s="T519">du͡o</ta>
            <ta e="T521" id="Seg_5957" s="T520">min</ta>
            <ta e="T522" id="Seg_5958" s="T521">kini-ni</ta>
            <ta e="T523" id="Seg_5959" s="T522">huːt-t-u͡o-m</ta>
            <ta e="T524" id="Seg_5960" s="T523">ehigi</ta>
            <ta e="T525" id="Seg_5961" s="T524">ist-er-giti-ger</ta>
            <ta e="T526" id="Seg_5962" s="T525">haːmaj</ta>
            <ta e="T527" id="Seg_5963" s="T526">kineːh-e</ta>
            <ta e="T528" id="Seg_5964" s="T527">kihi͡e-ke</ta>
            <ta e="T529" id="Seg_5965" s="T528">hugahaː-bat</ta>
            <ta e="T530" id="Seg_5966" s="T529">kihi</ta>
            <ta e="T531" id="Seg_5967" s="T530">oŋor-t-o</ta>
            <ta e="T532" id="Seg_5968" s="T531">bu͡olu͡o</ta>
            <ta e="T533" id="Seg_5969" s="T532">diː-r</ta>
            <ta e="T534" id="Seg_5970" s="T533">ogonnʼor-go</ta>
            <ta e="T535" id="Seg_5971" s="T534">albun</ta>
            <ta e="T536" id="Seg_5972" s="T535">huruk</ta>
            <ta e="T537" id="Seg_5973" s="T536">ɨːt-al-lar</ta>
            <ta e="T538" id="Seg_5974" s="T537">kineːh-e</ta>
            <ta e="T539" id="Seg_5975" s="T538">če</ta>
            <ta e="T540" id="Seg_5976" s="T539">nolu͡og-u-n</ta>
            <ta e="T541" id="Seg_5977" s="T540">karčɨ-tɨ-n</ta>
            <ta e="T542" id="Seg_5978" s="T541">ɨl-a</ta>
            <ta e="T543" id="Seg_5979" s="T542">kel-bekteː-tin</ta>
            <ta e="T544" id="Seg_5980" s="T543">bert</ta>
            <ta e="T545" id="Seg_5981" s="T544">muŋ</ta>
            <ta e="T546" id="Seg_5982" s="T545">türgen-nik</ta>
            <ta e="T547" id="Seg_5983" s="T546">di͡e-n</ta>
            <ta e="T548" id="Seg_5984" s="T547">huruj-ar</ta>
            <ta e="T549" id="Seg_5985" s="T548">ildʼit</ta>
            <ta e="T550" id="Seg_5986" s="T549">kihi</ta>
            <ta e="T551" id="Seg_5987" s="T550">ti͡erd-er</ta>
            <ta e="T552" id="Seg_5988" s="T551">hurug-u</ta>
            <ta e="T553" id="Seg_5989" s="T552">ogonnʼor</ta>
            <ta e="T554" id="Seg_5990" s="T553">muŋ</ta>
            <ta e="T555" id="Seg_5991" s="T554">ütü͡ö</ta>
            <ta e="T556" id="Seg_5992" s="T555">taba-lar-ɨ-nan</ta>
            <ta e="T557" id="Seg_5993" s="T556">kötüt-en</ta>
            <ta e="T558" id="Seg_5994" s="T557">kel-er</ta>
            <ta e="T559" id="Seg_5995" s="T558">taba-tɨ-n</ta>
            <ta e="T560" id="Seg_5996" s="T559">baːj-bɨt</ta>
            <ta e="T561" id="Seg_5997" s="T560">da</ta>
            <ta e="T562" id="Seg_5998" s="T561">köt-ön</ta>
            <ta e="T563" id="Seg_5999" s="T562">tüh-er</ta>
            <ta e="T564" id="Seg_6000" s="T563">ostoːl-u</ta>
            <ta e="T565" id="Seg_6001" s="T564">tögürüččü</ta>
            <ta e="T566" id="Seg_6002" s="T565">tojon</ta>
            <ta e="T567" id="Seg_6003" s="T566">bögö</ta>
            <ta e="T568" id="Seg_6004" s="T567">ɨraːktaːgɨ</ta>
            <ta e="T569" id="Seg_6005" s="T568">čaːj</ta>
            <ta e="T570" id="Seg_6006" s="T569">da</ta>
            <ta e="T571" id="Seg_6007" s="T570">ih-e-r-petek</ta>
            <ta e="T572" id="Seg_6008" s="T571">kaːn-a</ta>
            <ta e="T573" id="Seg_6009" s="T572">kuhagan</ta>
            <ta e="T574" id="Seg_6010" s="T573">čipčik</ta>
            <ta e="T575" id="Seg_6011" s="T574">atɨn</ta>
            <ta e="T576" id="Seg_6012" s="T575">dʼe</ta>
            <ta e="T577" id="Seg_6013" s="T576">kepseː</ta>
            <ta e="T578" id="Seg_6014" s="T577">ös-kü-n</ta>
            <ta e="T579" id="Seg_6015" s="T578">munnʼak</ta>
            <ta e="T580" id="Seg_6016" s="T579">dʼon-u</ta>
            <ta e="T581" id="Seg_6017" s="T580">togo</ta>
            <ta e="T582" id="Seg_6018" s="T581">kɨr-tɨ-ŋ</ta>
            <ta e="T583" id="Seg_6019" s="T582">tu͡ok</ta>
            <ta e="T584" id="Seg_6020" s="T583">munnʼag-ɨ-n</ta>
            <ta e="T585" id="Seg_6021" s="T584">hap-tar-a-gɨn</ta>
            <ta e="T586" id="Seg_6022" s="T585">kihi-ni</ta>
            <ta e="T587" id="Seg_6023" s="T586">kɨtta</ta>
            <ta e="T588" id="Seg_6024" s="T587">mökkü͡ön-ü-m</ta>
            <ta e="T589" id="Seg_6025" s="T588">da</ta>
            <ta e="T590" id="Seg_6026" s="T589">hu͡ok</ta>
            <ta e="T591" id="Seg_6027" s="T590">ɨraːktaːgɨ</ta>
            <ta e="T592" id="Seg_6028" s="T591">kineːs-ter</ta>
            <ta e="T593" id="Seg_6029" s="T592">huruk-tarɨ-n</ta>
            <ta e="T594" id="Seg_6030" s="T593">kör-dör-ör</ta>
            <ta e="T595" id="Seg_6031" s="T594">ehiːl-gi</ta>
            <ta e="T596" id="Seg_6032" s="T595">uː</ta>
            <ta e="T597" id="Seg_6033" s="T596">kel-i͡e-r</ta>
            <ta e="T598" id="Seg_6034" s="T597">di͡eri</ta>
            <ta e="T599" id="Seg_6035" s="T598">kaːjɨː-ga</ta>
            <ta e="T600" id="Seg_6036" s="T599">hɨt-ɨ͡a-ŋ</ta>
            <ta e="T601" id="Seg_6037" s="T600">miːgin-neːger</ta>
            <ta e="T602" id="Seg_6038" s="T601">ulakan</ta>
            <ta e="T603" id="Seg_6039" s="T602">ɨraːktaːgɨ-ga</ta>
            <ta e="T604" id="Seg_6040" s="T603">ɨːt-ɨ͡a-m</ta>
            <ta e="T605" id="Seg_6041" s="T604">ogonnʼor-u</ta>
            <ta e="T606" id="Seg_6042" s="T605">türme-ge</ta>
            <ta e="T607" id="Seg_6043" s="T606">kaːj-an</ta>
            <ta e="T608" id="Seg_6044" s="T607">keːh-el-ler</ta>
            <ta e="T609" id="Seg_6045" s="T608">üs</ta>
            <ta e="T610" id="Seg_6046" s="T609">kineːs</ta>
            <ta e="T611" id="Seg_6047" s="T610">baːj-ɨ-n</ta>
            <ta e="T612" id="Seg_6048" s="T611">üllest-i͡ek-tere</ta>
            <ta e="T613" id="Seg_6049" s="T612">emeːksin</ta>
            <ta e="T614" id="Seg_6050" s="T613">köh-ön</ta>
            <ta e="T615" id="Seg_6051" s="T614">kel-er</ta>
            <ta e="T616" id="Seg_6052" s="T615">ogonnʼor-o</ta>
            <ta e="T617" id="Seg_6053" s="T616">kaččaga</ta>
            <ta e="T618" id="Seg_6054" s="T617">da</ta>
            <ta e="T619" id="Seg_6055" s="T618">hu͡ok</ta>
            <ta e="T620" id="Seg_6056" s="T619">türme-ge</ta>
            <ta e="T621" id="Seg_6057" s="T620">hɨt-ar</ta>
            <ta e="T622" id="Seg_6058" s="T621">ɨraːktaːgɨ</ta>
            <ta e="T623" id="Seg_6059" s="T622">emeːksin-i</ta>
            <ta e="T624" id="Seg_6060" s="T623">üːr-en</ta>
            <ta e="T625" id="Seg_6061" s="T624">tahaːr-ar</ta>
            <ta e="T626" id="Seg_6062" s="T625">üs</ta>
            <ta e="T627" id="Seg_6063" s="T626">kineːs</ta>
            <ta e="T628" id="Seg_6064" s="T627">baːj-ɨ-n</ta>
            <ta e="T629" id="Seg_6065" s="T628">hab-a</ta>
            <ta e="T630" id="Seg_6066" s="T629">tüh-en</ta>
            <ta e="T631" id="Seg_6067" s="T630">üllest-en</ta>
            <ta e="T632" id="Seg_6068" s="T631">ɨl-al-lar</ta>
            <ta e="T633" id="Seg_6069" s="T632">biːr</ta>
            <ta e="T634" id="Seg_6070" s="T633">öldʼüːn-ü</ta>
            <ta e="T635" id="Seg_6071" s="T634">kaːl-lar-al-lar</ta>
            <ta e="T636" id="Seg_6072" s="T635">kɨhɨn-a</ta>
            <ta e="T637" id="Seg_6073" s="T636">baran-an</ta>
            <ta e="T638" id="Seg_6074" s="T637">poroku͡ot</ta>
            <ta e="T639" id="Seg_6075" s="T638">kel-eːkteː-bit</ta>
            <ta e="T640" id="Seg_6076" s="T639">üːhe-tten</ta>
            <ta e="T641" id="Seg_6077" s="T640">dʼe</ta>
            <ta e="T642" id="Seg_6078" s="T641">kör</ta>
            <ta e="T643" id="Seg_6079" s="T642">er-gi-n</ta>
            <ta e="T644" id="Seg_6080" s="T643">diː-l-ler</ta>
            <ta e="T645" id="Seg_6081" s="T644">ogonnʼor</ta>
            <ta e="T646" id="Seg_6082" s="T645">karag-a</ta>
            <ta e="T647" id="Seg_6083" s="T646">iːn</ta>
            <ta e="T648" id="Seg_6084" s="T647">ih-i-ger</ta>
            <ta e="T649" id="Seg_6085" s="T648">kiːr-bit</ta>
            <ta e="T650" id="Seg_6086" s="T649">ah-a-p-patak-tar</ta>
            <ta e="T651" id="Seg_6087" s="T650">okt-on</ta>
            <ta e="T652" id="Seg_6088" s="T651">kaːl-bɨt</ta>
            <ta e="T653" id="Seg_6089" s="T652">poroku͡ot</ta>
            <ta e="T654" id="Seg_6090" s="T653">ih-i-ger</ta>
            <ta e="T655" id="Seg_6091" s="T654">bɨrak-pɨt-tar</ta>
            <ta e="T656" id="Seg_6092" s="T655">nöŋü͡ö</ta>
            <ta e="T657" id="Seg_6093" s="T656">ɨraːktaːgɨ-ga</ta>
            <ta e="T658" id="Seg_6094" s="T657">illʼ-el-ler</ta>
            <ta e="T659" id="Seg_6095" s="T658">nosilka-nnan</ta>
            <ta e="T660" id="Seg_6096" s="T659">killer-el-ler</ta>
            <ta e="T661" id="Seg_6097" s="T660">öl-ön</ta>
            <ta e="T662" id="Seg_6098" s="T661">er-er</ta>
            <ta e="T663" id="Seg_6099" s="T662">kihi-ni</ta>
            <ta e="T664" id="Seg_6100" s="T663">kaja</ta>
            <ta e="T665" id="Seg_6101" s="T664">kajtak</ta>
            <ta e="T666" id="Seg_6102" s="T665">huːt-t-u͡o-m=uj</ta>
            <ta e="T667" id="Seg_6103" s="T666">öl-büt</ta>
            <ta e="T668" id="Seg_6104" s="T667">kihi-ni</ta>
            <ta e="T669" id="Seg_6105" s="T668">ildʼ-i-ŋ</ta>
            <ta e="T670" id="Seg_6106" s="T669">bolʼnica-ga</ta>
            <ta e="T671" id="Seg_6107" s="T670">ah-a-t-tɨnnar</ta>
            <ta e="T672" id="Seg_6108" s="T671">kör-dünner</ta>
            <ta e="T673" id="Seg_6109" s="T672">du͡oktuːr-dar</ta>
            <ta e="T674" id="Seg_6110" s="T673">emtiː-l-ler</ta>
            <ta e="T675" id="Seg_6111" s="T674">erge</ta>
            <ta e="T676" id="Seg_6112" s="T675">beje-te</ta>
            <ta e="T677" id="Seg_6113" s="T676">bu͡ol-ar</ta>
            <ta e="T678" id="Seg_6114" s="T677">du͡oktuːr-dar</ta>
            <ta e="T679" id="Seg_6115" s="T678">ɨjɨt-al-lar</ta>
            <ta e="T680" id="Seg_6116" s="T679">buruj-u-ŋ</ta>
            <ta e="T681" id="Seg_6117" s="T680">duː</ta>
            <ta e="T682" id="Seg_6118" s="T681">kütürüː-l-ler</ta>
            <ta e="T683" id="Seg_6119" s="T682">duː</ta>
            <ta e="T684" id="Seg_6120" s="T683">ogonnʼor</ta>
            <ta e="T685" id="Seg_6121" s="T684">kepsiː-r</ta>
            <ta e="T686" id="Seg_6122" s="T685">barɨ-tɨ-n</ta>
            <ta e="T687" id="Seg_6123" s="T686">du͡oktuːr-dar</ta>
            <ta e="T688" id="Seg_6124" s="T687">kaːn</ta>
            <ta e="T689" id="Seg_6125" s="T688">ɨl-al-lar</ta>
            <ta e="T690" id="Seg_6126" s="T689">buruj-daːk</ta>
            <ta e="T691" id="Seg_6127" s="T690">bu͡ol-lak-kɨna</ta>
            <ta e="T692" id="Seg_6128" s="T691">kaːn-ɨ-ŋ</ta>
            <ta e="T693" id="Seg_6129" s="T692">atɨn</ta>
            <ta e="T694" id="Seg_6130" s="T693">bu͡ol-u͡o</ta>
            <ta e="T695" id="Seg_6131" s="T694">diː-l-ler</ta>
            <ta e="T696" id="Seg_6132" s="T695">biːr-keːn</ta>
            <ta e="T697" id="Seg_6133" s="T696">da</ta>
            <ta e="T698" id="Seg_6134" s="T697">kaːpelʼka</ta>
            <ta e="T699" id="Seg_6135" s="T698">buruj-a</ta>
            <ta e="T700" id="Seg_6136" s="T699">hu͡ok</ta>
            <ta e="T701" id="Seg_6137" s="T700">e-bit</ta>
            <ta e="T702" id="Seg_6138" s="T701">huruk</ta>
            <ta e="T703" id="Seg_6139" s="T702">bi͡er-el-ler</ta>
            <ta e="T704" id="Seg_6140" s="T703">ma-nɨ</ta>
            <ta e="T705" id="Seg_6141" s="T704">huːk-ka</ta>
            <ta e="T706" id="Seg_6142" s="T705">pireːme</ta>
            <ta e="T707" id="Seg_6143" s="T706">bi͡er-eːr</ta>
            <ta e="T708" id="Seg_6144" s="T707">kineːs-ter</ta>
            <ta e="T709" id="Seg_6145" s="T708">kaːn-narɨ-n</ta>
            <ta e="T710" id="Seg_6146" s="T709">emi͡e</ta>
            <ta e="T711" id="Seg_6147" s="T710">ɨl-ɨ͡ak-pɨt</ta>
            <ta e="T712" id="Seg_6148" s="T711">diː-l-ler</ta>
            <ta e="T713" id="Seg_6149" s="T712">huːt</ta>
            <ta e="T714" id="Seg_6150" s="T713">bu͡ol-ar</ta>
            <ta e="T715" id="Seg_6151" s="T714">kineːs-ter</ta>
            <ta e="T716" id="Seg_6152" s="T715">maŋnajgɨ</ta>
            <ta e="T717" id="Seg_6153" s="T716">ɨraːktaːgɨ</ta>
            <ta e="T718" id="Seg_6154" s="T717">elbek</ta>
            <ta e="T719" id="Seg_6155" s="T718">dʼon</ta>
            <ta e="T720" id="Seg_6156" s="T719">munnʼus-t-u-but</ta>
            <ta e="T721" id="Seg_6157" s="T720">ɨraːktaːgɨ</ta>
            <ta e="T722" id="Seg_6158" s="T721">et-er</ta>
            <ta e="T723" id="Seg_6159" s="T722">öh-ü-n-tɨl-ɨ-n</ta>
            <ta e="T724" id="Seg_6160" s="T723">ist-i͡ek-ke</ta>
            <ta e="T725" id="Seg_6161" s="T724">diː-r</ta>
            <ta e="T726" id="Seg_6162" s="T725">ogonnʼor</ta>
            <ta e="T727" id="Seg_6163" s="T726">kepsiː-r</ta>
            <ta e="T728" id="Seg_6164" s="T727">min</ta>
            <ta e="T729" id="Seg_6165" s="T728">daːse</ta>
            <ta e="T730" id="Seg_6166" s="T729">kihi-ni</ta>
            <ta e="T731" id="Seg_6167" s="T730">kɨtta</ta>
            <ta e="T732" id="Seg_6168" s="T731">ölöss-ü-betek</ta>
            <ta e="T733" id="Seg_6169" s="T732">kihi-bin</ta>
            <ta e="T734" id="Seg_6170" s="T733">ɨsparaːpka-tɨ-n</ta>
            <ta e="T735" id="Seg_6171" s="T734">bi͡er-er</ta>
            <ta e="T736" id="Seg_6172" s="T735">bu</ta>
            <ta e="T737" id="Seg_6173" s="T736">kihi</ta>
            <ta e="T738" id="Seg_6174" s="T737">kaːpelʼka</ta>
            <ta e="T739" id="Seg_6175" s="T738">da</ta>
            <ta e="T740" id="Seg_6176" s="T739">buruj-a</ta>
            <ta e="T741" id="Seg_6177" s="T740">hu͡ok</ta>
            <ta e="T742" id="Seg_6178" s="T741">kineːs-ter</ta>
            <ta e="T743" id="Seg_6179" s="T742">kaːn-narɨ-n</ta>
            <ta e="T744" id="Seg_6180" s="T743">ɨl-al-lar</ta>
            <ta e="T745" id="Seg_6181" s="T744">haːmaj</ta>
            <ta e="T746" id="Seg_6182" s="T745">kohuːn-u-n</ta>
            <ta e="T747" id="Seg_6183" s="T746">kaːn-a</ta>
            <ta e="T748" id="Seg_6184" s="T747">ile</ta>
            <ta e="T749" id="Seg_6185" s="T748">atɨn</ta>
            <ta e="T750" id="Seg_6186" s="T749">kineːs-ter</ta>
            <ta e="T751" id="Seg_6187" s="T750">gi͡et-tere</ta>
            <ta e="T752" id="Seg_6188" s="T751">čiŋke</ta>
            <ta e="T753" id="Seg_6189" s="T752">atɨn</ta>
            <ta e="T754" id="Seg_6190" s="T753">ɨraːktaːgɨ</ta>
            <ta e="T755" id="Seg_6191" s="T754">dʼe</ta>
            <ta e="T756" id="Seg_6192" s="T755">ɨgaj-ar</ta>
            <ta e="T757" id="Seg_6193" s="T756">kim</ta>
            <ta e="T758" id="Seg_6194" s="T757">hübeleː-bit-e=j</ta>
            <ta e="T759" id="Seg_6195" s="T758">kepseː-ŋ</ta>
            <ta e="T760" id="Seg_6196" s="T759">tɨ͡a</ta>
            <ta e="T761" id="Seg_6197" s="T760">kineːh-e</ta>
            <ta e="T762" id="Seg_6198" s="T761">kepsiː-r</ta>
            <ta e="T763" id="Seg_6199" s="T762">dʼe</ta>
            <ta e="T764" id="Seg_6200" s="T763">kirdig-e</ta>
            <ta e="T765" id="Seg_6201" s="T764">bihigi</ta>
            <ta e="T766" id="Seg_6202" s="T765">buruj-daːk-pɨt</ta>
            <ta e="T767" id="Seg_6203" s="T766">haːmaj</ta>
            <ta e="T768" id="Seg_6204" s="T767">ulakan</ta>
            <ta e="T769" id="Seg_6205" s="T768">buruj-daːk</ta>
            <ta e="T770" id="Seg_6206" s="T769">dʼuraːk</ta>
            <ta e="T771" id="Seg_6207" s="T770">kineːh-e</ta>
            <ta e="T772" id="Seg_6208" s="T771">minʼigi-n</ta>
            <ta e="T773" id="Seg_6209" s="T772">kej-i͡ek</ta>
            <ta e="T774" id="Seg_6210" s="T773">baːl-lara</ta>
            <ta e="T775" id="Seg_6211" s="T774">kuttan-an</ta>
            <ta e="T777" id="Seg_6212" s="T776">bi͡er-bit-i-m</ta>
            <ta e="T778" id="Seg_6213" s="T777">ɨraːktaːgɨ</ta>
            <ta e="T779" id="Seg_6214" s="T778">dʼe</ta>
            <ta e="T780" id="Seg_6215" s="T779">ogonnʼor</ta>
            <ta e="T781" id="Seg_6216" s="T780">en</ta>
            <ta e="T782" id="Seg_6217" s="T781">buruj-u-n</ta>
            <ta e="T783" id="Seg_6218" s="T782">hu͡ok</ta>
            <ta e="T784" id="Seg_6219" s="T783">e-bit</ta>
            <ta e="T785" id="Seg_6220" s="T784">kaːjɨː-ga</ta>
            <ta e="T786" id="Seg_6221" s="T785">ejigi-n</ta>
            <ta e="T787" id="Seg_6222" s="T786">huːt-taː-bɨt</ta>
            <ta e="T788" id="Seg_6223" s="T787">ɨraːktaːgɨ-nɨ</ta>
            <ta e="T789" id="Seg_6224" s="T788">olord-u͡o-m</ta>
            <ta e="T790" id="Seg_6225" s="T789">en</ta>
            <ta e="T791" id="Seg_6226" s="T790">gini</ta>
            <ta e="T792" id="Seg_6227" s="T791">onnu-gar</ta>
            <ta e="T793" id="Seg_6228" s="T792">ɨraːktaːgɨ-nnan</ta>
            <ta e="T794" id="Seg_6229" s="T793">bar-ɨ͡a-ŋ</ta>
            <ta e="T795" id="Seg_6230" s="T794">kineːs-ter-i</ta>
            <ta e="T796" id="Seg_6231" s="T795">onno</ta>
            <ta e="T797" id="Seg_6232" s="T796">tij-en</ta>
            <ta e="T798" id="Seg_6233" s="T797">huːt-t-aːr</ta>
            <ta e="T799" id="Seg_6234" s="T798">di͡e-bit</ta>
            <ta e="T800" id="Seg_6235" s="T799">kamnaː-n</ta>
            <ta e="T801" id="Seg_6236" s="T800">kaːl-al-lar</ta>
            <ta e="T802" id="Seg_6237" s="T801">töttörü</ta>
            <ta e="T803" id="Seg_6238" s="T802">ogonnʼor</ta>
            <ta e="T804" id="Seg_6239" s="T803">olog-u-gar</ta>
            <ta e="T805" id="Seg_6240" s="T804">kel-en</ta>
            <ta e="T806" id="Seg_6241" s="T805">kineːs</ta>
            <ta e="T807" id="Seg_6242" s="T806">bu͡ol-ar</ta>
            <ta e="T808" id="Seg_6243" s="T807">u͡ol-a</ta>
            <ta e="T809" id="Seg_6244" s="T808">ulaːt-an</ta>
            <ta e="T810" id="Seg_6245" s="T809">pireːme-pireːme</ta>
            <ta e="T811" id="Seg_6246" s="T810">bukatɨːr</ta>
            <ta e="T812" id="Seg_6247" s="T811">bu͡ol-but</ta>
            <ta e="T813" id="Seg_6248" s="T812">kini</ta>
            <ta e="T814" id="Seg_6249" s="T813">hu͡og-una</ta>
            <ta e="T815" id="Seg_6250" s="T814">ölör-öːrü</ta>
            <ta e="T816" id="Seg_6251" s="T815">gɨm-mɨt-tar</ta>
            <ta e="T817" id="Seg_6252" s="T816">aga-tɨ-gar</ta>
            <ta e="T818" id="Seg_6253" s="T817">talɨ</ta>
            <ta e="T819" id="Seg_6254" s="T818">bu͡ol-u͡o</ta>
            <ta e="T820" id="Seg_6255" s="T819">di͡e-n</ta>
            <ta e="T821" id="Seg_6256" s="T820">u͡ol</ta>
            <ta e="T822" id="Seg_6257" s="T821">biːr</ta>
            <ta e="T823" id="Seg_6258" s="T822">kihi-ni</ta>
            <ta e="T824" id="Seg_6259" s="T823">tüŋner-i</ta>
            <ta e="T825" id="Seg_6260" s="T824">oks-u-but</ta>
            <ta e="T826" id="Seg_6261" s="T825">o-nu</ta>
            <ta e="T827" id="Seg_6262" s="T826">kaːjɨː-ga</ta>
            <ta e="T828" id="Seg_6263" s="T827">olor-put-tar</ta>
            <ta e="T829" id="Seg_6264" s="T828">kineːs-teri-n</ta>
            <ta e="T830" id="Seg_6265" s="T829">dʼüːllüː-r</ta>
            <ta e="T831" id="Seg_6266" s="T830">haːmaj</ta>
            <ta e="T832" id="Seg_6267" s="T831">kineːh-i-n</ta>
            <ta e="T833" id="Seg_6268" s="T832">dʼuraːk</ta>
            <ta e="T834" id="Seg_6269" s="T833">kineːh-i-niːn</ta>
            <ta e="T835" id="Seg_6270" s="T834">türme-ge</ta>
            <ta e="T836" id="Seg_6271" s="T835">olord-or</ta>
            <ta e="T837" id="Seg_6272" s="T836">tɨ͡a</ta>
            <ta e="T838" id="Seg_6273" s="T837">kineːh-i-n</ta>
            <ta e="T839" id="Seg_6274" s="T838">kineːh-i-tten</ta>
            <ta e="T840" id="Seg_6275" s="T839">uhul-ar</ta>
            <ta e="T841" id="Seg_6276" s="T840">u͡ol-u-n</ta>
            <ta e="T842" id="Seg_6277" s="T841">haːmaj</ta>
            <ta e="T843" id="Seg_6278" s="T842">kineːh-e</ta>
            <ta e="T844" id="Seg_6279" s="T843">oŋor-or</ta>
            <ta e="T845" id="Seg_6280" s="T844">ogonnʼor</ta>
            <ta e="T846" id="Seg_6281" s="T845">itinnik</ta>
            <ta e="T847" id="Seg_6282" s="T846">kirdig-e</ta>
            <ta e="T848" id="Seg_6283" s="T847">taks-ɨ-bɨt-a</ta>
            <ta e="T849" id="Seg_6284" s="T848">ele-te</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_6285" s="T0">ɨraːktaːgɨ-LAːK-LAr</ta>
            <ta e="T2" id="Seg_6286" s="T1">küheːjin-LAːK-LAr</ta>
            <ta e="T3" id="Seg_6287" s="T2">kineːs-LAːK-LAr</ta>
            <ta e="T4" id="Seg_6288" s="T3">bu͡ol-TAK-tA</ta>
            <ta e="T5" id="Seg_6289" s="T4">bu</ta>
            <ta e="T6" id="Seg_6290" s="T5">kihi</ta>
            <ta e="T7" id="Seg_6291" s="T6">tuspa</ta>
            <ta e="T8" id="Seg_6292" s="T7">olor-Ar</ta>
            <ta e="T9" id="Seg_6293" s="T8">kosuːn</ta>
            <ta e="T10" id="Seg_6294" s="T9">agaj</ta>
            <ta e="T11" id="Seg_6295" s="T10">biːr-kAːN</ta>
            <ta e="T12" id="Seg_6296" s="T11">da</ta>
            <ta e="T13" id="Seg_6297" s="T12">ogo-tA</ta>
            <ta e="T14" id="Seg_6298" s="T13">hu͡ok</ta>
            <ta e="T15" id="Seg_6299" s="T14">dʼɨl-GA</ta>
            <ta e="T16" id="Seg_6300" s="T15">biːrde</ta>
            <ta e="T17" id="Seg_6301" s="T16">tölöː-Ar</ta>
            <ta e="T18" id="Seg_6302" s="T17">nolu͡og-tI-n</ta>
            <ta e="T19" id="Seg_6303" s="T18">bajgal</ta>
            <ta e="T20" id="Seg_6304" s="T19">kɨrɨː-tI-GAr</ta>
            <ta e="T21" id="Seg_6305" s="T20">ile</ta>
            <ta e="T22" id="Seg_6306" s="T21">mu͡ora-GA</ta>
            <ta e="T23" id="Seg_6307" s="T22">olor-Ar</ta>
            <ta e="T24" id="Seg_6308" s="T23">bajgal</ta>
            <ta e="T25" id="Seg_6309" s="T24">hɨlbak-tI-n</ta>
            <ta e="T26" id="Seg_6310" s="T25">otut-I-n-Ar</ta>
            <ta e="T27" id="Seg_6311" s="T26">ör</ta>
            <ta e="T28" id="Seg_6312" s="T27">olor-BIT</ta>
            <ta e="T29" id="Seg_6313" s="T28">kɨrɨj-I-BIT</ta>
            <ta e="T30" id="Seg_6314" s="T29">dʼɨl-tA</ta>
            <ta e="T31" id="Seg_6315" s="T30">haːs-TIj-An</ta>
            <ta e="T32" id="Seg_6316" s="T31">bar-Ar</ta>
            <ta e="T33" id="Seg_6317" s="T32">emeːksin-tI-GAr</ta>
            <ta e="T34" id="Seg_6318" s="T33">et-Ar</ta>
            <ta e="T35" id="Seg_6319" s="T34">dʼɨl-BIt</ta>
            <ta e="T36" id="Seg_6320" s="T35">ir-TI-tA</ta>
            <ta e="T37" id="Seg_6321" s="T36">diː</ta>
            <ta e="T38" id="Seg_6322" s="T37">mas</ta>
            <ta e="T39" id="Seg_6323" s="T38">tart-I-n-A</ta>
            <ta e="T40" id="Seg_6324" s="T39">bar-IAK-m</ta>
            <ta e="T41" id="Seg_6325" s="T40">ehiːl-GI-GA</ta>
            <ta e="T42" id="Seg_6326" s="T41">otut</ta>
            <ta e="T43" id="Seg_6327" s="T42">hɨrga-TA</ta>
            <ta e="T44" id="Seg_6328" s="T43">hü͡ökkeː</ta>
            <ta e="T45" id="Seg_6329" s="T44">otut</ta>
            <ta e="T46" id="Seg_6330" s="T45">hɨrga-nAn</ta>
            <ta e="T47" id="Seg_6331" s="T46">bajgal-GA</ta>
            <ta e="T48" id="Seg_6332" s="T47">bar-An</ta>
            <ta e="T49" id="Seg_6333" s="T48">kaːl-Ar</ta>
            <ta e="T50" id="Seg_6334" s="T49">[C^1][V^1][C^2]-hogotogun</ta>
            <ta e="T51" id="Seg_6335" s="T50">bajgal-GA</ta>
            <ta e="T52" id="Seg_6336" s="T51">tumus</ta>
            <ta e="T53" id="Seg_6337" s="T52">bagajɨ-nAn</ta>
            <ta e="T54" id="Seg_6338" s="T53">kiːr-BIT</ta>
            <ta e="T55" id="Seg_6339" s="T54">čogotok</ta>
            <ta e="T56" id="Seg_6340" s="T55">hir-ttAn</ta>
            <ta e="T57" id="Seg_6341" s="T56">ti͡ej-n-BIT</ta>
            <ta e="T58" id="Seg_6342" s="T57">hɨːr-tI-n</ta>
            <ta e="T59" id="Seg_6343" s="T58">ürüt-tI-GAr</ta>
            <ta e="T60" id="Seg_6344" s="T59">biːr</ta>
            <ta e="T61" id="Seg_6345" s="T60">halaː</ta>
            <ta e="T62" id="Seg_6346" s="T61">üstün</ta>
            <ta e="T63" id="Seg_6347" s="T62">tönün-I-BIT</ta>
            <ta e="T64" id="Seg_6348" s="T63">bu</ta>
            <ta e="T65" id="Seg_6349" s="T64">tagɨs-An</ta>
            <ta e="T66" id="Seg_6350" s="T65">kaja-tI-n</ta>
            <ta e="T67" id="Seg_6351" s="T66">ürüt-tI-GAr</ta>
            <ta e="T68" id="Seg_6352" s="T67">tabaːk</ta>
            <ta e="T69" id="Seg_6353" s="T68">tart-A</ta>
            <ta e="T70" id="Seg_6354" s="T69">olor-BIT</ta>
            <ta e="T71" id="Seg_6355" s="T70">ebe</ta>
            <ta e="T72" id="Seg_6356" s="T71">dek</ta>
            <ta e="T73" id="Seg_6357" s="T72">kör-BIT-tA</ta>
            <ta e="T74" id="Seg_6358" s="T73">uː-GA</ta>
            <ta e="T75" id="Seg_6359" s="T74">ogo</ta>
            <ta e="T76" id="Seg_6360" s="T75">hɨrɨt-Ar</ta>
            <ta e="T77" id="Seg_6361" s="T76">ir-BIT</ta>
            <ta e="T78" id="Seg_6362" s="T77">uː-GA</ta>
            <ta e="T79" id="Seg_6363" s="T78">küːgen-LArI-nAn</ta>
            <ta e="T80" id="Seg_6364" s="T79">oːnnʼoː-A</ta>
            <ta e="T81" id="Seg_6365" s="T80">hɨrɨt-Ar</ta>
            <ta e="T82" id="Seg_6366" s="T81">u͡ol</ta>
            <ta e="T83" id="Seg_6367" s="T82">ogo</ta>
            <ta e="T84" id="Seg_6368" s="T83">bɨhɨːlaːk</ta>
            <ta e="T85" id="Seg_6369" s="T84">kantan</ta>
            <ta e="T86" id="Seg_6370" s="T85">da</ta>
            <ta e="T87" id="Seg_6371" s="T86">kel-BIT-tA</ta>
            <ta e="T88" id="Seg_6372" s="T87">bil-LIN-I-BAT</ta>
            <ta e="T89" id="Seg_6373" s="T88">oduːrgaː-BIT</ta>
            <ta e="T90" id="Seg_6374" s="T89">min</ta>
            <ta e="T91" id="Seg_6375" s="T90">ogo-tA</ta>
            <ta e="T92" id="Seg_6376" s="T91">hu͡ok-BIn</ta>
            <ta e="T93" id="Seg_6377" s="T92">diː</ta>
            <ta e="T94" id="Seg_6378" s="T93">ɨl-BIT</ta>
            <ta e="T95" id="Seg_6379" s="T94">kihi</ta>
            <ta e="T96" id="Seg_6380" s="T95">di͡e-A</ta>
            <ta e="T97" id="Seg_6381" s="T96">hanaː-BIT</ta>
            <ta e="T98" id="Seg_6382" s="T97">ogo-tA</ta>
            <ta e="T99" id="Seg_6383" s="T98">arɨː</ta>
            <ta e="T100" id="Seg_6384" s="T99">kumak-tI-GAr</ta>
            <ta e="T101" id="Seg_6385" s="T100">hɨrɨt-Ar</ta>
            <ta e="T102" id="Seg_6386" s="T101">ökögör</ta>
            <ta e="T103" id="Seg_6387" s="T102">mas</ta>
            <ta e="T104" id="Seg_6388" s="T103">baːr</ta>
            <ta e="T105" id="Seg_6389" s="T104">ol-nAn</ta>
            <ta e="T106" id="Seg_6390" s="T105">bar-TAK-tA</ta>
            <ta e="T107" id="Seg_6391" s="T106">ɨstan-An</ta>
            <ta e="T108" id="Seg_6392" s="T107">tij-Ar</ta>
            <ta e="T109" id="Seg_6393" s="T108">ogonnʼor</ta>
            <ta e="T110" id="Seg_6394" s="T109">argaː-tI-ttAn</ta>
            <ta e="T111" id="Seg_6395" s="T110">kap-An</ta>
            <ta e="T112" id="Seg_6396" s="T111">ɨl-An</ta>
            <ta e="T113" id="Seg_6397" s="T112">hokuj-tI-n</ta>
            <ta e="T114" id="Seg_6398" s="T113">is-tI-GAr</ta>
            <ta e="T115" id="Seg_6399" s="T114">uk-BIT</ta>
            <ta e="T116" id="Seg_6400" s="T115">kurdaː-A-n-An</ta>
            <ta e="T117" id="Seg_6401" s="T116">keːs-BIT</ta>
            <ta e="T118" id="Seg_6402" s="T117">dʼe</ta>
            <ta e="T119" id="Seg_6403" s="T118">agaj</ta>
            <ta e="T120" id="Seg_6404" s="T119">kamnaː-IːhI</ta>
            <ta e="T121" id="Seg_6405" s="T120">tɨ͡as</ta>
            <ta e="T122" id="Seg_6406" s="T121">köːkün-tA</ta>
            <ta e="T123" id="Seg_6407" s="T122">bu͡ol-Ar</ta>
            <ta e="T124" id="Seg_6408" s="T123">tur-BIT</ta>
            <ta e="T125" id="Seg_6409" s="T124">kör-BIT-tA</ta>
            <ta e="T126" id="Seg_6410" s="T125">bütün</ta>
            <ta e="T127" id="Seg_6411" s="T126">munnʼak</ta>
            <ta e="T128" id="Seg_6412" s="T127">kihi-tA</ta>
            <ta e="T129" id="Seg_6413" s="T128">öl-BIT</ta>
            <ta e="T130" id="Seg_6414" s="T129">hir-tA</ta>
            <ta e="T131" id="Seg_6415" s="T130">e-BIT</ta>
            <ta e="T132" id="Seg_6416" s="T131">eː</ta>
            <ta e="T133" id="Seg_6417" s="T132">bu-LAr-ttAn</ta>
            <ta e="T134" id="Seg_6418" s="T133">ort-An</ta>
            <ta e="T135" id="Seg_6419" s="T134">kaːl-BIT</ta>
            <ta e="T136" id="Seg_6420" s="T135">ogo</ta>
            <ta e="T137" id="Seg_6421" s="T136">bu͡ollaga</ta>
            <ta e="T138" id="Seg_6422" s="T137">biːr</ta>
            <ta e="T139" id="Seg_6423" s="T138">argaː-BIt-LAːK</ta>
            <ta e="T140" id="Seg_6424" s="T139">nʼu͡oguhut-tI-n</ta>
            <ta e="T141" id="Seg_6425" s="T140">egel-An</ta>
            <ta e="T142" id="Seg_6426" s="T141">ölör-An</ta>
            <ta e="T143" id="Seg_6427" s="T142">keːs-Ar</ta>
            <ta e="T144" id="Seg_6428" s="T143">ogo-GItI-n</ta>
            <ta e="T145" id="Seg_6429" s="T144">ilt-TI-m</ta>
            <ta e="T146" id="Seg_6430" s="T145">bu</ta>
            <ta e="T147" id="Seg_6431" s="T146">ogo-GIt</ta>
            <ta e="T148" id="Seg_6432" s="T147">oŋku-tA</ta>
            <ta e="T149" id="Seg_6433" s="T148">biːr</ta>
            <ta e="T150" id="Seg_6434" s="T149">taba-nI</ta>
            <ta e="T151" id="Seg_6435" s="T150">keːs-TI-m</ta>
            <ta e="T152" id="Seg_6436" s="T151">di͡e-Ar</ta>
            <ta e="T153" id="Seg_6437" s="T152">egel-TI-tA</ta>
            <ta e="T154" id="Seg_6438" s="T153">dʼi͡e-tI-GAr</ta>
            <ta e="T155" id="Seg_6439" s="T154">emeːksin-tI-GAr</ta>
            <ta e="T156" id="Seg_6440" s="T155">kepseː-Ar</ta>
            <ta e="T157" id="Seg_6441" s="T156">üs</ta>
            <ta e="T158" id="Seg_6442" s="T157">kün</ta>
            <ta e="T159" id="Seg_6443" s="T158">turkarɨ</ta>
            <ta e="T160" id="Seg_6444" s="T159">taba-LArI-n</ta>
            <ta e="T161" id="Seg_6445" s="T160">da</ta>
            <ta e="T162" id="Seg_6446" s="T161">ɨːt-BAkkA</ta>
            <ta e="T163" id="Seg_6447" s="T162">bu</ta>
            <ta e="T164" id="Seg_6448" s="T163">ogo-nAn</ta>
            <ta e="T165" id="Seg_6449" s="T164">oːnnʼoː-Ar-LAr</ta>
            <ta e="T166" id="Seg_6450" s="T165">ölgöbüːn-tA</ta>
            <ta e="T167" id="Seg_6451" s="T166">barɨ-tA</ta>
            <ta e="T168" id="Seg_6452" s="T167">kölüj-LIN-A</ta>
            <ta e="T169" id="Seg_6453" s="T168">hɨt-Ar</ta>
            <ta e="T170" id="Seg_6454" s="T169">e-BIT</ta>
            <ta e="T171" id="Seg_6455" s="T170">tu͡ok</ta>
            <ta e="T172" id="Seg_6456" s="T171">da</ta>
            <ta e="T173" id="Seg_6457" s="T172">bil-LIN-I-BAT</ta>
            <ta e="T174" id="Seg_6458" s="T173">ogo-tA</ta>
            <ta e="T175" id="Seg_6459" s="T174">hüːreleː-A</ta>
            <ta e="T176" id="Seg_6460" s="T175">hɨrɨt-Ar</ta>
            <ta e="T177" id="Seg_6461" s="T176">bu͡ol-BIT</ta>
            <ta e="T178" id="Seg_6462" s="T177">ulaːt-BIT</ta>
            <ta e="T179" id="Seg_6463" s="T178">oduːlaː-A</ta>
            <ta e="T180" id="Seg_6464" s="T179">olor-TAK-InA</ta>
            <ta e="T181" id="Seg_6465" s="T180">hɨrga-GA</ta>
            <ta e="T182" id="Seg_6466" s="T181">bi͡es</ta>
            <ta e="T183" id="Seg_6467" s="T182">čeːlkeː</ta>
            <ta e="T184" id="Seg_6468" s="T183">buːr-LAːK</ta>
            <ta e="T185" id="Seg_6469" s="T184">[C^1][V^1][C^2]-čeːlkeː</ta>
            <ta e="T186" id="Seg_6470" s="T185">taŋas-LAːK</ta>
            <ta e="T187" id="Seg_6471" s="T186">kihi</ta>
            <ta e="T188" id="Seg_6472" s="T187">is-Ar</ta>
            <ta e="T189" id="Seg_6473" s="T188">kineːs</ta>
            <ta e="T190" id="Seg_6474" s="T189">e-BIT</ta>
            <ta e="T191" id="Seg_6475" s="T190">dʼe</ta>
            <ta e="T192" id="Seg_6476" s="T191">tu͡ok-BI-n</ta>
            <ta e="T193" id="Seg_6477" s="T192">bi͡er-IAK-m=Ij</ta>
            <ta e="T194" id="Seg_6478" s="T193">di͡e-A</ta>
            <ta e="T195" id="Seg_6479" s="T194">hanaː-Ar</ta>
            <ta e="T196" id="Seg_6480" s="T195">ogonnʼor</ta>
            <ta e="T197" id="Seg_6481" s="T196">kajdak</ta>
            <ta e="T198" id="Seg_6482" s="T197">olor-TI-ŋ</ta>
            <ta e="T199" id="Seg_6483" s="T198">kajdak</ta>
            <ta e="T200" id="Seg_6484" s="T199">mannɨk</ta>
            <ta e="T201" id="Seg_6485" s="T200">ör</ta>
            <ta e="T202" id="Seg_6486" s="T201">bu͡ol-TI-m</ta>
            <ta e="T203" id="Seg_6487" s="T202">ogo-LAN-BIT-GIn</ta>
            <ta e="T204" id="Seg_6488" s="T203">du͡o</ta>
            <ta e="T205" id="Seg_6489" s="T204">di͡e-Ar</ta>
            <ta e="T206" id="Seg_6490" s="T205">kineːs</ta>
            <ta e="T207" id="Seg_6491" s="T206">kɨrɨj-Ar</ta>
            <ta e="T208" id="Seg_6492" s="T207">haːs-BItI-GAr</ta>
            <ta e="T209" id="Seg_6493" s="T208">ogo-LAN-TI-BIt</ta>
            <ta e="T210" id="Seg_6494" s="T209">di͡e-Ar</ta>
            <ta e="T211" id="Seg_6495" s="T210">ogonnʼor</ta>
            <ta e="T212" id="Seg_6496" s="T211">dʼe</ta>
            <ta e="T213" id="Seg_6497" s="T212">üčügej</ta>
            <ta e="T214" id="Seg_6498" s="T213">nolu͡og</ta>
            <ta e="T215" id="Seg_6499" s="T214">komuj-A</ta>
            <ta e="T216" id="Seg_6500" s="T215">kel-TI-m</ta>
            <ta e="T217" id="Seg_6501" s="T216">di͡e-Ar</ta>
            <ta e="T218" id="Seg_6502" s="T217">kineːs</ta>
            <ta e="T219" id="Seg_6503" s="T218">ogonnʼor</ta>
            <ta e="T220" id="Seg_6504" s="T219">töhö</ta>
            <ta e="T221" id="Seg_6505" s="T220">eme</ta>
            <ta e="T222" id="Seg_6506" s="T221">küːl</ta>
            <ta e="T223" id="Seg_6507" s="T222">kɨrsa-nI</ta>
            <ta e="T224" id="Seg_6508" s="T223">bi͡er-Ar</ta>
            <ta e="T225" id="Seg_6509" s="T224">kineːs-tA</ta>
            <ta e="T226" id="Seg_6510" s="T225">ü͡ör-Ar</ta>
            <ta e="T227" id="Seg_6511" s="T226">orduk-tI-n</ta>
            <ta e="T228" id="Seg_6512" s="T227">karčɨ</ta>
            <ta e="T229" id="Seg_6513" s="T228">ɨːt-IAK-m</ta>
            <ta e="T230" id="Seg_6514" s="T229">di͡e-Ar</ta>
            <ta e="T231" id="Seg_6515" s="T230">kineːs-tA</ta>
            <ta e="T232" id="Seg_6516" s="T231">kamnaː-IːhI</ta>
            <ta e="T233" id="Seg_6517" s="T232">kanna</ta>
            <ta e="T234" id="Seg_6518" s="T233">ere</ta>
            <ta e="T235" id="Seg_6519" s="T234">dʼuraːk</ta>
            <ta e="T236" id="Seg_6520" s="T235">kineːs-tI-GAr</ta>
            <ta e="T237" id="Seg_6521" s="T236">kötüt-TI-tA</ta>
            <ta e="T238" id="Seg_6522" s="T237">dogor-tI-GAr</ta>
            <ta e="T239" id="Seg_6523" s="T238">kineːs</ta>
            <ta e="T240" id="Seg_6524" s="T239">kineːs-tI-GAr</ta>
            <ta e="T241" id="Seg_6525" s="T240">kel-BIT</ta>
            <ta e="T242" id="Seg_6526" s="T241">aragiː-LAː-A</ta>
            <ta e="T243" id="Seg_6527" s="T242">hɨt-Ar-LAr</ta>
            <ta e="T244" id="Seg_6528" s="T243">kas</ta>
            <ta e="T245" id="Seg_6529" s="T244">kün</ta>
            <ta e="T246" id="Seg_6530" s="T245">aragiː-LAː-A</ta>
            <ta e="T247" id="Seg_6531" s="T246">hɨt-BIT-LAr</ta>
            <ta e="T248" id="Seg_6532" s="T247">ol</ta>
            <ta e="T249" id="Seg_6533" s="T248">olor-An</ta>
            <ta e="T250" id="Seg_6534" s="T249">ös</ta>
            <ta e="T251" id="Seg_6535" s="T250">kepseː-BIT</ta>
            <ta e="T252" id="Seg_6536" s="T251">dogor-tI-GAr</ta>
            <ta e="T253" id="Seg_6537" s="T252">dʼe-dʼe</ta>
            <ta e="T254" id="Seg_6538" s="T253">min</ta>
            <ta e="T255" id="Seg_6539" s="T254">kihi</ta>
            <ta e="T256" id="Seg_6540" s="T255">kuttan-Ar</ta>
            <ta e="T257" id="Seg_6541" s="T256">kihi-LAːK-BIn</ta>
            <ta e="T258" id="Seg_6542" s="T257">nolu͡og-tI-n</ta>
            <ta e="T259" id="Seg_6543" s="T258">ol</ta>
            <ta e="T260" id="Seg_6544" s="T259">ereːri</ta>
            <ta e="T261" id="Seg_6545" s="T260">aharɨ</ta>
            <ta e="T262" id="Seg_6546" s="T261">tölöː-Ar</ta>
            <ta e="T263" id="Seg_6547" s="T262">ol-ttAn</ta>
            <ta e="T264" id="Seg_6548" s="T263">kel-TI-m</ta>
            <ta e="T265" id="Seg_6549" s="T264">oduːrgaː-A-BIn</ta>
            <ta e="T266" id="Seg_6550" s="T265">hüːr-A</ta>
            <ta e="T267" id="Seg_6551" s="T266">hɨrɨt-Ar</ta>
            <ta e="T268" id="Seg_6552" s="T267">ogo-LAN-BIT</ta>
            <ta e="T269" id="Seg_6553" s="T268">beje-tA</ta>
            <ta e="T270" id="Seg_6554" s="T269">töröː-BIT</ta>
            <ta e="T271" id="Seg_6555" s="T270">ogo-tA</ta>
            <ta e="T272" id="Seg_6556" s="T271">bu͡ol-BAtAK</ta>
            <ta e="T273" id="Seg_6557" s="T272">kantan</ta>
            <ta e="T274" id="Seg_6558" s="T273">ɨl-BIT-tA</ta>
            <ta e="T275" id="Seg_6559" s="T274">bu͡ol-IAK.[tA]=Ij</ta>
            <ta e="T276" id="Seg_6560" s="T275">kim-nI</ta>
            <ta e="T277" id="Seg_6561" s="T276">ere</ta>
            <ta e="T278" id="Seg_6562" s="T277">dʼürü</ta>
            <ta e="T279" id="Seg_6563" s="T278">ölör-An</ta>
            <ta e="T280" id="Seg_6564" s="T279">ɨl-TAK-tA</ta>
            <ta e="T281" id="Seg_6565" s="T280">eːk</ta>
            <ta e="T282" id="Seg_6566" s="T281">di͡e-Ar</ta>
            <ta e="T283" id="Seg_6567" s="T282">dʼuraːk</ta>
            <ta e="T284" id="Seg_6568" s="T283">kineːs-tA</ta>
            <ta e="T285" id="Seg_6569" s="T284">bɨlɨr</ta>
            <ta e="T286" id="Seg_6570" s="T285">min</ta>
            <ta e="T287" id="Seg_6571" s="T286">biːr</ta>
            <ta e="T288" id="Seg_6572" s="T287">munnʼak</ta>
            <ta e="T289" id="Seg_6573" s="T288">kihi-m</ta>
            <ta e="T290" id="Seg_6574" s="T289">öl-BIT-tA</ta>
            <ta e="T291" id="Seg_6575" s="T290">ol-ttAn</ta>
            <ta e="T292" id="Seg_6576" s="T291">biːr</ta>
            <ta e="T293" id="Seg_6577" s="T292">bihik-LAːK</ta>
            <ta e="T294" id="Seg_6578" s="T293">ogo</ta>
            <ta e="T295" id="Seg_6579" s="T294">kaːl-BIT</ta>
            <ta e="T296" id="Seg_6580" s="T295">hurak-LAːK-tA</ta>
            <ta e="T297" id="Seg_6581" s="T296">ol-nI</ta>
            <ta e="T298" id="Seg_6582" s="T297">ɨl-TAr-BAtAK-I-m</ta>
            <ta e="T299" id="Seg_6583" s="T298">ɨ͡arɨː</ta>
            <ta e="T300" id="Seg_6584" s="T299">batɨs-IAK.[tA]</ta>
            <ta e="T301" id="Seg_6585" s="T300">di͡e-An-BIn</ta>
            <ta e="T302" id="Seg_6586" s="T301">ol-nI</ta>
            <ta e="T303" id="Seg_6587" s="T302">ɨl-TAK-tA</ta>
            <ta e="T304" id="Seg_6588" s="T303">ol</ta>
            <ta e="T305" id="Seg_6589" s="T304">ogo-nI</ta>
            <ta e="T306" id="Seg_6590" s="T305">kisteː-BIT</ta>
            <ta e="T307" id="Seg_6591" s="T306">e-BIT</ta>
            <ta e="T308" id="Seg_6592" s="T307">min-n</ta>
            <ta e="T309" id="Seg_6593" s="T308">ölör-IAK-LArA</ta>
            <ta e="T310" id="Seg_6594" s="T309">di͡e-An</ta>
            <ta e="T311" id="Seg_6595" s="T310">dʼe</ta>
            <ta e="T312" id="Seg_6596" s="T311">bugurduk</ta>
            <ta e="T313" id="Seg_6597" s="T312">hübeleː-A-BIn</ta>
            <ta e="T314" id="Seg_6598" s="T313">en</ta>
            <ta e="T315" id="Seg_6599" s="T314">kihi-GI-ttAn</ta>
            <ta e="T316" id="Seg_6600" s="T315">kuttan-I-BA</ta>
            <ta e="T317" id="Seg_6601" s="T316">ol</ta>
            <ta e="T318" id="Seg_6602" s="T317">munnʼak</ta>
            <ta e="T319" id="Seg_6603" s="T318">dʼon-tI-n</ta>
            <ta e="T320" id="Seg_6604" s="T319">kɨrt-An</ta>
            <ta e="T321" id="Seg_6605" s="T320">baran</ta>
            <ta e="T322" id="Seg_6606" s="T321">ogo-nI</ta>
            <ta e="T323" id="Seg_6607" s="T322">ɨl-BIT</ta>
            <ta e="T324" id="Seg_6608" s="T323">di͡e-An</ta>
            <ta e="T325" id="Seg_6609" s="T324">ikki</ta>
            <ta e="T326" id="Seg_6610" s="T325">kineːs</ta>
            <ta e="T327" id="Seg_6611" s="T326">ɨraːktaːgɨ-tA</ta>
            <ta e="T328" id="Seg_6612" s="T327">üŋ-s-An</ta>
            <ta e="T329" id="Seg_6613" s="T328">huruj-IAk</ta>
            <ta e="T330" id="Seg_6614" s="T329">oččogo</ta>
            <ta e="T331" id="Seg_6615" s="T330">bihigi</ta>
            <ta e="T332" id="Seg_6616" s="T331">tɨl-BItI-n</ta>
            <ta e="T333" id="Seg_6617" s="T332">ɨraːktaːgɨ</ta>
            <ta e="T334" id="Seg_6618" s="T333">bɨs-IAK.[tA]</ta>
            <ta e="T335" id="Seg_6619" s="T334">hu͡ok-tA</ta>
            <ta e="T336" id="Seg_6620" s="T335">gini-nI</ta>
            <ta e="T337" id="Seg_6621" s="T336">huːt-LAː-A-t-IAK.[tA]</ta>
            <ta e="T338" id="Seg_6622" s="T337">kaːjɨː-GA</ta>
            <ta e="T339" id="Seg_6623" s="T338">ɨːt-IAK-tA</ta>
            <ta e="T340" id="Seg_6624" s="T339">bihigi</ta>
            <ta e="T341" id="Seg_6625" s="T340">gini</ta>
            <ta e="T342" id="Seg_6626" s="T341">baːj-tI-n</ta>
            <ta e="T343" id="Seg_6627" s="T342">ɨl-IAK-BIt</ta>
            <ta e="T344" id="Seg_6628" s="T343">di͡e-BIT</ta>
            <ta e="T345" id="Seg_6629" s="T344">dʼuraːk</ta>
            <ta e="T346" id="Seg_6630" s="T345">kineːs-tA</ta>
            <ta e="T347" id="Seg_6631" s="T346">kihi-GA</ta>
            <ta e="T348" id="Seg_6632" s="T347">kütüreː-TAK-GA</ta>
            <ta e="T349" id="Seg_6633" s="T348">kuhagan</ta>
            <ta e="T350" id="Seg_6634" s="T349">bu͡ol-AːrAj</ta>
            <ta e="T351" id="Seg_6635" s="T350">di͡e-Ar</ta>
            <ta e="T352" id="Seg_6636" s="T351">haːmaj</ta>
            <ta e="T353" id="Seg_6637" s="T352">kineːs-tA</ta>
            <ta e="T354" id="Seg_6638" s="T353">tɨ͡a</ta>
            <ta e="T355" id="Seg_6639" s="T354">kineːs-tI-n</ta>
            <ta e="T356" id="Seg_6640" s="T355">ɨgɨr-BIT</ta>
            <ta e="T357" id="Seg_6641" s="T356">bu͡ol-TAR</ta>
            <ta e="T358" id="Seg_6642" s="T357">üs</ta>
            <ta e="T359" id="Seg_6643" s="T358">kineːs</ta>
            <ta e="T360" id="Seg_6644" s="T359">itegel-LAːK</ta>
            <ta e="T361" id="Seg_6645" s="T360">bu͡ol-IAK</ta>
            <ta e="T362" id="Seg_6646" s="T361">e-TI-tA</ta>
            <ta e="T363" id="Seg_6647" s="T362">dʼuraːk</ta>
            <ta e="T364" id="Seg_6648" s="T363">kineːs-tA</ta>
            <ta e="T365" id="Seg_6649" s="T364">kihi-tA</ta>
            <ta e="T366" id="Seg_6650" s="T365">ɨːt-IAK-m</ta>
            <ta e="T367" id="Seg_6651" s="T366">tɨ͡a-LAr</ta>
            <ta e="T368" id="Seg_6652" s="T367">kineːs-LArI-GAr</ta>
            <ta e="T369" id="Seg_6653" s="T368">üs</ta>
            <ta e="T370" id="Seg_6654" s="T369">konuk-I-nAn</ta>
            <ta e="T371" id="Seg_6655" s="T370">kel-IAK.[tA]</ta>
            <ta e="T372" id="Seg_6656" s="T371">höbüleː-A-n-BA-TAK-InA</ta>
            <ta e="T373" id="Seg_6657" s="T372">beje-tI-n</ta>
            <ta e="T374" id="Seg_6658" s="T373">ölör-I-LIN-IAK.[tA]</ta>
            <ta e="T375" id="Seg_6659" s="T374">utar-s-IAK.[tA]</ta>
            <ta e="T376" id="Seg_6660" s="T375">hu͡ok-tA</ta>
            <ta e="T377" id="Seg_6661" s="T376">di͡e-Ar</ta>
            <ta e="T378" id="Seg_6662" s="T377">hɨrga-LAːK</ta>
            <ta e="T379" id="Seg_6663" s="T378">kihi-nI</ta>
            <ta e="T380" id="Seg_6664" s="T379">ɨːt-TI-A</ta>
            <ta e="T381" id="Seg_6665" s="T380">tɨ͡a</ta>
            <ta e="T382" id="Seg_6666" s="T381">kineːs-tI-n</ta>
            <ta e="T383" id="Seg_6667" s="T382">ɨgɨr-TAr-A</ta>
            <ta e="T384" id="Seg_6668" s="T383">üs-Is</ta>
            <ta e="T385" id="Seg_6669" s="T384">kün-tI-GAr</ta>
            <ta e="T386" id="Seg_6670" s="T385">tɨ͡a</ta>
            <ta e="T387" id="Seg_6671" s="T386">kineːs-tA</ta>
            <ta e="T388" id="Seg_6672" s="T387">kel-Ar</ta>
            <ta e="T389" id="Seg_6673" s="T388">oguru͡o</ta>
            <ta e="T390" id="Seg_6674" s="T389">taŋas-LAːK</ta>
            <ta e="T391" id="Seg_6675" s="T390">üs</ta>
            <ta e="T392" id="Seg_6676" s="T391">kas</ta>
            <ta e="T393" id="Seg_6677" s="T392">kün</ta>
            <ta e="T394" id="Seg_6678" s="T393">aragiː-LAː-Ar-LAr</ta>
            <ta e="T395" id="Seg_6679" s="T394">dʼuraːk</ta>
            <ta e="T396" id="Seg_6680" s="T395">kineːs-tA</ta>
            <ta e="T397" id="Seg_6681" s="T396">kepseː-Ar</ta>
            <ta e="T398" id="Seg_6682" s="T397">haːmaj</ta>
            <ta e="T399" id="Seg_6683" s="T398">kihi-tA</ta>
            <ta e="T400" id="Seg_6684" s="T399">ɨ͡arɨː-ttAn</ta>
            <ta e="T401" id="Seg_6685" s="T400">öl-BIT</ta>
            <ta e="T402" id="Seg_6686" s="T401">dʼon-ttAn</ta>
            <ta e="T403" id="Seg_6687" s="T402">biːr</ta>
            <ta e="T404" id="Seg_6688" s="T403">ogo-nI</ta>
            <ta e="T405" id="Seg_6689" s="T404">ɨl-BIT</ta>
            <ta e="T406" id="Seg_6690" s="T405">ol-nI</ta>
            <ta e="T407" id="Seg_6691" s="T406">ɨ͡arɨː</ta>
            <ta e="T408" id="Seg_6692" s="T407">batɨs-IAK.[tA]</ta>
            <ta e="T409" id="Seg_6693" s="T408">bihigi</ta>
            <ta e="T410" id="Seg_6694" s="T409">ɨraːktaːgɨ-GA</ta>
            <ta e="T411" id="Seg_6695" s="T410">üŋ-s-IAgIŋ</ta>
            <ta e="T412" id="Seg_6696" s="T411">üs-IAn</ta>
            <ta e="T413" id="Seg_6697" s="T412">huruj-IAgIŋ</ta>
            <ta e="T414" id="Seg_6698" s="T413">tɨ͡a</ta>
            <ta e="T415" id="Seg_6699" s="T414">kineːs-tA</ta>
            <ta e="T416" id="Seg_6700" s="T415">dumaj-LAː-BIT</ta>
            <ta e="T417" id="Seg_6701" s="T416">di͡e-BIT</ta>
            <ta e="T418" id="Seg_6702" s="T417">huːt-LAː-Ar-tI-n</ta>
            <ta e="T419" id="Seg_6703" s="T418">huːt-LAː-mInA</ta>
            <ta e="T420" id="Seg_6704" s="T419">ol</ta>
            <ta e="T421" id="Seg_6705" s="T420">ereːri</ta>
            <ta e="T422" id="Seg_6706" s="T421">kütüreː-Ar</ta>
            <ta e="T423" id="Seg_6707" s="T422">hür-tA</ta>
            <ta e="T424" id="Seg_6708" s="T423">bert</ta>
            <ta e="T425" id="Seg_6709" s="T424">dʼon-nI</ta>
            <ta e="T426" id="Seg_6710" s="T425">kej-BIT</ta>
            <ta e="T427" id="Seg_6711" s="T426">di͡e-An</ta>
            <ta e="T428" id="Seg_6712" s="T427">tulaːjak-nI</ta>
            <ta e="T429" id="Seg_6713" s="T428">ɨl-BIT-tA</ta>
            <ta e="T430" id="Seg_6714" s="T429">tu͡ok</ta>
            <ta e="T431" id="Seg_6715" s="T430">buruj-tA=Ij</ta>
            <ta e="T432" id="Seg_6716" s="T431">huːt-GA</ta>
            <ta e="T433" id="Seg_6717" s="T432">bi͡er-IAK-BItI-n</ta>
            <ta e="T434" id="Seg_6718" s="T433">di͡e-BIT</ta>
            <ta e="T435" id="Seg_6719" s="T434">dʼuraːk</ta>
            <ta e="T436" id="Seg_6720" s="T435">kineːs-tA</ta>
            <ta e="T437" id="Seg_6721" s="T436">en</ta>
            <ta e="T438" id="Seg_6722" s="T437">kömüskeː-A-GIn</ta>
            <ta e="T439" id="Seg_6723" s="T438">du͡o</ta>
            <ta e="T440" id="Seg_6724" s="T439">ol</ta>
            <ta e="T441" id="Seg_6725" s="T440">kihi-nI</ta>
            <ta e="T442" id="Seg_6726" s="T441">oččogo</ta>
            <ta e="T443" id="Seg_6727" s="T442">bihigi</ta>
            <ta e="T444" id="Seg_6728" s="T443">en-n</ta>
            <ta e="T445" id="Seg_6729" s="T444">kej-A-BIt</ta>
            <ta e="T446" id="Seg_6730" s="T445">baːj-GI-n</ta>
            <ta e="T447" id="Seg_6731" s="T446">ɨl-IAK-BIt</ta>
            <ta e="T448" id="Seg_6732" s="T447">di͡e-Ar</ta>
            <ta e="T449" id="Seg_6733" s="T448">tɨ͡a</ta>
            <ta e="T450" id="Seg_6734" s="T449">kineːs-tA</ta>
            <ta e="T451" id="Seg_6735" s="T450">kuttan-BIT</ta>
            <ta e="T452" id="Seg_6736" s="T451">ölör-IAK-ČIt</ta>
            <ta e="T453" id="Seg_6737" s="T452">dʼon</ta>
            <ta e="T454" id="Seg_6738" s="T453">ikki-IAn</ta>
            <ta e="T455" id="Seg_6739" s="T454">dʼe</ta>
            <ta e="T456" id="Seg_6740" s="T455">kajdak</ta>
            <ta e="T457" id="Seg_6741" s="T456">hübeleː-A-GIt</ta>
            <ta e="T458" id="Seg_6742" s="T457">di͡e-BIT</ta>
            <ta e="T459" id="Seg_6743" s="T458">dʼuraːk</ta>
            <ta e="T460" id="Seg_6744" s="T459">kineːs-tA</ta>
            <ta e="T461" id="Seg_6745" s="T460">min</ta>
            <ta e="T462" id="Seg_6746" s="T461">tojon</ta>
            <ta e="T463" id="Seg_6747" s="T462">hübehit</ta>
            <ta e="T464" id="Seg_6748" s="T463">bu͡ol-A-BIn</ta>
            <ta e="T465" id="Seg_6749" s="T464">üŋ-s-IAgIŋ</ta>
            <ta e="T466" id="Seg_6750" s="T465">ɨraːktaːgɨ-GA</ta>
            <ta e="T467" id="Seg_6751" s="T466">bu</ta>
            <ta e="T468" id="Seg_6752" s="T467">kosuːn</ta>
            <ta e="T469" id="Seg_6753" s="T468">haːmaj</ta>
            <ta e="T470" id="Seg_6754" s="T469">munnʼak-tI-n</ta>
            <ta e="T471" id="Seg_6755" s="T470">kej-An</ta>
            <ta e="T472" id="Seg_6756" s="T471">baran</ta>
            <ta e="T473" id="Seg_6757" s="T472">ogo-nI</ta>
            <ta e="T474" id="Seg_6758" s="T473">ɨl-BIT</ta>
            <ta e="T475" id="Seg_6759" s="T474">di͡e-An</ta>
            <ta e="T476" id="Seg_6760" s="T475">huruj-IAK-BIt</ta>
            <ta e="T477" id="Seg_6761" s="T476">min</ta>
            <ta e="T478" id="Seg_6762" s="T477">kihi-LAr-I-m</ta>
            <ta e="T479" id="Seg_6763" s="T478">bar-An</ta>
            <ta e="T480" id="Seg_6764" s="T479">öl-BIT</ta>
            <ta e="T481" id="Seg_6765" s="T480">dʼon-nI</ta>
            <ta e="T482" id="Seg_6766" s="T481">ɨt-IAlAː-IAK-LArA</ta>
            <ta e="T483" id="Seg_6767" s="T482">kamʼissʼija</ta>
            <ta e="T484" id="Seg_6768" s="T483">kel-TAK-InA</ta>
            <ta e="T485" id="Seg_6769" s="T484">bul-An</ta>
            <ta e="T486" id="Seg_6770" s="T485">ɨl-IAK.[tA]</ta>
            <ta e="T487" id="Seg_6771" s="T486">hu͡ok-tA</ta>
            <ta e="T488" id="Seg_6772" s="T487">kim</ta>
            <ta e="T489" id="Seg_6773" s="T488">taːj-IAK.[tA]=Ij</ta>
            <ta e="T490" id="Seg_6774" s="T489">di͡e-Ar</ta>
            <ta e="T491" id="Seg_6775" s="T490">u͡on-ččA</ta>
            <ta e="T492" id="Seg_6776" s="T491">kihi</ta>
            <ta e="T493" id="Seg_6777" s="T492">bar-An</ta>
            <ta e="T494" id="Seg_6778" s="T493">bɨlɨr</ta>
            <ta e="T495" id="Seg_6779" s="T494">öl-BIT</ta>
            <ta e="T496" id="Seg_6780" s="T495">dʼon-nI</ta>
            <ta e="T497" id="Seg_6781" s="T496">ɨt-IAlAː-Ar-LAr</ta>
            <ta e="T498" id="Seg_6782" s="T497">huruj-An</ta>
            <ta e="T499" id="Seg_6783" s="T498">keːs-TI-LAr</ta>
            <ta e="T500" id="Seg_6784" s="T499">ogorduk</ta>
            <ta e="T501" id="Seg_6785" s="T500">haːmaj</ta>
            <ta e="T502" id="Seg_6786" s="T501">kineːs-tI-n</ta>
            <ta e="T503" id="Seg_6787" s="T502">ɨːt-Ar-LAr</ta>
            <ta e="T504" id="Seg_6788" s="T503">dʼuraːk</ta>
            <ta e="T505" id="Seg_6789" s="T504">kineːs-tI-LIːN</ta>
            <ta e="T506" id="Seg_6790" s="T505">ɨraːktaːgɨ</ta>
            <ta e="T507" id="Seg_6791" s="T506">aːk-An</ta>
            <ta e="T508" id="Seg_6792" s="T507">kör-Ar</ta>
            <ta e="T509" id="Seg_6793" s="T508">u͡on-ččA</ta>
            <ta e="T510" id="Seg_6794" s="T509">dʼi͡e</ta>
            <ta e="T511" id="Seg_6795" s="T510">dʼon-nI</ta>
            <ta e="T512" id="Seg_6796" s="T511">kej-BIT</ta>
            <ta e="T513" id="Seg_6797" s="T512">ogo-nI</ta>
            <ta e="T514" id="Seg_6798" s="T513">u͡or-BIT</ta>
            <ta e="T515" id="Seg_6799" s="T514">kihi</ta>
            <ta e="T516" id="Seg_6800" s="T515">ɨraːktaːgɨ</ta>
            <ta e="T517" id="Seg_6801" s="T516">barɨ-LArI-ttAn</ta>
            <ta e="T518" id="Seg_6802" s="T517">ɨjɨt-Ar</ta>
            <ta e="T519" id="Seg_6803" s="T518">kirdik</ta>
            <ta e="T520" id="Seg_6804" s="T519">du͡o</ta>
            <ta e="T521" id="Seg_6805" s="T520">min</ta>
            <ta e="T522" id="Seg_6806" s="T521">gini-nI</ta>
            <ta e="T523" id="Seg_6807" s="T522">huːt-LAː-IAK-m</ta>
            <ta e="T524" id="Seg_6808" s="T523">ehigi</ta>
            <ta e="T525" id="Seg_6809" s="T524">ihit-Ar-GItI-GAr</ta>
            <ta e="T526" id="Seg_6810" s="T525">haːmaj</ta>
            <ta e="T527" id="Seg_6811" s="T526">kineːs-tA</ta>
            <ta e="T528" id="Seg_6812" s="T527">kihi-GA</ta>
            <ta e="T529" id="Seg_6813" s="T528">čugahaː-BAT</ta>
            <ta e="T530" id="Seg_6814" s="T529">kihi</ta>
            <ta e="T531" id="Seg_6815" s="T530">oŋor-TI-tA</ta>
            <ta e="T532" id="Seg_6816" s="T531">bu͡olu͡o</ta>
            <ta e="T533" id="Seg_6817" s="T532">di͡e-Ar</ta>
            <ta e="T534" id="Seg_6818" s="T533">ogonnʼor-GA</ta>
            <ta e="T535" id="Seg_6819" s="T534">albun</ta>
            <ta e="T536" id="Seg_6820" s="T535">huruk</ta>
            <ta e="T537" id="Seg_6821" s="T536">ɨːt-Ar-LAr</ta>
            <ta e="T538" id="Seg_6822" s="T537">kineːs-tA</ta>
            <ta e="T539" id="Seg_6823" s="T538">dʼe</ta>
            <ta e="T540" id="Seg_6824" s="T539">nolu͡og-tI-n</ta>
            <ta e="T541" id="Seg_6825" s="T540">karčɨ-tI-n</ta>
            <ta e="T542" id="Seg_6826" s="T541">ɨl-A</ta>
            <ta e="T543" id="Seg_6827" s="T542">kel-BAktAː-TIn</ta>
            <ta e="T544" id="Seg_6828" s="T543">bert</ta>
            <ta e="T545" id="Seg_6829" s="T544">muŋ</ta>
            <ta e="T546" id="Seg_6830" s="T545">türgen-LIk</ta>
            <ta e="T547" id="Seg_6831" s="T546">di͡e-An</ta>
            <ta e="T548" id="Seg_6832" s="T547">huruj-Ar</ta>
            <ta e="T549" id="Seg_6833" s="T548">ildʼit</ta>
            <ta e="T550" id="Seg_6834" s="T549">kihi</ta>
            <ta e="T551" id="Seg_6835" s="T550">ti͡ert-Ar</ta>
            <ta e="T552" id="Seg_6836" s="T551">huruk-nI</ta>
            <ta e="T553" id="Seg_6837" s="T552">ogonnʼor</ta>
            <ta e="T554" id="Seg_6838" s="T553">muŋ</ta>
            <ta e="T555" id="Seg_6839" s="T554">ötü͡ö</ta>
            <ta e="T556" id="Seg_6840" s="T555">taba-LAr-tI-nAn</ta>
            <ta e="T557" id="Seg_6841" s="T556">kötüt-An</ta>
            <ta e="T558" id="Seg_6842" s="T557">kel-Ar</ta>
            <ta e="T559" id="Seg_6843" s="T558">taba-tI-n</ta>
            <ta e="T560" id="Seg_6844" s="T559">baːj-BIT</ta>
            <ta e="T561" id="Seg_6845" s="T560">da</ta>
            <ta e="T562" id="Seg_6846" s="T561">köt-An</ta>
            <ta e="T563" id="Seg_6847" s="T562">tüs-Ar</ta>
            <ta e="T564" id="Seg_6848" s="T563">ostu͡ol-nI</ta>
            <ta e="T565" id="Seg_6849" s="T564">tögürüččü</ta>
            <ta e="T566" id="Seg_6850" s="T565">tojon</ta>
            <ta e="T567" id="Seg_6851" s="T566">bögö</ta>
            <ta e="T568" id="Seg_6852" s="T567">ɨraːktaːgɨ</ta>
            <ta e="T569" id="Seg_6853" s="T568">čaːj</ta>
            <ta e="T570" id="Seg_6854" s="T569">da</ta>
            <ta e="T571" id="Seg_6855" s="T570">is-A-r.[t]-BAtAK</ta>
            <ta e="T572" id="Seg_6856" s="T571">kaːn-tA</ta>
            <ta e="T573" id="Seg_6857" s="T572">kuhagan</ta>
            <ta e="T574" id="Seg_6858" s="T573">čipčik</ta>
            <ta e="T575" id="Seg_6859" s="T574">atɨn</ta>
            <ta e="T576" id="Seg_6860" s="T575">dʼe</ta>
            <ta e="T577" id="Seg_6861" s="T576">kepseː</ta>
            <ta e="T578" id="Seg_6862" s="T577">ös-GI-n</ta>
            <ta e="T579" id="Seg_6863" s="T578">munnʼak</ta>
            <ta e="T580" id="Seg_6864" s="T579">dʼon-nI</ta>
            <ta e="T581" id="Seg_6865" s="T580">togo</ta>
            <ta e="T582" id="Seg_6866" s="T581">kɨrt-TI-ŋ</ta>
            <ta e="T583" id="Seg_6867" s="T582">tu͡ok</ta>
            <ta e="T584" id="Seg_6868" s="T583">munnʼak-tI-n</ta>
            <ta e="T585" id="Seg_6869" s="T584">hap-TAr-A-GIn</ta>
            <ta e="T586" id="Seg_6870" s="T585">kihi-nI</ta>
            <ta e="T587" id="Seg_6871" s="T586">kɨtta</ta>
            <ta e="T588" id="Seg_6872" s="T587">mökkü͡ön-I-m</ta>
            <ta e="T589" id="Seg_6873" s="T588">da</ta>
            <ta e="T590" id="Seg_6874" s="T589">hu͡ok</ta>
            <ta e="T591" id="Seg_6875" s="T590">ɨraːktaːgɨ</ta>
            <ta e="T592" id="Seg_6876" s="T591">kineːs-LAr</ta>
            <ta e="T593" id="Seg_6877" s="T592">huruk-LArI-n</ta>
            <ta e="T594" id="Seg_6878" s="T593">kör-TAr-Ar</ta>
            <ta e="T595" id="Seg_6879" s="T594">ehiːl-GI</ta>
            <ta e="T596" id="Seg_6880" s="T595">uː</ta>
            <ta e="T597" id="Seg_6881" s="T596">kel-IAK.[tI]-r</ta>
            <ta e="T598" id="Seg_6882" s="T597">di͡eri</ta>
            <ta e="T599" id="Seg_6883" s="T598">kaːjɨː-GA</ta>
            <ta e="T600" id="Seg_6884" s="T599">hɨt-IAK-ŋ</ta>
            <ta e="T601" id="Seg_6885" s="T600">min-TAːgAr</ta>
            <ta e="T602" id="Seg_6886" s="T601">ulakan</ta>
            <ta e="T603" id="Seg_6887" s="T602">ɨraːktaːgɨ-GA</ta>
            <ta e="T604" id="Seg_6888" s="T603">ɨːt-IAK-m</ta>
            <ta e="T605" id="Seg_6889" s="T604">ogonnʼor-nI</ta>
            <ta e="T606" id="Seg_6890" s="T605">türme-GA</ta>
            <ta e="T607" id="Seg_6891" s="T606">kaːj-An</ta>
            <ta e="T608" id="Seg_6892" s="T607">keːs-Ar-LAr</ta>
            <ta e="T609" id="Seg_6893" s="T608">üs</ta>
            <ta e="T610" id="Seg_6894" s="T609">kineːs</ta>
            <ta e="T611" id="Seg_6895" s="T610">baːj-tI-n</ta>
            <ta e="T612" id="Seg_6896" s="T611">üllehin-IAK-LArA</ta>
            <ta e="T613" id="Seg_6897" s="T612">emeːksin</ta>
            <ta e="T614" id="Seg_6898" s="T613">kös-An</ta>
            <ta e="T615" id="Seg_6899" s="T614">kel-Ar</ta>
            <ta e="T616" id="Seg_6900" s="T615">ogonnʼor-tA</ta>
            <ta e="T617" id="Seg_6901" s="T616">kaččaga</ta>
            <ta e="T618" id="Seg_6902" s="T617">da</ta>
            <ta e="T619" id="Seg_6903" s="T618">hu͡ok</ta>
            <ta e="T620" id="Seg_6904" s="T619">türme-GA</ta>
            <ta e="T621" id="Seg_6905" s="T620">hɨt-Ar</ta>
            <ta e="T622" id="Seg_6906" s="T621">ɨraːktaːgɨ</ta>
            <ta e="T623" id="Seg_6907" s="T622">emeːksin-nI</ta>
            <ta e="T624" id="Seg_6908" s="T623">üːr-An</ta>
            <ta e="T625" id="Seg_6909" s="T624">tahaːr-Ar</ta>
            <ta e="T626" id="Seg_6910" s="T625">üs</ta>
            <ta e="T627" id="Seg_6911" s="T626">kineːs</ta>
            <ta e="T628" id="Seg_6912" s="T627">baːj-tI-n</ta>
            <ta e="T629" id="Seg_6913" s="T628">hap-A</ta>
            <ta e="T630" id="Seg_6914" s="T629">tüs-An</ta>
            <ta e="T631" id="Seg_6915" s="T630">üllehin-An</ta>
            <ta e="T632" id="Seg_6916" s="T631">ɨl-Ar-LAr</ta>
            <ta e="T633" id="Seg_6917" s="T632">biːr</ta>
            <ta e="T634" id="Seg_6918" s="T633">öldüːn-nI</ta>
            <ta e="T635" id="Seg_6919" s="T634">kaːl-TAr-Ar-LAr</ta>
            <ta e="T636" id="Seg_6920" s="T635">kɨhɨn-tA</ta>
            <ta e="T637" id="Seg_6921" s="T636">baran-An</ta>
            <ta e="T638" id="Seg_6922" s="T637">poroku͡ot</ta>
            <ta e="T639" id="Seg_6923" s="T638">kel-AːktAː-BIT</ta>
            <ta e="T640" id="Seg_6924" s="T639">üːhe-ttAn</ta>
            <ta e="T641" id="Seg_6925" s="T640">dʼe</ta>
            <ta e="T642" id="Seg_6926" s="T641">kör</ta>
            <ta e="T643" id="Seg_6927" s="T642">er-GI-n</ta>
            <ta e="T644" id="Seg_6928" s="T643">di͡e-Ar-LAr</ta>
            <ta e="T645" id="Seg_6929" s="T644">ogonnʼor</ta>
            <ta e="T646" id="Seg_6930" s="T645">karak-tA</ta>
            <ta e="T647" id="Seg_6931" s="T646">iːn</ta>
            <ta e="T648" id="Seg_6932" s="T647">is-tI-GAr</ta>
            <ta e="T649" id="Seg_6933" s="T648">kiːr-BIT</ta>
            <ta e="T650" id="Seg_6934" s="T649">ahaː-A-t-BAtAK-LAr</ta>
            <ta e="T651" id="Seg_6935" s="T650">ogut-An</ta>
            <ta e="T652" id="Seg_6936" s="T651">kaːl-BIT</ta>
            <ta e="T653" id="Seg_6937" s="T652">poroku͡ot</ta>
            <ta e="T654" id="Seg_6938" s="T653">is-tI-GAr</ta>
            <ta e="T655" id="Seg_6939" s="T654">bɨrak-BIT-LAr</ta>
            <ta e="T656" id="Seg_6940" s="T655">nöŋü͡ö</ta>
            <ta e="T657" id="Seg_6941" s="T656">ɨraːktaːgɨ-GA</ta>
            <ta e="T658" id="Seg_6942" s="T657">ilt-Ar-LAr</ta>
            <ta e="T659" id="Seg_6943" s="T658">nosilka-nAn</ta>
            <ta e="T660" id="Seg_6944" s="T659">killer-Ar-LAr</ta>
            <ta e="T661" id="Seg_6945" s="T660">öl-An</ta>
            <ta e="T662" id="Seg_6946" s="T661">er-Ar</ta>
            <ta e="T663" id="Seg_6947" s="T662">kihi-nI</ta>
            <ta e="T664" id="Seg_6948" s="T663">kaja</ta>
            <ta e="T665" id="Seg_6949" s="T664">kajdak</ta>
            <ta e="T666" id="Seg_6950" s="T665">huːt-LAː-IAK-m=Ij</ta>
            <ta e="T667" id="Seg_6951" s="T666">öl-BIT</ta>
            <ta e="T668" id="Seg_6952" s="T667">kihi-nI</ta>
            <ta e="T669" id="Seg_6953" s="T668">ilt-I-ŋ</ta>
            <ta e="T670" id="Seg_6954" s="T669">bolʼnica-GA</ta>
            <ta e="T671" id="Seg_6955" s="T670">ahaː-A-t-TInnAr</ta>
            <ta e="T672" id="Seg_6956" s="T671">kör-TInnAr</ta>
            <ta e="T673" id="Seg_6957" s="T672">du͡oktuːr-LAr</ta>
            <ta e="T674" id="Seg_6958" s="T673">emteː-Ar-LAr</ta>
            <ta e="T675" id="Seg_6959" s="T674">erge</ta>
            <ta e="T676" id="Seg_6960" s="T675">beje-tA</ta>
            <ta e="T677" id="Seg_6961" s="T676">bu͡ol-Ar</ta>
            <ta e="T678" id="Seg_6962" s="T677">du͡oktuːr-LAr</ta>
            <ta e="T679" id="Seg_6963" s="T678">ɨjɨt-Ar-LAr</ta>
            <ta e="T680" id="Seg_6964" s="T679">buruj-I-ŋ</ta>
            <ta e="T681" id="Seg_6965" s="T680">du͡o</ta>
            <ta e="T682" id="Seg_6966" s="T681">kütüreː-Ar-LAr</ta>
            <ta e="T683" id="Seg_6967" s="T682">du͡o</ta>
            <ta e="T684" id="Seg_6968" s="T683">ogonnʼor</ta>
            <ta e="T685" id="Seg_6969" s="T684">kepseː-Ar</ta>
            <ta e="T686" id="Seg_6970" s="T685">barɨ-tI-n</ta>
            <ta e="T687" id="Seg_6971" s="T686">du͡oktuːr-LAr</ta>
            <ta e="T688" id="Seg_6972" s="T687">kaːn</ta>
            <ta e="T689" id="Seg_6973" s="T688">ɨl-Ar-LAr</ta>
            <ta e="T690" id="Seg_6974" s="T689">buruj-LAːK</ta>
            <ta e="T691" id="Seg_6975" s="T690">bu͡ol-TAK-GInA</ta>
            <ta e="T692" id="Seg_6976" s="T691">kaːn-I-ŋ</ta>
            <ta e="T693" id="Seg_6977" s="T692">atɨn</ta>
            <ta e="T694" id="Seg_6978" s="T693">bu͡ol-IAK.[tA]</ta>
            <ta e="T695" id="Seg_6979" s="T694">di͡e-Ar-LAr</ta>
            <ta e="T696" id="Seg_6980" s="T695">biːr-kAːN</ta>
            <ta e="T697" id="Seg_6981" s="T696">da</ta>
            <ta e="T698" id="Seg_6982" s="T697">kaːpelʼka</ta>
            <ta e="T699" id="Seg_6983" s="T698">buruj-tA</ta>
            <ta e="T700" id="Seg_6984" s="T699">hu͡ok</ta>
            <ta e="T701" id="Seg_6985" s="T700">e-BIT</ta>
            <ta e="T702" id="Seg_6986" s="T701">huruk</ta>
            <ta e="T703" id="Seg_6987" s="T702">bi͡er-Ar-LAr</ta>
            <ta e="T704" id="Seg_6988" s="T703">bu-nI</ta>
            <ta e="T705" id="Seg_6989" s="T704">huːt-GA</ta>
            <ta e="T706" id="Seg_6990" s="T705">pireːme</ta>
            <ta e="T707" id="Seg_6991" s="T706">bi͡er-Aːr</ta>
            <ta e="T708" id="Seg_6992" s="T707">kineːs-LAr</ta>
            <ta e="T709" id="Seg_6993" s="T708">kaːn-LArI-n</ta>
            <ta e="T710" id="Seg_6994" s="T709">emi͡e</ta>
            <ta e="T711" id="Seg_6995" s="T710">ɨl-IAK-BIt</ta>
            <ta e="T712" id="Seg_6996" s="T711">di͡e-Ar-LAr</ta>
            <ta e="T713" id="Seg_6997" s="T712">huːt</ta>
            <ta e="T714" id="Seg_6998" s="T713">bu͡ol-Ar</ta>
            <ta e="T715" id="Seg_6999" s="T714">kineːs-LAr</ta>
            <ta e="T716" id="Seg_7000" s="T715">maŋnajgɨ</ta>
            <ta e="T717" id="Seg_7001" s="T716">ɨraːktaːgɨ</ta>
            <ta e="T718" id="Seg_7002" s="T717">elbek</ta>
            <ta e="T719" id="Seg_7003" s="T718">dʼon</ta>
            <ta e="T720" id="Seg_7004" s="T719">munnʼus-n-I-BIT</ta>
            <ta e="T721" id="Seg_7005" s="T720">ɨraːktaːgɨ</ta>
            <ta e="T722" id="Seg_7006" s="T721">et-Ar</ta>
            <ta e="T723" id="Seg_7007" s="T722">ös-tI-n-tɨl-tI-n</ta>
            <ta e="T724" id="Seg_7008" s="T723">ihit-IAK-GA</ta>
            <ta e="T725" id="Seg_7009" s="T724">di͡e-Ar</ta>
            <ta e="T726" id="Seg_7010" s="T725">ogonnʼor</ta>
            <ta e="T727" id="Seg_7011" s="T726">kepseː-Ar</ta>
            <ta e="T728" id="Seg_7012" s="T727">min</ta>
            <ta e="T729" id="Seg_7013" s="T728">daːse</ta>
            <ta e="T730" id="Seg_7014" s="T729">kihi-nI</ta>
            <ta e="T731" id="Seg_7015" s="T730">kɨtta</ta>
            <ta e="T732" id="Seg_7016" s="T731">ölörös-I-BAtAK</ta>
            <ta e="T733" id="Seg_7017" s="T732">kihi-BIn</ta>
            <ta e="T734" id="Seg_7018" s="T733">ɨsparaːpka-tI-n</ta>
            <ta e="T735" id="Seg_7019" s="T734">bi͡er-Ar</ta>
            <ta e="T736" id="Seg_7020" s="T735">bu</ta>
            <ta e="T737" id="Seg_7021" s="T736">kihi</ta>
            <ta e="T738" id="Seg_7022" s="T737">kaːpelʼka</ta>
            <ta e="T739" id="Seg_7023" s="T738">da</ta>
            <ta e="T740" id="Seg_7024" s="T739">buruj-tA</ta>
            <ta e="T741" id="Seg_7025" s="T740">hu͡ok</ta>
            <ta e="T742" id="Seg_7026" s="T741">kineːs-LAr</ta>
            <ta e="T743" id="Seg_7027" s="T742">kaːn-LArI-n</ta>
            <ta e="T744" id="Seg_7028" s="T743">ɨl-Ar-LAr</ta>
            <ta e="T745" id="Seg_7029" s="T744">haːmaj</ta>
            <ta e="T746" id="Seg_7030" s="T745">kosuːn-tI-n</ta>
            <ta e="T747" id="Seg_7031" s="T746">kaːn-tA</ta>
            <ta e="T748" id="Seg_7032" s="T747">ile</ta>
            <ta e="T749" id="Seg_7033" s="T748">atɨn</ta>
            <ta e="T750" id="Seg_7034" s="T749">kineːs-LAr</ta>
            <ta e="T751" id="Seg_7035" s="T750">gi͡en-LArA</ta>
            <ta e="T752" id="Seg_7036" s="T751">čipčik</ta>
            <ta e="T753" id="Seg_7037" s="T752">atɨn</ta>
            <ta e="T754" id="Seg_7038" s="T753">ɨraːktaːgɨ</ta>
            <ta e="T755" id="Seg_7039" s="T754">dʼe</ta>
            <ta e="T756" id="Seg_7040" s="T755">ɨgaj-Ar</ta>
            <ta e="T757" id="Seg_7041" s="T756">kim</ta>
            <ta e="T758" id="Seg_7042" s="T757">hübeleː-BIT-tA=Ij</ta>
            <ta e="T759" id="Seg_7043" s="T758">kepseː-ŋ</ta>
            <ta e="T760" id="Seg_7044" s="T759">tɨ͡a</ta>
            <ta e="T761" id="Seg_7045" s="T760">kineːs-tA</ta>
            <ta e="T762" id="Seg_7046" s="T761">kepseː-Ar</ta>
            <ta e="T763" id="Seg_7047" s="T762">dʼe</ta>
            <ta e="T764" id="Seg_7048" s="T763">kirdik-tA</ta>
            <ta e="T765" id="Seg_7049" s="T764">bihigi</ta>
            <ta e="T766" id="Seg_7050" s="T765">buruj-LAːK-BIt</ta>
            <ta e="T767" id="Seg_7051" s="T766">haːmaj</ta>
            <ta e="T768" id="Seg_7052" s="T767">ulakan</ta>
            <ta e="T769" id="Seg_7053" s="T768">buruj-LAːK</ta>
            <ta e="T770" id="Seg_7054" s="T769">dʼuraːk</ta>
            <ta e="T771" id="Seg_7055" s="T770">kineːs-tA</ta>
            <ta e="T772" id="Seg_7056" s="T771">min-n</ta>
            <ta e="T773" id="Seg_7057" s="T772">kej-IAK</ta>
            <ta e="T774" id="Seg_7058" s="T773">baːr-LArA</ta>
            <ta e="T775" id="Seg_7059" s="T774">kuttan-An</ta>
            <ta e="T777" id="Seg_7060" s="T776">bi͡er-BIT-I-m</ta>
            <ta e="T778" id="Seg_7061" s="T777">ɨraːktaːgɨ</ta>
            <ta e="T779" id="Seg_7062" s="T778">dʼe</ta>
            <ta e="T780" id="Seg_7063" s="T779">ogonnʼor</ta>
            <ta e="T781" id="Seg_7064" s="T780">en</ta>
            <ta e="T782" id="Seg_7065" s="T781">buruj-I-ŋ</ta>
            <ta e="T783" id="Seg_7066" s="T782">hu͡ok</ta>
            <ta e="T784" id="Seg_7067" s="T783">e-BIT</ta>
            <ta e="T785" id="Seg_7068" s="T784">kaːjɨː-GA</ta>
            <ta e="T786" id="Seg_7069" s="T785">en-n</ta>
            <ta e="T787" id="Seg_7070" s="T786">huːt-LAː-BIT</ta>
            <ta e="T788" id="Seg_7071" s="T787">ɨraːktaːgɨ-nI</ta>
            <ta e="T789" id="Seg_7072" s="T788">olort-IAK-m</ta>
            <ta e="T790" id="Seg_7073" s="T789">en</ta>
            <ta e="T791" id="Seg_7074" s="T790">gini</ta>
            <ta e="T792" id="Seg_7075" s="T791">onnu-GAr</ta>
            <ta e="T793" id="Seg_7076" s="T792">ɨraːktaːgɨ-nAn</ta>
            <ta e="T794" id="Seg_7077" s="T793">bar-IAK-ŋ</ta>
            <ta e="T795" id="Seg_7078" s="T794">kineːs-LAr-nI</ta>
            <ta e="T796" id="Seg_7079" s="T795">onno</ta>
            <ta e="T797" id="Seg_7080" s="T796">tij-An</ta>
            <ta e="T798" id="Seg_7081" s="T797">huːt-LAː-Aːr</ta>
            <ta e="T799" id="Seg_7082" s="T798">di͡e-BIT</ta>
            <ta e="T800" id="Seg_7083" s="T799">kamnaː-An</ta>
            <ta e="T801" id="Seg_7084" s="T800">kaːl-Ar-LAr</ta>
            <ta e="T802" id="Seg_7085" s="T801">töttörü</ta>
            <ta e="T803" id="Seg_7086" s="T802">ogonnʼor</ta>
            <ta e="T804" id="Seg_7087" s="T803">olok-tI-GAr</ta>
            <ta e="T805" id="Seg_7088" s="T804">kel-An</ta>
            <ta e="T806" id="Seg_7089" s="T805">kineːs</ta>
            <ta e="T807" id="Seg_7090" s="T806">bu͡ol-Ar</ta>
            <ta e="T808" id="Seg_7091" s="T807">u͡ol-tA</ta>
            <ta e="T809" id="Seg_7092" s="T808">ulaːt-An</ta>
            <ta e="T810" id="Seg_7093" s="T809">pireːme-pireːme</ta>
            <ta e="T811" id="Seg_7094" s="T810">bukatɨːr</ta>
            <ta e="T812" id="Seg_7095" s="T811">bu͡ol-BIT</ta>
            <ta e="T813" id="Seg_7096" s="T812">gini</ta>
            <ta e="T814" id="Seg_7097" s="T813">hu͡ok-InA</ta>
            <ta e="T815" id="Seg_7098" s="T814">ölör-AːrI</ta>
            <ta e="T816" id="Seg_7099" s="T815">gɨn-BIT-LAr</ta>
            <ta e="T817" id="Seg_7100" s="T816">aga-tI-GAr</ta>
            <ta e="T818" id="Seg_7101" s="T817">talɨ</ta>
            <ta e="T819" id="Seg_7102" s="T818">bu͡ol-IAK.[tA]</ta>
            <ta e="T820" id="Seg_7103" s="T819">di͡e-An</ta>
            <ta e="T821" id="Seg_7104" s="T820">u͡ol</ta>
            <ta e="T822" id="Seg_7105" s="T821">biːr</ta>
            <ta e="T823" id="Seg_7106" s="T822">kihi-nI</ta>
            <ta e="T824" id="Seg_7107" s="T823">tüŋner-I</ta>
            <ta e="T825" id="Seg_7108" s="T824">ogus-I-BIT</ta>
            <ta e="T826" id="Seg_7109" s="T825">ol-nI</ta>
            <ta e="T827" id="Seg_7110" s="T826">kaːjɨː-GA</ta>
            <ta e="T828" id="Seg_7111" s="T827">olort-BIT-LAr</ta>
            <ta e="T829" id="Seg_7112" s="T828">kineːs-LArI-n</ta>
            <ta e="T830" id="Seg_7113" s="T829">dʼüːlleː-Ar</ta>
            <ta e="T831" id="Seg_7114" s="T830">haːmaj</ta>
            <ta e="T832" id="Seg_7115" s="T831">kineːs-tI-n</ta>
            <ta e="T833" id="Seg_7116" s="T832">dʼuraːk</ta>
            <ta e="T834" id="Seg_7117" s="T833">kineːs-tI-LIːN</ta>
            <ta e="T835" id="Seg_7118" s="T834">türme-GA</ta>
            <ta e="T836" id="Seg_7119" s="T835">olort-Ar</ta>
            <ta e="T837" id="Seg_7120" s="T836">tɨ͡a</ta>
            <ta e="T838" id="Seg_7121" s="T837">kineːs-tI-n</ta>
            <ta e="T839" id="Seg_7122" s="T838">kineːs-tI-ttAn</ta>
            <ta e="T840" id="Seg_7123" s="T839">ugul-Ar</ta>
            <ta e="T841" id="Seg_7124" s="T840">u͡ol-tI-n</ta>
            <ta e="T842" id="Seg_7125" s="T841">haːmaj</ta>
            <ta e="T843" id="Seg_7126" s="T842">kineːs-tA</ta>
            <ta e="T844" id="Seg_7127" s="T843">oŋor-Ar</ta>
            <ta e="T845" id="Seg_7128" s="T844">ogonnʼor</ta>
            <ta e="T846" id="Seg_7129" s="T845">itinnik</ta>
            <ta e="T847" id="Seg_7130" s="T846">kirdik-tA</ta>
            <ta e="T848" id="Seg_7131" s="T847">tagɨs-I-BIT-tA</ta>
            <ta e="T849" id="Seg_7132" s="T848">ele-tA</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_7133" s="T0">czar-PROPR-PL.[NOM]</ta>
            <ta e="T2" id="Seg_7134" s="T1">man.of.the.house-PROPR-PL.[NOM]</ta>
            <ta e="T3" id="Seg_7135" s="T2">prince-PROPR-PL.[NOM]</ta>
            <ta e="T4" id="Seg_7136" s="T3">be-INFER-3SG</ta>
            <ta e="T5" id="Seg_7137" s="T4">this</ta>
            <ta e="T6" id="Seg_7138" s="T5">human.being.[NOM]</ta>
            <ta e="T7" id="Seg_7139" s="T6">individually</ta>
            <ta e="T8" id="Seg_7140" s="T7">live-PRS.[3SG]</ta>
            <ta e="T9" id="Seg_7141" s="T8">brave.[NOM]</ta>
            <ta e="T10" id="Seg_7142" s="T9">only</ta>
            <ta e="T11" id="Seg_7143" s="T10">alone-INTNS.[NOM]</ta>
            <ta e="T12" id="Seg_7144" s="T11">and</ta>
            <ta e="T13" id="Seg_7145" s="T12">child-POSS</ta>
            <ta e="T14" id="Seg_7146" s="T13">NEG.[3SG]</ta>
            <ta e="T15" id="Seg_7147" s="T14">year-DAT/LOC</ta>
            <ta e="T16" id="Seg_7148" s="T15">once</ta>
            <ta e="T17" id="Seg_7149" s="T16">pay-PRS.[3SG]</ta>
            <ta e="T18" id="Seg_7150" s="T17">tax-3SG-ACC</ta>
            <ta e="T19" id="Seg_7151" s="T18">sea.[NOM]</ta>
            <ta e="T20" id="Seg_7152" s="T19">edge-3SG-DAT/LOC</ta>
            <ta e="T21" id="Seg_7153" s="T20">real</ta>
            <ta e="T22" id="Seg_7154" s="T21">tundra-DAT/LOC</ta>
            <ta e="T23" id="Seg_7155" s="T22">live-PRS.[3SG]</ta>
            <ta e="T24" id="Seg_7156" s="T23">sea.[NOM]</ta>
            <ta e="T25" id="Seg_7157" s="T24">deadwood-3SG-ACC</ta>
            <ta e="T26" id="Seg_7158" s="T25">heat-EP-MED-PRS.[3SG]</ta>
            <ta e="T27" id="Seg_7159" s="T26">long</ta>
            <ta e="T28" id="Seg_7160" s="T27">live-PST2.[3SG]</ta>
            <ta e="T29" id="Seg_7161" s="T28">age-EP-PST2.[3SG]</ta>
            <ta e="T30" id="Seg_7162" s="T29">year-3SG.[NOM]</ta>
            <ta e="T31" id="Seg_7163" s="T30">spring-INCH-CVB.SEQ</ta>
            <ta e="T32" id="Seg_7164" s="T31">go-PRS.[3SG]</ta>
            <ta e="T33" id="Seg_7165" s="T32">old.woman-3SG-DAT/LOC</ta>
            <ta e="T34" id="Seg_7166" s="T33">speak-PRS.[3SG]</ta>
            <ta e="T35" id="Seg_7167" s="T34">year-1PL.[NOM]</ta>
            <ta e="T36" id="Seg_7168" s="T35">melt-PST1-3SG</ta>
            <ta e="T37" id="Seg_7169" s="T36">EMPH</ta>
            <ta e="T38" id="Seg_7170" s="T37">wood.[NOM]</ta>
            <ta e="T39" id="Seg_7171" s="T38">pull-EP-MED-CVB.SIM</ta>
            <ta e="T40" id="Seg_7172" s="T39">go-FUT-1SG</ta>
            <ta e="T41" id="Seg_7173" s="T40">coming.year-ADJZ-DAT/LOC</ta>
            <ta e="T42" id="Seg_7174" s="T41">thirty</ta>
            <ta e="T43" id="Seg_7175" s="T42">sledge-PART</ta>
            <ta e="T44" id="Seg_7176" s="T43">prepare.[IMP.2SG]</ta>
            <ta e="T45" id="Seg_7177" s="T44">thirty</ta>
            <ta e="T46" id="Seg_7178" s="T45">sledge-INSTR</ta>
            <ta e="T47" id="Seg_7179" s="T46">sea-DAT/LOC</ta>
            <ta e="T48" id="Seg_7180" s="T47">go-CVB.SEQ</ta>
            <ta e="T49" id="Seg_7181" s="T48">stay-PRS.[3SG]</ta>
            <ta e="T50" id="Seg_7182" s="T49">EMPH-lonely</ta>
            <ta e="T51" id="Seg_7183" s="T50">sea-DAT/LOC</ta>
            <ta e="T52" id="Seg_7184" s="T51">promontory.[NOM]</ta>
            <ta e="T53" id="Seg_7185" s="T52">very-INSTR</ta>
            <ta e="T54" id="Seg_7186" s="T53">go.in-PST2.[3SG]</ta>
            <ta e="T55" id="Seg_7187" s="T54">lonely</ta>
            <ta e="T56" id="Seg_7188" s="T55">place-ABL</ta>
            <ta e="T57" id="Seg_7189" s="T56">load-MED-PST2.[3SG]</ta>
            <ta e="T58" id="Seg_7190" s="T57">high.shore-3SG-GEN</ta>
            <ta e="T59" id="Seg_7191" s="T58">upper.part-3SG-DAT/LOC</ta>
            <ta e="T60" id="Seg_7192" s="T59">one</ta>
            <ta e="T61" id="Seg_7193" s="T60">riverbed.[NOM]</ta>
            <ta e="T62" id="Seg_7194" s="T61">through</ta>
            <ta e="T63" id="Seg_7195" s="T62">come.back-EP-PST2.[3SG]</ta>
            <ta e="T64" id="Seg_7196" s="T63">this</ta>
            <ta e="T65" id="Seg_7197" s="T64">go.out-CVB.SEQ</ta>
            <ta e="T66" id="Seg_7198" s="T65">mountain-3SG-GEN</ta>
            <ta e="T67" id="Seg_7199" s="T66">upper.part-3SG-DAT/LOC</ta>
            <ta e="T68" id="Seg_7200" s="T67">tobacco.[NOM]</ta>
            <ta e="T69" id="Seg_7201" s="T68">smoke-CVB.SIM</ta>
            <ta e="T70" id="Seg_7202" s="T69">sit-PST2.[3SG]</ta>
            <ta e="T71" id="Seg_7203" s="T70">river.[NOM]</ta>
            <ta e="T72" id="Seg_7204" s="T71">to</ta>
            <ta e="T73" id="Seg_7205" s="T72">see-PST2-3SG</ta>
            <ta e="T74" id="Seg_7206" s="T73">water-DAT/LOC</ta>
            <ta e="T75" id="Seg_7207" s="T74">child.[NOM]</ta>
            <ta e="T76" id="Seg_7208" s="T75">go-PRS.[3SG]</ta>
            <ta e="T77" id="Seg_7209" s="T76">melt-PTCP.PST</ta>
            <ta e="T78" id="Seg_7210" s="T77">water-DAT/LOC</ta>
            <ta e="T79" id="Seg_7211" s="T78">foam-3PL-INSTR</ta>
            <ta e="T80" id="Seg_7212" s="T79">play-CVB.SIM</ta>
            <ta e="T81" id="Seg_7213" s="T80">go-PRS.[3SG]</ta>
            <ta e="T82" id="Seg_7214" s="T81">boy.[NOM]</ta>
            <ta e="T83" id="Seg_7215" s="T82">child.[NOM]</ta>
            <ta e="T84" id="Seg_7216" s="T83">apparently</ta>
            <ta e="T85" id="Seg_7217" s="T84">where.from</ta>
            <ta e="T86" id="Seg_7218" s="T85">and</ta>
            <ta e="T87" id="Seg_7219" s="T86">come-PST2-3SG</ta>
            <ta e="T88" id="Seg_7220" s="T87">know-PASS/REFL-EP-NEG.[3SG]</ta>
            <ta e="T89" id="Seg_7221" s="T88">be.surprised-PST2.[3SG]</ta>
            <ta e="T90" id="Seg_7222" s="T89">1SG.[NOM]</ta>
            <ta e="T91" id="Seg_7223" s="T90">child-POSS</ta>
            <ta e="T92" id="Seg_7224" s="T91">NEG-1SG</ta>
            <ta e="T93" id="Seg_7225" s="T92">EMPH</ta>
            <ta e="T94" id="Seg_7226" s="T93">take-PTCP.PST.[NOM]</ta>
            <ta e="T95" id="Seg_7227" s="T94">MOD</ta>
            <ta e="T96" id="Seg_7228" s="T95">think-CVB.SIM</ta>
            <ta e="T97" id="Seg_7229" s="T96">think-PST2.[3SG]</ta>
            <ta e="T98" id="Seg_7230" s="T97">child-3SG.[NOM]</ta>
            <ta e="T99" id="Seg_7231" s="T98">island.[NOM]</ta>
            <ta e="T100" id="Seg_7232" s="T99">sand-3SG-DAT/LOC</ta>
            <ta e="T101" id="Seg_7233" s="T100">go-PRS.[3SG]</ta>
            <ta e="T102" id="Seg_7234" s="T101">crooked</ta>
            <ta e="T103" id="Seg_7235" s="T102">tree.[NOM]</ta>
            <ta e="T104" id="Seg_7236" s="T103">there.is</ta>
            <ta e="T105" id="Seg_7237" s="T104">that-INSTR</ta>
            <ta e="T106" id="Seg_7238" s="T105">go-INFER-3SG</ta>
            <ta e="T107" id="Seg_7239" s="T106">jump-CVB.SEQ</ta>
            <ta e="T108" id="Seg_7240" s="T107">reach-PRS.[3SG]</ta>
            <ta e="T109" id="Seg_7241" s="T108">old.man.[NOM]</ta>
            <ta e="T110" id="Seg_7242" s="T109">back.part-3SG-ABL</ta>
            <ta e="T111" id="Seg_7243" s="T110">catch-CVB.SEQ</ta>
            <ta e="T112" id="Seg_7244" s="T111">take-CVB.SEQ</ta>
            <ta e="T113" id="Seg_7245" s="T112">long.coat-3SG-GEN</ta>
            <ta e="T114" id="Seg_7246" s="T113">inside-3SG-DAT/LOC</ta>
            <ta e="T115" id="Seg_7247" s="T114">stick-PST2.[3SG]</ta>
            <ta e="T116" id="Seg_7248" s="T115">gird-EP-REFL-CVB.SEQ</ta>
            <ta e="T117" id="Seg_7249" s="T116">throw-PST2.[3SG]</ta>
            <ta e="T118" id="Seg_7250" s="T117">well</ta>
            <ta e="T119" id="Seg_7251" s="T118">only</ta>
            <ta e="T120" id="Seg_7252" s="T119">hit.the.road-CAP.[3SG]</ta>
            <ta e="T121" id="Seg_7253" s="T120">sound.[NOM]</ta>
            <ta e="T122" id="Seg_7254" s="T121">mumbling-3SG.[NOM]</ta>
            <ta e="T123" id="Seg_7255" s="T122">be-PRS.[3SG]</ta>
            <ta e="T124" id="Seg_7256" s="T123">stand-PST2.[3SG]</ta>
            <ta e="T125" id="Seg_7257" s="T124">see-PST2-3SG</ta>
            <ta e="T126" id="Seg_7258" s="T125">complete</ta>
            <ta e="T127" id="Seg_7259" s="T126">settlement.[NOM]</ta>
            <ta e="T128" id="Seg_7260" s="T127">human.being-3SG.[NOM]</ta>
            <ta e="T129" id="Seg_7261" s="T128">die-PTCP.PST</ta>
            <ta e="T130" id="Seg_7262" s="T129">place-3SG.[NOM]</ta>
            <ta e="T131" id="Seg_7263" s="T130">be-PST2.[3SG]</ta>
            <ta e="T132" id="Seg_7264" s="T131">hey</ta>
            <ta e="T133" id="Seg_7265" s="T132">this-PL-ABL</ta>
            <ta e="T134" id="Seg_7266" s="T133">remain-CVB.SEQ</ta>
            <ta e="T135" id="Seg_7267" s="T134">stay-PST2.[3SG]</ta>
            <ta e="T136" id="Seg_7268" s="T135">child.[NOM]</ta>
            <ta e="T137" id="Seg_7269" s="T136">MOD</ta>
            <ta e="T138" id="Seg_7270" s="T137">one</ta>
            <ta e="T139" id="Seg_7271" s="T138">back.part-1PL-PROPR</ta>
            <ta e="T140" id="Seg_7272" s="T139">front.reindeer-3SG-ACC</ta>
            <ta e="T141" id="Seg_7273" s="T140">bring-CVB.SEQ</ta>
            <ta e="T142" id="Seg_7274" s="T141">kill-CVB.SEQ</ta>
            <ta e="T143" id="Seg_7275" s="T142">throw-PRS.[3SG]</ta>
            <ta e="T144" id="Seg_7276" s="T143">child-2PL-ACC</ta>
            <ta e="T145" id="Seg_7277" s="T144">carry-PST1-1SG</ta>
            <ta e="T146" id="Seg_7278" s="T145">this</ta>
            <ta e="T147" id="Seg_7279" s="T146">child-2PL.[NOM]</ta>
            <ta e="T148" id="Seg_7280" s="T147">trace-3SG.[NOM]</ta>
            <ta e="T149" id="Seg_7281" s="T148">one</ta>
            <ta e="T150" id="Seg_7282" s="T149">reindeer-ACC</ta>
            <ta e="T151" id="Seg_7283" s="T150">throw-PST1-1SG</ta>
            <ta e="T152" id="Seg_7284" s="T151">say-PRS.[3SG]</ta>
            <ta e="T153" id="Seg_7285" s="T152">bring-PST1-3SG</ta>
            <ta e="T154" id="Seg_7286" s="T153">house-3SG-DAT/LOC</ta>
            <ta e="T155" id="Seg_7287" s="T154">old.woman-3SG-DAT/LOC</ta>
            <ta e="T156" id="Seg_7288" s="T155">tell-PRS.[3SG]</ta>
            <ta e="T157" id="Seg_7289" s="T156">three</ta>
            <ta e="T158" id="Seg_7290" s="T157">day.[NOM]</ta>
            <ta e="T159" id="Seg_7291" s="T158">as.long.as</ta>
            <ta e="T160" id="Seg_7292" s="T159">reindeer-3PL-ACC</ta>
            <ta e="T161" id="Seg_7293" s="T160">and</ta>
            <ta e="T162" id="Seg_7294" s="T161">release-NEG.CVB.SIM</ta>
            <ta e="T163" id="Seg_7295" s="T162">this</ta>
            <ta e="T164" id="Seg_7296" s="T163">child-INSTR</ta>
            <ta e="T165" id="Seg_7297" s="T164">play-PRS-3PL</ta>
            <ta e="T166" id="Seg_7298" s="T165">caravan-3SG.[NOM]</ta>
            <ta e="T167" id="Seg_7299" s="T166">every-3SG.[NOM]</ta>
            <ta e="T168" id="Seg_7300" s="T167">harness-PASS/REFL-CVB.SIM</ta>
            <ta e="T169" id="Seg_7301" s="T168">lie-PTCP.PRS</ta>
            <ta e="T170" id="Seg_7302" s="T169">be-PST2.[3SG]</ta>
            <ta e="T171" id="Seg_7303" s="T170">what.[NOM]</ta>
            <ta e="T172" id="Seg_7304" s="T171">NEG</ta>
            <ta e="T173" id="Seg_7305" s="T172">know-PASS/REFL-EP-NEG.[3SG]</ta>
            <ta e="T174" id="Seg_7306" s="T173">child-3SG.[NOM]</ta>
            <ta e="T175" id="Seg_7307" s="T174">run-CVB.SIM</ta>
            <ta e="T176" id="Seg_7308" s="T175">go-PTCP.PRS</ta>
            <ta e="T177" id="Seg_7309" s="T176">be-PST2.[3SG]</ta>
            <ta e="T178" id="Seg_7310" s="T177">grow-PST2.[3SG]</ta>
            <ta e="T179" id="Seg_7311" s="T178">watch-CVB.SIM</ta>
            <ta e="T180" id="Seg_7312" s="T179">sit-TEMP-3SG</ta>
            <ta e="T181" id="Seg_7313" s="T180">sledge-DAT/LOC</ta>
            <ta e="T182" id="Seg_7314" s="T181">five</ta>
            <ta e="T183" id="Seg_7315" s="T182">white</ta>
            <ta e="T184" id="Seg_7316" s="T183">male.reindeer-PROPR</ta>
            <ta e="T185" id="Seg_7317" s="T184">EMPH-white</ta>
            <ta e="T186" id="Seg_7318" s="T185">clothes-PROPR</ta>
            <ta e="T187" id="Seg_7319" s="T186">human.being.[NOM]</ta>
            <ta e="T188" id="Seg_7320" s="T187">drive-PRS.[3SG]</ta>
            <ta e="T189" id="Seg_7321" s="T188">prince.[NOM]</ta>
            <ta e="T190" id="Seg_7322" s="T189">be-PST2.[3SG]</ta>
            <ta e="T191" id="Seg_7323" s="T190">well</ta>
            <ta e="T192" id="Seg_7324" s="T191">what-1SG-ACC</ta>
            <ta e="T193" id="Seg_7325" s="T192">give-FUT-1SG=Q</ta>
            <ta e="T194" id="Seg_7326" s="T193">think-CVB.SIM</ta>
            <ta e="T195" id="Seg_7327" s="T194">think-PRS.[3SG]</ta>
            <ta e="T196" id="Seg_7328" s="T195">old.man.[NOM]</ta>
            <ta e="T197" id="Seg_7329" s="T196">how</ta>
            <ta e="T198" id="Seg_7330" s="T197">live-PST1-2SG</ta>
            <ta e="T199" id="Seg_7331" s="T198">how</ta>
            <ta e="T200" id="Seg_7332" s="T199">such</ta>
            <ta e="T201" id="Seg_7333" s="T200">long</ta>
            <ta e="T202" id="Seg_7334" s="T201">be-PST1-1SG</ta>
            <ta e="T203" id="Seg_7335" s="T202">child-VBZ-PST2-2SG</ta>
            <ta e="T204" id="Seg_7336" s="T203">Q</ta>
            <ta e="T205" id="Seg_7337" s="T204">say-PRS.[3SG]</ta>
            <ta e="T206" id="Seg_7338" s="T205">prince.[NOM]</ta>
            <ta e="T207" id="Seg_7339" s="T206">age-PTCP.PRS</ta>
            <ta e="T208" id="Seg_7340" s="T207">age-1PL-DAT/LOC</ta>
            <ta e="T209" id="Seg_7341" s="T208">child-VBZ-PST1-1PL</ta>
            <ta e="T210" id="Seg_7342" s="T209">say-PRS.[3SG]</ta>
            <ta e="T211" id="Seg_7343" s="T210">old.man.[NOM]</ta>
            <ta e="T212" id="Seg_7344" s="T211">well</ta>
            <ta e="T213" id="Seg_7345" s="T212">good.[NOM]</ta>
            <ta e="T214" id="Seg_7346" s="T213">tax.[NOM]</ta>
            <ta e="T215" id="Seg_7347" s="T214">gather-CVB.SIM</ta>
            <ta e="T216" id="Seg_7348" s="T215">come-PST1-1SG</ta>
            <ta e="T217" id="Seg_7349" s="T216">say-PRS.[3SG]</ta>
            <ta e="T218" id="Seg_7350" s="T217">prince.[NOM]</ta>
            <ta e="T219" id="Seg_7351" s="T218">old.man.[NOM]</ta>
            <ta e="T220" id="Seg_7352" s="T219">how.much</ta>
            <ta e="T221" id="Seg_7353" s="T220">INDEF</ta>
            <ta e="T222" id="Seg_7354" s="T221">sack.[NOM]</ta>
            <ta e="T223" id="Seg_7355" s="T222">polar.fox-ACC</ta>
            <ta e="T224" id="Seg_7356" s="T223">give-PRS.[3SG]</ta>
            <ta e="T225" id="Seg_7357" s="T224">prince-3SG.[NOM]</ta>
            <ta e="T226" id="Seg_7358" s="T225">be.happy-PRS.[3SG]</ta>
            <ta e="T227" id="Seg_7359" s="T226">rest-3SG-ACC</ta>
            <ta e="T228" id="Seg_7360" s="T227">money.[NOM]</ta>
            <ta e="T229" id="Seg_7361" s="T228">send-FUT-1SG</ta>
            <ta e="T230" id="Seg_7362" s="T229">say-PRS.[3SG]</ta>
            <ta e="T231" id="Seg_7363" s="T230">prince-3SG.[NOM]</ta>
            <ta e="T232" id="Seg_7364" s="T231">hit.the.road-CAP.[3SG]</ta>
            <ta e="T233" id="Seg_7365" s="T232">whereto</ta>
            <ta e="T234" id="Seg_7366" s="T233">INDEF</ta>
            <ta e="T235" id="Seg_7367" s="T234">Nenets</ta>
            <ta e="T236" id="Seg_7368" s="T235">prince-3SG-DAT/LOC</ta>
            <ta e="T237" id="Seg_7369" s="T236">run-PST1-3SG</ta>
            <ta e="T238" id="Seg_7370" s="T237">friend-3SG-DAT/LOC</ta>
            <ta e="T239" id="Seg_7371" s="T238">prince.[NOM]</ta>
            <ta e="T240" id="Seg_7372" s="T239">prince-3SG-DAT/LOC</ta>
            <ta e="T241" id="Seg_7373" s="T240">come-PST2.[3SG]</ta>
            <ta e="T242" id="Seg_7374" s="T241">spirit-VBZ-CVB.SIM</ta>
            <ta e="T243" id="Seg_7375" s="T242">lie-PRS-3PL</ta>
            <ta e="T244" id="Seg_7376" s="T243">how.much</ta>
            <ta e="T245" id="Seg_7377" s="T244">day.[NOM]</ta>
            <ta e="T246" id="Seg_7378" s="T245">spirit-VBZ-CVB.SIM</ta>
            <ta e="T247" id="Seg_7379" s="T246">lie-PST2-3PL</ta>
            <ta e="T248" id="Seg_7380" s="T247">that</ta>
            <ta e="T249" id="Seg_7381" s="T248">live-CVB.SEQ</ta>
            <ta e="T250" id="Seg_7382" s="T249">news.[NOM]</ta>
            <ta e="T251" id="Seg_7383" s="T250">tell-PST2.[3SG]</ta>
            <ta e="T252" id="Seg_7384" s="T251">friend-3SG-DAT/LOC</ta>
            <ta e="T253" id="Seg_7385" s="T252">well-well</ta>
            <ta e="T254" id="Seg_7386" s="T253">1SG.[NOM]</ta>
            <ta e="T255" id="Seg_7387" s="T254">human.being.[NOM]</ta>
            <ta e="T256" id="Seg_7388" s="T255">be.afraid-PTCP.PRS</ta>
            <ta e="T257" id="Seg_7389" s="T256">human.being-PROPR-1SG</ta>
            <ta e="T258" id="Seg_7390" s="T257">tax-3SG-ACC</ta>
            <ta e="T259" id="Seg_7391" s="T258">that.[NOM]</ta>
            <ta e="T260" id="Seg_7392" s="T259">despite</ta>
            <ta e="T261" id="Seg_7393" s="T260">further</ta>
            <ta e="T262" id="Seg_7394" s="T261">pay-PRS.[3SG]</ta>
            <ta e="T263" id="Seg_7395" s="T262">that-ABL</ta>
            <ta e="T264" id="Seg_7396" s="T263">come-PST1-1SG</ta>
            <ta e="T265" id="Seg_7397" s="T264">be.surprised-PRS-1SG</ta>
            <ta e="T266" id="Seg_7398" s="T265">run-CVB.SIM</ta>
            <ta e="T267" id="Seg_7399" s="T266">go-PTCP.PRS</ta>
            <ta e="T268" id="Seg_7400" s="T267">child-VBZ-PST2.[3SG]</ta>
            <ta e="T269" id="Seg_7401" s="T268">self-3SG.[NOM]</ta>
            <ta e="T270" id="Seg_7402" s="T269">be.born-PTCP.PST</ta>
            <ta e="T271" id="Seg_7403" s="T270">child-POSS</ta>
            <ta e="T272" id="Seg_7404" s="T271">be-PST2.NEG.[3SG]</ta>
            <ta e="T273" id="Seg_7405" s="T272">where.from</ta>
            <ta e="T274" id="Seg_7406" s="T273">take-PST2-3SG</ta>
            <ta e="T275" id="Seg_7407" s="T274">be-FUT.[3SG]=Q</ta>
            <ta e="T276" id="Seg_7408" s="T275">who-ACC</ta>
            <ta e="T277" id="Seg_7409" s="T276">INDEF</ta>
            <ta e="T278" id="Seg_7410" s="T277">Q</ta>
            <ta e="T279" id="Seg_7411" s="T278">kill-CVB.SEQ</ta>
            <ta e="T280" id="Seg_7412" s="T279">take-INFER-3SG</ta>
            <ta e="T281" id="Seg_7413" s="T280">ah</ta>
            <ta e="T282" id="Seg_7414" s="T281">say-PRS.[3SG]</ta>
            <ta e="T283" id="Seg_7415" s="T282">Nenets</ta>
            <ta e="T284" id="Seg_7416" s="T283">prince-3SG.[NOM]</ta>
            <ta e="T285" id="Seg_7417" s="T284">long.ago</ta>
            <ta e="T286" id="Seg_7418" s="T285">1SG.[NOM]</ta>
            <ta e="T287" id="Seg_7419" s="T286">one</ta>
            <ta e="T288" id="Seg_7420" s="T287">settlement.[NOM]</ta>
            <ta e="T289" id="Seg_7421" s="T288">human.being-1SG.[NOM]</ta>
            <ta e="T290" id="Seg_7422" s="T289">die-PST2-3SG</ta>
            <ta e="T291" id="Seg_7423" s="T290">that-ABL</ta>
            <ta e="T292" id="Seg_7424" s="T291">one</ta>
            <ta e="T293" id="Seg_7425" s="T292">cradle-PROPR</ta>
            <ta e="T294" id="Seg_7426" s="T293">child.[NOM]</ta>
            <ta e="T295" id="Seg_7427" s="T294">stay-PST2.[3SG]</ta>
            <ta e="T296" id="Seg_7428" s="T295">message-PROPR-3SG.[NOM]</ta>
            <ta e="T297" id="Seg_7429" s="T296">that-ACC</ta>
            <ta e="T298" id="Seg_7430" s="T297">take-CAUS-PST2.NEG-EP-1SG</ta>
            <ta e="T299" id="Seg_7431" s="T298">disease.[NOM]</ta>
            <ta e="T300" id="Seg_7432" s="T299">follow-FUT.[3SG]</ta>
            <ta e="T301" id="Seg_7433" s="T300">think-CVB.SEQ-1SG</ta>
            <ta e="T302" id="Seg_7434" s="T301">that-ACC</ta>
            <ta e="T303" id="Seg_7435" s="T302">take-INFER-3SG</ta>
            <ta e="T304" id="Seg_7436" s="T303">that</ta>
            <ta e="T305" id="Seg_7437" s="T304">child-ACC</ta>
            <ta e="T306" id="Seg_7438" s="T305">hide-PTCP.PST</ta>
            <ta e="T307" id="Seg_7439" s="T306">be-PST2.[3SG]</ta>
            <ta e="T308" id="Seg_7440" s="T307">1SG-ACC</ta>
            <ta e="T309" id="Seg_7441" s="T308">kill-FUT-3PL</ta>
            <ta e="T310" id="Seg_7442" s="T309">think-CVB.SEQ</ta>
            <ta e="T311" id="Seg_7443" s="T310">well</ta>
            <ta e="T312" id="Seg_7444" s="T311">like.this</ta>
            <ta e="T313" id="Seg_7445" s="T312">advise-PRS-1SG</ta>
            <ta e="T314" id="Seg_7446" s="T313">2SG.[NOM]</ta>
            <ta e="T315" id="Seg_7447" s="T314">human.being-2SG-ABL</ta>
            <ta e="T316" id="Seg_7448" s="T315">be.afraid-EP-NEG.[IMP.2SG]</ta>
            <ta e="T317" id="Seg_7449" s="T316">that</ta>
            <ta e="T318" id="Seg_7450" s="T317">settlement.[NOM]</ta>
            <ta e="T319" id="Seg_7451" s="T318">people-3SG-ACC</ta>
            <ta e="T320" id="Seg_7452" s="T319">kill.off-CVB.SEQ</ta>
            <ta e="T321" id="Seg_7453" s="T320">after</ta>
            <ta e="T322" id="Seg_7454" s="T321">child-ACC</ta>
            <ta e="T323" id="Seg_7455" s="T322">take-PST2.[3SG]</ta>
            <ta e="T324" id="Seg_7456" s="T323">say-CVB.SEQ</ta>
            <ta e="T325" id="Seg_7457" s="T324">two</ta>
            <ta e="T326" id="Seg_7458" s="T325">prince.[NOM]</ta>
            <ta e="T327" id="Seg_7459" s="T326">czar-3SG.[NOM]</ta>
            <ta e="T328" id="Seg_7460" s="T327">complain-RECP/COLL-CVB.SEQ</ta>
            <ta e="T329" id="Seg_7461" s="T328">write-IMP.1DU</ta>
            <ta e="T330" id="Seg_7462" s="T329">then</ta>
            <ta e="T331" id="Seg_7463" s="T330">1PL.[NOM]</ta>
            <ta e="T332" id="Seg_7464" s="T331">word-1PL-ACC</ta>
            <ta e="T333" id="Seg_7465" s="T332">czar.[NOM]</ta>
            <ta e="T334" id="Seg_7466" s="T333">cut-FUT.[3SG]</ta>
            <ta e="T335" id="Seg_7467" s="T334">NEG-3SG</ta>
            <ta e="T336" id="Seg_7468" s="T335">3SG-ACC</ta>
            <ta e="T337" id="Seg_7469" s="T336">court-VBZ-EP-CAUS-FUT.[3SG]</ta>
            <ta e="T338" id="Seg_7470" s="T337">prison-DAT/LOC</ta>
            <ta e="T339" id="Seg_7471" s="T338">send-FUT-3SG</ta>
            <ta e="T340" id="Seg_7472" s="T339">1PL.[NOM]</ta>
            <ta e="T341" id="Seg_7473" s="T340">3SG.[NOM]</ta>
            <ta e="T342" id="Seg_7474" s="T341">wealth-3SG-ACC</ta>
            <ta e="T343" id="Seg_7475" s="T342">take-FUT-1PL</ta>
            <ta e="T344" id="Seg_7476" s="T343">say-PST2.[3SG]</ta>
            <ta e="T345" id="Seg_7477" s="T344">Nenets</ta>
            <ta e="T346" id="Seg_7478" s="T345">prince-3SG.[NOM]</ta>
            <ta e="T347" id="Seg_7479" s="T346">human.being-DAT/LOC</ta>
            <ta e="T348" id="Seg_7480" s="T347">suspect-PTCP.COND-DAT/LOC</ta>
            <ta e="T349" id="Seg_7481" s="T348">bad.[NOM]</ta>
            <ta e="T350" id="Seg_7482" s="T349">be-POT.3SG</ta>
            <ta e="T351" id="Seg_7483" s="T350">say-PRS.[3SG]</ta>
            <ta e="T352" id="Seg_7484" s="T351">Nganasan</ta>
            <ta e="T353" id="Seg_7485" s="T352">prince-3SG.[NOM]</ta>
            <ta e="T354" id="Seg_7486" s="T353">Dolgan</ta>
            <ta e="T355" id="Seg_7487" s="T354">prince-3SG-ACC</ta>
            <ta e="T356" id="Seg_7488" s="T355">invite-PTCP.PST</ta>
            <ta e="T357" id="Seg_7489" s="T356">be-COND.[3SG]</ta>
            <ta e="T358" id="Seg_7490" s="T357">three</ta>
            <ta e="T359" id="Seg_7491" s="T358">prince.[NOM]</ta>
            <ta e="T360" id="Seg_7492" s="T359">faith-PROPR.[NOM]</ta>
            <ta e="T361" id="Seg_7493" s="T360">be-PTCP.FUT</ta>
            <ta e="T362" id="Seg_7494" s="T361">be-PST1-3SG</ta>
            <ta e="T363" id="Seg_7495" s="T362">Nenets</ta>
            <ta e="T364" id="Seg_7496" s="T363">prince-3SG.[NOM]</ta>
            <ta e="T365" id="Seg_7497" s="T364">human.being-3SG.[NOM]</ta>
            <ta e="T366" id="Seg_7498" s="T365">send-FUT-1SG</ta>
            <ta e="T367" id="Seg_7499" s="T366">Dolgan-PL.[NOM]</ta>
            <ta e="T368" id="Seg_7500" s="T367">prince-3PL-DAT/LOC</ta>
            <ta e="T369" id="Seg_7501" s="T368">three</ta>
            <ta e="T370" id="Seg_7502" s="T369">day.and.night-EP-INSTR</ta>
            <ta e="T371" id="Seg_7503" s="T370">come-FUT.[3SG]</ta>
            <ta e="T372" id="Seg_7504" s="T371">agree-EP-MED-NEG-TEMP-3SG</ta>
            <ta e="T373" id="Seg_7505" s="T372">self-3SG-ACC</ta>
            <ta e="T374" id="Seg_7506" s="T373">kill-EP-PASS/REFL-FUT.[3SG]</ta>
            <ta e="T375" id="Seg_7507" s="T374">be.against-RECP/COLL-FUT.[3SG]</ta>
            <ta e="T376" id="Seg_7508" s="T375">NEG-3SG</ta>
            <ta e="T377" id="Seg_7509" s="T376">say-PRS.[3SG]</ta>
            <ta e="T378" id="Seg_7510" s="T377">sledge-PROPR</ta>
            <ta e="T379" id="Seg_7511" s="T378">human.being-ACC</ta>
            <ta e="T380" id="Seg_7512" s="T379">send-PST1-3SG</ta>
            <ta e="T381" id="Seg_7513" s="T380">Dolgan</ta>
            <ta e="T382" id="Seg_7514" s="T381">prince-3SG-ACC</ta>
            <ta e="T383" id="Seg_7515" s="T382">invite-CAUS-CVB.SIM</ta>
            <ta e="T384" id="Seg_7516" s="T383">three-ORD</ta>
            <ta e="T385" id="Seg_7517" s="T384">day-3SG-DAT/LOC</ta>
            <ta e="T386" id="Seg_7518" s="T385">Dolgan</ta>
            <ta e="T387" id="Seg_7519" s="T386">prince-3SG.[NOM]</ta>
            <ta e="T388" id="Seg_7520" s="T387">come-PRS.[3SG]</ta>
            <ta e="T389" id="Seg_7521" s="T388">beads.[NOM]</ta>
            <ta e="T390" id="Seg_7522" s="T389">clothes-PROPR.[NOM]</ta>
            <ta e="T391" id="Seg_7523" s="T390">three</ta>
            <ta e="T392" id="Seg_7524" s="T391">how.much</ta>
            <ta e="T393" id="Seg_7525" s="T392">day.[NOM]</ta>
            <ta e="T394" id="Seg_7526" s="T393">spirit-VBZ-PRS-3PL</ta>
            <ta e="T395" id="Seg_7527" s="T394">Nenets</ta>
            <ta e="T396" id="Seg_7528" s="T395">prince-3SG.[NOM]</ta>
            <ta e="T397" id="Seg_7529" s="T396">tell-PRS.[3SG]</ta>
            <ta e="T398" id="Seg_7530" s="T397">Nganasan</ta>
            <ta e="T399" id="Seg_7531" s="T398">human.being-3SG.[NOM]</ta>
            <ta e="T400" id="Seg_7532" s="T399">disease-ABL</ta>
            <ta e="T401" id="Seg_7533" s="T400">die-PTCP.PST</ta>
            <ta e="T402" id="Seg_7534" s="T401">people-ABL</ta>
            <ta e="T403" id="Seg_7535" s="T402">one</ta>
            <ta e="T404" id="Seg_7536" s="T403">child-ACC</ta>
            <ta e="T405" id="Seg_7537" s="T404">take-PST2.[3SG]</ta>
            <ta e="T406" id="Seg_7538" s="T405">that-ACC</ta>
            <ta e="T407" id="Seg_7539" s="T406">disease.[NOM]</ta>
            <ta e="T408" id="Seg_7540" s="T407">follow-FUT.[3SG]</ta>
            <ta e="T409" id="Seg_7541" s="T408">1PL.[NOM]</ta>
            <ta e="T410" id="Seg_7542" s="T409">czar-DAT/LOC</ta>
            <ta e="T411" id="Seg_7543" s="T410">complain-RECP/COLL-IMP.1PL</ta>
            <ta e="T412" id="Seg_7544" s="T411">three-COLL</ta>
            <ta e="T413" id="Seg_7545" s="T412">write-IMP.1PL</ta>
            <ta e="T414" id="Seg_7546" s="T413">Dolgan</ta>
            <ta e="T415" id="Seg_7547" s="T414">prince-3SG.[NOM]</ta>
            <ta e="T416" id="Seg_7548" s="T415">think-VBZ-PST2.[3SG]</ta>
            <ta e="T417" id="Seg_7549" s="T416">say-PST2.[3SG]</ta>
            <ta e="T418" id="Seg_7550" s="T417">court-VBZ-PTCP.PRS-3SG-ACC</ta>
            <ta e="T419" id="Seg_7551" s="T418">court-VBZ-NEG.CVB</ta>
            <ta e="T420" id="Seg_7552" s="T419">that.[NOM]</ta>
            <ta e="T421" id="Seg_7553" s="T420">despite</ta>
            <ta e="T422" id="Seg_7554" s="T421">suspect-PTCP.PRS</ta>
            <ta e="T423" id="Seg_7555" s="T422">horror-3SG.[NOM]</ta>
            <ta e="T424" id="Seg_7556" s="T423">very</ta>
            <ta e="T425" id="Seg_7557" s="T424">people-ACC</ta>
            <ta e="T426" id="Seg_7558" s="T425">chop-PST2.[3SG]</ta>
            <ta e="T427" id="Seg_7559" s="T426">say-CVB.SEQ</ta>
            <ta e="T428" id="Seg_7560" s="T427">orphan-ACC</ta>
            <ta e="T429" id="Seg_7561" s="T428">take-PST2-3SG</ta>
            <ta e="T430" id="Seg_7562" s="T429">what.[NOM]</ta>
            <ta e="T431" id="Seg_7563" s="T430">guilt-3SG.[NOM]=Q</ta>
            <ta e="T432" id="Seg_7564" s="T431">court-DAT/LOC</ta>
            <ta e="T433" id="Seg_7565" s="T432">give-PTCP.FUT-1PL-ACC</ta>
            <ta e="T434" id="Seg_7566" s="T433">say-PST2.[3SG]</ta>
            <ta e="T435" id="Seg_7567" s="T434">Nenets</ta>
            <ta e="T436" id="Seg_7568" s="T435">prince-3SG.[NOM]</ta>
            <ta e="T437" id="Seg_7569" s="T436">2SG.[NOM]</ta>
            <ta e="T438" id="Seg_7570" s="T437">protect-PRS-2SG</ta>
            <ta e="T439" id="Seg_7571" s="T438">Q</ta>
            <ta e="T440" id="Seg_7572" s="T439">that</ta>
            <ta e="T441" id="Seg_7573" s="T440">human.being-ACC</ta>
            <ta e="T442" id="Seg_7574" s="T441">then</ta>
            <ta e="T443" id="Seg_7575" s="T442">1PL.[NOM]</ta>
            <ta e="T444" id="Seg_7576" s="T443">2SG-ACC</ta>
            <ta e="T445" id="Seg_7577" s="T444">chop-PRS-1PL</ta>
            <ta e="T446" id="Seg_7578" s="T445">wealth-2SG-ACC</ta>
            <ta e="T447" id="Seg_7579" s="T446">take-FUT-1PL</ta>
            <ta e="T448" id="Seg_7580" s="T447">say-PRS.[3SG]</ta>
            <ta e="T449" id="Seg_7581" s="T448">Dolgan</ta>
            <ta e="T450" id="Seg_7582" s="T449">prince-3SG.[NOM]</ta>
            <ta e="T451" id="Seg_7583" s="T450">startle-PST2.[3SG]</ta>
            <ta e="T452" id="Seg_7584" s="T451">kill-PTCP.FUT-AG.[NOM]</ta>
            <ta e="T453" id="Seg_7585" s="T452">people.[NOM]</ta>
            <ta e="T454" id="Seg_7586" s="T453">two-COLL.[NOM]</ta>
            <ta e="T455" id="Seg_7587" s="T454">well</ta>
            <ta e="T456" id="Seg_7588" s="T455">how</ta>
            <ta e="T457" id="Seg_7589" s="T456">advise-PRS-2PL</ta>
            <ta e="T458" id="Seg_7590" s="T457">say-PST2.[3SG]</ta>
            <ta e="T459" id="Seg_7591" s="T458">Nenets</ta>
            <ta e="T460" id="Seg_7592" s="T459">prince-3SG.[NOM]</ta>
            <ta e="T461" id="Seg_7593" s="T460">1SG.[NOM]</ta>
            <ta e="T462" id="Seg_7594" s="T461">lord.[NOM]</ta>
            <ta e="T463" id="Seg_7595" s="T462">leader.[NOM]</ta>
            <ta e="T464" id="Seg_7596" s="T463">be-PRS-1SG</ta>
            <ta e="T465" id="Seg_7597" s="T464">complain-RECP/COLL-IMP.1PL</ta>
            <ta e="T466" id="Seg_7598" s="T465">czar-DAT/LOC</ta>
            <ta e="T467" id="Seg_7599" s="T466">this</ta>
            <ta e="T468" id="Seg_7600" s="T467">brave.[NOM]</ta>
            <ta e="T469" id="Seg_7601" s="T468">Nganasan</ta>
            <ta e="T470" id="Seg_7602" s="T469">settlement-3SG-ACC</ta>
            <ta e="T471" id="Seg_7603" s="T470">chop-CVB.SEQ</ta>
            <ta e="T472" id="Seg_7604" s="T471">after</ta>
            <ta e="T473" id="Seg_7605" s="T472">child-ACC</ta>
            <ta e="T474" id="Seg_7606" s="T473">take-PST2.[3SG]</ta>
            <ta e="T475" id="Seg_7607" s="T474">say-CVB.SEQ</ta>
            <ta e="T476" id="Seg_7608" s="T475">write-FUT-1PL</ta>
            <ta e="T477" id="Seg_7609" s="T476">1SG.[NOM]</ta>
            <ta e="T478" id="Seg_7610" s="T477">human.being-PL-EP-1SG.[NOM]</ta>
            <ta e="T479" id="Seg_7611" s="T478">go-CVB.SEQ</ta>
            <ta e="T480" id="Seg_7612" s="T479">die-PTCP.PST</ta>
            <ta e="T481" id="Seg_7613" s="T480">people-ACC</ta>
            <ta e="T482" id="Seg_7614" s="T481">shoot-FREQ-FUT-3PL</ta>
            <ta e="T483" id="Seg_7615" s="T482">commission.[NOM]</ta>
            <ta e="T484" id="Seg_7616" s="T483">come-TEMP-3SG</ta>
            <ta e="T485" id="Seg_7617" s="T484">find-CVB.SEQ</ta>
            <ta e="T486" id="Seg_7618" s="T485">take-FUT.[3SG]</ta>
            <ta e="T487" id="Seg_7619" s="T486">NEG-3SG</ta>
            <ta e="T488" id="Seg_7620" s="T487">who.[NOM]</ta>
            <ta e="T489" id="Seg_7621" s="T488">realize-FUT.[3SG]=Q</ta>
            <ta e="T490" id="Seg_7622" s="T489">say-PRS.[3SG]</ta>
            <ta e="T491" id="Seg_7623" s="T490">ten-APRX</ta>
            <ta e="T492" id="Seg_7624" s="T491">human.being.[NOM]</ta>
            <ta e="T493" id="Seg_7625" s="T492">go-CVB.SEQ</ta>
            <ta e="T494" id="Seg_7626" s="T493">long.ago</ta>
            <ta e="T495" id="Seg_7627" s="T494">die-PTCP.PST</ta>
            <ta e="T496" id="Seg_7628" s="T495">people-ACC</ta>
            <ta e="T497" id="Seg_7629" s="T496">shoot-FREQ-PRS-3PL</ta>
            <ta e="T498" id="Seg_7630" s="T497">write-CVB.SEQ</ta>
            <ta e="T499" id="Seg_7631" s="T498">throw-PST1-3PL</ta>
            <ta e="T500" id="Seg_7632" s="T499">like.that</ta>
            <ta e="T501" id="Seg_7633" s="T500">Nganasan</ta>
            <ta e="T502" id="Seg_7634" s="T501">prince-3SG-ACC</ta>
            <ta e="T503" id="Seg_7635" s="T502">send-PRS-3PL</ta>
            <ta e="T504" id="Seg_7636" s="T503">Nenets</ta>
            <ta e="T505" id="Seg_7637" s="T504">prince-3SG-COM</ta>
            <ta e="T506" id="Seg_7638" s="T505">czar.[NOM]</ta>
            <ta e="T507" id="Seg_7639" s="T506">read-CVB.SEQ</ta>
            <ta e="T508" id="Seg_7640" s="T507">see-PRS.[3SG]</ta>
            <ta e="T509" id="Seg_7641" s="T508">ten-APRX</ta>
            <ta e="T510" id="Seg_7642" s="T509">tent.[NOM]</ta>
            <ta e="T511" id="Seg_7643" s="T510">people-ACC</ta>
            <ta e="T512" id="Seg_7644" s="T511">chop-PTCP.PST</ta>
            <ta e="T513" id="Seg_7645" s="T512">child-ACC</ta>
            <ta e="T514" id="Seg_7646" s="T513">steal-PTCP.PST</ta>
            <ta e="T515" id="Seg_7647" s="T514">human.being.[NOM]</ta>
            <ta e="T516" id="Seg_7648" s="T515">czar.[NOM]</ta>
            <ta e="T517" id="Seg_7649" s="T516">every-3PL-ABL</ta>
            <ta e="T518" id="Seg_7650" s="T517">ask-PRS.[3SG]</ta>
            <ta e="T519" id="Seg_7651" s="T518">truth.[NOM]</ta>
            <ta e="T520" id="Seg_7652" s="T519">Q</ta>
            <ta e="T521" id="Seg_7653" s="T520">1SG.[NOM]</ta>
            <ta e="T522" id="Seg_7654" s="T521">3SG-ACC</ta>
            <ta e="T523" id="Seg_7655" s="T522">court-VBZ-FUT-1SG</ta>
            <ta e="T524" id="Seg_7656" s="T523">2PL.[NOM]</ta>
            <ta e="T525" id="Seg_7657" s="T524">hear-PTCP.PRS-2PL-DAT/LOC</ta>
            <ta e="T526" id="Seg_7658" s="T525">Nganasan</ta>
            <ta e="T527" id="Seg_7659" s="T526">prince-3SG.[NOM]</ta>
            <ta e="T528" id="Seg_7660" s="T527">human.being-DAT/LOC</ta>
            <ta e="T529" id="Seg_7661" s="T528">come.closer-NEG.PTCP</ta>
            <ta e="T530" id="Seg_7662" s="T529">human.being.[NOM]</ta>
            <ta e="T531" id="Seg_7663" s="T530">make-PST1-3SG</ta>
            <ta e="T532" id="Seg_7664" s="T531">probably</ta>
            <ta e="T533" id="Seg_7665" s="T532">say-PRS.[3SG]</ta>
            <ta e="T534" id="Seg_7666" s="T533">old.man-DAT/LOC</ta>
            <ta e="T535" id="Seg_7667" s="T534">deception.[NOM]</ta>
            <ta e="T536" id="Seg_7668" s="T535">letter.[NOM]</ta>
            <ta e="T537" id="Seg_7669" s="T536">send-PRS-3PL</ta>
            <ta e="T538" id="Seg_7670" s="T537">prince-3SG.[NOM]</ta>
            <ta e="T539" id="Seg_7671" s="T538">well</ta>
            <ta e="T540" id="Seg_7672" s="T539">tax-3SG-ACC</ta>
            <ta e="T541" id="Seg_7673" s="T540">money-3SG-ACC</ta>
            <ta e="T542" id="Seg_7674" s="T541">take-CVB.SIM</ta>
            <ta e="T543" id="Seg_7675" s="T542">come-INCH-IMP.3SG</ta>
            <ta e="T544" id="Seg_7676" s="T543">very</ta>
            <ta e="T545" id="Seg_7677" s="T544">most</ta>
            <ta e="T546" id="Seg_7678" s="T545">fast-ADVZ</ta>
            <ta e="T547" id="Seg_7679" s="T546">say-CVB.SEQ</ta>
            <ta e="T548" id="Seg_7680" s="T547">write-PRS.[3SG]</ta>
            <ta e="T549" id="Seg_7681" s="T548">messenger.[NOM]</ta>
            <ta e="T550" id="Seg_7682" s="T549">human.being.[NOM]</ta>
            <ta e="T551" id="Seg_7683" s="T550">bring-PRS.[3SG]</ta>
            <ta e="T552" id="Seg_7684" s="T551">letter-ACC</ta>
            <ta e="T553" id="Seg_7685" s="T552">old.man.[NOM]</ta>
            <ta e="T554" id="Seg_7686" s="T553">most</ta>
            <ta e="T555" id="Seg_7687" s="T554">good</ta>
            <ta e="T556" id="Seg_7688" s="T555">reindeer-PL-3SG-INSTR</ta>
            <ta e="T557" id="Seg_7689" s="T556">run-CVB.SEQ</ta>
            <ta e="T558" id="Seg_7690" s="T557">come-PRS.[3SG]</ta>
            <ta e="T559" id="Seg_7691" s="T558">reindeer-3SG-ACC</ta>
            <ta e="T560" id="Seg_7692" s="T559">tie-PST2.[3SG]</ta>
            <ta e="T561" id="Seg_7693" s="T560">and</ta>
            <ta e="T562" id="Seg_7694" s="T561">run-CVB.SEQ</ta>
            <ta e="T563" id="Seg_7695" s="T562">fall-PRS.[3SG]</ta>
            <ta e="T564" id="Seg_7696" s="T563">table-ACC</ta>
            <ta e="T565" id="Seg_7697" s="T564">around</ta>
            <ta e="T566" id="Seg_7698" s="T565">lord.[NOM]</ta>
            <ta e="T567" id="Seg_7699" s="T566">a.lot</ta>
            <ta e="T568" id="Seg_7700" s="T567">czar.[NOM]</ta>
            <ta e="T569" id="Seg_7701" s="T568">tea.[NOM]</ta>
            <ta e="T570" id="Seg_7702" s="T569">NEG</ta>
            <ta e="T571" id="Seg_7703" s="T570">drink-EP-CAUS.[CAUS]-PST2.NEG.[3SG]</ta>
            <ta e="T572" id="Seg_7704" s="T571">blood-3SG.[NOM]</ta>
            <ta e="T573" id="Seg_7705" s="T572">bad.[NOM]</ta>
            <ta e="T574" id="Seg_7706" s="T573">at.all</ta>
            <ta e="T575" id="Seg_7707" s="T574">different.[NOM]</ta>
            <ta e="T576" id="Seg_7708" s="T575">well</ta>
            <ta e="T577" id="Seg_7709" s="T576">tell.[IMP.2SG]</ta>
            <ta e="T578" id="Seg_7710" s="T577">news-2SG-ACC</ta>
            <ta e="T579" id="Seg_7711" s="T578">settlement.[NOM]</ta>
            <ta e="T580" id="Seg_7712" s="T579">people-ACC</ta>
            <ta e="T581" id="Seg_7713" s="T580">why</ta>
            <ta e="T582" id="Seg_7714" s="T581">kill.off-PST1-2SG</ta>
            <ta e="T583" id="Seg_7715" s="T582">what</ta>
            <ta e="T584" id="Seg_7716" s="T583">settlement-3SG-ACC</ta>
            <ta e="T585" id="Seg_7717" s="T584">cover-CAUS-PRS-2SG</ta>
            <ta e="T586" id="Seg_7718" s="T585">human.being-ACC</ta>
            <ta e="T587" id="Seg_7719" s="T586">with</ta>
            <ta e="T588" id="Seg_7720" s="T587">dispute-EP-1SG.[NOM]</ta>
            <ta e="T589" id="Seg_7721" s="T588">NEG</ta>
            <ta e="T590" id="Seg_7722" s="T589">NEG.EX</ta>
            <ta e="T591" id="Seg_7723" s="T590">czar.[NOM]</ta>
            <ta e="T592" id="Seg_7724" s="T591">prince-PL.[NOM]</ta>
            <ta e="T593" id="Seg_7725" s="T592">letter-3PL-ACC</ta>
            <ta e="T594" id="Seg_7726" s="T593">see-CAUS-PRS.[3SG]</ta>
            <ta e="T595" id="Seg_7727" s="T594">coming.year-ADJZ</ta>
            <ta e="T596" id="Seg_7728" s="T595">water.[NOM]</ta>
            <ta e="T597" id="Seg_7729" s="T596">come-PTCP.FUT.[3SG]-DAT/LOC</ta>
            <ta e="T598" id="Seg_7730" s="T597">until</ta>
            <ta e="T599" id="Seg_7731" s="T598">prison-DAT/LOC</ta>
            <ta e="T600" id="Seg_7732" s="T599">lie-FUT-2SG</ta>
            <ta e="T601" id="Seg_7733" s="T600">1SG-COMP</ta>
            <ta e="T602" id="Seg_7734" s="T601">big</ta>
            <ta e="T603" id="Seg_7735" s="T602">czar-DAT/LOC</ta>
            <ta e="T604" id="Seg_7736" s="T603">send-FUT-1SG</ta>
            <ta e="T605" id="Seg_7737" s="T604">old.man-ACC</ta>
            <ta e="T606" id="Seg_7738" s="T605">prison-DAT/LOC</ta>
            <ta e="T607" id="Seg_7739" s="T606">block-CVB.SEQ</ta>
            <ta e="T608" id="Seg_7740" s="T607">throw-PRS-3PL</ta>
            <ta e="T609" id="Seg_7741" s="T608">three</ta>
            <ta e="T610" id="Seg_7742" s="T609">prince.[NOM]</ta>
            <ta e="T611" id="Seg_7743" s="T610">wealth-3SG-ACC</ta>
            <ta e="T612" id="Seg_7744" s="T611">divide-FUT-3PL</ta>
            <ta e="T613" id="Seg_7745" s="T612">old.woman.[NOM]</ta>
            <ta e="T614" id="Seg_7746" s="T613">nomadize-CVB.SEQ</ta>
            <ta e="T615" id="Seg_7747" s="T614">come-PRS.[3SG]</ta>
            <ta e="T616" id="Seg_7748" s="T615">old.man-3SG.[NOM]</ta>
            <ta e="T617" id="Seg_7749" s="T616">when</ta>
            <ta e="T618" id="Seg_7750" s="T617">NEG</ta>
            <ta e="T619" id="Seg_7751" s="T618">NEG.EX</ta>
            <ta e="T620" id="Seg_7752" s="T619">prison-DAT/LOC</ta>
            <ta e="T621" id="Seg_7753" s="T620">lie-PRS.[3SG]</ta>
            <ta e="T622" id="Seg_7754" s="T621">czar.[NOM]</ta>
            <ta e="T623" id="Seg_7755" s="T622">old.woman-ACC</ta>
            <ta e="T624" id="Seg_7756" s="T623">hunt-CVB.SEQ</ta>
            <ta e="T625" id="Seg_7757" s="T624">take.out-PRS.[3SG]</ta>
            <ta e="T626" id="Seg_7758" s="T625">three</ta>
            <ta e="T627" id="Seg_7759" s="T626">prince.[NOM]</ta>
            <ta e="T628" id="Seg_7760" s="T627">wealth-3SG-ACC</ta>
            <ta e="T629" id="Seg_7761" s="T628">cover-CVB.SIM</ta>
            <ta e="T630" id="Seg_7762" s="T629">fall-CVB.SEQ</ta>
            <ta e="T631" id="Seg_7763" s="T630">divide-CVB.SEQ</ta>
            <ta e="T632" id="Seg_7764" s="T631">take-PRS-3PL</ta>
            <ta e="T633" id="Seg_7765" s="T632">one</ta>
            <ta e="T634" id="Seg_7766" s="T633">leather.cover.for.a.tent-ACC</ta>
            <ta e="T635" id="Seg_7767" s="T634">stay-CAUS-PRS-3PL</ta>
            <ta e="T636" id="Seg_7768" s="T635">winter-3SG.[NOM]</ta>
            <ta e="T637" id="Seg_7769" s="T636">end-CVB.SEQ</ta>
            <ta e="T638" id="Seg_7770" s="T637">steamer.[NOM]</ta>
            <ta e="T639" id="Seg_7771" s="T638">come-EMOT-PST2.[3SG]</ta>
            <ta e="T640" id="Seg_7772" s="T639">up-ABL</ta>
            <ta e="T641" id="Seg_7773" s="T640">well</ta>
            <ta e="T642" id="Seg_7774" s="T641">see.[IMP.2SG]</ta>
            <ta e="T643" id="Seg_7775" s="T642">husband-2SG-ACC</ta>
            <ta e="T644" id="Seg_7776" s="T643">say-PRS-3PL</ta>
            <ta e="T645" id="Seg_7777" s="T644">old.man.[NOM]</ta>
            <ta e="T646" id="Seg_7778" s="T645">eye-3SG.[NOM]</ta>
            <ta e="T647" id="Seg_7779" s="T646">pit.[NOM]</ta>
            <ta e="T648" id="Seg_7780" s="T647">inside-3SG-DAT/LOC</ta>
            <ta e="T649" id="Seg_7781" s="T648">go.in-PST2.[3SG]</ta>
            <ta e="T650" id="Seg_7782" s="T649">eat-EP-CAUS-PST2.NEG-3PL</ta>
            <ta e="T651" id="Seg_7783" s="T650">hunger-CVB.SEQ</ta>
            <ta e="T652" id="Seg_7784" s="T651">stay-PST2.[3SG]</ta>
            <ta e="T653" id="Seg_7785" s="T652">steamer.[NOM]</ta>
            <ta e="T654" id="Seg_7786" s="T653">inside-3SG-DAT/LOC</ta>
            <ta e="T655" id="Seg_7787" s="T654">throw-PST2-3PL</ta>
            <ta e="T656" id="Seg_7788" s="T655">next</ta>
            <ta e="T657" id="Seg_7789" s="T656">czar-DAT/LOC</ta>
            <ta e="T658" id="Seg_7790" s="T657">carry-PRS-3PL</ta>
            <ta e="T659" id="Seg_7791" s="T658">stretcher-INSTR</ta>
            <ta e="T660" id="Seg_7792" s="T659">bring.in-PRS-3PL</ta>
            <ta e="T661" id="Seg_7793" s="T660">die-CVB.SEQ</ta>
            <ta e="T662" id="Seg_7794" s="T661">be-PTCP.PRS</ta>
            <ta e="T663" id="Seg_7795" s="T662">human.being-ACC</ta>
            <ta e="T664" id="Seg_7796" s="T663">well</ta>
            <ta e="T665" id="Seg_7797" s="T664">how</ta>
            <ta e="T666" id="Seg_7798" s="T665">court-VBZ-FUT-1SG=Q</ta>
            <ta e="T667" id="Seg_7799" s="T666">die-PTCP.PST</ta>
            <ta e="T668" id="Seg_7800" s="T667">human.being-ACC</ta>
            <ta e="T669" id="Seg_7801" s="T668">carry-EP-IMP.2PL</ta>
            <ta e="T670" id="Seg_7802" s="T669">hospital-DAT/LOC</ta>
            <ta e="T671" id="Seg_7803" s="T670">eat-EP-CAUS-IMP.3PL</ta>
            <ta e="T672" id="Seg_7804" s="T671">see-IMP.3PL</ta>
            <ta e="T673" id="Seg_7805" s="T672">doctor-PL.[NOM]</ta>
            <ta e="T674" id="Seg_7806" s="T673">heal-PRS-3PL</ta>
            <ta e="T675" id="Seg_7807" s="T674">old</ta>
            <ta e="T676" id="Seg_7808" s="T675">self-3SG.[NOM]</ta>
            <ta e="T677" id="Seg_7809" s="T676">be-PRS.[3SG]</ta>
            <ta e="T678" id="Seg_7810" s="T677">doctor-PL.[NOM]</ta>
            <ta e="T679" id="Seg_7811" s="T678">ask-PRS-3PL</ta>
            <ta e="T680" id="Seg_7812" s="T679">guilt-EP-2SG.[NOM]</ta>
            <ta e="T681" id="Seg_7813" s="T680">Q</ta>
            <ta e="T682" id="Seg_7814" s="T681">suspect-PRS-3PL</ta>
            <ta e="T683" id="Seg_7815" s="T682">Q</ta>
            <ta e="T684" id="Seg_7816" s="T683">old.man.[NOM]</ta>
            <ta e="T685" id="Seg_7817" s="T684">tell-PRS.[3SG]</ta>
            <ta e="T686" id="Seg_7818" s="T685">every-3SG-ACC</ta>
            <ta e="T687" id="Seg_7819" s="T686">doctor-PL.[NOM]</ta>
            <ta e="T688" id="Seg_7820" s="T687">blood.[NOM]</ta>
            <ta e="T689" id="Seg_7821" s="T688">take-PRS-3PL</ta>
            <ta e="T690" id="Seg_7822" s="T689">guilt-PROPR.[NOM]</ta>
            <ta e="T691" id="Seg_7823" s="T690">be-TEMP-2SG</ta>
            <ta e="T692" id="Seg_7824" s="T691">blood-EP-2SG.[NOM]</ta>
            <ta e="T693" id="Seg_7825" s="T692">different.[NOM]</ta>
            <ta e="T694" id="Seg_7826" s="T693">be-FUT.[3SG]</ta>
            <ta e="T695" id="Seg_7827" s="T694">say-PRS-3PL</ta>
            <ta e="T696" id="Seg_7828" s="T695">one-INTNS.[NOM]</ta>
            <ta e="T697" id="Seg_7829" s="T696">NEG</ta>
            <ta e="T698" id="Seg_7830" s="T697">small.drop.[NOM]</ta>
            <ta e="T699" id="Seg_7831" s="T698">guilt-3SG.[NOM]</ta>
            <ta e="T700" id="Seg_7832" s="T699">NEG.EX</ta>
            <ta e="T701" id="Seg_7833" s="T700">be-PST2.[3SG]</ta>
            <ta e="T702" id="Seg_7834" s="T701">letter.[NOM]</ta>
            <ta e="T703" id="Seg_7835" s="T702">give-PRS-3PL</ta>
            <ta e="T704" id="Seg_7836" s="T703">this-ACC</ta>
            <ta e="T705" id="Seg_7837" s="T704">court-DAT/LOC</ta>
            <ta e="T706" id="Seg_7838" s="T705">directly</ta>
            <ta e="T707" id="Seg_7839" s="T706">give-FUT.[IMP.2SG]</ta>
            <ta e="T708" id="Seg_7840" s="T707">prince-PL.[NOM]</ta>
            <ta e="T709" id="Seg_7841" s="T708">blood-3PL-ACC</ta>
            <ta e="T710" id="Seg_7842" s="T709">also</ta>
            <ta e="T711" id="Seg_7843" s="T710">take-FUT-1PL</ta>
            <ta e="T712" id="Seg_7844" s="T711">say-PRS-3PL</ta>
            <ta e="T713" id="Seg_7845" s="T712">court.[NOM]</ta>
            <ta e="T714" id="Seg_7846" s="T713">be-PRS.[3SG]</ta>
            <ta e="T715" id="Seg_7847" s="T714">prince-PL.[NOM]</ta>
            <ta e="T716" id="Seg_7848" s="T715">first</ta>
            <ta e="T717" id="Seg_7849" s="T716">czar.[NOM]</ta>
            <ta e="T718" id="Seg_7850" s="T717">many</ta>
            <ta e="T719" id="Seg_7851" s="T718">people.[NOM]</ta>
            <ta e="T720" id="Seg_7852" s="T719">gather.oneself-REFL-EP-PST2.[3SG]</ta>
            <ta e="T721" id="Seg_7853" s="T720">czar.[NOM]</ta>
            <ta e="T722" id="Seg_7854" s="T721">speak-PRS.[3SG]</ta>
            <ta e="T723" id="Seg_7855" s="T722">word-3SG-ACC-word-3SG-ACC</ta>
            <ta e="T724" id="Seg_7856" s="T723">hear-PTCP.FUT-DAT/LOC</ta>
            <ta e="T725" id="Seg_7857" s="T724">say-PRS.[3SG]</ta>
            <ta e="T726" id="Seg_7858" s="T725">old.man.[NOM]</ta>
            <ta e="T727" id="Seg_7859" s="T726">tell-PRS.[3SG]</ta>
            <ta e="T728" id="Seg_7860" s="T727">1SG.[NOM]</ta>
            <ta e="T729" id="Seg_7861" s="T728">even</ta>
            <ta e="T730" id="Seg_7862" s="T729">human.being-ACC</ta>
            <ta e="T731" id="Seg_7863" s="T730">with</ta>
            <ta e="T732" id="Seg_7864" s="T731">fight-EP-NEG.PTCP.PST</ta>
            <ta e="T733" id="Seg_7865" s="T732">human.being-1SG</ta>
            <ta e="T734" id="Seg_7866" s="T733">testimony-3SG-ACC</ta>
            <ta e="T735" id="Seg_7867" s="T734">give-PRS.[3SG]</ta>
            <ta e="T736" id="Seg_7868" s="T735">this</ta>
            <ta e="T737" id="Seg_7869" s="T736">human.being.[NOM]</ta>
            <ta e="T738" id="Seg_7870" s="T737">small.drop.[NOM]</ta>
            <ta e="T739" id="Seg_7871" s="T738">NEG</ta>
            <ta e="T740" id="Seg_7872" s="T739">guilt-POSS</ta>
            <ta e="T741" id="Seg_7873" s="T740">NEG.[3SG]</ta>
            <ta e="T742" id="Seg_7874" s="T741">prince-PL.[NOM]</ta>
            <ta e="T743" id="Seg_7875" s="T742">blood-3PL-ACC</ta>
            <ta e="T744" id="Seg_7876" s="T743">take-PRS-3PL</ta>
            <ta e="T745" id="Seg_7877" s="T744">Nganasan</ta>
            <ta e="T746" id="Seg_7878" s="T745">brave-3SG-GEN</ta>
            <ta e="T747" id="Seg_7879" s="T746">blood-3SG.[NOM]</ta>
            <ta e="T748" id="Seg_7880" s="T747">indeed</ta>
            <ta e="T749" id="Seg_7881" s="T748">different.[NOM]</ta>
            <ta e="T750" id="Seg_7882" s="T749">prince-PL.[NOM]</ta>
            <ta e="T751" id="Seg_7883" s="T750">own-3PL.[NOM]</ta>
            <ta e="T752" id="Seg_7884" s="T751">at.all</ta>
            <ta e="T753" id="Seg_7885" s="T752">different.[NOM]</ta>
            <ta e="T754" id="Seg_7886" s="T753">czar.[NOM]</ta>
            <ta e="T755" id="Seg_7887" s="T754">well</ta>
            <ta e="T756" id="Seg_7888" s="T755">get.angry-PRS.[3SG]</ta>
            <ta e="T757" id="Seg_7889" s="T756">who.[NOM]</ta>
            <ta e="T758" id="Seg_7890" s="T757">advise-PST2-3SG=Q</ta>
            <ta e="T759" id="Seg_7891" s="T758">tell-IMP.2PL</ta>
            <ta e="T760" id="Seg_7892" s="T759">Dolgan</ta>
            <ta e="T761" id="Seg_7893" s="T760">prince-3SG.[NOM]</ta>
            <ta e="T762" id="Seg_7894" s="T761">tell-PRS.[3SG]</ta>
            <ta e="T763" id="Seg_7895" s="T762">well</ta>
            <ta e="T764" id="Seg_7896" s="T763">truth-3SG.[NOM]</ta>
            <ta e="T765" id="Seg_7897" s="T764">1PL.[NOM]</ta>
            <ta e="T766" id="Seg_7898" s="T765">guilt-PROPR-1PL</ta>
            <ta e="T767" id="Seg_7899" s="T766">most</ta>
            <ta e="T768" id="Seg_7900" s="T767">big</ta>
            <ta e="T769" id="Seg_7901" s="T768">guilt-PROPR.[NOM]</ta>
            <ta e="T770" id="Seg_7902" s="T769">Nenets</ta>
            <ta e="T771" id="Seg_7903" s="T770">prince-3SG.[NOM]</ta>
            <ta e="T772" id="Seg_7904" s="T771">1SG-ACC</ta>
            <ta e="T773" id="Seg_7905" s="T772">chop-PTCP.FUT</ta>
            <ta e="T774" id="Seg_7906" s="T773">there.is-3PL</ta>
            <ta e="T775" id="Seg_7907" s="T774">startle-CVB.SEQ</ta>
            <ta e="T777" id="Seg_7908" s="T776">give-PST2-EP-1SG</ta>
            <ta e="T778" id="Seg_7909" s="T777">czar.[NOM]</ta>
            <ta e="T779" id="Seg_7910" s="T778">well</ta>
            <ta e="T780" id="Seg_7911" s="T779">old.man.[NOM]</ta>
            <ta e="T781" id="Seg_7912" s="T780">2SG.[NOM]</ta>
            <ta e="T782" id="Seg_7913" s="T781">guilt-EP-2SG.[NOM]</ta>
            <ta e="T783" id="Seg_7914" s="T782">NEG.EX</ta>
            <ta e="T784" id="Seg_7915" s="T783">be-PST2.[3SG]</ta>
            <ta e="T785" id="Seg_7916" s="T784">prison-DAT/LOC</ta>
            <ta e="T786" id="Seg_7917" s="T785">2SG-ACC</ta>
            <ta e="T787" id="Seg_7918" s="T786">court-VBZ-PTCP.PST</ta>
            <ta e="T788" id="Seg_7919" s="T787">czar-ACC</ta>
            <ta e="T789" id="Seg_7920" s="T788">seat-FUT-1SG</ta>
            <ta e="T790" id="Seg_7921" s="T789">2SG.[NOM]</ta>
            <ta e="T791" id="Seg_7922" s="T790">3SG.[NOM]</ta>
            <ta e="T792" id="Seg_7923" s="T791">place-DAT/LOC</ta>
            <ta e="T793" id="Seg_7924" s="T792">czar-INSTR</ta>
            <ta e="T794" id="Seg_7925" s="T793">go-FUT-2SG</ta>
            <ta e="T795" id="Seg_7926" s="T794">prince-PL-ACC</ta>
            <ta e="T796" id="Seg_7927" s="T795">there</ta>
            <ta e="T797" id="Seg_7928" s="T796">reach-CVB.SEQ</ta>
            <ta e="T798" id="Seg_7929" s="T797">court-VBZ-FUT.[IMP.2SG]</ta>
            <ta e="T799" id="Seg_7930" s="T798">say-PST2.[3SG]</ta>
            <ta e="T800" id="Seg_7931" s="T799">hit.the.road-CVB.SEQ</ta>
            <ta e="T801" id="Seg_7932" s="T800">stay-PRS-3PL</ta>
            <ta e="T802" id="Seg_7933" s="T801">back</ta>
            <ta e="T803" id="Seg_7934" s="T802">old.man.[NOM]</ta>
            <ta e="T804" id="Seg_7935" s="T803">settlement-3SG-DAT/LOC</ta>
            <ta e="T805" id="Seg_7936" s="T804">come-CVB.SEQ</ta>
            <ta e="T806" id="Seg_7937" s="T805">prince.[NOM]</ta>
            <ta e="T807" id="Seg_7938" s="T806">become-PRS.[3SG]</ta>
            <ta e="T808" id="Seg_7939" s="T807">son-3SG.[NOM]</ta>
            <ta e="T809" id="Seg_7940" s="T808">grow-CVB.SEQ</ta>
            <ta e="T810" id="Seg_7941" s="T809">directly-directly</ta>
            <ta e="T811" id="Seg_7942" s="T810">hero.[NOM]</ta>
            <ta e="T812" id="Seg_7943" s="T811">become-PST2.[3SG]</ta>
            <ta e="T813" id="Seg_7944" s="T812">3SG.[NOM]</ta>
            <ta e="T814" id="Seg_7945" s="T813">no-3SG</ta>
            <ta e="T815" id="Seg_7946" s="T814">kill-CVB.PURP</ta>
            <ta e="T816" id="Seg_7947" s="T815">want-PST2-3PL</ta>
            <ta e="T817" id="Seg_7948" s="T816">father-3SG-DAT/LOC</ta>
            <ta e="T818" id="Seg_7949" s="T817">similar</ta>
            <ta e="T819" id="Seg_7950" s="T818">be-FUT.[3SG]</ta>
            <ta e="T820" id="Seg_7951" s="T819">think-CVB.SEQ</ta>
            <ta e="T821" id="Seg_7952" s="T820">boy.[NOM]</ta>
            <ta e="T822" id="Seg_7953" s="T821">one</ta>
            <ta e="T823" id="Seg_7954" s="T822">human.being-ACC</ta>
            <ta e="T824" id="Seg_7955" s="T823">upset-ADVZ</ta>
            <ta e="T825" id="Seg_7956" s="T824">beat-EP-PST2.[3SG]</ta>
            <ta e="T826" id="Seg_7957" s="T825">that-ACC</ta>
            <ta e="T827" id="Seg_7958" s="T826">prison-DAT/LOC</ta>
            <ta e="T828" id="Seg_7959" s="T827">seat-PST2-3PL</ta>
            <ta e="T829" id="Seg_7960" s="T828">prince-3PL-ACC</ta>
            <ta e="T830" id="Seg_7961" s="T829">judge-PRS.[3SG]</ta>
            <ta e="T831" id="Seg_7962" s="T830">Nganasan</ta>
            <ta e="T832" id="Seg_7963" s="T831">prince-3SG-ACC</ta>
            <ta e="T833" id="Seg_7964" s="T832">Nenets</ta>
            <ta e="T834" id="Seg_7965" s="T833">prince-3SG-COM</ta>
            <ta e="T835" id="Seg_7966" s="T834">prison-DAT/LOC</ta>
            <ta e="T836" id="Seg_7967" s="T835">seat-PRS.[3SG]</ta>
            <ta e="T837" id="Seg_7968" s="T836">Dolgan</ta>
            <ta e="T838" id="Seg_7969" s="T837">prince-3SG-ACC</ta>
            <ta e="T839" id="Seg_7970" s="T838">prince-3SG-ABL</ta>
            <ta e="T840" id="Seg_7971" s="T839">undress-PRS.[3SG]</ta>
            <ta e="T841" id="Seg_7972" s="T840">son-3SG-ACC</ta>
            <ta e="T842" id="Seg_7973" s="T841">Nganasan</ta>
            <ta e="T843" id="Seg_7974" s="T842">prince-3SG.[NOM]</ta>
            <ta e="T844" id="Seg_7975" s="T843">make-PRS.[3SG]</ta>
            <ta e="T845" id="Seg_7976" s="T844">old.man.[NOM]</ta>
            <ta e="T846" id="Seg_7977" s="T845">such</ta>
            <ta e="T847" id="Seg_7978" s="T846">truth-3SG.[NOM]</ta>
            <ta e="T848" id="Seg_7979" s="T847">go.out-EP-PST2-3SG</ta>
            <ta e="T849" id="Seg_7980" s="T848">last-3SG.[NOM]</ta>
         </annotation>
         <annotation name="gg" tierref="gg">
            <ta e="T1" id="Seg_7981" s="T0">Zar-PROPR-PL.[NOM]</ta>
            <ta e="T2" id="Seg_7982" s="T1">Hausherr-PROPR-PL.[NOM]</ta>
            <ta e="T3" id="Seg_7983" s="T2">Fürst-PROPR-PL.[NOM]</ta>
            <ta e="T4" id="Seg_7984" s="T3">sein-INFER-3SG</ta>
            <ta e="T5" id="Seg_7985" s="T4">dieses</ta>
            <ta e="T6" id="Seg_7986" s="T5">Mensch.[NOM]</ta>
            <ta e="T7" id="Seg_7987" s="T6">einzeln</ta>
            <ta e="T8" id="Seg_7988" s="T7">leben-PRS.[3SG]</ta>
            <ta e="T9" id="Seg_7989" s="T8">mutig.[NOM]</ta>
            <ta e="T10" id="Seg_7990" s="T9">nur</ta>
            <ta e="T11" id="Seg_7991" s="T10">alleine-INTNS.[NOM]</ta>
            <ta e="T12" id="Seg_7992" s="T11">und</ta>
            <ta e="T13" id="Seg_7993" s="T12">Kind-POSS</ta>
            <ta e="T14" id="Seg_7994" s="T13">NEG.[3SG]</ta>
            <ta e="T15" id="Seg_7995" s="T14">Jahr-DAT/LOC</ta>
            <ta e="T16" id="Seg_7996" s="T15">einmal</ta>
            <ta e="T17" id="Seg_7997" s="T16">bezahlen-PRS.[3SG]</ta>
            <ta e="T18" id="Seg_7998" s="T17">Steuer-3SG-ACC</ta>
            <ta e="T19" id="Seg_7999" s="T18">Meer.[NOM]</ta>
            <ta e="T20" id="Seg_8000" s="T19">Rand-3SG-DAT/LOC</ta>
            <ta e="T21" id="Seg_8001" s="T20">echt</ta>
            <ta e="T22" id="Seg_8002" s="T21">Tundra-DAT/LOC</ta>
            <ta e="T23" id="Seg_8003" s="T22">leben-PRS.[3SG]</ta>
            <ta e="T24" id="Seg_8004" s="T23">Meer.[NOM]</ta>
            <ta e="T25" id="Seg_8005" s="T24">Totholz-3SG-ACC</ta>
            <ta e="T26" id="Seg_8006" s="T25">heizen-EP-MED-PRS.[3SG]</ta>
            <ta e="T27" id="Seg_8007" s="T26">lange</ta>
            <ta e="T28" id="Seg_8008" s="T27">leben-PST2.[3SG]</ta>
            <ta e="T29" id="Seg_8009" s="T28">altern-EP-PST2.[3SG]</ta>
            <ta e="T30" id="Seg_8010" s="T29">Jahr-3SG.[NOM]</ta>
            <ta e="T31" id="Seg_8011" s="T30">Frühling-INCH-CVB.SEQ</ta>
            <ta e="T32" id="Seg_8012" s="T31">gehen-PRS.[3SG]</ta>
            <ta e="T33" id="Seg_8013" s="T32">Alte-3SG-DAT/LOC</ta>
            <ta e="T34" id="Seg_8014" s="T33">sprechen-PRS.[3SG]</ta>
            <ta e="T35" id="Seg_8015" s="T34">Jahr-1PL.[NOM]</ta>
            <ta e="T36" id="Seg_8016" s="T35">schmelzen-PST1-3SG</ta>
            <ta e="T37" id="Seg_8017" s="T36">EMPH</ta>
            <ta e="T38" id="Seg_8018" s="T37">Holz.[NOM]</ta>
            <ta e="T39" id="Seg_8019" s="T38">ziehen-EP-MED-CVB.SIM</ta>
            <ta e="T40" id="Seg_8020" s="T39">gehen-FUT-1SG</ta>
            <ta e="T41" id="Seg_8021" s="T40">kommendes.Jahr-ADJZ-DAT/LOC</ta>
            <ta e="T42" id="Seg_8022" s="T41">dreißig</ta>
            <ta e="T43" id="Seg_8023" s="T42">Schlitten-PART</ta>
            <ta e="T44" id="Seg_8024" s="T43">vorbereiten.[IMP.2SG]</ta>
            <ta e="T45" id="Seg_8025" s="T44">dreißig</ta>
            <ta e="T46" id="Seg_8026" s="T45">Schlitten-INSTR</ta>
            <ta e="T47" id="Seg_8027" s="T46">Meer-DAT/LOC</ta>
            <ta e="T48" id="Seg_8028" s="T47">gehen-CVB.SEQ</ta>
            <ta e="T49" id="Seg_8029" s="T48">bleiben-PRS.[3SG]</ta>
            <ta e="T50" id="Seg_8030" s="T49">EMPH-einsam</ta>
            <ta e="T51" id="Seg_8031" s="T50">Meer-DAT/LOC</ta>
            <ta e="T52" id="Seg_8032" s="T51">Felsvorsprung.[NOM]</ta>
            <ta e="T53" id="Seg_8033" s="T52">sehr-INSTR</ta>
            <ta e="T54" id="Seg_8034" s="T53">hineingehen-PST2.[3SG]</ta>
            <ta e="T55" id="Seg_8035" s="T54">einsam</ta>
            <ta e="T56" id="Seg_8036" s="T55">Ort-ABL</ta>
            <ta e="T57" id="Seg_8037" s="T56">einladen-MED-PST2.[3SG]</ta>
            <ta e="T58" id="Seg_8038" s="T57">hohes.Ufer-3SG-GEN</ta>
            <ta e="T59" id="Seg_8039" s="T58">oberer.Teil-3SG-DAT/LOC</ta>
            <ta e="T60" id="Seg_8040" s="T59">eins</ta>
            <ta e="T61" id="Seg_8041" s="T60">Flussbett.[NOM]</ta>
            <ta e="T62" id="Seg_8042" s="T61">durch</ta>
            <ta e="T63" id="Seg_8043" s="T62">zurückkommen-EP-PST2.[3SG]</ta>
            <ta e="T64" id="Seg_8044" s="T63">dieses</ta>
            <ta e="T65" id="Seg_8045" s="T64">hinausgehen-CVB.SEQ</ta>
            <ta e="T66" id="Seg_8046" s="T65">Berg-3SG-GEN</ta>
            <ta e="T67" id="Seg_8047" s="T66">oberer.Teil-3SG-DAT/LOC</ta>
            <ta e="T68" id="Seg_8048" s="T67">Tabak.[NOM]</ta>
            <ta e="T69" id="Seg_8049" s="T68">rauchen-CVB.SIM</ta>
            <ta e="T70" id="Seg_8050" s="T69">sitzen-PST2.[3SG]</ta>
            <ta e="T71" id="Seg_8051" s="T70">Fluss.[NOM]</ta>
            <ta e="T72" id="Seg_8052" s="T71">zu</ta>
            <ta e="T73" id="Seg_8053" s="T72">sehen-PST2-3SG</ta>
            <ta e="T74" id="Seg_8054" s="T73">Wasser-DAT/LOC</ta>
            <ta e="T75" id="Seg_8055" s="T74">Kind.[NOM]</ta>
            <ta e="T76" id="Seg_8056" s="T75">gehen-PRS.[3SG]</ta>
            <ta e="T77" id="Seg_8057" s="T76">schmelzen-PTCP.PST</ta>
            <ta e="T78" id="Seg_8058" s="T77">Wasser-DAT/LOC</ta>
            <ta e="T79" id="Seg_8059" s="T78">Schaum-3PL-INSTR</ta>
            <ta e="T80" id="Seg_8060" s="T79">spielen-CVB.SIM</ta>
            <ta e="T81" id="Seg_8061" s="T80">gehen-PRS.[3SG]</ta>
            <ta e="T82" id="Seg_8062" s="T81">Junge.[NOM]</ta>
            <ta e="T83" id="Seg_8063" s="T82">Kind.[NOM]</ta>
            <ta e="T84" id="Seg_8064" s="T83">offenbar</ta>
            <ta e="T85" id="Seg_8065" s="T84">woher</ta>
            <ta e="T86" id="Seg_8066" s="T85">und</ta>
            <ta e="T87" id="Seg_8067" s="T86">kommen-PST2-3SG</ta>
            <ta e="T88" id="Seg_8068" s="T87">wissen-PASS/REFL-EP-NEG.[3SG]</ta>
            <ta e="T89" id="Seg_8069" s="T88">sich.wundern-PST2.[3SG]</ta>
            <ta e="T90" id="Seg_8070" s="T89">1SG.[NOM]</ta>
            <ta e="T91" id="Seg_8071" s="T90">Kind-POSS</ta>
            <ta e="T92" id="Seg_8072" s="T91">NEG-1SG</ta>
            <ta e="T93" id="Seg_8073" s="T92">EMPH</ta>
            <ta e="T94" id="Seg_8074" s="T93">nehmen-PTCP.PST.[NOM]</ta>
            <ta e="T95" id="Seg_8075" s="T94">MOD</ta>
            <ta e="T96" id="Seg_8076" s="T95">denken-CVB.SIM</ta>
            <ta e="T97" id="Seg_8077" s="T96">denken-PST2.[3SG]</ta>
            <ta e="T98" id="Seg_8078" s="T97">Kind-3SG.[NOM]</ta>
            <ta e="T99" id="Seg_8079" s="T98">Insel.[NOM]</ta>
            <ta e="T100" id="Seg_8080" s="T99">Sand-3SG-DAT/LOC</ta>
            <ta e="T101" id="Seg_8081" s="T100">gehen-PRS.[3SG]</ta>
            <ta e="T102" id="Seg_8082" s="T101">krumm</ta>
            <ta e="T103" id="Seg_8083" s="T102">Baum.[NOM]</ta>
            <ta e="T104" id="Seg_8084" s="T103">es.gibt</ta>
            <ta e="T105" id="Seg_8085" s="T104">jenes-INSTR</ta>
            <ta e="T106" id="Seg_8086" s="T105">gehen-INFER-3SG</ta>
            <ta e="T107" id="Seg_8087" s="T106">springen-CVB.SEQ</ta>
            <ta e="T108" id="Seg_8088" s="T107">ankommen-PRS.[3SG]</ta>
            <ta e="T109" id="Seg_8089" s="T108">alter.Mann.[NOM]</ta>
            <ta e="T110" id="Seg_8090" s="T109">Hinterseite-3SG-ABL</ta>
            <ta e="T111" id="Seg_8091" s="T110">fangen-CVB.SEQ</ta>
            <ta e="T112" id="Seg_8092" s="T111">nehmen-CVB.SEQ</ta>
            <ta e="T113" id="Seg_8093" s="T112">langer.Mantel-3SG-GEN</ta>
            <ta e="T114" id="Seg_8094" s="T113">Inneres-3SG-DAT/LOC</ta>
            <ta e="T115" id="Seg_8095" s="T114">stecken-PST2.[3SG]</ta>
            <ta e="T116" id="Seg_8096" s="T115">gürten-EP-REFL-CVB.SEQ</ta>
            <ta e="T117" id="Seg_8097" s="T116">werfen-PST2.[3SG]</ta>
            <ta e="T118" id="Seg_8098" s="T117">doch</ta>
            <ta e="T119" id="Seg_8099" s="T118">nur</ta>
            <ta e="T120" id="Seg_8100" s="T119">sich.auf.den.Weg.machen-CAP.[3SG]</ta>
            <ta e="T121" id="Seg_8101" s="T120">Geräusch.[NOM]</ta>
            <ta e="T122" id="Seg_8102" s="T121">Gemurmel-3SG.[NOM]</ta>
            <ta e="T123" id="Seg_8103" s="T122">sein-PRS.[3SG]</ta>
            <ta e="T124" id="Seg_8104" s="T123">stehen-PST2.[3SG]</ta>
            <ta e="T125" id="Seg_8105" s="T124">sehen-PST2-3SG</ta>
            <ta e="T126" id="Seg_8106" s="T125">vollständig</ta>
            <ta e="T127" id="Seg_8107" s="T126">Siedlung.[NOM]</ta>
            <ta e="T128" id="Seg_8108" s="T127">Mensch-3SG.[NOM]</ta>
            <ta e="T129" id="Seg_8109" s="T128">sterben-PTCP.PST</ta>
            <ta e="T130" id="Seg_8110" s="T129">Ort-3SG.[NOM]</ta>
            <ta e="T131" id="Seg_8111" s="T130">sein-PST2.[3SG]</ta>
            <ta e="T132" id="Seg_8112" s="T131">hey</ta>
            <ta e="T133" id="Seg_8113" s="T132">dieses-PL-ABL</ta>
            <ta e="T134" id="Seg_8114" s="T133">übrig.bleiben-CVB.SEQ</ta>
            <ta e="T135" id="Seg_8115" s="T134">bleiben-PST2.[3SG]</ta>
            <ta e="T136" id="Seg_8116" s="T135">Kind.[NOM]</ta>
            <ta e="T137" id="Seg_8117" s="T136">MOD</ta>
            <ta e="T138" id="Seg_8118" s="T137">eins</ta>
            <ta e="T139" id="Seg_8119" s="T138">Hinterseite-1PL-PROPR</ta>
            <ta e="T140" id="Seg_8120" s="T139">vorderstes.Rentier-3SG-ACC</ta>
            <ta e="T141" id="Seg_8121" s="T140">bringen-CVB.SEQ</ta>
            <ta e="T142" id="Seg_8122" s="T141">töten-CVB.SEQ</ta>
            <ta e="T143" id="Seg_8123" s="T142">werfen-PRS.[3SG]</ta>
            <ta e="T144" id="Seg_8124" s="T143">Kind-2PL-ACC</ta>
            <ta e="T145" id="Seg_8125" s="T144">tragen-PST1-1SG</ta>
            <ta e="T146" id="Seg_8126" s="T145">dieses</ta>
            <ta e="T147" id="Seg_8127" s="T146">Kind-2PL.[NOM]</ta>
            <ta e="T148" id="Seg_8128" s="T147">Spur-3SG.[NOM]</ta>
            <ta e="T149" id="Seg_8129" s="T148">eins</ta>
            <ta e="T150" id="Seg_8130" s="T149">Rentier-ACC</ta>
            <ta e="T151" id="Seg_8131" s="T150">werfen-PST1-1SG</ta>
            <ta e="T152" id="Seg_8132" s="T151">sagen-PRS.[3SG]</ta>
            <ta e="T153" id="Seg_8133" s="T152">bringen-PST1-3SG</ta>
            <ta e="T154" id="Seg_8134" s="T153">Haus-3SG-DAT/LOC</ta>
            <ta e="T155" id="Seg_8135" s="T154">Alte-3SG-DAT/LOC</ta>
            <ta e="T156" id="Seg_8136" s="T155">erzählen-PRS.[3SG]</ta>
            <ta e="T157" id="Seg_8137" s="T156">drei</ta>
            <ta e="T158" id="Seg_8138" s="T157">Tag.[NOM]</ta>
            <ta e="T159" id="Seg_8139" s="T158">solange</ta>
            <ta e="T160" id="Seg_8140" s="T159">Rentier-3PL-ACC</ta>
            <ta e="T161" id="Seg_8141" s="T160">und</ta>
            <ta e="T162" id="Seg_8142" s="T161">lassen-NEG.CVB.SIM</ta>
            <ta e="T163" id="Seg_8143" s="T162">dieses</ta>
            <ta e="T164" id="Seg_8144" s="T163">Kind-INSTR</ta>
            <ta e="T165" id="Seg_8145" s="T164">spielen-PRS-3PL</ta>
            <ta e="T166" id="Seg_8146" s="T165">Karawane-3SG.[NOM]</ta>
            <ta e="T167" id="Seg_8147" s="T166">jeder-3SG.[NOM]</ta>
            <ta e="T168" id="Seg_8148" s="T167">einspannen-PASS/REFL-CVB.SIM</ta>
            <ta e="T169" id="Seg_8149" s="T168">liegen-PTCP.PRS</ta>
            <ta e="T170" id="Seg_8150" s="T169">sein-PST2.[3SG]</ta>
            <ta e="T171" id="Seg_8151" s="T170">was.[NOM]</ta>
            <ta e="T172" id="Seg_8152" s="T171">NEG</ta>
            <ta e="T173" id="Seg_8153" s="T172">wissen-PASS/REFL-EP-NEG.[3SG]</ta>
            <ta e="T174" id="Seg_8154" s="T173">Kind-3SG.[NOM]</ta>
            <ta e="T175" id="Seg_8155" s="T174">laufen-CVB.SIM</ta>
            <ta e="T176" id="Seg_8156" s="T175">gehen-PTCP.PRS</ta>
            <ta e="T177" id="Seg_8157" s="T176">sein-PST2.[3SG]</ta>
            <ta e="T178" id="Seg_8158" s="T177">wachsen-PST2.[3SG]</ta>
            <ta e="T179" id="Seg_8159" s="T178">betrachten-CVB.SIM</ta>
            <ta e="T180" id="Seg_8160" s="T179">sitzen-TEMP-3SG</ta>
            <ta e="T181" id="Seg_8161" s="T180">Schlitten-DAT/LOC</ta>
            <ta e="T182" id="Seg_8162" s="T181">fünf</ta>
            <ta e="T183" id="Seg_8163" s="T182">weiß</ta>
            <ta e="T184" id="Seg_8164" s="T183">männliches.Rentier-PROPR</ta>
            <ta e="T185" id="Seg_8165" s="T184">EMPH-weiß</ta>
            <ta e="T186" id="Seg_8166" s="T185">Kleidung-PROPR</ta>
            <ta e="T187" id="Seg_8167" s="T186">Mensch.[NOM]</ta>
            <ta e="T188" id="Seg_8168" s="T187">fahren-PRS.[3SG]</ta>
            <ta e="T189" id="Seg_8169" s="T188">Fürst.[NOM]</ta>
            <ta e="T190" id="Seg_8170" s="T189">sein-PST2.[3SG]</ta>
            <ta e="T191" id="Seg_8171" s="T190">doch</ta>
            <ta e="T192" id="Seg_8172" s="T191">was-1SG-ACC</ta>
            <ta e="T193" id="Seg_8173" s="T192">geben-FUT-1SG=Q</ta>
            <ta e="T194" id="Seg_8174" s="T193">denken-CVB.SIM</ta>
            <ta e="T195" id="Seg_8175" s="T194">denken-PRS.[3SG]</ta>
            <ta e="T196" id="Seg_8176" s="T195">alter.Mann.[NOM]</ta>
            <ta e="T197" id="Seg_8177" s="T196">wie</ta>
            <ta e="T198" id="Seg_8178" s="T197">leben-PST1-2SG</ta>
            <ta e="T199" id="Seg_8179" s="T198">wie</ta>
            <ta e="T200" id="Seg_8180" s="T199">solch</ta>
            <ta e="T201" id="Seg_8181" s="T200">lange</ta>
            <ta e="T202" id="Seg_8182" s="T201">sein-PST1-1SG</ta>
            <ta e="T203" id="Seg_8183" s="T202">Kind-VBZ-PST2-2SG</ta>
            <ta e="T204" id="Seg_8184" s="T203">Q</ta>
            <ta e="T205" id="Seg_8185" s="T204">sagen-PRS.[3SG]</ta>
            <ta e="T206" id="Seg_8186" s="T205">Fürst.[NOM]</ta>
            <ta e="T207" id="Seg_8187" s="T206">altern-PTCP.PRS</ta>
            <ta e="T208" id="Seg_8188" s="T207">Alter-1PL-DAT/LOC</ta>
            <ta e="T209" id="Seg_8189" s="T208">Kind-VBZ-PST1-1PL</ta>
            <ta e="T210" id="Seg_8190" s="T209">sagen-PRS.[3SG]</ta>
            <ta e="T211" id="Seg_8191" s="T210">alter.Mann.[NOM]</ta>
            <ta e="T212" id="Seg_8192" s="T211">doch</ta>
            <ta e="T213" id="Seg_8193" s="T212">gut.[NOM]</ta>
            <ta e="T214" id="Seg_8194" s="T213">Steuer.[NOM]</ta>
            <ta e="T215" id="Seg_8195" s="T214">sammeln-CVB.SIM</ta>
            <ta e="T216" id="Seg_8196" s="T215">kommen-PST1-1SG</ta>
            <ta e="T217" id="Seg_8197" s="T216">sagen-PRS.[3SG]</ta>
            <ta e="T218" id="Seg_8198" s="T217">Fürst.[NOM]</ta>
            <ta e="T219" id="Seg_8199" s="T218">alter.Mann.[NOM]</ta>
            <ta e="T220" id="Seg_8200" s="T219">wie.viel</ta>
            <ta e="T221" id="Seg_8201" s="T220">INDEF</ta>
            <ta e="T222" id="Seg_8202" s="T221">Sack.[NOM]</ta>
            <ta e="T223" id="Seg_8203" s="T222">Polarfuchs-ACC</ta>
            <ta e="T224" id="Seg_8204" s="T223">geben-PRS.[3SG]</ta>
            <ta e="T225" id="Seg_8205" s="T224">Fürst-3SG.[NOM]</ta>
            <ta e="T226" id="Seg_8206" s="T225">sich.freuen-PRS.[3SG]</ta>
            <ta e="T227" id="Seg_8207" s="T226">Rest-3SG-ACC</ta>
            <ta e="T228" id="Seg_8208" s="T227">Geld.[NOM]</ta>
            <ta e="T229" id="Seg_8209" s="T228">schicken-FUT-1SG</ta>
            <ta e="T230" id="Seg_8210" s="T229">sagen-PRS.[3SG]</ta>
            <ta e="T231" id="Seg_8211" s="T230">Fürst-3SG.[NOM]</ta>
            <ta e="T232" id="Seg_8212" s="T231">sich.auf.den.Weg.machen-CAP.[3SG]</ta>
            <ta e="T233" id="Seg_8213" s="T232">wohin</ta>
            <ta e="T234" id="Seg_8214" s="T233">INDEF</ta>
            <ta e="T235" id="Seg_8215" s="T234">nenzisch</ta>
            <ta e="T236" id="Seg_8216" s="T235">Fürst-3SG-DAT/LOC</ta>
            <ta e="T237" id="Seg_8217" s="T236">rennen-PST1-3SG</ta>
            <ta e="T238" id="Seg_8218" s="T237">Freund-3SG-DAT/LOC</ta>
            <ta e="T239" id="Seg_8219" s="T238">Fürst.[NOM]</ta>
            <ta e="T240" id="Seg_8220" s="T239">Fürst-3SG-DAT/LOC</ta>
            <ta e="T241" id="Seg_8221" s="T240">kommen-PST2.[3SG]</ta>
            <ta e="T242" id="Seg_8222" s="T241">Schnaps-VBZ-CVB.SIM</ta>
            <ta e="T243" id="Seg_8223" s="T242">liegen-PRS-3PL</ta>
            <ta e="T244" id="Seg_8224" s="T243">wie.viel</ta>
            <ta e="T245" id="Seg_8225" s="T244">Tag.[NOM]</ta>
            <ta e="T246" id="Seg_8226" s="T245">Schnaps-VBZ-CVB.SIM</ta>
            <ta e="T247" id="Seg_8227" s="T246">liegen-PST2-3PL</ta>
            <ta e="T248" id="Seg_8228" s="T247">jenes</ta>
            <ta e="T249" id="Seg_8229" s="T248">leben-CVB.SEQ</ta>
            <ta e="T250" id="Seg_8230" s="T249">Neuigkeit.[NOM]</ta>
            <ta e="T251" id="Seg_8231" s="T250">erzählen-PST2.[3SG]</ta>
            <ta e="T252" id="Seg_8232" s="T251">Freund-3SG-DAT/LOC</ta>
            <ta e="T253" id="Seg_8233" s="T252">doch-doch</ta>
            <ta e="T254" id="Seg_8234" s="T253">1SG.[NOM]</ta>
            <ta e="T255" id="Seg_8235" s="T254">Mensch.[NOM]</ta>
            <ta e="T256" id="Seg_8236" s="T255">Angst.haben-PTCP.PRS</ta>
            <ta e="T257" id="Seg_8237" s="T256">Mensch-PROPR-1SG</ta>
            <ta e="T258" id="Seg_8238" s="T257">Steuer-3SG-ACC</ta>
            <ta e="T259" id="Seg_8239" s="T258">jenes.[NOM]</ta>
            <ta e="T260" id="Seg_8240" s="T259">trotz</ta>
            <ta e="T261" id="Seg_8241" s="T260">weiter</ta>
            <ta e="T262" id="Seg_8242" s="T261">bezahlen-PRS.[3SG]</ta>
            <ta e="T263" id="Seg_8243" s="T262">jenes-ABL</ta>
            <ta e="T264" id="Seg_8244" s="T263">kommen-PST1-1SG</ta>
            <ta e="T265" id="Seg_8245" s="T264">sich.wundern-PRS-1SG</ta>
            <ta e="T266" id="Seg_8246" s="T265">laufen-CVB.SIM</ta>
            <ta e="T267" id="Seg_8247" s="T266">gehen-PTCP.PRS</ta>
            <ta e="T268" id="Seg_8248" s="T267">Kind-VBZ-PST2.[3SG]</ta>
            <ta e="T269" id="Seg_8249" s="T268">selbst-3SG.[NOM]</ta>
            <ta e="T270" id="Seg_8250" s="T269">geboren.werden-PTCP.PST</ta>
            <ta e="T271" id="Seg_8251" s="T270">Kind-POSS</ta>
            <ta e="T272" id="Seg_8252" s="T271">sein-PST2.NEG.[3SG]</ta>
            <ta e="T273" id="Seg_8253" s="T272">woher</ta>
            <ta e="T274" id="Seg_8254" s="T273">nehmen-PST2-3SG</ta>
            <ta e="T275" id="Seg_8255" s="T274">sein-FUT.[3SG]=Q</ta>
            <ta e="T276" id="Seg_8256" s="T275">wer-ACC</ta>
            <ta e="T277" id="Seg_8257" s="T276">INDEF</ta>
            <ta e="T278" id="Seg_8258" s="T277">Q</ta>
            <ta e="T279" id="Seg_8259" s="T278">töten-CVB.SEQ</ta>
            <ta e="T280" id="Seg_8260" s="T279">nehmen-INFER-3SG</ta>
            <ta e="T281" id="Seg_8261" s="T280">ach</ta>
            <ta e="T282" id="Seg_8262" s="T281">sagen-PRS.[3SG]</ta>
            <ta e="T283" id="Seg_8263" s="T282">nenzisch</ta>
            <ta e="T284" id="Seg_8264" s="T283">Fürst-3SG.[NOM]</ta>
            <ta e="T285" id="Seg_8265" s="T284">vor.langer.Zeit</ta>
            <ta e="T286" id="Seg_8266" s="T285">1SG.[NOM]</ta>
            <ta e="T287" id="Seg_8267" s="T286">eins</ta>
            <ta e="T288" id="Seg_8268" s="T287">Siedlung.[NOM]</ta>
            <ta e="T289" id="Seg_8269" s="T288">Mensch-1SG.[NOM]</ta>
            <ta e="T290" id="Seg_8270" s="T289">sterben-PST2-3SG</ta>
            <ta e="T291" id="Seg_8271" s="T290">jenes-ABL</ta>
            <ta e="T292" id="Seg_8272" s="T291">eins</ta>
            <ta e="T293" id="Seg_8273" s="T292">Wiege-PROPR</ta>
            <ta e="T294" id="Seg_8274" s="T293">Kind.[NOM]</ta>
            <ta e="T295" id="Seg_8275" s="T294">bleiben-PST2.[3SG]</ta>
            <ta e="T296" id="Seg_8276" s="T295">Nachricht-PROPR-3SG.[NOM]</ta>
            <ta e="T297" id="Seg_8277" s="T296">jenes-ACC</ta>
            <ta e="T298" id="Seg_8278" s="T297">nehmen-CAUS-PST2.NEG-EP-1SG</ta>
            <ta e="T299" id="Seg_8279" s="T298">Krankheit.[NOM]</ta>
            <ta e="T300" id="Seg_8280" s="T299">folgen-FUT.[3SG]</ta>
            <ta e="T301" id="Seg_8281" s="T300">denken-CVB.SEQ-1SG</ta>
            <ta e="T302" id="Seg_8282" s="T301">jenes-ACC</ta>
            <ta e="T303" id="Seg_8283" s="T302">nehmen-INFER-3SG</ta>
            <ta e="T304" id="Seg_8284" s="T303">jenes</ta>
            <ta e="T305" id="Seg_8285" s="T304">Kind-ACC</ta>
            <ta e="T306" id="Seg_8286" s="T305">verstecken-PTCP.PST</ta>
            <ta e="T307" id="Seg_8287" s="T306">sein-PST2.[3SG]</ta>
            <ta e="T308" id="Seg_8288" s="T307">1SG-ACC</ta>
            <ta e="T309" id="Seg_8289" s="T308">töten-FUT-3PL</ta>
            <ta e="T310" id="Seg_8290" s="T309">denken-CVB.SEQ</ta>
            <ta e="T311" id="Seg_8291" s="T310">doch</ta>
            <ta e="T312" id="Seg_8292" s="T311">so</ta>
            <ta e="T313" id="Seg_8293" s="T312">raten-PRS-1SG</ta>
            <ta e="T314" id="Seg_8294" s="T313">2SG.[NOM]</ta>
            <ta e="T315" id="Seg_8295" s="T314">Mensch-2SG-ABL</ta>
            <ta e="T316" id="Seg_8296" s="T315">Angst.haben-EP-NEG.[IMP.2SG]</ta>
            <ta e="T317" id="Seg_8297" s="T316">jenes</ta>
            <ta e="T318" id="Seg_8298" s="T317">Siedlung.[NOM]</ta>
            <ta e="T319" id="Seg_8299" s="T318">Volk-3SG-ACC</ta>
            <ta e="T320" id="Seg_8300" s="T319">ausrotten-CVB.SEQ</ta>
            <ta e="T321" id="Seg_8301" s="T320">nachdem</ta>
            <ta e="T322" id="Seg_8302" s="T321">Kind-ACC</ta>
            <ta e="T323" id="Seg_8303" s="T322">nehmen-PST2.[3SG]</ta>
            <ta e="T324" id="Seg_8304" s="T323">sagen-CVB.SEQ</ta>
            <ta e="T325" id="Seg_8305" s="T324">zwei</ta>
            <ta e="T326" id="Seg_8306" s="T325">Fürst.[NOM]</ta>
            <ta e="T327" id="Seg_8307" s="T326">Zar-3SG.[NOM]</ta>
            <ta e="T328" id="Seg_8308" s="T327">sich.beklagen-RECP/COLL-CVB.SEQ</ta>
            <ta e="T329" id="Seg_8309" s="T328">schreiben-IMP.1DU</ta>
            <ta e="T330" id="Seg_8310" s="T329">dann</ta>
            <ta e="T331" id="Seg_8311" s="T330">1PL.[NOM]</ta>
            <ta e="T332" id="Seg_8312" s="T331">Wort-1PL-ACC</ta>
            <ta e="T333" id="Seg_8313" s="T332">Zar.[NOM]</ta>
            <ta e="T334" id="Seg_8314" s="T333">schneiden-FUT.[3SG]</ta>
            <ta e="T335" id="Seg_8315" s="T334">NEG-3SG</ta>
            <ta e="T336" id="Seg_8316" s="T335">3SG-ACC</ta>
            <ta e="T337" id="Seg_8317" s="T336">Gericht-VBZ-EP-CAUS-FUT.[3SG]</ta>
            <ta e="T338" id="Seg_8318" s="T337">Gefängnis-DAT/LOC</ta>
            <ta e="T339" id="Seg_8319" s="T338">schicken-FUT-3SG</ta>
            <ta e="T340" id="Seg_8320" s="T339">1PL.[NOM]</ta>
            <ta e="T341" id="Seg_8321" s="T340">3SG.[NOM]</ta>
            <ta e="T342" id="Seg_8322" s="T341">Reichtum-3SG-ACC</ta>
            <ta e="T343" id="Seg_8323" s="T342">nehmen-FUT-1PL</ta>
            <ta e="T344" id="Seg_8324" s="T343">sagen-PST2.[3SG]</ta>
            <ta e="T345" id="Seg_8325" s="T344">nenzisch</ta>
            <ta e="T346" id="Seg_8326" s="T345">Fürst-3SG.[NOM]</ta>
            <ta e="T347" id="Seg_8327" s="T346">Mensch-DAT/LOC</ta>
            <ta e="T348" id="Seg_8328" s="T347">verdächtigen-PTCP.COND-DAT/LOC</ta>
            <ta e="T349" id="Seg_8329" s="T348">schlecht.[NOM]</ta>
            <ta e="T350" id="Seg_8330" s="T349">sein-POT.3SG</ta>
            <ta e="T351" id="Seg_8331" s="T350">sagen-PRS.[3SG]</ta>
            <ta e="T352" id="Seg_8332" s="T351">nganasanisch</ta>
            <ta e="T353" id="Seg_8333" s="T352">Fürst-3SG.[NOM]</ta>
            <ta e="T354" id="Seg_8334" s="T353">dolganisch</ta>
            <ta e="T355" id="Seg_8335" s="T354">Fürst-3SG-ACC</ta>
            <ta e="T356" id="Seg_8336" s="T355">einladen-PTCP.PST</ta>
            <ta e="T357" id="Seg_8337" s="T356">sein-COND.[3SG]</ta>
            <ta e="T358" id="Seg_8338" s="T357">drei</ta>
            <ta e="T359" id="Seg_8339" s="T358">Fürst.[NOM]</ta>
            <ta e="T360" id="Seg_8340" s="T359">Vertrauen-PROPR.[NOM]</ta>
            <ta e="T361" id="Seg_8341" s="T360">sein-PTCP.FUT</ta>
            <ta e="T362" id="Seg_8342" s="T361">sein-PST1-3SG</ta>
            <ta e="T363" id="Seg_8343" s="T362">nenzisch</ta>
            <ta e="T364" id="Seg_8344" s="T363">Fürst-3SG.[NOM]</ta>
            <ta e="T365" id="Seg_8345" s="T364">Mensch-3SG.[NOM]</ta>
            <ta e="T366" id="Seg_8346" s="T365">schicken-FUT-1SG</ta>
            <ta e="T367" id="Seg_8347" s="T366">Dolgane-PL.[NOM]</ta>
            <ta e="T368" id="Seg_8348" s="T367">Fürst-3PL-DAT/LOC</ta>
            <ta e="T369" id="Seg_8349" s="T368">drei</ta>
            <ta e="T370" id="Seg_8350" s="T369">Tag.und.Nacht-EP-INSTR</ta>
            <ta e="T371" id="Seg_8351" s="T370">kommen-FUT.[3SG]</ta>
            <ta e="T372" id="Seg_8352" s="T371">einverstanden.sein-EP-MED-NEG-TEMP-3SG</ta>
            <ta e="T373" id="Seg_8353" s="T372">selbst-3SG-ACC</ta>
            <ta e="T374" id="Seg_8354" s="T373">töten-EP-PASS/REFL-FUT.[3SG]</ta>
            <ta e="T375" id="Seg_8355" s="T374">dagegen.sein-RECP/COLL-FUT.[3SG]</ta>
            <ta e="T376" id="Seg_8356" s="T375">NEG-3SG</ta>
            <ta e="T377" id="Seg_8357" s="T376">sagen-PRS.[3SG]</ta>
            <ta e="T378" id="Seg_8358" s="T377">Schlitten-PROPR</ta>
            <ta e="T379" id="Seg_8359" s="T378">Mensch-ACC</ta>
            <ta e="T380" id="Seg_8360" s="T379">schicken-PST1-3SG</ta>
            <ta e="T381" id="Seg_8361" s="T380">dolganisch</ta>
            <ta e="T382" id="Seg_8362" s="T381">Fürst-3SG-ACC</ta>
            <ta e="T383" id="Seg_8363" s="T382">einladen-CAUS-CVB.SIM</ta>
            <ta e="T384" id="Seg_8364" s="T383">drei-ORD</ta>
            <ta e="T385" id="Seg_8365" s="T384">Tag-3SG-DAT/LOC</ta>
            <ta e="T386" id="Seg_8366" s="T385">dolganisch</ta>
            <ta e="T387" id="Seg_8367" s="T386">Fürst-3SG.[NOM]</ta>
            <ta e="T388" id="Seg_8368" s="T387">kommen-PRS.[3SG]</ta>
            <ta e="T389" id="Seg_8369" s="T388">Perlen.[NOM]</ta>
            <ta e="T390" id="Seg_8370" s="T389">Kleidung-PROPR.[NOM]</ta>
            <ta e="T391" id="Seg_8371" s="T390">drei</ta>
            <ta e="T392" id="Seg_8372" s="T391">wie.viel</ta>
            <ta e="T393" id="Seg_8373" s="T392">Tag.[NOM]</ta>
            <ta e="T394" id="Seg_8374" s="T393">Schnaps-VBZ-PRS-3PL</ta>
            <ta e="T395" id="Seg_8375" s="T394">nenzisch</ta>
            <ta e="T396" id="Seg_8376" s="T395">Fürst-3SG.[NOM]</ta>
            <ta e="T397" id="Seg_8377" s="T396">erzählen-PRS.[3SG]</ta>
            <ta e="T398" id="Seg_8378" s="T397">nganasanisch</ta>
            <ta e="T399" id="Seg_8379" s="T398">Mensch-3SG.[NOM]</ta>
            <ta e="T400" id="Seg_8380" s="T399">Krankheit-ABL</ta>
            <ta e="T401" id="Seg_8381" s="T400">sterben-PTCP.PST</ta>
            <ta e="T402" id="Seg_8382" s="T401">Leute-ABL</ta>
            <ta e="T403" id="Seg_8383" s="T402">eins</ta>
            <ta e="T404" id="Seg_8384" s="T403">Kind-ACC</ta>
            <ta e="T405" id="Seg_8385" s="T404">nehmen-PST2.[3SG]</ta>
            <ta e="T406" id="Seg_8386" s="T405">jenes-ACC</ta>
            <ta e="T407" id="Seg_8387" s="T406">Krankheit.[NOM]</ta>
            <ta e="T408" id="Seg_8388" s="T407">folgen-FUT.[3SG]</ta>
            <ta e="T409" id="Seg_8389" s="T408">1PL.[NOM]</ta>
            <ta e="T410" id="Seg_8390" s="T409">Zar-DAT/LOC</ta>
            <ta e="T411" id="Seg_8391" s="T410">sich.beklagen-RECP/COLL-IMP.1PL</ta>
            <ta e="T412" id="Seg_8392" s="T411">drei-COLL</ta>
            <ta e="T413" id="Seg_8393" s="T412">schreiben-IMP.1PL</ta>
            <ta e="T414" id="Seg_8394" s="T413">dolganisch</ta>
            <ta e="T415" id="Seg_8395" s="T414">Fürst-3SG.[NOM]</ta>
            <ta e="T416" id="Seg_8396" s="T415">denken-VBZ-PST2.[3SG]</ta>
            <ta e="T417" id="Seg_8397" s="T416">sagen-PST2.[3SG]</ta>
            <ta e="T418" id="Seg_8398" s="T417">Gericht-VBZ-PTCP.PRS-3SG-ACC</ta>
            <ta e="T419" id="Seg_8399" s="T418">Gericht-VBZ-NEG.CVB</ta>
            <ta e="T420" id="Seg_8400" s="T419">jenes.[NOM]</ta>
            <ta e="T421" id="Seg_8401" s="T420">trotz</ta>
            <ta e="T422" id="Seg_8402" s="T421">verdächtigen-PTCP.PRS</ta>
            <ta e="T423" id="Seg_8403" s="T422">Grauen-3SG.[NOM]</ta>
            <ta e="T424" id="Seg_8404" s="T423">sehr</ta>
            <ta e="T425" id="Seg_8405" s="T424">Leute-ACC</ta>
            <ta e="T426" id="Seg_8406" s="T425">hacken-PST2.[3SG]</ta>
            <ta e="T427" id="Seg_8407" s="T426">sagen-CVB.SEQ</ta>
            <ta e="T428" id="Seg_8408" s="T427">Waise-ACC</ta>
            <ta e="T429" id="Seg_8409" s="T428">nehmen-PST2-3SG</ta>
            <ta e="T430" id="Seg_8410" s="T429">was.[NOM]</ta>
            <ta e="T431" id="Seg_8411" s="T430">Schuld-3SG.[NOM]=Q</ta>
            <ta e="T432" id="Seg_8412" s="T431">Gericht-DAT/LOC</ta>
            <ta e="T433" id="Seg_8413" s="T432">geben-PTCP.FUT-1PL-ACC</ta>
            <ta e="T434" id="Seg_8414" s="T433">sagen-PST2.[3SG]</ta>
            <ta e="T435" id="Seg_8415" s="T434">nenzisch</ta>
            <ta e="T436" id="Seg_8416" s="T435">Fürst-3SG.[NOM]</ta>
            <ta e="T437" id="Seg_8417" s="T436">2SG.[NOM]</ta>
            <ta e="T438" id="Seg_8418" s="T437">schützen-PRS-2SG</ta>
            <ta e="T439" id="Seg_8419" s="T438">Q</ta>
            <ta e="T440" id="Seg_8420" s="T439">jenes</ta>
            <ta e="T441" id="Seg_8421" s="T440">Mensch-ACC</ta>
            <ta e="T442" id="Seg_8422" s="T441">dann</ta>
            <ta e="T443" id="Seg_8423" s="T442">1PL.[NOM]</ta>
            <ta e="T444" id="Seg_8424" s="T443">2SG-ACC</ta>
            <ta e="T445" id="Seg_8425" s="T444">hacken-PRS-1PL</ta>
            <ta e="T446" id="Seg_8426" s="T445">Reichtum-2SG-ACC</ta>
            <ta e="T447" id="Seg_8427" s="T446">nehmen-FUT-1PL</ta>
            <ta e="T448" id="Seg_8428" s="T447">sagen-PRS.[3SG]</ta>
            <ta e="T449" id="Seg_8429" s="T448">dolganisch</ta>
            <ta e="T450" id="Seg_8430" s="T449">Fürst-3SG.[NOM]</ta>
            <ta e="T451" id="Seg_8431" s="T450">erschrecken-PST2.[3SG]</ta>
            <ta e="T452" id="Seg_8432" s="T451">töten-PTCP.FUT-AG.[NOM]</ta>
            <ta e="T453" id="Seg_8433" s="T452">Leute.[NOM]</ta>
            <ta e="T454" id="Seg_8434" s="T453">zwei-COLL.[NOM]</ta>
            <ta e="T455" id="Seg_8435" s="T454">doch</ta>
            <ta e="T456" id="Seg_8436" s="T455">wie</ta>
            <ta e="T457" id="Seg_8437" s="T456">raten-PRS-2PL</ta>
            <ta e="T458" id="Seg_8438" s="T457">sagen-PST2.[3SG]</ta>
            <ta e="T459" id="Seg_8439" s="T458">nenzisch</ta>
            <ta e="T460" id="Seg_8440" s="T459">Fürst-3SG.[NOM]</ta>
            <ta e="T461" id="Seg_8441" s="T460">1SG.[NOM]</ta>
            <ta e="T462" id="Seg_8442" s="T461">Herr.[NOM]</ta>
            <ta e="T463" id="Seg_8443" s="T462">Anführer.[NOM]</ta>
            <ta e="T464" id="Seg_8444" s="T463">sein-PRS-1SG</ta>
            <ta e="T465" id="Seg_8445" s="T464">sich.beklagen-RECP/COLL-IMP.1PL</ta>
            <ta e="T466" id="Seg_8446" s="T465">Zar-DAT/LOC</ta>
            <ta e="T467" id="Seg_8447" s="T466">dieses</ta>
            <ta e="T468" id="Seg_8448" s="T467">mutig.[NOM]</ta>
            <ta e="T469" id="Seg_8449" s="T468">nganasanisch</ta>
            <ta e="T470" id="Seg_8450" s="T469">Siedlung-3SG-ACC</ta>
            <ta e="T471" id="Seg_8451" s="T470">hacken-CVB.SEQ</ta>
            <ta e="T472" id="Seg_8452" s="T471">nachdem</ta>
            <ta e="T473" id="Seg_8453" s="T472">Kind-ACC</ta>
            <ta e="T474" id="Seg_8454" s="T473">nehmen-PST2.[3SG]</ta>
            <ta e="T475" id="Seg_8455" s="T474">sagen-CVB.SEQ</ta>
            <ta e="T476" id="Seg_8456" s="T475">schreiben-FUT-1PL</ta>
            <ta e="T477" id="Seg_8457" s="T476">1SG.[NOM]</ta>
            <ta e="T478" id="Seg_8458" s="T477">Mensch-PL-EP-1SG.[NOM]</ta>
            <ta e="T479" id="Seg_8459" s="T478">gehen-CVB.SEQ</ta>
            <ta e="T480" id="Seg_8460" s="T479">sterben-PTCP.PST</ta>
            <ta e="T481" id="Seg_8461" s="T480">Leute-ACC</ta>
            <ta e="T482" id="Seg_8462" s="T481">schießen-FREQ-FUT-3PL</ta>
            <ta e="T483" id="Seg_8463" s="T482">Kommission.[NOM]</ta>
            <ta e="T484" id="Seg_8464" s="T483">kommen-TEMP-3SG</ta>
            <ta e="T485" id="Seg_8465" s="T484">finden-CVB.SEQ</ta>
            <ta e="T486" id="Seg_8466" s="T485">nehmen-FUT.[3SG]</ta>
            <ta e="T487" id="Seg_8467" s="T486">NEG-3SG</ta>
            <ta e="T488" id="Seg_8468" s="T487">wer.[NOM]</ta>
            <ta e="T489" id="Seg_8469" s="T488">erkennen-FUT.[3SG]=Q</ta>
            <ta e="T490" id="Seg_8470" s="T489">sagen-PRS.[3SG]</ta>
            <ta e="T491" id="Seg_8471" s="T490">zehn-APRX</ta>
            <ta e="T492" id="Seg_8472" s="T491">Mensch.[NOM]</ta>
            <ta e="T493" id="Seg_8473" s="T492">gehen-CVB.SEQ</ta>
            <ta e="T494" id="Seg_8474" s="T493">vor.langer.Zeit</ta>
            <ta e="T495" id="Seg_8475" s="T494">sterben-PTCP.PST</ta>
            <ta e="T496" id="Seg_8476" s="T495">Leute-ACC</ta>
            <ta e="T497" id="Seg_8477" s="T496">schießen-FREQ-PRS-3PL</ta>
            <ta e="T498" id="Seg_8478" s="T497">schreiben-CVB.SEQ</ta>
            <ta e="T499" id="Seg_8479" s="T498">werfen-PST1-3PL</ta>
            <ta e="T500" id="Seg_8480" s="T499">so</ta>
            <ta e="T501" id="Seg_8481" s="T500">nganasanisch</ta>
            <ta e="T502" id="Seg_8482" s="T501">Fürst-3SG-ACC</ta>
            <ta e="T503" id="Seg_8483" s="T502">schicken-PRS-3PL</ta>
            <ta e="T504" id="Seg_8484" s="T503">nenzisch</ta>
            <ta e="T505" id="Seg_8485" s="T504">Fürst-3SG-COM</ta>
            <ta e="T506" id="Seg_8486" s="T505">Zar.[NOM]</ta>
            <ta e="T507" id="Seg_8487" s="T506">lesen-CVB.SEQ</ta>
            <ta e="T508" id="Seg_8488" s="T507">sehen-PRS.[3SG]</ta>
            <ta e="T509" id="Seg_8489" s="T508">zehn-APRX</ta>
            <ta e="T510" id="Seg_8490" s="T509">Zelt.[NOM]</ta>
            <ta e="T511" id="Seg_8491" s="T510">Leute-ACC</ta>
            <ta e="T512" id="Seg_8492" s="T511">hacken-PTCP.PST</ta>
            <ta e="T513" id="Seg_8493" s="T512">Kind-ACC</ta>
            <ta e="T514" id="Seg_8494" s="T513">stehlen-PTCP.PST</ta>
            <ta e="T515" id="Seg_8495" s="T514">Mensch.[NOM]</ta>
            <ta e="T516" id="Seg_8496" s="T515">Zar.[NOM]</ta>
            <ta e="T517" id="Seg_8497" s="T516">jeder-3PL-ABL</ta>
            <ta e="T518" id="Seg_8498" s="T517">fragen-PRS.[3SG]</ta>
            <ta e="T519" id="Seg_8499" s="T518">Wahrheit.[NOM]</ta>
            <ta e="T520" id="Seg_8500" s="T519">Q</ta>
            <ta e="T521" id="Seg_8501" s="T520">1SG.[NOM]</ta>
            <ta e="T522" id="Seg_8502" s="T521">3SG-ACC</ta>
            <ta e="T523" id="Seg_8503" s="T522">Gericht-VBZ-FUT-1SG</ta>
            <ta e="T524" id="Seg_8504" s="T523">2PL.[NOM]</ta>
            <ta e="T525" id="Seg_8505" s="T524">hören-PTCP.PRS-2PL-DAT/LOC</ta>
            <ta e="T526" id="Seg_8506" s="T525">nganasanisch</ta>
            <ta e="T527" id="Seg_8507" s="T526">Fürst-3SG.[NOM]</ta>
            <ta e="T528" id="Seg_8508" s="T527">Mensch-DAT/LOC</ta>
            <ta e="T529" id="Seg_8509" s="T528">sich.nähern-NEG.PTCP</ta>
            <ta e="T530" id="Seg_8510" s="T529">Mensch.[NOM]</ta>
            <ta e="T531" id="Seg_8511" s="T530">machen-PST1-3SG</ta>
            <ta e="T532" id="Seg_8512" s="T531">wahrscheinlich</ta>
            <ta e="T533" id="Seg_8513" s="T532">sagen-PRS.[3SG]</ta>
            <ta e="T534" id="Seg_8514" s="T533">alter.Mann-DAT/LOC</ta>
            <ta e="T535" id="Seg_8515" s="T534">Betrug.[NOM]</ta>
            <ta e="T536" id="Seg_8516" s="T535">Brief.[NOM]</ta>
            <ta e="T537" id="Seg_8517" s="T536">schicken-PRS-3PL</ta>
            <ta e="T538" id="Seg_8518" s="T537">Fürst-3SG.[NOM]</ta>
            <ta e="T539" id="Seg_8519" s="T538">doch</ta>
            <ta e="T540" id="Seg_8520" s="T539">Steuer-3SG-ACC</ta>
            <ta e="T541" id="Seg_8521" s="T540">Geld-3SG-ACC</ta>
            <ta e="T542" id="Seg_8522" s="T541">nehmen-CVB.SIM</ta>
            <ta e="T543" id="Seg_8523" s="T542">kommen-INCH-IMP.3SG</ta>
            <ta e="T544" id="Seg_8524" s="T543">sehr</ta>
            <ta e="T545" id="Seg_8525" s="T544">meist</ta>
            <ta e="T546" id="Seg_8526" s="T545">schnell-ADVZ</ta>
            <ta e="T547" id="Seg_8527" s="T546">sagen-CVB.SEQ</ta>
            <ta e="T548" id="Seg_8528" s="T547">schreiben-PRS.[3SG]</ta>
            <ta e="T549" id="Seg_8529" s="T548">Bote.[NOM]</ta>
            <ta e="T550" id="Seg_8530" s="T549">Mensch.[NOM]</ta>
            <ta e="T551" id="Seg_8531" s="T550">bringen-PRS.[3SG]</ta>
            <ta e="T552" id="Seg_8532" s="T551">Brief-ACC</ta>
            <ta e="T553" id="Seg_8533" s="T552">alter.Mann.[NOM]</ta>
            <ta e="T554" id="Seg_8534" s="T553">meist</ta>
            <ta e="T555" id="Seg_8535" s="T554">gut</ta>
            <ta e="T556" id="Seg_8536" s="T555">Rentier-PL-3SG-INSTR</ta>
            <ta e="T557" id="Seg_8537" s="T556">rennen-CVB.SEQ</ta>
            <ta e="T558" id="Seg_8538" s="T557">kommen-PRS.[3SG]</ta>
            <ta e="T559" id="Seg_8539" s="T558">Rentier-3SG-ACC</ta>
            <ta e="T560" id="Seg_8540" s="T559">binden-PST2.[3SG]</ta>
            <ta e="T561" id="Seg_8541" s="T560">und</ta>
            <ta e="T562" id="Seg_8542" s="T561">rennen-CVB.SEQ</ta>
            <ta e="T563" id="Seg_8543" s="T562">fallen-PRS.[3SG]</ta>
            <ta e="T564" id="Seg_8544" s="T563">Tisch-ACC</ta>
            <ta e="T565" id="Seg_8545" s="T564">um.herum</ta>
            <ta e="T566" id="Seg_8546" s="T565">Herr.[NOM]</ta>
            <ta e="T567" id="Seg_8547" s="T566">sehr.viel</ta>
            <ta e="T568" id="Seg_8548" s="T567">Zar.[NOM]</ta>
            <ta e="T569" id="Seg_8549" s="T568">Tee.[NOM]</ta>
            <ta e="T570" id="Seg_8550" s="T569">NEG</ta>
            <ta e="T571" id="Seg_8551" s="T570">trinken-EP-CAUS.[CAUS]-PST2.NEG.[3SG]</ta>
            <ta e="T572" id="Seg_8552" s="T571">Blut-3SG.[NOM]</ta>
            <ta e="T573" id="Seg_8553" s="T572">schlecht.[NOM]</ta>
            <ta e="T574" id="Seg_8554" s="T573">ganz</ta>
            <ta e="T575" id="Seg_8555" s="T574">anders.[NOM]</ta>
            <ta e="T576" id="Seg_8556" s="T575">doch</ta>
            <ta e="T577" id="Seg_8557" s="T576">erzählen.[IMP.2SG]</ta>
            <ta e="T578" id="Seg_8558" s="T577">Neuigkeit-2SG-ACC</ta>
            <ta e="T579" id="Seg_8559" s="T578">Siedlung.[NOM]</ta>
            <ta e="T580" id="Seg_8560" s="T579">Leute-ACC</ta>
            <ta e="T581" id="Seg_8561" s="T580">warum</ta>
            <ta e="T582" id="Seg_8562" s="T581">ausrotten-PST1-2SG</ta>
            <ta e="T583" id="Seg_8563" s="T582">was</ta>
            <ta e="T584" id="Seg_8564" s="T583">Siedlung-3SG-ACC</ta>
            <ta e="T585" id="Seg_8565" s="T584">bedecken-CAUS-PRS-2SG</ta>
            <ta e="T586" id="Seg_8566" s="T585">Mensch-ACC</ta>
            <ta e="T587" id="Seg_8567" s="T586">mit</ta>
            <ta e="T588" id="Seg_8568" s="T587">Streit-EP-1SG.[NOM]</ta>
            <ta e="T589" id="Seg_8569" s="T588">NEG</ta>
            <ta e="T590" id="Seg_8570" s="T589">NEG.EX</ta>
            <ta e="T591" id="Seg_8571" s="T590">Zar.[NOM]</ta>
            <ta e="T592" id="Seg_8572" s="T591">Fürst-PL.[NOM]</ta>
            <ta e="T593" id="Seg_8573" s="T592">Brief-3PL-ACC</ta>
            <ta e="T594" id="Seg_8574" s="T593">sehen-CAUS-PRS.[3SG]</ta>
            <ta e="T595" id="Seg_8575" s="T594">kommendes.Jahr-ADJZ</ta>
            <ta e="T596" id="Seg_8576" s="T595">Wasser.[NOM]</ta>
            <ta e="T597" id="Seg_8577" s="T596">kommen-PTCP.FUT.[3SG]-DAT/LOC</ta>
            <ta e="T598" id="Seg_8578" s="T597">bis.zu</ta>
            <ta e="T599" id="Seg_8579" s="T598">Gefängnis-DAT/LOC</ta>
            <ta e="T600" id="Seg_8580" s="T599">liegen-FUT-2SG</ta>
            <ta e="T601" id="Seg_8581" s="T600">1SG-COMP</ta>
            <ta e="T602" id="Seg_8582" s="T601">groß</ta>
            <ta e="T603" id="Seg_8583" s="T602">Zar-DAT/LOC</ta>
            <ta e="T604" id="Seg_8584" s="T603">schicken-FUT-1SG</ta>
            <ta e="T605" id="Seg_8585" s="T604">alter.Mann-ACC</ta>
            <ta e="T606" id="Seg_8586" s="T605">Gefängnis-DAT/LOC</ta>
            <ta e="T607" id="Seg_8587" s="T606">versperren-CVB.SEQ</ta>
            <ta e="T608" id="Seg_8588" s="T607">werfen-PRS-3PL</ta>
            <ta e="T609" id="Seg_8589" s="T608">drei</ta>
            <ta e="T610" id="Seg_8590" s="T609">Fürst.[NOM]</ta>
            <ta e="T611" id="Seg_8591" s="T610">Reichtum-3SG-ACC</ta>
            <ta e="T612" id="Seg_8592" s="T611">aufteilen-FUT-3PL</ta>
            <ta e="T613" id="Seg_8593" s="T612">Alte.[NOM]</ta>
            <ta e="T614" id="Seg_8594" s="T613">nomadisieren-CVB.SEQ</ta>
            <ta e="T615" id="Seg_8595" s="T614">kommen-PRS.[3SG]</ta>
            <ta e="T616" id="Seg_8596" s="T615">alter.Mann-3SG.[NOM]</ta>
            <ta e="T617" id="Seg_8597" s="T616">wann</ta>
            <ta e="T618" id="Seg_8598" s="T617">NEG</ta>
            <ta e="T619" id="Seg_8599" s="T618">NEG.EX</ta>
            <ta e="T620" id="Seg_8600" s="T619">Gefängnis-DAT/LOC</ta>
            <ta e="T621" id="Seg_8601" s="T620">liegen-PRS.[3SG]</ta>
            <ta e="T622" id="Seg_8602" s="T621">Zar.[NOM]</ta>
            <ta e="T623" id="Seg_8603" s="T622">Alte-ACC</ta>
            <ta e="T624" id="Seg_8604" s="T623">jagen-CVB.SEQ</ta>
            <ta e="T625" id="Seg_8605" s="T624">herausnehmen-PRS.[3SG]</ta>
            <ta e="T626" id="Seg_8606" s="T625">drei</ta>
            <ta e="T627" id="Seg_8607" s="T626">Fürst.[NOM]</ta>
            <ta e="T628" id="Seg_8608" s="T627">Reichtum-3SG-ACC</ta>
            <ta e="T629" id="Seg_8609" s="T628">bedecken-CVB.SIM</ta>
            <ta e="T630" id="Seg_8610" s="T629">fallen-CVB.SEQ</ta>
            <ta e="T631" id="Seg_8611" s="T630">aufteilen-CVB.SEQ</ta>
            <ta e="T632" id="Seg_8612" s="T631">nehmen-PRS-3PL</ta>
            <ta e="T633" id="Seg_8613" s="T632">eins</ta>
            <ta e="T634" id="Seg_8614" s="T633">Lederdecke.eines.Zeltes-ACC</ta>
            <ta e="T635" id="Seg_8615" s="T634">bleiben-CAUS-PRS-3PL</ta>
            <ta e="T636" id="Seg_8616" s="T635">Winter-3SG.[NOM]</ta>
            <ta e="T637" id="Seg_8617" s="T636">enden-CVB.SEQ</ta>
            <ta e="T638" id="Seg_8618" s="T637">Dampfer.[NOM]</ta>
            <ta e="T639" id="Seg_8619" s="T638">kommen-EMOT-PST2.[3SG]</ta>
            <ta e="T640" id="Seg_8620" s="T639">oben-ABL</ta>
            <ta e="T641" id="Seg_8621" s="T640">doch</ta>
            <ta e="T642" id="Seg_8622" s="T641">sehen.[IMP.2SG]</ta>
            <ta e="T643" id="Seg_8623" s="T642">Ehemann-2SG-ACC</ta>
            <ta e="T644" id="Seg_8624" s="T643">sagen-PRS-3PL</ta>
            <ta e="T645" id="Seg_8625" s="T644">alter.Mann.[NOM]</ta>
            <ta e="T646" id="Seg_8626" s="T645">Auge-3SG.[NOM]</ta>
            <ta e="T647" id="Seg_8627" s="T646">Grube.[NOM]</ta>
            <ta e="T648" id="Seg_8628" s="T647">Inneres-3SG-DAT/LOC</ta>
            <ta e="T649" id="Seg_8629" s="T648">hineingehen-PST2.[3SG]</ta>
            <ta e="T650" id="Seg_8630" s="T649">essen-EP-CAUS-PST2.NEG-3PL</ta>
            <ta e="T651" id="Seg_8631" s="T650">hungern-CVB.SEQ</ta>
            <ta e="T652" id="Seg_8632" s="T651">bleiben-PST2.[3SG]</ta>
            <ta e="T653" id="Seg_8633" s="T652">Dampfer.[NOM]</ta>
            <ta e="T654" id="Seg_8634" s="T653">Inneres-3SG-DAT/LOC</ta>
            <ta e="T655" id="Seg_8635" s="T654">werfen-PST2-3PL</ta>
            <ta e="T656" id="Seg_8636" s="T655">nächster</ta>
            <ta e="T657" id="Seg_8637" s="T656">Zar-DAT/LOC</ta>
            <ta e="T658" id="Seg_8638" s="T657">tragen-PRS-3PL</ta>
            <ta e="T659" id="Seg_8639" s="T658">Trage-INSTR</ta>
            <ta e="T660" id="Seg_8640" s="T659">hereinbringen-PRS-3PL</ta>
            <ta e="T661" id="Seg_8641" s="T660">sterben-CVB.SEQ</ta>
            <ta e="T662" id="Seg_8642" s="T661">sein-PTCP.PRS</ta>
            <ta e="T663" id="Seg_8643" s="T662">Mensch-ACC</ta>
            <ta e="T664" id="Seg_8644" s="T663">na</ta>
            <ta e="T665" id="Seg_8645" s="T664">wie</ta>
            <ta e="T666" id="Seg_8646" s="T665">Gericht-VBZ-FUT-1SG=Q</ta>
            <ta e="T667" id="Seg_8647" s="T666">sterben-PTCP.PST</ta>
            <ta e="T668" id="Seg_8648" s="T667">Mensch-ACC</ta>
            <ta e="T669" id="Seg_8649" s="T668">tragen-EP-IMP.2PL</ta>
            <ta e="T670" id="Seg_8650" s="T669">Krankenhaus-DAT/LOC</ta>
            <ta e="T671" id="Seg_8651" s="T670">essen-EP-CAUS-IMP.3PL</ta>
            <ta e="T672" id="Seg_8652" s="T671">sehen-IMP.3PL</ta>
            <ta e="T673" id="Seg_8653" s="T672">Doktor-PL.[NOM]</ta>
            <ta e="T674" id="Seg_8654" s="T673">heilen-PRS-3PL</ta>
            <ta e="T675" id="Seg_8655" s="T674">alt</ta>
            <ta e="T676" id="Seg_8656" s="T675">selbst-3SG.[NOM]</ta>
            <ta e="T677" id="Seg_8657" s="T676">sein-PRS.[3SG]</ta>
            <ta e="T678" id="Seg_8658" s="T677">Doktor-PL.[NOM]</ta>
            <ta e="T679" id="Seg_8659" s="T678">fragen-PRS-3PL</ta>
            <ta e="T680" id="Seg_8660" s="T679">Schuld-EP-2SG.[NOM]</ta>
            <ta e="T681" id="Seg_8661" s="T680">Q</ta>
            <ta e="T682" id="Seg_8662" s="T681">verdächtigen-PRS-3PL</ta>
            <ta e="T683" id="Seg_8663" s="T682">Q</ta>
            <ta e="T684" id="Seg_8664" s="T683">alter.Mann.[NOM]</ta>
            <ta e="T685" id="Seg_8665" s="T684">erzählen-PRS.[3SG]</ta>
            <ta e="T686" id="Seg_8666" s="T685">jeder-3SG-ACC</ta>
            <ta e="T687" id="Seg_8667" s="T686">Doktor-PL.[NOM]</ta>
            <ta e="T688" id="Seg_8668" s="T687">Blut.[NOM]</ta>
            <ta e="T689" id="Seg_8669" s="T688">nehmen-PRS-3PL</ta>
            <ta e="T690" id="Seg_8670" s="T689">Schuld-PROPR.[NOM]</ta>
            <ta e="T691" id="Seg_8671" s="T690">sein-TEMP-2SG</ta>
            <ta e="T692" id="Seg_8672" s="T691">Blut-EP-2SG.[NOM]</ta>
            <ta e="T693" id="Seg_8673" s="T692">anders.[NOM]</ta>
            <ta e="T694" id="Seg_8674" s="T693">sein-FUT.[3SG]</ta>
            <ta e="T695" id="Seg_8675" s="T694">sagen-PRS-3PL</ta>
            <ta e="T696" id="Seg_8676" s="T695">eins-INTNS.[NOM]</ta>
            <ta e="T697" id="Seg_8677" s="T696">NEG</ta>
            <ta e="T698" id="Seg_8678" s="T697">Tröpfchen.[NOM]</ta>
            <ta e="T699" id="Seg_8679" s="T698">Schuld-3SG.[NOM]</ta>
            <ta e="T700" id="Seg_8680" s="T699">NEG.EX</ta>
            <ta e="T701" id="Seg_8681" s="T700">sein-PST2.[3SG]</ta>
            <ta e="T702" id="Seg_8682" s="T701">Brief.[NOM]</ta>
            <ta e="T703" id="Seg_8683" s="T702">geben-PRS-3PL</ta>
            <ta e="T704" id="Seg_8684" s="T703">dieses-ACC</ta>
            <ta e="T705" id="Seg_8685" s="T704">Gericht-DAT/LOC</ta>
            <ta e="T706" id="Seg_8686" s="T705">direkt</ta>
            <ta e="T707" id="Seg_8687" s="T706">geben-FUT.[IMP.2SG]</ta>
            <ta e="T708" id="Seg_8688" s="T707">Fürst-PL.[NOM]</ta>
            <ta e="T709" id="Seg_8689" s="T708">Blut-3PL-ACC</ta>
            <ta e="T710" id="Seg_8690" s="T709">auch</ta>
            <ta e="T711" id="Seg_8691" s="T710">nehmen-FUT-1PL</ta>
            <ta e="T712" id="Seg_8692" s="T711">sagen-PRS-3PL</ta>
            <ta e="T713" id="Seg_8693" s="T712">Gericht.[NOM]</ta>
            <ta e="T714" id="Seg_8694" s="T713">sein-PRS.[3SG]</ta>
            <ta e="T715" id="Seg_8695" s="T714">Fürst-PL.[NOM]</ta>
            <ta e="T716" id="Seg_8696" s="T715">erster</ta>
            <ta e="T717" id="Seg_8697" s="T716">Zar.[NOM]</ta>
            <ta e="T718" id="Seg_8698" s="T717">viel</ta>
            <ta e="T719" id="Seg_8699" s="T718">Leute.[NOM]</ta>
            <ta e="T720" id="Seg_8700" s="T719">sich.sammeln-REFL-EP-PST2.[3SG]</ta>
            <ta e="T721" id="Seg_8701" s="T720">Zar.[NOM]</ta>
            <ta e="T722" id="Seg_8702" s="T721">sprechen-PRS.[3SG]</ta>
            <ta e="T723" id="Seg_8703" s="T722">Wort-3SG-ACC-Wort-3SG-ACC</ta>
            <ta e="T724" id="Seg_8704" s="T723">hören-PTCP.FUT-DAT/LOC</ta>
            <ta e="T725" id="Seg_8705" s="T724">sagen-PRS.[3SG]</ta>
            <ta e="T726" id="Seg_8706" s="T725">alter.Mann.[NOM]</ta>
            <ta e="T727" id="Seg_8707" s="T726">erzählen-PRS.[3SG]</ta>
            <ta e="T728" id="Seg_8708" s="T727">1SG.[NOM]</ta>
            <ta e="T729" id="Seg_8709" s="T728">sogar</ta>
            <ta e="T730" id="Seg_8710" s="T729">Mensch-ACC</ta>
            <ta e="T731" id="Seg_8711" s="T730">mit</ta>
            <ta e="T732" id="Seg_8712" s="T731">kämpfen-EP-NEG.PTCP.PST</ta>
            <ta e="T733" id="Seg_8713" s="T732">Mensch-1SG</ta>
            <ta e="T734" id="Seg_8714" s="T733">Bescheinigung-3SG-ACC</ta>
            <ta e="T735" id="Seg_8715" s="T734">geben-PRS.[3SG]</ta>
            <ta e="T736" id="Seg_8716" s="T735">dieses</ta>
            <ta e="T737" id="Seg_8717" s="T736">Mensch.[NOM]</ta>
            <ta e="T738" id="Seg_8718" s="T737">Tröpfchen.[NOM]</ta>
            <ta e="T739" id="Seg_8719" s="T738">NEG</ta>
            <ta e="T740" id="Seg_8720" s="T739">Schuld-POSS</ta>
            <ta e="T741" id="Seg_8721" s="T740">NEG.[3SG]</ta>
            <ta e="T742" id="Seg_8722" s="T741">Fürst-PL.[NOM]</ta>
            <ta e="T743" id="Seg_8723" s="T742">Blut-3PL-ACC</ta>
            <ta e="T744" id="Seg_8724" s="T743">nehmen-PRS-3PL</ta>
            <ta e="T745" id="Seg_8725" s="T744">nganasanisch</ta>
            <ta e="T746" id="Seg_8726" s="T745">mutig-3SG-GEN</ta>
            <ta e="T747" id="Seg_8727" s="T746">Blut-3SG.[NOM]</ta>
            <ta e="T748" id="Seg_8728" s="T747">tatsächlich</ta>
            <ta e="T749" id="Seg_8729" s="T748">anders.[NOM]</ta>
            <ta e="T750" id="Seg_8730" s="T749">Fürst-PL.[NOM]</ta>
            <ta e="T751" id="Seg_8731" s="T750">eigen-3PL.[NOM]</ta>
            <ta e="T752" id="Seg_8732" s="T751">ganz</ta>
            <ta e="T753" id="Seg_8733" s="T752">anders.[NOM]</ta>
            <ta e="T754" id="Seg_8734" s="T753">Zar.[NOM]</ta>
            <ta e="T755" id="Seg_8735" s="T754">doch</ta>
            <ta e="T756" id="Seg_8736" s="T755">böse.werden-PRS.[3SG]</ta>
            <ta e="T757" id="Seg_8737" s="T756">wer.[NOM]</ta>
            <ta e="T758" id="Seg_8738" s="T757">raten-PST2-3SG=Q</ta>
            <ta e="T759" id="Seg_8739" s="T758">erzählen-IMP.2PL</ta>
            <ta e="T760" id="Seg_8740" s="T759">dolganisch</ta>
            <ta e="T761" id="Seg_8741" s="T760">Fürst-3SG.[NOM]</ta>
            <ta e="T762" id="Seg_8742" s="T761">erzählen-PRS.[3SG]</ta>
            <ta e="T763" id="Seg_8743" s="T762">doch</ta>
            <ta e="T764" id="Seg_8744" s="T763">Wahrheit-3SG.[NOM]</ta>
            <ta e="T765" id="Seg_8745" s="T764">1PL.[NOM]</ta>
            <ta e="T766" id="Seg_8746" s="T765">Schuld-PROPR-1PL</ta>
            <ta e="T767" id="Seg_8747" s="T766">meist</ta>
            <ta e="T768" id="Seg_8748" s="T767">groß</ta>
            <ta e="T769" id="Seg_8749" s="T768">Schuld-PROPR.[NOM]</ta>
            <ta e="T770" id="Seg_8750" s="T769">nenzisch</ta>
            <ta e="T771" id="Seg_8751" s="T770">Fürst-3SG.[NOM]</ta>
            <ta e="T772" id="Seg_8752" s="T771">1SG-ACC</ta>
            <ta e="T773" id="Seg_8753" s="T772">hacken-PTCP.FUT</ta>
            <ta e="T774" id="Seg_8754" s="T773">es.gibt-3PL</ta>
            <ta e="T775" id="Seg_8755" s="T774">erschrecken-CVB.SEQ</ta>
            <ta e="T777" id="Seg_8756" s="T776">geben-PST2-EP-1SG</ta>
            <ta e="T778" id="Seg_8757" s="T777">Zar.[NOM]</ta>
            <ta e="T779" id="Seg_8758" s="T778">doch</ta>
            <ta e="T780" id="Seg_8759" s="T779">alter.Mann.[NOM]</ta>
            <ta e="T781" id="Seg_8760" s="T780">2SG.[NOM]</ta>
            <ta e="T782" id="Seg_8761" s="T781">Schuld-EP-2SG.[NOM]</ta>
            <ta e="T783" id="Seg_8762" s="T782">NEG.EX</ta>
            <ta e="T784" id="Seg_8763" s="T783">sein-PST2.[3SG]</ta>
            <ta e="T785" id="Seg_8764" s="T784">Gefängnis-DAT/LOC</ta>
            <ta e="T786" id="Seg_8765" s="T785">2SG-ACC</ta>
            <ta e="T787" id="Seg_8766" s="T786">Gericht-VBZ-PTCP.PST</ta>
            <ta e="T788" id="Seg_8767" s="T787">Zar-ACC</ta>
            <ta e="T789" id="Seg_8768" s="T788">setzen-FUT-1SG</ta>
            <ta e="T790" id="Seg_8769" s="T789">2SG.[NOM]</ta>
            <ta e="T791" id="Seg_8770" s="T790">3SG.[NOM]</ta>
            <ta e="T792" id="Seg_8771" s="T791">Stelle-DAT/LOC</ta>
            <ta e="T793" id="Seg_8772" s="T792">Zar-INSTR</ta>
            <ta e="T794" id="Seg_8773" s="T793">gehen-FUT-2SG</ta>
            <ta e="T795" id="Seg_8774" s="T794">Fürst-PL-ACC</ta>
            <ta e="T796" id="Seg_8775" s="T795">dort</ta>
            <ta e="T797" id="Seg_8776" s="T796">ankommen-CVB.SEQ</ta>
            <ta e="T798" id="Seg_8777" s="T797">Gericht-VBZ-FUT.[IMP.2SG]</ta>
            <ta e="T799" id="Seg_8778" s="T798">sagen-PST2.[3SG]</ta>
            <ta e="T800" id="Seg_8779" s="T799">sich.auf.den.Weg.machen-CVB.SEQ</ta>
            <ta e="T801" id="Seg_8780" s="T800">bleiben-PRS-3PL</ta>
            <ta e="T802" id="Seg_8781" s="T801">zurück</ta>
            <ta e="T803" id="Seg_8782" s="T802">alter.Mann.[NOM]</ta>
            <ta e="T804" id="Seg_8783" s="T803">Siedlung-3SG-DAT/LOC</ta>
            <ta e="T805" id="Seg_8784" s="T804">kommen-CVB.SEQ</ta>
            <ta e="T806" id="Seg_8785" s="T805">Fürst.[NOM]</ta>
            <ta e="T807" id="Seg_8786" s="T806">werden-PRS.[3SG]</ta>
            <ta e="T808" id="Seg_8787" s="T807">Sohn-3SG.[NOM]</ta>
            <ta e="T809" id="Seg_8788" s="T808">wachsen-CVB.SEQ</ta>
            <ta e="T810" id="Seg_8789" s="T809">direkt-direkt</ta>
            <ta e="T811" id="Seg_8790" s="T810">Held.[NOM]</ta>
            <ta e="T812" id="Seg_8791" s="T811">werden-PST2.[3SG]</ta>
            <ta e="T813" id="Seg_8792" s="T812">3SG.[NOM]</ta>
            <ta e="T814" id="Seg_8793" s="T813">nein-3SG</ta>
            <ta e="T815" id="Seg_8794" s="T814">töten-CVB.PURP</ta>
            <ta e="T816" id="Seg_8795" s="T815">wollen-PST2-3PL</ta>
            <ta e="T817" id="Seg_8796" s="T816">Vater-3SG-DAT/LOC</ta>
            <ta e="T818" id="Seg_8797" s="T817">ähnlich</ta>
            <ta e="T819" id="Seg_8798" s="T818">sein-FUT.[3SG]</ta>
            <ta e="T820" id="Seg_8799" s="T819">denken-CVB.SEQ</ta>
            <ta e="T821" id="Seg_8800" s="T820">Junge.[NOM]</ta>
            <ta e="T822" id="Seg_8801" s="T821">eins</ta>
            <ta e="T823" id="Seg_8802" s="T822">Mensch-ACC</ta>
            <ta e="T824" id="Seg_8803" s="T823">umwerfen-ADVZ</ta>
            <ta e="T825" id="Seg_8804" s="T824">schlagen-EP-PST2.[3SG]</ta>
            <ta e="T826" id="Seg_8805" s="T825">jenes-ACC</ta>
            <ta e="T827" id="Seg_8806" s="T826">Gefängnis-DAT/LOC</ta>
            <ta e="T828" id="Seg_8807" s="T827">setzen-PST2-3PL</ta>
            <ta e="T829" id="Seg_8808" s="T828">Fürst-3PL-ACC</ta>
            <ta e="T830" id="Seg_8809" s="T829">urteilen-PRS.[3SG]</ta>
            <ta e="T831" id="Seg_8810" s="T830">nganasanisch</ta>
            <ta e="T832" id="Seg_8811" s="T831">Fürst-3SG-ACC</ta>
            <ta e="T833" id="Seg_8812" s="T832">nenzisch</ta>
            <ta e="T834" id="Seg_8813" s="T833">Fürst-3SG-COM</ta>
            <ta e="T835" id="Seg_8814" s="T834">Gefängnis-DAT/LOC</ta>
            <ta e="T836" id="Seg_8815" s="T835">setzen-PRS.[3SG]</ta>
            <ta e="T837" id="Seg_8816" s="T836">dolganisch</ta>
            <ta e="T838" id="Seg_8817" s="T837">Fürst-3SG-ACC</ta>
            <ta e="T839" id="Seg_8818" s="T838">Fürst-3SG-ABL</ta>
            <ta e="T840" id="Seg_8819" s="T839">ausziehen-PRS.[3SG]</ta>
            <ta e="T841" id="Seg_8820" s="T840">Sohn-3SG-ACC</ta>
            <ta e="T842" id="Seg_8821" s="T841">nganasanisch</ta>
            <ta e="T843" id="Seg_8822" s="T842">Fürst-3SG.[NOM]</ta>
            <ta e="T844" id="Seg_8823" s="T843">machen-PRS.[3SG]</ta>
            <ta e="T845" id="Seg_8824" s="T844">alter.Mann.[NOM]</ta>
            <ta e="T846" id="Seg_8825" s="T845">solch</ta>
            <ta e="T847" id="Seg_8826" s="T846">Wahrheit-3SG.[NOM]</ta>
            <ta e="T848" id="Seg_8827" s="T847">hinausgehen-EP-PST2-3SG</ta>
            <ta e="T849" id="Seg_8828" s="T848">letzter-3SG.[NOM]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_8829" s="T0">царь-PROPR-PL.[NOM]</ta>
            <ta e="T2" id="Seg_8830" s="T1">хозяин-PROPR-PL.[NOM]</ta>
            <ta e="T3" id="Seg_8831" s="T2">князь-PROPR-PL.[NOM]</ta>
            <ta e="T4" id="Seg_8832" s="T3">быть-INFER-3SG</ta>
            <ta e="T5" id="Seg_8833" s="T4">этот</ta>
            <ta e="T6" id="Seg_8834" s="T5">человек.[NOM]</ta>
            <ta e="T7" id="Seg_8835" s="T6">отдельно</ta>
            <ta e="T8" id="Seg_8836" s="T7">жить-PRS.[3SG]</ta>
            <ta e="T9" id="Seg_8837" s="T8">смелый.[NOM]</ta>
            <ta e="T10" id="Seg_8838" s="T9">только</ta>
            <ta e="T11" id="Seg_8839" s="T10">один-INTNS.[NOM]</ta>
            <ta e="T12" id="Seg_8840" s="T11">да</ta>
            <ta e="T13" id="Seg_8841" s="T12">ребенок-POSS</ta>
            <ta e="T14" id="Seg_8842" s="T13">NEG.[3SG]</ta>
            <ta e="T15" id="Seg_8843" s="T14">год-DAT/LOC</ta>
            <ta e="T16" id="Seg_8844" s="T15">однажды</ta>
            <ta e="T17" id="Seg_8845" s="T16">платить-PRS.[3SG]</ta>
            <ta e="T18" id="Seg_8846" s="T17">налог-3SG-ACC</ta>
            <ta e="T19" id="Seg_8847" s="T18">море.[NOM]</ta>
            <ta e="T20" id="Seg_8848" s="T19">край-3SG-DAT/LOC</ta>
            <ta e="T21" id="Seg_8849" s="T20">настоящий</ta>
            <ta e="T22" id="Seg_8850" s="T21">тундра-DAT/LOC</ta>
            <ta e="T23" id="Seg_8851" s="T22">жить-PRS.[3SG]</ta>
            <ta e="T24" id="Seg_8852" s="T23">море.[NOM]</ta>
            <ta e="T25" id="Seg_8853" s="T24">валежник-3SG-ACC</ta>
            <ta e="T26" id="Seg_8854" s="T25">отапливать-EP-MED-PRS.[3SG]</ta>
            <ta e="T27" id="Seg_8855" s="T26">долго</ta>
            <ta e="T28" id="Seg_8856" s="T27">жить-PST2.[3SG]</ta>
            <ta e="T29" id="Seg_8857" s="T28">постареть-EP-PST2.[3SG]</ta>
            <ta e="T30" id="Seg_8858" s="T29">год-3SG.[NOM]</ta>
            <ta e="T31" id="Seg_8859" s="T30">весна-INCH-CVB.SEQ</ta>
            <ta e="T32" id="Seg_8860" s="T31">идти-PRS.[3SG]</ta>
            <ta e="T33" id="Seg_8861" s="T32">старуха-3SG-DAT/LOC</ta>
            <ta e="T34" id="Seg_8862" s="T33">говорить-PRS.[3SG]</ta>
            <ta e="T35" id="Seg_8863" s="T34">год-1PL.[NOM]</ta>
            <ta e="T36" id="Seg_8864" s="T35">таять-PST1-3SG</ta>
            <ta e="T37" id="Seg_8865" s="T36">EMPH</ta>
            <ta e="T38" id="Seg_8866" s="T37">дерево.[NOM]</ta>
            <ta e="T39" id="Seg_8867" s="T38">тянуть-EP-MED-CVB.SIM</ta>
            <ta e="T40" id="Seg_8868" s="T39">идти-FUT-1SG</ta>
            <ta e="T41" id="Seg_8869" s="T40">будущий.год-ADJZ-DAT/LOC</ta>
            <ta e="T42" id="Seg_8870" s="T41">тридцать</ta>
            <ta e="T43" id="Seg_8871" s="T42">сани-PART</ta>
            <ta e="T44" id="Seg_8872" s="T43">приготовить.[IMP.2SG]</ta>
            <ta e="T45" id="Seg_8873" s="T44">тридцать</ta>
            <ta e="T46" id="Seg_8874" s="T45">сани-INSTR</ta>
            <ta e="T47" id="Seg_8875" s="T46">море-DAT/LOC</ta>
            <ta e="T48" id="Seg_8876" s="T47">идти-CVB.SEQ</ta>
            <ta e="T49" id="Seg_8877" s="T48">оставаться-PRS.[3SG]</ta>
            <ta e="T50" id="Seg_8878" s="T49">EMPH-одиноко</ta>
            <ta e="T51" id="Seg_8879" s="T50">море-DAT/LOC</ta>
            <ta e="T52" id="Seg_8880" s="T51">мыс.[NOM]</ta>
            <ta e="T53" id="Seg_8881" s="T52">очень-INSTR</ta>
            <ta e="T54" id="Seg_8882" s="T53">входить-PST2.[3SG]</ta>
            <ta e="T55" id="Seg_8883" s="T54">одинаковый</ta>
            <ta e="T56" id="Seg_8884" s="T55">место-ABL</ta>
            <ta e="T57" id="Seg_8885" s="T56">грузить-MED-PST2.[3SG]</ta>
            <ta e="T58" id="Seg_8886" s="T57">высокий.берег-3SG-GEN</ta>
            <ta e="T59" id="Seg_8887" s="T58">верхняя.часть-3SG-DAT/LOC</ta>
            <ta e="T60" id="Seg_8888" s="T59">один</ta>
            <ta e="T61" id="Seg_8889" s="T60">русло.[NOM]</ta>
            <ta e="T62" id="Seg_8890" s="T61">через</ta>
            <ta e="T63" id="Seg_8891" s="T62">возвращаться-EP-PST2.[3SG]</ta>
            <ta e="T64" id="Seg_8892" s="T63">этот</ta>
            <ta e="T65" id="Seg_8893" s="T64">выйти-CVB.SEQ</ta>
            <ta e="T66" id="Seg_8894" s="T65">гора-3SG-GEN</ta>
            <ta e="T67" id="Seg_8895" s="T66">верхняя.часть-3SG-DAT/LOC</ta>
            <ta e="T68" id="Seg_8896" s="T67">табак.[NOM]</ta>
            <ta e="T69" id="Seg_8897" s="T68">курить-CVB.SIM</ta>
            <ta e="T70" id="Seg_8898" s="T69">сидеть-PST2.[3SG]</ta>
            <ta e="T71" id="Seg_8899" s="T70">река.[NOM]</ta>
            <ta e="T72" id="Seg_8900" s="T71">к</ta>
            <ta e="T73" id="Seg_8901" s="T72">видеть-PST2-3SG</ta>
            <ta e="T74" id="Seg_8902" s="T73">вода-DAT/LOC</ta>
            <ta e="T75" id="Seg_8903" s="T74">ребенок.[NOM]</ta>
            <ta e="T76" id="Seg_8904" s="T75">идти-PRS.[3SG]</ta>
            <ta e="T77" id="Seg_8905" s="T76">таять-PTCP.PST</ta>
            <ta e="T78" id="Seg_8906" s="T77">вода-DAT/LOC</ta>
            <ta e="T79" id="Seg_8907" s="T78">пена-3PL-INSTR</ta>
            <ta e="T80" id="Seg_8908" s="T79">играть-CVB.SIM</ta>
            <ta e="T81" id="Seg_8909" s="T80">идти-PRS.[3SG]</ta>
            <ta e="T82" id="Seg_8910" s="T81">мальчик.[NOM]</ta>
            <ta e="T83" id="Seg_8911" s="T82">ребенок.[NOM]</ta>
            <ta e="T84" id="Seg_8912" s="T83">наверное</ta>
            <ta e="T85" id="Seg_8913" s="T84">откуда</ta>
            <ta e="T86" id="Seg_8914" s="T85">да</ta>
            <ta e="T87" id="Seg_8915" s="T86">приходить-PST2-3SG</ta>
            <ta e="T88" id="Seg_8916" s="T87">знать-PASS/REFL-EP-NEG.[3SG]</ta>
            <ta e="T89" id="Seg_8917" s="T88">удивляться-PST2.[3SG]</ta>
            <ta e="T90" id="Seg_8918" s="T89">1SG.[NOM]</ta>
            <ta e="T91" id="Seg_8919" s="T90">ребенок-POSS</ta>
            <ta e="T92" id="Seg_8920" s="T91">NEG-1SG</ta>
            <ta e="T93" id="Seg_8921" s="T92">EMPH</ta>
            <ta e="T94" id="Seg_8922" s="T93">взять-PTCP.PST.[NOM]</ta>
            <ta e="T95" id="Seg_8923" s="T94">MOD</ta>
            <ta e="T96" id="Seg_8924" s="T95">думать-CVB.SIM</ta>
            <ta e="T97" id="Seg_8925" s="T96">думать-PST2.[3SG]</ta>
            <ta e="T98" id="Seg_8926" s="T97">ребенок-3SG.[NOM]</ta>
            <ta e="T99" id="Seg_8927" s="T98">остров.[NOM]</ta>
            <ta e="T100" id="Seg_8928" s="T99">песок-3SG-DAT/LOC</ta>
            <ta e="T101" id="Seg_8929" s="T100">идти-PRS.[3SG]</ta>
            <ta e="T102" id="Seg_8930" s="T101">кривой</ta>
            <ta e="T103" id="Seg_8931" s="T102">дерево.[NOM]</ta>
            <ta e="T104" id="Seg_8932" s="T103">есть</ta>
            <ta e="T105" id="Seg_8933" s="T104">тот-INSTR</ta>
            <ta e="T106" id="Seg_8934" s="T105">идти-INFER-3SG</ta>
            <ta e="T107" id="Seg_8935" s="T106">прыгать-CVB.SEQ</ta>
            <ta e="T108" id="Seg_8936" s="T107">доезжать-PRS.[3SG]</ta>
            <ta e="T109" id="Seg_8937" s="T108">старик.[NOM]</ta>
            <ta e="T110" id="Seg_8938" s="T109">задняя.сторона-3SG-ABL</ta>
            <ta e="T111" id="Seg_8939" s="T110">поймать-CVB.SEQ</ta>
            <ta e="T112" id="Seg_8940" s="T111">взять-CVB.SEQ</ta>
            <ta e="T113" id="Seg_8941" s="T112">сокуй-3SG-GEN</ta>
            <ta e="T114" id="Seg_8942" s="T113">нутро-3SG-DAT/LOC</ta>
            <ta e="T115" id="Seg_8943" s="T114">вставлять-PST2.[3SG]</ta>
            <ta e="T116" id="Seg_8944" s="T115">подпоясать-EP-REFL-CVB.SEQ</ta>
            <ta e="T117" id="Seg_8945" s="T116">бросать-PST2.[3SG]</ta>
            <ta e="T118" id="Seg_8946" s="T117">вот</ta>
            <ta e="T119" id="Seg_8947" s="T118">только</ta>
            <ta e="T120" id="Seg_8948" s="T119">отправляться-CAP.[3SG]</ta>
            <ta e="T121" id="Seg_8949" s="T120">звук.[NOM]</ta>
            <ta e="T122" id="Seg_8950" s="T121">бормотанье-3SG.[NOM]</ta>
            <ta e="T123" id="Seg_8951" s="T122">быть-PRS.[3SG]</ta>
            <ta e="T124" id="Seg_8952" s="T123">стоять-PST2.[3SG]</ta>
            <ta e="T125" id="Seg_8953" s="T124">видеть-PST2-3SG</ta>
            <ta e="T126" id="Seg_8954" s="T125">полный</ta>
            <ta e="T127" id="Seg_8955" s="T126">стойбище.[NOM]</ta>
            <ta e="T128" id="Seg_8956" s="T127">человек-3SG.[NOM]</ta>
            <ta e="T129" id="Seg_8957" s="T128">умирать-PTCP.PST</ta>
            <ta e="T130" id="Seg_8958" s="T129">место-3SG.[NOM]</ta>
            <ta e="T131" id="Seg_8959" s="T130">быть-PST2.[3SG]</ta>
            <ta e="T132" id="Seg_8960" s="T131">ээ</ta>
            <ta e="T133" id="Seg_8961" s="T132">этот-PL-ABL</ta>
            <ta e="T134" id="Seg_8962" s="T133">быть.лишным-CVB.SEQ</ta>
            <ta e="T135" id="Seg_8963" s="T134">оставаться-PST2.[3SG]</ta>
            <ta e="T136" id="Seg_8964" s="T135">ребенок.[NOM]</ta>
            <ta e="T137" id="Seg_8965" s="T136">MOD</ta>
            <ta e="T138" id="Seg_8966" s="T137">один</ta>
            <ta e="T139" id="Seg_8967" s="T138">задняя.сторона-1PL-PROPR</ta>
            <ta e="T140" id="Seg_8968" s="T139">передовой.олень-3SG-ACC</ta>
            <ta e="T141" id="Seg_8969" s="T140">принести-CVB.SEQ</ta>
            <ta e="T142" id="Seg_8970" s="T141">убить-CVB.SEQ</ta>
            <ta e="T143" id="Seg_8971" s="T142">бросать-PRS.[3SG]</ta>
            <ta e="T144" id="Seg_8972" s="T143">ребенок-2PL-ACC</ta>
            <ta e="T145" id="Seg_8973" s="T144">носить-PST1-1SG</ta>
            <ta e="T146" id="Seg_8974" s="T145">этот</ta>
            <ta e="T147" id="Seg_8975" s="T146">ребенок-2PL.[NOM]</ta>
            <ta e="T148" id="Seg_8976" s="T147">след-3SG.[NOM]</ta>
            <ta e="T149" id="Seg_8977" s="T148">один</ta>
            <ta e="T150" id="Seg_8978" s="T149">олень-ACC</ta>
            <ta e="T151" id="Seg_8979" s="T150">бросать-PST1-1SG</ta>
            <ta e="T152" id="Seg_8980" s="T151">говорить-PRS.[3SG]</ta>
            <ta e="T153" id="Seg_8981" s="T152">принести-PST1-3SG</ta>
            <ta e="T154" id="Seg_8982" s="T153">дом-3SG-DAT/LOC</ta>
            <ta e="T155" id="Seg_8983" s="T154">старуха-3SG-DAT/LOC</ta>
            <ta e="T156" id="Seg_8984" s="T155">рассказывать-PRS.[3SG]</ta>
            <ta e="T157" id="Seg_8985" s="T156">три</ta>
            <ta e="T158" id="Seg_8986" s="T157">день.[NOM]</ta>
            <ta e="T159" id="Seg_8987" s="T158">так.долго</ta>
            <ta e="T160" id="Seg_8988" s="T159">олень-3PL-ACC</ta>
            <ta e="T161" id="Seg_8989" s="T160">да</ta>
            <ta e="T162" id="Seg_8990" s="T161">пустить-NEG.CVB.SIM</ta>
            <ta e="T163" id="Seg_8991" s="T162">этот</ta>
            <ta e="T164" id="Seg_8992" s="T163">ребенок-INSTR</ta>
            <ta e="T165" id="Seg_8993" s="T164">играть-PRS-3PL</ta>
            <ta e="T166" id="Seg_8994" s="T165">караван-3SG.[NOM]</ta>
            <ta e="T167" id="Seg_8995" s="T166">каждый-3SG.[NOM]</ta>
            <ta e="T168" id="Seg_8996" s="T167">запрячь-PASS/REFL-CVB.SIM</ta>
            <ta e="T169" id="Seg_8997" s="T168">лежать-PTCP.PRS</ta>
            <ta e="T170" id="Seg_8998" s="T169">быть-PST2.[3SG]</ta>
            <ta e="T171" id="Seg_8999" s="T170">что.[NOM]</ta>
            <ta e="T172" id="Seg_9000" s="T171">NEG</ta>
            <ta e="T173" id="Seg_9001" s="T172">знать-PASS/REFL-EP-NEG.[3SG]</ta>
            <ta e="T174" id="Seg_9002" s="T173">ребенок-3SG.[NOM]</ta>
            <ta e="T175" id="Seg_9003" s="T174">бежать-CVB.SIM</ta>
            <ta e="T176" id="Seg_9004" s="T175">идти-PTCP.PRS</ta>
            <ta e="T177" id="Seg_9005" s="T176">быть-PST2.[3SG]</ta>
            <ta e="T178" id="Seg_9006" s="T177">расти-PST2.[3SG]</ta>
            <ta e="T179" id="Seg_9007" s="T178">смотреть-CVB.SIM</ta>
            <ta e="T180" id="Seg_9008" s="T179">сидеть-TEMP-3SG</ta>
            <ta e="T181" id="Seg_9009" s="T180">сани-DAT/LOC</ta>
            <ta e="T182" id="Seg_9010" s="T181">пять</ta>
            <ta e="T183" id="Seg_9011" s="T182">белый</ta>
            <ta e="T184" id="Seg_9012" s="T183">олений.самец-PROPR</ta>
            <ta e="T185" id="Seg_9013" s="T184">EMPH-белый</ta>
            <ta e="T186" id="Seg_9014" s="T185">одежда-PROPR</ta>
            <ta e="T187" id="Seg_9015" s="T186">человек.[NOM]</ta>
            <ta e="T188" id="Seg_9016" s="T187">ехать-PRS.[3SG]</ta>
            <ta e="T189" id="Seg_9017" s="T188">князь.[NOM]</ta>
            <ta e="T190" id="Seg_9018" s="T189">быть-PST2.[3SG]</ta>
            <ta e="T191" id="Seg_9019" s="T190">вот</ta>
            <ta e="T192" id="Seg_9020" s="T191">что-1SG-ACC</ta>
            <ta e="T193" id="Seg_9021" s="T192">давать-FUT-1SG=Q</ta>
            <ta e="T194" id="Seg_9022" s="T193">думать-CVB.SIM</ta>
            <ta e="T195" id="Seg_9023" s="T194">думать-PRS.[3SG]</ta>
            <ta e="T196" id="Seg_9024" s="T195">старик.[NOM]</ta>
            <ta e="T197" id="Seg_9025" s="T196">как</ta>
            <ta e="T198" id="Seg_9026" s="T197">жить-PST1-2SG</ta>
            <ta e="T199" id="Seg_9027" s="T198">как</ta>
            <ta e="T200" id="Seg_9028" s="T199">такой</ta>
            <ta e="T201" id="Seg_9029" s="T200">долго</ta>
            <ta e="T202" id="Seg_9030" s="T201">быть-PST1-1SG</ta>
            <ta e="T203" id="Seg_9031" s="T202">ребенок-VBZ-PST2-2SG</ta>
            <ta e="T204" id="Seg_9032" s="T203">Q</ta>
            <ta e="T205" id="Seg_9033" s="T204">говорить-PRS.[3SG]</ta>
            <ta e="T206" id="Seg_9034" s="T205">князь.[NOM]</ta>
            <ta e="T207" id="Seg_9035" s="T206">постареть-PTCP.PRS</ta>
            <ta e="T208" id="Seg_9036" s="T207">возраст-1PL-DAT/LOC</ta>
            <ta e="T209" id="Seg_9037" s="T208">ребенок-VBZ-PST1-1PL</ta>
            <ta e="T210" id="Seg_9038" s="T209">говорить-PRS.[3SG]</ta>
            <ta e="T211" id="Seg_9039" s="T210">старик.[NOM]</ta>
            <ta e="T212" id="Seg_9040" s="T211">вот</ta>
            <ta e="T213" id="Seg_9041" s="T212">хороший.[NOM]</ta>
            <ta e="T214" id="Seg_9042" s="T213">налог.[NOM]</ta>
            <ta e="T215" id="Seg_9043" s="T214">собирать-CVB.SIM</ta>
            <ta e="T216" id="Seg_9044" s="T215">приходить-PST1-1SG</ta>
            <ta e="T217" id="Seg_9045" s="T216">говорить-PRS.[3SG]</ta>
            <ta e="T218" id="Seg_9046" s="T217">князь.[NOM]</ta>
            <ta e="T219" id="Seg_9047" s="T218">старик.[NOM]</ta>
            <ta e="T220" id="Seg_9048" s="T219">сколько</ta>
            <ta e="T221" id="Seg_9049" s="T220">INDEF</ta>
            <ta e="T222" id="Seg_9050" s="T221">мешок.[NOM]</ta>
            <ta e="T223" id="Seg_9051" s="T222">песец-ACC</ta>
            <ta e="T224" id="Seg_9052" s="T223">давать-PRS.[3SG]</ta>
            <ta e="T225" id="Seg_9053" s="T224">князь-3SG.[NOM]</ta>
            <ta e="T226" id="Seg_9054" s="T225">радоваться-PRS.[3SG]</ta>
            <ta e="T227" id="Seg_9055" s="T226">остаток-3SG-ACC</ta>
            <ta e="T228" id="Seg_9056" s="T227">деньги.[NOM]</ta>
            <ta e="T229" id="Seg_9057" s="T228">послать-FUT-1SG</ta>
            <ta e="T230" id="Seg_9058" s="T229">говорить-PRS.[3SG]</ta>
            <ta e="T231" id="Seg_9059" s="T230">князь-3SG.[NOM]</ta>
            <ta e="T232" id="Seg_9060" s="T231">отправляться-CAP.[3SG]</ta>
            <ta e="T233" id="Seg_9061" s="T232">куда</ta>
            <ta e="T234" id="Seg_9062" s="T233">INDEF</ta>
            <ta e="T235" id="Seg_9063" s="T234">ненецкий</ta>
            <ta e="T236" id="Seg_9064" s="T235">князь-3SG-DAT/LOC</ta>
            <ta e="T237" id="Seg_9065" s="T236">мчаться-PST1-3SG</ta>
            <ta e="T238" id="Seg_9066" s="T237">друг-3SG-DAT/LOC</ta>
            <ta e="T239" id="Seg_9067" s="T238">князь.[NOM]</ta>
            <ta e="T240" id="Seg_9068" s="T239">князь-3SG-DAT/LOC</ta>
            <ta e="T241" id="Seg_9069" s="T240">приходить-PST2.[3SG]</ta>
            <ta e="T242" id="Seg_9070" s="T241">спиртное-VBZ-CVB.SIM</ta>
            <ta e="T243" id="Seg_9071" s="T242">лежать-PRS-3PL</ta>
            <ta e="T244" id="Seg_9072" s="T243">сколько</ta>
            <ta e="T245" id="Seg_9073" s="T244">день.[NOM]</ta>
            <ta e="T246" id="Seg_9074" s="T245">спиртное-VBZ-CVB.SIM</ta>
            <ta e="T247" id="Seg_9075" s="T246">лежать-PST2-3PL</ta>
            <ta e="T248" id="Seg_9076" s="T247">тот</ta>
            <ta e="T249" id="Seg_9077" s="T248">жить-CVB.SEQ</ta>
            <ta e="T250" id="Seg_9078" s="T249">новость.[NOM]</ta>
            <ta e="T251" id="Seg_9079" s="T250">рассказывать-PST2.[3SG]</ta>
            <ta e="T252" id="Seg_9080" s="T251">друг-3SG-DAT/LOC</ta>
            <ta e="T253" id="Seg_9081" s="T252">вот-вот</ta>
            <ta e="T254" id="Seg_9082" s="T253">1SG.[NOM]</ta>
            <ta e="T255" id="Seg_9083" s="T254">человек.[NOM]</ta>
            <ta e="T256" id="Seg_9084" s="T255">бояться-PTCP.PRS</ta>
            <ta e="T257" id="Seg_9085" s="T256">человек-PROPR-1SG</ta>
            <ta e="T258" id="Seg_9086" s="T257">налог-3SG-ACC</ta>
            <ta e="T259" id="Seg_9087" s="T258">тот.[NOM]</ta>
            <ta e="T260" id="Seg_9088" s="T259">вопреки</ta>
            <ta e="T261" id="Seg_9089" s="T260">дальше</ta>
            <ta e="T262" id="Seg_9090" s="T261">платить-PRS.[3SG]</ta>
            <ta e="T263" id="Seg_9091" s="T262">тот-ABL</ta>
            <ta e="T264" id="Seg_9092" s="T263">приходить-PST1-1SG</ta>
            <ta e="T265" id="Seg_9093" s="T264">удивляться-PRS-1SG</ta>
            <ta e="T266" id="Seg_9094" s="T265">бегать-CVB.SIM</ta>
            <ta e="T267" id="Seg_9095" s="T266">идти-PTCP.PRS</ta>
            <ta e="T268" id="Seg_9096" s="T267">ребенок-VBZ-PST2.[3SG]</ta>
            <ta e="T269" id="Seg_9097" s="T268">сам-3SG.[NOM]</ta>
            <ta e="T270" id="Seg_9098" s="T269">родиться-PTCP.PST</ta>
            <ta e="T271" id="Seg_9099" s="T270">ребенок-POSS</ta>
            <ta e="T272" id="Seg_9100" s="T271">быть-PST2.NEG.[3SG]</ta>
            <ta e="T273" id="Seg_9101" s="T272">откуда</ta>
            <ta e="T274" id="Seg_9102" s="T273">взять-PST2-3SG</ta>
            <ta e="T275" id="Seg_9103" s="T274">быть-FUT.[3SG]=Q</ta>
            <ta e="T276" id="Seg_9104" s="T275">кто-ACC</ta>
            <ta e="T277" id="Seg_9105" s="T276">INDEF</ta>
            <ta e="T278" id="Seg_9106" s="T277">Q</ta>
            <ta e="T279" id="Seg_9107" s="T278">убить-CVB.SEQ</ta>
            <ta e="T280" id="Seg_9108" s="T279">взять-INFER-3SG</ta>
            <ta e="T281" id="Seg_9109" s="T280">ай</ta>
            <ta e="T282" id="Seg_9110" s="T281">говорить-PRS.[3SG]</ta>
            <ta e="T283" id="Seg_9111" s="T282">ненецкий</ta>
            <ta e="T284" id="Seg_9112" s="T283">князь-3SG.[NOM]</ta>
            <ta e="T285" id="Seg_9113" s="T284">давно</ta>
            <ta e="T286" id="Seg_9114" s="T285">1SG.[NOM]</ta>
            <ta e="T287" id="Seg_9115" s="T286">один</ta>
            <ta e="T288" id="Seg_9116" s="T287">стойбище.[NOM]</ta>
            <ta e="T289" id="Seg_9117" s="T288">человек-1SG.[NOM]</ta>
            <ta e="T290" id="Seg_9118" s="T289">умирать-PST2-3SG</ta>
            <ta e="T291" id="Seg_9119" s="T290">тот-ABL</ta>
            <ta e="T292" id="Seg_9120" s="T291">один</ta>
            <ta e="T293" id="Seg_9121" s="T292">колыбель-PROPR</ta>
            <ta e="T294" id="Seg_9122" s="T293">ребенок.[NOM]</ta>
            <ta e="T295" id="Seg_9123" s="T294">оставаться-PST2.[3SG]</ta>
            <ta e="T296" id="Seg_9124" s="T295">весть-PROPR-3SG.[NOM]</ta>
            <ta e="T297" id="Seg_9125" s="T296">тот-ACC</ta>
            <ta e="T298" id="Seg_9126" s="T297">взять-CAUS-PST2.NEG-EP-1SG</ta>
            <ta e="T299" id="Seg_9127" s="T298">болезнь.[NOM]</ta>
            <ta e="T300" id="Seg_9128" s="T299">следовать-FUT.[3SG]</ta>
            <ta e="T301" id="Seg_9129" s="T300">думать-CVB.SEQ-1SG</ta>
            <ta e="T302" id="Seg_9130" s="T301">тот-ACC</ta>
            <ta e="T303" id="Seg_9131" s="T302">взять-INFER-3SG</ta>
            <ta e="T304" id="Seg_9132" s="T303">тот</ta>
            <ta e="T305" id="Seg_9133" s="T304">ребенок-ACC</ta>
            <ta e="T306" id="Seg_9134" s="T305">скрывать-PTCP.PST</ta>
            <ta e="T307" id="Seg_9135" s="T306">быть-PST2.[3SG]</ta>
            <ta e="T308" id="Seg_9136" s="T307">1SG-ACC</ta>
            <ta e="T309" id="Seg_9137" s="T308">убить-FUT-3PL</ta>
            <ta e="T310" id="Seg_9138" s="T309">думать-CVB.SEQ</ta>
            <ta e="T311" id="Seg_9139" s="T310">вот</ta>
            <ta e="T312" id="Seg_9140" s="T311">так</ta>
            <ta e="T313" id="Seg_9141" s="T312">советовать-PRS-1SG</ta>
            <ta e="T314" id="Seg_9142" s="T313">2SG.[NOM]</ta>
            <ta e="T315" id="Seg_9143" s="T314">человек-2SG-ABL</ta>
            <ta e="T316" id="Seg_9144" s="T315">бояться-EP-NEG.[IMP.2SG]</ta>
            <ta e="T317" id="Seg_9145" s="T316">тот</ta>
            <ta e="T318" id="Seg_9146" s="T317">стойбище.[NOM]</ta>
            <ta e="T319" id="Seg_9147" s="T318">народ-3SG-ACC</ta>
            <ta e="T320" id="Seg_9148" s="T319">истреблять-CVB.SEQ</ta>
            <ta e="T321" id="Seg_9149" s="T320">после</ta>
            <ta e="T322" id="Seg_9150" s="T321">ребенок-ACC</ta>
            <ta e="T323" id="Seg_9151" s="T322">взять-PST2.[3SG]</ta>
            <ta e="T324" id="Seg_9152" s="T323">говорить-CVB.SEQ</ta>
            <ta e="T325" id="Seg_9153" s="T324">два</ta>
            <ta e="T326" id="Seg_9154" s="T325">князь.[NOM]</ta>
            <ta e="T327" id="Seg_9155" s="T326">царь-3SG.[NOM]</ta>
            <ta e="T328" id="Seg_9156" s="T327">жаловаться-RECP/COLL-CVB.SEQ</ta>
            <ta e="T329" id="Seg_9157" s="T328">писать-IMP.1DU</ta>
            <ta e="T330" id="Seg_9158" s="T329">тогда</ta>
            <ta e="T331" id="Seg_9159" s="T330">1PL.[NOM]</ta>
            <ta e="T332" id="Seg_9160" s="T331">слово-1PL-ACC</ta>
            <ta e="T333" id="Seg_9161" s="T332">царь.[NOM]</ta>
            <ta e="T334" id="Seg_9162" s="T333">резать-FUT.[3SG]</ta>
            <ta e="T335" id="Seg_9163" s="T334">NEG-3SG</ta>
            <ta e="T336" id="Seg_9164" s="T335">3SG-ACC</ta>
            <ta e="T337" id="Seg_9165" s="T336">суд-VBZ-EP-CAUS-FUT.[3SG]</ta>
            <ta e="T338" id="Seg_9166" s="T337">тюрьма-DAT/LOC</ta>
            <ta e="T339" id="Seg_9167" s="T338">послать-FUT-3SG</ta>
            <ta e="T340" id="Seg_9168" s="T339">1PL.[NOM]</ta>
            <ta e="T341" id="Seg_9169" s="T340">3SG.[NOM]</ta>
            <ta e="T342" id="Seg_9170" s="T341">богатство-3SG-ACC</ta>
            <ta e="T343" id="Seg_9171" s="T342">взять-FUT-1PL</ta>
            <ta e="T344" id="Seg_9172" s="T343">говорить-PST2.[3SG]</ta>
            <ta e="T345" id="Seg_9173" s="T344">ненецкий</ta>
            <ta e="T346" id="Seg_9174" s="T345">князь-3SG.[NOM]</ta>
            <ta e="T347" id="Seg_9175" s="T346">человек-DAT/LOC</ta>
            <ta e="T348" id="Seg_9176" s="T347">подозревать-PTCP.COND-DAT/LOC</ta>
            <ta e="T349" id="Seg_9177" s="T348">плохой.[NOM]</ta>
            <ta e="T350" id="Seg_9178" s="T349">быть-POT.3SG</ta>
            <ta e="T351" id="Seg_9179" s="T350">говорить-PRS.[3SG]</ta>
            <ta e="T352" id="Seg_9180" s="T351">нганасанский</ta>
            <ta e="T353" id="Seg_9181" s="T352">князь-3SG.[NOM]</ta>
            <ta e="T354" id="Seg_9182" s="T353">долганский</ta>
            <ta e="T355" id="Seg_9183" s="T354">князь-3SG-ACC</ta>
            <ta e="T356" id="Seg_9184" s="T355">зазывать-PTCP.PST</ta>
            <ta e="T357" id="Seg_9185" s="T356">быть-COND.[3SG]</ta>
            <ta e="T358" id="Seg_9186" s="T357">три</ta>
            <ta e="T359" id="Seg_9187" s="T358">князь.[NOM]</ta>
            <ta e="T360" id="Seg_9188" s="T359">доверие-PROPR.[NOM]</ta>
            <ta e="T361" id="Seg_9189" s="T360">быть-PTCP.FUT</ta>
            <ta e="T362" id="Seg_9190" s="T361">быть-PST1-3SG</ta>
            <ta e="T363" id="Seg_9191" s="T362">ненецкий</ta>
            <ta e="T364" id="Seg_9192" s="T363">князь-3SG.[NOM]</ta>
            <ta e="T365" id="Seg_9193" s="T364">человек-3SG.[NOM]</ta>
            <ta e="T366" id="Seg_9194" s="T365">послать-FUT-1SG</ta>
            <ta e="T367" id="Seg_9195" s="T366">Долган-PL.[NOM]</ta>
            <ta e="T368" id="Seg_9196" s="T367">князь-3PL-DAT/LOC</ta>
            <ta e="T369" id="Seg_9197" s="T368">три</ta>
            <ta e="T370" id="Seg_9198" s="T369">сутки-EP-INSTR</ta>
            <ta e="T371" id="Seg_9199" s="T370">приходить-FUT.[3SG]</ta>
            <ta e="T372" id="Seg_9200" s="T371">согласить-EP-MED-NEG-TEMP-3SG</ta>
            <ta e="T373" id="Seg_9201" s="T372">сам-3SG-ACC</ta>
            <ta e="T374" id="Seg_9202" s="T373">убить-EP-PASS/REFL-FUT.[3SG]</ta>
            <ta e="T375" id="Seg_9203" s="T374">быть.против-RECP/COLL-FUT.[3SG]</ta>
            <ta e="T376" id="Seg_9204" s="T375">NEG-3SG</ta>
            <ta e="T377" id="Seg_9205" s="T376">говорить-PRS.[3SG]</ta>
            <ta e="T378" id="Seg_9206" s="T377">сани-PROPR</ta>
            <ta e="T379" id="Seg_9207" s="T378">человек-ACC</ta>
            <ta e="T380" id="Seg_9208" s="T379">послать-PST1-3SG</ta>
            <ta e="T381" id="Seg_9209" s="T380">долганский</ta>
            <ta e="T382" id="Seg_9210" s="T381">князь-3SG-ACC</ta>
            <ta e="T383" id="Seg_9211" s="T382">зазывать-CAUS-CVB.SIM</ta>
            <ta e="T384" id="Seg_9212" s="T383">три-ORD</ta>
            <ta e="T385" id="Seg_9213" s="T384">день-3SG-DAT/LOC</ta>
            <ta e="T386" id="Seg_9214" s="T385">долганский</ta>
            <ta e="T387" id="Seg_9215" s="T386">князь-3SG.[NOM]</ta>
            <ta e="T388" id="Seg_9216" s="T387">приходить-PRS.[3SG]</ta>
            <ta e="T389" id="Seg_9217" s="T388">бисер.[NOM]</ta>
            <ta e="T390" id="Seg_9218" s="T389">одежда-PROPR.[NOM]</ta>
            <ta e="T391" id="Seg_9219" s="T390">три</ta>
            <ta e="T392" id="Seg_9220" s="T391">сколько</ta>
            <ta e="T393" id="Seg_9221" s="T392">день.[NOM]</ta>
            <ta e="T394" id="Seg_9222" s="T393">спиртное-VBZ-PRS-3PL</ta>
            <ta e="T395" id="Seg_9223" s="T394">ненецкий</ta>
            <ta e="T396" id="Seg_9224" s="T395">князь-3SG.[NOM]</ta>
            <ta e="T397" id="Seg_9225" s="T396">рассказывать-PRS.[3SG]</ta>
            <ta e="T398" id="Seg_9226" s="T397">нганасанский</ta>
            <ta e="T399" id="Seg_9227" s="T398">человек-3SG.[NOM]</ta>
            <ta e="T400" id="Seg_9228" s="T399">болезнь-ABL</ta>
            <ta e="T401" id="Seg_9229" s="T400">умирать-PTCP.PST</ta>
            <ta e="T402" id="Seg_9230" s="T401">люди-ABL</ta>
            <ta e="T403" id="Seg_9231" s="T402">один</ta>
            <ta e="T404" id="Seg_9232" s="T403">ребенок-ACC</ta>
            <ta e="T405" id="Seg_9233" s="T404">взять-PST2.[3SG]</ta>
            <ta e="T406" id="Seg_9234" s="T405">тот-ACC</ta>
            <ta e="T407" id="Seg_9235" s="T406">болезнь.[NOM]</ta>
            <ta e="T408" id="Seg_9236" s="T407">следовать-FUT.[3SG]</ta>
            <ta e="T409" id="Seg_9237" s="T408">1PL.[NOM]</ta>
            <ta e="T410" id="Seg_9238" s="T409">царь-DAT/LOC</ta>
            <ta e="T411" id="Seg_9239" s="T410">жаловаться-RECP/COLL-IMP.1PL</ta>
            <ta e="T412" id="Seg_9240" s="T411">три-COLL</ta>
            <ta e="T413" id="Seg_9241" s="T412">писать-IMP.1PL</ta>
            <ta e="T414" id="Seg_9242" s="T413">долганский</ta>
            <ta e="T415" id="Seg_9243" s="T414">князь-3SG.[NOM]</ta>
            <ta e="T416" id="Seg_9244" s="T415">думать-VBZ-PST2.[3SG]</ta>
            <ta e="T417" id="Seg_9245" s="T416">говорить-PST2.[3SG]</ta>
            <ta e="T418" id="Seg_9246" s="T417">суд-VBZ-PTCP.PRS-3SG-ACC</ta>
            <ta e="T419" id="Seg_9247" s="T418">суд-VBZ-NEG.CVB</ta>
            <ta e="T420" id="Seg_9248" s="T419">тот.[NOM]</ta>
            <ta e="T421" id="Seg_9249" s="T420">вопреки</ta>
            <ta e="T422" id="Seg_9250" s="T421">подозревать-PTCP.PRS</ta>
            <ta e="T423" id="Seg_9251" s="T422">ужас-3SG.[NOM]</ta>
            <ta e="T424" id="Seg_9252" s="T423">очень</ta>
            <ta e="T425" id="Seg_9253" s="T424">люди-ACC</ta>
            <ta e="T426" id="Seg_9254" s="T425">колоть-PST2.[3SG]</ta>
            <ta e="T427" id="Seg_9255" s="T426">говорить-CVB.SEQ</ta>
            <ta e="T428" id="Seg_9256" s="T427">сирота-ACC</ta>
            <ta e="T429" id="Seg_9257" s="T428">взять-PST2-3SG</ta>
            <ta e="T430" id="Seg_9258" s="T429">что.[NOM]</ta>
            <ta e="T431" id="Seg_9259" s="T430">вина-3SG.[NOM]=Q</ta>
            <ta e="T432" id="Seg_9260" s="T431">суд-DAT/LOC</ta>
            <ta e="T433" id="Seg_9261" s="T432">давать-PTCP.FUT-1PL-ACC</ta>
            <ta e="T434" id="Seg_9262" s="T433">говорить-PST2.[3SG]</ta>
            <ta e="T435" id="Seg_9263" s="T434">ненецкий</ta>
            <ta e="T436" id="Seg_9264" s="T435">князь-3SG.[NOM]</ta>
            <ta e="T437" id="Seg_9265" s="T436">2SG.[NOM]</ta>
            <ta e="T438" id="Seg_9266" s="T437">защищать-PRS-2SG</ta>
            <ta e="T439" id="Seg_9267" s="T438">Q</ta>
            <ta e="T440" id="Seg_9268" s="T439">тот</ta>
            <ta e="T441" id="Seg_9269" s="T440">человек-ACC</ta>
            <ta e="T442" id="Seg_9270" s="T441">тогда</ta>
            <ta e="T443" id="Seg_9271" s="T442">1PL.[NOM]</ta>
            <ta e="T444" id="Seg_9272" s="T443">2SG-ACC</ta>
            <ta e="T445" id="Seg_9273" s="T444">колоть-PRS-1PL</ta>
            <ta e="T446" id="Seg_9274" s="T445">богатство-2SG-ACC</ta>
            <ta e="T447" id="Seg_9275" s="T446">взять-FUT-1PL</ta>
            <ta e="T448" id="Seg_9276" s="T447">говорить-PRS.[3SG]</ta>
            <ta e="T449" id="Seg_9277" s="T448">долганский</ta>
            <ta e="T450" id="Seg_9278" s="T449">князь-3SG.[NOM]</ta>
            <ta e="T451" id="Seg_9279" s="T450">испугаться-PST2.[3SG]</ta>
            <ta e="T452" id="Seg_9280" s="T451">убить-PTCP.FUT-AG.[NOM]</ta>
            <ta e="T453" id="Seg_9281" s="T452">люди.[NOM]</ta>
            <ta e="T454" id="Seg_9282" s="T453">два-COLL.[NOM]</ta>
            <ta e="T455" id="Seg_9283" s="T454">вот</ta>
            <ta e="T456" id="Seg_9284" s="T455">как</ta>
            <ta e="T457" id="Seg_9285" s="T456">советовать-PRS-2PL</ta>
            <ta e="T458" id="Seg_9286" s="T457">говорить-PST2.[3SG]</ta>
            <ta e="T459" id="Seg_9287" s="T458">ненецкий</ta>
            <ta e="T460" id="Seg_9288" s="T459">князь-3SG.[NOM]</ta>
            <ta e="T461" id="Seg_9289" s="T460">1SG.[NOM]</ta>
            <ta e="T462" id="Seg_9290" s="T461">господин.[NOM]</ta>
            <ta e="T463" id="Seg_9291" s="T462">руководитель.[NOM]</ta>
            <ta e="T464" id="Seg_9292" s="T463">быть-PRS-1SG</ta>
            <ta e="T465" id="Seg_9293" s="T464">жаловаться-RECP/COLL-IMP.1PL</ta>
            <ta e="T466" id="Seg_9294" s="T465">царь-DAT/LOC</ta>
            <ta e="T467" id="Seg_9295" s="T466">этот</ta>
            <ta e="T468" id="Seg_9296" s="T467">смелый.[NOM]</ta>
            <ta e="T469" id="Seg_9297" s="T468">нганасанский</ta>
            <ta e="T470" id="Seg_9298" s="T469">стойбище-3SG-ACC</ta>
            <ta e="T471" id="Seg_9299" s="T470">колоть-CVB.SEQ</ta>
            <ta e="T472" id="Seg_9300" s="T471">после</ta>
            <ta e="T473" id="Seg_9301" s="T472">ребенок-ACC</ta>
            <ta e="T474" id="Seg_9302" s="T473">взять-PST2.[3SG]</ta>
            <ta e="T475" id="Seg_9303" s="T474">говорить-CVB.SEQ</ta>
            <ta e="T476" id="Seg_9304" s="T475">писать-FUT-1PL</ta>
            <ta e="T477" id="Seg_9305" s="T476">1SG.[NOM]</ta>
            <ta e="T478" id="Seg_9306" s="T477">человек-PL-EP-1SG.[NOM]</ta>
            <ta e="T479" id="Seg_9307" s="T478">идти-CVB.SEQ</ta>
            <ta e="T480" id="Seg_9308" s="T479">умирать-PTCP.PST</ta>
            <ta e="T481" id="Seg_9309" s="T480">люди-ACC</ta>
            <ta e="T482" id="Seg_9310" s="T481">стрелять-FREQ-FUT-3PL</ta>
            <ta e="T483" id="Seg_9311" s="T482">комиссия.[NOM]</ta>
            <ta e="T484" id="Seg_9312" s="T483">приходить-TEMP-3SG</ta>
            <ta e="T485" id="Seg_9313" s="T484">найти-CVB.SEQ</ta>
            <ta e="T486" id="Seg_9314" s="T485">взять-FUT.[3SG]</ta>
            <ta e="T487" id="Seg_9315" s="T486">NEG-3SG</ta>
            <ta e="T488" id="Seg_9316" s="T487">кто.[NOM]</ta>
            <ta e="T489" id="Seg_9317" s="T488">узнавать-FUT.[3SG]=Q</ta>
            <ta e="T490" id="Seg_9318" s="T489">говорить-PRS.[3SG]</ta>
            <ta e="T491" id="Seg_9319" s="T490">десять-APRX</ta>
            <ta e="T492" id="Seg_9320" s="T491">человек.[NOM]</ta>
            <ta e="T493" id="Seg_9321" s="T492">идти-CVB.SEQ</ta>
            <ta e="T494" id="Seg_9322" s="T493">давно</ta>
            <ta e="T495" id="Seg_9323" s="T494">умирать-PTCP.PST</ta>
            <ta e="T496" id="Seg_9324" s="T495">люди-ACC</ta>
            <ta e="T497" id="Seg_9325" s="T496">стрелять-FREQ-PRS-3PL</ta>
            <ta e="T498" id="Seg_9326" s="T497">писать-CVB.SEQ</ta>
            <ta e="T499" id="Seg_9327" s="T498">бросать-PST1-3PL</ta>
            <ta e="T500" id="Seg_9328" s="T499">так</ta>
            <ta e="T501" id="Seg_9329" s="T500">нганасанский</ta>
            <ta e="T502" id="Seg_9330" s="T501">князь-3SG-ACC</ta>
            <ta e="T503" id="Seg_9331" s="T502">послать-PRS-3PL</ta>
            <ta e="T504" id="Seg_9332" s="T503">ненецкий</ta>
            <ta e="T505" id="Seg_9333" s="T504">князь-3SG-COM</ta>
            <ta e="T506" id="Seg_9334" s="T505">царь.[NOM]</ta>
            <ta e="T507" id="Seg_9335" s="T506">читать-CVB.SEQ</ta>
            <ta e="T508" id="Seg_9336" s="T507">видеть-PRS.[3SG]</ta>
            <ta e="T509" id="Seg_9337" s="T508">десять-APRX</ta>
            <ta e="T510" id="Seg_9338" s="T509">чум.[NOM]</ta>
            <ta e="T511" id="Seg_9339" s="T510">люди-ACC</ta>
            <ta e="T512" id="Seg_9340" s="T511">колоть-PTCP.PST</ta>
            <ta e="T513" id="Seg_9341" s="T512">ребенок-ACC</ta>
            <ta e="T514" id="Seg_9342" s="T513">красть-PTCP.PST</ta>
            <ta e="T515" id="Seg_9343" s="T514">человек.[NOM]</ta>
            <ta e="T516" id="Seg_9344" s="T515">царь.[NOM]</ta>
            <ta e="T517" id="Seg_9345" s="T516">каждый-3PL-ABL</ta>
            <ta e="T518" id="Seg_9346" s="T517">спрашивать-PRS.[3SG]</ta>
            <ta e="T519" id="Seg_9347" s="T518">правда.[NOM]</ta>
            <ta e="T520" id="Seg_9348" s="T519">Q</ta>
            <ta e="T521" id="Seg_9349" s="T520">1SG.[NOM]</ta>
            <ta e="T522" id="Seg_9350" s="T521">3SG-ACC</ta>
            <ta e="T523" id="Seg_9351" s="T522">суд-VBZ-FUT-1SG</ta>
            <ta e="T524" id="Seg_9352" s="T523">2PL.[NOM]</ta>
            <ta e="T525" id="Seg_9353" s="T524">слышать-PTCP.PRS-2PL-DAT/LOC</ta>
            <ta e="T526" id="Seg_9354" s="T525">нганасанский</ta>
            <ta e="T527" id="Seg_9355" s="T526">князь-3SG.[NOM]</ta>
            <ta e="T528" id="Seg_9356" s="T527">человек-DAT/LOC</ta>
            <ta e="T529" id="Seg_9357" s="T528">приближаться-NEG.PTCP</ta>
            <ta e="T530" id="Seg_9358" s="T529">человек.[NOM]</ta>
            <ta e="T531" id="Seg_9359" s="T530">делать-PST1-3SG</ta>
            <ta e="T532" id="Seg_9360" s="T531">наверное</ta>
            <ta e="T533" id="Seg_9361" s="T532">говорить-PRS.[3SG]</ta>
            <ta e="T534" id="Seg_9362" s="T533">старик-DAT/LOC</ta>
            <ta e="T535" id="Seg_9363" s="T534">обман.[NOM]</ta>
            <ta e="T536" id="Seg_9364" s="T535">письмо.[NOM]</ta>
            <ta e="T537" id="Seg_9365" s="T536">послать-PRS-3PL</ta>
            <ta e="T538" id="Seg_9366" s="T537">князь-3SG.[NOM]</ta>
            <ta e="T539" id="Seg_9367" s="T538">вот</ta>
            <ta e="T540" id="Seg_9368" s="T539">налог-3SG-ACC</ta>
            <ta e="T541" id="Seg_9369" s="T540">деньги-3SG-ACC</ta>
            <ta e="T542" id="Seg_9370" s="T541">взять-CVB.SIM</ta>
            <ta e="T543" id="Seg_9371" s="T542">приходить-INCH-IMP.3SG</ta>
            <ta e="T544" id="Seg_9372" s="T543">очень</ta>
            <ta e="T545" id="Seg_9373" s="T544">самый</ta>
            <ta e="T546" id="Seg_9374" s="T545">быстрый-ADVZ</ta>
            <ta e="T547" id="Seg_9375" s="T546">говорить-CVB.SEQ</ta>
            <ta e="T548" id="Seg_9376" s="T547">писать-PRS.[3SG]</ta>
            <ta e="T549" id="Seg_9377" s="T548">вестник.[NOM]</ta>
            <ta e="T550" id="Seg_9378" s="T549">человек.[NOM]</ta>
            <ta e="T551" id="Seg_9379" s="T550">довезти-PRS.[3SG]</ta>
            <ta e="T552" id="Seg_9380" s="T551">письмо-ACC</ta>
            <ta e="T553" id="Seg_9381" s="T552">старик.[NOM]</ta>
            <ta e="T554" id="Seg_9382" s="T553">самый</ta>
            <ta e="T555" id="Seg_9383" s="T554">добрый</ta>
            <ta e="T556" id="Seg_9384" s="T555">олень-PL-3SG-INSTR</ta>
            <ta e="T557" id="Seg_9385" s="T556">мчаться-CVB.SEQ</ta>
            <ta e="T558" id="Seg_9386" s="T557">приходить-PRS.[3SG]</ta>
            <ta e="T559" id="Seg_9387" s="T558">олень-3SG-ACC</ta>
            <ta e="T560" id="Seg_9388" s="T559">связывать-PST2.[3SG]</ta>
            <ta e="T561" id="Seg_9389" s="T560">да</ta>
            <ta e="T562" id="Seg_9390" s="T561">бежать-CVB.SEQ</ta>
            <ta e="T563" id="Seg_9391" s="T562">падать-PRS.[3SG]</ta>
            <ta e="T564" id="Seg_9392" s="T563">стол-ACC</ta>
            <ta e="T565" id="Seg_9393" s="T564">вокруг</ta>
            <ta e="T566" id="Seg_9394" s="T565">господин.[NOM]</ta>
            <ta e="T567" id="Seg_9395" s="T566">очень.много</ta>
            <ta e="T568" id="Seg_9396" s="T567">царь.[NOM]</ta>
            <ta e="T569" id="Seg_9397" s="T568">чай.[NOM]</ta>
            <ta e="T570" id="Seg_9398" s="T569">NEG</ta>
            <ta e="T571" id="Seg_9399" s="T570">пить-EP-CAUS.[CAUS]-PST2.NEG.[3SG]</ta>
            <ta e="T572" id="Seg_9400" s="T571">кровь-3SG.[NOM]</ta>
            <ta e="T573" id="Seg_9401" s="T572">плохой.[NOM]</ta>
            <ta e="T574" id="Seg_9402" s="T573">совсем</ta>
            <ta e="T575" id="Seg_9403" s="T574">другой.[NOM]</ta>
            <ta e="T576" id="Seg_9404" s="T575">вот</ta>
            <ta e="T577" id="Seg_9405" s="T576">рассказывать.[IMP.2SG]</ta>
            <ta e="T578" id="Seg_9406" s="T577">новость-2SG-ACC</ta>
            <ta e="T579" id="Seg_9407" s="T578">стойбище.[NOM]</ta>
            <ta e="T580" id="Seg_9408" s="T579">люди-ACC</ta>
            <ta e="T581" id="Seg_9409" s="T580">почему</ta>
            <ta e="T582" id="Seg_9410" s="T581">истреблять-PST1-2SG</ta>
            <ta e="T583" id="Seg_9411" s="T582">что</ta>
            <ta e="T584" id="Seg_9412" s="T583">стойбище-3SG-ACC</ta>
            <ta e="T585" id="Seg_9413" s="T584">покрывать-CAUS-PRS-2SG</ta>
            <ta e="T586" id="Seg_9414" s="T585">человек-ACC</ta>
            <ta e="T587" id="Seg_9415" s="T586">с</ta>
            <ta e="T588" id="Seg_9416" s="T587">спор-EP-1SG.[NOM]</ta>
            <ta e="T589" id="Seg_9417" s="T588">NEG</ta>
            <ta e="T590" id="Seg_9418" s="T589">NEG.EX</ta>
            <ta e="T591" id="Seg_9419" s="T590">царь.[NOM]</ta>
            <ta e="T592" id="Seg_9420" s="T591">князь-PL.[NOM]</ta>
            <ta e="T593" id="Seg_9421" s="T592">письмо-3PL-ACC</ta>
            <ta e="T594" id="Seg_9422" s="T593">видеть-CAUS-PRS.[3SG]</ta>
            <ta e="T595" id="Seg_9423" s="T594">будущий.год-ADJZ</ta>
            <ta e="T596" id="Seg_9424" s="T595">вода.[NOM]</ta>
            <ta e="T597" id="Seg_9425" s="T596">приходить-PTCP.FUT.[3SG]-DAT/LOC</ta>
            <ta e="T598" id="Seg_9426" s="T597">пока</ta>
            <ta e="T599" id="Seg_9427" s="T598">тюрьма-DAT/LOC</ta>
            <ta e="T600" id="Seg_9428" s="T599">лежать-FUT-2SG</ta>
            <ta e="T601" id="Seg_9429" s="T600">1SG-COMP</ta>
            <ta e="T602" id="Seg_9430" s="T601">большой</ta>
            <ta e="T603" id="Seg_9431" s="T602">царь-DAT/LOC</ta>
            <ta e="T604" id="Seg_9432" s="T603">послать-FUT-1SG</ta>
            <ta e="T605" id="Seg_9433" s="T604">старик-ACC</ta>
            <ta e="T606" id="Seg_9434" s="T605">тюрьма-DAT/LOC</ta>
            <ta e="T607" id="Seg_9435" s="T606">заложить-CVB.SEQ</ta>
            <ta e="T608" id="Seg_9436" s="T607">бросать-PRS-3PL</ta>
            <ta e="T609" id="Seg_9437" s="T608">три</ta>
            <ta e="T610" id="Seg_9438" s="T609">князь.[NOM]</ta>
            <ta e="T611" id="Seg_9439" s="T610">богатство-3SG-ACC</ta>
            <ta e="T612" id="Seg_9440" s="T611">разделить-FUT-3PL</ta>
            <ta e="T613" id="Seg_9441" s="T612">старуха.[NOM]</ta>
            <ta e="T614" id="Seg_9442" s="T613">кочевать-CVB.SEQ</ta>
            <ta e="T615" id="Seg_9443" s="T614">приходить-PRS.[3SG]</ta>
            <ta e="T616" id="Seg_9444" s="T615">старик-3SG.[NOM]</ta>
            <ta e="T617" id="Seg_9445" s="T616">когда</ta>
            <ta e="T618" id="Seg_9446" s="T617">NEG</ta>
            <ta e="T619" id="Seg_9447" s="T618">NEG.EX</ta>
            <ta e="T620" id="Seg_9448" s="T619">тюрьма-DAT/LOC</ta>
            <ta e="T621" id="Seg_9449" s="T620">лежать-PRS.[3SG]</ta>
            <ta e="T622" id="Seg_9450" s="T621">царь.[NOM]</ta>
            <ta e="T623" id="Seg_9451" s="T622">старуха-ACC</ta>
            <ta e="T624" id="Seg_9452" s="T623">гнать-CVB.SEQ</ta>
            <ta e="T625" id="Seg_9453" s="T624">вынимать-PRS.[3SG]</ta>
            <ta e="T626" id="Seg_9454" s="T625">три</ta>
            <ta e="T627" id="Seg_9455" s="T626">князь.[NOM]</ta>
            <ta e="T628" id="Seg_9456" s="T627">богатство-3SG-ACC</ta>
            <ta e="T629" id="Seg_9457" s="T628">покрывать-CVB.SIM</ta>
            <ta e="T630" id="Seg_9458" s="T629">падать-CVB.SEQ</ta>
            <ta e="T631" id="Seg_9459" s="T630">разделить-CVB.SEQ</ta>
            <ta e="T632" id="Seg_9460" s="T631">взять-PRS-3PL</ta>
            <ta e="T633" id="Seg_9461" s="T632">один</ta>
            <ta e="T634" id="Seg_9462" s="T633">кожаная.покрышка.чума-ACC</ta>
            <ta e="T635" id="Seg_9463" s="T634">оставаться-CAUS-PRS-3PL</ta>
            <ta e="T636" id="Seg_9464" s="T635">зима-3SG.[NOM]</ta>
            <ta e="T637" id="Seg_9465" s="T636">кончаться-CVB.SEQ</ta>
            <ta e="T638" id="Seg_9466" s="T637">пароход.[NOM]</ta>
            <ta e="T639" id="Seg_9467" s="T638">приходить-EMOT-PST2.[3SG]</ta>
            <ta e="T640" id="Seg_9468" s="T639">наверху-ABL</ta>
            <ta e="T641" id="Seg_9469" s="T640">вот</ta>
            <ta e="T642" id="Seg_9470" s="T641">видеть.[IMP.2SG]</ta>
            <ta e="T643" id="Seg_9471" s="T642">муж-2SG-ACC</ta>
            <ta e="T644" id="Seg_9472" s="T643">говорить-PRS-3PL</ta>
            <ta e="T645" id="Seg_9473" s="T644">старик.[NOM]</ta>
            <ta e="T646" id="Seg_9474" s="T645">глаз-3SG.[NOM]</ta>
            <ta e="T647" id="Seg_9475" s="T646">яма.[NOM]</ta>
            <ta e="T648" id="Seg_9476" s="T647">нутро-3SG-DAT/LOC</ta>
            <ta e="T649" id="Seg_9477" s="T648">входить-PST2.[3SG]</ta>
            <ta e="T650" id="Seg_9478" s="T649">есть-EP-CAUS-PST2.NEG-3PL</ta>
            <ta e="T651" id="Seg_9479" s="T650">голодать-CVB.SEQ</ta>
            <ta e="T652" id="Seg_9480" s="T651">оставаться-PST2.[3SG]</ta>
            <ta e="T653" id="Seg_9481" s="T652">пароход.[NOM]</ta>
            <ta e="T654" id="Seg_9482" s="T653">нутро-3SG-DAT/LOC</ta>
            <ta e="T655" id="Seg_9483" s="T654">бросать-PST2-3PL</ta>
            <ta e="T656" id="Seg_9484" s="T655">следующий</ta>
            <ta e="T657" id="Seg_9485" s="T656">царь-DAT/LOC</ta>
            <ta e="T658" id="Seg_9486" s="T657">носить-PRS-3PL</ta>
            <ta e="T659" id="Seg_9487" s="T658">носилка-INSTR</ta>
            <ta e="T660" id="Seg_9488" s="T659">внести-PRS-3PL</ta>
            <ta e="T661" id="Seg_9489" s="T660">умирать-CVB.SEQ</ta>
            <ta e="T662" id="Seg_9490" s="T661">быть-PTCP.PRS</ta>
            <ta e="T663" id="Seg_9491" s="T662">человек-ACC</ta>
            <ta e="T664" id="Seg_9492" s="T663">эй</ta>
            <ta e="T665" id="Seg_9493" s="T664">как</ta>
            <ta e="T666" id="Seg_9494" s="T665">суд-VBZ-FUT-1SG=Q</ta>
            <ta e="T667" id="Seg_9495" s="T666">умирать-PTCP.PST</ta>
            <ta e="T668" id="Seg_9496" s="T667">человек-ACC</ta>
            <ta e="T669" id="Seg_9497" s="T668">носить-EP-IMP.2PL</ta>
            <ta e="T670" id="Seg_9498" s="T669">больница-DAT/LOC</ta>
            <ta e="T671" id="Seg_9499" s="T670">есть-EP-CAUS-IMP.3PL</ta>
            <ta e="T672" id="Seg_9500" s="T671">видеть-IMP.3PL</ta>
            <ta e="T673" id="Seg_9501" s="T672">доктор-PL.[NOM]</ta>
            <ta e="T674" id="Seg_9502" s="T673">лечить-PRS-3PL</ta>
            <ta e="T675" id="Seg_9503" s="T674">старый</ta>
            <ta e="T676" id="Seg_9504" s="T675">сам-3SG.[NOM]</ta>
            <ta e="T677" id="Seg_9505" s="T676">быть-PRS.[3SG]</ta>
            <ta e="T678" id="Seg_9506" s="T677">доктор-PL.[NOM]</ta>
            <ta e="T679" id="Seg_9507" s="T678">спрашивать-PRS-3PL</ta>
            <ta e="T680" id="Seg_9508" s="T679">вина-EP-2SG.[NOM]</ta>
            <ta e="T681" id="Seg_9509" s="T680">Q</ta>
            <ta e="T682" id="Seg_9510" s="T681">подозревать-PRS-3PL</ta>
            <ta e="T683" id="Seg_9511" s="T682">Q</ta>
            <ta e="T684" id="Seg_9512" s="T683">старик.[NOM]</ta>
            <ta e="T685" id="Seg_9513" s="T684">рассказывать-PRS.[3SG]</ta>
            <ta e="T686" id="Seg_9514" s="T685">каждый-3SG-ACC</ta>
            <ta e="T687" id="Seg_9515" s="T686">доктор-PL.[NOM]</ta>
            <ta e="T688" id="Seg_9516" s="T687">кровь.[NOM]</ta>
            <ta e="T689" id="Seg_9517" s="T688">взять-PRS-3PL</ta>
            <ta e="T690" id="Seg_9518" s="T689">вина-PROPR.[NOM]</ta>
            <ta e="T691" id="Seg_9519" s="T690">быть-TEMP-2SG</ta>
            <ta e="T692" id="Seg_9520" s="T691">кровь-EP-2SG.[NOM]</ta>
            <ta e="T693" id="Seg_9521" s="T692">другой.[NOM]</ta>
            <ta e="T694" id="Seg_9522" s="T693">быть-FUT.[3SG]</ta>
            <ta e="T695" id="Seg_9523" s="T694">говорить-PRS-3PL</ta>
            <ta e="T696" id="Seg_9524" s="T695">один-INTNS.[NOM]</ta>
            <ta e="T697" id="Seg_9525" s="T696">NEG</ta>
            <ta e="T698" id="Seg_9526" s="T697">капелька.[NOM]</ta>
            <ta e="T699" id="Seg_9527" s="T698">вина-3SG.[NOM]</ta>
            <ta e="T700" id="Seg_9528" s="T699">NEG.EX</ta>
            <ta e="T701" id="Seg_9529" s="T700">быть-PST2.[3SG]</ta>
            <ta e="T702" id="Seg_9530" s="T701">письмо.[NOM]</ta>
            <ta e="T703" id="Seg_9531" s="T702">давать-PRS-3PL</ta>
            <ta e="T704" id="Seg_9532" s="T703">этот-ACC</ta>
            <ta e="T705" id="Seg_9533" s="T704">суд-DAT/LOC</ta>
            <ta e="T706" id="Seg_9534" s="T705">прямо</ta>
            <ta e="T707" id="Seg_9535" s="T706">давать-FUT.[IMP.2SG]</ta>
            <ta e="T708" id="Seg_9536" s="T707">князь-PL.[NOM]</ta>
            <ta e="T709" id="Seg_9537" s="T708">кровь-3PL-ACC</ta>
            <ta e="T710" id="Seg_9538" s="T709">тоже</ta>
            <ta e="T711" id="Seg_9539" s="T710">взять-FUT-1PL</ta>
            <ta e="T712" id="Seg_9540" s="T711">говорить-PRS-3PL</ta>
            <ta e="T713" id="Seg_9541" s="T712">суд.[NOM]</ta>
            <ta e="T714" id="Seg_9542" s="T713">быть-PRS.[3SG]</ta>
            <ta e="T715" id="Seg_9543" s="T714">князь-PL.[NOM]</ta>
            <ta e="T716" id="Seg_9544" s="T715">первый</ta>
            <ta e="T717" id="Seg_9545" s="T716">царь.[NOM]</ta>
            <ta e="T718" id="Seg_9546" s="T717">много</ta>
            <ta e="T719" id="Seg_9547" s="T718">люди.[NOM]</ta>
            <ta e="T720" id="Seg_9548" s="T719">собираться-REFL-EP-PST2.[3SG]</ta>
            <ta e="T721" id="Seg_9549" s="T720">царь.[NOM]</ta>
            <ta e="T722" id="Seg_9550" s="T721">говорить-PRS.[3SG]</ta>
            <ta e="T723" id="Seg_9551" s="T722">слово-3SG-ACC-слово-3SG-ACC</ta>
            <ta e="T724" id="Seg_9552" s="T723">слышать-PTCP.FUT-DAT/LOC</ta>
            <ta e="T725" id="Seg_9553" s="T724">говорить-PRS.[3SG]</ta>
            <ta e="T726" id="Seg_9554" s="T725">старик.[NOM]</ta>
            <ta e="T727" id="Seg_9555" s="T726">рассказывать-PRS.[3SG]</ta>
            <ta e="T728" id="Seg_9556" s="T727">1SG.[NOM]</ta>
            <ta e="T729" id="Seg_9557" s="T728">даже</ta>
            <ta e="T730" id="Seg_9558" s="T729">человек-ACC</ta>
            <ta e="T731" id="Seg_9559" s="T730">с</ta>
            <ta e="T732" id="Seg_9560" s="T731">побить-EP-NEG.PTCP.PST</ta>
            <ta e="T733" id="Seg_9561" s="T732">человек-1SG</ta>
            <ta e="T734" id="Seg_9562" s="T733">справка-3SG-ACC</ta>
            <ta e="T735" id="Seg_9563" s="T734">давать-PRS.[3SG]</ta>
            <ta e="T736" id="Seg_9564" s="T735">этот</ta>
            <ta e="T737" id="Seg_9565" s="T736">человек.[NOM]</ta>
            <ta e="T738" id="Seg_9566" s="T737">капелька.[NOM]</ta>
            <ta e="T739" id="Seg_9567" s="T738">NEG</ta>
            <ta e="T740" id="Seg_9568" s="T739">вина-POSS</ta>
            <ta e="T741" id="Seg_9569" s="T740">NEG.[3SG]</ta>
            <ta e="T742" id="Seg_9570" s="T741">князь-PL.[NOM]</ta>
            <ta e="T743" id="Seg_9571" s="T742">кровь-3PL-ACC</ta>
            <ta e="T744" id="Seg_9572" s="T743">взять-PRS-3PL</ta>
            <ta e="T745" id="Seg_9573" s="T744">нганасанский</ta>
            <ta e="T746" id="Seg_9574" s="T745">смелый-3SG-GEN</ta>
            <ta e="T747" id="Seg_9575" s="T746">кровь-3SG.[NOM]</ta>
            <ta e="T748" id="Seg_9576" s="T747">вправду</ta>
            <ta e="T749" id="Seg_9577" s="T748">другой.[NOM]</ta>
            <ta e="T750" id="Seg_9578" s="T749">князь-PL.[NOM]</ta>
            <ta e="T751" id="Seg_9579" s="T750">собственный-3PL.[NOM]</ta>
            <ta e="T752" id="Seg_9580" s="T751">совсем</ta>
            <ta e="T753" id="Seg_9581" s="T752">другой.[NOM]</ta>
            <ta e="T754" id="Seg_9582" s="T753">царь.[NOM]</ta>
            <ta e="T755" id="Seg_9583" s="T754">вот</ta>
            <ta e="T756" id="Seg_9584" s="T755">разгневаться-PRS.[3SG]</ta>
            <ta e="T757" id="Seg_9585" s="T756">кто.[NOM]</ta>
            <ta e="T758" id="Seg_9586" s="T757">советовать-PST2-3SG=Q</ta>
            <ta e="T759" id="Seg_9587" s="T758">рассказывать-IMP.2PL</ta>
            <ta e="T760" id="Seg_9588" s="T759">долганский</ta>
            <ta e="T761" id="Seg_9589" s="T760">князь-3SG.[NOM]</ta>
            <ta e="T762" id="Seg_9590" s="T761">рассказывать-PRS.[3SG]</ta>
            <ta e="T763" id="Seg_9591" s="T762">вот</ta>
            <ta e="T764" id="Seg_9592" s="T763">правда-3SG.[NOM]</ta>
            <ta e="T765" id="Seg_9593" s="T764">1PL.[NOM]</ta>
            <ta e="T766" id="Seg_9594" s="T765">вина-PROPR-1PL</ta>
            <ta e="T767" id="Seg_9595" s="T766">самый</ta>
            <ta e="T768" id="Seg_9596" s="T767">большой</ta>
            <ta e="T769" id="Seg_9597" s="T768">вина-PROPR.[NOM]</ta>
            <ta e="T770" id="Seg_9598" s="T769">ненецкий</ta>
            <ta e="T771" id="Seg_9599" s="T770">князь-3SG.[NOM]</ta>
            <ta e="T772" id="Seg_9600" s="T771">1SG-ACC</ta>
            <ta e="T773" id="Seg_9601" s="T772">колоть-PTCP.FUT</ta>
            <ta e="T774" id="Seg_9602" s="T773">есть-3PL</ta>
            <ta e="T775" id="Seg_9603" s="T774">испугаться-CVB.SEQ</ta>
            <ta e="T777" id="Seg_9604" s="T776">давать-PST2-EP-1SG</ta>
            <ta e="T778" id="Seg_9605" s="T777">царь.[NOM]</ta>
            <ta e="T779" id="Seg_9606" s="T778">вот</ta>
            <ta e="T780" id="Seg_9607" s="T779">старик.[NOM]</ta>
            <ta e="T781" id="Seg_9608" s="T780">2SG.[NOM]</ta>
            <ta e="T782" id="Seg_9609" s="T781">вина-EP-2SG.[NOM]</ta>
            <ta e="T783" id="Seg_9610" s="T782">NEG.EX</ta>
            <ta e="T784" id="Seg_9611" s="T783">быть-PST2.[3SG]</ta>
            <ta e="T785" id="Seg_9612" s="T784">тюрьма-DAT/LOC</ta>
            <ta e="T786" id="Seg_9613" s="T785">2SG-ACC</ta>
            <ta e="T787" id="Seg_9614" s="T786">суд-VBZ-PTCP.PST</ta>
            <ta e="T788" id="Seg_9615" s="T787">царь-ACC</ta>
            <ta e="T789" id="Seg_9616" s="T788">посадить-FUT-1SG</ta>
            <ta e="T790" id="Seg_9617" s="T789">2SG.[NOM]</ta>
            <ta e="T791" id="Seg_9618" s="T790">3SG.[NOM]</ta>
            <ta e="T792" id="Seg_9619" s="T791">место-DAT/LOC</ta>
            <ta e="T793" id="Seg_9620" s="T792">царь-INSTR</ta>
            <ta e="T794" id="Seg_9621" s="T793">идти-FUT-2SG</ta>
            <ta e="T795" id="Seg_9622" s="T794">князь-PL-ACC</ta>
            <ta e="T796" id="Seg_9623" s="T795">там</ta>
            <ta e="T797" id="Seg_9624" s="T796">доезжать-CVB.SEQ</ta>
            <ta e="T798" id="Seg_9625" s="T797">суд-VBZ-FUT.[IMP.2SG]</ta>
            <ta e="T799" id="Seg_9626" s="T798">говорить-PST2.[3SG]</ta>
            <ta e="T800" id="Seg_9627" s="T799">отправляться-CVB.SEQ</ta>
            <ta e="T801" id="Seg_9628" s="T800">оставаться-PRS-3PL</ta>
            <ta e="T802" id="Seg_9629" s="T801">назад</ta>
            <ta e="T803" id="Seg_9630" s="T802">старик.[NOM]</ta>
            <ta e="T804" id="Seg_9631" s="T803">стойбище-3SG-DAT/LOC</ta>
            <ta e="T805" id="Seg_9632" s="T804">приходить-CVB.SEQ</ta>
            <ta e="T806" id="Seg_9633" s="T805">князь.[NOM]</ta>
            <ta e="T807" id="Seg_9634" s="T806">становиться-PRS.[3SG]</ta>
            <ta e="T808" id="Seg_9635" s="T807">сын-3SG.[NOM]</ta>
            <ta e="T809" id="Seg_9636" s="T808">расти-CVB.SEQ</ta>
            <ta e="T810" id="Seg_9637" s="T809">прямо-прямо</ta>
            <ta e="T811" id="Seg_9638" s="T810">богатырь.[NOM]</ta>
            <ta e="T812" id="Seg_9639" s="T811">становиться-PST2.[3SG]</ta>
            <ta e="T813" id="Seg_9640" s="T812">3SG.[NOM]</ta>
            <ta e="T814" id="Seg_9641" s="T813">нет-3SG</ta>
            <ta e="T815" id="Seg_9642" s="T814">убить-CVB.PURP</ta>
            <ta e="T816" id="Seg_9643" s="T815">хотеть-PST2-3PL</ta>
            <ta e="T817" id="Seg_9644" s="T816">отец-3SG-DAT/LOC</ta>
            <ta e="T818" id="Seg_9645" s="T817">подобно</ta>
            <ta e="T819" id="Seg_9646" s="T818">быть-FUT.[3SG]</ta>
            <ta e="T820" id="Seg_9647" s="T819">думать-CVB.SEQ</ta>
            <ta e="T821" id="Seg_9648" s="T820">мальчик.[NOM]</ta>
            <ta e="T822" id="Seg_9649" s="T821">один</ta>
            <ta e="T823" id="Seg_9650" s="T822">человек-ACC</ta>
            <ta e="T824" id="Seg_9651" s="T823">опрокинуть-ADVZ</ta>
            <ta e="T825" id="Seg_9652" s="T824">бить-EP-PST2.[3SG]</ta>
            <ta e="T826" id="Seg_9653" s="T825">тот-ACC</ta>
            <ta e="T827" id="Seg_9654" s="T826">тюрьма-DAT/LOC</ta>
            <ta e="T828" id="Seg_9655" s="T827">посадить-PST2-3PL</ta>
            <ta e="T829" id="Seg_9656" s="T828">князь-3PL-ACC</ta>
            <ta e="T830" id="Seg_9657" s="T829">судить-PRS.[3SG]</ta>
            <ta e="T831" id="Seg_9658" s="T830">нганасанский</ta>
            <ta e="T832" id="Seg_9659" s="T831">князь-3SG-ACC</ta>
            <ta e="T833" id="Seg_9660" s="T832">ненецкий</ta>
            <ta e="T834" id="Seg_9661" s="T833">князь-3SG-COM</ta>
            <ta e="T835" id="Seg_9662" s="T834">тюрьма-DAT/LOC</ta>
            <ta e="T836" id="Seg_9663" s="T835">посадить-PRS.[3SG]</ta>
            <ta e="T837" id="Seg_9664" s="T836">долганский</ta>
            <ta e="T838" id="Seg_9665" s="T837">князь-3SG-ACC</ta>
            <ta e="T839" id="Seg_9666" s="T838">князь-3SG-ABL</ta>
            <ta e="T840" id="Seg_9667" s="T839">снимать-PRS.[3SG]</ta>
            <ta e="T841" id="Seg_9668" s="T840">сын-3SG-ACC</ta>
            <ta e="T842" id="Seg_9669" s="T841">нганасанский</ta>
            <ta e="T843" id="Seg_9670" s="T842">князь-3SG.[NOM]</ta>
            <ta e="T844" id="Seg_9671" s="T843">делать-PRS.[3SG]</ta>
            <ta e="T845" id="Seg_9672" s="T844">старик.[NOM]</ta>
            <ta e="T846" id="Seg_9673" s="T845">такой</ta>
            <ta e="T847" id="Seg_9674" s="T846">правда-3SG.[NOM]</ta>
            <ta e="T848" id="Seg_9675" s="T847">выйти-EP-PST2-3SG</ta>
            <ta e="T849" id="Seg_9676" s="T848">последний-3SG.[NOM]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_9677" s="T0">n-n&gt;adj-n:(num)-n:case</ta>
            <ta e="T2" id="Seg_9678" s="T1">n-n&gt;adj-n:(num)-n:case</ta>
            <ta e="T3" id="Seg_9679" s="T2">n-n&gt;adj-n:(num)-n:case</ta>
            <ta e="T4" id="Seg_9680" s="T3">v-v:mood-v:poss.pn</ta>
            <ta e="T5" id="Seg_9681" s="T4">dempro</ta>
            <ta e="T6" id="Seg_9682" s="T5">n-n:case</ta>
            <ta e="T7" id="Seg_9683" s="T6">adv</ta>
            <ta e="T8" id="Seg_9684" s="T7">v-v:tense-v:pred.pn</ta>
            <ta e="T9" id="Seg_9685" s="T8">adj-n:case</ta>
            <ta e="T10" id="Seg_9686" s="T9">ptcl</ta>
            <ta e="T11" id="Seg_9687" s="T10">adj-adj&gt;adj-n:case</ta>
            <ta e="T12" id="Seg_9688" s="T11">conj</ta>
            <ta e="T13" id="Seg_9689" s="T12">n-n:(poss)</ta>
            <ta e="T14" id="Seg_9690" s="T13">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T15" id="Seg_9691" s="T14">n-n:case</ta>
            <ta e="T16" id="Seg_9692" s="T15">adv</ta>
            <ta e="T17" id="Seg_9693" s="T16">v-v:tense-v:pred.pn</ta>
            <ta e="T18" id="Seg_9694" s="T17">n-n:poss-n:case</ta>
            <ta e="T19" id="Seg_9695" s="T18">n-n:case</ta>
            <ta e="T20" id="Seg_9696" s="T19">n-n:poss-n:case</ta>
            <ta e="T21" id="Seg_9697" s="T20">adj</ta>
            <ta e="T22" id="Seg_9698" s="T21">n-n:case</ta>
            <ta e="T23" id="Seg_9699" s="T22">v-v:tense-v:pred.pn</ta>
            <ta e="T24" id="Seg_9700" s="T23">n-n:case</ta>
            <ta e="T25" id="Seg_9701" s="T24">n-n:poss-n:case</ta>
            <ta e="T26" id="Seg_9702" s="T25">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T27" id="Seg_9703" s="T26">adv</ta>
            <ta e="T28" id="Seg_9704" s="T27">v-v:tense-v:pred.pn</ta>
            <ta e="T29" id="Seg_9705" s="T28">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T30" id="Seg_9706" s="T29">n-n:(poss)-n:case</ta>
            <ta e="T31" id="Seg_9707" s="T30">n-n&gt;v-v:cvb</ta>
            <ta e="T32" id="Seg_9708" s="T31">v-v:tense-v:pred.pn</ta>
            <ta e="T33" id="Seg_9709" s="T32">n-n:poss-n:case</ta>
            <ta e="T34" id="Seg_9710" s="T33">v-v:tense-v:pred.pn</ta>
            <ta e="T35" id="Seg_9711" s="T34">n-n:(poss)-n:case</ta>
            <ta e="T36" id="Seg_9712" s="T35">v-v:tense-v:poss.pn</ta>
            <ta e="T37" id="Seg_9713" s="T36">ptcl</ta>
            <ta e="T38" id="Seg_9714" s="T37">n-n:case</ta>
            <ta e="T39" id="Seg_9715" s="T38">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T40" id="Seg_9716" s="T39">v-v:tense-v:poss.pn</ta>
            <ta e="T41" id="Seg_9717" s="T40">n-n&gt;adj-n:case</ta>
            <ta e="T42" id="Seg_9718" s="T41">cardnum</ta>
            <ta e="T43" id="Seg_9719" s="T42">n-n:case</ta>
            <ta e="T44" id="Seg_9720" s="T43">v-v:mood.pn</ta>
            <ta e="T45" id="Seg_9721" s="T44">cardnum</ta>
            <ta e="T46" id="Seg_9722" s="T45">n-n:case</ta>
            <ta e="T47" id="Seg_9723" s="T46">n-n:case</ta>
            <ta e="T48" id="Seg_9724" s="T47">v-v:cvb</ta>
            <ta e="T49" id="Seg_9725" s="T48">v-v:tense-v:pred.pn</ta>
            <ta e="T50" id="Seg_9726" s="T49">adv&gt;adv-adv</ta>
            <ta e="T51" id="Seg_9727" s="T50">n-n:case</ta>
            <ta e="T52" id="Seg_9728" s="T51">n-n:case</ta>
            <ta e="T53" id="Seg_9729" s="T52">ptcl-n:case</ta>
            <ta e="T54" id="Seg_9730" s="T53">v-v:tense-v:pred.pn</ta>
            <ta e="T55" id="Seg_9731" s="T54">adj</ta>
            <ta e="T56" id="Seg_9732" s="T55">n-n:case</ta>
            <ta e="T57" id="Seg_9733" s="T56">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T58" id="Seg_9734" s="T57">n-n:poss-n:case</ta>
            <ta e="T59" id="Seg_9735" s="T58">n-n:poss-n:case</ta>
            <ta e="T60" id="Seg_9736" s="T59">cardnum</ta>
            <ta e="T61" id="Seg_9737" s="T60">n-n:case</ta>
            <ta e="T62" id="Seg_9738" s="T61">post</ta>
            <ta e="T63" id="Seg_9739" s="T62">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T64" id="Seg_9740" s="T63">dempro</ta>
            <ta e="T65" id="Seg_9741" s="T64">v-v:cvb</ta>
            <ta e="T66" id="Seg_9742" s="T65">n-n:poss-n:case</ta>
            <ta e="T67" id="Seg_9743" s="T66">n-n:poss-n:case</ta>
            <ta e="T68" id="Seg_9744" s="T67">n-n:case</ta>
            <ta e="T69" id="Seg_9745" s="T68">v-v:cvb</ta>
            <ta e="T70" id="Seg_9746" s="T69">v-v:tense-v:pred.pn</ta>
            <ta e="T71" id="Seg_9747" s="T70">n-n:case</ta>
            <ta e="T72" id="Seg_9748" s="T71">post</ta>
            <ta e="T73" id="Seg_9749" s="T72">v-v:tense-v:poss.pn</ta>
            <ta e="T74" id="Seg_9750" s="T73">n-n:case</ta>
            <ta e="T75" id="Seg_9751" s="T74">n-n:case</ta>
            <ta e="T76" id="Seg_9752" s="T75">v-v:tense-v:pred.pn</ta>
            <ta e="T77" id="Seg_9753" s="T76">v-v:ptcp</ta>
            <ta e="T78" id="Seg_9754" s="T77">n-n:case</ta>
            <ta e="T79" id="Seg_9755" s="T78">n-n:poss-n:case</ta>
            <ta e="T80" id="Seg_9756" s="T79">v-v:cvb</ta>
            <ta e="T81" id="Seg_9757" s="T80">v-v:tense-v:pred.pn</ta>
            <ta e="T82" id="Seg_9758" s="T81">n-n:case</ta>
            <ta e="T83" id="Seg_9759" s="T82">n-n:case</ta>
            <ta e="T84" id="Seg_9760" s="T83">adv</ta>
            <ta e="T85" id="Seg_9761" s="T84">que</ta>
            <ta e="T86" id="Seg_9762" s="T85">conj</ta>
            <ta e="T87" id="Seg_9763" s="T86">v-v:tense-v:poss.pn</ta>
            <ta e="T88" id="Seg_9764" s="T87">v-v&gt;v-v:(ins)-v:(neg)-v:pred.pn</ta>
            <ta e="T89" id="Seg_9765" s="T88">v-v:tense-v:pred.pn</ta>
            <ta e="T90" id="Seg_9766" s="T89">pers-pro:case</ta>
            <ta e="T91" id="Seg_9767" s="T90">n-n:(poss)</ta>
            <ta e="T92" id="Seg_9768" s="T91">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T93" id="Seg_9769" s="T92">ptcl</ta>
            <ta e="T94" id="Seg_9770" s="T93">v-v:ptcp-v:(case)</ta>
            <ta e="T95" id="Seg_9771" s="T94">ptcl</ta>
            <ta e="T96" id="Seg_9772" s="T95">v-v:cvb</ta>
            <ta e="T97" id="Seg_9773" s="T96">v-v:tense-v:pred.pn</ta>
            <ta e="T98" id="Seg_9774" s="T97">n-n:(poss)-n:case</ta>
            <ta e="T99" id="Seg_9775" s="T98">n-n:case</ta>
            <ta e="T100" id="Seg_9776" s="T99">n-n:poss-n:case</ta>
            <ta e="T101" id="Seg_9777" s="T100">v-v:tense-v:pred.pn</ta>
            <ta e="T102" id="Seg_9778" s="T101">adj</ta>
            <ta e="T103" id="Seg_9779" s="T102">n-n:case</ta>
            <ta e="T104" id="Seg_9780" s="T103">ptcl</ta>
            <ta e="T105" id="Seg_9781" s="T104">dempro-pro:case</ta>
            <ta e="T106" id="Seg_9782" s="T105">v-v:mood-v:poss.pn</ta>
            <ta e="T107" id="Seg_9783" s="T106">v-v:cvb</ta>
            <ta e="T108" id="Seg_9784" s="T107">v-v:tense-v:pred.pn</ta>
            <ta e="T109" id="Seg_9785" s="T108">n-n:case</ta>
            <ta e="T110" id="Seg_9786" s="T109">n-n:poss-n:case</ta>
            <ta e="T111" id="Seg_9787" s="T110">v-v:cvb</ta>
            <ta e="T112" id="Seg_9788" s="T111">v-v:cvb</ta>
            <ta e="T113" id="Seg_9789" s="T112">n-n:poss-n:case</ta>
            <ta e="T114" id="Seg_9790" s="T113">n-n:poss-n:case</ta>
            <ta e="T115" id="Seg_9791" s="T114">v-v:tense-v:pred.pn</ta>
            <ta e="T116" id="Seg_9792" s="T115">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T117" id="Seg_9793" s="T116">v-v:tense-v:pred.pn</ta>
            <ta e="T118" id="Seg_9794" s="T117">ptcl</ta>
            <ta e="T119" id="Seg_9795" s="T118">ptcl</ta>
            <ta e="T120" id="Seg_9796" s="T119">v-v:mood-v:pred.pn</ta>
            <ta e="T121" id="Seg_9797" s="T120">n-n:case</ta>
            <ta e="T122" id="Seg_9798" s="T121">n-n:(poss)-n:case</ta>
            <ta e="T123" id="Seg_9799" s="T122">v-v:tense-v:pred.pn</ta>
            <ta e="T124" id="Seg_9800" s="T123">v-v:tense-v:pred.pn</ta>
            <ta e="T125" id="Seg_9801" s="T124">v-v:tense-v:poss.pn</ta>
            <ta e="T126" id="Seg_9802" s="T125">adj</ta>
            <ta e="T127" id="Seg_9803" s="T126">n-n:case</ta>
            <ta e="T128" id="Seg_9804" s="T127">n-n:(poss)-n:case</ta>
            <ta e="T129" id="Seg_9805" s="T128">v-v:ptcp</ta>
            <ta e="T130" id="Seg_9806" s="T129">n-n:(poss)-n:case</ta>
            <ta e="T131" id="Seg_9807" s="T130">v-v:tense-v:pred.pn</ta>
            <ta e="T132" id="Seg_9808" s="T131">interj</ta>
            <ta e="T133" id="Seg_9809" s="T132">dempro-pro:(num)-pro:case</ta>
            <ta e="T134" id="Seg_9810" s="T133">v-v:cvb</ta>
            <ta e="T135" id="Seg_9811" s="T134">v-v:tense-v:pred.pn</ta>
            <ta e="T136" id="Seg_9812" s="T135">n-n:case</ta>
            <ta e="T137" id="Seg_9813" s="T136">ptcl</ta>
            <ta e="T138" id="Seg_9814" s="T137">cardnum</ta>
            <ta e="T139" id="Seg_9815" s="T138">n-n:(poss)-n&gt;adj</ta>
            <ta e="T140" id="Seg_9816" s="T139">n-n:poss-n:case</ta>
            <ta e="T141" id="Seg_9817" s="T140">v-v:cvb</ta>
            <ta e="T142" id="Seg_9818" s="T141">v-v:cvb</ta>
            <ta e="T143" id="Seg_9819" s="T142">v-v:tense-v:pred.pn</ta>
            <ta e="T144" id="Seg_9820" s="T143">n-n:poss-n:case</ta>
            <ta e="T145" id="Seg_9821" s="T144">v-v:tense-v:poss.pn</ta>
            <ta e="T146" id="Seg_9822" s="T145">dempro</ta>
            <ta e="T147" id="Seg_9823" s="T146">n-n:(poss)-n:case</ta>
            <ta e="T148" id="Seg_9824" s="T147">n-n:(poss)-n:case</ta>
            <ta e="T149" id="Seg_9825" s="T148">cardnum</ta>
            <ta e="T150" id="Seg_9826" s="T149">n-n:case</ta>
            <ta e="T151" id="Seg_9827" s="T150">v-v:tense-v:poss.pn</ta>
            <ta e="T152" id="Seg_9828" s="T151">v-v:tense-v:pred.pn</ta>
            <ta e="T153" id="Seg_9829" s="T152">v-v:tense-v:poss.pn</ta>
            <ta e="T154" id="Seg_9830" s="T153">n-n:poss-n:case</ta>
            <ta e="T155" id="Seg_9831" s="T154">n-n:poss-n:case</ta>
            <ta e="T156" id="Seg_9832" s="T155">v-v:tense-v:pred.pn</ta>
            <ta e="T157" id="Seg_9833" s="T156">cardnum</ta>
            <ta e="T158" id="Seg_9834" s="T157">n-n:case</ta>
            <ta e="T159" id="Seg_9835" s="T158">post</ta>
            <ta e="T160" id="Seg_9836" s="T159">n-n:poss-n:case</ta>
            <ta e="T161" id="Seg_9837" s="T160">conj</ta>
            <ta e="T162" id="Seg_9838" s="T161">v-v:cvb</ta>
            <ta e="T163" id="Seg_9839" s="T162">dempro</ta>
            <ta e="T164" id="Seg_9840" s="T163">n-n:case</ta>
            <ta e="T165" id="Seg_9841" s="T164">v-v:tense-v:pred.pn</ta>
            <ta e="T166" id="Seg_9842" s="T165">n-n:(poss)-n:case</ta>
            <ta e="T167" id="Seg_9843" s="T166">adj-n:(poss)-n:case</ta>
            <ta e="T168" id="Seg_9844" s="T167">v-v&gt;v-v:cvb</ta>
            <ta e="T169" id="Seg_9845" s="T168">v-v:ptcp</ta>
            <ta e="T170" id="Seg_9846" s="T169">v-v:tense-v:pred.pn</ta>
            <ta e="T171" id="Seg_9847" s="T170">que-pro:case</ta>
            <ta e="T172" id="Seg_9848" s="T171">ptcl</ta>
            <ta e="T173" id="Seg_9849" s="T172">v-v&gt;v-v:(ins)-v:(neg)-v:pred.pn</ta>
            <ta e="T174" id="Seg_9850" s="T173">n-n:(poss)-n:case</ta>
            <ta e="T175" id="Seg_9851" s="T174">v-v:cvb</ta>
            <ta e="T176" id="Seg_9852" s="T175">v-v:ptcp</ta>
            <ta e="T177" id="Seg_9853" s="T176">v-v:tense-v:pred.pn</ta>
            <ta e="T178" id="Seg_9854" s="T177">v-v:tense-v:pred.pn</ta>
            <ta e="T179" id="Seg_9855" s="T178">v-v:cvb</ta>
            <ta e="T180" id="Seg_9856" s="T179">v-v:mood-v:temp.pn</ta>
            <ta e="T181" id="Seg_9857" s="T180">n-n:case</ta>
            <ta e="T182" id="Seg_9858" s="T181">cardnum</ta>
            <ta e="T183" id="Seg_9859" s="T182">adj</ta>
            <ta e="T184" id="Seg_9860" s="T183">n-n&gt;adj</ta>
            <ta e="T185" id="Seg_9861" s="T184">adj&gt;adj-adj</ta>
            <ta e="T186" id="Seg_9862" s="T185">n-n&gt;adj</ta>
            <ta e="T187" id="Seg_9863" s="T186">n-n:case</ta>
            <ta e="T188" id="Seg_9864" s="T187">v-v:tense-v:pred.pn</ta>
            <ta e="T189" id="Seg_9865" s="T188">n-n:case</ta>
            <ta e="T190" id="Seg_9866" s="T189">v-v:tense-v:pred.pn</ta>
            <ta e="T191" id="Seg_9867" s="T190">ptcl</ta>
            <ta e="T192" id="Seg_9868" s="T191">que-pro:(poss)-pro:case</ta>
            <ta e="T193" id="Seg_9869" s="T192">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T194" id="Seg_9870" s="T193">v-v:cvb</ta>
            <ta e="T195" id="Seg_9871" s="T194">v-v:tense-v:pred.pn</ta>
            <ta e="T196" id="Seg_9872" s="T195">n-n:case</ta>
            <ta e="T197" id="Seg_9873" s="T196">que</ta>
            <ta e="T198" id="Seg_9874" s="T197">v-v:tense-v:poss.pn</ta>
            <ta e="T199" id="Seg_9875" s="T198">que</ta>
            <ta e="T200" id="Seg_9876" s="T199">dempro</ta>
            <ta e="T201" id="Seg_9877" s="T200">adv</ta>
            <ta e="T202" id="Seg_9878" s="T201">v-v:tense-v:poss.pn</ta>
            <ta e="T203" id="Seg_9879" s="T202">n-n&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T204" id="Seg_9880" s="T203">ptcl</ta>
            <ta e="T205" id="Seg_9881" s="T204">v-v:tense-v:pred.pn</ta>
            <ta e="T206" id="Seg_9882" s="T205">n-n:case</ta>
            <ta e="T207" id="Seg_9883" s="T206">v-v:ptcp</ta>
            <ta e="T208" id="Seg_9884" s="T207">n-n:poss-n:case</ta>
            <ta e="T209" id="Seg_9885" s="T208">n-n&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T210" id="Seg_9886" s="T209">v-v:tense-v:pred.pn</ta>
            <ta e="T211" id="Seg_9887" s="T210">n-n:case</ta>
            <ta e="T212" id="Seg_9888" s="T211">ptcl</ta>
            <ta e="T213" id="Seg_9889" s="T212">adj-n:case</ta>
            <ta e="T214" id="Seg_9890" s="T213">n-n:case</ta>
            <ta e="T215" id="Seg_9891" s="T214">v-v:cvb</ta>
            <ta e="T216" id="Seg_9892" s="T215">v-v:tense-v:poss.pn</ta>
            <ta e="T217" id="Seg_9893" s="T216">v-v:tense-v:pred.pn</ta>
            <ta e="T218" id="Seg_9894" s="T217">n-n:case</ta>
            <ta e="T219" id="Seg_9895" s="T218">n-n:case</ta>
            <ta e="T220" id="Seg_9896" s="T219">que</ta>
            <ta e="T221" id="Seg_9897" s="T220">ptcl</ta>
            <ta e="T222" id="Seg_9898" s="T221">n-n:case</ta>
            <ta e="T223" id="Seg_9899" s="T222">n-n:case</ta>
            <ta e="T224" id="Seg_9900" s="T223">v-v:tense-v:pred.pn</ta>
            <ta e="T225" id="Seg_9901" s="T224">n-n:(poss)-n:case</ta>
            <ta e="T226" id="Seg_9902" s="T225">v-v:tense-v:pred.pn</ta>
            <ta e="T227" id="Seg_9903" s="T226">n-n:poss-n:case</ta>
            <ta e="T228" id="Seg_9904" s="T227">n-n:case</ta>
            <ta e="T229" id="Seg_9905" s="T228">v-v:tense-v:poss.pn</ta>
            <ta e="T230" id="Seg_9906" s="T229">v-v:tense-v:pred.pn</ta>
            <ta e="T231" id="Seg_9907" s="T230">n-n:(poss)-n:case</ta>
            <ta e="T232" id="Seg_9908" s="T231">v-v:mood-v:pred.pn</ta>
            <ta e="T233" id="Seg_9909" s="T232">que</ta>
            <ta e="T234" id="Seg_9910" s="T233">ptcl</ta>
            <ta e="T235" id="Seg_9911" s="T234">adj</ta>
            <ta e="T236" id="Seg_9912" s="T235">n-n:poss-n:case</ta>
            <ta e="T237" id="Seg_9913" s="T236">v-v:tense-v:poss.pn</ta>
            <ta e="T238" id="Seg_9914" s="T237">n-n:poss-n:case</ta>
            <ta e="T239" id="Seg_9915" s="T238">n-n:case</ta>
            <ta e="T240" id="Seg_9916" s="T239">n-n:poss-n:case</ta>
            <ta e="T241" id="Seg_9917" s="T240">v-v:tense-v:pred.pn</ta>
            <ta e="T242" id="Seg_9918" s="T241">n-n&gt;v-v:cvb</ta>
            <ta e="T243" id="Seg_9919" s="T242">v-v:tense-v:pred.pn</ta>
            <ta e="T244" id="Seg_9920" s="T243">que</ta>
            <ta e="T245" id="Seg_9921" s="T244">n-n:case</ta>
            <ta e="T246" id="Seg_9922" s="T245">n-n&gt;v-v:cvb</ta>
            <ta e="T247" id="Seg_9923" s="T246">v-v:tense-v:pred.pn</ta>
            <ta e="T248" id="Seg_9924" s="T247">dempro</ta>
            <ta e="T249" id="Seg_9925" s="T248">v-v:cvb</ta>
            <ta e="T250" id="Seg_9926" s="T249">n-n:case</ta>
            <ta e="T251" id="Seg_9927" s="T250">v-v:tense-v:pred.pn</ta>
            <ta e="T252" id="Seg_9928" s="T251">n-n:poss-n:case</ta>
            <ta e="T253" id="Seg_9929" s="T252">ptcl-ptcl</ta>
            <ta e="T254" id="Seg_9930" s="T253">pers-pro:case</ta>
            <ta e="T255" id="Seg_9931" s="T254">n-n:case</ta>
            <ta e="T256" id="Seg_9932" s="T255">v-v:ptcp</ta>
            <ta e="T257" id="Seg_9933" s="T256">n-n&gt;adj-n:(pred.pn)</ta>
            <ta e="T258" id="Seg_9934" s="T257">n-n:poss-n:case</ta>
            <ta e="T259" id="Seg_9935" s="T258">dempro-pro:case</ta>
            <ta e="T260" id="Seg_9936" s="T259">post</ta>
            <ta e="T261" id="Seg_9937" s="T260">adv</ta>
            <ta e="T262" id="Seg_9938" s="T261">v-v:tense-v:pred.pn</ta>
            <ta e="T263" id="Seg_9939" s="T262">dempro-pro:case</ta>
            <ta e="T264" id="Seg_9940" s="T263">v-v:tense-v:poss.pn</ta>
            <ta e="T265" id="Seg_9941" s="T264">v-v:tense-v:pred.pn</ta>
            <ta e="T266" id="Seg_9942" s="T265">v-v:cvb</ta>
            <ta e="T267" id="Seg_9943" s="T266">v-v:ptcp</ta>
            <ta e="T268" id="Seg_9944" s="T267">n-n&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T269" id="Seg_9945" s="T268">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T270" id="Seg_9946" s="T269">v-v:ptcp</ta>
            <ta e="T271" id="Seg_9947" s="T270">n-n:(poss)</ta>
            <ta e="T272" id="Seg_9948" s="T271">v-v:neg-v:pred.pn</ta>
            <ta e="T273" id="Seg_9949" s="T272">que</ta>
            <ta e="T274" id="Seg_9950" s="T273">v-v:tense-v:poss.pn</ta>
            <ta e="T275" id="Seg_9951" s="T274">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T276" id="Seg_9952" s="T275">que-pro:case</ta>
            <ta e="T277" id="Seg_9953" s="T276">ptcl</ta>
            <ta e="T278" id="Seg_9954" s="T277">ptcl</ta>
            <ta e="T279" id="Seg_9955" s="T278">v-v:cvb</ta>
            <ta e="T280" id="Seg_9956" s="T279">v-v:mood-v:poss.pn</ta>
            <ta e="T281" id="Seg_9957" s="T280">interj</ta>
            <ta e="T282" id="Seg_9958" s="T281">v-v:tense-v:pred.pn</ta>
            <ta e="T283" id="Seg_9959" s="T282">adj</ta>
            <ta e="T284" id="Seg_9960" s="T283">n-n:(poss)-n:case</ta>
            <ta e="T285" id="Seg_9961" s="T284">adv</ta>
            <ta e="T286" id="Seg_9962" s="T285">pers-pro:case</ta>
            <ta e="T287" id="Seg_9963" s="T286">cardnum</ta>
            <ta e="T288" id="Seg_9964" s="T287">n-n:case</ta>
            <ta e="T289" id="Seg_9965" s="T288">n-n:(poss)-n:case</ta>
            <ta e="T290" id="Seg_9966" s="T289">v-v:tense-v:poss.pn</ta>
            <ta e="T291" id="Seg_9967" s="T290">dempro-pro:case</ta>
            <ta e="T292" id="Seg_9968" s="T291">cardnum</ta>
            <ta e="T293" id="Seg_9969" s="T292">n-n&gt;adj</ta>
            <ta e="T294" id="Seg_9970" s="T293">n-n:case</ta>
            <ta e="T295" id="Seg_9971" s="T294">v-v:tense-v:pred.pn</ta>
            <ta e="T296" id="Seg_9972" s="T295">n-n&gt;adj-n:(poss)-n:case</ta>
            <ta e="T297" id="Seg_9973" s="T296">dempro-pro:case</ta>
            <ta e="T298" id="Seg_9974" s="T297">v-v&gt;v-v:neg-v:(ins)-v:poss.pn</ta>
            <ta e="T299" id="Seg_9975" s="T298">n-n:case</ta>
            <ta e="T300" id="Seg_9976" s="T299">v-v:tense-v:poss.pn</ta>
            <ta e="T301" id="Seg_9977" s="T300">v-v:cvb-v:pred.pn</ta>
            <ta e="T302" id="Seg_9978" s="T301">dempro-pro:case</ta>
            <ta e="T303" id="Seg_9979" s="T302">v-v:mood-v:poss.pn</ta>
            <ta e="T304" id="Seg_9980" s="T303">dempro</ta>
            <ta e="T305" id="Seg_9981" s="T304">n-n:case</ta>
            <ta e="T306" id="Seg_9982" s="T305">v-v:ptcp</ta>
            <ta e="T307" id="Seg_9983" s="T306">v-v:tense-v:pred.pn</ta>
            <ta e="T308" id="Seg_9984" s="T307">pers-pro:case</ta>
            <ta e="T309" id="Seg_9985" s="T308">v-v:tense-v:poss.pn</ta>
            <ta e="T310" id="Seg_9986" s="T309">v-v:cvb</ta>
            <ta e="T311" id="Seg_9987" s="T310">ptcl</ta>
            <ta e="T312" id="Seg_9988" s="T311">adv</ta>
            <ta e="T313" id="Seg_9989" s="T312">v-v:tense-v:pred.pn</ta>
            <ta e="T314" id="Seg_9990" s="T313">pers-pro:case</ta>
            <ta e="T315" id="Seg_9991" s="T314">n-n:poss-n:case</ta>
            <ta e="T316" id="Seg_9992" s="T315">v-v:(ins)-v:(neg)-v:mood.pn</ta>
            <ta e="T317" id="Seg_9993" s="T316">dempro</ta>
            <ta e="T318" id="Seg_9994" s="T317">n-n:case</ta>
            <ta e="T319" id="Seg_9995" s="T318">n-n:poss-n:case</ta>
            <ta e="T320" id="Seg_9996" s="T319">v-v:cvb</ta>
            <ta e="T321" id="Seg_9997" s="T320">post</ta>
            <ta e="T322" id="Seg_9998" s="T321">n-n:case</ta>
            <ta e="T323" id="Seg_9999" s="T322">v-v:tense-v:pred.pn</ta>
            <ta e="T324" id="Seg_10000" s="T323">v-v:cvb</ta>
            <ta e="T325" id="Seg_10001" s="T324">cardnum</ta>
            <ta e="T326" id="Seg_10002" s="T325">n-n:case</ta>
            <ta e="T327" id="Seg_10003" s="T326">n-n:(poss)-n:case</ta>
            <ta e="T328" id="Seg_10004" s="T327">v-v&gt;v-v:cvb</ta>
            <ta e="T329" id="Seg_10005" s="T328">v-v:mood.pn</ta>
            <ta e="T330" id="Seg_10006" s="T329">adv</ta>
            <ta e="T331" id="Seg_10007" s="T330">pers-pro:case</ta>
            <ta e="T332" id="Seg_10008" s="T331">n-n:poss-n:case</ta>
            <ta e="T333" id="Seg_10009" s="T332">n-n:case</ta>
            <ta e="T334" id="Seg_10010" s="T333">v-v:tense-v:poss.pn</ta>
            <ta e="T335" id="Seg_10011" s="T334">ptcl-ptcl:(poss.pn)</ta>
            <ta e="T336" id="Seg_10012" s="T335">pers-pro:case</ta>
            <ta e="T337" id="Seg_10013" s="T336">n-n&gt;v-v:(ins)-v&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T338" id="Seg_10014" s="T337">n-n:case</ta>
            <ta e="T339" id="Seg_10015" s="T338">v-v:tense-v:poss.pn</ta>
            <ta e="T340" id="Seg_10016" s="T339">pers-pro:case</ta>
            <ta e="T341" id="Seg_10017" s="T340">pers-pro:case</ta>
            <ta e="T342" id="Seg_10018" s="T341">n-n:poss-n:case</ta>
            <ta e="T343" id="Seg_10019" s="T342">v-v:tense-v:poss.pn</ta>
            <ta e="T344" id="Seg_10020" s="T343">v-v:tense-v:pred.pn</ta>
            <ta e="T345" id="Seg_10021" s="T344">adj</ta>
            <ta e="T346" id="Seg_10022" s="T345">n-n:(poss)-n:case</ta>
            <ta e="T347" id="Seg_10023" s="T346">n-n:case</ta>
            <ta e="T348" id="Seg_10024" s="T347">v-v:ptcp-v:(case)</ta>
            <ta e="T349" id="Seg_10025" s="T348">adj-n:case</ta>
            <ta e="T350" id="Seg_10026" s="T349">v-v:mood.pn</ta>
            <ta e="T351" id="Seg_10027" s="T350">v-v:tense-v:pred.pn</ta>
            <ta e="T352" id="Seg_10028" s="T351">adj</ta>
            <ta e="T353" id="Seg_10029" s="T352">n-n:(poss)-n:case</ta>
            <ta e="T354" id="Seg_10030" s="T353">adj</ta>
            <ta e="T355" id="Seg_10031" s="T354">n-n:poss-n:case</ta>
            <ta e="T356" id="Seg_10032" s="T355">v-v:ptcp</ta>
            <ta e="T357" id="Seg_10033" s="T356">v-v:mood-v:pred.pn</ta>
            <ta e="T358" id="Seg_10034" s="T357">cardnum</ta>
            <ta e="T359" id="Seg_10035" s="T358">n-n:case</ta>
            <ta e="T360" id="Seg_10036" s="T359">n-n&gt;adj-n:case</ta>
            <ta e="T361" id="Seg_10037" s="T360">v-v:ptcp</ta>
            <ta e="T362" id="Seg_10038" s="T361">v-v:tense-v:poss.pn</ta>
            <ta e="T363" id="Seg_10039" s="T362">adj</ta>
            <ta e="T364" id="Seg_10040" s="T363">n-n:(poss)-n:case</ta>
            <ta e="T365" id="Seg_10041" s="T364">n-n:(poss)-n:case</ta>
            <ta e="T366" id="Seg_10042" s="T365">v-v:tense-v:poss.pn</ta>
            <ta e="T367" id="Seg_10043" s="T366">n-n:(num)-n:case</ta>
            <ta e="T368" id="Seg_10044" s="T367">n-n:poss-n:case</ta>
            <ta e="T369" id="Seg_10045" s="T368">cardnum</ta>
            <ta e="T370" id="Seg_10046" s="T369">n-n:(ins)-n:case</ta>
            <ta e="T371" id="Seg_10047" s="T370">v-v:tense-v:poss.pn</ta>
            <ta e="T372" id="Seg_10048" s="T371">v-v:(ins)-v&gt;v-v:(neg)-v:mood-v:temp.pn</ta>
            <ta e="T373" id="Seg_10049" s="T372">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T374" id="Seg_10050" s="T373">v-v:(ins)-v&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T375" id="Seg_10051" s="T374">v-v&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T376" id="Seg_10052" s="T375">ptcl-ptcl:(poss.pn)</ta>
            <ta e="T377" id="Seg_10053" s="T376">v-v:tense-v:pred.pn</ta>
            <ta e="T378" id="Seg_10054" s="T377">n-n&gt;adj</ta>
            <ta e="T379" id="Seg_10055" s="T378">n-n:case</ta>
            <ta e="T380" id="Seg_10056" s="T379">v-v:tense-v:poss.pn</ta>
            <ta e="T381" id="Seg_10057" s="T380">adj</ta>
            <ta e="T382" id="Seg_10058" s="T381">n-n:poss-n:case</ta>
            <ta e="T383" id="Seg_10059" s="T382">v-v&gt;v-v:cvb</ta>
            <ta e="T384" id="Seg_10060" s="T383">cardnum-cardnum&gt;ordnum</ta>
            <ta e="T385" id="Seg_10061" s="T384">n-n:poss-n:case</ta>
            <ta e="T386" id="Seg_10062" s="T385">adj</ta>
            <ta e="T387" id="Seg_10063" s="T386">n-n:(poss)-n:case</ta>
            <ta e="T388" id="Seg_10064" s="T387">v-v:tense-v:pred.pn</ta>
            <ta e="T389" id="Seg_10065" s="T388">n-n:case</ta>
            <ta e="T390" id="Seg_10066" s="T389">n-n&gt;adj-n:case</ta>
            <ta e="T391" id="Seg_10067" s="T390">cardnum</ta>
            <ta e="T392" id="Seg_10068" s="T391">que</ta>
            <ta e="T393" id="Seg_10069" s="T392">n-n:case</ta>
            <ta e="T394" id="Seg_10070" s="T393">n-n&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T395" id="Seg_10071" s="T394">adj</ta>
            <ta e="T396" id="Seg_10072" s="T395">n-n:(poss)-n:case</ta>
            <ta e="T397" id="Seg_10073" s="T396">v-v:tense-v:pred.pn</ta>
            <ta e="T398" id="Seg_10074" s="T397">adj</ta>
            <ta e="T399" id="Seg_10075" s="T398">n-n:(poss)-n:case</ta>
            <ta e="T400" id="Seg_10076" s="T399">n-n:case</ta>
            <ta e="T401" id="Seg_10077" s="T400">v-v:ptcp</ta>
            <ta e="T402" id="Seg_10078" s="T401">n-n:case</ta>
            <ta e="T403" id="Seg_10079" s="T402">cardnum</ta>
            <ta e="T404" id="Seg_10080" s="T403">n-n:case</ta>
            <ta e="T405" id="Seg_10081" s="T404">v-v:tense-v:pred.pn</ta>
            <ta e="T406" id="Seg_10082" s="T405">dempro-pro:case</ta>
            <ta e="T407" id="Seg_10083" s="T406">n-n:case</ta>
            <ta e="T408" id="Seg_10084" s="T407">v-v:tense-v:poss.pn</ta>
            <ta e="T409" id="Seg_10085" s="T408">pers-pro:case</ta>
            <ta e="T410" id="Seg_10086" s="T409">n-n:case</ta>
            <ta e="T411" id="Seg_10087" s="T410">v-v&gt;v-v:mood.pn</ta>
            <ta e="T412" id="Seg_10088" s="T411">cardnum-cardnum&gt;collnum</ta>
            <ta e="T413" id="Seg_10089" s="T412">v-v:mood.pn</ta>
            <ta e="T414" id="Seg_10090" s="T413">adj</ta>
            <ta e="T415" id="Seg_10091" s="T414">n-n:(poss)-n:case</ta>
            <ta e="T416" id="Seg_10092" s="T415">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T417" id="Seg_10093" s="T416">v-v:tense-v:pred.pn</ta>
            <ta e="T418" id="Seg_10094" s="T417">n-n&gt;v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T419" id="Seg_10095" s="T418">n-n&gt;v-v:cvb</ta>
            <ta e="T420" id="Seg_10096" s="T419">dempro-pro:case</ta>
            <ta e="T421" id="Seg_10097" s="T420">post</ta>
            <ta e="T422" id="Seg_10098" s="T421">v-v:ptcp</ta>
            <ta e="T423" id="Seg_10099" s="T422">n-n:(poss)-n:case</ta>
            <ta e="T424" id="Seg_10100" s="T423">adv</ta>
            <ta e="T425" id="Seg_10101" s="T424">n-n:case</ta>
            <ta e="T426" id="Seg_10102" s="T425">v-v:tense-v:pred.pn</ta>
            <ta e="T427" id="Seg_10103" s="T426">v-v:cvb</ta>
            <ta e="T428" id="Seg_10104" s="T427">n-n:case</ta>
            <ta e="T429" id="Seg_10105" s="T428">v-v:tense-v:poss.pn</ta>
            <ta e="T430" id="Seg_10106" s="T429">que-pro:case</ta>
            <ta e="T431" id="Seg_10107" s="T430">n-n:(poss)-n:case-ptcl</ta>
            <ta e="T432" id="Seg_10108" s="T431">n-n:case</ta>
            <ta e="T433" id="Seg_10109" s="T432">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T434" id="Seg_10110" s="T433">v-v:tense-v:pred.pn</ta>
            <ta e="T435" id="Seg_10111" s="T434">adj</ta>
            <ta e="T436" id="Seg_10112" s="T435">n-n:(poss)-n:case</ta>
            <ta e="T437" id="Seg_10113" s="T436">pers-pro:case</ta>
            <ta e="T438" id="Seg_10114" s="T437">v-v:tense-v:pred.pn</ta>
            <ta e="T439" id="Seg_10115" s="T438">ptcl</ta>
            <ta e="T440" id="Seg_10116" s="T439">dempro</ta>
            <ta e="T441" id="Seg_10117" s="T440">n-n:case</ta>
            <ta e="T442" id="Seg_10118" s="T441">adv</ta>
            <ta e="T443" id="Seg_10119" s="T442">pers-pro:case</ta>
            <ta e="T444" id="Seg_10120" s="T443">pers-pro:case</ta>
            <ta e="T445" id="Seg_10121" s="T444">v-v:tense-v:pred.pn</ta>
            <ta e="T446" id="Seg_10122" s="T445">n-n:poss-n:case</ta>
            <ta e="T447" id="Seg_10123" s="T446">v-v:tense-v:poss.pn</ta>
            <ta e="T448" id="Seg_10124" s="T447">v-v:tense-v:pred.pn</ta>
            <ta e="T449" id="Seg_10125" s="T448">adj</ta>
            <ta e="T450" id="Seg_10126" s="T449">n-n:(poss)-n:case</ta>
            <ta e="T451" id="Seg_10127" s="T450">v-v:tense-v:pred.pn</ta>
            <ta e="T452" id="Seg_10128" s="T451">v-v:ptcp-v&gt;n-n:case</ta>
            <ta e="T453" id="Seg_10129" s="T452">n-n:case</ta>
            <ta e="T454" id="Seg_10130" s="T453">cardnum-cardnum&gt;collnum-n:case</ta>
            <ta e="T455" id="Seg_10131" s="T454">ptcl</ta>
            <ta e="T456" id="Seg_10132" s="T455">que</ta>
            <ta e="T457" id="Seg_10133" s="T456">v-v:tense-v:pred.pn</ta>
            <ta e="T458" id="Seg_10134" s="T457">v-v:tense-v:pred.pn</ta>
            <ta e="T459" id="Seg_10135" s="T458">adj</ta>
            <ta e="T460" id="Seg_10136" s="T459">n-n:(poss)-n:case</ta>
            <ta e="T461" id="Seg_10137" s="T460">pers-pro:case</ta>
            <ta e="T462" id="Seg_10138" s="T461">n-n:case</ta>
            <ta e="T463" id="Seg_10139" s="T462">n-n:case</ta>
            <ta e="T464" id="Seg_10140" s="T463">v-v:tense-v:pred.pn</ta>
            <ta e="T465" id="Seg_10141" s="T464">v-v&gt;v-v:mood.pn</ta>
            <ta e="T466" id="Seg_10142" s="T465">n-n:case</ta>
            <ta e="T467" id="Seg_10143" s="T466">dempro</ta>
            <ta e="T468" id="Seg_10144" s="T467">adj-n:case</ta>
            <ta e="T469" id="Seg_10145" s="T468">adj</ta>
            <ta e="T470" id="Seg_10146" s="T469">n-n:poss-n:case</ta>
            <ta e="T471" id="Seg_10147" s="T470">v-v:cvb</ta>
            <ta e="T472" id="Seg_10148" s="T471">post</ta>
            <ta e="T473" id="Seg_10149" s="T472">n-n:case</ta>
            <ta e="T474" id="Seg_10150" s="T473">v-v:tense-v:pred.pn</ta>
            <ta e="T475" id="Seg_10151" s="T474">v-v:cvb</ta>
            <ta e="T476" id="Seg_10152" s="T475">v-v:tense-v:poss.pn</ta>
            <ta e="T477" id="Seg_10153" s="T476">pers-pro:case</ta>
            <ta e="T478" id="Seg_10154" s="T477">n-n:(num)-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T479" id="Seg_10155" s="T478">v-v:cvb</ta>
            <ta e="T480" id="Seg_10156" s="T479">v-v:ptcp</ta>
            <ta e="T481" id="Seg_10157" s="T480">n-n:case</ta>
            <ta e="T482" id="Seg_10158" s="T481">v-v&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T483" id="Seg_10159" s="T482">n-n:case</ta>
            <ta e="T484" id="Seg_10160" s="T483">v-v:mood-v:temp.pn</ta>
            <ta e="T485" id="Seg_10161" s="T484">v-v:cvb</ta>
            <ta e="T486" id="Seg_10162" s="T485">v-v:tense-v:poss.pn</ta>
            <ta e="T487" id="Seg_10163" s="T486">ptcl-ptcl:(poss.pn)</ta>
            <ta e="T488" id="Seg_10164" s="T487">que-pro:case</ta>
            <ta e="T489" id="Seg_10165" s="T488">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T490" id="Seg_10166" s="T489">v-v:tense-v:pred.pn</ta>
            <ta e="T491" id="Seg_10167" s="T490">cardnum-cardnum&gt;cardnum</ta>
            <ta e="T492" id="Seg_10168" s="T491">n-n:case</ta>
            <ta e="T493" id="Seg_10169" s="T492">v-v:cvb</ta>
            <ta e="T494" id="Seg_10170" s="T493">adv</ta>
            <ta e="T495" id="Seg_10171" s="T494">v-v:ptcp</ta>
            <ta e="T496" id="Seg_10172" s="T495">n-n:case</ta>
            <ta e="T497" id="Seg_10173" s="T496">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T498" id="Seg_10174" s="T497">v-v:cvb</ta>
            <ta e="T499" id="Seg_10175" s="T498">v-v:tense-v:pred.pn</ta>
            <ta e="T500" id="Seg_10176" s="T499">adv</ta>
            <ta e="T501" id="Seg_10177" s="T500">adj</ta>
            <ta e="T502" id="Seg_10178" s="T501">n-n:poss-n:case</ta>
            <ta e="T503" id="Seg_10179" s="T502">v-v:tense-v:pred.pn</ta>
            <ta e="T504" id="Seg_10180" s="T503">adj</ta>
            <ta e="T505" id="Seg_10181" s="T504">n-n:poss-n:case</ta>
            <ta e="T506" id="Seg_10182" s="T505">n-n:case</ta>
            <ta e="T507" id="Seg_10183" s="T506">v-v:cvb</ta>
            <ta e="T508" id="Seg_10184" s="T507">v-v:tense-v:pred.pn</ta>
            <ta e="T509" id="Seg_10185" s="T508">cardnum-cardnum&gt;cardnum</ta>
            <ta e="T510" id="Seg_10186" s="T509">n-n:case</ta>
            <ta e="T511" id="Seg_10187" s="T510">n-n:case</ta>
            <ta e="T512" id="Seg_10188" s="T511">v-v:ptcp</ta>
            <ta e="T513" id="Seg_10189" s="T512">n-n:case</ta>
            <ta e="T514" id="Seg_10190" s="T513">v-v:ptcp</ta>
            <ta e="T515" id="Seg_10191" s="T514">n-n:case</ta>
            <ta e="T516" id="Seg_10192" s="T515">n-n:case</ta>
            <ta e="T517" id="Seg_10193" s="T516">adj-n:poss-n:case</ta>
            <ta e="T518" id="Seg_10194" s="T517">v-v:tense-v:pred.pn</ta>
            <ta e="T519" id="Seg_10195" s="T518">n-n:case</ta>
            <ta e="T520" id="Seg_10196" s="T519">ptcl</ta>
            <ta e="T521" id="Seg_10197" s="T520">pers-pro:case</ta>
            <ta e="T522" id="Seg_10198" s="T521">pers-pro:case</ta>
            <ta e="T523" id="Seg_10199" s="T522">n-n&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T524" id="Seg_10200" s="T523">pers-pro:case</ta>
            <ta e="T525" id="Seg_10201" s="T524">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T526" id="Seg_10202" s="T525">adj</ta>
            <ta e="T527" id="Seg_10203" s="T526">n-n:(poss)-n:case</ta>
            <ta e="T528" id="Seg_10204" s="T527">n-n:case</ta>
            <ta e="T529" id="Seg_10205" s="T528">v-v:ptcp</ta>
            <ta e="T530" id="Seg_10206" s="T529">n-n:case</ta>
            <ta e="T531" id="Seg_10207" s="T530">v-v:tense-v:poss.pn</ta>
            <ta e="T532" id="Seg_10208" s="T531">adv</ta>
            <ta e="T533" id="Seg_10209" s="T532">v-v:tense-v:pred.pn</ta>
            <ta e="T534" id="Seg_10210" s="T533">n-n:case</ta>
            <ta e="T535" id="Seg_10211" s="T534">n-n:case</ta>
            <ta e="T536" id="Seg_10212" s="T535">n-n:case</ta>
            <ta e="T537" id="Seg_10213" s="T536">v-v:tense-v:pred.pn</ta>
            <ta e="T538" id="Seg_10214" s="T537">n-n:(poss)-n:case</ta>
            <ta e="T539" id="Seg_10215" s="T538">ptcl</ta>
            <ta e="T540" id="Seg_10216" s="T539">n-n:poss-n:case</ta>
            <ta e="T541" id="Seg_10217" s="T540">n-n:poss-n:case</ta>
            <ta e="T542" id="Seg_10218" s="T541">v-v:cvb</ta>
            <ta e="T543" id="Seg_10219" s="T542">v-v&gt;v-v:mood.pn</ta>
            <ta e="T544" id="Seg_10220" s="T543">adv</ta>
            <ta e="T545" id="Seg_10221" s="T544">ptcl</ta>
            <ta e="T546" id="Seg_10222" s="T545">adj-adj&gt;adv</ta>
            <ta e="T547" id="Seg_10223" s="T546">v-v:cvb</ta>
            <ta e="T548" id="Seg_10224" s="T547">v-v:tense-v:pred.pn</ta>
            <ta e="T549" id="Seg_10225" s="T548">n-n:case</ta>
            <ta e="T550" id="Seg_10226" s="T549">n-n:case</ta>
            <ta e="T551" id="Seg_10227" s="T550">v-v:tense-v:pred.pn</ta>
            <ta e="T552" id="Seg_10228" s="T551">n-n:case</ta>
            <ta e="T553" id="Seg_10229" s="T552">n-n:case</ta>
            <ta e="T554" id="Seg_10230" s="T553">ptcl</ta>
            <ta e="T555" id="Seg_10231" s="T554">adj</ta>
            <ta e="T556" id="Seg_10232" s="T555">n-n:(num)-n:poss-n:case</ta>
            <ta e="T557" id="Seg_10233" s="T556">v-v:cvb</ta>
            <ta e="T558" id="Seg_10234" s="T557">v-v:tense-v:pred.pn</ta>
            <ta e="T559" id="Seg_10235" s="T558">n-n:poss-n:case</ta>
            <ta e="T560" id="Seg_10236" s="T559">v-v:tense-v:pred.pn</ta>
            <ta e="T561" id="Seg_10237" s="T560">conj</ta>
            <ta e="T562" id="Seg_10238" s="T561">v-v:cvb</ta>
            <ta e="T563" id="Seg_10239" s="T562">v-v:tense-v:pred.pn</ta>
            <ta e="T564" id="Seg_10240" s="T563">n-n:case</ta>
            <ta e="T565" id="Seg_10241" s="T564">post</ta>
            <ta e="T566" id="Seg_10242" s="T565">n-n:case</ta>
            <ta e="T567" id="Seg_10243" s="T566">ptcl</ta>
            <ta e="T568" id="Seg_10244" s="T567">n-n:case</ta>
            <ta e="T569" id="Seg_10245" s="T568">n-n:case</ta>
            <ta e="T570" id="Seg_10246" s="T569">ptcl</ta>
            <ta e="T571" id="Seg_10247" s="T570">v-v:(ins)-v&gt;v-v&gt;v-v:neg-v:pred.pn</ta>
            <ta e="T572" id="Seg_10248" s="T571">n-n:(poss)-n:case</ta>
            <ta e="T573" id="Seg_10249" s="T572">adj-n:case</ta>
            <ta e="T574" id="Seg_10250" s="T573">adv</ta>
            <ta e="T575" id="Seg_10251" s="T574">adj-n:case</ta>
            <ta e="T576" id="Seg_10252" s="T575">ptcl</ta>
            <ta e="T577" id="Seg_10253" s="T576">v-v:mood.pn</ta>
            <ta e="T578" id="Seg_10254" s="T577">n-n:poss-n:case</ta>
            <ta e="T579" id="Seg_10255" s="T578">n-n:case</ta>
            <ta e="T580" id="Seg_10256" s="T579">n-n:case</ta>
            <ta e="T581" id="Seg_10257" s="T580">que</ta>
            <ta e="T582" id="Seg_10258" s="T581">v-v:tense-v:poss.pn</ta>
            <ta e="T583" id="Seg_10259" s="T582">que</ta>
            <ta e="T584" id="Seg_10260" s="T583">n-n:poss-n:case</ta>
            <ta e="T585" id="Seg_10261" s="T584">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T586" id="Seg_10262" s="T585">n-n:case</ta>
            <ta e="T587" id="Seg_10263" s="T586">post</ta>
            <ta e="T588" id="Seg_10264" s="T587">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T589" id="Seg_10265" s="T588">ptcl</ta>
            <ta e="T590" id="Seg_10266" s="T589">ptcl</ta>
            <ta e="T591" id="Seg_10267" s="T590">n-n:case</ta>
            <ta e="T592" id="Seg_10268" s="T591">n-n:(num)-n:case</ta>
            <ta e="T593" id="Seg_10269" s="T592">n-n:poss-n:case</ta>
            <ta e="T594" id="Seg_10270" s="T593">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T595" id="Seg_10271" s="T594">n-n&gt;adj</ta>
            <ta e="T596" id="Seg_10272" s="T595">n-n:case</ta>
            <ta e="T597" id="Seg_10273" s="T596">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T598" id="Seg_10274" s="T597">post</ta>
            <ta e="T599" id="Seg_10275" s="T598">n-n:case</ta>
            <ta e="T600" id="Seg_10276" s="T599">v-v:tense-v:poss.pn</ta>
            <ta e="T601" id="Seg_10277" s="T600">pers-pro:case</ta>
            <ta e="T602" id="Seg_10278" s="T601">adj</ta>
            <ta e="T603" id="Seg_10279" s="T602">n-n:case</ta>
            <ta e="T604" id="Seg_10280" s="T603">v-v:tense-v:poss.pn</ta>
            <ta e="T605" id="Seg_10281" s="T604">n-n:case</ta>
            <ta e="T606" id="Seg_10282" s="T605">n-n:case</ta>
            <ta e="T607" id="Seg_10283" s="T606">v-v:cvb</ta>
            <ta e="T608" id="Seg_10284" s="T607">v-v:tense-v:pred.pn</ta>
            <ta e="T609" id="Seg_10285" s="T608">cardnum</ta>
            <ta e="T610" id="Seg_10286" s="T609">n-n:case</ta>
            <ta e="T611" id="Seg_10287" s="T610">n-n:poss-n:case</ta>
            <ta e="T612" id="Seg_10288" s="T611">v-v:tense-v:poss.pn</ta>
            <ta e="T613" id="Seg_10289" s="T612">n-n:case</ta>
            <ta e="T614" id="Seg_10290" s="T613">v-v:cvb</ta>
            <ta e="T615" id="Seg_10291" s="T614">v-v:tense-v:pred.pn</ta>
            <ta e="T616" id="Seg_10292" s="T615">n-n:(poss)-n:case</ta>
            <ta e="T617" id="Seg_10293" s="T616">que</ta>
            <ta e="T618" id="Seg_10294" s="T617">ptcl</ta>
            <ta e="T619" id="Seg_10295" s="T618">ptcl</ta>
            <ta e="T620" id="Seg_10296" s="T619">n-n:case</ta>
            <ta e="T621" id="Seg_10297" s="T620">v-v:tense-v:pred.pn</ta>
            <ta e="T622" id="Seg_10298" s="T621">n-n:case</ta>
            <ta e="T623" id="Seg_10299" s="T622">n-n:case</ta>
            <ta e="T624" id="Seg_10300" s="T623">v-v:cvb</ta>
            <ta e="T625" id="Seg_10301" s="T624">v-v:tense-v:pred.pn</ta>
            <ta e="T626" id="Seg_10302" s="T625">cardnum</ta>
            <ta e="T627" id="Seg_10303" s="T626">n-n:case</ta>
            <ta e="T628" id="Seg_10304" s="T627">n-n:poss-n:case</ta>
            <ta e="T629" id="Seg_10305" s="T628">v-v:cvb</ta>
            <ta e="T630" id="Seg_10306" s="T629">v-v:cvb</ta>
            <ta e="T631" id="Seg_10307" s="T630">v-v:cvb</ta>
            <ta e="T632" id="Seg_10308" s="T631">v-v:tense-v:pred.pn</ta>
            <ta e="T633" id="Seg_10309" s="T632">cardnum</ta>
            <ta e="T634" id="Seg_10310" s="T633">n-n:case</ta>
            <ta e="T635" id="Seg_10311" s="T634">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T636" id="Seg_10312" s="T635">n-n:(poss)-n:case</ta>
            <ta e="T637" id="Seg_10313" s="T636">v-v:cvb</ta>
            <ta e="T638" id="Seg_10314" s="T637">n-n:case</ta>
            <ta e="T639" id="Seg_10315" s="T638">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T640" id="Seg_10316" s="T639">adv-n:case</ta>
            <ta e="T641" id="Seg_10317" s="T640">ptcl</ta>
            <ta e="T642" id="Seg_10318" s="T641">v-v:mood.pn</ta>
            <ta e="T643" id="Seg_10319" s="T642">n-n:poss-n:case</ta>
            <ta e="T644" id="Seg_10320" s="T643">v-v:tense-v:pred.pn</ta>
            <ta e="T645" id="Seg_10321" s="T644">n-n:case</ta>
            <ta e="T646" id="Seg_10322" s="T645">n-n:(poss)-n:case</ta>
            <ta e="T647" id="Seg_10323" s="T646">n-n:case</ta>
            <ta e="T648" id="Seg_10324" s="T647">n-n:poss-n:case</ta>
            <ta e="T649" id="Seg_10325" s="T648">v-v:tense-v:pred.pn</ta>
            <ta e="T650" id="Seg_10326" s="T649">v-v:(ins)-v&gt;v-v:neg-v:pred.pn</ta>
            <ta e="T651" id="Seg_10327" s="T650">v-v:cvb</ta>
            <ta e="T652" id="Seg_10328" s="T651">v-v:tense-v:pred.pn</ta>
            <ta e="T653" id="Seg_10329" s="T652">n-n:case</ta>
            <ta e="T654" id="Seg_10330" s="T653">n-n:poss-n:case</ta>
            <ta e="T655" id="Seg_10331" s="T654">v-v:tense-v:pred.pn</ta>
            <ta e="T656" id="Seg_10332" s="T655">adj</ta>
            <ta e="T657" id="Seg_10333" s="T656">n-n:case</ta>
            <ta e="T658" id="Seg_10334" s="T657">v-v:tense-v:pred.pn</ta>
            <ta e="T659" id="Seg_10335" s="T658">n-n:case</ta>
            <ta e="T660" id="Seg_10336" s="T659">v-v:tense-v:pred.pn</ta>
            <ta e="T661" id="Seg_10337" s="T660">v-v:cvb</ta>
            <ta e="T662" id="Seg_10338" s="T661">v-v:ptcp</ta>
            <ta e="T663" id="Seg_10339" s="T662">n-n:case</ta>
            <ta e="T664" id="Seg_10340" s="T663">interj</ta>
            <ta e="T665" id="Seg_10341" s="T664">que</ta>
            <ta e="T666" id="Seg_10342" s="T665">n-n&gt;v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T667" id="Seg_10343" s="T666">v-v:ptcp</ta>
            <ta e="T668" id="Seg_10344" s="T667">n-n:case</ta>
            <ta e="T669" id="Seg_10345" s="T668">v-v:(ins)-v:mood.pn</ta>
            <ta e="T670" id="Seg_10346" s="T669">n-n:case</ta>
            <ta e="T671" id="Seg_10347" s="T670">v-v:(ins)-v&gt;v-v:mood.pn</ta>
            <ta e="T672" id="Seg_10348" s="T671">v-v:mood.pn</ta>
            <ta e="T673" id="Seg_10349" s="T672">n-n:(num)-n:case</ta>
            <ta e="T674" id="Seg_10350" s="T673">v-v:tense-v:pred.pn</ta>
            <ta e="T675" id="Seg_10351" s="T674">adj</ta>
            <ta e="T676" id="Seg_10352" s="T675">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T677" id="Seg_10353" s="T676">v-v:tense-v:pred.pn</ta>
            <ta e="T678" id="Seg_10354" s="T677">n-n:(num)-n:case</ta>
            <ta e="T679" id="Seg_10355" s="T678">v-v:tense-v:pred.pn</ta>
            <ta e="T680" id="Seg_10356" s="T679">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T681" id="Seg_10357" s="T680">ptcl</ta>
            <ta e="T682" id="Seg_10358" s="T681">v-v:tense-v:pred.pn</ta>
            <ta e="T683" id="Seg_10359" s="T682">ptcl</ta>
            <ta e="T684" id="Seg_10360" s="T683">n-n:case</ta>
            <ta e="T685" id="Seg_10361" s="T684">v-v:tense-v:pred.pn</ta>
            <ta e="T686" id="Seg_10362" s="T685">adj-n:poss-n:case</ta>
            <ta e="T687" id="Seg_10363" s="T686">n-n:(num)-n:case</ta>
            <ta e="T688" id="Seg_10364" s="T687">n-n:case</ta>
            <ta e="T689" id="Seg_10365" s="T688">v-v:tense-v:pred.pn</ta>
            <ta e="T690" id="Seg_10366" s="T689">n-n&gt;adj-n:case</ta>
            <ta e="T691" id="Seg_10367" s="T690">v-v:mood-v:temp.pn</ta>
            <ta e="T692" id="Seg_10368" s="T691">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T693" id="Seg_10369" s="T692">adj-n:case</ta>
            <ta e="T694" id="Seg_10370" s="T693">v-v:tense-v:poss.pn</ta>
            <ta e="T695" id="Seg_10371" s="T694">v-v:tense-v:pred.pn</ta>
            <ta e="T696" id="Seg_10372" s="T695">cardnum-n&gt;n-n:case</ta>
            <ta e="T697" id="Seg_10373" s="T696">ptcl</ta>
            <ta e="T698" id="Seg_10374" s="T697">n-n:case</ta>
            <ta e="T699" id="Seg_10375" s="T698">n-n:(poss)-n:case</ta>
            <ta e="T700" id="Seg_10376" s="T699">ptcl</ta>
            <ta e="T701" id="Seg_10377" s="T700">v-v:tense-v:pred.pn</ta>
            <ta e="T702" id="Seg_10378" s="T701">n-n:case</ta>
            <ta e="T703" id="Seg_10379" s="T702">v-v:tense-v:pred.pn</ta>
            <ta e="T704" id="Seg_10380" s="T703">dempro-pro:case</ta>
            <ta e="T705" id="Seg_10381" s="T704">n-n:case</ta>
            <ta e="T706" id="Seg_10382" s="T705">adv</ta>
            <ta e="T707" id="Seg_10383" s="T706">v-v:(tense)-v:mood.pn</ta>
            <ta e="T708" id="Seg_10384" s="T707">n-n:(num)-n:case</ta>
            <ta e="T709" id="Seg_10385" s="T708">n-n:poss-n:case</ta>
            <ta e="T710" id="Seg_10386" s="T709">ptcl</ta>
            <ta e="T711" id="Seg_10387" s="T710">v-v:tense-v:poss.pn</ta>
            <ta e="T712" id="Seg_10388" s="T711">v-v:tense-v:pred.pn</ta>
            <ta e="T713" id="Seg_10389" s="T712">n-n:case</ta>
            <ta e="T714" id="Seg_10390" s="T713">v-v:tense-v:pred.pn</ta>
            <ta e="T715" id="Seg_10391" s="T714">n-n:(num)-n:case</ta>
            <ta e="T716" id="Seg_10392" s="T715">ordnum</ta>
            <ta e="T717" id="Seg_10393" s="T716">n-n:case</ta>
            <ta e="T718" id="Seg_10394" s="T717">quant</ta>
            <ta e="T719" id="Seg_10395" s="T718">n-n:case</ta>
            <ta e="T720" id="Seg_10396" s="T719">v-v&gt;v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T721" id="Seg_10397" s="T720">n-n:case</ta>
            <ta e="T722" id="Seg_10398" s="T721">v-v:tense-v:pred.pn</ta>
            <ta e="T723" id="Seg_10399" s="T722">n-n:poss-n:case-n-n:poss-n:case</ta>
            <ta e="T724" id="Seg_10400" s="T723">v-v:ptcp-v:(case)</ta>
            <ta e="T725" id="Seg_10401" s="T724">v-v:tense-v:pred.pn</ta>
            <ta e="T726" id="Seg_10402" s="T725">n-n:case</ta>
            <ta e="T727" id="Seg_10403" s="T726">v-v:tense-v:pred.pn</ta>
            <ta e="T728" id="Seg_10404" s="T727">pers-pro:case</ta>
            <ta e="T729" id="Seg_10405" s="T728">ptcl</ta>
            <ta e="T730" id="Seg_10406" s="T729">n-n:case</ta>
            <ta e="T731" id="Seg_10407" s="T730">post</ta>
            <ta e="T732" id="Seg_10408" s="T731">v-v:(ins)-v:ptcp</ta>
            <ta e="T733" id="Seg_10409" s="T732">n-n:(pred.pn)</ta>
            <ta e="T734" id="Seg_10410" s="T733">n-n:poss-n:case</ta>
            <ta e="T735" id="Seg_10411" s="T734">v-v:tense-v:pred.pn</ta>
            <ta e="T736" id="Seg_10412" s="T735">dempro</ta>
            <ta e="T737" id="Seg_10413" s="T736">n-n:case</ta>
            <ta e="T738" id="Seg_10414" s="T737">n-n:case</ta>
            <ta e="T739" id="Seg_10415" s="T738">ptcl</ta>
            <ta e="T740" id="Seg_10416" s="T739">n-n:(poss)</ta>
            <ta e="T741" id="Seg_10417" s="T740">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T742" id="Seg_10418" s="T741">n-n:(num)-n:case</ta>
            <ta e="T743" id="Seg_10419" s="T742">n-n:poss-n:case</ta>
            <ta e="T744" id="Seg_10420" s="T743">v-v:tense-v:pred.pn</ta>
            <ta e="T745" id="Seg_10421" s="T744">adj</ta>
            <ta e="T746" id="Seg_10422" s="T745">adj-n:poss-n:case</ta>
            <ta e="T747" id="Seg_10423" s="T746">n-n:(poss)-n:case</ta>
            <ta e="T748" id="Seg_10424" s="T747">adv</ta>
            <ta e="T749" id="Seg_10425" s="T748">adj-n:case</ta>
            <ta e="T750" id="Seg_10426" s="T749">n-n:(num)-n:case</ta>
            <ta e="T751" id="Seg_10427" s="T750">adj-n:(poss)-n:case</ta>
            <ta e="T752" id="Seg_10428" s="T751">adv</ta>
            <ta e="T753" id="Seg_10429" s="T752">adj-n:case</ta>
            <ta e="T754" id="Seg_10430" s="T753">n-n:case</ta>
            <ta e="T755" id="Seg_10431" s="T754">ptcl</ta>
            <ta e="T756" id="Seg_10432" s="T755">v-v:tense-v:pred.pn</ta>
            <ta e="T757" id="Seg_10433" s="T756">que-pro:case</ta>
            <ta e="T758" id="Seg_10434" s="T757">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T759" id="Seg_10435" s="T758">v-v:mood.pn</ta>
            <ta e="T760" id="Seg_10436" s="T759">adj</ta>
            <ta e="T761" id="Seg_10437" s="T760">n-n:(poss)-n:case</ta>
            <ta e="T762" id="Seg_10438" s="T761">v-v:tense-v:pred.pn</ta>
            <ta e="T763" id="Seg_10439" s="T762">ptcl</ta>
            <ta e="T764" id="Seg_10440" s="T763">n-n:(poss)-n:case</ta>
            <ta e="T765" id="Seg_10441" s="T764">pers-pro:case</ta>
            <ta e="T766" id="Seg_10442" s="T765">n-n&gt;adj-n:(pred.pn)</ta>
            <ta e="T767" id="Seg_10443" s="T766">ptcl</ta>
            <ta e="T768" id="Seg_10444" s="T767">adj</ta>
            <ta e="T769" id="Seg_10445" s="T768">n-n&gt;adj-n:case</ta>
            <ta e="T770" id="Seg_10446" s="T769">adj</ta>
            <ta e="T771" id="Seg_10447" s="T770">n-n:(poss)-n:case</ta>
            <ta e="T772" id="Seg_10448" s="T771">pers-pro:case</ta>
            <ta e="T773" id="Seg_10449" s="T772">v-v:ptcp</ta>
            <ta e="T774" id="Seg_10450" s="T773">ptcl-ptcl:(poss.pn)</ta>
            <ta e="T775" id="Seg_10451" s="T774">v-v:cvb</ta>
            <ta e="T777" id="Seg_10452" s="T776">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T778" id="Seg_10453" s="T777">n-n:case</ta>
            <ta e="T779" id="Seg_10454" s="T778">ptcl</ta>
            <ta e="T780" id="Seg_10455" s="T779">n-n:case</ta>
            <ta e="T781" id="Seg_10456" s="T780">pers-pro:case</ta>
            <ta e="T782" id="Seg_10457" s="T781">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T783" id="Seg_10458" s="T782">ptcl</ta>
            <ta e="T784" id="Seg_10459" s="T783">v-v:tense-v:pred.pn</ta>
            <ta e="T785" id="Seg_10460" s="T784">n-n:case</ta>
            <ta e="T786" id="Seg_10461" s="T785">pers-pro:case</ta>
            <ta e="T787" id="Seg_10462" s="T786">n-n&gt;v-v:ptcp</ta>
            <ta e="T788" id="Seg_10463" s="T787">n-n:case</ta>
            <ta e="T789" id="Seg_10464" s="T788">v-v:tense-v:poss.pn</ta>
            <ta e="T790" id="Seg_10465" s="T789">pers-pro:case</ta>
            <ta e="T791" id="Seg_10466" s="T790">pers-pro:case</ta>
            <ta e="T792" id="Seg_10467" s="T791">n-n:case</ta>
            <ta e="T793" id="Seg_10468" s="T792">n-n:case</ta>
            <ta e="T794" id="Seg_10469" s="T793">v-v:tense-v:poss.pn</ta>
            <ta e="T795" id="Seg_10470" s="T794">n-n:(num)-n:case</ta>
            <ta e="T796" id="Seg_10471" s="T795">adv</ta>
            <ta e="T797" id="Seg_10472" s="T796">v-v:cvb</ta>
            <ta e="T798" id="Seg_10473" s="T797">n-n&gt;v-v:(tense)-v:mood.pn</ta>
            <ta e="T799" id="Seg_10474" s="T798">v-v:tense-v:pred.pn</ta>
            <ta e="T800" id="Seg_10475" s="T799">v-v:cvb</ta>
            <ta e="T801" id="Seg_10476" s="T800">v-v:tense-v:pred.pn</ta>
            <ta e="T802" id="Seg_10477" s="T801">adv</ta>
            <ta e="T803" id="Seg_10478" s="T802">n-n:case</ta>
            <ta e="T804" id="Seg_10479" s="T803">n-n:poss-n:case</ta>
            <ta e="T805" id="Seg_10480" s="T804">v-v:cvb</ta>
            <ta e="T806" id="Seg_10481" s="T805">n-n:case</ta>
            <ta e="T807" id="Seg_10482" s="T806">v-v:tense-v:pred.pn</ta>
            <ta e="T808" id="Seg_10483" s="T807">n-n:(poss)-n:case</ta>
            <ta e="T809" id="Seg_10484" s="T808">v-v:cvb</ta>
            <ta e="T810" id="Seg_10485" s="T809">adv-adv</ta>
            <ta e="T811" id="Seg_10486" s="T810">n-n:case</ta>
            <ta e="T812" id="Seg_10487" s="T811">v-v:tense-v:pred.pn</ta>
            <ta e="T813" id="Seg_10488" s="T812">pers-pro:case</ta>
            <ta e="T814" id="Seg_10489" s="T813">ptcl-ptcl:(temp.pn)</ta>
            <ta e="T815" id="Seg_10490" s="T814">v-v:cvb</ta>
            <ta e="T816" id="Seg_10491" s="T815">v-v:tense-v:pred.pn</ta>
            <ta e="T817" id="Seg_10492" s="T816">n-n:poss-n:case</ta>
            <ta e="T818" id="Seg_10493" s="T817">post</ta>
            <ta e="T819" id="Seg_10494" s="T818">v-v:tense-v:poss.pn</ta>
            <ta e="T820" id="Seg_10495" s="T819">v-v:cvb</ta>
            <ta e="T821" id="Seg_10496" s="T820">n-n:case</ta>
            <ta e="T822" id="Seg_10497" s="T821">cardnum</ta>
            <ta e="T823" id="Seg_10498" s="T822">n-n:case</ta>
            <ta e="T824" id="Seg_10499" s="T823">v-v&gt;adv</ta>
            <ta e="T825" id="Seg_10500" s="T824">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T826" id="Seg_10501" s="T825">dempro-pro:case</ta>
            <ta e="T827" id="Seg_10502" s="T826">n-n:case</ta>
            <ta e="T828" id="Seg_10503" s="T827">v-v:tense-v:pred.pn</ta>
            <ta e="T829" id="Seg_10504" s="T828">n-n:poss-n:case</ta>
            <ta e="T830" id="Seg_10505" s="T829">v-v:tense-v:pred.pn</ta>
            <ta e="T831" id="Seg_10506" s="T830">adj</ta>
            <ta e="T832" id="Seg_10507" s="T831">n-n:poss-n:case</ta>
            <ta e="T833" id="Seg_10508" s="T832">adj</ta>
            <ta e="T834" id="Seg_10509" s="T833">n-n:poss-n:case</ta>
            <ta e="T835" id="Seg_10510" s="T834">n-n:case</ta>
            <ta e="T836" id="Seg_10511" s="T835">v-v:tense-v:pred.pn</ta>
            <ta e="T837" id="Seg_10512" s="T836">adj</ta>
            <ta e="T838" id="Seg_10513" s="T837">n-n:poss-n:case</ta>
            <ta e="T839" id="Seg_10514" s="T838">n-n:poss-n:case</ta>
            <ta e="T840" id="Seg_10515" s="T839">v-v:tense-v:pred.pn</ta>
            <ta e="T841" id="Seg_10516" s="T840">n-n:poss-n:case</ta>
            <ta e="T842" id="Seg_10517" s="T841">adj</ta>
            <ta e="T843" id="Seg_10518" s="T842">n-n:(poss)-n:case</ta>
            <ta e="T844" id="Seg_10519" s="T843">v-v:tense-v:pred.pn</ta>
            <ta e="T845" id="Seg_10520" s="T844">n-n:case</ta>
            <ta e="T846" id="Seg_10521" s="T845">dempro</ta>
            <ta e="T847" id="Seg_10522" s="T846">n-n:(poss)-n:case</ta>
            <ta e="T848" id="Seg_10523" s="T847">v-v:(ins)-v:tense-v:poss.pn</ta>
            <ta e="T849" id="Seg_10524" s="T848">adj-n:(poss)-n:case</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_10525" s="T0">n</ta>
            <ta e="T2" id="Seg_10526" s="T1">n</ta>
            <ta e="T3" id="Seg_10527" s="T2">n</ta>
            <ta e="T4" id="Seg_10528" s="T3">cop</ta>
            <ta e="T5" id="Seg_10529" s="T4">dempro</ta>
            <ta e="T6" id="Seg_10530" s="T5">n</ta>
            <ta e="T7" id="Seg_10531" s="T6">adv</ta>
            <ta e="T8" id="Seg_10532" s="T7">v</ta>
            <ta e="T9" id="Seg_10533" s="T8">adj</ta>
            <ta e="T10" id="Seg_10534" s="T9">ptcl</ta>
            <ta e="T11" id="Seg_10535" s="T10">adj</ta>
            <ta e="T12" id="Seg_10536" s="T11">conj</ta>
            <ta e="T13" id="Seg_10537" s="T12">n</ta>
            <ta e="T14" id="Seg_10538" s="T13">ptcl</ta>
            <ta e="T15" id="Seg_10539" s="T14">n</ta>
            <ta e="T16" id="Seg_10540" s="T15">adv</ta>
            <ta e="T17" id="Seg_10541" s="T16">v</ta>
            <ta e="T18" id="Seg_10542" s="T17">n</ta>
            <ta e="T19" id="Seg_10543" s="T18">n</ta>
            <ta e="T20" id="Seg_10544" s="T19">n</ta>
            <ta e="T21" id="Seg_10545" s="T20">adj</ta>
            <ta e="T22" id="Seg_10546" s="T21">n</ta>
            <ta e="T23" id="Seg_10547" s="T22">v</ta>
            <ta e="T24" id="Seg_10548" s="T23">n</ta>
            <ta e="T25" id="Seg_10549" s="T24">n</ta>
            <ta e="T26" id="Seg_10550" s="T25">v</ta>
            <ta e="T27" id="Seg_10551" s="T26">adv</ta>
            <ta e="T28" id="Seg_10552" s="T27">v</ta>
            <ta e="T29" id="Seg_10553" s="T28">v</ta>
            <ta e="T30" id="Seg_10554" s="T29">n</ta>
            <ta e="T31" id="Seg_10555" s="T30">v</ta>
            <ta e="T32" id="Seg_10556" s="T31">aux</ta>
            <ta e="T33" id="Seg_10557" s="T32">n</ta>
            <ta e="T34" id="Seg_10558" s="T33">v</ta>
            <ta e="T35" id="Seg_10559" s="T34">n</ta>
            <ta e="T36" id="Seg_10560" s="T35">v</ta>
            <ta e="T37" id="Seg_10561" s="T36">ptcl</ta>
            <ta e="T38" id="Seg_10562" s="T37">n</ta>
            <ta e="T39" id="Seg_10563" s="T38">v</ta>
            <ta e="T40" id="Seg_10564" s="T39">v</ta>
            <ta e="T41" id="Seg_10565" s="T40">n</ta>
            <ta e="T42" id="Seg_10566" s="T41">cardnum</ta>
            <ta e="T43" id="Seg_10567" s="T42">n</ta>
            <ta e="T44" id="Seg_10568" s="T43">v</ta>
            <ta e="T45" id="Seg_10569" s="T44">cardnum</ta>
            <ta e="T46" id="Seg_10570" s="T45">n</ta>
            <ta e="T47" id="Seg_10571" s="T46">n</ta>
            <ta e="T48" id="Seg_10572" s="T47">v</ta>
            <ta e="T49" id="Seg_10573" s="T48">aux</ta>
            <ta e="T50" id="Seg_10574" s="T49">adv</ta>
            <ta e="T51" id="Seg_10575" s="T50">n</ta>
            <ta e="T52" id="Seg_10576" s="T51">n</ta>
            <ta e="T53" id="Seg_10577" s="T52">ptcl</ta>
            <ta e="T54" id="Seg_10578" s="T53">v</ta>
            <ta e="T55" id="Seg_10579" s="T54">adj</ta>
            <ta e="T56" id="Seg_10580" s="T55">n</ta>
            <ta e="T57" id="Seg_10581" s="T56">v</ta>
            <ta e="T58" id="Seg_10582" s="T57">n</ta>
            <ta e="T59" id="Seg_10583" s="T58">n</ta>
            <ta e="T60" id="Seg_10584" s="T59">cardnum</ta>
            <ta e="T61" id="Seg_10585" s="T60">n</ta>
            <ta e="T62" id="Seg_10586" s="T61">post</ta>
            <ta e="T63" id="Seg_10587" s="T62">v</ta>
            <ta e="T64" id="Seg_10588" s="T63">dempro</ta>
            <ta e="T65" id="Seg_10589" s="T64">v</ta>
            <ta e="T66" id="Seg_10590" s="T65">n</ta>
            <ta e="T67" id="Seg_10591" s="T66">n</ta>
            <ta e="T68" id="Seg_10592" s="T67">n</ta>
            <ta e="T69" id="Seg_10593" s="T68">v</ta>
            <ta e="T70" id="Seg_10594" s="T69">v</ta>
            <ta e="T71" id="Seg_10595" s="T70">n</ta>
            <ta e="T72" id="Seg_10596" s="T71">post</ta>
            <ta e="T73" id="Seg_10597" s="T72">v</ta>
            <ta e="T74" id="Seg_10598" s="T73">n</ta>
            <ta e="T75" id="Seg_10599" s="T74">n</ta>
            <ta e="T76" id="Seg_10600" s="T75">v</ta>
            <ta e="T77" id="Seg_10601" s="T76">v</ta>
            <ta e="T78" id="Seg_10602" s="T77">n</ta>
            <ta e="T79" id="Seg_10603" s="T78">n</ta>
            <ta e="T80" id="Seg_10604" s="T79">v</ta>
            <ta e="T81" id="Seg_10605" s="T80">aux</ta>
            <ta e="T82" id="Seg_10606" s="T81">n</ta>
            <ta e="T83" id="Seg_10607" s="T82">n</ta>
            <ta e="T84" id="Seg_10608" s="T83">adv</ta>
            <ta e="T85" id="Seg_10609" s="T84">que</ta>
            <ta e="T86" id="Seg_10610" s="T85">conj</ta>
            <ta e="T87" id="Seg_10611" s="T86">v</ta>
            <ta e="T88" id="Seg_10612" s="T87">v</ta>
            <ta e="T89" id="Seg_10613" s="T88">v</ta>
            <ta e="T90" id="Seg_10614" s="T89">pers</ta>
            <ta e="T91" id="Seg_10615" s="T90">n</ta>
            <ta e="T92" id="Seg_10616" s="T91">ptcl</ta>
            <ta e="T93" id="Seg_10617" s="T92">ptcl</ta>
            <ta e="T94" id="Seg_10618" s="T93">v</ta>
            <ta e="T95" id="Seg_10619" s="T94">ptcl</ta>
            <ta e="T96" id="Seg_10620" s="T95">v</ta>
            <ta e="T97" id="Seg_10621" s="T96">v</ta>
            <ta e="T98" id="Seg_10622" s="T97">n</ta>
            <ta e="T99" id="Seg_10623" s="T98">n</ta>
            <ta e="T100" id="Seg_10624" s="T99">n</ta>
            <ta e="T101" id="Seg_10625" s="T100">v</ta>
            <ta e="T102" id="Seg_10626" s="T101">adj</ta>
            <ta e="T103" id="Seg_10627" s="T102">n</ta>
            <ta e="T104" id="Seg_10628" s="T103">ptcl</ta>
            <ta e="T105" id="Seg_10629" s="T104">dempro</ta>
            <ta e="T106" id="Seg_10630" s="T105">v</ta>
            <ta e="T107" id="Seg_10631" s="T106">v</ta>
            <ta e="T108" id="Seg_10632" s="T107">v</ta>
            <ta e="T109" id="Seg_10633" s="T108">n</ta>
            <ta e="T110" id="Seg_10634" s="T109">n</ta>
            <ta e="T111" id="Seg_10635" s="T110">v</ta>
            <ta e="T112" id="Seg_10636" s="T111">aux</ta>
            <ta e="T113" id="Seg_10637" s="T112">n</ta>
            <ta e="T114" id="Seg_10638" s="T113">n</ta>
            <ta e="T115" id="Seg_10639" s="T114">v</ta>
            <ta e="T116" id="Seg_10640" s="T115">v</ta>
            <ta e="T117" id="Seg_10641" s="T116">aux</ta>
            <ta e="T118" id="Seg_10642" s="T117">ptcl</ta>
            <ta e="T119" id="Seg_10643" s="T118">ptcl</ta>
            <ta e="T120" id="Seg_10644" s="T119">v</ta>
            <ta e="T121" id="Seg_10645" s="T120">n</ta>
            <ta e="T122" id="Seg_10646" s="T121">n</ta>
            <ta e="T123" id="Seg_10647" s="T122">cop</ta>
            <ta e="T124" id="Seg_10648" s="T123">v</ta>
            <ta e="T125" id="Seg_10649" s="T124">v</ta>
            <ta e="T126" id="Seg_10650" s="T125">adj</ta>
            <ta e="T127" id="Seg_10651" s="T126">n</ta>
            <ta e="T128" id="Seg_10652" s="T127">n</ta>
            <ta e="T129" id="Seg_10653" s="T128">v</ta>
            <ta e="T130" id="Seg_10654" s="T129">n</ta>
            <ta e="T131" id="Seg_10655" s="T130">cop</ta>
            <ta e="T132" id="Seg_10656" s="T131">interj</ta>
            <ta e="T133" id="Seg_10657" s="T132">dempro</ta>
            <ta e="T134" id="Seg_10658" s="T133">v</ta>
            <ta e="T135" id="Seg_10659" s="T134">aux</ta>
            <ta e="T136" id="Seg_10660" s="T135">n</ta>
            <ta e="T137" id="Seg_10661" s="T136">ptcl</ta>
            <ta e="T138" id="Seg_10662" s="T137">cardnum</ta>
            <ta e="T139" id="Seg_10663" s="T138">adj</ta>
            <ta e="T140" id="Seg_10664" s="T139">n</ta>
            <ta e="T141" id="Seg_10665" s="T140">v</ta>
            <ta e="T142" id="Seg_10666" s="T141">v</ta>
            <ta e="T143" id="Seg_10667" s="T142">aux</ta>
            <ta e="T144" id="Seg_10668" s="T143">n</ta>
            <ta e="T145" id="Seg_10669" s="T144">v</ta>
            <ta e="T146" id="Seg_10670" s="T145">dempro</ta>
            <ta e="T147" id="Seg_10671" s="T146">n</ta>
            <ta e="T148" id="Seg_10672" s="T147">n</ta>
            <ta e="T149" id="Seg_10673" s="T148">cardnum</ta>
            <ta e="T150" id="Seg_10674" s="T149">n</ta>
            <ta e="T151" id="Seg_10675" s="T150">v</ta>
            <ta e="T152" id="Seg_10676" s="T151">v</ta>
            <ta e="T153" id="Seg_10677" s="T152">v</ta>
            <ta e="T154" id="Seg_10678" s="T153">n</ta>
            <ta e="T155" id="Seg_10679" s="T154">n</ta>
            <ta e="T156" id="Seg_10680" s="T155">v</ta>
            <ta e="T157" id="Seg_10681" s="T156">cardnum</ta>
            <ta e="T158" id="Seg_10682" s="T157">n</ta>
            <ta e="T159" id="Seg_10683" s="T158">post</ta>
            <ta e="T160" id="Seg_10684" s="T159">n</ta>
            <ta e="T161" id="Seg_10685" s="T160">conj</ta>
            <ta e="T162" id="Seg_10686" s="T161">v</ta>
            <ta e="T163" id="Seg_10687" s="T162">dempro</ta>
            <ta e="T164" id="Seg_10688" s="T163">n</ta>
            <ta e="T165" id="Seg_10689" s="T164">v</ta>
            <ta e="T166" id="Seg_10690" s="T165">n</ta>
            <ta e="T167" id="Seg_10691" s="T166">adj</ta>
            <ta e="T168" id="Seg_10692" s="T167">v</ta>
            <ta e="T169" id="Seg_10693" s="T168">v</ta>
            <ta e="T170" id="Seg_10694" s="T169">aux</ta>
            <ta e="T171" id="Seg_10695" s="T170">que</ta>
            <ta e="T172" id="Seg_10696" s="T171">ptcl</ta>
            <ta e="T173" id="Seg_10697" s="T172">v</ta>
            <ta e="T174" id="Seg_10698" s="T173">n</ta>
            <ta e="T175" id="Seg_10699" s="T174">v</ta>
            <ta e="T176" id="Seg_10700" s="T175">aux</ta>
            <ta e="T177" id="Seg_10701" s="T176">aux</ta>
            <ta e="T178" id="Seg_10702" s="T177">v</ta>
            <ta e="T179" id="Seg_10703" s="T178">v</ta>
            <ta e="T180" id="Seg_10704" s="T179">v</ta>
            <ta e="T181" id="Seg_10705" s="T180">n</ta>
            <ta e="T182" id="Seg_10706" s="T181">cardnum</ta>
            <ta e="T183" id="Seg_10707" s="T182">adj</ta>
            <ta e="T184" id="Seg_10708" s="T183">adj</ta>
            <ta e="T185" id="Seg_10709" s="T184">adj</ta>
            <ta e="T186" id="Seg_10710" s="T185">adj</ta>
            <ta e="T187" id="Seg_10711" s="T186">n</ta>
            <ta e="T188" id="Seg_10712" s="T187">v</ta>
            <ta e="T189" id="Seg_10713" s="T188">n</ta>
            <ta e="T190" id="Seg_10714" s="T189">cop</ta>
            <ta e="T191" id="Seg_10715" s="T190">ptcl</ta>
            <ta e="T192" id="Seg_10716" s="T191">que</ta>
            <ta e="T193" id="Seg_10717" s="T192">v</ta>
            <ta e="T194" id="Seg_10718" s="T193">v</ta>
            <ta e="T195" id="Seg_10719" s="T194">v</ta>
            <ta e="T196" id="Seg_10720" s="T195">n</ta>
            <ta e="T197" id="Seg_10721" s="T196">que</ta>
            <ta e="T198" id="Seg_10722" s="T197">v</ta>
            <ta e="T199" id="Seg_10723" s="T198">que</ta>
            <ta e="T200" id="Seg_10724" s="T199">dempro</ta>
            <ta e="T201" id="Seg_10725" s="T200">adv</ta>
            <ta e="T202" id="Seg_10726" s="T201">cop</ta>
            <ta e="T203" id="Seg_10727" s="T202">v</ta>
            <ta e="T204" id="Seg_10728" s="T203">ptcl</ta>
            <ta e="T205" id="Seg_10729" s="T204">v</ta>
            <ta e="T206" id="Seg_10730" s="T205">n</ta>
            <ta e="T207" id="Seg_10731" s="T206">v</ta>
            <ta e="T208" id="Seg_10732" s="T207">n</ta>
            <ta e="T209" id="Seg_10733" s="T208">v</ta>
            <ta e="T210" id="Seg_10734" s="T209">v</ta>
            <ta e="T211" id="Seg_10735" s="T210">n</ta>
            <ta e="T212" id="Seg_10736" s="T211">ptcl</ta>
            <ta e="T213" id="Seg_10737" s="T212">adj</ta>
            <ta e="T214" id="Seg_10738" s="T213">n</ta>
            <ta e="T215" id="Seg_10739" s="T214">v</ta>
            <ta e="T216" id="Seg_10740" s="T215">v</ta>
            <ta e="T217" id="Seg_10741" s="T216">v</ta>
            <ta e="T218" id="Seg_10742" s="T217">n</ta>
            <ta e="T219" id="Seg_10743" s="T218">n</ta>
            <ta e="T220" id="Seg_10744" s="T219">que</ta>
            <ta e="T221" id="Seg_10745" s="T220">ptcl</ta>
            <ta e="T222" id="Seg_10746" s="T221">n</ta>
            <ta e="T223" id="Seg_10747" s="T222">n</ta>
            <ta e="T224" id="Seg_10748" s="T223">v</ta>
            <ta e="T225" id="Seg_10749" s="T224">n</ta>
            <ta e="T226" id="Seg_10750" s="T225">v</ta>
            <ta e="T227" id="Seg_10751" s="T226">n</ta>
            <ta e="T228" id="Seg_10752" s="T227">n</ta>
            <ta e="T229" id="Seg_10753" s="T228">v</ta>
            <ta e="T230" id="Seg_10754" s="T229">v</ta>
            <ta e="T231" id="Seg_10755" s="T230">n</ta>
            <ta e="T232" id="Seg_10756" s="T231">v</ta>
            <ta e="T233" id="Seg_10757" s="T232">que</ta>
            <ta e="T234" id="Seg_10758" s="T233">ptcl</ta>
            <ta e="T235" id="Seg_10759" s="T234">adj</ta>
            <ta e="T236" id="Seg_10760" s="T235">n</ta>
            <ta e="T237" id="Seg_10761" s="T236">v</ta>
            <ta e="T238" id="Seg_10762" s="T237">n</ta>
            <ta e="T239" id="Seg_10763" s="T238">n</ta>
            <ta e="T240" id="Seg_10764" s="T239">n</ta>
            <ta e="T241" id="Seg_10765" s="T240">v</ta>
            <ta e="T242" id="Seg_10766" s="T241">v</ta>
            <ta e="T243" id="Seg_10767" s="T242">aux</ta>
            <ta e="T244" id="Seg_10768" s="T243">que</ta>
            <ta e="T245" id="Seg_10769" s="T244">n</ta>
            <ta e="T246" id="Seg_10770" s="T245">v</ta>
            <ta e="T247" id="Seg_10771" s="T246">aux</ta>
            <ta e="T248" id="Seg_10772" s="T247">dempro</ta>
            <ta e="T249" id="Seg_10773" s="T248">v</ta>
            <ta e="T250" id="Seg_10774" s="T249">n</ta>
            <ta e="T251" id="Seg_10775" s="T250">v</ta>
            <ta e="T252" id="Seg_10776" s="T251">n</ta>
            <ta e="T253" id="Seg_10777" s="T252">ptcl</ta>
            <ta e="T254" id="Seg_10778" s="T253">pers</ta>
            <ta e="T255" id="Seg_10779" s="T254">n</ta>
            <ta e="T256" id="Seg_10780" s="T255">v</ta>
            <ta e="T257" id="Seg_10781" s="T256">adj</ta>
            <ta e="T258" id="Seg_10782" s="T257">n</ta>
            <ta e="T259" id="Seg_10783" s="T258">dempro</ta>
            <ta e="T260" id="Seg_10784" s="T259">post</ta>
            <ta e="T261" id="Seg_10785" s="T260">adv</ta>
            <ta e="T262" id="Seg_10786" s="T261">v</ta>
            <ta e="T263" id="Seg_10787" s="T262">dempro</ta>
            <ta e="T264" id="Seg_10788" s="T263">v</ta>
            <ta e="T265" id="Seg_10789" s="T264">v</ta>
            <ta e="T266" id="Seg_10790" s="T265">v</ta>
            <ta e="T267" id="Seg_10791" s="T266">aux</ta>
            <ta e="T268" id="Seg_10792" s="T267">v</ta>
            <ta e="T269" id="Seg_10793" s="T268">emphpro</ta>
            <ta e="T270" id="Seg_10794" s="T269">v</ta>
            <ta e="T271" id="Seg_10795" s="T270">n</ta>
            <ta e="T272" id="Seg_10796" s="T271">cop</ta>
            <ta e="T273" id="Seg_10797" s="T272">que</ta>
            <ta e="T274" id="Seg_10798" s="T273">v</ta>
            <ta e="T275" id="Seg_10799" s="T274">aux</ta>
            <ta e="T276" id="Seg_10800" s="T275">que</ta>
            <ta e="T277" id="Seg_10801" s="T276">ptcl</ta>
            <ta e="T278" id="Seg_10802" s="T277">ptcl</ta>
            <ta e="T279" id="Seg_10803" s="T278">v</ta>
            <ta e="T280" id="Seg_10804" s="T279">v</ta>
            <ta e="T281" id="Seg_10805" s="T280">interj</ta>
            <ta e="T282" id="Seg_10806" s="T281">v</ta>
            <ta e="T283" id="Seg_10807" s="T282">adj</ta>
            <ta e="T284" id="Seg_10808" s="T283">n</ta>
            <ta e="T285" id="Seg_10809" s="T284">adv</ta>
            <ta e="T286" id="Seg_10810" s="T285">pers</ta>
            <ta e="T287" id="Seg_10811" s="T286">cardnum</ta>
            <ta e="T288" id="Seg_10812" s="T287">n</ta>
            <ta e="T289" id="Seg_10813" s="T288">n</ta>
            <ta e="T290" id="Seg_10814" s="T289">v</ta>
            <ta e="T291" id="Seg_10815" s="T290">dempro</ta>
            <ta e="T292" id="Seg_10816" s="T291">cardnum</ta>
            <ta e="T293" id="Seg_10817" s="T292">adj</ta>
            <ta e="T294" id="Seg_10818" s="T293">n</ta>
            <ta e="T295" id="Seg_10819" s="T294">v</ta>
            <ta e="T296" id="Seg_10820" s="T295">adj</ta>
            <ta e="T297" id="Seg_10821" s="T296">dempro</ta>
            <ta e="T298" id="Seg_10822" s="T297">v</ta>
            <ta e="T299" id="Seg_10823" s="T298">n</ta>
            <ta e="T300" id="Seg_10824" s="T299">v</ta>
            <ta e="T301" id="Seg_10825" s="T300">v</ta>
            <ta e="T302" id="Seg_10826" s="T301">dempro</ta>
            <ta e="T303" id="Seg_10827" s="T302">v</ta>
            <ta e="T304" id="Seg_10828" s="T303">dempro</ta>
            <ta e="T305" id="Seg_10829" s="T304">n</ta>
            <ta e="T306" id="Seg_10830" s="T305">v</ta>
            <ta e="T307" id="Seg_10831" s="T306">aux</ta>
            <ta e="T308" id="Seg_10832" s="T307">pers</ta>
            <ta e="T309" id="Seg_10833" s="T308">v</ta>
            <ta e="T310" id="Seg_10834" s="T309">v</ta>
            <ta e="T311" id="Seg_10835" s="T310">ptcl</ta>
            <ta e="T312" id="Seg_10836" s="T311">adv</ta>
            <ta e="T313" id="Seg_10837" s="T312">v</ta>
            <ta e="T314" id="Seg_10838" s="T313">pers</ta>
            <ta e="T315" id="Seg_10839" s="T314">n</ta>
            <ta e="T316" id="Seg_10840" s="T315">v</ta>
            <ta e="T317" id="Seg_10841" s="T316">dempro</ta>
            <ta e="T318" id="Seg_10842" s="T317">n</ta>
            <ta e="T319" id="Seg_10843" s="T318">n</ta>
            <ta e="T320" id="Seg_10844" s="T319">v</ta>
            <ta e="T321" id="Seg_10845" s="T320">post</ta>
            <ta e="T322" id="Seg_10846" s="T321">n</ta>
            <ta e="T323" id="Seg_10847" s="T322">v</ta>
            <ta e="T324" id="Seg_10848" s="T323">v</ta>
            <ta e="T325" id="Seg_10849" s="T324">cardnum</ta>
            <ta e="T326" id="Seg_10850" s="T325">n</ta>
            <ta e="T327" id="Seg_10851" s="T326">n</ta>
            <ta e="T328" id="Seg_10852" s="T327">v</ta>
            <ta e="T329" id="Seg_10853" s="T328">v</ta>
            <ta e="T330" id="Seg_10854" s="T329">adv</ta>
            <ta e="T331" id="Seg_10855" s="T330">pers</ta>
            <ta e="T332" id="Seg_10856" s="T331">n</ta>
            <ta e="T333" id="Seg_10857" s="T332">n</ta>
            <ta e="T334" id="Seg_10858" s="T333">v</ta>
            <ta e="T335" id="Seg_10859" s="T334">ptcl</ta>
            <ta e="T336" id="Seg_10860" s="T335">pers</ta>
            <ta e="T337" id="Seg_10861" s="T336">v</ta>
            <ta e="T338" id="Seg_10862" s="T337">n</ta>
            <ta e="T339" id="Seg_10863" s="T338">v</ta>
            <ta e="T340" id="Seg_10864" s="T339">pers</ta>
            <ta e="T341" id="Seg_10865" s="T340">pers</ta>
            <ta e="T342" id="Seg_10866" s="T341">n</ta>
            <ta e="T343" id="Seg_10867" s="T342">v</ta>
            <ta e="T344" id="Seg_10868" s="T343">n</ta>
            <ta e="T345" id="Seg_10869" s="T344">adj</ta>
            <ta e="T346" id="Seg_10870" s="T345">n</ta>
            <ta e="T347" id="Seg_10871" s="T346">n</ta>
            <ta e="T348" id="Seg_10872" s="T347">v</ta>
            <ta e="T349" id="Seg_10873" s="T348">adj</ta>
            <ta e="T350" id="Seg_10874" s="T349">cop</ta>
            <ta e="T351" id="Seg_10875" s="T350">v</ta>
            <ta e="T352" id="Seg_10876" s="T351">adj</ta>
            <ta e="T353" id="Seg_10877" s="T352">n</ta>
            <ta e="T354" id="Seg_10878" s="T353">adj</ta>
            <ta e="T355" id="Seg_10879" s="T354">n</ta>
            <ta e="T356" id="Seg_10880" s="T355">v</ta>
            <ta e="T357" id="Seg_10881" s="T356">aux</ta>
            <ta e="T358" id="Seg_10882" s="T357">cardnum</ta>
            <ta e="T359" id="Seg_10883" s="T358">n</ta>
            <ta e="T360" id="Seg_10884" s="T359">adj</ta>
            <ta e="T361" id="Seg_10885" s="T360">cop</ta>
            <ta e="T362" id="Seg_10886" s="T361">aux</ta>
            <ta e="T363" id="Seg_10887" s="T362">adj</ta>
            <ta e="T364" id="Seg_10888" s="T363">n</ta>
            <ta e="T365" id="Seg_10889" s="T364">n</ta>
            <ta e="T366" id="Seg_10890" s="T365">v</ta>
            <ta e="T367" id="Seg_10891" s="T366">n</ta>
            <ta e="T368" id="Seg_10892" s="T367">n</ta>
            <ta e="T369" id="Seg_10893" s="T368">cardnum</ta>
            <ta e="T370" id="Seg_10894" s="T369">n</ta>
            <ta e="T371" id="Seg_10895" s="T370">v</ta>
            <ta e="T372" id="Seg_10896" s="T371">v</ta>
            <ta e="T373" id="Seg_10897" s="T372">emphpro</ta>
            <ta e="T374" id="Seg_10898" s="T373">v</ta>
            <ta e="T375" id="Seg_10899" s="T374">v</ta>
            <ta e="T376" id="Seg_10900" s="T375">ptcl</ta>
            <ta e="T377" id="Seg_10901" s="T376">v</ta>
            <ta e="T378" id="Seg_10902" s="T377">adj</ta>
            <ta e="T379" id="Seg_10903" s="T378">n</ta>
            <ta e="T380" id="Seg_10904" s="T379">v</ta>
            <ta e="T381" id="Seg_10905" s="T380">adj</ta>
            <ta e="T382" id="Seg_10906" s="T381">n</ta>
            <ta e="T383" id="Seg_10907" s="T382">v</ta>
            <ta e="T384" id="Seg_10908" s="T383">ordnum</ta>
            <ta e="T385" id="Seg_10909" s="T384">n</ta>
            <ta e="T386" id="Seg_10910" s="T385">adj</ta>
            <ta e="T387" id="Seg_10911" s="T386">n</ta>
            <ta e="T388" id="Seg_10912" s="T387">v</ta>
            <ta e="T389" id="Seg_10913" s="T388">n</ta>
            <ta e="T390" id="Seg_10914" s="T389">adj</ta>
            <ta e="T391" id="Seg_10915" s="T390">cardnum</ta>
            <ta e="T392" id="Seg_10916" s="T391">que</ta>
            <ta e="T393" id="Seg_10917" s="T392">n</ta>
            <ta e="T394" id="Seg_10918" s="T393">v</ta>
            <ta e="T395" id="Seg_10919" s="T394">adj</ta>
            <ta e="T396" id="Seg_10920" s="T395">n</ta>
            <ta e="T397" id="Seg_10921" s="T396">v</ta>
            <ta e="T398" id="Seg_10922" s="T397">adj</ta>
            <ta e="T399" id="Seg_10923" s="T398">n</ta>
            <ta e="T400" id="Seg_10924" s="T399">n</ta>
            <ta e="T401" id="Seg_10925" s="T400">v</ta>
            <ta e="T402" id="Seg_10926" s="T401">n</ta>
            <ta e="T403" id="Seg_10927" s="T402">cardnum</ta>
            <ta e="T404" id="Seg_10928" s="T403">n</ta>
            <ta e="T405" id="Seg_10929" s="T404">v</ta>
            <ta e="T406" id="Seg_10930" s="T405">dempro</ta>
            <ta e="T407" id="Seg_10931" s="T406">n</ta>
            <ta e="T408" id="Seg_10932" s="T407">v</ta>
            <ta e="T409" id="Seg_10933" s="T408">pers</ta>
            <ta e="T410" id="Seg_10934" s="T409">n</ta>
            <ta e="T411" id="Seg_10935" s="T410">v</ta>
            <ta e="T412" id="Seg_10936" s="T411">collnum</ta>
            <ta e="T413" id="Seg_10937" s="T412">v</ta>
            <ta e="T414" id="Seg_10938" s="T413">adj</ta>
            <ta e="T415" id="Seg_10939" s="T414">n</ta>
            <ta e="T416" id="Seg_10940" s="T415">v</ta>
            <ta e="T417" id="Seg_10941" s="T416">v</ta>
            <ta e="T418" id="Seg_10942" s="T417">v</ta>
            <ta e="T419" id="Seg_10943" s="T418">v</ta>
            <ta e="T420" id="Seg_10944" s="T419">dempro</ta>
            <ta e="T421" id="Seg_10945" s="T420">post</ta>
            <ta e="T422" id="Seg_10946" s="T421">v</ta>
            <ta e="T423" id="Seg_10947" s="T422">n</ta>
            <ta e="T424" id="Seg_10948" s="T423">adv</ta>
            <ta e="T425" id="Seg_10949" s="T424">n</ta>
            <ta e="T426" id="Seg_10950" s="T425">v</ta>
            <ta e="T427" id="Seg_10951" s="T426">v</ta>
            <ta e="T428" id="Seg_10952" s="T427">n</ta>
            <ta e="T429" id="Seg_10953" s="T428">v</ta>
            <ta e="T430" id="Seg_10954" s="T429">que</ta>
            <ta e="T431" id="Seg_10955" s="T430">n</ta>
            <ta e="T432" id="Seg_10956" s="T431">n</ta>
            <ta e="T433" id="Seg_10957" s="T432">v</ta>
            <ta e="T434" id="Seg_10958" s="T433">v</ta>
            <ta e="T435" id="Seg_10959" s="T434">adj</ta>
            <ta e="T436" id="Seg_10960" s="T435">n</ta>
            <ta e="T437" id="Seg_10961" s="T436">pers</ta>
            <ta e="T438" id="Seg_10962" s="T437">v</ta>
            <ta e="T439" id="Seg_10963" s="T438">ptcl</ta>
            <ta e="T440" id="Seg_10964" s="T439">dempro</ta>
            <ta e="T441" id="Seg_10965" s="T440">n</ta>
            <ta e="T442" id="Seg_10966" s="T441">adv</ta>
            <ta e="T443" id="Seg_10967" s="T442">pers</ta>
            <ta e="T444" id="Seg_10968" s="T443">pers</ta>
            <ta e="T445" id="Seg_10969" s="T444">v</ta>
            <ta e="T446" id="Seg_10970" s="T445">n</ta>
            <ta e="T447" id="Seg_10971" s="T446">v</ta>
            <ta e="T448" id="Seg_10972" s="T447">v</ta>
            <ta e="T449" id="Seg_10973" s="T448">adj</ta>
            <ta e="T450" id="Seg_10974" s="T449">n</ta>
            <ta e="T451" id="Seg_10975" s="T450">v</ta>
            <ta e="T452" id="Seg_10976" s="T451">n</ta>
            <ta e="T453" id="Seg_10977" s="T452">n</ta>
            <ta e="T454" id="Seg_10978" s="T453">collnum</ta>
            <ta e="T455" id="Seg_10979" s="T454">ptcl</ta>
            <ta e="T456" id="Seg_10980" s="T455">que</ta>
            <ta e="T457" id="Seg_10981" s="T456">v</ta>
            <ta e="T458" id="Seg_10982" s="T457">v</ta>
            <ta e="T459" id="Seg_10983" s="T458">adj</ta>
            <ta e="T460" id="Seg_10984" s="T459">n</ta>
            <ta e="T461" id="Seg_10985" s="T460">pers</ta>
            <ta e="T462" id="Seg_10986" s="T461">n</ta>
            <ta e="T463" id="Seg_10987" s="T462">n</ta>
            <ta e="T464" id="Seg_10988" s="T463">cop</ta>
            <ta e="T465" id="Seg_10989" s="T464">v</ta>
            <ta e="T466" id="Seg_10990" s="T465">n</ta>
            <ta e="T467" id="Seg_10991" s="T466">dempro</ta>
            <ta e="T468" id="Seg_10992" s="T467">n</ta>
            <ta e="T469" id="Seg_10993" s="T468">adj</ta>
            <ta e="T470" id="Seg_10994" s="T469">n</ta>
            <ta e="T471" id="Seg_10995" s="T470">v</ta>
            <ta e="T472" id="Seg_10996" s="T471">post</ta>
            <ta e="T473" id="Seg_10997" s="T472">n</ta>
            <ta e="T474" id="Seg_10998" s="T473">v</ta>
            <ta e="T475" id="Seg_10999" s="T474">v</ta>
            <ta e="T476" id="Seg_11000" s="T475">v</ta>
            <ta e="T477" id="Seg_11001" s="T476">pers</ta>
            <ta e="T478" id="Seg_11002" s="T477">n</ta>
            <ta e="T479" id="Seg_11003" s="T478">v</ta>
            <ta e="T480" id="Seg_11004" s="T479">v</ta>
            <ta e="T481" id="Seg_11005" s="T480">n</ta>
            <ta e="T482" id="Seg_11006" s="T481">v</ta>
            <ta e="T483" id="Seg_11007" s="T482">n</ta>
            <ta e="T484" id="Seg_11008" s="T483">v</ta>
            <ta e="T485" id="Seg_11009" s="T484">v</ta>
            <ta e="T486" id="Seg_11010" s="T485">aux</ta>
            <ta e="T487" id="Seg_11011" s="T486">ptcl</ta>
            <ta e="T488" id="Seg_11012" s="T487">que</ta>
            <ta e="T489" id="Seg_11013" s="T488">v</ta>
            <ta e="T490" id="Seg_11014" s="T489">v</ta>
            <ta e="T491" id="Seg_11015" s="T490">cardnum</ta>
            <ta e="T492" id="Seg_11016" s="T491">n</ta>
            <ta e="T493" id="Seg_11017" s="T492">v</ta>
            <ta e="T494" id="Seg_11018" s="T493">adv</ta>
            <ta e="T495" id="Seg_11019" s="T494">v</ta>
            <ta e="T496" id="Seg_11020" s="T495">n</ta>
            <ta e="T497" id="Seg_11021" s="T496">v</ta>
            <ta e="T498" id="Seg_11022" s="T497">v</ta>
            <ta e="T499" id="Seg_11023" s="T498">aux</ta>
            <ta e="T500" id="Seg_11024" s="T499">adv</ta>
            <ta e="T501" id="Seg_11025" s="T500">adj</ta>
            <ta e="T502" id="Seg_11026" s="T501">n</ta>
            <ta e="T503" id="Seg_11027" s="T502">v</ta>
            <ta e="T504" id="Seg_11028" s="T503">adj</ta>
            <ta e="T505" id="Seg_11029" s="T504">n</ta>
            <ta e="T506" id="Seg_11030" s="T505">n</ta>
            <ta e="T507" id="Seg_11031" s="T506">v</ta>
            <ta e="T508" id="Seg_11032" s="T507">aux</ta>
            <ta e="T509" id="Seg_11033" s="T508">cardnum</ta>
            <ta e="T510" id="Seg_11034" s="T509">n</ta>
            <ta e="T511" id="Seg_11035" s="T510">n</ta>
            <ta e="T512" id="Seg_11036" s="T511">v</ta>
            <ta e="T513" id="Seg_11037" s="T512">n</ta>
            <ta e="T514" id="Seg_11038" s="T513">v</ta>
            <ta e="T515" id="Seg_11039" s="T514">n</ta>
            <ta e="T516" id="Seg_11040" s="T515">n</ta>
            <ta e="T517" id="Seg_11041" s="T516">adj</ta>
            <ta e="T518" id="Seg_11042" s="T517">v</ta>
            <ta e="T519" id="Seg_11043" s="T518">n</ta>
            <ta e="T520" id="Seg_11044" s="T519">ptcl</ta>
            <ta e="T521" id="Seg_11045" s="T520">pers</ta>
            <ta e="T522" id="Seg_11046" s="T521">pers</ta>
            <ta e="T523" id="Seg_11047" s="T522">v</ta>
            <ta e="T524" id="Seg_11048" s="T523">pers</ta>
            <ta e="T525" id="Seg_11049" s="T524">v</ta>
            <ta e="T526" id="Seg_11050" s="T525">adj</ta>
            <ta e="T527" id="Seg_11051" s="T526">n</ta>
            <ta e="T528" id="Seg_11052" s="T527">n</ta>
            <ta e="T529" id="Seg_11053" s="T528">v</ta>
            <ta e="T530" id="Seg_11054" s="T529">n</ta>
            <ta e="T531" id="Seg_11055" s="T530">v</ta>
            <ta e="T532" id="Seg_11056" s="T531">adv</ta>
            <ta e="T533" id="Seg_11057" s="T532">v</ta>
            <ta e="T534" id="Seg_11058" s="T533">n</ta>
            <ta e="T535" id="Seg_11059" s="T534">n</ta>
            <ta e="T536" id="Seg_11060" s="T535">n</ta>
            <ta e="T537" id="Seg_11061" s="T536">v</ta>
            <ta e="T538" id="Seg_11062" s="T537">n</ta>
            <ta e="T539" id="Seg_11063" s="T538">ptcl</ta>
            <ta e="T540" id="Seg_11064" s="T539">n</ta>
            <ta e="T541" id="Seg_11065" s="T540">n</ta>
            <ta e="T542" id="Seg_11066" s="T541">v</ta>
            <ta e="T543" id="Seg_11067" s="T542">v</ta>
            <ta e="T544" id="Seg_11068" s="T543">adv</ta>
            <ta e="T545" id="Seg_11069" s="T544">ptcl</ta>
            <ta e="T546" id="Seg_11070" s="T545">adv</ta>
            <ta e="T547" id="Seg_11071" s="T546">v</ta>
            <ta e="T548" id="Seg_11072" s="T547">v</ta>
            <ta e="T549" id="Seg_11073" s="T548">n</ta>
            <ta e="T550" id="Seg_11074" s="T549">n</ta>
            <ta e="T551" id="Seg_11075" s="T550">v</ta>
            <ta e="T552" id="Seg_11076" s="T551">n</ta>
            <ta e="T553" id="Seg_11077" s="T552">n</ta>
            <ta e="T554" id="Seg_11078" s="T553">ptcl</ta>
            <ta e="T555" id="Seg_11079" s="T554">adj</ta>
            <ta e="T556" id="Seg_11080" s="T555">n</ta>
            <ta e="T557" id="Seg_11081" s="T556">v</ta>
            <ta e="T558" id="Seg_11082" s="T557">aux</ta>
            <ta e="T559" id="Seg_11083" s="T558">n</ta>
            <ta e="T560" id="Seg_11084" s="T559">v</ta>
            <ta e="T561" id="Seg_11085" s="T560">conj</ta>
            <ta e="T562" id="Seg_11086" s="T561">v</ta>
            <ta e="T563" id="Seg_11087" s="T562">aux</ta>
            <ta e="T564" id="Seg_11088" s="T563">n</ta>
            <ta e="T565" id="Seg_11089" s="T564">post</ta>
            <ta e="T566" id="Seg_11090" s="T565">n</ta>
            <ta e="T567" id="Seg_11091" s="T566">ptcl</ta>
            <ta e="T568" id="Seg_11092" s="T567">n</ta>
            <ta e="T569" id="Seg_11093" s="T568">n</ta>
            <ta e="T570" id="Seg_11094" s="T569">ptcl</ta>
            <ta e="T571" id="Seg_11095" s="T570">v</ta>
            <ta e="T572" id="Seg_11096" s="T571">n</ta>
            <ta e="T573" id="Seg_11097" s="T572">adj</ta>
            <ta e="T574" id="Seg_11098" s="T573">adv</ta>
            <ta e="T575" id="Seg_11099" s="T574">adj</ta>
            <ta e="T576" id="Seg_11100" s="T575">ptcl</ta>
            <ta e="T577" id="Seg_11101" s="T576">v</ta>
            <ta e="T578" id="Seg_11102" s="T577">n</ta>
            <ta e="T579" id="Seg_11103" s="T578">n</ta>
            <ta e="T580" id="Seg_11104" s="T579">n</ta>
            <ta e="T581" id="Seg_11105" s="T580">que</ta>
            <ta e="T582" id="Seg_11106" s="T581">v</ta>
            <ta e="T583" id="Seg_11107" s="T582">que</ta>
            <ta e="T584" id="Seg_11108" s="T583">n</ta>
            <ta e="T585" id="Seg_11109" s="T584">v</ta>
            <ta e="T586" id="Seg_11110" s="T585">n</ta>
            <ta e="T587" id="Seg_11111" s="T586">post</ta>
            <ta e="T588" id="Seg_11112" s="T587">n</ta>
            <ta e="T589" id="Seg_11113" s="T588">ptcl</ta>
            <ta e="T590" id="Seg_11114" s="T589">ptcl</ta>
            <ta e="T591" id="Seg_11115" s="T590">n</ta>
            <ta e="T592" id="Seg_11116" s="T591">n</ta>
            <ta e="T593" id="Seg_11117" s="T592">n</ta>
            <ta e="T594" id="Seg_11118" s="T593">v</ta>
            <ta e="T595" id="Seg_11119" s="T594">adj</ta>
            <ta e="T596" id="Seg_11120" s="T595">n</ta>
            <ta e="T597" id="Seg_11121" s="T596">v</ta>
            <ta e="T598" id="Seg_11122" s="T597">post</ta>
            <ta e="T599" id="Seg_11123" s="T598">n</ta>
            <ta e="T600" id="Seg_11124" s="T599">v</ta>
            <ta e="T601" id="Seg_11125" s="T600">pers</ta>
            <ta e="T602" id="Seg_11126" s="T601">adj</ta>
            <ta e="T603" id="Seg_11127" s="T602">n</ta>
            <ta e="T604" id="Seg_11128" s="T603">v</ta>
            <ta e="T605" id="Seg_11129" s="T604">n</ta>
            <ta e="T606" id="Seg_11130" s="T605">n</ta>
            <ta e="T607" id="Seg_11131" s="T606">v</ta>
            <ta e="T608" id="Seg_11132" s="T607">aux</ta>
            <ta e="T609" id="Seg_11133" s="T608">cardnum</ta>
            <ta e="T610" id="Seg_11134" s="T609">n</ta>
            <ta e="T611" id="Seg_11135" s="T610">n</ta>
            <ta e="T612" id="Seg_11136" s="T611">v</ta>
            <ta e="T613" id="Seg_11137" s="T612">n</ta>
            <ta e="T614" id="Seg_11138" s="T613">v</ta>
            <ta e="T615" id="Seg_11139" s="T614">v</ta>
            <ta e="T616" id="Seg_11140" s="T615">n</ta>
            <ta e="T617" id="Seg_11141" s="T616">que</ta>
            <ta e="T618" id="Seg_11142" s="T617">ptcl</ta>
            <ta e="T619" id="Seg_11143" s="T618">ptcl</ta>
            <ta e="T620" id="Seg_11144" s="T619">n</ta>
            <ta e="T621" id="Seg_11145" s="T620">v</ta>
            <ta e="T622" id="Seg_11146" s="T621">n</ta>
            <ta e="T623" id="Seg_11147" s="T622">n</ta>
            <ta e="T624" id="Seg_11148" s="T623">v</ta>
            <ta e="T625" id="Seg_11149" s="T624">aux</ta>
            <ta e="T626" id="Seg_11150" s="T625">cardnum</ta>
            <ta e="T627" id="Seg_11151" s="T626">n</ta>
            <ta e="T628" id="Seg_11152" s="T627">n</ta>
            <ta e="T629" id="Seg_11153" s="T628">v</ta>
            <ta e="T630" id="Seg_11154" s="T629">aux</ta>
            <ta e="T631" id="Seg_11155" s="T630">v</ta>
            <ta e="T632" id="Seg_11156" s="T631">aux</ta>
            <ta e="T633" id="Seg_11157" s="T632">cardnum</ta>
            <ta e="T634" id="Seg_11158" s="T633">n</ta>
            <ta e="T635" id="Seg_11159" s="T634">v</ta>
            <ta e="T636" id="Seg_11160" s="T635">n</ta>
            <ta e="T637" id="Seg_11161" s="T636">v</ta>
            <ta e="T638" id="Seg_11162" s="T637">n</ta>
            <ta e="T639" id="Seg_11163" s="T638">v</ta>
            <ta e="T640" id="Seg_11164" s="T639">adv</ta>
            <ta e="T641" id="Seg_11165" s="T640">ptcl</ta>
            <ta e="T642" id="Seg_11166" s="T641">v</ta>
            <ta e="T643" id="Seg_11167" s="T642">n</ta>
            <ta e="T644" id="Seg_11168" s="T643">v</ta>
            <ta e="T645" id="Seg_11169" s="T644">n</ta>
            <ta e="T646" id="Seg_11170" s="T645">n</ta>
            <ta e="T647" id="Seg_11171" s="T646">n</ta>
            <ta e="T648" id="Seg_11172" s="T647">n</ta>
            <ta e="T649" id="Seg_11173" s="T648">v</ta>
            <ta e="T650" id="Seg_11174" s="T649">v</ta>
            <ta e="T651" id="Seg_11175" s="T650">v</ta>
            <ta e="T652" id="Seg_11176" s="T651">aux</ta>
            <ta e="T653" id="Seg_11177" s="T652">n</ta>
            <ta e="T654" id="Seg_11178" s="T653">n</ta>
            <ta e="T655" id="Seg_11179" s="T654">v</ta>
            <ta e="T656" id="Seg_11180" s="T655">adj</ta>
            <ta e="T657" id="Seg_11181" s="T656">n</ta>
            <ta e="T658" id="Seg_11182" s="T657">v</ta>
            <ta e="T659" id="Seg_11183" s="T658">n</ta>
            <ta e="T660" id="Seg_11184" s="T659">v</ta>
            <ta e="T661" id="Seg_11185" s="T660">v</ta>
            <ta e="T662" id="Seg_11186" s="T661">aux</ta>
            <ta e="T663" id="Seg_11187" s="T662">n</ta>
            <ta e="T664" id="Seg_11188" s="T663">interj</ta>
            <ta e="T665" id="Seg_11189" s="T664">que</ta>
            <ta e="T666" id="Seg_11190" s="T665">v</ta>
            <ta e="T667" id="Seg_11191" s="T666">v</ta>
            <ta e="T668" id="Seg_11192" s="T667">n</ta>
            <ta e="T669" id="Seg_11193" s="T668">v</ta>
            <ta e="T670" id="Seg_11194" s="T669">n</ta>
            <ta e="T671" id="Seg_11195" s="T670">v</ta>
            <ta e="T672" id="Seg_11196" s="T671">v</ta>
            <ta e="T673" id="Seg_11197" s="T672">n</ta>
            <ta e="T674" id="Seg_11198" s="T673">v</ta>
            <ta e="T675" id="Seg_11199" s="T674">adj</ta>
            <ta e="T676" id="Seg_11200" s="T675">emphpro</ta>
            <ta e="T677" id="Seg_11201" s="T676">cop</ta>
            <ta e="T678" id="Seg_11202" s="T677">n</ta>
            <ta e="T679" id="Seg_11203" s="T678">v</ta>
            <ta e="T680" id="Seg_11204" s="T679">n</ta>
            <ta e="T681" id="Seg_11205" s="T680">ptcl</ta>
            <ta e="T682" id="Seg_11206" s="T681">v</ta>
            <ta e="T683" id="Seg_11207" s="T682">ptcl</ta>
            <ta e="T684" id="Seg_11208" s="T683">n</ta>
            <ta e="T685" id="Seg_11209" s="T684">v</ta>
            <ta e="T686" id="Seg_11210" s="T685">adj</ta>
            <ta e="T687" id="Seg_11211" s="T686">n</ta>
            <ta e="T688" id="Seg_11212" s="T687">n</ta>
            <ta e="T689" id="Seg_11213" s="T688">v</ta>
            <ta e="T690" id="Seg_11214" s="T689">adj</ta>
            <ta e="T691" id="Seg_11215" s="T690">cop</ta>
            <ta e="T692" id="Seg_11216" s="T691">n</ta>
            <ta e="T693" id="Seg_11217" s="T692">adj</ta>
            <ta e="T694" id="Seg_11218" s="T693">cop</ta>
            <ta e="T695" id="Seg_11219" s="T694">v</ta>
            <ta e="T696" id="Seg_11220" s="T695">cardnum</ta>
            <ta e="T697" id="Seg_11221" s="T696">ptcl</ta>
            <ta e="T698" id="Seg_11222" s="T697">n</ta>
            <ta e="T699" id="Seg_11223" s="T698">n</ta>
            <ta e="T700" id="Seg_11224" s="T699">ptcl</ta>
            <ta e="T701" id="Seg_11225" s="T700">cop</ta>
            <ta e="T702" id="Seg_11226" s="T701">n</ta>
            <ta e="T703" id="Seg_11227" s="T702">v</ta>
            <ta e="T704" id="Seg_11228" s="T703">dempro</ta>
            <ta e="T705" id="Seg_11229" s="T704">n</ta>
            <ta e="T706" id="Seg_11230" s="T705">adv</ta>
            <ta e="T707" id="Seg_11231" s="T706">v</ta>
            <ta e="T708" id="Seg_11232" s="T707">n</ta>
            <ta e="T709" id="Seg_11233" s="T708">n</ta>
            <ta e="T710" id="Seg_11234" s="T709">ptcl</ta>
            <ta e="T711" id="Seg_11235" s="T710">v</ta>
            <ta e="T712" id="Seg_11236" s="T711">v</ta>
            <ta e="T713" id="Seg_11237" s="T712">n</ta>
            <ta e="T714" id="Seg_11238" s="T713">v</ta>
            <ta e="T715" id="Seg_11239" s="T714">n</ta>
            <ta e="T716" id="Seg_11240" s="T715">cardnum</ta>
            <ta e="T717" id="Seg_11241" s="T716">n</ta>
            <ta e="T718" id="Seg_11242" s="T717">quant</ta>
            <ta e="T719" id="Seg_11243" s="T718">n</ta>
            <ta e="T720" id="Seg_11244" s="T719">v</ta>
            <ta e="T721" id="Seg_11245" s="T720">n</ta>
            <ta e="T722" id="Seg_11246" s="T721">v</ta>
            <ta e="T723" id="Seg_11247" s="T722">n</ta>
            <ta e="T724" id="Seg_11248" s="T723">v</ta>
            <ta e="T725" id="Seg_11249" s="T724">v</ta>
            <ta e="T726" id="Seg_11250" s="T725">n</ta>
            <ta e="T727" id="Seg_11251" s="T726">v</ta>
            <ta e="T728" id="Seg_11252" s="T727">pers</ta>
            <ta e="T729" id="Seg_11253" s="T728">ptcl</ta>
            <ta e="T730" id="Seg_11254" s="T729">n</ta>
            <ta e="T731" id="Seg_11255" s="T730">post</ta>
            <ta e="T732" id="Seg_11256" s="T731">v</ta>
            <ta e="T733" id="Seg_11257" s="T732">n</ta>
            <ta e="T734" id="Seg_11258" s="T733">n</ta>
            <ta e="T735" id="Seg_11259" s="T734">v</ta>
            <ta e="T736" id="Seg_11260" s="T735">dempro</ta>
            <ta e="T737" id="Seg_11261" s="T736">n</ta>
            <ta e="T738" id="Seg_11262" s="T737">n</ta>
            <ta e="T739" id="Seg_11263" s="T738">ptcl</ta>
            <ta e="T740" id="Seg_11264" s="T739">n</ta>
            <ta e="T741" id="Seg_11265" s="T740">ptcl</ta>
            <ta e="T742" id="Seg_11266" s="T741">n</ta>
            <ta e="T743" id="Seg_11267" s="T742">n</ta>
            <ta e="T744" id="Seg_11268" s="T743">v</ta>
            <ta e="T745" id="Seg_11269" s="T744">adj</ta>
            <ta e="T746" id="Seg_11270" s="T745">n</ta>
            <ta e="T747" id="Seg_11271" s="T746">n</ta>
            <ta e="T748" id="Seg_11272" s="T747">adv</ta>
            <ta e="T749" id="Seg_11273" s="T748">adj</ta>
            <ta e="T750" id="Seg_11274" s="T749">n</ta>
            <ta e="T751" id="Seg_11275" s="T750">adj</ta>
            <ta e="T752" id="Seg_11276" s="T751">adv</ta>
            <ta e="T753" id="Seg_11277" s="T752">adj</ta>
            <ta e="T754" id="Seg_11278" s="T753">n</ta>
            <ta e="T755" id="Seg_11279" s="T754">ptcl</ta>
            <ta e="T756" id="Seg_11280" s="T755">v</ta>
            <ta e="T757" id="Seg_11281" s="T756">que</ta>
            <ta e="T758" id="Seg_11282" s="T757">v</ta>
            <ta e="T759" id="Seg_11283" s="T758">v</ta>
            <ta e="T760" id="Seg_11284" s="T759">adj</ta>
            <ta e="T761" id="Seg_11285" s="T760">n</ta>
            <ta e="T762" id="Seg_11286" s="T761">v</ta>
            <ta e="T763" id="Seg_11287" s="T762">ptcl</ta>
            <ta e="T764" id="Seg_11288" s="T763">n</ta>
            <ta e="T765" id="Seg_11289" s="T764">pers</ta>
            <ta e="T766" id="Seg_11290" s="T765">adj</ta>
            <ta e="T767" id="Seg_11291" s="T766">ptcl</ta>
            <ta e="T768" id="Seg_11292" s="T767">adj</ta>
            <ta e="T769" id="Seg_11293" s="T768">adj</ta>
            <ta e="T770" id="Seg_11294" s="T769">adj</ta>
            <ta e="T771" id="Seg_11295" s="T770">n</ta>
            <ta e="T772" id="Seg_11296" s="T771">pers</ta>
            <ta e="T773" id="Seg_11297" s="T772">v</ta>
            <ta e="T774" id="Seg_11298" s="T773">ptcl</ta>
            <ta e="T775" id="Seg_11299" s="T774">v</ta>
            <ta e="T777" id="Seg_11300" s="T776">v</ta>
            <ta e="T778" id="Seg_11301" s="T777">n</ta>
            <ta e="T779" id="Seg_11302" s="T778">ptcl</ta>
            <ta e="T780" id="Seg_11303" s="T779">n</ta>
            <ta e="T781" id="Seg_11304" s="T780">pers</ta>
            <ta e="T782" id="Seg_11305" s="T781">n</ta>
            <ta e="T783" id="Seg_11306" s="T782">ptcl</ta>
            <ta e="T784" id="Seg_11307" s="T783">cop</ta>
            <ta e="T785" id="Seg_11308" s="T784">n</ta>
            <ta e="T786" id="Seg_11309" s="T785">pers</ta>
            <ta e="T787" id="Seg_11310" s="T786">v</ta>
            <ta e="T788" id="Seg_11311" s="T787">n</ta>
            <ta e="T789" id="Seg_11312" s="T788">v</ta>
            <ta e="T790" id="Seg_11313" s="T789">pers</ta>
            <ta e="T791" id="Seg_11314" s="T790">pers</ta>
            <ta e="T792" id="Seg_11315" s="T791">n</ta>
            <ta e="T793" id="Seg_11316" s="T792">n</ta>
            <ta e="T794" id="Seg_11317" s="T793">v</ta>
            <ta e="T795" id="Seg_11318" s="T794">n</ta>
            <ta e="T796" id="Seg_11319" s="T795">adv</ta>
            <ta e="T797" id="Seg_11320" s="T796">v</ta>
            <ta e="T798" id="Seg_11321" s="T797">v</ta>
            <ta e="T799" id="Seg_11322" s="T798">v</ta>
            <ta e="T800" id="Seg_11323" s="T799">v</ta>
            <ta e="T801" id="Seg_11324" s="T800">aux</ta>
            <ta e="T802" id="Seg_11325" s="T801">adv</ta>
            <ta e="T803" id="Seg_11326" s="T802">n</ta>
            <ta e="T804" id="Seg_11327" s="T803">n</ta>
            <ta e="T805" id="Seg_11328" s="T804">v</ta>
            <ta e="T806" id="Seg_11329" s="T805">n</ta>
            <ta e="T807" id="Seg_11330" s="T806">cop</ta>
            <ta e="T808" id="Seg_11331" s="T807">n</ta>
            <ta e="T809" id="Seg_11332" s="T808">v</ta>
            <ta e="T810" id="Seg_11333" s="T809">adv</ta>
            <ta e="T811" id="Seg_11334" s="T810">n</ta>
            <ta e="T812" id="Seg_11335" s="T811">cop</ta>
            <ta e="T813" id="Seg_11336" s="T812">pers</ta>
            <ta e="T814" id="Seg_11337" s="T813">ptcl</ta>
            <ta e="T815" id="Seg_11338" s="T814">v</ta>
            <ta e="T816" id="Seg_11339" s="T815">v</ta>
            <ta e="T817" id="Seg_11340" s="T816">n</ta>
            <ta e="T818" id="Seg_11341" s="T817">post</ta>
            <ta e="T819" id="Seg_11342" s="T818">cop</ta>
            <ta e="T820" id="Seg_11343" s="T819">v</ta>
            <ta e="T821" id="Seg_11344" s="T820">n</ta>
            <ta e="T822" id="Seg_11345" s="T821">cardnum</ta>
            <ta e="T823" id="Seg_11346" s="T822">n</ta>
            <ta e="T824" id="Seg_11347" s="T823">adv</ta>
            <ta e="T825" id="Seg_11348" s="T824">v</ta>
            <ta e="T826" id="Seg_11349" s="T825">dempro</ta>
            <ta e="T827" id="Seg_11350" s="T826">n</ta>
            <ta e="T828" id="Seg_11351" s="T827">v</ta>
            <ta e="T829" id="Seg_11352" s="T828">n</ta>
            <ta e="T830" id="Seg_11353" s="T829">v</ta>
            <ta e="T831" id="Seg_11354" s="T830">adj</ta>
            <ta e="T832" id="Seg_11355" s="T831">n</ta>
            <ta e="T833" id="Seg_11356" s="T832">adj</ta>
            <ta e="T834" id="Seg_11357" s="T833">n</ta>
            <ta e="T835" id="Seg_11358" s="T834">n</ta>
            <ta e="T836" id="Seg_11359" s="T835">v</ta>
            <ta e="T837" id="Seg_11360" s="T836">adj</ta>
            <ta e="T838" id="Seg_11361" s="T837">n</ta>
            <ta e="T839" id="Seg_11362" s="T838">n</ta>
            <ta e="T840" id="Seg_11363" s="T839">v</ta>
            <ta e="T841" id="Seg_11364" s="T840">n</ta>
            <ta e="T842" id="Seg_11365" s="T841">adj</ta>
            <ta e="T843" id="Seg_11366" s="T842">n</ta>
            <ta e="T844" id="Seg_11367" s="T843">v</ta>
            <ta e="T845" id="Seg_11368" s="T844">n</ta>
            <ta e="T846" id="Seg_11369" s="T845">dempro</ta>
            <ta e="T847" id="Seg_11370" s="T846">n</ta>
            <ta e="T848" id="Seg_11371" s="T847">v</ta>
            <ta e="T849" id="Seg_11372" s="T848">adj</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR" />
         <annotation name="SyF" tierref="SyF" />
         <annotation name="IST" tierref="IST" />
         <annotation name="Top" tierref="Top" />
         <annotation name="Foc" tierref="Foc" />
         <annotation name="BOR" tierref="BOR" />
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_11373" s="T0">There are apparently [people] who have a czar, a master or a prince.</ta>
            <ta e="T10" id="Seg_11374" s="T4">This man lives alone, he is very brave.</ta>
            <ta e="T14" id="Seg_11375" s="T10">He is all alone and doesn't have any children.</ta>
            <ta e="T18" id="Seg_11376" s="T14">Once a year he pays taxes.</ta>
            <ta e="T26" id="Seg_11377" s="T18">On the seashore, in the very tundra, he lives and heats with driftwood. </ta>
            <ta e="T29" id="Seg_11378" s="T26">He lived long and turned old.</ta>
            <ta e="T32" id="Seg_11379" s="T29">Spring came.</ta>
            <ta e="T34" id="Seg_11380" s="T32">He said to his old woman: </ta>
            <ta e="T37" id="Seg_11381" s="T34">"It became warmer.</ta>
            <ta e="T41" id="Seg_11382" s="T37">I will go and fetch wood for the coming year.</ta>
            <ta e="T44" id="Seg_11383" s="T41">Prepare thirty sledges.</ta>
            <ta e="T50" id="Seg_11384" s="T44">On those thirty sledges he went all alone to the sea.</ta>
            <ta e="T54" id="Seg_11385" s="T50">He reached the sea through a very long cape.</ta>
            <ta e="T63" id="Seg_11386" s="T54">In that lonely place he loaded the sledges and through a riverbed he came back to the high shore.</ta>
            <ta e="T70" id="Seg_11387" s="T63">He came up the mountain, sat down and started smoking.</ta>
            <ta e="T81" id="Seg_11388" s="T70">He looked to the river, a child was walking along the water, playing with the foam of the melted water.</ta>
            <ta e="T84" id="Seg_11389" s="T81">It was apparentely a boy.</ta>
            <ta e="T88" id="Seg_11390" s="T84">Where he came from was unknown.</ta>
            <ta e="T89" id="Seg_11391" s="T88">He got surprised.</ta>
            <ta e="T93" id="Seg_11392" s="T89">"Well, I don't have a child!</ta>
            <ta e="T97" id="Seg_11393" s="T93">Maybe I should take him", he thought.</ta>
            <ta e="T106" id="Seg_11394" s="T97">The child went to a sandy ait, there was a crooked tree there, it climbed on it.</ta>
            <ta e="T117" id="Seg_11395" s="T106">The old man jumped, grabbed him from behind, stuck him into his coat and girded.</ta>
            <ta e="T120" id="Seg_11396" s="T117">Well, he hit the road again.</ta>
            <ta e="T123" id="Seg_11397" s="T120">A mumbling came up (?).</ta>
            <ta e="T124" id="Seg_11398" s="T123">He stopped.</ta>
            <ta e="T131" id="Seg_11399" s="T124">He saw that a whole settlement of humans was dead.</ta>
            <ta e="T137" id="Seg_11400" s="T131">"Hey, this child must be the one to remain."</ta>
            <ta e="T143" id="Seg_11401" s="T137">He got the front reindeer and killed it: </ta>
            <ta e="T152" id="Seg_11402" s="T143">"I took your child, as a gift for your child I left one reindeer here", he said.</ta>
            <ta e="T156" id="Seg_11403" s="T152">He brought him home and told [everything] to his old woman.</ta>
            <ta e="T165" id="Seg_11404" s="T156">Three days long they played with the child without releasing their reindeers.</ta>
            <ta e="T170" id="Seg_11405" s="T165">The whole caravan lied still harnessed there.</ta>
            <ta e="T173" id="Seg_11406" s="T170">Nobody knew about it.</ta>
            <ta e="T178" id="Seg_11407" s="T173">The child grew, it started to walk.</ta>
            <ta e="T188" id="Seg_11408" s="T178">Once sitting, he [=the man] saw a human in white clothes driving a sledge harnessed with five white reindeer bulls.</ta>
            <ta e="T190" id="Seg_11409" s="T188">It was a prince.</ta>
            <ta e="T195" id="Seg_11410" s="T190">"Well, what will I give to him?", he thought.</ta>
            <ta e="T198" id="Seg_11411" s="T195">"Old man, how is your life?</ta>
            <ta e="T206" id="Seg_11412" s="T198">How long haven't I been here that you had received a child?", said the prince.</ta>
            <ta e="T211" id="Seg_11413" s="T206">"In our old age we've got a child!", said the old man.</ta>
            <ta e="T213" id="Seg_11414" s="T211">"Well, good.</ta>
            <ta e="T218" id="Seg_11415" s="T213">I came to gather taxes", said the prince.</ta>
            <ta e="T224" id="Seg_11416" s="T218">The old man gave several sacks with polar foxes fell.</ta>
            <ta e="T230" id="Seg_11417" s="T224">The prince was happy, "for the rest I will send you money", he said.</ta>
            <ta e="T232" id="Seg_11418" s="T230">The prince hit the road.</ta>
            <ta e="T238" id="Seg_11419" s="T232">He gallopped somewhere to his friend, the Nenets prince.</ta>
            <ta e="T241" id="Seg_11420" s="T238">One prince came to another prince.</ta>
            <ta e="T243" id="Seg_11421" s="T241">They started drinking.</ta>
            <ta e="T247" id="Seg_11422" s="T243">The were drinking several days long.</ta>
            <ta e="T252" id="Seg_11423" s="T247">All the while he told his friend: </ta>
            <ta e="T257" id="Seg_11424" s="T252">"Well, I have a dangerous human.</ta>
            <ta e="T262" id="Seg_11425" s="T257">He keeps paying taxes.</ta>
            <ta e="T264" id="Seg_11426" s="T262">I come from him.</ta>
            <ta e="T268" id="Seg_11427" s="T264">I am surprised, he received a child that already walks.</ta>
            <ta e="T272" id="Seg_11428" s="T268">It is not his own child.</ta>
            <ta e="T275" id="Seg_11429" s="T272">Where did he take it from?</ta>
            <ta e="T280" id="Seg_11430" s="T275">Maybe he killed somebody and took it?!"</ta>
            <ta e="T290" id="Seg_11431" s="T280">"Ah", said the Nenets prince, "One of my settlements died long ago.</ta>
            <ta e="T296" id="Seg_11432" s="T290">Only one newborn child stayed alive, they say.</ta>
            <ta e="T301" id="Seg_11433" s="T296">I told them not to take this child, thinking that he would spread the disease.</ta>
            <ta e="T303" id="Seg_11434" s="T301">He took it, apparently.</ta>
            <ta e="T307" id="Seg_11435" s="T303">He hid that child, apparently.</ta>
            <ta e="T310" id="Seg_11436" s="T307">Thinking: "They will kill me".</ta>
            <ta e="T312" id="Seg_11437" s="T310">Well, that's what.</ta>
            <ta e="T316" id="Seg_11438" s="T312">I advise you not to be afraid of that human.</ta>
            <ta e="T329" id="Seg_11439" s="T316">Let us write a complaint to the czar about him having killed that settlement and having taken the child.</ta>
            <ta e="T346" id="Seg_11440" s="T329">The czar won't deny our words, he will judge him and send him to prison and we will take his wealth", said the Nenets prince.</ta>
            <ta e="T353" id="Seg_11441" s="T346">"Woudn't it be bad to slander a human?", said the Nganasan prince.</ta>
            <ta e="T362" id="Seg_11442" s="T353">"We should call the Dolgan prince, three princes would be trustworthier."</ta>
            <ta e="T364" id="Seg_11443" s="T362">The Nenets prince [said]: </ta>
            <ta e="T371" id="Seg_11444" s="T364">"I will send a human for the Dolgan prince, he will come within three days.</ta>
            <ta e="T374" id="Seg_11445" s="T371">If he doesn't agree, he himself will be killed.</ta>
            <ta e="T377" id="Seg_11446" s="T374">He won't be against", he said.</ta>
            <ta e="T383" id="Seg_11447" s="T377">He sent a human in a sledge to invite the Dolgan prince.</ta>
            <ta e="T390" id="Seg_11448" s="T383">On the third day the Dolgan prince came, wearing beaded clothing.</ta>
            <ta e="T394" id="Seg_11449" s="T390">They were drinking for three days.</ta>
            <ta e="T397" id="Seg_11450" s="T394">The Nenets prince said: </ta>
            <ta e="T405" id="Seg_11451" s="T397">"One Nganasan took the child of people who died of one disease.</ta>
            <ta e="T408" id="Seg_11452" s="T405">The disease will follow him.</ta>
            <ta e="T413" id="Seg_11453" s="T408">Let us write a complaint to the czar together.</ta>
            <ta e="T417" id="Seg_11454" s="T413">The Dolgan prince thought it over and said: </ta>
            <ta e="T419" id="Seg_11455" s="T417">"We might judge him or not.</ta>
            <ta e="T427" id="Seg_11456" s="T419">But still it would be too much to slander him of having chopped those people.</ta>
            <ta e="T434" id="Seg_11457" s="T427">He took an orphan, so what is he guilty of, for us to take him to court?", he said.</ta>
            <ta e="T436" id="Seg_11458" s="T434">The Nenets prince said: </ta>
            <ta e="T441" id="Seg_11459" s="T436">"Are you protecting that human?</ta>
            <ta e="T445" id="Seg_11460" s="T441">Then we will chop you.</ta>
            <ta e="T448" id="Seg_11461" s="T445">We will take your wealth", he said.</ta>
            <ta e="T451" id="Seg_11462" s="T448">The Dolgan prince got scared.</ta>
            <ta e="T454" id="Seg_11463" s="T451">They both are murderers.</ta>
            <ta e="T458" id="Seg_11464" s="T454">"Well, what will you advise?", he said.</ta>
            <ta e="T460" id="Seg_11465" s="T458">The Nenets prince: </ta>
            <ta e="T464" id="Seg_11466" s="T460">"I will be the leader.</ta>
            <ta e="T476" id="Seg_11467" s="T464">Let us complain to the czar, writing, that this warrior chopped the Nganasan settlement and took the child.</ta>
            <ta e="T487" id="Seg_11468" s="T476">My people will go and shoot the dead people, when the commission comes, it will not find anything.</ta>
            <ta e="T490" id="Seg_11469" s="T487">Who will guess?!" he said.</ta>
            <ta e="T497" id="Seg_11470" s="T490">About ten men went and shot since a long time dead people.</ta>
            <ta e="T500" id="Seg_11471" s="T497">They wrote so.</ta>
            <ta e="T505" id="Seg_11472" s="T500">They sent the Nganasan prince together with the Nenets prince.</ta>
            <ta e="T515" id="Seg_11473" s="T505">The czar read: "There is a human who chopped people from about ten tents and stole a child".</ta>
            <ta e="T518" id="Seg_11474" s="T515">The czar asked them all: </ta>
            <ta e="T520" id="Seg_11475" s="T518">"Is it true?</ta>
            <ta e="T525" id="Seg_11476" s="T520">I will judge him according to what I've heard from you."</ta>
            <ta e="T533" id="Seg_11477" s="T525">The Nganasan prince said: "This is a desolated human, it's probably him who has done this."</ta>
            <ta e="T537" id="Seg_11478" s="T533">They send the old man a forged letter.</ta>
            <ta e="T548" id="Seg_11479" s="T537">The prince wrote to him, that he should come as fast as possible to receive his money for the taxes.</ta>
            <ta e="T552" id="Seg_11480" s="T548">A messenger brought the letter.</ta>
            <ta e="T558" id="Seg_11481" s="T552">The old man came in gallop on his best reindeers.</ta>
            <ta e="T563" id="Seg_11482" s="T558">He tied up his reindeers and ran inside.</ta>
            <ta e="T567" id="Seg_11483" s="T563">A lot of lord were sitting around a table.</ta>
            <ta e="T575" id="Seg_11484" s="T567">The czar didn't give him any tea to drink, his blood was bad, completely different.</ta>
            <ta e="T578" id="Seg_11485" s="T575">"Well, tell your news.</ta>
            <ta e="T582" id="Seg_11486" s="T578">Why did you kull off people from the settlement?"</ta>
            <ta e="T590" id="Seg_11487" s="T582">"What settlement do you mean(?), I have no dispute with anyone."</ta>
            <ta e="T594" id="Seg_11488" s="T590">The czar showed him the letter of the princes: </ta>
            <ta e="T604" id="Seg_11489" s="T594">"Until the melting waters in the next year, you will stay in prison, then I will send you to an even bigger czar."</ta>
            <ta e="T608" id="Seg_11490" s="T604">They locked the old man up in prison.</ta>
            <ta e="T612" id="Seg_11491" s="T608">The three princes will divide his wealth.</ta>
            <ta e="T621" id="Seg_11492" s="T612">The old woman came, but the old man was not there, he stayed in prison.</ta>
            <ta e="T625" id="Seg_11493" s="T621">The czar sent the old woman away.</ta>
            <ta e="T635" id="Seg_11494" s="T625">The three princes divided the old man's wealth among each other, they left her only a leather cover for a tent.</ta>
            <ta e="T640" id="Seg_11495" s="T635">The winter came to an end and a steamer sailed up.</ta>
            <ta e="T644" id="Seg_11496" s="T640">"Well, see your husband", they said.</ta>
            <ta e="T652" id="Seg_11497" s="T644">The old man's eyes fell into a hollow, they almost didn't give him to eat, he starved out.</ta>
            <ta e="T655" id="Seg_11498" s="T652">They threw him into the steamer.</ta>
            <ta e="T663" id="Seg_11499" s="T655">They brought him to the next czar, they brought the dying man in the stretcher inside.</ta>
            <ta e="T668" id="Seg_11500" s="T663">"What is it? How should I judge a dying human?</ta>
            <ta e="T672" id="Seg_11501" s="T668">Bring him into a hospital, they should give him to eat and take care of him."</ta>
            <ta e="T677" id="Seg_11502" s="T672">Doctors healed him, he became himself again.</ta>
            <ta e="T679" id="Seg_11503" s="T677">The doctors asked him: </ta>
            <ta e="T683" id="Seg_11504" s="T679">"Are you guilty or do they slander you?"</ta>
            <ta e="T686" id="Seg_11505" s="T683">The old man told everything.</ta>
            <ta e="T689" id="Seg_11506" s="T686">The doctors took his blood: </ta>
            <ta e="T695" id="Seg_11507" s="T689">"If you were guilty, your blood should be different", they said.</ta>
            <ta e="T701" id="Seg_11508" s="T695">"You are obviously not guilty at all."</ta>
            <ta e="T703" id="Seg_11509" s="T701">They gave him a letter: </ta>
            <ta e="T707" id="Seg_11510" s="T703">"Give it directly to the court.</ta>
            <ta e="T712" id="Seg_11511" s="T707">We will take the blood of the princes too", they said.</ta>
            <ta e="T714" id="Seg_11512" s="T712">The court began to judge [on the old man].</ta>
            <ta e="T720" id="Seg_11513" s="T714">The princes, the first czar and a lot of people gathered together.</ta>
            <ta e="T722" id="Seg_11514" s="T720">The czar said: </ta>
            <ta e="T725" id="Seg_11515" s="T722">"Well, let us listen to his words", he said.</ta>
            <ta e="T727" id="Seg_11516" s="T725">The old man told. </ta>
            <ta e="T733" id="Seg_11517" s="T727">"I am a human who never argued with anybody." </ta>
            <ta e="T741" id="Seg_11518" s="T733">And he gave the testimony: "This human is not guilty at all."</ta>
            <ta e="T744" id="Seg_11519" s="T741">They took blood of the princes.</ta>
            <ta e="T753" id="Seg_11520" s="T744">The Nganasan warrior's blood is very different from the bloof of the princes.</ta>
            <ta e="T756" id="Seg_11521" s="T753">The czar got angry: </ta>
            <ta e="T758" id="Seg_11522" s="T756">"Who advised to that?</ta>
            <ta e="T759" id="Seg_11523" s="T758">Tell!"</ta>
            <ta e="T762" id="Seg_11524" s="T759">The Dolgan prince told: </ta>
            <ta e="T766" id="Seg_11525" s="T762">"The truth is that we are guilty.</ta>
            <ta e="T771" id="Seg_11526" s="T766">Most guilty is the Nenets prince.</ta>
            <ta e="T777" id="Seg_11527" s="T771">They would have chopped me, I've signed being afraid of it.</ta>
            <ta e="T778" id="Seg_11528" s="T777">The czar said: </ta>
            <ta e="T784" id="Seg_11529" s="T778">"Well, old man, you are obviously not guilty.</ta>
            <ta e="T789" id="Seg_11530" s="T784">I will send the czar, that judged you, to prison.</ta>
            <ta e="T799" id="Seg_11531" s="T789">You will be czar instead of him, when you come, judge by yourself on the princes", he said.</ta>
            <ta e="T802" id="Seg_11532" s="T799">They went back.</ta>
            <ta e="T807" id="Seg_11533" s="T802">The old man came home and became a czar.</ta>
            <ta e="T812" id="Seg_11534" s="T807">His son kept growing into a hero.</ta>
            <ta e="T820" id="Seg_11535" s="T812">As long as his father was away, they wanted to kill him too, because they thought he would become as him.</ta>
            <ta e="T828" id="Seg_11536" s="T820">The boy beat one of them and got into prison because of that.</ta>
            <ta e="T830" id="Seg_11537" s="T828">He[the old man] judged on the princes.</ta>
            <ta e="T836" id="Seg_11538" s="T830">He threw the Nganasan prince and the Nenets prince into prison.</ta>
            <ta e="T840" id="Seg_11539" s="T836">He removed from post the Dolgan prince.</ta>
            <ta e="T844" id="Seg_11540" s="T840">He made his son to the Nganasan prince.</ta>
            <ta e="T848" id="Seg_11541" s="T844">This way the truth of the old man came out.</ta>
            <ta e="T849" id="Seg_11542" s="T848">The end.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_11543" s="T0">Es gibt wohl welche, die haben einen Zaren, einen Hausherren oder einen Prinzen.</ta>
            <ta e="T10" id="Seg_11544" s="T4">Dieser Mensch lebt alleine, er ist sehr stark.</ta>
            <ta e="T14" id="Seg_11545" s="T10">Er ist ganz alleine und hat keine Kinder.</ta>
            <ta e="T18" id="Seg_11546" s="T14">Einmal im Jahr bezahlt er Steuern.</ta>
            <ta e="T26" id="Seg_11547" s="T18">Am Ufer des Meeres, in der richtigen Tundra lebt er, er heizt mit Treibholz.</ta>
            <ta e="T29" id="Seg_11548" s="T26">Er lebte lange, er wurde alt.</ta>
            <ta e="T32" id="Seg_11549" s="T29">Es wurde Frühling.</ta>
            <ta e="T34" id="Seg_11550" s="T32">Er sagte zu seiner alten Frau: </ta>
            <ta e="T37" id="Seg_11551" s="T34">"Es ist schon warm geworden.</ta>
            <ta e="T41" id="Seg_11552" s="T37">Ich fahre Holz für das kommende Jahr holen.</ta>
            <ta e="T44" id="Seg_11553" s="T41">Bereite dreißig Schlitten vor.</ta>
            <ta e="T50" id="Seg_11554" s="T44">Mit den dreißig Schlitten fuhr er ganz alleine zum Meer.</ta>
            <ta e="T54" id="Seg_11555" s="T50">Zum Meer gelangte er über ein sehr langes Kap.</ta>
            <ta e="T63" id="Seg_11556" s="T54">Er lud von dem einsamen Ort ein, durch ein Flussbett gelangte er zurück auf das hohe Ufer.</ta>
            <ta e="T70" id="Seg_11557" s="T63">Er kam hinauf, setzte sich auf einen Berg und rauchte.</ta>
            <ta e="T81" id="Seg_11558" s="T70">Er schaute zum Fluss, und im Wasser ging ein Kind, es spielte im Schmelzwasser mit Schaum.</ta>
            <ta e="T84" id="Seg_11559" s="T81">Es war offenbar ein Junge.</ta>
            <ta e="T88" id="Seg_11560" s="T84">Woher er kam, weiß man nicht.</ta>
            <ta e="T89" id="Seg_11561" s="T88">Er wunderte sich.</ta>
            <ta e="T93" id="Seg_11562" s="T89">"Ich habe doch keine Kinder!</ta>
            <ta e="T97" id="Seg_11563" s="T93">Ob ich ihn mitnehme", dachte er.</ta>
            <ta e="T106" id="Seg_11564" s="T97">Das Kind ging auf eine Sandinsel, dort war ein krummer Baum, es kletterte hinauf.</ta>
            <ta e="T117" id="Seg_11565" s="T106">Der alte Mann sprang hin, er griff ihn von hinten, steckte ihn in einen Mantel und gürtete sich.</ta>
            <ta e="T120" id="Seg_11566" s="T117">Und er machte sich wieder auf den Weg.</ta>
            <ta e="T123" id="Seg_11567" s="T120">Eine Murmeln kam auf (?).</ta>
            <ta e="T124" id="Seg_11568" s="T123">Er blieb stehen.</ta>
            <ta e="T131" id="Seg_11569" s="T124">Er sah, dass dort wohl eine ganze Siedlung an Menschen gestorben war.</ta>
            <ta e="T137" id="Seg_11570" s="T131">"Ah, da ist das Kind wohl übrig geblieben."</ta>
            <ta e="T143" id="Seg_11571" s="T137">Er holte ein vorderes Rentier und tötete es: </ta>
            <ta e="T152" id="Seg_11572" s="T143">"Ich habe euer Kind genommen, als Bezahlung für euer Kind lasse ich ein Rentier hier", sagte er.</ta>
            <ta e="T156" id="Seg_11573" s="T152">Er brachte es nach Hause und erzählte seiner alten Frau alles.</ta>
            <ta e="T165" id="Seg_11574" s="T156">Drei Tage lang spielen sie mit dem Kind, ohne die Rentiere loszulassen.</ta>
            <ta e="T170" id="Seg_11575" s="T165">Das ganze Gespann lag angespannt da.</ta>
            <ta e="T173" id="Seg_11576" s="T170">Man weiß nichts.</ta>
            <ta e="T178" id="Seg_11577" s="T173">Das Kind war gewachsen, es konnte laufen.</ta>
            <ta e="T188" id="Seg_11578" s="T178">Als er [=der Mann] einmal saß und schaute, da fuhr ein Mensch, der ganz weiß angezogen war, mit fünf Schlitten mit weißen Rentierbullen.</ta>
            <ta e="T190" id="Seg_11579" s="T188">Das war offenbar ein Fürst.</ta>
            <ta e="T195" id="Seg_11580" s="T190">"Nun, was gebe ich ihm", dachte er.</ta>
            <ta e="T198" id="Seg_11581" s="T195">"Alter Mann, wie lebst du?</ta>
            <ta e="T206" id="Seg_11582" s="T198">Lange war ich nicht mehr hier, hast du ein Kind bekommen?", sagte der Fürst.</ta>
            <ta e="T211" id="Seg_11583" s="T206">"Auf unsere alten Jahre haben wir noch ein Kind bekommen!", sagte der alte Mann.</ta>
            <ta e="T213" id="Seg_11584" s="T211">"Nun gut.</ta>
            <ta e="T218" id="Seg_11585" s="T213">Ich komme, um Steuern einzutreiben", sagte der Fürst.</ta>
            <ta e="T224" id="Seg_11586" s="T218">Der alte Mann gab einige Säcke mit Polarfuchsfellen.</ta>
            <ta e="T230" id="Seg_11587" s="T224">Der Fürst freute sich, "für den Rest schicke ich Geld", sagte er.</ta>
            <ta e="T232" id="Seg_11588" s="T230">Der Fürst machte sich auf den Weg.</ta>
            <ta e="T238" id="Seg_11589" s="T232">Er galoppierte irgendwohin zu einem nenzischen Fürsten, zu seinem Freund.</ta>
            <ta e="T241" id="Seg_11590" s="T238">Der Fürst kam zu dem Fürsten.</ta>
            <ta e="T243" id="Seg_11591" s="T241">Sie fingen an zu trinken.</ta>
            <ta e="T247" id="Seg_11592" s="T243">Einige Tage lang tranken sie.</ta>
            <ta e="T252" id="Seg_11593" s="T247">Als sie so lebten, erzählte er seinem Freund: </ta>
            <ta e="T257" id="Seg_11594" s="T252">"Nun, ich habe einen gefährlichen Menschen.</ta>
            <ta e="T262" id="Seg_11595" s="T257">Seine Steuern bezahlt er mehr als nötig.</ta>
            <ta e="T264" id="Seg_11596" s="T262">Von ihm komme ich. </ta>
            <ta e="T268" id="Seg_11597" s="T264">Ich wundere mich, er hat ein Kind bekommen, das schon laufen kann.</ta>
            <ta e="T272" id="Seg_11598" s="T268">Eigene Kinder hat er nicht.</ta>
            <ta e="T275" id="Seg_11599" s="T272">Woher mag er es bekommen haben?</ta>
            <ta e="T280" id="Seg_11600" s="T275">Ob er jemanden umgebracht und es genommen hat?!"</ta>
            <ta e="T290" id="Seg_11601" s="T280">"Ach", sagte der nenzische Fürst, "mir ist vor langer Zeit eine ganze Siedlung an Menschen gestorben.</ta>
            <ta e="T296" id="Seg_11602" s="T290">Nur ein Kind im Wiegenalter ist geblieben, sagt man.</ta>
            <ta e="T301" id="Seg_11603" s="T296">Ich befahl, es nicht zu nehmen, ich dachte, ihm folgte eine Krankheit.</ta>
            <ta e="T303" id="Seg_11604" s="T301">Das hat er wohl genommen.</ta>
            <ta e="T307" id="Seg_11605" s="T303">Das Kind hat er wohl versteckt.</ta>
            <ta e="T310" id="Seg_11606" s="T307">"Sie töten mich" denkend. </ta>
            <ta e="T312" id="Seg_11607" s="T310">Nun gut.</ta>
            <ta e="T316" id="Seg_11608" s="T312">Ich rate dir, keine Angst vor diesem Menschen zu haben.</ta>
            <ta e="T329" id="Seg_11609" s="T316">Lass uns eine Beschwerde an den Zaren schreiben, dass er eine Gruppe Leute umgebracht und das Kind genommen hat.</ta>
            <ta e="T346" id="Seg_11610" s="T329">Unser Zar wird uns das nicht abschlagen, er wird ihn verurteilen, ihn ins Gefängnis stecken und wir nehmen seinen Reichtum", sagte der nenzische Fürst.</ta>
            <ta e="T353" id="Seg_11611" s="T346">"Ist es nicht schlecht, einen Menschen einfach anzuschwärzen?", sagte der nganasanische Fürst.</ta>
            <ta e="T362" id="Seg_11612" s="T353">"Rufen wir noch den dolganischen Fürsten, drei Fürsten vertraut man noch mehr."</ta>
            <ta e="T364" id="Seg_11613" s="T362">Der nenzische Fürst [sagte]: </ta>
            <ta e="T371" id="Seg_11614" s="T364">"Ich schicke einen Menschen nach dem Fürsten der Dolganen, innerhalb von drei Tagen wird er kommen.</ta>
            <ta e="T374" id="Seg_11615" s="T371">Wenn er nicht einverstanden ist, dann wird er selbst getötet.</ta>
            <ta e="T377" id="Seg_11616" s="T374">Er wird nicht dagegen sein", sagte er.</ta>
            <ta e="T383" id="Seg_11617" s="T377">Er schickte einen Menschen mit Schlitten(gespann), um den dolganischen Fürsten einzuladen.</ta>
            <ta e="T390" id="Seg_11618" s="T383">Am dritten Tag kam der dolganische Fürst, seine Kleidung ist mit Perlen bestickt.</ta>
            <ta e="T394" id="Seg_11619" s="T390">Sie tranken drei Tage lang.</ta>
            <ta e="T397" id="Seg_11620" s="T394">Der nenzische Fürst sagte: </ta>
            <ta e="T405" id="Seg_11621" s="T397">"Ein Nganasane hat von an einer Krankheit gestorbenen Leuten ein Kind genommen.</ta>
            <ta e="T408" id="Seg_11622" s="T405">Die Krankheit folgt ihm.</ta>
            <ta e="T413" id="Seg_11623" s="T408">Lass uns beim Zaren beklagen, lass uns zu dritt schreiben.</ta>
            <ta e="T417" id="Seg_11624" s="T413">Der dolganische Fürst dachte nach und sagte: </ta>
            <ta e="T419" id="Seg_11625" s="T417">"Urteilen können wir wohl.</ta>
            <ta e="T427" id="Seg_11626" s="T419">Aber es ist wohl dennoch zu viel ihn zu verdächtigen, Leute zerhackt zu haben.</ta>
            <ta e="T434" id="Seg_11627" s="T427">Er hat ein Waisenkind aufgenommen, was für eine Schuld trägt er, dass wir ihn ans Gericht übergeben könnten?", sagte er.</ta>
            <ta e="T436" id="Seg_11628" s="T434">Der nenzische Fürst: </ta>
            <ta e="T441" id="Seg_11629" s="T436">"Schützt du diesen Menschen?</ta>
            <ta e="T445" id="Seg_11630" s="T441">Dann zerhacken wir dich.</ta>
            <ta e="T448" id="Seg_11631" s="T445">Wir werden deinen Reichtum nehmen", sagte er.</ta>
            <ta e="T451" id="Seg_11632" s="T448">Der dolganische Fürst erschrak.</ta>
            <ta e="T454" id="Seg_11633" s="T451">Die beiden sind wohl Mörder.</ta>
            <ta e="T458" id="Seg_11634" s="T454">"Nun, was ratet ihr mir?", sagte er.</ta>
            <ta e="T460" id="Seg_11635" s="T458">Der nenzische Fürst: </ta>
            <ta e="T464" id="Seg_11636" s="T460">"Ich werde Anführer.</ta>
            <ta e="T476" id="Seg_11637" s="T464">Lass uns beim Zaren beschweren: Wir schreiben, dass dieser Recke eine nganasanische Siedlung umgebracht hat und ein Kind genommen hat.</ta>
            <ta e="T487" id="Seg_11638" s="T476">Meine Leute gehen und schießen auf die toten Leute, wenn eine Kommission kommt, werden sie nichts finden.</ta>
            <ta e="T490" id="Seg_11639" s="T487">Wer soll das herausfinden?!" sagte er.</ta>
            <ta e="T497" id="Seg_11640" s="T490">Etwa zehn Menschen gingen und schossen auf die schon lange gestorbenen Menschen.</ta>
            <ta e="T500" id="Seg_11641" s="T497">Und so schrieben sie auch.</ta>
            <ta e="T505" id="Seg_11642" s="T500">Sie schickten den nganasanischen Fürsten gemeinsam mit dem nenzischen Fürsten.</ta>
            <ta e="T515" id="Seg_11643" s="T505">Der Zar las: "Es ist ein Mensch, der die Leute aus etwa zehn Zelten umgebracht hat und ein Kind gestohlen hat".</ta>
            <ta e="T518" id="Seg_11644" s="T515">Der Zar fragte sie alle: </ta>
            <ta e="T520" id="Seg_11645" s="T518">"Ist das die Wahrheit?</ta>
            <ta e="T525" id="Seg_11646" s="T520">Ich werde ihn verurteilen, nach dem, was ich von euch gehört habe."</ta>
            <ta e="T533" id="Seg_11647" s="T525">Der nganasanische Fürst sagte: "Das ist ein unnahbarer Mensch, wahrscheinlich hat er das gemacht."</ta>
            <ta e="T537" id="Seg_11648" s="T533">Sie schickten dem alten Mann den gefälschten Brief.</ta>
            <ta e="T548" id="Seg_11649" s="T537">Der Fürst schrieb ihm, er möge so schnell wie möglich kommen, um das Geld für seine [zu viel gezahlten] Abgaben zu bekommen.</ta>
            <ta e="T552" id="Seg_11650" s="T548">Ein Bote brachte den Brief.</ta>
            <ta e="T558" id="Seg_11651" s="T552">Der alte Mann kam mit seinen besten Rentieren angaloppiert.</ta>
            <ta e="T563" id="Seg_11652" s="T558">Er band die Rentiere an und rannte hinein.</ta>
            <ta e="T567" id="Seg_11653" s="T563">Um einen Tisch herum saßen viele Herren.</ta>
            <ta e="T575" id="Seg_11654" s="T567">Der Zar gab ihm keinen Tee zu trinken, sein Blut war schlecht, ganz anders.</ta>
            <ta e="T578" id="Seg_11655" s="T575">"Nun, erzähl deine Neuigkeiten.</ta>
            <ta e="T582" id="Seg_11656" s="T578">Warum hast du die Leute einer Siedlung ausgerottet?"</ta>
            <ta e="T590" id="Seg_11657" s="T582">"Was für eine Siedlung meinst(?) du, ich habe mit niemandem Streit."</ta>
            <ta e="T594" id="Seg_11658" s="T590">Der Zar zeigte ihm den Brief der Fürsten: </ta>
            <ta e="T604" id="Seg_11659" s="T594">"Bis das Wasser im nächsten Jahr kommt, wirst du im Gefängnis sitzen, dann schicke ich dich zu einem noch größeren Zaren als ich."</ta>
            <ta e="T608" id="Seg_11660" s="T604">Sie sperrten den alten Mann ins Gefängnis.</ta>
            <ta e="T612" id="Seg_11661" s="T608">Die drei Fürsten werden seinen Reichtum unter sich aufteilen.</ta>
            <ta e="T621" id="Seg_11662" s="T612">Die alte Frau kam gefahren, aber der alte Mann war nicht da, er saß im Gefängnis.</ta>
            <ta e="T625" id="Seg_11663" s="T621">Der Zar schickte die alte Frau weg.</ta>
            <ta e="T635" id="Seg_11664" s="T625">Die drei Fürsten teilten den Reichtum unter sich auf, sie ließen ihr die Lederdecke eines Zeltes.</ta>
            <ta e="T640" id="Seg_11665" s="T635">Der Winter ging zuende und ein Dampfer kam [den Fluss] herab.</ta>
            <ta e="T644" id="Seg_11666" s="T640">"Nun, sieh nach deinem Mann", sagten sie.</ta>
            <ta e="T652" id="Seg_11667" s="T644">Die Augen des alten Mannes fielen fast in die Augenhöhle, sie hatten ihm nichts zu essen gegeben, er war ganz ausgehungert.</ta>
            <ta e="T655" id="Seg_11668" s="T652">Sie warfen ihn in den Dampfer hinein.</ta>
            <ta e="T663" id="Seg_11669" s="T655">Sie brachten ihn zum nächsten Zaren, sie trugen ihn auf einer Trage hinein, den sterbenden Menschen.</ta>
            <ta e="T668" id="Seg_11670" s="T663">"Hey, wie soll ich über einen sterbenden Menschen urteilen!</ta>
            <ta e="T672" id="Seg_11671" s="T668">Bringt ihn ins Krankenhaus, sie sollen ihm zu essen geben und nach ihm sehen."</ta>
            <ta e="T677" id="Seg_11672" s="T672">Die Doktoren heilten ihn, er wurde wieder er selbst.</ta>
            <ta e="T679" id="Seg_11673" s="T677">Die Doktoren fragten: </ta>
            <ta e="T683" id="Seg_11674" s="T679">"Trägst du Schuld oder schwärzen sie dich an?"</ta>
            <ta e="T686" id="Seg_11675" s="T683">Der alte Mann erzählte alles.</ta>
            <ta e="T689" id="Seg_11676" s="T686">Die Doktoren nahmen ihm Blut ab: </ta>
            <ta e="T695" id="Seg_11677" s="T689">"Wenn du schuldig bist, dann wird dein Blut wohl anders sein", sagten sie.</ta>
            <ta e="T701" id="Seg_11678" s="T695">"Du trägst offenbar nicht die Spur von Schuld."</ta>
            <ta e="T703" id="Seg_11679" s="T701">Sie gaben ihm einen Brief: </ta>
            <ta e="T707" id="Seg_11680" s="T703">"Gib ihn direkt ans Gericht.</ta>
            <ta e="T712" id="Seg_11681" s="T707">Wir nehmen auch den Fürsten Blut ab", sagten sie.</ta>
            <ta e="T714" id="Seg_11682" s="T712">Es tagte das Gericht.</ta>
            <ta e="T720" id="Seg_11683" s="T714">Die Fürsten, der erste Zar, viele Leute hatten sich versammelt.</ta>
            <ta e="T722" id="Seg_11684" s="T720">Der Zar sagte: </ta>
            <ta e="T725" id="Seg_11685" s="T722">"Nun, hören wir uns seine Worte an", sagte er.</ta>
            <ta e="T727" id="Seg_11686" s="T725">Der alte Mann erzählte: </ta>
            <ta e="T733" id="Seg_11687" s="T727">"Ich bin ein Mensch, der sich mit niemandem gestritten hat." </ta>
            <ta e="T741" id="Seg_11688" s="T733">Und er übergibt die Bescheinigung: "Dieser Mensch trägt nicht die Spur von Schuld."</ta>
            <ta e="T744" id="Seg_11689" s="T741">Sie nahmen die Fürsten Blut ab.</ta>
            <ta e="T753" id="Seg_11690" s="T744">Das Blut des nganasanischen Recken ist tatsächlich anders, das der Fürsten ist ganz anders.</ta>
            <ta e="T756" id="Seg_11691" s="T753">Da wurde der Zar böse: </ta>
            <ta e="T758" id="Seg_11692" s="T756">"Wer hat dazu geraten?</ta>
            <ta e="T759" id="Seg_11693" s="T758">Erzählt!"</ta>
            <ta e="T762" id="Seg_11694" s="T759">Der dolganische Fürst erzählte: </ta>
            <ta e="T766" id="Seg_11695" s="T762">"Die Wahrheit ist, wir sind Schuld.</ta>
            <ta e="T771" id="Seg_11696" s="T766">Am meisten Schuld hat der nenzische Fürst.</ta>
            <ta e="T777" id="Seg_11697" s="T771">Sie hätten mich töten können, vor Angst habe ich unterschrieben."</ta>
            <ta e="T778" id="Seg_11698" s="T777">Der Zar sagte: </ta>
            <ta e="T784" id="Seg_11699" s="T778">"Nun, alter Mann, du trägst offenbar keine Schuld.</ta>
            <ta e="T789" id="Seg_11700" s="T784">Den Zaren, der dich verurteilt hat, stecke ich ins Gefängnis.</ta>
            <ta e="T799" id="Seg_11701" s="T789">Du wirst an seiner Stelle Zar, wenn du angekommen bist, dann urteile selber über die Fürsten", sagte er.</ta>
            <ta e="T802" id="Seg_11702" s="T799">Sie fuhren zurück.</ta>
            <ta e="T807" id="Seg_11703" s="T802">Der alte Mann kam nach Hause und wurde Fürst.</ta>
            <ta e="T812" id="Seg_11704" s="T807">Sein Sohn wuchs und wurde direkt zu einem Helden.</ta>
            <ta e="T820" id="Seg_11705" s="T812">Solange [der Vater] nicht da war, wollte er ihn töten, er hatte Angst, dass er würde wie er.</ta>
            <ta e="T828" id="Seg_11706" s="T820">Der Junge hatte einen der Menschen umgeworfen, dafür hatten sie ihn ins Gefängnis gesteckt.</ta>
            <ta e="T830" id="Seg_11707" s="T828">Er [der alte Mann] urteilte über die Fürsten.</ta>
            <ta e="T836" id="Seg_11708" s="T830">Den nganasanischen und den nenzischen Fürsten steckte er ins Gefängnis.</ta>
            <ta e="T840" id="Seg_11709" s="T836">Den dolganischen Fürsten enthob er seines Amtes.</ta>
            <ta e="T844" id="Seg_11710" s="T840">Seinen Sohn machte er zum nganasanischen Fürsten.</ta>
            <ta e="T848" id="Seg_11711" s="T844">So kam die Wahrheit des alten Mannes heraus.</ta>
            <ta e="T849" id="Seg_11712" s="T848">Ende.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_11713" s="T0">Все имеют, конечно же, своих царей, господ, князей.</ta>
            <ta e="T10" id="Seg_11714" s="T4">Вот этот человек живет отдельно, очень [сильный] косун.</ta>
            <ta e="T14" id="Seg_11715" s="T10">Бездетный он.</ta>
            <ta e="T18" id="Seg_11716" s="T14">Раз в году платит подать.</ta>
            <ta e="T26" id="Seg_11717" s="T18">У берега моря, в самой тундре живет, топит очаг плавником.</ta>
            <ta e="T29" id="Seg_11718" s="T26">Долго жил, состарился.</ta>
            <ta e="T32" id="Seg_11719" s="T29">Год к весне шел.</ta>
            <ta e="T34" id="Seg_11720" s="T32">Говорит он старухе: </ta>
            <ta e="T37" id="Seg_11721" s="T34">— Уже потеплело.</ta>
            <ta e="T41" id="Seg_11722" s="T37">Поеду заготавливать дрова на будущий год.</ta>
            <ta e="T44" id="Seg_11723" s="T41">Приготовь тридцать нарт.</ta>
            <ta e="T50" id="Seg_11724" s="T44">На тридцати нартах совсем один поехал к морю.</ta>
            <ta e="T54" id="Seg_11725" s="T50">До моря доехал длинным мысом.</ta>
            <ta e="T63" id="Seg_11726" s="T54">Нагрузил нарты, не сходя с места, поднялся на высокий берег по руслу одной речки.</ta>
            <ta e="T70" id="Seg_11727" s="T63">Выехал, сел на горе и курит.</ta>
            <ta e="T81" id="Seg_11728" s="T70">Посмотрел в сторону речки, а у воды ходит ребенок, на талой воде играет пеной.</ta>
            <ta e="T84" id="Seg_11729" s="T81">Видно, мальчик.</ta>
            <ta e="T88" id="Seg_11730" s="T84">Откуда взялся — неизвестно.</ta>
            <ta e="T89" id="Seg_11731" s="T88">Удивился.</ta>
            <ta e="T93" id="Seg_11732" s="T89">— У меня же нет детей!</ta>
            <ta e="T97" id="Seg_11733" s="T93">Взять бы [его], — подумал.</ta>
            <ta e="T106" id="Seg_11734" s="T97">Тот мальчик ходит по песчаному островку, где стоит накренившееся дерево, [потом] забрался [на дерево].</ta>
            <ta e="T117" id="Seg_11735" s="T106">Старик подскочил, ухватил мальчика за загривок, сунул за пазуху в сокуй и подпоясался.</ta>
            <ta e="T120" id="Seg_11736" s="T117">Ну вот, двинулся опять в путь.</ta>
            <ta e="T123" id="Seg_11737" s="T120">Под полозьями затрещало.</ta>
            <ta e="T124" id="Seg_11738" s="T123">Остановился.</ta>
            <ta e="T131" id="Seg_11739" s="T124">Посмотрел: оказывается, тут целое стойбище вымерло.</ta>
            <ta e="T137" id="Seg_11740" s="T131">"Э-э, видно, ребенок от них остался", — [подумал].</ta>
            <ta e="T143" id="Seg_11741" s="T137">Приводит передового оленя и тут же его забивает: </ta>
            <ta e="T152" id="Seg_11742" s="T143">— Взял ваше дитя, в дар за него оставил одного оленя, — сказал.</ta>
            <ta e="T156" id="Seg_11743" s="T152">Привез домой, старухе своей [все] рассказал.</ta>
            <ta e="T165" id="Seg_11744" s="T156">Целых три дня, даже не успев распрячь оленей, тешатся этим ребенком — </ta>
            <ta e="T170" id="Seg_11745" s="T165">Вся упряжка так и лежит.</ta>
            <ta e="T173" id="Seg_11746" s="T170">Ничего не знают.</ta>
            <ta e="T178" id="Seg_11747" s="T173">Мальчик уже подрос, сам бегает.</ta>
            <ta e="T188" id="Seg_11748" s="T178">Однажды видит: едет одетый во все белое человек, в сани пять белых самцов впряжено.</ta>
            <ta e="T190" id="Seg_11749" s="T188">Оказалось, князь.</ta>
            <ta e="T195" id="Seg_11750" s="T190">"Ну, что я ему дам?" — думает.</ta>
            <ta e="T198" id="Seg_11751" s="T195">— Старик, как жил?</ta>
            <ta e="T206" id="Seg_11752" s="T198">Долго же меня не было, уже и ребенком успели обзавестись, — говорит князь.</ta>
            <ta e="T211" id="Seg_11753" s="T206">— Вот на старости лет обзавелись!" отвечает старик.</ta>
            <ta e="T213" id="Seg_11754" s="T211">— Ну хорошо.</ta>
            <ta e="T218" id="Seg_11755" s="T213">Приехал подати собирать, — говорит князь.</ta>
            <ta e="T224" id="Seg_11756" s="T218">Старик отдал очень большой мешок песцовых шкур.</ta>
            <ta e="T230" id="Seg_11757" s="T224">Князь радуется, за часть [пушнины] обещает прислать денег.</ta>
            <ta e="T232" id="Seg_11758" s="T230">Собрался уезжать.</ta>
            <ta e="T238" id="Seg_11759" s="T232">Помчался куда-то к ненецкому князю, своему другу.</ta>
            <ta e="T241" id="Seg_11760" s="T238">Князь пожаловал к князю.</ta>
            <ta e="T243" id="Seg_11761" s="T241">Пьянствуют.</ta>
            <ta e="T247" id="Seg_11762" s="T243">Пили несколько дней.</ta>
            <ta e="T252" id="Seg_11763" s="T247">Вот в это время другу рассказывает: </ta>
            <ta e="T257" id="Seg_11764" s="T252">— Вот, мой друг, есть у меня опасный человек.</ta>
            <ta e="T262" id="Seg_11765" s="T257">Но подати даже переплачивает.</ta>
            <ta e="T264" id="Seg_11766" s="T262">От него еду.</ta>
            <ta e="T268" id="Seg_11767" s="T264">Удивляюсь, у него появился ребенок, который уже бегает.</ta>
            <ta e="T272" id="Seg_11768" s="T268">Hе родное его дитя.</ta>
            <ta e="T275" id="Seg_11769" s="T272">Где мог взять?</ta>
            <ta e="T280" id="Seg_11770" s="T275">Видно, кого-то убил и взял?!</ta>
            <ta e="T290" id="Seg_11771" s="T280">— Да, — говорит ненецкий князь, — у меня давно перемело целое стойбище.</ta>
            <ta e="T296" id="Seg_11772" s="T290">Говорили, что остался в живых только один грудной ребенок.</ta>
            <ta e="T301" id="Seg_11773" s="T296">Велел его не брать, боялся, что за ним болезнь последует.</ta>
            <ta e="T303" id="Seg_11774" s="T301">Его, видно, и взял.</ta>
            <ta e="T307" id="Seg_11775" s="T303">И скрыл, оказывается, того ребенка.</ta>
            <ta e="T310" id="Seg_11776" s="T307">Опасался, что его самого убьют. </ta>
            <ta e="T312" id="Seg_11777" s="T310">Вот что. </ta>
            <ta e="T316" id="Seg_11778" s="T312">Советую тебе не опасаться того человека.</ta>
            <ta e="T329" id="Seg_11779" s="T316">Мы с тобой два князя, давай напишем начальству жалобу, что он перебил целое стойбище и забрал ребенка.</ta>
            <ta e="T346" id="Seg_11780" s="T329">Нашему человеку начальство не откажет, засудит его, отправит в тюрьму, а мы заберем его богатство, — сказал ненецкий князь.</ta>
            <ta e="T353" id="Seg_11781" s="T346">— Зря оговаривать человека ни худо ли? — говорит нганасанский князь.</ta>
            <ta e="T362" id="Seg_11782" s="T353">— Вызвать бы и долганского князя, трем князьям доверия больше было бы.</ta>
            <ta e="T364" id="Seg_11783" s="T362">Ненецкий князь [сказал]: </ta>
            <ta e="T371" id="Seg_11784" s="T364">— Пошлю человека за долганским князем, через трое суток приедет.</ta>
            <ta e="T374" id="Seg_11785" s="T371">Если не согласится, то его самого убьем.</ta>
            <ta e="T377" id="Seg_11786" s="T374">Против не пойдет.</ta>
            <ta e="T383" id="Seg_11787" s="T377">Послал человека на оленьей упряжке за долганским князем.</ta>
            <ta e="T390" id="Seg_11788" s="T383">На третий день приехал долганский князь, вся одежда его расшита бисером.</ta>
            <ta e="T394" id="Seg_11789" s="T390">Дня три, что ли, пьют.</ta>
            <ta e="T397" id="Seg_11790" s="T394">Ненецкий князь говорит: </ta>
            <ta e="T405" id="Seg_11791" s="T397">— Один нганасанин взял ребенка в вымершем от мора стойвище.</ta>
            <ta e="T408" id="Seg_11792" s="T405">Болезнь за ним последует. </ta>
            <ta e="T413" id="Seg_11793" s="T408">Давайте подадим жалобу начальству, напишем втроем.</ta>
            <ta e="T417" id="Seg_11794" s="T413">Долганский князь, призадумавшись, говорит: </ta>
            <ta e="T419" id="Seg_11795" s="T417">Судить-то мы можем. </ta>
            <ta e="T427" id="Seg_11796" s="T419">Но оговорить, что он перебил людей, — это будет слишком. </ta>
            <ta e="T434" id="Seg_11797" s="T427">Забрал сироту, так в этом вины нет, чтобы подавать на него в суд.</ta>
            <ta e="T436" id="Seg_11798" s="T434">Ненецкий князь: </ta>
            <ta e="T441" id="Seg_11799" s="T436">Ты защищаешь того человека? </ta>
            <ta e="T445" id="Seg_11800" s="T441">Тогда мы тебя самого заколем. </ta>
            <ta e="T448" id="Seg_11801" s="T445">Заберем твое богатство, — говорит.</ta>
            <ta e="T451" id="Seg_11802" s="T448">Долганский князь испугался. </ta>
            <ta e="T454" id="Seg_11803" s="T451">Оба ведь убийцы.</ta>
            <ta e="T458" id="Seg_11804" s="T454">— Ну, что посоветуете? — спросил.</ta>
            <ta e="T460" id="Seg_11805" s="T458">Ненецкий князь говорит: </ta>
            <ta e="T464" id="Seg_11806" s="T460">— Я главный советчик.</ta>
            <ta e="T476" id="Seg_11807" s="T464">Подадим жалобу царю, напишем, что этот косун перебил нганасанское стойбище и забрал ребенка.</ta>
            <ta e="T487" id="Seg_11808" s="T476">Мои люди пойдут и постреляют трупы, комиссия приедет и не разберется.</ta>
            <ta e="T490" id="Seg_11809" s="T487">Кто догадается?!</ta>
            <ta e="T497" id="Seg_11810" s="T490">Человек с десять пошли и постреляли давно умерших людей.</ta>
            <ta e="T500" id="Seg_11811" s="T497">Так и написали.</ta>
            <ta e="T505" id="Seg_11812" s="T500">Послали нганасанского князя вместе с ненецким князем. </ta>
            <ta e="T515" id="Seg_11813" s="T505">Начальник читает: "Человек перебил с десяток чумов людей, украл ребенка". </ta>
            <ta e="T518" id="Seg_11814" s="T515">Начальник спрашивает у них: </ta>
            <ta e="T520" id="Seg_11815" s="T518">— Правда ли это? </ta>
            <ta e="T525" id="Seg_11816" s="T520">Я буду судить его при вас.</ta>
            <ta e="T533" id="Seg_11817" s="T525">Нганасанский князь: — Нелюдимый человек. Наверника, его это дело — говорит.</ta>
            <ta e="T537" id="Seg_11818" s="T533">Старику посылают подложное письмо.</ta>
            <ta e="T548" id="Seg_11819" s="T537">Князь ему пишет, чтоб очень спешно приехал получить деньги за подать.</ta>
            <ta e="T552" id="Seg_11820" s="T548">Посыльный доставляет письмо.</ta>
            <ta e="T558" id="Seg_11821" s="T552">Старик примчался на самых лучших оленях.</ta>
            <ta e="T563" id="Seg_11822" s="T558">Привязывает оленей и тут же входит в чум.</ta>
            <ta e="T567" id="Seg_11823" s="T563">Вокруг стола полно начальников.</ta>
            <ta e="T575" id="Seg_11824" s="T567">Начальник даже чаем не напоил, к лицу кровь прилила, совсем злой.</ta>
            <ta e="T578" id="Seg_11825" s="T575">— Ну, рассказывай о своих новостях.</ta>
            <ta e="T582" id="Seg_11826" s="T578">Зачем перебил целое стойбище людей?</ta>
            <ta e="T590" id="Seg_11827" s="T582">"Какое стойбище имеешь в виду(?), у меня нет спора ни с кем."</ta>
            <ta e="T594" id="Seg_11828" s="T590">Начальник показал письмо князей: </ta>
            <ta e="T604" id="Seg_11829" s="T594">— До половодья следующего года будешь сидеть в тюрьме, а там отошло тебя к еще большему начальнику.</ta>
            <ta e="T608" id="Seg_11830" s="T604">Старика тут же послали в тюрьму.</ta>
            <ta e="T612" id="Seg_11831" s="T608">Три князя готовятся разделить его богатство.</ta>
            <ta e="T621" id="Seg_11832" s="T612">Прикочевала старуха — старика нет, сидит в тюрьме.</ta>
            <ta e="T625" id="Seg_11833" s="T621">Начальник старуху выгнал.</ta>
            <ta e="T635" id="Seg_11834" s="T625">Три князя тут же разделили между собой ее богатство, оставили ей одну покрышку от чума.</ta>
            <ta e="T640" id="Seg_11835" s="T635">Кончилась зима, приплывает сверху пароход.</ta>
            <ta e="T644" id="Seg_11836" s="T640">— Ну, посмотри на своего мужа, — говорят.</ta>
            <ta e="T652" id="Seg_11837" s="T644">Глаза старика будто в яму провалились, плохо кормились, совсем обессилел.</ta>
            <ta e="T655" id="Seg_11838" s="T652">Бросили в трюм парохода.</ta>
            <ta e="T663" id="Seg_11839" s="T655">Повезли к другому начальнику, вносят на носилках умирающего человека.</ta>
            <ta e="T668" id="Seg_11840" s="T663">— Что это? Как буду судить умирающего человека?</ta>
            <ta e="T672" id="Seg_11841" s="T668">Увезите в больницу, пусть накормят, призреют.</ta>
            <ta e="T677" id="Seg_11842" s="T672">Доктора лечат, прежний вид принимает.</ta>
            <ta e="T679" id="Seg_11843" s="T677">Доктора спрашивают: </ta>
            <ta e="T683" id="Seg_11844" s="T679">— Виноват или оговаривают тебя?</ta>
            <ta e="T686" id="Seg_11845" s="T683">Старик все рассказывает.</ta>
            <ta e="T689" id="Seg_11846" s="T686">Доктора берут у него кровь: </ta>
            <ta e="T695" id="Seg_11847" s="T689">— Если виноват, кровь должна быть иной, — говорят.</ta>
            <ta e="T701" id="Seg_11848" s="T695">Оказывается, ни на одну капельку не виноват.</ta>
            <ta e="T703" id="Seg_11849" s="T701">Дают письмо: </ta>
            <ta e="T707" id="Seg_11850" s="T703">— Это передай прямо в суд.</ta>
            <ta e="T712" id="Seg_11851" s="T707">Возьмем кровь и у князей, — говорят.</ta>
            <ta e="T714" id="Seg_11852" s="T712">Вот стали судить [старика].</ta>
            <ta e="T720" id="Seg_11853" s="T714">Князья, первый начальник, много народу собралось.</ta>
            <ta e="T722" id="Seg_11854" s="T720">Начальник говорит: </ta>
            <ta e="T725" id="Seg_11855" s="T722">— Давайте послушаем его слова.</ta>
            <ta e="T727" id="Seg_11856" s="T725">Старик рассказывает: </ta>
            <ta e="T733" id="Seg_11857" s="T727">— Я в жизни ни с одним человеком не поссорится. </ta>
            <ta e="T741" id="Seg_11858" s="T733">— и дает справку: "Этот человек и на капельку не виноват".</ta>
            <ta e="T744" id="Seg_11859" s="T741">Берут кровь у князей.</ta>
            <ta e="T753" id="Seg_11860" s="T744">Кровь нганасанского косуна совершенно другая, у князей совсем иная.</ta>
            <ta e="T756" id="Seg_11861" s="T753">Тут начальник разгневался: </ta>
            <ta e="T758" id="Seg_11862" s="T756">— Кто посоветовал?</ta>
            <ta e="T759" id="Seg_11863" s="T758">Расскажите!</ta>
            <ta e="T762" id="Seg_11864" s="T759">Долганский князь рассказывает: </ta>
            <ta e="T766" id="Seg_11865" s="T762">— Правду сказать, мы виноваты.</ta>
            <ta e="T771" id="Seg_11866" s="T766">Самый главный виновник — ненецкий князь.</ta>
            <ta e="T777" id="Seg_11867" s="T771">Меня могли убить, со страху и дал свою подпись.</ta>
            <ta e="T778" id="Seg_11868" s="T777">Начальник говорит: </ta>
            <ta e="T784" id="Seg_11869" s="T778">— Ну, старик, ты, оказывается, не виноват.</ta>
            <ta e="T789" id="Seg_11870" s="T784">В тюрьму посажу начальника, который тебя засудил.</ta>
            <ta e="T799" id="Seg_11871" s="T789">Ты вместо него начальником поедешь туда, сам и суди князей.</ta>
            <ta e="T802" id="Seg_11872" s="T799">Уезжают обратно.</ta>
            <ta e="T807" id="Seg_11873" s="T802">Старик в своей стороне князем стал.</ta>
            <ta e="T812" id="Seg_11874" s="T807">Сын вырос, ну прямо-таки богатырь.</ta>
            <ta e="T820" id="Seg_11875" s="T812">Пока [отца] не было, хотели его убить, опасаясь, что станет таким же, как отец.</ta>
            <ta e="T828" id="Seg_11876" s="T820">Парень одного [из нападавших] сшиб с ног, за это посадили в тюрьму.</ta>
            <ta e="T830" id="Seg_11877" s="T828">Стал [старик] судить тех князей.</ta>
            <ta e="T836" id="Seg_11878" s="T830">Нганасанского и ненецкою посадил в тюрьму.</ta>
            <ta e="T840" id="Seg_11879" s="T836">Долганского князя снял с должности.</ta>
            <ta e="T844" id="Seg_11880" s="T840">Сына назначил нганасанским князем.</ta>
            <ta e="T848" id="Seg_11881" s="T844">Так выявилась правда старика.</ta>
            <ta e="T849" id="Seg_11882" s="T848">Конец.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T4" id="Seg_11883" s="T0">Все имеют, конечно же, своих царей, господ, князей.</ta>
            <ta e="T10" id="Seg_11884" s="T4">Вот этот человек живет отдельно, очень [сильный] косун.</ta>
            <ta e="T14" id="Seg_11885" s="T10">Бездетный он.</ta>
            <ta e="T18" id="Seg_11886" s="T14">Раз в году платит подать.</ta>
            <ta e="T26" id="Seg_11887" s="T18">У берега моря, в самой тундре живет, топит очаг плавником.</ta>
            <ta e="T29" id="Seg_11888" s="T26">Долго жил, состарился.</ta>
            <ta e="T32" id="Seg_11889" s="T29">Год к весне шел.</ta>
            <ta e="T34" id="Seg_11890" s="T32">Говорит он старухе: </ta>
            <ta e="T37" id="Seg_11891" s="T34">— Уже потеплело.</ta>
            <ta e="T41" id="Seg_11892" s="T37">Поеду заготавливать дрова на будущий год.</ta>
            <ta e="T44" id="Seg_11893" s="T41">Приготовь тридцать нарт.</ta>
            <ta e="T50" id="Seg_11894" s="T44">На тридцати нартах совсем один поехал к морю.</ta>
            <ta e="T54" id="Seg_11895" s="T50">До моря доехал длинным мысом.</ta>
            <ta e="T63" id="Seg_11896" s="T54">Нагрузил нарты, не сходя с места, поднялся на высокий берег по руслу одной речки.</ta>
            <ta e="T70" id="Seg_11897" s="T63">Выехал, сел на горе и курит.</ta>
            <ta e="T81" id="Seg_11898" s="T70">Посмотрел в сторону речки, а у воды ходит ребенок, на талой воде играет пеной.</ta>
            <ta e="T84" id="Seg_11899" s="T81">Видно, мальчик.</ta>
            <ta e="T88" id="Seg_11900" s="T84">Откуда взялся — неизвестно.</ta>
            <ta e="T89" id="Seg_11901" s="T88">Удивился.</ta>
            <ta e="T93" id="Seg_11902" s="T89">— У меня же нет детей!</ta>
            <ta e="T97" id="Seg_11903" s="T93">Взять бы [его], — подумал.</ta>
            <ta e="T106" id="Seg_11904" s="T97">Тот мальчик ходит по песчаному островку, где стоит накренившееся дерево, [потом] забрался [на дерево].</ta>
            <ta e="T117" id="Seg_11905" s="T106">Старик подскочил, ухватил мальчика за загривок, сунул за пазуху в сокуй и подпоясался.</ta>
            <ta e="T120" id="Seg_11906" s="T117">Ну вот, двинулся опять в путь.</ta>
            <ta e="T123" id="Seg_11907" s="T120">Под полозьями затрещало.</ta>
            <ta e="T124" id="Seg_11908" s="T123">Остановился.</ta>
            <ta e="T143" id="Seg_11909" s="T137">Приводит передового оленя и тут же его забивает: </ta>
            <ta e="T152" id="Seg_11910" s="T143">— Взял ваше дитя, в дар за него оставил одного оленя, — сказал.</ta>
            <ta e="T156" id="Seg_11911" s="T152">Привез домой, старухе своей [все] рассказал.</ta>
            <ta e="T165" id="Seg_11912" s="T156">Целых три дня, даже не успев распрячь оленей, тешатся этим ребенком — </ta>
            <ta e="T170" id="Seg_11913" s="T165">вся упряжка так и лежит.</ta>
            <ta e="T173" id="Seg_11914" s="T170">Никто к ним не заезжает.</ta>
            <ta e="T178" id="Seg_11915" s="T173">Мальчик уже подрос, сам бегает.</ta>
            <ta e="T188" id="Seg_11916" s="T178">Однажды видит: едет одетый во все белое человек, в сани пять белых самцов впряжено.</ta>
            <ta e="T190" id="Seg_11917" s="T188">Оказалось, князь.</ta>
            <ta e="T195" id="Seg_11918" s="T190">"Ну, что я ему дам?" — думает.</ta>
            <ta e="T198" id="Seg_11919" s="T195">— Старик, как жил?</ta>
            <ta e="T206" id="Seg_11920" s="T198">Долго же меня не было, уже и ребенком успели обзавестись, — говорит князь.</ta>
            <ta e="T211" id="Seg_11921" s="T206">— Вот на старости лет обзавелись!— отвечает старик,</ta>
            <ta e="T213" id="Seg_11922" s="T211">— Ну хорошо.</ta>
            <ta e="T218" id="Seg_11923" s="T213">Приехал подати собирать, — говорит князь. </ta>
            <ta e="T224" id="Seg_11924" s="T218">Старик отдал очень большой мешок песцовых шкур. </ta>
            <ta e="T230" id="Seg_11925" s="T224">Князь радуется, за часть [пушнины] обещает прислать денег. </ta>
            <ta e="T232" id="Seg_11926" s="T230">Собрался уезжать.</ta>
            <ta e="T238" id="Seg_11927" s="T232">Помчался куда-то к ненецкому князю, своему другу.</ta>
            <ta e="T241" id="Seg_11928" s="T238">Князь пожаловал к князю.</ta>
            <ta e="T243" id="Seg_11929" s="T241">Пьянствуют.</ta>
            <ta e="T247" id="Seg_11930" s="T243">Пьют несколько дней.</ta>
            <ta e="T252" id="Seg_11931" s="T247">Вот в это время другу рассказывает: </ta>
            <ta e="T257" id="Seg_11932" s="T252">— Вот, мой друг, есть у меня опасный человек.</ta>
            <ta e="T262" id="Seg_11933" s="T257">Но подати даже переплачивает. </ta>
            <ta e="T264" id="Seg_11934" s="T262">От него еду.</ta>
            <ta e="T268" id="Seg_11935" s="T264">Удивляюсь, у него появился ребенок, который уже бегает.</ta>
            <ta e="T272" id="Seg_11936" s="T268">Hе родное его дитя. </ta>
            <ta e="T275" id="Seg_11937" s="T272">Где мог взять? </ta>
            <ta e="T280" id="Seg_11938" s="T275">Видно, кого-то убил и взял?!</ta>
            <ta e="T290" id="Seg_11939" s="T280">— Да, — говорит ненецкий князь, — у меня давно перемело целое стойбище.</ta>
            <ta e="T296" id="Seg_11940" s="T290">Говорили, что остался в живых только один грудной ребенок. </ta>
            <ta e="T301" id="Seg_11941" s="T296">Велел его не брать, боялся, что за ним болезнь последует. </ta>
            <ta e="T303" id="Seg_11942" s="T301">Его, видно, и взял. </ta>
            <ta e="T307" id="Seg_11943" s="T303">И скрыл, оказывается, того ребенка. </ta>
            <ta e="T310" id="Seg_11944" s="T307">Опасался, что его самого убьют. </ta>
            <ta e="T312" id="Seg_11945" s="T310">Вот что. </ta>
            <ta e="T316" id="Seg_11946" s="T312">Советую тебе не опасаться того человека. </ta>
            <ta e="T329" id="Seg_11947" s="T316">Мы с тобой два князя, давай напишем начальству жалобу, что он перебил целое стойбище и забрал ребенка.</ta>
            <ta e="T346" id="Seg_11948" s="T329">Нашему человеку начальство не откажет, засудит его, отправит в тюрьму, а мы заберем его богатство, — сказал ненецкий князь.</ta>
            <ta e="T353" id="Seg_11949" s="T346">— Зря оговаривать человека ни худо ли? — говорит нганасанский князь. </ta>
            <ta e="T362" id="Seg_11950" s="T353">— Вызвать бы и долганского князя, трем князьям доверия больше было бы.</ta>
            <ta e="T364" id="Seg_11951" s="T362">Ненецкий князь сказал: </ta>
            <ta e="T371" id="Seg_11952" s="T364">— Пошлю человека за долганским князем, через трое суток приедет.</ta>
            <ta e="T374" id="Seg_11953" s="T371">Если не согласится, то его самого убьем. </ta>
            <ta e="T377" id="Seg_11954" s="T374">Против не пойдет.</ta>
            <ta e="T383" id="Seg_11955" s="T377">Послал человека на оленьей упряжке за долганским князем.</ta>
            <ta e="T390" id="Seg_11956" s="T383">На третий день приехал долганский князь, вся одежда его расшита бисером.</ta>
            <ta e="T397" id="Seg_11957" s="T394">Ненецкий князь говорит: </ta>
            <ta e="T405" id="Seg_11958" s="T397">— Один нганасанин взял ребенка в вымершем от мора стойвище.</ta>
            <ta e="T408" id="Seg_11959" s="T405">Болезнь за ним последует. </ta>
            <ta e="T413" id="Seg_11960" s="T408">Давайте подадим жалобу начальству, напишем втроем.</ta>
            <ta e="T417" id="Seg_11961" s="T413">Долганский князь, призадумавшись, говорит: </ta>
            <ta e="T419" id="Seg_11962" s="T417">Судить-то мы можем. </ta>
            <ta e="T427" id="Seg_11963" s="T419">Но оговорить, что он перебил людей, — это будет слишком. </ta>
            <ta e="T434" id="Seg_11964" s="T427">Забрал сироту, так в этом вины нет, чтобы подавать на него в суд.</ta>
            <ta e="T436" id="Seg_11965" s="T434">Ненецкий князь: </ta>
            <ta e="T441" id="Seg_11966" s="T436">Ты защищаешь того человека? </ta>
            <ta e="T445" id="Seg_11967" s="T441">Тогда мы тебя самого заколем. </ta>
            <ta e="T448" id="Seg_11968" s="T445">Заберем твое богатство, — говорит.</ta>
            <ta e="T451" id="Seg_11969" s="T448">Долганский князь испугался. </ta>
            <ta e="T454" id="Seg_11970" s="T451">Оба ведь убийцы.</ta>
            <ta e="T458" id="Seg_11971" s="T454">— Ну, что посоветуете? — спросил.</ta>
            <ta e="T460" id="Seg_11972" s="T458">Ненецкий князь говорит: </ta>
            <ta e="T464" id="Seg_11973" s="T460">— Я главный советчик.</ta>
            <ta e="T476" id="Seg_11974" s="T464">Подадим жалобу царю, напишем, что этот косун перебил нганасанское стойбище и забрал ребенка.</ta>
            <ta e="T487" id="Seg_11975" s="T476">Мои люди пойдут и постреляют трупы, комиссия приедет и не разберется.</ta>
            <ta e="T490" id="Seg_11976" s="T487">Кто догадается?!</ta>
            <ta e="T497" id="Seg_11977" s="T490">Человек с десять пошли и постреляли давно умерших людей.</ta>
            <ta e="T500" id="Seg_11978" s="T497">Так и написали.</ta>
            <ta e="T505" id="Seg_11979" s="T500">Послали нганасанского князя вместе с ненецким князем. </ta>
            <ta e="T515" id="Seg_11980" s="T505">Начальник читает: "Человек перебил с десяток чумов людей, украл ребенка". </ta>
            <ta e="T518" id="Seg_11981" s="T515">Начальник спрашивает у них: </ta>
            <ta e="T520" id="Seg_11982" s="T518">— Правда ли это? </ta>
            <ta e="T525" id="Seg_11983" s="T520">Я буду судить его при вас.</ta>
            <ta e="T533" id="Seg_11984" s="T525">Нганасанский князь: — Нелюдимый человек. Наверника, его это дело — говорит.</ta>
            <ta e="T537" id="Seg_11985" s="T533">Старику посылают подложное письмо.</ta>
            <ta e="T548" id="Seg_11986" s="T537">Князь ему пишет, чтоб очень спешно приехал получить деньги за подать.</ta>
            <ta e="T552" id="Seg_11987" s="T548">Посыльный доставляет письмо.</ta>
            <ta e="T558" id="Seg_11988" s="T552">Старик примчался на самых лучших оленях.</ta>
            <ta e="T563" id="Seg_11989" s="T558">Привязывает оленей и тут же входит в чум.</ta>
            <ta e="T567" id="Seg_11990" s="T563">Вокруг стола полно начальников.</ta>
            <ta e="T575" id="Seg_11991" s="T567">Начальник даже чаем не напоил, к лицу кровь прилила, совсем злой.</ta>
            <ta e="T578" id="Seg_11992" s="T575">— Ну, рассказывай о своих новостях.</ta>
            <ta e="T582" id="Seg_11993" s="T578">Зачем перебил целое стойбище людей?</ta>
            <ta e="T590" id="Seg_11994" s="T582">— Что за стойбище перебил, говорите?</ta>
            <ta e="T594" id="Seg_11995" s="T590">Начальник показал письмо князей: </ta>
            <ta e="T604" id="Seg_11996" s="T594">— До половодья следующего года будешь сидеть в тюрьме, а там отошло тебя к еще большему начальнику.</ta>
            <ta e="T608" id="Seg_11997" s="T604">Старика тут же послали в тюрьму.</ta>
            <ta e="T612" id="Seg_11998" s="T608">Три князя готовятся разделить его богатство.</ta>
            <ta e="T621" id="Seg_11999" s="T612">Прикочевала старуха — старика нет, сидит в тюрьме.</ta>
            <ta e="T625" id="Seg_12000" s="T621">Начальник старуху выгнал.</ta>
            <ta e="T635" id="Seg_12001" s="T625">Три князя тут же разделили между собой ее богатство, оставили ей одну покрышку от чума.</ta>
            <ta e="T640" id="Seg_12002" s="T635">Кончилась зима, приплывает сверху пароход.</ta>
            <ta e="T644" id="Seg_12003" s="T640">— Ну, посмотри на своего мужа, — говорят.</ta>
            <ta e="T652" id="Seg_12004" s="T644">Глаза старика будто в яму провалились, плохо кормились, совсем обессилел.</ta>
            <ta e="T655" id="Seg_12005" s="T652">Бросили в трюм парохода.</ta>
            <ta e="T663" id="Seg_12006" s="T655">Повезли к другому начальнику, вносят на носилках умирающего человека.</ta>
            <ta e="T668" id="Seg_12007" s="T663">— Что это? Как буду судить умирающего человека?</ta>
            <ta e="T672" id="Seg_12008" s="T668">Увезите в больницу, пусть накормят, призреют.</ta>
            <ta e="T677" id="Seg_12009" s="T672">Доктора лечат, прежний вид принимает.</ta>
            <ta e="T679" id="Seg_12010" s="T677">Доктора спрашивают: </ta>
            <ta e="T683" id="Seg_12011" s="T679">— Виноват или оговаривают тебя?</ta>
            <ta e="T686" id="Seg_12012" s="T683">Старик все рассказывает.</ta>
            <ta e="T689" id="Seg_12013" s="T686">Доктора берут у него кровь: </ta>
            <ta e="T695" id="Seg_12014" s="T689">— Если виноват, кровь должна быть иной, — говорят.</ta>
            <ta e="T701" id="Seg_12015" s="T695">Оказывается, ни на одну капельку не виноват.</ta>
            <ta e="T703" id="Seg_12016" s="T701"> Дают письмо: </ta>
            <ta e="T707" id="Seg_12017" s="T703">— Это передай прямо в суд.</ta>
            <ta e="T712" id="Seg_12018" s="T707">Возьмем кровь и у князей, — говорят.</ta>
            <ta e="T714" id="Seg_12019" s="T712">Вот стали судить [старика].</ta>
            <ta e="T720" id="Seg_12020" s="T714">Князья, первый начальник, много народу собралось.</ta>
            <ta e="T722" id="Seg_12021" s="T720">Начальник говорит: </ta>
            <ta e="T725" id="Seg_12022" s="T722">— Давайте послушаем его слова.</ta>
            <ta e="T727" id="Seg_12023" s="T725">Старик рассказывает: </ta>
            <ta e="T733" id="Seg_12024" s="T727">— Я в жизни ни с одним человеком не поссорится, </ta>
            <ta e="T741" id="Seg_12025" s="T733">— и дает справку: "Этот человек и на капельку не виноват".</ta>
            <ta e="T744" id="Seg_12026" s="T741">Берут кровь у князей.</ta>
            <ta e="T753" id="Seg_12027" s="T744">Кровь нганасанского косуна совершенно другая, у князей совсем иная.</ta>
            <ta e="T756" id="Seg_12028" s="T753"> Тут начальник разгневался: </ta>
            <ta e="T758" id="Seg_12029" s="T756">— Кто посоветовал?</ta>
            <ta e="T759" id="Seg_12030" s="T758">Расскажите!</ta>
            <ta e="T762" id="Seg_12031" s="T759">Долганский князь рассказывает: </ta>
            <ta e="T766" id="Seg_12032" s="T762">— Правду сказать, мы виноваты.</ta>
            <ta e="T771" id="Seg_12033" s="T766">Самый главный виновник — ненецкий князь.</ta>
            <ta e="T777" id="Seg_12034" s="T771">Меня могли убить, со страху и дал свою подпись.</ta>
            <ta e="T778" id="Seg_12035" s="T777">Начальник говорит: </ta>
            <ta e="T784" id="Seg_12036" s="T778">— Ну, старик, ты, оказывается, не виноват.</ta>
            <ta e="T789" id="Seg_12037" s="T784">В тюрьму посажу начальника, который тебя засудил.</ta>
            <ta e="T799" id="Seg_12038" s="T789">Ты вместо него начальником поедешь туда, сам и суди князей.</ta>
            <ta e="T802" id="Seg_12039" s="T799">Уезжают обратно.</ta>
            <ta e="T807" id="Seg_12040" s="T802">Старик в своей стороне князем стал.</ta>
            <ta e="T812" id="Seg_12041" s="T807">Сын вырос, ну прямо-таки богатырь.</ta>
            <ta e="T820" id="Seg_12042" s="T812">Пока [отца] не было, хотели его убить, опасаясь, что станет таким же, как отец.</ta>
            <ta e="T828" id="Seg_12043" s="T820">Парень одного [из нападавших] сшиб с ног, за это посадили в тюрьму.</ta>
            <ta e="T830" id="Seg_12044" s="T828">Стал [старик] судить тех князей.</ta>
            <ta e="T836" id="Seg_12045" s="T830">Нганасанского и ненецкою посадил в тюрьму.</ta>
            <ta e="T840" id="Seg_12046" s="T836">Долганского князя снял с должности.</ta>
            <ta e="T844" id="Seg_12047" s="T840">Сына назначил нганасанским князем.</ta>
            <ta e="T848" id="Seg_12048" s="T844">Так выявилась правда старика.</ta>
            <ta e="T849" id="Seg_12049" s="T848">Конец.</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T559" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
            <conversion-tli id="T571" />
            <conversion-tli id="T572" />
            <conversion-tli id="T573" />
            <conversion-tli id="T574" />
            <conversion-tli id="T575" />
            <conversion-tli id="T576" />
            <conversion-tli id="T577" />
            <conversion-tli id="T578" />
            <conversion-tli id="T579" />
            <conversion-tli id="T580" />
            <conversion-tli id="T581" />
            <conversion-tli id="T582" />
            <conversion-tli id="T583" />
            <conversion-tli id="T584" />
            <conversion-tli id="T585" />
            <conversion-tli id="T586" />
            <conversion-tli id="T587" />
            <conversion-tli id="T588" />
            <conversion-tli id="T589" />
            <conversion-tli id="T590" />
            <conversion-tli id="T591" />
            <conversion-tli id="T592" />
            <conversion-tli id="T593" />
            <conversion-tli id="T594" />
            <conversion-tli id="T595" />
            <conversion-tli id="T596" />
            <conversion-tli id="T597" />
            <conversion-tli id="T598" />
            <conversion-tli id="T599" />
            <conversion-tli id="T600" />
            <conversion-tli id="T601" />
            <conversion-tli id="T602" />
            <conversion-tli id="T603" />
            <conversion-tli id="T604" />
            <conversion-tli id="T605" />
            <conversion-tli id="T606" />
            <conversion-tli id="T607" />
            <conversion-tli id="T608" />
            <conversion-tli id="T609" />
            <conversion-tli id="T610" />
            <conversion-tli id="T611" />
            <conversion-tli id="T612" />
            <conversion-tli id="T613" />
            <conversion-tli id="T614" />
            <conversion-tli id="T615" />
            <conversion-tli id="T616" />
            <conversion-tli id="T617" />
            <conversion-tli id="T618" />
            <conversion-tli id="T619" />
            <conversion-tli id="T620" />
            <conversion-tli id="T621" />
            <conversion-tli id="T622" />
            <conversion-tli id="T623" />
            <conversion-tli id="T624" />
            <conversion-tli id="T625" />
            <conversion-tli id="T626" />
            <conversion-tli id="T627" />
            <conversion-tli id="T628" />
            <conversion-tli id="T629" />
            <conversion-tli id="T630" />
            <conversion-tli id="T631" />
            <conversion-tli id="T632" />
            <conversion-tli id="T633" />
            <conversion-tli id="T634" />
            <conversion-tli id="T635" />
            <conversion-tli id="T636" />
            <conversion-tli id="T637" />
            <conversion-tli id="T638" />
            <conversion-tli id="T639" />
            <conversion-tli id="T640" />
            <conversion-tli id="T641" />
            <conversion-tli id="T642" />
            <conversion-tli id="T643" />
            <conversion-tli id="T644" />
            <conversion-tli id="T645" />
            <conversion-tli id="T646" />
            <conversion-tli id="T647" />
            <conversion-tli id="T648" />
            <conversion-tli id="T649" />
            <conversion-tli id="T650" />
            <conversion-tli id="T651" />
            <conversion-tli id="T652" />
            <conversion-tli id="T653" />
            <conversion-tli id="T654" />
            <conversion-tli id="T655" />
            <conversion-tli id="T656" />
            <conversion-tli id="T657" />
            <conversion-tli id="T658" />
            <conversion-tli id="T659" />
            <conversion-tli id="T660" />
            <conversion-tli id="T661" />
            <conversion-tli id="T662" />
            <conversion-tli id="T663" />
            <conversion-tli id="T664" />
            <conversion-tli id="T665" />
            <conversion-tli id="T666" />
            <conversion-tli id="T667" />
            <conversion-tli id="T668" />
            <conversion-tli id="T669" />
            <conversion-tli id="T670" />
            <conversion-tli id="T671" />
            <conversion-tli id="T672" />
            <conversion-tli id="T673" />
            <conversion-tli id="T674" />
            <conversion-tli id="T675" />
            <conversion-tli id="T676" />
            <conversion-tli id="T677" />
            <conversion-tli id="T678" />
            <conversion-tli id="T679" />
            <conversion-tli id="T680" />
            <conversion-tli id="T681" />
            <conversion-tli id="T682" />
            <conversion-tli id="T683" />
            <conversion-tli id="T684" />
            <conversion-tli id="T685" />
            <conversion-tli id="T686" />
            <conversion-tli id="T687" />
            <conversion-tli id="T688" />
            <conversion-tli id="T689" />
            <conversion-tli id="T690" />
            <conversion-tli id="T691" />
            <conversion-tli id="T692" />
            <conversion-tli id="T693" />
            <conversion-tli id="T694" />
            <conversion-tli id="T695" />
            <conversion-tli id="T696" />
            <conversion-tli id="T697" />
            <conversion-tli id="T698" />
            <conversion-tli id="T699" />
            <conversion-tli id="T700" />
            <conversion-tli id="T701" />
            <conversion-tli id="T702" />
            <conversion-tli id="T703" />
            <conversion-tli id="T704" />
            <conversion-tli id="T705" />
            <conversion-tli id="T706" />
            <conversion-tli id="T707" />
            <conversion-tli id="T708" />
            <conversion-tli id="T709" />
            <conversion-tli id="T710" />
            <conversion-tli id="T711" />
            <conversion-tli id="T712" />
            <conversion-tli id="T713" />
            <conversion-tli id="T714" />
            <conversion-tli id="T715" />
            <conversion-tli id="T716" />
            <conversion-tli id="T717" />
            <conversion-tli id="T718" />
            <conversion-tli id="T719" />
            <conversion-tli id="T720" />
            <conversion-tli id="T721" />
            <conversion-tli id="T722" />
            <conversion-tli id="T723" />
            <conversion-tli id="T724" />
            <conversion-tli id="T725" />
            <conversion-tli id="T726" />
            <conversion-tli id="T727" />
            <conversion-tli id="T728" />
            <conversion-tli id="T729" />
            <conversion-tli id="T730" />
            <conversion-tli id="T731" />
            <conversion-tli id="T732" />
            <conversion-tli id="T733" />
            <conversion-tli id="T734" />
            <conversion-tli id="T735" />
            <conversion-tli id="T736" />
            <conversion-tli id="T737" />
            <conversion-tli id="T738" />
            <conversion-tli id="T739" />
            <conversion-tli id="T740" />
            <conversion-tli id="T741" />
            <conversion-tli id="T742" />
            <conversion-tli id="T743" />
            <conversion-tli id="T744" />
            <conversion-tli id="T745" />
            <conversion-tli id="T746" />
            <conversion-tli id="T747" />
            <conversion-tli id="T748" />
            <conversion-tli id="T749" />
            <conversion-tli id="T750" />
            <conversion-tli id="T751" />
            <conversion-tli id="T752" />
            <conversion-tli id="T753" />
            <conversion-tli id="T754" />
            <conversion-tli id="T755" />
            <conversion-tli id="T756" />
            <conversion-tli id="T757" />
            <conversion-tli id="T758" />
            <conversion-tli id="T759" />
            <conversion-tli id="T760" />
            <conversion-tli id="T761" />
            <conversion-tli id="T762" />
            <conversion-tli id="T763" />
            <conversion-tli id="T764" />
            <conversion-tli id="T765" />
            <conversion-tli id="T766" />
            <conversion-tli id="T767" />
            <conversion-tli id="T768" />
            <conversion-tli id="T769" />
            <conversion-tli id="T770" />
            <conversion-tli id="T771" />
            <conversion-tli id="T772" />
            <conversion-tli id="T773" />
            <conversion-tli id="T774" />
            <conversion-tli id="T775" />
            <conversion-tli id="T776" />
            <conversion-tli id="T777" />
            <conversion-tli id="T778" />
            <conversion-tli id="T779" />
            <conversion-tli id="T780" />
            <conversion-tli id="T781" />
            <conversion-tli id="T782" />
            <conversion-tli id="T783" />
            <conversion-tli id="T784" />
            <conversion-tli id="T785" />
            <conversion-tli id="T786" />
            <conversion-tli id="T787" />
            <conversion-tli id="T788" />
            <conversion-tli id="T789" />
            <conversion-tli id="T790" />
            <conversion-tli id="T791" />
            <conversion-tli id="T792" />
            <conversion-tli id="T793" />
            <conversion-tli id="T794" />
            <conversion-tli id="T795" />
            <conversion-tli id="T796" />
            <conversion-tli id="T797" />
            <conversion-tli id="T798" />
            <conversion-tli id="T799" />
            <conversion-tli id="T800" />
            <conversion-tli id="T801" />
            <conversion-tli id="T802" />
            <conversion-tli id="T803" />
            <conversion-tli id="T804" />
            <conversion-tli id="T805" />
            <conversion-tli id="T806" />
            <conversion-tli id="T807" />
            <conversion-tli id="T808" />
            <conversion-tli id="T809" />
            <conversion-tli id="T810" />
            <conversion-tli id="T811" />
            <conversion-tli id="T812" />
            <conversion-tli id="T813" />
            <conversion-tli id="T814" />
            <conversion-tli id="T815" />
            <conversion-tli id="T816" />
            <conversion-tli id="T817" />
            <conversion-tli id="T818" />
            <conversion-tli id="T819" />
            <conversion-tli id="T820" />
            <conversion-tli id="T821" />
            <conversion-tli id="T822" />
            <conversion-tli id="T823" />
            <conversion-tli id="T824" />
            <conversion-tli id="T825" />
            <conversion-tli id="T826" />
            <conversion-tli id="T827" />
            <conversion-tli id="T828" />
            <conversion-tli id="T829" />
            <conversion-tli id="T830" />
            <conversion-tli id="T831" />
            <conversion-tli id="T832" />
            <conversion-tli id="T833" />
            <conversion-tli id="T834" />
            <conversion-tli id="T835" />
            <conversion-tli id="T836" />
            <conversion-tli id="T837" />
            <conversion-tli id="T838" />
            <conversion-tli id="T839" />
            <conversion-tli id="T840" />
            <conversion-tli id="T841" />
            <conversion-tli id="T842" />
            <conversion-tli id="T843" />
            <conversion-tli id="T844" />
            <conversion-tli id="T845" />
            <conversion-tli id="T846" />
            <conversion-tli id="T847" />
            <conversion-tli id="T848" />
            <conversion-tli id="T849" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg"
                          name="gg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top"
                          name="Top"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc"
                          name="Foc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
