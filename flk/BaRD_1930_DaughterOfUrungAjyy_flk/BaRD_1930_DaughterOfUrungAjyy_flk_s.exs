<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID73CDC787-5FEC-4672-450C-74115CB2A865">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>BaRD_1930_CourtingDaughterOfÜrüngAjyy_flk</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\flk\BaRD_1930_DaughterOfUrungAjyy_flk\BaRD_1930_DaughterOfUrungAjyy_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">548</ud-information>
            <ud-information attribute-name="# HIAT:w">411</ud-information>
            <ud-information attribute-name="# e">403</ud-information>
            <ud-information attribute-name="# HIAT:u">59</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="BaRD">
            <abbreviation>BaRD</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="4.0" type="appl" />
         <tli id="T1" time="4.5" type="appl" />
         <tli id="T2" time="5.0" type="appl" />
         <tli id="T3" time="5.5" type="appl" />
         <tli id="T4" time="6.0" type="appl" />
         <tli id="T5" time="6.5" type="appl" />
         <tli id="T6" time="7.0" type="appl" />
         <tli id="T7" time="7.5" type="appl" />
         <tli id="T8" time="8.0" type="appl" />
         <tli id="T9" time="8.5" type="appl" />
         <tli id="T10" time="9.0" type="appl" />
         <tli id="T11" time="9.5" type="appl" />
         <tli id="T12" time="10.0" type="appl" />
         <tli id="T13" time="10.5" type="appl" />
         <tli id="T14" time="11.0" type="appl" />
         <tli id="T15" time="11.5" type="appl" />
         <tli id="T16" time="12.0" type="appl" />
         <tli id="T17" time="12.5" type="appl" />
         <tli id="T18" time="13.0" type="appl" />
         <tli id="T19" time="13.5" type="appl" />
         <tli id="T20" time="14.0" type="appl" />
         <tli id="T21" time="14.5" type="appl" />
         <tli id="T22" time="15.0" type="appl" />
         <tli id="T23" time="15.5" type="appl" />
         <tli id="T24" time="16.0" type="appl" />
         <tli id="T25" time="16.5" type="appl" />
         <tli id="T26" time="17.0" type="appl" />
         <tli id="T27" time="17.5" type="appl" />
         <tli id="T28" time="18.0" type="appl" />
         <tli id="T29" time="18.5" type="appl" />
         <tli id="T30" time="19.0" type="appl" />
         <tli id="T31" time="19.5" type="appl" />
         <tli id="T32" time="20.0" type="appl" />
         <tli id="T33" time="20.5" type="appl" />
         <tli id="T34" time="21.0" type="appl" />
         <tli id="T35" time="21.5" type="appl" />
         <tli id="T36" time="22.0" type="appl" />
         <tli id="T37" time="22.5" type="appl" />
         <tli id="T38" time="23.0" type="appl" />
         <tli id="T39" time="23.5" type="appl" />
         <tli id="T40" time="24.0" type="appl" />
         <tli id="T41" time="24.5" type="appl" />
         <tli id="T42" time="25.0" type="appl" />
         <tli id="T43" time="25.5" type="appl" />
         <tli id="T44" time="26.0" type="appl" />
         <tli id="T45" time="26.5" type="appl" />
         <tli id="T46" time="27.0" type="appl" />
         <tli id="T47" time="27.5" type="appl" />
         <tli id="T48" time="28.0" type="appl" />
         <tli id="T49" time="28.5" type="appl" />
         <tli id="T50" time="29.0" type="appl" />
         <tli id="T51" time="29.5" type="appl" />
         <tli id="T52" time="30.0" type="appl" />
         <tli id="T53" time="30.5" type="appl" />
         <tli id="T54" time="31.0" type="appl" />
         <tli id="T55" time="31.5" type="appl" />
         <tli id="T56" time="32.0" type="appl" />
         <tli id="T57" time="32.5" type="appl" />
         <tli id="T58" time="33.0" type="appl" />
         <tli id="T59" time="33.5" type="appl" />
         <tli id="T60" time="34.0" type="appl" />
         <tli id="T61" time="34.5" type="appl" />
         <tli id="T62" time="35.0" type="appl" />
         <tli id="T63" time="35.5" type="appl" />
         <tli id="T64" time="36.0" type="appl" />
         <tli id="T65" time="36.5" type="appl" />
         <tli id="T66" time="37.0" type="appl" />
         <tli id="T67" time="37.5" type="appl" />
         <tli id="T68" time="38.0" type="appl" />
         <tli id="T69" time="38.5" type="appl" />
         <tli id="T70" time="39.0" type="appl" />
         <tli id="T71" time="39.5" type="appl" />
         <tli id="T72" time="40.0" type="appl" />
         <tli id="T73" time="40.5" type="appl" />
         <tli id="T74" time="41.0" type="appl" />
         <tli id="T75" time="41.5" type="appl" />
         <tli id="T76" time="42.0" type="appl" />
         <tli id="T77" time="42.5" type="appl" />
         <tli id="T78" time="43.0" type="appl" />
         <tli id="T79" time="43.5" type="appl" />
         <tli id="T80" time="44.0" type="appl" />
         <tli id="T81" time="44.5" type="appl" />
         <tli id="T82" time="45.0" type="appl" />
         <tli id="T83" time="45.5" type="appl" />
         <tli id="T84" time="46.0" type="appl" />
         <tli id="T85" time="46.5" type="appl" />
         <tli id="T86" time="47.0" type="appl" />
         <tli id="T87" time="47.5" type="appl" />
         <tli id="T88" time="48.0" type="appl" />
         <tli id="T89" time="48.5" type="appl" />
         <tli id="T90" time="49.0" type="appl" />
         <tli id="T91" time="49.5" type="appl" />
         <tli id="T92" time="50.0" type="appl" />
         <tli id="T93" time="50.5" type="appl" />
         <tli id="T94" time="51.0" type="appl" />
         <tli id="T95" time="51.5" type="appl" />
         <tli id="T96" time="52.0" type="appl" />
         <tli id="T97" time="52.5" type="appl" />
         <tli id="T98" time="53.0" type="appl" />
         <tli id="T99" time="53.5" type="appl" />
         <tli id="T100" time="54.0" type="appl" />
         <tli id="T101" time="54.5" type="appl" />
         <tli id="T102" time="55.0" type="appl" />
         <tli id="T103" time="55.5" type="appl" />
         <tli id="T104" time="56.0" type="appl" />
         <tli id="T105" time="56.5" type="appl" />
         <tli id="T106" time="57.0" type="appl" />
         <tli id="T107" time="57.5" type="appl" />
         <tli id="T108" time="58.0" type="appl" />
         <tli id="T109" time="58.5" type="appl" />
         <tli id="T110" time="59.0" type="appl" />
         <tli id="T111" time="59.5" type="appl" />
         <tli id="T112" time="60.0" type="appl" />
         <tli id="T113" time="60.5" type="appl" />
         <tli id="T114" time="61.0" type="appl" />
         <tli id="T115" time="61.5" type="appl" />
         <tli id="T116" time="62.0" type="appl" />
         <tli id="T117" time="62.5" type="appl" />
         <tli id="T118" time="63.0" type="appl" />
         <tli id="T119" time="63.5" type="appl" />
         <tli id="T120" time="64.0" type="appl" />
         <tli id="T121" time="64.5" type="appl" />
         <tli id="T122" time="65.0" type="appl" />
         <tli id="T123" time="65.5" type="appl" />
         <tli id="T124" time="66.0" type="appl" />
         <tli id="T125" time="66.5" type="appl" />
         <tli id="T126" time="67.0" type="appl" />
         <tli id="T127" time="67.5" type="appl" />
         <tli id="T128" time="68.0" type="appl" />
         <tli id="T129" time="68.5" type="appl" />
         <tli id="T130" time="69.0" type="appl" />
         <tli id="T131" time="69.5" type="appl" />
         <tli id="T132" time="70.0" type="appl" />
         <tli id="T133" time="70.5" type="appl" />
         <tli id="T134" time="71.0" type="appl" />
         <tli id="T135" time="71.5" type="appl" />
         <tli id="T136" time="72.0" type="appl" />
         <tli id="T137" time="72.5" type="appl" />
         <tli id="T138" time="73.0" type="appl" />
         <tli id="T139" time="73.5" type="appl" />
         <tli id="T140" time="74.0" type="appl" />
         <tli id="T141" time="74.5" type="appl" />
         <tli id="T142" time="75.0" type="appl" />
         <tli id="T143" time="75.5" type="appl" />
         <tli id="T144" time="76.0" type="appl" />
         <tli id="T145" time="76.5" type="appl" />
         <tli id="T146" time="77.0" type="appl" />
         <tli id="T147" time="77.5" type="appl" />
         <tli id="T148" time="78.0" type="appl" />
         <tli id="T149" time="78.5" type="appl" />
         <tli id="T150" time="79.0" type="appl" />
         <tli id="T151" time="79.5" type="appl" />
         <tli id="T152" time="80.0" type="appl" />
         <tli id="T153" time="80.5" type="appl" />
         <tli id="T154" time="81.0" type="appl" />
         <tli id="T155" time="81.5" type="appl" />
         <tli id="T156" time="82.0" type="appl" />
         <tli id="T157" time="82.5" type="appl" />
         <tli id="T158" time="83.0" type="appl" />
         <tli id="T159" time="83.5" type="appl" />
         <tli id="T160" time="84.0" type="appl" />
         <tli id="T161" time="84.5" type="appl" />
         <tli id="T162" time="85.0" type="appl" />
         <tli id="T163" time="85.5" type="appl" />
         <tli id="T164" time="86.0" type="appl" />
         <tli id="T165" time="86.5" type="appl" />
         <tli id="T166" time="87.0" type="appl" />
         <tli id="T167" time="87.5" type="appl" />
         <tli id="T168" time="88.0" type="appl" />
         <tli id="T169" time="88.5" type="appl" />
         <tli id="T170" time="89.0" type="appl" />
         <tli id="T171" time="89.5" type="appl" />
         <tli id="T172" time="90.0" type="appl" />
         <tli id="T173" time="90.5" type="appl" />
         <tli id="T174" time="91.0" type="appl" />
         <tli id="T175" time="91.5" type="appl" />
         <tli id="T176" time="92.0" type="appl" />
         <tli id="T177" time="92.5" type="appl" />
         <tli id="T178" time="93.0" type="appl" />
         <tli id="T179" time="93.5" type="appl" />
         <tli id="T180" time="94.0" type="appl" />
         <tli id="T181" time="94.5" type="appl" />
         <tli id="T182" time="95.0" type="appl" />
         <tli id="T183" time="95.5" type="appl" />
         <tli id="T184" time="96.0" type="appl" />
         <tli id="T185" time="96.5" type="appl" />
         <tli id="T186" time="97.0" type="appl" />
         <tli id="T187" time="97.5" type="appl" />
         <tli id="T188" time="98.0" type="appl" />
         <tli id="T189" time="98.5" type="appl" />
         <tli id="T190" time="99.0" type="appl" />
         <tli id="T191" time="99.5" type="appl" />
         <tli id="T192" time="100.0" type="appl" />
         <tli id="T193" time="100.5" type="appl" />
         <tli id="T194" time="101.0" type="appl" />
         <tli id="T195" time="101.5" type="appl" />
         <tli id="T196" time="102.0" type="appl" />
         <tli id="T197" time="102.5" type="appl" />
         <tli id="T198" time="103.0" type="appl" />
         <tli id="T199" time="103.5" type="appl" />
         <tli id="T200" time="104.0" type="appl" />
         <tli id="T201" time="104.5" type="appl" />
         <tli id="T202" time="105.0" type="appl" />
         <tli id="T203" time="105.5" type="appl" />
         <tli id="T204" time="106.0" type="appl" />
         <tli id="T205" time="106.5" type="appl" />
         <tli id="T206" time="107.0" type="appl" />
         <tli id="T207" time="107.5" type="appl" />
         <tli id="T208" time="108.0" type="appl" />
         <tli id="T209" time="108.5" type="appl" />
         <tli id="T210" time="109.0" type="appl" />
         <tli id="T211" time="109.5" type="appl" />
         <tli id="T212" time="110.0" type="appl" />
         <tli id="T213" time="110.5" type="appl" />
         <tli id="T214" time="111.0" type="appl" />
         <tli id="T215" time="111.5" type="appl" />
         <tli id="T216" time="112.0" type="appl" />
         <tli id="T217" time="112.5" type="appl" />
         <tli id="T218" time="113.0" type="appl" />
         <tli id="T219" time="113.5" type="appl" />
         <tli id="T220" time="114.0" type="appl" />
         <tli id="T221" time="114.5" type="appl" />
         <tli id="T222" time="115.0" type="appl" />
         <tli id="T223" time="115.5" type="appl" />
         <tli id="T224" time="116.0" type="appl" />
         <tli id="T225" time="116.5" type="appl" />
         <tli id="T226" time="117.0" type="appl" />
         <tli id="T227" time="117.5" type="appl" />
         <tli id="T228" time="118.0" type="appl" />
         <tli id="T229" time="118.5" type="appl" />
         <tli id="T230" time="119.0" type="appl" />
         <tli id="T231" time="119.5" type="appl" />
         <tli id="T232" time="120.0" type="appl" />
         <tli id="T233" time="120.5" type="appl" />
         <tli id="T234" time="121.0" type="appl" />
         <tli id="T235" time="121.5" type="appl" />
         <tli id="T236" time="122.0" type="appl" />
         <tli id="T237" time="122.5" type="appl" />
         <tli id="T238" time="123.0" type="appl" />
         <tli id="T239" time="123.5" type="appl" />
         <tli id="T240" time="124.0" type="appl" />
         <tli id="T241" time="124.5" type="appl" />
         <tli id="T242" time="125.0" type="appl" />
         <tli id="T243" time="125.5" type="appl" />
         <tli id="T244" time="126.0" type="appl" />
         <tli id="T245" time="126.5" type="appl" />
         <tli id="T246" time="127.0" type="appl" />
         <tli id="T247" time="127.5" type="appl" />
         <tli id="T248" time="128.0" type="appl" />
         <tli id="T249" time="128.5" type="appl" />
         <tli id="T250" time="129.0" type="appl" />
         <tli id="T251" time="129.5" type="appl" />
         <tli id="T252" time="130.0" type="appl" />
         <tli id="T253" time="130.5" type="appl" />
         <tli id="T254" time="131.0" type="appl" />
         <tli id="T255" time="131.5" type="appl" />
         <tli id="T256" time="132.0" type="appl" />
         <tli id="T257" time="132.5" type="appl" />
         <tli id="T258" time="133.0" type="appl" />
         <tli id="T259" time="133.5" type="appl" />
         <tli id="T260" time="134.0" type="appl" />
         <tli id="T261" time="134.5" type="appl" />
         <tli id="T262" time="135.0" type="appl" />
         <tli id="T263" time="135.5" type="appl" />
         <tli id="T264" time="136.0" type="appl" />
         <tli id="T265" time="136.5" type="appl" />
         <tli id="T266" time="137.0" type="appl" />
         <tli id="T267" time="137.5" type="appl" />
         <tli id="T268" time="138.0" type="appl" />
         <tli id="T269" time="138.5" type="appl" />
         <tli id="T270" time="139.0" type="appl" />
         <tli id="T271" time="139.5" type="appl" />
         <tli id="T272" time="140.0" type="appl" />
         <tli id="T273" time="140.5" type="appl" />
         <tli id="T274" time="141.0" type="appl" />
         <tli id="T275" time="141.5" type="appl" />
         <tli id="T276" time="142.0" type="appl" />
         <tli id="T277" time="142.5" type="appl" />
         <tli id="T278" time="143.0" type="appl" />
         <tli id="T279" time="143.5" type="appl" />
         <tli id="T280" time="144.0" type="appl" />
         <tli id="T281" time="144.5" type="appl" />
         <tli id="T282" time="145.0" type="appl" />
         <tli id="T283" time="145.5" type="appl" />
         <tli id="T284" time="146.0" type="appl" />
         <tli id="T285" time="146.5" type="appl" />
         <tli id="T286" time="147.0" type="appl" />
         <tli id="T287" time="147.5" type="appl" />
         <tli id="T288" time="148.0" type="appl" />
         <tli id="T289" time="148.5" type="appl" />
         <tli id="T290" time="149.0" type="appl" />
         <tli id="T291" time="149.5" type="appl" />
         <tli id="T292" time="150.0" type="appl" />
         <tli id="T293" time="150.5" type="appl" />
         <tli id="T294" time="151.0" type="appl" />
         <tli id="T295" time="151.5" type="appl" />
         <tli id="T296" time="152.0" type="appl" />
         <tli id="T297" time="152.5" type="appl" />
         <tli id="T298" time="153.0" type="appl" />
         <tli id="T299" time="153.5" type="appl" />
         <tli id="T300" time="154.0" type="appl" />
         <tli id="T301" time="154.5" type="appl" />
         <tli id="T302" time="155.0" type="appl" />
         <tli id="T303" time="155.5" type="appl" />
         <tli id="T304" time="156.0" type="appl" />
         <tli id="T305" time="156.5" type="appl" />
         <tli id="T306" time="157.0" type="appl" />
         <tli id="T307" time="157.5" type="appl" />
         <tli id="T308" time="158.0" type="appl" />
         <tli id="T309" time="158.5" type="appl" />
         <tli id="T310" time="159.0" type="appl" />
         <tli id="T311" time="159.5" type="appl" />
         <tli id="T312" time="160.0" type="appl" />
         <tli id="T313" time="160.5" type="appl" />
         <tli id="T314" time="161.0" type="appl" />
         <tli id="T315" time="161.5" type="appl" />
         <tli id="T316" time="162.0" type="appl" />
         <tli id="T317" time="162.5" type="appl" />
         <tli id="T318" time="163.0" type="appl" />
         <tli id="T319" time="163.5" type="appl" />
         <tli id="T320" time="164.0" type="appl" />
         <tli id="T321" time="164.5" type="appl" />
         <tli id="T322" time="165.0" type="appl" />
         <tli id="T323" time="165.5" type="appl" />
         <tli id="T324" time="166.0" type="appl" />
         <tli id="T325" time="166.5" type="appl" />
         <tli id="T326" time="167.0" type="appl" />
         <tli id="T327" time="167.5" type="appl" />
         <tli id="T328" time="168.0" type="appl" />
         <tli id="T329" time="168.5" type="appl" />
         <tli id="T330" time="169.0" type="appl" />
         <tli id="T331" time="169.5" type="appl" />
         <tli id="T332" time="170.0" type="appl" />
         <tli id="T333" time="170.5" type="appl" />
         <tli id="T334" time="171.0" type="appl" />
         <tli id="T335" time="171.5" type="appl" />
         <tli id="T336" time="172.0" type="appl" />
         <tli id="T337" time="172.5" type="appl" />
         <tli id="T338" time="173.0" type="appl" />
         <tli id="T339" time="173.5" type="appl" />
         <tli id="T340" time="174.0" type="appl" />
         <tli id="T341" time="174.5" type="appl" />
         <tli id="T342" time="175.0" type="appl" />
         <tli id="T343" time="175.5" type="appl" />
         <tli id="T344" time="176.0" type="appl" />
         <tli id="T345" time="176.5" type="appl" />
         <tli id="T346" time="177.0" type="appl" />
         <tli id="T347" time="177.5" type="appl" />
         <tli id="T348" time="178.0" type="appl" />
         <tli id="T349" time="178.5" type="appl" />
         <tli id="T350" time="179.0" type="appl" />
         <tli id="T351" time="179.5" type="appl" />
         <tli id="T352" time="180.0" type="appl" />
         <tli id="T353" time="180.5" type="appl" />
         <tli id="T354" time="181.0" type="appl" />
         <tli id="T355" time="181.5" type="appl" />
         <tli id="T356" time="182.0" type="appl" />
         <tli id="T357" time="182.5" type="appl" />
         <tli id="T358" time="183.0" type="appl" />
         <tli id="T359" time="183.5" type="appl" />
         <tli id="T360" time="184.0" type="appl" />
         <tli id="T361" time="184.5" type="appl" />
         <tli id="T362" time="185.0" type="appl" />
         <tli id="T363" time="185.5" type="appl" />
         <tli id="T364" time="186.0" type="appl" />
         <tli id="T365" time="186.5" type="appl" />
         <tli id="T366" time="187.0" type="appl" />
         <tli id="T367" time="187.5" type="appl" />
         <tli id="T368" time="188.0" type="appl" />
         <tli id="T369" time="188.5" type="appl" />
         <tli id="T370" time="189.0" type="appl" />
         <tli id="T371" time="189.5" type="appl" />
         <tli id="T372" time="190.0" type="appl" />
         <tli id="T373" time="190.5" type="appl" />
         <tli id="T374" time="191.0" type="appl" />
         <tli id="T375" time="191.5" type="appl" />
         <tli id="T376" time="192.0" type="appl" />
         <tli id="T377" time="192.5" type="appl" />
         <tli id="T378" time="193.0" type="appl" />
         <tli id="T379" time="193.5" type="appl" />
         <tli id="T380" time="194.0" type="appl" />
         <tli id="T381" time="194.5" type="appl" />
         <tli id="T382" time="195.0" type="appl" />
         <tli id="T383" time="195.5" type="appl" />
         <tli id="T384" time="196.0" type="appl" />
         <tli id="T385" time="196.5" type="appl" />
         <tli id="T386" time="197.0" type="appl" />
         <tli id="T387" time="197.5" type="appl" />
         <tli id="T388" time="198.0" type="appl" />
         <tli id="T389" time="198.5" type="appl" />
         <tli id="T390" time="199.0" type="appl" />
         <tli id="T391" time="199.5" type="appl" />
         <tli id="T392" time="200.0" type="appl" />
         <tli id="T393" time="200.5" type="appl" />
         <tli id="T394" time="201.0" type="appl" />
         <tli id="T395" time="201.5" type="appl" />
         <tli id="T396" time="202.0" type="appl" />
         <tli id="T397" time="202.5" type="appl" />
         <tli id="T398" time="203.0" type="appl" />
         <tli id="T399" time="203.5" type="appl" />
         <tli id="T400" time="204.0" type="appl" />
         <tli id="T401" time="204.5" type="appl" />
         <tli id="T402" time="205.0" type="appl" />
         <tli id="T403" time="205.5" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="BaRD"
                      type="t">
         <timeline-fork end="T27" start="T26">
            <tli id="T26.tx.1" />
         </timeline-fork>
         <timeline-fork end="T53" start="T52">
            <tli id="T52.tx.1" />
         </timeline-fork>
         <timeline-fork end="T86" start="T85">
            <tli id="T85.tx.1" />
         </timeline-fork>
         <timeline-fork end="T101" start="T100">
            <tli id="T100.tx.1" />
         </timeline-fork>
         <timeline-fork end="T148" start="T147">
            <tli id="T147.tx.1" />
         </timeline-fork>
         <timeline-fork end="T193" start="T192">
            <tli id="T192.tx.1" />
         </timeline-fork>
         <timeline-fork end="T245" start="T244">
            <tli id="T244.tx.1" />
         </timeline-fork>
         <timeline-fork end="T255" start="T254">
            <tli id="T254.tx.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T403" id="Seg_0" n="sc" s="T0">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Bɨlɨr</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">Norilskajga</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">kineːsteːkter</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">ühü</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T6" id="Seg_17" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">Hiŋil</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">kihi</ts>
                  <nts id="Seg_23" n="HIAT:ip">.</nts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T14" id="Seg_26" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_28" n="HIAT:w" s="T6">Bu</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_31" n="HIAT:w" s="T7">kihilere</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_34" n="HIAT:w" s="T8">dʼaktara</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_37" n="HIAT:w" s="T9">hu͡ok</ts>
                  <nts id="Seg_38" n="HIAT:ip">,</nts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_41" n="HIAT:w" s="T10">manɨ</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_44" n="HIAT:w" s="T11">dʼonnoro</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_47" n="HIAT:w" s="T12">biːrde</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_50" n="HIAT:w" s="T13">ɨjɨtallar</ts>
                  <nts id="Seg_51" n="HIAT:ip">:</nts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_54" n="HIAT:u" s="T14">
                  <nts id="Seg_55" n="HIAT:ip">"</nts>
                  <ts e="T15" id="Seg_57" n="HIAT:w" s="T14">Togo</ts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_60" n="HIAT:w" s="T15">dʼaktardammakkɨn</ts>
                  <nts id="Seg_61" n="HIAT:ip">"</nts>
                  <nts id="Seg_62" n="HIAT:ip">,</nts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_65" n="HIAT:w" s="T16">di͡en</ts>
                  <nts id="Seg_66" n="HIAT:ip">.</nts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T19" id="Seg_69" n="HIAT:u" s="T17">
                  <ts e="T18" id="Seg_71" n="HIAT:w" s="T17">Onu͡oga</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_74" n="HIAT:w" s="T18">eter</ts>
                  <nts id="Seg_75" n="HIAT:ip">:</nts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T25" id="Seg_78" n="HIAT:u" s="T19">
                  <ts e="T20" id="Seg_80" n="HIAT:w" s="T19">Min</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_83" n="HIAT:w" s="T20">bu</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_86" n="HIAT:w" s="T21">dojdu</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_89" n="HIAT:w" s="T22">kɨːhɨn</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_92" n="HIAT:w" s="T23">ɨlbat</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_95" n="HIAT:w" s="T24">kihibin</ts>
                  <nts id="Seg_96" n="HIAT:ip">.</nts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T37" id="Seg_99" n="HIAT:u" s="T25">
                  <ts e="T26" id="Seg_101" n="HIAT:w" s="T25">Ü͡öhe</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26.tx.1" id="Seg_104" n="HIAT:w" s="T26">Ürüŋ</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_107" n="HIAT:w" s="T26.tx.1">Ajɨː</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_110" n="HIAT:w" s="T27">di͡en</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_113" n="HIAT:w" s="T28">baːr</ts>
                  <nts id="Seg_114" n="HIAT:ip">,</nts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_117" n="HIAT:w" s="T29">ol</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_120" n="HIAT:w" s="T30">kɨːhɨn</ts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_123" n="HIAT:w" s="T31">araj</ts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_126" n="HIAT:w" s="T32">ɨllakpɨna</ts>
                  <nts id="Seg_127" n="HIAT:ip">,</nts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_130" n="HIAT:w" s="T33">oččogo</ts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_133" n="HIAT:w" s="T34">höbülehi͡ek</ts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_136" n="HIAT:w" s="T35">etim</ts>
                  <nts id="Seg_137" n="HIAT:ip">"</nts>
                  <nts id="Seg_138" n="HIAT:ip">,</nts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_141" n="HIAT:w" s="T36">diːr</ts>
                  <nts id="Seg_142" n="HIAT:ip">.</nts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T43" id="Seg_145" n="HIAT:u" s="T37">
                  <ts e="T38" id="Seg_147" n="HIAT:w" s="T37">Manɨ</ts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_150" n="HIAT:w" s="T38">dʼonnor</ts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_152" n="HIAT:ip">"</nts>
                  <ts e="T40" id="Seg_154" n="HIAT:w" s="T39">onnʼoːn</ts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_157" n="HIAT:w" s="T40">haŋardaga</ts>
                  <nts id="Seg_158" n="HIAT:ip">"</nts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_161" n="HIAT:w" s="T41">di͡en</ts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_164" n="HIAT:w" s="T42">keːheller</ts>
                  <nts id="Seg_165" n="HIAT:ip">.</nts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T48" id="Seg_168" n="HIAT:u" s="T43">
                  <ts e="T44" id="Seg_170" n="HIAT:w" s="T43">Biːrde</ts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_173" n="HIAT:w" s="T44">munnʼakka</ts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_176" n="HIAT:w" s="T45">kineːs</ts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_179" n="HIAT:w" s="T46">ojunu</ts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_182" n="HIAT:w" s="T47">kaːjar</ts>
                  <nts id="Seg_183" n="HIAT:ip">:</nts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T58" id="Seg_186" n="HIAT:u" s="T48">
                  <nts id="Seg_187" n="HIAT:ip">"</nts>
                  <ts e="T49" id="Seg_189" n="HIAT:w" s="T48">En</ts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_192" n="HIAT:w" s="T49">mi͡eke</ts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_195" n="HIAT:w" s="T50">kallaːŋŋa</ts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_198" n="HIAT:w" s="T51">baːr</ts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52.tx.1" id="Seg_201" n="HIAT:w" s="T52">Ürüŋ</ts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_204" n="HIAT:w" s="T52.tx.1">Ajɨː</ts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_207" n="HIAT:w" s="T53">kɨːhɨn</ts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_210" n="HIAT:w" s="T54">dʼaktar</ts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_213" n="HIAT:w" s="T55">gɨnɨ͡akpɨn</ts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_216" n="HIAT:w" s="T56">hu͡orumdʼulaː</ts>
                  <nts id="Seg_217" n="HIAT:ip">"</nts>
                  <nts id="Seg_218" n="HIAT:ip">,</nts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_221" n="HIAT:w" s="T57">diːr</ts>
                  <nts id="Seg_222" n="HIAT:ip">.</nts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T62" id="Seg_225" n="HIAT:u" s="T58">
                  <ts e="T59" id="Seg_227" n="HIAT:w" s="T58">Manɨ</ts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_230" n="HIAT:w" s="T59">ojuna</ts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_233" n="HIAT:w" s="T60">dʼulaja</ts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_236" n="HIAT:w" s="T61">ister</ts>
                  <nts id="Seg_237" n="HIAT:ip">:</nts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T72" id="Seg_240" n="HIAT:u" s="T62">
                  <nts id="Seg_241" n="HIAT:ip">"</nts>
                  <ts e="T63" id="Seg_243" n="HIAT:w" s="T62">Tuguŋ</ts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_246" n="HIAT:w" s="T63">haŋataj</ts>
                  <nts id="Seg_247" n="HIAT:ip">,</nts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_250" n="HIAT:w" s="T64">kajdak</ts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_253" n="HIAT:w" s="T65">da</ts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_256" n="HIAT:w" s="T66">ol</ts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_259" n="HIAT:w" s="T67">dojduttan</ts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_262" n="HIAT:w" s="T68">kɨ͡ajan</ts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_265" n="HIAT:w" s="T69">egelbet</ts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_268" n="HIAT:w" s="T70">hirim</ts>
                  <nts id="Seg_269" n="HIAT:ip">"</nts>
                  <nts id="Seg_270" n="HIAT:ip">,</nts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_273" n="HIAT:w" s="T71">diːr</ts>
                  <nts id="Seg_274" n="HIAT:ip">.</nts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T77" id="Seg_277" n="HIAT:u" s="T72">
                  <ts e="T73" id="Seg_279" n="HIAT:w" s="T72">Manɨ</ts>
                  <nts id="Seg_280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_282" n="HIAT:w" s="T73">kineːs</ts>
                  <nts id="Seg_283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_285" n="HIAT:w" s="T74">hette</ts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_288" n="HIAT:w" s="T75">talagɨ</ts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_291" n="HIAT:w" s="T76">kördörör</ts>
                  <nts id="Seg_292" n="HIAT:ip">:</nts>
                  <nts id="Seg_293" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T79" id="Seg_295" n="HIAT:u" s="T77">
                  <nts id="Seg_296" n="HIAT:ip">"</nts>
                  <ts e="T78" id="Seg_298" n="HIAT:w" s="T77">En</ts>
                  <nts id="Seg_299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_301" n="HIAT:w" s="T78">albɨnnanɨma</ts>
                  <nts id="Seg_302" n="HIAT:ip">.</nts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T90" id="Seg_305" n="HIAT:u" s="T79">
                  <ts e="T80" id="Seg_307" n="HIAT:w" s="T79">Min</ts>
                  <nts id="Seg_308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_310" n="HIAT:w" s="T80">ejigin</ts>
                  <nts id="Seg_311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_313" n="HIAT:w" s="T81">i͡eŋŋin</ts>
                  <nts id="Seg_314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_316" n="HIAT:w" s="T82">kastɨ͡am</ts>
                  <nts id="Seg_317" n="HIAT:ip">,</nts>
                  <nts id="Seg_318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_320" n="HIAT:w" s="T83">kün</ts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_323" n="HIAT:w" s="T84">harsɨn</ts>
                  <nts id="Seg_324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85.tx.1" id="Seg_326" n="HIAT:w" s="T85">Ürüŋ</ts>
                  <nts id="Seg_327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_329" n="HIAT:w" s="T85.tx.1">Ajɨː</ts>
                  <nts id="Seg_330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_332" n="HIAT:w" s="T86">kɨːhɨn</ts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_335" n="HIAT:w" s="T87">baːr</ts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_338" n="HIAT:w" s="T88">gɨn</ts>
                  <nts id="Seg_339" n="HIAT:ip">"</nts>
                  <nts id="Seg_340" n="HIAT:ip">,</nts>
                  <nts id="Seg_341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_343" n="HIAT:w" s="T89">diːr</ts>
                  <nts id="Seg_344" n="HIAT:ip">.</nts>
                  <nts id="Seg_345" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T96" id="Seg_347" n="HIAT:u" s="T90">
                  <ts e="T91" id="Seg_349" n="HIAT:w" s="T90">Ojun</ts>
                  <nts id="Seg_350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_352" n="HIAT:w" s="T91">bɨstɨ͡a</ts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_355" n="HIAT:w" s="T92">duː</ts>
                  <nts id="Seg_356" n="HIAT:ip">,</nts>
                  <nts id="Seg_357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_359" n="HIAT:w" s="T93">dʼe</ts>
                  <nts id="Seg_360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_362" n="HIAT:w" s="T94">kɨːran</ts>
                  <nts id="Seg_363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_365" n="HIAT:w" s="T95">barar</ts>
                  <nts id="Seg_366" n="HIAT:ip">.</nts>
                  <nts id="Seg_367" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T104" id="Seg_369" n="HIAT:u" s="T96">
                  <ts e="T97" id="Seg_371" n="HIAT:w" s="T96">Bu</ts>
                  <nts id="Seg_372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_374" n="HIAT:w" s="T97">ojun</ts>
                  <nts id="Seg_375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_377" n="HIAT:w" s="T98">čugahaːbɨtɨn</ts>
                  <nts id="Seg_378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_380" n="HIAT:w" s="T99">isten</ts>
                  <nts id="Seg_381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100.tx.1" id="Seg_383" n="HIAT:w" s="T100">Ürün</ts>
                  <nts id="Seg_384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_386" n="HIAT:w" s="T100.tx.1">Ajɨː</ts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_389" n="HIAT:w" s="T101">tojon</ts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_392" n="HIAT:w" s="T102">hurdeːktik</ts>
                  <nts id="Seg_393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_395" n="HIAT:w" s="T103">u͡oktanar</ts>
                  <nts id="Seg_396" n="HIAT:ip">.</nts>
                  <nts id="Seg_397" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T111" id="Seg_399" n="HIAT:u" s="T104">
                  <nts id="Seg_400" n="HIAT:ip">"</nts>
                  <ts e="T105" id="Seg_402" n="HIAT:w" s="T104">Tu͡ok</ts>
                  <nts id="Seg_403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_405" n="HIAT:w" s="T105">ölör</ts>
                  <nts id="Seg_406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_408" n="HIAT:w" s="T106">ölüː</ts>
                  <nts id="Seg_409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_411" n="HIAT:w" s="T107">ojuna</ts>
                  <nts id="Seg_412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_414" n="HIAT:w" s="T108">ɨjaːga</ts>
                  <nts id="Seg_415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_417" n="HIAT:w" s="T109">hu͡ok</ts>
                  <nts id="Seg_418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_420" n="HIAT:w" s="T110">kellegej</ts>
                  <nts id="Seg_421" n="HIAT:ip">?</nts>
                  <nts id="Seg_422" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T119" id="Seg_424" n="HIAT:u" s="T111">
                  <ts e="T112" id="Seg_426" n="HIAT:w" s="T111">Egeliŋ</ts>
                  <nts id="Seg_427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_429" n="HIAT:w" s="T112">mi͡eke</ts>
                  <nts id="Seg_430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_432" n="HIAT:w" s="T113">u͡ot</ts>
                  <nts id="Seg_433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_435" n="HIAT:w" s="T114">boloppun</ts>
                  <nts id="Seg_436" n="HIAT:ip">,</nts>
                  <nts id="Seg_437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_439" n="HIAT:w" s="T115">bahɨ</ts>
                  <nts id="Seg_440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_442" n="HIAT:w" s="T116">bɨha</ts>
                  <nts id="Seg_443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_445" n="HIAT:w" s="T117">oksu͡okpun</ts>
                  <nts id="Seg_446" n="HIAT:ip">"</nts>
                  <nts id="Seg_447" n="HIAT:ip">,</nts>
                  <nts id="Seg_448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_450" n="HIAT:w" s="T118">di͡ebit</ts>
                  <nts id="Seg_451" n="HIAT:ip">.</nts>
                  <nts id="Seg_452" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T125" id="Seg_454" n="HIAT:u" s="T119">
                  <ts e="T120" id="Seg_456" n="HIAT:w" s="T119">Ojuna</ts>
                  <nts id="Seg_457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_459" n="HIAT:w" s="T120">kelbite</ts>
                  <nts id="Seg_460" n="HIAT:ip">,</nts>
                  <nts id="Seg_461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_463" n="HIAT:w" s="T121">horunan</ts>
                  <nts id="Seg_464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_466" n="HIAT:w" s="T122">oloror</ts>
                  <nts id="Seg_467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_469" n="HIAT:w" s="T123">ebit</ts>
                  <nts id="Seg_470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_472" n="HIAT:w" s="T124">ajɨːta</ts>
                  <nts id="Seg_473" n="HIAT:ip">.</nts>
                  <nts id="Seg_474" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T129" id="Seg_476" n="HIAT:u" s="T125">
                  <ts e="T126" id="Seg_478" n="HIAT:w" s="T125">Manɨ</ts>
                  <nts id="Seg_479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_481" n="HIAT:w" s="T126">dʼe</ts>
                  <nts id="Seg_482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_484" n="HIAT:w" s="T127">ünen</ts>
                  <nts id="Seg_485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_487" n="HIAT:w" s="T128">kördöhör</ts>
                  <nts id="Seg_488" n="HIAT:ip">:</nts>
                  <nts id="Seg_489" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T139" id="Seg_491" n="HIAT:u" s="T129">
                  <nts id="Seg_492" n="HIAT:ip">"</nts>
                  <ts e="T130" id="Seg_494" n="HIAT:w" s="T129">Min</ts>
                  <nts id="Seg_495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_497" n="HIAT:w" s="T130">kɨhalgattan</ts>
                  <nts id="Seg_498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_500" n="HIAT:w" s="T131">kellim</ts>
                  <nts id="Seg_501" n="HIAT:ip">,</nts>
                  <nts id="Seg_502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_504" n="HIAT:w" s="T132">en</ts>
                  <nts id="Seg_505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_507" n="HIAT:w" s="T133">kurduk</ts>
                  <nts id="Seg_508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_510" n="HIAT:w" s="T134">hirbitiger</ts>
                  <nts id="Seg_511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_513" n="HIAT:w" s="T135">bahɨlɨktaːk</ts>
                  <nts id="Seg_514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_516" n="HIAT:w" s="T136">bu͡olabɨt</ts>
                  <nts id="Seg_517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_518" n="HIAT:ip">—</nts>
                  <nts id="Seg_519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_521" n="HIAT:w" s="T137">kineːs</ts>
                  <nts id="Seg_522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_524" n="HIAT:w" s="T138">di͡en</ts>
                  <nts id="Seg_525" n="HIAT:ip">.</nts>
                  <nts id="Seg_526" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T146" id="Seg_528" n="HIAT:u" s="T139">
                  <ts e="T140" id="Seg_530" n="HIAT:w" s="T139">Ol</ts>
                  <nts id="Seg_531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_533" n="HIAT:w" s="T140">kaːjɨːtɨttan</ts>
                  <nts id="Seg_534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_536" n="HIAT:w" s="T141">kellim</ts>
                  <nts id="Seg_537" n="HIAT:ip">"</nts>
                  <nts id="Seg_538" n="HIAT:ip">,</nts>
                  <nts id="Seg_539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_541" n="HIAT:w" s="T142">di͡en</ts>
                  <nts id="Seg_542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_544" n="HIAT:w" s="T143">tu͡onan</ts>
                  <nts id="Seg_545" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_547" n="HIAT:w" s="T144">naːdatɨn</ts>
                  <nts id="Seg_548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_550" n="HIAT:w" s="T145">kepsiːr</ts>
                  <nts id="Seg_551" n="HIAT:ip">.</nts>
                  <nts id="Seg_552" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T149" id="Seg_554" n="HIAT:u" s="T146">
                  <ts e="T147" id="Seg_556" n="HIAT:w" s="T146">Onu͡oga</ts>
                  <nts id="Seg_557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147.tx.1" id="Seg_559" n="HIAT:w" s="T147">Ürüŋ</ts>
                  <nts id="Seg_560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_562" n="HIAT:w" s="T147.tx.1">Ajɨːta</ts>
                  <nts id="Seg_563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_565" n="HIAT:w" s="T148">eter</ts>
                  <nts id="Seg_566" n="HIAT:ip">:</nts>
                  <nts id="Seg_567" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T150" id="Seg_569" n="HIAT:u" s="T149">
                  <nts id="Seg_570" n="HIAT:ip">"</nts>
                  <ts e="T150" id="Seg_572" n="HIAT:w" s="T149">Hu͡ok</ts>
                  <nts id="Seg_573" n="HIAT:ip">!</nts>
                  <nts id="Seg_574" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T155" id="Seg_576" n="HIAT:u" s="T150">
                  <nts id="Seg_577" n="HIAT:ip">"</nts>
                  <ts e="T151" id="Seg_579" n="HIAT:w" s="T150">Baran</ts>
                  <nts id="Seg_580" n="HIAT:ip">"</nts>
                  <nts id="Seg_581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_583" n="HIAT:w" s="T151">et</ts>
                  <nts id="Seg_584" n="HIAT:ip">"</nts>
                  <nts id="Seg_585" n="HIAT:ip">,</nts>
                  <nts id="Seg_586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_588" n="HIAT:w" s="T152">di͡en</ts>
                  <nts id="Seg_589" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_591" n="HIAT:w" s="T153">tönnörör</ts>
                  <nts id="Seg_592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_594" n="HIAT:w" s="T154">ojunun</ts>
                  <nts id="Seg_595" n="HIAT:ip">.</nts>
                  <nts id="Seg_596" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T160" id="Seg_598" n="HIAT:u" s="T155">
                  <ts e="T156" id="Seg_600" n="HIAT:w" s="T155">Manta</ts>
                  <nts id="Seg_601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_603" n="HIAT:w" s="T156">kɨːran</ts>
                  <nts id="Seg_604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_606" n="HIAT:w" s="T157">büppütüger</ts>
                  <nts id="Seg_607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_609" n="HIAT:w" s="T158">kineːhe</ts>
                  <nts id="Seg_610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_612" n="HIAT:w" s="T159">kaːjar</ts>
                  <nts id="Seg_613" n="HIAT:ip">:</nts>
                  <nts id="Seg_614" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T165" id="Seg_616" n="HIAT:u" s="T160">
                  <ts e="T161" id="Seg_618" n="HIAT:w" s="T160">En</ts>
                  <nts id="Seg_619" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_621" n="HIAT:w" s="T161">tü͡ökününen</ts>
                  <nts id="Seg_622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_624" n="HIAT:w" s="T162">onno-manna</ts>
                  <nts id="Seg_625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_627" n="HIAT:w" s="T163">barbɨt</ts>
                  <nts id="Seg_628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_630" n="HIAT:w" s="T164">bu͡olagɨn</ts>
                  <nts id="Seg_631" n="HIAT:ip">.</nts>
                  <nts id="Seg_632" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T173" id="Seg_634" n="HIAT:u" s="T165">
                  <ts e="T166" id="Seg_636" n="HIAT:w" s="T165">Tutuːr</ts>
                  <nts id="Seg_637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_639" n="HIAT:w" s="T166">kɨrsaŋ</ts>
                  <nts id="Seg_640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_642" n="HIAT:w" s="T167">manna</ts>
                  <nts id="Seg_643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_645" n="HIAT:w" s="T168">turar</ts>
                  <nts id="Seg_646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_648" n="HIAT:w" s="T169">bu͡olbat</ts>
                  <nts id="Seg_649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_651" n="HIAT:w" s="T170">du͡o</ts>
                  <nts id="Seg_652" n="HIAT:ip">"</nts>
                  <nts id="Seg_653" n="HIAT:ip">,</nts>
                  <nts id="Seg_654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_656" n="HIAT:w" s="T171">di͡en</ts>
                  <nts id="Seg_657" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_659" n="HIAT:w" s="T172">kaːjar</ts>
                  <nts id="Seg_660" n="HIAT:ip">.</nts>
                  <nts id="Seg_661" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T179" id="Seg_663" n="HIAT:u" s="T173">
                  <nts id="Seg_664" n="HIAT:ip">"</nts>
                  <ts e="T174" id="Seg_666" n="HIAT:w" s="T173">Bulgu</ts>
                  <nts id="Seg_667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_669" n="HIAT:w" s="T174">mi͡eke</ts>
                  <nts id="Seg_670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_672" n="HIAT:w" s="T175">kɨːspɨn</ts>
                  <nts id="Seg_673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_675" n="HIAT:w" s="T176">egelen</ts>
                  <nts id="Seg_676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_678" n="HIAT:w" s="T177">bi͡er</ts>
                  <nts id="Seg_679" n="HIAT:ip">"</nts>
                  <nts id="Seg_680" n="HIAT:ip">,</nts>
                  <nts id="Seg_681" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_683" n="HIAT:w" s="T178">diːr</ts>
                  <nts id="Seg_684" n="HIAT:ip">.</nts>
                  <nts id="Seg_685" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T186" id="Seg_687" n="HIAT:u" s="T179">
                  <ts e="T180" id="Seg_689" n="HIAT:w" s="T179">Dʼe</ts>
                  <nts id="Seg_690" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_692" n="HIAT:w" s="T180">emi͡e</ts>
                  <nts id="Seg_693" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_695" n="HIAT:w" s="T181">baː</ts>
                  <nts id="Seg_696" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_698" n="HIAT:w" s="T182">ojun</ts>
                  <nts id="Seg_699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_701" n="HIAT:w" s="T183">harsɨ͡arda</ts>
                  <nts id="Seg_702" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_704" n="HIAT:w" s="T184">kɨːran</ts>
                  <nts id="Seg_705" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_707" n="HIAT:w" s="T185">muŋnannar</ts>
                  <nts id="Seg_708" n="HIAT:ip">.</nts>
                  <nts id="Seg_709" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T195" id="Seg_711" n="HIAT:u" s="T186">
                  <ts e="T187" id="Seg_713" n="HIAT:w" s="T186">Bu</ts>
                  <nts id="Seg_714" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_716" n="HIAT:w" s="T187">kɨːran</ts>
                  <nts id="Seg_717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_719" n="HIAT:w" s="T188">tijbitiger</ts>
                  <nts id="Seg_720" n="HIAT:ip">,</nts>
                  <nts id="Seg_721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_723" n="HIAT:w" s="T189">tu͡oktan</ts>
                  <nts id="Seg_724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_726" n="HIAT:w" s="T190">daː</ts>
                  <nts id="Seg_727" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_729" n="HIAT:w" s="T191">hürdenen</ts>
                  <nts id="Seg_730" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192.tx.1" id="Seg_732" n="HIAT:w" s="T192">Ürüŋ</ts>
                  <nts id="Seg_733" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_735" n="HIAT:w" s="T192.tx.1">Ajɨːta</ts>
                  <nts id="Seg_736" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_738" n="HIAT:w" s="T193">ojunun</ts>
                  <nts id="Seg_739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_741" n="HIAT:w" s="T194">abalɨːr</ts>
                  <nts id="Seg_742" n="HIAT:ip">.</nts>
                  <nts id="Seg_743" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T200" id="Seg_745" n="HIAT:u" s="T195">
                  <ts e="T196" id="Seg_747" n="HIAT:w" s="T195">Manɨ͡aka</ts>
                  <nts id="Seg_748" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_750" n="HIAT:w" s="T196">emi͡e</ts>
                  <nts id="Seg_751" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_753" n="HIAT:w" s="T197">ojuna</ts>
                  <nts id="Seg_754" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_756" n="HIAT:w" s="T198">ɨtanan</ts>
                  <nts id="Seg_757" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_759" n="HIAT:w" s="T199">kebilener</ts>
                  <nts id="Seg_760" n="HIAT:ip">:</nts>
                  <nts id="Seg_761" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T204" id="Seg_763" n="HIAT:u" s="T200">
                  <nts id="Seg_764" n="HIAT:ip">"</nts>
                  <ts e="T201" id="Seg_766" n="HIAT:w" s="T200">Haːtar</ts>
                  <nts id="Seg_767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_769" n="HIAT:w" s="T201">beli͡ete</ts>
                  <nts id="Seg_770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_772" n="HIAT:w" s="T202">bi͡er</ts>
                  <nts id="Seg_773" n="HIAT:ip">"</nts>
                  <nts id="Seg_774" n="HIAT:ip">,</nts>
                  <nts id="Seg_775" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_777" n="HIAT:w" s="T203">di͡en</ts>
                  <nts id="Seg_778" n="HIAT:ip">.</nts>
                  <nts id="Seg_779" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T218" id="Seg_781" n="HIAT:u" s="T204">
                  <nts id="Seg_782" n="HIAT:ip">"</nts>
                  <ts e="T205" id="Seg_784" n="HIAT:w" s="T204">Hu͡ok</ts>
                  <nts id="Seg_785" n="HIAT:ip">,</nts>
                  <nts id="Seg_786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_788" n="HIAT:w" s="T205">bi͡eri͡em</ts>
                  <nts id="Seg_789" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_791" n="HIAT:w" s="T206">hu͡oga</ts>
                  <nts id="Seg_792" n="HIAT:ip">,</nts>
                  <nts id="Seg_793" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_795" n="HIAT:w" s="T207">togo</ts>
                  <nts id="Seg_796" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_798" n="HIAT:w" s="T208">aŋɨraj</ts>
                  <nts id="Seg_799" n="HIAT:ip">,</nts>
                  <nts id="Seg_800" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_802" n="HIAT:w" s="T209">tu͡ora</ts>
                  <nts id="Seg_803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_805" n="HIAT:w" s="T210">karaktaːk</ts>
                  <nts id="Seg_806" n="HIAT:ip">,</nts>
                  <nts id="Seg_807" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_809" n="HIAT:w" s="T211">ajɨlaːgɨn</ts>
                  <nts id="Seg_810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_812" n="HIAT:w" s="T212">en</ts>
                  <nts id="Seg_813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_815" n="HIAT:w" s="T213">öllögüŋ</ts>
                  <nts id="Seg_816" n="HIAT:ip">"</nts>
                  <nts id="Seg_817" n="HIAT:ip">,</nts>
                  <nts id="Seg_818" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_820" n="HIAT:w" s="T214">di͡en</ts>
                  <nts id="Seg_821" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_823" n="HIAT:w" s="T215">ojunun</ts>
                  <nts id="Seg_824" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_826" n="HIAT:w" s="T216">emi͡e</ts>
                  <nts id="Seg_827" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_829" n="HIAT:w" s="T217">tönnörör</ts>
                  <nts id="Seg_830" n="HIAT:ip">.</nts>
                  <nts id="Seg_831" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T224" id="Seg_833" n="HIAT:u" s="T218">
                  <ts e="T219" id="Seg_835" n="HIAT:w" s="T218">Kɨːran</ts>
                  <nts id="Seg_836" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_838" n="HIAT:w" s="T219">büten</ts>
                  <nts id="Seg_839" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_841" n="HIAT:w" s="T220">baran</ts>
                  <nts id="Seg_842" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_844" n="HIAT:w" s="T221">ojun</ts>
                  <nts id="Seg_845" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_847" n="HIAT:w" s="T222">kineːsten</ts>
                  <nts id="Seg_848" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_850" n="HIAT:w" s="T223">aːrtahar</ts>
                  <nts id="Seg_851" n="HIAT:ip">:</nts>
                  <nts id="Seg_852" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T238" id="Seg_854" n="HIAT:u" s="T224">
                  <nts id="Seg_855" n="HIAT:ip">"</nts>
                  <ts e="T225" id="Seg_857" n="HIAT:w" s="T224">Tu͡ok</ts>
                  <nts id="Seg_858" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_860" n="HIAT:w" s="T225">daː</ts>
                  <nts id="Seg_861" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_863" n="HIAT:w" s="T226">barɨta</ts>
                  <nts id="Seg_864" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_866" n="HIAT:w" s="T227">üs</ts>
                  <nts id="Seg_867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_869" n="HIAT:w" s="T228">tögülleːk</ts>
                  <nts id="Seg_870" n="HIAT:ip">,</nts>
                  <nts id="Seg_871" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_873" n="HIAT:w" s="T229">ölörüme</ts>
                  <nts id="Seg_874" n="HIAT:ip">,</nts>
                  <nts id="Seg_875" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_876" n="HIAT:ip">"</nts>
                  <ts e="T231" id="Seg_878" n="HIAT:w" s="T230">horukpun</ts>
                  <nts id="Seg_879" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_881" n="HIAT:w" s="T231">hippete</ts>
                  <nts id="Seg_882" n="HIAT:ip">"</nts>
                  <nts id="Seg_883" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_885" n="HIAT:w" s="T232">di͡en</ts>
                  <nts id="Seg_886" n="HIAT:ip">,</nts>
                  <nts id="Seg_887" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_889" n="HIAT:w" s="T233">harsɨn</ts>
                  <nts id="Seg_890" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_892" n="HIAT:w" s="T234">emi͡e</ts>
                  <nts id="Seg_893" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_895" n="HIAT:w" s="T235">muŋnahan</ts>
                  <nts id="Seg_896" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_898" n="HIAT:w" s="T236">körü͡öm</ts>
                  <nts id="Seg_899" n="HIAT:ip">"</nts>
                  <nts id="Seg_900" n="HIAT:ip">,</nts>
                  <nts id="Seg_901" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_903" n="HIAT:w" s="T237">diːr</ts>
                  <nts id="Seg_904" n="HIAT:ip">.</nts>
                  <nts id="Seg_905" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T242" id="Seg_907" n="HIAT:u" s="T238">
                  <ts e="T239" id="Seg_909" n="HIAT:w" s="T238">Harsɨŋŋɨtɨgar</ts>
                  <nts id="Seg_910" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_912" n="HIAT:w" s="T239">ühüs</ts>
                  <nts id="Seg_913" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_915" n="HIAT:w" s="T240">kɨːrɨːtɨn</ts>
                  <nts id="Seg_916" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_918" n="HIAT:w" s="T241">kɨːrar</ts>
                  <nts id="Seg_919" n="HIAT:ip">.</nts>
                  <nts id="Seg_920" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T247" id="Seg_922" n="HIAT:u" s="T242">
                  <ts e="T243" id="Seg_924" n="HIAT:w" s="T242">Bu</ts>
                  <nts id="Seg_925" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_927" n="HIAT:w" s="T243">kɨːran</ts>
                  <nts id="Seg_928" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244.tx.1" id="Seg_930" n="HIAT:w" s="T244">Ürüŋ</ts>
                  <nts id="Seg_931" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_933" n="HIAT:w" s="T244.tx.1">Ajɨː</ts>
                  <nts id="Seg_934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_936" n="HIAT:w" s="T245">dʼi͡etiger</ts>
                  <nts id="Seg_937" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_939" n="HIAT:w" s="T246">tijer</ts>
                  <nts id="Seg_940" n="HIAT:ip">.</nts>
                  <nts id="Seg_941" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T262" id="Seg_943" n="HIAT:u" s="T247">
                  <ts e="T248" id="Seg_945" n="HIAT:w" s="T247">Bu</ts>
                  <nts id="Seg_946" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_948" n="HIAT:w" s="T248">tijen</ts>
                  <nts id="Seg_949" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_951" n="HIAT:w" s="T249">toŋkojon</ts>
                  <nts id="Seg_952" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_954" n="HIAT:w" s="T250">dʼi͡etin</ts>
                  <nts id="Seg_955" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_957" n="HIAT:w" s="T251">hu͡onatɨn</ts>
                  <nts id="Seg_958" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_960" n="HIAT:w" s="T252">iččitiger</ts>
                  <nts id="Seg_961" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_963" n="HIAT:w" s="T253">aːrtahan</ts>
                  <nts id="Seg_964" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254.tx.1" id="Seg_966" n="HIAT:w" s="T254">Ürüŋ</ts>
                  <nts id="Seg_967" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_969" n="HIAT:w" s="T254.tx.1">Ajɨːtɨn</ts>
                  <nts id="Seg_970" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_972" n="HIAT:w" s="T255">atagar</ts>
                  <nts id="Seg_973" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_975" n="HIAT:w" s="T256">hɨːlan</ts>
                  <nts id="Seg_976" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_978" n="HIAT:w" s="T257">kelen</ts>
                  <nts id="Seg_979" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_981" n="HIAT:w" s="T258">baː</ts>
                  <nts id="Seg_982" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_984" n="HIAT:w" s="T259">dʼiŋŋi-ti͡egeːger</ts>
                  <nts id="Seg_985" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_987" n="HIAT:w" s="T260">hürdeːktik</ts>
                  <nts id="Seg_988" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_990" n="HIAT:w" s="T261">tu͡onar-ɨtɨːr</ts>
                  <nts id="Seg_991" n="HIAT:ip">.</nts>
                  <nts id="Seg_992" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T266" id="Seg_994" n="HIAT:u" s="T262">
                  <ts e="T263" id="Seg_996" n="HIAT:w" s="T262">Manɨ</ts>
                  <nts id="Seg_997" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_999" n="HIAT:w" s="T263">dʼe</ts>
                  <nts id="Seg_1000" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_1002" n="HIAT:w" s="T264">hüreginen</ts>
                  <nts id="Seg_1003" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_1005" n="HIAT:w" s="T265">ahɨnar</ts>
                  <nts id="Seg_1006" n="HIAT:ip">.</nts>
                  <nts id="Seg_1007" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T274" id="Seg_1009" n="HIAT:u" s="T266">
                  <nts id="Seg_1010" n="HIAT:ip">"</nts>
                  <ts e="T267" id="Seg_1012" n="HIAT:w" s="T266">Dʼe</ts>
                  <nts id="Seg_1013" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_1015" n="HIAT:w" s="T267">kirdik</ts>
                  <nts id="Seg_1016" n="HIAT:ip">,</nts>
                  <nts id="Seg_1017" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_1019" n="HIAT:w" s="T268">min</ts>
                  <nts id="Seg_1020" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_1022" n="HIAT:w" s="T269">ejigin</ts>
                  <nts id="Seg_1023" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_1025" n="HIAT:w" s="T270">ölöröːrü</ts>
                  <nts id="Seg_1026" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_1028" n="HIAT:w" s="T271">bejem</ts>
                  <nts id="Seg_1029" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_1031" n="HIAT:w" s="T272">ojunum</ts>
                  <nts id="Seg_1032" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_1034" n="HIAT:w" s="T273">gɨmmatagɨm</ts>
                  <nts id="Seg_1035" n="HIAT:ip">.</nts>
                  <nts id="Seg_1036" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T279" id="Seg_1038" n="HIAT:u" s="T274">
                  <ts e="T275" id="Seg_1040" n="HIAT:w" s="T274">Togo</ts>
                  <nts id="Seg_1041" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_1043" n="HIAT:w" s="T275">bagas</ts>
                  <nts id="Seg_1044" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_1046" n="HIAT:w" s="T276">kineːhiŋ</ts>
                  <nts id="Seg_1047" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_1049" n="HIAT:w" s="T277">oburgu</ts>
                  <nts id="Seg_1050" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_1052" n="HIAT:w" s="T278">aŋɨraj</ts>
                  <nts id="Seg_1053" n="HIAT:ip">!</nts>
                  <nts id="Seg_1054" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T284" id="Seg_1056" n="HIAT:u" s="T279">
                  <ts e="T280" id="Seg_1058" n="HIAT:w" s="T279">Dʼe</ts>
                  <nts id="Seg_1059" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_1061" n="HIAT:w" s="T280">bejetin</ts>
                  <nts id="Seg_1062" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_1064" n="HIAT:w" s="T281">noru͡otugar</ts>
                  <nts id="Seg_1065" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_1067" n="HIAT:w" s="T282">i͡edeːni</ts>
                  <nts id="Seg_1068" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_1070" n="HIAT:w" s="T283">oŋordogo</ts>
                  <nts id="Seg_1071" n="HIAT:ip">.</nts>
                  <nts id="Seg_1072" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T290" id="Seg_1074" n="HIAT:u" s="T284">
                  <ts e="T285" id="Seg_1076" n="HIAT:w" s="T284">Noru͡okkututtan</ts>
                  <nts id="Seg_1077" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_1079" n="HIAT:w" s="T285">hogotok</ts>
                  <nts id="Seg_1080" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_1082" n="HIAT:w" s="T286">en</ts>
                  <nts id="Seg_1083" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_1085" n="HIAT:w" s="T287">u͡olgun</ts>
                  <nts id="Seg_1086" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_1088" n="HIAT:w" s="T288">kɨtarɨ</ts>
                  <nts id="Seg_1089" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_1091" n="HIAT:w" s="T289">ordu͡okkut</ts>
                  <nts id="Seg_1092" n="HIAT:ip">.</nts>
                  <nts id="Seg_1093" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T295" id="Seg_1095" n="HIAT:u" s="T290">
                  <ts e="T291" id="Seg_1097" n="HIAT:w" s="T290">Tu͡ok</ts>
                  <nts id="Seg_1098" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_1100" n="HIAT:w" s="T291">da</ts>
                  <nts id="Seg_1101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_1103" n="HIAT:w" s="T292">tɨːnnaːk</ts>
                  <nts id="Seg_1104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_1106" n="HIAT:w" s="T293">kaːlɨ͡a</ts>
                  <nts id="Seg_1107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1109" n="HIAT:w" s="T294">hu͡oga</ts>
                  <nts id="Seg_1110" n="HIAT:ip">.</nts>
                  <nts id="Seg_1111" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T304" id="Seg_1113" n="HIAT:u" s="T295">
                  <ts e="T296" id="Seg_1115" n="HIAT:w" s="T295">Atɨn</ts>
                  <nts id="Seg_1116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_1118" n="HIAT:w" s="T296">hirten</ts>
                  <nts id="Seg_1119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_1121" n="HIAT:w" s="T297">dʼon</ts>
                  <nts id="Seg_1122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_1124" n="HIAT:w" s="T298">kelenner</ts>
                  <nts id="Seg_1125" n="HIAT:ip">,</nts>
                  <nts id="Seg_1126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_1128" n="HIAT:w" s="T299">onton</ts>
                  <nts id="Seg_1129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_1131" n="HIAT:w" s="T300">haŋa</ts>
                  <nts id="Seg_1132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_1134" n="HIAT:w" s="T301">dʼon</ts>
                  <nts id="Seg_1135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1137" n="HIAT:w" s="T302">ü͡öskü͡öge</ts>
                  <nts id="Seg_1138" n="HIAT:ip">"</nts>
                  <nts id="Seg_1139" n="HIAT:ip">,</nts>
                  <nts id="Seg_1140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_1142" n="HIAT:w" s="T303">diːr</ts>
                  <nts id="Seg_1143" n="HIAT:ip">.</nts>
                  <nts id="Seg_1144" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T325" id="Seg_1146" n="HIAT:u" s="T304">
                  <nts id="Seg_1147" n="HIAT:ip">"</nts>
                  <ts e="T305" id="Seg_1149" n="HIAT:w" s="T304">Haŋa</ts>
                  <nts id="Seg_1150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_1152" n="HIAT:w" s="T305">ɨraːs</ts>
                  <nts id="Seg_1153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_1155" n="HIAT:w" s="T306">čeːlkeː</ts>
                  <nts id="Seg_1156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_1158" n="HIAT:w" s="T307">öldüːnne</ts>
                  <nts id="Seg_1159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1161" n="HIAT:w" s="T308">anɨ</ts>
                  <nts id="Seg_1162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_1164" n="HIAT:w" s="T309">üs</ts>
                  <nts id="Seg_1165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_1167" n="HIAT:w" s="T310">konugunan</ts>
                  <nts id="Seg_1168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_1170" n="HIAT:w" s="T311">harsɨ͡arda</ts>
                  <nts id="Seg_1171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_1173" n="HIAT:w" s="T312">turu͡ordunnar</ts>
                  <nts id="Seg_1174" n="HIAT:ip">,</nts>
                  <nts id="Seg_1175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_1177" n="HIAT:w" s="T313">ču͡ogur</ts>
                  <nts id="Seg_1178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_1180" n="HIAT:w" s="T314">čeːlkeː</ts>
                  <nts id="Seg_1181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1183" n="HIAT:w" s="T315">tabanɨ</ts>
                  <nts id="Seg_1184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_1186" n="HIAT:w" s="T316">ölördünner</ts>
                  <nts id="Seg_1187" n="HIAT:ip">,</nts>
                  <nts id="Seg_1188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_1190" n="HIAT:w" s="T317">ol</ts>
                  <nts id="Seg_1191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1193" n="HIAT:w" s="T318">uraha</ts>
                  <nts id="Seg_1194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_1196" n="HIAT:w" s="T319">tulatɨn</ts>
                  <nts id="Seg_1197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1199" n="HIAT:w" s="T320">barɨtɨn</ts>
                  <nts id="Seg_1200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_1202" n="HIAT:w" s="T321">čeːlkeː</ts>
                  <nts id="Seg_1203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1205" n="HIAT:w" s="T322">taba</ts>
                  <nts id="Seg_1206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_1208" n="HIAT:w" s="T323">tiriːtinen</ts>
                  <nts id="Seg_1209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1211" n="HIAT:w" s="T324">telgeːtinner</ts>
                  <nts id="Seg_1212" n="HIAT:ip">.</nts>
                  <nts id="Seg_1213" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T329" id="Seg_1215" n="HIAT:u" s="T325">
                  <ts e="T326" id="Seg_1217" n="HIAT:w" s="T325">Manan</ts>
                  <nts id="Seg_1218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1220" n="HIAT:w" s="T326">üktenen</ts>
                  <nts id="Seg_1221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_1223" n="HIAT:w" s="T327">kiːri͡ege</ts>
                  <nts id="Seg_1224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_1226" n="HIAT:w" s="T328">kineːskit</ts>
                  <nts id="Seg_1227" n="HIAT:ip">.</nts>
                  <nts id="Seg_1228" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T335" id="Seg_1230" n="HIAT:u" s="T329">
                  <ts e="T330" id="Seg_1232" n="HIAT:w" s="T329">Bɨrtak</ts>
                  <nts id="Seg_1233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_1235" n="HIAT:w" s="T330">tu͡ok</ts>
                  <nts id="Seg_1236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_1238" n="HIAT:w" s="T331">da</ts>
                  <nts id="Seg_1239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_1241" n="HIAT:w" s="T332">hɨstɨ͡a</ts>
                  <nts id="Seg_1242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_1244" n="HIAT:w" s="T333">hu͡oktaːk</ts>
                  <nts id="Seg_1245" n="HIAT:ip">"</nts>
                  <nts id="Seg_1246" n="HIAT:ip">,</nts>
                  <nts id="Seg_1247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_1249" n="HIAT:w" s="T334">diːr</ts>
                  <nts id="Seg_1250" n="HIAT:ip">.</nts>
                  <nts id="Seg_1251" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T346" id="Seg_1253" n="HIAT:u" s="T335">
                  <nts id="Seg_1254" n="HIAT:ip">"</nts>
                  <ts e="T336" id="Seg_1256" n="HIAT:w" s="T335">Dʼonnor</ts>
                  <nts id="Seg_1257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_1259" n="HIAT:w" s="T336">kurum</ts>
                  <nts id="Seg_1260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_1262" n="HIAT:w" s="T337">di͡en</ts>
                  <nts id="Seg_1263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_1265" n="HIAT:w" s="T338">kelbetinner</ts>
                  <nts id="Seg_1266" n="HIAT:ip">,</nts>
                  <nts id="Seg_1267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_1269" n="HIAT:w" s="T339">kim</ts>
                  <nts id="Seg_1270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_1272" n="HIAT:w" s="T340">tɨːnɨn</ts>
                  <nts id="Seg_1273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_1275" n="HIAT:w" s="T341">karɨstaːrɨ</ts>
                  <nts id="Seg_1276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1278" n="HIAT:w" s="T342">gɨnar</ts>
                  <nts id="Seg_1279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_1281" n="HIAT:w" s="T343">ɨraːk</ts>
                  <nts id="Seg_1282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_1284" n="HIAT:w" s="T344">bardɨn</ts>
                  <nts id="Seg_1285" n="HIAT:ip">"</nts>
                  <nts id="Seg_1286" n="HIAT:ip">,</nts>
                  <nts id="Seg_1287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_1289" n="HIAT:w" s="T345">di͡ebit</ts>
                  <nts id="Seg_1290" n="HIAT:ip">.</nts>
                  <nts id="Seg_1291" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T351" id="Seg_1293" n="HIAT:u" s="T346">
                  <ts e="T347" id="Seg_1295" n="HIAT:w" s="T346">Ühüs</ts>
                  <nts id="Seg_1296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1298" n="HIAT:w" s="T347">künüger</ts>
                  <nts id="Seg_1299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_1301" n="HIAT:w" s="T348">eppitin</ts>
                  <nts id="Seg_1302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_1304" n="HIAT:w" s="T349">kurduk</ts>
                  <nts id="Seg_1305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_1307" n="HIAT:w" s="T350">oŋorbuttar</ts>
                  <nts id="Seg_1308" n="HIAT:ip">.</nts>
                  <nts id="Seg_1309" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T355" id="Seg_1311" n="HIAT:u" s="T351">
                  <ts e="T352" id="Seg_1313" n="HIAT:w" s="T351">Manna</ts>
                  <nts id="Seg_1314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_1316" n="HIAT:w" s="T352">ojun</ts>
                  <nts id="Seg_1317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_1319" n="HIAT:w" s="T353">körön</ts>
                  <nts id="Seg_1320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_1322" n="HIAT:w" s="T354">turbut</ts>
                  <nts id="Seg_1323" n="HIAT:ip">.</nts>
                  <nts id="Seg_1324" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T365" id="Seg_1326" n="HIAT:u" s="T355">
                  <ts e="T356" id="Seg_1328" n="HIAT:w" s="T355">Kütür</ts>
                  <nts id="Seg_1329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_1331" n="HIAT:w" s="T356">bagajɨ</ts>
                  <nts id="Seg_1332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_1334" n="HIAT:w" s="T357">boloho</ts>
                  <nts id="Seg_1335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_1337" n="HIAT:w" s="T358">tɨ͡al</ts>
                  <nts id="Seg_1338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_1340" n="HIAT:w" s="T359">tüspüt</ts>
                  <nts id="Seg_1341" n="HIAT:ip">,</nts>
                  <nts id="Seg_1342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_1344" n="HIAT:w" s="T360">ču͡oraːn</ts>
                  <nts id="Seg_1345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_1347" n="HIAT:w" s="T361">tɨ͡aha</ts>
                  <nts id="Seg_1348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_1350" n="HIAT:w" s="T362">di͡en</ts>
                  <nts id="Seg_1351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_1353" n="HIAT:w" s="T363">bagajɨ</ts>
                  <nts id="Seg_1354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_1356" n="HIAT:w" s="T364">ihillibit</ts>
                  <nts id="Seg_1357" n="HIAT:ip">.</nts>
                  <nts id="Seg_1358" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T374" id="Seg_1360" n="HIAT:u" s="T365">
                  <ts e="T366" id="Seg_1362" n="HIAT:w" s="T365">Tu͡ok</ts>
                  <nts id="Seg_1363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_1365" n="HIAT:w" s="T366">ere</ts>
                  <nts id="Seg_1366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_1368" n="HIAT:w" s="T367">tɨ͡al</ts>
                  <nts id="Seg_1369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_1371" n="HIAT:w" s="T368">kurduk</ts>
                  <nts id="Seg_1372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_1374" n="HIAT:w" s="T369">uraha</ts>
                  <nts id="Seg_1375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_1377" n="HIAT:w" s="T370">aːnɨn</ts>
                  <nts id="Seg_1378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_1380" n="HIAT:w" s="T371">arɨjarga</ts>
                  <nts id="Seg_1381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_1383" n="HIAT:w" s="T372">dɨlɨ</ts>
                  <nts id="Seg_1384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_1386" n="HIAT:w" s="T373">gɨmmɨt</ts>
                  <nts id="Seg_1387" n="HIAT:ip">.</nts>
                  <nts id="Seg_1388" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T379" id="Seg_1390" n="HIAT:u" s="T374">
                  <ts e="T375" id="Seg_1392" n="HIAT:w" s="T374">Ol</ts>
                  <nts id="Seg_1393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_1395" n="HIAT:w" s="T375">kennine</ts>
                  <nts id="Seg_1396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_1398" n="HIAT:w" s="T376">tɨ͡ala</ts>
                  <nts id="Seg_1399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378" id="Seg_1401" n="HIAT:w" s="T377">hu͡ok</ts>
                  <nts id="Seg_1402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_1404" n="HIAT:w" s="T378">bu͡olbut</ts>
                  <nts id="Seg_1405" n="HIAT:ip">.</nts>
                  <nts id="Seg_1406" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T392" id="Seg_1408" n="HIAT:u" s="T379">
                  <ts e="T380" id="Seg_1410" n="HIAT:w" s="T379">Manɨ</ts>
                  <nts id="Seg_1411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_1413" n="HIAT:w" s="T380">ojun</ts>
                  <nts id="Seg_1414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T382" id="Seg_1416" n="HIAT:w" s="T381">baran</ts>
                  <nts id="Seg_1417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_1419" n="HIAT:w" s="T382">körbüte</ts>
                  <nts id="Seg_1420" n="HIAT:ip">,</nts>
                  <nts id="Seg_1421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_1423" n="HIAT:w" s="T383">kineːs</ts>
                  <nts id="Seg_1424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_1426" n="HIAT:w" s="T384">da</ts>
                  <nts id="Seg_1427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T386" id="Seg_1429" n="HIAT:w" s="T385">barɨ</ts>
                  <nts id="Seg_1430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_1432" n="HIAT:w" s="T386">kelbit</ts>
                  <nts id="Seg_1433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_1435" n="HIAT:w" s="T387">da</ts>
                  <nts id="Seg_1436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_1438" n="HIAT:w" s="T388">dʼon</ts>
                  <nts id="Seg_1439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_1441" n="HIAT:w" s="T389">ölö</ts>
                  <nts id="Seg_1442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T391" id="Seg_1444" n="HIAT:w" s="T390">hɨtallar</ts>
                  <nts id="Seg_1445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_1447" n="HIAT:w" s="T391">ebit</ts>
                  <nts id="Seg_1448" n="HIAT:ip">.</nts>
                  <nts id="Seg_1449" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T395" id="Seg_1451" n="HIAT:u" s="T392">
                  <ts e="T393" id="Seg_1453" n="HIAT:w" s="T392">Manan</ts>
                  <nts id="Seg_1454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_1456" n="HIAT:w" s="T393">norilʼskaj</ts>
                  <nts id="Seg_1457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_1459" n="HIAT:w" s="T394">dʼadajda</ts>
                  <nts id="Seg_1460" n="HIAT:ip">.</nts>
                  <nts id="Seg_1461" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T403" id="Seg_1463" n="HIAT:u" s="T395">
                  <ts e="T396" id="Seg_1465" n="HIAT:w" s="T395">Iti</ts>
                  <nts id="Seg_1466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_1468" n="HIAT:w" s="T396">edʼeːn</ts>
                  <nts id="Seg_1469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_1471" n="HIAT:w" s="T397">tu͡orattan</ts>
                  <nts id="Seg_1472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_1474" n="HIAT:w" s="T398">kelen</ts>
                  <nts id="Seg_1475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T400" id="Seg_1477" n="HIAT:w" s="T399">onton</ts>
                  <nts id="Seg_1478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_1480" n="HIAT:w" s="T400">ere</ts>
                  <nts id="Seg_1481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T402" id="Seg_1483" n="HIAT:w" s="T401">ü͡ösküːr</ts>
                  <nts id="Seg_1484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_1486" n="HIAT:w" s="T402">bu͡ollular</ts>
                  <nts id="Seg_1487" n="HIAT:ip">.</nts>
                  <nts id="Seg_1488" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T403" id="Seg_1489" n="sc" s="T0">
               <ts e="T1" id="Seg_1491" n="e" s="T0">Bɨlɨr </ts>
               <ts e="T2" id="Seg_1493" n="e" s="T1">Norilskajga </ts>
               <ts e="T3" id="Seg_1495" n="e" s="T2">kineːsteːkter </ts>
               <ts e="T4" id="Seg_1497" n="e" s="T3">ühü. </ts>
               <ts e="T5" id="Seg_1499" n="e" s="T4">Hiŋil </ts>
               <ts e="T6" id="Seg_1501" n="e" s="T5">kihi. </ts>
               <ts e="T7" id="Seg_1503" n="e" s="T6">Bu </ts>
               <ts e="T8" id="Seg_1505" n="e" s="T7">kihilere </ts>
               <ts e="T9" id="Seg_1507" n="e" s="T8">dʼaktara </ts>
               <ts e="T10" id="Seg_1509" n="e" s="T9">hu͡ok, </ts>
               <ts e="T11" id="Seg_1511" n="e" s="T10">manɨ </ts>
               <ts e="T12" id="Seg_1513" n="e" s="T11">dʼonnoro </ts>
               <ts e="T13" id="Seg_1515" n="e" s="T12">biːrde </ts>
               <ts e="T14" id="Seg_1517" n="e" s="T13">ɨjɨtallar: </ts>
               <ts e="T15" id="Seg_1519" n="e" s="T14">"Togo </ts>
               <ts e="T16" id="Seg_1521" n="e" s="T15">dʼaktardammakkɨn", </ts>
               <ts e="T17" id="Seg_1523" n="e" s="T16">di͡en. </ts>
               <ts e="T18" id="Seg_1525" n="e" s="T17">Onu͡oga </ts>
               <ts e="T19" id="Seg_1527" n="e" s="T18">eter: </ts>
               <ts e="T20" id="Seg_1529" n="e" s="T19">Min </ts>
               <ts e="T21" id="Seg_1531" n="e" s="T20">bu </ts>
               <ts e="T22" id="Seg_1533" n="e" s="T21">dojdu </ts>
               <ts e="T23" id="Seg_1535" n="e" s="T22">kɨːhɨn </ts>
               <ts e="T24" id="Seg_1537" n="e" s="T23">ɨlbat </ts>
               <ts e="T25" id="Seg_1539" n="e" s="T24">kihibin. </ts>
               <ts e="T26" id="Seg_1541" n="e" s="T25">Ü͡öhe </ts>
               <ts e="T27" id="Seg_1543" n="e" s="T26">Ürüŋ Ajɨː </ts>
               <ts e="T28" id="Seg_1545" n="e" s="T27">di͡en </ts>
               <ts e="T29" id="Seg_1547" n="e" s="T28">baːr, </ts>
               <ts e="T30" id="Seg_1549" n="e" s="T29">ol </ts>
               <ts e="T31" id="Seg_1551" n="e" s="T30">kɨːhɨn </ts>
               <ts e="T32" id="Seg_1553" n="e" s="T31">araj </ts>
               <ts e="T33" id="Seg_1555" n="e" s="T32">ɨllakpɨna, </ts>
               <ts e="T34" id="Seg_1557" n="e" s="T33">oččogo </ts>
               <ts e="T35" id="Seg_1559" n="e" s="T34">höbülehi͡ek </ts>
               <ts e="T36" id="Seg_1561" n="e" s="T35">etim", </ts>
               <ts e="T37" id="Seg_1563" n="e" s="T36">diːr. </ts>
               <ts e="T38" id="Seg_1565" n="e" s="T37">Manɨ </ts>
               <ts e="T39" id="Seg_1567" n="e" s="T38">dʼonnor </ts>
               <ts e="T40" id="Seg_1569" n="e" s="T39">"onnʼoːn </ts>
               <ts e="T41" id="Seg_1571" n="e" s="T40">haŋardaga" </ts>
               <ts e="T42" id="Seg_1573" n="e" s="T41">di͡en </ts>
               <ts e="T43" id="Seg_1575" n="e" s="T42">keːheller. </ts>
               <ts e="T44" id="Seg_1577" n="e" s="T43">Biːrde </ts>
               <ts e="T45" id="Seg_1579" n="e" s="T44">munnʼakka </ts>
               <ts e="T46" id="Seg_1581" n="e" s="T45">kineːs </ts>
               <ts e="T47" id="Seg_1583" n="e" s="T46">ojunu </ts>
               <ts e="T48" id="Seg_1585" n="e" s="T47">kaːjar: </ts>
               <ts e="T49" id="Seg_1587" n="e" s="T48">"En </ts>
               <ts e="T50" id="Seg_1589" n="e" s="T49">mi͡eke </ts>
               <ts e="T51" id="Seg_1591" n="e" s="T50">kallaːŋŋa </ts>
               <ts e="T52" id="Seg_1593" n="e" s="T51">baːr </ts>
               <ts e="T53" id="Seg_1595" n="e" s="T52">Ürüŋ Ajɨː </ts>
               <ts e="T54" id="Seg_1597" n="e" s="T53">kɨːhɨn </ts>
               <ts e="T55" id="Seg_1599" n="e" s="T54">dʼaktar </ts>
               <ts e="T56" id="Seg_1601" n="e" s="T55">gɨnɨ͡akpɨn </ts>
               <ts e="T57" id="Seg_1603" n="e" s="T56">hu͡orumdʼulaː", </ts>
               <ts e="T58" id="Seg_1605" n="e" s="T57">diːr. </ts>
               <ts e="T59" id="Seg_1607" n="e" s="T58">Manɨ </ts>
               <ts e="T60" id="Seg_1609" n="e" s="T59">ojuna </ts>
               <ts e="T61" id="Seg_1611" n="e" s="T60">dʼulaja </ts>
               <ts e="T62" id="Seg_1613" n="e" s="T61">ister: </ts>
               <ts e="T63" id="Seg_1615" n="e" s="T62">"Tuguŋ </ts>
               <ts e="T64" id="Seg_1617" n="e" s="T63">haŋataj, </ts>
               <ts e="T65" id="Seg_1619" n="e" s="T64">kajdak </ts>
               <ts e="T66" id="Seg_1621" n="e" s="T65">da </ts>
               <ts e="T67" id="Seg_1623" n="e" s="T66">ol </ts>
               <ts e="T68" id="Seg_1625" n="e" s="T67">dojduttan </ts>
               <ts e="T69" id="Seg_1627" n="e" s="T68">kɨ͡ajan </ts>
               <ts e="T70" id="Seg_1629" n="e" s="T69">egelbet </ts>
               <ts e="T71" id="Seg_1631" n="e" s="T70">hirim", </ts>
               <ts e="T72" id="Seg_1633" n="e" s="T71">diːr. </ts>
               <ts e="T73" id="Seg_1635" n="e" s="T72">Manɨ </ts>
               <ts e="T74" id="Seg_1637" n="e" s="T73">kineːs </ts>
               <ts e="T75" id="Seg_1639" n="e" s="T74">hette </ts>
               <ts e="T76" id="Seg_1641" n="e" s="T75">talagɨ </ts>
               <ts e="T77" id="Seg_1643" n="e" s="T76">kördörör: </ts>
               <ts e="T78" id="Seg_1645" n="e" s="T77">"En </ts>
               <ts e="T79" id="Seg_1647" n="e" s="T78">albɨnnanɨma. </ts>
               <ts e="T80" id="Seg_1649" n="e" s="T79">Min </ts>
               <ts e="T81" id="Seg_1651" n="e" s="T80">ejigin </ts>
               <ts e="T82" id="Seg_1653" n="e" s="T81">i͡eŋŋin </ts>
               <ts e="T83" id="Seg_1655" n="e" s="T82">kastɨ͡am, </ts>
               <ts e="T84" id="Seg_1657" n="e" s="T83">kün </ts>
               <ts e="T85" id="Seg_1659" n="e" s="T84">harsɨn </ts>
               <ts e="T86" id="Seg_1661" n="e" s="T85">Ürüŋ Ajɨː </ts>
               <ts e="T87" id="Seg_1663" n="e" s="T86">kɨːhɨn </ts>
               <ts e="T88" id="Seg_1665" n="e" s="T87">baːr </ts>
               <ts e="T89" id="Seg_1667" n="e" s="T88">gɨn", </ts>
               <ts e="T90" id="Seg_1669" n="e" s="T89">diːr. </ts>
               <ts e="T91" id="Seg_1671" n="e" s="T90">Ojun </ts>
               <ts e="T92" id="Seg_1673" n="e" s="T91">bɨstɨ͡a </ts>
               <ts e="T93" id="Seg_1675" n="e" s="T92">duː, </ts>
               <ts e="T94" id="Seg_1677" n="e" s="T93">dʼe </ts>
               <ts e="T95" id="Seg_1679" n="e" s="T94">kɨːran </ts>
               <ts e="T96" id="Seg_1681" n="e" s="T95">barar. </ts>
               <ts e="T97" id="Seg_1683" n="e" s="T96">Bu </ts>
               <ts e="T98" id="Seg_1685" n="e" s="T97">ojun </ts>
               <ts e="T99" id="Seg_1687" n="e" s="T98">čugahaːbɨtɨn </ts>
               <ts e="T100" id="Seg_1689" n="e" s="T99">isten </ts>
               <ts e="T101" id="Seg_1691" n="e" s="T100">Ürün Ajɨː </ts>
               <ts e="T102" id="Seg_1693" n="e" s="T101">tojon </ts>
               <ts e="T103" id="Seg_1695" n="e" s="T102">hurdeːktik </ts>
               <ts e="T104" id="Seg_1697" n="e" s="T103">u͡oktanar. </ts>
               <ts e="T105" id="Seg_1699" n="e" s="T104">"Tu͡ok </ts>
               <ts e="T106" id="Seg_1701" n="e" s="T105">ölör </ts>
               <ts e="T107" id="Seg_1703" n="e" s="T106">ölüː </ts>
               <ts e="T108" id="Seg_1705" n="e" s="T107">ojuna </ts>
               <ts e="T109" id="Seg_1707" n="e" s="T108">ɨjaːga </ts>
               <ts e="T110" id="Seg_1709" n="e" s="T109">hu͡ok </ts>
               <ts e="T111" id="Seg_1711" n="e" s="T110">kellegej? </ts>
               <ts e="T112" id="Seg_1713" n="e" s="T111">Egeliŋ </ts>
               <ts e="T113" id="Seg_1715" n="e" s="T112">mi͡eke </ts>
               <ts e="T114" id="Seg_1717" n="e" s="T113">u͡ot </ts>
               <ts e="T115" id="Seg_1719" n="e" s="T114">boloppun, </ts>
               <ts e="T116" id="Seg_1721" n="e" s="T115">bahɨ </ts>
               <ts e="T117" id="Seg_1723" n="e" s="T116">bɨha </ts>
               <ts e="T118" id="Seg_1725" n="e" s="T117">oksu͡okpun", </ts>
               <ts e="T119" id="Seg_1727" n="e" s="T118">di͡ebit. </ts>
               <ts e="T120" id="Seg_1729" n="e" s="T119">Ojuna </ts>
               <ts e="T121" id="Seg_1731" n="e" s="T120">kelbite, </ts>
               <ts e="T122" id="Seg_1733" n="e" s="T121">horunan </ts>
               <ts e="T123" id="Seg_1735" n="e" s="T122">oloror </ts>
               <ts e="T124" id="Seg_1737" n="e" s="T123">ebit </ts>
               <ts e="T125" id="Seg_1739" n="e" s="T124">ajɨːta. </ts>
               <ts e="T126" id="Seg_1741" n="e" s="T125">Manɨ </ts>
               <ts e="T127" id="Seg_1743" n="e" s="T126">dʼe </ts>
               <ts e="T128" id="Seg_1745" n="e" s="T127">ünen </ts>
               <ts e="T129" id="Seg_1747" n="e" s="T128">kördöhör: </ts>
               <ts e="T130" id="Seg_1749" n="e" s="T129">"Min </ts>
               <ts e="T131" id="Seg_1751" n="e" s="T130">kɨhalgattan </ts>
               <ts e="T132" id="Seg_1753" n="e" s="T131">kellim, </ts>
               <ts e="T133" id="Seg_1755" n="e" s="T132">en </ts>
               <ts e="T134" id="Seg_1757" n="e" s="T133">kurduk </ts>
               <ts e="T135" id="Seg_1759" n="e" s="T134">hirbitiger </ts>
               <ts e="T136" id="Seg_1761" n="e" s="T135">bahɨlɨktaːk </ts>
               <ts e="T137" id="Seg_1763" n="e" s="T136">bu͡olabɨt — </ts>
               <ts e="T138" id="Seg_1765" n="e" s="T137">kineːs </ts>
               <ts e="T139" id="Seg_1767" n="e" s="T138">di͡en. </ts>
               <ts e="T140" id="Seg_1769" n="e" s="T139">Ol </ts>
               <ts e="T141" id="Seg_1771" n="e" s="T140">kaːjɨːtɨttan </ts>
               <ts e="T142" id="Seg_1773" n="e" s="T141">kellim", </ts>
               <ts e="T143" id="Seg_1775" n="e" s="T142">di͡en </ts>
               <ts e="T144" id="Seg_1777" n="e" s="T143">tu͡onan </ts>
               <ts e="T145" id="Seg_1779" n="e" s="T144">naːdatɨn </ts>
               <ts e="T146" id="Seg_1781" n="e" s="T145">kepsiːr. </ts>
               <ts e="T147" id="Seg_1783" n="e" s="T146">Onu͡oga </ts>
               <ts e="T148" id="Seg_1785" n="e" s="T147">Ürüŋ Ajɨːta </ts>
               <ts e="T149" id="Seg_1787" n="e" s="T148">eter: </ts>
               <ts e="T150" id="Seg_1789" n="e" s="T149">"Hu͡ok! </ts>
               <ts e="T151" id="Seg_1791" n="e" s="T150">"Baran" </ts>
               <ts e="T152" id="Seg_1793" n="e" s="T151">et", </ts>
               <ts e="T153" id="Seg_1795" n="e" s="T152">di͡en </ts>
               <ts e="T154" id="Seg_1797" n="e" s="T153">tönnörör </ts>
               <ts e="T155" id="Seg_1799" n="e" s="T154">ojunun. </ts>
               <ts e="T156" id="Seg_1801" n="e" s="T155">Manta </ts>
               <ts e="T157" id="Seg_1803" n="e" s="T156">kɨːran </ts>
               <ts e="T158" id="Seg_1805" n="e" s="T157">büppütüger </ts>
               <ts e="T159" id="Seg_1807" n="e" s="T158">kineːhe </ts>
               <ts e="T160" id="Seg_1809" n="e" s="T159">kaːjar: </ts>
               <ts e="T161" id="Seg_1811" n="e" s="T160">En </ts>
               <ts e="T162" id="Seg_1813" n="e" s="T161">tü͡ökününen </ts>
               <ts e="T163" id="Seg_1815" n="e" s="T162">onno-manna </ts>
               <ts e="T164" id="Seg_1817" n="e" s="T163">barbɨt </ts>
               <ts e="T165" id="Seg_1819" n="e" s="T164">bu͡olagɨn. </ts>
               <ts e="T166" id="Seg_1821" n="e" s="T165">Tutuːr </ts>
               <ts e="T167" id="Seg_1823" n="e" s="T166">kɨrsaŋ </ts>
               <ts e="T168" id="Seg_1825" n="e" s="T167">manna </ts>
               <ts e="T169" id="Seg_1827" n="e" s="T168">turar </ts>
               <ts e="T170" id="Seg_1829" n="e" s="T169">bu͡olbat </ts>
               <ts e="T171" id="Seg_1831" n="e" s="T170">du͡o", </ts>
               <ts e="T172" id="Seg_1833" n="e" s="T171">di͡en </ts>
               <ts e="T173" id="Seg_1835" n="e" s="T172">kaːjar. </ts>
               <ts e="T174" id="Seg_1837" n="e" s="T173">"Bulgu </ts>
               <ts e="T175" id="Seg_1839" n="e" s="T174">mi͡eke </ts>
               <ts e="T176" id="Seg_1841" n="e" s="T175">kɨːspɨn </ts>
               <ts e="T177" id="Seg_1843" n="e" s="T176">egelen </ts>
               <ts e="T178" id="Seg_1845" n="e" s="T177">bi͡er", </ts>
               <ts e="T179" id="Seg_1847" n="e" s="T178">diːr. </ts>
               <ts e="T180" id="Seg_1849" n="e" s="T179">Dʼe </ts>
               <ts e="T181" id="Seg_1851" n="e" s="T180">emi͡e </ts>
               <ts e="T182" id="Seg_1853" n="e" s="T181">baː </ts>
               <ts e="T183" id="Seg_1855" n="e" s="T182">ojun </ts>
               <ts e="T184" id="Seg_1857" n="e" s="T183">harsɨ͡arda </ts>
               <ts e="T185" id="Seg_1859" n="e" s="T184">kɨːran </ts>
               <ts e="T186" id="Seg_1861" n="e" s="T185">muŋnannar. </ts>
               <ts e="T187" id="Seg_1863" n="e" s="T186">Bu </ts>
               <ts e="T188" id="Seg_1865" n="e" s="T187">kɨːran </ts>
               <ts e="T189" id="Seg_1867" n="e" s="T188">tijbitiger, </ts>
               <ts e="T190" id="Seg_1869" n="e" s="T189">tu͡oktan </ts>
               <ts e="T191" id="Seg_1871" n="e" s="T190">daː </ts>
               <ts e="T192" id="Seg_1873" n="e" s="T191">hürdenen </ts>
               <ts e="T193" id="Seg_1875" n="e" s="T192">Ürüŋ Ajɨːta </ts>
               <ts e="T194" id="Seg_1877" n="e" s="T193">ojunun </ts>
               <ts e="T195" id="Seg_1879" n="e" s="T194">abalɨːr. </ts>
               <ts e="T196" id="Seg_1881" n="e" s="T195">Manɨ͡aka </ts>
               <ts e="T197" id="Seg_1883" n="e" s="T196">emi͡e </ts>
               <ts e="T198" id="Seg_1885" n="e" s="T197">ojuna </ts>
               <ts e="T199" id="Seg_1887" n="e" s="T198">ɨtanan </ts>
               <ts e="T200" id="Seg_1889" n="e" s="T199">kebilener: </ts>
               <ts e="T201" id="Seg_1891" n="e" s="T200">"Haːtar </ts>
               <ts e="T202" id="Seg_1893" n="e" s="T201">beli͡ete </ts>
               <ts e="T203" id="Seg_1895" n="e" s="T202">bi͡er", </ts>
               <ts e="T204" id="Seg_1897" n="e" s="T203">di͡en. </ts>
               <ts e="T205" id="Seg_1899" n="e" s="T204">"Hu͡ok, </ts>
               <ts e="T206" id="Seg_1901" n="e" s="T205">bi͡eri͡em </ts>
               <ts e="T207" id="Seg_1903" n="e" s="T206">hu͡oga, </ts>
               <ts e="T208" id="Seg_1905" n="e" s="T207">togo </ts>
               <ts e="T209" id="Seg_1907" n="e" s="T208">aŋɨraj, </ts>
               <ts e="T210" id="Seg_1909" n="e" s="T209">tu͡ora </ts>
               <ts e="T211" id="Seg_1911" n="e" s="T210">karaktaːk, </ts>
               <ts e="T212" id="Seg_1913" n="e" s="T211">ajɨlaːgɨn </ts>
               <ts e="T213" id="Seg_1915" n="e" s="T212">en </ts>
               <ts e="T214" id="Seg_1917" n="e" s="T213">öllögüŋ", </ts>
               <ts e="T215" id="Seg_1919" n="e" s="T214">di͡en </ts>
               <ts e="T216" id="Seg_1921" n="e" s="T215">ojunun </ts>
               <ts e="T217" id="Seg_1923" n="e" s="T216">emi͡e </ts>
               <ts e="T218" id="Seg_1925" n="e" s="T217">tönnörör. </ts>
               <ts e="T219" id="Seg_1927" n="e" s="T218">Kɨːran </ts>
               <ts e="T220" id="Seg_1929" n="e" s="T219">büten </ts>
               <ts e="T221" id="Seg_1931" n="e" s="T220">baran </ts>
               <ts e="T222" id="Seg_1933" n="e" s="T221">ojun </ts>
               <ts e="T223" id="Seg_1935" n="e" s="T222">kineːsten </ts>
               <ts e="T224" id="Seg_1937" n="e" s="T223">aːrtahar: </ts>
               <ts e="T225" id="Seg_1939" n="e" s="T224">"Tu͡ok </ts>
               <ts e="T226" id="Seg_1941" n="e" s="T225">daː </ts>
               <ts e="T227" id="Seg_1943" n="e" s="T226">barɨta </ts>
               <ts e="T228" id="Seg_1945" n="e" s="T227">üs </ts>
               <ts e="T229" id="Seg_1947" n="e" s="T228">tögülleːk, </ts>
               <ts e="T230" id="Seg_1949" n="e" s="T229">ölörüme, </ts>
               <ts e="T231" id="Seg_1951" n="e" s="T230">"horukpun </ts>
               <ts e="T232" id="Seg_1953" n="e" s="T231">hippete" </ts>
               <ts e="T233" id="Seg_1955" n="e" s="T232">di͡en, </ts>
               <ts e="T234" id="Seg_1957" n="e" s="T233">harsɨn </ts>
               <ts e="T235" id="Seg_1959" n="e" s="T234">emi͡e </ts>
               <ts e="T236" id="Seg_1961" n="e" s="T235">muŋnahan </ts>
               <ts e="T237" id="Seg_1963" n="e" s="T236">körü͡öm", </ts>
               <ts e="T238" id="Seg_1965" n="e" s="T237">diːr. </ts>
               <ts e="T239" id="Seg_1967" n="e" s="T238">Harsɨŋŋɨtɨgar </ts>
               <ts e="T240" id="Seg_1969" n="e" s="T239">ühüs </ts>
               <ts e="T241" id="Seg_1971" n="e" s="T240">kɨːrɨːtɨn </ts>
               <ts e="T242" id="Seg_1973" n="e" s="T241">kɨːrar. </ts>
               <ts e="T243" id="Seg_1975" n="e" s="T242">Bu </ts>
               <ts e="T244" id="Seg_1977" n="e" s="T243">kɨːran </ts>
               <ts e="T245" id="Seg_1979" n="e" s="T244">Ürüŋ Ajɨː </ts>
               <ts e="T246" id="Seg_1981" n="e" s="T245">dʼi͡etiger </ts>
               <ts e="T247" id="Seg_1983" n="e" s="T246">tijer. </ts>
               <ts e="T248" id="Seg_1985" n="e" s="T247">Bu </ts>
               <ts e="T249" id="Seg_1987" n="e" s="T248">tijen </ts>
               <ts e="T250" id="Seg_1989" n="e" s="T249">toŋkojon </ts>
               <ts e="T251" id="Seg_1991" n="e" s="T250">dʼi͡etin </ts>
               <ts e="T252" id="Seg_1993" n="e" s="T251">hu͡onatɨn </ts>
               <ts e="T253" id="Seg_1995" n="e" s="T252">iččitiger </ts>
               <ts e="T254" id="Seg_1997" n="e" s="T253">aːrtahan </ts>
               <ts e="T255" id="Seg_1999" n="e" s="T254">Ürüŋ Ajɨːtɨn </ts>
               <ts e="T256" id="Seg_2001" n="e" s="T255">atagar </ts>
               <ts e="T257" id="Seg_2003" n="e" s="T256">hɨːlan </ts>
               <ts e="T258" id="Seg_2005" n="e" s="T257">kelen </ts>
               <ts e="T259" id="Seg_2007" n="e" s="T258">baː </ts>
               <ts e="T260" id="Seg_2009" n="e" s="T259">dʼiŋŋi-ti͡egeːger </ts>
               <ts e="T261" id="Seg_2011" n="e" s="T260">hürdeːktik </ts>
               <ts e="T262" id="Seg_2013" n="e" s="T261">tu͡onar-ɨtɨːr. </ts>
               <ts e="T263" id="Seg_2015" n="e" s="T262">Manɨ </ts>
               <ts e="T264" id="Seg_2017" n="e" s="T263">dʼe </ts>
               <ts e="T265" id="Seg_2019" n="e" s="T264">hüreginen </ts>
               <ts e="T266" id="Seg_2021" n="e" s="T265">ahɨnar. </ts>
               <ts e="T267" id="Seg_2023" n="e" s="T266">"Dʼe </ts>
               <ts e="T268" id="Seg_2025" n="e" s="T267">kirdik, </ts>
               <ts e="T269" id="Seg_2027" n="e" s="T268">min </ts>
               <ts e="T270" id="Seg_2029" n="e" s="T269">ejigin </ts>
               <ts e="T271" id="Seg_2031" n="e" s="T270">ölöröːrü </ts>
               <ts e="T272" id="Seg_2033" n="e" s="T271">bejem </ts>
               <ts e="T273" id="Seg_2035" n="e" s="T272">ojunum </ts>
               <ts e="T274" id="Seg_2037" n="e" s="T273">gɨmmatagɨm. </ts>
               <ts e="T275" id="Seg_2039" n="e" s="T274">Togo </ts>
               <ts e="T276" id="Seg_2041" n="e" s="T275">bagas </ts>
               <ts e="T277" id="Seg_2043" n="e" s="T276">kineːhiŋ </ts>
               <ts e="T278" id="Seg_2045" n="e" s="T277">oburgu </ts>
               <ts e="T279" id="Seg_2047" n="e" s="T278">aŋɨraj! </ts>
               <ts e="T280" id="Seg_2049" n="e" s="T279">Dʼe </ts>
               <ts e="T281" id="Seg_2051" n="e" s="T280">bejetin </ts>
               <ts e="T282" id="Seg_2053" n="e" s="T281">noru͡otugar </ts>
               <ts e="T283" id="Seg_2055" n="e" s="T282">i͡edeːni </ts>
               <ts e="T284" id="Seg_2057" n="e" s="T283">oŋordogo. </ts>
               <ts e="T285" id="Seg_2059" n="e" s="T284">Noru͡okkututtan </ts>
               <ts e="T286" id="Seg_2061" n="e" s="T285">hogotok </ts>
               <ts e="T287" id="Seg_2063" n="e" s="T286">en </ts>
               <ts e="T288" id="Seg_2065" n="e" s="T287">u͡olgun </ts>
               <ts e="T289" id="Seg_2067" n="e" s="T288">kɨtarɨ </ts>
               <ts e="T290" id="Seg_2069" n="e" s="T289">ordu͡okkut. </ts>
               <ts e="T291" id="Seg_2071" n="e" s="T290">Tu͡ok </ts>
               <ts e="T292" id="Seg_2073" n="e" s="T291">da </ts>
               <ts e="T293" id="Seg_2075" n="e" s="T292">tɨːnnaːk </ts>
               <ts e="T294" id="Seg_2077" n="e" s="T293">kaːlɨ͡a </ts>
               <ts e="T295" id="Seg_2079" n="e" s="T294">hu͡oga. </ts>
               <ts e="T296" id="Seg_2081" n="e" s="T295">Atɨn </ts>
               <ts e="T297" id="Seg_2083" n="e" s="T296">hirten </ts>
               <ts e="T298" id="Seg_2085" n="e" s="T297">dʼon </ts>
               <ts e="T299" id="Seg_2087" n="e" s="T298">kelenner, </ts>
               <ts e="T300" id="Seg_2089" n="e" s="T299">onton </ts>
               <ts e="T301" id="Seg_2091" n="e" s="T300">haŋa </ts>
               <ts e="T302" id="Seg_2093" n="e" s="T301">dʼon </ts>
               <ts e="T303" id="Seg_2095" n="e" s="T302">ü͡öskü͡öge", </ts>
               <ts e="T304" id="Seg_2097" n="e" s="T303">diːr. </ts>
               <ts e="T305" id="Seg_2099" n="e" s="T304">"Haŋa </ts>
               <ts e="T306" id="Seg_2101" n="e" s="T305">ɨraːs </ts>
               <ts e="T307" id="Seg_2103" n="e" s="T306">čeːlkeː </ts>
               <ts e="T308" id="Seg_2105" n="e" s="T307">öldüːnne </ts>
               <ts e="T309" id="Seg_2107" n="e" s="T308">anɨ </ts>
               <ts e="T310" id="Seg_2109" n="e" s="T309">üs </ts>
               <ts e="T311" id="Seg_2111" n="e" s="T310">konugunan </ts>
               <ts e="T312" id="Seg_2113" n="e" s="T311">harsɨ͡arda </ts>
               <ts e="T313" id="Seg_2115" n="e" s="T312">turu͡ordunnar, </ts>
               <ts e="T314" id="Seg_2117" n="e" s="T313">ču͡ogur </ts>
               <ts e="T315" id="Seg_2119" n="e" s="T314">čeːlkeː </ts>
               <ts e="T316" id="Seg_2121" n="e" s="T315">tabanɨ </ts>
               <ts e="T317" id="Seg_2123" n="e" s="T316">ölördünner, </ts>
               <ts e="T318" id="Seg_2125" n="e" s="T317">ol </ts>
               <ts e="T319" id="Seg_2127" n="e" s="T318">uraha </ts>
               <ts e="T320" id="Seg_2129" n="e" s="T319">tulatɨn </ts>
               <ts e="T321" id="Seg_2131" n="e" s="T320">barɨtɨn </ts>
               <ts e="T322" id="Seg_2133" n="e" s="T321">čeːlkeː </ts>
               <ts e="T323" id="Seg_2135" n="e" s="T322">taba </ts>
               <ts e="T324" id="Seg_2137" n="e" s="T323">tiriːtinen </ts>
               <ts e="T325" id="Seg_2139" n="e" s="T324">telgeːtinner. </ts>
               <ts e="T326" id="Seg_2141" n="e" s="T325">Manan </ts>
               <ts e="T327" id="Seg_2143" n="e" s="T326">üktenen </ts>
               <ts e="T328" id="Seg_2145" n="e" s="T327">kiːri͡ege </ts>
               <ts e="T329" id="Seg_2147" n="e" s="T328">kineːskit. </ts>
               <ts e="T330" id="Seg_2149" n="e" s="T329">Bɨrtak </ts>
               <ts e="T331" id="Seg_2151" n="e" s="T330">tu͡ok </ts>
               <ts e="T332" id="Seg_2153" n="e" s="T331">da </ts>
               <ts e="T333" id="Seg_2155" n="e" s="T332">hɨstɨ͡a </ts>
               <ts e="T334" id="Seg_2157" n="e" s="T333">hu͡oktaːk", </ts>
               <ts e="T335" id="Seg_2159" n="e" s="T334">diːr. </ts>
               <ts e="T336" id="Seg_2161" n="e" s="T335">"Dʼonnor </ts>
               <ts e="T337" id="Seg_2163" n="e" s="T336">kurum </ts>
               <ts e="T338" id="Seg_2165" n="e" s="T337">di͡en </ts>
               <ts e="T339" id="Seg_2167" n="e" s="T338">kelbetinner, </ts>
               <ts e="T340" id="Seg_2169" n="e" s="T339">kim </ts>
               <ts e="T341" id="Seg_2171" n="e" s="T340">tɨːnɨn </ts>
               <ts e="T342" id="Seg_2173" n="e" s="T341">karɨstaːrɨ </ts>
               <ts e="T343" id="Seg_2175" n="e" s="T342">gɨnar </ts>
               <ts e="T344" id="Seg_2177" n="e" s="T343">ɨraːk </ts>
               <ts e="T345" id="Seg_2179" n="e" s="T344">bardɨn", </ts>
               <ts e="T346" id="Seg_2181" n="e" s="T345">di͡ebit. </ts>
               <ts e="T347" id="Seg_2183" n="e" s="T346">Ühüs </ts>
               <ts e="T348" id="Seg_2185" n="e" s="T347">künüger </ts>
               <ts e="T349" id="Seg_2187" n="e" s="T348">eppitin </ts>
               <ts e="T350" id="Seg_2189" n="e" s="T349">kurduk </ts>
               <ts e="T351" id="Seg_2191" n="e" s="T350">oŋorbuttar. </ts>
               <ts e="T352" id="Seg_2193" n="e" s="T351">Manna </ts>
               <ts e="T353" id="Seg_2195" n="e" s="T352">ojun </ts>
               <ts e="T354" id="Seg_2197" n="e" s="T353">körön </ts>
               <ts e="T355" id="Seg_2199" n="e" s="T354">turbut. </ts>
               <ts e="T356" id="Seg_2201" n="e" s="T355">Kütür </ts>
               <ts e="T357" id="Seg_2203" n="e" s="T356">bagajɨ </ts>
               <ts e="T358" id="Seg_2205" n="e" s="T357">boloho </ts>
               <ts e="T359" id="Seg_2207" n="e" s="T358">tɨ͡al </ts>
               <ts e="T360" id="Seg_2209" n="e" s="T359">tüspüt, </ts>
               <ts e="T361" id="Seg_2211" n="e" s="T360">ču͡oraːn </ts>
               <ts e="T362" id="Seg_2213" n="e" s="T361">tɨ͡aha </ts>
               <ts e="T363" id="Seg_2215" n="e" s="T362">di͡en </ts>
               <ts e="T364" id="Seg_2217" n="e" s="T363">bagajɨ </ts>
               <ts e="T365" id="Seg_2219" n="e" s="T364">ihillibit. </ts>
               <ts e="T366" id="Seg_2221" n="e" s="T365">Tu͡ok </ts>
               <ts e="T367" id="Seg_2223" n="e" s="T366">ere </ts>
               <ts e="T368" id="Seg_2225" n="e" s="T367">tɨ͡al </ts>
               <ts e="T369" id="Seg_2227" n="e" s="T368">kurduk </ts>
               <ts e="T370" id="Seg_2229" n="e" s="T369">uraha </ts>
               <ts e="T371" id="Seg_2231" n="e" s="T370">aːnɨn </ts>
               <ts e="T372" id="Seg_2233" n="e" s="T371">arɨjarga </ts>
               <ts e="T373" id="Seg_2235" n="e" s="T372">dɨlɨ </ts>
               <ts e="T374" id="Seg_2237" n="e" s="T373">gɨmmɨt. </ts>
               <ts e="T375" id="Seg_2239" n="e" s="T374">Ol </ts>
               <ts e="T376" id="Seg_2241" n="e" s="T375">kennine </ts>
               <ts e="T377" id="Seg_2243" n="e" s="T376">tɨ͡ala </ts>
               <ts e="T378" id="Seg_2245" n="e" s="T377">hu͡ok </ts>
               <ts e="T379" id="Seg_2247" n="e" s="T378">bu͡olbut. </ts>
               <ts e="T380" id="Seg_2249" n="e" s="T379">Manɨ </ts>
               <ts e="T381" id="Seg_2251" n="e" s="T380">ojun </ts>
               <ts e="T382" id="Seg_2253" n="e" s="T381">baran </ts>
               <ts e="T383" id="Seg_2255" n="e" s="T382">körbüte, </ts>
               <ts e="T384" id="Seg_2257" n="e" s="T383">kineːs </ts>
               <ts e="T385" id="Seg_2259" n="e" s="T384">da </ts>
               <ts e="T386" id="Seg_2261" n="e" s="T385">barɨ </ts>
               <ts e="T387" id="Seg_2263" n="e" s="T386">kelbit </ts>
               <ts e="T388" id="Seg_2265" n="e" s="T387">da </ts>
               <ts e="T389" id="Seg_2267" n="e" s="T388">dʼon </ts>
               <ts e="T390" id="Seg_2269" n="e" s="T389">ölö </ts>
               <ts e="T391" id="Seg_2271" n="e" s="T390">hɨtallar </ts>
               <ts e="T392" id="Seg_2273" n="e" s="T391">ebit. </ts>
               <ts e="T393" id="Seg_2275" n="e" s="T392">Manan </ts>
               <ts e="T394" id="Seg_2277" n="e" s="T393">norilʼskaj </ts>
               <ts e="T395" id="Seg_2279" n="e" s="T394">dʼadajda. </ts>
               <ts e="T396" id="Seg_2281" n="e" s="T395">Iti </ts>
               <ts e="T397" id="Seg_2283" n="e" s="T396">edʼeːn </ts>
               <ts e="T398" id="Seg_2285" n="e" s="T397">tu͡orattan </ts>
               <ts e="T399" id="Seg_2287" n="e" s="T398">kelen </ts>
               <ts e="T400" id="Seg_2289" n="e" s="T399">onton </ts>
               <ts e="T401" id="Seg_2291" n="e" s="T400">ere </ts>
               <ts e="T402" id="Seg_2293" n="e" s="T401">ü͡ösküːr </ts>
               <ts e="T403" id="Seg_2295" n="e" s="T402">bu͡ollular. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_2296" s="T0">BaRD_1930_DaughterOfUrungAjyy_flk.001 (001.001)</ta>
            <ta e="T6" id="Seg_2297" s="T4">BaRD_1930_DaughterOfUrungAjyy_flk.002 (001.002)</ta>
            <ta e="T14" id="Seg_2298" s="T6">BaRD_1930_DaughterOfUrungAjyy_flk.003 (001.003)</ta>
            <ta e="T17" id="Seg_2299" s="T14">BaRD_1930_DaughterOfUrungAjyy_flk.004 (001.004)</ta>
            <ta e="T19" id="Seg_2300" s="T17">BaRD_1930_DaughterOfUrungAjyy_flk.005 (001.006)</ta>
            <ta e="T25" id="Seg_2301" s="T19">BaRD_1930_DaughterOfUrungAjyy_flk.006 (001.007)</ta>
            <ta e="T37" id="Seg_2302" s="T25">BaRD_1930_DaughterOfUrungAjyy_flk.007 (001.008)</ta>
            <ta e="T43" id="Seg_2303" s="T37">BaRD_1930_DaughterOfUrungAjyy_flk.008 (001.009)</ta>
            <ta e="T48" id="Seg_2304" s="T43">BaRD_1930_DaughterOfUrungAjyy_flk.009 (001.010)</ta>
            <ta e="T58" id="Seg_2305" s="T48">BaRD_1930_DaughterOfUrungAjyy_flk.010 (001.011)</ta>
            <ta e="T62" id="Seg_2306" s="T58">BaRD_1930_DaughterOfUrungAjyy_flk.011 (001.012)</ta>
            <ta e="T72" id="Seg_2307" s="T62">BaRD_1930_DaughterOfUrungAjyy_flk.012 (001.013)</ta>
            <ta e="T77" id="Seg_2308" s="T72">BaRD_1930_DaughterOfUrungAjyy_flk.013 (001.014)</ta>
            <ta e="T79" id="Seg_2309" s="T77">BaRD_1930_DaughterOfUrungAjyy_flk.014 (001.015)</ta>
            <ta e="T90" id="Seg_2310" s="T79">BaRD_1930_DaughterOfUrungAjyy_flk.015 (001.016)</ta>
            <ta e="T96" id="Seg_2311" s="T90">BaRD_1930_DaughterOfUrungAjyy_flk.016 (001.018)</ta>
            <ta e="T104" id="Seg_2312" s="T96">BaRD_1930_DaughterOfUrungAjyy_flk.017 (001.019)</ta>
            <ta e="T111" id="Seg_2313" s="T104">BaRD_1930_DaughterOfUrungAjyy_flk.018 (001.020)</ta>
            <ta e="T119" id="Seg_2314" s="T111">BaRD_1930_DaughterOfUrungAjyy_flk.019 (001.021)</ta>
            <ta e="T125" id="Seg_2315" s="T119">BaRD_1930_DaughterOfUrungAjyy_flk.020 (001.023)</ta>
            <ta e="T129" id="Seg_2316" s="T125">BaRD_1930_DaughterOfUrungAjyy_flk.021 (001.024)</ta>
            <ta e="T139" id="Seg_2317" s="T129">BaRD_1930_DaughterOfUrungAjyy_flk.022 (001.025)</ta>
            <ta e="T146" id="Seg_2318" s="T139">BaRD_1930_DaughterOfUrungAjyy_flk.023 (001.026)</ta>
            <ta e="T149" id="Seg_2319" s="T146">BaRD_1930_DaughterOfUrungAjyy_flk.024 (001.027)</ta>
            <ta e="T150" id="Seg_2320" s="T149">BaRD_1930_DaughterOfUrungAjyy_flk.025 (001.028)</ta>
            <ta e="T155" id="Seg_2321" s="T150">BaRD_1930_DaughterOfUrungAjyy_flk.026 (001.029)</ta>
            <ta e="T160" id="Seg_2322" s="T155">BaRD_1930_DaughterOfUrungAjyy_flk.027 (001.030)</ta>
            <ta e="T165" id="Seg_2323" s="T160">BaRD_1930_DaughterOfUrungAjyy_flk.028 (001.031)</ta>
            <ta e="T173" id="Seg_2324" s="T165">BaRD_1930_DaughterOfUrungAjyy_flk.029 (001.032)</ta>
            <ta e="T179" id="Seg_2325" s="T173">BaRD_1930_DaughterOfUrungAjyy_flk.030 (001.034)</ta>
            <ta e="T186" id="Seg_2326" s="T179">BaRD_1930_DaughterOfUrungAjyy_flk.031 (001.036)</ta>
            <ta e="T195" id="Seg_2327" s="T186">BaRD_1930_DaughterOfUrungAjyy_flk.032 (001.037)</ta>
            <ta e="T200" id="Seg_2328" s="T195">BaRD_1930_DaughterOfUrungAjyy_flk.033 (001.038)</ta>
            <ta e="T204" id="Seg_2329" s="T200">BaRD_1930_DaughterOfUrungAjyy_flk.034 (001.039)</ta>
            <ta e="T218" id="Seg_2330" s="T204">BaRD_1930_DaughterOfUrungAjyy_flk.035 (001.040)</ta>
            <ta e="T224" id="Seg_2331" s="T218">BaRD_1930_DaughterOfUrungAjyy_flk.036 (001.041)</ta>
            <ta e="T238" id="Seg_2332" s="T224">BaRD_1930_DaughterOfUrungAjyy_flk.037 (001.042)</ta>
            <ta e="T242" id="Seg_2333" s="T238">BaRD_1930_DaughterOfUrungAjyy_flk.038 (001.043)</ta>
            <ta e="T247" id="Seg_2334" s="T242">BaRD_1930_DaughterOfUrungAjyy_flk.039 (001.044)</ta>
            <ta e="T262" id="Seg_2335" s="T247">BaRD_1930_DaughterOfUrungAjyy_flk.040 (001.045)</ta>
            <ta e="T266" id="Seg_2336" s="T262">BaRD_1930_DaughterOfUrungAjyy_flk.041 (001.046)</ta>
            <ta e="T274" id="Seg_2337" s="T266">BaRD_1930_DaughterOfUrungAjyy_flk.042 (001.047)</ta>
            <ta e="T279" id="Seg_2338" s="T274">BaRD_1930_DaughterOfUrungAjyy_flk.043 (001.048)</ta>
            <ta e="T284" id="Seg_2339" s="T279">BaRD_1930_DaughterOfUrungAjyy_flk.044 (001.049)</ta>
            <ta e="T290" id="Seg_2340" s="T284">BaRD_1930_DaughterOfUrungAjyy_flk.045 (001.050)</ta>
            <ta e="T295" id="Seg_2341" s="T290">BaRD_1930_DaughterOfUrungAjyy_flk.046 (001.051)</ta>
            <ta e="T304" id="Seg_2342" s="T295">BaRD_1930_DaughterOfUrungAjyy_flk.047 (001.052)</ta>
            <ta e="T325" id="Seg_2343" s="T304">BaRD_1930_DaughterOfUrungAjyy_flk.048 (001.053)</ta>
            <ta e="T329" id="Seg_2344" s="T325">BaRD_1930_DaughterOfUrungAjyy_flk.049 (001.054)</ta>
            <ta e="T335" id="Seg_2345" s="T329">BaRD_1930_DaughterOfUrungAjyy_flk.050 (001.055)</ta>
            <ta e="T346" id="Seg_2346" s="T335">BaRD_1930_DaughterOfUrungAjyy_flk.051 (001.056)</ta>
            <ta e="T351" id="Seg_2347" s="T346">BaRD_1930_DaughterOfUrungAjyy_flk.052 (001.057)</ta>
            <ta e="T355" id="Seg_2348" s="T351">BaRD_1930_DaughterOfUrungAjyy_flk.053 (001.058)</ta>
            <ta e="T365" id="Seg_2349" s="T355">BaRD_1930_DaughterOfUrungAjyy_flk.054 (001.059)</ta>
            <ta e="T374" id="Seg_2350" s="T365">BaRD_1930_DaughterOfUrungAjyy_flk.055 (001.060)</ta>
            <ta e="T379" id="Seg_2351" s="T374">BaRD_1930_DaughterOfUrungAjyy_flk.056 (001.061)</ta>
            <ta e="T392" id="Seg_2352" s="T379">BaRD_1930_DaughterOfUrungAjyy_flk.057 (001.062)</ta>
            <ta e="T395" id="Seg_2353" s="T392">BaRD_1930_DaughterOfUrungAjyy_flk.058 (001.063)</ta>
            <ta e="T403" id="Seg_2354" s="T395">BaRD_1930_DaughterOfUrungAjyy_flk.059 (001.064)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T4" id="Seg_2355" s="T0">Былыр Норилскайга кинээстээктэр үһү.</ta>
            <ta e="T6" id="Seg_2356" s="T4">Һиҥил киһи.</ta>
            <ta e="T14" id="Seg_2357" s="T6">Бу киһилэрэ дьактара һуок, маны дьонноро биирдэ ыйыталлар:</ta>
            <ta e="T17" id="Seg_2358" s="T14">— Того дьактардаммаккын? — диэн.</ta>
            <ta e="T19" id="Seg_2359" s="T17">Онуога этэр:</ta>
            <ta e="T25" id="Seg_2360" s="T19">— Мин бу дойду кыыһын ылбат киһибин.</ta>
            <ta e="T37" id="Seg_2361" s="T25">Үөһэ Үрүҥ Айыы диэн баар, ол кыыһын арай ыллакпына, оччого һөбүлэһиэк этим, — диир.</ta>
            <ta e="T43" id="Seg_2362" s="T37">Маны дьоннор онньоон һаҥардага диэн кээһэллэр.</ta>
            <ta e="T48" id="Seg_2363" s="T43">Биирдэ мунньакка кинээс ойуну каайар:</ta>
            <ta e="T58" id="Seg_2364" s="T48">— Эн миэкэ каллааҥҥа баар Үрүҥ Айыы кыыһын дьактар гыныакпын һуорумдьулаа, — диир.</ta>
            <ta e="T62" id="Seg_2365" s="T58">Маны ойуна дьулайа истэр:</ta>
            <ta e="T72" id="Seg_2366" s="T62">— Тугуҥ һаҥатай, кайдак да ол дойдуттан кыайан эгэлбэт һирим, — диир.</ta>
            <ta e="T77" id="Seg_2367" s="T72">Маны кинээс һэттэ талагы көрдөрөр:</ta>
            <ta e="T79" id="Seg_2368" s="T77">— Эн албыннаныма.</ta>
            <ta e="T90" id="Seg_2369" s="T79">Мин эйигин иэҥҥин кастыам, күн һарсын Үрүҥ Айыы кыыһын баар гын! — диир.</ta>
            <ta e="T96" id="Seg_2370" s="T90">Ойун быстыа дуу, дьэ кыыран барар.</ta>
            <ta e="T104" id="Seg_2371" s="T96">Бу ойун чугаһаабытын истэн Үрүн Айыы тойон һурдээктик уоктанар.</ta>
            <ta e="T111" id="Seg_2372" s="T104">— Туок өлөр өлүү ойуна ыйаага һуок кэллэгэй?!</ta>
            <ta e="T119" id="Seg_2373" s="T111">Эгэлиҥ миэкэ уот болоппун, баһы[н] быһа оксуокпун! — диэбит.</ta>
            <ta e="T125" id="Seg_2374" s="T119">Ойуна кэлбитэ: һорунан олорор эбит айыыта.</ta>
            <ta e="T129" id="Seg_2375" s="T125">Маны дьэ үнэн көрдөһөр:</ta>
            <ta e="T139" id="Seg_2376" s="T129">— Мин кыһалгаттан кэллим, эн курдук һирбитигэр баһылыктаак буолабыт — кинээс диэн.</ta>
            <ta e="T146" id="Seg_2377" s="T139">Ол каайыытыттан кэллим, — диэн туонан наадатын кэпсиир.</ta>
            <ta e="T149" id="Seg_2378" s="T146">Онуога Үрүҥ Айыыта этэр:</ta>
            <ta e="T150" id="Seg_2379" s="T149">— Һуок!</ta>
            <ta e="T155" id="Seg_2380" s="T150">Баран эт, — диэн төннөрөр ойунун.</ta>
            <ta e="T160" id="Seg_2381" s="T155">Манта кыыран бүппүтүгэр кинээһэ каайар:</ta>
            <ta e="T165" id="Seg_2382" s="T160">— Эн түөкүнүнэн онно-манна барбыт буолагын.</ta>
            <ta e="T173" id="Seg_2383" s="T165">Тутуур кырсаҥ манна турар буолбат дуо? — диэн каайар.</ta>
            <ta e="T179" id="Seg_2384" s="T173">— Булгу миэкэ кыыспын эгэлэн биэр! — диир.</ta>
            <ta e="T186" id="Seg_2385" s="T179">Дьэ эмиэ баа ойун һарсыарда кыыран муҥнаннар.</ta>
            <ta e="T195" id="Seg_2386" s="T186">Бу кыыран тийбитигэр, туоктан даа һүрдэнэн Үрүҥ Айыыта ойунун абалыыр.</ta>
            <ta e="T200" id="Seg_2387" s="T195">Маныака эмиэ ойуна ытанан кэбилэнэр:</ta>
            <ta e="T204" id="Seg_2388" s="T200">— Һаатар бэлиэтэ биэр, — диэн.</ta>
            <ta e="T218" id="Seg_2389" s="T204">— һуок, биэриэм һуога, того аҥырай, туора карактаак, айылаагын эн өллөгүҥ, — диэн ойунун эмиэ төннөрөр.</ta>
            <ta e="T224" id="Seg_2390" s="T218">Кыыран бүтэн баран ойун кинээстэн аартаһар:</ta>
            <ta e="T238" id="Seg_2391" s="T224">— Туок даа барыта үс төгүллээк, өлөрүмэ, һорукпун һиппэтэ диэн, һарсын эмиэ муҥнаһан көрүөм, — диир.</ta>
            <ta e="T242" id="Seg_2392" s="T238">Һарсыҥҥытыгар үһүс кыырыытын кыырар.</ta>
            <ta e="T247" id="Seg_2393" s="T242">Бу кыыран Үрүҥ Айыы дьиэтигэр тийэр.</ta>
            <ta e="T262" id="Seg_2394" s="T247">Бу тийэн тоҥкойон дьиэтин һуонатын иччитигэр аартаһан Үрүҥ Айыытын атагар һыылан кэлэн баа дьиҥҥи-тиэгээгэр һүрдээктик туонар-ытыыр.</ta>
            <ta e="T266" id="Seg_2395" s="T262">Маны дьэ һүрэгинэн аһынар.</ta>
            <ta e="T274" id="Seg_2396" s="T266">— Дьэ кирдик, мин эйигин өлөрөөрү бэйэм ойунум гымматагым.</ta>
            <ta e="T279" id="Seg_2397" s="T274">Того багас кинээһиҥ обургу аҥырай!</ta>
            <ta e="T284" id="Seg_2398" s="T279">Дьэ бэйэтин норуотугар иэдээни оҥордого.</ta>
            <ta e="T290" id="Seg_2399" s="T284">Норуоккутуттан һоготок эн уолгун кытары ордуоккут.</ta>
            <ta e="T295" id="Seg_2400" s="T290">Туок да тыыннаак каалыа һуога.</ta>
            <ta e="T304" id="Seg_2401" s="T295">Атын һиртэн дьон кэлэннэр, онтон һаҥа дьон үөскүөгэ, — диир.</ta>
            <ta e="T325" id="Seg_2402" s="T304">— һаҥа ыраас чээлкээ өлдүүннэ аны үс конугунан һарсыарда туруордуннар, чуогур чээлкээ табаны өлөрдүннэр, ол ураһа тулатын барытын чээлкээ таба тириитинэн тэлгээтиннэр.</ta>
            <ta e="T329" id="Seg_2403" s="T325">Манан үктэнэн киириэгэ кинээскит.</ta>
            <ta e="T335" id="Seg_2404" s="T329">Быртак туок да һыстыа һуоктаак, — диир.</ta>
            <ta e="T346" id="Seg_2405" s="T335">— Дьоннор курум диэн кэлбэтиннэр, ким тыынын карыстаары гынар ыраак бардын, — диэбит.</ta>
            <ta e="T351" id="Seg_2406" s="T346">Үһүс күнүгэр эппитин курдук оҥорбуттар.</ta>
            <ta e="T355" id="Seg_2407" s="T351">Манна ойун көрөн турбут.</ta>
            <ta e="T365" id="Seg_2408" s="T355">Күтүр багайы болоһо тыал түспүт, чуораан тыаһа диэн багайы иһиллибит.</ta>
            <ta e="T374" id="Seg_2409" s="T365">Туок эрэ тыал курдук ураһа аанын арыйарга дылы гыммыт.</ta>
            <ta e="T379" id="Seg_2410" s="T374">Ол кэннинэ тыала һуок буолбут.</ta>
            <ta e="T392" id="Seg_2411" s="T379">Маны ойун баран көрбүтэ: кинээс да бары кэлбит да дьон өлө һыталлар эбит.</ta>
            <ta e="T395" id="Seg_2412" s="T392">Манан норильскай дьадайда.</ta>
            <ta e="T403" id="Seg_2413" s="T395">Ити эдьээн туораттан кэлэн онтон эрэ үөскүүр буоллулар.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_2414" s="T0">Bɨlɨr Norilskajga kineːsteːkter ühü. </ta>
            <ta e="T6" id="Seg_2415" s="T4">Hiŋil kihi. </ta>
            <ta e="T14" id="Seg_2416" s="T6">Bu kihilere dʼaktara hu͡ok, manɨ dʼonnoro biːrde ɨjɨtallar: </ta>
            <ta e="T17" id="Seg_2417" s="T14">"Togo dʼaktardammakkɨn", di͡en. </ta>
            <ta e="T19" id="Seg_2418" s="T17">Onu͡oga eter: </ta>
            <ta e="T25" id="Seg_2419" s="T19">"Min bu dojdu kɨːhɨn ɨlbat kihibin. </ta>
            <ta e="T37" id="Seg_2420" s="T25">Ü͡öhe Ürüŋ Ajɨː di͡en baːr, ol kɨːhɨn araj ɨllakpɨna, oččogo höbülehi͡ek etim", diːr. </ta>
            <ta e="T43" id="Seg_2421" s="T37">Manɨ dʼonnor "onnʼoːn haŋardaga" di͡en keːheller. </ta>
            <ta e="T48" id="Seg_2422" s="T43">Biːrde munnʼakka kineːs ojunu kaːjar:</ta>
            <ta e="T58" id="Seg_2423" s="T48">"En mi͡eke kallaːŋŋa baːr Ürüŋ Ajɨː kɨːhɨn dʼaktar gɨnɨ͡akpɨn hu͡orumdʼulaː", diːr. </ta>
            <ta e="T62" id="Seg_2424" s="T58">Manɨ ojuna dʼulaja ister:</ta>
            <ta e="T72" id="Seg_2425" s="T62">"Tuguŋ haŋataj, kajdak da ol dojduttan kɨ͡ajan egelbet hirim", diːr. </ta>
            <ta e="T77" id="Seg_2426" s="T72">Manɨ kineːs hette talagɨ kördörör:</ta>
            <ta e="T79" id="Seg_2427" s="T77">"En albɨnnanɨma. </ta>
            <ta e="T90" id="Seg_2428" s="T79">Min ejigin i͡eŋŋin kastɨ͡am, kün harsɨn Ürüŋ Ajɨː kɨːhɨn baːr gɨn", diːr. </ta>
            <ta e="T96" id="Seg_2429" s="T90">Ojun bɨstɨ͡a duː, dʼe kɨːran barar. </ta>
            <ta e="T104" id="Seg_2430" s="T96">Bu ojun čugahaːbɨtɨn isten Ürün Ajɨː tojon hurdeːktik u͡oktanar. </ta>
            <ta e="T111" id="Seg_2431" s="T104">"Tu͡ok ölör ölüː ojuna ɨjaːga hu͡ok kellegej?! </ta>
            <ta e="T119" id="Seg_2432" s="T111">Egeliŋ mi͡eke u͡ot boloppun, bahɨ bɨha oksu͡okpun", di͡ebit. </ta>
            <ta e="T125" id="Seg_2433" s="T119">Ojuna kelbite, horunan oloror ebit ajɨːta. </ta>
            <ta e="T129" id="Seg_2434" s="T125">Manɨ dʼe ünen kördöhör:</ta>
            <ta e="T139" id="Seg_2435" s="T129">"Min kɨhalgattan kellim, en kurduk hirbitiger bahɨlɨktaːk bu͡olabɨt — kineːs di͡en. </ta>
            <ta e="T146" id="Seg_2436" s="T139">Ol kaːjɨːtɨttan kellim", di͡en tu͡onan naːdatɨn kepsiːr. </ta>
            <ta e="T149" id="Seg_2437" s="T146">Onu͡oga Ürüŋ Ajɨːta eter:</ta>
            <ta e="T150" id="Seg_2438" s="T149">"Hu͡ok! </ta>
            <ta e="T155" id="Seg_2439" s="T150">"Baran" et", di͡en tönnörör ojunun. </ta>
            <ta e="T160" id="Seg_2440" s="T155">Manta kɨːran büppütüger kineːhe kaːjar: </ta>
            <ta e="T165" id="Seg_2441" s="T160">"En tü͡ökününen onno-manna barbɨt bu͡olagɨn. </ta>
            <ta e="T173" id="Seg_2442" s="T165">Tutuːr kɨrsaŋ manna turar bu͡olbat du͡o", di͡en kaːjar. </ta>
            <ta e="T179" id="Seg_2443" s="T173">"Bulgu mi͡eke kɨːspɨn egelen bi͡er", diːr. </ta>
            <ta e="T186" id="Seg_2444" s="T179">Dʼe emi͡e baː ojun harsɨ͡arda kɨːran muŋnannar. </ta>
            <ta e="T195" id="Seg_2445" s="T186">Bu kɨːran tijbitiger, tu͡oktan daː hürdenen Ürüŋ Ajɨːta ojunun abalɨːr. </ta>
            <ta e="T200" id="Seg_2446" s="T195">Manɨ͡aka emi͡e ojuna ɨtanan kebilener:</ta>
            <ta e="T204" id="Seg_2447" s="T200">"Haːtar beli͡ete bi͡er", di͡en. </ta>
            <ta e="T218" id="Seg_2448" s="T204">"Hu͡ok, bi͡eri͡em hu͡oga, togo aŋɨraj, tu͡ora karaktaːk, ajɨlaːgɨn en öllögüŋ", di͡en ojunun emi͡e tönnörör. </ta>
            <ta e="T224" id="Seg_2449" s="T218">Kɨːran büten baran ojun kineːsten aːrtahar:</ta>
            <ta e="T238" id="Seg_2450" s="T224">"Tu͡ok daː barɨta üs tögülleːk, ölörüme, "horukpun hippete" di͡en, harsɨn emi͡e muŋnahan körü͡öm", diːr. </ta>
            <ta e="T242" id="Seg_2451" s="T238">Harsɨŋŋɨtɨgar ühüs kɨːrɨːtɨn kɨːrar. </ta>
            <ta e="T247" id="Seg_2452" s="T242">Bu kɨːran Ürüŋ Ajɨː dʼi͡etiger tijer. </ta>
            <ta e="T262" id="Seg_2453" s="T247">Bu tijen toŋkojon dʼi͡etin hu͡onatɨn iččitiger aːrtahan Ürüŋ Ajɨːtɨn atagar hɨːlan kelen baː dʼiŋŋi-ti͡egeːger hürdeːktik tu͡onar-ɨtɨːr. </ta>
            <ta e="T266" id="Seg_2454" s="T262">Manɨ dʼe hüreginen ahɨnar. </ta>
            <ta e="T274" id="Seg_2455" s="T266">"Dʼe kirdik, min ejigin ölöröːrü bejem ojunum gɨmmatagɨm. </ta>
            <ta e="T279" id="Seg_2456" s="T274">Togo bagas kineːhiŋ oburgu aŋɨraj! </ta>
            <ta e="T284" id="Seg_2457" s="T279">Dʼe bejetin noru͡otugar i͡edeːni oŋordogo. </ta>
            <ta e="T290" id="Seg_2458" s="T284">Noru͡okkututtan hogotok en u͡olgun kɨtarɨ ordu͡okkut. </ta>
            <ta e="T295" id="Seg_2459" s="T290">Tu͡ok da tɨːnnaːk kaːlɨ͡a hu͡oga. </ta>
            <ta e="T304" id="Seg_2460" s="T295">Atɨn hirten dʼon kelenner, onton haŋa dʼon ü͡öskü͡öge", diːr. </ta>
            <ta e="T325" id="Seg_2461" s="T304">"Haŋa ɨraːs čeːlkeː öldüːnne anɨ üs konugunan harsɨ͡arda turu͡ordunnar, ču͡ogur čeːlkeː tabanɨ ölördünner, ol uraha tulatɨn barɨtɨn čeːlkeː taba tiriːtinen telgeːtinner. </ta>
            <ta e="T329" id="Seg_2462" s="T325">Manan üktenen kiːri͡ege kineːskit. </ta>
            <ta e="T335" id="Seg_2463" s="T329">Bɨrtak tu͡ok da hɨstɨ͡a hu͡oktaːk", diːr. </ta>
            <ta e="T346" id="Seg_2464" s="T335">"Dʼonnor kurum di͡en kelbetinner, kim tɨːnɨn karɨstaːrɨ gɨnar ɨraːk bardɨn", di͡ebit. </ta>
            <ta e="T351" id="Seg_2465" s="T346">Ühüs künüger eppitin kurduk oŋorbuttar. </ta>
            <ta e="T355" id="Seg_2466" s="T351">Manna ojun körön turbut. </ta>
            <ta e="T365" id="Seg_2467" s="T355">Kütür bagajɨ boloho tɨ͡al tüspüt, ču͡oraːn tɨ͡aha di͡en bagajɨ ihillibit. </ta>
            <ta e="T374" id="Seg_2468" s="T365">Tu͡ok ere tɨ͡al kurduk uraha aːnɨn arɨjarga dɨlɨ gɨmmɨt. </ta>
            <ta e="T379" id="Seg_2469" s="T374">Ol kennine tɨ͡ala hu͡ok bu͡olbut. </ta>
            <ta e="T392" id="Seg_2470" s="T379">Manɨ ojun baran körbüte, kineːs da barɨ kelbit da dʼon ölö hɨtallar ebit. </ta>
            <ta e="T395" id="Seg_2471" s="T392">Manan norilʼskaj dʼadajda. </ta>
            <ta e="T403" id="Seg_2472" s="T395">Iti edʼeːn tu͡orattan kelen onton ere ü͡ösküːr bu͡ollular. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_2473" s="T0">bɨlɨr</ta>
            <ta e="T2" id="Seg_2474" s="T1">Norilskaj-ga</ta>
            <ta e="T3" id="Seg_2475" s="T2">kineːs-teːk-ter</ta>
            <ta e="T4" id="Seg_2476" s="T3">ühü</ta>
            <ta e="T5" id="Seg_2477" s="T4">hiŋil</ta>
            <ta e="T6" id="Seg_2478" s="T5">kihi</ta>
            <ta e="T7" id="Seg_2479" s="T6">bu</ta>
            <ta e="T8" id="Seg_2480" s="T7">kihi-lere</ta>
            <ta e="T9" id="Seg_2481" s="T8">dʼaktar-a</ta>
            <ta e="T10" id="Seg_2482" s="T9">hu͡ok</ta>
            <ta e="T11" id="Seg_2483" s="T10">ma-nɨ</ta>
            <ta e="T12" id="Seg_2484" s="T11">dʼon-nor-o</ta>
            <ta e="T13" id="Seg_2485" s="T12">biːrde</ta>
            <ta e="T14" id="Seg_2486" s="T13">ɨjɨt-al-lar</ta>
            <ta e="T15" id="Seg_2487" s="T14">togo</ta>
            <ta e="T16" id="Seg_2488" s="T15">dʼaktar-dam-mak-kɨn</ta>
            <ta e="T17" id="Seg_2489" s="T16">di͡e-n</ta>
            <ta e="T18" id="Seg_2490" s="T17">onu͡o-ga</ta>
            <ta e="T19" id="Seg_2491" s="T18">et-er</ta>
            <ta e="T20" id="Seg_2492" s="T19">min</ta>
            <ta e="T21" id="Seg_2493" s="T20">bu</ta>
            <ta e="T22" id="Seg_2494" s="T21">dojdu</ta>
            <ta e="T23" id="Seg_2495" s="T22">kɨːh-ɨ-n</ta>
            <ta e="T24" id="Seg_2496" s="T23">ɨl-bat</ta>
            <ta e="T25" id="Seg_2497" s="T24">kihi-bin</ta>
            <ta e="T26" id="Seg_2498" s="T25">ü͡öhe</ta>
            <ta e="T27" id="Seg_2499" s="T26">Ürüŋ Ajɨː</ta>
            <ta e="T28" id="Seg_2500" s="T27">di͡e-n</ta>
            <ta e="T29" id="Seg_2501" s="T28">baːr</ta>
            <ta e="T30" id="Seg_2502" s="T29">ol</ta>
            <ta e="T31" id="Seg_2503" s="T30">kɨːh-ɨ-n</ta>
            <ta e="T32" id="Seg_2504" s="T31">araj</ta>
            <ta e="T33" id="Seg_2505" s="T32">ɨl-lak-pɨna</ta>
            <ta e="T34" id="Seg_2506" s="T33">oččogo</ta>
            <ta e="T35" id="Seg_2507" s="T34">höbül-e-h-i͡ek</ta>
            <ta e="T36" id="Seg_2508" s="T35">e-ti-m</ta>
            <ta e="T37" id="Seg_2509" s="T36">diː-r</ta>
            <ta e="T38" id="Seg_2510" s="T37">ma-nɨ</ta>
            <ta e="T39" id="Seg_2511" s="T38">dʼon-nor</ta>
            <ta e="T40" id="Seg_2512" s="T39">onnʼoː-n</ta>
            <ta e="T41" id="Seg_2513" s="T40">haŋar-dag-a</ta>
            <ta e="T42" id="Seg_2514" s="T41">di͡e-n</ta>
            <ta e="T43" id="Seg_2515" s="T42">keːh-el-ler</ta>
            <ta e="T44" id="Seg_2516" s="T43">biːrde</ta>
            <ta e="T45" id="Seg_2517" s="T44">munnʼak-ka</ta>
            <ta e="T46" id="Seg_2518" s="T45">kineːs</ta>
            <ta e="T47" id="Seg_2519" s="T46">ojun-u</ta>
            <ta e="T48" id="Seg_2520" s="T47">kaːj-ar</ta>
            <ta e="T49" id="Seg_2521" s="T48">en</ta>
            <ta e="T50" id="Seg_2522" s="T49">mi͡e-ke</ta>
            <ta e="T51" id="Seg_2523" s="T50">kallaːŋ-ŋa</ta>
            <ta e="T52" id="Seg_2524" s="T51">baːr</ta>
            <ta e="T53" id="Seg_2525" s="T52">Ürüŋ Ajɨː</ta>
            <ta e="T54" id="Seg_2526" s="T53">kɨːh-ɨ-n</ta>
            <ta e="T55" id="Seg_2527" s="T54">dʼaktar</ta>
            <ta e="T56" id="Seg_2528" s="T55">gɨn-ɨ͡ak-pɨ-n</ta>
            <ta e="T57" id="Seg_2529" s="T56">hu͡orumdʼu-laː</ta>
            <ta e="T58" id="Seg_2530" s="T57">diː-r</ta>
            <ta e="T59" id="Seg_2531" s="T58">ma-nɨ</ta>
            <ta e="T60" id="Seg_2532" s="T59">ojun-a</ta>
            <ta e="T61" id="Seg_2533" s="T60">dʼulaj-a</ta>
            <ta e="T62" id="Seg_2534" s="T61">ist-er</ta>
            <ta e="T63" id="Seg_2535" s="T62">tugu-ŋ</ta>
            <ta e="T64" id="Seg_2536" s="T63">haŋa-ta=j</ta>
            <ta e="T65" id="Seg_2537" s="T64">kajdak</ta>
            <ta e="T66" id="Seg_2538" s="T65">da</ta>
            <ta e="T67" id="Seg_2539" s="T66">ol</ta>
            <ta e="T68" id="Seg_2540" s="T67">dojdu-ttan</ta>
            <ta e="T69" id="Seg_2541" s="T68">kɨ͡aj-an</ta>
            <ta e="T70" id="Seg_2542" s="T69">egel-bet</ta>
            <ta e="T71" id="Seg_2543" s="T70">hir-i-m</ta>
            <ta e="T72" id="Seg_2544" s="T71">diː-r</ta>
            <ta e="T73" id="Seg_2545" s="T72">ma-nɨ</ta>
            <ta e="T74" id="Seg_2546" s="T73">kineːs</ta>
            <ta e="T75" id="Seg_2547" s="T74">hette</ta>
            <ta e="T76" id="Seg_2548" s="T75">talag-ɨ</ta>
            <ta e="T77" id="Seg_2549" s="T76">kör-dör-ör</ta>
            <ta e="T78" id="Seg_2550" s="T77">en</ta>
            <ta e="T79" id="Seg_2551" s="T78">albɨn-nan-ɨ-ma</ta>
            <ta e="T80" id="Seg_2552" s="T79">min</ta>
            <ta e="T81" id="Seg_2553" s="T80">ejigi-n</ta>
            <ta e="T82" id="Seg_2554" s="T81">i͡eŋ-ŋi-n</ta>
            <ta e="T83" id="Seg_2555" s="T82">kast-ɨ͡a-m</ta>
            <ta e="T84" id="Seg_2556" s="T83">kün</ta>
            <ta e="T85" id="Seg_2557" s="T84">harsɨn</ta>
            <ta e="T86" id="Seg_2558" s="T85">Ürüŋ Ajɨː</ta>
            <ta e="T87" id="Seg_2559" s="T86">kɨːh-ɨ-n</ta>
            <ta e="T88" id="Seg_2560" s="T87">baːr</ta>
            <ta e="T89" id="Seg_2561" s="T88">gɨn</ta>
            <ta e="T90" id="Seg_2562" s="T89">diː-r</ta>
            <ta e="T91" id="Seg_2563" s="T90">ojun</ta>
            <ta e="T92" id="Seg_2564" s="T91">bɨst-ɨ͡a</ta>
            <ta e="T93" id="Seg_2565" s="T92">duː</ta>
            <ta e="T94" id="Seg_2566" s="T93">dʼe</ta>
            <ta e="T95" id="Seg_2567" s="T94">kɨːr-an</ta>
            <ta e="T96" id="Seg_2568" s="T95">bar-ar</ta>
            <ta e="T97" id="Seg_2569" s="T96">bu</ta>
            <ta e="T98" id="Seg_2570" s="T97">ojun</ta>
            <ta e="T99" id="Seg_2571" s="T98">čugahaː-bɨt-ɨ-n</ta>
            <ta e="T100" id="Seg_2572" s="T99">ist-en</ta>
            <ta e="T101" id="Seg_2573" s="T100">Ürün Ajɨː</ta>
            <ta e="T102" id="Seg_2574" s="T101">tojon</ta>
            <ta e="T103" id="Seg_2575" s="T102">hurdeːk-tik</ta>
            <ta e="T104" id="Seg_2576" s="T103">u͡oktan-ar</ta>
            <ta e="T105" id="Seg_2577" s="T104">tu͡ok</ta>
            <ta e="T106" id="Seg_2578" s="T105">öl-ör</ta>
            <ta e="T107" id="Seg_2579" s="T106">ölüː</ta>
            <ta e="T108" id="Seg_2580" s="T107">ojun-a</ta>
            <ta e="T109" id="Seg_2581" s="T108">ɨjaːg-a</ta>
            <ta e="T110" id="Seg_2582" s="T109">hu͡ok</ta>
            <ta e="T111" id="Seg_2583" s="T110">kel-leg-e=j</ta>
            <ta e="T112" id="Seg_2584" s="T111">egel-i-ŋ</ta>
            <ta e="T113" id="Seg_2585" s="T112">mi͡e-ke</ta>
            <ta e="T114" id="Seg_2586" s="T113">u͡ot</ta>
            <ta e="T115" id="Seg_2587" s="T114">bolop-pu-n</ta>
            <ta e="T116" id="Seg_2588" s="T115">bah-ɨ</ta>
            <ta e="T117" id="Seg_2589" s="T116">bɨh-a</ta>
            <ta e="T118" id="Seg_2590" s="T117">oks-u͡ok-pu-n</ta>
            <ta e="T119" id="Seg_2591" s="T118">di͡e-bit</ta>
            <ta e="T120" id="Seg_2592" s="T119">ojun-a</ta>
            <ta e="T121" id="Seg_2593" s="T120">kel-bit-e</ta>
            <ta e="T122" id="Seg_2594" s="T121">horun-an</ta>
            <ta e="T123" id="Seg_2595" s="T122">olor-or</ta>
            <ta e="T124" id="Seg_2596" s="T123">e-bit</ta>
            <ta e="T125" id="Seg_2597" s="T124">Ajɨː-ta</ta>
            <ta e="T126" id="Seg_2598" s="T125">ma-nɨ</ta>
            <ta e="T127" id="Seg_2599" s="T126">dʼe</ta>
            <ta e="T128" id="Seg_2600" s="T127">ün-en</ta>
            <ta e="T129" id="Seg_2601" s="T128">körd-ö-h-ör</ta>
            <ta e="T130" id="Seg_2602" s="T129">min</ta>
            <ta e="T131" id="Seg_2603" s="T130">kɨhalga-ttan</ta>
            <ta e="T132" id="Seg_2604" s="T131">kel-li-m</ta>
            <ta e="T133" id="Seg_2605" s="T132">en</ta>
            <ta e="T134" id="Seg_2606" s="T133">kurduk</ta>
            <ta e="T135" id="Seg_2607" s="T134">hir-biti-ger</ta>
            <ta e="T136" id="Seg_2608" s="T135">bahɨlɨk-taːk</ta>
            <ta e="T137" id="Seg_2609" s="T136">bu͡ol-a-bɨt</ta>
            <ta e="T138" id="Seg_2610" s="T137">kineːs</ta>
            <ta e="T139" id="Seg_2611" s="T138">di͡e-n</ta>
            <ta e="T140" id="Seg_2612" s="T139">ol</ta>
            <ta e="T141" id="Seg_2613" s="T140">kaːj-ɨː-tɨ-ttan</ta>
            <ta e="T142" id="Seg_2614" s="T141">kel-li-m</ta>
            <ta e="T143" id="Seg_2615" s="T142">di͡e-n</ta>
            <ta e="T144" id="Seg_2616" s="T143">tu͡on-an</ta>
            <ta e="T145" id="Seg_2617" s="T144">naːda-tɨ-n</ta>
            <ta e="T146" id="Seg_2618" s="T145">kepsiː-r</ta>
            <ta e="T147" id="Seg_2619" s="T146">onu͡o-ga</ta>
            <ta e="T148" id="Seg_2620" s="T147">Ürüŋ Ajɨː-ta</ta>
            <ta e="T149" id="Seg_2621" s="T148">et-er</ta>
            <ta e="T150" id="Seg_2622" s="T149">hu͡ok</ta>
            <ta e="T151" id="Seg_2623" s="T150">baran</ta>
            <ta e="T152" id="Seg_2624" s="T151">et</ta>
            <ta e="T153" id="Seg_2625" s="T152">di͡e-n</ta>
            <ta e="T154" id="Seg_2626" s="T153">tönn-ö-r-ör</ta>
            <ta e="T155" id="Seg_2627" s="T154">ojun-u-n</ta>
            <ta e="T156" id="Seg_2628" s="T155">man-ta</ta>
            <ta e="T157" id="Seg_2629" s="T156">kɨːr-an</ta>
            <ta e="T158" id="Seg_2630" s="T157">büp-püt-ü-ger</ta>
            <ta e="T159" id="Seg_2631" s="T158">kineːh-e</ta>
            <ta e="T160" id="Seg_2632" s="T159">kaːj-ar</ta>
            <ta e="T161" id="Seg_2633" s="T160">en</ta>
            <ta e="T162" id="Seg_2634" s="T161">tü͡ökünün-en</ta>
            <ta e="T163" id="Seg_2635" s="T162">onno-manna</ta>
            <ta e="T164" id="Seg_2636" s="T163">bar-bɨt</ta>
            <ta e="T165" id="Seg_2637" s="T164">bu͡ol-a-gɨn</ta>
            <ta e="T166" id="Seg_2638" s="T165">tutuːr</ta>
            <ta e="T167" id="Seg_2639" s="T166">kɨrsa-ŋ</ta>
            <ta e="T168" id="Seg_2640" s="T167">manna</ta>
            <ta e="T169" id="Seg_2641" s="T168">tur-ar</ta>
            <ta e="T170" id="Seg_2642" s="T169">bu͡ol-bat</ta>
            <ta e="T171" id="Seg_2643" s="T170">du͡o</ta>
            <ta e="T172" id="Seg_2644" s="T171">di͡e-n</ta>
            <ta e="T173" id="Seg_2645" s="T172">kaːj-ar</ta>
            <ta e="T174" id="Seg_2646" s="T173">bulgu</ta>
            <ta e="T175" id="Seg_2647" s="T174">mi͡e-ke</ta>
            <ta e="T176" id="Seg_2648" s="T175">kɨːs-pɨ-n</ta>
            <ta e="T177" id="Seg_2649" s="T176">egel-en</ta>
            <ta e="T178" id="Seg_2650" s="T177">bi͡er</ta>
            <ta e="T179" id="Seg_2651" s="T178">diː-r</ta>
            <ta e="T180" id="Seg_2652" s="T179">dʼe</ta>
            <ta e="T181" id="Seg_2653" s="T180">emi͡e</ta>
            <ta e="T182" id="Seg_2654" s="T181">baː</ta>
            <ta e="T183" id="Seg_2655" s="T182">ojun</ta>
            <ta e="T184" id="Seg_2656" s="T183">harsɨ͡arda</ta>
            <ta e="T185" id="Seg_2657" s="T184">kɨːr-an</ta>
            <ta e="T186" id="Seg_2658" s="T185">muŋ-nan-n-ar</ta>
            <ta e="T187" id="Seg_2659" s="T186">bu</ta>
            <ta e="T188" id="Seg_2660" s="T187">kɨːr-an</ta>
            <ta e="T189" id="Seg_2661" s="T188">tij-bit-i-ger</ta>
            <ta e="T190" id="Seg_2662" s="T189">tu͡ok-tan</ta>
            <ta e="T191" id="Seg_2663" s="T190">daː</ta>
            <ta e="T192" id="Seg_2664" s="T191">hür-den-en</ta>
            <ta e="T193" id="Seg_2665" s="T192">Ürüŋ Ajɨː-ta</ta>
            <ta e="T194" id="Seg_2666" s="T193">ojun-u-n</ta>
            <ta e="T195" id="Seg_2667" s="T194">abalɨː-r</ta>
            <ta e="T196" id="Seg_2668" s="T195">manɨ͡a-ka</ta>
            <ta e="T197" id="Seg_2669" s="T196">emi͡e</ta>
            <ta e="T198" id="Seg_2670" s="T197">ojun-a</ta>
            <ta e="T199" id="Seg_2671" s="T198">ɨt-a-n-an</ta>
            <ta e="T200" id="Seg_2672" s="T199">kebilen-er</ta>
            <ta e="T201" id="Seg_2673" s="T200">haːtar</ta>
            <ta e="T202" id="Seg_2674" s="T201">beli͡e-te</ta>
            <ta e="T203" id="Seg_2675" s="T202">bi͡er</ta>
            <ta e="T204" id="Seg_2676" s="T203">di͡e-n</ta>
            <ta e="T205" id="Seg_2677" s="T204">hu͡ok</ta>
            <ta e="T206" id="Seg_2678" s="T205">bi͡er-i͡e-m</ta>
            <ta e="T207" id="Seg_2679" s="T206">hu͡og-a</ta>
            <ta e="T208" id="Seg_2680" s="T207">togo</ta>
            <ta e="T209" id="Seg_2681" s="T208">aŋɨr=aj</ta>
            <ta e="T210" id="Seg_2682" s="T209">tu͡ora</ta>
            <ta e="T211" id="Seg_2683" s="T210">karak-taːk</ta>
            <ta e="T212" id="Seg_2684" s="T211">ajɨlaːgɨn</ta>
            <ta e="T213" id="Seg_2685" s="T212">en</ta>
            <ta e="T214" id="Seg_2686" s="T213">öl-lög-ü-ŋ</ta>
            <ta e="T215" id="Seg_2687" s="T214">di͡e-n</ta>
            <ta e="T216" id="Seg_2688" s="T215">ojun-u-n</ta>
            <ta e="T217" id="Seg_2689" s="T216">emi͡e</ta>
            <ta e="T218" id="Seg_2690" s="T217">tönn-ö-r-ör</ta>
            <ta e="T219" id="Seg_2691" s="T218">kɨːr-an</ta>
            <ta e="T220" id="Seg_2692" s="T219">büt-en</ta>
            <ta e="T221" id="Seg_2693" s="T220">baran</ta>
            <ta e="T222" id="Seg_2694" s="T221">ojun</ta>
            <ta e="T223" id="Seg_2695" s="T222">kineːs-ten</ta>
            <ta e="T224" id="Seg_2696" s="T223">aːrtah-ar</ta>
            <ta e="T225" id="Seg_2697" s="T224">tu͡ok</ta>
            <ta e="T226" id="Seg_2698" s="T225">daː</ta>
            <ta e="T227" id="Seg_2699" s="T226">barɨta</ta>
            <ta e="T228" id="Seg_2700" s="T227">üs</ta>
            <ta e="T229" id="Seg_2701" s="T228">tögül-leːk</ta>
            <ta e="T230" id="Seg_2702" s="T229">ölör-ü-me</ta>
            <ta e="T231" id="Seg_2703" s="T230">horuk-pu-n</ta>
            <ta e="T232" id="Seg_2704" s="T231">hip-pe-t-e</ta>
            <ta e="T233" id="Seg_2705" s="T232">di͡e-n</ta>
            <ta e="T234" id="Seg_2706" s="T233">harsɨn</ta>
            <ta e="T235" id="Seg_2707" s="T234">emi͡e</ta>
            <ta e="T236" id="Seg_2708" s="T235">muŋn-a-h-an</ta>
            <ta e="T237" id="Seg_2709" s="T236">kör-ü͡ö-m</ta>
            <ta e="T238" id="Seg_2710" s="T237">diː-r</ta>
            <ta e="T239" id="Seg_2711" s="T238">harsɨŋŋɨ-tɨ-gar</ta>
            <ta e="T240" id="Seg_2712" s="T239">üh-üs</ta>
            <ta e="T241" id="Seg_2713" s="T240">kɨːr-ɨː-tɨ-n</ta>
            <ta e="T242" id="Seg_2714" s="T241">kɨːr-ar</ta>
            <ta e="T243" id="Seg_2715" s="T242">bu</ta>
            <ta e="T244" id="Seg_2716" s="T243">kɨːr-an</ta>
            <ta e="T245" id="Seg_2717" s="T244">Ürüŋ Ajɨː</ta>
            <ta e="T246" id="Seg_2718" s="T245">dʼi͡e-ti-ger</ta>
            <ta e="T247" id="Seg_2719" s="T246">tij-er</ta>
            <ta e="T248" id="Seg_2720" s="T247">bu</ta>
            <ta e="T249" id="Seg_2721" s="T248">tij-en</ta>
            <ta e="T250" id="Seg_2722" s="T249">toŋkoj-on</ta>
            <ta e="T251" id="Seg_2723" s="T250">dʼi͡e-ti-n</ta>
            <ta e="T252" id="Seg_2724" s="T251">hu͡ona-tɨ-n</ta>
            <ta e="T253" id="Seg_2725" s="T252">ičči-ti-ger</ta>
            <ta e="T254" id="Seg_2726" s="T253">aːrtah-an</ta>
            <ta e="T255" id="Seg_2727" s="T254">Ürüŋ Ajɨː-tɨ-n</ta>
            <ta e="T256" id="Seg_2728" s="T255">atag-a-r</ta>
            <ta e="T257" id="Seg_2729" s="T256">hɨːl-an</ta>
            <ta e="T258" id="Seg_2730" s="T257">kel-en</ta>
            <ta e="T259" id="Seg_2731" s="T258">baː</ta>
            <ta e="T260" id="Seg_2732" s="T259">dʼiŋŋi-ti͡e-geːger</ta>
            <ta e="T261" id="Seg_2733" s="T260">hürdeːk-tik</ta>
            <ta e="T262" id="Seg_2734" s="T261">tu͡on-ar-ɨtɨː-r</ta>
            <ta e="T263" id="Seg_2735" s="T262">ma-nɨ</ta>
            <ta e="T264" id="Seg_2736" s="T263">dʼe</ta>
            <ta e="T265" id="Seg_2737" s="T264">hüreg-i-nen</ta>
            <ta e="T266" id="Seg_2738" s="T265">ahɨn-ar</ta>
            <ta e="T267" id="Seg_2739" s="T266">dʼe</ta>
            <ta e="T268" id="Seg_2740" s="T267">kirdik</ta>
            <ta e="T269" id="Seg_2741" s="T268">min</ta>
            <ta e="T270" id="Seg_2742" s="T269">ejigi-n</ta>
            <ta e="T271" id="Seg_2743" s="T270">ölör-öːrü</ta>
            <ta e="T272" id="Seg_2744" s="T271">beje-m</ta>
            <ta e="T273" id="Seg_2745" s="T272">ojun-u-m</ta>
            <ta e="T274" id="Seg_2746" s="T273">gɨm-matag-ɨ-m</ta>
            <ta e="T275" id="Seg_2747" s="T274">togo</ta>
            <ta e="T276" id="Seg_2748" s="T275">bagas</ta>
            <ta e="T277" id="Seg_2749" s="T276">kineːh-i-ŋ</ta>
            <ta e="T278" id="Seg_2750" s="T277">oburgu</ta>
            <ta e="T279" id="Seg_2751" s="T278">aŋɨr=aj</ta>
            <ta e="T280" id="Seg_2752" s="T279">dʼe</ta>
            <ta e="T281" id="Seg_2753" s="T280">beje-ti-n</ta>
            <ta e="T282" id="Seg_2754" s="T281">noru͡ot-u-gar</ta>
            <ta e="T283" id="Seg_2755" s="T282">i͡edeːn-i</ta>
            <ta e="T284" id="Seg_2756" s="T283">oŋor-dog-o</ta>
            <ta e="T285" id="Seg_2757" s="T284">noru͡ok-kutu-ttan</ta>
            <ta e="T286" id="Seg_2758" s="T285">hogotok</ta>
            <ta e="T287" id="Seg_2759" s="T286">en</ta>
            <ta e="T288" id="Seg_2760" s="T287">u͡ol-gu-n</ta>
            <ta e="T289" id="Seg_2761" s="T288">kɨtarɨ</ta>
            <ta e="T290" id="Seg_2762" s="T289">ord-u͡ok-kut</ta>
            <ta e="T291" id="Seg_2763" s="T290">tu͡ok</ta>
            <ta e="T292" id="Seg_2764" s="T291">da</ta>
            <ta e="T293" id="Seg_2765" s="T292">tɨːnnaːk</ta>
            <ta e="T294" id="Seg_2766" s="T293">kaːl-ɨ͡a</ta>
            <ta e="T295" id="Seg_2767" s="T294">hu͡og-a</ta>
            <ta e="T296" id="Seg_2768" s="T295">atɨn</ta>
            <ta e="T297" id="Seg_2769" s="T296">hir-ten</ta>
            <ta e="T298" id="Seg_2770" s="T297">dʼon</ta>
            <ta e="T299" id="Seg_2771" s="T298">kel-en-ner</ta>
            <ta e="T300" id="Seg_2772" s="T299">on-ton</ta>
            <ta e="T301" id="Seg_2773" s="T300">haŋa</ta>
            <ta e="T302" id="Seg_2774" s="T301">dʼon</ta>
            <ta e="T303" id="Seg_2775" s="T302">ü͡ösk-ü͡ög-e</ta>
            <ta e="T304" id="Seg_2776" s="T303">diː-r</ta>
            <ta e="T305" id="Seg_2777" s="T304">haŋa</ta>
            <ta e="T306" id="Seg_2778" s="T305">ɨraːs</ta>
            <ta e="T307" id="Seg_2779" s="T306">čeːlkeː</ta>
            <ta e="T308" id="Seg_2780" s="T307">öldüːn-ne</ta>
            <ta e="T309" id="Seg_2781" s="T308">anɨ</ta>
            <ta e="T310" id="Seg_2782" s="T309">üs</ta>
            <ta e="T311" id="Seg_2783" s="T310">konug-u-nan</ta>
            <ta e="T312" id="Seg_2784" s="T311">harsɨ͡arda</ta>
            <ta e="T313" id="Seg_2785" s="T312">turu͡or-dunnar</ta>
            <ta e="T314" id="Seg_2786" s="T313">ču͡ogur</ta>
            <ta e="T315" id="Seg_2787" s="T314">čeːlkeː</ta>
            <ta e="T316" id="Seg_2788" s="T315">taba-nɨ</ta>
            <ta e="T317" id="Seg_2789" s="T316">ölör-dünner</ta>
            <ta e="T318" id="Seg_2790" s="T317">ol</ta>
            <ta e="T319" id="Seg_2791" s="T318">uraha</ta>
            <ta e="T320" id="Seg_2792" s="T319">tula-tɨ-n</ta>
            <ta e="T321" id="Seg_2793" s="T320">barɨ-tɨ-n</ta>
            <ta e="T322" id="Seg_2794" s="T321">čeːlkeː</ta>
            <ta e="T323" id="Seg_2795" s="T322">taba</ta>
            <ta e="T324" id="Seg_2796" s="T323">tiriː-ti-nen</ta>
            <ta e="T325" id="Seg_2797" s="T324">telgeː-tinner</ta>
            <ta e="T326" id="Seg_2798" s="T325">ma-nan</ta>
            <ta e="T327" id="Seg_2799" s="T326">ükten-en</ta>
            <ta e="T328" id="Seg_2800" s="T327">kiːr-i͡eg-e</ta>
            <ta e="T329" id="Seg_2801" s="T328">kineːs-kit</ta>
            <ta e="T330" id="Seg_2802" s="T329">bɨrtak</ta>
            <ta e="T331" id="Seg_2803" s="T330">tu͡ok</ta>
            <ta e="T332" id="Seg_2804" s="T331">da</ta>
            <ta e="T333" id="Seg_2805" s="T332">hɨst-ɨ͡a</ta>
            <ta e="T334" id="Seg_2806" s="T333">hu͡ok-taːk</ta>
            <ta e="T335" id="Seg_2807" s="T334">diː-r</ta>
            <ta e="T336" id="Seg_2808" s="T335">dʼon-nor</ta>
            <ta e="T337" id="Seg_2809" s="T336">kurum</ta>
            <ta e="T338" id="Seg_2810" s="T337">di͡e-n</ta>
            <ta e="T339" id="Seg_2811" s="T338">kel-be-tinner</ta>
            <ta e="T340" id="Seg_2812" s="T339">kim</ta>
            <ta e="T341" id="Seg_2813" s="T340">tɨːn-ɨ-n</ta>
            <ta e="T342" id="Seg_2814" s="T341">karɨst-aːrɨ</ta>
            <ta e="T343" id="Seg_2815" s="T342">gɨn-ar</ta>
            <ta e="T344" id="Seg_2816" s="T343">ɨraːk</ta>
            <ta e="T345" id="Seg_2817" s="T344">bar-dɨn</ta>
            <ta e="T346" id="Seg_2818" s="T345">di͡e-bit</ta>
            <ta e="T347" id="Seg_2819" s="T346">üh-üs</ta>
            <ta e="T348" id="Seg_2820" s="T347">kün-ü-ger</ta>
            <ta e="T349" id="Seg_2821" s="T348">ep-pit-i-n</ta>
            <ta e="T350" id="Seg_2822" s="T349">kurduk</ta>
            <ta e="T351" id="Seg_2823" s="T350">oŋor-but-tar</ta>
            <ta e="T352" id="Seg_2824" s="T351">manna</ta>
            <ta e="T353" id="Seg_2825" s="T352">ojun</ta>
            <ta e="T354" id="Seg_2826" s="T353">kör-ön</ta>
            <ta e="T355" id="Seg_2827" s="T354">tur-but</ta>
            <ta e="T356" id="Seg_2828" s="T355">kütür</ta>
            <ta e="T357" id="Seg_2829" s="T356">bagajɨ</ta>
            <ta e="T358" id="Seg_2830" s="T357">boloho</ta>
            <ta e="T359" id="Seg_2831" s="T358">tɨ͡al</ta>
            <ta e="T360" id="Seg_2832" s="T359">tüs-püt</ta>
            <ta e="T361" id="Seg_2833" s="T360">ču͡oraːn</ta>
            <ta e="T362" id="Seg_2834" s="T361">tɨ͡ah-a</ta>
            <ta e="T363" id="Seg_2835" s="T362">di͡e-n</ta>
            <ta e="T364" id="Seg_2836" s="T363">bagajɨ</ta>
            <ta e="T365" id="Seg_2837" s="T364">ihill-i-bit</ta>
            <ta e="T366" id="Seg_2838" s="T365">tu͡ok</ta>
            <ta e="T367" id="Seg_2839" s="T366">ere</ta>
            <ta e="T368" id="Seg_2840" s="T367">tɨ͡al</ta>
            <ta e="T369" id="Seg_2841" s="T368">kurduk</ta>
            <ta e="T370" id="Seg_2842" s="T369">uraha</ta>
            <ta e="T371" id="Seg_2843" s="T370">aːn-ɨ-n</ta>
            <ta e="T372" id="Seg_2844" s="T371">arɨj-ar-ga</ta>
            <ta e="T373" id="Seg_2845" s="T372">dɨlɨ</ta>
            <ta e="T374" id="Seg_2846" s="T373">gɨm-mɨt</ta>
            <ta e="T375" id="Seg_2847" s="T374">ol</ta>
            <ta e="T376" id="Seg_2848" s="T375">kennine</ta>
            <ta e="T377" id="Seg_2849" s="T376">tɨ͡al-a</ta>
            <ta e="T378" id="Seg_2850" s="T377">hu͡ok</ta>
            <ta e="T379" id="Seg_2851" s="T378">bu͡ol-but</ta>
            <ta e="T380" id="Seg_2852" s="T379">ma-nɨ</ta>
            <ta e="T381" id="Seg_2853" s="T380">ojun</ta>
            <ta e="T382" id="Seg_2854" s="T381">bar-an</ta>
            <ta e="T383" id="Seg_2855" s="T382">kör-büt-e</ta>
            <ta e="T384" id="Seg_2856" s="T383">kineːs</ta>
            <ta e="T385" id="Seg_2857" s="T384">da</ta>
            <ta e="T386" id="Seg_2858" s="T385">barɨ</ta>
            <ta e="T387" id="Seg_2859" s="T386">kel-bit</ta>
            <ta e="T388" id="Seg_2860" s="T387">da</ta>
            <ta e="T389" id="Seg_2861" s="T388">dʼon</ta>
            <ta e="T390" id="Seg_2862" s="T389">öl-ö</ta>
            <ta e="T391" id="Seg_2863" s="T390">hɨt-al-lar</ta>
            <ta e="T392" id="Seg_2864" s="T391">e-bit</ta>
            <ta e="T393" id="Seg_2865" s="T392">ma-nan</ta>
            <ta e="T394" id="Seg_2866" s="T393">norilʼskaj</ta>
            <ta e="T395" id="Seg_2867" s="T394">dʼadaj-d-a</ta>
            <ta e="T396" id="Seg_2868" s="T395">iti</ta>
            <ta e="T397" id="Seg_2869" s="T396">edʼeːn</ta>
            <ta e="T398" id="Seg_2870" s="T397">tu͡ora-ttan</ta>
            <ta e="T399" id="Seg_2871" s="T398">kel-en</ta>
            <ta e="T400" id="Seg_2872" s="T399">onton</ta>
            <ta e="T401" id="Seg_2873" s="T400">ere</ta>
            <ta e="T402" id="Seg_2874" s="T401">ü͡ösküː-r</ta>
            <ta e="T403" id="Seg_2875" s="T402">bu͡ol-lu-lar</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_2876" s="T0">bɨlɨr</ta>
            <ta e="T2" id="Seg_2877" s="T1">Norilskaj-GA</ta>
            <ta e="T3" id="Seg_2878" s="T2">kineːs-LAːK-LAr</ta>
            <ta e="T4" id="Seg_2879" s="T3">ühü</ta>
            <ta e="T5" id="Seg_2880" s="T4">hiŋil</ta>
            <ta e="T6" id="Seg_2881" s="T5">kihi</ta>
            <ta e="T7" id="Seg_2882" s="T6">bu</ta>
            <ta e="T8" id="Seg_2883" s="T7">kihi-LArA</ta>
            <ta e="T9" id="Seg_2884" s="T8">dʼaktar-tA</ta>
            <ta e="T10" id="Seg_2885" s="T9">hu͡ok</ta>
            <ta e="T11" id="Seg_2886" s="T10">bu-nI</ta>
            <ta e="T12" id="Seg_2887" s="T11">dʼon-LAr-tA</ta>
            <ta e="T13" id="Seg_2888" s="T12">biːrde</ta>
            <ta e="T14" id="Seg_2889" s="T13">ɨjɨt-Ar-LAr</ta>
            <ta e="T15" id="Seg_2890" s="T14">togo</ta>
            <ta e="T16" id="Seg_2891" s="T15">dʼaktar-LAN-BAT-GIn</ta>
            <ta e="T17" id="Seg_2892" s="T16">di͡e-An</ta>
            <ta e="T18" id="Seg_2893" s="T17">ol-GA</ta>
            <ta e="T19" id="Seg_2894" s="T18">et-Ar</ta>
            <ta e="T20" id="Seg_2895" s="T19">min</ta>
            <ta e="T21" id="Seg_2896" s="T20">bu</ta>
            <ta e="T22" id="Seg_2897" s="T21">dojdu</ta>
            <ta e="T23" id="Seg_2898" s="T22">kɨːs-tI-n</ta>
            <ta e="T24" id="Seg_2899" s="T23">ɨl-BAT</ta>
            <ta e="T25" id="Seg_2900" s="T24">kihi-BIn</ta>
            <ta e="T26" id="Seg_2901" s="T25">ü͡öhe</ta>
            <ta e="T27" id="Seg_2902" s="T26">Ürüŋ Ajɨː</ta>
            <ta e="T28" id="Seg_2903" s="T27">di͡e-An</ta>
            <ta e="T29" id="Seg_2904" s="T28">baːr</ta>
            <ta e="T30" id="Seg_2905" s="T29">ol</ta>
            <ta e="T31" id="Seg_2906" s="T30">kɨːs-tI-n</ta>
            <ta e="T32" id="Seg_2907" s="T31">agaj</ta>
            <ta e="T33" id="Seg_2908" s="T32">ɨl-TAK-BInA</ta>
            <ta e="T34" id="Seg_2909" s="T33">oččogo</ta>
            <ta e="T35" id="Seg_2910" s="T34">höbüleː-A-s-IAK</ta>
            <ta e="T36" id="Seg_2911" s="T35">e-TI-m</ta>
            <ta e="T37" id="Seg_2912" s="T36">di͡e-Ar</ta>
            <ta e="T38" id="Seg_2913" s="T37">bu-nI</ta>
            <ta e="T39" id="Seg_2914" s="T38">dʼon-LAr</ta>
            <ta e="T40" id="Seg_2915" s="T39">oːnnʼoː-An</ta>
            <ta e="T41" id="Seg_2916" s="T40">haŋar-TAK-tA</ta>
            <ta e="T42" id="Seg_2917" s="T41">di͡e-An</ta>
            <ta e="T43" id="Seg_2918" s="T42">keːs-Ar-LAr</ta>
            <ta e="T44" id="Seg_2919" s="T43">biːrde</ta>
            <ta e="T45" id="Seg_2920" s="T44">munnʼak-GA</ta>
            <ta e="T46" id="Seg_2921" s="T45">kineːs</ta>
            <ta e="T47" id="Seg_2922" s="T46">ojun-nI</ta>
            <ta e="T48" id="Seg_2923" s="T47">kaːj-Ar</ta>
            <ta e="T49" id="Seg_2924" s="T48">en</ta>
            <ta e="T50" id="Seg_2925" s="T49">min-GA</ta>
            <ta e="T51" id="Seg_2926" s="T50">kallaːn-GA</ta>
            <ta e="T52" id="Seg_2927" s="T51">baːr</ta>
            <ta e="T53" id="Seg_2928" s="T52">Ürüŋ Ajɨː</ta>
            <ta e="T54" id="Seg_2929" s="T53">kɨːs-tI-n</ta>
            <ta e="T55" id="Seg_2930" s="T54">dʼaktar</ta>
            <ta e="T56" id="Seg_2931" s="T55">gɨn-IAK-BI-n</ta>
            <ta e="T57" id="Seg_2932" s="T56">hu͡orumnʼu-LAː</ta>
            <ta e="T58" id="Seg_2933" s="T57">di͡e-Ar</ta>
            <ta e="T59" id="Seg_2934" s="T58">bu-nI</ta>
            <ta e="T60" id="Seg_2935" s="T59">ojun-tA</ta>
            <ta e="T61" id="Seg_2936" s="T60">dʼulaj-A</ta>
            <ta e="T62" id="Seg_2937" s="T61">ihit-Ar</ta>
            <ta e="T63" id="Seg_2938" s="T62">tu͡ok-ŋ</ta>
            <ta e="T64" id="Seg_2939" s="T63">haŋa-tA=Ij</ta>
            <ta e="T65" id="Seg_2940" s="T64">kajdak</ta>
            <ta e="T66" id="Seg_2941" s="T65">da</ta>
            <ta e="T67" id="Seg_2942" s="T66">ol</ta>
            <ta e="T68" id="Seg_2943" s="T67">dojdu-ttAn</ta>
            <ta e="T69" id="Seg_2944" s="T68">kɨ͡aj-An</ta>
            <ta e="T70" id="Seg_2945" s="T69">egel-BAT</ta>
            <ta e="T71" id="Seg_2946" s="T70">hir-I-m</ta>
            <ta e="T72" id="Seg_2947" s="T71">di͡e-Ar</ta>
            <ta e="T73" id="Seg_2948" s="T72">bu-nI</ta>
            <ta e="T74" id="Seg_2949" s="T73">kineːs</ta>
            <ta e="T75" id="Seg_2950" s="T74">hette</ta>
            <ta e="T76" id="Seg_2951" s="T75">talak-nI</ta>
            <ta e="T77" id="Seg_2952" s="T76">kör-TAr-Ar</ta>
            <ta e="T78" id="Seg_2953" s="T77">en</ta>
            <ta e="T79" id="Seg_2954" s="T78">albun-LAN-I-m</ta>
            <ta e="T80" id="Seg_2955" s="T79">min</ta>
            <ta e="T81" id="Seg_2956" s="T80">en-n</ta>
            <ta e="T82" id="Seg_2957" s="T81">i͡en-GI-n</ta>
            <ta e="T83" id="Seg_2958" s="T82">kastaː-IAK-m</ta>
            <ta e="T84" id="Seg_2959" s="T83">kün</ta>
            <ta e="T85" id="Seg_2960" s="T84">harsɨn</ta>
            <ta e="T86" id="Seg_2961" s="T85">Ürüŋ Ajɨː</ta>
            <ta e="T87" id="Seg_2962" s="T86">kɨːs-tI-n</ta>
            <ta e="T88" id="Seg_2963" s="T87">baːr</ta>
            <ta e="T89" id="Seg_2964" s="T88">gɨn</ta>
            <ta e="T90" id="Seg_2965" s="T89">di͡e-Ar</ta>
            <ta e="T91" id="Seg_2966" s="T90">ojun</ta>
            <ta e="T92" id="Seg_2967" s="T91">bɨhɨn-IAK.[tA]</ta>
            <ta e="T93" id="Seg_2968" s="T92">du͡o</ta>
            <ta e="T94" id="Seg_2969" s="T93">dʼe</ta>
            <ta e="T95" id="Seg_2970" s="T94">kɨːr-An</ta>
            <ta e="T96" id="Seg_2971" s="T95">bar-Ar</ta>
            <ta e="T97" id="Seg_2972" s="T96">bu</ta>
            <ta e="T98" id="Seg_2973" s="T97">ojun</ta>
            <ta e="T99" id="Seg_2974" s="T98">čugahaː-BIT-tI-n</ta>
            <ta e="T100" id="Seg_2975" s="T99">ihit-An</ta>
            <ta e="T101" id="Seg_2976" s="T100">Ürüŋ Ajɨː</ta>
            <ta e="T102" id="Seg_2977" s="T101">tojon</ta>
            <ta e="T103" id="Seg_2978" s="T102">hürdeːk-LIk</ta>
            <ta e="T104" id="Seg_2979" s="T103">u͡oktan-Ar</ta>
            <ta e="T105" id="Seg_2980" s="T104">tu͡ok</ta>
            <ta e="T106" id="Seg_2981" s="T105">öl-Ar</ta>
            <ta e="T107" id="Seg_2982" s="T106">ölüː</ta>
            <ta e="T108" id="Seg_2983" s="T107">ojun-tA</ta>
            <ta e="T109" id="Seg_2984" s="T108">ɨjaːk-tA</ta>
            <ta e="T110" id="Seg_2985" s="T109">hu͡ok</ta>
            <ta e="T111" id="Seg_2986" s="T110">kel-TAK-tA=Ij</ta>
            <ta e="T112" id="Seg_2987" s="T111">egel-I-ŋ</ta>
            <ta e="T113" id="Seg_2988" s="T112">min-GA</ta>
            <ta e="T114" id="Seg_2989" s="T113">u͡ot</ta>
            <ta e="T115" id="Seg_2990" s="T114">bolot-BI-n</ta>
            <ta e="T116" id="Seg_2991" s="T115">bas-nI</ta>
            <ta e="T117" id="Seg_2992" s="T116">bɨs-A</ta>
            <ta e="T118" id="Seg_2993" s="T117">ogus-IAK-BI-n</ta>
            <ta e="T119" id="Seg_2994" s="T118">di͡e-BIT</ta>
            <ta e="T120" id="Seg_2995" s="T119">ojun-tA</ta>
            <ta e="T121" id="Seg_2996" s="T120">kel-BIT-tA</ta>
            <ta e="T122" id="Seg_2997" s="T121">horun-An</ta>
            <ta e="T123" id="Seg_2998" s="T122">olor-Ar</ta>
            <ta e="T124" id="Seg_2999" s="T123">e-BIT</ta>
            <ta e="T125" id="Seg_3000" s="T124">ajɨː-tA</ta>
            <ta e="T126" id="Seg_3001" s="T125">bu-nI</ta>
            <ta e="T127" id="Seg_3002" s="T126">dʼe</ta>
            <ta e="T128" id="Seg_3003" s="T127">ün-An</ta>
            <ta e="T129" id="Seg_3004" s="T128">kördöː-A-s-Ar</ta>
            <ta e="T130" id="Seg_3005" s="T129">min</ta>
            <ta e="T131" id="Seg_3006" s="T130">kɨhalga-ttAn</ta>
            <ta e="T132" id="Seg_3007" s="T131">kel-TI-m</ta>
            <ta e="T133" id="Seg_3008" s="T132">en</ta>
            <ta e="T134" id="Seg_3009" s="T133">kurduk</ta>
            <ta e="T135" id="Seg_3010" s="T134">hir-BItI-GAr</ta>
            <ta e="T136" id="Seg_3011" s="T135">bahɨlɨk-LAːK</ta>
            <ta e="T137" id="Seg_3012" s="T136">bu͡ol-A-BIt</ta>
            <ta e="T138" id="Seg_3013" s="T137">kineːs</ta>
            <ta e="T139" id="Seg_3014" s="T138">di͡e-An</ta>
            <ta e="T140" id="Seg_3015" s="T139">ol</ta>
            <ta e="T141" id="Seg_3016" s="T140">kaːj-Iː-tI-ttAn</ta>
            <ta e="T142" id="Seg_3017" s="T141">kel-TI-m</ta>
            <ta e="T143" id="Seg_3018" s="T142">di͡e-An</ta>
            <ta e="T144" id="Seg_3019" s="T143">tu͡on-An</ta>
            <ta e="T145" id="Seg_3020" s="T144">naːda-tI-n</ta>
            <ta e="T146" id="Seg_3021" s="T145">kepseː-Ar</ta>
            <ta e="T147" id="Seg_3022" s="T146">ol-GA</ta>
            <ta e="T148" id="Seg_3023" s="T147">Ürüŋ Ajɨː-tA</ta>
            <ta e="T149" id="Seg_3024" s="T148">et-Ar</ta>
            <ta e="T150" id="Seg_3025" s="T149">hu͡ok</ta>
            <ta e="T151" id="Seg_3026" s="T150">baran</ta>
            <ta e="T152" id="Seg_3027" s="T151">et</ta>
            <ta e="T153" id="Seg_3028" s="T152">di͡e-An</ta>
            <ta e="T154" id="Seg_3029" s="T153">tönün-A-r-Ar</ta>
            <ta e="T155" id="Seg_3030" s="T154">ojun-tI-n</ta>
            <ta e="T156" id="Seg_3031" s="T155">bu-tA</ta>
            <ta e="T157" id="Seg_3032" s="T156">kɨːr-An</ta>
            <ta e="T158" id="Seg_3033" s="T157">büt-BIT-tI-GAr</ta>
            <ta e="T159" id="Seg_3034" s="T158">kineːs-tA</ta>
            <ta e="T160" id="Seg_3035" s="T159">kaːj-Ar</ta>
            <ta e="T161" id="Seg_3036" s="T160">en</ta>
            <ta e="T162" id="Seg_3037" s="T161">tü͡ökünün-An</ta>
            <ta e="T163" id="Seg_3038" s="T162">onno-manna</ta>
            <ta e="T164" id="Seg_3039" s="T163">bar-BIT</ta>
            <ta e="T165" id="Seg_3040" s="T164">bu͡ol-A-GIn</ta>
            <ta e="T166" id="Seg_3041" s="T165">tutuːr</ta>
            <ta e="T167" id="Seg_3042" s="T166">kɨrsa-ŋ</ta>
            <ta e="T168" id="Seg_3043" s="T167">manna</ta>
            <ta e="T169" id="Seg_3044" s="T168">tur-Ar</ta>
            <ta e="T170" id="Seg_3045" s="T169">bu͡ol-BAT</ta>
            <ta e="T171" id="Seg_3046" s="T170">du͡o</ta>
            <ta e="T172" id="Seg_3047" s="T171">di͡e-An</ta>
            <ta e="T173" id="Seg_3048" s="T172">kaːj-Ar</ta>
            <ta e="T174" id="Seg_3049" s="T173">bulgu</ta>
            <ta e="T175" id="Seg_3050" s="T174">min-GA</ta>
            <ta e="T176" id="Seg_3051" s="T175">kɨːs-BI-n</ta>
            <ta e="T177" id="Seg_3052" s="T176">egel-An</ta>
            <ta e="T178" id="Seg_3053" s="T177">bi͡er</ta>
            <ta e="T179" id="Seg_3054" s="T178">di͡e-Ar</ta>
            <ta e="T180" id="Seg_3055" s="T179">dʼe</ta>
            <ta e="T181" id="Seg_3056" s="T180">emi͡e</ta>
            <ta e="T182" id="Seg_3057" s="T181">bu</ta>
            <ta e="T183" id="Seg_3058" s="T182">ojun</ta>
            <ta e="T184" id="Seg_3059" s="T183">harsi͡erda</ta>
            <ta e="T185" id="Seg_3060" s="T184">kɨːr-An</ta>
            <ta e="T186" id="Seg_3061" s="T185">muŋ-LAN-n-Ar</ta>
            <ta e="T187" id="Seg_3062" s="T186">bu</ta>
            <ta e="T188" id="Seg_3063" s="T187">kɨːr-An</ta>
            <ta e="T189" id="Seg_3064" s="T188">tij-BIT-tI-GAr</ta>
            <ta e="T190" id="Seg_3065" s="T189">tu͡ok-ttAn</ta>
            <ta e="T191" id="Seg_3066" s="T190">da</ta>
            <ta e="T192" id="Seg_3067" s="T191">hür-LAN-An</ta>
            <ta e="T193" id="Seg_3068" s="T192">Ürüŋ Ajɨː-tA</ta>
            <ta e="T194" id="Seg_3069" s="T193">ojun-tI-n</ta>
            <ta e="T195" id="Seg_3070" s="T194">abalaː-Ar</ta>
            <ta e="T196" id="Seg_3071" s="T195">bu-GA</ta>
            <ta e="T197" id="Seg_3072" s="T196">emi͡e</ta>
            <ta e="T198" id="Seg_3073" s="T197">ojun-tA</ta>
            <ta e="T199" id="Seg_3074" s="T198">ɨtaː-A-n-An</ta>
            <ta e="T200" id="Seg_3075" s="T199">kebilen-Ar</ta>
            <ta e="T201" id="Seg_3076" s="T200">haːtar</ta>
            <ta e="T202" id="Seg_3077" s="T201">beli͡e-TA</ta>
            <ta e="T203" id="Seg_3078" s="T202">bi͡er</ta>
            <ta e="T204" id="Seg_3079" s="T203">di͡e-An</ta>
            <ta e="T205" id="Seg_3080" s="T204">hu͡ok</ta>
            <ta e="T206" id="Seg_3081" s="T205">bi͡er-IAK-m</ta>
            <ta e="T207" id="Seg_3082" s="T206">hu͡ok-tA</ta>
            <ta e="T208" id="Seg_3083" s="T207">togo</ta>
            <ta e="T209" id="Seg_3084" s="T208">aŋɨr=Ij</ta>
            <ta e="T210" id="Seg_3085" s="T209">tu͡ora</ta>
            <ta e="T211" id="Seg_3086" s="T210">karak-LAːK</ta>
            <ta e="T212" id="Seg_3087" s="T211">ajɨlaːgɨn</ta>
            <ta e="T213" id="Seg_3088" s="T212">en</ta>
            <ta e="T214" id="Seg_3089" s="T213">öl-TAK-I-ŋ</ta>
            <ta e="T215" id="Seg_3090" s="T214">di͡e-An</ta>
            <ta e="T216" id="Seg_3091" s="T215">ojun-tI-n</ta>
            <ta e="T217" id="Seg_3092" s="T216">emi͡e</ta>
            <ta e="T218" id="Seg_3093" s="T217">tönün-A-r-Ar</ta>
            <ta e="T219" id="Seg_3094" s="T218">kɨːr-An</ta>
            <ta e="T220" id="Seg_3095" s="T219">büt-An</ta>
            <ta e="T221" id="Seg_3096" s="T220">baran</ta>
            <ta e="T222" id="Seg_3097" s="T221">ojun</ta>
            <ta e="T223" id="Seg_3098" s="T222">kineːs-ttAn</ta>
            <ta e="T224" id="Seg_3099" s="T223">aːrtas-Ar</ta>
            <ta e="T225" id="Seg_3100" s="T224">tu͡ok</ta>
            <ta e="T226" id="Seg_3101" s="T225">da</ta>
            <ta e="T227" id="Seg_3102" s="T226">barɨta</ta>
            <ta e="T228" id="Seg_3103" s="T227">üs</ta>
            <ta e="T229" id="Seg_3104" s="T228">tögül-LAːK</ta>
            <ta e="T230" id="Seg_3105" s="T229">ölör-I-m</ta>
            <ta e="T231" id="Seg_3106" s="T230">horuk-BI-n</ta>
            <ta e="T232" id="Seg_3107" s="T231">hit-BA-tI-tA</ta>
            <ta e="T233" id="Seg_3108" s="T232">di͡e-An</ta>
            <ta e="T234" id="Seg_3109" s="T233">harsɨn</ta>
            <ta e="T235" id="Seg_3110" s="T234">emi͡e</ta>
            <ta e="T236" id="Seg_3111" s="T235">muŋnaː-A-s-An</ta>
            <ta e="T237" id="Seg_3112" s="T236">kör-IAK-m</ta>
            <ta e="T238" id="Seg_3113" s="T237">di͡e-Ar</ta>
            <ta e="T239" id="Seg_3114" s="T238">harsɨŋŋɨ-tI-GAr</ta>
            <ta e="T240" id="Seg_3115" s="T239">üs-Is</ta>
            <ta e="T241" id="Seg_3116" s="T240">kɨːr-Iː-tI-n</ta>
            <ta e="T242" id="Seg_3117" s="T241">kɨːr-Ar</ta>
            <ta e="T243" id="Seg_3118" s="T242">bu</ta>
            <ta e="T244" id="Seg_3119" s="T243">kɨːr-An</ta>
            <ta e="T245" id="Seg_3120" s="T244">Ürüŋ Ajɨː</ta>
            <ta e="T246" id="Seg_3121" s="T245">dʼi͡e-tI-GAr</ta>
            <ta e="T247" id="Seg_3122" s="T246">tij-Ar</ta>
            <ta e="T248" id="Seg_3123" s="T247">bu</ta>
            <ta e="T249" id="Seg_3124" s="T248">tij-An</ta>
            <ta e="T250" id="Seg_3125" s="T249">toŋkoj-An</ta>
            <ta e="T251" id="Seg_3126" s="T250">dʼi͡e-tI-n</ta>
            <ta e="T252" id="Seg_3127" s="T251">hu͡ona-tI-n</ta>
            <ta e="T253" id="Seg_3128" s="T252">ičči-tI-GAr</ta>
            <ta e="T254" id="Seg_3129" s="T253">aːrtas-An</ta>
            <ta e="T255" id="Seg_3130" s="T254">Ürüŋ Ajɨː-tI-n</ta>
            <ta e="T256" id="Seg_3131" s="T255">atak-tA-r</ta>
            <ta e="T257" id="Seg_3132" s="T256">hɨːl-An</ta>
            <ta e="T258" id="Seg_3133" s="T257">kel-An</ta>
            <ta e="T259" id="Seg_3134" s="T258">bu</ta>
            <ta e="T260" id="Seg_3135" s="T259">dʼiŋŋi-tI-TAːgAr</ta>
            <ta e="T261" id="Seg_3136" s="T260">hürdeːk-LIk</ta>
            <ta e="T262" id="Seg_3137" s="T261">tu͡on-Ar-ɨtaː-Ar</ta>
            <ta e="T263" id="Seg_3138" s="T262">bu-nI</ta>
            <ta e="T264" id="Seg_3139" s="T263">dʼe</ta>
            <ta e="T265" id="Seg_3140" s="T264">hürek-tI-nAn</ta>
            <ta e="T266" id="Seg_3141" s="T265">ahɨn-Ar</ta>
            <ta e="T267" id="Seg_3142" s="T266">dʼe</ta>
            <ta e="T268" id="Seg_3143" s="T267">kirdik</ta>
            <ta e="T269" id="Seg_3144" s="T268">min</ta>
            <ta e="T270" id="Seg_3145" s="T269">en-n</ta>
            <ta e="T271" id="Seg_3146" s="T270">ölör-AːrI</ta>
            <ta e="T272" id="Seg_3147" s="T271">beje-m</ta>
            <ta e="T273" id="Seg_3148" s="T272">ojun-I-m</ta>
            <ta e="T274" id="Seg_3149" s="T273">gɨn-BAtAK-I-m</ta>
            <ta e="T275" id="Seg_3150" s="T274">togo</ta>
            <ta e="T276" id="Seg_3151" s="T275">bagas</ta>
            <ta e="T277" id="Seg_3152" s="T276">kineːs-I-ŋ</ta>
            <ta e="T278" id="Seg_3153" s="T277">oburgu</ta>
            <ta e="T279" id="Seg_3154" s="T278">aŋɨr=Ij</ta>
            <ta e="T280" id="Seg_3155" s="T279">dʼe</ta>
            <ta e="T281" id="Seg_3156" s="T280">beje-tI-n</ta>
            <ta e="T282" id="Seg_3157" s="T281">noru͡ot-tI-GAr</ta>
            <ta e="T283" id="Seg_3158" s="T282">i͡edeːn-nI</ta>
            <ta e="T284" id="Seg_3159" s="T283">oŋor-TAK-tA</ta>
            <ta e="T285" id="Seg_3160" s="T284">noru͡ot-GItI-ttAn</ta>
            <ta e="T286" id="Seg_3161" s="T285">čogotok</ta>
            <ta e="T287" id="Seg_3162" s="T286">en</ta>
            <ta e="T288" id="Seg_3163" s="T287">u͡ol-GI-n</ta>
            <ta e="T289" id="Seg_3164" s="T288">kɨtarɨ</ta>
            <ta e="T290" id="Seg_3165" s="T289">ort-IAK-GIt</ta>
            <ta e="T291" id="Seg_3166" s="T290">tu͡ok</ta>
            <ta e="T292" id="Seg_3167" s="T291">da</ta>
            <ta e="T293" id="Seg_3168" s="T292">tɨːnnaːk</ta>
            <ta e="T294" id="Seg_3169" s="T293">kaːl-IAK.[tA]</ta>
            <ta e="T295" id="Seg_3170" s="T294">hu͡ok-tA</ta>
            <ta e="T296" id="Seg_3171" s="T295">atɨn</ta>
            <ta e="T297" id="Seg_3172" s="T296">hir-ttAn</ta>
            <ta e="T298" id="Seg_3173" s="T297">dʼon</ta>
            <ta e="T299" id="Seg_3174" s="T298">kel-An-LAr</ta>
            <ta e="T300" id="Seg_3175" s="T299">ol-ttAn</ta>
            <ta e="T301" id="Seg_3176" s="T300">haŋa</ta>
            <ta e="T302" id="Seg_3177" s="T301">dʼon</ta>
            <ta e="T303" id="Seg_3178" s="T302">ü͡öskeː-IAK-tA</ta>
            <ta e="T304" id="Seg_3179" s="T303">di͡e-Ar</ta>
            <ta e="T305" id="Seg_3180" s="T304">haŋa</ta>
            <ta e="T306" id="Seg_3181" s="T305">ɨraːs</ta>
            <ta e="T307" id="Seg_3182" s="T306">čeːlkeː</ta>
            <ta e="T308" id="Seg_3183" s="T307">öldüːn-nA</ta>
            <ta e="T309" id="Seg_3184" s="T308">anɨ</ta>
            <ta e="T310" id="Seg_3185" s="T309">üs</ta>
            <ta e="T311" id="Seg_3186" s="T310">konuk-tI-nAn</ta>
            <ta e="T312" id="Seg_3187" s="T311">harsi͡erda</ta>
            <ta e="T313" id="Seg_3188" s="T312">turu͡or-TInnAr</ta>
            <ta e="T314" id="Seg_3189" s="T313">ču͡ogur</ta>
            <ta e="T315" id="Seg_3190" s="T314">čeːlkeː</ta>
            <ta e="T316" id="Seg_3191" s="T315">taba-nI</ta>
            <ta e="T317" id="Seg_3192" s="T316">ölör-TInnAr</ta>
            <ta e="T318" id="Seg_3193" s="T317">ol</ta>
            <ta e="T319" id="Seg_3194" s="T318">uraha</ta>
            <ta e="T320" id="Seg_3195" s="T319">tula-tI-n</ta>
            <ta e="T321" id="Seg_3196" s="T320">barɨ-tI-n</ta>
            <ta e="T322" id="Seg_3197" s="T321">čeːlkeː</ta>
            <ta e="T323" id="Seg_3198" s="T322">taba</ta>
            <ta e="T324" id="Seg_3199" s="T323">tiriː-tI-nAn</ta>
            <ta e="T325" id="Seg_3200" s="T324">telgeː-TInnAr</ta>
            <ta e="T326" id="Seg_3201" s="T325">bu-nAn</ta>
            <ta e="T327" id="Seg_3202" s="T326">ükten-An</ta>
            <ta e="T328" id="Seg_3203" s="T327">kiːr-IAK-tA</ta>
            <ta e="T329" id="Seg_3204" s="T328">kineːs-GIt</ta>
            <ta e="T330" id="Seg_3205" s="T329">bɨrtak</ta>
            <ta e="T331" id="Seg_3206" s="T330">tu͡ok</ta>
            <ta e="T332" id="Seg_3207" s="T331">da</ta>
            <ta e="T333" id="Seg_3208" s="T332">hɨhɨn-IAK.[tA]</ta>
            <ta e="T334" id="Seg_3209" s="T333">hu͡ok-LAːK</ta>
            <ta e="T335" id="Seg_3210" s="T334">di͡e-Ar</ta>
            <ta e="T336" id="Seg_3211" s="T335">dʼon-LAr</ta>
            <ta e="T337" id="Seg_3212" s="T336">kurum</ta>
            <ta e="T338" id="Seg_3213" s="T337">di͡e-An</ta>
            <ta e="T339" id="Seg_3214" s="T338">kel-BA-TInnAr</ta>
            <ta e="T340" id="Seg_3215" s="T339">kim</ta>
            <ta e="T341" id="Seg_3216" s="T340">tɨːn-tI-n</ta>
            <ta e="T342" id="Seg_3217" s="T341">karɨstaː-AːrI</ta>
            <ta e="T343" id="Seg_3218" s="T342">gɨn-Ar</ta>
            <ta e="T344" id="Seg_3219" s="T343">ɨraːk</ta>
            <ta e="T345" id="Seg_3220" s="T344">bar-TIn</ta>
            <ta e="T346" id="Seg_3221" s="T345">di͡e-BIT</ta>
            <ta e="T347" id="Seg_3222" s="T346">üs-Is</ta>
            <ta e="T348" id="Seg_3223" s="T347">kün-tI-GAr</ta>
            <ta e="T349" id="Seg_3224" s="T348">et-BIT-tI-n</ta>
            <ta e="T350" id="Seg_3225" s="T349">kurduk</ta>
            <ta e="T351" id="Seg_3226" s="T350">oŋor-BIT-LAr</ta>
            <ta e="T352" id="Seg_3227" s="T351">manna</ta>
            <ta e="T353" id="Seg_3228" s="T352">ojun</ta>
            <ta e="T354" id="Seg_3229" s="T353">kör-An</ta>
            <ta e="T355" id="Seg_3230" s="T354">tur-BIT</ta>
            <ta e="T356" id="Seg_3231" s="T355">kütür</ta>
            <ta e="T357" id="Seg_3232" s="T356">bagajɨ</ta>
            <ta e="T358" id="Seg_3233" s="T357">boloho</ta>
            <ta e="T359" id="Seg_3234" s="T358">tɨ͡al</ta>
            <ta e="T360" id="Seg_3235" s="T359">tüs-BIT</ta>
            <ta e="T361" id="Seg_3236" s="T360">ču͡oraːn</ta>
            <ta e="T362" id="Seg_3237" s="T361">tɨ͡as-tA</ta>
            <ta e="T363" id="Seg_3238" s="T362">di͡e-An</ta>
            <ta e="T364" id="Seg_3239" s="T363">bagajɨ</ta>
            <ta e="T365" id="Seg_3240" s="T364">ihilin-I-BIT</ta>
            <ta e="T366" id="Seg_3241" s="T365">tu͡ok</ta>
            <ta e="T367" id="Seg_3242" s="T366">ere</ta>
            <ta e="T368" id="Seg_3243" s="T367">tɨ͡al</ta>
            <ta e="T369" id="Seg_3244" s="T368">kurduk</ta>
            <ta e="T370" id="Seg_3245" s="T369">uraha</ta>
            <ta e="T371" id="Seg_3246" s="T370">aːn-tI-n</ta>
            <ta e="T372" id="Seg_3247" s="T371">arɨj-Ar-GA</ta>
            <ta e="T373" id="Seg_3248" s="T372">dɨlɨ</ta>
            <ta e="T374" id="Seg_3249" s="T373">gɨn-BIT</ta>
            <ta e="T375" id="Seg_3250" s="T374">ol</ta>
            <ta e="T376" id="Seg_3251" s="T375">kennine</ta>
            <ta e="T377" id="Seg_3252" s="T376">tɨ͡al-tA</ta>
            <ta e="T378" id="Seg_3253" s="T377">hu͡ok</ta>
            <ta e="T379" id="Seg_3254" s="T378">bu͡ol-BIT</ta>
            <ta e="T380" id="Seg_3255" s="T379">bu-nI</ta>
            <ta e="T381" id="Seg_3256" s="T380">ojun</ta>
            <ta e="T382" id="Seg_3257" s="T381">bar-An</ta>
            <ta e="T383" id="Seg_3258" s="T382">kör-BIT-tA</ta>
            <ta e="T384" id="Seg_3259" s="T383">kineːs</ta>
            <ta e="T385" id="Seg_3260" s="T384">da</ta>
            <ta e="T386" id="Seg_3261" s="T385">barɨ</ta>
            <ta e="T387" id="Seg_3262" s="T386">kel-BIT</ta>
            <ta e="T388" id="Seg_3263" s="T387">da</ta>
            <ta e="T389" id="Seg_3264" s="T388">dʼon</ta>
            <ta e="T390" id="Seg_3265" s="T389">öl-A</ta>
            <ta e="T391" id="Seg_3266" s="T390">hɨt-Ar-LAr</ta>
            <ta e="T392" id="Seg_3267" s="T391">e-BIT</ta>
            <ta e="T393" id="Seg_3268" s="T392">bu-nAn</ta>
            <ta e="T394" id="Seg_3269" s="T393">Norilskaj</ta>
            <ta e="T395" id="Seg_3270" s="T394">dʼadaj-TI-tA</ta>
            <ta e="T396" id="Seg_3271" s="T395">iti</ta>
            <ta e="T397" id="Seg_3272" s="T396">edʼeːn</ta>
            <ta e="T398" id="Seg_3273" s="T397">tu͡ora-ttAn</ta>
            <ta e="T399" id="Seg_3274" s="T398">kel-An</ta>
            <ta e="T400" id="Seg_3275" s="T399">onton</ta>
            <ta e="T401" id="Seg_3276" s="T400">ere</ta>
            <ta e="T402" id="Seg_3277" s="T401">ü͡öskeː-Ar</ta>
            <ta e="T403" id="Seg_3278" s="T402">bu͡ol-TI-LAr</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_3279" s="T0">long.ago</ta>
            <ta e="T2" id="Seg_3280" s="T1">Norilsk-DAT/LOC</ta>
            <ta e="T3" id="Seg_3281" s="T2">prince-PROPR-3PL</ta>
            <ta e="T4" id="Seg_3282" s="T3">it.is.said</ta>
            <ta e="T5" id="Seg_3283" s="T4">young</ta>
            <ta e="T6" id="Seg_3284" s="T5">human.being.[NOM]</ta>
            <ta e="T7" id="Seg_3285" s="T6">this</ta>
            <ta e="T8" id="Seg_3286" s="T7">human.being-3PL.[NOM]</ta>
            <ta e="T9" id="Seg_3287" s="T8">woman-POSS</ta>
            <ta e="T10" id="Seg_3288" s="T9">NEG.[3SG]</ta>
            <ta e="T11" id="Seg_3289" s="T10">this-ACC</ta>
            <ta e="T12" id="Seg_3290" s="T11">family-PL-3SG.[NOM]</ta>
            <ta e="T13" id="Seg_3291" s="T12">once</ta>
            <ta e="T14" id="Seg_3292" s="T13">ask-PRS-3PL</ta>
            <ta e="T15" id="Seg_3293" s="T14">why</ta>
            <ta e="T16" id="Seg_3294" s="T15">woman-VBZ-NEG-2SG</ta>
            <ta e="T17" id="Seg_3295" s="T16">say-CVB.SEQ</ta>
            <ta e="T18" id="Seg_3296" s="T17">that-DAT/LOC</ta>
            <ta e="T19" id="Seg_3297" s="T18">speak-PRS.[3SG]</ta>
            <ta e="T20" id="Seg_3298" s="T19">1SG.[NOM]</ta>
            <ta e="T21" id="Seg_3299" s="T20">this</ta>
            <ta e="T22" id="Seg_3300" s="T21">country.[NOM]</ta>
            <ta e="T23" id="Seg_3301" s="T22">girl-3SG-ACC</ta>
            <ta e="T24" id="Seg_3302" s="T23">take-NEG.PTCP</ta>
            <ta e="T25" id="Seg_3303" s="T24">human.being-1SG</ta>
            <ta e="T26" id="Seg_3304" s="T25">up</ta>
            <ta e="T27" id="Seg_3305" s="T26">Urung.Ajyy.[NOM]</ta>
            <ta e="T28" id="Seg_3306" s="T27">say-CVB.SEQ</ta>
            <ta e="T29" id="Seg_3307" s="T28">there.is</ta>
            <ta e="T30" id="Seg_3308" s="T29">that.[NOM]</ta>
            <ta e="T31" id="Seg_3309" s="T30">daughter-3SG-ACC</ta>
            <ta e="T32" id="Seg_3310" s="T31">only</ta>
            <ta e="T33" id="Seg_3311" s="T32">take-TEMP-1SG</ta>
            <ta e="T34" id="Seg_3312" s="T33">then</ta>
            <ta e="T35" id="Seg_3313" s="T34">agree-EP-RECP/COLL-PTCP.FUT</ta>
            <ta e="T36" id="Seg_3314" s="T35">be-PST1-1SG</ta>
            <ta e="T37" id="Seg_3315" s="T36">say-PRS.[3SG]</ta>
            <ta e="T38" id="Seg_3316" s="T37">this-ACC</ta>
            <ta e="T39" id="Seg_3317" s="T38">people-PL.[NOM]</ta>
            <ta e="T40" id="Seg_3318" s="T39">play-CVB.SEQ</ta>
            <ta e="T41" id="Seg_3319" s="T40">speak-INFER-3SG</ta>
            <ta e="T42" id="Seg_3320" s="T41">think-CVB.SEQ</ta>
            <ta e="T43" id="Seg_3321" s="T42">throw-PRS-3PL</ta>
            <ta e="T44" id="Seg_3322" s="T43">once</ta>
            <ta e="T45" id="Seg_3323" s="T44">gathering-DAT/LOC</ta>
            <ta e="T46" id="Seg_3324" s="T45">prince.[NOM]</ta>
            <ta e="T47" id="Seg_3325" s="T46">shaman-ACC</ta>
            <ta e="T48" id="Seg_3326" s="T47">force-PRS.[3SG]</ta>
            <ta e="T49" id="Seg_3327" s="T48">2SG.[NOM]</ta>
            <ta e="T50" id="Seg_3328" s="T49">1SG-DAT/LOC</ta>
            <ta e="T51" id="Seg_3329" s="T50">sky-DAT/LOC</ta>
            <ta e="T52" id="Seg_3330" s="T51">there.is</ta>
            <ta e="T53" id="Seg_3331" s="T52">Urung.Ajyy.[NOM]</ta>
            <ta e="T54" id="Seg_3332" s="T53">daughter-3SG-ACC</ta>
            <ta e="T55" id="Seg_3333" s="T54">woman.[NOM]</ta>
            <ta e="T56" id="Seg_3334" s="T55">make-PTCP.FUT-1SG-ACC</ta>
            <ta e="T57" id="Seg_3335" s="T56">courting-VBZ.[IMP.2SG]</ta>
            <ta e="T58" id="Seg_3336" s="T57">say-PRS.[3SG]</ta>
            <ta e="T59" id="Seg_3337" s="T58">this-ACC</ta>
            <ta e="T60" id="Seg_3338" s="T59">shaman-3SG.[NOM]</ta>
            <ta e="T61" id="Seg_3339" s="T60">be.rather.afraid-CVB.SIM</ta>
            <ta e="T62" id="Seg_3340" s="T61">hear-PRS.[3SG]</ta>
            <ta e="T63" id="Seg_3341" s="T62">what-2SG.[NOM]</ta>
            <ta e="T64" id="Seg_3342" s="T63">word-3SG.[NOM]=Q</ta>
            <ta e="T65" id="Seg_3343" s="T64">how</ta>
            <ta e="T66" id="Seg_3344" s="T65">NEG</ta>
            <ta e="T67" id="Seg_3345" s="T66">that</ta>
            <ta e="T68" id="Seg_3346" s="T67">country-ABL</ta>
            <ta e="T69" id="Seg_3347" s="T68">can-CVB.SEQ</ta>
            <ta e="T70" id="Seg_3348" s="T69">bring-NEG.PTCP</ta>
            <ta e="T71" id="Seg_3349" s="T70">matter-EP-1SG.[NOM]</ta>
            <ta e="T72" id="Seg_3350" s="T71">say-PRS.[3SG]</ta>
            <ta e="T73" id="Seg_3351" s="T72">this-ACC</ta>
            <ta e="T74" id="Seg_3352" s="T73">prince.[NOM]</ta>
            <ta e="T75" id="Seg_3353" s="T74">seven</ta>
            <ta e="T76" id="Seg_3354" s="T75">wicker-ACC</ta>
            <ta e="T77" id="Seg_3355" s="T76">see-CAUS-PRS.[3SG]</ta>
            <ta e="T78" id="Seg_3356" s="T77">2SG.[NOM]</ta>
            <ta e="T79" id="Seg_3357" s="T78">deception-VBZ-EP-NEG.[IMP.2SG]</ta>
            <ta e="T80" id="Seg_3358" s="T79">1SG.[NOM]</ta>
            <ta e="T81" id="Seg_3359" s="T80">2SG-ACC</ta>
            <ta e="T82" id="Seg_3360" s="T81">lower.part.of.the.back-2SG-ACC</ta>
            <ta e="T83" id="Seg_3361" s="T82">skin-FUT-1SG</ta>
            <ta e="T84" id="Seg_3362" s="T83">day.[NOM]</ta>
            <ta e="T85" id="Seg_3363" s="T84">tomorrow</ta>
            <ta e="T86" id="Seg_3364" s="T85">Urung.Ajyy.[NOM]</ta>
            <ta e="T87" id="Seg_3365" s="T86">daughter-3SG-ACC</ta>
            <ta e="T88" id="Seg_3366" s="T87">there.is</ta>
            <ta e="T89" id="Seg_3367" s="T88">make.[IMP.2SG]</ta>
            <ta e="T90" id="Seg_3368" s="T89">say-PRS.[3SG]</ta>
            <ta e="T91" id="Seg_3369" s="T90">shaman.[NOM]</ta>
            <ta e="T92" id="Seg_3370" s="T91">come.to.an.end-FUT.[3SG]</ta>
            <ta e="T93" id="Seg_3371" s="T92">MOD</ta>
            <ta e="T94" id="Seg_3372" s="T93">well</ta>
            <ta e="T95" id="Seg_3373" s="T94">shamanize-CVB.SEQ</ta>
            <ta e="T96" id="Seg_3374" s="T95">go-PRS.[3SG]</ta>
            <ta e="T97" id="Seg_3375" s="T96">this</ta>
            <ta e="T98" id="Seg_3376" s="T97">shaman.[NOM]</ta>
            <ta e="T99" id="Seg_3377" s="T98">come.closer-PTCP.PST-3SG-ACC</ta>
            <ta e="T100" id="Seg_3378" s="T99">hear-CVB.SEQ</ta>
            <ta e="T101" id="Seg_3379" s="T100">Urung.Ajyy.[NOM]</ta>
            <ta e="T102" id="Seg_3380" s="T101">lord.[NOM]</ta>
            <ta e="T103" id="Seg_3381" s="T102">very-ADVZ</ta>
            <ta e="T104" id="Seg_3382" s="T103">get.angry-PRS.[3SG]</ta>
            <ta e="T105" id="Seg_3383" s="T104">what.[NOM]</ta>
            <ta e="T106" id="Seg_3384" s="T105">die-PTCP.PRS</ta>
            <ta e="T107" id="Seg_3385" s="T106">lower.world.[NOM]</ta>
            <ta e="T108" id="Seg_3386" s="T107">shaman-3SG.[NOM]</ta>
            <ta e="T109" id="Seg_3387" s="T108">order-POSS</ta>
            <ta e="T110" id="Seg_3388" s="T109">NEG</ta>
            <ta e="T111" id="Seg_3389" s="T110">come-INFER-3SG=Q</ta>
            <ta e="T112" id="Seg_3390" s="T111">bring-EP-IMP.2PL</ta>
            <ta e="T113" id="Seg_3391" s="T112">1SG-DAT/LOC</ta>
            <ta e="T114" id="Seg_3392" s="T113">fire.[NOM]</ta>
            <ta e="T115" id="Seg_3393" s="T114">sword-1SG-ACC</ta>
            <ta e="T116" id="Seg_3394" s="T115">head-ACC</ta>
            <ta e="T117" id="Seg_3395" s="T116">cut-CVB.SIM</ta>
            <ta e="T118" id="Seg_3396" s="T117">beat-PTCP.FUT-1SG-ACC</ta>
            <ta e="T119" id="Seg_3397" s="T118">say-PST2.[3SG]</ta>
            <ta e="T120" id="Seg_3398" s="T119">shaman-3SG.[NOM]</ta>
            <ta e="T121" id="Seg_3399" s="T120">come-PST2-3SG</ta>
            <ta e="T122" id="Seg_3400" s="T121">get.ready-CVB.SEQ</ta>
            <ta e="T123" id="Seg_3401" s="T122">sit-PTCP.PRS</ta>
            <ta e="T124" id="Seg_3402" s="T123">be-PST2.[3SG]</ta>
            <ta e="T125" id="Seg_3403" s="T124">Ajyy-3SG.[NOM]</ta>
            <ta e="T126" id="Seg_3404" s="T125">this-ACC</ta>
            <ta e="T127" id="Seg_3405" s="T126">well</ta>
            <ta e="T128" id="Seg_3406" s="T127">crawl-CVB.SEQ</ta>
            <ta e="T129" id="Seg_3407" s="T128">beg-EP-MED-PRS.[3SG]</ta>
            <ta e="T130" id="Seg_3408" s="T129">1SG.[NOM]</ta>
            <ta e="T131" id="Seg_3409" s="T130">need-ABL</ta>
            <ta e="T132" id="Seg_3410" s="T131">come-PST1-1SG</ta>
            <ta e="T133" id="Seg_3411" s="T132">2SG.[NOM]</ta>
            <ta e="T134" id="Seg_3412" s="T133">like</ta>
            <ta e="T135" id="Seg_3413" s="T134">earth-1PL-DAT/LOC</ta>
            <ta e="T136" id="Seg_3414" s="T135">leader-PROPR.[NOM]</ta>
            <ta e="T137" id="Seg_3415" s="T136">be-PRS-1PL</ta>
            <ta e="T138" id="Seg_3416" s="T137">prince.[NOM]</ta>
            <ta e="T139" id="Seg_3417" s="T138">say-CVB.SEQ</ta>
            <ta e="T140" id="Seg_3418" s="T139">that.[NOM]</ta>
            <ta e="T141" id="Seg_3419" s="T140">force-NMNZ-3SG-ABL</ta>
            <ta e="T142" id="Seg_3420" s="T141">come-PST1-1SG</ta>
            <ta e="T143" id="Seg_3421" s="T142">say-CVB.SEQ</ta>
            <ta e="T144" id="Seg_3422" s="T143">complain-CVB.SEQ</ta>
            <ta e="T145" id="Seg_3423" s="T144">need-3SG-ACC</ta>
            <ta e="T146" id="Seg_3424" s="T145">tell-PRS.[3SG]</ta>
            <ta e="T147" id="Seg_3425" s="T146">that-DAT/LOC</ta>
            <ta e="T148" id="Seg_3426" s="T147">Urung.Ajyy-3SG.[NOM]</ta>
            <ta e="T149" id="Seg_3427" s="T148">speak-PRS.[3SG]</ta>
            <ta e="T150" id="Seg_3428" s="T149">no</ta>
            <ta e="T151" id="Seg_3429" s="T150">end.[IMP.2SG]</ta>
            <ta e="T152" id="Seg_3430" s="T151">speak.[IMP.2SG]</ta>
            <ta e="T153" id="Seg_3431" s="T152">say-CVB.SEQ</ta>
            <ta e="T154" id="Seg_3432" s="T153">come.back-EP-CAUS-PRS.[3SG]</ta>
            <ta e="T155" id="Seg_3433" s="T154">shaman-3SG-ACC</ta>
            <ta e="T156" id="Seg_3434" s="T155">this-3SG.[NOM]</ta>
            <ta e="T157" id="Seg_3435" s="T156">shamanize-CVB.SEQ</ta>
            <ta e="T158" id="Seg_3436" s="T157">stop-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T159" id="Seg_3437" s="T158">prince-3SG.[NOM]</ta>
            <ta e="T160" id="Seg_3438" s="T159">force-PRS.[3SG]</ta>
            <ta e="T161" id="Seg_3439" s="T160">2SG.[NOM]</ta>
            <ta e="T162" id="Seg_3440" s="T161">pretend-CVB.SEQ</ta>
            <ta e="T163" id="Seg_3441" s="T162">thither-hither</ta>
            <ta e="T164" id="Seg_3442" s="T163">go-PTCP.PST</ta>
            <ta e="T165" id="Seg_3443" s="T164">be-PRS-2SG</ta>
            <ta e="T166" id="Seg_3444" s="T165">sacrifice.[NOM]</ta>
            <ta e="T167" id="Seg_3445" s="T166">polar.fox-2SG.[NOM]</ta>
            <ta e="T168" id="Seg_3446" s="T167">here</ta>
            <ta e="T169" id="Seg_3447" s="T168">stand-PTCP.PRS</ta>
            <ta e="T170" id="Seg_3448" s="T169">be-NEG.[3SG]</ta>
            <ta e="T171" id="Seg_3449" s="T170">Q</ta>
            <ta e="T172" id="Seg_3450" s="T171">say-CVB.SEQ</ta>
            <ta e="T173" id="Seg_3451" s="T172">force-PRS.[3SG]</ta>
            <ta e="T174" id="Seg_3452" s="T173">by.all.means</ta>
            <ta e="T175" id="Seg_3453" s="T174">1SG-DAT/LOC</ta>
            <ta e="T176" id="Seg_3454" s="T175">daughter-1SG-ACC</ta>
            <ta e="T177" id="Seg_3455" s="T176">get-CVB.SEQ</ta>
            <ta e="T178" id="Seg_3456" s="T177">give.[IMP.2SG]</ta>
            <ta e="T179" id="Seg_3457" s="T178">say-PRS.[3SG]</ta>
            <ta e="T180" id="Seg_3458" s="T179">well</ta>
            <ta e="T181" id="Seg_3459" s="T180">again</ta>
            <ta e="T182" id="Seg_3460" s="T181">this</ta>
            <ta e="T183" id="Seg_3461" s="T182">shaman.[NOM]</ta>
            <ta e="T184" id="Seg_3462" s="T183">in.the.morning</ta>
            <ta e="T185" id="Seg_3463" s="T184">shamanize-CVB.SEQ</ta>
            <ta e="T186" id="Seg_3464" s="T185">misery-VBZ-MED-PRS.[3SG]</ta>
            <ta e="T187" id="Seg_3465" s="T186">this</ta>
            <ta e="T188" id="Seg_3466" s="T187">shamanize-CVB.SEQ</ta>
            <ta e="T189" id="Seg_3467" s="T188">reach-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T190" id="Seg_3468" s="T189">what-ABL</ta>
            <ta e="T191" id="Seg_3469" s="T190">and</ta>
            <ta e="T192" id="Seg_3470" s="T191">horror-VBZ-CVB.SEQ</ta>
            <ta e="T193" id="Seg_3471" s="T192">Urung.Ajyy-3SG.[NOM]</ta>
            <ta e="T194" id="Seg_3472" s="T193">shaman-3SG-ACC</ta>
            <ta e="T195" id="Seg_3473" s="T194">be.upset-PRS.[3SG]</ta>
            <ta e="T196" id="Seg_3474" s="T195">this-DAT/LOC</ta>
            <ta e="T197" id="Seg_3475" s="T196">again</ta>
            <ta e="T198" id="Seg_3476" s="T197">shaman-3SG.[NOM]</ta>
            <ta e="T199" id="Seg_3477" s="T198">cry-EP-MED-CVB.SEQ</ta>
            <ta e="T200" id="Seg_3478" s="T199">suffer-PRS.[3SG]</ta>
            <ta e="T201" id="Seg_3479" s="T200">though</ta>
            <ta e="T202" id="Seg_3480" s="T201">mark-PART</ta>
            <ta e="T203" id="Seg_3481" s="T202">give.[IMP.2SG]</ta>
            <ta e="T204" id="Seg_3482" s="T203">say-CVB.SEQ</ta>
            <ta e="T205" id="Seg_3483" s="T204">no</ta>
            <ta e="T206" id="Seg_3484" s="T205">give-FUT-1SG</ta>
            <ta e="T207" id="Seg_3485" s="T206">NEG-3SG</ta>
            <ta e="T208" id="Seg_3486" s="T207">why</ta>
            <ta e="T209" id="Seg_3487" s="T208">stupid.[NOM]=Q</ta>
            <ta e="T210" id="Seg_3488" s="T209">across</ta>
            <ta e="T211" id="Seg_3489" s="T210">eye-PROPR.[NOM]</ta>
            <ta e="T212" id="Seg_3490" s="T211">better</ta>
            <ta e="T213" id="Seg_3491" s="T212">2SG.[NOM]</ta>
            <ta e="T214" id="Seg_3492" s="T213">die-INFER-EP-2SG</ta>
            <ta e="T215" id="Seg_3493" s="T214">say-CVB.SEQ</ta>
            <ta e="T216" id="Seg_3494" s="T215">shaman-3SG-ACC</ta>
            <ta e="T217" id="Seg_3495" s="T216">again</ta>
            <ta e="T218" id="Seg_3496" s="T217">come.back-EP-CAUS-PRS.[3SG]</ta>
            <ta e="T219" id="Seg_3497" s="T218">shamanize-CVB.SEQ</ta>
            <ta e="T220" id="Seg_3498" s="T219">stop-CVB.SEQ</ta>
            <ta e="T221" id="Seg_3499" s="T220">after</ta>
            <ta e="T222" id="Seg_3500" s="T221">shaman.[NOM]</ta>
            <ta e="T223" id="Seg_3501" s="T222">prince-ABL</ta>
            <ta e="T224" id="Seg_3502" s="T223">beseech-PRS.[3SG]</ta>
            <ta e="T225" id="Seg_3503" s="T224">what</ta>
            <ta e="T226" id="Seg_3504" s="T225">NEG</ta>
            <ta e="T227" id="Seg_3505" s="T226">completely</ta>
            <ta e="T228" id="Seg_3506" s="T227">three</ta>
            <ta e="T229" id="Seg_3507" s="T228">time-PROPR.[NOM]</ta>
            <ta e="T230" id="Seg_3508" s="T229">kill-EP-NEG.[IMP.2SG]</ta>
            <ta e="T231" id="Seg_3509" s="T230">wish-1SG-ACC</ta>
            <ta e="T232" id="Seg_3510" s="T231">reach-NEG-PST1-3SG</ta>
            <ta e="T233" id="Seg_3511" s="T232">say-CVB.SEQ</ta>
            <ta e="T234" id="Seg_3512" s="T233">tomorrow</ta>
            <ta e="T235" id="Seg_3513" s="T234">again</ta>
            <ta e="T236" id="Seg_3514" s="T235">torture-EP-RECP/COLL-CVB.SEQ</ta>
            <ta e="T237" id="Seg_3515" s="T236">try-FUT-1SG</ta>
            <ta e="T238" id="Seg_3516" s="T237">say-PRS.[3SG]</ta>
            <ta e="T239" id="Seg_3517" s="T238">next.morning-3SG-DAT/LOC</ta>
            <ta e="T240" id="Seg_3518" s="T239">three-ORD</ta>
            <ta e="T241" id="Seg_3519" s="T240">shamanize-NMNZ-3SG-ACC</ta>
            <ta e="T242" id="Seg_3520" s="T241">shamanize-PRS.[3SG]</ta>
            <ta e="T243" id="Seg_3521" s="T242">this</ta>
            <ta e="T244" id="Seg_3522" s="T243">shamanize-CVB.SEQ</ta>
            <ta e="T245" id="Seg_3523" s="T244">Urung.Ajyy.[NOM]</ta>
            <ta e="T246" id="Seg_3524" s="T245">house-3SG-DAT/LOC</ta>
            <ta e="T247" id="Seg_3525" s="T246">reach-PRS.[3SG]</ta>
            <ta e="T248" id="Seg_3526" s="T247">this</ta>
            <ta e="T249" id="Seg_3527" s="T248">reach-CVB.SEQ</ta>
            <ta e="T250" id="Seg_3528" s="T249">bow.to-CVB.SEQ</ta>
            <ta e="T251" id="Seg_3529" s="T250">tent-3SG-GEN</ta>
            <ta e="T252" id="Seg_3530" s="T251">main.pole.of.a.tent-3SG-GEN</ta>
            <ta e="T253" id="Seg_3531" s="T252">master-3SG-DAT/LOC</ta>
            <ta e="T254" id="Seg_3532" s="T253">beseech-CVB.SEQ</ta>
            <ta e="T255" id="Seg_3533" s="T254">Urung.Ajyy-3SG-GEN</ta>
            <ta e="T256" id="Seg_3534" s="T255">foot-3SG-DAT/LOC</ta>
            <ta e="T257" id="Seg_3535" s="T256">crawl-CVB.SEQ</ta>
            <ta e="T258" id="Seg_3536" s="T257">come-CVB.SEQ</ta>
            <ta e="T259" id="Seg_3537" s="T258">this</ta>
            <ta e="T260" id="Seg_3538" s="T259">former-3SG-COMP</ta>
            <ta e="T261" id="Seg_3539" s="T260">very-ADVZ</ta>
            <ta e="T262" id="Seg_3540" s="T261">complain-PRS.[3SG]-cry-PRS.[3SG]</ta>
            <ta e="T263" id="Seg_3541" s="T262">this-ACC</ta>
            <ta e="T264" id="Seg_3542" s="T263">well</ta>
            <ta e="T265" id="Seg_3543" s="T264">heart-3SG-INSTR</ta>
            <ta e="T266" id="Seg_3544" s="T265">feel.sorry-PRS.[3SG]</ta>
            <ta e="T267" id="Seg_3545" s="T266">well</ta>
            <ta e="T268" id="Seg_3546" s="T267">truth.[NOM]</ta>
            <ta e="T269" id="Seg_3547" s="T268">1SG.[NOM]</ta>
            <ta e="T270" id="Seg_3548" s="T269">2SG-ACC</ta>
            <ta e="T271" id="Seg_3549" s="T270">kill-CVB.PURP</ta>
            <ta e="T272" id="Seg_3550" s="T271">self-1SG.[NOM]</ta>
            <ta e="T273" id="Seg_3551" s="T272">shaman-EP-1SG.[NOM]</ta>
            <ta e="T274" id="Seg_3552" s="T273">make-PST2.NEG-EP-1SG</ta>
            <ta e="T275" id="Seg_3553" s="T274">why</ta>
            <ta e="T276" id="Seg_3554" s="T275">EMPH</ta>
            <ta e="T277" id="Seg_3555" s="T276">prince-EP-2SG.[NOM]</ta>
            <ta e="T278" id="Seg_3556" s="T277">MOD</ta>
            <ta e="T279" id="Seg_3557" s="T278">stupid.[NOM]=Q</ta>
            <ta e="T280" id="Seg_3558" s="T279">well</ta>
            <ta e="T281" id="Seg_3559" s="T280">self-3SG-GEN</ta>
            <ta e="T282" id="Seg_3560" s="T281">people-3SG-DAT/LOC</ta>
            <ta e="T283" id="Seg_3561" s="T282">calamity-ACC</ta>
            <ta e="T284" id="Seg_3562" s="T283">make-INFER-3SG</ta>
            <ta e="T285" id="Seg_3563" s="T284">people-2PL-ABL</ta>
            <ta e="T286" id="Seg_3564" s="T285">alone</ta>
            <ta e="T287" id="Seg_3565" s="T286">2SG.[NOM]</ta>
            <ta e="T288" id="Seg_3566" s="T287">son-2SG-ACC</ta>
            <ta e="T289" id="Seg_3567" s="T288">together.with</ta>
            <ta e="T290" id="Seg_3568" s="T289">remain-FUT-2PL</ta>
            <ta e="T291" id="Seg_3569" s="T290">what.[NOM]</ta>
            <ta e="T292" id="Seg_3570" s="T291">NEG</ta>
            <ta e="T293" id="Seg_3571" s="T292">alive.[NOM]</ta>
            <ta e="T294" id="Seg_3572" s="T293">stay-FUT.[3SG]</ta>
            <ta e="T295" id="Seg_3573" s="T294">NEG-3SG</ta>
            <ta e="T296" id="Seg_3574" s="T295">different</ta>
            <ta e="T297" id="Seg_3575" s="T296">place-ABL</ta>
            <ta e="T298" id="Seg_3576" s="T297">people.[NOM]</ta>
            <ta e="T299" id="Seg_3577" s="T298">come-CVB.SEQ-3PL</ta>
            <ta e="T300" id="Seg_3578" s="T299">that-ABL</ta>
            <ta e="T301" id="Seg_3579" s="T300">new</ta>
            <ta e="T302" id="Seg_3580" s="T301">people.[NOM]</ta>
            <ta e="T303" id="Seg_3581" s="T302">arise-FUT-3SG</ta>
            <ta e="T304" id="Seg_3582" s="T303">say-PRS.[3SG]</ta>
            <ta e="T305" id="Seg_3583" s="T304">new</ta>
            <ta e="T306" id="Seg_3584" s="T305">clean</ta>
            <ta e="T307" id="Seg_3585" s="T306">white</ta>
            <ta e="T308" id="Seg_3586" s="T307">leather.cover.for.a.tent-PART</ta>
            <ta e="T309" id="Seg_3587" s="T308">now</ta>
            <ta e="T310" id="Seg_3588" s="T309">three</ta>
            <ta e="T311" id="Seg_3589" s="T310">day.and.night-3SG-INSTR</ta>
            <ta e="T312" id="Seg_3590" s="T311">morning</ta>
            <ta e="T313" id="Seg_3591" s="T312">place-IMP.3PL</ta>
            <ta e="T314" id="Seg_3592" s="T313">colourful</ta>
            <ta e="T315" id="Seg_3593" s="T314">white</ta>
            <ta e="T316" id="Seg_3594" s="T315">reindeer-ACC</ta>
            <ta e="T317" id="Seg_3595" s="T316">kill-IMP.3PL</ta>
            <ta e="T318" id="Seg_3596" s="T317">that</ta>
            <ta e="T319" id="Seg_3597" s="T318">pole.tent.[NOM]</ta>
            <ta e="T320" id="Seg_3598" s="T319">surrounding-3SG-ACC</ta>
            <ta e="T321" id="Seg_3599" s="T320">every-3SG-ACC</ta>
            <ta e="T322" id="Seg_3600" s="T321">white</ta>
            <ta e="T323" id="Seg_3601" s="T322">reindeer.[NOM]</ta>
            <ta e="T324" id="Seg_3602" s="T323">skin-3SG-INSTR</ta>
            <ta e="T325" id="Seg_3603" s="T324">spread-IMP.3PL</ta>
            <ta e="T326" id="Seg_3604" s="T325">this-INSTR</ta>
            <ta e="T327" id="Seg_3605" s="T326">step.out-CVB.SEQ</ta>
            <ta e="T328" id="Seg_3606" s="T327">go.in-FUT-3SG</ta>
            <ta e="T329" id="Seg_3607" s="T328">prince-2PL.[NOM]</ta>
            <ta e="T330" id="Seg_3608" s="T329">dirty</ta>
            <ta e="T331" id="Seg_3609" s="T330">what.[NOM]</ta>
            <ta e="T332" id="Seg_3610" s="T331">NEG</ta>
            <ta e="T333" id="Seg_3611" s="T332">stick-FUT.[3SG]</ta>
            <ta e="T334" id="Seg_3612" s="T333">NEG-NEC.[3SG]</ta>
            <ta e="T335" id="Seg_3613" s="T334">say-PRS.[3SG]</ta>
            <ta e="T336" id="Seg_3614" s="T335">people-PL.[NOM]</ta>
            <ta e="T337" id="Seg_3615" s="T336">wedding.[NOM]</ta>
            <ta e="T338" id="Seg_3616" s="T337">think-CVB.SEQ</ta>
            <ta e="T339" id="Seg_3617" s="T338">come-NEG-IMP.3PL</ta>
            <ta e="T340" id="Seg_3618" s="T339">who.[NOM]</ta>
            <ta e="T341" id="Seg_3619" s="T340">soul-3SG-ACC</ta>
            <ta e="T342" id="Seg_3620" s="T341">preserve-CVB.PURP</ta>
            <ta e="T343" id="Seg_3621" s="T342">want-PRS.[3SG]</ta>
            <ta e="T344" id="Seg_3622" s="T343">far.away</ta>
            <ta e="T345" id="Seg_3623" s="T344">go-IMP.3SG</ta>
            <ta e="T346" id="Seg_3624" s="T345">say-PST2.[3SG]</ta>
            <ta e="T347" id="Seg_3625" s="T346">three-ORD</ta>
            <ta e="T348" id="Seg_3626" s="T347">day-3SG-DAT/LOC</ta>
            <ta e="T349" id="Seg_3627" s="T348">speak-PTCP.PST-3SG-ACC</ta>
            <ta e="T350" id="Seg_3628" s="T349">like</ta>
            <ta e="T351" id="Seg_3629" s="T350">make-PST2-3PL</ta>
            <ta e="T352" id="Seg_3630" s="T351">here</ta>
            <ta e="T353" id="Seg_3631" s="T352">shaman.[NOM]</ta>
            <ta e="T354" id="Seg_3632" s="T353">see-CVB.SEQ</ta>
            <ta e="T355" id="Seg_3633" s="T354">stand-PST2.[3SG]</ta>
            <ta e="T356" id="Seg_3634" s="T355">very.big</ta>
            <ta e="T357" id="Seg_3635" s="T356">very</ta>
            <ta e="T358" id="Seg_3636" s="T357">whirlwind.[NOM]</ta>
            <ta e="T359" id="Seg_3637" s="T358">wind.[NOM]</ta>
            <ta e="T360" id="Seg_3638" s="T359">suddenly.begin-PST2.[3SG]</ta>
            <ta e="T361" id="Seg_3639" s="T360">bell.[NOM]</ta>
            <ta e="T362" id="Seg_3640" s="T361">sound-3SG.[NOM]</ta>
            <ta e="T363" id="Seg_3641" s="T362">say-CVB.SEQ</ta>
            <ta e="T364" id="Seg_3642" s="T363">very</ta>
            <ta e="T365" id="Seg_3643" s="T364">be.heard-EP-PST2.[3SG]</ta>
            <ta e="T366" id="Seg_3644" s="T365">what.[NOM]</ta>
            <ta e="T367" id="Seg_3645" s="T366">just</ta>
            <ta e="T368" id="Seg_3646" s="T367">wind.[NOM]</ta>
            <ta e="T369" id="Seg_3647" s="T368">like</ta>
            <ta e="T370" id="Seg_3648" s="T369">pole.tent.[NOM]</ta>
            <ta e="T371" id="Seg_3649" s="T370">door-3SG-ACC</ta>
            <ta e="T372" id="Seg_3650" s="T371">open-PTCP.PRS-DAT/LOC</ta>
            <ta e="T373" id="Seg_3651" s="T372">like</ta>
            <ta e="T374" id="Seg_3652" s="T373">make-PST2.[3SG]</ta>
            <ta e="T375" id="Seg_3653" s="T374">that.[NOM]</ta>
            <ta e="T376" id="Seg_3654" s="T375">after</ta>
            <ta e="T377" id="Seg_3655" s="T376">wind-POSS</ta>
            <ta e="T378" id="Seg_3656" s="T377">NEG</ta>
            <ta e="T379" id="Seg_3657" s="T378">become-PST2.[3SG]</ta>
            <ta e="T380" id="Seg_3658" s="T379">this-ACC</ta>
            <ta e="T381" id="Seg_3659" s="T380">shaman.[NOM]</ta>
            <ta e="T382" id="Seg_3660" s="T381">go-CVB.SEQ</ta>
            <ta e="T383" id="Seg_3661" s="T382">see-PST2-3SG</ta>
            <ta e="T384" id="Seg_3662" s="T383">prince.[NOM]</ta>
            <ta e="T385" id="Seg_3663" s="T384">and</ta>
            <ta e="T386" id="Seg_3664" s="T385">every</ta>
            <ta e="T387" id="Seg_3665" s="T386">come-PTCP.PST</ta>
            <ta e="T388" id="Seg_3666" s="T387">and</ta>
            <ta e="T389" id="Seg_3667" s="T388">people.[NOM]</ta>
            <ta e="T390" id="Seg_3668" s="T389">die-CVB.SIM</ta>
            <ta e="T391" id="Seg_3669" s="T390">lie-PRS-3PL</ta>
            <ta e="T392" id="Seg_3670" s="T391">be-PST2.[3SG]</ta>
            <ta e="T393" id="Seg_3671" s="T392">this-INSTR</ta>
            <ta e="T394" id="Seg_3672" s="T393">Norilsk.[NOM]</ta>
            <ta e="T395" id="Seg_3673" s="T394">become.poor-PST1-3SG</ta>
            <ta e="T396" id="Seg_3674" s="T395">that.[NOM]</ta>
            <ta e="T397" id="Seg_3675" s="T396">Edyan.[NOM]</ta>
            <ta e="T398" id="Seg_3676" s="T397">diameter-ABL</ta>
            <ta e="T399" id="Seg_3677" s="T398">come-CVB.SEQ</ta>
            <ta e="T400" id="Seg_3678" s="T399">then</ta>
            <ta e="T401" id="Seg_3679" s="T400">just</ta>
            <ta e="T402" id="Seg_3680" s="T401">arise-PTCP.PRS</ta>
            <ta e="T403" id="Seg_3681" s="T402">be-PST1-3PL</ta>
         </annotation>
         <annotation name="gg" tierref="gg">
            <ta e="T1" id="Seg_3682" s="T0">vor.langer.Zeit</ta>
            <ta e="T2" id="Seg_3683" s="T1">Norilsker-DAT/LOC</ta>
            <ta e="T3" id="Seg_3684" s="T2">Fürst-PROPR-3PL</ta>
            <ta e="T4" id="Seg_3685" s="T3">man.sagt</ta>
            <ta e="T5" id="Seg_3686" s="T4">jung</ta>
            <ta e="T6" id="Seg_3687" s="T5">Mensch.[NOM]</ta>
            <ta e="T7" id="Seg_3688" s="T6">dieses</ta>
            <ta e="T8" id="Seg_3689" s="T7">Mensch-3PL.[NOM]</ta>
            <ta e="T9" id="Seg_3690" s="T8">Frau-POSS</ta>
            <ta e="T10" id="Seg_3691" s="T9">NEG.[3SG]</ta>
            <ta e="T11" id="Seg_3692" s="T10">dieses-ACC</ta>
            <ta e="T12" id="Seg_3693" s="T11">Familie-PL-3SG.[NOM]</ta>
            <ta e="T13" id="Seg_3694" s="T12">einmal</ta>
            <ta e="T14" id="Seg_3695" s="T13">fragen-PRS-3PL</ta>
            <ta e="T15" id="Seg_3696" s="T14">warum</ta>
            <ta e="T16" id="Seg_3697" s="T15">Frau-VBZ-NEG-2SG</ta>
            <ta e="T17" id="Seg_3698" s="T16">sagen-CVB.SEQ</ta>
            <ta e="T18" id="Seg_3699" s="T17">jenes-DAT/LOC</ta>
            <ta e="T19" id="Seg_3700" s="T18">sprechen-PRS.[3SG]</ta>
            <ta e="T20" id="Seg_3701" s="T19">1SG.[NOM]</ta>
            <ta e="T21" id="Seg_3702" s="T20">dieses</ta>
            <ta e="T22" id="Seg_3703" s="T21">Land.[NOM]</ta>
            <ta e="T23" id="Seg_3704" s="T22">Mädchen-3SG-ACC</ta>
            <ta e="T24" id="Seg_3705" s="T23">nehmen-NEG.PTCP</ta>
            <ta e="T25" id="Seg_3706" s="T24">Mensch-1SG</ta>
            <ta e="T26" id="Seg_3707" s="T25">oben</ta>
            <ta e="T27" id="Seg_3708" s="T26">Ürüng.Ajyy.[NOM]</ta>
            <ta e="T28" id="Seg_3709" s="T27">sagen-CVB.SEQ</ta>
            <ta e="T29" id="Seg_3710" s="T28">es.gibt</ta>
            <ta e="T30" id="Seg_3711" s="T29">jenes.[NOM]</ta>
            <ta e="T31" id="Seg_3712" s="T30">Tochter-3SG-ACC</ta>
            <ta e="T32" id="Seg_3713" s="T31">nur</ta>
            <ta e="T33" id="Seg_3714" s="T32">nehmen-TEMP-1SG</ta>
            <ta e="T34" id="Seg_3715" s="T33">dann</ta>
            <ta e="T35" id="Seg_3716" s="T34">einverstanden.sein-EP-RECP/COLL-PTCP.FUT</ta>
            <ta e="T36" id="Seg_3717" s="T35">sein-PST1-1SG</ta>
            <ta e="T37" id="Seg_3718" s="T36">sagen-PRS.[3SG]</ta>
            <ta e="T38" id="Seg_3719" s="T37">dieses-ACC</ta>
            <ta e="T39" id="Seg_3720" s="T38">Leute-PL.[NOM]</ta>
            <ta e="T40" id="Seg_3721" s="T39">spielen-CVB.SEQ</ta>
            <ta e="T41" id="Seg_3722" s="T40">sprechen-INFER-3SG</ta>
            <ta e="T42" id="Seg_3723" s="T41">denken-CVB.SEQ</ta>
            <ta e="T43" id="Seg_3724" s="T42">werfen-PRS-3PL</ta>
            <ta e="T44" id="Seg_3725" s="T43">einmal</ta>
            <ta e="T45" id="Seg_3726" s="T44">Versammlung-DAT/LOC</ta>
            <ta e="T46" id="Seg_3727" s="T45">Fürst.[NOM]</ta>
            <ta e="T47" id="Seg_3728" s="T46">Schamane-ACC</ta>
            <ta e="T48" id="Seg_3729" s="T47">zwingen-PRS.[3SG]</ta>
            <ta e="T49" id="Seg_3730" s="T48">2SG.[NOM]</ta>
            <ta e="T50" id="Seg_3731" s="T49">1SG-DAT/LOC</ta>
            <ta e="T51" id="Seg_3732" s="T50">Himmel-DAT/LOC</ta>
            <ta e="T52" id="Seg_3733" s="T51">es.gibt</ta>
            <ta e="T53" id="Seg_3734" s="T52">Ürüng.Ajyy.[NOM]</ta>
            <ta e="T54" id="Seg_3735" s="T53">Tochter-3SG-ACC</ta>
            <ta e="T55" id="Seg_3736" s="T54">Frau.[NOM]</ta>
            <ta e="T56" id="Seg_3737" s="T55">machen-PTCP.FUT-1SG-ACC</ta>
            <ta e="T57" id="Seg_3738" s="T56">Brautwerbung-VBZ.[IMP.2SG]</ta>
            <ta e="T58" id="Seg_3739" s="T57">sagen-PRS.[3SG]</ta>
            <ta e="T59" id="Seg_3740" s="T58">dieses-ACC</ta>
            <ta e="T60" id="Seg_3741" s="T59">Schamane-3SG.[NOM]</ta>
            <ta e="T61" id="Seg_3742" s="T60">etwas.Angst.haben-CVB.SIM</ta>
            <ta e="T62" id="Seg_3743" s="T61">hören-PRS.[3SG]</ta>
            <ta e="T63" id="Seg_3744" s="T62">was-2SG.[NOM]</ta>
            <ta e="T64" id="Seg_3745" s="T63">Wort-3SG.[NOM]=Q</ta>
            <ta e="T65" id="Seg_3746" s="T64">wie</ta>
            <ta e="T66" id="Seg_3747" s="T65">NEG</ta>
            <ta e="T67" id="Seg_3748" s="T66">jenes</ta>
            <ta e="T68" id="Seg_3749" s="T67">Land-ABL</ta>
            <ta e="T69" id="Seg_3750" s="T68">können-CVB.SEQ</ta>
            <ta e="T70" id="Seg_3751" s="T69">bringen-NEG.PTCP</ta>
            <ta e="T71" id="Seg_3752" s="T70">Sache-EP-1SG.[NOM]</ta>
            <ta e="T72" id="Seg_3753" s="T71">sagen-PRS.[3SG]</ta>
            <ta e="T73" id="Seg_3754" s="T72">dieses-ACC</ta>
            <ta e="T74" id="Seg_3755" s="T73">Fürst.[NOM]</ta>
            <ta e="T75" id="Seg_3756" s="T74">sieben</ta>
            <ta e="T76" id="Seg_3757" s="T75">Weidenrute-ACC</ta>
            <ta e="T77" id="Seg_3758" s="T76">sehen-CAUS-PRS.[3SG]</ta>
            <ta e="T78" id="Seg_3759" s="T77">2SG.[NOM]</ta>
            <ta e="T79" id="Seg_3760" s="T78">Betrug-VBZ-EP-NEG.[IMP.2SG]</ta>
            <ta e="T80" id="Seg_3761" s="T79">1SG.[NOM]</ta>
            <ta e="T81" id="Seg_3762" s="T80">2SG-ACC</ta>
            <ta e="T82" id="Seg_3763" s="T81">unterer.Teil.des.Rückens-2SG-ACC</ta>
            <ta e="T83" id="Seg_3764" s="T82">Haut.abziehen-FUT-1SG</ta>
            <ta e="T84" id="Seg_3765" s="T83">Tag.[NOM]</ta>
            <ta e="T85" id="Seg_3766" s="T84">morgen</ta>
            <ta e="T86" id="Seg_3767" s="T85">Ürüng.Ajyy.[NOM]</ta>
            <ta e="T87" id="Seg_3768" s="T86">Tochter-3SG-ACC</ta>
            <ta e="T88" id="Seg_3769" s="T87">es.gibt</ta>
            <ta e="T89" id="Seg_3770" s="T88">machen.[IMP.2SG]</ta>
            <ta e="T90" id="Seg_3771" s="T89">sagen-PRS.[3SG]</ta>
            <ta e="T91" id="Seg_3772" s="T90">Schamane.[NOM]</ta>
            <ta e="T92" id="Seg_3773" s="T91">zu.Ende.gehen-FUT.[3SG]</ta>
            <ta e="T93" id="Seg_3774" s="T92">MOD</ta>
            <ta e="T94" id="Seg_3775" s="T93">doch</ta>
            <ta e="T95" id="Seg_3776" s="T94">schamanisieren-CVB.SEQ</ta>
            <ta e="T96" id="Seg_3777" s="T95">gehen-PRS.[3SG]</ta>
            <ta e="T97" id="Seg_3778" s="T96">dieses</ta>
            <ta e="T98" id="Seg_3779" s="T97">Schamane.[NOM]</ta>
            <ta e="T99" id="Seg_3780" s="T98">sich.nähern-PTCP.PST-3SG-ACC</ta>
            <ta e="T100" id="Seg_3781" s="T99">hören-CVB.SEQ</ta>
            <ta e="T101" id="Seg_3782" s="T100">Ürüng.Ajyy.[NOM]</ta>
            <ta e="T102" id="Seg_3783" s="T101">Herr.[NOM]</ta>
            <ta e="T103" id="Seg_3784" s="T102">sehr-ADVZ</ta>
            <ta e="T104" id="Seg_3785" s="T103">böse.werden-PRS.[3SG]</ta>
            <ta e="T105" id="Seg_3786" s="T104">was.[NOM]</ta>
            <ta e="T106" id="Seg_3787" s="T105">sterben-PTCP.PRS</ta>
            <ta e="T107" id="Seg_3788" s="T106">untere.Welt.[NOM]</ta>
            <ta e="T108" id="Seg_3789" s="T107">Schamane-3SG.[NOM]</ta>
            <ta e="T109" id="Seg_3790" s="T108">Anordnung-POSS</ta>
            <ta e="T110" id="Seg_3791" s="T109">NEG</ta>
            <ta e="T111" id="Seg_3792" s="T110">kommen-INFER-3SG=Q</ta>
            <ta e="T112" id="Seg_3793" s="T111">bringen-EP-IMP.2PL</ta>
            <ta e="T113" id="Seg_3794" s="T112">1SG-DAT/LOC</ta>
            <ta e="T114" id="Seg_3795" s="T113">Feuer.[NOM]</ta>
            <ta e="T115" id="Seg_3796" s="T114">Schwert-1SG-ACC</ta>
            <ta e="T116" id="Seg_3797" s="T115">Kopf-ACC</ta>
            <ta e="T117" id="Seg_3798" s="T116">schneiden-CVB.SIM</ta>
            <ta e="T118" id="Seg_3799" s="T117">schlagen-PTCP.FUT-1SG-ACC</ta>
            <ta e="T119" id="Seg_3800" s="T118">sagen-PST2.[3SG]</ta>
            <ta e="T120" id="Seg_3801" s="T119">Schamane-3SG.[NOM]</ta>
            <ta e="T121" id="Seg_3802" s="T120">kommen-PST2-3SG</ta>
            <ta e="T122" id="Seg_3803" s="T121">sich.bereit.machen-CVB.SEQ</ta>
            <ta e="T123" id="Seg_3804" s="T122">sitzen-PTCP.PRS</ta>
            <ta e="T124" id="Seg_3805" s="T123">sein-PST2.[3SG]</ta>
            <ta e="T125" id="Seg_3806" s="T124">Ajyy-3SG.[NOM]</ta>
            <ta e="T126" id="Seg_3807" s="T125">dieses-ACC</ta>
            <ta e="T127" id="Seg_3808" s="T126">doch</ta>
            <ta e="T128" id="Seg_3809" s="T127">kriechen-CVB.SEQ</ta>
            <ta e="T129" id="Seg_3810" s="T128">bitten-EP-MED-PRS.[3SG]</ta>
            <ta e="T130" id="Seg_3811" s="T129">1SG.[NOM]</ta>
            <ta e="T131" id="Seg_3812" s="T130">Bedarf-ABL</ta>
            <ta e="T132" id="Seg_3813" s="T131">kommen-PST1-1SG</ta>
            <ta e="T133" id="Seg_3814" s="T132">2SG.[NOM]</ta>
            <ta e="T134" id="Seg_3815" s="T133">wie</ta>
            <ta e="T135" id="Seg_3816" s="T134">Erde-1PL-DAT/LOC</ta>
            <ta e="T136" id="Seg_3817" s="T135">Anführer-PROPR.[NOM]</ta>
            <ta e="T137" id="Seg_3818" s="T136">sein-PRS-1PL</ta>
            <ta e="T138" id="Seg_3819" s="T137">Fürst.[NOM]</ta>
            <ta e="T139" id="Seg_3820" s="T138">sagen-CVB.SEQ</ta>
            <ta e="T140" id="Seg_3821" s="T139">jenes.[NOM]</ta>
            <ta e="T141" id="Seg_3822" s="T140">zwingen-NMNZ-3SG-ABL</ta>
            <ta e="T142" id="Seg_3823" s="T141">kommen-PST1-1SG</ta>
            <ta e="T143" id="Seg_3824" s="T142">sagen-CVB.SEQ</ta>
            <ta e="T144" id="Seg_3825" s="T143">sich.beklagen-CVB.SEQ</ta>
            <ta e="T145" id="Seg_3826" s="T144">Bedarf-3SG-ACC</ta>
            <ta e="T146" id="Seg_3827" s="T145">erzählen-PRS.[3SG]</ta>
            <ta e="T147" id="Seg_3828" s="T146">jenes-DAT/LOC</ta>
            <ta e="T148" id="Seg_3829" s="T147">Ürüng.Ajyy-3SG.[NOM]</ta>
            <ta e="T149" id="Seg_3830" s="T148">sprechen-PRS.[3SG]</ta>
            <ta e="T150" id="Seg_3831" s="T149">nein</ta>
            <ta e="T151" id="Seg_3832" s="T150">enden.[IMP.2SG]</ta>
            <ta e="T152" id="Seg_3833" s="T151">sprechen.[IMP.2SG]</ta>
            <ta e="T153" id="Seg_3834" s="T152">sagen-CVB.SEQ</ta>
            <ta e="T154" id="Seg_3835" s="T153">zurückkommen-EP-CAUS-PRS.[3SG]</ta>
            <ta e="T155" id="Seg_3836" s="T154">Schamane-3SG-ACC</ta>
            <ta e="T156" id="Seg_3837" s="T155">dieses-3SG.[NOM]</ta>
            <ta e="T157" id="Seg_3838" s="T156">schamanisieren-CVB.SEQ</ta>
            <ta e="T158" id="Seg_3839" s="T157">aufhören-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T159" id="Seg_3840" s="T158">Fürst-3SG.[NOM]</ta>
            <ta e="T160" id="Seg_3841" s="T159">zwingen-PRS.[3SG]</ta>
            <ta e="T161" id="Seg_3842" s="T160">2SG.[NOM]</ta>
            <ta e="T162" id="Seg_3843" s="T161">vortäuschen-CVB.SEQ</ta>
            <ta e="T163" id="Seg_3844" s="T162">dorthin-hierher</ta>
            <ta e="T164" id="Seg_3845" s="T163">gehen-PTCP.PST</ta>
            <ta e="T165" id="Seg_3846" s="T164">sein-PRS-2SG</ta>
            <ta e="T166" id="Seg_3847" s="T165">Opfer.[NOM]</ta>
            <ta e="T167" id="Seg_3848" s="T166">Polarfuchs-2SG.[NOM]</ta>
            <ta e="T168" id="Seg_3849" s="T167">hier</ta>
            <ta e="T169" id="Seg_3850" s="T168">stehen-PTCP.PRS</ta>
            <ta e="T170" id="Seg_3851" s="T169">sein-NEG.[3SG]</ta>
            <ta e="T171" id="Seg_3852" s="T170">Q</ta>
            <ta e="T172" id="Seg_3853" s="T171">sagen-CVB.SEQ</ta>
            <ta e="T173" id="Seg_3854" s="T172">zwingen-PRS.[3SG]</ta>
            <ta e="T174" id="Seg_3855" s="T173">unbedingt</ta>
            <ta e="T175" id="Seg_3856" s="T174">1SG-DAT/LOC</ta>
            <ta e="T176" id="Seg_3857" s="T175">Tochter-1SG-ACC</ta>
            <ta e="T177" id="Seg_3858" s="T176">holen-CVB.SEQ</ta>
            <ta e="T178" id="Seg_3859" s="T177">geben.[IMP.2SG]</ta>
            <ta e="T179" id="Seg_3860" s="T178">sagen-PRS.[3SG]</ta>
            <ta e="T180" id="Seg_3861" s="T179">doch</ta>
            <ta e="T181" id="Seg_3862" s="T180">wieder</ta>
            <ta e="T182" id="Seg_3863" s="T181">dieses</ta>
            <ta e="T183" id="Seg_3864" s="T182">Schamane.[NOM]</ta>
            <ta e="T184" id="Seg_3865" s="T183">am.Morgen</ta>
            <ta e="T185" id="Seg_3866" s="T184">schamanisieren-CVB.SEQ</ta>
            <ta e="T186" id="Seg_3867" s="T185">Unglück-VBZ-MED-PRS.[3SG]</ta>
            <ta e="T187" id="Seg_3868" s="T186">dieses</ta>
            <ta e="T188" id="Seg_3869" s="T187">schamanisieren-CVB.SEQ</ta>
            <ta e="T189" id="Seg_3870" s="T188">ankommen-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T190" id="Seg_3871" s="T189">was-ABL</ta>
            <ta e="T191" id="Seg_3872" s="T190">und</ta>
            <ta e="T192" id="Seg_3873" s="T191">Grauen-VBZ-CVB.SEQ</ta>
            <ta e="T193" id="Seg_3874" s="T192">Ürüng.Ajyy-3SG.[NOM]</ta>
            <ta e="T194" id="Seg_3875" s="T193">Schamane-3SG-ACC</ta>
            <ta e="T195" id="Seg_3876" s="T194">sich.ärgern-PRS.[3SG]</ta>
            <ta e="T196" id="Seg_3877" s="T195">dieses-DAT/LOC</ta>
            <ta e="T197" id="Seg_3878" s="T196">wieder</ta>
            <ta e="T198" id="Seg_3879" s="T197">Schamane-3SG.[NOM]</ta>
            <ta e="T199" id="Seg_3880" s="T198">weinen-EP-MED-CVB.SEQ</ta>
            <ta e="T200" id="Seg_3881" s="T199">leiden-PRS.[3SG]</ta>
            <ta e="T201" id="Seg_3882" s="T200">wenn.auch</ta>
            <ta e="T202" id="Seg_3883" s="T201">Kennzeichen-PART</ta>
            <ta e="T203" id="Seg_3884" s="T202">geben.[IMP.2SG]</ta>
            <ta e="T204" id="Seg_3885" s="T203">sagen-CVB.SEQ</ta>
            <ta e="T205" id="Seg_3886" s="T204">nein</ta>
            <ta e="T206" id="Seg_3887" s="T205">geben-FUT-1SG</ta>
            <ta e="T207" id="Seg_3888" s="T206">NEG-3SG</ta>
            <ta e="T208" id="Seg_3889" s="T207">warum</ta>
            <ta e="T209" id="Seg_3890" s="T208">dumm.[NOM]=Q</ta>
            <ta e="T210" id="Seg_3891" s="T209">quer.über</ta>
            <ta e="T211" id="Seg_3892" s="T210">Auge-PROPR.[NOM]</ta>
            <ta e="T212" id="Seg_3893" s="T211">besser</ta>
            <ta e="T213" id="Seg_3894" s="T212">2SG.[NOM]</ta>
            <ta e="T214" id="Seg_3895" s="T213">sterben-INFER-EP-2SG</ta>
            <ta e="T215" id="Seg_3896" s="T214">sagen-CVB.SEQ</ta>
            <ta e="T216" id="Seg_3897" s="T215">Schamane-3SG-ACC</ta>
            <ta e="T217" id="Seg_3898" s="T216">wieder</ta>
            <ta e="T218" id="Seg_3899" s="T217">zurückkommen-EP-CAUS-PRS.[3SG]</ta>
            <ta e="T219" id="Seg_3900" s="T218">schamanisieren-CVB.SEQ</ta>
            <ta e="T220" id="Seg_3901" s="T219">aufhören-CVB.SEQ</ta>
            <ta e="T221" id="Seg_3902" s="T220">nachdem</ta>
            <ta e="T222" id="Seg_3903" s="T221">Schamane.[NOM]</ta>
            <ta e="T223" id="Seg_3904" s="T222">Fürst-ABL</ta>
            <ta e="T224" id="Seg_3905" s="T223">anflehen-PRS.[3SG]</ta>
            <ta e="T225" id="Seg_3906" s="T224">was</ta>
            <ta e="T226" id="Seg_3907" s="T225">NEG</ta>
            <ta e="T227" id="Seg_3908" s="T226">ganz</ta>
            <ta e="T228" id="Seg_3909" s="T227">drei</ta>
            <ta e="T229" id="Seg_3910" s="T228">Mal-PROPR.[NOM]</ta>
            <ta e="T230" id="Seg_3911" s="T229">töten-EP-NEG.[IMP.2SG]</ta>
            <ta e="T231" id="Seg_3912" s="T230">Wunsch-1SG-ACC</ta>
            <ta e="T232" id="Seg_3913" s="T231">erreichen-NEG-PST1-3SG</ta>
            <ta e="T233" id="Seg_3914" s="T232">sagen-CVB.SEQ</ta>
            <ta e="T234" id="Seg_3915" s="T233">morgen</ta>
            <ta e="T235" id="Seg_3916" s="T234">wieder</ta>
            <ta e="T236" id="Seg_3917" s="T235">quälen-EP-RECP/COLL-CVB.SEQ</ta>
            <ta e="T237" id="Seg_3918" s="T236">versuchen-FUT-1SG</ta>
            <ta e="T238" id="Seg_3919" s="T237">sagen-PRS.[3SG]</ta>
            <ta e="T239" id="Seg_3920" s="T238">nächster.Morgen-3SG-DAT/LOC</ta>
            <ta e="T240" id="Seg_3921" s="T239">drei-ORD</ta>
            <ta e="T241" id="Seg_3922" s="T240">schamanisieren-NMNZ-3SG-ACC</ta>
            <ta e="T242" id="Seg_3923" s="T241">schamanisieren-PRS.[3SG]</ta>
            <ta e="T243" id="Seg_3924" s="T242">dieses</ta>
            <ta e="T244" id="Seg_3925" s="T243">schamanisieren-CVB.SEQ</ta>
            <ta e="T245" id="Seg_3926" s="T244">Ürüng.Ajyy.[NOM]</ta>
            <ta e="T246" id="Seg_3927" s="T245">Haus-3SG-DAT/LOC</ta>
            <ta e="T247" id="Seg_3928" s="T246">ankommen-PRS.[3SG]</ta>
            <ta e="T248" id="Seg_3929" s="T247">dieses</ta>
            <ta e="T249" id="Seg_3930" s="T248">ankommen-CVB.SEQ</ta>
            <ta e="T250" id="Seg_3931" s="T249">sich.verbeugen-CVB.SEQ</ta>
            <ta e="T251" id="Seg_3932" s="T250">Zelt-3SG-GEN</ta>
            <ta e="T252" id="Seg_3933" s="T251">Hauptstange.des.Zeltes-3SG-GEN</ta>
            <ta e="T253" id="Seg_3934" s="T252">Herr-3SG-DAT/LOC</ta>
            <ta e="T254" id="Seg_3935" s="T253">anflehen-CVB.SEQ</ta>
            <ta e="T255" id="Seg_3936" s="T254">Ürüng.Ajyy-3SG-GEN</ta>
            <ta e="T256" id="Seg_3937" s="T255">Fuß-3SG-DAT/LOC</ta>
            <ta e="T257" id="Seg_3938" s="T256">kriechen-CVB.SEQ</ta>
            <ta e="T258" id="Seg_3939" s="T257">kommen-CVB.SEQ</ta>
            <ta e="T259" id="Seg_3940" s="T258">dieses</ta>
            <ta e="T260" id="Seg_3941" s="T259">vorheriger-3SG-COMP</ta>
            <ta e="T261" id="Seg_3942" s="T260">sehr-ADVZ</ta>
            <ta e="T262" id="Seg_3943" s="T261">sich.beklagen-PRS.[3SG]-weinen-PRS.[3SG]</ta>
            <ta e="T263" id="Seg_3944" s="T262">dieses-ACC</ta>
            <ta e="T264" id="Seg_3945" s="T263">doch</ta>
            <ta e="T265" id="Seg_3946" s="T264">Herz-3SG-INSTR</ta>
            <ta e="T266" id="Seg_3947" s="T265">bemitleiden-PRS.[3SG]</ta>
            <ta e="T267" id="Seg_3948" s="T266">doch</ta>
            <ta e="T268" id="Seg_3949" s="T267">Wahrheit.[NOM]</ta>
            <ta e="T269" id="Seg_3950" s="T268">1SG.[NOM]</ta>
            <ta e="T270" id="Seg_3951" s="T269">2SG-ACC</ta>
            <ta e="T271" id="Seg_3952" s="T270">töten-CVB.PURP</ta>
            <ta e="T272" id="Seg_3953" s="T271">selbst-1SG.[NOM]</ta>
            <ta e="T273" id="Seg_3954" s="T272">Schamane-EP-1SG.[NOM]</ta>
            <ta e="T274" id="Seg_3955" s="T273">machen-PST2.NEG-EP-1SG</ta>
            <ta e="T275" id="Seg_3956" s="T274">warum</ta>
            <ta e="T276" id="Seg_3957" s="T275">EMPH</ta>
            <ta e="T277" id="Seg_3958" s="T276">Fürst-EP-2SG.[NOM]</ta>
            <ta e="T278" id="Seg_3959" s="T277">MOD</ta>
            <ta e="T279" id="Seg_3960" s="T278">dumm.[NOM]=Q</ta>
            <ta e="T280" id="Seg_3961" s="T279">doch</ta>
            <ta e="T281" id="Seg_3962" s="T280">selbst-3SG-GEN</ta>
            <ta e="T282" id="Seg_3963" s="T281">Volk-3SG-DAT/LOC</ta>
            <ta e="T283" id="Seg_3964" s="T282">Unglück-ACC</ta>
            <ta e="T284" id="Seg_3965" s="T283">machen-INFER-3SG</ta>
            <ta e="T285" id="Seg_3966" s="T284">Volk-2PL-ABL</ta>
            <ta e="T286" id="Seg_3967" s="T285">alleine</ta>
            <ta e="T287" id="Seg_3968" s="T286">2SG.[NOM]</ta>
            <ta e="T288" id="Seg_3969" s="T287">Sohn-2SG-ACC</ta>
            <ta e="T289" id="Seg_3970" s="T288">zusammen.mit</ta>
            <ta e="T290" id="Seg_3971" s="T289">übrig.bleiben-FUT-2PL</ta>
            <ta e="T291" id="Seg_3972" s="T290">was.[NOM]</ta>
            <ta e="T292" id="Seg_3973" s="T291">NEG</ta>
            <ta e="T293" id="Seg_3974" s="T292">lebendig.[NOM]</ta>
            <ta e="T294" id="Seg_3975" s="T293">bleiben-FUT.[3SG]</ta>
            <ta e="T295" id="Seg_3976" s="T294">NEG-3SG</ta>
            <ta e="T296" id="Seg_3977" s="T295">anders</ta>
            <ta e="T297" id="Seg_3978" s="T296">Ort-ABL</ta>
            <ta e="T298" id="Seg_3979" s="T297">Leute.[NOM]</ta>
            <ta e="T299" id="Seg_3980" s="T298">kommen-CVB.SEQ-3PL</ta>
            <ta e="T300" id="Seg_3981" s="T299">jenes-ABL</ta>
            <ta e="T301" id="Seg_3982" s="T300">neu</ta>
            <ta e="T302" id="Seg_3983" s="T301">Volk.[NOM]</ta>
            <ta e="T303" id="Seg_3984" s="T302">entstehen-FUT-3SG</ta>
            <ta e="T304" id="Seg_3985" s="T303">sagen-PRS.[3SG]</ta>
            <ta e="T305" id="Seg_3986" s="T304">neu</ta>
            <ta e="T306" id="Seg_3987" s="T305">sauber</ta>
            <ta e="T307" id="Seg_3988" s="T306">weiß</ta>
            <ta e="T308" id="Seg_3989" s="T307">Lederdecke.eines.Zeltes-PART</ta>
            <ta e="T309" id="Seg_3990" s="T308">jetzt</ta>
            <ta e="T310" id="Seg_3991" s="T309">drei</ta>
            <ta e="T311" id="Seg_3992" s="T310">Tag.und.Nacht-3SG-INSTR</ta>
            <ta e="T312" id="Seg_3993" s="T311">Morgen</ta>
            <ta e="T313" id="Seg_3994" s="T312">stellen-IMP.3PL</ta>
            <ta e="T314" id="Seg_3995" s="T313">bunt</ta>
            <ta e="T315" id="Seg_3996" s="T314">weiß</ta>
            <ta e="T316" id="Seg_3997" s="T315">Rentier-ACC</ta>
            <ta e="T317" id="Seg_3998" s="T316">töten-IMP.3PL</ta>
            <ta e="T318" id="Seg_3999" s="T317">jenes</ta>
            <ta e="T319" id="Seg_4000" s="T318">Stangenzelt.[NOM]</ta>
            <ta e="T320" id="Seg_4001" s="T319">Umgebung-3SG-ACC</ta>
            <ta e="T321" id="Seg_4002" s="T320">jeder-3SG-ACC</ta>
            <ta e="T322" id="Seg_4003" s="T321">weiß</ta>
            <ta e="T323" id="Seg_4004" s="T322">Rentier.[NOM]</ta>
            <ta e="T324" id="Seg_4005" s="T323">Haut-3SG-INSTR</ta>
            <ta e="T325" id="Seg_4006" s="T324">ausbreiten-IMP.3PL</ta>
            <ta e="T326" id="Seg_4007" s="T325">dieses-INSTR</ta>
            <ta e="T327" id="Seg_4008" s="T326">hervortreten-CVB.SEQ</ta>
            <ta e="T328" id="Seg_4009" s="T327">hineingehen-FUT-3SG</ta>
            <ta e="T329" id="Seg_4010" s="T328">Fürst-2PL.[NOM]</ta>
            <ta e="T330" id="Seg_4011" s="T329">schmutzig</ta>
            <ta e="T331" id="Seg_4012" s="T330">was.[NOM]</ta>
            <ta e="T332" id="Seg_4013" s="T331">NEG</ta>
            <ta e="T333" id="Seg_4014" s="T332">haften-FUT.[3SG]</ta>
            <ta e="T334" id="Seg_4015" s="T333">NEG-NEC.[3SG]</ta>
            <ta e="T335" id="Seg_4016" s="T334">sagen-PRS.[3SG]</ta>
            <ta e="T336" id="Seg_4017" s="T335">Leute-PL.[NOM]</ta>
            <ta e="T337" id="Seg_4018" s="T336">Hochzeit.[NOM]</ta>
            <ta e="T338" id="Seg_4019" s="T337">denken-CVB.SEQ</ta>
            <ta e="T339" id="Seg_4020" s="T338">kommen-NEG-IMP.3PL</ta>
            <ta e="T340" id="Seg_4021" s="T339">wer.[NOM]</ta>
            <ta e="T341" id="Seg_4022" s="T340">Seele-3SG-ACC</ta>
            <ta e="T342" id="Seg_4023" s="T341">bewahren-CVB.PURP</ta>
            <ta e="T343" id="Seg_4024" s="T342">wollen-PRS.[3SG]</ta>
            <ta e="T344" id="Seg_4025" s="T343">weit.weg</ta>
            <ta e="T345" id="Seg_4026" s="T344">gehen-IMP.3SG</ta>
            <ta e="T346" id="Seg_4027" s="T345">sagen-PST2.[3SG]</ta>
            <ta e="T347" id="Seg_4028" s="T346">drei-ORD</ta>
            <ta e="T348" id="Seg_4029" s="T347">Tag-3SG-DAT/LOC</ta>
            <ta e="T349" id="Seg_4030" s="T348">sprechen-PTCP.PST-3SG-ACC</ta>
            <ta e="T350" id="Seg_4031" s="T349">wie</ta>
            <ta e="T351" id="Seg_4032" s="T350">machen-PST2-3PL</ta>
            <ta e="T352" id="Seg_4033" s="T351">hier</ta>
            <ta e="T353" id="Seg_4034" s="T352">Schamane.[NOM]</ta>
            <ta e="T354" id="Seg_4035" s="T353">sehen-CVB.SEQ</ta>
            <ta e="T355" id="Seg_4036" s="T354">stehen-PST2.[3SG]</ta>
            <ta e="T356" id="Seg_4037" s="T355">riesig</ta>
            <ta e="T357" id="Seg_4038" s="T356">sehr</ta>
            <ta e="T358" id="Seg_4039" s="T357">Wirbelwind.[NOM]</ta>
            <ta e="T359" id="Seg_4040" s="T358">Wind.[NOM]</ta>
            <ta e="T360" id="Seg_4041" s="T359">plötzlich.anfangen-PST2.[3SG]</ta>
            <ta e="T361" id="Seg_4042" s="T360">Klingel.[NOM]</ta>
            <ta e="T362" id="Seg_4043" s="T361">Geräusch-3SG.[NOM]</ta>
            <ta e="T363" id="Seg_4044" s="T362">sagen-CVB.SEQ</ta>
            <ta e="T364" id="Seg_4045" s="T363">sehr</ta>
            <ta e="T365" id="Seg_4046" s="T364">gehört.werden-EP-PST2.[3SG]</ta>
            <ta e="T366" id="Seg_4047" s="T365">was.[NOM]</ta>
            <ta e="T367" id="Seg_4048" s="T366">nur</ta>
            <ta e="T368" id="Seg_4049" s="T367">Wind.[NOM]</ta>
            <ta e="T369" id="Seg_4050" s="T368">wie</ta>
            <ta e="T370" id="Seg_4051" s="T369">Stangenzelt.[NOM]</ta>
            <ta e="T371" id="Seg_4052" s="T370">Tür-3SG-ACC</ta>
            <ta e="T372" id="Seg_4053" s="T371">öffnen-PTCP.PRS-DAT/LOC</ta>
            <ta e="T373" id="Seg_4054" s="T372">so.wie</ta>
            <ta e="T374" id="Seg_4055" s="T373">machen-PST2.[3SG]</ta>
            <ta e="T375" id="Seg_4056" s="T374">jenes.[NOM]</ta>
            <ta e="T376" id="Seg_4057" s="T375">nach</ta>
            <ta e="T377" id="Seg_4058" s="T376">Wind-POSS</ta>
            <ta e="T378" id="Seg_4059" s="T377">NEG</ta>
            <ta e="T379" id="Seg_4060" s="T378">werden-PST2.[3SG]</ta>
            <ta e="T380" id="Seg_4061" s="T379">dieses-ACC</ta>
            <ta e="T381" id="Seg_4062" s="T380">Schamane.[NOM]</ta>
            <ta e="T382" id="Seg_4063" s="T381">gehen-CVB.SEQ</ta>
            <ta e="T383" id="Seg_4064" s="T382">sehen-PST2-3SG</ta>
            <ta e="T384" id="Seg_4065" s="T383">Fürst.[NOM]</ta>
            <ta e="T385" id="Seg_4066" s="T384">und</ta>
            <ta e="T386" id="Seg_4067" s="T385">jeder</ta>
            <ta e="T387" id="Seg_4068" s="T386">kommen-PTCP.PST</ta>
            <ta e="T388" id="Seg_4069" s="T387">und</ta>
            <ta e="T389" id="Seg_4070" s="T388">Leute.[NOM]</ta>
            <ta e="T390" id="Seg_4071" s="T389">sterben-CVB.SIM</ta>
            <ta e="T391" id="Seg_4072" s="T390">liegen-PRS-3PL</ta>
            <ta e="T392" id="Seg_4073" s="T391">sein-PST2.[3SG]</ta>
            <ta e="T393" id="Seg_4074" s="T392">dieses-INSTR</ta>
            <ta e="T394" id="Seg_4075" s="T393">Norilsker.[NOM]</ta>
            <ta e="T395" id="Seg_4076" s="T394">arm.werden-PST1-3SG</ta>
            <ta e="T396" id="Seg_4077" s="T395">dieses.[NOM]</ta>
            <ta e="T397" id="Seg_4078" s="T396">Edjan.[NOM]</ta>
            <ta e="T398" id="Seg_4079" s="T397">Durchmesser-ABL</ta>
            <ta e="T399" id="Seg_4080" s="T398">kommen-CVB.SEQ</ta>
            <ta e="T400" id="Seg_4081" s="T399">dann</ta>
            <ta e="T401" id="Seg_4082" s="T400">nur</ta>
            <ta e="T402" id="Seg_4083" s="T401">entstehen-PTCP.PRS</ta>
            <ta e="T403" id="Seg_4084" s="T402">sein-PST1-3PL</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_4085" s="T0">давно</ta>
            <ta e="T2" id="Seg_4086" s="T1">норильский-DAT/LOC</ta>
            <ta e="T3" id="Seg_4087" s="T2">князь-PROPR-3PL</ta>
            <ta e="T4" id="Seg_4088" s="T3">говорят</ta>
            <ta e="T5" id="Seg_4089" s="T4">молодой</ta>
            <ta e="T6" id="Seg_4090" s="T5">человек.[NOM]</ta>
            <ta e="T7" id="Seg_4091" s="T6">этот</ta>
            <ta e="T8" id="Seg_4092" s="T7">человек-3PL.[NOM]</ta>
            <ta e="T9" id="Seg_4093" s="T8">жена-POSS</ta>
            <ta e="T10" id="Seg_4094" s="T9">NEG.[3SG]</ta>
            <ta e="T11" id="Seg_4095" s="T10">этот-ACC</ta>
            <ta e="T12" id="Seg_4096" s="T11">семья-PL-3SG.[NOM]</ta>
            <ta e="T13" id="Seg_4097" s="T12">однажды</ta>
            <ta e="T14" id="Seg_4098" s="T13">спрашивать-PRS-3PL</ta>
            <ta e="T15" id="Seg_4099" s="T14">почему</ta>
            <ta e="T16" id="Seg_4100" s="T15">жена-VBZ-NEG-2SG</ta>
            <ta e="T17" id="Seg_4101" s="T16">говорить-CVB.SEQ</ta>
            <ta e="T18" id="Seg_4102" s="T17">тот-DAT/LOC</ta>
            <ta e="T19" id="Seg_4103" s="T18">говорить-PRS.[3SG]</ta>
            <ta e="T20" id="Seg_4104" s="T19">1SG.[NOM]</ta>
            <ta e="T21" id="Seg_4105" s="T20">этот</ta>
            <ta e="T22" id="Seg_4106" s="T21">страна.[NOM]</ta>
            <ta e="T23" id="Seg_4107" s="T22">девушка-3SG-ACC</ta>
            <ta e="T24" id="Seg_4108" s="T23">взять-NEG.PTCP</ta>
            <ta e="T25" id="Seg_4109" s="T24">человек-1SG</ta>
            <ta e="T26" id="Seg_4110" s="T25">наверху</ta>
            <ta e="T27" id="Seg_4111" s="T26">Юрюнг.Айыы.[NOM]</ta>
            <ta e="T28" id="Seg_4112" s="T27">говорить-CVB.SEQ</ta>
            <ta e="T29" id="Seg_4113" s="T28">есть</ta>
            <ta e="T30" id="Seg_4114" s="T29">тот.[NOM]</ta>
            <ta e="T31" id="Seg_4115" s="T30">дочь-3SG-ACC</ta>
            <ta e="T32" id="Seg_4116" s="T31">только</ta>
            <ta e="T33" id="Seg_4117" s="T32">взять-TEMP-1SG</ta>
            <ta e="T34" id="Seg_4118" s="T33">тогда</ta>
            <ta e="T35" id="Seg_4119" s="T34">согласить-EP-RECP/COLL-PTCP.FUT</ta>
            <ta e="T36" id="Seg_4120" s="T35">быть-PST1-1SG</ta>
            <ta e="T37" id="Seg_4121" s="T36">говорить-PRS.[3SG]</ta>
            <ta e="T38" id="Seg_4122" s="T37">этот-ACC</ta>
            <ta e="T39" id="Seg_4123" s="T38">люди-PL.[NOM]</ta>
            <ta e="T40" id="Seg_4124" s="T39">играть-CVB.SEQ</ta>
            <ta e="T41" id="Seg_4125" s="T40">говорить-INFER-3SG</ta>
            <ta e="T42" id="Seg_4126" s="T41">думать-CVB.SEQ</ta>
            <ta e="T43" id="Seg_4127" s="T42">бросать-PRS-3PL</ta>
            <ta e="T44" id="Seg_4128" s="T43">однажды</ta>
            <ta e="T45" id="Seg_4129" s="T44">сход-DAT/LOC</ta>
            <ta e="T46" id="Seg_4130" s="T45">князь.[NOM]</ta>
            <ta e="T47" id="Seg_4131" s="T46">шаман-ACC</ta>
            <ta e="T48" id="Seg_4132" s="T47">принуждать-PRS.[3SG]</ta>
            <ta e="T49" id="Seg_4133" s="T48">2SG.[NOM]</ta>
            <ta e="T50" id="Seg_4134" s="T49">1SG-DAT/LOC</ta>
            <ta e="T51" id="Seg_4135" s="T50">небо-DAT/LOC</ta>
            <ta e="T52" id="Seg_4136" s="T51">есть</ta>
            <ta e="T53" id="Seg_4137" s="T52">Юрюнг.Айыы.[NOM]</ta>
            <ta e="T54" id="Seg_4138" s="T53">дочь-3SG-ACC</ta>
            <ta e="T55" id="Seg_4139" s="T54">жена.[NOM]</ta>
            <ta e="T56" id="Seg_4140" s="T55">делать-PTCP.FUT-1SG-ACC</ta>
            <ta e="T57" id="Seg_4141" s="T56">сватовство-VBZ.[IMP.2SG]</ta>
            <ta e="T58" id="Seg_4142" s="T57">говорить-PRS.[3SG]</ta>
            <ta e="T59" id="Seg_4143" s="T58">этот-ACC</ta>
            <ta e="T60" id="Seg_4144" s="T59">шаман-3SG.[NOM]</ta>
            <ta e="T61" id="Seg_4145" s="T60">побаиваться-CVB.SIM</ta>
            <ta e="T62" id="Seg_4146" s="T61">слышать-PRS.[3SG]</ta>
            <ta e="T63" id="Seg_4147" s="T62">что-2SG.[NOM]</ta>
            <ta e="T64" id="Seg_4148" s="T63">слово-3SG.[NOM]=Q</ta>
            <ta e="T65" id="Seg_4149" s="T64">как</ta>
            <ta e="T66" id="Seg_4150" s="T65">NEG</ta>
            <ta e="T67" id="Seg_4151" s="T66">тот</ta>
            <ta e="T68" id="Seg_4152" s="T67">страна-ABL</ta>
            <ta e="T69" id="Seg_4153" s="T68">мочь-CVB.SEQ</ta>
            <ta e="T70" id="Seg_4154" s="T69">принести-NEG.PTCP</ta>
            <ta e="T71" id="Seg_4155" s="T70">дело-EP-1SG.[NOM]</ta>
            <ta e="T72" id="Seg_4156" s="T71">говорить-PRS.[3SG]</ta>
            <ta e="T73" id="Seg_4157" s="T72">этот-ACC</ta>
            <ta e="T74" id="Seg_4158" s="T73">князь.[NOM]</ta>
            <ta e="T75" id="Seg_4159" s="T74">семь</ta>
            <ta e="T76" id="Seg_4160" s="T75">прут-ACC</ta>
            <ta e="T77" id="Seg_4161" s="T76">видеть-CAUS-PRS.[3SG]</ta>
            <ta e="T78" id="Seg_4162" s="T77">2SG.[NOM]</ta>
            <ta e="T79" id="Seg_4163" s="T78">обман-VBZ-EP-NEG.[IMP.2SG]</ta>
            <ta e="T80" id="Seg_4164" s="T79">1SG.[NOM]</ta>
            <ta e="T81" id="Seg_4165" s="T80">2SG-ACC</ta>
            <ta e="T82" id="Seg_4166" s="T81">нижияя.часть.спины-2SG-ACC</ta>
            <ta e="T83" id="Seg_4167" s="T82">сдирать.кожу-FUT-1SG</ta>
            <ta e="T84" id="Seg_4168" s="T83">день.[NOM]</ta>
            <ta e="T85" id="Seg_4169" s="T84">завтра</ta>
            <ta e="T86" id="Seg_4170" s="T85">Юрюнг.Айыы.[NOM]</ta>
            <ta e="T87" id="Seg_4171" s="T86">дочь-3SG-ACC</ta>
            <ta e="T88" id="Seg_4172" s="T87">есть</ta>
            <ta e="T89" id="Seg_4173" s="T88">делать.[IMP.2SG]</ta>
            <ta e="T90" id="Seg_4174" s="T89">говорить-PRS.[3SG]</ta>
            <ta e="T91" id="Seg_4175" s="T90">шаман.[NOM]</ta>
            <ta e="T92" id="Seg_4176" s="T91">кончаться-FUT.[3SG]</ta>
            <ta e="T93" id="Seg_4177" s="T92">MOD</ta>
            <ta e="T94" id="Seg_4178" s="T93">вот</ta>
            <ta e="T95" id="Seg_4179" s="T94">камлать-CVB.SEQ</ta>
            <ta e="T96" id="Seg_4180" s="T95">идти-PRS.[3SG]</ta>
            <ta e="T97" id="Seg_4181" s="T96">этот</ta>
            <ta e="T98" id="Seg_4182" s="T97">шаман.[NOM]</ta>
            <ta e="T99" id="Seg_4183" s="T98">приближаться-PTCP.PST-3SG-ACC</ta>
            <ta e="T100" id="Seg_4184" s="T99">слышать-CVB.SEQ</ta>
            <ta e="T101" id="Seg_4185" s="T100">Юрюнг.Айыы.[NOM]</ta>
            <ta e="T102" id="Seg_4186" s="T101">господин.[NOM]</ta>
            <ta e="T103" id="Seg_4187" s="T102">очень-ADVZ</ta>
            <ta e="T104" id="Seg_4188" s="T103">разгневаться-PRS.[3SG]</ta>
            <ta e="T105" id="Seg_4189" s="T104">что.[NOM]</ta>
            <ta e="T106" id="Seg_4190" s="T105">умирать-PTCP.PRS</ta>
            <ta e="T107" id="Seg_4191" s="T106">нижний.мир.[NOM]</ta>
            <ta e="T108" id="Seg_4192" s="T107">шаман-3SG.[NOM]</ta>
            <ta e="T109" id="Seg_4193" s="T108">ведомо-POSS</ta>
            <ta e="T110" id="Seg_4194" s="T109">NEG</ta>
            <ta e="T111" id="Seg_4195" s="T110">приходить-INFER-3SG=Q</ta>
            <ta e="T112" id="Seg_4196" s="T111">принести-EP-IMP.2PL</ta>
            <ta e="T113" id="Seg_4197" s="T112">1SG-DAT/LOC</ta>
            <ta e="T114" id="Seg_4198" s="T113">огонь.[NOM]</ta>
            <ta e="T115" id="Seg_4199" s="T114">меч-1SG-ACC</ta>
            <ta e="T116" id="Seg_4200" s="T115">голова-ACC</ta>
            <ta e="T117" id="Seg_4201" s="T116">резать-CVB.SIM</ta>
            <ta e="T118" id="Seg_4202" s="T117">бить-PTCP.FUT-1SG-ACC</ta>
            <ta e="T119" id="Seg_4203" s="T118">говорить-PST2.[3SG]</ta>
            <ta e="T120" id="Seg_4204" s="T119">шаман-3SG.[NOM]</ta>
            <ta e="T121" id="Seg_4205" s="T120">приходить-PST2-3SG</ta>
            <ta e="T122" id="Seg_4206" s="T121">собраться-CVB.SEQ</ta>
            <ta e="T123" id="Seg_4207" s="T122">сидеть-PTCP.PRS</ta>
            <ta e="T124" id="Seg_4208" s="T123">быть-PST2.[3SG]</ta>
            <ta e="T125" id="Seg_4209" s="T124">Айыы-3SG.[NOM]</ta>
            <ta e="T126" id="Seg_4210" s="T125">этот-ACC</ta>
            <ta e="T127" id="Seg_4211" s="T126">вот</ta>
            <ta e="T128" id="Seg_4212" s="T127">ползать-CVB.SEQ</ta>
            <ta e="T129" id="Seg_4213" s="T128">попросить-EP-MED-PRS.[3SG]</ta>
            <ta e="T130" id="Seg_4214" s="T129">1SG.[NOM]</ta>
            <ta e="T131" id="Seg_4215" s="T130">нужда-ABL</ta>
            <ta e="T132" id="Seg_4216" s="T131">приходить-PST1-1SG</ta>
            <ta e="T133" id="Seg_4217" s="T132">2SG.[NOM]</ta>
            <ta e="T134" id="Seg_4218" s="T133">как</ta>
            <ta e="T135" id="Seg_4219" s="T134">земля-1PL-DAT/LOC</ta>
            <ta e="T136" id="Seg_4220" s="T135">руководитель-PROPR.[NOM]</ta>
            <ta e="T137" id="Seg_4221" s="T136">быть-PRS-1PL</ta>
            <ta e="T138" id="Seg_4222" s="T137">князь.[NOM]</ta>
            <ta e="T139" id="Seg_4223" s="T138">говорить-CVB.SEQ</ta>
            <ta e="T140" id="Seg_4224" s="T139">тот.[NOM]</ta>
            <ta e="T141" id="Seg_4225" s="T140">принуждать-NMNZ-3SG-ABL</ta>
            <ta e="T142" id="Seg_4226" s="T141">приходить-PST1-1SG</ta>
            <ta e="T143" id="Seg_4227" s="T142">говорить-CVB.SEQ</ta>
            <ta e="T144" id="Seg_4228" s="T143">жалеться-CVB.SEQ</ta>
            <ta e="T145" id="Seg_4229" s="T144">потребность-3SG-ACC</ta>
            <ta e="T146" id="Seg_4230" s="T145">рассказывать-PRS.[3SG]</ta>
            <ta e="T147" id="Seg_4231" s="T146">тот-DAT/LOC</ta>
            <ta e="T148" id="Seg_4232" s="T147">Юрюнг.Айыы-3SG.[NOM]</ta>
            <ta e="T149" id="Seg_4233" s="T148">говорить-PRS.[3SG]</ta>
            <ta e="T150" id="Seg_4234" s="T149">нет</ta>
            <ta e="T151" id="Seg_4235" s="T150">кончаться.[IMP.2SG]</ta>
            <ta e="T152" id="Seg_4236" s="T151">говорить.[IMP.2SG]</ta>
            <ta e="T153" id="Seg_4237" s="T152">говорить-CVB.SEQ</ta>
            <ta e="T154" id="Seg_4238" s="T153">возвращаться-EP-CAUS-PRS.[3SG]</ta>
            <ta e="T155" id="Seg_4239" s="T154">шаман-3SG-ACC</ta>
            <ta e="T156" id="Seg_4240" s="T155">этот-3SG.[NOM]</ta>
            <ta e="T157" id="Seg_4241" s="T156">камлать-CVB.SEQ</ta>
            <ta e="T158" id="Seg_4242" s="T157">кончать-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T159" id="Seg_4243" s="T158">князь-3SG.[NOM]</ta>
            <ta e="T160" id="Seg_4244" s="T159">принуждать-PRS.[3SG]</ta>
            <ta e="T161" id="Seg_4245" s="T160">2SG.[NOM]</ta>
            <ta e="T162" id="Seg_4246" s="T161">притворяться-CVB.SEQ</ta>
            <ta e="T163" id="Seg_4247" s="T162">туда-сюда</ta>
            <ta e="T164" id="Seg_4248" s="T163">идти-PTCP.PST</ta>
            <ta e="T165" id="Seg_4249" s="T164">быть-PRS-2SG</ta>
            <ta e="T166" id="Seg_4250" s="T165">жертва.[NOM]</ta>
            <ta e="T167" id="Seg_4251" s="T166">песец-2SG.[NOM]</ta>
            <ta e="T168" id="Seg_4252" s="T167">здесь</ta>
            <ta e="T169" id="Seg_4253" s="T168">стоять-PTCP.PRS</ta>
            <ta e="T170" id="Seg_4254" s="T169">быть-NEG.[3SG]</ta>
            <ta e="T171" id="Seg_4255" s="T170">Q</ta>
            <ta e="T172" id="Seg_4256" s="T171">говорить-CVB.SEQ</ta>
            <ta e="T173" id="Seg_4257" s="T172">принуждать-PRS.[3SG]</ta>
            <ta e="T174" id="Seg_4258" s="T173">непременно</ta>
            <ta e="T175" id="Seg_4259" s="T174">1SG-DAT/LOC</ta>
            <ta e="T176" id="Seg_4260" s="T175">дочь-1SG-ACC</ta>
            <ta e="T177" id="Seg_4261" s="T176">приносить-CVB.SEQ</ta>
            <ta e="T178" id="Seg_4262" s="T177">давать.[IMP.2SG]</ta>
            <ta e="T179" id="Seg_4263" s="T178">говорить-PRS.[3SG]</ta>
            <ta e="T180" id="Seg_4264" s="T179">вот</ta>
            <ta e="T181" id="Seg_4265" s="T180">опять</ta>
            <ta e="T182" id="Seg_4266" s="T181">этот</ta>
            <ta e="T183" id="Seg_4267" s="T182">шаман.[NOM]</ta>
            <ta e="T184" id="Seg_4268" s="T183">утром</ta>
            <ta e="T185" id="Seg_4269" s="T184">камлать-CVB.SEQ</ta>
            <ta e="T186" id="Seg_4270" s="T185">беда-VBZ-MED-PRS.[3SG]</ta>
            <ta e="T187" id="Seg_4271" s="T186">этот</ta>
            <ta e="T188" id="Seg_4272" s="T187">камлать-CVB.SEQ</ta>
            <ta e="T189" id="Seg_4273" s="T188">доезжать-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T190" id="Seg_4274" s="T189">что-ABL</ta>
            <ta e="T191" id="Seg_4275" s="T190">да</ta>
            <ta e="T192" id="Seg_4276" s="T191">ужас-VBZ-CVB.SEQ</ta>
            <ta e="T193" id="Seg_4277" s="T192">Юрюнг.Айыы-3SG.[NOM]</ta>
            <ta e="T194" id="Seg_4278" s="T193">шаман-3SG-ACC</ta>
            <ta e="T195" id="Seg_4279" s="T194">сердиться-PRS.[3SG]</ta>
            <ta e="T196" id="Seg_4280" s="T195">этот-DAT/LOC</ta>
            <ta e="T197" id="Seg_4281" s="T196">опять</ta>
            <ta e="T198" id="Seg_4282" s="T197">шаман-3SG.[NOM]</ta>
            <ta e="T199" id="Seg_4283" s="T198">плакать-EP-MED-CVB.SEQ</ta>
            <ta e="T200" id="Seg_4284" s="T199">маяться-PRS.[3SG]</ta>
            <ta e="T201" id="Seg_4285" s="T200">хоть</ta>
            <ta e="T202" id="Seg_4286" s="T201">метка-PART</ta>
            <ta e="T203" id="Seg_4287" s="T202">давать.[IMP.2SG]</ta>
            <ta e="T204" id="Seg_4288" s="T203">говорить-CVB.SEQ</ta>
            <ta e="T205" id="Seg_4289" s="T204">нет</ta>
            <ta e="T206" id="Seg_4290" s="T205">давать-FUT-1SG</ta>
            <ta e="T207" id="Seg_4291" s="T206">NEG-3SG</ta>
            <ta e="T208" id="Seg_4292" s="T207">почему</ta>
            <ta e="T209" id="Seg_4293" s="T208">глупый.[NOM]=Q</ta>
            <ta e="T210" id="Seg_4294" s="T209">поперек</ta>
            <ta e="T211" id="Seg_4295" s="T210">глаз-PROPR.[NOM]</ta>
            <ta e="T212" id="Seg_4296" s="T211">лучше</ta>
            <ta e="T213" id="Seg_4297" s="T212">2SG.[NOM]</ta>
            <ta e="T214" id="Seg_4298" s="T213">умирать-INFER-EP-2SG</ta>
            <ta e="T215" id="Seg_4299" s="T214">говорить-CVB.SEQ</ta>
            <ta e="T216" id="Seg_4300" s="T215">шаман-3SG-ACC</ta>
            <ta e="T217" id="Seg_4301" s="T216">опять</ta>
            <ta e="T218" id="Seg_4302" s="T217">возвращаться-EP-CAUS-PRS.[3SG]</ta>
            <ta e="T219" id="Seg_4303" s="T218">камлать-CVB.SEQ</ta>
            <ta e="T220" id="Seg_4304" s="T219">кончать-CVB.SEQ</ta>
            <ta e="T221" id="Seg_4305" s="T220">после</ta>
            <ta e="T222" id="Seg_4306" s="T221">шаман.[NOM]</ta>
            <ta e="T223" id="Seg_4307" s="T222">князь-ABL</ta>
            <ta e="T224" id="Seg_4308" s="T223">умолять-PRS.[3SG]</ta>
            <ta e="T225" id="Seg_4309" s="T224">что</ta>
            <ta e="T226" id="Seg_4310" s="T225">NEG</ta>
            <ta e="T227" id="Seg_4311" s="T226">полностью</ta>
            <ta e="T228" id="Seg_4312" s="T227">три</ta>
            <ta e="T229" id="Seg_4313" s="T228">раз-PROPR.[NOM]</ta>
            <ta e="T230" id="Seg_4314" s="T229">убить-EP-NEG.[IMP.2SG]</ta>
            <ta e="T231" id="Seg_4315" s="T230">желание-1SG-ACC</ta>
            <ta e="T232" id="Seg_4316" s="T231">достигать-NEG-PST1-3SG</ta>
            <ta e="T233" id="Seg_4317" s="T232">говорить-CVB.SEQ</ta>
            <ta e="T234" id="Seg_4318" s="T233">завтра</ta>
            <ta e="T235" id="Seg_4319" s="T234">опять</ta>
            <ta e="T236" id="Seg_4320" s="T235">мучить-EP-RECP/COLL-CVB.SEQ</ta>
            <ta e="T237" id="Seg_4321" s="T236">попробовать-FUT-1SG</ta>
            <ta e="T238" id="Seg_4322" s="T237">говорить-PRS.[3SG]</ta>
            <ta e="T239" id="Seg_4323" s="T238">следующее.утро-3SG-DAT/LOC</ta>
            <ta e="T240" id="Seg_4324" s="T239">три-ORD</ta>
            <ta e="T241" id="Seg_4325" s="T240">камлать-NMNZ-3SG-ACC</ta>
            <ta e="T242" id="Seg_4326" s="T241">камлать-PRS.[3SG]</ta>
            <ta e="T243" id="Seg_4327" s="T242">этот</ta>
            <ta e="T244" id="Seg_4328" s="T243">камлать-CVB.SEQ</ta>
            <ta e="T245" id="Seg_4329" s="T244">Юрюнг.Айыы.[NOM]</ta>
            <ta e="T246" id="Seg_4330" s="T245">дом-3SG-DAT/LOC</ta>
            <ta e="T247" id="Seg_4331" s="T246">доезжать-PRS.[3SG]</ta>
            <ta e="T248" id="Seg_4332" s="T247">этот</ta>
            <ta e="T249" id="Seg_4333" s="T248">доезжать-CVB.SEQ</ta>
            <ta e="T250" id="Seg_4334" s="T249">кланяться-CVB.SEQ</ta>
            <ta e="T251" id="Seg_4335" s="T250">чум-3SG-GEN</ta>
            <ta e="T252" id="Seg_4336" s="T251">главный.столб.чума-3SG-GEN</ta>
            <ta e="T253" id="Seg_4337" s="T252">господин-3SG-DAT/LOC</ta>
            <ta e="T254" id="Seg_4338" s="T253">умолять-CVB.SEQ</ta>
            <ta e="T255" id="Seg_4339" s="T254">Юрюнг.Айыы-3SG-GEN</ta>
            <ta e="T256" id="Seg_4340" s="T255">нога-3SG-DAT/LOC</ta>
            <ta e="T257" id="Seg_4341" s="T256">ползать-CVB.SEQ</ta>
            <ta e="T258" id="Seg_4342" s="T257">приходить-CVB.SEQ</ta>
            <ta e="T259" id="Seg_4343" s="T258">этот</ta>
            <ta e="T260" id="Seg_4344" s="T259">прежний-3SG-COMP</ta>
            <ta e="T261" id="Seg_4345" s="T260">очень-ADVZ</ta>
            <ta e="T262" id="Seg_4346" s="T261">жалеться-PRS.[3SG]-плакать-PRS.[3SG]</ta>
            <ta e="T263" id="Seg_4347" s="T262">этот-ACC</ta>
            <ta e="T264" id="Seg_4348" s="T263">вот</ta>
            <ta e="T265" id="Seg_4349" s="T264">сердце-3SG-INSTR</ta>
            <ta e="T266" id="Seg_4350" s="T265">жалеть-PRS.[3SG]</ta>
            <ta e="T267" id="Seg_4351" s="T266">вот</ta>
            <ta e="T268" id="Seg_4352" s="T267">правда.[NOM]</ta>
            <ta e="T269" id="Seg_4353" s="T268">1SG.[NOM]</ta>
            <ta e="T270" id="Seg_4354" s="T269">2SG-ACC</ta>
            <ta e="T271" id="Seg_4355" s="T270">убить-CVB.PURP</ta>
            <ta e="T272" id="Seg_4356" s="T271">сам-1SG.[NOM]</ta>
            <ta e="T273" id="Seg_4357" s="T272">шаман-EP-1SG.[NOM]</ta>
            <ta e="T274" id="Seg_4358" s="T273">делать-PST2.NEG-EP-1SG</ta>
            <ta e="T275" id="Seg_4359" s="T274">почему</ta>
            <ta e="T276" id="Seg_4360" s="T275">EMPH</ta>
            <ta e="T277" id="Seg_4361" s="T276">князь-EP-2SG.[NOM]</ta>
            <ta e="T278" id="Seg_4362" s="T277">MOD</ta>
            <ta e="T279" id="Seg_4363" s="T278">глупый.[NOM]=Q</ta>
            <ta e="T280" id="Seg_4364" s="T279">вот</ta>
            <ta e="T281" id="Seg_4365" s="T280">сам-3SG-GEN</ta>
            <ta e="T282" id="Seg_4366" s="T281">народ-3SG-DAT/LOC</ta>
            <ta e="T283" id="Seg_4367" s="T282">беда-ACC</ta>
            <ta e="T284" id="Seg_4368" s="T283">делать-INFER-3SG</ta>
            <ta e="T285" id="Seg_4369" s="T284">народ-2PL-ABL</ta>
            <ta e="T286" id="Seg_4370" s="T285">одиноко</ta>
            <ta e="T287" id="Seg_4371" s="T286">2SG.[NOM]</ta>
            <ta e="T288" id="Seg_4372" s="T287">сын-2SG-ACC</ta>
            <ta e="T289" id="Seg_4373" s="T288">вместе.с</ta>
            <ta e="T290" id="Seg_4374" s="T289">быть.лишным-FUT-2PL</ta>
            <ta e="T291" id="Seg_4375" s="T290">что.[NOM]</ta>
            <ta e="T292" id="Seg_4376" s="T291">NEG</ta>
            <ta e="T293" id="Seg_4377" s="T292">живой.[NOM]</ta>
            <ta e="T294" id="Seg_4378" s="T293">оставаться-FUT.[3SG]</ta>
            <ta e="T295" id="Seg_4379" s="T294">NEG-3SG</ta>
            <ta e="T296" id="Seg_4380" s="T295">другой</ta>
            <ta e="T297" id="Seg_4381" s="T296">место-ABL</ta>
            <ta e="T298" id="Seg_4382" s="T297">люди.[NOM]</ta>
            <ta e="T299" id="Seg_4383" s="T298">приходить-CVB.SEQ-3PL</ta>
            <ta e="T300" id="Seg_4384" s="T299">тот-ABL</ta>
            <ta e="T301" id="Seg_4385" s="T300">новый</ta>
            <ta e="T302" id="Seg_4386" s="T301">народ.[NOM]</ta>
            <ta e="T303" id="Seg_4387" s="T302">создаваться-FUT-3SG</ta>
            <ta e="T304" id="Seg_4388" s="T303">говорить-PRS.[3SG]</ta>
            <ta e="T305" id="Seg_4389" s="T304">новый</ta>
            <ta e="T306" id="Seg_4390" s="T305">чистый</ta>
            <ta e="T307" id="Seg_4391" s="T306">белый</ta>
            <ta e="T308" id="Seg_4392" s="T307">кожаная.покрышка.чума-PART</ta>
            <ta e="T309" id="Seg_4393" s="T308">теперь</ta>
            <ta e="T310" id="Seg_4394" s="T309">три</ta>
            <ta e="T311" id="Seg_4395" s="T310">сутки-3SG-INSTR</ta>
            <ta e="T312" id="Seg_4396" s="T311">утро</ta>
            <ta e="T313" id="Seg_4397" s="T312">ставить-IMP.3PL</ta>
            <ta e="T314" id="Seg_4398" s="T313">пестрый</ta>
            <ta e="T315" id="Seg_4399" s="T314">белый</ta>
            <ta e="T316" id="Seg_4400" s="T315">олень-ACC</ta>
            <ta e="T317" id="Seg_4401" s="T316">убить-IMP.3PL</ta>
            <ta e="T318" id="Seg_4402" s="T317">тот</ta>
            <ta e="T319" id="Seg_4403" s="T318">чум.[NOM]</ta>
            <ta e="T320" id="Seg_4404" s="T319">окрестность-3SG-ACC</ta>
            <ta e="T321" id="Seg_4405" s="T320">каждый-3SG-ACC</ta>
            <ta e="T322" id="Seg_4406" s="T321">белый</ta>
            <ta e="T323" id="Seg_4407" s="T322">олень.[NOM]</ta>
            <ta e="T324" id="Seg_4408" s="T323">кожа-3SG-INSTR</ta>
            <ta e="T325" id="Seg_4409" s="T324">постелить-IMP.3PL</ta>
            <ta e="T326" id="Seg_4410" s="T325">этот-INSTR</ta>
            <ta e="T327" id="Seg_4411" s="T326">выступать-CVB.SEQ</ta>
            <ta e="T328" id="Seg_4412" s="T327">входить-FUT-3SG</ta>
            <ta e="T329" id="Seg_4413" s="T328">князь-2PL.[NOM]</ta>
            <ta e="T330" id="Seg_4414" s="T329">грязный</ta>
            <ta e="T331" id="Seg_4415" s="T330">что.[NOM]</ta>
            <ta e="T332" id="Seg_4416" s="T331">NEG</ta>
            <ta e="T333" id="Seg_4417" s="T332">прилипать-FUT.[3SG]</ta>
            <ta e="T334" id="Seg_4418" s="T333">NEG-NEC.[3SG]</ta>
            <ta e="T335" id="Seg_4419" s="T334">говорить-PRS.[3SG]</ta>
            <ta e="T336" id="Seg_4420" s="T335">люди-PL.[NOM]</ta>
            <ta e="T337" id="Seg_4421" s="T336">свадьба.[NOM]</ta>
            <ta e="T338" id="Seg_4422" s="T337">думать-CVB.SEQ</ta>
            <ta e="T339" id="Seg_4423" s="T338">приходить-NEG-IMP.3PL</ta>
            <ta e="T340" id="Seg_4424" s="T339">кто.[NOM]</ta>
            <ta e="T341" id="Seg_4425" s="T340">душа-3SG-ACC</ta>
            <ta e="T342" id="Seg_4426" s="T341">уберечь-CVB.PURP</ta>
            <ta e="T343" id="Seg_4427" s="T342">хотеть-PRS.[3SG]</ta>
            <ta e="T344" id="Seg_4428" s="T343">далеко</ta>
            <ta e="T345" id="Seg_4429" s="T344">идти-IMP.3SG</ta>
            <ta e="T346" id="Seg_4430" s="T345">говорить-PST2.[3SG]</ta>
            <ta e="T347" id="Seg_4431" s="T346">три-ORD</ta>
            <ta e="T348" id="Seg_4432" s="T347">день-3SG-DAT/LOC</ta>
            <ta e="T349" id="Seg_4433" s="T348">говорить-PTCP.PST-3SG-ACC</ta>
            <ta e="T350" id="Seg_4434" s="T349">как</ta>
            <ta e="T351" id="Seg_4435" s="T350">делать-PST2-3PL</ta>
            <ta e="T352" id="Seg_4436" s="T351">здесь</ta>
            <ta e="T353" id="Seg_4437" s="T352">шаман.[NOM]</ta>
            <ta e="T354" id="Seg_4438" s="T353">видеть-CVB.SEQ</ta>
            <ta e="T355" id="Seg_4439" s="T354">стоять-PST2.[3SG]</ta>
            <ta e="T356" id="Seg_4440" s="T355">огромный</ta>
            <ta e="T357" id="Seg_4441" s="T356">очень</ta>
            <ta e="T358" id="Seg_4442" s="T357">вихрь.[NOM]</ta>
            <ta e="T359" id="Seg_4443" s="T358">ветер.[NOM]</ta>
            <ta e="T360" id="Seg_4444" s="T359">вдруг.начинать-PST2.[3SG]</ta>
            <ta e="T361" id="Seg_4445" s="T360">бубенчик.[NOM]</ta>
            <ta e="T362" id="Seg_4446" s="T361">звук-3SG.[NOM]</ta>
            <ta e="T363" id="Seg_4447" s="T362">говорить-CVB.SEQ</ta>
            <ta e="T364" id="Seg_4448" s="T363">очень</ta>
            <ta e="T365" id="Seg_4449" s="T364">слышаться-EP-PST2.[3SG]</ta>
            <ta e="T366" id="Seg_4450" s="T365">что.[NOM]</ta>
            <ta e="T367" id="Seg_4451" s="T366">только</ta>
            <ta e="T368" id="Seg_4452" s="T367">ветер.[NOM]</ta>
            <ta e="T369" id="Seg_4453" s="T368">как</ta>
            <ta e="T370" id="Seg_4454" s="T369">чум.[NOM]</ta>
            <ta e="T371" id="Seg_4455" s="T370">дверь-3SG-ACC</ta>
            <ta e="T372" id="Seg_4456" s="T371">открывать-PTCP.PRS-DAT/LOC</ta>
            <ta e="T373" id="Seg_4457" s="T372">как</ta>
            <ta e="T374" id="Seg_4458" s="T373">делать-PST2.[3SG]</ta>
            <ta e="T375" id="Seg_4459" s="T374">тот.[NOM]</ta>
            <ta e="T376" id="Seg_4460" s="T375">после</ta>
            <ta e="T377" id="Seg_4461" s="T376">ветер-POSS</ta>
            <ta e="T378" id="Seg_4462" s="T377">NEG</ta>
            <ta e="T379" id="Seg_4463" s="T378">становиться-PST2.[3SG]</ta>
            <ta e="T380" id="Seg_4464" s="T379">этот-ACC</ta>
            <ta e="T381" id="Seg_4465" s="T380">шаман.[NOM]</ta>
            <ta e="T382" id="Seg_4466" s="T381">идти-CVB.SEQ</ta>
            <ta e="T383" id="Seg_4467" s="T382">видеть-PST2-3SG</ta>
            <ta e="T384" id="Seg_4468" s="T383">князь.[NOM]</ta>
            <ta e="T385" id="Seg_4469" s="T384">да</ta>
            <ta e="T386" id="Seg_4470" s="T385">каждый</ta>
            <ta e="T387" id="Seg_4471" s="T386">приходить-PTCP.PST</ta>
            <ta e="T388" id="Seg_4472" s="T387">да</ta>
            <ta e="T389" id="Seg_4473" s="T388">люди.[NOM]</ta>
            <ta e="T390" id="Seg_4474" s="T389">умирать-CVB.SIM</ta>
            <ta e="T391" id="Seg_4475" s="T390">лежать-PRS-3PL</ta>
            <ta e="T392" id="Seg_4476" s="T391">быть-PST2.[3SG]</ta>
            <ta e="T393" id="Seg_4477" s="T392">этот-INSTR</ta>
            <ta e="T394" id="Seg_4478" s="T393">норильский.[NOM]</ta>
            <ta e="T395" id="Seg_4479" s="T394">беднеть-PST1-3SG</ta>
            <ta e="T396" id="Seg_4480" s="T395">тот.[NOM]</ta>
            <ta e="T397" id="Seg_4481" s="T396">Эдян.[NOM]</ta>
            <ta e="T398" id="Seg_4482" s="T397">поперечник-ABL</ta>
            <ta e="T399" id="Seg_4483" s="T398">приходить-CVB.SEQ</ta>
            <ta e="T400" id="Seg_4484" s="T399">потом</ta>
            <ta e="T401" id="Seg_4485" s="T400">только</ta>
            <ta e="T402" id="Seg_4486" s="T401">создаваться-PTCP.PRS</ta>
            <ta e="T403" id="Seg_4487" s="T402">быть-PST1-3PL</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_4488" s="T0">adv</ta>
            <ta e="T2" id="Seg_4489" s="T1">adj-n:case</ta>
            <ta e="T3" id="Seg_4490" s="T2">n-n&gt;adj-n:pred.pn</ta>
            <ta e="T4" id="Seg_4491" s="T3">ptcl</ta>
            <ta e="T5" id="Seg_4492" s="T4">adj</ta>
            <ta e="T6" id="Seg_4493" s="T5">n-n:case</ta>
            <ta e="T7" id="Seg_4494" s="T6">dempro</ta>
            <ta e="T8" id="Seg_4495" s="T7">n-n:(poss)-n:case</ta>
            <ta e="T9" id="Seg_4496" s="T8">n-n:(poss)</ta>
            <ta e="T10" id="Seg_4497" s="T9">ptcl-ptcl:pred.pn</ta>
            <ta e="T11" id="Seg_4498" s="T10">dempro-pro:case</ta>
            <ta e="T12" id="Seg_4499" s="T11">n-n:(num)-n:(poss)-n:case</ta>
            <ta e="T13" id="Seg_4500" s="T12">adv</ta>
            <ta e="T14" id="Seg_4501" s="T13">v-v:tense-v:pred.pn</ta>
            <ta e="T15" id="Seg_4502" s="T14">que</ta>
            <ta e="T16" id="Seg_4503" s="T15">n-n&gt;v-v:(neg)-v:pred.pn</ta>
            <ta e="T17" id="Seg_4504" s="T16">v-v:cvb</ta>
            <ta e="T18" id="Seg_4505" s="T17">dempro-pro:case</ta>
            <ta e="T19" id="Seg_4506" s="T18">v-v:tense-v:pred.pn</ta>
            <ta e="T20" id="Seg_4507" s="T19">pers-pro:case</ta>
            <ta e="T21" id="Seg_4508" s="T20">dempro</ta>
            <ta e="T22" id="Seg_4509" s="T21">n-n:case</ta>
            <ta e="T23" id="Seg_4510" s="T22">n-n:poss-n:case</ta>
            <ta e="T24" id="Seg_4511" s="T23">v-v:ptcp</ta>
            <ta e="T25" id="Seg_4512" s="T24">n-n:(pred.pn)</ta>
            <ta e="T26" id="Seg_4513" s="T25">adv</ta>
            <ta e="T27" id="Seg_4514" s="T26">propr-n:case</ta>
            <ta e="T28" id="Seg_4515" s="T27">v-v:cvb</ta>
            <ta e="T29" id="Seg_4516" s="T28">ptcl</ta>
            <ta e="T30" id="Seg_4517" s="T29">dempro-pro:case</ta>
            <ta e="T31" id="Seg_4518" s="T30">n-n:poss-n:case</ta>
            <ta e="T32" id="Seg_4519" s="T31">ptcl</ta>
            <ta e="T33" id="Seg_4520" s="T32">v-v:mood-v:temp.pn</ta>
            <ta e="T34" id="Seg_4521" s="T33">adv</ta>
            <ta e="T35" id="Seg_4522" s="T34">v-v:(ins)-v&gt;v-v:ptcp</ta>
            <ta e="T36" id="Seg_4523" s="T35">v-v:tense-v:poss.pn</ta>
            <ta e="T37" id="Seg_4524" s="T36">v-v:tense-v:pred.pn</ta>
            <ta e="T38" id="Seg_4525" s="T37">dempro-pro:case</ta>
            <ta e="T39" id="Seg_4526" s="T38">n-n:(num)-n:case</ta>
            <ta e="T40" id="Seg_4527" s="T39">v-v:cvb</ta>
            <ta e="T41" id="Seg_4528" s="T40">v-v:mood-v:poss.pn</ta>
            <ta e="T42" id="Seg_4529" s="T41">v-v:cvb</ta>
            <ta e="T43" id="Seg_4530" s="T42">v-v:tense-v:pred.pn</ta>
            <ta e="T44" id="Seg_4531" s="T43">adv</ta>
            <ta e="T45" id="Seg_4532" s="T44">n-n:case</ta>
            <ta e="T46" id="Seg_4533" s="T45">n-n:case</ta>
            <ta e="T47" id="Seg_4534" s="T46">n-n:case</ta>
            <ta e="T48" id="Seg_4535" s="T47">v-v:tense-v:pred.pn</ta>
            <ta e="T49" id="Seg_4536" s="T48">pers-pro:case</ta>
            <ta e="T50" id="Seg_4537" s="T49">pers-pro:case</ta>
            <ta e="T51" id="Seg_4538" s="T50">n-n:case</ta>
            <ta e="T52" id="Seg_4539" s="T51">ptcl</ta>
            <ta e="T53" id="Seg_4540" s="T52">propr-n:case</ta>
            <ta e="T54" id="Seg_4541" s="T53">n-n:poss-n:case</ta>
            <ta e="T55" id="Seg_4542" s="T54">n-n:case</ta>
            <ta e="T56" id="Seg_4543" s="T55">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T57" id="Seg_4544" s="T56">n-n&gt;v-v:mood.pn</ta>
            <ta e="T58" id="Seg_4545" s="T57">v-v:tense-v:pred.pn</ta>
            <ta e="T59" id="Seg_4546" s="T58">dempro-pro:case</ta>
            <ta e="T60" id="Seg_4547" s="T59">n-n:(poss)-n:case</ta>
            <ta e="T61" id="Seg_4548" s="T60">v-v:cvb</ta>
            <ta e="T62" id="Seg_4549" s="T61">v-v:tense-v:pred.pn</ta>
            <ta e="T63" id="Seg_4550" s="T62">que-pro:(poss)-pro:case</ta>
            <ta e="T64" id="Seg_4551" s="T63">n-n:(poss)-n:case-ptcl</ta>
            <ta e="T65" id="Seg_4552" s="T64">que</ta>
            <ta e="T66" id="Seg_4553" s="T65">ptcl</ta>
            <ta e="T67" id="Seg_4554" s="T66">dempro</ta>
            <ta e="T68" id="Seg_4555" s="T67">n-n:case</ta>
            <ta e="T69" id="Seg_4556" s="T68">v-v:cvb</ta>
            <ta e="T70" id="Seg_4557" s="T69">v-v:ptcp</ta>
            <ta e="T71" id="Seg_4558" s="T70">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T72" id="Seg_4559" s="T71">v-v:tense-v:pred.pn</ta>
            <ta e="T73" id="Seg_4560" s="T72">dempro-pro:case</ta>
            <ta e="T74" id="Seg_4561" s="T73">n-n:case</ta>
            <ta e="T75" id="Seg_4562" s="T74">cardnum</ta>
            <ta e="T76" id="Seg_4563" s="T75">n-n:case</ta>
            <ta e="T77" id="Seg_4564" s="T76">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T78" id="Seg_4565" s="T77">pers-pro:case</ta>
            <ta e="T79" id="Seg_4566" s="T78">n-n&gt;v-v:(ins)-v:(neg)-v:mood.pn</ta>
            <ta e="T80" id="Seg_4567" s="T79">pers-pro:case</ta>
            <ta e="T81" id="Seg_4568" s="T80">pers-pro:case</ta>
            <ta e="T82" id="Seg_4569" s="T81">n-n:poss-n:case</ta>
            <ta e="T83" id="Seg_4570" s="T82">v-v:tense-v:poss.pn</ta>
            <ta e="T84" id="Seg_4571" s="T83">n-n:case</ta>
            <ta e="T85" id="Seg_4572" s="T84">adv</ta>
            <ta e="T86" id="Seg_4573" s="T85">propr-n:case</ta>
            <ta e="T87" id="Seg_4574" s="T86">n-n:poss-n:case</ta>
            <ta e="T88" id="Seg_4575" s="T87">ptcl</ta>
            <ta e="T89" id="Seg_4576" s="T88">v-v:mood.pn</ta>
            <ta e="T90" id="Seg_4577" s="T89">v-v:tense-v:pred.pn</ta>
            <ta e="T91" id="Seg_4578" s="T90">n-n:case</ta>
            <ta e="T92" id="Seg_4579" s="T91">v-v:tense-v:poss.pn</ta>
            <ta e="T93" id="Seg_4580" s="T92">ptcl</ta>
            <ta e="T94" id="Seg_4581" s="T93">ptcl</ta>
            <ta e="T95" id="Seg_4582" s="T94">v-v:cvb</ta>
            <ta e="T96" id="Seg_4583" s="T95">v-v:tense-v:pred.pn</ta>
            <ta e="T97" id="Seg_4584" s="T96">dempro</ta>
            <ta e="T98" id="Seg_4585" s="T97">n-n:case</ta>
            <ta e="T99" id="Seg_4586" s="T98">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T100" id="Seg_4587" s="T99">v-v:cvb</ta>
            <ta e="T101" id="Seg_4588" s="T100">propr-n:case</ta>
            <ta e="T102" id="Seg_4589" s="T101">n-n:case</ta>
            <ta e="T103" id="Seg_4590" s="T102">adv-adv&gt;adv</ta>
            <ta e="T104" id="Seg_4591" s="T103">v-v:tense-v:pred.pn</ta>
            <ta e="T105" id="Seg_4592" s="T104">que-pro:case</ta>
            <ta e="T106" id="Seg_4593" s="T105">v-v:ptcp</ta>
            <ta e="T107" id="Seg_4594" s="T106">n-n:case</ta>
            <ta e="T108" id="Seg_4595" s="T107">n-n:(poss)-n:case</ta>
            <ta e="T109" id="Seg_4596" s="T108">n-n:(poss)</ta>
            <ta e="T110" id="Seg_4597" s="T109">ptcl</ta>
            <ta e="T111" id="Seg_4598" s="T110">v-v:mood-v:poss.pn-ptcl</ta>
            <ta e="T112" id="Seg_4599" s="T111">v-v:(ins)-v:mood.pn</ta>
            <ta e="T113" id="Seg_4600" s="T112">pers-pro:case</ta>
            <ta e="T114" id="Seg_4601" s="T113">n-n:case</ta>
            <ta e="T115" id="Seg_4602" s="T114">n-n:poss-n:case</ta>
            <ta e="T116" id="Seg_4603" s="T115">n-n:case</ta>
            <ta e="T117" id="Seg_4604" s="T116">v-v:cvb</ta>
            <ta e="T118" id="Seg_4605" s="T117">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T119" id="Seg_4606" s="T118">v-v:tense-v:pred.pn</ta>
            <ta e="T120" id="Seg_4607" s="T119">n-n:(poss)-n:case</ta>
            <ta e="T121" id="Seg_4608" s="T120">v-v:tense-v:poss.pn</ta>
            <ta e="T122" id="Seg_4609" s="T121">v-v:cvb</ta>
            <ta e="T123" id="Seg_4610" s="T122">v-v:ptcp</ta>
            <ta e="T124" id="Seg_4611" s="T123">v-v:tense-v:pred.pn</ta>
            <ta e="T125" id="Seg_4612" s="T124">propr-n:(poss)-n:case</ta>
            <ta e="T126" id="Seg_4613" s="T125">dempro-pro:case</ta>
            <ta e="T127" id="Seg_4614" s="T126">ptcl</ta>
            <ta e="T128" id="Seg_4615" s="T127">v-v:cvb</ta>
            <ta e="T129" id="Seg_4616" s="T128">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T130" id="Seg_4617" s="T129">pers-pro:case</ta>
            <ta e="T131" id="Seg_4618" s="T130">n-n:case</ta>
            <ta e="T132" id="Seg_4619" s="T131">v-v:tense-v:poss.pn</ta>
            <ta e="T133" id="Seg_4620" s="T132">pers-pro:case</ta>
            <ta e="T134" id="Seg_4621" s="T133">post</ta>
            <ta e="T135" id="Seg_4622" s="T134">n-n:poss-n:case</ta>
            <ta e="T136" id="Seg_4623" s="T135">n-n&gt;adj-n:case</ta>
            <ta e="T137" id="Seg_4624" s="T136">v-v:tense-v:pred.pn</ta>
            <ta e="T138" id="Seg_4625" s="T137">n-n:case</ta>
            <ta e="T139" id="Seg_4626" s="T138">v-v:cvb</ta>
            <ta e="T140" id="Seg_4627" s="T139">dempro-pro:case</ta>
            <ta e="T141" id="Seg_4628" s="T140">v-v&gt;n-n:poss-n:case</ta>
            <ta e="T142" id="Seg_4629" s="T141">v-v:tense-v:poss.pn</ta>
            <ta e="T143" id="Seg_4630" s="T142">v-v:cvb</ta>
            <ta e="T144" id="Seg_4631" s="T143">v-v:cvb</ta>
            <ta e="T145" id="Seg_4632" s="T144">n-n:poss-n:case</ta>
            <ta e="T146" id="Seg_4633" s="T145">v-v:tense-v:pred.pn</ta>
            <ta e="T147" id="Seg_4634" s="T146">dempro-pro:case</ta>
            <ta e="T148" id="Seg_4635" s="T147">propr-n:(poss)-n:case</ta>
            <ta e="T149" id="Seg_4636" s="T148">v-v:tense-v:pred.pn</ta>
            <ta e="T150" id="Seg_4637" s="T149">ptcl</ta>
            <ta e="T151" id="Seg_4638" s="T150">v-v:mood.pn</ta>
            <ta e="T152" id="Seg_4639" s="T151">v-v:mood.pn</ta>
            <ta e="T153" id="Seg_4640" s="T152">v-v:cvb</ta>
            <ta e="T154" id="Seg_4641" s="T153">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T155" id="Seg_4642" s="T154">n-n:poss-n:case</ta>
            <ta e="T156" id="Seg_4643" s="T155">dempro-pro:(poss)-pro:case</ta>
            <ta e="T157" id="Seg_4644" s="T156">v-v:cvb</ta>
            <ta e="T158" id="Seg_4645" s="T157">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T159" id="Seg_4646" s="T158">n-n:(poss)-n:case</ta>
            <ta e="T160" id="Seg_4647" s="T159">v-v:tense-v:pred.pn</ta>
            <ta e="T161" id="Seg_4648" s="T160">pers-pro:case</ta>
            <ta e="T162" id="Seg_4649" s="T161">v-v:cvb</ta>
            <ta e="T163" id="Seg_4650" s="T162">adv-adv</ta>
            <ta e="T164" id="Seg_4651" s="T163">v-v:ptcp</ta>
            <ta e="T165" id="Seg_4652" s="T164">v-v:tense-v:pred.pn</ta>
            <ta e="T166" id="Seg_4653" s="T165">n-n:case</ta>
            <ta e="T167" id="Seg_4654" s="T166">n-n:(poss)-n:case</ta>
            <ta e="T168" id="Seg_4655" s="T167">adv</ta>
            <ta e="T169" id="Seg_4656" s="T168">v-v:ptcp</ta>
            <ta e="T170" id="Seg_4657" s="T169">v-v:(neg)-v:pred.pn</ta>
            <ta e="T171" id="Seg_4658" s="T170">ptcl</ta>
            <ta e="T172" id="Seg_4659" s="T171">v-v:cvb</ta>
            <ta e="T173" id="Seg_4660" s="T172">v-v:tense-v:pred.pn</ta>
            <ta e="T174" id="Seg_4661" s="T173">adv</ta>
            <ta e="T175" id="Seg_4662" s="T174">pers-pro:case</ta>
            <ta e="T176" id="Seg_4663" s="T175">n-n:poss-n:case</ta>
            <ta e="T177" id="Seg_4664" s="T176">v-v:cvb</ta>
            <ta e="T178" id="Seg_4665" s="T177">v-v:mood.pn</ta>
            <ta e="T179" id="Seg_4666" s="T178">v-v:tense-v:pred.pn</ta>
            <ta e="T180" id="Seg_4667" s="T179">ptcl</ta>
            <ta e="T181" id="Seg_4668" s="T180">ptcl</ta>
            <ta e="T182" id="Seg_4669" s="T181">dempro</ta>
            <ta e="T183" id="Seg_4670" s="T182">n-n:case</ta>
            <ta e="T184" id="Seg_4671" s="T183">adv</ta>
            <ta e="T185" id="Seg_4672" s="T184">v-v:cvb</ta>
            <ta e="T186" id="Seg_4673" s="T185">n-n&gt;v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T187" id="Seg_4674" s="T186">dempro</ta>
            <ta e="T188" id="Seg_4675" s="T187">v-v:cvb</ta>
            <ta e="T189" id="Seg_4676" s="T188">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T190" id="Seg_4677" s="T189">que-pro:case</ta>
            <ta e="T191" id="Seg_4678" s="T190">conj</ta>
            <ta e="T192" id="Seg_4679" s="T191">n-n&gt;v-v:cvb</ta>
            <ta e="T193" id="Seg_4680" s="T192">propr-n:(poss)-n:case</ta>
            <ta e="T194" id="Seg_4681" s="T193">n-n:poss-n:case</ta>
            <ta e="T195" id="Seg_4682" s="T194">v-v:tense-v:pred.pn</ta>
            <ta e="T196" id="Seg_4683" s="T195">dempro-pro:case</ta>
            <ta e="T197" id="Seg_4684" s="T196">ptcl</ta>
            <ta e="T198" id="Seg_4685" s="T197">n-n:(poss)-n:case</ta>
            <ta e="T199" id="Seg_4686" s="T198">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T200" id="Seg_4687" s="T199">v-v:tense-v:pred.pn</ta>
            <ta e="T201" id="Seg_4688" s="T200">ptcl</ta>
            <ta e="T202" id="Seg_4689" s="T201">n-n:case</ta>
            <ta e="T203" id="Seg_4690" s="T202">v-v:mood.pn</ta>
            <ta e="T204" id="Seg_4691" s="T203">v-v:cvb</ta>
            <ta e="T205" id="Seg_4692" s="T204">ptcl</ta>
            <ta e="T206" id="Seg_4693" s="T205">v-v:tense-v:poss.pn</ta>
            <ta e="T207" id="Seg_4694" s="T206">ptcl-ptcl:(poss.pn)</ta>
            <ta e="T208" id="Seg_4695" s="T207">que</ta>
            <ta e="T209" id="Seg_4696" s="T208">adj-n:case-ptcl</ta>
            <ta e="T210" id="Seg_4697" s="T209">adv</ta>
            <ta e="T211" id="Seg_4698" s="T210">n-n&gt;adj-n:case</ta>
            <ta e="T212" id="Seg_4699" s="T211">ptcl</ta>
            <ta e="T213" id="Seg_4700" s="T212">pers-pro:case</ta>
            <ta e="T214" id="Seg_4701" s="T213">v-v:mood-v:(ins)-v:poss.pn</ta>
            <ta e="T215" id="Seg_4702" s="T214">v-v:cvb</ta>
            <ta e="T216" id="Seg_4703" s="T215">n-n:poss-n:case</ta>
            <ta e="T217" id="Seg_4704" s="T216">ptcl</ta>
            <ta e="T218" id="Seg_4705" s="T217">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T219" id="Seg_4706" s="T218">v-v:cvb</ta>
            <ta e="T220" id="Seg_4707" s="T219">v-v:cvb</ta>
            <ta e="T221" id="Seg_4708" s="T220">post</ta>
            <ta e="T222" id="Seg_4709" s="T221">n-n:case</ta>
            <ta e="T223" id="Seg_4710" s="T222">n-n:case</ta>
            <ta e="T224" id="Seg_4711" s="T223">v-v:tense-v:pred.pn</ta>
            <ta e="T225" id="Seg_4712" s="T224">que</ta>
            <ta e="T226" id="Seg_4713" s="T225">ptcl</ta>
            <ta e="T227" id="Seg_4714" s="T226">adv</ta>
            <ta e="T228" id="Seg_4715" s="T227">cardnum</ta>
            <ta e="T229" id="Seg_4716" s="T228">n-n&gt;adj-n:case</ta>
            <ta e="T230" id="Seg_4717" s="T229">v-v:(ins)-v:(neg)-v:mood.pn</ta>
            <ta e="T231" id="Seg_4718" s="T230">n-n:poss-n:case</ta>
            <ta e="T232" id="Seg_4719" s="T231">v-v:(neg)-v:tense-v:poss.pn</ta>
            <ta e="T233" id="Seg_4720" s="T232">v-v:cvb</ta>
            <ta e="T234" id="Seg_4721" s="T233">adv</ta>
            <ta e="T235" id="Seg_4722" s="T234">ptcl</ta>
            <ta e="T236" id="Seg_4723" s="T235">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T237" id="Seg_4724" s="T236">v-v:tense-v:poss.pn</ta>
            <ta e="T238" id="Seg_4725" s="T237">v-v:tense-v:pred.pn</ta>
            <ta e="T239" id="Seg_4726" s="T238">n-n:poss-n:case</ta>
            <ta e="T240" id="Seg_4727" s="T239">cardnum-cardnum&gt;ordnum</ta>
            <ta e="T241" id="Seg_4728" s="T240">v-v&gt;n-n:poss-n:case</ta>
            <ta e="T242" id="Seg_4729" s="T241">v-v:tense-v:pred.pn</ta>
            <ta e="T243" id="Seg_4730" s="T242">dempro</ta>
            <ta e="T244" id="Seg_4731" s="T243">v-v:cvb</ta>
            <ta e="T245" id="Seg_4732" s="T244">propr-n:case</ta>
            <ta e="T246" id="Seg_4733" s="T245">n-n:poss-n:case</ta>
            <ta e="T247" id="Seg_4734" s="T246">v-v:tense-v:pred.pn</ta>
            <ta e="T248" id="Seg_4735" s="T247">dempro</ta>
            <ta e="T249" id="Seg_4736" s="T248">v-v:cvb</ta>
            <ta e="T250" id="Seg_4737" s="T249">v-v:cvb</ta>
            <ta e="T251" id="Seg_4738" s="T250">n-n:poss-n:case</ta>
            <ta e="T252" id="Seg_4739" s="T251">n-n:poss-n:case</ta>
            <ta e="T253" id="Seg_4740" s="T252">n-n:poss-n:case</ta>
            <ta e="T254" id="Seg_4741" s="T253">v-v:cvb</ta>
            <ta e="T255" id="Seg_4742" s="T254">propr-n:poss-n:case</ta>
            <ta e="T256" id="Seg_4743" s="T255">n-n:(poss)-n:case</ta>
            <ta e="T257" id="Seg_4744" s="T256">v-v:cvb</ta>
            <ta e="T258" id="Seg_4745" s="T257">v-v:cvb</ta>
            <ta e="T259" id="Seg_4746" s="T258">dempro</ta>
            <ta e="T260" id="Seg_4747" s="T259">adj-n:poss-n:case</ta>
            <ta e="T261" id="Seg_4748" s="T260">adv-adv&gt;adv</ta>
            <ta e="T262" id="Seg_4749" s="T261">v-v:tense-v:pred.pn-v-v:tense-v:pred.pn</ta>
            <ta e="T263" id="Seg_4750" s="T262">dempro-pro:case</ta>
            <ta e="T264" id="Seg_4751" s="T263">ptcl</ta>
            <ta e="T265" id="Seg_4752" s="T264">n-n:poss-n:case</ta>
            <ta e="T266" id="Seg_4753" s="T265">v-v:tense-v:pred.pn</ta>
            <ta e="T267" id="Seg_4754" s="T266">ptcl</ta>
            <ta e="T268" id="Seg_4755" s="T267">n-n:case</ta>
            <ta e="T269" id="Seg_4756" s="T268">pers-pro:case</ta>
            <ta e="T270" id="Seg_4757" s="T269">pers-pro:case</ta>
            <ta e="T271" id="Seg_4758" s="T270">v-v:cvb</ta>
            <ta e="T272" id="Seg_4759" s="T271">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T273" id="Seg_4760" s="T272">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T274" id="Seg_4761" s="T273">v-v:neg-v:(ins)-v:poss.pn</ta>
            <ta e="T275" id="Seg_4762" s="T274">que</ta>
            <ta e="T276" id="Seg_4763" s="T275">ptcl</ta>
            <ta e="T277" id="Seg_4764" s="T276">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T278" id="Seg_4765" s="T277">ptcl</ta>
            <ta e="T279" id="Seg_4766" s="T278">adj-n:case-ptcl</ta>
            <ta e="T280" id="Seg_4767" s="T279">ptcl</ta>
            <ta e="T281" id="Seg_4768" s="T280">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T282" id="Seg_4769" s="T281">n-n:poss-n:case</ta>
            <ta e="T283" id="Seg_4770" s="T282">n-n:case</ta>
            <ta e="T284" id="Seg_4771" s="T283">v-v:mood-v:poss.pn</ta>
            <ta e="T285" id="Seg_4772" s="T284">n-n:poss-n:case</ta>
            <ta e="T286" id="Seg_4773" s="T285">adv</ta>
            <ta e="T287" id="Seg_4774" s="T286">pers-pro:case</ta>
            <ta e="T288" id="Seg_4775" s="T287">n-n:poss-n:case</ta>
            <ta e="T289" id="Seg_4776" s="T288">post</ta>
            <ta e="T290" id="Seg_4777" s="T289">v-v:tense-v:poss.pn</ta>
            <ta e="T291" id="Seg_4778" s="T290">que-pro:case</ta>
            <ta e="T292" id="Seg_4779" s="T291">ptcl</ta>
            <ta e="T293" id="Seg_4780" s="T292">adj-n:case</ta>
            <ta e="T294" id="Seg_4781" s="T293">v-v:tense-v:poss.pn</ta>
            <ta e="T295" id="Seg_4782" s="T294">ptcl-ptcl:(poss.pn)</ta>
            <ta e="T296" id="Seg_4783" s="T295">adj</ta>
            <ta e="T297" id="Seg_4784" s="T296">n-n:case</ta>
            <ta e="T298" id="Seg_4785" s="T297">n-n:case</ta>
            <ta e="T299" id="Seg_4786" s="T298">v-v:cvb-v:pred.pn</ta>
            <ta e="T300" id="Seg_4787" s="T299">dempro-pro:case</ta>
            <ta e="T301" id="Seg_4788" s="T300">adj</ta>
            <ta e="T302" id="Seg_4789" s="T301">n-n:case</ta>
            <ta e="T303" id="Seg_4790" s="T302">v-v:tense-v:poss.pn</ta>
            <ta e="T304" id="Seg_4791" s="T303">v-v:tense-v:pred.pn</ta>
            <ta e="T305" id="Seg_4792" s="T304">adj</ta>
            <ta e="T306" id="Seg_4793" s="T305">adj</ta>
            <ta e="T307" id="Seg_4794" s="T306">adj</ta>
            <ta e="T308" id="Seg_4795" s="T307">n-n:case</ta>
            <ta e="T309" id="Seg_4796" s="T308">adv</ta>
            <ta e="T310" id="Seg_4797" s="T309">cardnum</ta>
            <ta e="T311" id="Seg_4798" s="T310">n-n:poss-n:case</ta>
            <ta e="T312" id="Seg_4799" s="T311">n</ta>
            <ta e="T313" id="Seg_4800" s="T312">v-v:mood.pn</ta>
            <ta e="T314" id="Seg_4801" s="T313">adj</ta>
            <ta e="T315" id="Seg_4802" s="T314">adj</ta>
            <ta e="T316" id="Seg_4803" s="T315">n-n:case</ta>
            <ta e="T317" id="Seg_4804" s="T316">v-v:mood.pn</ta>
            <ta e="T318" id="Seg_4805" s="T317">dempro</ta>
            <ta e="T319" id="Seg_4806" s="T318">n-n:case</ta>
            <ta e="T320" id="Seg_4807" s="T319">n-n:poss-n:case</ta>
            <ta e="T321" id="Seg_4808" s="T320">adj-n:poss-n:case</ta>
            <ta e="T322" id="Seg_4809" s="T321">adj</ta>
            <ta e="T323" id="Seg_4810" s="T322">n-n:case</ta>
            <ta e="T324" id="Seg_4811" s="T323">n-n:poss-n:case</ta>
            <ta e="T325" id="Seg_4812" s="T324">v-v:mood.pn</ta>
            <ta e="T326" id="Seg_4813" s="T325">dempro-pro:case</ta>
            <ta e="T327" id="Seg_4814" s="T326">v-v:cvb</ta>
            <ta e="T328" id="Seg_4815" s="T327">v-v:tense-v:poss.pn</ta>
            <ta e="T329" id="Seg_4816" s="T328">n-n:(poss)-n:case</ta>
            <ta e="T330" id="Seg_4817" s="T329">adj</ta>
            <ta e="T331" id="Seg_4818" s="T330">que-pro:case</ta>
            <ta e="T332" id="Seg_4819" s="T331">ptcl</ta>
            <ta e="T333" id="Seg_4820" s="T332">v-v:tense-v:poss.pn</ta>
            <ta e="T334" id="Seg_4821" s="T333">ptcl-ptcl:(mood)-ptcl:pred.pn</ta>
            <ta e="T335" id="Seg_4822" s="T334">v-v:tense-v:pred.pn</ta>
            <ta e="T336" id="Seg_4823" s="T335">n-n:(num)-n:case</ta>
            <ta e="T337" id="Seg_4824" s="T336">n-n:case</ta>
            <ta e="T338" id="Seg_4825" s="T337">v-v:cvb</ta>
            <ta e="T339" id="Seg_4826" s="T338">v-v:(neg)-v:mood.pn</ta>
            <ta e="T340" id="Seg_4827" s="T339">que-pro:case</ta>
            <ta e="T341" id="Seg_4828" s="T340">n-n:poss-n:case</ta>
            <ta e="T342" id="Seg_4829" s="T341">v-v:cvb</ta>
            <ta e="T343" id="Seg_4830" s="T342">v-v:tense-v:pred.pn</ta>
            <ta e="T344" id="Seg_4831" s="T343">adv</ta>
            <ta e="T345" id="Seg_4832" s="T344">v-v:mood.pn</ta>
            <ta e="T346" id="Seg_4833" s="T345">v-v:tense-v:pred.pn</ta>
            <ta e="T347" id="Seg_4834" s="T346">cardnum-cardnum&gt;ordnum</ta>
            <ta e="T348" id="Seg_4835" s="T347">n-n:poss-n:case</ta>
            <ta e="T349" id="Seg_4836" s="T348">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T350" id="Seg_4837" s="T349">post</ta>
            <ta e="T351" id="Seg_4838" s="T350">v-v:tense-v:pred.pn</ta>
            <ta e="T352" id="Seg_4839" s="T351">adv</ta>
            <ta e="T353" id="Seg_4840" s="T352">n-n:case</ta>
            <ta e="T354" id="Seg_4841" s="T353">v-v:cvb</ta>
            <ta e="T355" id="Seg_4842" s="T354">v-v:tense-v:pred.pn</ta>
            <ta e="T356" id="Seg_4843" s="T355">adj</ta>
            <ta e="T357" id="Seg_4844" s="T356">ptcl</ta>
            <ta e="T358" id="Seg_4845" s="T357">n-n:case</ta>
            <ta e="T359" id="Seg_4846" s="T358">n-n:case</ta>
            <ta e="T360" id="Seg_4847" s="T359">v-v:tense-v:pred.pn</ta>
            <ta e="T361" id="Seg_4848" s="T360">n-n:case</ta>
            <ta e="T362" id="Seg_4849" s="T361">n-n:(poss)-n:case</ta>
            <ta e="T363" id="Seg_4850" s="T362">v-v:cvb</ta>
            <ta e="T364" id="Seg_4851" s="T363">ptcl</ta>
            <ta e="T365" id="Seg_4852" s="T364">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T366" id="Seg_4853" s="T365">que-pro:case</ta>
            <ta e="T367" id="Seg_4854" s="T366">ptcl</ta>
            <ta e="T368" id="Seg_4855" s="T367">n-n:case</ta>
            <ta e="T369" id="Seg_4856" s="T368">post</ta>
            <ta e="T370" id="Seg_4857" s="T369">n-n:case</ta>
            <ta e="T371" id="Seg_4858" s="T370">n-n:poss-n:case</ta>
            <ta e="T372" id="Seg_4859" s="T371">v-v:ptcp-v:(case)</ta>
            <ta e="T373" id="Seg_4860" s="T372">post</ta>
            <ta e="T374" id="Seg_4861" s="T373">v-v:tense-v:pred.pn</ta>
            <ta e="T375" id="Seg_4862" s="T374">dempro-pro:case</ta>
            <ta e="T376" id="Seg_4863" s="T375">post</ta>
            <ta e="T377" id="Seg_4864" s="T376">n-n:(poss)</ta>
            <ta e="T378" id="Seg_4865" s="T377">ptcl</ta>
            <ta e="T379" id="Seg_4866" s="T378">v-v:tense-v:pred.pn</ta>
            <ta e="T380" id="Seg_4867" s="T379">dempro-pro:case</ta>
            <ta e="T381" id="Seg_4868" s="T380">n-n:case</ta>
            <ta e="T382" id="Seg_4869" s="T381">v-v:cvb</ta>
            <ta e="T383" id="Seg_4870" s="T382">v-v:tense-v:poss.pn</ta>
            <ta e="T384" id="Seg_4871" s="T383">n-n:case</ta>
            <ta e="T385" id="Seg_4872" s="T384">conj</ta>
            <ta e="T386" id="Seg_4873" s="T385">adj</ta>
            <ta e="T387" id="Seg_4874" s="T386">v-v:ptcp</ta>
            <ta e="T388" id="Seg_4875" s="T387">conj</ta>
            <ta e="T389" id="Seg_4876" s="T388">n-n:case</ta>
            <ta e="T390" id="Seg_4877" s="T389">v-v:cvb</ta>
            <ta e="T391" id="Seg_4878" s="T390">v-v:tense-v:pred.pn</ta>
            <ta e="T392" id="Seg_4879" s="T391">v-v:tense-v:pred.pn</ta>
            <ta e="T393" id="Seg_4880" s="T392">dempro-pro:case</ta>
            <ta e="T394" id="Seg_4881" s="T393">adj-n:case</ta>
            <ta e="T395" id="Seg_4882" s="T394">v-v:tense-v:poss.pn</ta>
            <ta e="T396" id="Seg_4883" s="T395">dempro-pro:case</ta>
            <ta e="T397" id="Seg_4884" s="T396">propr-n:case</ta>
            <ta e="T398" id="Seg_4885" s="T397">n-n:case</ta>
            <ta e="T399" id="Seg_4886" s="T398">v-v:cvb</ta>
            <ta e="T400" id="Seg_4887" s="T399">adv</ta>
            <ta e="T401" id="Seg_4888" s="T400">ptcl</ta>
            <ta e="T402" id="Seg_4889" s="T401">v-v:ptcp</ta>
            <ta e="T403" id="Seg_4890" s="T402">v-v:tense-v:pred.pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_4891" s="T0">adv</ta>
            <ta e="T2" id="Seg_4892" s="T1">n</ta>
            <ta e="T3" id="Seg_4893" s="T2">adj</ta>
            <ta e="T4" id="Seg_4894" s="T3">ptcl</ta>
            <ta e="T5" id="Seg_4895" s="T4">adj</ta>
            <ta e="T6" id="Seg_4896" s="T5">n</ta>
            <ta e="T7" id="Seg_4897" s="T6">dempro</ta>
            <ta e="T8" id="Seg_4898" s="T7">n</ta>
            <ta e="T9" id="Seg_4899" s="T8">n</ta>
            <ta e="T10" id="Seg_4900" s="T9">ptcl</ta>
            <ta e="T11" id="Seg_4901" s="T10">dempro</ta>
            <ta e="T12" id="Seg_4902" s="T11">n</ta>
            <ta e="T13" id="Seg_4903" s="T12">adv</ta>
            <ta e="T14" id="Seg_4904" s="T13">v</ta>
            <ta e="T15" id="Seg_4905" s="T14">que</ta>
            <ta e="T16" id="Seg_4906" s="T15">v</ta>
            <ta e="T17" id="Seg_4907" s="T16">v</ta>
            <ta e="T18" id="Seg_4908" s="T17">dempro</ta>
            <ta e="T19" id="Seg_4909" s="T18">v</ta>
            <ta e="T20" id="Seg_4910" s="T19">pers</ta>
            <ta e="T21" id="Seg_4911" s="T20">dempro</ta>
            <ta e="T22" id="Seg_4912" s="T21">n</ta>
            <ta e="T23" id="Seg_4913" s="T22">n</ta>
            <ta e="T24" id="Seg_4914" s="T23">v</ta>
            <ta e="T25" id="Seg_4915" s="T24">n</ta>
            <ta e="T26" id="Seg_4916" s="T25">adv</ta>
            <ta e="T27" id="Seg_4917" s="T26">propr</ta>
            <ta e="T28" id="Seg_4918" s="T27">v</ta>
            <ta e="T29" id="Seg_4919" s="T28">ptcl</ta>
            <ta e="T30" id="Seg_4920" s="T29">dempro</ta>
            <ta e="T31" id="Seg_4921" s="T30">n</ta>
            <ta e="T32" id="Seg_4922" s="T31">ptcl</ta>
            <ta e="T33" id="Seg_4923" s="T32">v</ta>
            <ta e="T34" id="Seg_4924" s="T33">adv</ta>
            <ta e="T35" id="Seg_4925" s="T34">v</ta>
            <ta e="T36" id="Seg_4926" s="T35">aux</ta>
            <ta e="T37" id="Seg_4927" s="T36">v</ta>
            <ta e="T38" id="Seg_4928" s="T37">dempro</ta>
            <ta e="T39" id="Seg_4929" s="T38">n</ta>
            <ta e="T40" id="Seg_4930" s="T39">v</ta>
            <ta e="T41" id="Seg_4931" s="T40">v</ta>
            <ta e="T42" id="Seg_4932" s="T41">v</ta>
            <ta e="T43" id="Seg_4933" s="T42">aux</ta>
            <ta e="T44" id="Seg_4934" s="T43">adv</ta>
            <ta e="T45" id="Seg_4935" s="T44">n</ta>
            <ta e="T46" id="Seg_4936" s="T45">n</ta>
            <ta e="T47" id="Seg_4937" s="T46">n</ta>
            <ta e="T48" id="Seg_4938" s="T47">v</ta>
            <ta e="T49" id="Seg_4939" s="T48">pers</ta>
            <ta e="T50" id="Seg_4940" s="T49">pers</ta>
            <ta e="T51" id="Seg_4941" s="T50">n</ta>
            <ta e="T52" id="Seg_4942" s="T51">ptcl</ta>
            <ta e="T53" id="Seg_4943" s="T52">propr</ta>
            <ta e="T54" id="Seg_4944" s="T53">n</ta>
            <ta e="T55" id="Seg_4945" s="T54">n</ta>
            <ta e="T56" id="Seg_4946" s="T55">v</ta>
            <ta e="T57" id="Seg_4947" s="T56">v</ta>
            <ta e="T58" id="Seg_4948" s="T57">v</ta>
            <ta e="T59" id="Seg_4949" s="T58">dempro</ta>
            <ta e="T60" id="Seg_4950" s="T59">n</ta>
            <ta e="T61" id="Seg_4951" s="T60">v</ta>
            <ta e="T62" id="Seg_4952" s="T61">v</ta>
            <ta e="T63" id="Seg_4953" s="T62">que</ta>
            <ta e="T64" id="Seg_4954" s="T63">n</ta>
            <ta e="T65" id="Seg_4955" s="T64">que</ta>
            <ta e="T66" id="Seg_4956" s="T65">ptcl</ta>
            <ta e="T67" id="Seg_4957" s="T66">dempro</ta>
            <ta e="T68" id="Seg_4958" s="T67">n</ta>
            <ta e="T69" id="Seg_4959" s="T68">v</ta>
            <ta e="T70" id="Seg_4960" s="T69">v</ta>
            <ta e="T71" id="Seg_4961" s="T70">n</ta>
            <ta e="T72" id="Seg_4962" s="T71">v</ta>
            <ta e="T73" id="Seg_4963" s="T72">dempro</ta>
            <ta e="T74" id="Seg_4964" s="T73">n</ta>
            <ta e="T75" id="Seg_4965" s="T74">cardnum</ta>
            <ta e="T76" id="Seg_4966" s="T75">n</ta>
            <ta e="T77" id="Seg_4967" s="T76">v</ta>
            <ta e="T78" id="Seg_4968" s="T77">pers</ta>
            <ta e="T79" id="Seg_4969" s="T78">v</ta>
            <ta e="T80" id="Seg_4970" s="T79">pers</ta>
            <ta e="T81" id="Seg_4971" s="T80">pers</ta>
            <ta e="T82" id="Seg_4972" s="T81">n</ta>
            <ta e="T83" id="Seg_4973" s="T82">v</ta>
            <ta e="T84" id="Seg_4974" s="T83">n</ta>
            <ta e="T85" id="Seg_4975" s="T84">adv</ta>
            <ta e="T86" id="Seg_4976" s="T85">propr</ta>
            <ta e="T87" id="Seg_4977" s="T86">n</ta>
            <ta e="T88" id="Seg_4978" s="T87">ptcl</ta>
            <ta e="T89" id="Seg_4979" s="T88">v</ta>
            <ta e="T90" id="Seg_4980" s="T89">v</ta>
            <ta e="T91" id="Seg_4981" s="T90">n</ta>
            <ta e="T92" id="Seg_4982" s="T91">v</ta>
            <ta e="T93" id="Seg_4983" s="T92">ptcl</ta>
            <ta e="T94" id="Seg_4984" s="T93">ptcl</ta>
            <ta e="T95" id="Seg_4985" s="T94">v</ta>
            <ta e="T96" id="Seg_4986" s="T95">aux</ta>
            <ta e="T97" id="Seg_4987" s="T96">dempro</ta>
            <ta e="T98" id="Seg_4988" s="T97">n</ta>
            <ta e="T99" id="Seg_4989" s="T98">v</ta>
            <ta e="T100" id="Seg_4990" s="T99">v</ta>
            <ta e="T101" id="Seg_4991" s="T100">propr</ta>
            <ta e="T102" id="Seg_4992" s="T101">n</ta>
            <ta e="T103" id="Seg_4993" s="T102">adv</ta>
            <ta e="T104" id="Seg_4994" s="T103">v</ta>
            <ta e="T105" id="Seg_4995" s="T104">que</ta>
            <ta e="T106" id="Seg_4996" s="T105">v</ta>
            <ta e="T107" id="Seg_4997" s="T106">n</ta>
            <ta e="T108" id="Seg_4998" s="T107">n</ta>
            <ta e="T109" id="Seg_4999" s="T108">n</ta>
            <ta e="T110" id="Seg_5000" s="T109">ptcl</ta>
            <ta e="T111" id="Seg_5001" s="T110">v</ta>
            <ta e="T112" id="Seg_5002" s="T111">v</ta>
            <ta e="T113" id="Seg_5003" s="T112">pers</ta>
            <ta e="T114" id="Seg_5004" s="T113">n</ta>
            <ta e="T115" id="Seg_5005" s="T114">n</ta>
            <ta e="T116" id="Seg_5006" s="T115">n</ta>
            <ta e="T117" id="Seg_5007" s="T116">v</ta>
            <ta e="T118" id="Seg_5008" s="T117">v</ta>
            <ta e="T119" id="Seg_5009" s="T118">v</ta>
            <ta e="T120" id="Seg_5010" s="T119">n</ta>
            <ta e="T121" id="Seg_5011" s="T120">v</ta>
            <ta e="T122" id="Seg_5012" s="T121">v</ta>
            <ta e="T123" id="Seg_5013" s="T122">v</ta>
            <ta e="T124" id="Seg_5014" s="T123">aux</ta>
            <ta e="T125" id="Seg_5015" s="T124">propr</ta>
            <ta e="T126" id="Seg_5016" s="T125">dempro</ta>
            <ta e="T127" id="Seg_5017" s="T126">ptcl</ta>
            <ta e="T128" id="Seg_5018" s="T127">v</ta>
            <ta e="T129" id="Seg_5019" s="T128">v</ta>
            <ta e="T130" id="Seg_5020" s="T129">pers</ta>
            <ta e="T131" id="Seg_5021" s="T130">n</ta>
            <ta e="T132" id="Seg_5022" s="T131">v</ta>
            <ta e="T133" id="Seg_5023" s="T132">pers</ta>
            <ta e="T134" id="Seg_5024" s="T133">post</ta>
            <ta e="T135" id="Seg_5025" s="T134">n</ta>
            <ta e="T136" id="Seg_5026" s="T135">adj</ta>
            <ta e="T137" id="Seg_5027" s="T136">cop</ta>
            <ta e="T138" id="Seg_5028" s="T137">n</ta>
            <ta e="T139" id="Seg_5029" s="T138">v</ta>
            <ta e="T140" id="Seg_5030" s="T139">dempro</ta>
            <ta e="T141" id="Seg_5031" s="T140">n</ta>
            <ta e="T142" id="Seg_5032" s="T141">v</ta>
            <ta e="T143" id="Seg_5033" s="T142">v</ta>
            <ta e="T144" id="Seg_5034" s="T143">v</ta>
            <ta e="T145" id="Seg_5035" s="T144">n</ta>
            <ta e="T146" id="Seg_5036" s="T145">v</ta>
            <ta e="T147" id="Seg_5037" s="T146">dempro</ta>
            <ta e="T148" id="Seg_5038" s="T147">propr</ta>
            <ta e="T149" id="Seg_5039" s="T148">v</ta>
            <ta e="T150" id="Seg_5040" s="T149">ptcl</ta>
            <ta e="T151" id="Seg_5041" s="T150">v</ta>
            <ta e="T152" id="Seg_5042" s="T151">v</ta>
            <ta e="T153" id="Seg_5043" s="T152">v</ta>
            <ta e="T154" id="Seg_5044" s="T153">v</ta>
            <ta e="T155" id="Seg_5045" s="T154">n</ta>
            <ta e="T156" id="Seg_5046" s="T155">dempro</ta>
            <ta e="T157" id="Seg_5047" s="T156">v</ta>
            <ta e="T158" id="Seg_5048" s="T157">v</ta>
            <ta e="T159" id="Seg_5049" s="T158">n</ta>
            <ta e="T160" id="Seg_5050" s="T159">v</ta>
            <ta e="T161" id="Seg_5051" s="T160">pers</ta>
            <ta e="T162" id="Seg_5052" s="T161">v</ta>
            <ta e="T163" id="Seg_5053" s="T162">adv</ta>
            <ta e="T164" id="Seg_5054" s="T163">v</ta>
            <ta e="T165" id="Seg_5055" s="T164">aux</ta>
            <ta e="T166" id="Seg_5056" s="T165">n</ta>
            <ta e="T167" id="Seg_5057" s="T166">n</ta>
            <ta e="T168" id="Seg_5058" s="T167">adv</ta>
            <ta e="T169" id="Seg_5059" s="T168">v</ta>
            <ta e="T170" id="Seg_5060" s="T169">aux</ta>
            <ta e="T171" id="Seg_5061" s="T170">ptcl</ta>
            <ta e="T172" id="Seg_5062" s="T171">v</ta>
            <ta e="T173" id="Seg_5063" s="T172">v</ta>
            <ta e="T174" id="Seg_5064" s="T173">adv</ta>
            <ta e="T175" id="Seg_5065" s="T174">pers</ta>
            <ta e="T176" id="Seg_5066" s="T175">n</ta>
            <ta e="T177" id="Seg_5067" s="T176">v</ta>
            <ta e="T178" id="Seg_5068" s="T177">aux</ta>
            <ta e="T179" id="Seg_5069" s="T178">v</ta>
            <ta e="T180" id="Seg_5070" s="T179">ptcl</ta>
            <ta e="T181" id="Seg_5071" s="T180">ptcl</ta>
            <ta e="T182" id="Seg_5072" s="T181">dempro</ta>
            <ta e="T183" id="Seg_5073" s="T182">n</ta>
            <ta e="T184" id="Seg_5074" s="T183">adv</ta>
            <ta e="T185" id="Seg_5075" s="T184">v</ta>
            <ta e="T186" id="Seg_5076" s="T185">v</ta>
            <ta e="T187" id="Seg_5077" s="T186">dempro</ta>
            <ta e="T188" id="Seg_5078" s="T187">v</ta>
            <ta e="T189" id="Seg_5079" s="T188">v</ta>
            <ta e="T190" id="Seg_5080" s="T189">que</ta>
            <ta e="T191" id="Seg_5081" s="T190">conj</ta>
            <ta e="T192" id="Seg_5082" s="T191">v</ta>
            <ta e="T193" id="Seg_5083" s="T192">propr</ta>
            <ta e="T194" id="Seg_5084" s="T193">n</ta>
            <ta e="T195" id="Seg_5085" s="T194">v</ta>
            <ta e="T196" id="Seg_5086" s="T195">dempro</ta>
            <ta e="T197" id="Seg_5087" s="T196">ptcl</ta>
            <ta e="T198" id="Seg_5088" s="T197">n</ta>
            <ta e="T199" id="Seg_5089" s="T198">v</ta>
            <ta e="T200" id="Seg_5090" s="T199">v</ta>
            <ta e="T201" id="Seg_5091" s="T200">ptcl</ta>
            <ta e="T202" id="Seg_5092" s="T201">n</ta>
            <ta e="T203" id="Seg_5093" s="T202">v</ta>
            <ta e="T204" id="Seg_5094" s="T203">v</ta>
            <ta e="T205" id="Seg_5095" s="T204">ptcl</ta>
            <ta e="T206" id="Seg_5096" s="T205">v</ta>
            <ta e="T207" id="Seg_5097" s="T206">ptcl</ta>
            <ta e="T208" id="Seg_5098" s="T207">que</ta>
            <ta e="T209" id="Seg_5099" s="T208">adj</ta>
            <ta e="T210" id="Seg_5100" s="T209">adv</ta>
            <ta e="T211" id="Seg_5101" s="T210">adj</ta>
            <ta e="T212" id="Seg_5102" s="T211">ptcl</ta>
            <ta e="T213" id="Seg_5103" s="T212">pers</ta>
            <ta e="T214" id="Seg_5104" s="T213">v</ta>
            <ta e="T215" id="Seg_5105" s="T214">v</ta>
            <ta e="T216" id="Seg_5106" s="T215">n</ta>
            <ta e="T217" id="Seg_5107" s="T216">ptcl</ta>
            <ta e="T218" id="Seg_5108" s="T217">v</ta>
            <ta e="T219" id="Seg_5109" s="T218">v</ta>
            <ta e="T220" id="Seg_5110" s="T219">v</ta>
            <ta e="T221" id="Seg_5111" s="T220">post</ta>
            <ta e="T222" id="Seg_5112" s="T221">n</ta>
            <ta e="T223" id="Seg_5113" s="T222">n</ta>
            <ta e="T224" id="Seg_5114" s="T223">v</ta>
            <ta e="T225" id="Seg_5115" s="T224">que</ta>
            <ta e="T226" id="Seg_5116" s="T225">ptcl</ta>
            <ta e="T227" id="Seg_5117" s="T226">adv</ta>
            <ta e="T228" id="Seg_5118" s="T227">cardnum</ta>
            <ta e="T229" id="Seg_5119" s="T228">adj</ta>
            <ta e="T230" id="Seg_5120" s="T229">v</ta>
            <ta e="T231" id="Seg_5121" s="T230">n</ta>
            <ta e="T232" id="Seg_5122" s="T231">v</ta>
            <ta e="T233" id="Seg_5123" s="T232">v</ta>
            <ta e="T234" id="Seg_5124" s="T233">adv</ta>
            <ta e="T235" id="Seg_5125" s="T234">ptcl</ta>
            <ta e="T236" id="Seg_5126" s="T235">v</ta>
            <ta e="T237" id="Seg_5127" s="T236">v</ta>
            <ta e="T238" id="Seg_5128" s="T237">v</ta>
            <ta e="T239" id="Seg_5129" s="T238">n</ta>
            <ta e="T240" id="Seg_5130" s="T239">ordnum</ta>
            <ta e="T241" id="Seg_5131" s="T240">n</ta>
            <ta e="T242" id="Seg_5132" s="T241">v</ta>
            <ta e="T243" id="Seg_5133" s="T242">dempro</ta>
            <ta e="T244" id="Seg_5134" s="T243">v</ta>
            <ta e="T245" id="Seg_5135" s="T244">propr</ta>
            <ta e="T246" id="Seg_5136" s="T245">n</ta>
            <ta e="T247" id="Seg_5137" s="T246">v</ta>
            <ta e="T248" id="Seg_5138" s="T247">dempro</ta>
            <ta e="T249" id="Seg_5139" s="T248">v</ta>
            <ta e="T250" id="Seg_5140" s="T249">v</ta>
            <ta e="T251" id="Seg_5141" s="T250">n</ta>
            <ta e="T252" id="Seg_5142" s="T251">n</ta>
            <ta e="T253" id="Seg_5143" s="T252">n</ta>
            <ta e="T254" id="Seg_5144" s="T253">v</ta>
            <ta e="T255" id="Seg_5145" s="T254">propr</ta>
            <ta e="T256" id="Seg_5146" s="T255">n</ta>
            <ta e="T257" id="Seg_5147" s="T256">v</ta>
            <ta e="T258" id="Seg_5148" s="T257">v</ta>
            <ta e="T259" id="Seg_5149" s="T258">dempro</ta>
            <ta e="T260" id="Seg_5150" s="T259">n</ta>
            <ta e="T261" id="Seg_5151" s="T260">adv</ta>
            <ta e="T262" id="Seg_5152" s="T261">v</ta>
            <ta e="T263" id="Seg_5153" s="T262">dempro</ta>
            <ta e="T264" id="Seg_5154" s="T263">ptcl</ta>
            <ta e="T265" id="Seg_5155" s="T264">n</ta>
            <ta e="T266" id="Seg_5156" s="T265">v</ta>
            <ta e="T267" id="Seg_5157" s="T266">ptcl</ta>
            <ta e="T268" id="Seg_5158" s="T267">n</ta>
            <ta e="T269" id="Seg_5159" s="T268">pers</ta>
            <ta e="T270" id="Seg_5160" s="T269">pers</ta>
            <ta e="T271" id="Seg_5161" s="T270">v</ta>
            <ta e="T272" id="Seg_5162" s="T271">emphpro</ta>
            <ta e="T273" id="Seg_5163" s="T272">n</ta>
            <ta e="T274" id="Seg_5164" s="T273">v</ta>
            <ta e="T275" id="Seg_5165" s="T274">que</ta>
            <ta e="T276" id="Seg_5166" s="T275">ptcl</ta>
            <ta e="T277" id="Seg_5167" s="T276">n</ta>
            <ta e="T278" id="Seg_5168" s="T277">ptcl</ta>
            <ta e="T279" id="Seg_5169" s="T278">adj</ta>
            <ta e="T280" id="Seg_5170" s="T279">ptcl</ta>
            <ta e="T281" id="Seg_5171" s="T280">emphpro</ta>
            <ta e="T282" id="Seg_5172" s="T281">n</ta>
            <ta e="T283" id="Seg_5173" s="T282">n</ta>
            <ta e="T284" id="Seg_5174" s="T283">v</ta>
            <ta e="T285" id="Seg_5175" s="T284">n</ta>
            <ta e="T286" id="Seg_5176" s="T285">adv</ta>
            <ta e="T287" id="Seg_5177" s="T286">pers</ta>
            <ta e="T288" id="Seg_5178" s="T287">n</ta>
            <ta e="T289" id="Seg_5179" s="T288">post</ta>
            <ta e="T290" id="Seg_5180" s="T289">v</ta>
            <ta e="T291" id="Seg_5181" s="T290">que</ta>
            <ta e="T292" id="Seg_5182" s="T291">ptcl</ta>
            <ta e="T293" id="Seg_5183" s="T292">adj</ta>
            <ta e="T294" id="Seg_5184" s="T293">v</ta>
            <ta e="T295" id="Seg_5185" s="T294">ptcl</ta>
            <ta e="T296" id="Seg_5186" s="T295">adj</ta>
            <ta e="T297" id="Seg_5187" s="T296">n</ta>
            <ta e="T298" id="Seg_5188" s="T297">n</ta>
            <ta e="T299" id="Seg_5189" s="T298">v</ta>
            <ta e="T300" id="Seg_5190" s="T299">dempro</ta>
            <ta e="T301" id="Seg_5191" s="T300">adj</ta>
            <ta e="T302" id="Seg_5192" s="T301">n</ta>
            <ta e="T303" id="Seg_5193" s="T302">v</ta>
            <ta e="T304" id="Seg_5194" s="T303">v</ta>
            <ta e="T305" id="Seg_5195" s="T304">adj</ta>
            <ta e="T306" id="Seg_5196" s="T305">adj</ta>
            <ta e="T307" id="Seg_5197" s="T306">adj</ta>
            <ta e="T308" id="Seg_5198" s="T307">n</ta>
            <ta e="T309" id="Seg_5199" s="T308">adv</ta>
            <ta e="T310" id="Seg_5200" s="T309">cardnum</ta>
            <ta e="T311" id="Seg_5201" s="T310">n</ta>
            <ta e="T312" id="Seg_5202" s="T311">n</ta>
            <ta e="T313" id="Seg_5203" s="T312">v</ta>
            <ta e="T314" id="Seg_5204" s="T313">adj</ta>
            <ta e="T315" id="Seg_5205" s="T314">adj</ta>
            <ta e="T316" id="Seg_5206" s="T315">n</ta>
            <ta e="T317" id="Seg_5207" s="T316">v</ta>
            <ta e="T318" id="Seg_5208" s="T317">dempro</ta>
            <ta e="T319" id="Seg_5209" s="T318">n</ta>
            <ta e="T320" id="Seg_5210" s="T319">n</ta>
            <ta e="T321" id="Seg_5211" s="T320">adj</ta>
            <ta e="T322" id="Seg_5212" s="T321">adj</ta>
            <ta e="T323" id="Seg_5213" s="T322">n</ta>
            <ta e="T324" id="Seg_5214" s="T323">n</ta>
            <ta e="T325" id="Seg_5215" s="T324">v</ta>
            <ta e="T326" id="Seg_5216" s="T325">dempro</ta>
            <ta e="T327" id="Seg_5217" s="T326">v</ta>
            <ta e="T328" id="Seg_5218" s="T327">v</ta>
            <ta e="T329" id="Seg_5219" s="T328">n</ta>
            <ta e="T330" id="Seg_5220" s="T329">adj</ta>
            <ta e="T331" id="Seg_5221" s="T330">que</ta>
            <ta e="T332" id="Seg_5222" s="T331">ptcl</ta>
            <ta e="T333" id="Seg_5223" s="T332">v</ta>
            <ta e="T334" id="Seg_5224" s="T333">ptcl</ta>
            <ta e="T335" id="Seg_5225" s="T334">v</ta>
            <ta e="T336" id="Seg_5226" s="T335">n</ta>
            <ta e="T337" id="Seg_5227" s="T336">n</ta>
            <ta e="T338" id="Seg_5228" s="T337">v</ta>
            <ta e="T339" id="Seg_5229" s="T338">v</ta>
            <ta e="T340" id="Seg_5230" s="T339">que</ta>
            <ta e="T341" id="Seg_5231" s="T340">n</ta>
            <ta e="T342" id="Seg_5232" s="T341">v</ta>
            <ta e="T343" id="Seg_5233" s="T342">v</ta>
            <ta e="T344" id="Seg_5234" s="T343">adv</ta>
            <ta e="T345" id="Seg_5235" s="T344">v</ta>
            <ta e="T346" id="Seg_5236" s="T345">v</ta>
            <ta e="T347" id="Seg_5237" s="T346">ordnum</ta>
            <ta e="T348" id="Seg_5238" s="T347">n</ta>
            <ta e="T349" id="Seg_5239" s="T348">v</ta>
            <ta e="T350" id="Seg_5240" s="T349">post</ta>
            <ta e="T351" id="Seg_5241" s="T350">v</ta>
            <ta e="T352" id="Seg_5242" s="T351">adv</ta>
            <ta e="T353" id="Seg_5243" s="T352">n</ta>
            <ta e="T354" id="Seg_5244" s="T353">v</ta>
            <ta e="T355" id="Seg_5245" s="T354">aux</ta>
            <ta e="T356" id="Seg_5246" s="T355">adj</ta>
            <ta e="T357" id="Seg_5247" s="T356">ptcl</ta>
            <ta e="T358" id="Seg_5248" s="T357">n</ta>
            <ta e="T359" id="Seg_5249" s="T358">n</ta>
            <ta e="T360" id="Seg_5250" s="T359">v</ta>
            <ta e="T361" id="Seg_5251" s="T360">n</ta>
            <ta e="T362" id="Seg_5252" s="T361">n</ta>
            <ta e="T363" id="Seg_5253" s="T362">v</ta>
            <ta e="T364" id="Seg_5254" s="T363">ptcl</ta>
            <ta e="T365" id="Seg_5255" s="T364">v</ta>
            <ta e="T366" id="Seg_5256" s="T365">que</ta>
            <ta e="T367" id="Seg_5257" s="T366">ptcl</ta>
            <ta e="T368" id="Seg_5258" s="T367">n</ta>
            <ta e="T369" id="Seg_5259" s="T368">post</ta>
            <ta e="T370" id="Seg_5260" s="T369">n</ta>
            <ta e="T371" id="Seg_5261" s="T370">n</ta>
            <ta e="T372" id="Seg_5262" s="T371">v</ta>
            <ta e="T373" id="Seg_5263" s="T372">post</ta>
            <ta e="T374" id="Seg_5264" s="T373">v</ta>
            <ta e="T375" id="Seg_5265" s="T374">dempro</ta>
            <ta e="T376" id="Seg_5266" s="T375">post</ta>
            <ta e="T377" id="Seg_5267" s="T376">n</ta>
            <ta e="T378" id="Seg_5268" s="T377">ptcl</ta>
            <ta e="T379" id="Seg_5269" s="T378">cop</ta>
            <ta e="T380" id="Seg_5270" s="T379">dempro</ta>
            <ta e="T381" id="Seg_5271" s="T380">n</ta>
            <ta e="T382" id="Seg_5272" s="T381">v</ta>
            <ta e="T383" id="Seg_5273" s="T382">v</ta>
            <ta e="T384" id="Seg_5274" s="T383">n</ta>
            <ta e="T385" id="Seg_5275" s="T384">conj</ta>
            <ta e="T386" id="Seg_5276" s="T385">adj</ta>
            <ta e="T387" id="Seg_5277" s="T386">v</ta>
            <ta e="T388" id="Seg_5278" s="T387">conj</ta>
            <ta e="T389" id="Seg_5279" s="T388">n</ta>
            <ta e="T390" id="Seg_5280" s="T389">v</ta>
            <ta e="T391" id="Seg_5281" s="T390">v</ta>
            <ta e="T392" id="Seg_5282" s="T391">aux</ta>
            <ta e="T393" id="Seg_5283" s="T392">dempro</ta>
            <ta e="T394" id="Seg_5284" s="T393">n</ta>
            <ta e="T395" id="Seg_5285" s="T394">v</ta>
            <ta e="T396" id="Seg_5286" s="T395">dempro</ta>
            <ta e="T397" id="Seg_5287" s="T396">propr</ta>
            <ta e="T398" id="Seg_5288" s="T397">n</ta>
            <ta e="T399" id="Seg_5289" s="T398">v</ta>
            <ta e="T400" id="Seg_5290" s="T399">adv</ta>
            <ta e="T401" id="Seg_5291" s="T400">ptcl</ta>
            <ta e="T402" id="Seg_5292" s="T401">v</ta>
            <ta e="T403" id="Seg_5293" s="T402">aux</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_5294" s="T0">adv:Time</ta>
            <ta e="T2" id="Seg_5295" s="T1">np:L</ta>
            <ta e="T3" id="Seg_5296" s="T2">0.3.h:Poss np.h:Th</ta>
            <ta e="T8" id="Seg_5297" s="T7">np.h:Poss</ta>
            <ta e="T9" id="Seg_5298" s="T8">np.h:Th</ta>
            <ta e="T11" id="Seg_5299" s="T10">pro.h:R</ta>
            <ta e="T12" id="Seg_5300" s="T11">np.h:A</ta>
            <ta e="T15" id="Seg_5301" s="T14">pro:Cau</ta>
            <ta e="T16" id="Seg_5302" s="T15">0.2.h:A</ta>
            <ta e="T17" id="Seg_5303" s="T16">0.3.h:A</ta>
            <ta e="T19" id="Seg_5304" s="T18">0.3.h:A</ta>
            <ta e="T20" id="Seg_5305" s="T19">pro.h:Th</ta>
            <ta e="T22" id="Seg_5306" s="T21">np:Poss</ta>
            <ta e="T23" id="Seg_5307" s="T22">np.h:Th</ta>
            <ta e="T24" id="Seg_5308" s="T23">0.1.h:A</ta>
            <ta e="T26" id="Seg_5309" s="T25">adv:L</ta>
            <ta e="T27" id="Seg_5310" s="T26">np.h:Th</ta>
            <ta e="T30" id="Seg_5311" s="T29">pro.h:Poss</ta>
            <ta e="T31" id="Seg_5312" s="T30">np.h:Th</ta>
            <ta e="T33" id="Seg_5313" s="T32">0.1.h:A</ta>
            <ta e="T36" id="Seg_5314" s="T35">0.1.h:A</ta>
            <ta e="T37" id="Seg_5315" s="T36">0.3.h:A</ta>
            <ta e="T39" id="Seg_5316" s="T38">np.h:E</ta>
            <ta e="T41" id="Seg_5317" s="T40">0.3.h:A</ta>
            <ta e="T45" id="Seg_5318" s="T44">np:L</ta>
            <ta e="T46" id="Seg_5319" s="T45">np.h:A</ta>
            <ta e="T47" id="Seg_5320" s="T46">np.h:Th</ta>
            <ta e="T49" id="Seg_5321" s="T48">pro.h:A</ta>
            <ta e="T50" id="Seg_5322" s="T49">pro.h:B</ta>
            <ta e="T51" id="Seg_5323" s="T50">np:L</ta>
            <ta e="T53" id="Seg_5324" s="T52">np.h:Poss</ta>
            <ta e="T54" id="Seg_5325" s="T53">np.h:Th</ta>
            <ta e="T56" id="Seg_5326" s="T55">0.1.h:A</ta>
            <ta e="T58" id="Seg_5327" s="T57">0.3.h:A</ta>
            <ta e="T59" id="Seg_5328" s="T58">pro:St</ta>
            <ta e="T60" id="Seg_5329" s="T59">np.h:E</ta>
            <ta e="T61" id="Seg_5330" s="T60">0.3.h:E</ta>
            <ta e="T68" id="Seg_5331" s="T67">np:So</ta>
            <ta e="T71" id="Seg_5332" s="T70">0.3:Th</ta>
            <ta e="T72" id="Seg_5333" s="T71">0.3.h:A</ta>
            <ta e="T74" id="Seg_5334" s="T73">np.h:A</ta>
            <ta e="T76" id="Seg_5335" s="T75">np:Th</ta>
            <ta e="T78" id="Seg_5336" s="T77">pro.h:A</ta>
            <ta e="T80" id="Seg_5337" s="T79">pro.h:A</ta>
            <ta e="T81" id="Seg_5338" s="T80">pro.h:P</ta>
            <ta e="T82" id="Seg_5339" s="T81">0.2.h:Poss np:P</ta>
            <ta e="T85" id="Seg_5340" s="T84">adv:Time</ta>
            <ta e="T86" id="Seg_5341" s="T85">np.h:Poss</ta>
            <ta e="T87" id="Seg_5342" s="T86">np.h:Th</ta>
            <ta e="T89" id="Seg_5343" s="T88">0.2.h:A</ta>
            <ta e="T90" id="Seg_5344" s="T89">0.3.h:A</ta>
            <ta e="T91" id="Seg_5345" s="T90">np.h:A</ta>
            <ta e="T92" id="Seg_5346" s="T91">0.3.h:P</ta>
            <ta e="T98" id="Seg_5347" s="T97">np.h:A</ta>
            <ta e="T99" id="Seg_5348" s="T98">np:St</ta>
            <ta e="T100" id="Seg_5349" s="T99">0.3.h:E</ta>
            <ta e="T102" id="Seg_5350" s="T101">np.h:E</ta>
            <ta e="T107" id="Seg_5351" s="T106">np:Poss</ta>
            <ta e="T108" id="Seg_5352" s="T107">np.h:A</ta>
            <ta e="T112" id="Seg_5353" s="T111">0.2.h:A</ta>
            <ta e="T113" id="Seg_5354" s="T112">pro.h:R</ta>
            <ta e="T115" id="Seg_5355" s="T114">0.1.h:Poss np:Th</ta>
            <ta e="T116" id="Seg_5356" s="T115">np:P</ta>
            <ta e="T118" id="Seg_5357" s="T117">0.1.h:A</ta>
            <ta e="T119" id="Seg_5358" s="T118">0.3.h:A</ta>
            <ta e="T120" id="Seg_5359" s="T119">np.h:A</ta>
            <ta e="T122" id="Seg_5360" s="T121">0.3.h:A</ta>
            <ta e="T125" id="Seg_5361" s="T124">np.h:Th</ta>
            <ta e="T129" id="Seg_5362" s="T128">0.3.h:A</ta>
            <ta e="T130" id="Seg_5363" s="T129">pro.h:A</ta>
            <ta e="T131" id="Seg_5364" s="T130">np:Cau</ta>
            <ta e="T135" id="Seg_5365" s="T134">np:L</ta>
            <ta e="T136" id="Seg_5366" s="T135">np.h:Th</ta>
            <ta e="T137" id="Seg_5367" s="T136">0.1.h:Poss</ta>
            <ta e="T140" id="Seg_5368" s="T139">pro.h:Poss</ta>
            <ta e="T141" id="Seg_5369" s="T140">np:Cau</ta>
            <ta e="T142" id="Seg_5370" s="T141">0.1.h:A</ta>
            <ta e="T143" id="Seg_5371" s="T142">0.3.h:A</ta>
            <ta e="T144" id="Seg_5372" s="T143">0.3.h:A</ta>
            <ta e="T145" id="Seg_5373" s="T144">0.3.h:Poss np:Th</ta>
            <ta e="T146" id="Seg_5374" s="T145">0.3.h:A</ta>
            <ta e="T148" id="Seg_5375" s="T147">np.h:A</ta>
            <ta e="T151" id="Seg_5376" s="T150">0.2.h:A</ta>
            <ta e="T152" id="Seg_5377" s="T151">0.2.h:A</ta>
            <ta e="T153" id="Seg_5378" s="T152">0.3.h:A</ta>
            <ta e="T154" id="Seg_5379" s="T153">0.3.h:A</ta>
            <ta e="T155" id="Seg_5380" s="T154">np.h:Th</ta>
            <ta e="T156" id="Seg_5381" s="T155">pro.h:A</ta>
            <ta e="T159" id="Seg_5382" s="T158">np.h:A</ta>
            <ta e="T161" id="Seg_5383" s="T160">pro.h:A</ta>
            <ta e="T162" id="Seg_5384" s="T161">0.2.h:A</ta>
            <ta e="T163" id="Seg_5385" s="T162">adv:G</ta>
            <ta e="T167" id="Seg_5386" s="T166">np:Th</ta>
            <ta e="T168" id="Seg_5387" s="T167">adv:L</ta>
            <ta e="T172" id="Seg_5388" s="T171">0.3.h:A</ta>
            <ta e="T173" id="Seg_5389" s="T172">0.3.h:A</ta>
            <ta e="T175" id="Seg_5390" s="T174">pro.h:R</ta>
            <ta e="T176" id="Seg_5391" s="T175">0.3.h:Poss np.h:Th</ta>
            <ta e="T178" id="Seg_5392" s="T177">0.2.h:A</ta>
            <ta e="T179" id="Seg_5393" s="T178">0.3.h:A</ta>
            <ta e="T183" id="Seg_5394" s="T182">np.h:A</ta>
            <ta e="T184" id="Seg_5395" s="T183">adv:Time</ta>
            <ta e="T188" id="Seg_5396" s="T187">0.3.h:A</ta>
            <ta e="T189" id="Seg_5397" s="T188">0.3.h:Th</ta>
            <ta e="T193" id="Seg_5398" s="T192">np.h:E</ta>
            <ta e="T198" id="Seg_5399" s="T197">np.h:E</ta>
            <ta e="T199" id="Seg_5400" s="T198">0.3.h:A</ta>
            <ta e="T202" id="Seg_5401" s="T201">np:Th</ta>
            <ta e="T203" id="Seg_5402" s="T202">0.2.h:A</ta>
            <ta e="T204" id="Seg_5403" s="T203">0.3.h:A</ta>
            <ta e="T206" id="Seg_5404" s="T205">0.1.h:A</ta>
            <ta e="T209" id="Seg_5405" s="T208">0.3.h:Th</ta>
            <ta e="T213" id="Seg_5406" s="T212">np.h:P</ta>
            <ta e="T215" id="Seg_5407" s="T214">0.3.h:A</ta>
            <ta e="T216" id="Seg_5408" s="T215">np.h:Th</ta>
            <ta e="T218" id="Seg_5409" s="T217">0.3.h:A</ta>
            <ta e="T220" id="Seg_5410" s="T219">0.3.h:A</ta>
            <ta e="T222" id="Seg_5411" s="T221">np.h:A</ta>
            <ta e="T223" id="Seg_5412" s="T222">np.h:R</ta>
            <ta e="T230" id="Seg_5413" s="T229">0.2.h:A</ta>
            <ta e="T231" id="Seg_5414" s="T230">0.1.h:Poss np:Th</ta>
            <ta e="T232" id="Seg_5415" s="T231">0.3.h:A</ta>
            <ta e="T233" id="Seg_5416" s="T232">0.2.h:A</ta>
            <ta e="T234" id="Seg_5417" s="T233">adv:Time</ta>
            <ta e="T237" id="Seg_5418" s="T236">0.1.h:A</ta>
            <ta e="T238" id="Seg_5419" s="T237">0.3.h:A</ta>
            <ta e="T239" id="Seg_5420" s="T238">n:Time</ta>
            <ta e="T241" id="Seg_5421" s="T240">0.3.h:Poss np:Th</ta>
            <ta e="T242" id="Seg_5422" s="T241">0.3.h:A</ta>
            <ta e="T244" id="Seg_5423" s="T243">0.3.h:A</ta>
            <ta e="T245" id="Seg_5424" s="T244">np.h:Poss</ta>
            <ta e="T246" id="Seg_5425" s="T245">np:G</ta>
            <ta e="T247" id="Seg_5426" s="T246">0.3.h:Th</ta>
            <ta e="T249" id="Seg_5427" s="T248">0.3.h:Th</ta>
            <ta e="T250" id="Seg_5428" s="T249">0.3.h:A</ta>
            <ta e="T251" id="Seg_5429" s="T250">np:Poss</ta>
            <ta e="T252" id="Seg_5430" s="T251">np:Poss</ta>
            <ta e="T253" id="Seg_5431" s="T252">np.h:R</ta>
            <ta e="T254" id="Seg_5432" s="T253">0.3.h:A</ta>
            <ta e="T255" id="Seg_5433" s="T254">np.h:Poss</ta>
            <ta e="T256" id="Seg_5434" s="T255">np:G</ta>
            <ta e="T258" id="Seg_5435" s="T257">0.3.h:A</ta>
            <ta e="T262" id="Seg_5436" s="T261">0.3.h:A</ta>
            <ta e="T266" id="Seg_5437" s="T265">0.3.h:E</ta>
            <ta e="T269" id="Seg_5438" s="T268">pro.h:A</ta>
            <ta e="T270" id="Seg_5439" s="T269">pro.h:P</ta>
            <ta e="T275" id="Seg_5440" s="T274">pro:Cau</ta>
            <ta e="T277" id="Seg_5441" s="T276">0.2.h:Poss np.h:Th</ta>
            <ta e="T281" id="Seg_5442" s="T280">pro.h:Poss</ta>
            <ta e="T282" id="Seg_5443" s="T281">np.h:R</ta>
            <ta e="T283" id="Seg_5444" s="T282">np:Th</ta>
            <ta e="T284" id="Seg_5445" s="T283">0.3.h:A</ta>
            <ta e="T285" id="Seg_5446" s="T284">0.2.h:Poss np:So</ta>
            <ta e="T287" id="Seg_5447" s="T286">pro.h:Th</ta>
            <ta e="T289" id="Seg_5448" s="T287">0.2.h:Poss pp:Com</ta>
            <ta e="T291" id="Seg_5449" s="T290">pro.h:Th</ta>
            <ta e="T297" id="Seg_5450" s="T296">np:So</ta>
            <ta e="T298" id="Seg_5451" s="T297">np.h:A</ta>
            <ta e="T300" id="Seg_5452" s="T299">pro:So</ta>
            <ta e="T302" id="Seg_5453" s="T301">np.h:Th</ta>
            <ta e="T304" id="Seg_5454" s="T303">0.3.h:A</ta>
            <ta e="T308" id="Seg_5455" s="T307">np:Th</ta>
            <ta e="T311" id="Seg_5456" s="T310">n:Time</ta>
            <ta e="T312" id="Seg_5457" s="T311">adv:Time</ta>
            <ta e="T313" id="Seg_5458" s="T312">0.3.h:A</ta>
            <ta e="T316" id="Seg_5459" s="T315">np:P</ta>
            <ta e="T317" id="Seg_5460" s="T316">0.3.h:A</ta>
            <ta e="T319" id="Seg_5461" s="T318">np:Poss</ta>
            <ta e="T320" id="Seg_5462" s="T319">np:L</ta>
            <ta e="T323" id="Seg_5463" s="T322">np:Poss</ta>
            <ta e="T324" id="Seg_5464" s="T323">np:Th</ta>
            <ta e="T325" id="Seg_5465" s="T324">0.3.h:A</ta>
            <ta e="T326" id="Seg_5466" s="T325">pro:Path</ta>
            <ta e="T327" id="Seg_5467" s="T326">0.3.h:A</ta>
            <ta e="T329" id="Seg_5468" s="T328">0.2.h:Poss np.h:A</ta>
            <ta e="T331" id="Seg_5469" s="T330">pro:Th</ta>
            <ta e="T335" id="Seg_5470" s="T334">0.3.h:A</ta>
            <ta e="T336" id="Seg_5471" s="T335">np.h:A</ta>
            <ta e="T338" id="Seg_5472" s="T337">0.3.h:E</ta>
            <ta e="T340" id="Seg_5473" s="T339">pro.h:A</ta>
            <ta e="T341" id="Seg_5474" s="T340">0.3.h:Poss np:Th</ta>
            <ta e="T345" id="Seg_5475" s="T344">0.3.h:A</ta>
            <ta e="T346" id="Seg_5476" s="T345">0.3.h:A</ta>
            <ta e="T348" id="Seg_5477" s="T347">n:Time</ta>
            <ta e="T349" id="Seg_5478" s="T348">0.3.h:A</ta>
            <ta e="T351" id="Seg_5479" s="T350">0.3.h:A</ta>
            <ta e="T353" id="Seg_5480" s="T352">np.h:A</ta>
            <ta e="T359" id="Seg_5481" s="T358">np:Th</ta>
            <ta e="T361" id="Seg_5482" s="T360">np:Poss</ta>
            <ta e="T362" id="Seg_5483" s="T361">np:St</ta>
            <ta e="T376" id="Seg_5484" s="T374">pp:Time</ta>
            <ta e="T379" id="Seg_5485" s="T378">0.3:Th</ta>
            <ta e="T381" id="Seg_5486" s="T380">np.h:E</ta>
            <ta e="T382" id="Seg_5487" s="T381">0.3.h:A</ta>
            <ta e="T384" id="Seg_5488" s="T383">np.h:Th</ta>
            <ta e="T389" id="Seg_5489" s="T388">np.h:Th</ta>
            <ta e="T394" id="Seg_5490" s="T393">np:Th</ta>
            <ta e="T397" id="Seg_5491" s="T396">np.h:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T3" id="Seg_5492" s="T2">0.3.h:S n:pred</ta>
            <ta e="T8" id="Seg_5493" s="T7">np.h:S</ta>
            <ta e="T10" id="Seg_5494" s="T9">ptcl:pred</ta>
            <ta e="T11" id="Seg_5495" s="T10">pro.h:O</ta>
            <ta e="T12" id="Seg_5496" s="T11">np.h:S</ta>
            <ta e="T14" id="Seg_5497" s="T13">v:pred</ta>
            <ta e="T16" id="Seg_5498" s="T15">0.2.h:S v:pred</ta>
            <ta e="T17" id="Seg_5499" s="T16">s:adv</ta>
            <ta e="T19" id="Seg_5500" s="T18">0.3.h:S v:pred</ta>
            <ta e="T20" id="Seg_5501" s="T19">pro.h:S</ta>
            <ta e="T24" id="Seg_5502" s="T20">s:rel</ta>
            <ta e="T25" id="Seg_5503" s="T24">n:pred</ta>
            <ta e="T27" id="Seg_5504" s="T26">np.h:S</ta>
            <ta e="T29" id="Seg_5505" s="T28">ptcl:pred</ta>
            <ta e="T33" id="Seg_5506" s="T29">s:cond</ta>
            <ta e="T36" id="Seg_5507" s="T35">0.1.h:S v:pred</ta>
            <ta e="T37" id="Seg_5508" s="T36">0.3.h:S v:pred</ta>
            <ta e="T39" id="Seg_5509" s="T38">np.h:S</ta>
            <ta e="T41" id="Seg_5510" s="T40">0.3.h:S v:pred</ta>
            <ta e="T43" id="Seg_5511" s="T42">v:pred</ta>
            <ta e="T46" id="Seg_5512" s="T45">np.h:S</ta>
            <ta e="T47" id="Seg_5513" s="T46">np.h:O</ta>
            <ta e="T48" id="Seg_5514" s="T47">v:pred</ta>
            <ta e="T49" id="Seg_5515" s="T48">pro.h:S</ta>
            <ta e="T56" id="Seg_5516" s="T50">s:purp</ta>
            <ta e="T57" id="Seg_5517" s="T56">v:pred</ta>
            <ta e="T58" id="Seg_5518" s="T57">0.3.h:S v:pred</ta>
            <ta e="T60" id="Seg_5519" s="T59">np.h:S</ta>
            <ta e="T61" id="Seg_5520" s="T60">s:adv</ta>
            <ta e="T62" id="Seg_5521" s="T61">v:pred</ta>
            <ta e="T70" id="Seg_5522" s="T66">s:rel</ta>
            <ta e="T71" id="Seg_5523" s="T70">0.3:S n:pred</ta>
            <ta e="T72" id="Seg_5524" s="T71">0.3.h:S v:pred</ta>
            <ta e="T74" id="Seg_5525" s="T73">np.h:S</ta>
            <ta e="T76" id="Seg_5526" s="T75">np:O</ta>
            <ta e="T77" id="Seg_5527" s="T76">v:pred</ta>
            <ta e="T78" id="Seg_5528" s="T77">pro.h:S</ta>
            <ta e="T79" id="Seg_5529" s="T78">v:pred</ta>
            <ta e="T80" id="Seg_5530" s="T79">pro.h:S</ta>
            <ta e="T81" id="Seg_5531" s="T80">pro.h:O</ta>
            <ta e="T83" id="Seg_5532" s="T82">v:pred</ta>
            <ta e="T88" id="Seg_5533" s="T83">s:comp</ta>
            <ta e="T89" id="Seg_5534" s="T88">0.2.h:S v:pred</ta>
            <ta e="T90" id="Seg_5535" s="T89">0.3.h:S v:pred</ta>
            <ta e="T91" id="Seg_5536" s="T90">np.h:S</ta>
            <ta e="T93" id="Seg_5537" s="T91">s:adv</ta>
            <ta e="T96" id="Seg_5538" s="T95">v:pred</ta>
            <ta e="T100" id="Seg_5539" s="T96">s:temp</ta>
            <ta e="T102" id="Seg_5540" s="T101">np.h:S</ta>
            <ta e="T104" id="Seg_5541" s="T103">v:pred</ta>
            <ta e="T108" id="Seg_5542" s="T107">np.h:S</ta>
            <ta e="T111" id="Seg_5543" s="T110">v:pred</ta>
            <ta e="T112" id="Seg_5544" s="T111">0.2.h:S v:pred</ta>
            <ta e="T115" id="Seg_5545" s="T114">np:O</ta>
            <ta e="T118" id="Seg_5546" s="T115">s:purp</ta>
            <ta e="T119" id="Seg_5547" s="T118">0.3.h:S v:pred</ta>
            <ta e="T120" id="Seg_5548" s="T119">np.h:S</ta>
            <ta e="T121" id="Seg_5549" s="T120">v:pred</ta>
            <ta e="T122" id="Seg_5550" s="T121">s:adv</ta>
            <ta e="T124" id="Seg_5551" s="T123">v:pred</ta>
            <ta e="T125" id="Seg_5552" s="T124">np.h:S</ta>
            <ta e="T129" id="Seg_5553" s="T128">0.3.h:S v:pred</ta>
            <ta e="T130" id="Seg_5554" s="T129">pro.h:S</ta>
            <ta e="T132" id="Seg_5555" s="T131">v:pred</ta>
            <ta e="T136" id="Seg_5556" s="T135">adj:pred</ta>
            <ta e="T137" id="Seg_5557" s="T136">0.1.h:S cop</ta>
            <ta e="T139" id="Seg_5558" s="T137">s:adv</ta>
            <ta e="T142" id="Seg_5559" s="T141">0.1.h:S v:pred</ta>
            <ta e="T143" id="Seg_5560" s="T142">s:adv</ta>
            <ta e="T144" id="Seg_5561" s="T143">s:adv</ta>
            <ta e="T145" id="Seg_5562" s="T144">np:O</ta>
            <ta e="T146" id="Seg_5563" s="T145">0.3.h:S v:pred</ta>
            <ta e="T148" id="Seg_5564" s="T147">np.h:S</ta>
            <ta e="T149" id="Seg_5565" s="T148">v:pred</ta>
            <ta e="T151" id="Seg_5566" s="T150">0.2.h:S v:pred</ta>
            <ta e="T152" id="Seg_5567" s="T151">0.2.h:S v:pred</ta>
            <ta e="T153" id="Seg_5568" s="T152">s:adv</ta>
            <ta e="T154" id="Seg_5569" s="T153">0.3.h:S v:pred</ta>
            <ta e="T155" id="Seg_5570" s="T154">np.h:O</ta>
            <ta e="T158" id="Seg_5571" s="T155">s:temp</ta>
            <ta e="T159" id="Seg_5572" s="T158">np.h:S</ta>
            <ta e="T160" id="Seg_5573" s="T159">v:pred</ta>
            <ta e="T161" id="Seg_5574" s="T160">pro.h:S</ta>
            <ta e="T162" id="Seg_5575" s="T161">s:adv</ta>
            <ta e="T165" id="Seg_5576" s="T164">v:pred</ta>
            <ta e="T167" id="Seg_5577" s="T166">np:S</ta>
            <ta e="T170" id="Seg_5578" s="T169">v:pred</ta>
            <ta e="T172" id="Seg_5579" s="T171">s:adv</ta>
            <ta e="T173" id="Seg_5580" s="T172">0.3.h:S v:pred</ta>
            <ta e="T176" id="Seg_5581" s="T175">np.h:O</ta>
            <ta e="T178" id="Seg_5582" s="T177">0.2.h:S v:pred</ta>
            <ta e="T179" id="Seg_5583" s="T178">0.3.h:S v:pred</ta>
            <ta e="T183" id="Seg_5584" s="T182">np.h:S</ta>
            <ta e="T185" id="Seg_5585" s="T184">s:adv</ta>
            <ta e="T186" id="Seg_5586" s="T185">v:pred</ta>
            <ta e="T189" id="Seg_5587" s="T186">s:temp</ta>
            <ta e="T193" id="Seg_5588" s="T192">np.h:S</ta>
            <ta e="T194" id="Seg_5589" s="T193">np.h:O</ta>
            <ta e="T195" id="Seg_5590" s="T194">v:pred</ta>
            <ta e="T198" id="Seg_5591" s="T197">np.h:S</ta>
            <ta e="T199" id="Seg_5592" s="T198">s:adv</ta>
            <ta e="T200" id="Seg_5593" s="T199">v:pred</ta>
            <ta e="T202" id="Seg_5594" s="T201">np:O</ta>
            <ta e="T203" id="Seg_5595" s="T202">0.2.h:S v:pred</ta>
            <ta e="T204" id="Seg_5596" s="T203">s:adv</ta>
            <ta e="T206" id="Seg_5597" s="T205">0.1.h:S v:pred</ta>
            <ta e="T209" id="Seg_5598" s="T208">0.3.h:S adj:pred</ta>
            <ta e="T213" id="Seg_5599" s="T212">np.h:S</ta>
            <ta e="T214" id="Seg_5600" s="T213">v:pred</ta>
            <ta e="T215" id="Seg_5601" s="T214">s:adv</ta>
            <ta e="T216" id="Seg_5602" s="T215">np.h:O</ta>
            <ta e="T218" id="Seg_5603" s="T217">0.3.h:S v:pred</ta>
            <ta e="T221" id="Seg_5604" s="T218">s:temp</ta>
            <ta e="T222" id="Seg_5605" s="T221">np.h:S</ta>
            <ta e="T224" id="Seg_5606" s="T223">v:pred</ta>
            <ta e="T230" id="Seg_5607" s="T229">0.2.h:S v:pred</ta>
            <ta e="T233" id="Seg_5608" s="T230">s:adv</ta>
            <ta e="T236" id="Seg_5609" s="T235">s:adv</ta>
            <ta e="T237" id="Seg_5610" s="T236">0.1.h:S v:pred</ta>
            <ta e="T238" id="Seg_5611" s="T237">0.3.h:S v:pred</ta>
            <ta e="T241" id="Seg_5612" s="T240">np:O</ta>
            <ta e="T242" id="Seg_5613" s="T241">0.3.h:S v:pred</ta>
            <ta e="T244" id="Seg_5614" s="T242">s:adv</ta>
            <ta e="T247" id="Seg_5615" s="T246">0.3.h:S v:pred</ta>
            <ta e="T249" id="Seg_5616" s="T247">s:temp</ta>
            <ta e="T250" id="Seg_5617" s="T249">s:adv</ta>
            <ta e="T254" id="Seg_5618" s="T250">s:adv</ta>
            <ta e="T258" id="Seg_5619" s="T254">s:adv</ta>
            <ta e="T262" id="Seg_5620" s="T261">0.3.h:S v:pred</ta>
            <ta e="T266" id="Seg_5621" s="T265">0.3.h:S v:pred</ta>
            <ta e="T269" id="Seg_5622" s="T268">pro.h:S</ta>
            <ta e="T270" id="Seg_5623" s="T269">pro.h:O</ta>
            <ta e="T271" id="Seg_5624" s="T270">s:purp</ta>
            <ta e="T274" id="Seg_5625" s="T273">v:pred</ta>
            <ta e="T277" id="Seg_5626" s="T276">np.h:S</ta>
            <ta e="T279" id="Seg_5627" s="T278">adj:pred</ta>
            <ta e="T283" id="Seg_5628" s="T282">np:O</ta>
            <ta e="T284" id="Seg_5629" s="T283">0.3.h:S v:pred</ta>
            <ta e="T287" id="Seg_5630" s="T286">pro.h:S</ta>
            <ta e="T290" id="Seg_5631" s="T289">v:pred</ta>
            <ta e="T291" id="Seg_5632" s="T290">pro.h:S</ta>
            <ta e="T294" id="Seg_5633" s="T293">v:pred</ta>
            <ta e="T299" id="Seg_5634" s="T295">s:temp</ta>
            <ta e="T302" id="Seg_5635" s="T301">np.h:S</ta>
            <ta e="T303" id="Seg_5636" s="T302">v:pred</ta>
            <ta e="T304" id="Seg_5637" s="T303">0.3.h:S v:pred</ta>
            <ta e="T308" id="Seg_5638" s="T307">np:O</ta>
            <ta e="T313" id="Seg_5639" s="T312">0.3.h:S v:pred</ta>
            <ta e="T316" id="Seg_5640" s="T315">np:O</ta>
            <ta e="T317" id="Seg_5641" s="T316">0.3.h:S v:pred</ta>
            <ta e="T320" id="Seg_5642" s="T319">np:O</ta>
            <ta e="T325" id="Seg_5643" s="T324">0.3.h:S v:pred</ta>
            <ta e="T327" id="Seg_5644" s="T325">s:adv</ta>
            <ta e="T328" id="Seg_5645" s="T327">v:pred</ta>
            <ta e="T329" id="Seg_5646" s="T328">np.h:S</ta>
            <ta e="T331" id="Seg_5647" s="T330">pro:S</ta>
            <ta e="T334" id="Seg_5648" s="T333">v:pred</ta>
            <ta e="T335" id="Seg_5649" s="T334">0.3.h:S v:pred</ta>
            <ta e="T336" id="Seg_5650" s="T335">np.h:S</ta>
            <ta e="T338" id="Seg_5651" s="T336">s:adv</ta>
            <ta e="T339" id="Seg_5652" s="T338">v:pred</ta>
            <ta e="T343" id="Seg_5653" s="T339">s:rel</ta>
            <ta e="T345" id="Seg_5654" s="T344">0.3.h:S v:pred</ta>
            <ta e="T346" id="Seg_5655" s="T345">0.3.h:S v:pred</ta>
            <ta e="T350" id="Seg_5656" s="T348">s:adv</ta>
            <ta e="T351" id="Seg_5657" s="T350">0.3.h:S v:pred</ta>
            <ta e="T353" id="Seg_5658" s="T352">np.h:S</ta>
            <ta e="T355" id="Seg_5659" s="T354">v:pred</ta>
            <ta e="T359" id="Seg_5660" s="T358">np:S</ta>
            <ta e="T360" id="Seg_5661" s="T359">v:pred</ta>
            <ta e="T362" id="Seg_5662" s="T361">np:S</ta>
            <ta e="T365" id="Seg_5663" s="T364">v:pred</ta>
            <ta e="T374" id="Seg_5664" s="T373">0.3:S v:pred</ta>
            <ta e="T377" id="Seg_5665" s="T376">n:pred</ta>
            <ta e="T379" id="Seg_5666" s="T378">0.3:S cop</ta>
            <ta e="T381" id="Seg_5667" s="T380">np.h:S</ta>
            <ta e="T382" id="Seg_5668" s="T381">s:adv</ta>
            <ta e="T383" id="Seg_5669" s="T382">v:pred</ta>
            <ta e="T384" id="Seg_5670" s="T383">np.h:S</ta>
            <ta e="T389" id="Seg_5671" s="T388">np.h:S</ta>
            <ta e="T392" id="Seg_5672" s="T391">v:pred</ta>
            <ta e="T394" id="Seg_5673" s="T393">np:S</ta>
            <ta e="T395" id="Seg_5674" s="T394">v:pred</ta>
            <ta e="T397" id="Seg_5675" s="T396">np.h:S</ta>
            <ta e="T399" id="Seg_5676" s="T397">s:adv</ta>
            <ta e="T403" id="Seg_5677" s="T402">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST">
            <ta e="T2" id="Seg_5678" s="T1">accs-gen</ta>
            <ta e="T3" id="Seg_5679" s="T2">new</ta>
            <ta e="T8" id="Seg_5680" s="T7">giv-active</ta>
            <ta e="T11" id="Seg_5681" s="T10">giv-active</ta>
            <ta e="T12" id="Seg_5682" s="T11">accs-inf</ta>
            <ta e="T14" id="Seg_5683" s="T13">quot-sp</ta>
            <ta e="T16" id="Seg_5684" s="T15">0.giv-active-Q</ta>
            <ta e="T17" id="Seg_5685" s="T16">0.giv-active 0.quot-sp</ta>
            <ta e="T19" id="Seg_5686" s="T18">0.giv-active 0.quot-sp</ta>
            <ta e="T20" id="Seg_5687" s="T19">giv-active-Q</ta>
            <ta e="T22" id="Seg_5688" s="T21">accs-sit-Q</ta>
            <ta e="T27" id="Seg_5689" s="T26">new-Q</ta>
            <ta e="T30" id="Seg_5690" s="T29">giv-active-Q</ta>
            <ta e="T31" id="Seg_5691" s="T30">accs-inf-Q</ta>
            <ta e="T33" id="Seg_5692" s="T32">0.giv-active-Q</ta>
            <ta e="T36" id="Seg_5693" s="T35">0.giv-active-Q</ta>
            <ta e="T37" id="Seg_5694" s="T36">0.giv-active 0.quot-sp</ta>
            <ta e="T39" id="Seg_5695" s="T38">giv-inactive</ta>
            <ta e="T41" id="Seg_5696" s="T40">0.giv-active-Q</ta>
            <ta e="T43" id="Seg_5697" s="T42">quot-th</ta>
            <ta e="T45" id="Seg_5698" s="T44">new</ta>
            <ta e="T46" id="Seg_5699" s="T45">giv-active</ta>
            <ta e="T47" id="Seg_5700" s="T46">new</ta>
            <ta e="T48" id="Seg_5701" s="T47">quot-sp</ta>
            <ta e="T49" id="Seg_5702" s="T48">giv-active-Q</ta>
            <ta e="T50" id="Seg_5703" s="T49">giv-active-Q</ta>
            <ta e="T51" id="Seg_5704" s="T50">accs-gen-Q</ta>
            <ta e="T53" id="Seg_5705" s="T52">giv-inactive-Q</ta>
            <ta e="T54" id="Seg_5706" s="T53">giv-inactive-Q</ta>
            <ta e="T56" id="Seg_5707" s="T55">0.giv-active-Q</ta>
            <ta e="T58" id="Seg_5708" s="T57">0.giv-active 0.quot-sp</ta>
            <ta e="T60" id="Seg_5709" s="T59">giv-active</ta>
            <ta e="T61" id="Seg_5710" s="T60">0.giv-active</ta>
            <ta e="T68" id="Seg_5711" s="T67">accs-gen-Q</ta>
            <ta e="T72" id="Seg_5712" s="T71">0.giv-active 0.quot-sp</ta>
            <ta e="T74" id="Seg_5713" s="T73">giv-inactive</ta>
            <ta e="T76" id="Seg_5714" s="T75">new</ta>
            <ta e="T78" id="Seg_5715" s="T77">giv-inactive-Q</ta>
            <ta e="T80" id="Seg_5716" s="T79">giv-active-Q</ta>
            <ta e="T81" id="Seg_5717" s="T80">giv-active-Q</ta>
            <ta e="T82" id="Seg_5718" s="T81">accs-inf-Q</ta>
            <ta e="T85" id="Seg_5719" s="T84">accs-sit-Q</ta>
            <ta e="T86" id="Seg_5720" s="T85">giv-inactive-Q</ta>
            <ta e="T87" id="Seg_5721" s="T86">giv-inactive-Q</ta>
            <ta e="T89" id="Seg_5722" s="T88">0.giv-active-Q</ta>
            <ta e="T90" id="Seg_5723" s="T89">0.giv-active 0.quot-sp</ta>
            <ta e="T91" id="Seg_5724" s="T90">giv-active</ta>
            <ta e="T92" id="Seg_5725" s="T91">0.giv-active</ta>
            <ta e="T98" id="Seg_5726" s="T97">giv-active</ta>
            <ta e="T100" id="Seg_5727" s="T99">0.giv-inactive</ta>
            <ta e="T102" id="Seg_5728" s="T101">giv-active</ta>
            <ta e="T108" id="Seg_5729" s="T107">giv-active-Q</ta>
            <ta e="T112" id="Seg_5730" s="T111">0.accs-sit-Q</ta>
            <ta e="T113" id="Seg_5731" s="T112">giv-inactive-Q</ta>
            <ta e="T115" id="Seg_5732" s="T114">new-Q</ta>
            <ta e="T116" id="Seg_5733" s="T115">accs-inf-Q</ta>
            <ta e="T118" id="Seg_5734" s="T117">0.giv-active-Q</ta>
            <ta e="T119" id="Seg_5735" s="T118">0.giv-active 0.quot-sp</ta>
            <ta e="T120" id="Seg_5736" s="T119">giv-inactive</ta>
            <ta e="T122" id="Seg_5737" s="T121">0.giv-active</ta>
            <ta e="T125" id="Seg_5738" s="T124">giv-active</ta>
            <ta e="T129" id="Seg_5739" s="T128">0.giv-active 0.quot-sp</ta>
            <ta e="T130" id="Seg_5740" s="T129">giv-active-Q</ta>
            <ta e="T133" id="Seg_5741" s="T132">giv-inactive-Q</ta>
            <ta e="T135" id="Seg_5742" s="T134">accs-gen-Q</ta>
            <ta e="T136" id="Seg_5743" s="T135">giv-inactive-Q</ta>
            <ta e="T137" id="Seg_5744" s="T136">0.accs-inf-Q</ta>
            <ta e="T140" id="Seg_5745" s="T139">giv-active-Q</ta>
            <ta e="T141" id="Seg_5746" s="T140">new-Q</ta>
            <ta e="T142" id="Seg_5747" s="T141">0.giv-active-Q</ta>
            <ta e="T143" id="Seg_5748" s="T142">0.giv-active 0.quot-sp</ta>
            <ta e="T144" id="Seg_5749" s="T143">0.giv-active</ta>
            <ta e="T146" id="Seg_5750" s="T145">0.giv-active</ta>
            <ta e="T148" id="Seg_5751" s="T147">giv-inactive</ta>
            <ta e="T149" id="Seg_5752" s="T148">quot-sp</ta>
            <ta e="T151" id="Seg_5753" s="T150">0.giv-inactive-Q</ta>
            <ta e="T152" id="Seg_5754" s="T151">0.giv-inactive-Q 0.quot-sp-Q</ta>
            <ta e="T153" id="Seg_5755" s="T152">0.giv-active 0.quot-sp</ta>
            <ta e="T154" id="Seg_5756" s="T153">0.giv-active</ta>
            <ta e="T155" id="Seg_5757" s="T154">giv-active</ta>
            <ta e="T156" id="Seg_5758" s="T155">giv-active</ta>
            <ta e="T159" id="Seg_5759" s="T158">giv-active</ta>
            <ta e="T160" id="Seg_5760" s="T159">quot-sp</ta>
            <ta e="T161" id="Seg_5761" s="T160">giv-active-Q</ta>
            <ta e="T162" id="Seg_5762" s="T161">0.giv-active-Q</ta>
            <ta e="T167" id="Seg_5763" s="T166">accs-gen-Q</ta>
            <ta e="T172" id="Seg_5764" s="T171">0.giv-inactive 0.quot-sp</ta>
            <ta e="T173" id="Seg_5765" s="T172">0.giv-active</ta>
            <ta e="T175" id="Seg_5766" s="T174">giv-active-Q</ta>
            <ta e="T176" id="Seg_5767" s="T175">giv-inactive-Q</ta>
            <ta e="T178" id="Seg_5768" s="T177">0.giv-inactive-Q</ta>
            <ta e="T179" id="Seg_5769" s="T178">0.giv-active 0.quot-sp</ta>
            <ta e="T183" id="Seg_5770" s="T182">giv-active</ta>
            <ta e="T188" id="Seg_5771" s="T187">0.giv-active</ta>
            <ta e="T189" id="Seg_5772" s="T188">0.giv-active</ta>
            <ta e="T193" id="Seg_5773" s="T192">giv-inactive</ta>
            <ta e="T194" id="Seg_5774" s="T193">giv-active</ta>
            <ta e="T198" id="Seg_5775" s="T197">giv-active</ta>
            <ta e="T199" id="Seg_5776" s="T198">0.giv-active</ta>
            <ta e="T202" id="Seg_5777" s="T201">new-Q</ta>
            <ta e="T203" id="Seg_5778" s="T202">0.giv-inactive-Q</ta>
            <ta e="T204" id="Seg_5779" s="T203">0.giv-active 0.quot-sp</ta>
            <ta e="T206" id="Seg_5780" s="T205">0.giv-active-Q</ta>
            <ta e="T209" id="Seg_5781" s="T208">0.giv-inactive-Q</ta>
            <ta e="T213" id="Seg_5782" s="T212">giv-active-Q</ta>
            <ta e="T215" id="Seg_5783" s="T214">0.giv-active 0.quot-sp</ta>
            <ta e="T216" id="Seg_5784" s="T215">giv-active</ta>
            <ta e="T218" id="Seg_5785" s="T217">0.giv-active</ta>
            <ta e="T219" id="Seg_5786" s="T218">0.giv-active</ta>
            <ta e="T222" id="Seg_5787" s="T221">giv-active</ta>
            <ta e="T223" id="Seg_5788" s="T222">giv-active</ta>
            <ta e="T224" id="Seg_5789" s="T223">quot-sp</ta>
            <ta e="T230" id="Seg_5790" s="T229">0.giv-active-Q</ta>
            <ta e="T231" id="Seg_5791" s="T230">accs-inf-Q</ta>
            <ta e="T232" id="Seg_5792" s="T231">0.giv-active-Q</ta>
            <ta e="T233" id="Seg_5793" s="T232">0.giv-active-Q 0.quot-sp-Q</ta>
            <ta e="T234" id="Seg_5794" s="T233">accs-sit-Q</ta>
            <ta e="T237" id="Seg_5795" s="T236">0.giv-active-Q</ta>
            <ta e="T238" id="Seg_5796" s="T237">0.giv-active 0.quot-sp</ta>
            <ta e="T239" id="Seg_5797" s="T238">giv-active</ta>
            <ta e="T241" id="Seg_5798" s="T240">accs-inf</ta>
            <ta e="T242" id="Seg_5799" s="T241">0.giv-active</ta>
            <ta e="T244" id="Seg_5800" s="T243">0.giv-active</ta>
            <ta e="T245" id="Seg_5801" s="T244">giv-inactive</ta>
            <ta e="T246" id="Seg_5802" s="T245">accs-inf</ta>
            <ta e="T247" id="Seg_5803" s="T246">0.giv-active</ta>
            <ta e="T249" id="Seg_5804" s="T248">0.giv-active</ta>
            <ta e="T250" id="Seg_5805" s="T249">0.giv-active</ta>
            <ta e="T251" id="Seg_5806" s="T250">giv-active</ta>
            <ta e="T252" id="Seg_5807" s="T251">accs-inf</ta>
            <ta e="T253" id="Seg_5808" s="T252">accs-inf</ta>
            <ta e="T254" id="Seg_5809" s="T253">0.giv-active</ta>
            <ta e="T255" id="Seg_5810" s="T254">giv-active</ta>
            <ta e="T256" id="Seg_5811" s="T255">accs-inf</ta>
            <ta e="T258" id="Seg_5812" s="T257">0.giv-active</ta>
            <ta e="T262" id="Seg_5813" s="T261">0.giv-active</ta>
            <ta e="T265" id="Seg_5814" s="T264">accs-inf</ta>
            <ta e="T266" id="Seg_5815" s="T265">0.giv-active</ta>
            <ta e="T269" id="Seg_5816" s="T268">giv-active-Q</ta>
            <ta e="T270" id="Seg_5817" s="T269">giv-inactive-Q</ta>
            <ta e="T277" id="Seg_5818" s="T276">giv-inactive-Q</ta>
            <ta e="T281" id="Seg_5819" s="T280">giv-active-Q</ta>
            <ta e="T282" id="Seg_5820" s="T281">accs-inf-Q</ta>
            <ta e="T283" id="Seg_5821" s="T282">new-Q</ta>
            <ta e="T284" id="Seg_5822" s="T283">0.giv-active-Q</ta>
            <ta e="T285" id="Seg_5823" s="T284">giv-active-Q</ta>
            <ta e="T287" id="Seg_5824" s="T286">giv-inactive-Q</ta>
            <ta e="T288" id="Seg_5825" s="T287">accs-inf-Q</ta>
            <ta e="T297" id="Seg_5826" s="T296">new-Q</ta>
            <ta e="T298" id="Seg_5827" s="T297">new-Q</ta>
            <ta e="T300" id="Seg_5828" s="T299">giv-active-Q</ta>
            <ta e="T304" id="Seg_5829" s="T303">0.giv-inactive 0.quot-sp</ta>
            <ta e="T308" id="Seg_5830" s="T307">new-Q</ta>
            <ta e="T311" id="Seg_5831" s="T310">accs-sit-Q</ta>
            <ta e="T312" id="Seg_5832" s="T311">accs-inf-Q</ta>
            <ta e="T313" id="Seg_5833" s="T312">0.giv-inactive-Q</ta>
            <ta e="T316" id="Seg_5834" s="T315">new-Q</ta>
            <ta e="T317" id="Seg_5835" s="T316">0.giv-active-Q</ta>
            <ta e="T319" id="Seg_5836" s="T318">giv-active-Q</ta>
            <ta e="T320" id="Seg_5837" s="T319">accs-inf-Q</ta>
            <ta e="T324" id="Seg_5838" s="T323">new-Q</ta>
            <ta e="T325" id="Seg_5839" s="T324">0.giv-active-Q</ta>
            <ta e="T327" id="Seg_5840" s="T326">0.giv-inactive</ta>
            <ta e="T329" id="Seg_5841" s="T328">giv-active</ta>
            <ta e="T335" id="Seg_5842" s="T334">0.giv-inactive 0.quot-sp</ta>
            <ta e="T336" id="Seg_5843" s="T335">giv-inactive-Q</ta>
            <ta e="T338" id="Seg_5844" s="T337">0.giv-active-Q 0.quot-th-Q</ta>
            <ta e="T341" id="Seg_5845" s="T340">accs-inf-Q</ta>
            <ta e="T345" id="Seg_5846" s="T344">0.accs-inf-Q</ta>
            <ta e="T346" id="Seg_5847" s="T345">0.giv-active 0.quot-sp</ta>
            <ta e="T348" id="Seg_5848" s="T347">accs-sit</ta>
            <ta e="T349" id="Seg_5849" s="T348">0.giv-active</ta>
            <ta e="T351" id="Seg_5850" s="T350">0.giv-inactive</ta>
            <ta e="T353" id="Seg_5851" s="T352">giv-inactive</ta>
            <ta e="T359" id="Seg_5852" s="T358">new</ta>
            <ta e="T361" id="Seg_5853" s="T360">new</ta>
            <ta e="T362" id="Seg_5854" s="T361">accs-inf</ta>
            <ta e="T370" id="Seg_5855" s="T369">giv-inactive</ta>
            <ta e="T371" id="Seg_5856" s="T370">accs-inf</ta>
            <ta e="T374" id="Seg_5857" s="T373">0.giv-active</ta>
            <ta e="T377" id="Seg_5858" s="T376">giv-active</ta>
            <ta e="T381" id="Seg_5859" s="T380">giv-inactive</ta>
            <ta e="T382" id="Seg_5860" s="T381">0.giv-active</ta>
            <ta e="T384" id="Seg_5861" s="T383">giv-inactive</ta>
            <ta e="T389" id="Seg_5862" s="T388">giv-inactive</ta>
            <ta e="T394" id="Seg_5863" s="T393">giv-inactive</ta>
            <ta e="T397" id="Seg_5864" s="T396">accs-gen</ta>
         </annotation>
         <annotation name="Top" tierref="Top">
            <ta e="T8" id="Seg_5865" s="T6">top.int.concr</ta>
            <ta e="T11" id="Seg_5866" s="T10">top.int.concr</ta>
            <ta e="T19" id="Seg_5867" s="T18">0.top.int.concr</ta>
            <ta e="T20" id="Seg_5868" s="T19">top.int.concr</ta>
            <ta e="T26" id="Seg_5869" s="T25">top.int.concr</ta>
            <ta e="T36" id="Seg_5870" s="T35">0.top.int.concr</ta>
            <ta e="T39" id="Seg_5871" s="T38">top.int.concr</ta>
            <ta e="T45" id="Seg_5872" s="T44">top.int.concr</ta>
            <ta e="T59" id="Seg_5873" s="T58">top.int.concr</ta>
            <ta e="T80" id="Seg_5874" s="T79">top.int.concr</ta>
            <ta e="T91" id="Seg_5875" s="T90">top.int.concr</ta>
            <ta e="T100" id="Seg_5876" s="T96">top.int.concr</ta>
            <ta e="T120" id="Seg_5877" s="T119">top.int.concr</ta>
            <ta e="T129" id="Seg_5878" s="T128">0.top.int.concr</ta>
            <ta e="T130" id="Seg_5879" s="T129">top.int.concr</ta>
            <ta e="T137" id="Seg_5880" s="T136">0.top.int.concr</ta>
            <ta e="T154" id="Seg_5881" s="T153">0.top.int.concr</ta>
            <ta e="T158" id="Seg_5882" s="T155">top.int.concr</ta>
            <ta e="T161" id="Seg_5883" s="T160">top.int.concr</ta>
            <ta e="T183" id="Seg_5884" s="T181">top.int.concr</ta>
            <ta e="T189" id="Seg_5885" s="T186">top.int.concr</ta>
            <ta e="T218" id="Seg_5886" s="T217">0.top.int.concr</ta>
            <ta e="T221" id="Seg_5887" s="T218">top.int.concr</ta>
            <ta e="T232" id="Seg_5888" s="T231">0.top.int.concr</ta>
            <ta e="T234" id="Seg_5889" s="T233">top.int.concr</ta>
            <ta e="T239" id="Seg_5890" s="T238">top.int.concr</ta>
            <ta e="T247" id="Seg_5891" s="T246">0.top.int.concr</ta>
            <ta e="T262" id="Seg_5892" s="T261">0.top.int.concr</ta>
            <ta e="T266" id="Seg_5893" s="T265">0.top.int.concr</ta>
            <ta e="T269" id="Seg_5894" s="T268">top.int.concr</ta>
            <ta e="T284" id="Seg_5895" s="T283">0.top.int.concr</ta>
            <ta e="T285" id="Seg_5896" s="T284">top.int.concr</ta>
            <ta e="T300" id="Seg_5897" s="T299">top.int.concr</ta>
            <ta e="T313" id="Seg_5898" s="T312">0.top.int.concr</ta>
            <ta e="T317" id="Seg_5899" s="T316">0.top.int.concr</ta>
            <ta e="T325" id="Seg_5900" s="T324">0.top.int.concr</ta>
            <ta e="T326" id="Seg_5901" s="T325">top.int.concr</ta>
            <ta e="T336" id="Seg_5902" s="T335">top.int.concr</ta>
            <ta e="T343" id="Seg_5903" s="T339">top.int.concr</ta>
            <ta e="T348" id="Seg_5904" s="T346">top.int.concr</ta>
            <ta e="T353" id="Seg_5905" s="T352">top.int.concr</ta>
            <ta e="T376" id="Seg_5906" s="T374">top.int.concr</ta>
            <ta e="T389" id="Seg_5907" s="T383">top.int.concr</ta>
            <ta e="T394" id="Seg_5908" s="T393">top.int.concr</ta>
         </annotation>
         <annotation name="Foc" tierref="Foc">
            <ta e="T4" id="Seg_5909" s="T0">foc.wid</ta>
            <ta e="T10" id="Seg_5910" s="T8">foc.int</ta>
            <ta e="T14" id="Seg_5911" s="T12">foc.int</ta>
            <ta e="T15" id="Seg_5912" s="T14">foc.nar</ta>
            <ta e="T19" id="Seg_5913" s="T17">foc.int</ta>
            <ta e="T25" id="Seg_5914" s="T20">foc.int</ta>
            <ta e="T27" id="Seg_5915" s="T26">foc.nar</ta>
            <ta e="T36" id="Seg_5916" s="T33">foc.int</ta>
            <ta e="T43" id="Seg_5917" s="T39">foc.int</ta>
            <ta e="T48" id="Seg_5918" s="T44">foc.wid</ta>
            <ta e="T57" id="Seg_5919" s="T49">foc.int</ta>
            <ta e="T62" id="Seg_5920" s="T60">foc.int</ta>
            <ta e="T77" id="Seg_5921" s="T72">foc.wid</ta>
            <ta e="T79" id="Seg_5922" s="T78">foc.int</ta>
            <ta e="T83" id="Seg_5923" s="T80">foc.int</ta>
            <ta e="T89" id="Seg_5924" s="T83">foc.int</ta>
            <ta e="T96" id="Seg_5925" s="T94">foc.int</ta>
            <ta e="T104" id="Seg_5926" s="T102">foc.int</ta>
            <ta e="T108" id="Seg_5927" s="T104">foc.nar</ta>
            <ta e="T115" id="Seg_5928" s="T113">foc.nar</ta>
            <ta e="T121" id="Seg_5929" s="T120">foc.int</ta>
            <ta e="T124" id="Seg_5930" s="T122">foc.nar</ta>
            <ta e="T129" id="Seg_5931" s="T125">foc.int</ta>
            <ta e="T132" id="Seg_5932" s="T130">foc.int</ta>
            <ta e="T137" id="Seg_5933" s="T132">foc.int</ta>
            <ta e="T138" id="Seg_5934" s="T137">foc.nar</ta>
            <ta e="T141" id="Seg_5935" s="T139">foc.nar</ta>
            <ta e="T149" id="Seg_5936" s="T146">foc.wid</ta>
            <ta e="T151" id="Seg_5937" s="T150">foc.int</ta>
            <ta e="T152" id="Seg_5938" s="T151">foc.int</ta>
            <ta e="T155" id="Seg_5939" s="T153">foc.int</ta>
            <ta e="T160" id="Seg_5940" s="T158">foc.wid</ta>
            <ta e="T165" id="Seg_5941" s="T161">foc.int</ta>
            <ta e="T171" id="Seg_5942" s="T170">foc.ver</ta>
            <ta e="T178" id="Seg_5943" s="T173">foc.int</ta>
            <ta e="T186" id="Seg_5944" s="T183">foc.int</ta>
            <ta e="T195" id="Seg_5945" s="T189">foc.wid</ta>
            <ta e="T200" id="Seg_5946" s="T195">foc.wid</ta>
            <ta e="T202" id="Seg_5947" s="T201">foc.nar</ta>
            <ta e="T205" id="Seg_5948" s="T204">foc.ver</ta>
            <ta e="T207" id="Seg_5949" s="T205">foc.ver</ta>
            <ta e="T208" id="Seg_5950" s="T207">foc.nar</ta>
            <ta e="T214" id="Seg_5951" s="T211">foc.wid</ta>
            <ta e="T218" id="Seg_5952" s="T214">foc.int</ta>
            <ta e="T224" id="Seg_5953" s="T222">foc.int</ta>
            <ta e="T230" id="Seg_5954" s="T229">foc.int</ta>
            <ta e="T232" id="Seg_5955" s="T230">foc.int</ta>
            <ta e="T237" id="Seg_5956" s="T234">foc.int</ta>
            <ta e="T242" id="Seg_5957" s="T239">foc.int</ta>
            <ta e="T247" id="Seg_5958" s="T244">foc.int</ta>
            <ta e="T261" id="Seg_5959" s="T258">foc.nar</ta>
            <ta e="T266" id="Seg_5960" s="T262">foc.int</ta>
            <ta e="T274" id="Seg_5961" s="T269">foc.int</ta>
            <ta e="T275" id="Seg_5962" s="T274">foc.nar</ta>
            <ta e="T284" id="Seg_5963" s="T279">foc.int</ta>
            <ta e="T289" id="Seg_5964" s="T286">foc.nar</ta>
            <ta e="T292" id="Seg_5965" s="T290">foc.nar</ta>
            <ta e="T299" id="Seg_5966" s="T295">foc.wid</ta>
            <ta e="T302" id="Seg_5967" s="T300">foc.nar</ta>
            <ta e="T313" id="Seg_5968" s="T304">foc.int</ta>
            <ta e="T317" id="Seg_5969" s="T313">foc.int</ta>
            <ta e="T325" id="Seg_5970" s="T317">foc.int</ta>
            <ta e="T328" id="Seg_5971" s="T326">foc.int</ta>
            <ta e="T332" id="Seg_5972" s="T329">foc.nar</ta>
            <ta e="T339" id="Seg_5973" s="T336">foc.int</ta>
            <ta e="T345" id="Seg_5974" s="T343">foc.int</ta>
            <ta e="T351" id="Seg_5975" s="T348">foc.int</ta>
            <ta e="T355" id="Seg_5976" s="T353">foc.int</ta>
            <ta e="T360" id="Seg_5977" s="T355">foc.wid</ta>
            <ta e="T365" id="Seg_5978" s="T360">foc.wid</ta>
            <ta e="T379" id="Seg_5979" s="T377">foc.int</ta>
            <ta e="T383" id="Seg_5980" s="T379">foc.wid</ta>
            <ta e="T392" id="Seg_5981" s="T389">foc.int</ta>
            <ta e="T395" id="Seg_5982" s="T394">foc.int</ta>
            <ta e="T403" id="Seg_5983" s="T395">foc.wid</ta>
         </annotation>
         <annotation name="BOR" tierref="BOR">
            <ta e="T2" id="Seg_5984" s="T1">RUS:cult</ta>
            <ta e="T3" id="Seg_5985" s="T2">RUS:cult</ta>
            <ta e="T46" id="Seg_5986" s="T45">RUS:cult</ta>
            <ta e="T74" id="Seg_5987" s="T73">RUS:cult</ta>
            <ta e="T138" id="Seg_5988" s="T137">RUS:cult</ta>
            <ta e="T145" id="Seg_5989" s="T144">RUS:cult</ta>
            <ta e="T191" id="Seg_5990" s="T190">RUS:gram</ta>
            <ta e="T223" id="Seg_5991" s="T222">RUS:cult</ta>
            <ta e="T252" id="Seg_5992" s="T251">EV:cult</ta>
            <ta e="T277" id="Seg_5993" s="T276">RUS:cult</ta>
            <ta e="T282" id="Seg_5994" s="T281">RUS:core</ta>
            <ta e="T285" id="Seg_5995" s="T284">RUS:core</ta>
            <ta e="T307" id="Seg_5996" s="T306">EV:core</ta>
            <ta e="T308" id="Seg_5997" s="T307">EV:cult</ta>
            <ta e="T315" id="Seg_5998" s="T314">EV:core</ta>
            <ta e="T329" id="Seg_5999" s="T328">RUS:cult</ta>
            <ta e="T384" id="Seg_6000" s="T383">RUS:cult</ta>
            <ta e="T385" id="Seg_6001" s="T384">RUS:gram</ta>
            <ta e="T388" id="Seg_6002" s="T387">RUS:gram</ta>
            <ta e="T394" id="Seg_6003" s="T393">RUS:cult</ta>
            <ta e="T397" id="Seg_6004" s="T396">EV:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon">
            <ta e="T3" id="Seg_6005" s="T2">medVins Vsub fortition</ta>
            <ta e="T46" id="Seg_6006" s="T45">medVins Vsub fortition</ta>
            <ta e="T74" id="Seg_6007" s="T73">medVins Vsub fortition</ta>
            <ta e="T138" id="Seg_6008" s="T137">medVins Vsub fortition</ta>
            <ta e="T145" id="Seg_6009" s="T144">Vsub</ta>
            <ta e="T191" id="Seg_6010" s="T190">Vsub</ta>
            <ta e="T223" id="Seg_6011" s="T222">medVins Vsub fortition</ta>
            <ta e="T252" id="Seg_6012" s="T251">Vsub</ta>
            <ta e="T277" id="Seg_6013" s="T276">medVins Vsub fortition</ta>
            <ta e="T282" id="Seg_6014" s="T281">Vsub Vsub fortition</ta>
            <ta e="T285" id="Seg_6015" s="T284">Vsub Vsub fortition</ta>
            <ta e="T307" id="Seg_6016" s="T306">Vsub Vsub</ta>
            <ta e="T308" id="Seg_6017" s="T307">Vsub Vsub</ta>
            <ta e="T315" id="Seg_6018" s="T314">Vsub Vsub</ta>
            <ta e="T329" id="Seg_6019" s="T328">medVins Vsub fortition</ta>
            <ta e="T384" id="Seg_6020" s="T383">medVins Vsub fortition</ta>
            <ta e="T397" id="Seg_6021" s="T396">Vsub</ta>
         </annotation>
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T2" id="Seg_6022" s="T1">indir:infl</ta>
            <ta e="T3" id="Seg_6023" s="T2">dir:infl</ta>
            <ta e="T46" id="Seg_6024" s="T45">dir:bare</ta>
            <ta e="T74" id="Seg_6025" s="T73">dir:bare</ta>
            <ta e="T138" id="Seg_6026" s="T137">dir:bare</ta>
            <ta e="T145" id="Seg_6027" s="T144">dir:infl</ta>
            <ta e="T191" id="Seg_6028" s="T190">dir:bare</ta>
            <ta e="T223" id="Seg_6029" s="T222">dir:infl</ta>
            <ta e="T252" id="Seg_6030" s="T251">dir:infl</ta>
            <ta e="T277" id="Seg_6031" s="T276">dir:infl</ta>
            <ta e="T282" id="Seg_6032" s="T281">dir:infl</ta>
            <ta e="T285" id="Seg_6033" s="T284">dir:infl</ta>
            <ta e="T307" id="Seg_6034" s="T306">dir:bare</ta>
            <ta e="T308" id="Seg_6035" s="T307">dir:infl</ta>
            <ta e="T315" id="Seg_6036" s="T314">dir:bare</ta>
            <ta e="T329" id="Seg_6037" s="T328">dir:infl</ta>
            <ta e="T384" id="Seg_6038" s="T383">dir:bare</ta>
            <ta e="T385" id="Seg_6039" s="T384">dir:bare</ta>
            <ta e="T388" id="Seg_6040" s="T387">dir:bare</ta>
            <ta e="T394" id="Seg_6041" s="T393">indir:bare</ta>
            <ta e="T397" id="Seg_6042" s="T396">dir:bare</ta>
         </annotation>
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_6043" s="T0">A long time ago the citizens of Norilsk had a prince, they say.</ta>
            <ta e="T6" id="Seg_6044" s="T4">A young man.</ta>
            <ta e="T14" id="Seg_6045" s="T6">That man doesn't have a wife, his family asks once:</ta>
            <ta e="T17" id="Seg_6046" s="T14">"Why don't you marry?", they say.</ta>
            <ta e="T19" id="Seg_6047" s="T17">On that he says:</ta>
            <ta e="T25" id="Seg_6048" s="T19">"I'm not a man who takes a girl from this world.</ta>
            <ta e="T37" id="Seg_6049" s="T25">Up there is Ürüng Ajyy, if I could take his daughter, I would agree", he says.</ta>
            <ta e="T43" id="Seg_6050" s="T37">The people think: "He is joking."</ta>
            <ta e="T48" id="Seg_6051" s="T43">Once at one gathering the prince forces a shaman:</ta>
            <ta e="T58" id="Seg_6052" s="T48">"Match me with the daughter of Ürüng Ajyy, who lives in the upper world", he says.</ta>
            <ta e="T62" id="Seg_6053" s="T58">The shaman hears this with some fear.</ta>
            <ta e="T72" id="Seg_6054" s="T62">"What are you saying, I cannot bring anyone from that world", he says.</ta>
            <ta e="T77" id="Seg_6055" s="T72">Then the prince shows him seven wickers.</ta>
            <ta e="T79" id="Seg_6056" s="T77">"Don't deceive me.</ta>
            <ta e="T90" id="Seg_6057" s="T79">I will skin your back, make that the daughter of Ürüng Ajyy is here tomorrow!", he says.</ta>
            <ta e="T96" id="Seg_6058" s="T90">The shaman, who doesn't want to die, starts shamanizing.</ta>
            <ta e="T104" id="Seg_6059" s="T96">As he hears that the shaman gets closer, the lord Ürüng Ajyy gets very angry:</ta>
            <ta e="T111" id="Seg_6060" s="T104">"How does a shaman of the lower world dare to come here without an order?!</ta>
            <ta e="T119" id="Seg_6061" s="T111">Bring me my fire sword in order to cut his head off!", he said.</ta>
            <ta e="T125" id="Seg_6062" s="T119">The shaman came and Ajyy was sitting there already prepared.</ta>
            <ta e="T129" id="Seg_6063" s="T125">There he beseeches and begs him:</ta>
            <ta e="T139" id="Seg_6064" s="T129">"I came in need, on the earth we have a leader like you, they call him "prince".</ta>
            <ta e="T146" id="Seg_6065" s="T139">Under his force I came here", he complains and tells what he needs.</ta>
            <ta e="T149" id="Seg_6066" s="T146">Ürüng Ajyy says on that:</ta>
            <ta e="T150" id="Seg_6067" s="T149">"No!</ta>
            <ta e="T155" id="Seg_6068" s="T150">"Stop", say [to him]", he says and sends the shaman back.</ta>
            <ta e="T160" id="Seg_6069" s="T155">As that one stops to shamanize, the prince forces him:</ta>
            <ta e="T165" id="Seg_6070" s="T160">"You are pretending that you went somewhere.</ta>
            <ta e="T173" id="Seg_6071" s="T165">Isn't the polar fox for sacrificing still hanging here?", he says and forces him:</ta>
            <ta e="T179" id="Seg_6072" s="T173">"Bring me this daughter by all means!", he says.</ta>
            <ta e="T186" id="Seg_6073" s="T179">Well, in the morning this shaman shamanized against his will again.</ta>
            <ta e="T195" id="Seg_6074" s="T186">As he arrives shamanizing, Ürüng Ajyy gets even more mad with the shaman.</ta>
            <ta e="T200" id="Seg_6075" s="T195">On that the shaman cries and suffers again:</ta>
            <ta e="T204" id="Seg_6076" s="T200">"Give me some mark, though", he says.</ta>
            <ta e="T218" id="Seg_6077" s="T204">"No, I won't give you any, why he is so stupid, narrow-eyed, better you die", he says and sends the shaman back again.</ta>
            <ta e="T224" id="Seg_6078" s="T218">After having shamanized the shaman beseeches the prince:</ta>
            <ta e="T238" id="Seg_6079" s="T224">"Third time lucky, don't kill me, because he didn't fulfil your wish, tomorrow I will try it again", he says.</ta>
            <ta e="T242" id="Seg_6080" s="T238">The next morning he shamanizes for the third time.</ta>
            <ta e="T247" id="Seg_6081" s="T242">Shamanizing he comes to Ürüng Ajyy's house.</ta>
            <ta e="T262" id="Seg_6082" s="T247">He comes there, bows to the master of the main pole of the tent, crawls to Ürüng Ajyy's feet and complains even more than before.</ta>
            <ta e="T266" id="Seg_6083" s="T262">[Ürüng Ajyy's] heart softens:</ta>
            <ta e="T274" id="Seg_6084" s="T266">"It is true, I didn't make you my shaman in order to kill you.</ta>
            <ta e="T279" id="Seg_6085" s="T274">Why is your prince so stupid?!</ta>
            <ta e="T284" id="Seg_6086" s="T279">He brought trouble to his own people.</ta>
            <ta e="T290" id="Seg_6087" s="T284">Of all your people only you and your son will stay alive.</ta>
            <ta e="T295" id="Seg_6088" s="T290">Nobody else will stay alive.</ta>
            <ta e="T304" id="Seg_6089" s="T295">From other places people will come, a new people will arise from them", he says.</ta>
            <ta e="T325" id="Seg_6090" s="T304">"In three days from now in the morning they shall place a clean and white leather cover, they shall kill white-colourful reindeers, they shall spread white reindeer furs around the summer tent.</ta>
            <ta e="T329" id="Seg_6091" s="T325">Your prince will step onto them and enter.</ta>
            <ta e="T335" id="Seg_6092" s="T329">Nothing dirty shall stick to it", he says.</ta>
            <ta e="T346" id="Seg_6093" s="T335">"People shall not come thinking that there is a wedding; who wants to save his soul shall go far away", he said.</ta>
            <ta e="T351" id="Seg_6094" s="T346">On the third day they made so as it was said.</ta>
            <ta e="T355" id="Seg_6095" s="T351">The shaman watched all this.</ta>
            <ta e="T365" id="Seg_6096" s="T355">Suddenly a strong whirlwind started to blow, a loud sound of bells was heard.</ta>
            <ta e="T374" id="Seg_6097" s="T365">As if the wind would open the door of the pole tent.</ta>
            <ta e="T379" id="Seg_6098" s="T374">Then the wind was gone again.</ta>
            <ta e="T392" id="Seg_6099" s="T379">The shaman went there and saw that the prince and all people that had come were lying dead there.</ta>
            <ta e="T395" id="Seg_6100" s="T392">So the Norilsk people became few.</ta>
            <ta e="T403" id="Seg_6101" s="T395">So the stem of the Edyans came and settled there.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_6102" s="T0">Vor langer Zeit, sagt man, hatte man in Norilsk einen Fürsten.</ta>
            <ta e="T6" id="Seg_6103" s="T4">Einen jungen Menschen.</ta>
            <ta e="T14" id="Seg_6104" s="T6">Dieser Mensch hat keine Frau, seine Familie fragt einmal:</ta>
            <ta e="T17" id="Seg_6105" s="T14">"Warum heiratest du nicht?", sagen sie.</ta>
            <ta e="T19" id="Seg_6106" s="T17">Darauf sagt er:</ta>
            <ta e="T25" id="Seg_6107" s="T19">"Ich bin kein Mensch, der ein Mädchen von dieser Welt nimmt.</ta>
            <ta e="T37" id="Seg_6108" s="T25">Oben [in der oberen Welt] ist Ürüng Ajyy, wenn ich seine Tochter nähme, dann wäre ich einverstanden", sagt er.</ta>
            <ta e="T43" id="Seg_6109" s="T37">Die Leute denken: "Er macht Spaß."</ta>
            <ta e="T48" id="Seg_6110" s="T43">Einmal auf einer Versammlung zwingt der Fürst einen Schamanen:</ta>
            <ta e="T58" id="Seg_6111" s="T48">"Sorge dafür, dass ich die Tochter von Ürüng Ajyy, der im Himmel ist, zur Frau nehme", sagt er.</ta>
            <ta e="T62" id="Seg_6112" s="T58">Der Schamane hört das mit etwas Angst:</ta>
            <ta e="T72" id="Seg_6113" s="T62">"Was sagst du da, aus jener Welt kann ich niemals jemanden bringen", sagt er.</ta>
            <ta e="T77" id="Seg_6114" s="T72">Da zeigt ihm der Fürst sieben Weidenruten:</ta>
            <ta e="T79" id="Seg_6115" s="T77">"Betrüge mich nicht.</ta>
            <ta e="T90" id="Seg_6116" s="T79">Ich ziehe dir das Fell ab, mach, dass die Tochter von Ürüng Ajyy morgen hier ist!", sagt er.</ta>
            <ta e="T96" id="Seg_6117" s="T90">Der Schamane, der noch nicht sterben will, fängt an zu schamanisieren.</ta>
            <ta e="T104" id="Seg_6118" s="T96">Als er hört, dass dieser Schamane sich nähert, wird der Herr Ürüng Ajyy sehr böse:</ta>
            <ta e="T111" id="Seg_6119" s="T104">"Was kommt eine Schamane der unteren Welt ohne Anordnung hier her?!</ta>
            <ta e="T119" id="Seg_6120" s="T111">Bringt mir mein Feuerschwert, damit ich ihm den Kopf abschlage!", sagte er.</ta>
            <ta e="T125" id="Seg_6121" s="T119">Der Schamane kam hin und Ajyy saß wohl schon bereit da.</ta>
            <ta e="T129" id="Seg_6122" s="T125">Darauf bittet und fleht er:</ta>
            <ta e="T139" id="Seg_6123" s="T129">"Ich komme in Not, bei uns auf der Erde haben wir einen Anführer, er nennt sich "Fürst".</ta>
            <ta e="T146" id="Seg_6124" s="T139">Unter seinem Zwang komme ich", so beklagt er sich und erzählt, was er braucht.</ta>
            <ta e="T149" id="Seg_6125" s="T146">Darauf sagt Ürüng Ajyy:</ta>
            <ta e="T150" id="Seg_6126" s="T149">"Nein!</ta>
            <ta e="T155" id="Seg_6127" s="T150">"Hör auf", sag [ihm]", sagt er und schickt den Schamanen zurück.</ta>
            <ta e="T160" id="Seg_6128" s="T155">Als dieser aufhört zu schamanisieren, zwingt der Fürst ihn:</ta>
            <ta e="T165" id="Seg_6129" s="T160">"Du tust so, als ob du irgendwohin gegangen bist.</ta>
            <ta e="T173" id="Seg_6130" s="T165">Hängt hier nicht der Fuchs [das Fuchsfell] zum opfern?", sagt er und nötigt ihn:</ta>
            <ta e="T179" id="Seg_6131" s="T173">"Bring mir unbedingt diese Tochter!", sagt er.</ta>
            <ta e="T186" id="Seg_6132" s="T179">Nun am Morgen schamanisierte dieser Schamane unwillig wieder.</ta>
            <ta e="T195" id="Seg_6133" s="T186">Als er schamanisierend ankommt, ärgert sich Ürüng Ajyy noch mehr über den Schamanen.</ta>
            <ta e="T200" id="Seg_6134" s="T195">Darauf weint und leider der Schamane wieder:</ta>
            <ta e="T204" id="Seg_6135" s="T200">"Gib mir doch irgendein Erkennungszeichen", sagt er.</ta>
            <ta e="T218" id="Seg_6136" s="T204">"Nein, gebe ich nicht, warum ist er so dumm, Queräugiger, lieber stirbst du", sagt er und schickt den Schamanen wieder zurück.</ta>
            <ta e="T224" id="Seg_6137" s="T218">Als er aufhörte zu schamanisieren, fleht der Schamane den Fürsten an:</ta>
            <ta e="T238" id="Seg_6138" s="T224">"Aller guten Dinge sind drei, töte mich nicht, weil er deinen Wunsch nicht erfüllt hat, morgen versuche ich es noch mal", sagt er.</ta>
            <ta e="T242" id="Seg_6139" s="T238">Am nächsten Morgen schamanisiert er das dritte Mal.</ta>
            <ta e="T247" id="Seg_6140" s="T242">Er schamanisierte und kommt zu Ürüng Ajyys Haus.</ta>
            <ta e="T262" id="Seg_6141" s="T247">Er kommt an, verbeugt sich vor dem Herrn der Hauptstange seines Zeltes, kriecht zu den Füßen von Ürüng Ajyy und fleht und weint noch mehr als vorher.</ta>
            <ta e="T266" id="Seg_6142" s="T262">Da wurde sein Herz weich:</ta>
            <ta e="T274" id="Seg_6143" s="T266">"Es ist wahr, ich habe dich nicht zu meinem Schamanen gemacht, um dich zu töten.</ta>
            <ta e="T279" id="Seg_6144" s="T274">Warum ist dein Fürst so dumm?!</ta>
            <ta e="T284" id="Seg_6145" s="T279">Wenn er seinem Volk Unglück bringt.</ta>
            <ta e="T290" id="Seg_6146" s="T284">Aus eurem Volk bleiben alleine du und dein Sohn übrig.</ta>
            <ta e="T295" id="Seg_6147" s="T290">Sonst bleibt keiner am Leben.</ta>
            <ta e="T304" id="Seg_6148" s="T295">Von anderen Orten werden Leute kommen, daraus wird ein neues Volk entstehen", sagt er.</ta>
            <ta e="T325" id="Seg_6149" s="T304">"Nach drei Tagen am Morgen sollen sie eine sauber, weiße Decke hinlegen, sie sollen weiß-bunte Rentiere töten, sie sollen um das Sommerzelt herum weiße Rentierfelle ausbreiten.</ta>
            <ta e="T329" id="Seg_6150" s="T325">Euer Fürst wird sie betreten und hineingehen.</ta>
            <ta e="T335" id="Seg_6151" s="T329">Nichts Schmutziges soll daran haften", sagt er.</ta>
            <ta e="T346" id="Seg_6152" s="T335">"Die Leute sollen nicht kommen und denken, dass es eine Hochzeit gibt, wer seine Seele retten möchte, soll weiter gehen", sagte er.</ta>
            <ta e="T351" id="Seg_6153" s="T346">Am dritten Tag machten sie es so, wie es gesagt worden war.</ta>
            <ta e="T355" id="Seg_6154" s="T351">Der Schamane schaute sich das alles an.</ta>
            <ta e="T365" id="Seg_6155" s="T355">Da kam plötzlich ein sehr starker Wirbelwind auf, es war ein lautes Geräusch von Klingeln zu hören.</ta>
            <ta e="T374" id="Seg_6156" s="T365">Als ob der Wind die Tür des Zeltes aufreißen würde.</ta>
            <ta e="T379" id="Seg_6157" s="T374">Danach legte sich der Wind wieder.</ta>
            <ta e="T392" id="Seg_6158" s="T379">Da kam der Schamane heran und sah, dass sowohl der Fürst als auch alle Leute, die gekommen waren, tot da lagen.</ta>
            <ta e="T395" id="Seg_6159" s="T392">So wurden es wenige Norilsker.</ta>
            <ta e="T403" id="Seg_6160" s="T395">So kam der Stamm der Edjany und ließ sich hier nieder.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_6161" s="T0">В старину норильские имели князя, говорят.</ta>
            <ta e="T6" id="Seg_6162" s="T4">Молодого человека.</ta>
            <ta e="T14" id="Seg_6163" s="T6">У этого человека нет жены, сородичи однажды его спрашивают:</ta>
            <ta e="T17" id="Seg_6164" s="T14">"Почему не женишься?", говорят.</ta>
            <ta e="T19" id="Seg_6165" s="T17">На это говорит:</ta>
            <ta e="T25" id="Seg_6166" s="T19">"Я не такой человек, чтоб брать [в жены] девушку этого мира.</ta>
            <ta e="T37" id="Seg_6167" s="T25">В Верхнем мире живет Юрюнг Айыы, если бы взять его дочь, тогда бы согласился", так говорит.</ta>
            <ta e="T43" id="Seg_6168" s="T37">Люди думают, что он пошутил.</ta>
            <ta e="T48" id="Seg_6169" s="T43">Однажды, когда собирается народ, князь принуждает шамана:</ta>
            <ta e="T58" id="Seg_6170" s="T48">"Ты мне сосватай в жены дочь Юрюнг Айны, живущего в Верхнем мире", говорит.</ta>
            <ta e="T62" id="Seg_6171" s="T58">Шаман слушает это со страхом:</ta>
            <ta e="T72" id="Seg_6172" s="T62">"Что ты говоришь, из того мира я никого не могу привести", говорит.</ta>
            <ta e="T77" id="Seg_6173" s="T72">Тут князь показывает семь прутьев:</ta>
            <ta e="T79" id="Seg_6174" s="T77">"Ты не обманывай мена.</ta>
            <ta e="T90" id="Seg_6175" s="T79">Я тебе шкуру со спины спущу, чтоб завтра же дочь Юрюнг Айыы была здесь!" говорит.</ta>
            <ta e="T96" id="Seg_6176" s="T90">Вот шаман, не пропадать же ему, начинает камлать.</ta>
            <ta e="T104" id="Seg_6177" s="T96">Услышав приближение этого шамана, господин Юрюнг Айыы страшно разгневается:</ta>
            <ta e="T111" id="Seg_6178" s="T104">"Как смеет шаман Нижнего мира являться без ведома?!</ta>
            <ta e="T119" id="Seg_6179" s="T111">Подайте мне мой огненный меч, чтобы отсекать ему голову!", сказал.</ta>
            <ta e="T125" id="Seg_6180" s="T119">Шаман приблизился: айыы сидел уже готов, оказывается.</ta>
            <ta e="T129" id="Seg_6181" s="T125">На это умоляет-упрашивает:</ta>
            <ta e="T139" id="Seg_6182" s="T129">"Я по принуждению явился, на земле у нас есть властелин, подобный тебе, "князь" называется.</ta>
            <ta e="T146" id="Seg_6183" s="T139">По его принуждению явился", так жалуясь, поведал о своей цели.</ta>
            <ta e="T149" id="Seg_6184" s="T146">На это Юрюнг Айыы говорит:</ta>
            <ta e="T150" id="Seg_6185" s="T149">"Нет!</ta>
            <ta e="T155" id="Seg_6186" s="T150">Пусть откажется от этой мысли, иди и передай ему", так говорит, возвращая шамана.</ta>
            <ta e="T160" id="Seg_6187" s="T155">Когда тот кончает камлать, князь принуждает [его]:</ta>
            <ta e="T165" id="Seg_6188" s="T160">"Ты притворяешься, будто куда-то ездил.</ta>
            <ta e="T173" id="Seg_6189" s="T165">Песцовая шкура, предназначенная в жертву, здесь же висит?", так говоря, опять требует:</ta>
            <ta e="T179" id="Seg_6190" s="T173">"Приведи мне непременно дочь [айыы]!", говорит.</ta>
            <ta e="T186" id="Seg_6191" s="T179">Ну вот, утром тот шаман опять поневоле стал камлать.</ta>
            <ta e="T195" id="Seg_6192" s="T186">Когда, камлая, прибыл к Юрюнг Айыы, тот еще пуще сердится на шамана.</ta>
            <ta e="T200" id="Seg_6193" s="T195">На это шаман опять плачется-мается:</ta>
            <ta e="T204" id="Seg_6194" s="T200">"Хоть примету какую подай", говорит.</ta>
            <ta e="T218" id="Seg_6195" s="T204">"Нет, не дам, до чего же глупый [князь], лучше ты помрешь, поперечноглазый", так опять возвращает шамана.</ta>
            <ta e="T224" id="Seg_6196" s="T218">Кончив камлать, шаман умоляет князя:</ta>
            <ta e="T238" id="Seg_6197" s="T224">"Все проходит три круга, не убивай за то, что не исполнил твоего желания, завтра опять попытаюсь", говорит.</ta>
            <ta e="T242" id="Seg_6198" s="T238">Назавтра третье камлание совершает.</ta>
            <ta e="T247" id="Seg_6199" s="T242">Так камлая, восходит к обители Юрюнг Айыы.</ta>
            <ta e="T262" id="Seg_6200" s="T247">Дойдя, молясь духу главного столба его жилища, подползает к ногам Юрюнг Айыы и пуще прежнего страшно плачется.</ta>
            <ta e="T266" id="Seg_6201" s="T262">На это [Юрюнг Айыы] смягчается:</ta>
            <ta e="T274" id="Seg_6202" s="T266">"Да, правда, я определил тебя своим шаманом не для того, чтоб погубить.</ta>
            <ta e="T279" id="Seg_6203" s="T274">Разве такой глупый твой князь?!</ta>
            <ta e="T284" id="Seg_6204" s="T279">На свой народ он беду навлек.</ta>
            <ta e="T290" id="Seg_6205" s="T284">Из вашего племени в живых останешься только ты со своим сыном.</ta>
            <ta e="T295" id="Seg_6206" s="T290">Никто [больше] в живых не останется.</ta>
            <ta e="T304" id="Seg_6207" s="T295">Из других мест придут люди, от них пойдет новый народ", говорит.</ta>
            <ta e="T325" id="Seg_6208" s="T304">"Через три дня утром пусть положить чистую, белую покрышку, пусть забьют бело-пестрого оленя, вокруг урасы настелят белые оленьи шкуры.</ta>
            <ta e="T329" id="Seg_6209" s="T325">Ступая по ним, князь войдет [в урасу].</ta>
            <ta e="T335" id="Seg_6210" s="T329">Ничто нечистое не должно коснуться [всего этого]", говорит.</ta>
            <ta e="T346" id="Seg_6211" s="T335">"Думая, что это свадьба, придут люди, но если хотят уберечь свои души, то пусть уйдут подальше [от этого места]", сказал.</ta>
            <ta e="T351" id="Seg_6212" s="T346">На третий день сделали так, как он сказал.</ta>
            <ta e="T355" id="Seg_6213" s="T351">Шаман на все это смотрел.</ta>
            <ta e="T365" id="Seg_6214" s="T355">Вот задул сильный порывистый ветер, послышался громкий звон бубенчиков.</ta>
            <ta e="T374" id="Seg_6215" s="T365">Будто бы ветер рванул дверную полость урасы.</ta>
            <ta e="T379" id="Seg_6216" s="T374">После этого ветра вдруг не стало.</ta>
            <ta e="T392" id="Seg_6217" s="T379">Тут шаман подошел и увидел: и князь, и все пришедшие люди лежат мертвые.</ta>
            <ta e="T395" id="Seg_6218" s="T392">После этого норильские зачахли.</ta>
            <ta e="T403" id="Seg_6219" s="T395">Эдяны со стороны прикочевали и здесь расселились.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T4" id="Seg_6220" s="T0">В старину норильские имели князя, говорят.</ta>
            <ta e="T6" id="Seg_6221" s="T4">Молодого человека.</ta>
            <ta e="T14" id="Seg_6222" s="T6">У этого человека не было жены, сородичи однажды его спросили:</ta>
            <ta e="T17" id="Seg_6223" s="T14">— Почему не женишься?</ta>
            <ta e="T19" id="Seg_6224" s="T17">На это ответил:</ta>
            <ta e="T25" id="Seg_6225" s="T19">— Я не такой человек, чтоб брать [в жены] девушку этого мира.</ta>
            <ta e="T37" id="Seg_6226" s="T25">В Верхнем мире живет Юрюнг Айыы, если бы взять его дочь, тогда бы согласился, — так сказал.</ta>
            <ta e="T43" id="Seg_6227" s="T37">Люди подумали, что он пошутил.</ta>
            <ta e="T48" id="Seg_6228" s="T43">Однажды, когда собрался народ, князь стал принуждать шамана:</ta>
            <ta e="T58" id="Seg_6229" s="T48">— Ты мне сосватай в жены дочь Юрюнг Айны, живущего в Верхнем мире, — говорит.</ta>
            <ta e="T62" id="Seg_6230" s="T58">Шаман это выслушал со страхом:</ta>
            <ta e="T72" id="Seg_6231" s="T62">— Что ты говоришь, из того мира я никого не могу привести, — говорит.</ta>
            <ta e="T77" id="Seg_6232" s="T72">Тут князь показывает семь прутьев:</ta>
            <ta e="T79" id="Seg_6233" s="T77">— Ты не обманывай мена.</ta>
            <ta e="T90" id="Seg_6234" s="T79">Я тебе шкуру со спины спущу, чтоб завтра же дочь Юрюнг Айыы была здесь! — говорит.</ta>
            <ta e="T96" id="Seg_6235" s="T90">Вот шаман, не пропадать же ему, начинает камлать.</ta>
            <ta e="T104" id="Seg_6236" s="T96">Услышав приближение этого шамана, Юрюнг Айыы-тойон страшно разгневался:</ta>
            <ta e="T111" id="Seg_6237" s="T104">— Как смеет шаман Нижнего мира являться без ведома?!</ta>
            <ta e="T119" id="Seg_6238" s="T111">Подайте мне мой огненный меч, отсеку ему голову! — сказал.</ta>
            <ta e="T125" id="Seg_6239" s="T119">Шаман приблизился: айыы сидит в гневе, оказывается.</ta>
            <ta e="T129" id="Seg_6240" s="T125">Поэтому стал умолять-упрашивать:</ta>
            <ta e="T139" id="Seg_6241" s="T129">— Я по принуждению явился, на земле у нас есть властелин, подобный тебе, "князь" называется.</ta>
            <ta e="T146" id="Seg_6242" s="T139">По его принуждению явился, — так жалуясь, поведал о своей цели.</ta>
            <ta e="T149" id="Seg_6243" s="T146">На это Юрюнг Айыы говорит:</ta>
            <ta e="T150" id="Seg_6244" s="T149">— Нет!</ta>
            <ta e="T155" id="Seg_6245" s="T150">Пусть откажется от этой мысли, иди и передай ему, — так сказал, возвращая шамана.</ta>
            <ta e="T160" id="Seg_6246" s="T155">Когда тот кончил камлать, князь стал уличать ею [во лжи]:</ta>
            <ta e="T165" id="Seg_6247" s="T160">— Ты притворяешься, будто куда-то ездил.</ta>
            <ta e="T173" id="Seg_6248" s="T165">Песцовая шкура, предназначенная в жертву, здесь же висит? — так говоря, опять требует:</ta>
            <ta e="T179" id="Seg_6249" s="T173">— Приведи мне непременно дочь [айыы]!</ta>
            <ta e="T186" id="Seg_6250" s="T179">Ну вот, утром тот шаман опять поневоле стал камлать.</ta>
            <ta e="T195" id="Seg_6251" s="T186">Когда, камлая, прибыл к Юрюнг Айыы, тот еще пуще сердится на шамана.</ta>
            <ta e="T200" id="Seg_6252" s="T195">На это шаман опять плачется-мается:</ta>
            <ta e="T204" id="Seg_6253" s="T200">— Хоть примету какую подай, — говорит.</ta>
            <ta e="T218" id="Seg_6254" s="T204">— Нет, не дам, до чего же глупый [князь], лучше ты помрешь, поперечноглазый, — так опять возвращает шамана.</ta>
            <ta e="T224" id="Seg_6255" s="T218">Кончив камлать, шаман умоляет князя:</ta>
            <ta e="T238" id="Seg_6256" s="T224">— Все проходит три круга, не убивай за то, что не исполнил твоего желания, завтра опять попытаюсь, — говорит.</ta>
            <ta e="T242" id="Seg_6257" s="T238">Назавтра третье камлание совершает.</ta>
            <ta e="T247" id="Seg_6258" s="T242">Так камлая, восходит к обители Юрюнг Айыы.</ta>
            <ta e="T262" id="Seg_6259" s="T247">Дойдя, молясь духу главного столба его жилища, подползает к ногам Юрюнг Айыы и пуще прежнего страшно плачется.</ta>
            <ta e="T266" id="Seg_6260" s="T262">На это [Юрюнг Айыы] смягчается:</ta>
            <ta e="T274" id="Seg_6261" s="T266">— Да, правда, я определил тебя своим шаманом не для того, чтоб погубить.</ta>
            <ta e="T279" id="Seg_6262" s="T274">Разве такой глупый твой князь?!</ta>
            <ta e="T284" id="Seg_6263" s="T279">На свой народ он беду навлек.</ta>
            <ta e="T290" id="Seg_6264" s="T284">Из вашего племени в живых останешься только ты со своим сыном.</ta>
            <ta e="T295" id="Seg_6265" s="T290">Никто [больше] в живых не останется.</ta>
            <ta e="T304" id="Seg_6266" s="T295">Из других мест придут люди, от них пойдет новый народ, — говорит.</ta>
            <ta e="T325" id="Seg_6267" s="T304">— Через три дня утром пусть поставят новую урасу из чистых белых покрышек, пусть забьют бело-пестрого оленя, вокруг урасы настелят белые оленьи шкуры.</ta>
            <ta e="T329" id="Seg_6268" s="T325">Ступая по ним, князь войдет [в урасу].</ta>
            <ta e="T335" id="Seg_6269" s="T329">Ничто нечистое не должно коснуться [всего этого], — говорит.</ta>
            <ta e="T346" id="Seg_6270" s="T335">— Думая, что это свадьба, придут люди, но если хотят уберечь свои души, то пусть уйдут подальше [от этого места], — сказал.</ta>
            <ta e="T351" id="Seg_6271" s="T346">На третий день сделали так, как он сказал.</ta>
            <ta e="T355" id="Seg_6272" s="T351">Шаман на все это смотрел.</ta>
            <ta e="T365" id="Seg_6273" s="T355">Вот задул сильный порывистый ветер, послышался громкий звон бубенчиков.</ta>
            <ta e="T374" id="Seg_6274" s="T365">Будто бы ветер рванул дверную полость урасы.</ta>
            <ta e="T379" id="Seg_6275" s="T374">После этого ветра вдруг не стало.</ta>
            <ta e="T392" id="Seg_6276" s="T379">Тут шаман подошел и увидел: и князь, и все пришедшие люди лежат мертвые.</ta>
            <ta e="T395" id="Seg_6277" s="T392">После этого норильские зачахли.</ta>
            <ta e="T403" id="Seg_6278" s="T395">Эдяны со стороны прикочевали и здесь расселились.</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg"
                          name="gg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top"
                          name="Top"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc"
                          name="Foc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
