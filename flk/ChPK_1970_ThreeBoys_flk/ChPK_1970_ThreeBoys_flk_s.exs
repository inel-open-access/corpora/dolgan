<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID125A58CB-926C-AB9A-CA87-D40CEA0CC189">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>ChPK_1970_ThreeBoys_flk</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="creation-date">2017-01-03T11:40:21.74+01:00</ud-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\flk\ChPK_1970_ThreeBoys_flk\ChPK_1970_ThreeBoys_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">999</ud-information>
            <ud-information attribute-name="# HIAT:w">748</ud-information>
            <ud-information attribute-name="# e">747</ud-information>
            <ud-information attribute-name="# HIAT:u">115</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="ChPK">
            <abbreviation>ChPK</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="4.0" type="appl" />
         <tli id="T1" time="4.5" type="appl" />
         <tli id="T2" time="5.0" type="appl" />
         <tli id="T3" time="5.5" type="appl" />
         <tli id="T4" time="6.0" type="appl" />
         <tli id="T5" time="6.5" type="appl" />
         <tli id="T6" time="7.0" type="appl" />
         <tli id="T7" time="7.5" type="appl" />
         <tli id="T8" time="8.0" type="appl" />
         <tli id="T9" time="8.5" type="appl" />
         <tli id="T10" time="9.0" type="appl" />
         <tli id="T11" time="9.5" type="appl" />
         <tli id="T12" time="10.0" type="appl" />
         <tli id="T13" time="10.5" type="appl" />
         <tli id="T14" time="11.0" type="appl" />
         <tli id="T15" time="11.5" type="appl" />
         <tli id="T16" time="12.0" type="appl" />
         <tli id="T17" time="12.5" type="appl" />
         <tli id="T18" time="13.0" type="appl" />
         <tli id="T19" time="13.5" type="appl" />
         <tli id="T20" time="14.0" type="appl" />
         <tli id="T21" time="14.5" type="appl" />
         <tli id="T22" time="15.0" type="appl" />
         <tli id="T23" time="15.5" type="appl" />
         <tli id="T24" time="16.0" type="appl" />
         <tli id="T25" time="16.5" type="appl" />
         <tli id="T26" time="17.0" type="appl" />
         <tli id="T27" time="17.5" type="appl" />
         <tli id="T28" time="18.0" type="appl" />
         <tli id="T29" time="18.5" type="appl" />
         <tli id="T30" time="19.0" type="appl" />
         <tli id="T31" time="19.5" type="appl" />
         <tli id="T32" time="20.0" type="appl" />
         <tli id="T33" time="20.5" type="appl" />
         <tli id="T34" time="21.0" type="appl" />
         <tli id="T35" time="21.5" type="appl" />
         <tli id="T36" time="22.0" type="appl" />
         <tli id="T37" time="22.5" type="appl" />
         <tli id="T38" time="23.0" type="appl" />
         <tli id="T39" time="23.5" type="appl" />
         <tli id="T40" time="24.0" type="appl" />
         <tli id="T41" time="24.5" type="appl" />
         <tli id="T42" time="25.0" type="appl" />
         <tli id="T43" time="25.5" type="appl" />
         <tli id="T44" time="26.0" type="appl" />
         <tli id="T45" time="26.5" type="appl" />
         <tli id="T46" time="27.0" type="appl" />
         <tli id="T47" time="27.5" type="appl" />
         <tli id="T48" time="28.0" type="appl" />
         <tli id="T49" time="28.5" type="appl" />
         <tli id="T50" time="29.0" type="appl" />
         <tli id="T51" time="29.5" type="appl" />
         <tli id="T52" time="30.0" type="appl" />
         <tli id="T53" time="30.5" type="appl" />
         <tli id="T54" time="31.0" type="appl" />
         <tli id="T55" time="31.5" type="appl" />
         <tli id="T56" time="32.0" type="appl" />
         <tli id="T57" time="32.5" type="appl" />
         <tli id="T58" time="33.0" type="appl" />
         <tli id="T59" time="33.5" type="appl" />
         <tli id="T60" time="34.0" type="appl" />
         <tli id="T61" time="34.5" type="appl" />
         <tli id="T62" time="35.0" type="appl" />
         <tli id="T63" time="35.5" type="appl" />
         <tli id="T64" time="36.0" type="appl" />
         <tli id="T65" time="36.5" type="appl" />
         <tli id="T66" time="37.0" type="appl" />
         <tli id="T67" time="37.5" type="appl" />
         <tli id="T68" time="38.0" type="appl" />
         <tli id="T69" time="38.5" type="appl" />
         <tli id="T70" time="39.0" type="appl" />
         <tli id="T71" time="39.5" type="appl" />
         <tli id="T72" time="40.0" type="appl" />
         <tli id="T73" time="40.5" type="appl" />
         <tli id="T74" time="41.0" type="appl" />
         <tli id="T75" time="41.5" type="appl" />
         <tli id="T76" time="42.0" type="appl" />
         <tli id="T77" time="42.5" type="appl" />
         <tli id="T78" time="43.0" type="appl" />
         <tli id="T79" time="43.5" type="appl" />
         <tli id="T80" time="44.0" type="appl" />
         <tli id="T81" time="44.5" type="appl" />
         <tli id="T82" time="45.0" type="appl" />
         <tli id="T83" time="45.5" type="appl" />
         <tli id="T84" time="46.0" type="appl" />
         <tli id="T85" time="46.5" type="appl" />
         <tli id="T86" time="47.0" type="appl" />
         <tli id="T87" time="47.5" type="appl" />
         <tli id="T88" time="48.0" type="appl" />
         <tli id="T89" time="48.5" type="appl" />
         <tli id="T90" time="49.0" type="appl" />
         <tli id="T91" time="49.5" type="appl" />
         <tli id="T92" time="50.0" type="appl" />
         <tli id="T93" time="50.5" type="appl" />
         <tli id="T94" time="51.0" type="appl" />
         <tli id="T95" time="51.5" type="appl" />
         <tli id="T96" time="52.0" type="appl" />
         <tli id="T97" time="52.5" type="appl" />
         <tli id="T98" time="53.0" type="appl" />
         <tli id="T99" time="53.5" type="appl" />
         <tli id="T100" time="54.0" type="appl" />
         <tli id="T101" time="54.5" type="appl" />
         <tli id="T102" time="55.0" type="appl" />
         <tli id="T103" time="55.5" type="appl" />
         <tli id="T104" time="56.0" type="appl" />
         <tli id="T105" time="56.5" type="appl" />
         <tli id="T106" time="57.0" type="appl" />
         <tli id="T107" time="57.5" type="appl" />
         <tli id="T108" time="58.0" type="appl" />
         <tli id="T109" time="58.5" type="appl" />
         <tli id="T110" time="59.0" type="appl" />
         <tli id="T111" time="59.5" type="appl" />
         <tli id="T112" time="60.0" type="appl" />
         <tli id="T113" time="60.5" type="appl" />
         <tli id="T114" time="61.0" type="appl" />
         <tli id="T115" time="61.5" type="appl" />
         <tli id="T116" time="62.0" type="appl" />
         <tli id="T117" time="62.5" type="appl" />
         <tli id="T118" time="63.0" type="appl" />
         <tli id="T119" time="63.5" type="appl" />
         <tli id="T120" time="64.0" type="appl" />
         <tli id="T121" time="64.5" type="appl" />
         <tli id="T122" time="65.0" type="appl" />
         <tli id="T123" time="65.5" type="appl" />
         <tli id="T124" time="66.0" type="appl" />
         <tli id="T125" time="66.5" type="appl" />
         <tli id="T126" time="67.0" type="appl" />
         <tli id="T127" time="67.5" type="appl" />
         <tli id="T128" time="68.0" type="appl" />
         <tli id="T129" time="68.5" type="appl" />
         <tli id="T130" time="69.0" type="appl" />
         <tli id="T131" time="69.5" type="appl" />
         <tli id="T132" time="70.0" type="appl" />
         <tli id="T133" time="70.5" type="appl" />
         <tli id="T134" time="71.0" type="appl" />
         <tli id="T135" time="71.5" type="appl" />
         <tli id="T136" time="72.0" type="appl" />
         <tli id="T137" time="72.5" type="appl" />
         <tli id="T138" time="73.0" type="appl" />
         <tli id="T139" time="73.5" type="appl" />
         <tli id="T140" time="74.0" type="appl" />
         <tli id="T141" time="74.5" type="appl" />
         <tli id="T142" time="75.0" type="appl" />
         <tli id="T143" time="75.5" type="appl" />
         <tli id="T144" time="76.0" type="appl" />
         <tli id="T145" time="76.5" type="appl" />
         <tli id="T146" time="77.0" type="appl" />
         <tli id="T147" time="77.5" type="appl" />
         <tli id="T148" time="78.0" type="appl" />
         <tli id="T149" time="78.5" type="appl" />
         <tli id="T150" time="79.0" type="appl" />
         <tli id="T151" time="79.5" type="appl" />
         <tli id="T152" time="80.0" type="appl" />
         <tli id="T153" time="80.5" type="appl" />
         <tli id="T154" time="81.0" type="appl" />
         <tli id="T155" time="81.5" type="appl" />
         <tli id="T156" time="82.0" type="appl" />
         <tli id="T157" time="82.5" type="appl" />
         <tli id="T158" time="83.0" type="appl" />
         <tli id="T159" time="83.5" type="appl" />
         <tli id="T160" time="84.0" type="appl" />
         <tli id="T161" time="84.5" type="appl" />
         <tli id="T162" time="85.0" type="appl" />
         <tli id="T163" time="85.5" type="appl" />
         <tli id="T164" time="86.0" type="appl" />
         <tli id="T165" time="86.5" type="appl" />
         <tli id="T166" time="87.0" type="appl" />
         <tli id="T167" time="87.5" type="appl" />
         <tli id="T168" time="88.0" type="appl" />
         <tli id="T169" time="88.5" type="appl" />
         <tli id="T170" time="89.0" type="appl" />
         <tli id="T171" time="89.5" type="appl" />
         <tli id="T172" time="90.0" type="appl" />
         <tli id="T173" time="90.5" type="appl" />
         <tli id="T174" time="91.0" type="appl" />
         <tli id="T175" time="91.5" type="appl" />
         <tli id="T176" time="92.0" type="appl" />
         <tli id="T177" time="92.5" type="appl" />
         <tli id="T178" time="93.0" type="appl" />
         <tli id="T179" time="93.5" type="appl" />
         <tli id="T180" time="94.0" type="appl" />
         <tli id="T181" time="94.5" type="appl" />
         <tli id="T182" time="95.0" type="appl" />
         <tli id="T183" time="95.5" type="appl" />
         <tli id="T184" time="96.0" type="appl" />
         <tli id="T185" time="96.5" type="appl" />
         <tli id="T186" time="97.0" type="appl" />
         <tli id="T187" time="97.5" type="appl" />
         <tli id="T188" time="98.0" type="appl" />
         <tli id="T189" time="98.5" type="appl" />
         <tli id="T190" time="99.0" type="appl" />
         <tli id="T191" time="99.5" type="appl" />
         <tli id="T192" time="100.0" type="appl" />
         <tli id="T193" time="100.5" type="appl" />
         <tli id="T194" time="101.0" type="appl" />
         <tli id="T195" time="101.5" type="appl" />
         <tli id="T196" time="102.0" type="appl" />
         <tli id="T197" time="102.5" type="appl" />
         <tli id="T198" time="103.0" type="appl" />
         <tli id="T199" time="103.5" type="appl" />
         <tli id="T200" time="104.0" type="appl" />
         <tli id="T201" time="104.5" type="appl" />
         <tli id="T202" time="105.0" type="appl" />
         <tli id="T203" time="105.5" type="appl" />
         <tli id="T204" time="106.0" type="appl" />
         <tli id="T205" time="106.5" type="appl" />
         <tli id="T206" time="107.0" type="appl" />
         <tli id="T207" time="107.5" type="appl" />
         <tli id="T208" time="108.0" type="appl" />
         <tli id="T209" time="108.5" type="appl" />
         <tli id="T210" time="109.0" type="appl" />
         <tli id="T211" time="109.5" type="appl" />
         <tli id="T212" time="110.0" type="appl" />
         <tli id="T213" time="110.5" type="appl" />
         <tli id="T214" time="111.0" type="appl" />
         <tli id="T215" time="111.5" type="appl" />
         <tli id="T216" time="112.0" type="appl" />
         <tli id="T217" time="112.5" type="appl" />
         <tli id="T218" time="113.0" type="appl" />
         <tli id="T219" time="113.5" type="appl" />
         <tli id="T220" time="114.0" type="appl" />
         <tli id="T221" time="114.5" type="appl" />
         <tli id="T222" time="115.0" type="appl" />
         <tli id="T223" time="115.5" type="appl" />
         <tli id="T224" time="116.0" type="appl" />
         <tli id="T225" time="116.5" type="appl" />
         <tli id="T226" time="117.0" type="appl" />
         <tli id="T227" time="117.5" type="appl" />
         <tli id="T228" time="118.0" type="appl" />
         <tli id="T229" time="118.5" type="appl" />
         <tli id="T230" time="119.0" type="appl" />
         <tli id="T231" time="119.5" type="appl" />
         <tli id="T232" time="120.0" type="appl" />
         <tli id="T233" time="120.5" type="appl" />
         <tli id="T234" time="121.0" type="appl" />
         <tli id="T235" time="121.5" type="appl" />
         <tli id="T236" time="122.0" type="appl" />
         <tli id="T237" time="122.5" type="appl" />
         <tli id="T238" time="123.0" type="appl" />
         <tli id="T239" time="123.5" type="appl" />
         <tli id="T240" time="124.0" type="appl" />
         <tli id="T241" time="124.5" type="appl" />
         <tli id="T242" time="125.0" type="appl" />
         <tli id="T243" time="125.5" type="appl" />
         <tli id="T244" time="126.0" type="appl" />
         <tli id="T245" time="126.5" type="appl" />
         <tli id="T246" time="127.0" type="appl" />
         <tli id="T247" time="127.5" type="appl" />
         <tli id="T248" time="128.0" type="appl" />
         <tli id="T249" time="128.5" type="appl" />
         <tli id="T250" time="129.0" type="appl" />
         <tli id="T251" time="129.5" type="appl" />
         <tli id="T252" time="130.0" type="appl" />
         <tli id="T253" time="130.5" type="appl" />
         <tli id="T254" time="131.0" type="appl" />
         <tli id="T255" time="131.5" type="appl" />
         <tli id="T256" time="132.0" type="appl" />
         <tli id="T257" time="132.5" type="appl" />
         <tli id="T258" time="133.0" type="appl" />
         <tli id="T259" time="133.5" type="appl" />
         <tli id="T260" time="134.0" type="appl" />
         <tli id="T261" time="134.5" type="appl" />
         <tli id="T262" time="135.0" type="appl" />
         <tli id="T263" time="135.5" type="appl" />
         <tli id="T264" time="136.0" type="appl" />
         <tli id="T265" time="136.5" type="appl" />
         <tli id="T266" time="137.0" type="appl" />
         <tli id="T267" time="137.5" type="appl" />
         <tli id="T268" time="138.0" type="appl" />
         <tli id="T269" time="138.5" type="appl" />
         <tli id="T270" time="139.0" type="appl" />
         <tli id="T271" time="139.5" type="appl" />
         <tli id="T272" time="140.0" type="appl" />
         <tli id="T273" time="140.5" type="appl" />
         <tli id="T274" time="141.0" type="appl" />
         <tli id="T275" time="141.5" type="appl" />
         <tli id="T276" time="142.0" type="appl" />
         <tli id="T277" time="142.5" type="appl" />
         <tli id="T278" time="143.0" type="appl" />
         <tli id="T279" time="143.5" type="appl" />
         <tli id="T280" time="144.0" type="appl" />
         <tli id="T281" time="144.5" type="appl" />
         <tli id="T282" time="145.0" type="appl" />
         <tli id="T283" time="145.5" type="appl" />
         <tli id="T284" time="146.0" type="appl" />
         <tli id="T285" time="146.5" type="appl" />
         <tli id="T286" time="147.0" type="appl" />
         <tli id="T287" time="147.5" type="appl" />
         <tli id="T288" time="148.0" type="appl" />
         <tli id="T289" time="148.5" type="appl" />
         <tli id="T290" time="149.0" type="appl" />
         <tli id="T291" time="149.5" type="appl" />
         <tli id="T292" time="150.0" type="appl" />
         <tli id="T293" time="150.5" type="appl" />
         <tli id="T294" time="151.0" type="appl" />
         <tli id="T295" time="151.5" type="appl" />
         <tli id="T296" time="152.0" type="appl" />
         <tli id="T297" time="152.5" type="appl" />
         <tli id="T298" time="153.0" type="appl" />
         <tli id="T299" time="153.5" type="appl" />
         <tli id="T300" time="154.0" type="appl" />
         <tli id="T301" time="154.5" type="appl" />
         <tli id="T302" time="155.0" type="appl" />
         <tli id="T303" time="155.5" type="appl" />
         <tli id="T304" time="156.0" type="appl" />
         <tli id="T305" time="156.5" type="appl" />
         <tli id="T306" time="157.0" type="appl" />
         <tli id="T307" time="157.5" type="appl" />
         <tli id="T308" time="158.0" type="appl" />
         <tli id="T309" time="158.5" type="appl" />
         <tli id="T310" time="159.0" type="appl" />
         <tli id="T311" time="159.5" type="appl" />
         <tli id="T312" time="160.0" type="appl" />
         <tli id="T313" time="160.5" type="appl" />
         <tli id="T314" time="161.0" type="appl" />
         <tli id="T315" time="161.5" type="appl" />
         <tli id="T316" time="162.0" type="appl" />
         <tli id="T317" time="162.5" type="appl" />
         <tli id="T318" time="163.0" type="appl" />
         <tli id="T319" time="163.5" type="appl" />
         <tli id="T320" time="164.0" type="appl" />
         <tli id="T321" time="164.5" type="appl" />
         <tli id="T322" time="165.0" type="appl" />
         <tli id="T323" time="165.5" type="appl" />
         <tli id="T324" time="166.0" type="appl" />
         <tli id="T325" time="166.5" type="appl" />
         <tli id="T326" time="167.0" type="appl" />
         <tli id="T327" time="167.5" type="appl" />
         <tli id="T328" time="168.0" type="appl" />
         <tli id="T329" time="168.5" type="appl" />
         <tli id="T330" time="169.0" type="appl" />
         <tli id="T331" time="169.5" type="appl" />
         <tli id="T332" time="170.0" type="appl" />
         <tli id="T333" time="170.5" type="appl" />
         <tli id="T334" time="171.0" type="appl" />
         <tli id="T335" time="171.5" type="appl" />
         <tli id="T336" time="172.0" type="appl" />
         <tli id="T337" time="172.5" type="appl" />
         <tli id="T338" time="173.0" type="appl" />
         <tli id="T339" time="173.5" type="appl" />
         <tli id="T340" time="174.0" type="appl" />
         <tli id="T341" time="174.5" type="appl" />
         <tli id="T342" time="175.0" type="appl" />
         <tli id="T343" time="175.5" type="appl" />
         <tli id="T344" time="176.0" type="appl" />
         <tli id="T345" time="176.5" type="appl" />
         <tli id="T346" time="177.0" type="appl" />
         <tli id="T347" time="177.5" type="appl" />
         <tli id="T348" time="178.0" type="appl" />
         <tli id="T349" time="178.5" type="appl" />
         <tli id="T350" time="179.0" type="appl" />
         <tli id="T351" time="179.5" type="appl" />
         <tli id="T352" time="180.0" type="appl" />
         <tli id="T353" time="180.5" type="appl" />
         <tli id="T354" time="181.0" type="appl" />
         <tli id="T355" time="181.5" type="appl" />
         <tli id="T356" time="182.0" type="appl" />
         <tli id="T357" time="182.5" type="appl" />
         <tli id="T358" time="183.0" type="appl" />
         <tli id="T359" time="183.5" type="appl" />
         <tli id="T360" time="184.0" type="appl" />
         <tli id="T361" time="184.5" type="appl" />
         <tli id="T362" time="185.0" type="appl" />
         <tli id="T363" time="185.5" type="appl" />
         <tli id="T364" time="186.0" type="appl" />
         <tli id="T365" time="186.5" type="appl" />
         <tli id="T366" time="187.0" type="appl" />
         <tli id="T367" time="187.5" type="appl" />
         <tli id="T368" time="188.0" type="appl" />
         <tli id="T369" time="188.5" type="appl" />
         <tli id="T370" time="189.0" type="appl" />
         <tli id="T371" time="189.5" type="appl" />
         <tli id="T372" time="190.0" type="appl" />
         <tli id="T373" time="190.5" type="appl" />
         <tli id="T374" time="191.0" type="appl" />
         <tli id="T375" time="191.5" type="appl" />
         <tli id="T376" time="192.0" type="appl" />
         <tli id="T377" time="192.5" type="appl" />
         <tli id="T378" time="193.0" type="appl" />
         <tli id="T379" time="193.5" type="appl" />
         <tli id="T380" time="194.0" type="appl" />
         <tli id="T381" time="194.5" type="appl" />
         <tli id="T382" time="195.0" type="appl" />
         <tli id="T383" time="195.5" type="appl" />
         <tli id="T384" time="196.0" type="appl" />
         <tli id="T385" time="196.5" type="appl" />
         <tli id="T386" time="197.0" type="appl" />
         <tli id="T387" time="197.5" type="appl" />
         <tli id="T388" time="198.0" type="appl" />
         <tli id="T389" time="198.5" type="appl" />
         <tli id="T390" time="199.0" type="appl" />
         <tli id="T391" time="199.5" type="appl" />
         <tli id="T392" time="200.0" type="appl" />
         <tli id="T393" time="200.5" type="appl" />
         <tli id="T394" time="201.0" type="appl" />
         <tli id="T395" time="201.5" type="appl" />
         <tli id="T396" time="202.0" type="appl" />
         <tli id="T397" time="202.5" type="appl" />
         <tli id="T398" time="203.0" type="appl" />
         <tli id="T399" time="203.5" type="appl" />
         <tli id="T400" time="204.0" type="appl" />
         <tli id="T401" time="204.5" type="appl" />
         <tli id="T402" time="205.0" type="appl" />
         <tli id="T403" time="205.5" type="appl" />
         <tli id="T404" time="206.0" type="appl" />
         <tli id="T405" time="206.5" type="appl" />
         <tli id="T406" time="207.0" type="appl" />
         <tli id="T407" time="207.5" type="appl" />
         <tli id="T408" time="208.0" type="appl" />
         <tli id="T409" time="208.5" type="appl" />
         <tli id="T410" time="209.0" type="appl" />
         <tli id="T411" time="209.5" type="appl" />
         <tli id="T412" time="210.0" type="appl" />
         <tli id="T413" time="210.5" type="appl" />
         <tli id="T414" time="211.0" type="appl" />
         <tli id="T415" time="211.5" type="appl" />
         <tli id="T416" time="212.0" type="appl" />
         <tli id="T417" time="212.5" type="appl" />
         <tli id="T418" time="213.0" type="appl" />
         <tli id="T419" time="213.5" type="appl" />
         <tli id="T420" time="214.0" type="appl" />
         <tli id="T421" time="214.5" type="appl" />
         <tli id="T422" time="215.0" type="appl" />
         <tli id="T423" time="215.5" type="appl" />
         <tli id="T424" time="216.0" type="appl" />
         <tli id="T425" time="216.5" type="appl" />
         <tli id="T426" time="217.0" type="appl" />
         <tli id="T427" time="217.5" type="appl" />
         <tli id="T428" time="218.0" type="appl" />
         <tli id="T429" time="218.5" type="appl" />
         <tli id="T430" time="219.0" type="appl" />
         <tli id="T431" time="219.5" type="appl" />
         <tli id="T432" time="220.0" type="appl" />
         <tli id="T433" time="220.5" type="appl" />
         <tli id="T434" time="221.0" type="appl" />
         <tli id="T435" time="221.5" type="appl" />
         <tli id="T436" time="222.0" type="appl" />
         <tli id="T437" time="222.5" type="appl" />
         <tli id="T438" time="223.0" type="appl" />
         <tli id="T439" time="223.5" type="appl" />
         <tli id="T440" time="224.0" type="appl" />
         <tli id="T441" time="224.5" type="appl" />
         <tli id="T442" time="225.0" type="appl" />
         <tli id="T443" time="225.5" type="appl" />
         <tli id="T444" time="226.0" type="appl" />
         <tli id="T445" time="226.5" type="appl" />
         <tli id="T446" time="227.0" type="appl" />
         <tli id="T447" time="227.5" type="appl" />
         <tli id="T448" time="228.0" type="appl" />
         <tli id="T449" time="228.5" type="appl" />
         <tli id="T450" time="229.0" type="appl" />
         <tli id="T451" time="229.5" type="appl" />
         <tli id="T452" time="230.0" type="appl" />
         <tli id="T453" time="230.5" type="appl" />
         <tli id="T454" time="231.0" type="appl" />
         <tli id="T455" time="231.5" type="appl" />
         <tli id="T456" time="232.0" type="appl" />
         <tli id="T457" time="232.5" type="appl" />
         <tli id="T458" time="233.0" type="appl" />
         <tli id="T459" time="233.5" type="appl" />
         <tli id="T460" time="234.0" type="appl" />
         <tli id="T461" time="234.5" type="appl" />
         <tli id="T462" time="235.0" type="appl" />
         <tli id="T463" time="235.5" type="appl" />
         <tli id="T464" time="236.0" type="appl" />
         <tli id="T465" time="236.5" type="appl" />
         <tli id="T466" time="237.0" type="appl" />
         <tli id="T467" time="237.5" type="appl" />
         <tli id="T468" time="238.0" type="appl" />
         <tli id="T469" time="238.5" type="appl" />
         <tli id="T470" time="239.0" type="appl" />
         <tli id="T471" time="239.5" type="appl" />
         <tli id="T472" time="240.0" type="appl" />
         <tli id="T473" time="240.5" type="appl" />
         <tli id="T474" time="241.0" type="appl" />
         <tli id="T475" time="241.5" type="appl" />
         <tli id="T476" time="242.0" type="appl" />
         <tli id="T477" time="242.5" type="appl" />
         <tli id="T478" time="243.0" type="appl" />
         <tli id="T479" time="243.5" type="appl" />
         <tli id="T480" time="244.0" type="appl" />
         <tli id="T481" time="244.5" type="appl" />
         <tli id="T482" time="245.0" type="appl" />
         <tli id="T483" time="245.5" type="appl" />
         <tli id="T484" time="246.0" type="appl" />
         <tli id="T485" time="246.5" type="appl" />
         <tli id="T486" time="247.0" type="appl" />
         <tli id="T487" time="247.5" type="appl" />
         <tli id="T488" time="248.0" type="appl" />
         <tli id="T489" time="248.5" type="appl" />
         <tli id="T490" time="249.0" type="appl" />
         <tli id="T491" time="249.5" type="appl" />
         <tli id="T492" time="250.0" type="appl" />
         <tli id="T493" time="250.5" type="appl" />
         <tli id="T494" time="251.0" type="appl" />
         <tli id="T495" time="251.5" type="appl" />
         <tli id="T496" time="252.0" type="appl" />
         <tli id="T497" time="252.5" type="appl" />
         <tli id="T498" time="253.0" type="appl" />
         <tli id="T499" time="253.5" type="appl" />
         <tli id="T500" time="254.0" type="appl" />
         <tli id="T501" time="254.5" type="appl" />
         <tli id="T502" time="255.0" type="appl" />
         <tli id="T503" time="255.5" type="appl" />
         <tli id="T504" time="256.0" type="appl" />
         <tli id="T505" time="256.5" type="appl" />
         <tli id="T506" time="257.0" type="appl" />
         <tli id="T507" time="257.5" type="appl" />
         <tli id="T508" time="258.0" type="appl" />
         <tli id="T509" time="258.5" type="appl" />
         <tli id="T510" time="259.0" type="appl" />
         <tli id="T511" time="259.5" type="appl" />
         <tli id="T512" time="260.0" type="appl" />
         <tli id="T513" time="260.5" type="appl" />
         <tli id="T514" time="261.0" type="appl" />
         <tli id="T515" time="261.5" type="appl" />
         <tli id="T516" time="262.0" type="appl" />
         <tli id="T517" time="262.5" type="appl" />
         <tli id="T518" time="263.0" type="appl" />
         <tli id="T519" time="263.5" type="appl" />
         <tli id="T520" time="264.0" type="appl" />
         <tli id="T521" time="264.5" type="appl" />
         <tli id="T522" time="265.0" type="appl" />
         <tli id="T523" time="265.5" type="appl" />
         <tli id="T524" time="266.0" type="appl" />
         <tli id="T525" time="266.5" type="appl" />
         <tli id="T526" time="267.0" type="appl" />
         <tli id="T527" time="267.5" type="appl" />
         <tli id="T528" time="268.0" type="appl" />
         <tli id="T529" time="268.5" type="appl" />
         <tli id="T530" time="269.0" type="appl" />
         <tli id="T531" time="269.5" type="appl" />
         <tli id="T532" time="270.0" type="appl" />
         <tli id="T533" time="270.5" type="appl" />
         <tli id="T534" time="271.0" type="appl" />
         <tli id="T535" time="271.5" type="appl" />
         <tli id="T536" time="272.0" type="appl" />
         <tli id="T537" time="272.5" type="appl" />
         <tli id="T538" time="273.0" type="appl" />
         <tli id="T539" time="273.5" type="appl" />
         <tli id="T540" time="274.0" type="appl" />
         <tli id="T541" time="274.5" type="appl" />
         <tli id="T542" time="275.0" type="appl" />
         <tli id="T543" time="275.5" type="appl" />
         <tli id="T544" time="276.0" type="appl" />
         <tli id="T545" time="276.5" type="appl" />
         <tli id="T546" time="277.0" type="appl" />
         <tli id="T547" time="277.5" type="appl" />
         <tli id="T548" time="278.0" type="appl" />
         <tli id="T549" time="278.5" type="appl" />
         <tli id="T550" time="279.0" type="appl" />
         <tli id="T551" time="279.5" type="appl" />
         <tli id="T552" time="280.0" type="appl" />
         <tli id="T553" time="280.5" type="appl" />
         <tli id="T554" time="281.0" type="appl" />
         <tli id="T555" time="281.5" type="appl" />
         <tli id="T556" time="282.0" type="appl" />
         <tli id="T557" time="282.5" type="appl" />
         <tli id="T558" time="283.0" type="appl" />
         <tli id="T559" time="283.5" type="appl" />
         <tli id="T560" time="284.0" type="appl" />
         <tli id="T561" time="284.5" type="appl" />
         <tli id="T562" time="285.0" type="appl" />
         <tli id="T563" time="285.5" type="appl" />
         <tli id="T564" time="286.0" type="appl" />
         <tli id="T565" time="286.5" type="appl" />
         <tli id="T566" time="287.0" type="appl" />
         <tli id="T567" time="287.5" type="appl" />
         <tli id="T568" time="288.0" type="appl" />
         <tli id="T569" time="288.5" type="appl" />
         <tli id="T570" time="289.0" type="appl" />
         <tli id="T571" time="289.5" type="appl" />
         <tli id="T572" time="290.0" type="appl" />
         <tli id="T573" time="290.5" type="appl" />
         <tli id="T574" time="291.0" type="appl" />
         <tli id="T575" time="291.5" type="appl" />
         <tli id="T576" time="292.0" type="appl" />
         <tli id="T577" time="292.5" type="appl" />
         <tli id="T578" time="293.0" type="appl" />
         <tli id="T579" time="293.5" type="appl" />
         <tli id="T580" time="294.0" type="appl" />
         <tli id="T581" time="294.5" type="appl" />
         <tli id="T582" time="295.0" type="appl" />
         <tli id="T583" time="295.5" type="appl" />
         <tli id="T584" time="296.0" type="appl" />
         <tli id="T585" time="296.5" type="appl" />
         <tli id="T586" time="297.0" type="appl" />
         <tli id="T587" time="297.5" type="appl" />
         <tli id="T588" time="298.0" type="appl" />
         <tli id="T589" time="298.5" type="appl" />
         <tli id="T590" time="299.0" type="appl" />
         <tli id="T591" time="299.5" type="appl" />
         <tli id="T592" time="300.0" type="appl" />
         <tli id="T593" time="300.5" type="appl" />
         <tli id="T594" time="301.0" type="appl" />
         <tli id="T595" time="301.5" type="appl" />
         <tli id="T596" time="302.0" type="appl" />
         <tli id="T597" time="302.5" type="appl" />
         <tli id="T598" time="303.0" type="appl" />
         <tli id="T599" time="303.5" type="appl" />
         <tli id="T600" time="304.0" type="appl" />
         <tli id="T601" time="304.5" type="appl" />
         <tli id="T602" time="305.0" type="appl" />
         <tli id="T603" time="305.5" type="appl" />
         <tli id="T604" time="306.0" type="appl" />
         <tli id="T605" time="306.5" type="appl" />
         <tli id="T606" time="307.0" type="appl" />
         <tli id="T607" time="307.5" type="appl" />
         <tli id="T608" time="308.0" type="appl" />
         <tli id="T609" time="308.5" type="appl" />
         <tli id="T610" time="309.0" type="appl" />
         <tli id="T611" time="309.5" type="appl" />
         <tli id="T612" time="310.0" type="appl" />
         <tli id="T613" time="310.5" type="appl" />
         <tli id="T614" time="311.0" type="appl" />
         <tli id="T615" time="311.5" type="appl" />
         <tli id="T616" time="312.0" type="appl" />
         <tli id="T617" time="312.5" type="appl" />
         <tli id="T618" time="313.0" type="appl" />
         <tli id="T619" time="313.5" type="appl" />
         <tli id="T620" time="314.0" type="appl" />
         <tli id="T621" time="314.5" type="appl" />
         <tli id="T622" time="315.0" type="appl" />
         <tli id="T623" time="315.5" type="appl" />
         <tli id="T624" time="316.0" type="appl" />
         <tli id="T625" time="316.5" type="appl" />
         <tli id="T626" time="317.0" type="appl" />
         <tli id="T627" time="317.5" type="appl" />
         <tli id="T628" time="318.0" type="appl" />
         <tli id="T629" time="318.5" type="appl" />
         <tli id="T630" time="319.0" type="appl" />
         <tli id="T631" time="319.5" type="appl" />
         <tli id="T632" time="320.0" type="appl" />
         <tli id="T633" time="320.5" type="appl" />
         <tli id="T634" time="321.0" type="appl" />
         <tli id="T635" time="321.5" type="appl" />
         <tli id="T636" time="322.0" type="appl" />
         <tli id="T637" time="322.5" type="appl" />
         <tli id="T638" time="323.0" type="appl" />
         <tli id="T639" time="323.5" type="appl" />
         <tli id="T640" time="324.0" type="appl" />
         <tli id="T641" time="324.5" type="appl" />
         <tli id="T642" time="325.0" type="appl" />
         <tli id="T643" time="325.5" type="appl" />
         <tli id="T644" time="326.0" type="appl" />
         <tli id="T645" time="326.5" type="appl" />
         <tli id="T646" time="327.0" type="appl" />
         <tli id="T647" time="327.5" type="appl" />
         <tli id="T648" time="328.0" type="appl" />
         <tli id="T649" time="328.5" type="appl" />
         <tli id="T650" time="329.0" type="appl" />
         <tli id="T651" time="329.5" type="appl" />
         <tli id="T652" time="330.0" type="appl" />
         <tli id="T653" time="330.5" type="appl" />
         <tli id="T654" time="331.0" type="appl" />
         <tli id="T655" time="331.5" type="appl" />
         <tli id="T656" time="332.0" type="appl" />
         <tli id="T657" time="332.5" type="appl" />
         <tli id="T658" time="333.0" type="appl" />
         <tli id="T659" time="333.5" type="appl" />
         <tli id="T660" time="334.0" type="appl" />
         <tli id="T661" time="334.5" type="appl" />
         <tli id="T662" time="335.0" type="appl" />
         <tli id="T663" time="335.5" type="appl" />
         <tli id="T664" time="336.0" type="appl" />
         <tli id="T665" time="336.5" type="appl" />
         <tli id="T666" time="337.0" type="appl" />
         <tli id="T667" time="337.5" type="appl" />
         <tli id="T668" time="338.0" type="appl" />
         <tli id="T669" time="338.5" type="appl" />
         <tli id="T670" time="339.0" type="appl" />
         <tli id="T671" time="339.5" type="appl" />
         <tli id="T672" time="340.0" type="appl" />
         <tli id="T673" time="340.5" type="appl" />
         <tli id="T674" time="341.0" type="appl" />
         <tli id="T675" time="341.5" type="appl" />
         <tli id="T676" time="342.0" type="appl" />
         <tli id="T677" time="342.5" type="appl" />
         <tli id="T678" time="343.0" type="appl" />
         <tli id="T679" time="343.5" type="appl" />
         <tli id="T680" time="344.0" type="appl" />
         <tli id="T681" time="344.5" type="appl" />
         <tli id="T682" time="345.0" type="appl" />
         <tli id="T683" time="345.5" type="appl" />
         <tli id="T684" time="346.0" type="appl" />
         <tli id="T685" time="346.5" type="appl" />
         <tli id="T686" time="347.0" type="appl" />
         <tli id="T687" time="347.5" type="appl" />
         <tli id="T688" time="348.0" type="appl" />
         <tli id="T689" time="348.5" type="appl" />
         <tli id="T690" time="349.0" type="appl" />
         <tli id="T691" time="349.5" type="appl" />
         <tli id="T692" time="350.0" type="appl" />
         <tli id="T693" time="350.5" type="appl" />
         <tli id="T694" time="351.0" type="appl" />
         <tli id="T695" time="351.5" type="appl" />
         <tli id="T696" time="352.0" type="appl" />
         <tli id="T697" time="352.5" type="appl" />
         <tli id="T698" time="353.0" type="appl" />
         <tli id="T699" time="353.5" type="appl" />
         <tli id="T700" time="354.0" type="appl" />
         <tli id="T701" time="354.5" type="appl" />
         <tli id="T702" time="355.0" type="appl" />
         <tli id="T703" time="355.5" type="appl" />
         <tli id="T704" time="356.0" type="appl" />
         <tli id="T705" time="356.5" type="appl" />
         <tli id="T706" time="357.0" type="appl" />
         <tli id="T707" time="357.5" type="appl" />
         <tli id="T708" time="358.0" type="appl" />
         <tli id="T709" time="358.5" type="appl" />
         <tli id="T710" time="359.0" type="appl" />
         <tli id="T711" time="359.5" type="appl" />
         <tli id="T712" time="360.0" type="appl" />
         <tli id="T713" time="360.5" type="appl" />
         <tli id="T714" time="361.0" type="appl" />
         <tli id="T715" time="361.5" type="appl" />
         <tli id="T716" time="362.0" type="appl" />
         <tli id="T717" time="362.5" type="appl" />
         <tli id="T718" time="363.0" type="appl" />
         <tli id="T719" time="363.5" type="appl" />
         <tli id="T720" time="364.0" type="appl" />
         <tli id="T721" time="364.5" type="appl" />
         <tli id="T722" time="365.0" type="appl" />
         <tli id="T723" time="365.5" type="appl" />
         <tli id="T724" time="366.0" type="appl" />
         <tli id="T725" time="366.5" type="appl" />
         <tli id="T726" time="367.0" type="appl" />
         <tli id="T727" time="367.5" type="appl" />
         <tli id="T728" time="368.0" type="appl" />
         <tli id="T729" time="368.5" type="appl" />
         <tli id="T730" time="369.0" type="appl" />
         <tli id="T731" time="369.5" type="appl" />
         <tli id="T732" time="370.0" type="appl" />
         <tli id="T733" time="370.5" type="appl" />
         <tli id="T734" time="371.0" type="appl" />
         <tli id="T735" time="371.5" type="appl" />
         <tli id="T736" time="372.0" type="appl" />
         <tli id="T737" time="372.5" type="appl" />
         <tli id="T738" time="373.0" type="appl" />
         <tli id="T739" time="373.5" type="appl" />
         <tli id="T740" time="374.0" type="appl" />
         <tli id="T741" time="374.5" type="appl" />
         <tli id="T742" time="375.0" type="appl" />
         <tli id="T743" time="375.5" type="appl" />
         <tli id="T744" time="376.0" type="appl" />
         <tli id="T745" time="376.5" type="appl" />
         <tli id="T746" time="377.0" type="appl" />
         <tli id="T747" time="377.5" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="ChPK"
                      type="t">
         <timeline-fork end="T489" start="T488">
            <tli id="T488.tx.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T747" id="Seg_0" n="sc" s="T0">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Bɨlɨr-bɨlɨr</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">biːr</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">ɨraːktaːgɨ</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">olorbut</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T8" id="Seg_17" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">Ogoto</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">hu͡ok</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">ete</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">ebit</ts>
                  <nts id="Seg_29" n="HIAT:ip">.</nts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T12" id="Seg_32" n="HIAT:u" s="T8">
                  <ts e="T9" id="Seg_34" n="HIAT:w" s="T8">Biːrde</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_37" n="HIAT:w" s="T9">bu</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_40" n="HIAT:w" s="T10">ɨraːktaːgɨ</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_43" n="HIAT:w" s="T11">hanaːbɨt</ts>
                  <nts id="Seg_44" n="HIAT:ip">:</nts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_47" n="HIAT:u" s="T12">
                  <nts id="Seg_48" n="HIAT:ip">"</nts>
                  <ts e="T13" id="Seg_50" n="HIAT:w" s="T12">Bu</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_53" n="HIAT:w" s="T13">togo</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_56" n="HIAT:w" s="T14">bihigi</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_59" n="HIAT:w" s="T15">ogoto</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_62" n="HIAT:w" s="T16">hu͡okputuj</ts>
                  <nts id="Seg_63" n="HIAT:ip">?</nts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_66" n="HIAT:u" s="T17">
                  <ts e="T18" id="Seg_68" n="HIAT:w" s="T17">Kɨrdʼagastartan</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_71" n="HIAT:w" s="T18">ɨjɨppɨt</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_74" n="HIAT:w" s="T19">kihi</ts>
                  <nts id="Seg_75" n="HIAT:ip">.</nts>
                  <nts id="Seg_76" n="HIAT:ip">"</nts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T25" id="Seg_79" n="HIAT:u" s="T20">
                  <ts e="T21" id="Seg_81" n="HIAT:w" s="T20">Bu</ts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_84" n="HIAT:w" s="T21">hanɨː</ts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_87" n="HIAT:w" s="T22">olordoguna</ts>
                  <nts id="Seg_88" n="HIAT:ip">,</nts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_91" n="HIAT:w" s="T23">dʼaktara</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_94" n="HIAT:w" s="T24">kiːrbit</ts>
                  <nts id="Seg_95" n="HIAT:ip">:</nts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T28" id="Seg_98" n="HIAT:u" s="T25">
                  <nts id="Seg_99" n="HIAT:ip">"</nts>
                  <ts e="T26" id="Seg_101" n="HIAT:w" s="T25">Tugu</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_104" n="HIAT:w" s="T26">hanaːrgaːtɨŋ</ts>
                  <nts id="Seg_105" n="HIAT:ip">,</nts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_108" n="HIAT:w" s="T27">dogoː</ts>
                  <nts id="Seg_109" n="HIAT:ip">?</nts>
                  <nts id="Seg_110" n="HIAT:ip">"</nts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T32" id="Seg_113" n="HIAT:u" s="T28">
                  <ts e="T29" id="Seg_115" n="HIAT:w" s="T28">Harsɨːn</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_118" n="HIAT:w" s="T29">bihigi</ts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_121" n="HIAT:w" s="T30">kihileri</ts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_124" n="HIAT:w" s="T31">komuju͡okput</ts>
                  <nts id="Seg_125" n="HIAT:ip">.</nts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T40" id="Seg_128" n="HIAT:u" s="T32">
                  <ts e="T33" id="Seg_130" n="HIAT:w" s="T32">Kanɨga</ts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_133" n="HIAT:w" s="T33">dʼi͡eri</ts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_136" n="HIAT:w" s="T34">iččitek</ts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_139" n="HIAT:w" s="T35">isti͡eneni</ts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_142" n="HIAT:w" s="T36">körön</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_145" n="HIAT:w" s="T37">oloru͡okputuj</ts>
                  <nts id="Seg_146" n="HIAT:ip">,</nts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_149" n="HIAT:w" s="T38">ogoto</ts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_152" n="HIAT:w" s="T39">hu͡ok</ts>
                  <nts id="Seg_153" n="HIAT:ip">?</nts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T44" id="Seg_156" n="HIAT:u" s="T40">
                  <ts e="T41" id="Seg_158" n="HIAT:w" s="T40">ɨjɨtɨ͡ak</ts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_161" n="HIAT:w" s="T41">togo</ts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_164" n="HIAT:w" s="T42">ogoto</ts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_167" n="HIAT:w" s="T43">hu͡okputuj</ts>
                  <nts id="Seg_168" n="HIAT:ip">?</nts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T48" id="Seg_171" n="HIAT:u" s="T44">
                  <ts e="T45" id="Seg_173" n="HIAT:w" s="T44">Kimi͡eke</ts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_176" n="HIAT:w" s="T45">baːjbɨtɨn</ts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_179" n="HIAT:w" s="T46">bi͡eri͡ekpitij</ts>
                  <nts id="Seg_180" n="HIAT:ip">,</nts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_183" n="HIAT:w" s="T47">öllökpütüne</ts>
                  <nts id="Seg_184" n="HIAT:ip">?</nts>
                  <nts id="Seg_185" n="HIAT:ip">"</nts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T54" id="Seg_188" n="HIAT:u" s="T48">
                  <ts e="T49" id="Seg_190" n="HIAT:w" s="T48">Harsi͡erda</ts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_193" n="HIAT:w" s="T49">turbuttara</ts>
                  <nts id="Seg_194" n="HIAT:ip">,</nts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_197" n="HIAT:w" s="T50">ügüːs</ts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_200" n="HIAT:w" s="T51">bagajɨ</ts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_203" n="HIAT:w" s="T52">kihi</ts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_206" n="HIAT:w" s="T53">komullubut</ts>
                  <nts id="Seg_207" n="HIAT:ip">.</nts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T57" id="Seg_210" n="HIAT:u" s="T54">
                  <ts e="T55" id="Seg_212" n="HIAT:w" s="T54">Onno</ts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_215" n="HIAT:w" s="T55">ɨraːktaːgɨ</ts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_218" n="HIAT:w" s="T56">ɨjɨppɨt</ts>
                  <nts id="Seg_219" n="HIAT:ip">:</nts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T62" id="Seg_222" n="HIAT:u" s="T57">
                  <nts id="Seg_223" n="HIAT:ip">"</nts>
                  <ts e="T58" id="Seg_225" n="HIAT:w" s="T57">Bu</ts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_228" n="HIAT:w" s="T58">togo</ts>
                  <nts id="Seg_229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_231" n="HIAT:w" s="T59">bihigi</ts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_234" n="HIAT:w" s="T60">ogoto</ts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_237" n="HIAT:w" s="T61">hu͡okputuj</ts>
                  <nts id="Seg_238" n="HIAT:ip">?</nts>
                  <nts id="Seg_239" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T70" id="Seg_241" n="HIAT:u" s="T62">
                  <ts e="T63" id="Seg_243" n="HIAT:w" s="T62">Kajtak</ts>
                  <nts id="Seg_244" n="HIAT:ip">,</nts>
                  <nts id="Seg_245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_247" n="HIAT:w" s="T63">tugu</ts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_250" n="HIAT:w" s="T64">gɨnnakka</ts>
                  <nts id="Seg_251" n="HIAT:ip">,</nts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_254" n="HIAT:w" s="T65">ogolonu͡okputuj</ts>
                  <nts id="Seg_255" n="HIAT:ip">,</nts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_258" n="HIAT:w" s="T66">ehigi</ts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_261" n="HIAT:w" s="T67">kajtak</ts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_264" n="HIAT:w" s="T68">dʼiː</ts>
                  <nts id="Seg_265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_267" n="HIAT:w" s="T69">hanɨːgɨt</ts>
                  <nts id="Seg_268" n="HIAT:ip">?</nts>
                  <nts id="Seg_269" n="HIAT:ip">"</nts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T73" id="Seg_272" n="HIAT:u" s="T70">
                  <ts e="T71" id="Seg_274" n="HIAT:w" s="T70">Kihiler</ts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_277" n="HIAT:w" s="T71">ajdaːja</ts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_280" n="HIAT:w" s="T72">tüspütter</ts>
                  <nts id="Seg_281" n="HIAT:ip">.</nts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T79" id="Seg_284" n="HIAT:u" s="T73">
                  <ts e="T74" id="Seg_286" n="HIAT:w" s="T73">Aːnɨnan</ts>
                  <nts id="Seg_287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_289" n="HIAT:w" s="T74">kɨrdʼagas</ts>
                  <nts id="Seg_290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_292" n="HIAT:w" s="T75">hokkor</ts>
                  <nts id="Seg_293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_295" n="HIAT:w" s="T76">ogonnʼor</ts>
                  <nts id="Seg_296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_298" n="HIAT:w" s="T77">bɨltas</ts>
                  <nts id="Seg_299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_301" n="HIAT:w" s="T78">gɨmmɨt</ts>
                  <nts id="Seg_302" n="HIAT:ip">.</nts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T84" id="Seg_305" n="HIAT:u" s="T79">
                  <ts e="T80" id="Seg_307" n="HIAT:w" s="T79">Hokkor</ts>
                  <nts id="Seg_308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_310" n="HIAT:w" s="T80">erejdeːgi</ts>
                  <nts id="Seg_311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_313" n="HIAT:w" s="T81">ortogo</ts>
                  <nts id="Seg_314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_316" n="HIAT:w" s="T82">üten</ts>
                  <nts id="Seg_317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_319" n="HIAT:w" s="T83">keːspitter</ts>
                  <nts id="Seg_320" n="HIAT:ip">:</nts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T88" id="Seg_323" n="HIAT:u" s="T84">
                  <nts id="Seg_324" n="HIAT:ip">"</nts>
                  <ts e="T85" id="Seg_326" n="HIAT:w" s="T84">Bu</ts>
                  <nts id="Seg_327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_329" n="HIAT:w" s="T85">baːr</ts>
                  <nts id="Seg_330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_332" n="HIAT:w" s="T86">biler</ts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_335" n="HIAT:w" s="T87">kihi</ts>
                  <nts id="Seg_336" n="HIAT:ip">.</nts>
                  <nts id="Seg_337" n="HIAT:ip">"</nts>
                  <nts id="Seg_338" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T96" id="Seg_340" n="HIAT:u" s="T88">
                  <ts e="T89" id="Seg_342" n="HIAT:w" s="T88">Ogonnʼor</ts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_345" n="HIAT:w" s="T89">örö</ts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_348" n="HIAT:w" s="T90">körön</ts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_351" n="HIAT:w" s="T91">baraːn</ts>
                  <nts id="Seg_352" n="HIAT:ip">,</nts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_355" n="HIAT:w" s="T92">his</ts>
                  <nts id="Seg_356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_358" n="HIAT:w" s="T93">tuttan</ts>
                  <nts id="Seg_359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_361" n="HIAT:w" s="T94">baraːn</ts>
                  <nts id="Seg_362" n="HIAT:ip">,</nts>
                  <nts id="Seg_363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_365" n="HIAT:w" s="T95">dʼi͡ebit</ts>
                  <nts id="Seg_366" n="HIAT:ip">:</nts>
                  <nts id="Seg_367" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T103" id="Seg_369" n="HIAT:u" s="T96">
                  <nts id="Seg_370" n="HIAT:ip">"</nts>
                  <ts e="T97" id="Seg_372" n="HIAT:w" s="T96">Oŋoruŋ</ts>
                  <nts id="Seg_373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_375" n="HIAT:w" s="T97">kömüs</ts>
                  <nts id="Seg_376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_378" n="HIAT:w" s="T98">ilimne</ts>
                  <nts id="Seg_379" n="HIAT:ip">,</nts>
                  <nts id="Seg_380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_382" n="HIAT:w" s="T99">kömüs</ts>
                  <nts id="Seg_383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_385" n="HIAT:w" s="T100">tɨːta</ts>
                  <nts id="Seg_386" n="HIAT:ip">,</nts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_389" n="HIAT:w" s="T101">kömüs</ts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_392" n="HIAT:w" s="T102">erdiːte</ts>
                  <nts id="Seg_393" n="HIAT:ip">.</nts>
                  <nts id="Seg_394" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T107" id="Seg_396" n="HIAT:u" s="T103">
                  <ts e="T104" id="Seg_398" n="HIAT:w" s="T103">Onton</ts>
                  <nts id="Seg_399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_401" n="HIAT:w" s="T104">ɨraːktaːgɨ</ts>
                  <nts id="Seg_402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_404" n="HIAT:w" s="T105">balɨktɨː</ts>
                  <nts id="Seg_405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_407" n="HIAT:w" s="T106">bardɨn</ts>
                  <nts id="Seg_408" n="HIAT:ip">.</nts>
                  <nts id="Seg_409" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T113" id="Seg_411" n="HIAT:u" s="T107">
                  <ts e="T108" id="Seg_413" n="HIAT:w" s="T107">Ilimiger</ts>
                  <nts id="Seg_414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_416" n="HIAT:w" s="T108">balɨk</ts>
                  <nts id="Seg_417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_419" n="HIAT:w" s="T109">tutu͡o</ts>
                  <nts id="Seg_420" n="HIAT:ip">,</nts>
                  <nts id="Seg_421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_423" n="HIAT:w" s="T110">onu</ts>
                  <nts id="Seg_424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_426" n="HIAT:w" s="T111">dʼaktara</ts>
                  <nts id="Seg_427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_429" n="HIAT:w" s="T112">hi͡etin</ts>
                  <nts id="Seg_430" n="HIAT:ip">.</nts>
                  <nts id="Seg_431" n="HIAT:ip">"</nts>
                  <nts id="Seg_432" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T116" id="Seg_434" n="HIAT:u" s="T113">
                  <ts e="T114" id="Seg_436" n="HIAT:w" s="T113">ɨraːktaːgɨ</ts>
                  <nts id="Seg_437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_439" n="HIAT:w" s="T114">dʼaktara</ts>
                  <nts id="Seg_440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_442" n="HIAT:w" s="T115">külbüt</ts>
                  <nts id="Seg_443" n="HIAT:ip">:</nts>
                  <nts id="Seg_444" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T120" id="Seg_446" n="HIAT:u" s="T116">
                  <nts id="Seg_447" n="HIAT:ip">"</nts>
                  <ts e="T117" id="Seg_449" n="HIAT:w" s="T116">Onton</ts>
                  <nts id="Seg_450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_452" n="HIAT:w" s="T117">min</ts>
                  <nts id="Seg_453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_455" n="HIAT:w" s="T118">totu͡om</ts>
                  <nts id="Seg_456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_458" n="HIAT:w" s="T119">du͡o</ts>
                  <nts id="Seg_459" n="HIAT:ip">?</nts>
                  <nts id="Seg_460" n="HIAT:ip">"</nts>
                  <nts id="Seg_461" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T130" id="Seg_463" n="HIAT:u" s="T120">
                  <ts e="T121" id="Seg_465" n="HIAT:w" s="T120">Ör</ts>
                  <nts id="Seg_466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_468" n="HIAT:w" s="T121">bu͡olu͡oj</ts>
                  <nts id="Seg_469" n="HIAT:ip">,</nts>
                  <nts id="Seg_470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_472" n="HIAT:w" s="T122">ɨraːktaːgɨ</ts>
                  <nts id="Seg_473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_475" n="HIAT:w" s="T123">harsɨŋŋɨtɨn</ts>
                  <nts id="Seg_476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_478" n="HIAT:w" s="T124">kömüs</ts>
                  <nts id="Seg_479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_481" n="HIAT:w" s="T125">tɨːlaːk</ts>
                  <nts id="Seg_482" n="HIAT:ip">,</nts>
                  <nts id="Seg_483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_485" n="HIAT:w" s="T126">kömüs</ts>
                  <nts id="Seg_486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_488" n="HIAT:w" s="T127">erdiːleːk</ts>
                  <nts id="Seg_489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_491" n="HIAT:w" s="T128">balɨktana</ts>
                  <nts id="Seg_492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_494" n="HIAT:w" s="T129">barbɨt</ts>
                  <nts id="Seg_495" n="HIAT:ip">.</nts>
                  <nts id="Seg_496" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T136" id="Seg_498" n="HIAT:u" s="T130">
                  <ts e="T131" id="Seg_500" n="HIAT:w" s="T130">Biːr</ts>
                  <nts id="Seg_501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_503" n="HIAT:w" s="T131">balɨgɨ</ts>
                  <nts id="Seg_504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_506" n="HIAT:w" s="T132">ilibireten</ts>
                  <nts id="Seg_507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_509" n="HIAT:w" s="T133">tɨːnnaːktɨː</ts>
                  <nts id="Seg_510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_512" n="HIAT:w" s="T134">egelbet</ts>
                  <nts id="Seg_513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_515" n="HIAT:w" s="T135">duː</ts>
                  <nts id="Seg_516" n="HIAT:ip">.</nts>
                  <nts id="Seg_517" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T143" id="Seg_519" n="HIAT:u" s="T136">
                  <nts id="Seg_520" n="HIAT:ip">"</nts>
                  <ts e="T137" id="Seg_522" n="HIAT:w" s="T136">Bu</ts>
                  <nts id="Seg_523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_525" n="HIAT:w" s="T137">bultum</ts>
                  <nts id="Seg_526" n="HIAT:ip">"</nts>
                  <nts id="Seg_527" n="HIAT:ip">,</nts>
                  <nts id="Seg_528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_530" n="HIAT:w" s="T138">dʼi͡ebit</ts>
                  <nts id="Seg_531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_533" n="HIAT:w" s="T139">da</ts>
                  <nts id="Seg_534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_536" n="HIAT:w" s="T140">asčɨttarɨgar</ts>
                  <nts id="Seg_537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_539" n="HIAT:w" s="T141">bi͡eren</ts>
                  <nts id="Seg_540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_542" n="HIAT:w" s="T142">kebispit</ts>
                  <nts id="Seg_543" n="HIAT:ip">.</nts>
                  <nts id="Seg_544" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T145" id="Seg_546" n="HIAT:u" s="T143">
                  <nts id="Seg_547" n="HIAT:ip">"</nts>
                  <ts e="T144" id="Seg_549" n="HIAT:w" s="T143">Hanɨkaːn</ts>
                  <nts id="Seg_550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_552" n="HIAT:w" s="T144">kü͡östeːŋ</ts>
                  <nts id="Seg_553" n="HIAT:ip">!</nts>
                  <nts id="Seg_554" n="HIAT:ip">"</nts>
                  <nts id="Seg_555" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T157" id="Seg_557" n="HIAT:u" s="T145">
                  <ts e="T146" id="Seg_559" n="HIAT:w" s="T145">Asčɨt</ts>
                  <nts id="Seg_560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_562" n="HIAT:w" s="T146">balɨk</ts>
                  <nts id="Seg_563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_565" n="HIAT:w" s="T147">katɨrɨgɨn</ts>
                  <nts id="Seg_566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_568" n="HIAT:w" s="T148">ot</ts>
                  <nts id="Seg_569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_571" n="HIAT:w" s="T149">ihiger</ts>
                  <nts id="Seg_572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_574" n="HIAT:w" s="T150">ɨraːstaːn</ts>
                  <nts id="Seg_575" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_577" n="HIAT:w" s="T151">keːspit</ts>
                  <nts id="Seg_578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_579" n="HIAT:ip">—</nts>
                  <nts id="Seg_580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_582" n="HIAT:w" s="T152">onu</ts>
                  <nts id="Seg_583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_585" n="HIAT:w" s="T153">ɨraːktaːgɨ</ts>
                  <nts id="Seg_586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_588" n="HIAT:w" s="T154">koru͡obata</ts>
                  <nts id="Seg_589" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_591" n="HIAT:w" s="T155">hi͡en</ts>
                  <nts id="Seg_592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_594" n="HIAT:w" s="T156">keːspit</ts>
                  <nts id="Seg_595" n="HIAT:ip">.</nts>
                  <nts id="Seg_596" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T168" id="Seg_598" n="HIAT:u" s="T157">
                  <ts e="T158" id="Seg_600" n="HIAT:w" s="T157">ɨraːktaːgɨ</ts>
                  <nts id="Seg_601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_603" n="HIAT:w" s="T158">dʼaktara</ts>
                  <nts id="Seg_604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_606" n="HIAT:w" s="T159">balɨgɨ</ts>
                  <nts id="Seg_607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_609" n="HIAT:w" s="T160">hi͡en</ts>
                  <nts id="Seg_610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_612" n="HIAT:w" s="T161">baran</ts>
                  <nts id="Seg_613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_615" n="HIAT:w" s="T162">oŋu͡ogun</ts>
                  <nts id="Seg_616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_618" n="HIAT:w" s="T163">teri͡elkege</ts>
                  <nts id="Seg_619" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_621" n="HIAT:w" s="T164">kaːllarbɨtɨn</ts>
                  <nts id="Seg_622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_624" n="HIAT:w" s="T165">asčɨt</ts>
                  <nts id="Seg_625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_627" n="HIAT:w" s="T166">ɨkka</ts>
                  <nts id="Seg_628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_630" n="HIAT:w" s="T167">bɨrakpɨt</ts>
                  <nts id="Seg_631" n="HIAT:ip">.</nts>
                  <nts id="Seg_632" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T172" id="Seg_634" n="HIAT:u" s="T168">
                  <ts e="T169" id="Seg_636" n="HIAT:w" s="T168">Oloŋkogo</ts>
                  <nts id="Seg_637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_639" n="HIAT:w" s="T169">ör</ts>
                  <nts id="Seg_640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_642" n="HIAT:w" s="T170">bu͡olu͡o</ts>
                  <nts id="Seg_643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_645" n="HIAT:w" s="T171">du͡o</ts>
                  <nts id="Seg_646" n="HIAT:ip">?</nts>
                  <nts id="Seg_647" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T176" id="Seg_649" n="HIAT:u" s="T172">
                  <ts e="T173" id="Seg_651" n="HIAT:w" s="T172">Ühü͡önnere</ts>
                  <nts id="Seg_652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_654" n="HIAT:w" s="T173">üs</ts>
                  <nts id="Seg_655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_657" n="HIAT:w" s="T174">u͡olu</ts>
                  <nts id="Seg_658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_660" n="HIAT:w" s="T175">töröːbütter</ts>
                  <nts id="Seg_661" n="HIAT:ip">.</nts>
                  <nts id="Seg_662" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T183" id="Seg_664" n="HIAT:u" s="T176">
                  <ts e="T177" id="Seg_666" n="HIAT:w" s="T176">Ogolor</ts>
                  <nts id="Seg_667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_669" n="HIAT:w" s="T177">töhö</ts>
                  <nts id="Seg_670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_672" n="HIAT:w" s="T178">da</ts>
                  <nts id="Seg_673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_675" n="HIAT:w" s="T179">bu͡olbakka</ts>
                  <nts id="Seg_676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_678" n="HIAT:w" s="T180">biːrge</ts>
                  <nts id="Seg_679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_681" n="HIAT:w" s="T181">oːnnʼuːr</ts>
                  <nts id="Seg_682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_684" n="HIAT:w" s="T182">bu͡olbuttar</ts>
                  <nts id="Seg_685" n="HIAT:ip">.</nts>
                  <nts id="Seg_686" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T188" id="Seg_688" n="HIAT:u" s="T183">
                  <ts e="T184" id="Seg_690" n="HIAT:w" s="T183">Biːrde</ts>
                  <nts id="Seg_691" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_693" n="HIAT:w" s="T184">araj</ts>
                  <nts id="Seg_694" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_696" n="HIAT:w" s="T185">ɨraːktaːgɨ</ts>
                  <nts id="Seg_697" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_699" n="HIAT:w" s="T186">u͡ola</ts>
                  <nts id="Seg_700" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_702" n="HIAT:w" s="T187">dʼi͡ebit</ts>
                  <nts id="Seg_703" n="HIAT:ip">:</nts>
                  <nts id="Seg_704" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T191" id="Seg_706" n="HIAT:u" s="T188">
                  <nts id="Seg_707" n="HIAT:ip">"</nts>
                  <ts e="T189" id="Seg_709" n="HIAT:w" s="T188">Dogottor</ts>
                  <nts id="Seg_710" n="HIAT:ip">,</nts>
                  <nts id="Seg_711" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_713" n="HIAT:w" s="T189">tɨːlana</ts>
                  <nts id="Seg_714" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_716" n="HIAT:w" s="T190">barɨ͡agɨŋ</ts>
                  <nts id="Seg_717" n="HIAT:ip">.</nts>
                  <nts id="Seg_718" n="HIAT:ip">"</nts>
                  <nts id="Seg_719" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T197" id="Seg_721" n="HIAT:u" s="T191">
                  <ts e="T192" id="Seg_723" n="HIAT:w" s="T191">U͡olattar</ts>
                  <nts id="Seg_724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_726" n="HIAT:w" s="T192">töhö</ts>
                  <nts id="Seg_727" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_729" n="HIAT:w" s="T193">ör</ts>
                  <nts id="Seg_730" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_732" n="HIAT:w" s="T194">erden</ts>
                  <nts id="Seg_733" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_735" n="HIAT:w" s="T195">ispittere</ts>
                  <nts id="Seg_736" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_738" n="HIAT:w" s="T196">bu͡olla</ts>
                  <nts id="Seg_739" n="HIAT:ip">.</nts>
                  <nts id="Seg_740" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T205" id="Seg_742" n="HIAT:u" s="T197">
                  <ts e="T198" id="Seg_744" n="HIAT:w" s="T197">Körbüttere</ts>
                  <nts id="Seg_745" n="HIAT:ip">,</nts>
                  <nts id="Seg_746" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_748" n="HIAT:w" s="T198">ebe</ts>
                  <nts id="Seg_749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_751" n="HIAT:w" s="T199">onu͡or</ts>
                  <nts id="Seg_752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_754" n="HIAT:w" s="T200">ulakan</ts>
                  <nts id="Seg_755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_757" n="HIAT:w" s="T201">bagajɨ</ts>
                  <nts id="Seg_758" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_760" n="HIAT:w" s="T202">balagan</ts>
                  <nts id="Seg_761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_763" n="HIAT:w" s="T203">dʼi͡e</ts>
                  <nts id="Seg_764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_766" n="HIAT:w" s="T204">turar</ts>
                  <nts id="Seg_767" n="HIAT:ip">.</nts>
                  <nts id="Seg_768" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T208" id="Seg_770" n="HIAT:u" s="T205">
                  <nts id="Seg_771" n="HIAT:ip">"</nts>
                  <ts e="T206" id="Seg_773" n="HIAT:w" s="T205">Iti</ts>
                  <nts id="Seg_774" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_776" n="HIAT:w" s="T206">tu͡okputuj</ts>
                  <nts id="Seg_777" n="HIAT:ip">"</nts>
                  <nts id="Seg_778" n="HIAT:ip">,</nts>
                  <nts id="Seg_779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_781" n="HIAT:w" s="T207">despitter</ts>
                  <nts id="Seg_782" n="HIAT:ip">.</nts>
                  <nts id="Seg_783" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T210" id="Seg_785" n="HIAT:u" s="T208">
                  <ts e="T209" id="Seg_787" n="HIAT:w" s="T208">ɨraːktaːgɨ</ts>
                  <nts id="Seg_788" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_790" n="HIAT:w" s="T209">u͡ola</ts>
                  <nts id="Seg_791" n="HIAT:ip">:</nts>
                  <nts id="Seg_792" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T213" id="Seg_794" n="HIAT:u" s="T210">
                  <nts id="Seg_795" n="HIAT:ip">"</nts>
                  <ts e="T211" id="Seg_797" n="HIAT:w" s="T210">Tiksi͡egiŋ</ts>
                  <nts id="Seg_798" n="HIAT:ip">,</nts>
                  <nts id="Seg_799" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_801" n="HIAT:w" s="T211">kaːrtɨlɨ͡akpɨt</ts>
                  <nts id="Seg_802" n="HIAT:ip">"</nts>
                  <nts id="Seg_803" n="HIAT:ip">,</nts>
                  <nts id="Seg_804" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_806" n="HIAT:w" s="T212">dʼi͡ebit</ts>
                  <nts id="Seg_807" n="HIAT:ip">.</nts>
                  <nts id="Seg_808" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T215" id="Seg_810" n="HIAT:u" s="T213">
                  <ts e="T214" id="Seg_812" n="HIAT:w" s="T213">ɨt</ts>
                  <nts id="Seg_813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_815" n="HIAT:w" s="T214">u͡ola</ts>
                  <nts id="Seg_816" n="HIAT:ip">:</nts>
                  <nts id="Seg_817" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T217" id="Seg_819" n="HIAT:u" s="T215">
                  <nts id="Seg_820" n="HIAT:ip">"</nts>
                  <ts e="T216" id="Seg_822" n="HIAT:w" s="T215">E</ts>
                  <nts id="Seg_823" n="HIAT:ip">,</nts>
                  <nts id="Seg_824" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_826" n="HIAT:w" s="T216">togo</ts>
                  <nts id="Seg_827" n="HIAT:ip">?</nts>
                  <nts id="Seg_828" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T221" id="Seg_830" n="HIAT:u" s="T217">
                  <ts e="T218" id="Seg_832" n="HIAT:w" s="T217">Bagar</ts>
                  <nts id="Seg_833" n="HIAT:ip">,</nts>
                  <nts id="Seg_834" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_836" n="HIAT:w" s="T218">abaːhɨlar</ts>
                  <nts id="Seg_837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_839" n="HIAT:w" s="T219">dʼi͡elere</ts>
                  <nts id="Seg_840" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_842" n="HIAT:w" s="T220">bu͡olu͡o</ts>
                  <nts id="Seg_843" n="HIAT:ip">?</nts>
                  <nts id="Seg_844" n="HIAT:ip">"</nts>
                  <nts id="Seg_845" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T223" id="Seg_847" n="HIAT:u" s="T221">
                  <ts e="T222" id="Seg_849" n="HIAT:w" s="T221">Koru͡oba</ts>
                  <nts id="Seg_850" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_852" n="HIAT:w" s="T222">u͡ola</ts>
                  <nts id="Seg_853" n="HIAT:ip">:</nts>
                  <nts id="Seg_854" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T225" id="Seg_856" n="HIAT:u" s="T223">
                  <nts id="Seg_857" n="HIAT:ip">"</nts>
                  <ts e="T224" id="Seg_859" n="HIAT:w" s="T223">Tiksi͡egiŋ</ts>
                  <nts id="Seg_860" n="HIAT:ip">,</nts>
                  <nts id="Seg_861" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_863" n="HIAT:w" s="T224">tiksi͡egiŋ</ts>
                  <nts id="Seg_864" n="HIAT:ip">.</nts>
                  <nts id="Seg_865" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T227" id="Seg_867" n="HIAT:u" s="T225">
                  <ts e="T226" id="Seg_869" n="HIAT:w" s="T225">Kaːmalɨː</ts>
                  <nts id="Seg_870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_872" n="HIAT:w" s="T226">tühü͡ögüŋ</ts>
                  <nts id="Seg_873" n="HIAT:ip">.</nts>
                  <nts id="Seg_874" n="HIAT:ip">"</nts>
                  <nts id="Seg_875" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T234" id="Seg_877" n="HIAT:u" s="T227">
                  <ts e="T228" id="Seg_879" n="HIAT:w" s="T227">Kɨtɨlga</ts>
                  <nts id="Seg_880" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_882" n="HIAT:w" s="T228">taksaːt</ts>
                  <nts id="Seg_883" n="HIAT:ip">,</nts>
                  <nts id="Seg_884" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_886" n="HIAT:w" s="T229">bu</ts>
                  <nts id="Seg_887" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_889" n="HIAT:w" s="T230">ikki</ts>
                  <nts id="Seg_890" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_892" n="HIAT:w" s="T231">u͡ol</ts>
                  <nts id="Seg_893" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_895" n="HIAT:w" s="T232">kaːrtɨlɨː</ts>
                  <nts id="Seg_896" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_898" n="HIAT:w" s="T233">olorbuttar</ts>
                  <nts id="Seg_899" n="HIAT:ip">.</nts>
                  <nts id="Seg_900" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T238" id="Seg_902" n="HIAT:u" s="T234">
                  <ts e="T235" id="Seg_904" n="HIAT:w" s="T234">ɨt</ts>
                  <nts id="Seg_905" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_907" n="HIAT:w" s="T235">u͡ola</ts>
                  <nts id="Seg_908" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_910" n="HIAT:w" s="T236">utujan</ts>
                  <nts id="Seg_911" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_913" n="HIAT:w" s="T237">kaːlbɨt</ts>
                  <nts id="Seg_914" n="HIAT:ip">.</nts>
                  <nts id="Seg_915" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T247" id="Seg_917" n="HIAT:u" s="T238">
                  <ts e="T239" id="Seg_919" n="HIAT:w" s="T238">Töhö</ts>
                  <nts id="Seg_920" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_922" n="HIAT:w" s="T239">ör</ts>
                  <nts id="Seg_923" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_925" n="HIAT:w" s="T240">oːnnʼoːbuttara</ts>
                  <nts id="Seg_926" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_928" n="HIAT:w" s="T241">bu͡olla</ts>
                  <nts id="Seg_929" n="HIAT:ip">,</nts>
                  <nts id="Seg_930" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_932" n="HIAT:w" s="T242">bu</ts>
                  <nts id="Seg_933" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_935" n="HIAT:w" s="T243">ogolor</ts>
                  <nts id="Seg_936" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_938" n="HIAT:w" s="T244">emi͡e</ts>
                  <nts id="Seg_939" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_941" n="HIAT:w" s="T245">utujan</ts>
                  <nts id="Seg_942" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_944" n="HIAT:w" s="T246">kaːlbɨttar</ts>
                  <nts id="Seg_945" n="HIAT:ip">.</nts>
                  <nts id="Seg_946" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T256" id="Seg_948" n="HIAT:u" s="T247">
                  <ts e="T248" id="Seg_950" n="HIAT:w" s="T247">ɨt</ts>
                  <nts id="Seg_951" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_953" n="HIAT:w" s="T248">u͡ola</ts>
                  <nts id="Seg_954" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_956" n="HIAT:w" s="T249">tüːn</ts>
                  <nts id="Seg_957" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_959" n="HIAT:w" s="T250">orto</ts>
                  <nts id="Seg_960" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_962" n="HIAT:w" s="T251">uhuktubuta</ts>
                  <nts id="Seg_963" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_964" n="HIAT:ip">—</nts>
                  <nts id="Seg_965" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_967" n="HIAT:w" s="T252">kihi</ts>
                  <nts id="Seg_968" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_970" n="HIAT:w" s="T253">kaːmar</ts>
                  <nts id="Seg_971" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_973" n="HIAT:w" s="T254">tɨ͡aha</ts>
                  <nts id="Seg_974" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_976" n="HIAT:w" s="T255">ihillibit</ts>
                  <nts id="Seg_977" n="HIAT:ip">.</nts>
                  <nts id="Seg_978" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T263" id="Seg_980" n="HIAT:u" s="T256">
                  <ts e="T257" id="Seg_982" n="HIAT:w" s="T256">Taksɨbɨta</ts>
                  <nts id="Seg_983" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_984" n="HIAT:ip">—</nts>
                  <nts id="Seg_985" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_987" n="HIAT:w" s="T257">abaːhɨ</ts>
                  <nts id="Seg_988" n="HIAT:ip">,</nts>
                  <nts id="Seg_989" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_991" n="HIAT:w" s="T258">balagan</ts>
                  <nts id="Seg_992" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_994" n="HIAT:w" s="T259">dʼi͡ek</ts>
                  <nts id="Seg_995" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_997" n="HIAT:w" s="T260">kaːman</ts>
                  <nts id="Seg_998" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_1000" n="HIAT:w" s="T261">iheːktiːr</ts>
                  <nts id="Seg_1001" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_1003" n="HIAT:w" s="T262">dʼe</ts>
                  <nts id="Seg_1004" n="HIAT:ip">.</nts>
                  <nts id="Seg_1005" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T272" id="Seg_1007" n="HIAT:u" s="T263">
                  <nts id="Seg_1008" n="HIAT:ip">"</nts>
                  <ts e="T264" id="Seg_1010" n="HIAT:w" s="T263">O</ts>
                  <nts id="Seg_1011" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_1013" n="HIAT:w" s="T264">dʼe</ts>
                  <nts id="Seg_1014" n="HIAT:ip">,</nts>
                  <nts id="Seg_1015" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_1017" n="HIAT:w" s="T265">ölbüppüt</ts>
                  <nts id="Seg_1018" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_1020" n="HIAT:w" s="T266">ebit</ts>
                  <nts id="Seg_1021" n="HIAT:ip">"</nts>
                  <nts id="Seg_1022" n="HIAT:ip">,</nts>
                  <nts id="Seg_1023" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_1025" n="HIAT:w" s="T267">dʼi͡en</ts>
                  <nts id="Seg_1026" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_1028" n="HIAT:w" s="T268">baran</ts>
                  <nts id="Seg_1029" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_1031" n="HIAT:w" s="T269">dogottorun</ts>
                  <nts id="Seg_1032" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_1034" n="HIAT:w" s="T270">uhugunnara</ts>
                  <nts id="Seg_1035" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_1037" n="HIAT:w" s="T271">hataːbɨt</ts>
                  <nts id="Seg_1038" n="HIAT:ip">.</nts>
                  <nts id="Seg_1039" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T276" id="Seg_1041" n="HIAT:u" s="T272">
                  <ts e="T273" id="Seg_1043" n="HIAT:w" s="T272">Bu</ts>
                  <nts id="Seg_1044" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_1046" n="HIAT:w" s="T273">u͡olattar</ts>
                  <nts id="Seg_1047" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_1049" n="HIAT:w" s="T274">togo</ts>
                  <nts id="Seg_1050" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_1052" n="HIAT:w" s="T275">uhuktu͡oktaraj</ts>
                  <nts id="Seg_1053" n="HIAT:ip">.</nts>
                  <nts id="Seg_1054" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T283" id="Seg_1056" n="HIAT:u" s="T276">
                  <nts id="Seg_1057" n="HIAT:ip">"</nts>
                  <ts e="T277" id="Seg_1059" n="HIAT:w" s="T276">Kel-kel</ts>
                  <nts id="Seg_1060" n="HIAT:ip">,</nts>
                  <nts id="Seg_1061" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_1063" n="HIAT:w" s="T277">min</ts>
                  <nts id="Seg_1064" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_1066" n="HIAT:w" s="T278">enʼigin</ts>
                  <nts id="Seg_1067" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_1069" n="HIAT:w" s="T279">hi͡em</ts>
                  <nts id="Seg_1070" n="HIAT:ip">"</nts>
                  <nts id="Seg_1071" n="HIAT:ip">,</nts>
                  <nts id="Seg_1072" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_1074" n="HIAT:w" s="T280">abaːhɨ</ts>
                  <nts id="Seg_1075" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_1077" n="HIAT:w" s="T281">haŋata</ts>
                  <nts id="Seg_1078" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_1080" n="HIAT:w" s="T282">ihillibit</ts>
                  <nts id="Seg_1081" n="HIAT:ip">.</nts>
                  <nts id="Seg_1082" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T291" id="Seg_1084" n="HIAT:u" s="T283">
                  <ts e="T284" id="Seg_1086" n="HIAT:w" s="T283">Bu</ts>
                  <nts id="Seg_1087" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_1089" n="HIAT:w" s="T284">u͡ol</ts>
                  <nts id="Seg_1090" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_1092" n="HIAT:w" s="T285">taksa</ts>
                  <nts id="Seg_1093" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_1095" n="HIAT:w" s="T286">köppüt</ts>
                  <nts id="Seg_1096" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_1098" n="HIAT:w" s="T287">da</ts>
                  <nts id="Seg_1099" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_1101" n="HIAT:w" s="T288">abaːhɨnɨ</ts>
                  <nts id="Seg_1102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_1104" n="HIAT:w" s="T289">gɨtta</ts>
                  <nts id="Seg_1105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_1107" n="HIAT:w" s="T290">ölörsübüt</ts>
                  <nts id="Seg_1108" n="HIAT:ip">.</nts>
                  <nts id="Seg_1109" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T300" id="Seg_1111" n="HIAT:u" s="T291">
                  <ts e="T292" id="Seg_1113" n="HIAT:w" s="T291">Töhö</ts>
                  <nts id="Seg_1114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_1116" n="HIAT:w" s="T292">da</ts>
                  <nts id="Seg_1117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_1119" n="HIAT:w" s="T293">bu͡olbakka</ts>
                  <nts id="Seg_1120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1122" n="HIAT:w" s="T294">abaːhɨ</ts>
                  <nts id="Seg_1123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1125" n="HIAT:w" s="T295">menʼiːtin</ts>
                  <nts id="Seg_1126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_1128" n="HIAT:w" s="T296">boldok</ts>
                  <nts id="Seg_1129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_1131" n="HIAT:w" s="T297">taːska</ts>
                  <nts id="Seg_1132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_1134" n="HIAT:w" s="T298">kaja</ts>
                  <nts id="Seg_1135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_1137" n="HIAT:w" s="T299">oksubut</ts>
                  <nts id="Seg_1138" n="HIAT:ip">.</nts>
                  <nts id="Seg_1139" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T313" id="Seg_1141" n="HIAT:u" s="T300">
                  <ts e="T301" id="Seg_1143" n="HIAT:w" s="T300">Ol</ts>
                  <nts id="Seg_1144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_1146" n="HIAT:w" s="T301">gɨnan</ts>
                  <nts id="Seg_1147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1149" n="HIAT:w" s="T302">baran</ts>
                  <nts id="Seg_1150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_1152" n="HIAT:w" s="T303">irgetin</ts>
                  <nts id="Seg_1153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_1155" n="HIAT:w" s="T304">ihitten</ts>
                  <nts id="Seg_1156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_1158" n="HIAT:w" s="T305">huruktaːk</ts>
                  <nts id="Seg_1159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_1161" n="HIAT:w" s="T306">taːhɨ</ts>
                  <nts id="Seg_1162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_1164" n="HIAT:w" s="T307">oroːbut</ts>
                  <nts id="Seg_1165" n="HIAT:ip">,</nts>
                  <nts id="Seg_1166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1168" n="HIAT:w" s="T308">taːhɨ</ts>
                  <nts id="Seg_1169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_1171" n="HIAT:w" s="T309">karmaːnɨn</ts>
                  <nts id="Seg_1172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_1174" n="HIAT:w" s="T310">ihinen</ts>
                  <nts id="Seg_1175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_1177" n="HIAT:w" s="T311">uktan</ts>
                  <nts id="Seg_1178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_1180" n="HIAT:w" s="T312">keːspit</ts>
                  <nts id="Seg_1181" n="HIAT:ip">.</nts>
                  <nts id="Seg_1182" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T322" id="Seg_1184" n="HIAT:u" s="T313">
                  <ts e="T314" id="Seg_1186" n="HIAT:w" s="T313">Harsi͡erda</ts>
                  <nts id="Seg_1187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_1189" n="HIAT:w" s="T314">tu͡ok</ts>
                  <nts id="Seg_1190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1192" n="HIAT:w" s="T315">da</ts>
                  <nts id="Seg_1193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_1195" n="HIAT:w" s="T316">bu͡olbatak</ts>
                  <nts id="Seg_1196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_1198" n="HIAT:w" s="T317">kördük</ts>
                  <nts id="Seg_1199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1201" n="HIAT:w" s="T318">ɨt</ts>
                  <nts id="Seg_1202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_1204" n="HIAT:w" s="T319">u͡ola</ts>
                  <nts id="Seg_1205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1207" n="HIAT:w" s="T320">dogottorun</ts>
                  <nts id="Seg_1208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_1210" n="HIAT:w" s="T321">uhugunnartaːbɨt</ts>
                  <nts id="Seg_1211" n="HIAT:ip">:</nts>
                  <nts id="Seg_1212" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T325" id="Seg_1214" n="HIAT:u" s="T322">
                  <nts id="Seg_1215" n="HIAT:ip">"</nts>
                  <ts e="T323" id="Seg_1217" n="HIAT:w" s="T322">Čej</ts>
                  <nts id="Seg_1218" n="HIAT:ip">,</nts>
                  <nts id="Seg_1219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_1221" n="HIAT:w" s="T323">kaːstana</ts>
                  <nts id="Seg_1222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1224" n="HIAT:w" s="T324">barɨ͡agɨŋ</ts>
                  <nts id="Seg_1225" n="HIAT:ip">.</nts>
                  <nts id="Seg_1226" n="HIAT:ip">"</nts>
                  <nts id="Seg_1227" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T331" id="Seg_1229" n="HIAT:u" s="T325">
                  <ts e="T326" id="Seg_1231" n="HIAT:w" s="T325">U͡olattar</ts>
                  <nts id="Seg_1232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1234" n="HIAT:w" s="T326">tɨːllaŋnaːn</ts>
                  <nts id="Seg_1235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_1237" n="HIAT:w" s="T327">arɨːččɨ</ts>
                  <nts id="Seg_1238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_1240" n="HIAT:w" s="T328">turan</ts>
                  <nts id="Seg_1241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_1243" n="HIAT:w" s="T329">čaːj</ts>
                  <nts id="Seg_1244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_1246" n="HIAT:w" s="T330">ispitter</ts>
                  <nts id="Seg_1247" n="HIAT:ip">.</nts>
                  <nts id="Seg_1248" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T334" id="Seg_1250" n="HIAT:u" s="T331">
                  <ts e="T332" id="Seg_1252" n="HIAT:w" s="T331">Oloŋkogo</ts>
                  <nts id="Seg_1253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_1255" n="HIAT:w" s="T332">kün</ts>
                  <nts id="Seg_1256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_1258" n="HIAT:w" s="T333">kɨlgas</ts>
                  <nts id="Seg_1259" n="HIAT:ip">.</nts>
                  <nts id="Seg_1260" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T339" id="Seg_1262" n="HIAT:u" s="T334">
                  <ts e="T335" id="Seg_1264" n="HIAT:w" s="T334">Töhö</ts>
                  <nts id="Seg_1265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_1267" n="HIAT:w" s="T335">da</ts>
                  <nts id="Seg_1268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_1270" n="HIAT:w" s="T336">bu͡olbakka</ts>
                  <nts id="Seg_1271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_1273" n="HIAT:w" s="T337">kallaːn</ts>
                  <nts id="Seg_1274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_1276" n="HIAT:w" s="T338">karaŋarbɨt</ts>
                  <nts id="Seg_1277" n="HIAT:ip">.</nts>
                  <nts id="Seg_1278" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T349" id="Seg_1280" n="HIAT:u" s="T339">
                  <ts e="T340" id="Seg_1282" n="HIAT:w" s="T339">ɨt</ts>
                  <nts id="Seg_1283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_1285" n="HIAT:w" s="T340">u͡ola</ts>
                  <nts id="Seg_1286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_1288" n="HIAT:w" s="T341">ahaːn-hi͡en</ts>
                  <nts id="Seg_1289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1291" n="HIAT:w" s="T342">baran</ts>
                  <nts id="Seg_1292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_1294" n="HIAT:w" s="T343">utujan</ts>
                  <nts id="Seg_1295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_1297" n="HIAT:w" s="T344">kaːlbɨt</ts>
                  <nts id="Seg_1298" n="HIAT:ip">,</nts>
                  <nts id="Seg_1299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_1301" n="HIAT:w" s="T345">dogottoro</ts>
                  <nts id="Seg_1302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_1304" n="HIAT:w" s="T346">emi͡e</ts>
                  <nts id="Seg_1305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1307" n="HIAT:w" s="T347">kaːrtɨlɨː</ts>
                  <nts id="Seg_1308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_1310" n="HIAT:w" s="T348">olorbuttar</ts>
                  <nts id="Seg_1311" n="HIAT:ip">.</nts>
                  <nts id="Seg_1312" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T357" id="Seg_1314" n="HIAT:u" s="T349">
                  <ts e="T350" id="Seg_1316" n="HIAT:w" s="T349">Tüːn</ts>
                  <nts id="Seg_1317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_1319" n="HIAT:w" s="T350">orto</ts>
                  <nts id="Seg_1320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_1322" n="HIAT:w" s="T351">uhuktubuta</ts>
                  <nts id="Seg_1323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1324" n="HIAT:ip">—</nts>
                  <nts id="Seg_1325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_1327" n="HIAT:w" s="T352">dogottoro</ts>
                  <nts id="Seg_1328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_1330" n="HIAT:w" s="T353">olordo</ts>
                  <nts id="Seg_1331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_1333" n="HIAT:w" s="T354">olorbutunan</ts>
                  <nts id="Seg_1334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_1336" n="HIAT:w" s="T355">utujan</ts>
                  <nts id="Seg_1337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_1339" n="HIAT:w" s="T356">kaːlbɨttar</ts>
                  <nts id="Seg_1340" n="HIAT:ip">.</nts>
                  <nts id="Seg_1341" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T370" id="Seg_1343" n="HIAT:u" s="T357">
                  <ts e="T358" id="Seg_1345" n="HIAT:w" s="T357">Bu</ts>
                  <nts id="Seg_1346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_1348" n="HIAT:w" s="T358">ogo</ts>
                  <nts id="Seg_1349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_1351" n="HIAT:w" s="T359">uhuktan</ts>
                  <nts id="Seg_1352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_1354" n="HIAT:w" s="T360">baran</ts>
                  <nts id="Seg_1355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_1357" n="HIAT:w" s="T361">ihilliː</ts>
                  <nts id="Seg_1358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_1360" n="HIAT:w" s="T362">hɨppɨt</ts>
                  <nts id="Seg_1361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1362" n="HIAT:ip">—</nts>
                  <nts id="Seg_1363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_1365" n="HIAT:w" s="T363">ɨraːk</ts>
                  <nts id="Seg_1366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_1368" n="HIAT:w" s="T364">kanna</ts>
                  <nts id="Seg_1369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_1371" n="HIAT:w" s="T365">ire</ts>
                  <nts id="Seg_1372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_1374" n="HIAT:w" s="T366">emi͡e</ts>
                  <nts id="Seg_1375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_1377" n="HIAT:w" s="T367">kaːmar</ts>
                  <nts id="Seg_1378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_1380" n="HIAT:w" s="T368">tɨ͡as</ts>
                  <nts id="Seg_1381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_1383" n="HIAT:w" s="T369">ihillibit</ts>
                  <nts id="Seg_1384" n="HIAT:ip">.</nts>
                  <nts id="Seg_1385" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T377" id="Seg_1387" n="HIAT:u" s="T370">
                  <ts e="T371" id="Seg_1389" n="HIAT:w" s="T370">Dogottorun</ts>
                  <nts id="Seg_1390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_1392" n="HIAT:w" s="T371">uhugunnarbɨta</ts>
                  <nts id="Seg_1393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1394" n="HIAT:ip">—</nts>
                  <nts id="Seg_1395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_1397" n="HIAT:w" s="T372">togo</ts>
                  <nts id="Seg_1398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_1400" n="HIAT:w" s="T373">uhuktu͡oktaraj</ts>
                  <nts id="Seg_1401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_1403" n="HIAT:w" s="T374">čubu</ts>
                  <nts id="Seg_1404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_1406" n="HIAT:w" s="T375">utujbut</ts>
                  <nts id="Seg_1407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_1409" n="HIAT:w" s="T376">kihiler</ts>
                  <nts id="Seg_1410" n="HIAT:ip">.</nts>
                  <nts id="Seg_1411" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T389" id="Seg_1413" n="HIAT:u" s="T377">
                  <ts e="T378" id="Seg_1415" n="HIAT:w" s="T377">ɨt</ts>
                  <nts id="Seg_1416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_1418" n="HIAT:w" s="T378">u͡ola</ts>
                  <nts id="Seg_1419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_1421" n="HIAT:w" s="T379">taksɨbɨt</ts>
                  <nts id="Seg_1422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_1424" n="HIAT:w" s="T380">da</ts>
                  <nts id="Seg_1425" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T382" id="Seg_1427" n="HIAT:w" s="T381">ikkis</ts>
                  <nts id="Seg_1428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_1430" n="HIAT:w" s="T382">abaːhɨ</ts>
                  <nts id="Seg_1431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_1433" n="HIAT:w" s="T383">menʼiːtin</ts>
                  <nts id="Seg_1434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_1436" n="HIAT:w" s="T384">emi͡e</ts>
                  <nts id="Seg_1437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T386" id="Seg_1439" n="HIAT:w" s="T385">boldok</ts>
                  <nts id="Seg_1440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_1442" n="HIAT:w" s="T386">taːska</ts>
                  <nts id="Seg_1443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_1445" n="HIAT:w" s="T387">kampɨ</ts>
                  <nts id="Seg_1446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_1448" n="HIAT:w" s="T388">oksubut</ts>
                  <nts id="Seg_1449" n="HIAT:ip">.</nts>
                  <nts id="Seg_1450" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T395" id="Seg_1452" n="HIAT:u" s="T389">
                  <ts e="T390" id="Seg_1454" n="HIAT:w" s="T389">Abaːhɨ</ts>
                  <nts id="Seg_1455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T391" id="Seg_1457" n="HIAT:w" s="T390">tebi͡elene</ts>
                  <nts id="Seg_1458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_1460" n="HIAT:w" s="T391">tühen</ts>
                  <nts id="Seg_1461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T393" id="Seg_1463" n="HIAT:w" s="T392">baran</ts>
                  <nts id="Seg_1464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_1466" n="HIAT:w" s="T393">čirkes</ts>
                  <nts id="Seg_1467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_1469" n="HIAT:w" s="T394">gɨmmɨt</ts>
                  <nts id="Seg_1470" n="HIAT:ip">.</nts>
                  <nts id="Seg_1471" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T406" id="Seg_1473" n="HIAT:u" s="T395">
                  <ts e="T396" id="Seg_1475" n="HIAT:w" s="T395">ɨt</ts>
                  <nts id="Seg_1476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_1478" n="HIAT:w" s="T396">u͡ola</ts>
                  <nts id="Seg_1479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_1481" n="HIAT:w" s="T397">irgetitten</ts>
                  <nts id="Seg_1482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_1484" n="HIAT:w" s="T398">huruktaːk</ts>
                  <nts id="Seg_1485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T400" id="Seg_1487" n="HIAT:w" s="T399">taːhɨ</ts>
                  <nts id="Seg_1488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_1490" n="HIAT:w" s="T400">oroːto</ts>
                  <nts id="Seg_1491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T402" id="Seg_1493" n="HIAT:w" s="T401">dagɨnɨ</ts>
                  <nts id="Seg_1494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_1496" n="HIAT:w" s="T402">karmaːnɨgar</ts>
                  <nts id="Seg_1497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T404" id="Seg_1499" n="HIAT:w" s="T403">emi͡e</ts>
                  <nts id="Seg_1500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T405" id="Seg_1502" n="HIAT:w" s="T404">uktan</ts>
                  <nts id="Seg_1503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_1505" n="HIAT:w" s="T405">keːspit</ts>
                  <nts id="Seg_1506" n="HIAT:ip">.</nts>
                  <nts id="Seg_1507" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T417" id="Seg_1509" n="HIAT:u" s="T406">
                  <ts e="T407" id="Seg_1511" n="HIAT:w" s="T406">Ühüs</ts>
                  <nts id="Seg_1512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T408" id="Seg_1514" n="HIAT:w" s="T407">tüːnüger</ts>
                  <nts id="Seg_1515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T409" id="Seg_1517" n="HIAT:w" s="T408">ühüs</ts>
                  <nts id="Seg_1518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_1520" n="HIAT:w" s="T409">abaːhɨ</ts>
                  <nts id="Seg_1521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T411" id="Seg_1523" n="HIAT:w" s="T410">menʼiːtin</ts>
                  <nts id="Seg_1524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T412" id="Seg_1526" n="HIAT:w" s="T411">kaja</ts>
                  <nts id="Seg_1527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T413" id="Seg_1529" n="HIAT:w" s="T412">okson</ts>
                  <nts id="Seg_1530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414" id="Seg_1532" n="HIAT:w" s="T413">huruktaːk</ts>
                  <nts id="Seg_1533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T415" id="Seg_1535" n="HIAT:w" s="T414">taːhɨ</ts>
                  <nts id="Seg_1536" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T416" id="Seg_1538" n="HIAT:w" s="T415">oruː</ts>
                  <nts id="Seg_1539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T417" id="Seg_1541" n="HIAT:w" s="T416">oksubut</ts>
                  <nts id="Seg_1542" n="HIAT:ip">.</nts>
                  <nts id="Seg_1543" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T422" id="Seg_1545" n="HIAT:u" s="T417">
                  <ts e="T418" id="Seg_1547" n="HIAT:w" s="T417">Dogottorun</ts>
                  <nts id="Seg_1548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T419" id="Seg_1550" n="HIAT:w" s="T418">im</ts>
                  <nts id="Seg_1551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T420" id="Seg_1553" n="HIAT:w" s="T419">turarɨn</ts>
                  <nts id="Seg_1554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_1556" n="HIAT:w" s="T420">kɨtta</ts>
                  <nts id="Seg_1557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T422" id="Seg_1559" n="HIAT:w" s="T421">uhugunnarbɨt</ts>
                  <nts id="Seg_1560" n="HIAT:ip">.</nts>
                  <nts id="Seg_1561" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T428" id="Seg_1563" n="HIAT:u" s="T422">
                  <nts id="Seg_1564" n="HIAT:ip">"</nts>
                  <ts e="T423" id="Seg_1566" n="HIAT:w" s="T422">Če</ts>
                  <nts id="Seg_1567" n="HIAT:ip">,</nts>
                  <nts id="Seg_1568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T424" id="Seg_1570" n="HIAT:w" s="T423">dʼi͡ebitiger</ts>
                  <nts id="Seg_1571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T425" id="Seg_1573" n="HIAT:w" s="T424">barɨ͡agɨŋ</ts>
                  <nts id="Seg_1574" n="HIAT:ip">"</nts>
                  <nts id="Seg_1575" n="HIAT:ip">,</nts>
                  <nts id="Seg_1576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T426" id="Seg_1578" n="HIAT:w" s="T425">dʼi͡ebit</ts>
                  <nts id="Seg_1579" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T427" id="Seg_1581" n="HIAT:w" s="T426">bu</ts>
                  <nts id="Seg_1582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T428" id="Seg_1584" n="HIAT:w" s="T427">u͡ol</ts>
                  <nts id="Seg_1585" n="HIAT:ip">.</nts>
                  <nts id="Seg_1586" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T431" id="Seg_1588" n="HIAT:u" s="T428">
                  <ts e="T429" id="Seg_1590" n="HIAT:w" s="T428">ɨraːktaːgɨ</ts>
                  <nts id="Seg_1591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_1593" n="HIAT:w" s="T429">u͡ola</ts>
                  <nts id="Seg_1594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T431" id="Seg_1596" n="HIAT:w" s="T430">öčöspüt</ts>
                  <nts id="Seg_1597" n="HIAT:ip">:</nts>
                  <nts id="Seg_1598" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T433" id="Seg_1600" n="HIAT:u" s="T431">
                  <nts id="Seg_1601" n="HIAT:ip">"</nts>
                  <ts e="T432" id="Seg_1603" n="HIAT:w" s="T431">Togo</ts>
                  <nts id="Seg_1604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T433" id="Seg_1606" n="HIAT:w" s="T432">doː</ts>
                  <nts id="Seg_1607" n="HIAT:ip">!</nts>
                  <nts id="Seg_1608" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T436" id="Seg_1610" n="HIAT:u" s="T433">
                  <ts e="T434" id="Seg_1612" n="HIAT:w" s="T433">Oloro</ts>
                  <nts id="Seg_1613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T435" id="Seg_1615" n="HIAT:w" s="T434">tühü͡ögüŋ</ts>
                  <nts id="Seg_1616" n="HIAT:ip">,</nts>
                  <nts id="Seg_1617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_1619" n="HIAT:w" s="T435">beseleː</ts>
                  <nts id="Seg_1620" n="HIAT:ip">.</nts>
                  <nts id="Seg_1621" n="HIAT:ip">"</nts>
                  <nts id="Seg_1622" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T442" id="Seg_1624" n="HIAT:u" s="T436">
                  <ts e="T437" id="Seg_1626" n="HIAT:w" s="T436">Koru͡oba</ts>
                  <nts id="Seg_1627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T438" id="Seg_1629" n="HIAT:w" s="T437">u͡ola</ts>
                  <nts id="Seg_1630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T439" id="Seg_1632" n="HIAT:w" s="T438">oloro</ts>
                  <nts id="Seg_1633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440" id="Seg_1635" n="HIAT:w" s="T439">tühen</ts>
                  <nts id="Seg_1636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_1638" n="HIAT:w" s="T440">baran</ts>
                  <nts id="Seg_1639" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T442" id="Seg_1641" n="HIAT:w" s="T441">dʼi͡ebit</ts>
                  <nts id="Seg_1642" n="HIAT:ip">:</nts>
                  <nts id="Seg_1643" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T446" id="Seg_1645" n="HIAT:u" s="T442">
                  <nts id="Seg_1646" n="HIAT:ip">"</nts>
                  <ts e="T443" id="Seg_1648" n="HIAT:w" s="T442">Kergetterbit</ts>
                  <nts id="Seg_1649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T444" id="Seg_1651" n="HIAT:w" s="T443">küːtü͡öktere</ts>
                  <nts id="Seg_1652" n="HIAT:ip">,</nts>
                  <nts id="Seg_1653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T445" id="Seg_1655" n="HIAT:w" s="T444">barɨ͡agɨŋ</ts>
                  <nts id="Seg_1656" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T446" id="Seg_1658" n="HIAT:w" s="T445">eːt</ts>
                  <nts id="Seg_1659" n="HIAT:ip">.</nts>
                  <nts id="Seg_1660" n="HIAT:ip">"</nts>
                  <nts id="Seg_1661" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T448" id="Seg_1663" n="HIAT:u" s="T446">
                  <ts e="T447" id="Seg_1665" n="HIAT:w" s="T446">U͡olattar</ts>
                  <nts id="Seg_1666" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T448" id="Seg_1668" n="HIAT:w" s="T447">kamnaːbɨttar</ts>
                  <nts id="Seg_1669" n="HIAT:ip">.</nts>
                  <nts id="Seg_1670" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T452" id="Seg_1672" n="HIAT:u" s="T448">
                  <ts e="T449" id="Seg_1674" n="HIAT:w" s="T448">ɨt</ts>
                  <nts id="Seg_1675" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T450" id="Seg_1677" n="HIAT:w" s="T449">u͡ola</ts>
                  <nts id="Seg_1678" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T451" id="Seg_1680" n="HIAT:w" s="T450">erden</ts>
                  <nts id="Seg_1681" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T452" id="Seg_1683" n="HIAT:w" s="T451">ispit</ts>
                  <nts id="Seg_1684" n="HIAT:ip">.</nts>
                  <nts id="Seg_1685" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T458" id="Seg_1687" n="HIAT:u" s="T452">
                  <ts e="T453" id="Seg_1689" n="HIAT:w" s="T452">Töhö</ts>
                  <nts id="Seg_1690" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T454" id="Seg_1692" n="HIAT:w" s="T453">da</ts>
                  <nts id="Seg_1693" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T455" id="Seg_1695" n="HIAT:w" s="T454">bu͡olbakka</ts>
                  <nts id="Seg_1696" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T456" id="Seg_1698" n="HIAT:w" s="T455">kɨːnɨn</ts>
                  <nts id="Seg_1699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T457" id="Seg_1701" n="HIAT:w" s="T456">tuttan</ts>
                  <nts id="Seg_1702" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T458" id="Seg_1704" n="HIAT:w" s="T457">baran</ts>
                  <nts id="Seg_1705" n="HIAT:ip">:</nts>
                  <nts id="Seg_1706" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T464" id="Seg_1708" n="HIAT:u" s="T458">
                  <ts e="T459" id="Seg_1710" n="HIAT:w" s="T458">Dogottoːr</ts>
                  <nts id="Seg_1711" n="HIAT:ip">,</nts>
                  <nts id="Seg_1712" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T460" id="Seg_1714" n="HIAT:w" s="T459">bahakpɨn</ts>
                  <nts id="Seg_1715" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T461" id="Seg_1717" n="HIAT:w" s="T460">kaːllarbɨppɨn</ts>
                  <nts id="Seg_1718" n="HIAT:ip">,</nts>
                  <nts id="Seg_1719" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T462" id="Seg_1721" n="HIAT:w" s="T461">kördüː</ts>
                  <nts id="Seg_1722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T463" id="Seg_1724" n="HIAT:w" s="T462">barɨ͡am</ts>
                  <nts id="Seg_1725" n="HIAT:ip">,</nts>
                  <nts id="Seg_1726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T464" id="Seg_1728" n="HIAT:w" s="T463">dʼi͡ebit</ts>
                  <nts id="Seg_1729" n="HIAT:ip">.</nts>
                  <nts id="Seg_1730" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T472" id="Seg_1732" n="HIAT:u" s="T464">
                  <ts e="T465" id="Seg_1734" n="HIAT:w" s="T464">ɨt</ts>
                  <nts id="Seg_1735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T466" id="Seg_1737" n="HIAT:w" s="T465">u͡ola</ts>
                  <nts id="Seg_1738" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T467" id="Seg_1740" n="HIAT:w" s="T466">balagaŋŋa</ts>
                  <nts id="Seg_1741" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T468" id="Seg_1743" n="HIAT:w" s="T467">kelbite</ts>
                  <nts id="Seg_1744" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T469" id="Seg_1746" n="HIAT:w" s="T468">dʼaktattar</ts>
                  <nts id="Seg_1747" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T470" id="Seg_1749" n="HIAT:w" s="T469">ɨtɨːr</ts>
                  <nts id="Seg_1750" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T471" id="Seg_1752" n="HIAT:w" s="T470">haŋalara</ts>
                  <nts id="Seg_1753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T472" id="Seg_1755" n="HIAT:w" s="T471">ihillibit</ts>
                  <nts id="Seg_1756" n="HIAT:ip">.</nts>
                  <nts id="Seg_1757" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T482" id="Seg_1759" n="HIAT:u" s="T472">
                  <nts id="Seg_1760" n="HIAT:ip">"</nts>
                  <ts e="T473" id="Seg_1762" n="HIAT:w" s="T472">Min</ts>
                  <nts id="Seg_1763" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T474" id="Seg_1765" n="HIAT:w" s="T473">minnʼiges</ts>
                  <nts id="Seg_1766" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T475" id="Seg_1768" n="HIAT:w" s="T474">as</ts>
                  <nts id="Seg_1769" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T476" id="Seg_1771" n="HIAT:w" s="T475">bu͡olan</ts>
                  <nts id="Seg_1772" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T477" id="Seg_1774" n="HIAT:w" s="T476">baran</ts>
                  <nts id="Seg_1775" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T478" id="Seg_1777" n="HIAT:w" s="T477">ulakan</ts>
                  <nts id="Seg_1778" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T479" id="Seg_1780" n="HIAT:w" s="T478">teri͡elkege</ts>
                  <nts id="Seg_1781" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T480" id="Seg_1783" n="HIAT:w" s="T479">tɨːlarɨn</ts>
                  <nts id="Seg_1784" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T481" id="Seg_1786" n="HIAT:w" s="T480">ihiger</ts>
                  <nts id="Seg_1787" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T482" id="Seg_1789" n="HIAT:w" s="T481">tühü͡öm</ts>
                  <nts id="Seg_1790" n="HIAT:ip">.</nts>
                  <nts id="Seg_1791" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T490" id="Seg_1793" n="HIAT:u" s="T482">
                  <ts e="T483" id="Seg_1795" n="HIAT:w" s="T482">Minʼigin</ts>
                  <nts id="Seg_1796" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T484" id="Seg_1798" n="HIAT:w" s="T483">hi͡etekterine</ts>
                  <nts id="Seg_1799" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T485" id="Seg_1801" n="HIAT:w" s="T484">ölön</ts>
                  <nts id="Seg_1802" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T486" id="Seg_1804" n="HIAT:w" s="T485">kaːlɨ͡aktara</ts>
                  <nts id="Seg_1805" n="HIAT:ip">"</nts>
                  <nts id="Seg_1806" n="HIAT:ip">,</nts>
                  <nts id="Seg_1807" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T487" id="Seg_1809" n="HIAT:w" s="T486">dʼi͡ebit</ts>
                  <nts id="Seg_1810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T488" id="Seg_1812" n="HIAT:w" s="T487">kardʼagas</ts>
                  <nts id="Seg_1813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T488.tx.1" id="Seg_1815" n="HIAT:w" s="T488">Dʼige</ts>
                  <nts id="Seg_1816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T489" id="Seg_1818" n="HIAT:w" s="T488.tx.1">baːba</ts>
                  <nts id="Seg_1819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T490" id="Seg_1821" n="HIAT:w" s="T489">ku͡olaha</ts>
                  <nts id="Seg_1822" n="HIAT:ip">.</nts>
                  <nts id="Seg_1823" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T506" id="Seg_1825" n="HIAT:u" s="T490">
                  <nts id="Seg_1826" n="HIAT:ip">"</nts>
                  <ts e="T491" id="Seg_1828" n="HIAT:w" s="T490">Min</ts>
                  <nts id="Seg_1829" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T492" id="Seg_1831" n="HIAT:w" s="T491">bu͡ollagɨna</ts>
                  <nts id="Seg_1832" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T493" id="Seg_1834" n="HIAT:w" s="T492">kahan</ts>
                  <nts id="Seg_1835" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T494" id="Seg_1837" n="HIAT:w" s="T493">da</ts>
                  <nts id="Seg_1838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T495" id="Seg_1840" n="HIAT:w" s="T494">körbötök</ts>
                  <nts id="Seg_1841" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T496" id="Seg_1843" n="HIAT:w" s="T495">bosku͡oj</ts>
                  <nts id="Seg_1844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T497" id="Seg_1846" n="HIAT:w" s="T496">holko</ts>
                  <nts id="Seg_1847" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T498" id="Seg_1849" n="HIAT:w" s="T497">pɨlaːt</ts>
                  <nts id="Seg_1850" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T499" id="Seg_1852" n="HIAT:w" s="T498">bu͡olan</ts>
                  <nts id="Seg_1853" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T500" id="Seg_1855" n="HIAT:w" s="T499">ɨraːktaːgɨ</ts>
                  <nts id="Seg_1856" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T501" id="Seg_1858" n="HIAT:w" s="T500">u͡olun</ts>
                  <nts id="Seg_1859" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T502" id="Seg_1861" n="HIAT:w" s="T501">töhögüger</ts>
                  <nts id="Seg_1862" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T503" id="Seg_1864" n="HIAT:w" s="T502">tühü͡öm</ts>
                  <nts id="Seg_1865" n="HIAT:ip">,</nts>
                  <nts id="Seg_1866" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T504" id="Seg_1868" n="HIAT:w" s="T503">barɨkaːttarɨn</ts>
                  <nts id="Seg_1869" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T505" id="Seg_1871" n="HIAT:w" s="T504">moŋunnaran</ts>
                  <nts id="Seg_1872" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T506" id="Seg_1874" n="HIAT:w" s="T505">ölörü͡öm</ts>
                  <nts id="Seg_1875" n="HIAT:ip">.</nts>
                  <nts id="Seg_1876" n="HIAT:ip">"</nts>
                  <nts id="Seg_1877" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T518" id="Seg_1879" n="HIAT:u" s="T506">
                  <nts id="Seg_1880" n="HIAT:ip">"</nts>
                  <ts e="T507" id="Seg_1882" n="HIAT:w" s="T506">Min</ts>
                  <nts id="Seg_1883" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T508" id="Seg_1885" n="HIAT:w" s="T507">bu͡ollagɨna</ts>
                  <nts id="Seg_1886" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T509" id="Seg_1888" n="HIAT:w" s="T508">tɨːlarɨn</ts>
                  <nts id="Seg_1889" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T510" id="Seg_1891" n="HIAT:w" s="T509">ipseri</ts>
                  <nts id="Seg_1892" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T511" id="Seg_1894" n="HIAT:w" s="T510">hilimniːr</ts>
                  <nts id="Seg_1895" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T512" id="Seg_1897" n="HIAT:w" s="T511">gɨna</ts>
                  <nts id="Seg_1898" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T513" id="Seg_1900" n="HIAT:w" s="T512">uː</ts>
                  <nts id="Seg_1901" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T514" id="Seg_1903" n="HIAT:w" s="T513">hɨmala</ts>
                  <nts id="Seg_1904" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T515" id="Seg_1906" n="HIAT:w" s="T514">bu͡olu͡om</ts>
                  <nts id="Seg_1907" n="HIAT:ip">,</nts>
                  <nts id="Seg_1908" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T516" id="Seg_1910" n="HIAT:w" s="T515">barɨlarɨn</ts>
                  <nts id="Seg_1911" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T517" id="Seg_1913" n="HIAT:w" s="T516">oboron</ts>
                  <nts id="Seg_1914" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T518" id="Seg_1916" n="HIAT:w" s="T517">timirdi͡em</ts>
                  <nts id="Seg_1917" n="HIAT:ip">.</nts>
                  <nts id="Seg_1918" n="HIAT:ip">"</nts>
                  <nts id="Seg_1919" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T520" id="Seg_1921" n="HIAT:u" s="T518">
                  <nts id="Seg_1922" n="HIAT:ip">"</nts>
                  <ts e="T519" id="Seg_1924" n="HIAT:w" s="T518">Dʼe</ts>
                  <nts id="Seg_1925" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T520" id="Seg_1927" n="HIAT:w" s="T519">üčügej</ts>
                  <nts id="Seg_1928" n="HIAT:ip">.</nts>
                  <nts id="Seg_1929" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T526" id="Seg_1931" n="HIAT:u" s="T520">
                  <ts e="T521" id="Seg_1933" n="HIAT:w" s="T520">Bertik</ts>
                  <nts id="Seg_1934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T522" id="Seg_1936" n="HIAT:w" s="T521">hanaːbɨkkɨt</ts>
                  <nts id="Seg_1937" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T523" id="Seg_1939" n="HIAT:w" s="T522">ebit</ts>
                  <nts id="Seg_1940" n="HIAT:ip">"</nts>
                  <nts id="Seg_1941" n="HIAT:ip">,</nts>
                  <nts id="Seg_1942" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T524" id="Seg_1944" n="HIAT:w" s="T523">dʼi͡ebit</ts>
                  <nts id="Seg_1945" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T525" id="Seg_1947" n="HIAT:w" s="T524">mannajgɨ</ts>
                  <nts id="Seg_1948" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T526" id="Seg_1950" n="HIAT:w" s="T525">dʼaktar</ts>
                  <nts id="Seg_1951" n="HIAT:ip">.</nts>
                  <nts id="Seg_1952" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T540" id="Seg_1954" n="HIAT:u" s="T526">
                  <ts e="T527" id="Seg_1956" n="HIAT:w" s="T526">ɨt</ts>
                  <nts id="Seg_1957" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T528" id="Seg_1959" n="HIAT:w" s="T527">u͡ola</ts>
                  <nts id="Seg_1960" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T529" id="Seg_1962" n="HIAT:w" s="T528">itini</ts>
                  <nts id="Seg_1963" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T530" id="Seg_1965" n="HIAT:w" s="T529">istibit</ts>
                  <nts id="Seg_1966" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T531" id="Seg_1968" n="HIAT:w" s="T530">daːganɨ</ts>
                  <nts id="Seg_1969" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T532" id="Seg_1971" n="HIAT:w" s="T531">hɨːr</ts>
                  <nts id="Seg_1972" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T533" id="Seg_1974" n="HIAT:w" s="T532">annɨgar</ts>
                  <nts id="Seg_1975" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T534" id="Seg_1977" n="HIAT:w" s="T533">hüːren</ts>
                  <nts id="Seg_1978" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T535" id="Seg_1980" n="HIAT:w" s="T534">kiːrbit</ts>
                  <nts id="Seg_1981" n="HIAT:ip">,</nts>
                  <nts id="Seg_1982" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T536" id="Seg_1984" n="HIAT:w" s="T535">tɨːtɨn</ts>
                  <nts id="Seg_1985" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T537" id="Seg_1987" n="HIAT:w" s="T536">ɨlbaktɨːr</ts>
                  <nts id="Seg_1988" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T538" id="Seg_1990" n="HIAT:w" s="T537">da</ts>
                  <nts id="Seg_1991" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T539" id="Seg_1993" n="HIAT:w" s="T538">erden</ts>
                  <nts id="Seg_1994" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T540" id="Seg_1996" n="HIAT:w" s="T539">ispit</ts>
                  <nts id="Seg_1997" n="HIAT:ip">.</nts>
                  <nts id="Seg_1998" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T545" id="Seg_2000" n="HIAT:u" s="T540">
                  <ts e="T541" id="Seg_2002" n="HIAT:w" s="T540">Dogottorun</ts>
                  <nts id="Seg_2003" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T542" id="Seg_2005" n="HIAT:w" s="T541">ebe</ts>
                  <nts id="Seg_2006" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T543" id="Seg_2008" n="HIAT:w" s="T542">ortotugar</ts>
                  <nts id="Seg_2009" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T544" id="Seg_2011" n="HIAT:w" s="T543">hite</ts>
                  <nts id="Seg_2012" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T545" id="Seg_2014" n="HIAT:w" s="T544">oksubut</ts>
                  <nts id="Seg_2015" n="HIAT:ip">.</nts>
                  <nts id="Seg_2016" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T549" id="Seg_2018" n="HIAT:u" s="T545">
                  <ts e="T546" id="Seg_2020" n="HIAT:w" s="T545">U͡olattar</ts>
                  <nts id="Seg_2021" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T547" id="Seg_2023" n="HIAT:w" s="T546">orgujakaːn</ts>
                  <nts id="Seg_2024" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T548" id="Seg_2026" n="HIAT:w" s="T547">erden</ts>
                  <nts id="Seg_2027" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T549" id="Seg_2029" n="HIAT:w" s="T548">ispitter</ts>
                  <nts id="Seg_2030" n="HIAT:ip">.</nts>
                  <nts id="Seg_2031" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T557" id="Seg_2033" n="HIAT:u" s="T549">
                  <ts e="T550" id="Seg_2035" n="HIAT:w" s="T549">Töhö</ts>
                  <nts id="Seg_2036" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T551" id="Seg_2038" n="HIAT:w" s="T550">ör</ts>
                  <nts id="Seg_2039" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T552" id="Seg_2041" n="HIAT:w" s="T551">bu͡olu͡oj</ts>
                  <nts id="Seg_2042" n="HIAT:ip">,</nts>
                  <nts id="Seg_2043" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T553" id="Seg_2045" n="HIAT:w" s="T552">tɨːlarɨn</ts>
                  <nts id="Seg_2046" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T554" id="Seg_2048" n="HIAT:w" s="T553">ihiger</ts>
                  <nts id="Seg_2049" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T555" id="Seg_2051" n="HIAT:w" s="T554">astaːk</ts>
                  <nts id="Seg_2052" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T556" id="Seg_2054" n="HIAT:w" s="T555">teri͡elke</ts>
                  <nts id="Seg_2055" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T557" id="Seg_2057" n="HIAT:w" s="T556">tüspüt</ts>
                  <nts id="Seg_2058" n="HIAT:ip">.</nts>
                  <nts id="Seg_2059" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T573" id="Seg_2061" n="HIAT:u" s="T557">
                  <ts e="T558" id="Seg_2063" n="HIAT:w" s="T557">ɨraːktaːgɨ</ts>
                  <nts id="Seg_2064" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T559" id="Seg_2066" n="HIAT:w" s="T558">u͡ola</ts>
                  <nts id="Seg_2067" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T560" id="Seg_2069" n="HIAT:w" s="T559">ü͡örbüt</ts>
                  <nts id="Seg_2070" n="HIAT:ip">,</nts>
                  <nts id="Seg_2071" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T561" id="Seg_2073" n="HIAT:w" s="T560">iliːtin</ts>
                  <nts id="Seg_2074" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T562" id="Seg_2076" n="HIAT:w" s="T561">teri͡elke</ts>
                  <nts id="Seg_2077" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T563" id="Seg_2079" n="HIAT:w" s="T562">dʼi͡ek</ts>
                  <nts id="Seg_2080" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T564" id="Seg_2082" n="HIAT:w" s="T563">harbas</ts>
                  <nts id="Seg_2083" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T565" id="Seg_2085" n="HIAT:w" s="T564">gɨnarɨn</ts>
                  <nts id="Seg_2086" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T566" id="Seg_2088" n="HIAT:w" s="T565">gɨtta</ts>
                  <nts id="Seg_2089" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T567" id="Seg_2091" n="HIAT:w" s="T566">ɨt</ts>
                  <nts id="Seg_2092" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T568" id="Seg_2094" n="HIAT:w" s="T567">u͡ola</ts>
                  <nts id="Seg_2095" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T569" id="Seg_2097" n="HIAT:w" s="T568">astaːk</ts>
                  <nts id="Seg_2098" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T570" id="Seg_2100" n="HIAT:w" s="T569">teri͡elkeni</ts>
                  <nts id="Seg_2101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T571" id="Seg_2103" n="HIAT:w" s="T570">uːga</ts>
                  <nts id="Seg_2104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T572" id="Seg_2106" n="HIAT:w" s="T571">bɨragan</ts>
                  <nts id="Seg_2107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T573" id="Seg_2109" n="HIAT:w" s="T572">keːspit</ts>
                  <nts id="Seg_2110" n="HIAT:ip">.</nts>
                  <nts id="Seg_2111" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T586" id="Seg_2113" n="HIAT:u" s="T573">
                  <ts e="T574" id="Seg_2115" n="HIAT:w" s="T573">ɨraːktaːgɨ</ts>
                  <nts id="Seg_2116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T575" id="Seg_2118" n="HIAT:w" s="T574">u͡ola</ts>
                  <nts id="Seg_2119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T576" id="Seg_2121" n="HIAT:w" s="T575">kɨːhɨran</ts>
                  <nts id="Seg_2122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T577" id="Seg_2124" n="HIAT:w" s="T576">huntujan</ts>
                  <nts id="Seg_2125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T578" id="Seg_2127" n="HIAT:w" s="T577">olordoguna</ts>
                  <nts id="Seg_2128" n="HIAT:ip">,</nts>
                  <nts id="Seg_2129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T579" id="Seg_2131" n="HIAT:w" s="T578">bosku͡oj</ts>
                  <nts id="Seg_2132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T580" id="Seg_2134" n="HIAT:w" s="T579">bagajɨ</ts>
                  <nts id="Seg_2135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T581" id="Seg_2137" n="HIAT:w" s="T580">holko</ts>
                  <nts id="Seg_2138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T582" id="Seg_2140" n="HIAT:w" s="T581">pulaːt</ts>
                  <nts id="Seg_2141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T583" id="Seg_2143" n="HIAT:w" s="T582">ɨraːktaːgɨ</ts>
                  <nts id="Seg_2144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T584" id="Seg_2146" n="HIAT:w" s="T583">u͡olun</ts>
                  <nts id="Seg_2147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T585" id="Seg_2149" n="HIAT:w" s="T584">töhögüger</ts>
                  <nts id="Seg_2150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T586" id="Seg_2152" n="HIAT:w" s="T585">tüspüt</ts>
                  <nts id="Seg_2153" n="HIAT:ip">.</nts>
                  <nts id="Seg_2154" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T607" id="Seg_2156" n="HIAT:u" s="T586">
                  <nts id="Seg_2157" n="HIAT:ip">"</nts>
                  <ts e="T587" id="Seg_2159" n="HIAT:w" s="T586">O</ts>
                  <nts id="Seg_2160" n="HIAT:ip">,</nts>
                  <nts id="Seg_2161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T588" id="Seg_2163" n="HIAT:w" s="T587">taŋara</ts>
                  <nts id="Seg_2164" n="HIAT:ip">,</nts>
                  <nts id="Seg_2165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T589" id="Seg_2167" n="HIAT:w" s="T588">maːmabar</ts>
                  <nts id="Seg_2168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T590" id="Seg_2170" n="HIAT:w" s="T589">kehiː</ts>
                  <nts id="Seg_2171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T591" id="Seg_2173" n="HIAT:w" s="T590">gɨnɨ͡am</ts>
                  <nts id="Seg_2174" n="HIAT:ip">"</nts>
                  <nts id="Seg_2175" n="HIAT:ip">,</nts>
                  <nts id="Seg_2176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T592" id="Seg_2178" n="HIAT:w" s="T591">dʼi͡ebit</ts>
                  <nts id="Seg_2179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T593" id="Seg_2181" n="HIAT:w" s="T592">da</ts>
                  <nts id="Seg_2182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T594" id="Seg_2184" n="HIAT:w" s="T593">karmaːnɨgar</ts>
                  <nts id="Seg_2185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T595" id="Seg_2187" n="HIAT:w" s="T594">uktaːrɨ</ts>
                  <nts id="Seg_2188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T596" id="Seg_2190" n="HIAT:w" s="T595">gɨnarɨn</ts>
                  <nts id="Seg_2191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T597" id="Seg_2193" n="HIAT:w" s="T596">gɨtta</ts>
                  <nts id="Seg_2194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T598" id="Seg_2196" n="HIAT:w" s="T597">u͡ola</ts>
                  <nts id="Seg_2197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T599" id="Seg_2199" n="HIAT:w" s="T598">talaːn</ts>
                  <nts id="Seg_2200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T600" id="Seg_2202" n="HIAT:w" s="T599">ɨlbɨt</ts>
                  <nts id="Seg_2203" n="HIAT:ip">,</nts>
                  <nts id="Seg_2204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T601" id="Seg_2206" n="HIAT:w" s="T600">bahagɨnan</ts>
                  <nts id="Seg_2207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T602" id="Seg_2209" n="HIAT:w" s="T601">ilči</ts>
                  <nts id="Seg_2210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T603" id="Seg_2212" n="HIAT:w" s="T602">kerden</ts>
                  <nts id="Seg_2213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T604" id="Seg_2215" n="HIAT:w" s="T603">baran</ts>
                  <nts id="Seg_2216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T605" id="Seg_2218" n="HIAT:w" s="T604">uːga</ts>
                  <nts id="Seg_2219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T606" id="Seg_2221" n="HIAT:w" s="T605">bɨragan</ts>
                  <nts id="Seg_2222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T607" id="Seg_2224" n="HIAT:w" s="T606">keːspit</ts>
                  <nts id="Seg_2225" n="HIAT:ip">.</nts>
                  <nts id="Seg_2226" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T613" id="Seg_2228" n="HIAT:u" s="T607">
                  <ts e="T608" id="Seg_2230" n="HIAT:w" s="T607">ɨraːktaːgɨ</ts>
                  <nts id="Seg_2231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T609" id="Seg_2233" n="HIAT:w" s="T608">u͡ola</ts>
                  <nts id="Seg_2234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T610" id="Seg_2236" n="HIAT:w" s="T609">kɨːhɨran</ts>
                  <nts id="Seg_2237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T611" id="Seg_2239" n="HIAT:w" s="T610">haŋata</ts>
                  <nts id="Seg_2240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T612" id="Seg_2242" n="HIAT:w" s="T611">da</ts>
                  <nts id="Seg_2243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T613" id="Seg_2245" n="HIAT:w" s="T612">taksɨbatak</ts>
                  <nts id="Seg_2246" n="HIAT:ip">.</nts>
                  <nts id="Seg_2247" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T624" id="Seg_2249" n="HIAT:u" s="T613">
                  <ts e="T614" id="Seg_2251" n="HIAT:w" s="T613">Töhö</ts>
                  <nts id="Seg_2252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T615" id="Seg_2254" n="HIAT:w" s="T614">ör</ts>
                  <nts id="Seg_2255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T616" id="Seg_2257" n="HIAT:w" s="T615">bu͡olu͡oj</ts>
                  <nts id="Seg_2258" n="HIAT:ip">,</nts>
                  <nts id="Seg_2259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T617" id="Seg_2261" n="HIAT:w" s="T616">tɨːlara</ts>
                  <nts id="Seg_2262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T618" id="Seg_2264" n="HIAT:w" s="T617">uːga</ts>
                  <nts id="Seg_2265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T619" id="Seg_2267" n="HIAT:w" s="T618">hɨstan</ts>
                  <nts id="Seg_2268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T620" id="Seg_2270" n="HIAT:w" s="T619">kaːlbɨt</ts>
                  <nts id="Seg_2271" n="HIAT:ip">,</nts>
                  <nts id="Seg_2272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T621" id="Seg_2274" n="HIAT:w" s="T620">ebe</ts>
                  <nts id="Seg_2275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T622" id="Seg_2277" n="HIAT:w" s="T621">uːta</ts>
                  <nts id="Seg_2278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T623" id="Seg_2280" n="HIAT:w" s="T622">karaːrt</ts>
                  <nts id="Seg_2281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T624" id="Seg_2283" n="HIAT:w" s="T623">gɨmmɨt</ts>
                  <nts id="Seg_2284" n="HIAT:ip">.</nts>
                  <nts id="Seg_2285" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T630" id="Seg_2287" n="HIAT:u" s="T624">
                  <ts e="T625" id="Seg_2289" n="HIAT:w" s="T624">Bu</ts>
                  <nts id="Seg_2290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T626" id="Seg_2292" n="HIAT:w" s="T625">u͡olattar</ts>
                  <nts id="Seg_2293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T627" id="Seg_2295" n="HIAT:w" s="T626">hataːn</ts>
                  <nts id="Seg_2296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T628" id="Seg_2298" n="HIAT:w" s="T627">erdimmet</ts>
                  <nts id="Seg_2299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T629" id="Seg_2301" n="HIAT:w" s="T628">bu͡olan</ts>
                  <nts id="Seg_2302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T630" id="Seg_2304" n="HIAT:w" s="T629">kaːlbɨttar</ts>
                  <nts id="Seg_2305" n="HIAT:ip">.</nts>
                  <nts id="Seg_2306" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T631" id="Seg_2308" n="HIAT:u" s="T630">
                  <ts e="T631" id="Seg_2310" n="HIAT:w" s="T630">Kuttammɨttar</ts>
                  <nts id="Seg_2311" n="HIAT:ip">.</nts>
                  <nts id="Seg_2312" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T633" id="Seg_2314" n="HIAT:u" s="T631">
                  <ts e="T632" id="Seg_2316" n="HIAT:w" s="T631">ɨt</ts>
                  <nts id="Seg_2317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T633" id="Seg_2319" n="HIAT:w" s="T632">u͡ola</ts>
                  <nts id="Seg_2320" n="HIAT:ip">:</nts>
                  <nts id="Seg_2321" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T645" id="Seg_2323" n="HIAT:u" s="T633">
                  <nts id="Seg_2324" n="HIAT:ip">"</nts>
                  <ts e="T634" id="Seg_2326" n="HIAT:w" s="T633">Uːnu</ts>
                  <nts id="Seg_2327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T635" id="Seg_2329" n="HIAT:w" s="T634">hügennen</ts>
                  <nts id="Seg_2330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T636" id="Seg_2332" n="HIAT:w" s="T635">bɨhɨta</ts>
                  <nts id="Seg_2333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T637" id="Seg_2335" n="HIAT:w" s="T636">kerdiŋ</ts>
                  <nts id="Seg_2336" n="HIAT:ip">,</nts>
                  <nts id="Seg_2337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T638" id="Seg_2339" n="HIAT:w" s="T637">bahagɨnan</ts>
                  <nts id="Seg_2340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T639" id="Seg_2342" n="HIAT:w" s="T638">tehite</ts>
                  <nts id="Seg_2343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T640" id="Seg_2345" n="HIAT:w" s="T639">annʼɨŋ</ts>
                  <nts id="Seg_2346" n="HIAT:ip">,</nts>
                  <nts id="Seg_2347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T641" id="Seg_2349" n="HIAT:w" s="T640">min</ts>
                  <nts id="Seg_2350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T642" id="Seg_2352" n="HIAT:w" s="T641">erden</ts>
                  <nts id="Seg_2353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T643" id="Seg_2355" n="HIAT:w" s="T642">ihi͡em</ts>
                  <nts id="Seg_2356" n="HIAT:ip">"</nts>
                  <nts id="Seg_2357" n="HIAT:ip">,</nts>
                  <nts id="Seg_2358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T644" id="Seg_2360" n="HIAT:w" s="T643">dʼi͡ebit</ts>
                  <nts id="Seg_2361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T645" id="Seg_2363" n="HIAT:w" s="T644">dogottorugar</ts>
                  <nts id="Seg_2364" n="HIAT:ip">.</nts>
                  <nts id="Seg_2365" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T651" id="Seg_2367" n="HIAT:u" s="T645">
                  <ts e="T646" id="Seg_2369" n="HIAT:w" s="T645">Körbüttere</ts>
                  <nts id="Seg_2370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2371" n="HIAT:ip">—</nts>
                  <nts id="Seg_2372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T647" id="Seg_2374" n="HIAT:w" s="T646">uːlara</ts>
                  <nts id="Seg_2375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T648" id="Seg_2377" n="HIAT:w" s="T647">kirdik</ts>
                  <nts id="Seg_2378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T649" id="Seg_2380" n="HIAT:w" s="T648">hɨrdaːn</ts>
                  <nts id="Seg_2381" n="HIAT:ip">,</nts>
                  <nts id="Seg_2382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T650" id="Seg_2384" n="HIAT:w" s="T649">kɨtaran</ts>
                  <nts id="Seg_2385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T651" id="Seg_2387" n="HIAT:w" s="T650">ispit</ts>
                  <nts id="Seg_2388" n="HIAT:ip">.</nts>
                  <nts id="Seg_2389" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T654" id="Seg_2391" n="HIAT:u" s="T651">
                  <ts e="T652" id="Seg_2393" n="HIAT:w" s="T651">Hotoru</ts>
                  <nts id="Seg_2394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T653" id="Seg_2396" n="HIAT:w" s="T652">tɨːlara</ts>
                  <nts id="Seg_2397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T654" id="Seg_2399" n="HIAT:w" s="T653">boskolommut</ts>
                  <nts id="Seg_2400" n="HIAT:ip">.</nts>
                  <nts id="Seg_2401" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T658" id="Seg_2403" n="HIAT:u" s="T654">
                  <ts e="T655" id="Seg_2405" n="HIAT:w" s="T654">U͡olattar</ts>
                  <nts id="Seg_2406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T656" id="Seg_2408" n="HIAT:w" s="T655">erdinen</ts>
                  <nts id="Seg_2409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T657" id="Seg_2411" n="HIAT:w" s="T656">ispitter</ts>
                  <nts id="Seg_2412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T658" id="Seg_2414" n="HIAT:w" s="T657">orguːjakaːn</ts>
                  <nts id="Seg_2415" n="HIAT:ip">.</nts>
                  <nts id="Seg_2416" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T667" id="Seg_2418" n="HIAT:u" s="T658">
                  <ts e="T659" id="Seg_2420" n="HIAT:w" s="T658">Töhö</ts>
                  <nts id="Seg_2421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T660" id="Seg_2423" n="HIAT:w" s="T659">da</ts>
                  <nts id="Seg_2424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T661" id="Seg_2426" n="HIAT:w" s="T660">ör</ts>
                  <nts id="Seg_2427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T662" id="Seg_2429" n="HIAT:w" s="T661">bu͡olbakka</ts>
                  <nts id="Seg_2430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T663" id="Seg_2432" n="HIAT:w" s="T662">ebe</ts>
                  <nts id="Seg_2433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T664" id="Seg_2435" n="HIAT:w" s="T663">ürdük</ts>
                  <nts id="Seg_2436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T665" id="Seg_2438" n="HIAT:w" s="T664">kɨtɨlɨgar</ts>
                  <nts id="Seg_2439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T666" id="Seg_2441" n="HIAT:w" s="T665">gu͡orattara</ts>
                  <nts id="Seg_2442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T667" id="Seg_2444" n="HIAT:w" s="T666">köstübüt</ts>
                  <nts id="Seg_2445" n="HIAT:ip">.</nts>
                  <nts id="Seg_2446" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T674" id="Seg_2448" n="HIAT:u" s="T667">
                  <ts e="T668" id="Seg_2450" n="HIAT:w" s="T667">Tiksen</ts>
                  <nts id="Seg_2451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T669" id="Seg_2453" n="HIAT:w" s="T668">baran</ts>
                  <nts id="Seg_2454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T670" id="Seg_2456" n="HIAT:w" s="T669">u͡olattar</ts>
                  <nts id="Seg_2457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T671" id="Seg_2459" n="HIAT:w" s="T670">üs</ts>
                  <nts id="Seg_2460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T672" id="Seg_2462" n="HIAT:w" s="T671">aŋɨ</ts>
                  <nts id="Seg_2463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T673" id="Seg_2465" n="HIAT:w" s="T672">araksan</ts>
                  <nts id="Seg_2466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T674" id="Seg_2468" n="HIAT:w" s="T673">kaːlbɨttar</ts>
                  <nts id="Seg_2469" n="HIAT:ip">.</nts>
                  <nts id="Seg_2470" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T693" id="Seg_2472" n="HIAT:u" s="T674">
                  <ts e="T675" id="Seg_2474" n="HIAT:w" s="T674">ɨraːktaːgɨ</ts>
                  <nts id="Seg_2475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T676" id="Seg_2477" n="HIAT:w" s="T675">u͡ola</ts>
                  <nts id="Seg_2478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T677" id="Seg_2480" n="HIAT:w" s="T676">hɨːr</ts>
                  <nts id="Seg_2481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T678" id="Seg_2483" n="HIAT:w" s="T677">ürdüger</ts>
                  <nts id="Seg_2484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T679" id="Seg_2486" n="HIAT:w" s="T678">čeːlkeː</ts>
                  <nts id="Seg_2487" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T680" id="Seg_2489" n="HIAT:w" s="T679">dʼi͡ege</ts>
                  <nts id="Seg_2490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T681" id="Seg_2492" n="HIAT:w" s="T680">barbɨt</ts>
                  <nts id="Seg_2493" n="HIAT:ip">,</nts>
                  <nts id="Seg_2494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T682" id="Seg_2496" n="HIAT:w" s="T681">ɨt</ts>
                  <nts id="Seg_2497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T683" id="Seg_2499" n="HIAT:w" s="T682">u͡ola</ts>
                  <nts id="Seg_2500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T684" id="Seg_2502" n="HIAT:w" s="T683">maːmatɨn</ts>
                  <nts id="Seg_2503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T685" id="Seg_2505" n="HIAT:w" s="T684">nʼamčɨgas</ts>
                  <nts id="Seg_2506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T686" id="Seg_2508" n="HIAT:w" s="T685">koroːn</ts>
                  <nts id="Seg_2509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T687" id="Seg_2511" n="HIAT:w" s="T686">dʼi͡etiger</ts>
                  <nts id="Seg_2512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T688" id="Seg_2514" n="HIAT:w" s="T687">kiːrbit</ts>
                  <nts id="Seg_2515" n="HIAT:ip">,</nts>
                  <nts id="Seg_2516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T689" id="Seg_2518" n="HIAT:w" s="T688">koru͡oba</ts>
                  <nts id="Seg_2519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T690" id="Seg_2521" n="HIAT:w" s="T689">u͡ola</ts>
                  <nts id="Seg_2522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T691" id="Seg_2524" n="HIAT:w" s="T690">tahaːraː</ts>
                  <nts id="Seg_2525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T692" id="Seg_2527" n="HIAT:w" s="T691">inʼetin</ts>
                  <nts id="Seg_2528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T693" id="Seg_2530" n="HIAT:w" s="T692">körsübüt</ts>
                  <nts id="Seg_2531" n="HIAT:ip">.</nts>
                  <nts id="Seg_2532" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T697" id="Seg_2534" n="HIAT:u" s="T693">
                  <ts e="T694" id="Seg_2536" n="HIAT:w" s="T693">Harsi͡erda</ts>
                  <nts id="Seg_2537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T695" id="Seg_2539" n="HIAT:w" s="T694">ɨraːktaːgɨ</ts>
                  <nts id="Seg_2540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T696" id="Seg_2542" n="HIAT:w" s="T695">u͡olattarɨ</ts>
                  <nts id="Seg_2543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T697" id="Seg_2545" n="HIAT:w" s="T696">ɨgɨrtatalaːbɨt</ts>
                  <nts id="Seg_2546" n="HIAT:ip">:</nts>
                  <nts id="Seg_2547" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T705" id="Seg_2549" n="HIAT:u" s="T697">
                  <nts id="Seg_2550" n="HIAT:ip">"</nts>
                  <ts e="T698" id="Seg_2552" n="HIAT:w" s="T697">Če</ts>
                  <nts id="Seg_2553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T699" id="Seg_2555" n="HIAT:w" s="T698">ogolorum</ts>
                  <nts id="Seg_2556" n="HIAT:ip">,</nts>
                  <nts id="Seg_2557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T700" id="Seg_2559" n="HIAT:w" s="T699">kepseːŋ</ts>
                  <nts id="Seg_2560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T701" id="Seg_2562" n="HIAT:w" s="T700">tugu</ts>
                  <nts id="Seg_2563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T702" id="Seg_2565" n="HIAT:w" s="T701">gɨna</ts>
                  <nts id="Seg_2566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T703" id="Seg_2568" n="HIAT:w" s="T702">bu</ts>
                  <nts id="Seg_2569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T704" id="Seg_2571" n="HIAT:w" s="T703">turkarɨ</ts>
                  <nts id="Seg_2572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T705" id="Seg_2574" n="HIAT:w" s="T704">hɨldʼɨbɨkkɨtɨj</ts>
                  <nts id="Seg_2575" n="HIAT:ip">?</nts>
                  <nts id="Seg_2576" n="HIAT:ip">"</nts>
                  <nts id="Seg_2577" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T721" id="Seg_2579" n="HIAT:u" s="T705">
                  <nts id="Seg_2580" n="HIAT:ip">"</nts>
                  <ts e="T706" id="Seg_2582" n="HIAT:w" s="T705">Üs</ts>
                  <nts id="Seg_2583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T707" id="Seg_2585" n="HIAT:w" s="T706">abaːhɨ</ts>
                  <nts id="Seg_2586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T708" id="Seg_2588" n="HIAT:w" s="T707">irgetitten</ts>
                  <nts id="Seg_2589" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T709" id="Seg_2591" n="HIAT:w" s="T708">üs</ts>
                  <nts id="Seg_2592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T710" id="Seg_2594" n="HIAT:w" s="T709">huruktaːk</ts>
                  <nts id="Seg_2595" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T711" id="Seg_2597" n="HIAT:w" s="T710">taːhɨ</ts>
                  <nts id="Seg_2598" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T712" id="Seg_2600" n="HIAT:w" s="T711">bulbutum</ts>
                  <nts id="Seg_2601" n="HIAT:ip">"</nts>
                  <nts id="Seg_2602" n="HIAT:ip">,</nts>
                  <nts id="Seg_2603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T713" id="Seg_2605" n="HIAT:w" s="T712">ɨt</ts>
                  <nts id="Seg_2606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T714" id="Seg_2608" n="HIAT:w" s="T713">u͡ola</ts>
                  <nts id="Seg_2609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T715" id="Seg_2611" n="HIAT:w" s="T714">dʼi͡ebit</ts>
                  <nts id="Seg_2612" n="HIAT:ip">,</nts>
                  <nts id="Seg_2613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T716" id="Seg_2615" n="HIAT:w" s="T715">ɨraːktaːgɨ</ts>
                  <nts id="Seg_2616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T717" id="Seg_2618" n="HIAT:w" s="T716">iliːtiger</ts>
                  <nts id="Seg_2619" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T718" id="Seg_2621" n="HIAT:w" s="T717">üs</ts>
                  <nts id="Seg_2622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T719" id="Seg_2624" n="HIAT:w" s="T718">taːhɨ</ts>
                  <nts id="Seg_2625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T720" id="Seg_2627" n="HIAT:w" s="T719">tuttaran</ts>
                  <nts id="Seg_2628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T721" id="Seg_2630" n="HIAT:w" s="T720">keːspit</ts>
                  <nts id="Seg_2631" n="HIAT:ip">.</nts>
                  <nts id="Seg_2632" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T731" id="Seg_2634" n="HIAT:u" s="T721">
                  <ts e="T722" id="Seg_2636" n="HIAT:w" s="T721">ɨːraːktaːgɨ</ts>
                  <nts id="Seg_2637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T723" id="Seg_2639" n="HIAT:w" s="T722">ɨt</ts>
                  <nts id="Seg_2640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T724" id="Seg_2642" n="HIAT:w" s="T723">u͡olun</ts>
                  <nts id="Seg_2643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T725" id="Seg_2645" n="HIAT:w" s="T724">moːjuttan</ts>
                  <nts id="Seg_2646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T726" id="Seg_2648" n="HIAT:w" s="T725">kuːhan</ts>
                  <nts id="Seg_2649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T727" id="Seg_2651" n="HIAT:w" s="T726">ɨlbɨt</ts>
                  <nts id="Seg_2652" n="HIAT:ip">,</nts>
                  <nts id="Seg_2653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T728" id="Seg_2655" n="HIAT:w" s="T727">ɨtamsɨja</ts>
                  <nts id="Seg_2656" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T729" id="Seg_2658" n="HIAT:w" s="T728">tühen</ts>
                  <nts id="Seg_2659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T730" id="Seg_2661" n="HIAT:w" s="T729">baran</ts>
                  <nts id="Seg_2662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T731" id="Seg_2664" n="HIAT:w" s="T730">dʼi͡ebit</ts>
                  <nts id="Seg_2665" n="HIAT:ip">:</nts>
                  <nts id="Seg_2666" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T737" id="Seg_2668" n="HIAT:u" s="T731">
                  <nts id="Seg_2669" n="HIAT:ip">"</nts>
                  <ts e="T732" id="Seg_2671" n="HIAT:w" s="T731">Bügüŋŋütten</ts>
                  <nts id="Seg_2672" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T733" id="Seg_2674" n="HIAT:w" s="T732">en</ts>
                  <nts id="Seg_2675" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T734" id="Seg_2677" n="HIAT:w" s="T733">ɨraːktaːgɨ</ts>
                  <nts id="Seg_2678" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T735" id="Seg_2680" n="HIAT:w" s="T734">bu͡olar</ts>
                  <nts id="Seg_2681" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T736" id="Seg_2683" n="HIAT:w" s="T735">ɨjaːgɨŋ</ts>
                  <nts id="Seg_2684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T737" id="Seg_2686" n="HIAT:w" s="T736">kelle</ts>
                  <nts id="Seg_2687" n="HIAT:ip">.</nts>
                  <nts id="Seg_2688" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T742" id="Seg_2690" n="HIAT:u" s="T737">
                  <ts e="T738" id="Seg_2692" n="HIAT:w" s="T737">U͡olbun</ts>
                  <nts id="Seg_2693" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T739" id="Seg_2695" n="HIAT:w" s="T738">kɨtta</ts>
                  <nts id="Seg_2696" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T740" id="Seg_2698" n="HIAT:w" s="T739">ubaj-balɨs</ts>
                  <nts id="Seg_2699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T741" id="Seg_2701" n="HIAT:w" s="T740">bu͡olan</ts>
                  <nts id="Seg_2702" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T742" id="Seg_2704" n="HIAT:w" s="T741">oloruŋ</ts>
                  <nts id="Seg_2705" n="HIAT:ip">.</nts>
                  <nts id="Seg_2706" n="HIAT:ip">"</nts>
                  <nts id="Seg_2707" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T747" id="Seg_2709" n="HIAT:u" s="T742">
                  <ts e="T743" id="Seg_2711" n="HIAT:w" s="T742">Üs</ts>
                  <nts id="Seg_2712" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T744" id="Seg_2714" n="HIAT:w" s="T743">u͡ol</ts>
                  <nts id="Seg_2715" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T745" id="Seg_2717" n="HIAT:w" s="T744">dogorduː</ts>
                  <nts id="Seg_2718" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T746" id="Seg_2720" n="HIAT:w" s="T745">bajan-tajan</ts>
                  <nts id="Seg_2721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T747" id="Seg_2723" n="HIAT:w" s="T746">olorbuttar</ts>
                  <nts id="Seg_2724" n="HIAT:ip">.</nts>
                  <nts id="Seg_2725" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T747" id="Seg_2726" n="sc" s="T0">
               <ts e="T1" id="Seg_2728" n="e" s="T0">Bɨlɨr-bɨlɨr </ts>
               <ts e="T2" id="Seg_2730" n="e" s="T1">biːr </ts>
               <ts e="T3" id="Seg_2732" n="e" s="T2">ɨraːktaːgɨ </ts>
               <ts e="T4" id="Seg_2734" n="e" s="T3">olorbut. </ts>
               <ts e="T5" id="Seg_2736" n="e" s="T4">Ogoto </ts>
               <ts e="T6" id="Seg_2738" n="e" s="T5">hu͡ok </ts>
               <ts e="T7" id="Seg_2740" n="e" s="T6">ete </ts>
               <ts e="T8" id="Seg_2742" n="e" s="T7">ebit. </ts>
               <ts e="T9" id="Seg_2744" n="e" s="T8">Biːrde </ts>
               <ts e="T10" id="Seg_2746" n="e" s="T9">bu </ts>
               <ts e="T11" id="Seg_2748" n="e" s="T10">ɨraːktaːgɨ </ts>
               <ts e="T12" id="Seg_2750" n="e" s="T11">hanaːbɨt: </ts>
               <ts e="T13" id="Seg_2752" n="e" s="T12">"Bu </ts>
               <ts e="T14" id="Seg_2754" n="e" s="T13">togo </ts>
               <ts e="T15" id="Seg_2756" n="e" s="T14">bihigi </ts>
               <ts e="T16" id="Seg_2758" n="e" s="T15">ogoto </ts>
               <ts e="T17" id="Seg_2760" n="e" s="T16">hu͡okputuj? </ts>
               <ts e="T18" id="Seg_2762" n="e" s="T17">Kɨrdʼagastartan </ts>
               <ts e="T19" id="Seg_2764" n="e" s="T18">ɨjɨppɨt </ts>
               <ts e="T20" id="Seg_2766" n="e" s="T19">kihi." </ts>
               <ts e="T21" id="Seg_2768" n="e" s="T20">Bu </ts>
               <ts e="T22" id="Seg_2770" n="e" s="T21">hanɨː </ts>
               <ts e="T23" id="Seg_2772" n="e" s="T22">olordoguna, </ts>
               <ts e="T24" id="Seg_2774" n="e" s="T23">dʼaktara </ts>
               <ts e="T25" id="Seg_2776" n="e" s="T24">kiːrbit: </ts>
               <ts e="T26" id="Seg_2778" n="e" s="T25">"Tugu </ts>
               <ts e="T27" id="Seg_2780" n="e" s="T26">hanaːrgaːtɨŋ, </ts>
               <ts e="T28" id="Seg_2782" n="e" s="T27">dogoː?" </ts>
               <ts e="T29" id="Seg_2784" n="e" s="T28">Harsɨːn </ts>
               <ts e="T30" id="Seg_2786" n="e" s="T29">bihigi </ts>
               <ts e="T31" id="Seg_2788" n="e" s="T30">kihileri </ts>
               <ts e="T32" id="Seg_2790" n="e" s="T31">komuju͡okput. </ts>
               <ts e="T33" id="Seg_2792" n="e" s="T32">Kanɨga </ts>
               <ts e="T34" id="Seg_2794" n="e" s="T33">dʼi͡eri </ts>
               <ts e="T35" id="Seg_2796" n="e" s="T34">iččitek </ts>
               <ts e="T36" id="Seg_2798" n="e" s="T35">isti͡eneni </ts>
               <ts e="T37" id="Seg_2800" n="e" s="T36">körön </ts>
               <ts e="T38" id="Seg_2802" n="e" s="T37">oloru͡okputuj, </ts>
               <ts e="T39" id="Seg_2804" n="e" s="T38">ogoto </ts>
               <ts e="T40" id="Seg_2806" n="e" s="T39">hu͡ok? </ts>
               <ts e="T41" id="Seg_2808" n="e" s="T40">ɨjɨtɨ͡ak </ts>
               <ts e="T42" id="Seg_2810" n="e" s="T41">togo </ts>
               <ts e="T43" id="Seg_2812" n="e" s="T42">ogoto </ts>
               <ts e="T44" id="Seg_2814" n="e" s="T43">hu͡okputuj? </ts>
               <ts e="T45" id="Seg_2816" n="e" s="T44">Kimi͡eke </ts>
               <ts e="T46" id="Seg_2818" n="e" s="T45">baːjbɨtɨn </ts>
               <ts e="T47" id="Seg_2820" n="e" s="T46">bi͡eri͡ekpitij, </ts>
               <ts e="T48" id="Seg_2822" n="e" s="T47">öllökpütüne?" </ts>
               <ts e="T49" id="Seg_2824" n="e" s="T48">Harsi͡erda </ts>
               <ts e="T50" id="Seg_2826" n="e" s="T49">turbuttara, </ts>
               <ts e="T51" id="Seg_2828" n="e" s="T50">ügüːs </ts>
               <ts e="T52" id="Seg_2830" n="e" s="T51">bagajɨ </ts>
               <ts e="T53" id="Seg_2832" n="e" s="T52">kihi </ts>
               <ts e="T54" id="Seg_2834" n="e" s="T53">komullubut. </ts>
               <ts e="T55" id="Seg_2836" n="e" s="T54">Onno </ts>
               <ts e="T56" id="Seg_2838" n="e" s="T55">ɨraːktaːgɨ </ts>
               <ts e="T57" id="Seg_2840" n="e" s="T56">ɨjɨppɨt: </ts>
               <ts e="T58" id="Seg_2842" n="e" s="T57">"Bu </ts>
               <ts e="T59" id="Seg_2844" n="e" s="T58">togo </ts>
               <ts e="T60" id="Seg_2846" n="e" s="T59">bihigi </ts>
               <ts e="T61" id="Seg_2848" n="e" s="T60">ogoto </ts>
               <ts e="T62" id="Seg_2850" n="e" s="T61">hu͡okputuj? </ts>
               <ts e="T63" id="Seg_2852" n="e" s="T62">Kajtak, </ts>
               <ts e="T64" id="Seg_2854" n="e" s="T63">tugu </ts>
               <ts e="T65" id="Seg_2856" n="e" s="T64">gɨnnakka, </ts>
               <ts e="T66" id="Seg_2858" n="e" s="T65">ogolonu͡okputuj, </ts>
               <ts e="T67" id="Seg_2860" n="e" s="T66">ehigi </ts>
               <ts e="T68" id="Seg_2862" n="e" s="T67">kajtak </ts>
               <ts e="T69" id="Seg_2864" n="e" s="T68">dʼiː </ts>
               <ts e="T70" id="Seg_2866" n="e" s="T69">hanɨːgɨt?" </ts>
               <ts e="T71" id="Seg_2868" n="e" s="T70">Kihiler </ts>
               <ts e="T72" id="Seg_2870" n="e" s="T71">ajdaːja </ts>
               <ts e="T73" id="Seg_2872" n="e" s="T72">tüspütter. </ts>
               <ts e="T74" id="Seg_2874" n="e" s="T73">Aːnɨnan </ts>
               <ts e="T75" id="Seg_2876" n="e" s="T74">kɨrdʼagas </ts>
               <ts e="T76" id="Seg_2878" n="e" s="T75">hokkor </ts>
               <ts e="T77" id="Seg_2880" n="e" s="T76">ogonnʼor </ts>
               <ts e="T78" id="Seg_2882" n="e" s="T77">bɨltas </ts>
               <ts e="T79" id="Seg_2884" n="e" s="T78">gɨmmɨt. </ts>
               <ts e="T80" id="Seg_2886" n="e" s="T79">Hokkor </ts>
               <ts e="T81" id="Seg_2888" n="e" s="T80">erejdeːgi </ts>
               <ts e="T82" id="Seg_2890" n="e" s="T81">ortogo </ts>
               <ts e="T83" id="Seg_2892" n="e" s="T82">üten </ts>
               <ts e="T84" id="Seg_2894" n="e" s="T83">keːspitter: </ts>
               <ts e="T85" id="Seg_2896" n="e" s="T84">"Bu </ts>
               <ts e="T86" id="Seg_2898" n="e" s="T85">baːr </ts>
               <ts e="T87" id="Seg_2900" n="e" s="T86">biler </ts>
               <ts e="T88" id="Seg_2902" n="e" s="T87">kihi." </ts>
               <ts e="T89" id="Seg_2904" n="e" s="T88">Ogonnʼor </ts>
               <ts e="T90" id="Seg_2906" n="e" s="T89">örö </ts>
               <ts e="T91" id="Seg_2908" n="e" s="T90">körön </ts>
               <ts e="T92" id="Seg_2910" n="e" s="T91">baraːn, </ts>
               <ts e="T93" id="Seg_2912" n="e" s="T92">his </ts>
               <ts e="T94" id="Seg_2914" n="e" s="T93">tuttan </ts>
               <ts e="T95" id="Seg_2916" n="e" s="T94">baraːn, </ts>
               <ts e="T96" id="Seg_2918" n="e" s="T95">dʼi͡ebit: </ts>
               <ts e="T97" id="Seg_2920" n="e" s="T96">"Oŋoruŋ </ts>
               <ts e="T98" id="Seg_2922" n="e" s="T97">kömüs </ts>
               <ts e="T99" id="Seg_2924" n="e" s="T98">ilimne, </ts>
               <ts e="T100" id="Seg_2926" n="e" s="T99">kömüs </ts>
               <ts e="T101" id="Seg_2928" n="e" s="T100">tɨːta, </ts>
               <ts e="T102" id="Seg_2930" n="e" s="T101">kömüs </ts>
               <ts e="T103" id="Seg_2932" n="e" s="T102">erdiːte. </ts>
               <ts e="T104" id="Seg_2934" n="e" s="T103">Onton </ts>
               <ts e="T105" id="Seg_2936" n="e" s="T104">ɨraːktaːgɨ </ts>
               <ts e="T106" id="Seg_2938" n="e" s="T105">balɨktɨː </ts>
               <ts e="T107" id="Seg_2940" n="e" s="T106">bardɨn. </ts>
               <ts e="T108" id="Seg_2942" n="e" s="T107">Ilimiger </ts>
               <ts e="T109" id="Seg_2944" n="e" s="T108">balɨk </ts>
               <ts e="T110" id="Seg_2946" n="e" s="T109">tutu͡o, </ts>
               <ts e="T111" id="Seg_2948" n="e" s="T110">onu </ts>
               <ts e="T112" id="Seg_2950" n="e" s="T111">dʼaktara </ts>
               <ts e="T113" id="Seg_2952" n="e" s="T112">hi͡etin." </ts>
               <ts e="T114" id="Seg_2954" n="e" s="T113">ɨraːktaːgɨ </ts>
               <ts e="T115" id="Seg_2956" n="e" s="T114">dʼaktara </ts>
               <ts e="T116" id="Seg_2958" n="e" s="T115">külbüt: </ts>
               <ts e="T117" id="Seg_2960" n="e" s="T116">"Onton </ts>
               <ts e="T118" id="Seg_2962" n="e" s="T117">min </ts>
               <ts e="T119" id="Seg_2964" n="e" s="T118">totu͡om </ts>
               <ts e="T120" id="Seg_2966" n="e" s="T119">du͡o?" </ts>
               <ts e="T121" id="Seg_2968" n="e" s="T120">Ör </ts>
               <ts e="T122" id="Seg_2970" n="e" s="T121">bu͡olu͡oj, </ts>
               <ts e="T123" id="Seg_2972" n="e" s="T122">ɨraːktaːgɨ </ts>
               <ts e="T124" id="Seg_2974" n="e" s="T123">harsɨŋŋɨtɨn </ts>
               <ts e="T125" id="Seg_2976" n="e" s="T124">kömüs </ts>
               <ts e="T126" id="Seg_2978" n="e" s="T125">tɨːlaːk, </ts>
               <ts e="T127" id="Seg_2980" n="e" s="T126">kömüs </ts>
               <ts e="T128" id="Seg_2982" n="e" s="T127">erdiːleːk </ts>
               <ts e="T129" id="Seg_2984" n="e" s="T128">balɨktana </ts>
               <ts e="T130" id="Seg_2986" n="e" s="T129">barbɨt. </ts>
               <ts e="T131" id="Seg_2988" n="e" s="T130">Biːr </ts>
               <ts e="T132" id="Seg_2990" n="e" s="T131">balɨgɨ </ts>
               <ts e="T133" id="Seg_2992" n="e" s="T132">ilibireten </ts>
               <ts e="T134" id="Seg_2994" n="e" s="T133">tɨːnnaːktɨː </ts>
               <ts e="T135" id="Seg_2996" n="e" s="T134">egelbet </ts>
               <ts e="T136" id="Seg_2998" n="e" s="T135">duː. </ts>
               <ts e="T137" id="Seg_3000" n="e" s="T136">"Bu </ts>
               <ts e="T138" id="Seg_3002" n="e" s="T137">bultum", </ts>
               <ts e="T139" id="Seg_3004" n="e" s="T138">dʼi͡ebit </ts>
               <ts e="T140" id="Seg_3006" n="e" s="T139">da </ts>
               <ts e="T141" id="Seg_3008" n="e" s="T140">asčɨttarɨgar </ts>
               <ts e="T142" id="Seg_3010" n="e" s="T141">bi͡eren </ts>
               <ts e="T143" id="Seg_3012" n="e" s="T142">kebispit. </ts>
               <ts e="T144" id="Seg_3014" n="e" s="T143">"Hanɨkaːn </ts>
               <ts e="T145" id="Seg_3016" n="e" s="T144">kü͡östeːŋ!" </ts>
               <ts e="T146" id="Seg_3018" n="e" s="T145">Asčɨt </ts>
               <ts e="T147" id="Seg_3020" n="e" s="T146">balɨk </ts>
               <ts e="T148" id="Seg_3022" n="e" s="T147">katɨrɨgɨn </ts>
               <ts e="T149" id="Seg_3024" n="e" s="T148">ot </ts>
               <ts e="T150" id="Seg_3026" n="e" s="T149">ihiger </ts>
               <ts e="T151" id="Seg_3028" n="e" s="T150">ɨraːstaːn </ts>
               <ts e="T152" id="Seg_3030" n="e" s="T151">keːspit — </ts>
               <ts e="T153" id="Seg_3032" n="e" s="T152">onu </ts>
               <ts e="T154" id="Seg_3034" n="e" s="T153">ɨraːktaːgɨ </ts>
               <ts e="T155" id="Seg_3036" n="e" s="T154">koru͡obata </ts>
               <ts e="T156" id="Seg_3038" n="e" s="T155">hi͡en </ts>
               <ts e="T157" id="Seg_3040" n="e" s="T156">keːspit. </ts>
               <ts e="T158" id="Seg_3042" n="e" s="T157">ɨraːktaːgɨ </ts>
               <ts e="T159" id="Seg_3044" n="e" s="T158">dʼaktara </ts>
               <ts e="T160" id="Seg_3046" n="e" s="T159">balɨgɨ </ts>
               <ts e="T161" id="Seg_3048" n="e" s="T160">hi͡en </ts>
               <ts e="T162" id="Seg_3050" n="e" s="T161">baran </ts>
               <ts e="T163" id="Seg_3052" n="e" s="T162">oŋu͡ogun </ts>
               <ts e="T164" id="Seg_3054" n="e" s="T163">teri͡elkege </ts>
               <ts e="T165" id="Seg_3056" n="e" s="T164">kaːllarbɨtɨn </ts>
               <ts e="T166" id="Seg_3058" n="e" s="T165">asčɨt </ts>
               <ts e="T167" id="Seg_3060" n="e" s="T166">ɨkka </ts>
               <ts e="T168" id="Seg_3062" n="e" s="T167">bɨrakpɨt. </ts>
               <ts e="T169" id="Seg_3064" n="e" s="T168">Oloŋkogo </ts>
               <ts e="T170" id="Seg_3066" n="e" s="T169">ör </ts>
               <ts e="T171" id="Seg_3068" n="e" s="T170">bu͡olu͡o </ts>
               <ts e="T172" id="Seg_3070" n="e" s="T171">du͡o? </ts>
               <ts e="T173" id="Seg_3072" n="e" s="T172">Ühü͡önnere </ts>
               <ts e="T174" id="Seg_3074" n="e" s="T173">üs </ts>
               <ts e="T175" id="Seg_3076" n="e" s="T174">u͡olu </ts>
               <ts e="T176" id="Seg_3078" n="e" s="T175">töröːbütter. </ts>
               <ts e="T177" id="Seg_3080" n="e" s="T176">Ogolor </ts>
               <ts e="T178" id="Seg_3082" n="e" s="T177">töhö </ts>
               <ts e="T179" id="Seg_3084" n="e" s="T178">da </ts>
               <ts e="T180" id="Seg_3086" n="e" s="T179">bu͡olbakka </ts>
               <ts e="T181" id="Seg_3088" n="e" s="T180">biːrge </ts>
               <ts e="T182" id="Seg_3090" n="e" s="T181">oːnnʼuːr </ts>
               <ts e="T183" id="Seg_3092" n="e" s="T182">bu͡olbuttar. </ts>
               <ts e="T184" id="Seg_3094" n="e" s="T183">Biːrde </ts>
               <ts e="T185" id="Seg_3096" n="e" s="T184">araj </ts>
               <ts e="T186" id="Seg_3098" n="e" s="T185">ɨraːktaːgɨ </ts>
               <ts e="T187" id="Seg_3100" n="e" s="T186">u͡ola </ts>
               <ts e="T188" id="Seg_3102" n="e" s="T187">dʼi͡ebit: </ts>
               <ts e="T189" id="Seg_3104" n="e" s="T188">"Dogottor, </ts>
               <ts e="T190" id="Seg_3106" n="e" s="T189">tɨːlana </ts>
               <ts e="T191" id="Seg_3108" n="e" s="T190">barɨ͡agɨŋ." </ts>
               <ts e="T192" id="Seg_3110" n="e" s="T191">U͡olattar </ts>
               <ts e="T193" id="Seg_3112" n="e" s="T192">töhö </ts>
               <ts e="T194" id="Seg_3114" n="e" s="T193">ör </ts>
               <ts e="T195" id="Seg_3116" n="e" s="T194">erden </ts>
               <ts e="T196" id="Seg_3118" n="e" s="T195">ispittere </ts>
               <ts e="T197" id="Seg_3120" n="e" s="T196">bu͡olla. </ts>
               <ts e="T198" id="Seg_3122" n="e" s="T197">Körbüttere, </ts>
               <ts e="T199" id="Seg_3124" n="e" s="T198">ebe </ts>
               <ts e="T200" id="Seg_3126" n="e" s="T199">onu͡or </ts>
               <ts e="T201" id="Seg_3128" n="e" s="T200">ulakan </ts>
               <ts e="T202" id="Seg_3130" n="e" s="T201">bagajɨ </ts>
               <ts e="T203" id="Seg_3132" n="e" s="T202">balagan </ts>
               <ts e="T204" id="Seg_3134" n="e" s="T203">dʼi͡e </ts>
               <ts e="T205" id="Seg_3136" n="e" s="T204">turar. </ts>
               <ts e="T206" id="Seg_3138" n="e" s="T205">"Iti </ts>
               <ts e="T207" id="Seg_3140" n="e" s="T206">tu͡okputuj", </ts>
               <ts e="T208" id="Seg_3142" n="e" s="T207">despitter. </ts>
               <ts e="T209" id="Seg_3144" n="e" s="T208">ɨraːktaːgɨ </ts>
               <ts e="T210" id="Seg_3146" n="e" s="T209">u͡ola: </ts>
               <ts e="T211" id="Seg_3148" n="e" s="T210">"Tiksi͡egiŋ, </ts>
               <ts e="T212" id="Seg_3150" n="e" s="T211">kaːrtɨlɨ͡akpɨt", </ts>
               <ts e="T213" id="Seg_3152" n="e" s="T212">dʼi͡ebit. </ts>
               <ts e="T214" id="Seg_3154" n="e" s="T213">ɨt </ts>
               <ts e="T215" id="Seg_3156" n="e" s="T214">u͡ola: </ts>
               <ts e="T216" id="Seg_3158" n="e" s="T215">"E, </ts>
               <ts e="T217" id="Seg_3160" n="e" s="T216">togo? </ts>
               <ts e="T218" id="Seg_3162" n="e" s="T217">Bagar, </ts>
               <ts e="T219" id="Seg_3164" n="e" s="T218">abaːhɨlar </ts>
               <ts e="T220" id="Seg_3166" n="e" s="T219">dʼi͡elere </ts>
               <ts e="T221" id="Seg_3168" n="e" s="T220">bu͡olu͡o?" </ts>
               <ts e="T222" id="Seg_3170" n="e" s="T221">Koru͡oba </ts>
               <ts e="T223" id="Seg_3172" n="e" s="T222">u͡ola: </ts>
               <ts e="T224" id="Seg_3174" n="e" s="T223">"Tiksi͡egiŋ, </ts>
               <ts e="T225" id="Seg_3176" n="e" s="T224">tiksi͡egiŋ. </ts>
               <ts e="T226" id="Seg_3178" n="e" s="T225">Kaːmalɨː </ts>
               <ts e="T227" id="Seg_3180" n="e" s="T226">tühü͡ögüŋ." </ts>
               <ts e="T228" id="Seg_3182" n="e" s="T227">Kɨtɨlga </ts>
               <ts e="T229" id="Seg_3184" n="e" s="T228">taksaːt, </ts>
               <ts e="T230" id="Seg_3186" n="e" s="T229">bu </ts>
               <ts e="T231" id="Seg_3188" n="e" s="T230">ikki </ts>
               <ts e="T232" id="Seg_3190" n="e" s="T231">u͡ol </ts>
               <ts e="T233" id="Seg_3192" n="e" s="T232">kaːrtɨlɨː </ts>
               <ts e="T234" id="Seg_3194" n="e" s="T233">olorbuttar. </ts>
               <ts e="T235" id="Seg_3196" n="e" s="T234">ɨt </ts>
               <ts e="T236" id="Seg_3198" n="e" s="T235">u͡ola </ts>
               <ts e="T237" id="Seg_3200" n="e" s="T236">utujan </ts>
               <ts e="T238" id="Seg_3202" n="e" s="T237">kaːlbɨt. </ts>
               <ts e="T239" id="Seg_3204" n="e" s="T238">Töhö </ts>
               <ts e="T240" id="Seg_3206" n="e" s="T239">ör </ts>
               <ts e="T241" id="Seg_3208" n="e" s="T240">oːnnʼoːbuttara </ts>
               <ts e="T242" id="Seg_3210" n="e" s="T241">bu͡olla, </ts>
               <ts e="T243" id="Seg_3212" n="e" s="T242">bu </ts>
               <ts e="T244" id="Seg_3214" n="e" s="T243">ogolor </ts>
               <ts e="T245" id="Seg_3216" n="e" s="T244">emi͡e </ts>
               <ts e="T246" id="Seg_3218" n="e" s="T245">utujan </ts>
               <ts e="T247" id="Seg_3220" n="e" s="T246">kaːlbɨttar. </ts>
               <ts e="T248" id="Seg_3222" n="e" s="T247">ɨt </ts>
               <ts e="T249" id="Seg_3224" n="e" s="T248">u͡ola </ts>
               <ts e="T250" id="Seg_3226" n="e" s="T249">tüːn </ts>
               <ts e="T251" id="Seg_3228" n="e" s="T250">orto </ts>
               <ts e="T252" id="Seg_3230" n="e" s="T251">uhuktubuta — </ts>
               <ts e="T253" id="Seg_3232" n="e" s="T252">kihi </ts>
               <ts e="T254" id="Seg_3234" n="e" s="T253">kaːmar </ts>
               <ts e="T255" id="Seg_3236" n="e" s="T254">tɨ͡aha </ts>
               <ts e="T256" id="Seg_3238" n="e" s="T255">ihillibit. </ts>
               <ts e="T257" id="Seg_3240" n="e" s="T256">Taksɨbɨta — </ts>
               <ts e="T258" id="Seg_3242" n="e" s="T257">abaːhɨ, </ts>
               <ts e="T259" id="Seg_3244" n="e" s="T258">balagan </ts>
               <ts e="T260" id="Seg_3246" n="e" s="T259">dʼi͡ek </ts>
               <ts e="T261" id="Seg_3248" n="e" s="T260">kaːman </ts>
               <ts e="T262" id="Seg_3250" n="e" s="T261">iheːktiːr </ts>
               <ts e="T263" id="Seg_3252" n="e" s="T262">dʼe. </ts>
               <ts e="T264" id="Seg_3254" n="e" s="T263">"O </ts>
               <ts e="T265" id="Seg_3256" n="e" s="T264">dʼe, </ts>
               <ts e="T266" id="Seg_3258" n="e" s="T265">ölbüppüt </ts>
               <ts e="T267" id="Seg_3260" n="e" s="T266">ebit", </ts>
               <ts e="T268" id="Seg_3262" n="e" s="T267">dʼi͡en </ts>
               <ts e="T269" id="Seg_3264" n="e" s="T268">baran </ts>
               <ts e="T270" id="Seg_3266" n="e" s="T269">dogottorun </ts>
               <ts e="T271" id="Seg_3268" n="e" s="T270">uhugunnara </ts>
               <ts e="T272" id="Seg_3270" n="e" s="T271">hataːbɨt. </ts>
               <ts e="T273" id="Seg_3272" n="e" s="T272">Bu </ts>
               <ts e="T274" id="Seg_3274" n="e" s="T273">u͡olattar </ts>
               <ts e="T275" id="Seg_3276" n="e" s="T274">togo </ts>
               <ts e="T276" id="Seg_3278" n="e" s="T275">uhuktu͡oktaraj. </ts>
               <ts e="T277" id="Seg_3280" n="e" s="T276">"Kel-kel, </ts>
               <ts e="T278" id="Seg_3282" n="e" s="T277">min </ts>
               <ts e="T279" id="Seg_3284" n="e" s="T278">enʼigin </ts>
               <ts e="T280" id="Seg_3286" n="e" s="T279">hi͡em", </ts>
               <ts e="T281" id="Seg_3288" n="e" s="T280">abaːhɨ </ts>
               <ts e="T282" id="Seg_3290" n="e" s="T281">haŋata </ts>
               <ts e="T283" id="Seg_3292" n="e" s="T282">ihillibit. </ts>
               <ts e="T284" id="Seg_3294" n="e" s="T283">Bu </ts>
               <ts e="T285" id="Seg_3296" n="e" s="T284">u͡ol </ts>
               <ts e="T286" id="Seg_3298" n="e" s="T285">taksa </ts>
               <ts e="T287" id="Seg_3300" n="e" s="T286">köppüt </ts>
               <ts e="T288" id="Seg_3302" n="e" s="T287">da </ts>
               <ts e="T289" id="Seg_3304" n="e" s="T288">abaːhɨnɨ </ts>
               <ts e="T290" id="Seg_3306" n="e" s="T289">gɨtta </ts>
               <ts e="T291" id="Seg_3308" n="e" s="T290">ölörsübüt. </ts>
               <ts e="T292" id="Seg_3310" n="e" s="T291">Töhö </ts>
               <ts e="T293" id="Seg_3312" n="e" s="T292">da </ts>
               <ts e="T294" id="Seg_3314" n="e" s="T293">bu͡olbakka </ts>
               <ts e="T295" id="Seg_3316" n="e" s="T294">abaːhɨ </ts>
               <ts e="T296" id="Seg_3318" n="e" s="T295">menʼiːtin </ts>
               <ts e="T297" id="Seg_3320" n="e" s="T296">boldok </ts>
               <ts e="T298" id="Seg_3322" n="e" s="T297">taːska </ts>
               <ts e="T299" id="Seg_3324" n="e" s="T298">kaja </ts>
               <ts e="T300" id="Seg_3326" n="e" s="T299">oksubut. </ts>
               <ts e="T301" id="Seg_3328" n="e" s="T300">Ol </ts>
               <ts e="T302" id="Seg_3330" n="e" s="T301">gɨnan </ts>
               <ts e="T303" id="Seg_3332" n="e" s="T302">baran </ts>
               <ts e="T304" id="Seg_3334" n="e" s="T303">irgetin </ts>
               <ts e="T305" id="Seg_3336" n="e" s="T304">ihitten </ts>
               <ts e="T306" id="Seg_3338" n="e" s="T305">huruktaːk </ts>
               <ts e="T307" id="Seg_3340" n="e" s="T306">taːhɨ </ts>
               <ts e="T308" id="Seg_3342" n="e" s="T307">oroːbut, </ts>
               <ts e="T309" id="Seg_3344" n="e" s="T308">taːhɨ </ts>
               <ts e="T310" id="Seg_3346" n="e" s="T309">karmaːnɨn </ts>
               <ts e="T311" id="Seg_3348" n="e" s="T310">ihinen </ts>
               <ts e="T312" id="Seg_3350" n="e" s="T311">uktan </ts>
               <ts e="T313" id="Seg_3352" n="e" s="T312">keːspit. </ts>
               <ts e="T314" id="Seg_3354" n="e" s="T313">Harsi͡erda </ts>
               <ts e="T315" id="Seg_3356" n="e" s="T314">tu͡ok </ts>
               <ts e="T316" id="Seg_3358" n="e" s="T315">da </ts>
               <ts e="T317" id="Seg_3360" n="e" s="T316">bu͡olbatak </ts>
               <ts e="T318" id="Seg_3362" n="e" s="T317">kördük </ts>
               <ts e="T319" id="Seg_3364" n="e" s="T318">ɨt </ts>
               <ts e="T320" id="Seg_3366" n="e" s="T319">u͡ola </ts>
               <ts e="T321" id="Seg_3368" n="e" s="T320">dogottorun </ts>
               <ts e="T322" id="Seg_3370" n="e" s="T321">uhugunnartaːbɨt: </ts>
               <ts e="T323" id="Seg_3372" n="e" s="T322">"Čej, </ts>
               <ts e="T324" id="Seg_3374" n="e" s="T323">kaːstana </ts>
               <ts e="T325" id="Seg_3376" n="e" s="T324">barɨ͡agɨŋ." </ts>
               <ts e="T326" id="Seg_3378" n="e" s="T325">U͡olattar </ts>
               <ts e="T327" id="Seg_3380" n="e" s="T326">tɨːllaŋnaːn </ts>
               <ts e="T328" id="Seg_3382" n="e" s="T327">arɨːččɨ </ts>
               <ts e="T329" id="Seg_3384" n="e" s="T328">turan </ts>
               <ts e="T330" id="Seg_3386" n="e" s="T329">čaːj </ts>
               <ts e="T331" id="Seg_3388" n="e" s="T330">ispitter. </ts>
               <ts e="T332" id="Seg_3390" n="e" s="T331">Oloŋkogo </ts>
               <ts e="T333" id="Seg_3392" n="e" s="T332">kün </ts>
               <ts e="T334" id="Seg_3394" n="e" s="T333">kɨlgas. </ts>
               <ts e="T335" id="Seg_3396" n="e" s="T334">Töhö </ts>
               <ts e="T336" id="Seg_3398" n="e" s="T335">da </ts>
               <ts e="T337" id="Seg_3400" n="e" s="T336">bu͡olbakka </ts>
               <ts e="T338" id="Seg_3402" n="e" s="T337">kallaːn </ts>
               <ts e="T339" id="Seg_3404" n="e" s="T338">karaŋarbɨt. </ts>
               <ts e="T340" id="Seg_3406" n="e" s="T339">ɨt </ts>
               <ts e="T341" id="Seg_3408" n="e" s="T340">u͡ola </ts>
               <ts e="T342" id="Seg_3410" n="e" s="T341">ahaːn-hi͡en </ts>
               <ts e="T343" id="Seg_3412" n="e" s="T342">baran </ts>
               <ts e="T344" id="Seg_3414" n="e" s="T343">utujan </ts>
               <ts e="T345" id="Seg_3416" n="e" s="T344">kaːlbɨt, </ts>
               <ts e="T346" id="Seg_3418" n="e" s="T345">dogottoro </ts>
               <ts e="T347" id="Seg_3420" n="e" s="T346">emi͡e </ts>
               <ts e="T348" id="Seg_3422" n="e" s="T347">kaːrtɨlɨː </ts>
               <ts e="T349" id="Seg_3424" n="e" s="T348">olorbuttar. </ts>
               <ts e="T350" id="Seg_3426" n="e" s="T349">Tüːn </ts>
               <ts e="T351" id="Seg_3428" n="e" s="T350">orto </ts>
               <ts e="T352" id="Seg_3430" n="e" s="T351">uhuktubuta — </ts>
               <ts e="T353" id="Seg_3432" n="e" s="T352">dogottoro </ts>
               <ts e="T354" id="Seg_3434" n="e" s="T353">olordo </ts>
               <ts e="T355" id="Seg_3436" n="e" s="T354">olorbutunan </ts>
               <ts e="T356" id="Seg_3438" n="e" s="T355">utujan </ts>
               <ts e="T357" id="Seg_3440" n="e" s="T356">kaːlbɨttar. </ts>
               <ts e="T358" id="Seg_3442" n="e" s="T357">Bu </ts>
               <ts e="T359" id="Seg_3444" n="e" s="T358">ogo </ts>
               <ts e="T360" id="Seg_3446" n="e" s="T359">uhuktan </ts>
               <ts e="T361" id="Seg_3448" n="e" s="T360">baran </ts>
               <ts e="T362" id="Seg_3450" n="e" s="T361">ihilliː </ts>
               <ts e="T363" id="Seg_3452" n="e" s="T362">hɨppɨt — </ts>
               <ts e="T364" id="Seg_3454" n="e" s="T363">ɨraːk </ts>
               <ts e="T365" id="Seg_3456" n="e" s="T364">kanna </ts>
               <ts e="T366" id="Seg_3458" n="e" s="T365">ire </ts>
               <ts e="T367" id="Seg_3460" n="e" s="T366">emi͡e </ts>
               <ts e="T368" id="Seg_3462" n="e" s="T367">kaːmar </ts>
               <ts e="T369" id="Seg_3464" n="e" s="T368">tɨ͡as </ts>
               <ts e="T370" id="Seg_3466" n="e" s="T369">ihillibit. </ts>
               <ts e="T371" id="Seg_3468" n="e" s="T370">Dogottorun </ts>
               <ts e="T372" id="Seg_3470" n="e" s="T371">uhugunnarbɨta — </ts>
               <ts e="T373" id="Seg_3472" n="e" s="T372">togo </ts>
               <ts e="T374" id="Seg_3474" n="e" s="T373">uhuktu͡oktaraj </ts>
               <ts e="T375" id="Seg_3476" n="e" s="T374">čubu </ts>
               <ts e="T376" id="Seg_3478" n="e" s="T375">utujbut </ts>
               <ts e="T377" id="Seg_3480" n="e" s="T376">kihiler. </ts>
               <ts e="T378" id="Seg_3482" n="e" s="T377">ɨt </ts>
               <ts e="T379" id="Seg_3484" n="e" s="T378">u͡ola </ts>
               <ts e="T380" id="Seg_3486" n="e" s="T379">taksɨbɨt </ts>
               <ts e="T381" id="Seg_3488" n="e" s="T380">da </ts>
               <ts e="T382" id="Seg_3490" n="e" s="T381">ikkis </ts>
               <ts e="T383" id="Seg_3492" n="e" s="T382">abaːhɨ </ts>
               <ts e="T384" id="Seg_3494" n="e" s="T383">menʼiːtin </ts>
               <ts e="T385" id="Seg_3496" n="e" s="T384">emi͡e </ts>
               <ts e="T386" id="Seg_3498" n="e" s="T385">boldok </ts>
               <ts e="T387" id="Seg_3500" n="e" s="T386">taːska </ts>
               <ts e="T388" id="Seg_3502" n="e" s="T387">kampɨ </ts>
               <ts e="T389" id="Seg_3504" n="e" s="T388">oksubut. </ts>
               <ts e="T390" id="Seg_3506" n="e" s="T389">Abaːhɨ </ts>
               <ts e="T391" id="Seg_3508" n="e" s="T390">tebi͡elene </ts>
               <ts e="T392" id="Seg_3510" n="e" s="T391">tühen </ts>
               <ts e="T393" id="Seg_3512" n="e" s="T392">baran </ts>
               <ts e="T394" id="Seg_3514" n="e" s="T393">čirkes </ts>
               <ts e="T395" id="Seg_3516" n="e" s="T394">gɨmmɨt. </ts>
               <ts e="T396" id="Seg_3518" n="e" s="T395">ɨt </ts>
               <ts e="T397" id="Seg_3520" n="e" s="T396">u͡ola </ts>
               <ts e="T398" id="Seg_3522" n="e" s="T397">irgetitten </ts>
               <ts e="T399" id="Seg_3524" n="e" s="T398">huruktaːk </ts>
               <ts e="T400" id="Seg_3526" n="e" s="T399">taːhɨ </ts>
               <ts e="T401" id="Seg_3528" n="e" s="T400">oroːto </ts>
               <ts e="T402" id="Seg_3530" n="e" s="T401">dagɨnɨ </ts>
               <ts e="T403" id="Seg_3532" n="e" s="T402">karmaːnɨgar </ts>
               <ts e="T404" id="Seg_3534" n="e" s="T403">emi͡e </ts>
               <ts e="T405" id="Seg_3536" n="e" s="T404">uktan </ts>
               <ts e="T406" id="Seg_3538" n="e" s="T405">keːspit. </ts>
               <ts e="T407" id="Seg_3540" n="e" s="T406">Ühüs </ts>
               <ts e="T408" id="Seg_3542" n="e" s="T407">tüːnüger </ts>
               <ts e="T409" id="Seg_3544" n="e" s="T408">ühüs </ts>
               <ts e="T410" id="Seg_3546" n="e" s="T409">abaːhɨ </ts>
               <ts e="T411" id="Seg_3548" n="e" s="T410">menʼiːtin </ts>
               <ts e="T412" id="Seg_3550" n="e" s="T411">kaja </ts>
               <ts e="T413" id="Seg_3552" n="e" s="T412">okson </ts>
               <ts e="T414" id="Seg_3554" n="e" s="T413">huruktaːk </ts>
               <ts e="T415" id="Seg_3556" n="e" s="T414">taːhɨ </ts>
               <ts e="T416" id="Seg_3558" n="e" s="T415">oruː </ts>
               <ts e="T417" id="Seg_3560" n="e" s="T416">oksubut. </ts>
               <ts e="T418" id="Seg_3562" n="e" s="T417">Dogottorun </ts>
               <ts e="T419" id="Seg_3564" n="e" s="T418">im </ts>
               <ts e="T420" id="Seg_3566" n="e" s="T419">turarɨn </ts>
               <ts e="T421" id="Seg_3568" n="e" s="T420">kɨtta </ts>
               <ts e="T422" id="Seg_3570" n="e" s="T421">uhugunnarbɨt. </ts>
               <ts e="T423" id="Seg_3572" n="e" s="T422">"Če, </ts>
               <ts e="T424" id="Seg_3574" n="e" s="T423">dʼi͡ebitiger </ts>
               <ts e="T425" id="Seg_3576" n="e" s="T424">barɨ͡agɨŋ", </ts>
               <ts e="T426" id="Seg_3578" n="e" s="T425">dʼi͡ebit </ts>
               <ts e="T427" id="Seg_3580" n="e" s="T426">bu </ts>
               <ts e="T428" id="Seg_3582" n="e" s="T427">u͡ol. </ts>
               <ts e="T429" id="Seg_3584" n="e" s="T428">ɨraːktaːgɨ </ts>
               <ts e="T430" id="Seg_3586" n="e" s="T429">u͡ola </ts>
               <ts e="T431" id="Seg_3588" n="e" s="T430">öčöspüt: </ts>
               <ts e="T432" id="Seg_3590" n="e" s="T431">"Togo </ts>
               <ts e="T433" id="Seg_3592" n="e" s="T432">doː! </ts>
               <ts e="T434" id="Seg_3594" n="e" s="T433">Oloro </ts>
               <ts e="T435" id="Seg_3596" n="e" s="T434">tühü͡ögüŋ, </ts>
               <ts e="T436" id="Seg_3598" n="e" s="T435">beseleː." </ts>
               <ts e="T437" id="Seg_3600" n="e" s="T436">Koru͡oba </ts>
               <ts e="T438" id="Seg_3602" n="e" s="T437">u͡ola </ts>
               <ts e="T439" id="Seg_3604" n="e" s="T438">oloro </ts>
               <ts e="T440" id="Seg_3606" n="e" s="T439">tühen </ts>
               <ts e="T441" id="Seg_3608" n="e" s="T440">baran </ts>
               <ts e="T442" id="Seg_3610" n="e" s="T441">dʼi͡ebit: </ts>
               <ts e="T443" id="Seg_3612" n="e" s="T442">"Kergetterbit </ts>
               <ts e="T444" id="Seg_3614" n="e" s="T443">küːtü͡öktere, </ts>
               <ts e="T445" id="Seg_3616" n="e" s="T444">barɨ͡agɨŋ </ts>
               <ts e="T446" id="Seg_3618" n="e" s="T445">eːt." </ts>
               <ts e="T447" id="Seg_3620" n="e" s="T446">U͡olattar </ts>
               <ts e="T448" id="Seg_3622" n="e" s="T447">kamnaːbɨttar. </ts>
               <ts e="T449" id="Seg_3624" n="e" s="T448">ɨt </ts>
               <ts e="T450" id="Seg_3626" n="e" s="T449">u͡ola </ts>
               <ts e="T451" id="Seg_3628" n="e" s="T450">erden </ts>
               <ts e="T452" id="Seg_3630" n="e" s="T451">ispit. </ts>
               <ts e="T453" id="Seg_3632" n="e" s="T452">Töhö </ts>
               <ts e="T454" id="Seg_3634" n="e" s="T453">da </ts>
               <ts e="T455" id="Seg_3636" n="e" s="T454">bu͡olbakka </ts>
               <ts e="T456" id="Seg_3638" n="e" s="T455">kɨːnɨn </ts>
               <ts e="T457" id="Seg_3640" n="e" s="T456">tuttan </ts>
               <ts e="T458" id="Seg_3642" n="e" s="T457">baran: </ts>
               <ts e="T459" id="Seg_3644" n="e" s="T458">Dogottoːr, </ts>
               <ts e="T460" id="Seg_3646" n="e" s="T459">bahakpɨn </ts>
               <ts e="T461" id="Seg_3648" n="e" s="T460">kaːllarbɨppɨn, </ts>
               <ts e="T462" id="Seg_3650" n="e" s="T461">kördüː </ts>
               <ts e="T463" id="Seg_3652" n="e" s="T462">barɨ͡am, </ts>
               <ts e="T464" id="Seg_3654" n="e" s="T463">dʼi͡ebit. </ts>
               <ts e="T465" id="Seg_3656" n="e" s="T464">ɨt </ts>
               <ts e="T466" id="Seg_3658" n="e" s="T465">u͡ola </ts>
               <ts e="T467" id="Seg_3660" n="e" s="T466">balagaŋŋa </ts>
               <ts e="T468" id="Seg_3662" n="e" s="T467">kelbite </ts>
               <ts e="T469" id="Seg_3664" n="e" s="T468">dʼaktattar </ts>
               <ts e="T470" id="Seg_3666" n="e" s="T469">ɨtɨːr </ts>
               <ts e="T471" id="Seg_3668" n="e" s="T470">haŋalara </ts>
               <ts e="T472" id="Seg_3670" n="e" s="T471">ihillibit. </ts>
               <ts e="T473" id="Seg_3672" n="e" s="T472">"Min </ts>
               <ts e="T474" id="Seg_3674" n="e" s="T473">minnʼiges </ts>
               <ts e="T475" id="Seg_3676" n="e" s="T474">as </ts>
               <ts e="T476" id="Seg_3678" n="e" s="T475">bu͡olan </ts>
               <ts e="T477" id="Seg_3680" n="e" s="T476">baran </ts>
               <ts e="T478" id="Seg_3682" n="e" s="T477">ulakan </ts>
               <ts e="T479" id="Seg_3684" n="e" s="T478">teri͡elkege </ts>
               <ts e="T480" id="Seg_3686" n="e" s="T479">tɨːlarɨn </ts>
               <ts e="T481" id="Seg_3688" n="e" s="T480">ihiger </ts>
               <ts e="T482" id="Seg_3690" n="e" s="T481">tühü͡öm. </ts>
               <ts e="T483" id="Seg_3692" n="e" s="T482">Minʼigin </ts>
               <ts e="T484" id="Seg_3694" n="e" s="T483">hi͡etekterine </ts>
               <ts e="T485" id="Seg_3696" n="e" s="T484">ölön </ts>
               <ts e="T486" id="Seg_3698" n="e" s="T485">kaːlɨ͡aktara", </ts>
               <ts e="T487" id="Seg_3700" n="e" s="T486">dʼi͡ebit </ts>
               <ts e="T488" id="Seg_3702" n="e" s="T487">kardʼagas </ts>
               <ts e="T489" id="Seg_3704" n="e" s="T488">Dʼige baːba </ts>
               <ts e="T490" id="Seg_3706" n="e" s="T489">ku͡olaha. </ts>
               <ts e="T491" id="Seg_3708" n="e" s="T490">"Min </ts>
               <ts e="T492" id="Seg_3710" n="e" s="T491">bu͡ollagɨna </ts>
               <ts e="T493" id="Seg_3712" n="e" s="T492">kahan </ts>
               <ts e="T494" id="Seg_3714" n="e" s="T493">da </ts>
               <ts e="T495" id="Seg_3716" n="e" s="T494">körbötök </ts>
               <ts e="T496" id="Seg_3718" n="e" s="T495">bosku͡oj </ts>
               <ts e="T497" id="Seg_3720" n="e" s="T496">holko </ts>
               <ts e="T498" id="Seg_3722" n="e" s="T497">pɨlaːt </ts>
               <ts e="T499" id="Seg_3724" n="e" s="T498">bu͡olan </ts>
               <ts e="T500" id="Seg_3726" n="e" s="T499">ɨraːktaːgɨ </ts>
               <ts e="T501" id="Seg_3728" n="e" s="T500">u͡olun </ts>
               <ts e="T502" id="Seg_3730" n="e" s="T501">töhögüger </ts>
               <ts e="T503" id="Seg_3732" n="e" s="T502">tühü͡öm, </ts>
               <ts e="T504" id="Seg_3734" n="e" s="T503">barɨkaːttarɨn </ts>
               <ts e="T505" id="Seg_3736" n="e" s="T504">moŋunnaran </ts>
               <ts e="T506" id="Seg_3738" n="e" s="T505">ölörü͡öm." </ts>
               <ts e="T507" id="Seg_3740" n="e" s="T506">"Min </ts>
               <ts e="T508" id="Seg_3742" n="e" s="T507">bu͡ollagɨna </ts>
               <ts e="T509" id="Seg_3744" n="e" s="T508">tɨːlarɨn </ts>
               <ts e="T510" id="Seg_3746" n="e" s="T509">ipseri </ts>
               <ts e="T511" id="Seg_3748" n="e" s="T510">hilimniːr </ts>
               <ts e="T512" id="Seg_3750" n="e" s="T511">gɨna </ts>
               <ts e="T513" id="Seg_3752" n="e" s="T512">uː </ts>
               <ts e="T514" id="Seg_3754" n="e" s="T513">hɨmala </ts>
               <ts e="T515" id="Seg_3756" n="e" s="T514">bu͡olu͡om, </ts>
               <ts e="T516" id="Seg_3758" n="e" s="T515">barɨlarɨn </ts>
               <ts e="T517" id="Seg_3760" n="e" s="T516">oboron </ts>
               <ts e="T518" id="Seg_3762" n="e" s="T517">timirdi͡em." </ts>
               <ts e="T519" id="Seg_3764" n="e" s="T518">"Dʼe </ts>
               <ts e="T520" id="Seg_3766" n="e" s="T519">üčügej. </ts>
               <ts e="T521" id="Seg_3768" n="e" s="T520">Bertik </ts>
               <ts e="T522" id="Seg_3770" n="e" s="T521">hanaːbɨkkɨt </ts>
               <ts e="T523" id="Seg_3772" n="e" s="T522">ebit", </ts>
               <ts e="T524" id="Seg_3774" n="e" s="T523">dʼi͡ebit </ts>
               <ts e="T525" id="Seg_3776" n="e" s="T524">mannajgɨ </ts>
               <ts e="T526" id="Seg_3778" n="e" s="T525">dʼaktar. </ts>
               <ts e="T527" id="Seg_3780" n="e" s="T526">ɨt </ts>
               <ts e="T528" id="Seg_3782" n="e" s="T527">u͡ola </ts>
               <ts e="T529" id="Seg_3784" n="e" s="T528">itini </ts>
               <ts e="T530" id="Seg_3786" n="e" s="T529">istibit </ts>
               <ts e="T531" id="Seg_3788" n="e" s="T530">daːganɨ </ts>
               <ts e="T532" id="Seg_3790" n="e" s="T531">hɨːr </ts>
               <ts e="T533" id="Seg_3792" n="e" s="T532">annɨgar </ts>
               <ts e="T534" id="Seg_3794" n="e" s="T533">hüːren </ts>
               <ts e="T535" id="Seg_3796" n="e" s="T534">kiːrbit, </ts>
               <ts e="T536" id="Seg_3798" n="e" s="T535">tɨːtɨn </ts>
               <ts e="T537" id="Seg_3800" n="e" s="T536">ɨlbaktɨːr </ts>
               <ts e="T538" id="Seg_3802" n="e" s="T537">da </ts>
               <ts e="T539" id="Seg_3804" n="e" s="T538">erden </ts>
               <ts e="T540" id="Seg_3806" n="e" s="T539">ispit. </ts>
               <ts e="T541" id="Seg_3808" n="e" s="T540">Dogottorun </ts>
               <ts e="T542" id="Seg_3810" n="e" s="T541">ebe </ts>
               <ts e="T543" id="Seg_3812" n="e" s="T542">ortotugar </ts>
               <ts e="T544" id="Seg_3814" n="e" s="T543">hite </ts>
               <ts e="T545" id="Seg_3816" n="e" s="T544">oksubut. </ts>
               <ts e="T546" id="Seg_3818" n="e" s="T545">U͡olattar </ts>
               <ts e="T547" id="Seg_3820" n="e" s="T546">orgujakaːn </ts>
               <ts e="T548" id="Seg_3822" n="e" s="T547">erden </ts>
               <ts e="T549" id="Seg_3824" n="e" s="T548">ispitter. </ts>
               <ts e="T550" id="Seg_3826" n="e" s="T549">Töhö </ts>
               <ts e="T551" id="Seg_3828" n="e" s="T550">ör </ts>
               <ts e="T552" id="Seg_3830" n="e" s="T551">bu͡olu͡oj, </ts>
               <ts e="T553" id="Seg_3832" n="e" s="T552">tɨːlarɨn </ts>
               <ts e="T554" id="Seg_3834" n="e" s="T553">ihiger </ts>
               <ts e="T555" id="Seg_3836" n="e" s="T554">astaːk </ts>
               <ts e="T556" id="Seg_3838" n="e" s="T555">teri͡elke </ts>
               <ts e="T557" id="Seg_3840" n="e" s="T556">tüspüt. </ts>
               <ts e="T558" id="Seg_3842" n="e" s="T557">ɨraːktaːgɨ </ts>
               <ts e="T559" id="Seg_3844" n="e" s="T558">u͡ola </ts>
               <ts e="T560" id="Seg_3846" n="e" s="T559">ü͡örbüt, </ts>
               <ts e="T561" id="Seg_3848" n="e" s="T560">iliːtin </ts>
               <ts e="T562" id="Seg_3850" n="e" s="T561">teri͡elke </ts>
               <ts e="T563" id="Seg_3852" n="e" s="T562">dʼi͡ek </ts>
               <ts e="T564" id="Seg_3854" n="e" s="T563">harbas </ts>
               <ts e="T565" id="Seg_3856" n="e" s="T564">gɨnarɨn </ts>
               <ts e="T566" id="Seg_3858" n="e" s="T565">gɨtta </ts>
               <ts e="T567" id="Seg_3860" n="e" s="T566">ɨt </ts>
               <ts e="T568" id="Seg_3862" n="e" s="T567">u͡ola </ts>
               <ts e="T569" id="Seg_3864" n="e" s="T568">astaːk </ts>
               <ts e="T570" id="Seg_3866" n="e" s="T569">teri͡elkeni </ts>
               <ts e="T571" id="Seg_3868" n="e" s="T570">uːga </ts>
               <ts e="T572" id="Seg_3870" n="e" s="T571">bɨragan </ts>
               <ts e="T573" id="Seg_3872" n="e" s="T572">keːspit. </ts>
               <ts e="T574" id="Seg_3874" n="e" s="T573">ɨraːktaːgɨ </ts>
               <ts e="T575" id="Seg_3876" n="e" s="T574">u͡ola </ts>
               <ts e="T576" id="Seg_3878" n="e" s="T575">kɨːhɨran </ts>
               <ts e="T577" id="Seg_3880" n="e" s="T576">huntujan </ts>
               <ts e="T578" id="Seg_3882" n="e" s="T577">olordoguna, </ts>
               <ts e="T579" id="Seg_3884" n="e" s="T578">bosku͡oj </ts>
               <ts e="T580" id="Seg_3886" n="e" s="T579">bagajɨ </ts>
               <ts e="T581" id="Seg_3888" n="e" s="T580">holko </ts>
               <ts e="T582" id="Seg_3890" n="e" s="T581">pulaːt </ts>
               <ts e="T583" id="Seg_3892" n="e" s="T582">ɨraːktaːgɨ </ts>
               <ts e="T584" id="Seg_3894" n="e" s="T583">u͡olun </ts>
               <ts e="T585" id="Seg_3896" n="e" s="T584">töhögüger </ts>
               <ts e="T586" id="Seg_3898" n="e" s="T585">tüspüt. </ts>
               <ts e="T587" id="Seg_3900" n="e" s="T586">"O, </ts>
               <ts e="T588" id="Seg_3902" n="e" s="T587">taŋara, </ts>
               <ts e="T589" id="Seg_3904" n="e" s="T588">maːmabar </ts>
               <ts e="T590" id="Seg_3906" n="e" s="T589">kehiː </ts>
               <ts e="T591" id="Seg_3908" n="e" s="T590">gɨnɨ͡am", </ts>
               <ts e="T592" id="Seg_3910" n="e" s="T591">dʼi͡ebit </ts>
               <ts e="T593" id="Seg_3912" n="e" s="T592">da </ts>
               <ts e="T594" id="Seg_3914" n="e" s="T593">karmaːnɨgar </ts>
               <ts e="T595" id="Seg_3916" n="e" s="T594">uktaːrɨ </ts>
               <ts e="T596" id="Seg_3918" n="e" s="T595">gɨnarɨn </ts>
               <ts e="T597" id="Seg_3920" n="e" s="T596">gɨtta </ts>
               <ts e="T598" id="Seg_3922" n="e" s="T597">u͡ola </ts>
               <ts e="T599" id="Seg_3924" n="e" s="T598">talaːn </ts>
               <ts e="T600" id="Seg_3926" n="e" s="T599">ɨlbɨt, </ts>
               <ts e="T601" id="Seg_3928" n="e" s="T600">bahagɨnan </ts>
               <ts e="T602" id="Seg_3930" n="e" s="T601">ilči </ts>
               <ts e="T603" id="Seg_3932" n="e" s="T602">kerden </ts>
               <ts e="T604" id="Seg_3934" n="e" s="T603">baran </ts>
               <ts e="T605" id="Seg_3936" n="e" s="T604">uːga </ts>
               <ts e="T606" id="Seg_3938" n="e" s="T605">bɨragan </ts>
               <ts e="T607" id="Seg_3940" n="e" s="T606">keːspit. </ts>
               <ts e="T608" id="Seg_3942" n="e" s="T607">ɨraːktaːgɨ </ts>
               <ts e="T609" id="Seg_3944" n="e" s="T608">u͡ola </ts>
               <ts e="T610" id="Seg_3946" n="e" s="T609">kɨːhɨran </ts>
               <ts e="T611" id="Seg_3948" n="e" s="T610">haŋata </ts>
               <ts e="T612" id="Seg_3950" n="e" s="T611">da </ts>
               <ts e="T613" id="Seg_3952" n="e" s="T612">taksɨbatak. </ts>
               <ts e="T614" id="Seg_3954" n="e" s="T613">Töhö </ts>
               <ts e="T615" id="Seg_3956" n="e" s="T614">ör </ts>
               <ts e="T616" id="Seg_3958" n="e" s="T615">bu͡olu͡oj, </ts>
               <ts e="T617" id="Seg_3960" n="e" s="T616">tɨːlara </ts>
               <ts e="T618" id="Seg_3962" n="e" s="T617">uːga </ts>
               <ts e="T619" id="Seg_3964" n="e" s="T618">hɨstan </ts>
               <ts e="T620" id="Seg_3966" n="e" s="T619">kaːlbɨt, </ts>
               <ts e="T621" id="Seg_3968" n="e" s="T620">ebe </ts>
               <ts e="T622" id="Seg_3970" n="e" s="T621">uːta </ts>
               <ts e="T623" id="Seg_3972" n="e" s="T622">karaːrt </ts>
               <ts e="T624" id="Seg_3974" n="e" s="T623">gɨmmɨt. </ts>
               <ts e="T625" id="Seg_3976" n="e" s="T624">Bu </ts>
               <ts e="T626" id="Seg_3978" n="e" s="T625">u͡olattar </ts>
               <ts e="T627" id="Seg_3980" n="e" s="T626">hataːn </ts>
               <ts e="T628" id="Seg_3982" n="e" s="T627">erdimmet </ts>
               <ts e="T629" id="Seg_3984" n="e" s="T628">bu͡olan </ts>
               <ts e="T630" id="Seg_3986" n="e" s="T629">kaːlbɨttar. </ts>
               <ts e="T631" id="Seg_3988" n="e" s="T630">Kuttammɨttar. </ts>
               <ts e="T632" id="Seg_3990" n="e" s="T631">ɨt </ts>
               <ts e="T633" id="Seg_3992" n="e" s="T632">u͡ola: </ts>
               <ts e="T634" id="Seg_3994" n="e" s="T633">"Uːnu </ts>
               <ts e="T635" id="Seg_3996" n="e" s="T634">hügennen </ts>
               <ts e="T636" id="Seg_3998" n="e" s="T635">bɨhɨta </ts>
               <ts e="T637" id="Seg_4000" n="e" s="T636">kerdiŋ, </ts>
               <ts e="T638" id="Seg_4002" n="e" s="T637">bahagɨnan </ts>
               <ts e="T639" id="Seg_4004" n="e" s="T638">tehite </ts>
               <ts e="T640" id="Seg_4006" n="e" s="T639">annʼɨŋ, </ts>
               <ts e="T641" id="Seg_4008" n="e" s="T640">min </ts>
               <ts e="T642" id="Seg_4010" n="e" s="T641">erden </ts>
               <ts e="T643" id="Seg_4012" n="e" s="T642">ihi͡em", </ts>
               <ts e="T644" id="Seg_4014" n="e" s="T643">dʼi͡ebit </ts>
               <ts e="T645" id="Seg_4016" n="e" s="T644">dogottorugar. </ts>
               <ts e="T646" id="Seg_4018" n="e" s="T645">Körbüttere — </ts>
               <ts e="T647" id="Seg_4020" n="e" s="T646">uːlara </ts>
               <ts e="T648" id="Seg_4022" n="e" s="T647">kirdik </ts>
               <ts e="T649" id="Seg_4024" n="e" s="T648">hɨrdaːn, </ts>
               <ts e="T650" id="Seg_4026" n="e" s="T649">kɨtaran </ts>
               <ts e="T651" id="Seg_4028" n="e" s="T650">ispit. </ts>
               <ts e="T652" id="Seg_4030" n="e" s="T651">Hotoru </ts>
               <ts e="T653" id="Seg_4032" n="e" s="T652">tɨːlara </ts>
               <ts e="T654" id="Seg_4034" n="e" s="T653">boskolommut. </ts>
               <ts e="T655" id="Seg_4036" n="e" s="T654">U͡olattar </ts>
               <ts e="T656" id="Seg_4038" n="e" s="T655">erdinen </ts>
               <ts e="T657" id="Seg_4040" n="e" s="T656">ispitter </ts>
               <ts e="T658" id="Seg_4042" n="e" s="T657">orguːjakaːn. </ts>
               <ts e="T659" id="Seg_4044" n="e" s="T658">Töhö </ts>
               <ts e="T660" id="Seg_4046" n="e" s="T659">da </ts>
               <ts e="T661" id="Seg_4048" n="e" s="T660">ör </ts>
               <ts e="T662" id="Seg_4050" n="e" s="T661">bu͡olbakka </ts>
               <ts e="T663" id="Seg_4052" n="e" s="T662">ebe </ts>
               <ts e="T664" id="Seg_4054" n="e" s="T663">ürdük </ts>
               <ts e="T665" id="Seg_4056" n="e" s="T664">kɨtɨlɨgar </ts>
               <ts e="T666" id="Seg_4058" n="e" s="T665">gu͡orattara </ts>
               <ts e="T667" id="Seg_4060" n="e" s="T666">köstübüt. </ts>
               <ts e="T668" id="Seg_4062" n="e" s="T667">Tiksen </ts>
               <ts e="T669" id="Seg_4064" n="e" s="T668">baran </ts>
               <ts e="T670" id="Seg_4066" n="e" s="T669">u͡olattar </ts>
               <ts e="T671" id="Seg_4068" n="e" s="T670">üs </ts>
               <ts e="T672" id="Seg_4070" n="e" s="T671">aŋɨ </ts>
               <ts e="T673" id="Seg_4072" n="e" s="T672">araksan </ts>
               <ts e="T674" id="Seg_4074" n="e" s="T673">kaːlbɨttar. </ts>
               <ts e="T675" id="Seg_4076" n="e" s="T674">ɨraːktaːgɨ </ts>
               <ts e="T676" id="Seg_4078" n="e" s="T675">u͡ola </ts>
               <ts e="T677" id="Seg_4080" n="e" s="T676">hɨːr </ts>
               <ts e="T678" id="Seg_4082" n="e" s="T677">ürdüger </ts>
               <ts e="T679" id="Seg_4084" n="e" s="T678">čeːlkeː </ts>
               <ts e="T680" id="Seg_4086" n="e" s="T679">dʼi͡ege </ts>
               <ts e="T681" id="Seg_4088" n="e" s="T680">barbɨt, </ts>
               <ts e="T682" id="Seg_4090" n="e" s="T681">ɨt </ts>
               <ts e="T683" id="Seg_4092" n="e" s="T682">u͡ola </ts>
               <ts e="T684" id="Seg_4094" n="e" s="T683">maːmatɨn </ts>
               <ts e="T685" id="Seg_4096" n="e" s="T684">nʼamčɨgas </ts>
               <ts e="T686" id="Seg_4098" n="e" s="T685">koroːn </ts>
               <ts e="T687" id="Seg_4100" n="e" s="T686">dʼi͡etiger </ts>
               <ts e="T688" id="Seg_4102" n="e" s="T687">kiːrbit, </ts>
               <ts e="T689" id="Seg_4104" n="e" s="T688">koru͡oba </ts>
               <ts e="T690" id="Seg_4106" n="e" s="T689">u͡ola </ts>
               <ts e="T691" id="Seg_4108" n="e" s="T690">tahaːraː </ts>
               <ts e="T692" id="Seg_4110" n="e" s="T691">inʼetin </ts>
               <ts e="T693" id="Seg_4112" n="e" s="T692">körsübüt. </ts>
               <ts e="T694" id="Seg_4114" n="e" s="T693">Harsi͡erda </ts>
               <ts e="T695" id="Seg_4116" n="e" s="T694">ɨraːktaːgɨ </ts>
               <ts e="T696" id="Seg_4118" n="e" s="T695">u͡olattarɨ </ts>
               <ts e="T697" id="Seg_4120" n="e" s="T696">ɨgɨrtatalaːbɨt: </ts>
               <ts e="T698" id="Seg_4122" n="e" s="T697">"Če </ts>
               <ts e="T699" id="Seg_4124" n="e" s="T698">ogolorum, </ts>
               <ts e="T700" id="Seg_4126" n="e" s="T699">kepseːŋ </ts>
               <ts e="T701" id="Seg_4128" n="e" s="T700">tugu </ts>
               <ts e="T702" id="Seg_4130" n="e" s="T701">gɨna </ts>
               <ts e="T703" id="Seg_4132" n="e" s="T702">bu </ts>
               <ts e="T704" id="Seg_4134" n="e" s="T703">turkarɨ </ts>
               <ts e="T705" id="Seg_4136" n="e" s="T704">hɨldʼɨbɨkkɨtɨj?" </ts>
               <ts e="T706" id="Seg_4138" n="e" s="T705">"Üs </ts>
               <ts e="T707" id="Seg_4140" n="e" s="T706">abaːhɨ </ts>
               <ts e="T708" id="Seg_4142" n="e" s="T707">irgetitten </ts>
               <ts e="T709" id="Seg_4144" n="e" s="T708">üs </ts>
               <ts e="T710" id="Seg_4146" n="e" s="T709">huruktaːk </ts>
               <ts e="T711" id="Seg_4148" n="e" s="T710">taːhɨ </ts>
               <ts e="T712" id="Seg_4150" n="e" s="T711">bulbutum", </ts>
               <ts e="T713" id="Seg_4152" n="e" s="T712">ɨt </ts>
               <ts e="T714" id="Seg_4154" n="e" s="T713">u͡ola </ts>
               <ts e="T715" id="Seg_4156" n="e" s="T714">dʼi͡ebit, </ts>
               <ts e="T716" id="Seg_4158" n="e" s="T715">ɨraːktaːgɨ </ts>
               <ts e="T717" id="Seg_4160" n="e" s="T716">iliːtiger </ts>
               <ts e="T718" id="Seg_4162" n="e" s="T717">üs </ts>
               <ts e="T719" id="Seg_4164" n="e" s="T718">taːhɨ </ts>
               <ts e="T720" id="Seg_4166" n="e" s="T719">tuttaran </ts>
               <ts e="T721" id="Seg_4168" n="e" s="T720">keːspit. </ts>
               <ts e="T722" id="Seg_4170" n="e" s="T721">ɨːraːktaːgɨ </ts>
               <ts e="T723" id="Seg_4172" n="e" s="T722">ɨt </ts>
               <ts e="T724" id="Seg_4174" n="e" s="T723">u͡olun </ts>
               <ts e="T725" id="Seg_4176" n="e" s="T724">moːjuttan </ts>
               <ts e="T726" id="Seg_4178" n="e" s="T725">kuːhan </ts>
               <ts e="T727" id="Seg_4180" n="e" s="T726">ɨlbɨt, </ts>
               <ts e="T728" id="Seg_4182" n="e" s="T727">ɨtamsɨja </ts>
               <ts e="T729" id="Seg_4184" n="e" s="T728">tühen </ts>
               <ts e="T730" id="Seg_4186" n="e" s="T729">baran </ts>
               <ts e="T731" id="Seg_4188" n="e" s="T730">dʼi͡ebit: </ts>
               <ts e="T732" id="Seg_4190" n="e" s="T731">"Bügüŋŋütten </ts>
               <ts e="T733" id="Seg_4192" n="e" s="T732">en </ts>
               <ts e="T734" id="Seg_4194" n="e" s="T733">ɨraːktaːgɨ </ts>
               <ts e="T735" id="Seg_4196" n="e" s="T734">bu͡olar </ts>
               <ts e="T736" id="Seg_4198" n="e" s="T735">ɨjaːgɨŋ </ts>
               <ts e="T737" id="Seg_4200" n="e" s="T736">kelle. </ts>
               <ts e="T738" id="Seg_4202" n="e" s="T737">U͡olbun </ts>
               <ts e="T739" id="Seg_4204" n="e" s="T738">kɨtta </ts>
               <ts e="T740" id="Seg_4206" n="e" s="T739">ubaj-balɨs </ts>
               <ts e="T741" id="Seg_4208" n="e" s="T740">bu͡olan </ts>
               <ts e="T742" id="Seg_4210" n="e" s="T741">oloruŋ." </ts>
               <ts e="T743" id="Seg_4212" n="e" s="T742">Üs </ts>
               <ts e="T744" id="Seg_4214" n="e" s="T743">u͡ol </ts>
               <ts e="T745" id="Seg_4216" n="e" s="T744">dogorduː </ts>
               <ts e="T746" id="Seg_4218" n="e" s="T745">bajan-tajan </ts>
               <ts e="T747" id="Seg_4220" n="e" s="T746">olorbuttar. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_4221" s="T0">ChPK_1970_ThreeBoys_flk.001</ta>
            <ta e="T8" id="Seg_4222" s="T4">ChPK_1970_ThreeBoys_flk.002</ta>
            <ta e="T12" id="Seg_4223" s="T8">ChPK_1970_ThreeBoys_flk.003</ta>
            <ta e="T17" id="Seg_4224" s="T12">ChPK_1970_ThreeBoys_flk.004</ta>
            <ta e="T20" id="Seg_4225" s="T17">ChPK_1970_ThreeBoys_flk.005</ta>
            <ta e="T25" id="Seg_4226" s="T20">ChPK_1970_ThreeBoys_flk.006</ta>
            <ta e="T28" id="Seg_4227" s="T25">ChPK_1970_ThreeBoys_flk.007</ta>
            <ta e="T32" id="Seg_4228" s="T28">ChPK_1970_ThreeBoys_flk.008</ta>
            <ta e="T40" id="Seg_4229" s="T32">ChPK_1970_ThreeBoys_flk.009</ta>
            <ta e="T44" id="Seg_4230" s="T40">ChPK_1970_ThreeBoys_flk.010</ta>
            <ta e="T48" id="Seg_4231" s="T44">ChPK_1970_ThreeBoys_flk.011</ta>
            <ta e="T54" id="Seg_4232" s="T48">ChPK_1970_ThreeBoys_flk.012</ta>
            <ta e="T57" id="Seg_4233" s="T54">ChPK_1970_ThreeBoys_flk.013</ta>
            <ta e="T62" id="Seg_4234" s="T57">ChPK_1970_ThreeBoys_flk.014</ta>
            <ta e="T70" id="Seg_4235" s="T62">ChPK_1970_ThreeBoys_flk.015</ta>
            <ta e="T73" id="Seg_4236" s="T70">ChPK_1970_ThreeBoys_flk.016</ta>
            <ta e="T79" id="Seg_4237" s="T73">ChPK_1970_ThreeBoys_flk.017</ta>
            <ta e="T84" id="Seg_4238" s="T79">ChPK_1970_ThreeBoys_flk.018</ta>
            <ta e="T88" id="Seg_4239" s="T84">ChPK_1970_ThreeBoys_flk.019</ta>
            <ta e="T96" id="Seg_4240" s="T88">ChPK_1970_ThreeBoys_flk.020</ta>
            <ta e="T103" id="Seg_4241" s="T96">ChPK_1970_ThreeBoys_flk.021</ta>
            <ta e="T107" id="Seg_4242" s="T103">ChPK_1970_ThreeBoys_flk.022</ta>
            <ta e="T113" id="Seg_4243" s="T107">ChPK_1970_ThreeBoys_flk.023</ta>
            <ta e="T116" id="Seg_4244" s="T113">ChPK_1970_ThreeBoys_flk.024</ta>
            <ta e="T120" id="Seg_4245" s="T116">ChPK_1970_ThreeBoys_flk.025</ta>
            <ta e="T130" id="Seg_4246" s="T120">ChPK_1970_ThreeBoys_flk.026</ta>
            <ta e="T136" id="Seg_4247" s="T130">ChPK_1970_ThreeBoys_flk.027</ta>
            <ta e="T143" id="Seg_4248" s="T136">ChPK_1970_ThreeBoys_flk.028</ta>
            <ta e="T145" id="Seg_4249" s="T143">ChPK_1970_ThreeBoys_flk.029</ta>
            <ta e="T157" id="Seg_4250" s="T145">ChPK_1970_ThreeBoys_flk.030</ta>
            <ta e="T168" id="Seg_4251" s="T157">ChPK_1970_ThreeBoys_flk.031</ta>
            <ta e="T172" id="Seg_4252" s="T168">ChPK_1970_ThreeBoys_flk.032</ta>
            <ta e="T176" id="Seg_4253" s="T172">ChPK_1970_ThreeBoys_flk.033</ta>
            <ta e="T183" id="Seg_4254" s="T176">ChPK_1970_ThreeBoys_flk.034</ta>
            <ta e="T188" id="Seg_4255" s="T183">ChPK_1970_ThreeBoys_flk.035</ta>
            <ta e="T191" id="Seg_4256" s="T188">ChPK_1970_ThreeBoys_flk.036</ta>
            <ta e="T197" id="Seg_4257" s="T191">ChPK_1970_ThreeBoys_flk.037</ta>
            <ta e="T205" id="Seg_4258" s="T197">ChPK_1970_ThreeBoys_flk.038</ta>
            <ta e="T208" id="Seg_4259" s="T205">ChPK_1970_ThreeBoys_flk.039</ta>
            <ta e="T210" id="Seg_4260" s="T208">ChPK_1970_ThreeBoys_flk.040</ta>
            <ta e="T213" id="Seg_4261" s="T210">ChPK_1970_ThreeBoys_flk.041</ta>
            <ta e="T215" id="Seg_4262" s="T213">ChPK_1970_ThreeBoys_flk.042</ta>
            <ta e="T217" id="Seg_4263" s="T215">ChPK_1970_ThreeBoys_flk.043</ta>
            <ta e="T221" id="Seg_4264" s="T217">ChPK_1970_ThreeBoys_flk.044</ta>
            <ta e="T223" id="Seg_4265" s="T221">ChPK_1970_ThreeBoys_flk.045</ta>
            <ta e="T225" id="Seg_4266" s="T223">ChPK_1970_ThreeBoys_flk.046</ta>
            <ta e="T227" id="Seg_4267" s="T225">ChPK_1970_ThreeBoys_flk.047</ta>
            <ta e="T234" id="Seg_4268" s="T227">ChPK_1970_ThreeBoys_flk.048</ta>
            <ta e="T238" id="Seg_4269" s="T234">ChPK_1970_ThreeBoys_flk.049</ta>
            <ta e="T247" id="Seg_4270" s="T238">ChPK_1970_ThreeBoys_flk.050</ta>
            <ta e="T256" id="Seg_4271" s="T247">ChPK_1970_ThreeBoys_flk.051</ta>
            <ta e="T263" id="Seg_4272" s="T256">ChPK_1970_ThreeBoys_flk.052</ta>
            <ta e="T272" id="Seg_4273" s="T263">ChPK_1970_ThreeBoys_flk.053</ta>
            <ta e="T276" id="Seg_4274" s="T272">ChPK_1970_ThreeBoys_flk.054</ta>
            <ta e="T283" id="Seg_4275" s="T276">ChPK_1970_ThreeBoys_flk.055</ta>
            <ta e="T291" id="Seg_4276" s="T283">ChPK_1970_ThreeBoys_flk.056</ta>
            <ta e="T300" id="Seg_4277" s="T291">ChPK_1970_ThreeBoys_flk.057</ta>
            <ta e="T313" id="Seg_4278" s="T300">ChPK_1970_ThreeBoys_flk.058</ta>
            <ta e="T322" id="Seg_4279" s="T313">ChPK_1970_ThreeBoys_flk.059</ta>
            <ta e="T325" id="Seg_4280" s="T322">ChPK_1970_ThreeBoys_flk.060</ta>
            <ta e="T331" id="Seg_4281" s="T325">ChPK_1970_ThreeBoys_flk.061</ta>
            <ta e="T334" id="Seg_4282" s="T331">ChPK_1970_ThreeBoys_flk.062</ta>
            <ta e="T339" id="Seg_4283" s="T334">ChPK_1970_ThreeBoys_flk.063</ta>
            <ta e="T349" id="Seg_4284" s="T339">ChPK_1970_ThreeBoys_flk.064</ta>
            <ta e="T357" id="Seg_4285" s="T349">ChPK_1970_ThreeBoys_flk.065</ta>
            <ta e="T370" id="Seg_4286" s="T357">ChPK_1970_ThreeBoys_flk.066</ta>
            <ta e="T377" id="Seg_4287" s="T370">ChPK_1970_ThreeBoys_flk.067</ta>
            <ta e="T389" id="Seg_4288" s="T377">ChPK_1970_ThreeBoys_flk.068</ta>
            <ta e="T395" id="Seg_4289" s="T389">ChPK_1970_ThreeBoys_flk.069</ta>
            <ta e="T406" id="Seg_4290" s="T395">ChPK_1970_ThreeBoys_flk.070</ta>
            <ta e="T417" id="Seg_4291" s="T406">ChPK_1970_ThreeBoys_flk.071</ta>
            <ta e="T422" id="Seg_4292" s="T417">ChPK_1970_ThreeBoys_flk.072</ta>
            <ta e="T428" id="Seg_4293" s="T422">ChPK_1970_ThreeBoys_flk.073</ta>
            <ta e="T431" id="Seg_4294" s="T428">ChPK_1970_ThreeBoys_flk.074</ta>
            <ta e="T433" id="Seg_4295" s="T431">ChPK_1970_ThreeBoys_flk.075</ta>
            <ta e="T436" id="Seg_4296" s="T433">ChPK_1970_ThreeBoys_flk.076</ta>
            <ta e="T442" id="Seg_4297" s="T436">ChPK_1970_ThreeBoys_flk.077</ta>
            <ta e="T446" id="Seg_4298" s="T442">ChPK_1970_ThreeBoys_flk.078</ta>
            <ta e="T448" id="Seg_4299" s="T446">ChPK_1970_ThreeBoys_flk.079</ta>
            <ta e="T452" id="Seg_4300" s="T448">ChPK_1970_ThreeBoys_flk.080</ta>
            <ta e="T458" id="Seg_4301" s="T452">ChPK_1970_ThreeBoys_flk.081</ta>
            <ta e="T464" id="Seg_4302" s="T458">ChPK_1970_ThreeBoys_flk.082</ta>
            <ta e="T472" id="Seg_4303" s="T464">ChPK_1970_ThreeBoys_flk.083</ta>
            <ta e="T482" id="Seg_4304" s="T472">ChPK_1970_ThreeBoys_flk.084</ta>
            <ta e="T490" id="Seg_4305" s="T482">ChPK_1970_ThreeBoys_flk.085</ta>
            <ta e="T506" id="Seg_4306" s="T490">ChPK_1970_ThreeBoys_flk.086</ta>
            <ta e="T518" id="Seg_4307" s="T506">ChPK_1970_ThreeBoys_flk.087</ta>
            <ta e="T520" id="Seg_4308" s="T518">ChPK_1970_ThreeBoys_flk.088</ta>
            <ta e="T526" id="Seg_4309" s="T520">ChPK_1970_ThreeBoys_flk.089</ta>
            <ta e="T540" id="Seg_4310" s="T526">ChPK_1970_ThreeBoys_flk.090</ta>
            <ta e="T545" id="Seg_4311" s="T540">ChPK_1970_ThreeBoys_flk.091</ta>
            <ta e="T549" id="Seg_4312" s="T545">ChPK_1970_ThreeBoys_flk.092</ta>
            <ta e="T557" id="Seg_4313" s="T549">ChPK_1970_ThreeBoys_flk.093</ta>
            <ta e="T573" id="Seg_4314" s="T557">ChPK_1970_ThreeBoys_flk.094</ta>
            <ta e="T586" id="Seg_4315" s="T573">ChPK_1970_ThreeBoys_flk.095</ta>
            <ta e="T607" id="Seg_4316" s="T586">ChPK_1970_ThreeBoys_flk.096</ta>
            <ta e="T613" id="Seg_4317" s="T607">ChPK_1970_ThreeBoys_flk.097</ta>
            <ta e="T624" id="Seg_4318" s="T613">ChPK_1970_ThreeBoys_flk.098</ta>
            <ta e="T630" id="Seg_4319" s="T624">ChPK_1970_ThreeBoys_flk.099</ta>
            <ta e="T631" id="Seg_4320" s="T630">ChPK_1970_ThreeBoys_flk.100</ta>
            <ta e="T633" id="Seg_4321" s="T631">ChPK_1970_ThreeBoys_flk.101</ta>
            <ta e="T645" id="Seg_4322" s="T633">ChPK_1970_ThreeBoys_flk.102</ta>
            <ta e="T651" id="Seg_4323" s="T645">ChPK_1970_ThreeBoys_flk.103</ta>
            <ta e="T654" id="Seg_4324" s="T651">ChPK_1970_ThreeBoys_flk.104</ta>
            <ta e="T658" id="Seg_4325" s="T654">ChPK_1970_ThreeBoys_flk.105</ta>
            <ta e="T667" id="Seg_4326" s="T658">ChPK_1970_ThreeBoys_flk.106</ta>
            <ta e="T674" id="Seg_4327" s="T667">ChPK_1970_ThreeBoys_flk.107</ta>
            <ta e="T693" id="Seg_4328" s="T674">ChPK_1970_ThreeBoys_flk.108</ta>
            <ta e="T697" id="Seg_4329" s="T693">ChPK_1970_ThreeBoys_flk.109</ta>
            <ta e="T705" id="Seg_4330" s="T697">ChPK_1970_ThreeBoys_flk.110</ta>
            <ta e="T721" id="Seg_4331" s="T705">ChPK_1970_ThreeBoys_flk.111</ta>
            <ta e="T731" id="Seg_4332" s="T721">ChPK_1970_ThreeBoys_flk.112</ta>
            <ta e="T737" id="Seg_4333" s="T731">ChPK_1970_ThreeBoys_flk.113</ta>
            <ta e="T742" id="Seg_4334" s="T737">ChPK_1970_ThreeBoys_flk.114</ta>
            <ta e="T747" id="Seg_4335" s="T742">ChPK_1970_ThreeBoys_flk.115</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T4" id="Seg_4336" s="T0">Былыр-былыр биир ыраактаагы олорбут.</ta>
            <ta e="T8" id="Seg_4337" s="T4">Огото һуок этэ эбит.</ta>
            <ta e="T12" id="Seg_4338" s="T8">Биирдэ бу ыраактаагы һанаабыт: </ta>
            <ta e="T17" id="Seg_4339" s="T12">"Бу того биһиги огото һуокпутуй?</ta>
            <ta e="T20" id="Seg_4340" s="T17">Кырдьагастартан ыйыппыт киһи".</ta>
            <ta e="T25" id="Seg_4341" s="T20">Бу һаныы олордогуна, дьактара киирбит:</ta>
            <ta e="T28" id="Seg_4342" s="T25">— Тугу һанааргаатыҥ, догоо?</ta>
            <ta e="T32" id="Seg_4343" s="T28">— һарсыын биһиги киһилэри комуйуокпут.</ta>
            <ta e="T40" id="Seg_4344" s="T32">Каныга дьиэри иччитэк истиэнэни көрөн олоруокпутуй, огото һуок?</ta>
            <ta e="T44" id="Seg_4345" s="T40">Ыйытыак того огото һуокпутуй?</ta>
            <ta e="T48" id="Seg_4346" s="T44">Кимиэкэ баайбытын биэриэкпитий, өллөкпүтүнэ?</ta>
            <ta e="T54" id="Seg_4347" s="T48">һарсиэрда турбуттара, үгүүс багайы киһи комуллубут.</ta>
            <ta e="T57" id="Seg_4348" s="T54">Онно ыраактаагы ыйыппыт:</ta>
            <ta e="T62" id="Seg_4349" s="T57">— Бу того биһиги огото һуокпутуй?</ta>
            <ta e="T70" id="Seg_4350" s="T62">Кайтак, тугу гыннакка, оголонуокпутуй, эһиги кайтак дьии һаныыгыт?</ta>
            <ta e="T73" id="Seg_4351" s="T70">Киһилэр айдаайа түспүттэр.</ta>
            <ta e="T79" id="Seg_4352" s="T73">Аанынан кырдьагас һоккор огонньор былтас гыммыт.</ta>
            <ta e="T84" id="Seg_4353" s="T79">һоккор эрэйдээги ортого үтэн кээспиттэр:</ta>
            <ta e="T88" id="Seg_4354" s="T84">— Бу баар билэр киһи.</ta>
            <ta e="T96" id="Seg_4355" s="T88">Огонньор өрө көрөн бараан, һис туттан бараан, дьиэбит:</ta>
            <ta e="T103" id="Seg_4356" s="T96">— Оҥоруҥ көмүс илимнэ, көмүс тыыта, көмүс эрдиитэ.</ta>
            <ta e="T107" id="Seg_4357" s="T103">Онтон ыраактаагы балыктыы бардын.</ta>
            <ta e="T113" id="Seg_4358" s="T107">Илимигэр балык тутуо, ону дьактара һиэтин.</ta>
            <ta e="T116" id="Seg_4359" s="T113">Ыраактаагы дьактара күлбүт:</ta>
            <ta e="T120" id="Seg_4360" s="T116">— Онтон мин тотуом дуо?</ta>
            <ta e="T130" id="Seg_4361" s="T120">Өр буолуой, ыраактаагы һарсыҥҥытын көмүс тыылаак, көмүс эрдиилээк балыктана барбыт.</ta>
            <ta e="T136" id="Seg_4362" s="T130">Биир балыгы илибирэтэн тыыннаактыы эгэлбэт дуу.</ta>
            <ta e="T143" id="Seg_4363" s="T136">— Бу бултум, — дьиэбит да асчыттарыгар биэрэн кэбиспит.</ta>
            <ta e="T145" id="Seg_4364" s="T143">— һаныкаан күөстээҥ!</ta>
            <ta e="T157" id="Seg_4365" s="T145">Асчыт балык катырыгын от иһигэр ыраастаан кээспит — ону ыраактаагы коруобата һиэн кээспит.</ta>
            <ta e="T168" id="Seg_4366" s="T157">Ыраактаагы дьактара балыгы һиэн баран оҥуогун тэриэлкэгэ каалларбытын асчыт ыкка быракпыт.</ta>
            <ta e="T172" id="Seg_4367" s="T168">Олоҥкого өр буолуо дуо?</ta>
            <ta e="T176" id="Seg_4368" s="T172">Үһүөннэрэ үс уолу төрөөбүттэр.</ta>
            <ta e="T183" id="Seg_4369" s="T176">Оголор төһө да буолбакка бииргэ оонньуур буолбуттар.</ta>
            <ta e="T188" id="Seg_4370" s="T183">Биирдэ арай ыраактаагы уола дьиэбит:</ta>
            <ta e="T191" id="Seg_4371" s="T188">— Доготтор, тыылана барыагыҥ.</ta>
            <ta e="T197" id="Seg_4372" s="T191">Уолаттар төһө өр эрдэн испиттэрэ буолла.</ta>
            <ta e="T205" id="Seg_4373" s="T197">Көрбүттэрэ: эбэ онуор улакан багайы балаган дьиэ турар.</ta>
            <ta e="T208" id="Seg_4374" s="T205">— Ити туокпутуй? — дэспиттэр.</ta>
            <ta e="T210" id="Seg_4375" s="T208">Ыраактаагы уола:</ta>
            <ta e="T213" id="Seg_4376" s="T210">— Тиксиэгиҥ, каартылыакпыт, — дьиэбит.</ta>
            <ta e="T215" id="Seg_4377" s="T213">Ыт уола:</ta>
            <ta e="T217" id="Seg_4378" s="T215">— Э, того?</ta>
            <ta e="T221" id="Seg_4379" s="T217">Багар, абааһылар дьиэлэрэ буолуо?</ta>
            <ta e="T223" id="Seg_4380" s="T221">Коруоба уола:</ta>
            <ta e="T225" id="Seg_4381" s="T223">— Тиксиэгиҥ, тиксиэгиҥ.</ta>
            <ta e="T227" id="Seg_4382" s="T225">Каамалыы түһүөгүҥ.</ta>
            <ta e="T234" id="Seg_4383" s="T227">Кытылга таксаат, бу икки уол каартылыы олорбуттар.</ta>
            <ta e="T238" id="Seg_4384" s="T234">Ыт уола утуйан каалбыт.</ta>
            <ta e="T247" id="Seg_4385" s="T238">Төһө өр оонньообуттара буолла, бу оголор эмиэ утуйан каалбыттар.</ta>
            <ta e="T256" id="Seg_4386" s="T247">Ыт уола түүн орто уһуктубута — киһи каамар тыаһа иһиллибит.</ta>
            <ta e="T263" id="Seg_4387" s="T256">Таксыбыта — абааһы… балаган дьиэк кааман иһээктиир дьэ.</ta>
            <ta e="T272" id="Seg_4388" s="T263">"О дьэ, өлбүппүт эбит", — дьиэн баран доготторун уһугуннара һатаабыт.</ta>
            <ta e="T276" id="Seg_4389" s="T272">Бу уолаттар того уһуктуоктарай.</ta>
            <ta e="T283" id="Seg_4390" s="T276">— Кэл-кэл, мин эньигин һиэм, — абааһы һаҥата иһиллибит.</ta>
            <ta e="T291" id="Seg_4391" s="T283">Бу уол такса көппүт да абааһыны гытта өлөрсүбүт.</ta>
            <ta e="T300" id="Seg_4392" s="T291">Төһө да буолбакка абааһы мэньиитин болдок тааска кайа оксубут.</ta>
            <ta e="T313" id="Seg_4393" s="T300">Ол гынан баран иргэтин иһиттэн һуруктаак тааһы орообут, тааһы кармаанын иһинэн уктан кээспит.</ta>
            <ta e="T322" id="Seg_4394" s="T313">һарсиэрда туок да буолбатак көрдүк ыт уола доготторун уһугуннартаабыт:</ta>
            <ta e="T325" id="Seg_4395" s="T322">— Чэй, каастана барыагыҥ.</ta>
            <ta e="T331" id="Seg_4396" s="T325">Уолаттар тыыллаҥнаан арыыччы туран чаай испиттэр.</ta>
            <ta e="T334" id="Seg_4397" s="T331">Олоҥкого күн кылгас.</ta>
            <ta e="T339" id="Seg_4398" s="T334">Төһө да буолбакка каллаан караҥарбыт.</ta>
            <ta e="T349" id="Seg_4399" s="T339">Ыт уола аһаанһиэн баран утуйан каалбыт, доготторо эмиэ каартылыы олорбуттар.</ta>
            <ta e="T357" id="Seg_4400" s="T349">Түүн орто уһуктубута — доготторо олордо олорбутунан утуйан каалбыттар.</ta>
            <ta e="T370" id="Seg_4401" s="T357">Бу ого уһуктан баран иһиллии һыппыт — ыраак канна ирэ эмиэ каамар тыас иһиллибит.</ta>
            <ta e="T377" id="Seg_4402" s="T370">Доготторун уһугуннарбыта — того уһуктуоктарай чубу утуйбут киһилэр.</ta>
            <ta e="T389" id="Seg_4403" s="T377">Ыт уола таксыбыт да иккис абааһы мэньиитин эмиэ болдок тааска кампы оксубут.</ta>
            <ta e="T395" id="Seg_4404" s="T389">Абааһы тэбиэлэнэ түһэн баран чиркэс гыммыт.</ta>
            <ta e="T406" id="Seg_4405" s="T395">Ыт уола иргэтиттэн һуруктаак тааһы ороото дагыны кармааныгар эмиэ уктан кээспит.</ta>
            <ta e="T417" id="Seg_4406" s="T406">Үһүс түүнүгэр үһүс абааһы мэньиитин кайа оксон һуруктаак тааһы оруу оксубут.</ta>
            <ta e="T422" id="Seg_4407" s="T417">Доготторун им турарын кытта уһугуннарбыт.</ta>
            <ta e="T428" id="Seg_4408" s="T422">— Чэ, дьиэбитигэр барыагыҥ, — дьиэбит бу уол.</ta>
            <ta e="T431" id="Seg_4409" s="T428">Ыраактаагы уола өчөспүт:</ta>
            <ta e="T433" id="Seg_4410" s="T431">— Того доо!</ta>
            <ta e="T436" id="Seg_4411" s="T433">Олоро түһүөгүҥ, бэсэлээ.</ta>
            <ta e="T442" id="Seg_4412" s="T436">Коруоба уола олоро түһэн баран дьиэбит:</ta>
            <ta e="T446" id="Seg_4413" s="T442">— Кэргэттэрбит күүтүөктэрэ, барыагыҥ ээт.</ta>
            <ta e="T448" id="Seg_4414" s="T446">Уолаттар камнаабыттар.</ta>
            <ta e="T452" id="Seg_4415" s="T448">Ыт уола эрдэн испит.</ta>
            <ta e="T458" id="Seg_4416" s="T452">Төһө да буолбакка кыынын туттан баран:</ta>
            <ta e="T464" id="Seg_4417" s="T458">— Доготтоор, баһакпын каалларбыппын, көрдүү барыам, — дьиэбит.</ta>
            <ta e="T472" id="Seg_4418" s="T464">Ыт уола балагаҥҥа кэлбитэ дьактаттар ытыыр һаҥалара иһиллибит.</ta>
            <ta e="T482" id="Seg_4419" s="T472">— Мин минньигэс ас буолан баран улакан тэриэлкэгэ тыыларын иһигэр түһүөм.</ta>
            <ta e="T490" id="Seg_4420" s="T482">Миньигин һиэтэктэринэ өлөн каалыактара, — дьиэбит кардьагас Дьигэ бааба куолаһа.</ta>
            <ta e="T506" id="Seg_4421" s="T490">— Мин буоллагына каһан да көрбөтөк боскуой һолко пылаат буолан ыраактаагы уолун төһөгүгэр түһүөм, барыкааттарын моҥуннаран өлөрүөм.</ta>
            <ta e="T518" id="Seg_4422" s="T506">— Мин буоллагына тыыларын ипсэри һилимниир гына уу һымала буолуом, барыларын оборон тимирдиэм.</ta>
            <ta e="T520" id="Seg_4423" s="T518">— Дьэ үчүгэй.</ta>
            <ta e="T526" id="Seg_4424" s="T520">Бэртик һанаабыккыт эбит, — дьиэбит маннайгы дьактар.</ta>
            <ta e="T540" id="Seg_4425" s="T526">Ыт уола итини истибит дааганы һыыр анныгар һүүрэн киирбит, тыытын ылбактыыр да эрдэн испит.</ta>
            <ta e="T545" id="Seg_4426" s="T540">Доготторун эбэ ортотугар һитэ оксубут.</ta>
            <ta e="T549" id="Seg_4427" s="T545">Уолаттар оргуйакаан эрдэн испиттэр.</ta>
            <ta e="T557" id="Seg_4428" s="T549">Төһө өр буолуой, тыыларын иһигэр астаак тэриэлкэ түспүт.</ta>
            <ta e="T573" id="Seg_4429" s="T557">Ыраактаагы уола үөрбүт, илиитин тэриэлкэ дьиэк һарбас гынарын гытта ыт уола астаак тэриэлкэни ууга быраган кээспит.</ta>
            <ta e="T586" id="Seg_4430" s="T573">Ыраактаагы уола кыыһыран һунтуйан олордогуна, боскуой багайы һолко пулаат ыраактаагы уолун төһөгүгэр түспүт.</ta>
            <ta e="T607" id="Seg_4431" s="T586">— О, таҥара, маамабар кэһии гыныам, — дьиэбит да кармааныгар уктаары гынарын гытта уола талаан ылбыт, баһагынан илчи кэрдэн баран ууга быраган кээспит.</ta>
            <ta e="T613" id="Seg_4432" s="T607">Ыраактаагы уола кыыһыран һаҥата да таксыбатак.</ta>
            <ta e="T624" id="Seg_4433" s="T613">Төһө өр буолуой, тыылара ууга һыстан каалбыт, эбэ уута караарт гыммыт.</ta>
            <ta e="T630" id="Seg_4434" s="T624">Бу уолаттар һатаан эрдиммэт буолан каалбыттар.</ta>
            <ta e="T631" id="Seg_4435" s="T630">Куттаммыттар.</ta>
            <ta e="T633" id="Seg_4436" s="T631">Ыт уола:</ta>
            <ta e="T645" id="Seg_4437" s="T633">— Ууну һүгэннэн быһыта кэрдиҥ, баһагынан тэһитэ анньыҥ, мин эрдэн иһиэм, — дьиэбит доготторугар.</ta>
            <ta e="T651" id="Seg_4438" s="T645">Көрбүттэрэ — уулара кирдик һырдаан, кытаран испит.</ta>
            <ta e="T654" id="Seg_4439" s="T651">һотору тыылара босколоммут.</ta>
            <ta e="T658" id="Seg_4440" s="T654">Уолаттар эрдинэн испиттэр оргууйакаан.</ta>
            <ta e="T667" id="Seg_4441" s="T658">Төһө да өр буолбакка эбэ үрдүк кытылыгар гуораттара көстүбүт.</ta>
            <ta e="T674" id="Seg_4442" s="T667">Тиксэн баран уолаттар үс аҥы араксан каалбыттар.</ta>
            <ta e="T693" id="Seg_4443" s="T674">Ыраактаагы уола һыыр үрдүгэр чээлкээ дьиэгэ барбыт, ыт уола мааматын ньамчыгас короон дьиэтигэр киирбит, коруоба уола таһаараа иньэтин көрсүбүт.</ta>
            <ta e="T697" id="Seg_4444" s="T693">Һарсиэрда ыраактаагы уолаттары ыгыртаталаабыт:</ta>
            <ta e="T705" id="Seg_4445" s="T697">— Чэ оголорум, кэпсээҥ тугу гына бу туркары һылдьыбыккытый?</ta>
            <ta e="T721" id="Seg_4446" s="T705">— Үс абааһы иргэтиттэн үс һуруктаак тааһы булбутум, — ыт уола дьиэбит, ыраактаагы илиитигэр үс тааһы туттаран кээспит.</ta>
            <ta e="T731" id="Seg_4447" s="T721">Ыыраактаагы ыт уолун моойуттан кууһан ылбыт, ытамсыйа түһэн баран дьиэбит:</ta>
            <ta e="T737" id="Seg_4448" s="T731">— Бүгүҥҥүттэн эн ыраактаагы буолар ыйаагыҥ кэллэ.</ta>
            <ta e="T742" id="Seg_4449" s="T737">Уолбун кытта убай-балыс буолан олоруҥ.</ta>
            <ta e="T747" id="Seg_4450" s="T742">Үс уол догордуу байан-тайан олорбуттар.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_4451" s="T0">Bɨlɨr-bɨlɨr biːr ɨraːktaːgɨ olorbut. </ta>
            <ta e="T8" id="Seg_4452" s="T4">Ogoto hu͡ok ete ebit. </ta>
            <ta e="T12" id="Seg_4453" s="T8">Biːrde bu ɨraːktaːgɨ hanaːbɨt: </ta>
            <ta e="T17" id="Seg_4454" s="T12">"Bu togo bihigi ogoto hu͡okputuj? </ta>
            <ta e="T20" id="Seg_4455" s="T17">Kɨrdʼagastartan ɨjɨppɨt kihi." </ta>
            <ta e="T25" id="Seg_4456" s="T20">Bu hanɨː olordoguna, dʼaktara kiːrbit:</ta>
            <ta e="T28" id="Seg_4457" s="T25">"Tugu hanaːrgaːtɨŋ, dogoː?" </ta>
            <ta e="T32" id="Seg_4458" s="T28">"Harsɨːn bihigi kihileri komuju͡okput. </ta>
            <ta e="T40" id="Seg_4459" s="T32">Kanɨga dʼi͡eri iččitek isti͡eneni körön oloru͡okputuj, ogoto hu͡ok? </ta>
            <ta e="T44" id="Seg_4460" s="T40">ɨjɨtɨ͡ak togo ogoto hu͡okputuj? </ta>
            <ta e="T48" id="Seg_4461" s="T44">Kimi͡eke baːjbɨtɨn bi͡eri͡ekpitij, öllökpütüne?" </ta>
            <ta e="T54" id="Seg_4462" s="T48">Harsi͡erda turbuttara, ügüːs bagajɨ kihi komullubut. </ta>
            <ta e="T57" id="Seg_4463" s="T54">Onno ɨraːktaːgɨ ɨjɨppɨt:</ta>
            <ta e="T62" id="Seg_4464" s="T57">"Bu togo bihigi ogoto hu͡okputuj? </ta>
            <ta e="T70" id="Seg_4465" s="T62">Kajtak, tugu gɨnnakka, ogolonu͡okputuj, ehigi kajtak dʼiː hanɨːgɨt?" </ta>
            <ta e="T73" id="Seg_4466" s="T70">Kihiler ajdaːja tüspütter. </ta>
            <ta e="T79" id="Seg_4467" s="T73">Aːnɨnan kɨrdʼagas hokkor ogonnʼor bɨltas gɨmmɨt. </ta>
            <ta e="T84" id="Seg_4468" s="T79">Hokkor erejdeːgi ortogo üten keːspitter:</ta>
            <ta e="T88" id="Seg_4469" s="T84">"Bu baːr biler kihi." </ta>
            <ta e="T96" id="Seg_4470" s="T88">Ogonnʼor örö körön baraːn, his tuttan baraːn, dʼi͡ebit:</ta>
            <ta e="T103" id="Seg_4471" s="T96">"Oŋoruŋ kömüs ilimne, kömüs tɨːta, kömüs erdiːte. </ta>
            <ta e="T107" id="Seg_4472" s="T103">Onton ɨraːktaːgɨ balɨktɨː bardɨn. </ta>
            <ta e="T113" id="Seg_4473" s="T107">Ilimiger balɨk tutu͡o, onu dʼaktara hi͡etin." </ta>
            <ta e="T116" id="Seg_4474" s="T113">ɨraːktaːgɨ dʼaktara külbüt:</ta>
            <ta e="T120" id="Seg_4475" s="T116">"Onton min totu͡om du͡o?" </ta>
            <ta e="T130" id="Seg_4476" s="T120">Ör bu͡olu͡oj, ɨraːktaːgɨ harsɨŋŋɨtɨn kömüs tɨːlaːk, kömüs erdiːleːk balɨktana barbɨt. </ta>
            <ta e="T136" id="Seg_4477" s="T130">Biːr balɨgɨ ilibireten tɨːnnaːktɨː egelbet duː. </ta>
            <ta e="T143" id="Seg_4478" s="T136">"Bu bultum", dʼi͡ebit da asčɨttarɨgar bi͡eren kebispit. </ta>
            <ta e="T145" id="Seg_4479" s="T143">"Hanɨkaːn kü͡östeːŋ!" </ta>
            <ta e="T157" id="Seg_4480" s="T145">Asčɨt balɨk katɨrɨgɨn ot ihiger ɨraːstaːn keːspit — onu ɨraːktaːgɨ koru͡obata hi͡en keːspit. </ta>
            <ta e="T168" id="Seg_4481" s="T157">ɨraːktaːgɨ dʼaktara balɨgɨ hi͡en baran oŋu͡ogun teri͡elkege kaːllarbɨtɨn asčɨt ɨkka bɨrakpɨt. </ta>
            <ta e="T172" id="Seg_4482" s="T168">Oloŋkogo ör bu͡olu͡o du͡o? </ta>
            <ta e="T176" id="Seg_4483" s="T172">Ühü͡önnere üs u͡olu töröːbütter. </ta>
            <ta e="T183" id="Seg_4484" s="T176">Ogolor töhö da bu͡olbakka biːrge oːnnʼuːr bu͡olbuttar. </ta>
            <ta e="T188" id="Seg_4485" s="T183">Biːrde araj ɨraːktaːgɨ u͡ola dʼi͡ebit:</ta>
            <ta e="T191" id="Seg_4486" s="T188">"Dogottor, tɨːlana barɨ͡agɨŋ." </ta>
            <ta e="T197" id="Seg_4487" s="T191">U͡olattar töhö ör erden ispittere bu͡olla. </ta>
            <ta e="T205" id="Seg_4488" s="T197">Körbüttere, ebe onu͡or ulakan bagajɨ balagan dʼi͡e turar. </ta>
            <ta e="T208" id="Seg_4489" s="T205">"Iti tu͡okputuj?", despitter. </ta>
            <ta e="T210" id="Seg_4490" s="T208">ɨraːktaːgɨ u͡ola:</ta>
            <ta e="T213" id="Seg_4491" s="T210">"Tiksi͡egiŋ, kaːrtɨlɨ͡akpɨt", dʼi͡ebit. </ta>
            <ta e="T215" id="Seg_4492" s="T213">ɨt u͡ola:</ta>
            <ta e="T217" id="Seg_4493" s="T215">"E, togo? </ta>
            <ta e="T221" id="Seg_4494" s="T217">Bagar, abaːhɨlar dʼi͡elere bu͡olu͡o?" </ta>
            <ta e="T223" id="Seg_4495" s="T221">Koru͡oba u͡ola:</ta>
            <ta e="T225" id="Seg_4496" s="T223">"Tiksi͡egiŋ, tiksi͡egiŋ. </ta>
            <ta e="T227" id="Seg_4497" s="T225">Kaːmalɨː tühü͡ögüŋ." </ta>
            <ta e="T234" id="Seg_4498" s="T227">Kɨtɨlga taksaːt, bu ikki u͡ol kaːrtɨlɨː olorbuttar. </ta>
            <ta e="T238" id="Seg_4499" s="T234">ɨt u͡ola utujan kaːlbɨt. </ta>
            <ta e="T247" id="Seg_4500" s="T238">Töhö ör oːnnʼoːbuttara bu͡olla, bu ogolor emi͡e utujan kaːlbɨttar. </ta>
            <ta e="T256" id="Seg_4501" s="T247">ɨt u͡ola tüːn orto uhuktubuta — kihi kaːmar tɨ͡aha ihillibit. </ta>
            <ta e="T263" id="Seg_4502" s="T256">Taksɨbɨta — abaːhɨ, balagan dʼi͡ek kaːman iheːktiːr dʼe. </ta>
            <ta e="T272" id="Seg_4503" s="T263">"O dʼe, ölbüppüt ebit", dʼi͡en baran dogottorun uhugunnara hataːbɨt. </ta>
            <ta e="T276" id="Seg_4504" s="T272">Bu u͡olattar togo uhuktu͡oktaraj. </ta>
            <ta e="T283" id="Seg_4505" s="T276">"Kel-kel, min enʼigin hi͡em", abaːhɨ haŋata ihillibit. </ta>
            <ta e="T291" id="Seg_4506" s="T283">Bu u͡ol taksa köppüt da abaːhɨnɨ gɨtta ölörsübüt. </ta>
            <ta e="T300" id="Seg_4507" s="T291">Töhö da bu͡olbakka abaːhɨ menʼiːtin boldok taːska kaja oksubut. </ta>
            <ta e="T313" id="Seg_4508" s="T300">Ol gɨnan baran irgetin ihitten huruktaːk taːhɨ oroːbut, taːhɨ karmaːnɨn ihinen uktan keːspit. </ta>
            <ta e="T322" id="Seg_4509" s="T313">Harsi͡erda tu͡ok da bu͡olbatak kördük ɨt u͡ola dogottorun uhugunnartaːbɨt:</ta>
            <ta e="T325" id="Seg_4510" s="T322">"Čej, kaːstana barɨ͡agɨŋ." </ta>
            <ta e="T331" id="Seg_4511" s="T325">U͡olattar tɨːllaŋnaːn arɨːččɨ turan čaːj ispitter. </ta>
            <ta e="T334" id="Seg_4512" s="T331">Oloŋkogo kün kɨlgas. </ta>
            <ta e="T339" id="Seg_4513" s="T334">Töhö da bu͡olbakka kallaːn karaŋarbɨt. </ta>
            <ta e="T349" id="Seg_4514" s="T339">ɨt u͡ola ahaːnhi͡en baran utujan kaːlbɨt, dogottoro emi͡e kaːrtɨlɨː olorbuttar. </ta>
            <ta e="T357" id="Seg_4515" s="T349">Tüːn orto uhuktubuta — dogottoro olordo olorbutunan utujan kaːlbɨttar. </ta>
            <ta e="T370" id="Seg_4516" s="T357">Bu ogo uhuktan baran ihilliː hɨppɨt — ɨraːk kanna ire emi͡e kaːmar tɨ͡as ihillibit. </ta>
            <ta e="T377" id="Seg_4517" s="T370">Dogottorun uhugunnarbɨta — togo uhuktu͡oktaraj čubu utujbut kihiler. </ta>
            <ta e="T389" id="Seg_4518" s="T377">ɨt u͡ola taksɨbɨt da ikkis abaːhɨ menʼiːtin emi͡e boldok taːska kampɨ oksubut. </ta>
            <ta e="T395" id="Seg_4519" s="T389">Abaːhɨ tebi͡elene tühen baran čirkes gɨmmɨt. </ta>
            <ta e="T406" id="Seg_4520" s="T395">ɨt u͡ola irgetitten huruktaːk taːhɨ oroːto dagɨnɨ karmaːnɨgar emi͡e uktan keːspit. </ta>
            <ta e="T417" id="Seg_4521" s="T406">Ühüs tüːnüger ühüs abaːhɨ menʼiːtin kaja okson huruktaːk taːhɨ oruː oksubut. </ta>
            <ta e="T422" id="Seg_4522" s="T417">Dogottorun im turarɨn kɨtta uhugunnarbɨt. </ta>
            <ta e="T428" id="Seg_4523" s="T422">"Če, dʼi͡ebitiger barɨ͡agɨŋ", dʼi͡ebit bu u͡ol. </ta>
            <ta e="T431" id="Seg_4524" s="T428">ɨraːktaːgɨ u͡ola öčöspüt:</ta>
            <ta e="T433" id="Seg_4525" s="T431">"Togo doː! </ta>
            <ta e="T436" id="Seg_4526" s="T433">Oloro tühü͡ögüŋ, beseleː." </ta>
            <ta e="T442" id="Seg_4527" s="T436">Koru͡oba u͡ola oloro tühen baran dʼi͡ebit:</ta>
            <ta e="T446" id="Seg_4528" s="T442">"Kergetterbit küːtü͡öktere, barɨ͡agɨŋ eːt." </ta>
            <ta e="T448" id="Seg_4529" s="T446">U͡olattar kamnaːbɨttar. </ta>
            <ta e="T452" id="Seg_4530" s="T448">ɨt u͡ola erden ispit. </ta>
            <ta e="T458" id="Seg_4531" s="T452">Töhö da bu͡olbakka kɨːnɨn tuttan baran:</ta>
            <ta e="T464" id="Seg_4532" s="T458">"Dogottoːr, bahakpɨn kaːllarbɨppɨn, kördüː barɨ͡am", dʼi͡ebit. </ta>
            <ta e="T472" id="Seg_4533" s="T464">ɨt u͡ola balagaŋŋa kelbite dʼaktattar ɨtɨːr haŋalara ihillibit. </ta>
            <ta e="T482" id="Seg_4534" s="T472">"Min minnʼiges as bu͡olan baran ulakan teri͡elkege tɨːlarɨn ihiger tühü͡öm. </ta>
            <ta e="T490" id="Seg_4535" s="T482">Minʼigin hi͡etekterine ölön kaːlɨ͡aktara", dʼi͡ebit kardʼagas Dʼige baːba ku͡olaha. </ta>
            <ta e="T506" id="Seg_4536" s="T490">"Min bu͡ollagɨna kahan da körbötök bosku͡oj holko pɨlaːt bu͡olan ɨraːktaːgɨ u͡olun töhögüger tühü͡öm, barɨkaːttarɨn moŋunnaran ölörü͡öm." </ta>
            <ta e="T518" id="Seg_4537" s="T506">"Min bu͡ollagɨna tɨːlarɨn ipseri hilimniːr gɨna uː hɨmala bu͡olu͡om, barɨlarɨn oboron timirdi͡em." </ta>
            <ta e="T520" id="Seg_4538" s="T518">"Dʼe üčügej. </ta>
            <ta e="T526" id="Seg_4539" s="T520">Bertik hanaːbɨkkɨt ebit", dʼi͡ebit mannajgɨ dʼaktar. </ta>
            <ta e="T540" id="Seg_4540" s="T526">ɨt u͡ola itini istibit daːganɨ hɨːr annɨgar hüːren kiːrbit, tɨːtɨn ɨlbaktɨːr da erden ispit. </ta>
            <ta e="T545" id="Seg_4541" s="T540">Dogottorun ebe ortotugar hite oksubut. </ta>
            <ta e="T549" id="Seg_4542" s="T545">U͡olattar orgujakaːn erden ispitter. </ta>
            <ta e="T557" id="Seg_4543" s="T549">Töhö ör bu͡olu͡oj, tɨːlarɨn ihiger astaːk teri͡elke tüspüt. </ta>
            <ta e="T573" id="Seg_4544" s="T557">ɨraːktaːgɨ u͡ola ü͡örbüt, iliːtin teri͡elke dʼi͡ek harbas gɨnarɨn gɨtta ɨt u͡ola astaːk teri͡elkeni uːga bɨragan keːspit. </ta>
            <ta e="T586" id="Seg_4545" s="T573">ɨraːktaːgɨ u͡ola kɨːhɨran huntujan olordoguna, bosku͡oj bagajɨ holko pulaːt ɨraːktaːgɨ u͡olun töhögüger tüspüt. </ta>
            <ta e="T607" id="Seg_4546" s="T586">"O, taŋara, maːmabar kehiː gɨnɨ͡am", dʼi͡ebit da karmaːnɨgar uktaːrɨ gɨnarɨn gɨtta u͡ola talaːn ɨlbɨt, bahagɨnan ilči kerden baran uːga bɨragan keːspit. </ta>
            <ta e="T613" id="Seg_4547" s="T607">ɨraːktaːgɨ u͡ola kɨːhɨran haŋata da taksɨbatak. </ta>
            <ta e="T624" id="Seg_4548" s="T613">Töhö ör bu͡olu͡oj, tɨːlara uːga hɨstan kaːlbɨt, ebe uːta karaːrt gɨmmɨt. </ta>
            <ta e="T630" id="Seg_4549" s="T624">Bu u͡olattar hataːn erdimmet bu͡olan kaːlbɨttar. </ta>
            <ta e="T631" id="Seg_4550" s="T630">Kuttammɨttar. </ta>
            <ta e="T633" id="Seg_4551" s="T631">ɨt u͡ola:</ta>
            <ta e="T645" id="Seg_4552" s="T633">"Uːnu hügennen bɨhɨta kerdiŋ, bahagɨnan tehite annʼɨŋ, min erden ihi͡em", dʼi͡ebit dogottorugar. </ta>
            <ta e="T651" id="Seg_4553" s="T645">Körbüttere — uːlara kirdik hɨrdaːn, kɨtaran ispit. </ta>
            <ta e="T654" id="Seg_4554" s="T651">Hotoru tɨːlara boskolommut. </ta>
            <ta e="T658" id="Seg_4555" s="T654">U͡olattar erdinen ispitter orguːjakaːn. </ta>
            <ta e="T667" id="Seg_4556" s="T658">Töhö da ör bu͡olbakka ebe ürdük kɨtɨlɨgar gu͡orattara köstübüt. </ta>
            <ta e="T674" id="Seg_4557" s="T667">Tiksen baran u͡olattar üs aŋɨ araksan kaːlbɨttar. </ta>
            <ta e="T693" id="Seg_4558" s="T674">ɨraːktaːgɨ u͡ola hɨːr ürdüger čeːlkeː dʼi͡ege barbɨt, ɨt u͡ola maːmatɨn nʼamčɨgas koroːn dʼi͡etiger kiːrbit, koru͡oba u͡ola tahaːraː inʼetin körsübüt. </ta>
            <ta e="T697" id="Seg_4559" s="T693">Harsi͡erda ɨraːktaːgɨ u͡olattarɨ ɨgɨrtatalaːbɨt:</ta>
            <ta e="T705" id="Seg_4560" s="T697">"Če ogolorum, kepseːŋ tugu gɨna bu turkarɨ hɨldʼɨbɨkkɨtɨj?" </ta>
            <ta e="T721" id="Seg_4561" s="T705">"Üs abaːhɨ irgetitten üs huruktaːk taːhɨ bulbutum", ɨt u͡ola dʼi͡ebit, ɨraːktaːgɨ iliːtiger üs taːhɨ tuttaran keːspit. </ta>
            <ta e="T731" id="Seg_4562" s="T721">ɨːraːktaːgɨ ɨt u͡olun moːjuttan kuːhan ɨlbɨt, ɨtamsɨja tühen baran dʼi͡ebit:</ta>
            <ta e="T737" id="Seg_4563" s="T731">"Bügüŋŋütten en ɨraːktaːgɨ bu͡olar ɨjaːgɨŋ kelle. </ta>
            <ta e="T742" id="Seg_4564" s="T737">U͡olbun kɨtta ubaj-balɨs bu͡olan oloruŋ." </ta>
            <ta e="T747" id="Seg_4565" s="T742">Üs u͡ol dogorduː bajan-tajan olorbuttar. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_4566" s="T0">bɨlɨr-bɨlɨr</ta>
            <ta e="T2" id="Seg_4567" s="T1">biːr</ta>
            <ta e="T3" id="Seg_4568" s="T2">ɨraːktaːgɨ</ta>
            <ta e="T4" id="Seg_4569" s="T3">olor-but</ta>
            <ta e="T5" id="Seg_4570" s="T4">ogo-to</ta>
            <ta e="T6" id="Seg_4571" s="T5">hu͡ok</ta>
            <ta e="T7" id="Seg_4572" s="T6">e-t-e</ta>
            <ta e="T8" id="Seg_4573" s="T7">e-bit</ta>
            <ta e="T9" id="Seg_4574" s="T8">biːrde</ta>
            <ta e="T10" id="Seg_4575" s="T9">bu</ta>
            <ta e="T11" id="Seg_4576" s="T10">ɨraːktaːgɨ</ta>
            <ta e="T12" id="Seg_4577" s="T11">hanaː-bɨt</ta>
            <ta e="T13" id="Seg_4578" s="T12">bu</ta>
            <ta e="T14" id="Seg_4579" s="T13">togo</ta>
            <ta e="T15" id="Seg_4580" s="T14">bihigi</ta>
            <ta e="T16" id="Seg_4581" s="T15">ogo-to</ta>
            <ta e="T17" id="Seg_4582" s="T16">hu͡ok-put=uj</ta>
            <ta e="T18" id="Seg_4583" s="T17">kɨrdʼagas-tar-tan</ta>
            <ta e="T19" id="Seg_4584" s="T18">ɨjɨp-pɨt</ta>
            <ta e="T20" id="Seg_4585" s="T19">kihi</ta>
            <ta e="T21" id="Seg_4586" s="T20">bu</ta>
            <ta e="T22" id="Seg_4587" s="T21">han-ɨː</ta>
            <ta e="T23" id="Seg_4588" s="T22">olor-dog-una</ta>
            <ta e="T24" id="Seg_4589" s="T23">dʼaktar-a</ta>
            <ta e="T25" id="Seg_4590" s="T24">kiːr-bit</ta>
            <ta e="T26" id="Seg_4591" s="T25">tug-u</ta>
            <ta e="T27" id="Seg_4592" s="T26">hanaːrgaː-tɨ-ŋ</ta>
            <ta e="T28" id="Seg_4593" s="T27">dogoː</ta>
            <ta e="T29" id="Seg_4594" s="T28">harsɨːn</ta>
            <ta e="T30" id="Seg_4595" s="T29">bihigi</ta>
            <ta e="T31" id="Seg_4596" s="T30">kihi-ler-i</ta>
            <ta e="T32" id="Seg_4597" s="T31">komuj-u͡ok-put</ta>
            <ta e="T33" id="Seg_4598" s="T32">kanɨ-ga</ta>
            <ta e="T34" id="Seg_4599" s="T33">dʼi͡eri</ta>
            <ta e="T35" id="Seg_4600" s="T34">iččitek</ta>
            <ta e="T36" id="Seg_4601" s="T35">isti͡ene-ni</ta>
            <ta e="T37" id="Seg_4602" s="T36">kör-ön</ta>
            <ta e="T38" id="Seg_4603" s="T37">olor-u͡ok-put=uj</ta>
            <ta e="T39" id="Seg_4604" s="T38">ogo-to</ta>
            <ta e="T40" id="Seg_4605" s="T39">hu͡ok</ta>
            <ta e="T41" id="Seg_4606" s="T40">ɨjɨt-ɨ͡ak</ta>
            <ta e="T42" id="Seg_4607" s="T41">togo</ta>
            <ta e="T43" id="Seg_4608" s="T42">ogo-to</ta>
            <ta e="T44" id="Seg_4609" s="T43">hu͡ok-put=uj</ta>
            <ta e="T45" id="Seg_4610" s="T44">kimi͡e-ke</ta>
            <ta e="T46" id="Seg_4611" s="T45">baːj-bɨtɨ-n</ta>
            <ta e="T47" id="Seg_4612" s="T46">bi͡er-i͡ek-pit=ij</ta>
            <ta e="T48" id="Seg_4613" s="T47">öl-lök-pütüne</ta>
            <ta e="T49" id="Seg_4614" s="T48">harsi͡erda</ta>
            <ta e="T50" id="Seg_4615" s="T49">tur-but-tara</ta>
            <ta e="T51" id="Seg_4616" s="T50">ügüːs</ta>
            <ta e="T52" id="Seg_4617" s="T51">bagajɨ</ta>
            <ta e="T53" id="Seg_4618" s="T52">kihi</ta>
            <ta e="T54" id="Seg_4619" s="T53">komu-ll-u-but</ta>
            <ta e="T55" id="Seg_4620" s="T54">onno</ta>
            <ta e="T56" id="Seg_4621" s="T55">ɨraːktaːgɨ</ta>
            <ta e="T57" id="Seg_4622" s="T56">ɨjɨp-pɨt</ta>
            <ta e="T58" id="Seg_4623" s="T57">bu</ta>
            <ta e="T59" id="Seg_4624" s="T58">togo</ta>
            <ta e="T60" id="Seg_4625" s="T59">bihigi</ta>
            <ta e="T61" id="Seg_4626" s="T60">ogo-to</ta>
            <ta e="T62" id="Seg_4627" s="T61">hu͡ok-put=uj</ta>
            <ta e="T63" id="Seg_4628" s="T62">kajtak</ta>
            <ta e="T64" id="Seg_4629" s="T63">tug-u</ta>
            <ta e="T65" id="Seg_4630" s="T64">gɨn-nak-ka</ta>
            <ta e="T66" id="Seg_4631" s="T65">ogo-lon-u͡ok-put=uj</ta>
            <ta e="T67" id="Seg_4632" s="T66">ehigi</ta>
            <ta e="T68" id="Seg_4633" s="T67">kajtak</ta>
            <ta e="T69" id="Seg_4634" s="T68">dʼ-iː</ta>
            <ta e="T70" id="Seg_4635" s="T69">han-ɨː-gɨt</ta>
            <ta e="T71" id="Seg_4636" s="T70">kihi-ler</ta>
            <ta e="T72" id="Seg_4637" s="T71">ajdaːj-a</ta>
            <ta e="T73" id="Seg_4638" s="T72">tüs-püt-ter</ta>
            <ta e="T74" id="Seg_4639" s="T73">aːn-ɨ-nan</ta>
            <ta e="T75" id="Seg_4640" s="T74">kɨrdʼagas</ta>
            <ta e="T76" id="Seg_4641" s="T75">hokkor</ta>
            <ta e="T77" id="Seg_4642" s="T76">ogonnʼor</ta>
            <ta e="T78" id="Seg_4643" s="T77">bɨlta-s</ta>
            <ta e="T79" id="Seg_4644" s="T78">gɨm-mɨt</ta>
            <ta e="T80" id="Seg_4645" s="T79">hokkor</ta>
            <ta e="T81" id="Seg_4646" s="T80">erej-deːg-i</ta>
            <ta e="T82" id="Seg_4647" s="T81">orto-go</ta>
            <ta e="T83" id="Seg_4648" s="T82">üt-en</ta>
            <ta e="T84" id="Seg_4649" s="T83">keːs-pit-ter</ta>
            <ta e="T85" id="Seg_4650" s="T84">bu</ta>
            <ta e="T86" id="Seg_4651" s="T85">baːr</ta>
            <ta e="T87" id="Seg_4652" s="T86">bil-er</ta>
            <ta e="T88" id="Seg_4653" s="T87">kihi</ta>
            <ta e="T89" id="Seg_4654" s="T88">ogonnʼor</ta>
            <ta e="T90" id="Seg_4655" s="T89">örö</ta>
            <ta e="T91" id="Seg_4656" s="T90">kör-ön</ta>
            <ta e="T92" id="Seg_4657" s="T91">baraːn</ta>
            <ta e="T93" id="Seg_4658" s="T92">his</ta>
            <ta e="T94" id="Seg_4659" s="T93">tutt-an</ta>
            <ta e="T95" id="Seg_4660" s="T94">baraːn</ta>
            <ta e="T96" id="Seg_4661" s="T95">dʼi͡e-bit</ta>
            <ta e="T97" id="Seg_4662" s="T96">oŋor-u-ŋ</ta>
            <ta e="T98" id="Seg_4663" s="T97">kömüs</ta>
            <ta e="T99" id="Seg_4664" s="T98">ilim-ne</ta>
            <ta e="T100" id="Seg_4665" s="T99">kömüs</ta>
            <ta e="T101" id="Seg_4666" s="T100">tɨː-ta</ta>
            <ta e="T102" id="Seg_4667" s="T101">kömüs</ta>
            <ta e="T103" id="Seg_4668" s="T102">erdiː-te</ta>
            <ta e="T104" id="Seg_4669" s="T103">onton</ta>
            <ta e="T105" id="Seg_4670" s="T104">ɨraːktaːgɨ</ta>
            <ta e="T106" id="Seg_4671" s="T105">balɨkt-ɨː</ta>
            <ta e="T107" id="Seg_4672" s="T106">bar-dɨn</ta>
            <ta e="T108" id="Seg_4673" s="T107">ilim-i-ger</ta>
            <ta e="T109" id="Seg_4674" s="T108">balɨk</ta>
            <ta e="T110" id="Seg_4675" s="T109">tut-u͡o</ta>
            <ta e="T111" id="Seg_4676" s="T110">o-nu</ta>
            <ta e="T112" id="Seg_4677" s="T111">dʼaktar-a</ta>
            <ta e="T113" id="Seg_4678" s="T112">hi͡e-tin</ta>
            <ta e="T114" id="Seg_4679" s="T113">ɨraːktaːgɨ</ta>
            <ta e="T115" id="Seg_4680" s="T114">dʼaktar-a</ta>
            <ta e="T116" id="Seg_4681" s="T115">kül-büt</ta>
            <ta e="T117" id="Seg_4682" s="T116">on-ton</ta>
            <ta e="T118" id="Seg_4683" s="T117">min</ta>
            <ta e="T119" id="Seg_4684" s="T118">tot-u͡o-m</ta>
            <ta e="T120" id="Seg_4685" s="T119">du͡o</ta>
            <ta e="T121" id="Seg_4686" s="T120">ör</ta>
            <ta e="T122" id="Seg_4687" s="T121">bu͡ol-u͡o=j</ta>
            <ta e="T123" id="Seg_4688" s="T122">ɨraːktaːgɨ</ta>
            <ta e="T124" id="Seg_4689" s="T123">harsɨŋŋɨ-tɨ-n</ta>
            <ta e="T125" id="Seg_4690" s="T124">kömüs</ta>
            <ta e="T126" id="Seg_4691" s="T125">tɨː-laːk</ta>
            <ta e="T127" id="Seg_4692" s="T126">kömüs</ta>
            <ta e="T128" id="Seg_4693" s="T127">erdiː-leːk</ta>
            <ta e="T129" id="Seg_4694" s="T128">balɨk-tan-a</ta>
            <ta e="T130" id="Seg_4695" s="T129">bar-bɨt</ta>
            <ta e="T131" id="Seg_4696" s="T130">biːr</ta>
            <ta e="T132" id="Seg_4697" s="T131">balɨg-ɨ</ta>
            <ta e="T133" id="Seg_4698" s="T132">ilibiret-en</ta>
            <ta e="T134" id="Seg_4699" s="T133">tɨːnnaːk-tɨː</ta>
            <ta e="T135" id="Seg_4700" s="T134">egel-bet</ta>
            <ta e="T136" id="Seg_4701" s="T135">duː</ta>
            <ta e="T137" id="Seg_4702" s="T136">bu</ta>
            <ta e="T138" id="Seg_4703" s="T137">bult-u-m</ta>
            <ta e="T139" id="Seg_4704" s="T138">dʼi͡e-bit</ta>
            <ta e="T140" id="Seg_4705" s="T139">da</ta>
            <ta e="T141" id="Seg_4706" s="T140">asčɨt-tarɨ-gar</ta>
            <ta e="T142" id="Seg_4707" s="T141">bi͡er-en</ta>
            <ta e="T143" id="Seg_4708" s="T142">kebis-pit</ta>
            <ta e="T144" id="Seg_4709" s="T143">hanɨkaːn</ta>
            <ta e="T145" id="Seg_4710" s="T144">kü͡östeː-ŋ</ta>
            <ta e="T146" id="Seg_4711" s="T145">asčɨt</ta>
            <ta e="T147" id="Seg_4712" s="T146">balɨk</ta>
            <ta e="T148" id="Seg_4713" s="T147">katɨrɨg-ɨ-n</ta>
            <ta e="T149" id="Seg_4714" s="T148">ot</ta>
            <ta e="T150" id="Seg_4715" s="T149">ih-i-ger</ta>
            <ta e="T151" id="Seg_4716" s="T150">ɨraːstaː-n</ta>
            <ta e="T152" id="Seg_4717" s="T151">keːs-pit</ta>
            <ta e="T153" id="Seg_4718" s="T152">o-nu</ta>
            <ta e="T154" id="Seg_4719" s="T153">ɨraːktaːgɨ</ta>
            <ta e="T155" id="Seg_4720" s="T154">koru͡oba-ta</ta>
            <ta e="T156" id="Seg_4721" s="T155">hi͡e-n</ta>
            <ta e="T157" id="Seg_4722" s="T156">keːs-pit</ta>
            <ta e="T158" id="Seg_4723" s="T157">ɨraːktaːgɨ</ta>
            <ta e="T159" id="Seg_4724" s="T158">dʼaktar-a</ta>
            <ta e="T160" id="Seg_4725" s="T159">balɨg-ɨ</ta>
            <ta e="T161" id="Seg_4726" s="T160">hi͡e-n</ta>
            <ta e="T162" id="Seg_4727" s="T161">baran</ta>
            <ta e="T163" id="Seg_4728" s="T162">oŋu͡og-u-n</ta>
            <ta e="T164" id="Seg_4729" s="T163">teri͡elke-ge</ta>
            <ta e="T165" id="Seg_4730" s="T164">kaːl-lar-bɨt-ɨ-n</ta>
            <ta e="T166" id="Seg_4731" s="T165">asčɨt</ta>
            <ta e="T167" id="Seg_4732" s="T166">ɨk-ka</ta>
            <ta e="T168" id="Seg_4733" s="T167">bɨrak-pɨt</ta>
            <ta e="T169" id="Seg_4734" s="T168">oloŋko-go</ta>
            <ta e="T170" id="Seg_4735" s="T169">ör</ta>
            <ta e="T171" id="Seg_4736" s="T170">bu͡ol-u͡o</ta>
            <ta e="T172" id="Seg_4737" s="T171">du͡o</ta>
            <ta e="T173" id="Seg_4738" s="T172">üh-ü͡ön-nere</ta>
            <ta e="T174" id="Seg_4739" s="T173">üs</ta>
            <ta e="T175" id="Seg_4740" s="T174">u͡ol-u</ta>
            <ta e="T176" id="Seg_4741" s="T175">töröː-büt-ter</ta>
            <ta e="T177" id="Seg_4742" s="T176">ogo-lor</ta>
            <ta e="T178" id="Seg_4743" s="T177">töhö</ta>
            <ta e="T179" id="Seg_4744" s="T178">da</ta>
            <ta e="T180" id="Seg_4745" s="T179">bu͡ol-bakka</ta>
            <ta e="T181" id="Seg_4746" s="T180">biːrge</ta>
            <ta e="T182" id="Seg_4747" s="T181">oːnnʼuː-r</ta>
            <ta e="T183" id="Seg_4748" s="T182">bu͡ol-but-tar</ta>
            <ta e="T184" id="Seg_4749" s="T183">biːrde</ta>
            <ta e="T185" id="Seg_4750" s="T184">araj</ta>
            <ta e="T186" id="Seg_4751" s="T185">ɨraːktaːgɨ</ta>
            <ta e="T187" id="Seg_4752" s="T186">u͡ol-a</ta>
            <ta e="T188" id="Seg_4753" s="T187">dʼi͡e-bit</ta>
            <ta e="T189" id="Seg_4754" s="T188">dogot-tor</ta>
            <ta e="T190" id="Seg_4755" s="T189">tɨː-lan-a</ta>
            <ta e="T191" id="Seg_4756" s="T190">bar-ɨ͡agɨŋ</ta>
            <ta e="T192" id="Seg_4757" s="T191">u͡olat-tar</ta>
            <ta e="T193" id="Seg_4758" s="T192">töhö</ta>
            <ta e="T194" id="Seg_4759" s="T193">ör</ta>
            <ta e="T195" id="Seg_4760" s="T194">erd-en</ta>
            <ta e="T196" id="Seg_4761" s="T195">is-pit-tere</ta>
            <ta e="T197" id="Seg_4762" s="T196">bu͡olla</ta>
            <ta e="T198" id="Seg_4763" s="T197">kör-büt-tere</ta>
            <ta e="T199" id="Seg_4764" s="T198">ebe</ta>
            <ta e="T200" id="Seg_4765" s="T199">onu͡or</ta>
            <ta e="T201" id="Seg_4766" s="T200">ulagan</ta>
            <ta e="T202" id="Seg_4767" s="T201">bagajɨ</ta>
            <ta e="T203" id="Seg_4768" s="T202">balagan</ta>
            <ta e="T204" id="Seg_4769" s="T203">dʼi͡e</ta>
            <ta e="T205" id="Seg_4770" s="T204">tur-ar</ta>
            <ta e="T206" id="Seg_4771" s="T205">iti</ta>
            <ta e="T207" id="Seg_4772" s="T206">tu͡ok-put=uj</ta>
            <ta e="T208" id="Seg_4773" s="T207">d-e-s-pit-ter</ta>
            <ta e="T209" id="Seg_4774" s="T208">ɨraːktaːgɨ</ta>
            <ta e="T210" id="Seg_4775" s="T209">u͡ol-a</ta>
            <ta e="T211" id="Seg_4776" s="T210">tiks-i͡egiŋ</ta>
            <ta e="T212" id="Seg_4777" s="T211">kaːrtɨ-l-ɨ͡ak-pɨt</ta>
            <ta e="T213" id="Seg_4778" s="T212">dʼi͡e-bit</ta>
            <ta e="T214" id="Seg_4779" s="T213">ɨt</ta>
            <ta e="T215" id="Seg_4780" s="T214">u͡ol-a</ta>
            <ta e="T216" id="Seg_4781" s="T215">e</ta>
            <ta e="T217" id="Seg_4782" s="T216">togo</ta>
            <ta e="T218" id="Seg_4783" s="T217">bagar</ta>
            <ta e="T219" id="Seg_4784" s="T218">abaːhɨ-lar</ta>
            <ta e="T220" id="Seg_4785" s="T219">dʼi͡e-lere</ta>
            <ta e="T221" id="Seg_4786" s="T220">bu͡ol-u͡o</ta>
            <ta e="T222" id="Seg_4787" s="T221">koru͡oba</ta>
            <ta e="T223" id="Seg_4788" s="T222">u͡ol-a</ta>
            <ta e="T224" id="Seg_4789" s="T223">tiks-i͡egiŋ</ta>
            <ta e="T225" id="Seg_4790" s="T224">tiks-i͡egiŋ</ta>
            <ta e="T226" id="Seg_4791" s="T225">kaːm-al-ɨː</ta>
            <ta e="T227" id="Seg_4792" s="T226">tüh-ü͡ögüŋ</ta>
            <ta e="T228" id="Seg_4793" s="T227">kɨtɨl-ga</ta>
            <ta e="T229" id="Seg_4794" s="T228">taks-aːt</ta>
            <ta e="T230" id="Seg_4795" s="T229">bu</ta>
            <ta e="T231" id="Seg_4796" s="T230">ikki</ta>
            <ta e="T232" id="Seg_4797" s="T231">u͡ol</ta>
            <ta e="T233" id="Seg_4798" s="T232">kaːrtɨ-l-ɨː</ta>
            <ta e="T234" id="Seg_4799" s="T233">olor-but-tar</ta>
            <ta e="T235" id="Seg_4800" s="T234">ɨt</ta>
            <ta e="T236" id="Seg_4801" s="T235">u͡ol-a</ta>
            <ta e="T237" id="Seg_4802" s="T236">utuj-an</ta>
            <ta e="T238" id="Seg_4803" s="T237">kaːl-bɨt</ta>
            <ta e="T239" id="Seg_4804" s="T238">töhö</ta>
            <ta e="T240" id="Seg_4805" s="T239">ör</ta>
            <ta e="T241" id="Seg_4806" s="T240">oːnnʼoː-but-tara</ta>
            <ta e="T242" id="Seg_4807" s="T241">bu͡olla</ta>
            <ta e="T243" id="Seg_4808" s="T242">bu</ta>
            <ta e="T244" id="Seg_4809" s="T243">ogo-lor</ta>
            <ta e="T245" id="Seg_4810" s="T244">emi͡e</ta>
            <ta e="T246" id="Seg_4811" s="T245">utuj-an</ta>
            <ta e="T247" id="Seg_4812" s="T246">kaːl-bɨt-tar</ta>
            <ta e="T248" id="Seg_4813" s="T247">ɨt</ta>
            <ta e="T249" id="Seg_4814" s="T248">u͡ol-a</ta>
            <ta e="T250" id="Seg_4815" s="T249">tüːn</ta>
            <ta e="T251" id="Seg_4816" s="T250">orto</ta>
            <ta e="T252" id="Seg_4817" s="T251">uhukt-u-but-a</ta>
            <ta e="T253" id="Seg_4818" s="T252">kihi</ta>
            <ta e="T254" id="Seg_4819" s="T253">kaːm-ar</ta>
            <ta e="T255" id="Seg_4820" s="T254">tɨ͡ah-a</ta>
            <ta e="T256" id="Seg_4821" s="T255">ihill-i-bit</ta>
            <ta e="T257" id="Seg_4822" s="T256">taks-ɨ-bɨt-a</ta>
            <ta e="T258" id="Seg_4823" s="T257">abaːhɨ</ta>
            <ta e="T259" id="Seg_4824" s="T258">balagan</ta>
            <ta e="T260" id="Seg_4825" s="T259">dʼi͡ek</ta>
            <ta e="T261" id="Seg_4826" s="T260">kaːm-an</ta>
            <ta e="T262" id="Seg_4827" s="T261">ih-eːktiː-r</ta>
            <ta e="T263" id="Seg_4828" s="T262">dʼe</ta>
            <ta e="T264" id="Seg_4829" s="T263">o</ta>
            <ta e="T265" id="Seg_4830" s="T264">dʼe</ta>
            <ta e="T266" id="Seg_4831" s="T265">öl-büp-püt</ta>
            <ta e="T267" id="Seg_4832" s="T266">e-bit</ta>
            <ta e="T268" id="Seg_4833" s="T267">dʼi͡e-n</ta>
            <ta e="T269" id="Seg_4834" s="T268">baran</ta>
            <ta e="T270" id="Seg_4835" s="T269">dogot-tor-u-n</ta>
            <ta e="T271" id="Seg_4836" s="T270">uhugun-nar-a</ta>
            <ta e="T272" id="Seg_4837" s="T271">hataː-bɨt</ta>
            <ta e="T273" id="Seg_4838" s="T272">bu</ta>
            <ta e="T274" id="Seg_4839" s="T273">u͡olat-tar</ta>
            <ta e="T275" id="Seg_4840" s="T274">togo</ta>
            <ta e="T276" id="Seg_4841" s="T275">uhukt-u͡ok-tara=j</ta>
            <ta e="T277" id="Seg_4842" s="T276">kel-kel</ta>
            <ta e="T278" id="Seg_4843" s="T277">min</ta>
            <ta e="T279" id="Seg_4844" s="T278">enʼigi-n</ta>
            <ta e="T280" id="Seg_4845" s="T279">h-i͡e-m</ta>
            <ta e="T281" id="Seg_4846" s="T280">abaːhɨ</ta>
            <ta e="T282" id="Seg_4847" s="T281">haŋa-ta</ta>
            <ta e="T283" id="Seg_4848" s="T282">ihill-i-bit</ta>
            <ta e="T284" id="Seg_4849" s="T283">bu</ta>
            <ta e="T285" id="Seg_4850" s="T284">u͡ol</ta>
            <ta e="T286" id="Seg_4851" s="T285">taks-a</ta>
            <ta e="T287" id="Seg_4852" s="T286">köp-püt</ta>
            <ta e="T288" id="Seg_4853" s="T287">da</ta>
            <ta e="T289" id="Seg_4854" s="T288">abaːhɨ-nɨ</ta>
            <ta e="T290" id="Seg_4855" s="T289">gɨtta</ta>
            <ta e="T291" id="Seg_4856" s="T290">ölörs-ü-büt</ta>
            <ta e="T292" id="Seg_4857" s="T291">töhö</ta>
            <ta e="T293" id="Seg_4858" s="T292">da</ta>
            <ta e="T294" id="Seg_4859" s="T293">bu͡ol-bakka</ta>
            <ta e="T295" id="Seg_4860" s="T294">abaːhɨ</ta>
            <ta e="T296" id="Seg_4861" s="T295">menʼiː-ti-n</ta>
            <ta e="T297" id="Seg_4862" s="T296">boldok</ta>
            <ta e="T298" id="Seg_4863" s="T297">taːs-ka</ta>
            <ta e="T299" id="Seg_4864" s="T298">kaj-a</ta>
            <ta e="T300" id="Seg_4865" s="T299">oks-u-but</ta>
            <ta e="T301" id="Seg_4866" s="T300">ol</ta>
            <ta e="T302" id="Seg_4867" s="T301">gɨn-an</ta>
            <ta e="T303" id="Seg_4868" s="T302">baran</ta>
            <ta e="T304" id="Seg_4869" s="T303">irge-ti-n</ta>
            <ta e="T305" id="Seg_4870" s="T304">ih-i-tten</ta>
            <ta e="T306" id="Seg_4871" s="T305">huruk-taːk</ta>
            <ta e="T307" id="Seg_4872" s="T306">taːh-ɨ</ta>
            <ta e="T308" id="Seg_4873" s="T307">oroː-but</ta>
            <ta e="T309" id="Seg_4874" s="T308">taːh-ɨ</ta>
            <ta e="T310" id="Seg_4875" s="T309">karmaːn-ɨ-n</ta>
            <ta e="T311" id="Seg_4876" s="T310">ih-i-nen</ta>
            <ta e="T312" id="Seg_4877" s="T311">ukt-an</ta>
            <ta e="T313" id="Seg_4878" s="T312">keːs-pit</ta>
            <ta e="T314" id="Seg_4879" s="T313">harsi͡erda</ta>
            <ta e="T315" id="Seg_4880" s="T314">tu͡ok</ta>
            <ta e="T316" id="Seg_4881" s="T315">da</ta>
            <ta e="T317" id="Seg_4882" s="T316">bu͡ol-batak</ta>
            <ta e="T318" id="Seg_4883" s="T317">kördük</ta>
            <ta e="T319" id="Seg_4884" s="T318">ɨt</ta>
            <ta e="T320" id="Seg_4885" s="T319">u͡ol-a</ta>
            <ta e="T321" id="Seg_4886" s="T320">dogot-tor-u-n</ta>
            <ta e="T322" id="Seg_4887" s="T321">uhugun-nar-taː-bɨt</ta>
            <ta e="T323" id="Seg_4888" s="T322">čej</ta>
            <ta e="T324" id="Seg_4889" s="T323">kaːs-tan-a</ta>
            <ta e="T325" id="Seg_4890" s="T324">bar-ɨ͡agɨŋ</ta>
            <ta e="T326" id="Seg_4891" s="T325">u͡olat-tar</ta>
            <ta e="T327" id="Seg_4892" s="T326">tɨːllaŋnaː-n</ta>
            <ta e="T328" id="Seg_4893" s="T327">arɨːččɨ</ta>
            <ta e="T329" id="Seg_4894" s="T328">tur-an</ta>
            <ta e="T330" id="Seg_4895" s="T329">čaːj</ta>
            <ta e="T331" id="Seg_4896" s="T330">is-pit-ter</ta>
            <ta e="T332" id="Seg_4897" s="T331">oloŋko-go</ta>
            <ta e="T333" id="Seg_4898" s="T332">kün</ta>
            <ta e="T334" id="Seg_4899" s="T333">kɨlgas</ta>
            <ta e="T335" id="Seg_4900" s="T334">töhö</ta>
            <ta e="T336" id="Seg_4901" s="T335">da</ta>
            <ta e="T337" id="Seg_4902" s="T336">bu͡ol-bakka</ta>
            <ta e="T338" id="Seg_4903" s="T337">kallaːn</ta>
            <ta e="T339" id="Seg_4904" s="T338">karaŋar-bɨt</ta>
            <ta e="T340" id="Seg_4905" s="T339">ɨt</ta>
            <ta e="T341" id="Seg_4906" s="T340">u͡ol-a</ta>
            <ta e="T342" id="Seg_4907" s="T341">ahaː-n-hi͡e-n</ta>
            <ta e="T343" id="Seg_4908" s="T342">baran</ta>
            <ta e="T344" id="Seg_4909" s="T343">utuj-an</ta>
            <ta e="T345" id="Seg_4910" s="T344">kaːl-bɨt</ta>
            <ta e="T346" id="Seg_4911" s="T345">dogot-tor-o</ta>
            <ta e="T347" id="Seg_4912" s="T346">emi͡e</ta>
            <ta e="T348" id="Seg_4913" s="T347">kaːrtɨ-l-ɨː</ta>
            <ta e="T349" id="Seg_4914" s="T348">olor-but-tar</ta>
            <ta e="T350" id="Seg_4915" s="T349">tüːn</ta>
            <ta e="T351" id="Seg_4916" s="T350">orto</ta>
            <ta e="T352" id="Seg_4917" s="T351">uhukt-u-but-a</ta>
            <ta e="T353" id="Seg_4918" s="T352">dogot-tor-o</ta>
            <ta e="T354" id="Seg_4919" s="T353">olord-o</ta>
            <ta e="T355" id="Seg_4920" s="T354">olor-but-u-nan</ta>
            <ta e="T356" id="Seg_4921" s="T355">utuj-an</ta>
            <ta e="T357" id="Seg_4922" s="T356">kaːl-bɨt-tar</ta>
            <ta e="T358" id="Seg_4923" s="T357">bu</ta>
            <ta e="T359" id="Seg_4924" s="T358">ogo</ta>
            <ta e="T360" id="Seg_4925" s="T359">uhukt-an</ta>
            <ta e="T361" id="Seg_4926" s="T360">baran</ta>
            <ta e="T362" id="Seg_4927" s="T361">ihill-iː</ta>
            <ta e="T363" id="Seg_4928" s="T362">hɨp-pɨt</ta>
            <ta e="T364" id="Seg_4929" s="T363">ɨraːk</ta>
            <ta e="T365" id="Seg_4930" s="T364">kanna</ta>
            <ta e="T366" id="Seg_4931" s="T365">ire</ta>
            <ta e="T367" id="Seg_4932" s="T366">emi͡e</ta>
            <ta e="T368" id="Seg_4933" s="T367">kaːm-ar</ta>
            <ta e="T369" id="Seg_4934" s="T368">tɨ͡as</ta>
            <ta e="T370" id="Seg_4935" s="T369">ihill-i-bit</ta>
            <ta e="T371" id="Seg_4936" s="T370">dogot-tor-u-n</ta>
            <ta e="T372" id="Seg_4937" s="T371">uhugun-nar-bɨt-a</ta>
            <ta e="T373" id="Seg_4938" s="T372">togo</ta>
            <ta e="T374" id="Seg_4939" s="T373">uhukt-u͡ok-tara=j</ta>
            <ta e="T375" id="Seg_4940" s="T374">čubu</ta>
            <ta e="T376" id="Seg_4941" s="T375">utuj-but</ta>
            <ta e="T377" id="Seg_4942" s="T376">kihi-ler</ta>
            <ta e="T378" id="Seg_4943" s="T377">ɨt</ta>
            <ta e="T379" id="Seg_4944" s="T378">u͡ol-a</ta>
            <ta e="T380" id="Seg_4945" s="T379">taks-ɨ-bɨt</ta>
            <ta e="T381" id="Seg_4946" s="T380">da</ta>
            <ta e="T382" id="Seg_4947" s="T381">ikki-s</ta>
            <ta e="T383" id="Seg_4948" s="T382">abaːhɨ</ta>
            <ta e="T384" id="Seg_4949" s="T383">menʼiː-ti-n</ta>
            <ta e="T385" id="Seg_4950" s="T384">emi͡e</ta>
            <ta e="T386" id="Seg_4951" s="T385">boldok</ta>
            <ta e="T387" id="Seg_4952" s="T386">taːs-ka</ta>
            <ta e="T388" id="Seg_4953" s="T387">kampɨ</ta>
            <ta e="T389" id="Seg_4954" s="T388">oks-u-but</ta>
            <ta e="T390" id="Seg_4955" s="T389">abaːhɨ</ta>
            <ta e="T391" id="Seg_4956" s="T390">tebi͡elen-e</ta>
            <ta e="T392" id="Seg_4957" s="T391">tüh-en</ta>
            <ta e="T393" id="Seg_4958" s="T392">baran</ta>
            <ta e="T394" id="Seg_4959" s="T393">čirke-s</ta>
            <ta e="T395" id="Seg_4960" s="T394">gɨm-mɨt</ta>
            <ta e="T396" id="Seg_4961" s="T395">ɨt</ta>
            <ta e="T397" id="Seg_4962" s="T396">u͡ol-a</ta>
            <ta e="T398" id="Seg_4963" s="T397">irge-ti-tten</ta>
            <ta e="T399" id="Seg_4964" s="T398">huruk-taːk</ta>
            <ta e="T400" id="Seg_4965" s="T399">taːh-ɨ</ta>
            <ta e="T401" id="Seg_4966" s="T400">oroː-t-o</ta>
            <ta e="T402" id="Seg_4967" s="T401">dagɨnɨ</ta>
            <ta e="T403" id="Seg_4968" s="T402">karmaːn-ɨ-gar</ta>
            <ta e="T404" id="Seg_4969" s="T403">emi͡e</ta>
            <ta e="T405" id="Seg_4970" s="T404">ukt-an</ta>
            <ta e="T406" id="Seg_4971" s="T405">keːs-pit</ta>
            <ta e="T407" id="Seg_4972" s="T406">üh-üs</ta>
            <ta e="T408" id="Seg_4973" s="T407">tüːn-ü-ger</ta>
            <ta e="T409" id="Seg_4974" s="T408">üh-üs</ta>
            <ta e="T410" id="Seg_4975" s="T409">abaːhɨ</ta>
            <ta e="T411" id="Seg_4976" s="T410">menʼiː-ti-n</ta>
            <ta e="T412" id="Seg_4977" s="T411">kaj-a</ta>
            <ta e="T413" id="Seg_4978" s="T412">oks-on</ta>
            <ta e="T414" id="Seg_4979" s="T413">huruk-taːk</ta>
            <ta e="T415" id="Seg_4980" s="T414">taːh-ɨ</ta>
            <ta e="T416" id="Seg_4981" s="T415">or-uː</ta>
            <ta e="T417" id="Seg_4982" s="T416">oks-u-but</ta>
            <ta e="T418" id="Seg_4983" s="T417">dogot-tor-u-n</ta>
            <ta e="T419" id="Seg_4984" s="T418">im</ta>
            <ta e="T420" id="Seg_4985" s="T419">tur-ar-ɨ-n</ta>
            <ta e="T421" id="Seg_4986" s="T420">kɨtta</ta>
            <ta e="T422" id="Seg_4987" s="T421">uhugun-nar-bɨt</ta>
            <ta e="T423" id="Seg_4988" s="T422">če</ta>
            <ta e="T424" id="Seg_4989" s="T423">dʼi͡e-biti-ger</ta>
            <ta e="T425" id="Seg_4990" s="T424">bar-ɨ͡agɨŋ</ta>
            <ta e="T426" id="Seg_4991" s="T425">dʼi͡e-bit</ta>
            <ta e="T427" id="Seg_4992" s="T426">bu</ta>
            <ta e="T428" id="Seg_4993" s="T427">u͡ol</ta>
            <ta e="T429" id="Seg_4994" s="T428">ɨraːktaːgɨ</ta>
            <ta e="T430" id="Seg_4995" s="T429">u͡ol-a</ta>
            <ta e="T431" id="Seg_4996" s="T430">öčös-püt</ta>
            <ta e="T432" id="Seg_4997" s="T431">togo</ta>
            <ta e="T433" id="Seg_4998" s="T432">doː</ta>
            <ta e="T434" id="Seg_4999" s="T433">olor-o</ta>
            <ta e="T435" id="Seg_5000" s="T434">tüh-ü͡ögüŋ</ta>
            <ta e="T436" id="Seg_5001" s="T435">beseleː</ta>
            <ta e="T437" id="Seg_5002" s="T436">koru͡oba</ta>
            <ta e="T438" id="Seg_5003" s="T437">u͡ol-a</ta>
            <ta e="T439" id="Seg_5004" s="T438">olor-o</ta>
            <ta e="T440" id="Seg_5005" s="T439">tüh-en</ta>
            <ta e="T441" id="Seg_5006" s="T440">baran</ta>
            <ta e="T442" id="Seg_5007" s="T441">dʼi͡e-bit</ta>
            <ta e="T443" id="Seg_5008" s="T442">kerget-ter-bit</ta>
            <ta e="T444" id="Seg_5009" s="T443">küːt-ü͡ök-tere</ta>
            <ta e="T445" id="Seg_5010" s="T444">bar-ɨ͡agɨŋ</ta>
            <ta e="T446" id="Seg_5011" s="T445">eːt</ta>
            <ta e="T447" id="Seg_5012" s="T446">u͡olat-tar</ta>
            <ta e="T448" id="Seg_5013" s="T447">kamnaː-bɨt-tar</ta>
            <ta e="T449" id="Seg_5014" s="T448">ɨt</ta>
            <ta e="T450" id="Seg_5015" s="T449">u͡ol-a</ta>
            <ta e="T451" id="Seg_5016" s="T450">erd-en</ta>
            <ta e="T452" id="Seg_5017" s="T451">is-pit</ta>
            <ta e="T453" id="Seg_5018" s="T452">töhö</ta>
            <ta e="T454" id="Seg_5019" s="T453">da</ta>
            <ta e="T455" id="Seg_5020" s="T454">bu͡ol-bakka</ta>
            <ta e="T456" id="Seg_5021" s="T455">kɨːn-ɨ-n</ta>
            <ta e="T457" id="Seg_5022" s="T456">tutt-an</ta>
            <ta e="T458" id="Seg_5023" s="T457">bar-an</ta>
            <ta e="T459" id="Seg_5024" s="T458">dogot-toːr</ta>
            <ta e="T460" id="Seg_5025" s="T459">bahak-pɨ-n</ta>
            <ta e="T461" id="Seg_5026" s="T460">kaːl-lar-bɨp-pɨn</ta>
            <ta e="T462" id="Seg_5027" s="T461">körd-üː</ta>
            <ta e="T463" id="Seg_5028" s="T462">bar-ɨ͡a-m</ta>
            <ta e="T464" id="Seg_5029" s="T463">dʼi͡e-bit</ta>
            <ta e="T465" id="Seg_5030" s="T464">ɨt</ta>
            <ta e="T466" id="Seg_5031" s="T465">u͡ol-a</ta>
            <ta e="T467" id="Seg_5032" s="T466">balagaŋ-ŋa</ta>
            <ta e="T468" id="Seg_5033" s="T467">kel-bit-e</ta>
            <ta e="T469" id="Seg_5034" s="T468">dʼaktat-tar</ta>
            <ta e="T470" id="Seg_5035" s="T469">ɨtɨː-r</ta>
            <ta e="T471" id="Seg_5036" s="T470">haŋa-lara</ta>
            <ta e="T472" id="Seg_5037" s="T471">ihill-i-bit</ta>
            <ta e="T473" id="Seg_5038" s="T472">min</ta>
            <ta e="T474" id="Seg_5039" s="T473">minnʼiges</ta>
            <ta e="T475" id="Seg_5040" s="T474">as</ta>
            <ta e="T476" id="Seg_5041" s="T475">bu͡ol-an</ta>
            <ta e="T477" id="Seg_5042" s="T476">baran</ta>
            <ta e="T478" id="Seg_5043" s="T477">ulakan</ta>
            <ta e="T479" id="Seg_5044" s="T478">teri͡elke-ge</ta>
            <ta e="T480" id="Seg_5045" s="T479">tɨː-larɨ-n</ta>
            <ta e="T481" id="Seg_5046" s="T480">ih-i-ger</ta>
            <ta e="T482" id="Seg_5047" s="T481">tüh-ü͡ö-m</ta>
            <ta e="T483" id="Seg_5048" s="T482">minʼigi-n</ta>
            <ta e="T484" id="Seg_5049" s="T483">hi͡e-tek-terine</ta>
            <ta e="T485" id="Seg_5050" s="T484">öl-ön</ta>
            <ta e="T486" id="Seg_5051" s="T485">kaːl-ɨ͡ak-tara</ta>
            <ta e="T487" id="Seg_5052" s="T486">dʼi͡e-bit</ta>
            <ta e="T488" id="Seg_5053" s="T487">kardʼagas</ta>
            <ta e="T489" id="Seg_5054" s="T488">Dʼige baːba</ta>
            <ta e="T490" id="Seg_5055" s="T489">ku͡olah-a</ta>
            <ta e="T491" id="Seg_5056" s="T490">min</ta>
            <ta e="T492" id="Seg_5057" s="T491">bu͡ollagɨna</ta>
            <ta e="T493" id="Seg_5058" s="T492">kahan</ta>
            <ta e="T494" id="Seg_5059" s="T493">da</ta>
            <ta e="T495" id="Seg_5060" s="T494">kör-bötök</ta>
            <ta e="T496" id="Seg_5061" s="T495">bosku͡oj</ta>
            <ta e="T497" id="Seg_5062" s="T496">holko</ta>
            <ta e="T498" id="Seg_5063" s="T497">pɨlaːt</ta>
            <ta e="T499" id="Seg_5064" s="T498">bu͡ol-an</ta>
            <ta e="T500" id="Seg_5065" s="T499">ɨraːktaːgɨ</ta>
            <ta e="T501" id="Seg_5066" s="T500">u͡ol-u-n</ta>
            <ta e="T502" id="Seg_5067" s="T501">töhög-ü-ger</ta>
            <ta e="T503" id="Seg_5068" s="T502">tüh-ü͡ö-m</ta>
            <ta e="T504" id="Seg_5069" s="T503">barɨ-kaːt-tarɨ-n</ta>
            <ta e="T505" id="Seg_5070" s="T504">moŋun-nar-an</ta>
            <ta e="T506" id="Seg_5071" s="T505">ölör-ü͡ö-m</ta>
            <ta e="T507" id="Seg_5072" s="T506">min</ta>
            <ta e="T508" id="Seg_5073" s="T507">bu͡ollagɨna</ta>
            <ta e="T509" id="Seg_5074" s="T508">tɨː-larɨ-n</ta>
            <ta e="T510" id="Seg_5075" s="T509">ipseri</ta>
            <ta e="T511" id="Seg_5076" s="T510">hilimniː-r</ta>
            <ta e="T512" id="Seg_5077" s="T511">gɨn-a</ta>
            <ta e="T513" id="Seg_5078" s="T512">uː</ta>
            <ta e="T514" id="Seg_5079" s="T513">hɨmala</ta>
            <ta e="T515" id="Seg_5080" s="T514">bu͡ol-u͡o-m</ta>
            <ta e="T516" id="Seg_5081" s="T515">barɨ-larɨ-n</ta>
            <ta e="T517" id="Seg_5082" s="T516">obor-on</ta>
            <ta e="T518" id="Seg_5083" s="T517">timir-d-i͡e-m</ta>
            <ta e="T519" id="Seg_5084" s="T518">dʼe</ta>
            <ta e="T520" id="Seg_5085" s="T519">üčügej</ta>
            <ta e="T521" id="Seg_5086" s="T520">bertik</ta>
            <ta e="T522" id="Seg_5087" s="T521">hanaː-bɨk-kɨt</ta>
            <ta e="T523" id="Seg_5088" s="T522">e-bit</ta>
            <ta e="T524" id="Seg_5089" s="T523">dʼi͡e-bit</ta>
            <ta e="T525" id="Seg_5090" s="T524">mannajgɨ</ta>
            <ta e="T526" id="Seg_5091" s="T525">dʼaktar</ta>
            <ta e="T527" id="Seg_5092" s="T526">ɨt</ta>
            <ta e="T528" id="Seg_5093" s="T527">u͡ol-a</ta>
            <ta e="T529" id="Seg_5094" s="T528">iti-ni</ta>
            <ta e="T530" id="Seg_5095" s="T529">ist-i-bit</ta>
            <ta e="T531" id="Seg_5096" s="T530">daːganɨ</ta>
            <ta e="T532" id="Seg_5097" s="T531">hɨːr</ta>
            <ta e="T533" id="Seg_5098" s="T532">ann-ɨ-gar</ta>
            <ta e="T534" id="Seg_5099" s="T533">hüːr-en</ta>
            <ta e="T535" id="Seg_5100" s="T534">kiːr-bit</ta>
            <ta e="T536" id="Seg_5101" s="T535">tɨː-tɨ-n</ta>
            <ta e="T537" id="Seg_5102" s="T536">ɨl-baktɨː-r</ta>
            <ta e="T538" id="Seg_5103" s="T537">da</ta>
            <ta e="T539" id="Seg_5104" s="T538">erd-en</ta>
            <ta e="T540" id="Seg_5105" s="T539">is-pit</ta>
            <ta e="T541" id="Seg_5106" s="T540">dogot-tor-u-n</ta>
            <ta e="T542" id="Seg_5107" s="T541">ebe</ta>
            <ta e="T543" id="Seg_5108" s="T542">orto-tu-gar</ta>
            <ta e="T544" id="Seg_5109" s="T543">hit-e</ta>
            <ta e="T545" id="Seg_5110" s="T544">oks-u-but</ta>
            <ta e="T546" id="Seg_5111" s="T545">u͡olat-tar</ta>
            <ta e="T547" id="Seg_5112" s="T546">orgujakaːn</ta>
            <ta e="T548" id="Seg_5113" s="T547">erd-en</ta>
            <ta e="T549" id="Seg_5114" s="T548">is-pit-ter</ta>
            <ta e="T550" id="Seg_5115" s="T549">töhö</ta>
            <ta e="T551" id="Seg_5116" s="T550">ör</ta>
            <ta e="T552" id="Seg_5117" s="T551">bu͡ol-u͡o=j</ta>
            <ta e="T553" id="Seg_5118" s="T552">tɨː-larɨ-n</ta>
            <ta e="T554" id="Seg_5119" s="T553">ih-i-ger</ta>
            <ta e="T555" id="Seg_5120" s="T554">as-taːk</ta>
            <ta e="T556" id="Seg_5121" s="T555">teri͡elke</ta>
            <ta e="T557" id="Seg_5122" s="T556">tüs-püt</ta>
            <ta e="T558" id="Seg_5123" s="T557">ɨraːktaːgɨ</ta>
            <ta e="T559" id="Seg_5124" s="T558">u͡ol-a</ta>
            <ta e="T560" id="Seg_5125" s="T559">ü͡ör-büt</ta>
            <ta e="T561" id="Seg_5126" s="T560">iliː-ti-n</ta>
            <ta e="T562" id="Seg_5127" s="T561">teri͡elke</ta>
            <ta e="T563" id="Seg_5128" s="T562">dʼi͡ek</ta>
            <ta e="T564" id="Seg_5129" s="T563">harba-s</ta>
            <ta e="T565" id="Seg_5130" s="T564">gɨn-ar-ɨ-n</ta>
            <ta e="T566" id="Seg_5131" s="T565">gɨtta</ta>
            <ta e="T567" id="Seg_5132" s="T566">ɨt</ta>
            <ta e="T568" id="Seg_5133" s="T567">u͡ol-a</ta>
            <ta e="T569" id="Seg_5134" s="T568">as-taːk</ta>
            <ta e="T570" id="Seg_5135" s="T569">teri͡elke-ni</ta>
            <ta e="T571" id="Seg_5136" s="T570">uː-ga</ta>
            <ta e="T572" id="Seg_5137" s="T571">bɨrag-an</ta>
            <ta e="T573" id="Seg_5138" s="T572">keːs-pit</ta>
            <ta e="T574" id="Seg_5139" s="T573">ɨraːktaːgɨ</ta>
            <ta e="T575" id="Seg_5140" s="T574">u͡ol-a</ta>
            <ta e="T576" id="Seg_5141" s="T575">kɨːhɨr-an</ta>
            <ta e="T577" id="Seg_5142" s="T576">huntuj-an</ta>
            <ta e="T578" id="Seg_5143" s="T577">olor-dog-una</ta>
            <ta e="T579" id="Seg_5144" s="T578">bosku͡oj</ta>
            <ta e="T580" id="Seg_5145" s="T579">bagajɨ</ta>
            <ta e="T581" id="Seg_5146" s="T580">holko</ta>
            <ta e="T582" id="Seg_5147" s="T581">pulaːt</ta>
            <ta e="T583" id="Seg_5148" s="T582">ɨraːktaːgɨ</ta>
            <ta e="T584" id="Seg_5149" s="T583">u͡ol-u-n</ta>
            <ta e="T585" id="Seg_5150" s="T584">töhög-ü-ger</ta>
            <ta e="T586" id="Seg_5151" s="T585">tüs-püt</ta>
            <ta e="T587" id="Seg_5152" s="T586">o</ta>
            <ta e="T588" id="Seg_5153" s="T587">taŋara</ta>
            <ta e="T589" id="Seg_5154" s="T588">maːma-ba-r</ta>
            <ta e="T590" id="Seg_5155" s="T589">kehiː</ta>
            <ta e="T591" id="Seg_5156" s="T590">gɨn-ɨ͡a-m</ta>
            <ta e="T592" id="Seg_5157" s="T591">dʼi͡e-bit</ta>
            <ta e="T593" id="Seg_5158" s="T592">da</ta>
            <ta e="T594" id="Seg_5159" s="T593">karmaːn-ɨ-gar</ta>
            <ta e="T595" id="Seg_5160" s="T594">ukt-aːrɨ</ta>
            <ta e="T596" id="Seg_5161" s="T595">gɨn-ar-ɨ-n</ta>
            <ta e="T597" id="Seg_5162" s="T596">gɨtta</ta>
            <ta e="T598" id="Seg_5163" s="T597">u͡ol-a</ta>
            <ta e="T599" id="Seg_5164" s="T598">tal-aːn</ta>
            <ta e="T600" id="Seg_5165" s="T599">ɨl-bɨt</ta>
            <ta e="T601" id="Seg_5166" s="T600">bahag-ɨ-nan</ta>
            <ta e="T602" id="Seg_5167" s="T601">ilči</ta>
            <ta e="T603" id="Seg_5168" s="T602">kerd-en</ta>
            <ta e="T604" id="Seg_5169" s="T603">baran</ta>
            <ta e="T605" id="Seg_5170" s="T604">uː-ga</ta>
            <ta e="T606" id="Seg_5171" s="T605">bɨrag-an</ta>
            <ta e="T607" id="Seg_5172" s="T606">keːs-pit</ta>
            <ta e="T608" id="Seg_5173" s="T607">ɨraːktaːgɨ</ta>
            <ta e="T609" id="Seg_5174" s="T608">u͡ol-a</ta>
            <ta e="T610" id="Seg_5175" s="T609">kɨːhɨr-an</ta>
            <ta e="T611" id="Seg_5176" s="T610">haŋa-ta</ta>
            <ta e="T612" id="Seg_5177" s="T611">da</ta>
            <ta e="T613" id="Seg_5178" s="T612">taks-ɨ-batak</ta>
            <ta e="T614" id="Seg_5179" s="T613">töhö</ta>
            <ta e="T615" id="Seg_5180" s="T614">ör</ta>
            <ta e="T616" id="Seg_5181" s="T615">bu͡ol-u͡o=j</ta>
            <ta e="T617" id="Seg_5182" s="T616">tɨː-lara</ta>
            <ta e="T618" id="Seg_5183" s="T617">uː-ga</ta>
            <ta e="T619" id="Seg_5184" s="T618">hɨst-an</ta>
            <ta e="T620" id="Seg_5185" s="T619">kaːl-bɨt</ta>
            <ta e="T621" id="Seg_5186" s="T620">ebe</ta>
            <ta e="T622" id="Seg_5187" s="T621">uː-ta</ta>
            <ta e="T623" id="Seg_5188" s="T622">karaːrt</ta>
            <ta e="T624" id="Seg_5189" s="T623">gɨm-mɨt</ta>
            <ta e="T625" id="Seg_5190" s="T624">bu</ta>
            <ta e="T626" id="Seg_5191" s="T625">u͡olat-tar</ta>
            <ta e="T627" id="Seg_5192" s="T626">hataː-n</ta>
            <ta e="T628" id="Seg_5193" s="T627">erd-i-m-met</ta>
            <ta e="T629" id="Seg_5194" s="T628">bu͡ol-an</ta>
            <ta e="T630" id="Seg_5195" s="T629">kaːl-bɨt-tar</ta>
            <ta e="T631" id="Seg_5196" s="T630">kuttam-mɨt-tar</ta>
            <ta e="T632" id="Seg_5197" s="T631">ɨt</ta>
            <ta e="T633" id="Seg_5198" s="T632">u͡ol-a</ta>
            <ta e="T634" id="Seg_5199" s="T633">uː-nu</ta>
            <ta e="T635" id="Seg_5200" s="T634">hüge-nnen</ta>
            <ta e="T636" id="Seg_5201" s="T635">bɨhɨt-a</ta>
            <ta e="T637" id="Seg_5202" s="T636">kerd-i-ŋ</ta>
            <ta e="T638" id="Seg_5203" s="T637">bahag-ɨ-nan</ta>
            <ta e="T639" id="Seg_5204" s="T638">teh-i-t-e</ta>
            <ta e="T640" id="Seg_5205" s="T639">annʼ-ɨ-ŋ</ta>
            <ta e="T641" id="Seg_5206" s="T640">min</ta>
            <ta e="T642" id="Seg_5207" s="T641">erd-en</ta>
            <ta e="T643" id="Seg_5208" s="T642">ih-i͡e-m</ta>
            <ta e="T644" id="Seg_5209" s="T643">dʼi͡e-bit</ta>
            <ta e="T645" id="Seg_5210" s="T644">dogot-tor-u-gar</ta>
            <ta e="T646" id="Seg_5211" s="T645">kör-büt-tere</ta>
            <ta e="T647" id="Seg_5212" s="T646">uː-lara</ta>
            <ta e="T648" id="Seg_5213" s="T647">kirdik</ta>
            <ta e="T649" id="Seg_5214" s="T648">hɨrdaː-n</ta>
            <ta e="T650" id="Seg_5215" s="T649">kɨtar-an</ta>
            <ta e="T651" id="Seg_5216" s="T650">is-pit</ta>
            <ta e="T652" id="Seg_5217" s="T651">hotoru</ta>
            <ta e="T653" id="Seg_5218" s="T652">tɨː-lara</ta>
            <ta e="T654" id="Seg_5219" s="T653">boskol-o-m-mut</ta>
            <ta e="T655" id="Seg_5220" s="T654">u͡olat-tar</ta>
            <ta e="T656" id="Seg_5221" s="T655">erd-i-n-en</ta>
            <ta e="T657" id="Seg_5222" s="T656">is-pit-ter</ta>
            <ta e="T658" id="Seg_5223" s="T657">orguːjakaːn</ta>
            <ta e="T659" id="Seg_5224" s="T658">töhö</ta>
            <ta e="T660" id="Seg_5225" s="T659">da</ta>
            <ta e="T661" id="Seg_5226" s="T660">ör</ta>
            <ta e="T662" id="Seg_5227" s="T661">bu͡ol-bakka</ta>
            <ta e="T663" id="Seg_5228" s="T662">ebe</ta>
            <ta e="T664" id="Seg_5229" s="T663">ürdük</ta>
            <ta e="T665" id="Seg_5230" s="T664">kɨtɨl-ɨ-gar</ta>
            <ta e="T666" id="Seg_5231" s="T665">gu͡orat-tara</ta>
            <ta e="T667" id="Seg_5232" s="T666">köst-ü-büt</ta>
            <ta e="T668" id="Seg_5233" s="T667">tiks-en</ta>
            <ta e="T669" id="Seg_5234" s="T668">baran</ta>
            <ta e="T670" id="Seg_5235" s="T669">u͡olat-tar</ta>
            <ta e="T671" id="Seg_5236" s="T670">üs</ta>
            <ta e="T672" id="Seg_5237" s="T671">aŋɨ</ta>
            <ta e="T673" id="Seg_5238" s="T672">araks-an</ta>
            <ta e="T674" id="Seg_5239" s="T673">kaːl-bɨt-tar</ta>
            <ta e="T675" id="Seg_5240" s="T674">ɨraːktaːgɨ</ta>
            <ta e="T676" id="Seg_5241" s="T675">u͡ol-a</ta>
            <ta e="T677" id="Seg_5242" s="T676">hɨːr</ta>
            <ta e="T678" id="Seg_5243" s="T677">ürd-ü-ger</ta>
            <ta e="T679" id="Seg_5244" s="T678">čeːlkeː</ta>
            <ta e="T680" id="Seg_5245" s="T679">dʼi͡e-ge</ta>
            <ta e="T681" id="Seg_5246" s="T680">bar-bɨt</ta>
            <ta e="T682" id="Seg_5247" s="T681">ɨt</ta>
            <ta e="T683" id="Seg_5248" s="T682">u͡ol-a</ta>
            <ta e="T684" id="Seg_5249" s="T683">maːma-tɨ-n</ta>
            <ta e="T685" id="Seg_5250" s="T684">nʼamčɨgas</ta>
            <ta e="T686" id="Seg_5251" s="T685">koroːn</ta>
            <ta e="T687" id="Seg_5252" s="T686">dʼi͡e-ti-ger</ta>
            <ta e="T688" id="Seg_5253" s="T687">kiːr-bit</ta>
            <ta e="T689" id="Seg_5254" s="T688">koru͡oba</ta>
            <ta e="T690" id="Seg_5255" s="T689">u͡ol-a</ta>
            <ta e="T691" id="Seg_5256" s="T690">tahaːraː</ta>
            <ta e="T692" id="Seg_5257" s="T691">inʼe-ti-n</ta>
            <ta e="T693" id="Seg_5258" s="T692">körs-ü-büt</ta>
            <ta e="T694" id="Seg_5259" s="T693">harsi͡erda</ta>
            <ta e="T695" id="Seg_5260" s="T694">ɨraːktaːgɨ</ta>
            <ta e="T696" id="Seg_5261" s="T695">u͡olat-tar-ɨ</ta>
            <ta e="T697" id="Seg_5262" s="T696">ɨgɨr-t-atalaː-bɨt</ta>
            <ta e="T698" id="Seg_5263" s="T697">če</ta>
            <ta e="T699" id="Seg_5264" s="T698">ogo-lor-u-m</ta>
            <ta e="T700" id="Seg_5265" s="T699">kepseː-ŋ</ta>
            <ta e="T701" id="Seg_5266" s="T700">tug-u</ta>
            <ta e="T702" id="Seg_5267" s="T701">gɨn-a</ta>
            <ta e="T703" id="Seg_5268" s="T702">bu</ta>
            <ta e="T704" id="Seg_5269" s="T703">turkarɨ</ta>
            <ta e="T705" id="Seg_5270" s="T704">hɨldʼ-ɨ-bɨk-kɨt=ɨj</ta>
            <ta e="T706" id="Seg_5271" s="T705">üs</ta>
            <ta e="T707" id="Seg_5272" s="T706">abaːhɨ</ta>
            <ta e="T708" id="Seg_5273" s="T707">irge-ti-tten</ta>
            <ta e="T709" id="Seg_5274" s="T708">üs</ta>
            <ta e="T710" id="Seg_5275" s="T709">huruk-taːk</ta>
            <ta e="T711" id="Seg_5276" s="T710">taːh-ɨ</ta>
            <ta e="T712" id="Seg_5277" s="T711">bul-but-u-m</ta>
            <ta e="T713" id="Seg_5278" s="T712">ɨt</ta>
            <ta e="T714" id="Seg_5279" s="T713">u͡ol-a</ta>
            <ta e="T715" id="Seg_5280" s="T714">dʼi͡e-bit</ta>
            <ta e="T716" id="Seg_5281" s="T715">ɨraːktaːgɨ</ta>
            <ta e="T717" id="Seg_5282" s="T716">iliː-ti-ger</ta>
            <ta e="T718" id="Seg_5283" s="T717">üs</ta>
            <ta e="T719" id="Seg_5284" s="T718">taːh-ɨ</ta>
            <ta e="T720" id="Seg_5285" s="T719">tuttar-an</ta>
            <ta e="T721" id="Seg_5286" s="T720">keːs-pit</ta>
            <ta e="T722" id="Seg_5287" s="T721">ɨːraːktaːgɨ</ta>
            <ta e="T723" id="Seg_5288" s="T722">ɨt</ta>
            <ta e="T724" id="Seg_5289" s="T723">u͡ol-u-n</ta>
            <ta e="T725" id="Seg_5290" s="T724">moːj-u-ttan</ta>
            <ta e="T726" id="Seg_5291" s="T725">kuːh-an</ta>
            <ta e="T727" id="Seg_5292" s="T726">ɨl-bɨt</ta>
            <ta e="T728" id="Seg_5293" s="T727">ɨt-a-msɨj-a</ta>
            <ta e="T729" id="Seg_5294" s="T728">tüh-en</ta>
            <ta e="T730" id="Seg_5295" s="T729">baran</ta>
            <ta e="T731" id="Seg_5296" s="T730">dʼi͡e-bit</ta>
            <ta e="T732" id="Seg_5297" s="T731">bügüŋŋü-tten</ta>
            <ta e="T733" id="Seg_5298" s="T732">en</ta>
            <ta e="T734" id="Seg_5299" s="T733">ɨraːktaːgɨ</ta>
            <ta e="T735" id="Seg_5300" s="T734">bu͡ol-ar</ta>
            <ta e="T736" id="Seg_5301" s="T735">ɨjaːg-ɨ-ŋ</ta>
            <ta e="T737" id="Seg_5302" s="T736">kel-l-e</ta>
            <ta e="T738" id="Seg_5303" s="T737">u͡ol-bu-n</ta>
            <ta e="T739" id="Seg_5304" s="T738">kɨtta</ta>
            <ta e="T740" id="Seg_5305" s="T739">ubaj-balɨs</ta>
            <ta e="T741" id="Seg_5306" s="T740">bu͡ol-an</ta>
            <ta e="T742" id="Seg_5307" s="T741">olor-u-ŋ</ta>
            <ta e="T743" id="Seg_5308" s="T742">üs</ta>
            <ta e="T744" id="Seg_5309" s="T743">u͡ol</ta>
            <ta e="T745" id="Seg_5310" s="T744">dogor-duː</ta>
            <ta e="T746" id="Seg_5311" s="T745">baj-an-taj-an</ta>
            <ta e="T747" id="Seg_5312" s="T746">olor-but-tar</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_5313" s="T0">bɨlɨr-bɨlɨr</ta>
            <ta e="T2" id="Seg_5314" s="T1">biːr</ta>
            <ta e="T3" id="Seg_5315" s="T2">ɨraːktaːgɨ</ta>
            <ta e="T4" id="Seg_5316" s="T3">olor-BIT</ta>
            <ta e="T5" id="Seg_5317" s="T4">ogo-tA</ta>
            <ta e="T6" id="Seg_5318" s="T5">hu͡ok</ta>
            <ta e="T7" id="Seg_5319" s="T6">e-TI-tA</ta>
            <ta e="T8" id="Seg_5320" s="T7">e-BIT</ta>
            <ta e="T9" id="Seg_5321" s="T8">biːrde</ta>
            <ta e="T10" id="Seg_5322" s="T9">bu</ta>
            <ta e="T11" id="Seg_5323" s="T10">ɨraːktaːgɨ</ta>
            <ta e="T12" id="Seg_5324" s="T11">hanaː-BIT</ta>
            <ta e="T13" id="Seg_5325" s="T12">bu</ta>
            <ta e="T14" id="Seg_5326" s="T13">togo</ta>
            <ta e="T15" id="Seg_5327" s="T14">bihigi</ta>
            <ta e="T16" id="Seg_5328" s="T15">ogo-tA</ta>
            <ta e="T17" id="Seg_5329" s="T16">hu͡ok-BIt=Ij</ta>
            <ta e="T18" id="Seg_5330" s="T17">kɨrdʼagas-LAr-ttAn</ta>
            <ta e="T19" id="Seg_5331" s="T18">ɨjɨt-BIT</ta>
            <ta e="T20" id="Seg_5332" s="T19">kihi</ta>
            <ta e="T21" id="Seg_5333" s="T20">bu</ta>
            <ta e="T22" id="Seg_5334" s="T21">hanaː-A</ta>
            <ta e="T23" id="Seg_5335" s="T22">olor-TAK-InA</ta>
            <ta e="T24" id="Seg_5336" s="T23">dʼaktar-tA</ta>
            <ta e="T25" id="Seg_5337" s="T24">kiːr-BIT</ta>
            <ta e="T26" id="Seg_5338" s="T25">tu͡ok-nI</ta>
            <ta e="T27" id="Seg_5339" s="T26">hanaːrgaː-TI-ŋ</ta>
            <ta e="T28" id="Seg_5340" s="T27">dogor</ta>
            <ta e="T29" id="Seg_5341" s="T28">harsɨːn</ta>
            <ta e="T30" id="Seg_5342" s="T29">bihigi</ta>
            <ta e="T31" id="Seg_5343" s="T30">kihi-LAr-nI</ta>
            <ta e="T32" id="Seg_5344" s="T31">komuj-IAK-BIt</ta>
            <ta e="T33" id="Seg_5345" s="T32">kannɨk-GA</ta>
            <ta e="T34" id="Seg_5346" s="T33">di͡eri</ta>
            <ta e="T35" id="Seg_5347" s="T34">iččitek</ta>
            <ta e="T36" id="Seg_5348" s="T35">isti͡ene-nI</ta>
            <ta e="T37" id="Seg_5349" s="T36">kör-An</ta>
            <ta e="T38" id="Seg_5350" s="T37">olor-IAK-BIt=Ij</ta>
            <ta e="T39" id="Seg_5351" s="T38">ogo-tA</ta>
            <ta e="T40" id="Seg_5352" s="T39">hu͡ok</ta>
            <ta e="T41" id="Seg_5353" s="T40">ɨjɨt-IAk</ta>
            <ta e="T42" id="Seg_5354" s="T41">togo</ta>
            <ta e="T43" id="Seg_5355" s="T42">ogo-tA</ta>
            <ta e="T44" id="Seg_5356" s="T43">hu͡ok-BIt=Ij</ta>
            <ta e="T45" id="Seg_5357" s="T44">kim-GA</ta>
            <ta e="T46" id="Seg_5358" s="T45">baːj-BItI-n</ta>
            <ta e="T47" id="Seg_5359" s="T46">bi͡er-IAK-BIt=Ij</ta>
            <ta e="T48" id="Seg_5360" s="T47">öl-TAK-BItInA</ta>
            <ta e="T49" id="Seg_5361" s="T48">harsi͡erda</ta>
            <ta e="T50" id="Seg_5362" s="T49">tur-BIT-LArA</ta>
            <ta e="T51" id="Seg_5363" s="T50">ügüs</ta>
            <ta e="T52" id="Seg_5364" s="T51">bagajɨ</ta>
            <ta e="T53" id="Seg_5365" s="T52">kihi</ta>
            <ta e="T54" id="Seg_5366" s="T53">komuj-LIN-I-BIT</ta>
            <ta e="T55" id="Seg_5367" s="T54">onno</ta>
            <ta e="T56" id="Seg_5368" s="T55">ɨraːktaːgɨ</ta>
            <ta e="T57" id="Seg_5369" s="T56">ɨjɨt-BIT</ta>
            <ta e="T58" id="Seg_5370" s="T57">bu</ta>
            <ta e="T59" id="Seg_5371" s="T58">togo</ta>
            <ta e="T60" id="Seg_5372" s="T59">bihigi</ta>
            <ta e="T61" id="Seg_5373" s="T60">ogo-tA</ta>
            <ta e="T62" id="Seg_5374" s="T61">hu͡ok-BIt=Ij</ta>
            <ta e="T63" id="Seg_5375" s="T62">kajdak</ta>
            <ta e="T64" id="Seg_5376" s="T63">tu͡ok-nI</ta>
            <ta e="T65" id="Seg_5377" s="T64">gɨn-TAK-GA</ta>
            <ta e="T66" id="Seg_5378" s="T65">ogo-LAN-IAK-BIt=Ij</ta>
            <ta e="T67" id="Seg_5379" s="T66">ehigi</ta>
            <ta e="T68" id="Seg_5380" s="T67">kajdak</ta>
            <ta e="T69" id="Seg_5381" s="T68">di͡e-A</ta>
            <ta e="T70" id="Seg_5382" s="T69">hanaː-A-GIt</ta>
            <ta e="T71" id="Seg_5383" s="T70">kihi-LAr</ta>
            <ta e="T72" id="Seg_5384" s="T71">ajdaːj-A</ta>
            <ta e="T73" id="Seg_5385" s="T72">tüs-BIT-LAr</ta>
            <ta e="T74" id="Seg_5386" s="T73">aːn-tI-nAn</ta>
            <ta e="T75" id="Seg_5387" s="T74">kɨrdʼagas</ta>
            <ta e="T76" id="Seg_5388" s="T75">hokkor</ta>
            <ta e="T77" id="Seg_5389" s="T76">ogonnʼor</ta>
            <ta e="T78" id="Seg_5390" s="T77">bɨltaj-s</ta>
            <ta e="T79" id="Seg_5391" s="T78">gɨn-BIT</ta>
            <ta e="T80" id="Seg_5392" s="T79">hokkor</ta>
            <ta e="T81" id="Seg_5393" s="T80">erej-LAːK-nI</ta>
            <ta e="T82" id="Seg_5394" s="T81">orto-GA</ta>
            <ta e="T83" id="Seg_5395" s="T82">üt-An</ta>
            <ta e="T84" id="Seg_5396" s="T83">keːs-BIT-LAr</ta>
            <ta e="T85" id="Seg_5397" s="T84">bu</ta>
            <ta e="T86" id="Seg_5398" s="T85">baːr</ta>
            <ta e="T87" id="Seg_5399" s="T86">bil-Ar</ta>
            <ta e="T88" id="Seg_5400" s="T87">kihi</ta>
            <ta e="T89" id="Seg_5401" s="T88">ogonnʼor</ta>
            <ta e="T90" id="Seg_5402" s="T89">örö</ta>
            <ta e="T91" id="Seg_5403" s="T90">kör-An</ta>
            <ta e="T92" id="Seg_5404" s="T91">baran</ta>
            <ta e="T93" id="Seg_5405" s="T92">his</ta>
            <ta e="T94" id="Seg_5406" s="T93">tutun-An</ta>
            <ta e="T95" id="Seg_5407" s="T94">baran</ta>
            <ta e="T96" id="Seg_5408" s="T95">dʼi͡e-BIT</ta>
            <ta e="T97" id="Seg_5409" s="T96">oŋor-I-ŋ</ta>
            <ta e="T98" id="Seg_5410" s="T97">kömüs</ta>
            <ta e="T99" id="Seg_5411" s="T98">ilim-nA</ta>
            <ta e="T100" id="Seg_5412" s="T99">kömüs</ta>
            <ta e="T101" id="Seg_5413" s="T100">tɨː-TA</ta>
            <ta e="T102" id="Seg_5414" s="T101">kömüs</ta>
            <ta e="T103" id="Seg_5415" s="T102">erdiː-TA</ta>
            <ta e="T104" id="Seg_5416" s="T103">onton</ta>
            <ta e="T105" id="Seg_5417" s="T104">ɨraːktaːgɨ</ta>
            <ta e="T106" id="Seg_5418" s="T105">balɨktaː-A</ta>
            <ta e="T107" id="Seg_5419" s="T106">bar-TIn</ta>
            <ta e="T108" id="Seg_5420" s="T107">ilim-tI-GAr</ta>
            <ta e="T109" id="Seg_5421" s="T108">balɨk</ta>
            <ta e="T110" id="Seg_5422" s="T109">tut-IAK.[tA]</ta>
            <ta e="T111" id="Seg_5423" s="T110">ol-nI</ta>
            <ta e="T112" id="Seg_5424" s="T111">dʼaktar-tA</ta>
            <ta e="T113" id="Seg_5425" s="T112">hi͡e-TIn</ta>
            <ta e="T114" id="Seg_5426" s="T113">ɨraːktaːgɨ</ta>
            <ta e="T115" id="Seg_5427" s="T114">dʼaktar-tA</ta>
            <ta e="T116" id="Seg_5428" s="T115">kül-BIT</ta>
            <ta e="T117" id="Seg_5429" s="T116">ol-ttAn</ta>
            <ta e="T118" id="Seg_5430" s="T117">min</ta>
            <ta e="T119" id="Seg_5431" s="T118">tot-IAK-m</ta>
            <ta e="T120" id="Seg_5432" s="T119">du͡o</ta>
            <ta e="T121" id="Seg_5433" s="T120">ör</ta>
            <ta e="T122" id="Seg_5434" s="T121">bu͡ol-IAK.[tA]=Ij</ta>
            <ta e="T123" id="Seg_5435" s="T122">ɨraːktaːgɨ</ta>
            <ta e="T124" id="Seg_5436" s="T123">harsɨŋŋɨ-tI-n</ta>
            <ta e="T125" id="Seg_5437" s="T124">kömüs</ta>
            <ta e="T126" id="Seg_5438" s="T125">tɨː-LAːK</ta>
            <ta e="T127" id="Seg_5439" s="T126">kömüs</ta>
            <ta e="T128" id="Seg_5440" s="T127">erdiː-LAːK</ta>
            <ta e="T129" id="Seg_5441" s="T128">balɨk-LAN-A</ta>
            <ta e="T130" id="Seg_5442" s="T129">bar-BIT</ta>
            <ta e="T131" id="Seg_5443" s="T130">biːr</ta>
            <ta e="T132" id="Seg_5444" s="T131">balɨk-nI</ta>
            <ta e="T133" id="Seg_5445" s="T132">ilibiret-An</ta>
            <ta e="T134" id="Seg_5446" s="T133">tɨːnnaːk-LIː</ta>
            <ta e="T135" id="Seg_5447" s="T134">egel-BAT</ta>
            <ta e="T136" id="Seg_5448" s="T135">du͡o</ta>
            <ta e="T137" id="Seg_5449" s="T136">bu</ta>
            <ta e="T138" id="Seg_5450" s="T137">bult-I-m</ta>
            <ta e="T139" id="Seg_5451" s="T138">dʼi͡e-BIT</ta>
            <ta e="T140" id="Seg_5452" s="T139">da</ta>
            <ta e="T141" id="Seg_5453" s="T140">asčɨt-LArI-GAr</ta>
            <ta e="T142" id="Seg_5454" s="T141">bi͡er-An</ta>
            <ta e="T143" id="Seg_5455" s="T142">keːs-BIT</ta>
            <ta e="T144" id="Seg_5456" s="T143">hanɨkaːn</ta>
            <ta e="T145" id="Seg_5457" s="T144">kü͡östeː-ŋ</ta>
            <ta e="T146" id="Seg_5458" s="T145">asčɨt</ta>
            <ta e="T147" id="Seg_5459" s="T146">balɨk</ta>
            <ta e="T148" id="Seg_5460" s="T147">katɨrɨk-tI-n</ta>
            <ta e="T149" id="Seg_5461" s="T148">ot</ta>
            <ta e="T150" id="Seg_5462" s="T149">is-tI-GAr</ta>
            <ta e="T151" id="Seg_5463" s="T150">ɨraːstaː-An</ta>
            <ta e="T152" id="Seg_5464" s="T151">keːs-BIT</ta>
            <ta e="T153" id="Seg_5465" s="T152">ol-nI</ta>
            <ta e="T154" id="Seg_5466" s="T153">ɨraːktaːgɨ</ta>
            <ta e="T155" id="Seg_5467" s="T154">koru͡oba-tA</ta>
            <ta e="T156" id="Seg_5468" s="T155">hi͡e-An</ta>
            <ta e="T157" id="Seg_5469" s="T156">keːs-BIT</ta>
            <ta e="T158" id="Seg_5470" s="T157">ɨraːktaːgɨ</ta>
            <ta e="T159" id="Seg_5471" s="T158">dʼaktar-tA</ta>
            <ta e="T160" id="Seg_5472" s="T159">balɨk-nI</ta>
            <ta e="T161" id="Seg_5473" s="T160">hi͡e-An</ta>
            <ta e="T162" id="Seg_5474" s="T161">baran</ta>
            <ta e="T163" id="Seg_5475" s="T162">oŋu͡ok-tI-n</ta>
            <ta e="T164" id="Seg_5476" s="T163">teri͡elke-GA</ta>
            <ta e="T165" id="Seg_5477" s="T164">kaːl-TAr-BIT-tI-n</ta>
            <ta e="T166" id="Seg_5478" s="T165">asčɨt</ta>
            <ta e="T167" id="Seg_5479" s="T166">ɨt-GA</ta>
            <ta e="T168" id="Seg_5480" s="T167">bɨrak-BIT</ta>
            <ta e="T169" id="Seg_5481" s="T168">oloŋko-GA</ta>
            <ta e="T170" id="Seg_5482" s="T169">ör</ta>
            <ta e="T171" id="Seg_5483" s="T170">bu͡ol-IAK.[tA]</ta>
            <ta e="T172" id="Seg_5484" s="T171">du͡o</ta>
            <ta e="T173" id="Seg_5485" s="T172">üs-IAn-LArA</ta>
            <ta e="T174" id="Seg_5486" s="T173">üs</ta>
            <ta e="T175" id="Seg_5487" s="T174">u͡ol-nI</ta>
            <ta e="T176" id="Seg_5488" s="T175">töröː-BIT-LAr</ta>
            <ta e="T177" id="Seg_5489" s="T176">ogo-LAr</ta>
            <ta e="T178" id="Seg_5490" s="T177">töhö</ta>
            <ta e="T179" id="Seg_5491" s="T178">da</ta>
            <ta e="T180" id="Seg_5492" s="T179">bu͡ol-BAkkA</ta>
            <ta e="T181" id="Seg_5493" s="T180">biːrge</ta>
            <ta e="T182" id="Seg_5494" s="T181">oːnnʼoː-Ar</ta>
            <ta e="T183" id="Seg_5495" s="T182">bu͡ol-BIT-LAr</ta>
            <ta e="T184" id="Seg_5496" s="T183">biːrde</ta>
            <ta e="T185" id="Seg_5497" s="T184">agaj</ta>
            <ta e="T186" id="Seg_5498" s="T185">ɨraːktaːgɨ</ta>
            <ta e="T187" id="Seg_5499" s="T186">u͡ol-tA</ta>
            <ta e="T188" id="Seg_5500" s="T187">dʼi͡e-BIT</ta>
            <ta e="T189" id="Seg_5501" s="T188">dogor-LAr</ta>
            <ta e="T190" id="Seg_5502" s="T189">tɨː-LAN-A</ta>
            <ta e="T191" id="Seg_5503" s="T190">bar-IAgIŋ</ta>
            <ta e="T192" id="Seg_5504" s="T191">u͡ol-LAr</ta>
            <ta e="T193" id="Seg_5505" s="T192">töhö</ta>
            <ta e="T194" id="Seg_5506" s="T193">ör</ta>
            <ta e="T195" id="Seg_5507" s="T194">ert-An</ta>
            <ta e="T196" id="Seg_5508" s="T195">is-BIT-LArA</ta>
            <ta e="T197" id="Seg_5509" s="T196">bu͡olla</ta>
            <ta e="T198" id="Seg_5510" s="T197">kör-BIT-LArA</ta>
            <ta e="T199" id="Seg_5511" s="T198">ebe</ta>
            <ta e="T200" id="Seg_5512" s="T199">onu͡or</ta>
            <ta e="T201" id="Seg_5513" s="T200">ulakan</ta>
            <ta e="T202" id="Seg_5514" s="T201">bagajɨ</ta>
            <ta e="T203" id="Seg_5515" s="T202">balagan</ta>
            <ta e="T204" id="Seg_5516" s="T203">dʼi͡e</ta>
            <ta e="T205" id="Seg_5517" s="T204">tur-Ar</ta>
            <ta e="T206" id="Seg_5518" s="T205">iti</ta>
            <ta e="T207" id="Seg_5519" s="T206">tu͡ok-BIt=Ij</ta>
            <ta e="T208" id="Seg_5520" s="T207">di͡e-A-s-BIT-LAr</ta>
            <ta e="T209" id="Seg_5521" s="T208">ɨraːktaːgɨ</ta>
            <ta e="T210" id="Seg_5522" s="T209">u͡ol-tA</ta>
            <ta e="T211" id="Seg_5523" s="T210">tigis-IAgIŋ</ta>
            <ta e="T212" id="Seg_5524" s="T211">kaːrtɨ-LAː-IAK-BIt</ta>
            <ta e="T213" id="Seg_5525" s="T212">dʼi͡e-BIT</ta>
            <ta e="T214" id="Seg_5526" s="T213">ɨt</ta>
            <ta e="T215" id="Seg_5527" s="T214">u͡ol-tA</ta>
            <ta e="T216" id="Seg_5528" s="T215">eː</ta>
            <ta e="T217" id="Seg_5529" s="T216">togo</ta>
            <ta e="T218" id="Seg_5530" s="T217">bagar</ta>
            <ta e="T219" id="Seg_5531" s="T218">abaːhɨ-LAr</ta>
            <ta e="T220" id="Seg_5532" s="T219">dʼi͡e-LArA</ta>
            <ta e="T221" id="Seg_5533" s="T220">bu͡ol-IAK.[tA]</ta>
            <ta e="T222" id="Seg_5534" s="T221">koru͡oba</ta>
            <ta e="T223" id="Seg_5535" s="T222">u͡ol-tA</ta>
            <ta e="T224" id="Seg_5536" s="T223">tigis-IAgIŋ</ta>
            <ta e="T225" id="Seg_5537" s="T224">tigis-IAgIŋ</ta>
            <ta e="T226" id="Seg_5538" s="T225">kaːm-AlAː-A</ta>
            <ta e="T227" id="Seg_5539" s="T226">tüs-IAgIŋ</ta>
            <ta e="T228" id="Seg_5540" s="T227">kɨtɨl-GA</ta>
            <ta e="T229" id="Seg_5541" s="T228">tagɨs-AːT</ta>
            <ta e="T230" id="Seg_5542" s="T229">bu</ta>
            <ta e="T231" id="Seg_5543" s="T230">ikki</ta>
            <ta e="T232" id="Seg_5544" s="T231">u͡ol</ta>
            <ta e="T233" id="Seg_5545" s="T232">kaːrtɨ-LAː-A</ta>
            <ta e="T234" id="Seg_5546" s="T233">olor-BIT-LAr</ta>
            <ta e="T235" id="Seg_5547" s="T234">ɨt</ta>
            <ta e="T236" id="Seg_5548" s="T235">u͡ol-tA</ta>
            <ta e="T237" id="Seg_5549" s="T236">utuj-An</ta>
            <ta e="T238" id="Seg_5550" s="T237">kaːl-BIT</ta>
            <ta e="T239" id="Seg_5551" s="T238">töhö</ta>
            <ta e="T240" id="Seg_5552" s="T239">ör</ta>
            <ta e="T241" id="Seg_5553" s="T240">oːnnʼoː-BIT-LArA</ta>
            <ta e="T242" id="Seg_5554" s="T241">bu͡olla</ta>
            <ta e="T243" id="Seg_5555" s="T242">bu</ta>
            <ta e="T244" id="Seg_5556" s="T243">ogo-LAr</ta>
            <ta e="T245" id="Seg_5557" s="T244">emi͡e</ta>
            <ta e="T246" id="Seg_5558" s="T245">utuj-An</ta>
            <ta e="T247" id="Seg_5559" s="T246">kaːl-BIT-LAr</ta>
            <ta e="T248" id="Seg_5560" s="T247">ɨt</ta>
            <ta e="T249" id="Seg_5561" s="T248">u͡ol-tA</ta>
            <ta e="T250" id="Seg_5562" s="T249">tüːn</ta>
            <ta e="T251" id="Seg_5563" s="T250">orto</ta>
            <ta e="T252" id="Seg_5564" s="T251">uhugun-I-BIT-tA</ta>
            <ta e="T253" id="Seg_5565" s="T252">kihi</ta>
            <ta e="T254" id="Seg_5566" s="T253">kaːm-Ar</ta>
            <ta e="T255" id="Seg_5567" s="T254">tɨ͡as-tA</ta>
            <ta e="T256" id="Seg_5568" s="T255">ihilin-I-BIT</ta>
            <ta e="T257" id="Seg_5569" s="T256">tagɨs-I-BIT-tA</ta>
            <ta e="T258" id="Seg_5570" s="T257">abaːhɨ</ta>
            <ta e="T259" id="Seg_5571" s="T258">balagan</ta>
            <ta e="T260" id="Seg_5572" s="T259">dek</ta>
            <ta e="T261" id="Seg_5573" s="T260">kaːm-An</ta>
            <ta e="T262" id="Seg_5574" s="T261">is-AːktAː-Ar</ta>
            <ta e="T263" id="Seg_5575" s="T262">dʼe</ta>
            <ta e="T264" id="Seg_5576" s="T263">o</ta>
            <ta e="T265" id="Seg_5577" s="T264">dʼe</ta>
            <ta e="T266" id="Seg_5578" s="T265">öl-BIT-BIt</ta>
            <ta e="T267" id="Seg_5579" s="T266">e-BIT</ta>
            <ta e="T268" id="Seg_5580" s="T267">dʼi͡e-An</ta>
            <ta e="T269" id="Seg_5581" s="T268">baran</ta>
            <ta e="T270" id="Seg_5582" s="T269">dogor-LAr-tI-n</ta>
            <ta e="T271" id="Seg_5583" s="T270">uhugun-TAr-A</ta>
            <ta e="T272" id="Seg_5584" s="T271">hataː-BIT</ta>
            <ta e="T273" id="Seg_5585" s="T272">bu</ta>
            <ta e="T274" id="Seg_5586" s="T273">u͡ol-LAr</ta>
            <ta e="T275" id="Seg_5587" s="T274">togo</ta>
            <ta e="T276" id="Seg_5588" s="T275">uhugun-IAK-LArA=Ij</ta>
            <ta e="T277" id="Seg_5589" s="T276">kel-kel</ta>
            <ta e="T278" id="Seg_5590" s="T277">min</ta>
            <ta e="T279" id="Seg_5591" s="T278">en-n</ta>
            <ta e="T280" id="Seg_5592" s="T279">hi͡e-IAK-m</ta>
            <ta e="T281" id="Seg_5593" s="T280">abaːhɨ</ta>
            <ta e="T282" id="Seg_5594" s="T281">haŋa-tA</ta>
            <ta e="T283" id="Seg_5595" s="T282">ihilin-I-BIT</ta>
            <ta e="T284" id="Seg_5596" s="T283">bu</ta>
            <ta e="T285" id="Seg_5597" s="T284">u͡ol</ta>
            <ta e="T286" id="Seg_5598" s="T285">tagɨs-A</ta>
            <ta e="T287" id="Seg_5599" s="T286">köt-BIT</ta>
            <ta e="T288" id="Seg_5600" s="T287">da</ta>
            <ta e="T289" id="Seg_5601" s="T288">abaːhɨ-nI</ta>
            <ta e="T290" id="Seg_5602" s="T289">kɨtta</ta>
            <ta e="T291" id="Seg_5603" s="T290">ölörös-I-BIT</ta>
            <ta e="T292" id="Seg_5604" s="T291">töhö</ta>
            <ta e="T293" id="Seg_5605" s="T292">da</ta>
            <ta e="T294" id="Seg_5606" s="T293">bu͡ol-BAkkA</ta>
            <ta e="T295" id="Seg_5607" s="T294">abaːhɨ</ta>
            <ta e="T296" id="Seg_5608" s="T295">menʼiː-tI-n</ta>
            <ta e="T297" id="Seg_5609" s="T296">boldok</ta>
            <ta e="T298" id="Seg_5610" s="T297">taːs-GA</ta>
            <ta e="T299" id="Seg_5611" s="T298">kaj-A</ta>
            <ta e="T300" id="Seg_5612" s="T299">ogus-I-BIT</ta>
            <ta e="T301" id="Seg_5613" s="T300">ol</ta>
            <ta e="T302" id="Seg_5614" s="T301">gɨn-An</ta>
            <ta e="T303" id="Seg_5615" s="T302">baran</ta>
            <ta e="T304" id="Seg_5616" s="T303">irge-tI-n</ta>
            <ta e="T305" id="Seg_5617" s="T304">is-tI-ttAn</ta>
            <ta e="T306" id="Seg_5618" s="T305">huruk-LAːK</ta>
            <ta e="T307" id="Seg_5619" s="T306">taːs-nI</ta>
            <ta e="T308" id="Seg_5620" s="T307">oroː-BIT</ta>
            <ta e="T309" id="Seg_5621" s="T308">taːs-nI</ta>
            <ta e="T310" id="Seg_5622" s="T309">karmaːn-tI-n</ta>
            <ta e="T311" id="Seg_5623" s="T310">is-tI-nAn</ta>
            <ta e="T312" id="Seg_5624" s="T311">ugun-An</ta>
            <ta e="T313" id="Seg_5625" s="T312">keːs-BIT</ta>
            <ta e="T314" id="Seg_5626" s="T313">harsi͡erda</ta>
            <ta e="T315" id="Seg_5627" s="T314">tu͡ok</ta>
            <ta e="T316" id="Seg_5628" s="T315">da</ta>
            <ta e="T317" id="Seg_5629" s="T316">bu͡ol-BAtAK</ta>
            <ta e="T318" id="Seg_5630" s="T317">kördük</ta>
            <ta e="T319" id="Seg_5631" s="T318">ɨt</ta>
            <ta e="T320" id="Seg_5632" s="T319">u͡ol-tA</ta>
            <ta e="T321" id="Seg_5633" s="T320">dogor-LAr-tI-n</ta>
            <ta e="T322" id="Seg_5634" s="T321">uhugun-TAr-TAː-BIT</ta>
            <ta e="T323" id="Seg_5635" s="T322">dʼe</ta>
            <ta e="T324" id="Seg_5636" s="T323">kaːs-LAN-A</ta>
            <ta e="T325" id="Seg_5637" s="T324">bar-IAgIŋ</ta>
            <ta e="T326" id="Seg_5638" s="T325">u͡ol-LAr</ta>
            <ta e="T327" id="Seg_5639" s="T326">tɨːllaŋnaː-An</ta>
            <ta e="T328" id="Seg_5640" s="T327">arɨːččɨ</ta>
            <ta e="T329" id="Seg_5641" s="T328">tur-An</ta>
            <ta e="T330" id="Seg_5642" s="T329">čaːj</ta>
            <ta e="T331" id="Seg_5643" s="T330">is-BIT-LAr</ta>
            <ta e="T332" id="Seg_5644" s="T331">oloŋko-GA</ta>
            <ta e="T333" id="Seg_5645" s="T332">kün</ta>
            <ta e="T334" id="Seg_5646" s="T333">kɨlgas</ta>
            <ta e="T335" id="Seg_5647" s="T334">töhö</ta>
            <ta e="T336" id="Seg_5648" s="T335">da</ta>
            <ta e="T337" id="Seg_5649" s="T336">bu͡ol-BAkkA</ta>
            <ta e="T338" id="Seg_5650" s="T337">kallaːn</ta>
            <ta e="T339" id="Seg_5651" s="T338">karaŋar-BIT</ta>
            <ta e="T340" id="Seg_5652" s="T339">ɨt</ta>
            <ta e="T341" id="Seg_5653" s="T340">u͡ol-tA</ta>
            <ta e="T342" id="Seg_5654" s="T341">ahaː-An-hi͡e-An</ta>
            <ta e="T343" id="Seg_5655" s="T342">baran</ta>
            <ta e="T344" id="Seg_5656" s="T343">utuj-An</ta>
            <ta e="T345" id="Seg_5657" s="T344">kaːl-BIT</ta>
            <ta e="T346" id="Seg_5658" s="T345">dogor-LAr-tA</ta>
            <ta e="T347" id="Seg_5659" s="T346">emi͡e</ta>
            <ta e="T348" id="Seg_5660" s="T347">kaːrtɨ-LAː-A</ta>
            <ta e="T349" id="Seg_5661" s="T348">olor-BIT-LAr</ta>
            <ta e="T350" id="Seg_5662" s="T349">tüːn</ta>
            <ta e="T351" id="Seg_5663" s="T350">orto</ta>
            <ta e="T352" id="Seg_5664" s="T351">uhugun-I-BIT-tA</ta>
            <ta e="T353" id="Seg_5665" s="T352">dogor-LAr-tA</ta>
            <ta e="T354" id="Seg_5666" s="T353">olort-A</ta>
            <ta e="T355" id="Seg_5667" s="T354">olor-BIT-tI-nAn</ta>
            <ta e="T356" id="Seg_5668" s="T355">utuj-An</ta>
            <ta e="T357" id="Seg_5669" s="T356">kaːl-BIT-LAr</ta>
            <ta e="T358" id="Seg_5670" s="T357">bu</ta>
            <ta e="T359" id="Seg_5671" s="T358">ogo</ta>
            <ta e="T360" id="Seg_5672" s="T359">uhugun-An</ta>
            <ta e="T361" id="Seg_5673" s="T360">baran</ta>
            <ta e="T362" id="Seg_5674" s="T361">ihilleː-A</ta>
            <ta e="T363" id="Seg_5675" s="T362">hɨt-BIT</ta>
            <ta e="T364" id="Seg_5676" s="T363">ɨraːk</ta>
            <ta e="T365" id="Seg_5677" s="T364">kanna</ta>
            <ta e="T366" id="Seg_5678" s="T365">ere</ta>
            <ta e="T367" id="Seg_5679" s="T366">emi͡e</ta>
            <ta e="T368" id="Seg_5680" s="T367">kaːm-Ar</ta>
            <ta e="T369" id="Seg_5681" s="T368">tɨ͡as</ta>
            <ta e="T370" id="Seg_5682" s="T369">ihilin-I-BIT</ta>
            <ta e="T371" id="Seg_5683" s="T370">dogor-LAr-tI-n</ta>
            <ta e="T372" id="Seg_5684" s="T371">uhugun-TAr-BIT-tA</ta>
            <ta e="T373" id="Seg_5685" s="T372">togo</ta>
            <ta e="T374" id="Seg_5686" s="T373">uhugun-IAK-LArA=Ij</ta>
            <ta e="T375" id="Seg_5687" s="T374">čubu</ta>
            <ta e="T376" id="Seg_5688" s="T375">utuj-BIT</ta>
            <ta e="T377" id="Seg_5689" s="T376">kihi-LAr</ta>
            <ta e="T378" id="Seg_5690" s="T377">ɨt</ta>
            <ta e="T379" id="Seg_5691" s="T378">u͡ol-tA</ta>
            <ta e="T380" id="Seg_5692" s="T379">tagɨs-I-BIT</ta>
            <ta e="T381" id="Seg_5693" s="T380">da</ta>
            <ta e="T382" id="Seg_5694" s="T381">ikki-Is</ta>
            <ta e="T383" id="Seg_5695" s="T382">abaːhɨ</ta>
            <ta e="T384" id="Seg_5696" s="T383">menʼiː-tI-n</ta>
            <ta e="T385" id="Seg_5697" s="T384">emi͡e</ta>
            <ta e="T386" id="Seg_5698" s="T385">boldok</ta>
            <ta e="T387" id="Seg_5699" s="T386">taːs-GA</ta>
            <ta e="T388" id="Seg_5700" s="T387">kampɨ</ta>
            <ta e="T389" id="Seg_5701" s="T388">ogus-I-BIT</ta>
            <ta e="T390" id="Seg_5702" s="T389">abaːhɨ</ta>
            <ta e="T391" id="Seg_5703" s="T390">tebi͡elen-A</ta>
            <ta e="T392" id="Seg_5704" s="T391">tüs-An</ta>
            <ta e="T393" id="Seg_5705" s="T392">baran</ta>
            <ta e="T394" id="Seg_5706" s="T393">čirkej-s</ta>
            <ta e="T395" id="Seg_5707" s="T394">gɨn-BIT</ta>
            <ta e="T396" id="Seg_5708" s="T395">ɨt</ta>
            <ta e="T397" id="Seg_5709" s="T396">u͡ol-tA</ta>
            <ta e="T398" id="Seg_5710" s="T397">irge-tI-ttAn</ta>
            <ta e="T399" id="Seg_5711" s="T398">huruk-LAːK</ta>
            <ta e="T400" id="Seg_5712" s="T399">taːs-nI</ta>
            <ta e="T401" id="Seg_5713" s="T400">oroː-TI-tA</ta>
            <ta e="T402" id="Seg_5714" s="T401">daːganɨ</ta>
            <ta e="T403" id="Seg_5715" s="T402">karmaːn-tI-GAr</ta>
            <ta e="T404" id="Seg_5716" s="T403">emi͡e</ta>
            <ta e="T405" id="Seg_5717" s="T404">ugun-An</ta>
            <ta e="T406" id="Seg_5718" s="T405">keːs-BIT</ta>
            <ta e="T407" id="Seg_5719" s="T406">üs-Is</ta>
            <ta e="T408" id="Seg_5720" s="T407">tüːn-tI-GAr</ta>
            <ta e="T409" id="Seg_5721" s="T408">üs-Is</ta>
            <ta e="T410" id="Seg_5722" s="T409">abaːhɨ</ta>
            <ta e="T411" id="Seg_5723" s="T410">menʼiː-tI-n</ta>
            <ta e="T412" id="Seg_5724" s="T411">kaj-A</ta>
            <ta e="T413" id="Seg_5725" s="T412">ogus-An</ta>
            <ta e="T414" id="Seg_5726" s="T413">huruk-LAːK</ta>
            <ta e="T415" id="Seg_5727" s="T414">taːs-nI</ta>
            <ta e="T416" id="Seg_5728" s="T415">oroː-A</ta>
            <ta e="T417" id="Seg_5729" s="T416">ogus-I-BIT</ta>
            <ta e="T418" id="Seg_5730" s="T417">dogor-LAr-tI-n</ta>
            <ta e="T419" id="Seg_5731" s="T418">im</ta>
            <ta e="T420" id="Seg_5732" s="T419">tur-Ar-tI-n</ta>
            <ta e="T421" id="Seg_5733" s="T420">kɨtta</ta>
            <ta e="T422" id="Seg_5734" s="T421">uhugun-TAr-BIT</ta>
            <ta e="T423" id="Seg_5735" s="T422">dʼe</ta>
            <ta e="T424" id="Seg_5736" s="T423">dʼi͡e-BItI-GAr</ta>
            <ta e="T425" id="Seg_5737" s="T424">bar-IAgIŋ</ta>
            <ta e="T426" id="Seg_5738" s="T425">dʼi͡e-BIT</ta>
            <ta e="T427" id="Seg_5739" s="T426">bu</ta>
            <ta e="T428" id="Seg_5740" s="T427">u͡ol</ta>
            <ta e="T429" id="Seg_5741" s="T428">ɨraːktaːgɨ</ta>
            <ta e="T430" id="Seg_5742" s="T429">u͡ol-tA</ta>
            <ta e="T431" id="Seg_5743" s="T430">öčös-BIT</ta>
            <ta e="T432" id="Seg_5744" s="T431">togo</ta>
            <ta e="T433" id="Seg_5745" s="T432">doː</ta>
            <ta e="T434" id="Seg_5746" s="T433">olor-A</ta>
            <ta e="T435" id="Seg_5747" s="T434">tüs-IAgIŋ</ta>
            <ta e="T436" id="Seg_5748" s="T435">beseleː</ta>
            <ta e="T437" id="Seg_5749" s="T436">koru͡oba</ta>
            <ta e="T438" id="Seg_5750" s="T437">u͡ol-tA</ta>
            <ta e="T439" id="Seg_5751" s="T438">olor-A</ta>
            <ta e="T440" id="Seg_5752" s="T439">tüs-An</ta>
            <ta e="T441" id="Seg_5753" s="T440">baran</ta>
            <ta e="T442" id="Seg_5754" s="T441">dʼi͡e-BIT</ta>
            <ta e="T443" id="Seg_5755" s="T442">kergen-LAr-BIt</ta>
            <ta e="T444" id="Seg_5756" s="T443">köhüt-IAK-LArA</ta>
            <ta e="T445" id="Seg_5757" s="T444">bar-IAgIŋ</ta>
            <ta e="T446" id="Seg_5758" s="T445">eːt</ta>
            <ta e="T447" id="Seg_5759" s="T446">u͡ol-LAr</ta>
            <ta e="T448" id="Seg_5760" s="T447">kamnaː-BIT-LAr</ta>
            <ta e="T449" id="Seg_5761" s="T448">ɨt</ta>
            <ta e="T450" id="Seg_5762" s="T449">u͡ol-tA</ta>
            <ta e="T451" id="Seg_5763" s="T450">ert-An</ta>
            <ta e="T452" id="Seg_5764" s="T451">is-BIT</ta>
            <ta e="T453" id="Seg_5765" s="T452">töhö</ta>
            <ta e="T454" id="Seg_5766" s="T453">da</ta>
            <ta e="T455" id="Seg_5767" s="T454">bu͡ol-BAkkA</ta>
            <ta e="T456" id="Seg_5768" s="T455">kɨːn-tI-n</ta>
            <ta e="T457" id="Seg_5769" s="T456">tutun-An</ta>
            <ta e="T458" id="Seg_5770" s="T457">bar-An</ta>
            <ta e="T459" id="Seg_5771" s="T458">dogor-LAr</ta>
            <ta e="T460" id="Seg_5772" s="T459">bahak-BI-n</ta>
            <ta e="T461" id="Seg_5773" s="T460">kaːl-TAr-BIT-BIn</ta>
            <ta e="T462" id="Seg_5774" s="T461">kördöː-A</ta>
            <ta e="T463" id="Seg_5775" s="T462">bar-IAK-m</ta>
            <ta e="T464" id="Seg_5776" s="T463">dʼi͡e-BIT</ta>
            <ta e="T465" id="Seg_5777" s="T464">ɨt</ta>
            <ta e="T466" id="Seg_5778" s="T465">u͡ol-tA</ta>
            <ta e="T467" id="Seg_5779" s="T466">balagan-GA</ta>
            <ta e="T468" id="Seg_5780" s="T467">kel-BIT-tA</ta>
            <ta e="T469" id="Seg_5781" s="T468">dʼaktar-LAr</ta>
            <ta e="T470" id="Seg_5782" s="T469">ɨtaː-Ar</ta>
            <ta e="T471" id="Seg_5783" s="T470">haŋa-LArA</ta>
            <ta e="T472" id="Seg_5784" s="T471">ihilin-I-BIT</ta>
            <ta e="T473" id="Seg_5785" s="T472">min</ta>
            <ta e="T474" id="Seg_5786" s="T473">minnʼiges</ta>
            <ta e="T475" id="Seg_5787" s="T474">as</ta>
            <ta e="T476" id="Seg_5788" s="T475">bu͡ol-An</ta>
            <ta e="T477" id="Seg_5789" s="T476">baran</ta>
            <ta e="T478" id="Seg_5790" s="T477">ulakan</ta>
            <ta e="T479" id="Seg_5791" s="T478">teri͡elke-GA</ta>
            <ta e="T480" id="Seg_5792" s="T479">tɨː-LArI-n</ta>
            <ta e="T481" id="Seg_5793" s="T480">is-tI-GAr</ta>
            <ta e="T482" id="Seg_5794" s="T481">tüs-IAK-m</ta>
            <ta e="T483" id="Seg_5795" s="T482">min-n</ta>
            <ta e="T484" id="Seg_5796" s="T483">hi͡e-TAK-TArInA</ta>
            <ta e="T485" id="Seg_5797" s="T484">öl-An</ta>
            <ta e="T486" id="Seg_5798" s="T485">kaːl-IAK-LArA</ta>
            <ta e="T487" id="Seg_5799" s="T486">dʼi͡e-BIT</ta>
            <ta e="T488" id="Seg_5800" s="T487">kɨrdʼagas</ta>
            <ta e="T489" id="Seg_5801" s="T488">Dʼige baːba</ta>
            <ta e="T490" id="Seg_5802" s="T489">ku͡olas-tA</ta>
            <ta e="T491" id="Seg_5803" s="T490">min</ta>
            <ta e="T492" id="Seg_5804" s="T491">bu͡ollagɨna</ta>
            <ta e="T493" id="Seg_5805" s="T492">kahan</ta>
            <ta e="T494" id="Seg_5806" s="T493">da</ta>
            <ta e="T495" id="Seg_5807" s="T494">kör-BAtAK</ta>
            <ta e="T496" id="Seg_5808" s="T495">bosku͡oj</ta>
            <ta e="T497" id="Seg_5809" s="T496">holko</ta>
            <ta e="T498" id="Seg_5810" s="T497">pɨlaːt</ta>
            <ta e="T499" id="Seg_5811" s="T498">bu͡ol-An</ta>
            <ta e="T500" id="Seg_5812" s="T499">ɨraːktaːgɨ</ta>
            <ta e="T501" id="Seg_5813" s="T500">u͡ol-tI-n</ta>
            <ta e="T502" id="Seg_5814" s="T501">töhök-tI-GAr</ta>
            <ta e="T503" id="Seg_5815" s="T502">tüs-IAK-m</ta>
            <ta e="T504" id="Seg_5816" s="T503">barɨ-kAːN-LArI-n</ta>
            <ta e="T505" id="Seg_5817" s="T504">moŋun-TAr-An</ta>
            <ta e="T506" id="Seg_5818" s="T505">ölör-IAK-m</ta>
            <ta e="T507" id="Seg_5819" s="T506">min</ta>
            <ta e="T508" id="Seg_5820" s="T507">bu͡ollagɨna</ta>
            <ta e="T509" id="Seg_5821" s="T508">tɨː-LArI-n</ta>
            <ta e="T510" id="Seg_5822" s="T509">ipseri</ta>
            <ta e="T511" id="Seg_5823" s="T510">hilimneː-Ar</ta>
            <ta e="T512" id="Seg_5824" s="T511">gɨn-A</ta>
            <ta e="T513" id="Seg_5825" s="T512">uː</ta>
            <ta e="T514" id="Seg_5826" s="T513">hɨmala</ta>
            <ta e="T515" id="Seg_5827" s="T514">bu͡ol-IAK-m</ta>
            <ta e="T516" id="Seg_5828" s="T515">barɨ-LArI-n</ta>
            <ta e="T517" id="Seg_5829" s="T516">obor-An</ta>
            <ta e="T518" id="Seg_5830" s="T517">timir-t-IAK-m</ta>
            <ta e="T519" id="Seg_5831" s="T518">dʼe</ta>
            <ta e="T520" id="Seg_5832" s="T519">üčügej</ta>
            <ta e="T521" id="Seg_5833" s="T520">bertik</ta>
            <ta e="T522" id="Seg_5834" s="T521">hanaː-BIT-GIt</ta>
            <ta e="T523" id="Seg_5835" s="T522">e-BIT</ta>
            <ta e="T524" id="Seg_5836" s="T523">dʼi͡e-BIT</ta>
            <ta e="T525" id="Seg_5837" s="T524">maŋnajgɨ</ta>
            <ta e="T526" id="Seg_5838" s="T525">dʼaktar</ta>
            <ta e="T527" id="Seg_5839" s="T526">ɨt</ta>
            <ta e="T528" id="Seg_5840" s="T527">u͡ol-tA</ta>
            <ta e="T529" id="Seg_5841" s="T528">iti-nI</ta>
            <ta e="T530" id="Seg_5842" s="T529">ihit-I-BIT</ta>
            <ta e="T531" id="Seg_5843" s="T530">daːganɨ</ta>
            <ta e="T532" id="Seg_5844" s="T531">hɨːr</ta>
            <ta e="T533" id="Seg_5845" s="T532">alɨn-tI-GAr</ta>
            <ta e="T534" id="Seg_5846" s="T533">hüːr-An</ta>
            <ta e="T535" id="Seg_5847" s="T534">kiːr-BIT</ta>
            <ta e="T536" id="Seg_5848" s="T535">tɨː-tI-n</ta>
            <ta e="T537" id="Seg_5849" s="T536">ɨl-BAktAː-Ar</ta>
            <ta e="T538" id="Seg_5850" s="T537">da</ta>
            <ta e="T539" id="Seg_5851" s="T538">ert-An</ta>
            <ta e="T540" id="Seg_5852" s="T539">is-BIT</ta>
            <ta e="T541" id="Seg_5853" s="T540">dogor-LAr-tI-n</ta>
            <ta e="T542" id="Seg_5854" s="T541">ebe</ta>
            <ta e="T543" id="Seg_5855" s="T542">orto-tI-GAr</ta>
            <ta e="T544" id="Seg_5856" s="T543">hit-A</ta>
            <ta e="T545" id="Seg_5857" s="T544">ogus-I-BIT</ta>
            <ta e="T546" id="Seg_5858" s="T545">u͡ol-LAr</ta>
            <ta e="T547" id="Seg_5859" s="T546">orgujakaːn</ta>
            <ta e="T548" id="Seg_5860" s="T547">ert-An</ta>
            <ta e="T549" id="Seg_5861" s="T548">is-BIT-LAr</ta>
            <ta e="T550" id="Seg_5862" s="T549">töhö</ta>
            <ta e="T551" id="Seg_5863" s="T550">ör</ta>
            <ta e="T552" id="Seg_5864" s="T551">bu͡ol-IAK.[tA]=Ij</ta>
            <ta e="T553" id="Seg_5865" s="T552">tɨː-LArI-n</ta>
            <ta e="T554" id="Seg_5866" s="T553">is-tI-GAr</ta>
            <ta e="T555" id="Seg_5867" s="T554">as-LAːK</ta>
            <ta e="T556" id="Seg_5868" s="T555">teri͡elke</ta>
            <ta e="T557" id="Seg_5869" s="T556">tüs-BIT</ta>
            <ta e="T558" id="Seg_5870" s="T557">ɨraːktaːgɨ</ta>
            <ta e="T559" id="Seg_5871" s="T558">u͡ol-tA</ta>
            <ta e="T560" id="Seg_5872" s="T559">ü͡ör-BIT</ta>
            <ta e="T561" id="Seg_5873" s="T560">iliː-tI-n</ta>
            <ta e="T562" id="Seg_5874" s="T561">teri͡elke</ta>
            <ta e="T563" id="Seg_5875" s="T562">dek</ta>
            <ta e="T564" id="Seg_5876" s="T563">harbaj-s</ta>
            <ta e="T565" id="Seg_5877" s="T564">gɨn-Ar-tI-n</ta>
            <ta e="T566" id="Seg_5878" s="T565">kɨtta</ta>
            <ta e="T567" id="Seg_5879" s="T566">ɨt</ta>
            <ta e="T568" id="Seg_5880" s="T567">u͡ol-tA</ta>
            <ta e="T569" id="Seg_5881" s="T568">as-LAːK</ta>
            <ta e="T570" id="Seg_5882" s="T569">teri͡elke-nI</ta>
            <ta e="T571" id="Seg_5883" s="T570">uː-GA</ta>
            <ta e="T572" id="Seg_5884" s="T571">bɨrak-An</ta>
            <ta e="T573" id="Seg_5885" s="T572">keːs-BIT</ta>
            <ta e="T574" id="Seg_5886" s="T573">ɨraːktaːgɨ</ta>
            <ta e="T575" id="Seg_5887" s="T574">u͡ol-tA</ta>
            <ta e="T576" id="Seg_5888" s="T575">kɨːhɨr-An</ta>
            <ta e="T577" id="Seg_5889" s="T576">huntuj-An</ta>
            <ta e="T578" id="Seg_5890" s="T577">olor-TAK-InA</ta>
            <ta e="T579" id="Seg_5891" s="T578">bosku͡oj</ta>
            <ta e="T580" id="Seg_5892" s="T579">bagajɨ</ta>
            <ta e="T581" id="Seg_5893" s="T580">holko</ta>
            <ta e="T582" id="Seg_5894" s="T581">pɨlaːt</ta>
            <ta e="T583" id="Seg_5895" s="T582">ɨraːktaːgɨ</ta>
            <ta e="T584" id="Seg_5896" s="T583">u͡ol-tI-n</ta>
            <ta e="T585" id="Seg_5897" s="T584">töhök-tI-GAr</ta>
            <ta e="T586" id="Seg_5898" s="T585">tüs-BIT</ta>
            <ta e="T587" id="Seg_5899" s="T586">o</ta>
            <ta e="T588" id="Seg_5900" s="T587">taŋara</ta>
            <ta e="T589" id="Seg_5901" s="T588">maːma-BA-r</ta>
            <ta e="T590" id="Seg_5902" s="T589">kehiː</ta>
            <ta e="T591" id="Seg_5903" s="T590">gɨn-IAK-m</ta>
            <ta e="T592" id="Seg_5904" s="T591">dʼi͡e-BIT</ta>
            <ta e="T593" id="Seg_5905" s="T592">da</ta>
            <ta e="T594" id="Seg_5906" s="T593">karmaːn-tI-GAr</ta>
            <ta e="T595" id="Seg_5907" s="T594">ugun-AːrI</ta>
            <ta e="T596" id="Seg_5908" s="T595">gɨn-Ar-tI-n</ta>
            <ta e="T597" id="Seg_5909" s="T596">kɨtta</ta>
            <ta e="T598" id="Seg_5910" s="T597">u͡ol-tA</ta>
            <ta e="T599" id="Seg_5911" s="T598">tal-An</ta>
            <ta e="T600" id="Seg_5912" s="T599">ɨl-BIT</ta>
            <ta e="T601" id="Seg_5913" s="T600">bahak-tI-nAn</ta>
            <ta e="T602" id="Seg_5914" s="T601">ilči</ta>
            <ta e="T603" id="Seg_5915" s="T602">kert-An</ta>
            <ta e="T604" id="Seg_5916" s="T603">baran</ta>
            <ta e="T605" id="Seg_5917" s="T604">uː-GA</ta>
            <ta e="T606" id="Seg_5918" s="T605">bɨrak-An</ta>
            <ta e="T607" id="Seg_5919" s="T606">keːs-BIT</ta>
            <ta e="T608" id="Seg_5920" s="T607">ɨraːktaːgɨ</ta>
            <ta e="T609" id="Seg_5921" s="T608">u͡ol-tA</ta>
            <ta e="T610" id="Seg_5922" s="T609">kɨːhɨr-An</ta>
            <ta e="T611" id="Seg_5923" s="T610">haŋa-tA</ta>
            <ta e="T612" id="Seg_5924" s="T611">da</ta>
            <ta e="T613" id="Seg_5925" s="T612">tagɨs-I-BAtAK</ta>
            <ta e="T614" id="Seg_5926" s="T613">töhö</ta>
            <ta e="T615" id="Seg_5927" s="T614">ör</ta>
            <ta e="T616" id="Seg_5928" s="T615">bu͡ol-IAK.[tA]=Ij</ta>
            <ta e="T617" id="Seg_5929" s="T616">tɨː-LArA</ta>
            <ta e="T618" id="Seg_5930" s="T617">uː-GA</ta>
            <ta e="T619" id="Seg_5931" s="T618">hɨhɨn-An</ta>
            <ta e="T620" id="Seg_5932" s="T619">kaːl-BIT</ta>
            <ta e="T621" id="Seg_5933" s="T620">ebe</ta>
            <ta e="T622" id="Seg_5934" s="T621">uː-tA</ta>
            <ta e="T623" id="Seg_5935" s="T622">karaːrt</ta>
            <ta e="T624" id="Seg_5936" s="T623">gɨn-BIT</ta>
            <ta e="T625" id="Seg_5937" s="T624">bu</ta>
            <ta e="T626" id="Seg_5938" s="T625">u͡ol-LAr</ta>
            <ta e="T627" id="Seg_5939" s="T626">hataː-An</ta>
            <ta e="T628" id="Seg_5940" s="T627">ert-I-n-BAT</ta>
            <ta e="T629" id="Seg_5941" s="T628">bu͡ol-An</ta>
            <ta e="T630" id="Seg_5942" s="T629">kaːl-BIT-LAr</ta>
            <ta e="T631" id="Seg_5943" s="T630">kuttan-BIT-LAr</ta>
            <ta e="T632" id="Seg_5944" s="T631">ɨt</ta>
            <ta e="T633" id="Seg_5945" s="T632">u͡ol-tA</ta>
            <ta e="T634" id="Seg_5946" s="T633">uː-nI</ta>
            <ta e="T635" id="Seg_5947" s="T634">hüge-nAn</ta>
            <ta e="T636" id="Seg_5948" s="T635">bɨhɨt-A</ta>
            <ta e="T637" id="Seg_5949" s="T636">kert-I-ŋ</ta>
            <ta e="T638" id="Seg_5950" s="T637">bahak-tI-nAn</ta>
            <ta e="T639" id="Seg_5951" s="T638">tes-I-t-A</ta>
            <ta e="T640" id="Seg_5952" s="T639">as-I-ŋ</ta>
            <ta e="T641" id="Seg_5953" s="T640">min</ta>
            <ta e="T642" id="Seg_5954" s="T641">ert-An</ta>
            <ta e="T643" id="Seg_5955" s="T642">is-IAK-m</ta>
            <ta e="T644" id="Seg_5956" s="T643">dʼi͡e-BIT</ta>
            <ta e="T645" id="Seg_5957" s="T644">dogor-LAr-tI-GAr</ta>
            <ta e="T646" id="Seg_5958" s="T645">kör-BIT-LArA</ta>
            <ta e="T647" id="Seg_5959" s="T646">uː-LArA</ta>
            <ta e="T648" id="Seg_5960" s="T647">kirdik</ta>
            <ta e="T649" id="Seg_5961" s="T648">hɨrdaː-An</ta>
            <ta e="T650" id="Seg_5962" s="T649">kɨtar-An</ta>
            <ta e="T651" id="Seg_5963" s="T650">is-BIT</ta>
            <ta e="T652" id="Seg_5964" s="T651">hotoru</ta>
            <ta e="T653" id="Seg_5965" s="T652">tɨː-LArA</ta>
            <ta e="T654" id="Seg_5966" s="T653">boskoloː-A-n-BIT</ta>
            <ta e="T655" id="Seg_5967" s="T654">u͡ol-LAr</ta>
            <ta e="T656" id="Seg_5968" s="T655">ert-I-n-An</ta>
            <ta e="T657" id="Seg_5969" s="T656">is-BIT-LAr</ta>
            <ta e="T658" id="Seg_5970" s="T657">orgujakaːn</ta>
            <ta e="T659" id="Seg_5971" s="T658">töhö</ta>
            <ta e="T660" id="Seg_5972" s="T659">da</ta>
            <ta e="T661" id="Seg_5973" s="T660">ör</ta>
            <ta e="T662" id="Seg_5974" s="T661">bu͡ol-BAkkA</ta>
            <ta e="T663" id="Seg_5975" s="T662">ebe</ta>
            <ta e="T664" id="Seg_5976" s="T663">ürdük</ta>
            <ta e="T665" id="Seg_5977" s="T664">kɨtɨl-tI-GAr</ta>
            <ta e="T666" id="Seg_5978" s="T665">gu͡orat-LArA</ta>
            <ta e="T667" id="Seg_5979" s="T666">köhün-I-BIT</ta>
            <ta e="T668" id="Seg_5980" s="T667">tigis-An</ta>
            <ta e="T669" id="Seg_5981" s="T668">baran</ta>
            <ta e="T670" id="Seg_5982" s="T669">u͡ol-LAr</ta>
            <ta e="T671" id="Seg_5983" s="T670">üs</ta>
            <ta e="T672" id="Seg_5984" s="T671">aŋɨ</ta>
            <ta e="T673" id="Seg_5985" s="T672">aragɨs-An</ta>
            <ta e="T674" id="Seg_5986" s="T673">kaːl-BIT-LAr</ta>
            <ta e="T675" id="Seg_5987" s="T674">ɨraːktaːgɨ</ta>
            <ta e="T676" id="Seg_5988" s="T675">u͡ol-tA</ta>
            <ta e="T677" id="Seg_5989" s="T676">hɨːr</ta>
            <ta e="T678" id="Seg_5990" s="T677">ürüt-tI-GAr</ta>
            <ta e="T679" id="Seg_5991" s="T678">čeːlkeː</ta>
            <ta e="T680" id="Seg_5992" s="T679">dʼi͡e-GA</ta>
            <ta e="T681" id="Seg_5993" s="T680">bar-BIT</ta>
            <ta e="T682" id="Seg_5994" s="T681">ɨt</ta>
            <ta e="T683" id="Seg_5995" s="T682">u͡ol-tA</ta>
            <ta e="T684" id="Seg_5996" s="T683">maːma-tI-n</ta>
            <ta e="T685" id="Seg_5997" s="T684">nʼamčɨgas</ta>
            <ta e="T686" id="Seg_5998" s="T685">koroːn</ta>
            <ta e="T687" id="Seg_5999" s="T686">dʼi͡e-tI-GAr</ta>
            <ta e="T688" id="Seg_6000" s="T687">kiːr-BIT</ta>
            <ta e="T689" id="Seg_6001" s="T688">koru͡oba</ta>
            <ta e="T690" id="Seg_6002" s="T689">u͡ol-tA</ta>
            <ta e="T691" id="Seg_6003" s="T690">tahaːra</ta>
            <ta e="T692" id="Seg_6004" s="T691">inʼe-tI-n</ta>
            <ta e="T693" id="Seg_6005" s="T692">körüs-I-BIT</ta>
            <ta e="T694" id="Seg_6006" s="T693">harsi͡erda</ta>
            <ta e="T695" id="Seg_6007" s="T694">ɨraːktaːgɨ</ta>
            <ta e="T696" id="Seg_6008" s="T695">u͡ol-LAr-nI</ta>
            <ta e="T697" id="Seg_6009" s="T696">ɨgɨr-t-ItAlAː-BIT</ta>
            <ta e="T698" id="Seg_6010" s="T697">dʼe</ta>
            <ta e="T699" id="Seg_6011" s="T698">ogo-LAr-I-m</ta>
            <ta e="T700" id="Seg_6012" s="T699">kepseː-ŋ</ta>
            <ta e="T701" id="Seg_6013" s="T700">tu͡ok-nI</ta>
            <ta e="T702" id="Seg_6014" s="T701">gɨn-A</ta>
            <ta e="T703" id="Seg_6015" s="T702">bu</ta>
            <ta e="T704" id="Seg_6016" s="T703">turkarɨ</ta>
            <ta e="T705" id="Seg_6017" s="T704">hɨrɨt-I-BIT-GIt=Ij</ta>
            <ta e="T706" id="Seg_6018" s="T705">üs</ta>
            <ta e="T707" id="Seg_6019" s="T706">abaːhɨ</ta>
            <ta e="T708" id="Seg_6020" s="T707">irge-tI-ttAn</ta>
            <ta e="T709" id="Seg_6021" s="T708">üs</ta>
            <ta e="T710" id="Seg_6022" s="T709">huruk-LAːK</ta>
            <ta e="T711" id="Seg_6023" s="T710">taːs-nI</ta>
            <ta e="T712" id="Seg_6024" s="T711">bul-BIT-I-m</ta>
            <ta e="T713" id="Seg_6025" s="T712">ɨt</ta>
            <ta e="T714" id="Seg_6026" s="T713">u͡ol-tA</ta>
            <ta e="T715" id="Seg_6027" s="T714">dʼi͡e-BIT</ta>
            <ta e="T716" id="Seg_6028" s="T715">ɨraːktaːgɨ</ta>
            <ta e="T717" id="Seg_6029" s="T716">iliː-tI-GAr</ta>
            <ta e="T718" id="Seg_6030" s="T717">üs</ta>
            <ta e="T719" id="Seg_6031" s="T718">taːs-nI</ta>
            <ta e="T720" id="Seg_6032" s="T719">tuttar-An</ta>
            <ta e="T721" id="Seg_6033" s="T720">keːs-BIT</ta>
            <ta e="T722" id="Seg_6034" s="T721">ɨraːktaːgɨ</ta>
            <ta e="T723" id="Seg_6035" s="T722">ɨt</ta>
            <ta e="T724" id="Seg_6036" s="T723">u͡ol-tI-n</ta>
            <ta e="T725" id="Seg_6037" s="T724">moːj-tI-ttAn</ta>
            <ta e="T726" id="Seg_6038" s="T725">kuːs-An</ta>
            <ta e="T727" id="Seg_6039" s="T726">ɨl-BIT</ta>
            <ta e="T728" id="Seg_6040" s="T727">ɨtaː-A-msIj-A</ta>
            <ta e="T729" id="Seg_6041" s="T728">tüs-An</ta>
            <ta e="T730" id="Seg_6042" s="T729">baran</ta>
            <ta e="T731" id="Seg_6043" s="T730">dʼi͡e-BIT</ta>
            <ta e="T732" id="Seg_6044" s="T731">bügüŋŋü-ttAn</ta>
            <ta e="T733" id="Seg_6045" s="T732">en</ta>
            <ta e="T734" id="Seg_6046" s="T733">ɨraːktaːgɨ</ta>
            <ta e="T735" id="Seg_6047" s="T734">bu͡ol-Ar</ta>
            <ta e="T736" id="Seg_6048" s="T735">ɨjaːk-I-ŋ</ta>
            <ta e="T737" id="Seg_6049" s="T736">kel-TI-tA</ta>
            <ta e="T738" id="Seg_6050" s="T737">u͡ol-BI-n</ta>
            <ta e="T739" id="Seg_6051" s="T738">kɨtta</ta>
            <ta e="T740" id="Seg_6052" s="T739">ubaj-balɨs</ta>
            <ta e="T741" id="Seg_6053" s="T740">bu͡ol-An</ta>
            <ta e="T742" id="Seg_6054" s="T741">olor-I-ŋ</ta>
            <ta e="T743" id="Seg_6055" s="T742">üs</ta>
            <ta e="T744" id="Seg_6056" s="T743">u͡ol</ta>
            <ta e="T745" id="Seg_6057" s="T744">dogor-LIː</ta>
            <ta e="T746" id="Seg_6058" s="T745">baːj-An-baːj-An</ta>
            <ta e="T747" id="Seg_6059" s="T746">olor-BIT-LAr</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_6060" s="T0">long.ago-long.ago</ta>
            <ta e="T2" id="Seg_6061" s="T1">one</ta>
            <ta e="T3" id="Seg_6062" s="T2">czar.[NOM]</ta>
            <ta e="T4" id="Seg_6063" s="T3">live-PST2.[3SG]</ta>
            <ta e="T5" id="Seg_6064" s="T4">child-POSS</ta>
            <ta e="T6" id="Seg_6065" s="T5">NEG</ta>
            <ta e="T7" id="Seg_6066" s="T6">be-PST1-3SG</ta>
            <ta e="T8" id="Seg_6067" s="T7">be-PST2.[3SG]</ta>
            <ta e="T9" id="Seg_6068" s="T8">once</ta>
            <ta e="T10" id="Seg_6069" s="T9">this</ta>
            <ta e="T11" id="Seg_6070" s="T10">czar.[NOM]</ta>
            <ta e="T12" id="Seg_6071" s="T11">think-PST2.[3SG]</ta>
            <ta e="T13" id="Seg_6072" s="T12">this</ta>
            <ta e="T14" id="Seg_6073" s="T13">why</ta>
            <ta e="T15" id="Seg_6074" s="T14">1PL.[NOM]</ta>
            <ta e="T16" id="Seg_6075" s="T15">child-POSS</ta>
            <ta e="T17" id="Seg_6076" s="T16">NEG-1PL=Q</ta>
            <ta e="T18" id="Seg_6077" s="T17">old-PL-ABL</ta>
            <ta e="T19" id="Seg_6078" s="T18">ask-PTCP.PST.[NOM]</ta>
            <ta e="T20" id="Seg_6079" s="T19">MOD</ta>
            <ta e="T21" id="Seg_6080" s="T20">this</ta>
            <ta e="T22" id="Seg_6081" s="T21">think-CVB.SIM</ta>
            <ta e="T23" id="Seg_6082" s="T22">sit-TEMP-3SG</ta>
            <ta e="T24" id="Seg_6083" s="T23">woman-3SG.[NOM]</ta>
            <ta e="T25" id="Seg_6084" s="T24">go.in-PST2.[3SG]</ta>
            <ta e="T26" id="Seg_6085" s="T25">what-ACC</ta>
            <ta e="T27" id="Seg_6086" s="T26">think.about-PST1-2SG</ta>
            <ta e="T28" id="Seg_6087" s="T27">friend</ta>
            <ta e="T29" id="Seg_6088" s="T28">tomorrow</ta>
            <ta e="T30" id="Seg_6089" s="T29">1PL.[NOM]</ta>
            <ta e="T31" id="Seg_6090" s="T30">human.being-PL-ACC</ta>
            <ta e="T32" id="Seg_6091" s="T31">gather-FUT-1PL</ta>
            <ta e="T33" id="Seg_6092" s="T32">what.kind.of-DAT/LOC</ta>
            <ta e="T34" id="Seg_6093" s="T33">until</ta>
            <ta e="T35" id="Seg_6094" s="T34">empty</ta>
            <ta e="T36" id="Seg_6095" s="T35">wall-ACC</ta>
            <ta e="T37" id="Seg_6096" s="T36">see-CVB.SEQ</ta>
            <ta e="T38" id="Seg_6097" s="T37">live-FUT-1PL=Q</ta>
            <ta e="T39" id="Seg_6098" s="T38">child-POSS</ta>
            <ta e="T40" id="Seg_6099" s="T39">NEG</ta>
            <ta e="T41" id="Seg_6100" s="T40">ask-IMP.1DU</ta>
            <ta e="T42" id="Seg_6101" s="T41">why</ta>
            <ta e="T43" id="Seg_6102" s="T42">child-POSS</ta>
            <ta e="T44" id="Seg_6103" s="T43">NEG-1PL=Q</ta>
            <ta e="T45" id="Seg_6104" s="T44">who-DAT/LOC</ta>
            <ta e="T46" id="Seg_6105" s="T45">wealth-1PL-ACC</ta>
            <ta e="T47" id="Seg_6106" s="T46">give-FUT-1PL=Q</ta>
            <ta e="T48" id="Seg_6107" s="T47">die-TEMP-1PL</ta>
            <ta e="T49" id="Seg_6108" s="T48">in.the.morning</ta>
            <ta e="T50" id="Seg_6109" s="T49">stand.up-PST2-3PL</ta>
            <ta e="T51" id="Seg_6110" s="T50">many</ta>
            <ta e="T52" id="Seg_6111" s="T51">very</ta>
            <ta e="T53" id="Seg_6112" s="T52">human.being.[NOM]</ta>
            <ta e="T54" id="Seg_6113" s="T53">gather-PASS/REFL-EP-PST2.[3SG]</ta>
            <ta e="T55" id="Seg_6114" s="T54">there</ta>
            <ta e="T56" id="Seg_6115" s="T55">czar.[NOM]</ta>
            <ta e="T57" id="Seg_6116" s="T56">ask-PST2.[3SG]</ta>
            <ta e="T58" id="Seg_6117" s="T57">this</ta>
            <ta e="T59" id="Seg_6118" s="T58">why</ta>
            <ta e="T60" id="Seg_6119" s="T59">1PL.[NOM]</ta>
            <ta e="T61" id="Seg_6120" s="T60">child-POSS</ta>
            <ta e="T62" id="Seg_6121" s="T61">NEG-1PL=Q</ta>
            <ta e="T63" id="Seg_6122" s="T62">how</ta>
            <ta e="T64" id="Seg_6123" s="T63">what-ACC</ta>
            <ta e="T65" id="Seg_6124" s="T64">make-PTCP.COND-DAT/LOC</ta>
            <ta e="T66" id="Seg_6125" s="T65">child-VBZ-PTCP.FUT-1PL=Q</ta>
            <ta e="T67" id="Seg_6126" s="T66">2PL.[NOM]</ta>
            <ta e="T68" id="Seg_6127" s="T67">how</ta>
            <ta e="T69" id="Seg_6128" s="T68">think-CVB.SIM</ta>
            <ta e="T70" id="Seg_6129" s="T69">think-PRS-2PL</ta>
            <ta e="T71" id="Seg_6130" s="T70">human.being-PL.[NOM]</ta>
            <ta e="T72" id="Seg_6131" s="T71">make.noise-CVB.SIM</ta>
            <ta e="T73" id="Seg_6132" s="T72">fall-PST2-3PL</ta>
            <ta e="T74" id="Seg_6133" s="T73">door-3SG-INSTR</ta>
            <ta e="T75" id="Seg_6134" s="T74">old</ta>
            <ta e="T76" id="Seg_6135" s="T75">buckled</ta>
            <ta e="T77" id="Seg_6136" s="T76">old.man.[NOM]</ta>
            <ta e="T78" id="Seg_6137" s="T77">lean.out-NMNZ.[NOM]</ta>
            <ta e="T79" id="Seg_6138" s="T78">make-PST2.[3SG]</ta>
            <ta e="T80" id="Seg_6139" s="T79">buckled</ta>
            <ta e="T81" id="Seg_6140" s="T80">pain-PROPR-ACC</ta>
            <ta e="T82" id="Seg_6141" s="T81">middle-DAT/LOC</ta>
            <ta e="T83" id="Seg_6142" s="T82">push-CVB.SEQ</ta>
            <ta e="T84" id="Seg_6143" s="T83">throw-PST2-3PL</ta>
            <ta e="T85" id="Seg_6144" s="T84">this</ta>
            <ta e="T86" id="Seg_6145" s="T85">there.is</ta>
            <ta e="T87" id="Seg_6146" s="T86">know-PTCP.PRS</ta>
            <ta e="T88" id="Seg_6147" s="T87">human.being.[NOM]</ta>
            <ta e="T89" id="Seg_6148" s="T88">old.man.[NOM]</ta>
            <ta e="T90" id="Seg_6149" s="T89">up</ta>
            <ta e="T91" id="Seg_6150" s="T90">see-CVB.SEQ</ta>
            <ta e="T92" id="Seg_6151" s="T91">after</ta>
            <ta e="T93" id="Seg_6152" s="T92">back.[NOM]</ta>
            <ta e="T94" id="Seg_6153" s="T93">catch-CVB.SEQ</ta>
            <ta e="T95" id="Seg_6154" s="T94">after</ta>
            <ta e="T96" id="Seg_6155" s="T95">say-PST2.[3SG]</ta>
            <ta e="T97" id="Seg_6156" s="T96">make-EP-IMP.2PL</ta>
            <ta e="T98" id="Seg_6157" s="T97">gold.[NOM]</ta>
            <ta e="T99" id="Seg_6158" s="T98">net-PART</ta>
            <ta e="T100" id="Seg_6159" s="T99">gold.[NOM]</ta>
            <ta e="T101" id="Seg_6160" s="T100">small.boat-PART</ta>
            <ta e="T102" id="Seg_6161" s="T101">gold.[NOM]</ta>
            <ta e="T103" id="Seg_6162" s="T102">oar-PART</ta>
            <ta e="T104" id="Seg_6163" s="T103">then</ta>
            <ta e="T105" id="Seg_6164" s="T104">czar.[NOM]</ta>
            <ta e="T106" id="Seg_6165" s="T105">fish-CVB.SIM</ta>
            <ta e="T107" id="Seg_6166" s="T106">go-IMP.3SG</ta>
            <ta e="T108" id="Seg_6167" s="T107">net-3SG-DAT/LOC</ta>
            <ta e="T109" id="Seg_6168" s="T108">fish.[NOM]</ta>
            <ta e="T110" id="Seg_6169" s="T109">hold-FUT.[3SG]</ta>
            <ta e="T111" id="Seg_6170" s="T110">that-ACC</ta>
            <ta e="T112" id="Seg_6171" s="T111">woman-3SG.[NOM]</ta>
            <ta e="T113" id="Seg_6172" s="T112">eat-IMP.3SG</ta>
            <ta e="T114" id="Seg_6173" s="T113">czar.[NOM]</ta>
            <ta e="T115" id="Seg_6174" s="T114">woman-3SG.[NOM]</ta>
            <ta e="T116" id="Seg_6175" s="T115">laugh-PST2.[3SG]</ta>
            <ta e="T117" id="Seg_6176" s="T116">that-ABL</ta>
            <ta e="T118" id="Seg_6177" s="T117">1SG.[NOM]</ta>
            <ta e="T119" id="Seg_6178" s="T118">eat.ones.fill-FUT-1SG</ta>
            <ta e="T120" id="Seg_6179" s="T119">Q</ta>
            <ta e="T121" id="Seg_6180" s="T120">long</ta>
            <ta e="T122" id="Seg_6181" s="T121">be-FUT.[3SG]=Q</ta>
            <ta e="T123" id="Seg_6182" s="T122">czar.[NOM]</ta>
            <ta e="T124" id="Seg_6183" s="T123">next.morning-3SG-ACC</ta>
            <ta e="T125" id="Seg_6184" s="T124">gold.[NOM]</ta>
            <ta e="T126" id="Seg_6185" s="T125">small.boat-PROPR</ta>
            <ta e="T127" id="Seg_6186" s="T126">gold.[NOM]</ta>
            <ta e="T128" id="Seg_6187" s="T127">oar-PROPR</ta>
            <ta e="T129" id="Seg_6188" s="T128">fish-VBZ-CVB.SIM</ta>
            <ta e="T130" id="Seg_6189" s="T129">go-PST2.[3SG]</ta>
            <ta e="T131" id="Seg_6190" s="T130">one</ta>
            <ta e="T132" id="Seg_6191" s="T131">fish-ACC</ta>
            <ta e="T133" id="Seg_6192" s="T132">tremble-CVB.SEQ</ta>
            <ta e="T134" id="Seg_6193" s="T133">alive-SIM</ta>
            <ta e="T135" id="Seg_6194" s="T134">bring-NEG.[3SG]</ta>
            <ta e="T136" id="Seg_6195" s="T135">MOD</ta>
            <ta e="T137" id="Seg_6196" s="T136">this.[NOM]</ta>
            <ta e="T138" id="Seg_6197" s="T137">haul-EP-1SG.[NOM]</ta>
            <ta e="T139" id="Seg_6198" s="T138">say-PST2.[3SG]</ta>
            <ta e="T140" id="Seg_6199" s="T139">and</ta>
            <ta e="T141" id="Seg_6200" s="T140">cook-3PL-DAT/LOC</ta>
            <ta e="T142" id="Seg_6201" s="T141">give-CVB.SEQ</ta>
            <ta e="T143" id="Seg_6202" s="T142">throw-PST2.[3SG]</ta>
            <ta e="T144" id="Seg_6203" s="T143">at.once</ta>
            <ta e="T145" id="Seg_6204" s="T144">cook-IMP.2PL</ta>
            <ta e="T146" id="Seg_6205" s="T145">cook.[NOM]</ta>
            <ta e="T147" id="Seg_6206" s="T146">fish.[NOM]</ta>
            <ta e="T148" id="Seg_6207" s="T147">scale-3SG-ACC</ta>
            <ta e="T149" id="Seg_6208" s="T148">grass.[NOM]</ta>
            <ta e="T150" id="Seg_6209" s="T149">inside-3SG-DAT/LOC</ta>
            <ta e="T151" id="Seg_6210" s="T150">clean-CVB.SEQ</ta>
            <ta e="T152" id="Seg_6211" s="T151">throw-PST2.[3SG]</ta>
            <ta e="T153" id="Seg_6212" s="T152">that-ACC</ta>
            <ta e="T154" id="Seg_6213" s="T153">czar.[NOM]</ta>
            <ta e="T155" id="Seg_6214" s="T154">cow-3SG.[NOM]</ta>
            <ta e="T156" id="Seg_6215" s="T155">eat-CVB.SEQ</ta>
            <ta e="T157" id="Seg_6216" s="T156">throw-PST2.[3SG]</ta>
            <ta e="T158" id="Seg_6217" s="T157">czar.[NOM]</ta>
            <ta e="T159" id="Seg_6218" s="T158">woman-3SG.[NOM]</ta>
            <ta e="T160" id="Seg_6219" s="T159">fish-ACC</ta>
            <ta e="T161" id="Seg_6220" s="T160">eat-CVB.SEQ</ta>
            <ta e="T162" id="Seg_6221" s="T161">after</ta>
            <ta e="T163" id="Seg_6222" s="T162">bone-3SG-ACC</ta>
            <ta e="T164" id="Seg_6223" s="T163">plate-DAT/LOC</ta>
            <ta e="T165" id="Seg_6224" s="T164">stay-CAUS-PTCP.PST-3SG-ACC</ta>
            <ta e="T166" id="Seg_6225" s="T165">cook.[NOM]</ta>
            <ta e="T167" id="Seg_6226" s="T166">dog-DAT/LOC</ta>
            <ta e="T168" id="Seg_6227" s="T167">throw-PST2.[3SG]</ta>
            <ta e="T169" id="Seg_6228" s="T168">tale-DAT/LOC</ta>
            <ta e="T170" id="Seg_6229" s="T169">long</ta>
            <ta e="T171" id="Seg_6230" s="T170">become-FUT.[3SG]</ta>
            <ta e="T172" id="Seg_6231" s="T171">Q</ta>
            <ta e="T173" id="Seg_6232" s="T172">three-COLL-3PL.[NOM]</ta>
            <ta e="T174" id="Seg_6233" s="T173">three</ta>
            <ta e="T175" id="Seg_6234" s="T174">boy-ACC</ta>
            <ta e="T176" id="Seg_6235" s="T175">give.birth-PST2-3PL</ta>
            <ta e="T177" id="Seg_6236" s="T176">child-PL.[NOM]</ta>
            <ta e="T178" id="Seg_6237" s="T177">how.much</ta>
            <ta e="T179" id="Seg_6238" s="T178">NEG</ta>
            <ta e="T180" id="Seg_6239" s="T179">be-NEG.CVB.SIM</ta>
            <ta e="T181" id="Seg_6240" s="T180">together</ta>
            <ta e="T182" id="Seg_6241" s="T181">play-PTCP.PRS</ta>
            <ta e="T183" id="Seg_6242" s="T182">become-PST2-3PL</ta>
            <ta e="T184" id="Seg_6243" s="T183">once</ta>
            <ta e="T185" id="Seg_6244" s="T184">only</ta>
            <ta e="T186" id="Seg_6245" s="T185">czar.[NOM]</ta>
            <ta e="T187" id="Seg_6246" s="T186">son-3SG.[NOM]</ta>
            <ta e="T188" id="Seg_6247" s="T187">say-PST2.[3SG]</ta>
            <ta e="T189" id="Seg_6248" s="T188">friend-PL.[NOM]</ta>
            <ta e="T190" id="Seg_6249" s="T189">small.boat-VBZ-CVB.SIM</ta>
            <ta e="T191" id="Seg_6250" s="T190">go-IMP.1PL</ta>
            <ta e="T192" id="Seg_6251" s="T191">boy-PL.[NOM]</ta>
            <ta e="T193" id="Seg_6252" s="T192">how.much</ta>
            <ta e="T194" id="Seg_6253" s="T193">long</ta>
            <ta e="T195" id="Seg_6254" s="T194">row-CVB.SEQ</ta>
            <ta e="T196" id="Seg_6255" s="T195">go-PST2-3PL</ta>
            <ta e="T197" id="Seg_6256" s="T196">MOD</ta>
            <ta e="T198" id="Seg_6257" s="T197">see-PST2-3PL</ta>
            <ta e="T199" id="Seg_6258" s="T198">river.[NOM]</ta>
            <ta e="T200" id="Seg_6259" s="T199">on.the.other.shore</ta>
            <ta e="T201" id="Seg_6260" s="T200">big</ta>
            <ta e="T202" id="Seg_6261" s="T201">very</ta>
            <ta e="T203" id="Seg_6262" s="T202">yurt.[NOM]</ta>
            <ta e="T204" id="Seg_6263" s="T203">house.[NOM]</ta>
            <ta e="T205" id="Seg_6264" s="T204">stand-PRS.[3SG]</ta>
            <ta e="T206" id="Seg_6265" s="T205">that.[NOM]</ta>
            <ta e="T207" id="Seg_6266" s="T206">what-1PL.[NOM]=Q</ta>
            <ta e="T208" id="Seg_6267" s="T207">say-EP-RECP/COLL-PST2-3PL</ta>
            <ta e="T209" id="Seg_6268" s="T208">czar.[NOM]</ta>
            <ta e="T210" id="Seg_6269" s="T209">son-3SG.[NOM]</ta>
            <ta e="T211" id="Seg_6270" s="T210">come.close-IMP.1PL</ta>
            <ta e="T212" id="Seg_6271" s="T211">cards-VBZ-FUT-1PL</ta>
            <ta e="T213" id="Seg_6272" s="T212">say-PST2.[3SG]</ta>
            <ta e="T214" id="Seg_6273" s="T213">dog.[NOM]</ta>
            <ta e="T215" id="Seg_6274" s="T214">son-3SG.[NOM]</ta>
            <ta e="T216" id="Seg_6275" s="T215">hey</ta>
            <ta e="T217" id="Seg_6276" s="T216">why</ta>
            <ta e="T218" id="Seg_6277" s="T217">maybe</ta>
            <ta e="T219" id="Seg_6278" s="T218">evil.spirit-PL.[NOM]</ta>
            <ta e="T220" id="Seg_6279" s="T219">house-3PL.[NOM]</ta>
            <ta e="T221" id="Seg_6280" s="T220">be-FUT.[3SG]</ta>
            <ta e="T222" id="Seg_6281" s="T221">cow.[NOM]</ta>
            <ta e="T223" id="Seg_6282" s="T222">son-3SG.[NOM]</ta>
            <ta e="T224" id="Seg_6283" s="T223">come.close-IMP.1PL</ta>
            <ta e="T225" id="Seg_6284" s="T224">come.close-IMP.1PL</ta>
            <ta e="T226" id="Seg_6285" s="T225">walk-FREQ-CVB.SIM</ta>
            <ta e="T227" id="Seg_6286" s="T226">fall-IMP.1PL</ta>
            <ta e="T228" id="Seg_6287" s="T227">shore-DAT/LOC</ta>
            <ta e="T229" id="Seg_6288" s="T228">go.out-CVB.ANT</ta>
            <ta e="T230" id="Seg_6289" s="T229">this</ta>
            <ta e="T231" id="Seg_6290" s="T230">two</ta>
            <ta e="T232" id="Seg_6291" s="T231">boy.[NOM]</ta>
            <ta e="T233" id="Seg_6292" s="T232">cards-VBZ-CVB.SIM</ta>
            <ta e="T234" id="Seg_6293" s="T233">sit.down-PST2-3PL</ta>
            <ta e="T235" id="Seg_6294" s="T234">dog.[NOM]</ta>
            <ta e="T236" id="Seg_6295" s="T235">son-3SG.[NOM]</ta>
            <ta e="T237" id="Seg_6296" s="T236">sleep-CVB.SEQ</ta>
            <ta e="T238" id="Seg_6297" s="T237">stay-PST2.[3SG]</ta>
            <ta e="T239" id="Seg_6298" s="T238">how.much</ta>
            <ta e="T240" id="Seg_6299" s="T239">long</ta>
            <ta e="T241" id="Seg_6300" s="T240">play-PST2-3PL</ta>
            <ta e="T242" id="Seg_6301" s="T241">MOD</ta>
            <ta e="T243" id="Seg_6302" s="T242">this</ta>
            <ta e="T244" id="Seg_6303" s="T243">child-PL.[NOM]</ta>
            <ta e="T245" id="Seg_6304" s="T244">also</ta>
            <ta e="T246" id="Seg_6305" s="T245">sleep-CVB.SEQ</ta>
            <ta e="T247" id="Seg_6306" s="T246">stay-PST2-3PL</ta>
            <ta e="T248" id="Seg_6307" s="T247">dog.[NOM]</ta>
            <ta e="T249" id="Seg_6308" s="T248">son-3SG.[NOM]</ta>
            <ta e="T250" id="Seg_6309" s="T249">night.[NOM]</ta>
            <ta e="T251" id="Seg_6310" s="T250">middle.[NOM]</ta>
            <ta e="T252" id="Seg_6311" s="T251">wake.up-EP-PST2-3SG</ta>
            <ta e="T253" id="Seg_6312" s="T252">human.being.[NOM]</ta>
            <ta e="T254" id="Seg_6313" s="T253">stride-PTCP.PRS</ta>
            <ta e="T255" id="Seg_6314" s="T254">sound-3SG.[NOM]</ta>
            <ta e="T256" id="Seg_6315" s="T255">be.heard-EP-PST2.[3SG]</ta>
            <ta e="T257" id="Seg_6316" s="T256">appear-EP-PST2-3SG</ta>
            <ta e="T258" id="Seg_6317" s="T257">evil.spirit.[NOM]</ta>
            <ta e="T259" id="Seg_6318" s="T258">yurt.[NOM]</ta>
            <ta e="T260" id="Seg_6319" s="T259">to</ta>
            <ta e="T261" id="Seg_6320" s="T260">walk-CVB.SEQ</ta>
            <ta e="T262" id="Seg_6321" s="T261">go-EMOT-PRS.[3SG]</ta>
            <ta e="T263" id="Seg_6322" s="T262">well</ta>
            <ta e="T264" id="Seg_6323" s="T263">oh</ta>
            <ta e="T265" id="Seg_6324" s="T264">well</ta>
            <ta e="T266" id="Seg_6325" s="T265">die-PST2-1PL</ta>
            <ta e="T267" id="Seg_6326" s="T266">be-PST2.[3SG]</ta>
            <ta e="T268" id="Seg_6327" s="T267">say-CVB.SEQ</ta>
            <ta e="T269" id="Seg_6328" s="T268">after</ta>
            <ta e="T270" id="Seg_6329" s="T269">friend-PL-3SG-ACC</ta>
            <ta e="T271" id="Seg_6330" s="T270">wake.up-CAUS-CVB.SIM</ta>
            <ta e="T272" id="Seg_6331" s="T271">do.in.vain-PST2.[3SG]</ta>
            <ta e="T273" id="Seg_6332" s="T272">this</ta>
            <ta e="T274" id="Seg_6333" s="T273">boy-PL.[NOM]</ta>
            <ta e="T275" id="Seg_6334" s="T274">then</ta>
            <ta e="T276" id="Seg_6335" s="T275">wake.up-FUT-3PL=Q</ta>
            <ta e="T277" id="Seg_6336" s="T276">come.[IMP.2SG]-come.[IMP.2SG]</ta>
            <ta e="T278" id="Seg_6337" s="T277">1SG.[NOM]</ta>
            <ta e="T279" id="Seg_6338" s="T278">2SG-ACC</ta>
            <ta e="T280" id="Seg_6339" s="T279">eat-FUT-1SG</ta>
            <ta e="T281" id="Seg_6340" s="T280">evil.spirit.[NOM]</ta>
            <ta e="T282" id="Seg_6341" s="T281">voice-3SG.[NOM]</ta>
            <ta e="T283" id="Seg_6342" s="T282">be.heard-EP-PST2.[3SG]</ta>
            <ta e="T284" id="Seg_6343" s="T283">this</ta>
            <ta e="T285" id="Seg_6344" s="T284">boy.[NOM]</ta>
            <ta e="T286" id="Seg_6345" s="T285">go.out-CVB.SIM</ta>
            <ta e="T287" id="Seg_6346" s="T286">rise-PST2.[3SG]</ta>
            <ta e="T288" id="Seg_6347" s="T287">and</ta>
            <ta e="T289" id="Seg_6348" s="T288">evil.spirit-ACC</ta>
            <ta e="T290" id="Seg_6349" s="T289">with</ta>
            <ta e="T291" id="Seg_6350" s="T290">fight-EP-PST2.[3SG]</ta>
            <ta e="T292" id="Seg_6351" s="T291">how.much</ta>
            <ta e="T293" id="Seg_6352" s="T292">NEG</ta>
            <ta e="T294" id="Seg_6353" s="T293">be-NEG.CVB.SIM</ta>
            <ta e="T295" id="Seg_6354" s="T294">evil.spirit.[NOM]</ta>
            <ta e="T296" id="Seg_6355" s="T295">head-3SG-ACC</ta>
            <ta e="T297" id="Seg_6356" s="T296">big.stone.[NOM]</ta>
            <ta e="T298" id="Seg_6357" s="T297">stone-DAT/LOC</ta>
            <ta e="T299" id="Seg_6358" s="T298">split-CVB.SIM</ta>
            <ta e="T300" id="Seg_6359" s="T299">beat-EP-PST2.[3SG]</ta>
            <ta e="T301" id="Seg_6360" s="T300">that</ta>
            <ta e="T302" id="Seg_6361" s="T301">make-CVB.SEQ</ta>
            <ta e="T303" id="Seg_6362" s="T302">after</ta>
            <ta e="T304" id="Seg_6363" s="T303">skull-3SG-GEN</ta>
            <ta e="T305" id="Seg_6364" s="T304">inside-3SG-ABL</ta>
            <ta e="T306" id="Seg_6365" s="T305">writing-PROPR</ta>
            <ta e="T307" id="Seg_6366" s="T306">stone-ACC</ta>
            <ta e="T308" id="Seg_6367" s="T307">get.out-PST2.[3SG]</ta>
            <ta e="T309" id="Seg_6368" s="T308">stone-ACC</ta>
            <ta e="T310" id="Seg_6369" s="T309">pocket-3SG-GEN</ta>
            <ta e="T311" id="Seg_6370" s="T310">inside-3SG-INSTR</ta>
            <ta e="T312" id="Seg_6371" s="T311">put.in-CVB.SEQ</ta>
            <ta e="T313" id="Seg_6372" s="T312">throw-PST2.[3SG]</ta>
            <ta e="T314" id="Seg_6373" s="T313">in.the.morning</ta>
            <ta e="T315" id="Seg_6374" s="T314">what.[NOM]</ta>
            <ta e="T316" id="Seg_6375" s="T315">NEG</ta>
            <ta e="T317" id="Seg_6376" s="T316">be-NEG.PTCP.PST.[NOM]</ta>
            <ta e="T318" id="Seg_6377" s="T317">similar</ta>
            <ta e="T319" id="Seg_6378" s="T318">dog.[NOM]</ta>
            <ta e="T320" id="Seg_6379" s="T319">son-3SG.[NOM]</ta>
            <ta e="T321" id="Seg_6380" s="T320">friend-PL-3SG-ACC</ta>
            <ta e="T322" id="Seg_6381" s="T321">wake.up-CAUS-ITER-PST2.[3SG]</ta>
            <ta e="T323" id="Seg_6382" s="T322">hey</ta>
            <ta e="T324" id="Seg_6383" s="T323">goose-VBZ-CVB.SIM</ta>
            <ta e="T325" id="Seg_6384" s="T324">go-IMP.1PL</ta>
            <ta e="T326" id="Seg_6385" s="T325">boy-PL.[NOM]</ta>
            <ta e="T327" id="Seg_6386" s="T326">stretch.oneself-CVB.SEQ</ta>
            <ta e="T328" id="Seg_6387" s="T327">hardly</ta>
            <ta e="T329" id="Seg_6388" s="T328">stand-CVB.SEQ</ta>
            <ta e="T330" id="Seg_6389" s="T329">tea.[NOM]</ta>
            <ta e="T331" id="Seg_6390" s="T330">drink-PST2-3PL</ta>
            <ta e="T332" id="Seg_6391" s="T331">tale-DAT/LOC</ta>
            <ta e="T333" id="Seg_6392" s="T332">day.[NOM]</ta>
            <ta e="T334" id="Seg_6393" s="T333">short.[NOM]</ta>
            <ta e="T335" id="Seg_6394" s="T334">how.much</ta>
            <ta e="T336" id="Seg_6395" s="T335">NEG</ta>
            <ta e="T337" id="Seg_6396" s="T336">be-NEG.CVB.SIM</ta>
            <ta e="T338" id="Seg_6397" s="T337">sky.[NOM]</ta>
            <ta e="T339" id="Seg_6398" s="T338">get.dark-PST2.[3SG]</ta>
            <ta e="T340" id="Seg_6399" s="T339">dog.[NOM]</ta>
            <ta e="T341" id="Seg_6400" s="T340">son-3SG.[NOM]</ta>
            <ta e="T342" id="Seg_6401" s="T341">eat-CVB.SEQ-eat-CVB.SEQ</ta>
            <ta e="T343" id="Seg_6402" s="T342">after</ta>
            <ta e="T344" id="Seg_6403" s="T343">sleep-CVB.SEQ</ta>
            <ta e="T345" id="Seg_6404" s="T344">stay-PST2.[3SG]</ta>
            <ta e="T346" id="Seg_6405" s="T345">friend-PL-3SG.[NOM]</ta>
            <ta e="T347" id="Seg_6406" s="T346">again</ta>
            <ta e="T348" id="Seg_6407" s="T347">cards-VBZ-CVB.SIM</ta>
            <ta e="T349" id="Seg_6408" s="T348">sit.down-PST2-3PL</ta>
            <ta e="T350" id="Seg_6409" s="T349">night.[NOM]</ta>
            <ta e="T351" id="Seg_6410" s="T350">middle.[NOM]</ta>
            <ta e="T352" id="Seg_6411" s="T351">wake.up-EP-PST2-3SG</ta>
            <ta e="T353" id="Seg_6412" s="T352">friend-PL-3SG.[NOM]</ta>
            <ta e="T354" id="Seg_6413" s="T353">seat-CVB.SIM</ta>
            <ta e="T355" id="Seg_6414" s="T354">sit.down-PTCP.PST-3SG-INSTR</ta>
            <ta e="T356" id="Seg_6415" s="T355">sleep-CVB.SEQ</ta>
            <ta e="T357" id="Seg_6416" s="T356">stay-PST2-3PL</ta>
            <ta e="T358" id="Seg_6417" s="T357">this</ta>
            <ta e="T359" id="Seg_6418" s="T358">child.[NOM]</ta>
            <ta e="T360" id="Seg_6419" s="T359">wake.up-CVB.SEQ</ta>
            <ta e="T361" id="Seg_6420" s="T360">after</ta>
            <ta e="T362" id="Seg_6421" s="T361">listen-CVB.SIM</ta>
            <ta e="T363" id="Seg_6422" s="T362">lie-PST2.[3SG]</ta>
            <ta e="T364" id="Seg_6423" s="T363">distant.[NOM]</ta>
            <ta e="T365" id="Seg_6424" s="T364">whereto</ta>
            <ta e="T366" id="Seg_6425" s="T365">INDEF</ta>
            <ta e="T367" id="Seg_6426" s="T366">again</ta>
            <ta e="T368" id="Seg_6427" s="T367">stride-PTCP.PRS</ta>
            <ta e="T369" id="Seg_6428" s="T368">sound.[NOM]</ta>
            <ta e="T370" id="Seg_6429" s="T369">be.heard-EP-PST2.[3SG]</ta>
            <ta e="T371" id="Seg_6430" s="T370">friend-PL-3SG-ACC</ta>
            <ta e="T372" id="Seg_6431" s="T371">wake.up-CAUS-PST2-3SG</ta>
            <ta e="T373" id="Seg_6432" s="T372">then</ta>
            <ta e="T374" id="Seg_6433" s="T373">wake.up-FUT-3PL=Q</ta>
            <ta e="T375" id="Seg_6434" s="T374">soon</ta>
            <ta e="T376" id="Seg_6435" s="T375">fall.asleep-PTCP.PST</ta>
            <ta e="T377" id="Seg_6436" s="T376">human.being-PL.[NOM]</ta>
            <ta e="T378" id="Seg_6437" s="T377">dog.[NOM]</ta>
            <ta e="T379" id="Seg_6438" s="T378">son-3SG.[NOM]</ta>
            <ta e="T380" id="Seg_6439" s="T379">go.out-EP-PST2.[3SG]</ta>
            <ta e="T381" id="Seg_6440" s="T380">and</ta>
            <ta e="T382" id="Seg_6441" s="T381">two-ORD</ta>
            <ta e="T383" id="Seg_6442" s="T382">evil.spirit.[NOM]</ta>
            <ta e="T384" id="Seg_6443" s="T383">head-3SG-ACC</ta>
            <ta e="T385" id="Seg_6444" s="T384">also</ta>
            <ta e="T386" id="Seg_6445" s="T385">big.stone.[NOM]</ta>
            <ta e="T387" id="Seg_6446" s="T386">stone-DAT/LOC</ta>
            <ta e="T388" id="Seg_6447" s="T387">apart</ta>
            <ta e="T389" id="Seg_6448" s="T388">beat-EP-PST2.[3SG]</ta>
            <ta e="T390" id="Seg_6449" s="T389">evil.spirit.[NOM]</ta>
            <ta e="T391" id="Seg_6450" s="T390">fidget.with.legs-CVB.SIM</ta>
            <ta e="T392" id="Seg_6451" s="T391">fall-CVB.SEQ</ta>
            <ta e="T393" id="Seg_6452" s="T392">after</ta>
            <ta e="T394" id="Seg_6453" s="T393">stretch.oneself-NMNZ.[NOM]</ta>
            <ta e="T395" id="Seg_6454" s="T394">make-PST2.[3SG]</ta>
            <ta e="T396" id="Seg_6455" s="T395">dog.[NOM]</ta>
            <ta e="T397" id="Seg_6456" s="T396">son-3SG.[NOM]</ta>
            <ta e="T398" id="Seg_6457" s="T397">skull-3SG-ABL</ta>
            <ta e="T399" id="Seg_6458" s="T398">writing-PROPR</ta>
            <ta e="T400" id="Seg_6459" s="T399">stone-ACC</ta>
            <ta e="T401" id="Seg_6460" s="T400">get.out-PST1-3SG</ta>
            <ta e="T402" id="Seg_6461" s="T401">EMPH</ta>
            <ta e="T403" id="Seg_6462" s="T402">pocket-3SG-DAT/LOC</ta>
            <ta e="T404" id="Seg_6463" s="T403">also</ta>
            <ta e="T405" id="Seg_6464" s="T404">put.in-CVB.SEQ</ta>
            <ta e="T406" id="Seg_6465" s="T405">throw-PST2.[3SG]</ta>
            <ta e="T407" id="Seg_6466" s="T406">three-ORD</ta>
            <ta e="T408" id="Seg_6467" s="T407">night-3SG-DAT/LOC</ta>
            <ta e="T409" id="Seg_6468" s="T408">three-ORD</ta>
            <ta e="T410" id="Seg_6469" s="T409">evil.spirit.[NOM]</ta>
            <ta e="T411" id="Seg_6470" s="T410">head-3SG-ACC</ta>
            <ta e="T412" id="Seg_6471" s="T411">split-CVB.SIM</ta>
            <ta e="T413" id="Seg_6472" s="T412">beat-CVB.SEQ</ta>
            <ta e="T414" id="Seg_6473" s="T413">writing-PROPR</ta>
            <ta e="T415" id="Seg_6474" s="T414">stone-ACC</ta>
            <ta e="T416" id="Seg_6475" s="T415">get.out-CVB.SIM</ta>
            <ta e="T417" id="Seg_6476" s="T416">beat-EP-PST2.[3SG]</ta>
            <ta e="T418" id="Seg_6477" s="T417">friend-PL-3SG-ACC</ta>
            <ta e="T419" id="Seg_6478" s="T418">dawn.[NOM]</ta>
            <ta e="T420" id="Seg_6479" s="T419">stand.up-PTCP.PRS-3SG-ACC</ta>
            <ta e="T421" id="Seg_6480" s="T420">with</ta>
            <ta e="T422" id="Seg_6481" s="T421">wake.up-CAUS-PST2.[3SG]</ta>
            <ta e="T423" id="Seg_6482" s="T422">well</ta>
            <ta e="T424" id="Seg_6483" s="T423">house-1PL-DAT/LOC</ta>
            <ta e="T425" id="Seg_6484" s="T424">go-IMP.1PL</ta>
            <ta e="T426" id="Seg_6485" s="T425">say-PST2.[3SG]</ta>
            <ta e="T427" id="Seg_6486" s="T426">this</ta>
            <ta e="T428" id="Seg_6487" s="T427">boy.[NOM]</ta>
            <ta e="T429" id="Seg_6488" s="T428">czar.[NOM]</ta>
            <ta e="T430" id="Seg_6489" s="T429">son-3SG.[NOM]</ta>
            <ta e="T431" id="Seg_6490" s="T430">baulk-PST2.[3SG]</ta>
            <ta e="T432" id="Seg_6491" s="T431">why</ta>
            <ta e="T433" id="Seg_6492" s="T432">well</ta>
            <ta e="T434" id="Seg_6493" s="T433">live-CVB.SIM</ta>
            <ta e="T435" id="Seg_6494" s="T434">fall-IMP.1PL</ta>
            <ta e="T436" id="Seg_6495" s="T435">happily</ta>
            <ta e="T437" id="Seg_6496" s="T436">cow.[NOM]</ta>
            <ta e="T438" id="Seg_6497" s="T437">son-3SG.[NOM]</ta>
            <ta e="T439" id="Seg_6498" s="T438">sit-CVB.SIM</ta>
            <ta e="T440" id="Seg_6499" s="T439">fall-CVB.SEQ</ta>
            <ta e="T441" id="Seg_6500" s="T440">after</ta>
            <ta e="T442" id="Seg_6501" s="T441">say-PST2.[3SG]</ta>
            <ta e="T443" id="Seg_6502" s="T442">family-PL-1PL.[NOM]</ta>
            <ta e="T444" id="Seg_6503" s="T443">wait-FUT-3PL</ta>
            <ta e="T445" id="Seg_6504" s="T444">go-IMP.1PL</ta>
            <ta e="T446" id="Seg_6505" s="T445">EVID</ta>
            <ta e="T447" id="Seg_6506" s="T446">boy-PL.[NOM]</ta>
            <ta e="T448" id="Seg_6507" s="T447">hit.the.road-PST2-3PL</ta>
            <ta e="T449" id="Seg_6508" s="T448">dog.[NOM]</ta>
            <ta e="T450" id="Seg_6509" s="T449">son-3SG.[NOM]</ta>
            <ta e="T451" id="Seg_6510" s="T450">row-CVB.SEQ</ta>
            <ta e="T452" id="Seg_6511" s="T451">go-PST2.[3SG]</ta>
            <ta e="T453" id="Seg_6512" s="T452">how.much</ta>
            <ta e="T454" id="Seg_6513" s="T453">NEG</ta>
            <ta e="T455" id="Seg_6514" s="T454">be-NEG.CVB.SIM</ta>
            <ta e="T456" id="Seg_6515" s="T455">sheath-3SG-ACC</ta>
            <ta e="T457" id="Seg_6516" s="T456">catch-CVB.SEQ</ta>
            <ta e="T458" id="Seg_6517" s="T457">go-CVB.SEQ</ta>
            <ta e="T459" id="Seg_6518" s="T458">friend-PL.[NOM]</ta>
            <ta e="T460" id="Seg_6519" s="T459">knife-1SG-ACC</ta>
            <ta e="T461" id="Seg_6520" s="T460">stay-CAUS-PST2-1SG</ta>
            <ta e="T462" id="Seg_6521" s="T461">search-CVB.SIM</ta>
            <ta e="T463" id="Seg_6522" s="T462">go-FUT-1SG</ta>
            <ta e="T464" id="Seg_6523" s="T463">say-PST2.[3SG]</ta>
            <ta e="T465" id="Seg_6524" s="T464">dog.[NOM]</ta>
            <ta e="T466" id="Seg_6525" s="T465">son-3SG.[NOM]</ta>
            <ta e="T467" id="Seg_6526" s="T466">yurt-DAT/LOC</ta>
            <ta e="T468" id="Seg_6527" s="T467">come-PST2-3SG</ta>
            <ta e="T469" id="Seg_6528" s="T468">woman-PL.[NOM]</ta>
            <ta e="T470" id="Seg_6529" s="T469">cry-PTCP.PRS</ta>
            <ta e="T471" id="Seg_6530" s="T470">voice-3PL.[NOM]</ta>
            <ta e="T472" id="Seg_6531" s="T471">be.heard-EP-PST2.[3SG]</ta>
            <ta e="T473" id="Seg_6532" s="T472">1SG.[NOM]</ta>
            <ta e="T474" id="Seg_6533" s="T473">tasty</ta>
            <ta e="T475" id="Seg_6534" s="T474">food.[NOM]</ta>
            <ta e="T476" id="Seg_6535" s="T475">become-CVB.SEQ</ta>
            <ta e="T477" id="Seg_6536" s="T476">after</ta>
            <ta e="T478" id="Seg_6537" s="T477">big</ta>
            <ta e="T479" id="Seg_6538" s="T478">plate-DAT/LOC</ta>
            <ta e="T480" id="Seg_6539" s="T479">small.boat-3PL-GEN</ta>
            <ta e="T481" id="Seg_6540" s="T480">inside-3SG-DAT/LOC</ta>
            <ta e="T482" id="Seg_6541" s="T481">fall-FUT-1SG</ta>
            <ta e="T483" id="Seg_6542" s="T482">1SG-ACC</ta>
            <ta e="T484" id="Seg_6543" s="T483">eat-TEMP-3PL</ta>
            <ta e="T485" id="Seg_6544" s="T484">die-CVB.SEQ</ta>
            <ta e="T486" id="Seg_6545" s="T485">stay-FUT-3PL</ta>
            <ta e="T487" id="Seg_6546" s="T486">say-PST2.[3SG]</ta>
            <ta e="T488" id="Seg_6547" s="T487">old</ta>
            <ta e="T489" id="Seg_6548" s="T488">Baba.Yaga.[NOM]</ta>
            <ta e="T490" id="Seg_6549" s="T489">voice-3SG.[NOM]</ta>
            <ta e="T491" id="Seg_6550" s="T490">1SG.[NOM]</ta>
            <ta e="T492" id="Seg_6551" s="T491">though</ta>
            <ta e="T493" id="Seg_6552" s="T492">when</ta>
            <ta e="T494" id="Seg_6553" s="T493">NEG</ta>
            <ta e="T495" id="Seg_6554" s="T494">see-NEG.PTCP.PST</ta>
            <ta e="T496" id="Seg_6555" s="T495">beautiful</ta>
            <ta e="T497" id="Seg_6556" s="T496">silk.[NOM]</ta>
            <ta e="T498" id="Seg_6557" s="T497">kerchief.[NOM]</ta>
            <ta e="T499" id="Seg_6558" s="T498">become-CVB.SEQ</ta>
            <ta e="T500" id="Seg_6559" s="T499">czar.[NOM]</ta>
            <ta e="T501" id="Seg_6560" s="T500">son-3SG-GEN</ta>
            <ta e="T502" id="Seg_6561" s="T501">knee-3SG-DAT/LOC</ta>
            <ta e="T503" id="Seg_6562" s="T502">fall-FUT-1SG</ta>
            <ta e="T504" id="Seg_6563" s="T503">every-INTNS-3PL-ACC</ta>
            <ta e="T505" id="Seg_6564" s="T504">hang.oneself-CAUS-CVB.SEQ</ta>
            <ta e="T506" id="Seg_6565" s="T505">kill-FUT-1SG</ta>
            <ta e="T507" id="Seg_6566" s="T506">1SG.[NOM]</ta>
            <ta e="T508" id="Seg_6567" s="T507">though</ta>
            <ta e="T509" id="Seg_6568" s="T508">small.boat-3PL-ACC</ta>
            <ta e="T510" id="Seg_6569" s="T509">tight</ta>
            <ta e="T511" id="Seg_6570" s="T510">glue-PTCP.PRS</ta>
            <ta e="T512" id="Seg_6571" s="T511">make-CVB.SIM</ta>
            <ta e="T513" id="Seg_6572" s="T512">liquid</ta>
            <ta e="T514" id="Seg_6573" s="T513">tar.[NOM]</ta>
            <ta e="T515" id="Seg_6574" s="T514">become-FUT-1SG</ta>
            <ta e="T516" id="Seg_6575" s="T515">every-3PL-ACC</ta>
            <ta e="T517" id="Seg_6576" s="T516">pull.up-CVB.SEQ</ta>
            <ta e="T518" id="Seg_6577" s="T517">sink-CAUS-FUT-1SG</ta>
            <ta e="T519" id="Seg_6578" s="T518">well</ta>
            <ta e="T520" id="Seg_6579" s="T519">good.[NOM]</ta>
            <ta e="T521" id="Seg_6580" s="T520">very.well</ta>
            <ta e="T522" id="Seg_6581" s="T521">think-PST2-2PL</ta>
            <ta e="T523" id="Seg_6582" s="T522">be-PST2.[3SG]</ta>
            <ta e="T524" id="Seg_6583" s="T523">say-PST2.[3SG]</ta>
            <ta e="T525" id="Seg_6584" s="T524">first</ta>
            <ta e="T526" id="Seg_6585" s="T525">woman.[NOM]</ta>
            <ta e="T527" id="Seg_6586" s="T526">dog.[NOM]</ta>
            <ta e="T528" id="Seg_6587" s="T527">son-3SG.[NOM]</ta>
            <ta e="T529" id="Seg_6588" s="T528">that-ACC</ta>
            <ta e="T530" id="Seg_6589" s="T529">hear-EP-PST2.[3SG]</ta>
            <ta e="T531" id="Seg_6590" s="T530">EMPH</ta>
            <ta e="T532" id="Seg_6591" s="T531">high.shore.[NOM]</ta>
            <ta e="T533" id="Seg_6592" s="T532">lower.part-3SG-DAT/LOC</ta>
            <ta e="T534" id="Seg_6593" s="T533">run-CVB.SEQ</ta>
            <ta e="T535" id="Seg_6594" s="T534">reach-PST2.[3SG]</ta>
            <ta e="T536" id="Seg_6595" s="T535">small.boat-3SG-ACC</ta>
            <ta e="T537" id="Seg_6596" s="T536">take-INCH-PRS.[3SG]</ta>
            <ta e="T538" id="Seg_6597" s="T537">and</ta>
            <ta e="T539" id="Seg_6598" s="T538">row-CVB.SEQ</ta>
            <ta e="T540" id="Seg_6599" s="T539">go-PST2.[3SG]</ta>
            <ta e="T541" id="Seg_6600" s="T540">friend-PL-3SG-ACC</ta>
            <ta e="T542" id="Seg_6601" s="T541">river.[NOM]</ta>
            <ta e="T543" id="Seg_6602" s="T542">middle-3SG-DAT/LOC</ta>
            <ta e="T544" id="Seg_6603" s="T543">chase-CVB.SIM</ta>
            <ta e="T545" id="Seg_6604" s="T544">beat-EP-PST2.[3SG]</ta>
            <ta e="T546" id="Seg_6605" s="T545">boy-PL.[NOM]</ta>
            <ta e="T547" id="Seg_6606" s="T546">silently</ta>
            <ta e="T548" id="Seg_6607" s="T547">row-CVB.SEQ</ta>
            <ta e="T549" id="Seg_6608" s="T548">go-PST2-3PL</ta>
            <ta e="T550" id="Seg_6609" s="T549">how.much</ta>
            <ta e="T551" id="Seg_6610" s="T550">long</ta>
            <ta e="T552" id="Seg_6611" s="T551">be-FUT.[3SG]=Q</ta>
            <ta e="T553" id="Seg_6612" s="T552">small.boat-3PL-GEN</ta>
            <ta e="T554" id="Seg_6613" s="T553">inside-3SG-DAT/LOC</ta>
            <ta e="T555" id="Seg_6614" s="T554">food-PROPR</ta>
            <ta e="T556" id="Seg_6615" s="T555">plate.[NOM]</ta>
            <ta e="T557" id="Seg_6616" s="T556">fall-PST2.[3SG]</ta>
            <ta e="T558" id="Seg_6617" s="T557">czar.[NOM]</ta>
            <ta e="T559" id="Seg_6618" s="T558">son-3SG.[NOM]</ta>
            <ta e="T560" id="Seg_6619" s="T559">be.happy-PST2.[3SG]</ta>
            <ta e="T561" id="Seg_6620" s="T560">hand-3SG-ACC</ta>
            <ta e="T562" id="Seg_6621" s="T561">plate.[NOM]</ta>
            <ta e="T563" id="Seg_6622" s="T562">to</ta>
            <ta e="T564" id="Seg_6623" s="T563">spread-NMNZ.[NOM]</ta>
            <ta e="T565" id="Seg_6624" s="T564">make-PTCP.PRS-3SG-ACC</ta>
            <ta e="T566" id="Seg_6625" s="T565">with</ta>
            <ta e="T567" id="Seg_6626" s="T566">dog.[NOM]</ta>
            <ta e="T568" id="Seg_6627" s="T567">son-3SG.[NOM]</ta>
            <ta e="T569" id="Seg_6628" s="T568">food-PROPR</ta>
            <ta e="T570" id="Seg_6629" s="T569">plate-ACC</ta>
            <ta e="T571" id="Seg_6630" s="T570">water-DAT/LOC</ta>
            <ta e="T572" id="Seg_6631" s="T571">throw-CVB.SEQ</ta>
            <ta e="T573" id="Seg_6632" s="T572">throw-PST2.[3SG]</ta>
            <ta e="T574" id="Seg_6633" s="T573">czar.[NOM]</ta>
            <ta e="T575" id="Seg_6634" s="T574">son-3SG.[NOM]</ta>
            <ta e="T576" id="Seg_6635" s="T575">be.angry-CVB.SEQ</ta>
            <ta e="T577" id="Seg_6636" s="T576">sit.offended-CVB.SEQ</ta>
            <ta e="T578" id="Seg_6637" s="T577">sit-TEMP-3SG</ta>
            <ta e="T579" id="Seg_6638" s="T578">beautiful</ta>
            <ta e="T580" id="Seg_6639" s="T579">very</ta>
            <ta e="T581" id="Seg_6640" s="T580">silk.[NOM]</ta>
            <ta e="T582" id="Seg_6641" s="T581">kerchief.[NOM]</ta>
            <ta e="T583" id="Seg_6642" s="T582">czar.[NOM]</ta>
            <ta e="T584" id="Seg_6643" s="T583">son-3SG-GEN</ta>
            <ta e="T585" id="Seg_6644" s="T584">knee-3SG-DAT/LOC</ta>
            <ta e="T586" id="Seg_6645" s="T585">fall-PST2.[3SG]</ta>
            <ta e="T587" id="Seg_6646" s="T586">oh</ta>
            <ta e="T588" id="Seg_6647" s="T587">god.[NOM]</ta>
            <ta e="T589" id="Seg_6648" s="T588">mum-1SG-DAT/LOC</ta>
            <ta e="T590" id="Seg_6649" s="T589">gift.[NOM]</ta>
            <ta e="T591" id="Seg_6650" s="T590">make-FUT-1SG</ta>
            <ta e="T592" id="Seg_6651" s="T591">say-PST2.[3SG]</ta>
            <ta e="T593" id="Seg_6652" s="T592">and</ta>
            <ta e="T594" id="Seg_6653" s="T593">pocket-3SG-DAT/LOC</ta>
            <ta e="T595" id="Seg_6654" s="T594">put.in-CVB.PURP</ta>
            <ta e="T596" id="Seg_6655" s="T595">want-PTCP.PRS-3SG-ACC</ta>
            <ta e="T597" id="Seg_6656" s="T596">with</ta>
            <ta e="T598" id="Seg_6657" s="T597">son-3SG.[NOM]</ta>
            <ta e="T599" id="Seg_6658" s="T598">choose-CVB.SEQ</ta>
            <ta e="T600" id="Seg_6659" s="T599">take-PST2.[3SG]</ta>
            <ta e="T601" id="Seg_6660" s="T600">knife-3SG-INSTR</ta>
            <ta e="T602" id="Seg_6661" s="T601">much</ta>
            <ta e="T603" id="Seg_6662" s="T602">cut.up-CVB.SEQ</ta>
            <ta e="T604" id="Seg_6663" s="T603">after</ta>
            <ta e="T605" id="Seg_6664" s="T604">water-DAT/LOC</ta>
            <ta e="T606" id="Seg_6665" s="T605">throw-CVB.SEQ</ta>
            <ta e="T607" id="Seg_6666" s="T606">throw-PST2.[3SG]</ta>
            <ta e="T608" id="Seg_6667" s="T607">czar.[NOM]</ta>
            <ta e="T609" id="Seg_6668" s="T608">son-3SG.[NOM]</ta>
            <ta e="T610" id="Seg_6669" s="T609">be.angry-CVB.SEQ</ta>
            <ta e="T611" id="Seg_6670" s="T610">voice-3SG.[NOM]</ta>
            <ta e="T612" id="Seg_6671" s="T611">NEG</ta>
            <ta e="T613" id="Seg_6672" s="T612">appear-EP-PST2.NEG.[3SG]</ta>
            <ta e="T614" id="Seg_6673" s="T613">how.much</ta>
            <ta e="T615" id="Seg_6674" s="T614">long</ta>
            <ta e="T616" id="Seg_6675" s="T615">be-FUT.[3SG]=Q</ta>
            <ta e="T617" id="Seg_6676" s="T616">small.boat-3PL.[NOM]</ta>
            <ta e="T618" id="Seg_6677" s="T617">water-DAT/LOC</ta>
            <ta e="T619" id="Seg_6678" s="T618">stick-CVB.SEQ</ta>
            <ta e="T620" id="Seg_6679" s="T619">stay-PST2.[3SG]</ta>
            <ta e="T621" id="Seg_6680" s="T620">river.[NOM]</ta>
            <ta e="T622" id="Seg_6681" s="T621">water-3SG.[NOM]</ta>
            <ta e="T623" id="Seg_6682" s="T622">black</ta>
            <ta e="T624" id="Seg_6683" s="T623">make-PST2.[3SG]</ta>
            <ta e="T625" id="Seg_6684" s="T624">this</ta>
            <ta e="T626" id="Seg_6685" s="T625">boy-PL.[NOM]</ta>
            <ta e="T627" id="Seg_6686" s="T626">can-CVB.SEQ</ta>
            <ta e="T628" id="Seg_6687" s="T627">row-EP-MED-NEG.PTCP</ta>
            <ta e="T629" id="Seg_6688" s="T628">be-CVB.SEQ</ta>
            <ta e="T630" id="Seg_6689" s="T629">stay-PST2-3PL</ta>
            <ta e="T631" id="Seg_6690" s="T630">be.afraid-PST2-3PL</ta>
            <ta e="T632" id="Seg_6691" s="T631">dog.[NOM]</ta>
            <ta e="T633" id="Seg_6692" s="T632">son-3SG.[NOM]</ta>
            <ta e="T634" id="Seg_6693" s="T633">water-ACC</ta>
            <ta e="T635" id="Seg_6694" s="T634">axe-INSTR</ta>
            <ta e="T636" id="Seg_6695" s="T635">cut-CVB.SIM</ta>
            <ta e="T637" id="Seg_6696" s="T636">cut.up-EP-IMP.2PL</ta>
            <ta e="T638" id="Seg_6697" s="T637">knife-3SG-INSTR</ta>
            <ta e="T639" id="Seg_6698" s="T638">pierce-EP-CAUS-CVB.SIM</ta>
            <ta e="T640" id="Seg_6699" s="T639">push-EP-IMP.2PL</ta>
            <ta e="T641" id="Seg_6700" s="T640">1SG.[NOM]</ta>
            <ta e="T642" id="Seg_6701" s="T641">row-CVB.SEQ</ta>
            <ta e="T643" id="Seg_6702" s="T642">go-FUT-1SG</ta>
            <ta e="T644" id="Seg_6703" s="T643">say-PST2.[3SG]</ta>
            <ta e="T645" id="Seg_6704" s="T644">friend-PL-3SG-DAT/LOC</ta>
            <ta e="T646" id="Seg_6705" s="T645">see-PST2-3PL</ta>
            <ta e="T647" id="Seg_6706" s="T646">water-3PL.[NOM]</ta>
            <ta e="T648" id="Seg_6707" s="T647">really</ta>
            <ta e="T649" id="Seg_6708" s="T648">get.light-CVB.SEQ</ta>
            <ta e="T650" id="Seg_6709" s="T649">get.red-CVB.SEQ</ta>
            <ta e="T651" id="Seg_6710" s="T650">go-PST2.[3SG]</ta>
            <ta e="T652" id="Seg_6711" s="T651">soon</ta>
            <ta e="T653" id="Seg_6712" s="T652">small.boat-3PL.[NOM]</ta>
            <ta e="T654" id="Seg_6713" s="T653">free-EP-REFL-PST2.[3SG]</ta>
            <ta e="T655" id="Seg_6714" s="T654">boy-PL.[NOM]</ta>
            <ta e="T656" id="Seg_6715" s="T655">row-EP-MED-CVB.SEQ</ta>
            <ta e="T657" id="Seg_6716" s="T656">go-PST2-3PL</ta>
            <ta e="T658" id="Seg_6717" s="T657">silently</ta>
            <ta e="T659" id="Seg_6718" s="T658">how.much</ta>
            <ta e="T660" id="Seg_6719" s="T659">NEG</ta>
            <ta e="T661" id="Seg_6720" s="T660">long</ta>
            <ta e="T662" id="Seg_6721" s="T661">be-NEG.CVB.SIM</ta>
            <ta e="T663" id="Seg_6722" s="T662">river.[NOM]</ta>
            <ta e="T664" id="Seg_6723" s="T663">high</ta>
            <ta e="T665" id="Seg_6724" s="T664">shore-3SG-DAT/LOC</ta>
            <ta e="T666" id="Seg_6725" s="T665">city-3PL.[NOM]</ta>
            <ta e="T667" id="Seg_6726" s="T666">to.be.on.view-EP-PST2.[3SG]</ta>
            <ta e="T668" id="Seg_6727" s="T667">come.close-CVB.SEQ</ta>
            <ta e="T669" id="Seg_6728" s="T668">after</ta>
            <ta e="T670" id="Seg_6729" s="T669">boy-PL.[NOM]</ta>
            <ta e="T671" id="Seg_6730" s="T670">three</ta>
            <ta e="T672" id="Seg_6731" s="T671">direction.[NOM]</ta>
            <ta e="T673" id="Seg_6732" s="T672">separate-CVB.SEQ</ta>
            <ta e="T674" id="Seg_6733" s="T673">stay-PST2-3PL</ta>
            <ta e="T675" id="Seg_6734" s="T674">czar.[NOM]</ta>
            <ta e="T676" id="Seg_6735" s="T675">son-3SG.[NOM]</ta>
            <ta e="T677" id="Seg_6736" s="T676">high.shore.[NOM]</ta>
            <ta e="T678" id="Seg_6737" s="T677">upper.part-3SG-DAT/LOC</ta>
            <ta e="T679" id="Seg_6738" s="T678">white</ta>
            <ta e="T680" id="Seg_6739" s="T679">house-DAT/LOC</ta>
            <ta e="T681" id="Seg_6740" s="T680">go-PST2.[3SG]</ta>
            <ta e="T682" id="Seg_6741" s="T681">dog.[NOM]</ta>
            <ta e="T683" id="Seg_6742" s="T682">son-3SG.[NOM]</ta>
            <ta e="T684" id="Seg_6743" s="T683">mum-3SG-GEN</ta>
            <ta e="T685" id="Seg_6744" s="T684">low</ta>
            <ta e="T686" id="Seg_6745" s="T685">burrow.[NOM]</ta>
            <ta e="T687" id="Seg_6746" s="T686">house-3SG-DAT/LOC</ta>
            <ta e="T688" id="Seg_6747" s="T687">go.in-PST2.[3SG]</ta>
            <ta e="T689" id="Seg_6748" s="T688">cow.[NOM]</ta>
            <ta e="T690" id="Seg_6749" s="T689">son-3SG.[NOM]</ta>
            <ta e="T691" id="Seg_6750" s="T690">outside</ta>
            <ta e="T692" id="Seg_6751" s="T691">mother-3SG-ACC</ta>
            <ta e="T693" id="Seg_6752" s="T692">meet-EP-PST2.[3SG]</ta>
            <ta e="T694" id="Seg_6753" s="T693">in.the.morning</ta>
            <ta e="T695" id="Seg_6754" s="T694">czar.[NOM]</ta>
            <ta e="T696" id="Seg_6755" s="T695">boy-PL-ACC</ta>
            <ta e="T697" id="Seg_6756" s="T696">call-MED-FREQ-PST2.[3SG]</ta>
            <ta e="T698" id="Seg_6757" s="T697">well</ta>
            <ta e="T699" id="Seg_6758" s="T698">child-PL-EP-1SG.[NOM]</ta>
            <ta e="T700" id="Seg_6759" s="T699">tell-IMP.2PL</ta>
            <ta e="T701" id="Seg_6760" s="T700">what-ACC</ta>
            <ta e="T702" id="Seg_6761" s="T701">make-CVB.SIM</ta>
            <ta e="T703" id="Seg_6762" s="T702">this</ta>
            <ta e="T704" id="Seg_6763" s="T703">so.long</ta>
            <ta e="T705" id="Seg_6764" s="T704">go-EP-PST2-2PL=Q</ta>
            <ta e="T706" id="Seg_6765" s="T705">three</ta>
            <ta e="T707" id="Seg_6766" s="T706">evil.spirit.[NOM]</ta>
            <ta e="T708" id="Seg_6767" s="T707">skull-3SG-ABL</ta>
            <ta e="T709" id="Seg_6768" s="T708">three</ta>
            <ta e="T710" id="Seg_6769" s="T709">writing-PROPR</ta>
            <ta e="T711" id="Seg_6770" s="T710">stone-ACC</ta>
            <ta e="T712" id="Seg_6771" s="T711">find-PST2-EP-1SG</ta>
            <ta e="T713" id="Seg_6772" s="T712">dog.[NOM]</ta>
            <ta e="T714" id="Seg_6773" s="T713">son-3SG.[NOM]</ta>
            <ta e="T715" id="Seg_6774" s="T714">say-PST2.[3SG]</ta>
            <ta e="T716" id="Seg_6775" s="T715">czar.[NOM]</ta>
            <ta e="T717" id="Seg_6776" s="T716">hand-3SG-DAT/LOC</ta>
            <ta e="T718" id="Seg_6777" s="T717">three</ta>
            <ta e="T719" id="Seg_6778" s="T718">stone-ACC</ta>
            <ta e="T720" id="Seg_6779" s="T719">hand-CVB.SEQ</ta>
            <ta e="T721" id="Seg_6780" s="T720">throw-PST2.[3SG]</ta>
            <ta e="T722" id="Seg_6781" s="T721">czar.[NOM]</ta>
            <ta e="T723" id="Seg_6782" s="T722">dog.[NOM]</ta>
            <ta e="T724" id="Seg_6783" s="T723">son-3SG-ACC</ta>
            <ta e="T725" id="Seg_6784" s="T724">neck-3SG-ABL</ta>
            <ta e="T726" id="Seg_6785" s="T725">embrace-CVB.SEQ</ta>
            <ta e="T727" id="Seg_6786" s="T726">take-PST2.[3SG]</ta>
            <ta e="T728" id="Seg_6787" s="T727">cry-EP-MOM-CVB.SIM</ta>
            <ta e="T729" id="Seg_6788" s="T728">fall-CVB.SEQ</ta>
            <ta e="T730" id="Seg_6789" s="T729">after</ta>
            <ta e="T731" id="Seg_6790" s="T730">say-PST2.[3SG]</ta>
            <ta e="T732" id="Seg_6791" s="T731">todays-ABL</ta>
            <ta e="T733" id="Seg_6792" s="T732">2SG.[NOM]</ta>
            <ta e="T734" id="Seg_6793" s="T733">czar.[NOM]</ta>
            <ta e="T735" id="Seg_6794" s="T734">be-PTCP.PRS</ta>
            <ta e="T736" id="Seg_6795" s="T735">destiny-EP-2SG.[NOM]</ta>
            <ta e="T737" id="Seg_6796" s="T736">come-PST1-3SG</ta>
            <ta e="T738" id="Seg_6797" s="T737">son-1SG-ACC</ta>
            <ta e="T739" id="Seg_6798" s="T738">with</ta>
            <ta e="T740" id="Seg_6799" s="T739">elder.brother.[NOM]-younger.brother.[NOM]</ta>
            <ta e="T741" id="Seg_6800" s="T740">be-CVB.SEQ</ta>
            <ta e="T742" id="Seg_6801" s="T741">live-EP-IMP.2PL</ta>
            <ta e="T743" id="Seg_6802" s="T742">three</ta>
            <ta e="T744" id="Seg_6803" s="T743">boy.[NOM]</ta>
            <ta e="T745" id="Seg_6804" s="T744">friend-SIM</ta>
            <ta e="T746" id="Seg_6805" s="T745">be.rich-CVB.SEQ-be.rich-CVB.SEQ</ta>
            <ta e="T747" id="Seg_6806" s="T746">live-PST2-3PL</ta>
         </annotation>
         <annotation name="gg" tierref="gd">
            <ta e="T1" id="Seg_6807" s="T0">vor.langer.Zeit-vor.langer.Zeit</ta>
            <ta e="T2" id="Seg_6808" s="T1">eins</ta>
            <ta e="T3" id="Seg_6809" s="T2">Zar.[NOM]</ta>
            <ta e="T4" id="Seg_6810" s="T3">leben-PST2.[3SG]</ta>
            <ta e="T5" id="Seg_6811" s="T4">Kind-POSS</ta>
            <ta e="T6" id="Seg_6812" s="T5">NEG</ta>
            <ta e="T7" id="Seg_6813" s="T6">be-PST1-3SG</ta>
            <ta e="T8" id="Seg_6814" s="T7">sein-PST2.[3SG]</ta>
            <ta e="T9" id="Seg_6815" s="T8">einmal</ta>
            <ta e="T10" id="Seg_6816" s="T9">dieses</ta>
            <ta e="T11" id="Seg_6817" s="T10">Zar.[NOM]</ta>
            <ta e="T12" id="Seg_6818" s="T11">denken-PST2.[3SG]</ta>
            <ta e="T13" id="Seg_6819" s="T12">dieses</ta>
            <ta e="T14" id="Seg_6820" s="T13">warum</ta>
            <ta e="T15" id="Seg_6821" s="T14">1PL.[NOM]</ta>
            <ta e="T16" id="Seg_6822" s="T15">Kind-POSS</ta>
            <ta e="T17" id="Seg_6823" s="T16">NEG-1PL=Q</ta>
            <ta e="T18" id="Seg_6824" s="T17">alt-PL-ABL</ta>
            <ta e="T19" id="Seg_6825" s="T18">fragen-PTCP.PST.[NOM]</ta>
            <ta e="T20" id="Seg_6826" s="T19">MOD</ta>
            <ta e="T21" id="Seg_6827" s="T20">dieses</ta>
            <ta e="T22" id="Seg_6828" s="T21">denken-CVB.SIM</ta>
            <ta e="T23" id="Seg_6829" s="T22">sitzen-TEMP-3SG</ta>
            <ta e="T24" id="Seg_6830" s="T23">Frau-3SG.[NOM]</ta>
            <ta e="T25" id="Seg_6831" s="T24">hineingehen-PST2.[3SG]</ta>
            <ta e="T26" id="Seg_6832" s="T25">was-ACC</ta>
            <ta e="T27" id="Seg_6833" s="T26">nachdenken-PST1-2SG</ta>
            <ta e="T28" id="Seg_6834" s="T27">Freund</ta>
            <ta e="T29" id="Seg_6835" s="T28">morgen</ta>
            <ta e="T30" id="Seg_6836" s="T29">1PL.[NOM]</ta>
            <ta e="T31" id="Seg_6837" s="T30">Mensch-PL-ACC</ta>
            <ta e="T32" id="Seg_6838" s="T31">sammeln-FUT-1PL</ta>
            <ta e="T33" id="Seg_6839" s="T32">was.für.ein-DAT/LOC</ta>
            <ta e="T34" id="Seg_6840" s="T33">bis.zu</ta>
            <ta e="T35" id="Seg_6841" s="T34">leer</ta>
            <ta e="T36" id="Seg_6842" s="T35">Wand-ACC</ta>
            <ta e="T37" id="Seg_6843" s="T36">sehen-CVB.SEQ</ta>
            <ta e="T38" id="Seg_6844" s="T37">leben-FUT-1PL=Q</ta>
            <ta e="T39" id="Seg_6845" s="T38">Kind-POSS</ta>
            <ta e="T40" id="Seg_6846" s="T39">NEG</ta>
            <ta e="T41" id="Seg_6847" s="T40">fragen-IMP.1DU</ta>
            <ta e="T42" id="Seg_6848" s="T41">warum</ta>
            <ta e="T43" id="Seg_6849" s="T42">Kind-POSS</ta>
            <ta e="T44" id="Seg_6850" s="T43">NEG-1PL=Q</ta>
            <ta e="T45" id="Seg_6851" s="T44">wer-DAT/LOC</ta>
            <ta e="T46" id="Seg_6852" s="T45">Reichtum-1PL-ACC</ta>
            <ta e="T47" id="Seg_6853" s="T46">geben-FUT-1PL=Q</ta>
            <ta e="T48" id="Seg_6854" s="T47">sterben-TEMP-1PL</ta>
            <ta e="T49" id="Seg_6855" s="T48">am.Morgen</ta>
            <ta e="T50" id="Seg_6856" s="T49">aufstehen-PST2-3PL</ta>
            <ta e="T51" id="Seg_6857" s="T50">viel</ta>
            <ta e="T52" id="Seg_6858" s="T51">sehr</ta>
            <ta e="T53" id="Seg_6859" s="T52">Mensch.[NOM]</ta>
            <ta e="T54" id="Seg_6860" s="T53">sammeln-PASS/REFL-EP-PST2.[3SG]</ta>
            <ta e="T55" id="Seg_6861" s="T54">dort</ta>
            <ta e="T56" id="Seg_6862" s="T55">Zar.[NOM]</ta>
            <ta e="T57" id="Seg_6863" s="T56">fragen-PST2.[3SG]</ta>
            <ta e="T58" id="Seg_6864" s="T57">dieses</ta>
            <ta e="T59" id="Seg_6865" s="T58">warum</ta>
            <ta e="T60" id="Seg_6866" s="T59">1PL.[NOM]</ta>
            <ta e="T61" id="Seg_6867" s="T60">Kind-POSS</ta>
            <ta e="T62" id="Seg_6868" s="T61">NEG-1PL=Q</ta>
            <ta e="T63" id="Seg_6869" s="T62">wie</ta>
            <ta e="T64" id="Seg_6870" s="T63">was-ACC</ta>
            <ta e="T65" id="Seg_6871" s="T64">machen-PTCP.COND-DAT/LOC</ta>
            <ta e="T66" id="Seg_6872" s="T65">Kind-VBZ-PTCP.FUT-1PL=Q</ta>
            <ta e="T67" id="Seg_6873" s="T66">2PL.[NOM]</ta>
            <ta e="T68" id="Seg_6874" s="T67">wie</ta>
            <ta e="T69" id="Seg_6875" s="T68">denken-CVB.SIM</ta>
            <ta e="T70" id="Seg_6876" s="T69">denken-PRS-2PL</ta>
            <ta e="T71" id="Seg_6877" s="T70">Mensch-PL.[NOM]</ta>
            <ta e="T72" id="Seg_6878" s="T71">lärmen-CVB.SIM</ta>
            <ta e="T73" id="Seg_6879" s="T72">fallen-PST2-3PL</ta>
            <ta e="T74" id="Seg_6880" s="T73">Tür-3SG-INSTR</ta>
            <ta e="T75" id="Seg_6881" s="T74">alt</ta>
            <ta e="T76" id="Seg_6882" s="T75">krumm</ta>
            <ta e="T77" id="Seg_6883" s="T76">alter.Mann.[NOM]</ta>
            <ta e="T78" id="Seg_6884" s="T77">sich.hinauslehnen-NMNZ.[NOM]</ta>
            <ta e="T79" id="Seg_6885" s="T78">machen-PST2.[3SG]</ta>
            <ta e="T80" id="Seg_6886" s="T79">krumm</ta>
            <ta e="T81" id="Seg_6887" s="T80">Qual-PROPR-ACC</ta>
            <ta e="T82" id="Seg_6888" s="T81">Mitte-DAT/LOC</ta>
            <ta e="T83" id="Seg_6889" s="T82">stoßen-CVB.SEQ</ta>
            <ta e="T84" id="Seg_6890" s="T83">werfen-PST2-3PL</ta>
            <ta e="T85" id="Seg_6891" s="T84">dieses</ta>
            <ta e="T86" id="Seg_6892" s="T85">es.gibt</ta>
            <ta e="T87" id="Seg_6893" s="T86">wissen-PTCP.PRS</ta>
            <ta e="T88" id="Seg_6894" s="T87">Mensch.[NOM]</ta>
            <ta e="T89" id="Seg_6895" s="T88">alter.Mann.[NOM]</ta>
            <ta e="T90" id="Seg_6896" s="T89">nach.oben</ta>
            <ta e="T91" id="Seg_6897" s="T90">sehen-CVB.SEQ</ta>
            <ta e="T92" id="Seg_6898" s="T91">nachdem</ta>
            <ta e="T93" id="Seg_6899" s="T92">Rücken.[NOM]</ta>
            <ta e="T94" id="Seg_6900" s="T93">ergreifen-CVB.SEQ</ta>
            <ta e="T95" id="Seg_6901" s="T94">nachdem</ta>
            <ta e="T96" id="Seg_6902" s="T95">sagen-PST2.[3SG]</ta>
            <ta e="T97" id="Seg_6903" s="T96">machen-EP-IMP.2PL</ta>
            <ta e="T98" id="Seg_6904" s="T97">Gold.[NOM]</ta>
            <ta e="T99" id="Seg_6905" s="T98">Netz-PART</ta>
            <ta e="T100" id="Seg_6906" s="T99">Gold.[NOM]</ta>
            <ta e="T101" id="Seg_6907" s="T100">kleines.Boot-PART</ta>
            <ta e="T102" id="Seg_6908" s="T101">Gold.[NOM]</ta>
            <ta e="T103" id="Seg_6909" s="T102">Ruder-PART</ta>
            <ta e="T104" id="Seg_6910" s="T103">dann</ta>
            <ta e="T105" id="Seg_6911" s="T104">Zar.[NOM]</ta>
            <ta e="T106" id="Seg_6912" s="T105">fischen-CVB.SIM</ta>
            <ta e="T107" id="Seg_6913" s="T106">gehen-IMP.3SG</ta>
            <ta e="T108" id="Seg_6914" s="T107">Netz-3SG-DAT/LOC</ta>
            <ta e="T109" id="Seg_6915" s="T108">Fisch.[NOM]</ta>
            <ta e="T110" id="Seg_6916" s="T109">halten-FUT.[3SG]</ta>
            <ta e="T111" id="Seg_6917" s="T110">jenes-ACC</ta>
            <ta e="T112" id="Seg_6918" s="T111">Frau-3SG.[NOM]</ta>
            <ta e="T113" id="Seg_6919" s="T112">essen-IMP.3SG</ta>
            <ta e="T114" id="Seg_6920" s="T113">Zar.[NOM]</ta>
            <ta e="T115" id="Seg_6921" s="T114">Frau-3SG.[NOM]</ta>
            <ta e="T116" id="Seg_6922" s="T115">lachen-PST2.[3SG]</ta>
            <ta e="T117" id="Seg_6923" s="T116">jenes-ABL</ta>
            <ta e="T118" id="Seg_6924" s="T117">1SG.[NOM]</ta>
            <ta e="T119" id="Seg_6925" s="T118">sich.satt.essen-FUT-1SG</ta>
            <ta e="T120" id="Seg_6926" s="T119">Q</ta>
            <ta e="T121" id="Seg_6927" s="T120">lange</ta>
            <ta e="T122" id="Seg_6928" s="T121">sein-FUT.[3SG]=Q</ta>
            <ta e="T123" id="Seg_6929" s="T122">Zar.[NOM]</ta>
            <ta e="T124" id="Seg_6930" s="T123">nächster.Morgen-3SG-ACC</ta>
            <ta e="T125" id="Seg_6931" s="T124">Gold.[NOM]</ta>
            <ta e="T126" id="Seg_6932" s="T125">kleines.Boot-PROPR</ta>
            <ta e="T127" id="Seg_6933" s="T126">Gold.[NOM]</ta>
            <ta e="T128" id="Seg_6934" s="T127">Ruder-PROPR</ta>
            <ta e="T129" id="Seg_6935" s="T128">fischen-VBZ-CVB.SIM</ta>
            <ta e="T130" id="Seg_6936" s="T129">gehen-PST2.[3SG]</ta>
            <ta e="T131" id="Seg_6937" s="T130">eins</ta>
            <ta e="T132" id="Seg_6938" s="T131">Fisch-ACC</ta>
            <ta e="T133" id="Seg_6939" s="T132">zittern-CVB.SEQ</ta>
            <ta e="T134" id="Seg_6940" s="T133">lebendig-SIM</ta>
            <ta e="T135" id="Seg_6941" s="T134">bringen-NEG.[3SG]</ta>
            <ta e="T136" id="Seg_6942" s="T135">MOD</ta>
            <ta e="T137" id="Seg_6943" s="T136">dieses.[NOM]</ta>
            <ta e="T138" id="Seg_6944" s="T137">Fang-EP-1SG.[NOM]</ta>
            <ta e="T139" id="Seg_6945" s="T138">sagen-PST2.[3SG]</ta>
            <ta e="T140" id="Seg_6946" s="T139">and</ta>
            <ta e="T141" id="Seg_6947" s="T140">Koch-3PL-DAT/LOC</ta>
            <ta e="T142" id="Seg_6948" s="T141">geben-CVB.SEQ</ta>
            <ta e="T143" id="Seg_6949" s="T142">werfen-PST2.[3SG]</ta>
            <ta e="T144" id="Seg_6950" s="T143">sofort</ta>
            <ta e="T145" id="Seg_6951" s="T144">kochen-IMP.2PL</ta>
            <ta e="T146" id="Seg_6952" s="T145">Koch.[NOM]</ta>
            <ta e="T147" id="Seg_6953" s="T146">Fisch.[NOM]</ta>
            <ta e="T148" id="Seg_6954" s="T147">Schuppe-3SG-ACC</ta>
            <ta e="T149" id="Seg_6955" s="T148">Gras.[NOM]</ta>
            <ta e="T150" id="Seg_6956" s="T149">Inneres-3SG-DAT/LOC</ta>
            <ta e="T151" id="Seg_6957" s="T150">säubern-CVB.SEQ</ta>
            <ta e="T152" id="Seg_6958" s="T151">werfen-PST2.[3SG]</ta>
            <ta e="T153" id="Seg_6959" s="T152">jenes-ACC</ta>
            <ta e="T154" id="Seg_6960" s="T153">Zar.[NOM]</ta>
            <ta e="T155" id="Seg_6961" s="T154">Kuh-3SG.[NOM]</ta>
            <ta e="T156" id="Seg_6962" s="T155">essen-CVB.SEQ</ta>
            <ta e="T157" id="Seg_6963" s="T156">werfen-PST2.[3SG]</ta>
            <ta e="T158" id="Seg_6964" s="T157">Zar.[NOM]</ta>
            <ta e="T159" id="Seg_6965" s="T158">Frau-3SG.[NOM]</ta>
            <ta e="T160" id="Seg_6966" s="T159">Fisch-ACC</ta>
            <ta e="T161" id="Seg_6967" s="T160">essen-CVB.SEQ</ta>
            <ta e="T162" id="Seg_6968" s="T161">nachdem</ta>
            <ta e="T163" id="Seg_6969" s="T162">Knochen-3SG-ACC</ta>
            <ta e="T164" id="Seg_6970" s="T163">Teller-DAT/LOC</ta>
            <ta e="T165" id="Seg_6971" s="T164">bleiben-CAUS-PTCP.PST-3SG-ACC</ta>
            <ta e="T166" id="Seg_6972" s="T165">Koch.[NOM]</ta>
            <ta e="T167" id="Seg_6973" s="T166">Hund-DAT/LOC</ta>
            <ta e="T168" id="Seg_6974" s="T167">werfen-PST2.[3SG]</ta>
            <ta e="T169" id="Seg_6975" s="T168">Märchen-DAT/LOC</ta>
            <ta e="T170" id="Seg_6976" s="T169">lange</ta>
            <ta e="T171" id="Seg_6977" s="T170">werden-FUT.[3SG]</ta>
            <ta e="T172" id="Seg_6978" s="T171">Q</ta>
            <ta e="T173" id="Seg_6979" s="T172">drei-COLL-3PL.[NOM]</ta>
            <ta e="T174" id="Seg_6980" s="T173">drei</ta>
            <ta e="T175" id="Seg_6981" s="T174">Junge-ACC</ta>
            <ta e="T176" id="Seg_6982" s="T175">gebären-PST2-3PL</ta>
            <ta e="T177" id="Seg_6983" s="T176">Kind-PL.[NOM]</ta>
            <ta e="T178" id="Seg_6984" s="T177">wie.viel</ta>
            <ta e="T179" id="Seg_6985" s="T178">NEG</ta>
            <ta e="T180" id="Seg_6986" s="T179">sein-NEG.CVB.SIM</ta>
            <ta e="T181" id="Seg_6987" s="T180">zusammen</ta>
            <ta e="T182" id="Seg_6988" s="T181">spielen-PTCP.PRS</ta>
            <ta e="T183" id="Seg_6989" s="T182">werden-PST2-3PL</ta>
            <ta e="T184" id="Seg_6990" s="T183">einmal</ta>
            <ta e="T185" id="Seg_6991" s="T184">nur</ta>
            <ta e="T186" id="Seg_6992" s="T185">Zar.[NOM]</ta>
            <ta e="T187" id="Seg_6993" s="T186">Sohn-3SG.[NOM]</ta>
            <ta e="T188" id="Seg_6994" s="T187">sagen-PST2.[3SG]</ta>
            <ta e="T189" id="Seg_6995" s="T188">Freund-PL.[NOM]</ta>
            <ta e="T190" id="Seg_6996" s="T189">kleines.Boot-VBZ-CVB.SIM</ta>
            <ta e="T191" id="Seg_6997" s="T190">gehen-IMP.1PL</ta>
            <ta e="T192" id="Seg_6998" s="T191">Junge-PL.[NOM]</ta>
            <ta e="T193" id="Seg_6999" s="T192">wie.viel</ta>
            <ta e="T194" id="Seg_7000" s="T193">lange</ta>
            <ta e="T195" id="Seg_7001" s="T194">rudern-CVB.SEQ</ta>
            <ta e="T196" id="Seg_7002" s="T195">gehen-PST2-3PL</ta>
            <ta e="T197" id="Seg_7003" s="T196">MOD</ta>
            <ta e="T198" id="Seg_7004" s="T197">sehen-PST2-3PL</ta>
            <ta e="T199" id="Seg_7005" s="T198">Fluss.[NOM]</ta>
            <ta e="T200" id="Seg_7006" s="T199">am.anderen.Ufer</ta>
            <ta e="T201" id="Seg_7007" s="T200">groß</ta>
            <ta e="T202" id="Seg_7008" s="T201">sehr</ta>
            <ta e="T203" id="Seg_7009" s="T202">Jurte.[NOM]</ta>
            <ta e="T204" id="Seg_7010" s="T203">Haus.[NOM]</ta>
            <ta e="T205" id="Seg_7011" s="T204">stehen-PRS.[3SG]</ta>
            <ta e="T206" id="Seg_7012" s="T205">dieses.[NOM]</ta>
            <ta e="T207" id="Seg_7013" s="T206">was-1PL.[NOM]=Q</ta>
            <ta e="T208" id="Seg_7014" s="T207">sagen-EP-RECP/COLL-PST2-3PL</ta>
            <ta e="T209" id="Seg_7015" s="T208">Zar.[NOM]</ta>
            <ta e="T210" id="Seg_7016" s="T209">Sohn-3SG.[NOM]</ta>
            <ta e="T211" id="Seg_7017" s="T210">herankommen-IMP.1PL</ta>
            <ta e="T212" id="Seg_7018" s="T211">Karten-VBZ-FUT-1PL</ta>
            <ta e="T213" id="Seg_7019" s="T212">sagen-PST2.[3SG]</ta>
            <ta e="T214" id="Seg_7020" s="T213">Hund.[NOM]</ta>
            <ta e="T215" id="Seg_7021" s="T214">Sohn-3SG.[NOM]</ta>
            <ta e="T216" id="Seg_7022" s="T215">hey</ta>
            <ta e="T217" id="Seg_7023" s="T216">warum</ta>
            <ta e="T218" id="Seg_7024" s="T217">vielleicht</ta>
            <ta e="T219" id="Seg_7025" s="T218">böser.Geist-PL.[NOM]</ta>
            <ta e="T220" id="Seg_7026" s="T219">Haus-3PL.[NOM]</ta>
            <ta e="T221" id="Seg_7027" s="T220">sein-FUT.[3SG]</ta>
            <ta e="T222" id="Seg_7028" s="T221">Kuh.[NOM]</ta>
            <ta e="T223" id="Seg_7029" s="T222">Sohn-3SG.[NOM]</ta>
            <ta e="T224" id="Seg_7030" s="T223">herankommen-IMP.1PL</ta>
            <ta e="T225" id="Seg_7031" s="T224">herankommen-IMP.1PL</ta>
            <ta e="T226" id="Seg_7032" s="T225">schreiten-FREQ-CVB.SIM</ta>
            <ta e="T227" id="Seg_7033" s="T226">fallen-IMP.1PL</ta>
            <ta e="T228" id="Seg_7034" s="T227">Ufer-DAT/LOC</ta>
            <ta e="T229" id="Seg_7035" s="T228">hinausgehen-CVB.ANT</ta>
            <ta e="T230" id="Seg_7036" s="T229">dieses</ta>
            <ta e="T231" id="Seg_7037" s="T230">zwei</ta>
            <ta e="T232" id="Seg_7038" s="T231">Junge.[NOM]</ta>
            <ta e="T233" id="Seg_7039" s="T232">Karten-VBZ-CVB.SIM</ta>
            <ta e="T234" id="Seg_7040" s="T233">sich.setzen-PST2-3PL</ta>
            <ta e="T235" id="Seg_7041" s="T234">Hund.[NOM]</ta>
            <ta e="T236" id="Seg_7042" s="T235">Sohn-3SG.[NOM]</ta>
            <ta e="T237" id="Seg_7043" s="T236">schlafen-CVB.SEQ</ta>
            <ta e="T238" id="Seg_7044" s="T237">bleiben-PST2.[3SG]</ta>
            <ta e="T239" id="Seg_7045" s="T238">wie.viel</ta>
            <ta e="T240" id="Seg_7046" s="T239">lange</ta>
            <ta e="T241" id="Seg_7047" s="T240">spielen-PST2-3PL</ta>
            <ta e="T242" id="Seg_7048" s="T241">MOD</ta>
            <ta e="T243" id="Seg_7049" s="T242">dieses</ta>
            <ta e="T244" id="Seg_7050" s="T243">Kind-PL.[NOM]</ta>
            <ta e="T245" id="Seg_7051" s="T244">auch</ta>
            <ta e="T246" id="Seg_7052" s="T245">schlafen-CVB.SEQ</ta>
            <ta e="T247" id="Seg_7053" s="T246">bleiben-PST2-3PL</ta>
            <ta e="T248" id="Seg_7054" s="T247">Hund.[NOM]</ta>
            <ta e="T249" id="Seg_7055" s="T248">Sohn-3SG.[NOM]</ta>
            <ta e="T250" id="Seg_7056" s="T249">Nacht.[NOM]</ta>
            <ta e="T251" id="Seg_7057" s="T250">Mitte.[NOM]</ta>
            <ta e="T252" id="Seg_7058" s="T251">aufwachen-EP-PST2-3SG</ta>
            <ta e="T253" id="Seg_7059" s="T252">Mensch.[NOM]</ta>
            <ta e="T254" id="Seg_7060" s="T253">schreiten-PTCP.PRS</ta>
            <ta e="T255" id="Seg_7061" s="T254">Geräusch-3SG.[NOM]</ta>
            <ta e="T256" id="Seg_7062" s="T255">gehört.werden-EP-PST2.[3SG]</ta>
            <ta e="T257" id="Seg_7063" s="T256">erscheinen-EP-PST2-3SG</ta>
            <ta e="T258" id="Seg_7064" s="T257">böser.Geist.[NOM]</ta>
            <ta e="T259" id="Seg_7065" s="T258">Jurte.[NOM]</ta>
            <ta e="T260" id="Seg_7066" s="T259">zu</ta>
            <ta e="T261" id="Seg_7067" s="T260">schreiten-CVB.SEQ</ta>
            <ta e="T262" id="Seg_7068" s="T261">gehen-EMOT-PRS.[3SG]</ta>
            <ta e="T263" id="Seg_7069" s="T262">doch</ta>
            <ta e="T264" id="Seg_7070" s="T263">oh</ta>
            <ta e="T265" id="Seg_7071" s="T264">doch</ta>
            <ta e="T266" id="Seg_7072" s="T265">sterben-PST2-1PL</ta>
            <ta e="T267" id="Seg_7073" s="T266">sein-PST2.[3SG]</ta>
            <ta e="T268" id="Seg_7074" s="T267">sagen-CVB.SEQ</ta>
            <ta e="T269" id="Seg_7075" s="T268">nachdem</ta>
            <ta e="T270" id="Seg_7076" s="T269">Freund-PL-3SG-ACC</ta>
            <ta e="T271" id="Seg_7077" s="T270">aufwachen-CAUS-CVB.SIM</ta>
            <ta e="T272" id="Seg_7078" s="T271">vergeblich.tun-PST2.[3SG]</ta>
            <ta e="T273" id="Seg_7079" s="T272">dieses</ta>
            <ta e="T274" id="Seg_7080" s="T273">Junge-PL.[NOM]</ta>
            <ta e="T275" id="Seg_7081" s="T274">denn</ta>
            <ta e="T276" id="Seg_7082" s="T275">aufwachen-FUT-3PL=Q</ta>
            <ta e="T277" id="Seg_7083" s="T276">kommen.[IMP.2SG]-kommen.[IMP.2SG]</ta>
            <ta e="T278" id="Seg_7084" s="T277">1SG.[NOM]</ta>
            <ta e="T279" id="Seg_7085" s="T278">2SG-ACC</ta>
            <ta e="T280" id="Seg_7086" s="T279">essen-FUT-1SG</ta>
            <ta e="T281" id="Seg_7087" s="T280">böser.Geist.[NOM]</ta>
            <ta e="T282" id="Seg_7088" s="T281">Stimme-3SG.[NOM]</ta>
            <ta e="T283" id="Seg_7089" s="T282">gehört.werden-EP-PST2.[3SG]</ta>
            <ta e="T284" id="Seg_7090" s="T283">dieses</ta>
            <ta e="T285" id="Seg_7091" s="T284">Junge.[NOM]</ta>
            <ta e="T286" id="Seg_7092" s="T285">hinausgehen-CVB.SIM</ta>
            <ta e="T287" id="Seg_7093" s="T286">sich.erheben-PST2.[3SG]</ta>
            <ta e="T288" id="Seg_7094" s="T287">und</ta>
            <ta e="T289" id="Seg_7095" s="T288">böser.Geist-ACC</ta>
            <ta e="T290" id="Seg_7096" s="T289">mit</ta>
            <ta e="T291" id="Seg_7097" s="T290">kämpfen-EP-PST2.[3SG]</ta>
            <ta e="T292" id="Seg_7098" s="T291">wie.viel</ta>
            <ta e="T293" id="Seg_7099" s="T292">NEG</ta>
            <ta e="T294" id="Seg_7100" s="T293">sein-NEG.CVB.SIM</ta>
            <ta e="T295" id="Seg_7101" s="T294">böser.Geist.[NOM]</ta>
            <ta e="T296" id="Seg_7102" s="T295">Kopf-3SG-ACC</ta>
            <ta e="T297" id="Seg_7103" s="T296">großer.Stein.[NOM]</ta>
            <ta e="T298" id="Seg_7104" s="T297">Stein-DAT/LOC</ta>
            <ta e="T299" id="Seg_7105" s="T298">spalten-CVB.SIM</ta>
            <ta e="T300" id="Seg_7106" s="T299">schlagen-EP-PST2.[3SG]</ta>
            <ta e="T301" id="Seg_7107" s="T300">jenes</ta>
            <ta e="T302" id="Seg_7108" s="T301">machen-CVB.SEQ</ta>
            <ta e="T303" id="Seg_7109" s="T302">nachdem</ta>
            <ta e="T304" id="Seg_7110" s="T303">Schädel-3SG-GEN</ta>
            <ta e="T305" id="Seg_7111" s="T304">Inneres-3SG-ABL</ta>
            <ta e="T306" id="Seg_7112" s="T305">Schrift-PROPR</ta>
            <ta e="T307" id="Seg_7113" s="T306">Stein-ACC</ta>
            <ta e="T308" id="Seg_7114" s="T307">herausholen-PST2.[3SG]</ta>
            <ta e="T309" id="Seg_7115" s="T308">Stein-ACC</ta>
            <ta e="T310" id="Seg_7116" s="T309">Tasche-3SG-GEN</ta>
            <ta e="T311" id="Seg_7117" s="T310">Inneres-3SG-INSTR</ta>
            <ta e="T312" id="Seg_7118" s="T311">hineintun-CVB.SEQ</ta>
            <ta e="T313" id="Seg_7119" s="T312">werfen-PST2.[3SG]</ta>
            <ta e="T314" id="Seg_7120" s="T313">am.Morgen</ta>
            <ta e="T315" id="Seg_7121" s="T314">was.[NOM]</ta>
            <ta e="T316" id="Seg_7122" s="T315">NEG</ta>
            <ta e="T317" id="Seg_7123" s="T316">sein-NEG.PTCP.PST.[NOM]</ta>
            <ta e="T318" id="Seg_7124" s="T317">ähnlich</ta>
            <ta e="T319" id="Seg_7125" s="T318">Hund.[NOM]</ta>
            <ta e="T320" id="Seg_7126" s="T319">Sohn-3SG.[NOM]</ta>
            <ta e="T321" id="Seg_7127" s="T320">Freund-PL-3SG-ACC</ta>
            <ta e="T322" id="Seg_7128" s="T321">aufwachen-CAUS-ITER-PST2.[3SG]</ta>
            <ta e="T323" id="Seg_7129" s="T322">nun</ta>
            <ta e="T324" id="Seg_7130" s="T323">Gans-VBZ-CVB.SIM</ta>
            <ta e="T325" id="Seg_7131" s="T324">gehen-IMP.1PL</ta>
            <ta e="T326" id="Seg_7132" s="T325">Junge-PL.[NOM]</ta>
            <ta e="T327" id="Seg_7133" s="T326">sich.strecken-CVB.SEQ</ta>
            <ta e="T328" id="Seg_7134" s="T327">kaum</ta>
            <ta e="T329" id="Seg_7135" s="T328">stehen-CVB.SEQ</ta>
            <ta e="T330" id="Seg_7136" s="T329">Tee.[NOM]</ta>
            <ta e="T331" id="Seg_7137" s="T330">trinken-PST2-3PL</ta>
            <ta e="T332" id="Seg_7138" s="T331">Märchen-DAT/LOC</ta>
            <ta e="T333" id="Seg_7139" s="T332">Tag.[NOM]</ta>
            <ta e="T334" id="Seg_7140" s="T333">kurz.[NOM]</ta>
            <ta e="T335" id="Seg_7141" s="T334">wie.viel</ta>
            <ta e="T336" id="Seg_7142" s="T335">NEG</ta>
            <ta e="T337" id="Seg_7143" s="T336">sein-NEG.CVB.SIM</ta>
            <ta e="T338" id="Seg_7144" s="T337">Himmel.[NOM]</ta>
            <ta e="T339" id="Seg_7145" s="T338">dunkel.werden-PST2.[3SG]</ta>
            <ta e="T340" id="Seg_7146" s="T339">Hund.[NOM]</ta>
            <ta e="T341" id="Seg_7147" s="T340">Sohn-3SG.[NOM]</ta>
            <ta e="T342" id="Seg_7148" s="T341">essen-CVB.SEQ-essen-CVB.SEQ</ta>
            <ta e="T343" id="Seg_7149" s="T342">nachdem</ta>
            <ta e="T344" id="Seg_7150" s="T343">schlafen-CVB.SEQ</ta>
            <ta e="T345" id="Seg_7151" s="T344">bleiben-PST2.[3SG]</ta>
            <ta e="T346" id="Seg_7152" s="T345">Freund-PL-3SG.[NOM]</ta>
            <ta e="T347" id="Seg_7153" s="T346">wieder</ta>
            <ta e="T348" id="Seg_7154" s="T347">Karten-VBZ-CVB.SIM</ta>
            <ta e="T349" id="Seg_7155" s="T348">sich.setzen-PST2-3PL</ta>
            <ta e="T350" id="Seg_7156" s="T349">Nacht.[NOM]</ta>
            <ta e="T351" id="Seg_7157" s="T350">Mitte.[NOM]</ta>
            <ta e="T352" id="Seg_7158" s="T351">aufwachen-EP-PST2-3SG</ta>
            <ta e="T353" id="Seg_7159" s="T352">Freund-PL-3SG.[NOM]</ta>
            <ta e="T354" id="Seg_7160" s="T353">sitzen-CVB.SIM</ta>
            <ta e="T355" id="Seg_7161" s="T354">sich.setzen-PTCP.PST-3SG-INSTR</ta>
            <ta e="T356" id="Seg_7162" s="T355">schlafen-CVB.SEQ</ta>
            <ta e="T357" id="Seg_7163" s="T356">bleiben-PST2-3PL</ta>
            <ta e="T358" id="Seg_7164" s="T357">dieses</ta>
            <ta e="T359" id="Seg_7165" s="T358">Kind.[NOM]</ta>
            <ta e="T360" id="Seg_7166" s="T359">aufwachen-CVB.SEQ</ta>
            <ta e="T361" id="Seg_7167" s="T360">nachdem</ta>
            <ta e="T362" id="Seg_7168" s="T361">zuhören-CVB.SIM</ta>
            <ta e="T363" id="Seg_7169" s="T362">liegen-PST2.[3SG]</ta>
            <ta e="T364" id="Seg_7170" s="T363">fern.[NOM]</ta>
            <ta e="T365" id="Seg_7171" s="T364">wo</ta>
            <ta e="T366" id="Seg_7172" s="T365">INDEF</ta>
            <ta e="T367" id="Seg_7173" s="T366">wieder</ta>
            <ta e="T368" id="Seg_7174" s="T367">schreiten-PTCP.PRS</ta>
            <ta e="T369" id="Seg_7175" s="T368">Geräusch.[NOM]</ta>
            <ta e="T370" id="Seg_7176" s="T369">gehört.werden-EP-PST2.[3SG]</ta>
            <ta e="T371" id="Seg_7177" s="T370">Freund-PL-3SG-ACC</ta>
            <ta e="T372" id="Seg_7178" s="T371">aufwachen-CAUS-PST2-3SG</ta>
            <ta e="T373" id="Seg_7179" s="T372">denn</ta>
            <ta e="T374" id="Seg_7180" s="T373">aufwachen-FUT-3PL=Q</ta>
            <ta e="T375" id="Seg_7181" s="T374">bald</ta>
            <ta e="T376" id="Seg_7182" s="T375">einschlafen-PTCP.PST</ta>
            <ta e="T377" id="Seg_7183" s="T376">Mensch-PL.[NOM]</ta>
            <ta e="T378" id="Seg_7184" s="T377">Hund.[NOM]</ta>
            <ta e="T379" id="Seg_7185" s="T378">Sohn-3SG.[NOM]</ta>
            <ta e="T380" id="Seg_7186" s="T379">hinausgehen-EP-PST2.[3SG]</ta>
            <ta e="T381" id="Seg_7187" s="T380">und</ta>
            <ta e="T382" id="Seg_7188" s="T381">zwei-ORD</ta>
            <ta e="T383" id="Seg_7189" s="T382">böser.Geist.[NOM]</ta>
            <ta e="T384" id="Seg_7190" s="T383">Kopf-3SG-ACC</ta>
            <ta e="T385" id="Seg_7191" s="T384">auch</ta>
            <ta e="T386" id="Seg_7192" s="T385">großer.Stein.[NOM]</ta>
            <ta e="T387" id="Seg_7193" s="T386">Stein-DAT/LOC</ta>
            <ta e="T388" id="Seg_7194" s="T387">entzwei</ta>
            <ta e="T389" id="Seg_7195" s="T388">schlagen-EP-PST2.[3SG]</ta>
            <ta e="T390" id="Seg_7196" s="T389">böser.Geist.[NOM]</ta>
            <ta e="T391" id="Seg_7197" s="T390">mit.den.Beinen.zappeln-CVB.SIM</ta>
            <ta e="T392" id="Seg_7198" s="T391">fallen-CVB.SEQ</ta>
            <ta e="T393" id="Seg_7199" s="T392">nachdem</ta>
            <ta e="T394" id="Seg_7200" s="T393">sich.strecken-NMNZ.[NOM]</ta>
            <ta e="T395" id="Seg_7201" s="T394">machen-PST2.[3SG]</ta>
            <ta e="T396" id="Seg_7202" s="T395">Hund.[NOM]</ta>
            <ta e="T397" id="Seg_7203" s="T396">Sohn-3SG.[NOM]</ta>
            <ta e="T398" id="Seg_7204" s="T397">Schädel-3SG-ABL</ta>
            <ta e="T399" id="Seg_7205" s="T398">Schrift-PROPR</ta>
            <ta e="T400" id="Seg_7206" s="T399">Stein-ACC</ta>
            <ta e="T401" id="Seg_7207" s="T400">herausholen-PST1-3SG</ta>
            <ta e="T402" id="Seg_7208" s="T401">EMPH</ta>
            <ta e="T403" id="Seg_7209" s="T402">Tasche-3SG-DAT/LOC</ta>
            <ta e="T404" id="Seg_7210" s="T403">auch</ta>
            <ta e="T405" id="Seg_7211" s="T404">hineintun-CVB.SEQ</ta>
            <ta e="T406" id="Seg_7212" s="T405">werfen-PST2.[3SG]</ta>
            <ta e="T407" id="Seg_7213" s="T406">drei-ORD</ta>
            <ta e="T408" id="Seg_7214" s="T407">Nacht-3SG-DAT/LOC</ta>
            <ta e="T409" id="Seg_7215" s="T408">drei-ORD</ta>
            <ta e="T410" id="Seg_7216" s="T409">böser.Geist.[NOM]</ta>
            <ta e="T411" id="Seg_7217" s="T410">Kopf-3SG-ACC</ta>
            <ta e="T412" id="Seg_7218" s="T411">spalten-CVB.SIM</ta>
            <ta e="T413" id="Seg_7219" s="T412">schlagen-CVB.SEQ</ta>
            <ta e="T414" id="Seg_7220" s="T413">Schrift-PROPR</ta>
            <ta e="T415" id="Seg_7221" s="T414">Stein-ACC</ta>
            <ta e="T416" id="Seg_7222" s="T415">herausholen-CVB.SIM</ta>
            <ta e="T417" id="Seg_7223" s="T416">schlagen-EP-PST2.[3SG]</ta>
            <ta e="T418" id="Seg_7224" s="T417">Freund-PL-3SG-ACC</ta>
            <ta e="T419" id="Seg_7225" s="T418">Morgenrot.[NOM]</ta>
            <ta e="T420" id="Seg_7226" s="T419">aufstehen-PTCP.PRS-3SG-ACC</ta>
            <ta e="T421" id="Seg_7227" s="T420">mit</ta>
            <ta e="T422" id="Seg_7228" s="T421">aufwachen-CAUS-PST2.[3SG]</ta>
            <ta e="T423" id="Seg_7229" s="T422">doch</ta>
            <ta e="T424" id="Seg_7230" s="T423">Haus-1PL-DAT/LOC</ta>
            <ta e="T425" id="Seg_7231" s="T424">gehen-IMP.1PL</ta>
            <ta e="T426" id="Seg_7232" s="T425">sagen-PST2.[3SG]</ta>
            <ta e="T427" id="Seg_7233" s="T426">dieses</ta>
            <ta e="T428" id="Seg_7234" s="T427">Junge.[NOM]</ta>
            <ta e="T429" id="Seg_7235" s="T428">Zar.[NOM]</ta>
            <ta e="T430" id="Seg_7236" s="T429">Sohn-3SG.[NOM]</ta>
            <ta e="T431" id="Seg_7237" s="T430">sich.sträuben-PST2.[3SG]</ta>
            <ta e="T432" id="Seg_7238" s="T431">warum</ta>
            <ta e="T433" id="Seg_7239" s="T432">nun</ta>
            <ta e="T434" id="Seg_7240" s="T433">leben-CVB.SIM</ta>
            <ta e="T435" id="Seg_7241" s="T434">fallen-IMP.1PL</ta>
            <ta e="T436" id="Seg_7242" s="T435">fröhlich</ta>
            <ta e="T437" id="Seg_7243" s="T436">Kuh.[NOM]</ta>
            <ta e="T438" id="Seg_7244" s="T437">Sohn-3SG.[NOM]</ta>
            <ta e="T439" id="Seg_7245" s="T438">sitzen-CVB.SIM</ta>
            <ta e="T440" id="Seg_7246" s="T439">fallen-CVB.SEQ</ta>
            <ta e="T441" id="Seg_7247" s="T440">nachdem</ta>
            <ta e="T442" id="Seg_7248" s="T441">sagen-PST2.[3SG]</ta>
            <ta e="T443" id="Seg_7249" s="T442">Familie-PL-1PL.[NOM]</ta>
            <ta e="T444" id="Seg_7250" s="T443">warten-FUT-3PL</ta>
            <ta e="T445" id="Seg_7251" s="T444">gehen-IMP.1PL</ta>
            <ta e="T446" id="Seg_7252" s="T445">EVID</ta>
            <ta e="T447" id="Seg_7253" s="T446">Junge-PL.[NOM]</ta>
            <ta e="T448" id="Seg_7254" s="T447">sich.auf.den.Weg.machen-PST2-3PL</ta>
            <ta e="T449" id="Seg_7255" s="T448">Hund.[NOM]</ta>
            <ta e="T450" id="Seg_7256" s="T449">Sohn-3SG.[NOM]</ta>
            <ta e="T451" id="Seg_7257" s="T450">rudern-CVB.SEQ</ta>
            <ta e="T452" id="Seg_7258" s="T451">gehen-PST2.[3SG]</ta>
            <ta e="T453" id="Seg_7259" s="T452">wie.viel</ta>
            <ta e="T454" id="Seg_7260" s="T453">NEG</ta>
            <ta e="T455" id="Seg_7261" s="T454">sein-NEG.CVB.SIM</ta>
            <ta e="T456" id="Seg_7262" s="T455">Scheide-3SG-ACC</ta>
            <ta e="T457" id="Seg_7263" s="T456">ergreifen-CVB.SEQ</ta>
            <ta e="T458" id="Seg_7264" s="T457">gehen-CVB.SEQ</ta>
            <ta e="T459" id="Seg_7265" s="T458">Freund-PL.[NOM]</ta>
            <ta e="T460" id="Seg_7266" s="T459">Messer-1SG-ACC</ta>
            <ta e="T461" id="Seg_7267" s="T460">bleiben-CAUS-PST2-1SG</ta>
            <ta e="T462" id="Seg_7268" s="T461">suchen-CVB.SIM</ta>
            <ta e="T463" id="Seg_7269" s="T462">gehen-FUT-1SG</ta>
            <ta e="T464" id="Seg_7270" s="T463">sagen-PST2.[3SG]</ta>
            <ta e="T465" id="Seg_7271" s="T464">Hund.[NOM]</ta>
            <ta e="T466" id="Seg_7272" s="T465">Sohn-3SG.[NOM]</ta>
            <ta e="T467" id="Seg_7273" s="T466">Jurte-DAT/LOC</ta>
            <ta e="T468" id="Seg_7274" s="T467">kommen-PST2-3SG</ta>
            <ta e="T469" id="Seg_7275" s="T468">Frau-PL.[NOM]</ta>
            <ta e="T470" id="Seg_7276" s="T469">weinen-PTCP.PRS</ta>
            <ta e="T471" id="Seg_7277" s="T470">Stimme-3PL.[NOM]</ta>
            <ta e="T472" id="Seg_7278" s="T471">gehört.werden-EP-PST2.[3SG]</ta>
            <ta e="T473" id="Seg_7279" s="T472">1SG.[NOM]</ta>
            <ta e="T474" id="Seg_7280" s="T473">schmackhaft</ta>
            <ta e="T475" id="Seg_7281" s="T474">Nahrung.[NOM]</ta>
            <ta e="T476" id="Seg_7282" s="T475">werden-CVB.SEQ</ta>
            <ta e="T477" id="Seg_7283" s="T476">nachdem</ta>
            <ta e="T478" id="Seg_7284" s="T477">groß</ta>
            <ta e="T479" id="Seg_7285" s="T478">Teller-DAT/LOC</ta>
            <ta e="T480" id="Seg_7286" s="T479">kleines.Boot-3PL-GEN</ta>
            <ta e="T481" id="Seg_7287" s="T480">Inneres-3SG-DAT/LOC</ta>
            <ta e="T482" id="Seg_7288" s="T481">fallen-FUT-1SG</ta>
            <ta e="T483" id="Seg_7289" s="T482">1SG-ACC</ta>
            <ta e="T484" id="Seg_7290" s="T483">essen-TEMP-3PL</ta>
            <ta e="T485" id="Seg_7291" s="T484">sterben-CVB.SEQ</ta>
            <ta e="T486" id="Seg_7292" s="T485">bleiben-FUT-3PL</ta>
            <ta e="T487" id="Seg_7293" s="T486">sagen-PST2.[3SG]</ta>
            <ta e="T488" id="Seg_7294" s="T487">alt</ta>
            <ta e="T489" id="Seg_7295" s="T488">Baba.Jaga.[NOM]</ta>
            <ta e="T490" id="Seg_7296" s="T489">Stimme-3SG.[NOM]</ta>
            <ta e="T491" id="Seg_7297" s="T490">1SG.[NOM]</ta>
            <ta e="T492" id="Seg_7298" s="T491">aber</ta>
            <ta e="T493" id="Seg_7299" s="T492">wann</ta>
            <ta e="T494" id="Seg_7300" s="T493">NEG</ta>
            <ta e="T495" id="Seg_7301" s="T494">sehen-NEG.PTCP.PST</ta>
            <ta e="T496" id="Seg_7302" s="T495">schön</ta>
            <ta e="T497" id="Seg_7303" s="T496">Seide.[NOM]</ta>
            <ta e="T498" id="Seg_7304" s="T497">Tuch.[NOM]</ta>
            <ta e="T499" id="Seg_7305" s="T498">werden-CVB.SEQ</ta>
            <ta e="T500" id="Seg_7306" s="T499">Zar.[NOM]</ta>
            <ta e="T501" id="Seg_7307" s="T500">Sohn-3SG-GEN</ta>
            <ta e="T502" id="Seg_7308" s="T501">Knie-3SG-DAT/LOC</ta>
            <ta e="T503" id="Seg_7309" s="T502">fallen-FUT-1SG</ta>
            <ta e="T504" id="Seg_7310" s="T503">jeder-INTNS-3PL-ACC</ta>
            <ta e="T505" id="Seg_7311" s="T504">sich.aufhängen-CAUS-CVB.SEQ</ta>
            <ta e="T506" id="Seg_7312" s="T505">töten-FUT-1SG</ta>
            <ta e="T507" id="Seg_7313" s="T506">1SG.[NOM]</ta>
            <ta e="T508" id="Seg_7314" s="T507">aber</ta>
            <ta e="T509" id="Seg_7315" s="T508">kleines.Boot-3PL-ACC</ta>
            <ta e="T510" id="Seg_7316" s="T509">eng</ta>
            <ta e="T511" id="Seg_7317" s="T510">kleben-PTCP.PRS</ta>
            <ta e="T512" id="Seg_7318" s="T511">machen-CVB.SIM</ta>
            <ta e="T513" id="Seg_7319" s="T512">flüssig</ta>
            <ta e="T514" id="Seg_7320" s="T513">Teer.[NOM]</ta>
            <ta e="T515" id="Seg_7321" s="T514">werden-FUT-1SG</ta>
            <ta e="T516" id="Seg_7322" s="T515">jeder-3PL-ACC</ta>
            <ta e="T517" id="Seg_7323" s="T516">heranziehen-CVB.SEQ</ta>
            <ta e="T518" id="Seg_7324" s="T517">sinken-CAUS-FUT-1SG</ta>
            <ta e="T519" id="Seg_7325" s="T518">doch</ta>
            <ta e="T520" id="Seg_7326" s="T519">gut.[NOM]</ta>
            <ta e="T521" id="Seg_7327" s="T520">hervorragend</ta>
            <ta e="T522" id="Seg_7328" s="T521">denken-PST2-2PL</ta>
            <ta e="T523" id="Seg_7329" s="T522">sein-PST2.[3SG]</ta>
            <ta e="T524" id="Seg_7330" s="T523">sagen-PST2.[3SG]</ta>
            <ta e="T525" id="Seg_7331" s="T524">erster</ta>
            <ta e="T526" id="Seg_7332" s="T525">Frau.[NOM]</ta>
            <ta e="T527" id="Seg_7333" s="T526">Hund.[NOM]</ta>
            <ta e="T528" id="Seg_7334" s="T527">Sohn-3SG.[NOM]</ta>
            <ta e="T529" id="Seg_7335" s="T528">dieses-ACC</ta>
            <ta e="T530" id="Seg_7336" s="T529">hören-EP-PST2.[3SG]</ta>
            <ta e="T531" id="Seg_7337" s="T530">EMPH</ta>
            <ta e="T532" id="Seg_7338" s="T531">hohes.Ufer.[NOM]</ta>
            <ta e="T533" id="Seg_7339" s="T532">Unterteil-3SG-DAT/LOC</ta>
            <ta e="T534" id="Seg_7340" s="T533">laufen-CVB.SEQ</ta>
            <ta e="T535" id="Seg_7341" s="T534">ankommen-PST2.[3SG]</ta>
            <ta e="T536" id="Seg_7342" s="T535">kleines.Boot-3SG-ACC</ta>
            <ta e="T537" id="Seg_7343" s="T536">nehmen-INCH-PRS.[3SG]</ta>
            <ta e="T538" id="Seg_7344" s="T537">und</ta>
            <ta e="T539" id="Seg_7345" s="T538">rudern-CVB.SEQ</ta>
            <ta e="T540" id="Seg_7346" s="T539">gehen-PST2.[3SG]</ta>
            <ta e="T541" id="Seg_7347" s="T540">Freund-PL-3SG-ACC</ta>
            <ta e="T542" id="Seg_7348" s="T541">Fluss.[NOM]</ta>
            <ta e="T543" id="Seg_7349" s="T542">Mitte-3SG-DAT/LOC</ta>
            <ta e="T544" id="Seg_7350" s="T543">verfolgen-CVB.SIM</ta>
            <ta e="T545" id="Seg_7351" s="T544">schlagen-EP-PST2.[3SG]</ta>
            <ta e="T546" id="Seg_7352" s="T545">Junge-PL.[NOM]</ta>
            <ta e="T547" id="Seg_7353" s="T546">ganz.leise</ta>
            <ta e="T548" id="Seg_7354" s="T547">rudern-CVB.SEQ</ta>
            <ta e="T549" id="Seg_7355" s="T548">gehen-PST2-3PL</ta>
            <ta e="T550" id="Seg_7356" s="T549">wie.viel</ta>
            <ta e="T551" id="Seg_7357" s="T550">lange</ta>
            <ta e="T552" id="Seg_7358" s="T551">sein-FUT.[3SG]=Q</ta>
            <ta e="T553" id="Seg_7359" s="T552">kleines.Boot-3PL-GEN</ta>
            <ta e="T554" id="Seg_7360" s="T553">Inneres-3SG-DAT/LOC</ta>
            <ta e="T555" id="Seg_7361" s="T554">Nahrung-PROPR</ta>
            <ta e="T556" id="Seg_7362" s="T555">Teller.[NOM]</ta>
            <ta e="T557" id="Seg_7363" s="T556">fallen-PST2.[3SG]</ta>
            <ta e="T558" id="Seg_7364" s="T557">Zar.[NOM]</ta>
            <ta e="T559" id="Seg_7365" s="T558">Sohn-3SG.[NOM]</ta>
            <ta e="T560" id="Seg_7366" s="T559">sich.freuen-PST2.[3SG]</ta>
            <ta e="T561" id="Seg_7367" s="T560">Hand-3SG-ACC</ta>
            <ta e="T562" id="Seg_7368" s="T561">Teller.[NOM]</ta>
            <ta e="T563" id="Seg_7369" s="T562">zu</ta>
            <ta e="T564" id="Seg_7370" s="T563">spreizen-NMNZ.[NOM]</ta>
            <ta e="T565" id="Seg_7371" s="T564">machen-PTCP.PRS-3SG-ACC</ta>
            <ta e="T566" id="Seg_7372" s="T565">mit</ta>
            <ta e="T567" id="Seg_7373" s="T566">Hund.[NOM]</ta>
            <ta e="T568" id="Seg_7374" s="T567">Sohn-3SG.[NOM]</ta>
            <ta e="T569" id="Seg_7375" s="T568">Nahrung-PROPR</ta>
            <ta e="T570" id="Seg_7376" s="T569">Teller-ACC</ta>
            <ta e="T571" id="Seg_7377" s="T570">Wasser-DAT/LOC</ta>
            <ta e="T572" id="Seg_7378" s="T571">werfen-CVB.SEQ</ta>
            <ta e="T573" id="Seg_7379" s="T572">werfen-PST2.[3SG]</ta>
            <ta e="T574" id="Seg_7380" s="T573">Zar.[NOM]</ta>
            <ta e="T575" id="Seg_7381" s="T574">Sohn-3SG.[NOM]</ta>
            <ta e="T576" id="Seg_7382" s="T575">sich.ärgern-CVB.SEQ</ta>
            <ta e="T577" id="Seg_7383" s="T576">beleidigt.dasitzen-CVB.SEQ</ta>
            <ta e="T578" id="Seg_7384" s="T577">sitzen-TEMP-3SG</ta>
            <ta e="T579" id="Seg_7385" s="T578">schön</ta>
            <ta e="T580" id="Seg_7386" s="T579">sehr</ta>
            <ta e="T581" id="Seg_7387" s="T580">Seide.[NOM]</ta>
            <ta e="T582" id="Seg_7388" s="T581">Tuch.[NOM]</ta>
            <ta e="T583" id="Seg_7389" s="T582">Zar.[NOM]</ta>
            <ta e="T584" id="Seg_7390" s="T583">Sohn-3SG-GEN</ta>
            <ta e="T585" id="Seg_7391" s="T584">Knie-3SG-DAT/LOC</ta>
            <ta e="T586" id="Seg_7392" s="T585">fallen-PST2.[3SG]</ta>
            <ta e="T587" id="Seg_7393" s="T586">oh</ta>
            <ta e="T588" id="Seg_7394" s="T587">Gott.[NOM]</ta>
            <ta e="T589" id="Seg_7395" s="T588">Mama-1SG-DAT/LOC</ta>
            <ta e="T590" id="Seg_7396" s="T589">Geschenk.[NOM]</ta>
            <ta e="T591" id="Seg_7397" s="T590">machen-FUT-1SG</ta>
            <ta e="T592" id="Seg_7398" s="T591">sagen-PST2.[3SG]</ta>
            <ta e="T593" id="Seg_7399" s="T592">und</ta>
            <ta e="T594" id="Seg_7400" s="T593">Tasche-3SG-DAT/LOC</ta>
            <ta e="T595" id="Seg_7401" s="T594">hineintun-CVB.PURP</ta>
            <ta e="T596" id="Seg_7402" s="T595">wollen-PTCP.PRS-3SG-ACC</ta>
            <ta e="T597" id="Seg_7403" s="T596">mit</ta>
            <ta e="T598" id="Seg_7404" s="T597">Sohn-3SG.[NOM]</ta>
            <ta e="T599" id="Seg_7405" s="T598">wählen-CVB.SEQ</ta>
            <ta e="T600" id="Seg_7406" s="T599">nehmen-PST2.[3SG]</ta>
            <ta e="T601" id="Seg_7407" s="T600">Messer-3SG-INSTR</ta>
            <ta e="T602" id="Seg_7408" s="T601">viel</ta>
            <ta e="T603" id="Seg_7409" s="T602">zerschneiden-CVB.SEQ</ta>
            <ta e="T604" id="Seg_7410" s="T603">nachdem</ta>
            <ta e="T605" id="Seg_7411" s="T604">Wasser-DAT/LOC</ta>
            <ta e="T606" id="Seg_7412" s="T605">werfen-CVB.SEQ</ta>
            <ta e="T607" id="Seg_7413" s="T606">werfen-PST2.[3SG]</ta>
            <ta e="T608" id="Seg_7414" s="T607">Zar.[NOM]</ta>
            <ta e="T609" id="Seg_7415" s="T608">Sohn-3SG.[NOM]</ta>
            <ta e="T610" id="Seg_7416" s="T609">sich.ärgern-CVB.SEQ</ta>
            <ta e="T611" id="Seg_7417" s="T610">Stimme-3SG.[NOM]</ta>
            <ta e="T612" id="Seg_7418" s="T611">NEG</ta>
            <ta e="T613" id="Seg_7419" s="T612">erscheinen-EP-PST2.NEG.[3SG]</ta>
            <ta e="T614" id="Seg_7420" s="T613">wie.viel</ta>
            <ta e="T615" id="Seg_7421" s="T614">lange</ta>
            <ta e="T616" id="Seg_7422" s="T615">sein-FUT.[3SG]=Q</ta>
            <ta e="T617" id="Seg_7423" s="T616">kleines.Boot-3PL.[NOM]</ta>
            <ta e="T618" id="Seg_7424" s="T617">Wasser-DAT/LOC</ta>
            <ta e="T619" id="Seg_7425" s="T618">haften-CVB.SEQ</ta>
            <ta e="T620" id="Seg_7426" s="T619">bleiben-PST2.[3SG]</ta>
            <ta e="T621" id="Seg_7427" s="T620">Fluss.[NOM]</ta>
            <ta e="T622" id="Seg_7428" s="T621">Wasser-3SG.[NOM]</ta>
            <ta e="T623" id="Seg_7429" s="T622">schwarz</ta>
            <ta e="T624" id="Seg_7430" s="T623">machen-PST2.[3SG]</ta>
            <ta e="T625" id="Seg_7431" s="T624">dieses</ta>
            <ta e="T626" id="Seg_7432" s="T625">Junge-PL.[NOM]</ta>
            <ta e="T627" id="Seg_7433" s="T626">können-CVB.SEQ</ta>
            <ta e="T628" id="Seg_7434" s="T627">rudern-EP-MED-NEG.PTCP</ta>
            <ta e="T629" id="Seg_7435" s="T628">sein-CVB.SEQ</ta>
            <ta e="T630" id="Seg_7436" s="T629">bleiben-PST2-3PL</ta>
            <ta e="T631" id="Seg_7437" s="T630">Angst.haben-PST2-3PL</ta>
            <ta e="T632" id="Seg_7438" s="T631">Hund.[NOM]</ta>
            <ta e="T633" id="Seg_7439" s="T632">Sohn-3SG.[NOM]</ta>
            <ta e="T634" id="Seg_7440" s="T633">Wasser-ACC</ta>
            <ta e="T635" id="Seg_7441" s="T634">Beil-INSTR</ta>
            <ta e="T636" id="Seg_7442" s="T635">schneiden-CVB.SIM</ta>
            <ta e="T637" id="Seg_7443" s="T636">zerschneiden-EP-IMP.2PL</ta>
            <ta e="T638" id="Seg_7444" s="T637">Messer-3SG-INSTR</ta>
            <ta e="T639" id="Seg_7445" s="T638">durchstechen-EP-CAUS-CVB.SIM</ta>
            <ta e="T640" id="Seg_7446" s="T639">stoßen-EP-IMP.2PL</ta>
            <ta e="T641" id="Seg_7447" s="T640">1SG.[NOM]</ta>
            <ta e="T642" id="Seg_7448" s="T641">rudern-CVB.SEQ</ta>
            <ta e="T643" id="Seg_7449" s="T642">gehen-FUT-1SG</ta>
            <ta e="T644" id="Seg_7450" s="T643">sagen-PST2.[3SG]</ta>
            <ta e="T645" id="Seg_7451" s="T644">Freund-PL-3SG-DAT/LOC</ta>
            <ta e="T646" id="Seg_7452" s="T645">sehen-PST2-3PL</ta>
            <ta e="T647" id="Seg_7453" s="T646">Wasser-3PL.[NOM]</ta>
            <ta e="T648" id="Seg_7454" s="T647">tatsächlich</ta>
            <ta e="T649" id="Seg_7455" s="T648">hell.werden-CVB.SEQ</ta>
            <ta e="T650" id="Seg_7456" s="T649">rot.werden-CVB.SEQ</ta>
            <ta e="T651" id="Seg_7457" s="T650">gehen-PST2.[3SG]</ta>
            <ta e="T652" id="Seg_7458" s="T651">bald</ta>
            <ta e="T653" id="Seg_7459" s="T652">kleines.Boot-3PL.[NOM]</ta>
            <ta e="T654" id="Seg_7460" s="T653">befreien-EP-REFL-PST2.[3SG]</ta>
            <ta e="T655" id="Seg_7461" s="T654">Junge-PL.[NOM]</ta>
            <ta e="T656" id="Seg_7462" s="T655">rudern-EP-MED-CVB.SEQ</ta>
            <ta e="T657" id="Seg_7463" s="T656">gehen-PST2-3PL</ta>
            <ta e="T658" id="Seg_7464" s="T657">ganz.leise</ta>
            <ta e="T659" id="Seg_7465" s="T658">wie.viel</ta>
            <ta e="T660" id="Seg_7466" s="T659">NEG</ta>
            <ta e="T661" id="Seg_7467" s="T660">lange</ta>
            <ta e="T662" id="Seg_7468" s="T661">sein-NEG.CVB.SIM</ta>
            <ta e="T663" id="Seg_7469" s="T662">Fluss.[NOM]</ta>
            <ta e="T664" id="Seg_7470" s="T663">hoch</ta>
            <ta e="T665" id="Seg_7471" s="T664">Ufer-3SG-DAT/LOC</ta>
            <ta e="T666" id="Seg_7472" s="T665">Stadt-3PL.[NOM]</ta>
            <ta e="T667" id="Seg_7473" s="T666">zu.sehen.sein-EP-PST2.[3SG]</ta>
            <ta e="T668" id="Seg_7474" s="T667">herankommen-CVB.SEQ</ta>
            <ta e="T669" id="Seg_7475" s="T668">nachdem</ta>
            <ta e="T670" id="Seg_7476" s="T669">Junge-PL.[NOM]</ta>
            <ta e="T671" id="Seg_7477" s="T670">drei</ta>
            <ta e="T672" id="Seg_7478" s="T671">Richtung.[NOM]</ta>
            <ta e="T673" id="Seg_7479" s="T672">sich.trennen-CVB.SEQ</ta>
            <ta e="T674" id="Seg_7480" s="T673">bleiben-PST2-3PL</ta>
            <ta e="T675" id="Seg_7481" s="T674">Zar.[NOM]</ta>
            <ta e="T676" id="Seg_7482" s="T675">Sohn-3SG.[NOM]</ta>
            <ta e="T677" id="Seg_7483" s="T676">hohes.Ufer.[NOM]</ta>
            <ta e="T678" id="Seg_7484" s="T677">oberer.Teil-3SG-DAT/LOC</ta>
            <ta e="T679" id="Seg_7485" s="T678">weiß</ta>
            <ta e="T680" id="Seg_7486" s="T679">Haus-DAT/LOC</ta>
            <ta e="T681" id="Seg_7487" s="T680">gehen-PST2.[3SG]</ta>
            <ta e="T682" id="Seg_7488" s="T681">Hund.[NOM]</ta>
            <ta e="T683" id="Seg_7489" s="T682">Sohn-3SG.[NOM]</ta>
            <ta e="T684" id="Seg_7490" s="T683">Mama-3SG-GEN</ta>
            <ta e="T685" id="Seg_7491" s="T684">niedrig</ta>
            <ta e="T686" id="Seg_7492" s="T685">Höhle.[NOM]</ta>
            <ta e="T687" id="Seg_7493" s="T686">Haus-3SG-DAT/LOC</ta>
            <ta e="T688" id="Seg_7494" s="T687">hineingehen-PST2.[3SG]</ta>
            <ta e="T689" id="Seg_7495" s="T688">Kuh.[NOM]</ta>
            <ta e="T690" id="Seg_7496" s="T689">Sohn-3SG.[NOM]</ta>
            <ta e="T691" id="Seg_7497" s="T690">draußen</ta>
            <ta e="T692" id="Seg_7498" s="T691">Mutter-3SG-ACC</ta>
            <ta e="T693" id="Seg_7499" s="T692">treffen-EP-PST2.[3SG]</ta>
            <ta e="T694" id="Seg_7500" s="T693">am.Morgen</ta>
            <ta e="T695" id="Seg_7501" s="T694">Zar.[NOM]</ta>
            <ta e="T696" id="Seg_7502" s="T695">Junge-PL-ACC</ta>
            <ta e="T697" id="Seg_7503" s="T696">rufen-MED-FREQ-PST2.[3SG]</ta>
            <ta e="T698" id="Seg_7504" s="T697">doch</ta>
            <ta e="T699" id="Seg_7505" s="T698">Kind-PL-EP-1SG.[NOM]</ta>
            <ta e="T700" id="Seg_7506" s="T699">erzählen-IMP.2PL</ta>
            <ta e="T701" id="Seg_7507" s="T700">was-ACC</ta>
            <ta e="T702" id="Seg_7508" s="T701">machen-CVB.SIM</ta>
            <ta e="T703" id="Seg_7509" s="T702">dieses</ta>
            <ta e="T704" id="Seg_7510" s="T703">so.lange</ta>
            <ta e="T705" id="Seg_7511" s="T704">gehen-EP-PST2-2PL=Q</ta>
            <ta e="T706" id="Seg_7512" s="T705">drei</ta>
            <ta e="T707" id="Seg_7513" s="T706">böser.Geist.[NOM]</ta>
            <ta e="T708" id="Seg_7514" s="T707">Schädel-3SG-ABL</ta>
            <ta e="T709" id="Seg_7515" s="T708">drei</ta>
            <ta e="T710" id="Seg_7516" s="T709">Schrift-PROPR</ta>
            <ta e="T711" id="Seg_7517" s="T710">Stein-ACC</ta>
            <ta e="T712" id="Seg_7518" s="T711">finden-PST2-EP-1SG</ta>
            <ta e="T713" id="Seg_7519" s="T712">Hund.[NOM]</ta>
            <ta e="T714" id="Seg_7520" s="T713">Sohn-3SG.[NOM]</ta>
            <ta e="T715" id="Seg_7521" s="T714">sagen-PST2.[3SG]</ta>
            <ta e="T716" id="Seg_7522" s="T715">Zar.[NOM]</ta>
            <ta e="T717" id="Seg_7523" s="T716">Hand-3SG-DAT/LOC</ta>
            <ta e="T718" id="Seg_7524" s="T717">drei</ta>
            <ta e="T719" id="Seg_7525" s="T718">Stein-ACC</ta>
            <ta e="T720" id="Seg_7526" s="T719">überreichen-CVB.SEQ</ta>
            <ta e="T721" id="Seg_7527" s="T720">werfen-PST2.[3SG]</ta>
            <ta e="T722" id="Seg_7528" s="T721">Zar.[NOM]</ta>
            <ta e="T723" id="Seg_7529" s="T722">Hund.[NOM]</ta>
            <ta e="T724" id="Seg_7530" s="T723">Sohn-3SG-ACC</ta>
            <ta e="T725" id="Seg_7531" s="T724">Hals-3SG-ABL</ta>
            <ta e="T726" id="Seg_7532" s="T725">umarmen-CVB.SEQ</ta>
            <ta e="T727" id="Seg_7533" s="T726">nehmen-PST2.[3SG]</ta>
            <ta e="T728" id="Seg_7534" s="T727">weinen-EP-MOM-CVB.SIM</ta>
            <ta e="T729" id="Seg_7535" s="T728">fallen-CVB.SEQ</ta>
            <ta e="T730" id="Seg_7536" s="T729">nachdem</ta>
            <ta e="T731" id="Seg_7537" s="T730">sagen-PST2.[3SG]</ta>
            <ta e="T732" id="Seg_7538" s="T731">heutig-ABL</ta>
            <ta e="T733" id="Seg_7539" s="T732">2SG.[NOM]</ta>
            <ta e="T734" id="Seg_7540" s="T733">Zar.[NOM]</ta>
            <ta e="T735" id="Seg_7541" s="T734">sein-PTCP.PRS</ta>
            <ta e="T736" id="Seg_7542" s="T735">Bestimmung-EP-2SG.[NOM]</ta>
            <ta e="T737" id="Seg_7543" s="T736">kommen-PST1-3SG</ta>
            <ta e="T738" id="Seg_7544" s="T737">Sohn-1SG-ACC</ta>
            <ta e="T739" id="Seg_7545" s="T738">mit</ta>
            <ta e="T740" id="Seg_7546" s="T739">älterer.Bruder.[NOM]-jüngerer.Bruder.[NOM]</ta>
            <ta e="T741" id="Seg_7547" s="T740">sein-CVB.SEQ</ta>
            <ta e="T742" id="Seg_7548" s="T741">leben-EP-IMP.2PL</ta>
            <ta e="T743" id="Seg_7549" s="T742">drei</ta>
            <ta e="T744" id="Seg_7550" s="T743">Junge.[NOM]</ta>
            <ta e="T745" id="Seg_7551" s="T744">Freund-SIM</ta>
            <ta e="T746" id="Seg_7552" s="T745">reich.sein-CVB.SEQ-reich.sein-CVB.SEQ</ta>
            <ta e="T747" id="Seg_7553" s="T746">leben-PST2-3PL</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_7554" s="T0">давно-давно</ta>
            <ta e="T2" id="Seg_7555" s="T1">один</ta>
            <ta e="T3" id="Seg_7556" s="T2">царь.[NOM]</ta>
            <ta e="T4" id="Seg_7557" s="T3">жить-PST2.[3SG]</ta>
            <ta e="T5" id="Seg_7558" s="T4">ребенок-POSS</ta>
            <ta e="T6" id="Seg_7559" s="T5">NEG</ta>
            <ta e="T7" id="Seg_7560" s="T6">быть-PST1-3SG</ta>
            <ta e="T8" id="Seg_7561" s="T7">быть-PST2.[3SG]</ta>
            <ta e="T9" id="Seg_7562" s="T8">однажды</ta>
            <ta e="T10" id="Seg_7563" s="T9">этот</ta>
            <ta e="T11" id="Seg_7564" s="T10">царь.[NOM]</ta>
            <ta e="T12" id="Seg_7565" s="T11">думать-PST2.[3SG]</ta>
            <ta e="T13" id="Seg_7566" s="T12">этот</ta>
            <ta e="T14" id="Seg_7567" s="T13">почему</ta>
            <ta e="T15" id="Seg_7568" s="T14">1PL.[NOM]</ta>
            <ta e="T16" id="Seg_7569" s="T15">ребенок-POSS</ta>
            <ta e="T17" id="Seg_7570" s="T16">NEG-1PL=Q</ta>
            <ta e="T18" id="Seg_7571" s="T17">старый-PL-ABL</ta>
            <ta e="T19" id="Seg_7572" s="T18">спрашивать-PTCP.PST.[NOM]</ta>
            <ta e="T20" id="Seg_7573" s="T19">MOD</ta>
            <ta e="T21" id="Seg_7574" s="T20">этот</ta>
            <ta e="T22" id="Seg_7575" s="T21">думать-CVB.SIM</ta>
            <ta e="T23" id="Seg_7576" s="T22">сидеть-TEMP-3SG</ta>
            <ta e="T24" id="Seg_7577" s="T23">жена-3SG.[NOM]</ta>
            <ta e="T25" id="Seg_7578" s="T24">входить-PST2.[3SG]</ta>
            <ta e="T26" id="Seg_7579" s="T25">что-ACC</ta>
            <ta e="T27" id="Seg_7580" s="T26">задуматься-PST1-2SG</ta>
            <ta e="T28" id="Seg_7581" s="T27">друг</ta>
            <ta e="T29" id="Seg_7582" s="T28">завтра</ta>
            <ta e="T30" id="Seg_7583" s="T29">1PL.[NOM]</ta>
            <ta e="T31" id="Seg_7584" s="T30">человек-PL-ACC</ta>
            <ta e="T32" id="Seg_7585" s="T31">собирать-FUT-1PL</ta>
            <ta e="T33" id="Seg_7586" s="T32">какой-DAT/LOC</ta>
            <ta e="T34" id="Seg_7587" s="T33">пока</ta>
            <ta e="T35" id="Seg_7588" s="T34">пустой</ta>
            <ta e="T36" id="Seg_7589" s="T35">стена-ACC</ta>
            <ta e="T37" id="Seg_7590" s="T36">видеть-CVB.SEQ</ta>
            <ta e="T38" id="Seg_7591" s="T37">жить-FUT-1PL=Q</ta>
            <ta e="T39" id="Seg_7592" s="T38">ребенок-POSS</ta>
            <ta e="T40" id="Seg_7593" s="T39">NEG</ta>
            <ta e="T41" id="Seg_7594" s="T40">спрашивать-IMP.1DU</ta>
            <ta e="T42" id="Seg_7595" s="T41">почему</ta>
            <ta e="T43" id="Seg_7596" s="T42">ребенок-POSS</ta>
            <ta e="T44" id="Seg_7597" s="T43">NEG-1PL=Q</ta>
            <ta e="T45" id="Seg_7598" s="T44">кто-DAT/LOC</ta>
            <ta e="T46" id="Seg_7599" s="T45">богатство-1PL-ACC</ta>
            <ta e="T47" id="Seg_7600" s="T46">давать-FUT-1PL=Q</ta>
            <ta e="T48" id="Seg_7601" s="T47">умирать-TEMP-1PL</ta>
            <ta e="T49" id="Seg_7602" s="T48">утром</ta>
            <ta e="T50" id="Seg_7603" s="T49">вставать-PST2-3PL</ta>
            <ta e="T51" id="Seg_7604" s="T50">много</ta>
            <ta e="T52" id="Seg_7605" s="T51">очень</ta>
            <ta e="T53" id="Seg_7606" s="T52">человек.[NOM]</ta>
            <ta e="T54" id="Seg_7607" s="T53">собирать-PASS/REFL-EP-PST2.[3SG]</ta>
            <ta e="T55" id="Seg_7608" s="T54">там</ta>
            <ta e="T56" id="Seg_7609" s="T55">царь.[NOM]</ta>
            <ta e="T57" id="Seg_7610" s="T56">спрашивать-PST2.[3SG]</ta>
            <ta e="T58" id="Seg_7611" s="T57">этот</ta>
            <ta e="T59" id="Seg_7612" s="T58">почему</ta>
            <ta e="T60" id="Seg_7613" s="T59">1PL.[NOM]</ta>
            <ta e="T61" id="Seg_7614" s="T60">ребенок-POSS</ta>
            <ta e="T62" id="Seg_7615" s="T61">NEG-1PL=Q</ta>
            <ta e="T63" id="Seg_7616" s="T62">как</ta>
            <ta e="T64" id="Seg_7617" s="T63">что-ACC</ta>
            <ta e="T65" id="Seg_7618" s="T64">делать-PTCP.COND-DAT/LOC</ta>
            <ta e="T66" id="Seg_7619" s="T65">ребенок-VBZ-PTCP.FUT-1PL=Q</ta>
            <ta e="T67" id="Seg_7620" s="T66">2PL.[NOM]</ta>
            <ta e="T68" id="Seg_7621" s="T67">как</ta>
            <ta e="T69" id="Seg_7622" s="T68">думать-CVB.SIM</ta>
            <ta e="T70" id="Seg_7623" s="T69">думать-PRS-2PL</ta>
            <ta e="T71" id="Seg_7624" s="T70">человек-PL.[NOM]</ta>
            <ta e="T72" id="Seg_7625" s="T71">шуметь-CVB.SIM</ta>
            <ta e="T73" id="Seg_7626" s="T72">падать-PST2-3PL</ta>
            <ta e="T74" id="Seg_7627" s="T73">дверь-3SG-INSTR</ta>
            <ta e="T75" id="Seg_7628" s="T74">старый</ta>
            <ta e="T76" id="Seg_7629" s="T75">кривой</ta>
            <ta e="T77" id="Seg_7630" s="T76">старик.[NOM]</ta>
            <ta e="T78" id="Seg_7631" s="T77">высовываться-NMNZ.[NOM]</ta>
            <ta e="T79" id="Seg_7632" s="T78">делать-PST2.[3SG]</ta>
            <ta e="T80" id="Seg_7633" s="T79">кривой</ta>
            <ta e="T81" id="Seg_7634" s="T80">мука-PROPR-ACC</ta>
            <ta e="T82" id="Seg_7635" s="T81">середина-DAT/LOC</ta>
            <ta e="T83" id="Seg_7636" s="T82">толкать-CVB.SEQ</ta>
            <ta e="T84" id="Seg_7637" s="T83">бросать-PST2-3PL</ta>
            <ta e="T85" id="Seg_7638" s="T84">этот</ta>
            <ta e="T86" id="Seg_7639" s="T85">есть</ta>
            <ta e="T87" id="Seg_7640" s="T86">знать-PTCP.PRS</ta>
            <ta e="T88" id="Seg_7641" s="T87">человек.[NOM]</ta>
            <ta e="T89" id="Seg_7642" s="T88">старик.[NOM]</ta>
            <ta e="T90" id="Seg_7643" s="T89">вверх</ta>
            <ta e="T91" id="Seg_7644" s="T90">видеть-CVB.SEQ</ta>
            <ta e="T92" id="Seg_7645" s="T91">после</ta>
            <ta e="T93" id="Seg_7646" s="T92">спина.[NOM]</ta>
            <ta e="T94" id="Seg_7647" s="T93">поймать-CVB.SEQ</ta>
            <ta e="T95" id="Seg_7648" s="T94">после</ta>
            <ta e="T96" id="Seg_7649" s="T95">говорить-PST2.[3SG]</ta>
            <ta e="T97" id="Seg_7650" s="T96">делать-EP-IMP.2PL</ta>
            <ta e="T98" id="Seg_7651" s="T97">золото.[NOM]</ta>
            <ta e="T99" id="Seg_7652" s="T98">сеть-PART</ta>
            <ta e="T100" id="Seg_7653" s="T99">золото.[NOM]</ta>
            <ta e="T101" id="Seg_7654" s="T100">лодочка-PART</ta>
            <ta e="T102" id="Seg_7655" s="T101">золото.[NOM]</ta>
            <ta e="T103" id="Seg_7656" s="T102">весло-PART</ta>
            <ta e="T104" id="Seg_7657" s="T103">потом</ta>
            <ta e="T105" id="Seg_7658" s="T104">царь.[NOM]</ta>
            <ta e="T106" id="Seg_7659" s="T105">рыбачить-CVB.SIM</ta>
            <ta e="T107" id="Seg_7660" s="T106">идти-IMP.3SG</ta>
            <ta e="T108" id="Seg_7661" s="T107">сеть-3SG-DAT/LOC</ta>
            <ta e="T109" id="Seg_7662" s="T108">рыба.[NOM]</ta>
            <ta e="T110" id="Seg_7663" s="T109">держать-FUT.[3SG]</ta>
            <ta e="T111" id="Seg_7664" s="T110">тот-ACC</ta>
            <ta e="T112" id="Seg_7665" s="T111">жена-3SG.[NOM]</ta>
            <ta e="T113" id="Seg_7666" s="T112">есть-IMP.3SG</ta>
            <ta e="T114" id="Seg_7667" s="T113">царь.[NOM]</ta>
            <ta e="T115" id="Seg_7668" s="T114">жена-3SG.[NOM]</ta>
            <ta e="T116" id="Seg_7669" s="T115">смеяться-PST2.[3SG]</ta>
            <ta e="T117" id="Seg_7670" s="T116">тот-ABL</ta>
            <ta e="T118" id="Seg_7671" s="T117">1SG.[NOM]</ta>
            <ta e="T119" id="Seg_7672" s="T118">наесться-FUT-1SG</ta>
            <ta e="T120" id="Seg_7673" s="T119">Q</ta>
            <ta e="T121" id="Seg_7674" s="T120">долго</ta>
            <ta e="T122" id="Seg_7675" s="T121">быть-FUT.[3SG]=Q</ta>
            <ta e="T123" id="Seg_7676" s="T122">царь.[NOM]</ta>
            <ta e="T124" id="Seg_7677" s="T123">следующее.утро-3SG-ACC</ta>
            <ta e="T125" id="Seg_7678" s="T124">золото.[NOM]</ta>
            <ta e="T126" id="Seg_7679" s="T125">лодочка-PROPR</ta>
            <ta e="T127" id="Seg_7680" s="T126">золото.[NOM]</ta>
            <ta e="T128" id="Seg_7681" s="T127">весло-PROPR</ta>
            <ta e="T129" id="Seg_7682" s="T128">рыба-VBZ-CVB.SIM</ta>
            <ta e="T130" id="Seg_7683" s="T129">идти-PST2.[3SG]</ta>
            <ta e="T131" id="Seg_7684" s="T130">один</ta>
            <ta e="T132" id="Seg_7685" s="T131">рыба-ACC</ta>
            <ta e="T133" id="Seg_7686" s="T132">трепетать-CVB.SEQ</ta>
            <ta e="T134" id="Seg_7687" s="T133">живой-SIM</ta>
            <ta e="T135" id="Seg_7688" s="T134">принести-NEG.[3SG]</ta>
            <ta e="T136" id="Seg_7689" s="T135">MOD</ta>
            <ta e="T137" id="Seg_7690" s="T136">этот.[NOM]</ta>
            <ta e="T138" id="Seg_7691" s="T137">добыча-EP-1SG.[NOM]</ta>
            <ta e="T139" id="Seg_7692" s="T138">говорить-PST2.[3SG]</ta>
            <ta e="T140" id="Seg_7693" s="T139">да</ta>
            <ta e="T141" id="Seg_7694" s="T140">повар-3PL-DAT/LOC</ta>
            <ta e="T142" id="Seg_7695" s="T141">давать-CVB.SEQ</ta>
            <ta e="T143" id="Seg_7696" s="T142">бросать-PST2.[3SG]</ta>
            <ta e="T144" id="Seg_7697" s="T143">немедленно</ta>
            <ta e="T145" id="Seg_7698" s="T144">варить-IMP.2PL</ta>
            <ta e="T146" id="Seg_7699" s="T145">повар.[NOM]</ta>
            <ta e="T147" id="Seg_7700" s="T146">рыба.[NOM]</ta>
            <ta e="T148" id="Seg_7701" s="T147">чешуя-3SG-ACC</ta>
            <ta e="T149" id="Seg_7702" s="T148">трава.[NOM]</ta>
            <ta e="T150" id="Seg_7703" s="T149">нутро-3SG-DAT/LOC</ta>
            <ta e="T151" id="Seg_7704" s="T150">очистить-CVB.SEQ</ta>
            <ta e="T152" id="Seg_7705" s="T151">бросать-PST2.[3SG]</ta>
            <ta e="T153" id="Seg_7706" s="T152">тот-ACC</ta>
            <ta e="T154" id="Seg_7707" s="T153">царь.[NOM]</ta>
            <ta e="T155" id="Seg_7708" s="T154">корова-3SG.[NOM]</ta>
            <ta e="T156" id="Seg_7709" s="T155">есть-CVB.SEQ</ta>
            <ta e="T157" id="Seg_7710" s="T156">бросать-PST2.[3SG]</ta>
            <ta e="T158" id="Seg_7711" s="T157">царь.[NOM]</ta>
            <ta e="T159" id="Seg_7712" s="T158">жена-3SG.[NOM]</ta>
            <ta e="T160" id="Seg_7713" s="T159">рыба-ACC</ta>
            <ta e="T161" id="Seg_7714" s="T160">есть-CVB.SEQ</ta>
            <ta e="T162" id="Seg_7715" s="T161">после</ta>
            <ta e="T163" id="Seg_7716" s="T162">кость-3SG-ACC</ta>
            <ta e="T164" id="Seg_7717" s="T163">тарелка-DAT/LOC</ta>
            <ta e="T165" id="Seg_7718" s="T164">оставаться-CAUS-PTCP.PST-3SG-ACC</ta>
            <ta e="T166" id="Seg_7719" s="T165">повар.[NOM]</ta>
            <ta e="T167" id="Seg_7720" s="T166">собака-DAT/LOC</ta>
            <ta e="T168" id="Seg_7721" s="T167">бросать-PST2.[3SG]</ta>
            <ta e="T169" id="Seg_7722" s="T168">сказка-DAT/LOC</ta>
            <ta e="T170" id="Seg_7723" s="T169">долго</ta>
            <ta e="T171" id="Seg_7724" s="T170">становиться-FUT.[3SG]</ta>
            <ta e="T172" id="Seg_7725" s="T171">Q</ta>
            <ta e="T173" id="Seg_7726" s="T172">три-COLL-3PL.[NOM]</ta>
            <ta e="T174" id="Seg_7727" s="T173">три</ta>
            <ta e="T175" id="Seg_7728" s="T174">мальчик-ACC</ta>
            <ta e="T176" id="Seg_7729" s="T175">родить-PST2-3PL</ta>
            <ta e="T177" id="Seg_7730" s="T176">ребенок-PL.[NOM]</ta>
            <ta e="T178" id="Seg_7731" s="T177">сколько</ta>
            <ta e="T179" id="Seg_7732" s="T178">NEG</ta>
            <ta e="T180" id="Seg_7733" s="T179">быть-NEG.CVB.SIM</ta>
            <ta e="T181" id="Seg_7734" s="T180">вместе</ta>
            <ta e="T182" id="Seg_7735" s="T181">играть-PTCP.PRS</ta>
            <ta e="T183" id="Seg_7736" s="T182">становиться-PST2-3PL</ta>
            <ta e="T184" id="Seg_7737" s="T183">однажды</ta>
            <ta e="T185" id="Seg_7738" s="T184">только</ta>
            <ta e="T186" id="Seg_7739" s="T185">царь.[NOM]</ta>
            <ta e="T187" id="Seg_7740" s="T186">сын-3SG.[NOM]</ta>
            <ta e="T188" id="Seg_7741" s="T187">говорить-PST2.[3SG]</ta>
            <ta e="T189" id="Seg_7742" s="T188">друг-PL.[NOM]</ta>
            <ta e="T190" id="Seg_7743" s="T189">лодочка-VBZ-CVB.SIM</ta>
            <ta e="T191" id="Seg_7744" s="T190">идти-IMP.1PL</ta>
            <ta e="T192" id="Seg_7745" s="T191">мальчик-PL.[NOM]</ta>
            <ta e="T193" id="Seg_7746" s="T192">сколько</ta>
            <ta e="T194" id="Seg_7747" s="T193">долго</ta>
            <ta e="T195" id="Seg_7748" s="T194">грести-CVB.SEQ</ta>
            <ta e="T196" id="Seg_7749" s="T195">идти-PST2-3PL</ta>
            <ta e="T197" id="Seg_7750" s="T196">MOD</ta>
            <ta e="T198" id="Seg_7751" s="T197">видеть-PST2-3PL</ta>
            <ta e="T199" id="Seg_7752" s="T198">река.[NOM]</ta>
            <ta e="T200" id="Seg_7753" s="T199">на.том.берегу</ta>
            <ta e="T201" id="Seg_7754" s="T200">большой</ta>
            <ta e="T202" id="Seg_7755" s="T201">очень</ta>
            <ta e="T203" id="Seg_7756" s="T202">юрта.[NOM]</ta>
            <ta e="T204" id="Seg_7757" s="T203">дом.[NOM]</ta>
            <ta e="T205" id="Seg_7758" s="T204">стоять-PRS.[3SG]</ta>
            <ta e="T206" id="Seg_7759" s="T205">тот.[NOM]</ta>
            <ta e="T207" id="Seg_7760" s="T206">что-1PL.[NOM]=Q</ta>
            <ta e="T208" id="Seg_7761" s="T207">говорить-EP-RECP/COLL-PST2-3PL</ta>
            <ta e="T209" id="Seg_7762" s="T208">царь.[NOM]</ta>
            <ta e="T210" id="Seg_7763" s="T209">сын-3SG.[NOM]</ta>
            <ta e="T211" id="Seg_7764" s="T210">приставать-IMP.1PL</ta>
            <ta e="T212" id="Seg_7765" s="T211">карты-VBZ-FUT-1PL</ta>
            <ta e="T213" id="Seg_7766" s="T212">говорить-PST2.[3SG]</ta>
            <ta e="T214" id="Seg_7767" s="T213">собака.[NOM]</ta>
            <ta e="T215" id="Seg_7768" s="T214">сын-3SG.[NOM]</ta>
            <ta e="T216" id="Seg_7769" s="T215">ээ</ta>
            <ta e="T217" id="Seg_7770" s="T216">почему</ta>
            <ta e="T218" id="Seg_7771" s="T217">можно</ta>
            <ta e="T219" id="Seg_7772" s="T218">злой.дух-PL.[NOM]</ta>
            <ta e="T220" id="Seg_7773" s="T219">дом-3PL.[NOM]</ta>
            <ta e="T221" id="Seg_7774" s="T220">быть-FUT.[3SG]</ta>
            <ta e="T222" id="Seg_7775" s="T221">корова.[NOM]</ta>
            <ta e="T223" id="Seg_7776" s="T222">сын-3SG.[NOM]</ta>
            <ta e="T224" id="Seg_7777" s="T223">приставать-IMP.1PL</ta>
            <ta e="T225" id="Seg_7778" s="T224">приставать-IMP.1PL</ta>
            <ta e="T226" id="Seg_7779" s="T225">идти-FREQ-CVB.SIM</ta>
            <ta e="T227" id="Seg_7780" s="T226">падать-IMP.1PL</ta>
            <ta e="T228" id="Seg_7781" s="T227">берег-DAT/LOC</ta>
            <ta e="T229" id="Seg_7782" s="T228">выйти-CVB.ANT</ta>
            <ta e="T230" id="Seg_7783" s="T229">этот</ta>
            <ta e="T231" id="Seg_7784" s="T230">два</ta>
            <ta e="T232" id="Seg_7785" s="T231">мальчик.[NOM]</ta>
            <ta e="T233" id="Seg_7786" s="T232">карты-VBZ-CVB.SIM</ta>
            <ta e="T234" id="Seg_7787" s="T233">сесть-PST2-3PL</ta>
            <ta e="T235" id="Seg_7788" s="T234">собака.[NOM]</ta>
            <ta e="T236" id="Seg_7789" s="T235">сын-3SG.[NOM]</ta>
            <ta e="T237" id="Seg_7790" s="T236">спать-CVB.SEQ</ta>
            <ta e="T238" id="Seg_7791" s="T237">оставаться-PST2.[3SG]</ta>
            <ta e="T239" id="Seg_7792" s="T238">сколько</ta>
            <ta e="T240" id="Seg_7793" s="T239">долго</ta>
            <ta e="T241" id="Seg_7794" s="T240">играть-PST2-3PL</ta>
            <ta e="T242" id="Seg_7795" s="T241">MOD</ta>
            <ta e="T243" id="Seg_7796" s="T242">этот</ta>
            <ta e="T244" id="Seg_7797" s="T243">ребенок-PL.[NOM]</ta>
            <ta e="T245" id="Seg_7798" s="T244">тоже</ta>
            <ta e="T246" id="Seg_7799" s="T245">спать-CVB.SEQ</ta>
            <ta e="T247" id="Seg_7800" s="T246">оставаться-PST2-3PL</ta>
            <ta e="T248" id="Seg_7801" s="T247">собака.[NOM]</ta>
            <ta e="T249" id="Seg_7802" s="T248">сын-3SG.[NOM]</ta>
            <ta e="T250" id="Seg_7803" s="T249">ночь.[NOM]</ta>
            <ta e="T251" id="Seg_7804" s="T250">середина.[NOM]</ta>
            <ta e="T252" id="Seg_7805" s="T251">просыпаться-EP-PST2-3SG</ta>
            <ta e="T253" id="Seg_7806" s="T252">человек.[NOM]</ta>
            <ta e="T254" id="Seg_7807" s="T253">шагать-PTCP.PRS</ta>
            <ta e="T255" id="Seg_7808" s="T254">звук-3SG.[NOM]</ta>
            <ta e="T256" id="Seg_7809" s="T255">слышаться-EP-PST2.[3SG]</ta>
            <ta e="T257" id="Seg_7810" s="T256">пояляться-EP-PST2-3SG</ta>
            <ta e="T258" id="Seg_7811" s="T257">злой.дух.[NOM]</ta>
            <ta e="T259" id="Seg_7812" s="T258">юрта.[NOM]</ta>
            <ta e="T260" id="Seg_7813" s="T259">к</ta>
            <ta e="T261" id="Seg_7814" s="T260">идти-CVB.SEQ</ta>
            <ta e="T262" id="Seg_7815" s="T261">идти-EMOT-PRS.[3SG]</ta>
            <ta e="T263" id="Seg_7816" s="T262">вот</ta>
            <ta e="T264" id="Seg_7817" s="T263">о</ta>
            <ta e="T265" id="Seg_7818" s="T264">вот</ta>
            <ta e="T266" id="Seg_7819" s="T265">умирать-PST2-1PL</ta>
            <ta e="T267" id="Seg_7820" s="T266">быть-PST2.[3SG]</ta>
            <ta e="T268" id="Seg_7821" s="T267">говорить-CVB.SEQ</ta>
            <ta e="T269" id="Seg_7822" s="T268">после</ta>
            <ta e="T270" id="Seg_7823" s="T269">друг-PL-3SG-ACC</ta>
            <ta e="T271" id="Seg_7824" s="T270">просыпаться-CAUS-CVB.SIM</ta>
            <ta e="T272" id="Seg_7825" s="T271">делать.напрасно-PST2.[3SG]</ta>
            <ta e="T273" id="Seg_7826" s="T272">этот</ta>
            <ta e="T274" id="Seg_7827" s="T273">мальчик-PL.[NOM]</ta>
            <ta e="T275" id="Seg_7828" s="T274">разве</ta>
            <ta e="T276" id="Seg_7829" s="T275">просыпаться-FUT-3PL=Q</ta>
            <ta e="T277" id="Seg_7830" s="T276">приходить.[IMP.2SG]-приходить.[IMP.2SG]</ta>
            <ta e="T278" id="Seg_7831" s="T277">1SG.[NOM]</ta>
            <ta e="T279" id="Seg_7832" s="T278">2SG-ACC</ta>
            <ta e="T280" id="Seg_7833" s="T279">есть-FUT-1SG</ta>
            <ta e="T281" id="Seg_7834" s="T280">злой.дух.[NOM]</ta>
            <ta e="T282" id="Seg_7835" s="T281">голос-3SG.[NOM]</ta>
            <ta e="T283" id="Seg_7836" s="T282">слышаться-EP-PST2.[3SG]</ta>
            <ta e="T284" id="Seg_7837" s="T283">этот</ta>
            <ta e="T285" id="Seg_7838" s="T284">мальчик.[NOM]</ta>
            <ta e="T286" id="Seg_7839" s="T285">выйти-CVB.SIM</ta>
            <ta e="T287" id="Seg_7840" s="T286">подниматься-PST2.[3SG]</ta>
            <ta e="T288" id="Seg_7841" s="T287">да</ta>
            <ta e="T289" id="Seg_7842" s="T288">злой.дух-ACC</ta>
            <ta e="T290" id="Seg_7843" s="T289">с</ta>
            <ta e="T291" id="Seg_7844" s="T290">побить-EP-PST2.[3SG]</ta>
            <ta e="T292" id="Seg_7845" s="T291">сколько</ta>
            <ta e="T293" id="Seg_7846" s="T292">NEG</ta>
            <ta e="T294" id="Seg_7847" s="T293">быть-NEG.CVB.SIM</ta>
            <ta e="T295" id="Seg_7848" s="T294">злой.дух.[NOM]</ta>
            <ta e="T296" id="Seg_7849" s="T295">голова-3SG-ACC</ta>
            <ta e="T297" id="Seg_7850" s="T296">валун.[NOM]</ta>
            <ta e="T298" id="Seg_7851" s="T297">камень-DAT/LOC</ta>
            <ta e="T299" id="Seg_7852" s="T298">колоть-CVB.SIM</ta>
            <ta e="T300" id="Seg_7853" s="T299">бить-EP-PST2.[3SG]</ta>
            <ta e="T301" id="Seg_7854" s="T300">тот</ta>
            <ta e="T302" id="Seg_7855" s="T301">делать-CVB.SEQ</ta>
            <ta e="T303" id="Seg_7856" s="T302">после</ta>
            <ta e="T304" id="Seg_7857" s="T303">череп-3SG-GEN</ta>
            <ta e="T305" id="Seg_7858" s="T304">нутро-3SG-ABL</ta>
            <ta e="T306" id="Seg_7859" s="T305">письмо-PROPR</ta>
            <ta e="T307" id="Seg_7860" s="T306">камень-ACC</ta>
            <ta e="T308" id="Seg_7861" s="T307">вытащить-PST2.[3SG]</ta>
            <ta e="T309" id="Seg_7862" s="T308">камень-ACC</ta>
            <ta e="T310" id="Seg_7863" s="T309">карман-3SG-GEN</ta>
            <ta e="T311" id="Seg_7864" s="T310">нутро-3SG-INSTR</ta>
            <ta e="T312" id="Seg_7865" s="T311">вкладывать-CVB.SEQ</ta>
            <ta e="T313" id="Seg_7866" s="T312">бросать-PST2.[3SG]</ta>
            <ta e="T314" id="Seg_7867" s="T313">утром</ta>
            <ta e="T315" id="Seg_7868" s="T314">что.[NOM]</ta>
            <ta e="T316" id="Seg_7869" s="T315">NEG</ta>
            <ta e="T317" id="Seg_7870" s="T316">быть-NEG.PTCP.PST.[NOM]</ta>
            <ta e="T318" id="Seg_7871" s="T317">подобно</ta>
            <ta e="T319" id="Seg_7872" s="T318">собака.[NOM]</ta>
            <ta e="T320" id="Seg_7873" s="T319">сын-3SG.[NOM]</ta>
            <ta e="T321" id="Seg_7874" s="T320">друг-PL-3SG-ACC</ta>
            <ta e="T322" id="Seg_7875" s="T321">просыпаться-CAUS-ITER-PST2.[3SG]</ta>
            <ta e="T323" id="Seg_7876" s="T322">ну</ta>
            <ta e="T324" id="Seg_7877" s="T323">гусь-VBZ-CVB.SIM</ta>
            <ta e="T325" id="Seg_7878" s="T324">идти-IMP.1PL</ta>
            <ta e="T326" id="Seg_7879" s="T325">мальчик-PL.[NOM]</ta>
            <ta e="T327" id="Seg_7880" s="T326">потягиваться-CVB.SEQ</ta>
            <ta e="T328" id="Seg_7881" s="T327">еле</ta>
            <ta e="T329" id="Seg_7882" s="T328">стоять-CVB.SEQ</ta>
            <ta e="T330" id="Seg_7883" s="T329">чай.[NOM]</ta>
            <ta e="T331" id="Seg_7884" s="T330">пить-PST2-3PL</ta>
            <ta e="T332" id="Seg_7885" s="T331">сказка-DAT/LOC</ta>
            <ta e="T333" id="Seg_7886" s="T332">день.[NOM]</ta>
            <ta e="T334" id="Seg_7887" s="T333">короткий.[NOM]</ta>
            <ta e="T335" id="Seg_7888" s="T334">сколько</ta>
            <ta e="T336" id="Seg_7889" s="T335">NEG</ta>
            <ta e="T337" id="Seg_7890" s="T336">быть-NEG.CVB.SIM</ta>
            <ta e="T338" id="Seg_7891" s="T337">небо.[NOM]</ta>
            <ta e="T339" id="Seg_7892" s="T338">темнеть-PST2.[3SG]</ta>
            <ta e="T340" id="Seg_7893" s="T339">собака.[NOM]</ta>
            <ta e="T341" id="Seg_7894" s="T340">сын-3SG.[NOM]</ta>
            <ta e="T342" id="Seg_7895" s="T341">есть-CVB.SEQ-есть-CVB.SEQ</ta>
            <ta e="T343" id="Seg_7896" s="T342">после</ta>
            <ta e="T344" id="Seg_7897" s="T343">спать-CVB.SEQ</ta>
            <ta e="T345" id="Seg_7898" s="T344">оставаться-PST2.[3SG]</ta>
            <ta e="T346" id="Seg_7899" s="T345">друг-PL-3SG.[NOM]</ta>
            <ta e="T347" id="Seg_7900" s="T346">опять</ta>
            <ta e="T348" id="Seg_7901" s="T347">карты-VBZ-CVB.SIM</ta>
            <ta e="T349" id="Seg_7902" s="T348">сесть-PST2-3PL</ta>
            <ta e="T350" id="Seg_7903" s="T349">ночь.[NOM]</ta>
            <ta e="T351" id="Seg_7904" s="T350">середина.[NOM]</ta>
            <ta e="T352" id="Seg_7905" s="T351">просыпаться-EP-PST2-3SG</ta>
            <ta e="T353" id="Seg_7906" s="T352">друг-PL-3SG.[NOM]</ta>
            <ta e="T354" id="Seg_7907" s="T353">посадить-CVB.SIM</ta>
            <ta e="T355" id="Seg_7908" s="T354">сесть-PTCP.PST-3SG-INSTR</ta>
            <ta e="T356" id="Seg_7909" s="T355">спать-CVB.SEQ</ta>
            <ta e="T357" id="Seg_7910" s="T356">оставаться-PST2-3PL</ta>
            <ta e="T358" id="Seg_7911" s="T357">этот</ta>
            <ta e="T359" id="Seg_7912" s="T358">ребенок.[NOM]</ta>
            <ta e="T360" id="Seg_7913" s="T359">просыпаться-CVB.SEQ</ta>
            <ta e="T361" id="Seg_7914" s="T360">после</ta>
            <ta e="T362" id="Seg_7915" s="T361">слушать-CVB.SIM</ta>
            <ta e="T363" id="Seg_7916" s="T362">лежать-PST2.[3SG]</ta>
            <ta e="T364" id="Seg_7917" s="T363">далекий.[NOM]</ta>
            <ta e="T365" id="Seg_7918" s="T364">куда</ta>
            <ta e="T366" id="Seg_7919" s="T365">INDEF</ta>
            <ta e="T367" id="Seg_7920" s="T366">опять</ta>
            <ta e="T368" id="Seg_7921" s="T367">шагать-PTCP.PRS</ta>
            <ta e="T369" id="Seg_7922" s="T368">звук.[NOM]</ta>
            <ta e="T370" id="Seg_7923" s="T369">слышаться-EP-PST2.[3SG]</ta>
            <ta e="T371" id="Seg_7924" s="T370">друг-PL-3SG-ACC</ta>
            <ta e="T372" id="Seg_7925" s="T371">просыпаться-CAUS-PST2-3SG</ta>
            <ta e="T373" id="Seg_7926" s="T372">разве</ta>
            <ta e="T374" id="Seg_7927" s="T373">просыпаться-FUT-3PL=Q</ta>
            <ta e="T375" id="Seg_7928" s="T374">скоро</ta>
            <ta e="T376" id="Seg_7929" s="T375">уснуть-PTCP.PST</ta>
            <ta e="T377" id="Seg_7930" s="T376">человек-PL.[NOM]</ta>
            <ta e="T378" id="Seg_7931" s="T377">собака.[NOM]</ta>
            <ta e="T379" id="Seg_7932" s="T378">сын-3SG.[NOM]</ta>
            <ta e="T380" id="Seg_7933" s="T379">выйти-EP-PST2.[3SG]</ta>
            <ta e="T381" id="Seg_7934" s="T380">да</ta>
            <ta e="T382" id="Seg_7935" s="T381">два-ORD</ta>
            <ta e="T383" id="Seg_7936" s="T382">злой.дух.[NOM]</ta>
            <ta e="T384" id="Seg_7937" s="T383">голова-3SG-ACC</ta>
            <ta e="T385" id="Seg_7938" s="T384">тоже</ta>
            <ta e="T386" id="Seg_7939" s="T385">валун.[NOM]</ta>
            <ta e="T387" id="Seg_7940" s="T386">камень-DAT/LOC</ta>
            <ta e="T388" id="Seg_7941" s="T387">врозь</ta>
            <ta e="T389" id="Seg_7942" s="T388">бить-EP-PST2.[3SG]</ta>
            <ta e="T390" id="Seg_7943" s="T389">злой.дух.[NOM]</ta>
            <ta e="T391" id="Seg_7944" s="T390">дернуть.ногами-CVB.SIM</ta>
            <ta e="T392" id="Seg_7945" s="T391">падать-CVB.SEQ</ta>
            <ta e="T393" id="Seg_7946" s="T392">после</ta>
            <ta e="T394" id="Seg_7947" s="T393">вытянуться-NMNZ.[NOM]</ta>
            <ta e="T395" id="Seg_7948" s="T394">делать-PST2.[3SG]</ta>
            <ta e="T396" id="Seg_7949" s="T395">собака.[NOM]</ta>
            <ta e="T397" id="Seg_7950" s="T396">сын-3SG.[NOM]</ta>
            <ta e="T398" id="Seg_7951" s="T397">череп-3SG-ABL</ta>
            <ta e="T399" id="Seg_7952" s="T398">письмо-PROPR</ta>
            <ta e="T400" id="Seg_7953" s="T399">камень-ACC</ta>
            <ta e="T401" id="Seg_7954" s="T400">вытащить-PST1-3SG</ta>
            <ta e="T402" id="Seg_7955" s="T401">EMPH</ta>
            <ta e="T403" id="Seg_7956" s="T402">карман-3SG-DAT/LOC</ta>
            <ta e="T404" id="Seg_7957" s="T403">тоже</ta>
            <ta e="T405" id="Seg_7958" s="T404">вкладывать-CVB.SEQ</ta>
            <ta e="T406" id="Seg_7959" s="T405">бросать-PST2.[3SG]</ta>
            <ta e="T407" id="Seg_7960" s="T406">три-ORD</ta>
            <ta e="T408" id="Seg_7961" s="T407">ночь-3SG-DAT/LOC</ta>
            <ta e="T409" id="Seg_7962" s="T408">три-ORD</ta>
            <ta e="T410" id="Seg_7963" s="T409">злой.дух.[NOM]</ta>
            <ta e="T411" id="Seg_7964" s="T410">голова-3SG-ACC</ta>
            <ta e="T412" id="Seg_7965" s="T411">колоть-CVB.SIM</ta>
            <ta e="T413" id="Seg_7966" s="T412">бить-CVB.SEQ</ta>
            <ta e="T414" id="Seg_7967" s="T413">письмо-PROPR</ta>
            <ta e="T415" id="Seg_7968" s="T414">камень-ACC</ta>
            <ta e="T416" id="Seg_7969" s="T415">вытащить-CVB.SIM</ta>
            <ta e="T417" id="Seg_7970" s="T416">бить-EP-PST2.[3SG]</ta>
            <ta e="T418" id="Seg_7971" s="T417">друг-PL-3SG-ACC</ta>
            <ta e="T419" id="Seg_7972" s="T418">заря.[NOM]</ta>
            <ta e="T420" id="Seg_7973" s="T419">вставать-PTCP.PRS-3SG-ACC</ta>
            <ta e="T421" id="Seg_7974" s="T420">с</ta>
            <ta e="T422" id="Seg_7975" s="T421">просыпаться-CAUS-PST2.[3SG]</ta>
            <ta e="T423" id="Seg_7976" s="T422">вот</ta>
            <ta e="T424" id="Seg_7977" s="T423">дом-1PL-DAT/LOC</ta>
            <ta e="T425" id="Seg_7978" s="T424">идти-IMP.1PL</ta>
            <ta e="T426" id="Seg_7979" s="T425">говорить-PST2.[3SG]</ta>
            <ta e="T427" id="Seg_7980" s="T426">этот</ta>
            <ta e="T428" id="Seg_7981" s="T427">мальчик.[NOM]</ta>
            <ta e="T429" id="Seg_7982" s="T428">царь.[NOM]</ta>
            <ta e="T430" id="Seg_7983" s="T429">сын-3SG.[NOM]</ta>
            <ta e="T431" id="Seg_7984" s="T430">упрямиться-PST2.[3SG]</ta>
            <ta e="T432" id="Seg_7985" s="T431">почему</ta>
            <ta e="T433" id="Seg_7986" s="T432">ну</ta>
            <ta e="T434" id="Seg_7987" s="T433">жить-CVB.SIM</ta>
            <ta e="T435" id="Seg_7988" s="T434">падать-IMP.1PL</ta>
            <ta e="T436" id="Seg_7989" s="T435">весело</ta>
            <ta e="T437" id="Seg_7990" s="T436">корова.[NOM]</ta>
            <ta e="T438" id="Seg_7991" s="T437">сын-3SG.[NOM]</ta>
            <ta e="T439" id="Seg_7992" s="T438">сидеть-CVB.SIM</ta>
            <ta e="T440" id="Seg_7993" s="T439">падать-CVB.SEQ</ta>
            <ta e="T441" id="Seg_7994" s="T440">после</ta>
            <ta e="T442" id="Seg_7995" s="T441">говорить-PST2.[3SG]</ta>
            <ta e="T443" id="Seg_7996" s="T442">семья-PL-1PL.[NOM]</ta>
            <ta e="T444" id="Seg_7997" s="T443">ждать-FUT-3PL</ta>
            <ta e="T445" id="Seg_7998" s="T444">идти-IMP.1PL</ta>
            <ta e="T446" id="Seg_7999" s="T445">EVID</ta>
            <ta e="T447" id="Seg_8000" s="T446">мальчик-PL.[NOM]</ta>
            <ta e="T448" id="Seg_8001" s="T447">отправляться-PST2-3PL</ta>
            <ta e="T449" id="Seg_8002" s="T448">собака.[NOM]</ta>
            <ta e="T450" id="Seg_8003" s="T449">сын-3SG.[NOM]</ta>
            <ta e="T451" id="Seg_8004" s="T450">грести-CVB.SEQ</ta>
            <ta e="T452" id="Seg_8005" s="T451">идти-PST2.[3SG]</ta>
            <ta e="T453" id="Seg_8006" s="T452">сколько</ta>
            <ta e="T454" id="Seg_8007" s="T453">NEG</ta>
            <ta e="T455" id="Seg_8008" s="T454">быть-NEG.CVB.SIM</ta>
            <ta e="T456" id="Seg_8009" s="T455">ножны-3SG-ACC</ta>
            <ta e="T457" id="Seg_8010" s="T456">поймать-CVB.SEQ</ta>
            <ta e="T458" id="Seg_8011" s="T457">идти-CVB.SEQ</ta>
            <ta e="T459" id="Seg_8012" s="T458">друг-PL.[NOM]</ta>
            <ta e="T460" id="Seg_8013" s="T459">нож-1SG-ACC</ta>
            <ta e="T461" id="Seg_8014" s="T460">оставаться-CAUS-PST2-1SG</ta>
            <ta e="T462" id="Seg_8015" s="T461">искать-CVB.SIM</ta>
            <ta e="T463" id="Seg_8016" s="T462">идти-FUT-1SG</ta>
            <ta e="T464" id="Seg_8017" s="T463">говорить-PST2.[3SG]</ta>
            <ta e="T465" id="Seg_8018" s="T464">собака.[NOM]</ta>
            <ta e="T466" id="Seg_8019" s="T465">сын-3SG.[NOM]</ta>
            <ta e="T467" id="Seg_8020" s="T466">юрта-DAT/LOC</ta>
            <ta e="T468" id="Seg_8021" s="T467">приходить-PST2-3SG</ta>
            <ta e="T469" id="Seg_8022" s="T468">жена-PL.[NOM]</ta>
            <ta e="T470" id="Seg_8023" s="T469">плакать-PTCP.PRS</ta>
            <ta e="T471" id="Seg_8024" s="T470">голос-3PL.[NOM]</ta>
            <ta e="T472" id="Seg_8025" s="T471">слышаться-EP-PST2.[3SG]</ta>
            <ta e="T473" id="Seg_8026" s="T472">1SG.[NOM]</ta>
            <ta e="T474" id="Seg_8027" s="T473">вкусный</ta>
            <ta e="T475" id="Seg_8028" s="T474">пища.[NOM]</ta>
            <ta e="T476" id="Seg_8029" s="T475">становиться-CVB.SEQ</ta>
            <ta e="T477" id="Seg_8030" s="T476">после</ta>
            <ta e="T478" id="Seg_8031" s="T477">большой</ta>
            <ta e="T479" id="Seg_8032" s="T478">тарелка-DAT/LOC</ta>
            <ta e="T480" id="Seg_8033" s="T479">лодочка-3PL-GEN</ta>
            <ta e="T481" id="Seg_8034" s="T480">нутро-3SG-DAT/LOC</ta>
            <ta e="T482" id="Seg_8035" s="T481">падать-FUT-1SG</ta>
            <ta e="T483" id="Seg_8036" s="T482">1SG-ACC</ta>
            <ta e="T484" id="Seg_8037" s="T483">есть-TEMP-3PL</ta>
            <ta e="T485" id="Seg_8038" s="T484">умирать-CVB.SEQ</ta>
            <ta e="T486" id="Seg_8039" s="T485">оставаться-FUT-3PL</ta>
            <ta e="T487" id="Seg_8040" s="T486">говорить-PST2.[3SG]</ta>
            <ta e="T488" id="Seg_8041" s="T487">старый</ta>
            <ta e="T489" id="Seg_8042" s="T488">Баба.Яга.[NOM]</ta>
            <ta e="T490" id="Seg_8043" s="T489">голос-3SG.[NOM]</ta>
            <ta e="T491" id="Seg_8044" s="T490">1SG.[NOM]</ta>
            <ta e="T492" id="Seg_8045" s="T491">однако</ta>
            <ta e="T493" id="Seg_8046" s="T492">когда</ta>
            <ta e="T494" id="Seg_8047" s="T493">NEG</ta>
            <ta e="T495" id="Seg_8048" s="T494">видеть-NEG.PTCP.PST</ta>
            <ta e="T496" id="Seg_8049" s="T495">красивый</ta>
            <ta e="T497" id="Seg_8050" s="T496">шелк.[NOM]</ta>
            <ta e="T498" id="Seg_8051" s="T497">платок.[NOM]</ta>
            <ta e="T499" id="Seg_8052" s="T498">становиться-CVB.SEQ</ta>
            <ta e="T500" id="Seg_8053" s="T499">царь.[NOM]</ta>
            <ta e="T501" id="Seg_8054" s="T500">сын-3SG-GEN</ta>
            <ta e="T502" id="Seg_8055" s="T501">колено-3SG-DAT/LOC</ta>
            <ta e="T503" id="Seg_8056" s="T502">падать-FUT-1SG</ta>
            <ta e="T504" id="Seg_8057" s="T503">каждый-INTNS-3PL-ACC</ta>
            <ta e="T505" id="Seg_8058" s="T504">повеситься-CAUS-CVB.SEQ</ta>
            <ta e="T506" id="Seg_8059" s="T505">убить-FUT-1SG</ta>
            <ta e="T507" id="Seg_8060" s="T506">1SG.[NOM]</ta>
            <ta e="T508" id="Seg_8061" s="T507">однако</ta>
            <ta e="T509" id="Seg_8062" s="T508">лодочка-3PL-ACC</ta>
            <ta e="T510" id="Seg_8063" s="T509">плотно</ta>
            <ta e="T511" id="Seg_8064" s="T510">клеить-PTCP.PRS</ta>
            <ta e="T512" id="Seg_8065" s="T511">делать-CVB.SIM</ta>
            <ta e="T513" id="Seg_8066" s="T512">жидкий</ta>
            <ta e="T514" id="Seg_8067" s="T513">деготь.[NOM]</ta>
            <ta e="T515" id="Seg_8068" s="T514">становиться-FUT-1SG</ta>
            <ta e="T516" id="Seg_8069" s="T515">каждый-3PL-ACC</ta>
            <ta e="T517" id="Seg_8070" s="T516">втягивать-CVB.SEQ</ta>
            <ta e="T518" id="Seg_8071" s="T517">тонуть-CAUS-FUT-1SG</ta>
            <ta e="T519" id="Seg_8072" s="T518">вот</ta>
            <ta e="T520" id="Seg_8073" s="T519">хороший.[NOM]</ta>
            <ta e="T521" id="Seg_8074" s="T520">прекрасно</ta>
            <ta e="T522" id="Seg_8075" s="T521">думать-PST2-2PL</ta>
            <ta e="T523" id="Seg_8076" s="T522">быть-PST2.[3SG]</ta>
            <ta e="T524" id="Seg_8077" s="T523">говорить-PST2.[3SG]</ta>
            <ta e="T525" id="Seg_8078" s="T524">первый</ta>
            <ta e="T526" id="Seg_8079" s="T525">жена.[NOM]</ta>
            <ta e="T527" id="Seg_8080" s="T526">собака.[NOM]</ta>
            <ta e="T528" id="Seg_8081" s="T527">сын-3SG.[NOM]</ta>
            <ta e="T529" id="Seg_8082" s="T528">тот-ACC</ta>
            <ta e="T530" id="Seg_8083" s="T529">слышать-EP-PST2.[3SG]</ta>
            <ta e="T531" id="Seg_8084" s="T530">EMPH</ta>
            <ta e="T532" id="Seg_8085" s="T531">высокий.берег.[NOM]</ta>
            <ta e="T533" id="Seg_8086" s="T532">нижняя.часть-3SG-DAT/LOC</ta>
            <ta e="T534" id="Seg_8087" s="T533">бегать-CVB.SEQ</ta>
            <ta e="T535" id="Seg_8088" s="T534">доехать-PST2.[3SG]</ta>
            <ta e="T536" id="Seg_8089" s="T535">лодочка-3SG-ACC</ta>
            <ta e="T537" id="Seg_8090" s="T536">взять-INCH-PRS.[3SG]</ta>
            <ta e="T538" id="Seg_8091" s="T537">да</ta>
            <ta e="T539" id="Seg_8092" s="T538">грести-CVB.SEQ</ta>
            <ta e="T540" id="Seg_8093" s="T539">идти-PST2.[3SG]</ta>
            <ta e="T541" id="Seg_8094" s="T540">друг-PL-3SG-ACC</ta>
            <ta e="T542" id="Seg_8095" s="T541">река.[NOM]</ta>
            <ta e="T543" id="Seg_8096" s="T542">середина-3SG-DAT/LOC</ta>
            <ta e="T544" id="Seg_8097" s="T543">догонять-CVB.SIM</ta>
            <ta e="T545" id="Seg_8098" s="T544">бить-EP-PST2.[3SG]</ta>
            <ta e="T546" id="Seg_8099" s="T545">мальчик-PL.[NOM]</ta>
            <ta e="T547" id="Seg_8100" s="T546">потихоньку</ta>
            <ta e="T548" id="Seg_8101" s="T547">грести-CVB.SEQ</ta>
            <ta e="T549" id="Seg_8102" s="T548">идти-PST2-3PL</ta>
            <ta e="T550" id="Seg_8103" s="T549">сколько</ta>
            <ta e="T551" id="Seg_8104" s="T550">долго</ta>
            <ta e="T552" id="Seg_8105" s="T551">быть-FUT.[3SG]=Q</ta>
            <ta e="T553" id="Seg_8106" s="T552">лодочка-3PL-GEN</ta>
            <ta e="T554" id="Seg_8107" s="T553">нутро-3SG-DAT/LOC</ta>
            <ta e="T555" id="Seg_8108" s="T554">пища-PROPR</ta>
            <ta e="T556" id="Seg_8109" s="T555">тарелка.[NOM]</ta>
            <ta e="T557" id="Seg_8110" s="T556">падать-PST2.[3SG]</ta>
            <ta e="T558" id="Seg_8111" s="T557">царь.[NOM]</ta>
            <ta e="T559" id="Seg_8112" s="T558">сын-3SG.[NOM]</ta>
            <ta e="T560" id="Seg_8113" s="T559">радоваться-PST2.[3SG]</ta>
            <ta e="T561" id="Seg_8114" s="T560">рука-3SG-ACC</ta>
            <ta e="T562" id="Seg_8115" s="T561">тарелка.[NOM]</ta>
            <ta e="T563" id="Seg_8116" s="T562">к</ta>
            <ta e="T564" id="Seg_8117" s="T563">растопыривать-NMNZ.[NOM]</ta>
            <ta e="T565" id="Seg_8118" s="T564">делать-PTCP.PRS-3SG-ACC</ta>
            <ta e="T566" id="Seg_8119" s="T565">с</ta>
            <ta e="T567" id="Seg_8120" s="T566">собака.[NOM]</ta>
            <ta e="T568" id="Seg_8121" s="T567">сын-3SG.[NOM]</ta>
            <ta e="T569" id="Seg_8122" s="T568">пища-PROPR</ta>
            <ta e="T570" id="Seg_8123" s="T569">тарелка-ACC</ta>
            <ta e="T571" id="Seg_8124" s="T570">вода-DAT/LOC</ta>
            <ta e="T572" id="Seg_8125" s="T571">бросать-CVB.SEQ</ta>
            <ta e="T573" id="Seg_8126" s="T572">бросать-PST2.[3SG]</ta>
            <ta e="T574" id="Seg_8127" s="T573">царь.[NOM]</ta>
            <ta e="T575" id="Seg_8128" s="T574">сын-3SG.[NOM]</ta>
            <ta e="T576" id="Seg_8129" s="T575">сердиться-CVB.SEQ</ta>
            <ta e="T577" id="Seg_8130" s="T576">сидеть.обиженно-CVB.SEQ</ta>
            <ta e="T578" id="Seg_8131" s="T577">сидеть-TEMP-3SG</ta>
            <ta e="T579" id="Seg_8132" s="T578">красивый</ta>
            <ta e="T580" id="Seg_8133" s="T579">очень</ta>
            <ta e="T581" id="Seg_8134" s="T580">шелк.[NOM]</ta>
            <ta e="T582" id="Seg_8135" s="T581">платок.[NOM]</ta>
            <ta e="T583" id="Seg_8136" s="T582">царь.[NOM]</ta>
            <ta e="T584" id="Seg_8137" s="T583">сын-3SG-GEN</ta>
            <ta e="T585" id="Seg_8138" s="T584">колено-3SG-DAT/LOC</ta>
            <ta e="T586" id="Seg_8139" s="T585">падать-PST2.[3SG]</ta>
            <ta e="T587" id="Seg_8140" s="T586">о</ta>
            <ta e="T588" id="Seg_8141" s="T587">Бог.[NOM]</ta>
            <ta e="T589" id="Seg_8142" s="T588">мама-1SG-DAT/LOC</ta>
            <ta e="T590" id="Seg_8143" s="T589">подарок.[NOM]</ta>
            <ta e="T591" id="Seg_8144" s="T590">делать-FUT-1SG</ta>
            <ta e="T592" id="Seg_8145" s="T591">говорить-PST2.[3SG]</ta>
            <ta e="T593" id="Seg_8146" s="T592">да</ta>
            <ta e="T594" id="Seg_8147" s="T593">карман-3SG-DAT/LOC</ta>
            <ta e="T595" id="Seg_8148" s="T594">вкладывать-CVB.PURP</ta>
            <ta e="T596" id="Seg_8149" s="T595">хотеть-PTCP.PRS-3SG-ACC</ta>
            <ta e="T597" id="Seg_8150" s="T596">с</ta>
            <ta e="T598" id="Seg_8151" s="T597">сын-3SG.[NOM]</ta>
            <ta e="T599" id="Seg_8152" s="T598">выбирать-CVB.SEQ</ta>
            <ta e="T600" id="Seg_8153" s="T599">взять-PST2.[3SG]</ta>
            <ta e="T601" id="Seg_8154" s="T600">нож-3SG-INSTR</ta>
            <ta e="T602" id="Seg_8155" s="T601">много</ta>
            <ta e="T603" id="Seg_8156" s="T602">разрезать-CVB.SEQ</ta>
            <ta e="T604" id="Seg_8157" s="T603">после</ta>
            <ta e="T605" id="Seg_8158" s="T604">вода-DAT/LOC</ta>
            <ta e="T606" id="Seg_8159" s="T605">бросать-CVB.SEQ</ta>
            <ta e="T607" id="Seg_8160" s="T606">бросать-PST2.[3SG]</ta>
            <ta e="T608" id="Seg_8161" s="T607">царь.[NOM]</ta>
            <ta e="T609" id="Seg_8162" s="T608">сын-3SG.[NOM]</ta>
            <ta e="T610" id="Seg_8163" s="T609">сердиться-CVB.SEQ</ta>
            <ta e="T611" id="Seg_8164" s="T610">голос-3SG.[NOM]</ta>
            <ta e="T612" id="Seg_8165" s="T611">NEG</ta>
            <ta e="T613" id="Seg_8166" s="T612">пояляться-EP-PST2.NEG.[3SG]</ta>
            <ta e="T614" id="Seg_8167" s="T613">сколько</ta>
            <ta e="T615" id="Seg_8168" s="T614">долго</ta>
            <ta e="T616" id="Seg_8169" s="T615">быть-FUT.[3SG]=Q</ta>
            <ta e="T617" id="Seg_8170" s="T616">лодочка-3PL.[NOM]</ta>
            <ta e="T618" id="Seg_8171" s="T617">вода-DAT/LOC</ta>
            <ta e="T619" id="Seg_8172" s="T618">прилипать-CVB.SEQ</ta>
            <ta e="T620" id="Seg_8173" s="T619">оставаться-PST2.[3SG]</ta>
            <ta e="T621" id="Seg_8174" s="T620">река.[NOM]</ta>
            <ta e="T622" id="Seg_8175" s="T621">вода-3SG.[NOM]</ta>
            <ta e="T623" id="Seg_8176" s="T622">черно</ta>
            <ta e="T624" id="Seg_8177" s="T623">делать-PST2.[3SG]</ta>
            <ta e="T625" id="Seg_8178" s="T624">этот</ta>
            <ta e="T626" id="Seg_8179" s="T625">мальчик-PL.[NOM]</ta>
            <ta e="T627" id="Seg_8180" s="T626">мочь-CVB.SEQ</ta>
            <ta e="T628" id="Seg_8181" s="T627">грести-EP-MED-NEG.PTCP</ta>
            <ta e="T629" id="Seg_8182" s="T628">быть-CVB.SEQ</ta>
            <ta e="T630" id="Seg_8183" s="T629">оставаться-PST2-3PL</ta>
            <ta e="T631" id="Seg_8184" s="T630">бояться-PST2-3PL</ta>
            <ta e="T632" id="Seg_8185" s="T631">собака.[NOM]</ta>
            <ta e="T633" id="Seg_8186" s="T632">сын-3SG.[NOM]</ta>
            <ta e="T634" id="Seg_8187" s="T633">вода-ACC</ta>
            <ta e="T635" id="Seg_8188" s="T634">топор-INSTR</ta>
            <ta e="T636" id="Seg_8189" s="T635">резать-CVB.SIM</ta>
            <ta e="T637" id="Seg_8190" s="T636">разрезать-EP-IMP.2PL</ta>
            <ta e="T638" id="Seg_8191" s="T637">нож-3SG-INSTR</ta>
            <ta e="T639" id="Seg_8192" s="T638">протыкать-EP-CAUS-CVB.SIM</ta>
            <ta e="T640" id="Seg_8193" s="T639">толкать-EP-IMP.2PL</ta>
            <ta e="T641" id="Seg_8194" s="T640">1SG.[NOM]</ta>
            <ta e="T642" id="Seg_8195" s="T641">грести-CVB.SEQ</ta>
            <ta e="T643" id="Seg_8196" s="T642">идти-FUT-1SG</ta>
            <ta e="T644" id="Seg_8197" s="T643">говорить-PST2.[3SG]</ta>
            <ta e="T645" id="Seg_8198" s="T644">друг-PL-3SG-DAT/LOC</ta>
            <ta e="T646" id="Seg_8199" s="T645">видеть-PST2-3PL</ta>
            <ta e="T647" id="Seg_8200" s="T646">вода-3PL.[NOM]</ta>
            <ta e="T648" id="Seg_8201" s="T647">вправду</ta>
            <ta e="T649" id="Seg_8202" s="T648">посветлеть-CVB.SEQ</ta>
            <ta e="T650" id="Seg_8203" s="T649">краснеть-CVB.SEQ</ta>
            <ta e="T651" id="Seg_8204" s="T650">идти-PST2.[3SG]</ta>
            <ta e="T652" id="Seg_8205" s="T651">скоро</ta>
            <ta e="T653" id="Seg_8206" s="T652">лодочка-3PL.[NOM]</ta>
            <ta e="T654" id="Seg_8207" s="T653">освободить-EP-REFL-PST2.[3SG]</ta>
            <ta e="T655" id="Seg_8208" s="T654">мальчик-PL.[NOM]</ta>
            <ta e="T656" id="Seg_8209" s="T655">грести-EP-MED-CVB.SEQ</ta>
            <ta e="T657" id="Seg_8210" s="T656">идти-PST2-3PL</ta>
            <ta e="T658" id="Seg_8211" s="T657">потихоньку</ta>
            <ta e="T659" id="Seg_8212" s="T658">сколько</ta>
            <ta e="T660" id="Seg_8213" s="T659">NEG</ta>
            <ta e="T661" id="Seg_8214" s="T660">долго</ta>
            <ta e="T662" id="Seg_8215" s="T661">быть-NEG.CVB.SIM</ta>
            <ta e="T663" id="Seg_8216" s="T662">река.[NOM]</ta>
            <ta e="T664" id="Seg_8217" s="T663">высокий</ta>
            <ta e="T665" id="Seg_8218" s="T664">берег-3SG-DAT/LOC</ta>
            <ta e="T666" id="Seg_8219" s="T665">город-3PL.[NOM]</ta>
            <ta e="T667" id="Seg_8220" s="T666">быть.видно-EP-PST2.[3SG]</ta>
            <ta e="T668" id="Seg_8221" s="T667">приставать-CVB.SEQ</ta>
            <ta e="T669" id="Seg_8222" s="T668">после</ta>
            <ta e="T670" id="Seg_8223" s="T669">мальчик-PL.[NOM]</ta>
            <ta e="T671" id="Seg_8224" s="T670">три</ta>
            <ta e="T672" id="Seg_8225" s="T671">сторона.[NOM]</ta>
            <ta e="T673" id="Seg_8226" s="T672">расходиться-CVB.SEQ</ta>
            <ta e="T674" id="Seg_8227" s="T673">оставаться-PST2-3PL</ta>
            <ta e="T675" id="Seg_8228" s="T674">царь.[NOM]</ta>
            <ta e="T676" id="Seg_8229" s="T675">сын-3SG.[NOM]</ta>
            <ta e="T677" id="Seg_8230" s="T676">высокий.берег.[NOM]</ta>
            <ta e="T678" id="Seg_8231" s="T677">верхняя.часть-3SG-DAT/LOC</ta>
            <ta e="T679" id="Seg_8232" s="T678">белый</ta>
            <ta e="T680" id="Seg_8233" s="T679">дом-DAT/LOC</ta>
            <ta e="T681" id="Seg_8234" s="T680">идти-PST2.[3SG]</ta>
            <ta e="T682" id="Seg_8235" s="T681">собака.[NOM]</ta>
            <ta e="T683" id="Seg_8236" s="T682">сын-3SG.[NOM]</ta>
            <ta e="T684" id="Seg_8237" s="T683">мама-3SG-GEN</ta>
            <ta e="T685" id="Seg_8238" s="T684">низкий</ta>
            <ta e="T686" id="Seg_8239" s="T685">нора.[NOM]</ta>
            <ta e="T687" id="Seg_8240" s="T686">дом-3SG-DAT/LOC</ta>
            <ta e="T688" id="Seg_8241" s="T687">входить-PST2.[3SG]</ta>
            <ta e="T689" id="Seg_8242" s="T688">корова.[NOM]</ta>
            <ta e="T690" id="Seg_8243" s="T689">сын-3SG.[NOM]</ta>
            <ta e="T691" id="Seg_8244" s="T690">на.улице</ta>
            <ta e="T692" id="Seg_8245" s="T691">мать-3SG-ACC</ta>
            <ta e="T693" id="Seg_8246" s="T692">встречать-EP-PST2.[3SG]</ta>
            <ta e="T694" id="Seg_8247" s="T693">утром</ta>
            <ta e="T695" id="Seg_8248" s="T694">царь.[NOM]</ta>
            <ta e="T696" id="Seg_8249" s="T695">мальчик-PL-ACC</ta>
            <ta e="T697" id="Seg_8250" s="T696">звать-MED-FREQ-PST2.[3SG]</ta>
            <ta e="T698" id="Seg_8251" s="T697">вот</ta>
            <ta e="T699" id="Seg_8252" s="T698">ребенок-PL-EP-1SG.[NOM]</ta>
            <ta e="T700" id="Seg_8253" s="T699">рассказывать-IMP.2PL</ta>
            <ta e="T701" id="Seg_8254" s="T700">что-ACC</ta>
            <ta e="T702" id="Seg_8255" s="T701">делать-CVB.SIM</ta>
            <ta e="T703" id="Seg_8256" s="T702">этот</ta>
            <ta e="T704" id="Seg_8257" s="T703">так.долго</ta>
            <ta e="T705" id="Seg_8258" s="T704">идти-EP-PST2-2PL=Q</ta>
            <ta e="T706" id="Seg_8259" s="T705">три</ta>
            <ta e="T707" id="Seg_8260" s="T706">злой.дух.[NOM]</ta>
            <ta e="T708" id="Seg_8261" s="T707">череп-3SG-ABL</ta>
            <ta e="T709" id="Seg_8262" s="T708">три</ta>
            <ta e="T710" id="Seg_8263" s="T709">письмо-PROPR</ta>
            <ta e="T711" id="Seg_8264" s="T710">камень-ACC</ta>
            <ta e="T712" id="Seg_8265" s="T711">найти-PST2-EP-1SG</ta>
            <ta e="T713" id="Seg_8266" s="T712">собака.[NOM]</ta>
            <ta e="T714" id="Seg_8267" s="T713">сын-3SG.[NOM]</ta>
            <ta e="T715" id="Seg_8268" s="T714">говорить-PST2.[3SG]</ta>
            <ta e="T716" id="Seg_8269" s="T715">царь.[NOM]</ta>
            <ta e="T717" id="Seg_8270" s="T716">рука-3SG-DAT/LOC</ta>
            <ta e="T718" id="Seg_8271" s="T717">три</ta>
            <ta e="T719" id="Seg_8272" s="T718">камень-ACC</ta>
            <ta e="T720" id="Seg_8273" s="T719">подать-CVB.SEQ</ta>
            <ta e="T721" id="Seg_8274" s="T720">бросать-PST2.[3SG]</ta>
            <ta e="T722" id="Seg_8275" s="T721">царь.[NOM]</ta>
            <ta e="T723" id="Seg_8276" s="T722">собака.[NOM]</ta>
            <ta e="T724" id="Seg_8277" s="T723">сын-3SG-ACC</ta>
            <ta e="T725" id="Seg_8278" s="T724">шея-3SG-ABL</ta>
            <ta e="T726" id="Seg_8279" s="T725">обнимать-CVB.SEQ</ta>
            <ta e="T727" id="Seg_8280" s="T726">взять-PST2.[3SG]</ta>
            <ta e="T728" id="Seg_8281" s="T727">плакать-EP-MOM-CVB.SIM</ta>
            <ta e="T729" id="Seg_8282" s="T728">падать-CVB.SEQ</ta>
            <ta e="T730" id="Seg_8283" s="T729">после</ta>
            <ta e="T731" id="Seg_8284" s="T730">говорить-PST2.[3SG]</ta>
            <ta e="T732" id="Seg_8285" s="T731">сегодняшний-ABL</ta>
            <ta e="T733" id="Seg_8286" s="T732">2SG.[NOM]</ta>
            <ta e="T734" id="Seg_8287" s="T733">царь.[NOM]</ta>
            <ta e="T735" id="Seg_8288" s="T734">быть-PTCP.PRS</ta>
            <ta e="T736" id="Seg_8289" s="T735">предназначение-EP-2SG.[NOM]</ta>
            <ta e="T737" id="Seg_8290" s="T736">приходить-PST1-3SG</ta>
            <ta e="T738" id="Seg_8291" s="T737">сын-1SG-ACC</ta>
            <ta e="T739" id="Seg_8292" s="T738">с</ta>
            <ta e="T740" id="Seg_8293" s="T739">старший.брат.[NOM]-младший.брат.[NOM]</ta>
            <ta e="T741" id="Seg_8294" s="T740">быть-CVB.SEQ</ta>
            <ta e="T742" id="Seg_8295" s="T741">жить-EP-IMP.2PL</ta>
            <ta e="T743" id="Seg_8296" s="T742">три</ta>
            <ta e="T744" id="Seg_8297" s="T743">мальчик.[NOM]</ta>
            <ta e="T745" id="Seg_8298" s="T744">друг-SIM</ta>
            <ta e="T746" id="Seg_8299" s="T745">быть.богатым-CVB.SEQ-быть.богатым-CVB.SEQ</ta>
            <ta e="T747" id="Seg_8300" s="T746">жить-PST2-3PL</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_8301" s="T0">adv-adv</ta>
            <ta e="T2" id="Seg_8302" s="T1">cardnum</ta>
            <ta e="T3" id="Seg_8303" s="T2">n-n:case</ta>
            <ta e="T4" id="Seg_8304" s="T3">v-v:tense-v:pred.pn</ta>
            <ta e="T5" id="Seg_8305" s="T4">n-n:(poss)</ta>
            <ta e="T6" id="Seg_8306" s="T5">ptcl</ta>
            <ta e="T7" id="Seg_8307" s="T6">v-v:tense-v:poss.pn</ta>
            <ta e="T8" id="Seg_8308" s="T7">v-v:tense-v:pred.pn</ta>
            <ta e="T9" id="Seg_8309" s="T8">adv</ta>
            <ta e="T10" id="Seg_8310" s="T9">dempro</ta>
            <ta e="T11" id="Seg_8311" s="T10">n-n:case</ta>
            <ta e="T12" id="Seg_8312" s="T11">v-v:tense-v:pred.pn</ta>
            <ta e="T13" id="Seg_8313" s="T12">dempro</ta>
            <ta e="T14" id="Seg_8314" s="T13">que</ta>
            <ta e="T15" id="Seg_8315" s="T14">pers-pro:case</ta>
            <ta e="T16" id="Seg_8316" s="T15">n-n:(poss)</ta>
            <ta e="T17" id="Seg_8317" s="T16">ptcl-ptcl:(pred.pn)-ptcl</ta>
            <ta e="T18" id="Seg_8318" s="T17">adj-n:(num)-n:case</ta>
            <ta e="T19" id="Seg_8319" s="T18">v-v:ptcp-v:(case)</ta>
            <ta e="T20" id="Seg_8320" s="T19">ptcl</ta>
            <ta e="T21" id="Seg_8321" s="T20">dempro</ta>
            <ta e="T22" id="Seg_8322" s="T21">v-v:cvb</ta>
            <ta e="T23" id="Seg_8323" s="T22">v-v:mood-v:temp.pn</ta>
            <ta e="T24" id="Seg_8324" s="T23">n-n:(poss)-n:case</ta>
            <ta e="T25" id="Seg_8325" s="T24">v-v:tense-v:pred.pn</ta>
            <ta e="T26" id="Seg_8326" s="T25">que-pro:case</ta>
            <ta e="T27" id="Seg_8327" s="T26">v-v:tense-v:poss.pn</ta>
            <ta e="T28" id="Seg_8328" s="T27">n</ta>
            <ta e="T29" id="Seg_8329" s="T28">adv</ta>
            <ta e="T30" id="Seg_8330" s="T29">pers-pro:case</ta>
            <ta e="T31" id="Seg_8331" s="T30">n-n:(num)-n:case</ta>
            <ta e="T32" id="Seg_8332" s="T31">v-v:tense-v:poss.pn</ta>
            <ta e="T33" id="Seg_8333" s="T32">que-pro:case</ta>
            <ta e="T34" id="Seg_8334" s="T33">post</ta>
            <ta e="T35" id="Seg_8335" s="T34">adj</ta>
            <ta e="T36" id="Seg_8336" s="T35">n-n:case</ta>
            <ta e="T37" id="Seg_8337" s="T36">v-v:cvb</ta>
            <ta e="T38" id="Seg_8338" s="T37">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T39" id="Seg_8339" s="T38">n-n:(poss)</ta>
            <ta e="T40" id="Seg_8340" s="T39">ptcl</ta>
            <ta e="T41" id="Seg_8341" s="T40">v-v:mood.pn</ta>
            <ta e="T42" id="Seg_8342" s="T41">que</ta>
            <ta e="T43" id="Seg_8343" s="T42">n-n:(poss)</ta>
            <ta e="T44" id="Seg_8344" s="T43">ptcl-ptcl:(pred.pn)-ptcl</ta>
            <ta e="T45" id="Seg_8345" s="T44">que-pro:case</ta>
            <ta e="T46" id="Seg_8346" s="T45">n-n:poss-n:case</ta>
            <ta e="T47" id="Seg_8347" s="T46">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T48" id="Seg_8348" s="T47">v-v:mood-v:temp.pn</ta>
            <ta e="T49" id="Seg_8349" s="T48">adv</ta>
            <ta e="T50" id="Seg_8350" s="T49">v-v:tense-v:poss.pn</ta>
            <ta e="T51" id="Seg_8351" s="T50">quant</ta>
            <ta e="T52" id="Seg_8352" s="T51">ptcl</ta>
            <ta e="T53" id="Seg_8353" s="T52">n-n:case</ta>
            <ta e="T54" id="Seg_8354" s="T53">v-v&gt;v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T55" id="Seg_8355" s="T54">adv</ta>
            <ta e="T56" id="Seg_8356" s="T55">n-n:case</ta>
            <ta e="T57" id="Seg_8357" s="T56">v-v:tense-v:pred.pn</ta>
            <ta e="T58" id="Seg_8358" s="T57">dempro</ta>
            <ta e="T59" id="Seg_8359" s="T58">que</ta>
            <ta e="T60" id="Seg_8360" s="T59">pers-pro:case</ta>
            <ta e="T61" id="Seg_8361" s="T60">n-n:(poss)</ta>
            <ta e="T62" id="Seg_8362" s="T61">ptcl-ptcl:(pred.pn)-ptcl</ta>
            <ta e="T63" id="Seg_8363" s="T62">que</ta>
            <ta e="T64" id="Seg_8364" s="T63">que-pro:case</ta>
            <ta e="T65" id="Seg_8365" s="T64">v-v:ptcp-v:(case)</ta>
            <ta e="T66" id="Seg_8366" s="T65">n-n&gt;v-v:ptcp-v:(poss)-ptcl</ta>
            <ta e="T67" id="Seg_8367" s="T66">pers-pro:case</ta>
            <ta e="T68" id="Seg_8368" s="T67">que</ta>
            <ta e="T69" id="Seg_8369" s="T68">v-v:cvb</ta>
            <ta e="T70" id="Seg_8370" s="T69">v-v:tense-v:pred.pn</ta>
            <ta e="T71" id="Seg_8371" s="T70">n-n:(num)-n:case</ta>
            <ta e="T72" id="Seg_8372" s="T71">v-v:cvb</ta>
            <ta e="T73" id="Seg_8373" s="T72">v-v:tense-v:pred.pn</ta>
            <ta e="T74" id="Seg_8374" s="T73">n-n:poss-n:case</ta>
            <ta e="T75" id="Seg_8375" s="T74">adj</ta>
            <ta e="T76" id="Seg_8376" s="T75">adj</ta>
            <ta e="T77" id="Seg_8377" s="T76">n-n:case</ta>
            <ta e="T78" id="Seg_8378" s="T77">v-v&gt;n-n:case</ta>
            <ta e="T79" id="Seg_8379" s="T78">v-v:tense-v:pred.pn</ta>
            <ta e="T80" id="Seg_8380" s="T79">adj</ta>
            <ta e="T81" id="Seg_8381" s="T80">n-n&gt;adj-n:case</ta>
            <ta e="T82" id="Seg_8382" s="T81">n-n:case</ta>
            <ta e="T83" id="Seg_8383" s="T82">v-v:cvb</ta>
            <ta e="T84" id="Seg_8384" s="T83">v-v:tense-v:pred.pn</ta>
            <ta e="T85" id="Seg_8385" s="T84">dempro</ta>
            <ta e="T86" id="Seg_8386" s="T85">ptcl</ta>
            <ta e="T87" id="Seg_8387" s="T86">v-v:ptcp</ta>
            <ta e="T88" id="Seg_8388" s="T87">n-n:case</ta>
            <ta e="T89" id="Seg_8389" s="T88">n-n:case</ta>
            <ta e="T90" id="Seg_8390" s="T89">adv</ta>
            <ta e="T91" id="Seg_8391" s="T90">v-v:cvb</ta>
            <ta e="T92" id="Seg_8392" s="T91">post</ta>
            <ta e="T93" id="Seg_8393" s="T92">n-n:case</ta>
            <ta e="T94" id="Seg_8394" s="T93">v-v:cvb</ta>
            <ta e="T95" id="Seg_8395" s="T94">post</ta>
            <ta e="T96" id="Seg_8396" s="T95">v-v:tense-v:pred.pn</ta>
            <ta e="T97" id="Seg_8397" s="T96">v-v:(ins)-v:mood.pn</ta>
            <ta e="T98" id="Seg_8398" s="T97">n-n:case</ta>
            <ta e="T99" id="Seg_8399" s="T98">n-n:case</ta>
            <ta e="T100" id="Seg_8400" s="T99">n-n:case</ta>
            <ta e="T101" id="Seg_8401" s="T100">n-n:case</ta>
            <ta e="T102" id="Seg_8402" s="T101">n-n:case</ta>
            <ta e="T103" id="Seg_8403" s="T102">n-n:case</ta>
            <ta e="T104" id="Seg_8404" s="T103">adv</ta>
            <ta e="T105" id="Seg_8405" s="T104">n-n:case</ta>
            <ta e="T106" id="Seg_8406" s="T105">v-v:cvb</ta>
            <ta e="T107" id="Seg_8407" s="T106">v-v:mood.pn</ta>
            <ta e="T108" id="Seg_8408" s="T107">n-n:poss-n:case</ta>
            <ta e="T109" id="Seg_8409" s="T108">n-n:case</ta>
            <ta e="T110" id="Seg_8410" s="T109">v-v:tense-v:poss.pn</ta>
            <ta e="T111" id="Seg_8411" s="T110">dempro-pro:case</ta>
            <ta e="T112" id="Seg_8412" s="T111">n-n:(poss)-n:case</ta>
            <ta e="T113" id="Seg_8413" s="T112">v-v:mood.pn</ta>
            <ta e="T114" id="Seg_8414" s="T113">n-n:case</ta>
            <ta e="T115" id="Seg_8415" s="T114">n-n:(poss)-n:case</ta>
            <ta e="T116" id="Seg_8416" s="T115">v-v:tense-v:pred.pn</ta>
            <ta e="T117" id="Seg_8417" s="T116">dempro-pro:case</ta>
            <ta e="T118" id="Seg_8418" s="T117">pers-pro:case</ta>
            <ta e="T119" id="Seg_8419" s="T118">v-v:tense-v:poss.pn</ta>
            <ta e="T120" id="Seg_8420" s="T119">ptcl</ta>
            <ta e="T121" id="Seg_8421" s="T120">adv</ta>
            <ta e="T122" id="Seg_8422" s="T121">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T123" id="Seg_8423" s="T122">n-n:case</ta>
            <ta e="T124" id="Seg_8424" s="T123">n-n:poss-n:case</ta>
            <ta e="T125" id="Seg_8425" s="T124">n-n:case</ta>
            <ta e="T126" id="Seg_8426" s="T125">n-n&gt;adj</ta>
            <ta e="T127" id="Seg_8427" s="T126">n-n:case</ta>
            <ta e="T128" id="Seg_8428" s="T127">n-n&gt;adj</ta>
            <ta e="T129" id="Seg_8429" s="T128">n-n&gt;v-v:cvb</ta>
            <ta e="T130" id="Seg_8430" s="T129">v-v:tense-v:pred.pn</ta>
            <ta e="T131" id="Seg_8431" s="T130">cardnum</ta>
            <ta e="T132" id="Seg_8432" s="T131">n-n:case</ta>
            <ta e="T133" id="Seg_8433" s="T132">v-v:cvb</ta>
            <ta e="T134" id="Seg_8434" s="T133">adj-adj&gt;adv</ta>
            <ta e="T135" id="Seg_8435" s="T134">v-v:(neg)-v:pred.pn</ta>
            <ta e="T136" id="Seg_8436" s="T135">ptcl</ta>
            <ta e="T137" id="Seg_8437" s="T136">dempro-pro:case</ta>
            <ta e="T138" id="Seg_8438" s="T137">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T139" id="Seg_8439" s="T138">v-v:tense-v:pred.pn</ta>
            <ta e="T140" id="Seg_8440" s="T139">conj</ta>
            <ta e="T141" id="Seg_8441" s="T140">n-n:poss-n:case</ta>
            <ta e="T142" id="Seg_8442" s="T141">v-v:cvb</ta>
            <ta e="T143" id="Seg_8443" s="T142">v-v:tense-v:pred.pn</ta>
            <ta e="T144" id="Seg_8444" s="T143">adv</ta>
            <ta e="T145" id="Seg_8445" s="T144">v-v:mood.pn</ta>
            <ta e="T146" id="Seg_8446" s="T145">n-n:case</ta>
            <ta e="T147" id="Seg_8447" s="T146">n-n:case</ta>
            <ta e="T148" id="Seg_8448" s="T147">n-n:poss-n:case</ta>
            <ta e="T149" id="Seg_8449" s="T148">n-n:case</ta>
            <ta e="T150" id="Seg_8450" s="T149">n-n:poss-n:case</ta>
            <ta e="T151" id="Seg_8451" s="T150">v-v:cvb</ta>
            <ta e="T152" id="Seg_8452" s="T151">v-v:tense-v:pred.pn</ta>
            <ta e="T153" id="Seg_8453" s="T152">dempro-pro:case</ta>
            <ta e="T154" id="Seg_8454" s="T153">n-n:case</ta>
            <ta e="T155" id="Seg_8455" s="T154">n-n:(poss)-n:case</ta>
            <ta e="T156" id="Seg_8456" s="T155">v-v:cvb</ta>
            <ta e="T157" id="Seg_8457" s="T156">v-v:tense-v:pred.pn</ta>
            <ta e="T158" id="Seg_8458" s="T157">n-n:case</ta>
            <ta e="T159" id="Seg_8459" s="T158">n-n:(poss)-n:case</ta>
            <ta e="T160" id="Seg_8460" s="T159">n-n:case</ta>
            <ta e="T161" id="Seg_8461" s="T160">v-v:cvb</ta>
            <ta e="T162" id="Seg_8462" s="T161">post</ta>
            <ta e="T163" id="Seg_8463" s="T162">n-n:poss-n:case</ta>
            <ta e="T164" id="Seg_8464" s="T163">n-n:case</ta>
            <ta e="T165" id="Seg_8465" s="T164">v-v&gt;v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T166" id="Seg_8466" s="T165">n-n:case</ta>
            <ta e="T167" id="Seg_8467" s="T166">n-n:case</ta>
            <ta e="T168" id="Seg_8468" s="T167">v-v:tense-v:pred.pn</ta>
            <ta e="T169" id="Seg_8469" s="T168">n-n:case</ta>
            <ta e="T170" id="Seg_8470" s="T169">adv</ta>
            <ta e="T171" id="Seg_8471" s="T170">v-v:tense-v:poss.pn</ta>
            <ta e="T172" id="Seg_8472" s="T171">ptcl</ta>
            <ta e="T173" id="Seg_8473" s="T172">cardnum-cardnum&gt;collnum-n:(poss)-n:case</ta>
            <ta e="T174" id="Seg_8474" s="T173">cardnum</ta>
            <ta e="T175" id="Seg_8475" s="T174">n-n:case</ta>
            <ta e="T176" id="Seg_8476" s="T175">v-v:tense-v:pred.pn</ta>
            <ta e="T177" id="Seg_8477" s="T176">n-n:(num)-n:case</ta>
            <ta e="T178" id="Seg_8478" s="T177">que</ta>
            <ta e="T179" id="Seg_8479" s="T178">ptcl</ta>
            <ta e="T180" id="Seg_8480" s="T179">v-v:cvb</ta>
            <ta e="T181" id="Seg_8481" s="T180">adv</ta>
            <ta e="T182" id="Seg_8482" s="T181">v-v:ptcp</ta>
            <ta e="T183" id="Seg_8483" s="T182">v-v:tense-v:pred.pn</ta>
            <ta e="T184" id="Seg_8484" s="T183">adv</ta>
            <ta e="T185" id="Seg_8485" s="T184">ptcl</ta>
            <ta e="T186" id="Seg_8486" s="T185">n-n:case</ta>
            <ta e="T187" id="Seg_8487" s="T186">n-n:(poss)-n:case</ta>
            <ta e="T188" id="Seg_8488" s="T187">v-v:tense-v:pred.pn</ta>
            <ta e="T189" id="Seg_8489" s="T188">n-n:(num)-n:case</ta>
            <ta e="T190" id="Seg_8490" s="T189">n-n&gt;v-v:cvb</ta>
            <ta e="T191" id="Seg_8491" s="T190">v-v:mood.pn</ta>
            <ta e="T192" id="Seg_8492" s="T191">n-n:(num)-n:case</ta>
            <ta e="T193" id="Seg_8493" s="T192">que</ta>
            <ta e="T194" id="Seg_8494" s="T193">adv</ta>
            <ta e="T195" id="Seg_8495" s="T194">v-v:cvb</ta>
            <ta e="T196" id="Seg_8496" s="T195">v-v:tense-v:poss.pn</ta>
            <ta e="T197" id="Seg_8497" s="T196">ptcl</ta>
            <ta e="T198" id="Seg_8498" s="T197">v-v:tense-v:poss.pn</ta>
            <ta e="T199" id="Seg_8499" s="T198">n-n:case</ta>
            <ta e="T200" id="Seg_8500" s="T199">adv</ta>
            <ta e="T201" id="Seg_8501" s="T200">adj</ta>
            <ta e="T202" id="Seg_8502" s="T201">ptcl</ta>
            <ta e="T203" id="Seg_8503" s="T202">n-n:case</ta>
            <ta e="T204" id="Seg_8504" s="T203">n-n:case</ta>
            <ta e="T205" id="Seg_8505" s="T204">v-v:tense-v:pred.pn</ta>
            <ta e="T206" id="Seg_8506" s="T205">dempro-pro:case</ta>
            <ta e="T207" id="Seg_8507" s="T206">que-pro:(poss)-pro:case-ptcl</ta>
            <ta e="T208" id="Seg_8508" s="T207">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T209" id="Seg_8509" s="T208">n-n:case</ta>
            <ta e="T210" id="Seg_8510" s="T209">n-n:(poss)-n:case</ta>
            <ta e="T211" id="Seg_8511" s="T210">v-v:mood.pn</ta>
            <ta e="T212" id="Seg_8512" s="T211">n-n&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T213" id="Seg_8513" s="T212">v-v:tense-v:pred.pn</ta>
            <ta e="T214" id="Seg_8514" s="T213">n-n:case</ta>
            <ta e="T215" id="Seg_8515" s="T214">n-n:(poss)-n:case</ta>
            <ta e="T216" id="Seg_8516" s="T215">interj</ta>
            <ta e="T217" id="Seg_8517" s="T216">que</ta>
            <ta e="T218" id="Seg_8518" s="T217">adv</ta>
            <ta e="T219" id="Seg_8519" s="T218">n-n:(num)-n:case</ta>
            <ta e="T220" id="Seg_8520" s="T219">n-n:(poss)-n:case</ta>
            <ta e="T221" id="Seg_8521" s="T220">v-v:tense-v:poss.pn</ta>
            <ta e="T222" id="Seg_8522" s="T221">n-n:case</ta>
            <ta e="T223" id="Seg_8523" s="T222">n-n:(poss)-n:case</ta>
            <ta e="T224" id="Seg_8524" s="T223">v-v:mood.pn</ta>
            <ta e="T225" id="Seg_8525" s="T224">v-v:mood.pn</ta>
            <ta e="T226" id="Seg_8526" s="T225">v-v&gt;v-v:cvb</ta>
            <ta e="T227" id="Seg_8527" s="T226">v-v:mood.pn</ta>
            <ta e="T228" id="Seg_8528" s="T227">n-n:case</ta>
            <ta e="T229" id="Seg_8529" s="T228">v-v:cvb</ta>
            <ta e="T230" id="Seg_8530" s="T229">dempro</ta>
            <ta e="T231" id="Seg_8531" s="T230">cardnum</ta>
            <ta e="T232" id="Seg_8532" s="T231">n-n:case</ta>
            <ta e="T233" id="Seg_8533" s="T232">n-n&gt;v-v:cvb</ta>
            <ta e="T234" id="Seg_8534" s="T233">v-v:tense-v:pred.pn</ta>
            <ta e="T235" id="Seg_8535" s="T234">n-n:case</ta>
            <ta e="T236" id="Seg_8536" s="T235">n-n:(poss)-n:case</ta>
            <ta e="T237" id="Seg_8537" s="T236">v-v:cvb</ta>
            <ta e="T238" id="Seg_8538" s="T237">v-v:tense-v:pred.pn</ta>
            <ta e="T239" id="Seg_8539" s="T238">que</ta>
            <ta e="T240" id="Seg_8540" s="T239">adv</ta>
            <ta e="T241" id="Seg_8541" s="T240">v-v:tense-v:poss.pn</ta>
            <ta e="T242" id="Seg_8542" s="T241">ptcl</ta>
            <ta e="T243" id="Seg_8543" s="T242">dempro</ta>
            <ta e="T244" id="Seg_8544" s="T243">n-n:(num)-n:case</ta>
            <ta e="T245" id="Seg_8545" s="T244">ptcl</ta>
            <ta e="T246" id="Seg_8546" s="T245">v-v:cvb</ta>
            <ta e="T247" id="Seg_8547" s="T246">v-v:tense-v:pred.pn</ta>
            <ta e="T248" id="Seg_8548" s="T247">n-n:case</ta>
            <ta e="T249" id="Seg_8549" s="T248">n-n:(poss)-n:case</ta>
            <ta e="T250" id="Seg_8550" s="T249">n-n:case</ta>
            <ta e="T251" id="Seg_8551" s="T250">n-n:case</ta>
            <ta e="T252" id="Seg_8552" s="T251">v-v:(ins)-v:tense-v:poss.pn</ta>
            <ta e="T253" id="Seg_8553" s="T252">n-n:case</ta>
            <ta e="T254" id="Seg_8554" s="T253">v-v:ptcp</ta>
            <ta e="T255" id="Seg_8555" s="T254">n-n:(poss)-n:case</ta>
            <ta e="T256" id="Seg_8556" s="T255">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T257" id="Seg_8557" s="T256">v-v:(ins)-v:tense-v:poss.pn</ta>
            <ta e="T258" id="Seg_8558" s="T257">n-n:case</ta>
            <ta e="T259" id="Seg_8559" s="T258">n-n:case</ta>
            <ta e="T260" id="Seg_8560" s="T259">post</ta>
            <ta e="T261" id="Seg_8561" s="T260">v-v:cvb</ta>
            <ta e="T262" id="Seg_8562" s="T261">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T263" id="Seg_8563" s="T262">ptcl</ta>
            <ta e="T264" id="Seg_8564" s="T263">interj</ta>
            <ta e="T265" id="Seg_8565" s="T264">ptcl</ta>
            <ta e="T266" id="Seg_8566" s="T265">v-v:tense-v:pred.pn</ta>
            <ta e="T267" id="Seg_8567" s="T266">v-v:tense-v:pred.pn</ta>
            <ta e="T268" id="Seg_8568" s="T267">v-v:cvb</ta>
            <ta e="T269" id="Seg_8569" s="T268">post</ta>
            <ta e="T270" id="Seg_8570" s="T269">n-n:(num)-n:poss-n:case</ta>
            <ta e="T271" id="Seg_8571" s="T270">v-v&gt;v-v:cvb</ta>
            <ta e="T272" id="Seg_8572" s="T271">v-v:tense-v:pred.pn</ta>
            <ta e="T273" id="Seg_8573" s="T272">dempro</ta>
            <ta e="T274" id="Seg_8574" s="T273">n-n:(num)-n:case</ta>
            <ta e="T275" id="Seg_8575" s="T274">ptcl</ta>
            <ta e="T276" id="Seg_8576" s="T275">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T277" id="Seg_8577" s="T276">v-v:mood.pn-v-v:mood.pn</ta>
            <ta e="T278" id="Seg_8578" s="T277">pers-pro:case</ta>
            <ta e="T279" id="Seg_8579" s="T278">pers-pro:case</ta>
            <ta e="T280" id="Seg_8580" s="T279">v-v:tense-v:poss.pn</ta>
            <ta e="T281" id="Seg_8581" s="T280">n-n:case</ta>
            <ta e="T282" id="Seg_8582" s="T281">n-n:(poss)-n:case</ta>
            <ta e="T283" id="Seg_8583" s="T282">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T284" id="Seg_8584" s="T283">dempro</ta>
            <ta e="T285" id="Seg_8585" s="T284">n-n:case</ta>
            <ta e="T286" id="Seg_8586" s="T285">v-v:cvb</ta>
            <ta e="T287" id="Seg_8587" s="T286">v-v:tense-v:pred.pn</ta>
            <ta e="T288" id="Seg_8588" s="T287">conj</ta>
            <ta e="T289" id="Seg_8589" s="T288">n-n:case</ta>
            <ta e="T290" id="Seg_8590" s="T289">post</ta>
            <ta e="T291" id="Seg_8591" s="T290">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T292" id="Seg_8592" s="T291">que</ta>
            <ta e="T293" id="Seg_8593" s="T292">ptcl</ta>
            <ta e="T294" id="Seg_8594" s="T293">v-v:cvb</ta>
            <ta e="T295" id="Seg_8595" s="T294">n-n:case</ta>
            <ta e="T296" id="Seg_8596" s="T295">n-n:poss-n:case</ta>
            <ta e="T297" id="Seg_8597" s="T296">n-n:case</ta>
            <ta e="T298" id="Seg_8598" s="T297">n-n:case</ta>
            <ta e="T299" id="Seg_8599" s="T298">v-v:cvb</ta>
            <ta e="T300" id="Seg_8600" s="T299">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T301" id="Seg_8601" s="T300">dempro</ta>
            <ta e="T302" id="Seg_8602" s="T301">v-v:cvb</ta>
            <ta e="T303" id="Seg_8603" s="T302">post</ta>
            <ta e="T304" id="Seg_8604" s="T303">n-n:poss-n:case</ta>
            <ta e="T305" id="Seg_8605" s="T304">n-n:poss-n:case</ta>
            <ta e="T306" id="Seg_8606" s="T305">n-n&gt;adj</ta>
            <ta e="T307" id="Seg_8607" s="T306">n-n:case</ta>
            <ta e="T308" id="Seg_8608" s="T307">v-v:tense-v:pred.pn</ta>
            <ta e="T309" id="Seg_8609" s="T308">n-n:case</ta>
            <ta e="T310" id="Seg_8610" s="T309">n-n:poss-n:case</ta>
            <ta e="T311" id="Seg_8611" s="T310">n-n:poss-n:case</ta>
            <ta e="T312" id="Seg_8612" s="T311">v-v:cvb</ta>
            <ta e="T313" id="Seg_8613" s="T312">v-v:tense-v:pred.pn</ta>
            <ta e="T314" id="Seg_8614" s="T313">adv</ta>
            <ta e="T315" id="Seg_8615" s="T314">que-pro:case</ta>
            <ta e="T316" id="Seg_8616" s="T315">ptcl</ta>
            <ta e="T317" id="Seg_8617" s="T316">v-v:ptcp-v:(case)</ta>
            <ta e="T318" id="Seg_8618" s="T317">post</ta>
            <ta e="T319" id="Seg_8619" s="T318">n-n:case</ta>
            <ta e="T320" id="Seg_8620" s="T319">n-n:(poss)-n:case</ta>
            <ta e="T321" id="Seg_8621" s="T320">n-n:(num)-n:poss-n:case</ta>
            <ta e="T322" id="Seg_8622" s="T321">v-v&gt;v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T323" id="Seg_8623" s="T322">interj</ta>
            <ta e="T324" id="Seg_8624" s="T323">n-n&gt;v-v:cvb</ta>
            <ta e="T325" id="Seg_8625" s="T324">v-v:mood.pn</ta>
            <ta e="T326" id="Seg_8626" s="T325">n-n:(num)-n:case</ta>
            <ta e="T327" id="Seg_8627" s="T326">v-v:cvb</ta>
            <ta e="T328" id="Seg_8628" s="T327">adv</ta>
            <ta e="T329" id="Seg_8629" s="T328">v-v:cvb</ta>
            <ta e="T330" id="Seg_8630" s="T329">n-n:case</ta>
            <ta e="T331" id="Seg_8631" s="T330">v-v:tense-v:pred.pn</ta>
            <ta e="T332" id="Seg_8632" s="T331">n-n:case</ta>
            <ta e="T333" id="Seg_8633" s="T332">n-n:case</ta>
            <ta e="T334" id="Seg_8634" s="T333">adj-n:case</ta>
            <ta e="T335" id="Seg_8635" s="T334">que</ta>
            <ta e="T336" id="Seg_8636" s="T335">ptcl</ta>
            <ta e="T337" id="Seg_8637" s="T336">v-v:cvb</ta>
            <ta e="T338" id="Seg_8638" s="T337">n-n:case</ta>
            <ta e="T339" id="Seg_8639" s="T338">v-v:tense-v:pred.pn</ta>
            <ta e="T340" id="Seg_8640" s="T339">n-n:case</ta>
            <ta e="T341" id="Seg_8641" s="T340">n-n:(poss)-n:case</ta>
            <ta e="T342" id="Seg_8642" s="T341">v-v:cvb-v-v:cvb</ta>
            <ta e="T343" id="Seg_8643" s="T342">post</ta>
            <ta e="T344" id="Seg_8644" s="T343">v-v:cvb</ta>
            <ta e="T345" id="Seg_8645" s="T344">v-v:tense-v:pred.pn</ta>
            <ta e="T346" id="Seg_8646" s="T345">n-n:(num)-n:(poss)-n:case</ta>
            <ta e="T347" id="Seg_8647" s="T346">ptcl</ta>
            <ta e="T348" id="Seg_8648" s="T347">n-n&gt;v-v:cvb</ta>
            <ta e="T349" id="Seg_8649" s="T348">v-v:tense-v:pred.pn</ta>
            <ta e="T350" id="Seg_8650" s="T349">n-n:case</ta>
            <ta e="T351" id="Seg_8651" s="T350">n-n:case</ta>
            <ta e="T352" id="Seg_8652" s="T351">v-v:(ins)-v:tense-v:poss.pn</ta>
            <ta e="T353" id="Seg_8653" s="T352">n-n:(num)-n:(poss)-n:case</ta>
            <ta e="T354" id="Seg_8654" s="T353">v-v:cvb</ta>
            <ta e="T355" id="Seg_8655" s="T354">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T356" id="Seg_8656" s="T355">v-v:cvb</ta>
            <ta e="T357" id="Seg_8657" s="T356">v-v:tense-v:pred.pn</ta>
            <ta e="T358" id="Seg_8658" s="T357">dempro</ta>
            <ta e="T359" id="Seg_8659" s="T358">n-n:case</ta>
            <ta e="T360" id="Seg_8660" s="T359">v-v:cvb</ta>
            <ta e="T361" id="Seg_8661" s="T360">post</ta>
            <ta e="T362" id="Seg_8662" s="T361">v-v:cvb</ta>
            <ta e="T363" id="Seg_8663" s="T362">v-v:tense-v:pred.pn</ta>
            <ta e="T364" id="Seg_8664" s="T363">adj-n:case</ta>
            <ta e="T365" id="Seg_8665" s="T364">que</ta>
            <ta e="T366" id="Seg_8666" s="T365">ptcl</ta>
            <ta e="T367" id="Seg_8667" s="T366">ptcl</ta>
            <ta e="T368" id="Seg_8668" s="T367">v-v:ptcp</ta>
            <ta e="T369" id="Seg_8669" s="T368">n-n:case</ta>
            <ta e="T370" id="Seg_8670" s="T369">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T371" id="Seg_8671" s="T370">n-n:(num)-n:poss-n:case</ta>
            <ta e="T372" id="Seg_8672" s="T371">v-v&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T373" id="Seg_8673" s="T372">ptcl</ta>
            <ta e="T374" id="Seg_8674" s="T373">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T375" id="Seg_8675" s="T374">adv</ta>
            <ta e="T376" id="Seg_8676" s="T375">v-v:ptcp</ta>
            <ta e="T377" id="Seg_8677" s="T376">n-n:(num)-n:case</ta>
            <ta e="T378" id="Seg_8678" s="T377">n-n:case</ta>
            <ta e="T379" id="Seg_8679" s="T378">n-n:(poss)-n:case</ta>
            <ta e="T380" id="Seg_8680" s="T379">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T381" id="Seg_8681" s="T380">conj</ta>
            <ta e="T382" id="Seg_8682" s="T381">cardnum-cardnum&gt;ordnum</ta>
            <ta e="T383" id="Seg_8683" s="T382">n-n:case</ta>
            <ta e="T384" id="Seg_8684" s="T383">n-n:poss-n:case</ta>
            <ta e="T385" id="Seg_8685" s="T384">ptcl</ta>
            <ta e="T386" id="Seg_8686" s="T385">n-n:case</ta>
            <ta e="T387" id="Seg_8687" s="T386">n-n:case</ta>
            <ta e="T388" id="Seg_8688" s="T387">adv</ta>
            <ta e="T389" id="Seg_8689" s="T388">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T390" id="Seg_8690" s="T389">n-n:case</ta>
            <ta e="T391" id="Seg_8691" s="T390">v-v:cvb</ta>
            <ta e="T392" id="Seg_8692" s="T391">v-v:cvb</ta>
            <ta e="T393" id="Seg_8693" s="T392">post</ta>
            <ta e="T394" id="Seg_8694" s="T393">v-v&gt;n-n:case</ta>
            <ta e="T395" id="Seg_8695" s="T394">v-v:tense-v:pred.pn</ta>
            <ta e="T396" id="Seg_8696" s="T395">n-n:case</ta>
            <ta e="T397" id="Seg_8697" s="T396">n-n:(poss)-n:case</ta>
            <ta e="T398" id="Seg_8698" s="T397">n-n:poss-n:case</ta>
            <ta e="T399" id="Seg_8699" s="T398">n-n&gt;adj</ta>
            <ta e="T400" id="Seg_8700" s="T399">n-n:case</ta>
            <ta e="T401" id="Seg_8701" s="T400">v-v:tense-v:poss.pn</ta>
            <ta e="T402" id="Seg_8702" s="T401">ptcl</ta>
            <ta e="T403" id="Seg_8703" s="T402">n-n:poss-n:case</ta>
            <ta e="T404" id="Seg_8704" s="T403">ptcl</ta>
            <ta e="T405" id="Seg_8705" s="T404">v-v:cvb</ta>
            <ta e="T406" id="Seg_8706" s="T405">v-v:tense-v:pred.pn</ta>
            <ta e="T407" id="Seg_8707" s="T406">cardnum-cardnum&gt;ordnum</ta>
            <ta e="T408" id="Seg_8708" s="T407">n-n:poss-n:case</ta>
            <ta e="T409" id="Seg_8709" s="T408">cardnum-cardnum&gt;ordnum</ta>
            <ta e="T410" id="Seg_8710" s="T409">n-n:case</ta>
            <ta e="T411" id="Seg_8711" s="T410">n-n:poss-n:case</ta>
            <ta e="T412" id="Seg_8712" s="T411">v-v:cvb</ta>
            <ta e="T413" id="Seg_8713" s="T412">v-v:cvb</ta>
            <ta e="T414" id="Seg_8714" s="T413">n-n&gt;adj</ta>
            <ta e="T415" id="Seg_8715" s="T414">n-n:case</ta>
            <ta e="T416" id="Seg_8716" s="T415">v-v:cvb</ta>
            <ta e="T417" id="Seg_8717" s="T416">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T418" id="Seg_8718" s="T417">n-n:(num)-n:poss-n:case</ta>
            <ta e="T419" id="Seg_8719" s="T418">n-n:case</ta>
            <ta e="T420" id="Seg_8720" s="T419">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T421" id="Seg_8721" s="T420">post</ta>
            <ta e="T422" id="Seg_8722" s="T421">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T423" id="Seg_8723" s="T422">ptcl</ta>
            <ta e="T424" id="Seg_8724" s="T423">n-n:poss-n:case</ta>
            <ta e="T425" id="Seg_8725" s="T424">v-v:mood.pn</ta>
            <ta e="T426" id="Seg_8726" s="T425">v-v:tense-v:pred.pn</ta>
            <ta e="T427" id="Seg_8727" s="T426">dempro</ta>
            <ta e="T428" id="Seg_8728" s="T427">n-n:case</ta>
            <ta e="T429" id="Seg_8729" s="T428">n-n:case</ta>
            <ta e="T430" id="Seg_8730" s="T429">n-n:(poss)-n:case</ta>
            <ta e="T431" id="Seg_8731" s="T430">v-v:tense-v:pred.pn</ta>
            <ta e="T432" id="Seg_8732" s="T431">que</ta>
            <ta e="T433" id="Seg_8733" s="T432">ptcl</ta>
            <ta e="T434" id="Seg_8734" s="T433">v-v:cvb</ta>
            <ta e="T435" id="Seg_8735" s="T434">v-v:mood.pn</ta>
            <ta e="T436" id="Seg_8736" s="T435">adv</ta>
            <ta e="T437" id="Seg_8737" s="T436">n-n:case</ta>
            <ta e="T438" id="Seg_8738" s="T437">n-n:(poss)-n:case</ta>
            <ta e="T439" id="Seg_8739" s="T438">v-v:cvb</ta>
            <ta e="T440" id="Seg_8740" s="T439">v-v:cvb</ta>
            <ta e="T441" id="Seg_8741" s="T440">post</ta>
            <ta e="T442" id="Seg_8742" s="T441">v-v:tense-v:pred.pn</ta>
            <ta e="T443" id="Seg_8743" s="T442">n-n:(num)-n:(poss)-n:case</ta>
            <ta e="T444" id="Seg_8744" s="T443">v-v:tense-v:poss.pn</ta>
            <ta e="T445" id="Seg_8745" s="T444">v-v:mood.pn</ta>
            <ta e="T446" id="Seg_8746" s="T445">ptcl</ta>
            <ta e="T447" id="Seg_8747" s="T446">n-n:(num)-n:case</ta>
            <ta e="T448" id="Seg_8748" s="T447">v-v:tense-v:pred.pn</ta>
            <ta e="T449" id="Seg_8749" s="T448">n-n:case</ta>
            <ta e="T450" id="Seg_8750" s="T449">n-n:(poss)-n:case</ta>
            <ta e="T451" id="Seg_8751" s="T450">v-v:cvb</ta>
            <ta e="T452" id="Seg_8752" s="T451">v-v:tense-v:pred.pn</ta>
            <ta e="T453" id="Seg_8753" s="T452">que</ta>
            <ta e="T454" id="Seg_8754" s="T453">ptcl</ta>
            <ta e="T455" id="Seg_8755" s="T454">v-v:cvb</ta>
            <ta e="T456" id="Seg_8756" s="T455">n-n:poss-n:case</ta>
            <ta e="T457" id="Seg_8757" s="T456">v-v:cvb</ta>
            <ta e="T458" id="Seg_8758" s="T457">v-v:cvb</ta>
            <ta e="T459" id="Seg_8759" s="T458">n-n:(num)</ta>
            <ta e="T460" id="Seg_8760" s="T459">n-n:poss-n:case</ta>
            <ta e="T461" id="Seg_8761" s="T460">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T462" id="Seg_8762" s="T461">v-v:cvb</ta>
            <ta e="T463" id="Seg_8763" s="T462">v-v:tense-v:poss.pn</ta>
            <ta e="T464" id="Seg_8764" s="T463">v-v:tense-v:pred.pn</ta>
            <ta e="T465" id="Seg_8765" s="T464">n-n:case</ta>
            <ta e="T466" id="Seg_8766" s="T465">n-n:(poss)-n:case</ta>
            <ta e="T467" id="Seg_8767" s="T466">n-n:case</ta>
            <ta e="T468" id="Seg_8768" s="T467">v-v:tense-v:poss.pn</ta>
            <ta e="T469" id="Seg_8769" s="T468">n-n:(num)-n:case</ta>
            <ta e="T470" id="Seg_8770" s="T469">v-v:ptcp</ta>
            <ta e="T471" id="Seg_8771" s="T470">n-n:(poss)-n:case</ta>
            <ta e="T472" id="Seg_8772" s="T471">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T473" id="Seg_8773" s="T472">pers-pro:case</ta>
            <ta e="T474" id="Seg_8774" s="T473">adj</ta>
            <ta e="T475" id="Seg_8775" s="T474">n-n:case</ta>
            <ta e="T476" id="Seg_8776" s="T475">v-v:cvb</ta>
            <ta e="T477" id="Seg_8777" s="T476">post</ta>
            <ta e="T478" id="Seg_8778" s="T477">adj</ta>
            <ta e="T479" id="Seg_8779" s="T478">n-n:case</ta>
            <ta e="T480" id="Seg_8780" s="T479">n-n:poss-n:case</ta>
            <ta e="T481" id="Seg_8781" s="T480">n-n:poss-n:case</ta>
            <ta e="T482" id="Seg_8782" s="T481">v-v:tense-v:poss.pn</ta>
            <ta e="T483" id="Seg_8783" s="T482">pers-pro:case</ta>
            <ta e="T484" id="Seg_8784" s="T483">v-v:mood-v:temp.pn</ta>
            <ta e="T485" id="Seg_8785" s="T484">v-v:cvb</ta>
            <ta e="T486" id="Seg_8786" s="T485">v-v:tense-v:poss.pn</ta>
            <ta e="T487" id="Seg_8787" s="T486">v-v:tense-v:pred.pn</ta>
            <ta e="T488" id="Seg_8788" s="T487">adj</ta>
            <ta e="T489" id="Seg_8789" s="T488">propr-n:case</ta>
            <ta e="T490" id="Seg_8790" s="T489">n-n:(poss)-n:case</ta>
            <ta e="T491" id="Seg_8791" s="T490">pers-pro:case</ta>
            <ta e="T492" id="Seg_8792" s="T491">ptcl</ta>
            <ta e="T493" id="Seg_8793" s="T492">que</ta>
            <ta e="T494" id="Seg_8794" s="T493">ptcl</ta>
            <ta e="T495" id="Seg_8795" s="T494">v-v:ptcp</ta>
            <ta e="T496" id="Seg_8796" s="T495">adj</ta>
            <ta e="T497" id="Seg_8797" s="T496">n-n:case</ta>
            <ta e="T498" id="Seg_8798" s="T497">n-n:case</ta>
            <ta e="T499" id="Seg_8799" s="T498">v-v:cvb</ta>
            <ta e="T500" id="Seg_8800" s="T499">n-n:case</ta>
            <ta e="T501" id="Seg_8801" s="T500">n-n:poss-n:case</ta>
            <ta e="T502" id="Seg_8802" s="T501">n-n:poss-n:case</ta>
            <ta e="T503" id="Seg_8803" s="T502">v-v:tense-v:poss.pn</ta>
            <ta e="T504" id="Seg_8804" s="T503">adj-n&gt;n-n:poss-n:case</ta>
            <ta e="T505" id="Seg_8805" s="T504">v-v&gt;v-v:cvb</ta>
            <ta e="T506" id="Seg_8806" s="T505">v-v:tense-v:poss.pn</ta>
            <ta e="T507" id="Seg_8807" s="T506">pers-pro:case</ta>
            <ta e="T508" id="Seg_8808" s="T507">ptcl</ta>
            <ta e="T509" id="Seg_8809" s="T508">n-n:poss-n:case</ta>
            <ta e="T510" id="Seg_8810" s="T509">adv</ta>
            <ta e="T511" id="Seg_8811" s="T510">v-v:ptcp</ta>
            <ta e="T512" id="Seg_8812" s="T511">v-v:cvb</ta>
            <ta e="T513" id="Seg_8813" s="T512">adj</ta>
            <ta e="T514" id="Seg_8814" s="T513">n-n:case</ta>
            <ta e="T515" id="Seg_8815" s="T514">v-v:tense-v:poss.pn</ta>
            <ta e="T516" id="Seg_8816" s="T515">adj-n:poss-n:case</ta>
            <ta e="T517" id="Seg_8817" s="T516">v-v:cvb</ta>
            <ta e="T518" id="Seg_8818" s="T517">v-v&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T519" id="Seg_8819" s="T518">ptcl</ta>
            <ta e="T520" id="Seg_8820" s="T519">adj-n:case</ta>
            <ta e="T521" id="Seg_8821" s="T520">adv</ta>
            <ta e="T522" id="Seg_8822" s="T521">v-v:tense-v:pred.pn</ta>
            <ta e="T523" id="Seg_8823" s="T522">v-v:tense-v:pred.pn</ta>
            <ta e="T524" id="Seg_8824" s="T523">v-v:tense-v:pred.pn</ta>
            <ta e="T525" id="Seg_8825" s="T524">ordnum</ta>
            <ta e="T526" id="Seg_8826" s="T525">n-n:case</ta>
            <ta e="T527" id="Seg_8827" s="T526">n-n:case</ta>
            <ta e="T528" id="Seg_8828" s="T527">n-n:(poss)-n:case</ta>
            <ta e="T529" id="Seg_8829" s="T528">dempro-pro:case</ta>
            <ta e="T530" id="Seg_8830" s="T529">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T531" id="Seg_8831" s="T530">ptcl</ta>
            <ta e="T532" id="Seg_8832" s="T531">n-n:case</ta>
            <ta e="T533" id="Seg_8833" s="T532">n-n:poss-n:case</ta>
            <ta e="T534" id="Seg_8834" s="T533">v-v:cvb</ta>
            <ta e="T535" id="Seg_8835" s="T534">v-v:tense-v:pred.pn</ta>
            <ta e="T536" id="Seg_8836" s="T535">n-n:poss-n:case</ta>
            <ta e="T537" id="Seg_8837" s="T536">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T538" id="Seg_8838" s="T537">conj</ta>
            <ta e="T539" id="Seg_8839" s="T538">v-v:cvb</ta>
            <ta e="T540" id="Seg_8840" s="T539">v-v:tense-v:pred.pn</ta>
            <ta e="T541" id="Seg_8841" s="T540">n-n:(num)-n:poss-n:case</ta>
            <ta e="T542" id="Seg_8842" s="T541">n-n:case</ta>
            <ta e="T543" id="Seg_8843" s="T542">n-n:poss-n:case</ta>
            <ta e="T544" id="Seg_8844" s="T543">v-v:cvb</ta>
            <ta e="T545" id="Seg_8845" s="T544">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T546" id="Seg_8846" s="T545">n-n:(num)-n:case</ta>
            <ta e="T547" id="Seg_8847" s="T546">adv</ta>
            <ta e="T548" id="Seg_8848" s="T547">v-v:cvb</ta>
            <ta e="T549" id="Seg_8849" s="T548">v-v:tense-v:pred.pn</ta>
            <ta e="T550" id="Seg_8850" s="T549">que</ta>
            <ta e="T551" id="Seg_8851" s="T550">adv</ta>
            <ta e="T552" id="Seg_8852" s="T551">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T553" id="Seg_8853" s="T552">n-n:poss-n:case</ta>
            <ta e="T554" id="Seg_8854" s="T553">n-n:poss-n:case</ta>
            <ta e="T555" id="Seg_8855" s="T554">n-n&gt;adj</ta>
            <ta e="T556" id="Seg_8856" s="T555">n-n:case</ta>
            <ta e="T557" id="Seg_8857" s="T556">v-v:tense-v:pred.pn</ta>
            <ta e="T558" id="Seg_8858" s="T557">n-n:case</ta>
            <ta e="T559" id="Seg_8859" s="T558">n-n:(poss)-n:case</ta>
            <ta e="T560" id="Seg_8860" s="T559">v-v:tense-v:pred.pn</ta>
            <ta e="T561" id="Seg_8861" s="T560">n-n:poss-n:case</ta>
            <ta e="T562" id="Seg_8862" s="T561">n-n:case</ta>
            <ta e="T563" id="Seg_8863" s="T562">post</ta>
            <ta e="T564" id="Seg_8864" s="T563">v-v&gt;n-n:case</ta>
            <ta e="T565" id="Seg_8865" s="T564">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T566" id="Seg_8866" s="T565">post</ta>
            <ta e="T567" id="Seg_8867" s="T566">n-n:case</ta>
            <ta e="T568" id="Seg_8868" s="T567">n-n:(poss)-n:case</ta>
            <ta e="T569" id="Seg_8869" s="T568">n-n&gt;adj</ta>
            <ta e="T570" id="Seg_8870" s="T569">n-n:case</ta>
            <ta e="T571" id="Seg_8871" s="T570">n-n:case</ta>
            <ta e="T572" id="Seg_8872" s="T571">v-v:cvb</ta>
            <ta e="T573" id="Seg_8873" s="T572">v-v:tense-v:pred.pn</ta>
            <ta e="T574" id="Seg_8874" s="T573">n-n:case</ta>
            <ta e="T575" id="Seg_8875" s="T574">n-n:(poss)-n:case</ta>
            <ta e="T576" id="Seg_8876" s="T575">v-v:cvb</ta>
            <ta e="T577" id="Seg_8877" s="T576">v-v:cvb</ta>
            <ta e="T578" id="Seg_8878" s="T577">v-v:mood-v:temp.pn</ta>
            <ta e="T579" id="Seg_8879" s="T578">adj</ta>
            <ta e="T580" id="Seg_8880" s="T579">ptcl</ta>
            <ta e="T581" id="Seg_8881" s="T580">n-n:case</ta>
            <ta e="T582" id="Seg_8882" s="T581">n-n:case</ta>
            <ta e="T583" id="Seg_8883" s="T582">n-n:case</ta>
            <ta e="T584" id="Seg_8884" s="T583">n-n:poss-n:case</ta>
            <ta e="T585" id="Seg_8885" s="T584">n-n:poss-n:case</ta>
            <ta e="T586" id="Seg_8886" s="T585">v-v:tense-v:pred.pn</ta>
            <ta e="T587" id="Seg_8887" s="T586">interj</ta>
            <ta e="T588" id="Seg_8888" s="T587">n-n:case</ta>
            <ta e="T589" id="Seg_8889" s="T588">n-n:poss-n:case</ta>
            <ta e="T590" id="Seg_8890" s="T589">n-n:case</ta>
            <ta e="T591" id="Seg_8891" s="T590">v-v:tense-v:poss.pn</ta>
            <ta e="T592" id="Seg_8892" s="T591">v-v:tense-v:pred.pn</ta>
            <ta e="T593" id="Seg_8893" s="T592">conj</ta>
            <ta e="T594" id="Seg_8894" s="T593">n-n:poss-n:case</ta>
            <ta e="T595" id="Seg_8895" s="T594">v-v:cvb</ta>
            <ta e="T596" id="Seg_8896" s="T595">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T597" id="Seg_8897" s="T596">post</ta>
            <ta e="T598" id="Seg_8898" s="T597">n-n:(poss)-n:case</ta>
            <ta e="T599" id="Seg_8899" s="T598">v-v:cvb</ta>
            <ta e="T600" id="Seg_8900" s="T599">v-v:tense-v:pred.pn</ta>
            <ta e="T601" id="Seg_8901" s="T600">n-n:poss-n:case</ta>
            <ta e="T602" id="Seg_8902" s="T601">adv</ta>
            <ta e="T603" id="Seg_8903" s="T602">v-v:cvb</ta>
            <ta e="T604" id="Seg_8904" s="T603">post</ta>
            <ta e="T605" id="Seg_8905" s="T604">n-n:case</ta>
            <ta e="T606" id="Seg_8906" s="T605">v-v:cvb</ta>
            <ta e="T607" id="Seg_8907" s="T606">v-v:tense-v:pred.pn</ta>
            <ta e="T608" id="Seg_8908" s="T607">n-n:case</ta>
            <ta e="T609" id="Seg_8909" s="T608">n-n:(poss)-n:case</ta>
            <ta e="T610" id="Seg_8910" s="T609">v-v:cvb</ta>
            <ta e="T611" id="Seg_8911" s="T610">n-n:(poss)-n:case</ta>
            <ta e="T612" id="Seg_8912" s="T611">ptcl</ta>
            <ta e="T613" id="Seg_8913" s="T612">v-v:(ins)-v:neg-v:pred.pn</ta>
            <ta e="T614" id="Seg_8914" s="T613">que</ta>
            <ta e="T615" id="Seg_8915" s="T614">adv</ta>
            <ta e="T616" id="Seg_8916" s="T615">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T617" id="Seg_8917" s="T616">n-n:(poss)-n:case</ta>
            <ta e="T618" id="Seg_8918" s="T617">n-n:case</ta>
            <ta e="T619" id="Seg_8919" s="T618">v-v:cvb</ta>
            <ta e="T620" id="Seg_8920" s="T619">v-v:tense-v:pred.pn</ta>
            <ta e="T621" id="Seg_8921" s="T620">n-n:case</ta>
            <ta e="T622" id="Seg_8922" s="T621">n-n:(poss)-n:case</ta>
            <ta e="T623" id="Seg_8923" s="T622">adv</ta>
            <ta e="T624" id="Seg_8924" s="T623">v-v:tense-v:pred.pn</ta>
            <ta e="T625" id="Seg_8925" s="T624">dempro</ta>
            <ta e="T626" id="Seg_8926" s="T625">n-n:(num)-n:case</ta>
            <ta e="T627" id="Seg_8927" s="T626">v-v:cvb</ta>
            <ta e="T628" id="Seg_8928" s="T627">v-v:(ins)-v&gt;v-v:ptcp</ta>
            <ta e="T629" id="Seg_8929" s="T628">v-v:cvb</ta>
            <ta e="T630" id="Seg_8930" s="T629">v-v:tense-v:pred.pn</ta>
            <ta e="T631" id="Seg_8931" s="T630">v-v:tense-v:pred.pn</ta>
            <ta e="T632" id="Seg_8932" s="T631">n-n:case</ta>
            <ta e="T633" id="Seg_8933" s="T632">n-n:(poss)-n:case</ta>
            <ta e="T634" id="Seg_8934" s="T633">n-n:case</ta>
            <ta e="T635" id="Seg_8935" s="T634">n-n:case</ta>
            <ta e="T636" id="Seg_8936" s="T635">v-v:cvb</ta>
            <ta e="T637" id="Seg_8937" s="T636">v-v:(ins)-v:mood.pn</ta>
            <ta e="T638" id="Seg_8938" s="T637">n-n:poss-n:case</ta>
            <ta e="T639" id="Seg_8939" s="T638">v-n:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T640" id="Seg_8940" s="T639">v-v:(ins)-v:mood.pn</ta>
            <ta e="T641" id="Seg_8941" s="T640">pers-pro:case</ta>
            <ta e="T642" id="Seg_8942" s="T641">v-v:cvb</ta>
            <ta e="T643" id="Seg_8943" s="T642">v-v:tense-v:poss.pn</ta>
            <ta e="T644" id="Seg_8944" s="T643">v-v:tense-v:pred.pn</ta>
            <ta e="T645" id="Seg_8945" s="T644">n-n:(num)-n:poss-n:case</ta>
            <ta e="T646" id="Seg_8946" s="T645">v-v:tense-v:poss.pn</ta>
            <ta e="T647" id="Seg_8947" s="T646">n-n:(poss)-n:case</ta>
            <ta e="T648" id="Seg_8948" s="T647">adv</ta>
            <ta e="T649" id="Seg_8949" s="T648">v-v:cvb</ta>
            <ta e="T650" id="Seg_8950" s="T649">v-v:cvb</ta>
            <ta e="T651" id="Seg_8951" s="T650">v-v:tense-v:pred.pn</ta>
            <ta e="T652" id="Seg_8952" s="T651">adv</ta>
            <ta e="T653" id="Seg_8953" s="T652">n-n:(poss)-n:case</ta>
            <ta e="T654" id="Seg_8954" s="T653">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T655" id="Seg_8955" s="T654">n-n:(num)-n:case</ta>
            <ta e="T656" id="Seg_8956" s="T655">v-n:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T657" id="Seg_8957" s="T656">v-v:tense-v:pred.pn</ta>
            <ta e="T658" id="Seg_8958" s="T657">adv</ta>
            <ta e="T659" id="Seg_8959" s="T658">que</ta>
            <ta e="T660" id="Seg_8960" s="T659">ptcl</ta>
            <ta e="T661" id="Seg_8961" s="T660">adv</ta>
            <ta e="T662" id="Seg_8962" s="T661">v-v:cvb</ta>
            <ta e="T663" id="Seg_8963" s="T662">n-n:case</ta>
            <ta e="T664" id="Seg_8964" s="T663">adj</ta>
            <ta e="T665" id="Seg_8965" s="T664">n-n:poss-n:case</ta>
            <ta e="T666" id="Seg_8966" s="T665">n-n:(poss)-n:case</ta>
            <ta e="T667" id="Seg_8967" s="T666">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T668" id="Seg_8968" s="T667">v-v:cvb</ta>
            <ta e="T669" id="Seg_8969" s="T668">post</ta>
            <ta e="T670" id="Seg_8970" s="T669">n-n:(num)-n:case</ta>
            <ta e="T671" id="Seg_8971" s="T670">cardnum</ta>
            <ta e="T672" id="Seg_8972" s="T671">n-n:case</ta>
            <ta e="T673" id="Seg_8973" s="T672">v-v:cvb</ta>
            <ta e="T674" id="Seg_8974" s="T673">v-v:tense-v:pred.pn</ta>
            <ta e="T675" id="Seg_8975" s="T674">n-n:case</ta>
            <ta e="T676" id="Seg_8976" s="T675">n-n:(poss)-n:case</ta>
            <ta e="T677" id="Seg_8977" s="T676">n-n:case</ta>
            <ta e="T678" id="Seg_8978" s="T677">n-n:poss-n:case</ta>
            <ta e="T679" id="Seg_8979" s="T678">adj</ta>
            <ta e="T680" id="Seg_8980" s="T679">n-n:case</ta>
            <ta e="T681" id="Seg_8981" s="T680">v-v:tense-v:pred.pn</ta>
            <ta e="T682" id="Seg_8982" s="T681">n-n:case</ta>
            <ta e="T683" id="Seg_8983" s="T682">n-n:(poss)-n:case</ta>
            <ta e="T684" id="Seg_8984" s="T683">n-n:poss-n:case</ta>
            <ta e="T685" id="Seg_8985" s="T684">adj</ta>
            <ta e="T686" id="Seg_8986" s="T685">n-n:case</ta>
            <ta e="T687" id="Seg_8987" s="T686">n-n:poss-n:case</ta>
            <ta e="T688" id="Seg_8988" s="T687">v-v:tense-v:pred.pn</ta>
            <ta e="T689" id="Seg_8989" s="T688">n-n:case</ta>
            <ta e="T690" id="Seg_8990" s="T689">n-n:(poss)-n:case</ta>
            <ta e="T691" id="Seg_8991" s="T690">adv</ta>
            <ta e="T692" id="Seg_8992" s="T691">n-n:poss-n:case</ta>
            <ta e="T693" id="Seg_8993" s="T692">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T694" id="Seg_8994" s="T693">adv</ta>
            <ta e="T695" id="Seg_8995" s="T694">n-n:case</ta>
            <ta e="T696" id="Seg_8996" s="T695">n-n:(num)-n:case</ta>
            <ta e="T697" id="Seg_8997" s="T696">v-v&gt;v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T698" id="Seg_8998" s="T697">ptcl</ta>
            <ta e="T699" id="Seg_8999" s="T698">n-n:(num)-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T700" id="Seg_9000" s="T699">v-v:mood.pn</ta>
            <ta e="T701" id="Seg_9001" s="T700">que-n:case</ta>
            <ta e="T702" id="Seg_9002" s="T701">v-v:cvb</ta>
            <ta e="T703" id="Seg_9003" s="T702">dempro</ta>
            <ta e="T704" id="Seg_9004" s="T703">adv</ta>
            <ta e="T705" id="Seg_9005" s="T704">v-v:(ins)-v:tense-v:pred.pn-ptcl</ta>
            <ta e="T706" id="Seg_9006" s="T705">cardnum</ta>
            <ta e="T707" id="Seg_9007" s="T706">n-n:case</ta>
            <ta e="T708" id="Seg_9008" s="T707">n-n:poss-n:case</ta>
            <ta e="T709" id="Seg_9009" s="T708">cardnum</ta>
            <ta e="T710" id="Seg_9010" s="T709">n-n&gt;adj</ta>
            <ta e="T711" id="Seg_9011" s="T710">n-n:case</ta>
            <ta e="T712" id="Seg_9012" s="T711">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T713" id="Seg_9013" s="T712">n-n:case</ta>
            <ta e="T714" id="Seg_9014" s="T713">n-n:(poss)-n:case</ta>
            <ta e="T715" id="Seg_9015" s="T714">v-v:tense-v:pred.pn</ta>
            <ta e="T716" id="Seg_9016" s="T715">n-n:case</ta>
            <ta e="T717" id="Seg_9017" s="T716">n-n:poss-n:case</ta>
            <ta e="T718" id="Seg_9018" s="T717">cardnum</ta>
            <ta e="T719" id="Seg_9019" s="T718">n-n:case</ta>
            <ta e="T720" id="Seg_9020" s="T719">v-v:cvb</ta>
            <ta e="T721" id="Seg_9021" s="T720">v-v:tense-v:pred.pn</ta>
            <ta e="T722" id="Seg_9022" s="T721">n-n:case</ta>
            <ta e="T723" id="Seg_9023" s="T722">n-n:case</ta>
            <ta e="T724" id="Seg_9024" s="T723">n-n:poss-n:case</ta>
            <ta e="T725" id="Seg_9025" s="T724">n-n:poss-n:case</ta>
            <ta e="T726" id="Seg_9026" s="T725">v-v:cvb</ta>
            <ta e="T727" id="Seg_9027" s="T726">v-v:tense-v:pred.pn</ta>
            <ta e="T728" id="Seg_9028" s="T727">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T729" id="Seg_9029" s="T728">v-v:cvb</ta>
            <ta e="T730" id="Seg_9030" s="T729">post</ta>
            <ta e="T731" id="Seg_9031" s="T730">v-v:tense-v:pred.pn</ta>
            <ta e="T732" id="Seg_9032" s="T731">adj-n:case</ta>
            <ta e="T733" id="Seg_9033" s="T732">pers-pro:case</ta>
            <ta e="T734" id="Seg_9034" s="T733">n-n:case</ta>
            <ta e="T735" id="Seg_9035" s="T734">v-v:ptcp</ta>
            <ta e="T736" id="Seg_9036" s="T735">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T737" id="Seg_9037" s="T736">v-v:tense-v:poss.pn</ta>
            <ta e="T738" id="Seg_9038" s="T737">n-n:poss-n:case</ta>
            <ta e="T739" id="Seg_9039" s="T738">post</ta>
            <ta e="T740" id="Seg_9040" s="T739">n-n:case-n-n:case</ta>
            <ta e="T741" id="Seg_9041" s="T740">v-v:cvb</ta>
            <ta e="T742" id="Seg_9042" s="T741">v-v:(ins)-v:mood.pn</ta>
            <ta e="T743" id="Seg_9043" s="T742">cardnum</ta>
            <ta e="T744" id="Seg_9044" s="T743">n-n:case</ta>
            <ta e="T745" id="Seg_9045" s="T744">n-n&gt;adv</ta>
            <ta e="T746" id="Seg_9046" s="T745">v-v:cvb-v-v:cvb</ta>
            <ta e="T747" id="Seg_9047" s="T746">v-v:tense-v:pred.pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_9048" s="T0">adv</ta>
            <ta e="T2" id="Seg_9049" s="T1">cardnum</ta>
            <ta e="T3" id="Seg_9050" s="T2">n</ta>
            <ta e="T4" id="Seg_9051" s="T3">v</ta>
            <ta e="T5" id="Seg_9052" s="T4">n</ta>
            <ta e="T6" id="Seg_9053" s="T5">ptcl</ta>
            <ta e="T7" id="Seg_9054" s="T6">cop</ta>
            <ta e="T8" id="Seg_9055" s="T7">aux</ta>
            <ta e="T9" id="Seg_9056" s="T8">adv</ta>
            <ta e="T10" id="Seg_9057" s="T9">dempro</ta>
            <ta e="T11" id="Seg_9058" s="T10">n</ta>
            <ta e="T12" id="Seg_9059" s="T11">v</ta>
            <ta e="T13" id="Seg_9060" s="T12">dempro</ta>
            <ta e="T14" id="Seg_9061" s="T13">que</ta>
            <ta e="T15" id="Seg_9062" s="T14">pers</ta>
            <ta e="T16" id="Seg_9063" s="T15">n</ta>
            <ta e="T17" id="Seg_9064" s="T16">ptcl</ta>
            <ta e="T18" id="Seg_9065" s="T17">n</ta>
            <ta e="T19" id="Seg_9066" s="T18">v</ta>
            <ta e="T20" id="Seg_9067" s="T19">ptcl</ta>
            <ta e="T21" id="Seg_9068" s="T20">dempro</ta>
            <ta e="T22" id="Seg_9069" s="T21">v</ta>
            <ta e="T23" id="Seg_9070" s="T22">v</ta>
            <ta e="T24" id="Seg_9071" s="T23">n</ta>
            <ta e="T25" id="Seg_9072" s="T24">v</ta>
            <ta e="T26" id="Seg_9073" s="T25">que</ta>
            <ta e="T27" id="Seg_9074" s="T26">v</ta>
            <ta e="T28" id="Seg_9075" s="T27">n</ta>
            <ta e="T29" id="Seg_9076" s="T28">adv</ta>
            <ta e="T30" id="Seg_9077" s="T29">pers</ta>
            <ta e="T31" id="Seg_9078" s="T30">n</ta>
            <ta e="T32" id="Seg_9079" s="T31">v</ta>
            <ta e="T33" id="Seg_9080" s="T32">que</ta>
            <ta e="T34" id="Seg_9081" s="T33">post</ta>
            <ta e="T35" id="Seg_9082" s="T34">adj</ta>
            <ta e="T36" id="Seg_9083" s="T35">n</ta>
            <ta e="T37" id="Seg_9084" s="T36">v</ta>
            <ta e="T38" id="Seg_9085" s="T37">v</ta>
            <ta e="T39" id="Seg_9086" s="T38">n</ta>
            <ta e="T40" id="Seg_9087" s="T39">ptcl</ta>
            <ta e="T41" id="Seg_9088" s="T40">v</ta>
            <ta e="T42" id="Seg_9089" s="T41">que</ta>
            <ta e="T43" id="Seg_9090" s="T42">n</ta>
            <ta e="T44" id="Seg_9091" s="T43">ptcl</ta>
            <ta e="T45" id="Seg_9092" s="T44">que</ta>
            <ta e="T46" id="Seg_9093" s="T45">n</ta>
            <ta e="T47" id="Seg_9094" s="T46">v</ta>
            <ta e="T48" id="Seg_9095" s="T47">v</ta>
            <ta e="T49" id="Seg_9096" s="T48">adv</ta>
            <ta e="T50" id="Seg_9097" s="T49">v</ta>
            <ta e="T51" id="Seg_9098" s="T50">quant</ta>
            <ta e="T52" id="Seg_9099" s="T51">ptcl</ta>
            <ta e="T53" id="Seg_9100" s="T52">n</ta>
            <ta e="T54" id="Seg_9101" s="T53">v</ta>
            <ta e="T55" id="Seg_9102" s="T54">adv</ta>
            <ta e="T56" id="Seg_9103" s="T55">n</ta>
            <ta e="T57" id="Seg_9104" s="T56">v</ta>
            <ta e="T58" id="Seg_9105" s="T57">dempro</ta>
            <ta e="T59" id="Seg_9106" s="T58">que</ta>
            <ta e="T60" id="Seg_9107" s="T59">pers</ta>
            <ta e="T61" id="Seg_9108" s="T60">n</ta>
            <ta e="T62" id="Seg_9109" s="T61">ptcl</ta>
            <ta e="T63" id="Seg_9110" s="T62">que</ta>
            <ta e="T64" id="Seg_9111" s="T63">que</ta>
            <ta e="T65" id="Seg_9112" s="T64">v</ta>
            <ta e="T66" id="Seg_9113" s="T65">v</ta>
            <ta e="T67" id="Seg_9114" s="T66">pers</ta>
            <ta e="T68" id="Seg_9115" s="T67">que</ta>
            <ta e="T69" id="Seg_9116" s="T68">v</ta>
            <ta e="T70" id="Seg_9117" s="T69">v</ta>
            <ta e="T71" id="Seg_9118" s="T70">n</ta>
            <ta e="T72" id="Seg_9119" s="T71">v</ta>
            <ta e="T73" id="Seg_9120" s="T72">aux</ta>
            <ta e="T74" id="Seg_9121" s="T73">n</ta>
            <ta e="T75" id="Seg_9122" s="T74">adj</ta>
            <ta e="T76" id="Seg_9123" s="T75">adj</ta>
            <ta e="T77" id="Seg_9124" s="T76">n</ta>
            <ta e="T78" id="Seg_9125" s="T77">n</ta>
            <ta e="T79" id="Seg_9126" s="T78">v</ta>
            <ta e="T80" id="Seg_9127" s="T79">adj</ta>
            <ta e="T81" id="Seg_9128" s="T80">n</ta>
            <ta e="T82" id="Seg_9129" s="T81">n</ta>
            <ta e="T83" id="Seg_9130" s="T82">v</ta>
            <ta e="T84" id="Seg_9131" s="T83">aux</ta>
            <ta e="T85" id="Seg_9132" s="T84">dempro</ta>
            <ta e="T86" id="Seg_9133" s="T85">ptcl</ta>
            <ta e="T87" id="Seg_9134" s="T86">adj</ta>
            <ta e="T88" id="Seg_9135" s="T87">n</ta>
            <ta e="T89" id="Seg_9136" s="T88">n</ta>
            <ta e="T90" id="Seg_9137" s="T89">adv</ta>
            <ta e="T91" id="Seg_9138" s="T90">v</ta>
            <ta e="T92" id="Seg_9139" s="T91">post</ta>
            <ta e="T93" id="Seg_9140" s="T92">n</ta>
            <ta e="T94" id="Seg_9141" s="T93">v</ta>
            <ta e="T95" id="Seg_9142" s="T94">post</ta>
            <ta e="T96" id="Seg_9143" s="T95">v</ta>
            <ta e="T97" id="Seg_9144" s="T96">v</ta>
            <ta e="T98" id="Seg_9145" s="T97">n</ta>
            <ta e="T99" id="Seg_9146" s="T98">n</ta>
            <ta e="T100" id="Seg_9147" s="T99">n</ta>
            <ta e="T101" id="Seg_9148" s="T100">n</ta>
            <ta e="T102" id="Seg_9149" s="T101">n</ta>
            <ta e="T103" id="Seg_9150" s="T102">n</ta>
            <ta e="T104" id="Seg_9151" s="T103">adv</ta>
            <ta e="T105" id="Seg_9152" s="T104">n</ta>
            <ta e="T106" id="Seg_9153" s="T105">v</ta>
            <ta e="T107" id="Seg_9154" s="T106">v</ta>
            <ta e="T108" id="Seg_9155" s="T107">n</ta>
            <ta e="T109" id="Seg_9156" s="T108">n</ta>
            <ta e="T110" id="Seg_9157" s="T109">v</ta>
            <ta e="T111" id="Seg_9158" s="T110">dempro</ta>
            <ta e="T112" id="Seg_9159" s="T111">n</ta>
            <ta e="T113" id="Seg_9160" s="T112">v</ta>
            <ta e="T114" id="Seg_9161" s="T113">n</ta>
            <ta e="T115" id="Seg_9162" s="T114">n</ta>
            <ta e="T116" id="Seg_9163" s="T115">v</ta>
            <ta e="T117" id="Seg_9164" s="T116">dempro</ta>
            <ta e="T118" id="Seg_9165" s="T117">pers</ta>
            <ta e="T119" id="Seg_9166" s="T118">v</ta>
            <ta e="T120" id="Seg_9167" s="T119">ptcl</ta>
            <ta e="T121" id="Seg_9168" s="T120">adv</ta>
            <ta e="T122" id="Seg_9169" s="T121">v</ta>
            <ta e="T123" id="Seg_9170" s="T122">n</ta>
            <ta e="T124" id="Seg_9171" s="T123">n</ta>
            <ta e="T125" id="Seg_9172" s="T124">n</ta>
            <ta e="T126" id="Seg_9173" s="T125">adj</ta>
            <ta e="T127" id="Seg_9174" s="T126">n</ta>
            <ta e="T128" id="Seg_9175" s="T127">adj</ta>
            <ta e="T129" id="Seg_9176" s="T128">v</ta>
            <ta e="T130" id="Seg_9177" s="T129">v</ta>
            <ta e="T131" id="Seg_9178" s="T130">cardnum</ta>
            <ta e="T132" id="Seg_9179" s="T131">n</ta>
            <ta e="T133" id="Seg_9180" s="T132">v</ta>
            <ta e="T134" id="Seg_9181" s="T133">adv</ta>
            <ta e="T135" id="Seg_9182" s="T134">v</ta>
            <ta e="T136" id="Seg_9183" s="T135">ptcl</ta>
            <ta e="T137" id="Seg_9184" s="T136">dempro</ta>
            <ta e="T138" id="Seg_9185" s="T137">n</ta>
            <ta e="T139" id="Seg_9186" s="T138">n</ta>
            <ta e="T140" id="Seg_9187" s="T139">conj</ta>
            <ta e="T141" id="Seg_9188" s="T140">n</ta>
            <ta e="T142" id="Seg_9189" s="T141">v</ta>
            <ta e="T143" id="Seg_9190" s="T142">aux</ta>
            <ta e="T144" id="Seg_9191" s="T143">adv</ta>
            <ta e="T145" id="Seg_9192" s="T144">v</ta>
            <ta e="T146" id="Seg_9193" s="T145">n</ta>
            <ta e="T147" id="Seg_9194" s="T146">n</ta>
            <ta e="T148" id="Seg_9195" s="T147">n</ta>
            <ta e="T149" id="Seg_9196" s="T148">n</ta>
            <ta e="T150" id="Seg_9197" s="T149">n</ta>
            <ta e="T151" id="Seg_9198" s="T150">v</ta>
            <ta e="T152" id="Seg_9199" s="T151">aux</ta>
            <ta e="T153" id="Seg_9200" s="T152">dempro</ta>
            <ta e="T154" id="Seg_9201" s="T153">n</ta>
            <ta e="T155" id="Seg_9202" s="T154">n</ta>
            <ta e="T156" id="Seg_9203" s="T155">v</ta>
            <ta e="T157" id="Seg_9204" s="T156">aux</ta>
            <ta e="T158" id="Seg_9205" s="T157">n</ta>
            <ta e="T159" id="Seg_9206" s="T158">n</ta>
            <ta e="T160" id="Seg_9207" s="T159">n</ta>
            <ta e="T161" id="Seg_9208" s="T160">v</ta>
            <ta e="T162" id="Seg_9209" s="T161">post</ta>
            <ta e="T163" id="Seg_9210" s="T162">n</ta>
            <ta e="T164" id="Seg_9211" s="T163">n</ta>
            <ta e="T165" id="Seg_9212" s="T164">v</ta>
            <ta e="T166" id="Seg_9213" s="T165">n</ta>
            <ta e="T167" id="Seg_9214" s="T166">n</ta>
            <ta e="T168" id="Seg_9215" s="T167">v</ta>
            <ta e="T169" id="Seg_9216" s="T168">n</ta>
            <ta e="T170" id="Seg_9217" s="T169">adv</ta>
            <ta e="T171" id="Seg_9218" s="T170">cop</ta>
            <ta e="T172" id="Seg_9219" s="T171">ptcl</ta>
            <ta e="T173" id="Seg_9220" s="T172">collnum</ta>
            <ta e="T174" id="Seg_9221" s="T173">cardnum</ta>
            <ta e="T175" id="Seg_9222" s="T174">n</ta>
            <ta e="T176" id="Seg_9223" s="T175">v</ta>
            <ta e="T177" id="Seg_9224" s="T176">n</ta>
            <ta e="T178" id="Seg_9225" s="T177">que</ta>
            <ta e="T179" id="Seg_9226" s="T178">ptcl</ta>
            <ta e="T180" id="Seg_9227" s="T179">cop</ta>
            <ta e="T181" id="Seg_9228" s="T180">adv</ta>
            <ta e="T182" id="Seg_9229" s="T181">v</ta>
            <ta e="T183" id="Seg_9230" s="T182">aux</ta>
            <ta e="T184" id="Seg_9231" s="T183">adv</ta>
            <ta e="T185" id="Seg_9232" s="T184">ptcl</ta>
            <ta e="T186" id="Seg_9233" s="T185">n</ta>
            <ta e="T187" id="Seg_9234" s="T186">n</ta>
            <ta e="T188" id="Seg_9235" s="T187">v</ta>
            <ta e="T189" id="Seg_9236" s="T188">n</ta>
            <ta e="T190" id="Seg_9237" s="T189">v</ta>
            <ta e="T191" id="Seg_9238" s="T190">aux</ta>
            <ta e="T192" id="Seg_9239" s="T191">n</ta>
            <ta e="T193" id="Seg_9240" s="T192">que</ta>
            <ta e="T194" id="Seg_9241" s="T193">adv</ta>
            <ta e="T195" id="Seg_9242" s="T194">v</ta>
            <ta e="T196" id="Seg_9243" s="T195">aux</ta>
            <ta e="T197" id="Seg_9244" s="T196">ptcl</ta>
            <ta e="T198" id="Seg_9245" s="T197">v</ta>
            <ta e="T199" id="Seg_9246" s="T198">n</ta>
            <ta e="T200" id="Seg_9247" s="T199">adv</ta>
            <ta e="T201" id="Seg_9248" s="T200">adj</ta>
            <ta e="T202" id="Seg_9249" s="T201">ptcl</ta>
            <ta e="T203" id="Seg_9250" s="T202">n</ta>
            <ta e="T204" id="Seg_9251" s="T203">n</ta>
            <ta e="T205" id="Seg_9252" s="T204">v</ta>
            <ta e="T206" id="Seg_9253" s="T205">dempro</ta>
            <ta e="T207" id="Seg_9254" s="T206">que</ta>
            <ta e="T208" id="Seg_9255" s="T207">v</ta>
            <ta e="T209" id="Seg_9256" s="T208">n</ta>
            <ta e="T210" id="Seg_9257" s="T209">n</ta>
            <ta e="T211" id="Seg_9258" s="T210">v</ta>
            <ta e="T212" id="Seg_9259" s="T211">v</ta>
            <ta e="T213" id="Seg_9260" s="T212">v</ta>
            <ta e="T214" id="Seg_9261" s="T213">n</ta>
            <ta e="T215" id="Seg_9262" s="T214">n</ta>
            <ta e="T216" id="Seg_9263" s="T215">interj</ta>
            <ta e="T217" id="Seg_9264" s="T216">que</ta>
            <ta e="T218" id="Seg_9265" s="T217">adv</ta>
            <ta e="T219" id="Seg_9266" s="T218">n</ta>
            <ta e="T220" id="Seg_9267" s="T219">n</ta>
            <ta e="T221" id="Seg_9268" s="T220">cop</ta>
            <ta e="T222" id="Seg_9269" s="T221">n</ta>
            <ta e="T223" id="Seg_9270" s="T222">n</ta>
            <ta e="T224" id="Seg_9271" s="T223">v</ta>
            <ta e="T225" id="Seg_9272" s="T224">v</ta>
            <ta e="T226" id="Seg_9273" s="T225">v</ta>
            <ta e="T227" id="Seg_9274" s="T226">aux</ta>
            <ta e="T228" id="Seg_9275" s="T227">n</ta>
            <ta e="T229" id="Seg_9276" s="T228">v</ta>
            <ta e="T230" id="Seg_9277" s="T229">dempro</ta>
            <ta e="T231" id="Seg_9278" s="T230">cardnum</ta>
            <ta e="T232" id="Seg_9279" s="T231">n</ta>
            <ta e="T233" id="Seg_9280" s="T232">v</ta>
            <ta e="T234" id="Seg_9281" s="T233">v</ta>
            <ta e="T235" id="Seg_9282" s="T234">n</ta>
            <ta e="T236" id="Seg_9283" s="T235">n</ta>
            <ta e="T237" id="Seg_9284" s="T236">v</ta>
            <ta e="T238" id="Seg_9285" s="T237">aux</ta>
            <ta e="T239" id="Seg_9286" s="T238">que</ta>
            <ta e="T240" id="Seg_9287" s="T239">adv</ta>
            <ta e="T241" id="Seg_9288" s="T240">v</ta>
            <ta e="T242" id="Seg_9289" s="T241">ptcl</ta>
            <ta e="T243" id="Seg_9290" s="T242">dempro</ta>
            <ta e="T244" id="Seg_9291" s="T243">n</ta>
            <ta e="T245" id="Seg_9292" s="T244">ptcl</ta>
            <ta e="T246" id="Seg_9293" s="T245">v</ta>
            <ta e="T247" id="Seg_9294" s="T246">aux</ta>
            <ta e="T248" id="Seg_9295" s="T247">n</ta>
            <ta e="T249" id="Seg_9296" s="T248">n</ta>
            <ta e="T250" id="Seg_9297" s="T249">n</ta>
            <ta e="T251" id="Seg_9298" s="T250">n</ta>
            <ta e="T252" id="Seg_9299" s="T251">v</ta>
            <ta e="T253" id="Seg_9300" s="T252">n</ta>
            <ta e="T254" id="Seg_9301" s="T253">v</ta>
            <ta e="T255" id="Seg_9302" s="T254">n</ta>
            <ta e="T256" id="Seg_9303" s="T255">v</ta>
            <ta e="T257" id="Seg_9304" s="T256">v</ta>
            <ta e="T258" id="Seg_9305" s="T257">n</ta>
            <ta e="T259" id="Seg_9306" s="T258">n</ta>
            <ta e="T260" id="Seg_9307" s="T259">post</ta>
            <ta e="T261" id="Seg_9308" s="T260">v</ta>
            <ta e="T262" id="Seg_9309" s="T261">v</ta>
            <ta e="T263" id="Seg_9310" s="T262">ptcl</ta>
            <ta e="T264" id="Seg_9311" s="T263">interj</ta>
            <ta e="T265" id="Seg_9312" s="T264">ptcl</ta>
            <ta e="T266" id="Seg_9313" s="T265">v</ta>
            <ta e="T267" id="Seg_9314" s="T266">aux</ta>
            <ta e="T268" id="Seg_9315" s="T267">v</ta>
            <ta e="T269" id="Seg_9316" s="T268">post</ta>
            <ta e="T270" id="Seg_9317" s="T269">n</ta>
            <ta e="T271" id="Seg_9318" s="T270">v</ta>
            <ta e="T272" id="Seg_9319" s="T271">v</ta>
            <ta e="T273" id="Seg_9320" s="T272">dempro</ta>
            <ta e="T274" id="Seg_9321" s="T273">n</ta>
            <ta e="T275" id="Seg_9322" s="T274">ptcl</ta>
            <ta e="T276" id="Seg_9323" s="T275">v</ta>
            <ta e="T277" id="Seg_9324" s="T276">v</ta>
            <ta e="T278" id="Seg_9325" s="T277">pers</ta>
            <ta e="T279" id="Seg_9326" s="T278">pers</ta>
            <ta e="T280" id="Seg_9327" s="T279">v</ta>
            <ta e="T281" id="Seg_9328" s="T280">n</ta>
            <ta e="T282" id="Seg_9329" s="T281">n</ta>
            <ta e="T283" id="Seg_9330" s="T282">v</ta>
            <ta e="T284" id="Seg_9331" s="T283">dempro</ta>
            <ta e="T285" id="Seg_9332" s="T284">n</ta>
            <ta e="T286" id="Seg_9333" s="T285">v</ta>
            <ta e="T287" id="Seg_9334" s="T286">v</ta>
            <ta e="T288" id="Seg_9335" s="T287">conj</ta>
            <ta e="T289" id="Seg_9336" s="T288">n</ta>
            <ta e="T290" id="Seg_9337" s="T289">post</ta>
            <ta e="T291" id="Seg_9338" s="T290">v</ta>
            <ta e="T292" id="Seg_9339" s="T291">que</ta>
            <ta e="T293" id="Seg_9340" s="T292">ptcl</ta>
            <ta e="T294" id="Seg_9341" s="T293">cop</ta>
            <ta e="T295" id="Seg_9342" s="T294">n</ta>
            <ta e="T296" id="Seg_9343" s="T295">n</ta>
            <ta e="T297" id="Seg_9344" s="T296">n</ta>
            <ta e="T298" id="Seg_9345" s="T297">n</ta>
            <ta e="T299" id="Seg_9346" s="T298">v</ta>
            <ta e="T300" id="Seg_9347" s="T299">aux</ta>
            <ta e="T301" id="Seg_9348" s="T300">dempro</ta>
            <ta e="T302" id="Seg_9349" s="T301">v</ta>
            <ta e="T303" id="Seg_9350" s="T302">post</ta>
            <ta e="T304" id="Seg_9351" s="T303">n</ta>
            <ta e="T305" id="Seg_9352" s="T304">n</ta>
            <ta e="T306" id="Seg_9353" s="T305">adj</ta>
            <ta e="T307" id="Seg_9354" s="T306">n</ta>
            <ta e="T308" id="Seg_9355" s="T307">v</ta>
            <ta e="T309" id="Seg_9356" s="T308">n</ta>
            <ta e="T310" id="Seg_9357" s="T309">n</ta>
            <ta e="T311" id="Seg_9358" s="T310">n</ta>
            <ta e="T312" id="Seg_9359" s="T311">v</ta>
            <ta e="T313" id="Seg_9360" s="T312">aux</ta>
            <ta e="T314" id="Seg_9361" s="T313">adv</ta>
            <ta e="T315" id="Seg_9362" s="T314">que</ta>
            <ta e="T316" id="Seg_9363" s="T315">ptcl</ta>
            <ta e="T317" id="Seg_9364" s="T316">cop</ta>
            <ta e="T318" id="Seg_9365" s="T317">post</ta>
            <ta e="T319" id="Seg_9366" s="T318">n</ta>
            <ta e="T320" id="Seg_9367" s="T319">n</ta>
            <ta e="T321" id="Seg_9368" s="T320">n</ta>
            <ta e="T322" id="Seg_9369" s="T321">v</ta>
            <ta e="T323" id="Seg_9370" s="T322">interj</ta>
            <ta e="T324" id="Seg_9371" s="T323">v</ta>
            <ta e="T325" id="Seg_9372" s="T324">v</ta>
            <ta e="T326" id="Seg_9373" s="T325">n</ta>
            <ta e="T327" id="Seg_9374" s="T326">v</ta>
            <ta e="T328" id="Seg_9375" s="T327">adv</ta>
            <ta e="T329" id="Seg_9376" s="T328">v</ta>
            <ta e="T330" id="Seg_9377" s="T329">n</ta>
            <ta e="T331" id="Seg_9378" s="T330">v</ta>
            <ta e="T332" id="Seg_9379" s="T331">n</ta>
            <ta e="T333" id="Seg_9380" s="T332">n</ta>
            <ta e="T334" id="Seg_9381" s="T333">adj</ta>
            <ta e="T335" id="Seg_9382" s="T334">que</ta>
            <ta e="T336" id="Seg_9383" s="T335">ptcl</ta>
            <ta e="T337" id="Seg_9384" s="T336">cop</ta>
            <ta e="T338" id="Seg_9385" s="T337">n</ta>
            <ta e="T339" id="Seg_9386" s="T338">v</ta>
            <ta e="T340" id="Seg_9387" s="T339">n</ta>
            <ta e="T341" id="Seg_9388" s="T340">n</ta>
            <ta e="T342" id="Seg_9389" s="T341">v</ta>
            <ta e="T343" id="Seg_9390" s="T342">post</ta>
            <ta e="T344" id="Seg_9391" s="T343">v</ta>
            <ta e="T345" id="Seg_9392" s="T344">aux</ta>
            <ta e="T346" id="Seg_9393" s="T345">n</ta>
            <ta e="T347" id="Seg_9394" s="T346">ptcl</ta>
            <ta e="T348" id="Seg_9395" s="T347">v</ta>
            <ta e="T349" id="Seg_9396" s="T348">v</ta>
            <ta e="T350" id="Seg_9397" s="T349">n</ta>
            <ta e="T351" id="Seg_9398" s="T350">n</ta>
            <ta e="T352" id="Seg_9399" s="T351">v</ta>
            <ta e="T353" id="Seg_9400" s="T352">n</ta>
            <ta e="T354" id="Seg_9401" s="T353">v</ta>
            <ta e="T355" id="Seg_9402" s="T354">v</ta>
            <ta e="T356" id="Seg_9403" s="T355">v</ta>
            <ta e="T357" id="Seg_9404" s="T356">aux</ta>
            <ta e="T358" id="Seg_9405" s="T357">dempro</ta>
            <ta e="T359" id="Seg_9406" s="T358">n</ta>
            <ta e="T360" id="Seg_9407" s="T359">v</ta>
            <ta e="T361" id="Seg_9408" s="T360">post</ta>
            <ta e="T362" id="Seg_9409" s="T361">v</ta>
            <ta e="T363" id="Seg_9410" s="T362">aux</ta>
            <ta e="T364" id="Seg_9411" s="T363">adj</ta>
            <ta e="T365" id="Seg_9412" s="T364">que</ta>
            <ta e="T366" id="Seg_9413" s="T365">ptcl</ta>
            <ta e="T367" id="Seg_9414" s="T366">ptcl</ta>
            <ta e="T368" id="Seg_9415" s="T367">v</ta>
            <ta e="T369" id="Seg_9416" s="T368">n</ta>
            <ta e="T370" id="Seg_9417" s="T369">v</ta>
            <ta e="T371" id="Seg_9418" s="T370">n</ta>
            <ta e="T372" id="Seg_9419" s="T371">v</ta>
            <ta e="T373" id="Seg_9420" s="T372">ptcl</ta>
            <ta e="T374" id="Seg_9421" s="T373">v</ta>
            <ta e="T375" id="Seg_9422" s="T374">adv</ta>
            <ta e="T376" id="Seg_9423" s="T375">v</ta>
            <ta e="T377" id="Seg_9424" s="T376">n</ta>
            <ta e="T378" id="Seg_9425" s="T377">n</ta>
            <ta e="T379" id="Seg_9426" s="T378">n</ta>
            <ta e="T380" id="Seg_9427" s="T379">v</ta>
            <ta e="T381" id="Seg_9428" s="T380">conj</ta>
            <ta e="T382" id="Seg_9429" s="T381">ordnum</ta>
            <ta e="T383" id="Seg_9430" s="T382">n</ta>
            <ta e="T384" id="Seg_9431" s="T383">n</ta>
            <ta e="T385" id="Seg_9432" s="T384">ptcl</ta>
            <ta e="T386" id="Seg_9433" s="T385">n</ta>
            <ta e="T387" id="Seg_9434" s="T386">n</ta>
            <ta e="T388" id="Seg_9435" s="T387">adv</ta>
            <ta e="T389" id="Seg_9436" s="T388">v</ta>
            <ta e="T390" id="Seg_9437" s="T389">n</ta>
            <ta e="T391" id="Seg_9438" s="T390">v</ta>
            <ta e="T392" id="Seg_9439" s="T391">aux</ta>
            <ta e="T393" id="Seg_9440" s="T392">post</ta>
            <ta e="T394" id="Seg_9441" s="T393">n</ta>
            <ta e="T395" id="Seg_9442" s="T394">v</ta>
            <ta e="T396" id="Seg_9443" s="T395">n</ta>
            <ta e="T397" id="Seg_9444" s="T396">n</ta>
            <ta e="T398" id="Seg_9445" s="T397">n</ta>
            <ta e="T399" id="Seg_9446" s="T398">adj</ta>
            <ta e="T400" id="Seg_9447" s="T399">n</ta>
            <ta e="T401" id="Seg_9448" s="T400">v</ta>
            <ta e="T402" id="Seg_9449" s="T401">ptcl</ta>
            <ta e="T403" id="Seg_9450" s="T402">n</ta>
            <ta e="T404" id="Seg_9451" s="T403">ptcl</ta>
            <ta e="T405" id="Seg_9452" s="T404">v</ta>
            <ta e="T406" id="Seg_9453" s="T405">aux</ta>
            <ta e="T407" id="Seg_9454" s="T406">ordnum</ta>
            <ta e="T408" id="Seg_9455" s="T407">n</ta>
            <ta e="T409" id="Seg_9456" s="T408">adj</ta>
            <ta e="T410" id="Seg_9457" s="T409">n</ta>
            <ta e="T411" id="Seg_9458" s="T410">n</ta>
            <ta e="T412" id="Seg_9459" s="T411">v</ta>
            <ta e="T413" id="Seg_9460" s="T412">aux</ta>
            <ta e="T414" id="Seg_9461" s="T413">adj</ta>
            <ta e="T415" id="Seg_9462" s="T414">n</ta>
            <ta e="T416" id="Seg_9463" s="T415">n</ta>
            <ta e="T417" id="Seg_9464" s="T416">aux</ta>
            <ta e="T418" id="Seg_9465" s="T417">n</ta>
            <ta e="T419" id="Seg_9466" s="T418">n</ta>
            <ta e="T420" id="Seg_9467" s="T419">v</ta>
            <ta e="T421" id="Seg_9468" s="T420">post</ta>
            <ta e="T422" id="Seg_9469" s="T421">v</ta>
            <ta e="T423" id="Seg_9470" s="T422">ptcl</ta>
            <ta e="T424" id="Seg_9471" s="T423">n</ta>
            <ta e="T425" id="Seg_9472" s="T424">v</ta>
            <ta e="T426" id="Seg_9473" s="T425">v</ta>
            <ta e="T427" id="Seg_9474" s="T426">dempro</ta>
            <ta e="T428" id="Seg_9475" s="T427">n</ta>
            <ta e="T429" id="Seg_9476" s="T428">n</ta>
            <ta e="T430" id="Seg_9477" s="T429">n</ta>
            <ta e="T431" id="Seg_9478" s="T430">v</ta>
            <ta e="T432" id="Seg_9479" s="T431">que</ta>
            <ta e="T433" id="Seg_9480" s="T432">ptcl</ta>
            <ta e="T434" id="Seg_9481" s="T433">v</ta>
            <ta e="T435" id="Seg_9482" s="T434">aux</ta>
            <ta e="T436" id="Seg_9483" s="T435">adv</ta>
            <ta e="T437" id="Seg_9484" s="T436">n</ta>
            <ta e="T438" id="Seg_9485" s="T437">n</ta>
            <ta e="T439" id="Seg_9486" s="T438">v</ta>
            <ta e="T440" id="Seg_9487" s="T439">aux</ta>
            <ta e="T441" id="Seg_9488" s="T440">post</ta>
            <ta e="T442" id="Seg_9489" s="T441">v</ta>
            <ta e="T443" id="Seg_9490" s="T442">n</ta>
            <ta e="T444" id="Seg_9491" s="T443">v</ta>
            <ta e="T445" id="Seg_9492" s="T444">v</ta>
            <ta e="T446" id="Seg_9493" s="T445">ptcl</ta>
            <ta e="T447" id="Seg_9494" s="T446">n</ta>
            <ta e="T448" id="Seg_9495" s="T447">v</ta>
            <ta e="T449" id="Seg_9496" s="T448">n</ta>
            <ta e="T450" id="Seg_9497" s="T449">n</ta>
            <ta e="T451" id="Seg_9498" s="T450">v</ta>
            <ta e="T452" id="Seg_9499" s="T451">aux</ta>
            <ta e="T453" id="Seg_9500" s="T452">que</ta>
            <ta e="T454" id="Seg_9501" s="T453">ptcl</ta>
            <ta e="T455" id="Seg_9502" s="T454">cop</ta>
            <ta e="T456" id="Seg_9503" s="T455">n</ta>
            <ta e="T457" id="Seg_9504" s="T456">v</ta>
            <ta e="T458" id="Seg_9505" s="T457">aux</ta>
            <ta e="T459" id="Seg_9506" s="T458">n</ta>
            <ta e="T460" id="Seg_9507" s="T459">n</ta>
            <ta e="T461" id="Seg_9508" s="T460">v</ta>
            <ta e="T462" id="Seg_9509" s="T461">v</ta>
            <ta e="T463" id="Seg_9510" s="T462">v</ta>
            <ta e="T464" id="Seg_9511" s="T463">v</ta>
            <ta e="T465" id="Seg_9512" s="T464">n</ta>
            <ta e="T466" id="Seg_9513" s="T465">n</ta>
            <ta e="T467" id="Seg_9514" s="T466">n</ta>
            <ta e="T468" id="Seg_9515" s="T467">v</ta>
            <ta e="T469" id="Seg_9516" s="T468">n</ta>
            <ta e="T470" id="Seg_9517" s="T469">v</ta>
            <ta e="T471" id="Seg_9518" s="T470">n</ta>
            <ta e="T472" id="Seg_9519" s="T471">v</ta>
            <ta e="T473" id="Seg_9520" s="T472">pers</ta>
            <ta e="T474" id="Seg_9521" s="T473">adj</ta>
            <ta e="T475" id="Seg_9522" s="T474">n</ta>
            <ta e="T476" id="Seg_9523" s="T475">cop</ta>
            <ta e="T477" id="Seg_9524" s="T476">post</ta>
            <ta e="T478" id="Seg_9525" s="T477">adj</ta>
            <ta e="T479" id="Seg_9526" s="T478">n</ta>
            <ta e="T480" id="Seg_9527" s="T479">n</ta>
            <ta e="T481" id="Seg_9528" s="T480">n</ta>
            <ta e="T482" id="Seg_9529" s="T481">v</ta>
            <ta e="T483" id="Seg_9530" s="T482">pers</ta>
            <ta e="T484" id="Seg_9531" s="T483">v</ta>
            <ta e="T485" id="Seg_9532" s="T484">v</ta>
            <ta e="T486" id="Seg_9533" s="T485">aux</ta>
            <ta e="T487" id="Seg_9534" s="T486">v</ta>
            <ta e="T488" id="Seg_9535" s="T487">adj</ta>
            <ta e="T489" id="Seg_9536" s="T488">propr</ta>
            <ta e="T490" id="Seg_9537" s="T489">n</ta>
            <ta e="T491" id="Seg_9538" s="T490">pers</ta>
            <ta e="T492" id="Seg_9539" s="T491">ptcl</ta>
            <ta e="T493" id="Seg_9540" s="T492">que</ta>
            <ta e="T494" id="Seg_9541" s="T493">ptcl</ta>
            <ta e="T495" id="Seg_9542" s="T494">v</ta>
            <ta e="T496" id="Seg_9543" s="T495">adj</ta>
            <ta e="T497" id="Seg_9544" s="T496">n</ta>
            <ta e="T498" id="Seg_9545" s="T497">n</ta>
            <ta e="T499" id="Seg_9546" s="T498">cop</ta>
            <ta e="T500" id="Seg_9547" s="T499">n</ta>
            <ta e="T501" id="Seg_9548" s="T500">n</ta>
            <ta e="T502" id="Seg_9549" s="T501">n</ta>
            <ta e="T503" id="Seg_9550" s="T502">v</ta>
            <ta e="T504" id="Seg_9551" s="T503">adj</ta>
            <ta e="T505" id="Seg_9552" s="T504">v</ta>
            <ta e="T506" id="Seg_9553" s="T505">v</ta>
            <ta e="T507" id="Seg_9554" s="T506">pers</ta>
            <ta e="T508" id="Seg_9555" s="T507">ptcl</ta>
            <ta e="T509" id="Seg_9556" s="T508">n</ta>
            <ta e="T510" id="Seg_9557" s="T509">adv</ta>
            <ta e="T511" id="Seg_9558" s="T510">v</ta>
            <ta e="T512" id="Seg_9559" s="T511">v</ta>
            <ta e="T513" id="Seg_9560" s="T512">adj</ta>
            <ta e="T514" id="Seg_9561" s="T513">n</ta>
            <ta e="T515" id="Seg_9562" s="T514">cop</ta>
            <ta e="T516" id="Seg_9563" s="T515">adj</ta>
            <ta e="T517" id="Seg_9564" s="T516">v</ta>
            <ta e="T518" id="Seg_9565" s="T517">v</ta>
            <ta e="T519" id="Seg_9566" s="T518">ptcl</ta>
            <ta e="T520" id="Seg_9567" s="T519">adj</ta>
            <ta e="T521" id="Seg_9568" s="T520">adv</ta>
            <ta e="T522" id="Seg_9569" s="T521">v</ta>
            <ta e="T523" id="Seg_9570" s="T522">aux</ta>
            <ta e="T524" id="Seg_9571" s="T523">v</ta>
            <ta e="T525" id="Seg_9572" s="T524">ordnum</ta>
            <ta e="T526" id="Seg_9573" s="T525">n</ta>
            <ta e="T527" id="Seg_9574" s="T526">n</ta>
            <ta e="T528" id="Seg_9575" s="T527">n</ta>
            <ta e="T529" id="Seg_9576" s="T528">dempro</ta>
            <ta e="T530" id="Seg_9577" s="T529">v</ta>
            <ta e="T531" id="Seg_9578" s="T530">ptcl</ta>
            <ta e="T532" id="Seg_9579" s="T531">n</ta>
            <ta e="T533" id="Seg_9580" s="T532">n</ta>
            <ta e="T534" id="Seg_9581" s="T533">v</ta>
            <ta e="T535" id="Seg_9582" s="T534">v</ta>
            <ta e="T536" id="Seg_9583" s="T535">n</ta>
            <ta e="T537" id="Seg_9584" s="T536">v</ta>
            <ta e="T538" id="Seg_9585" s="T537">conj</ta>
            <ta e="T539" id="Seg_9586" s="T538">v</ta>
            <ta e="T540" id="Seg_9587" s="T539">aux</ta>
            <ta e="T541" id="Seg_9588" s="T540">n</ta>
            <ta e="T542" id="Seg_9589" s="T541">n</ta>
            <ta e="T543" id="Seg_9590" s="T542">n</ta>
            <ta e="T544" id="Seg_9591" s="T543">v</ta>
            <ta e="T545" id="Seg_9592" s="T544">aux</ta>
            <ta e="T546" id="Seg_9593" s="T545">n</ta>
            <ta e="T547" id="Seg_9594" s="T546">adv</ta>
            <ta e="T548" id="Seg_9595" s="T547">v</ta>
            <ta e="T549" id="Seg_9596" s="T548">aux</ta>
            <ta e="T550" id="Seg_9597" s="T549">que</ta>
            <ta e="T551" id="Seg_9598" s="T550">adv</ta>
            <ta e="T552" id="Seg_9599" s="T551">cop</ta>
            <ta e="T553" id="Seg_9600" s="T552">n</ta>
            <ta e="T554" id="Seg_9601" s="T553">n</ta>
            <ta e="T555" id="Seg_9602" s="T554">adj</ta>
            <ta e="T556" id="Seg_9603" s="T555">n</ta>
            <ta e="T557" id="Seg_9604" s="T556">v</ta>
            <ta e="T558" id="Seg_9605" s="T557">n</ta>
            <ta e="T559" id="Seg_9606" s="T558">n</ta>
            <ta e="T560" id="Seg_9607" s="T559">v</ta>
            <ta e="T561" id="Seg_9608" s="T560">n</ta>
            <ta e="T562" id="Seg_9609" s="T561">n</ta>
            <ta e="T563" id="Seg_9610" s="T562">post</ta>
            <ta e="T564" id="Seg_9611" s="T563">n</ta>
            <ta e="T565" id="Seg_9612" s="T564">v</ta>
            <ta e="T566" id="Seg_9613" s="T565">post</ta>
            <ta e="T567" id="Seg_9614" s="T566">n</ta>
            <ta e="T568" id="Seg_9615" s="T567">n</ta>
            <ta e="T569" id="Seg_9616" s="T568">adj</ta>
            <ta e="T570" id="Seg_9617" s="T569">n</ta>
            <ta e="T571" id="Seg_9618" s="T570">n</ta>
            <ta e="T572" id="Seg_9619" s="T571">v</ta>
            <ta e="T573" id="Seg_9620" s="T572">aux</ta>
            <ta e="T574" id="Seg_9621" s="T573">n</ta>
            <ta e="T575" id="Seg_9622" s="T574">n</ta>
            <ta e="T576" id="Seg_9623" s="T575">v</ta>
            <ta e="T577" id="Seg_9624" s="T576">v</ta>
            <ta e="T578" id="Seg_9625" s="T577">v</ta>
            <ta e="T579" id="Seg_9626" s="T578">adj</ta>
            <ta e="T580" id="Seg_9627" s="T579">ptcl</ta>
            <ta e="T581" id="Seg_9628" s="T580">n</ta>
            <ta e="T582" id="Seg_9629" s="T581">n</ta>
            <ta e="T583" id="Seg_9630" s="T582">n</ta>
            <ta e="T584" id="Seg_9631" s="T583">n</ta>
            <ta e="T585" id="Seg_9632" s="T584">n</ta>
            <ta e="T586" id="Seg_9633" s="T585">v</ta>
            <ta e="T587" id="Seg_9634" s="T586">interj</ta>
            <ta e="T588" id="Seg_9635" s="T587">n</ta>
            <ta e="T589" id="Seg_9636" s="T588">n</ta>
            <ta e="T590" id="Seg_9637" s="T589">n</ta>
            <ta e="T591" id="Seg_9638" s="T590">v</ta>
            <ta e="T592" id="Seg_9639" s="T591">v</ta>
            <ta e="T593" id="Seg_9640" s="T592">conj</ta>
            <ta e="T594" id="Seg_9641" s="T593">n</ta>
            <ta e="T595" id="Seg_9642" s="T594">v</ta>
            <ta e="T596" id="Seg_9643" s="T595">v</ta>
            <ta e="T597" id="Seg_9644" s="T596">post</ta>
            <ta e="T598" id="Seg_9645" s="T597">n</ta>
            <ta e="T599" id="Seg_9646" s="T598">v</ta>
            <ta e="T600" id="Seg_9647" s="T599">v</ta>
            <ta e="T601" id="Seg_9648" s="T600">n</ta>
            <ta e="T602" id="Seg_9649" s="T601">adv</ta>
            <ta e="T603" id="Seg_9650" s="T602">v</ta>
            <ta e="T604" id="Seg_9651" s="T603">post</ta>
            <ta e="T605" id="Seg_9652" s="T604">n</ta>
            <ta e="T606" id="Seg_9653" s="T605">v</ta>
            <ta e="T607" id="Seg_9654" s="T606">aux</ta>
            <ta e="T608" id="Seg_9655" s="T607">n</ta>
            <ta e="T609" id="Seg_9656" s="T608">n</ta>
            <ta e="T610" id="Seg_9657" s="T609">v</ta>
            <ta e="T611" id="Seg_9658" s="T610">n</ta>
            <ta e="T612" id="Seg_9659" s="T611">ptcl</ta>
            <ta e="T613" id="Seg_9660" s="T612">v</ta>
            <ta e="T614" id="Seg_9661" s="T613">que</ta>
            <ta e="T615" id="Seg_9662" s="T614">adv</ta>
            <ta e="T616" id="Seg_9663" s="T615">v</ta>
            <ta e="T617" id="Seg_9664" s="T616">n</ta>
            <ta e="T618" id="Seg_9665" s="T617">n</ta>
            <ta e="T619" id="Seg_9666" s="T618">v</ta>
            <ta e="T620" id="Seg_9667" s="T619">aux</ta>
            <ta e="T621" id="Seg_9668" s="T620">n</ta>
            <ta e="T622" id="Seg_9669" s="T621">n</ta>
            <ta e="T623" id="Seg_9670" s="T622">adv</ta>
            <ta e="T624" id="Seg_9671" s="T623">v</ta>
            <ta e="T625" id="Seg_9672" s="T624">dempro</ta>
            <ta e="T626" id="Seg_9673" s="T625">n</ta>
            <ta e="T627" id="Seg_9674" s="T626">v</ta>
            <ta e="T628" id="Seg_9675" s="T627">v</ta>
            <ta e="T629" id="Seg_9676" s="T628">aux</ta>
            <ta e="T630" id="Seg_9677" s="T629">aux</ta>
            <ta e="T631" id="Seg_9678" s="T630">v</ta>
            <ta e="T632" id="Seg_9679" s="T631">n</ta>
            <ta e="T633" id="Seg_9680" s="T632">n</ta>
            <ta e="T634" id="Seg_9681" s="T633">n</ta>
            <ta e="T635" id="Seg_9682" s="T634">n</ta>
            <ta e="T636" id="Seg_9683" s="T635">v</ta>
            <ta e="T637" id="Seg_9684" s="T636">v</ta>
            <ta e="T638" id="Seg_9685" s="T637">n</ta>
            <ta e="T639" id="Seg_9686" s="T638">v</ta>
            <ta e="T640" id="Seg_9687" s="T639">v</ta>
            <ta e="T641" id="Seg_9688" s="T640">pers</ta>
            <ta e="T642" id="Seg_9689" s="T641">v</ta>
            <ta e="T643" id="Seg_9690" s="T642">aux</ta>
            <ta e="T644" id="Seg_9691" s="T643">v</ta>
            <ta e="T645" id="Seg_9692" s="T644">n</ta>
            <ta e="T646" id="Seg_9693" s="T645">v</ta>
            <ta e="T647" id="Seg_9694" s="T646">n</ta>
            <ta e="T648" id="Seg_9695" s="T647">adv</ta>
            <ta e="T649" id="Seg_9696" s="T648">v</ta>
            <ta e="T650" id="Seg_9697" s="T649">v</ta>
            <ta e="T651" id="Seg_9698" s="T650">aux</ta>
            <ta e="T652" id="Seg_9699" s="T651">adv</ta>
            <ta e="T653" id="Seg_9700" s="T652">n</ta>
            <ta e="T654" id="Seg_9701" s="T653">v</ta>
            <ta e="T655" id="Seg_9702" s="T654">n</ta>
            <ta e="T656" id="Seg_9703" s="T655">v</ta>
            <ta e="T657" id="Seg_9704" s="T656">aux</ta>
            <ta e="T658" id="Seg_9705" s="T657">adv</ta>
            <ta e="T659" id="Seg_9706" s="T658">que</ta>
            <ta e="T660" id="Seg_9707" s="T659">ptcl</ta>
            <ta e="T661" id="Seg_9708" s="T660">adv</ta>
            <ta e="T662" id="Seg_9709" s="T661">cop</ta>
            <ta e="T663" id="Seg_9710" s="T662">n</ta>
            <ta e="T664" id="Seg_9711" s="T663">adj</ta>
            <ta e="T665" id="Seg_9712" s="T664">n</ta>
            <ta e="T666" id="Seg_9713" s="T665">n</ta>
            <ta e="T667" id="Seg_9714" s="T666">v</ta>
            <ta e="T668" id="Seg_9715" s="T667">v</ta>
            <ta e="T669" id="Seg_9716" s="T668">post</ta>
            <ta e="T670" id="Seg_9717" s="T669">n</ta>
            <ta e="T671" id="Seg_9718" s="T670">cardnum</ta>
            <ta e="T672" id="Seg_9719" s="T671">n</ta>
            <ta e="T673" id="Seg_9720" s="T672">v</ta>
            <ta e="T674" id="Seg_9721" s="T673">aux</ta>
            <ta e="T675" id="Seg_9722" s="T674">n</ta>
            <ta e="T676" id="Seg_9723" s="T675">n</ta>
            <ta e="T677" id="Seg_9724" s="T676">n</ta>
            <ta e="T678" id="Seg_9725" s="T677">n</ta>
            <ta e="T679" id="Seg_9726" s="T678">adj</ta>
            <ta e="T680" id="Seg_9727" s="T679">n</ta>
            <ta e="T681" id="Seg_9728" s="T680">v</ta>
            <ta e="T682" id="Seg_9729" s="T681">n</ta>
            <ta e="T683" id="Seg_9730" s="T682">n</ta>
            <ta e="T684" id="Seg_9731" s="T683">n</ta>
            <ta e="T685" id="Seg_9732" s="T684">adj</ta>
            <ta e="T686" id="Seg_9733" s="T685">n</ta>
            <ta e="T687" id="Seg_9734" s="T686">n</ta>
            <ta e="T688" id="Seg_9735" s="T687">v</ta>
            <ta e="T689" id="Seg_9736" s="T688">n</ta>
            <ta e="T690" id="Seg_9737" s="T689">n</ta>
            <ta e="T691" id="Seg_9738" s="T690">adv</ta>
            <ta e="T692" id="Seg_9739" s="T691">n</ta>
            <ta e="T693" id="Seg_9740" s="T692">v</ta>
            <ta e="T694" id="Seg_9741" s="T693">adv</ta>
            <ta e="T695" id="Seg_9742" s="T694">n</ta>
            <ta e="T696" id="Seg_9743" s="T695">n</ta>
            <ta e="T697" id="Seg_9744" s="T696">v</ta>
            <ta e="T698" id="Seg_9745" s="T697">ptcl</ta>
            <ta e="T699" id="Seg_9746" s="T698">n</ta>
            <ta e="T700" id="Seg_9747" s="T699">v</ta>
            <ta e="T701" id="Seg_9748" s="T700">que</ta>
            <ta e="T702" id="Seg_9749" s="T701">v</ta>
            <ta e="T703" id="Seg_9750" s="T702">dempro</ta>
            <ta e="T704" id="Seg_9751" s="T703">adv</ta>
            <ta e="T705" id="Seg_9752" s="T704">v</ta>
            <ta e="T706" id="Seg_9753" s="T705">cardnum</ta>
            <ta e="T707" id="Seg_9754" s="T706">n</ta>
            <ta e="T708" id="Seg_9755" s="T707">n</ta>
            <ta e="T709" id="Seg_9756" s="T708">cardnum</ta>
            <ta e="T710" id="Seg_9757" s="T709">adj</ta>
            <ta e="T711" id="Seg_9758" s="T710">n</ta>
            <ta e="T712" id="Seg_9759" s="T711">v</ta>
            <ta e="T713" id="Seg_9760" s="T712">n</ta>
            <ta e="T714" id="Seg_9761" s="T713">n</ta>
            <ta e="T715" id="Seg_9762" s="T714">v</ta>
            <ta e="T716" id="Seg_9763" s="T715">n</ta>
            <ta e="T717" id="Seg_9764" s="T716">n</ta>
            <ta e="T718" id="Seg_9765" s="T717">cardnum</ta>
            <ta e="T719" id="Seg_9766" s="T718">n</ta>
            <ta e="T720" id="Seg_9767" s="T719">v</ta>
            <ta e="T721" id="Seg_9768" s="T720">aux</ta>
            <ta e="T722" id="Seg_9769" s="T721">n</ta>
            <ta e="T723" id="Seg_9770" s="T722">n</ta>
            <ta e="T724" id="Seg_9771" s="T723">n</ta>
            <ta e="T725" id="Seg_9772" s="T724">n</ta>
            <ta e="T726" id="Seg_9773" s="T725">v</ta>
            <ta e="T727" id="Seg_9774" s="T726">aux</ta>
            <ta e="T728" id="Seg_9775" s="T727">v</ta>
            <ta e="T729" id="Seg_9776" s="T728">aux</ta>
            <ta e="T730" id="Seg_9777" s="T729">post</ta>
            <ta e="T731" id="Seg_9778" s="T730">v</ta>
            <ta e="T732" id="Seg_9779" s="T731">n</ta>
            <ta e="T733" id="Seg_9780" s="T732">pers</ta>
            <ta e="T734" id="Seg_9781" s="T733">n</ta>
            <ta e="T735" id="Seg_9782" s="T734">cop</ta>
            <ta e="T736" id="Seg_9783" s="T735">n</ta>
            <ta e="T737" id="Seg_9784" s="T736">v</ta>
            <ta e="T738" id="Seg_9785" s="T737">n</ta>
            <ta e="T739" id="Seg_9786" s="T738">post</ta>
            <ta e="T740" id="Seg_9787" s="T739">n</ta>
            <ta e="T741" id="Seg_9788" s="T740">cop</ta>
            <ta e="T742" id="Seg_9789" s="T741">v</ta>
            <ta e="T743" id="Seg_9790" s="T742">cardnum</ta>
            <ta e="T744" id="Seg_9791" s="T743">n</ta>
            <ta e="T745" id="Seg_9792" s="T744">adv</ta>
            <ta e="T746" id="Seg_9793" s="T745">v</ta>
            <ta e="T747" id="Seg_9794" s="T746">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_9795" s="T0">adv:Time</ta>
            <ta e="T3" id="Seg_9796" s="T2">np.h:Th</ta>
            <ta e="T5" id="Seg_9797" s="T4">0.3.h:Poss np.h:Th</ta>
            <ta e="T9" id="Seg_9798" s="T8">adv:Time</ta>
            <ta e="T11" id="Seg_9799" s="T10">np.h:E</ta>
            <ta e="T14" id="Seg_9800" s="T13">pro:Cau</ta>
            <ta e="T15" id="Seg_9801" s="T14">pro.h:Poss</ta>
            <ta e="T16" id="Seg_9802" s="T15">np.h:Th</ta>
            <ta e="T18" id="Seg_9803" s="T17">np:So</ta>
            <ta e="T20" id="Seg_9804" s="T18">0.1.h:A</ta>
            <ta e="T23" id="Seg_9805" s="T22">0.3.h:A</ta>
            <ta e="T24" id="Seg_9806" s="T23">np.h:A</ta>
            <ta e="T26" id="Seg_9807" s="T25">pro:Th</ta>
            <ta e="T27" id="Seg_9808" s="T26">0.2.h:E</ta>
            <ta e="T29" id="Seg_9809" s="T28">adv:Time</ta>
            <ta e="T30" id="Seg_9810" s="T29">pro.h:A</ta>
            <ta e="T31" id="Seg_9811" s="T30">np.h:Th</ta>
            <ta e="T34" id="Seg_9812" s="T32">pp:Time</ta>
            <ta e="T36" id="Seg_9813" s="T35">np:Th</ta>
            <ta e="T38" id="Seg_9814" s="T37">0.1.h:E</ta>
            <ta e="T39" id="Seg_9815" s="T38">np.h:Th</ta>
            <ta e="T41" id="Seg_9816" s="T40">0.1.h:A</ta>
            <ta e="T42" id="Seg_9817" s="T41">pro:Cau</ta>
            <ta e="T43" id="Seg_9818" s="T42">np.h:Th</ta>
            <ta e="T44" id="Seg_9819" s="T43">0.1.h:Poss</ta>
            <ta e="T45" id="Seg_9820" s="T44">pro.h:R</ta>
            <ta e="T46" id="Seg_9821" s="T45">0.1.h:Poss np:Th</ta>
            <ta e="T47" id="Seg_9822" s="T46">0.1.h:A</ta>
            <ta e="T48" id="Seg_9823" s="T47">0.1.h:P</ta>
            <ta e="T49" id="Seg_9824" s="T48">adv:Time</ta>
            <ta e="T50" id="Seg_9825" s="T49">0.3.h:A</ta>
            <ta e="T53" id="Seg_9826" s="T52">np.h:A</ta>
            <ta e="T55" id="Seg_9827" s="T54">adv:Time</ta>
            <ta e="T56" id="Seg_9828" s="T55">np.h:A</ta>
            <ta e="T59" id="Seg_9829" s="T58">pro:Cau</ta>
            <ta e="T60" id="Seg_9830" s="T59">pro.h:Poss</ta>
            <ta e="T61" id="Seg_9831" s="T60">np.h:Th</ta>
            <ta e="T64" id="Seg_9832" s="T63">pro:P</ta>
            <ta e="T66" id="Seg_9833" s="T65">0.1.h:Th</ta>
            <ta e="T67" id="Seg_9834" s="T66">pro.h:E</ta>
            <ta e="T71" id="Seg_9835" s="T70">np.h:A</ta>
            <ta e="T74" id="Seg_9836" s="T73">np:Path</ta>
            <ta e="T77" id="Seg_9837" s="T76">np.h:A</ta>
            <ta e="T78" id="Seg_9838" s="T77">np:P</ta>
            <ta e="T81" id="Seg_9839" s="T80">np.h:Th</ta>
            <ta e="T82" id="Seg_9840" s="T81">np:G</ta>
            <ta e="T84" id="Seg_9841" s="T82">0.3.h:A</ta>
            <ta e="T88" id="Seg_9842" s="T87">np.h:Th</ta>
            <ta e="T89" id="Seg_9843" s="T88">np.h:A</ta>
            <ta e="T90" id="Seg_9844" s="T89">adv:G</ta>
            <ta e="T93" id="Seg_9845" s="T92">np:P</ta>
            <ta e="T97" id="Seg_9846" s="T96">0.2.h:A</ta>
            <ta e="T99" id="Seg_9847" s="T98">np:P</ta>
            <ta e="T101" id="Seg_9848" s="T100">np:P</ta>
            <ta e="T103" id="Seg_9849" s="T102">np:P</ta>
            <ta e="T104" id="Seg_9850" s="T103">pro:Time</ta>
            <ta e="T105" id="Seg_9851" s="T104">np.h:A</ta>
            <ta e="T108" id="Seg_9852" s="T107">np:L</ta>
            <ta e="T109" id="Seg_9853" s="T108">np:Th</ta>
            <ta e="T110" id="Seg_9854" s="T109">0.3.h:A</ta>
            <ta e="T111" id="Seg_9855" s="T110">pro:P</ta>
            <ta e="T112" id="Seg_9856" s="T111">np.h:A</ta>
            <ta e="T114" id="Seg_9857" s="T113">np.h:Poss</ta>
            <ta e="T115" id="Seg_9858" s="T114">np.h:E</ta>
            <ta e="T117" id="Seg_9859" s="T116">np:So</ta>
            <ta e="T118" id="Seg_9860" s="T117">pro.h:A</ta>
            <ta e="T123" id="Seg_9861" s="T122">np.h:A</ta>
            <ta e="T124" id="Seg_9862" s="T123">n:Time</ta>
            <ta e="T126" id="Seg_9863" s="T125">np:Ins</ta>
            <ta e="T128" id="Seg_9864" s="T127">np:Ins</ta>
            <ta e="T132" id="Seg_9865" s="T131">np:Th</ta>
            <ta e="T136" id="Seg_9866" s="T134">0.3.h:A</ta>
            <ta e="T137" id="Seg_9867" s="T136">pro:Th</ta>
            <ta e="T139" id="Seg_9868" s="T138">0.3.h:A</ta>
            <ta e="T141" id="Seg_9869" s="T140">0.3.h:Poss np.h:R</ta>
            <ta e="T143" id="Seg_9870" s="T141">0.3.h:A</ta>
            <ta e="T144" id="Seg_9871" s="T143">adv:Time</ta>
            <ta e="T145" id="Seg_9872" s="T144">0.2.h:A</ta>
            <ta e="T146" id="Seg_9873" s="T145">np.h:A</ta>
            <ta e="T147" id="Seg_9874" s="T146">np:Poss</ta>
            <ta e="T148" id="Seg_9875" s="T147">np:P</ta>
            <ta e="T150" id="Seg_9876" s="T148">pp:L</ta>
            <ta e="T153" id="Seg_9877" s="T152">np:P</ta>
            <ta e="T154" id="Seg_9878" s="T153">np.h:Poss</ta>
            <ta e="T155" id="Seg_9879" s="T154">np:A</ta>
            <ta e="T158" id="Seg_9880" s="T157">np.h:Poss</ta>
            <ta e="T159" id="Seg_9881" s="T158">np.h:A</ta>
            <ta e="T160" id="Seg_9882" s="T159">np:P</ta>
            <ta e="T163" id="Seg_9883" s="T162">np:Th</ta>
            <ta e="T164" id="Seg_9884" s="T163">np:G</ta>
            <ta e="T166" id="Seg_9885" s="T165">np.h:A</ta>
            <ta e="T167" id="Seg_9886" s="T166">np:R</ta>
            <ta e="T169" id="Seg_9887" s="T168">np:L</ta>
            <ta e="T173" id="Seg_9888" s="T172">np.h:A</ta>
            <ta e="T175" id="Seg_9889" s="T174">np.h:P</ta>
            <ta e="T177" id="Seg_9890" s="T176">np.h:A</ta>
            <ta e="T184" id="Seg_9891" s="T183">adv:Time</ta>
            <ta e="T186" id="Seg_9892" s="T185">np.h:Poss</ta>
            <ta e="T187" id="Seg_9893" s="T186">np.h:A</ta>
            <ta e="T191" id="Seg_9894" s="T190">0.1.h:A</ta>
            <ta e="T192" id="Seg_9895" s="T191">np.h:A</ta>
            <ta e="T194" id="Seg_9896" s="T192">adv:Time</ta>
            <ta e="T198" id="Seg_9897" s="T197">0.3.h:E</ta>
            <ta e="T200" id="Seg_9898" s="T198">pp:L</ta>
            <ta e="T204" id="Seg_9899" s="T203">np:Th</ta>
            <ta e="T206" id="Seg_9900" s="T205">pro:Th</ta>
            <ta e="T208" id="Seg_9901" s="T207">0.3.h:E</ta>
            <ta e="T209" id="Seg_9902" s="T208">np.h:Poss</ta>
            <ta e="T210" id="Seg_9903" s="T209">np.h:A</ta>
            <ta e="T211" id="Seg_9904" s="T210">0.1.h:A</ta>
            <ta e="T212" id="Seg_9905" s="T211">0.1.h:A</ta>
            <ta e="T213" id="Seg_9906" s="T212">0.3.h:A</ta>
            <ta e="T214" id="Seg_9907" s="T213">np.h:Poss</ta>
            <ta e="T215" id="Seg_9908" s="T214">np.h:A</ta>
            <ta e="T217" id="Seg_9909" s="T216">pro:Cau</ta>
            <ta e="T219" id="Seg_9910" s="T218">np.h:Poss</ta>
            <ta e="T221" id="Seg_9911" s="T220">0.3:Th</ta>
            <ta e="T222" id="Seg_9912" s="T221">np.h:Poss</ta>
            <ta e="T223" id="Seg_9913" s="T222">np.h:A</ta>
            <ta e="T224" id="Seg_9914" s="T223">0.1.h:A</ta>
            <ta e="T225" id="Seg_9915" s="T224">0.1.h:A</ta>
            <ta e="T227" id="Seg_9916" s="T225">0.1.h:A</ta>
            <ta e="T228" id="Seg_9917" s="T227">np:G</ta>
            <ta e="T232" id="Seg_9918" s="T231">np.h:A</ta>
            <ta e="T235" id="Seg_9919" s="T234">np.h:Poss</ta>
            <ta e="T236" id="Seg_9920" s="T235">np.h:P</ta>
            <ta e="T240" id="Seg_9921" s="T238">adv:Time</ta>
            <ta e="T244" id="Seg_9922" s="T243">np.h:P</ta>
            <ta e="T248" id="Seg_9923" s="T247">np.h:Poss</ta>
            <ta e="T249" id="Seg_9924" s="T248">np.h:P</ta>
            <ta e="T250" id="Seg_9925" s="T249">np:Poss</ta>
            <ta e="T251" id="Seg_9926" s="T250">n:Time</ta>
            <ta e="T254" id="Seg_9927" s="T253">np:Poss</ta>
            <ta e="T255" id="Seg_9928" s="T254">np:Th</ta>
            <ta e="T258" id="Seg_9929" s="T257">np.h:Th</ta>
            <ta e="T260" id="Seg_9930" s="T258">pp:G</ta>
            <ta e="T262" id="Seg_9931" s="T261">0.3.h:A</ta>
            <ta e="T267" id="Seg_9932" s="T265">0.1.h:P</ta>
            <ta e="T270" id="Seg_9933" s="T269">0.3.h:Poss np.h:P</ta>
            <ta e="T272" id="Seg_9934" s="T271">0.3.h:A</ta>
            <ta e="T274" id="Seg_9935" s="T273">np.h:P</ta>
            <ta e="T277" id="Seg_9936" s="T276">0.2.h:A</ta>
            <ta e="T278" id="Seg_9937" s="T277">pro.h:A</ta>
            <ta e="T279" id="Seg_9938" s="T278">np.h:P</ta>
            <ta e="T281" id="Seg_9939" s="T280">np.h:Poss</ta>
            <ta e="T282" id="Seg_9940" s="T281">np:Th</ta>
            <ta e="T285" id="Seg_9941" s="T284">np.h:A</ta>
            <ta e="T290" id="Seg_9942" s="T288">pp:Com</ta>
            <ta e="T291" id="Seg_9943" s="T290">0.3.h:A</ta>
            <ta e="T295" id="Seg_9944" s="T294">np.h:Poss</ta>
            <ta e="T296" id="Seg_9945" s="T295">np:P</ta>
            <ta e="T298" id="Seg_9946" s="T297">np:L</ta>
            <ta e="T300" id="Seg_9947" s="T298">0.3.h:A</ta>
            <ta e="T304" id="Seg_9948" s="T303">np:Poss</ta>
            <ta e="T305" id="Seg_9949" s="T304">np:So</ta>
            <ta e="T307" id="Seg_9950" s="T306">np:Th</ta>
            <ta e="T308" id="Seg_9951" s="T307">0.3.h:A</ta>
            <ta e="T309" id="Seg_9952" s="T308">np:Th</ta>
            <ta e="T310" id="Seg_9953" s="T309">0.3.h:Poss np:Poss</ta>
            <ta e="T311" id="Seg_9954" s="T310">np:G</ta>
            <ta e="T313" id="Seg_9955" s="T311">0.3.h:A</ta>
            <ta e="T314" id="Seg_9956" s="T313">adv:Time</ta>
            <ta e="T315" id="Seg_9957" s="T314">pro:Th</ta>
            <ta e="T319" id="Seg_9958" s="T318">np.h:Poss</ta>
            <ta e="T320" id="Seg_9959" s="T319">np.h:A</ta>
            <ta e="T321" id="Seg_9960" s="T320">0.3.h:Poss np.h:P</ta>
            <ta e="T325" id="Seg_9961" s="T324">0.1.h:A</ta>
            <ta e="T326" id="Seg_9962" s="T325">np.h:A</ta>
            <ta e="T330" id="Seg_9963" s="T329">np:P</ta>
            <ta e="T332" id="Seg_9964" s="T331">np:L</ta>
            <ta e="T333" id="Seg_9965" s="T332">np:Th</ta>
            <ta e="T335" id="Seg_9966" s="T334">pro:Th</ta>
            <ta e="T338" id="Seg_9967" s="T337">np:Th</ta>
            <ta e="T340" id="Seg_9968" s="T339">np.h:Poss</ta>
            <ta e="T341" id="Seg_9969" s="T340">np.h:P</ta>
            <ta e="T346" id="Seg_9970" s="T345">0.3.h:Poss np.h:A</ta>
            <ta e="T350" id="Seg_9971" s="T349">np:Poss</ta>
            <ta e="T351" id="Seg_9972" s="T350">n:Time</ta>
            <ta e="T352" id="Seg_9973" s="T351">0.3.h:P</ta>
            <ta e="T353" id="Seg_9974" s="T352">0.3.h:Poss np.h:Th</ta>
            <ta e="T357" id="Seg_9975" s="T355">0.3.h:P</ta>
            <ta e="T359" id="Seg_9976" s="T358">np.h:A</ta>
            <ta e="T366" id="Seg_9977" s="T364">pro:L</ta>
            <ta e="T369" id="Seg_9978" s="T368">np:Th</ta>
            <ta e="T371" id="Seg_9979" s="T370">0.3.h:Poss np.h:P</ta>
            <ta e="T372" id="Seg_9980" s="T371">0.3.h:A</ta>
            <ta e="T377" id="Seg_9981" s="T376">np.h:P</ta>
            <ta e="T378" id="Seg_9982" s="T377">np.h:Poss</ta>
            <ta e="T379" id="Seg_9983" s="T378">np.h:A</ta>
            <ta e="T383" id="Seg_9984" s="T382">np.h:Poss</ta>
            <ta e="T384" id="Seg_9985" s="T383">np:P</ta>
            <ta e="T387" id="Seg_9986" s="T386">np:L</ta>
            <ta e="T389" id="Seg_9987" s="T388">0.3.h:A</ta>
            <ta e="T390" id="Seg_9988" s="T389">np.h:A</ta>
            <ta e="T394" id="Seg_9989" s="T393">np:P</ta>
            <ta e="T396" id="Seg_9990" s="T395">np.h:Poss</ta>
            <ta e="T397" id="Seg_9991" s="T396">np.h:A</ta>
            <ta e="T398" id="Seg_9992" s="T397">0.3.h:Poss np:So</ta>
            <ta e="T400" id="Seg_9993" s="T399">np:Th</ta>
            <ta e="T403" id="Seg_9994" s="T402">0.3.h:Poss np:G</ta>
            <ta e="T406" id="Seg_9995" s="T404">0.3.h:A</ta>
            <ta e="T408" id="Seg_9996" s="T407">n:Time</ta>
            <ta e="T410" id="Seg_9997" s="T409">np.h:Poss</ta>
            <ta e="T411" id="Seg_9998" s="T410">np:P</ta>
            <ta e="T415" id="Seg_9999" s="T414">np:Th</ta>
            <ta e="T417" id="Seg_10000" s="T415">0.3.h:A</ta>
            <ta e="T418" id="Seg_10001" s="T417">0.3.h:Poss np.h:P</ta>
            <ta e="T421" id="Seg_10002" s="T418">pp:Time</ta>
            <ta e="T422" id="Seg_10003" s="T421">0.3.h:A</ta>
            <ta e="T424" id="Seg_10004" s="T423">0.1.h:Poss np:G</ta>
            <ta e="T425" id="Seg_10005" s="T424">0.1.h:A</ta>
            <ta e="T428" id="Seg_10006" s="T427">np.h:A</ta>
            <ta e="T429" id="Seg_10007" s="T428">np.h:Poss</ta>
            <ta e="T430" id="Seg_10008" s="T429">np.h:A</ta>
            <ta e="T435" id="Seg_10009" s="T433">0.1.h:A</ta>
            <ta e="T436" id="Seg_10010" s="T435">0.3:Th</ta>
            <ta e="T437" id="Seg_10011" s="T436">np.h:Poss</ta>
            <ta e="T438" id="Seg_10012" s="T437">np.h:A</ta>
            <ta e="T443" id="Seg_10013" s="T442">0.1.h:Poss np.h:E</ta>
            <ta e="T445" id="Seg_10014" s="T444">0.1.h:A</ta>
            <ta e="T447" id="Seg_10015" s="T446">np.h:A</ta>
            <ta e="T449" id="Seg_10016" s="T448">np.h:Poss</ta>
            <ta e="T450" id="Seg_10017" s="T449">np.h:A</ta>
            <ta e="T453" id="Seg_10018" s="T452">pro:Th</ta>
            <ta e="T456" id="Seg_10019" s="T455">0.3.h:Poss np:P</ta>
            <ta e="T458" id="Seg_10020" s="T456">0.3.h:A</ta>
            <ta e="T460" id="Seg_10021" s="T459">0.1.h:Poss np:Th</ta>
            <ta e="T461" id="Seg_10022" s="T460">0.1.h:E</ta>
            <ta e="T463" id="Seg_10023" s="T462">0.1.h:A</ta>
            <ta e="T464" id="Seg_10024" s="T463">0.3.h:A</ta>
            <ta e="T465" id="Seg_10025" s="T464">np.h:Poss</ta>
            <ta e="T466" id="Seg_10026" s="T465">np.h:A</ta>
            <ta e="T467" id="Seg_10027" s="T466">np.h:A</ta>
            <ta e="T469" id="Seg_10028" s="T468">np.h:Poss</ta>
            <ta e="T471" id="Seg_10029" s="T470">np:Th</ta>
            <ta e="T473" id="Seg_10030" s="T472">pro.h:P</ta>
            <ta e="T475" id="Seg_10031" s="T474">np:Th</ta>
            <ta e="T479" id="Seg_10032" s="T478">np:L</ta>
            <ta e="T480" id="Seg_10033" s="T479">0.3.h:Poss np:Poss</ta>
            <ta e="T481" id="Seg_10034" s="T480">np:G</ta>
            <ta e="T483" id="Seg_10035" s="T482">pro.h:P</ta>
            <ta e="T484" id="Seg_10036" s="T483">0.3.h:A</ta>
            <ta e="T486" id="Seg_10037" s="T484">0.3.h:P</ta>
            <ta e="T489" id="Seg_10038" s="T488">np.h:Poss</ta>
            <ta e="T490" id="Seg_10039" s="T489">np:Th</ta>
            <ta e="T491" id="Seg_10040" s="T490">pro.h:P</ta>
            <ta e="T494" id="Seg_10041" s="T492">pro:Time</ta>
            <ta e="T500" id="Seg_10042" s="T499">np.h:Poss</ta>
            <ta e="T501" id="Seg_10043" s="T500">np.h:Poss</ta>
            <ta e="T502" id="Seg_10044" s="T501">np:G</ta>
            <ta e="T504" id="Seg_10045" s="T503">np.h:P</ta>
            <ta e="T506" id="Seg_10046" s="T505">0.1.h:A</ta>
            <ta e="T507" id="Seg_10047" s="T506">pro.h:Th</ta>
            <ta e="T509" id="Seg_10048" s="T508">0.3.h:Poss np:G</ta>
            <ta e="T516" id="Seg_10049" s="T515">np.h:P</ta>
            <ta e="T518" id="Seg_10050" s="T517">0.1.h:A</ta>
            <ta e="T522" id="Seg_10051" s="T521">0.2.h:A</ta>
            <ta e="T526" id="Seg_10052" s="T525">np.h:A</ta>
            <ta e="T527" id="Seg_10053" s="T526">np.h:Poss</ta>
            <ta e="T528" id="Seg_10054" s="T527">np.h:E</ta>
            <ta e="T529" id="Seg_10055" s="T528">pro:Th</ta>
            <ta e="T533" id="Seg_10056" s="T532">np:G</ta>
            <ta e="T535" id="Seg_10057" s="T533">0.3.h:A</ta>
            <ta e="T536" id="Seg_10058" s="T535">np:Th</ta>
            <ta e="T540" id="Seg_10059" s="T538">0.3.h:A</ta>
            <ta e="T541" id="Seg_10060" s="T540">0.3.h:Poss np.h:Th</ta>
            <ta e="T542" id="Seg_10061" s="T541">np:Poss</ta>
            <ta e="T543" id="Seg_10062" s="T542">np:L</ta>
            <ta e="T545" id="Seg_10063" s="T543">0.3.h:A</ta>
            <ta e="T546" id="Seg_10064" s="T545">np.h:A</ta>
            <ta e="T550" id="Seg_10065" s="T549">pro:Th</ta>
            <ta e="T553" id="Seg_10066" s="T552">0.3.h:Poss np:Poss</ta>
            <ta e="T554" id="Seg_10067" s="T553">np:G</ta>
            <ta e="T556" id="Seg_10068" s="T555">np:P</ta>
            <ta e="T558" id="Seg_10069" s="T557">np.h:Poss</ta>
            <ta e="T559" id="Seg_10070" s="T558">np.h:E</ta>
            <ta e="T561" id="Seg_10071" s="T560">0.3.h:Poss np:Th</ta>
            <ta e="T563" id="Seg_10072" s="T561">pp:G</ta>
            <ta e="T564" id="Seg_10073" s="T563">np:P</ta>
            <ta e="T567" id="Seg_10074" s="T566">np.h:Poss</ta>
            <ta e="T568" id="Seg_10075" s="T567">np.h:A</ta>
            <ta e="T570" id="Seg_10076" s="T569">np:Th</ta>
            <ta e="T571" id="Seg_10077" s="T570">np:G</ta>
            <ta e="T574" id="Seg_10078" s="T573">np.h:Poss</ta>
            <ta e="T575" id="Seg_10079" s="T574">np.h:E</ta>
            <ta e="T582" id="Seg_10080" s="T581">np:P</ta>
            <ta e="T583" id="Seg_10081" s="T582">np.h:Poss</ta>
            <ta e="T584" id="Seg_10082" s="T583">np.h:Poss</ta>
            <ta e="T585" id="Seg_10083" s="T584">np:G</ta>
            <ta e="T589" id="Seg_10084" s="T588">0.1.h:Poss np.h:Ben</ta>
            <ta e="T590" id="Seg_10085" s="T589">np:Th</ta>
            <ta e="T591" id="Seg_10086" s="T590">0.1.h:A</ta>
            <ta e="T592" id="Seg_10087" s="T591">0.3.h:A</ta>
            <ta e="T594" id="Seg_10088" s="T593">0.3.h:Poss np:G</ta>
            <ta e="T598" id="Seg_10089" s="T597">0.3.h:Poss np.h:A</ta>
            <ta e="T601" id="Seg_10090" s="T600">0.3.h:Poss np:Ins</ta>
            <ta e="T605" id="Seg_10091" s="T604">np:G</ta>
            <ta e="T607" id="Seg_10092" s="T605">0.3.h:A</ta>
            <ta e="T608" id="Seg_10093" s="T607">np.h:Poss</ta>
            <ta e="T609" id="Seg_10094" s="T608">np.h:E</ta>
            <ta e="T611" id="Seg_10095" s="T610">0.3.h:Poss np:Th</ta>
            <ta e="T614" id="Seg_10096" s="T613">pro:Th</ta>
            <ta e="T617" id="Seg_10097" s="T616">0.3.h:Poss np:P</ta>
            <ta e="T618" id="Seg_10098" s="T617">np:G</ta>
            <ta e="T621" id="Seg_10099" s="T620">np:Poss</ta>
            <ta e="T622" id="Seg_10100" s="T621">np:Th</ta>
            <ta e="T623" id="Seg_10101" s="T622">np:P</ta>
            <ta e="T626" id="Seg_10102" s="T625">np.h:E</ta>
            <ta e="T631" id="Seg_10103" s="T630">0.3.h:E</ta>
            <ta e="T632" id="Seg_10104" s="T631">np.h:Poss</ta>
            <ta e="T633" id="Seg_10105" s="T632">np.h:A</ta>
            <ta e="T634" id="Seg_10106" s="T633">np:P</ta>
            <ta e="T635" id="Seg_10107" s="T634">np:Ins</ta>
            <ta e="T637" id="Seg_10108" s="T636">0.2.h:A</ta>
            <ta e="T638" id="Seg_10109" s="T637">np:Ins</ta>
            <ta e="T640" id="Seg_10110" s="T638">0.2.h:A</ta>
            <ta e="T641" id="Seg_10111" s="T640">pro.h:A</ta>
            <ta e="T644" id="Seg_10112" s="T643">0.3.h:A</ta>
            <ta e="T645" id="Seg_10113" s="T644">0.3.h:Poss np.h:R</ta>
            <ta e="T646" id="Seg_10114" s="T645">0.3.h:E</ta>
            <ta e="T647" id="Seg_10115" s="T646">np:Th</ta>
            <ta e="T652" id="Seg_10116" s="T651">adv:Time</ta>
            <ta e="T653" id="Seg_10117" s="T652">0.3.h:Poss np:Th</ta>
            <ta e="T655" id="Seg_10118" s="T654">np.h:A</ta>
            <ta e="T659" id="Seg_10119" s="T658">pro:Th</ta>
            <ta e="T663" id="Seg_10120" s="T662">np:Poss</ta>
            <ta e="T665" id="Seg_10121" s="T664">np:L</ta>
            <ta e="T666" id="Seg_10122" s="T665">0.3.h:Poss np:Th</ta>
            <ta e="T670" id="Seg_10123" s="T669">np.h:A</ta>
            <ta e="T672" id="Seg_10124" s="T671">np:G</ta>
            <ta e="T675" id="Seg_10125" s="T674">np.h:Poss</ta>
            <ta e="T676" id="Seg_10126" s="T675">np.h:A</ta>
            <ta e="T677" id="Seg_10127" s="T676">np:Poss</ta>
            <ta e="T678" id="Seg_10128" s="T677">np:L</ta>
            <ta e="T680" id="Seg_10129" s="T679">np:G</ta>
            <ta e="T682" id="Seg_10130" s="T681">np.h:Poss</ta>
            <ta e="T683" id="Seg_10131" s="T682">np.h:A</ta>
            <ta e="T684" id="Seg_10132" s="T683">0.3.h:Poss np.h:Poss</ta>
            <ta e="T687" id="Seg_10133" s="T686">np:G</ta>
            <ta e="T689" id="Seg_10134" s="T688">np.h:Poss</ta>
            <ta e="T690" id="Seg_10135" s="T689">np.h:E</ta>
            <ta e="T691" id="Seg_10136" s="T690">adv:L</ta>
            <ta e="T692" id="Seg_10137" s="T691">0.3.h:Poss np.h:Th</ta>
            <ta e="T694" id="Seg_10138" s="T693">adv:Time</ta>
            <ta e="T695" id="Seg_10139" s="T694">np.h:A</ta>
            <ta e="T696" id="Seg_10140" s="T695">np.h:Th</ta>
            <ta e="T700" id="Seg_10141" s="T699">0.2.h:A</ta>
            <ta e="T701" id="Seg_10142" s="T700">pro:P</ta>
            <ta e="T705" id="Seg_10143" s="T704">0.2.h:A</ta>
            <ta e="T707" id="Seg_10144" s="T706">np.h:Poss</ta>
            <ta e="T708" id="Seg_10145" s="T707">np:L</ta>
            <ta e="T711" id="Seg_10146" s="T710">np:Th</ta>
            <ta e="T712" id="Seg_10147" s="T711">0.1.h:E</ta>
            <ta e="T713" id="Seg_10148" s="T712">np.h:Poss</ta>
            <ta e="T714" id="Seg_10149" s="T713">np.h:A</ta>
            <ta e="T716" id="Seg_10150" s="T715">np.h:Poss</ta>
            <ta e="T717" id="Seg_10151" s="T716">np:G</ta>
            <ta e="T719" id="Seg_10152" s="T718">np:Th</ta>
            <ta e="T722" id="Seg_10153" s="T721">np.h:A</ta>
            <ta e="T723" id="Seg_10154" s="T722">np.h:Poss</ta>
            <ta e="T724" id="Seg_10155" s="T723">np.h:Th</ta>
            <ta e="T725" id="Seg_10156" s="T724">np:L</ta>
            <ta e="T731" id="Seg_10157" s="T730">0.3.h:A</ta>
            <ta e="T732" id="Seg_10158" s="T731">np:Ins</ta>
            <ta e="T733" id="Seg_10159" s="T732">pro.h:Poss</ta>
            <ta e="T736" id="Seg_10160" s="T735">np:Th</ta>
            <ta e="T739" id="Seg_10161" s="T737">0.1.h:Poss pp:Com</ta>
            <ta e="T742" id="Seg_10162" s="T741">0.2.h:Th</ta>
            <ta e="T744" id="Seg_10163" s="T743">np.h:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T3" id="Seg_10164" s="T2">np.h:S</ta>
            <ta e="T4" id="Seg_10165" s="T3">v:pred</ta>
            <ta e="T5" id="Seg_10166" s="T4">np.h:S</ta>
            <ta e="T8" id="Seg_10167" s="T5">ptcl:pred</ta>
            <ta e="T11" id="Seg_10168" s="T10">np.h:S</ta>
            <ta e="T12" id="Seg_10169" s="T11">v:pred</ta>
            <ta e="T15" id="Seg_10170" s="T14">pro.h:S</ta>
            <ta e="T17" id="Seg_10171" s="T16">ptcl:pred</ta>
            <ta e="T20" id="Seg_10172" s="T18">0.1.h:S adj:pred</ta>
            <ta e="T23" id="Seg_10173" s="T20">s:temp</ta>
            <ta e="T24" id="Seg_10174" s="T23">np.h:S</ta>
            <ta e="T25" id="Seg_10175" s="T24">v:pred</ta>
            <ta e="T26" id="Seg_10176" s="T25">pro:O</ta>
            <ta e="T27" id="Seg_10177" s="T26">0.2.h:S v:pred</ta>
            <ta e="T30" id="Seg_10178" s="T29">pro.h:S</ta>
            <ta e="T31" id="Seg_10179" s="T30">np.h:O</ta>
            <ta e="T32" id="Seg_10180" s="T31">v:pred</ta>
            <ta e="T36" id="Seg_10181" s="T35">np:O</ta>
            <ta e="T38" id="Seg_10182" s="T37">0.1.h:S v:pred</ta>
            <ta e="T39" id="Seg_10183" s="T38">np.h:S</ta>
            <ta e="T40" id="Seg_10184" s="T39">ptcl:pred</ta>
            <ta e="T41" id="Seg_10185" s="T40">0.1.h:S v:pred</ta>
            <ta e="T44" id="Seg_10186" s="T41">s:comp</ta>
            <ta e="T46" id="Seg_10187" s="T45">np:O</ta>
            <ta e="T47" id="Seg_10188" s="T46">0.1.h:S v:pred</ta>
            <ta e="T48" id="Seg_10189" s="T47">s:temp</ta>
            <ta e="T50" id="Seg_10190" s="T49">0.3.h:S v:pred</ta>
            <ta e="T53" id="Seg_10191" s="T52">np.h:S</ta>
            <ta e="T54" id="Seg_10192" s="T53">v:pred</ta>
            <ta e="T56" id="Seg_10193" s="T55">np.h:S</ta>
            <ta e="T57" id="Seg_10194" s="T56">v:pred</ta>
            <ta e="T60" id="Seg_10195" s="T59">pro.h:S</ta>
            <ta e="T62" id="Seg_10196" s="T61">ptcl:pred</ta>
            <ta e="T65" id="Seg_10197" s="T63">s:adv</ta>
            <ta e="T66" id="Seg_10198" s="T65">0.1.h:S v:pred</ta>
            <ta e="T67" id="Seg_10199" s="T66">pro.h:S</ta>
            <ta e="T70" id="Seg_10200" s="T68">v:pred</ta>
            <ta e="T71" id="Seg_10201" s="T70">np.h:S</ta>
            <ta e="T73" id="Seg_10202" s="T71">v:pred</ta>
            <ta e="T77" id="Seg_10203" s="T76">np.h:S</ta>
            <ta e="T78" id="Seg_10204" s="T77">np:O</ta>
            <ta e="T79" id="Seg_10205" s="T78">v:pred</ta>
            <ta e="T81" id="Seg_10206" s="T80">np.h:O</ta>
            <ta e="T84" id="Seg_10207" s="T82">0.3.h:S v:pred</ta>
            <ta e="T86" id="Seg_10208" s="T85">ptcl:pred</ta>
            <ta e="T88" id="Seg_10209" s="T87">np.h:S</ta>
            <ta e="T89" id="Seg_10210" s="T88">np.h:S</ta>
            <ta e="T92" id="Seg_10211" s="T89">s:temp</ta>
            <ta e="T95" id="Seg_10212" s="T92">s:temp</ta>
            <ta e="T96" id="Seg_10213" s="T95">v:pred</ta>
            <ta e="T97" id="Seg_10214" s="T96">0.2.h:S v:pred</ta>
            <ta e="T99" id="Seg_10215" s="T98">np:O</ta>
            <ta e="T101" id="Seg_10216" s="T100">np:O</ta>
            <ta e="T103" id="Seg_10217" s="T102">np:O</ta>
            <ta e="T105" id="Seg_10218" s="T104">np.h:S</ta>
            <ta e="T107" id="Seg_10219" s="T105">v:pred</ta>
            <ta e="T109" id="Seg_10220" s="T108">np:O</ta>
            <ta e="T110" id="Seg_10221" s="T109">0.3.h:S v:pred</ta>
            <ta e="T111" id="Seg_10222" s="T110">pro:O</ta>
            <ta e="T112" id="Seg_10223" s="T111">np.h:S</ta>
            <ta e="T113" id="Seg_10224" s="T112">v:pred</ta>
            <ta e="T115" id="Seg_10225" s="T114">np.h:S</ta>
            <ta e="T116" id="Seg_10226" s="T115">v:pred</ta>
            <ta e="T118" id="Seg_10227" s="T117">pro.h:S</ta>
            <ta e="T119" id="Seg_10228" s="T118">v:pred</ta>
            <ta e="T123" id="Seg_10229" s="T122">np.h:S</ta>
            <ta e="T130" id="Seg_10230" s="T128">v:pred</ta>
            <ta e="T132" id="Seg_10231" s="T131">np:O</ta>
            <ta e="T136" id="Seg_10232" s="T134">0.3.h:S ptcl:pred</ta>
            <ta e="T137" id="Seg_10233" s="T136">pro:S</ta>
            <ta e="T138" id="Seg_10234" s="T137">n:pred</ta>
            <ta e="T139" id="Seg_10235" s="T138">0.3.h:S v:pred</ta>
            <ta e="T143" id="Seg_10236" s="T141">0.3.h:S v:pred</ta>
            <ta e="T145" id="Seg_10237" s="T144">0.2.h:S v:pred</ta>
            <ta e="T146" id="Seg_10238" s="T145">np.h:S</ta>
            <ta e="T148" id="Seg_10239" s="T147">np:O</ta>
            <ta e="T152" id="Seg_10240" s="T150">v:pred</ta>
            <ta e="T153" id="Seg_10241" s="T152">pro:O</ta>
            <ta e="T155" id="Seg_10242" s="T154">np:S</ta>
            <ta e="T157" id="Seg_10243" s="T155">v:pred</ta>
            <ta e="T162" id="Seg_10244" s="T157">s:temp</ta>
            <ta e="T163" id="Seg_10245" s="T162">np:O</ta>
            <ta e="T165" id="Seg_10246" s="T163">s:rel</ta>
            <ta e="T166" id="Seg_10247" s="T165">np.h:S</ta>
            <ta e="T168" id="Seg_10248" s="T167">v:pred</ta>
            <ta e="T170" id="Seg_10249" s="T169">adj:pred</ta>
            <ta e="T171" id="Seg_10250" s="T170">cop</ta>
            <ta e="T173" id="Seg_10251" s="T172">np.h:S</ta>
            <ta e="T175" id="Seg_10252" s="T174">np.h:O</ta>
            <ta e="T176" id="Seg_10253" s="T175">v:pred</ta>
            <ta e="T177" id="Seg_10254" s="T176">np.h:S</ta>
            <ta e="T180" id="Seg_10255" s="T177">s:temp</ta>
            <ta e="T183" id="Seg_10256" s="T181">v:pred</ta>
            <ta e="T187" id="Seg_10257" s="T186">np.h:S</ta>
            <ta e="T188" id="Seg_10258" s="T187">v:pred</ta>
            <ta e="T191" id="Seg_10259" s="T190">0.1.h:S v:pred</ta>
            <ta e="T192" id="Seg_10260" s="T191">np.h:S</ta>
            <ta e="T196" id="Seg_10261" s="T194">v:pred</ta>
            <ta e="T198" id="Seg_10262" s="T197">0.3.h:S v:pred</ta>
            <ta e="T204" id="Seg_10263" s="T203">np:S</ta>
            <ta e="T205" id="Seg_10264" s="T204">v:pred</ta>
            <ta e="T207" id="Seg_10265" s="T206">pro:pred</ta>
            <ta e="T208" id="Seg_10266" s="T207">0.3.h:S v:pred</ta>
            <ta e="T210" id="Seg_10267" s="T209">np.h:S</ta>
            <ta e="T211" id="Seg_10268" s="T210">0.1.h:S v:pred</ta>
            <ta e="T212" id="Seg_10269" s="T211">0.1.h:S v:pred</ta>
            <ta e="T213" id="Seg_10270" s="T212">0.3.h:S v:pred</ta>
            <ta e="T215" id="Seg_10271" s="T214">np.h:S</ta>
            <ta e="T220" id="Seg_10272" s="T218">n:pred</ta>
            <ta e="T221" id="Seg_10273" s="T220">0.3:S cop</ta>
            <ta e="T223" id="Seg_10274" s="T222">np.h:S</ta>
            <ta e="T224" id="Seg_10275" s="T223">0.1.h:S v:pred</ta>
            <ta e="T225" id="Seg_10276" s="T224">0.1.h:S v:pred</ta>
            <ta e="T227" id="Seg_10277" s="T225">0.1.h:S v:pred</ta>
            <ta e="T229" id="Seg_10278" s="T227">s:temp</ta>
            <ta e="T232" id="Seg_10279" s="T231">np.h:S</ta>
            <ta e="T234" id="Seg_10280" s="T233">v:pred</ta>
            <ta e="T236" id="Seg_10281" s="T235">np.h:S</ta>
            <ta e="T238" id="Seg_10282" s="T236">v:pred</ta>
            <ta e="T241" id="Seg_10283" s="T238">s:comp</ta>
            <ta e="T242" id="Seg_10284" s="T241">ptcl:pred</ta>
            <ta e="T244" id="Seg_10285" s="T243">np.h:S</ta>
            <ta e="T247" id="Seg_10286" s="T245">v:pred</ta>
            <ta e="T249" id="Seg_10287" s="T248">np.h:S</ta>
            <ta e="T252" id="Seg_10288" s="T251">v:pred</ta>
            <ta e="T255" id="Seg_10289" s="T254">np:S</ta>
            <ta e="T256" id="Seg_10290" s="T255">v:pred</ta>
            <ta e="T257" id="Seg_10291" s="T256">v:pred</ta>
            <ta e="T258" id="Seg_10292" s="T257">np.h:S</ta>
            <ta e="T262" id="Seg_10293" s="T261">0.3.h:S v:pred</ta>
            <ta e="T267" id="Seg_10294" s="T265">0.1.h:S ptcl:pred</ta>
            <ta e="T269" id="Seg_10295" s="T267">s:temp</ta>
            <ta e="T271" id="Seg_10296" s="T269">s:comp</ta>
            <ta e="T272" id="Seg_10297" s="T271">0.3.h:S v:pred</ta>
            <ta e="T274" id="Seg_10298" s="T273">np.h:S</ta>
            <ta e="T276" id="Seg_10299" s="T275">v:pred</ta>
            <ta e="T277" id="Seg_10300" s="T276">0.2.h:S v:pred</ta>
            <ta e="T278" id="Seg_10301" s="T277">pro.h:S</ta>
            <ta e="T279" id="Seg_10302" s="T278">pro.h:O</ta>
            <ta e="T280" id="Seg_10303" s="T279">v:pred</ta>
            <ta e="T282" id="Seg_10304" s="T281">np:S</ta>
            <ta e="T283" id="Seg_10305" s="T282">v:pred</ta>
            <ta e="T285" id="Seg_10306" s="T284">np.h:S</ta>
            <ta e="T287" id="Seg_10307" s="T285">v:pred</ta>
            <ta e="T291" id="Seg_10308" s="T290">0.3.h:S v:pred</ta>
            <ta e="T294" id="Seg_10309" s="T291">s:temp</ta>
            <ta e="T296" id="Seg_10310" s="T295">np:O</ta>
            <ta e="T300" id="Seg_10311" s="T298">0.3.h:S v:pred</ta>
            <ta e="T303" id="Seg_10312" s="T300">s:temp</ta>
            <ta e="T307" id="Seg_10313" s="T306">np:O</ta>
            <ta e="T308" id="Seg_10314" s="T307">0.3.h:S v:pred</ta>
            <ta e="T309" id="Seg_10315" s="T308">np:O</ta>
            <ta e="T313" id="Seg_10316" s="T311">0.3.h:S v:pred</ta>
            <ta e="T318" id="Seg_10317" s="T314">s:adv</ta>
            <ta e="T320" id="Seg_10318" s="T319">np.h:S</ta>
            <ta e="T321" id="Seg_10319" s="T320">np.h:O</ta>
            <ta e="T322" id="Seg_10320" s="T321">v:pred</ta>
            <ta e="T325" id="Seg_10321" s="T324">0.1.h:S v:pred</ta>
            <ta e="T326" id="Seg_10322" s="T325">np.h:S</ta>
            <ta e="T329" id="Seg_10323" s="T326">s:adv</ta>
            <ta e="T330" id="Seg_10324" s="T329">np:O</ta>
            <ta e="T331" id="Seg_10325" s="T330">v:pred</ta>
            <ta e="T333" id="Seg_10326" s="T332">np:S</ta>
            <ta e="T334" id="Seg_10327" s="T333">adj:pred</ta>
            <ta e="T337" id="Seg_10328" s="T334">s:temp</ta>
            <ta e="T338" id="Seg_10329" s="T337">np:S</ta>
            <ta e="T339" id="Seg_10330" s="T338">v:pred</ta>
            <ta e="T341" id="Seg_10331" s="T340">np.h:S</ta>
            <ta e="T343" id="Seg_10332" s="T341">s:temp</ta>
            <ta e="T345" id="Seg_10333" s="T343">v:pred</ta>
            <ta e="T346" id="Seg_10334" s="T345">np.h:S</ta>
            <ta e="T348" id="Seg_10335" s="T347">s:purp</ta>
            <ta e="T349" id="Seg_10336" s="T348">v:pred</ta>
            <ta e="T352" id="Seg_10337" s="T351">0.3.h:S v:pred</ta>
            <ta e="T353" id="Seg_10338" s="T352">np.h:S</ta>
            <ta e="T354" id="Seg_10339" s="T353">v:pred</ta>
            <ta e="T356" id="Seg_10340" s="T354">s:adv</ta>
            <ta e="T357" id="Seg_10341" s="T356">0.3.h:S v:pred</ta>
            <ta e="T359" id="Seg_10342" s="T358">np.h:S</ta>
            <ta e="T361" id="Seg_10343" s="T359">s:temp</ta>
            <ta e="T363" id="Seg_10344" s="T361">v:pred</ta>
            <ta e="T369" id="Seg_10345" s="T368">np:S</ta>
            <ta e="T370" id="Seg_10346" s="T369">v:pred</ta>
            <ta e="T371" id="Seg_10347" s="T370">np.h:O</ta>
            <ta e="T372" id="Seg_10348" s="T371">0.3.h:S v:pred</ta>
            <ta e="T374" id="Seg_10349" s="T373">v:pred</ta>
            <ta e="T377" id="Seg_10350" s="T376">np.h:S</ta>
            <ta e="T379" id="Seg_10351" s="T378">np.h:S</ta>
            <ta e="T380" id="Seg_10352" s="T379">v:pred</ta>
            <ta e="T384" id="Seg_10353" s="T383">np:O</ta>
            <ta e="T389" id="Seg_10354" s="T388">0.3.h:S v:pred</ta>
            <ta e="T390" id="Seg_10355" s="T389">np.h:S</ta>
            <ta e="T393" id="Seg_10356" s="T390">s:temp</ta>
            <ta e="T394" id="Seg_10357" s="T393">np:O</ta>
            <ta e="T395" id="Seg_10358" s="T394">v:pred</ta>
            <ta e="T397" id="Seg_10359" s="T396">np.h:S</ta>
            <ta e="T400" id="Seg_10360" s="T399">np:O</ta>
            <ta e="T401" id="Seg_10361" s="T400">v:pred</ta>
            <ta e="T406" id="Seg_10362" s="T404">0.3.h:S v:pred</ta>
            <ta e="T413" id="Seg_10363" s="T408">s:adv</ta>
            <ta e="T415" id="Seg_10364" s="T414">np:O</ta>
            <ta e="T417" id="Seg_10365" s="T415">0.3.h:S v:pred</ta>
            <ta e="T418" id="Seg_10366" s="T417">np.h:O</ta>
            <ta e="T422" id="Seg_10367" s="T421">0.3.h:S v:pred</ta>
            <ta e="T425" id="Seg_10368" s="T424">0.1.h:S v:pred</ta>
            <ta e="T426" id="Seg_10369" s="T425">v:pred</ta>
            <ta e="T428" id="Seg_10370" s="T427">np.h:S</ta>
            <ta e="T430" id="Seg_10371" s="T429">np.h:S</ta>
            <ta e="T431" id="Seg_10372" s="T430">v:pred</ta>
            <ta e="T435" id="Seg_10373" s="T433">0.1.h:S v:pred</ta>
            <ta e="T436" id="Seg_10374" s="T435">adj:pred</ta>
            <ta e="T438" id="Seg_10375" s="T437">np.h:S</ta>
            <ta e="T441" id="Seg_10376" s="T438">s:temp</ta>
            <ta e="T442" id="Seg_10377" s="T441">v:pred</ta>
            <ta e="T443" id="Seg_10378" s="T442">np.h:S</ta>
            <ta e="T444" id="Seg_10379" s="T443">v:pred</ta>
            <ta e="T445" id="Seg_10380" s="T444">0.1.h:S v:pred</ta>
            <ta e="T447" id="Seg_10381" s="T446">np.h:S</ta>
            <ta e="T448" id="Seg_10382" s="T447">v:pred</ta>
            <ta e="T450" id="Seg_10383" s="T449">np.h:S</ta>
            <ta e="T452" id="Seg_10384" s="T450">v:pred</ta>
            <ta e="T455" id="Seg_10385" s="T452">s:temp</ta>
            <ta e="T456" id="Seg_10386" s="T455">np:O</ta>
            <ta e="T458" id="Seg_10387" s="T456">0.3.h:S v:pred</ta>
            <ta e="T460" id="Seg_10388" s="T459">np:O</ta>
            <ta e="T461" id="Seg_10389" s="T460">0.1.h:S v:pred</ta>
            <ta e="T463" id="Seg_10390" s="T462">0.1.h:S v:pred</ta>
            <ta e="T464" id="Seg_10391" s="T463">0.3.h:S v:pred</ta>
            <ta e="T466" id="Seg_10392" s="T465">np.h:S</ta>
            <ta e="T468" id="Seg_10393" s="T467">v:pred</ta>
            <ta e="T471" id="Seg_10394" s="T470">np:S</ta>
            <ta e="T472" id="Seg_10395" s="T471">v:pred</ta>
            <ta e="T473" id="Seg_10396" s="T472">pro.h:S</ta>
            <ta e="T477" id="Seg_10397" s="T473">s:temp</ta>
            <ta e="T482" id="Seg_10398" s="T481">v:pred</ta>
            <ta e="T484" id="Seg_10399" s="T482">s:temp</ta>
            <ta e="T486" id="Seg_10400" s="T484">0.3.h:S v:pred</ta>
            <ta e="T487" id="Seg_10401" s="T486">v:pred</ta>
            <ta e="T490" id="Seg_10402" s="T489">np:S</ta>
            <ta e="T491" id="Seg_10403" s="T490">pro.h:S</ta>
            <ta e="T499" id="Seg_10404" s="T492">s:adv</ta>
            <ta e="T503" id="Seg_10405" s="T502">v:pred</ta>
            <ta e="T504" id="Seg_10406" s="T503">np.h:O</ta>
            <ta e="T506" id="Seg_10407" s="T505">0.1.h:S v:pred</ta>
            <ta e="T507" id="Seg_10408" s="T506">pro.h:S</ta>
            <ta e="T512" id="Seg_10409" s="T508">s:rel</ta>
            <ta e="T514" id="Seg_10410" s="T513">n:pred</ta>
            <ta e="T515" id="Seg_10411" s="T514">cop</ta>
            <ta e="T516" id="Seg_10412" s="T515">np.h:O</ta>
            <ta e="T518" id="Seg_10413" s="T517">0.1.h:S v:pred</ta>
            <ta e="T523" id="Seg_10414" s="T521">0.2.h:S ptcl:pred</ta>
            <ta e="T524" id="Seg_10415" s="T523">v:pred</ta>
            <ta e="T526" id="Seg_10416" s="T525">np.h:S</ta>
            <ta e="T528" id="Seg_10417" s="T527">np.h:S</ta>
            <ta e="T529" id="Seg_10418" s="T528">pro:O</ta>
            <ta e="T530" id="Seg_10419" s="T529">v:pred</ta>
            <ta e="T535" id="Seg_10420" s="T533">0.3.h:S v:pred</ta>
            <ta e="T536" id="Seg_10421" s="T535">np:O</ta>
            <ta e="T540" id="Seg_10422" s="T538">0.3.h:S v:pred</ta>
            <ta e="T541" id="Seg_10423" s="T540">np.h:O</ta>
            <ta e="T545" id="Seg_10424" s="T543">0.3.h:S v:pred</ta>
            <ta e="T546" id="Seg_10425" s="T545">np.h:S</ta>
            <ta e="T549" id="Seg_10426" s="T547">v:pred</ta>
            <ta e="T552" id="Seg_10427" s="T549">s:temp</ta>
            <ta e="T556" id="Seg_10428" s="T555">np:S</ta>
            <ta e="T557" id="Seg_10429" s="T556">v:pred</ta>
            <ta e="T559" id="Seg_10430" s="T558">np.h:S</ta>
            <ta e="T560" id="Seg_10431" s="T559">v:pred</ta>
            <ta e="T566" id="Seg_10432" s="T560">s:adv</ta>
            <ta e="T568" id="Seg_10433" s="T567">np.h:S</ta>
            <ta e="T570" id="Seg_10434" s="T569">np:O</ta>
            <ta e="T573" id="Seg_10435" s="T571">v:pred</ta>
            <ta e="T578" id="Seg_10436" s="T573">s:temp</ta>
            <ta e="T582" id="Seg_10437" s="T581">np:S</ta>
            <ta e="T586" id="Seg_10438" s="T585">v:pred</ta>
            <ta e="T590" id="Seg_10439" s="T589">np:O</ta>
            <ta e="T591" id="Seg_10440" s="T590">0.1.h:S v:pred</ta>
            <ta e="T592" id="Seg_10441" s="T591">0.3.h:S v:pred</ta>
            <ta e="T597" id="Seg_10442" s="T593">s:adv</ta>
            <ta e="T598" id="Seg_10443" s="T597">np.h:S</ta>
            <ta e="T600" id="Seg_10444" s="T598">v:pred</ta>
            <ta e="T604" id="Seg_10445" s="T600">s:temp</ta>
            <ta e="T607" id="Seg_10446" s="T605">0.3.h:S v:pred</ta>
            <ta e="T610" id="Seg_10447" s="T607">s:adv</ta>
            <ta e="T611" id="Seg_10448" s="T610">np:S</ta>
            <ta e="T613" id="Seg_10449" s="T612">v:pred</ta>
            <ta e="T616" id="Seg_10450" s="T613">s:temp</ta>
            <ta e="T617" id="Seg_10451" s="T616">np:S</ta>
            <ta e="T620" id="Seg_10452" s="T618">v:pred</ta>
            <ta e="T622" id="Seg_10453" s="T621">np:S</ta>
            <ta e="T623" id="Seg_10454" s="T622">np:O</ta>
            <ta e="T624" id="Seg_10455" s="T623">v:pred</ta>
            <ta e="T626" id="Seg_10456" s="T625">np.h:S</ta>
            <ta e="T628" id="Seg_10457" s="T626">s:adv</ta>
            <ta e="T630" id="Seg_10458" s="T628">v:pred</ta>
            <ta e="T631" id="Seg_10459" s="T630">0.3.h:S v:pred</ta>
            <ta e="T633" id="Seg_10460" s="T632">np.h:S</ta>
            <ta e="T634" id="Seg_10461" s="T633">np:O</ta>
            <ta e="T637" id="Seg_10462" s="T636">0.2.h:S v:pred</ta>
            <ta e="T640" id="Seg_10463" s="T638">0.2.h:S v:pred</ta>
            <ta e="T641" id="Seg_10464" s="T640">pro.h:S</ta>
            <ta e="T643" id="Seg_10465" s="T641">v:pred</ta>
            <ta e="T644" id="Seg_10466" s="T643">0.3.h:S v:pred</ta>
            <ta e="T646" id="Seg_10467" s="T645">0.3.h:S v:pred</ta>
            <ta e="T647" id="Seg_10468" s="T646">np:S</ta>
            <ta e="T649" id="Seg_10469" s="T647">s:adv</ta>
            <ta e="T651" id="Seg_10470" s="T649">v:pred</ta>
            <ta e="T653" id="Seg_10471" s="T652">np:S</ta>
            <ta e="T654" id="Seg_10472" s="T653">v:pred</ta>
            <ta e="T655" id="Seg_10473" s="T654">np.h:S</ta>
            <ta e="T657" id="Seg_10474" s="T655">v:pred</ta>
            <ta e="T662" id="Seg_10475" s="T658">s:temp</ta>
            <ta e="T666" id="Seg_10476" s="T665">np:S</ta>
            <ta e="T667" id="Seg_10477" s="T666">v:pred</ta>
            <ta e="T669" id="Seg_10478" s="T667">s:temp</ta>
            <ta e="T670" id="Seg_10479" s="T669">np.h:S</ta>
            <ta e="T674" id="Seg_10480" s="T672">v:pred</ta>
            <ta e="T676" id="Seg_10481" s="T675">np.h:S</ta>
            <ta e="T681" id="Seg_10482" s="T680">v:pred</ta>
            <ta e="T683" id="Seg_10483" s="T682">np.h:S</ta>
            <ta e="T688" id="Seg_10484" s="T687">v:pred</ta>
            <ta e="T690" id="Seg_10485" s="T689">np.h:S</ta>
            <ta e="T692" id="Seg_10486" s="T691">np.h:O</ta>
            <ta e="T693" id="Seg_10487" s="T692">v:pred</ta>
            <ta e="T695" id="Seg_10488" s="T694">np.h:S</ta>
            <ta e="T696" id="Seg_10489" s="T695">np.h:O</ta>
            <ta e="T697" id="Seg_10490" s="T696">v:pred</ta>
            <ta e="T700" id="Seg_10491" s="T699">0.2.h:S v:pred</ta>
            <ta e="T705" id="Seg_10492" s="T700">s:comp</ta>
            <ta e="T711" id="Seg_10493" s="T710">np:O</ta>
            <ta e="T712" id="Seg_10494" s="T711">0.1.h:S v:pred</ta>
            <ta e="T714" id="Seg_10495" s="T713">np.h:S</ta>
            <ta e="T715" id="Seg_10496" s="T714">v:pred</ta>
            <ta e="T719" id="Seg_10497" s="T718">np:O</ta>
            <ta e="T721" id="Seg_10498" s="T719">0.3.h:S v:pred</ta>
            <ta e="T722" id="Seg_10499" s="T721">np.h:S</ta>
            <ta e="T724" id="Seg_10500" s="T723">np.h:O</ta>
            <ta e="T727" id="Seg_10501" s="T725">v:pred</ta>
            <ta e="T730" id="Seg_10502" s="T727">s:temp</ta>
            <ta e="T731" id="Seg_10503" s="T730">0.3.h:S v:pred</ta>
            <ta e="T736" id="Seg_10504" s="T735">np:S</ta>
            <ta e="T737" id="Seg_10505" s="T736">v:pred</ta>
            <ta e="T741" id="Seg_10506" s="T739">s:adv</ta>
            <ta e="T742" id="Seg_10507" s="T741">0.2.h:S v:pred</ta>
            <ta e="T744" id="Seg_10508" s="T743">np.h:S</ta>
            <ta e="T747" id="Seg_10509" s="T746">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST">
            <ta e="T3" id="Seg_10510" s="T2">new</ta>
            <ta e="T5" id="Seg_10511" s="T4">0.giv-active accs-gen</ta>
            <ta e="T11" id="Seg_10512" s="T10">giv-active</ta>
            <ta e="T12" id="Seg_10513" s="T11">quot-th</ta>
            <ta e="T15" id="Seg_10514" s="T14">accs-inf-Q</ta>
            <ta e="T16" id="Seg_10515" s="T15">accs-gen-Q</ta>
            <ta e="T18" id="Seg_10516" s="T17">accs-gen-Q</ta>
            <ta e="T19" id="Seg_10517" s="T18">0.giv-active-Q</ta>
            <ta e="T23" id="Seg_10518" s="T22">0.giv-active</ta>
            <ta e="T24" id="Seg_10519" s="T23">giv-inactive</ta>
            <ta e="T27" id="Seg_10520" s="T26">0.giv-active-Q</ta>
            <ta e="T29" id="Seg_10521" s="T28">accs-sit-Q</ta>
            <ta e="T30" id="Seg_10522" s="T29">giv-inactive-Q</ta>
            <ta e="T31" id="Seg_10523" s="T30">accs-gen-Q</ta>
            <ta e="T36" id="Seg_10524" s="T35">accs-gen-Q</ta>
            <ta e="T38" id="Seg_10525" s="T37">0.giv-active-Q</ta>
            <ta e="T39" id="Seg_10526" s="T38">accs-gen-Q</ta>
            <ta e="T41" id="Seg_10527" s="T40">0.giv-active-Q</ta>
            <ta e="T43" id="Seg_10528" s="T42">accs-gen-Q</ta>
            <ta e="T44" id="Seg_10529" s="T43">0.giv-active-Q</ta>
            <ta e="T46" id="Seg_10530" s="T45">accs-inf-Q</ta>
            <ta e="T47" id="Seg_10531" s="T46">0.giv-active-Q</ta>
            <ta e="T48" id="Seg_10532" s="T47">0.giv-active-Q</ta>
            <ta e="T49" id="Seg_10533" s="T48">accs-inf</ta>
            <ta e="T50" id="Seg_10534" s="T49">0.giv-active</ta>
            <ta e="T53" id="Seg_10535" s="T52">accs-gen</ta>
            <ta e="T56" id="Seg_10536" s="T55">giv-active</ta>
            <ta e="T57" id="Seg_10537" s="T56">quot-sp</ta>
            <ta e="T60" id="Seg_10538" s="T59">giv-active-Q</ta>
            <ta e="T61" id="Seg_10539" s="T60">accs-gen-Q</ta>
            <ta e="T66" id="Seg_10540" s="T65">0.giv-active-Q</ta>
            <ta e="T67" id="Seg_10541" s="T66">accs-sit-Q</ta>
            <ta e="T71" id="Seg_10542" s="T70">giv-active</ta>
            <ta e="T74" id="Seg_10543" s="T73">accs-inf</ta>
            <ta e="T77" id="Seg_10544" s="T76">new</ta>
            <ta e="T81" id="Seg_10545" s="T80">giv-active</ta>
            <ta e="T82" id="Seg_10546" s="T81">accs-inf</ta>
            <ta e="T84" id="Seg_10547" s="T82">0.giv-active</ta>
            <ta e="T85" id="Seg_10548" s="T84">giv-active-Q</ta>
            <ta e="T88" id="Seg_10549" s="T87">giv-active-Q</ta>
            <ta e="T89" id="Seg_10550" s="T88">giv-active</ta>
            <ta e="T93" id="Seg_10551" s="T92">accs-inf</ta>
            <ta e="T96" id="Seg_10552" s="T95">quot-sp</ta>
            <ta e="T97" id="Seg_10553" s="T96">0.giv-inactive-Q</ta>
            <ta e="T99" id="Seg_10554" s="T98">new-Q</ta>
            <ta e="T101" id="Seg_10555" s="T100">new-Q</ta>
            <ta e="T103" id="Seg_10556" s="T102">new-Q</ta>
            <ta e="T105" id="Seg_10557" s="T104">giv-inactive-Q</ta>
            <ta e="T108" id="Seg_10558" s="T107">giv-inactive-Q</ta>
            <ta e="T109" id="Seg_10559" s="T108">new-Q</ta>
            <ta e="T111" id="Seg_10560" s="T110">giv-active-Q</ta>
            <ta e="T112" id="Seg_10561" s="T111">giv-inactive-Q</ta>
            <ta e="T114" id="Seg_10562" s="T113">giv-active</ta>
            <ta e="T115" id="Seg_10563" s="T114">giv-active</ta>
            <ta e="T116" id="Seg_10564" s="T115">quot-sp</ta>
            <ta e="T117" id="Seg_10565" s="T116">giv-active-Q</ta>
            <ta e="T118" id="Seg_10566" s="T117">giv-active-Q</ta>
            <ta e="T123" id="Seg_10567" s="T122">giv-inactive</ta>
            <ta e="T124" id="Seg_10568" s="T123">accs-sit</ta>
            <ta e="T126" id="Seg_10569" s="T125">accs-sit</ta>
            <ta e="T128" id="Seg_10570" s="T127">accs-sit</ta>
            <ta e="T132" id="Seg_10571" s="T131">new</ta>
            <ta e="T135" id="Seg_10572" s="T134">0.giv-active</ta>
            <ta e="T137" id="Seg_10573" s="T136">giv-active-Q</ta>
            <ta e="T138" id="Seg_10574" s="T137">giv-active-Q</ta>
            <ta e="T139" id="Seg_10575" s="T138">0.quot-sp</ta>
            <ta e="T141" id="Seg_10576" s="T140">new</ta>
            <ta e="T143" id="Seg_10577" s="T141">0.giv-active</ta>
            <ta e="T145" id="Seg_10578" s="T144">0.giv-active-Q</ta>
            <ta e="T146" id="Seg_10579" s="T145">giv-active</ta>
            <ta e="T147" id="Seg_10580" s="T146">giv-inactive</ta>
            <ta e="T148" id="Seg_10581" s="T147">accs-inf</ta>
            <ta e="T149" id="Seg_10582" s="T148">accs-gen</ta>
            <ta e="T153" id="Seg_10583" s="T152">giv-active</ta>
            <ta e="T154" id="Seg_10584" s="T153">giv-inactive</ta>
            <ta e="T155" id="Seg_10585" s="T154">new</ta>
            <ta e="T158" id="Seg_10586" s="T157">giv-active</ta>
            <ta e="T159" id="Seg_10587" s="T158">giv-inactive</ta>
            <ta e="T160" id="Seg_10588" s="T159">giv-active</ta>
            <ta e="T163" id="Seg_10589" s="T162">accs-inf</ta>
            <ta e="T164" id="Seg_10590" s="T163">accs-sit</ta>
            <ta e="T165" id="Seg_10591" s="T164">giv-active</ta>
            <ta e="T166" id="Seg_10592" s="T165">giv-inactive</ta>
            <ta e="T167" id="Seg_10593" s="T166">new</ta>
            <ta e="T169" id="Seg_10594" s="T168">accs-gen</ta>
            <ta e="T173" id="Seg_10595" s="T172">accs-aggr</ta>
            <ta e="T175" id="Seg_10596" s="T174">new</ta>
            <ta e="T177" id="Seg_10597" s="T176">giv-active</ta>
            <ta e="T187" id="Seg_10598" s="T186">giv-active</ta>
            <ta e="T188" id="Seg_10599" s="T187">quot-sp</ta>
            <ta e="T189" id="Seg_10600" s="T188">giv-inactive-Q</ta>
            <ta e="T191" id="Seg_10601" s="T190">0.giv-active-Q</ta>
            <ta e="T192" id="Seg_10602" s="T191">giv-active</ta>
            <ta e="T198" id="Seg_10603" s="T197">0.giv-active</ta>
            <ta e="T199" id="Seg_10604" s="T198">accs-sit</ta>
            <ta e="T204" id="Seg_10605" s="T203">new</ta>
            <ta e="T206" id="Seg_10606" s="T205">giv-active-Q</ta>
            <ta e="T208" id="Seg_10607" s="T207">0.quot-th</ta>
            <ta e="T210" id="Seg_10608" s="T209">giv-active</ta>
            <ta e="T211" id="Seg_10609" s="T210">0.giv-inactive-Q</ta>
            <ta e="T212" id="Seg_10610" s="T211">0.giv-active-Q</ta>
            <ta e="T213" id="Seg_10611" s="T212">0.quot-sp</ta>
            <ta e="T215" id="Seg_10612" s="T214">giv-active</ta>
            <ta e="T219" id="Seg_10613" s="T218">accs-gen-Q</ta>
            <ta e="T220" id="Seg_10614" s="T219">giv-inactive-Q</ta>
            <ta e="T223" id="Seg_10615" s="T222">giv-inactive</ta>
            <ta e="T224" id="Seg_10616" s="T223">0.giv-inactive-Q</ta>
            <ta e="T225" id="Seg_10617" s="T224">0.giv-active-Q</ta>
            <ta e="T227" id="Seg_10618" s="T225">0.giv-active-Q</ta>
            <ta e="T228" id="Seg_10619" s="T227">accs-inf</ta>
            <ta e="T232" id="Seg_10620" s="T231">giv-active</ta>
            <ta e="T236" id="Seg_10621" s="T235">giv-inactive</ta>
            <ta e="T241" id="Seg_10622" s="T240">0.giv-inactive</ta>
            <ta e="T244" id="Seg_10623" s="T243">giv-active</ta>
            <ta e="T249" id="Seg_10624" s="T248">giv-active</ta>
            <ta e="T251" id="Seg_10625" s="T250">accs-sit</ta>
            <ta e="T253" id="Seg_10626" s="T252">accs-gen</ta>
            <ta e="T255" id="Seg_10627" s="T254">accs-inf</ta>
            <ta e="T258" id="Seg_10628" s="T257">accs-gen</ta>
            <ta e="T259" id="Seg_10629" s="T258">giv-inactive</ta>
            <ta e="T262" id="Seg_10630" s="T261">0.giv-active</ta>
            <ta e="T266" id="Seg_10631" s="T265">0.giv-inactive-Q</ta>
            <ta e="T268" id="Seg_10632" s="T267">0.quot-th</ta>
            <ta e="T270" id="Seg_10633" s="T269">giv-active</ta>
            <ta e="T272" id="Seg_10634" s="T271">0.giv-active</ta>
            <ta e="T274" id="Seg_10635" s="T273">giv-active</ta>
            <ta e="T277" id="Seg_10636" s="T276">0.giv-active-Q</ta>
            <ta e="T278" id="Seg_10637" s="T277">giv-inactive-Q</ta>
            <ta e="T279" id="Seg_10638" s="T278">giv-active-Q</ta>
            <ta e="T281" id="Seg_10639" s="T280">giv-active</ta>
            <ta e="T282" id="Seg_10640" s="T281">accs-inf</ta>
            <ta e="T283" id="Seg_10641" s="T282">quot-sp</ta>
            <ta e="T285" id="Seg_10642" s="T284">giv-active</ta>
            <ta e="T289" id="Seg_10643" s="T288">giv-active</ta>
            <ta e="T291" id="Seg_10644" s="T290">0.giv-active</ta>
            <ta e="T295" id="Seg_10645" s="T294">giv-active</ta>
            <ta e="T296" id="Seg_10646" s="T295">accs-inf</ta>
            <ta e="T298" id="Seg_10647" s="T297">new</ta>
            <ta e="T300" id="Seg_10648" s="T298">0.giv-active</ta>
            <ta e="T304" id="Seg_10649" s="T303">accs-inf</ta>
            <ta e="T305" id="Seg_10650" s="T304">accs-inf</ta>
            <ta e="T307" id="Seg_10651" s="T306">new</ta>
            <ta e="T308" id="Seg_10652" s="T307">0.giv-active</ta>
            <ta e="T309" id="Seg_10653" s="T308">giv-active</ta>
            <ta e="T310" id="Seg_10654" s="T309">new</ta>
            <ta e="T311" id="Seg_10655" s="T310">accs-inf</ta>
            <ta e="T313" id="Seg_10656" s="T311">0.giv-active</ta>
            <ta e="T320" id="Seg_10657" s="T319">giv-active</ta>
            <ta e="T321" id="Seg_10658" s="T320">giv-inactive</ta>
            <ta e="T325" id="Seg_10659" s="T324">0.giv-active-Q</ta>
            <ta e="T326" id="Seg_10660" s="T325">giv-active</ta>
            <ta e="T330" id="Seg_10661" s="T329">new</ta>
            <ta e="T332" id="Seg_10662" s="T331">accs-gen</ta>
            <ta e="T333" id="Seg_10663" s="T332">accs-gen</ta>
            <ta e="T338" id="Seg_10664" s="T337">accs-gen</ta>
            <ta e="T341" id="Seg_10665" s="T340">giv-inactive</ta>
            <ta e="T346" id="Seg_10666" s="T345">giv-inactive</ta>
            <ta e="T351" id="Seg_10667" s="T350">accs-sit</ta>
            <ta e="T352" id="Seg_10668" s="T351">0.giv-active</ta>
            <ta e="T353" id="Seg_10669" s="T352">giv-active</ta>
            <ta e="T359" id="Seg_10670" s="T358">giv-active</ta>
            <ta e="T369" id="Seg_10671" s="T368">new</ta>
            <ta e="T371" id="Seg_10672" s="T370">giv-inactive</ta>
            <ta e="T372" id="Seg_10673" s="T371">0.giv-active</ta>
            <ta e="T377" id="Seg_10674" s="T376">giv-active</ta>
            <ta e="T379" id="Seg_10675" s="T378">giv-active</ta>
            <ta e="T383" id="Seg_10676" s="T382">new</ta>
            <ta e="T384" id="Seg_10677" s="T383">accs-inf</ta>
            <ta e="T387" id="Seg_10678" s="T386">giv-inactive</ta>
            <ta e="T390" id="Seg_10679" s="T389">giv-active</ta>
            <ta e="T397" id="Seg_10680" s="T396">giv-inactive</ta>
            <ta e="T398" id="Seg_10681" s="T397">accs-inf</ta>
            <ta e="T400" id="Seg_10682" s="T399">new</ta>
            <ta e="T403" id="Seg_10683" s="T402">giv-inactive</ta>
            <ta e="T408" id="Seg_10684" s="T407">accs-sit</ta>
            <ta e="T410" id="Seg_10685" s="T409">new</ta>
            <ta e="T411" id="Seg_10686" s="T410">accs-inf</ta>
            <ta e="T415" id="Seg_10687" s="T414">new</ta>
            <ta e="T417" id="Seg_10688" s="T415">0.giv-active</ta>
            <ta e="T418" id="Seg_10689" s="T417">giv-inactive</ta>
            <ta e="T419" id="Seg_10690" s="T418">accs-sit</ta>
            <ta e="T422" id="Seg_10691" s="T421">0.giv-active</ta>
            <ta e="T424" id="Seg_10692" s="T423">accs-inf-Q</ta>
            <ta e="T425" id="Seg_10693" s="T424">0.giv-active-Q</ta>
            <ta e="T426" id="Seg_10694" s="T425">quot-sp</ta>
            <ta e="T428" id="Seg_10695" s="T427">giv-active</ta>
            <ta e="T430" id="Seg_10696" s="T429">giv-active</ta>
            <ta e="T435" id="Seg_10697" s="T433">0.giv-inactive-Q</ta>
            <ta e="T438" id="Seg_10698" s="T437">giv-active</ta>
            <ta e="T442" id="Seg_10699" s="T441">quot-sp</ta>
            <ta e="T443" id="Seg_10700" s="T442">accs-inf-Q</ta>
            <ta e="T445" id="Seg_10701" s="T444">0.giv-active-Q</ta>
            <ta e="T447" id="Seg_10702" s="T446">giv-active</ta>
            <ta e="T450" id="Seg_10703" s="T449">giv-active</ta>
            <ta e="T456" id="Seg_10704" s="T455">new</ta>
            <ta e="T458" id="Seg_10705" s="T456">0.giv-active</ta>
            <ta e="T459" id="Seg_10706" s="T458">giv-inactive-Q</ta>
            <ta e="T460" id="Seg_10707" s="T459">accs-inf-Q</ta>
            <ta e="T461" id="Seg_10708" s="T460">0.giv-active-Q</ta>
            <ta e="T463" id="Seg_10709" s="T462">0.giv-active-Q</ta>
            <ta e="T464" id="Seg_10710" s="T463">0.quot-sp</ta>
            <ta e="T466" id="Seg_10711" s="T465">giv-active</ta>
            <ta e="T467" id="Seg_10712" s="T466">giv-inactive</ta>
            <ta e="T469" id="Seg_10713" s="T468">accs-gen</ta>
            <ta e="T471" id="Seg_10714" s="T470">accs-gen</ta>
            <ta e="T473" id="Seg_10715" s="T472">accs-inf-Q</ta>
            <ta e="T475" id="Seg_10716" s="T474">accs-gen-Q</ta>
            <ta e="T479" id="Seg_10717" s="T478">accs-gen-Q</ta>
            <ta e="T480" id="Seg_10718" s="T479">giv-inactive-Q</ta>
            <ta e="T481" id="Seg_10719" s="T480">accs-inf-Q</ta>
            <ta e="T483" id="Seg_10720" s="T482">giv-active-Q</ta>
            <ta e="T484" id="Seg_10721" s="T483">0.giv-inactive-Q</ta>
            <ta e="T486" id="Seg_10722" s="T484">0.giv-active-Q</ta>
            <ta e="T487" id="Seg_10723" s="T486">quot-sp</ta>
            <ta e="T489" id="Seg_10724" s="T488">giv-active</ta>
            <ta e="T490" id="Seg_10725" s="T489">giv-inactive</ta>
            <ta e="T491" id="Seg_10726" s="T490">accs-inf-Q</ta>
            <ta e="T498" id="Seg_10727" s="T497">accs-gen-Q</ta>
            <ta e="T501" id="Seg_10728" s="T500">giv-inactive-Q</ta>
            <ta e="T502" id="Seg_10729" s="T501">accs-inf-Q</ta>
            <ta e="T504" id="Seg_10730" s="T503">giv-active-Q</ta>
            <ta e="T506" id="Seg_10731" s="T505">0.giv-active-Q</ta>
            <ta e="T507" id="Seg_10732" s="T506">accs-inf-Q</ta>
            <ta e="T509" id="Seg_10733" s="T508">giv-inactive-Q</ta>
            <ta e="T514" id="Seg_10734" s="T513">accs-gen-Q</ta>
            <ta e="T516" id="Seg_10735" s="T515">giv-active-Q</ta>
            <ta e="T518" id="Seg_10736" s="T517">0.giv-active-Q</ta>
            <ta e="T522" id="Seg_10737" s="T521">0.giv-inactive-Q</ta>
            <ta e="T524" id="Seg_10738" s="T523">quot-sp</ta>
            <ta e="T526" id="Seg_10739" s="T525">giv-inactive</ta>
            <ta e="T528" id="Seg_10740" s="T527">giv-inactive</ta>
            <ta e="T529" id="Seg_10741" s="T528">accs-sit</ta>
            <ta e="T532" id="Seg_10742" s="T531">giv-inactive</ta>
            <ta e="T533" id="Seg_10743" s="T532">accs-inf</ta>
            <ta e="T536" id="Seg_10744" s="T535">giv-inactive</ta>
            <ta e="T537" id="Seg_10745" s="T536">0.giv-active</ta>
            <ta e="T540" id="Seg_10746" s="T538">0.giv-active</ta>
            <ta e="T541" id="Seg_10747" s="T540">giv-inactive</ta>
            <ta e="T542" id="Seg_10748" s="T541">giv-inactive</ta>
            <ta e="T543" id="Seg_10749" s="T542">accs-inf</ta>
            <ta e="T545" id="Seg_10750" s="T543">0.giv-active</ta>
            <ta e="T546" id="Seg_10751" s="T545">giv-active</ta>
            <ta e="T553" id="Seg_10752" s="T552">giv-inactive</ta>
            <ta e="T554" id="Seg_10753" s="T553">giv-inactive</ta>
            <ta e="T556" id="Seg_10754" s="T555">new</ta>
            <ta e="T559" id="Seg_10755" s="T558">giv-inactive</ta>
            <ta e="T561" id="Seg_10756" s="T560">accs-inf</ta>
            <ta e="T562" id="Seg_10757" s="T561">giv-active</ta>
            <ta e="T565" id="Seg_10758" s="T564">0.giv-active</ta>
            <ta e="T568" id="Seg_10759" s="T567">giv-inactive</ta>
            <ta e="T570" id="Seg_10760" s="T569">giv-active</ta>
            <ta e="T571" id="Seg_10761" s="T570">accs-sit</ta>
            <ta e="T575" id="Seg_10762" s="T574">giv-active</ta>
            <ta e="T582" id="Seg_10763" s="T581">new</ta>
            <ta e="T584" id="Seg_10764" s="T583">giv-active</ta>
            <ta e="T585" id="Seg_10765" s="T584">giv-inactive</ta>
            <ta e="T588" id="Seg_10766" s="T587">accs-gen-Q</ta>
            <ta e="T589" id="Seg_10767" s="T588">accs-inf-Q</ta>
            <ta e="T591" id="Seg_10768" s="T590">0.giv-active-Q</ta>
            <ta e="T592" id="Seg_10769" s="T591">0.quot-sp</ta>
            <ta e="T594" id="Seg_10770" s="T593">giv-inactive</ta>
            <ta e="T596" id="Seg_10771" s="T595">0.giv-active</ta>
            <ta e="T598" id="Seg_10772" s="T597">giv-inactive</ta>
            <ta e="T601" id="Seg_10773" s="T600">giv-inactive</ta>
            <ta e="T605" id="Seg_10774" s="T604">giv-inactive</ta>
            <ta e="T607" id="Seg_10775" s="T605">0.giv-active</ta>
            <ta e="T609" id="Seg_10776" s="T608">giv-active</ta>
            <ta e="T611" id="Seg_10777" s="T610">accs-inf</ta>
            <ta e="T617" id="Seg_10778" s="T616">giv-inactive</ta>
            <ta e="T618" id="Seg_10779" s="T617">giv-inactive</ta>
            <ta e="T621" id="Seg_10780" s="T620">giv-inactive</ta>
            <ta e="T622" id="Seg_10781" s="T621">giv-active</ta>
            <ta e="T626" id="Seg_10782" s="T625">giv-inactive</ta>
            <ta e="T631" id="Seg_10783" s="T630">0.giv-active</ta>
            <ta e="T633" id="Seg_10784" s="T632">giv-active</ta>
            <ta e="T634" id="Seg_10785" s="T633">giv-inactive-Q</ta>
            <ta e="T635" id="Seg_10786" s="T634">new</ta>
            <ta e="T637" id="Seg_10787" s="T636">0.giv-inactive-Q</ta>
            <ta e="T638" id="Seg_10788" s="T637">new</ta>
            <ta e="T640" id="Seg_10789" s="T638">0.giv-active-Q</ta>
            <ta e="T641" id="Seg_10790" s="T640">giv-active-Q</ta>
            <ta e="T644" id="Seg_10791" s="T643">0.quot-sp</ta>
            <ta e="T645" id="Seg_10792" s="T644">giv-active</ta>
            <ta e="T646" id="Seg_10793" s="T645">0.giv-active</ta>
            <ta e="T647" id="Seg_10794" s="T646">giv-inactive</ta>
            <ta e="T653" id="Seg_10795" s="T652">giv-inactive</ta>
            <ta e="T655" id="Seg_10796" s="T654">giv-inactive</ta>
            <ta e="T663" id="Seg_10797" s="T662">giv-inactive</ta>
            <ta e="T665" id="Seg_10798" s="T664">accs-inf</ta>
            <ta e="T666" id="Seg_10799" s="T665">new</ta>
            <ta e="T670" id="Seg_10800" s="T669">giv-inactive</ta>
            <ta e="T672" id="Seg_10801" s="T671">accs-sit</ta>
            <ta e="T676" id="Seg_10802" s="T675">giv-active</ta>
            <ta e="T677" id="Seg_10803" s="T676">giv-inactive</ta>
            <ta e="T678" id="Seg_10804" s="T677">accs-inf</ta>
            <ta e="T680" id="Seg_10805" s="T679">new</ta>
            <ta e="T683" id="Seg_10806" s="T682">giv-active</ta>
            <ta e="T684" id="Seg_10807" s="T683">accs-inf</ta>
            <ta e="T687" id="Seg_10808" s="T686">new</ta>
            <ta e="T690" id="Seg_10809" s="T689">giv-active</ta>
            <ta e="T692" id="Seg_10810" s="T691">accs-inf</ta>
            <ta e="T695" id="Seg_10811" s="T694">giv-inactive</ta>
            <ta e="T696" id="Seg_10812" s="T695">giv-active</ta>
            <ta e="T699" id="Seg_10813" s="T698">giv-active-Q</ta>
            <ta e="T700" id="Seg_10814" s="T699">0.giv-active-Q</ta>
            <ta e="T705" id="Seg_10815" s="T704">0.giv-active-Q</ta>
            <ta e="T707" id="Seg_10816" s="T706">accs-aggr-Q</ta>
            <ta e="T708" id="Seg_10817" s="T707">accs-aggr-Q</ta>
            <ta e="T711" id="Seg_10818" s="T710">accs-aggr-Q</ta>
            <ta e="T712" id="Seg_10819" s="T711">0.giv-active-Q</ta>
            <ta e="T714" id="Seg_10820" s="T713">giv-active</ta>
            <ta e="T715" id="Seg_10821" s="T714">quot-sp</ta>
            <ta e="T716" id="Seg_10822" s="T715">giv-active</ta>
            <ta e="T717" id="Seg_10823" s="T716">accs-inf</ta>
            <ta e="T719" id="Seg_10824" s="T718">giv-active</ta>
            <ta e="T721" id="Seg_10825" s="T720">0.giv-active</ta>
            <ta e="T722" id="Seg_10826" s="T721">giv-active</ta>
            <ta e="T724" id="Seg_10827" s="T723">giv-active</ta>
            <ta e="T725" id="Seg_10828" s="T724">accs-inf</ta>
            <ta e="T731" id="Seg_10829" s="T730">0.quot-sp</ta>
            <ta e="T733" id="Seg_10830" s="T732">giv-inactive-Q</ta>
            <ta e="T736" id="Seg_10831" s="T735">new-Q</ta>
            <ta e="T738" id="Seg_10832" s="T737">giv-inactive-Q</ta>
            <ta e="T742" id="Seg_10833" s="T741">0.giv-active-Q</ta>
            <ta e="T744" id="Seg_10834" s="T743">giv-inactive</ta>
         </annotation>
         <annotation name="Top" tierref="Top" />
         <annotation name="Foc" tierref="Foc" />
         <annotation name="BOR" tierref="TIE0" />
         <annotation name="BOR-Phon" tierref="TIE1" />
         <annotation name="BOR-Morph" tierref="TIE2" />
         <annotation name="CS" tierref="TIE3" />
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_10835" s="T0">A long time ago there lived one czar.</ta>
            <ta e="T8" id="Seg_10836" s="T4">They say, he had no children.</ta>
            <ta e="T12" id="Seg_10837" s="T8">Once this czar thought: </ta>
            <ta e="T17" id="Seg_10838" s="T12">"Why don't we have any children?</ta>
            <ta e="T20" id="Seg_10839" s="T17">I should ask the old people."</ta>
            <ta e="T25" id="Seg_10840" s="T20">As he was sitting and thinking, his wife came in.</ta>
            <ta e="T28" id="Seg_10841" s="T25">"What have you been thinking about, my dear?"</ta>
            <ta e="T32" id="Seg_10842" s="T28">"Tomorrow we will gather people.</ta>
            <ta e="T40" id="Seg_10843" s="T32">Until when shall we live staring at empty walls, without children?</ta>
            <ta e="T44" id="Seg_10844" s="T40">Let us ask why we don't have children.</ta>
            <ta e="T48" id="Seg_10845" s="T44">Whom will we give our wealth when we die?"</ta>
            <ta e="T54" id="Seg_10846" s="T48">In the morning they stood up, a lot of people had gathered.</ta>
            <ta e="T57" id="Seg_10847" s="T54">There the czar asked:</ta>
            <ta e="T62" id="Seg_10848" s="T57">"Well, why don't we have any children?</ta>
            <ta e="T70" id="Seg_10849" s="T62">How can we get children, what do you think?"</ta>
            <ta e="T73" id="Seg_10850" s="T70">The people suddenly got noisy.</ta>
            <ta e="T79" id="Seg_10851" s="T73">One old buckled man peeped through the door.</ta>
            <ta e="T84" id="Seg_10852" s="T79">They pushed this poor buckled one into the middle.</ta>
            <ta e="T88" id="Seg_10853" s="T84">"Here is a wise man."</ta>
            <ta e="T96" id="Seg_10854" s="T88">The old man looked up, put his hands behind his back and said:</ta>
            <ta e="T103" id="Seg_10855" s="T96">"Make golden nets, a small golden boat and golden oars.</ta>
            <ta e="T107" id="Seg_10856" s="T103">Then the czar should go fishing.</ta>
            <ta e="T113" id="Seg_10857" s="T107">He will hold a fish in his net, the woman should eat that one."</ta>
            <ta e="T116" id="Seg_10858" s="T113">The czar's woman laughed:</ta>
            <ta e="T120" id="Seg_10859" s="T116">"Will it be enough for me?"</ta>
            <ta e="T130" id="Seg_10860" s="T120">It didn't last too long, the next morning the czar went fishing in a small golden boat with golden oars.</ta>
            <ta e="T136" id="Seg_10861" s="T130">He brought one living and trembling fish. </ta>
            <ta e="T143" id="Seg_10862" s="T136">"This is my haul", he said and gave it to the cooks.</ta>
            <ta e="T145" id="Seg_10863" s="T143">"Cook it immediately!"</ta>
            <ta e="T157" id="Seg_10864" s="T145">The cook cleaned the fish from its scales in the grass, the czar's cow ate it.</ta>
            <ta e="T168" id="Seg_10865" s="T157">After the czar's wife had eaten the fish, the cook threw the bones, which she had left on the plate, to the dog.</ta>
            <ta e="T172" id="Seg_10866" s="T168">Does it take long in the tale?</ta>
            <ta e="T176" id="Seg_10867" s="T172">The three gave birth to three boys.</ta>
            <ta e="T183" id="Seg_10868" s="T176">After a while the children started to play together.</ta>
            <ta e="T188" id="Seg_10869" s="T183">Once the czar's son said:</ta>
            <ta e="T191" id="Seg_10870" s="T188">"Friends, let us go by boat".</ta>
            <ta e="T197" id="Seg_10871" s="T191">How long they rowed, nobody knows.</ta>
            <ta e="T205" id="Seg_10872" s="T197">They saw a very big yurt standing on the other side of the river.</ta>
            <ta e="T208" id="Seg_10873" s="T205">"What is that?", they thought.</ta>
            <ta e="T210" id="Seg_10874" s="T208">The czar's son:</ta>
            <ta e="T213" id="Seg_10875" s="T210">"Let us come closer, we will play cards", he said.</ta>
            <ta e="T215" id="Seg_10876" s="T213">The dog's son:</ta>
            <ta e="T217" id="Seg_10877" s="T215">"Hey, why?</ta>
            <ta e="T221" id="Seg_10878" s="T217">Maybe it is the house of evel spirits."</ta>
            <ta e="T223" id="Seg_10879" s="T221">The cow's son:</ta>
            <ta e="T225" id="Seg_10880" s="T223">"Let's go closer, let's go closer.</ta>
            <ta e="T227" id="Seg_10881" s="T225">Let's move a little bit."</ta>
            <ta e="T234" id="Seg_10882" s="T227">As they went out on the shore, these two boys sat down to play cards.</ta>
            <ta e="T238" id="Seg_10883" s="T234">The dog's son fell asleep.</ta>
            <ta e="T247" id="Seg_10884" s="T238">For how long they played, nobody knows, these children also fell asleep.</ta>
            <ta e="T256" id="Seg_10885" s="T247">At midnight the dog's son woke up, the sound of human steps was heard.</ta>
            <ta e="T263" id="Seg_10886" s="T256">An evel spirit came up, he goes in the direction of the yurt.</ta>
            <ta e="T272" id="Seg_10887" s="T263">"Oh, now we will die", he thought and tried to wake up his friends in vain.</ta>
            <ta e="T276" id="Seg_10888" s="T272">Will these boys ever wake up?</ta>
            <ta e="T283" id="Seg_10889" s="T276">"Come, come, I will eat you", the voice of the evel spirit was heard.</ta>
            <ta e="T291" id="Seg_10890" s="T283">The boy jumped up and fought with the evel spirit.</ta>
            <ta e="T300" id="Seg_10891" s="T291">It didn't last too long, he split the evel spirit's head on a big stone.</ta>
            <ta e="T313" id="Seg_10892" s="T300">After that he got a lettered stone out of the skull and put it into his pocket. </ta>
            <ta e="T322" id="Seg_10893" s="T313">In the morning the dog's son woke up his friends, as if nothing had happened.</ta>
            <ta e="T325" id="Seg_10894" s="T322">"Hey, let us hunt some geese".</ta>
            <ta e="T331" id="Seg_10895" s="T325">The boys stretched themselves, hardly stood up, drank tea.</ta>
            <ta e="T334" id="Seg_10896" s="T331">One day in a tale is short.</ta>
            <ta e="T339" id="Seg_10897" s="T334">Soon it turned dark.</ta>
            <ta e="T349" id="Seg_10898" s="T339">The dog's son ate and fell asleep, his friends sat down to play cards again.</ta>
            <ta e="T357" id="Seg_10899" s="T349">At midnight he woke up, his friends were sitting as before and had fallen asleep.</ta>
            <ta e="T370" id="Seg_10900" s="T357">After it woke up, this child began to listen: somewhere far away the sound of steps was heard again.</ta>
            <ta e="T377" id="Seg_10901" s="T370">He tried to wake up his friends, but will those who just fell asleep ever wake up?</ta>
            <ta e="T389" id="Seg_10902" s="T377">The dog's son went out and split also the head of the second evil spirit on this big stone.</ta>
            <ta e="T395" id="Seg_10903" s="T389">The evel spirit fidgeted wit his legs and stretched himself.</ta>
            <ta e="T406" id="Seg_10904" s="T395">The dog's son got a lettered stone out of the skull and put it again into his pocket. </ta>
            <ta e="T417" id="Seg_10905" s="T406">In the third night he split the head of the third evel spirit and pulled a lettered stone out of it.</ta>
            <ta e="T422" id="Seg_10906" s="T417">In the dawn he woke up his friends.</ta>
            <ta e="T428" id="Seg_10907" s="T422">"Hey, let's go home", that boy said.</ta>
            <ta e="T431" id="Seg_10908" s="T428">The czar's son baulked:</ta>
            <ta e="T433" id="Seg_10909" s="T431">"But why!</ta>
            <ta e="T436" id="Seg_10910" s="T433">Let us stay [here] more, it is fun."</ta>
            <ta e="T442" id="Seg_10911" s="T436">The cow's son sat for a while and said: </ta>
            <ta e="T446" id="Seg_10912" s="T442">"Our families are waiting, let's go."</ta>
            <ta e="T448" id="Seg_10913" s="T446">The boys hit the road.</ta>
            <ta e="T452" id="Seg_10914" s="T448">The dog's son rowed.</ta>
            <ta e="T458" id="Seg_10915" s="T452">After a while he clawed at the sheath [of his knife] [and said]:</ta>
            <ta e="T464" id="Seg_10916" s="T458">"Friends, I forgot my knife, I will go to search for it", he said.</ta>
            <ta e="T472" id="Seg_10917" s="T464">The dog's son came up to the yurt, there crying voices of women were heard.</ta>
            <ta e="T482" id="Seg_10918" s="T472">"I will change into tasty food and fall on a big plate into their boat.</ta>
            <ta e="T490" id="Seg_10919" s="T482">If they eat me, they will die", said the voice of Baba Yaga.</ta>
            <ta e="T506" id="Seg_10920" s="T490">"I, though, will change into a silk kerchief of never seen beauty and fall onto the knees of the czar's son and then I let them all hang up theirselves."</ta>
            <ta e="T518" id="Seg_10921" s="T506">"I, though, will change into liquid tar which glues tightly to the small boat, I will pull out and drown each of them."</ta>
            <ta e="T520" id="Seg_10922" s="T518">"Well, then.</ta>
            <ta e="T526" id="Seg_10923" s="T520">You've thought it through very well", said the first woman.</ta>
            <ta e="T540" id="Seg_10924" s="T526">As the dog's son heard this, he ran down the high shore to the boat and started to row.</ta>
            <ta e="T545" id="Seg_10925" s="T540">He came up with his friends in the middle of the river. </ta>
            <ta e="T549" id="Seg_10926" s="T545">The boys rowed very silently.</ta>
            <ta e="T557" id="Seg_10927" s="T549">It was not long, a plate with food fell down into the boat.</ta>
            <ta e="T573" id="Seg_10928" s="T557">The czar's son was happy and streched his hand to the plate, there the dog's son threw the plate with the food into the water.</ta>
            <ta e="T586" id="Seg_10929" s="T573">As the czar's son was sitting there offended, a very beautiful silk kerchief fell onto his knees.</ta>
            <ta e="T607" id="Seg_10930" s="T586">"Oh god, I will give it as a present to my mother", he said and just wanted to put it into his pocker as the [dog's] son took it, cut it with his knife and threw it into the water.</ta>
            <ta e="T613" id="Seg_10931" s="T607">The czar's son was so angry, that he couldn't say a word.</ta>
            <ta e="T624" id="Seg_10932" s="T613">Sooner or later the small boat sticked to the water, the river turned black.</ta>
            <ta e="T630" id="Seg_10933" s="T624">The boys couldn't row anymore.</ta>
            <ta e="T631" id="Seg_10934" s="T630">They got afraid.</ta>
            <ta e="T633" id="Seg_10935" s="T631">The dog's son: </ta>
            <ta e="T645" id="Seg_10936" s="T633">"Cut the water with an axe, pierce it with a knife, I will row", he said to his friends.</ta>
            <ta e="T651" id="Seg_10937" s="T645">They saw that the water really lightened and turned red.</ta>
            <ta e="T654" id="Seg_10938" s="T651">Soon their small boat got free.</ta>
            <ta e="T658" id="Seg_10939" s="T654">The boys rowed silently.</ta>
            <ta e="T667" id="Seg_10940" s="T658">Sooner or later their city was seen on the high shore of the river.</ta>
            <ta e="T674" id="Seg_10941" s="T667">After they came ashore, the boys separated in three directions.</ta>
            <ta e="T693" id="Seg_10942" s="T674">The czar's son went to the white house on the top of the shore, the dog's son went to the burrow house of his mother, the cow's son met his mother outside.</ta>
            <ta e="T697" id="Seg_10943" s="T693">In the morning the czar called the boys.</ta>
            <ta e="T705" id="Seg_10944" s="T697">"Well, my children, tell me what you were doing for so long, where you stayed?"</ta>
            <ta e="T721" id="Seg_10945" s="T705">"In the skulls of three evel spirits I found three lettered stones", the dog's son said and gave the three stones to the czar.</ta>
            <ta e="T731" id="Seg_10946" s="T721">The czar embraced the dog's son, cried and said:</ta>
            <ta e="T737" id="Seg_10947" s="T731">"From today it's your destiny to be a czar.</ta>
            <ta e="T742" id="Seg_10948" s="T737">Live with my son like brothers."</ta>
            <ta e="T747" id="Seg_10949" s="T742">The three boys stayed friends and lived rich and wealthy.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_10950" s="T0">Vor langer, langer Zeit lebte ein Zar.</ta>
            <ta e="T8" id="Seg_10951" s="T4">Man sagt, dass er keine Kinder hatte.</ta>
            <ta e="T12" id="Seg_10952" s="T8">Einmal dachte dieser Zar: </ta>
            <ta e="T17" id="Seg_10953" s="T12">"Warum haben wir keine Kinder?</ta>
            <ta e="T20" id="Seg_10954" s="T17">Mal bei den Alten nachfragen."</ta>
            <ta e="T25" id="Seg_10955" s="T20">Als er saß und nachdachte, kam seine Frau herein:</ta>
            <ta e="T28" id="Seg_10956" s="T25">"Worüber denkst du nach, mein Lieber?"</ta>
            <ta e="T32" id="Seg_10957" s="T28">"Morgen versammeln wir Leute.</ta>
            <ta e="T40" id="Seg_10958" s="T32">Wie lange sollen wir leben und dabei nur die leere Wand anstarren, ohne Kinder?</ta>
            <ta e="T44" id="Seg_10959" s="T40">Lass uns fragen, warum wir keine Kinder haben.</ta>
            <ta e="T48" id="Seg_10960" s="T44">Wem vermachen wir unseren Reichtum, wenn wir sterben?"</ta>
            <ta e="T54" id="Seg_10961" s="T48">Am Morgen standen sie auf, es haben sich sehr viele Leute versammelt.</ta>
            <ta e="T57" id="Seg_10962" s="T54">Da fragte der Zar:</ta>
            <ta e="T62" id="Seg_10963" s="T57">"Nun, warum haben wir keine Kinder?</ta>
            <ta e="T70" id="Seg_10964" s="T62">Was ist zu tun, damit wir Kinder bekommen, was denkt ihr?"</ta>
            <ta e="T73" id="Seg_10965" s="T70">Die Leute fingen plötzlich an zu lärmen.</ta>
            <ta e="T79" id="Seg_10966" s="T73">Ein alter krummer Mann lehnte sich in die Tür.</ta>
            <ta e="T84" id="Seg_10967" s="T79">Diesen armen Krummen stießen sie in die Mitte:</ta>
            <ta e="T88" id="Seg_10968" s="T84">"Hier ist ein wissender Mensch."</ta>
            <ta e="T96" id="Seg_10969" s="T88">Der alte Mann schaute hoch, griff sich an den Rücken und sagte:</ta>
            <ta e="T103" id="Seg_10970" s="T96">"Macht goldene Netze, ein goldenes kleines Boot, goldene Ruder.</ta>
            <ta e="T107" id="Seg_10971" s="T103">Dann soll der Zar fischen gehen.</ta>
            <ta e="T113" id="Seg_10972" s="T107">Im Netz wird er einen Fisch halten, diesen soll die Frau essen."</ta>
            <ta e="T116" id="Seg_10973" s="T113">Die Frau des Zaren lachte:</ta>
            <ta e="T120" id="Seg_10974" s="T116">"Davon soll ich satt werden?"</ta>
            <ta e="T130" id="Seg_10975" s="T120">Nicht lange dauerte es, da ging der Zar am nächsten Morgen mit einem goldenen kleinen Boot und goldenen Rudern zum fischen.</ta>
            <ta e="T136" id="Seg_10976" s="T130">Einen lebenden zitternden Fisch bringt er wohl.</ta>
            <ta e="T143" id="Seg_10977" s="T136">"Dies ist mein Fang", sagte er und gab ihn ihren Köchen.</ta>
            <ta e="T145" id="Seg_10978" s="T143">"Jetzt kocht ihn!"</ta>
            <ta e="T157" id="Seg_10979" s="T145">Im Gras säuberte der Koch den Fisch von Schuppen, diese fraß die Kuh des Zaren.</ta>
            <ta e="T168" id="Seg_10980" s="T157">Nachdem die Frau des Zaren den Fisch gegessen hatte, warf der Koch die Gräten, die sie auf dem Teller gelassen hatte, dem Hund hin.</ta>
            <ta e="T172" id="Seg_10981" s="T168">Dauert das lange im Märchen?</ta>
            <ta e="T176" id="Seg_10982" s="T172">Die drei gebaren drei Jungen.</ta>
            <ta e="T183" id="Seg_10983" s="T176">Es verging nicht viel [Zeit], da fingen die Kinder an zusammen zu spielen.</ta>
            <ta e="T188" id="Seg_10984" s="T183">Da sagte einmal der Sohn des Zaren:</ta>
            <ta e="T191" id="Seg_10985" s="T188">"Freunde, gehen wir Boot fahren."</ta>
            <ta e="T197" id="Seg_10986" s="T191">Wie lange die Jungen ruderten, weiß man nicht.</ta>
            <ta e="T205" id="Seg_10987" s="T197">Sie sahen, dass am anderen Ufer des Flusses eine sehr große Jurte stand.</ta>
            <ta e="T208" id="Seg_10988" s="T205">"Was ist das?", fragten sie sich.</ta>
            <ta e="T210" id="Seg_10989" s="T208">Der Sohn des Zaren:</ta>
            <ta e="T213" id="Seg_10990" s="T210">"Fahren wir näher heran, dann spielen wir Karten", sagte er.</ta>
            <ta e="T215" id="Seg_10991" s="T213">Der Sohn des Hundes:</ta>
            <ta e="T217" id="Seg_10992" s="T215">"Äh, warum?</ta>
            <ta e="T221" id="Seg_10993" s="T217">Vielleicht ist es das Haus von bösen Geistern."</ta>
            <ta e="T223" id="Seg_10994" s="T221">Der Sohn der Kuh:</ta>
            <ta e="T225" id="Seg_10995" s="T223">"Gehen wir hin, gehen wir hin.</ta>
            <ta e="T227" id="Seg_10996" s="T225">Bewegen wir uns ein Bisschen."</ta>
            <ta e="T234" id="Seg_10997" s="T227">Als sie an Land gegangen waren, setzten sich diese zwei Jungen und spielten Karten.</ta>
            <ta e="T238" id="Seg_10998" s="T234">Der Sohn des Hundes schlief ein.</ta>
            <ta e="T247" id="Seg_10999" s="T238">Wie lange sie spielten, weiß man nicht, diese Kinder schliefen auch ein.</ta>
            <ta e="T256" id="Seg_11000" s="T247">Um Mitternacht wachte der Sohn des Hundes auf, es war das Geräusch menschlicher Schritte zu hören.</ta>
            <ta e="T263" id="Seg_11001" s="T256">Es erschien ein böser Geist, er geht in Richtung der Jurte.</ta>
            <ta e="T272" id="Seg_11002" s="T263">"Oh, nun sind wir wohl tot", dachte er und versuchte vergeblich seine Freunde aufzuwecken.</ta>
            <ta e="T276" id="Seg_11003" s="T272">Wachen diese Jungen denn auf?</ta>
            <ta e="T283" id="Seg_11004" s="T276">"Komm, komm, ich werde dich essen", war die Stimme des bösen Geistes zu hören.</ta>
            <ta e="T291" id="Seg_11005" s="T283">Der Junge sprang auf und kämpfte mit dem bösen Geist.</ta>
            <ta e="T300" id="Seg_11006" s="T291">Nicht lange dauerte es und er spaltete den Kopf des bösen Geistes auf einem großen Stein.</ta>
            <ta e="T313" id="Seg_11007" s="T300">Dann holte er aus dem Schädel einen beschrifteten Stein heraus, er steckte den Stein in seine Tasche.</ta>
            <ta e="T322" id="Seg_11008" s="T313">Am Morgen weckte der Sohn des Hundes seine Freunde, als sei nichts gewesen.</ta>
            <ta e="T325" id="Seg_11009" s="T322">"Hey, gehen wir Gänse jagen."</ta>
            <ta e="T331" id="Seg_11010" s="T325">Die Jungen streckten sich, standen kaum auf und tranken Tee.</ta>
            <ta e="T334" id="Seg_11011" s="T331">Im Märchen ist ein Tag kurz.</ta>
            <ta e="T339" id="Seg_11012" s="T334">Es verging nicht viel Zeit und es wurde dunkel.</ta>
            <ta e="T349" id="Seg_11013" s="T339">Der Sohn des Hundes aß und schlief dann ein, seine Freunde setzten sich wieder, um Karten zu spielen.</ta>
            <ta e="T357" id="Seg_11014" s="T349">Um Mitternacht wachte er auf, so wie seine Freunde sich gesetzt hatten, saßen sie noch und waren eingeschlafen.</ta>
            <ta e="T370" id="Seg_11015" s="T357">Nachdem es aufgewacht war, fing dieses Kind an zu lauschen: Irgendwo in der Ferne waren wieder das Geräusch von Schritten zu hören.</ta>
            <ta e="T377" id="Seg_11016" s="T370">Er versuchte seine Freunde zu wecken, aber wachen denn diese eingeschlafenen Menschen nicht auf?</ta>
            <ta e="T389" id="Seg_11017" s="T377">Der Sohn des Hundes ging hinaus und zerschlug auch den Kopf des zweiten bösen Geistes auf dem großen Stein.</ta>
            <ta e="T395" id="Seg_11018" s="T389">Der böse Geist zappelte mit den Beinen und streckte sich.</ta>
            <ta e="T406" id="Seg_11019" s="T395">Der Sohn des Hundes zog aus dem Schädel einen beschrifteten Stein und steckte auch diesen in seine Tasche.</ta>
            <ta e="T417" id="Seg_11020" s="T406">In der dritten Nacht zerschlug er den Kopf des dritten bösen Geistes und zog einen beschrifteten Stein heraus.</ta>
            <ta e="T422" id="Seg_11021" s="T417">Als es dämmerte, weckte er seine Freunde auf.</ta>
            <ta e="T428" id="Seg_11022" s="T422">"Hey, fahren wir nach Hause", sagte dieser Junge.</ta>
            <ta e="T431" id="Seg_11023" s="T428">Der Sohn des Zaren sträubte sich:</ta>
            <ta e="T433" id="Seg_11024" s="T431">"Warum denn!</ta>
            <ta e="T436" id="Seg_11025" s="T433">Lasst uns [hier] noch etwas weiterleben, es ist fröhlich."</ta>
            <ta e="T442" id="Seg_11026" s="T436">Der Sohn der Kuh saß etwas da und sagte:</ta>
            <ta e="T446" id="Seg_11027" s="T442">"Unsere Familien werden uns erwarten, fahren wir also."</ta>
            <ta e="T448" id="Seg_11028" s="T446">Die Jungen machte sich auf den Weg.</ta>
            <ta e="T452" id="Seg_11029" s="T448">Der Sohn des Hundes ruderte.</ta>
            <ta e="T458" id="Seg_11030" s="T452">Nach einiger Zeit griff er nach der Scheide [seines Messers] [und sagte]:</ta>
            <ta e="T464" id="Seg_11031" s="T458">"Freunde, ich habe mein Messer vergessen, ich gehe es suchen", sagte er.</ta>
            <ta e="T472" id="Seg_11032" s="T464">Der Sohn des Hundes kam zur Jurte, dort waren weinende Frauenstimmen zu hören.</ta>
            <ta e="T482" id="Seg_11033" s="T472">"Ich werde zu leckerem Essen und lande auf einem großen Teller in ihrem kleinen Boot.</ta>
            <ta e="T490" id="Seg_11034" s="T482">Wenn sie mich essen, dann werden sie sterben", sagte die Stimme der alten Baba Jaga.</ta>
            <ta e="T506" id="Seg_11035" s="T490">"Ich aber, wenn ich ein Seidentuch von noch nie gesehener Schönheit geworden bin, falle auf die Knie des Zarensohns und dann lasse ich sie alle sich aufhängen."</ta>
            <ta e="T518" id="Seg_11036" s="T506">"Und ich werde zu flüssigem Teer, der eng am kleinen Boot klebt, ich ziehe alle heraus und ertränke sie."</ta>
            <ta e="T520" id="Seg_11037" s="T518">"Nun gut.</ta>
            <ta e="T526" id="Seg_11038" s="T520">Das habt ihr euch wohl hervorragend ausgedacht", sagte die erste Frau.</ta>
            <ta e="T540" id="Seg_11039" s="T526">Als der Sohn des Hundes das hörte, lief er vom Ufer hinunter, nahm das Boot und ruderte los.</ta>
            <ta e="T545" id="Seg_11040" s="T540">Seine Freunde erreichte er in der Mitte des Flusses.</ta>
            <ta e="T549" id="Seg_11041" s="T545">Die Jungen ruderten ganz leise.</ta>
            <ta e="T557" id="Seg_11042" s="T549">Es verging nicht viel Zeit, die fiel ein Teller mit Essen in ihr Boot.</ta>
            <ta e="T573" id="Seg_11043" s="T557">Der Sohn des Zaren freute sich, er streckte die Hand in Richtung des Tellers, da warf der Sohn des Hundes den Teller mit Essen ins Wasser.</ta>
            <ta e="T586" id="Seg_11044" s="T573">Als der Sohn des Zaren beleidigt da saß, fiel ihm ein sehr schönes Seidentuch auf die Knie.</ta>
            <ta e="T607" id="Seg_11045" s="T586">"Mein Gott, ich schenke es der Mama", sagte er und wollte es gerade in seine Tasche stecken, da griff der Sohn [des Hundes] es, zerschnitt es und warf es ins Wasser.</ta>
            <ta e="T613" id="Seg_11046" s="T607">Vor Ärger konnte der Sohn des Zaren nichts sagen.</ta>
            <ta e="T624" id="Seg_11047" s="T613">Es verging nicht viel Zeit, da blieb das Boot am Wasser kleben und das Wasser wurde schwarz.</ta>
            <ta e="T630" id="Seg_11048" s="T624">Die Jungen konnte nicht mehr rudern.</ta>
            <ta e="T631" id="Seg_11049" s="T630">Sie bekamen Angst.</ta>
            <ta e="T633" id="Seg_11050" s="T631">Der Sohn des Hundes:</ta>
            <ta e="T645" id="Seg_11051" s="T633">"Zerteilt das Wasser mit einem Beil, durchstecht es mit dem Messer, ich werde rudern", sagte er seinen Freunden.</ta>
            <ta e="T651" id="Seg_11052" s="T645">Sie sahen, dass das Wasser tatsächlich heller wurde, es wurde rot.</ta>
            <ta e="T654" id="Seg_11053" s="T651">Bald kam ihr Boot frei.</ta>
            <ta e="T658" id="Seg_11054" s="T654">Die Jungen ruderten ganz leise.</ta>
            <ta e="T667" id="Seg_11055" s="T658">Es dauerte nicht lange, da kam auf dem hohen Ufer des Flusses ihre Stadt in Sicht.</ta>
            <ta e="T674" id="Seg_11056" s="T667">Nachdem sie angelegt hatten, gingen sich die Jungen in drei Richtungen auseinander.</ta>
            <ta e="T693" id="Seg_11057" s="T674">Der Sohn des Zaren ging zum weißen Haus auf dem hohen Ufer, der Sohn des Hundes kroch in die niedrige Hütte seiner Mutter, der Sohn der Kuh traf seine Mutter draußen.</ta>
            <ta e="T697" id="Seg_11058" s="T693">Am Morgen rief der Zar die Jungen:</ta>
            <ta e="T705" id="Seg_11059" s="T697">"Nun, meine Kinder, erzählt, was ihr so lange gemacht habe, wo ihr wart?"</ta>
            <ta e="T721" id="Seg_11060" s="T705">"In den Schädeln von drei bösen Geistern habe ich drei beschriftete Steine gefunden", sagte der Sohn des Hundes und überreichte des Zaren die drei Steine.</ta>
            <ta e="T731" id="Seg_11061" s="T721">Der Zar umarmte den Sohn des Hundes, fing an zu weinen und sagte:</ta>
            <ta e="T737" id="Seg_11062" s="T731">"Ab heute ist es dir beschieden, Zar zu sein.</ta>
            <ta e="T742" id="Seg_11063" s="T737">Lebe mit meinem Sohn wie Brüder."</ta>
            <ta e="T747" id="Seg_11064" s="T742">Die drei Jungen lebten in Freundschaft, sie lebten in Reichtum und Wohlstand.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_11065" s="T0">Давным-давно жил один царь.</ta>
            <ta e="T8" id="Seg_11066" s="T4">Он не имел детей, говорят.</ta>
            <ta e="T12" id="Seg_11067" s="T8">Однажды этот царь призадумался: </ta>
            <ta e="T17" id="Seg_11068" s="T12">"Почему же у нас нет детей?</ta>
            <ta e="T20" id="Seg_11069" s="T17">Спросить бы у старых людей."</ta>
            <ta e="T25" id="Seg_11070" s="T20">Когда так, задумаясь, сидел, вошла жена:</ta>
            <ta e="T28" id="Seg_11071" s="T25">"О чем задумался, милый?"</ta>
            <ta e="T32" id="Seg_11072" s="T28">"Завтра мы соберем людей.</ta>
            <ta e="T40" id="Seg_11073" s="T32">До каких пор будем жить, глядя на пустые стены, без детей?</ta>
            <ta e="T44" id="Seg_11074" s="T40">Спросим, почему нет у нас детей.</ta>
            <ta e="T48" id="Seg_11075" s="T44">Кому оставим наше богатство, когда умрем?"</ta>
            <ta e="T54" id="Seg_11076" s="T48">Утром встали — очень много людей собралось.</ta>
            <ta e="T57" id="Seg_11077" s="T54">Тут царь спросил:</ta>
            <ta e="T62" id="Seg_11078" s="T57">"Почему, вот, мы не имеем детей?</ta>
            <ta e="T70" id="Seg_11079" s="T62">Что сделать, чтобы заполучить ребенка, что вы об этом думаете?"</ta>
            <ta e="T73" id="Seg_11080" s="T70">Люди вдруг зашумели.</ta>
            <ta e="T79" id="Seg_11081" s="T73">В дверь заглянул один кривой дряхлый старик.</ta>
            <ta e="T84" id="Seg_11082" s="T79">Бедного кривого вытолкнули на середину:</ta>
            <ta e="T88" id="Seg_11083" s="T84">"Вот знающий человек."</ta>
            <ta e="T96" id="Seg_11084" s="T88">Старик взглянул снизу вверх, заложил руки за спину и сказал:</ta>
            <ta e="T103" id="Seg_11085" s="T96">"Готовьте золотые сети, золотую лодочку, золотые весла.</ta>
            <ta e="T107" id="Seg_11086" s="T103">Потом пусть царь едет рыбачить.</ta>
            <ta e="T113" id="Seg_11087" s="T107">В сети попадется рыба, пусть жена ее съест."</ta>
            <ta e="T116" id="Seg_11088" s="T113">Царица засмеялась:</ta>
            <ta e="T120" id="Seg_11089" s="T116">"Наемся ли ею?"</ta>
            <ta e="T130" id="Seg_11090" s="T120">Долго не было, царь назавтра в золотой лодочке с золотым веслом уехал рыбачить.</ta>
            <ta e="T136" id="Seg_11091" s="T130">Одну живую трепещущую рыбку тут приносит.</ta>
            <ta e="T143" id="Seg_11092" s="T136">"Вот моя добыча", сказал и поварам отдал.</ta>
            <ta e="T145" id="Seg_11093" s="T143">"Сейчас же сварите!"</ta>
            <ta e="T157" id="Seg_11094" s="T145">Повар рыбу от чешуи очистил в траве — ее съела корова царя.</ta>
            <ta e="T168" id="Seg_11095" s="T157">Царица, съев рыбу, кости оставила на тарелке, повар их выбросил собаке.</ta>
            <ta e="T172" id="Seg_11096" s="T168">Долго ли делается в олонгко?</ta>
            <ta e="T176" id="Seg_11097" s="T172">Все трое родили трех мальчиков.</ta>
            <ta e="T183" id="Seg_11098" s="T176">Сколько-то [времени] прошло, дети стали играть вместе.</ta>
            <ta e="T188" id="Seg_11099" s="T183">Вот однажды царский сын говорит:</ta>
            <ta e="T191" id="Seg_11100" s="T188">"Друзья, поедем на лодке."</ta>
            <ta e="T197" id="Seg_11101" s="T191">Сколько гребли парни — неизвестно.</ta>
            <ta e="T205" id="Seg_11102" s="T197">Увидели: на том берегу реки стоит большущиая юрта.</ta>
            <ta e="T208" id="Seg_11103" s="T205">"Что это?", удивились.</ta>
            <ta e="T210" id="Seg_11104" s="T208">Царский сын:</ta>
            <ta e="T213" id="Seg_11105" s="T210">"Пристанем, поиграем в карты", сказал.</ta>
            <ta e="T215" id="Seg_11106" s="T213">Сын собаки:</ta>
            <ta e="T217" id="Seg_11107" s="T215">"Э, зачем?</ta>
            <ta e="T221" id="Seg_11108" s="T217">Может, это жилище злых духов."</ta>
            <ta e="T223" id="Seg_11109" s="T221">Сын коровы:</ta>
            <ta e="T225" id="Seg_11110" s="T223">"Пристанем, пристанем.</ta>
            <ta e="T227" id="Seg_11111" s="T225">Немного разомнемся."</ta>
            <ta e="T234" id="Seg_11112" s="T227">Как только вышли на берег, эти два парня сели играть в карты.</ta>
            <ta e="T238" id="Seg_11113" s="T234">Сын собаки уснул.</ta>
            <ta e="T247" id="Seg_11114" s="T238">Сколько играли — неизвестно, эти дети тоже уснули.</ta>
            <ta e="T256" id="Seg_11115" s="T247">В полночь проснулся сын собаки, звук человеческих шагов послышался.</ta>
            <ta e="T263" id="Seg_11116" s="T256">Появлялся — злой дуx, идет в сторону юрты, вот.</ta>
            <ta e="T272" id="Seg_11117" s="T263">"О, ну погибли мы, кажется", подумав, напрасно попытался разбудить друзей.</ta>
            <ta e="T276" id="Seg_11118" s="T272">Те парни разве проснутся?</ta>
            <ta e="T283" id="Seg_11119" s="T276">"Иди-иди, я тебя съем", послышался голос злого духа.</ta>
            <ta e="T291" id="Seg_11120" s="T283">Парень тут выскочил и побился с злым духом.</ta>
            <ta e="T300" id="Seg_11121" s="T291">Много ли [времени] прошло — голову злого духа разбил на валуне.</ta>
            <ta e="T313" id="Seg_11122" s="T300">После этого из черепа вынул камень с письменами, положил камень в карман.</ta>
            <ta e="T322" id="Seg_11123" s="T313">Утром, будто ничего не произошло, сын собаки разбудил друзей:</ta>
            <ta e="T325" id="Seg_11124" s="T322">"Давайте пойдем поохотимся на гусей."</ta>
            <ta e="T331" id="Seg_11125" s="T325">Парни, потягиваясь, еле встали, стали чай пить.</ta>
            <ta e="T334" id="Seg_11126" s="T331">В сказке день короткий.</ta>
            <ta e="T339" id="Seg_11127" s="T334">Много ли [времени] прошло — стемнело.</ta>
            <ta e="T349" id="Seg_11128" s="T339">Сын собаки поел и уснул, а друзья его опять сели играть в карты.</ta>
            <ta e="T357" id="Seg_11129" s="T349">Проснулся в полночь — друзья как сидели, так и заснули.</ta>
            <ta e="T370" id="Seg_11130" s="T357">Проснувшись, этот парень стал прислушиваться — где-то далеко послышались чьи-то шаги.</ta>
            <ta e="T377" id="Seg_11131" s="T370">Пытался разбудить друзей, да разве проснутся только что уснувшие люди?</ta>
            <ta e="T389" id="Seg_11132" s="T377">Сын собаки вышел и голову второго злого духа тоже разбил на валуне.</ta>
            <ta e="T395" id="Seg_11133" s="T389">Злой дух ногами дернул — и вытянулся.</ta>
            <ta e="T406" id="Seg_11134" s="T395">Сын собаки из черепа вынул камень с письменами и опять положил в карман.</ta>
            <ta e="T417" id="Seg_11135" s="T406">На третью ночь разбил голову третьему злому духу и успел вынуть камень с письменами.</ta>
            <ta e="T422" id="Seg_11136" s="T417">Как только встала заря, разбудил друзей.</ta>
            <ta e="T428" id="Seg_11137" s="T422">"Давайте поедем домой", сказал этот парень.</ta>
            <ta e="T431" id="Seg_11138" s="T428">Царский сын заупрямился:</ta>
            <ta e="T433" id="Seg_11139" s="T431">"Зачем же!</ta>
            <ta e="T436" id="Seg_11140" s="T433">Еще поживем, весело-то как."</ta>
            <ta e="T442" id="Seg_11141" s="T436">Сын коровы, посидев немного, сказал:</ta>
            <ta e="T446" id="Seg_11142" s="T442">"Родные заждались, поедем все же."</ta>
            <ta e="T448" id="Seg_11143" s="T446">Парни тронулись в путь.</ta>
            <ta e="T452" id="Seg_11144" s="T448">Сын собаки греб.</ta>
            <ta e="T458" id="Seg_11145" s="T452">Много ли [времени] прошло, ощупав ножны, [сказал]:</ta>
            <ta e="T464" id="Seg_11146" s="T458">"Друзья, забыл я нож, пойду поищу", сказал.</ta>
            <ta e="T472" id="Seg_11147" s="T464">Сын собаки подошел к юрте, тут послышался плач женщин.</ta>
            <ta e="T482" id="Seg_11148" s="T472">"Я стану вкусной пищей и спущусь на большой тарелке в их лодку.</ta>
            <ta e="T490" id="Seg_11149" s="T482">Когда отпробуют меня, все передохнут", сказал голос старой Бабы-Яги.</ta>
            <ta e="T506" id="Seg_11150" s="T490">"А я-то, став невиданно красивым шелковым платком, расстелюсь на коленях царского сына и заставлю всех удавиться."</ta>
            <ta e="T518" id="Seg_11151" s="T506">"А я-то стану дегтем, приклею намертво их лодку и, всех утянув, утоплю."</ta>
            <ta e="T520" id="Seg_11152" s="T518">"Ну ладно.</ta>
            <ta e="T526" id="Seg_11153" s="T520">Хорошо задумали, оказывается", сказала первая женщина.</ta>
            <ta e="T540" id="Seg_11154" s="T526">Как услышал это сын собаки, сбежал вниз с берега, сел в лодочку и стал грести.</ta>
            <ta e="T545" id="Seg_11155" s="T540">Друзей нагнал на середине реки.</ta>
            <ta e="T549" id="Seg_11156" s="T545">Парни потихоньку гребли.</ta>
            <ta e="T557" id="Seg_11157" s="T549">Много ли [времени] прошло, в лодку спустилась тарелка с едой.</ta>
            <ta e="T573" id="Seg_11158" s="T557">Царский сын обрадовался, потянул пальцы в сторону тарелки — сын собаки тарелку столкнул в воду.</ta>
            <ta e="T586" id="Seg_11159" s="T573">Когда царский сын от досады с вытянутым носом сидел, на коленях его расстелился красивый шелковый платок.</ta>
            <ta e="T607" id="Seg_11160" s="T586">"О, боже, маме сделаю подарок", сказал, и только хотел было положить в карман, как сын [собаки] выхватил и, ножом искромсав, бросил [платок] в воду.</ta>
            <ta e="T613" id="Seg_11161" s="T607">От ярости царский сын даже слова не мог вымолвить.</ta>
            <ta e="T624" id="Seg_11162" s="T613">Много ли еще [времени] прошло — лодка прилипла к воде, вода реки почернела.</ta>
            <ta e="T630" id="Seg_11163" s="T624">Эти парни совсем грести не могут.</ta>
            <ta e="T631" id="Seg_11164" s="T630">Испугались.</ta>
            <ta e="T633" id="Seg_11165" s="T631">Сын собаки:</ta>
            <ta e="T645" id="Seg_11166" s="T633">"Воду рубите топором, протыкайте ножом, а я стану грести", сказал своим друзьям.</ta>
            <ta e="T651" id="Seg_11167" s="T645">Взглянули — вода и вправду стала светлеть, алеть.</ta>
            <ta e="T654" id="Seg_11168" s="T651">Скоро лодка их освободилась.</ta>
            <ta e="T658" id="Seg_11169" s="T654">Парни гребли потихоньку.</ta>
            <ta e="T667" id="Seg_11170" s="T658">Много ли [времени] прошло — на высоком берегу реки показался их город.</ta>
            <ta e="T674" id="Seg_11171" s="T667">Пристали к берегу и разошлись на три стороны.</ta>
            <ta e="T693" id="Seg_11172" s="T674">Царский сын пошел к белому дому на берегу, сын собаки нырнул в низкий дом-конуру матери, сын коровы мать во дворе встретил.</ta>
            <ta e="T697" id="Seg_11173" s="T693">Утром царь позвал парней:</ta>
            <ta e="T705" id="Seg_11174" s="T697">"Ну, дети мои, рассказывайте, что так долго ездили?"</ta>
            <ta e="T721" id="Seg_11175" s="T705">"В черепах трех злых духов нашел три камня с письменами", сказал сын собаки и вручил царю три камня.</ta>
            <ta e="T731" id="Seg_11176" s="T721">Царь сына собаки обнял за шею и, всплакнув, сказал:</ta>
            <ta e="T737" id="Seg_11177" s="T731">"С сегодняшнего дня тебе суждено стать царем.</ta>
            <ta e="T742" id="Seg_11178" s="T737">С моим сыном живите как братья."</ta>
            <ta e="T747" id="Seg_11179" s="T742">Трое парней зажили дружно, в богатстве и довольстве.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T4" id="Seg_11180" s="T0">Давным-давно жил один царь.</ta>
            <ta e="T8" id="Seg_11181" s="T4">Он не имел детей, оказывается.</ta>
            <ta e="T12" id="Seg_11182" s="T8">Однажды этот царь призадумался: </ta>
            <ta e="T17" id="Seg_11183" s="T12">"Почему же у нас нет детей?</ta>
            <ta e="T20" id="Seg_11184" s="T17">Спросить бы у старых людей".</ta>
            <ta e="T25" id="Seg_11185" s="T20">Когда так, задумавшись, сидел, вошла жена:</ta>
            <ta e="T28" id="Seg_11186" s="T25">— О чем задумался, милый?</ta>
            <ta e="T32" id="Seg_11187" s="T28">— Завтра мы соберем людей.</ta>
            <ta e="T40" id="Seg_11188" s="T32">До каких пор будем жить, глядя на пустые стены, без детей?</ta>
            <ta e="T44" id="Seg_11189" s="T40">Спросим, почему нет у нас детей?</ta>
            <ta e="T48" id="Seg_11190" s="T44">Кому оставим наше богатство, когда умрем?</ta>
            <ta e="T54" id="Seg_11191" s="T48">Утром встали — много людей собралось.</ta>
            <ta e="T57" id="Seg_11192" s="T54">Тут царь спросил:</ta>
            <ta e="T62" id="Seg_11193" s="T57">— Почему, вот, мы не имеем детей?</ta>
            <ta e="T70" id="Seg_11194" s="T62">Что сделать, чтобы заполучить ребенка, что вы об этом думаете?</ta>
            <ta e="T73" id="Seg_11195" s="T70">Люди зашумели.</ta>
            <ta e="T79" id="Seg_11196" s="T73">В дверь заглянул один кривой дряхлый старик.</ta>
            <ta e="T84" id="Seg_11197" s="T79">Бедного кривого вытолкнули на середину:</ta>
            <ta e="T88" id="Seg_11198" s="T84">— Вот знающий человек.</ta>
            <ta e="T96" id="Seg_11199" s="T88">Старик взглянул снизу вверх, заложил руки за спину и сказал:</ta>
            <ta e="T103" id="Seg_11200" s="T96">— Готовьте золотые сети, золотую лодочку, золотые весла.</ta>
            <ta e="T107" id="Seg_11201" s="T103">Потом пусть царь едет рыбачить.</ta>
            <ta e="T113" id="Seg_11202" s="T107">В сети попадется рыба, пусть ее съест [царица].</ta>
            <ta e="T116" id="Seg_11203" s="T113">Царица засмеялась:</ta>
            <ta e="T120" id="Seg_11204" s="T116">— Наемся ли ею?</ta>
            <ta e="T130" id="Seg_11205" s="T120">Долго не думая, царь назавтра в золотой лодочке с золотым веслом уехал рыбачить.</ta>
            <ta e="T136" id="Seg_11206" s="T130">Одну живую трепещущую рыбку тут приносит.</ta>
            <ta e="T143" id="Seg_11207" s="T136">— Вот моя добыча, — сказал и поварам отдал.</ta>
            <ta e="T145" id="Seg_11208" s="T143">— Сейчас же сварите!</ta>
            <ta e="T157" id="Seg_11209" s="T145">Повар рыбу от чешуи очистил в траве — [ее] съела корова царя.</ta>
            <ta e="T168" id="Seg_11210" s="T157">Царица, съев рыбу, кости оставила на тарелке, повар их выбросил собаке.</ta>
            <ta e="T172" id="Seg_11211" s="T168">Долго ли делается в олонгко?</ta>
            <ta e="T176" id="Seg_11212" s="T172">Все трое родили трех мальчиков.</ta>
            <ta e="T183" id="Seg_11213" s="T176">Сколько-то [времени] прошло, дети подросли и стали играть вместе.</ta>
            <ta e="T188" id="Seg_11214" s="T183">Вот однажды царский сын говорит:</ta>
            <ta e="T191" id="Seg_11215" s="T188">— Друзья, поедем на лодке.</ta>
            <ta e="T197" id="Seg_11216" s="T191">Сколько гребли парни — неизвестно.</ta>
            <ta e="T205" id="Seg_11217" s="T197">Увидели: на том берегу реки-бабушки стоит большущий балаган.</ta>
            <ta e="T208" id="Seg_11218" s="T205">— Что это? — удивились.</ta>
            <ta e="T210" id="Seg_11219" s="T208">Царский сын сказал:</ta>
            <ta e="T213" id="Seg_11220" s="T210">— Пристанем, поиграем в карты.</ta>
            <ta e="T215" id="Seg_11221" s="T213">Сын собаки:</ta>
            <ta e="T217" id="Seg_11222" s="T215">— Э, зачем?</ta>
            <ta e="T221" id="Seg_11223" s="T217">Может, это жилище абаасы.</ta>
            <ta e="T223" id="Seg_11224" s="T221">Сын коровы:</ta>
            <ta e="T225" id="Seg_11225" s="T223">— Пристанем, пристанем.</ta>
            <ta e="T227" id="Seg_11226" s="T225">Немного разомнемся.</ta>
            <ta e="T234" id="Seg_11227" s="T227">Как только вышли на берег, эти два парня сели играть в карты.</ta>
            <ta e="T238" id="Seg_11228" s="T234">Сын собаки уснул.</ta>
            <ta e="T247" id="Seg_11229" s="T238">Сколько играли — неизвестно, эти дети тоже уснули.</ta>
            <ta e="T256" id="Seg_11230" s="T247">В полночь проснулся сын собаки и услышал чьи-то шаги.</ta>
            <ta e="T263" id="Seg_11231" s="T256">Вышел — сам абаасы… идет в сторону балагана, вот.</ta>
            <ta e="T272" id="Seg_11232" s="T263">"О, ну погибли мы, кажется", — подумав, попытался разбудить друзей.</ta>
            <ta e="T276" id="Seg_11233" s="T272">Те парни разве проснутся?</ta>
            <ta e="T283" id="Seg_11234" s="T276">— Иди-иди, я тебя съем, — послышался голос абаасы.</ta>
            <ta e="T291" id="Seg_11235" s="T283">Парень тут выскочил и стал биться с абаасы.</ta>
            <ta e="T300" id="Seg_11236" s="T291">Много ли [времени] прошло — голову абаасы разбило камень-бугор.</ta>
            <ta e="T313" id="Seg_11237" s="T300">После этого из черепа [абаасы] вынул камень с письменами, положил камень в карман.</ta>
            <ta e="T322" id="Seg_11238" s="T313">Утром, будто ничего не произошло, сын собаки разбудил друзей:</ta>
            <ta e="T325" id="Seg_11239" s="T322">— Давайте пойдем поохотимся на гусей.</ta>
            <ta e="T331" id="Seg_11240" s="T325">Парни, потягиваясь, еле встали, стали чай пить.</ta>
            <ta e="T334" id="Seg_11241" s="T331">В олонгко день короткий.</ta>
            <ta e="T339" id="Seg_11242" s="T334">Много ли [времени] прошло — стемнело.</ta>
            <ta e="T349" id="Seg_11243" s="T339">Сын собаки поел и уснул, а друзья его опять сели играть в карты.</ta>
            <ta e="T357" id="Seg_11244" s="T349">Проснулся в полночь — друзья как сидели, так и заснули.</ta>
            <ta e="T370" id="Seg_11245" s="T357">Проснувшись, этот парень стал прислушиваться — где-то далеко послышались чьи-то шаги.</ta>
            <ta e="T377" id="Seg_11246" s="T370">Пытался разбудить друзей, да разве проснутся только что уснувшие люди?</ta>
            <ta e="T389" id="Seg_11247" s="T377">Сын собаки вышел и голову второго абаасы опять разбил о камень-бугор.</ta>
            <ta e="T395" id="Seg_11248" s="T389">Абаасы ногами дернул — и вытянулся.</ta>
            <ta e="T406" id="Seg_11249" s="T395">Сын собаки из черепа вынул камень с письменами и опять положил в карман.</ta>
            <ta e="T417" id="Seg_11250" s="T406">На третью ночь разбил голову третьему абаасы и успел вынуть камень с письменами.</ta>
            <ta e="T422" id="Seg_11251" s="T417">Как только встала заря, разбудил друзей.</ta>
            <ta e="T428" id="Seg_11252" s="T422">— Давайте поедем домой, — сказал этот парень.</ta>
            <ta e="T431" id="Seg_11253" s="T428">Царский сын заупрямился.</ta>
            <ta e="T433" id="Seg_11254" s="T431">— Зачем же!</ta>
            <ta e="T436" id="Seg_11255" s="T433">Еще поживем, весело-то как.</ta>
            <ta e="T442" id="Seg_11256" s="T436">Сын коровы, помолчав, сказал:</ta>
            <ta e="T446" id="Seg_11257" s="T442">— Родные заждались, поедем все же.</ta>
            <ta e="T448" id="Seg_11258" s="T446">Парни тронулись в путь.</ta>
            <ta e="T452" id="Seg_11259" s="T448">Сын собаки греб.</ta>
            <ta e="T458" id="Seg_11260" s="T452">Много ли [времени] прошло, ощупав ножны, сказал:</ta>
            <ta e="T464" id="Seg_11261" s="T458">— Друзья, забыл я нож, пойду поищу.</ta>
            <ta e="T472" id="Seg_11262" s="T464">Сын собаки подошел к балагану, тут послышался плач женщин.</ta>
            <ta e="T482" id="Seg_11263" s="T472">— Я стану вкусной пищей и спущусь на большой тарелке в их лодку.</ta>
            <ta e="T490" id="Seg_11264" s="T482">Когда отпробуют меня, все передохнут, — сказал голос старой Джигэ-бабы.</ta>
            <ta e="T506" id="Seg_11265" s="T490">— А я-то, став невиданно красивым шелковым платком, расстелюсь на коленях царского сына и заставлю всех удавиться.</ta>
            <ta e="T518" id="Seg_11266" s="T506">— А я-то стану жидким клеем, приклею намертво их лодку и, всех утянув, утоплю.</ta>
            <ta e="T520" id="Seg_11267" s="T518">— Ну ладно.</ta>
            <ta e="T526" id="Seg_11268" s="T520">Хорошо задумали, оказывается, — сказала первая женщина.</ta>
            <ta e="T540" id="Seg_11269" s="T526">Как услышал это сын собаки, сбежал вниз с берега, сел в лодочку и стал грести.</ta>
            <ta e="T545" id="Seg_11270" s="T540">Друзей нагнал на середине реки-бабушки.</ta>
            <ta e="T549" id="Seg_11271" s="T545">Парни потихоньку гребли.</ta>
            <ta e="T557" id="Seg_11272" s="T549">Много ли [времени] прошло, в лодку спустилась тарелка с едой.</ta>
            <ta e="T573" id="Seg_11273" s="T557">Царский сын обрадовался, растопырил пальцы в сторону тарелки — сын собаки тарелку столкнул в воду.</ta>
            <ta e="T586" id="Seg_11274" s="T573">Когда царский сын от досады с вытянутым носом сидел, на коленях его расстелился красивый шелковый платок.</ta>
            <ta e="T607" id="Seg_11275" s="T586">— О, боже, маме сделаю подарок, — сказал, и только хотел было положить в карман, как сын [собаки] выхватил и, ножом искромсав, бросил [платок] в воду.</ta>
            <ta e="T613" id="Seg_11276" s="T607">От ярости царский сын даже слова не мог вымолвить.</ta>
            <ta e="T624" id="Seg_11277" s="T613">Много ли еще [времени] прошло — лодка прилипла к воде, вода в реке-бабушке почернела.</ta>
            <ta e="T630" id="Seg_11278" s="T624">Эти парни совсем грести не могут.</ta>
            <ta e="T631" id="Seg_11279" s="T630">Испугались.</ta>
            <ta e="T633" id="Seg_11280" s="T631">Сын собаки велит друзьям:</ta>
            <ta e="T645" id="Seg_11281" s="T633">— Воду рубите топором, протыкайте ножом, а я стану грести.</ta>
            <ta e="T651" id="Seg_11282" s="T645">Взглянули — вода и вправду стала светлеть, алеть.</ta>
            <ta e="T654" id="Seg_11283" s="T651">Скоро лодка их освободилась.</ta>
            <ta e="T658" id="Seg_11284" s="T654">Парни гребут потихоньку.</ta>
            <ta e="T667" id="Seg_11285" s="T658">Много ли [времени] прошло — на высоком берегу реки-бабушки показался их город.</ta>
            <ta e="T674" id="Seg_11286" s="T667">Пристали к берегу и разошлись на три стороны.</ta>
            <ta e="T693" id="Seg_11287" s="T674">Царский сын пошел к белому дому на берегу, сын собаки нырнул в низкий дом-конуру матери, сын коровы мать во дворе встретил.</ta>
            <ta e="T697" id="Seg_11288" s="T693">Утром царь позвал парней:</ta>
            <ta e="T705" id="Seg_11289" s="T697">— Ну, дети мои, рассказывайте, что так долго ездили?</ta>
            <ta e="T721" id="Seg_11290" s="T705">— В черепах трех абаасы нашел три камня с письменами, — сказал сын собаки и вручил царю три камня.</ta>
            <ta e="T731" id="Seg_11291" s="T721">Царь сына собаки обнял за шею и, всплакнув, сказал:</ta>
            <ta e="T737" id="Seg_11292" s="T731">— С сегодняшнего дня тебе суждено стать царем.</ta>
            <ta e="T742" id="Seg_11293" s="T737">С моим сыном живите как братья.</ta>
            <ta e="T747" id="Seg_11294" s="T742">Трое парней зажили дружно, в богатстве и довольстве.</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T559" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
            <conversion-tli id="T571" />
            <conversion-tli id="T572" />
            <conversion-tli id="T573" />
            <conversion-tli id="T574" />
            <conversion-tli id="T575" />
            <conversion-tli id="T576" />
            <conversion-tli id="T577" />
            <conversion-tli id="T578" />
            <conversion-tli id="T579" />
            <conversion-tli id="T580" />
            <conversion-tli id="T581" />
            <conversion-tli id="T582" />
            <conversion-tli id="T583" />
            <conversion-tli id="T584" />
            <conversion-tli id="T585" />
            <conversion-tli id="T586" />
            <conversion-tli id="T587" />
            <conversion-tli id="T588" />
            <conversion-tli id="T589" />
            <conversion-tli id="T590" />
            <conversion-tli id="T591" />
            <conversion-tli id="T592" />
            <conversion-tli id="T593" />
            <conversion-tli id="T594" />
            <conversion-tli id="T595" />
            <conversion-tli id="T596" />
            <conversion-tli id="T597" />
            <conversion-tli id="T598" />
            <conversion-tli id="T599" />
            <conversion-tli id="T600" />
            <conversion-tli id="T601" />
            <conversion-tli id="T602" />
            <conversion-tli id="T603" />
            <conversion-tli id="T604" />
            <conversion-tli id="T605" />
            <conversion-tli id="T606" />
            <conversion-tli id="T607" />
            <conversion-tli id="T608" />
            <conversion-tli id="T609" />
            <conversion-tli id="T610" />
            <conversion-tli id="T611" />
            <conversion-tli id="T612" />
            <conversion-tli id="T613" />
            <conversion-tli id="T614" />
            <conversion-tli id="T615" />
            <conversion-tli id="T616" />
            <conversion-tli id="T617" />
            <conversion-tli id="T618" />
            <conversion-tli id="T619" />
            <conversion-tli id="T620" />
            <conversion-tli id="T621" />
            <conversion-tli id="T622" />
            <conversion-tli id="T623" />
            <conversion-tli id="T624" />
            <conversion-tli id="T625" />
            <conversion-tli id="T626" />
            <conversion-tli id="T627" />
            <conversion-tli id="T628" />
            <conversion-tli id="T629" />
            <conversion-tli id="T630" />
            <conversion-tli id="T631" />
            <conversion-tli id="T632" />
            <conversion-tli id="T633" />
            <conversion-tli id="T634" />
            <conversion-tli id="T635" />
            <conversion-tli id="T636" />
            <conversion-tli id="T637" />
            <conversion-tli id="T638" />
            <conversion-tli id="T639" />
            <conversion-tli id="T640" />
            <conversion-tli id="T641" />
            <conversion-tli id="T642" />
            <conversion-tli id="T643" />
            <conversion-tli id="T644" />
            <conversion-tli id="T645" />
            <conversion-tli id="T646" />
            <conversion-tli id="T647" />
            <conversion-tli id="T648" />
            <conversion-tli id="T649" />
            <conversion-tli id="T650" />
            <conversion-tli id="T651" />
            <conversion-tli id="T652" />
            <conversion-tli id="T653" />
            <conversion-tli id="T654" />
            <conversion-tli id="T655" />
            <conversion-tli id="T656" />
            <conversion-tli id="T657" />
            <conversion-tli id="T658" />
            <conversion-tli id="T659" />
            <conversion-tli id="T660" />
            <conversion-tli id="T661" />
            <conversion-tli id="T662" />
            <conversion-tli id="T663" />
            <conversion-tli id="T664" />
            <conversion-tli id="T665" />
            <conversion-tli id="T666" />
            <conversion-tli id="T667" />
            <conversion-tli id="T668" />
            <conversion-tli id="T669" />
            <conversion-tli id="T670" />
            <conversion-tli id="T671" />
            <conversion-tli id="T672" />
            <conversion-tli id="T673" />
            <conversion-tli id="T674" />
            <conversion-tli id="T675" />
            <conversion-tli id="T676" />
            <conversion-tli id="T677" />
            <conversion-tli id="T678" />
            <conversion-tli id="T679" />
            <conversion-tli id="T680" />
            <conversion-tli id="T681" />
            <conversion-tli id="T682" />
            <conversion-tli id="T683" />
            <conversion-tli id="T684" />
            <conversion-tli id="T685" />
            <conversion-tli id="T686" />
            <conversion-tli id="T687" />
            <conversion-tli id="T688" />
            <conversion-tli id="T689" />
            <conversion-tli id="T690" />
            <conversion-tli id="T691" />
            <conversion-tli id="T692" />
            <conversion-tli id="T693" />
            <conversion-tli id="T694" />
            <conversion-tli id="T695" />
            <conversion-tli id="T696" />
            <conversion-tli id="T697" />
            <conversion-tli id="T698" />
            <conversion-tli id="T699" />
            <conversion-tli id="T700" />
            <conversion-tli id="T701" />
            <conversion-tli id="T702" />
            <conversion-tli id="T703" />
            <conversion-tli id="T704" />
            <conversion-tli id="T705" />
            <conversion-tli id="T706" />
            <conversion-tli id="T707" />
            <conversion-tli id="T708" />
            <conversion-tli id="T709" />
            <conversion-tli id="T710" />
            <conversion-tli id="T711" />
            <conversion-tli id="T712" />
            <conversion-tli id="T713" />
            <conversion-tli id="T714" />
            <conversion-tli id="T715" />
            <conversion-tli id="T716" />
            <conversion-tli id="T717" />
            <conversion-tli id="T718" />
            <conversion-tli id="T719" />
            <conversion-tli id="T720" />
            <conversion-tli id="T721" />
            <conversion-tli id="T722" />
            <conversion-tli id="T723" />
            <conversion-tli id="T724" />
            <conversion-tli id="T725" />
            <conversion-tli id="T726" />
            <conversion-tli id="T727" />
            <conversion-tli id="T728" />
            <conversion-tli id="T729" />
            <conversion-tli id="T730" />
            <conversion-tli id="T731" />
            <conversion-tli id="T732" />
            <conversion-tli id="T733" />
            <conversion-tli id="T734" />
            <conversion-tli id="T735" />
            <conversion-tli id="T736" />
            <conversion-tli id="T737" />
            <conversion-tli id="T738" />
            <conversion-tli id="T739" />
            <conversion-tli id="T740" />
            <conversion-tli id="T741" />
            <conversion-tli id="T742" />
            <conversion-tli id="T743" />
            <conversion-tli id="T744" />
            <conversion-tli id="T745" />
            <conversion-tli id="T746" />
            <conversion-tli id="T747" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg"
                          name="gg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top"
                          name="Top"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc"
                          name="Foc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
