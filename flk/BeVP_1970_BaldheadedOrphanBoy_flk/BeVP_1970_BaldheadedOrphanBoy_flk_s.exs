<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID80FF7381-A037-FCAE-2212-CB2BD13CF2BA">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>BeVP_1970_BaldheadedOrphanBoy_flk</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\flk\BeVP_1970_BaldheadedOrphanBoy_flk\BeVP_1970_BaldheadedOrphanBoy_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">681</ud-information>
            <ud-information attribute-name="# HIAT:w">496</ud-information>
            <ud-information attribute-name="# e">496</ud-information>
            <ud-information attribute-name="# HIAT:u">99</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="BeVP">
            <abbreviation>BeVP</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="4.0" type="appl" />
         <tli id="T1" time="4.5" type="appl" />
         <tli id="T2" time="5.0" type="appl" />
         <tli id="T3" time="5.5" type="appl" />
         <tli id="T4" time="6.0" type="appl" />
         <tli id="T5" time="6.5" type="appl" />
         <tli id="T6" time="7.0" type="appl" />
         <tli id="T7" time="7.5" type="appl" />
         <tli id="T8" time="8.0" type="appl" />
         <tli id="T9" time="8.5" type="appl" />
         <tli id="T10" time="9.0" type="appl" />
         <tli id="T11" time="9.5" type="appl" />
         <tli id="T12" time="10.0" type="appl" />
         <tli id="T13" time="10.5" type="appl" />
         <tli id="T14" time="11.0" type="appl" />
         <tli id="T15" time="11.5" type="appl" />
         <tli id="T16" time="12.0" type="appl" />
         <tli id="T17" time="12.5" type="appl" />
         <tli id="T18" time="13.0" type="appl" />
         <tli id="T19" time="13.5" type="appl" />
         <tli id="T20" time="14.0" type="appl" />
         <tli id="T21" time="14.5" type="appl" />
         <tli id="T22" time="15.0" type="appl" />
         <tli id="T23" time="15.5" type="appl" />
         <tli id="T24" time="16.0" type="appl" />
         <tli id="T25" time="16.5" type="appl" />
         <tli id="T26" time="17.0" type="appl" />
         <tli id="T27" time="17.5" type="appl" />
         <tli id="T28" time="18.0" type="appl" />
         <tli id="T29" time="18.5" type="appl" />
         <tli id="T30" time="19.0" type="appl" />
         <tli id="T31" time="19.5" type="appl" />
         <tli id="T32" time="20.0" type="appl" />
         <tli id="T33" time="20.5" type="appl" />
         <tli id="T34" time="21.0" type="appl" />
         <tli id="T35" time="21.5" type="appl" />
         <tli id="T36" time="22.0" type="appl" />
         <tli id="T37" time="22.5" type="appl" />
         <tli id="T38" time="23.0" type="appl" />
         <tli id="T39" time="23.5" type="appl" />
         <tli id="T40" time="24.0" type="appl" />
         <tli id="T41" time="24.5" type="appl" />
         <tli id="T42" time="25.0" type="appl" />
         <tli id="T43" time="25.5" type="appl" />
         <tli id="T44" time="26.0" type="appl" />
         <tli id="T45" time="26.5" type="appl" />
         <tli id="T46" time="27.0" type="appl" />
         <tli id="T47" time="27.5" type="appl" />
         <tli id="T48" time="28.0" type="appl" />
         <tli id="T49" time="28.5" type="appl" />
         <tli id="T50" time="29.0" type="appl" />
         <tli id="T51" time="29.5" type="appl" />
         <tli id="T52" time="30.0" type="appl" />
         <tli id="T53" time="30.5" type="appl" />
         <tli id="T54" time="31.0" type="appl" />
         <tli id="T55" time="31.5" type="appl" />
         <tli id="T56" time="32.0" type="appl" />
         <tli id="T57" time="32.5" type="appl" />
         <tli id="T58" time="33.0" type="appl" />
         <tli id="T59" time="33.5" type="appl" />
         <tli id="T60" time="34.0" type="appl" />
         <tli id="T61" time="34.5" type="appl" />
         <tli id="T62" time="35.0" type="appl" />
         <tli id="T63" time="35.5" type="appl" />
         <tli id="T64" time="36.0" type="appl" />
         <tli id="T65" time="36.5" type="appl" />
         <tli id="T66" time="37.0" type="appl" />
         <tli id="T67" time="37.5" type="appl" />
         <tli id="T68" time="38.0" type="appl" />
         <tli id="T69" time="38.5" type="appl" />
         <tli id="T70" time="39.0" type="appl" />
         <tli id="T71" time="39.5" type="appl" />
         <tli id="T72" time="40.0" type="appl" />
         <tli id="T73" time="40.5" type="appl" />
         <tli id="T74" time="41.0" type="appl" />
         <tli id="T75" time="41.5" type="appl" />
         <tli id="T76" time="42.0" type="appl" />
         <tli id="T77" time="42.5" type="appl" />
         <tli id="T78" time="43.0" type="appl" />
         <tli id="T79" time="43.5" type="appl" />
         <tli id="T80" time="44.0" type="appl" />
         <tli id="T81" time="44.5" type="appl" />
         <tli id="T82" time="45.0" type="appl" />
         <tli id="T83" time="45.5" type="appl" />
         <tli id="T84" time="46.0" type="appl" />
         <tli id="T85" time="46.5" type="appl" />
         <tli id="T86" time="47.0" type="appl" />
         <tli id="T87" time="47.5" type="appl" />
         <tli id="T88" time="48.0" type="appl" />
         <tli id="T89" time="48.5" type="appl" />
         <tli id="T90" time="49.0" type="appl" />
         <tli id="T91" time="49.5" type="appl" />
         <tli id="T92" time="50.0" type="appl" />
         <tli id="T93" time="50.5" type="appl" />
         <tli id="T94" time="51.0" type="appl" />
         <tli id="T95" time="51.5" type="appl" />
         <tli id="T96" time="52.0" type="appl" />
         <tli id="T97" time="52.5" type="appl" />
         <tli id="T98" time="53.0" type="appl" />
         <tli id="T99" time="53.5" type="appl" />
         <tli id="T100" time="54.0" type="appl" />
         <tli id="T101" time="54.5" type="appl" />
         <tli id="T102" time="55.0" type="appl" />
         <tli id="T103" time="55.5" type="appl" />
         <tli id="T104" time="56.0" type="appl" />
         <tli id="T105" time="56.5" type="appl" />
         <tli id="T106" time="57.0" type="appl" />
         <tli id="T107" time="57.5" type="appl" />
         <tli id="T108" time="58.0" type="appl" />
         <tli id="T109" time="58.5" type="appl" />
         <tli id="T110" time="59.0" type="appl" />
         <tli id="T111" time="59.5" type="appl" />
         <tli id="T112" time="60.0" type="appl" />
         <tli id="T113" time="60.5" type="appl" />
         <tli id="T114" time="61.0" type="appl" />
         <tli id="T115" time="61.5" type="appl" />
         <tli id="T116" time="62.0" type="appl" />
         <tli id="T117" time="62.5" type="appl" />
         <tli id="T118" time="63.0" type="appl" />
         <tli id="T119" time="63.5" type="appl" />
         <tli id="T120" time="64.0" type="appl" />
         <tli id="T121" time="64.5" type="appl" />
         <tli id="T122" time="65.0" type="appl" />
         <tli id="T123" time="65.5" type="appl" />
         <tli id="T124" time="66.0" type="appl" />
         <tli id="T125" time="66.5" type="appl" />
         <tli id="T126" time="67.0" type="appl" />
         <tli id="T127" time="67.5" type="appl" />
         <tli id="T128" time="68.0" type="appl" />
         <tli id="T129" time="68.5" type="appl" />
         <tli id="T130" time="69.0" type="appl" />
         <tli id="T131" time="69.5" type="appl" />
         <tli id="T132" time="70.0" type="appl" />
         <tli id="T133" time="70.5" type="appl" />
         <tli id="T134" time="71.0" type="appl" />
         <tli id="T135" time="71.5" type="appl" />
         <tli id="T136" time="72.0" type="appl" />
         <tli id="T137" time="72.5" type="appl" />
         <tli id="T138" time="73.0" type="appl" />
         <tli id="T139" time="73.5" type="appl" />
         <tli id="T140" time="74.0" type="appl" />
         <tli id="T141" time="74.5" type="appl" />
         <tli id="T142" time="75.0" type="appl" />
         <tli id="T143" time="75.5" type="appl" />
         <tli id="T144" time="76.0" type="appl" />
         <tli id="T145" time="76.5" type="appl" />
         <tli id="T146" time="77.0" type="appl" />
         <tli id="T147" time="77.5" type="appl" />
         <tli id="T148" time="78.0" type="appl" />
         <tli id="T149" time="78.5" type="appl" />
         <tli id="T150" time="79.0" type="appl" />
         <tli id="T151" time="79.5" type="appl" />
         <tli id="T152" time="80.0" type="appl" />
         <tli id="T153" time="80.5" type="appl" />
         <tli id="T154" time="81.0" type="appl" />
         <tli id="T155" time="81.5" type="appl" />
         <tli id="T156" time="82.0" type="appl" />
         <tli id="T157" time="82.5" type="appl" />
         <tli id="T158" time="83.0" type="appl" />
         <tli id="T159" time="83.5" type="appl" />
         <tli id="T160" time="84.0" type="appl" />
         <tli id="T161" time="84.5" type="appl" />
         <tli id="T162" time="85.0" type="appl" />
         <tli id="T163" time="85.5" type="appl" />
         <tli id="T164" time="86.0" type="appl" />
         <tli id="T165" time="86.5" type="appl" />
         <tli id="T166" time="87.0" type="appl" />
         <tli id="T167" time="87.5" type="appl" />
         <tli id="T168" time="88.0" type="appl" />
         <tli id="T169" time="88.5" type="appl" />
         <tli id="T170" time="89.0" type="appl" />
         <tli id="T171" time="89.5" type="appl" />
         <tli id="T172" time="90.0" type="appl" />
         <tli id="T173" time="90.5" type="appl" />
         <tli id="T174" time="91.0" type="appl" />
         <tli id="T175" time="91.5" type="appl" />
         <tli id="T176" time="92.0" type="appl" />
         <tli id="T177" time="92.5" type="appl" />
         <tli id="T178" time="93.0" type="appl" />
         <tli id="T179" time="93.5" type="appl" />
         <tli id="T180" time="94.0" type="appl" />
         <tli id="T181" time="94.5" type="appl" />
         <tli id="T182" time="95.0" type="appl" />
         <tli id="T183" time="95.5" type="appl" />
         <tli id="T184" time="96.0" type="appl" />
         <tli id="T185" time="96.5" type="appl" />
         <tli id="T186" time="97.0" type="appl" />
         <tli id="T187" time="97.5" type="appl" />
         <tli id="T188" time="98.0" type="appl" />
         <tli id="T189" time="98.5" type="appl" />
         <tli id="T190" time="99.0" type="appl" />
         <tli id="T191" time="99.5" type="appl" />
         <tli id="T192" time="100.0" type="appl" />
         <tli id="T193" time="100.5" type="appl" />
         <tli id="T194" time="101.0" type="appl" />
         <tli id="T195" time="101.5" type="appl" />
         <tli id="T196" time="102.0" type="appl" />
         <tli id="T197" time="102.5" type="appl" />
         <tli id="T198" time="103.0" type="appl" />
         <tli id="T199" time="103.5" type="appl" />
         <tli id="T200" time="104.0" type="appl" />
         <tli id="T201" time="104.5" type="appl" />
         <tli id="T202" time="105.0" type="appl" />
         <tli id="T203" time="105.5" type="appl" />
         <tli id="T204" time="106.0" type="appl" />
         <tli id="T205" time="106.5" type="appl" />
         <tli id="T206" time="107.0" type="appl" />
         <tli id="T207" time="107.5" type="appl" />
         <tli id="T208" time="108.0" type="appl" />
         <tli id="T209" time="108.5" type="appl" />
         <tli id="T210" time="109.0" type="appl" />
         <tli id="T211" time="109.5" type="appl" />
         <tli id="T212" time="110.0" type="appl" />
         <tli id="T213" time="110.5" type="appl" />
         <tli id="T214" time="111.0" type="appl" />
         <tli id="T215" time="111.5" type="appl" />
         <tli id="T216" time="112.0" type="appl" />
         <tli id="T217" time="112.5" type="appl" />
         <tli id="T218" time="113.0" type="appl" />
         <tli id="T219" time="113.5" type="appl" />
         <tli id="T220" time="114.0" type="appl" />
         <tli id="T221" time="114.5" type="appl" />
         <tli id="T222" time="115.0" type="appl" />
         <tli id="T223" time="115.5" type="appl" />
         <tli id="T224" time="116.0" type="appl" />
         <tli id="T225" time="116.5" type="appl" />
         <tli id="T226" time="117.0" type="appl" />
         <tli id="T227" time="117.5" type="appl" />
         <tli id="T228" time="118.0" type="appl" />
         <tli id="T229" time="118.5" type="appl" />
         <tli id="T230" time="119.0" type="appl" />
         <tli id="T231" time="119.5" type="appl" />
         <tli id="T232" time="120.0" type="appl" />
         <tli id="T233" time="120.5" type="appl" />
         <tli id="T234" time="121.0" type="appl" />
         <tli id="T235" time="121.5" type="appl" />
         <tli id="T236" time="122.0" type="appl" />
         <tli id="T237" time="122.5" type="appl" />
         <tli id="T238" time="123.0" type="appl" />
         <tli id="T239" time="123.5" type="appl" />
         <tli id="T240" time="124.0" type="appl" />
         <tli id="T241" time="124.5" type="appl" />
         <tli id="T242" time="125.0" type="appl" />
         <tli id="T243" time="125.5" type="appl" />
         <tli id="T244" time="126.0" type="appl" />
         <tli id="T245" time="126.5" type="appl" />
         <tli id="T246" time="127.0" type="appl" />
         <tli id="T247" time="127.5" type="appl" />
         <tli id="T248" time="128.0" type="appl" />
         <tli id="T249" time="128.5" type="appl" />
         <tli id="T250" time="129.0" type="appl" />
         <tli id="T251" time="129.5" type="appl" />
         <tli id="T252" time="130.0" type="appl" />
         <tli id="T253" time="130.5" type="appl" />
         <tli id="T254" time="131.0" type="appl" />
         <tli id="T255" time="131.5" type="appl" />
         <tli id="T256" time="132.0" type="appl" />
         <tli id="T257" time="132.5" type="appl" />
         <tli id="T258" time="133.0" type="appl" />
         <tli id="T259" time="133.5" type="appl" />
         <tli id="T260" time="134.0" type="appl" />
         <tli id="T261" time="134.5" type="appl" />
         <tli id="T262" time="135.0" type="appl" />
         <tli id="T263" time="135.5" type="appl" />
         <tli id="T264" time="136.0" type="appl" />
         <tli id="T265" time="136.5" type="appl" />
         <tli id="T266" time="137.0" type="appl" />
         <tli id="T267" time="137.5" type="appl" />
         <tli id="T268" time="138.0" type="appl" />
         <tli id="T269" time="138.5" type="appl" />
         <tli id="T270" time="139.0" type="appl" />
         <tli id="T271" time="139.5" type="appl" />
         <tli id="T272" time="140.0" type="appl" />
         <tli id="T273" time="140.5" type="appl" />
         <tli id="T274" time="141.0" type="appl" />
         <tli id="T275" time="141.5" type="appl" />
         <tli id="T276" time="142.0" type="appl" />
         <tli id="T277" time="142.5" type="appl" />
         <tli id="T278" time="143.0" type="appl" />
         <tli id="T279" time="143.5" type="appl" />
         <tli id="T280" time="144.0" type="appl" />
         <tli id="T281" time="144.5" type="appl" />
         <tli id="T282" time="145.0" type="appl" />
         <tli id="T283" time="145.5" type="appl" />
         <tli id="T284" time="146.0" type="appl" />
         <tli id="T285" time="146.5" type="appl" />
         <tli id="T286" time="147.0" type="appl" />
         <tli id="T287" time="147.5" type="appl" />
         <tli id="T288" time="148.0" type="appl" />
         <tli id="T289" time="148.5" type="appl" />
         <tli id="T290" time="149.0" type="appl" />
         <tli id="T291" time="149.5" type="appl" />
         <tli id="T292" time="150.0" type="appl" />
         <tli id="T293" time="150.5" type="appl" />
         <tli id="T294" time="151.0" type="appl" />
         <tli id="T295" time="151.5" type="appl" />
         <tli id="T296" time="152.0" type="appl" />
         <tli id="T297" time="152.5" type="appl" />
         <tli id="T298" time="153.0" type="appl" />
         <tli id="T299" time="153.5" type="appl" />
         <tli id="T300" time="154.0" type="appl" />
         <tli id="T301" time="154.5" type="appl" />
         <tli id="T302" time="155.0" type="appl" />
         <tli id="T303" time="155.5" type="appl" />
         <tli id="T304" time="156.0" type="appl" />
         <tli id="T305" time="156.5" type="appl" />
         <tli id="T306" time="157.0" type="appl" />
         <tli id="T307" time="157.5" type="appl" />
         <tli id="T308" time="158.0" type="appl" />
         <tli id="T309" time="158.5" type="appl" />
         <tli id="T310" time="159.0" type="appl" />
         <tli id="T311" time="159.5" type="appl" />
         <tli id="T312" time="160.0" type="appl" />
         <tli id="T313" time="160.5" type="appl" />
         <tli id="T314" time="161.0" type="appl" />
         <tli id="T315" time="161.5" type="appl" />
         <tli id="T316" time="162.0" type="appl" />
         <tli id="T317" time="162.5" type="appl" />
         <tli id="T318" time="163.0" type="appl" />
         <tli id="T319" time="163.5" type="appl" />
         <tli id="T320" time="164.0" type="appl" />
         <tli id="T321" time="164.5" type="appl" />
         <tli id="T322" time="165.0" type="appl" />
         <tli id="T323" time="165.5" type="appl" />
         <tli id="T324" time="166.0" type="appl" />
         <tli id="T325" time="166.5" type="appl" />
         <tli id="T326" time="167.0" type="appl" />
         <tli id="T327" time="167.5" type="appl" />
         <tli id="T328" time="168.0" type="appl" />
         <tli id="T329" time="168.5" type="appl" />
         <tli id="T330" time="169.0" type="appl" />
         <tli id="T331" time="169.5" type="appl" />
         <tli id="T332" time="170.0" type="appl" />
         <tli id="T333" time="170.5" type="appl" />
         <tli id="T334" time="171.0" type="appl" />
         <tli id="T335" time="171.5" type="appl" />
         <tli id="T336" time="172.0" type="appl" />
         <tli id="T337" time="172.5" type="appl" />
         <tli id="T338" time="173.0" type="appl" />
         <tli id="T339" time="173.5" type="appl" />
         <tli id="T340" time="174.0" type="appl" />
         <tli id="T341" time="174.5" type="appl" />
         <tli id="T342" time="175.0" type="appl" />
         <tli id="T343" time="175.5" type="appl" />
         <tli id="T344" time="176.0" type="appl" />
         <tli id="T345" time="176.5" type="appl" />
         <tli id="T346" time="177.0" type="appl" />
         <tli id="T347" time="177.5" type="appl" />
         <tli id="T348" time="178.0" type="appl" />
         <tli id="T349" time="178.5" type="appl" />
         <tli id="T350" time="179.0" type="appl" />
         <tli id="T351" time="179.5" type="appl" />
         <tli id="T352" time="180.0" type="appl" />
         <tli id="T353" time="180.5" type="appl" />
         <tli id="T354" time="181.0" type="appl" />
         <tli id="T355" time="181.5" type="appl" />
         <tli id="T356" time="182.0" type="appl" />
         <tli id="T357" time="182.5" type="appl" />
         <tli id="T358" time="183.0" type="appl" />
         <tli id="T359" time="183.5" type="appl" />
         <tli id="T360" time="184.0" type="appl" />
         <tli id="T361" time="184.5" type="appl" />
         <tli id="T362" time="185.0" type="appl" />
         <tli id="T363" time="185.5" type="appl" />
         <tli id="T364" time="186.0" type="appl" />
         <tli id="T365" time="186.5" type="appl" />
         <tli id="T366" time="187.0" type="appl" />
         <tli id="T367" time="187.5" type="appl" />
         <tli id="T368" time="188.0" type="appl" />
         <tli id="T369" time="188.5" type="appl" />
         <tli id="T370" time="189.0" type="appl" />
         <tli id="T371" time="189.5" type="appl" />
         <tli id="T372" time="190.0" type="appl" />
         <tli id="T373" time="190.5" type="appl" />
         <tli id="T374" time="191.0" type="appl" />
         <tli id="T375" time="191.5" type="appl" />
         <tli id="T376" time="192.0" type="appl" />
         <tli id="T377" time="192.5" type="appl" />
         <tli id="T378" time="193.0" type="appl" />
         <tli id="T379" time="193.5" type="appl" />
         <tli id="T380" time="194.0" type="appl" />
         <tli id="T381" time="194.5" type="appl" />
         <tli id="T382" time="195.0" type="appl" />
         <tli id="T383" time="195.5" type="appl" />
         <tli id="T384" time="196.0" type="appl" />
         <tli id="T385" time="196.5" type="appl" />
         <tli id="T386" time="197.0" type="appl" />
         <tli id="T387" time="197.5" type="appl" />
         <tli id="T388" time="198.0" type="appl" />
         <tli id="T389" time="198.5" type="appl" />
         <tli id="T390" time="199.0" type="appl" />
         <tli id="T391" time="199.5" type="appl" />
         <tli id="T392" time="200.0" type="appl" />
         <tli id="T393" time="200.5" type="appl" />
         <tli id="T394" time="201.0" type="appl" />
         <tli id="T395" time="201.5" type="appl" />
         <tli id="T396" time="202.0" type="appl" />
         <tli id="T397" time="202.5" type="appl" />
         <tli id="T398" time="203.0" type="appl" />
         <tli id="T399" time="203.5" type="appl" />
         <tli id="T400" time="204.0" type="appl" />
         <tli id="T401" time="204.5" type="appl" />
         <tli id="T402" time="205.0" type="appl" />
         <tli id="T403" time="205.5" type="appl" />
         <tli id="T404" time="206.0" type="appl" />
         <tli id="T405" time="206.5" type="appl" />
         <tli id="T406" time="207.0" type="appl" />
         <tli id="T407" time="207.5" type="appl" />
         <tli id="T408" time="208.0" type="appl" />
         <tli id="T409" time="208.5" type="appl" />
         <tli id="T410" time="209.0" type="appl" />
         <tli id="T411" time="209.5" type="appl" />
         <tli id="T412" time="210.0" type="appl" />
         <tli id="T413" time="210.5" type="appl" />
         <tli id="T414" time="211.0" type="appl" />
         <tli id="T415" time="211.5" type="appl" />
         <tli id="T416" time="212.0" type="appl" />
         <tli id="T417" time="212.5" type="appl" />
         <tli id="T418" time="213.0" type="appl" />
         <tli id="T419" time="213.5" type="appl" />
         <tli id="T420" time="214.0" type="appl" />
         <tli id="T421" time="214.5" type="appl" />
         <tli id="T422" time="215.0" type="appl" />
         <tli id="T423" time="215.5" type="appl" />
         <tli id="T424" time="216.0" type="appl" />
         <tli id="T425" time="216.5" type="appl" />
         <tli id="T426" time="217.0" type="appl" />
         <tli id="T427" time="217.5" type="appl" />
         <tli id="T428" time="218.0" type="appl" />
         <tli id="T429" time="218.5" type="appl" />
         <tli id="T430" time="219.0" type="appl" />
         <tli id="T431" time="219.5" type="appl" />
         <tli id="T432" time="220.0" type="appl" />
         <tli id="T433" time="220.5" type="appl" />
         <tli id="T434" time="221.0" type="appl" />
         <tli id="T435" time="221.5" type="appl" />
         <tli id="T436" time="222.0" type="appl" />
         <tli id="T437" time="222.5" type="appl" />
         <tli id="T438" time="223.0" type="appl" />
         <tli id="T439" time="223.5" type="appl" />
         <tli id="T440" time="224.0" type="appl" />
         <tli id="T441" time="224.5" type="appl" />
         <tli id="T442" time="225.0" type="appl" />
         <tli id="T443" time="225.5" type="appl" />
         <tli id="T444" time="226.0" type="appl" />
         <tli id="T445" time="226.5" type="appl" />
         <tli id="T446" time="227.0" type="appl" />
         <tli id="T447" time="227.5" type="appl" />
         <tli id="T448" time="228.0" type="appl" />
         <tli id="T449" time="228.5" type="appl" />
         <tli id="T450" time="229.0" type="appl" />
         <tli id="T451" time="229.5" type="appl" />
         <tli id="T452" time="230.0" type="appl" />
         <tli id="T453" time="230.5" type="appl" />
         <tli id="T454" time="231.0" type="appl" />
         <tli id="T455" time="231.5" type="appl" />
         <tli id="T456" time="232.0" type="appl" />
         <tli id="T457" time="232.5" type="appl" />
         <tli id="T458" time="233.0" type="appl" />
         <tli id="T459" time="233.5" type="appl" />
         <tli id="T460" time="234.0" type="appl" />
         <tli id="T461" time="234.5" type="appl" />
         <tli id="T462" time="235.0" type="appl" />
         <tli id="T463" time="235.5" type="appl" />
         <tli id="T464" time="236.0" type="appl" />
         <tli id="T465" time="236.5" type="appl" />
         <tli id="T466" time="237.0" type="appl" />
         <tli id="T467" time="237.5" type="appl" />
         <tli id="T468" time="238.0" type="appl" />
         <tli id="T469" time="238.5" type="appl" />
         <tli id="T470" time="239.0" type="appl" />
         <tli id="T471" time="239.5" type="appl" />
         <tli id="T472" time="240.0" type="appl" />
         <tli id="T473" time="240.5" type="appl" />
         <tli id="T474" time="241.0" type="appl" />
         <tli id="T475" time="241.5" type="appl" />
         <tli id="T476" time="242.0" type="appl" />
         <tli id="T477" time="242.5" type="appl" />
         <tli id="T478" time="243.0" type="appl" />
         <tli id="T479" time="243.5" type="appl" />
         <tli id="T480" time="244.0" type="appl" />
         <tli id="T481" time="244.5" type="appl" />
         <tli id="T482" time="245.0" type="appl" />
         <tli id="T483" time="245.5" type="appl" />
         <tli id="T484" time="246.0" type="appl" />
         <tli id="T485" time="246.5" type="appl" />
         <tli id="T486" time="247.0" type="appl" />
         <tli id="T487" time="247.5" type="appl" />
         <tli id="T488" time="248.0" type="appl" />
         <tli id="T489" time="248.5" type="appl" />
         <tli id="T490" time="249.0" type="appl" />
         <tli id="T491" time="249.5" type="appl" />
         <tli id="T492" time="250.0" type="appl" />
         <tli id="T493" time="250.5" type="appl" />
         <tli id="T494" time="251.0" type="appl" />
         <tli id="T495" time="251.5" type="appl" />
         <tli id="T496" time="252.0" type="appl" />
         <tli id="T497" time="252.5" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="BeVP"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T497" id="Seg_0" n="sc" s="T0">
               <ts e="T7" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Kineːhi</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">gɨtta</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">staršina</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">biːrdiː</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">u͡ollaːktar</ts>
                  <nts id="Seg_17" n="HIAT:ip">,</nts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_20" n="HIAT:w" s="T5">olorbuttar</ts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_23" n="HIAT:w" s="T6">ebit</ts>
                  <nts id="Seg_24" n="HIAT:ip">.</nts>
                  <nts id="Seg_25" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_27" n="HIAT:u" s="T7">
                  <ts e="T8" id="Seg_29" n="HIAT:w" s="T7">Ikki͡ennere</ts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_31" n="HIAT:ip">—</nts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_34" n="HIAT:w" s="T8">tɨ͡a</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_37" n="HIAT:w" s="T9">kihilere</ts>
                  <nts id="Seg_38" n="HIAT:ip">.</nts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_41" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_43" n="HIAT:w" s="T10">U͡olattar</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_46" n="HIAT:w" s="T11">biːrge</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_49" n="HIAT:w" s="T12">ü͡öskeːn</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_52" n="HIAT:w" s="T13">dogorduː</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_55" n="HIAT:w" s="T14">bagajɨ</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_58" n="HIAT:w" s="T15">ebittere</ts>
                  <nts id="Seg_59" n="HIAT:ip">.</nts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T22" id="Seg_62" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_64" n="HIAT:w" s="T16">Bu</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_67" n="HIAT:w" s="T17">kihiler</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_70" n="HIAT:w" s="T18">bɨlɨrgɨttan</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_73" n="HIAT:w" s="T19">köhö</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_76" n="HIAT:w" s="T20">hɨldʼar</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_79" n="HIAT:w" s="T21">ebittere</ts>
                  <nts id="Seg_80" n="HIAT:ip">.</nts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T28" id="Seg_83" n="HIAT:u" s="T22">
                  <ts e="T23" id="Seg_85" n="HIAT:w" s="T22">Araj</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_88" n="HIAT:w" s="T23">kühün</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_91" n="HIAT:w" s="T24">paːs</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_94" n="HIAT:w" s="T25">iːter</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_97" n="HIAT:w" s="T26">kem</ts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_100" n="HIAT:w" s="T27">bu͡olbut</ts>
                  <nts id="Seg_101" n="HIAT:ip">.</nts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T32" id="Seg_104" n="HIAT:u" s="T28">
                  <ts e="T29" id="Seg_106" n="HIAT:w" s="T28">Astarɨn</ts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_109" n="HIAT:w" s="T29">erdetten</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_112" n="HIAT:w" s="T30">belemneːn</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_115" n="HIAT:w" s="T31">keːspitter</ts>
                  <nts id="Seg_116" n="HIAT:ip">.</nts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T37" id="Seg_119" n="HIAT:u" s="T32">
                  <ts e="T33" id="Seg_121" n="HIAT:w" s="T32">Ikki</ts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_124" n="HIAT:w" s="T33">u͡ol</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_127" n="HIAT:w" s="T34">ɨtɨnan</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_130" n="HIAT:w" s="T35">paːstɨː</ts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_133" n="HIAT:w" s="T36">barbɨttar</ts>
                  <nts id="Seg_134" n="HIAT:ip">.</nts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T42" id="Seg_137" n="HIAT:u" s="T37">
                  <ts e="T38" id="Seg_139" n="HIAT:w" s="T37">Paːs</ts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_142" n="HIAT:w" s="T38">iːtten-iːtten</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_145" n="HIAT:w" s="T39">baran</ts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_148" n="HIAT:w" s="T40">očihalarɨgar</ts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_151" n="HIAT:w" s="T41">kommuttar</ts>
                  <nts id="Seg_152" n="HIAT:ip">.</nts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T54" id="Seg_155" n="HIAT:u" s="T42">
                  <ts e="T43" id="Seg_157" n="HIAT:w" s="T42">Biːrde</ts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_160" n="HIAT:w" s="T43">ki͡ehe</ts>
                  <nts id="Seg_161" n="HIAT:ip">,</nts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_164" n="HIAT:w" s="T44">ɨttarɨn</ts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_167" n="HIAT:w" s="T45">baːjan</ts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_170" n="HIAT:w" s="T46">baran</ts>
                  <nts id="Seg_171" n="HIAT:ip">,</nts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_174" n="HIAT:w" s="T47">balɨk</ts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_177" n="HIAT:w" s="T48">hɨ͡atɨnan</ts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_180" n="HIAT:w" s="T49">eli͡eke</ts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_183" n="HIAT:w" s="T50">oŋostubuttar</ts>
                  <nts id="Seg_184" n="HIAT:ip">,</nts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_187" n="HIAT:w" s="T51">čaːj</ts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_190" n="HIAT:w" s="T52">ihe</ts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_193" n="HIAT:w" s="T53">olorbuttar</ts>
                  <nts id="Seg_194" n="HIAT:ip">.</nts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T58" id="Seg_197" n="HIAT:u" s="T54">
                  <ts e="T55" id="Seg_199" n="HIAT:w" s="T54">Araj</ts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_202" n="HIAT:w" s="T55">hɨrgalaːk</ts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_205" n="HIAT:w" s="T56">tɨ͡aha</ts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_208" n="HIAT:w" s="T57">ihillibit</ts>
                  <nts id="Seg_209" n="HIAT:ip">.</nts>
                  <nts id="Seg_210" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T61" id="Seg_212" n="HIAT:u" s="T58">
                  <ts e="T59" id="Seg_214" n="HIAT:w" s="T58">Kineːs</ts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_217" n="HIAT:w" s="T59">u͡ola</ts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_220" n="HIAT:w" s="T60">dʼi͡ebit</ts>
                  <nts id="Seg_221" n="HIAT:ip">:</nts>
                  <nts id="Seg_222" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T66" id="Seg_224" n="HIAT:u" s="T61">
                  <nts id="Seg_225" n="HIAT:ip">"</nts>
                  <ts e="T62" id="Seg_227" n="HIAT:w" s="T61">Ütü͡ö</ts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_230" n="HIAT:w" s="T62">kihi</ts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_233" n="HIAT:w" s="T63">kelbete</ts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_236" n="HIAT:w" s="T64">bu͡olu͡o</ts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_239" n="HIAT:w" s="T65">du͡o</ts>
                  <nts id="Seg_240" n="HIAT:ip">?</nts>
                  <nts id="Seg_241" n="HIAT:ip">"</nts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T69" id="Seg_244" n="HIAT:u" s="T66">
                  <ts e="T67" id="Seg_246" n="HIAT:w" s="T66">Bejete</ts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_249" n="HIAT:w" s="T67">tabaktɨː</ts>
                  <nts id="Seg_250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_252" n="HIAT:w" s="T68">olorbut</ts>
                  <nts id="Seg_253" n="HIAT:ip">.</nts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T72" id="Seg_256" n="HIAT:u" s="T69">
                  <ts e="T70" id="Seg_258" n="HIAT:w" s="T69">Dogoro</ts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_261" n="HIAT:w" s="T70">taraja</ts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_264" n="HIAT:w" s="T71">hɨppɨt</ts>
                  <nts id="Seg_265" n="HIAT:ip">.</nts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T76" id="Seg_268" n="HIAT:u" s="T72">
                  <ts e="T73" id="Seg_270" n="HIAT:w" s="T72">Ihillemmittere</ts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_272" n="HIAT:ip">—</nts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_275" n="HIAT:w" s="T73">ɨttara</ts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_278" n="HIAT:w" s="T74">ürbet</ts>
                  <nts id="Seg_279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_281" n="HIAT:w" s="T75">bu͡olbut</ts>
                  <nts id="Seg_282" n="HIAT:ip">.</nts>
                  <nts id="Seg_283" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T79" id="Seg_285" n="HIAT:u" s="T76">
                  <ts e="T77" id="Seg_287" n="HIAT:w" s="T76">Kaːmar</ts>
                  <nts id="Seg_288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_290" n="HIAT:w" s="T77">tɨ͡as</ts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_293" n="HIAT:w" s="T78">ihillibit</ts>
                  <nts id="Seg_294" n="HIAT:ip">.</nts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T81" id="Seg_297" n="HIAT:u" s="T79">
                  <ts e="T80" id="Seg_299" n="HIAT:w" s="T79">Kineːs</ts>
                  <nts id="Seg_300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_302" n="HIAT:w" s="T80">u͡ola</ts>
                  <nts id="Seg_303" n="HIAT:ip">:</nts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T83" id="Seg_306" n="HIAT:u" s="T81">
                  <nts id="Seg_307" n="HIAT:ip">"</nts>
                  <ts e="T82" id="Seg_309" n="HIAT:w" s="T81">Kuttanabɨn</ts>
                  <nts id="Seg_310" n="HIAT:ip">"</nts>
                  <nts id="Seg_311" n="HIAT:ip">,</nts>
                  <nts id="Seg_312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_314" n="HIAT:w" s="T82">di͡ebit</ts>
                  <nts id="Seg_315" n="HIAT:ip">.</nts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T88" id="Seg_318" n="HIAT:u" s="T83">
                  <ts e="T84" id="Seg_320" n="HIAT:w" s="T83">Dogoro</ts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_323" n="HIAT:w" s="T84">hol</ts>
                  <nts id="Seg_324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_326" n="HIAT:w" s="T85">kördük</ts>
                  <nts id="Seg_327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_329" n="HIAT:w" s="T86">taraja</ts>
                  <nts id="Seg_330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_332" n="HIAT:w" s="T87">hɨppɨt</ts>
                  <nts id="Seg_333" n="HIAT:ip">.</nts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T105" id="Seg_336" n="HIAT:u" s="T88">
                  <ts e="T89" id="Seg_338" n="HIAT:w" s="T88">Aːn</ts>
                  <nts id="Seg_339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_341" n="HIAT:w" s="T89">arɨllɨbɨt</ts>
                  <nts id="Seg_342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_344" n="HIAT:w" s="T90">da</ts>
                  <nts id="Seg_345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_347" n="HIAT:w" s="T91">hu͡on</ts>
                  <nts id="Seg_348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_350" n="HIAT:w" s="T92">bagajɨ</ts>
                  <nts id="Seg_351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_353" n="HIAT:w" s="T93">dʼaktar</ts>
                  <nts id="Seg_354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_356" n="HIAT:w" s="T94">arɨččɨ</ts>
                  <nts id="Seg_357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_359" n="HIAT:w" s="T95">batan</ts>
                  <nts id="Seg_360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_362" n="HIAT:w" s="T96">kiːrbit</ts>
                  <nts id="Seg_363" n="HIAT:ip">,</nts>
                  <nts id="Seg_364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_366" n="HIAT:w" s="T97">čöl</ts>
                  <nts id="Seg_367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_369" n="HIAT:w" s="T98">oguru͡o</ts>
                  <nts id="Seg_370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_372" n="HIAT:w" s="T99">taŋastaːk</ts>
                  <nts id="Seg_373" n="HIAT:ip">,</nts>
                  <nts id="Seg_374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_376" n="HIAT:w" s="T100">oguru͡o</ts>
                  <nts id="Seg_377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_379" n="HIAT:w" s="T101">bergeheleːk</ts>
                  <nts id="Seg_380" n="HIAT:ip">,</nts>
                  <nts id="Seg_381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_383" n="HIAT:w" s="T102">baːj</ts>
                  <nts id="Seg_384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_386" n="HIAT:w" s="T103">bagajɨ</ts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_389" n="HIAT:w" s="T104">dʼühünneːk</ts>
                  <nts id="Seg_390" n="HIAT:ip">.</nts>
                  <nts id="Seg_391" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T108" id="Seg_393" n="HIAT:u" s="T105">
                  <ts e="T106" id="Seg_395" n="HIAT:w" s="T105">Staršina</ts>
                  <nts id="Seg_396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_398" n="HIAT:w" s="T106">u͡ola</ts>
                  <nts id="Seg_399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_401" n="HIAT:w" s="T107">dogorun</ts>
                  <nts id="Seg_402" n="HIAT:ip">:</nts>
                  <nts id="Seg_403" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T111" id="Seg_405" n="HIAT:u" s="T108">
                  <nts id="Seg_406" n="HIAT:ip">"</nts>
                  <ts e="T109" id="Seg_408" n="HIAT:w" s="T108">Čaːjdaː</ts>
                  <nts id="Seg_409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_411" n="HIAT:w" s="T109">ɨ͡aldʼɨtɨ</ts>
                  <nts id="Seg_412" n="HIAT:ip">"</nts>
                  <nts id="Seg_413" n="HIAT:ip">,</nts>
                  <nts id="Seg_414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_416" n="HIAT:w" s="T110">dʼi͡ebit</ts>
                  <nts id="Seg_417" n="HIAT:ip">.</nts>
                  <nts id="Seg_418" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T118" id="Seg_420" n="HIAT:u" s="T111">
                  <ts e="T112" id="Seg_422" n="HIAT:w" s="T111">Kineːs</ts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_425" n="HIAT:w" s="T112">u͡ola</ts>
                  <nts id="Seg_426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_428" n="HIAT:w" s="T113">turan</ts>
                  <nts id="Seg_429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_431" n="HIAT:w" s="T114">ohok</ts>
                  <nts id="Seg_432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_434" n="HIAT:w" s="T115">ottummut</ts>
                  <nts id="Seg_435" n="HIAT:ip">,</nts>
                  <nts id="Seg_436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_438" n="HIAT:w" s="T116">čaːj</ts>
                  <nts id="Seg_439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_441" n="HIAT:w" s="T117">kɨːjnʼarbat</ts>
                  <nts id="Seg_442" n="HIAT:ip">.</nts>
                  <nts id="Seg_443" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T123" id="Seg_445" n="HIAT:u" s="T118">
                  <ts e="T119" id="Seg_447" n="HIAT:w" s="T118">Araj</ts>
                  <nts id="Seg_448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_450" n="HIAT:w" s="T119">dʼaktardara</ts>
                  <nts id="Seg_451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_453" n="HIAT:w" s="T120">hɨrajɨn</ts>
                  <nts id="Seg_454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_456" n="HIAT:w" s="T121">köllʼörböt</ts>
                  <nts id="Seg_457" n="HIAT:ip">,</nts>
                  <nts id="Seg_458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_460" n="HIAT:w" s="T122">haŋarsɨbat</ts>
                  <nts id="Seg_461" n="HIAT:ip">.</nts>
                  <nts id="Seg_462" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T141" id="Seg_464" n="HIAT:u" s="T123">
                  <ts e="T124" id="Seg_466" n="HIAT:w" s="T123">Bu</ts>
                  <nts id="Seg_467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_469" n="HIAT:w" s="T124">u͡ol</ts>
                  <nts id="Seg_470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_472" n="HIAT:w" s="T125">balɨk</ts>
                  <nts id="Seg_473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_475" n="HIAT:w" s="T126">kü͡öhe</ts>
                  <nts id="Seg_476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_478" n="HIAT:w" s="T127">hɨ͡alammɨt</ts>
                  <nts id="Seg_479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_481" n="HIAT:w" s="T128">ispiːčke</ts>
                  <nts id="Seg_482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_484" n="HIAT:w" s="T129">mahɨnan</ts>
                  <nts id="Seg_485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_487" n="HIAT:w" s="T130">karagɨn</ts>
                  <nts id="Seg_488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_490" n="HIAT:w" s="T131">tepteren</ts>
                  <nts id="Seg_491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_493" n="HIAT:w" s="T132">baran</ts>
                  <nts id="Seg_494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_496" n="HIAT:w" s="T133">körbüte</ts>
                  <nts id="Seg_497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_498" n="HIAT:ip">—</nts>
                  <nts id="Seg_499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_501" n="HIAT:w" s="T134">dʼaktar</ts>
                  <nts id="Seg_502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_504" n="HIAT:w" s="T135">küle</ts>
                  <nts id="Seg_505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_507" n="HIAT:w" s="T136">oloror</ts>
                  <nts id="Seg_508" n="HIAT:ip">,</nts>
                  <nts id="Seg_509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_511" n="HIAT:w" s="T137">hogotok</ts>
                  <nts id="Seg_512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_514" n="HIAT:w" s="T138">ire</ts>
                  <nts id="Seg_515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_517" n="HIAT:w" s="T139">tiːsteːk</ts>
                  <nts id="Seg_518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_520" n="HIAT:w" s="T140">ebit</ts>
                  <nts id="Seg_521" n="HIAT:ip">.</nts>
                  <nts id="Seg_522" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T147" id="Seg_524" n="HIAT:u" s="T141">
                  <ts e="T142" id="Seg_526" n="HIAT:w" s="T141">Mas</ts>
                  <nts id="Seg_527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_529" n="HIAT:w" s="T142">killere</ts>
                  <nts id="Seg_530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_532" n="HIAT:w" s="T143">taksɨbɨt</ts>
                  <nts id="Seg_533" n="HIAT:ip">,</nts>
                  <nts id="Seg_534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_536" n="HIAT:w" s="T144">ɨttarɨn</ts>
                  <nts id="Seg_537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_539" n="HIAT:w" s="T145">kölüjteleːn</ts>
                  <nts id="Seg_540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_542" n="HIAT:w" s="T146">keːspit</ts>
                  <nts id="Seg_543" n="HIAT:ip">.</nts>
                  <nts id="Seg_544" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T153" id="Seg_546" n="HIAT:u" s="T147">
                  <ts e="T148" id="Seg_548" n="HIAT:w" s="T147">Körbüte</ts>
                  <nts id="Seg_549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_550" n="HIAT:ip">—</nts>
                  <nts id="Seg_551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_553" n="HIAT:w" s="T148">dʼaktar</ts>
                  <nts id="Seg_554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_556" n="HIAT:w" s="T149">hɨrgatɨgar</ts>
                  <nts id="Seg_557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_559" n="HIAT:w" s="T150">alta</ts>
                  <nts id="Seg_560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_562" n="HIAT:w" s="T151">ɨt</ts>
                  <nts id="Seg_563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_565" n="HIAT:w" s="T152">kölüllübüt</ts>
                  <nts id="Seg_566" n="HIAT:ip">.</nts>
                  <nts id="Seg_567" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T157" id="Seg_569" n="HIAT:u" s="T153">
                  <ts e="T154" id="Seg_571" n="HIAT:w" s="T153">Kiːren</ts>
                  <nts id="Seg_572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_574" n="HIAT:w" s="T154">baran</ts>
                  <nts id="Seg_575" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_577" n="HIAT:w" s="T155">ɨ͡aldʼɨtɨn</ts>
                  <nts id="Seg_578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_580" n="HIAT:w" s="T156">ahappɨt</ts>
                  <nts id="Seg_581" n="HIAT:ip">.</nts>
                  <nts id="Seg_582" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T169" id="Seg_584" n="HIAT:u" s="T157">
                  <nts id="Seg_585" n="HIAT:ip">"</nts>
                  <ts e="T158" id="Seg_587" n="HIAT:w" s="T157">Buːs</ts>
                  <nts id="Seg_588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_590" n="HIAT:w" s="T158">killeste</ts>
                  <nts id="Seg_591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_593" n="HIAT:w" s="T159">taksɨ͡am</ts>
                  <nts id="Seg_594" n="HIAT:ip">"</nts>
                  <nts id="Seg_595" n="HIAT:ip">,</nts>
                  <nts id="Seg_596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_598" n="HIAT:w" s="T160">dʼi͡en</ts>
                  <nts id="Seg_599" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_601" n="HIAT:w" s="T161">baran</ts>
                  <nts id="Seg_602" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_604" n="HIAT:w" s="T162">tanastarɨn</ts>
                  <nts id="Seg_605" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_607" n="HIAT:w" s="T163">kɨbɨnan</ts>
                  <nts id="Seg_608" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_610" n="HIAT:w" s="T164">tahaːrbɨt</ts>
                  <nts id="Seg_611" n="HIAT:ip">,</nts>
                  <nts id="Seg_612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_614" n="HIAT:w" s="T165">ɨtɨn</ts>
                  <nts id="Seg_615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_617" n="HIAT:w" s="T166">hɨrgatɨgar</ts>
                  <nts id="Seg_618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_620" n="HIAT:w" s="T167">uːran</ts>
                  <nts id="Seg_621" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_623" n="HIAT:w" s="T168">keːspit</ts>
                  <nts id="Seg_624" n="HIAT:ip">.</nts>
                  <nts id="Seg_625" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T178" id="Seg_627" n="HIAT:u" s="T169">
                  <ts e="T170" id="Seg_629" n="HIAT:w" s="T169">Onton</ts>
                  <nts id="Seg_630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_632" n="HIAT:w" s="T170">kiːrbit</ts>
                  <nts id="Seg_633" n="HIAT:ip">,</nts>
                  <nts id="Seg_634" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_636" n="HIAT:w" s="T171">dʼaktarɨ</ts>
                  <nts id="Seg_637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_639" n="HIAT:w" s="T172">olorbut</ts>
                  <nts id="Seg_640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_642" n="HIAT:w" s="T173">hiriger</ts>
                  <nts id="Seg_643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_645" n="HIAT:w" s="T174">utupput</ts>
                  <nts id="Seg_646" n="HIAT:ip">,</nts>
                  <nts id="Seg_647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_649" n="HIAT:w" s="T175">tiriː</ts>
                  <nts id="Seg_650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_652" n="HIAT:w" s="T176">bi͡erbit</ts>
                  <nts id="Seg_653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_655" n="HIAT:w" s="T177">telgeni͡en</ts>
                  <nts id="Seg_656" n="HIAT:ip">.</nts>
                  <nts id="Seg_657" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T183" id="Seg_659" n="HIAT:u" s="T178">
                  <ts e="T179" id="Seg_661" n="HIAT:w" s="T178">Balɨk</ts>
                  <nts id="Seg_662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_664" n="HIAT:w" s="T179">hɨ͡ata</ts>
                  <nts id="Seg_665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_667" n="HIAT:w" s="T180">hunuːrdarɨn</ts>
                  <nts id="Seg_668" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_670" n="HIAT:w" s="T181">ututan</ts>
                  <nts id="Seg_671" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_673" n="HIAT:w" s="T182">keːspitter</ts>
                  <nts id="Seg_674" n="HIAT:ip">.</nts>
                  <nts id="Seg_675" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T184" id="Seg_677" n="HIAT:u" s="T183">
                  <ts e="T184" id="Seg_679" n="HIAT:w" s="T183">Utujbuttar</ts>
                  <nts id="Seg_680" n="HIAT:ip">.</nts>
                  <nts id="Seg_681" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T188" id="Seg_683" n="HIAT:u" s="T184">
                  <ts e="T185" id="Seg_685" n="HIAT:w" s="T184">Staršina</ts>
                  <nts id="Seg_686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_688" n="HIAT:w" s="T185">u͡ola</ts>
                  <nts id="Seg_689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_691" n="HIAT:w" s="T186">dogorun</ts>
                  <nts id="Seg_692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_694" n="HIAT:w" s="T187">eppit</ts>
                  <nts id="Seg_695" n="HIAT:ip">:</nts>
                  <nts id="Seg_696" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T192" id="Seg_698" n="HIAT:u" s="T188">
                  <nts id="Seg_699" n="HIAT:ip">"</nts>
                  <ts e="T189" id="Seg_701" n="HIAT:w" s="T188">Doː</ts>
                  <nts id="Seg_702" n="HIAT:ip">,</nts>
                  <nts id="Seg_703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_705" n="HIAT:w" s="T189">mantɨbɨt</ts>
                  <nts id="Seg_706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_708" n="HIAT:w" s="T190">kɨːs</ts>
                  <nts id="Seg_709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_711" n="HIAT:w" s="T191">bu͡olbataːk</ts>
                  <nts id="Seg_712" n="HIAT:ip">?</nts>
                  <nts id="Seg_713" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T193" id="Seg_715" n="HIAT:u" s="T192">
                  <ts e="T193" id="Seg_717" n="HIAT:w" s="T192">Kɨːstɨ͡ak</ts>
                  <nts id="Seg_718" n="HIAT:ip">.</nts>
                  <nts id="Seg_719" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T195" id="Seg_721" n="HIAT:u" s="T193">
                  <ts e="T194" id="Seg_723" n="HIAT:w" s="T193">Tugun</ts>
                  <nts id="Seg_724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_726" n="HIAT:w" s="T194">kuttanagɨn</ts>
                  <nts id="Seg_727" n="HIAT:ip">?</nts>
                  <nts id="Seg_728" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T198" id="Seg_730" n="HIAT:u" s="T195">
                  <ts e="T196" id="Seg_732" n="HIAT:w" s="T195">Min</ts>
                  <nts id="Seg_733" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_735" n="HIAT:w" s="T196">oččogo</ts>
                  <nts id="Seg_736" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_738" n="HIAT:w" s="T197">barɨ͡am</ts>
                  <nts id="Seg_739" n="HIAT:ip">.</nts>
                  <nts id="Seg_740" n="HIAT:ip">"</nts>
                  <nts id="Seg_741" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T199" id="Seg_743" n="HIAT:u" s="T198">
                  <ts e="T199" id="Seg_745" n="HIAT:w" s="T198">Dogoro</ts>
                  <nts id="Seg_746" n="HIAT:ip">:</nts>
                  <nts id="Seg_747" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T204" id="Seg_749" n="HIAT:u" s="T199">
                  <nts id="Seg_750" n="HIAT:ip">"</nts>
                  <ts e="T200" id="Seg_752" n="HIAT:w" s="T199">Min</ts>
                  <nts id="Seg_753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_755" n="HIAT:w" s="T200">taksɨbɨtɨm</ts>
                  <nts id="Seg_756" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_758" n="HIAT:w" s="T201">kenne</ts>
                  <nts id="Seg_759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_761" n="HIAT:w" s="T202">kɨːskar</ts>
                  <nts id="Seg_762" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_764" n="HIAT:w" s="T203">baraːr</ts>
                  <nts id="Seg_765" n="HIAT:ip">.</nts>
                  <nts id="Seg_766" n="HIAT:ip">"</nts>
                  <nts id="Seg_767" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T206" id="Seg_769" n="HIAT:u" s="T204">
                  <ts e="T205" id="Seg_771" n="HIAT:w" s="T204">Taksan</ts>
                  <nts id="Seg_772" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_774" n="HIAT:w" s="T205">kaːlbɨt</ts>
                  <nts id="Seg_775" n="HIAT:ip">.</nts>
                  <nts id="Seg_776" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T210" id="Seg_778" n="HIAT:u" s="T206">
                  <ts e="T207" id="Seg_780" n="HIAT:w" s="T206">Taŋnan</ts>
                  <nts id="Seg_781" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_783" n="HIAT:w" s="T207">baran</ts>
                  <nts id="Seg_784" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_786" n="HIAT:w" s="T208">hɨrgatɨgar</ts>
                  <nts id="Seg_787" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_789" n="HIAT:w" s="T209">olorbut</ts>
                  <nts id="Seg_790" n="HIAT:ip">.</nts>
                  <nts id="Seg_791" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T215" id="Seg_793" n="HIAT:u" s="T210">
                  <ts e="T211" id="Seg_795" n="HIAT:w" s="T210">Töhö</ts>
                  <nts id="Seg_796" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_798" n="HIAT:w" s="T211">bu͡olu͡oj</ts>
                  <nts id="Seg_799" n="HIAT:ip">,</nts>
                  <nts id="Seg_800" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_802" n="HIAT:w" s="T212">ölör-kaːlar</ts>
                  <nts id="Seg_803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_805" n="HIAT:w" s="T213">üːgü</ts>
                  <nts id="Seg_806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_808" n="HIAT:w" s="T214">ihillibit</ts>
                  <nts id="Seg_809" n="HIAT:ip">:</nts>
                  <nts id="Seg_810" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T216" id="Seg_812" n="HIAT:u" s="T215">
                  <nts id="Seg_813" n="HIAT:ip">—</nts>
                  <ts e="T216" id="Seg_815" n="HIAT:w" s="T215">Araː</ts>
                  <nts id="Seg_816" n="HIAT:ip">!</nts>
                  <nts id="Seg_817" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T217" id="Seg_819" n="HIAT:u" s="T216">
                  <ts e="T217" id="Seg_821" n="HIAT:w" s="T216">Abɨraː</ts>
                  <nts id="Seg_822" n="HIAT:ip">!</nts>
                  <nts id="Seg_823" n="HIAT:ip">"</nts>
                  <nts id="Seg_824" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T224" id="Seg_826" n="HIAT:u" s="T217">
                  <ts e="T218" id="Seg_828" n="HIAT:w" s="T217">Bu</ts>
                  <nts id="Seg_829" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_831" n="HIAT:w" s="T218">u͡ol</ts>
                  <nts id="Seg_832" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_834" n="HIAT:w" s="T219">nʼu͡oŋutun</ts>
                  <nts id="Seg_835" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_837" n="HIAT:w" s="T220">ɨlla</ts>
                  <nts id="Seg_838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_840" n="HIAT:w" s="T221">daːganɨ</ts>
                  <nts id="Seg_841" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_843" n="HIAT:w" s="T222">kötüten</ts>
                  <nts id="Seg_844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_846" n="HIAT:w" s="T223">kaːlbɨt</ts>
                  <nts id="Seg_847" n="HIAT:ip">.</nts>
                  <nts id="Seg_848" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T228" id="Seg_850" n="HIAT:u" s="T224">
                  <ts e="T225" id="Seg_852" n="HIAT:w" s="T224">Kennitten</ts>
                  <nts id="Seg_853" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_855" n="HIAT:w" s="T225">dʼaktar</ts>
                  <nts id="Seg_856" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_858" n="HIAT:w" s="T226">haŋata</ts>
                  <nts id="Seg_859" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_861" n="HIAT:w" s="T227">ihillibit</ts>
                  <nts id="Seg_862" n="HIAT:ip">:</nts>
                  <nts id="Seg_863" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T234" id="Seg_865" n="HIAT:u" s="T228">
                  <nts id="Seg_866" n="HIAT:ip">"</nts>
                  <ts e="T229" id="Seg_868" n="HIAT:w" s="T228">Bilbitim</ts>
                  <nts id="Seg_869" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_871" n="HIAT:w" s="T229">bu͡ollar</ts>
                  <nts id="Seg_872" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_874" n="HIAT:w" s="T230">hi͡en</ts>
                  <nts id="Seg_875" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_877" n="HIAT:w" s="T231">keːhi͡e</ts>
                  <nts id="Seg_878" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_880" n="HIAT:w" s="T232">etim</ts>
                  <nts id="Seg_881" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_883" n="HIAT:w" s="T233">bu͡o</ts>
                  <nts id="Seg_884" n="HIAT:ip">!</nts>
                  <nts id="Seg_885" n="HIAT:ip">"</nts>
                  <nts id="Seg_886" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T240" id="Seg_888" n="HIAT:u" s="T234">
                  <ts e="T235" id="Seg_890" n="HIAT:w" s="T234">Kelen</ts>
                  <nts id="Seg_891" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_893" n="HIAT:w" s="T235">bu</ts>
                  <nts id="Seg_894" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_896" n="HIAT:w" s="T236">u͡ol</ts>
                  <nts id="Seg_897" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_899" n="HIAT:w" s="T237">agatɨgar</ts>
                  <nts id="Seg_900" n="HIAT:ip">,</nts>
                  <nts id="Seg_901" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_903" n="HIAT:w" s="T238">kineːske</ts>
                  <nts id="Seg_904" n="HIAT:ip">,</nts>
                  <nts id="Seg_905" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_907" n="HIAT:w" s="T239">kepseːbit</ts>
                  <nts id="Seg_908" n="HIAT:ip">.</nts>
                  <nts id="Seg_909" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T247" id="Seg_911" n="HIAT:u" s="T240">
                  <ts e="T241" id="Seg_913" n="HIAT:w" s="T240">Kineːhi</ts>
                  <nts id="Seg_914" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_916" n="HIAT:w" s="T241">gɨtta</ts>
                  <nts id="Seg_917" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_919" n="HIAT:w" s="T242">staršina</ts>
                  <nts id="Seg_920" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_922" n="HIAT:w" s="T243">hette</ts>
                  <nts id="Seg_923" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_925" n="HIAT:w" s="T244">dʼi͡eni</ts>
                  <nts id="Seg_926" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_928" n="HIAT:w" s="T245">komujan</ts>
                  <nts id="Seg_929" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_931" n="HIAT:w" s="T246">munnʼaktaːbɨttar</ts>
                  <nts id="Seg_932" n="HIAT:ip">.</nts>
                  <nts id="Seg_933" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T251" id="Seg_935" n="HIAT:u" s="T247">
                  <ts e="T248" id="Seg_937" n="HIAT:w" s="T247">Ikki</ts>
                  <nts id="Seg_938" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_940" n="HIAT:w" s="T248">u͡olu</ts>
                  <nts id="Seg_941" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_943" n="HIAT:w" s="T249">ɨːppɨttar</ts>
                  <nts id="Seg_944" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_946" n="HIAT:w" s="T250">körü͡ökterin</ts>
                  <nts id="Seg_947" n="HIAT:ip">.</nts>
                  <nts id="Seg_948" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T256" id="Seg_950" n="HIAT:u" s="T251">
                  <ts e="T252" id="Seg_952" n="HIAT:w" s="T251">Ikki</ts>
                  <nts id="Seg_953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_955" n="HIAT:w" s="T252">u͡ol</ts>
                  <nts id="Seg_956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_958" n="HIAT:w" s="T253">bu͡olan</ts>
                  <nts id="Seg_959" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_961" n="HIAT:w" s="T254">baran</ts>
                  <nts id="Seg_962" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_964" n="HIAT:w" s="T255">kaːllɨlar</ts>
                  <nts id="Seg_965" n="HIAT:ip">.</nts>
                  <nts id="Seg_966" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T260" id="Seg_968" n="HIAT:u" s="T256">
                  <ts e="T257" id="Seg_970" n="HIAT:w" s="T256">Ör</ts>
                  <nts id="Seg_971" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_973" n="HIAT:w" s="T257">bu͡olu͡oktaraj</ts>
                  <nts id="Seg_974" n="HIAT:ip">,</nts>
                  <nts id="Seg_975" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_977" n="HIAT:w" s="T258">hɨrdɨkka</ts>
                  <nts id="Seg_978" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_980" n="HIAT:w" s="T259">kelbitter</ts>
                  <nts id="Seg_981" n="HIAT:ip">.</nts>
                  <nts id="Seg_982" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T268" id="Seg_984" n="HIAT:u" s="T260">
                  <ts e="T261" id="Seg_986" n="HIAT:w" s="T260">Körbüttere</ts>
                  <nts id="Seg_987" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_988" n="HIAT:ip">—</nts>
                  <nts id="Seg_989" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_991" n="HIAT:w" s="T261">biːr</ts>
                  <nts id="Seg_992" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_994" n="HIAT:w" s="T262">da</ts>
                  <nts id="Seg_995" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_997" n="HIAT:w" s="T263">kihi</ts>
                  <nts id="Seg_998" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_1000" n="HIAT:w" s="T264">hu͡ok</ts>
                  <nts id="Seg_1001" n="HIAT:ip">,</nts>
                  <nts id="Seg_1002" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_1004" n="HIAT:w" s="T265">hu͡ol</ts>
                  <nts id="Seg_1005" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_1007" n="HIAT:w" s="T266">da</ts>
                  <nts id="Seg_1008" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_1010" n="HIAT:w" s="T267">hu͡ok</ts>
                  <nts id="Seg_1011" n="HIAT:ip">.</nts>
                  <nts id="Seg_1012" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T276" id="Seg_1014" n="HIAT:u" s="T268">
                  <ts e="T269" id="Seg_1016" n="HIAT:w" s="T268">Uraha</ts>
                  <nts id="Seg_1017" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_1019" n="HIAT:w" s="T269">dʼi͡ege</ts>
                  <nts id="Seg_1020" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_1022" n="HIAT:w" s="T270">kiːrbittere</ts>
                  <nts id="Seg_1023" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1024" n="HIAT:ip">—</nts>
                  <nts id="Seg_1025" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_1027" n="HIAT:w" s="T271">u͡ol</ts>
                  <nts id="Seg_1028" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_1030" n="HIAT:w" s="T272">oŋu͡oga</ts>
                  <nts id="Seg_1031" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_1033" n="HIAT:w" s="T273">ire</ts>
                  <nts id="Seg_1034" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_1036" n="HIAT:w" s="T274">dardaja</ts>
                  <nts id="Seg_1037" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_1039" n="HIAT:w" s="T275">hɨtar</ts>
                  <nts id="Seg_1040" n="HIAT:ip">.</nts>
                  <nts id="Seg_1041" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T280" id="Seg_1043" n="HIAT:u" s="T276">
                  <ts e="T277" id="Seg_1045" n="HIAT:w" s="T276">Bu</ts>
                  <nts id="Seg_1046" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_1048" n="HIAT:w" s="T277">ikki</ts>
                  <nts id="Seg_1049" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_1051" n="HIAT:w" s="T278">u͡ol</ts>
                  <nts id="Seg_1052" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_1054" n="HIAT:w" s="T279">tönnübütter</ts>
                  <nts id="Seg_1055" n="HIAT:ip">.</nts>
                  <nts id="Seg_1056" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T282" id="Seg_1058" n="HIAT:u" s="T280">
                  <ts e="T281" id="Seg_1060" n="HIAT:w" s="T280">Kineːske</ts>
                  <nts id="Seg_1061" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_1063" n="HIAT:w" s="T281">kepseːbitter</ts>
                  <nts id="Seg_1064" n="HIAT:ip">.</nts>
                  <nts id="Seg_1065" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T293" id="Seg_1067" n="HIAT:u" s="T282">
                  <ts e="T283" id="Seg_1069" n="HIAT:w" s="T282">Biːr</ts>
                  <nts id="Seg_1070" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_1072" n="HIAT:w" s="T283">tulaːjak</ts>
                  <nts id="Seg_1073" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_1075" n="HIAT:w" s="T284">ogo</ts>
                  <nts id="Seg_1076" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_1078" n="HIAT:w" s="T285">baːr</ts>
                  <nts id="Seg_1079" n="HIAT:ip">,</nts>
                  <nts id="Seg_1080" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_1082" n="HIAT:w" s="T286">ikki</ts>
                  <nts id="Seg_1083" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_1085" n="HIAT:w" s="T287">hannɨttan</ts>
                  <nts id="Seg_1086" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_1088" n="HIAT:w" s="T288">tarakaːj</ts>
                  <nts id="Seg_1089" n="HIAT:ip">,</nts>
                  <nts id="Seg_1090" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_1092" n="HIAT:w" s="T289">hogotok</ts>
                  <nts id="Seg_1093" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_1095" n="HIAT:w" s="T290">inʼeleːk</ts>
                  <nts id="Seg_1096" n="HIAT:ip">,</nts>
                  <nts id="Seg_1097" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_1099" n="HIAT:w" s="T291">biːr</ts>
                  <nts id="Seg_1100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_1102" n="HIAT:w" s="T292">tabalaːk</ts>
                  <nts id="Seg_1103" n="HIAT:ip">.</nts>
                  <nts id="Seg_1104" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T297" id="Seg_1106" n="HIAT:u" s="T293">
                  <ts e="T294" id="Seg_1108" n="HIAT:w" s="T293">Kineːhi</ts>
                  <nts id="Seg_1109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1111" n="HIAT:w" s="T294">gɨtta</ts>
                  <nts id="Seg_1112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1114" n="HIAT:w" s="T295">staršina</ts>
                  <nts id="Seg_1115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_1117" n="HIAT:w" s="T296">munnʼaktaːbɨttar</ts>
                  <nts id="Seg_1118" n="HIAT:ip">.</nts>
                  <nts id="Seg_1119" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T302" id="Seg_1121" n="HIAT:u" s="T297">
                  <nts id="Seg_1122" n="HIAT:ip">"</nts>
                  <ts e="T298" id="Seg_1124" n="HIAT:w" s="T297">Kajdak</ts>
                  <nts id="Seg_1125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_1127" n="HIAT:w" s="T298">bu͡olu͡okputuj</ts>
                  <nts id="Seg_1128" n="HIAT:ip">,</nts>
                  <nts id="Seg_1129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_1131" n="HIAT:w" s="T299">hirbitin</ts>
                  <nts id="Seg_1132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_1134" n="HIAT:w" s="T300">bɨragɨ͡akpɨt</ts>
                  <nts id="Seg_1135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_1137" n="HIAT:w" s="T301">duː</ts>
                  <nts id="Seg_1138" n="HIAT:ip">?</nts>
                  <nts id="Seg_1139" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T308" id="Seg_1141" n="HIAT:u" s="T302">
                  <ts e="T303" id="Seg_1143" n="HIAT:w" s="T302">Emi͡e</ts>
                  <nts id="Seg_1144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_1146" n="HIAT:w" s="T303">da</ts>
                  <nts id="Seg_1147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_1149" n="HIAT:w" s="T304">paːstarbɨt</ts>
                  <nts id="Seg_1150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_1152" n="HIAT:w" s="T305">baːllar</ts>
                  <nts id="Seg_1153" n="HIAT:ip">"</nts>
                  <nts id="Seg_1154" n="HIAT:ip">,</nts>
                  <nts id="Seg_1155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_1157" n="HIAT:w" s="T307">despitter</ts>
                  <nts id="Seg_1158" n="HIAT:ip">.</nts>
                  <nts id="Seg_1159" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T312" id="Seg_1161" n="HIAT:u" s="T308">
                  <ts e="T309" id="Seg_1163" n="HIAT:w" s="T308">Staršina</ts>
                  <nts id="Seg_1164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_1166" n="HIAT:w" s="T309">onno</ts>
                  <nts id="Seg_1167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_1169" n="HIAT:w" s="T310">turan</ts>
                  <nts id="Seg_1170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_1172" n="HIAT:w" s="T311">eppit</ts>
                  <nts id="Seg_1173" n="HIAT:ip">:</nts>
                  <nts id="Seg_1174" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T321" id="Seg_1176" n="HIAT:u" s="T312">
                  <nts id="Seg_1177" n="HIAT:ip">"</nts>
                  <ts e="T313" id="Seg_1179" n="HIAT:w" s="T312">Tulaːjak</ts>
                  <nts id="Seg_1180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_1182" n="HIAT:w" s="T313">ogonu</ts>
                  <nts id="Seg_1183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_1185" n="HIAT:w" s="T314">ɨːtɨ͡akka</ts>
                  <nts id="Seg_1186" n="HIAT:ip">,</nts>
                  <nts id="Seg_1187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1189" n="HIAT:w" s="T315">min</ts>
                  <nts id="Seg_1190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_1192" n="HIAT:w" s="T316">kɨːspɨn</ts>
                  <nts id="Seg_1193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_1195" n="HIAT:w" s="T317">da</ts>
                  <nts id="Seg_1196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1198" n="HIAT:w" s="T318">bi͡eri͡em</ts>
                  <nts id="Seg_1199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_1201" n="HIAT:w" s="T319">ete</ts>
                  <nts id="Seg_1202" n="HIAT:ip">,</nts>
                  <nts id="Seg_1203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1205" n="HIAT:w" s="T320">ölördögüne</ts>
                  <nts id="Seg_1206" n="HIAT:ip">.</nts>
                  <nts id="Seg_1207" n="HIAT:ip">"</nts>
                  <nts id="Seg_1208" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T326" id="Seg_1210" n="HIAT:u" s="T321">
                  <ts e="T322" id="Seg_1212" n="HIAT:w" s="T321">Bu</ts>
                  <nts id="Seg_1213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1215" n="HIAT:w" s="T322">u͡olu</ts>
                  <nts id="Seg_1216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_1218" n="HIAT:w" s="T323">ɨgɨrbɨttar</ts>
                  <nts id="Seg_1219" n="HIAT:ip">,</nts>
                  <nts id="Seg_1220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1222" n="HIAT:w" s="T324">kepseːbitter</ts>
                  <nts id="Seg_1223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_1225" n="HIAT:w" s="T325">barɨtɨn</ts>
                  <nts id="Seg_1226" n="HIAT:ip">.</nts>
                  <nts id="Seg_1227" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T329" id="Seg_1229" n="HIAT:u" s="T326">
                  <ts e="T327" id="Seg_1231" n="HIAT:w" s="T326">Dʼe</ts>
                  <nts id="Seg_1232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_1234" n="HIAT:w" s="T327">üleger</ts>
                  <nts id="Seg_1235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_1237" n="HIAT:w" s="T328">ebit</ts>
                  <nts id="Seg_1238" n="HIAT:ip">.</nts>
                  <nts id="Seg_1239" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T342" id="Seg_1241" n="HIAT:u" s="T329">
                  <ts e="T330" id="Seg_1243" n="HIAT:w" s="T329">Ogo</ts>
                  <nts id="Seg_1244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_1246" n="HIAT:w" s="T330">muŋnaːk</ts>
                  <nts id="Seg_1247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_1249" n="HIAT:w" s="T331">hogotogun</ts>
                  <nts id="Seg_1250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_1252" n="HIAT:w" s="T332">barar</ts>
                  <nts id="Seg_1253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_1255" n="HIAT:w" s="T333">bu͡olla</ts>
                  <nts id="Seg_1256" n="HIAT:ip">,</nts>
                  <nts id="Seg_1257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_1259" n="HIAT:w" s="T334">ɨt</ts>
                  <nts id="Seg_1260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_1262" n="HIAT:w" s="T335">ire</ts>
                  <nts id="Seg_1263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_1265" n="HIAT:w" s="T336">bi͡erbitter</ts>
                  <nts id="Seg_1266" n="HIAT:ip">,</nts>
                  <nts id="Seg_1267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_1269" n="HIAT:w" s="T337">haː</ts>
                  <nts id="Seg_1270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_1272" n="HIAT:w" s="T338">da</ts>
                  <nts id="Seg_1273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_1275" n="HIAT:w" s="T339">tu͡ok</ts>
                  <nts id="Seg_1276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_1278" n="HIAT:w" s="T340">da</ts>
                  <nts id="Seg_1279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_1281" n="HIAT:w" s="T341">hu͡ok</ts>
                  <nts id="Seg_1282" n="HIAT:ip">.</nts>
                  <nts id="Seg_1283" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T346" id="Seg_1285" n="HIAT:u" s="T342">
                  <ts e="T343" id="Seg_1287" n="HIAT:w" s="T342">Dʼi͡etiger</ts>
                  <nts id="Seg_1288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_1290" n="HIAT:w" s="T343">kelen</ts>
                  <nts id="Seg_1291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_1293" n="HIAT:w" s="T344">ɨtana</ts>
                  <nts id="Seg_1294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_1296" n="HIAT:w" s="T345">olorbut</ts>
                  <nts id="Seg_1297" n="HIAT:ip">.</nts>
                  <nts id="Seg_1298" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T347" id="Seg_1300" n="HIAT:u" s="T346">
                  <ts e="T347" id="Seg_1302" n="HIAT:w" s="T346">Inʼete</ts>
                  <nts id="Seg_1303" n="HIAT:ip">:</nts>
                  <nts id="Seg_1304" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T349" id="Seg_1306" n="HIAT:u" s="T347">
                  <nts id="Seg_1307" n="HIAT:ip">"</nts>
                  <ts e="T348" id="Seg_1309" n="HIAT:w" s="T347">Tu͡ok</ts>
                  <nts id="Seg_1310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_1312" n="HIAT:w" s="T348">bu͡olluŋ</ts>
                  <nts id="Seg_1313" n="HIAT:ip">?</nts>
                  <nts id="Seg_1314" n="HIAT:ip">"</nts>
                  <nts id="Seg_1315" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T351" id="Seg_1317" n="HIAT:u" s="T349">
                  <ts e="T350" id="Seg_1319" n="HIAT:w" s="T349">Kepseːbit</ts>
                  <nts id="Seg_1320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_1322" n="HIAT:w" s="T350">enʼi</ts>
                  <nts id="Seg_1323" n="HIAT:ip">.</nts>
                  <nts id="Seg_1324" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T365" id="Seg_1326" n="HIAT:u" s="T351">
                  <nts id="Seg_1327" n="HIAT:ip">"</nts>
                  <ts e="T352" id="Seg_1329" n="HIAT:w" s="T351">Agaŋ</ts>
                  <nts id="Seg_1330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_1332" n="HIAT:w" s="T352">batɨjata</ts>
                  <nts id="Seg_1333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_1335" n="HIAT:w" s="T353">kanna</ts>
                  <nts id="Seg_1336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_1338" n="HIAT:w" s="T354">ire</ts>
                  <nts id="Seg_1339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_1341" n="HIAT:w" s="T355">baːr</ts>
                  <nts id="Seg_1342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_1344" n="HIAT:w" s="T356">ete</ts>
                  <nts id="Seg_1345" n="HIAT:ip">,</nts>
                  <nts id="Seg_1346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_1348" n="HIAT:w" s="T357">onu</ts>
                  <nts id="Seg_1349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_1351" n="HIAT:w" s="T358">ilʼlʼeːr</ts>
                  <nts id="Seg_1352" n="HIAT:ip">"</nts>
                  <nts id="Seg_1353" n="HIAT:ip">,</nts>
                  <nts id="Seg_1354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_1356" n="HIAT:w" s="T359">inʼete</ts>
                  <nts id="Seg_1357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_1359" n="HIAT:w" s="T360">muŋnaːk</ts>
                  <nts id="Seg_1360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_1362" n="HIAT:w" s="T361">kam</ts>
                  <nts id="Seg_1363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_1365" n="HIAT:w" s="T362">dʼebintijbit</ts>
                  <nts id="Seg_1366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_1368" n="HIAT:w" s="T363">batɨjanɨ</ts>
                  <nts id="Seg_1369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_1371" n="HIAT:w" s="T364">killerbit</ts>
                  <nts id="Seg_1372" n="HIAT:ip">.</nts>
                  <nts id="Seg_1373" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T371" id="Seg_1375" n="HIAT:u" s="T365">
                  <ts e="T366" id="Seg_1377" n="HIAT:w" s="T365">Bu</ts>
                  <nts id="Seg_1378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_1380" n="HIAT:w" s="T366">u͡ol</ts>
                  <nts id="Seg_1381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_1383" n="HIAT:w" s="T367">ör</ts>
                  <nts id="Seg_1384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_1386" n="HIAT:w" s="T368">bu͡olbakka</ts>
                  <nts id="Seg_1387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_1389" n="HIAT:w" s="T369">baran</ts>
                  <nts id="Seg_1390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_1392" n="HIAT:w" s="T370">kaːlbɨt</ts>
                  <nts id="Seg_1393" n="HIAT:ip">.</nts>
                  <nts id="Seg_1394" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T373" id="Seg_1396" n="HIAT:u" s="T371">
                  <ts e="T372" id="Seg_1398" n="HIAT:w" s="T371">Bɨstɨ͡a</ts>
                  <nts id="Seg_1399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_1401" n="HIAT:w" s="T372">duː</ts>
                  <nts id="Seg_1402" n="HIAT:ip">?</nts>
                  <nts id="Seg_1403" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T380" id="Seg_1405" n="HIAT:u" s="T373">
                  <ts e="T374" id="Seg_1407" n="HIAT:w" s="T373">Kelbite</ts>
                  <nts id="Seg_1408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_1410" n="HIAT:w" s="T374">kim</ts>
                  <nts id="Seg_1411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_1413" n="HIAT:w" s="T375">da</ts>
                  <nts id="Seg_1414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_1416" n="HIAT:w" s="T376">hu͡ok</ts>
                  <nts id="Seg_1417" n="HIAT:ip">,</nts>
                  <nts id="Seg_1418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378" id="Seg_1420" n="HIAT:w" s="T377">kihi</ts>
                  <nts id="Seg_1421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_1423" n="HIAT:w" s="T378">ire</ts>
                  <nts id="Seg_1424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_1426" n="HIAT:w" s="T379">oŋu͡oga</ts>
                  <nts id="Seg_1427" n="HIAT:ip">.</nts>
                  <nts id="Seg_1428" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T386" id="Seg_1430" n="HIAT:u" s="T380">
                  <ts e="T381" id="Seg_1432" n="HIAT:w" s="T380">Očihaga</ts>
                  <nts id="Seg_1433" n="HIAT:ip">,</nts>
                  <nts id="Seg_1434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T382" id="Seg_1436" n="HIAT:w" s="T381">im</ts>
                  <nts id="Seg_1437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_1439" n="HIAT:w" s="T382">tüspütün</ts>
                  <nts id="Seg_1440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_1442" n="HIAT:w" s="T383">kenne</ts>
                  <nts id="Seg_1443" n="HIAT:ip">,</nts>
                  <nts id="Seg_1444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_1446" n="HIAT:w" s="T384">hunuːrun</ts>
                  <nts id="Seg_1447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T386" id="Seg_1449" n="HIAT:w" s="T385">ubappɨt</ts>
                  <nts id="Seg_1450" n="HIAT:ip">.</nts>
                  <nts id="Seg_1451" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T393" id="Seg_1453" n="HIAT:u" s="T386">
                  <ts e="T387" id="Seg_1455" n="HIAT:w" s="T386">Kihi</ts>
                  <nts id="Seg_1456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_1458" n="HIAT:w" s="T387">utujar</ts>
                  <nts id="Seg_1459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_1461" n="HIAT:w" s="T388">kemin</ts>
                  <nts id="Seg_1462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_1464" n="HIAT:w" s="T389">dʼi͡ek</ts>
                  <nts id="Seg_1465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T391" id="Seg_1467" n="HIAT:w" s="T390">hɨrgalaːk</ts>
                  <nts id="Seg_1468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_1470" n="HIAT:w" s="T391">tɨ͡aha</ts>
                  <nts id="Seg_1471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T393" id="Seg_1473" n="HIAT:w" s="T392">ihillibit</ts>
                  <nts id="Seg_1474" n="HIAT:ip">.</nts>
                  <nts id="Seg_1475" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T396" id="Seg_1477" n="HIAT:u" s="T393">
                  <ts e="T394" id="Seg_1479" n="HIAT:w" s="T393">ɨt</ts>
                  <nts id="Seg_1480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_1482" n="HIAT:w" s="T394">da</ts>
                  <nts id="Seg_1483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T396" id="Seg_1485" n="HIAT:w" s="T395">ürbetek</ts>
                  <nts id="Seg_1486" n="HIAT:ip">.</nts>
                  <nts id="Seg_1487" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T400" id="Seg_1489" n="HIAT:u" s="T396">
                  <ts e="T397" id="Seg_1491" n="HIAT:w" s="T396">U͡otun</ts>
                  <nts id="Seg_1492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_1494" n="HIAT:w" s="T397">ututan</ts>
                  <nts id="Seg_1495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_1497" n="HIAT:w" s="T398">baran</ts>
                  <nts id="Seg_1498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T400" id="Seg_1500" n="HIAT:w" s="T399">olorbut</ts>
                  <nts id="Seg_1501" n="HIAT:ip">.</nts>
                  <nts id="Seg_1502" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T403" id="Seg_1504" n="HIAT:u" s="T400">
                  <ts e="T401" id="Seg_1506" n="HIAT:w" s="T400">Tɨ͡as</ts>
                  <nts id="Seg_1507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T402" id="Seg_1509" n="HIAT:w" s="T401">bögö</ts>
                  <nts id="Seg_1510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_1512" n="HIAT:w" s="T402">bu͡olbut</ts>
                  <nts id="Seg_1513" n="HIAT:ip">.</nts>
                  <nts id="Seg_1514" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T408" id="Seg_1516" n="HIAT:u" s="T403">
                  <ts e="T404" id="Seg_1518" n="HIAT:w" s="T403">Aːŋŋa</ts>
                  <nts id="Seg_1519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T405" id="Seg_1521" n="HIAT:w" s="T404">bappat</ts>
                  <nts id="Seg_1522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_1524" n="HIAT:w" s="T405">hu͡on</ts>
                  <nts id="Seg_1525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T407" id="Seg_1527" n="HIAT:w" s="T406">dʼaktar</ts>
                  <nts id="Seg_1528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T408" id="Seg_1530" n="HIAT:w" s="T407">kiːrbit</ts>
                  <nts id="Seg_1531" n="HIAT:ip">.</nts>
                  <nts id="Seg_1532" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T419" id="Seg_1534" n="HIAT:u" s="T408">
                  <ts e="T409" id="Seg_1536" n="HIAT:w" s="T408">Anaːr</ts>
                  <nts id="Seg_1537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_1539" n="HIAT:w" s="T409">atagɨn</ts>
                  <nts id="Seg_1540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T411" id="Seg_1542" n="HIAT:w" s="T410">aːŋŋa</ts>
                  <nts id="Seg_1543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T412" id="Seg_1545" n="HIAT:w" s="T411">annʼarɨn</ts>
                  <nts id="Seg_1546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T413" id="Seg_1548" n="HIAT:w" s="T412">gɨtta</ts>
                  <nts id="Seg_1549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414" id="Seg_1551" n="HIAT:w" s="T413">ikki</ts>
                  <nts id="Seg_1552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T415" id="Seg_1554" n="HIAT:w" s="T414">konnogun</ts>
                  <nts id="Seg_1555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T416" id="Seg_1557" n="HIAT:w" s="T415">annɨnan</ts>
                  <nts id="Seg_1558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T417" id="Seg_1560" n="HIAT:w" s="T416">batɨjanan</ts>
                  <nts id="Seg_1561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T418" id="Seg_1563" n="HIAT:w" s="T417">batarɨ</ts>
                  <nts id="Seg_1564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T419" id="Seg_1566" n="HIAT:w" s="T418">aspɨt</ts>
                  <nts id="Seg_1567" n="HIAT:ip">.</nts>
                  <nts id="Seg_1568" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T423" id="Seg_1570" n="HIAT:u" s="T419">
                  <ts e="T420" id="Seg_1572" n="HIAT:w" s="T419">Bejete</ts>
                  <nts id="Seg_1573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_1575" n="HIAT:w" s="T420">konnogun</ts>
                  <nts id="Seg_1576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T422" id="Seg_1578" n="HIAT:w" s="T421">annɨnan</ts>
                  <nts id="Seg_1579" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T423" id="Seg_1581" n="HIAT:w" s="T422">küreːbit</ts>
                  <nts id="Seg_1582" n="HIAT:ip">.</nts>
                  <nts id="Seg_1583" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T426" id="Seg_1585" n="HIAT:u" s="T423">
                  <ts e="T424" id="Seg_1587" n="HIAT:w" s="T423">Tüːn</ts>
                  <nts id="Seg_1588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T425" id="Seg_1590" n="HIAT:w" s="T424">dʼi͡etiger</ts>
                  <nts id="Seg_1591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T426" id="Seg_1593" n="HIAT:w" s="T425">kelbit</ts>
                  <nts id="Seg_1594" n="HIAT:ip">.</nts>
                  <nts id="Seg_1595" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T430" id="Seg_1597" n="HIAT:u" s="T426">
                  <ts e="T427" id="Seg_1599" n="HIAT:w" s="T426">Inʼete</ts>
                  <nts id="Seg_1600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T428" id="Seg_1602" n="HIAT:w" s="T427">muŋnaːk</ts>
                  <nts id="Seg_1603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T429" id="Seg_1605" n="HIAT:w" s="T428">utujbakka</ts>
                  <nts id="Seg_1606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_1608" n="HIAT:w" s="T429">küːten</ts>
                  <nts id="Seg_1609" n="HIAT:ip">.</nts>
                  <nts id="Seg_1610" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T435" id="Seg_1612" n="HIAT:u" s="T430">
                  <ts e="T431" id="Seg_1614" n="HIAT:w" s="T430">Harsi͡erde</ts>
                  <nts id="Seg_1615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_1617" n="HIAT:w" s="T431">inʼete</ts>
                  <nts id="Seg_1618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T433" id="Seg_1620" n="HIAT:w" s="T432">kineːske</ts>
                  <nts id="Seg_1621" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T434" id="Seg_1623" n="HIAT:w" s="T433">baran</ts>
                  <nts id="Seg_1624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T435" id="Seg_1626" n="HIAT:w" s="T434">kepseːbit</ts>
                  <nts id="Seg_1627" n="HIAT:ip">.</nts>
                  <nts id="Seg_1628" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T439" id="Seg_1630" n="HIAT:u" s="T435">
                  <ts e="T436" id="Seg_1632" n="HIAT:w" s="T435">Kineːs</ts>
                  <nts id="Seg_1633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T437" id="Seg_1635" n="HIAT:w" s="T436">bu</ts>
                  <nts id="Seg_1636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T438" id="Seg_1638" n="HIAT:w" s="T437">u͡olu</ts>
                  <nts id="Seg_1639" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T439" id="Seg_1641" n="HIAT:w" s="T438">ɨgɨrbɨt</ts>
                  <nts id="Seg_1642" n="HIAT:ip">.</nts>
                  <nts id="Seg_1643" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T441" id="Seg_1645" n="HIAT:u" s="T439">
                  <nts id="Seg_1646" n="HIAT:ip">"</nts>
                  <ts e="T440" id="Seg_1648" n="HIAT:w" s="T439">Dʼe</ts>
                  <nts id="Seg_1649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_1651" n="HIAT:w" s="T440">kajdagɨj</ts>
                  <nts id="Seg_1652" n="HIAT:ip">?</nts>
                  <nts id="Seg_1653" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T443" id="Seg_1655" n="HIAT:u" s="T441">
                  <ts e="T442" id="Seg_1657" n="HIAT:w" s="T441">Ölördüŋ</ts>
                  <nts id="Seg_1658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T443" id="Seg_1660" n="HIAT:w" s="T442">duː</ts>
                  <nts id="Seg_1661" n="HIAT:ip">?</nts>
                  <nts id="Seg_1662" n="HIAT:ip">"</nts>
                  <nts id="Seg_1663" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T449" id="Seg_1665" n="HIAT:u" s="T443">
                  <nts id="Seg_1666" n="HIAT:ip">"</nts>
                  <ts e="T444" id="Seg_1668" n="HIAT:w" s="T443">Bilbeppin</ts>
                  <nts id="Seg_1669" n="HIAT:ip">"</nts>
                  <nts id="Seg_1670" n="HIAT:ip">,</nts>
                  <nts id="Seg_1671" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T445" id="Seg_1673" n="HIAT:w" s="T444">di͡ebit</ts>
                  <nts id="Seg_1674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T446" id="Seg_1676" n="HIAT:w" s="T445">u͡ol</ts>
                  <nts id="Seg_1677" n="HIAT:ip">,</nts>
                  <nts id="Seg_1678" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T447" id="Seg_1680" n="HIAT:w" s="T446">kepseːbit</ts>
                  <nts id="Seg_1681" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T448" id="Seg_1683" n="HIAT:w" s="T447">kajtak</ts>
                  <nts id="Seg_1684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T449" id="Seg_1686" n="HIAT:w" s="T448">gɨmmɨtɨn</ts>
                  <nts id="Seg_1687" n="HIAT:ip">.</nts>
                  <nts id="Seg_1688" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T450" id="Seg_1690" n="HIAT:u" s="T449">
                  <ts e="T450" id="Seg_1692" n="HIAT:w" s="T449">Munnʼaktaːbɨttar</ts>
                  <nts id="Seg_1693" n="HIAT:ip">.</nts>
                  <nts id="Seg_1694" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T457" id="Seg_1696" n="HIAT:u" s="T450">
                  <ts e="T451" id="Seg_1698" n="HIAT:w" s="T450">Alta</ts>
                  <nts id="Seg_1699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T452" id="Seg_1701" n="HIAT:w" s="T451">kihini</ts>
                  <nts id="Seg_1702" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T453" id="Seg_1704" n="HIAT:w" s="T452">köllʼörö</ts>
                  <nts id="Seg_1705" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T454" id="Seg_1707" n="HIAT:w" s="T453">ɨːppɨttar</ts>
                  <nts id="Seg_1708" n="HIAT:ip">,</nts>
                  <nts id="Seg_1709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T455" id="Seg_1711" n="HIAT:w" s="T454">staršina</ts>
                  <nts id="Seg_1712" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T456" id="Seg_1714" n="HIAT:w" s="T455">emi͡e</ts>
                  <nts id="Seg_1715" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T457" id="Seg_1717" n="HIAT:w" s="T456">barsɨbɨt</ts>
                  <nts id="Seg_1718" n="HIAT:ip">.</nts>
                  <nts id="Seg_1719" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T461" id="Seg_1721" n="HIAT:u" s="T457">
                  <ts e="T458" id="Seg_1723" n="HIAT:w" s="T457">Tabannan</ts>
                  <nts id="Seg_1724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T459" id="Seg_1726" n="HIAT:w" s="T458">gɨtta</ts>
                  <nts id="Seg_1727" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T460" id="Seg_1729" n="HIAT:w" s="T459">atɨnan</ts>
                  <nts id="Seg_1730" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T461" id="Seg_1732" n="HIAT:w" s="T460">barbɨttar</ts>
                  <nts id="Seg_1733" n="HIAT:ip">.</nts>
                  <nts id="Seg_1734" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T464" id="Seg_1736" n="HIAT:u" s="T461">
                  <ts e="T462" id="Seg_1738" n="HIAT:w" s="T461">Tijbitter</ts>
                  <nts id="Seg_1739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T463" id="Seg_1741" n="HIAT:w" s="T462">künüs</ts>
                  <nts id="Seg_1742" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T464" id="Seg_1744" n="HIAT:w" s="T463">össü͡ö</ts>
                  <nts id="Seg_1745" n="HIAT:ip">.</nts>
                  <nts id="Seg_1746" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T468" id="Seg_1748" n="HIAT:u" s="T464">
                  <ts e="T465" id="Seg_1750" n="HIAT:w" s="T464">Biːr</ts>
                  <nts id="Seg_1751" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T466" id="Seg_1753" n="HIAT:w" s="T465">da</ts>
                  <nts id="Seg_1754" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T467" id="Seg_1756" n="HIAT:w" s="T466">kihi</ts>
                  <nts id="Seg_1757" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T468" id="Seg_1759" n="HIAT:w" s="T467">hu͡ok</ts>
                  <nts id="Seg_1760" n="HIAT:ip">.</nts>
                  <nts id="Seg_1761" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T474" id="Seg_1763" n="HIAT:u" s="T468">
                  <ts e="T469" id="Seg_1765" n="HIAT:w" s="T468">Dʼi͡elerin</ts>
                  <nts id="Seg_1766" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T470" id="Seg_1768" n="HIAT:w" s="T469">potolu͡oga</ts>
                  <nts id="Seg_1769" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T471" id="Seg_1771" n="HIAT:w" s="T470">kajdɨbɨt</ts>
                  <nts id="Seg_1772" n="HIAT:ip">,</nts>
                  <nts id="Seg_1773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T472" id="Seg_1775" n="HIAT:w" s="T471">aːna</ts>
                  <nts id="Seg_1776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T473" id="Seg_1778" n="HIAT:w" s="T472">arɨllan</ts>
                  <nts id="Seg_1779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T474" id="Seg_1781" n="HIAT:w" s="T473">turar</ts>
                  <nts id="Seg_1782" n="HIAT:ip">.</nts>
                  <nts id="Seg_1783" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T482" id="Seg_1785" n="HIAT:u" s="T474">
                  <ts e="T475" id="Seg_1787" n="HIAT:w" s="T474">Dʼi͡e</ts>
                  <nts id="Seg_1788" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T476" id="Seg_1790" n="HIAT:w" s="T475">ihin</ts>
                  <nts id="Seg_1791" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T477" id="Seg_1793" n="HIAT:w" s="T476">körbüttere</ts>
                  <nts id="Seg_1794" n="HIAT:ip">,</nts>
                  <nts id="Seg_1795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T478" id="Seg_1797" n="HIAT:w" s="T477">kɨːs</ts>
                  <nts id="Seg_1798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T479" id="Seg_1800" n="HIAT:w" s="T478">ihe</ts>
                  <nts id="Seg_1801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T480" id="Seg_1803" n="HIAT:w" s="T479">üllen</ts>
                  <nts id="Seg_1804" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T481" id="Seg_1806" n="HIAT:w" s="T480">baran</ts>
                  <nts id="Seg_1807" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T482" id="Seg_1809" n="HIAT:w" s="T481">hɨtar</ts>
                  <nts id="Seg_1810" n="HIAT:ip">.</nts>
                  <nts id="Seg_1811" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T483" id="Seg_1813" n="HIAT:u" s="T482">
                  <ts e="T483" id="Seg_1815" n="HIAT:w" s="T482">Staršina</ts>
                  <nts id="Seg_1816" n="HIAT:ip">:</nts>
                  <nts id="Seg_1817" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T485" id="Seg_1819" n="HIAT:u" s="T483">
                  <nts id="Seg_1820" n="HIAT:ip">"</nts>
                  <ts e="T484" id="Seg_1822" n="HIAT:w" s="T483">Dʼi͡eni</ts>
                  <nts id="Seg_1823" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T485" id="Seg_1825" n="HIAT:w" s="T484">ubatɨ͡agɨŋ</ts>
                  <nts id="Seg_1826" n="HIAT:ip">!</nts>
                  <nts id="Seg_1827" n="HIAT:ip">"</nts>
                  <nts id="Seg_1828" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T490" id="Seg_1830" n="HIAT:u" s="T485">
                  <ts e="T486" id="Seg_1832" n="HIAT:w" s="T485">Bu</ts>
                  <nts id="Seg_1833" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T487" id="Seg_1835" n="HIAT:w" s="T486">dʼi͡eni</ts>
                  <nts id="Seg_1836" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T488" id="Seg_1838" n="HIAT:w" s="T487">kɨːstɨːn</ts>
                  <nts id="Seg_1839" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T489" id="Seg_1841" n="HIAT:w" s="T488">ubatan</ts>
                  <nts id="Seg_1842" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T490" id="Seg_1844" n="HIAT:w" s="T489">keːspitter</ts>
                  <nts id="Seg_1845" n="HIAT:ip">.</nts>
                  <nts id="Seg_1846" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T497" id="Seg_1848" n="HIAT:u" s="T490">
                  <ts e="T491" id="Seg_1850" n="HIAT:w" s="T490">Tarakaːj</ts>
                  <nts id="Seg_1851" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T492" id="Seg_1853" n="HIAT:w" s="T491">u͡ol</ts>
                  <nts id="Seg_1854" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T493" id="Seg_1856" n="HIAT:w" s="T492">staršina</ts>
                  <nts id="Seg_1857" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T494" id="Seg_1859" n="HIAT:w" s="T493">kɨːhɨn</ts>
                  <nts id="Seg_1860" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T495" id="Seg_1862" n="HIAT:w" s="T494">ɨlan</ts>
                  <nts id="Seg_1863" n="HIAT:ip">,</nts>
                  <nts id="Seg_1864" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T496" id="Seg_1866" n="HIAT:w" s="T495">bajan-toton</ts>
                  <nts id="Seg_1867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T497" id="Seg_1869" n="HIAT:w" s="T496">olorbut</ts>
                  <nts id="Seg_1870" n="HIAT:ip">.</nts>
                  <nts id="Seg_1871" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T497" id="Seg_1872" n="sc" s="T0">
               <ts e="T1" id="Seg_1874" n="e" s="T0">Kineːhi </ts>
               <ts e="T2" id="Seg_1876" n="e" s="T1">gɨtta </ts>
               <ts e="T3" id="Seg_1878" n="e" s="T2">staršina </ts>
               <ts e="T4" id="Seg_1880" n="e" s="T3">biːrdiː </ts>
               <ts e="T5" id="Seg_1882" n="e" s="T4">u͡ollaːktar, </ts>
               <ts e="T6" id="Seg_1884" n="e" s="T5">olorbuttar </ts>
               <ts e="T7" id="Seg_1886" n="e" s="T6">ebit. </ts>
               <ts e="T8" id="Seg_1888" n="e" s="T7">Ikki͡ennere — </ts>
               <ts e="T9" id="Seg_1890" n="e" s="T8">tɨ͡a </ts>
               <ts e="T10" id="Seg_1892" n="e" s="T9">kihilere. </ts>
               <ts e="T11" id="Seg_1894" n="e" s="T10">U͡olattar </ts>
               <ts e="T12" id="Seg_1896" n="e" s="T11">biːrge </ts>
               <ts e="T13" id="Seg_1898" n="e" s="T12">ü͡öskeːn </ts>
               <ts e="T14" id="Seg_1900" n="e" s="T13">dogorduː </ts>
               <ts e="T15" id="Seg_1902" n="e" s="T14">bagajɨ </ts>
               <ts e="T16" id="Seg_1904" n="e" s="T15">ebittere. </ts>
               <ts e="T17" id="Seg_1906" n="e" s="T16">Bu </ts>
               <ts e="T18" id="Seg_1908" n="e" s="T17">kihiler </ts>
               <ts e="T19" id="Seg_1910" n="e" s="T18">bɨlɨrgɨttan </ts>
               <ts e="T20" id="Seg_1912" n="e" s="T19">köhö </ts>
               <ts e="T21" id="Seg_1914" n="e" s="T20">hɨldʼar </ts>
               <ts e="T22" id="Seg_1916" n="e" s="T21">ebittere. </ts>
               <ts e="T23" id="Seg_1918" n="e" s="T22">Araj </ts>
               <ts e="T24" id="Seg_1920" n="e" s="T23">kühün </ts>
               <ts e="T25" id="Seg_1922" n="e" s="T24">paːs </ts>
               <ts e="T26" id="Seg_1924" n="e" s="T25">iːter </ts>
               <ts e="T27" id="Seg_1926" n="e" s="T26">kem </ts>
               <ts e="T28" id="Seg_1928" n="e" s="T27">bu͡olbut. </ts>
               <ts e="T29" id="Seg_1930" n="e" s="T28">Astarɨn </ts>
               <ts e="T30" id="Seg_1932" n="e" s="T29">erdetten </ts>
               <ts e="T31" id="Seg_1934" n="e" s="T30">belemneːn </ts>
               <ts e="T32" id="Seg_1936" n="e" s="T31">keːspitter. </ts>
               <ts e="T33" id="Seg_1938" n="e" s="T32">Ikki </ts>
               <ts e="T34" id="Seg_1940" n="e" s="T33">u͡ol </ts>
               <ts e="T35" id="Seg_1942" n="e" s="T34">ɨtɨnan </ts>
               <ts e="T36" id="Seg_1944" n="e" s="T35">paːstɨː </ts>
               <ts e="T37" id="Seg_1946" n="e" s="T36">barbɨttar. </ts>
               <ts e="T38" id="Seg_1948" n="e" s="T37">Paːs </ts>
               <ts e="T39" id="Seg_1950" n="e" s="T38">iːtten-iːtten </ts>
               <ts e="T40" id="Seg_1952" n="e" s="T39">baran </ts>
               <ts e="T41" id="Seg_1954" n="e" s="T40">očihalarɨgar </ts>
               <ts e="T42" id="Seg_1956" n="e" s="T41">kommuttar. </ts>
               <ts e="T43" id="Seg_1958" n="e" s="T42">Biːrde </ts>
               <ts e="T44" id="Seg_1960" n="e" s="T43">ki͡ehe, </ts>
               <ts e="T45" id="Seg_1962" n="e" s="T44">ɨttarɨn </ts>
               <ts e="T46" id="Seg_1964" n="e" s="T45">baːjan </ts>
               <ts e="T47" id="Seg_1966" n="e" s="T46">baran, </ts>
               <ts e="T48" id="Seg_1968" n="e" s="T47">balɨk </ts>
               <ts e="T49" id="Seg_1970" n="e" s="T48">hɨ͡atɨnan </ts>
               <ts e="T50" id="Seg_1972" n="e" s="T49">eli͡eke </ts>
               <ts e="T51" id="Seg_1974" n="e" s="T50">oŋostubuttar, </ts>
               <ts e="T52" id="Seg_1976" n="e" s="T51">čaːj </ts>
               <ts e="T53" id="Seg_1978" n="e" s="T52">ihe </ts>
               <ts e="T54" id="Seg_1980" n="e" s="T53">olorbuttar. </ts>
               <ts e="T55" id="Seg_1982" n="e" s="T54">Araj </ts>
               <ts e="T56" id="Seg_1984" n="e" s="T55">hɨrgalaːk </ts>
               <ts e="T57" id="Seg_1986" n="e" s="T56">tɨ͡aha </ts>
               <ts e="T58" id="Seg_1988" n="e" s="T57">ihillibit. </ts>
               <ts e="T59" id="Seg_1990" n="e" s="T58">Kineːs </ts>
               <ts e="T60" id="Seg_1992" n="e" s="T59">u͡ola </ts>
               <ts e="T61" id="Seg_1994" n="e" s="T60">dʼi͡ebit: </ts>
               <ts e="T62" id="Seg_1996" n="e" s="T61">"Ütü͡ö </ts>
               <ts e="T63" id="Seg_1998" n="e" s="T62">kihi </ts>
               <ts e="T64" id="Seg_2000" n="e" s="T63">kelbete </ts>
               <ts e="T65" id="Seg_2002" n="e" s="T64">bu͡olu͡o </ts>
               <ts e="T66" id="Seg_2004" n="e" s="T65">du͡o?" </ts>
               <ts e="T67" id="Seg_2006" n="e" s="T66">Bejete </ts>
               <ts e="T68" id="Seg_2008" n="e" s="T67">tabaktɨː </ts>
               <ts e="T69" id="Seg_2010" n="e" s="T68">olorbut. </ts>
               <ts e="T70" id="Seg_2012" n="e" s="T69">Dogoro </ts>
               <ts e="T71" id="Seg_2014" n="e" s="T70">taraja </ts>
               <ts e="T72" id="Seg_2016" n="e" s="T71">hɨppɨt. </ts>
               <ts e="T73" id="Seg_2018" n="e" s="T72">Ihillemmittere — </ts>
               <ts e="T74" id="Seg_2020" n="e" s="T73">ɨttara </ts>
               <ts e="T75" id="Seg_2022" n="e" s="T74">ürbet </ts>
               <ts e="T76" id="Seg_2024" n="e" s="T75">bu͡olbut. </ts>
               <ts e="T77" id="Seg_2026" n="e" s="T76">Kaːmar </ts>
               <ts e="T78" id="Seg_2028" n="e" s="T77">tɨ͡as </ts>
               <ts e="T79" id="Seg_2030" n="e" s="T78">ihillibit. </ts>
               <ts e="T80" id="Seg_2032" n="e" s="T79">Kineːs </ts>
               <ts e="T81" id="Seg_2034" n="e" s="T80">u͡ola: </ts>
               <ts e="T82" id="Seg_2036" n="e" s="T81">"Kuttanabɨn", </ts>
               <ts e="T83" id="Seg_2038" n="e" s="T82">di͡ebit. </ts>
               <ts e="T84" id="Seg_2040" n="e" s="T83">Dogoro </ts>
               <ts e="T85" id="Seg_2042" n="e" s="T84">hol </ts>
               <ts e="T86" id="Seg_2044" n="e" s="T85">kördük </ts>
               <ts e="T87" id="Seg_2046" n="e" s="T86">taraja </ts>
               <ts e="T88" id="Seg_2048" n="e" s="T87">hɨppɨt. </ts>
               <ts e="T89" id="Seg_2050" n="e" s="T88">Aːn </ts>
               <ts e="T90" id="Seg_2052" n="e" s="T89">arɨllɨbɨt </ts>
               <ts e="T91" id="Seg_2054" n="e" s="T90">da </ts>
               <ts e="T92" id="Seg_2056" n="e" s="T91">hu͡on </ts>
               <ts e="T93" id="Seg_2058" n="e" s="T92">bagajɨ </ts>
               <ts e="T94" id="Seg_2060" n="e" s="T93">dʼaktar </ts>
               <ts e="T95" id="Seg_2062" n="e" s="T94">arɨččɨ </ts>
               <ts e="T96" id="Seg_2064" n="e" s="T95">batan </ts>
               <ts e="T97" id="Seg_2066" n="e" s="T96">kiːrbit, </ts>
               <ts e="T98" id="Seg_2068" n="e" s="T97">čöl </ts>
               <ts e="T99" id="Seg_2070" n="e" s="T98">oguru͡o </ts>
               <ts e="T100" id="Seg_2072" n="e" s="T99">taŋastaːk, </ts>
               <ts e="T101" id="Seg_2074" n="e" s="T100">oguru͡o </ts>
               <ts e="T102" id="Seg_2076" n="e" s="T101">bergeheleːk, </ts>
               <ts e="T103" id="Seg_2078" n="e" s="T102">baːj </ts>
               <ts e="T104" id="Seg_2080" n="e" s="T103">bagajɨ </ts>
               <ts e="T105" id="Seg_2082" n="e" s="T104">dʼühünneːk. </ts>
               <ts e="T106" id="Seg_2084" n="e" s="T105">Staršina </ts>
               <ts e="T107" id="Seg_2086" n="e" s="T106">u͡ola </ts>
               <ts e="T108" id="Seg_2088" n="e" s="T107">dogorun: </ts>
               <ts e="T109" id="Seg_2090" n="e" s="T108">"Čaːjdaː </ts>
               <ts e="T110" id="Seg_2092" n="e" s="T109">ɨ͡aldʼɨtɨ", </ts>
               <ts e="T111" id="Seg_2094" n="e" s="T110">dʼi͡ebit. </ts>
               <ts e="T112" id="Seg_2096" n="e" s="T111">Kineːs </ts>
               <ts e="T113" id="Seg_2098" n="e" s="T112">u͡ola </ts>
               <ts e="T114" id="Seg_2100" n="e" s="T113">turan </ts>
               <ts e="T115" id="Seg_2102" n="e" s="T114">ohok </ts>
               <ts e="T116" id="Seg_2104" n="e" s="T115">ottummut, </ts>
               <ts e="T117" id="Seg_2106" n="e" s="T116">čaːj </ts>
               <ts e="T118" id="Seg_2108" n="e" s="T117">kɨːjnʼarbat. </ts>
               <ts e="T119" id="Seg_2110" n="e" s="T118">Araj </ts>
               <ts e="T120" id="Seg_2112" n="e" s="T119">dʼaktardara </ts>
               <ts e="T121" id="Seg_2114" n="e" s="T120">hɨrajɨn </ts>
               <ts e="T122" id="Seg_2116" n="e" s="T121">köllʼörböt, </ts>
               <ts e="T123" id="Seg_2118" n="e" s="T122">haŋarsɨbat. </ts>
               <ts e="T124" id="Seg_2120" n="e" s="T123">Bu </ts>
               <ts e="T125" id="Seg_2122" n="e" s="T124">u͡ol </ts>
               <ts e="T126" id="Seg_2124" n="e" s="T125">balɨk </ts>
               <ts e="T127" id="Seg_2126" n="e" s="T126">kü͡öhe </ts>
               <ts e="T128" id="Seg_2128" n="e" s="T127">hɨ͡alammɨt </ts>
               <ts e="T129" id="Seg_2130" n="e" s="T128">ispiːčke </ts>
               <ts e="T130" id="Seg_2132" n="e" s="T129">mahɨnan </ts>
               <ts e="T131" id="Seg_2134" n="e" s="T130">karagɨn </ts>
               <ts e="T132" id="Seg_2136" n="e" s="T131">tepteren </ts>
               <ts e="T133" id="Seg_2138" n="e" s="T132">baran </ts>
               <ts e="T134" id="Seg_2140" n="e" s="T133">körbüte — </ts>
               <ts e="T135" id="Seg_2142" n="e" s="T134">dʼaktar </ts>
               <ts e="T136" id="Seg_2144" n="e" s="T135">küle </ts>
               <ts e="T137" id="Seg_2146" n="e" s="T136">oloror, </ts>
               <ts e="T138" id="Seg_2148" n="e" s="T137">hogotok </ts>
               <ts e="T139" id="Seg_2150" n="e" s="T138">ire </ts>
               <ts e="T140" id="Seg_2152" n="e" s="T139">tiːsteːk </ts>
               <ts e="T141" id="Seg_2154" n="e" s="T140">ebit. </ts>
               <ts e="T142" id="Seg_2156" n="e" s="T141">Mas </ts>
               <ts e="T143" id="Seg_2158" n="e" s="T142">killere </ts>
               <ts e="T144" id="Seg_2160" n="e" s="T143">taksɨbɨt, </ts>
               <ts e="T145" id="Seg_2162" n="e" s="T144">ɨttarɨn </ts>
               <ts e="T146" id="Seg_2164" n="e" s="T145">kölüjteleːn </ts>
               <ts e="T147" id="Seg_2166" n="e" s="T146">keːspit. </ts>
               <ts e="T148" id="Seg_2168" n="e" s="T147">Körbüte — </ts>
               <ts e="T149" id="Seg_2170" n="e" s="T148">dʼaktar </ts>
               <ts e="T150" id="Seg_2172" n="e" s="T149">hɨrgatɨgar </ts>
               <ts e="T151" id="Seg_2174" n="e" s="T150">alta </ts>
               <ts e="T152" id="Seg_2176" n="e" s="T151">ɨt </ts>
               <ts e="T153" id="Seg_2178" n="e" s="T152">kölüllübüt. </ts>
               <ts e="T154" id="Seg_2180" n="e" s="T153">Kiːren </ts>
               <ts e="T155" id="Seg_2182" n="e" s="T154">baran </ts>
               <ts e="T156" id="Seg_2184" n="e" s="T155">ɨ͡aldʼɨtɨn </ts>
               <ts e="T157" id="Seg_2186" n="e" s="T156">ahappɨt. </ts>
               <ts e="T158" id="Seg_2188" n="e" s="T157">"Buːs </ts>
               <ts e="T159" id="Seg_2190" n="e" s="T158">killeste </ts>
               <ts e="T160" id="Seg_2192" n="e" s="T159">taksɨ͡am", </ts>
               <ts e="T161" id="Seg_2194" n="e" s="T160">dʼi͡en </ts>
               <ts e="T162" id="Seg_2196" n="e" s="T161">baran </ts>
               <ts e="T163" id="Seg_2198" n="e" s="T162">tanastarɨn </ts>
               <ts e="T164" id="Seg_2200" n="e" s="T163">kɨbɨnan </ts>
               <ts e="T165" id="Seg_2202" n="e" s="T164">tahaːrbɨt, </ts>
               <ts e="T166" id="Seg_2204" n="e" s="T165">ɨtɨn </ts>
               <ts e="T167" id="Seg_2206" n="e" s="T166">hɨrgatɨgar </ts>
               <ts e="T168" id="Seg_2208" n="e" s="T167">uːran </ts>
               <ts e="T169" id="Seg_2210" n="e" s="T168">keːspit. </ts>
               <ts e="T170" id="Seg_2212" n="e" s="T169">Onton </ts>
               <ts e="T171" id="Seg_2214" n="e" s="T170">kiːrbit, </ts>
               <ts e="T172" id="Seg_2216" n="e" s="T171">dʼaktarɨ </ts>
               <ts e="T173" id="Seg_2218" n="e" s="T172">olorbut </ts>
               <ts e="T174" id="Seg_2220" n="e" s="T173">hiriger </ts>
               <ts e="T175" id="Seg_2222" n="e" s="T174">utupput, </ts>
               <ts e="T176" id="Seg_2224" n="e" s="T175">tiriː </ts>
               <ts e="T177" id="Seg_2226" n="e" s="T176">bi͡erbit </ts>
               <ts e="T178" id="Seg_2228" n="e" s="T177">telgeni͡en. </ts>
               <ts e="T179" id="Seg_2230" n="e" s="T178">Balɨk </ts>
               <ts e="T180" id="Seg_2232" n="e" s="T179">hɨ͡ata </ts>
               <ts e="T181" id="Seg_2234" n="e" s="T180">hunuːrdarɨn </ts>
               <ts e="T182" id="Seg_2236" n="e" s="T181">ututan </ts>
               <ts e="T183" id="Seg_2238" n="e" s="T182">keːspitter. </ts>
               <ts e="T184" id="Seg_2240" n="e" s="T183">Utujbuttar. </ts>
               <ts e="T185" id="Seg_2242" n="e" s="T184">Staršina </ts>
               <ts e="T186" id="Seg_2244" n="e" s="T185">u͡ola </ts>
               <ts e="T187" id="Seg_2246" n="e" s="T186">dogorun </ts>
               <ts e="T188" id="Seg_2248" n="e" s="T187">eppit: </ts>
               <ts e="T189" id="Seg_2250" n="e" s="T188">"Doː, </ts>
               <ts e="T190" id="Seg_2252" n="e" s="T189">mantɨbɨt </ts>
               <ts e="T191" id="Seg_2254" n="e" s="T190">kɨːs </ts>
               <ts e="T192" id="Seg_2256" n="e" s="T191">bu͡olbataːk? </ts>
               <ts e="T193" id="Seg_2258" n="e" s="T192">Kɨːstɨ͡ak. </ts>
               <ts e="T194" id="Seg_2260" n="e" s="T193">Tugun </ts>
               <ts e="T195" id="Seg_2262" n="e" s="T194">kuttanagɨn? </ts>
               <ts e="T196" id="Seg_2264" n="e" s="T195">Min </ts>
               <ts e="T197" id="Seg_2266" n="e" s="T196">oččogo </ts>
               <ts e="T198" id="Seg_2268" n="e" s="T197">barɨ͡am." </ts>
               <ts e="T199" id="Seg_2270" n="e" s="T198">Dogoro: </ts>
               <ts e="T200" id="Seg_2272" n="e" s="T199">"Min </ts>
               <ts e="T201" id="Seg_2274" n="e" s="T200">taksɨbɨtɨm </ts>
               <ts e="T202" id="Seg_2276" n="e" s="T201">kenne </ts>
               <ts e="T203" id="Seg_2278" n="e" s="T202">kɨːskar </ts>
               <ts e="T204" id="Seg_2280" n="e" s="T203">baraːr." </ts>
               <ts e="T205" id="Seg_2282" n="e" s="T204">Taksan </ts>
               <ts e="T206" id="Seg_2284" n="e" s="T205">kaːlbɨt. </ts>
               <ts e="T207" id="Seg_2286" n="e" s="T206">Taŋnan </ts>
               <ts e="T208" id="Seg_2288" n="e" s="T207">baran </ts>
               <ts e="T209" id="Seg_2290" n="e" s="T208">hɨrgatɨgar </ts>
               <ts e="T210" id="Seg_2292" n="e" s="T209">olorbut. </ts>
               <ts e="T211" id="Seg_2294" n="e" s="T210">Töhö </ts>
               <ts e="T212" id="Seg_2296" n="e" s="T211">bu͡olu͡oj, </ts>
               <ts e="T213" id="Seg_2298" n="e" s="T212">ölör-kaːlar </ts>
               <ts e="T214" id="Seg_2300" n="e" s="T213">üːgü </ts>
               <ts e="T215" id="Seg_2302" n="e" s="T214">ihillibit: </ts>
               <ts e="T216" id="Seg_2304" n="e" s="T215">—Araː! </ts>
               <ts e="T217" id="Seg_2306" n="e" s="T216">Abɨraː!" </ts>
               <ts e="T218" id="Seg_2308" n="e" s="T217">Bu </ts>
               <ts e="T219" id="Seg_2310" n="e" s="T218">u͡ol </ts>
               <ts e="T220" id="Seg_2312" n="e" s="T219">nʼu͡oŋutun </ts>
               <ts e="T221" id="Seg_2314" n="e" s="T220">ɨlla </ts>
               <ts e="T222" id="Seg_2316" n="e" s="T221">daːganɨ </ts>
               <ts e="T223" id="Seg_2318" n="e" s="T222">kötüten </ts>
               <ts e="T224" id="Seg_2320" n="e" s="T223">kaːlbɨt. </ts>
               <ts e="T225" id="Seg_2322" n="e" s="T224">Kennitten </ts>
               <ts e="T226" id="Seg_2324" n="e" s="T225">dʼaktar </ts>
               <ts e="T227" id="Seg_2326" n="e" s="T226">haŋata </ts>
               <ts e="T228" id="Seg_2328" n="e" s="T227">ihillibit: </ts>
               <ts e="T229" id="Seg_2330" n="e" s="T228">"Bilbitim </ts>
               <ts e="T230" id="Seg_2332" n="e" s="T229">bu͡ollar </ts>
               <ts e="T231" id="Seg_2334" n="e" s="T230">hi͡en </ts>
               <ts e="T232" id="Seg_2336" n="e" s="T231">keːhi͡e </ts>
               <ts e="T233" id="Seg_2338" n="e" s="T232">etim </ts>
               <ts e="T234" id="Seg_2340" n="e" s="T233">bu͡o!" </ts>
               <ts e="T235" id="Seg_2342" n="e" s="T234">Kelen </ts>
               <ts e="T236" id="Seg_2344" n="e" s="T235">bu </ts>
               <ts e="T237" id="Seg_2346" n="e" s="T236">u͡ol </ts>
               <ts e="T238" id="Seg_2348" n="e" s="T237">agatɨgar, </ts>
               <ts e="T239" id="Seg_2350" n="e" s="T238">kineːske, </ts>
               <ts e="T240" id="Seg_2352" n="e" s="T239">kepseːbit. </ts>
               <ts e="T241" id="Seg_2354" n="e" s="T240">Kineːhi </ts>
               <ts e="T242" id="Seg_2356" n="e" s="T241">gɨtta </ts>
               <ts e="T243" id="Seg_2358" n="e" s="T242">staršina </ts>
               <ts e="T244" id="Seg_2360" n="e" s="T243">hette </ts>
               <ts e="T245" id="Seg_2362" n="e" s="T244">dʼi͡eni </ts>
               <ts e="T246" id="Seg_2364" n="e" s="T245">komujan </ts>
               <ts e="T247" id="Seg_2366" n="e" s="T246">munnʼaktaːbɨttar. </ts>
               <ts e="T248" id="Seg_2368" n="e" s="T247">Ikki </ts>
               <ts e="T249" id="Seg_2370" n="e" s="T248">u͡olu </ts>
               <ts e="T250" id="Seg_2372" n="e" s="T249">ɨːppɨttar </ts>
               <ts e="T251" id="Seg_2374" n="e" s="T250">körü͡ökterin. </ts>
               <ts e="T252" id="Seg_2376" n="e" s="T251">Ikki </ts>
               <ts e="T253" id="Seg_2378" n="e" s="T252">u͡ol </ts>
               <ts e="T254" id="Seg_2380" n="e" s="T253">bu͡olan </ts>
               <ts e="T255" id="Seg_2382" n="e" s="T254">baran </ts>
               <ts e="T256" id="Seg_2384" n="e" s="T255">kaːllɨlar. </ts>
               <ts e="T257" id="Seg_2386" n="e" s="T256">Ör </ts>
               <ts e="T258" id="Seg_2388" n="e" s="T257">bu͡olu͡oktaraj, </ts>
               <ts e="T259" id="Seg_2390" n="e" s="T258">hɨrdɨkka </ts>
               <ts e="T260" id="Seg_2392" n="e" s="T259">kelbitter. </ts>
               <ts e="T261" id="Seg_2394" n="e" s="T260">Körbüttere — </ts>
               <ts e="T262" id="Seg_2396" n="e" s="T261">biːr </ts>
               <ts e="T263" id="Seg_2398" n="e" s="T262">da </ts>
               <ts e="T264" id="Seg_2400" n="e" s="T263">kihi </ts>
               <ts e="T265" id="Seg_2402" n="e" s="T264">hu͡ok, </ts>
               <ts e="T266" id="Seg_2404" n="e" s="T265">hu͡ol </ts>
               <ts e="T267" id="Seg_2406" n="e" s="T266">da </ts>
               <ts e="T268" id="Seg_2408" n="e" s="T267">hu͡ok. </ts>
               <ts e="T269" id="Seg_2410" n="e" s="T268">Uraha </ts>
               <ts e="T270" id="Seg_2412" n="e" s="T269">dʼi͡ege </ts>
               <ts e="T271" id="Seg_2414" n="e" s="T270">kiːrbittere — </ts>
               <ts e="T272" id="Seg_2416" n="e" s="T271">u͡ol </ts>
               <ts e="T273" id="Seg_2418" n="e" s="T272">oŋu͡oga </ts>
               <ts e="T274" id="Seg_2420" n="e" s="T273">ire </ts>
               <ts e="T275" id="Seg_2422" n="e" s="T274">dardaja </ts>
               <ts e="T276" id="Seg_2424" n="e" s="T275">hɨtar. </ts>
               <ts e="T277" id="Seg_2426" n="e" s="T276">Bu </ts>
               <ts e="T278" id="Seg_2428" n="e" s="T277">ikki </ts>
               <ts e="T279" id="Seg_2430" n="e" s="T278">u͡ol </ts>
               <ts e="T280" id="Seg_2432" n="e" s="T279">tönnübütter. </ts>
               <ts e="T281" id="Seg_2434" n="e" s="T280">Kineːske </ts>
               <ts e="T282" id="Seg_2436" n="e" s="T281">kepseːbitter. </ts>
               <ts e="T283" id="Seg_2438" n="e" s="T282">Biːr </ts>
               <ts e="T284" id="Seg_2440" n="e" s="T283">tulaːjak </ts>
               <ts e="T285" id="Seg_2442" n="e" s="T284">ogo </ts>
               <ts e="T286" id="Seg_2444" n="e" s="T285">baːr, </ts>
               <ts e="T287" id="Seg_2446" n="e" s="T286">ikki </ts>
               <ts e="T288" id="Seg_2448" n="e" s="T287">hannɨttan </ts>
               <ts e="T289" id="Seg_2450" n="e" s="T288">tarakaːj, </ts>
               <ts e="T290" id="Seg_2452" n="e" s="T289">hogotok </ts>
               <ts e="T291" id="Seg_2454" n="e" s="T290">inʼeleːk, </ts>
               <ts e="T292" id="Seg_2456" n="e" s="T291">biːr </ts>
               <ts e="T293" id="Seg_2458" n="e" s="T292">tabalaːk. </ts>
               <ts e="T294" id="Seg_2460" n="e" s="T293">Kineːhi </ts>
               <ts e="T295" id="Seg_2462" n="e" s="T294">gɨtta </ts>
               <ts e="T296" id="Seg_2464" n="e" s="T295">staršina </ts>
               <ts e="T297" id="Seg_2466" n="e" s="T296">munnʼaktaːbɨttar. </ts>
               <ts e="T298" id="Seg_2468" n="e" s="T297">"Kajdak </ts>
               <ts e="T299" id="Seg_2470" n="e" s="T298">bu͡olu͡okputuj, </ts>
               <ts e="T300" id="Seg_2472" n="e" s="T299">hirbitin </ts>
               <ts e="T301" id="Seg_2474" n="e" s="T300">bɨragɨ͡akpɨt </ts>
               <ts e="T302" id="Seg_2476" n="e" s="T301">duː? </ts>
               <ts e="T303" id="Seg_2478" n="e" s="T302">Emi͡e </ts>
               <ts e="T304" id="Seg_2480" n="e" s="T303">da </ts>
               <ts e="T305" id="Seg_2482" n="e" s="T304">paːstarbɨt </ts>
               <ts e="T307" id="Seg_2484" n="e" s="T305">baːllar", </ts>
               <ts e="T308" id="Seg_2486" n="e" s="T307">despitter. </ts>
               <ts e="T309" id="Seg_2488" n="e" s="T308">Staršina </ts>
               <ts e="T310" id="Seg_2490" n="e" s="T309">onno </ts>
               <ts e="T311" id="Seg_2492" n="e" s="T310">turan </ts>
               <ts e="T312" id="Seg_2494" n="e" s="T311">eppit: </ts>
               <ts e="T313" id="Seg_2496" n="e" s="T312">"Tulaːjak </ts>
               <ts e="T314" id="Seg_2498" n="e" s="T313">ogonu </ts>
               <ts e="T315" id="Seg_2500" n="e" s="T314">ɨːtɨ͡akka, </ts>
               <ts e="T316" id="Seg_2502" n="e" s="T315">min </ts>
               <ts e="T317" id="Seg_2504" n="e" s="T316">kɨːspɨn </ts>
               <ts e="T318" id="Seg_2506" n="e" s="T317">da </ts>
               <ts e="T319" id="Seg_2508" n="e" s="T318">bi͡eri͡em </ts>
               <ts e="T320" id="Seg_2510" n="e" s="T319">ete, </ts>
               <ts e="T321" id="Seg_2512" n="e" s="T320">ölördögüne." </ts>
               <ts e="T322" id="Seg_2514" n="e" s="T321">Bu </ts>
               <ts e="T323" id="Seg_2516" n="e" s="T322">u͡olu </ts>
               <ts e="T324" id="Seg_2518" n="e" s="T323">ɨgɨrbɨttar, </ts>
               <ts e="T325" id="Seg_2520" n="e" s="T324">kepseːbitter </ts>
               <ts e="T326" id="Seg_2522" n="e" s="T325">barɨtɨn. </ts>
               <ts e="T327" id="Seg_2524" n="e" s="T326">Dʼe </ts>
               <ts e="T328" id="Seg_2526" n="e" s="T327">üleger </ts>
               <ts e="T329" id="Seg_2528" n="e" s="T328">ebit. </ts>
               <ts e="T330" id="Seg_2530" n="e" s="T329">Ogo </ts>
               <ts e="T331" id="Seg_2532" n="e" s="T330">muŋnaːk </ts>
               <ts e="T332" id="Seg_2534" n="e" s="T331">hogotogun </ts>
               <ts e="T333" id="Seg_2536" n="e" s="T332">barar </ts>
               <ts e="T334" id="Seg_2538" n="e" s="T333">bu͡olla, </ts>
               <ts e="T335" id="Seg_2540" n="e" s="T334">ɨt </ts>
               <ts e="T336" id="Seg_2542" n="e" s="T335">ire </ts>
               <ts e="T337" id="Seg_2544" n="e" s="T336">bi͡erbitter, </ts>
               <ts e="T338" id="Seg_2546" n="e" s="T337">haː </ts>
               <ts e="T339" id="Seg_2548" n="e" s="T338">da </ts>
               <ts e="T340" id="Seg_2550" n="e" s="T339">tu͡ok </ts>
               <ts e="T341" id="Seg_2552" n="e" s="T340">da </ts>
               <ts e="T342" id="Seg_2554" n="e" s="T341">hu͡ok. </ts>
               <ts e="T343" id="Seg_2556" n="e" s="T342">Dʼi͡etiger </ts>
               <ts e="T344" id="Seg_2558" n="e" s="T343">kelen </ts>
               <ts e="T345" id="Seg_2560" n="e" s="T344">ɨtana </ts>
               <ts e="T346" id="Seg_2562" n="e" s="T345">olorbut. </ts>
               <ts e="T347" id="Seg_2564" n="e" s="T346">Inʼete: </ts>
               <ts e="T348" id="Seg_2566" n="e" s="T347">"Tu͡ok </ts>
               <ts e="T349" id="Seg_2568" n="e" s="T348">bu͡olluŋ?" </ts>
               <ts e="T350" id="Seg_2570" n="e" s="T349">Kepseːbit </ts>
               <ts e="T351" id="Seg_2572" n="e" s="T350">enʼi. </ts>
               <ts e="T352" id="Seg_2574" n="e" s="T351">"Agaŋ </ts>
               <ts e="T353" id="Seg_2576" n="e" s="T352">batɨjata </ts>
               <ts e="T354" id="Seg_2578" n="e" s="T353">kanna </ts>
               <ts e="T355" id="Seg_2580" n="e" s="T354">ire </ts>
               <ts e="T356" id="Seg_2582" n="e" s="T355">baːr </ts>
               <ts e="T357" id="Seg_2584" n="e" s="T356">ete, </ts>
               <ts e="T358" id="Seg_2586" n="e" s="T357">onu </ts>
               <ts e="T359" id="Seg_2588" n="e" s="T358">ilʼlʼeːr", </ts>
               <ts e="T360" id="Seg_2590" n="e" s="T359">inʼete </ts>
               <ts e="T361" id="Seg_2592" n="e" s="T360">muŋnaːk </ts>
               <ts e="T362" id="Seg_2594" n="e" s="T361">kam </ts>
               <ts e="T363" id="Seg_2596" n="e" s="T362">dʼebintijbit </ts>
               <ts e="T364" id="Seg_2598" n="e" s="T363">batɨjanɨ </ts>
               <ts e="T365" id="Seg_2600" n="e" s="T364">killerbit. </ts>
               <ts e="T366" id="Seg_2602" n="e" s="T365">Bu </ts>
               <ts e="T367" id="Seg_2604" n="e" s="T366">u͡ol </ts>
               <ts e="T368" id="Seg_2606" n="e" s="T367">ör </ts>
               <ts e="T369" id="Seg_2608" n="e" s="T368">bu͡olbakka </ts>
               <ts e="T370" id="Seg_2610" n="e" s="T369">baran </ts>
               <ts e="T371" id="Seg_2612" n="e" s="T370">kaːlbɨt. </ts>
               <ts e="T372" id="Seg_2614" n="e" s="T371">Bɨstɨ͡a </ts>
               <ts e="T373" id="Seg_2616" n="e" s="T372">duː? </ts>
               <ts e="T374" id="Seg_2618" n="e" s="T373">Kelbite </ts>
               <ts e="T375" id="Seg_2620" n="e" s="T374">kim </ts>
               <ts e="T376" id="Seg_2622" n="e" s="T375">da </ts>
               <ts e="T377" id="Seg_2624" n="e" s="T376">hu͡ok, </ts>
               <ts e="T378" id="Seg_2626" n="e" s="T377">kihi </ts>
               <ts e="T379" id="Seg_2628" n="e" s="T378">ire </ts>
               <ts e="T380" id="Seg_2630" n="e" s="T379">oŋu͡oga. </ts>
               <ts e="T381" id="Seg_2632" n="e" s="T380">Očihaga, </ts>
               <ts e="T382" id="Seg_2634" n="e" s="T381">im </ts>
               <ts e="T383" id="Seg_2636" n="e" s="T382">tüspütün </ts>
               <ts e="T384" id="Seg_2638" n="e" s="T383">kenne, </ts>
               <ts e="T385" id="Seg_2640" n="e" s="T384">hunuːrun </ts>
               <ts e="T386" id="Seg_2642" n="e" s="T385">ubappɨt. </ts>
               <ts e="T387" id="Seg_2644" n="e" s="T386">Kihi </ts>
               <ts e="T388" id="Seg_2646" n="e" s="T387">utujar </ts>
               <ts e="T389" id="Seg_2648" n="e" s="T388">kemin </ts>
               <ts e="T390" id="Seg_2650" n="e" s="T389">dʼi͡ek </ts>
               <ts e="T391" id="Seg_2652" n="e" s="T390">hɨrgalaːk </ts>
               <ts e="T392" id="Seg_2654" n="e" s="T391">tɨ͡aha </ts>
               <ts e="T393" id="Seg_2656" n="e" s="T392">ihillibit. </ts>
               <ts e="T394" id="Seg_2658" n="e" s="T393">ɨt </ts>
               <ts e="T395" id="Seg_2660" n="e" s="T394">da </ts>
               <ts e="T396" id="Seg_2662" n="e" s="T395">ürbetek. </ts>
               <ts e="T397" id="Seg_2664" n="e" s="T396">U͡otun </ts>
               <ts e="T398" id="Seg_2666" n="e" s="T397">ututan </ts>
               <ts e="T399" id="Seg_2668" n="e" s="T398">baran </ts>
               <ts e="T400" id="Seg_2670" n="e" s="T399">olorbut. </ts>
               <ts e="T401" id="Seg_2672" n="e" s="T400">Tɨ͡as </ts>
               <ts e="T402" id="Seg_2674" n="e" s="T401">bögö </ts>
               <ts e="T403" id="Seg_2676" n="e" s="T402">bu͡olbut. </ts>
               <ts e="T404" id="Seg_2678" n="e" s="T403">Aːŋŋa </ts>
               <ts e="T405" id="Seg_2680" n="e" s="T404">bappat </ts>
               <ts e="T406" id="Seg_2682" n="e" s="T405">hu͡on </ts>
               <ts e="T407" id="Seg_2684" n="e" s="T406">dʼaktar </ts>
               <ts e="T408" id="Seg_2686" n="e" s="T407">kiːrbit. </ts>
               <ts e="T409" id="Seg_2688" n="e" s="T408">Anaːr </ts>
               <ts e="T410" id="Seg_2690" n="e" s="T409">atagɨn </ts>
               <ts e="T411" id="Seg_2692" n="e" s="T410">aːŋŋa </ts>
               <ts e="T412" id="Seg_2694" n="e" s="T411">annʼarɨn </ts>
               <ts e="T413" id="Seg_2696" n="e" s="T412">gɨtta </ts>
               <ts e="T414" id="Seg_2698" n="e" s="T413">ikki </ts>
               <ts e="T415" id="Seg_2700" n="e" s="T414">konnogun </ts>
               <ts e="T416" id="Seg_2702" n="e" s="T415">annɨnan </ts>
               <ts e="T417" id="Seg_2704" n="e" s="T416">batɨjanan </ts>
               <ts e="T418" id="Seg_2706" n="e" s="T417">batarɨ </ts>
               <ts e="T419" id="Seg_2708" n="e" s="T418">aspɨt. </ts>
               <ts e="T420" id="Seg_2710" n="e" s="T419">Bejete </ts>
               <ts e="T421" id="Seg_2712" n="e" s="T420">konnogun </ts>
               <ts e="T422" id="Seg_2714" n="e" s="T421">annɨnan </ts>
               <ts e="T423" id="Seg_2716" n="e" s="T422">küreːbit. </ts>
               <ts e="T424" id="Seg_2718" n="e" s="T423">Tüːn </ts>
               <ts e="T425" id="Seg_2720" n="e" s="T424">dʼi͡etiger </ts>
               <ts e="T426" id="Seg_2722" n="e" s="T425">kelbit. </ts>
               <ts e="T427" id="Seg_2724" n="e" s="T426">Inʼete </ts>
               <ts e="T428" id="Seg_2726" n="e" s="T427">muŋnaːk </ts>
               <ts e="T429" id="Seg_2728" n="e" s="T428">utujbakka </ts>
               <ts e="T430" id="Seg_2730" n="e" s="T429">küːten. </ts>
               <ts e="T431" id="Seg_2732" n="e" s="T430">Harsi͡erde </ts>
               <ts e="T432" id="Seg_2734" n="e" s="T431">inʼete </ts>
               <ts e="T433" id="Seg_2736" n="e" s="T432">kineːske </ts>
               <ts e="T434" id="Seg_2738" n="e" s="T433">baran </ts>
               <ts e="T435" id="Seg_2740" n="e" s="T434">kepseːbit. </ts>
               <ts e="T436" id="Seg_2742" n="e" s="T435">Kineːs </ts>
               <ts e="T437" id="Seg_2744" n="e" s="T436">bu </ts>
               <ts e="T438" id="Seg_2746" n="e" s="T437">u͡olu </ts>
               <ts e="T439" id="Seg_2748" n="e" s="T438">ɨgɨrbɨt. </ts>
               <ts e="T440" id="Seg_2750" n="e" s="T439">"Dʼe </ts>
               <ts e="T441" id="Seg_2752" n="e" s="T440">kajdagɨj? </ts>
               <ts e="T442" id="Seg_2754" n="e" s="T441">Ölördüŋ </ts>
               <ts e="T443" id="Seg_2756" n="e" s="T442">duː?" </ts>
               <ts e="T444" id="Seg_2758" n="e" s="T443">"Bilbeppin", </ts>
               <ts e="T445" id="Seg_2760" n="e" s="T444">di͡ebit </ts>
               <ts e="T446" id="Seg_2762" n="e" s="T445">u͡ol, </ts>
               <ts e="T447" id="Seg_2764" n="e" s="T446">kepseːbit </ts>
               <ts e="T448" id="Seg_2766" n="e" s="T447">kajtak </ts>
               <ts e="T449" id="Seg_2768" n="e" s="T448">gɨmmɨtɨn. </ts>
               <ts e="T450" id="Seg_2770" n="e" s="T449">Munnʼaktaːbɨttar. </ts>
               <ts e="T451" id="Seg_2772" n="e" s="T450">Alta </ts>
               <ts e="T452" id="Seg_2774" n="e" s="T451">kihini </ts>
               <ts e="T453" id="Seg_2776" n="e" s="T452">köllʼörö </ts>
               <ts e="T454" id="Seg_2778" n="e" s="T453">ɨːppɨttar, </ts>
               <ts e="T455" id="Seg_2780" n="e" s="T454">staršina </ts>
               <ts e="T456" id="Seg_2782" n="e" s="T455">emi͡e </ts>
               <ts e="T457" id="Seg_2784" n="e" s="T456">barsɨbɨt. </ts>
               <ts e="T458" id="Seg_2786" n="e" s="T457">Tabannan </ts>
               <ts e="T459" id="Seg_2788" n="e" s="T458">gɨtta </ts>
               <ts e="T460" id="Seg_2790" n="e" s="T459">atɨnan </ts>
               <ts e="T461" id="Seg_2792" n="e" s="T460">barbɨttar. </ts>
               <ts e="T462" id="Seg_2794" n="e" s="T461">Tijbitter </ts>
               <ts e="T463" id="Seg_2796" n="e" s="T462">künüs </ts>
               <ts e="T464" id="Seg_2798" n="e" s="T463">össü͡ö. </ts>
               <ts e="T465" id="Seg_2800" n="e" s="T464">Biːr </ts>
               <ts e="T466" id="Seg_2802" n="e" s="T465">da </ts>
               <ts e="T467" id="Seg_2804" n="e" s="T466">kihi </ts>
               <ts e="T468" id="Seg_2806" n="e" s="T467">hu͡ok. </ts>
               <ts e="T469" id="Seg_2808" n="e" s="T468">Dʼi͡elerin </ts>
               <ts e="T470" id="Seg_2810" n="e" s="T469">potolu͡oga </ts>
               <ts e="T471" id="Seg_2812" n="e" s="T470">kajdɨbɨt, </ts>
               <ts e="T472" id="Seg_2814" n="e" s="T471">aːna </ts>
               <ts e="T473" id="Seg_2816" n="e" s="T472">arɨllan </ts>
               <ts e="T474" id="Seg_2818" n="e" s="T473">turar. </ts>
               <ts e="T475" id="Seg_2820" n="e" s="T474">Dʼi͡e </ts>
               <ts e="T476" id="Seg_2822" n="e" s="T475">ihin </ts>
               <ts e="T477" id="Seg_2824" n="e" s="T476">körbüttere, </ts>
               <ts e="T478" id="Seg_2826" n="e" s="T477">kɨːs </ts>
               <ts e="T479" id="Seg_2828" n="e" s="T478">ihe </ts>
               <ts e="T480" id="Seg_2830" n="e" s="T479">üllen </ts>
               <ts e="T481" id="Seg_2832" n="e" s="T480">baran </ts>
               <ts e="T482" id="Seg_2834" n="e" s="T481">hɨtar. </ts>
               <ts e="T483" id="Seg_2836" n="e" s="T482">Staršina: </ts>
               <ts e="T484" id="Seg_2838" n="e" s="T483">"Dʼi͡eni </ts>
               <ts e="T485" id="Seg_2840" n="e" s="T484">ubatɨ͡agɨŋ!" </ts>
               <ts e="T486" id="Seg_2842" n="e" s="T485">Bu </ts>
               <ts e="T487" id="Seg_2844" n="e" s="T486">dʼi͡eni </ts>
               <ts e="T488" id="Seg_2846" n="e" s="T487">kɨːstɨːn </ts>
               <ts e="T489" id="Seg_2848" n="e" s="T488">ubatan </ts>
               <ts e="T490" id="Seg_2850" n="e" s="T489">keːspitter. </ts>
               <ts e="T491" id="Seg_2852" n="e" s="T490">Tarakaːj </ts>
               <ts e="T492" id="Seg_2854" n="e" s="T491">u͡ol </ts>
               <ts e="T493" id="Seg_2856" n="e" s="T492">staršina </ts>
               <ts e="T494" id="Seg_2858" n="e" s="T493">kɨːhɨn </ts>
               <ts e="T495" id="Seg_2860" n="e" s="T494">ɨlan, </ts>
               <ts e="T496" id="Seg_2862" n="e" s="T495">bajan-toton </ts>
               <ts e="T497" id="Seg_2864" n="e" s="T496">olorbut. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T7" id="Seg_2865" s="T0">BeVP_1970_BaldheadedOrphanBoy_flk.001 (001.001)</ta>
            <ta e="T10" id="Seg_2866" s="T7">BeVP_1970_BaldheadedOrphanBoy_flk.002 (001.002)</ta>
            <ta e="T16" id="Seg_2867" s="T10">BeVP_1970_BaldheadedOrphanBoy_flk.003 (001.003)</ta>
            <ta e="T22" id="Seg_2868" s="T16">BeVP_1970_BaldheadedOrphanBoy_flk.004 (001.004)</ta>
            <ta e="T28" id="Seg_2869" s="T22">BeVP_1970_BaldheadedOrphanBoy_flk.005 (001.005)</ta>
            <ta e="T32" id="Seg_2870" s="T28">BeVP_1970_BaldheadedOrphanBoy_flk.006 (001.006)</ta>
            <ta e="T37" id="Seg_2871" s="T32">BeVP_1970_BaldheadedOrphanBoy_flk.007 (001.007)</ta>
            <ta e="T42" id="Seg_2872" s="T37">BeVP_1970_BaldheadedOrphanBoy_flk.008 (001.008)</ta>
            <ta e="T54" id="Seg_2873" s="T42">BeVP_1970_BaldheadedOrphanBoy_flk.009 (001.009)</ta>
            <ta e="T58" id="Seg_2874" s="T54">BeVP_1970_BaldheadedOrphanBoy_flk.010 (001.010)</ta>
            <ta e="T61" id="Seg_2875" s="T58">BeVP_1970_BaldheadedOrphanBoy_flk.011 (001.011)</ta>
            <ta e="T66" id="Seg_2876" s="T61">BeVP_1970_BaldheadedOrphanBoy_flk.012 (001.012)</ta>
            <ta e="T69" id="Seg_2877" s="T66">BeVP_1970_BaldheadedOrphanBoy_flk.013 (001.013)</ta>
            <ta e="T72" id="Seg_2878" s="T69">BeVP_1970_BaldheadedOrphanBoy_flk.014 (001.014)</ta>
            <ta e="T76" id="Seg_2879" s="T72">BeVP_1970_BaldheadedOrphanBoy_flk.015 (001.015)</ta>
            <ta e="T79" id="Seg_2880" s="T76">BeVP_1970_BaldheadedOrphanBoy_flk.016 (001.016)</ta>
            <ta e="T81" id="Seg_2881" s="T79">BeVP_1970_BaldheadedOrphanBoy_flk.017 (001.017)</ta>
            <ta e="T83" id="Seg_2882" s="T81">BeVP_1970_BaldheadedOrphanBoy_flk.018 (001.018)</ta>
            <ta e="T88" id="Seg_2883" s="T83">BeVP_1970_BaldheadedOrphanBoy_flk.019 (001.019)</ta>
            <ta e="T105" id="Seg_2884" s="T88">BeVP_1970_BaldheadedOrphanBoy_flk.020 (001.020)</ta>
            <ta e="T108" id="Seg_2885" s="T105">BeVP_1970_BaldheadedOrphanBoy_flk.021 (001.021)</ta>
            <ta e="T111" id="Seg_2886" s="T108">BeVP_1970_BaldheadedOrphanBoy_flk.022 (001.022)</ta>
            <ta e="T118" id="Seg_2887" s="T111">BeVP_1970_BaldheadedOrphanBoy_flk.023 (001.023)</ta>
            <ta e="T123" id="Seg_2888" s="T118">BeVP_1970_BaldheadedOrphanBoy_flk.024 (001.024)</ta>
            <ta e="T141" id="Seg_2889" s="T123">BeVP_1970_BaldheadedOrphanBoy_flk.025 (001.025)</ta>
            <ta e="T147" id="Seg_2890" s="T141">BeVP_1970_BaldheadedOrphanBoy_flk.026 (001.026)</ta>
            <ta e="T153" id="Seg_2891" s="T147">BeVP_1970_BaldheadedOrphanBoy_flk.027 (001.027)</ta>
            <ta e="T157" id="Seg_2892" s="T153">BeVP_1970_BaldheadedOrphanBoy_flk.028 (001.028)</ta>
            <ta e="T169" id="Seg_2893" s="T157">BeVP_1970_BaldheadedOrphanBoy_flk.029 (001.029)</ta>
            <ta e="T178" id="Seg_2894" s="T169">BeVP_1970_BaldheadedOrphanBoy_flk.030 (001.030)</ta>
            <ta e="T183" id="Seg_2895" s="T178">BeVP_1970_BaldheadedOrphanBoy_flk.031 (001.031)</ta>
            <ta e="T184" id="Seg_2896" s="T183">BeVP_1970_BaldheadedOrphanBoy_flk.032 (001.032)</ta>
            <ta e="T188" id="Seg_2897" s="T184">BeVP_1970_BaldheadedOrphanBoy_flk.033 (001.033)</ta>
            <ta e="T192" id="Seg_2898" s="T188">BeVP_1970_BaldheadedOrphanBoy_flk.034 (001.034)</ta>
            <ta e="T193" id="Seg_2899" s="T192">BeVP_1970_BaldheadedOrphanBoy_flk.035 (001.035)</ta>
            <ta e="T195" id="Seg_2900" s="T193">BeVP_1970_BaldheadedOrphanBoy_flk.036 (001.036)</ta>
            <ta e="T198" id="Seg_2901" s="T195">BeVP_1970_BaldheadedOrphanBoy_flk.037 (001.037)</ta>
            <ta e="T199" id="Seg_2902" s="T198">BeVP_1970_BaldheadedOrphanBoy_flk.038 (001.038)</ta>
            <ta e="T204" id="Seg_2903" s="T199">BeVP_1970_BaldheadedOrphanBoy_flk.039 (001.039)</ta>
            <ta e="T206" id="Seg_2904" s="T204">BeVP_1970_BaldheadedOrphanBoy_flk.040 (001.040)</ta>
            <ta e="T210" id="Seg_2905" s="T206">BeVP_1970_BaldheadedOrphanBoy_flk.041 (001.041)</ta>
            <ta e="T215" id="Seg_2906" s="T210">BeVP_1970_BaldheadedOrphanBoy_flk.042 (001.042)</ta>
            <ta e="T216" id="Seg_2907" s="T215">BeVP_1970_BaldheadedOrphanBoy_flk.043 (001.043)</ta>
            <ta e="T217" id="Seg_2908" s="T216">BeVP_1970_BaldheadedOrphanBoy_flk.044 (001.044)</ta>
            <ta e="T224" id="Seg_2909" s="T217">BeVP_1970_BaldheadedOrphanBoy_flk.045 (001.045)</ta>
            <ta e="T228" id="Seg_2910" s="T224">BeVP_1970_BaldheadedOrphanBoy_flk.046 (001.046)</ta>
            <ta e="T234" id="Seg_2911" s="T228">BeVP_1970_BaldheadedOrphanBoy_flk.047 (001.047)</ta>
            <ta e="T240" id="Seg_2912" s="T234">BeVP_1970_BaldheadedOrphanBoy_flk.048 (001.048)</ta>
            <ta e="T247" id="Seg_2913" s="T240">BeVP_1970_BaldheadedOrphanBoy_flk.049 (001.049)</ta>
            <ta e="T251" id="Seg_2914" s="T247">BeVP_1970_BaldheadedOrphanBoy_flk.050 (001.050)</ta>
            <ta e="T256" id="Seg_2915" s="T251">BeVP_1970_BaldheadedOrphanBoy_flk.051 (001.051)</ta>
            <ta e="T260" id="Seg_2916" s="T256">BeVP_1970_BaldheadedOrphanBoy_flk.052 (001.052)</ta>
            <ta e="T268" id="Seg_2917" s="T260">BeVP_1970_BaldheadedOrphanBoy_flk.053 (001.053)</ta>
            <ta e="T276" id="Seg_2918" s="T268">BeVP_1970_BaldheadedOrphanBoy_flk.054 (001.054)</ta>
            <ta e="T280" id="Seg_2919" s="T276">BeVP_1970_BaldheadedOrphanBoy_flk.055 (001.055)</ta>
            <ta e="T282" id="Seg_2920" s="T280">BeVP_1970_BaldheadedOrphanBoy_flk.056 (001.056)</ta>
            <ta e="T293" id="Seg_2921" s="T282">BeVP_1970_BaldheadedOrphanBoy_flk.057 (001.057)</ta>
            <ta e="T297" id="Seg_2922" s="T293">BeVP_1970_BaldheadedOrphanBoy_flk.058 (001.058)</ta>
            <ta e="T302" id="Seg_2923" s="T297">BeVP_1970_BaldheadedOrphanBoy_flk.059 (001.059)</ta>
            <ta e="T308" id="Seg_2924" s="T302">BeVP_1970_BaldheadedOrphanBoy_flk.060 (001.060)</ta>
            <ta e="T312" id="Seg_2925" s="T308">BeVP_1970_BaldheadedOrphanBoy_flk.061 (001.061)</ta>
            <ta e="T321" id="Seg_2926" s="T312">BeVP_1970_BaldheadedOrphanBoy_flk.062 (001.062)</ta>
            <ta e="T326" id="Seg_2927" s="T321">BeVP_1970_BaldheadedOrphanBoy_flk.063 (001.063)</ta>
            <ta e="T329" id="Seg_2928" s="T326">BeVP_1970_BaldheadedOrphanBoy_flk.064 (001.064)</ta>
            <ta e="T342" id="Seg_2929" s="T329">BeVP_1970_BaldheadedOrphanBoy_flk.065 (001.065)</ta>
            <ta e="T346" id="Seg_2930" s="T342">BeVP_1970_BaldheadedOrphanBoy_flk.066 (001.066)</ta>
            <ta e="T347" id="Seg_2931" s="T346">BeVP_1970_BaldheadedOrphanBoy_flk.067 (001.067)</ta>
            <ta e="T349" id="Seg_2932" s="T347">BeVP_1970_BaldheadedOrphanBoy_flk.068 (001.068)</ta>
            <ta e="T351" id="Seg_2933" s="T349">BeVP_1970_BaldheadedOrphanBoy_flk.069 (001.069)</ta>
            <ta e="T365" id="Seg_2934" s="T351">BeVP_1970_BaldheadedOrphanBoy_flk.070 (001.070)</ta>
            <ta e="T371" id="Seg_2935" s="T365">BeVP_1970_BaldheadedOrphanBoy_flk.071 (001.071)</ta>
            <ta e="T373" id="Seg_2936" s="T371">BeVP_1970_BaldheadedOrphanBoy_flk.072 (001.072)</ta>
            <ta e="T380" id="Seg_2937" s="T373">BeVP_1970_BaldheadedOrphanBoy_flk.073 (001.073)</ta>
            <ta e="T386" id="Seg_2938" s="T380">BeVP_1970_BaldheadedOrphanBoy_flk.074 (001.074)</ta>
            <ta e="T393" id="Seg_2939" s="T386">BeVP_1970_BaldheadedOrphanBoy_flk.075 (001.075)</ta>
            <ta e="T396" id="Seg_2940" s="T393">BeVP_1970_BaldheadedOrphanBoy_flk.076 (001.076)</ta>
            <ta e="T400" id="Seg_2941" s="T396">BeVP_1970_BaldheadedOrphanBoy_flk.077 (001.077)</ta>
            <ta e="T403" id="Seg_2942" s="T400">BeVP_1970_BaldheadedOrphanBoy_flk.078 (001.078)</ta>
            <ta e="T408" id="Seg_2943" s="T403">BeVP_1970_BaldheadedOrphanBoy_flk.079 (001.079)</ta>
            <ta e="T419" id="Seg_2944" s="T408">BeVP_1970_BaldheadedOrphanBoy_flk.080 (001.080)</ta>
            <ta e="T423" id="Seg_2945" s="T419">BeVP_1970_BaldheadedOrphanBoy_flk.081 (001.081)</ta>
            <ta e="T426" id="Seg_2946" s="T423">BeVP_1970_BaldheadedOrphanBoy_flk.082 (001.082)</ta>
            <ta e="T430" id="Seg_2947" s="T426">BeVP_1970_BaldheadedOrphanBoy_flk.083 (001.083)</ta>
            <ta e="T435" id="Seg_2948" s="T430">BeVP_1970_BaldheadedOrphanBoy_flk.084 (001.084)</ta>
            <ta e="T439" id="Seg_2949" s="T435">BeVP_1970_BaldheadedOrphanBoy_flk.085 (001.085)</ta>
            <ta e="T441" id="Seg_2950" s="T439">BeVP_1970_BaldheadedOrphanBoy_flk.086 (001.086)</ta>
            <ta e="T443" id="Seg_2951" s="T441">BeVP_1970_BaldheadedOrphanBoy_flk.087 (001.087)</ta>
            <ta e="T449" id="Seg_2952" s="T443">BeVP_1970_BaldheadedOrphanBoy_flk.088 (001.088)</ta>
            <ta e="T450" id="Seg_2953" s="T449">BeVP_1970_BaldheadedOrphanBoy_flk.089 (001.089)</ta>
            <ta e="T457" id="Seg_2954" s="T450">BeVP_1970_BaldheadedOrphanBoy_flk.090 (001.090)</ta>
            <ta e="T461" id="Seg_2955" s="T457">BeVP_1970_BaldheadedOrphanBoy_flk.091 (001.091)</ta>
            <ta e="T464" id="Seg_2956" s="T461">BeVP_1970_BaldheadedOrphanBoy_flk.092 (001.092)</ta>
            <ta e="T468" id="Seg_2957" s="T464">BeVP_1970_BaldheadedOrphanBoy_flk.093 (001.093)</ta>
            <ta e="T474" id="Seg_2958" s="T468">BeVP_1970_BaldheadedOrphanBoy_flk.094 (001.094)</ta>
            <ta e="T482" id="Seg_2959" s="T474">BeVP_1970_BaldheadedOrphanBoy_flk.095 (001.095)</ta>
            <ta e="T483" id="Seg_2960" s="T482">BeVP_1970_BaldheadedOrphanBoy_flk.096 (001.096)</ta>
            <ta e="T485" id="Seg_2961" s="T483">BeVP_1970_BaldheadedOrphanBoy_flk.097 (001.097)</ta>
            <ta e="T490" id="Seg_2962" s="T485">BeVP_1970_BaldheadedOrphanBoy_flk.098 (001.098)</ta>
            <ta e="T497" id="Seg_2963" s="T490">BeVP_1970_BaldheadedOrphanBoy_flk.099 (001.099)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T7" id="Seg_2964" s="T0">Кинээһи гытта старшина биирдии уоллаактар, олорбуттар эбит.</ta>
            <ta e="T10" id="Seg_2965" s="T7">Иккиэннэрэ — тыа киһилэрэ.</ta>
            <ta e="T16" id="Seg_2966" s="T10">Уолаттар бииргэ үөскээн догордуу багайы эбиттэрэ.</ta>
            <ta e="T22" id="Seg_2967" s="T16">Бу киһилэр былыргыттан көһө һылдьар эбиттэрэ.</ta>
            <ta e="T28" id="Seg_2968" s="T22">Арай күһүн паас иитэр кэм буолбут.</ta>
            <ta e="T32" id="Seg_2969" s="T28">Астарын эрдэттэн бэлэмнээн кээспиттэр.</ta>
            <ta e="T37" id="Seg_2970" s="T32">Икки уол ытынан паастыы барбыттар.</ta>
            <ta e="T42" id="Seg_2971" s="T37">Паас ииттэн-ииттэн баран очиһаларыгар коммуттар.</ta>
            <ta e="T54" id="Seg_2972" s="T42">Биирдэ киэһэ, ыттарын баайан баран, балык һыатынан элиэкэ оҥостубуттар, чаай иһэ олорбуттар.</ta>
            <ta e="T58" id="Seg_2973" s="T54">Арай һыргалаак тыаһа иһиллибит.</ta>
            <ta e="T61" id="Seg_2974" s="T58">Кинээс уола дьиэбит:</ta>
            <ta e="T66" id="Seg_2975" s="T61">— Үтүө киһи кэлбэтэ буолуо дуо?</ta>
            <ta e="T69" id="Seg_2976" s="T66">Бэйэтэ табактыы олорбут.</ta>
            <ta e="T72" id="Seg_2977" s="T69">Догоро тарайа һыппыт.</ta>
            <ta e="T76" id="Seg_2978" s="T72">Иһиллэммиттэрэ — ыттара үрбэт буолбут.</ta>
            <ta e="T79" id="Seg_2979" s="T76">Каамар тыас иһиллибит.</ta>
            <ta e="T81" id="Seg_2980" s="T79">Кинээс уола:</ta>
            <ta e="T83" id="Seg_2981" s="T81">— Куттанабын, — диэбит.</ta>
            <ta e="T88" id="Seg_2982" s="T83">Догоро һол көрдүк тарайа һыппыт.</ta>
            <ta e="T105" id="Seg_2983" s="T88">Аан арыллыбыт да һуон багайы дьактар арыччы батан киирбит, чөл огуруо таҥастаак, огуруо бэргэһэлээк, баай багайы дьүһүннээк.</ta>
            <ta e="T108" id="Seg_2984" s="T105">Старшина уола догорун:</ta>
            <ta e="T111" id="Seg_2985" s="T108">— Чаайдаа ыалдьыты, — дьиэбит.</ta>
            <ta e="T118" id="Seg_2986" s="T111">Кинээс уола туран оһок оттуммут, чаай кыыйньарбат.</ta>
            <ta e="T123" id="Seg_2987" s="T118">Арай дьактардара һырайын көлльөрбөт, һаҥарсыбат.</ta>
            <ta e="T141" id="Seg_2988" s="T123">Бу уол балык куөһэ һыаламмыт испиичкэ маһынан карагын тэптэрэн баран көрбүтэ — дьактар күлэ олорор, һоготок ирэ тиистээк эбит.</ta>
            <ta e="T147" id="Seg_2989" s="T141">Мас киллэрэ таксыбыт, ыттарын көлүйтэлээн кээспит.</ta>
            <ta e="T153" id="Seg_2990" s="T147">Көрбүтэ — дьактар һыргатыгар алта ыт көлүллүбүт.</ta>
            <ta e="T157" id="Seg_2991" s="T153">Киирэн баран ыалдьытын аһаппыт.</ta>
            <ta e="T169" id="Seg_2992" s="T157">— Буус киллэстэ таксыам, — дьиэн баран танастарын кыбынан таһаарбыт, ытын һыргатыгар ууран кээспит.</ta>
            <ta e="T178" id="Seg_2993" s="T169">Онтон киирбит, дьактары олорбут һиригэр утуппут, тирии биэрбит тэлгэниэн.</ta>
            <ta e="T183" id="Seg_2994" s="T178">Балык һыата һунуурдарын утутан кээспиттэр.</ta>
            <ta e="T184" id="Seg_2995" s="T183">Утуйбуттар.</ta>
            <ta e="T188" id="Seg_2996" s="T184">Старшина уола догорун эппит:</ta>
            <ta e="T192" id="Seg_2997" s="T188">— Доо, мантыбыт кыыс буолбатаак?!</ta>
            <ta e="T193" id="Seg_2998" s="T192">Кыыстыак.</ta>
            <ta e="T195" id="Seg_2999" s="T193">Тугун куттанагын?</ta>
            <ta e="T198" id="Seg_3000" s="T195">Мин оччого барыам.</ta>
            <ta e="T199" id="Seg_3001" s="T198">Догоро:</ta>
            <ta e="T204" id="Seg_3002" s="T199">— Мин таксыбытым кэннэ кыыскар бараар.</ta>
            <ta e="T206" id="Seg_3003" s="T204">Таксан каалбыт.</ta>
            <ta e="T210" id="Seg_3004" s="T206">Таҥнан баран һыргатыгар олорбут.</ta>
            <ta e="T215" id="Seg_3005" s="T210">Төһө буолуой, өлөр-каалар үүгү иһиллибит:</ta>
            <ta e="T216" id="Seg_3006" s="T215">— Араа!..</ta>
            <ta e="T217" id="Seg_3007" s="T216">Абыраа!!!</ta>
            <ta e="T224" id="Seg_3008" s="T217">Бу уол ньуоҥутун ылла дааганы көтүтэн каалбыт.</ta>
            <ta e="T228" id="Seg_3009" s="T224">Кэнниттэн дьактар һаҥата иһиллибит:</ta>
            <ta e="T234" id="Seg_3010" s="T228">— Билбитим буоллар һиэн кээһиэ этим буо!</ta>
            <ta e="T240" id="Seg_3011" s="T234">Кэлэн бу уол агатыгар, кинээскэ, кэпсээбит.</ta>
            <ta e="T247" id="Seg_3012" s="T240">Кинээһи гытта старшина һэттэ дьиэни комуйан мунньактаабыттар.</ta>
            <ta e="T251" id="Seg_3013" s="T247">Икки уолу ыыппыттар көрүөктэрин.</ta>
            <ta e="T256" id="Seg_3014" s="T251">Икки уол буолан баран кааллылар.</ta>
            <ta e="T260" id="Seg_3015" s="T256">Өр буолуоктарай, һырдыкка кэлбиттэр.</ta>
            <ta e="T268" id="Seg_3016" s="T260">Көрбүттэрэ — биир да киһи һуок, һуол да һуок.</ta>
            <ta e="T276" id="Seg_3017" s="T268">Ураһа дьиэгэ киирбиттэрэ — уол оҥуога ирэ дардайа һытар.</ta>
            <ta e="T280" id="Seg_3018" s="T276">Бу икки уол төннүбүттэр.</ta>
            <ta e="T282" id="Seg_3019" s="T280">Кинээскэ кэпсээбиттэр.</ta>
            <ta e="T293" id="Seg_3020" s="T282">Биир тулаайак ого баар, икки һанныттан таракаай, һоготок иньэлээк, биир табалаак.</ta>
            <ta e="T297" id="Seg_3021" s="T293">Кинээһи гытта старшина мунньактаабыттар.</ta>
            <ta e="T302" id="Seg_3022" s="T297">— Кайдак буолуокпутуй: һирбитин бырагыакпыт дуу?</ta>
            <ta e="T308" id="Seg_3023" s="T302">Эмиэ да паастарбыт бааллар… — дэспиттэр.</ta>
            <ta e="T312" id="Seg_3024" s="T308">Старшина онно туран эппит:</ta>
            <ta e="T321" id="Seg_3025" s="T312">— Тулаайак огону ыытыакка, мин кыыспын да биэриэм этэ, өлөрдөгүнэ.</ta>
            <ta e="T326" id="Seg_3026" s="T321">Бу уолу ыгырбыттар, кэпсээбиттэр барытын.</ta>
            <ta e="T329" id="Seg_3027" s="T326">Дьэ үлэгэр эбит.</ta>
            <ta e="T342" id="Seg_3028" s="T329">Ого муҥнаак һоготогун барар буолла, ыт ирэ биэрбиттэр, һаа да туок да һуок.</ta>
            <ta e="T346" id="Seg_3029" s="T342">Дьиэтигэр кэлэн ытана олорбут.</ta>
            <ta e="T347" id="Seg_3030" s="T346">Иньэтэ:</ta>
            <ta e="T349" id="Seg_3031" s="T347">— Туок буоллуҥ?</ta>
            <ta e="T351" id="Seg_3032" s="T349">Кэпсээбит эньи.</ta>
            <ta e="T365" id="Seg_3033" s="T351">— Агаҥ батыйата канна ирэ баар этэ, ону ильльээр, — иньэтэ муҥнаак кам дьэбинтийбит батыйаны киллэрбит.</ta>
            <ta e="T371" id="Seg_3034" s="T365">Бу уол өр буолбакка баран каалбыт.</ta>
            <ta e="T373" id="Seg_3035" s="T371">Быстыа дуу?</ta>
            <ta e="T380" id="Seg_3036" s="T373">Кэлбитэ ким да һуок, киһи ирэ оҥуога.</ta>
            <ta e="T386" id="Seg_3037" s="T380">Очиһага, им түспүтүн кэннэ, һунуурун убаппыт.</ta>
            <ta e="T393" id="Seg_3038" s="T386">Киһи утуйар кэмин дьиэк һыргалаак тыаһа иһиллибит.</ta>
            <ta e="T396" id="Seg_3039" s="T393">Ыт да үрбэтэк.</ta>
            <ta e="T400" id="Seg_3040" s="T396">Уотун утутан баран олорбут.</ta>
            <ta e="T403" id="Seg_3041" s="T400">Тыас бөгө буолбут.</ta>
            <ta e="T408" id="Seg_3042" s="T403">Ааҥҥа баппат һуон дьактар киирбит.</ta>
            <ta e="T419" id="Seg_3043" s="T408">Анаар атагын ааҥҥа анньарын гытта икки конногун аннынан батыйанан батары аспыт.</ta>
            <ta e="T423" id="Seg_3044" s="T419">Бэйэтэ конногун аннынан күрээбит.</ta>
            <ta e="T426" id="Seg_3045" s="T423">Түүн дьиэтигэр кэлбит.</ta>
            <ta e="T430" id="Seg_3046" s="T426">Иньэтэ муҥнаак утуйбакка күүтэн.</ta>
            <ta e="T435" id="Seg_3047" s="T430">Һарсиэрдэ иньэтэ кинээскэ баран кэпсээбит.</ta>
            <ta e="T439" id="Seg_3048" s="T435">Кинээс бу уолу ыгырбыт.</ta>
            <ta e="T441" id="Seg_3049" s="T439">— Дьэ кайдагый?</ta>
            <ta e="T443" id="Seg_3050" s="T441">Өлөрдүҥ дуу?</ta>
            <ta e="T449" id="Seg_3051" s="T443">— Билбэппин, — диэбит уол, кэпсээбит кайтак гыммытын.</ta>
            <ta e="T450" id="Seg_3052" s="T449">Мунньактаабыттар.</ta>
            <ta e="T457" id="Seg_3053" s="T450">Алта киһини көлльөрө ыыппыттар, старшина эмиэ барсыбыт.</ta>
            <ta e="T461" id="Seg_3054" s="T457">Табаннан гытта атынан барбыттар.</ta>
            <ta e="T464" id="Seg_3055" s="T461">Тийбиттэр күнүс өссүө.</ta>
            <ta e="T468" id="Seg_3056" s="T464">Биир да киһи һуок.</ta>
            <ta e="T474" id="Seg_3057" s="T468">Дьиэлэрин потолуога кайдыбыт, аана арыллан турар.</ta>
            <ta e="T482" id="Seg_3058" s="T474">Дьиэ иһин көрбүттэрэ, кыыс иһэ үллэн баран һытар.</ta>
            <ta e="T483" id="Seg_3059" s="T482">Старшина:</ta>
            <ta e="T485" id="Seg_3060" s="T483">— Дьиэни убатыагыҥ!</ta>
            <ta e="T490" id="Seg_3061" s="T485">Бу дьиэни кыыстыын убатан кээспиттэр.</ta>
            <ta e="T497" id="Seg_3062" s="T490">Таракаай уол старшина кыыһын ылан, байан-тотон олорбут.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T7" id="Seg_3063" s="T0">Kineːhi gɨtta staršina biːrdiː u͡ollaːktar, olorbuttar ebit. </ta>
            <ta e="T10" id="Seg_3064" s="T7">Ikki͡ennere — tɨ͡a kihilere. </ta>
            <ta e="T16" id="Seg_3065" s="T10">U͡olattar biːrge ü͡öskeːn dogorduː bagajɨ ebittere. </ta>
            <ta e="T22" id="Seg_3066" s="T16">Bu kihiler bɨlɨrgɨttan köhö hɨldʼar ebittere. </ta>
            <ta e="T28" id="Seg_3067" s="T22">Araj kühün paːs iːter kem bu͡olbut. </ta>
            <ta e="T32" id="Seg_3068" s="T28">Astarɨn erdetten belemneːn keːspitter. </ta>
            <ta e="T37" id="Seg_3069" s="T32">Ikki u͡ol ɨtɨnan paːstɨː barbɨttar. </ta>
            <ta e="T42" id="Seg_3070" s="T37">Paːs iːtten-iːtten baran očihalarɨgar kommuttar. </ta>
            <ta e="T54" id="Seg_3071" s="T42">Biːrde ki͡ehe, ɨttarɨn baːjan baran, balɨk hɨ͡atɨnan eli͡eke oŋostubuttar, čaːj ihe olorbuttar. </ta>
            <ta e="T58" id="Seg_3072" s="T54">Araj hɨrgalaːk tɨ͡aha ihillibit. </ta>
            <ta e="T61" id="Seg_3073" s="T58">Kineːs u͡ola dʼi͡ebit: </ta>
            <ta e="T66" id="Seg_3074" s="T61">"Ütü͡ö kihi kelbete bu͡olu͡o du͡o?" </ta>
            <ta e="T69" id="Seg_3075" s="T66">Bejete tabaktɨː olorbut. </ta>
            <ta e="T72" id="Seg_3076" s="T69">Dogoro taraja hɨppɨt. </ta>
            <ta e="T76" id="Seg_3077" s="T72">Ihillemmittere — ɨttara ürbet bu͡olbut. </ta>
            <ta e="T79" id="Seg_3078" s="T76">Kaːmar tɨ͡as ihillibit. </ta>
            <ta e="T81" id="Seg_3079" s="T79">Kineːs u͡ola: </ta>
            <ta e="T83" id="Seg_3080" s="T81">"Kuttanabɨn", di͡ebit. </ta>
            <ta e="T88" id="Seg_3081" s="T83">Dogoro hol kördük taraja hɨppɨt. </ta>
            <ta e="T105" id="Seg_3082" s="T88">Aːn arɨllɨbɨt da hu͡on bagajɨ dʼaktar arɨččɨ batan kiːrbit, čöl oguru͡o taŋastaːk, oguru͡o bergeheleːk, baːj bagajɨ dʼühünneːk. </ta>
            <ta e="T108" id="Seg_3083" s="T105">Staršina u͡ola dogorun: </ta>
            <ta e="T111" id="Seg_3084" s="T108">"Čaːjdaː ɨ͡aldʼɨtɨ", dʼi͡ebit. </ta>
            <ta e="T118" id="Seg_3085" s="T111">Kineːs u͡ola turan ohok ottummut, čaːj kɨːjnʼarbat. </ta>
            <ta e="T123" id="Seg_3086" s="T118">Araj dʼaktardara hɨrajɨn köllʼörböt, haŋarsɨbat. </ta>
            <ta e="T141" id="Seg_3087" s="T123">Bu u͡ol balɨk kü͡öhe hɨ͡alammɨt ispiːčke mahɨnan karagɨn tepteren baran körbüte — dʼaktar küle oloror, hogotok ire tiːsteːk ebit. </ta>
            <ta e="T147" id="Seg_3088" s="T141">Mas killere taksɨbɨt, ɨttarɨn kölüjteleːn keːspit. </ta>
            <ta e="T153" id="Seg_3089" s="T147">Körbüte — dʼaktar hɨrgatɨgar alta ɨt kölüllübüt. </ta>
            <ta e="T157" id="Seg_3090" s="T153">Kiːren baran ɨ͡aldʼɨtɨn ahappɨt. </ta>
            <ta e="T169" id="Seg_3091" s="T157">"Buːs killeste taksɨ͡am", dʼi͡en baran tanastarɨn kɨbɨnan tahaːrbɨt, ɨtɨn hɨrgatɨgar uːran keːspit. </ta>
            <ta e="T178" id="Seg_3092" s="T169">Onton kiːrbit, dʼaktarɨ olorbut hiriger utupput, tiriː bi͡erbit telgeni͡en. </ta>
            <ta e="T183" id="Seg_3093" s="T178">Balɨk hɨ͡ata hunuːrdarɨn ututan keːspitter. </ta>
            <ta e="T184" id="Seg_3094" s="T183">Utujbuttar. </ta>
            <ta e="T188" id="Seg_3095" s="T184">Staršina u͡ola dogorun eppit: </ta>
            <ta e="T192" id="Seg_3096" s="T188">"Doː, mantɨbɨt kɨːs bu͡olbataːk? </ta>
            <ta e="T193" id="Seg_3097" s="T192">Kɨːstɨ͡ak. </ta>
            <ta e="T195" id="Seg_3098" s="T193">Tugun kuttanagɨn? </ta>
            <ta e="T198" id="Seg_3099" s="T195">Min oččogo barɨ͡am." </ta>
            <ta e="T199" id="Seg_3100" s="T198">Dogoro:</ta>
            <ta e="T204" id="Seg_3101" s="T199">"Min taksɨbɨtɨm kenne kɨːskar baraːr." </ta>
            <ta e="T206" id="Seg_3102" s="T204">Taksan kaːlbɨt. </ta>
            <ta e="T210" id="Seg_3103" s="T206">Taŋnan baran hɨrgatɨgar olorbut. </ta>
            <ta e="T215" id="Seg_3104" s="T210">Töhö bu͡olu͡oj, ölör-kaːlar üːgü ihillibit: </ta>
            <ta e="T216" id="Seg_3105" s="T215">—Araː! </ta>
            <ta e="T217" id="Seg_3106" s="T216">Abɨraː!" </ta>
            <ta e="T224" id="Seg_3107" s="T217">Bu u͡ol nʼu͡oŋutun ɨlla daːganɨ kötüten kaːlbɨt. </ta>
            <ta e="T228" id="Seg_3108" s="T224">Kennitten dʼaktar haŋata ihillibit: </ta>
            <ta e="T234" id="Seg_3109" s="T228">"Bilbitim bu͡ollar hi͡en keːhi͡e etim bu͡o!" </ta>
            <ta e="T240" id="Seg_3110" s="T234">Kelen bu u͡ol agatɨgar, kineːske, kepseːbit. </ta>
            <ta e="T247" id="Seg_3111" s="T240">Kineːhi gɨtta staršina hette dʼi͡eni komujan munnʼaktaːbɨttar. </ta>
            <ta e="T251" id="Seg_3112" s="T247">Ikki u͡olu ɨːppɨttar körü͡ökterin. </ta>
            <ta e="T256" id="Seg_3113" s="T251">Ikki u͡ol bu͡olan baran kaːllɨlar. </ta>
            <ta e="T260" id="Seg_3114" s="T256">Ör bu͡olu͡oktaraj, hɨrdɨkka kelbitter. </ta>
            <ta e="T268" id="Seg_3115" s="T260">Körbüttere — biːr da kihi hu͡ok, hu͡ol da hu͡ok. </ta>
            <ta e="T276" id="Seg_3116" s="T268">Uraha dʼi͡ege kiːrbittere — u͡ol oŋu͡oga ire dardaja hɨtar. </ta>
            <ta e="T280" id="Seg_3117" s="T276">Bu ikki u͡ol tönnübütter. </ta>
            <ta e="T282" id="Seg_3118" s="T280">Kineːske kepseːbitter. </ta>
            <ta e="T293" id="Seg_3119" s="T282">Biːr tulaːjak ogo baːr, ikki hannɨttan tarakaːj, hogotok inʼeleːk, biːr tabalaːk. </ta>
            <ta e="T297" id="Seg_3120" s="T293">Kineːhi gɨtta staršina munnʼaktaːbɨttar. </ta>
            <ta e="T302" id="Seg_3121" s="T297">"Kajdak bu͡olu͡okputuj, hirbitin bɨragɨ͡akpɨt duː? </ta>
            <ta e="T308" id="Seg_3122" s="T302">Emi͡e da paːstarbɨt baːllar", despitter. </ta>
            <ta e="T312" id="Seg_3123" s="T308">Staršina onno turan eppit: </ta>
            <ta e="T321" id="Seg_3124" s="T312">"Tulaːjak ogonu ɨːtɨ͡akka, min kɨːspɨn da bi͡eri͡em ete, ölördögüne." </ta>
            <ta e="T326" id="Seg_3125" s="T321">Bu u͡olu ɨgɨrbɨttar, kepseːbitter barɨtɨn. </ta>
            <ta e="T329" id="Seg_3126" s="T326">Dʼe üleger ebit. </ta>
            <ta e="T342" id="Seg_3127" s="T329">Ogo muŋnaːk hogotogun barar bu͡olla, ɨt ire bi͡erbitter, haː da tu͡ok da hu͡ok. </ta>
            <ta e="T346" id="Seg_3128" s="T342">Dʼi͡etiger kelen ɨtana olorbut. </ta>
            <ta e="T347" id="Seg_3129" s="T346">Inʼete: </ta>
            <ta e="T349" id="Seg_3130" s="T347">"Tu͡ok bu͡olluŋ?" </ta>
            <ta e="T351" id="Seg_3131" s="T349">Kepseːbit enʼi. </ta>
            <ta e="T365" id="Seg_3132" s="T351">"Agaŋ batɨjata kanna ire baːr ete, onu ilʼlʼeːr", inʼete muŋnaːk kam dʼebintijbit batɨjanɨ killerbit. </ta>
            <ta e="T371" id="Seg_3133" s="T365">Bu u͡ol ör bu͡olbakka baran kaːlbɨt. </ta>
            <ta e="T373" id="Seg_3134" s="T371">Bɨstɨ͡a duː? </ta>
            <ta e="T380" id="Seg_3135" s="T373">Kelbite kim da hu͡ok, kihi ire oŋu͡oga. </ta>
            <ta e="T386" id="Seg_3136" s="T380">Očihaga, im tüspütün kenne, hunuːrun ubappɨt. </ta>
            <ta e="T393" id="Seg_3137" s="T386">Kihi utujar kemin dʼi͡ek hɨrgalaːk tɨ͡aha ihillibit. </ta>
            <ta e="T396" id="Seg_3138" s="T393">ɨt da ürbetek. </ta>
            <ta e="T400" id="Seg_3139" s="T396">U͡otun ututan baran olorbut. </ta>
            <ta e="T403" id="Seg_3140" s="T400">Tɨ͡as bögö bu͡olbut. </ta>
            <ta e="T408" id="Seg_3141" s="T403">Aːŋŋa bappat hu͡on dʼaktar kiːrbit. </ta>
            <ta e="T419" id="Seg_3142" s="T408">Anaːr atagɨn aːŋŋa annʼarɨn gɨtta ikki konnogun annɨnan batɨjanan batarɨ aspɨt. </ta>
            <ta e="T423" id="Seg_3143" s="T419">Bejete konnogun annɨnan küreːbit. </ta>
            <ta e="T426" id="Seg_3144" s="T423">Tüːn dʼi͡etiger kelbit. </ta>
            <ta e="T430" id="Seg_3145" s="T426">Inʼete muŋnaːk utujbakka küːten. </ta>
            <ta e="T435" id="Seg_3146" s="T430">Harsi͡erde inʼete kineːske baran kepseːbit. </ta>
            <ta e="T439" id="Seg_3147" s="T435">Kineːs bu u͡olu ɨgɨrbɨt. </ta>
            <ta e="T441" id="Seg_3148" s="T439">"Dʼe kajdagɨj? </ta>
            <ta e="T443" id="Seg_3149" s="T441">Ölördüŋ duː?" </ta>
            <ta e="T449" id="Seg_3150" s="T443">"Bilbeppin", di͡ebit u͡ol, kepseːbit kajtak gɨmmɨtɨn. </ta>
            <ta e="T450" id="Seg_3151" s="T449">Munnʼaktaːbɨttar. </ta>
            <ta e="T457" id="Seg_3152" s="T450">Alta kihini köllʼörö ɨːppɨttar, staršina emi͡e barsɨbɨt. </ta>
            <ta e="T461" id="Seg_3153" s="T457">Tabannan gɨtta atɨnan barbɨttar. </ta>
            <ta e="T464" id="Seg_3154" s="T461">Tijbitter künüs össü͡ö. </ta>
            <ta e="T468" id="Seg_3155" s="T464">Biːr da kihi hu͡ok. </ta>
            <ta e="T474" id="Seg_3156" s="T468">Dʼi͡elerin potolu͡oga kajdɨbɨt, aːna arɨllan turar. </ta>
            <ta e="T482" id="Seg_3157" s="T474">Dʼi͡e ihin körbüttere, kɨːs ihe üllen baran hɨtar. </ta>
            <ta e="T483" id="Seg_3158" s="T482">Staršina: </ta>
            <ta e="T485" id="Seg_3159" s="T483">"Dʼi͡eni ubatɨ͡agɨŋ!" </ta>
            <ta e="T490" id="Seg_3160" s="T485">Bu dʼi͡eni kɨːstɨːn ubatan keːspitter. </ta>
            <ta e="T497" id="Seg_3161" s="T490">Tarakaːj u͡ol staršina kɨːhɨn ɨlan, bajan-toton olorbut. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_3162" s="T0">kineːh-i</ta>
            <ta e="T2" id="Seg_3163" s="T1">gɨtta</ta>
            <ta e="T3" id="Seg_3164" s="T2">staršina</ta>
            <ta e="T4" id="Seg_3165" s="T3">biːr-diː</ta>
            <ta e="T5" id="Seg_3166" s="T4">u͡ol-laːk-tar</ta>
            <ta e="T6" id="Seg_3167" s="T5">olor-but-tar</ta>
            <ta e="T7" id="Seg_3168" s="T6">e-bit</ta>
            <ta e="T8" id="Seg_3169" s="T7">ikk-i͡en-nere</ta>
            <ta e="T9" id="Seg_3170" s="T8">tɨ͡a</ta>
            <ta e="T10" id="Seg_3171" s="T9">kihi-lere</ta>
            <ta e="T11" id="Seg_3172" s="T10">u͡olat-tar</ta>
            <ta e="T12" id="Seg_3173" s="T11">biːrge</ta>
            <ta e="T13" id="Seg_3174" s="T12">ü͡öskeː-n</ta>
            <ta e="T14" id="Seg_3175" s="T13">dogor-duː</ta>
            <ta e="T15" id="Seg_3176" s="T14">bagajɨ</ta>
            <ta e="T16" id="Seg_3177" s="T15">e-bit-tere</ta>
            <ta e="T17" id="Seg_3178" s="T16">bu</ta>
            <ta e="T18" id="Seg_3179" s="T17">kihi-ler</ta>
            <ta e="T19" id="Seg_3180" s="T18">bɨlɨrgɨ-ttan</ta>
            <ta e="T20" id="Seg_3181" s="T19">köh-ö</ta>
            <ta e="T21" id="Seg_3182" s="T20">hɨldʼ-ar</ta>
            <ta e="T22" id="Seg_3183" s="T21">e-bit-tere</ta>
            <ta e="T23" id="Seg_3184" s="T22">araj</ta>
            <ta e="T24" id="Seg_3185" s="T23">kühün</ta>
            <ta e="T25" id="Seg_3186" s="T24">paːs</ta>
            <ta e="T26" id="Seg_3187" s="T25">iːt-er</ta>
            <ta e="T27" id="Seg_3188" s="T26">kem</ta>
            <ta e="T28" id="Seg_3189" s="T27">bu͡ol-but</ta>
            <ta e="T29" id="Seg_3190" s="T28">as-tarɨ-n</ta>
            <ta e="T30" id="Seg_3191" s="T29">erdetten</ta>
            <ta e="T31" id="Seg_3192" s="T30">belem-neː-n</ta>
            <ta e="T32" id="Seg_3193" s="T31">keːs-pit-ter</ta>
            <ta e="T33" id="Seg_3194" s="T32">ikki</ta>
            <ta e="T34" id="Seg_3195" s="T33">u͡ol</ta>
            <ta e="T35" id="Seg_3196" s="T34">ɨt-ɨ-nan</ta>
            <ta e="T36" id="Seg_3197" s="T35">paːs-t-ɨː</ta>
            <ta e="T37" id="Seg_3198" s="T36">bar-bɨt-tar</ta>
            <ta e="T38" id="Seg_3199" s="T37">paːs</ta>
            <ta e="T39" id="Seg_3200" s="T38">iːt-t-en-iːt-t-en</ta>
            <ta e="T40" id="Seg_3201" s="T39">baran</ta>
            <ta e="T41" id="Seg_3202" s="T40">očiha-larɨ-gar</ta>
            <ta e="T42" id="Seg_3203" s="T41">kom-mut-tar</ta>
            <ta e="T43" id="Seg_3204" s="T42">biːrde</ta>
            <ta e="T44" id="Seg_3205" s="T43">ki͡ehe</ta>
            <ta e="T45" id="Seg_3206" s="T44">ɨt-tarɨ-n</ta>
            <ta e="T46" id="Seg_3207" s="T45">baːj-an</ta>
            <ta e="T47" id="Seg_3208" s="T46">baran</ta>
            <ta e="T48" id="Seg_3209" s="T47">balɨk</ta>
            <ta e="T49" id="Seg_3210" s="T48">hɨ͡a-tɨ-nan</ta>
            <ta e="T50" id="Seg_3211" s="T49">eli͡eke</ta>
            <ta e="T51" id="Seg_3212" s="T50">oŋost-u-but-tar</ta>
            <ta e="T52" id="Seg_3213" s="T51">čaːj</ta>
            <ta e="T53" id="Seg_3214" s="T52">ih-e</ta>
            <ta e="T54" id="Seg_3215" s="T53">olor-but-tar</ta>
            <ta e="T55" id="Seg_3216" s="T54">araj</ta>
            <ta e="T56" id="Seg_3217" s="T55">hɨrga-laːk</ta>
            <ta e="T57" id="Seg_3218" s="T56">tɨ͡ah-a</ta>
            <ta e="T58" id="Seg_3219" s="T57">ihill-i-bit</ta>
            <ta e="T59" id="Seg_3220" s="T58">kineːs</ta>
            <ta e="T60" id="Seg_3221" s="T59">u͡ol-a</ta>
            <ta e="T61" id="Seg_3222" s="T60">dʼi͡e-bit</ta>
            <ta e="T62" id="Seg_3223" s="T61">ütü͡ö</ta>
            <ta e="T63" id="Seg_3224" s="T62">kihi</ta>
            <ta e="T64" id="Seg_3225" s="T63">kel-be-t-e</ta>
            <ta e="T65" id="Seg_3226" s="T64">bu͡olu͡o</ta>
            <ta e="T66" id="Seg_3227" s="T65">du͡o</ta>
            <ta e="T67" id="Seg_3228" s="T66">beje-te</ta>
            <ta e="T68" id="Seg_3229" s="T67">tabak-t-ɨː</ta>
            <ta e="T69" id="Seg_3230" s="T68">olor-but</ta>
            <ta e="T70" id="Seg_3231" s="T69">dogor-o</ta>
            <ta e="T71" id="Seg_3232" s="T70">taraj-a</ta>
            <ta e="T72" id="Seg_3233" s="T71">hɨp-pɨt</ta>
            <ta e="T73" id="Seg_3234" s="T72">ihill-e-m-mit-tere</ta>
            <ta e="T74" id="Seg_3235" s="T73">ɨt-tara</ta>
            <ta e="T75" id="Seg_3236" s="T74">ür-bet</ta>
            <ta e="T76" id="Seg_3237" s="T75">bu͡ol-but</ta>
            <ta e="T77" id="Seg_3238" s="T76">kaːm-ar</ta>
            <ta e="T78" id="Seg_3239" s="T77">tɨ͡as</ta>
            <ta e="T79" id="Seg_3240" s="T78">ihill-i-bit</ta>
            <ta e="T80" id="Seg_3241" s="T79">kineːs</ta>
            <ta e="T81" id="Seg_3242" s="T80">u͡ol-a</ta>
            <ta e="T82" id="Seg_3243" s="T81">kuttan-a-bɨn</ta>
            <ta e="T83" id="Seg_3244" s="T82">di͡e-bit</ta>
            <ta e="T84" id="Seg_3245" s="T83">dogor-o</ta>
            <ta e="T85" id="Seg_3246" s="T84">hol</ta>
            <ta e="T86" id="Seg_3247" s="T85">kördük</ta>
            <ta e="T87" id="Seg_3248" s="T86">taraj-a</ta>
            <ta e="T88" id="Seg_3249" s="T87">hɨp-pɨt</ta>
            <ta e="T89" id="Seg_3250" s="T88">aːn</ta>
            <ta e="T90" id="Seg_3251" s="T89">arɨ-ll-ɨ-bɨt</ta>
            <ta e="T91" id="Seg_3252" s="T90">da</ta>
            <ta e="T92" id="Seg_3253" s="T91">hu͡on</ta>
            <ta e="T93" id="Seg_3254" s="T92">bagajɨ</ta>
            <ta e="T94" id="Seg_3255" s="T93">dʼaktar</ta>
            <ta e="T95" id="Seg_3256" s="T94">arɨččɨ</ta>
            <ta e="T96" id="Seg_3257" s="T95">bat-an</ta>
            <ta e="T97" id="Seg_3258" s="T96">kiːr-bit</ta>
            <ta e="T98" id="Seg_3259" s="T97">čöl</ta>
            <ta e="T99" id="Seg_3260" s="T98">oguru͡o</ta>
            <ta e="T100" id="Seg_3261" s="T99">taŋas-taːk</ta>
            <ta e="T101" id="Seg_3262" s="T100">oguru͡o</ta>
            <ta e="T102" id="Seg_3263" s="T101">bergehe-leːk</ta>
            <ta e="T103" id="Seg_3264" s="T102">baːj</ta>
            <ta e="T104" id="Seg_3265" s="T103">bagajɨ</ta>
            <ta e="T105" id="Seg_3266" s="T104">dʼühün-neːk</ta>
            <ta e="T106" id="Seg_3267" s="T105">staršina</ta>
            <ta e="T107" id="Seg_3268" s="T106">u͡ol-a</ta>
            <ta e="T108" id="Seg_3269" s="T107">dogor-u-n</ta>
            <ta e="T109" id="Seg_3270" s="T108">čaːj-daː</ta>
            <ta e="T110" id="Seg_3271" s="T109">ɨ͡aldʼɨt-ɨ</ta>
            <ta e="T111" id="Seg_3272" s="T110">dʼi͡e-bit</ta>
            <ta e="T112" id="Seg_3273" s="T111">kineːs</ta>
            <ta e="T113" id="Seg_3274" s="T112">u͡ol-a</ta>
            <ta e="T114" id="Seg_3275" s="T113">tur-an</ta>
            <ta e="T115" id="Seg_3276" s="T114">ohok</ta>
            <ta e="T116" id="Seg_3277" s="T115">ott-u-m-mut</ta>
            <ta e="T117" id="Seg_3278" s="T116">čaːj</ta>
            <ta e="T118" id="Seg_3279" s="T117">kɨːjnʼ-a-r-bat</ta>
            <ta e="T119" id="Seg_3280" s="T118">araj</ta>
            <ta e="T120" id="Seg_3281" s="T119">dʼaktar-dara</ta>
            <ta e="T121" id="Seg_3282" s="T120">hɨraj-ɨ-n</ta>
            <ta e="T122" id="Seg_3283" s="T121">köllʼör-böt</ta>
            <ta e="T123" id="Seg_3284" s="T122">haŋar-s-ɨ-bat</ta>
            <ta e="T124" id="Seg_3285" s="T123">bu</ta>
            <ta e="T125" id="Seg_3286" s="T124">u͡ol</ta>
            <ta e="T126" id="Seg_3287" s="T125">balɨk</ta>
            <ta e="T127" id="Seg_3288" s="T126">kü͡öh-e</ta>
            <ta e="T128" id="Seg_3289" s="T127">hɨ͡a-lam-mɨt</ta>
            <ta e="T129" id="Seg_3290" s="T128">ispiːčke</ta>
            <ta e="T130" id="Seg_3291" s="T129">mah-ɨ-nan</ta>
            <ta e="T131" id="Seg_3292" s="T130">karag-ɨ-n</ta>
            <ta e="T132" id="Seg_3293" s="T131">tepter-en</ta>
            <ta e="T133" id="Seg_3294" s="T132">baran</ta>
            <ta e="T134" id="Seg_3295" s="T133">kör-büt-e</ta>
            <ta e="T135" id="Seg_3296" s="T134">dʼaktar</ta>
            <ta e="T136" id="Seg_3297" s="T135">kül-e</ta>
            <ta e="T137" id="Seg_3298" s="T136">olor-or</ta>
            <ta e="T138" id="Seg_3299" s="T137">hogotok</ta>
            <ta e="T139" id="Seg_3300" s="T138">ire</ta>
            <ta e="T140" id="Seg_3301" s="T139">tiːs-teːk</ta>
            <ta e="T141" id="Seg_3302" s="T140">e-bit</ta>
            <ta e="T142" id="Seg_3303" s="T141">mas</ta>
            <ta e="T143" id="Seg_3304" s="T142">killer-e</ta>
            <ta e="T144" id="Seg_3305" s="T143">taks-ɨ-bɨt</ta>
            <ta e="T145" id="Seg_3306" s="T144">ɨt-tarɨ-n</ta>
            <ta e="T146" id="Seg_3307" s="T145">kölüj-teleː-n</ta>
            <ta e="T147" id="Seg_3308" s="T146">keːs-pit</ta>
            <ta e="T148" id="Seg_3309" s="T147">kör-büt-e</ta>
            <ta e="T149" id="Seg_3310" s="T148">dʼaktar</ta>
            <ta e="T150" id="Seg_3311" s="T149">hɨrga-tɨ-gar</ta>
            <ta e="T151" id="Seg_3312" s="T150">alta</ta>
            <ta e="T152" id="Seg_3313" s="T151">ɨt</ta>
            <ta e="T153" id="Seg_3314" s="T152">kölü-ll-ü-büt</ta>
            <ta e="T154" id="Seg_3315" s="T153">kiːr-en</ta>
            <ta e="T155" id="Seg_3316" s="T154">baran</ta>
            <ta e="T156" id="Seg_3317" s="T155">ɨ͡aldʼɨt-ɨ-n</ta>
            <ta e="T157" id="Seg_3318" s="T156">ah-a-p-pɨt</ta>
            <ta e="T158" id="Seg_3319" s="T157">buːs</ta>
            <ta e="T159" id="Seg_3320" s="T158">killest-e</ta>
            <ta e="T160" id="Seg_3321" s="T159">taks-ɨ͡a-m</ta>
            <ta e="T161" id="Seg_3322" s="T160">dʼi͡e-n</ta>
            <ta e="T162" id="Seg_3323" s="T161">baran</ta>
            <ta e="T163" id="Seg_3324" s="T162">tanas-tar-ɨ-n</ta>
            <ta e="T164" id="Seg_3325" s="T163">kɨbɨn-an</ta>
            <ta e="T165" id="Seg_3326" s="T164">tahaːr-bɨt</ta>
            <ta e="T166" id="Seg_3327" s="T165">ɨt-ɨ-n</ta>
            <ta e="T167" id="Seg_3328" s="T166">hɨrga-tɨ-gar</ta>
            <ta e="T168" id="Seg_3329" s="T167">uːr-an</ta>
            <ta e="T169" id="Seg_3330" s="T168">keːs-pit</ta>
            <ta e="T170" id="Seg_3331" s="T169">onton</ta>
            <ta e="T171" id="Seg_3332" s="T170">kiːr-bit</ta>
            <ta e="T172" id="Seg_3333" s="T171">dʼaktar-ɨ</ta>
            <ta e="T173" id="Seg_3334" s="T172">olor-but</ta>
            <ta e="T174" id="Seg_3335" s="T173">hir-i-ger</ta>
            <ta e="T175" id="Seg_3336" s="T174">utu-p-put</ta>
            <ta e="T176" id="Seg_3337" s="T175">tiriː</ta>
            <ta e="T177" id="Seg_3338" s="T176">bi͡er-bit</ta>
            <ta e="T178" id="Seg_3339" s="T177">telg-e-ni͡en</ta>
            <ta e="T179" id="Seg_3340" s="T178">balɨk</ta>
            <ta e="T180" id="Seg_3341" s="T179">hɨ͡a-ta</ta>
            <ta e="T181" id="Seg_3342" s="T180">hunuːr-darɨ-n</ta>
            <ta e="T182" id="Seg_3343" s="T181">utut-an</ta>
            <ta e="T183" id="Seg_3344" s="T182">keːs-pit-ter</ta>
            <ta e="T184" id="Seg_3345" s="T183">utuj-but-tar</ta>
            <ta e="T185" id="Seg_3346" s="T184">staršina</ta>
            <ta e="T186" id="Seg_3347" s="T185">u͡ol-a</ta>
            <ta e="T187" id="Seg_3348" s="T186">dogor-u-n</ta>
            <ta e="T188" id="Seg_3349" s="T187">ep-pit</ta>
            <ta e="T189" id="Seg_3350" s="T188">doː</ta>
            <ta e="T190" id="Seg_3351" s="T189">man-tɨ-bɨt</ta>
            <ta e="T191" id="Seg_3352" s="T190">kɨːs</ta>
            <ta e="T192" id="Seg_3353" s="T191">bu͡ol-bataːk</ta>
            <ta e="T193" id="Seg_3354" s="T192">kɨːs-t-ɨ͡ak</ta>
            <ta e="T194" id="Seg_3355" s="T193">tug-u-n</ta>
            <ta e="T195" id="Seg_3356" s="T194">kuttan-a-gɨn</ta>
            <ta e="T196" id="Seg_3357" s="T195">min</ta>
            <ta e="T197" id="Seg_3358" s="T196">oččogo</ta>
            <ta e="T198" id="Seg_3359" s="T197">bar-ɨ͡a-m</ta>
            <ta e="T199" id="Seg_3360" s="T198">dogor-o</ta>
            <ta e="T200" id="Seg_3361" s="T199">min</ta>
            <ta e="T201" id="Seg_3362" s="T200">taks-ɨ-bɨt-ɨ-m</ta>
            <ta e="T202" id="Seg_3363" s="T201">kenne</ta>
            <ta e="T203" id="Seg_3364" s="T202">kɨːs-ka-r</ta>
            <ta e="T204" id="Seg_3365" s="T203">bar-aːr</ta>
            <ta e="T205" id="Seg_3366" s="T204">taks-an</ta>
            <ta e="T206" id="Seg_3367" s="T205">kaːl-bɨt</ta>
            <ta e="T207" id="Seg_3368" s="T206">taŋn-an</ta>
            <ta e="T208" id="Seg_3369" s="T207">baran</ta>
            <ta e="T209" id="Seg_3370" s="T208">hɨrga-tɨ-gar</ta>
            <ta e="T210" id="Seg_3371" s="T209">olor-but</ta>
            <ta e="T211" id="Seg_3372" s="T210">töhö</ta>
            <ta e="T212" id="Seg_3373" s="T211">bu͡ol-u͡o=j</ta>
            <ta e="T213" id="Seg_3374" s="T212">öl-ör-kaːl-ar</ta>
            <ta e="T214" id="Seg_3375" s="T213">üːgü</ta>
            <ta e="T215" id="Seg_3376" s="T214">ihill-i-bit</ta>
            <ta e="T216" id="Seg_3377" s="T215">araː</ta>
            <ta e="T217" id="Seg_3378" s="T216">abɨraː</ta>
            <ta e="T218" id="Seg_3379" s="T217">bu</ta>
            <ta e="T219" id="Seg_3380" s="T218">u͡ol</ta>
            <ta e="T220" id="Seg_3381" s="T219">nʼu͡oŋu-tu-n</ta>
            <ta e="T221" id="Seg_3382" s="T220">ɨl-l-a</ta>
            <ta e="T222" id="Seg_3383" s="T221">daːganɨ</ta>
            <ta e="T223" id="Seg_3384" s="T222">kötüt-en</ta>
            <ta e="T224" id="Seg_3385" s="T223">kaːl-bɨt</ta>
            <ta e="T225" id="Seg_3386" s="T224">kenn-i-tten</ta>
            <ta e="T226" id="Seg_3387" s="T225">dʼaktar</ta>
            <ta e="T227" id="Seg_3388" s="T226">haŋa-ta</ta>
            <ta e="T228" id="Seg_3389" s="T227">ihill-i-bit</ta>
            <ta e="T229" id="Seg_3390" s="T228">bil-bit-i-m</ta>
            <ta e="T230" id="Seg_3391" s="T229">bu͡ol-lar</ta>
            <ta e="T231" id="Seg_3392" s="T230">hi͡e-n</ta>
            <ta e="T232" id="Seg_3393" s="T231">keːh-i͡e</ta>
            <ta e="T233" id="Seg_3394" s="T232">e-ti-m</ta>
            <ta e="T234" id="Seg_3395" s="T233">bu͡o</ta>
            <ta e="T235" id="Seg_3396" s="T234">kel-en</ta>
            <ta e="T236" id="Seg_3397" s="T235">bu</ta>
            <ta e="T237" id="Seg_3398" s="T236">u͡ol</ta>
            <ta e="T238" id="Seg_3399" s="T237">aga-tɨ-gar</ta>
            <ta e="T239" id="Seg_3400" s="T238">kineːs-ke</ta>
            <ta e="T240" id="Seg_3401" s="T239">kepseː-bit</ta>
            <ta e="T241" id="Seg_3402" s="T240">kineːh-i</ta>
            <ta e="T242" id="Seg_3403" s="T241">gɨtta</ta>
            <ta e="T243" id="Seg_3404" s="T242">staršina</ta>
            <ta e="T244" id="Seg_3405" s="T243">hette</ta>
            <ta e="T245" id="Seg_3406" s="T244">dʼi͡e-ni</ta>
            <ta e="T246" id="Seg_3407" s="T245">komuj-an</ta>
            <ta e="T247" id="Seg_3408" s="T246">munnʼak-taː-bɨt-tar</ta>
            <ta e="T248" id="Seg_3409" s="T247">ikki</ta>
            <ta e="T249" id="Seg_3410" s="T248">u͡ol-u</ta>
            <ta e="T250" id="Seg_3411" s="T249">ɨːp-pɨt-tar</ta>
            <ta e="T251" id="Seg_3412" s="T250">kör-ü͡ök-teri-n</ta>
            <ta e="T252" id="Seg_3413" s="T251">ikki</ta>
            <ta e="T253" id="Seg_3414" s="T252">u͡ol</ta>
            <ta e="T254" id="Seg_3415" s="T253">bu͡ol-an</ta>
            <ta e="T255" id="Seg_3416" s="T254">bar-an</ta>
            <ta e="T256" id="Seg_3417" s="T255">kaːl-lɨ-lar</ta>
            <ta e="T257" id="Seg_3418" s="T256">ör</ta>
            <ta e="T258" id="Seg_3419" s="T257">bu͡ol-u͡ok-tara=j</ta>
            <ta e="T259" id="Seg_3420" s="T258">hɨrdɨk-ka</ta>
            <ta e="T260" id="Seg_3421" s="T259">kel-bit-ter</ta>
            <ta e="T261" id="Seg_3422" s="T260">kör-büt-tere</ta>
            <ta e="T262" id="Seg_3423" s="T261">biːr</ta>
            <ta e="T263" id="Seg_3424" s="T262">da</ta>
            <ta e="T264" id="Seg_3425" s="T263">kihi</ta>
            <ta e="T265" id="Seg_3426" s="T264">hu͡ok</ta>
            <ta e="T266" id="Seg_3427" s="T265">hu͡ol</ta>
            <ta e="T267" id="Seg_3428" s="T266">da</ta>
            <ta e="T268" id="Seg_3429" s="T267">hu͡ok</ta>
            <ta e="T269" id="Seg_3430" s="T268">uraha</ta>
            <ta e="T270" id="Seg_3431" s="T269">dʼi͡e-ge</ta>
            <ta e="T271" id="Seg_3432" s="T270">kiːr-bit-tere</ta>
            <ta e="T272" id="Seg_3433" s="T271">u͡ol</ta>
            <ta e="T273" id="Seg_3434" s="T272">oŋu͡og-a</ta>
            <ta e="T274" id="Seg_3435" s="T273">ire</ta>
            <ta e="T275" id="Seg_3436" s="T274">dardaj-a</ta>
            <ta e="T276" id="Seg_3437" s="T275">hɨt-ar</ta>
            <ta e="T277" id="Seg_3438" s="T276">bu</ta>
            <ta e="T278" id="Seg_3439" s="T277">ikki</ta>
            <ta e="T279" id="Seg_3440" s="T278">u͡ol</ta>
            <ta e="T280" id="Seg_3441" s="T279">tönn-ü-büt-ter</ta>
            <ta e="T281" id="Seg_3442" s="T280">kineːs-ke</ta>
            <ta e="T282" id="Seg_3443" s="T281">kepseː-bit-ter</ta>
            <ta e="T283" id="Seg_3444" s="T282">biːr</ta>
            <ta e="T284" id="Seg_3445" s="T283">tulaːjak</ta>
            <ta e="T285" id="Seg_3446" s="T284">ogo</ta>
            <ta e="T286" id="Seg_3447" s="T285">baːr</ta>
            <ta e="T287" id="Seg_3448" s="T286">ikki</ta>
            <ta e="T288" id="Seg_3449" s="T287">hannɨ-ttan</ta>
            <ta e="T289" id="Seg_3450" s="T288">tarakaːj</ta>
            <ta e="T290" id="Seg_3451" s="T289">hogotok</ta>
            <ta e="T291" id="Seg_3452" s="T290">inʼe-leːk</ta>
            <ta e="T292" id="Seg_3453" s="T291">biːr</ta>
            <ta e="T293" id="Seg_3454" s="T292">taba-laːk</ta>
            <ta e="T294" id="Seg_3455" s="T293">kineːh-i</ta>
            <ta e="T295" id="Seg_3456" s="T294">gɨtta</ta>
            <ta e="T296" id="Seg_3457" s="T295">staršina</ta>
            <ta e="T297" id="Seg_3458" s="T296">munnʼak-taː-bɨt-tar</ta>
            <ta e="T298" id="Seg_3459" s="T297">kajdak</ta>
            <ta e="T299" id="Seg_3460" s="T298">bu͡ol-u͡ok-put=uj</ta>
            <ta e="T300" id="Seg_3461" s="T299">hir-biti-n</ta>
            <ta e="T301" id="Seg_3462" s="T300">bɨrag-ɨ͡ak-pɨt</ta>
            <ta e="T302" id="Seg_3463" s="T301">duː</ta>
            <ta e="T303" id="Seg_3464" s="T302">emi͡e</ta>
            <ta e="T304" id="Seg_3465" s="T303">da</ta>
            <ta e="T305" id="Seg_3466" s="T304">paːs-tar-bɨt</ta>
            <ta e="T307" id="Seg_3467" s="T305">baːl-lar</ta>
            <ta e="T308" id="Seg_3468" s="T307">d-e-s-pit-ter</ta>
            <ta e="T309" id="Seg_3469" s="T308">staršina</ta>
            <ta e="T310" id="Seg_3470" s="T309">onno</ta>
            <ta e="T311" id="Seg_3471" s="T310">tur-an</ta>
            <ta e="T312" id="Seg_3472" s="T311">ep-pit</ta>
            <ta e="T313" id="Seg_3473" s="T312">tulaːjak</ta>
            <ta e="T314" id="Seg_3474" s="T313">ogo-nu</ta>
            <ta e="T315" id="Seg_3475" s="T314">ɨːt-ɨ͡ak-ka</ta>
            <ta e="T316" id="Seg_3476" s="T315">min</ta>
            <ta e="T317" id="Seg_3477" s="T316">kɨːs-pɨ-n</ta>
            <ta e="T318" id="Seg_3478" s="T317">da</ta>
            <ta e="T319" id="Seg_3479" s="T318">bi͡er-i͡e-m</ta>
            <ta e="T320" id="Seg_3480" s="T319">e-t-e</ta>
            <ta e="T321" id="Seg_3481" s="T320">ölör-dög-üne</ta>
            <ta e="T322" id="Seg_3482" s="T321">bu</ta>
            <ta e="T323" id="Seg_3483" s="T322">u͡ol-u</ta>
            <ta e="T324" id="Seg_3484" s="T323">ɨgɨr-bɨt-tar</ta>
            <ta e="T325" id="Seg_3485" s="T324">kepseː-bit-ter</ta>
            <ta e="T326" id="Seg_3486" s="T325">barɨ-tɨ-n</ta>
            <ta e="T327" id="Seg_3487" s="T326">dʼe</ta>
            <ta e="T328" id="Seg_3488" s="T327">üleger</ta>
            <ta e="T329" id="Seg_3489" s="T328">e-bit</ta>
            <ta e="T330" id="Seg_3490" s="T329">ogo</ta>
            <ta e="T331" id="Seg_3491" s="T330">muŋ-naːk</ta>
            <ta e="T332" id="Seg_3492" s="T331">hogotogun</ta>
            <ta e="T333" id="Seg_3493" s="T332">bar-ar</ta>
            <ta e="T334" id="Seg_3494" s="T333">bu͡ol-l-a</ta>
            <ta e="T335" id="Seg_3495" s="T334">ɨt</ta>
            <ta e="T336" id="Seg_3496" s="T335">ire</ta>
            <ta e="T337" id="Seg_3497" s="T336">bi͡er-bit-ter</ta>
            <ta e="T338" id="Seg_3498" s="T337">haː</ta>
            <ta e="T339" id="Seg_3499" s="T338">da</ta>
            <ta e="T340" id="Seg_3500" s="T339">tu͡ok</ta>
            <ta e="T341" id="Seg_3501" s="T340">da</ta>
            <ta e="T342" id="Seg_3502" s="T341">hu͡ok</ta>
            <ta e="T343" id="Seg_3503" s="T342">dʼi͡e-ti-ger</ta>
            <ta e="T344" id="Seg_3504" s="T343">kel-en</ta>
            <ta e="T345" id="Seg_3505" s="T344">ɨt-a-n-a</ta>
            <ta e="T346" id="Seg_3506" s="T345">olor-but</ta>
            <ta e="T347" id="Seg_3507" s="T346">inʼe-te</ta>
            <ta e="T348" id="Seg_3508" s="T347">tu͡ok</ta>
            <ta e="T349" id="Seg_3509" s="T348">bu͡ol-lu-ŋ</ta>
            <ta e="T350" id="Seg_3510" s="T349">kepseː-bit</ta>
            <ta e="T351" id="Seg_3511" s="T350">enʼi</ta>
            <ta e="T352" id="Seg_3512" s="T351">aga-ŋ</ta>
            <ta e="T353" id="Seg_3513" s="T352">batɨja-ta</ta>
            <ta e="T354" id="Seg_3514" s="T353">kanna</ta>
            <ta e="T355" id="Seg_3515" s="T354">ire</ta>
            <ta e="T356" id="Seg_3516" s="T355">baːr</ta>
            <ta e="T357" id="Seg_3517" s="T356">e-t-e</ta>
            <ta e="T358" id="Seg_3518" s="T357">o-nu</ta>
            <ta e="T359" id="Seg_3519" s="T358">ilʼlʼ-eːr</ta>
            <ta e="T360" id="Seg_3520" s="T359">inʼe-te</ta>
            <ta e="T361" id="Seg_3521" s="T360">muŋ-naːk</ta>
            <ta e="T362" id="Seg_3522" s="T361">kam</ta>
            <ta e="T363" id="Seg_3523" s="T362">dʼebintij-bit</ta>
            <ta e="T364" id="Seg_3524" s="T363">batɨja-nɨ</ta>
            <ta e="T365" id="Seg_3525" s="T364">killer-bit</ta>
            <ta e="T366" id="Seg_3526" s="T365">bu</ta>
            <ta e="T367" id="Seg_3527" s="T366">u͡ol</ta>
            <ta e="T368" id="Seg_3528" s="T367">ör</ta>
            <ta e="T369" id="Seg_3529" s="T368">bu͡ol-bakka</ta>
            <ta e="T370" id="Seg_3530" s="T369">bar-an</ta>
            <ta e="T371" id="Seg_3531" s="T370">kaːl-bɨt</ta>
            <ta e="T372" id="Seg_3532" s="T371">bɨst-ɨ͡a</ta>
            <ta e="T373" id="Seg_3533" s="T372">duː</ta>
            <ta e="T374" id="Seg_3534" s="T373">kel-bit-e</ta>
            <ta e="T375" id="Seg_3535" s="T374">kim</ta>
            <ta e="T376" id="Seg_3536" s="T375">da</ta>
            <ta e="T377" id="Seg_3537" s="T376">hu͡ok</ta>
            <ta e="T378" id="Seg_3538" s="T377">kihi</ta>
            <ta e="T379" id="Seg_3539" s="T378">ire</ta>
            <ta e="T380" id="Seg_3540" s="T379">oŋu͡og-a</ta>
            <ta e="T381" id="Seg_3541" s="T380">očiha-ga</ta>
            <ta e="T382" id="Seg_3542" s="T381">im</ta>
            <ta e="T383" id="Seg_3543" s="T382">tüs-püt-ü-n</ta>
            <ta e="T384" id="Seg_3544" s="T383">kenne</ta>
            <ta e="T385" id="Seg_3545" s="T384">hunuːr-u-n</ta>
            <ta e="T386" id="Seg_3546" s="T385">ubap-pɨt</ta>
            <ta e="T387" id="Seg_3547" s="T386">kihi</ta>
            <ta e="T388" id="Seg_3548" s="T387">utuj-ar</ta>
            <ta e="T389" id="Seg_3549" s="T388">kem-i-n</ta>
            <ta e="T390" id="Seg_3550" s="T389">dʼi͡ek</ta>
            <ta e="T391" id="Seg_3551" s="T390">hɨrga-laːk</ta>
            <ta e="T392" id="Seg_3552" s="T391">tɨ͡ah-a</ta>
            <ta e="T393" id="Seg_3553" s="T392">ihill-i-bit</ta>
            <ta e="T394" id="Seg_3554" s="T393">ɨt</ta>
            <ta e="T395" id="Seg_3555" s="T394">da</ta>
            <ta e="T396" id="Seg_3556" s="T395">ür-betek</ta>
            <ta e="T397" id="Seg_3557" s="T396">u͡ot-u-n</ta>
            <ta e="T398" id="Seg_3558" s="T397">utut-an</ta>
            <ta e="T399" id="Seg_3559" s="T398">baran</ta>
            <ta e="T400" id="Seg_3560" s="T399">olor-but</ta>
            <ta e="T401" id="Seg_3561" s="T400">tɨ͡as</ta>
            <ta e="T402" id="Seg_3562" s="T401">bögö</ta>
            <ta e="T403" id="Seg_3563" s="T402">bu͡ol-but</ta>
            <ta e="T404" id="Seg_3564" s="T403">aːŋ-ŋa</ta>
            <ta e="T405" id="Seg_3565" s="T404">bap-pat</ta>
            <ta e="T406" id="Seg_3566" s="T405">hu͡on</ta>
            <ta e="T407" id="Seg_3567" s="T406">dʼaktar</ta>
            <ta e="T408" id="Seg_3568" s="T407">kiːr-bit</ta>
            <ta e="T409" id="Seg_3569" s="T408">anaːr</ta>
            <ta e="T410" id="Seg_3570" s="T409">atag-ɨ-n</ta>
            <ta e="T411" id="Seg_3571" s="T410">aːŋ-ŋa</ta>
            <ta e="T412" id="Seg_3572" s="T411">annʼ-ar-ɨ-n</ta>
            <ta e="T413" id="Seg_3573" s="T412">gɨtta</ta>
            <ta e="T414" id="Seg_3574" s="T413">ikki</ta>
            <ta e="T415" id="Seg_3575" s="T414">konnog-u-n</ta>
            <ta e="T416" id="Seg_3576" s="T415">ann-ɨ-nan</ta>
            <ta e="T417" id="Seg_3577" s="T416">batɨja-nan</ta>
            <ta e="T418" id="Seg_3578" s="T417">batarɨ</ta>
            <ta e="T419" id="Seg_3579" s="T418">as-pɨt</ta>
            <ta e="T420" id="Seg_3580" s="T419">beje-te</ta>
            <ta e="T421" id="Seg_3581" s="T420">konnog-u-n</ta>
            <ta e="T422" id="Seg_3582" s="T421">ann-ɨ-nan</ta>
            <ta e="T423" id="Seg_3583" s="T422">küreː-bit</ta>
            <ta e="T424" id="Seg_3584" s="T423">tüːn</ta>
            <ta e="T425" id="Seg_3585" s="T424">dʼi͡e-ti-ger</ta>
            <ta e="T426" id="Seg_3586" s="T425">kel-bit</ta>
            <ta e="T427" id="Seg_3587" s="T426">inʼe-te</ta>
            <ta e="T428" id="Seg_3588" s="T427">muŋ-naːk</ta>
            <ta e="T429" id="Seg_3589" s="T428">utuj-bakka</ta>
            <ta e="T430" id="Seg_3590" s="T429">küːt-en</ta>
            <ta e="T431" id="Seg_3591" s="T430">harsi͡erde</ta>
            <ta e="T432" id="Seg_3592" s="T431">inʼe-te</ta>
            <ta e="T433" id="Seg_3593" s="T432">kineːs-ke</ta>
            <ta e="T434" id="Seg_3594" s="T433">bar-an</ta>
            <ta e="T435" id="Seg_3595" s="T434">kepseː-bit</ta>
            <ta e="T436" id="Seg_3596" s="T435">kineːs</ta>
            <ta e="T437" id="Seg_3597" s="T436">bu</ta>
            <ta e="T438" id="Seg_3598" s="T437">u͡ol-u</ta>
            <ta e="T439" id="Seg_3599" s="T438">ɨgɨr-bɨt</ta>
            <ta e="T440" id="Seg_3600" s="T439">dʼe</ta>
            <ta e="T441" id="Seg_3601" s="T440">kajdag=ɨj</ta>
            <ta e="T442" id="Seg_3602" s="T441">ölör-dü-ŋ</ta>
            <ta e="T443" id="Seg_3603" s="T442">duː</ta>
            <ta e="T444" id="Seg_3604" s="T443">bil-bep-pin</ta>
            <ta e="T445" id="Seg_3605" s="T444">di͡e-bit</ta>
            <ta e="T446" id="Seg_3606" s="T445">u͡ol</ta>
            <ta e="T447" id="Seg_3607" s="T446">kepseː-bit</ta>
            <ta e="T448" id="Seg_3608" s="T447">kajtak</ta>
            <ta e="T449" id="Seg_3609" s="T448">gɨm-mɨt-ɨ-n</ta>
            <ta e="T450" id="Seg_3610" s="T449">munnʼak-taː-bɨt-tar</ta>
            <ta e="T451" id="Seg_3611" s="T450">alta</ta>
            <ta e="T452" id="Seg_3612" s="T451">kihi-ni</ta>
            <ta e="T453" id="Seg_3613" s="T452">köl-lʼör-ö</ta>
            <ta e="T454" id="Seg_3614" s="T453">ɨːp-pɨt-tar</ta>
            <ta e="T455" id="Seg_3615" s="T454">staršina</ta>
            <ta e="T456" id="Seg_3616" s="T455">emi͡e</ta>
            <ta e="T457" id="Seg_3617" s="T456">bars-ɨ-bɨt</ta>
            <ta e="T458" id="Seg_3618" s="T457">taba-nnan</ta>
            <ta e="T459" id="Seg_3619" s="T458">gɨtta</ta>
            <ta e="T460" id="Seg_3620" s="T459">at-ɨ-nan</ta>
            <ta e="T461" id="Seg_3621" s="T460">bar-bɨt-tar</ta>
            <ta e="T462" id="Seg_3622" s="T461">tij-bit-ter</ta>
            <ta e="T463" id="Seg_3623" s="T462">künüs</ta>
            <ta e="T464" id="Seg_3624" s="T463">össü͡ö</ta>
            <ta e="T465" id="Seg_3625" s="T464">biːr</ta>
            <ta e="T466" id="Seg_3626" s="T465">da</ta>
            <ta e="T467" id="Seg_3627" s="T466">kihi</ta>
            <ta e="T468" id="Seg_3628" s="T467">hu͡ok</ta>
            <ta e="T469" id="Seg_3629" s="T468">dʼi͡e-leri-n</ta>
            <ta e="T470" id="Seg_3630" s="T469">potolu͡og-a</ta>
            <ta e="T471" id="Seg_3631" s="T470">kajd-ɨ-bɨt</ta>
            <ta e="T472" id="Seg_3632" s="T471">aːn-a</ta>
            <ta e="T473" id="Seg_3633" s="T472">arɨ-ll-an</ta>
            <ta e="T474" id="Seg_3634" s="T473">tur-ar</ta>
            <ta e="T475" id="Seg_3635" s="T474">dʼi͡e</ta>
            <ta e="T476" id="Seg_3636" s="T475">ih-i-n</ta>
            <ta e="T477" id="Seg_3637" s="T476">kör-büt-tere</ta>
            <ta e="T478" id="Seg_3638" s="T477">kɨːs</ta>
            <ta e="T479" id="Seg_3639" s="T478">ih-e</ta>
            <ta e="T480" id="Seg_3640" s="T479">üll-en</ta>
            <ta e="T481" id="Seg_3641" s="T480">bar-an</ta>
            <ta e="T482" id="Seg_3642" s="T481">hɨt-ar</ta>
            <ta e="T483" id="Seg_3643" s="T482">staršina</ta>
            <ta e="T484" id="Seg_3644" s="T483">dʼi͡e-ni</ta>
            <ta e="T485" id="Seg_3645" s="T484">ubat-ɨ͡agɨŋ</ta>
            <ta e="T486" id="Seg_3646" s="T485">bu</ta>
            <ta e="T487" id="Seg_3647" s="T486">dʼi͡e-ni</ta>
            <ta e="T488" id="Seg_3648" s="T487">kɨːs-tɨːn</ta>
            <ta e="T489" id="Seg_3649" s="T488">ubat-an</ta>
            <ta e="T490" id="Seg_3650" s="T489">keːs-pit-ter</ta>
            <ta e="T491" id="Seg_3651" s="T490">tarakaːj</ta>
            <ta e="T492" id="Seg_3652" s="T491">u͡ol</ta>
            <ta e="T493" id="Seg_3653" s="T492">staršina</ta>
            <ta e="T494" id="Seg_3654" s="T493">kɨːh-ɨ-n</ta>
            <ta e="T495" id="Seg_3655" s="T494">ɨl-an</ta>
            <ta e="T496" id="Seg_3656" s="T495">baj-an-tot-on</ta>
            <ta e="T497" id="Seg_3657" s="T496">olor-but</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_3658" s="T0">kineːs-nI</ta>
            <ta e="T2" id="Seg_3659" s="T1">kɨtta</ta>
            <ta e="T3" id="Seg_3660" s="T2">staršina</ta>
            <ta e="T4" id="Seg_3661" s="T3">biːr-LIː</ta>
            <ta e="T5" id="Seg_3662" s="T4">u͡ol-LAːK-LAr</ta>
            <ta e="T6" id="Seg_3663" s="T5">olor-BIT-LAr</ta>
            <ta e="T7" id="Seg_3664" s="T6">e-BIT</ta>
            <ta e="T8" id="Seg_3665" s="T7">ikki-IAn-LArA</ta>
            <ta e="T9" id="Seg_3666" s="T8">tɨ͡a</ta>
            <ta e="T10" id="Seg_3667" s="T9">kihi-LArA</ta>
            <ta e="T11" id="Seg_3668" s="T10">u͡ol-LAr</ta>
            <ta e="T12" id="Seg_3669" s="T11">biːrge</ta>
            <ta e="T13" id="Seg_3670" s="T12">ü͡öskeː-An</ta>
            <ta e="T14" id="Seg_3671" s="T13">dogor-LIː</ta>
            <ta e="T15" id="Seg_3672" s="T14">bagajɨ</ta>
            <ta e="T16" id="Seg_3673" s="T15">e-BIT-LArA</ta>
            <ta e="T17" id="Seg_3674" s="T16">bu</ta>
            <ta e="T18" id="Seg_3675" s="T17">kihi-LAr</ta>
            <ta e="T19" id="Seg_3676" s="T18">bɨlɨrgɨ-ttAn</ta>
            <ta e="T20" id="Seg_3677" s="T19">kös-A</ta>
            <ta e="T21" id="Seg_3678" s="T20">hɨrɨt-Ar</ta>
            <ta e="T22" id="Seg_3679" s="T21">e-BIT-LArA</ta>
            <ta e="T23" id="Seg_3680" s="T22">agaj</ta>
            <ta e="T24" id="Seg_3681" s="T23">kühün</ta>
            <ta e="T25" id="Seg_3682" s="T24">paːs</ta>
            <ta e="T26" id="Seg_3683" s="T25">iːt-Ar</ta>
            <ta e="T27" id="Seg_3684" s="T26">kem</ta>
            <ta e="T28" id="Seg_3685" s="T27">bu͡ol-BIT</ta>
            <ta e="T29" id="Seg_3686" s="T28">as-LArI-n</ta>
            <ta e="T30" id="Seg_3687" s="T29">erdetten</ta>
            <ta e="T31" id="Seg_3688" s="T30">belem-LAː-An</ta>
            <ta e="T32" id="Seg_3689" s="T31">keːs-BIT-LAr</ta>
            <ta e="T33" id="Seg_3690" s="T32">ikki</ta>
            <ta e="T34" id="Seg_3691" s="T33">u͡ol</ta>
            <ta e="T35" id="Seg_3692" s="T34">ɨt-tI-nAn</ta>
            <ta e="T36" id="Seg_3693" s="T35">paːs-LAː-A</ta>
            <ta e="T37" id="Seg_3694" s="T36">bar-BIT-LAr</ta>
            <ta e="T38" id="Seg_3695" s="T37">paːs</ta>
            <ta e="T39" id="Seg_3696" s="T38">iːt-n-An-iːt-n-An</ta>
            <ta e="T40" id="Seg_3697" s="T39">baran</ta>
            <ta e="T41" id="Seg_3698" s="T40">očiha-LArI-GAr</ta>
            <ta e="T42" id="Seg_3699" s="T41">kon-BIT-LAr</ta>
            <ta e="T43" id="Seg_3700" s="T42">biːrde</ta>
            <ta e="T44" id="Seg_3701" s="T43">ki͡ehe</ta>
            <ta e="T45" id="Seg_3702" s="T44">ɨt-LArI-n</ta>
            <ta e="T46" id="Seg_3703" s="T45">baːj-An</ta>
            <ta e="T47" id="Seg_3704" s="T46">baran</ta>
            <ta e="T48" id="Seg_3705" s="T47">balɨk</ta>
            <ta e="T49" id="Seg_3706" s="T48">hɨ͡a-tI-nAn</ta>
            <ta e="T50" id="Seg_3707" s="T49">eli͡eke</ta>
            <ta e="T51" id="Seg_3708" s="T50">oŋohun-I-BIT-LAr</ta>
            <ta e="T52" id="Seg_3709" s="T51">čaːj</ta>
            <ta e="T53" id="Seg_3710" s="T52">is-A</ta>
            <ta e="T54" id="Seg_3711" s="T53">olor-BIT-LAr</ta>
            <ta e="T55" id="Seg_3712" s="T54">agaj</ta>
            <ta e="T56" id="Seg_3713" s="T55">hɨrga-LAːK</ta>
            <ta e="T57" id="Seg_3714" s="T56">tɨ͡as-tA</ta>
            <ta e="T58" id="Seg_3715" s="T57">ihilin-I-BIT</ta>
            <ta e="T59" id="Seg_3716" s="T58">kineːs</ta>
            <ta e="T60" id="Seg_3717" s="T59">u͡ol-tA</ta>
            <ta e="T61" id="Seg_3718" s="T60">dʼi͡e-BIT</ta>
            <ta e="T62" id="Seg_3719" s="T61">ötü͡ö</ta>
            <ta e="T63" id="Seg_3720" s="T62">kihi</ta>
            <ta e="T64" id="Seg_3721" s="T63">kel-BA-TI-tA</ta>
            <ta e="T65" id="Seg_3722" s="T64">bu͡olu͡o</ta>
            <ta e="T66" id="Seg_3723" s="T65">du͡o</ta>
            <ta e="T67" id="Seg_3724" s="T66">beje-tA</ta>
            <ta e="T68" id="Seg_3725" s="T67">tabaːk-LAː-A</ta>
            <ta e="T69" id="Seg_3726" s="T68">olor-BIT</ta>
            <ta e="T70" id="Seg_3727" s="T69">dogor-tA</ta>
            <ta e="T71" id="Seg_3728" s="T70">taraj-A</ta>
            <ta e="T72" id="Seg_3729" s="T71">hɨt-BIT</ta>
            <ta e="T73" id="Seg_3730" s="T72">ihilleː-A-n-BIT-LArA</ta>
            <ta e="T74" id="Seg_3731" s="T73">ɨt-LArA</ta>
            <ta e="T75" id="Seg_3732" s="T74">ür-BAT</ta>
            <ta e="T76" id="Seg_3733" s="T75">bu͡ol-BIT</ta>
            <ta e="T77" id="Seg_3734" s="T76">kaːm-Ar</ta>
            <ta e="T78" id="Seg_3735" s="T77">tɨ͡as</ta>
            <ta e="T79" id="Seg_3736" s="T78">ihilin-I-BIT</ta>
            <ta e="T80" id="Seg_3737" s="T79">kineːs</ta>
            <ta e="T81" id="Seg_3738" s="T80">u͡ol-tA</ta>
            <ta e="T82" id="Seg_3739" s="T81">kuttan-A-BIn</ta>
            <ta e="T83" id="Seg_3740" s="T82">di͡e-BIT</ta>
            <ta e="T84" id="Seg_3741" s="T83">dogor-tA</ta>
            <ta e="T85" id="Seg_3742" s="T84">hol</ta>
            <ta e="T86" id="Seg_3743" s="T85">kördük</ta>
            <ta e="T87" id="Seg_3744" s="T86">taraj-A</ta>
            <ta e="T88" id="Seg_3745" s="T87">hɨt-BIT</ta>
            <ta e="T89" id="Seg_3746" s="T88">aːn</ta>
            <ta e="T90" id="Seg_3747" s="T89">arɨj-LIN-I-BIT</ta>
            <ta e="T91" id="Seg_3748" s="T90">da</ta>
            <ta e="T92" id="Seg_3749" s="T91">hu͡on</ta>
            <ta e="T93" id="Seg_3750" s="T92">bagajɨ</ta>
            <ta e="T94" id="Seg_3751" s="T93">dʼaktar</ta>
            <ta e="T95" id="Seg_3752" s="T94">arɨːččɨ</ta>
            <ta e="T96" id="Seg_3753" s="T95">bat-An</ta>
            <ta e="T97" id="Seg_3754" s="T96">kiːr-BIT</ta>
            <ta e="T98" id="Seg_3755" s="T97">čöl</ta>
            <ta e="T99" id="Seg_3756" s="T98">oguru͡o</ta>
            <ta e="T100" id="Seg_3757" s="T99">taŋas-LAːK</ta>
            <ta e="T101" id="Seg_3758" s="T100">oguru͡o</ta>
            <ta e="T102" id="Seg_3759" s="T101">bergehe-LAːK</ta>
            <ta e="T103" id="Seg_3760" s="T102">baːj</ta>
            <ta e="T104" id="Seg_3761" s="T103">bagajɨ</ta>
            <ta e="T105" id="Seg_3762" s="T104">dʼühün-LAːK</ta>
            <ta e="T106" id="Seg_3763" s="T105">staršina</ta>
            <ta e="T107" id="Seg_3764" s="T106">u͡ol-tA</ta>
            <ta e="T108" id="Seg_3765" s="T107">dogor-tI-n</ta>
            <ta e="T109" id="Seg_3766" s="T108">čaːj-LAː</ta>
            <ta e="T110" id="Seg_3767" s="T109">ɨ͡aldʼɨt-nI</ta>
            <ta e="T111" id="Seg_3768" s="T110">dʼi͡e-BIT</ta>
            <ta e="T112" id="Seg_3769" s="T111">kineːs</ta>
            <ta e="T113" id="Seg_3770" s="T112">u͡ol-tA</ta>
            <ta e="T114" id="Seg_3771" s="T113">tur-An</ta>
            <ta e="T115" id="Seg_3772" s="T114">ačaːk</ta>
            <ta e="T116" id="Seg_3773" s="T115">otut-I-n-BIT</ta>
            <ta e="T117" id="Seg_3774" s="T116">čaːj</ta>
            <ta e="T118" id="Seg_3775" s="T117">kɨːj-A-r-BAT</ta>
            <ta e="T119" id="Seg_3776" s="T118">agaj</ta>
            <ta e="T120" id="Seg_3777" s="T119">dʼaktar-LArA</ta>
            <ta e="T121" id="Seg_3778" s="T120">hɨraj-tI-n</ta>
            <ta e="T122" id="Seg_3779" s="T121">köllör-BAT</ta>
            <ta e="T123" id="Seg_3780" s="T122">haŋar-s-I-BAT</ta>
            <ta e="T124" id="Seg_3781" s="T123">bu</ta>
            <ta e="T125" id="Seg_3782" s="T124">u͡ol</ta>
            <ta e="T126" id="Seg_3783" s="T125">balɨk</ta>
            <ta e="T127" id="Seg_3784" s="T126">kü͡ös-tA</ta>
            <ta e="T128" id="Seg_3785" s="T127">hɨ͡a-LAN-BIT</ta>
            <ta e="T129" id="Seg_3786" s="T128">ispiːčke</ta>
            <ta e="T130" id="Seg_3787" s="T129">mas-tI-nAn</ta>
            <ta e="T131" id="Seg_3788" s="T130">karak-tI-n</ta>
            <ta e="T132" id="Seg_3789" s="T131">tepter-An</ta>
            <ta e="T133" id="Seg_3790" s="T132">baran</ta>
            <ta e="T134" id="Seg_3791" s="T133">kör-BIT-tA</ta>
            <ta e="T135" id="Seg_3792" s="T134">dʼaktar</ta>
            <ta e="T136" id="Seg_3793" s="T135">kül-A</ta>
            <ta e="T137" id="Seg_3794" s="T136">olor-Ar</ta>
            <ta e="T138" id="Seg_3795" s="T137">čogotok</ta>
            <ta e="T139" id="Seg_3796" s="T138">ere</ta>
            <ta e="T140" id="Seg_3797" s="T139">tiːs-LAːK</ta>
            <ta e="T141" id="Seg_3798" s="T140">e-BIT</ta>
            <ta e="T142" id="Seg_3799" s="T141">mas</ta>
            <ta e="T143" id="Seg_3800" s="T142">killer-A</ta>
            <ta e="T144" id="Seg_3801" s="T143">tagɨs-I-BIT</ta>
            <ta e="T145" id="Seg_3802" s="T144">ɨt-LArI-n</ta>
            <ta e="T146" id="Seg_3803" s="T145">kölüj-ItAlAː-An</ta>
            <ta e="T147" id="Seg_3804" s="T146">keːs-BIT</ta>
            <ta e="T148" id="Seg_3805" s="T147">kör-BIT-tA</ta>
            <ta e="T149" id="Seg_3806" s="T148">dʼaktar</ta>
            <ta e="T150" id="Seg_3807" s="T149">hɨrga-tI-GAr</ta>
            <ta e="T151" id="Seg_3808" s="T150">alta</ta>
            <ta e="T152" id="Seg_3809" s="T151">ɨt</ta>
            <ta e="T153" id="Seg_3810" s="T152">kölüj-LIN-I-BIT</ta>
            <ta e="T154" id="Seg_3811" s="T153">kiːr-An</ta>
            <ta e="T155" id="Seg_3812" s="T154">baran</ta>
            <ta e="T156" id="Seg_3813" s="T155">ɨ͡aldʼɨt-tI-n</ta>
            <ta e="T157" id="Seg_3814" s="T156">ahaː-A-t-BIT</ta>
            <ta e="T158" id="Seg_3815" s="T157">buːs</ta>
            <ta e="T159" id="Seg_3816" s="T158">killehen-A</ta>
            <ta e="T160" id="Seg_3817" s="T159">tagɨs-IAK-m</ta>
            <ta e="T161" id="Seg_3818" s="T160">dʼi͡e-An</ta>
            <ta e="T162" id="Seg_3819" s="T161">baran</ta>
            <ta e="T163" id="Seg_3820" s="T162">taŋas-LAr-tI-n</ta>
            <ta e="T164" id="Seg_3821" s="T163">kɨbɨn-An</ta>
            <ta e="T165" id="Seg_3822" s="T164">tahaːr-BIT</ta>
            <ta e="T166" id="Seg_3823" s="T165">ɨt-tI-n</ta>
            <ta e="T167" id="Seg_3824" s="T166">hɨrga-tI-GAr</ta>
            <ta e="T168" id="Seg_3825" s="T167">uːr-An</ta>
            <ta e="T169" id="Seg_3826" s="T168">keːs-BIT</ta>
            <ta e="T170" id="Seg_3827" s="T169">onton</ta>
            <ta e="T171" id="Seg_3828" s="T170">kiːr-BIT</ta>
            <ta e="T172" id="Seg_3829" s="T171">dʼaktar-nI</ta>
            <ta e="T173" id="Seg_3830" s="T172">olor-BIT</ta>
            <ta e="T174" id="Seg_3831" s="T173">hir-tI-GAr</ta>
            <ta e="T175" id="Seg_3832" s="T174">utuj-t-BIT</ta>
            <ta e="T176" id="Seg_3833" s="T175">tiriː</ta>
            <ta e="T177" id="Seg_3834" s="T176">bi͡er-BIT</ta>
            <ta e="T178" id="Seg_3835" s="T177">tellek-A-nAn</ta>
            <ta e="T179" id="Seg_3836" s="T178">balɨk</ta>
            <ta e="T180" id="Seg_3837" s="T179">hɨ͡a-tA</ta>
            <ta e="T181" id="Seg_3838" s="T180">hunuːr-LArI-n</ta>
            <ta e="T182" id="Seg_3839" s="T181">utut-An</ta>
            <ta e="T183" id="Seg_3840" s="T182">keːs-BIT-LAr</ta>
            <ta e="T184" id="Seg_3841" s="T183">utuj-BIT-LAr</ta>
            <ta e="T185" id="Seg_3842" s="T184">staršina</ta>
            <ta e="T186" id="Seg_3843" s="T185">u͡ol-tA</ta>
            <ta e="T187" id="Seg_3844" s="T186">dogor-tI-n</ta>
            <ta e="T188" id="Seg_3845" s="T187">et-BIT</ta>
            <ta e="T189" id="Seg_3846" s="T188">doː</ta>
            <ta e="T190" id="Seg_3847" s="T189">bu-tI-BIt</ta>
            <ta e="T191" id="Seg_3848" s="T190">kɨːs</ta>
            <ta e="T192" id="Seg_3849" s="T191">bu͡ol-BAtAK</ta>
            <ta e="T193" id="Seg_3850" s="T192">kɨːs-LAː-IAk</ta>
            <ta e="T194" id="Seg_3851" s="T193">tu͡ok-I-n</ta>
            <ta e="T195" id="Seg_3852" s="T194">kuttan-A-GIn</ta>
            <ta e="T196" id="Seg_3853" s="T195">min</ta>
            <ta e="T197" id="Seg_3854" s="T196">oččogo</ta>
            <ta e="T198" id="Seg_3855" s="T197">bar-IAK-m</ta>
            <ta e="T199" id="Seg_3856" s="T198">dogor-tA</ta>
            <ta e="T200" id="Seg_3857" s="T199">min</ta>
            <ta e="T201" id="Seg_3858" s="T200">tagɨs-I-BIT-I-m</ta>
            <ta e="T202" id="Seg_3859" s="T201">genne</ta>
            <ta e="T203" id="Seg_3860" s="T202">kɨːs-GA-r</ta>
            <ta e="T204" id="Seg_3861" s="T203">bar-Aːr</ta>
            <ta e="T205" id="Seg_3862" s="T204">tagɨs-An</ta>
            <ta e="T206" id="Seg_3863" s="T205">kaːl-BIT</ta>
            <ta e="T207" id="Seg_3864" s="T206">taŋɨn-An</ta>
            <ta e="T208" id="Seg_3865" s="T207">baran</ta>
            <ta e="T209" id="Seg_3866" s="T208">hɨrga-tI-GAr</ta>
            <ta e="T210" id="Seg_3867" s="T209">olor-BIT</ta>
            <ta e="T211" id="Seg_3868" s="T210">töhö</ta>
            <ta e="T212" id="Seg_3869" s="T211">bu͡ol-IAK.[tA]=Ij</ta>
            <ta e="T213" id="Seg_3870" s="T212">öl-Ar-kaːl-Ar</ta>
            <ta e="T214" id="Seg_3871" s="T213">ü͡ögüː</ta>
            <ta e="T215" id="Seg_3872" s="T214">ihilin-I-BIT</ta>
            <ta e="T216" id="Seg_3873" s="T215">araː</ta>
            <ta e="T217" id="Seg_3874" s="T216">abɨraː</ta>
            <ta e="T218" id="Seg_3875" s="T217">bu</ta>
            <ta e="T219" id="Seg_3876" s="T218">u͡ol</ta>
            <ta e="T220" id="Seg_3877" s="T219">nʼu͡oguː-tI-n</ta>
            <ta e="T221" id="Seg_3878" s="T220">ɨl-TI-tA</ta>
            <ta e="T222" id="Seg_3879" s="T221">daːganɨ</ta>
            <ta e="T223" id="Seg_3880" s="T222">kötüt-An</ta>
            <ta e="T224" id="Seg_3881" s="T223">kaːl-BIT</ta>
            <ta e="T225" id="Seg_3882" s="T224">kelin-tI-ttAn</ta>
            <ta e="T226" id="Seg_3883" s="T225">dʼaktar</ta>
            <ta e="T227" id="Seg_3884" s="T226">haŋa-tA</ta>
            <ta e="T228" id="Seg_3885" s="T227">ihilin-I-BIT</ta>
            <ta e="T229" id="Seg_3886" s="T228">bil-BIT-I-m</ta>
            <ta e="T230" id="Seg_3887" s="T229">bu͡ol-TAR</ta>
            <ta e="T231" id="Seg_3888" s="T230">hi͡e-An</ta>
            <ta e="T232" id="Seg_3889" s="T231">keːs-IAK</ta>
            <ta e="T233" id="Seg_3890" s="T232">e-TI-m</ta>
            <ta e="T234" id="Seg_3891" s="T233">bu͡o</ta>
            <ta e="T235" id="Seg_3892" s="T234">kel-An</ta>
            <ta e="T236" id="Seg_3893" s="T235">bu</ta>
            <ta e="T237" id="Seg_3894" s="T236">u͡ol</ta>
            <ta e="T238" id="Seg_3895" s="T237">aga-tI-GAr</ta>
            <ta e="T239" id="Seg_3896" s="T238">kineːs-GA</ta>
            <ta e="T240" id="Seg_3897" s="T239">kepseː-BIT</ta>
            <ta e="T241" id="Seg_3898" s="T240">kineːs-nI</ta>
            <ta e="T242" id="Seg_3899" s="T241">kɨtta</ta>
            <ta e="T243" id="Seg_3900" s="T242">staršina</ta>
            <ta e="T244" id="Seg_3901" s="T243">hette</ta>
            <ta e="T245" id="Seg_3902" s="T244">dʼi͡e-nI</ta>
            <ta e="T246" id="Seg_3903" s="T245">komuj-An</ta>
            <ta e="T247" id="Seg_3904" s="T246">munnʼak-LAː-BIT-LAr</ta>
            <ta e="T248" id="Seg_3905" s="T247">ikki</ta>
            <ta e="T249" id="Seg_3906" s="T248">u͡ol-nI</ta>
            <ta e="T250" id="Seg_3907" s="T249">ɨːt-BIT-LAr</ta>
            <ta e="T251" id="Seg_3908" s="T250">kör-IAK-LArI-n</ta>
            <ta e="T252" id="Seg_3909" s="T251">ikki</ta>
            <ta e="T253" id="Seg_3910" s="T252">u͡ol</ta>
            <ta e="T254" id="Seg_3911" s="T253">bu͡ol-An</ta>
            <ta e="T255" id="Seg_3912" s="T254">bar-An</ta>
            <ta e="T256" id="Seg_3913" s="T255">kaːl-TI-LAr</ta>
            <ta e="T257" id="Seg_3914" s="T256">ör</ta>
            <ta e="T258" id="Seg_3915" s="T257">bu͡ol-IAK-LArA=Ij</ta>
            <ta e="T259" id="Seg_3916" s="T258">hɨrdɨk-GA</ta>
            <ta e="T260" id="Seg_3917" s="T259">kel-BIT-LAr</ta>
            <ta e="T261" id="Seg_3918" s="T260">kör-BIT-LArA</ta>
            <ta e="T262" id="Seg_3919" s="T261">biːr</ta>
            <ta e="T263" id="Seg_3920" s="T262">da</ta>
            <ta e="T264" id="Seg_3921" s="T263">kihi</ta>
            <ta e="T265" id="Seg_3922" s="T264">hu͡ok</ta>
            <ta e="T266" id="Seg_3923" s="T265">hu͡ol</ta>
            <ta e="T267" id="Seg_3924" s="T266">da</ta>
            <ta e="T268" id="Seg_3925" s="T267">hu͡ok</ta>
            <ta e="T269" id="Seg_3926" s="T268">uraha</ta>
            <ta e="T270" id="Seg_3927" s="T269">dʼi͡e-GA</ta>
            <ta e="T271" id="Seg_3928" s="T270">kiːr-BIT-LArA</ta>
            <ta e="T272" id="Seg_3929" s="T271">u͡ol</ta>
            <ta e="T273" id="Seg_3930" s="T272">oŋu͡ok-tA</ta>
            <ta e="T274" id="Seg_3931" s="T273">ere</ta>
            <ta e="T275" id="Seg_3932" s="T274">dardaj-A</ta>
            <ta e="T276" id="Seg_3933" s="T275">hɨt-Ar</ta>
            <ta e="T277" id="Seg_3934" s="T276">bu</ta>
            <ta e="T278" id="Seg_3935" s="T277">ikki</ta>
            <ta e="T279" id="Seg_3936" s="T278">u͡ol</ta>
            <ta e="T280" id="Seg_3937" s="T279">tönün-I-BIT-LAr</ta>
            <ta e="T281" id="Seg_3938" s="T280">kineːs-GA</ta>
            <ta e="T282" id="Seg_3939" s="T281">kepseː-BIT-LAr</ta>
            <ta e="T283" id="Seg_3940" s="T282">biːr</ta>
            <ta e="T284" id="Seg_3941" s="T283">tulaːjak</ta>
            <ta e="T285" id="Seg_3942" s="T284">ogo</ta>
            <ta e="T286" id="Seg_3943" s="T285">baːr</ta>
            <ta e="T287" id="Seg_3944" s="T286">ikki</ta>
            <ta e="T288" id="Seg_3945" s="T287">hannɨ-ttAn</ta>
            <ta e="T289" id="Seg_3946" s="T288">tarakaːj</ta>
            <ta e="T290" id="Seg_3947" s="T289">čogotok</ta>
            <ta e="T291" id="Seg_3948" s="T290">inʼe-LAːK</ta>
            <ta e="T292" id="Seg_3949" s="T291">biːr</ta>
            <ta e="T293" id="Seg_3950" s="T292">taba-LAːK</ta>
            <ta e="T294" id="Seg_3951" s="T293">kineːs-nI</ta>
            <ta e="T295" id="Seg_3952" s="T294">kɨtta</ta>
            <ta e="T296" id="Seg_3953" s="T295">staršina</ta>
            <ta e="T297" id="Seg_3954" s="T296">munnʼak-LAː-BIT-LAr</ta>
            <ta e="T298" id="Seg_3955" s="T297">kajdak</ta>
            <ta e="T299" id="Seg_3956" s="T298">bu͡ol-IAK-BIt=Ij</ta>
            <ta e="T300" id="Seg_3957" s="T299">hir-BItI-n</ta>
            <ta e="T301" id="Seg_3958" s="T300">bɨrak-IAK-BIt</ta>
            <ta e="T302" id="Seg_3959" s="T301">du͡o</ta>
            <ta e="T303" id="Seg_3960" s="T302">emi͡e</ta>
            <ta e="T304" id="Seg_3961" s="T303">da</ta>
            <ta e="T305" id="Seg_3962" s="T304">paːs-LAr-BIt</ta>
            <ta e="T307" id="Seg_3963" s="T305">baːr-LAr</ta>
            <ta e="T308" id="Seg_3964" s="T307">di͡e-A-s-BIT-LAr</ta>
            <ta e="T309" id="Seg_3965" s="T308">staršina</ta>
            <ta e="T310" id="Seg_3966" s="T309">onno</ta>
            <ta e="T311" id="Seg_3967" s="T310">tur-An</ta>
            <ta e="T312" id="Seg_3968" s="T311">et-BIT</ta>
            <ta e="T313" id="Seg_3969" s="T312">tulaːjak</ta>
            <ta e="T314" id="Seg_3970" s="T313">ogo-nI</ta>
            <ta e="T315" id="Seg_3971" s="T314">ɨːt-IAK-GA</ta>
            <ta e="T316" id="Seg_3972" s="T315">min</ta>
            <ta e="T317" id="Seg_3973" s="T316">kɨːs-BI-n</ta>
            <ta e="T318" id="Seg_3974" s="T317">da</ta>
            <ta e="T319" id="Seg_3975" s="T318">bi͡er-IAK-m</ta>
            <ta e="T320" id="Seg_3976" s="T319">e-TI-tA</ta>
            <ta e="T321" id="Seg_3977" s="T320">ölör-TAK-InA</ta>
            <ta e="T322" id="Seg_3978" s="T321">bu</ta>
            <ta e="T323" id="Seg_3979" s="T322">u͡ol-nI</ta>
            <ta e="T324" id="Seg_3980" s="T323">ɨgɨr-BIT-LAr</ta>
            <ta e="T325" id="Seg_3981" s="T324">kepseː-BIT-LAr</ta>
            <ta e="T326" id="Seg_3982" s="T325">barɨ-tI-n</ta>
            <ta e="T327" id="Seg_3983" s="T326">dʼe</ta>
            <ta e="T328" id="Seg_3984" s="T327">üleger</ta>
            <ta e="T329" id="Seg_3985" s="T328">e-BIT</ta>
            <ta e="T330" id="Seg_3986" s="T329">ogo</ta>
            <ta e="T331" id="Seg_3987" s="T330">muŋ-LAːK</ta>
            <ta e="T332" id="Seg_3988" s="T331">hogotogun</ta>
            <ta e="T333" id="Seg_3989" s="T332">bar-Ar</ta>
            <ta e="T334" id="Seg_3990" s="T333">bu͡ol-TI-tA</ta>
            <ta e="T335" id="Seg_3991" s="T334">ɨt</ta>
            <ta e="T336" id="Seg_3992" s="T335">ere</ta>
            <ta e="T337" id="Seg_3993" s="T336">bi͡er-BIT-LAr</ta>
            <ta e="T338" id="Seg_3994" s="T337">haː</ta>
            <ta e="T339" id="Seg_3995" s="T338">da</ta>
            <ta e="T340" id="Seg_3996" s="T339">tu͡ok</ta>
            <ta e="T341" id="Seg_3997" s="T340">da</ta>
            <ta e="T342" id="Seg_3998" s="T341">hu͡ok</ta>
            <ta e="T343" id="Seg_3999" s="T342">dʼi͡e-tI-GAr</ta>
            <ta e="T344" id="Seg_4000" s="T343">kel-An</ta>
            <ta e="T345" id="Seg_4001" s="T344">ɨtaː-A-n-A</ta>
            <ta e="T346" id="Seg_4002" s="T345">olor-BIT</ta>
            <ta e="T347" id="Seg_4003" s="T346">inʼe-tA</ta>
            <ta e="T348" id="Seg_4004" s="T347">tu͡ok</ta>
            <ta e="T349" id="Seg_4005" s="T348">bu͡ol-TI-ŋ</ta>
            <ta e="T350" id="Seg_4006" s="T349">kepseː-BIT</ta>
            <ta e="T351" id="Seg_4007" s="T350">eni</ta>
            <ta e="T352" id="Seg_4008" s="T351">aga-ŋ</ta>
            <ta e="T353" id="Seg_4009" s="T352">batɨja-tA</ta>
            <ta e="T354" id="Seg_4010" s="T353">kanna</ta>
            <ta e="T355" id="Seg_4011" s="T354">ere</ta>
            <ta e="T356" id="Seg_4012" s="T355">baːr</ta>
            <ta e="T357" id="Seg_4013" s="T356">e-TI-tA</ta>
            <ta e="T358" id="Seg_4014" s="T357">ol-nI</ta>
            <ta e="T359" id="Seg_4015" s="T358">ilt-Aːr</ta>
            <ta e="T360" id="Seg_4016" s="T359">inʼe-tA</ta>
            <ta e="T361" id="Seg_4017" s="T360">muŋ-LAːK</ta>
            <ta e="T362" id="Seg_4018" s="T361">kam</ta>
            <ta e="T363" id="Seg_4019" s="T362">dʼebintij-BIT</ta>
            <ta e="T364" id="Seg_4020" s="T363">batɨja-nI</ta>
            <ta e="T365" id="Seg_4021" s="T364">killer-BIT</ta>
            <ta e="T366" id="Seg_4022" s="T365">bu</ta>
            <ta e="T367" id="Seg_4023" s="T366">u͡ol</ta>
            <ta e="T368" id="Seg_4024" s="T367">ör</ta>
            <ta e="T369" id="Seg_4025" s="T368">bu͡ol-BAkkA</ta>
            <ta e="T370" id="Seg_4026" s="T369">bar-An</ta>
            <ta e="T371" id="Seg_4027" s="T370">kaːl-BIT</ta>
            <ta e="T372" id="Seg_4028" s="T371">bɨhɨn-IAK.[tA]</ta>
            <ta e="T373" id="Seg_4029" s="T372">du͡o</ta>
            <ta e="T374" id="Seg_4030" s="T373">kel-BIT-tA</ta>
            <ta e="T375" id="Seg_4031" s="T374">kim</ta>
            <ta e="T376" id="Seg_4032" s="T375">da</ta>
            <ta e="T377" id="Seg_4033" s="T376">hu͡ok</ta>
            <ta e="T378" id="Seg_4034" s="T377">kihi</ta>
            <ta e="T379" id="Seg_4035" s="T378">ere</ta>
            <ta e="T380" id="Seg_4036" s="T379">oŋu͡ok-tA</ta>
            <ta e="T381" id="Seg_4037" s="T380">očiha-GA</ta>
            <ta e="T382" id="Seg_4038" s="T381">im</ta>
            <ta e="T383" id="Seg_4039" s="T382">tüs-BIT-tI-n</ta>
            <ta e="T384" id="Seg_4040" s="T383">genne</ta>
            <ta e="T385" id="Seg_4041" s="T384">hunuːr-tI-n</ta>
            <ta e="T386" id="Seg_4042" s="T385">ubat-BIT</ta>
            <ta e="T387" id="Seg_4043" s="T386">kihi</ta>
            <ta e="T388" id="Seg_4044" s="T387">utuj-Ar</ta>
            <ta e="T389" id="Seg_4045" s="T388">kem-tI-n</ta>
            <ta e="T390" id="Seg_4046" s="T389">dek</ta>
            <ta e="T391" id="Seg_4047" s="T390">hɨrga-LAːK</ta>
            <ta e="T392" id="Seg_4048" s="T391">tɨ͡as-tA</ta>
            <ta e="T393" id="Seg_4049" s="T392">ihilin-I-BIT</ta>
            <ta e="T394" id="Seg_4050" s="T393">ɨt</ta>
            <ta e="T395" id="Seg_4051" s="T394">da</ta>
            <ta e="T396" id="Seg_4052" s="T395">ür-BAtAK</ta>
            <ta e="T397" id="Seg_4053" s="T396">u͡ot-tI-n</ta>
            <ta e="T398" id="Seg_4054" s="T397">utut-An</ta>
            <ta e="T399" id="Seg_4055" s="T398">baran</ta>
            <ta e="T400" id="Seg_4056" s="T399">olor-BIT</ta>
            <ta e="T401" id="Seg_4057" s="T400">tɨ͡as</ta>
            <ta e="T402" id="Seg_4058" s="T401">bögö</ta>
            <ta e="T403" id="Seg_4059" s="T402">bu͡ol-BIT</ta>
            <ta e="T404" id="Seg_4060" s="T403">aːn-GA</ta>
            <ta e="T405" id="Seg_4061" s="T404">bat-BAT</ta>
            <ta e="T406" id="Seg_4062" s="T405">hu͡on</ta>
            <ta e="T407" id="Seg_4063" s="T406">dʼaktar</ta>
            <ta e="T408" id="Seg_4064" s="T407">kiːr-BIT</ta>
            <ta e="T409" id="Seg_4065" s="T408">aŋar</ta>
            <ta e="T410" id="Seg_4066" s="T409">atak-tI-n</ta>
            <ta e="T411" id="Seg_4067" s="T410">aːn-GA</ta>
            <ta e="T412" id="Seg_4068" s="T411">as-Ar-tI-n</ta>
            <ta e="T413" id="Seg_4069" s="T412">kɨtta</ta>
            <ta e="T414" id="Seg_4070" s="T413">ikki</ta>
            <ta e="T415" id="Seg_4071" s="T414">konnok-tI-n</ta>
            <ta e="T416" id="Seg_4072" s="T415">alɨn-tI-nAn</ta>
            <ta e="T417" id="Seg_4073" s="T416">batɨja-nAn</ta>
            <ta e="T418" id="Seg_4074" s="T417">batarɨ</ta>
            <ta e="T419" id="Seg_4075" s="T418">as-BIT</ta>
            <ta e="T420" id="Seg_4076" s="T419">beje-tA</ta>
            <ta e="T421" id="Seg_4077" s="T420">konnok-tI-n</ta>
            <ta e="T422" id="Seg_4078" s="T421">alɨn-tI-nAn</ta>
            <ta e="T423" id="Seg_4079" s="T422">küreː-BIT</ta>
            <ta e="T424" id="Seg_4080" s="T423">tüːn</ta>
            <ta e="T425" id="Seg_4081" s="T424">dʼi͡e-tI-GAr</ta>
            <ta e="T426" id="Seg_4082" s="T425">kel-BIT</ta>
            <ta e="T427" id="Seg_4083" s="T426">inʼe-tA</ta>
            <ta e="T428" id="Seg_4084" s="T427">muŋ-LAːK</ta>
            <ta e="T429" id="Seg_4085" s="T428">utuj-BAkkA</ta>
            <ta e="T430" id="Seg_4086" s="T429">köhüt-An</ta>
            <ta e="T431" id="Seg_4087" s="T430">harsi͡erda</ta>
            <ta e="T432" id="Seg_4088" s="T431">inʼe-tA</ta>
            <ta e="T433" id="Seg_4089" s="T432">kineːs-GA</ta>
            <ta e="T434" id="Seg_4090" s="T433">bar-An</ta>
            <ta e="T435" id="Seg_4091" s="T434">kepseː-BIT</ta>
            <ta e="T436" id="Seg_4092" s="T435">kineːs</ta>
            <ta e="T437" id="Seg_4093" s="T436">bu</ta>
            <ta e="T438" id="Seg_4094" s="T437">u͡ol-nI</ta>
            <ta e="T439" id="Seg_4095" s="T438">ɨgɨr-BIT</ta>
            <ta e="T440" id="Seg_4096" s="T439">dʼe</ta>
            <ta e="T441" id="Seg_4097" s="T440">kajdak=Ij</ta>
            <ta e="T442" id="Seg_4098" s="T441">ölör-TI-ŋ</ta>
            <ta e="T443" id="Seg_4099" s="T442">du͡o</ta>
            <ta e="T444" id="Seg_4100" s="T443">bil-BAT-BIn</ta>
            <ta e="T445" id="Seg_4101" s="T444">di͡e-BIT</ta>
            <ta e="T446" id="Seg_4102" s="T445">u͡ol</ta>
            <ta e="T447" id="Seg_4103" s="T446">kepseː-BIT</ta>
            <ta e="T448" id="Seg_4104" s="T447">kajdak</ta>
            <ta e="T449" id="Seg_4105" s="T448">gɨn-BIT-tI-n</ta>
            <ta e="T450" id="Seg_4106" s="T449">munnʼak-LAː-BIT-LAr</ta>
            <ta e="T451" id="Seg_4107" s="T450">alta</ta>
            <ta e="T452" id="Seg_4108" s="T451">kihi-nI</ta>
            <ta e="T453" id="Seg_4109" s="T452">kör-TAr-A</ta>
            <ta e="T454" id="Seg_4110" s="T453">ɨːt-BIT-LAr</ta>
            <ta e="T455" id="Seg_4111" s="T454">staršina</ta>
            <ta e="T456" id="Seg_4112" s="T455">emi͡e</ta>
            <ta e="T457" id="Seg_4113" s="T456">barɨs-I-BIT</ta>
            <ta e="T458" id="Seg_4114" s="T457">taba-nAn</ta>
            <ta e="T459" id="Seg_4115" s="T458">kɨtta</ta>
            <ta e="T460" id="Seg_4116" s="T459">at-I-nAn</ta>
            <ta e="T461" id="Seg_4117" s="T460">bar-BIT-LAr</ta>
            <ta e="T462" id="Seg_4118" s="T461">tij-BIT-LAr</ta>
            <ta e="T463" id="Seg_4119" s="T462">künüs</ta>
            <ta e="T464" id="Seg_4120" s="T463">össü͡ö</ta>
            <ta e="T465" id="Seg_4121" s="T464">biːr</ta>
            <ta e="T466" id="Seg_4122" s="T465">da</ta>
            <ta e="T467" id="Seg_4123" s="T466">kihi</ta>
            <ta e="T468" id="Seg_4124" s="T467">hu͡ok</ta>
            <ta e="T469" id="Seg_4125" s="T468">dʼi͡e-LArI-n</ta>
            <ta e="T470" id="Seg_4126" s="T469">potolu͡ok-tA</ta>
            <ta e="T471" id="Seg_4127" s="T470">kajɨt-I-BIT</ta>
            <ta e="T472" id="Seg_4128" s="T471">aːn-tA</ta>
            <ta e="T473" id="Seg_4129" s="T472">arɨj-LIN-An</ta>
            <ta e="T474" id="Seg_4130" s="T473">tur-Ar</ta>
            <ta e="T475" id="Seg_4131" s="T474">dʼi͡e</ta>
            <ta e="T476" id="Seg_4132" s="T475">is-tI-n</ta>
            <ta e="T477" id="Seg_4133" s="T476">kör-BIT-LArA</ta>
            <ta e="T478" id="Seg_4134" s="T477">kɨːs</ta>
            <ta e="T479" id="Seg_4135" s="T478">is-tA</ta>
            <ta e="T480" id="Seg_4136" s="T479">ülün-An</ta>
            <ta e="T481" id="Seg_4137" s="T480">bar-An</ta>
            <ta e="T482" id="Seg_4138" s="T481">hɨt-Ar</ta>
            <ta e="T483" id="Seg_4139" s="T482">staršina</ta>
            <ta e="T484" id="Seg_4140" s="T483">dʼi͡e-nI</ta>
            <ta e="T485" id="Seg_4141" s="T484">ubat-IAgIŋ</ta>
            <ta e="T486" id="Seg_4142" s="T485">bu</ta>
            <ta e="T487" id="Seg_4143" s="T486">dʼi͡e-nI</ta>
            <ta e="T488" id="Seg_4144" s="T487">kɨːs-LIːN</ta>
            <ta e="T489" id="Seg_4145" s="T488">ubat-An</ta>
            <ta e="T490" id="Seg_4146" s="T489">keːs-BIT-LAr</ta>
            <ta e="T491" id="Seg_4147" s="T490">tarakaːj</ta>
            <ta e="T492" id="Seg_4148" s="T491">u͡ol</ta>
            <ta e="T493" id="Seg_4149" s="T492">staršina</ta>
            <ta e="T494" id="Seg_4150" s="T493">kɨːs-tI-n</ta>
            <ta e="T495" id="Seg_4151" s="T494">ɨl-An</ta>
            <ta e="T496" id="Seg_4152" s="T495">baːj-An-tot-An</ta>
            <ta e="T497" id="Seg_4153" s="T496">olor-BIT</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_4154" s="T0">prince-ACC</ta>
            <ta e="T2" id="Seg_4155" s="T1">with</ta>
            <ta e="T3" id="Seg_4156" s="T2">village.elder.[NOM]</ta>
            <ta e="T4" id="Seg_4157" s="T3">one-DISTR</ta>
            <ta e="T5" id="Seg_4158" s="T4">boy-PROPR-3PL</ta>
            <ta e="T6" id="Seg_4159" s="T5">live-PST2-3PL</ta>
            <ta e="T7" id="Seg_4160" s="T6">be-PST2.[3SG]</ta>
            <ta e="T8" id="Seg_4161" s="T7">two-COLL-3PL.[NOM]</ta>
            <ta e="T9" id="Seg_4162" s="T8">Dolgan</ta>
            <ta e="T10" id="Seg_4163" s="T9">human.being-3PL.[NOM]</ta>
            <ta e="T11" id="Seg_4164" s="T10">boy-PL.[NOM]</ta>
            <ta e="T12" id="Seg_4165" s="T11">together</ta>
            <ta e="T13" id="Seg_4166" s="T12">arise-CVB.SEQ</ta>
            <ta e="T14" id="Seg_4167" s="T13">friend-SIM</ta>
            <ta e="T15" id="Seg_4168" s="T14">very</ta>
            <ta e="T16" id="Seg_4169" s="T15">be-PST2-3PL</ta>
            <ta e="T17" id="Seg_4170" s="T16">this</ta>
            <ta e="T18" id="Seg_4171" s="T17">human.being-PL.[NOM]</ta>
            <ta e="T19" id="Seg_4172" s="T18">olden.time-ABL</ta>
            <ta e="T20" id="Seg_4173" s="T19">nomadize-CVB.SIM</ta>
            <ta e="T21" id="Seg_4174" s="T20">go-PTCP.PRS</ta>
            <ta e="T22" id="Seg_4175" s="T21">be-PST2-3PL</ta>
            <ta e="T23" id="Seg_4176" s="T22">only</ta>
            <ta e="T24" id="Seg_4177" s="T23">in.autumn</ta>
            <ta e="T25" id="Seg_4178" s="T24">deadfall.[NOM]</ta>
            <ta e="T26" id="Seg_4179" s="T25">install-PTCP.PRS</ta>
            <ta e="T27" id="Seg_4180" s="T26">time.[NOM]</ta>
            <ta e="T28" id="Seg_4181" s="T27">become-PST2.[3SG]</ta>
            <ta e="T29" id="Seg_4182" s="T28">bait-3PL-ACC</ta>
            <ta e="T30" id="Seg_4183" s="T29">in.advance</ta>
            <ta e="T31" id="Seg_4184" s="T30">ready-VBZ-CVB.SEQ</ta>
            <ta e="T32" id="Seg_4185" s="T31">throw-PST2-3PL</ta>
            <ta e="T33" id="Seg_4186" s="T32">two</ta>
            <ta e="T34" id="Seg_4187" s="T33">boy.[NOM]</ta>
            <ta e="T35" id="Seg_4188" s="T34">dog-3SG-INSTR</ta>
            <ta e="T36" id="Seg_4189" s="T35">deadfall-VBZ-CVB.SIM</ta>
            <ta e="T37" id="Seg_4190" s="T36">go-PST2-3PL</ta>
            <ta e="T38" id="Seg_4191" s="T37">deadfall.[NOM]</ta>
            <ta e="T39" id="Seg_4192" s="T38">install-MED-CVB.SEQ-install-MED-CVB.SEQ</ta>
            <ta e="T40" id="Seg_4193" s="T39">after</ta>
            <ta e="T41" id="Seg_4194" s="T40">tent-3PL-DAT/LOC</ta>
            <ta e="T42" id="Seg_4195" s="T41">overnight-PST2-3PL</ta>
            <ta e="T43" id="Seg_4196" s="T42">once</ta>
            <ta e="T44" id="Seg_4197" s="T43">in.the.evening</ta>
            <ta e="T45" id="Seg_4198" s="T44">dog-3PL-ACC</ta>
            <ta e="T46" id="Seg_4199" s="T45">tie-CVB.SEQ</ta>
            <ta e="T47" id="Seg_4200" s="T46">after</ta>
            <ta e="T48" id="Seg_4201" s="T47">fish.[NOM]</ta>
            <ta e="T49" id="Seg_4202" s="T48">fat-3SG-INSTR</ta>
            <ta e="T50" id="Seg_4203" s="T49">lamp.[NOM]</ta>
            <ta e="T51" id="Seg_4204" s="T50">make-EP-PST2-3PL</ta>
            <ta e="T52" id="Seg_4205" s="T51">tea.[NOM]</ta>
            <ta e="T53" id="Seg_4206" s="T52">drink-CVB.SIM</ta>
            <ta e="T54" id="Seg_4207" s="T53">sit-PST2-3PL</ta>
            <ta e="T55" id="Seg_4208" s="T54">suddenly</ta>
            <ta e="T56" id="Seg_4209" s="T55">sledge-PROPR</ta>
            <ta e="T57" id="Seg_4210" s="T56">sound-3SG.[NOM]</ta>
            <ta e="T58" id="Seg_4211" s="T57">be.heard-EP-PST2.[3SG]</ta>
            <ta e="T59" id="Seg_4212" s="T58">prince.[NOM]</ta>
            <ta e="T60" id="Seg_4213" s="T59">son-3SG.[NOM]</ta>
            <ta e="T61" id="Seg_4214" s="T60">say-PST2.[3SG]</ta>
            <ta e="T62" id="Seg_4215" s="T61">good</ta>
            <ta e="T63" id="Seg_4216" s="T62">human.being.[NOM]</ta>
            <ta e="T64" id="Seg_4217" s="T63">come-NEG-PST1-3SG</ta>
            <ta e="T65" id="Seg_4218" s="T64">probably</ta>
            <ta e="T66" id="Seg_4219" s="T65">Q</ta>
            <ta e="T67" id="Seg_4220" s="T66">self-3SG.[NOM]</ta>
            <ta e="T68" id="Seg_4221" s="T67">tobacco-VBZ-CVB.SIM</ta>
            <ta e="T69" id="Seg_4222" s="T68">sit-PST2.[3SG]</ta>
            <ta e="T70" id="Seg_4223" s="T69">friend-3SG.[NOM]</ta>
            <ta e="T71" id="Seg_4224" s="T70">lie.on.the.back-CVB.SIM</ta>
            <ta e="T72" id="Seg_4225" s="T71">lie-PST2.[3SG]</ta>
            <ta e="T73" id="Seg_4226" s="T72">listen-EP-MED-PST2-3PL</ta>
            <ta e="T74" id="Seg_4227" s="T73">dog-3PL.[NOM]</ta>
            <ta e="T75" id="Seg_4228" s="T74">bark-NEG.PTCP</ta>
            <ta e="T76" id="Seg_4229" s="T75">become-PST2.[3SG]</ta>
            <ta e="T77" id="Seg_4230" s="T76">stride-PTCP.PRS</ta>
            <ta e="T78" id="Seg_4231" s="T77">sound.[NOM]</ta>
            <ta e="T79" id="Seg_4232" s="T78">be.heard-EP-PST2.[3SG]</ta>
            <ta e="T80" id="Seg_4233" s="T79">prince.[NOM]</ta>
            <ta e="T81" id="Seg_4234" s="T80">son-3SG.[NOM]</ta>
            <ta e="T82" id="Seg_4235" s="T81">be.afraid-PRS-1SG</ta>
            <ta e="T83" id="Seg_4236" s="T82">say-PST2.[3SG]</ta>
            <ta e="T84" id="Seg_4237" s="T83">friend-3SG.[NOM]</ta>
            <ta e="T85" id="Seg_4238" s="T84">that.EMPH.[NOM]</ta>
            <ta e="T86" id="Seg_4239" s="T85">similar</ta>
            <ta e="T87" id="Seg_4240" s="T86">lie.on.the.back-CVB.SIM</ta>
            <ta e="T88" id="Seg_4241" s="T87">lie-PST2.[3SG]</ta>
            <ta e="T89" id="Seg_4242" s="T88">entrance.[NOM]</ta>
            <ta e="T90" id="Seg_4243" s="T89">open-PASS/REFL-EP-PST2.[3SG]</ta>
            <ta e="T91" id="Seg_4244" s="T90">and</ta>
            <ta e="T92" id="Seg_4245" s="T91">fat</ta>
            <ta e="T93" id="Seg_4246" s="T92">very</ta>
            <ta e="T94" id="Seg_4247" s="T93">woman.[NOM]</ta>
            <ta e="T95" id="Seg_4248" s="T94">hardly</ta>
            <ta e="T96" id="Seg_4249" s="T95">fit-CVB.SEQ</ta>
            <ta e="T97" id="Seg_4250" s="T96">go.in-PST2.[3SG]</ta>
            <ta e="T98" id="Seg_4251" s="T97">best</ta>
            <ta e="T99" id="Seg_4252" s="T98">beads.[NOM]</ta>
            <ta e="T100" id="Seg_4253" s="T99">clothes-PROPR.[NOM]</ta>
            <ta e="T101" id="Seg_4254" s="T100">beads.[NOM]</ta>
            <ta e="T102" id="Seg_4255" s="T101">cap-PROPR.[NOM]</ta>
            <ta e="T103" id="Seg_4256" s="T102">rich</ta>
            <ta e="T104" id="Seg_4257" s="T103">very</ta>
            <ta e="T105" id="Seg_4258" s="T104">shape-PROPR.[NOM]</ta>
            <ta e="T106" id="Seg_4259" s="T105">village.elder.[NOM]</ta>
            <ta e="T107" id="Seg_4260" s="T106">son-3SG.[NOM]</ta>
            <ta e="T108" id="Seg_4261" s="T107">friend-3SG-ACC</ta>
            <ta e="T109" id="Seg_4262" s="T108">tea-VBZ.[IMP.2SG]</ta>
            <ta e="T110" id="Seg_4263" s="T109">guest-ACC</ta>
            <ta e="T111" id="Seg_4264" s="T110">say-PST2.[3SG]</ta>
            <ta e="T112" id="Seg_4265" s="T111">prince.[NOM]</ta>
            <ta e="T113" id="Seg_4266" s="T112">son-3SG.[NOM]</ta>
            <ta e="T114" id="Seg_4267" s="T113">stand.up-CVB.SEQ</ta>
            <ta e="T115" id="Seg_4268" s="T114">stove.[NOM]</ta>
            <ta e="T116" id="Seg_4269" s="T115">heat-EP-MED-PST2.[3SG]</ta>
            <ta e="T117" id="Seg_4270" s="T116">tea.[NOM]</ta>
            <ta e="T118" id="Seg_4271" s="T117">boil-EP-CAUS-NEG.[3SG]</ta>
            <ta e="T119" id="Seg_4272" s="T118">only</ta>
            <ta e="T120" id="Seg_4273" s="T119">woman-3PL.[NOM]</ta>
            <ta e="T121" id="Seg_4274" s="T120">face-3SG-ACC</ta>
            <ta e="T122" id="Seg_4275" s="T121">show-NEG.[3SG]</ta>
            <ta e="T123" id="Seg_4276" s="T122">speak-RECP/COLL-EP-NEG.[3SG]</ta>
            <ta e="T124" id="Seg_4277" s="T123">this</ta>
            <ta e="T125" id="Seg_4278" s="T124">boy.[NOM]</ta>
            <ta e="T126" id="Seg_4279" s="T125">fish.[NOM]</ta>
            <ta e="T127" id="Seg_4280" s="T126">kettle-3SG.[NOM]</ta>
            <ta e="T128" id="Seg_4281" s="T127">fat-VBZ-PTCP.PST</ta>
            <ta e="T129" id="Seg_4282" s="T128">match.[NOM]</ta>
            <ta e="T130" id="Seg_4283" s="T129">wood-3SG-INSTR</ta>
            <ta e="T131" id="Seg_4284" s="T130">eye-3SG-ACC</ta>
            <ta e="T132" id="Seg_4285" s="T131">support-CVB.SEQ</ta>
            <ta e="T133" id="Seg_4286" s="T132">after</ta>
            <ta e="T134" id="Seg_4287" s="T133">see-PST2-3SG</ta>
            <ta e="T135" id="Seg_4288" s="T134">woman.[NOM]</ta>
            <ta e="T136" id="Seg_4289" s="T135">laugh-CVB.SIM</ta>
            <ta e="T137" id="Seg_4290" s="T136">sit-PRS.[3SG]</ta>
            <ta e="T138" id="Seg_4291" s="T137">lonely</ta>
            <ta e="T139" id="Seg_4292" s="T138">just</ta>
            <ta e="T140" id="Seg_4293" s="T139">tooth-PROPR.[NOM]</ta>
            <ta e="T141" id="Seg_4294" s="T140">be-PST2.[3SG]</ta>
            <ta e="T142" id="Seg_4295" s="T141">wood.[NOM]</ta>
            <ta e="T143" id="Seg_4296" s="T142">bring.in-CVB.SIM</ta>
            <ta e="T144" id="Seg_4297" s="T143">go.out-EP-PST2.[3SG]</ta>
            <ta e="T145" id="Seg_4298" s="T144">dog-3PL-ACC</ta>
            <ta e="T146" id="Seg_4299" s="T145">harness-FREQ-CVB.SEQ</ta>
            <ta e="T147" id="Seg_4300" s="T146">throw-PST2.[3SG]</ta>
            <ta e="T148" id="Seg_4301" s="T147">see-PST2-3SG</ta>
            <ta e="T149" id="Seg_4302" s="T148">woman.[NOM]</ta>
            <ta e="T150" id="Seg_4303" s="T149">sledge-3SG-DAT/LOC</ta>
            <ta e="T151" id="Seg_4304" s="T150">six</ta>
            <ta e="T152" id="Seg_4305" s="T151">dog.[NOM]</ta>
            <ta e="T153" id="Seg_4306" s="T152">harness-PASS/REFL-EP-PTCP.PST.[NOM]</ta>
            <ta e="T154" id="Seg_4307" s="T153">go.in-CVB.SEQ</ta>
            <ta e="T155" id="Seg_4308" s="T154">after</ta>
            <ta e="T156" id="Seg_4309" s="T155">guest-3SG-ACC</ta>
            <ta e="T157" id="Seg_4310" s="T156">eat-EP-CAUS-PST2.[3SG]</ta>
            <ta e="T158" id="Seg_4311" s="T157">ice.[NOM]</ta>
            <ta e="T159" id="Seg_4312" s="T158">bring.in-CVB.SIM</ta>
            <ta e="T160" id="Seg_4313" s="T159">go.out-FUT-1SG</ta>
            <ta e="T161" id="Seg_4314" s="T160">say-CVB.SEQ</ta>
            <ta e="T162" id="Seg_4315" s="T161">after</ta>
            <ta e="T163" id="Seg_4316" s="T162">clothes-PL-3SG-ACC</ta>
            <ta e="T164" id="Seg_4317" s="T163">take.along-CVB.SEQ</ta>
            <ta e="T165" id="Seg_4318" s="T164">take.out-PST2.[3SG]</ta>
            <ta e="T166" id="Seg_4319" s="T165">dog-3SG-GEN</ta>
            <ta e="T167" id="Seg_4320" s="T166">sledge-3SG-DAT/LOC</ta>
            <ta e="T168" id="Seg_4321" s="T167">lay-CVB.SEQ</ta>
            <ta e="T169" id="Seg_4322" s="T168">throw-PST2.[3SG]</ta>
            <ta e="T170" id="Seg_4323" s="T169">then</ta>
            <ta e="T171" id="Seg_4324" s="T170">go.in-PST2.[3SG]</ta>
            <ta e="T172" id="Seg_4325" s="T171">woman-ACC</ta>
            <ta e="T173" id="Seg_4326" s="T172">sit-PTCP.PST</ta>
            <ta e="T174" id="Seg_4327" s="T173">place-3SG-DAT/LOC</ta>
            <ta e="T175" id="Seg_4328" s="T174">sleep-CAUS-PST2.[3SG]</ta>
            <ta e="T176" id="Seg_4329" s="T175">fur.[NOM]</ta>
            <ta e="T177" id="Seg_4330" s="T176">give-PST2.[3SG]</ta>
            <ta e="T178" id="Seg_4331" s="T177">bedding-EP-INSTR</ta>
            <ta e="T179" id="Seg_4332" s="T178">fish.[NOM]</ta>
            <ta e="T180" id="Seg_4333" s="T179">fat-3SG.[NOM]</ta>
            <ta e="T181" id="Seg_4334" s="T180">lamp-3PL-ACC</ta>
            <ta e="T182" id="Seg_4335" s="T181">put.out-CVB.SEQ</ta>
            <ta e="T183" id="Seg_4336" s="T182">throw-PST2-3PL</ta>
            <ta e="T184" id="Seg_4337" s="T183">fall.asleep-PST2-3PL</ta>
            <ta e="T185" id="Seg_4338" s="T184">village.elder.[NOM]</ta>
            <ta e="T186" id="Seg_4339" s="T185">son-3SG.[NOM]</ta>
            <ta e="T187" id="Seg_4340" s="T186">friend-3SG-ACC</ta>
            <ta e="T188" id="Seg_4341" s="T187">speak-PST2.[3SG]</ta>
            <ta e="T189" id="Seg_4342" s="T188">well</ta>
            <ta e="T190" id="Seg_4343" s="T189">this-3SG-1PL.[NOM]</ta>
            <ta e="T191" id="Seg_4344" s="T190">girl.[NOM]</ta>
            <ta e="T192" id="Seg_4345" s="T191">be-PST2.NEG.[3SG]</ta>
            <ta e="T193" id="Seg_4346" s="T192">girl-VBZ-IMP.1DU</ta>
            <ta e="T194" id="Seg_4347" s="T193">what-EP-ACC</ta>
            <ta e="T195" id="Seg_4348" s="T194">startle-PRS-2SG</ta>
            <ta e="T196" id="Seg_4349" s="T195">1SG.[NOM]</ta>
            <ta e="T197" id="Seg_4350" s="T196">then</ta>
            <ta e="T198" id="Seg_4351" s="T197">go-FUT-1SG</ta>
            <ta e="T199" id="Seg_4352" s="T198">friend-3SG.[NOM]</ta>
            <ta e="T200" id="Seg_4353" s="T199">1SG.[NOM]</ta>
            <ta e="T201" id="Seg_4354" s="T200">go.out-EP-PTCP.PST-EP-1SG.[NOM]</ta>
            <ta e="T202" id="Seg_4355" s="T201">after</ta>
            <ta e="T203" id="Seg_4356" s="T202">girl-2SG-DAT/LOC</ta>
            <ta e="T204" id="Seg_4357" s="T203">go-FUT.[IMP.2SG]</ta>
            <ta e="T205" id="Seg_4358" s="T204">go.out-CVB.SEQ</ta>
            <ta e="T206" id="Seg_4359" s="T205">stay-PST2.[3SG]</ta>
            <ta e="T207" id="Seg_4360" s="T206">dress-CVB.SEQ</ta>
            <ta e="T208" id="Seg_4361" s="T207">after</ta>
            <ta e="T209" id="Seg_4362" s="T208">sledge-3SG-DAT/LOC</ta>
            <ta e="T210" id="Seg_4363" s="T209">sit.down-PST2.[3SG]</ta>
            <ta e="T211" id="Seg_4364" s="T210">how.much</ta>
            <ta e="T212" id="Seg_4365" s="T211">be-FUT.[3SG]=Q</ta>
            <ta e="T213" id="Seg_4366" s="T212">die-PTCP.PRS-stay-PTCP.PRS</ta>
            <ta e="T214" id="Seg_4367" s="T213">shout.[NOM]</ta>
            <ta e="T215" id="Seg_4368" s="T214">be.heard-EP-PST2.[3SG]</ta>
            <ta e="T216" id="Seg_4369" s="T215">oh.dear</ta>
            <ta e="T217" id="Seg_4370" s="T216">come.to.aid.[IMP.2SG]</ta>
            <ta e="T218" id="Seg_4371" s="T217">this</ta>
            <ta e="T219" id="Seg_4372" s="T218">boy.[NOM]</ta>
            <ta e="T220" id="Seg_4373" s="T219">rein-3SG-ACC</ta>
            <ta e="T221" id="Seg_4374" s="T220">take-PST1-3SG</ta>
            <ta e="T222" id="Seg_4375" s="T221">EMPH</ta>
            <ta e="T223" id="Seg_4376" s="T222">run-CVB.SEQ</ta>
            <ta e="T224" id="Seg_4377" s="T223">stay-PST2.[3SG]</ta>
            <ta e="T225" id="Seg_4378" s="T224">back-3SG-ABL</ta>
            <ta e="T226" id="Seg_4379" s="T225">woman.[NOM]</ta>
            <ta e="T227" id="Seg_4380" s="T226">voice-3SG.[NOM]</ta>
            <ta e="T228" id="Seg_4381" s="T227">be.heard-EP-PST2.[3SG]</ta>
            <ta e="T229" id="Seg_4382" s="T228">know-PTCP.PST-EP-1SG.[NOM]</ta>
            <ta e="T230" id="Seg_4383" s="T229">be-COND.[3SG]</ta>
            <ta e="T231" id="Seg_4384" s="T230">eat-CVB.SEQ</ta>
            <ta e="T232" id="Seg_4385" s="T231">throw-PTCP.FUT</ta>
            <ta e="T233" id="Seg_4386" s="T232">be-PST1-1SG</ta>
            <ta e="T234" id="Seg_4387" s="T233">EMPH</ta>
            <ta e="T235" id="Seg_4388" s="T234">come-CVB.SEQ</ta>
            <ta e="T236" id="Seg_4389" s="T235">this</ta>
            <ta e="T237" id="Seg_4390" s="T236">boy.[NOM]</ta>
            <ta e="T238" id="Seg_4391" s="T237">father-3SG-DAT/LOC</ta>
            <ta e="T239" id="Seg_4392" s="T238">prince-DAT/LOC</ta>
            <ta e="T240" id="Seg_4393" s="T239">tell-PST2.[3SG]</ta>
            <ta e="T241" id="Seg_4394" s="T240">prince-ACC</ta>
            <ta e="T242" id="Seg_4395" s="T241">with</ta>
            <ta e="T243" id="Seg_4396" s="T242">village.elder.[NOM]</ta>
            <ta e="T244" id="Seg_4397" s="T243">seven</ta>
            <ta e="T245" id="Seg_4398" s="T244">tent-ACC</ta>
            <ta e="T246" id="Seg_4399" s="T245">gather-CVB.SEQ</ta>
            <ta e="T247" id="Seg_4400" s="T246">gathering-VBZ-PST2-3PL</ta>
            <ta e="T248" id="Seg_4401" s="T247">two</ta>
            <ta e="T249" id="Seg_4402" s="T248">boy-ACC</ta>
            <ta e="T250" id="Seg_4403" s="T249">send-PST2-3PL</ta>
            <ta e="T251" id="Seg_4404" s="T250">see-PTCP.FUT-3PL-ACC</ta>
            <ta e="T252" id="Seg_4405" s="T251">two</ta>
            <ta e="T253" id="Seg_4406" s="T252">boy.[NOM]</ta>
            <ta e="T254" id="Seg_4407" s="T253">be-CVB.SEQ</ta>
            <ta e="T255" id="Seg_4408" s="T254">go-CVB.SEQ</ta>
            <ta e="T256" id="Seg_4409" s="T255">stay-PST1-3PL</ta>
            <ta e="T257" id="Seg_4410" s="T256">long</ta>
            <ta e="T258" id="Seg_4411" s="T257">be-FUT-3PL=Q</ta>
            <ta e="T259" id="Seg_4412" s="T258">light-DAT/LOC</ta>
            <ta e="T260" id="Seg_4413" s="T259">come-PST2-3PL</ta>
            <ta e="T261" id="Seg_4414" s="T260">see-PST2-3PL</ta>
            <ta e="T262" id="Seg_4415" s="T261">one</ta>
            <ta e="T263" id="Seg_4416" s="T262">NEG</ta>
            <ta e="T264" id="Seg_4417" s="T263">human.being.[NOM]</ta>
            <ta e="T265" id="Seg_4418" s="T264">NEG.EX</ta>
            <ta e="T266" id="Seg_4419" s="T265">trace.[NOM]</ta>
            <ta e="T267" id="Seg_4420" s="T266">NEG</ta>
            <ta e="T268" id="Seg_4421" s="T267">NEG.EX</ta>
            <ta e="T269" id="Seg_4422" s="T268">pole.[NOM]</ta>
            <ta e="T270" id="Seg_4423" s="T269">tent-DAT/LOC</ta>
            <ta e="T271" id="Seg_4424" s="T270">go.in-PST2-3PL</ta>
            <ta e="T272" id="Seg_4425" s="T271">boy.[NOM]</ta>
            <ta e="T273" id="Seg_4426" s="T272">bone-3SG.[NOM]</ta>
            <ta e="T274" id="Seg_4427" s="T273">just</ta>
            <ta e="T275" id="Seg_4428" s="T274">be.very.bony-CVB.SIM</ta>
            <ta e="T276" id="Seg_4429" s="T275">lie-PRS.[3SG]</ta>
            <ta e="T277" id="Seg_4430" s="T276">this</ta>
            <ta e="T278" id="Seg_4431" s="T277">two</ta>
            <ta e="T279" id="Seg_4432" s="T278">boy.[NOM]</ta>
            <ta e="T280" id="Seg_4433" s="T279">come.back-EP-PST2-3PL</ta>
            <ta e="T281" id="Seg_4434" s="T280">prince-DAT/LOC</ta>
            <ta e="T282" id="Seg_4435" s="T281">tell-PST2-3PL</ta>
            <ta e="T283" id="Seg_4436" s="T282">one</ta>
            <ta e="T284" id="Seg_4437" s="T283">orphan.[NOM]</ta>
            <ta e="T285" id="Seg_4438" s="T284">child.[NOM]</ta>
            <ta e="T286" id="Seg_4439" s="T285">there.is</ta>
            <ta e="T287" id="Seg_4440" s="T286">two</ta>
            <ta e="T288" id="Seg_4441" s="T287">shoulder-ABL</ta>
            <ta e="T289" id="Seg_4442" s="T288">baldheaded.[NOM]</ta>
            <ta e="T290" id="Seg_4443" s="T289">lonely</ta>
            <ta e="T291" id="Seg_4444" s="T290">mother-PROPR.[NOM]</ta>
            <ta e="T292" id="Seg_4445" s="T291">one</ta>
            <ta e="T293" id="Seg_4446" s="T292">reindeer-PROPR.[NOM]</ta>
            <ta e="T294" id="Seg_4447" s="T293">prince-ACC</ta>
            <ta e="T295" id="Seg_4448" s="T294">with</ta>
            <ta e="T296" id="Seg_4449" s="T295">village.elder.[NOM]</ta>
            <ta e="T297" id="Seg_4450" s="T296">gathering-VBZ-PST2-3PL</ta>
            <ta e="T298" id="Seg_4451" s="T297">how</ta>
            <ta e="T299" id="Seg_4452" s="T298">be-FUT-1PL=Q</ta>
            <ta e="T300" id="Seg_4453" s="T299">earth-1PL-ACC</ta>
            <ta e="T301" id="Seg_4454" s="T300">throw-FUT-1PL</ta>
            <ta e="T302" id="Seg_4455" s="T301">Q</ta>
            <ta e="T303" id="Seg_4456" s="T302">also</ta>
            <ta e="T304" id="Seg_4457" s="T303">and</ta>
            <ta e="T305" id="Seg_4458" s="T304">deadfall-PL-1PL.[NOM]</ta>
            <ta e="T307" id="Seg_4459" s="T305">there.is-3PL</ta>
            <ta e="T308" id="Seg_4460" s="T307">say-EP-RECP/COLL-PST2-3PL</ta>
            <ta e="T309" id="Seg_4461" s="T308">village.elder.[NOM]</ta>
            <ta e="T310" id="Seg_4462" s="T309">there</ta>
            <ta e="T311" id="Seg_4463" s="T310">stand-CVB.SEQ</ta>
            <ta e="T312" id="Seg_4464" s="T311">speak-PST2.[3SG]</ta>
            <ta e="T313" id="Seg_4465" s="T312">orphan.[NOM]</ta>
            <ta e="T314" id="Seg_4466" s="T313">child-ACC</ta>
            <ta e="T315" id="Seg_4467" s="T314">send-PTCP.FUT-DAT/LOC</ta>
            <ta e="T316" id="Seg_4468" s="T315">1SG.[NOM]</ta>
            <ta e="T317" id="Seg_4469" s="T316">daughter-1SG-ACC</ta>
            <ta e="T318" id="Seg_4470" s="T317">and</ta>
            <ta e="T319" id="Seg_4471" s="T318">give-PTCP.FUT-1SG</ta>
            <ta e="T320" id="Seg_4472" s="T319">be-PST1-3SG</ta>
            <ta e="T321" id="Seg_4473" s="T320">kill-TEMP-3SG</ta>
            <ta e="T322" id="Seg_4474" s="T321">this</ta>
            <ta e="T323" id="Seg_4475" s="T322">boy-ACC</ta>
            <ta e="T324" id="Seg_4476" s="T323">call-PST2-3PL</ta>
            <ta e="T325" id="Seg_4477" s="T324">tell-PST2-3PL</ta>
            <ta e="T326" id="Seg_4478" s="T325">every-3SG-ACC</ta>
            <ta e="T327" id="Seg_4479" s="T326">well</ta>
            <ta e="T328" id="Seg_4480" s="T327">INTJ</ta>
            <ta e="T329" id="Seg_4481" s="T328">be-PST2.[3SG]</ta>
            <ta e="T330" id="Seg_4482" s="T329">child.[NOM]</ta>
            <ta e="T331" id="Seg_4483" s="T330">misery-PROPR</ta>
            <ta e="T332" id="Seg_4484" s="T331">lonely</ta>
            <ta e="T333" id="Seg_4485" s="T332">go-PTCP.PRS</ta>
            <ta e="T334" id="Seg_4486" s="T333">be-PST1-3SG</ta>
            <ta e="T335" id="Seg_4487" s="T334">dog.[NOM]</ta>
            <ta e="T336" id="Seg_4488" s="T335">just</ta>
            <ta e="T337" id="Seg_4489" s="T336">give-PST2-3PL</ta>
            <ta e="T338" id="Seg_4490" s="T337">rifle.[NOM]</ta>
            <ta e="T339" id="Seg_4491" s="T338">NEG</ta>
            <ta e="T340" id="Seg_4492" s="T339">what.[NOM]</ta>
            <ta e="T341" id="Seg_4493" s="T340">NEG</ta>
            <ta e="T342" id="Seg_4494" s="T341">NEG.EX</ta>
            <ta e="T343" id="Seg_4495" s="T342">house-3SG-DAT/LOC</ta>
            <ta e="T344" id="Seg_4496" s="T343">come-CVB.SEQ</ta>
            <ta e="T345" id="Seg_4497" s="T344">cry-EP-MED-CVB.SIM</ta>
            <ta e="T346" id="Seg_4498" s="T345">sit-PST2.[3SG]</ta>
            <ta e="T347" id="Seg_4499" s="T346">mother-3SG.[NOM]</ta>
            <ta e="T348" id="Seg_4500" s="T347">what.[NOM]</ta>
            <ta e="T349" id="Seg_4501" s="T348">be-PST1-2SG</ta>
            <ta e="T350" id="Seg_4502" s="T349">tell-PST2.[3SG]</ta>
            <ta e="T351" id="Seg_4503" s="T350">apparently</ta>
            <ta e="T352" id="Seg_4504" s="T351">father-2SG.[NOM]</ta>
            <ta e="T353" id="Seg_4505" s="T352">glaive-3SG.[NOM]</ta>
            <ta e="T354" id="Seg_4506" s="T353">where</ta>
            <ta e="T355" id="Seg_4507" s="T354">INDEF</ta>
            <ta e="T356" id="Seg_4508" s="T355">there.is</ta>
            <ta e="T357" id="Seg_4509" s="T356">be-PST1-3SG</ta>
            <ta e="T358" id="Seg_4510" s="T357">that-ACC</ta>
            <ta e="T359" id="Seg_4511" s="T358">carry-FUT.[IMP.2SG]</ta>
            <ta e="T360" id="Seg_4512" s="T359">mother-3SG.[NOM]</ta>
            <ta e="T361" id="Seg_4513" s="T360">misery-PROPR</ta>
            <ta e="T362" id="Seg_4514" s="T361">dense</ta>
            <ta e="T363" id="Seg_4515" s="T362">rust-PTCP.PST</ta>
            <ta e="T364" id="Seg_4516" s="T363">glaive-ACC</ta>
            <ta e="T365" id="Seg_4517" s="T364">bring.in-PST2.[3SG]</ta>
            <ta e="T366" id="Seg_4518" s="T365">this</ta>
            <ta e="T367" id="Seg_4519" s="T366">boy.[NOM]</ta>
            <ta e="T368" id="Seg_4520" s="T367">long</ta>
            <ta e="T369" id="Seg_4521" s="T368">be-NEG.CVB.SIM</ta>
            <ta e="T370" id="Seg_4522" s="T369">go-CVB.SEQ</ta>
            <ta e="T371" id="Seg_4523" s="T370">stay-PST2.[3SG]</ta>
            <ta e="T372" id="Seg_4524" s="T371">come.to.an.end-FUT.[3SG]</ta>
            <ta e="T373" id="Seg_4525" s="T372">Q</ta>
            <ta e="T374" id="Seg_4526" s="T373">come-PST2-3SG</ta>
            <ta e="T375" id="Seg_4527" s="T374">who.[NOM]</ta>
            <ta e="T376" id="Seg_4528" s="T375">NEG</ta>
            <ta e="T377" id="Seg_4529" s="T376">NEG.EX</ta>
            <ta e="T378" id="Seg_4530" s="T377">human.being.[NOM]</ta>
            <ta e="T379" id="Seg_4531" s="T378">just</ta>
            <ta e="T380" id="Seg_4532" s="T379">bone-3SG.[NOM]</ta>
            <ta e="T381" id="Seg_4533" s="T380">tent-DAT/LOC</ta>
            <ta e="T382" id="Seg_4534" s="T381">dusk.[NOM]</ta>
            <ta e="T383" id="Seg_4535" s="T382">fall-PTCP.PST-3SG-ACC</ta>
            <ta e="T384" id="Seg_4536" s="T383">after</ta>
            <ta e="T385" id="Seg_4537" s="T384">lamp-3SG-ACC</ta>
            <ta e="T386" id="Seg_4538" s="T385">light-PST2.[3SG]</ta>
            <ta e="T387" id="Seg_4539" s="T386">human.being.[NOM]</ta>
            <ta e="T388" id="Seg_4540" s="T387">sleep-PTCP.PRS</ta>
            <ta e="T389" id="Seg_4541" s="T388">time-3SG-ACC</ta>
            <ta e="T390" id="Seg_4542" s="T389">to</ta>
            <ta e="T391" id="Seg_4543" s="T390">sledge-PROPR</ta>
            <ta e="T392" id="Seg_4544" s="T391">sound-3SG.[NOM]</ta>
            <ta e="T393" id="Seg_4545" s="T392">be.heard-EP-PST2.[3SG]</ta>
            <ta e="T394" id="Seg_4546" s="T393">dog.[NOM]</ta>
            <ta e="T395" id="Seg_4547" s="T394">and</ta>
            <ta e="T396" id="Seg_4548" s="T395">bark-PST2.NEG.[3SG]</ta>
            <ta e="T397" id="Seg_4549" s="T396">fire-3SG-ACC</ta>
            <ta e="T398" id="Seg_4550" s="T397">put.out-CVB.SEQ</ta>
            <ta e="T399" id="Seg_4551" s="T398">after</ta>
            <ta e="T400" id="Seg_4552" s="T399">sit-PST2.[3SG]</ta>
            <ta e="T401" id="Seg_4553" s="T400">sound.[NOM]</ta>
            <ta e="T402" id="Seg_4554" s="T401">strong.[NOM]</ta>
            <ta e="T403" id="Seg_4555" s="T402">become-PST2.[3SG]</ta>
            <ta e="T404" id="Seg_4556" s="T403">entrance-DAT/LOC</ta>
            <ta e="T405" id="Seg_4557" s="T404">fit-NEG.PTCP</ta>
            <ta e="T406" id="Seg_4558" s="T405">fat</ta>
            <ta e="T407" id="Seg_4559" s="T406">woman.[NOM]</ta>
            <ta e="T408" id="Seg_4560" s="T407">go.in-PST2.[3SG]</ta>
            <ta e="T409" id="Seg_4561" s="T408">other.of.two</ta>
            <ta e="T410" id="Seg_4562" s="T409">leg-3SG-ACC</ta>
            <ta e="T411" id="Seg_4563" s="T410">door-DAT/LOC</ta>
            <ta e="T412" id="Seg_4564" s="T411">push-PTCP.PRS-3SG-ACC</ta>
            <ta e="T413" id="Seg_4565" s="T412">with</ta>
            <ta e="T414" id="Seg_4566" s="T413">two</ta>
            <ta e="T415" id="Seg_4567" s="T414">arm.pit-3SG-GEN</ta>
            <ta e="T416" id="Seg_4568" s="T415">lower.part-3SG-INSTR</ta>
            <ta e="T417" id="Seg_4569" s="T416">glaive-INSTR</ta>
            <ta e="T418" id="Seg_4570" s="T417">into</ta>
            <ta e="T419" id="Seg_4571" s="T418">push-PST2.[3SG]</ta>
            <ta e="T420" id="Seg_4572" s="T419">self-3SG.[NOM]</ta>
            <ta e="T421" id="Seg_4573" s="T420">arm.pit-3SG-GEN</ta>
            <ta e="T422" id="Seg_4574" s="T421">lower.part-3SG-INSTR</ta>
            <ta e="T423" id="Seg_4575" s="T422">escape-PST2.[3SG]</ta>
            <ta e="T424" id="Seg_4576" s="T423">at.night</ta>
            <ta e="T425" id="Seg_4577" s="T424">house-3SG-DAT/LOC</ta>
            <ta e="T426" id="Seg_4578" s="T425">come-PST2.[3SG]</ta>
            <ta e="T427" id="Seg_4579" s="T426">mother-3SG.[NOM]</ta>
            <ta e="T428" id="Seg_4580" s="T427">misery-PROPR</ta>
            <ta e="T429" id="Seg_4581" s="T428">sleep-NEG.CVB.SIM</ta>
            <ta e="T430" id="Seg_4582" s="T429">wait-CVB.SEQ</ta>
            <ta e="T431" id="Seg_4583" s="T430">in.the.morning</ta>
            <ta e="T432" id="Seg_4584" s="T431">mother-3SG.[NOM]</ta>
            <ta e="T433" id="Seg_4585" s="T432">prince-DAT/LOC</ta>
            <ta e="T434" id="Seg_4586" s="T433">go-CVB.SEQ</ta>
            <ta e="T435" id="Seg_4587" s="T434">tell-PST2.[3SG]</ta>
            <ta e="T436" id="Seg_4588" s="T435">prince.[NOM]</ta>
            <ta e="T437" id="Seg_4589" s="T436">this</ta>
            <ta e="T438" id="Seg_4590" s="T437">boy-ACC</ta>
            <ta e="T439" id="Seg_4591" s="T438">call-PST2.[3SG]</ta>
            <ta e="T440" id="Seg_4592" s="T439">well</ta>
            <ta e="T441" id="Seg_4593" s="T440">how=Q</ta>
            <ta e="T442" id="Seg_4594" s="T441">kill-PST1-2SG</ta>
            <ta e="T443" id="Seg_4595" s="T442">Q</ta>
            <ta e="T444" id="Seg_4596" s="T443">know-NEG-1SG</ta>
            <ta e="T445" id="Seg_4597" s="T444">say-PST2.[3SG]</ta>
            <ta e="T446" id="Seg_4598" s="T445">boy.[NOM]</ta>
            <ta e="T447" id="Seg_4599" s="T446">tell-PST2.[3SG]</ta>
            <ta e="T448" id="Seg_4600" s="T447">how</ta>
            <ta e="T449" id="Seg_4601" s="T448">make-PTCP.PST-3SG-ACC</ta>
            <ta e="T450" id="Seg_4602" s="T449">gathering-VBZ-PST2-3PL</ta>
            <ta e="T451" id="Seg_4603" s="T450">six</ta>
            <ta e="T452" id="Seg_4604" s="T451">human.being-ACC</ta>
            <ta e="T453" id="Seg_4605" s="T452">see-CAUS-CVB.SIM</ta>
            <ta e="T454" id="Seg_4606" s="T453">send-PST2-3PL</ta>
            <ta e="T455" id="Seg_4607" s="T454">village.elder.[NOM]</ta>
            <ta e="T456" id="Seg_4608" s="T455">also</ta>
            <ta e="T457" id="Seg_4609" s="T456">come.along-EP-PST2.[3SG]</ta>
            <ta e="T458" id="Seg_4610" s="T457">reindeer-INSTR</ta>
            <ta e="T459" id="Seg_4611" s="T458">with</ta>
            <ta e="T460" id="Seg_4612" s="T459">horse-EP-INSTR</ta>
            <ta e="T461" id="Seg_4613" s="T460">go-PST2-3PL</ta>
            <ta e="T462" id="Seg_4614" s="T461">reach-PST2-3PL</ta>
            <ta e="T463" id="Seg_4615" s="T462">by.day</ta>
            <ta e="T464" id="Seg_4616" s="T463">still</ta>
            <ta e="T465" id="Seg_4617" s="T464">one</ta>
            <ta e="T466" id="Seg_4618" s="T465">NEG</ta>
            <ta e="T467" id="Seg_4619" s="T466">human.being.[NOM]</ta>
            <ta e="T468" id="Seg_4620" s="T467">NEG.EX</ta>
            <ta e="T469" id="Seg_4621" s="T468">tent-3PL-GEN</ta>
            <ta e="T470" id="Seg_4622" s="T469">ceiling-3SG.[NOM]</ta>
            <ta e="T471" id="Seg_4623" s="T470">tear-EP-PTCP.PST.[NOM]</ta>
            <ta e="T472" id="Seg_4624" s="T471">entrance-3SG.[NOM]</ta>
            <ta e="T473" id="Seg_4625" s="T472">open-PASS/REFL-CVB.SEQ</ta>
            <ta e="T474" id="Seg_4626" s="T473">stand-PRS.[3SG]</ta>
            <ta e="T475" id="Seg_4627" s="T474">tent.[NOM]</ta>
            <ta e="T476" id="Seg_4628" s="T475">inside-3SG-ACC</ta>
            <ta e="T477" id="Seg_4629" s="T476">see-PST2-3PL</ta>
            <ta e="T478" id="Seg_4630" s="T477">girl.[NOM]</ta>
            <ta e="T479" id="Seg_4631" s="T478">belly-3SG.[NOM]</ta>
            <ta e="T480" id="Seg_4632" s="T479">swell-CVB.SEQ</ta>
            <ta e="T481" id="Seg_4633" s="T480">go-CVB.SEQ</ta>
            <ta e="T482" id="Seg_4634" s="T481">lie-PRS.[3SG]</ta>
            <ta e="T483" id="Seg_4635" s="T482">village.elder.[NOM]</ta>
            <ta e="T484" id="Seg_4636" s="T483">tent-ACC</ta>
            <ta e="T485" id="Seg_4637" s="T484">light-IMP.1PL</ta>
            <ta e="T486" id="Seg_4638" s="T485">this</ta>
            <ta e="T487" id="Seg_4639" s="T486">tent-ACC</ta>
            <ta e="T488" id="Seg_4640" s="T487">girl-COM</ta>
            <ta e="T489" id="Seg_4641" s="T488">light-CVB.SEQ</ta>
            <ta e="T490" id="Seg_4642" s="T489">throw-PST2-3PL</ta>
            <ta e="T491" id="Seg_4643" s="T490">baldheaded</ta>
            <ta e="T492" id="Seg_4644" s="T491">boy.[NOM]</ta>
            <ta e="T493" id="Seg_4645" s="T492">village.elder.[NOM]</ta>
            <ta e="T494" id="Seg_4646" s="T493">daughter-3SG-ACC</ta>
            <ta e="T495" id="Seg_4647" s="T494">take-CVB.SEQ</ta>
            <ta e="T496" id="Seg_4648" s="T495">be.rich-CVB.SEQ-eat.ones.fill-CVB.SEQ</ta>
            <ta e="T497" id="Seg_4649" s="T496">live-PST2.[3SG]</ta>
         </annotation>
         <annotation name="gg" tierref="gg">
            <ta e="T1" id="Seg_4650" s="T0">Fürst-ACC</ta>
            <ta e="T2" id="Seg_4651" s="T1">mit</ta>
            <ta e="T3" id="Seg_4652" s="T2">Dorfältester.[NOM]</ta>
            <ta e="T4" id="Seg_4653" s="T3">eins-DISTR</ta>
            <ta e="T5" id="Seg_4654" s="T4">Junge-PROPR-3PL</ta>
            <ta e="T6" id="Seg_4655" s="T5">leben-PST2-3PL</ta>
            <ta e="T7" id="Seg_4656" s="T6">sein-PST2.[3SG]</ta>
            <ta e="T8" id="Seg_4657" s="T7">zwei-COLL-3PL.[NOM]</ta>
            <ta e="T9" id="Seg_4658" s="T8">dolganisch</ta>
            <ta e="T10" id="Seg_4659" s="T9">Mensch-3PL.[NOM]</ta>
            <ta e="T11" id="Seg_4660" s="T10">Junge-PL.[NOM]</ta>
            <ta e="T12" id="Seg_4661" s="T11">zusammen</ta>
            <ta e="T13" id="Seg_4662" s="T12">entstehen-CVB.SEQ</ta>
            <ta e="T14" id="Seg_4663" s="T13">Freund-SIM</ta>
            <ta e="T15" id="Seg_4664" s="T14">sehr</ta>
            <ta e="T16" id="Seg_4665" s="T15">sein-PST2-3PL</ta>
            <ta e="T17" id="Seg_4666" s="T16">dieses</ta>
            <ta e="T18" id="Seg_4667" s="T17">Mensch-PL.[NOM]</ta>
            <ta e="T19" id="Seg_4668" s="T18">alte.Zeiten-ABL</ta>
            <ta e="T20" id="Seg_4669" s="T19">nomadisieren-CVB.SIM</ta>
            <ta e="T21" id="Seg_4670" s="T20">gehen-PTCP.PRS</ta>
            <ta e="T22" id="Seg_4671" s="T21">sein-PST2-3PL</ta>
            <ta e="T23" id="Seg_4672" s="T22">nur</ta>
            <ta e="T24" id="Seg_4673" s="T23">im.Herbst</ta>
            <ta e="T25" id="Seg_4674" s="T24">Totfalle.[NOM]</ta>
            <ta e="T26" id="Seg_4675" s="T25">aufstellen-PTCP.PRS</ta>
            <ta e="T27" id="Seg_4676" s="T26">Zeit.[NOM]</ta>
            <ta e="T28" id="Seg_4677" s="T27">werden-PST2.[3SG]</ta>
            <ta e="T29" id="Seg_4678" s="T28">Köder-3PL-ACC</ta>
            <ta e="T30" id="Seg_4679" s="T29">im.Voraus</ta>
            <ta e="T31" id="Seg_4680" s="T30">fertig-VBZ-CVB.SEQ</ta>
            <ta e="T32" id="Seg_4681" s="T31">werfen-PST2-3PL</ta>
            <ta e="T33" id="Seg_4682" s="T32">zwei</ta>
            <ta e="T34" id="Seg_4683" s="T33">Junge.[NOM]</ta>
            <ta e="T35" id="Seg_4684" s="T34">Hund-3SG-INSTR</ta>
            <ta e="T36" id="Seg_4685" s="T35">Totfalle-VBZ-CVB.SIM</ta>
            <ta e="T37" id="Seg_4686" s="T36">gehen-PST2-3PL</ta>
            <ta e="T38" id="Seg_4687" s="T37">Totfalle.[NOM]</ta>
            <ta e="T39" id="Seg_4688" s="T38">aufstellen-MED-CVB.SEQ-aufstellen-MED-CVB.SEQ</ta>
            <ta e="T40" id="Seg_4689" s="T39">nachdem</ta>
            <ta e="T41" id="Seg_4690" s="T40">Zelt-3PL-DAT/LOC</ta>
            <ta e="T42" id="Seg_4691" s="T41">übernachten-PST2-3PL</ta>
            <ta e="T43" id="Seg_4692" s="T42">einmal</ta>
            <ta e="T44" id="Seg_4693" s="T43">am.Abend</ta>
            <ta e="T45" id="Seg_4694" s="T44">Hund-3PL-ACC</ta>
            <ta e="T46" id="Seg_4695" s="T45">binden-CVB.SEQ</ta>
            <ta e="T47" id="Seg_4696" s="T46">nachdem</ta>
            <ta e="T48" id="Seg_4697" s="T47">Fisch.[NOM]</ta>
            <ta e="T49" id="Seg_4698" s="T48">Fett-3SG-INSTR</ta>
            <ta e="T50" id="Seg_4699" s="T49">Leuchte.[NOM]</ta>
            <ta e="T51" id="Seg_4700" s="T50">machen-EP-PST2-3PL</ta>
            <ta e="T52" id="Seg_4701" s="T51">Tee.[NOM]</ta>
            <ta e="T53" id="Seg_4702" s="T52">trinken-CVB.SIM</ta>
            <ta e="T54" id="Seg_4703" s="T53">sitzen-PST2-3PL</ta>
            <ta e="T55" id="Seg_4704" s="T54">plötzlich</ta>
            <ta e="T56" id="Seg_4705" s="T55">Schlitten-PROPR</ta>
            <ta e="T57" id="Seg_4706" s="T56">Geräusch-3SG.[NOM]</ta>
            <ta e="T58" id="Seg_4707" s="T57">gehört.werden-EP-PST2.[3SG]</ta>
            <ta e="T59" id="Seg_4708" s="T58">Fürst.[NOM]</ta>
            <ta e="T60" id="Seg_4709" s="T59">Sohn-3SG.[NOM]</ta>
            <ta e="T61" id="Seg_4710" s="T60">sagen-PST2.[3SG]</ta>
            <ta e="T62" id="Seg_4711" s="T61">gut</ta>
            <ta e="T63" id="Seg_4712" s="T62">Mensch.[NOM]</ta>
            <ta e="T64" id="Seg_4713" s="T63">kommen-NEG-PST1-3SG</ta>
            <ta e="T65" id="Seg_4714" s="T64">wahrscheinlich</ta>
            <ta e="T66" id="Seg_4715" s="T65">Q</ta>
            <ta e="T67" id="Seg_4716" s="T66">selbst-3SG.[NOM]</ta>
            <ta e="T68" id="Seg_4717" s="T67">Tabak-VBZ-CVB.SIM</ta>
            <ta e="T69" id="Seg_4718" s="T68">sitzen-PST2.[3SG]</ta>
            <ta e="T70" id="Seg_4719" s="T69">Freund-3SG.[NOM]</ta>
            <ta e="T71" id="Seg_4720" s="T70">auf.dem.Rücken.liegen-CVB.SIM</ta>
            <ta e="T72" id="Seg_4721" s="T71">liegen-PST2.[3SG]</ta>
            <ta e="T73" id="Seg_4722" s="T72">zuhören-EP-MED-PST2-3PL</ta>
            <ta e="T74" id="Seg_4723" s="T73">Hund-3PL.[NOM]</ta>
            <ta e="T75" id="Seg_4724" s="T74">bellen-NEG.PTCP</ta>
            <ta e="T76" id="Seg_4725" s="T75">werden-PST2.[3SG]</ta>
            <ta e="T77" id="Seg_4726" s="T76">schreiten-PTCP.PRS</ta>
            <ta e="T78" id="Seg_4727" s="T77">Geräusch.[NOM]</ta>
            <ta e="T79" id="Seg_4728" s="T78">gehört.werden-EP-PST2.[3SG]</ta>
            <ta e="T80" id="Seg_4729" s="T79">Fürst.[NOM]</ta>
            <ta e="T81" id="Seg_4730" s="T80">Sohn-3SG.[NOM]</ta>
            <ta e="T82" id="Seg_4731" s="T81">Angst.haben-PRS-1SG</ta>
            <ta e="T83" id="Seg_4732" s="T82">sagen-PST2.[3SG]</ta>
            <ta e="T84" id="Seg_4733" s="T83">Freund-3SG.[NOM]</ta>
            <ta e="T85" id="Seg_4734" s="T84">jenes.EMPH.[NOM]</ta>
            <ta e="T86" id="Seg_4735" s="T85">ähnlich</ta>
            <ta e="T87" id="Seg_4736" s="T86">auf.dem.Rücken.liegen-CVB.SIM</ta>
            <ta e="T88" id="Seg_4737" s="T87">liegen-PST2.[3SG]</ta>
            <ta e="T89" id="Seg_4738" s="T88">Eingang.[NOM]</ta>
            <ta e="T90" id="Seg_4739" s="T89">öffnen-PASS/REFL-EP-PST2.[3SG]</ta>
            <ta e="T91" id="Seg_4740" s="T90">und</ta>
            <ta e="T92" id="Seg_4741" s="T91">dick</ta>
            <ta e="T93" id="Seg_4742" s="T92">sehr</ta>
            <ta e="T94" id="Seg_4743" s="T93">Frau.[NOM]</ta>
            <ta e="T95" id="Seg_4744" s="T94">kaum</ta>
            <ta e="T96" id="Seg_4745" s="T95">passen-CVB.SEQ</ta>
            <ta e="T97" id="Seg_4746" s="T96">hineingehen-PST2.[3SG]</ta>
            <ta e="T98" id="Seg_4747" s="T97">bester</ta>
            <ta e="T99" id="Seg_4748" s="T98">Perlen.[NOM]</ta>
            <ta e="T100" id="Seg_4749" s="T99">Kleidung-PROPR.[NOM]</ta>
            <ta e="T101" id="Seg_4750" s="T100">Perlen.[NOM]</ta>
            <ta e="T102" id="Seg_4751" s="T101">Mütze-PROPR.[NOM]</ta>
            <ta e="T103" id="Seg_4752" s="T102">reich</ta>
            <ta e="T104" id="Seg_4753" s="T103">sehr</ta>
            <ta e="T105" id="Seg_4754" s="T104">Gestalt-PROPR.[NOM]</ta>
            <ta e="T106" id="Seg_4755" s="T105">Dorfältester.[NOM]</ta>
            <ta e="T107" id="Seg_4756" s="T106">Sohn-3SG.[NOM]</ta>
            <ta e="T108" id="Seg_4757" s="T107">Freund-3SG-ACC</ta>
            <ta e="T109" id="Seg_4758" s="T108">Tee-VBZ.[IMP.2SG]</ta>
            <ta e="T110" id="Seg_4759" s="T109">Gast-ACC</ta>
            <ta e="T111" id="Seg_4760" s="T110">sagen-PST2.[3SG]</ta>
            <ta e="T112" id="Seg_4761" s="T111">Fürst.[NOM]</ta>
            <ta e="T113" id="Seg_4762" s="T112">Sohn-3SG.[NOM]</ta>
            <ta e="T114" id="Seg_4763" s="T113">aufstehen-CVB.SEQ</ta>
            <ta e="T115" id="Seg_4764" s="T114">Herd.[NOM]</ta>
            <ta e="T116" id="Seg_4765" s="T115">heizen-EP-MED-PST2.[3SG]</ta>
            <ta e="T117" id="Seg_4766" s="T116">Tee.[NOM]</ta>
            <ta e="T118" id="Seg_4767" s="T117">kochen-EP-CAUS-NEG.[3SG]</ta>
            <ta e="T119" id="Seg_4768" s="T118">nur</ta>
            <ta e="T120" id="Seg_4769" s="T119">Frau-3PL.[NOM]</ta>
            <ta e="T121" id="Seg_4770" s="T120">Gesicht-3SG-ACC</ta>
            <ta e="T122" id="Seg_4771" s="T121">zeigen-NEG.[3SG]</ta>
            <ta e="T123" id="Seg_4772" s="T122">sprechen-RECP/COLL-EP-NEG.[3SG]</ta>
            <ta e="T124" id="Seg_4773" s="T123">dieses</ta>
            <ta e="T125" id="Seg_4774" s="T124">Junge.[NOM]</ta>
            <ta e="T126" id="Seg_4775" s="T125">Fisch.[NOM]</ta>
            <ta e="T127" id="Seg_4776" s="T126">Kessel-3SG.[NOM]</ta>
            <ta e="T128" id="Seg_4777" s="T127">Fett-VBZ-PTCP.PST</ta>
            <ta e="T129" id="Seg_4778" s="T128">Streichholz.[NOM]</ta>
            <ta e="T130" id="Seg_4779" s="T129">Holz-3SG-INSTR</ta>
            <ta e="T131" id="Seg_4780" s="T130">Auge-3SG-ACC</ta>
            <ta e="T132" id="Seg_4781" s="T131">unterstützen-CVB.SEQ</ta>
            <ta e="T133" id="Seg_4782" s="T132">nachdem</ta>
            <ta e="T134" id="Seg_4783" s="T133">sehen-PST2-3SG</ta>
            <ta e="T135" id="Seg_4784" s="T134">Frau.[NOM]</ta>
            <ta e="T136" id="Seg_4785" s="T135">lachen-CVB.SIM</ta>
            <ta e="T137" id="Seg_4786" s="T136">sitzen-PRS.[3SG]</ta>
            <ta e="T138" id="Seg_4787" s="T137">einsam</ta>
            <ta e="T139" id="Seg_4788" s="T138">nur</ta>
            <ta e="T140" id="Seg_4789" s="T139">Zahn-PROPR.[NOM]</ta>
            <ta e="T141" id="Seg_4790" s="T140">sein-PST2.[3SG]</ta>
            <ta e="T142" id="Seg_4791" s="T141">Holz.[NOM]</ta>
            <ta e="T143" id="Seg_4792" s="T142">hereinbringen-CVB.SIM</ta>
            <ta e="T144" id="Seg_4793" s="T143">hinausgehen-EP-PST2.[3SG]</ta>
            <ta e="T145" id="Seg_4794" s="T144">Hund-3PL-ACC</ta>
            <ta e="T146" id="Seg_4795" s="T145">einspannen-FREQ-CVB.SEQ</ta>
            <ta e="T147" id="Seg_4796" s="T146">werfen-PST2.[3SG]</ta>
            <ta e="T148" id="Seg_4797" s="T147">sehen-PST2-3SG</ta>
            <ta e="T149" id="Seg_4798" s="T148">Frau.[NOM]</ta>
            <ta e="T150" id="Seg_4799" s="T149">Schlitten-3SG-DAT/LOC</ta>
            <ta e="T151" id="Seg_4800" s="T150">sechs</ta>
            <ta e="T152" id="Seg_4801" s="T151">Hund.[NOM]</ta>
            <ta e="T153" id="Seg_4802" s="T152">einspannen-PASS/REFL-EP-PTCP.PST.[NOM]</ta>
            <ta e="T154" id="Seg_4803" s="T153">hineingehen-CVB.SEQ</ta>
            <ta e="T155" id="Seg_4804" s="T154">nachdem</ta>
            <ta e="T156" id="Seg_4805" s="T155">Gast-3SG-ACC</ta>
            <ta e="T157" id="Seg_4806" s="T156">essen-EP-CAUS-PST2.[3SG]</ta>
            <ta e="T158" id="Seg_4807" s="T157">Eis.[NOM]</ta>
            <ta e="T159" id="Seg_4808" s="T158">hineinbringen-CVB.SIM</ta>
            <ta e="T160" id="Seg_4809" s="T159">hinausgehen-FUT-1SG</ta>
            <ta e="T161" id="Seg_4810" s="T160">sagen-CVB.SEQ</ta>
            <ta e="T162" id="Seg_4811" s="T161">nachdem</ta>
            <ta e="T163" id="Seg_4812" s="T162">Kleidung-PL-3SG-ACC</ta>
            <ta e="T164" id="Seg_4813" s="T163">mitnehmen-CVB.SEQ</ta>
            <ta e="T165" id="Seg_4814" s="T164">herausnehmen-PST2.[3SG]</ta>
            <ta e="T166" id="Seg_4815" s="T165">Hund-3SG-GEN</ta>
            <ta e="T167" id="Seg_4816" s="T166">Schlitten-3SG-DAT/LOC</ta>
            <ta e="T168" id="Seg_4817" s="T167">legen-CVB.SEQ</ta>
            <ta e="T169" id="Seg_4818" s="T168">werfen-PST2.[3SG]</ta>
            <ta e="T170" id="Seg_4819" s="T169">dann</ta>
            <ta e="T171" id="Seg_4820" s="T170">hineingehen-PST2.[3SG]</ta>
            <ta e="T172" id="Seg_4821" s="T171">Frau-ACC</ta>
            <ta e="T173" id="Seg_4822" s="T172">sitzen-PTCP.PST</ta>
            <ta e="T174" id="Seg_4823" s="T173">Ort-3SG-DAT/LOC</ta>
            <ta e="T175" id="Seg_4824" s="T174">schlafen-CAUS-PST2.[3SG]</ta>
            <ta e="T176" id="Seg_4825" s="T175">Fell.[NOM]</ta>
            <ta e="T177" id="Seg_4826" s="T176">geben-PST2.[3SG]</ta>
            <ta e="T178" id="Seg_4827" s="T177">Bettwäsche-EP-INSTR</ta>
            <ta e="T179" id="Seg_4828" s="T178">Fisch.[NOM]</ta>
            <ta e="T180" id="Seg_4829" s="T179">Fett-3SG.[NOM]</ta>
            <ta e="T181" id="Seg_4830" s="T180">Leuchte-3PL-ACC</ta>
            <ta e="T182" id="Seg_4831" s="T181">ausmachen-CVB.SEQ</ta>
            <ta e="T183" id="Seg_4832" s="T182">werfen-PST2-3PL</ta>
            <ta e="T184" id="Seg_4833" s="T183">einschlafen-PST2-3PL</ta>
            <ta e="T185" id="Seg_4834" s="T184">Dorfältester.[NOM]</ta>
            <ta e="T186" id="Seg_4835" s="T185">Sohn-3SG.[NOM]</ta>
            <ta e="T187" id="Seg_4836" s="T186">Freund-3SG-ACC</ta>
            <ta e="T188" id="Seg_4837" s="T187">sprechen-PST2.[3SG]</ta>
            <ta e="T189" id="Seg_4838" s="T188">nun</ta>
            <ta e="T190" id="Seg_4839" s="T189">dieses-3SG-1PL.[NOM]</ta>
            <ta e="T191" id="Seg_4840" s="T190">Mädchen.[NOM]</ta>
            <ta e="T192" id="Seg_4841" s="T191">sein-PST2.NEG.[3SG]</ta>
            <ta e="T193" id="Seg_4842" s="T192">Mädchen-VBZ-IMP.1DU</ta>
            <ta e="T194" id="Seg_4843" s="T193">was-EP-ACC</ta>
            <ta e="T195" id="Seg_4844" s="T194">erschrecken-PRS-2SG</ta>
            <ta e="T196" id="Seg_4845" s="T195">1SG.[NOM]</ta>
            <ta e="T197" id="Seg_4846" s="T196">dann</ta>
            <ta e="T198" id="Seg_4847" s="T197">gehen-FUT-1SG</ta>
            <ta e="T199" id="Seg_4848" s="T198">Freund-3SG.[NOM]</ta>
            <ta e="T200" id="Seg_4849" s="T199">1SG.[NOM]</ta>
            <ta e="T201" id="Seg_4850" s="T200">hinausgehen-EP-PTCP.PST-EP-1SG.[NOM]</ta>
            <ta e="T202" id="Seg_4851" s="T201">nachdem</ta>
            <ta e="T203" id="Seg_4852" s="T202">Mädchen-2SG-DAT/LOC</ta>
            <ta e="T204" id="Seg_4853" s="T203">gehen-FUT.[IMP.2SG]</ta>
            <ta e="T205" id="Seg_4854" s="T204">hinausgehen-CVB.SEQ</ta>
            <ta e="T206" id="Seg_4855" s="T205">bleiben-PST2.[3SG]</ta>
            <ta e="T207" id="Seg_4856" s="T206">sich.anziehen-CVB.SEQ</ta>
            <ta e="T208" id="Seg_4857" s="T207">nachdem</ta>
            <ta e="T209" id="Seg_4858" s="T208">Schlitten-3SG-DAT/LOC</ta>
            <ta e="T210" id="Seg_4859" s="T209">sich.setzen-PST2.[3SG]</ta>
            <ta e="T211" id="Seg_4860" s="T210">wie.viel</ta>
            <ta e="T212" id="Seg_4861" s="T211">sein-FUT.[3SG]=Q</ta>
            <ta e="T213" id="Seg_4862" s="T212">sterben-PTCP.PRS-bleiben-PTCP.PRS</ta>
            <ta e="T214" id="Seg_4863" s="T213">Schrei.[NOM]</ta>
            <ta e="T215" id="Seg_4864" s="T214">gehört.werden-EP-PST2.[3SG]</ta>
            <ta e="T216" id="Seg_4865" s="T215">oh.nein</ta>
            <ta e="T217" id="Seg_4866" s="T216">zu.Hilfe.kommen.[IMP.2SG]</ta>
            <ta e="T218" id="Seg_4867" s="T217">dieses</ta>
            <ta e="T219" id="Seg_4868" s="T218">Junge.[NOM]</ta>
            <ta e="T220" id="Seg_4869" s="T219">Zügel-3SG-ACC</ta>
            <ta e="T221" id="Seg_4870" s="T220">nehmen-PST1-3SG</ta>
            <ta e="T222" id="Seg_4871" s="T221">EMPH</ta>
            <ta e="T223" id="Seg_4872" s="T222">rennen-CVB.SEQ</ta>
            <ta e="T224" id="Seg_4873" s="T223">bleiben-PST2.[3SG]</ta>
            <ta e="T225" id="Seg_4874" s="T224">Hinterteil-3SG-ABL</ta>
            <ta e="T226" id="Seg_4875" s="T225">Frau.[NOM]</ta>
            <ta e="T227" id="Seg_4876" s="T226">Stimme-3SG.[NOM]</ta>
            <ta e="T228" id="Seg_4877" s="T227">gehört.werden-EP-PST2.[3SG]</ta>
            <ta e="T229" id="Seg_4878" s="T228">wissen-PTCP.PST-EP-1SG.[NOM]</ta>
            <ta e="T230" id="Seg_4879" s="T229">sein-COND.[3SG]</ta>
            <ta e="T231" id="Seg_4880" s="T230">essen-CVB.SEQ</ta>
            <ta e="T232" id="Seg_4881" s="T231">werfen-PTCP.FUT</ta>
            <ta e="T233" id="Seg_4882" s="T232">sein-PST1-1SG</ta>
            <ta e="T234" id="Seg_4883" s="T233">EMPH</ta>
            <ta e="T235" id="Seg_4884" s="T234">kommen-CVB.SEQ</ta>
            <ta e="T236" id="Seg_4885" s="T235">dieses</ta>
            <ta e="T237" id="Seg_4886" s="T236">Junge.[NOM]</ta>
            <ta e="T238" id="Seg_4887" s="T237">Vater-3SG-DAT/LOC</ta>
            <ta e="T239" id="Seg_4888" s="T238">Fürst-DAT/LOC</ta>
            <ta e="T240" id="Seg_4889" s="T239">erzählen-PST2.[3SG]</ta>
            <ta e="T241" id="Seg_4890" s="T240">Fürst-ACC</ta>
            <ta e="T242" id="Seg_4891" s="T241">mit</ta>
            <ta e="T243" id="Seg_4892" s="T242">Dorfältester.[NOM]</ta>
            <ta e="T244" id="Seg_4893" s="T243">sieben</ta>
            <ta e="T245" id="Seg_4894" s="T244">Zelt-ACC</ta>
            <ta e="T246" id="Seg_4895" s="T245">sammeln-CVB.SEQ</ta>
            <ta e="T247" id="Seg_4896" s="T246">Versammlung-VBZ-PST2-3PL</ta>
            <ta e="T248" id="Seg_4897" s="T247">zwei</ta>
            <ta e="T249" id="Seg_4898" s="T248">Junge-ACC</ta>
            <ta e="T250" id="Seg_4899" s="T249">schicken-PST2-3PL</ta>
            <ta e="T251" id="Seg_4900" s="T250">sehen-PTCP.FUT-3PL-ACC</ta>
            <ta e="T252" id="Seg_4901" s="T251">zwei</ta>
            <ta e="T253" id="Seg_4902" s="T252">Junge.[NOM]</ta>
            <ta e="T254" id="Seg_4903" s="T253">sein-CVB.SEQ</ta>
            <ta e="T255" id="Seg_4904" s="T254">gehen-CVB.SEQ</ta>
            <ta e="T256" id="Seg_4905" s="T255">bleiben-PST1-3PL</ta>
            <ta e="T257" id="Seg_4906" s="T256">lange</ta>
            <ta e="T258" id="Seg_4907" s="T257">sein-FUT-3PL=Q</ta>
            <ta e="T259" id="Seg_4908" s="T258">Licht-DAT/LOC</ta>
            <ta e="T260" id="Seg_4909" s="T259">kommen-PST2-3PL</ta>
            <ta e="T261" id="Seg_4910" s="T260">sehen-PST2-3PL</ta>
            <ta e="T262" id="Seg_4911" s="T261">eins</ta>
            <ta e="T263" id="Seg_4912" s="T262">NEG</ta>
            <ta e="T264" id="Seg_4913" s="T263">Mensch.[NOM]</ta>
            <ta e="T265" id="Seg_4914" s="T264">NEG.EX</ta>
            <ta e="T266" id="Seg_4915" s="T265">Spur.[NOM]</ta>
            <ta e="T267" id="Seg_4916" s="T266">NEG</ta>
            <ta e="T268" id="Seg_4917" s="T267">NEG.EX</ta>
            <ta e="T269" id="Seg_4918" s="T268">Stange.[NOM]</ta>
            <ta e="T270" id="Seg_4919" s="T269">Zelt-DAT/LOC</ta>
            <ta e="T271" id="Seg_4920" s="T270">hineingehen-PST2-3PL</ta>
            <ta e="T272" id="Seg_4921" s="T271">Junge.[NOM]</ta>
            <ta e="T273" id="Seg_4922" s="T272">Knochen-3SG.[NOM]</ta>
            <ta e="T274" id="Seg_4923" s="T273">nur</ta>
            <ta e="T275" id="Seg_4924" s="T274">sehr.knochig.sein-CVB.SIM</ta>
            <ta e="T276" id="Seg_4925" s="T275">liegen-PRS.[3SG]</ta>
            <ta e="T277" id="Seg_4926" s="T276">dieses</ta>
            <ta e="T278" id="Seg_4927" s="T277">zwei</ta>
            <ta e="T279" id="Seg_4928" s="T278">Junge.[NOM]</ta>
            <ta e="T280" id="Seg_4929" s="T279">zurückkommen-EP-PST2-3PL</ta>
            <ta e="T281" id="Seg_4930" s="T280">Fürst-DAT/LOC</ta>
            <ta e="T282" id="Seg_4931" s="T281">erzählen-PST2-3PL</ta>
            <ta e="T283" id="Seg_4932" s="T282">eins</ta>
            <ta e="T284" id="Seg_4933" s="T283">Waise.[NOM]</ta>
            <ta e="T285" id="Seg_4934" s="T284">Kind.[NOM]</ta>
            <ta e="T286" id="Seg_4935" s="T285">es.gibt</ta>
            <ta e="T287" id="Seg_4936" s="T286">zwei</ta>
            <ta e="T288" id="Seg_4937" s="T287">Schulter-ABL</ta>
            <ta e="T289" id="Seg_4938" s="T288">glatzköpfig.[NOM]</ta>
            <ta e="T290" id="Seg_4939" s="T289">einsam</ta>
            <ta e="T291" id="Seg_4940" s="T290">Mutter-PROPR.[NOM]</ta>
            <ta e="T292" id="Seg_4941" s="T291">eins</ta>
            <ta e="T293" id="Seg_4942" s="T292">Rentier-PROPR.[NOM]</ta>
            <ta e="T294" id="Seg_4943" s="T293">Fürst-ACC</ta>
            <ta e="T295" id="Seg_4944" s="T294">mit</ta>
            <ta e="T296" id="Seg_4945" s="T295">Dorfältester.[NOM]</ta>
            <ta e="T297" id="Seg_4946" s="T296">Versammlung-VBZ-PST2-3PL</ta>
            <ta e="T298" id="Seg_4947" s="T297">wie</ta>
            <ta e="T299" id="Seg_4948" s="T298">sein-FUT-1PL=Q</ta>
            <ta e="T300" id="Seg_4949" s="T299">Erde-1PL-ACC</ta>
            <ta e="T301" id="Seg_4950" s="T300">werfen-FUT-1PL</ta>
            <ta e="T302" id="Seg_4951" s="T301">Q</ta>
            <ta e="T303" id="Seg_4952" s="T302">auch</ta>
            <ta e="T304" id="Seg_4953" s="T303">und</ta>
            <ta e="T305" id="Seg_4954" s="T304">Totfalle-PL-1PL.[NOM]</ta>
            <ta e="T307" id="Seg_4955" s="T305">es.gibt-3PL</ta>
            <ta e="T308" id="Seg_4956" s="T307">sagen-EP-RECP/COLL-PST2-3PL</ta>
            <ta e="T309" id="Seg_4957" s="T308">Dorfältester.[NOM]</ta>
            <ta e="T310" id="Seg_4958" s="T309">dort</ta>
            <ta e="T311" id="Seg_4959" s="T310">stehen-CVB.SEQ</ta>
            <ta e="T312" id="Seg_4960" s="T311">sprechen-PST2.[3SG]</ta>
            <ta e="T313" id="Seg_4961" s="T312">Waise.[NOM]</ta>
            <ta e="T314" id="Seg_4962" s="T313">Kind-ACC</ta>
            <ta e="T315" id="Seg_4963" s="T314">schicken-PTCP.FUT-DAT/LOC</ta>
            <ta e="T316" id="Seg_4964" s="T315">1SG.[NOM]</ta>
            <ta e="T317" id="Seg_4965" s="T316">Tochter-1SG-ACC</ta>
            <ta e="T318" id="Seg_4966" s="T317">und</ta>
            <ta e="T319" id="Seg_4967" s="T318">geben-PTCP.FUT-1SG</ta>
            <ta e="T320" id="Seg_4968" s="T319">sein-PST1-3SG</ta>
            <ta e="T321" id="Seg_4969" s="T320">töten-TEMP-3SG</ta>
            <ta e="T322" id="Seg_4970" s="T321">dieses</ta>
            <ta e="T323" id="Seg_4971" s="T322">Junge-ACC</ta>
            <ta e="T324" id="Seg_4972" s="T323">rufen-PST2-3PL</ta>
            <ta e="T325" id="Seg_4973" s="T324">erzählen-PST2-3PL</ta>
            <ta e="T326" id="Seg_4974" s="T325">jeder-3SG-ACC</ta>
            <ta e="T327" id="Seg_4975" s="T326">doch</ta>
            <ta e="T328" id="Seg_4976" s="T327">INTJ</ta>
            <ta e="T329" id="Seg_4977" s="T328">sein-PST2.[3SG]</ta>
            <ta e="T330" id="Seg_4978" s="T329">Kind.[NOM]</ta>
            <ta e="T331" id="Seg_4979" s="T330">Unglück-PROPR</ta>
            <ta e="T332" id="Seg_4980" s="T331">einsam</ta>
            <ta e="T333" id="Seg_4981" s="T332">gehen-PTCP.PRS</ta>
            <ta e="T334" id="Seg_4982" s="T333">sein-PST1-3SG</ta>
            <ta e="T335" id="Seg_4983" s="T334">Hund.[NOM]</ta>
            <ta e="T336" id="Seg_4984" s="T335">nur</ta>
            <ta e="T337" id="Seg_4985" s="T336">geben-PST2-3PL</ta>
            <ta e="T338" id="Seg_4986" s="T337">Gewehr.[NOM]</ta>
            <ta e="T339" id="Seg_4987" s="T338">NEG</ta>
            <ta e="T340" id="Seg_4988" s="T339">was.[NOM]</ta>
            <ta e="T341" id="Seg_4989" s="T340">NEG</ta>
            <ta e="T342" id="Seg_4990" s="T341">NEG.EX</ta>
            <ta e="T343" id="Seg_4991" s="T342">Haus-3SG-DAT/LOC</ta>
            <ta e="T344" id="Seg_4992" s="T343">kommen-CVB.SEQ</ta>
            <ta e="T345" id="Seg_4993" s="T344">weinen-EP-MED-CVB.SIM</ta>
            <ta e="T346" id="Seg_4994" s="T345">sitzen-PST2.[3SG]</ta>
            <ta e="T347" id="Seg_4995" s="T346">Mutter-3SG.[NOM]</ta>
            <ta e="T348" id="Seg_4996" s="T347">was.[NOM]</ta>
            <ta e="T349" id="Seg_4997" s="T348">sein-PST1-2SG</ta>
            <ta e="T350" id="Seg_4998" s="T349">erzählen-PST2.[3SG]</ta>
            <ta e="T351" id="Seg_4999" s="T350">offenbar</ta>
            <ta e="T352" id="Seg_5000" s="T351">Vater-2SG.[NOM]</ta>
            <ta e="T353" id="Seg_5001" s="T352">Glefe-3SG.[NOM]</ta>
            <ta e="T354" id="Seg_5002" s="T353">wo</ta>
            <ta e="T355" id="Seg_5003" s="T354">INDEF</ta>
            <ta e="T356" id="Seg_5004" s="T355">es.gibt</ta>
            <ta e="T357" id="Seg_5005" s="T356">sein-PST1-3SG</ta>
            <ta e="T358" id="Seg_5006" s="T357">jenes-ACC</ta>
            <ta e="T359" id="Seg_5007" s="T358">tragen-FUT.[IMP.2SG]</ta>
            <ta e="T360" id="Seg_5008" s="T359">Mutter-3SG.[NOM]</ta>
            <ta e="T361" id="Seg_5009" s="T360">Unglück-PROPR</ta>
            <ta e="T362" id="Seg_5010" s="T361">dicht</ta>
            <ta e="T363" id="Seg_5011" s="T362">rosten-PTCP.PST</ta>
            <ta e="T364" id="Seg_5012" s="T363">Glefe-ACC</ta>
            <ta e="T365" id="Seg_5013" s="T364">hereinbringen-PST2.[3SG]</ta>
            <ta e="T366" id="Seg_5014" s="T365">dieses</ta>
            <ta e="T367" id="Seg_5015" s="T366">Junge.[NOM]</ta>
            <ta e="T368" id="Seg_5016" s="T367">lange</ta>
            <ta e="T369" id="Seg_5017" s="T368">sein-NEG.CVB.SIM</ta>
            <ta e="T370" id="Seg_5018" s="T369">gehen-CVB.SEQ</ta>
            <ta e="T371" id="Seg_5019" s="T370">bleiben-PST2.[3SG]</ta>
            <ta e="T372" id="Seg_5020" s="T371">zu.Ende.gehen-FUT.[3SG]</ta>
            <ta e="T373" id="Seg_5021" s="T372">Q</ta>
            <ta e="T374" id="Seg_5022" s="T373">kommen-PST2-3SG</ta>
            <ta e="T375" id="Seg_5023" s="T374">wer.[NOM]</ta>
            <ta e="T376" id="Seg_5024" s="T375">NEG</ta>
            <ta e="T377" id="Seg_5025" s="T376">NEG.EX</ta>
            <ta e="T378" id="Seg_5026" s="T377">Mensch.[NOM]</ta>
            <ta e="T379" id="Seg_5027" s="T378">nur</ta>
            <ta e="T380" id="Seg_5028" s="T379">Knochen-3SG.[NOM]</ta>
            <ta e="T381" id="Seg_5029" s="T380">Zelt-DAT/LOC</ta>
            <ta e="T382" id="Seg_5030" s="T381">Abenddämmerung.[NOM]</ta>
            <ta e="T383" id="Seg_5031" s="T382">fallen-PTCP.PST-3SG-ACC</ta>
            <ta e="T384" id="Seg_5032" s="T383">nachdem</ta>
            <ta e="T385" id="Seg_5033" s="T384">Leuchte-3SG-ACC</ta>
            <ta e="T386" id="Seg_5034" s="T385">anzünden-PST2.[3SG]</ta>
            <ta e="T387" id="Seg_5035" s="T386">Mensch.[NOM]</ta>
            <ta e="T388" id="Seg_5036" s="T387">schlafen-PTCP.PRS</ta>
            <ta e="T389" id="Seg_5037" s="T388">Zeit-3SG-ACC</ta>
            <ta e="T390" id="Seg_5038" s="T389">zu</ta>
            <ta e="T391" id="Seg_5039" s="T390">Schlitten-PROPR</ta>
            <ta e="T392" id="Seg_5040" s="T391">Geräusch-3SG.[NOM]</ta>
            <ta e="T393" id="Seg_5041" s="T392">gehört.werden-EP-PST2.[3SG]</ta>
            <ta e="T394" id="Seg_5042" s="T393">Hund.[NOM]</ta>
            <ta e="T395" id="Seg_5043" s="T394">und</ta>
            <ta e="T396" id="Seg_5044" s="T395">bellen-PST2.NEG.[3SG]</ta>
            <ta e="T397" id="Seg_5045" s="T396">Feuer-3SG-ACC</ta>
            <ta e="T398" id="Seg_5046" s="T397">ausmachen-CVB.SEQ</ta>
            <ta e="T399" id="Seg_5047" s="T398">nachdem</ta>
            <ta e="T400" id="Seg_5048" s="T399">sitzen-PST2.[3SG]</ta>
            <ta e="T401" id="Seg_5049" s="T400">Geräusch.[NOM]</ta>
            <ta e="T402" id="Seg_5050" s="T401">kräftig.[NOM]</ta>
            <ta e="T403" id="Seg_5051" s="T402">werden-PST2.[3SG]</ta>
            <ta e="T404" id="Seg_5052" s="T403">Eingang-DAT/LOC</ta>
            <ta e="T405" id="Seg_5053" s="T404">passen-NEG.PTCP</ta>
            <ta e="T406" id="Seg_5054" s="T405">dick</ta>
            <ta e="T407" id="Seg_5055" s="T406">Frau.[NOM]</ta>
            <ta e="T408" id="Seg_5056" s="T407">hineingehen-PST2.[3SG]</ta>
            <ta e="T409" id="Seg_5057" s="T408">anderer.von.zwei</ta>
            <ta e="T410" id="Seg_5058" s="T409">Bein-3SG-ACC</ta>
            <ta e="T411" id="Seg_5059" s="T410">Tür-DAT/LOC</ta>
            <ta e="T412" id="Seg_5060" s="T411">stoßen-PTCP.PRS-3SG-ACC</ta>
            <ta e="T413" id="Seg_5061" s="T412">mit</ta>
            <ta e="T414" id="Seg_5062" s="T413">zwei</ta>
            <ta e="T415" id="Seg_5063" s="T414">Achselhöhle-3SG-GEN</ta>
            <ta e="T416" id="Seg_5064" s="T415">Unterteil-3SG-INSTR</ta>
            <ta e="T417" id="Seg_5065" s="T416">Glefe-INSTR</ta>
            <ta e="T418" id="Seg_5066" s="T417">nach.innen</ta>
            <ta e="T419" id="Seg_5067" s="T418">stoßen-PST2.[3SG]</ta>
            <ta e="T420" id="Seg_5068" s="T419">selbst-3SG.[NOM]</ta>
            <ta e="T421" id="Seg_5069" s="T420">Achselhöhle-3SG-GEN</ta>
            <ta e="T422" id="Seg_5070" s="T421">Unterteil-3SG-INSTR</ta>
            <ta e="T423" id="Seg_5071" s="T422">entfliehen-PST2.[3SG]</ta>
            <ta e="T424" id="Seg_5072" s="T423">nachts</ta>
            <ta e="T425" id="Seg_5073" s="T424">Haus-3SG-DAT/LOC</ta>
            <ta e="T426" id="Seg_5074" s="T425">kommen-PST2.[3SG]</ta>
            <ta e="T427" id="Seg_5075" s="T426">Mutter-3SG.[NOM]</ta>
            <ta e="T428" id="Seg_5076" s="T427">Unglück-PROPR</ta>
            <ta e="T429" id="Seg_5077" s="T428">schlafen-NEG.CVB.SIM</ta>
            <ta e="T430" id="Seg_5078" s="T429">warten-CVB.SEQ</ta>
            <ta e="T431" id="Seg_5079" s="T430">morgens</ta>
            <ta e="T432" id="Seg_5080" s="T431">Mutter-3SG.[NOM]</ta>
            <ta e="T433" id="Seg_5081" s="T432">Fürst-DAT/LOC</ta>
            <ta e="T434" id="Seg_5082" s="T433">gehen-CVB.SEQ</ta>
            <ta e="T435" id="Seg_5083" s="T434">erzählen-PST2.[3SG]</ta>
            <ta e="T436" id="Seg_5084" s="T435">Fürst.[NOM]</ta>
            <ta e="T437" id="Seg_5085" s="T436">dieses</ta>
            <ta e="T438" id="Seg_5086" s="T437">Junge-ACC</ta>
            <ta e="T439" id="Seg_5087" s="T438">rufen-PST2.[3SG]</ta>
            <ta e="T440" id="Seg_5088" s="T439">doch</ta>
            <ta e="T441" id="Seg_5089" s="T440">wie=Q</ta>
            <ta e="T442" id="Seg_5090" s="T441">töten-PST1-2SG</ta>
            <ta e="T443" id="Seg_5091" s="T442">Q</ta>
            <ta e="T444" id="Seg_5092" s="T443">wissen-NEG-1SG</ta>
            <ta e="T445" id="Seg_5093" s="T444">sagen-PST2.[3SG]</ta>
            <ta e="T446" id="Seg_5094" s="T445">Junge.[NOM]</ta>
            <ta e="T447" id="Seg_5095" s="T446">erzählen-PST2.[3SG]</ta>
            <ta e="T448" id="Seg_5096" s="T447">wie</ta>
            <ta e="T449" id="Seg_5097" s="T448">machen-PTCP.PST-3SG-ACC</ta>
            <ta e="T450" id="Seg_5098" s="T449">Versammlung-VBZ-PST2-3PL</ta>
            <ta e="T451" id="Seg_5099" s="T450">sechs</ta>
            <ta e="T452" id="Seg_5100" s="T451">Mensch-ACC</ta>
            <ta e="T453" id="Seg_5101" s="T452">sehen-CAUS-CVB.SIM</ta>
            <ta e="T454" id="Seg_5102" s="T453">schicken-PST2-3PL</ta>
            <ta e="T455" id="Seg_5103" s="T454">Dorfältester.[NOM]</ta>
            <ta e="T456" id="Seg_5104" s="T455">auch</ta>
            <ta e="T457" id="Seg_5105" s="T456">mitkommen-EP-PST2.[3SG]</ta>
            <ta e="T458" id="Seg_5106" s="T457">Rentier-INSTR</ta>
            <ta e="T459" id="Seg_5107" s="T458">mit</ta>
            <ta e="T460" id="Seg_5108" s="T459">Pferd-EP-INSTR</ta>
            <ta e="T461" id="Seg_5109" s="T460">gehen-PST2-3PL</ta>
            <ta e="T462" id="Seg_5110" s="T461">ankommen-PST2-3PL</ta>
            <ta e="T463" id="Seg_5111" s="T462">am.Tag</ta>
            <ta e="T464" id="Seg_5112" s="T463">noch</ta>
            <ta e="T465" id="Seg_5113" s="T464">eins</ta>
            <ta e="T466" id="Seg_5114" s="T465">NEG</ta>
            <ta e="T467" id="Seg_5115" s="T466">Mensch.[NOM]</ta>
            <ta e="T468" id="Seg_5116" s="T467">NEG.EX</ta>
            <ta e="T469" id="Seg_5117" s="T468">Zelt-3PL-GEN</ta>
            <ta e="T470" id="Seg_5118" s="T469">Decke-3SG.[NOM]</ta>
            <ta e="T471" id="Seg_5119" s="T470">zerreißen-EP-PTCP.PST.[NOM]</ta>
            <ta e="T472" id="Seg_5120" s="T471">Eingang-3SG.[NOM]</ta>
            <ta e="T473" id="Seg_5121" s="T472">öffnen-PASS/REFL-CVB.SEQ</ta>
            <ta e="T474" id="Seg_5122" s="T473">stehen-PRS.[3SG]</ta>
            <ta e="T475" id="Seg_5123" s="T474">Zelt.[NOM]</ta>
            <ta e="T476" id="Seg_5124" s="T475">Inneres-3SG-ACC</ta>
            <ta e="T477" id="Seg_5125" s="T476">sehen-PST2-3PL</ta>
            <ta e="T478" id="Seg_5126" s="T477">Mädchen.[NOM]</ta>
            <ta e="T479" id="Seg_5127" s="T478">Bauch-3SG.[NOM]</ta>
            <ta e="T480" id="Seg_5128" s="T479">schwellen-CVB.SEQ</ta>
            <ta e="T481" id="Seg_5129" s="T480">gehen-CVB.SEQ</ta>
            <ta e="T482" id="Seg_5130" s="T481">liegen-PRS.[3SG]</ta>
            <ta e="T483" id="Seg_5131" s="T482">Dorfältester.[NOM]</ta>
            <ta e="T484" id="Seg_5132" s="T483">Zelt-ACC</ta>
            <ta e="T485" id="Seg_5133" s="T484">anzünden-IMP.1PL</ta>
            <ta e="T486" id="Seg_5134" s="T485">dieses</ta>
            <ta e="T487" id="Seg_5135" s="T486">Zelt-ACC</ta>
            <ta e="T488" id="Seg_5136" s="T487">Mädchen-COM</ta>
            <ta e="T489" id="Seg_5137" s="T488">anzünden-CVB.SEQ</ta>
            <ta e="T490" id="Seg_5138" s="T489">werfen-PST2-3PL</ta>
            <ta e="T491" id="Seg_5139" s="T490">glatzköpfig</ta>
            <ta e="T492" id="Seg_5140" s="T491">Junge.[NOM]</ta>
            <ta e="T493" id="Seg_5141" s="T492">Dorfältester.[NOM]</ta>
            <ta e="T494" id="Seg_5142" s="T493">Tochter-3SG-ACC</ta>
            <ta e="T495" id="Seg_5143" s="T494">nehmen-CVB.SEQ</ta>
            <ta e="T496" id="Seg_5144" s="T495">reich.sein-CVB.SEQ-sich.satt.essen-CVB.SEQ</ta>
            <ta e="T497" id="Seg_5145" s="T496">leben-PST2.[3SG]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_5146" s="T0">князь-ACC</ta>
            <ta e="T2" id="Seg_5147" s="T1">с</ta>
            <ta e="T3" id="Seg_5148" s="T2">старшина.[NOM]</ta>
            <ta e="T4" id="Seg_5149" s="T3">один-DISTR</ta>
            <ta e="T5" id="Seg_5150" s="T4">мальчик-PROPR-3PL</ta>
            <ta e="T6" id="Seg_5151" s="T5">жить-PST2-3PL</ta>
            <ta e="T7" id="Seg_5152" s="T6">быть-PST2.[3SG]</ta>
            <ta e="T8" id="Seg_5153" s="T7">два-COLL-3PL.[NOM]</ta>
            <ta e="T9" id="Seg_5154" s="T8">долганский</ta>
            <ta e="T10" id="Seg_5155" s="T9">человек-3PL.[NOM]</ta>
            <ta e="T11" id="Seg_5156" s="T10">мальчик-PL.[NOM]</ta>
            <ta e="T12" id="Seg_5157" s="T11">вместе</ta>
            <ta e="T13" id="Seg_5158" s="T12">создаваться-CVB.SEQ</ta>
            <ta e="T14" id="Seg_5159" s="T13">друг-SIM</ta>
            <ta e="T15" id="Seg_5160" s="T14">очень</ta>
            <ta e="T16" id="Seg_5161" s="T15">быть-PST2-3PL</ta>
            <ta e="T17" id="Seg_5162" s="T16">этот</ta>
            <ta e="T18" id="Seg_5163" s="T17">человек-PL.[NOM]</ta>
            <ta e="T19" id="Seg_5164" s="T18">старина-ABL</ta>
            <ta e="T20" id="Seg_5165" s="T19">кочевать-CVB.SIM</ta>
            <ta e="T21" id="Seg_5166" s="T20">идти-PTCP.PRS</ta>
            <ta e="T22" id="Seg_5167" s="T21">быть-PST2-3PL</ta>
            <ta e="T23" id="Seg_5168" s="T22">только</ta>
            <ta e="T24" id="Seg_5169" s="T23">осенью</ta>
            <ta e="T25" id="Seg_5170" s="T24">пасть.[NOM]</ta>
            <ta e="T26" id="Seg_5171" s="T25">ставить-PTCP.PRS</ta>
            <ta e="T27" id="Seg_5172" s="T26">час.[NOM]</ta>
            <ta e="T28" id="Seg_5173" s="T27">становиться-PST2.[3SG]</ta>
            <ta e="T29" id="Seg_5174" s="T28">приманка-3PL-ACC</ta>
            <ta e="T30" id="Seg_5175" s="T29">заранее</ta>
            <ta e="T31" id="Seg_5176" s="T30">готовый-VBZ-CVB.SEQ</ta>
            <ta e="T32" id="Seg_5177" s="T31">бросать-PST2-3PL</ta>
            <ta e="T33" id="Seg_5178" s="T32">два</ta>
            <ta e="T34" id="Seg_5179" s="T33">мальчик.[NOM]</ta>
            <ta e="T35" id="Seg_5180" s="T34">собака-3SG-INSTR</ta>
            <ta e="T36" id="Seg_5181" s="T35">пасть-VBZ-CVB.SIM</ta>
            <ta e="T37" id="Seg_5182" s="T36">идти-PST2-3PL</ta>
            <ta e="T38" id="Seg_5183" s="T37">пасть.[NOM]</ta>
            <ta e="T39" id="Seg_5184" s="T38">ставить-MED-CVB.SEQ-ставить-MED-CVB.SEQ</ta>
            <ta e="T40" id="Seg_5185" s="T39">после</ta>
            <ta e="T41" id="Seg_5186" s="T40">чум-3PL-DAT/LOC</ta>
            <ta e="T42" id="Seg_5187" s="T41">ночевать-PST2-3PL</ta>
            <ta e="T43" id="Seg_5188" s="T42">однажды</ta>
            <ta e="T44" id="Seg_5189" s="T43">вечером</ta>
            <ta e="T45" id="Seg_5190" s="T44">собака-3PL-ACC</ta>
            <ta e="T46" id="Seg_5191" s="T45">связывать-CVB.SEQ</ta>
            <ta e="T47" id="Seg_5192" s="T46">после</ta>
            <ta e="T48" id="Seg_5193" s="T47">рыба.[NOM]</ta>
            <ta e="T49" id="Seg_5194" s="T48">сало-3SG-INSTR</ta>
            <ta e="T50" id="Seg_5195" s="T49">светильник.[NOM]</ta>
            <ta e="T51" id="Seg_5196" s="T50">делать-EP-PST2-3PL</ta>
            <ta e="T52" id="Seg_5197" s="T51">чай.[NOM]</ta>
            <ta e="T53" id="Seg_5198" s="T52">пить-CVB.SIM</ta>
            <ta e="T54" id="Seg_5199" s="T53">сидеть-PST2-3PL</ta>
            <ta e="T55" id="Seg_5200" s="T54">вдруг</ta>
            <ta e="T56" id="Seg_5201" s="T55">сани-PROPR</ta>
            <ta e="T57" id="Seg_5202" s="T56">звук-3SG.[NOM]</ta>
            <ta e="T58" id="Seg_5203" s="T57">слышаться-EP-PST2.[3SG]</ta>
            <ta e="T59" id="Seg_5204" s="T58">князь.[NOM]</ta>
            <ta e="T60" id="Seg_5205" s="T59">сын-3SG.[NOM]</ta>
            <ta e="T61" id="Seg_5206" s="T60">говорить-PST2.[3SG]</ta>
            <ta e="T62" id="Seg_5207" s="T61">добрый</ta>
            <ta e="T63" id="Seg_5208" s="T62">человек.[NOM]</ta>
            <ta e="T64" id="Seg_5209" s="T63">приходить-NEG-PST1-3SG</ta>
            <ta e="T65" id="Seg_5210" s="T64">наверное</ta>
            <ta e="T66" id="Seg_5211" s="T65">Q</ta>
            <ta e="T67" id="Seg_5212" s="T66">сам-3SG.[NOM]</ta>
            <ta e="T68" id="Seg_5213" s="T67">табак-VBZ-CVB.SIM</ta>
            <ta e="T69" id="Seg_5214" s="T68">сидеть-PST2.[3SG]</ta>
            <ta e="T70" id="Seg_5215" s="T69">друг-3SG.[NOM]</ta>
            <ta e="T71" id="Seg_5216" s="T70">лежать.навзничь-CVB.SIM</ta>
            <ta e="T72" id="Seg_5217" s="T71">лежать-PST2.[3SG]</ta>
            <ta e="T73" id="Seg_5218" s="T72">слушать-EP-MED-PST2-3PL</ta>
            <ta e="T74" id="Seg_5219" s="T73">собака-3PL.[NOM]</ta>
            <ta e="T75" id="Seg_5220" s="T74">лаять-NEG.PTCP</ta>
            <ta e="T76" id="Seg_5221" s="T75">становиться-PST2.[3SG]</ta>
            <ta e="T77" id="Seg_5222" s="T76">шагать-PTCP.PRS</ta>
            <ta e="T78" id="Seg_5223" s="T77">звук.[NOM]</ta>
            <ta e="T79" id="Seg_5224" s="T78">слышаться-EP-PST2.[3SG]</ta>
            <ta e="T80" id="Seg_5225" s="T79">князь.[NOM]</ta>
            <ta e="T81" id="Seg_5226" s="T80">сын-3SG.[NOM]</ta>
            <ta e="T82" id="Seg_5227" s="T81">бояться-PRS-1SG</ta>
            <ta e="T83" id="Seg_5228" s="T82">говорить-PST2.[3SG]</ta>
            <ta e="T84" id="Seg_5229" s="T83">друг-3SG.[NOM]</ta>
            <ta e="T85" id="Seg_5230" s="T84">тот.EMPH.[NOM]</ta>
            <ta e="T86" id="Seg_5231" s="T85">подобно</ta>
            <ta e="T87" id="Seg_5232" s="T86">лежать.навзничь-CVB.SIM</ta>
            <ta e="T88" id="Seg_5233" s="T87">лежать-PST2.[3SG]</ta>
            <ta e="T89" id="Seg_5234" s="T88">вход.[NOM]</ta>
            <ta e="T90" id="Seg_5235" s="T89">открывать-PASS/REFL-EP-PST2.[3SG]</ta>
            <ta e="T91" id="Seg_5236" s="T90">да</ta>
            <ta e="T92" id="Seg_5237" s="T91">толстый</ta>
            <ta e="T93" id="Seg_5238" s="T92">очень</ta>
            <ta e="T94" id="Seg_5239" s="T93">жена.[NOM]</ta>
            <ta e="T95" id="Seg_5240" s="T94">еле</ta>
            <ta e="T96" id="Seg_5241" s="T95">подходить-CVB.SEQ</ta>
            <ta e="T97" id="Seg_5242" s="T96">входить-PST2.[3SG]</ta>
            <ta e="T98" id="Seg_5243" s="T97">лучший</ta>
            <ta e="T99" id="Seg_5244" s="T98">бисер.[NOM]</ta>
            <ta e="T100" id="Seg_5245" s="T99">одежда-PROPR.[NOM]</ta>
            <ta e="T101" id="Seg_5246" s="T100">бисер.[NOM]</ta>
            <ta e="T102" id="Seg_5247" s="T101">шапка-PROPR.[NOM]</ta>
            <ta e="T103" id="Seg_5248" s="T102">богатый</ta>
            <ta e="T104" id="Seg_5249" s="T103">очень</ta>
            <ta e="T105" id="Seg_5250" s="T104">образ-PROPR.[NOM]</ta>
            <ta e="T106" id="Seg_5251" s="T105">старшина.[NOM]</ta>
            <ta e="T107" id="Seg_5252" s="T106">сын-3SG.[NOM]</ta>
            <ta e="T108" id="Seg_5253" s="T107">друг-3SG-ACC</ta>
            <ta e="T109" id="Seg_5254" s="T108">чай-VBZ.[IMP.2SG]</ta>
            <ta e="T110" id="Seg_5255" s="T109">гость-ACC</ta>
            <ta e="T111" id="Seg_5256" s="T110">говорить-PST2.[3SG]</ta>
            <ta e="T112" id="Seg_5257" s="T111">князь.[NOM]</ta>
            <ta e="T113" id="Seg_5258" s="T112">сын-3SG.[NOM]</ta>
            <ta e="T114" id="Seg_5259" s="T113">вставать-CVB.SEQ</ta>
            <ta e="T115" id="Seg_5260" s="T114">очаг.[NOM]</ta>
            <ta e="T116" id="Seg_5261" s="T115">отапливать-EP-MED-PST2.[3SG]</ta>
            <ta e="T117" id="Seg_5262" s="T116">чай.[NOM]</ta>
            <ta e="T118" id="Seg_5263" s="T117">кипеть-EP-CAUS-NEG.[3SG]</ta>
            <ta e="T119" id="Seg_5264" s="T118">только</ta>
            <ta e="T120" id="Seg_5265" s="T119">жена-3PL.[NOM]</ta>
            <ta e="T121" id="Seg_5266" s="T120">лицо-3SG-ACC</ta>
            <ta e="T122" id="Seg_5267" s="T121">показывать-NEG.[3SG]</ta>
            <ta e="T123" id="Seg_5268" s="T122">говорить-RECP/COLL-EP-NEG.[3SG]</ta>
            <ta e="T124" id="Seg_5269" s="T123">этот</ta>
            <ta e="T125" id="Seg_5270" s="T124">мальчик.[NOM]</ta>
            <ta e="T126" id="Seg_5271" s="T125">рыба.[NOM]</ta>
            <ta e="T127" id="Seg_5272" s="T126">котел-3SG.[NOM]</ta>
            <ta e="T128" id="Seg_5273" s="T127">сало-VBZ-PTCP.PST</ta>
            <ta e="T129" id="Seg_5274" s="T128">спичка.[NOM]</ta>
            <ta e="T130" id="Seg_5275" s="T129">дерево-3SG-INSTR</ta>
            <ta e="T131" id="Seg_5276" s="T130">глаз-3SG-ACC</ta>
            <ta e="T132" id="Seg_5277" s="T131">поддерживать-CVB.SEQ</ta>
            <ta e="T133" id="Seg_5278" s="T132">после</ta>
            <ta e="T134" id="Seg_5279" s="T133">видеть-PST2-3SG</ta>
            <ta e="T135" id="Seg_5280" s="T134">жена.[NOM]</ta>
            <ta e="T136" id="Seg_5281" s="T135">смеяться-CVB.SIM</ta>
            <ta e="T137" id="Seg_5282" s="T136">сидеть-PRS.[3SG]</ta>
            <ta e="T138" id="Seg_5283" s="T137">одинокий</ta>
            <ta e="T139" id="Seg_5284" s="T138">только</ta>
            <ta e="T140" id="Seg_5285" s="T139">зуб-PROPR.[NOM]</ta>
            <ta e="T141" id="Seg_5286" s="T140">быть-PST2.[3SG]</ta>
            <ta e="T142" id="Seg_5287" s="T141">дерево.[NOM]</ta>
            <ta e="T143" id="Seg_5288" s="T142">внести-CVB.SIM</ta>
            <ta e="T144" id="Seg_5289" s="T143">выйти-EP-PST2.[3SG]</ta>
            <ta e="T145" id="Seg_5290" s="T144">собака-3PL-ACC</ta>
            <ta e="T146" id="Seg_5291" s="T145">запрячь-FREQ-CVB.SEQ</ta>
            <ta e="T147" id="Seg_5292" s="T146">бросать-PST2.[3SG]</ta>
            <ta e="T148" id="Seg_5293" s="T147">видеть-PST2-3SG</ta>
            <ta e="T149" id="Seg_5294" s="T148">жена.[NOM]</ta>
            <ta e="T150" id="Seg_5295" s="T149">сани-3SG-DAT/LOC</ta>
            <ta e="T151" id="Seg_5296" s="T150">шесть</ta>
            <ta e="T152" id="Seg_5297" s="T151">собака.[NOM]</ta>
            <ta e="T153" id="Seg_5298" s="T152">запрячь-PASS/REFL-EP-PTCP.PST.[NOM]</ta>
            <ta e="T154" id="Seg_5299" s="T153">входить-CVB.SEQ</ta>
            <ta e="T155" id="Seg_5300" s="T154">после</ta>
            <ta e="T156" id="Seg_5301" s="T155">гость-3SG-ACC</ta>
            <ta e="T157" id="Seg_5302" s="T156">есть-EP-CAUS-PST2.[3SG]</ta>
            <ta e="T158" id="Seg_5303" s="T157">лед.[NOM]</ta>
            <ta e="T159" id="Seg_5304" s="T158">вносить-CVB.SIM</ta>
            <ta e="T160" id="Seg_5305" s="T159">выйти-FUT-1SG</ta>
            <ta e="T161" id="Seg_5306" s="T160">говорить-CVB.SEQ</ta>
            <ta e="T162" id="Seg_5307" s="T161">после</ta>
            <ta e="T163" id="Seg_5308" s="T162">одежда-PL-3SG-ACC</ta>
            <ta e="T164" id="Seg_5309" s="T163">брать.с.собой-CVB.SEQ</ta>
            <ta e="T165" id="Seg_5310" s="T164">вынимать-PST2.[3SG]</ta>
            <ta e="T166" id="Seg_5311" s="T165">собака-3SG-GEN</ta>
            <ta e="T167" id="Seg_5312" s="T166">сани-3SG-DAT/LOC</ta>
            <ta e="T168" id="Seg_5313" s="T167">класть-CVB.SEQ</ta>
            <ta e="T169" id="Seg_5314" s="T168">бросать-PST2.[3SG]</ta>
            <ta e="T170" id="Seg_5315" s="T169">потом</ta>
            <ta e="T171" id="Seg_5316" s="T170">входить-PST2.[3SG]</ta>
            <ta e="T172" id="Seg_5317" s="T171">жена-ACC</ta>
            <ta e="T173" id="Seg_5318" s="T172">сидеть-PTCP.PST</ta>
            <ta e="T174" id="Seg_5319" s="T173">место-3SG-DAT/LOC</ta>
            <ta e="T175" id="Seg_5320" s="T174">спать-CAUS-PST2.[3SG]</ta>
            <ta e="T176" id="Seg_5321" s="T175">шкура.[NOM]</ta>
            <ta e="T177" id="Seg_5322" s="T176">давать-PST2.[3SG]</ta>
            <ta e="T178" id="Seg_5323" s="T177">постельное.белье-EP-INSTR</ta>
            <ta e="T179" id="Seg_5324" s="T178">рыба.[NOM]</ta>
            <ta e="T180" id="Seg_5325" s="T179">сало-3SG.[NOM]</ta>
            <ta e="T181" id="Seg_5326" s="T180">светильник-3PL-ACC</ta>
            <ta e="T182" id="Seg_5327" s="T181">гасить-CVB.SEQ</ta>
            <ta e="T183" id="Seg_5328" s="T182">бросать-PST2-3PL</ta>
            <ta e="T184" id="Seg_5329" s="T183">уснуть-PST2-3PL</ta>
            <ta e="T185" id="Seg_5330" s="T184">старшина.[NOM]</ta>
            <ta e="T186" id="Seg_5331" s="T185">сын-3SG.[NOM]</ta>
            <ta e="T187" id="Seg_5332" s="T186">друг-3SG-ACC</ta>
            <ta e="T188" id="Seg_5333" s="T187">говорить-PST2.[3SG]</ta>
            <ta e="T189" id="Seg_5334" s="T188">ну</ta>
            <ta e="T190" id="Seg_5335" s="T189">этот-3SG-1PL.[NOM]</ta>
            <ta e="T191" id="Seg_5336" s="T190">девушка.[NOM]</ta>
            <ta e="T192" id="Seg_5337" s="T191">быть-PST2.NEG.[3SG]</ta>
            <ta e="T193" id="Seg_5338" s="T192">девушка-VBZ-IMP.1DU</ta>
            <ta e="T194" id="Seg_5339" s="T193">что-EP-ACC</ta>
            <ta e="T195" id="Seg_5340" s="T194">испугаться-PRS-2SG</ta>
            <ta e="T196" id="Seg_5341" s="T195">1SG.[NOM]</ta>
            <ta e="T197" id="Seg_5342" s="T196">тогда</ta>
            <ta e="T198" id="Seg_5343" s="T197">идти-FUT-1SG</ta>
            <ta e="T199" id="Seg_5344" s="T198">друг-3SG.[NOM]</ta>
            <ta e="T200" id="Seg_5345" s="T199">1SG.[NOM]</ta>
            <ta e="T201" id="Seg_5346" s="T200">выйти-EP-PTCP.PST-EP-1SG.[NOM]</ta>
            <ta e="T202" id="Seg_5347" s="T201">после.того</ta>
            <ta e="T203" id="Seg_5348" s="T202">девушка-2SG-DAT/LOC</ta>
            <ta e="T204" id="Seg_5349" s="T203">идти-FUT.[IMP.2SG]</ta>
            <ta e="T205" id="Seg_5350" s="T204">выйти-CVB.SEQ</ta>
            <ta e="T206" id="Seg_5351" s="T205">оставаться-PST2.[3SG]</ta>
            <ta e="T207" id="Seg_5352" s="T206">одеваться-CVB.SEQ</ta>
            <ta e="T208" id="Seg_5353" s="T207">после</ta>
            <ta e="T209" id="Seg_5354" s="T208">сани-3SG-DAT/LOC</ta>
            <ta e="T210" id="Seg_5355" s="T209">сесть-PST2.[3SG]</ta>
            <ta e="T211" id="Seg_5356" s="T210">сколько</ta>
            <ta e="T212" id="Seg_5357" s="T211">быть-FUT.[3SG]=Q</ta>
            <ta e="T213" id="Seg_5358" s="T212">умирать-PTCP.PRS-оставаться-PTCP.PRS</ta>
            <ta e="T214" id="Seg_5359" s="T213">крик.[NOM]</ta>
            <ta e="T215" id="Seg_5360" s="T214">слышаться-EP-PST2.[3SG]</ta>
            <ta e="T216" id="Seg_5361" s="T215">вот.беда</ta>
            <ta e="T217" id="Seg_5362" s="T216">выручать.[IMP.2SG]</ta>
            <ta e="T218" id="Seg_5363" s="T217">этот</ta>
            <ta e="T219" id="Seg_5364" s="T218">мальчик.[NOM]</ta>
            <ta e="T220" id="Seg_5365" s="T219">повод-3SG-ACC</ta>
            <ta e="T221" id="Seg_5366" s="T220">взять-PST1-3SG</ta>
            <ta e="T222" id="Seg_5367" s="T221">EMPH</ta>
            <ta e="T223" id="Seg_5368" s="T222">мчаться-CVB.SEQ</ta>
            <ta e="T224" id="Seg_5369" s="T223">оставаться-PST2.[3SG]</ta>
            <ta e="T225" id="Seg_5370" s="T224">задняя.часть-3SG-ABL</ta>
            <ta e="T226" id="Seg_5371" s="T225">жена.[NOM]</ta>
            <ta e="T227" id="Seg_5372" s="T226">голос-3SG.[NOM]</ta>
            <ta e="T228" id="Seg_5373" s="T227">слышаться-EP-PST2.[3SG]</ta>
            <ta e="T229" id="Seg_5374" s="T228">знать-PTCP.PST-EP-1SG.[NOM]</ta>
            <ta e="T230" id="Seg_5375" s="T229">быть-COND.[3SG]</ta>
            <ta e="T231" id="Seg_5376" s="T230">есть-CVB.SEQ</ta>
            <ta e="T232" id="Seg_5377" s="T231">бросать-PTCP.FUT</ta>
            <ta e="T233" id="Seg_5378" s="T232">быть-PST1-1SG</ta>
            <ta e="T234" id="Seg_5379" s="T233">EMPH</ta>
            <ta e="T235" id="Seg_5380" s="T234">приходить-CVB.SEQ</ta>
            <ta e="T236" id="Seg_5381" s="T235">этот</ta>
            <ta e="T237" id="Seg_5382" s="T236">мальчик.[NOM]</ta>
            <ta e="T238" id="Seg_5383" s="T237">отец-3SG-DAT/LOC</ta>
            <ta e="T239" id="Seg_5384" s="T238">князь-DAT/LOC</ta>
            <ta e="T240" id="Seg_5385" s="T239">рассказывать-PST2.[3SG]</ta>
            <ta e="T241" id="Seg_5386" s="T240">князь-ACC</ta>
            <ta e="T242" id="Seg_5387" s="T241">с</ta>
            <ta e="T243" id="Seg_5388" s="T242">старшина.[NOM]</ta>
            <ta e="T244" id="Seg_5389" s="T243">семь</ta>
            <ta e="T245" id="Seg_5390" s="T244">чум-ACC</ta>
            <ta e="T246" id="Seg_5391" s="T245">собирать-CVB.SEQ</ta>
            <ta e="T247" id="Seg_5392" s="T246">сход-VBZ-PST2-3PL</ta>
            <ta e="T248" id="Seg_5393" s="T247">два</ta>
            <ta e="T249" id="Seg_5394" s="T248">мальчик-ACC</ta>
            <ta e="T250" id="Seg_5395" s="T249">послать-PST2-3PL</ta>
            <ta e="T251" id="Seg_5396" s="T250">видеть-PTCP.FUT-3PL-ACC</ta>
            <ta e="T252" id="Seg_5397" s="T251">два</ta>
            <ta e="T253" id="Seg_5398" s="T252">мальчик.[NOM]</ta>
            <ta e="T254" id="Seg_5399" s="T253">быть-CVB.SEQ</ta>
            <ta e="T255" id="Seg_5400" s="T254">идти-CVB.SEQ</ta>
            <ta e="T256" id="Seg_5401" s="T255">оставаться-PST1-3PL</ta>
            <ta e="T257" id="Seg_5402" s="T256">долго</ta>
            <ta e="T258" id="Seg_5403" s="T257">быть-FUT-3PL=Q</ta>
            <ta e="T259" id="Seg_5404" s="T258">луч-DAT/LOC</ta>
            <ta e="T260" id="Seg_5405" s="T259">приходить-PST2-3PL</ta>
            <ta e="T261" id="Seg_5406" s="T260">видеть-PST2-3PL</ta>
            <ta e="T262" id="Seg_5407" s="T261">один</ta>
            <ta e="T263" id="Seg_5408" s="T262">NEG</ta>
            <ta e="T264" id="Seg_5409" s="T263">человек.[NOM]</ta>
            <ta e="T265" id="Seg_5410" s="T264">NEG.EX</ta>
            <ta e="T266" id="Seg_5411" s="T265">след.[NOM]</ta>
            <ta e="T267" id="Seg_5412" s="T266">NEG</ta>
            <ta e="T268" id="Seg_5413" s="T267">NEG.EX</ta>
            <ta e="T269" id="Seg_5414" s="T268">шест.[NOM]</ta>
            <ta e="T270" id="Seg_5415" s="T269">чум-DAT/LOC</ta>
            <ta e="T271" id="Seg_5416" s="T270">входить-PST2-3PL</ta>
            <ta e="T272" id="Seg_5417" s="T271">мальчик.[NOM]</ta>
            <ta e="T273" id="Seg_5418" s="T272">кость-3SG.[NOM]</ta>
            <ta e="T274" id="Seg_5419" s="T273">только</ta>
            <ta e="T275" id="Seg_5420" s="T274">быть.очень.костлявым-CVB.SIM</ta>
            <ta e="T276" id="Seg_5421" s="T275">лежать-PRS.[3SG]</ta>
            <ta e="T277" id="Seg_5422" s="T276">этот</ta>
            <ta e="T278" id="Seg_5423" s="T277">два</ta>
            <ta e="T279" id="Seg_5424" s="T278">мальчик.[NOM]</ta>
            <ta e="T280" id="Seg_5425" s="T279">возвращаться-EP-PST2-3PL</ta>
            <ta e="T281" id="Seg_5426" s="T280">князь-DAT/LOC</ta>
            <ta e="T282" id="Seg_5427" s="T281">рассказывать-PST2-3PL</ta>
            <ta e="T283" id="Seg_5428" s="T282">один</ta>
            <ta e="T284" id="Seg_5429" s="T283">сирота.[NOM]</ta>
            <ta e="T285" id="Seg_5430" s="T284">ребенок.[NOM]</ta>
            <ta e="T286" id="Seg_5431" s="T285">есть</ta>
            <ta e="T287" id="Seg_5432" s="T286">два</ta>
            <ta e="T288" id="Seg_5433" s="T287">плечо-ABL</ta>
            <ta e="T289" id="Seg_5434" s="T288">плешивый.[NOM]</ta>
            <ta e="T290" id="Seg_5435" s="T289">одинокий</ta>
            <ta e="T291" id="Seg_5436" s="T290">мать-PROPR.[NOM]</ta>
            <ta e="T292" id="Seg_5437" s="T291">один</ta>
            <ta e="T293" id="Seg_5438" s="T292">олень-PROPR.[NOM]</ta>
            <ta e="T294" id="Seg_5439" s="T293">князь-ACC</ta>
            <ta e="T295" id="Seg_5440" s="T294">с</ta>
            <ta e="T296" id="Seg_5441" s="T295">старшина.[NOM]</ta>
            <ta e="T297" id="Seg_5442" s="T296">сход-VBZ-PST2-3PL</ta>
            <ta e="T298" id="Seg_5443" s="T297">как</ta>
            <ta e="T299" id="Seg_5444" s="T298">быть-FUT-1PL=Q</ta>
            <ta e="T300" id="Seg_5445" s="T299">земля-1PL-ACC</ta>
            <ta e="T301" id="Seg_5446" s="T300">бросать-FUT-1PL</ta>
            <ta e="T302" id="Seg_5447" s="T301">Q</ta>
            <ta e="T303" id="Seg_5448" s="T302">тоже</ta>
            <ta e="T304" id="Seg_5449" s="T303">да</ta>
            <ta e="T305" id="Seg_5450" s="T304">пасть-PL-1PL.[NOM]</ta>
            <ta e="T307" id="Seg_5451" s="T305">есть-3PL</ta>
            <ta e="T308" id="Seg_5452" s="T307">говорить-EP-RECP/COLL-PST2-3PL</ta>
            <ta e="T309" id="Seg_5453" s="T308">старшина.[NOM]</ta>
            <ta e="T310" id="Seg_5454" s="T309">там</ta>
            <ta e="T311" id="Seg_5455" s="T310">стоить-CVB.SEQ</ta>
            <ta e="T312" id="Seg_5456" s="T311">говорить-PST2.[3SG]</ta>
            <ta e="T313" id="Seg_5457" s="T312">сирота.[NOM]</ta>
            <ta e="T314" id="Seg_5458" s="T313">ребенок-ACC</ta>
            <ta e="T315" id="Seg_5459" s="T314">послать-PTCP.FUT-DAT/LOC</ta>
            <ta e="T316" id="Seg_5460" s="T315">1SG.[NOM]</ta>
            <ta e="T317" id="Seg_5461" s="T316">дочь-1SG-ACC</ta>
            <ta e="T318" id="Seg_5462" s="T317">да</ta>
            <ta e="T319" id="Seg_5463" s="T318">давать-PTCP.FUT-1SG</ta>
            <ta e="T320" id="Seg_5464" s="T319">быть-PST1-3SG</ta>
            <ta e="T321" id="Seg_5465" s="T320">убить-TEMP-3SG</ta>
            <ta e="T322" id="Seg_5466" s="T321">этот</ta>
            <ta e="T323" id="Seg_5467" s="T322">мальчик-ACC</ta>
            <ta e="T324" id="Seg_5468" s="T323">звать-PST2-3PL</ta>
            <ta e="T325" id="Seg_5469" s="T324">рассказывать-PST2-3PL</ta>
            <ta e="T326" id="Seg_5470" s="T325">каждый-3SG-ACC</ta>
            <ta e="T327" id="Seg_5471" s="T326">вот</ta>
            <ta e="T328" id="Seg_5472" s="T327">INTJ</ta>
            <ta e="T329" id="Seg_5473" s="T328">быть-PST2.[3SG]</ta>
            <ta e="T330" id="Seg_5474" s="T329">ребенок.[NOM]</ta>
            <ta e="T331" id="Seg_5475" s="T330">беда-PROPR</ta>
            <ta e="T332" id="Seg_5476" s="T331">одиноко</ta>
            <ta e="T333" id="Seg_5477" s="T332">идти-PTCP.PRS</ta>
            <ta e="T334" id="Seg_5478" s="T333">быть-PST1-3SG</ta>
            <ta e="T335" id="Seg_5479" s="T334">собака.[NOM]</ta>
            <ta e="T336" id="Seg_5480" s="T335">только</ta>
            <ta e="T337" id="Seg_5481" s="T336">давать-PST2-3PL</ta>
            <ta e="T338" id="Seg_5482" s="T337">ружье.[NOM]</ta>
            <ta e="T339" id="Seg_5483" s="T338">NEG</ta>
            <ta e="T340" id="Seg_5484" s="T339">что.[NOM]</ta>
            <ta e="T341" id="Seg_5485" s="T340">NEG</ta>
            <ta e="T342" id="Seg_5486" s="T341">NEG.EX</ta>
            <ta e="T343" id="Seg_5487" s="T342">дом-3SG-DAT/LOC</ta>
            <ta e="T344" id="Seg_5488" s="T343">приходить-CVB.SEQ</ta>
            <ta e="T345" id="Seg_5489" s="T344">плакать-EP-MED-CVB.SIM</ta>
            <ta e="T346" id="Seg_5490" s="T345">сидеть-PST2.[3SG]</ta>
            <ta e="T347" id="Seg_5491" s="T346">мать-3SG.[NOM]</ta>
            <ta e="T348" id="Seg_5492" s="T347">что.[NOM]</ta>
            <ta e="T349" id="Seg_5493" s="T348">быть-PST1-2SG</ta>
            <ta e="T350" id="Seg_5494" s="T349">рассказывать-PST2.[3SG]</ta>
            <ta e="T351" id="Seg_5495" s="T350">очевидно</ta>
            <ta e="T352" id="Seg_5496" s="T351">отец-2SG.[NOM]</ta>
            <ta e="T353" id="Seg_5497" s="T352">пальма-3SG.[NOM]</ta>
            <ta e="T354" id="Seg_5498" s="T353">где</ta>
            <ta e="T355" id="Seg_5499" s="T354">INDEF</ta>
            <ta e="T356" id="Seg_5500" s="T355">есть</ta>
            <ta e="T357" id="Seg_5501" s="T356">быть-PST1-3SG</ta>
            <ta e="T358" id="Seg_5502" s="T357">тот-ACC</ta>
            <ta e="T359" id="Seg_5503" s="T358">носить-FUT.[IMP.2SG]</ta>
            <ta e="T360" id="Seg_5504" s="T359">мать-3SG.[NOM]</ta>
            <ta e="T361" id="Seg_5505" s="T360">беда-PROPR</ta>
            <ta e="T362" id="Seg_5506" s="T361">сплошной</ta>
            <ta e="T363" id="Seg_5507" s="T362">заржаветь-PTCP.PST</ta>
            <ta e="T364" id="Seg_5508" s="T363">пальма-ACC</ta>
            <ta e="T365" id="Seg_5509" s="T364">внести-PST2.[3SG]</ta>
            <ta e="T366" id="Seg_5510" s="T365">этот</ta>
            <ta e="T367" id="Seg_5511" s="T366">мальчик.[NOM]</ta>
            <ta e="T368" id="Seg_5512" s="T367">долго</ta>
            <ta e="T369" id="Seg_5513" s="T368">быть-NEG.CVB.SIM</ta>
            <ta e="T370" id="Seg_5514" s="T369">идти-CVB.SEQ</ta>
            <ta e="T371" id="Seg_5515" s="T370">оставаться-PST2.[3SG]</ta>
            <ta e="T372" id="Seg_5516" s="T371">кончаться-FUT.[3SG]</ta>
            <ta e="T373" id="Seg_5517" s="T372">Q</ta>
            <ta e="T374" id="Seg_5518" s="T373">приходить-PST2-3SG</ta>
            <ta e="T375" id="Seg_5519" s="T374">кто.[NOM]</ta>
            <ta e="T376" id="Seg_5520" s="T375">NEG</ta>
            <ta e="T377" id="Seg_5521" s="T376">NEG.EX</ta>
            <ta e="T378" id="Seg_5522" s="T377">человек.[NOM]</ta>
            <ta e="T379" id="Seg_5523" s="T378">только</ta>
            <ta e="T380" id="Seg_5524" s="T379">кость-3SG.[NOM]</ta>
            <ta e="T381" id="Seg_5525" s="T380">чум-DAT/LOC</ta>
            <ta e="T382" id="Seg_5526" s="T381">сумерки.[NOM]</ta>
            <ta e="T383" id="Seg_5527" s="T382">падать-PTCP.PST-3SG-ACC</ta>
            <ta e="T384" id="Seg_5528" s="T383">после.того</ta>
            <ta e="T385" id="Seg_5529" s="T384">светильник-3SG-ACC</ta>
            <ta e="T386" id="Seg_5530" s="T385">зажигать-PST2.[3SG]</ta>
            <ta e="T387" id="Seg_5531" s="T386">человек.[NOM]</ta>
            <ta e="T388" id="Seg_5532" s="T387">спать-PTCP.PRS</ta>
            <ta e="T389" id="Seg_5533" s="T388">час-3SG-ACC</ta>
            <ta e="T390" id="Seg_5534" s="T389">к</ta>
            <ta e="T391" id="Seg_5535" s="T390">сани-PROPR</ta>
            <ta e="T392" id="Seg_5536" s="T391">звук-3SG.[NOM]</ta>
            <ta e="T393" id="Seg_5537" s="T392">слышаться-EP-PST2.[3SG]</ta>
            <ta e="T394" id="Seg_5538" s="T393">собака.[NOM]</ta>
            <ta e="T395" id="Seg_5539" s="T394">да</ta>
            <ta e="T396" id="Seg_5540" s="T395">лаять-PST2.NEG.[3SG]</ta>
            <ta e="T397" id="Seg_5541" s="T396">огонь-3SG-ACC</ta>
            <ta e="T398" id="Seg_5542" s="T397">гасить-CVB.SEQ</ta>
            <ta e="T399" id="Seg_5543" s="T398">после</ta>
            <ta e="T400" id="Seg_5544" s="T399">сидеть-PST2.[3SG]</ta>
            <ta e="T401" id="Seg_5545" s="T400">звук.[NOM]</ta>
            <ta e="T402" id="Seg_5546" s="T401">крепкий.[NOM]</ta>
            <ta e="T403" id="Seg_5547" s="T402">становиться-PST2.[3SG]</ta>
            <ta e="T404" id="Seg_5548" s="T403">вход-DAT/LOC</ta>
            <ta e="T405" id="Seg_5549" s="T404">подходить-NEG.PTCP</ta>
            <ta e="T406" id="Seg_5550" s="T405">толстый</ta>
            <ta e="T407" id="Seg_5551" s="T406">жена.[NOM]</ta>
            <ta e="T408" id="Seg_5552" s="T407">входить-PST2.[3SG]</ta>
            <ta e="T409" id="Seg_5553" s="T408">другой.из.двух</ta>
            <ta e="T410" id="Seg_5554" s="T409">нога-3SG-ACC</ta>
            <ta e="T411" id="Seg_5555" s="T410">дверь-DAT/LOC</ta>
            <ta e="T412" id="Seg_5556" s="T411">толкать-PTCP.PRS-3SG-ACC</ta>
            <ta e="T413" id="Seg_5557" s="T412">с</ta>
            <ta e="T414" id="Seg_5558" s="T413">два</ta>
            <ta e="T415" id="Seg_5559" s="T414">подмышка-3SG-GEN</ta>
            <ta e="T416" id="Seg_5560" s="T415">нижняя.часть-3SG-INSTR</ta>
            <ta e="T417" id="Seg_5561" s="T416">пальма-INSTR</ta>
            <ta e="T418" id="Seg_5562" s="T417">внутрь</ta>
            <ta e="T419" id="Seg_5563" s="T418">толкать-PST2.[3SG]</ta>
            <ta e="T420" id="Seg_5564" s="T419">сам-3SG.[NOM]</ta>
            <ta e="T421" id="Seg_5565" s="T420">подмышка-3SG-GEN</ta>
            <ta e="T422" id="Seg_5566" s="T421">нижняя.часть-3SG-INSTR</ta>
            <ta e="T423" id="Seg_5567" s="T422">спасаться-PST2.[3SG]</ta>
            <ta e="T424" id="Seg_5568" s="T423">ночью</ta>
            <ta e="T425" id="Seg_5569" s="T424">дом-3SG-DAT/LOC</ta>
            <ta e="T426" id="Seg_5570" s="T425">приходить-PST2.[3SG]</ta>
            <ta e="T427" id="Seg_5571" s="T426">мать-3SG.[NOM]</ta>
            <ta e="T428" id="Seg_5572" s="T427">беда-PROPR</ta>
            <ta e="T429" id="Seg_5573" s="T428">спать-NEG.CVB.SIM</ta>
            <ta e="T430" id="Seg_5574" s="T429">ждать-CVB.SEQ</ta>
            <ta e="T431" id="Seg_5575" s="T430">утром</ta>
            <ta e="T432" id="Seg_5576" s="T431">мать-3SG.[NOM]</ta>
            <ta e="T433" id="Seg_5577" s="T432">князь-DAT/LOC</ta>
            <ta e="T434" id="Seg_5578" s="T433">идти-CVB.SEQ</ta>
            <ta e="T435" id="Seg_5579" s="T434">рассказывать-PST2.[3SG]</ta>
            <ta e="T436" id="Seg_5580" s="T435">князь.[NOM]</ta>
            <ta e="T437" id="Seg_5581" s="T436">этот</ta>
            <ta e="T438" id="Seg_5582" s="T437">мальчик-ACC</ta>
            <ta e="T439" id="Seg_5583" s="T438">звать-PST2.[3SG]</ta>
            <ta e="T440" id="Seg_5584" s="T439">вот</ta>
            <ta e="T441" id="Seg_5585" s="T440">как=Q</ta>
            <ta e="T442" id="Seg_5586" s="T441">убить-PST1-2SG</ta>
            <ta e="T443" id="Seg_5587" s="T442">Q</ta>
            <ta e="T444" id="Seg_5588" s="T443">знать-NEG-1SG</ta>
            <ta e="T445" id="Seg_5589" s="T444">говорить-PST2.[3SG]</ta>
            <ta e="T446" id="Seg_5590" s="T445">мальчик.[NOM]</ta>
            <ta e="T447" id="Seg_5591" s="T446">рассказывать-PST2.[3SG]</ta>
            <ta e="T448" id="Seg_5592" s="T447">как</ta>
            <ta e="T449" id="Seg_5593" s="T448">делать-PTCP.PST-3SG-ACC</ta>
            <ta e="T450" id="Seg_5594" s="T449">сход-VBZ-PST2-3PL</ta>
            <ta e="T451" id="Seg_5595" s="T450">шесть</ta>
            <ta e="T452" id="Seg_5596" s="T451">человек-ACC</ta>
            <ta e="T453" id="Seg_5597" s="T452">видеть-CAUS-CVB.SIM</ta>
            <ta e="T454" id="Seg_5598" s="T453">послать-PST2-3PL</ta>
            <ta e="T455" id="Seg_5599" s="T454">старшина.[NOM]</ta>
            <ta e="T456" id="Seg_5600" s="T455">тоже</ta>
            <ta e="T457" id="Seg_5601" s="T456">сопровождать-EP-PST2.[3SG]</ta>
            <ta e="T458" id="Seg_5602" s="T457">олень-INSTR</ta>
            <ta e="T459" id="Seg_5603" s="T458">с</ta>
            <ta e="T460" id="Seg_5604" s="T459">лошадь-EP-INSTR</ta>
            <ta e="T461" id="Seg_5605" s="T460">идти-PST2-3PL</ta>
            <ta e="T462" id="Seg_5606" s="T461">доезжать-PST2-3PL</ta>
            <ta e="T463" id="Seg_5607" s="T462">днем</ta>
            <ta e="T464" id="Seg_5608" s="T463">еще</ta>
            <ta e="T465" id="Seg_5609" s="T464">один</ta>
            <ta e="T466" id="Seg_5610" s="T465">NEG</ta>
            <ta e="T467" id="Seg_5611" s="T466">человек.[NOM]</ta>
            <ta e="T468" id="Seg_5612" s="T467">NEG.EX</ta>
            <ta e="T469" id="Seg_5613" s="T468">чум-3PL-GEN</ta>
            <ta e="T470" id="Seg_5614" s="T469">потолок-3SG.[NOM]</ta>
            <ta e="T471" id="Seg_5615" s="T470">разорвать-EP-PTCP.PST.[NOM]</ta>
            <ta e="T472" id="Seg_5616" s="T471">вход-3SG.[NOM]</ta>
            <ta e="T473" id="Seg_5617" s="T472">открывать-PASS/REFL-CVB.SEQ</ta>
            <ta e="T474" id="Seg_5618" s="T473">стоять-PRS.[3SG]</ta>
            <ta e="T475" id="Seg_5619" s="T474">чум.[NOM]</ta>
            <ta e="T476" id="Seg_5620" s="T475">нутро-3SG-ACC</ta>
            <ta e="T477" id="Seg_5621" s="T476">видеть-PST2-3PL</ta>
            <ta e="T478" id="Seg_5622" s="T477">девушка.[NOM]</ta>
            <ta e="T479" id="Seg_5623" s="T478">живот-3SG.[NOM]</ta>
            <ta e="T480" id="Seg_5624" s="T479">вздуть-CVB.SEQ</ta>
            <ta e="T481" id="Seg_5625" s="T480">идти-CVB.SEQ</ta>
            <ta e="T482" id="Seg_5626" s="T481">лежать-PRS.[3SG]</ta>
            <ta e="T483" id="Seg_5627" s="T482">старшина.[NOM]</ta>
            <ta e="T484" id="Seg_5628" s="T483">чум-ACC</ta>
            <ta e="T485" id="Seg_5629" s="T484">зажигать-IMP.1PL</ta>
            <ta e="T486" id="Seg_5630" s="T485">этот</ta>
            <ta e="T487" id="Seg_5631" s="T486">чум-ACC</ta>
            <ta e="T488" id="Seg_5632" s="T487">девушка-COM</ta>
            <ta e="T489" id="Seg_5633" s="T488">зажигать-CVB.SEQ</ta>
            <ta e="T490" id="Seg_5634" s="T489">бросать-PST2-3PL</ta>
            <ta e="T491" id="Seg_5635" s="T490">плешивый</ta>
            <ta e="T492" id="Seg_5636" s="T491">мальчик.[NOM]</ta>
            <ta e="T493" id="Seg_5637" s="T492">старшина.[NOM]</ta>
            <ta e="T494" id="Seg_5638" s="T493">дочь-3SG-ACC</ta>
            <ta e="T495" id="Seg_5639" s="T494">взять-CVB.SEQ</ta>
            <ta e="T496" id="Seg_5640" s="T495">быть.богатым-CVB.SEQ-наесться-CVB.SEQ</ta>
            <ta e="T497" id="Seg_5641" s="T496">жить-PST2.[3SG]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_5642" s="T0">n-n:case</ta>
            <ta e="T2" id="Seg_5643" s="T1">post</ta>
            <ta e="T3" id="Seg_5644" s="T2">n-n:case</ta>
            <ta e="T4" id="Seg_5645" s="T3">cardnum-cardnum&gt;distrnum</ta>
            <ta e="T5" id="Seg_5646" s="T4">n-n&gt;adj-n:(pred.pn)</ta>
            <ta e="T6" id="Seg_5647" s="T5">v-v:tense-v:pred.pn</ta>
            <ta e="T7" id="Seg_5648" s="T6">v-v:tense-v:pred.pn</ta>
            <ta e="T8" id="Seg_5649" s="T7">cardnum-cardnum&gt;collnum-n:(poss)-n:case</ta>
            <ta e="T9" id="Seg_5650" s="T8">adj</ta>
            <ta e="T10" id="Seg_5651" s="T9">n-n:(poss)-n:case</ta>
            <ta e="T11" id="Seg_5652" s="T10">n-n:(num)-n:case</ta>
            <ta e="T12" id="Seg_5653" s="T11">adv</ta>
            <ta e="T13" id="Seg_5654" s="T12">v-v:cvb</ta>
            <ta e="T14" id="Seg_5655" s="T13">n-n&gt;adv</ta>
            <ta e="T15" id="Seg_5656" s="T14">ptcl</ta>
            <ta e="T16" id="Seg_5657" s="T15">v-v:tense-v:poss.pn</ta>
            <ta e="T17" id="Seg_5658" s="T16">dempro</ta>
            <ta e="T18" id="Seg_5659" s="T17">n-n:(num)-n:case</ta>
            <ta e="T19" id="Seg_5660" s="T18">n-n:case</ta>
            <ta e="T20" id="Seg_5661" s="T19">v-v:cvb</ta>
            <ta e="T21" id="Seg_5662" s="T20">v-v:ptcp</ta>
            <ta e="T22" id="Seg_5663" s="T21">v-v:tense-v:poss.pn</ta>
            <ta e="T23" id="Seg_5664" s="T22">ptcl</ta>
            <ta e="T24" id="Seg_5665" s="T23">adv</ta>
            <ta e="T25" id="Seg_5666" s="T24">n-n:case</ta>
            <ta e="T26" id="Seg_5667" s="T25">v-v:ptcp</ta>
            <ta e="T27" id="Seg_5668" s="T26">n-n:case</ta>
            <ta e="T28" id="Seg_5669" s="T27">v-v:tense-v:pred.pn</ta>
            <ta e="T29" id="Seg_5670" s="T28">n-n:poss-n:case</ta>
            <ta e="T30" id="Seg_5671" s="T29">adv</ta>
            <ta e="T31" id="Seg_5672" s="T30">adj-adj&gt;v-v:cvb</ta>
            <ta e="T32" id="Seg_5673" s="T31">v-v:tense-v:pred.pn</ta>
            <ta e="T33" id="Seg_5674" s="T32">cardnum</ta>
            <ta e="T34" id="Seg_5675" s="T33">n-n:case</ta>
            <ta e="T35" id="Seg_5676" s="T34">n-n:poss-n:case</ta>
            <ta e="T36" id="Seg_5677" s="T35">n-n&gt;v-v:cvb</ta>
            <ta e="T37" id="Seg_5678" s="T36">v-v:tense-v:pred.pn</ta>
            <ta e="T38" id="Seg_5679" s="T37">n-n:case</ta>
            <ta e="T39" id="Seg_5680" s="T38">v-v&gt;v-v:cvb-v-v&gt;v-v:cvb</ta>
            <ta e="T40" id="Seg_5681" s="T39">post</ta>
            <ta e="T41" id="Seg_5682" s="T40">n-n:poss-n:case</ta>
            <ta e="T42" id="Seg_5683" s="T41">v-v:tense-v:pred.pn</ta>
            <ta e="T43" id="Seg_5684" s="T42">adv</ta>
            <ta e="T44" id="Seg_5685" s="T43">adv</ta>
            <ta e="T45" id="Seg_5686" s="T44">n-n:poss-n:case</ta>
            <ta e="T46" id="Seg_5687" s="T45">v-v:cvb</ta>
            <ta e="T47" id="Seg_5688" s="T46">post</ta>
            <ta e="T48" id="Seg_5689" s="T47">n-n:case</ta>
            <ta e="T49" id="Seg_5690" s="T48">n-n:poss-n:case</ta>
            <ta e="T50" id="Seg_5691" s="T49">n-n:case</ta>
            <ta e="T51" id="Seg_5692" s="T50">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T52" id="Seg_5693" s="T51">n-n:case</ta>
            <ta e="T53" id="Seg_5694" s="T52">v-v:cvb</ta>
            <ta e="T54" id="Seg_5695" s="T53">v-v:tense-v:pred.pn</ta>
            <ta e="T55" id="Seg_5696" s="T54">adv</ta>
            <ta e="T56" id="Seg_5697" s="T55">n-n&gt;adj</ta>
            <ta e="T57" id="Seg_5698" s="T56">n-n:(poss)-n:case</ta>
            <ta e="T58" id="Seg_5699" s="T57">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T59" id="Seg_5700" s="T58">n-n:case</ta>
            <ta e="T60" id="Seg_5701" s="T59">n-n:(poss)-n:case</ta>
            <ta e="T61" id="Seg_5702" s="T60">v-v:tense-v:pred.pn</ta>
            <ta e="T62" id="Seg_5703" s="T61">adj</ta>
            <ta e="T63" id="Seg_5704" s="T62">n-n:case</ta>
            <ta e="T64" id="Seg_5705" s="T63">v-v:(neg)-v:tense-v:poss.pn</ta>
            <ta e="T65" id="Seg_5706" s="T64">adv</ta>
            <ta e="T66" id="Seg_5707" s="T65">ptcl</ta>
            <ta e="T67" id="Seg_5708" s="T66">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T68" id="Seg_5709" s="T67">n-n&gt;v-v:cvb</ta>
            <ta e="T69" id="Seg_5710" s="T68">v-v:tense-v:pred.pn</ta>
            <ta e="T70" id="Seg_5711" s="T69">n-n:(poss)-n:case</ta>
            <ta e="T71" id="Seg_5712" s="T70">v-v:cvb</ta>
            <ta e="T72" id="Seg_5713" s="T71">v-v:tense-v:pred.pn</ta>
            <ta e="T73" id="Seg_5714" s="T72">v-v:(ins)-v&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T74" id="Seg_5715" s="T73">n-n:(poss)-n:case</ta>
            <ta e="T75" id="Seg_5716" s="T74">v-v:ptcp</ta>
            <ta e="T76" id="Seg_5717" s="T75">v-v:tense-v:pred.pn</ta>
            <ta e="T77" id="Seg_5718" s="T76">v-v:ptcp</ta>
            <ta e="T78" id="Seg_5719" s="T77">n-n:case</ta>
            <ta e="T79" id="Seg_5720" s="T78">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T80" id="Seg_5721" s="T79">n-n:case</ta>
            <ta e="T81" id="Seg_5722" s="T80">n-n:(poss)-n:case</ta>
            <ta e="T82" id="Seg_5723" s="T81">v-v:tense-v:pred.pn</ta>
            <ta e="T83" id="Seg_5724" s="T82">v-v:tense-v:pred.pn</ta>
            <ta e="T84" id="Seg_5725" s="T83">n-n:(poss)-n:case</ta>
            <ta e="T85" id="Seg_5726" s="T84">dempro-pro:case</ta>
            <ta e="T86" id="Seg_5727" s="T85">post</ta>
            <ta e="T87" id="Seg_5728" s="T86">v-v:cvb</ta>
            <ta e="T88" id="Seg_5729" s="T87">v-v:tense-v:pred.pn</ta>
            <ta e="T89" id="Seg_5730" s="T88">n-n:case</ta>
            <ta e="T90" id="Seg_5731" s="T89">v-v&gt;v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T91" id="Seg_5732" s="T90">conj</ta>
            <ta e="T92" id="Seg_5733" s="T91">adj</ta>
            <ta e="T93" id="Seg_5734" s="T92">ptcl</ta>
            <ta e="T94" id="Seg_5735" s="T93">n-n:case</ta>
            <ta e="T95" id="Seg_5736" s="T94">adv</ta>
            <ta e="T96" id="Seg_5737" s="T95">v-v:cvb</ta>
            <ta e="T97" id="Seg_5738" s="T96">v-v:tense-v:pred.pn</ta>
            <ta e="T98" id="Seg_5739" s="T97">adj</ta>
            <ta e="T99" id="Seg_5740" s="T98">n-n:case</ta>
            <ta e="T100" id="Seg_5741" s="T99">n-n&gt;adj-n:case</ta>
            <ta e="T101" id="Seg_5742" s="T100">n-n:case</ta>
            <ta e="T102" id="Seg_5743" s="T101">n-n&gt;adj-n:case</ta>
            <ta e="T103" id="Seg_5744" s="T102">adj</ta>
            <ta e="T104" id="Seg_5745" s="T103">ptcl</ta>
            <ta e="T105" id="Seg_5746" s="T104">n-n&gt;adj-n:case</ta>
            <ta e="T106" id="Seg_5747" s="T105">n-n:case</ta>
            <ta e="T107" id="Seg_5748" s="T106">n-n:(poss)-n:case</ta>
            <ta e="T108" id="Seg_5749" s="T107">n-n:poss-n:case</ta>
            <ta e="T109" id="Seg_5750" s="T108">n-n&gt;v-v:mood.pn</ta>
            <ta e="T110" id="Seg_5751" s="T109">n-n:case</ta>
            <ta e="T111" id="Seg_5752" s="T110">v-v:tense-v:pred.pn</ta>
            <ta e="T112" id="Seg_5753" s="T111">n-n:case</ta>
            <ta e="T113" id="Seg_5754" s="T112">n-n:(poss)-n:case</ta>
            <ta e="T114" id="Seg_5755" s="T113">v-v:cvb</ta>
            <ta e="T115" id="Seg_5756" s="T114">n-n:case</ta>
            <ta e="T116" id="Seg_5757" s="T115">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T117" id="Seg_5758" s="T116">n-n:case</ta>
            <ta e="T118" id="Seg_5759" s="T117">v-v:(ins)-v&gt;v-v:(neg)-v:pred.pn</ta>
            <ta e="T119" id="Seg_5760" s="T118">ptcl</ta>
            <ta e="T120" id="Seg_5761" s="T119">n-n:(poss)-n:case</ta>
            <ta e="T121" id="Seg_5762" s="T120">n-n:poss-n:case</ta>
            <ta e="T122" id="Seg_5763" s="T121">v-v:(neg)-v:pred.pn</ta>
            <ta e="T123" id="Seg_5764" s="T122">v-v&gt;v-v:(ins)-v:(neg)-v:pred.pn</ta>
            <ta e="T124" id="Seg_5765" s="T123">dempro</ta>
            <ta e="T125" id="Seg_5766" s="T124">n-n:case</ta>
            <ta e="T126" id="Seg_5767" s="T125">n-n:case</ta>
            <ta e="T127" id="Seg_5768" s="T126">n-n:(poss)-n:case</ta>
            <ta e="T128" id="Seg_5769" s="T127">n-n&gt;v-v:ptcp</ta>
            <ta e="T129" id="Seg_5770" s="T128">n-n:case</ta>
            <ta e="T130" id="Seg_5771" s="T129">n-n:poss-n:case</ta>
            <ta e="T131" id="Seg_5772" s="T130">n-n:poss-n:case</ta>
            <ta e="T132" id="Seg_5773" s="T131">v-v:cvb</ta>
            <ta e="T133" id="Seg_5774" s="T132">post</ta>
            <ta e="T134" id="Seg_5775" s="T133">v-v:tense-v:poss.pn</ta>
            <ta e="T135" id="Seg_5776" s="T134">n-n:case</ta>
            <ta e="T136" id="Seg_5777" s="T135">v-v:cvb</ta>
            <ta e="T137" id="Seg_5778" s="T136">v-v:tense-v:pred.pn</ta>
            <ta e="T138" id="Seg_5779" s="T137">adj</ta>
            <ta e="T139" id="Seg_5780" s="T138">ptcl</ta>
            <ta e="T140" id="Seg_5781" s="T139">n-n&gt;adj-n:case</ta>
            <ta e="T141" id="Seg_5782" s="T140">v-v:tense-v:pred.pn</ta>
            <ta e="T142" id="Seg_5783" s="T141">n-n:case</ta>
            <ta e="T143" id="Seg_5784" s="T142">v-v:cvb</ta>
            <ta e="T144" id="Seg_5785" s="T143">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T145" id="Seg_5786" s="T144">n-n:poss-n:case</ta>
            <ta e="T146" id="Seg_5787" s="T145">v-v&gt;v-v:cvb</ta>
            <ta e="T147" id="Seg_5788" s="T146">v-v:tense-v:pred.pn</ta>
            <ta e="T148" id="Seg_5789" s="T147">v-v:tense-v:poss.pn</ta>
            <ta e="T149" id="Seg_5790" s="T148">n-n:case</ta>
            <ta e="T150" id="Seg_5791" s="T149">n-n:poss-n:case</ta>
            <ta e="T151" id="Seg_5792" s="T150">cardnum</ta>
            <ta e="T152" id="Seg_5793" s="T151">n-n:case</ta>
            <ta e="T153" id="Seg_5794" s="T152">v-v&gt;v-v:(ins)-v:ptcp-v:(case)</ta>
            <ta e="T154" id="Seg_5795" s="T153">v-v:cvb</ta>
            <ta e="T155" id="Seg_5796" s="T154">post</ta>
            <ta e="T156" id="Seg_5797" s="T155">n-n:poss-n:case</ta>
            <ta e="T157" id="Seg_5798" s="T156">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T158" id="Seg_5799" s="T157">n-n:case</ta>
            <ta e="T159" id="Seg_5800" s="T158">v-v:cvb</ta>
            <ta e="T160" id="Seg_5801" s="T159">v-v:tense-v:poss.pn</ta>
            <ta e="T161" id="Seg_5802" s="T160">v-v:cvb</ta>
            <ta e="T162" id="Seg_5803" s="T161">post</ta>
            <ta e="T163" id="Seg_5804" s="T162">n-n:(num)-n:poss-n:case</ta>
            <ta e="T164" id="Seg_5805" s="T163">v-v:cvb</ta>
            <ta e="T165" id="Seg_5806" s="T164">v-v:tense-v:pred.pn</ta>
            <ta e="T166" id="Seg_5807" s="T165">n-n:poss-n:case</ta>
            <ta e="T167" id="Seg_5808" s="T166">n-n:poss-n:case</ta>
            <ta e="T168" id="Seg_5809" s="T167">v-v:cvb</ta>
            <ta e="T169" id="Seg_5810" s="T168">v-v:tense-v:pred.pn</ta>
            <ta e="T170" id="Seg_5811" s="T169">adv</ta>
            <ta e="T171" id="Seg_5812" s="T170">v-v:tense-v:pred.pn</ta>
            <ta e="T172" id="Seg_5813" s="T171">n-n:case</ta>
            <ta e="T173" id="Seg_5814" s="T172">v-v:ptcp</ta>
            <ta e="T174" id="Seg_5815" s="T173">n-n:poss-n:case</ta>
            <ta e="T175" id="Seg_5816" s="T174">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T176" id="Seg_5817" s="T175">n-n:case</ta>
            <ta e="T177" id="Seg_5818" s="T176">v-v:tense-v:pred.pn</ta>
            <ta e="T178" id="Seg_5819" s="T177">n-n:(ins)-n:case</ta>
            <ta e="T179" id="Seg_5820" s="T178">n-n:case</ta>
            <ta e="T180" id="Seg_5821" s="T179">n-n:(poss)-n:case</ta>
            <ta e="T181" id="Seg_5822" s="T180">n-n:poss-n:case</ta>
            <ta e="T182" id="Seg_5823" s="T181">v-v:cvb</ta>
            <ta e="T183" id="Seg_5824" s="T182">v-v:tense-v:pred.pn</ta>
            <ta e="T184" id="Seg_5825" s="T183">v-v:tense-v:pred.pn</ta>
            <ta e="T185" id="Seg_5826" s="T184">n-n:case</ta>
            <ta e="T186" id="Seg_5827" s="T185">n-n:(poss)-n:case</ta>
            <ta e="T187" id="Seg_5828" s="T186">n-n:poss-n:case</ta>
            <ta e="T188" id="Seg_5829" s="T187">v-v:tense-v:pred.pn</ta>
            <ta e="T189" id="Seg_5830" s="T188">ptcl</ta>
            <ta e="T190" id="Seg_5831" s="T189">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T191" id="Seg_5832" s="T190">n-n:case</ta>
            <ta e="T192" id="Seg_5833" s="T191">v-v:neg-v:pred.pn</ta>
            <ta e="T193" id="Seg_5834" s="T192">n-n&gt;v-v:mood.pn</ta>
            <ta e="T194" id="Seg_5835" s="T193">que-pro:(ins)-pro:case</ta>
            <ta e="T195" id="Seg_5836" s="T194">v-v:tense-v:pred.pn</ta>
            <ta e="T196" id="Seg_5837" s="T195">pers-pro:case</ta>
            <ta e="T197" id="Seg_5838" s="T196">adv</ta>
            <ta e="T198" id="Seg_5839" s="T197">v-v:tense-v:poss.pn</ta>
            <ta e="T199" id="Seg_5840" s="T198">n-n:(poss)-n:case</ta>
            <ta e="T200" id="Seg_5841" s="T199">pers-pro:case</ta>
            <ta e="T201" id="Seg_5842" s="T200">v-v:(ins)-v:ptcp-v:(ins)-v:(poss)-v:(case)</ta>
            <ta e="T202" id="Seg_5843" s="T201">post</ta>
            <ta e="T203" id="Seg_5844" s="T202">n-n:poss-n:case</ta>
            <ta e="T204" id="Seg_5845" s="T203">v-v:(tense)-v:mood.pn</ta>
            <ta e="T205" id="Seg_5846" s="T204">v-v:cvb</ta>
            <ta e="T206" id="Seg_5847" s="T205">v-v:tense-v:pred.pn</ta>
            <ta e="T207" id="Seg_5848" s="T206">v-v:cvb</ta>
            <ta e="T208" id="Seg_5849" s="T207">post</ta>
            <ta e="T209" id="Seg_5850" s="T208">n-n:poss-n:case</ta>
            <ta e="T210" id="Seg_5851" s="T209">v-v:tense-v:pred.pn</ta>
            <ta e="T211" id="Seg_5852" s="T210">que</ta>
            <ta e="T212" id="Seg_5853" s="T211">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T213" id="Seg_5854" s="T212">v-v:ptcp-v-v:ptcp</ta>
            <ta e="T214" id="Seg_5855" s="T213">n-n:case</ta>
            <ta e="T215" id="Seg_5856" s="T214">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T216" id="Seg_5857" s="T215">interj</ta>
            <ta e="T217" id="Seg_5858" s="T216">v-v:mood.pn</ta>
            <ta e="T218" id="Seg_5859" s="T217">dempro</ta>
            <ta e="T219" id="Seg_5860" s="T218">n-n:case</ta>
            <ta e="T220" id="Seg_5861" s="T219">n-n:poss-n:case</ta>
            <ta e="T221" id="Seg_5862" s="T220">v-v:tense-v:poss.pn</ta>
            <ta e="T222" id="Seg_5863" s="T221">ptcl</ta>
            <ta e="T223" id="Seg_5864" s="T222">v-v:cvb</ta>
            <ta e="T224" id="Seg_5865" s="T223">v-v:tense-v:pred.pn</ta>
            <ta e="T225" id="Seg_5866" s="T224">n-n:poss-n:case</ta>
            <ta e="T226" id="Seg_5867" s="T225">n-n:case</ta>
            <ta e="T227" id="Seg_5868" s="T226">n-n:(poss)-n:case</ta>
            <ta e="T228" id="Seg_5869" s="T227">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T229" id="Seg_5870" s="T228">v-v:ptcp-v:(ins)-v:(poss)-v:(case)</ta>
            <ta e="T230" id="Seg_5871" s="T229">v-v:mood-v:pred.pn</ta>
            <ta e="T231" id="Seg_5872" s="T230">v-v:cvb</ta>
            <ta e="T232" id="Seg_5873" s="T231">v-v:ptcp</ta>
            <ta e="T233" id="Seg_5874" s="T232">v-v:tense-v:poss.pn</ta>
            <ta e="T234" id="Seg_5875" s="T233">ptcl</ta>
            <ta e="T235" id="Seg_5876" s="T234">v-v:cvb</ta>
            <ta e="T236" id="Seg_5877" s="T235">dempro</ta>
            <ta e="T237" id="Seg_5878" s="T236">n-n:case</ta>
            <ta e="T238" id="Seg_5879" s="T237">n-n:poss-n:case</ta>
            <ta e="T239" id="Seg_5880" s="T238">n-n:case</ta>
            <ta e="T240" id="Seg_5881" s="T239">v-v:tense-v:pred.pn</ta>
            <ta e="T241" id="Seg_5882" s="T240">n-n:case</ta>
            <ta e="T242" id="Seg_5883" s="T241">post</ta>
            <ta e="T243" id="Seg_5884" s="T242">n-n:case</ta>
            <ta e="T244" id="Seg_5885" s="T243">cardnum</ta>
            <ta e="T245" id="Seg_5886" s="T244">n-n:case</ta>
            <ta e="T246" id="Seg_5887" s="T245">v-v:cvb</ta>
            <ta e="T247" id="Seg_5888" s="T246">n-n&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T248" id="Seg_5889" s="T247">cardnum</ta>
            <ta e="T249" id="Seg_5890" s="T248">n-n:case</ta>
            <ta e="T250" id="Seg_5891" s="T249">v-v:tense-v:pred.pn</ta>
            <ta e="T251" id="Seg_5892" s="T250">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T252" id="Seg_5893" s="T251">cardnum</ta>
            <ta e="T253" id="Seg_5894" s="T252">n-n:case</ta>
            <ta e="T254" id="Seg_5895" s="T253">v-v:cvb</ta>
            <ta e="T255" id="Seg_5896" s="T254">v-v:cvb</ta>
            <ta e="T256" id="Seg_5897" s="T255">v-v:tense-v:pred.pn</ta>
            <ta e="T257" id="Seg_5898" s="T256">adv</ta>
            <ta e="T258" id="Seg_5899" s="T257">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T259" id="Seg_5900" s="T258">n-n:case</ta>
            <ta e="T260" id="Seg_5901" s="T259">v-v:tense-v:pred.pn</ta>
            <ta e="T261" id="Seg_5902" s="T260">v-v:tense-v:poss.pn</ta>
            <ta e="T262" id="Seg_5903" s="T261">cardnum</ta>
            <ta e="T263" id="Seg_5904" s="T262">ptcl</ta>
            <ta e="T264" id="Seg_5905" s="T263">n-n:case</ta>
            <ta e="T265" id="Seg_5906" s="T264">ptcl</ta>
            <ta e="T266" id="Seg_5907" s="T265">n-n:case</ta>
            <ta e="T267" id="Seg_5908" s="T266">ptcl</ta>
            <ta e="T268" id="Seg_5909" s="T267">ptcl</ta>
            <ta e="T269" id="Seg_5910" s="T268">n-n:case</ta>
            <ta e="T270" id="Seg_5911" s="T269">n-n:case</ta>
            <ta e="T271" id="Seg_5912" s="T270">v-v:tense-v:poss.pn</ta>
            <ta e="T272" id="Seg_5913" s="T271">n-n:case</ta>
            <ta e="T273" id="Seg_5914" s="T272">n-n:(poss)-n:case</ta>
            <ta e="T274" id="Seg_5915" s="T273">ptcl</ta>
            <ta e="T275" id="Seg_5916" s="T274">v-v:cvb</ta>
            <ta e="T276" id="Seg_5917" s="T275">v-v:tense-v:pred.pn</ta>
            <ta e="T277" id="Seg_5918" s="T276">dempro</ta>
            <ta e="T278" id="Seg_5919" s="T277">cardnum</ta>
            <ta e="T279" id="Seg_5920" s="T278">n-n:case</ta>
            <ta e="T280" id="Seg_5921" s="T279">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T281" id="Seg_5922" s="T280">n-n:case</ta>
            <ta e="T282" id="Seg_5923" s="T281">v-v:tense-v:pred.pn</ta>
            <ta e="T283" id="Seg_5924" s="T282">cardnum</ta>
            <ta e="T284" id="Seg_5925" s="T283">n-n:case</ta>
            <ta e="T285" id="Seg_5926" s="T284">n-n:case</ta>
            <ta e="T286" id="Seg_5927" s="T285">ptcl</ta>
            <ta e="T287" id="Seg_5928" s="T286">cardnum</ta>
            <ta e="T288" id="Seg_5929" s="T287">n-n:case</ta>
            <ta e="T289" id="Seg_5930" s="T288">adj-n:case</ta>
            <ta e="T290" id="Seg_5931" s="T289">adj</ta>
            <ta e="T291" id="Seg_5932" s="T290">n-n&gt;adj-n:case</ta>
            <ta e="T292" id="Seg_5933" s="T291">cardnum</ta>
            <ta e="T293" id="Seg_5934" s="T292">n-n&gt;adj-n:case</ta>
            <ta e="T294" id="Seg_5935" s="T293">n-n:case</ta>
            <ta e="T295" id="Seg_5936" s="T294">post</ta>
            <ta e="T296" id="Seg_5937" s="T295">n-n:case</ta>
            <ta e="T297" id="Seg_5938" s="T296">n-n&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T298" id="Seg_5939" s="T297">que</ta>
            <ta e="T299" id="Seg_5940" s="T298">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T300" id="Seg_5941" s="T299">n-n:poss-n:case</ta>
            <ta e="T301" id="Seg_5942" s="T300">v-v:tense-v:poss.pn</ta>
            <ta e="T302" id="Seg_5943" s="T301">ptcl</ta>
            <ta e="T303" id="Seg_5944" s="T302">ptcl</ta>
            <ta e="T304" id="Seg_5945" s="T303">conj</ta>
            <ta e="T305" id="Seg_5946" s="T304">n-n:(num)-n:(poss)-n:case</ta>
            <ta e="T307" id="Seg_5947" s="T305">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T308" id="Seg_5948" s="T307">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T309" id="Seg_5949" s="T308">n-n:case</ta>
            <ta e="T310" id="Seg_5950" s="T309">adv</ta>
            <ta e="T311" id="Seg_5951" s="T310">v-v:cvb</ta>
            <ta e="T312" id="Seg_5952" s="T311">v-v:tense-v:pred.pn</ta>
            <ta e="T313" id="Seg_5953" s="T312">n-n:case</ta>
            <ta e="T314" id="Seg_5954" s="T313">n-n:case</ta>
            <ta e="T315" id="Seg_5955" s="T314">v-v:ptcp-v:(case)</ta>
            <ta e="T316" id="Seg_5956" s="T315">pers-pro:case</ta>
            <ta e="T317" id="Seg_5957" s="T316">n-n:poss-n:case</ta>
            <ta e="T318" id="Seg_5958" s="T317">conj</ta>
            <ta e="T319" id="Seg_5959" s="T318">v-v:ptcp-v:(poss)</ta>
            <ta e="T320" id="Seg_5960" s="T319">v-v:tense-v:poss.pn</ta>
            <ta e="T321" id="Seg_5961" s="T320">v-v:mood-v:temp.pn</ta>
            <ta e="T322" id="Seg_5962" s="T321">dempro</ta>
            <ta e="T323" id="Seg_5963" s="T322">n-n:case</ta>
            <ta e="T324" id="Seg_5964" s="T323">v-v:tense-v:pred.pn</ta>
            <ta e="T325" id="Seg_5965" s="T324">v-v:tense-v:pred.pn</ta>
            <ta e="T326" id="Seg_5966" s="T325">adj-n:poss-n:case</ta>
            <ta e="T327" id="Seg_5967" s="T326">ptcl</ta>
            <ta e="T328" id="Seg_5968" s="T327">interj</ta>
            <ta e="T329" id="Seg_5969" s="T328">v-v:tense-v:pred.pn</ta>
            <ta e="T330" id="Seg_5970" s="T329">n-n:case</ta>
            <ta e="T331" id="Seg_5971" s="T330">n-n&gt;adj</ta>
            <ta e="T332" id="Seg_5972" s="T331">adv</ta>
            <ta e="T333" id="Seg_5973" s="T332">v-v:ptcp</ta>
            <ta e="T334" id="Seg_5974" s="T333">v-v:tense-v:poss.pn</ta>
            <ta e="T335" id="Seg_5975" s="T334">n-n:case</ta>
            <ta e="T336" id="Seg_5976" s="T335">ptcl</ta>
            <ta e="T337" id="Seg_5977" s="T336">v-v:tense-v:pred.pn</ta>
            <ta e="T338" id="Seg_5978" s="T337">n-n:case</ta>
            <ta e="T339" id="Seg_5979" s="T338">ptcl</ta>
            <ta e="T340" id="Seg_5980" s="T339">que-pro:case</ta>
            <ta e="T341" id="Seg_5981" s="T340">ptcl</ta>
            <ta e="T342" id="Seg_5982" s="T341">ptcl</ta>
            <ta e="T343" id="Seg_5983" s="T342">n-n:poss-n:case</ta>
            <ta e="T344" id="Seg_5984" s="T343">v-v:cvb</ta>
            <ta e="T345" id="Seg_5985" s="T344">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T346" id="Seg_5986" s="T345">v-v:tense-v:pred.pn</ta>
            <ta e="T347" id="Seg_5987" s="T346">n-n:(poss)-n:case</ta>
            <ta e="T348" id="Seg_5988" s="T347">que-pro:case</ta>
            <ta e="T349" id="Seg_5989" s="T348">v-v:tense-v:poss.pn</ta>
            <ta e="T350" id="Seg_5990" s="T349">v-v:tense-v:pred.pn</ta>
            <ta e="T351" id="Seg_5991" s="T350">ptcl</ta>
            <ta e="T352" id="Seg_5992" s="T351">n-n:(poss)-n:case</ta>
            <ta e="T353" id="Seg_5993" s="T352">n-n:(poss)-n:case</ta>
            <ta e="T354" id="Seg_5994" s="T353">que</ta>
            <ta e="T355" id="Seg_5995" s="T354">ptcl</ta>
            <ta e="T356" id="Seg_5996" s="T355">ptcl</ta>
            <ta e="T357" id="Seg_5997" s="T356">v-v:tense-v:poss.pn</ta>
            <ta e="T358" id="Seg_5998" s="T357">dempro-pro:case</ta>
            <ta e="T359" id="Seg_5999" s="T358">v-v:(tense)-v:mood.pn</ta>
            <ta e="T360" id="Seg_6000" s="T359">n-n:(poss)-n:case</ta>
            <ta e="T361" id="Seg_6001" s="T360">n-n&gt;adj</ta>
            <ta e="T362" id="Seg_6002" s="T361">adj</ta>
            <ta e="T363" id="Seg_6003" s="T362">v-v:ptcp</ta>
            <ta e="T364" id="Seg_6004" s="T363">n-n:case</ta>
            <ta e="T365" id="Seg_6005" s="T364">v-v:tense-v:pred.pn</ta>
            <ta e="T366" id="Seg_6006" s="T365">dempro</ta>
            <ta e="T367" id="Seg_6007" s="T366">n-n:case</ta>
            <ta e="T368" id="Seg_6008" s="T367">adv</ta>
            <ta e="T369" id="Seg_6009" s="T368">v-v:cvb</ta>
            <ta e="T370" id="Seg_6010" s="T369">v-v:cvb</ta>
            <ta e="T371" id="Seg_6011" s="T370">v-v:tense-v:pred.pn</ta>
            <ta e="T372" id="Seg_6012" s="T371">v-v:tense-v:poss.pn</ta>
            <ta e="T373" id="Seg_6013" s="T372">ptcl</ta>
            <ta e="T374" id="Seg_6014" s="T373">v-v:tense-v:poss.pn</ta>
            <ta e="T375" id="Seg_6015" s="T374">que-pro:case</ta>
            <ta e="T376" id="Seg_6016" s="T375">ptcl</ta>
            <ta e="T377" id="Seg_6017" s="T376">ptcl</ta>
            <ta e="T378" id="Seg_6018" s="T377">n-n:case</ta>
            <ta e="T379" id="Seg_6019" s="T378">ptcl</ta>
            <ta e="T380" id="Seg_6020" s="T379">n-n:(poss)-n:case</ta>
            <ta e="T381" id="Seg_6021" s="T380">n-n:case</ta>
            <ta e="T382" id="Seg_6022" s="T381">n-n:case</ta>
            <ta e="T383" id="Seg_6023" s="T382">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T384" id="Seg_6024" s="T383">post</ta>
            <ta e="T385" id="Seg_6025" s="T384">n-n:poss-n:case</ta>
            <ta e="T386" id="Seg_6026" s="T385">v-v:tense-v:pred.pn</ta>
            <ta e="T387" id="Seg_6027" s="T386">n-n:case</ta>
            <ta e="T388" id="Seg_6028" s="T387">v-v:ptcp</ta>
            <ta e="T389" id="Seg_6029" s="T388">n-n:poss-n:case</ta>
            <ta e="T390" id="Seg_6030" s="T389">post</ta>
            <ta e="T391" id="Seg_6031" s="T390">n-n&gt;adj</ta>
            <ta e="T392" id="Seg_6032" s="T391">n-n:(poss)-n:case</ta>
            <ta e="T393" id="Seg_6033" s="T392">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T394" id="Seg_6034" s="T393">n-n:case</ta>
            <ta e="T395" id="Seg_6035" s="T394">conj</ta>
            <ta e="T396" id="Seg_6036" s="T395">v-v:neg-v:pred.pn</ta>
            <ta e="T397" id="Seg_6037" s="T396">n-n:poss-n:case</ta>
            <ta e="T398" id="Seg_6038" s="T397">v-v:cvb</ta>
            <ta e="T399" id="Seg_6039" s="T398">post</ta>
            <ta e="T400" id="Seg_6040" s="T399">v-v:tense-v:pred.pn</ta>
            <ta e="T401" id="Seg_6041" s="T400">n-n:case</ta>
            <ta e="T402" id="Seg_6042" s="T401">adj-n:case</ta>
            <ta e="T403" id="Seg_6043" s="T402">v-v:tense-v:pred.pn</ta>
            <ta e="T404" id="Seg_6044" s="T403">n-n:case</ta>
            <ta e="T405" id="Seg_6045" s="T404">v-v:ptcp</ta>
            <ta e="T406" id="Seg_6046" s="T405">adj</ta>
            <ta e="T407" id="Seg_6047" s="T406">n-n:case</ta>
            <ta e="T408" id="Seg_6048" s="T407">v-v:tense-v:pred.pn</ta>
            <ta e="T409" id="Seg_6049" s="T408">adj</ta>
            <ta e="T410" id="Seg_6050" s="T409">n-n:poss-n:case</ta>
            <ta e="T411" id="Seg_6051" s="T410">n-n:case</ta>
            <ta e="T412" id="Seg_6052" s="T411">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T413" id="Seg_6053" s="T412">post</ta>
            <ta e="T414" id="Seg_6054" s="T413">cardnum</ta>
            <ta e="T415" id="Seg_6055" s="T414">n-n:poss-n:case</ta>
            <ta e="T416" id="Seg_6056" s="T415">n-n:poss-n:case</ta>
            <ta e="T417" id="Seg_6057" s="T416">n-n:case</ta>
            <ta e="T418" id="Seg_6058" s="T417">adv</ta>
            <ta e="T419" id="Seg_6059" s="T418">v-v:tense-v:pred.pn</ta>
            <ta e="T420" id="Seg_6060" s="T419">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T421" id="Seg_6061" s="T420">n-n:poss-n:case</ta>
            <ta e="T422" id="Seg_6062" s="T421">n-n:poss-n:case</ta>
            <ta e="T423" id="Seg_6063" s="T422">v-v:tense-v:pred.pn</ta>
            <ta e="T424" id="Seg_6064" s="T423">n-n:case</ta>
            <ta e="T425" id="Seg_6065" s="T424">n-n:poss-n:case</ta>
            <ta e="T426" id="Seg_6066" s="T425">v-v:tense-v:pred.pn</ta>
            <ta e="T427" id="Seg_6067" s="T426">n-n:(poss)-n:case</ta>
            <ta e="T428" id="Seg_6068" s="T427">n-n&gt;adj</ta>
            <ta e="T429" id="Seg_6069" s="T428">v-v:cvb</ta>
            <ta e="T430" id="Seg_6070" s="T429">v-v:cvb</ta>
            <ta e="T431" id="Seg_6071" s="T430">adv</ta>
            <ta e="T432" id="Seg_6072" s="T431">n-n:(poss)-n:case</ta>
            <ta e="T433" id="Seg_6073" s="T432">n-n:case</ta>
            <ta e="T434" id="Seg_6074" s="T433">v-v:cvb</ta>
            <ta e="T435" id="Seg_6075" s="T434">v-v:tense-v:pred.pn</ta>
            <ta e="T436" id="Seg_6076" s="T435">n-n:case</ta>
            <ta e="T437" id="Seg_6077" s="T436">dempro</ta>
            <ta e="T438" id="Seg_6078" s="T437">n-n:case</ta>
            <ta e="T439" id="Seg_6079" s="T438">v-v:tense-v:pred.pn</ta>
            <ta e="T440" id="Seg_6080" s="T439">ptcl</ta>
            <ta e="T441" id="Seg_6081" s="T440">que-ptcl</ta>
            <ta e="T442" id="Seg_6082" s="T441">v-v:tense-v:poss.pn</ta>
            <ta e="T443" id="Seg_6083" s="T442">ptcl</ta>
            <ta e="T444" id="Seg_6084" s="T443">v-v:(neg)-v:pred.pn</ta>
            <ta e="T445" id="Seg_6085" s="T444">v-v:tense-v:pred.pn</ta>
            <ta e="T446" id="Seg_6086" s="T445">n-n:case</ta>
            <ta e="T447" id="Seg_6087" s="T446">v-v:tense-v:pred.pn</ta>
            <ta e="T448" id="Seg_6088" s="T447">que</ta>
            <ta e="T449" id="Seg_6089" s="T448">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T450" id="Seg_6090" s="T449">n-n&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T451" id="Seg_6091" s="T450">cardnum</ta>
            <ta e="T452" id="Seg_6092" s="T451">n-n:case</ta>
            <ta e="T453" id="Seg_6093" s="T452">v-v&gt;v-v:cvb</ta>
            <ta e="T454" id="Seg_6094" s="T453">v-v:tense-v:pred.pn</ta>
            <ta e="T455" id="Seg_6095" s="T454">n-n:case</ta>
            <ta e="T456" id="Seg_6096" s="T455">ptcl</ta>
            <ta e="T457" id="Seg_6097" s="T456">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T458" id="Seg_6098" s="T457">n-n:case</ta>
            <ta e="T459" id="Seg_6099" s="T458">post</ta>
            <ta e="T460" id="Seg_6100" s="T459">n-n:(ins)-n:case</ta>
            <ta e="T461" id="Seg_6101" s="T460">v-v:tense-v:pred.pn</ta>
            <ta e="T462" id="Seg_6102" s="T461">v-v:tense-v:pred.pn</ta>
            <ta e="T463" id="Seg_6103" s="T462">adv</ta>
            <ta e="T464" id="Seg_6104" s="T463">adv</ta>
            <ta e="T465" id="Seg_6105" s="T464">cardnum</ta>
            <ta e="T466" id="Seg_6106" s="T465">ptcl</ta>
            <ta e="T467" id="Seg_6107" s="T466">n-n:case</ta>
            <ta e="T468" id="Seg_6108" s="T467">ptcl</ta>
            <ta e="T469" id="Seg_6109" s="T468">n-n:poss-n:case</ta>
            <ta e="T470" id="Seg_6110" s="T469">n-n:(poss)-n:case</ta>
            <ta e="T471" id="Seg_6111" s="T470">v-v:(ins)-v:ptcp-v:(case)</ta>
            <ta e="T472" id="Seg_6112" s="T471">n-n:(poss)-n:case</ta>
            <ta e="T473" id="Seg_6113" s="T472">v-v&gt;v-v:cvb</ta>
            <ta e="T474" id="Seg_6114" s="T473">v-v:tense-v:pred.pn</ta>
            <ta e="T475" id="Seg_6115" s="T474">n-n:case</ta>
            <ta e="T476" id="Seg_6116" s="T475">n-n:poss-n:case</ta>
            <ta e="T477" id="Seg_6117" s="T476">v-v:tense-v:poss.pn</ta>
            <ta e="T478" id="Seg_6118" s="T477">n-n:case</ta>
            <ta e="T479" id="Seg_6119" s="T478">n-n:(poss)-n:case</ta>
            <ta e="T480" id="Seg_6120" s="T479">v-v:cvb</ta>
            <ta e="T481" id="Seg_6121" s="T480">v-v:cvb</ta>
            <ta e="T482" id="Seg_6122" s="T481">v-v:tense-v:pred.pn</ta>
            <ta e="T483" id="Seg_6123" s="T482">n-n:case</ta>
            <ta e="T484" id="Seg_6124" s="T483">n-n:case</ta>
            <ta e="T485" id="Seg_6125" s="T484">v-v:mood.pn</ta>
            <ta e="T486" id="Seg_6126" s="T485">dempro</ta>
            <ta e="T487" id="Seg_6127" s="T486">n-n:case</ta>
            <ta e="T488" id="Seg_6128" s="T487">n-n:case</ta>
            <ta e="T489" id="Seg_6129" s="T488">v-v:cvb</ta>
            <ta e="T490" id="Seg_6130" s="T489">v-v:tense-v:pred.pn</ta>
            <ta e="T491" id="Seg_6131" s="T490">adj</ta>
            <ta e="T492" id="Seg_6132" s="T491">n-n:case</ta>
            <ta e="T493" id="Seg_6133" s="T492">n-n:case</ta>
            <ta e="T494" id="Seg_6134" s="T493">n-n:poss-n:case</ta>
            <ta e="T495" id="Seg_6135" s="T494">v-v:cvb</ta>
            <ta e="T496" id="Seg_6136" s="T495">v-v:cvb-v-v:cvb</ta>
            <ta e="T497" id="Seg_6137" s="T496">v-v:tense-v:pred.pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_6138" s="T0">n</ta>
            <ta e="T2" id="Seg_6139" s="T1">post</ta>
            <ta e="T3" id="Seg_6140" s="T2">n</ta>
            <ta e="T4" id="Seg_6141" s="T3">distrnum</ta>
            <ta e="T5" id="Seg_6142" s="T4">adj</ta>
            <ta e="T6" id="Seg_6143" s="T5">v</ta>
            <ta e="T7" id="Seg_6144" s="T6">aux</ta>
            <ta e="T8" id="Seg_6145" s="T7">collnum</ta>
            <ta e="T9" id="Seg_6146" s="T8">adj</ta>
            <ta e="T10" id="Seg_6147" s="T9">n</ta>
            <ta e="T11" id="Seg_6148" s="T10">n</ta>
            <ta e="T12" id="Seg_6149" s="T11">adv</ta>
            <ta e="T13" id="Seg_6150" s="T12">v</ta>
            <ta e="T14" id="Seg_6151" s="T13">adv</ta>
            <ta e="T15" id="Seg_6152" s="T14">ptcl</ta>
            <ta e="T16" id="Seg_6153" s="T15">cop</ta>
            <ta e="T17" id="Seg_6154" s="T16">dempro</ta>
            <ta e="T18" id="Seg_6155" s="T17">n</ta>
            <ta e="T19" id="Seg_6156" s="T18">n</ta>
            <ta e="T20" id="Seg_6157" s="T19">v</ta>
            <ta e="T21" id="Seg_6158" s="T20">aux</ta>
            <ta e="T22" id="Seg_6159" s="T21">aux</ta>
            <ta e="T23" id="Seg_6160" s="T22">ptcl</ta>
            <ta e="T24" id="Seg_6161" s="T23">adv</ta>
            <ta e="T25" id="Seg_6162" s="T24">n</ta>
            <ta e="T26" id="Seg_6163" s="T25">v</ta>
            <ta e="T27" id="Seg_6164" s="T26">n</ta>
            <ta e="T28" id="Seg_6165" s="T27">cop</ta>
            <ta e="T29" id="Seg_6166" s="T28">n</ta>
            <ta e="T30" id="Seg_6167" s="T29">adv</ta>
            <ta e="T31" id="Seg_6168" s="T30">v</ta>
            <ta e="T32" id="Seg_6169" s="T31">aux</ta>
            <ta e="T33" id="Seg_6170" s="T32">cardnum</ta>
            <ta e="T34" id="Seg_6171" s="T33">n</ta>
            <ta e="T35" id="Seg_6172" s="T34">n</ta>
            <ta e="T36" id="Seg_6173" s="T35">v</ta>
            <ta e="T37" id="Seg_6174" s="T36">v</ta>
            <ta e="T38" id="Seg_6175" s="T37">n</ta>
            <ta e="T39" id="Seg_6176" s="T38">v</ta>
            <ta e="T40" id="Seg_6177" s="T39">post</ta>
            <ta e="T41" id="Seg_6178" s="T40">n</ta>
            <ta e="T42" id="Seg_6179" s="T41">v</ta>
            <ta e="T43" id="Seg_6180" s="T42">adv</ta>
            <ta e="T44" id="Seg_6181" s="T43">adv</ta>
            <ta e="T45" id="Seg_6182" s="T44">n</ta>
            <ta e="T46" id="Seg_6183" s="T45">v</ta>
            <ta e="T47" id="Seg_6184" s="T46">post</ta>
            <ta e="T48" id="Seg_6185" s="T47">n</ta>
            <ta e="T49" id="Seg_6186" s="T48">n</ta>
            <ta e="T50" id="Seg_6187" s="T49">n</ta>
            <ta e="T51" id="Seg_6188" s="T50">v</ta>
            <ta e="T52" id="Seg_6189" s="T51">n</ta>
            <ta e="T53" id="Seg_6190" s="T52">v</ta>
            <ta e="T54" id="Seg_6191" s="T53">v</ta>
            <ta e="T55" id="Seg_6192" s="T54">adv</ta>
            <ta e="T56" id="Seg_6193" s="T55">adj</ta>
            <ta e="T57" id="Seg_6194" s="T56">n</ta>
            <ta e="T58" id="Seg_6195" s="T57">v</ta>
            <ta e="T59" id="Seg_6196" s="T58">n</ta>
            <ta e="T60" id="Seg_6197" s="T59">n</ta>
            <ta e="T61" id="Seg_6198" s="T60">v</ta>
            <ta e="T62" id="Seg_6199" s="T61">adj</ta>
            <ta e="T63" id="Seg_6200" s="T62">n</ta>
            <ta e="T64" id="Seg_6201" s="T63">v</ta>
            <ta e="T65" id="Seg_6202" s="T64">adv</ta>
            <ta e="T66" id="Seg_6203" s="T65">ptcl</ta>
            <ta e="T67" id="Seg_6204" s="T66">emphpro</ta>
            <ta e="T68" id="Seg_6205" s="T67">v</ta>
            <ta e="T69" id="Seg_6206" s="T68">v</ta>
            <ta e="T70" id="Seg_6207" s="T69">n</ta>
            <ta e="T71" id="Seg_6208" s="T70">v</ta>
            <ta e="T72" id="Seg_6209" s="T71">aux</ta>
            <ta e="T73" id="Seg_6210" s="T72">v</ta>
            <ta e="T74" id="Seg_6211" s="T73">n</ta>
            <ta e="T75" id="Seg_6212" s="T74">v</ta>
            <ta e="T76" id="Seg_6213" s="T75">aux</ta>
            <ta e="T77" id="Seg_6214" s="T76">v</ta>
            <ta e="T78" id="Seg_6215" s="T77">n</ta>
            <ta e="T79" id="Seg_6216" s="T78">v</ta>
            <ta e="T80" id="Seg_6217" s="T79">n</ta>
            <ta e="T81" id="Seg_6218" s="T80">n</ta>
            <ta e="T82" id="Seg_6219" s="T81">v</ta>
            <ta e="T83" id="Seg_6220" s="T82">v</ta>
            <ta e="T84" id="Seg_6221" s="T83">n</ta>
            <ta e="T85" id="Seg_6222" s="T84">dempro</ta>
            <ta e="T86" id="Seg_6223" s="T85">post</ta>
            <ta e="T87" id="Seg_6224" s="T86">v</ta>
            <ta e="T88" id="Seg_6225" s="T87">aux</ta>
            <ta e="T89" id="Seg_6226" s="T88">n</ta>
            <ta e="T90" id="Seg_6227" s="T89">v</ta>
            <ta e="T91" id="Seg_6228" s="T90">conj</ta>
            <ta e="T92" id="Seg_6229" s="T91">adj</ta>
            <ta e="T93" id="Seg_6230" s="T92">ptcl</ta>
            <ta e="T94" id="Seg_6231" s="T93">n</ta>
            <ta e="T95" id="Seg_6232" s="T94">adv</ta>
            <ta e="T96" id="Seg_6233" s="T95">v</ta>
            <ta e="T97" id="Seg_6234" s="T96">v</ta>
            <ta e="T98" id="Seg_6235" s="T97">adj</ta>
            <ta e="T99" id="Seg_6236" s="T98">n</ta>
            <ta e="T100" id="Seg_6237" s="T99">adj</ta>
            <ta e="T101" id="Seg_6238" s="T100">n</ta>
            <ta e="T102" id="Seg_6239" s="T101">adj</ta>
            <ta e="T103" id="Seg_6240" s="T102">adj</ta>
            <ta e="T104" id="Seg_6241" s="T103">ptcl</ta>
            <ta e="T105" id="Seg_6242" s="T104">adj</ta>
            <ta e="T106" id="Seg_6243" s="T105">n</ta>
            <ta e="T107" id="Seg_6244" s="T106">n</ta>
            <ta e="T108" id="Seg_6245" s="T107">n</ta>
            <ta e="T109" id="Seg_6246" s="T108">v</ta>
            <ta e="T110" id="Seg_6247" s="T109">n</ta>
            <ta e="T111" id="Seg_6248" s="T110">v</ta>
            <ta e="T112" id="Seg_6249" s="T111">n</ta>
            <ta e="T113" id="Seg_6250" s="T112">n</ta>
            <ta e="T114" id="Seg_6251" s="T113">v</ta>
            <ta e="T115" id="Seg_6252" s="T114">n</ta>
            <ta e="T116" id="Seg_6253" s="T115">v</ta>
            <ta e="T117" id="Seg_6254" s="T116">n</ta>
            <ta e="T118" id="Seg_6255" s="T117">v</ta>
            <ta e="T119" id="Seg_6256" s="T118">ptcl</ta>
            <ta e="T120" id="Seg_6257" s="T119">n</ta>
            <ta e="T121" id="Seg_6258" s="T120">n</ta>
            <ta e="T122" id="Seg_6259" s="T121">v</ta>
            <ta e="T123" id="Seg_6260" s="T122">v</ta>
            <ta e="T124" id="Seg_6261" s="T123">dempro</ta>
            <ta e="T125" id="Seg_6262" s="T124">n</ta>
            <ta e="T126" id="Seg_6263" s="T125">n</ta>
            <ta e="T127" id="Seg_6264" s="T126">n</ta>
            <ta e="T128" id="Seg_6265" s="T127">v</ta>
            <ta e="T129" id="Seg_6266" s="T128">n</ta>
            <ta e="T130" id="Seg_6267" s="T129">n</ta>
            <ta e="T131" id="Seg_6268" s="T130">n</ta>
            <ta e="T132" id="Seg_6269" s="T131">v</ta>
            <ta e="T133" id="Seg_6270" s="T132">post</ta>
            <ta e="T134" id="Seg_6271" s="T133">v</ta>
            <ta e="T135" id="Seg_6272" s="T134">n</ta>
            <ta e="T136" id="Seg_6273" s="T135">v</ta>
            <ta e="T137" id="Seg_6274" s="T136">v</ta>
            <ta e="T138" id="Seg_6275" s="T137">adj</ta>
            <ta e="T139" id="Seg_6276" s="T138">ptcl</ta>
            <ta e="T140" id="Seg_6277" s="T139">adj</ta>
            <ta e="T141" id="Seg_6278" s="T140">cop</ta>
            <ta e="T142" id="Seg_6279" s="T141">n</ta>
            <ta e="T143" id="Seg_6280" s="T142">v</ta>
            <ta e="T144" id="Seg_6281" s="T143">v</ta>
            <ta e="T145" id="Seg_6282" s="T144">n</ta>
            <ta e="T146" id="Seg_6283" s="T145">v</ta>
            <ta e="T147" id="Seg_6284" s="T146">aux</ta>
            <ta e="T148" id="Seg_6285" s="T147">v</ta>
            <ta e="T149" id="Seg_6286" s="T148">n</ta>
            <ta e="T150" id="Seg_6287" s="T149">n</ta>
            <ta e="T151" id="Seg_6288" s="T150">cardnum</ta>
            <ta e="T152" id="Seg_6289" s="T151">n</ta>
            <ta e="T153" id="Seg_6290" s="T152">v</ta>
            <ta e="T154" id="Seg_6291" s="T153">v</ta>
            <ta e="T155" id="Seg_6292" s="T154">post</ta>
            <ta e="T156" id="Seg_6293" s="T155">n</ta>
            <ta e="T157" id="Seg_6294" s="T156">v</ta>
            <ta e="T158" id="Seg_6295" s="T157">n</ta>
            <ta e="T159" id="Seg_6296" s="T158">v</ta>
            <ta e="T160" id="Seg_6297" s="T159">v</ta>
            <ta e="T161" id="Seg_6298" s="T160">v</ta>
            <ta e="T162" id="Seg_6299" s="T161">post</ta>
            <ta e="T163" id="Seg_6300" s="T162">n</ta>
            <ta e="T164" id="Seg_6301" s="T163">v</ta>
            <ta e="T165" id="Seg_6302" s="T164">v</ta>
            <ta e="T166" id="Seg_6303" s="T165">n</ta>
            <ta e="T167" id="Seg_6304" s="T166">n</ta>
            <ta e="T168" id="Seg_6305" s="T167">v</ta>
            <ta e="T169" id="Seg_6306" s="T168">aux</ta>
            <ta e="T170" id="Seg_6307" s="T169">adv</ta>
            <ta e="T171" id="Seg_6308" s="T170">v</ta>
            <ta e="T172" id="Seg_6309" s="T171">n</ta>
            <ta e="T173" id="Seg_6310" s="T172">v</ta>
            <ta e="T174" id="Seg_6311" s="T173">n</ta>
            <ta e="T175" id="Seg_6312" s="T174">v</ta>
            <ta e="T176" id="Seg_6313" s="T175">n</ta>
            <ta e="T177" id="Seg_6314" s="T176">v</ta>
            <ta e="T178" id="Seg_6315" s="T177">n</ta>
            <ta e="T179" id="Seg_6316" s="T178">n</ta>
            <ta e="T180" id="Seg_6317" s="T179">n</ta>
            <ta e="T181" id="Seg_6318" s="T180">n</ta>
            <ta e="T182" id="Seg_6319" s="T181">v</ta>
            <ta e="T183" id="Seg_6320" s="T182">aux</ta>
            <ta e="T184" id="Seg_6321" s="T183">v</ta>
            <ta e="T185" id="Seg_6322" s="T184">n</ta>
            <ta e="T186" id="Seg_6323" s="T185">n</ta>
            <ta e="T187" id="Seg_6324" s="T186">n</ta>
            <ta e="T188" id="Seg_6325" s="T187">v</ta>
            <ta e="T189" id="Seg_6326" s="T188">ptcl</ta>
            <ta e="T190" id="Seg_6327" s="T189">dempro</ta>
            <ta e="T191" id="Seg_6328" s="T190">n</ta>
            <ta e="T192" id="Seg_6329" s="T191">cop</ta>
            <ta e="T193" id="Seg_6330" s="T192">v</ta>
            <ta e="T194" id="Seg_6331" s="T193">que</ta>
            <ta e="T195" id="Seg_6332" s="T194">v</ta>
            <ta e="T196" id="Seg_6333" s="T195">pers</ta>
            <ta e="T197" id="Seg_6334" s="T196">adv</ta>
            <ta e="T198" id="Seg_6335" s="T197">v</ta>
            <ta e="T199" id="Seg_6336" s="T198">n</ta>
            <ta e="T200" id="Seg_6337" s="T199">pers</ta>
            <ta e="T201" id="Seg_6338" s="T200">v</ta>
            <ta e="T202" id="Seg_6339" s="T201">post</ta>
            <ta e="T203" id="Seg_6340" s="T202">n</ta>
            <ta e="T204" id="Seg_6341" s="T203">v</ta>
            <ta e="T205" id="Seg_6342" s="T204">v</ta>
            <ta e="T206" id="Seg_6343" s="T205">aux</ta>
            <ta e="T207" id="Seg_6344" s="T206">v</ta>
            <ta e="T208" id="Seg_6345" s="T207">post</ta>
            <ta e="T209" id="Seg_6346" s="T208">n</ta>
            <ta e="T210" id="Seg_6347" s="T209">v</ta>
            <ta e="T211" id="Seg_6348" s="T210">que</ta>
            <ta e="T212" id="Seg_6349" s="T211">cop</ta>
            <ta e="T213" id="Seg_6350" s="T212">v</ta>
            <ta e="T214" id="Seg_6351" s="T213">n</ta>
            <ta e="T215" id="Seg_6352" s="T214">v</ta>
            <ta e="T216" id="Seg_6353" s="T215">interj</ta>
            <ta e="T217" id="Seg_6354" s="T216">v</ta>
            <ta e="T218" id="Seg_6355" s="T217">dempro</ta>
            <ta e="T219" id="Seg_6356" s="T218">n</ta>
            <ta e="T220" id="Seg_6357" s="T219">n</ta>
            <ta e="T221" id="Seg_6358" s="T220">v</ta>
            <ta e="T222" id="Seg_6359" s="T221">ptcl</ta>
            <ta e="T223" id="Seg_6360" s="T222">v</ta>
            <ta e="T224" id="Seg_6361" s="T223">aux</ta>
            <ta e="T225" id="Seg_6362" s="T224">n</ta>
            <ta e="T226" id="Seg_6363" s="T225">n</ta>
            <ta e="T227" id="Seg_6364" s="T226">n</ta>
            <ta e="T228" id="Seg_6365" s="T227">v</ta>
            <ta e="T229" id="Seg_6366" s="T228">v</ta>
            <ta e="T230" id="Seg_6367" s="T229">cop</ta>
            <ta e="T231" id="Seg_6368" s="T230">v</ta>
            <ta e="T232" id="Seg_6369" s="T231">aux</ta>
            <ta e="T233" id="Seg_6370" s="T232">aux</ta>
            <ta e="T234" id="Seg_6371" s="T233">ptcl</ta>
            <ta e="T235" id="Seg_6372" s="T234">v</ta>
            <ta e="T236" id="Seg_6373" s="T235">dempro</ta>
            <ta e="T237" id="Seg_6374" s="T236">n</ta>
            <ta e="T238" id="Seg_6375" s="T237">n</ta>
            <ta e="T239" id="Seg_6376" s="T238">n</ta>
            <ta e="T240" id="Seg_6377" s="T239">v</ta>
            <ta e="T241" id="Seg_6378" s="T240">n</ta>
            <ta e="T242" id="Seg_6379" s="T241">post</ta>
            <ta e="T243" id="Seg_6380" s="T242">n</ta>
            <ta e="T244" id="Seg_6381" s="T243">cardnum</ta>
            <ta e="T245" id="Seg_6382" s="T244">n</ta>
            <ta e="T246" id="Seg_6383" s="T245">v</ta>
            <ta e="T247" id="Seg_6384" s="T246">v</ta>
            <ta e="T248" id="Seg_6385" s="T247">cardnum</ta>
            <ta e="T249" id="Seg_6386" s="T248">n</ta>
            <ta e="T250" id="Seg_6387" s="T249">v</ta>
            <ta e="T251" id="Seg_6388" s="T250">v</ta>
            <ta e="T252" id="Seg_6389" s="T251">cardnum</ta>
            <ta e="T253" id="Seg_6390" s="T252">n</ta>
            <ta e="T254" id="Seg_6391" s="T253">cop</ta>
            <ta e="T255" id="Seg_6392" s="T254">v</ta>
            <ta e="T256" id="Seg_6393" s="T255">aux</ta>
            <ta e="T257" id="Seg_6394" s="T256">adv</ta>
            <ta e="T258" id="Seg_6395" s="T257">cop</ta>
            <ta e="T259" id="Seg_6396" s="T258">n</ta>
            <ta e="T260" id="Seg_6397" s="T259">v</ta>
            <ta e="T261" id="Seg_6398" s="T260">v</ta>
            <ta e="T262" id="Seg_6399" s="T261">cardnum</ta>
            <ta e="T263" id="Seg_6400" s="T262">ptcl</ta>
            <ta e="T264" id="Seg_6401" s="T263">n</ta>
            <ta e="T265" id="Seg_6402" s="T264">ptcl</ta>
            <ta e="T266" id="Seg_6403" s="T265">n</ta>
            <ta e="T267" id="Seg_6404" s="T266">ptcl</ta>
            <ta e="T268" id="Seg_6405" s="T267">ptcl</ta>
            <ta e="T269" id="Seg_6406" s="T268">n</ta>
            <ta e="T270" id="Seg_6407" s="T269">n</ta>
            <ta e="T271" id="Seg_6408" s="T270">v</ta>
            <ta e="T272" id="Seg_6409" s="T271">n</ta>
            <ta e="T273" id="Seg_6410" s="T272">n</ta>
            <ta e="T274" id="Seg_6411" s="T273">ptcl</ta>
            <ta e="T275" id="Seg_6412" s="T274">v</ta>
            <ta e="T276" id="Seg_6413" s="T275">v</ta>
            <ta e="T277" id="Seg_6414" s="T276">dempro</ta>
            <ta e="T278" id="Seg_6415" s="T277">cardnum</ta>
            <ta e="T279" id="Seg_6416" s="T278">n</ta>
            <ta e="T280" id="Seg_6417" s="T279">v</ta>
            <ta e="T281" id="Seg_6418" s="T280">n</ta>
            <ta e="T282" id="Seg_6419" s="T281">v</ta>
            <ta e="T283" id="Seg_6420" s="T282">cardnum</ta>
            <ta e="T284" id="Seg_6421" s="T283">n</ta>
            <ta e="T285" id="Seg_6422" s="T284">n</ta>
            <ta e="T286" id="Seg_6423" s="T285">ptcl</ta>
            <ta e="T287" id="Seg_6424" s="T286">cardnum</ta>
            <ta e="T288" id="Seg_6425" s="T287">n</ta>
            <ta e="T289" id="Seg_6426" s="T288">adj</ta>
            <ta e="T290" id="Seg_6427" s="T289">adj</ta>
            <ta e="T291" id="Seg_6428" s="T290">adj</ta>
            <ta e="T292" id="Seg_6429" s="T291">cardnum</ta>
            <ta e="T293" id="Seg_6430" s="T292">adj</ta>
            <ta e="T294" id="Seg_6431" s="T293">n</ta>
            <ta e="T295" id="Seg_6432" s="T294">post</ta>
            <ta e="T296" id="Seg_6433" s="T295">n</ta>
            <ta e="T297" id="Seg_6434" s="T296">v</ta>
            <ta e="T298" id="Seg_6435" s="T297">que</ta>
            <ta e="T299" id="Seg_6436" s="T298">cop</ta>
            <ta e="T300" id="Seg_6437" s="T299">n</ta>
            <ta e="T301" id="Seg_6438" s="T300">v</ta>
            <ta e="T302" id="Seg_6439" s="T301">ptcl</ta>
            <ta e="T303" id="Seg_6440" s="T302">ptcl</ta>
            <ta e="T304" id="Seg_6441" s="T303">conj</ta>
            <ta e="T305" id="Seg_6442" s="T304">n</ta>
            <ta e="T307" id="Seg_6443" s="T305">ptcl</ta>
            <ta e="T308" id="Seg_6444" s="T307">v</ta>
            <ta e="T309" id="Seg_6445" s="T308">n</ta>
            <ta e="T310" id="Seg_6446" s="T309">adv</ta>
            <ta e="T311" id="Seg_6447" s="T310">aux</ta>
            <ta e="T312" id="Seg_6448" s="T311">v</ta>
            <ta e="T313" id="Seg_6449" s="T312">n</ta>
            <ta e="T314" id="Seg_6450" s="T313">n</ta>
            <ta e="T315" id="Seg_6451" s="T314">v</ta>
            <ta e="T316" id="Seg_6452" s="T315">pers</ta>
            <ta e="T317" id="Seg_6453" s="T316">n</ta>
            <ta e="T318" id="Seg_6454" s="T317">conj</ta>
            <ta e="T319" id="Seg_6455" s="T318">v</ta>
            <ta e="T320" id="Seg_6456" s="T319">aux</ta>
            <ta e="T321" id="Seg_6457" s="T320">v</ta>
            <ta e="T322" id="Seg_6458" s="T321">dempro</ta>
            <ta e="T323" id="Seg_6459" s="T322">n</ta>
            <ta e="T324" id="Seg_6460" s="T323">v</ta>
            <ta e="T325" id="Seg_6461" s="T324">v</ta>
            <ta e="T326" id="Seg_6462" s="T325">adj</ta>
            <ta e="T327" id="Seg_6463" s="T326">ptcl</ta>
            <ta e="T328" id="Seg_6464" s="T327">interj</ta>
            <ta e="T329" id="Seg_6465" s="T328">cop</ta>
            <ta e="T330" id="Seg_6466" s="T329">n</ta>
            <ta e="T331" id="Seg_6467" s="T330">adj</ta>
            <ta e="T332" id="Seg_6468" s="T331">adv</ta>
            <ta e="T333" id="Seg_6469" s="T332">v</ta>
            <ta e="T334" id="Seg_6470" s="T333">aux</ta>
            <ta e="T335" id="Seg_6471" s="T334">n</ta>
            <ta e="T336" id="Seg_6472" s="T335">ptcl</ta>
            <ta e="T337" id="Seg_6473" s="T336">v</ta>
            <ta e="T338" id="Seg_6474" s="T337">n</ta>
            <ta e="T339" id="Seg_6475" s="T338">ptcl</ta>
            <ta e="T340" id="Seg_6476" s="T339">que</ta>
            <ta e="T341" id="Seg_6477" s="T340">ptcl</ta>
            <ta e="T342" id="Seg_6478" s="T341">ptcl</ta>
            <ta e="T343" id="Seg_6479" s="T342">n</ta>
            <ta e="T344" id="Seg_6480" s="T343">v</ta>
            <ta e="T345" id="Seg_6481" s="T344">v</ta>
            <ta e="T346" id="Seg_6482" s="T345">aux</ta>
            <ta e="T347" id="Seg_6483" s="T346">n</ta>
            <ta e="T348" id="Seg_6484" s="T347">que</ta>
            <ta e="T349" id="Seg_6485" s="T348">cop</ta>
            <ta e="T350" id="Seg_6486" s="T349">v</ta>
            <ta e="T351" id="Seg_6487" s="T350">ptcl</ta>
            <ta e="T352" id="Seg_6488" s="T351">n</ta>
            <ta e="T353" id="Seg_6489" s="T352">n</ta>
            <ta e="T354" id="Seg_6490" s="T353">que</ta>
            <ta e="T355" id="Seg_6491" s="T354">ptcl</ta>
            <ta e="T356" id="Seg_6492" s="T355">ptcl</ta>
            <ta e="T357" id="Seg_6493" s="T356">cop</ta>
            <ta e="T358" id="Seg_6494" s="T357">dempro</ta>
            <ta e="T359" id="Seg_6495" s="T358">v</ta>
            <ta e="T360" id="Seg_6496" s="T359">n</ta>
            <ta e="T361" id="Seg_6497" s="T360">adj</ta>
            <ta e="T362" id="Seg_6498" s="T361">adj</ta>
            <ta e="T363" id="Seg_6499" s="T362">v</ta>
            <ta e="T364" id="Seg_6500" s="T363">n</ta>
            <ta e="T365" id="Seg_6501" s="T364">v</ta>
            <ta e="T366" id="Seg_6502" s="T365">dempro</ta>
            <ta e="T367" id="Seg_6503" s="T366">n</ta>
            <ta e="T368" id="Seg_6504" s="T367">adv</ta>
            <ta e="T369" id="Seg_6505" s="T368">cop</ta>
            <ta e="T370" id="Seg_6506" s="T369">v</ta>
            <ta e="T371" id="Seg_6507" s="T370">aux</ta>
            <ta e="T372" id="Seg_6508" s="T371">v</ta>
            <ta e="T373" id="Seg_6509" s="T372">ptcl</ta>
            <ta e="T374" id="Seg_6510" s="T373">v</ta>
            <ta e="T375" id="Seg_6511" s="T374">que</ta>
            <ta e="T376" id="Seg_6512" s="T375">ptcl</ta>
            <ta e="T377" id="Seg_6513" s="T376">ptcl</ta>
            <ta e="T378" id="Seg_6514" s="T377">n</ta>
            <ta e="T379" id="Seg_6515" s="T378">ptcl</ta>
            <ta e="T380" id="Seg_6516" s="T379">n</ta>
            <ta e="T381" id="Seg_6517" s="T380">n</ta>
            <ta e="T382" id="Seg_6518" s="T381">n</ta>
            <ta e="T383" id="Seg_6519" s="T382">v</ta>
            <ta e="T384" id="Seg_6520" s="T383">post</ta>
            <ta e="T385" id="Seg_6521" s="T384">n</ta>
            <ta e="T386" id="Seg_6522" s="T385">v</ta>
            <ta e="T387" id="Seg_6523" s="T386">n</ta>
            <ta e="T388" id="Seg_6524" s="T387">v</ta>
            <ta e="T389" id="Seg_6525" s="T388">n</ta>
            <ta e="T390" id="Seg_6526" s="T389">post</ta>
            <ta e="T391" id="Seg_6527" s="T390">adj</ta>
            <ta e="T392" id="Seg_6528" s="T391">n</ta>
            <ta e="T393" id="Seg_6529" s="T392">v</ta>
            <ta e="T394" id="Seg_6530" s="T393">n</ta>
            <ta e="T395" id="Seg_6531" s="T394">conj</ta>
            <ta e="T396" id="Seg_6532" s="T395">v</ta>
            <ta e="T397" id="Seg_6533" s="T396">n</ta>
            <ta e="T398" id="Seg_6534" s="T397">v</ta>
            <ta e="T399" id="Seg_6535" s="T398">post</ta>
            <ta e="T400" id="Seg_6536" s="T399">v</ta>
            <ta e="T401" id="Seg_6537" s="T400">n</ta>
            <ta e="T402" id="Seg_6538" s="T401">adj</ta>
            <ta e="T403" id="Seg_6539" s="T402">cop</ta>
            <ta e="T404" id="Seg_6540" s="T403">n</ta>
            <ta e="T405" id="Seg_6541" s="T404">v</ta>
            <ta e="T406" id="Seg_6542" s="T405">adj</ta>
            <ta e="T407" id="Seg_6543" s="T406">n</ta>
            <ta e="T408" id="Seg_6544" s="T407">v</ta>
            <ta e="T409" id="Seg_6545" s="T408">adj</ta>
            <ta e="T410" id="Seg_6546" s="T409">n</ta>
            <ta e="T411" id="Seg_6547" s="T410">n</ta>
            <ta e="T412" id="Seg_6548" s="T411">v</ta>
            <ta e="T413" id="Seg_6549" s="T412">post</ta>
            <ta e="T414" id="Seg_6550" s="T413">cardnum</ta>
            <ta e="T415" id="Seg_6551" s="T414">n</ta>
            <ta e="T416" id="Seg_6552" s="T415">n</ta>
            <ta e="T417" id="Seg_6553" s="T416">n</ta>
            <ta e="T418" id="Seg_6554" s="T417">adv</ta>
            <ta e="T419" id="Seg_6555" s="T418">v</ta>
            <ta e="T420" id="Seg_6556" s="T419">emphpro</ta>
            <ta e="T421" id="Seg_6557" s="T420">n</ta>
            <ta e="T422" id="Seg_6558" s="T421">n</ta>
            <ta e="T423" id="Seg_6559" s="T422">v</ta>
            <ta e="T424" id="Seg_6560" s="T423">adv</ta>
            <ta e="T425" id="Seg_6561" s="T424">n</ta>
            <ta e="T426" id="Seg_6562" s="T425">v</ta>
            <ta e="T427" id="Seg_6563" s="T426">n</ta>
            <ta e="T428" id="Seg_6564" s="T427">adj</ta>
            <ta e="T429" id="Seg_6565" s="T428">v</ta>
            <ta e="T430" id="Seg_6566" s="T429">v</ta>
            <ta e="T431" id="Seg_6567" s="T430">adv</ta>
            <ta e="T432" id="Seg_6568" s="T431">n</ta>
            <ta e="T433" id="Seg_6569" s="T432">n</ta>
            <ta e="T434" id="Seg_6570" s="T433">v</ta>
            <ta e="T435" id="Seg_6571" s="T434">v</ta>
            <ta e="T436" id="Seg_6572" s="T435">n</ta>
            <ta e="T437" id="Seg_6573" s="T436">dempro</ta>
            <ta e="T438" id="Seg_6574" s="T437">n</ta>
            <ta e="T439" id="Seg_6575" s="T438">v</ta>
            <ta e="T440" id="Seg_6576" s="T439">ptcl</ta>
            <ta e="T441" id="Seg_6577" s="T440">que</ta>
            <ta e="T442" id="Seg_6578" s="T441">v</ta>
            <ta e="T443" id="Seg_6579" s="T442">ptcl</ta>
            <ta e="T444" id="Seg_6580" s="T443">v</ta>
            <ta e="T445" id="Seg_6581" s="T444">v</ta>
            <ta e="T446" id="Seg_6582" s="T445">n</ta>
            <ta e="T447" id="Seg_6583" s="T446">v</ta>
            <ta e="T448" id="Seg_6584" s="T447">que</ta>
            <ta e="T449" id="Seg_6585" s="T448">v</ta>
            <ta e="T450" id="Seg_6586" s="T449">v</ta>
            <ta e="T451" id="Seg_6587" s="T450">cardnum</ta>
            <ta e="T452" id="Seg_6588" s="T451">n</ta>
            <ta e="T453" id="Seg_6589" s="T452">v</ta>
            <ta e="T454" id="Seg_6590" s="T453">v</ta>
            <ta e="T455" id="Seg_6591" s="T454">n</ta>
            <ta e="T456" id="Seg_6592" s="T455">ptcl</ta>
            <ta e="T457" id="Seg_6593" s="T456">v</ta>
            <ta e="T458" id="Seg_6594" s="T457">n</ta>
            <ta e="T459" id="Seg_6595" s="T458">post</ta>
            <ta e="T460" id="Seg_6596" s="T459">n</ta>
            <ta e="T461" id="Seg_6597" s="T460">v</ta>
            <ta e="T462" id="Seg_6598" s="T461">v</ta>
            <ta e="T463" id="Seg_6599" s="T462">adv</ta>
            <ta e="T464" id="Seg_6600" s="T463">adv</ta>
            <ta e="T465" id="Seg_6601" s="T464">cardnum</ta>
            <ta e="T466" id="Seg_6602" s="T465">ptcl</ta>
            <ta e="T467" id="Seg_6603" s="T466">n</ta>
            <ta e="T468" id="Seg_6604" s="T467">ptcl</ta>
            <ta e="T469" id="Seg_6605" s="T468">n</ta>
            <ta e="T470" id="Seg_6606" s="T469">n</ta>
            <ta e="T471" id="Seg_6607" s="T470">v</ta>
            <ta e="T472" id="Seg_6608" s="T471">n</ta>
            <ta e="T473" id="Seg_6609" s="T472">v</ta>
            <ta e="T474" id="Seg_6610" s="T473">aux</ta>
            <ta e="T475" id="Seg_6611" s="T474">n</ta>
            <ta e="T476" id="Seg_6612" s="T475">n</ta>
            <ta e="T477" id="Seg_6613" s="T476">v</ta>
            <ta e="T478" id="Seg_6614" s="T477">n</ta>
            <ta e="T479" id="Seg_6615" s="T478">n</ta>
            <ta e="T480" id="Seg_6616" s="T479">v</ta>
            <ta e="T481" id="Seg_6617" s="T480">aux</ta>
            <ta e="T482" id="Seg_6618" s="T481">v</ta>
            <ta e="T483" id="Seg_6619" s="T482">n</ta>
            <ta e="T484" id="Seg_6620" s="T483">n</ta>
            <ta e="T485" id="Seg_6621" s="T484">v</ta>
            <ta e="T486" id="Seg_6622" s="T485">dempro</ta>
            <ta e="T487" id="Seg_6623" s="T486">n</ta>
            <ta e="T488" id="Seg_6624" s="T487">n</ta>
            <ta e="T489" id="Seg_6625" s="T488">v</ta>
            <ta e="T490" id="Seg_6626" s="T489">aux</ta>
            <ta e="T491" id="Seg_6627" s="T490">adj</ta>
            <ta e="T492" id="Seg_6628" s="T491">n</ta>
            <ta e="T493" id="Seg_6629" s="T492">n</ta>
            <ta e="T494" id="Seg_6630" s="T493">n</ta>
            <ta e="T495" id="Seg_6631" s="T494">v</ta>
            <ta e="T496" id="Seg_6632" s="T495">v</ta>
            <ta e="T497" id="Seg_6633" s="T496">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR" />
         <annotation name="SyF" tierref="SyF" />
         <annotation name="IST" tierref="IST" />
         <annotation name="Top" tierref="Top" />
         <annotation name="Foc" tierref="Foc" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T1" id="Seg_6634" s="T0">RUS:cult</ta>
            <ta e="T3" id="Seg_6635" s="T2">RUS:cult</ta>
            <ta e="T14" id="Seg_6636" s="T13">EV:core</ta>
            <ta e="T25" id="Seg_6637" s="T24">RUS:cult</ta>
            <ta e="T36" id="Seg_6638" s="T35">RUS:cult</ta>
            <ta e="T38" id="Seg_6639" s="T37">RUS:cult</ta>
            <ta e="T52" id="Seg_6640" s="T51">RUS:cult</ta>
            <ta e="T59" id="Seg_6641" s="T58">RUS:cult</ta>
            <ta e="T68" id="Seg_6642" s="T67">RUS:cult</ta>
            <ta e="T70" id="Seg_6643" s="T69">EV:core</ta>
            <ta e="T80" id="Seg_6644" s="T79">RUS:cult</ta>
            <ta e="T84" id="Seg_6645" s="T83">EV:core</ta>
            <ta e="T91" id="Seg_6646" s="T90">RUS:gram</ta>
            <ta e="T106" id="Seg_6647" s="T105">RUS:cult</ta>
            <ta e="T108" id="Seg_6648" s="T107">EV:core</ta>
            <ta e="T109" id="Seg_6649" s="T108">RUS:cult</ta>
            <ta e="T112" id="Seg_6650" s="T111">RUS:cult</ta>
            <ta e="T115" id="Seg_6651" s="T114">RUS:cult</ta>
            <ta e="T117" id="Seg_6652" s="T116">RUS:cult</ta>
            <ta e="T129" id="Seg_6653" s="T128">RUS:cult</ta>
            <ta e="T181" id="Seg_6654" s="T180">RUS:cult</ta>
            <ta e="T185" id="Seg_6655" s="T184">RUS:cult</ta>
            <ta e="T187" id="Seg_6656" s="T186">EV:core</ta>
            <ta e="T199" id="Seg_6657" s="T198">EV:core</ta>
            <ta e="T239" id="Seg_6658" s="T238">RUS:cult</ta>
            <ta e="T241" id="Seg_6659" s="T240">RUS:cult</ta>
            <ta e="T243" id="Seg_6660" s="T242">RUS:cult</ta>
            <ta e="T281" id="Seg_6661" s="T280">RUS:cult</ta>
            <ta e="T294" id="Seg_6662" s="T293">RUS:cult</ta>
            <ta e="T296" id="Seg_6663" s="T295">RUS:cult</ta>
            <ta e="T304" id="Seg_6664" s="T303">RUS:gram</ta>
            <ta e="T305" id="Seg_6665" s="T304">RUS:cult</ta>
            <ta e="T309" id="Seg_6666" s="T308">RUS:cult</ta>
            <ta e="T318" id="Seg_6667" s="T317">RUS:gram</ta>
            <ta e="T385" id="Seg_6668" s="T384">RUS:cult</ta>
            <ta e="T395" id="Seg_6669" s="T394">RUS:gram</ta>
            <ta e="T433" id="Seg_6670" s="T432">RUS:cult</ta>
            <ta e="T436" id="Seg_6671" s="T435">RUS:cult</ta>
            <ta e="T455" id="Seg_6672" s="T454">RUS:cult</ta>
            <ta e="T464" id="Seg_6673" s="T463">RUS:mod</ta>
            <ta e="T470" id="Seg_6674" s="T469">RUS:cult</ta>
            <ta e="T483" id="Seg_6675" s="T482">RUS:cult</ta>
            <ta e="T493" id="Seg_6676" s="T492">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T7" id="Seg_6677" s="T0">There lived a prince and a village elder, both of them had a son.</ta>
            <ta e="T10" id="Seg_6678" s="T7">They were both Dolgans.</ta>
            <ta e="T16" id="Seg_6679" s="T10">The boys grew up together and were very good friends.</ta>
            <ta e="T22" id="Seg_6680" s="T16">Those people nomadized, apparentely, since olden times.</ta>
            <ta e="T28" id="Seg_6681" s="T22">One autumn there came the time to install deadfall traps.</ta>
            <ta e="T32" id="Seg_6682" s="T28">They had spread baits in advance.</ta>
            <ta e="T37" id="Seg_6683" s="T32">The two boys went with their dogs to install the deadfall traps.</ta>
            <ta e="T42" id="Seg_6684" s="T37">After they has installed a lot of deadfall traps, they overnighted in their [hunter] tent.</ta>
            <ta e="T54" id="Seg_6685" s="T42">One evening, after having tied the dogs, they lit a lamp with fish fat, sat and drank tea.</ta>
            <ta e="T58" id="Seg_6686" s="T54">Suddenly the sound of a sledge was heard.</ta>
            <ta e="T61" id="Seg_6687" s="T58">The prince's son said:</ta>
            <ta e="T66" id="Seg_6688" s="T61">"Is it a good human who came?"</ta>
            <ta e="T69" id="Seg_6689" s="T66">He himself was sitting and smoking.</ta>
            <ta e="T72" id="Seg_6690" s="T69">His friend was lying on his back.</ta>
            <ta e="T76" id="Seg_6691" s="T72">They listened: their dogs stopped barking.</ta>
            <ta e="T79" id="Seg_6692" s="T76">Steps were heard.</ta>
            <ta e="T81" id="Seg_6693" s="T79">The prince's son:</ta>
            <ta e="T83" id="Seg_6694" s="T81">"I am afraid", he said.</ta>
            <ta e="T88" id="Seg_6695" s="T83">His friend kept lying on his back.</ta>
            <ta e="T105" id="Seg_6696" s="T88">The entrance was opened: a very fat woman, hardly fitting in, came in; all her clothes and her cap were beaded, she looked very rich.</ta>
            <ta e="T108" id="Seg_6697" s="T105">The village elder's son to his friend:</ta>
            <ta e="T111" id="Seg_6698" s="T108">"Offer the guest some tea", he said.</ta>
            <ta e="T118" id="Seg_6699" s="T111">The prince's son stood up, heated the stove and didn't(?) prepare tea.</ta>
            <ta e="T123" id="Seg_6700" s="T118">But the woman doesn't show her face and doesn't speak.</ta>
            <ta e="T141" id="Seg_6701" s="T123">After having lit up a match with fish fat from the kettle, the boy saw the woman sitting there and laughing, she turned out to have just one tooth. </ta>
            <ta e="T147" id="Seg_6702" s="T141">He went outside to get some wood and harnessed their dogs.</ta>
            <ta e="T153" id="Seg_6703" s="T147">He saw that six dogs were harnessed at he woman's sledge.</ta>
            <ta e="T157" id="Seg_6704" s="T153">He went inside and offered the guest some food.</ta>
            <ta e="T169" id="Seg_6705" s="T157">"I will go out and bring some ice", he said, took his clothes outside and threw it onto the dog sledge.</ta>
            <ta e="T178" id="Seg_6706" s="T169">He went inside again, let the woman sleep there, where she was sitting, and gave her a reindeer fur as bedding.</ta>
            <ta e="T183" id="Seg_6707" s="T178">They put their fish fat lamp out.</ta>
            <ta e="T184" id="Seg_6708" s="T183">They went to sleep.</ta>
            <ta e="T188" id="Seg_6709" s="T184">The village elder's son said to his friend:</ta>
            <ta e="T192" id="Seg_6710" s="T188">"Well, isn't it a girl?!</ta>
            <ta e="T193" id="Seg_6711" s="T192">Let's take up with her.</ta>
            <ta e="T195" id="Seg_6712" s="T193">What are you afraid of?</ta>
            <ta e="T198" id="Seg_6713" s="T195">Then I will go [to her]."</ta>
            <ta e="T199" id="Seg_6714" s="T198">His friend:</ta>
            <ta e="T204" id="Seg_6715" s="T199">"When I have gone out, go to the girl."</ta>
            <ta e="T206" id="Seg_6716" s="T204">He went out.</ta>
            <ta e="T210" id="Seg_6717" s="T206">After he dressed himself he sat down onto the sledge.</ta>
            <ta e="T215" id="Seg_6718" s="T210">After a while a death cry was heard.</ta>
            <ta e="T216" id="Seg_6719" s="T215">"Oh dear!</ta>
            <ta e="T217" id="Seg_6720" s="T216">Help!!!"</ta>
            <ta e="T224" id="Seg_6721" s="T217">That boy took the reins and drove away.</ta>
            <ta e="T228" id="Seg_6722" s="T224">Behidn him the woman's voice was heard:</ta>
            <ta e="T234" id="Seg_6723" s="T228">"If I had known that, I would have eaten him at once!"</ta>
            <ta e="T240" id="Seg_6724" s="T234">The boy came to his father, the prince, and told [everything].</ta>
            <ta e="T247" id="Seg_6725" s="T240">The prince and the village elder gathered people from seven tents together.</ta>
            <ta e="T251" id="Seg_6726" s="T247">They sent two boys to scout.</ta>
            <ta e="T256" id="Seg_6727" s="T251">The two boys went off.</ta>
            <ta e="T260" id="Seg_6728" s="T256">Will it take long? They came [there] by daylight.</ta>
            <ta e="T268" id="Seg_6729" s="T260">They saw that there was no human and no traces.</ta>
            <ta e="T276" id="Seg_6730" s="T268">They went into the pole tent, just the boy's bare bones were lying there.</ta>
            <ta e="T280" id="Seg_6731" s="T276">The two boys went back.</ta>
            <ta e="T282" id="Seg_6732" s="T280">They told the prince.</ta>
            <ta e="T293" id="Seg_6733" s="T282">There was an orphan child there, completely boldheaded, he lived with his lonely mother and had one reindeer.</ta>
            <ta e="T297" id="Seg_6734" s="T293">The prince and the village elder discussed:</ta>
            <ta e="T302" id="Seg_6735" s="T297">"How will we do: should we leave our land?</ta>
            <ta e="T308" id="Seg_6736" s="T302">But our deadfall traps are already out there", they said.</ta>
            <ta e="T312" id="Seg_6737" s="T308">There the village elder said:</ta>
            <ta e="T321" id="Seg_6738" s="T312">"Let's send the orphan child, I would even give him my daughter, if he kills [that woman]."</ta>
            <ta e="T326" id="Seg_6739" s="T321">They called that boy and told him everything.</ta>
            <ta e="T329" id="Seg_6740" s="T326">Well, what a fag!</ta>
            <ta e="T342" id="Seg_6741" s="T329">The poor boy had to go alone, they just gave him a dog, no rifle, nothing.</ta>
            <ta e="T346" id="Seg_6742" s="T342">He came home and cried.</ta>
            <ta e="T347" id="Seg_6743" s="T346">His mother:</ta>
            <ta e="T349" id="Seg_6744" s="T347">"What's up with you?"</ta>
            <ta e="T351" id="Seg_6745" s="T349">He told her everything.</ta>
            <ta e="T365" id="Seg_6746" s="T351">"Somewhere there was the father's glaive, take that along", the poor mother brought a very rusty glaive.</ta>
            <ta e="T371" id="Seg_6747" s="T365">Soon the boy went off.</ta>
            <ta e="T373" id="Seg_6748" s="T371">Will he make it there?</ta>
            <ta e="T380" id="Seg_6749" s="T373">He came there, nobody was there, just human bones.</ta>
            <ta e="T386" id="Seg_6750" s="T380">In the tent, as soon as it turned dark, he lit up the lamp.</ta>
            <ta e="T393" id="Seg_6751" s="T386">At the time, when humans go to sleep, the sound of a sledge was heard.</ta>
            <ta e="T396" id="Seg_6752" s="T393">The dogs didn't bark.</ta>
            <ta e="T400" id="Seg_6753" s="T396">He put the light out and sat there.</ta>
            <ta e="T403" id="Seg_6754" s="T400">A very loud sound was heard.</ta>
            <ta e="T408" id="Seg_6755" s="T403">A fat woman entered the tent, she hardly fit into the entrance.</ta>
            <ta e="T419" id="Seg_6756" s="T408">As she pushed her leg through the door, he stuck the glaive into her arm pits.</ta>
            <ta e="T423" id="Seg_6757" s="T419">He himself escaped from her clutches.</ta>
            <ta e="T426" id="Seg_6758" s="T423">At night he came back home.</ta>
            <ta e="T430" id="Seg_6759" s="T426">His poor mother didn't sleep, she was waiting for him.</ta>
            <ta e="T435" id="Seg_6760" s="T430">In the morning the mother went to the prince and told him [everything].</ta>
            <ta e="T439" id="Seg_6761" s="T435">The prince called that boy.</ta>
            <ta e="T441" id="Seg_6762" s="T439">"Well, what's up?</ta>
            <ta e="T443" id="Seg_6763" s="T441">Did you kill her?"</ta>
            <ta e="T449" id="Seg_6764" s="T443">"I don't know", said the boy and told what he had done.</ta>
            <ta e="T450" id="Seg_6765" s="T449">They gathered themselves.</ta>
            <ta e="T457" id="Seg_6766" s="T450">Six people were sent to scout, the village elder went with them, too.</ta>
            <ta e="T461" id="Seg_6767" s="T457">They went on reindeers and horses.</ta>
            <ta e="T464" id="Seg_6768" s="T461">They came there by day.</ta>
            <ta e="T468" id="Seg_6769" s="T464">Nobody was there.</ta>
            <ta e="T474" id="Seg_6770" s="T468">The upper part of the tent is torn, the entrance is open.</ta>
            <ta e="T482" id="Seg_6771" s="T474">They looked into the tent, there lies a girl with a potbelly.</ta>
            <ta e="T483" id="Seg_6772" s="T482">The village elder:</ta>
            <ta e="T485" id="Seg_6773" s="T483">"Let us burn down the tent!"</ta>
            <ta e="T490" id="Seg_6774" s="T485">They burned down the tent with the girl.</ta>
            <ta e="T497" id="Seg_6775" s="T490">The baldheaded boy married the village elder's daughter, he lived rich and wealthy.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T7" id="Seg_6776" s="T0">Es lebten ein Fürst und ein Dorfältester zusammen, beide hatten einen Sohn.</ta>
            <ta e="T10" id="Seg_6777" s="T7">Beide waren Dolganen.</ta>
            <ta e="T16" id="Seg_6778" s="T10">Die Jungen wuchsen gemeinsam auf und waren sehr befreundet.</ta>
            <ta e="T22" id="Seg_6779" s="T16">Diese Leute nomadisierten offenbar seit langer Zeit.</ta>
            <ta e="T28" id="Seg_6780" s="T22">Einmal im Herbst kam die Zeit, Totfallen zu stellen.</ta>
            <ta e="T32" id="Seg_6781" s="T28">Köder hatten sie schon vorher verteilt.</ta>
            <ta e="T37" id="Seg_6782" s="T32">Die zwei Jungen fuhren mit ihren Hunden die Totfallen aufstellen.</ta>
            <ta e="T42" id="Seg_6783" s="T37">Nachdem sie viele Totfallen aufgestellt hatten, übernachteten sie in ihrem [Jäger]Zelt.</ta>
            <ta e="T54" id="Seg_6784" s="T42">Eines Abends, nachdem sie die Hunde angebunden hatten, machten sie ihre Lampe mit Fischtran an, saßen und tranken Tee.</ta>
            <ta e="T58" id="Seg_6785" s="T54">Auf einmal war das Geräusch eines Schlittens zu hören.</ta>
            <ta e="T61" id="Seg_6786" s="T58">Der Sohn des Fürsten sagte:</ta>
            <ta e="T66" id="Seg_6787" s="T61">"Ob es wohl ein guter Mensch ist, der kommt?"</ta>
            <ta e="T69" id="Seg_6788" s="T66">Er selber saß und rauchte.</ta>
            <ta e="T72" id="Seg_6789" s="T69">Sein Freund lag auf dem Rücken.</ta>
            <ta e="T76" id="Seg_6790" s="T72">Sie horchten: ihre Hunde hörten auf zu bellen.</ta>
            <ta e="T79" id="Seg_6791" s="T76">Es waren Schritte zu hören.</ta>
            <ta e="T81" id="Seg_6792" s="T79">Der Sohn des Fürsten:</ta>
            <ta e="T83" id="Seg_6793" s="T81">"Ich habe Angst", sagte er.</ta>
            <ta e="T88" id="Seg_6794" s="T83">Sein Freund lag immer noch so auf dem Rücken.</ta>
            <ta e="T105" id="Seg_6795" s="T88">Der Eingang öffnete sich: ins Zelt, kaum hineinpassend, kam eine sehr dicke Frau herein; ihre ganze Kleidung und ihre Mütze waren mit Perlen besetzt, sie sah sehr reich aus.</ta>
            <ta e="T108" id="Seg_6796" s="T105">Der Sohn des Dorfältesten zu seinem Freund:</ta>
            <ta e="T111" id="Seg_6797" s="T108">"Bewirte den Gast mit Tee", sagte er.</ta>
            <ta e="T118" id="Seg_6798" s="T111">Der Sohn des Fürsten stand auf, heizte den Ofen und kochte keinen(?) Tee.</ta>
            <ta e="T123" id="Seg_6799" s="T118">Die Frau aber zeigt ihr Gesicht nicht und unterhält sich nicht.</ta>
            <ta e="T141" id="Seg_6800" s="T123">Nachdem dieser Junge es sich mit einem Streichholz, das er in Fischtran getränkt hatte, hell gemacht hatte, sah er, dass die Frau da saß und lachte, sie hatte offenbar nur einen einzigen Zahn.</ta>
            <ta e="T147" id="Seg_6801" s="T141">Er ging hinaus, um Holz zu holen, und spannte ihre Hunde an.</ta>
            <ta e="T153" id="Seg_6802" s="T147">Er sah, dass am Schlitten der Frau sechs Hunde angespannt waren.</ta>
            <ta e="T157" id="Seg_6803" s="T153">Er ging hinein und gab dem Gast zu essen.</ta>
            <ta e="T169" id="Seg_6804" s="T157">"Ich gehe hinaus und hole Eis", sagte er, nahm seine Kleidung mit nach draußen und packte sie auf seinen Hundeschlitten.</ta>
            <ta e="T178" id="Seg_6805" s="T169">Er ging wieder hinein, ließ die Frau dort schlafen, wo sie gesessen hatte, und gab ihr ein Rentierfell als Unterlage. </ta>
            <ta e="T183" id="Seg_6806" s="T178">Sie löschten ihre Fischtranleuchte.</ta>
            <ta e="T184" id="Seg_6807" s="T183">Sie legten sich schlafen.</ta>
            <ta e="T188" id="Seg_6808" s="T184">Der Sohn des Dorfältesten sagte zu seinem Freund:</ta>
            <ta e="T192" id="Seg_6809" s="T188">"Nun, das ist wohl ein Mädchen?!</ta>
            <ta e="T193" id="Seg_6810" s="T192">Lass uns mit ihr anbandeln.</ta>
            <ta e="T195" id="Seg_6811" s="T193">Wovor hast du Angst?</ta>
            <ta e="T198" id="Seg_6812" s="T195">Dann gehe ich [zu ihr hin]."</ta>
            <ta e="T199" id="Seg_6813" s="T198">Sein Freund:</ta>
            <ta e="T204" id="Seg_6814" s="T199">"Wenn ich raus gegangen bin, dann geh du zu dem Mädchen."</ta>
            <ta e="T206" id="Seg_6815" s="T204">Er ging hinaus.</ta>
            <ta e="T210" id="Seg_6816" s="T206">Nachdem er sich angezogen hatte, setzte er sich auf seinen Schlitten.</ta>
            <ta e="T215" id="Seg_6817" s="T210">Nach einiger Zeit war ein Todesschrei zu hören:</ta>
            <ta e="T216" id="Seg_6818" s="T215">"Oh nein!</ta>
            <ta e="T217" id="Seg_6819" s="T216">Hilfe!!!"</ta>
            <ta e="T224" id="Seg_6820" s="T217">Jener Junge griff die Zügel und fuhr davon.</ta>
            <ta e="T228" id="Seg_6821" s="T224">Hinter ihm war die Stimme der Frau zu hören:</ta>
            <ta e="T234" id="Seg_6822" s="T228">"Wenn ich das gewusst hätte, hätte ich ihn sofort gegessen!"</ta>
            <ta e="T240" id="Seg_6823" s="T234">Der Junge kam zu seinem Vater, dem Fürsten, und erzählte.</ta>
            <ta e="T247" id="Seg_6824" s="T240">Der Fürst und der Dorfälteste versammelten Leuten aus sieben Zelten.</ta>
            <ta e="T251" id="Seg_6825" s="T247">Zwei Jungen schickte sie zum auskundschaften voraus.</ta>
            <ta e="T256" id="Seg_6826" s="T251">Die zwei Jungen gingen los.</ta>
            <ta e="T260" id="Seg_6827" s="T256">Ist es lange für sie? Sie kamen im Hellen an.</ta>
            <ta e="T268" id="Seg_6828" s="T260">Sie sahen, dass weder Mensch noch Spuren da waren.</ta>
            <ta e="T276" id="Seg_6829" s="T268">Sie gingen ins Stangenzelt hinein, dort lagen nur die abgenagten Knochen des Jungen.</ta>
            <ta e="T280" id="Seg_6830" s="T276">Die zwei Jungen kehrten zurück.</ta>
            <ta e="T282" id="Seg_6831" s="T280">Sie erzählten dem Fürsten.</ta>
            <ta e="T293" id="Seg_6832" s="T282">Es war dort ein Waisenjunge, ganz glatzköpfig, der lebte mit seiner einsamen Mutter und hatte ein Rentier.</ta>
            <ta e="T297" id="Seg_6833" s="T293">Der Fürst und der Dorfälteste berieten sich:</ta>
            <ta e="T302" id="Seg_6834" s="T297">"Wie ist es: sollen wir unser Land aufgeben?</ta>
            <ta e="T308" id="Seg_6835" s="T302">Aber unsere Totfallen sind auch schon da", sagten sie.</ta>
            <ta e="T312" id="Seg_6836" s="T308">Da sagte der Dorfälteste:</ta>
            <ta e="T321" id="Seg_6837" s="T312">"Schicken wir doch den Waisenjungen, ich würde ihm sogar meine Tochter geben, wenn er [die Frau] tötet."</ta>
            <ta e="T326" id="Seg_6838" s="T321">Sie riefen diesen Jungen und erzählten alles.</ta>
            <ta e="T329" id="Seg_6839" s="T326">Nun, was für eine Sache!</ta>
            <ta e="T342" id="Seg_6840" s="T329">Der arme Junge musste alleine gehen, sie gaben ihm nur einen Hund; ein Gewehr oder sonst etwas hatte er nicht.</ta>
            <ta e="T346" id="Seg_6841" s="T342">Er kam nach Hause und weinte.</ta>
            <ta e="T347" id="Seg_6842" s="T346">Seine Mutter:</ta>
            <ta e="T349" id="Seg_6843" s="T347">"Was ist mit dir?"</ta>
            <ta e="T351" id="Seg_6844" s="T349">Er erzählte alles.</ta>
            <ta e="T365" id="Seg_6845" s="T351">"Irgendwo war die Glefe des Vaters, nimm diese mit", brachte die arme Mutter eine völlig verrostete Glefe.</ta>
            <ta e="T371" id="Seg_6846" s="T365">Bald ging der Junge los.</ta>
            <ta e="T373" id="Seg_6847" s="T371">Ist er verschwunden?</ta>
            <ta e="T380" id="Seg_6848" s="T373">Er kam an, niemand war da, nur menschliche Knochen.</ta>
            <ta e="T386" id="Seg_6849" s="T380">Im Zelt, nachdem es dunkel geworden war, zündete er die Leuchte an.</ta>
            <ta e="T393" id="Seg_6850" s="T386">Als die Zeit kam, wo sich die Menschen schlafen legen, war das Geräusch eines Schlittens zu hören.</ta>
            <ta e="T396" id="Seg_6851" s="T393">Die Hunde bellten nicht.</ta>
            <ta e="T400" id="Seg_6852" s="T396">Er machte das Licht aus und saß da.</ta>
            <ta e="T403" id="Seg_6853" s="T400">Es ertönte ein sehr lautes Geräusch.</ta>
            <ta e="T408" id="Seg_6854" s="T403">Es kam eine Frau herein, die kaum durch den Eingang passte.</ta>
            <ta e="T419" id="Seg_6855" s="T408">Als sie gerade ein Bein in die Tür gesetzt hatte, da stieß er ihr mit der Gleife von unten in die Achselhöhlen.</ta>
            <ta e="T423" id="Seg_6856" s="T419">Er selber entfloh aus ihren Fängen.</ta>
            <ta e="T426" id="Seg_6857" s="T423">In der Nacht kam er nach Hause.</ta>
            <ta e="T430" id="Seg_6858" s="T426">Seine arme Mutter schlief nicht, sie wartete auf ihn.</ta>
            <ta e="T435" id="Seg_6859" s="T430">Am Morgen ging die Mutter zum Fürsten und erzählte.</ta>
            <ta e="T439" id="Seg_6860" s="T435">Der Fürst rief diesen Jungen.</ta>
            <ta e="T441" id="Seg_6861" s="T439">"Nun, wie ist es?</ta>
            <ta e="T443" id="Seg_6862" s="T441">Hast du sie getötet?"</ta>
            <ta e="T449" id="Seg_6863" s="T443">"Ich weiß es nicht", sagte der Junge und erzählte, was er gemacht hatte.</ta>
            <ta e="T450" id="Seg_6864" s="T449">Sie versammelten sich.</ta>
            <ta e="T457" id="Seg_6865" s="T450">Sechs Leute schickten sie zum auskundschaften, der Dorfälteste ging auch mit.</ta>
            <ta e="T461" id="Seg_6866" s="T457">Sie gingen mit Rentieren und mit Pferden.</ta>
            <ta e="T464" id="Seg_6867" s="T461">Sie kamen noch bei Tage an.</ta>
            <ta e="T468" id="Seg_6868" s="T464">Niemand war da.</ta>
            <ta e="T474" id="Seg_6869" s="T468">Der obere Teil des Zeltes ist zerrissen, der Eingang steht offen.</ta>
            <ta e="T482" id="Seg_6870" s="T474">Sie schauten ins Zelt hinein, dort lag ein Mädchen mit aufgeblähtem Bauch.</ta>
            <ta e="T483" id="Seg_6871" s="T482">Der Dorfälteste:</ta>
            <ta e="T485" id="Seg_6872" s="T483">"Los, verbrennen wir das Zelt!"</ta>
            <ta e="T490" id="Seg_6873" s="T485">Sie verbrannten das Zelt mit dem Mädchen.</ta>
            <ta e="T497" id="Seg_6874" s="T490">Der glatzköpfige Junge nahm die Tochter des Dorfältesten zur Frau und lebte in Wohlstand und Reichtum.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T7" id="Seg_6875" s="T0">Жили князь со старшиной, у каждого сын.</ta>
            <ta e="T10" id="Seg_6876" s="T7">Оба долганы.</ta>
            <ta e="T16" id="Seg_6877" s="T10">Парни росли вместе и были большими друзьями.</ta>
            <ta e="T22" id="Seg_6878" s="T16">Эти люди, оказывается, с давних времен кочевали.</ta>
            <ta e="T28" id="Seg_6879" s="T22">Однажды осенью подошло время ставить пасти [на песцов].</ta>
            <ta e="T32" id="Seg_6880" s="T28">Приваду разбросали заранее.</ta>
            <ta e="T37" id="Seg_6881" s="T32">Два парня на собаках поехали ставить пасти.</ta>
            <ta e="T42" id="Seg_6882" s="T37">Насторожив много пастей, стали жить в своем охотничьем чуме.</ta>
            <ta e="T54" id="Seg_6883" s="T42">Однажды вечером, привязав собак, засветили светильник на рыбьем жире и стали пить чай.</ta>
            <ta e="T58" id="Seg_6884" s="T54">Вдруг послышался скрип саней.</ta>
            <ta e="T61" id="Seg_6885" s="T58">Сын князя сказал:</ta>
            <ta e="T66" id="Seg_6886" s="T61">"Добрый ли человек приехал?"</ta>
            <ta e="T69" id="Seg_6887" s="T66">Сам он сидел и курил.</ta>
            <ta e="T72" id="Seg_6888" s="T69">А друг лежал, беспечно раскинувшись.</ta>
            <ta e="T76" id="Seg_6889" s="T72">Прислушались: собаки перестали лаять.</ta>
            <ta e="T79" id="Seg_6890" s="T76">Послышались чьи-то шаги.</ta>
            <ta e="T81" id="Seg_6891" s="T79">Сын князя:</ta>
            <ta e="T83" id="Seg_6892" s="T81">"Я боюсь", сказал.</ta>
            <ta e="T88" id="Seg_6893" s="T83">А друг его по-прежнему беспечно лежал.</ta>
            <ta e="T105" id="Seg_6894" s="T88">Открылся полог — в чум еле-еле влезла очень толстая женщина. Вся ее одежда, шапка были обшиты бисером, на вид очень богатая.</ta>
            <ta e="T108" id="Seg_6895" s="T105">Сын старшины другу:</ta>
            <ta e="T111" id="Seg_6896" s="T108">"Гостью угости чаем", сказал.</ta>
            <ta e="T118" id="Seg_6897" s="T111">Сын князя встал, разжег очаг и не(?) вскипятил чай.</ta>
            <ta e="T123" id="Seg_6898" s="T118">А та женщина свое лицо не показывает и не разговаривает.</ta>
            <ta e="T141" id="Seg_6899" s="T123">Этот парень подсветил себе спичкой, обмакнув ее в рыбий жир, и увидел, что женщина сидит и смеется, во рту у нее, оказывается, один-единственный зуб.</ta>
            <ta e="T147" id="Seg_6900" s="T141">Вышел принести дров и запряг своих собак.</ta>
            <ta e="T153" id="Seg_6901" s="T147">Посмотрел, в нарты женщины запряжено шесть собак.</ta>
            <ta e="T157" id="Seg_6902" s="T153">Зашел [в чум] и накормил гостью.</ta>
            <ta e="T169" id="Seg_6903" s="T157">"Пойду, принесу льда", вышел, прихватив свою верхнюю одежду, которую положил на свои собачьи нарты.</ta>
            <ta e="T178" id="Seg_6904" s="T169">Снова войдя, предложил женщине спать там, где она сидела, дал ей оленью шкуру на подстилку.</ta>
            <ta e="T183" id="Seg_6905" s="T178">Светильник погасили.</ta>
            <ta e="T184" id="Seg_6906" s="T183">Улеглись спать.</ta>
            <ta e="T188" id="Seg_6907" s="T184">Сын старшины сказал приятелю:</ta>
            <ta e="T192" id="Seg_6908" s="T188">"Друг, она ведь девушка?!</ta>
            <ta e="T193" id="Seg_6909" s="T192">Давай полюбезничаем с ней.</ta>
            <ta e="T195" id="Seg_6910" s="T193">Чего ты боишься?</ta>
            <ta e="T198" id="Seg_6911" s="T195">Я тогда сам [к ней] полезу."</ta>
            <ta e="T199" id="Seg_6912" s="T198">Друг:</ta>
            <ta e="T204" id="Seg_6913" s="T199">"Когда я выйду, тогда иди к девушке."</ta>
            <ta e="T206" id="Seg_6914" s="T204">Тут же вышел.</ta>
            <ta e="T210" id="Seg_6915" s="T206">Надев [верхнюю одежду], сел на свои нарты.</ta>
            <ta e="T215" id="Seg_6916" s="T210">Немного времени прошло и послышался предсмертный крик:</ta>
            <ta e="T216" id="Seg_6917" s="T215">"Ой, больно!</ta>
            <ta e="T217" id="Seg_6918" s="T216">Спаси!!!"</ta>
            <ta e="T224" id="Seg_6919" s="T217">Тут парень схватил поводья и умчался.</ta>
            <ta e="T228" id="Seg_6920" s="T224">Сзади послышался крик женщин:</ta>
            <ta e="T234" id="Seg_6921" s="T228">"Если б знала, съела бы сразу!"</ta>
            <ta e="T240" id="Seg_6922" s="T234">Приехав, этот парень рассказал [все] своему отцу-князю.</ta>
            <ta e="T247" id="Seg_6923" s="T240">Князь со старшиной собрали народ из семи чумов и стали совещаться.</ta>
            <ta e="T251" id="Seg_6924" s="T247">Послали двух парней, чтоб разведали.</ta>
            <ta e="T256" id="Seg_6925" s="T251">Те два парня уехали.</ta>
            <ta e="T260" id="Seg_6926" s="T256">Долго ли им? приехали туда засветло.</ta>
            <ta e="T268" id="Seg_6927" s="T260">Посмотрели: никого нет, и следов не видно.</ta>
            <ta e="T276" id="Seg_6928" s="T268">Зашли в урасу, лежат там только обглоданные кости парня.</ta>
            <ta e="T280" id="Seg_6929" s="T276">Те двое возвратились.</ta>
            <ta e="T282" id="Seg_6930" s="T280">Рассказали князю.</ta>
            <ta e="T293" id="Seg_6931" s="T282">Был там один парень, совсем плешивый, жил с матерью, имел только одного оленя.</ta>
            <ta e="T297" id="Seg_6932" s="T293">Князь со старшиной стали советоваться:</ta>
            <ta e="T302" id="Seg_6933" s="T297">"Как быть: не бросать же нам свои земли?</ta>
            <ta e="T308" id="Seg_6934" s="T302">Да и пасти мы там расставили", сказали.</ta>
            <ta e="T312" id="Seg_6935" s="T308">Старшина тут сказал:</ta>
            <ta e="T321" id="Seg_6936" s="T312">"Пошлем парня-сироту, я бы даже выдал дочь за него, если бы убил [ту женщину]."</ta>
            <ta e="T326" id="Seg_6937" s="T321">Позвали того парня, рассказали обо всем.</ta>
            <ta e="T329" id="Seg_6938" s="T326">Вот беда какая!</ta>
            <ta e="T342" id="Seg_6939" s="T329">Бедному парню идти одному; дали только собак, ни ружья, ничего нет.</ta>
            <ta e="T346" id="Seg_6940" s="T342">Пришел домой, стал плакать.</ta>
            <ta e="T347" id="Seg_6941" s="T346">Его мать:</ta>
            <ta e="T349" id="Seg_6942" s="T347">"Что с тобой?"</ta>
            <ta e="T351" id="Seg_6943" s="T349">Рассказал все.</ta>
            <ta e="T365" id="Seg_6944" s="T351">"Где-то была пальма отца, возьми ее с собой", бедняжка мать принесла совсем заржавевшую пальму.</ta>
            <ta e="T371" id="Seg_6945" s="T365">Вскоре этот парень уехал.</ta>
            <ta e="T373" id="Seg_6946" s="T371">Куда денется?</ta>
            <ta e="T380" id="Seg_6947" s="T373">Приехал — никого нет, только человеческие кости [лежат].</ta>
            <ta e="T386" id="Seg_6948" s="T380">В чуме, как стемнело, зажег светильник.</ta>
            <ta e="T393" id="Seg_6949" s="T386">В то время, когда люди укладываются спать, раздался скрип саней.</ta>
            <ta e="T396" id="Seg_6950" s="T393">И собаки не залаяли.</ta>
            <ta e="T400" id="Seg_6951" s="T396">Погасил светильник и сидел.</ta>
            <ta e="T403" id="Seg_6952" s="T400">Послышался сильный шум.</ta>
            <ta e="T408" id="Seg_6953" s="T403">Стала в чум влезать с трудом толстая женщина.</ta>
            <ta e="T419" id="Seg_6954" s="T408">Как только занесла одну ногу в проход, воткнул пальму ей в подмышку.</ta>
            <ta e="T423" id="Seg_6955" s="T419">Сам выскользнул из-под ее руки.</ta>
            <ta e="T426" id="Seg_6956" s="T423">Ночью приехал домой.</ta>
            <ta e="T430" id="Seg_6957" s="T426">Бедняжка мать не спала, ждала его.</ta>
            <ta e="T435" id="Seg_6958" s="T430">Утром мать обо всем рассказала князю.</ta>
            <ta e="T439" id="Seg_6959" s="T435">Князь позвал того парня.</ta>
            <ta e="T441" id="Seg_6960" s="T439">"Ну, как?</ta>
            <ta e="T443" id="Seg_6961" s="T441">Убил ее?"</ta>
            <ta e="T449" id="Seg_6962" s="T443">"Не знаю", сказал парень, рассказал, что он сделал.</ta>
            <ta e="T450" id="Seg_6963" s="T449">Посовещались.</ta>
            <ta e="T457" id="Seg_6964" s="T450">Шесть человек послали разведать, старшина тоже поехал.</ta>
            <ta e="T461" id="Seg_6965" s="T457">На оленях и на лошадях поехали.</ta>
            <ta e="T464" id="Seg_6966" s="T461">Подъехали еще днем.</ta>
            <ta e="T468" id="Seg_6967" s="T464">Никого нет.</ta>
            <ta e="T474" id="Seg_6968" s="T468">Верхняя часть чума разодрана, полог откинут.</ta>
            <ta e="T482" id="Seg_6969" s="T474">Заглянули в чум, там лежит девушка со вздутым животом.</ta>
            <ta e="T483" id="Seg_6970" s="T482">Старшина:</ta>
            <ta e="T485" id="Seg_6971" s="T483">"Давайте сожжем чум!"</ta>
            <ta e="T490" id="Seg_6972" s="T485">Этот чум с трупом девушки сожгли.</ta>
            <ta e="T497" id="Seg_6973" s="T490">Плешивый парень, взяв в жены дочь старшины, стал жить в богатстве и довольстве.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T7" id="Seg_6974" s="T0">Жили князь со старшиной, у каждого сын.</ta>
            <ta e="T10" id="Seg_6975" s="T7">Оба долганы.</ta>
            <ta e="T16" id="Seg_6976" s="T10">Парни росли вместе и были большими друзьями.</ta>
            <ta e="T22" id="Seg_6977" s="T16">Эти люди, оказывается, с давних времен кочевали.</ta>
            <ta e="T28" id="Seg_6978" s="T22">Однажды осенью подошло время ставить пасти [на песцов].</ta>
            <ta e="T32" id="Seg_6979" s="T28">Приваду разбросали заранее.</ta>
            <ta e="T37" id="Seg_6980" s="T32">Два парня на собаках поехали ставить пасти.</ta>
            <ta e="T42" id="Seg_6981" s="T37">Насторожив много пастей, стали жить в своем охотничьем чуме.</ta>
            <ta e="T54" id="Seg_6982" s="T42">Однажды вечером, привязав собак, засветили светильник на рыбьем жире и стали пить чай.</ta>
            <ta e="T58" id="Seg_6983" s="T54">Вдруг послышался скрип саней.</ta>
            <ta e="T61" id="Seg_6984" s="T58">Сын князя сказал:</ta>
            <ta e="T66" id="Seg_6985" s="T61">— Добрый ли человек приехал?</ta>
            <ta e="T69" id="Seg_6986" s="T66">Сам он сидел и курил.</ta>
            <ta e="T72" id="Seg_6987" s="T69">А друг лежал, беспечно раскинувшись.</ta>
            <ta e="T76" id="Seg_6988" s="T72">Прислушались: собаки перестали лаять.</ta>
            <ta e="T79" id="Seg_6989" s="T76">Послышались чьи-то шаги.</ta>
            <ta e="T81" id="Seg_6990" s="T79">Сын князя сказал:</ta>
            <ta e="T83" id="Seg_6991" s="T81">— Я боюсь.</ta>
            <ta e="T88" id="Seg_6992" s="T83">А друг его по-прежнему беспечно лежал.</ta>
            <ta e="T105" id="Seg_6993" s="T88">Открылся полог — в чум еле-еле влезла очень толстая женщина. Вся ее одежда, шапка были обшиты бисером, на вид очень богатая.</ta>
            <ta e="T108" id="Seg_6994" s="T105">Сын старшины сказал другу:</ta>
            <ta e="T111" id="Seg_6995" s="T108">— Гостью угости чаем.</ta>
            <ta e="T118" id="Seg_6996" s="T111">Сын князя встал, разжег очаг и вскипятил чай.</ta>
            <ta e="T123" id="Seg_6997" s="T118">А та женщина свое лицо не показывает и не разговаривает.</ta>
            <ta e="T141" id="Seg_6998" s="T123">Этот парень подсветил себе спичкой, обмакнув ее в рыбий жир, и увидел, что женщина сидит и смеется, во рту у нее, оказывается, один-единственный зуб.</ta>
            <ta e="T147" id="Seg_6999" s="T141">Вышел принести дров и запряг своих собак.</ta>
            <ta e="T153" id="Seg_7000" s="T147">Посмотрел, в нарты женщины запряжено шесть собак.</ta>
            <ta e="T157" id="Seg_7001" s="T153">Зашел [в чум] и накормил гостью. Затем сказав:</ta>
            <ta e="T169" id="Seg_7002" s="T157">— Пойду, принесу льда [для воды], — вышел, прихватив свою верхнюю одежду, которую положил на свои собачьи нарты.</ta>
            <ta e="T178" id="Seg_7003" s="T169">Снова войдя, предложил женщине спать там, где она сидела, дал ей оленью шкуру на подстилку.</ta>
            <ta e="T183" id="Seg_7004" s="T178">Светильник погасили.</ta>
            <ta e="T184" id="Seg_7005" s="T183">Улеглись спать.</ta>
            <ta e="T188" id="Seg_7006" s="T184">Сын старшины говорит приятелю:</ta>
            <ta e="T192" id="Seg_7007" s="T188">— Друг, она ведь девушка?!</ta>
            <ta e="T193" id="Seg_7008" s="T192">Давай полюбезничаем с ней.</ta>
            <ta e="T195" id="Seg_7009" s="T193">Чего ты боишься?</ta>
            <ta e="T198" id="Seg_7010" s="T195">Я тогда сам к ней полезу.</ta>
            <ta e="T199" id="Seg_7011" s="T198">Друг [попросил]:</ta>
            <ta e="T204" id="Seg_7012" s="T199">— Когда я выйду, тогда иди к девушке.</ta>
            <ta e="T206" id="Seg_7013" s="T204">Тут же вышел.</ta>
            <ta e="T210" id="Seg_7014" s="T206">Надев [верхнюю одежду], сел на свои нарты.</ta>
            <ta e="T215" id="Seg_7015" s="T210">Немного времени прошло и послышался предсмертный крик:</ta>
            <ta e="T216" id="Seg_7016" s="T215">— Ой, больно!…</ta>
            <ta e="T217" id="Seg_7017" s="T216">Спаси!!!</ta>
            <ta e="T224" id="Seg_7018" s="T217">Тут парень схватил поводья и умчался.</ta>
            <ta e="T228" id="Seg_7019" s="T224">Сзади послышался крик женщин:</ta>
            <ta e="T234" id="Seg_7020" s="T228">— Если б знала, съела бы сразу!</ta>
            <ta e="T240" id="Seg_7021" s="T234">Приехав, этот парень рассказал [все] своему отцу-князю.</ta>
            <ta e="T247" id="Seg_7022" s="T240">Князь со старшиной собрали народ из семи чумов и стали совещаться.</ta>
            <ta e="T251" id="Seg_7023" s="T247">Послали двух парней, чтоб разведали.</ta>
            <ta e="T256" id="Seg_7024" s="T251">Те два парня уехали.</ta>
            <ta e="T260" id="Seg_7025" s="T256">Долго ли им? приехали туда засветло.</ta>
            <ta e="T268" id="Seg_7026" s="T260">Посмотрели: никого нет, и следов не видно.</ta>
            <ta e="T276" id="Seg_7027" s="T268">Зашли в урасу, лежат там только обглоданные кости парня.</ta>
            <ta e="T280" id="Seg_7028" s="T276">Те двое возвратились.</ta>
            <ta e="T282" id="Seg_7029" s="T280">Рассказали князю.</ta>
            <ta e="T293" id="Seg_7030" s="T282">Был там один парень, совсем плешивый, жил с матерью, имел только одного оленя.</ta>
            <ta e="T297" id="Seg_7031" s="T293">Князь со старшиной стали советоваться:</ta>
            <ta e="T302" id="Seg_7032" s="T297">— Как быть: не бросать же нам свои земли?</ta>
            <ta e="T308" id="Seg_7033" s="T302">Да и пасти мы там расставили…</ta>
            <ta e="T312" id="Seg_7034" s="T308">Старшина тут говорит:</ta>
            <ta e="T321" id="Seg_7035" s="T312">— Пошлем парня-сироту, я бы даже выдал дочь за него, если бы убил [ту женщину].</ta>
            <ta e="T326" id="Seg_7036" s="T321">Позвали того парня, рассказали обо всем.</ta>
            <ta e="T329" id="Seg_7037" s="T326">Вот беда какая!</ta>
            <ta e="T342" id="Seg_7038" s="T329">Бедному парню идти одному; дали только собак, ни ружья, ничего нет.</ta>
            <ta e="T346" id="Seg_7039" s="T342">Пришел домой, стал плакать.</ta>
            <ta e="T347" id="Seg_7040" s="T346">Мать спрашивает:</ta>
            <ta e="T349" id="Seg_7041" s="T347">— Что с тобой?</ta>
            <ta e="T351" id="Seg_7042" s="T349">Рассказал все.</ta>
            <ta e="T365" id="Seg_7043" s="T351">— Где-то была пальма отца, возьми ее с собой, — бедняжка мать принесла совсем заржавевшую пальму.</ta>
            <ta e="T371" id="Seg_7044" s="T365">Вскоре этот парень уехал.</ta>
            <ta e="T373" id="Seg_7045" s="T371">Куда денется?</ta>
            <ta e="T380" id="Seg_7046" s="T373">Приехал — никого нет, только человеческие кости [лежат].</ta>
            <ta e="T386" id="Seg_7047" s="T380">В чуме, как стемнело, зажег светильник.</ta>
            <ta e="T393" id="Seg_7048" s="T386">В то время, когда люди укладываются спать, раздался скрип саней.</ta>
            <ta e="T396" id="Seg_7049" s="T393">И собаки не залаяли.</ta>
            <ta e="T400" id="Seg_7050" s="T396">Погасил светильник и стал ждать.</ta>
            <ta e="T403" id="Seg_7051" s="T400">Послышался сильный шум.</ta>
            <ta e="T408" id="Seg_7052" s="T403">Стала в чум влезать с трудом толстая женщина.</ta>
            <ta e="T419" id="Seg_7053" s="T408">Как только занесла одну ногу в проход, воткнул пальму ей в подмышку.</ta>
            <ta e="T423" id="Seg_7054" s="T419">Сам выскользнул из-под ее руки и убежал.</ta>
            <ta e="T426" id="Seg_7055" s="T423">Ночью приехал домой.</ta>
            <ta e="T430" id="Seg_7056" s="T426">Бедняжка мать не спала, ждала его.</ta>
            <ta e="T435" id="Seg_7057" s="T430">Утром мать обо всем рассказала князю.</ta>
            <ta e="T439" id="Seg_7058" s="T435">Князь позвал того парня.</ta>
            <ta e="T441" id="Seg_7059" s="T439">— Ну, как?</ta>
            <ta e="T443" id="Seg_7060" s="T441">Убил ее?</ta>
            <ta e="T449" id="Seg_7061" s="T443">— Не знаю, — говорит парень, рассказал, как было.</ta>
            <ta e="T450" id="Seg_7062" s="T449">Посовещались.</ta>
            <ta e="T457" id="Seg_7063" s="T450">Шесть человек послали разведать, старшина тоже поехал.</ta>
            <ta e="T461" id="Seg_7064" s="T457">На оленях и на лошадях поехали.</ta>
            <ta e="T464" id="Seg_7065" s="T461">Подъехали еще днем.</ta>
            <ta e="T468" id="Seg_7066" s="T464">Никого нет.</ta>
            <ta e="T474" id="Seg_7067" s="T468">Верхняя часть чума разодрана, полог откинут.</ta>
            <ta e="T482" id="Seg_7068" s="T474">Заглянули в чум, там лежит девушка со вздутым животом.</ta>
            <ta e="T483" id="Seg_7069" s="T482">Старшина [распорядился ]:</ta>
            <ta e="T485" id="Seg_7070" s="T483">— Давайте сожжем чум!</ta>
            <ta e="T490" id="Seg_7071" s="T485">Этот чум с трупом девушки сожгли.</ta>
            <ta e="T497" id="Seg_7072" s="T490">Плешивый парень, взяв в жены дочь старшины, стал жить в богатстве и довольстве.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T118" id="Seg_7073" s="T111">[DCh]: Not understandable why the boy does not make tea; maybe the form should be "kɨːjnʼarbɨt" which would be affirmative.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg"
                          name="gg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top"
                          name="Top"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc"
                          name="Foc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
